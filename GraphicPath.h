// GraphicPath.h: Schnittstelle f�r die Klasse CGraphicPath.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRAPHICPATH_H__AD440718_BBE4_4E22_B841_2BF1E7480888__INCLUDED_)
#define AFX_GRAPHICPATH_H__AD440718_BBE4_4E22_B841_2BF1E7480888__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


extern BOOL g_bIsContentView;


typedef double GP_VECTOR[4];

typedef struct gp_line
{
	GP_VECTOR a, b;
}GP_LINE;

typedef struct gp_point
{
	double m_fX, m_fY;
}GP_POINT;


class CGraphicPathElement : public CObject
{
public:
    DECLARE_SERIAL( CGraphicPathElement )
	CGraphicPathElement();
	CGraphicPathElement(int nType, CObject* pgpElement);
	CGraphicPathElement(const CGraphicPathElement& gpElement); // copy constructor

enum gpelemtype{GPTYPE_UNKNOWN, GPTYPE_VERTEX, GPTYPE_CIRCLE, GPTYPE_ARC, GPTYPE_SEQEND};

private:
	double	 m_fDummy;
	GP_POINT m_pt;

//Attributes:
public:
	COLORREF m_crColor;

//Operations:
public:
	virtual int		  GetType() { return CGraphicPathElement::GPTYPE_UNKNOWN; };
	virtual GP_POINT& GetVertexCoord() { return m_pt; };
	virtual double&	  GetVertexIndent() { return m_fDummy; };
	virtual void	  SetVertexIndent(double /*fIndent*/) {};
	virtual double&	  GetCircleRadius() {return m_fDummy; };
	virtual GP_POINT& GetCircleCenter() { return m_pt; };
	BOOL			  IsArc()	   { return (GetType() == CGraphicPathElement::GPTYPE_ARC)	? TRUE : FALSE;};
	BOOL			  IsCircle() { return (GetType() == CGraphicPathElement::GPTYPE_CIRCLE) ? TRUE : FALSE;};
	BOOL			  IsVertex() { return (GetType() == CGraphicPathElement::GPTYPE_VERTEX) ? TRUE : FALSE;};
	static void		  RotatePoint(double& fX, double& fY, int nAngle);
	static void		  MirrorPoint(double& fX, double& fY, int nMirrorAxis);

	virtual void  Copy(const CGraphicPathElement& gpElement);
	virtual const CGraphicPathElement& operator= (const CGraphicPathElement& gpElement);
	void		  Serialize(CArchive& ar);
};

class CGraphicPathArc : public CGraphicPathElement
{
public:
    DECLARE_SERIAL( CGraphicPathArc )
	CGraphicPathArc();
	CGraphicPathArc(double fXStart, double fYStart, double fXEnd, double fYEnd, double fIndent);

//Attributes:
public:
	GP_POINT m_ptCenter;
	double	 m_fRadius, m_fStartAngle, m_fEndAngle;

//Operations:
public:
	void		Copy(const CGraphicPathElement& gpElement);
	const		CGraphicPathArc& operator=(const CGraphicPathArc& gpArc);
	void		Serialize(CArchive& ar);
	int			GetType() { return CGraphicPathElement::GPTYPE_ARC; };
	static void	MittelSenkrechte(GP_LINE *ms, GP_VECTOR pt1, GP_VECTOR pt2);
	static void	Line(GP_LINE *g, GP_VECTOR pt1, GP_VECTOR pt2);
	static void	PointOnLine(GP_VECTOR pt, GP_LINE *g, double fT);
	static void Lot(GP_VECTOR l, GP_VECTOR v);
	BOOL		CoordIdentical(double fX1, double fY1, double fX2, double fY2);
	BOOL		CreateCircle(GP_VECTOR ptc, double& fRadius, GP_VECTOR pt1, GP_VECTOR pt2, GP_VECTOR pt3);
	BOOL		SchnittpGerGer(GP_VECTOR pts, GP_LINE *g1, GP_LINE *g2);
	void		Draw(CDC* pDC, COLORREF crColor);
	CString		MakePDFStream();
	CString		PDFShortArc(double fAlpha, double fBeta);
	CString		PDFCurveTo(double x_1, double y_1, double x_2, double y_2, double x_3, double y_3);
	CString		PDFCircle(double x, double y, double r);
};


class CGraphicPathCircle : public CGraphicPathElement
{
public:
    DECLARE_SERIAL( CGraphicPathCircle )
	CGraphicPathCircle();

//Attributes:
public:
	GP_POINT m_ptCenter;
	double	 m_fRadius;

//Operations:
public:
	void	  Copy(const CGraphicPathElement& gpElement);
	const	  CGraphicPathCircle& operator=(const CGraphicPathCircle& gpCircle);
	void	  Serialize(CArchive& ar);
	int		  GetType() { return CGraphicPathElement::GPTYPE_CIRCLE; };
	double&	  GetCircleRadius() { return m_fRadius;  };
	GP_POINT& GetCircleCenter() { return m_ptCenter; };
};


class CGraphicPathVertex : public CGraphicPathElement
{
public:
    DECLARE_SERIAL( CGraphicPathVertex )
	CGraphicPathVertex();

//Attributes:
public:
	GP_POINT m_ptCoord;
	double	 m_fIndent;

//Operations:
public:
	void	  Copy(const CGraphicPathElement& gpElement);
	const	  CGraphicPathVertex& operator=(const CGraphicPathVertex& gpVertex);
	void	  Serialize(CArchive& ar);
	int		  GetType() { return CGraphicPathElement::GPTYPE_VERTEX; };
	GP_POINT& GetVertexCoord () { return m_ptCoord; };
	double&	  GetVertexIndent() { return m_fIndent; };
	void	  SetVertexIndent(double fIndent) {m_fIndent = fIndent;};
};

class CGraphicPathSeqend : public CGraphicPathElement
{
public:
    DECLARE_SERIAL( CGraphicPathSeqend )
	CGraphicPathSeqend();

//Attributes:
public:

//Operations:
public:
	void	Copy(const CGraphicPathElement& gpElement);
	const	CGraphicPathSeqend& operator=(const CGraphicPathSeqend& gpSeqend);
	void	Serialize(CArchive& ar);
	int		GetType() { return CGraphicPathElement::GPTYPE_SEQEND; };
};



class CGraphicPath : public CGraphicItem
{
public:
    DECLARE_SERIAL( CGraphicPath )
	CGraphicPath();
	virtual ~CGraphicPath();

//Attributes:
public:
	CArray<CGraphicPathElement*, CGraphicPathElement*> m_elements;
	double											   m_fScaleX, m_fScaleY;

//Operations:
public:
	void		Copy(CGraphicItem* pgItem);
	void		Serialize(CArchive& ar);
	void		AppendCircle(double fCenterX, double fCenterY, double fRadius, int cColor);
	void		AppendVertex(double fStartX, double fStartY, int nColor);
	void		AppendSeqend();
	static BOOL	PosIdentical(double fX1,double fY1,double fX2,double fY2);
	void		MoveToOrigin(double fX0, double fY0);
	void		SetScale(double fScaleX, double fScaleY) { m_fScaleX = fScaleX; m_fScaleY = fScaleY; };
	void		Draw(CDC* pDC, double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis);
	void		CheckNegativeScale(double& fX, double& fY, double fWidth, double fHeight, int nAngle);
	CString		MakePDFStream(double fOutputMediaX, double fOutputMediaY, double fOutputMediaWidth, double fOutputMediaHeight, int nOutputMediaOrientation,
							  double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis);
	void		TransformToOutputMedia(double fCorrectionX, double fCorrectionY, double fMediaWidth, double fMediaHeight, int nRotation, double& fX, double& fY);

	static inline double PI()   { return 3.141592654f;};
	static inline double PI_2() { return 6.283185307f;};
	static inline double SQR(double x) { return ((x) * (x));};
};

typedef CGraphicPathElement* GPE_POINTER;

template <> void AFXAPI SerializeElements <GPE_POINTER> (CArchive& ar, GPE_POINTER* pGraphicPathElement, INT_PTR nCount);


#endif // !defined(AFX_GRAPHICPATH_H__AD440718_BBE4_4E22_B841_2BF1E7480888__INCLUDED_)
