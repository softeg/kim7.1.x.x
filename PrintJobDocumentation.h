#pragma once


class CPrintJobDocumentation
{
public:
	CPrintJobDocumentation(void);
	~CPrintJobDocumentation(void);

enum displayColIDs {ColFormatIndex, ColComponent, ColPages, ColNUp, ColFormat, ColQuantity, ColSheetCoverage, ColOverProduction};
typedef struct displayColumn
{
	int		nID;
	int		nWidth;
	TCHAR	szLabel[100];
	int		nDT_Attribute;
};

typedef CArray <struct displayColumn, struct displayColumn&> DISPLAY_COLUMN_LIST;

DISPLAY_COLUMN_LIST	m_displayColumns;


public:
	CSize			Initialize(CDC* pDC);
	void			GetPrinterCaps(CDC* pDC);
	CSize			GetPageSize(CDC* pDC = NULL);
	void			Print(HDC hDC, CPrintDialog* pPrintDlg = NULL);
	BOOL			PageToPrint(CPrintDialog* pPrintDlg, int nPageNum);
	int				PrintBookBlock(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage);
	int				PrintProductGroupInfoPages(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage);
	int				PrintLabelComponentPages(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage);
	int				PrintComponentPlanningPages(CDC* pDC, int nPageNum, CRect rcRealPage, CRect rcDstPage, int nComponentIndex);
	CPrintSheet*	PrintPrintSheetListPages(CDC* pDC, int nPageNum, CRect rcRealPage, CRect rcDstPage, CPrintSheet* pPrintSheet);
	int				PrintPaperListPages(CDC* pDC, int nPageNum, CRect rcRealPage, CRect rcDstPage);
	CLayout*		PrintLayoutPages(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage, CLayout* pNextLayout, int& nNextGroupIndex);
	CPrintSheet*	PrintSheetPages(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage, CPrintSheet* pNextPrintSheet, int& nNextComponentIndex);
	void			DrawPageHeader(CDC* pDC, CRect rcFrame);
	void			AddDisplayColumn(int nColID, int nWidth, CString strLabel, int nDT_Attribute = DT_LEFT);
	CRect			DrawDisplayColumnHeader(CDC* pDC, CRect rcFrame);
	CRect			DrawPrintSheetComponents(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, int& nNextComponentIndex);
	CRect			DrawLayoutFormatGroups(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, int& nNextGroupIndex);
	CRect			DrawFormatIndex(CDC* pDC, CRect rcFrame, int nIndex);

public:
	int 	m_nPrinterDpiX;
	int 	m_nPrinterDpiY;
	int 	m_nPrinterPixelsX;
	int 	m_nPrinterPixelsY;
	int 	m_nPrinterOffsetX;
	int 	m_nPrinterOffsetY;
	int 	m_nPrinterSizeX;
	int 	m_nPrinterSizeY;
	CSize	m_sizePrintPage;
	int		m_nPageOrientation;
	CRect	m_rcPrintMargins;
};
