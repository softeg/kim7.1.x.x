#pragma once

#include "PrecutThread.h"
#include "afxcmn.h"


// CDlgOptimizationMonitor-Dialogfeld

class CDlgOptimizationMonitor : public CDialog
{
	DECLARE_DYNAMIC(CDlgOptimizationMonitor)

public:
	CDlgOptimizationMonitor(CWnd* pParent = NULL, BOOL bRunModal = FALSE);   // Standardkonstruktor
	virtual ~CDlgOptimizationMonitor();

// Dialogfelddaten
	enum { IDD = IDD_OPTIMIZATION_MONITOR };


public:
	CString			m_strCurrentSheet;
	CString			m_strCurrentPaper;
	int				m_nProgress;
	BOOL			m_bEscapePressed;
	BOOL			m_bLocked;
	BOOL			m_bRunModal;

protected:
	CProgressCtrl	m_progressBar;
	CListCtrl		m_reportList;

public:
	void	OptimizeAll();
	void	sPrintOneOptimizeAll();
	void	KimOptimizeAll();
	void	OptimizeGroup  (CPrintGroup& rPrintGroup);
	BOOL	RunOptimization(CPrintGroup& rPrintGroup);
	void	UpdatePrintLayout(float fSheetLeft, float fSheetBottom);
	void	InitData();
	void	SetProgress(int nProgress);
	BOOL	AutoImposePrintComponents(CPrintComponentsGroup& rPrintComponents, CPrintingProfile& rPrintingProfile);
	void	CombineSimilarLayouts();
	void	ReportWindowOutput(CString strSubject, CString strMsg, BOOL bAppendMode = FALSE);


protected:
	DECLARE_MESSAGE_MAP()
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedOk();
};
