// PageListFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "MainFrm.h"
#include "PageListTopToolsView.h"
#include "PageListFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PageListView.h"
#include "PageSourceListView.h"
#include "PageListTableView.h"
#include "PDFReader.h"
#include "PageListDetailView.h"
#include "DlgJobColorDefinitions.h"
#include "DlgPageSourceProps.h"
#include "DlgPageTemplateProps.h"
#include "DlgNumeration.h"	
#include "DlgDirPicker.h"
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



IMPLEMENT_DYNAMIC(CPageListSplitter, CSplitterWnd)

BEGIN_MESSAGE_MAP(CPageListSplitter, CSplitterWnd)
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()

// CPageListSplitter message handlers
afx_msg LRESULT CPageListSplitter::OnNcHitTest(CPoint point)
{
//    return HTNOWHERE;
	return CSplitterWnd::OnNcHitTest(point);
}

void CPageListSplitter::OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect)
{
	//CSplitterWnd::OnDrawSplitter(pDC, nType, rect);
	//return;

	if (pDC == NULL)
	{
		RedrawWindow(rect, NULL, RDW_INVALIDATE|RDW_NOCHILDREN);
		return;
	}

	CRect rcRect = rect;
	switch (nType)
	{
	case splitBorder:
		rcRect.InflateRect(-1, -1);
		pDC->Draw3dRect(rcRect, LIGHTGRAY, LIGHTGRAY);
		return;

	case splitIntersection:
		break;

	case splitBox:
		rcRect.InflateRect(-1, -1);
		pDC->Draw3dRect(rcRect, LIGHTGRAY, DARKGRAY);
		rcRect.InflateRect(-1, -1);
		break;

	case splitBar:
		{
			CPen pen(PS_SOLID, 1, RGB(165,190,250));
			CBrush brush(RGB(193,211,251)); 
			pDC->SelectObject(&pen);
			pDC->SelectObject(&brush);
			pDC->Rectangle(rcRect);
		}
		break;

	default:
		ASSERT(FALSE);  // unknown splitter type
	}
}



/////////////////////////////////////////////////////////////////////////////
// CPageListFrame

IMPLEMENT_DYNCREATE(CPageListFrame, CMDIChildWnd)

CPageListFrame::CPageListFrame()
{
	m_bShowBitmap	  = TRUE;

	m_bShowPagesList		  = FALSE;
	m_bShowPageSource		  = TRUE;
	m_bShowPageDetail		  = FALSE;
	m_bShowPropsDlg			  = FALSE;
	m_bEnablePagesListButton  = TRUE;
	m_bEnablePageSourceButton = TRUE;
	m_bEnablePageDetailButton = TRUE;
	m_bEnablePropsDlgButton	  = FALSE;

	m_bLoadPsfileMenuActive			= TRUE;		// Menu-settings for initial PageSourceListView
	m_bAssigncolorsPsfileMenuActive = FALSE;	//	""
	m_bDeleteSelectionMenuActive	= FALSE;	//	""
	m_bPagesourcePropsMenuActive	= FALSE;	//	""
	m_bRipMessagesMenuActive		= FALSE;
	m_bShowLinkMenuActive			= FALSE;	//	""

	m_pDlgPageTemplateProps	  = new CDlgPageTemplateProps;
	m_pDlgFoldSheetNumeration = new CDlgNumeration;

	m_bZoomMode		  = FALSE;
	m_bZoomMenuActive = FALSE;
	m_bMainToolBarActive = TRUE;

	m_bStatusRestored = FALSE;
}

CPageListFrame::~CPageListFrame()  
{
	delete m_pDlgPageTemplateProps;
	delete m_pDlgFoldSheetNumeration;
}

BEGIN_MESSAGE_MAP(CPageListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CPageListFrame)
	ON_WM_INITMENUPOPUP()
	ON_WM_MENUSELECT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MDIACTIVATE()
	ON_COMMAND(ID_PRINTSHEET_VIEW, OnPrintsheetView)
	ON_COMMAND(ID_LOAD_PSFILE, OnLoadPsfile)
	ON_COMMAND(ID_BITMAPCHECK, OnBitmapcheck)
	ON_UPDATE_COMMAND_UI(ID_BITMAPCHECK, OnUpdateBitmapcheck)
	ON_COMMAND(ID_DELETE_SELECTION, OnDeleteSelection)
	ON_COMMAND(ID_ASSIGNCOLORS_PSFILE, OnAssigncolorsPsfile)
	ON_COMMAND(ID_PAGESOURCE_PROPS, OnPagesourceProps)
	ON_COMMAND(IDC_PAGESOURCE, OnPagesource)
	ON_UPDATE_COMMAND_UI(IDC_PAGESOURCE, OnUpdatePagesource)
	ON_COMMAND(IDC_PAGESLIST, OnPageslist)
	ON_UPDATE_COMMAND_UI(IDC_PAGESLIST, OnUpdatePageslist)
	ON_COMMAND(IDC_PAGEDETAIL, OnPagedetail)
	ON_UPDATE_COMMAND_UI(IDC_PAGEDETAIL, OnUpdatePagedetail)
	ON_COMMAND(ID_ZOOM, OnZoom)
	ON_UPDATE_COMMAND_UI(ID_ZOOM, OnUpdateZoom)
	ON_UPDATE_COMMAND_UI(IDC_PAGEPROPS, OnUpdatePageprops)
	ON_UPDATE_COMMAND_UI(ID_ASSIGNCOLORS_PSFILE, OnUpdateAssigncolorsPsfile)
	ON_UPDATE_COMMAND_UI(ID_DELETE_SELECTION, OnUpdateDeleteSelection)
	ON_UPDATE_COMMAND_UI(ID_LOAD_PSFILE, OnUpdateLoadPsfile)
	ON_UPDATE_COMMAND_UI(ID_PAGESOURCE_PROPS, OnUpdatePagesourceProps)
	ON_COMMAND(ID_VIEW_PAGELISTFRAME_BAR, OnViewPageListFrameBar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PAGELISTFRAME_BAR, OnUpdateViewPageListFrameBar)
	ON_COMMAND(ID_PAGINATION, OnPagination)
	ON_COMMAND(ID_RIPMESSAGES, OnRipmessages)
	ON_UPDATE_COMMAND_UI(ID_RIPMESSAGES, OnUpdateRipmessages)
	ON_COMMAND(ID_SHOW_LINK, OnShowLink)
	ON_UPDATE_COMMAND_UI(ID_SHOW_LINK, OnUpdateShowLink)
	ON_COMMAND(ID_BLOCK_PAGESOURCEHEADER, OnBlockPageSourceHeader)
	ON_UPDATE_COMMAND_UI(ID_SHOW_LINK, OnUpdateBlockPageSourceHeader)
	ON_COMMAND(ID_ZOOM_DECREASE, OnZoomDecrease)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_DECREASE, OnUpdateZoomDecrease)
	ON_COMMAND(ID_FILE_PAGE_SETUP, OnFilePageSetup)
	ON_COMMAND(ID_OPEN_PDF_DOC, OnOpenPdfDoc)
	ON_WM_DESTROY()
	ON_COMMAND(ID_REGISTER_PDF_DOC, OnRegisterPdfDoc)
	ON_COMMAND(IDC_PAGEPROPS, OnPageprops)
	ON_COMMAND(ID_REFRESH_PAGESOURCES, OnRefreshPagesources)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageListFrame message handlers

void CPageListFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd) 
{
	if ( ! bActivate) // If frame will be deactivated close an open DlgPageTemplateProps dialog
		if (m_pDlgPageTemplateProps->m_hWnd)
			OnPageprops();

	CMDIChildWnd::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);
	
	OnUpdateFrameTitle(TRUE);
}

void CPageListFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	// update our parent window first
	//GetMDIFrame()->OnUpdateFrameTitle(bAddToTitle);    // do nothing (done in CMainFrame)
	::SetWindowText(this->GetSafeHwnd(), _T(""));

	CDocument* pDoc = GetActiveDocument();
	if ( ! pDoc)
		return;

	if ( (theApp.m_nGUIStyle == CImpManApp::GUINewTemplate) || (theApp.m_nGUIStyle == CImpManApp::GUIPageList) )
		::SetWindowText(this->GetSafeHwnd(), pDoc->GetPathName());
	else
	if ( (theApp.m_nGUIStyle == CImpManApp::GUIOpenTemplate) || (theApp.m_nGUIStyle == CImpManApp::GUIOpenOutputList) )
		::SetWindowText(this->GetSafeHwnd(), pDoc->GetPathName());
	else
	{
		//CString title = pDoc->GetTitle();
		////title += _T(" - ");
		////CString string;
		////string.LoadString(IDS_PAGELIST_TITLE);
		////title += string;
		//::SetWindowText(this->GetSafeHwnd(), title);
	}
}


BOOL CPageListFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	if(CMDIChildWnd::PreCreateWindow(cs) == 0)
		return FALSE;
	cs.style&=~(LONG)FWS_ADDTOTITLE;	// Delete default title
	return TRUE;
}


BOOL CPageListFrame::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CMDIFrameWnd* pParentWnd, CCreateContext* pContext) 
{
	BOOL ret = CMDIChildWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, pContext);
	if (ret == 0)
		return FALSE;
	else 
		return TRUE;
}

BOOL CPageListFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* pContext) 
{
	if ( ! m_wndSplitter1.CreateStatic(this, 2, 1))
	{
		TRACE0("Failed to CreateStaticSplitter\n");
		return FALSE;
	}
	if ( ! m_wndSplitter1.CreateView(0, 0, RUNTIME_CLASS(CPageListTopToolsView), CSize(1000, 35), pContext))
	{
		TRACE0("Failed to create first pane\n");
		return FALSE;
	} 
	if ( ! m_wndSplitter.CreateStatic(&m_wndSplitter1, 1, 2, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter1.IdFromRowCol(1, 0)))
	{
		TRACE0("Failed to CreateStaticSplitter\n");
		return FALSE;
	}
	if ( ! m_wndSplitter.CreateView(0, 0, pContext->m_pNewViewClass, CSize(180, 500), pContext))
	{
		TRACE0("Failed to create first pane\n");
		return FALSE;
	}
	if ( ! m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CPageSourceListView), CSize(1000, 500), pContext))
	{
		TRACE0("Failed to create second pane\n");
		return FALSE;
	} 
	// activate the input view
	SetActiveView((CView*)m_wndSplitter.GetPane(0,0));

	return TRUE;
}

void CPageListFrame::OnSize(UINT nType, int cx, int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

void CPageListFrame::OnPrintsheetView() 
{
	CMainFrame frame;

	CDocument* pDoc = GetActiveDocument();
	ASSERT(pDoc != NULL);

	frame.CreateOrActivatePrintSheetFrame(theApp.m_pNewPrintSheetTemplate, pDoc);
}


#ifdef POSTSCRIPT
extern BOOL				  bLszPsRipIsRunning;
#endif
CPageSource*			  pPageSource;
CList <CString, CString&> m_PathNameList;

void CPageListFrame::OnLoadPsfile() 
{
	CMainFrame* pMainFrame = (CMainFrame*)GetParentFrame();
	if (pMainFrame)
	{
#ifdef POSTSCRIPT
		if (bLszPsRipIsRunning)
		{
			AfxMessageBox(IDS_ONLY_START_ONE_RIP, MB_ICONSTOP|MB_OK);
			return;
		}
#endif

		CString strFilter;
		strFilter.LoadString(IDS_LOAD_PSFILE_FILTER);
		CFileDialog dlg(TRUE, _T("PS"), NULL, 
						OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ALLOWMULTISELECT | OFN_ENABLESIZING,
						strFilter, this);

		CString strInitDir;
		if (CImpManDoc::GetDoc()->m_PageSourceList.IsEmpty())	// Take path from settings
			dlg.m_ofn.lpstrInitialDir = theApp.settings.m_szPSDir;
		else
		{														// Take path from last page source
			CPageSource& rPageSource  = CImpManDoc::GetDoc()->m_PageSourceList.GetTail();
			TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
			_tsplitpath((LPCTSTR)rPageSource.m_strDocumentFullpath, szDrive, szDir, szFname, szExt);
			strInitDir  = szDrive;
			strInitDir += szDir;
			dlg.m_ofn.lpstrInitialDir = (LPCTSTR)strInitDir;
		}

		if (dlg.DoModal() == IDCANCEL)
 			return;
	}
}


void CPageListFrame::OnUpdateLoadPsfile(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bLoadPsfileMenuActive);
}

void CPageListFrame::OnBitmapcheck() // (De-)Activates the PreView in the left splitter
{
	m_bShowBitmap = ! m_bShowBitmap;

	// Don't update PrintSheetTreeView there would be changed the current tree-selection
	theApp.UpdateView(RUNTIME_CLASS(CPageListView));
}

void CPageListFrame::OnUpdateBitmapcheck(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bShowBitmap);
}

void CPageListFrame::OnDeleteSelection() 
{
	CPageListTableView* pPageListTableView = (CPageListTableView*)theApp.GetView(RUNTIME_CLASS(CPageListTableView));
	if (pPageListTableView)
	{
		CDisplayItem* pDI = pPageListTableView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageListTableView, PDFDocument));
		if (pDI)
			DeletePDFDocument(pDI);
	}

	CPageSourceListView* pPageSourceListView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	if (pPageSourceListView)
	{
		CDisplayItem* pDI = pPageSourceListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSource));
		if (pDI)
		{
			BOOL		  bHasLinks = FALSE;
			CDisplayItem* pDITest	= pDI;
			while (pDITest)
			{
				CPageSource* pPageSource = (CPageSource*)pDITest->m_pData;
				if (pPageSource)
					if (pPageSource->HasLinks())
					{
						bHasLinks = TRUE;
						break;
					}
				pDITest = pPageSourceListView->m_DisplayList.GetNextSelectedItem(pDITest, DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSource));
			}

			if (bHasLinks)
			{
				int nRet = AfxMessageBox(IDS_DELETE_PAGESOURCE_OR_LINKS_PROMPT, MB_YESNOCANCEL|MB_DEFBUTTON2);
				switch (nRet)
				{
				case IDYES:		DeletePageSource(pDI);			break;
				case IDNO:		DeletePageSource(pDI, TRUE);	break;	// break links only
				case IDCANCEL:	break;
				}
			}
			else
				if (AfxMessageBox(IDS_DELETE_SELECTIONS_WARNING, MB_YESNO) == IDYES)
					DeletePageSource(pDI);			
		}
	}

	CPageListView* pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pPageListView)
	{
		CDisplayItem* pDI = pPageListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));
		if (pDI)
			DeleteAssignedPageSourcesFromPage(pDI);
	}
}

void CPageListFrame::OnUpdateDeleteSelection(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bDeleteSelectionMenuActive);
}

void CPageListFrame::DeletePDFDocument(CDisplayItem* pDI)
{
	CPageListTableView* pView = (CPageListTableView*)theApp.GetView(RUNTIME_CLASS(CPageListTableView));
	if (!pView)
		return;

	if (AfxMessageBox(IDS_DELETE_SELECTEDPAGESOURCES_WARNING, MB_YESNO) == IDNO)
		return;

	BOOL bDeleted = FALSE;
	while (pDI)
	{ 
		int			   nColorDefinitionIndex = (int)pDI->m_pData;
		CPageTemplate* pPageTemplate		 = (CPageTemplate*)pDI->m_pAuxData;
		if (pPageTemplate)
		{
			BOOL bRemoved = FALSE;
			for (int nSepIndex = 0; nSepIndex < pPageTemplate->m_ColorSeparations.GetSize(); nSepIndex++)
			{
				if (pPageTemplate->m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex == nColorDefinitionIndex)
				{
					CRect clientRect;
					pView->GetClientRect(clientRect);
					pView->InvalidateRect(CRect(clientRect.left, pDI->m_BoundingRect.top, clientRect.right, clientRect.bottom));

					pPageTemplate->m_ColorSeparations.RemoveAt(nSepIndex);

					if (pPageTemplate->m_nMultipageGroupNum >= 0)
					{
						CPageTemplate* p2ndPageTemplate = pPageTemplate->GetFirstMultipagePartner();
						if (pPageTemplate != p2ndPageTemplate)
							p2ndPageTemplate->m_ColorSeparations.RemoveAt(nSepIndex);
					}

					bDeleted = TRUE;
					bRemoved = TRUE;

					break;
				}
			}

			if (theApp.settings.m_bAutoCheckDoublePages)
				if (bRemoved && ! pPageTemplate->m_ColorSeparations.GetSize())
					if (pPageTemplate->m_nMultipageGroupNum >= 0)
						CPageListView::SetDoublePage(pPageTemplate, (pPageTemplate->GetMultipageSide() == LEFT) ? RIGHT : LEFT, FALSE);
		}
		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPageListTableView, PDFDocument));
	}

	if (bDeleted)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizeColorInfos();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->SetModifiedFlag();										

		theApp.OnUpdateView(RUNTIME_CLASS(CPageListView),		NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListDetailView), NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListTableView),	NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageSourceListView),	NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),		NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),	NULL, 0, NULL);
/*
		//pDoc->UpdateAllViews(NULL);

		pView->UpdateWindow();

		// TODO: later use UpdateAllViews() with hints - when done, remove PrintSheetView.h and InplaceEdit.h 
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
		{
			pView->Invalidate();
			pView->UpdateWindow();
		}
*/
	}
}

void CPageListFrame::DeletePageSource(CDisplayItem* pDI, BOOL bBreakLinksOnly)
{
	CPageSourceListView* pSourceView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	if (!pSourceView)
		return;

	BOOL bDeleted = FALSE;
	while (pDI)
	{
		short nPageSourceIndex = CImpManDoc::GetDoc()->m_PageSourceList.GetPageSourceIndex((CPageSource*)pDI->m_pData);
		if ((nPageSourceIndex >= 0) && (nPageSourceIndex < CImpManDoc::GetDoc()->m_PageSourceList.GetCount()))
		{
			POSITION pos = CImpManDoc::GetDoc()->m_PageSourceList.FindIndex(nPageSourceIndex);
			if (pos)
			{
				if ( ! bBreakLinksOnly)
					CImpManDoc::GetDoc()->m_PageSourceList.RemoveAt(pos);
				// TODO: remove corresponding *.pdf and *.bmp files
				CImpManDoc::GetDoc()->m_PageTemplateList.RemovePageSource(nPageSourceIndex, bBreakLinksOnly);
				CImpManDoc::GetDoc()->m_Bookblock.m_OutputDate.m_dt = 0;
				bDeleted = TRUE;
			}
		}

		pDI = pSourceView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSource));
	}
 
	if (bDeleted)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizeColorInfos();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->SetModifiedFlag();										

		theApp.OnUpdateView(RUNTIME_CLASS(CPageListView), NULL, 0, NULL);	
		theApp.UpdateView(RUNTIME_CLASS(CPageListView));
		theApp.UpdateView(RUNTIME_CLASS(CPageListDetailView));
		theApp.UpdateView(RUNTIME_CLASS(CPageListTableView));
		theApp.UpdateView(RUNTIME_CLASS(CPageSourceListView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTreeView));
	}
}

void CPageListFrame::DeleteAssignedPageSourcesFromPage(CDisplayItem* pDI)
{
	CPageListView* pListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (!pListView)
		return;

	CPageTemplate* pPageTemplate = (CPageTemplate*)pDI->m_pData;
	if (pPageTemplate)
	{
		if ((pPageTemplate->m_ColorSeparations.GetUpperBound()) == -1) // empty m_ColorSeparations-array ??
			return;
		else
			if (AfxMessageBox(IDS_DELETE_SELECTEDPAGESOURCES_WARNING, MB_YESNO) == IDNO)
				return;
	}

	BOOL bDeleted = FALSE;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	while (pDI)
	{
		pPageTemplate = (CPageTemplate*)pDI->m_pData;
		if (pPageTemplate)
		{
			pPageTemplate->m_ColorSeparations.RemoveAll();
			if (theApp.m_nGUIStyle != CImpManApp::GUIStandard)	// AutoServer command -> reset to original page number
				pPageTemplate->m_strPageID.Format(_T("%d"), pDoc->m_PageTemplateList.GetPageIndex(pPageTemplate) + 1);

			if (pPageTemplate->m_nMultipageGroupNum >= 0)
			{
				CPageTemplate* p2ndPageTemplate = pPageTemplate->GetFirstMultipagePartner();
				if (p2ndPageTemplate)
				{
					p2ndPageTemplate->m_ColorSeparations.RemoveAll();
					if (theApp.m_nGUIStyle != CImpManApp::GUIStandard)	// AutoServer command -> reset to original page number
						p2ndPageTemplate->m_strPageID.Format(_T("%d"), pDoc->m_PageTemplateList.GetPageIndex(p2ndPageTemplate) + 1);
				}

				if (theApp.settings.m_bAutoCheckDoublePages)
					CPageListView::SetDoublePage(pPageTemplate, (pPageTemplate->GetMultipageSide() == LEFT) ? RIGHT : LEFT, FALSE);
			}
		}

		pDI = pListView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));
		bDeleted = TRUE;
	}

	if (bDeleted)
	{
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizeColorInfos();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->SetModifiedFlag();										

		theApp.UpdateView(RUNTIME_CLASS(CPageListView));
		theApp.UpdateView(RUNTIME_CLASS(CPageListDetailView));
		theApp.UpdateView(RUNTIME_CLASS(CPageListTableView));
		theApp.UpdateView(RUNTIME_CLASS(CPageSourceListView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTreeView));
	}
}

void CPageListFrame::OnAssigncolorsPsfile() 
{
	CDlgJobColorDefinitions dlg;
	CPageSourceListView*	pView			  = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	CDisplayItem*			pDI				  = (pView) ? pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader)) : NULL;
	CPageSource*			pPageSource		  = (pDI) ? (CPageSource*)pDI->m_pAuxData	 : NULL;
	CPageSourceHeader*		pPageSourceHeader = (pDI) ? (CPageSourceHeader*)pDI->m_pData : NULL;
	if ( ! pPageSource || ! pPageSourceHeader)
		return;
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	for (int i = 0; i < pPageSource->m_PageSourceHeaders.GetSize(); i++)
		if (pPageSource->m_PageSourceHeaders[i].m_nPageNumber == pPageSourceHeader->m_nPageNumber)
		{
			CColorDefinition colorDef(pPageSource->m_PageSourceHeaders[i].m_strColorName, pPageSource->m_PageSourceHeaders[i].m_rgb, -1, 0);
			dlg.m_PSColorDefTable.InsertColorDef(colorDef);
		}

	if (dlg.DoModal() == IDCANCEL)
		return;

	int	nNumColor	 = dlg.m_PSColorDefTable.GetSize();
	int	nColIndex	 = 0;
	int	nPageNum	 = pPageSource->m_PageSourceHeaders.GetSize() + 1;	// Begin with number not yet present in list
																		// otherwise ReorganizePageNumbers() could fail
	while (pDI)
	{
		if (nColIndex == nNumColor)
		{
			nColIndex = 0;
			nPageNum++;
		}
	
		CColorDefinition&  rColorDef		 = dlg.m_PSColorDefTable[nColIndex++];
		CPageSourceHeader* pPageSourceHeader = (CPageSourceHeader*)pDI->m_pData;

		if (pPageSourceHeader)
		{
			short nOldColorDefinitionIndex = pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(pPageSourceHeader->m_strColorName);
	//		if (!pPageSourceHeader->m_bPageNumberFix)
				pPageSourceHeader->m_nPageNumber = nPageNum;
			pPageSourceHeader->m_strColorName	 = rColorDef.m_strColorName;
			pPageSourceHeader->m_rgb			 = rColorDef.m_rgb;
			pPageSourceHeader->m_nColorIndex	 = rColorDef.m_nColorIndex;
			short nNewColorDefinitionIndex = CImpManDoc::GetDoc()->m_ColorDefinitionTable.InsertColorDef(rColorDef);

			CPageTemplate* pPageTemplate = pPageSourceHeader->GetFirstPageTemplate();
			while (pPageTemplate)
			{
				SEPARATIONINFO* pSepInfo = pPageTemplate->GetSeparationInfo(nOldColorDefinitionIndex);
				if (pSepInfo)
					pSepInfo->m_nColorDefinitionIndex = nNewColorDefinitionIndex;

				pPageTemplate = pPageSourceHeader->GetNextPageTemplate(pPageTemplate);
			}
		}

		pDI = (pView) ? pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader)) : NULL;
	}

	if (pPageSource)
		pPageSource->ReorganizePageNumbers();

	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();
	pDoc->SetModifiedFlag();										
	pView->Invalidate();
	pView->UpdateWindow();
	theApp.OnUpdateView(RUNTIME_CLASS(CPageListView),		NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),		NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),	NULL, 0, NULL);
}

void CPageListFrame::OnUpdateAssigncolorsPsfile(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bAssigncolorsPsfileMenuActive);
}

void CPageListFrame::OnPagesourceProps() 
{
	CPageSourceListView* pPageSourceListView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	if (pPageSourceListView)
	{
		CDisplayItem* pDI = pPageSourceListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader));
		if (pDI)
		{
			CDlgPageSourceProps dlg;
			dlg.DoModal();
		}
	}
	else
	{
		CPageListDetailView* pPageListDetailView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
		if (pPageListDetailView)
		{
			if (pPageListDetailView->GetPageTemplate() && pPageListDetailView->GetLayoutObject())
			{
				CDlgPageTemplateProps dlg;
				dlg.m_pPageTemplate = pPageListDetailView->GetPageTemplate();
				dlg.m_pLayoutObject	= pPageListDetailView->GetLayoutObject();
				dlg.DoModal();
			}
		}
	}
}
void CPageListFrame::OnUpdatePagesourceProps(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bPagesourcePropsMenuActive);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// Main-ToolBar-Buttons (also menu-entries)
void CPageListFrame::OnPageslist() 
{
	CView* pView;
	pView = (CView*)m_wndSplitter.GetPane(0,1);
	if (pView->IsKindOf(RUNTIME_CLASS(CPageListTableView)))
		return;

	m_bShowPageSource = FALSE;
	m_bShowPagesList  = TRUE;
	m_bShowPageDetail = FALSE;

	m_bEnablePropsDlgButton = FALSE;

	m_bZoomMode		  = FALSE;
	m_bZoomMenuActive = FALSE;

	m_bLoadPsfileMenuActive			= FALSE;
	m_bAssigncolorsPsfileMenuActive = FALSE;
	m_bDeleteSelectionMenuActive	= FALSE;	// Will set to TRUE if object will be selected
	m_bPagesourcePropsMenuActive	= FALSE;
	m_bRipMessagesMenuActive		= FALSE;
	m_bShowLinkMenuActive			= FALSE;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CPageListTableView);
	context.m_pCurrentDoc	  = pView->GetDocument();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter.DeleteView(0, 1);
	m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CPageListTableView), CSize(0, 0), &context);

	CPageListTableView* pNewView = (CPageListTableView*)m_wndSplitter.GetPane(0, 1);
	SetActiveView(pNewView);
	m_wndSplitter.RecalcLayout();
	pNewView->m_DisplayList.Invalidate();
	((CView*)pNewView)->OnInitialUpdate();

	theApp.UpdateView(RUNTIME_CLASS(CPageListView));
}
void CPageListFrame::OnUpdatePageslist(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnablePagesListButton);
	pCmdUI->SetCheck(m_bShowPagesList);
}

void CPageListFrame::OnPagesource() 
{
	CView* pView;
	pView = (CView*)m_wndSplitter.GetPane(0,1);
	if (pView->IsKindOf(RUNTIME_CLASS(CPageSourceListView)))
		return;

	m_bShowPageSource = TRUE;
	m_bShowPagesList  = FALSE;
	m_bShowPageDetail = FALSE;

	m_bEnablePropsDlgButton = FALSE;

	m_bZoomMode		  = FALSE;
	m_bZoomMenuActive = FALSE;

	m_bLoadPsfileMenuActive			= TRUE;
	m_bAssigncolorsPsfileMenuActive = FALSE;
	m_bDeleteSelectionMenuActive	= FALSE;
	m_bPagesourcePropsMenuActive	= FALSE;
	m_bRipMessagesMenuActive		= FALSE;
	m_bShowLinkMenuActive			= FALSE;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CPageSourceListView);
	context.m_pCurrentDoc	  = pView->GetDocument();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter.DeleteView(0, 1);
	m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CPageSourceListView), CSize(0, 0), &context);

	CPageSourceListView* pNewView = (CPageSourceListView*)m_wndSplitter.GetPane(0, 1);
	SetActiveView(pNewView);
	m_wndSplitter.RecalcLayout();
	pNewView->m_DisplayList.Invalidate();
	((CView*)pNewView)->OnInitialUpdate();
	theApp.UpdateView(RUNTIME_CLASS(CPageListView));
}
void CPageListFrame::OnUpdatePagesource(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnablePageSourceButton);
	pCmdUI->SetCheck(m_bShowPageSource);
}

void CPageListFrame::OnPagedetail() 
{
	CView* pView;
	pView = (CView*)m_wndSplitter.GetPane(0,1);
	if (pView->IsKindOf(RUNTIME_CLASS(CPageListDetailView)))
		return;

	m_bShowPageSource = FALSE;
	m_bShowPagesList  = FALSE;
	m_bShowPageDetail = TRUE;

	m_bEnablePropsDlgButton = TRUE;

	m_bZoomMode		  = FALSE;
	m_bZoomMenuActive = TRUE;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CPageListDetailView);
	context.m_pCurrentDoc	  = pView->GetDocument();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter.DeleteView(0, 1);
	m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CPageListDetailView), CSize(0, 0), &context);

	CPageListDetailView* pNewView = (CPageListDetailView*)m_wndSplitter.GetPane(0, 1);
	SetActiveView(pNewView);
	m_wndSplitter.RecalcLayout();
	pNewView->m_DisplayList.Invalidate();
	pNewView->OnInitialUpdate();

///////////////////////////////////////////////////////////////////////////////////////////////
//  if PageListDetailView is created with a selected page in PageListView
	CPageListView* pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pPageListView)
	{
		CDisplayItem* pDI = pPageListView->m_DisplayList.GetFirstSelectedItem();
		if ( ! pDI)
		{
			pPageListView->m_DisplayList.SelectItem(*pPageListView->m_DisplayList.GetFirstItem());
			theApp.OnUpdateView(RUNTIME_CLASS(CPageListView), NULL, 0, NULL);
		}
		//if (pDI)
		//{
		//	CPageListDetailView* pPageListDetailView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
		//	if (pPageListDetailView)
		//	{
		//		pPageListDetailView->m_pPageTemplate = (CPageTemplate*)pDI->m_pData;
		//		if (pPageListDetailView->m_pPageTemplate)
		//			pPageListDetailView->m_pLayoutObject = pPageListDetailView->m_pPageTemplate->GetLayoutObject();
		//	}
		//}
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListDetailView), NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListView), NULL, 0, NULL);
//		CImpManDoc::GetDoc()->UpdateAllViews(pPageListView);
	}
///////////////////////////////////////////////////////////////////////////////////////////////
}
void CPageListFrame::OnUpdatePagedetail(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnablePageDetailButton);
	pCmdUI->SetCheck(m_bShowPageDetail);

	if ( ! m_bShowPageDetail)
		m_bShowPropsDlg = FALSE;
}

void CPageListFrame::OnPageprops() 
{
///////////////////////////////////////////////////////////////////////////////////////////////
//  if no page is selected in PageListView
	CPageListView* pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pPageListView)
	{
		CDisplayItem* pDI = pPageListView->m_DisplayList.GetFirstSelectedItem();
		if ( ! pDI)
			return;
	}
///////////////////////////////////////////////////////////////////////////////////////////////
	m_bShowPropsDlg = !m_bShowPropsDlg;

	if (m_bShowPropsDlg)
	{
		m_bEnablePagesListButton  = FALSE;
		m_bEnablePageSourceButton = FALSE;
		m_bEnablePageDetailButton = FALSE;
	}
	else
	{
		m_bEnablePagesListButton  = TRUE;
		m_bEnablePageSourceButton = TRUE;
		m_bEnablePageDetailButton = TRUE;
	}

	if ( ! m_pDlgPageTemplateProps->m_hWnd && m_bShowPropsDlg)
	{
		CPageListDetailView* pPageListDetailView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
		if (pPageListDetailView)
		{
			if (pPageListDetailView->GetPageTemplate())
			{
				m_pDlgPageTemplateProps->m_pPageTemplate = pPageListDetailView->GetPageTemplate();
				m_pDlgPageTemplateProps->m_pLayoutObject = pPageListDetailView->GetLayoutObject();

				m_pDlgPageTemplateProps->Create(IDD_PAGE_TEMPLATE_PROPS);
			}
		}
	}
	if (m_pDlgPageTemplateProps->m_hWnd && ! m_bShowPropsDlg)
		m_pDlgPageTemplateProps->DestroyWindow();

}
void CPageListFrame::OnUpdatePageprops(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnablePropsDlgButton);
	pCmdUI->SetCheck(m_bShowPropsDlg);
}

#ifdef POSTSCRIPT
extern BOOL bLszPsRipIsRunning;
#endif

void CPageListFrame::OnRipmessages() 
{
#ifdef POSTSCRIPT
	if (bLszPsRipIsRunning)
		return;

	CPageSourceListView* pPageSourceListView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	if (pPageSourceListView)
	{
		CDisplayItem* pDI = pPageSourceListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSource));
		if (pDI)
		{
			CMainFrame* pMainFrame	 = (CMainFrame*)theApp.m_pMainWnd;
			CPageSource* pPageSource = (CPageSource*)pDI->m_pData;
			pMainFrame->m_pRipOutDlg->m_strEPSFilesPath = pPageSource->m_strContentFilesPath;
			pMainFrame->m_pRipOutDlg->m_strWindowTitle  = pPageSource->m_strContentFilesPath;
			if (!pMainFrame->m_pRipOutDlg->m_hWnd)
				pMainFrame->m_pRipOutDlg->Create(IDD_RIP_OUT);
			pMainFrame->m_pRipOutDlg->UpdateStatus(CDlgRipOut::ShowMessagesOnly, 0, 0, FALSE);
		}
	}
#endif
}

void CPageListFrame::OnUpdateRipmessages(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bRipMessagesMenuActive);
}

void CPageListFrame::OnZoom() 
{
	m_bZoomMode = !m_bZoomMode;
}
void CPageListFrame::OnUpdateZoom(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bZoomMode);
	pCmdUI->Enable(m_bZoomMenuActive);
}

void CPageListFrame::OnZoomDecrease() 
{
	CPageListDetailView* pView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
	if ( ! pView)
		return;

	SetCursor(theApp.m_CursorMagnifyDecrease);
	pView->ZoomWindow(CPoint(0,0));
}

void CPageListFrame::OnUpdateZoomDecrease(CCmdUI* pCmdUI) 
{
	CPageListDetailView* pView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
	if ( ! pView)
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

void CPageListFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
  	CMDIChildWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	// CG: The following block was inserted by 'Status Bar' component.
	{
		GetParentFrame()->PostMessage(WM_INITMENUPOPUP,
			(WPARAM)pPopupMenu->GetSafeHmenu(), MAKELONG(nIndex, bSysMenu));
	}
}

void CPageListFrame::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu)
{
  	CMDIChildWnd::OnMenuSelect(nItemID, nFlags, hSysMenu);
	// CG: The following block was inserted by 'Status Bar' component.
	{
		static BOOL bMenuActive = FALSE;
		if (bMenuActive || hSysMenu != NULL)
		{
			GetParentFrame()->PostMessage(WM_MENUSELECT, 
				MAKELONG(nItemID, nFlags), (LPARAM)hSysMenu);
		}
		bMenuActive = hSysMenu != NULL;
	}
}

void CPageListFrame::OnViewPageListFrameBar() 
{
	if (m_bMainToolBarActive)
	{
		//ShowControlBar(&m_wndPageListFrameToolBar, FALSE, FALSE); 
		m_bMainToolBarActive = FALSE;
	}
	else
	{
		//ShowControlBar(&m_wndPageListFrameToolBar, TRUE, FALSE);
		m_bMainToolBarActive = TRUE;
	}
}
void CPageListFrame::OnUpdateViewPageListFrameBar(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bMainToolBarActive);
}

void CPageListFrame::OnPagination() 
{
	if (InitNumerationDialog(m_pDlgFoldSheetNumeration))
		if ( ! m_pDlgFoldSheetNumeration->m_hWnd)
			m_pDlgFoldSheetNumeration->Create(IDD_NUMERATION_DIALOG, this);
}

BOOL CPageListFrame::InitNumerationDialog(CDlgNumeration* pDlg)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	CDisplayItem*  pDI			 = NULL;
	CPageListView* pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pPageListView)
		pDI = pPageListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));

	pDlg->m_nProductPartIndex = 0;
	if (pPageListView)
	{
		CPageListFrame* pFrame = (CPageListFrame*)pPageListView->GetParentFrame();
		if (pFrame)
			pDlg->m_nProductPartIndex = pFrame->m_nProductPartIndex;
	}

	pDlg->m_strTitle.LoadString(IDS_PAGENUMERATION_TITLE);
	pDlg->m_pfnApplyCallback = ChangeNumeration;
	pDlg->m_pfnInitCallback  = InitNumerationDialog;
	pDlg->m_nSelection = (pDI) ? CDlgNumeration::SelectionMarked : CDlgNumeration::SelectionAll;
	if (pDI)
	{
		CPageTemplate* pFirstPageTemplate = (CPageTemplate*)pDI->m_pData;
		pDlg->m_nRangeFrom = (int)pDI->m_pAuxData + 1;

		pDI = pPageListView->m_DisplayList.GetLastSelectedItem(DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));
		CPageTemplate* pLastPageTemplate = (CPageTemplate*)pDI->m_pData;
		pDlg->m_nRangeTo   = (int)pDI->m_pAuxData + 1;

		pDlg->m_nStartNum  = (pDlg->m_bReverse) ? pLastPageTemplate->GetPageNum() : pFirstPageTemplate->GetPageNum();
	}
	else
	{
		pDlg->m_nRangeFrom = 1; 
		pDlg->m_nRangeTo   = pDoc->m_PageTemplateList.GetCount(pDlg->m_nProductPartIndex);

		CPageTemplate& rFirstPageTemplate = pDoc->m_PageTemplateList.GetHead(pDlg->m_nProductPartIndex);
		CPageTemplate& rLastPageTemplate  = pDoc->m_PageTemplateList.GetTail(pDlg->m_nProductPartIndex);
		pDlg->m_nStartNum  = (pDlg->m_bReverse) ? rLastPageTemplate.GetPageNum() : rFirstPageTemplate.GetPageNum();
	}


	pDlg->m_pViewToUpdate1 = pPageListView;

	if (pDlg->m_hWnd)
		pDlg->UpdateData(FALSE);

	return TRUE;
}

void CPageListFrame::ChangeNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nCurNum = (nStartNum - 1) * CDlgNumeration::GetSeqLen(strStyle) + 1;
	switch (nSelection)
	{
	case CDlgNumeration::SelectionAll:
	case CDlgNumeration::SelectionRange:	
		{
			int		 i   = 0;
			POSITION pos = pDoc->m_PageTemplateList.GetHeadPosition(nProductPartIndex);
			while (pos)
			{
				CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
				if ( ( nSelection == CDlgNumeration::SelectionAll) ||
					 ((nSelection == CDlgNumeration::SelectionRange) && (i + 1 >= nRangeFrom) && (i + 1 <= nRangeTo)) )
				{
					if (strStyle == _T("reset"))
						rTemplate.m_bPageNumLocked = FALSE;
					else
					{
						int nIndex = pDoc->m_PageTemplateList.GetIndex(&rTemplate, nProductPartIndex) + 1;
						rTemplate.m_strPageID	   = CDlgNumeration::GenerateNumber(nIndex, strStyle, nCurNum - nIndex);
						rTemplate.m_bPageNumLocked = TRUE;
						nCurNum += (bReverse) ? -1 : 1;
					}
				}
				i++;
			}
		}
		break;

	case CDlgNumeration::SelectionMarked:	
		{
			CPageListView* pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
			if ( ! pPageListView)
				return;
			CDisplayItem* pDI = pPageListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));
			while (pDI)
			{
				if ((int)(pDI->m_pAuxData) != -1)	// -1 means frame of multipage
				{
					CPageTemplate* pPageTemplate = (CPageTemplate*)pDI->m_pData;
					if (strStyle == _T("reset"))
						pPageTemplate->m_bPageNumLocked = FALSE;
					else
					{
						int nIndex = pDoc->m_PageTemplateList.GetIndex(pPageTemplate, nProductPartIndex) + 1;
						pPageTemplate->m_strPageID		= CDlgNumeration::GenerateNumber(nIndex, strStyle, nCurNum - nIndex);
						pPageTemplate->m_bPageNumLocked = TRUE;
						nCurNum += (bReverse) ? -1 : 1;
					}
				}
				pDI = pPageListView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));
			}
		}
		break;
	}

	if (strStyle == _T("reset"))
		pDoc->m_PageTemplateList.SortPages();

	pDoc->SetModifiedFlag(TRUE);

	theApp.OnUpdateView(RUNTIME_CLASS(CPageListView),			NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPageListTableView),		NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),			NULL, 0, NULL);
}

//*********************************************************************************

void CPageListFrame::OnShowLink() 
{
	CPageSourceListView* pPageSourceListView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	CPageListView*		 pPageListView	     = (CPageListView*)      theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (!pPageSourceListView && !pPageListView)
		return;

	CDisplayItem* pDIScrollTo = NULL;
	CDisplayItem* pDI		  = (pPageSourceListView) ? pPageSourceListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader)) : NULL;
	while (pDI)
	{
		CPageListView* pView	 = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
		CPageTemplate* pTemplate = ((CPageSourceHeader*)pDI->m_pData)->GetFirstPageTemplate();
		while (pTemplate && pView)
		{
			CDisplayItem* pDITemplate = pView->m_DisplayList.GetItemFromData(pTemplate);
			if (pDITemplate)
			{
				pView->m_DisplayList.SelectItem(*pDITemplate);
				pView->m_DisplayList.InvalidateItem(pDITemplate);
				if (!pDIScrollTo)
					pDIScrollTo = pDITemplate;
			}
			pTemplate = ((CPageSourceHeader*)pDI->m_pData)->GetNextPageTemplate(pTemplate);
		}

		pDI = (pPageSourceListView) ? pPageSourceListView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader)) : NULL;
	}

	if (pDIScrollTo)
	{
		if ( ! pDIScrollTo->m_bIsVisible)	
		{
			pPageListView->Invalidate();
			CPoint ScrollPos = pPageListView->GetScrollPosition();
			pPageListView->ScrollToPosition(CPoint(0, ScrollPos.y + pDIScrollTo->m_BoundingRect.top - 20));
		}
	}
	
}

void CPageListFrame::OnUpdateShowLink(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bShowLinkMenuActive);
}

void CPageListFrame::OnBlockPageSourceHeader() 
{
	CPageListView*		 pPageListView		 = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	CPageSourceListView* pPageSourceListView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	if (pPageSourceListView)
	{
		CDisplayItem* pDI = pPageSourceListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader));
		while (pDI)
		{
			CPageSourceHeader* pPageSourceHeader = (CPageSourceHeader*)pDI->m_pData;
			pPageSourceHeader->m_bOutputBlocked = ! pPageSourceHeader->m_bOutputBlocked;
			CRect rcInvalidate = pDI->m_BoundingRect;
			rcInvalidate.InflateRect(4,4);
			pPageSourceListView->InvalidateRect(rcInvalidate);
			if (pPageListView)
			{
				CPageTemplate* pTemplate = pPageSourceHeader->GetFirstPageTemplate();
				while (pTemplate)
				{
					CDisplayItem* pDITemplate = pPageListView->m_DisplayList.GetItemFromData(pTemplate);
					if (pDITemplate)
					{
						rcInvalidate = pDITemplate->m_BoundingRect;
						rcInvalidate.InflateRect(4,4);
						pPageListView->InvalidateRect(rcInvalidate);
					}
					pTemplate = pPageSourceHeader->GetNextPageTemplate(pTemplate);
				}
			}
			pDI = pPageSourceListView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader));
		}
		
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
			pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
		}

		if (pPageSourceListView)
			pPageSourceListView->UpdateWindow();
		if (pPageListView)
			pPageListView->UpdateWindow();
	}
}

void CPageListFrame::OnUpdateBlockPageSourceHeader(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bPagesourcePropsMenuActive);
}

void CPageListFrame::OnRegisterPdfDoc() 
{
#ifdef PDF
	CString strInitialFolder;
	if (CImpManDoc::GetDoc()->m_PageSourceList.IsEmpty())	// Take path from settings
		strInitialFolder = theApp.settings.m_szPDFInputFolder;
	else
	{														// Take path from last page source
		CPageSource& rPageSource  = CImpManDoc::GetDoc()->m_PageSourceList.GetTail();
		TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
		_tsplitpath((LPCTSTR)rPageSource.m_strDocumentFullpath, szDrive, szDir, szFname, szExt);
		strInitialFolder  = szDrive;
		strInitialFolder += szDir;
		CFileStatus status;
		if ( ! CFile::GetStatus(strInitialFolder + _T("."), status))	// if folder not present, take from settings
			strInitialFolder = theApp.settings.m_szPDFInputFolder;
	}

	int			 nPageIndex;
	CPageSource* pPageSource = GetSelectedPageSource(nPageIndex);

	theApp.m_pDlgPDFLibStatus->LaunchPDFEngine(strInitialFolder, this, CDlgPDFLibStatus::RegisterPages, 
											   (pPageSource) ? pPageSource->m_strDocumentFullpath : "");
#endif
}

CPageSource* CPageListFrame::GetSelectedPageSource(int& nPageIndex)
{
	CPageSource*		pPageSource		  = NULL;
	CPageSourceHeader*  pPageSourceHeader = NULL;
	CPageTemplate*		pPageTemplate	  = NULL;
	CPageListView*		pPageListView	  = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));

	nPageIndex = -1;
	if (pPageListView)
	{
		CDisplayItem* pDI = pPageListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));
		pPageTemplate = (pDI) ? (CPageTemplate*)pDI->m_pData : NULL;
		if (pPageTemplate)
			if (pPageTemplate->m_ColorSeparations.GetSize())
			{
				pPageSource		  = pPageTemplate->GetObjectSource		(pPageTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				pPageSourceHeader = pPageTemplate->GetObjectSourceHeader(pPageTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				nPageIndex		  = pPageSource->GetHeaderIndex(pPageSourceHeader);
			}
	}
	if ( ! pPageSource)
	{
		CPageSourceListView* pPageSourceListView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
		if (pPageSourceListView)
		{
			CDisplayItem* pDI = (pPageSourceListView) ? pPageSourceListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader)) : NULL;
			pPageSource		  = (pDI) ? (CPageSource*)pDI->m_pAuxData    : NULL;
			pPageSourceHeader = (pDI) ? (CPageSourceHeader*)pDI->m_pData : NULL;
			nPageIndex		  = (pPageSource) ? pPageSource->GetHeaderIndex(pPageSourceHeader) : -1;
		}
	}

	return pPageSource;
}

void CPageListFrame::OnOpenPdfDoc() 
{
	int			 nPageIndex;
	CPageSource* pPageSource = GetSelectedPageSource(nPageIndex);
	if ( ! pPageSource)
		return;

	BeginWaitCursor();     

	LaunchAcrobat(pPageSource->m_strDocumentFullpath, nPageIndex);

	EndWaitCursor();     
}

BOOL CPageListFrame::LaunchAcrobat(const CString& strPDFFile, int nPageIndex)
{
	BOOL bReader = FALSE;
	HKEY hKey;
	CString strKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\Acrobat.exe");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) != ERROR_SUCCESS)
	{
		strKey = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\AcroRd32.exe");
		if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) != ERROR_SUCCESS)
		{
			AfxMessageBox(_T("Cannot launch acrobat!"), MB_OK);
			return FALSE;
		}
		bReader = TRUE;
	}

	TCHAR szPath[512];
	DWORD dwSize = sizeof(szPath);
	RegQueryValueEx(hKey, _T("Path"), 0, NULL, (LPBYTE)szPath, &dwSize);
	RegCloseKey(hKey);

	CString strPath;
	strPath.Format(_T("%s\\%s"), szPath, (bReader) ? _T("AcroRd32.exe") : _T("Acrobat.exe"));

	CString strArg; strArg.Format(_T("page=%d=OpenActions"), (nPageIndex == -1) ? 1 : (nPageIndex + 1) );

	int nRet = _spawnl(_P_NOWAIT, strPath, (bReader) ? _T("AcroRd32.exe") : _T("Acrobat.exe"), "/A", strArg, strPDFFile, NULL);	

	return (nRet == 0) ? TRUE : FALSE;
}


void CPageListFrame::OnFilePageSetup() 
{
	PRINTDLG printDlg;
	theApp.GetPrinterDeviceDefaults(&printDlg);

	CPageSetupDialog dlg;
	dlg.m_psd.lStructSize		= sizeof(PAGESETUPDLG);
	dlg.m_psd.hwndOwner			= m_hWnd;
	dlg.m_psd.hDevNames			= printDlg.hDevNames;
	dlg.m_psd.hDevMode			= printDlg.hDevMode;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	TCHAR szMargins[100];
	DWORD dwSize = sizeof(szMargins);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szMargins, &dwSize) == ERROR_SUCCESS)
		_stscanf(szMargins, "%ld %ld %ld %ld", &dlg.m_psd.rtMargin.left, &dlg.m_psd.rtMargin.top, &dlg.m_psd.rtMargin.right, &dlg.m_psd.rtMargin.bottom);
	else
		dlg.m_psd.rtMargin = CRect(2500, 2500, 2500, 2500);

	dlg.DoModal();

	theApp.SelectPrinter(dlg.m_psd.hDevNames, dlg.m_psd.hDevMode);

	_stprintf(szMargins, "%ld %ld %ld %ld", dlg.m_psd.rtMargin.left, dlg.m_psd.rtMargin.top, dlg.m_psd.rtMargin.right, dlg.m_psd.rtMargin.bottom);
	RegSetValueEx(hKey, _T("PrintMargins"), 0, REG_SZ, (LPBYTE)szMargins, (_tcslen(szMargins) + 1) * sizeof(TCHAR));
}

void CPageListFrame::ActivateFrame(int nCmdShow) 
{
	CMainFrame::RestoreFrameStatus(this, m_bStatusRestored, nCmdShow, &m_wndSplitter, &m_wndSplitter1);
	int nCur, nMin;
	m_wndSplitter1.GetRowInfo(0, nCur, nMin);
	if ( (nCur < 0) || (nCur > 35) )
	{
		m_wndSplitter1.SetRowInfo(0, 35, 5);
	}
	CMDIChildWnd::ActivateFrame(nCmdShow);
}

void CPageListFrame::OnDestroy() 
{
	CMDIChildWnd::OnDestroy();
	CMainFrame::SaveFrameStatus(this, &m_wndSplitter, &m_wndSplitter1);
}

void CPageListFrame::OnRefreshPagesources() 
{
#ifdef PDF
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_PageSourceList.CheckPDFsModified();
		pDoc->m_PageSourceList.CheckDocuments();
	}
#endif
}

CPageListTopToolsView* CPageListFrame::GetPageListTopToolsView()
{
	if ( ! m_wndSplitter1.m_hWnd)
		return NULL;

	CPageListTopToolsView* pView = (CPageListTopToolsView*)m_wndSplitter1.GetPane(0, 0);
	if (pView->IsKindOf(RUNTIME_CLASS(CPageListTopToolsView)))
		return pView;
	else
		return NULL;
}

int CPageListFrame::GetActProductPartIndex()
{
	CPageListTopToolsView* pView = GetPageListTopToolsView();
	if ( ! pView)
		return ALL_PARTS;
	if ( ! pView->m_productCombo.m_hWnd)
		return ALL_PARTS;

	int nSel = pView->m_productCombo.GetCurSel();
	if (nSel == CB_ERR)
		return ALL_PARTS;

	return pView->m_productCombo.GetItemData(nSel);
}