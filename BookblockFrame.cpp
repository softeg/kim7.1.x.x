// BookblockFrame.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "MainFrm.h"
#include "ModalFrame.h"
#include "BookBlockView.h"
#include "BookBlockBottomView.h"
#include "BookBlockDataView.h"
#include "BookBlockToolsView.h"
#include "BookBlockProdDataView.h"
#include "DummyView.h"
#include "BookblockFrame.h"



IMPLEMENT_DYNAMIC(CBookblockSplitter, CSplitterWnd)

BEGIN_MESSAGE_MAP(CBookblockSplitter, CSplitterWnd)
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()

// CProdDataSplitter message handlers
afx_msg LRESULT CBookblockSplitter::OnNcHitTest(CPoint point)
{
	return CSplitterWnd::OnNcHitTest(point);

	//CBookblockFrame* pFrame = (CBookblockFrame*)GetParentFrame();
	//if (pFrame)
	//{
	//	if (this == &pFrame->m_wndSplitter2)
	//	{
	//		CWnd* pPane = pFrame->m_wndSplitter2.GetPane(0,0);
	//		CRect rcWindow1, rcWindow2;
	//		pPane->GetWindowRect(rcWindow1);
	//		pPane = pFrame->m_wndSplitter2.GetPane(1,0);
	//		pPane->GetWindowRect(rcWindow2);
	//		if ( (rcWindow1.bottom <= point.y) && (point.y <= rcWindow2.top) )
	//			return CSplitterWnd::OnNcHitTest(point);
	//	}
	//}

	//return HTNOWHERE;
}

void CBookblockSplitter::OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect)
{
	if (pDC == NULL)
	{
		RedrawWindow(rect, NULL, RDW_INVALIDATE|RDW_NOCHILDREN);
		return;
	}

	CBookblockFrame* pFrame = (CBookblockFrame*)GetParentFrame();
	CRect rcRect = rect;
	switch (nType)
	{
	case splitBorder:
		pDC->Draw3dRect(rcRect, MIDDLEGRAY, LIGHTGRAY);
		rcRect.InflateRect(-1, -1);
		if (this == &pFrame->m_wndSplitter2)
			pDC->Draw3dRect(rcRect, RGB(127,162,207), RGB(127,162,207));  // background color defined in Create() of CBookBlockView
		else
			pDC->Draw3dRect(rcRect, RGB(225,230,235), RGB(225,230,235));
		return;

	case splitIntersection:
		break;

	case splitBox:
		pDC->Draw3dRect(rcRect, LIGHTGRAY, WHITE);
		rcRect.InflateRect(-1, -1);
		pDC->Draw3dRect(rcRect, LIGHTGRAY, MIDDLEGRAY);
		rcRect.InflateRect(-1, -1);
		break;

	case splitBar:
		{
			CPen pen(PS_SOLID, 1, RGB(165,190,250));
			CBrush brush(RGB(193,211,251)); 
			pDC->SelectObject(&pen);
			pDC->SelectObject(&brush);
			pDC->Rectangle(rcRect);
		}
		break;

	default:
		ASSERT(FALSE);  // unknown splitter type
	}
}




// CBookblockFrame

IMPLEMENT_DYNCREATE(CBookblockFrame, CMDIChildWnd)

CBookblockFrame::CBookblockFrame()
{
	m_nModalResult		= -1;
	m_bZoomMode			= FALSE;
	m_bHandActive		= FALSE;
	m_bStatusRestored	= FALSE;
}

CBookblockFrame::~CBookblockFrame()
{
}


BEGIN_MESSAGE_MAP(CBookblockFrame, CMDIChildWnd)
	ON_COMMAND(ID_ZOOM, OnZoomIncrease)
	ON_COMMAND(ID_ZOOM_DECREASE, OnZoomDecrease)
	ON_COMMAND(ID_ZOOM_FULLVIEW, OnZoomToFit)
	ON_COMMAND(ID_SCROLLHAND, OnScrollhand)
	ON_COMMAND(ID_DELETE_SELECTION, OnDeleteSelection)
	ON_COMMAND(ID_BOOKBLOCK_PROOF, OnBookblockProof)
	ON_COMMAND(ID_BOOKBLOCK_PROOF_OPENFILE, OnBookblockProofOpenfile)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CBookblockFrame-Meldungshandler


BOOL CBookblockFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* pContext)
{
	// create a splitter with 1 row, 3 columns
	if ( ! m_wndSplitter.CreateStatic(this, 1, 3))
		return FALSE;
	if ( ! m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CBookBlockDataView), CSize(100, 0), pContext))
		return FALSE;
	if ( ! m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CBookBlockToolsView), CSize(50, 0), pContext))
		return FALSE;

	if ( ! m_wndSplitter2.CreateStatic(&m_wndSplitter, 2, 1, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter.IdFromRowCol(0, 2)))
		return FALSE;
	if ( ! m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CBookBlockView), CSize(0, 0), pContext))
		return FALSE;
	if ( ! m_wndSplitter2.CreateView(1, 0, RUNTIME_CLASS(CBookBlockProdDataView), CSize(0, 0), pContext))
		return FALSE;

	SetMenu(NULL);

	return TRUE;
}

void CBookblockFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	CMDIChildWnd::OnUpdateFrameTitle(bAddToTitle);

	CDocument* pDoc = GetActiveDocument();
	if (pDoc)
	{
		CString title = pDoc->GetTitle();
		title += _T(" - ");
		CString string;
		string.LoadString(IDS_BOOKBLOCK);
		title += string;
		::SetWindowText(this->GetSafeHwnd(), title);
	}
}

int	CBookblockFrame::GetActProductGroup()
{
	CBookBlockDataView* pView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
	if (pView)
		return pView->m_dlgShinglingAssistent.m_nGlobalProductPartIndex;
	else
		return 0;
}

void CBookblockFrame::OnSize(UINT nType, int cx, int cy) 
{
	CMDIChildWnd::OnSize(nType, cx, cy);

	if (m_wndSplitter2.m_hWnd)
	{
		RecalcLayout();
		CRect rcFrame;
		GetClientRect(rcFrame);
		int nWidth0 = ((CScrollView*)m_wndSplitter.GetPane(0, 0))->GetTotalSize().cx;
		int nWidth1 = 44;//((CScrollView*)m_wndSplitter.GetPane(0, 1))->GetTotalSize().cx;
		int nWidth2 = rcFrame.Width() - nWidth1 - nWidth0;
		if ( ! m_bStatusRestored)
		{
			m_wndSplitter.SetColumnInfo	(0, (nWidth0 >= 0) ? nWidth0 : 0, 10);
			m_wndSplitter.SetColumnInfo	(1, (nWidth1 >= 0) ? nWidth1 : 0, 10);
			m_wndSplitter.SetColumnInfo	(2, (nWidth2 >= 0) ? nWidth2 : 0, 10);

			int nHeight0 = rcFrame.Height()/3;
			int nHeight1 = rcFrame.Height() - nHeight0;
			m_wndSplitter2.SetRowInfo	(0, (nHeight0 >= 0) ? nHeight0 : 0, 10);
			m_wndSplitter2.SetRowInfo	(1, (nHeight1 >= 0) ? nHeight1 : 0, 10);
		}
		else
		{
			m_wndSplitter.SetColumnInfo	(0, (nWidth0 >= 0) ? nWidth0 : 0, 10);
			m_wndSplitter.SetColumnInfo	(1, (nWidth1 >= 0) ? nWidth1 : 0, 10);
			m_wndSplitter.SetColumnInfo	(2, (nWidth2 >= 0) ? nWidth2 : 0, 10);
		}

		m_wndSplitter.RecalcLayout();
		m_wndSplitter2.RecalcLayout();
	}
}

void CBookblockFrame::ActivateFrame(int nCmdShow) 
{
	CMainFrame::RestoreFrameStatus(this, m_bStatusRestored, nCmdShow, &m_wndSplitter, &m_wndSplitter2);//, &m_wndSplitter3);
	OnSize(0,0,0);
	CMDIChildWnd::ActivateFrame(nCmdShow);
}

void CBookblockFrame::OnDestroy() 
{
	CMainFrame::SaveFrameStatus(this, &m_wndSplitter, &m_wndSplitter2);//, &m_wndSplitter3);		// RestoreFrameStatus() in InitInstance()
	CMDIChildWnd::OnDestroy();
}

void CBookblockFrame::OnZoomIncrease() 
{
	CBookBlockView* pBookBlockView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));
	if (pBookBlockView)
		pBookBlockView->ZoomIncrease(); 
}

void CBookblockFrame::OnZoomDecrease() 
{
	CBookBlockView* pBookBlockView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));
	if (pBookBlockView)
		pBookBlockView->ZoomDecrease(); 
}

void CBookblockFrame::OnZoomToFit() 
{
	CBookBlockView* pBookBlockView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));
	if (pBookBlockView)
		pBookBlockView->ZoomToFit(); 
}

void CBookblockFrame::OnScrollhand() 
{
	m_bHandActive = ! m_bHandActive;
	if (m_bHandActive)
		m_bZoomMode	= FALSE;
}

void CBookblockFrame::OnClose()
{
	CMDIChildWnd::OnClose();
}

void CBookblockFrame::OnDeleteSelection() 
{
	CBookBlockView* pBookBlockView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));
	if (pBookBlockView)
		pBookBlockView->OnDeleteSelectedFoldsheet();
}

void CBookblockFrame::OnBookblockProof()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strFirstSelectedFoldsheet;
	CBookBlockDataView* pView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
	if (pView)
	{
		POSITION pos = pView->m_foldSheetList.GetFirstSelectedItemPosition();
		while (pos)
		{
			int nItem = pView->m_foldSheetList.GetNextSelectedItem(pos);
			if (nItem < 0)
				continue;
			CFoldSheet* pFoldSheet = (CFoldSheet*)pView->m_foldSheetList.GetItemData(nItem);
			if (pFoldSheet)
				if (strFirstSelectedFoldsheet.IsEmpty()) 
					strFirstSelectedFoldsheet = pFoldSheet->m_strFoldSheetTypeName;
		}
	}

	m_dlgOutput.m_nOutputTarget	    = CPDFTargetList::BookletTarget;
	m_dlgOutput.m_strPDFTargetName  = pDoc->m_Bookblock.m_outputTargetProps.m_strTargetName;
	m_dlgOutput.m_bNoSheetSelection	= FALSE;
	m_dlgOutput.m_strWhatLayout	    = strFirstSelectedFoldsheet;
	m_dlgOutput.m_bRunModal		    = FALSE;
	m_dlgOutput.m_nWhatSheets	    = (pView->m_foldSheetList.GetSelectedCount() > 1) ? 2 : 0;
	m_dlgOutput.Create(IDD_PDFOUTPUT_MEDIA_SETTINGS, this);
}

void CBookblockFrame::OnBookblockProofOpenfile()
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (pDoc->m_Bookblock.OutFileExists(BOTHSIDES, NULL))
	{
		BeginWaitCursor();     
		CString strFilename  = pDoc->m_Bookblock.GetOutFileFullPath(BOTHSIDES, NULL);
		CString strExtension = strFilename.Right(4);
		strExtension.MakeLower();
		if (strExtension == _T(".pdf"))
			CPageListFrame::LaunchAcrobat(strFilename, -1);
		else
			if (strExtension == _T(".jdf"))
				ShellExecute(NULL, _T("open"), strFilename, NULL, NULL, SW_SHOWNORMAL);	
		EndWaitCursor();     
	}
}

BOOL CBookblockFrame::PreTranslateMessage(MSG* pMsg) 
{
	CBookBlockView* pBookBlockView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));
	if (pBookBlockView)
		pBookBlockView->UpdateDialogControls( pBookBlockView, FALSE );

	return CMDIChildWnd::PreTranslateMessage(pMsg);
}

