#pragma once

#include "OwnerDrawnButtonList.h"
#include "afxcmn.h"


// CJobDocumentationSettingsView-Formularansicht

class CJobDocumentationSettingsView : public CFormView
{
	DECLARE_DYNCREATE(CJobDocumentationSettingsView)

protected:
	CJobDocumentationSettingsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CJobDocumentationSettingsView();

public:
	enum { IDD = IDD_JOBDOCUMENTATIONSETTINGSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

enum {ZoomMultiPage, ZoomPageFit, ZoomPageWidth};


public:
	COwnerDrawnButtonList m_toolbar1Buttons, m_toolbar2Buttons, m_toolbar3Buttons;
	int  m_nShowSide;
	int  m_nZoomState;
	BOOL m_bShowFoldSheet;
	BOOL m_bShowMeasure;
	BOOL m_bShowPlate;
	BOOL m_bShowBitmap;
	BOOL m_bShowExposures;
	BOOL m_bShowCutBlocks;
	BOOL m_bShowBoxShapes;
	int  m_nSheetZoomLevel;
	CSpinButtonCtrl m_sheetZoomLevelSpin;


public:
	BOOL PageToPrint(CPrintDialog* pPrintDlg, int nPageNum);


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnViewFrontside();
	afx_msg void OnViewBackside();
	afx_msg void OnViewBothsides();
	afx_msg void OnFoldsheetCheck();
	afx_msg void OnMeasureCheck();
	afx_msg void OnPlateCheck();
	afx_msg void OnBitmapCheck();
	afx_msg void OnExposureCheck();
	afx_msg void OnShowCutBlocks();
	afx_msg void OnShowBoxshapes();
	afx_msg void OnZoomMultipage();
	afx_msg void OnZoomPageFit();
	afx_msg void OnZoomPageWidth();
	afx_msg void OnBnClickedJobdocPrintButton();
	afx_msg void OnBnClickedJobdocPagesetupButton();
	afx_msg void OnUpdateViewFrontside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBackside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBothsides(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFoldsheetCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMeasureCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlateCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBitmapCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateExposureCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowCutBlocks(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowBoxshapes(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomMultipage(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomPageFit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomPageWidth(CCmdUI* pCmdUI); 
	afx_msg void OnDeltaposSheetZoomLevelSpin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnKillfocusSheetZoomLevelEdit();
};


