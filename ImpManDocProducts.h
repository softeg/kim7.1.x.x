// ImpManDocProducts.h : interface of the CImpManDoc class
//
/////////////////////////////////////////////////////////////////////////////


#pragma once

#include "ImpManDocNumberingList.h"
#include "ImpManDocColorantList.h"
#include "ImpManDocProduction.h"

// CBoundProductPartComponent ////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CBoundProductPartComponent : public CObject
{
public:
    DECLARE_SERIAL( CBoundProductPartComponent )
	CBoundProductPartComponent();
	CBoundProductPartComponent(const CBoundProductPartComponent& rBoundProductPartComponent); // copy constructor


public:
	CString		m_strFoldScheme;
	int			m_nNumSheets;
	CString		m_strPageRange;		// e.g. 1-8;25-32


public:
	const CBoundProductPartComponent& operator=(const CBoundProductPartComponent& rBoundProductPartComponent);
	void  Copy(const CBoundProductPartComponent& rBoundProductPartComponent);
};


template <> void AFXAPI SerializeElements <CBoundProductPartComponent> (CArchive& ar, CBoundProductPartComponent* pBoundProductPartComponent, INT_PTR nCount);


class CBoundProductPartComponentList : public CArray <CBoundProductPartComponent, CBoundProductPartComponent&> 
{
public:
	CBoundProductPartComponentList();
	CBoundProductPartComponentList(const CBoundProductPartComponentList& rBoundProductPartComponentList); // copy constructor

public:
	const	CBoundProductPartComponentList& operator=(const CBoundProductPartComponentList& rBoundProductPartComponentList);
	void	Copy(const CBoundProductPartComponentList& rBoundProductPartComponentList);
};


// CBoundProductPart ////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CBoundProductPart : public CObject
{
public:
    DECLARE_SERIAL( CBoundProductPart )
	CBoundProductPart();
	CBoundProductPart(const CBoundProductPart& rBoundProductPart); // copy constructor

	enum Subtype{Cover, Body, Insert};

public:
	struct displayInfo
	{
		CArray <CRect, CRect&> 	rcDisplayBoxes;
		int						nCurDisplayBoxIndex;
		BOOL					bIsDisplayed;
		BOOL					bShowExpanded;
	}m_dspi;

protected:
	CString							m_strID;


public:
	CString							m_strName;
	int								m_nSubType;
	int								m_nNumPages;
	CNumberingList					m_numberingList;
	float							m_fPageWidth;
	float							m_fPageHeight;
	CString							m_strFormatName;
	int								m_nPageOrientation;
	CBindingDefs					m_bindingDefs;
	CColorantList					m_colorantList;
	CString							m_strPaper;
	int								m_nPaperGrammage;
	float							m_fPaperVolume;
	int								m_nPaperType;
	int								m_nPaperGrain;
	long							m_nDesiredQuantity;
	long							m_nExtra;
	long		 					m_nRealQuantity;
	CString							m_strComment;
	CFoldingProcess					m_foldingParams;
	CTrimmingProcess				m_trimmingParams;
	CBoundProductPartComponentList	m_components;


public:
	const CBoundProductPart& operator=(const CBoundProductPart& rBoundProductPart);
	BOOL  					 IsNull() { return (m_strName.IsEmpty()) ? TRUE : FALSE; };
	int						 GetIndex();
	CString					 GetID() { return m_strID; };
	void					 SetID(CString& strID) { m_strID = strID; };
	void					 ResetDisplayBoxes(BOOL bRemove) { m_dspi.nCurDisplayBoxIndex = 0; if (bRemove) m_dspi.rcDisplayBoxes.RemoveAll(); };
	CRect					 GetNextDisplayBox() { int nIndex = m_dspi.nCurDisplayBoxIndex++; return ( (nIndex >= 0) && (nIndex < m_dspi.rcDisplayBoxes.GetSize()) ) ? m_dspi.rcDisplayBoxes[nIndex] : CRect(); };
	void					 AddDisplayBox(CRect& rcBox);
	class CBoundProduct&	 GetBoundProduct(int nThisProductPartIndex = -1);
	CFoldSheet*				 GetFirstFoldSheet();
	CFoldSheet*				 GetNextFoldSheet(CFoldSheet* pFoldSheet);
	CPrintGroup&			 GetFirstPrintGroup();
	CPrintGroup&			 GetLastPrintGroup();
	BOOL					 IsCover();
	int						 GetNumUnassignedPages();
	int						 GetNumUnfoldedPages();
	int						 GetNumPlacedPages();
	float					 GetPaperThickness();
	float					 GetShinglingValueX(BOOL bTakeFromPaper = FALSE);
	float					 GetShinglingValueXFoot(BOOL bTakeFromPaper = FALSE);
	float					 GetShinglingValueY(BOOL bTakeFromPaper = FALSE);
	CString					 InitPaperInfo();
	void					 CalcRealQuantity();
	void					 SetFoldSheetsDesiredQuantity(int nDesiredQuantity);
	CRect					 DrawSummary(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, int nNumSectionPages = -1,	 CDisplayList* pDisplayList = NULL);
	CRect					 DrawThumbnail(CDC* pDC, CRect rcFrame, int nNumSectionPages = -1,	int nThumbnailWidth = 0, CDisplayList* pDisplayList = NULL);
	CRect					 DrawSize	  (CDC* pDC, CRect rcFrame,								int nLabelWidth = 0,	 CDisplayList* pDisplayList = NULL);
	CRect					 DrawQuantity (CDC* pDC, CRect rcFrame,								int nLabelWidth = 0,	 CDisplayList* pDisplayList = NULL);
	CRect					 DrawOverProduction	(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect					 DrawPaperGrain		(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect					 DrawTrims			(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect					 DrawPaperInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect					 DrawInfo	  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground);
	void					 SetDesiredQuantity(long nQuantity) { m_nDesiredQuantity = nQuantity; };
	long					 GetDesiredQuantity()			    { return m_nDesiredQuantity; };
	void					 SetRealQuantity(long nQuantity) { m_nRealQuantity = nQuantity; };
	long					 GetRealQuantity()				 { return m_nRealQuantity; };
	void					 AssignProductionProfilePool(CProductionProfile& rProductionProfile);
};

template <> void AFXAPI SerializeElements <CBoundProductPart> (CArchive& ar, CBoundProductPart* pBoundProductPart, INT_PTR nCount);


// CBoundProduct ////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CBoundProduct : public CObject
{
public:
    DECLARE_SERIAL( CBoundProduct )
	CBoundProduct();
	CBoundProduct(const CBoundProduct& rBoundProduct); // copy constructor

enum ProductType {Brochure = 1, Flyer = 2 };

private:
	struct displayInfo
	{
		CRect		rcDisplayBox;
		BOOL		bIsDisplayed;
		BOOL		bShowExpanded;
	}				m_dspi;

	long		 			m_nDesiredQuantity;
	long		 			m_nRealQuantity;

protected:
	CString					m_strProductID;
public:
//Attributes:
	CString		 			m_strProductName;
	int						m_nProductType;
	CString	 	 			m_strComment;
	int		 	 			m_nNumPages;
	float	 	 			m_fTrimSizeWidth, m_fTrimSizeHeight;
	CString  	 			m_strFormatName;
	unsigned 	 			m_nPageOrientation;
	CString		 			m_strBindingComment;
	CBindingProcess			m_bindingParams;
	CString					m_strContentFilePath;

	CArray <CBoundProductPart, CBoundProductPart&> m_parts;


private:
	class CBoundProductList* m_pParentList;

//Operations:
public:
	void  				Copy(const CBoundProduct& rBoundProduct);
	const 				CBoundProduct& operator=(const CBoundProduct& rBoundProduct);
	BOOL  				IsNull() { return (m_strProductName.IsEmpty()) ? TRUE : FALSE; };
	CRect				GetDisplayBox() { return m_dspi.rcDisplayBox; };
	void				SetDisplayBox(CRect& rcBox) { m_dspi.rcDisplayBox = rcBox; };
	CString				GetProductID() { return m_strProductID; };
	void				SetProductID(const CString strProductID) { m_strProductID = strProductID; };
	int					GetIndex();
	CBoundProductPart&  GetPart(int nLocalProductPartIndex);
	CBoundProductPart&  FindPart(CString& strPartName);
	CBoundProductPart&	JDFAddProductPart(CString strJDFNodeID, CString strJDFProductType, CString strName, int nNumPages, float fPageWidth, float fPageHeight, CString strFormatName, CBoundProduct& rBoundProduct);
	int	  				GetNumPages();
	void  				SetNumPages(int nNumPages) { m_nNumPages = nNumPages; };
	void  				ChangePageFormat(CString strFormatName, float fWidth, float fHeight, int nPageOrientation, int nProductPartIndex = -1);
	void  				ReorganizeReferences(int nIndex);
	int					GetGlobalProductPartIndex(const CString& strID);
	int					TransformLocalToGlobalPartIndex(int nLocalProductPartIndex);
	BOOL				HasCover();
	int					GetCoverProductPartIndex();
	CRect				DrawInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground);
	CRect				DrawThumbnail(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	void				SetDesiredQuantity(long nQuantity)	{ m_nDesiredQuantity = nQuantity; };
	long				GetDesiredQuantity()				{ return m_nDesiredQuantity; };
	void				SetRealQuantity(long nQuantity)		{ m_nRealQuantity = nQuantity; };
	long				GetRealQuantity()					{ return m_nRealQuantity; };
	CFoldSheet* 		GetFoldSheetByID(const CString& strID);
	void				AssignProductionProfilePool(CProductionProfile& rProductionProfile);
	void				AssignDuplicateName();
	int					GetNumDuplicateName(CString strName);
	BOOL				PageSourceIsAssigned(CString strDocumentFullpath);
};

template <> void AFXAPI SerializeElements <CBoundProduct> (CArchive& ar, CBoundProduct* pBoundProduct, INT_PTR nCount);


class CBoundProductList : public CList <CBoundProduct, CBoundProduct&>	// CList is derived from CObject
{																		// - so serialization will run
public:
    DECLARE_SERIAL( CBoundProductList )
	CBoundProductList();

//Attributes:
	CBoundProduct		m_nullProduct;
	CBoundProductPart	m_nullBoundProductPart;

//Operations:
public:
	void				Serialize(CArchive& ar);
	virtual POSITION	AddTail(CBoundProduct& rBoundProduct);
	void				DeleteContents();
	void				ResetProductPartDisplayBoxes(BOOL bRemove);
	CBoundProduct&		FindProductByID(const CString& strID);
	CBoundProduct&		FindProduct(const CString& strProductName);
	CBoundProduct&		GetProduct(CString strProductID);
	CBoundProduct&		GetProduct(int nIndex);
	CBoundProduct&		GetFirstProduct();
	CBoundProduct&		GetNextProduct(CBoundProduct& rCurrentProduct);
	CBoundProductPart&	GetProductPart(int nProductPartIndex);
	CBoundProductPart*	GetFirstPartOfBoundProduct(int nProductPartIndex);
	CBoundProductPart*	GetNextPartOfBoundProduct(CBoundProductPart* pProductPart, int nProductPartIndex);
	int					GetNumPartsTotal();
	CBoundProductPart& 	GetPartTotal(int nProductPartIndex);
	int					TransformPartToProductIndex(int nProductPartIndex);
	int					TransformGlobalToLocalPartIndex(int nProductPartIndex);
	void				RemoveProductPart(CBoundProductPart& rProductPart);
	void				UpdateQuantities();
	void				GetProductInfo(CJobInfoExchange& rJobInfoExchange);
	void				NormalizeIDs();
};



// CFlatProduct ////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CFlatProduct : public CObject
{
public:
    DECLARE_SERIAL( CFlatProduct )
	CFlatProduct();
	CFlatProduct(const CFlatProduct& rFlatProduct); // copy constructor
	CFlatProduct(const CString& strProductName, float fWidth, float fHeight, long nDesiredQuantity, const CString& strComment) 
				: m_strProductName(strProductName), m_strComment(strComment), m_nNumPages(1), m_strPaper(_T("")), m_nPaperGrammage(0), m_fPaperVolume(0.0f), m_fTrimSizeWidth(fWidth), m_fTrimSizeHeight(fHeight), 
				m_strFormatName(_T("")), m_nPageOrientation(PORTRAIT), m_nDesiredQuantity(nDesiredQuantity), m_nRealQuantity(0), 
				m_fLeftTrim(0.0f), m_fBottomTrim(0.0f), m_fRightTrim(0.0f), m_fTopTrim(0.0f), m_nPaperGrain(4), m_nGroup(0), m_strPunchTool(_T("")), m_nPriority(0) {};

friend class CFlatProductList;

enum {ReplicateAllContent = -1, ReplicateNone = 0};

//Attributes:
protected:
	CString			m_strProductID;
	int				m_nReplicate;
private:
	friend class	CPrintGroup;


public:
	CString		 	m_strProductName;
	CString	 	 	m_strComment;
	int		 	 	m_nNumPages;	// 1 or 2 pages; front and back page (2) or only front page (1)
	CColorantList	m_colorantList;
	CString			m_strPaper;
	int				m_nPaperGrammage;
	float			m_fPaperVolume;
	int				m_nPaperType;
	float	 	 	m_fTrimSizeWidth, m_fTrimSizeHeight;
	CString  	 	m_strFormatName;
	unsigned 	 	m_nPageOrientation;
	long		 	m_nDesiredQuantity;
	long		 	m_nRealQuantity;
	float		 	m_fLeftTrim; 
	float		 	m_fBottomTrim;
	float		 	m_fRightTrim;
	float		 	m_fTopTrim;
	int			 	m_nPaperGrain;	// horizontal, vertical or doesn't matter
	int				m_nGroup;
	CString			m_strPunchTool;
	int			 	m_nPriority;
	COleDateTime	m_printDeadline;
	CGraphicList	m_graphicList;
	CString			m_strContentFilePath;

	int				m_nFlag;

private:
	class CFlatProductList* m_pParentList;

//Operations:
public:
	//void  			Copy(const CFlatProduct& rFlatProduct);
	const 			CFlatProduct& operator=(const CFlatProduct& rFlatProduct);
	CFlatProduct&	Copy(const CFlatProduct& rFlatProduct, BOOL bChangedOnly = FALSE);
	BOOL  			IsNull() { return (m_strProductName.IsEmpty()) ? TRUE : FALSE; };
	CString			GetProductID() { return m_strProductID; };
	void			SetProductID(const CString strProductID) { m_strProductID = strProductID; };
	int				GetReplication() { return m_nReplicate; };
	void			SetReplication(int nReplicate) { m_nReplicate = nReplicate; };
	int				GetIndex();
	int				GetFlatProductsIndex();
	void			AddGraphicObject(CGraphicObject* pgObject);
	CPrintSheet*	GetFirstPrintSheet();
	CPrintSheet*	GetNextPrintSheet(CPrintSheet* pPrintSheet);
	CPrintGroup&	GetFirstPrintGroup();
	CLayoutObject*	GetFirstLayoutObject(CPrintSheet* pPrintSheet);
	int				GetNUpPrintLayout(CPrintSheet* pPrintSheet);
	int				GetNUpAll();
	void			CalcRealQuantity();
	float			CalcCoverage(CPrintSheet* pPrintSheet);
	void			GetCutSize(float& fWidth, float& fHeight);
	CRect			DrawThumbnail		  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0, BOOL bRelativSize = TRUE);
	CRect			DrawSize			  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0);
	CRect			DrawTargetQuantity	  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0);
	CRect			DrawActualQuantity	  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0);
	CRect			DrawDifferenceQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0);
	CRect			DrawOverProduction	  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0);
	CRect			DrawCoverage	      (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0, CPrintSheet* pPrintSheet = NULL);
	CRect			DrawPunchInfo		  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect			DrawZone			  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect			DrawPaperInfo		  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect			DrawPaperGrain		  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect			DrawTrims			  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect			DrawSummary			  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet = NULL);
	CRect			DrawPreview			  (CDC* pDC, CRect rcFrame, CPageTemplate* pPageTemplate, CLayoutObject* pObject, BOOL bShowTrims, BOOL bShowBitmap);
	void			DrawPreviewMeasures	  (CDC* pDC, CRect pageRect, CRect trimRect);
	CRect			DrawSheetPreview	  (CDC* pDC, CRect rcFrame, CLayoutObject* pLayoutObject = NULL, BOOL bShowContent = FALSE);	
	void			AssignDuplicateName();
	int				GetNumDuplicateName(CString strName);
	BOOL			PageSourceIsAssigned(CString strDocumentFullpath);
};

template <> void AFXAPI SerializeElements <CFlatProduct> (CArchive& ar, CFlatProduct* rFlatProduct, INT_PTR nCount);


class CFlatProductList : public CList <CFlatProduct, CFlatProduct&>	// CList is derived from CObject
{																	// - so serialization will run
public:
    DECLARE_SERIAL( CFlatProductList )
	CFlatProductList();


//Attributes:


//Operations:
public:
	void			 Serialize(CArchive& ar);
	void			 Copy(CFlatProductList& rFlatProductList);
	virtual POSITION AddTail(CFlatProduct& rFlatProduct);
	CFlatProduct&	 FindByIndex(INT_PTR nIndex);
	CFlatProduct&	 FindProductByID(CString strID);
	CFlatProduct&	 FindProduct(CString strProductName);
	int				 FindProductIndex(CString strProductName);
	int				 FindProductIndex(CFlatProduct* pFlatProduct);
	void			 CheckLayoutObjectFormats();
	BOOL			 HasGraphics();
	float			 GetMaxHeight(int nNum = -1);
	void			 DeleteContents();
	CFlatProduct&	 GetProduct(int nID) { return FindByIndex(nID); };
	CFlatProduct&	 GetProduct(CString strProductID);
	CFlatProduct&	 GetFirstProduct();
	CFlatProduct&	 GetNextProduct(CFlatProduct& rCurrentProduct);
	void			 UpdateQuantities();
	CRect			 DrawHeader(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	int				 GetNumUnImposedFormats(float fWidth, float fHeight);
	CFlatProduct&	 SkipToSuitableUnassignedProduct(float fWidth, float fHeight, int& nFlatProductIndex, int nSkip);
	void			 ResetFlags();
	void			 GetProductInfo(CJobInfoExchange& rJobInfoExchange);
};

