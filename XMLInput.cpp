#include "stdafx.h"
#include "Imposition Manager.h"
#include "XMLInput.h"
#include "DlgPDFLibStatus.h"
#include "KimSocketsCommon.h"


CXMLInput::CXMLInput(void)
{
	m_bPreImpose = TRUE;

	setlocale(LC_NUMERIC, "english");	// because when german setted, sscanf() does expect numbers with Comma like "0,138"
}

CXMLInput::~CXMLInput(void)
{
	switch (theApp.settings.m_iLanguage)
	{
		case 0:  setlocale(LC_ALL, "german");	break; 
		case 1:  setlocale(LC_ALL, "english");	break;
		default: setlocale(LC_ALL, "english");	break;
	}
}

BOOL CXMLInput::ParseXML(const CString& strFilePath, BOOL bPreImpose, CProductionProfile defaultProductionProfile)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	m_bPreImpose			   = bPreImpose;
	m_defaultProductionProfile = defaultProductionProfile;

	int			nCount = 0;
	CFileStatus status;
	while ( ! CFile::GetStatus(strFilePath, status))		
	{
		if (nCount > 20)
		{
			CString strMsg;
			strMsg.Format(_T("Error!\nCannot open file %s"), strFilePath);
			KIMOpenLogMessage(strMsg, MB_ICONERROR);
			return FALSE;
		}

		Sleep(100L);
		nCount++;
	} 

	m_pJDFDoc = new JDFDoc(0);
	parser	  = new JDFParser();

	try
	{
		parser->Parse((WString)strFilePath);
	}
	catch (JDFException e)
	{
		CString strMsg;
		strMsg.Format(_T("xml parsing error: %s"), e.getMessage().getBytes());
		KIMOpenLogMessage(strMsg, MB_ICONERROR);
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	*m_pJDFDoc = parser->GetRoot();

	if (m_pJDFDoc->isNull())
	{
		KIMOpenLogMessage(_T("xml parsing error: Root is NULL"), MB_ICONERROR);
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	XERCES_CPP_NAMESPACE::DOMDocument* domDoc = m_pJDFDoc->GetDOMDocument(); 
 
	DOMNode* jobNode = domDoc->getFirstChild();
	CString strNodeName = jobNode->getNodeName();
	if (strNodeName != _T("KIMJob"))
	{
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	CString strJobName = _T("");
	DOMNamedNodeMap* nodeMap = jobNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"Name");
		strJobName = (item) ? (CString)item->getNodeValue() : _T("");
	}

	pDoc->m_GlobalData.m_strJob = strJobName;

	m_bUserNotified_PressDeviceNotDefined = FALSE;

	BOOL	 bResult = FALSE;
	DOMNode* node	 = jobNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("JobDataSection"))
			bResult = ProcessJobDataSectionNode(node);
		else
		if (strNodeName == _T("ResourceSection"))
			bResult = ProcessResourceSectionNode(node);
		else
		if (strNodeName == _T("ProductSection"))
			bResult = ProcessProductSectionNode(node);
		else
		if (strNodeName == _T("StrippingSection"))
			bResult = ProcessStrippingSectionNode(node); 

		node = node->getNextSibling();
	}

	delete parser;
	delete m_pJDFDoc;

	pDoc->SetModifiedFlag(TRUE);

	if (pDoc->m_PrintSheetList.GetSize() == 1)
	{
		if (pDoc->m_PrintSheetList.GetHead().IsEmpty())		// no stripping happend
		{
			CPrintingProfile& rPrintingProfile = pDoc->m_PrintSheetList.GetHead().GetPrintingProfile();
			rPrintingProfile.m_prepress = (m_productionSetup.m_prepressData.GetSize() >= 1) ? m_productionSetup.m_prepressData[0] : CPrepressParams();
			rPrintingProfile.m_press	= (m_productionSetup.m_pressData.GetSize()	  >= 1) ? m_productionSetup.m_pressData[0]	  : CPressParams();
			CPressDevice* pPressDevice	= theApp.m_PressDeviceList.GetDevice(rPrintingProfile.m_press.m_pressDevice.m_strName);
			if ( ! pPressDevice && ! rPrintingProfile.m_press.m_pressDevice.m_strName.IsEmpty() )
			{
				CString strMsg;
				AfxFormatString1(strMsg, IDS_PRESSDEVICE_NOTDEFINED, _T("'") + rPrintingProfile.m_press.m_pressDevice.m_strName + _T("'"));
				KIMOpenLogMessage(strMsg, MB_ICONWARNING);
			}
			else
			{
				rPrintingProfile.m_press.m_pressDevice		   = (pPressDevice) ? *pPressDevice : CPressDevice();
				rPrintingProfile.m_press.m_backSidePressDevice = (pPressDevice) ? *pPressDevice : CPressDevice();
				CSheet sheet;
				if (rPrintingProfile.m_press.m_pressDevice.FindLargestPaper(sheet))
				{
					rPrintingProfile.m_press.m_sheetData = sheet;
					sheet.m_nPaperGrain  = GRAIN_VERTICAL;		// initialize always with vertical
					rPrintingProfile.m_press.m_availableSheets.Add(sheet);
				}
			}
		}
	}

	if ( ! m_defaultProductionProfile.m_prepress.IsNull())
	{
		CPDFTargetList pdfTargetList; 
		pdfTargetList.Load(CPDFTargetList::FoldSheetTarget);
		CPDFTargetProperties* pOutputTarget = pdfTargetList.Find(m_defaultProductionProfile.m_prepress.m_strFoldSheetOutputProfile);
		if (pOutputTarget)
			pDoc->m_Bookblock.m_FoldSheetList.m_outputTargetProps = *pOutputTarget;

		pdfTargetList.Load(CPDFTargetList::BookletTarget);
		pOutputTarget = pdfTargetList.Find(m_defaultProductionProfile.m_prepress.m_strBookletOutputProfile);
		if (pOutputTarget)
			pDoc->m_Bookblock.m_outputTargetProps = *pOutputTarget;
	}

	float fDummy;
	for (int nProductIndex = 0; nProductIndex < pDoc->m_boundProducts.GetSize(); nProductIndex++)
	{
		for (int i = 0; i < pDoc->GetBoundProduct(nProductIndex).m_parts.GetSize(); i++)
		{
			CBoundProductPart& rProductPart = pDoc->GetBoundProduct(nProductIndex).m_parts[i];
			int nThisProductPartIndex = rProductPart.GetIndex();
			pDoc->m_Bookblock.SetShinglingValuesX(rProductPart.m_foldingParams.GetShinglingValueX(rProductPart), FLT_MAX,		nThisProductPartIndex);
			pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, rProductPart.m_foldingParams.GetShinglingValueXFoot(rProductPart),	nThisProductPartIndex);
			pDoc->m_Bookblock.SetShinglingValuesY(rProductPart.m_foldingParams.GetShinglingValueY(rProductPart),				nThisProductPartIndex);
			pDoc->m_PageTemplateList.CalcShinglingOffsets(fDummy, fDummy, fDummy, -nProductIndex - 1);
		}
	}

	pDoc->m_Bookblock.ArrangeAssemblySections();
	pDoc->m_Bookblock.ArrangeCover();
	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();	// has already be done in stripping process
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();

	return TRUE;
}


///////// JobDataSection

BOOL CXMLInput::ProcessJobDataSectionNode(DOMNode* jobDataSectionNode)
{
	BOOL	 bResult = FALSE;
	CString  strNodeName;
	DOMNode* node = jobDataSectionNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Order"))
			bResult = ProcessOrderNode(node);
		else
		if (strNodeName == _T("Customer"))
			bResult = ProcessCustomerNode(node);

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessOrderNode(DOMNode* orderNode)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL	bResult = FALSE;
	CString strID;
	DOMNamedNodeMap* nodeMap = orderNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		strID = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Name");
		//pDoc->m_GlobalData.m_strJob = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Number");
		//pDoc->m_GlobalData.m_strJobNumber = (item) ? (CString)item->getNodeValue() : _T("");
	}

	return bResult;
}

BOOL CXMLInput::ProcessCustomerNode(DOMNode* customerNode)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL	bResult = FALSE;
	CString strID;
	DOMNamedNodeMap* nodeMap = customerNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		strID = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Name");
		pDoc->m_GlobalData.m_strCustomer = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Number");
		pDoc->m_GlobalData.m_strCustomerNumber = (item) ? (CString)item->getNodeValue() : _T("");
	}

	return bResult;
}


///////// ResourceSection

BOOL CXMLInput::ProcessResourceSectionNode(DOMNode* resourceSectionNode)
{
	BOOL	 bResult = FALSE;
	CString  strNodeName;
	DOMNode* node = resourceSectionNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Paper"))
			bResult = ProcessPaperResourceNode(node);
		else 
		if (strNodeName == _T("PrepressProcess"))
			bResult = ProcessPrepressNode(node);
		else
		if (strNodeName == _T("PrintingProcess"))
			bResult = ProcessPrintingNode(node);
		else
		if (strNodeName == _T("FoldingProcess"))
			bResult = ProcessFoldingNode(node);
		else
		if (strNodeName == _T("BindingProcess"))
			bResult = ProcessBindingNode(node);
		else
		if (strNodeName == _T("TrimmingProcess"))
			bResult = ProcessTrimmingNode(node);
		else
		if (strNodeName == _T("ImpositionProcess"))
			bResult = ProcessImpositionNode(node);

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessPaperResourceNode(DOMNode* paperNode)
{
	BOOL	bResult = FALSE;
	CString strID, strName, strType;
	DOMNamedNodeMap* nodeMap = paperNode->getAttributes();
	if (nodeMap)
	{
		CPaperDefs paperDefs;

		DOMNode* item = nodeMap->getNamedItem(L"ID");
		paperDefs.SetID((item) ? (CString)item->getNodeValue() : _T(""));

		item = nodeMap->getNamedItem(L"Name");
		paperDefs.m_strName = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Weight");
		paperDefs.m_nGrammage = (item) ? atoi((CString)item->getNodeValue()) : 0;

		item = nodeMap->getNamedItem(L"Volume");
		paperDefs.m_fVolume = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;

		item = nodeMap->getNamedItem(L"Thickness");
		paperDefs.m_fThickness = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;

		item = nodeMap->getNamedItem(L"Grade");
		paperDefs.m_nGrade = (item) ? atoi((CString)item->getNodeValue()) : 0;

		item = nodeMap->getNamedItem(L"Type");
		paperDefs.m_nType = (item) ? atoi((CString)item->getNodeValue()) : 0;

		m_productionSetup.AddResource(paperDefs);
	}

	return bResult;
}

BOOL CXMLInput::ProcessPrepressNode(DOMNode* prepressProcessNode)
{
	CPrepressParams prepressProcess;

	BOOL	bResult = FALSE;
	DOMNamedNodeMap* nodeMap = prepressProcessNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		prepressProcess.SetID((item) ? (CString)item->getNodeValue() : _T(""));
	}

	CString  strNodeName;
	DOMNode* node = prepressProcessNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("OutputProfile"))
			bResult = ProcessOutputProfileNode(node, prepressProcess.m_strPrintSheetOutputProfile);
		if (strNodeName == _T("PrintSheetOutputProfile"))
			bResult = ProcessOutputProfileNode(node, prepressProcess.m_strPrintSheetOutputProfile);
		if (strNodeName == _T("FoldSheetOutputProfile"))
			bResult = ProcessOutputProfileNode(node, prepressProcess.m_strFoldSheetOutputProfile);
		if (strNodeName == _T("BookletOutputProfile"))
			bResult = ProcessOutputProfileNode(node, prepressProcess.m_strBookletOutputProfile);

		node = node->getNextSibling();
	}

	m_productionSetup.AddProcess(prepressProcess);

	return bResult;
}

BOOL CXMLInput::ProcessOutputProfileNode(DOMNode* outputProfileNode, CString& strOutputProfile)
{
	BOOL	bResult = FALSE;
	DOMNamedNodeMap* nodeMap = outputProfileNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"Name");
		strOutputProfile = (item) ? (CString)item->getNodeValue() : _T("");
	}

	return bResult;
}

BOOL CXMLInput::ProcessPrintingNode(DOMNode* printingProcessNode)
{
	CPressParams printingProcess;

	BOOL	bResult = FALSE;
	CString strID, strName, strType;
	DOMNamedNodeMap* nodeMap = printingProcessNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		printingProcess.SetID((item) ? (CString)item->getNodeValue() : _T(""));
	}

	CString  strNodeName, strWorkStyle, strTurnStyle;
	DOMNode* node = printingProcessNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Device"))
			bResult = ProcessPrintingDeviceNode(node, printingProcess);
		else
		if (strNodeName == _T("WorkStyle"))
		{
			strWorkStyle = node->getTextContent();
			if (strWorkStyle == _T("WorkAndBack"))
				printingProcess.m_nWorkStyle = WORK_AND_BACK;
			else
			if (strWorkStyle == _T("WorkAndTurn"))
				printingProcess.m_nWorkStyle = WORK_AND_TURN;
			else
			if (strWorkStyle == _T("WorkAndTumble"))
				printingProcess.m_nWorkStyle = WORK_AND_TUMBLE;
			else
			if (strWorkStyle == _T("WorkSingleSide"))
				printingProcess.m_nWorkStyle = WORK_SINGLE_SIDE;
		}
		else
		if (strNodeName == _T("TurnStyle"))
		{
			strTurnStyle = node->getTextContent();
			if (strTurnStyle == _T("Turn"))
				printingProcess.m_nTurnStyle = 0;
			else
			if (strTurnStyle == _T("Tumble"))
				printingProcess.m_nTurnStyle = 1;
		}
		else
		if (strNodeName == _T("Markset"))
			printingProcess.m_strMarkset = node->getTextContent();

		node = node->getNextSibling();
	}

	m_productionSetup.AddProcess(printingProcess);

	return bResult;
}

BOOL CXMLInput::ProcessPrintingDeviceNode(DOMNode* printingProcessDeviceNode, CPressParams& rPrintingProcess)
{
	BOOL	bResult = FALSE;
	CString strName;
	DOMNamedNodeMap* nodeMap = printingProcessDeviceNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"Name");
		rPrintingProcess.m_pressDevice.m_strName = (item) ? (CString)item->getNodeValue() : _T("");
	}

	CString  strNodeName;
	DOMNode* node = printingProcessDeviceNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("NumColors"))
			rPrintingProcess.m_nNumColorStationsFront = rPrintingProcess.m_nNumColorStationsBack = atoi((CString)node->getTextContent());

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessFoldingNode(DOMNode* foldingProcessNode)
{
	CFoldingProcess foldingProcess = m_defaultProductionProfile.m_postpress.m_folding;

	BOOL	bResult = FALSE;
	CString strID, strName, strType;
	DOMNamedNodeMap* nodeMap = foldingProcessNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		foldingProcess.SetID((item) ? (CString)item->getNodeValue() : _T(""));
	}

	CString  strNodeName;
	DOMNode* node = foldingProcessNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("FoldSchemePool"))
			bResult = ProcessFoldingSchemePoolNode(node, foldingProcess);
		else
		if (strNodeName == _T("Shingling"))
			bResult = ProcessFoldingShinglingNode(node, foldingProcess);
		else
		if (strNodeName == _T("Markset"))
			foldingProcess.m_strMarkset = node->getTextContent();

		node = node->getNextSibling();
	}

	m_productionSetup.AddProcess(foldingProcess);

	return bResult;
}

BOOL CXMLInput::ProcessFoldingSchemePoolNode(DOMNode* foldingProcessSchemePoolNode, CFoldingProcess& rFoldingProcess)
{
	BOOL	bResult = FALSE;
	CString strNodeName;
	DOMNode* node = foldingProcessSchemePoolNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Scheme"))
		{
			DOMNamedNodeMap* nodeMap = node->getAttributes();
			if (nodeMap)
			{
				DOMNode* item	   = nodeMap->getNamedItem(L"ID");
				CString  strID	   = (item) ? (CString)item->getNodeValue() : _T("");
						 item	   = nodeMap->getNamedItem(L"Name");
				CString  strScheme = (item) ? (CString)item->getNodeValue() : _T("");

				CString strFoldSchemeFullPath = theApp.settings.m_szSchemeDir + CString("\\") + strScheme + CString(".isc");
				CFoldSheet foldSheet;
				foldSheet.SetID(strID);
				foldSheet.CreateFromISC(strFoldSchemeFullPath, strScheme, 0, TRUE);
				if ( ! foldSheet.IsEmpty())
					rFoldingProcess.m_foldSchemeList.AddTail(foldSheet);
			}
		}

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessFoldingShinglingNode(DOMNode* foldingProcessShinglingNode, CFoldingProcess& rFoldingProcess)
{
	BOOL	bResult = FALSE;
	CString strNodeName;
	DOMNode* node = foldingProcessShinglingNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Direction"))
		{
			CString strDirection = (CString)node->getTextContent();
			if (strDirection.CompareNoCase(_T("Spine")) == 0)
				rFoldingProcess.m_nShinglingDirection = CBookblock::ShinglingDirectionSpine;
			else
				if (strDirection.CompareNoCase(_T("SpineHead")) == 0)
					rFoldingProcess.m_nShinglingDirection = CBookblock::ShinglingDirectionSpineHead;
				else
					if (strDirection.CompareNoCase(_T("SpineFoot")) == 0)
						rFoldingProcess.m_nShinglingDirection = CBookblock::ShinglingDirectionSpineFoot;
		}
		else
			if (strNodeName == _T("Method"))
			{
				DOMNamedNodeMap* nodeMap = node->getAttributes();
				if (nodeMap)
				{
					DOMNode* item = nodeMap->getNamedItem(L"Compensation");
					CString strCompensation	= (item) ? (CString)item->getNodeValue() : _T("");
					if (strCompensation.CompareNoCase(_T("none")) == 0)
						rFoldingProcess.m_bShinglingActive = FALSE;
					else
						if (strCompensation.CompareNoCase(_T("Offset")) == 0)
						{
							rFoldingProcess.m_bShinglingActive = TRUE;
							rFoldingProcess.m_nShinglingMethod = CBookblock::ShinglingMethodMove;
						}
						else
							if (strCompensation.CompareNoCase(_T("Scaling")) == 0)
							{
								rFoldingProcess.m_bShinglingActive = TRUE;
								rFoldingProcess.m_nShinglingMethod = CBookblock::ShinglingMethodScale;
							}

					item = nodeMap->getNamedItem(L"CropMarks");
					CString strCropMarks = (item) ? (CString)item->getNodeValue() : _T("");
					if (strCropMarks.CompareNoCase(_T("included")) == 0)
						rFoldingProcess.m_bShinglingCropMarks = TRUE;
				}
			}
			else
				if (strNodeName == _T("ValuePerPage"))
				{
					rFoldingProcess.SetShinglingValueX((float)atof((CString)node->getTextContent()));
				}

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessBindingNode(DOMNode* bindingProcessNode)
{
	CBindingProcess bindingProcess;

	BOOL	bResult = FALSE;
	CString strID, strName, strType;
	DOMNamedNodeMap* nodeMap = bindingProcessNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		bindingProcess.SetID((item) ? (CString)item->getNodeValue() : _T(""));
	}

	CString  strNodeName;
	DOMNode* node = bindingProcessNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Collecting"))
			bResult = ProcessBindingCollectingNode(node, bindingProcess);
		else
		if (strNodeName == _T("Gathering"))
			bResult = ProcessBindingGatheringNode(node, bindingProcess);

		node = node->getNextSibling();
	}

	m_productionSetup.AddProcess(bindingProcess);

	return bResult;
}

BOOL CXMLInput::ProcessBindingCollectingNode(DOMNode* bindingProcessCollectingNode, CBindingProcess& rBindingProcess)
{
	rBindingProcess.m_bindingDefs.m_nBinding = CBindingDefs::SaddleStitched;

	BOOL	bResult = FALSE;
	CString strNodeName;
	DOMNode* node = bindingProcessCollectingNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Overfold"))
		{
			DOMNamedNodeMap* nodeMap = node->getAttributes();
			if (nodeMap)
			{
				DOMNode* item = nodeMap->getNamedItem(L"Side");
				CString strSide	= (item) ? (CString)item->getNodeValue() : _T("");
				rBindingProcess.m_nOverFoldSide = (strSide.CompareNoCase(_T("BACK")) == 0) ? CBindingProcess::OverFoldBack : CBindingProcess::OverFoldFront;

				item = nodeMap->getNamedItem(L"Value");
				rBindingProcess.m_fOverFold = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
			}
		}

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessBindingGatheringNode(DOMNode* bindingProcessGatheringNode, CBindingProcess& rBindingProcess)
{
	rBindingProcess.m_bindingDefs.m_nBinding = CBindingDefs::PerfectBound;

	BOOL	bResult = FALSE;
	CString strNodeName;
	DOMNode* node = bindingProcessGatheringNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		//if (strNodeName == _T(""))	// no subnodes yet
		//{
		//}

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessTrimmingNode(DOMNode* trimmingProcessNode)
{
	BOOL bResult = FALSE;
	
	CTrimmingProcess trimmingProcess;

	DOMNamedNodeMap* nodeMap = trimmingProcessNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		trimmingProcess.SetID((item) ? (CString)item->getNodeValue() : _T(""));

		//item = nodeMap->getNamedItem(L"Name");
		//strName = (item) ? (CString)item->getNodeValue() : _T("");
	}

	CString  strNodeName;
	DOMNode* node = trimmingProcessNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("HeadTrim"))
			trimmingProcess.m_fHeadTrim = (float)atof((CString)node->getTextContent());
		else
		if (strNodeName == _T("FootTrim"))
			trimmingProcess.m_fFootTrim = (float)atof((CString)node->getTextContent());
		else
		if (strNodeName == _T("SideTrim"))
			trimmingProcess.m_fSideTrim = (float)atof((CString)node->getTextContent());

		node = node->getNextSibling();
	}

	m_productionSetup.AddProcess(trimmingProcess);

	return bResult;
}

BOOL CXMLInput::ProcessImpositionNode(DOMNode* impositionProcessNode)
{
	BOOL bResult = FALSE;
	
	//CImpositionProcess impositionProcess;

	//DOMNamedNodeMap* nodeMap = impositionProcessNode->getAttributes();
	//if (nodeMap)
	//{
	//	DOMNode* item = nodeMap->getNamedItem(L"ID");
	//	impositionProcess.SetID((item) ? (CString)item->getNodeValue() : _T(""));

	//	item = nodeMap->getNamedItem(L"Name");
	//	impositionProcess.m_strName = (item) ? (CString)item->getNodeValue() : _T("");
	//}

	//CString  strNodeName;
	//DOMNode* node = impositionProcessNode->getFirstChild();
	//while (node)
	//{
	//	strNodeName = node->getNodeName();
	//	if (strNodeName == _T("Style"))
	//	{
	//		CString strStyle = node->getTextContent();
	//		if (strStyle.CompareNoCase(_T("CutAndStack")) == 0)
	//			impositionProcess.m_nImpositionStyle = CImpositionProcess::CutAndStack;
	//	}

	//	node = node->getNextSibling();
	//}

	//m_productionSetup.AddProcess(impositionProcess);

	return bResult;
}

///////// ProductSection

BOOL CXMLInput::ProcessProductSectionNode(DOMNode* productSectionNode)
{
	BOOL	 bResult = FALSE;
	CString  strNodeName;
	DOMNode* node = productSectionNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Product"))
			bResult = ProcessProductNode(node);

		node = node->getNextSibling();
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_Bookblock.ReNumberFoldSheets(ALL_PARTS);
		pDoc->m_PageTemplateList.SortPages();
	}

	if (AnyContentFileExisting())
	{
		m_pPageSource = new CPageSource;
		theApp.m_pDlgPDFLibStatus->m_pPageSource = m_pPageSource;

#ifdef KIM_ENGINE
		BOOL bQuietMode = TRUE;
#else
		BOOL bQuietMode = (theApp.m_nGUIStyle != CImpManApp::GUIStandard) ? TRUE : FALSE;
#endif
		theApp.m_pDlgPDFLibStatus->LaunchPDFEngine("", theApp.m_pMainWnd, CDlgPDFLibStatus::RegisterPages, _T(""), FALSE, bQuietMode);

		POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
		while (pos)
		{
			CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);

			if ( ! rBoundProduct.m_strContentFilePath.IsEmpty())
			{
				if ( ! rBoundProduct.PageSourceIsAssigned(rBoundProduct.m_strContentFilePath))
				{
					CPageSource* pPageSource = pDoc->m_PageSourceList.FindByDocumentFullPath(rBoundProduct.m_strContentFilePath);
					if ( ! pPageSource)
					{
						theApp.m_pDlgPDFLibStatus->ProcessPDFPages(rBoundProduct.m_strContentFilePath, m_pPageSource, FALSE, FALSE);
						setlocale(LC_NUMERIC, "english");	// locale has been resetted in ProcessPDFPages() -> set back to english
						if (m_pPageSource->m_PageSourceHeaders.GetSize())
						{
							pDoc->m_PageSourceList.AddTail(*m_pPageSource);
							pDoc->m_PageTemplateList.AutoAssignPageSource(&pDoc->m_PageSourceList.GetTail(), -rBoundProduct.GetIndex() - 1, -1, FALSE);
						}
						m_pPageSource->m_PageSourceHeaders.RemoveAll();
						*m_pPageSource = CPageSource();
					}
					else
						pDoc->m_PageTemplateList.AutoAssignPageSource(pPageSource, -rBoundProduct.GetIndex() - 1, -1, FALSE);
				}
			}
			if (KimEngineDBJobAbort(theApp.m_nDBCurrentJobID))
				break;
		}

		pos = pDoc->m_flatProducts.GetHeadPosition();
		while (pos)
		{
			CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);

			if ( ! rFlatProduct.m_strContentFilePath.IsEmpty())
			{
				if ( ! rFlatProduct.PageSourceIsAssigned(rFlatProduct.m_strContentFilePath))
				{
					CPageSource* pPageSource = pDoc->m_PageSourceList.FindByDocumentFullPath(rFlatProduct.m_strContentFilePath);
					if ( ! pPageSource)
					{
						theApp.m_pDlgPDFLibStatus->ProcessPDFPages(rFlatProduct.m_strContentFilePath, m_pPageSource, FALSE, FALSE);
						setlocale(LC_NUMERIC, "english");	// locale has been resetted in ProcessPDFPages() -> set back to english

						if (rFlatProduct.GetReplication() == CFlatProduct::ReplicateAllContent)
							ReplicatePDFFlats(rFlatProduct, m_pPageSource, m_pPageSource->m_PageSourceHeaders.GetSize(), rFlatProduct.m_nNumPages);
						if (m_pPageSource->m_PageSourceHeaders.GetSize())
						{
							pDoc->m_PageSourceList.AddTail(*m_pPageSource);
							pDoc->m_PageTemplateList.AutoAssignPageSource(&pDoc->m_PageSourceList.GetTail(), pDoc->GetFlatProductsIndex(), -1, FALSE);
						}
						m_pPageSource->m_PageSourceHeaders.RemoveAll();
						*m_pPageSource = CPageSource();
					}
					else
						pDoc->m_PageTemplateList.AutoAssignPageSource(pPageSource, pDoc->GetFlatProductsIndex(), -1, FALSE);
				}
			}
			if (KimEngineDBJobAbort(theApp.m_nDBCurrentJobID))
				break;
		}

		if (theApp.m_pDlgPDFLibStatus->m_hWnd)
			theApp.m_pDlgPDFLibStatus->DestroyWindow();
		
		theApp.m_pDlgPDFLibStatus->m_pPageSource = NULL;
		if (m_pPageSource)
			delete m_pPageSource;
		m_pPageSource = NULL;
	}

	return bResult;
}

BOOL CXMLInput::ProcessProductNode(DOMNode* productNode)
{
	BOOL	bResult = FALSE;
	CString strID, strName, strType, strReplication;
	DOMNamedNodeMap* nodeMap = productNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		strID	= (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Name");
		strName = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Type");
		strType = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Replicate");
		strReplication = (item) ? (CString)item->getNodeValue() : _T("");
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return bResult;

	if ( (strType.CompareNoCase(_T("Flyer")) == 0) || (strType.CompareNoCase(_T("Brochure")) == 0) )
	{
		CBoundProduct& rProduct = pDoc->m_boundProducts.FindProductByID(strID);
		if (rProduct.IsNull())
		{
			CBoundProduct newBoundProduct;
			newBoundProduct.SetProductID(strID);
			newBoundProduct.m_strProductName = strName;
			if (strType.CompareNoCase(_T("Brochure")) == 0)
				newBoundProduct.m_nProductType = CBoundProduct::Brochure;
			else
			if (strType.CompareNoCase(_T("Flyer")) == 0)
				newBoundProduct.m_nProductType = CBoundProduct::Flyer;
			pDoc->m_boundProducts.AddTail(newBoundProduct);
			ProcessBoundProductNode(productNode, pDoc->m_boundProducts.GetTail());
		}
		else
			ProcessBoundProductNode(productNode, rProduct);
	}
	else
	if (strType.CompareNoCase(_T("Label")) == 0)
	{
		CFlatProduct& rProduct = pDoc->m_flatProducts.FindProductByID(strID);
		if (rProduct.IsNull())
		{
			CFlatProduct newFlatProduct;
			newFlatProduct.SetProductID(strID);
			newFlatProduct.SetReplication((strReplication.CompareNoCase(_T("AllContent")) == 0) ? CFlatProduct::ReplicateAllContent : CFlatProduct::ReplicateNone);
			newFlatProduct.m_strProductName = strName;
			pDoc->m_flatProducts.AddTail(newFlatProduct);
			pDoc->m_PageTemplateList.SynchronizeToFlatProductList(pDoc->m_flatProducts, pDoc->GetFlatProductsIndex());
			ProcessFlatProductNode(productNode, pDoc->m_flatProducts.GetTail());
		}
		else
			ProcessFlatProductNode(productNode, rProduct);
	}

	return bResult;
}

BOOL CXMLInput::ProcessBoundProductNode(DOMNode* boundProductNode, CBoundProduct& rBoundProduct)
{
	BOOL	bResult = FALSE;
	CString strType;
	DOMNamedNodeMap* nodeMap = boundProductNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"Type");
		if ( ! item)
			return FALSE;
		strType = item->getNodeValue();
	}

	DOMNode* pBindingInfoNode = NULL;
	CString  strNodeName;
	DOMNode* node = boundProductNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("ProductLayout"))
			bResult = ProcessBoundProductLayoutNode(node, rBoundProduct);
		else
		if (strNodeName == _T("Quantity"))
			bResult = ProcessBoundProductQuantityNode(node, rBoundProduct);
		else
		if (strNodeName == _T("Delivery"))
			bResult = ProcessBoundProductDeliveryNode(node, rBoundProduct);
		else
		if (strNodeName == _T("ContentFile"))
			bResult = ProcessBoundProductContentNode(node, rBoundProduct);
		else
		if (strNodeName == _T("ProductPart"))
			bResult = ProcessBoundProductPartNode(node, rBoundProduct);
		else
		if (strNodeName == _T("BindingInfo"))
			ProcessBoundProductBindingInfoNode(pBindingInfoNode = node, rBoundProduct);

		node = node->getNextSibling();
	}

	if ( ! pBindingInfoNode && m_bPreImpose)	// impose yourself automatically
		AutoImposeFoldSheets(rBoundProduct);

	for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
	{
		rBoundProduct.m_parts[i].SetDesiredQuantity(rBoundProduct.GetDesiredQuantity());
	}

	return bResult;
}

BOOL CXMLInput::ProcessBoundProductLayoutNode(DOMNode* boundProductLayoutNode, CBoundProduct& rBoundProduct)
{
	BOOL	bResult = FALSE;
	CString strNumPages;
	DOMNamedNodeMap* nodeMap = boundProductLayoutNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"NumPages");
		strNumPages	= (item) ? (CString)item->getNodeValue() : _T("");
		rBoundProduct.m_nNumPages = atoi(strNumPages);
	}

	CString  strNodeName;
	DOMNode* node = boundProductLayoutNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("TrimSize"))
		{
			nodeMap = node->getAttributes();
			if (nodeMap)
			{
				float fWidth, fHeight;
				CString strName;
				DOMNode* item = nodeMap->getNamedItem(L"Width");
				fWidth = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
				item = nodeMap->getNamedItem(L"Height");
				fHeight = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
				item = nodeMap->getNamedItem(L"Name");
				strName = (item) ? (CString)item->getNodeValue() : _T("");
				rBoundProduct.m_fTrimSizeWidth    = (fWidth  != 0.0f) ? fWidth			: 0.0f;
				rBoundProduct.m_fTrimSizeHeight   = (fHeight != 0.0f) ? fHeight			: 0.0f;
				rBoundProduct.m_strFormatName	  = (strName.GetLength() > 0) ? strName : _T("");
			}
		}

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessBoundProductQuantityNode(DOMNode* boundProductQuantityNode, CBoundProduct& rBoundProduct)
{
	DOMNamedNodeMap* nodeMap = boundProductQuantityNode->getAttributes();
	if (nodeMap)
	{
		int nDesiredQuantity;
		DOMNode* item = nodeMap->getNamedItem(L"Desired");
		nDesiredQuantity = (item) ? (int)atof((CString)item->getNodeValue()) : 0;
		rBoundProduct.SetDesiredQuantity(nDesiredQuantity);
	}

	return TRUE;
}

BOOL CXMLInput::ProcessBoundProductDeliveryNode(DOMNode* boundProductDeliveryNode, CBoundProduct& rBoundProduct)
{
	//DOMNamedNodeMap* nodeMap = boundProductLayoutNode->getAttributes();
	//if (nodeMap)
	//{
	//	CString strDeadline;
	//	DOMNode* item = nodeMap->getNamedItem(L"Deadline");
	//	strDeadline = (item) ? (CString)item->getNodeValue() : _T("");
	//	rBoundProduct.m_printDeadline.ParseDateTime(strDeadline, VAR_DATEVALUEONLY);
	//}

	return TRUE;
}

BOOL CXMLInput::ProcessBoundProductContentNode(DOMNode* boundProductContentNode, CBoundProduct& rBoundProduct)
{
	rBoundProduct.m_strContentFilePath = boundProductContentNode->getTextContent();

	return TRUE;
}

BOOL CXMLInput::ProcessBoundProductPartNode(DOMNode* boundProductPartNode, CBoundProduct& rBoundProduct)
{
	BOOL			 bResult = FALSE;
	CString			 strID, strName, strType;
	CTrimmingProcess trimmingProcess;
	CFoldingProcess  foldingProcess;
	DOMNamedNodeMap* nodeMap = boundProductPartNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		strID = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Name");
		strName = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Type");
		strType = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"FoldingProcessRefID");
		foldingProcess = GetFoldingProcess((item) ? (CString)item->getNodeValue() : _T(""));

		item = nodeMap->getNamedItem(L"TrimmingProcessRefID");
		trimmingProcess = GetTrimmingProcess((item) ? (CString)item->getNodeValue() : _T(""));
	}

	BOOL bIsNewProductPart = FALSE;
	if (rBoundProduct.FindPart(strName).IsNull())
	{
		CBoundProductPart newBoundProductPart;
		newBoundProductPart.SetID(strID);
		newBoundProductPart.m_strName		 = strName;
		newBoundProductPart.m_fPageWidth	 = rBoundProduct.m_fTrimSizeWidth;
		newBoundProductPart.m_fPageHeight    = rBoundProduct.m_fTrimSizeHeight;
		newBoundProductPart.m_strFormatName  = rBoundProduct.m_strFormatName;
		newBoundProductPart.m_trimmingParams = trimmingProcess;
		newBoundProductPart.m_foldingParams  = foldingProcess;
		rBoundProduct.m_parts.Add(newBoundProductPart);
		bIsNewProductPart = TRUE;
	}

	CBoundProductPart& rBoundProductPart = rBoundProduct.FindPart(strName);

	if (strType.CompareNoCase(_T("Cover")) == 0)
	{
		rBoundProductPart.m_nSubType = CBoundProductPart::Cover;
		rBoundProductPart.m_strName = theApp.m_strCoverName;
	}
	else
	if (strType.CompareNoCase(_T("Content")) == 0)
		rBoundProductPart.m_nSubType = CBoundProductPart::Body;	

	CString  strNodeName;
	DOMNode* node = boundProductPartNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("ProductPartLayout"))
			ProcessBoundProductPartLayoutNode(node, rBoundProductPart);
		else
		if (strNodeName == _T("ColorIntents"))
			bResult = ProcessBoundProductPartColorIntentsNode(node, rBoundProductPart);

		node = node->getNextSibling();
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		int nBoundProductIndex = rBoundProduct.GetIndex();
		int nProductPartIndex  = rBoundProductPart.GetIndex();
		if (bIsNewProductPart)
		{
			pDoc->m_PageTemplateList.ShiftProductPartIndices(nProductPartIndex);
			pDoc->m_Bookblock.ShiftProductPartIndices(nProductPartIndex);
			pDoc->m_PageTemplateList.InsertProductPartPages(rBoundProductPart.m_nNumPages, nBoundProductIndex, nProductPartIndex);
		}
		else
			pDoc->m_PageTemplateList.AdjustNumPages(rBoundProductPart.m_nNumPages, nBoundProductIndex, nProductPartIndex);

#ifdef KIM_ENGINE
		int nDBProductID = KimEngineDBSetProduct(theApp.m_nDBCurrentJobID, -1, kimAutoDB_product::Bound, rBoundProduct.m_strProductName, rBoundProductPart.m_strName, rBoundProductPart.m_fPageWidth, rBoundProductPart.m_fPageHeight, rBoundProductPart.m_nNumPages);
		KimEngineNotifyServer(MSG_PRODUCT_STATUS_CHANGED, nDBProductID);
#endif
	}

	return bResult;
}

BOOL CXMLInput::ProcessBoundProductBindingInfoNode(DOMNode* boundProductBindingInfoNode, CBoundProduct& rBoundProduct)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL bResult = FALSE;
	CString strBindingProcessRefID, strBindingStyleName;
	DOMNamedNodeMap* nodeMap = boundProductBindingInfoNode->getAttributes();
	if (nodeMap)
	{
		CString strBindingProcessRefID, strBindingStyleName;
		DOMNode* item = nodeMap->getNamedItem(L"BindingProcessRefID");
		strBindingProcessRefID = (item) ? (CString)item->getNodeValue() : _T("");
		item = nodeMap->getNamedItem(L"BindingStyleName");
		strBindingStyleName = (item) ? (CString)item->getNodeValue() : _T("");

		CBindingProcess bindingProcess = GetBindingProcess(strBindingProcessRefID);
		rBoundProduct.m_bindingParams  = bindingProcess;
		rBoundProduct.m_bindingParams.SetID(strBindingProcessRefID);
		if ( ! strBindingStyleName.IsEmpty())
			rBoundProduct.m_bindingParams.m_bindingDefs.m_strName = strBindingStyleName;
	}

	BOOL	 bBindingInfoIsEmpty = TRUE;
	CString  strNodeName;
	DOMNode* node = boundProductBindingInfoNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("FoldSheet"))
		{
			nodeMap = node->getAttributes();
			if (nodeMap)
			{
				bBindingInfoIsEmpty = FALSE;

				int		nNumber;
				CString strID, strScheme, strProductPartRefID, strPositionIndex;
				DOMNode* item = nodeMap->getNamedItem(L"ID");
				strID = (item) ? (CString)item->getNodeValue() : _T("");
				item = nodeMap->getNamedItem(L"Number");
				nNumber = (item) ? atoi((CString)item->getNodeValue()) : 0;
				item = nodeMap->getNamedItem(L"Scheme");
				strScheme = (item) ? (CString)item->getNodeValue() : _T("");
				item = nodeMap->getNamedItem(L"ProductPartRefID");
				strProductPartRefID = (item) ? (CString)item->getNodeValue() : _T("");
				item = nodeMap->getNamedItem(L"PositionIndex");
				strPositionIndex = (item) ? (CString)item->getNodeValue() : _T("");

				CFoldSheet* pFoldSheet = pDoc->m_Bookblock.GetFoldSheetByID(strID);
				if (pFoldSheet)
				{
					pFoldSheet->SetQuantityPlanned(rBoundProduct.GetDesiredQuantity(), FALSE);
					if (nNumber > 0)
					{
						pFoldSheet->m_nFoldSheetNumber	  = nNumber;
						pFoldSheet->m_bFoldSheetNumLocked = TRUE; 
					}
				}
				else
				{
					int		nProductPartIndex	  = rBoundProduct.GetGlobalProductPartIndex(strProductPartRefID);
					CString strFoldSchemeFullPath = theApp.settings.m_szSchemeDir + CString("\\") + strScheme + CString(".isc");
					CFoldSheet newFoldSheet;
					newFoldSheet.SetID(strID);
					newFoldSheet.CreateFromISC(strFoldSchemeFullPath, strScheme, nProductPartIndex, TRUE);
					if ( ! newFoldSheet.IsEmpty())
					{
						newFoldSheet.SetQuantityPlanned(rBoundProduct.GetDesiredQuantity(), FALSE);
						if (nNumber > 0)
						{
							newFoldSheet.m_nFoldSheetNumber	   = nNumber;
							newFoldSheet.m_bFoldSheetNumLocked = TRUE; 
						}

						int nNumUnfoldedPages = pDoc->m_boundProducts.GetProductPart(nProductPartIndex).GetNumUnfoldedPages();
						if ( (nNumUnfoldedPages > 0) && (nNumUnfoldedPages >= newFoldSheet.GetNumPages()) )
							pDoc->m_Bookblock.AddFoldSheetsSimple(&newFoldSheet, 1, NULL, nProductPartIndex, atoi(strPositionIndex), NULL);
					}
				}
			}
		}

		node = node->getNextSibling();
	}

	if (bBindingInfoIsEmpty && m_bPreImpose)	// impose yourself automatically
		AutoImposeFoldSheets(rBoundProduct);

	return bResult;
}

void CXMLInput::AutoImposeFoldSheets(CBoundProduct& rProduct)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strID;
	for (int nIndex = 0; nIndex < rProduct.m_parts.GetSize(); nIndex++)
	{
		CBoundProductPart& rProductPart = rProduct.GetPart(nIndex);
		if (rProductPart.m_foldingParams.m_foldSchemeList.GetSize() <= 0)
			continue;

		int nProductPartIndex = rProduct.TransformLocalToGlobalPartIndex(nIndex);
		int nNumUnfoldedPages = rProductPart.GetNumUnfoldedPages();
		while (nNumUnfoldedPages > 0)
		{
			int nPoolIndex = -1;;
			CFoldSheet newFoldSheet = rProductPart.m_foldingParams.FindBestFoldScheme(nNumUnfoldedPages);
			if (newFoldSheet.IsEmpty())
				continue;

			strID.Format(_T("%d"), pDoc->m_Bookblock.m_FoldSheetList.GetSize());
			newFoldSheet.SetID(strID);
			newFoldSheet.m_nProductPartIndex = nProductPartIndex;
			pDoc->m_Bookblock.AddFoldSheetsSimple(&newFoldSheet, 1, NULL, -1, -1, NULL);

			nNumUnfoldedPages = rProductPart.GetNumUnfoldedPages();
		}

		if (rProductPart.m_foldingParams.m_bShinglingActive)
		{
			rProductPart.m_foldingParams.SetShinglingValueX(rProductPart.GetShinglingValueX(TRUE));
			rProductPart.m_foldingParams.SetShinglingValueXFoot(rProductPart.GetShinglingValueXFoot(TRUE));
			rProductPart.m_foldingParams.SetShinglingValueY(rProductPart.GetShinglingValueY(TRUE));
			pDoc->m_Bookblock.SetShinglingValuesX(rProductPart.m_foldingParams.GetShinglingValueX(rProductPart), FLT_MAX, nProductPartIndex);
			pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, rProductPart.m_foldingParams.GetShinglingValueXFoot(rProductPart), nProductPartIndex);
			pDoc->m_Bookblock.SetShinglingValuesY(rProductPart.m_foldingParams.GetShinglingValueY(rProductPart), nProductPartIndex);
			float fDummy;
			pDoc->m_PageTemplateList.CalcShinglingOffsets(fDummy, fDummy, fDummy, nProductPartIndex);
		}
	}

	pDoc->m_Bookblock.ReNumberFoldSheets(ALL_PARTS);
}

BOOL CXMLInput::ProcessBoundProductPartLayoutNode(DOMNode* boundProductPartLayoutNode, CBoundProductPart& rBoundProductPart)
{
	BOOL	bResult = FALSE;
	CString strNumPages;
	DOMNamedNodeMap* nodeMap = boundProductPartLayoutNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"NumPages");
		strNumPages	= (item) ? (CString)item->getNodeValue() : _T("");
		rBoundProductPart.m_nNumPages = atoi(strNumPages);
	}

	CString  strNodeName;
	DOMNode* node = boundProductPartLayoutNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("TrimSize"))
		{
			nodeMap = node->getAttributes();
			if (nodeMap)
			{
				float fWidth, fHeight;
				CString strName;
				DOMNode* item = nodeMap->getNamedItem(L"Width");
				fWidth = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
				item = nodeMap->getNamedItem(L"Height");
				fHeight = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
				item = nodeMap->getNamedItem(L"Name");
				strName = (item) ? (CString)item->getNodeValue() : _T("");
				rBoundProductPart.m_fPageWidth    = (fWidth  != 0.0f) ? fWidth			: rBoundProductPart.m_fPageWidth;
				rBoundProductPart.m_fPageHeight   = (fHeight != 0.0f) ? fHeight			: rBoundProductPart.m_fPageHeight;
				rBoundProductPart.m_strFormatName = (strName.GetLength() > 0) ? strName : rBoundProductPart.m_strFormatName;
			}
		}

		if (strNodeName == _T("Paper"))
		{
			nodeMap = node->getAttributes();
			if (nodeMap)
			{
				CString strRefID, strPaperGrain;
				DOMNode* item = nodeMap->getNamedItem(L"RefID");
				strRefID = (item) ? (CString)item->getNodeValue() : _T("");
				item = nodeMap->getNamedItem(L"Grain");
				strPaperGrain = (item) ? (CString)item->getNodeValue() : _T("");
				if (strPaperGrain.CompareNoCase(_T("Vertical")) == 0)
					rBoundProductPart.m_nPaperGrain = GRAIN_VERTICAL;
				else
				if (strPaperGrain.CompareNoCase(_T("Horizontal")) == 0)
					rBoundProductPart.m_nPaperGrain = GRAIN_HORIZONTAL;
				else
					rBoundProductPart.m_nPaperGrain = GRAIN_EITHER;

				CPaperDefs& rPaperDefs = GetPaperResource(strRefID);
				rBoundProductPart.m_strPaper	   = rPaperDefs.m_strName;
				rBoundProductPart.m_nPaperGrammage = rPaperDefs.m_nGrammage;
				rBoundProductPart.m_fPaperVolume   = rPaperDefs.m_fVolume;
				rBoundProductPart.m_nPaperType	   = rPaperDefs.m_nType;
			}
		}

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessBoundProductPartColorIntentsNode(DOMNode* boundProductPartColorIntentsNode, CBoundProductPart& rBoundProductPart)
{
	CString  strNodeName;
	DOMNode* node = boundProductPartColorIntentsNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Separation"))
		{
			DOMNamedNodeMap* nodeMap = node->getAttributes();
			if (nodeMap)
			{
				CString strColorName;
				DOMNode* item = nodeMap->getNamedItem(L"Name");
				strColorName = (item) ? (CString)item->getNodeValue() : _T("");
				rBoundProductPart.m_colorantList.AddSeparation(strColorName);
			}
		}
		node = node->getNextSibling();
	}

	return TRUE;
}

//// flat products
BOOL CXMLInput::ProcessFlatProductNode(DOMNode* flatProductNode, CFlatProduct& rFlatProduct)
{
	BOOL	bResult = FALSE;
	CString strType;
	DOMNamedNodeMap* nodeMap = flatProductNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"Type");
		if ( ! item)
			return FALSE;
		strType = item->getNodeValue();
	}
	CString  strNodeName;
	DOMNode* node = flatProductNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("ProductLayout"))
			bResult = ProcessFlatProductLayoutNode(node, rFlatProduct);
		else
		if (strNodeName == _T("Quantity"))
			bResult = ProcessFlatProductQuantityNode(node, rFlatProduct);
		else
		if (strNodeName == _T("Delivery"))
			bResult = ProcessFlatProductDeliveryNode(node, rFlatProduct);
		else
		if (strNodeName == _T("ContentFile"))
			bResult = ProcessFlatProductContentNode(node, rFlatProduct);
		else
		if (strNodeName == _T("ColorIntents"))
			bResult = ProcessFlatProductColorIntentsNode(node, rFlatProduct);

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessFlatProductLayoutNode(DOMNode* flatProductLayoutNode, CFlatProduct& rFlatProduct)
{
	BOOL	bResult = FALSE;
	CString strNumPages;
	DOMNamedNodeMap* nodeMap = flatProductLayoutNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"NumPages");
		strNumPages	= (item) ? (CString)item->getNodeValue() : _T("");
		rFlatProduct.m_nNumPages = atoi(strNumPages);
	}

	CString  strNodeName;
	DOMNode* node = flatProductLayoutNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("TrimSize"))
		{
			nodeMap = node->getAttributes();
			if (nodeMap)
			{
				float fWidth, fHeight;
				CString strName;
				DOMNode* item = nodeMap->getNamedItem(L"Width");
				fWidth = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
				item = nodeMap->getNamedItem(L"Height");
				fHeight = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
				item = nodeMap->getNamedItem(L"Name");
				strName = (item) ? (CString)item->getNodeValue() : _T("");
				rFlatProduct.m_fTrimSizeWidth	= (fWidth  != 0.0f) ? fWidth			: 0.0f;
				rFlatProduct.m_fTrimSizeHeight  = (fHeight != 0.0f) ? fHeight			: 0.0f;
				rFlatProduct.m_strFormatName	= (strName.GetLength() > 0) ? strName : _T("");
			}
		}
		else
		if (strNodeName == _T("Bleeds"))
		{
			nodeMap = node->getAttributes();
			if (nodeMap)
			{
				DOMNode* item = nodeMap->getNamedItem(L"Left");
				rFlatProduct.m_fLeftTrim	= (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
				item = nodeMap->getNamedItem(L"Right");
				rFlatProduct.m_fRightTrim	= (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
				item = nodeMap->getNamedItem(L"Top");
				rFlatProduct.m_fTopTrim		= (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
				item = nodeMap->getNamedItem(L"Bottom");
				rFlatProduct.m_fBottomTrim	= (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
			}
		}
		else
		if (strNodeName == _T("Paper"))
		{
			nodeMap = node->getAttributes();
			if (nodeMap)
			{
				CString strRefID, strPaperGrain;
				DOMNode* item = nodeMap->getNamedItem(L"RefID");
				strRefID = (item) ? (CString)item->getNodeValue() : _T("");

				CPaperDefs& rPaperDefs = GetPaperResource(strRefID);
				rFlatProduct.m_strPaper			= rPaperDefs.m_strName;
				rFlatProduct.m_nPaperGrammage	= rPaperDefs.m_nGrammage;
				rFlatProduct.m_fPaperVolume		= rPaperDefs.m_fVolume;
				rFlatProduct.m_nPaperType		= rPaperDefs.m_nType;

				item = nodeMap->getNamedItem(L"Grain");
				strPaperGrain = (item) ? (CString)item->getNodeValue() : _T("");
				if (strPaperGrain.CompareNoCase(_T("Vertical")) == 0)
					rFlatProduct.m_nPaperGrain = GRAIN_VERTICAL;
				else
				if (strPaperGrain.CompareNoCase(_T("Horizontal")) == 0)
					rFlatProduct.m_nPaperGrain = GRAIN_HORIZONTAL;
				else
					rFlatProduct.m_nPaperGrain = GRAIN_EITHER;
			}
		}

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessFlatProductQuantityNode(DOMNode* flatProductLayoutNode, CFlatProduct& rFlatProduct)
{
	DOMNamedNodeMap* nodeMap = flatProductLayoutNode->getAttributes();
	if (nodeMap)
	{
		int nDesiredQuantity;
		DOMNode* item = nodeMap->getNamedItem(L"Desired");
		nDesiredQuantity = (item) ? (int)atof((CString)item->getNodeValue()) : 0;
		rFlatProduct.m_nDesiredQuantity = nDesiredQuantity;
	}

	return TRUE;
}

BOOL CXMLInput::ProcessFlatProductDeliveryNode(DOMNode* flatProductLayoutNode, CFlatProduct& rFlatProduct)
{
	DOMNamedNodeMap* nodeMap = flatProductLayoutNode->getAttributes();
	if (nodeMap)
	{
		CString strDeadline;
		DOMNode* item = nodeMap->getNamedItem(L"Deadline");
		strDeadline = (item) ? (CString)item->getNodeValue() : _T("");
		rFlatProduct.m_printDeadline.ParseDateTime(strDeadline, VAR_DATEVALUEONLY);
	}

	return TRUE;
}

BOOL CXMLInput::ProcessFlatProductContentNode(DOMNode* flatProductLayoutNode, CFlatProduct& rFlatProduct)
{
	rFlatProduct.m_strContentFilePath = flatProductLayoutNode->getTextContent();

	return TRUE;
}

BOOL CXMLInput::ProcessFlatProductColorIntentsNode(DOMNode* flatProductColorIntentsNode, CFlatProduct& rFlatProduct)
{
	rFlatProduct.m_colorantList.RemoveAll();

	CString  strNodeName;
	DOMNode* node = flatProductColorIntentsNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Separation"))
		{
			DOMNamedNodeMap* nodeMap = node->getAttributes();
			if (nodeMap)
			{
				CString strColorName;
				DOMNode* item = nodeMap->getNamedItem(L"Name");
				strColorName = (item) ? (CString)item->getNodeValue() : _T("");
				rFlatProduct.m_colorantList.AddSeparation(strColorName);
			}
		}
		node = node->getNextSibling();
	}

	return TRUE;
}


///////// StrippingSection

BOOL CXMLInput::ProcessStrippingSectionNode(DOMNode* strippingSectionNode)
{
	BOOL	 bResult = FALSE;
	CString  strNodeName;
	DOMNode* node = strippingSectionNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("ImpositionLayout"))
			bResult = ProcessPrintSheetNode(node);
		if (strNodeName == _T("PrintSheet"))
			bResult = ProcessPrintSheetNode(node);

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CXMLInput::ProcessPrintSheetNode(DOMNode* printSheetNode)
{
	BOOL bResult = FALSE;

	CString strID, strPrintSheetNumber, strPrepressProcessRefID, strPrintingProcessRefID, strImpositionProcessRefID;
	DOMNamedNodeMap* nodeMap = printSheetNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		strID = (item) ? (CString)item->getNodeValue() : _T("");
		
		item = nodeMap->getNamedItem(L"Number");
		strPrintSheetNumber = (item) ? (CString)item->getNodeValue() : _T("");
		
		item = nodeMap->getNamedItem(L"PrepressProcessRefID");
		strPrepressProcessRefID = (item) ? (CString)item->getNodeValue() : _T("");
		
		item = nodeMap->getNamedItem(L"PrintingProcessRefID");
		strPrintingProcessRefID = (item) ? (CString)item->getNodeValue() : _T("");
		
		item = nodeMap->getNamedItem(L"ImpositionProcessRefID");
		strImpositionProcessRefID = (item) ? (CString)item->getNodeValue() : _T("");
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	if (pDoc->m_PrintSheetList.GetPrintSheetByID(strID))
		return FALSE;

	CPrepressParams  prepressParams  = GetPrepressProcess(strPrepressProcessRefID);
	CPressParams	 pressParams	 = GetPressProcess(strPrintingProcessRefID);
	CPressDevice*	 pPressDevice	 = theApp.m_PressDeviceList.GetDevice(pressParams.m_pressDevice.m_strName);

	if ( ! pPressDevice && ! pressParams.m_pressDevice.m_strName.IsEmpty() && ! m_bUserNotified_PressDeviceNotDefined)
	{
		CString strMsg;
		AfxFormatString1(strMsg, IDS_PRESSDEVICE_NOTDEFINED, _T("'") + pressParams.m_pressDevice.m_strName + _T("'"));
		KIMOpenLogMessage(strMsg, MB_ICONWARNING);
		m_bUserNotified_PressDeviceNotDefined = TRUE;
	}

	CPrintingProfile printingProfile;
	printingProfile.m_press						  = pressParams;
	printingProfile.m_press.m_pressDevice		  = (pPressDevice) ? *pPressDevice : CPressDevice();
	printingProfile.m_press.m_backSidePressDevice = (pPressDevice) ? *pPressDevice : CPressDevice();

	CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.AddBlankSheet(printingProfile);

	if ( ! pPrintSheet)
		return FALSE;

	pPrintSheet->SetID(strID);
	pPrintSheet->m_strPDFTarget		  = prepressParams.m_strPrintSheetOutputProfile;
	pPrintSheet->m_strNumerationStyle = strPrintSheetNumber;

	CString  strNodeName;
	DOMNode* node = printSheetNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Paper"))
			bResult = ProcessPaperNode(node, pPrintSheet);
		else
		if (strNodeName == _T("Markset"))
			bResult = ProcessMarksetNode(node, pPrintSheet);
		else
		if (strNodeName == _T("FoldSheet"))
			bResult = ProcessFoldSheetNode(node, pPrintSheet);
		else
		if (strNodeName == _T("Flat"))
			bResult = ProcessFlatComponentNode(node, pPrintSheet);

		node = node->getNextSibling();
	}

	CLayout* pLayout = pPrintSheet->GetLayout();
	if (pLayout)
	{
		pLayout->SetTurnStyle(pressParams.m_nTurnStyle);
		pLayout->m_FrontSide.Invalidate();
		pLayout->m_BackSide.Invalidate();
#ifdef KIM_ENGINE
		int nDBPdfOutputID = KimEngineDBSetOutputSheet(theApp.m_nDBCurrentJobID, -1, *pPrintSheet, 0, kimAutoDB::outPDF_idle);
		KimEngineNotifyServer(MSG_OUTPUTPDF_STATUS_CHANGED, nDBPdfOutputID);
#endif
	}
	pPrintSheet->CalcTotalQuantity(TRUE);

	//CImpositionProcess impositionProcess = GetImpositionProcess(strImpositionProcessRefID);

	//switch (impositionProcess.m_nImpositionStyle)
	//{
	//case CImpositionProcess::CutAndStack:
	//			{
	//				pDoc->m_printGroupList.Analyze();
	//				int nNewNumSheets = pLayout->GetMaxNumPrintSheetsBasedOn();
	//				int nOldNumSheets = pLayout->GetNumPrintSheetsBasedOn();
	//				if (nNewNumSheets > nOldNumSheets)
	//				{
	//					pPrintSheet->m_strNumerationStyle = _T("$n");
	//					pPrintSheet->PrepareCutAndStack();
	//					pDoc->m_printGroupList.Analyze();
	//					pLayout->AddPrintSheetInstance(nNewNumSheets - nOldNumSheets);
	//					pDoc->m_printGroupList.Analyze();
	//					pDoc->m_PrintSheetList.ReNumberPrintSheets();
	//				}
	//			}
	//			break;
	//default:	break;
	//}

	return bResult;
}

BOOL CXMLInput::ProcessPaperNode(DOMNode* paperNode, CPrintSheet* pPrintSheet)
{
	BOOL bResult = FALSE;

	DOMNamedNodeMap* nodeMap = paperNode->getAttributes();
	if (nodeMap)
	{
		float   fWidth, fHeight;
		CString strPaperGrain;
		DOMNode* item = nodeMap->getNamedItem(L"SheetWidth");
		fWidth = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
		item = nodeMap->getNamedItem(L"SheetHeight");
		fHeight = (item) ? (float)atof((CString)item->getNodeValue()) : 0.0f;
		item = nodeMap->getNamedItem(L"Grain");
		strPaperGrain = (item) ? (CString)item->getNodeValue() : _T("");

		CLayout* pLayout = pPrintSheet->GetLayout();
		if (pLayout)
		{
			pLayout->m_FrontSide.m_fPaperWidth  = fWidth;
			pLayout->m_FrontSide.m_fPaperHeight = fHeight;
			pLayout->m_BackSide.m_fPaperWidth   = fWidth;
			pLayout->m_BackSide.m_fPaperHeight  = fHeight;
			if (strPaperGrain.CompareNoCase(_T("Vertical")) == 0)
			{
				pLayout->m_FrontSide.m_nPaperGrain = GRAIN_VERTICAL;
				pLayout->m_BackSide.m_nPaperGrain  = GRAIN_VERTICAL;
			}
			else
			if (strPaperGrain.CompareNoCase(_T("Horizontal")) == 0)
			{
				pLayout->m_FrontSide.m_nPaperGrain = GRAIN_HORIZONTAL;
				pLayout->m_BackSide.m_nPaperGrain  = GRAIN_HORIZONTAL;
			}
			else
			{
				pLayout->m_FrontSide.m_nPaperGrain = GRAIN_VERTICAL;
				pLayout->m_BackSide.m_nPaperGrain  = GRAIN_VERTICAL;
			}
			CSheet sheet;
			sheet.m_strSheetName.Format(_T("%.1f x %.1f"), fWidth, fHeight);
			sheet.m_fWidth		 = fWidth;
			sheet.m_fHeight		 = fHeight;
			sheet.m_nPaperGrain  = pLayout->m_FrontSide.m_nPaperGrain;		

			CPrintingProfile& rPrintingProfile = pLayout->GetPrintingProfile();
			rPrintingProfile.m_press.m_sheetData = sheet;

			BOOL bFound = FALSE;
			for (int i = 0; i < rPrintingProfile.m_press.m_availableSheets.GetSize(); i++)
			{
				if (rPrintingProfile.m_press.m_availableSheets[i].m_strSheetName == sheet.m_strSheetName)
					bFound = TRUE;
			}
			if ( ! bFound)
				rPrintingProfile.m_press.m_availableSheets.Add(sheet);
 
			pLayout->m_FrontSide.PositionLayoutSide();
			pLayout->m_BackSide.PositionLayoutSide();
		}
	}

	return bResult;
}

BOOL CXMLInput::ProcessMarksetNode(DOMNode* marksetNode, CPrintSheet* pPrintSheet)
{
	BOOL bResult = FALSE;

	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return bResult;

	CString strMarkset = marksetNode->getTextContent();

	if ( ! strMarkset.IsEmpty())
	{
		CString strMarksDir		   = CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets\\");
		CString strMarksetFullPath = strMarksDir + strMarkset + _T(".mks");
		CMarkList markList; 
		markList.Load(strMarksetFullPath);
		pLayout->m_markList.Append(markList);
	}

	return bResult;
}

BOOL CXMLInput::ProcessFoldSheetNode(DOMNode* foldSheetNode, CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL bResult = FALSE;

	DOMNamedNodeMap* nodeMap = foldSheetNode->getAttributes();
	if ( ! nodeMap)
		return FALSE;

	CString strFoldSheetRefID;
	CString	strProductRefID;
	DOMNode* item = nodeMap->getNamedItem(L"FoldSheetRefID");
	strFoldSheetRefID = (item) ? (CString)item->getNodeValue() : _T("");
	item = nodeMap->getNamedItem(L"ProductRefID");
	strProductRefID= (item) ? (CString)item->getNodeValue() : _T("");

	CBoundProduct&	rBoundProduct = pDoc->m_boundProducts.GetProduct(strProductRefID);
	CFoldSheet*		pFoldSheet	  = rBoundProduct.GetFoldSheetByID(strFoldSheetRefID);
	if ( ! pFoldSheet)
		return FALSE;
 
	CBoundProductPart&	rProductPart	   = pFoldSheet->GetProductPart();
	CTrimmingProcess	orgTrimmingProcess = rProductPart.m_trimmingParams;
	CString  			strSector, strSubSector, strOrientation, strOffset, strBlock, strPosition;
	CString  			strNodeName;
	int		 			nOrientation = ROTATE0, nBlockX = 1, nBlockY = 1;
	float				fOffsetX	 = 0.0f, fOffsetY = 0.0f, fPositionX = 0.0f, fPositionY = 0.0f;
	BOOL	 			bSpread		 = FALSE;
	DOMNode* 			node		 = foldSheetNode->getFirstChild(); 

	BOOL bPlaceByPosition = FALSE;
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Sector"))
			strSector = node->getTextContent();
		else
		if (strNodeName == _T("SubSector"))
			strSubSector = node->getTextContent();
		else
		if (strNodeName == _T("Block"))
		{
			strBlock = node->getTextContent();
			if ( ! strBlock.IsEmpty())
			{
				CString strNX = GetStringToken(strBlock, 0, _T(" "));
				CString strNY = GetStringToken(strBlock, 1, _T(" "));
				nBlockX = atoi(strNX);
				nBlockY = atoi(strNY);
			}
		}
		else
		if (strNodeName == _T("Orientation"))
		{
			strOrientation = node->getTextContent();
			if (strOrientation.CompareNoCase(_T("ROTATE0"))   == 0) nOrientation = ROTATE0;   else
			if (strOrientation.CompareNoCase(_T("ROTATE90"))  == 0) nOrientation = ROTATE90;  else
			if (strOrientation.CompareNoCase(_T("ROTATE180")) == 0) nOrientation = ROTATE180; else
			if (strOrientation.CompareNoCase(_T("ROTATE270")) == 0) nOrientation = ROTATE270; else
			if (strOrientation.CompareNoCase(_T("FLIP0"))	  == 0) nOrientation = FLIP0;	  else
			if (strOrientation.CompareNoCase(_T("FLIP90"))	  == 0) nOrientation = FLIP90;    else
			if (strOrientation.CompareNoCase(_T("FLIP180"))	  == 0) nOrientation = FLIP180;   else
			if (strOrientation.CompareNoCase(_T("FLIP270"))   == 0) nOrientation = FLIP270;    
		}
		else
		if (strNodeName == _T("Offset"))
		{
			strOffset = node->getTextContent();
			if ( ! strOffset.IsEmpty())
			{
				CString strOffsetX = GetStringToken(strOffset, 0, _T(" "));
				CString strOffsetY = GetStringToken(strOffset, 1, _T(" "));
				fOffsetX = (float)atof(strOffsetX);
				fOffsetY = (float)atof(strOffsetY);
			}
		}
		else
		if (strNodeName == _T("Position"))
		{
			strPosition = node->getTextContent();
			if ( ! strPosition.IsEmpty())
			{
				CString strPositionX = GetStringToken(strPosition, 0, _T(" "));
				CString strPositionY = GetStringToken(strPosition, 1, _T(" "));
				fPositionX = (float)atof(strPositionX);
				fPositionY = (float)atof(strPositionY);
			}
			bPlaceByPosition = TRUE;
		}
		else	// eventually override trims
		if (strNodeName == _T("HeadTrim"))
			rProductPart.m_trimmingParams.m_fHeadTrim = (float)atof((CString)node->getTextContent());
		else
		if (strNodeName == _T("FootTrim"))
			rProductPart.m_trimmingParams.m_fFootTrim = (float)atof((CString)node->getTextContent());
		else
		if (strNodeName == _T("SideTrim"))
			rProductPart.m_trimmingParams.m_fSideTrim = (float)atof((CString)node->getTextContent());
		else
		if (strNodeName == _T("FoldSheet"))
			bSpread = ProcessFoldSheetNode(node, pPrintSheet);

		node = node->getNextSibling();
	}

	CLayout* pLayout = pPrintSheet->GetLayout();
	if (pLayout)
	{
		int	  nLayerIndex = 0;
		float fGrossWidth, fGrossHeight;
		if ( (nOrientation == ROTATE90) || (nOrientation == ROTATE270) || (nOrientation == FLIP90) || (nOrientation == FLIP270) )
			pFoldSheet->GetCutSize(nLayerIndex, fGrossHeight, fGrossWidth);
		else
			pFoldSheet->GetCutSize(nLayerIndex, fGrossWidth,  fGrossHeight);

		float fFoldSheetPosX, fFoldSheetPosY;
		if (bPlaceByPosition)
		{
			fFoldSheetPosX = pLayout->m_FrontSide.m_fPaperLeft   + fPositionX;
			fFoldSheetPosY = pLayout->m_FrontSide.m_fPaperBottom + fPositionY;
		}
		else
		{
			PrintSheetGetFoldSheetPlacement(pLayout, fGrossWidth * nBlockX, fGrossHeight * nBlockY, strSector, strSubSector, strOrientation, fFoldSheetPosX, fFoldSheetPosY);
			fFoldSheetPosX += fOffsetX;
			fFoldSheetPosY += fOffsetY;
		}

		if (bSpread)	// place this foldsheet around all existing ones by spreading it along the vertical axis
		{
			int	  nSpreadAxis	= VERTICAL;
			int	  nSpreadIndex	= -1;
			float fSpreadGap	= 0.0f;
			float fLeft, fRight, fBottom, fTop, fFoldSheetLeftPart, fFoldSheetRightPart;
			pLayout->m_FrontSide.CalcBoundingBoxAll(&fLeft, &fBottom, &fRight, &fTop, TRUE, pPrintSheet, FRONTSIDE);
			fSpreadGap	   = fRight - fLeft;
			nSpreadIndex   = pFoldSheet->SpreadLayer(nLayerIndex, nOrientation, nSpreadAxis, fFoldSheetLeftPart, fFoldSheetRightPart);
			fFoldSheetPosX = fLeft - fFoldSheetLeftPart;

			CFoldSheetLayerRef layerRef((short)pDoc->m_Bookblock.GetFoldSheetIndex(pFoldSheet), (short)nLayerIndex);
			pPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);
			PrintSheetPlaceFoldSheetLayer(pLayout, pFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, nOrientation, pFoldSheet->GetProductPart(), nSpreadAxis, fSpreadGap, nSpreadIndex);
		}
		else
		{
			float fLeft	= fFoldSheetPosX;
			for (int nY = 0; nY < nBlockY; nY++)
			{
				for (int nX = 0; nX < nBlockX; nX++)
				{
					CFoldSheetLayerRef layerRef((short)pDoc->m_Bookblock.GetFoldSheetIndex(pFoldSheet), (short)nLayerIndex);
					pPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);
					PrintSheetPlaceFoldSheetLayer(pLayout, pFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, nOrientation, pFoldSheet->GetProductPart());
					fFoldSheetPosX += fGrossWidth;
					bResult = TRUE;
				}
				fFoldSheetPosX = fLeft;
				fFoldSheetPosY += fGrossHeight;
			}
		}
	}

	rProductPart.m_trimmingParams = orgTrimmingProcess;	// restore original trims

	return bResult;
}

void CXMLInput::PrintSheetGetFoldSheetPlacement(CLayout* pLayout, float fGrossWidth, float fGrossHeight, const CString strSector, const CString strSubSector, const CString strOrientation, float& fFoldSheetPosX, float& fFoldSheetPosY)
{
	fFoldSheetPosX = 0.0f; fFoldSheetPosY = 0.0f;
	if ( ! pLayout)
		return;
	if (strSector.IsEmpty())
		return;

	CPressDevice& rPressDevice = pLayout->GetPrintingProfile().m_press.m_pressDevice;
	float fGripper = 0.0f; //rPressDevice.m_fGripper
	float fSectorWidth  = pLayout->m_FrontSide.m_fPaperWidth  / 4.0f;
	float fSectorHeight = (pLayout->m_FrontSide.m_fPaperHeight - fGripper) / 2.0f;
	float fSectorBoxLeft = FLT_MAX, fSectorBoxBottom = FLT_MAX, fSectorBoxRight = -FLT_MAX, fSectorBoxTop = -FLT_MAX;

	int		nCurStringPos = 0;
	CString strToken = strSector.Tokenize(_T(" "), nCurStringPos);
	while ( ! strToken.IsEmpty())
	{
		strToken.TrimLeft(); strToken.TrimRight();

		int nIndexX = 0, nIndexY = 0;
		if (strToken.CompareNoCase(_T("A")) == 0) { nIndexX = 0; nIndexY = 0; } else
		if (strToken.CompareNoCase(_T("B")) == 0) { nIndexX = 1; nIndexY = 0; } else
		if (strToken.CompareNoCase(_T("C")) == 0) { nIndexX = 2; nIndexY = 0; } else
		if (strToken.CompareNoCase(_T("D")) == 0) { nIndexX = 3; nIndexY = 0; } else
		if (strToken.CompareNoCase(_T("E")) == 0) { nIndexX = 0; nIndexY = 1; } else
		if (strToken.CompareNoCase(_T("F")) == 0) { nIndexX = 1; nIndexY = 1; } else
		if (strToken.CompareNoCase(_T("G")) == 0) { nIndexX = 2; nIndexY = 1; } else
		if (strToken.CompareNoCase(_T("H")) == 0) { nIndexX = 3; nIndexY = 1; } else
		return;

		fSectorBoxLeft	 = min(fSectorBoxLeft,   pLayout->m_FrontSide.m_fPaperLeft   + nIndexX * fSectorWidth);
		fSectorBoxBottom = min(fSectorBoxBottom, pLayout->m_FrontSide.m_fPaperBottom + fGripper + nIndexY * fSectorHeight);
		fSectorBoxRight  = max(fSectorBoxRight,  pLayout->m_FrontSide.m_fPaperLeft   + (nIndexX + 1) * fSectorWidth);
		fSectorBoxTop	 = max(fSectorBoxTop,	 pLayout->m_FrontSide.m_fPaperBottom + fGripper + (nIndexY + 1) * fSectorHeight);

		strToken = strSector.Tokenize(_T(" "), nCurStringPos);
	}
	float fSectorBoxWidth  = fSectorBoxRight - fSectorBoxLeft;
	float fSectorBoxHeight = fSectorBoxTop   - fSectorBoxBottom;

	if (strSubSector.IsEmpty())	// if no sub sector -> place centered into sector
	{
		fFoldSheetPosX = fSectorBoxLeft   + (fSectorBoxWidth  - fGrossWidth)  / 2.0f;
		fFoldSheetPosY = fSectorBoxBottom + (fSectorBoxHeight - fGrossHeight) / 2.0f;
	}
	else						// -> place centered to sub sector inside sector
	{
		float fSubSectorWidth   = fSectorBoxWidth  / 2.0f;
		float fSubSectorHeight  = fSectorBoxHeight / 2.0f;
		float fSubSectorBoxLeft = FLT_MAX, fSubSectorBoxBottom = FLT_MAX, fSubSectorBoxRight = -FLT_MAX, fSubSectorBoxTop = -FLT_MAX;

		int	  nCurStringPos = 0;
		strToken = strSubSector.Tokenize(_T(" "), nCurStringPos);
		while ( ! strToken.IsEmpty())
		{
			strToken.TrimLeft(); strToken.TrimRight();

			int nIndexX = 0, nIndexY = 0;
			if (strToken.CompareNoCase(_T("1")) == 0) { nIndexX = 0; nIndexY = 0; } else
			if (strToken.CompareNoCase(_T("2")) == 0) { nIndexX = 1; nIndexY = 0; } else
			if (strToken.CompareNoCase(_T("3")) == 0) { nIndexX = 0; nIndexY = 1; } else
			if (strToken.CompareNoCase(_T("4")) == 0) { nIndexX = 1; nIndexY = 1; } else
			return;

			fSubSectorBoxLeft	= min(fSubSectorBoxLeft,   fSectorBoxLeft   + nIndexX * fSubSectorWidth);
			fSubSectorBoxBottom = min(fSubSectorBoxBottom, fSectorBoxBottom + nIndexY * fSubSectorHeight);
			fSubSectorBoxRight  = max(fSubSectorBoxRight,  fSectorBoxLeft   + (nIndexX + 1) * fSubSectorWidth);
			fSubSectorBoxTop	= max(fSubSectorBoxTop,	   fSectorBoxBottom + (nIndexY + 1) * fSubSectorHeight);

			strToken = strSubSector.Tokenize(_T(" "), nCurStringPos);
		}
		float fSubSectorBoxWidth  = fSubSectorBoxRight - fSubSectorBoxLeft;
		float fSubSectorBoxHeight = fSubSectorBoxTop   - fSubSectorBoxBottom;

		fFoldSheetPosX	 = fSubSectorBoxLeft   + (fSubSectorBoxWidth  - fGrossWidth)  / 2.0f;
		fFoldSheetPosY	 = fSubSectorBoxBottom + (fSubSectorBoxHeight - fGrossHeight) / 2.0f;
	}
}

BOOL CXMLInput::PrintSheetPlaceFoldSheetLayer(CLayout* pLayout, CFoldSheet* pFoldSheet, int nLayerIndex, float fFoldSheetPosX, float fFoldSheetPosY, int nOrientation, CBoundProductPart& rProductPart, int nSpreadAxis, float fSpreadGap, int nSpreadIndex)
{
	CFoldSheetLayer* pLayer = pFoldSheet->GetLayer(nLayerIndex);
	if ( ! pLayer)
		return FALSE;
	if ( ! pLayer->m_HeadPositionArray.GetSize())
		return FALSE;

	CString	strPage;
	int		nPageIndex			= -1;
	int		nComponentRefIndex	= pLayout->m_FoldSheetLayerRotation.GetSize();
	int		nNumX 				= pLayer->m_nPagesX;
	int		nNumY 				= pLayer->m_nPagesY;
	float	fLeft 				= fFoldSheetPosX, fBottom = fFoldSheetPosY;
	for (int ny = 0; ny < nNumY; ny++)
	{
		float fPageRowHeight = 0.0f;
		for (int nx = 0; nx < nNumX; nx++)
		{
			nPageIndex++;

			float fPageBoxWidth, fPageBoxHeight, fPageLeft, fPageBottom, fPageRight, fPageTop;
			pFoldSheet->GetPageCutBox(pLayer, nPageIndex, rProductPart, fPageBoxWidth, fPageBoxHeight, fPageLeft, fPageBottom, fPageRight, fPageTop);

			float fPageWidth  = rProductPart.m_fPageWidth;
			float fPageHeight = rProductPart.m_fPageHeight;
			if ( (pLayer->m_HeadPositionArray[nPageIndex] == LEFT) || (pLayer->m_HeadPositionArray[nPageIndex] == RIGHT) )
			{
				float fTemp = fPageWidth; fPageWidth = fPageHeight; fPageHeight = fTemp;
			}

			///// frontside
			strPage.Format("%d", pLayer->m_FrontPageArray[nPageIndex]);
			CLayoutObject layoutObjectFront;
			layoutObjectFront.CreatePage(fLeft + fPageLeft, fBottom + fPageBottom, fPageWidth, fPageHeight, strPage, rProductPart.m_strFormatName, (BYTE)rProductPart.m_nPageOrientation, 
										 *pLayer, (BYTE)nPageIndex, (BYTE)nComponentRefIndex, (BYTE)FRONTSIDE);

			if (nSpreadIndex != -1)
			{
				if (nSpreadAxis == VERTICAL)
				{
					if (nx > nSpreadIndex)
						layoutObjectFront.Offset(fSpreadGap,  0.0f);
				}
				else
					if (ny > nSpreadIndex)
						layoutObjectFront.Offset(0.0f, fSpreadGap);
			}

			fLeft += fPageBoxWidth;
			fPageRowHeight = max(fPageRowHeight, fPageBoxHeight);

			pLayout->m_FrontSide.m_ObjectList.AddTail(layoutObjectFront);

			///// backside
			strPage.Format("%d", pLayer->m_BackPageArray[nPageIndex]);
			CLayoutObject layoutObjectBack;
			layoutObjectBack.CreatePage(fLeft + fPageLeft, fBottom + fPageBottom, fPageWidth, fPageHeight, strPage, rProductPart.m_strFormatName, (BYTE)rProductPart.m_nPageOrientation, 
										*pLayer, (BYTE)nPageIndex, (BYTE)nComponentRefIndex, (BYTE)BACKSIDE);

			pLayout->m_BackSide.m_ObjectList.AddTail(layoutObjectBack);
			pLayout->m_BackSide.m_ObjectList.GetTail().AlignToCounterPart(&pLayout->m_FrontSide.m_ObjectList.GetTail());
		}

		fLeft	 = fFoldSheetPosX;
		fBottom += fPageRowHeight;
	}

	pLayout->m_FoldSheetLayerRotation.Add(0);
	pLayout->m_FoldSheetLayerTurned.Add(FALSE);

	switch (nOrientation)
	{
	case ROTATE0:	break;

	case ROTATE90:	pLayout->RotateFoldSheet90(nComponentRefIndex);			break;

	case ROTATE180: pLayout->RotateFoldSheet90(nComponentRefIndex); 
					pLayout->RotateFoldSheet90(nComponentRefIndex);			break;

	case ROTATE270: pLayout->RotateFoldSheet90(nComponentRefIndex, TRUE);	break;

	case FLIP0:		pLayout->TurnFoldSheet	  (nComponentRefIndex);
					pLayout->RotateFoldSheet90(nComponentRefIndex);
					pLayout->RotateFoldSheet90(nComponentRefIndex);			break;

	case FLIP90:	pLayout->RotateFoldSheet90(nComponentRefIndex);
					pLayout->TurnFoldSheet	  (nComponentRefIndex);
					pLayout->RotateFoldSheet90(nComponentRefIndex);
					pLayout->RotateFoldSheet90(nComponentRefIndex);			break;

	case FLIP180:	pLayout->TurnFoldSheet	  (nComponentRefIndex);			break;

	case FLIP270:	pLayout->RotateFoldSheet90(nComponentRefIndex);
					pLayout->TurnFoldSheet	  (nComponentRefIndex);			break;
	}

	pLayout->m_FrontSide.FindCutBlocks();
	pLayout->m_FrontSide.AnalyseCutblocks();
	pLayout->m_BackSide.FindCutBlocks();
	pLayout->m_BackSide.AnalyseCutblocks();

	return TRUE;
}

BOOL CXMLInput::ProcessFlatComponentNode(DOMNode* flatComponentNode, CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL bResult = FALSE;

	DOMNamedNodeMap* nodeMap = flatComponentNode->getAttributes();
	if ( ! nodeMap)
		return FALSE;

	CString	strProductRefID;
	DOMNode* item = nodeMap->getNamedItem(L"ProductRefID");
	strProductRefID= (item) ? (CString)item->getNodeValue() : _T("");

	CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetProduct(strProductRefID);
	if (rFlatProduct.IsNull())
		return FALSE;
 
	CString 	strSector, strSubSector, strOrientation, strOffset, strBlock, strPosition;
	CString 	strNodeName;
	int			nOrientation = ROTATE0, nBlockX = 1, nBlockY = 1;
	float		fOffsetX	 = 0.0f, fOffsetY	= 0.0f;
	float		fPositionX	 = 0.0f, fPositionY = 0.0f;
	DOMNode*	node		 = flatComponentNode->getFirstChild(); 

	BOOL bPlaceByPosition = FALSE;
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Sector"))
			strSector = node->getTextContent();
		else
		if (strNodeName == _T("SubSector"))
			strSubSector = node->getTextContent();
		else
		if (strNodeName == _T("Block"))
		{
			strBlock = node->getTextContent();
			if ( ! strBlock.IsEmpty())
			{
				CString strNX = GetStringToken(strBlock, 0, _T(" "));
				CString strNY = GetStringToken(strBlock, 1, _T(" "));
				nBlockX = atoi(strNX);
				nBlockY = atoi(strNY);
			}
		}
		else
		if (strNodeName == _T("Orientation"))
		{
			strOrientation = node->getTextContent();
			if (strOrientation.CompareNoCase(_T("ROTATE0"))   == 0) nOrientation = ROTATE0;   else
			if (strOrientation.CompareNoCase(_T("ROTATE90"))  == 0) nOrientation = ROTATE90;  else
			if (strOrientation.CompareNoCase(_T("ROTATE180")) == 0) nOrientation = ROTATE180; else
			if (strOrientation.CompareNoCase(_T("ROTATE270")) == 0) nOrientation = ROTATE270; else
			if (strOrientation.CompareNoCase(_T("FLIP0"))	  == 0) nOrientation = FLIP0;	  else
			if (strOrientation.CompareNoCase(_T("FLIP90"))	  == 0) nOrientation = FLIP90;    else
			if (strOrientation.CompareNoCase(_T("FLIP180"))	  == 0) nOrientation = FLIP180;   else
			if (strOrientation.CompareNoCase(_T("FLIP270"))   == 0) nOrientation = FLIP270;    
		}
		else
		if (strNodeName == _T("Offset"))
		{
			strOffset = node->getTextContent();
			if ( ! strOffset.IsEmpty())
			{
				CString strOffsetX = GetStringToken(strOffset, 0, _T(" "));
				CString strOffsetY = GetStringToken(strOffset, 1, _T(" "));
				fOffsetX = (float)atof(strOffsetX);
				fOffsetY = (float)atof(strOffsetY);
			}
		}
		else
		if (strNodeName == _T("Position"))
		{
			strPosition = node->getTextContent();
			if ( ! strPosition.IsEmpty())
			{
				CString strPositionX = GetStringToken(strPosition, 0, _T(" "));
				CString strPositionY = GetStringToken(strPosition, 1, _T(" "));
				fPositionX = (float)atof(strPositionX);
				fPositionY = (float)atof(strPositionY);
			}
			bPlaceByPosition = TRUE;
		}

		node = node->getNextSibling();
	}

	CLayout* pLayout = pPrintSheet->GetLayout();
	if (pLayout)
	{
		float fGrossWidth, fGrossHeight;
		if ( (nOrientation == ROTATE90) || (nOrientation == ROTATE270) || (nOrientation == FLIP90) || (nOrientation == FLIP270) )
			rFlatProduct.GetCutSize(fGrossHeight, fGrossWidth);
		else
			rFlatProduct.GetCutSize(fGrossWidth,  fGrossHeight);

		float fFlatPosX, fFlatPosY;
		if (bPlaceByPosition)
		{
			fFlatPosX = pLayout->m_FrontSide.m_fPaperLeft   + fPositionX;
			fFlatPosY = pLayout->m_FrontSide.m_fPaperBottom + fPositionY;
		}
		else
		{
			PrintSheetGetFlatComponentPlacement(pLayout, fGrossWidth * nBlockX, fGrossHeight * nBlockY, strSector, strSubSector, strOrientation, fFlatPosX, fFlatPosY);
			fFlatPosX += fOffsetX;
			fFlatPosY += fOffsetY;
		}

		float fLeft	= fFlatPosX;
		for (int nY = 0; nY < nBlockY; nY++)
		{
			for (int nX = 0; nX < nBlockX; nX++)
			{
				PrintSheetPlaceFlatComponent(pPrintSheet, rFlatProduct, fFlatPosX, fFlatPosY, nOrientation);
				fFlatPosX += fGrossWidth;
				bResult = TRUE;
			}
			fFlatPosX = fLeft;
			fFlatPosY += fGrossHeight;
		}
	}

	return bResult;
}

void CXMLInput::PrintSheetGetFlatComponentPlacement(CLayout* pLayout, float fGrossWidth, float fGrossHeight, const CString strSector, const CString strSubSector, const CString strOrientation, float& fFlatPosX, float& fFlatPosY)
{
	fFlatPosX = 0.0f; fFlatPosY = 0.0f;
	if ( ! pLayout)
		return;
	if (strSector.IsEmpty())
		return;

	CPressDevice& rPressDevice = pLayout->GetPrintingProfile().m_press.m_pressDevice;
	float fGripper = 0.0f; //rPressDevice.m_fGripper
	float fSectorWidth  = pLayout->m_FrontSide.m_fPaperWidth  / 4.0f;
	float fSectorHeight = (pLayout->m_FrontSide.m_fPaperHeight - fGripper) / 2.0f;
	float fSectorBoxLeft = FLT_MAX, fSectorBoxBottom = FLT_MAX, fSectorBoxRight = -FLT_MAX, fSectorBoxTop = -FLT_MAX;

	int		nCurStringPos = 0;
	CString strToken = strSector.Tokenize(_T(" "), nCurStringPos);
	while ( ! strToken.IsEmpty())
	{
		strToken.TrimLeft(); strToken.TrimRight();

		int nIndexX = 0, nIndexY = 0;
		if (strToken.CompareNoCase(_T("A")) == 0) { nIndexX = 0; nIndexY = 0; } else
		if (strToken.CompareNoCase(_T("B")) == 0) { nIndexX = 1; nIndexY = 0; } else
		if (strToken.CompareNoCase(_T("C")) == 0) { nIndexX = 2; nIndexY = 0; } else
		if (strToken.CompareNoCase(_T("D")) == 0) { nIndexX = 3; nIndexY = 0; } else
		if (strToken.CompareNoCase(_T("E")) == 0) { nIndexX = 0; nIndexY = 1; } else
		if (strToken.CompareNoCase(_T("F")) == 0) { nIndexX = 1; nIndexY = 1; } else
		if (strToken.CompareNoCase(_T("G")) == 0) { nIndexX = 2; nIndexY = 1; } else
		if (strToken.CompareNoCase(_T("H")) == 0) { nIndexX = 3; nIndexY = 1; } else
		return;

		fSectorBoxLeft	 = min(fSectorBoxLeft,   pLayout->m_FrontSide.m_fPaperLeft   + nIndexX * fSectorWidth);
		fSectorBoxBottom = min(fSectorBoxBottom, pLayout->m_FrontSide.m_fPaperBottom + fGripper + nIndexY * fSectorHeight);
		fSectorBoxRight  = max(fSectorBoxRight,  pLayout->m_FrontSide.m_fPaperLeft   + (nIndexX + 1) * fSectorWidth);
		fSectorBoxTop	 = max(fSectorBoxTop,	 pLayout->m_FrontSide.m_fPaperBottom + fGripper + (nIndexY + 1) * fSectorHeight);

		strToken = strSector.Tokenize(_T(" "), nCurStringPos);
	}
	float fSectorBoxWidth  = fSectorBoxRight - fSectorBoxLeft;
	float fSectorBoxHeight = fSectorBoxTop   - fSectorBoxBottom;

	if (strSubSector.IsEmpty())	// if no sub sector -> place centered into sector
	{
		fFlatPosX = fSectorBoxLeft   + (fSectorBoxWidth  - fGrossWidth)  / 2.0f;
		fFlatPosY = fSectorBoxBottom + (fSectorBoxHeight - fGrossHeight) / 2.0f;
	}
	else						// -> place centered to sub sector inside sector
	{
		float fSubSectorWidth   = fSectorBoxWidth  / 2.0f;
		float fSubSectorHeight  = fSectorBoxHeight / 2.0f;
		float fSubSectorBoxLeft = FLT_MAX, fSubSectorBoxBottom = FLT_MAX, fSubSectorBoxRight = -FLT_MAX, fSubSectorBoxTop = -FLT_MAX;

		int	  nCurStringPos = 0;
		strToken = strSubSector.Tokenize(_T(" "), nCurStringPos);
		while ( ! strToken.IsEmpty())
		{
			strToken.TrimLeft(); strToken.TrimRight();

			int nIndexX = 0, nIndexY = 0;
			if (strToken.CompareNoCase(_T("1")) == 0) { nIndexX = 0; nIndexY = 0; } else
			if (strToken.CompareNoCase(_T("2")) == 0) { nIndexX = 1; nIndexY = 0; } else
			if (strToken.CompareNoCase(_T("3")) == 0) { nIndexX = 0; nIndexY = 1; } else
			if (strToken.CompareNoCase(_T("4")) == 0) { nIndexX = 1; nIndexY = 1; } else
			return;

			fSubSectorBoxLeft	= min(fSubSectorBoxLeft,   fSectorBoxLeft   + nIndexX * fSubSectorWidth);
			fSubSectorBoxBottom = min(fSubSectorBoxBottom, fSectorBoxBottom + nIndexY * fSubSectorHeight);
			fSubSectorBoxRight  = max(fSubSectorBoxRight,  fSectorBoxLeft   + (nIndexX + 1) * fSubSectorWidth);
			fSubSectorBoxTop	= max(fSubSectorBoxTop,	   fSectorBoxBottom + (nIndexY + 1) * fSubSectorHeight);

			strToken = strSubSector.Tokenize(_T(" "), nCurStringPos);
		}
		float fSubSectorBoxWidth  = fSubSectorBoxRight - fSubSectorBoxLeft;
		float fSubSectorBoxHeight = fSubSectorBoxTop   - fSubSectorBoxBottom;

		fFlatPosX = fSubSectorBoxLeft   + (fSubSectorBoxWidth  - fGrossWidth)  / 2.0f;
		fFlatPosY = fSubSectorBoxBottom + (fSubSectorBoxHeight - fGrossHeight) / 2.0f;
	}
}

BOOL CXMLInput::PrintSheetPlaceFlatComponent(CPrintSheet* pPrintSheet, CFlatProduct& rFlatProduct, float fFlatPosX, float fFlatPosY, int nOrientation)
{
	if ( ! pPrintSheet)
		return FALSE;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return FALSE;

	int	nFlatProductIndex		= rFlatProduct.GetIndex(); 
	int	nFlatProductRefIndex	= pPrintSheet->MakeFlatProductRefIndex(nFlatProductIndex);

	int	 nHeadPosition = TOP;
	BOOL bFlip		   = FALSE;
	switch (nOrientation)
	{
	case ROTATE0:							break;
	case ROTATE90:	nHeadPosition = LEFT;	break;
	case ROTATE180:	nHeadPosition = BOTTOM;	break;
	case ROTATE270: nHeadPosition = RIGHT;	break;
	case FLIP0:		nHeadPosition = BOTTOM;	bFlip = TRUE; break;
	case FLIP90:	nHeadPosition = LEFT;	bFlip = TRUE; break;
	case FLIP180:	nHeadPosition = TOP;	bFlip = TRUE; break;
	case FLIP270:	nHeadPosition = RIGHT;	bFlip = TRUE; break;
	}
	
	BOOL bHasBackSide = (rFlatProduct.m_nNumPages > 1) ? TRUE : FALSE;

	float fWidth  = ( (nHeadPosition == TOP) ||  (nHeadPosition == BOTTOM) ) ? rFlatProduct.m_fTrimSizeWidth  : rFlatProduct.m_fTrimSizeHeight;
	float fHeight = ( (nHeadPosition == TOP) ||  (nHeadPosition == BOTTOM) ) ? rFlatProduct.m_fTrimSizeHeight : rFlatProduct.m_fTrimSizeWidth;
	float fLeftBorder = 0.0f, fBottomBorder = 0.0f; 
	switch (nHeadPosition)
	{
	case TOP:		fLeftBorder	  = rFlatProduct.m_fLeftTrim;
					fBottomBorder = rFlatProduct.m_fBottomTrim;
					break;
	case RIGHT:		fLeftBorder	  = rFlatProduct.m_fBottomTrim;
					fBottomBorder = rFlatProduct.m_fRightTrim;
					break;
	case BOTTOM:	fLeftBorder	  = rFlatProduct.m_fRightTrim;
					fBottomBorder = rFlatProduct.m_fTopTrim;
					break;
	case LEFT:		fLeftBorder	  = rFlatProduct.m_fTopTrim;
					fBottomBorder = rFlatProduct.m_fLeftTrim;
					break;
	}

	float fXPos = fFlatPosX + fLeftBorder;
	float fYPos = fFlatPosY + fBottomBorder;
	CLayoutObject layoutObject; 
	layoutObject.CreateFlatProduct(fXPos, fYPos, fWidth, fHeight, nHeadPosition, nFlatProductRefIndex);
	pLayout->m_FrontSide.m_ObjectList.AddTail(layoutObject);
	CLayoutObject* pFrontLayoutObject = &pLayout->m_FrontSide.m_ObjectList.GetTail(); 
	if (bFlip)
		pFrontLayoutObject->m_bObjectTurned = TRUE;

	layoutObject.CreateFlatProduct(fXPos, fYPos, fWidth, fHeight, nHeadPosition, nFlatProductRefIndex);
	pLayout->m_BackSide.m_ObjectList.AddTail(layoutObject);
	CLayoutObject* pBackLayoutObject = &pLayout->m_BackSide.m_ObjectList.GetTail();
	if (bFlip)
		pBackLayoutObject->m_bObjectTurned = TRUE;

	pBackLayoutObject->AlignToCounterPart(pFrontLayoutObject);

	return TRUE;
}

int CXMLInput::ReplicatePDFFlats(CFlatProduct& rMasterProduct, CPageSource* pNewObjectSource, int nNumPDFPages, int nSides)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return ALL_PARTS;

	if (rMasterProduct.m_strProductName.Find(_T("$n")) < 0)
		rMasterProduct.m_strProductName += _T("-$n");

	CFlatProductList flatProducts;
	int		nMasterID = atoi(rMasterProduct.GetProductID());
	CString strID;
	int		nNumProducts = nNumPDFPages / nSides - 1;	// -1 because 1 product already exists
	int		nStep		 = (nSides == 1) ? 1 : 2;
	int		nNameIndex	 = 2;
	for (int i = 0; i < pNewObjectSource->m_PageSourceHeaders.GetSize() - nSides; i = i + nStep, nNameIndex++)
	{
		float fTrimSizeWidth = rMasterProduct.m_fTrimSizeWidth, fTrimSizeHeight = rMasterProduct.m_fTrimSizeHeight;
		if ( (fabs(fTrimSizeWidth) <= 0.001f) || (fabs(fTrimSizeHeight) <= 0.001f) )
		{
			if (pNewObjectSource->m_PageSourceHeaders[i].HasTrimBox())
			{
				fTrimSizeWidth  = pNewObjectSource->m_PageSourceHeaders[i].m_fTrimBoxRight  - pNewObjectSource->m_PageSourceHeaders[i].m_fTrimBoxLeft;
				fTrimSizeHeight = pNewObjectSource->m_PageSourceHeaders[i].m_fTrimBoxTop	- pNewObjectSource->m_PageSourceHeaders[i].m_fTrimBoxBottom;
			}
			else
			{
				fTrimSizeWidth  = pNewObjectSource->m_PageSourceHeaders[i].m_fBBoxRight	- pNewObjectSource->m_PageSourceHeaders[i].m_fBBoxLeft;
				fTrimSizeHeight = pNewObjectSource->m_PageSourceHeaders[i].m_fBBoxTop	- pNewObjectSource->m_PageSourceHeaders[i].m_fBBoxBottom;
			}
			switch (theApp.settings.m_iUnit)
			{
			case MILLIMETER:	fTrimSizeWidth  = (long)(fTrimSizeWidth  * 10.0f + 0.5f) / 10.0f;	// round to 1 digit after decimal
								fTrimSizeHeight = (long)(fTrimSizeHeight * 10.0f + 0.5f) / 10.0f;
								break;
			case CENTIMETER:	
			case INCH:			fTrimSizeWidth  = (long)(fTrimSizeWidth  * 100.0f + 0.5f) / 100.0f;	// round to 2 digits after decimal
								fTrimSizeHeight = (long)(fTrimSizeHeight * 100.0f + 0.5f) / 100.0f;
								break;
			}
			rMasterProduct.m_fTrimSizeWidth = fTrimSizeWidth; rMasterProduct.m_fTrimSizeHeight = fTrimSizeHeight;
		}

		CFlatProduct flatProduct = rMasterProduct;
		strID.Format(_T("%d"), nMasterID + nNameIndex - 1);
		flatProduct.SetProductID(strID);
		flatProduct.m_strProductName	= CDlgNumeration::GenerateNumber(nNameIndex, rMasterProduct.m_strProductName, 0);
		flatProduct.m_nNumPages			= nSides;
		flatProduct.m_fTrimSizeWidth	= fTrimSizeWidth;	
		flatProduct.m_fTrimSizeHeight	= fTrimSizeHeight;

		flatProduct.m_colorantList.RemoveAll();
		for (int i = 0; i < pNewObjectSource->m_PageSourceHeaders[0].m_processColors.GetSize(); i++)
		{
			CColorant colorant;
			colorant.m_strName = pNewObjectSource->m_PageSourceHeaders[0].m_processColors[i];
			colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
			flatProduct.m_colorantList.Add(colorant);
		}
		for (int i = 0; i < pNewObjectSource->m_PageSourceHeaders[0].m_spotColors.GetSize(); i++)
		{
			CColorant colorant;
			colorant.m_strName = pNewObjectSource->m_PageSourceHeaders[0].m_spotColors[i];
			colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
			flatProduct.m_colorantList.Add(colorant);
		}

		flatProducts.AddTail(flatProduct);
	}

	rMasterProduct.m_strProductName = CDlgNumeration::GenerateNumber(1, rMasterProduct.m_strProductName, 0);

	POSITION pos = flatProducts.GetHeadPosition();
	while (pos)
	{
		pDoc->m_flatProducts.AddTail(flatProducts.GetNext(pos));
	}

	int nProductPartIndex = pDoc->GetFlatProductsIndex();
	pDoc->m_PageTemplateList.SynchronizeToFlatProductList(pDoc->m_flatProducts, nProductPartIndex);
	pDoc->m_PageTemplateList.FindPrintSheetLocations();

	return nProductPartIndex;
}

BOOL CXMLInput::AnyContentFileExisting()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		if ( ! rBoundProduct.m_strContentFilePath.IsEmpty())
			if (PathFileExists(rBoundProduct.m_strContentFilePath))
				return TRUE;
	}
	pos = pDoc->m_flatProducts.GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);

		if ( ! rFlatProduct.m_strContentFilePath.IsEmpty())
			if (PathFileExists(rFlatProduct.m_strContentFilePath))
				return TRUE;
	}

	return FALSE;
}

CPaperDefs CXMLInput::GetPaperResource  (const CString& strID)
{
	return m_productionSetup.GetPaperResource(strID);
}

CPrepressParams	CXMLInput::GetPrepressProcess(const CString& strID)
{
	return m_productionSetup.GetPrepressProcess(strID);
}

CPressParams CXMLInput::GetPressProcess	  (const CString& strID)
{
	return m_productionSetup.GetPressProcess(strID);
}

CCuttingProcess	CXMLInput::GetCuttingProcess (const CString& strID)
{
	return m_productionSetup.GetCuttingProcess(strID);
}

CFoldingProcess	CXMLInput::GetFoldingProcess (const CString& strID)
{
	return m_productionSetup.GetFoldingProcess(strID);
}

CBindingProcess	CXMLInput::GetBindingProcess (const CString& strID)
{
	return m_productionSetup.GetBindingProcess(strID);
}

CTrimmingProcess CXMLInput::GetTrimmingProcess(const CString& strID)
{
	return m_productionSetup.GetTrimmingProcess(strID);
}
