// PageListDetailView.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#ifdef PDF
#include "PDFReader.h"
#include "PDFEngineInterface.h"
#endif
#include "PageListDetailView.h"
#include "PageListView.h"
#include "PageListFrame.h"
#include "DlgJobColorDefinitions.h"	// Needed for ColorListBox definition in DlgPageTemplateProps
#include "DlgPageTemplateProps.h"	// Needed for destroying the dialog if docsize is empty
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "MyRectTracker.h"
#include "GraphicComponent.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPageListDetailView

IMPLEMENT_DYNCREATE(CPageListDetailView, CScrollView)

CPageListDetailView::CPageListDetailView()
{
	m_DisplayList.Create(this, FALSE, DISPLAY_LIST_TYPE(CPageListDetailView));

	m_nColorSel		= 0;
	m_fScale		= 1.0f;
	m_bInitialize	= TRUE;

	m_nColorFrameSelection = 0;
	m_bCheckRedFrame	= TRUE;
	m_bCheckBlackFrame	= FALSE;
	m_bCheckYellowFrame = FALSE;
	m_bCheckCyanFrame	= FALSE;
}

CPageListDetailView::~CPageListDetailView()
{
}


BEGIN_MESSAGE_MAP(CPageListDetailView, CScrollView)
	//{{AFX_MSG_MAP(CPageListDetailView)
	ON_WM_KEYDOWN()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_BLACK_FRAME, OnBlackFrame)
	ON_UPDATE_COMMAND_UI(ID_BLACK_FRAME, OnUpdateBlackFrame)
	ON_COMMAND(ID_CYAN_FRAME, OnCyanFrame)
	ON_UPDATE_COMMAND_UI(ID_CYAN_FRAME, OnUpdateCyanFrame)
	ON_COMMAND(ID_RED_FRAME, OnRedFrame)
	ON_UPDATE_COMMAND_UI(ID_RED_FRAME, OnUpdateRedFrame)
	ON_COMMAND(ID_YELLOW_FRAME, OnYellowFrame)
	ON_UPDATE_COMMAND_UI(ID_YELLOW_FRAME, OnUpdateYellowFrame)
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_KEYUP()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageListDetailView drawing

int CPageListDetailView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

#ifdef PDF
//	m_pdfReader.Create(NULL, NULL, WS_VISIBLE, CRect(0,0,0,0), this, 0);
#endif

	return 0;
}


void CPageListDetailView::OnSize(UINT nType, int cx, int cy) 
{
	CScrollView::OnSize(nType, cx, cy);
	
#ifdef PDF
//	m_pdfReader.MoveWindow(0, 0, cx, cy);
#endif
}


void CPageListDetailView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	// Create the ToolTip control.
	m_tooltip.Create(this);
	m_tooltip.Activate(TRUE);

	SetDocSize();	// May be changed in Pagelistview or propsDialog

	SetScrollSizes(MM_TEXT, CSize((long)(m_fDocSizeX + 0.5f), (long)(m_fDocSizeY + 0.5f)));
}

CPageTemplate* CPageListDetailView::GetPageTemplate()
{
	CPageListView* pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if ( ! pPageListView)
		return NULL;

	CDisplayItem* pDI = pPageListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));
	return (pDI) ? (CPageTemplate*)pDI->m_pData : NULL;
}

CLayoutObject* CPageListDetailView::GetLayoutObject()
{
	CPageTemplate* pPageTemplate = GetPageTemplate();
	return (pPageTemplate) ? pPageTemplate->GetLayoutObject() : NULL;
}

//CPageSource* CPageListDetailView::GetPageSource()
//{
//}

void CPageListDetailView::OnDraw(CDC* pDC)
{
	if ( ! GetPageTemplate())
		return;
	if (GetPageTemplate()->m_ColorSeparations.GetSize() <= 0)
		return;
	if (!m_pPageSource)
		return;

	CPageListView* pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if ( ! pPageListView)
		return;
	// Check if GetPageTemplate() and GetLayoutObject() still corresponds to selected page template
	// i.e. if layout object has been removed when detail view is open, GetLayoutObject() is no longer valid and docs global page size has to be taken
	CDisplayItem* pDI = pPageListView->m_DisplayList.GetFirstSelectedItem();
	if ( ! pDI)
		return;
	if (pDI->m_pData != GetPageTemplate())
		return;
	if (GetPageTemplate()->GetLayoutObject() != GetLayoutObject())
		SetDocSize();
	///////////////////////////////////////

	m_DisplayList.Invalidate();	// number of color buttons can change and we don't have too much display items -> so simply always invalidate

	m_DisplayList.BeginRegisterItems(pDC);
	InitStatusBar(pDC);	// Only register the ColorButton-Rectangles
	m_DisplayList.EndRegisterItems(pDC);

	CRect bitmapRect, pageRect, trimRect;
	bitmapRect.left   = (long)(m_fBitmapLeft   + 0.5f);
	bitmapRect.top    = (long)(m_fBitmapTop    + 0.5f);
	bitmapRect.right  = (long)(m_fBitmapRight  + 0.5f);
	bitmapRect.bottom = (long)(m_fBitmapBottom + 0.5f);
	pageRect.left	  = (long)(m_fPageLeft     + 0.5f);
	pageRect.top      = (long)(m_fPageTop      + 0.5f);
	pageRect.right    = (long)(m_fPageRight    + 0.5f);
	pageRect.bottom   = (long)(m_fPageBottom   + 0.5f);
	trimRect.left	  = (long)(m_fTrimLeft     + 0.5f);
	trimRect.top      = (long)(m_fTrimTop      + 0.5f);
	trimRect.right    = (long)(m_fTrimRight    + 0.5f);
	trimRect.bottom   = (long)(m_fTrimBottom   + 0.5f);

	CBrush  backGroundBrush(RGB(245,245,245));
	CBrush* pOldBrush = pDC->SelectObject(&backGroundBrush);
	CPen*   pOldPen   = (CPen*)pDC->SelectStockObject(NULL_PEN);
	pDC->Rectangle(pageRect);

	BOOL bBitmapDrawn = FALSE;
	int nSize = GetPageTemplate()->m_ColorSeparations.GetSize();
	if ((nSize > 0) && (m_nColorSel < nSize))
	{
		CString strPreviewFile;
		CPageSourceHeader* pPageSourceHeader = GetPageTemplate()->GetObjectSourceHeader(GetPageTemplate()->m_ColorSeparations[m_nColorSel].m_nColorDefinitionIndex);
		strPreviewFile						 = m_pPageSource->GetPreviewFileName(GetPageTemplate()->m_ColorSeparations[m_nColorSel].m_nPageNumInSourceFile);
		bBitmapDrawn						 = CPageListView::DrawPreview(pDC, bitmapRect, TOP, GetPageTemplate()->m_fContentRotation, strPreviewFile);
	}

	CRect clientRect;
	CRect clipRect;
	GetClientRect(clientRect);
	if (clipRect.IntersectRect(clientRect + GetScrollPosition(), pageRect))
		pDC->ExcludeClipRect(clipRect);
	CPrintSheetView::FillTransparent(pDC, bitmapRect, LIGHTGRAY, TRUE, 180);
	pDC->SelectClipRgn(NULL);

	// Only reinit ClipRgn -> because the previously command "pDC->SelectClipRgn(NULL);" is necessary
	InitStatusBar(pDC, FALSE);

	CPen solidPenRed   (PS_SOLID, 1, RED);
	CPen solidPenBlack (PS_SOLID, 1, BLACK);
	CPen solidPenYellow(PS_SOLID, 1, YELLOW);
	CPen solidPenBlue  (PS_SOLID, 1, LIGHTBLUE);
	CPen dottedPenGray (PS_DOT,	  1, DARKGRAY);
	switch(m_nColorFrameSelection)
	{
	case  0: pDC->SelectObject(&solidPenRed);
			 break;
	case  1: pDC->SelectObject(&solidPenBlack);
			 break;
	case  2: pDC->SelectObject(&solidPenYellow);
			 break;
	case  3: pDC->SelectObject(&solidPenBlue);
			 break;
	default: pDC->SelectObject(&solidPenRed);
			 break;
	}

	int		 nOldBKMode		= pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor	= pDC->SetBkColor(LIGHTRED);
	CFont*	 pOldFont		= (CFont*)pDC->SelectStockObject(ANSI_VAR_FONT);
	COLORREF crOldTextColor = pDC->SetTextColor(WHITE);

	CString strOut;

	long lLine = (long)(15.0f * m_fScale + 0.5f);

	pDC->MoveTo(pageRect.left  - lLine, pageRect.top);
	pDC->LineTo(pageRect.right + lLine, pageRect.top);
	pDC->MoveTo(pageRect.right,			pageRect.top	- lLine);
	pDC->LineTo(pageRect.right,			pageRect.bottom + lLine);
	pDC->MoveTo(pageRect.right + lLine,	pageRect.bottom);
	pDC->LineTo(pageRect.left  - lLine, pageRect.bottom);
	pDC->MoveTo(pageRect.left,			pageRect.bottom + lLine);
	pDC->LineTo(pageRect.left,			pageRect.top	- lLine);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		int		nFlatProductIndex = pDoc->m_PageTemplateList.GetIndex(GetPageTemplate(), GetPageTemplate()->m_nProductPartIndex); 
		CString strObjectName	  = pDoc->m_flatProducts.FindByIndex(nFlatProductIndex/2).m_strProductName;
		CGraphicObject* pgObject  = pDoc->m_flatProducts.FindByIndex(nFlatProductIndex/2).m_graphicList.GetObject(strObjectName);
		if (pgObject)
		{
			g_bIsContentView = TRUE;
			CLayoutObject* pObject = GetLayoutObject();
			if (pObject)
			{
				float fScaleX = pageRect.Width()  / pObject->GetWidth();
				float fScaleY = pageRect.Height() / pObject->GetHeight();
				pgObject->SetScale(fScaleX, -fScaleY);
			}
			pgObject->Draw(pDC, pageRect.left, pageRect.top, pageRect.Width(), pageRect.Height(), TOP, -1);
			pgObject->SetScale(1.0f, 1.0f);
		}
	}

	CString strWidth, strHeight;
	if ( ! trimRect.IsRectEmpty())
	{
		CPen dottedPen(PS_DOT, 1, BLUE);
		pDC->SelectObject(&dottedPen);
		pDC->SetBkMode(TRANSPARENT);

		pDC->MoveTo(trimRect.left  - lLine, trimRect.top);
		pDC->LineTo(trimRect.right + lLine, trimRect.top);
		pDC->MoveTo(trimRect.right,			trimRect.top	- lLine);
		pDC->LineTo(trimRect.right,			trimRect.bottom + lLine);
		pDC->MoveTo(trimRect.right + lLine,	trimRect.bottom);
		pDC->LineTo(trimRect.left  - lLine, trimRect.bottom);
		pDC->MoveTo(trimRect.left,			trimRect.bottom + lLine);
		pDC->LineTo(trimRect.left,			trimRect.top	- lLine);

		//CSize textExtent = pDC->GetTextExtent(strOut);
		//SetMeasure(0, m_fTrimBoxWidth,  strWidth);	// format string
		//SetMeasure(0, m_fTrimBoxHeight, strHeight);	// format string
		//CPrintSheetView::TruncateNumber(strWidth,  m_fTrimBoxWidth);
		//CPrintSheetView::TruncateNumber(strHeight, m_fTrimBoxHeight);
		//strOut.Format(_T(" TrimBox: %s x %s "), strWidth, strHeight);
		//pDC->SetBkColor(LIGHTBLUE);
		//pDC->SetBkMode(OPAQUE);
		//pDC->TextOut(textExtent.cx, 0, strOut);
	}
	pDC->SetBkColor(crOldBkColor);
	pDC->SetBkMode(nOldBKMode);
	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldTextColor);

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);

	backGroundBrush.DeleteObject();
	solidPenRed.DeleteObject();
	solidPenBlack.DeleteObject();
	solidPenYellow.DeleteObject();
	solidPenBlue.DeleteObject();
	dottedPenGray.DeleteObject();

//	DrawReflines(pDC);
}

//void CPageListDetailView::DrawReflines(pDC)
//{
//}

void CPageListDetailView::InitStatusBar(CDC* pDC, BOOL bCompleteWithDraw/*=TRUE*/)
{
	CRect clientRect;
	GetClientRect(clientRect);
	m_RightStatusBar	  = clientRect;
	m_RightStatusBar.left = m_RightStatusBar.right - 20;

    CClientDC dc(this);

	if (bCompleteWithDraw)
	{
		CPen	penDarkGray(PS_SOLID, 1, DARKGRAY);
		CBrush  brush(RGB(200,200,180));
		CBrush* pOldBrush = dc.SelectObject(&brush);
		CPen*	pOldPen	  = (CPen*)dc.SelectStockObject(NULL_PEN);
		
		dc.Rectangle(m_RightStatusBar);
		
		dc.SelectObject(&penDarkGray);
		dc.MoveTo(m_RightStatusBar.left+1, m_RightStatusBar.top);
		dc.LineTo(m_RightStatusBar.left+1, m_RightStatusBar.bottom);
		dc.SelectStockObject(WHITE_PEN);
		dc.MoveTo(m_RightStatusBar.left, m_RightStatusBar.top);
		dc.LineTo(m_RightStatusBar.left, m_RightStatusBar.bottom);

/////////////////////////////////////////////////////////////////////////////
// Draw and register the color-buttons

		int nCount = m_tooltip.GetToolCount();
		for (int n = 0; n < nCount; n++)
			m_tooltip.DelTool(this, n + 1);	// ID is > 0

		int nSize = GetPageTemplate()->m_ColorSeparations.GetSize();
		if (nSize > 0 && m_nColorSel < nSize)
		{
			CImpManDoc* pDoc = GetDocument();
			CRect buttonRect;
			for (int i = 0; i < nSize; i++)
			{
				CColorDefinition colorDef("Unknown", LIGHTGRAY, CColorDefinition::Spot, 0);
				if ( (GetPageTemplate()->m_ColorSeparations[i].m_nColorDefinitionIndex >= 0) && (GetPageTemplate()->m_ColorSeparations[i].m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
					colorDef = pDoc->m_ColorDefinitionTable[GetPageTemplate()->m_ColorSeparations[i].m_nColorDefinitionIndex];			
				else
				{
					CPageSourceHeader* pPageSourceHeader = GetPageTemplate()->GetObjectSourceHeader(GetPageTemplate()->m_ColorSeparations[i]);
					colorDef.m_strColorName = pPageSourceHeader->m_strColorName;
				}
				CBrush colorBrush(colorDef.m_rgb);
				dc.SelectObject(&colorBrush);
				dc.SelectStockObject(WHITE_PEN);

				buttonRect.left = m_RightStatusBar.left + 4;
				buttonRect.right = m_RightStatusBar.right - 4;
				buttonRect.top = m_RightStatusBar.top + (i * 12) + ((i + 1) * 4);
				buttonRect.bottom = buttonRect.top + 12;
				dc.Rectangle(buttonRect);
				// Add tooltip to button
				m_tooltip.AddTool(this, colorDef.m_strColorName, buttonRect, i + 1); // ID must be > 0

				if (i == m_nColorSel)

				{	// Draw a pressed button
					dc.SelectStockObject(BLACK_PEN);
					dc.MoveTo(buttonRect.left, buttonRect.bottom-1);
					dc.LineTo(buttonRect.left, buttonRect.top);
					dc.LineTo(buttonRect.right, buttonRect.top);
					dc.SelectObject(&penDarkGray);
					dc.MoveTo(buttonRect.left+1, buttonRect.bottom-1);
					dc.LineTo(buttonRect.left+1, buttonRect.top+1);
					dc.LineTo(buttonRect.right-1, buttonRect.top+1);
				}
				else
				{	// Draw an unpressed button
					dc.SelectStockObject(BLACK_PEN);
					dc.MoveTo(buttonRect.left, buttonRect.bottom);
					dc.LineTo(buttonRect.right, buttonRect.bottom);
					dc.LineTo(buttonRect.right, buttonRect.top-1);
					dc.SelectObject(&penDarkGray);
					dc.MoveTo(buttonRect.left+1, buttonRect.bottom-1);
					dc.LineTo(buttonRect.right-1, buttonRect.bottom-1);
					dc.LineTo(buttonRect.right-1, buttonRect.top);
				}

				// Register the button
				CPoint scrollPos = GetScrollPosition();	
				buttonRect+= scrollPos;	// otherwise clicking the buttons when page was zoomed is not possible
				m_DisplayList.RegisterItem(pDC, buttonRect, buttonRect, (void*)&colorDef, DISPLAY_ITEM_REGISTRY(CPageListDetailView, ColorButton));

				colorBrush.DeleteObject();
			}
		}
/////////////////////////////////////////////////////////////////////////////

		dc.SelectObject(pOldBrush);
		dc.SelectObject(pOldPen);
		penDarkGray.DeleteObject();
	}

	CRect clipRect = m_RightStatusBar;
	pDC->DPtoLP(clipRect);
	pDC->ExcludeClipRect(clipRect);
}

BOOL CPageListDetailView::OnSelectColorButton(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object has been selected
{
	pDI->m_nState = CDisplayItem::Normal;  // Don't draw the selection

    CDisplayItem* pDIfromList = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPageListDetailView, ColorButton));

	int nSize = m_DisplayList.m_List.GetSize();
    for (int i = 0; i < nSize && pDIfromList; i++)
    {
        if (pDI == pDIfromList)
        {
			if (m_nColorSel != i)
			{
				m_nColorSel = i;
				SetDocSize();
				Invalidate();
				UpdateWindow();

				CPageListView* pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
				if (pPageListView)
				{
					pPageListView->Invalidate();
					pPageListView->UpdateWindow(); 
				}
				break;
			}
        }

        pDIfromList = m_DisplayList.GetNextItem(pDIfromList, DISPLAY_ITEM_TYPEID(CPageListDetailView, ColorButton));
    }

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CPageListDetailView diagnostics

#ifdef _DEBUG
void CPageListDetailView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CPageListDetailView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CImpManDoc* CPageListDetailView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CImpManDoc)));
	return (CImpManDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPageListDetailView message handlers

void CPageListDetailView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/) 
{
	SetDocSize();	// May be changed in Pagelistview or propsDialog

/*
	if ( ! m_pPageSource)
		return;

	CString strOutputPDFFile;
	strOutputPDFFile.Format(_T("g:\\Out.pdf"));

	DeleteFile(strOutputPDFFile);

	int32 hPDFFile;
	int32 nResult = PDFOutputStream_Open2(strOutputPDFFile, &hPDFFile, theApp.settings.m_szTempDir);
	if (nResult)
	{
		CString strMsg;
		GetErrorStringFromErrorCode( nResult, strMsg.GetBuffer(255), 255, 0); strMsg.ReleaseBuffer();
		AfxMessageBox(strMsg, MB_OK | MB_ICONSTOP);
		return;
	}

	TDRECT mediaBox	= { 0, 0, 300, 300 };
	TDRECT cropBox	= { 0, 0, 300, 300 };
	int32  nSheetIndex;

	nResult = PDFOutputStream_AddSheet(hPDFFile, &nSheetIndex, &mediaBox, &cropBox);
	if (nResult)
	{
		CString strMsg;
		GetErrorStringFromErrorCode( nResult, strMsg.GetBuffer(255), 255, 0); strMsg.ReleaseBuffer();
		AfxMessageBox(strMsg, MB_OK | MB_ICONSTOP);
		return;
	}

	TOBJECTID objectID  = {sizeof(TOBJECTID), "", GetPageTemplate()->m_ColorSeparations[m_nColorSel].m_nPageNumInSourceFile, "", ""};
	_tcscpy(objectID.pdffile, m_pPageSource->m_strDocumentFullpath);
	TDMATRIX matrix = { 1.0, 0, 0, 1.0, 0.0, 0.0 };
	TDRECT   objectClipBox = {0, 300, 300, 0};

	nResult = PDFSheet_AddObject(hPDFFile, nSheetIndex, &objectID, &matrix, &objectClipBox, NULL);
	if (nResult)
	{
		CString strMsg;
		GetErrorStringFromErrorCode( nResult, strMsg.GetBuffer(255), 255, 0); strMsg.ReleaseBuffer();
		AfxMessageBox(strMsg, MB_OK | MB_ICONSTOP);
	}

	PDFOutputStream_Close(hPDFFile);

	m_pdfReader.LoadFile("G:\\out.pdf");
	return;
*/

	SetScrollSizes(MM_TEXT, CSize((long)(m_fDocSizeX + 0.5f), (long)(m_fDocSizeY + 0.5f)));
	Invalidate();

	OnNcPaint();
}


void CPageListDetailView::SetDocSize()
{
	m_pPageSource = NULL;
	m_fDocSizeX	  = 0.0f;
	m_fDocSizeY	  = 0.0f;

	CPageTemplate* pPageTemplate = GetPageTemplate();
	if (pPageTemplate)
	{
		CImpManDoc* pDoc = GetDocument();
		if ( ! pDoc)
			return;

		int nSize = pPageTemplate->m_ColorSeparations.GetSize();
		if (nSize > 0 && m_nColorSel < nSize)
		{
			int nColorDefinitionIndex = pPageTemplate->m_ColorSeparations[m_nColorSel].m_nColorDefinitionIndex;
			m_pPageSource			  = pPageTemplate->GetObjectSource(nColorDefinitionIndex);

			if ( ! m_pPageSource)
				return;

			float fPageWidth  = 0.0f;
			float fPageHeight = 0.0f;
			if (GetLayoutObject())
			{
				fPageWidth  = GetLayoutObject()->GetRealWidth();
				fPageHeight = GetLayoutObject()->GetRealHeight();
			}
			else
			{
				if ( ! pPageTemplate->IsFlatProduct())
				{
					fPageWidth  = pPageTemplate->GetProductPart().m_fPageWidth;
					fPageHeight = pPageTemplate->GetProductPart().m_fPageHeight;
				}
				else
				{
					int nIndex = pDoc->m_PageTemplateList.GetIndex(pPageTemplate, pPageTemplate->m_nProductPartIndex)/2;
					if (nIndex < pDoc->m_flatProducts.GetSize())
					{
						fPageWidth  = pDoc->m_flatProducts.FindByIndex(nIndex).m_fTrimSizeWidth;
						fPageHeight = pDoc->m_flatProducts.FindByIndex(nIndex).m_fTrimSizeHeight;
					}
				}
			}
			if (pPageTemplate->m_nMultipageGroupNum >= 0)
			{
				if (GetLayoutObject())
				{
					CLayoutObject* p2ndLayoutObject = GetLayoutObject()->GetCurrentMultipagePartner(GetPageTemplate(), FALSE);
					fPageWidth += p2ndLayoutObject->GetRealWidth();
				}
				else
					fPageWidth += pPageTemplate->GetProductPart().m_fPageWidth;
			}

			float fBBoxLeft, fBBoxBottom, fBBoxWidth, fBBoxHeight;
			m_fTrimLeft = m_fTrimTop = m_fTrimRight = m_fTrimBottom = 0.0f;
			CPageSourceHeader* pObjectSourceHeader = GetPageTemplate()->GetObjectSourceHeader(nColorDefinitionIndex);
			if ((pPageTemplate->m_fContentRotation == 0.0f) || (pPageTemplate->m_fContentRotation == 180.0f))
			{
				fBBoxWidth  = (pObjectSourceHeader->m_fBBoxRight - pObjectSourceHeader->m_fBBoxLeft)   * pPageTemplate->m_fContentScaleX;
				fBBoxHeight = (pObjectSourceHeader->m_fBBoxTop   - pObjectSourceHeader->m_fBBoxBottom) * pPageTemplate->m_fContentScaleY;
				fBBoxLeft	=  pObjectSourceHeader->m_fBBoxLeft; 
				fBBoxBottom =  pObjectSourceHeader->m_fBBoxBottom;
				m_fTrimLeft    = (pObjectSourceHeader->m_fTrimBoxLeft	* pPageTemplate->m_fContentScaleX) / PICA_POINTS;
				m_fTrimBottom  = ((fBBoxHeight - pObjectSourceHeader->m_fTrimBoxBottom) * pPageTemplate->m_fContentScaleY) / PICA_POINTS;
				m_fTrimRight   = (pObjectSourceHeader->m_fTrimBoxRight	* pPageTemplate->m_fContentScaleX) / PICA_POINTS;
				m_fTrimTop	   = ((fBBoxHeight - pObjectSourceHeader->m_fTrimBoxTop)	* pPageTemplate->m_fContentScaleY) / PICA_POINTS;
			}
			else
			{
				fBBoxWidth  = (pObjectSourceHeader->m_fBBoxTop   - pObjectSourceHeader->m_fBBoxBottom) * pPageTemplate->m_fContentScaleX;
				fBBoxHeight = (pObjectSourceHeader->m_fBBoxRight - pObjectSourceHeader->m_fBBoxLeft)   * pPageTemplate->m_fContentScaleY;
				fBBoxLeft	=  pObjectSourceHeader->m_fBBoxBottom; 
				fBBoxBottom =  pObjectSourceHeader->m_fBBoxLeft;
				m_fTrimLeft    = (pObjectSourceHeader->m_fTrimBoxLeft	* pPageTemplate->m_fContentScaleX) / PICA_POINTS;
				m_fTrimBottom  = ((fBBoxWidth - pObjectSourceHeader->m_fTrimBoxBottom) * pPageTemplate->m_fContentScaleY) / PICA_POINTS;
				m_fTrimRight   = (pObjectSourceHeader->m_fTrimBoxRight	* pPageTemplate->m_fContentScaleX) / PICA_POINTS;
				m_fTrimTop	   = ((fBBoxWidth - pObjectSourceHeader->m_fTrimBoxTop)	* pPageTemplate->m_fContentScaleY) / PICA_POINTS;
			}

			float fTrimCenterX = m_fTrimLeft + (m_fTrimRight  - m_fTrimLeft) / 2.0f;
			float fTrimCenterY = m_fTrimTop  + (m_fTrimBottom - m_fTrimTop)  / 2.0f;
			float fDeltaLeft   = fTrimCenterX  - m_fTrimLeft;
			float fDeltaRight  = m_fTrimRight  - fTrimCenterX;
			float fDeltaBottom = m_fTrimBottom - fTrimCenterY;
			float fDeltaTop    = fTrimCenterY  - m_fTrimTop;
			if ((GetPageTemplate()->m_fContentRotation == 0.0f) || (pPageTemplate->m_fContentRotation == 180.0f))
			{
				m_fTrimBoxWidth  = (pObjectSourceHeader->m_fTrimBoxRight - pObjectSourceHeader->m_fTrimBoxLeft)   * pPageTemplate->m_fContentScaleX;
				m_fTrimBoxHeight = (pObjectSourceHeader->m_fTrimBoxTop   - pObjectSourceHeader->m_fTrimBoxBottom) * pPageTemplate->m_fContentScaleY;
				if (pPageTemplate->m_fContentRotation == 180.0f)
				{
					float fLeft   = fTrimCenterX - fDeltaRight;
					float fBottom = fTrimCenterY + fDeltaTop;
					float fRight  = fTrimCenterX + fDeltaLeft;
					float fTop	  = fTrimCenterY - fDeltaBottom;
					m_fTrimLeft   = fLeft; m_fTrimBottom = fBottom; m_fTrimRight = fRight; m_fTrimTop = fTop;
				}
			}
			else
			{
				m_fTrimBoxWidth  = (pObjectSourceHeader->m_fTrimBoxTop   - pObjectSourceHeader->m_fTrimBoxBottom) * pPageTemplate->m_fContentScaleX;
				m_fTrimBoxHeight = (pObjectSourceHeader->m_fTrimBoxRight - pObjectSourceHeader->m_fTrimBoxLeft)   * pPageTemplate->m_fContentScaleY;

				float fTemp = fTrimCenterX; fTrimCenterX = fTrimCenterY; fTrimCenterY = fTemp;
				if (pPageTemplate->m_fContentRotation == 90.0f)
				{
					float fLeft   = fTrimCenterX - fDeltaTop;
					float fBottom = fTrimCenterY + fDeltaLeft;
					float fRight  = fTrimCenterX + fDeltaBottom;
					float fTop	  = fTrimCenterY - fDeltaRight;
					m_fTrimLeft   = fLeft; m_fTrimBottom = fBottom; m_fTrimRight = fRight; m_fTrimTop = fTop;
				}
				else
				{
					float fLeft   = fTrimCenterX - fDeltaBottom;
					float fBottom = fTrimCenterY + fDeltaRight;
					float fRight  = fTrimCenterX + fDeltaTop;
					float fTop	  = fTrimCenterY - fDeltaLeft;
					m_fTrimLeft   = fLeft; m_fTrimBottom = fBottom; m_fTrimRight = fRight; m_fTrimTop = fTop;
				}
			}
			BOOL bTrimBox;
			if ( (m_fTrimBoxWidth == 0.0f) || (m_fTrimBoxHeight == 0.0f) )	// no trim box
				bTrimBox = FALSE;
			else
				bTrimBox = TRUE;

			m_fBitmapLeft   =  fBBoxLeft	/ PICA_POINTS; 
			m_fBitmapTop	= -fBBoxBottom	/ PICA_POINTS; 
			m_fBitmapRight  = ( fBBoxLeft	+ fBBoxWidth)  / PICA_POINTS;
			m_fBitmapBottom = (-fBBoxBottom + fBBoxHeight) / PICA_POINTS; 

			m_fPageLeft   = ((fBBoxWidth  - fPageWidth ) / 2.0f + fBBoxLeft   - pPageTemplate->GetContentOffsetX()) / PICA_POINTS;
			m_fPageTop	  = ((fBBoxHeight - fPageHeight) / 2.0f - fBBoxBottom + pPageTemplate->GetContentOffsetY()) / PICA_POINTS;
			m_fPageRight  = ((fBBoxWidth  + fPageWidth ) / 2.0f + fBBoxLeft   - pPageTemplate->GetContentOffsetX()) / PICA_POINTS;
			m_fPageBottom = ((fBBoxHeight + fPageHeight) / 2.0f - fBBoxBottom + pPageTemplate->GetContentOffsetY()) / PICA_POINTS;

			const float fBorder = 36.0f; // 1/2 inch border

			float fOffsetLeft = fBorder - __min(m_fBitmapLeft, m_fPageLeft);
			float fOffsetTop  = fBorder - __min(m_fBitmapTop,  m_fPageTop);

			m_fBitmapLeft += fOffsetLeft; m_fBitmapTop   += fOffsetTop; 
			m_fBitmapRight+= fOffsetLeft; m_fBitmapBottom+= fOffsetTop; 
			m_fPageLeft   += fOffsetLeft; m_fPageTop     += fOffsetTop; 
			m_fPageRight  += fOffsetLeft; m_fPageBottom  += fOffsetTop; 

#ifdef PDF
			if (bTrimBox)
			{
				m_fTrimLeft   += fOffsetLeft; m_fTrimTop     += fOffsetTop; 
				m_fTrimRight  += fOffsetLeft; m_fTrimBottom  += fOffsetTop; 
			}
#endif

			if (m_bInitialize)
			{
				CRect clientRect;
				GetClientRect(&clientRect);
				float fScaleW = (float)clientRect.Width()  / __max(m_fPageRight  + fBorder,  m_fBitmapRight  + fBorder);
				float fScaleH = (float)clientRect.Height() / __max(m_fPageBottom + fBorder,  m_fBitmapBottom + fBorder);
				m_fScale	  = __min(fScaleW, fScaleH);
				m_bInitialize = FALSE;
			}

			m_fBitmapLeft  *= m_fScale; m_fBitmapTop	*= m_fScale;
			m_fBitmapRight *= m_fScale; m_fBitmapBottom *= m_fScale;
			m_fPageLeft	   *= m_fScale; m_fPageTop		*= m_fScale;
			m_fPageRight   *= m_fScale; m_fPageBottom	*= m_fScale;

#ifdef PDF
			if (bTrimBox)
			{
				m_fTrimLeft	   *= m_fScale; m_fTrimTop		*= m_fScale;
				m_fTrimRight   *= m_fScale; m_fTrimBottom	*= m_fScale;
			}
#endif
			m_fDocSizeX = __max(m_fPageRight,  m_fBitmapRight)  + fBorder * m_fScale;
			m_fDocSizeY = __max(m_fPageBottom, m_fBitmapBottom) + fBorder * m_fScale;
		}
	}
}

void CPageListDetailView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
	{
	case VK_HOME: 
		OnVScroll(SB_TOP, 0, NULL);
		OnHScroll(SB_LEFT, 0, NULL);
		break;
	case VK_END: 
		OnVScroll(SB_BOTTOM, 0, NULL);
		OnHScroll(SB_RIGHT, 0, NULL);
		break;
	case VK_UP: 
		OnVScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_DOWN: 
		OnVScroll(SB_LINEDOWN, 0, NULL);
		break;
	case VK_PRIOR: 
		OnVScroll(SB_PAGEUP, 0, NULL);
		break;
	case VK_NEXT:
		OnVScroll(SB_PAGEDOWN, 0, NULL);
		break;
	case VK_LEFT:
		OnHScroll(SB_LINELEFT, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(SB_LINERIGHT, 0, NULL);
		break;
	case VK_CONTROL:
		if (((CPageListFrame*)GetParentFrame())->m_bZoomMode)
			if (GetCursor() == theApp.m_CursorMagnifyIncrease)
				SetCursor(theApp.m_CursorMagnifyDecrease);
		break;
	default:
		break;
	}
	
	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CPageListDetailView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if (nChar == VK_CONTROL)
		if (((CPageListFrame*)GetParentFrame())->m_bZoomMode)
			if (GetCursor() == theApp.m_CursorMagnifyDecrease)
				SetCursor(theApp.m_CursorMagnifyIncrease);
	
	CScrollView::OnKeyUp(nChar, nRepCnt, nFlags);
}

BOOL CPageListDetailView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// The default call of OnMouseWheel causes an assertion or doesn't work
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion
}

void CPageListDetailView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

BOOL CPageListDetailView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// 2nd parameter must be 0 (means no standard-cursor will be registered), 
	// if we want draw out own cursors
	CBrush* pBkgrdBrush = new CBrush;
	pBkgrdBrush->CreateSolidBrush(DARKGRAY);

	lpszClassName = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(*pBkgrdBrush), 0);

	BOOL ret = CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	pBkgrdBrush->Detach();
	delete pBkgrdBrush;

	return ret;
}

BOOL CPageListDetailView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	
	if (pParentFrame->m_bZoomMode)	
		if (GetKeyState(VK_CONTROL) < 0)
   			SetCursor(theApp.m_CursorMagnifyDecrease);
		else
			SetCursor(theApp.m_CursorMagnifyIncrease);
	else
		SetCursor(theApp.m_CursorStandard);
	
	return CScrollView::OnSetCursor(pWnd, nHitTest, message);
}

void CPageListDetailView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if (((CPageListFrame*)GetParentFrame())->m_bZoomMode)
		ZoomWindow(point);
	else
		m_DisplayList.OnLButtonDown(point); // pass message to display list

	CScrollView::OnLButtonDown(nFlags, point);
}

void CPageListDetailView::ZoomWindow(const CPoint& rPoint, BOOL bRButton)
{
	CPoint scrollPos;
	BOOL   bUpdate = TRUE;

	if (GetCursor() == theApp.m_CursorMagnifyDecrease)
	{
		((CPageListFrame*)GetParentFrame())->m_bZoomMode = FALSE;
		m_fScale	  = 1.0f;
		m_bInitialize = TRUE;
		SetDocSize();	
		scrollPos = CPoint(0, 0);
	}
	else
	{
		scrollPos = GetScrollPosition();

		BOOL		   bTracked = FALSE;
	    CRectTracker   tracker;
	    CMyRectTracker rbTracker;
		bTracked = (bRButton) ? rbTracker.TrackRubberBand(this, rPoint, TRUE) : tracker.TrackRubberBand(this, rPoint, TRUE);

		if (bTracked)
		{
			CRect trackRect = (bRButton) ? rbTracker.m_rect : tracker.m_rect;
			trackRect.NormalizeRect(); 

			CRect clientRect;
			GetClientRect(&clientRect);
			float fFactor = (float)clientRect.Width()/(float)trackRect.Width();

			switch (theApp.m_dwPlatformId)
			{
			case VER_PLATFORM_WIN32_NT:		 bUpdate = ((m_fDocSizeX * fFactor < (float)LONG_MAX/2) && (m_fDocSizeY * fFactor < (float)LONG_MAX/2)) ? TRUE : FALSE; break;
			case VER_PLATFORM_WIN32_WINDOWS: bUpdate = ((m_fDocSizeX * fFactor < 5000.0f) && (m_fDocSizeY * fFactor < (float)5000.0f)) ? TRUE : FALSE; break;
			default:						 bUpdate = ((m_fDocSizeX * fFactor < 5000.0f) && (m_fDocSizeY * fFactor < (float)5000.0f)) ? TRUE : FALSE; break;
			}									// Under 95 values greater approx. 5000 doesn't work - StretchDIBits()
												// doesn't paint anything. Don't know why!!!

			if (bUpdate)
			{
				m_fScale *= fFactor;
				SetDocSize();	
				scrollPos  += trackRect.TopLeft();
				scrollPos.x = (long)((float)scrollPos.x * fFactor);
				scrollPos.y = (long)((float)scrollPos.y * fFactor);
			}
		}
	}

	if (bUpdate)
	{
		SetScrollSizes(MM_TEXT, CSize((long)(m_fDocSizeX + 0.5f), (long)(m_fDocSizeY + 0.5f)));
		ScrollToPosition(scrollPos);
		Invalidate();
		OnNcPaint();
	}
}

void CPageListDetailView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if (nFlags & MK_CONTROL)
	{
		SetCursor(theApp.m_CursorMagnifyDecrease);
		((CPageListFrame*)GetParentFrame())->m_bZoomMode = TRUE;
	}
	else
		if (m_DisplayList.BeginDrag(10))
		{
			((CPageListFrame*)GetParentFrame())->m_bZoomMode = TRUE;
			SetCursor(theApp.m_CursorMagnifyIncrease);
			ZoomWindow(point, TRUE);
			SetCursor(theApp.m_CursorStandard);
			((CPageListFrame*)GetParentFrame())->m_bZoomMode = FALSE;
		}
		else
		{
			m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list

			CPoint pt = point;
			ClientToScreen(&pt);
			OnContextMenu(NULL, pt);
		}
}

void CPageListDetailView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	if (nFlags & MK_CONTROL)
		if (GetCursor() == theApp.m_CursorMagnifyDecrease)
		{
			ZoomWindow(point);
			SetCursor(theApp.m_CursorStandard);
			((CPageListFrame*)GetParentFrame())->m_bZoomMode = FALSE;
		}
}

void CPageListDetailView::OnContextMenu(CWnd* /*pWnd*/, CPoint point) 
{
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_PAGELIST_DETAILVIEW_POPUP));

	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);

	CWnd* pWndPopupOwner = this;
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();
///////////////////////////////////////////////////////////////////
//  Add bitmaps to menu-items corresponding to frame colors
	CBitmap RedBmp, BlackBmp, YellowBmp, CyanBmp, RedChkBmp, BlackChkBmp, YellowChkBmp, CyanChkBmp;
	RedBmp.LoadBitmap(IDB_RED);
	BlackBmp.LoadBitmap(IDB_BLACK);
	YellowBmp.LoadBitmap(IDB_YELLOW);
	CyanBmp.LoadBitmap(IDB_CYAN);
	RedChkBmp.LoadBitmap(IDB_RED_CHECKED);
	BlackChkBmp.LoadBitmap(IDB_BLACK_CHECKED);
	YellowChkBmp.LoadBitmap(IDB_YELLOW_CHECKED);
	CyanChkBmp.LoadBitmap(IDB_CYAN_CHECKED);
	pPopup->SetMenuItemBitmaps(ID_RED_FRAME, MF_BYCOMMAND, &RedBmp, &RedChkBmp);
	pPopup->SetMenuItemBitmaps(ID_BLACK_FRAME, MF_BYCOMMAND, &BlackBmp, &BlackChkBmp);
	pPopup->SetMenuItemBitmaps(ID_YELLOW_FRAME, MF_BYCOMMAND, &YellowBmp, &YellowChkBmp);
	pPopup->SetMenuItemBitmaps(ID_CYAN_FRAME, MF_BYCOMMAND, &CyanBmp, &CyanChkBmp);
///////////////////////////////////////////////////////////////////
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
}

void CPageListDetailView::PostNcDestroy() 
{
	// Free the C++ class.
	delete this;
}
///////////////////////////////////////////////////////////////////
//  message-handlers for setting frame-colors
void CPageListDetailView::OnRedFrame() 
{
	m_nColorFrameSelection = 0;
	m_bCheckRedFrame	= TRUE;
	m_bCheckBlackFrame	= FALSE;
	m_bCheckYellowFrame = FALSE;
	m_bCheckCyanFrame	= FALSE;

	Invalidate();
	UpdateWindow(); 
}
void CPageListDetailView::OnUpdateRedFrame(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bCheckRedFrame);
}
void CPageListDetailView::OnBlackFrame() 
{
	m_nColorFrameSelection = 1;
	m_bCheckRedFrame	= FALSE;
	m_bCheckBlackFrame	= TRUE;
	m_bCheckYellowFrame = FALSE;
	m_bCheckCyanFrame	= FALSE;

	Invalidate();
	UpdateWindow(); 
}
void CPageListDetailView::OnUpdateBlackFrame(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bCheckBlackFrame);
}
void CPageListDetailView::OnYellowFrame() 
{
	m_nColorFrameSelection = 2;
	m_bCheckRedFrame	= FALSE;
	m_bCheckBlackFrame	= FALSE;
	m_bCheckYellowFrame = TRUE;
	m_bCheckCyanFrame	= FALSE;

	Invalidate();
	UpdateWindow(); 
}
void CPageListDetailView::OnUpdateYellowFrame(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bCheckYellowFrame);
}
void CPageListDetailView::OnCyanFrame() 
{
	m_nColorFrameSelection = 3;
	m_bCheckRedFrame	= FALSE;
	m_bCheckBlackFrame	= FALSE;
	m_bCheckYellowFrame = FALSE;
	m_bCheckCyanFrame	= TRUE;

	Invalidate();
	UpdateWindow(); 
}
void CPageListDetailView::OnUpdateCyanFrame(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bCheckCyanFrame);
}
///////////////////////////////////////////////////////////////////


// This function is overridden in order to:
//		- control catchrect and drag/drop feedback
//		- protect status bars from being scrolled 
BOOL CPageListDetailView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{

//// following taken from MFC /////
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		////////////////////////////////////////////////
		// This part is changed - rest is taken from MFC
		CRect clipRect;
		GetClientRect(clipRect);
		clipRect.SubtractRect(clipRect, m_RightStatusBar);	
		ScrollWindow(-(x-xOrig), -(y-yOrig), NULL, clipRect);
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);

		OnNcPaint();
	}
	return TRUE;
}

BOOL CPageListDetailView::PreTranslateMessage(MSG* pMsg) 
{
	// Let the ToolTip process this message.
	m_tooltip.RelayEvent(pMsg);
	
	return CScrollView::PreTranslateMessage(pMsg);
}

void CPageListDetailView::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{
	CScrollView::OnNcCalcSize(bCalcValidRects, lpncsp);

	if (bCalcValidRects)
	{
		lpncsp->rgrc[0].left += 15;		
		lpncsp->rgrc[0].top  += 40;		
	}
}

void CPageListDetailView::OnNcPaint()
{
	CView::OnNcPaint();

	CDC* pDC = GetWindowDC();

	CRect rcClient;
	GetClientRect(rcClient);
	pDC->FillSolidRect(0, 0, rcClient.Width() + 50, 40, LIGHTGRAY);

	CGraphicComponent gp;

	CPageTemplate* pPageTemplate = GetPageTemplate();

	CString strFinalPageSize, strWidth, strHeight, strOut;
	strFinalPageSize.LoadString(IDS_FINALPAGESIZE);
	float fPageWidth  = (GetLayoutObject()) ? GetLayoutObject()->GetRealWidth()  : ((pPageTemplate) ? pPageTemplate->GetProductPart().m_fPageWidth  : 210.0f);
	float fPageHeight = (GetLayoutObject()) ? GetLayoutObject()->GetRealHeight() : ((pPageTemplate) ? pPageTemplate->GetProductPart().m_fPageHeight : 297.0f);
	SetMeasure(0, fPageWidth, strWidth);	// format string
	SetMeasure(0, fPageWidth, strHeight);	// format string
	CPrintSheetView::TruncateNumber(strWidth,  fPageWidth);
	CPrintSheetView::TruncateNumber(strHeight, fPageHeight);
	strOut.Format(_T(" %s: %s (%s x %s) "), strFinalPageSize, (GetLayoutObject()) ? GetLayoutObject()->m_strFormatName : ((pPageTemplate) ? pPageTemplate->GetBoundProduct().m_strFormatName : ""), strWidth, strHeight);
	CRect rcText(CPoint(15, 3), CSize(300, 20)); 
	gp.DrawTitleBar(pDC, rcText, strOut, RGB(255,220,220), RGB(255,190,190), 90, 0);

	CRect trimRect;
	trimRect.left	  = (long)(m_fTrimLeft     + 0.5f);
	trimRect.top      = (long)(m_fTrimTop      + 0.5f);
	trimRect.right    = (long)(m_fTrimRight    + 0.5f);
	trimRect.bottom   = (long)(m_fTrimBottom   + 0.5f);

//	if ( ! trimRect.IsRectEmpty())
	{
		SetMeasure(0, m_fTrimBoxWidth,  strWidth);	// format string
		SetMeasure(0, m_fTrimBoxHeight, strHeight);	// format string
		CPrintSheetView::TruncateNumber(strWidth,  m_fTrimBoxWidth);
		CPrintSheetView::TruncateNumber(strHeight, m_fTrimBoxHeight);
		if (trimRect.IsRectEmpty())
		{
			CString string;	string.LoadString(IDS_UNKNOWN_STR);
			strOut.Format(_T(" TrimBox: %s"), string);
		}
		else
			strOut.Format(_T(" TrimBox: %s x %s "), strWidth, strHeight);
		rcText = CRect(CPoint(rcText.right, 3), CSize(300, 20)); 
		gp.DrawTitleBar(pDC, rcText, strOut, RGB(220,220,255), RGB(180,180,255), 90, 0);
	}

	CRect pageRect;
	pageRect.left   = (long)m_fPageLeft; 
	pageRect.top	= (long)m_fPageTop;
	pageRect.right  = (long)m_fPageRight;
	pageRect.bottom = (long)m_fPageBottom;
	pageRect.OffsetRect(15, 40);

	float fRulerFrom = (m_fPageLeft  / m_fScale) * fPSPoint;
	float fRulerTo   = (m_fPageRight / m_fScale) * fPSPoint;
	fRulerTo  -= fRulerFrom;	// transform to 0
	fRulerFrom = 0.0f;

	pDC->LPtoDP(pageRect);

	pageRect.OffsetRect(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT));
	CRect rcRuler = pageRect;

	CRect rcRulerBar(0, 25, 2000, 40);
	gp.DrawRuler(pDC, TOP, rcRulerBar);	
	gp.DrawRuler(pDC, TOP, rcRulerBar, rcRuler, fRulerFrom, fRulerTo, rcRulerBar);	


	fRulerFrom = (m_fPageTop	/ m_fScale) * fPSPoint;
	fRulerTo   = (m_fPageBottom / m_fScale) * fPSPoint;
	fRulerTo  -= fRulerFrom;	// transform to 0
	fRulerFrom = 0.0f;

	rcRulerBar = CRect(0, 25, 15, 2000);
	gp.DrawRuler(pDC, LEFT, rcRulerBar);	
	gp.DrawRuler(pDC, LEFT, rcRulerBar, rcRuler, fRulerFrom, fRulerTo, rcRulerBar);	

	ReleaseDC(pDC);
}