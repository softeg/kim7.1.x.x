// DlgPageTemplateProps.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPageTemplateProps dialog

class CDlgPageTemplateProps : public CDialog
{
// Construction
public:
	CDlgPageTemplateProps(CWnd* pParent = NULL);   // standard constructor
	~CDlgPageTemplateProps();

// Attributes
public:
	CPageTemplate*		m_pPageTemplate;
	CPageSource*		m_pPageSource;
	CPageSourceHeader*	m_pPageSourceHeader;
	CLayoutObject*		m_pLayoutObject;
protected:
	BOOL m_bModified;
// Operations
public:

protected:

	CToolTipCtrl m_tooltip;
	CBitmap		 m_bmPageRotation;


// Dialog Data
	//{{AFX_DATA(CDlgPageTemplateProps)
	enum { IDD = IDD_PAGE_TEMPLATE_PROPS };
	float	m_fContentOffsetX;
	float	m_fContentOffsetY;
	float	m_fContentScaleX;
	float	m_fContentScaleY;
	float	m_fTrimboxWidth;
	float	m_fTrimboxHeight;
	CSpinButtonCtrl	m_contentOffsetXSpin;
	CSpinButtonCtrl	m_contentOffsetYSpin;
	CSpinButtonCtrl	m_contentScaleXSpin;
	CSpinButtonCtrl	m_contentScaleYSpin;
	CSpinButtonCtrl	m_trimboxWidthSpin;
	CSpinButtonCtrl	m_trimboxHeightSpin;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPageTemplateProps)
	public:
	virtual BOOL DestroyWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	float GetPageSourceHeaderWidth();
	float GetPageSourceHeaderHeight();
	void  DeleteColorItemsFromPropsDialog();
	void  UpdateDialogFromPageListView();
protected:
	void SetNewPageTemplateData();

	// Generated message map functions
	//{{AFX_MSG(CDlgPageTemplateProps)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnAllPages();
	afx_msg void OnThisDocPages();
	afx_msg void OnThisPage();
	afx_msg void OnPrintButton();
	virtual void OnCancel();
	afx_msg void OnApplyNow();
	afx_msg void OnChangeCenteroffsetX();
	afx_msg void OnChangeCenteroffsetY();
	afx_msg void OnLeftPages();
	afx_msg void OnLeftRightPages();
	afx_msg void OnRightPages();
	afx_msg void OnChangeScaleX();
	afx_msg void OnChangeScaleY();
	afx_msg void OnDeltaposContentOffsetXSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposContentOffsetYSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposContentScaleXSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposContentScaleYSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposTrimboxWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposTrimboxHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClose();
	afx_msg void OnSelchangePageRotationCombo();
	afx_msg void OnEnKillfocusTrimboxWidth();
	afx_msg void OnEnKillfocusTrimboxHeight();
	afx_msg void OnEnKillfocusContentscaleX();
	afx_msg void OnEnKillfocusContentscaleY();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
