#if !defined(AFX_DLGSETMARKS_H__DF6D3E72_251B_11D2_9C1B_0000C0FB9CAB__INCLUDED_)
#define AFX_DLGSETMARKS_H__DF6D3E72_251B_11D2_9C1B_0000C0FB9CAB__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// DlgSetMarks.h : header file
//

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CDlgSetMarks dialog

class CDlgSetMarks : public CDialog
{
// Construction
public:
	CDlgSetMarks(CWnd* pParent = NULL);   // standard constructor

	friend class CMarkSourceWindow;	 // CMarkSourceWindow needs access to m_MarkSource
	friend class CMarkPreViewWindow; // CMarkPreViewWindow needs access to m_markList

protected:
	CMarkListOld m_markList;

	CPageSource	  m_MarkSource;
	CArray <SEPARATIONINFO, SEPARATIONINFO>	m_ColorSeparations;			

	CString	m_strMarkEpsName;

// Dialog Data
	//{{AFX_DATA(CDlgSetMarks)
	enum { IDD = IDD_SET_MARKS };
	CScrollBar	m_ScrollBarHeight;
	CScrollBar	m_ScrollBarWidth;
	CMarkPreViewWindow	m_MarkPreViewWindow;
	CMarkSourceWindow	m_MarkSourceWindow;
	CString	m_strTextmark;
	float	m_fMarkWidth;
	float	m_fMarkHeight;
	CString	m_strMarkSetName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgSetMarks)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL RegisterMarkFiles(CString strFileName = _T(""));
	BOOL LoadMarkSet();
	void UpdateMembers(POSITION pos);
	void PositionAndCreateMarkAuto(CMarkOld *markBuffer, int nSide);

	// Generated message map functions
	//{{AFX_MSG(CDlgSetMarks)
	virtual BOOL OnInitDialog();
	afx_msg void OnPropsButton();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSelchangeMark();
	afx_msg void OnSelchangeMarkSet();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	virtual void OnOK();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGSETMARKS_H__DF6D3E72_251B_11D2_9C1B_0000C0FB9CAB__INCLUDED_)
