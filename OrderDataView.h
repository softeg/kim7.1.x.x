#pragma once

#include "OwnerDrawnButtonList.h"
#include "MyBitmapButton.h"


// CODVCustomPaintFrame

class CODVCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CODVCustomPaintFrame)

public:
	CODVCustomPaintFrame();
	virtual ~CODVCustomPaintFrame();

public:
	DWORD m_itemData;

public:
	void Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, DWORD itemData) 
	{
		CStatic::Create(lpszText, dwStyle, rect, pParentWnd, nID); 
		m_itemData = itemData; 
	};


protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};


// CODVCustomPaintFrameList

class CODVCustomPaintFrameList : public CArray <CODVCustomPaintFrame*, CODVCustomPaintFrame*>
{
	DECLARE_DYNAMIC(CODVCustomPaintFrameList)

public:
	CODVCustomPaintFrameList();
	virtual ~CODVCustomPaintFrameList();

	virtual void RemoveAll();
};



// COrderDataView-Formularansicht

class COrderDataView : public CFormView
{
	DECLARE_DYNCREATE(COrderDataView)

	DECLARE_DISPLAY_LIST(COrderDataView, OnMultiSelect, NO, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(COrderDataView, OrderData, OnSelect, YES, OnDeselect, NO, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)


protected:
	COrderDataView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~COrderDataView();

public:
	enum { IDD = IDD_ORDERDATAVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	CODVCustomPaintFrame m_jobTitleHeader, m_orderDataHeader, m_orderDataFrame;
	CMyBitmapButton		 m_gotoOrderDataButton;
	CFont				 m_bigFont;



protected:
	DECLARE_MESSAGE_MAP()
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg void OnSelectProdPart();
public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	afx_msg void OnBnClickedJdModifyJobdata();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
};


