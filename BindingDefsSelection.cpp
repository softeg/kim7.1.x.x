#include "stdafx.h"
#include "Imposition Manager.h"
#include "BindingDefsSelection.h"


IMPLEMENT_SERIAL(CBindingDefs, CObject, VERSIONABLE_SCHEMA | 1)

CBindingDefs::CBindingDefs() 
{ 
	m_strName.Empty();
	m_nBinding = PerfectBound;
}

CBindingDefs::CBindingDefs(const CBindingDefs& rBindingDefs) 
{ 
	m_strName  = rBindingDefs.m_strName;
	m_nBinding = rBindingDefs.m_nBinding;
}

CBindingDefs::~CBindingDefs()
{
	m_strName.Empty();
}

const CBindingDefs& CBindingDefs::operator=(const CBindingDefs& rBindingDefs)
{
	m_strName  = rBindingDefs.m_strName;
	m_nBinding = rBindingDefs.m_nBinding;

	return *this;
}



CBindingDefsSelection::CBindingDefsSelection(void)
{
}

CBindingDefsSelection::~CBindingDefsSelection(void)
{
}

BOOL CBindingDefsSelection::InitComboBox(CWnd* pDlg, CString& rstrBindingName, int* pnBinding, int nIDComboBox)
{
	LoadData();

	CBindingDefs bindingDefsBuffer;

	CComboBoxEx	*pBox    = (CComboBoxEx*)pDlg->GetDlgItem (nIDComboBox);
	UINT		nFormats = m_bindingDefsList.GetCount(); 
	POSITION	pos      = m_bindingDefsList.GetHeadPosition();

	pBox->ResetContent();
	for (UINT i = 0; i < nFormats; i++)    // fill combo box list 
	{
		bindingDefsBuffer = m_bindingDefsList.GetAt(pos);
		//pBox->AddString(bindingDefsBuffer.m_strName);

		COMBOBOXEXITEM item;
		item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;// | CBEIF_LPARAM;
		item.iItem			= pBox->GetCount(); 
		item.pszText		= bindingDefsBuffer.m_strName.GetBuffer(MAX_TEXT);
		item.cchTextMax		= MAX_TEXT;
		item.iImage			= bindingDefsBuffer.m_nBinding;
		item.iSelectedImage = bindingDefsBuffer.m_nBinding;
		//item.iIndent		= 0;
		pBox->InsertItem(&item);

		m_bindingDefsList.GetNext(pos);
	}	

	int nIndex = pBox->FindStringExact(-1, (LPCTSTR)rstrBindingName);

	if (nIndex == CB_ERR)
	{
		CString strString;

		bindingDefsBuffer.m_strName	 = strString;
		bindingDefsBuffer.m_nBinding = *pnBinding;
		nIndex = -1;
	}
	else
		*pnBinding = nIndex;

	pBox->SetCurSel(nIndex);

	return TRUE;  // return TRUE unless you set the focus to a control
}

void CBindingDefsSelection::SelchangeFormatList(CWnd* pDlg, int* pnBinding, CString& rstrBindingName, int nIDComboBox)
{
	CBindingDefs bindingDefsBuffer;

	CComboBox *pBox	= (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

	int nIndex = pBox->GetCurSel();
	if (nIndex == CB_ERR)	// no item selected
		return;

	POSITION pos = m_bindingDefsList.FindIndex(nIndex);
	if (pos == NULL)
		return;

	bindingDefsBuffer = m_bindingDefsList.GetAt(pos);

	rstrBindingName = bindingDefsBuffer.m_strName;
	*pnBinding		= bindingDefsBuffer.m_nBinding;

	pDlg->UpdateData(FALSE);
}

void CBindingDefsSelection::DeleteEntry(CWnd* pDlg, int* pnBinding, CString& rstrBindingName, int nIDComboBox)
{
	if (m_bindingDefsList.IsEmpty()) 
		return;

	CString strMessage;
	AfxFormatString1(strMessage, IDS_DEL_LISTENTRY, rstrBindingName);

	if (AfxMessageBox(strMessage, MB_YESNO) == IDYES)	
	{
		CComboBox *pBox  = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

		int nIndex = pBox->FindStringExact(-1, rstrBindingName);
		if (nIndex == CB_ERR)
			return;

		POSITION pos = m_bindingDefsList.FindIndex(nIndex);
		if (pos == NULL)
			return;

		m_bindingDefsList.RemoveAt(pos);
		pBox->DeleteString(nIndex);

		if (SaveData() == FALSE)
		{
			AfxMessageBox(IDS_DEL_NOTOK, MB_OK);
			LoadData();
		}
		if (!m_bindingDefsList.IsEmpty()) 
		{
			if (pBox->GetCount() - 1 < nIndex)	// last entry was deleted
				nIndex = pBox->GetCount() - 1;

			pBox->SetCurSel(nIndex);
			SelchangeFormatList(pDlg, pnBinding, rstrBindingName, nIDComboBox);	
		}
		else
		{
			rstrBindingName = "";
			*pnBinding      = CBindingDefs::PerfectBound;
		}
	}
	pDlg->UpdateData(FALSE);      
}

void CBindingDefsSelection::NewEntry(CWnd* pDlg, int* pnBinding, CString& rstrBindingName, int nIDComboBox)
{
	CBindingDefs bindingDefsBuffer;
	CComboBox  *pBox = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

	pDlg->UpdateData(TRUE);      

	rstrBindingName.TrimLeft();
	rstrBindingName.TrimRight();
	if (rstrBindingName.IsEmpty())
	{
		AfxMessageBox(IDS_INVALID_NAME);
		return;
	}

	CString strMessage;
	AfxFormatString1(strMessage, IDS_NEW_LISTENTRY, rstrBindingName);

	if (AfxMessageBox(strMessage, MB_YESNO) == IDYES)	
	{
		bindingDefsBuffer.m_strName	 = rstrBindingName;
		bindingDefsBuffer.m_nBinding = *pnBinding;
		m_bindingDefsList.AddTail(bindingDefsBuffer);

		if (SaveData() == FALSE)
			AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

		LoadData(); //reload data
		pBox->AddString(rstrBindingName);
		pBox->FindStringExact(0, rstrBindingName);
		//pBox->SetCurSel(pBox->GetCount() - 1);
		SelchangeFormatList(pDlg, pnBinding, rstrBindingName, nIDComboBox);	
	}

	pDlg->UpdateData(FALSE);      
}

void CBindingDefsSelection::ChangeEntry(CWnd* pDlg, int* pnBinding, CString& rstrBindingName, int nIDComboBox)
{
	CBindingDefs bindingDefsBuffer;
	CComboBox  *pBox = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

	pDlg->UpdateData(TRUE);      

	if (AfxMessageBox(IDS_CHANGE_ENTRY, MB_YESNO) == IDYES)		
	{
		bindingDefsBuffer.m_strName	 = rstrBindingName;
		bindingDefsBuffer.m_nBinding = *pnBinding;

		int nIndex = pBox->GetCurSel();
		if (nIndex != CB_ERR)
		{
			POSITION pos = m_bindingDefsList.FindIndex(nIndex);
			m_bindingDefsList.SetAt(pos, bindingDefsBuffer);
			if (SaveData() == FALSE)
				AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

			LoadData(); //reload data
			pBox->SetCurSel(pBox->FindStringExact(-1, bindingDefsBuffer.m_strName));
			SelchangeFormatList(pDlg, pnBinding, rstrBindingName, nIDComboBox);	
		}
	}
	pDlg->UpdateData(FALSE);      
}

BOOL CBindingDefsSelection::LoadData()
{
	TCHAR			pszFileName[MAX_PATH];
	CFile			file;
	CFileException	fileException;

	_tcscpy(pszFileName,	   theApp.GetDataFolder()); 
	_tcscat(pszFileName,	   _T("\\BindingDefs.def"));

	m_bindingDefsList.RemoveAll(); // Be sure to work with an empty list
	if ( ! file.Open(pszFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n",
				pszFileName, fileException.m_cause);

		CBindingDefs bindingDefsBuffer;
		bindingDefsBuffer.m_nBinding = CBindingDefs::PerfectBound;
		bindingDefsBuffer.m_strName.LoadString(IDS_PERFECTBOUND_TOOLTIP);
		m_bindingDefsList.AddTail(bindingDefsBuffer);
		bindingDefsBuffer.m_nBinding = CBindingDefs::SaddleStitched;
		bindingDefsBuffer.m_strName.LoadString(IDS_SADDLESTITCHED_TOOLTIP);
		m_bindingDefsList.AddTail(bindingDefsBuffer);

		return FALSE; 
	}
	else
	{
		CArchive archive(&file, CArchive::load);
		m_bindingDefsList.Serialize(archive);

		archive.Close();
		file.Close();
		return TRUE;  	
	}
}

BOOL CBindingDefsSelection::SaveData()
{
	TCHAR		   pszFileName[MAX_PATH];
	CFile		   file;
	CFileException fileException;

	_tcscpy(pszFileName, theApp.GetDataFolder()); 
	_tcscat(pszFileName, _T("\\BindingDefs.def"));
	if ( ! file.Open(pszFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n",
				pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&file, CArchive::store);
		m_bindingDefsList.Serialize(archive);

		archive.Close();
		file.Close();

		return TRUE;
	}
}

int CBindingDefsSelection::GetBindingDefsIndex(CBindingDefs& rBindingDefs, BOOL bBoth)
{
	if ( ! m_bindingDefsList.GetCount())
		LoadData();

	int		 nIndex = 0;
	POSITION pos	= m_bindingDefsList.GetHeadPosition();
	while (pos)
	{
		if (m_bindingDefsList.GetNext(pos).m_strName == rBindingDefs.m_strName)
			return nIndex;
		nIndex++;
	}

	if (bBoth)
		return -1;

	// not found yet
	nIndex = 0;
	pos = m_bindingDefsList.GetHeadPosition();
	while (pos)
	{
		if (m_bindingDefsList.GetNext(pos).m_nBinding == rBindingDefs.m_nBinding)
			return nIndex;
		nIndex++;
	}
	if (bBoth)
		return -1;
	else
		return 0;
}

CBindingDefs& CBindingDefsSelection::GetBindingDefs(int nIndex)
{
	POSITION pos = m_bindingDefsList.FindIndex(nIndex);
	if (pos == NULL)
		return m_nullBindingDefs;

	return m_bindingDefsList.GetAt(pos);
}


// helper function for PDFTargetList elements
template <> void AFXAPI SerializeElements <CBindingDefs> (CArchive& ar, CBindingDefs* pBindingDefs, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		ar.SerializeClass(RUNTIME_CLASS(CBindingDefs));

		if (ar.IsStoring())
		{
			ar << pBindingDefs->m_strName;
			ar << pBindingDefs->m_nBinding;
		}
		else
		{
			UINT nVersion = ar.GetObjectSchema();
			switch (nVersion)
			{
			case 1:	ar >> pBindingDefs->m_strName;
					ar >> pBindingDefs->m_nBinding;
					break;
			default:break;
			}
		}
		
		pBindingDefs++;
	}
}

void AFXAPI CBindingDefsSelection::DDX_BindingDefsCombo(CDataExchange* pDX, int nIDC, CString& strValue)
{
	TCHAR szValue[MAX_TEXT];

	HWND hWndCtrl = pDX->PrepareEditCtrl(nIDC);
	if (pDX->m_bSaveAndValidate)
	{
		::GetWindowText(hWndCtrl, szValue, MAX_TEXT);  
		strValue = szValue;
	}
	else
		::SetWindowText(hWndCtrl, (LPCTSTR)strValue);
}
