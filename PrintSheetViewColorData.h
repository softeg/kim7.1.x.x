#pragma once

#include "ColorDataWindow.h"



// CPrintSheetViewColorData-Formularansicht

class CPrintSheetViewColorData : public CFormView
{
	DECLARE_DYNCREATE(CPrintSheetViewColorData)

protected:
	CPrintSheetViewColorData();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetViewColorData();

public:
	enum { IDD = IDD_PRINTSHEETVIEWCOLORDATA };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


public:
	CColorDataWindow m_colorDataWindow;


public:
	CPrintSheet*	GetActPrintSheet();
	CColorantList	GetActColorants();



protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);


	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};


