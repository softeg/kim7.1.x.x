// DlgSettings.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgDirPicker.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
//#include "AutoServerView.h"
#include "MainFrm.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CSettingsPage1, CPropertyPage)
IMPLEMENT_DYNCREATE(CSettingsPage2, CPropertyPage)
IMPLEMENT_DYNCREATE(CSettingsPage3, CPropertyPage)
IMPLEMENT_DYNCREATE(CSettingsPage4, CPropertyPage)


/////////////////////////////////////////////////////////////////////////////
// CSettings
// Loading of global settings for the application

BOOL CSettings::OnLoadingSettings()
{
	CString	  strInstallationFolder = theApp.GetDefaultInstallationFolder();
	const int nValueLenght = max(_MAX_PATH, MAX_TEXT) + 2;
	TCHAR	  szValue[nValueLenght];
	DWORD	  dwSize;
	dwSize = nValueLenght;
	HKEY hKey;
	DWORD dwDisposition; 
	CString strKey = theApp.m_strRegistryKey;
	long ret = RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition); 

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("DataFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szDataDir, szValue);
	else
		_tcscpy(theApp.settings.m_szDataDir, strInstallationFolder + _T("\\Data"));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("JobFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szJobsDir, szValue);
	else
		_tcscpy(theApp.settings.m_szJobsDir, strInstallationFolder + _T("\\Jobs"));

	dwSize = nValueLenght;
	if (RegQueryValueEx(hKey, _T("SchemeFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szSchemeDir, szValue);
	else
		_tcscpy(theApp.settings.m_szSchemeDir, strInstallationFolder + _T("\\Imposition Schemes"));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("PDFInputFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szPDFInputFolder, szValue);
	else
		_tcscpy(theApp.settings.m_szPDFInputFolder, strInstallationFolder + _T("\\PDF-Input"));
	dwSize = nValueLenght;
	if (RegQueryValueEx(hKey, _T("PDFPreviewFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szPDFPreviewFolder, szValue);
	else
		_tcscpy(theApp.settings.m_szPDFPreviewFolder, strInstallationFolder + _T("\\PDFPreviews"));
	dwSize = nValueLenght;
	if (RegQueryValueEx(hKey, _T("PDFPreviewsOrigPlace"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		theApp.settings.m_bPDFPreviewsOrigPlace = (_tcscmp(szValue, _T("TRUE")) == 0) ? TRUE : FALSE;

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("MarkFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szMarksDir, szValue);
	else
		_tcscpy(theApp.settings.m_szMarksDir, strInstallationFolder + _T("\\PDF-Marks"));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("StandardLayoutFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szStandardsDir, szValue);
	else
		_tcscpy(theApp.settings.m_szStandardsDir, strInstallationFolder + _T("\\Standards"));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("TempFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szTempDir, szValue);
	else
		_tcscpy(theApp.settings.m_szTempDir,	strInstallationFolder + _T("\\Temp"));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("Language"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szLanguage, szValue);
	else
		_tcscpy(theApp.settings.m_szLanguage, _T("GER"));
			if (_tcscmp (theApp.settings.m_szLanguage, _T("GER")) == 0)
				theApp.settings.m_iLanguage = 0;
			if (_tcscmp (theApp.settings.m_szLanguage, _T("ENG")) == 0)
				theApp.settings.m_iLanguage = 1;
			if (_tcscmp (theApp.settings.m_szLanguage, _T("FRA")) == 0)
				theApp.settings.m_iLanguage = 2;
			if (_tcscmp (theApp.settings.m_szLanguage, _T("ITA")) == 0)
				theApp.settings.m_iLanguage = 3;
			if (_tcscmp (theApp.settings.m_szLanguage, _T("SPA")) == 0)
				theApp.settings.m_iLanguage = 4;
			if (_tcscmp (theApp.settings.m_szLanguage, _T("HOL")) == 0)
				theApp.settings.m_iLanguage = 5;
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("Unit"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szUnit, szValue);
	else
		_tcscpy(theApp.settings.m_szUnit, _T("MM"));
			if (_tcscmp (theApp.settings.m_szUnit, _T("MM")) == 0)
				theApp.settings.m_iUnit = MILLIMETER;
			if (_tcscmp (theApp.settings.m_szUnit, _T("CM")) == 0)
				theApp.settings.m_iUnit = CENTIMETER;
			if (_tcscmp (theApp.settings.m_szUnit, _T("INCH")) == 0)
				theApp.settings.m_iUnit = INCH;
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("SheetsPerPage"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szSheetsPerPage, szValue);
	else
		_tcscpy(theApp.settings.m_szSheetsPerPage, _T("4"));
	theApp.settings.m_nSheetsPerPage = _ttoi(m_szSheetsPerPage);

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("AutoServerNameIP"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szAutoServerNameIP, szValue);
	else
		_tcscpy(theApp.settings.m_szAutoServerNameIP, _T(""));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("sPrintOneTenantID"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szSPrintOneTenantID, szValue);
	else
		_tcscpy(theApp.settings.m_szSPrintOneTenantID, _T(""));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("SPrintOneUserID"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szSPrintOneUserID, szValue);
	else
		_tcscpy(theApp.settings.m_szSPrintOneUserID, _T(""));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("SPrintOnePassword"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szSPrintOnePassword, szValue);
	else
		_tcscpy(theApp.settings.m_szSPrintOnePassword, _T(""));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("FrontName"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szFrontName, szValue);
	else
		_tcscpy(theApp.settings.m_szFrontName, _T("Front"));
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("BackName"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szBackName, szValue);
	else
		_tcscpy(theApp.settings.m_szBackName, _T("Back"));
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("LayoutName"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szLayoutName, szValue);
	else
		_tcscpy(theApp.settings.m_szLayoutName, _T("Layout"));
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("FoldSheetName"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szFoldSheetName, szValue);
	else
		_tcscpy(theApp.settings.m_szFoldSheetName, _T("FoldSheet"));
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("OnCheckForProductType"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szCheckForProductType, szValue);
	else
		_tcscpy(theApp.settings.m_szCheckForProductType, _T("TRUE"));
			if (_tcscmp (theApp.settings.m_szCheckForProductType, _T("TRUE")) == 0)
				theApp.settings.m_bCheckForProductType = TRUE;
			else
				theApp.settings.m_bCheckForProductType = FALSE;
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("WorkAndTurnName"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szWorkAndTurnName, szValue);
	else
		_tcscpy(theApp.settings.m_szWorkAndTurnName, _T("Zum Umschlagen"));
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("WorkAndTumbleName"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szWorkAndTumbleName, szValue);
	else
		_tcscpy(theApp.settings.m_szWorkAndTumbleName, _T("Zum Umst�lpen"));
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("SingleSidedName"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szSingleSidedName, szValue);
	else
		_tcscpy(theApp.settings.m_szSingleSidedName, _T("Einseitig"));

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("FrontRearParallel"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szFrontRearParallel, szValue);
	else
		_tcscpy(theApp.settings.m_szFrontRearParallel, _T("TRUE"));
			if (_tcscmp (theApp.settings.m_szFrontRearParallel, _T("TRUE")) == 0)
				theApp.settings.m_bFrontRearParallel = TRUE;
			else
				theApp.settings.m_bFrontRearParallel = FALSE;
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("OnStartFileNew"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szOnStartFileNew, szValue);
	else
		_tcscpy(theApp.settings.m_szOnStartFileNew, _T("TRUE"));
			if (_tcscmp (theApp.settings.m_szOnStartFileNew, _T("TRUE")) == 0)
				theApp.settings.m_bOnStartFileNew = TRUE;
			else
				theApp.settings.m_bOnStartFileNew = FALSE;
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("OnStartDelTemp"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szOnStartDelTemp, szValue);
	else
		_tcscpy(theApp.settings.m_szOnStartDelTemp, _T("TRUE"));
			if (_tcscmp (theApp.settings.m_szOnStartDelTemp, _T("TRUE")) == 0)
				theApp.settings.m_bOnStartDelTemp = TRUE;
			else
				theApp.settings.m_bOnStartDelTemp = FALSE;


	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("AutoBackup"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szAutoBackup, szValue);
	else
		_tcscpy(theApp.settings.m_szAutoBackup, _T("FALSE"));
			if (_tcscmp (theApp.settings.m_szAutoBackup, _T("TRUE")) == 0)
				theApp.settings.m_bAutoBackup = TRUE;
			else
				theApp.settings.m_bAutoBackup = FALSE;
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("AutoBackupTime"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szAutoBackupTime, szValue);
	else
		_tcscpy(theApp.settings.m_szAutoBackupTime, _T("30"));
	theApp.settings.m_iAutoBackupTime = _ttoi(m_szAutoBackupTime);


	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("RipBitmapGenerate"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szRipBitmapGenerate, szValue);
	else
		_tcscpy(theApp.settings.m_szRipBitmapGenerate, _T("TRUE"));
	if (_tcscmp (theApp.settings.m_szRipBitmapGenerate, _T("TRUE")) == 0)
	{
		theApp.settings.m_bRipBitmapGenerate = TRUE;

		dwSize = sizeof(szValue);
		if (RegQueryValueEx(hKey, _T("RipBitmapColorDepth"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
			_tcscpy(theApp.settings.m_szRipBitmapColorDepth, szValue);
		else
			_tcscpy(theApp.settings.m_szRipBitmapColorDepth, _T("8"));
				if (_tcscmp (theApp.settings.m_szRipBitmapColorDepth, _T("1")) == 0)
					theApp.settings.m_iRipBitmapColorDepth = 1;
				if (_tcscmp (theApp.settings.m_szRipBitmapColorDepth, _T("8")) == 0)
					theApp.settings.m_iRipBitmapColorDepth = 2;

		dwSize = sizeof(szValue);
		if (RegQueryValueEx(hKey, _T("RipBitmapResolution"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
			_tcscpy(theApp.settings.m_szRipBitmapResolution, szValue);
		else
			_tcscpy(theApp.settings.m_szRipBitmapResolution, _T("72"));
				if (_tcscmp (theApp.settings.m_szRipBitmapResolution, _T("36")) == 0)
					theApp.settings.m_iRipBitmapResolution = 0;
				if (_tcscmp (theApp.settings.m_szRipBitmapResolution, _T("72")) == 0)
					theApp.settings.m_iRipBitmapResolution = 1;
				if (_tcscmp (theApp.settings.m_szRipBitmapResolution, _T("144")) == 0)
					theApp.settings.m_iRipBitmapResolution = 2;
				if (_tcscmp (theApp.settings.m_szRipBitmapResolution, _T("300")) == 0)
					theApp.settings.m_iRipBitmapResolution = 3;
	}
	else
	{
		theApp.settings.m_bRipBitmapGenerate	= FALSE;
		_tcscpy(theApp.settings.m_szRipBitmapColorDepth, _T("0"));
		theApp.settings.m_iRipBitmapColorDepth	= 0;
		_tcscpy(theApp.settings.m_szRipBitmapResolution, _T("72"));
		theApp.settings.m_iRipBitmapResolution	= 1;
	}
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("RipBBoxDSC"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcscpy(theApp.settings.m_szRipBBoxDSC, szValue);
	else
		_tcscpy(theApp.settings.m_szRipBBoxDSC, _T("0"));
			if (_tcscmp (theApp.settings.m_szRipBBoxDSC, _T("0")) == 0)
				theApp.settings.m_iRipBBoxDSC = 0;
			else
				theApp.settings.m_iRipBBoxDSC = 1;

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("AutoCheckDoublePages"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		theApp.settings.m_bAutoCheckDoublePages = (_tcscmp(szValue, _T("TRUE")) == 0) ? TRUE : FALSE;
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("TrimboxPagesizeMismatchWarning"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		theApp.settings.m_bTrimboxPagesizeMismatchWarning = (_tcscmp(szValue, _T("TRUE")) == 0) ? TRUE : FALSE;
	else
		theApp.settings.m_bTrimboxPagesizeMismatchWarning = TRUE;

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("AutoWatchPageSources"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		theApp.settings.m_bAutoWatchPageSources = (_tcscmp(szValue, _T("TRUE")) == 0) ? TRUE : FALSE;
	else
		theApp.settings.m_bAutoWatchPageSources = TRUE;

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("LastPageFormat"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcsncpy(theApp.settings.m_szLastPageFormat, szValue, MAX_TEXT);
	else
		_tcsncpy(theApp.settings.m_szLastPageFormat, _T(""), MAX_TEXT);

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("LastBindingStyle"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcsncpy(theApp.settings.m_szLastBindingStyle, szValue, MAX_TEXT);
	else
		_tcsncpy(theApp.settings.m_szLastBindingStyle, _T(""), MAX_TEXT);

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("LastProductionStyle"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_tcsncpy(theApp.settings.m_szLastProductionStyle, szValue, MAX_TEXT);
	else
		_tcsncpy(theApp.settings.m_szLastProductionStyle, _T("Normal"), MAX_TEXT);

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("DefaultBleed"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		theApp.settings.m_fDefaultBleed = (float)_tstof(szValue);
	else
		theApp.settings.m_fDefaultBleed = 0.0f;

	ret = RegCloseKey(hKey);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CSettingsPage1 property page

CSettingsPage1::CSettingsPage1() : CPropertyPage(CSettingsPage1::IDD)
{
	//{{AFX_DATA_INIT(CSettingsPage1)
	m_strBackName = _T("");
	m_strFrontName = _T("");
	m_strFoldSheetTypeName = _T("");
	m_strLayoutName = _T("");
	m_strWorkAndTumbleName = _T("");
	m_strWorkAndTurnName = _T("");
	m_strSingleSidedName = _T("");
	m_bCheckForProductType = FALSE;
	m_fDefaultBleed = 0.0f;
	//}}AFX_DATA_INIT
}

CSettingsPage1::~CSettingsPage1()
{
}

void CSettingsPage1::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingsPage1)
	DDX_Text(pDX, IDC_BACKNAME, m_strBackName);
	DDV_MaxChars(pDX, m_strBackName, 15);
	DDX_Text(pDX, IDC_FRONTNAME, m_strFrontName);
	DDV_MaxChars(pDX, m_strFrontName, 15);
	DDX_Text(pDX, IDC_FOLDSHEETNAME, m_strFoldSheetTypeName);
	DDV_MaxChars(pDX, m_strFoldSheetTypeName, 15);
	DDX_Text(pDX, IDC_LAYOUTNAME, m_strLayoutName);
	DDV_MaxChars(pDX, m_strLayoutName, 15);
	DDX_Text(pDX, IDC_WORKANDTUMBLE_NAME, m_strWorkAndTumbleName);
	DDV_MaxChars(pDX, m_strWorkAndTumbleName, 15);
	DDX_Text(pDX, IDC_WORKANDTURN_NAME, m_strWorkAndTurnName);
	DDV_MaxChars(pDX, m_strWorkAndTurnName, 15);
	DDX_Text(pDX, IDC_SINGLESIDED_NAME, m_strSingleSidedName);
	DDV_MaxChars(pDX, m_strSingleSidedName, 15);
	DDX_Check(pDX, IDC_CHECK_PRODUCTTYPE, m_bCheckForProductType);
	DDX_Measure(pDX, IDC_DEFAULT_BLEED_EDIT, m_fDefaultBleed);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSettingsPage1, CPropertyPage)
	//{{AFX_MSG_MAP(CSettingsPage1)
	ON_BN_CLICKED(IDC_CHECK_PRODUCTTYPE, OnCheckProducttype)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSettingsPage2 property page

CSettingsPage2::CSettingsPage2() : CPropertyPage(CSettingsPage2::IDD)
{
	//{{AFX_DATA_INIT(CSettingsPage2)
	m_strDataDir = _T("");
	m_strJobsDir = _T("");
	m_strWorkDir = _T("");
	m_strSchemeDir = _T("");
	m_strPSDir = _T("");
	m_strTempDir = _T("");
	m_strStandardsDir = _T("");
	m_strMarksDir = _T("");
	m_strEPSDir = _T("");
	m_bEPSDirCheck = FALSE;
	m_strPDFInputFolder = _T("");
	m_strPDFPreviewFolder = _T("");
	m_bPDFPreviewsOrigPlace = FALSE;
	//}}AFX_DATA_INIT
}

CSettingsPage2::~CSettingsPage2()
{
}

void CSettingsPage2::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingsPage2)
	DDX_Text(pDX, IDC_DATADIR, m_strDataDir);
	DDV_MaxChars(pDX, m_strDataDir, 256);
	DDX_Text(pDX, IDC_JOBSDIR, m_strJobsDir);
	DDV_MaxChars(pDX, m_strJobsDir, 256);
	DDX_Text(pDX, IDC_WORKDIR, m_strWorkDir);
	DDV_MaxChars(pDX, m_strWorkDir, 256);
	DDX_Text(pDX, IDC_SCHEMEDIR, m_strSchemeDir);
	DDV_MaxChars(pDX, m_strSchemeDir, 256);
	DDX_Text(pDX, IDC_TEMPDIR, m_strTempDir);
	DDV_MaxChars(pDX, m_strTempDir, 256);
	DDX_Text(pDX, IDC_STANDARDSDIR, m_strStandardsDir);
	DDV_MaxChars(pDX, m_strStandardsDir, 256);
	DDX_Text(pDX, IDC_MARKSDIR, m_strMarksDir);
	DDV_MaxChars(pDX, m_strMarksDir, 256);
	//}}AFX_DATA_MAP
#ifdef PDF
	DDX_Text(pDX, IDC_PDF_INPUTFOLDER, m_strPDFInputFolder);
	DDX_Text(pDX, IDC_PREVIEW_FOLDER, m_strPDFPreviewFolder);
	DDX_Check(pDX, IDC_PREVIEW_FOLDER_CHECK, m_bPDFPreviewsOrigPlace);
#endif
}


BEGIN_MESSAGE_MAP(CSettingsPage2, CPropertyPage)
	//{{AFX_MSG_MAP(CSettingsPage2)
	ON_BN_CLICKED(IDC_BROWSE1, OnBrowse1)
	ON_BN_CLICKED(IDC_BROWSE2, OnBrowse2)
	ON_BN_CLICKED(IDC_BROWSE3, OnBrowse3)
	ON_BN_CLICKED(IDC_BROWSE4, OnBrowse4)
	ON_BN_CLICKED(IDC_BROWSE5, OnBrowse5)
	ON_BN_CLICKED(IDC_BROWSE6, OnBrowse6)
	ON_BN_CLICKED(IDC_BROWSE7, OnBrowse7)
	ON_BN_CLICKED(IDC_BROWSE8, OnBrowse8)
	ON_BN_CLICKED(IDC_PREVIEW_FOLDER_CHECK, OnPreviewFolderCheck)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSettingsPage3 property page

CSettingsPage3::CSettingsPage3() : CPropertyPage(CSettingsPage3::IDD)
{
	//{{AFX_DATA_INIT(CSettingsPage3)
	m_iLanguage = -1;
	m_iUnit = -1;
	m_nSheetsPerPage = 0;
	m_strAutoServerNameIP = _T("");
	//}}AFX_DATA_INIT
}

CSettingsPage3::~CSettingsPage3()
{
}

void CSettingsPage3::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingsPage3)
	DDX_Control(pDX, IDC_PRINTSHEETPERPAGE_SPIN, m_PrintSheetPerPageSpin);
	DDX_CBIndex(pDX, IDC_LANGUAGE, m_iLanguage);
	DDX_CBIndex(pDX, IDC_UNIT, m_iUnit);
	DDX_Text(pDX, IDC_PRINTSHEETPERPAGE_EDIT, m_nSheetsPerPage);
	DDV_MinMaxUInt(pDX, m_nSheetsPerPage, 2, 20);
	DDX_Text(pDX, IDC_SETTING_AUTOSERVERNAME_IP, m_strAutoServerNameIP);
	DDX_Text(pDX, IDC_SETTING_SPRINTONE_TENANTID, m_strSPrintOneTenantID);
	DDX_Text(pDX, IDC_SETTING_SPRINTONE_USERID, m_strSPrintOneUserID);
	DDX_Text(pDX, IDC_SETTING_SPRINTONE_PASSWORD, m_strSPrintOnePassword);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSettingsPage3, CPropertyPage)
	//{{AFX_MSG_MAP(CSettingsPage3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSettingsPage4 property page

CSettingsPage4::CSettingsPage4() : CPropertyPage(CSettingsPage4::IDD)
{
	//{{AFX_DATA_INIT(CSettingsPage4)
	m_bFrontRearParallel = FALSE;
	m_bOnStartFileNew	 = FALSE;
	m_bOnStartDelTemp	 = FALSE;
	m_bRipBitmapGenerate = FALSE;
	m_iRipBBoxDSC = -1;
	m_nAutoBackupTime = 0;
	m_bAutoBackup = FALSE;
	m_bAutoCheckDoublePages = FALSE;
	m_bTrimboxPagesizeMismatchWarning = FALSE;
	//}}AFX_DATA_INIT
}

CSettingsPage4::~CSettingsPage4()
{
}

void CSettingsPage4::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSettingsPage4)
	DDX_Control(pDX, IDC_AUTOBACKUPTIMESPIN, m_AutoBackupSpin);
	DDX_Check(pDX, IDC_FRONTREAR_PARALLEL, m_bFrontRearParallel);
	DDX_Check(pDX, IDC_ONSTART_FILENEW, m_bOnStartFileNew);
	DDX_Check(pDX, IDC_ONSTART_DELTEMP, m_bOnStartDelTemp);
	DDX_Text(pDX, IDC_AUTOBACKUPTIME, m_nAutoBackupTime);
	DDV_MinMaxUInt(pDX, m_nAutoBackupTime, 2, 240);
	DDX_Check(pDX, IDC_ONAUTOBACKUP, m_bAutoBackup);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_AUTOCHECK_DOUBLEPAGE, m_bAutoCheckDoublePages);
	DDX_Check(pDX, IDC_TRIMBOX_PAGEFORMAT_MISMATCH_CHECK, m_bTrimboxPagesizeMismatchWarning);
	DDX_Check(pDX, IDC_AUTOWATCH_PAGESOURCES, m_bAutoWatchPageSources);
}


BEGIN_MESSAGE_MAP(CSettingsPage4, CPropertyPage)
	//{{AFX_MSG_MAP(CSettingsPage4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSettingsSheet

IMPLEMENT_DYNAMIC(CSettingsSheet, CPropertySheet)

CSettingsSheet::CSettingsSheet(CWnd* pWndParent)
	 : CPropertySheet(IDS_SETTINGS_TITLE, pWndParent)
{
	// Add all of the property pages here.  Note that
	// the order that they appear in here will be
	// the order they appear in on screen.  By default,
	// the first page of the set is the active one.
	// One way to make a different property page the 
	// active one is to call SetActivePage().

	m_psh.dwFlags|= PSH_NOAPPLYNOW;  // no apply button
#ifdef CIPCUT
	AddPage(&m_Page2);
	AddPage(&m_Page3);
#else
	AddPage(&m_Page1);
	AddPage(&m_Page2);
	AddPage(&m_Page3);
	AddPage(&m_Page4);
#endif
}

CSettingsSheet::~CSettingsSheet()
{
}


BEGIN_MESSAGE_MAP(CSettingsSheet, CPropertySheet)
	//{{AFX_MSG_MAP(CSettingsSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSettingsSheet message handlers

BOOL CSettingsSheet::OnInitDialog()
{
	BOOL bResult = CPropertySheet::OnInitDialog();

	CenterWindow();
	return bResult;
}

BOOL CSettingsPage1::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	m_strFrontName		   = theApp.settings.m_szFrontName;
	m_strBackName		   = theApp.settings.m_szBackName;
	m_strLayoutName		   = theApp.settings.m_szLayoutName;
	m_strFoldSheetTypeName = theApp.settings.m_szFoldSheetName;
	m_strWorkAndTurnName   = theApp.settings.m_szWorkAndTurnName;
	m_strWorkAndTumbleName = theApp.settings.m_szWorkAndTumbleName;
	m_strSingleSidedName   = theApp.settings.m_szSingleSidedName;
	m_bCheckForProductType = theApp.settings.m_bCheckForProductType;
	m_fDefaultBleed		   = theApp.settings.m_fDefaultBleed;
	CEdit *pEdit = (CEdit*)GetDlgItem(IDC_WORKANDTURN_NAME);
	pEdit->EnableWindow(m_bCheckForProductType);
	pEdit		 = (CEdit*)GetDlgItem(IDC_WORKANDTUMBLE_NAME);
	pEdit->EnableWindow(m_bCheckForProductType);
/////////////////////////////////////////////////////////////////////////////////////////////
//  Tooltips	
	m_tooltip.Create(this);
	m_tooltip.Activate(TRUE);

	CRect rect1, rect2, uRect;
	GetDlgItem(IDC_WORKANDTURN_ICON)->GetWindowRect(&rect1);
	GetDlgItem(IDC_WORKANDTURN_NAME)->GetWindowRect(&rect2);
	uRect.UnionRect(rect1, rect2);
	ScreenToClient(uRect);
	m_tooltip.AddTool(this, IDS_PRODUCTTYPE_TOOLTIP, uRect, 1);
	GetDlgItem(IDC_WORKANDTUMBLE_ICON)->GetWindowRect(&rect1);
	GetDlgItem(IDC_WORKANDTUMBLE_NAME)->GetWindowRect(&rect2);
	uRect.UnionRect(rect1, rect2);
	ScreenToClient(uRect);
	m_tooltip.AddTool(this, IDS_PRODUCTTYPE_TOOLTIP, uRect, 1);
	m_tooltip.AddTool(GetDlgItem(IDC_CHECK_PRODUCTTYPE), IDS_ANALYSEPRODUCTTYPE_TOOLTIP);
/////////////////////////////////////////////////////////////////////////////////////////////
	UpdateData(FALSE);      
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
BOOL CSettingsPage1::PreTranslateMessage(MSG* pMsg) 
{
	m_tooltip.RelayEvent(pMsg);
	
	return CPropertyPage::PreTranslateMessage(pMsg);
}

void CSettingsPage1::OnCheckProducttype() 
{
	UpdateData(TRUE);
	CEdit *pEdit = (CEdit*)GetDlgItem(IDC_WORKANDTURN_NAME);
	pEdit->EnableWindow(m_bCheckForProductType);
	pEdit		 = (CEdit*)GetDlgItem(IDC_WORKANDTUMBLE_NAME);
	pEdit->EnableWindow(m_bCheckForProductType);
}

BOOL CSettingsPage2::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	m_strWorkDir			= theApp.settings.m_szWorkDir;
	m_strDataDir			= theApp.settings.m_szDataDir;
	m_strJobsDir			= theApp.settings.m_szJobsDir;
	m_strSchemeDir			= theApp.settings.m_szSchemeDir;
	m_strPSDir				= theApp.settings.m_szPSDir;
	m_strEPSDir				= theApp.settings.m_szEPSDir;
	m_bEPSDirCheck			= theApp.settings.m_bEPSDirDisabled;
	m_strPDFInputFolder		= theApp.settings.m_szPDFInputFolder;
	m_strPDFPreviewFolder	= theApp.settings.m_szPDFPreviewFolder;
	m_bPDFPreviewsOrigPlace	= theApp.settings.m_bPDFPreviewsOrigPlace;

#ifdef PDF
	CEdit *pEdit = (CEdit*)GetDlgItem (IDC_PREVIEW_FOLDER);
	pEdit->EnableWindow( ! m_bPDFPreviewsOrigPlace);
	CButton *pButton		= (CButton*)GetDlgItem (IDC_BROWSE8);
	pButton->EnableWindow( ! m_bPDFPreviewsOrigPlace);
#else
	CEdit *pEdit = (CEdit*)GetDlgItem (IDC_EPSDIR);
	pEdit->EnableWindow(m_bEPSDirCheck);
	CButton *pButton		= (CButton*)GetDlgItem (IDC_BROWSE8);
	pButton->EnableWindow(m_bEPSDirCheck);
#endif

	m_strMarksDir			= theApp.settings.m_szMarksDir;
	m_strStandardsDir		= theApp.settings.m_szStandardsDir;
	m_strTempDir			= theApp.settings.m_szTempDir;

	UpdateData(FALSE);      
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CSettingsPage3::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	m_iLanguage			 = theApp.settings.m_iLanguage;
	m_iUnit				 = theApp.settings.m_iUnit;
	
	CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_LANGUAGE);
	pBox->SetCurSel(m_iLanguage);
	pBox = (CComboBox*)GetDlgItem (IDC_UNIT);
	pBox->SetCurSel(m_iUnit);

	m_nSheetsPerPage = theApp.settings.m_nSheetsPerPage;
	m_PrintSheetPerPageSpin.SetRange(2, 20);

	m_strAutoServerNameIP  = theApp.settings.m_szAutoServerNameIP;
	m_strSPrintOneTenantID = theApp.settings.m_szSPrintOneTenantID;
	m_strSPrintOneUserID   = theApp.settings.m_szSPrintOneUserID;
	m_strSPrintOnePassword = theApp.settings.m_szSPrintOnePassword;

	UpdateData(FALSE);      
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CSettingsPage4::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	m_bFrontRearParallel = theApp.settings.m_bFrontRearParallel;
	m_bOnStartFileNew	 = theApp.settings.m_bOnStartFileNew;
	m_bOnStartDelTemp	 = theApp.settings.m_bOnStartDelTemp;

	UDACCEL AcceleratorSpin;
	AcceleratorSpin.nSec = 1;
	AcceleratorSpin.nInc = 1;
	m_AutoBackupSpin.SetAccel(1, &AcceleratorSpin);
	m_AutoBackupSpin.SetRange(2, 240);
	m_nAutoBackupTime = theApp.settings.m_iAutoBackupTime;
	m_bAutoBackup = theApp.settings.m_bAutoBackup;
	m_bAutoCheckDoublePages	= theApp.settings.m_bAutoCheckDoublePages;
	m_bTrimboxPagesizeMismatchWarning = theApp.settings.m_bTrimboxPagesizeMismatchWarning;
	m_bAutoWatchPageSources	= theApp.settings.m_bAutoWatchPageSources;

	UpdateData(FALSE);      
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSettingsPage1::OnOK() 
{
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	long ret = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	if (_tcscmp(m_strFrontName, theApp.settings.m_szFrontName) != 0) 
		RegSetValueEx(hKey, _T("FrontName"), 0, REG_SZ, (LPBYTE)m_strFrontName.GetBuffer(16), 16 * sizeof(TCHAR));
	if (_tcscmp(m_strBackName, theApp.settings.m_szBackName) != 0) 
		RegSetValueEx(hKey, _T("BackName"), 0, REG_SZ, (LPBYTE)m_strBackName.GetBuffer(16), 16 * sizeof(TCHAR));
	if (_tcscmp(m_strLayoutName, theApp.settings.m_szLayoutName) != 0) 
		RegSetValueEx(hKey, _T("LayoutName"), 0, REG_SZ, (LPBYTE)m_strLayoutName.GetBuffer(16), 16 * sizeof(TCHAR));
	if (_tcscmp(m_strFoldSheetTypeName, theApp.settings.m_szFoldSheetName) != 0) 
		RegSetValueEx(hKey, _T("FoldSheetName"), 0, REG_SZ, (LPBYTE)m_strFoldSheetTypeName.GetBuffer(16), 16 * sizeof(TCHAR));
	if (m_bCheckForProductType)
		RegSetValueEx(hKey, _T("OnCheckForProductType"), 0, REG_SZ, (LPBYTE)_T("TRUE"),  5 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("OnCheckForProductType"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));
	if (_tcscmp(m_strWorkAndTurnName, theApp.settings.m_szWorkAndTurnName) != 0) 
		RegSetValueEx(hKey, _T("WorkAndTurnName"), 0, REG_SZ, (LPBYTE)m_strWorkAndTurnName.GetBuffer(16), 16 * sizeof(TCHAR));
	if (_tcscmp(m_strWorkAndTumbleName, theApp.settings.m_szWorkAndTumbleName) != 0) 
		RegSetValueEx(hKey, _T("WorkAndTumbleName"), 0, REG_SZ, (LPBYTE)m_strWorkAndTumbleName.GetBuffer(16), 16 * sizeof(TCHAR));
	if (_tcscmp(m_strWorkAndTumbleName, theApp.settings.m_szSingleSidedName) != 0) 
		RegSetValueEx(hKey, _T("SingleSidedName"), 0, REG_SZ, (LPBYTE)m_strSingleSidedName.GetBuffer(16), 16 * sizeof(TCHAR));

	CString strValue; strValue.Format(_T("%.2f"), m_fDefaultBleed);
	RegSetValueEx(hKey, _T("DefaultBleed"), 0, REG_SZ, (LPBYTE)strValue.GetBuffer(), strValue.GetLength() * sizeof(TCHAR));

	ret = RegCloseKey(hKey);

	theApp.settings.OnLoadingSettings(); // reload new settings

	CPropertyPage::OnOK();
}

void CSettingsPage2::OnBrowse1() // Data-Directory
{
	UpdateData(TRUE);
	CString strDir = OnBrowseDirectories(m_strDataDir);
	if ( ! strDir.IsEmpty())
		m_strDataDir = strDir;
	UpdateData(FALSE);
}
void CSettingsPage2::OnBrowse2()  // Jobs-Directory
{
	UpdateData(TRUE);
	CString strDir = OnBrowseDirectories(m_strJobsDir);
	if ( ! strDir.IsEmpty())
		m_strJobsDir = strDir;
	UpdateData(FALSE);
}
void CSettingsPage2::OnBrowse3()  // Schemes-Directory
{
	UpdateData(TRUE);
	CString strDir = OnBrowseDirectories(m_strSchemeDir);
	if ( ! strDir.IsEmpty())
		m_strSchemeDir = strDir;
	UpdateData(FALSE);
}
void CSettingsPage2::OnBrowse4()  // PDF -> Input folder;	POSTSCRIPT -> PostScript-Directory
{
	UpdateData(TRUE);
#ifdef PDF
	CString strFolder = OnBrowseDirectories(m_strPDFInputFolder);
	if ( ! strFolder.IsEmpty())
		m_strPDFInputFolder = strFolder;
#else
	CString strDir = OnBrowseDirectories(m_strPSDir);
	if ( ! strDir.IsEmpty())
		m_strPSDir = strDir;
#endif
	UpdateData(FALSE);
}
void CSettingsPage2::OnBrowse5()  // Temp-Directory
{
	UpdateData(TRUE);
	CString strDir = OnBrowseDirectories(m_strTempDir);
	if ( ! strDir.IsEmpty())
		m_strTempDir = strDir;
	UpdateData(FALSE);
}
void CSettingsPage2::OnBrowse6()  // Standards-Directory
{
	UpdateData(TRUE);
	CString strDir = OnBrowseDirectories(m_strStandardsDir);
	if ( ! strDir.IsEmpty())
		m_strStandardsDir = strDir;
	UpdateData(FALSE);
}
void CSettingsPage2::OnBrowse7()  // Marks-Directory
{
	UpdateData(TRUE);
	CString strDir = OnBrowseDirectories(m_strMarksDir);
	if ( ! strDir.IsEmpty())
		m_strMarksDir = strDir;
	UpdateData(FALSE);
}
void CSettingsPage2::OnBrowse8()  // PDF -> Preview folder;	POSTSCRIPT -> EPS's-Directory
{
	UpdateData(TRUE);
#ifdef PDF
	CString strFolder = OnBrowseDirectories(m_strPDFPreviewFolder);
	if ( ! strFolder.IsEmpty())
		m_strPDFPreviewFolder = strFolder;
#else
	CString strEPSDir;
	if (m_strEPSDir.IsEmpty())
		strEPSDir = theApp.settings.m_szWorkDir;
	else
	{
		strEPSDir = m_strEPSDir;
		strEPSDir.GetBufferSetLength(m_strEPSDir.GetLength()-1);// cut the trailing backslash
	}
	CString strDir = OnBrowseDirectories(strEPSDir);
	if ( ! strDir.IsEmpty())
	{
		m_strEPSDir = strDir;
		m_strEPSDir += _T("\\");
	}
#endif
	UpdateData(FALSE);
}
CString CSettingsPage2::OnBrowseDirectories(LPCTSTR lpstrInitialDir) 
{
	CDlgDirPicker dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, _T(" |*.___||"), this);
	CString strTitle;
	strTitle.LoadString(IDS_DIRECTORY_STRING);
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = lpstrInitialDir;

	dlg.DoModal();
	return dlg.m_strPath;	// If the path is not valid or the dlg was canceled m_strPath.IsEmpty
}
void CSettingsPage2::OnEpsdirCheck() 
{
//	UpdateData(TRUE);
////	CEdit *pEdit = (CEdit*)GetDlgItem (IDC_EPSDIR);
//	pEdit->EnableWindow(m_bEPSDirCheck);
//	CButton *pButton = (CButton*)GetDlgItem (IDC_BROWSE8);
//	pButton->EnableWindow(m_bEPSDirCheck);
}

void CSettingsPage2::OnPreviewFolderCheck() 
{
	UpdateData(TRUE);
	CEdit *pEdit = (CEdit*)GetDlgItem (IDC_PREVIEW_FOLDER);
	pEdit->EnableWindow( ! m_bPDFPreviewsOrigPlace);
	CButton *pButton = (CButton*)GetDlgItem (IDC_BROWSE8);
	pButton->EnableWindow( ! m_bPDFPreviewsOrigPlace);
}

void CSettingsPage2::OnOK() 
{
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	long ret = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	// TODO: Verify existing pathes
	if (_tcscmp(m_strDataDir, theApp.settings.m_szDataDir) != 0) 
	{
		if (theApp.DirectoryExist(LPCTSTR(m_strDataDir)))
			RegSetValueEx(hKey, _T("DataFolder"), 0, REG_SZ, (LPBYTE)m_strDataDir.GetBuffer(_MAX_PATH), _MAX_PATH * sizeof(TCHAR));
	}
	if (_tcscmp(m_strJobsDir, theApp.settings.m_szJobsDir) != 0) 
	{
		if (theApp.DirectoryExist(LPCTSTR(m_strJobsDir)))
			RegSetValueEx(hKey, _T("JobFolder"), 0, REG_SZ, (LPBYTE)m_strJobsDir.GetBuffer(_MAX_PATH), _MAX_PATH * sizeof(TCHAR));
	}
	if (_tcscmp(m_strSchemeDir, theApp.settings.m_szSchemeDir) != 0) 
	{
		if (theApp.DirectoryExist(LPCTSTR(m_strSchemeDir)))
			RegSetValueEx(hKey, _T("SchemeFolder"), 0, REG_SZ, (LPBYTE)m_strSchemeDir.GetBuffer(_MAX_PATH), _MAX_PATH * sizeof(TCHAR));
	}

	if (_tcscmp(m_strPDFInputFolder, theApp.settings.m_szPDFInputFolder) != 0) 
	{
		if (theApp.DirectoryExist(LPCTSTR(m_strPDFInputFolder)))
			RegSetValueEx(hKey, _T("PDFInputFolder"), 0, REG_SZ, (LPBYTE)m_strPDFInputFolder.GetBuffer(_MAX_PATH), _MAX_PATH * sizeof(TCHAR));
	}
	if (_tcscmp(m_strPDFPreviewFolder, theApp.settings.m_szPDFPreviewFolder) != 0) 
	{
		if (theApp.DirectoryExist(LPCTSTR(m_strPDFPreviewFolder)))
			RegSetValueEx(hKey, _T("PDFPreviewFolder"), 0, REG_SZ, (LPBYTE)m_strPDFPreviewFolder.GetBuffer(_MAX_PATH), _MAX_PATH * sizeof(TCHAR));
	}
	if (m_bPDFPreviewsOrigPlace)
		RegSetValueEx(hKey, _T("PDFPreviewsOrigPlace"), 0, REG_SZ, (LPBYTE)_T("TRUE"),  5 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("PDFPreviewsOrigPlace"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));

	if (_tcscmp(m_strMarksDir, theApp.settings.m_szMarksDir) != 0) 
	{
		if (theApp.DirectoryExist(LPCTSTR(m_strMarksDir)))
			RegSetValueEx(hKey, _T("MarkFolder"), 0, REG_SZ, (LPBYTE)m_strMarksDir.GetBuffer(_MAX_PATH), _MAX_PATH * sizeof(TCHAR));
	}
	if (_tcscmp(m_strStandardsDir, theApp.settings.m_szStandardsDir) != 0) 
	{
		if (theApp.DirectoryExist(LPCTSTR(m_strStandardsDir)))
			RegSetValueEx(hKey, _T("StandardLayoutFolder"), 0, REG_SZ, (LPBYTE)m_strStandardsDir.GetBuffer(_MAX_PATH), _MAX_PATH * sizeof(TCHAR));
	}
	
	if (_tcscmp(m_strTempDir, theApp.settings.m_szTempDir) != 0) 
	{
		if (theApp.DirectoryExist(LPCTSTR(m_strTempDir)))
			RegSetValueEx(hKey, _T("TempFolder"), 0, REG_SZ, (LPBYTE)m_strTempDir.GetBuffer(_MAX_PATH), _MAX_PATH * sizeof(TCHAR));
	}
	
	ret = RegCloseKey(hKey);

	theApp.settings.OnLoadingSettings(); // reload new settings
	CPropertyPage::OnOK();
}

void CSettingsPage3::OnOK() 
{
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	long ret = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	if (m_iLanguage != theApp.settings.m_iLanguage)
	{
		switch (m_iLanguage)
		{
			case 0 : RegSetValueEx(hKey, _T("Language"), 0, REG_SZ, (LPBYTE)_T("GER"), 4 * sizeof(TCHAR));
					 break;	
			case 1 : RegSetValueEx(hKey, _T("Language"), 0, REG_SZ, (LPBYTE)_T("ENG"), 4 * sizeof(TCHAR));
					 break;	
			case 2 : RegSetValueEx(hKey, _T("Language"), 0, REG_SZ, (LPBYTE)_T("FRA"), 4 * sizeof(TCHAR));
					 break;	
			case 3 : RegSetValueEx(hKey, _T("Language"), 0, REG_SZ, (LPBYTE)_T("ITA"), 4 * sizeof(TCHAR));
					 break;	
			case 4 : RegSetValueEx(hKey, _T("Language"), 0, REG_SZ, (LPBYTE)_T("SPA"), 4 * sizeof(TCHAR));
					 break;	
			case 5 : RegSetValueEx(hKey, _T("Language"), 0, REG_SZ, (LPBYTE)_T("HOL"), 4 * sizeof(TCHAR));
					 break;	
			default: RegSetValueEx(hKey, _T("Language"), 0, REG_SZ, (LPBYTE)_T("GER"), 4 * sizeof(TCHAR));
					 break;	
		}
	}
	if (m_iUnit != theApp.settings.m_iUnit)
	{
		switch (m_iUnit)
		{
			case 0 : RegSetValueEx(hKey, _T("Unit"), 0, REG_SZ, (LPBYTE)_T("MM"),   3 * sizeof(TCHAR));
					 break;	
			case 1 : RegSetValueEx(hKey, _T("Unit"), 0, REG_SZ, (LPBYTE)_T("CM"),   3 * sizeof(TCHAR));
					 break;	
			case 2 : RegSetValueEx(hKey, _T("Unit"), 0, REG_SZ, (LPBYTE)_T("INCH"), 5 * sizeof(TCHAR));
					 break;	
			default: RegSetValueEx(hKey, _T("Unit"), 0, REG_SZ, (LPBYTE)_T("MM"),   3 * sizeof(TCHAR));
					 break;	
		}
	}

	if (m_nSheetsPerPage != theApp.settings.m_nSheetsPerPage)
	{
		TCHAR szCount[5];
		_stprintf(szCount, _T("%d"), m_nSheetsPerPage);
		RegSetValueEx(hKey, _T("SheetsPerPage"), 0, REG_SZ, (LPBYTE)szCount, 5 * sizeof(TCHAR));
	}

	if (m_strAutoServerNameIP.CompareNoCase(theApp.settings.m_szAutoServerNameIP) != 0)
		RegSetValueEx(hKey, _T("AutoServerNameIP"), 0, REG_SZ, (LPBYTE)m_strAutoServerNameIP.GetBuffer(_MAX_PATH), MAX_PATH * sizeof(TCHAR));

	if (m_strSPrintOneTenantID.CompareNoCase(theApp.settings.m_szSPrintOneTenantID) != 0)
		RegSetValueEx(hKey, _T("SPrintOneTenantID"), 0, REG_SZ, (LPBYTE)m_strSPrintOneTenantID.GetBuffer(_MAX_PATH), MAX_PATH * sizeof(TCHAR));

	if (m_strSPrintOneUserID.CompareNoCase(theApp.settings.m_szSPrintOneUserID) != 0)
		RegSetValueEx(hKey, _T("SPrintOneUserID"), 0, REG_SZ, (LPBYTE)m_strSPrintOneUserID.GetBuffer(_MAX_PATH), MAX_PATH * sizeof(TCHAR));

	if (m_strSPrintOnePassword.CompareNoCase(theApp.settings.m_szSPrintOnePassword) != 0)
		RegSetValueEx(hKey, _T("SPrintOnePassword"), 0, REG_SZ, (LPBYTE)m_strSPrintOnePassword.GetBuffer(_MAX_PATH), MAX_PATH * sizeof(TCHAR));

	ret = RegCloseKey(hKey);

	theApp.settings.OnLoadingSettings(); // reload new settings
	CPropertyPage::OnOK();
}

void CSettingsPage4::OnOK() 
{
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	long ret = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	if (m_bFrontRearParallel)
		RegSetValueEx(hKey, _T("FrontRearParallel"), 0, REG_SZ, (LPBYTE)_T("TRUE"), 5 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("FrontRearParallel"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));
	if (m_bOnStartFileNew)
		RegSetValueEx(hKey, _T("OnStartFileNew"), 0, REG_SZ, (LPBYTE)_T("TRUE"), 5 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("OnStartFileNew"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));
	if (m_bOnStartDelTemp)
		RegSetValueEx(hKey, _T("OnStartDelTemp"), 0, REG_SZ, (LPBYTE)_T("TRUE"), 5 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("OnStartDelTemp"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));

	if (m_bAutoBackup)
		RegSetValueEx(hKey, _T("AutoBackup"), 0, REG_SZ, (LPBYTE)_T("TRUE"), 5 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("AutoBackup"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));

	TCHAR szTime[5];
	_stprintf(szTime, _T("%d"), m_nAutoBackupTime);
	RegSetValueEx(hKey, _T("AutoBackupTime"), 0, REG_SZ, (LPBYTE)szTime, 5 * sizeof(TCHAR));

	if (m_bAutoCheckDoublePages)
		RegSetValueEx(hKey, _T("AutoCheckDoublePages"), 0, REG_SZ, (LPBYTE)_T("TRUE"), 5 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("AutoCheckDoublePages"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));
	if (m_bTrimboxPagesizeMismatchWarning)
		RegSetValueEx(hKey, _T("TrimboxPagesizeMismatchWarning"), 0, REG_SZ, (LPBYTE)_T("TRUE"), 5 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("TrimboxPagesizeMismatchWarning"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));

	if (m_bAutoWatchPageSources)
		RegSetValueEx(hKey, _T("AutoWatchPageSources"), 0, REG_SZ, (LPBYTE)_T("TRUE"), 5 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("AutoWatchPageSources"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));

	ret = RegCloseKey(hKey);

	theApp.settings.OnLoadingSettings(); // reload new settings
	CPropertyPage::OnOK();
}

BOOL CSettingsPage1::OnWizardFinish() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CPropertyPage::OnWizardFinish();
}

BOOL CSettingsPage2::OnWizardFinish() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CPropertyPage::OnWizardFinish();
}

BOOL CSettingsPage3::OnWizardFinish() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CPropertyPage::OnWizardFinish();
}
