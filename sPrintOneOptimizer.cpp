#include "stdafx.h"
#include "Imposition Manager.h"
#include "sPrintOneOptimizer.h"
#include <WinInet.h>  
#include "afxinet.h"
#include "KimSocketsCommon.h"
#include "Base64Decoder.h"


XMLByte g_XMLInMemBuf[200000];
MemBufInputSource* g_src;

CsPrintOneOptimizer::CsPrintOneOptimizer(CDlgOptimizationMonitor* pMonitor)
{
	m_pMonitor = pMonitor;
	g_src = new MemBufInputSource((const XMLByte*)g_XMLInMemBuf, 200000, "dummy");
}

CsPrintOneOptimizer::~CsPrintOneOptimizer(void)
{
	//delete g_src;
}

void CsPrintOneOptimizer::NotifyStatus(CString strSubject, CString strMsg, BOOL bOverwriteMode)
{
	if ( ! m_pMonitor)
		return;

	m_pMonitor->ReportWindowOutput(strSubject, strMsg, bOverwriteMode);
}

BOOL CsPrintOneOptimizer::DoOptimizeAll()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if ( ! CreateLayoutTask())
		return FALSE;

	for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
	{
		CPrintGroup& rPrintGroup = pDoc->m_printGroupList[i];

		CPrintComponentsGroup printComponents;
		rPrintGroup.InitPrintComponentsGroup(printComponents);
		rPrintGroup.PreImposeBoundComponents(printComponents);

		//AutoImposePrintComponents(printComponents, rPrintGroup.GetPrintingProfile());
		
		CPrintingProfile& rPrintingProfile = rPrintGroup.GetPrintingProfile();
		rPrintingProfile.m_press.SetID(_T("0"));

		if ( ! rPrintingProfile.m_press.m_availableSheets.GetSize())
			continue;

		LayoutTask_AddPrintingDevice(rPrintingProfile.m_press);

		for (int i = 0; i < rPrintingProfile.m_press.m_availableSheets.GetSize(); i++)
		{
			CCuttingProcess cuttingProcess  = (rPrintingProfile.ProcessIsActive(CProductionProfile::Cutting)) ? rPrintingProfile.m_cutting : CCuttingProcess();
			float fGripper   = (rPrintingProfile.m_press.m_pressDevice.m_nPressType == CPressDevice::SheetPress) ? rPrintingProfile.m_press.m_pressDevice.m_fGripper : 0.0f;
			CSheet sheet	 = rPrintingProfile.m_press.m_availableSheets[i];
			sheet.m_fWidth  -= cuttingProcess.m_fLeftBorder  + cuttingProcess.m_fRightBorder;
			sheet.m_fHeight -= cuttingProcess.m_fLowerBorder + cuttingProcess.m_fUpperBorder + fGripper;

			CString strSheetID; strSheetID.Format(_T("%d"), i);
			LayoutTask_AddMedia(sheet, strSheetID);
			LayoutTask_AddMountedMedia(sheet, strSheetID, rPrintingProfile.m_press);
		}
	}

	CString strID;
	int nIndex = 0;
	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		strID.Format(_T("%d"), nIndex);
		rFoldSheet.SetID(strID);
		LayoutTask_AddBinderySignature(rFoldSheet);
		nIndex++;
	}

	pos = pDoc->m_flatProducts.GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);
		LayoutTask_AddBinderySignature(rFlatProduct);
	}

	if (MakeCloudServerLayoutTaskRequest())
		LayoutTask_ProcessGangJobEvents(CPrintGroup());

	ReleaseLayoutTask();

	return TRUE;
}

BOOL CsPrintOneOptimizer::DoOptimizePrintGroup(CPrintGroup& rPrintGroup)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CPrintingProfile& rPrintingProfile = rPrintGroup.GetPrintingProfile();
	rPrintingProfile.m_press.SetID(_T("0"));

	if ( ! rPrintingProfile.m_press.m_availableSheets.GetSize())
		return FALSE;

	if ( ! CreateLayoutTask())
		return FALSE;

	LayoutTask_AddPrintingDevice(rPrintingProfile.m_press);

	for (int i = 0; i < rPrintingProfile.m_press.m_availableSheets.GetSize(); i++)
	{
		CCuttingProcess cuttingProcess  = (rPrintingProfile.ProcessIsActive(CProductionProfile::Cutting)) ? rPrintingProfile.m_cutting : CCuttingProcess();
		float fGripper   = (rPrintingProfile.m_press.m_pressDevice.m_nPressType == CPressDevice::SheetPress) ? rPrintingProfile.m_press.m_pressDevice.m_fGripper : 0.0f;
		CSheet sheet	 = rPrintingProfile.m_press.m_availableSheets[i];
		sheet.m_fWidth  -= cuttingProcess.m_fLeftBorder  + cuttingProcess.m_fRightBorder;
		sheet.m_fHeight -= cuttingProcess.m_fLowerBorder + cuttingProcess.m_fUpperBorder + fGripper;

		CString strSheetID; strSheetID.Format(_T("%d"), i);
		LayoutTask_AddMedia(sheet, strSheetID);
		LayoutTask_AddMountedMedia(sheet, strSheetID, rPrintingProfile.m_press);
	}

	CString strID;
	for (int i = 0; i < rPrintGroup.m_componentList.GetSize(); i++)
	{
		if (rPrintGroup.m_componentList[i].m_nProductClass == CPrintGroupComponent::Bound)
		{
			int				   nProductPartIndex = rPrintGroup.m_componentList[i].m_nProductClassIndex;
			CBoundProductPart& rProductPart		 = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
			CString strID;
			int nIndex = 0;
			POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
			while (pos)
			{
				CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
				if (rFoldSheet.m_nProductPartIndex == nProductPartIndex)
				{
					strID.Format(_T("%d"), nIndex);
					rFoldSheet.SetID(strID);
					LayoutTask_AddBinderySignature(rFoldSheet);
				}
				nIndex++;
			}
		}
		else
		{
			CFlatProduct&  rFlatProduct  = pDoc->m_flatProducts.FindByIndex(rPrintGroup.m_componentList[i].m_nFlatProductIndex);
			LayoutTask_AddBinderySignature(rFlatProduct);
		}
	}

	if (MakeCloudServerLayoutTaskRequest())
		LayoutTask_ProcessGangJobEvents(rPrintGroup);

	ReleaseLayoutTask();

	return TRUE;
}

BOOL CsPrintOneOptimizer::MakeCloudServerLayoutTaskRequest()
{
	BOOL bFailure = TRUE;

	CInternetSession session(_T("sPrintOne Session"));
	CHttpConnection* pServer = NULL;
	CHttpFile*		 pFile	 = NULL;
	INTERNET_PORT	 nPort	 = 80;
	CString			 strContentTypeHeader	= _T("Content-Type: application/xml");
	CString			 strContentLengthHeader;
	CString			 strResponse, strContent, strState, strObject, strTaskID;
	CString			 strAutorizationBase64;
	CString			 strAuthorizationHeader;// = _T("Authorization: Basic VXNlckBEZW1vNDI6dGVnZWxlcg==");
	strAutorizationBase64.Format(_T("%s@%s:%s"), theApp.settings.m_szSPrintOneUserID, theApp.settings.m_szSPrintOneTenantID, theApp.settings.m_szSPrintOnePassword);
	strAuthorizationHeader.Format(_T("Authorization: Basic %s"), base64_encode((unsigned char*)strAutorizationBase64.GetBuffer(), strAutorizationBase64.GetLength()).c_str());

	try
	{
		CString strXMLLayoutTask;
		WriteDOMDocToXMLString(strXMLLayoutTask, m_pRequestDOMDoc);
		WriteDOMDocToFile(_T("C:\\sPrintOneIn.xml"), m_pRequestDOMDoc);
		strContentLengthHeader.Format(_T("Content-Length: %d"), strXMLLayoutTask.GetLength());

		NotifyStatus(_T("Connect to"), _T("cloud.perfectpattern.de"));
		pServer = session.GetHttpConnection(_T("cloud.perfectpattern.de"), nPort, theApp.settings.m_szSPrintOneUserID + CString(_T("@")) + theApp.settings.m_szSPrintOneTenantID, theApp.settings.m_szSPrintOnePassword);

		strObject = _T("/sPrint.one.v2/api/tasks");
		NotifyStatus(_T("Upload layout task"), strObject);
		pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_POST, strObject);
		pFile->AddRequestHeaders(strContentTypeHeader);
		pFile->AddRequestHeaders(strContentLengthHeader);
		pFile->AddRequestHeaders(strAuthorizationHeader);
		pFile->SendRequest(NULL, 0, (void*)strXMLLayoutTask.GetBuffer(), strXMLLayoutTask.GetLength());

		if (CloudServerQueryStatusCode(pFile) == HTTP_STATUS_CREATED)	// 201
		{
			strTaskID = CloudServerQueryTaskID(pFile);
			pFile->Close();
			if ( ! strTaskID.IsEmpty())
			{
				NotifyStatus(_T("Layout task created"), strTaskID);
				int nCount = 0;
				strObject.Format(_T("/sPrint.one.v2/api/taskInfos/%s"), strTaskID);
				COleDateTime timeStart(COleDateTime::GetCurrentTime());
				do
				{
					nCount++;
					Sleep(1000L);
					COleDateTime timeCurrent(COleDateTime::GetCurrentTime());
					COleDateTimeSpan duration(timeCurrent - timeStart);
					if (duration.GetMinutes() >= 5)	// wait max. 5 minutes 
					{
						NotifyStatus(_T("Timeout"), _T("maximum 5 minutes exceeded"));
						break;
					}
					if (CancelledByUser())
					{
						NotifyStatus(_T("Job cancelled"), _T(""));
						break;
					}
					NotifyStatus(_T("Waiting for result"), duration.Format(_T("%M:%S")), (nCount == 1) ? FALSE : TRUE);
					pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_GET, strObject, NULL, 1, NULL, NULL, INTERNET_FLAG_RELOAD);		// no cache
					pFile->AddRequestHeaders(strAuthorizationHeader);
					pFile->SendRequest();
					if (CloudServerQueryStatusCode(pFile) == HTTP_STATUS_OK)	// 200
					{
						strState = CloudServerGetTaskInfoState(pFile);
						pFile->Close();
						if (strState == _T("SUCCESS"))
						{
							strObject.Format(_T("/sPrint.one.v2/api/tasks/%s"), strTaskID);
							NotifyStatus(_T("Download layout task"), strObject);
							pFile = pServer->OpenRequest(CHttpConnection::HTTP_VERB_GET, strObject);
							pFile->AddRequestHeaders(strAuthorizationHeader);
							pFile->SendRequest();
							if (CloudServerQueryStatusCode(pFile) == HTTP_STATUS_OK)	// 200
							{
								ParseXMLStringToDOMDoc(CloudServerGetLayoutTask(pFile), &m_pResponseDOMDoc);
								WriteDOMDocToFile(_T("C:\\sPrintOneOut.xml"), m_pResponseDOMDoc);
								bFailure = FALSE;
								pFile->Close();
								NotifyStatus(_T("Task finished"), _T("Success"));
								COleDateTime timeEnd(COleDateTime::GetCurrentTime());
								COleDateTimeSpan duration(timeEnd - timeStart);
								CString strDuration = duration.Format(_T("%H:%M:%S"));
								NotifyStatus(_T("Elapsed time"), strDuration);
								break;
							}
						}
						else
						{
							if (strState.CompareNoCase(_T("FAILURE")) == 0)
							{
								NotifyStatus(_T("Task finished"), _T("Failure"));
								break;
							}
						}
					}
				}
				while (1);	
			}
		}

		pServer->Close();
		session.Close();

		delete pFile;
		delete pServer;
	}
	catch (CInternetException* pEx)
	{
		CString strError;
		pEx->GetErrorMessage(strError.GetBuffer(64), 64);
		KIMOpenLogMessage(strError, MB_ICONERROR);
	}
	session.Close();

	return (bFailure) ? FALSE : TRUE;
}

BOOL CsPrintOneOptimizer::CancelledByUser()
{
	MSG msg;
	if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		if (msg.message == WM_LBUTTONDOWN)
		{
			CRect rcCancel;
			m_pMonitor->GetDlgItem(IDCANCEL)->GetWindowRect(rcCancel);
			CPoint ptMousePos;
			GetCursorPos(&ptMousePos);
			if (rcCancel.PtInRect(ptMousePos))
				if (AfxMessageBox(IDS_PDFLIB_REALLY_CANCEL, MB_YESNO) == IDYES)
					return TRUE;
		}
	}
	return FALSE;
}

int CsPrintOneOptimizer::CloudServerQueryStatusCode(CHttpFile* pFile)
{
	CString strInfo;
	pFile->QueryInfo(HTTP_QUERY_STATUS_CODE, strInfo);
	int nStatusCode = (strInfo.IsEmpty()) ? 0 : atoi(strInfo);
	if (nStatusCode >= 300)
	{
		CString strTitle, strMessage;
		CloudServerParseResponse(pFile, strTitle, strMessage);
		NotifyStatus(strTitle, strMessage);
	}
	return nStatusCode;
}

CString CsPrintOneOptimizer::CloudServerQueryTaskID(CHttpFile* pFile)
{
	CString strInfo;
	pFile->QueryInfo(HTTP_QUERY_LOCATION, strInfo);
	int nPos = strInfo.ReverseFind('/');
	if (nPos >= 0)
		return strInfo.Right(strInfo.GetLength() - nPos - 1);
	else
		return _T("");
}

CString CsPrintOneOptimizer::CloudServerGetTaskInfoState(CHttpFile* pFile)
{
	CString strResponse, strLine;
	while (pFile->ReadString(strLine))
		strResponse += strLine;

	memset(g_XMLInMemBuf, 0, 200000);
	memcpy(g_XMLInMemBuf, strResponse.GetBuffer(), strResponse.GetLength());

	//MemBufInputSource src((const XMLByte*)g_XMLInMemBuf, 200000, "dummy");

	g_src->resetMemBufInputSource((const XMLByte*)g_XMLInMemBuf, 200000);

	XercesDOMParser parser;
	parser.parse(*g_src);
	xercesc::DOMDocument* pDomDoc = parser.getDocument(); 
	if ( ! pDomDoc)
	{
		KIMOpenLogMessage(_T("xml parsing error: no DOMDocument"), MB_ICONERROR);
		return _T("");
	}
 	DOMNode* taskInfo = pDomDoc->getFirstChild();
	if ( ! taskInfo)
		return _T("");
	if (CString(taskInfo->getNodeName()) != _T("spoV2:taskInfo-Root"))
		return _T("");

	CString strState = _T("");
	DOMNamedNodeMap* nodeMap = taskInfo->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"state"); 
		strState = (item) ? (CString)item->getNodeValue() : _T("");
	}

	return strState;
}

CString CsPrintOneOptimizer::CloudServerGetLayoutTask(CHttpFile* pFile)
{
	CString strResponse, strLine;
	while (pFile->ReadString(strLine))
		strResponse += strLine;
	return strResponse;
}

BOOL CsPrintOneOptimizer::CreateLayoutTask()
{
	m_pDOMImplementation = DOMImplementationRegistry::getDOMImplementation(XMLString::transcode("core"));
	m_pDOMParser		 = new XercesDOMParser();
	m_pRequestDOMDoc	 = m_pDOMImplementation->createDocument();
	m_pResponseDOMDoc	 = NULL;

	CsP1LayoutTask* pLayoutTaskNode = (CsP1LayoutTask*)m_pRequestDOMDoc->createElement(CA2W("layoutTask-Root"));
	((DOMElement*)pLayoutTaskNode)->setAttribute(CA2W("xmlns"),	CA2W("http://www.perfectpattern.de/sPrint.one.v2.api"));
	((DOMElement*)pLayoutTaskNode)->setAttribute(CA2W("label"),	CA2W("KIM 7.1"));
	pLayoutTaskNode->AddChildNodes();
	m_pRequestDOMDoc->appendChild(pLayoutTaskNode);

	return TRUE;
}

BOOL CsPrintOneOptimizer::ReleaseLayoutTask()
{
	if (m_pRequestDOMDoc)
	{
		m_pRequestDOMDoc->release();
		m_pRequestDOMDoc = NULL;
	}
	if (m_pResponseDOMDoc)
	{
		m_pResponseDOMDoc->release();
		m_pResponseDOMDoc = NULL;
	}
//	delete m_pDOMParser;	

	return TRUE;
}

BOOL CsPrintOneOptimizer::WriteDOMDocToXMLString(CString& rstrXML, xercesc::DOMDocument* domDoc)
{
	DOMWriter* pDomWriter = ((DOMImplementation*)m_pDOMImplementation)->createDOMWriter();
    pDomWriter->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true); 
    pDomWriter->setNewLine(XMLString::transcode("\r\n"));

	XMLFormatTarget *myFormatTarget = new MemBufFormatTarget();

	pDomWriter->writeNode(myFormatTarget, *domDoc);
	char* theXMLString_Encoded = (char*)((MemBufFormatTarget*)myFormatTarget)->getRawBuffer();	
	rstrXML = CString(theXMLString_Encoded);

	delete myFormatTarget;
	pDomWriter->release();

	return TRUE;
}

BOOL CsPrintOneOptimizer::WriteDOMDocToFile(CString strFilePath, xercesc::DOMDocument* domDoc)
{
	DOMWriter* pDomWriter = ((DOMImplementation*)m_pDOMImplementation)->createDOMWriter();
    pDomWriter->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true); 
    pDomWriter->setNewLine(XMLString::transcode("\r\n"));

	XMLFormatTarget *pWriteTarget = new LocalFileFormatTarget(strFilePath.GetBuffer());
	pDomWriter->writeNode(pWriteTarget, *domDoc);

	delete pWriteTarget;
	pDomWriter->release();

	return TRUE;
}

BOOL CsPrintOneOptimizer::ParseXMLStringToDOMDoc(CString& strXML, xercesc::DOMDocument** domDoc)
{
	MemBufInputSource src((const XMLByte*)strXML.GetBuffer(), strXML.GetLength(), "dummy", false);

	m_pDOMParser->parse(src);
	*domDoc = m_pDOMParser->adoptDocument(); 

	DOMNode* taskInfo = (*domDoc)->getFirstChild();
	if (taskInfo)
	{
		CString strNodeName = taskInfo->getNodeName();
	}

	return TRUE;
}

BOOL CsPrintOneOptimizer::CloudServerParseResponse(CHttpFile* pFile, CString& rstrTitle, CString& rstrMessage)
{
	CString strResponse, strLine;
	while (pFile->ReadString(strLine))
		strResponse += strLine;

	m_XMLInMemBuf = strResponse;

	MemBufInputSource src((const XMLByte*)m_XMLInMemBuf.c_str(), m_XMLInMemBuf.size(), "dummy", false);
	XercesDOMParser   parser;
	parser.parse(src);
	xercesc::DOMDocument* pDomDoc = parser.getDocument(); 

	if ( ! pDomDoc)
	{
		KIMOpenLogMessage(_T("xml parsing error: no DOMDocument"), MB_ICONERROR);
		return FALSE;
	}
 	DOMNode* taskInfo = pDomDoc->getFirstChild();
	if ( ! taskInfo)
		return FALSE;
	if (CString(taskInfo->getNodeName()) != _T("error-Root"))
		return FALSE;

	CString strValue;
	DOMNamedNodeMap* nodeMap = taskInfo->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"title"); 
		rstrTitle = (item) ? (CString)item->getNodeValue() : _T("");
	}

	CString  strNodeName;
	DOMNode* pNode = taskInfo->getFirstChild();
	while (pNode)
	{
		strNodeName = pNode->getNodeName();
		if (strNodeName == _T("message"))
		{
			rstrMessage = pNode->getTextContent();
			break;
		}
		pNode = pNode->getNextSibling();
	}

	return TRUE;
}

BOOL CsPrintOneOptimizer::LayoutTask_AddPrintingDevice(CPressParams& rPressParams)
{
	DOMNodeList* pList = m_pRequestDOMDoc->getElementsByTagName(CA2W("devices"));
	if (pList->getLength() > 0)
		return ((CsP1Devices*)pList->item(0))->AddPrintingDevice(rPressParams);
	else
		return FALSE;
}

BOOL CsPrintOneOptimizer::LayoutTask_AddMedia(CSheet& rSheet, CString& strSheetID)
{
	DOMNodeList* pList = m_pRequestDOMDoc->getElementsByTagName(CA2W("medias"));
	if (pList->getLength() > 0)
		return ((CsP1Medias*)pList->item(0))->AddMedia(rSheet, strSheetID);
	else
		return FALSE;
}

BOOL CsPrintOneOptimizer::LayoutTask_AddMountedMedia(CSheet& rSheet, CString& strSheetID, CPressParams& rPressParams)
{
	DOMNodeList* pList = m_pRequestDOMDoc->getElementsByTagName(CA2W("mountedMedias"));
	if (pList->getLength() > 0)
		return ((CsP1MountedMedias*)pList->item(0))->AddMountedMedia(rSheet, strSheetID, rPressParams);
	else
		return FALSE;
}

BOOL CsPrintOneOptimizer::LayoutTask_AddBinderySignature(CFoldSheet& rFoldSheet)
{
	DOMNodeList* pList = m_pRequestDOMDoc->getElementsByTagName(CA2W("binderySignatures"));
	if (pList->getLength() > 0)
		return ((CsP1BinderySignatures*)pList->item(0))->AddBinderySignature(rFoldSheet);
	else
		return FALSE;
}

BOOL CsPrintOneOptimizer::LayoutTask_AddBinderySignature(CFlatProduct& rFlatProduct)
{
	DOMNodeList* pList = m_pRequestDOMDoc->getElementsByTagName(CA2W("binderySignatures"));
	if (pList->getLength() > 0)
		return ((CsP1BinderySignatures*)pList->item(0))->AddBinderySignature(rFlatProduct);
	else
		return FALSE;
}

BOOL CsPrintOneOptimizer::LayoutTask_ProcessGangJobEvents(CPrintGroup& rPrintGroup)
{
	if ( ! m_pResponseDOMDoc)
		return FALSE;
	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return FALSE;

	//pDoc->m_PrintSheetList.RemoveAll();
	DOMNodeList* pList = m_pResponseDOMDoc->getElementsByTagName(CA2W("spoV2:gangJobEvent"));
	for (unsigned i = 0; i < pList->getLength(); i++)
	{
		((CsP1GangJobEvent*)pList->item(i))->ProcessGangJob();
	}
	return TRUE;
}

CString CsPrintOneOptimizer::DateTimeToUnixTimestamp(CTime time)
{
	CString strTime; strTime.Format(_T("%d"), (long)time.GetTime());
	return strTime + _T("000");
    //return (dateTime - COleDateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
}


////////////////////////// LayoutTask //////////////////////////
BOOL CsP1LayoutTask::AddChildNodes()
{
	CsP1Parameters* pParameters = (CsP1Parameters*)getOwnerDocument()->createElement(CA2W("parameters"));
	pParameters->AddChildNodes();
	appendChild(pParameters);
	return TRUE;
}

////////////////////////// Parameters //////////////////////////
BOOL CsP1Parameters::AddChildNodes()
{
	CsP1Setup* pSetup = (CsP1Setup*)getOwnerDocument()->createElement(CA2W("setup"));
	pSetup->AddChildNodes();
	appendChild(pSetup);

	CsP1BinderySignaturePool* pBinderySignaturePool = (CsP1BinderySignaturePool*)getOwnerDocument()->createElement(CA2W("binderySignaturePool"));
	pBinderySignaturePool->AddChildNodes();
	appendChild(pBinderySignaturePool);

	CsP1StartSchedule* pStartSchedule = (CsP1StartSchedule*)getOwnerDocument()->createElement(CA2W("startSchedule"));
	pStartSchedule->AddChildNodes();
	appendChild(pStartSchedule);

	CsP1EngineConfiguration* pEngineConfiguration = (CsP1EngineConfiguration*)getOwnerDocument()->createElement(CA2W("engineConfiguration"));
	pEngineConfiguration->AddChildNodes();
	appendChild(pEngineConfiguration);

	return TRUE;
}

////////////////////////// Setup //////////////////////////
BOOL CsP1Setup::AddChildNodes()
{
	((DOMElement*)this)->setAttribute(CA2W("id"),			CA2W("string"));
	((DOMElement*)this)->setAttribute(CA2W("label"),		CA2W("string"));
	((DOMElement*)this)->setAttribute(CA2W("description"),	CA2W("setupData"));

	DOMNode* pNewNode = getOwnerDocument()->createElement(CA2W("medias"));
	appendChild(pNewNode);

	pNewNode = getOwnerDocument()->createElement(CA2W("devices"));
	appendChild(pNewNode);

	pNewNode = getOwnerDocument()->createElement(CA2W("mountedMedias"));
	appendChild(pNewNode);

	return TRUE;
}

////////////////////////// Medias //////////////////////////
BOOL CsP1Medias::AddMedia(CSheet& rSheet, CString& strSheetID)
{
	CString  strNodeName, strCurrentID;
	DOMNode* node = (DOMElement*)getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("media"))
		{
			DOMNamedNodeMap* nodeMap = node->getAttributes();
			if (nodeMap)
			{
				DOMNode* item = nodeMap->getNamedItem(L"id");
				strCurrentID  = (item) ? (CString)item->getNodeValue() : _T("");
				if (strCurrentID == strSheetID)
					return FALSE;
			}
		}
		node = node->getNextSibling();
	}

	DOMElement* pMedia = (DOMElement*)getOwnerDocument()->createElement(CA2W("media"));

	pMedia->setAttribute(CA2W("id"),				CA2W(strSheetID));
	pMedia->setAttribute(CA2W("grainDirection"),	CA2W("X"));
	pMedia->setAttribute(CA2W("description"),		CA2W("80 g/qm Preprint Standard"));
	pMedia->setAttribute(CA2W("label"),				CA2W("80 g/qm Preprint Standard"));
	pMedia->setAttribute(CA2W("categories"),		CA2W("80g/qm_PreprintStandard"));

	//// subnode format
	{
		CString strWidth;  strWidth.Format (_T("%d"), (long)(rSheet.m_fWidth  * 1000.0f + 0.5f));
		CString strHeight; strHeight.Format(_T("%d"), (long)(rSheet.m_fHeight * 1000.0f + 0.5f));
		DOMElement* pFormat = (DOMElement*)getOwnerDocument()->createElement(CA2W("format"));
		pFormat->setAttribute(CA2W("width"),  CA2W(strWidth));
		pFormat->setAttribute(CA2W("height"), CA2W(strHeight));
		pMedia->appendChild(pFormat);
	}

	appendChild(pMedia);
	return TRUE;
}

////////////////////////// MountedMedias //////////////////////////
BOOL CsP1MountedMedias::AddMountedMedia(CSheet& rSheet, CString& strSheetID, CPressParams& rPressParams)
{
	CString  strNodeName, strCurrentID;
	DOMNode* node = (DOMElement*)getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("mountedMedia"))
		{
			DOMNamedNodeMap* nodeMap = node->getAttributes();
			if (nodeMap)
			{
				DOMNode* item = nodeMap->getNamedItem(L"id");
				strCurrentID  = (item) ? (CString)item->getNodeValue() : _T("");
				if (strCurrentID == strSheetID)
					return FALSE;
			}
		}
		node = node->getNextSibling();
	}

	CsP1MountedMedia* pMountedMedia = (CsP1MountedMedia*)getOwnerDocument()->createElement(CA2W("mountedMedia"));

	((DOMElement*)pMountedMedia)->setAttribute(CA2W("id"),						CA2W(strSheetID));
	((DOMElement*)pMountedMedia)->setAttribute(CA2W("description"),				CA2W(rSheet.m_strSheetName));
	((DOMElement*)pMountedMedia)->setAttribute(CA2W("label"),					CA2W(rSheet.m_strSheetName));
	((DOMElement*)pMountedMedia)->setAttribute(CA2W("mediaId"),					CA2W(strSheetID));
	((DOMElement*)pMountedMedia)->setAttribute(CA2W("primaryGripperEdgeSide"),	CA2W("TOP"));
	((DOMElement*)pMountedMedia)->setAttribute(CA2W("printingDeviceId"),		CA2W(rPressParams.GetID()));

	pMountedMedia->AddPerformance();

	appendChild(pMountedMedia);
	return TRUE;
}

////////////////////////// MountedMedia //////////////////////////
BOOL CsP1MountedMedia::AddPerformance()
{
	DOMElement* pPerformance = (DOMElement*)getOwnerDocument()->createElement(CA2W("performance"));

	DOMElement* pNode;
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("preGangJob"));
	pNode->setAttribute(CA2W("cost"), CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("preGangJob_exposePrintingPlate"));
	pNode->setAttribute(CA2W("cost"), CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("gangJob_perSheet"));
	pNode->setAttribute(CA2W("cost"),		CA2W("100"));
	pNode->setAttribute(CA2W("duration"),	CA2W("1000000"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("prePrintRun"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("prePrintRun_changePrintingUnitState"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("prePrintRun_changeManualFlippingUnitState"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("prePrintRun_changeAutomaticFlippingUnitState"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("printRun_perSheet"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("printRun_manualTurn"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("printRun_manualTumble"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("printRun_automaticTurnPerSheet"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("printRun_automaticTumblePerSheet"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("postPrintRun"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("interPrintRun"));
	pNode->setAttribute(CA2W("cost"),		CA2W("0"));
	pNode->setAttribute(CA2W("duration"),	CA2W("0"));
	pPerformance->appendChild(pNode);
	pNode = (DOMElement*)getOwnerDocument()->createElement(CA2W("postGangJob"));
	pNode->setAttribute(CA2W("cost"),		CA2W("100"));
	pNode->setAttribute(CA2W("duration"),	CA2W("1000000"));
	pPerformance->appendChild(pNode);

	appendChild(pPerformance);
	return TRUE;
}

////////////////////////// Devices //////////////////////////
BOOL CsP1Devices::AddPrintingDevice(CPressParams& rPressParams)
{
	CString  strID = rPressParams.GetID();
	CString  strNodeName, strCurrentID;
	DOMNode* node = (DOMElement*)getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("printingDevice"))
		{
			DOMNamedNodeMap* nodeMap = node->getAttributes();
			if (nodeMap)
			{
				DOMNode* item = nodeMap->getNamedItem(L"id");
				strCurrentID  = (item) ? (CString)item->getNodeValue() : _T("");
				if (strCurrentID == strID)
					return FALSE;
			}
		}
		node = node->getNextSibling();
	}

	DOMElement* pPrintingDevice = (DOMElement*)getOwnerDocument()->createElement(CA2W("printingDevice"));
	pPrintingDevice->setAttribute(CA2W("id"),			CA2W(strID));
	pPrintingDevice->setAttribute(CA2W("description"),	CA2W(rPressParams.m_pressDevice.m_strName));
	pPrintingDevice->setAttribute(CA2W("label"),		CA2W(rPressParams.m_pressDevice.m_strName));
	pPrintingDevice->setAttribute(CA2W("categories"),	CA2W("all"));
	pPrintingDevice->setAttribute(CA2W("typeId"),		CA2W("offset_4-4"));
	appendChild(pPrintingDevice);

	return TRUE;
}

////////////////////////// StartSchedule //////////////////////////
BOOL CsP1StartSchedule::AddChildNodes()
{
	((DOMElement*)this)->setAttribute(CA2W("id"),		CA2W("emptySchedule"));
	((DOMElement*)this)->setAttribute(CA2W("label"),	CA2W("string"));
	return TRUE;
}

////////////////////////// BinderySignaturePool //////////////////////////
BOOL CsP1BinderySignaturePool::AddChildNodes()
{
	((DOMElement*)this)->setAttribute(CA2W("id"),			CA2W("string"));
	((DOMElement*)this)->setAttribute(CA2W("description"),	CA2W("string"));
	((DOMElement*)this)->setAttribute(CA2W("label"),		CA2W("string"));

	DOMNode* pNewNode = getOwnerDocument()->createElement(CA2W("binderySignatures"));
	appendChild(pNewNode);

	return TRUE;
}

////////////////////////// BinderySignatures //////////////////////////
BOOL CsP1BinderySignatures::AddBinderySignature(CFoldSheet& rFoldSheet)
{
	int nLayerIndex = 0;
	float fFoldSheetCutWidth, fFoldSheetCutHeight; 
	rFoldSheet.GetCutSize(nLayerIndex, fFoldSheetCutWidth, fFoldSheetCutHeight);

	//int nFoldSheetGrain = GRAIN_EITHER;
	//CFoldSheetLayer* pLayer = rFoldSheet.GetLayer(0);
	//if (pLayer)
	//{
	//	if (pLayer->m_HeadPositionArray.GetSize())
	//	{
	//		int nHeadPosition = pLayer->m_HeadPositionArray[0];
	//		switch (nHeadPosition)
	//		{
	//		case TOP:
	//		case BOTTOM:	nFoldSheetGrain = GRAIN_VERTICAL;	break;
	//		case LEFT:
	//		case RIGHT:		nFoldSheetGrain = GRAIN_HORIZONTAL; break;
	//		}
	//	}
	//}

	//if ( (nFoldSheetGrain != GRAIN_EITHER) && (nFoldSheetGrain != nPaperGrain) )
	//{
	//	float fTemp = sort.fFormatX; sort.fFormatX = sort.fFormatY; sort.fFormatY = fTemp;
	//	int	  nTemp = sort.nPunchX;	 sort.nPunchX  = sort.nPunchY;	sort.nPunchY  = nTemp;
	//		  fTemp = sort.fOffsetX; sort.fOffsetX = sort.fOffsetY; sort.fOffsetY = fTemp;
	//}

	DOMElement* pBinderySignature = (DOMElement*)getOwnerDocument()->createElement(CA2W("binderySignature"));

	CBoundProduct&		rProduct	 = rFoldSheet.GetBoundProduct();
	CBoundProductPart&  rProductPart = rFoldSheet.GetProductPart();

	CString strID; strID.Format(_T("%d@FoldSheets"), rFoldSheet.GetIndex());
	CString strMustDemand; strMustDemand.Format(_T("%d"), (rFoldSheet.GetQuantityPlanned() <= 0) ? 1 : rFoldSheet.GetQuantityPlanned());  
	CString strCanDemand;  strCanDemand.Format (_T("%d"), (rFoldSheet.GetQuantityPlanned() <= 0) ? 1 : rFoldSheet.GetQuantityPlanned());  
	pBinderySignature->setAttribute(CA2W("id"),							CA2W(strID));
	pBinderySignature->setAttribute(CA2W("mustDemand"),					CA2W(strMustDemand));
	pBinderySignature->setAttribute(CA2W("canDemand"),					CA2W(strCanDemand));
	pBinderySignature->setAttribute(CA2W("mediaCategories"),			CA2W("80g/qm_PreprintStandard"));
	pBinderySignature->setAttribute(CA2W("printingDeviceCategories"),	CA2W("all"));
	pBinderySignature->setAttribute(CA2W("sheetQuantity"),				CA2W("1"));

	/// format subnode
	{
		CString strWidth;  strWidth.Format (_T("%d"), (long)(fFoldSheetCutWidth  * 1000.0f + 0.5f));
		CString strHeight; strHeight.Format(_T("%d"), (long)(fFoldSheetCutHeight * 1000.0f + 0.5f));
		DOMElement* pFormat = (DOMElement*)getOwnerDocument()->createElement(CA2W("trimFormat"));
		pFormat->setAttribute(CA2W("width"),  CA2W(strWidth));
		pFormat->setAttribute(CA2W("height"), CA2W(strHeight));
	
		pBinderySignature->appendChild(pFormat);
	}

	CsP1ConstraintConfiguration* pConstraintConfiguration = (CsP1ConstraintConfiguration*)getOwnerDocument()->createElement(CA2W("constraintConfiguration"));
	pConstraintConfiguration->AddChildNodes();
	pBinderySignature->appendChild(pConstraintConfiguration);

	CsP1PrintData* pPrintData = (CsP1PrintData*)getOwnerDocument()->createElement(CA2W("printData"));
	pPrintData->AddChildNodes();
	pBinderySignature->appendChild(pPrintData);

	appendChild(pBinderySignature);

	return TRUE;
} 

BOOL CsP1BinderySignatures::AddBinderySignature(CFlatProduct& rFlatProduct)
{
	float fCutWidth  = rFlatProduct.m_fTrimSizeWidth  + rFlatProduct.m_fLeftTrim   + rFlatProduct.m_fRightTrim;
	float fCutHeight = rFlatProduct.m_fTrimSizeHeight + rFlatProduct.m_fBottomTrim + rFlatProduct.m_fTopTrim;

	//if ( (rFlatProduct.m_nPaperGrain != GRAIN_EITHER) && (rFlatProduct.m_nPaperGrain != nPaperGrain) )
	//{
	//	float fTemp = sort.fFormatX; sort.fFormatX = sort.fFormatY; sort.fFormatY = fTemp;
	//	int	  nTemp = sort.nPunchX;	 sort.nPunchX  = sort.nPunchY;	sort.nPunchY  = nTemp;
	//		  fTemp = sort.fOffsetX; sort.fOffsetX = sort.fOffsetY; sort.fOffsetY = fTemp;
	//}
	//sort.pivot	   = (rFlatProduct.m_nPaperGrain == GRAIN_EITHER) ? 1 : 0;

	DOMElement* pBinderySignature = (DOMElement*)getOwnerDocument()->createElement(CA2W("binderySignature"));

	int nMustDemand = (rFlatProduct.m_nDesiredQuantity <= 0) ? 1 : rFlatProduct.m_nDesiredQuantity;
	int nCanDemand  = (int)((float)nMustDemand * 2.0f + 0.5f);
	CString strId; strId.Format(_T("%d@FlatProducts"), rFlatProduct.GetIndex());
	CString strMustDemand; strMustDemand.Format(_T("%d"), nMustDemand);  
	CString strCanDemand;  strCanDemand.Format (_T("%d"), nCanDemand);  
	pBinderySignature->setAttribute(CA2W("id"),							CA2W(strId));
	pBinderySignature->setAttribute(CA2W("mustDemand"),					CA2W(strMustDemand));
	pBinderySignature->setAttribute(CA2W("canDemand"),					CA2W(strCanDemand));
	pBinderySignature->setAttribute(CA2W("mediaCategories"),			CA2W("80g/qm_PreprintStandard"));
	pBinderySignature->setAttribute(CA2W("printingDeviceCategories"),	CA2W("all"));
	pBinderySignature->setAttribute(CA2W("sheetQuantity"),				CA2W("1"));

	//// format subnode
	{
		CString strWidth;  strWidth.Format (_T("%d"), (long)(fCutWidth  * 1000.0f + 0.5f));
		CString strHeight; strHeight.Format(_T("%d"), (long)(fCutHeight * 1000.0f + 0.5f));
		DOMElement* pFormat = (DOMElement*)getOwnerDocument()->createElement(CA2W("trimFormat"));
		pFormat->setAttribute(CA2W("width"),  CA2W(strWidth));
		pFormat->setAttribute(CA2W("height"), CA2W(strHeight));

		pBinderySignature->appendChild(pFormat);
	}

	CsP1ConstraintConfiguration* pConstraintConfiguration = (CsP1ConstraintConfiguration*)getOwnerDocument()->createElement(CA2W("constraintConfiguration"));
	pConstraintConfiguration->AddChildNodes();
	pBinderySignature->appendChild(pConstraintConfiguration);

	CsP1PrintData* pPrintData = (CsP1PrintData*)getOwnerDocument()->createElement(CA2W("printData"));
	pPrintData->AddChildNodes();
	pBinderySignature->appendChild(pPrintData);

	appendChild(pBinderySignature);

	return TRUE;
}

////////////////////////// ConstraintConfiguration //////////////////////////
BOOL CsP1ConstraintConfiguration::AddChildNodes()
{
	CTime latestEndTime = CTime::GetCurrentTime() + CTimeSpan(7,0,0,0);
	CString strEndTime = CsPrintOneOptimizer::DateTimeToUnixTimestamp(latestEndTime);
	((DOMElement*)this)->setAttribute(CA2W("latestEndTime"),		CA2W(strEndTime));
	((DOMElement*)this)->setAttribute(CA2W("rotationConstraint"),	CA2W("FREE"));
	((DOMElement*)this)->setAttribute(CA2W("stamped"),				CA2W("false"));

	DOMElement* pBleed = (DOMElement*)getOwnerDocument()->createElement(CA2W("bleed"));
	pBleed->setAttribute(CA2W("top"),		CA2W("0"));
	pBleed->setAttribute(CA2W("right"),		CA2W("0"));
	pBleed->setAttribute(CA2W("bottom"),	CA2W("0"));
	pBleed->setAttribute(CA2W("left"),		CA2W("0"));
	appendChild(pBleed);

	DOMElement* pMargin = (DOMElement*)getOwnerDocument()->createElement(CA2W("margin"));
	pMargin->setAttribute(CA2W("top"),		CA2W("0"));
	pMargin->setAttribute(CA2W("right"),	CA2W("0"));
	pMargin->setAttribute(CA2W("bottom"),	CA2W("0"));
	pMargin->setAttribute(CA2W("left"),		CA2W("0"));
	appendChild(pMargin);

	DOMElement* pFrontPage = (DOMElement*)getOwnerDocument()->createElement(CA2W("frontPage"));
	pFrontPage->setAttribute(CA2W("varnished"),	CA2W("false"));
	{
		DOMElement* pColorUses = (DOMElement*)getOwnerDocument()->createElement(CA2W("colorUses"));
		{
			DOMElement* pColorUse = (DOMElement*)getOwnerDocument()->createElement(CA2W("colorUse"));
			pColorUse->setAttribute(CA2W("color"),		CA2W(""));
			pColorUse->setAttribute(CA2W("intensity"),	CA2W(""));
			pColorUses->appendChild(pColorUse);
		}
		pFrontPage->appendChild(pColorUses);
	}
	appendChild(pFrontPage);

	DOMElement* pBackPage = (DOMElement*)getOwnerDocument()->createElement(CA2W("backPage"));
	pBackPage->setAttribute(CA2W("varnished"),	CA2W("false"));
	{
		DOMElement* pColorUses = (DOMElement*)getOwnerDocument()->createElement(CA2W("colorUses"));
		{
			DOMElement* pColorUse = (DOMElement*)getOwnerDocument()->createElement(CA2W("colorUse"));
			pColorUse->setAttribute(CA2W("color"),		CA2W(""));
			pColorUse->setAttribute(CA2W("intensity"),	CA2W(""));
			pColorUses->appendChild(pColorUse);
		}
		pBackPage->appendChild(pColorUses);
	}
	appendChild(pBackPage);

	return TRUE;
} 

////////////////////////// PrintData //////////////////////////
BOOL CsP1PrintData::AddChildNodes()
{
	DOMElement* pFrontPage = (DOMElement*)getOwnerDocument()->createElement(CA2W("frontPage"));
	pFrontPage->setAttribute(CA2W("pdfUrl"),		CA2W(""));
	pFrontPage->setAttribute(CA2W("pdfPageNumber"),	CA2W(""));

	DOMElement* pBackPage = (DOMElement*)getOwnerDocument()->createElement(CA2W("backPage"));
	pBackPage->setAttribute(CA2W("pdfUrl"),			CA2W(""));
	pBackPage->setAttribute(CA2W("pdfPageNumber"),	CA2W(""));

	return TRUE;
}

////////////////////////// PrintData //////////////////////////
BOOL CsP1EngineConfiguration::AddChildNodes()
{
	((DOMElement*)this)->setAttribute(CA2W("id"),						CA2W("testConf"));
	((DOMElement*)this)->setAttribute(CA2W("label"),					CA2W("test configuration for demo pool"));
	((DOMElement*)this)->setAttribute(CA2W("cuttingStyle"),				CA2W("GUILLOTINE_PREFERENCE_IN_PRODUCTION_DIRECTION"));
	((DOMElement*)this)->setAttribute(CA2W("deviceChoiceStrategy"),		CA2W("HEURISTIC"));
	((DOMElement*)this)->setAttribute(CA2W("rounds"),					CA2W("10"));
	((DOMElement*)this)->setAttribute(CA2W("ignoreColorGradient"),		CA2W("true"));
	((DOMElement*)this)->setAttribute(CA2W("colorGradientThreshold"),	CA2W("25"));
	((DOMElement*)this)->setAttribute(CA2W("smartcutHealingDistance"),	CA2W("2000"));
	((DOMElement*)this)->setAttribute(CA2W("planningStartTimeOffset"),	CA2W("1800000"));

	return TRUE;
}

////////////////////////// GangJobEvent //////////////////////////
BOOL CsP1GangJobEvent::ProcessGangJob()
{
	CString  strNodeName;
	DOMNode* pNode = getFirstChild();
	while (pNode)
	{
		strNodeName = pNode->getNodeName();
		if (strNodeName == _T("spoV2:gangJob"))
		{
			((CsP1GangJob*)pNode)->ProcessForm();
		}
		pNode = pNode->getNextSibling();
	}
	return TRUE;
}

////////////////////////// GangJob //////////////////////////
BOOL CsP1GangJob::ProcessForm()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CString strPrintingDeviceID, strQuantity;
	DOMNamedNodeMap* nodeMap = getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"printingDeviceId");
		strPrintingDeviceID = (item) ? (CString)item->getNodeValue() : _T("");
		item = nodeMap->getNamedItem(L"quantity");
		strQuantity	  = (item) ? (CString)item->getNodeValue() : _T("");
	}

	CPrintingProfile printingProfile;
	POSITION pos = pDoc->m_printingProfiles.GetHeadPosition();
	while (pos)
	{
		CPrintingProfile& rPrintingProfile = pDoc->m_printingProfiles.GetNext(pos);
		if (rPrintingProfile.m_press.GetID() == strPrintingDeviceID)
		{
			printingProfile = rPrintingProfile;
			break;
		}
	}

	CPrintSheet* pPrintSheet = NULL;
	pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.GetPrintingProfile() == printingProfile)
			if (rPrintSheet.IsEmpty())
			{
				pPrintSheet = &rPrintSheet;
				break;
			}
	}
	if ( ! pPrintSheet)
		pPrintSheet = pDoc->m_PrintSheetList.AddBlankSheet(printingProfile);
	if ( ! pPrintSheet)
		return FALSE;

	pPrintSheet->m_nQuantityTotal = atoi(strQuantity);

	CString  strNodeName;
	DOMNode* pNode = getFirstChild();
	while (pNode)
	{
		strNodeName = pNode->getNodeName();
		if (strNodeName == _T("spoV2:form"))
			((CsP1Form*)pNode)->ProcessPlacementZone(pPrintSheet);
		pNode = pNode->getNextSibling();
	}
	return TRUE;
}

////////////////////////// Form //////////////////////////
BOOL CsP1Form::ProcessPlacementZone(CPrintSheet* pPrintSheet)
{
	CString  strNodeName;
	DOMNode* pNode = getFirstChild();
	while (pNode)
	{
		strNodeName = pNode->getNodeName();
		if (strNodeName == _T("spoV2:placementZone"))
			((CsP1PlacementZone*)pNode)->ProcessBinderySignaturePlacements(pPrintSheet);
		pNode = pNode->getNextSibling();
	}
	return TRUE;
}

////////////////////////// Form //////////////////////////
BOOL CsP1PlacementZone::ProcessBinderySignaturePlacements(CPrintSheet* pPrintSheet)
{
	CArray <int, int> foldSheetIDList;
	CString  strNodeName;
	DOMNode* pNode = getFirstChild();
	while (pNode)
	{
		strNodeName = pNode->getNodeName();
		if (strNodeName == _T("spoV2:binderySignaturePlacements"))
		{
			DOMNode* pChildNode = pNode->getFirstChild();
			while (pChildNode)
			{
				strNodeName = pChildNode->getNodeName();
				if (strNodeName == _T("spoV2:binderySignaturePlacement"))
					((CsP1BinderySignaturePlacement*)pChildNode)->Process(pPrintSheet);
				pChildNode = pChildNode->getNextSibling();
			}
		}
		pNode = pNode->getNextSibling();
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	if ( ! pPrintSheet)
		return FALSE;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return FALSE;

	pLayout->m_FrontSide.PositionLayoutSide();
	pLayout->m_BackSide.PositionLayoutSide();

	pLayout->m_FrontSide.Invalidate();
	pLayout->m_BackSide.Invalidate();

	pLayout->AnalyzeMarks(FALSE);

	return TRUE;
}

////////////////////////// BinderySignaturePlacement //////////////////////////
BOOL CsP1BinderySignaturePlacement::Process(CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	if ( ! pPrintSheet)
		return FALSE;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return FALSE;

	CString strLongID, strID, strType, strRotation, strXOffset, strYOffset;
	DOMNamedNodeMap* nodeMap = getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"binderySignatureId");
		strLongID	  = (item) ? (CString)item->getNodeValue() : _T("");
		item		  = nodeMap->getNamedItem(L"rotation");
		strRotation	  = (item) ? (CString)item->getNodeValue() : _T("");

		int	nStringPos = 0;
		strID	= strLongID.Tokenize(_T("@"), nStringPos);
		strType = strLongID.Tokenize(_T("@"), nStringPos);
	}

	if (strLongID == "0@FoldSheets")
	{
		BOOL bStop = TRUE;
	}

	int	nOrientation = ROTATE0;
	if (strRotation.CompareNoCase(_T("ZERO"))		== 0) nOrientation = ROTATE0;   else
	if (strRotation.CompareNoCase(_T("CC90"))		== 0) nOrientation = ROTATE90;  else
	if (strRotation.CompareNoCase(_T("CC180"))		== 0) nOrientation = ROTATE180; else
	if (strRotation.CompareNoCase(_T("CC270"))		== 0) nOrientation = ROTATE270; else
	if (strRotation.CompareNoCase(_T("ZERO_FLIP"))	== 0) nOrientation = FLIP0;	    else
	if (strRotation.CompareNoCase(_T("CC90_FLIP"))	== 0) nOrientation = FLIP90;    else
	if (strRotation.CompareNoCase(_T("CC180_FLIP")) == 0) nOrientation = FLIP180;   else
	if (strRotation.CompareNoCase(_T("CC270_FLIP")) == 0) nOrientation = FLIP270;    

	CString  strNodeName;
	DOMNode* pNode = getFirstChild();
	while (pNode)
	{
		strNodeName = pNode->getNodeName();
		if (strNodeName == _T("spoV2:offset"))
		{
			nodeMap = pNode->getAttributes();
			if (nodeMap)
			{
				DOMNode* item = nodeMap->getNamedItem(L"x");
				strXOffset	  = (item) ? (CString)item->getNodeValue() : _T("");
				item		  = nodeMap->getNamedItem(L"y");
				strYOffset	  = (item) ? (CString)item->getNodeValue() : _T("");
			}
		}
		pNode = pNode->getNextSibling();
	}

	CPressParams& rPressParams = pLayout->GetPrintingProfile().m_press;

	BOOL bHasBackSide = FALSE;

	if (strType == _T("FoldSheets"))
	{
		POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(atoi(strID));
		if (pos)
		{
			float fXPos = pLayout->m_FrontSide.m_fPaperLeft   + (float)atof(strXOffset) / 1000.0f;
			float fYPos = pLayout->m_FrontSide.m_fPaperBottom + (float)atof(strYOffset) / 1000.0f;

			CFoldSheet& rNewFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);
			int nLayerIndex = 0;

			pLayout->AddFoldSheetLayer(&rNewFoldSheet, nLayerIndex, fXPos, fYPos, rNewFoldSheet.GetProductPart(), nOrientation);

			CFoldSheetLayerRef layerRef((short)atoi(strID), (short)nLayerIndex);
			pPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);
			bHasBackSide = TRUE;
		}
	}
	else
	{
		int			  nFlatProductIndex		= atoi(strID); 
		int			  nFlatProductRefIndex	= pPrintSheet->MakeFlatProductRefIndex(nFlatProductIndex);
		CFlatProduct& rFlatProduct			= pDoc->m_flatProducts.FindByIndex(nFlatProductIndex);
			
		if (rFlatProduct.m_nNumPages > 1)
			bHasBackSide = TRUE;
		
		if ( (nFlatProductIndex < 0) || (nFlatProductIndex >= pDoc->m_flatProducts.GetSize()) )
			return FALSE;

		int	 nHeadPosition = TOP;
		BOOL bFlip		   = FALSE;
		switch (nOrientation)
		{
		case ROTATE0:							break;
		case ROTATE90:	nHeadPosition = LEFT;	break;
		case ROTATE180:	nHeadPosition = BOTTOM;	break;
		case ROTATE270: nHeadPosition = RIGHT;	break;
		case FLIP0:		nHeadPosition = BOTTOM;	bFlip = TRUE; break;
		case FLIP90:	nHeadPosition = LEFT;	bFlip = TRUE; break;
		case FLIP180:	nHeadPosition = TOP;	bFlip = TRUE; break;
		case FLIP270:	nHeadPosition = RIGHT;	bFlip = TRUE; break;
		}
		
		BOOL bHasBackSide = (rFlatProduct.m_nNumPages > 1) ? TRUE : FALSE;

		float fWidth  = ( (nHeadPosition == TOP) ||  (nHeadPosition == BOTTOM) ) ? rFlatProduct.m_fTrimSizeWidth  : rFlatProduct.m_fTrimSizeHeight;
		float fHeight = ( (nHeadPosition == TOP) ||  (nHeadPosition == BOTTOM) ) ? rFlatProduct.m_fTrimSizeHeight : rFlatProduct.m_fTrimSizeWidth;
		float fLeftBorder = 0.0f, fBottomBorder = 0.0f; 
		switch (nHeadPosition)
		{
		case TOP:		fLeftBorder	  = rFlatProduct.m_fLeftTrim;
						fBottomBorder = rFlatProduct.m_fBottomTrim;
						break;
		case RIGHT:		fLeftBorder	  = rFlatProduct.m_fBottomTrim;
						fBottomBorder = rFlatProduct.m_fRightTrim;
						break;
		case BOTTOM:	fLeftBorder	  = rFlatProduct.m_fRightTrim;
						fBottomBorder = rFlatProduct.m_fTopTrim;
						break;
		case LEFT:		fLeftBorder	  = rFlatProduct.m_fTopTrim;
						fBottomBorder = rFlatProduct.m_fLeftTrim;
						break;
		}

		float fXPos = pLayout->m_FrontSide.m_fPaperLeft   + (float)atof(strXOffset) / 1000.0f + fLeftBorder;
		float fYPos = pLayout->m_FrontSide.m_fPaperBottom + (float)atof(strYOffset) / 1000.0f + fBottomBorder;
		CLayoutObject layoutObject; 
		layoutObject.CreateFlatProduct(fXPos, fYPos, fWidth, fHeight, nHeadPosition, nFlatProductRefIndex);
		pLayout->m_FrontSide.m_ObjectList.AddTail(layoutObject);
		CLayoutObject* pFrontLayoutObject = &pLayout->m_FrontSide.m_ObjectList.GetTail(); 

		layoutObject.CreateFlatProduct(fXPos, fYPos, fWidth, fHeight, nHeadPosition, nFlatProductRefIndex);
		pLayout->m_BackSide.m_ObjectList.AddTail(layoutObject);
		CLayoutObject* pBackLayoutObject = &pLayout->m_BackSide.m_ObjectList.GetTail();

		pBackLayoutObject->AlignToCounterPart(pFrontLayoutObject);
	}

	return TRUE;
}


