#pragma once


// CDlgLabelAssistent-Dialogfeld

class CDlgLabelAssistent : public CDialog
{
	DECLARE_DYNAMIC(CDlgLabelAssistent)

public:
	CDlgLabelAssistent(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgLabelAssistent();

// Dialogfelddaten
	enum { IDD = IDD_LABELASSISTENT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
};
