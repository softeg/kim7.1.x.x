#if !defined(AFX_DLGPRINTCOLORTABLE_H__8775FB14_FCDA_11D4_87ED_0040F68C1EF7__INCLUDED_)
#define AFX_DLGPRINTCOLORTABLE_H__8775FB14_FCDA_11D4_87ED_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPrintColorTable.h : Header-Datei
//



/////////////////////////////////////////////////////////////////////////////
// CPrintingColorList - custom CListCtrl - containing printing color definitions

class CPrintingColorListItem
{
public:
	CPrintingColorListItem() {};
	CPrintingColorListItem(COLORREF crRGB, CString strColorName, int nColorType, int nBasicColorIndex, int nAlternateColorIndex, int nIPUIndex)
	{
		m_crRGB = crRGB; m_strColorName = strColorName; m_nColorType = nColorType; 
		m_nBasicColorIndex = nBasicColorIndex; m_nAlternateColorIndex = nAlternateColorIndex; m_nIPUIndex = nIPUIndex;
	}


//Attributes:
public:
	COLORREF m_crRGB;
	CString	 m_strColorName;
	int		 m_nColorType;
	int		 m_nBasicColorIndex;
	int		 m_nAlternateColorIndex;
	int		 m_nIPUIndex;
};



class CPrintingColorList : public CListCtrl
{
public:
	CPrintingColorList();


//Attributes:
public:
	CColorDefinitionTable										m_colorDefTable;
	CArray <CPrintingColorListItem, CPrintingColorListItem&>	m_colorListItems;
	CImageList													m_imageList;


enum{ ColorItemHeight = 20 };


// Operations
public:
	void	   Initialize(CWnd* pParent, BOOL bShowBasicColorsOnly);
	void	   AddColorItem(COLORREF color, CString& strName, int nType, int nBasicColorIndex, int nAlternateColorIndex, int nIPUIndex);
	static int CompareColorDefs(const void *p1, const void *p2);
//	void DeleteColorItem(int nIndex);

// Implementation
//	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMIS);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
};




/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPrintColorTable 

class CDlgPrintColorTable : public CDialog
{
// Konstruktion
public:
	CDlgPrintColorTable(CWnd* pParent = NULL);   // Standardkonstruktor

//Attributes:
public:
	CPrintingColorList m_printingColorList;

//Operations:
public:


// Dialogfelddaten
	//{{AFX_DATA(CDlgPrintColorTable)
	enum { IDD = IDD_PRINTCOLOR_TABLE };
	BOOL	m_bShowBasicColorsOnly;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPrintColorTable)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	void AddUndefinedJobPrintColor(CColorDefinition* pColorDef);


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPrintColorTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnPrintcolorProps();
	afx_msg void OnDblclkPrintingColors(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnShowBasiccolorsOnly();
	afx_msg void OnAddPrintcolor();
	afx_msg void OnRemovePrintcolor();
	virtual void OnOK();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPRINTCOLORTABLE_H__8775FB14_FCDA_11D4_87ED_0040F68C1EF7__INCLUDED_
