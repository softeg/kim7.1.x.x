// ImpManDocPrintGroups.cpp : implementation of the CImpManDoc class
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "ImpManDocPrintGroups.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgSelectNewMark.h"
#include "DlgMarkSet.h"
#include "PaperDefsSelection.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "GraphicComponent.h"
#include "ProductsView.h"
#include "PrintSheetPlanningView.h"
#include "NewPrintSheetFrame.h"
#include "PrintComponentsView.h"


/////////////////////////////////////////////////////////////////////////////
// CPrintGroupList

IMPLEMENT_SERIAL(CPrintGroupList, CObject, VERSIONABLE_SCHEMA | 1)

CPrintGroupList::CPrintGroupList()
{
}

CPrintGroup& CPrintGroupList::GetPrintGroup(int nGroupIndex)
{
	static private CPrintGroup nullPrintGroup;

	if ( (nGroupIndex < 0) || (nGroupIndex >= GetSize()) )
		return nullPrintGroup;
	else
		return GetAt(nGroupIndex);
}

CPrintGroup* CPrintGroupList::FindFirstUnassigned()
{
	//static private CPrintGroup nullPrintGroup;

	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).IsLocked())
			continue;
		CPrintSheet* pPrintSheet = GetAt(i).GetFirstPrintSheet();
		if ( ! pPrintSheet)
			return &GetAt(i);
		else
			if (pPrintSheet->IsEmpty())
				return &GetAt(i);
	}

	return NULL;//nullPrintGroup;
}

void CPrintGroupList::UnlockGroups()
{
	for (int i = 0; i < GetSize(); i++)
	{
		GetAt(i).SetLock(FALSE);
	}
}

int	CPrintGroupList::GetPrintGroupIndex(CPrintGroup* pPrintGroup)
{
	for (int i = 0; i < GetSize(); i++)
		if (&GetAt(i) == pPrintGroup)
			return i;
	return -1;
}

BOOL g_bLockPrintGroupList = FALSE;

void CPrintGroupList::Analyze()
{
	if (g_bLockPrintGroupList)
		return;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	for (int i = 0; i < GetSize(); i++)
	{
		GetAt(i).SetLayout(NULL);

		if (pDoc->m_printingProfiles.GetSize() <= i)	// add missing printing profiles
		{
			CPrintingProfile printingProfile = pDoc->m_printGroupList.FindSameProductClass(GetAt(i), GetSize() - 1).GetPrintingProfile();
			if (printingProfile.IsNull())
			{
				CProductionProfile productionProfile = theApp.m_productionProfiles.FindProfile(pDoc->m_productionSetup.m_strProductionProfile);
				//if (productionProfile.IsNull())
				//	productionProfile = theApp.m_productionProfiles.FindSuitable(GetAt(i)); 

				printingProfile.AssignProductionProfilePool(productionProfile);

				CBoundProductPart* pProductPart = GetAt(i).GetFirstBoundProductPart();
				while (pProductPart)
				{
					CBoundProduct& rProduct	= pProductPart->GetBoundProduct();
					rProduct.AssignProductionProfilePool(productionProfile);
					pProductPart->AssignProductionProfilePool(productionProfile);
					pProductPart = GetAt(i).GetNextBoundProductPart(pProductPart);
				}
			}
			pDoc->m_printingProfiles.AddTail(printingProfile);
		}

		if (pDoc->m_PrintSheetList.m_Layouts.GetSize() <= i)	// add missing print sheets
		{
			CPrintingProfile& rProfile = pDoc->m_printingProfiles.GetPrintingProfile(i);
			CLayout newLayout;
			newLayout.Create(rProfile);
			pDoc->m_PrintSheetList.m_Layouts.AddTail(newLayout);
			int nLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;
			CPrintSheet newPrintSheet;
			newPrintSheet.Create(nLayoutIndex, i, &pDoc->m_PrintSheetList.m_Layouts.GetTail());	
			pDoc->m_PrintSheetList.AddTail(newPrintSheet);
		}
	}

	while (pDoc->m_PrintSheetList.m_Layouts.GetSize() > pDoc->m_printingProfiles.GetSize())
		pDoc->m_PrintSheetList.m_Layouts.RemoveTail();

	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if ( ! rLayout.GetFirstPrintSheet())
		{
			int nLayoutIndex = rLayout.GetIndex();
			CPrintSheet newPrintSheet;
			newPrintSheet.Create(nLayoutIndex, 0, &rLayout);	
			pDoc->m_PrintSheetList.AddTail(newPrintSheet);
		}
	}

	pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		CLayout*	 pLayout	 = rPrintSheet.GetLayout();
		if ( ! pLayout)
			continue;
		if (pLayout->GetFirstPrintSheet() != &rPrintSheet)
			continue;

		int nLayoutIndex = pLayout->GetIndex();
		if (nLayoutIndex >= GetSize())
		{
			CPrintGroup printGroup;
			CreatePrintGroupFromLayout(*pLayout, printGroup);
			Add(printGroup);
		}
		else
		{
			CPrintGroup& rPrintGroup = pLayout->GetPrintGroup();
			CreatePrintGroupFromLayout(*pLayout, rPrintGroup);
		}

		if (nLayoutIndex >= pDoc->m_printingProfiles.GetSize())
		{
			CPrintGroup& rPrintGroup = pLayout->GetPrintGroup();
			CPrintingProfile printingProfile = pDoc->m_printGroupList.FindSameProductClass(rPrintGroup, GetSize() - 1).GetPrintingProfile();
			if (printingProfile.IsNull())
			{
				CProductionProfile productionProfile = theApp.m_productionProfiles.FindProfile(pDoc->m_productionSetup.m_strProductionProfile);
				//if (productionProfile.IsNull())
				//	productionProfile = theApp.m_productionProfiles.FindSuitable(rPrintGroup); 

				printingProfile.AssignProductionProfilePool(productionProfile);

				CBoundProductPart* pProductPart = rPrintGroup.GetFirstBoundProductPart();
				while (pProductPart)
				{
					CBoundProduct& rProduct	= pProductPart->GetBoundProduct();
					rProduct.AssignProductionProfilePool(productionProfile);
					pProductPart->AssignProductionProfilePool(productionProfile);
					pProductPart = rPrintGroup.GetNextBoundProductPart(pProductPart);
				}
			}
			pDoc->m_printingProfiles.AddTail(printingProfile);
		}
	}

	UnassignedProductsToPrintGroups();
	RemoveDoubleUnassignedComponents();

	pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		POSITION	 pos1		  = pos;
		CPrintSheet& rPrintSheet  = pDoc->m_PrintSheetList.GetNext(pos);
		CPrintGroup& rPrintGroup  = rPrintSheet.GetPrintGroup();
		int			 nLayoutIndex = rPrintSheet.m_nLayoutIndex;
		if ( (nLayoutIndex >= GetSize()) || rPrintGroup.IsNull() )
		{
			pDoc->m_PrintSheetList.CList <CPrintSheet, CPrintSheet&>::RemoveAt(pos1); 
			pos1 = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nLayoutIndex);
			if (pos1)
				pDoc->m_PrintSheetList.m_Layouts.RemoveAt(pos1);
			pos1 = pDoc->m_printingProfiles.FindIndex(nLayoutIndex);
			if (pos1)
				pDoc->m_printingProfiles.RemoveAt(pos1);
			if ( (nLayoutIndex >= 0) && (nLayoutIndex < pDoc->m_printGroupList.GetSize()) )
				pDoc->m_printGroupList.RemoveAt(nLayoutIndex);
		}
	}

	pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		while (rLayout.GetNumEmptyPrintSheetsBasedOn() > 1)
		{
			CPrintSheet* pPrintSheet = rLayout.GetLastEmptyPrintSheet();	// remove as many empty printsheets, as only one remains
			if (pPrintSheet)
			{
				POSITION posRemove = pDoc->m_PrintSheetList.GetPos(*pPrintSheet);
				pDoc->m_PrintSheetList.CList <CPrintSheet, CPrintSheet&>::RemoveAt(posRemove); 
			}
		}
	}

	pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			rPrintSheet.m_nQuantityTotal = rPrintSheet.m_nQuantityExtra = 0;
	}

	ReorderEmptyPrintSheets();

	pDoc->m_PrintSheetList.ReNumberPrintSheets();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();

	pDoc->m_printComponentsList.Analyze();
}

void CPrintGroupList::CreatePrintGroupFromLayout(CLayout& rLayout, CPrintGroup& rPrintGroup)
{
	rPrintGroup.m_componentList.RemoveAll();

	CPrintSheet* pPrintSheet = rLayout.GetFirstPrintSheet();
	while (pPrintSheet)
	{
		CBoundProductPart* pProductPart = pPrintSheet->GetFirstBoundProductPart();
		while (pProductPart)
		{
			rPrintGroup.m_colorantList.MixColors(pProductPart->m_colorantList);
			rPrintGroup.m_strPaper			= pProductPart->m_strPaper;
			rPrintGroup.m_nPaperGrammage	= pProductPart->m_nPaperGrammage;
			rPrintGroup.m_fPaperVolume		= pProductPart->m_fPaperVolume;
			rPrintGroup.m_nPaperType		= pProductPart->m_nPaperType;
			rPrintGroup.SetQuantityRange(pProductPart->m_nDesiredQuantity);
			rPrintGroup.AddBoundProductPartComponent(*pProductPart, pPrintSheet->GetNumPagesProductPart(pProductPart->GetIndex()));

			pProductPart = pPrintSheet->GetNextBoundProductPart(pProductPart);
		}

		for (int i = 0; i < pPrintSheet->m_flatProductTypeList.GetSize(); i++)
		{
			CFlatProductType& rFlatProductType = pPrintSheet->m_flatProductTypeList[i];
			CFlatProduct*	  pFlatProduct	   = rFlatProductType.m_pFlatProduct;
			if ( ! pFlatProduct)
				continue;
			rPrintGroup.m_colorantList.MixColors(pFlatProduct->m_colorantList);
			rPrintGroup.m_strPaper			= pFlatProduct->m_strPaper;
			rPrintGroup.m_nPaperGrammage	= pFlatProduct->m_nPaperGrammage;
			rPrintGroup.m_fPaperVolume		= pFlatProduct->m_fPaperVolume;
			rPrintGroup.m_nPaperType		= pFlatProduct->m_nPaperType;
			rPrintGroup.SetQuantityRange(pFlatProduct->m_nDesiredQuantity);
			rPrintGroup.AddFlatProductComponent(*pFlatProduct);
		}

		pPrintSheet->SetPrintGroupIndex(GetSize() - 1);

		pPrintSheet = rLayout.GetNextPrintSheet(pPrintSheet);
	}
}

void CPrintGroupList::UnassignedProductsToPrintGroups()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);

		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			CBoundProductPart& rProductPart = rBoundProduct.m_parts[i];
			int nNumUnassignedPages = rProductPart.GetNumUnassignedPages();
			if (nNumUnassignedPages > 0)
			{
				CPrintGroup printGroup;
				printGroup.m_colorantList.MixColors(rProductPart.m_colorantList);
				printGroup.m_strPaper		= rProductPart.m_strPaper;
				printGroup.m_nPaperGrammage	= rProductPart.m_nPaperGrammage;
				printGroup.m_fPaperVolume	= rProductPart.m_fPaperVolume;
				printGroup.m_nPaperType		= rProductPart.m_nPaperType;
				printGroup.SetQuantityRange(rProductPart.m_nDesiredQuantity);
				printGroup.AddBoundProductPartComponent(rProductPart, nNumUnassignedPages);
				AddFindPrintGroup(printGroup);
			}
		}
	}

	pos = pDoc->m_flatProducts.GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);
		if (rFlatProduct.GetNUpAll() <= 0)
		{
			CPrintGroup printGroup;
			printGroup.m_colorantList.MixColors(rFlatProduct.m_colorantList);
			printGroup.m_strPaper		= rFlatProduct.m_strPaper;
			printGroup.m_nPaperGrammage	= rFlatProduct.m_nPaperGrammage;
			printGroup.m_fPaperVolume	= rFlatProduct.m_fPaperVolume;
			printGroup.m_nPaperType		= rFlatProduct.m_nPaperType;
			printGroup.SetQuantityRange(rFlatProduct.m_nDesiredQuantity);
			printGroup.AddFlatProductComponent(rFlatProduct);
			AddFindPrintGroup(printGroup);
		}
	}
}

void CPrintGroupList::AddFindPrintGroup(CPrintGroup& rPrintGroup)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (rPrintGroup.m_componentList.GetSize() <= 0)
		return;

	CPrintGroupComponent& rNewComponent = rPrintGroup.m_componentList[0];

	for (int i = 0; i < GetSize(); i++)
	{
		CPrintGroup& rCurPrintGroup = GetAt(i);

		CLayout*	 pLayout	 = rCurPrintGroup.GetLayout();
		CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
		if (pPrintSheet)
			if ( ! pPrintSheet->IsEmpty())
				continue;
	
		//// do inspect this group only if group has no sheet yet or if sheet is empty
		if (rCurPrintGroup.m_strPaper == rPrintGroup.m_strPaper)
		{
			if (rCurPrintGroup.CheckInsideQuantityRange(rPrintGroup, pDoc->m_productionSetup))
			{
				if (rCurPrintGroup.CheckInsideColorRange(rPrintGroup, pDoc->m_productionSetup))
				{
					BOOL bAleadyExisting = FALSE;
					for (int j = 0; j < rCurPrintGroup.m_componentList.GetSize(); j++)
					{
						if (rCurPrintGroup.m_componentList[j].m_nProductClass == rNewComponent.m_nProductClass)
						{
							if (rNewComponent.m_nProductClass == CPrintGroupComponent::Bound)
							{
								if (rCurPrintGroup.m_componentList[j].m_nProductClassIndex == rNewComponent.m_nProductClassIndex)
								{
									bAleadyExisting = TRUE;
									rCurPrintGroup.m_componentList[j].m_nNumBoundPages = rNewComponent.m_nNumBoundPages;
									break;
								}
							}
							if (rNewComponent.m_nProductClass == CPrintGroupComponent::Flat)
							{
								if (rCurPrintGroup.m_componentList[j].m_nFlatProductIndex == rNewComponent.m_nFlatProductIndex)
								{
									bAleadyExisting = TRUE;
									break;
								}
							}
						}
					}

					if ( ! bAleadyExisting)
					{
						CBoundProductPart* pRefProductPart = rCurPrintGroup.GetFirstBoundProductPart();

						rCurPrintGroup.InsertComponent(rNewComponent);
						rCurPrintGroup.m_colorantList.MixColors(rPrintGroup.m_colorantList);

						//// set all product parts to same binding, trimming and folding params as first one
						if (pRefProductPart)
						{
							CBoundProduct&	   rRefProduct	= pRefProductPart->GetBoundProduct();
							CBoundProductPart* pProductPart	= rPrintGroup.GetFirstBoundProductPart();
							while (pProductPart)
							{
								if (pProductPart != pRefProductPart)
								{
									CBoundProduct& rProduct		   = pProductPart->GetBoundProduct();
									rProduct.m_bindingParams	   = rRefProduct.m_bindingParams;
									pProductPart->m_trimmingParams = pRefProductPart->m_trimmingParams;
									pProductPart->m_foldingParams  = pRefProductPart->m_foldingParams;
								}
								pProductPart = rCurPrintGroup.GetNextBoundProductPart(pProductPart);
							}
						}
					}

					return;
				}
			}
		}
	}

	CPrintingProfile printingProfile = pDoc->m_printGroupList.FindSameProductClass(rPrintGroup, GetSize() - 1).GetPrintingProfile();
	if (printingProfile.IsNull())
	{
		CProductionProfile productionProfile = theApp.m_productionProfiles.FindProfile(pDoc->m_productionSetup.m_strProductionProfile);
		//if (productionProfile.IsNull())
		//	productionProfile = theApp.m_productionProfiles.FindSuitable(rPrintGroup);	// does not work properly

		if ( ! productionProfile.IsNull())
		{
			printingProfile.AssignProductionProfilePool(productionProfile);

			CBoundProductPart* pProductPart = rPrintGroup.GetFirstBoundProductPart();
			while (pProductPart)
			{
				CBoundProduct& rProduct	= pProductPart->GetBoundProduct();
				rProduct.AssignProductionProfilePool(productionProfile);
				pProductPart->AssignProductionProfilePool(productionProfile);
				pProductPart = rPrintGroup.GetNextBoundProductPart(pProductPart);
			}
		}
	}
	pDoc->m_PrintSheetList.AddBlankSheet(printingProfile, -1, &rPrintGroup);
}

void CPrintGroupList::RemoveDoubleUnassignedComponents()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	for (int nGroupIndex1 = 0; nGroupIndex1 < GetSize(); nGroupIndex1++)
	{
		CPrintGroup& rPrintGroup1 = GetAt(nGroupIndex1);
		CLayout*	 pLayout	 = rPrintGroup1.GetLayout();
		CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
		if (pPrintSheet)
			if ( ! pPrintSheet->IsEmpty())
				continue;

		for (int nComponentIndex1 = 0; nComponentIndex1 < rPrintGroup1.m_componentList.GetSize(); nComponentIndex1++)
		{
			CPrintGroupComponent& rComponent1 = rPrintGroup1.m_componentList[nComponentIndex1];
			if (rComponent1.m_nProductClass == CPrintGroupComponent::Bound)
			{
				CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(rComponent1.m_nProductClassIndex);
				rComponent1.m_nNumBoundPages = min(rComponent1.m_nNumBoundPages, rProductPart.GetNumUnassignedPages());
				if (rComponent1.m_nNumBoundPages <= 0)
				{
					rPrintGroup1.m_componentList.RemoveAt(nComponentIndex1);
					nComponentIndex1--;
				}
			}
			//else
			//if (rComponent1.m_nProductClass == CPrintGroupComponent::Flat)
			//{
			//	CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(rComponent1.m_nFlatProductIndex);
			//	if (rFlatProduct.GetNUpAll() <= 0)
			//	{
			//		rPrintGroup1.m_componentList.RemoveAt(nComponentIndex1);
			//		nComponentIndex1--;
			//	}
			//}

			// now, remove all following/double components
			for (int nGroupIndex2 = nGroupIndex1 + 1; nGroupIndex2 < GetSize(); nGroupIndex2++)
			{
				CPrintGroup& rPrintGroup2 = GetAt(nGroupIndex2);
				pLayout		= rPrintGroup2.GetLayout();
				pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
				if (pPrintSheet)
					if ( ! pPrintSheet->IsEmpty())
						continue;

				for (int nComponentIndex2 = 0; nComponentIndex2 < rPrintGroup2.m_componentList.GetSize(); nComponentIndex2++)
				{
					CPrintGroupComponent& rComponent2 = rPrintGroup2.m_componentList[nComponentIndex2];
		
					if (rComponent1.m_nProductClass == rComponent2.m_nProductClass)
					{
						if (rComponent1.m_nProductClass == CPrintGroupComponent::Bound)
						{
							if (rComponent1.m_nProductClassIndex == rComponent2.m_nProductClassIndex)
							{
								rPrintGroup2.m_componentList.RemoveAt(nComponentIndex2);
								nComponentIndex2--;
							}
						}
						else
						if (rComponent1.m_nProductClass == CPrintGroupComponent::Flat)
						{
							if (rComponent1.m_nFlatProductIndex == rComponent2.m_nFlatProductIndex)
							{
								rPrintGroup2.m_componentList.RemoveAt(nComponentIndex2);
								nComponentIndex2--;
							}
						}
					}
				}
				if (rPrintGroup2.m_componentList.GetSize() <= 0)
				{
					RemoveAt(nGroupIndex2);
					POSITION pos = pDoc->m_printingProfiles.FindIndex(nGroupIndex2);
					if (pos)
						pDoc->m_printingProfiles.RemoveAt(pos);
					nGroupIndex2--;
				}
			}
		}

		//if (rPrintGroup1.m_componentList.GetSize() <= 0)
		//{
		//	RemoveAt(nGroupIndex1);
		//	POSITION pos = pDoc->m_printingProfiles.FindIndex(nGroupIndex1);
		//	if (pos)
		//		pDoc->m_printingProfiles.RemoveAt(pos);
		//	nGroupIndex1--;
		//}
	}
}

void CPrintGroupList::ReorderEmptyPrintSheets()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CArray <CPrintSheet*, CPrintSheet*> sortedPrintSheets;
	CPrintSheetList emptyPrintSheets;

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		POSITION prevPos = pos;
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
		{
			POSITION newPos = emptyPrintSheets.AddTail(rPrintSheet);
			pDoc->m_PrintSheetList.CList <CPrintSheet, CPrintSheet&>::RemoveAt(prevPos); 
			sortedPrintSheets.Add(&emptyPrintSheets.GetAt(newPos));
		}
	}

	qsort((void*)sortedPrintSheets.GetData(), sortedPrintSheets.GetSize(), sizeof(CPrintSheet*), ComparePrintSheetPos);

	for (int i = 0; i < sortedPrintSheets.GetSize(); i++)
	{
		CPrintSheet* pPrintSheet = sortedPrintSheets[i];
		
		BOOL	 bFound = FALSE;;
		POSITION pos	= pDoc->m_PrintSheetList.GetHeadPosition();
		while (pos)
		{
			CPrintSheet& rRefPrintSheet = pDoc->m_PrintSheetList.GetAt(pos);
			CPrintSheet* pRefPrintSheet = &rRefPrintSheet;
			if (ComparePrintSheetPos((const void*)&pPrintSheet, (const void*)&pRefPrintSheet) < 0)
			{
				pDoc->m_PrintSheetList.CList <CPrintSheet, CPrintSheet&>::InsertBefore(pos, *pPrintSheet);
				bFound = TRUE;
				break;
			}
			pDoc->m_PrintSheetList.GetNext(pos);
		}
		if ( ! bFound)
			pDoc->m_PrintSheetList.AddTail(*pPrintSheet);
	}
}

int CPrintGroupList::ComparePrintSheetPos(const void *p1, const void *p2)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CPrintSheet* pPrintSheet1 = *(CPrintSheet**)p1;
	CPrintSheet* pPrintSheet2 = *(CPrintSheet**)p2;
	CPrintGroup& rPrintGroup1 = pPrintSheet1->GetPrintGroup();
	CPrintGroup& rPrintGroup2 = pPrintSheet2->GetPrintGroup();

	if (pPrintSheet1->IsEmpty() && ! pPrintSheet2->IsEmpty())
	{
		if (rPrintGroup1.IsSimilar(rPrintGroup2))
			return 1;
	}
	else
		if (! pPrintSheet1->IsEmpty() && pPrintSheet2->IsEmpty())
		{
			if (rPrintGroup1.IsSimilar(rPrintGroup2))
				return -1;
		}

	if (rPrintGroup1.m_componentList.GetSize() <= 0)
		return -1;
	else
		if (rPrintGroup2.m_componentList.GetSize() <= 0)
			return 1;

	CPrintGroupComponent& rComponent1 = rPrintGroup1.m_componentList[0];
	CPrintGroupComponent& rComponent2 = rPrintGroup2.m_componentList[0];

	if ( (rComponent1.m_nProductClass == CPrintGroupComponent::Flat)  && (rComponent2.m_nProductClass == CPrintGroupComponent::Flat) )
	{
		if (rComponent1.m_nFlatProductIndex < rComponent2.m_nFlatProductIndex)
			return -1;
		else
			if (rComponent1.m_nFlatProductIndex > rComponent2.m_nFlatProductIndex)
				return 1;
			else
				return 0;
	}
	else
		if ( (rComponent1.m_nProductClass == CPrintGroupComponent::Bound) && (rComponent2.m_nProductClass == CPrintGroupComponent::Bound) )
		{
			if (rComponent1.m_nProductClassIndex < rComponent2.m_nProductClassIndex)
				return -1;
			else
				if (rComponent1.m_nProductClassIndex > rComponent2.m_nProductClassIndex)
					return 1;
				else
					return 0;
		}
		else
			if (rComponent1.m_nProductClass == CPrintGroupComponent::Bound)
				return -1;
			else
				return 1;
}

int CPrintGroupList::GetPrintSheetInsertionPos(CPrintGroup& rPrintGroup)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;
	CBoundProductPart*	pProductPart = rPrintGroup.GetFirstBoundProductPart();
	if ( ! pProductPart)
		return -1;
	int nProductPartIndex = pProductPart->GetIndex();
	if (nProductPartIndex < 0)
		return 0;
	if ( (nProductPartIndex == 0) && (pProductPart->GetNumPlacedPages() == 0))
		return 0;

	if (pProductPart->GetNumPlacedPages() == 0)	// if there is no printsheet with this product part on, look for last sheet of previous part
	{
		nProductPartIndex--;
		pProductPart = &pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
	}

	int			nCurIndex		= 0;
	int			nInsertionIndex = -1;
	POSITION	pos				= pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.GetFirstBoundProductPart() == pProductPart)
		{
			nInsertionIndex = nCurIndex;
		}
		nCurIndex++;
	}

	nInsertionIndex++;
	return nInsertionIndex;
}

CPrintGroup& CPrintGroupList::FindSameProductClass(CPrintGroup& rPrintGroup, int nExceptThisIndex)
{
	static private CPrintGroup nullPrintGroup;

	if ( (rPrintGroup.m_componentList.GetSize() <= 0) || (GetSize() <= 0) )
		return nullPrintGroup;

	for (int i = 0; i < GetSize(); i++)
	{
		CPrintGroup& rCurPrintGroup = GetAt(i);
		if (i != nExceptThisIndex)
		{
			for (int j = 0; j < rCurPrintGroup.m_componentList.GetSize(); j++)
			{
				if (rCurPrintGroup.m_componentList[j].m_nProductClassIndex == rPrintGroup.m_componentList[0].m_nProductClassIndex)
					return rCurPrintGroup;
			}
		}
	}
	return GetAt(0);
}

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CPrintGroup

IMPLEMENT_SERIAL(CPrintGroup, CPrintGroup, VERSIONABLE_SCHEMA | 5)

CPrintGroup::CPrintGroup()
{
	m_dspi.rcDisplayBox.SetRectEmpty();
	m_dspi.bShowExpanded	= TRUE;
	m_bLocked				= FALSE;
	m_pLayout				= NULL;

	m_strName.Empty();
	m_colorantList.RemoveAll();
	m_strPaper.Empty();
	m_nPaperGrammage = 0;
	m_fPaperVolume	 = 0.0f;
	m_nPaperType	 = 0;
	m_nMinQuantity	 = INT_MAX;
	m_nMaxQuantity	 = -INT_MAX;

	m_strComment.Empty();
	m_strDefaultProfile.Empty();
}

CPrintGroup::CPrintGroup(const CPrintGroup& rPrintGroup)
{
	Copy(rPrintGroup);
}

const CPrintGroup& CPrintGroup::operator=(const CPrintGroup& rPrintGroup)
{
	Copy(rPrintGroup);

	return *this;
}

BOOL CPrintGroup::operator==(const CPrintGroup& rPrintGroup)
{
	if (m_strName			!= rPrintGroup.m_strName)			return FALSE;
	if (m_strPaper			!= rPrintGroup.m_strPaper)			return FALSE;
	if (m_nPaperGrammage	!= rPrintGroup.m_nPaperGrammage)	return FALSE;
	if (m_fPaperVolume		!= rPrintGroup.m_fPaperVolume)		return FALSE;
	if (m_nPaperType		!= rPrintGroup.m_nPaperType)		return FALSE;
	if (m_nMinQuantity		!= rPrintGroup.m_nMinQuantity)		return FALSE;
	if (m_nMaxQuantity		!= rPrintGroup.m_nMaxQuantity)		return FALSE;
	if (m_strComment		!= rPrintGroup.m_strComment)		return FALSE;
	if (m_strDefaultProfile != rPrintGroup.m_strDefaultProfile)	return FALSE;

	if (m_colorantList.GetSize() != rPrintGroup.m_colorantList.GetSize())
		return FALSE;
	for (int i = 0; i < m_colorantList.GetSize(); i++)
	{
		if (m_colorantList[i].m_crRGB	!= rPrintGroup.m_colorantList[i].m_crRGB)
			return FALSE;
		if (m_colorantList[i].m_strName != rPrintGroup.m_colorantList[i].m_strName)
			return FALSE;
	}

	if (m_componentList.GetSize() != rPrintGroup.m_componentList.GetSize())
		return FALSE;
	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i] != rPrintGroup.m_componentList[i])
			return FALSE;
	}

	return TRUE;
}

BOOL CPrintGroup::IsSimilar(CPrintGroup& rPrintGroup)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if (m_strPaper != rPrintGroup.m_strPaper)			
		return FALSE;

	if (GetPrintingProfile() != rPrintGroup.GetPrintingProfile())
		return FALSE;

	if ( ! CheckInsideQuantityRange(rPrintGroup, pDoc->m_productionSetup))
		return FALSE;

	if ( ! CheckInsideColorRange(rPrintGroup, pDoc->m_productionSetup))
		return FALSE;

	return TRUE;
}

void CPrintGroup::Copy(const CPrintGroup& rPrintGroup)
{
	m_dspi				= rPrintGroup.m_dspi;
	m_bLocked			= rPrintGroup.m_bLocked;

	m_strName			= rPrintGroup.m_strName;
	m_colorantList.RemoveAll();
	m_colorantList.Append(rPrintGroup.m_colorantList);
	m_strPaper			= rPrintGroup.m_strPaper;
	m_nPaperGrammage	= rPrintGroup.m_nPaperGrammage;
	m_fPaperVolume		= rPrintGroup.m_fPaperVolume;
	m_nPaperType		= rPrintGroup.m_nPaperType;
	m_nMinQuantity		= rPrintGroup.m_nMinQuantity;
	m_nMaxQuantity		= rPrintGroup.m_nMaxQuantity;

	m_strComment		= rPrintGroup.m_strComment;
	m_componentList.RemoveAll();
	m_componentList.Append(rPrintGroup.m_componentList);
	m_strDefaultProfile = rPrintGroup.m_strDefaultProfile;
}

int	CPrintGroup::GetIndex()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
		if ( &pDoc->m_printGroupList[i] == this)
			return i;

	return -1;
}

CPrintGroupComponent& CPrintGroup::GetComponent(int nComponentIndex)
{
	static private CPrintGroupComponent nullPrintGroupComponent;

	if ( (nComponentIndex < 0) || (nComponentIndex >= m_componentList.GetSize()) )
		return nullPrintGroupComponent;
	else
		return m_componentList[nComponentIndex];
}

void CPrintGroup::AddBoundProductPartComponent(CBoundProductPart& rProductPart, int nNumPages)
{
	int nProductPartIndex = rProductPart.GetIndex();

	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i].m_nProductClassIndex == nProductPartIndex)
		{
			m_componentList[i].m_nNumBoundPages += nNumPages;
			return;
		}
	}

	CPrintGroupComponent component;

	component.m_nProductClass	   = CPrintGroupComponent::Bound;
	component.m_nProductClassIndex = nProductPartIndex;
	component.m_nNumBoundPages	   = nNumPages;

	InsertComponent(component);
}

void CPrintGroup::AddFlatProductComponent(CFlatProduct& rFlatProduct)
{
	int nProductClassIndex = rFlatProduct.GetFlatProductsIndex();
	int nFlatProductIndex  = rFlatProduct.GetIndex();

	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i].m_nFlatProductIndex == nFlatProductIndex)
			return;
	}

	CPrintGroupComponent component;

	component.m_nProductClass	   = CPrintGroupComponent::Flat;
	component.m_nProductClassIndex = nProductClassIndex;
	component.m_nFlatProductIndex  = nFlatProductIndex;
	component.m_nNumBoundPages	   = 1;

	InsertComponent(component);
}

void CPrintGroup::InsertComponent(CPrintGroupComponent component)
{
	BOOL bInsertAfter = TRUE;

	if (component.m_nProductClass == CPrintGroupComponent::Bound)
	{
		for (int i = 0; i < m_componentList.GetSize(); i++)
		{
			CPrintGroupComponent& rRefComponent = m_componentList[i];

			if (rRefComponent.m_nProductClass == CPrintGroupComponent::Bound)
			{
				if (component.m_nProductClassIndex < rRefComponent.m_nProductClassIndex)
				{
					m_componentList.InsertAt(i, component);
					m_componentList.GetAt(i).SetPrintGroupIndex(GetIndex());
					bInsertAfter = FALSE;
					break;
				}
			}
			else
			{
				m_componentList.InsertAt(i, component);
				m_componentList.GetAt(i).SetPrintGroupIndex(GetIndex());
				bInsertAfter = FALSE;
				break;
			}
		}
	}
	else
	{
		for (int i = 0; i < m_componentList.GetSize(); i++)
		{
			CPrintGroupComponent& rRefComponent = m_componentList[i];

			if (rRefComponent.m_nProductClass == CPrintGroupComponent::Flat)
			{
				if (component.m_nFlatProductIndex < rRefComponent.m_nFlatProductIndex)
				{
					m_componentList.InsertAt(i, component);
					m_componentList.GetAt(i).SetPrintGroupIndex(GetIndex());
					bInsertAfter = FALSE;
					break;
				}
			}
		}
	}

	if (bInsertAfter)
	{
		m_componentList.Add(component);
		m_componentList.GetAt(m_componentList.GetSize() - 1).SetPrintGroupIndex(GetIndex());
	}
}

CPrintGroupComponent* CPrintGroup::GetFirstBoundComponent()
{
	return GetNextBoundComponent(NULL);
}

CPrintGroupComponent* CPrintGroup::GetNextBoundComponent(CPrintGroupComponent* pComponent)
{
	int nFirstIndex = 0;
	if (pComponent)
	{
		for (int i = 0; i < m_componentList.GetSize(); i++)
		{
			if (&m_componentList[i] == pComponent)
			{
				nFirstIndex = i + 1;
				break;
			}
		}
	}
	for (int i = nFirstIndex; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i].m_nProductClass == CPrintGroupComponent::Bound)
		{
			return &m_componentList[i];
		}
	}
	return NULL;
}

CBoundProductPart* CPrintGroup::GetFirstBoundProductPart()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	CPrintGroupComponent* pComponent = GetFirstBoundComponent();
	if ( ! pComponent)
		return NULL;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(pComponent->m_nProductClassIndex);
	if (rProductPart.IsNull())
		return NULL;
	else
		return &rProductPart;
}

CBoundProductPart* CPrintGroup::GetNextBoundProductPart(CBoundProductPart* pProductPart)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	CPrintGroupComponent* pComponent = GetFirstBoundComponent();
	while (pComponent)
	{
		CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(pComponent->m_nProductClassIndex);
		if (&rProductPart == pProductPart)
		{
			pComponent = GetNextBoundComponent(pComponent);
			break;	
		}
		pComponent = GetNextBoundComponent(pComponent);
	}

	if (pComponent)
	{
		CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(pComponent->m_nProductClassIndex);
		if (rProductPart.IsNull())
			return NULL;
		else
			return &rProductPart;
	}

	return NULL;
}

CFlatProduct* CPrintGroup::GetFirstFlatProduct()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i].m_nProductClass == CPrintGroupComponent::Flat)
		{
			return &pDoc->m_flatProducts.FindByIndex(m_componentList[i].m_nFlatProductIndex);
		}
	}
	return NULL;
}

CFlatProduct* CPrintGroup::GetNextFlatProduct(CFlatProduct* pProduct)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int i = 0;
	for (; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i].m_nProductClass == CPrintGroupComponent::Flat)
			if (pProduct == &pDoc->m_flatProducts.FindByIndex(m_componentList[i].m_nFlatProductIndex))
				break;
	}

	i++;
	for (; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i].m_nProductClass == CPrintGroupComponent::Flat)
			return &pDoc->m_flatProducts.FindByIndex(m_componentList[i].m_nFlatProductIndex);
	}
	return NULL;
}

CPrintingProfile& CPrintGroup::GetPrintingProfile()
{
	static private CPrintingProfile nullPrintingProfile;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullPrintingProfile;

	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.FindIndex(GetIndex());
	if (pos)
		return pDoc->m_PrintSheetList.m_Layouts.GetAt(pos).GetPrintingProfile();
	else
	{
		POSITION pos = pDoc->m_printingProfiles.FindIndex(GetIndex());
		if (pos)
			return pDoc->m_printingProfiles.GetAt(pos);
		//else
		//	return theApp.m_printingProfiles.FindProfile(m_strDefaultProfile);
	}

	return nullPrintingProfile;
}


// helper function for CPrintGroup elements
template <> void AFXAPI SerializeElements <CPrintGroup> (CArchive& ar, CPrintGroup* pPrintGroup, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrintGroup));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPrintGroup->m_strName;
			ar << pPrintGroup->m_strPaper;
			ar << pPrintGroup->m_nPaperGrammage;
			ar << pPrintGroup->m_fPaperVolume;
			ar << pPrintGroup->m_nPaperType;
			ar << pPrintGroup->m_nMinQuantity;
			ar << pPrintGroup->m_nMaxQuantity;
			ar << pPrintGroup->m_strComment;
			ar << pPrintGroup->m_strDefaultProfile;
			pPrintGroup->m_componentList.Serialize(ar);
		}	
		else
		{ 
			switch (nVersion)
			{
				case 1:	ar >> pPrintGroup->m_strName;
						ar >> pPrintGroup->m_strPaper;
						ar >> pPrintGroup->m_nPaperGrammage;
						ar >> pPrintGroup->m_fPaperVolume;
						ar >> pPrintGroup->m_strComment;
						break;
				case 2:	ar >> pPrintGroup->m_strName;
						ar >> pPrintGroup->m_strPaper;
						ar >> pPrintGroup->m_nPaperGrammage;
						ar >> pPrintGroup->m_fPaperVolume;
						ar >> pPrintGroup->m_strComment;
						ar >> pPrintGroup->m_strDefaultProfile;
						break;
				case 3:	ar >> pPrintGroup->m_strName;
						ar >> pPrintGroup->m_strPaper;
						ar >> pPrintGroup->m_nPaperGrammage;
						ar >> pPrintGroup->m_fPaperVolume;
						ar >> pPrintGroup->m_strComment;
						ar >> pPrintGroup->m_strDefaultProfile;
						pPrintGroup->m_componentList.Serialize(ar);
						break;
				case 4:	ar >> pPrintGroup->m_strName;
						ar >> pPrintGroup->m_strPaper;
						ar >> pPrintGroup->m_nPaperGrammage;
						ar >> pPrintGroup->m_fPaperVolume;
						ar >> pPrintGroup->m_nMinQuantity;
						ar >> pPrintGroup->m_nMaxQuantity;
						ar >> pPrintGroup->m_strComment;
						ar >> pPrintGroup->m_strDefaultProfile;
						pPrintGroup->m_componentList.Serialize(ar);
						break;
				case 5:	ar >> pPrintGroup->m_strName;
						ar >> pPrintGroup->m_strPaper;
						ar >> pPrintGroup->m_nPaperGrammage;
						ar >> pPrintGroup->m_fPaperVolume;
						ar >> pPrintGroup->m_nPaperType;
						ar >> pPrintGroup->m_nMinQuantity;
						ar >> pPrintGroup->m_nMaxQuantity;
						ar >> pPrintGroup->m_strComment;
						ar >> pPrintGroup->m_strDefaultProfile;
						pPrintGroup->m_componentList.Serialize(ar);
						break;
			}
		}

		pPrintGroup->m_colorantList.Serialize(ar);

		pPrintGroup++;
	}
}

CPrintSheet* CPrintGroup::GetFirstPrintSheet()
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return NULL;
	else
		return pLayout->GetFirstPrintSheet();
	//return GetNextPrintSheet(NULL);
}

CPrintSheet* CPrintGroup::GetNextPrintSheet(CPrintSheet* pPrintSheet)
{
	//CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return NULL;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return NULL;
	else
		return pLayout->GetNextPrintSheet(pPrintSheet);

	//POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	//if (pPrintSheet)	// find startpos
	//{
	//	while (pos)
	//	{
	//		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetAt(pos);
	//		if (&rPrintSheet == pPrintSheet)
	//			break;
	//		pDoc->m_PrintSheetList.GetNext(pos);
	//	}
	//}

	//while (pos)
	//{
	//	CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
	//	if (rPrintSheet.GetLayout() == pLayout)
	//		return &rPrintSheet;
	//}

	//return NULL;
}

CLayout* CPrintGroup::GetLayout()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if (m_pLayout)
		return m_pLayout;

	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.FindIndex(GetIndex());
	m_pLayout = (pos) ? &pDoc->m_PrintSheetList.m_Layouts.GetAt(pos) : NULL;
	return m_pLayout;
}

BOOL CPrintGroup::HasBoundProducts()
{
	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i].m_nProductClass == CPrintGroupComponent::Bound)
			return TRUE;
	}
	return FALSE;
}

BOOL CPrintGroup::HasFlatProducts()
{
	int nNum = 0;
	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i].m_nProductClass == CPrintGroupComponent::Flat)
			nNum++;
	}
	return (nNum == 0) ? FALSE : nNum;
}

////// drawing /////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

CRect CPrintGroup::DrawHeader(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	  nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;
	HICON hIconPlus	  = theApp.LoadIcon(IDI_PLUS);
	HICON hIconMinus  = theApp.LoadIcon(IDI_MINUS);

	CRect rcIcon = rcFrame; rcIcon.top += 3; rcIcon.right = rcIcon.left + 9; rcIcon.bottom = rcIcon.top + 9;
	pDC->DrawIcon(rcIcon.TopLeft(), (DoShowProductsExpanded()) ? hIconMinus : hIconPlus);

	if (pDisplayList)
	{
		CRect rcDI = rcIcon; rcDI.InflateRect(5,5);
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, ProductGroupExpander), NULL, CDisplayItem::ForceRegister); // register item into display list
		pDI->SetMouseOverFeedback();
	}

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(RGB(40,120,30));

	CString strOut;
	CRect rcText = rcFrame; rcText.left += 10; rcText.bottom = rcText.top + nTextHeight; 
	if ( ! m_strName.IsEmpty())
		strOut += m_strName;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left += pDC->GetTextExtent(strOut).cx + 5;
	CRect rcColors = DrawColorInfo(pDC, rcText, pDisplayList);

	if ( ! m_strPaper.IsEmpty())
	{
		pDC->SetTextColor(DARKGRAY);
		rcText.left = rcColors.right + 5;
		strOut = _T(" |  ") + m_strPaper;
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
	}

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcFrame.left,  rcText.bottom);
	pDC->LineTo(rcFrame.right, rcText.bottom);
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	font.DeleteObject();

	return rcBox;
}

CRect CPrintGroup::DrawColorInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;
	if (m_colorantList.GetSize() <= 0)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(RGB(40,120,30));

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;

	CString strOut;
	pDC->SetTextColor(BLACK);
	strOut.Format(_T("%d"), m_colorantList.GetSize()); 
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx + 5;
	rcBox.UnionRect(rcBox, rcText);

	CRect rcIcon;
	rcIcon.left = rcText.right; rcIcon.top = rcFrame.top + 5; rcIcon.right = rcIcon.left + 6; rcIcon.bottom = rcIcon.top + 5;
	CPen pen(PS_SOLID, 1, MIDDLEGRAY);
	CBrush brush;
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CBrush* pOldBrush = pDC->GetCurrentBrush(); 
	for (int i = 0; i < m_colorantList.GetSize(); i++)
	{
		if (rcIcon.right + 15 > rcFrame.right)
		{
			pDC->SetTextColor(BLACK);
			rcText.left = rcBox.right + 5; rcText.right = rcFrame.right; rcText.bottom = rcIcon.bottom; rcText.top = rcText.bottom - nTextHeight; 
			pDC->DrawText(_T("..."), rcText, DT_LEFT | DT_SINGLELINE | DT_BOTTOM | DT_END_ELLIPSIS);
			break;
		}

		COLORREF crRGB = m_colorantList[i].m_crRGB;
		if (crRGB == WHITE)
			crRGB = pDoc->m_ColorDefinitionTable.FindColorRef(m_colorantList[i].m_strName);
		brush.CreateSolidBrush(crRGB);
		pDC->SelectObject(&brush);
		pDC->Rectangle(rcIcon);
		brush.DeleteObject();

		rcBox.UnionRect(rcBox, rcIcon);
		rcIcon.OffsetRect(7, 0);
	}
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();

	font.DeleteObject();

	return rcBox;
}

CRect CPrintGroup::DrawInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CLayout*	 pLayout	 = GetLayout();
	CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
	BOOL bIsEmpty = (pPrintSheet) ? pPrintSheet->IsEmpty() : TRUE;

	if (pDisplayList)		// we have a display list means, display box has been calculated -> draw it
	{
		//CBrush brush(crBackground);
		//CPen   pen(PS_SOLID, 1, (bIsEmpty) ? LIGHTRED : LIGHTGRAY);
		//CBrush* pOldBrush = pDC->SelectObject(&brush);
		//CPen*	pOldPen	  = pDC->SelectObject(&pen);
		//pDC->RoundRect(CRect(rcFrame.TopLeft(), CSize(GetDisplayBox().Width(), GetDisplayBox().Height())), CPoint(10,10));
		//pDC->SelectObject(pOldBrush);
		//pDC->SelectObject(pOldPen);
	}

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	rcFrame.left += 5; rcFrame.top += 5;

	CString strOut, strPaper, strQuantity, strColors;
	strPaper.LoadString(IDS_PAPER);					strPaper	+= _T(":");
	strQuantity.LoadString(IDS_QUANTITY);			strQuantity += _T(":");
	strColors.LoadString(IDS_PLATECOLOR_STRING);	strColors	+= _T(":");
	int nTab = max(pDC->GetTextExtent(strPaper).cx, pDC->GetTextExtent(strQuantity).cx);
		nTab = max(pDC->GetTextExtent(strColors).cx, nTab) + 5;

	BOOL	bDrawLine = FALSE;
	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	if (m_componentList.GetSize() <= 0)
	{
		pDC->SetTextColor(BLACK);
		CString string; string.LoadString(IDS_NONE);
		pDC->DrawText(string, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
		rcFrame.top += nTextHeight;
		bDrawLine = TRUE;
	}

	if ( ! m_strPaper.IsEmpty())
	{
		pDC->SetTextColor(DARKGRAY);
		pDC->DrawText(strPaper, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		pDC->SetTextColor(BLACK);
		rcText.left += nTab;
		pDC->DrawText(m_strPaper, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
		rcFrame.top += nTextHeight;
		bDrawLine = TRUE;
	}

	if ( (m_nMinQuantity != 0) || (m_nMaxQuantity != 0) )
	{
		rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
		pDC->SetTextColor(DARKGRAY);
		pDC->DrawText(strQuantity, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcText.left += nTab;
		if (m_nMinQuantity == m_nMaxQuantity)
			strOut.Format(_T("%d"), m_nMinQuantity);
		else
			strOut.Format(_T("%d - %d"), m_nMinQuantity, m_nMaxQuantity);
		pDC->SetTextColor(BLACK);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
		rcFrame.top += nTextHeight;
		bDrawLine = TRUE;
	}

	if (m_colorantList.GetSize() > 0)
	{
		rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
		pDC->SetTextColor(DARKGRAY);
		pDC->DrawText(strColors, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcText.left += nTab;
		CRect rcColors = rcFrame; rcColors.left += nTab; rcColors.bottom = rcColors.top + nTextHeight;
		rcColors = DrawColorInfo(pDC, rcColors, pDisplayList);
		rcBox.UnionRect(rcBox, rcColors);
	}

	if (bDrawLine)
	{
		CPen pen(PS_SOLID, 1, LIGHTGRAY);
		CPen* pOldPen = pDC->SelectObject(&pen);
		pDC->MoveTo(rcFrame.left,  rcBox.bottom + 5);
		pDC->LineTo(rcFrame.right, rcBox.bottom + 5);
		pDC->SelectObject(pOldPen);
		pen.DeleteObject();
	}

	rcFrame.top = rcBox.bottom + nTextHeight; 

	pDC->SetTextColor(BLACK);

	CRect rcRow = rcFrame; rcRow.bottom = rcRow.top + 20;
	CRect rcDI;
	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		CPrintGroupComponent& rComponent = m_componentList[i];
		if (rComponent.m_nProductClass != CPrintGroupComponent::Bound)
			continue;

		CBoundProductPart& rProductPart  = pDoc->m_boundProducts.GetProductPart(rComponent.m_nProductClassIndex);
		CBoundProduct&	   rBoundProduct = rProductPart.GetBoundProduct();

		CRect rcImg = rcRow; 
		rcImg = pDoc->m_PageTemplateList.DrawSinglePage(pDC, rcImg, 0, rProductPart.GetIndex(), FALSE, FALSE, TRUE, TRUE, TRUE);
		if (rcImg.top == rcImg.bottom)
		{
			rcImg.top = rcRow.top; rcImg.bottom = rcRow.bottom;
		}

		pDC->SelectObject(&boldFont);
		pDC->SetTextColor(GUICOLOR_CAPTION);
		strOut.Format(_T(" %d "), rComponent.m_nNumBoundPages);
		CRect rcPages = rcImg; rcPages.top = rcImg.CenterPoint().y - 6; rcPages.bottom = rcImg.CenterPoint().y + 8; rcPages.OffsetRect(6, 0); rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
		rcPages.InflateRect(1, 1);
		CBrush brush(WHITE);
		CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
		CBrush* pOldBrush = pDC->SelectObject(&brush);
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		pDC->RoundRect(rcPages, CPoint(8, 8));
		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		pDC->SelectObject(&font);
		pDC->SetBkMode(TRANSPARENT);


		int nNumUnassignedPages = rProductPart.GetNumUnassignedPages();

		pDC->SetTextColor((nNumUnassignedPages <= 0) ? GUICOLOR_CAPTION : MIDDLEGRAY);	
		rcText = rcRow; rcText.left = rcPages.right + 5;
		rcText.left = max(rcText.left, rcRow.left + 35);
		strOut.Format(_T("%s"), rProductPart.m_strName);
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);

		if (pDoc->m_boundProducts.GetSize() > 1)
		{
			pDC->SetTextColor((nNumUnassignedPages <= 0) ? RGB(40,120,30) : MIDDLEGRAY);	// green
			rcText.left += pDC->GetTextExtent(strOut).cx;
			strOut.Format(_T(" | %s"), rBoundProduct.m_strProductName);
			DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		}

		rcDI = rcRow; rcDI.left -= 3; rcDI.top -= 3; rcDI.bottom += 2;
		rcBox.UnionRect(rcBox, rcRow);
		rcRow.top = rcText.bottom + 5; rcRow.bottom = rcRow.top + 20;
		//rcRow.OffsetRect(0, rcImg.Height() + 5);

		//rcRow.OffsetRect(0,5);
	}

	//// draw stack of pages
	//int	  nNum = 0;	
	//int	  nComponentIndex = -1;
	//CRect rcStack = DrawFlatComponentStack(pDC, rcRow, pDisplayList);

	//rcBox.UnionRect(rcBox, rcStack);

	rcRow.bottom = rcRow.top + nTextHeight;

	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		CPrintGroupComponent& rComponent = m_componentList[i];
		if (rComponent.m_nProductClass != CPrintGroupComponent::Flat)
			continue;

		CString strProducts; 
		//if (nNum > 1)
		//{
		//	pDC->SelectObject(&boldFont);
		//	pDC->SetTextColor(GUICOLOR_CAPTION);
		//	strOut.Format(_T(" %d "), nNum);
		//	CRect rcPages = rcStack; rcPages.top = rcStack.CenterPoint().y - 6; rcPages.bottom = rcStack.CenterPoint().y + 8; rcPages.OffsetRect(rcStack.Width() - 2, 0); rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
		//	rcPages.InflateRect(1, 1);
		//	CBrush brush(WHITE);
		//	CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
		//	CBrush* pOldBrush = pDC->SelectObject(&brush);
		//	CPen*	pOldPen	  = pDC->SelectObject(&pen);
		//	pDC->RoundRect(rcPages, CPoint(8, 8));
		//	pDC->SelectObject(pOldBrush);
		//	pDC->SelectObject(pOldPen);
		//	DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		//	pDC->SelectObject(&font);
		//	pDC->SetBkMode(TRANSPARENT);
		//	strProducts.LoadString((nNum == 1) ? IDS_PRODUCT : IDS_PRODUCTS);
		//	pDC->SetTextColor(RGB(40,120,30));	// green
		//	rcText = rcPages; rcText.left = rcPages.right + 5; rcText.right = rcFrame.right;
		//}
		//else
		//{
		//	CFlatProduct* pFlatProduct = GetFirstFlatProduct();
		//	strProducts = (pFlatProduct) ? pFlatProduct->m_strProductName : _T("");
		//	pDC->SetTextColor(GUICOLOR_CAPTION);
		//	rcText = rcStack; rcText.left = rcStack.right + 5; rcText.right = rcFrame.right;
		//}
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(rComponent.m_nFlatProductIndex);
		float	fMaxHeight  = pDoc->m_flatProducts.GetMaxHeight(-1);
		int		nHeight		= (int)((float)(rcRow.Height()) * (rFlatProduct.m_fTrimSizeHeight / fMaxHeight));	
		int		nWidth		= nTextHeight;
		CRect rcImg = CRect(CPoint(rcRow.left, rcRow.top + 3), CSize(nWidth, nHeight));	
		CRect rcPage = pDoc->m_PageTemplateList.DrawFlatProduct(pDC, rcImg, rComponent.m_nFlatProductIndex * 2, FALSE, FALSE, TRUE, TRUE);

		pDC->SetTextColor((rFlatProduct.GetNUpAll() > 0) ? GUICOLOR_CAPTION : MIDDLEGRAY);
		rcText = rcRow;
		rcText.left = max(rcText.left, rcRow.left + 35);
		DrawTextMeasured(pDC, rFlatProduct.m_strProductName, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

		rcDI = rcRow; rcDI.left -= 3; rcDI.top -= 3; rcDI.bottom += 2;
		rcBox.UnionRect(rcBox, rcRow);
		rcRow.OffsetRect(0, rcRow.Height() + 3);

		//if (pDisplayList)
		//{
		//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PrintGroupComponent), (void*)nComponentIndex, CDisplayItem::ForceRegister); // register item into display list
		//	if (pDI)
		//	{
		//		pDI->SetMouseOverFeedback();
		//		pDI->SetNoStateFeedback();
		//		if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
		//		{
		//			CRect rcFeedback = pDI->m_BoundingRect; 
		//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
		//			COLORREF crHighlightColor = RGB(255,195,15);
		//			CGraphicComponent gc;
		//			gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
		//		}
		//	}
		//}
	}

	rcBox.bottom += 10; rcBox.right += 5;

	font.DeleteObject();
	boldFont.DeleteObject();

	//if ( ! pDisplayList)	// no display list means, we are in CalcDoc() -> initialize display box
		SetDisplayBox(rcBox);

	CGraphicComponent gc;
	gc.DrawFrame(pDC, rcBox, (bIsEmpty) ? RGB(255,180,180) : crBackground, TRUE);

	if (pDisplayList)
	{
		rcDI = rcBox;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PrintGroup), (void*)NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
			{
				CRect rcFeedback = pDI->m_BoundingRect; 
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				COLORREF crHighlightColor = RGB(255,195,15);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
			}
		}
	}

	return rcBox;
}

CRect CPrintGroup::DrawFlatComponentStack(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	  nTextHeight  = (pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13;
	int	  nNum		   = 0;
	float fMaxHeight   = pDoc->m_flatProducts.GetMaxHeight(-1);
	CRect rcImg;
	CRect rcStack = rcBox;
	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		CPrintGroupComponent& rComponent = m_componentList[i];
		if (rComponent.m_nProductClass != CPrintGroupComponent::Flat)
			continue;

		if (nNum < 3)
		{
			CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(rComponent.m_nFlatProductIndex);

			int nHeight = (int)((float)(rcFrame.Height()) * (rFlatProduct.m_fTrimSizeHeight / fMaxHeight));	
			int nWidth	= nTextHeight;
			switch (nNum)
			{
			case 0:	rcImg = CRect(CPoint(rcFrame.left,		rcFrame.top + 3),  CSize(nWidth, nHeight));	break;
			case 1:	rcImg = CRect(CPoint(rcFrame.left + 10, rcFrame.top),	   CSize(nWidth, nHeight));	break;
			case 2:	rcImg = CRect(CPoint(rcFrame.left +  5, rcFrame.top + 5),  CSize(nWidth, nHeight));	break;
			}

			CRect rcPage = pDoc->m_PageTemplateList.DrawFlatProduct(pDC, rcImg + CSize(0, 3), rComponent.m_nFlatProductIndex * 2, FALSE, FALSE, TRUE, TRUE);

			CPen pen(PS_SOLID, 1, RGB(220,220,220));
			CPen* pOldPen = pDC->SelectObject(&pen);
			pDC->SelectStockObject(HOLLOW_BRUSH);
			rcPage.InflateRect(1,1);
			rcPage.right++; rcPage.bottom++;
			pDC->Rectangle(rcPage);
			pen.DeleteObject();
			pen.CreatePen(PS_SOLID, 1, DARKGRAY);
			pDC->SelectObject(&pen);
			rcPage.right--; rcPage.bottom--;
			pDC->Rectangle(rcPage);
			pDC->SelectObject(pOldPen);

			rcStack.UnionRect(rcStack, rcPage);

			rcBox.UnionRect(rcBox, rcPage);
		}
		nNum++;
	}

	return rcBox;
}

CRect CPrintGroup::DrawPostpressInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int		 nTextHeight  = (pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13;
	COLORREF crValueColor = RGB(50,50,50);
	CString  strOut, strOut2;
	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_CAPTION);

	CRect rcText = rcFrame;
	pDC->DrawText(_T("Schneiden"), rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	//if (m_nType == Bound)
	//{
	//	rcText.top += nTextHeight;
	//	pDC->DrawText(_T("Falzen"), rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	//	rcText.top += nTextHeight;
	//	pDC->DrawText(_T("Binden"), rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	//}

	font.DeleteObject();

	return rcBox;
}

void CPrintGroup::DrawPlanningTableLinks(CDC* pDC, BOOL bHighlight, CDisplayList* pDisplayList)
{
	CPrintingProfile& rProfile = GetPrintingProfile();
	//if (rProfile.IsNull())
	//	return;
	if (rProfile.GetDisplayBox().IsRectNull())
		return;

	CPen  pen(PS_SOLID, 2, RGB(210,210,235));
	CPen* pOldPen = pDC->SelectObject(&pen);

	CRect rcSource = GetDisplayBox();
	CRect rcTarget = rProfile.GetDisplayBox();

	int nY = min(rcSource.CenterPoint().y, rcTarget.CenterPoint().y);
	pDC->MoveTo(rcSource.right, nY);
	pDC->LineTo(rcTarget.left,  nY);

	//int nMaxTop	 = max(rcSource.top,	  rcTarget.top);
	//int nMinBottom = min(rcSource.bottom, rcTarget.bottom);

	//if (nMaxTop < nMinBottom)
	//if ( (rcSource.CenterPoint().y > rcTarget.top) && (rcSource.CenterPoint().y < rcTarget.bottom) )
	//{
	//	int nY = rcSource.CenterPoint().y;//nMaxTop + (nMinBottom - nMaxTop)/2;
	//	pDC->MoveTo(rcSource.right, nY);
	//	pDC->LineTo(rcTarget.left,  nY);
	//}
	//else
	//{
	//	int nR = 15;
	//	if (rcSource.CenterPoint().y < rcTarget.CenterPoint().y)	// draw downward
	//	{
	//		CPoint pt1(rcSource.right,										rcSource.CenterPoint().y);
	//		CPoint pt2(rcSource.right + (rcTarget.left - rcSource.right)/2, rcSource.CenterPoint().y);
	//		CPoint pt3(pt2.x,		  rcTarget.CenterPoint().y);
	//		CPoint pt4(rcTarget.left, rcTarget.CenterPoint().y);
	//		CPoint cp1 = pt2 + CSize(-nR,  nR);
	//		CPoint cp2 = pt3 + CSize( nR, -nR);
	//		CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
	//		CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

	//		pDC->MoveTo(pt1);
	//		pDC->LineTo(pt2 - CSize(nR, 0));
	//		pDC->AngleArc(cp1.x, cp1.y, nR, 90, -90);
	//		pDC->LineTo(pt3 - CSize(0, nR));
	//		pDC->AngleArc(cp2.x, cp2.y, nR, 180, 90);
	//		pDC->LineTo(pt4);
	//	}
	//}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();
}

void CPrintGroup::SetQuantityRange(int nQuantity)
{
	m_nMinQuantity = min(m_nMinQuantity, nQuantity);
	m_nMaxQuantity = max(m_nMaxQuantity, nQuantity);
}

BOOL CPrintGroup::CheckInsideQuantityRange(CPrintGroup& rPrintGroup, CProductionSetup& rProductionSetup)
{
	if (rProductionSetup.m_quantityRanges.GetSize() <= 0)
	{

		m_nMinQuantity = min(m_nMinQuantity, rPrintGroup.m_nMinQuantity);
		m_nMaxQuantity = max(m_nMaxQuantity, rPrintGroup.m_nMaxQuantity);
		return TRUE;
	}

	int nValue	  = rPrintGroup.m_nMinQuantity; // m_nMinQuantity equals m_nMaxQuantity
	int nRangeLow = 1;
	int nRangeUp  = 1;
	for (int i = 0; i < rProductionSetup.m_quantityRanges.GetSize(); i++)
	{
		if ( (m_nMinQuantity >= rProductionSetup.m_quantityRanges[i].nQuantityMin) && (m_nMaxQuantity <= rProductionSetup.m_quantityRanges[i].nQuantityMax) )
		{
			nRangeLow = rProductionSetup.m_quantityRanges[i].nQuantityMin;
			nRangeUp  = rProductionSetup.m_quantityRanges[i].nQuantityMax;
		}
	}

	if ( (nRangeLow <= nValue) && (nValue <= nRangeUp) )
	{
		m_nMinQuantity = min(m_nMinQuantity, nValue);
		m_nMaxQuantity = max(m_nMaxQuantity, nValue);
		return TRUE;
	}
	else
		return FALSE;
}

BOOL CPrintGroup::CheckInsideColorRange(CPrintGroup& rPrintGroup, CProductionSetup& rProductionSetup)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	if (rPrintGroup.m_componentList.GetSize() <= 0)
		return FALSE;

	CColorantList			colorantList	= m_colorantList;
	CPrintGroupComponent&	rComponent		= rPrintGroup.m_componentList[0];
	if (rComponent.m_nProductClass == CPrintGroupComponent::Flat)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(rComponent.m_nFlatProductIndex);
		colorantList.MixColors(rFlatProduct.m_colorantList);
	}
	else
	{
		CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetPartTotal(rComponent.m_nProductClassIndex);
		colorantList.MixColors(rProductPart.m_colorantList);
	}

	CProductionProfile& rProfile = theApp.m_productionProfiles.FindProfile(rProductionSetup.m_strProductionProfile);
	if ( ! rProfile.IsNull())
	{
		if (colorantList.GetSize() > rProfile.m_press.m_nNumColorStationsFront)
			return FALSE;
		else
			return TRUE;
	}
	else
		return TRUE;
}

CPrintGroupComponentList CPrintGroup::PrepareComponentsToOptimize(float fSheetWidth, float fSheetHeight, CPrintingProfile& rPrintingProfile)
{
	CPrintGroupComponentList componentList;
	CImpManDoc*				 pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return componentList;

	double fSheetArea			= fSheetWidth * fSheetHeight;
	double fTotalComponentsArea = 0.0f;
	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		if (m_componentList[i].m_nProductClass == CPrintGroupComponent::Bound)
		{
			CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_componentList[i].m_nProductClassIndex);
			fTotalComponentsArea += rProductPart.m_fPageWidth * rProductPart.m_fPageHeight;
		}
		else
		{
			CFlatProduct&  rFlatProduct  = pDoc->m_flatProducts.FindByIndex(m_componentList[i].m_nFlatProductIndex);
			fTotalComponentsArea += rFlatProduct.m_fTrimSizeWidth * rFlatProduct.m_fTrimSizeHeight;
		}
		if (fTotalComponentsArea > fSheetArea)
			break;
		
		componentList.Add(m_componentList[i]);
	}

	//qsort((void*)componentList.GetData(), componentList.GetSize(), sizeof(CPrintGroupComponent), CPrintGroupComponent::CompareDesiredQuantity);
	// Sorting is problematic, because quicksort reorders components even if desired quantity is equal for all
	// So it seems to be better not to order, but keep the original order of the products

	return componentList;
}

void CPrintGroup::InitPrintComponentsGroup(CPrintComponentsGroup& rPrintComponents)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		CPrintGroupComponent& rComponent = m_componentList[i];
		if (rComponent.m_nProductClass == CPrintGroupComponent::Flat)
		{
			CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(rComponent.m_nFlatProductIndex);
			rPrintComponents.m_colorantList.MixColors(rFlatProduct.m_colorantList);
			rPrintComponents.m_strPaper = rFlatProduct.m_strPaper;
			rPrintComponents.SetQuantityRange(rFlatProduct.m_nDesiredQuantity);
			rPrintComponents.AddFlatProductComponent(rFlatProduct);
		}
		else
			if (rComponent.m_nProductClass == CPrintGroupComponent::Bound)
			{
				CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(rComponent.m_nProductClassIndex);
				rPrintComponents.m_colorantList.MixColors(rProductPart.m_colorantList);
				rPrintComponents.m_strPaper = rProductPart.m_strPaper;
				rPrintComponents.SetQuantityRange(rProductPart.m_nDesiredQuantity);

				CFoldSheet* pFoldSheet = rProductPart.GetFirstFoldSheet();
				while (pFoldSheet)
				{
					for (int nLayerIndex = 0; nLayerIndex < pFoldSheet->m_LayerList.GetSize(); nLayerIndex++)
					{
						rPrintComponents.AddFoldSheetComponent(rProductPart, pFoldSheet, nLayerIndex);
					}
					pFoldSheet = rProductPart.GetNextFoldSheet(pFoldSheet);
				}

				int nNumUnfoldedPages = rProductPart.GetNumUnfoldedPages();
				if (nNumUnfoldedPages > 0)
					rPrintComponents.AddPageSectionComponent(rProductPart, nNumUnfoldedPages);
			}
	}
}

void CPrintGroup::PreImposeBoundComponents(CPrintComponentsGroup& rPrintComponents)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if ( ! HasBoundProducts())
	{ 
		SetLock(TRUE);
		return;
	}

	CPrintingProfile&	rPrintingProfile = GetPrintingProfile();
	CPrintSheet*		pPrintSheet		 = NULL;
	for (int i = 0; i < m_componentList.GetSize(); i++)
	{
		CPrintGroupComponent& rComponent = m_componentList[i];
		if (rComponent.m_nProductClass != CPrintGroupComponent::Bound)
			continue;
		CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(rComponent.m_nProductClassIndex);
		if (rProductPart.IsNull())
			continue;
		if (rProductPart.m_foldingParams.m_foldSchemeList.GetSize() <= 0)
			continue;

		int nNumUnfoldedPages = rProductPart.GetNumUnfoldedPages();
		while (nNumUnfoldedPages > 0)
		{
			CFoldSheet newFoldSheet = rProductPart.m_foldingParams.FindBestFoldScheme(nNumUnfoldedPages);
			if (newFoldSheet.IsEmpty())
				break;

			newFoldSheet.m_nProductPartIndex = rComponent.m_nProductClassIndex;
			newFoldSheet.SetQuantityPlanned(rProductPart.m_nDesiredQuantity, FALSE);
			CArray <int, int> newFoldSheetIndices;
			pDoc->m_Bookblock.AddFoldSheetsSimple(&newFoldSheet, 1, &newFoldSheetIndices, -1, -1, NULL);
			if (newFoldSheetIndices.GetSize() > 0)
			{
				CFoldSheet* pFoldSheet = pDoc->m_Bookblock.GetFoldSheetByIndex(newFoldSheetIndices[0]);
				for (int nLayerIndex = 0; nLayerIndex < pFoldSheet->m_LayerList.GetSize(); nLayerIndex++)
				{
					rPrintComponents.AddFoldSheetComponent(rProductPart, pFoldSheet, nLayerIndex);
				}
			}

			nNumUnfoldedPages -= newFoldSheet.GetNumPages();
		}

		if (rProductPart.m_foldingParams.m_bShinglingActive)
		{
			rProductPart.m_foldingParams.SetShinglingValueX(rProductPart.GetShinglingValueX(TRUE));
			rProductPart.m_foldingParams.SetShinglingValueXFoot(rProductPart.GetShinglingValueXFoot(TRUE));
			rProductPart.m_foldingParams.SetShinglingValueY(rProductPart.GetShinglingValueY(TRUE));
			pDoc->m_Bookblock.SetShinglingValuesX(rProductPart.m_foldingParams.GetShinglingValueX(rProductPart), FLT_MAX, rComponent.m_nProductClassIndex);
			pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, rProductPart.m_foldingParams.GetShinglingValueXFoot(rProductPart), rComponent.m_nProductClassIndex);
			pDoc->m_Bookblock.SetShinglingValuesY(rProductPart.m_foldingParams.GetShinglingValueY(rProductPart), rComponent.m_nProductClassIndex);
			float fDummy;
			pDoc->m_PageTemplateList.CalcShinglingOffsets(fDummy, fDummy, fDummy, rComponent.m_nProductClassIndex);
		}
	}
	pDoc->m_Bookblock.ReNumberFoldSheets(ALL_PARTS);

	pDoc->m_printGroupList.Analyze();
}


/////////////////////////////////////////////////////////////////////////////
// CPrintGroupComponent

IMPLEMENT_SERIAL(CPrintGroupComponent, CPrintGroupComponent, VERSIONABLE_SCHEMA | 1)

CPrintGroupComponent::CPrintGroupComponent()
{
	m_nPrintGroupIndex		= -1;

	m_nProductClass			= -1;
	m_nProductClassIndex	= -1;
	m_nNumBoundPages		= 0;
	m_nFlatProductIndex		= -1;
}

CPrintGroupComponent::CPrintGroupComponent(const CPrintGroupComponent& rPrintGroupComponent)
{
	Copy(rPrintGroupComponent);
}

const CPrintGroupComponent& CPrintGroupComponent::operator=(const CPrintGroupComponent& rPrintGroupComponent)
{
	Copy(rPrintGroupComponent);

	return *this;
}

void CPrintGroupComponent::Copy(const CPrintGroupComponent& rPrintGroupComponent)
{
	m_nProductClass		 = rPrintGroupComponent.m_nProductClass;
	m_nProductClassIndex = rPrintGroupComponent.m_nProductClassIndex;
	m_nNumBoundPages	 = rPrintGroupComponent.m_nNumBoundPages;
	m_nFlatProductIndex  = rPrintGroupComponent.m_nFlatProductIndex;
}

BOOL CPrintGroupComponent::operator==(const CPrintGroupComponent& rPrintGroupComponent)
{
	if (m_nProductClass			!= rPrintGroupComponent.m_nProductClass)		return FALSE;
	if (m_nProductClassIndex	!= rPrintGroupComponent.m_nProductClassIndex)	return FALSE;
	if (m_nNumBoundPages		!= rPrintGroupComponent.m_nNumBoundPages)		return FALSE;
	if (m_nFlatProductIndex		!= rPrintGroupComponent.m_nFlatProductIndex)	return FALSE;

	return TRUE;
}

BOOL CPrintGroupComponent::operator!=(const CPrintGroupComponent& rPrintGroupComponent)
{
	return !(*this == rPrintGroupComponent);
}

int CPrintGroupComponent::GetIndex()
{
	return GetPrintGroup().m_componentList.GetComponentIndex(this);
}

// helper function for CPrintGroupComponent elements
template <> void AFXAPI SerializeElements <CPrintGroupComponent> (CArchive& ar, CPrintGroupComponent* pPrintGroupComponent, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrintGroupComponent));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPrintGroupComponent->m_nProductClass;
			ar << pPrintGroupComponent->m_nProductClassIndex;
			ar << pPrintGroupComponent->m_nNumBoundPages;
			ar << pPrintGroupComponent->m_nFlatProductIndex;
		}	
		else
		{ 
			switch (nVersion)
			{
				case 1:	ar >> pPrintGroupComponent->m_nProductClass;
						ar >> pPrintGroupComponent->m_nProductClassIndex;
						ar >> pPrintGroupComponent->m_nNumBoundPages;
						ar >> pPrintGroupComponent->m_nFlatProductIndex;
						break;
			}
		}

		pPrintGroupComponent++;
	}
}

CPrintGroup& CPrintGroupComponent::GetPrintGroup()
{
	static private CPrintGroup nullPrintGroup;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullPrintGroup;

	if (m_nPrintGroupIndex >= 0)
		return pDoc->m_printGroupList.GetPrintGroup(m_nPrintGroupIndex);
	else
	{
		for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
		{
			CPrintGroup& rPrintGroup = pDoc->m_printGroupList.GetAt(i);
			for (int j = 0; j < rPrintGroup.m_componentList.GetSize(); j++)
			{
				if (&rPrintGroup.m_componentList[j] == this)
				{
					m_nPrintGroupIndex = i;
					return rPrintGroup;
				}
			}
		}
	}

	return nullPrintGroup;
}

int CPrintGroupComponent::CompareDesiredQuantity(const void *p1, const void *p2)
{
	if ( ! p1 || ! p2)
		return 0;
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nDesiredQuantity1 = 0;
	int nDesiredQuantity2 = 0;

	CPrintGroupComponent* pObject1 = (CPrintGroupComponent*)p1; CPrintGroupComponent* pObject2 = (CPrintGroupComponent*)p2;
	if (pObject1->m_nProductClass == CPrintGroupComponent::Bound)
	{
		CBoundProductPart& rBoundPart = pDoc->m_boundProducts.GetProductPart(pObject1->m_nProductClassIndex);
		nDesiredQuantity1 = rBoundPart.m_nDesiredQuantity;
	}
	else
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(pObject1->m_nFlatProductIndex);
		nDesiredQuantity1 = rFlatProduct.m_nDesiredQuantity;
	}
	if (pObject2->m_nProductClass == CPrintGroupComponent::Bound)
	{
		CBoundProductPart& rBoundPart = pDoc->m_boundProducts.GetProductPart(pObject2->m_nProductClassIndex);
		nDesiredQuantity2 = rBoundPart.m_nDesiredQuantity;
	}
	else
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(pObject2->m_nFlatProductIndex);
		nDesiredQuantity2 = rFlatProduct.m_nDesiredQuantity;
	}

	if (nDesiredQuantity1 < nDesiredQuantity2)
		return 1;
	else
	if (nDesiredQuantity1 > nDesiredQuantity2)
		return -1;
	else
		return 0;
}

CRect CPrintGroupComponent::DrawInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox = CRect(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_BOLD,		FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CString strOut;
	if (m_nProductClass == CPrintGroupComponent::Bound)
	{
		CBoundProductPart& rProductPart  = pDoc->m_boundProducts.GetProductPart(m_nProductClassIndex);
		CBoundProduct&	   rBoundProduct = rProductPart.GetBoundProduct();

		if ( ! rProductPart.GetFirstFoldSheet())
		{
			CRect rcImg = rcFrame; rcImg.left += 3;
			rcImg = pDoc->m_PageTemplateList.DrawSinglePage(pDC, rcImg, 0, rProductPart.GetIndex(), FALSE, FALSE, TRUE, TRUE, TRUE);

			pDC->SelectObject(&font);
			pDC->SetTextColor(GUICOLOR_CAPTION);
			strOut.Format(_T(" %d "), m_nNumBoundPages);
			CRect rcPages = rcImg; rcPages.top = rcImg.CenterPoint().y - 6; rcPages.bottom = rcImg.CenterPoint().y + 8; rcPages.OffsetRect(8, 0); rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
			rcPages.InflateRect(1, 1);
			CBrush brush(WHITE);
			CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			pDC->RoundRect(rcPages, CPoint(8, 8));
			pDC->SelectObject(pOldBrush);
			pDC->SelectObject(pOldPen);
			DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
			pDC->SelectObject(&font);
			pDC->SetBkMode(TRANSPARENT);

			CRect rcText = rcFrame; rcText.left += 40;
			if (pDoc->m_boundProducts.GetSize() > 1)
			{
				pDC->SetTextColor(GUICOLOR_CAPTION);	
				strOut.Format(_T("%s  "), rBoundProduct.m_strProductName);
				DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
				rcText.left += pDC->GetTextExtent(strOut).cx;
			}

			pDC->SetTextColor(RGB(40,120,30));	// green
			strOut.Format(_T("%s "), rProductPart.m_strName);
			DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);

			if (pDisplayList)
			{
				CRect rcDI = rcText; rcDI.top -= 3; rcDI.left = rcFrame.left;
				CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)0, DISPLAY_ITEM_REGISTRY(CPrintComponentsView, PageSection), NULL, CDisplayItem::ForceRegister); // register item into display list
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
					if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
					{
						CRect rcFeedback = pDI->m_BoundingRect; rcFeedback.left++; rcFeedback.top++; rcFeedback.right -=2; rcFeedback.bottom -= 2;
						rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
						CGraphicComponent gc;
						gc.DrawTransparentFrame(pDC, rcFeedback, RGB(255,195,15));
					}
				}
			}
		}
		else
		{
			////// fold sheets
			////////////////////////////////////////////////////////////////////////////////////////////////

			COLORREF crValueColor = RGB(50,50,50);
			CPrintGroup& rPrintGroup = GetPrintGroup();
			CPrintSheet* pPrintSheet = rPrintGroup.GetFirstPrintSheet();
			int nRowHeight = 2*nTextHeight;
			BOOL bFirst = TRUE;
			int nMaxFoldSheetNumWidth = pDoc->m_printGroupList.GetLongestFoldSheetNumString(pDC) + pDC->GetTextExtent(_T("  ")).cx;
			while (pPrintSheet)
			{
				CRect rcRow = rcFrame; rcRow.bottom = rcRow.top + nRowHeight;

				CString strOut;
				pPrintSheet->InitializeFoldSheetTypeList();
				for (int i = 0; i < pPrintSheet->m_FoldSheetTypeList.GetSize(); i++)
				{
					CFoldSheet* pFoldSheet  = pPrintSheet->m_FoldSheetTypeList[i].m_pFoldSheet;
					int			nLayerIndex = pPrintSheet->m_FoldSheetTypeList[i].m_nFoldSheetLayerIndex;
					if ( ! pFoldSheet)
						continue;

					CRect rcThumbnail = rcRow; rcThumbnail.top += 5; rcThumbnail.left += 3; rcThumbnail.right = rcThumbnail.left + 23;
					pFoldSheet->DrawThumbnail(pDC, rcThumbnail, TRUE, FALSE, nLayerIndex);

					//pDC->SelectObject(&boldFont);

					//pDC->SetTextColor(RGB(255, 150, 50));
					//rcText.left += 25;
					//strOut.Format(_T("%s"), pFoldSheet->GetNumber());
					//pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
					//rcBox.bottom = max(rcBox.bottom, rcText.bottom);
			pDC->SelectObject(&font);
			pDC->SetTextColor(GUICOLOR_CAPTION);
			strOut.Format(_T(" %d "), pFoldSheet->GetNumPages());
			CRect rcPages = rcThumbnail; rcPages.top = rcThumbnail.CenterPoint().y - 8; rcPages.bottom = rcThumbnail.CenterPoint().y + 6; rcPages.OffsetRect(15, 0); rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
			rcPages.InflateRect(1, 1);
			CBrush brush(WHITE);
			CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			pDC->RoundRect(rcPages, CPoint(8, 8));
			pDC->SelectObject(pOldBrush);
			pDC->SelectObject(pOldPen);
			DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
			pDC->SelectObject(&font);
			pDC->SetBkMode(TRANSPARENT);

					pDC->SelectObject(&font);

					CRect rcText = rcRow; rcText.left += 40; 
					rcText.top = rcRow.top; rcText.bottom = rcText.top + nTextHeight;
					//rcText.left += 15;

					if (pDoc->m_boundProducts.GetSize() > 1)
					{
						pDC->SetTextColor(GUICOLOR_CAPTION);	
						strOut.Format(_T("%s  "), rBoundProduct.m_strProductName);
						DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
						rcText.left += pDC->GetTextExtent(strOut).cx + 5;
					}
				
					pDC->SetTextColor(RGB(40,120,30));	// green
					strOut.Format(_T("%s "), rProductPart.m_strName);
					DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);

					pDC->SelectObject(&boldFont);

					pDC->SetTextColor(RGB(255, 150, 50));
					rcText.left = rcFrame.left + 40; 
					rcText.OffsetRect(0, nTextHeight);
					//rcText.left += pDC->GetTextExtent(strOut).cx + 5;
					strOut.Format(_T("%s"), pFoldSheet->GetNumber());
					pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
					rcBox.bottom = max(rcBox.bottom, rcText.bottom);

					pDC->SelectObject(&font);

					rcText.left += nMaxFoldSheetNumWidth;
					strOut.Format(_T("%s"), pFoldSheet->m_strFoldSheetTypeName);
					pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
					rcBox.bottom = max(rcBox.bottom, rcText.bottom);
					if (pDisplayList)
					{
						CRect rcDI = rcRow; rcDI.top -= 3; rcDI.left = rcFrame.left;
						CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)0, DISPLAY_ITEM_REGISTRY(CPrintComponentsView, FoldSheet), NULL, CDisplayItem::ForceRegister); // register item into display list
						if (pDI)
						{
							pDI->SetMouseOverFeedback();
							pDI->SetNoStateFeedback();
							if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
							{
								CRect rcFeedback = pDI->m_BoundingRect; rcFeedback.left++; rcFeedback.top++; rcFeedback.right -=2; rcFeedback.bottom -= 2;
								rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
								CGraphicComponent gc;
								gc.DrawTransparentFrame(pDC, rcFeedback, RGB(255,195,15));
							}
						}
					}
				}

				pPrintSheet = rPrintGroup.GetNextPrintSheet(pPrintSheet);
				rcFrame.top += nRowHeight + 5;
			}
		}
	}
	else
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(m_nFlatProductIndex);
		
		float fMaxHeight = pDoc->m_flatProducts.GetMaxHeight(-1);
		int nHeight = (int)((float)(rcFrame.Height()) * (rFlatProduct.m_fTrimSizeHeight / fMaxHeight));	
		CRect rcImg = rcFrame; rcImg.left += 3; rcImg.bottom = rcImg.top + nHeight; rcImg.DeflateRect(0, 2);
		rcImg = pDoc->m_PageTemplateList.DrawFlatProduct(pDC, rcImg, m_nFlatProductIndex * 2, FALSE, FALSE, FALSE, TRUE, TRUE);

		CRect rcText = rcFrame; rcText.left = max(rcImg.right + 10, rcText.left + 40);
		pDC->SetTextColor(GUICOLOR_CAPTION);
		DrawTextMeasured(pDC, rFlatProduct.m_strProductName, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		if (pDisplayList)
		{
			CRect rcDI = rcText; rcDI.top -= 3; rcDI.left = rcFrame.left;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)0, DISPLAY_ITEM_REGISTRY(CPrintComponentsView, FlatProduct), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
				{
					CRect rcFeedback = pDI->m_BoundingRect; rcFeedback.left++; rcFeedback.top++; rcFeedback.right -=2; rcFeedback.bottom -= 2;
					rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
					CGraphicComponent gc;
					gc.DrawTransparentFrame(pDC, rcFeedback, RGB(255,195,15));
				}
			}
		}
	}

	return rcBox;
}

CRect CPrintGroupComponent::DrawQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	return CRect();
}


/////////////////////////////////////////////////////////////////////////////
// CPrintGroupComponentList

CPrintGroupComponentList::CPrintGroupComponentList()
{
}

CPrintGroupComponentList::CPrintGroupComponentList(const CPrintGroupComponentList& rPrintGroupComponentList)
{
	Copy(rPrintGroupComponentList);
}


const CPrintGroupComponentList& CPrintGroupComponentList::operator=(const CPrintGroupComponentList& rPrintGroupComponentList)
{
	Copy(rPrintGroupComponentList);
	return *this;
}

void CPrintGroupComponentList::Copy(const CPrintGroupComponentList& rPrintGroupComponentList)
{
	RemoveAll();
	for (int i = 0; i < rPrintGroupComponentList.GetSize(); i++)
	{
		CPrintGroupComponent printGroupComponent = rPrintGroupComponentList[i];
		Add(printGroupComponent);
	}
}

int CPrintGroupComponentList::GetComponentIndex(CPrintGroupComponent* pComponent)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (pComponent == &GetAt(i))
			return i;
	}
	return -1;
}
