// PrintSheetView.h : header file
//

#pragma once

#include "DlgSelectFoldSchemePopup.h"
#include "MyBitmapButton.h"
#include "DlgTrimming.h"
#include "DlgComponentImposer.h"
#include "DlgAlignObjects.h"



///////////////////////////////////////////////////////////////////////////////////////////////////
// bars for separation of print sheet columns

class CColumnBars
{
public:
	CColumnBars();           

friend class CPrintSheetView;

// Attributes
protected:
	BOOL				 m_bMouseIsOver;
	BOOL				 m_bDragging;
	int					 m_nDragIndex;
	CArray<CRect, CRect> m_rectList, m_rectListOld;

// Operations
protected:
	void OnMouseMove(BOOL bLButton, CPoint& rPoint, class CPrintSheetView* pView);
	void BeginDrag(CPoint& rPoint, class CPrintSheetView* pView);
	void OnDrag	  (CPoint& rPoint, class CPrintSheetView* pView);
	void OnDrop	  (CPoint& rPoint, class CPrintSheetView* pView);
};
///////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
// Settings for trimmings
class CTrimChannel : public CObject
{
public:
	int n_Direction,							// VERTICAL || HORIZONTAL
		n_MoveDirection,						// LEFT || RIGHT || BOTTOM || TOP
		n_Side,									// Mouse-click side: LEFT || RIGHT || BOTTOM || TOP
		n_SightToDraw;							// For determine to draw which Side of BOTHSIDES

	float fTrim1, fTrim2;								
	CRect Rect;									// The selected channel bounding-rectangle
	CRect OldRect;								// The previous selected channel bounding-rectangle
	CRect displayRect;							// The selected channel bounding-rectangle in screen coordinates
	CRect displayOldRect;						// The previous selected channel bounding-rectangle in screen coordinates

	CLayoutObject* pObject;						// Must hold the Objects of the first side in memory to
	CLayoutObject* pObjectNeighbour;			// make the same trimchannel on the other paperside

	CObList* pObjectList1;						// Objects belonging to one channel-side
	CObList* pObjectList2;						// Objects belonging to the other channel-side


public:
	const CTrimChannel& operator=(const CTrimChannel& rTrimChannel);
	int GetLayoutSide();
};

///////////////////////////////////////////////////////////////////////////////////////////////////
// Needed for measuring

class CChannelPartnerVertical : public CObject
{
public:
	CObList* pObjectListLeft;	// Objects belonging to one channel-side
	CObList* pObjectListRight;	// Objects belonging to the other channel-side

	CChannelPartnerVertical()
	{
		pObjectListLeft  = new CObList;
		pObjectListRight = new CObList;
	}
	~CChannelPartnerVertical()
	{
		delete pObjectListLeft;
		delete pObjectListRight;
	}

	CChannelPartnerVertical(const CChannelPartnerVertical& rPartner); // copy constructor
//Operations:
public:
	const CChannelPartnerVertical& operator=(const CChannelPartnerVertical& rPartner);
	void  Copy(const CChannelPartnerVertical& rPartner);
};
//typedef CList <CChannelPartnerVertical, CChannelPartnerVertical> ChannelPartnerVerticalList;
class ChannelPartnerVerticalList : public CList <CChannelPartnerVertical, CChannelPartnerVertical&>{};

///////////////////////////////////////////////////////////////////////////////////////////////////

class CChannelPartnerHorizontal : public CObject
{
public:
	CObList* pObjectListTop;	// Objects belonging to one channel-side
	CObList* pObjectListBottom;	// Objects belonging to the other channel-side

	CChannelPartnerHorizontal()
	{
		pObjectListTop	  = new CObList;
		pObjectListBottom = new CObList;
	}
	~CChannelPartnerHorizontal()
	{
		delete pObjectListTop;
		delete pObjectListBottom;
	}

	CChannelPartnerHorizontal(const CChannelPartnerHorizontal& rPartner); // copy constructor
//Operations:
public:
	const CChannelPartnerHorizontal& operator=(const CChannelPartnerHorizontal& rPartner);
	void  Copy(const CChannelPartnerHorizontal& rPartner);
};
//typedef CList <CChannelPartnerHorizontal, CChannelPartnerHorizontal> ChannelPartnerHorizontalList;
class ChannelPartnerHorizontalList : public CList <CChannelPartnerHorizontal, CChannelPartnerHorizontal&>{};



///////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintSheetView view


const int HINT_UPDATE_WINDOW  = 0;
const int HINT_UPDATE_RECT    = 1;
const int HINT_UPDATE_NULL    = 2;


class CPrintSheetFrame;
class CDlgPDFOutputMediaSettings;
class CDlgMoveObjects;
class CDlgLayoutObjectProperties;
class CDlgMarkSet;
class CDlgLayoutObjectGeometry;


typedef struct 
{
	CPoint ptVpOrg;
	CPoint ptWinOrg;
	CSize  sizeVpExt;
	CSize  sizeWinExt;
}STATUSBAR_DC_STATE;

typedef struct
{
	CRect			m_rcMark;
	BOOL			m_bShow;
	CLayoutObject*	m_pLayoutObject;
	CMark*			m_pMark;
	CPrintSheet*	m_pPrintSheet;
}MARKOBJECTINFO;



class CPrintSheetView : public CScrollView
{
public:
	CPrintSheetView();           // protected constructor used by dynamic creation
	virtual ~CPrintSheetView();

	DECLARE_DYNCREATE(CPrintSheetView)

friend class CColumnBars;
friend class CPrintSheetMarksView;	// give access to m_docSize/m_docOrg
friend class CProdDataPrintView;	// give access to m_docSize/m_docOrg
friend class CDlgMarkSet;			// give access to m_docSize/m_docOrg

// Attributes
public:
	CDlgTrimming	m_dlgTrimming;
	BOOL m_bDrawTrimChannel;	// Should the TrimChannelRect be drawn ?

protected:
	BOOL m_bCheckMarineFrame;
	BOOL m_bCheckCyanFrame;
	BOOL m_bCheckRedFrame;
	BOOL m_bCheckYellowFrame;
	BOOL m_bEnableColorMenues;

public:
	CSpinButtonCtrl								m_reflineXSpin;
	CSpinButtonCtrl								m_reflineYSpin;
	CEdit										m_reflineDistanceEdit;
	CFont										m_reflineDistanceFont;
	CRect										m_reflineUpdateRectFront;
	CRect										m_reflineUpdateRectBack;
	BOOL										m_bReflineUpdateEraseFront;
	BOOL										m_bReflineUpdateEraseBack;
	float*										m_pReflineXValue;
	float*										m_pReflineYValue;
	float										m_fScreenCenterX;
	float										m_fScreenCenterY;
	CBitmap*									m_pBkBitmap;
	CRect										m_rectBk;
	CMemFile									m_undoBuffer;
	CArray <MARKOBJECTINFO, MARKOBJECTINFO&>	m_markObjectInfoList;
	int											m_nActiveYRefLine;


	CTrimChannel m_TrimChannel;			// Settings for the TrimChannel
	BOOL		 m_bPaintStatusChanged;


	DECLARE_INPLACE_EDIT			(InplaceEditPageNum,	 CPrintSheetView);
	DECLARE_INPLACE_EDIT			(InplaceEditBleed,		 CPrintSheetView);
	DECLARE_INPLACE_EDIT			(InplaceEditGlobalMask,	 CPrintSheetView);
	DECLARE_INPLACE_EDIT			(InplaceEditShingling,	 CPrintSheetView);
	DECLARE_INPLACE_BUTTON			(InplaceButtonShingling, CPrintSheetView);
	DECLARE_INPLACE_EDIT			(InplaceEditSideName,	 CPrintSheetView);

	DECLARE_GROUPED_DISPLAY_LIST	(CPrintSheetView, OnMultiSelect, YES, OnStatusChanged, YES)

	DECLARE_SNAP_DISPLAY_ITEM(CPrintSheetView, LayoutObject,	 	OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, YES, OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, MarkObjectInfo,	 	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, FALSE)
	DECLARE_SNAP_DISPLAY_ITEM(CPrintSheetView, Sheet,			 	OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, FALSE)
	DECLARE_SNAP_DISPLAY_ITEM(CPrintSheetView, Plate,			 	OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, FALSE)
	DECLARE_SNAP_DISPLAY_ITEM(CPrintSheetView, OutputMedia,		 	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, RefEdgePrintSheet,	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_SNAP_DISPLAY_ITEM(CPrintSheetView, FoldSheet,		 	OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, YES, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, PlateTab,		 	OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_SNAP_DISPLAY_ITEM(CPrintSheetView, Exposure,		 	OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, YES, OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, GlobalMaskTag,	 	OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, ShinglingValueX,	 	OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, ShinglingValueY,	 	OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_SNAP_DISPLAY_ITEM(CPrintSheetView, Mark,			 	OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, ReflineXDistance, 	OnSelect, YES, OnDeselect, YES, OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, ReflineYDistance, 	OnSelect, YES, OnDeselect, YES, OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, FreeArea,		 	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO,  IsSelectable, FALSE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, TrackerResizeHandle, OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, Toolbar,			 	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO,  IsSelectable, FALSE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, LayoutSideName,	 	OnSelect, YES, OnDeselect, YES, OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, WebYRefLine1,	 	OnSelect, YES, OnDeselect, YES, OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, WebYRefLine2,	 	OnSelect, YES, OnDeselect, YES, OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM	 (CPrintSheetView, GroupTracker,	 	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, YES, IsSelectable, FALSE)
	DECLARE_SNAP_DISPLAY_ITEM(CPrintSheetView, BoundingBoxFrame,	OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO,  IsSelectable, FALSE)

	CMyBitmapButton		m_gotoDetailsButton;

	COleDropTarget		m_dropTarget;
	CColumnBars			m_ColumnBars;

protected:
	CRect m_LeftStatusBar, m_UpperStatusBar;
	BOOL  m_bUpperStatusBarUpdated;
	BOOL  m_bLockUpdateStatusBars;
	
// Operations
public:
	CImpManDoc* GetDocument();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintSheetView)
	public:
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnInitialUpdate();
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual void OnDragLeave();
	virtual DROPEFFECT OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

///////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation
public:
	CSize		 m_docSize;
	CPoint		 m_docOrg;
protected:
	CSize		 m_docBorderTopLeft,  m_docBorderBottomRight;
	CSize		 m_viewportSize;
	int			 m_nToolBarHeight;
	int			 m_viewportCellWidth;
	int			 m_nViewportCellRows, m_nViewportCellCols;
	CSize		 m_sizeTotal, m_sizePage, m_sizeLine;
	CPoint		 m_lastHandPos;
	float		 m_fScreenPrinterRatioX;	// Needed for Printing
	float		 m_fScreenPrinterRatioY;	// Needed for Printing
	int			 m_nPrinterNumColors;	// Needed for Printing
	int			 m_nSheetsPerPage;		// Needed for Printing
	CRect		 m_rcPrintMargins;
	CToolTipCtrl m_tooltip;
	UINT		 m_nFrameColorIndex;	// Which color for the LayoutObject Rectangle

public:
	CList <CZoomState, CZoomState&> m_zoomSteps;	

COwnerDrawnButtonList m_printViewToolbar1Buttons, m_printViewToolbar2Buttons, m_printViewToolbar3Buttons, m_zoomToolBar;


public:
	CDisplayItem* ChangeTopmostPlate(CDisplayItem* pDI, BOOL bSelectTreeItem = TRUE);
	static long	WorldToLP(float fValue) 
				{	switch (theApp.m_dwPlatformId)
					{
					case VER_PLATFORM_WIN32_NT:		 return (long)((float)(fValue * 100.0f + 0.5f));
					case VER_PLATFORM_WIN32_WINDOWS: return (long)((float)(fValue * 10.0f  + 0.5f));
					default:						 return (long)((float)(fValue		   + 0.5f));
					};
				}
	static long		   FRONT_BACK_GAP(BOOL bShowMeasuring)		// gap between front and back side
						{ 
							return WorldToLP((bShowMeasuring) ? ((GetSystemMetrics(SM_CXSCREEN) <= 1152) ? 450.0f : 350.0f) : 75.0f); 
						};	
	inline long		   STATUSBAR_THICKNESS(int nOrientation, BOOL bScreen = FALSE)
						{
							if ( (ViewMode() == ID_VIEW_SINGLE_SHEETS) || (ViewMode() == ID_ZOOM) )
							{
								if (bScreen) return ( (nOrientation == HORIZONTAL) ? 16L : 75L);//48L );
								return (nOrientation == HORIZONTAL) ? ((long)( 16.0f * m_fScreenPrinterRatioX)) : ((long)( 75.0f * m_fScreenPrinterRatioY));
							}
							else
							{
								if (bScreen) return ( (nOrientation == HORIZONTAL) ? 16L : 75L );
								return (nOrientation == HORIZONTAL) ? ((long)( 16.0f * m_fScreenPrinterRatioX)) : ((long)( 75.0f * m_fScreenPrinterRatioY));
							}
						};
	inline long		   RULER_THICKNESS(BOOL bScreen = FALSE)
						{
							if ( (ViewMode() == ID_VIEW_SINGLE_SHEETS) || (ViewMode() == ID_ZOOM) )
							{
								if (bScreen) return 16L;
								return (long)16.0f;
							}
							else
							{
								if (bScreen) return 0L;
								return (long)0.0f;
							}
						};
	inline long		   VIEWPORT_BORDER(BOOL bShowMeasuring, int nOrientation, BOOL bScreen = FALSE)
						{
							if (bScreen) 
							{ 
								if (bShowMeasuring) return 55L;	// room for measuring	
								return 30L;
							}
							return (nOrientation == HORIZONTAL) ? ((long)( 30.0f * m_fScreenPrinterRatioX)) : ((long)( 30.0f * m_fScreenPrinterRatioY));
						};
	inline long		   VIEWPORT_CELLBORDER(int nOrientation, BOOL bScreen = FALSE)
						{
							if (bScreen) return 5L;
							return (nOrientation == HORIZONTAL) ? ((long)( 5.0f * m_fScreenPrinterRatioX)) : ((long)( 5.0f * m_fScreenPrinterRatioY));
						};
	#define			   INITIAL_CELLWIDTH_SCREEN 300L
	STATUSBAR_DC_STATE PrepareStatusBarDC(CDC* pDC);
	void			   RestoreStatusBarDC(CDC* pDC, STATUSBAR_DC_STATE& rOldDCState);

	static void		UpdateToolbarBackground(CDC* pDC, CRect rcRect, CWnd* /*pWnd*/);

	CPrintSheet*	GetActPrintSheet();
	CLayout*		GetActLayout();
	CPressDevice*	GetActPressDevice(int nSide/*= 0*/);
	int				GetActSight();
	CPlate*			GetActPlate();
	
	void			Draw(CDC* pDC);

	void		 	InvalidateItemsOnOtherSheets(CLayoutObject* pObject, CPrintSheet* pSheet);
	void		 	UpdatePageID(CDisplayItem* pDI, CString strPageNum);
	void		 	UpdateBleed();
	void		 	UpdateGlobalMask();
	void		 	UpdateShinglingValue();
	void		 	CopyBleedToEnabledPlates(float fValue, int nHitPosition, CExposure* pExposure);
	void		 	DrawPrintSheetAllSidesToScreen(CDC* pDC);
	void		 	DrawPrintSheetAllSidesToPrinter(CDC* pDC, UINT nCurPage);
	void		 	OnGotoDetails();
	void		 	DrawPrintSheet(CDC* pDC, UINT nActLayoutSide, CLayout* pActLayout, CPrintSheet* pActPrintSheet, CFrameWnd* pParentFrame, BOOL bDrawTitle = TRUE);
	void		 	InitStatusBars(CDC* pDC);
	void		 	ClipStatusBars(CDC* pDC);
	void		 	ReInitLeftStatusBar(CDC* pDC);
	void		 	UpdateLeftStatusBar(CDC* pDC, CRect sheetRect, CPrintSheet* pActPrintSheet, CLayoutSide* pLayoutSide);
	void		 	UpdateUpperStatusBar(CDC* pDC, CRect sheetRect, CPrintSheet* pActPrintSheet, CLayoutSide* pLayoutSide, BOOL bDrawTitle);
	void		 	LockUpdateStatusBars  () { m_bLockUpdateStatusBars = TRUE;  };
	void		 	UnlockUpdateStatusBars() { m_bLockUpdateStatusBars = FALSE; };
	void		 	CalcScrollSizes(); 
	void		 	CalcDocExtents();
	void		 	CalcViewportExtents(BOOL bReset = FALSE);
	void		 	DoTrimmings			(CPoint point);
	void		 	DoAlignSection			(CPoint point);
	void		 	AlignSectionComplete	(CLayoutObject* pObject, int nSide);
	void		 	AlignSectionYRefLine	(CLayoutObject* pObject, int nSide, int nWhatRefLine);
	void		 	DoPositionMarks		(CPoint point);
	void		 	DoPositionOutputMedia	(CPoint point);
	void		 	SetAlignLayoutObjects	(CPoint point);
	void		 	DoPositionLayoutObjects(CPoint point);
	static BOOL  	GetReflineDIFilterMarks(CDisplayItem& rDI);
	CDisplayItem 	GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder, class CDlgMarkSet*				pDlg = NULL, BOOL bClicked = FALSE, CMark* pMark = NULL);
	CDisplayItem 	GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder, class CDlgPDFTargetProps*	    pDlg = NULL, BOOL bClicked = FALSE);
	CDisplayItem 	GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder,	class CDlgAlignObjects*		    pDlg = NULL, BOOL bClicked = FALSE);
	CDisplayItem 	GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder, class CDlgLayoutObjectGeometry* pDlg = NULL, BOOL bClicked = FALSE);
	CDisplayItem 	GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder,												 BOOL bClicked = FALSE);
	void			CheckCenterHandle(CRect rcRect, int& nSide, CPoint point);
	BOOL		 	InsideAnyFoldSheet(CPoint& point);
	unsigned int 	WhichQuadrant(CRect rect, CPoint point);
	void		 	SaveBitmap	  (CDC* pDC, CBitmap* pBitmap, const CRect& rect);
	void		 	RestoreBitmap(CDC* pDC, CBitmap* pBitmap, const CRect& rect, int nAlpha = 255);
	void		 	MoveBitmap(CDC* pDC, CBitmap* pbmBackground, void (*pfnDrawFeedback)(CDC*, CRect), CRect rcBackground, CRect rcForeground);
	void		 	MoveBitmap(CDC* pDC, CBitmap* pbmBackground, CBitmap* pbmForeground, CRect rcBackground, CRect rcForeground, int nAlpha = 255, void (*pfnDrawFeedback)(CDC*, CRect) = NULL, CRect rcFeedbackArea = CRect(0,0,0,0));
	void		 	ZoomWindow(const CPoint& rPoint, BOOL bRButton = FALSE);
	void		 	SaveBackground	  (CDC* pDC, const CRect& rect);
	void		 	RestoreBackground(CDC* pDC, const CRect& rect);
	void		 	DrawResizeTracker(CDC* pDC, CRect rcFrame, COLORREF crColor = RED, CDisplayItem* pActDI = NULL);
	void		 	OnMouseMoveControlMarks(CPoint point);
	void		 	OnMouseMoveOutputMedia (CPoint point);
	void		 	OnMouseMoveAlignSection(CPoint point);
	void		 	OnMouseMoveLayoutObject(CPoint point);
	void		 	GetPrinterMargins(CDC* pDC);	
	void		 	UndoSave();
	void		 	UndoRestore();


///////////////////////////////////////////////////////////////////////////////////////////////////
//Implementationfile PrintSheetViewDraw.cpp
public:
	static void			FillTransparent	(CDC* pDC, const CRect& rRect, COLORREF cr, BOOL bTrueTransparency = FALSE, int nAlpha = 80, BOOL bFrame = FALSE);
	static void			DrawRectangleExt(CDC* pDC, CRect rect);
	static void			FillRectangleExt(CDC* pDC, CRect rect, COLORREF crFill);
	static void			DrawHeadPosition(CDC* pDC, CRect& rPageRect, BYTE HeadPosition, COLORREF crColor);
	static void			DrawPreviewNotFoundMessage(CDC* pDC, CRect outRect, const CString& strPreviewFile);
	static void			DrawArrow (CDC* pDC, int nDirection, CPoint hotSpot, int nLen, COLORREF crOutline, COLORREF crFill, BOOL bSlim = TRUE, CPoint ptVPOrg = CPoint(0,0), int nEdgeLen = 3);
	static void			TruncateNumber(CString& string, float fValue, BOOL bShowUnit = FALSE);
	static void			ResolvePlaceholders   (TCHAR *pszDest, const TCHAR *pszSrc, CPrintSheet* pActPrintSheet, int nSide, CPlate* pPlate, int nTile, CString strFoldSheetNum, int nFoldSheetNumCAG, int nLayerNum, CFoldSheet* pFoldSheet = NULL);
	static void			ResolvePlaceholdersExt(TCHAR *pszDest, const TCHAR *pszSrc, CPrintSheet* pActPrintSheet, CPlate* pPlate, int nTile, CString strColorName = "", CReflinePositionParams* pReflinePosParams = NULL);
	static CString		CreateAsirCode(CFoldSheet* pFoldSheet, BOOL bOrigNums = FALSE);
	static int			GetColorNumber(CPlate* pPlate);
	static CString		ConvertToString(COleDateTime& rDate, BOOL bLongVersion = TRUE, BOOL bYearWithCentury = FALSE);
	static CFoldSheet*	GetMarkFoldSheetAndLayerNum(CPrintSheet* pPrintSheet, int nComponentRefIndex, int *nFoldSheetNumCAG, int *nLayerNum);
	static CFoldSheet*  GetFoldSheetFromPlaceholder(TCHAR **ppStart, TCHAR **ppEnd, CPrintSheet* pPrintSheet, int *nComponentRefIndex);
	static CString		TruncatePlaceholder(TCHAR **ppStart, TCHAR **ppEnd, CString strValue);
	static CString		FieldWidthPlaceholder(TCHAR **ppStart, TCHAR **ppEnd, int nValue);
	static void			ReadSheettextFromFile(int nPrintSheetNumber, TCHAR* szText);
	static void			DrawSystemCreatedContentPreview
									 (CDC* pDC, CPageTemplate* pObjectTemplate, CPageSource* pObjectSource, CRect outBBox, CLayoutObject* pObject, int nHeadPosition, const CString& strSystemCreationInfo, CPrintSheet* pPrintSheet);
	static void	 DrawEdge			 (CDC* pDC, CRect* rect, int ntype, CLayout* pActLayout, CPrintSheet* pActPrintSheet, int nActFoldSheetRefIndex, int nTypeOfEdge, int nSide = FRONTSIDE, CDisplayList* displayList = NULL, BOOL bShowFoldSheet = FALSE); // PaperRefEdge and FoldSheetRefEdge
	void		 DrawShinglingOffsets(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet); 
	static void	 DrawPageShingling	 (CDC* pDC, CPageTemplate* pPageTemplate, CRect pageRect, int nHeadPosition, int nPosition, CDisplayList* pDisplayList = NULL);
	static void	 DrawPageShinglingX	 (CDC* pDC, CPageTemplate* pPageTemplate, CRect pageRect, int nHeadPosition, int nPosition, CDisplayList* pDisplayList = NULL);
	static void	 DrawPageShinglingY	 (CDC* pDC, CPageTemplate* pPageTemplate, CRect pageRect, int nHeadPosition,				CDisplayList* pDisplayList = NULL);
	static CRect CalcBleedValueRect	 (CExposure* pExposure, const CRect& rRect, int nSide, CString& valueString, CDisplayList* pDisplayList, CDC* pDC = NULL);
	static CRect CalcBleedValueRect	 (float fValue, const CRect& rRect, int nSide, CString& valueString, CDisplayList* pDisplayList, CDC* pDC = NULL);
	static void	 DrawMaskArrow		 (CDC* pDC, const CRect& exposureRect, const CRect& pageRect, int nDirection);

	void		DrawPlatePreviews		(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, BYTE nType, CLayoutObject* pLayoutObject = NULL, CString strMarkName = _T(""));
	BOOL		DrawPreview				(CDC* pDC, CRect outBBox, CRect clipRect, CLayoutObject* pLayoutObject, CPageTemplate* pTemplate, const CString& strPreviewFile);
	void		DrawColorants			(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);
	void		DrawPlate				(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, CLayout* pActLayout, BOOL bDrawTitle); 
	void		DrawPlateTab			(CDC* pDC, CPrintSheet* pActPrintSheet, CPlate& rPlate, CPlate* pTopmostPlate, CRect& rTabRect, BOOL bTopmost);
	void		DrawSpotColorTab		(CDC* pDC, CPrintSheet* pActPrintSheet, CString strSpotColor, CRect& rTabRect);
	void		DrawGlobalMaskTag		(CDC* pDC, CPlate* pPlate, CPoint tagPos, CPrintSheet* pActPrintSheet);
	void		DrawExposures			(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);
	CRect		CalcBleedValueRect		(CExposure* pExposure, const CRect& rRect, const CPoint& clickPoint, int& nSide, CString& valueString);
	void		DrawMedia				(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);
	void		DrawMediaTile			(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, BOOL bShowContent, BOOL bShowMeasuring, BOOL bShowHighlighted);
	void		DrawGripper				(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);
	void		DrawCutBlocks			(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);
	CSize		GetDrawingOffset		(UINT nActLayoutSide, CLayout* pActLayout); //Offset between Plate 0,0 and Sheet 0,0
	void		DrawSheet				(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, BOOL bDrawTitle); 
	void		DrawSheetFrame			(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, BOOL bDrawTitle);
	void		DrawRefEdge				(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, CLayout* pActLayout); 
	void		DrawBoundingBoxFrame	(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);
	void		DrawObjects				(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, BYTE nType, BOOL bDrawSelected = FALSE, CString strMarkName = _T("")); 
	COLORREF	DrawPage				(CDC* pDC, CRect& rPageRect, CLayoutObject& rObject, CPageTemplate* pObjectTemplate, CPrintSheet* pActPrintSheet = NULL, BOOL bDrawSelected = FALSE, COLORREF* pcrBackground = NULL);
	void		DrawPanoramaPage		(CDC* pDC, CRect& rPageRect, CLayoutObject& rObject, CLayoutObject& rBuddy, CPrintSheet& rPrintSheet);
	BOOL		DrawMark				(CDC* pDC, CRect& rMarkRect, CLayoutObject& rObject, CPrintSheet* pActPrintSheet, BOOL bDoHighlightMark);
	void		DrawMarkInfoFields		(CDC* pDC, CPrintSheet* pActPrintSheet);
	void		DrawGraphics			(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);
	void		DrawArc					(CDC* pDC, CGraphicPathArc& gpArc, COLORREF crColor);
	void		DrawFanoutOffsets		(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet); 
	CRect		CalcPageIDRect			(CRect pageRect, CString strPageID, int nHeadPosition, int nIDPosition, 
										 int* pnIDCorner = NULL, CRect* pEditRect = NULL);
 	void		DrawFoldSheet			(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);
 	void		DrawFoldSheetRect		(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, int nActFoldSheetRefIndex, CFoldSheet* pFoldSheet);
	void		DrawFoldSheetSectionBar	(CDC* pDC, CPrintSheet* pActPrintSheet, int nActFoldSheetRefIndex, int nLayoutSide);
	void		DrawTrimChannelRect		(CDC* pDC, UINT nActLayoutSide);
	void		DrawSectionLinesForWebPress	
										(CDC* pDC, UINT nActLayoutSide, CLayout* pActLayout);
	void		DrawMoveObjectReflines	(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);


///////////////////////////////////////////////////////////////////////////////////////////////////
// Implementationfile PrintSheetViewDragDrop.cpp
	DROPEFFECT	OnDragOverGroup			(CDisplayItem& di, DWORD dwKeyState, CPoint point);
	DROPEFFECT	OnDragOverPrintComponent(CDisplayItem& di, DWORD dwKeyState, CPoint point);
	DROPEFFECT	OnDragOverOutputMedia	(CDisplayItem& di, DWORD dwKeyState, CPoint point);
	DROPEFFECT	OnDragOverMark			(CDisplayItem& di, DWORD dwKeyState, CPoint point);
	BOOL		OnDropGroup		   		(CDisplayItem& di, DROPEFFECT dropEffect, CPoint point);
	BOOL		OnDropPrintComponent	(CDisplayItem& di, DROPEFFECT dropEffect, CPoint point);
	BOOL		OnDropOutputMedia		(CDisplayItem& di, DROPEFFECT dropEffect, CPoint point);
	BOOL		OnDropMark				(CDisplayItem& di, DROPEFFECT dropEffect, CPoint point);
	CLayout*	MoveCopyFlatLayoutObject(CDisplayItem& di, DROPEFFECT dropEffect, float fXOffset, float fYOffset);
	CLayout*	MoveCopyFoldSheet		(CDisplayItem& di, DROPEFFECT dropEffect, float fXOffset, float fYOffset);
	CLayout*	AddNewFoldSheet			(CFoldSheet*   pFoldSheet, int nLayerIndex, float fXOffset, float fYOffset);
	CLayout*	AddNewFlatProduct		(CFlatProduct* pFlatProduct,				float fXOffset, float fYOffset);

///////////////////////////////////////////////////////////////////////////////////////////////////
//Implementationfile PrintSheetViewTrimming.cpp
public:
	BOOL					CalculateAndShowTrimChannel(CTrimChannel *pTrimChannel, CLayoutObject* pObject, int n_Side, CPoint point, CPrintSheet* pPrintSheet = NULL);
	static BOOL				CalculateCounterpartTrimChannel(CTrimChannel *pTrimChannel, CLayoutObject* pObject, CLayoutObject* pObjectNeighbour, int n_Side);
	static void				SetTrimChannelMovingDirections(CTrimChannel *pTrimChannel, int n_Side);
	static CLayoutObject*	FindOppositeObject(CLayoutObject* pObject, int n_Side, CPoint point);
	static BOOL				FindTrimChannel(CTrimChannel *pTrimChannel, CLayoutObject* pObject, CLayoutObject* pObjectNeighbour, int n_Side);
	static BOOL				FindChannelPartner(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide);
	static BOOL				FindUltimateChannelPartner(CTrimChannel *pTrimChannel, CObList *pObjectList1, CObList *pObjectList2, int nDirection);
	static BOOL				CalculateTrimChannelRect(CTrimChannel *pTrimChannel, CPrintSheet* pPrintSheet = NULL, CDisplayList* pDisplayList = NULL);
	static BOOL				SearchOnUpperLine(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide);
	static BOOL				SearchOnLowerLine(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide);
	static BOOL				SearchOnRightLine(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide);
	static BOOL				SearchOnLeftLine(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide);
	static BOOL				Neighbours(CObjectNeighbours* pNeighbourList, CLayoutObject* pObject);
	static BOOL				DetermineChannelWidth(CTrimChannel *pTrimChannel, float* pfTrim1 = NULL, float* pfTrim2 = NULL);
	static BOOL				MakeNewChannel(CTrimChannel *pTrimChannel, float fDistance1, float fDistance2);
	static void				MoveObjectRecur(CLayoutObject* pObject, float fMovingDistance, int n_Side);
	static void				MoveObjectInside(CLayoutObject* pObject, float fMovingDistance, int nDirection);

///////////////////////////////////////////////////////////////////////////////////////////////////
//Implementationfile PrintSheetViewMeasure.cpp
public:
	static void DrawMeasure(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet);
	static void MeasureChannel(CDC* pDC, CTrimChannel *pTrimChannel, CLayoutObject* pObject, 
						ChannelPartnerVerticalList *pChannelObjectsVertical, int *nChannelsVertical, 
						ChannelPartnerHorizontalList *pChannelObjectsHorizontal, int *nChannelsHorizontal, CObList* pStringRects);
	static BOOL FindInChannelObjectsVertical(CLayoutObject* pObjectLeft, CLayoutObject* pObjectRight, ChannelPartnerVerticalList *pChannelObjectsVertical, int nChannelnumber);
	static BOOL FindInChannelObjectsHorizontal(CLayoutObject* pObjectBottom, CLayoutObject* pObjectTop, ChannelPartnerHorizontalList *pChannelObjectsHorizontal, int nChannelnumber);
	static void MeasureChannelVertical(CDC* pDC, CTrimChannel *pTrimChannel, CLayoutObject *pObject1, CLayoutObject *pObject2, ChannelPartnerVerticalList *pChannelObjects, int Direction, int *nChannelnumber, CObList* pStringRects);
	static void MeasureChannelHorizontal(CDC* pDC, CTrimChannel *pTrimChannel, CLayoutObject* pObject1, CLayoutObject* pObject2, ChannelPartnerHorizontalList *pChannelObjects, int Direction, int *nChannelnumber, CObList* pStringRects);
	static void DrawSingleMeasure(CDC* pDC, CTrimChannel *pTrimChannel, int *nChannelnumber, CObList* pStringRects);
	static void DetermineMeasureTextRect(CTrimChannel *pTrimChannel, CLayoutObject* pObject, RECT *rect, CString string, int pos);
	static BOOL RectOverlapped(RECT *pRect1, RECT *pRect2);
	static BOOL MeasurementsVisibleInView(CDC* pDC, CLayout* pActLayout, UINT nActLayoutSide);
	static void	DrawSheetMeasuring(CDC* pDC, UINT nActLayoutSide, CLayout* pActLayout, BOOL bShowPlate);
	static void	DrawPlateMeasuring(CDC* pDC, UINT nActLayoutSide, CLayout* pActLayout);
	static void	DrawHMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1 = CPoint(INT_MAX, INT_MAX), CPoint ptRef2 = CPoint(INT_MAX, INT_MAX), BOOL bChain = FALSE);
	static void	DrawVMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1 = CPoint(INT_MAX, INT_MAX), CPoint ptRef2 = CPoint(INT_MAX, INT_MAX), BOOL bChain = FALSE);

	CDlgPDFOutputMediaSettings* GetDlgOutputMedia();
	CDlgMoveObjects*			GetDlgMoveObjects();
	CDlgLayoutObjectProperties*	GetDlgNewMark();
	CDlgMarkSet*				GetDlgMarkSet();
	CDlgLayoutObjectGeometry*	GetDlgLayoutObjectGeometry();
	CDlgLayoutObjectProperties*	GetDlgLayoutObjectProperties();
	CDlgSelectFoldSchemePopup*	GetDlgFoldSchemeSelector();
	CDlgComponentImposer*		GetDlgComponentImposer();
	CDlgAlignObjects*			GetDlgAlignObjects();
	BOOL						IsOpenDlgLayoutObjectGeometry();
	BOOL						IsOpenDlgLayoutObjectProperties();
	BOOL						IsOpenDlgNewMarkGeometry();
	BOOL						IsOpenMarkSetDlg();
	BOOL						IsOpenDlgNewMark();
	BOOL						IsOpenDlgMaskSettings();
	BOOL						IsOpenDlgShinglingAssistent();
	BOOL						IsOpenDlgFoldSchemeSelector();
	BOOL						IsOpenDlgComponentImposer();
	BOOL						IsOpenDlgAlignObjects();
	BOOL						IsOpenDlgPDFObjectContent();
	BOOL						IsOpenDlgObjectContent();
	BOOL						ShowMeasure();
	BOOL						ShowPlate();
	BOOL						ShowBitmap();
	BOOL						ShowExposures();
	BOOL						ShowColorants();
	BOOL						ShowFoldSheet();
	BOOL						ShowCutBlocks();
	BOOL						ShowBoxShapes();
	BOOL						ShowGripper();
	BOOL						AlignSectionActive();
	BOOL						MarksViewActive();
	BOOL						OutputViewActive();
	BOOL						HandActive();
	BOOL						TrimToolActive();
	int							ViewMode();
	void						SetViewMode(int nViewMode);
	void						SetLayoutObjectSelected(BOOL bSelected);
	void						SetFoldSheetRotateActive(BOOL bActive);
	void						SetFoldSheetTurnActive(BOOL bActive);
	void						SetFoldSheetTumbleActive(BOOL bActive);
	void						SetFoldSheetSelected(BOOL bSelected);
	void						SetEnableReleaseBleedButton(BOOL bEnable);
	void						SetUndoActive(BOOL bActive);
	CColorantList				GetActColorants();

friend class CPreviewView;

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CPrintSheetView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMarineFrame();
	afx_msg void OnUpdateMarineFrame(CCmdUI* pCmdUI);
	afx_msg void OnCyanFrame();
	afx_msg void OnUpdateCyanFrame(CCmdUI* pCmdUI);
	afx_msg void OnRedFrame();
	afx_msg void OnUpdateRedFrame(CCmdUI* pCmdUI);
	afx_msg void OnYellowFrame();
	afx_msg void OnUpdateYellowFrame(CCmdUI* pCmdUI);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();

protected:
	afx_msg void OnDeltaposReflineXSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposReflineYSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusReflineDistanceEdit();
	afx_msg void OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp );
	afx_msg void OnNcPaint();
	afx_msg void OnUpdatePrintSheetViewStrippingData(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePrintSheetViewMarks(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAddNewPrintSheet(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemovePrintSheet(CCmdUI* pCmdUI);
	afx_msg void OnUpdateModifyShingling(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewAllsides(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewFrontside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBackside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBothsides(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFoldsheetCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMeasureCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlateCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBitmapCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateExposureCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowCutBlocks(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowBoxshapes(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOutputMaskSettings(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOutputColorSettings(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUndo(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoom(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomDecrease(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomFullview(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScrollHand(CCmdUI* pCmdUI);
};
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _DEBUG  // debug version in PrintSheetView.cpp
inline CImpManDoc* CPrintSheetView::GetDocument()
   { return (CImpManDoc*)m_pDocument; }
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////

