// DlgTrimming.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgTrimming.h"
#include "PrintSheetNavigationView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgTrimming dialog


CDlgTrimming::CDlgTrimming(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgTrimming::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgTrimming)
	m_fTrimWidth = 0.0f;
	m_fTrim1 = 0.0f;
	m_fTrim2 = 0.0f;
	//}}AFX_DATA_INIT
	m_bLock = FALSE;
}

CDlgTrimming::~CDlgTrimming()
{
	DestroyWindow();
}


void CDlgTrimming::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgTrimming)
	DDX_Measure(pDX, IDC_TRIM_WIDTH, m_fTrimWidth);
	DDX_Measure(pDX, IDC_TRIM1, m_fTrim1);
	DDX_Measure(pDX, IDC_TRIM2, m_fTrim2);
	DDX_Control(pDX, IDC_TRIM_WIDTH_SPIN, m_trimWidthSpin);
	DDX_Control(pDX, IDC_TRIM1_SPIN, m_trim1Spin);
	DDX_Control(pDX, IDC_TRIM2_SPIN, m_trim2Spin);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgTrimming, CDialog)
	//{{AFX_MSG_MAP(CDlgTrimming)
	ON_NOTIFY(UDN_DELTAPOS, IDC_TRIM_WIDTH_SPIN, OnDeltaposTrimWidthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_TRIM1_SPIN, OnDeltaposTrim1Spin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_TRIM2_SPIN, OnDeltaposTrim2Spin)
	ON_EN_UPDATE(IDC_TRIM_WIDTH, OnUpdateTrimWidth)
	ON_EN_UPDATE(IDC_TRIM1, OnUpdateTrim1)
	ON_EN_UPDATE(IDC_TRIM2, OnUpdateTrim2)
	ON_COMMAND(ID_TRIM1, OnTrim1)
	ON_COMMAND(ID_TRIM2, OnTrim2)
	ON_COMMAND(ID_TRIM3, OnTrim3)
	ON_COMMAND(ID_TRIM4, OnTrim4)
	ON_COMMAND(ID_TRIM5, OnTrim5)
	ON_COMMAND(ID_TRIM6, OnTrim6)
	ON_COMMAND(ID_TRIM7, OnTrim7)
	ON_COMMAND(ID_TRIM8, OnTrim8)
	ON_UPDATE_COMMAND_UI(ID_TRIM1, OnUpdateUITrim1)
	ON_UPDATE_COMMAND_UI(ID_TRIM2, OnUpdateUITrim2)
	ON_UPDATE_COMMAND_UI(ID_TRIM3, OnUpdateUITrim3)
	ON_UPDATE_COMMAND_UI(ID_TRIM4, OnUpdateUITrim4)
	ON_UPDATE_COMMAND_UI(ID_TRIM5, OnUpdateUITrim5)
	ON_UPDATE_COMMAND_UI(ID_TRIM6, OnUpdateUITrim6)
	ON_UPDATE_COMMAND_UI(ID_TRIM7, OnUpdateUITrim7)
	ON_UPDATE_COMMAND_UI(ID_TRIM8, OnUpdateUITrim8)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgTrimming message handlers

BOOL CDlgTrimming::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_trimWidthSpin.SetRange(0, 1);  
	m_trim1Spin.SetRange(0, 1);  
	m_trim2Spin.SetRange(0, 1);  

	InitData();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		((CPrintSheetView *)pView)->m_TrimChannel.fTrim1 = 0.0f;
		((CPrintSheetView *)pView)->m_TrimChannel.fTrim2 = 0.0f;
	}

	m_fTrimWidth = m_fTrim1 = m_fTrim2 = 0.0f;

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgTrimming::InitData()
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CRect winRect, divRect;
		GetWindowRect(winRect);
		GetDlgItem(IDC_TRIM2)->GetWindowRect(divRect);
		winRect.right = divRect.right + 20;
		//MoveWindow(winRect);

		GetDlgItem(IDC_TRIM1_TEXT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TRIM2_TEXT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TRIM1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TRIM2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TRIM_EQUAL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TRIM_PLUS)->ShowWindow(SW_SHOW);

		CString strText1, strText2;
		if (((CPrintSheetView *)pView)->m_TrimChannel.n_Direction == HORIZONTAL)
		{
			strText1.LoadString(IDS_TOP);	 strText1 += _T(":");
			strText2.LoadString(IDS_BOTTOM); strText2 += _T(":");
		}
		else
		{
			strText1.LoadString(IDS_LEFT);	strText1 += _T(":");
			strText2.LoadString(IDS_RIGHT); strText2 += _T(":");
		}
		GetDlgItem(IDC_TRIM1_TEXT)->SetWindowText(strText1);
		GetDlgItem(IDC_TRIM2_TEXT)->SetWindowText(strText2);

		m_fOldTrim1 = m_fTrim1	 = ((CPrintSheetView *)pView)->m_TrimChannel.fTrim1;
		m_fOldTrim2 = m_fTrim2	 = ((CPrintSheetView *)pView)->m_TrimChannel.fTrim2;
		m_fTrimWidth = m_fTrim1 + m_fTrim2;

		CEdit* pEdit = (CEdit*)GetDlgItem(IDC_TRIM_WIDTH);
		pEdit->SetSel(0, -1);

		CRect rcButton; 
		if ( ! m_trimToolbarButtons.m_imgHot.IsNull())
			m_trimToolbarButtons.m_imgHot.Destroy();
		if ( ! m_trimToolbarButtons.m_imgCold.IsNull())
			m_trimToolbarButtons.m_imgCold.Destroy();
		m_trimToolbarButtons.RemoveAll();
		if (pView->m_TrimChannel.n_Direction == HORIZONTAL)
		{
			m_trimToolbarButtons.Create(this, this, NULL, IDB_HORIZONTAL_TRIMTOOLBAR, IDB_HORIZONTAL_TRIMTOOLBAR, CSize(32, 24));
			GetDlgItem(IDC_TRIMTOOLBAR_FRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton);	rcButton.right = rcButton.CenterPoint().x;
			m_trimToolbarButtons.AddButton(-1, ID_TRIM1,	ID_TRIM1,	rcButton, 0);	rcButton.OffsetRect(rcButton.Width(), 0);
			m_trimToolbarButtons.AddButton(-1, ID_TRIM2,	ID_TRIM2,	rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);
			if (pView->m_TrimChannel.pObjectNeighbour)
			{
				m_trimToolbarButtons.AddButton(-1, ID_TRIM3,	ID_TRIM3,	rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);
				m_trimToolbarButtons.AddButton(-1, ID_TRIM4,	ID_TRIM4,	rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);
				m_trimToolbarButtons.AddButton(-1, ID_TRIM5,	ID_TRIM5,	rcButton, 4);	rcButton.OffsetRect(rcButton.Width(), 0);
			}
		}
		else
		{
			m_trimToolbarButtons.Create(this, this, NULL, IDB_VERTICAL_TRIMTOOLBAR, IDB_VERTICAL_TRIMTOOLBAR, CSize(32, 24));
			GetDlgItem(IDC_TRIMTOOLBAR_FRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton);	rcButton.right = rcButton.CenterPoint().x;
			m_trimToolbarButtons.AddButton(-1, ID_TRIM1,	ID_TRIM1,	rcButton, 0);	rcButton.OffsetRect(rcButton.Width(), 0);
			m_trimToolbarButtons.AddButton(-1, ID_TRIM2,	ID_TRIM2,	rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);
			if (pView->m_TrimChannel.pObjectNeighbour)
			{
				m_trimToolbarButtons.AddButton(-1, ID_TRIM6,	ID_TRIM6,	rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);
				m_trimToolbarButtons.AddButton(-1, ID_TRIM7,	ID_TRIM7,	rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);
				m_trimToolbarButtons.AddButton(-1, ID_TRIM8,	ID_TRIM8,	rcButton, 4);	rcButton.OffsetRect(rcButton.Width(), 0);
			}
		}
	}

	UpdateData(FALSE);      
}

void CDlgTrimming::OnDeltaposTrimWidthSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_trimWidthSpin.GetBuddy(), pNMUpDown->iDelta);
	OnOK();
}

void CDlgTrimming::OnDeltaposTrim1Spin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_trim1Spin.GetBuddy(), pNMUpDown->iDelta);
	OnOK();
}

void CDlgTrimming::OnDeltaposTrim2Spin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_trim2Spin.GetBuddy(), pNMUpDown->iDelta);
	OnOK();
}

void CDlgTrimming::OnUpdateTrimWidth() 
{
	if (m_bLock)
		return;

	UpdateData(TRUE);

	m_fTrim1 = m_fTrim2 = m_fTrimWidth/2.0f;

	m_bLock = TRUE;
	SetMeasure(GetDlgItem(IDC_TRIM1)->m_hWnd, m_fTrim1);
	SetMeasure(GetDlgItem(IDC_TRIM2)->m_hWnd, m_fTrim2);
	m_bLock = FALSE;
}

void CDlgTrimming::OnUpdateTrim1() 
{
	if (m_bLock)
		return;

	UpdateData(TRUE);

	m_fTrimWidth = m_fTrim1 + m_fTrim2;

	m_bLock = TRUE;
	SetMeasure(GetDlgItem(IDC_TRIM_WIDTH)->m_hWnd, m_fTrimWidth);
	m_bLock = FALSE;
}

void CDlgTrimming::OnUpdateTrim2() 
{
	if (m_bLock)
		return;

	UpdateData(TRUE);

	m_fTrimWidth = m_fTrim1 + m_fTrim2;

	m_bLock = TRUE;
	SetMeasure(GetDlgItem(IDC_TRIM_WIDTH)->m_hWnd, m_fTrimWidth);
	m_bLock = FALSE;
}

void CDlgTrimming::OnTrim1() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bChannelThrough = TRUE;
	pDoc->m_TrimOptions.m_bChannelSingle  = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	pView->m_bDrawTrimChannel = FALSE;
    pView->m_TrimChannel.OldRect = CRect(0, 0, 0, 0);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgTrimming::OnTrim2() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bChannelThrough = FALSE;
	pDoc->m_TrimOptions.m_bChannelSingle  = TRUE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	pView->m_bDrawTrimChannel = FALSE;
    pView->m_TrimChannel.OldRect = CRect(0, 0, 0, 0);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgTrimming::OnTrim3() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bHoriUp		  = TRUE;
	pDoc->m_TrimOptions.m_bHoriLow		  = FALSE;
	pDoc->m_TrimOptions.m_bHoriSym		  = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetView::SetTrimChannelMovingDirections(&pView->m_TrimChannel, TOP);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgTrimming::OnTrim4() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bHoriUp		  = FALSE;
	pDoc->m_TrimOptions.m_bHoriLow		  = TRUE;
	pDoc->m_TrimOptions.m_bHoriSym		  = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetView::SetTrimChannelMovingDirections(&pView->m_TrimChannel, TOP);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgTrimming::OnTrim5() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bHoriUp		  = FALSE;
	pDoc->m_TrimOptions.m_bHoriLow		  = FALSE;
	pDoc->m_TrimOptions.m_bHoriSym		  = TRUE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetView::SetTrimChannelMovingDirections(&pView->m_TrimChannel, TOP);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgTrimming::OnTrim6() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bVertLeft		  = TRUE;
	pDoc->m_TrimOptions.m_bVertRight	  = FALSE;
	pDoc->m_TrimOptions.m_bVertSym		  = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetView::SetTrimChannelMovingDirections(&pView->m_TrimChannel, LEFT);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgTrimming::OnTrim7() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bVertLeft		  = FALSE;
	pDoc->m_TrimOptions.m_bVertRight	  = TRUE;
	pDoc->m_TrimOptions.m_bVertSym		  = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetView::SetTrimChannelMovingDirections(&pView->m_TrimChannel, LEFT);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgTrimming::OnTrim8() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bVertLeft		  = FALSE;
	pDoc->m_TrimOptions.m_bVertRight	  = FALSE;
	pDoc->m_TrimOptions.m_bVertSym		  = TRUE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetView::SetTrimChannelMovingDirections(&pView->m_TrimChannel, LEFT);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgTrimming::OnUpdateUITrim1(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	COwnerDrawnButtonList::SetCheck(pCmdUI, pDoc->m_TrimOptions.m_bChannelThrough);
}

void CDlgTrimming::OnUpdateUITrim2(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	COwnerDrawnButtonList::SetCheck(pCmdUI, pDoc->m_TrimOptions.m_bChannelSingle);
}

void CDlgTrimming::OnUpdateUITrim3(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	COwnerDrawnButtonList::SetCheck(pCmdUI, pDoc->m_TrimOptions.m_bHoriUp);
}

void CDlgTrimming::OnUpdateUITrim4(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	COwnerDrawnButtonList::SetCheck(pCmdUI, pDoc->m_TrimOptions.m_bHoriLow);
}

void CDlgTrimming::OnUpdateUITrim5(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	COwnerDrawnButtonList::SetCheck(pCmdUI, pDoc->m_TrimOptions.m_bHoriSym);
}

void CDlgTrimming::OnUpdateUITrim6(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	COwnerDrawnButtonList::SetCheck(pCmdUI, pDoc->m_TrimOptions.m_bVertLeft);
}

void CDlgTrimming::OnUpdateUITrim7(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	COwnerDrawnButtonList::SetCheck(pCmdUI, pDoc->m_TrimOptions.m_bVertRight);
}

void CDlgTrimming::OnUpdateUITrim8(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	COwnerDrawnButtonList::SetCheck(pCmdUI, pDoc->m_TrimOptions.m_bVertSym);
}

BOOL CDlgTrimming::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_trimToolbarButtons.m_pToolTip)            
		m_trimToolbarButtons.m_pToolTip->RelayEvent(pMsg);	

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgTrimming::OnOK() 
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CLayoutObject* pCounterpartObject		   = NULL;
		CLayoutObject* pCounterpartObjectNeighbour = NULL;

		CTrimChannel& rTrimChannel = pView->m_TrimChannel;
		if ( ! rTrimChannel.pObjectList1 && ! rTrimChannel.pObjectList2)
			return;

		rTrimChannel.fTrim1 = m_fTrim1;
		rTrimChannel.fTrim2 = m_fTrim2;

		if ( (fabs(rTrimChannel.fTrim1 - m_fOldTrim1) > 0.009) || (fabs(rTrimChannel.fTrim2 - m_fOldTrim2) > 0.009) )
		{
			if (theApp.settings.m_bFrontRearParallel)
			{
				pCounterpartObject			= rTrimChannel.pObject->FindCounterpartObject();
				pCounterpartObjectNeighbour = rTrimChannel.pObjectNeighbour->FindCounterpartObject();
			}

			CPrintSheetView::MakeNewChannel(&rTrimChannel, rTrimChannel.fTrim1 - m_fOldTrim1, rTrimChannel.fTrim2 - m_fOldTrim2);

			if (theApp.settings.m_bFrontRearParallel)
			{
				CTrimChannel counterTrimChannel; 
        		counterTrimChannel.pObjectList1 = new CObList;
        		counterTrimChannel.pObjectList2 = new CObList;
				counterTrimChannel = rTrimChannel;
				if (CPrintSheetView::CalculateCounterpartTrimChannel(&counterTrimChannel, pCounterpartObject, pCounterpartObjectNeighbour, counterTrimChannel.n_Side))
				{
					int nKindOfProduction = (counterTrimChannel.pObject->GetLayout()) ? counterTrimChannel.pObject->GetLayout()->KindOfProduction() : -1;
					if (counterTrimChannel.n_Direction == VERTICAL)
						if (nKindOfProduction == WORK_AND_TURN)
						{
							SwapValues(counterTrimChannel.fTrim1, counterTrimChannel.fTrim2); SwapValues(m_fOldTrim1, m_fOldTrim2);
						}
					if (counterTrimChannel.n_Direction == HORIZONTAL)
						if (nKindOfProduction == WORK_AND_TUMBLE)
						{
							SwapValues(counterTrimChannel.fTrim1, counterTrimChannel.fTrim2); SwapValues(m_fOldTrim1, m_fOldTrim2);
						}
					CPrintSheetView::MakeNewChannel(&counterTrimChannel, counterTrimChannel.fTrim1 - m_fOldTrim1, counterTrimChannel.fTrim2 - m_fOldTrim2);
				}
				delete counterTrimChannel.pObjectList1;
				delete counterTrimChannel.pObjectList2;
			}


			rTrimChannel.pObject->GetLayout()->RearrangePaper(TRUE, TRUE);	// don't change if paper size fits / prompt user
			rTrimChannel.pObject->GetLayout()->CheckForProductType();
			rTrimChannel.pObject->GetLayout()->AnalyzeMarks();

			pDoc->m_PageSourceList.InitializePageTemplateRefs();
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();	// because panorama pages could be arised or destroyed
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
			pDoc->SetModifiedFlag(TRUE, NULL, FALSE);	// automarks should not be changed

			InitData();
		}

		pView->Invalidate();
		pView->CalculateTrimChannelRect(&rTrimChannel);
		pView->DetermineChannelWidth(&rTrimChannel);

		CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
		if (pNavigationView)
			pNavigationView->UpdatePrintSheets(pView->GetActLayout());
	}
}

void CDlgTrimming::OnCancel()
{
	DestroyWindow();

	m_trimToolbarButtons.RemoveAll();
	m_trimToolbarButtons.m_imgHot.Destroy();
	m_trimToolbarButtons.m_imgCold.Destroy();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	pView->m_DisplayList.Invalidate();
	pView->m_bDrawTrimChannel = FALSE;
    pView->m_TrimChannel.OldRect = CRect(0, 0, 0, 0);

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}
