// PrintSheetPlanningView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetPlanningView.h"
#include "ProdDataGraphicOverView.h"
#include "GraphicComponent.h"
#include "DlgJobData.h"
#include "DlgPressDevice.h"
#include "NewPrintSheetFrame.h"
#include "MainFrm.h" 
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "PageListFrame.h"
#include "PageListView.h"
#include "BookBlockDataView.h"
#include "MyRectTracker.h"
#include "DlgOptimizationMonitor.h"
#include "DlgProductionManager.h"
#include "CFFOutputImposition.h"
#include "DlgMarkSet.h"
#include "DlgSelectPrintLayout.h" 
#include "DlgProductionSetups.h"



HCURSOR g_CursorSepStandard, g_CursorSepNS;


CSeparatorBars::CSeparatorBars()
{
	m_bMouseIsOver = FALSE;
	m_bDragging	   = FALSE;

	g_CursorSepNS	    = AfxGetApp()->LoadStandardCursor(IDC_SIZENS);
	g_CursorSepStandard = AfxGetApp()->LoadStandardCursor(IDC_ARROW);
}

void CSeparatorBars::OnMouseMove(BOOL bLButton, CPoint& rPoint, class CPrintSheetPlanningView* pView)
{
	if (bLButton && m_bDragging)
	{
		OnDrag(rPoint, pView);
		return;
	}

	if (m_bDragging)
		return;

	m_bMouseIsOver = FALSE;
	m_nDragIndex   = -1;
	for (int i = 0; i < m_rectList.GetSize(); i++)
		if (m_rectList[i].PtInRect(rPoint))
		{
			m_nDragIndex = i;
			break;
		}
	if (m_nDragIndex < 0)	// first separator is not allowed to be dragged
		m_bMouseIsOver = FALSE;
	else
		m_bMouseIsOver = TRUE;

	if (m_bMouseIsOver)
	{
		if (GetCursor() == g_CursorSepStandard)
			SetCursor(g_CursorSepNS);
	}
	else
	{
		if (GetCursor() == g_CursorSepNS)
			SetCursor(g_CursorSepStandard);
	}
}

void CSeparatorBars::BeginDrag(CPoint& /*rPoint*/, CPrintSheetPlanningView* pView)
{
	m_bDragging	= TRUE;

	m_rectListOld.RemoveAll();
	CClientDC dc(pView);

	for (int i = 0; i < m_rectList.GetSize(); i++)
	{
		CRect dragRect  = m_rectList[i];
		int	  nY	    = dragRect.CenterPoint().y;
		dragRect.top    = nY - 2;
		dragRect.bottom = nY + 2;
		dc.DrawDragRect(&dragRect, CSize(2,2), NULL, CSize(0,0));

		m_rectListOld.Add(m_rectList[i]);
	}
}

void CSeparatorBars::OnDrag(CPoint& rPoint, CPrintSheetPlanningView* pView)
{
	int nDiff = (rPoint.y / (m_nDragIndex + 1));
	if (nDiff < 50)
		return;
	for (int i = 0; i < m_rectList.GetSize(); i++)
	{
		m_rectList[i].top    = nDiff * (i + 1);
		m_rectList[i].bottom = nDiff * (i + 1);
		if (m_rectList[i] != m_rectListOld[i])
		{
			CClientDC dc(pView);
			CRect dragRect	   = m_rectList[i];
			int	  nY		   = dragRect.CenterPoint().y;
			dragRect.top       = nY - 2;
			dragRect.bottom    = nY + 2;
			CRect dragRectOld  = m_rectListOld[i];
			nY				   = dragRectOld.CenterPoint().y;
			dragRectOld.top    = nY - 2;
			dragRectOld.bottom = nY + 2;
			dc.DrawDragRect(&dragRect, CSize(2,2), &dragRectOld, CSize(2,2));
			m_rectListOld[i] = m_rectList[i];
		}
	}
}

void CSeparatorBars::OnDrop(CPoint& rPoint, CPrintSheetPlanningView* pView)
{
	m_bMouseIsOver = FALSE;
	m_bDragging	   = FALSE;

	int nDiff = (rPoint.y / (m_nDragIndex + 1));
	if (nDiff < 50)
		nDiff = 50;
	nDiff -= 35;	// ???
//	nDiff	 -= 3 * pView->VIEWPORT_CELLBORDER(HORIZONTAL, TRUE);
	pView->m_nPrintSheetRowHeight = nDiff;

	m_rectListOld.RemoveAll();
	m_rectList.RemoveAll();

	pView->m_DisplayList.Invalidate();

	pView->Invalidate();
	pView->OnUpdate(NULL, -1, NULL);
	pView->UpdateWindow();
	
	OnMouseMove(FALSE, rPoint, pView);
}


// CPrintSheetPlanningView
#define ID_STANDARDLAYOUT_LOAD1 12345
#define ID_CFF_STORE 12346
#define ID_PRODUCTGROUP_RULES 12347

#define ID_PLANNINGLIST_HEADER 11111
#define ID_ZOOM_FULLVIEW_Y ID_ZOOM_FULLVIEW + 1

IMPLEMENT_DYNCREATE(CPrintSheetPlanningView, CScrollView)

CPrintSheetPlanningView::CPrintSheetPlanningView()
{
	m_nShowPlanningView = ShowImposition;
	m_ptDragIcon = CPoint(0, 0);
	m_hDragIcon	 = theApp.LoadIcon(IDI_DRAG_FOLDSHEET);
	
	m_hArrowIcon = theApp.LoadIcon(IDI_INSERT_ARROW);
	m_pBkBitmap = NULL;
	m_redPen.CreatePen(PS_SOLID, 1, RED);

	m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CPrintSheetPlanningView)); // Ole Drag/Drop
	m_docSize = CSize(0,0);
	m_nLastSelectedComponentIndex = -1;
	m_nPrintSheetRowHeight = 200;
	m_nHeaderItemsTotalWidth = 0;
	m_nHeaderHeight = 0;
	m_nCurrentHeaderType = m_nShowPlanningView;
	m_nToolBarHeight = 34;

	m_boxFrameColor = RGB(210,210,235);

	m_HeaderFont.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
} 

CPrintSheetPlanningView::~CPrintSheetPlanningView()
{
	m_HeaderFont.DeleteObject();
}

void CPrintSheetPlanningView::ResetDisplayColums()
{
	m_displayColumns.RemoveAll();
}

void CPrintSheetPlanningView::AddDisplayColumn(int nColID, int nWidth)
{
	struct displayColumn dc = {nColID, nWidth};
	m_displayColumns.Add(dc);
}

COrderDataFrame* CPrintSheetPlanningView::GetOrderDataFrame()
{
	return (COrderDataFrame*)GetParentFrame();
}

BEGIN_MESSAGE_MAP(CPrintSheetPlanningView, CScrollView)
    ON_WM_ERASEBKGND()
	ON_WM_CREATE()
    ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONUP()
    ON_WM_RBUTTONDOWN()
    ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_COMMAND(ID_MAGNIFY_SHEET, OnMagnifySheet)
	ON_COMMAND(ID_GOTO_DETAILS, OnGotoDetails)
	ON_WM_NCMOUSEMOVE()
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	ON_WM_MOUSEWHEEL()
	ON_WM_SIZE()
	ON_WM_NCCALCSIZE()
	ON_NOTIFY(HDN_TRACK,	ID_PLANNINGLIST_HEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK, ID_PLANNINGLIST_HEADER, OnHeaderEndTrack)
	ON_COMMAND(ID_PRODUCTGROUP_RULES, OnProductGroupRules)
	ON_COMMAND(ID_DO_IMPOSE, OnDoImpose)
	ON_COMMAND(ID_DO_IMPOSE_REMOVE_ALL, OnDoImposeRemoveAll)
	ON_UPDATE_COMMAND_UI(ID_PRODUCTGROUP_RULES, OnUpdateProductGroupRules)
	ON_UPDATE_COMMAND_UI(ID_DO_IMPOSE, OnUpdateDoImpose)
	ON_UPDATE_COMMAND_UI(ID_DO_IMPOSE_REMOVE_ALL, OnUpdateDoImposeRemoveAll)
	ON_COMMAND(ID_DELETE_SELECTION, OnDeleteSelection)
	ON_COMMAND(ID_UNDO, OnUndo)
	ON_COMMAND(ID_STANDARDLAYOUT_LOAD1, OnLoadStandard)
	ON_COMMAND(ID_STANDARDLAYOUT_STORE, OnStoreStandard)
	ON_COMMAND(ID_CFF_STORE, OnStoreCFF)
	ON_COMMAND(ID_DO_NUMBER, OnDoNumber)
	ON_UPDATE_COMMAND_UI(ID_DELETE_SELECTION, OnUpdateDeleteSelection)
	ON_UPDATE_COMMAND_UI(ID_UNDO, OnUpdateUndo)
	ON_UPDATE_COMMAND_UI(ID_STANDARDLAYOUT_LOAD1, OnUpdateLoadStandard)
	ON_UPDATE_COMMAND_UI(ID_STANDARDLAYOUT_STORE, OnUpdateStoreStandard)
	ON_UPDATE_COMMAND_UI(ID_CFF_STORE, OnUpdateStoreCFF)
	ON_UPDATE_COMMAND_UI(ID_DO_NUMBER, OnUpdateDoNumber)
END_MESSAGE_MAP()



BOOL CPrintSheetPlanningView::OnEraseBkgnd(CDC* pDC) 
{
	m_planningToolBar.ClipButtons(pDC);
	m_strippingToolBar.ClipButtons(pDC);
	m_printSheetListToolBar.ClipButtons(pDC);
	m_foldSheetListToolBar.ClipButtons(pDC);
	m_pagesListToolBar.ClipButtons(pDC);

	CRect rect;
    GetClientRect(rect);
	rect.top = m_nToolBarHeight;

	COLORREF crBackGround = (m_nShowPlanningView == ShowImposition) ? WHITE  : TABLECOLOR_BACKGROUND;
	CBrush brush(crBackGround);
    pDC->FillRect(&rect, &brush);

	CRect rcHeader = rect;
	rcHeader.top = 0; rcHeader.bottom = m_nToolBarHeight;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CButton* pButton = m_planningToolBar.GetButtonByCommand(ID_PRODUCTGROUP_RULES);
		if (pButton)
		{
			CRect rcWin;
			pButton->GetWindowRect(rcWin);
			rcWin.OffsetRect(rcWin.Width(), 0); ScreenToClient(rcWin);
			rcWin.right = rcWin.left + 120;
			gp.DrawTitleBar(pDC, rcWin, pDoc->m_productionSetup.m_strName, GUICOLOR_CAPTION, -1);
		}
	}

	rcHeader.top = rcHeader.bottom - 3;
	pDC->FillSolidRect(rcHeader, RGB(215,220,225));//RGB(190,202,210));

    brush.DeleteObject();

	return TRUE;

//    return CScrollView::OnEraseBkgnd(pDC);
}


// CPrintSheetPlanningView-Zeichnung

void CPrintSheetPlanningView::OnInitialUpdate()
{
	CFrameWnd* pFrame = GetParentFrame();
	if (pFrame)
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		{
			if ( ((CNewPrintSheetFrame*)pFrame)->m_browserInfo.m_nShowInfo & CNewPrintSheetFrame::ShowPrintSheetList)
			{
				if (((CNewPrintSheetFrame*)pFrame)->m_wndSplitter1.GetPane(3, 0) == this)
					m_nShowPlanningView = ShowPrintSheetList;
			}
			else
				if ( ((CNewPrintSheetFrame*)pFrame)->m_browserInfo.m_nShowInfo & CNewPrintSheetFrame::ShowProducts)
				{
					if (((CNewPrintSheetFrame*)pFrame)->m_wndSplitter.GetPane(0, 1) == this)
						m_nShowPlanningView = ShowImposition;
				}
		}

	CRect rcButton; 
	m_planningToolBar.Create(this, GetParentFrame(), NULL, IDB_PRINTSHEET_PLANNING_TOOLBAR, IDB_PRINTSHEET_PLANNING_TOOLBAR_COLD, CSize(25, 26), &DrawToolsBackground);
	rcButton = CRect(CPoint(30,0), CSize(25,30));
	m_planningToolBar.AddButton(-1, IDS_PRODUCTGROUP_RULES,		ID_PRODUCTGROUP_RULES,		rcButton, 11);	rcButton.OffsetRect(rcButton.Width(), 0);	rcButton.OffsetRect(rcButton.Width()/2, 0); rcButton.OffsetRect(120, 0); // room for production setup name
	m_planningToolBar.AddButton(-1, IDS_IMPOSE,					ID_DO_IMPOSE,				rcButton,  0);	rcButton.OffsetRect(rcButton.Width(), 0);	
	m_planningToolBar.AddButton(-1, IDS_IMPOSE_REMOVE_ALL,		ID_DO_IMPOSE_REMOVE_ALL,	rcButton, 12);	rcButton.OffsetRect(rcButton.Width(), 0);	

	rcButton = CRect(CPoint(220,0), CSize(30,30));

	//m_strippingToolBar.Create(this, GetParentFrame(), NULL, IDB_PRINTVIEW_MODIFY_TOOLBAR, IDB_PRINTVIEW_MODIFY_TOOLBAR_COLD, CSize(30, 24), &DrawToolsBackground);
	//rcButton = CRect(CPoint(400,0), CSize(25,30));
	//m_strippingToolBar.AddButton(-1, IDS_OBJECTS_EXCHANGE,	ID_SWAP_OBJECTS,				rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);
	//m_strippingToolBar.AddButton(-1, IDS_TURN,				ID_OBJECTS_TURN,				rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);
	//m_strippingToolBar.AddButton(-1, IDS_TUMBLE,			ID_OBJECTS_TUMBLE,				rcButton, 4);	rcButton.OffsetRect(rcButton.Width(), 0);
	//m_strippingToolBar.AddButton(-1, IDS_ROTATE_RIGHT,		ID_OBJECTSROTATE_CLOCK,			rcButton, 5);	rcButton.OffsetRect(rcButton.Width(), 0);
	//m_strippingToolBar.AddButton(-1, IDS_ROTATE_LEFT,		ID_OBJECTSROTATE_COUNTERCLOCK,	rcButton, 6);	rcButton.OffsetRect(rcButton.Width(), 0);
	//m_strippingToolBar.AddButton(-1, IDS_DELETE,			ID_DELETE_SELECTION,			rcButton, 8);	rcButton.OffsetRect(rcButton.Width(), 0);
	//																										rcButton.OffsetRect(rcButton.Width(), 0);
	//m_strippingToolBar.AddButton(-1, IDS_UNDO_TIP,			ID_UNDO,						rcButton, 9);

	m_printSheetListToolBar.Create(this, GetParentFrame(), NULL, IDB_PRINTSHEET_PLANNING_TOOLBAR, IDB_PRINTSHEET_PLANNING_TOOLBAR_COLD, CSize(25, 26), &DrawToolsBackground);
	rcButton = CRect(CPoint(30,0), CSize(25,30));
	m_printSheetListToolBar.AddButton(-1,	IDS_LOADSTANDARD_TITLE,			ID_STANDARDLAYOUT_LOAD1,	rcButton, 5);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printSheetListToolBar.AddButton(-1,	IDS_STORESTANDARD_TITLE,		ID_STANDARDLAYOUT_STORE,	rcButton, 6);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printSheetListToolBar.AddButton(-1,	IDS_CFF_STORE,					ID_CFF_STORE,				rcButton, 7);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printSheetListToolBar.AddButton(-1,	IDS_PRINTSHEETNUMERATION_TITLE,	ID_DO_NUMBER,				rcButton, 8);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printSheetListToolBar.AddButton(-1,	IDS_DELETE,						ID_DELETE_SELECTION,		rcButton, 9);	rcButton.OffsetRect(rcButton.Width(), 0);
	//m_printSheetListToolBar.AddButton(-1,	IDS_UNDO_TIP,					ID_UNDO,					rcButton, 10);

	m_foldSheetListToolBar.Create(this, GetParentFrame(), NULL, IDB_PRODUCTVIEW_TOOLBAR, IDB_PRODUCTVIEW_TOOLBAR, CSize(40, 25), &DrawToolsBackground);
	rcButton = CRect(CPoint(30,0), CSize(40,30));
	m_foldSheetListToolBar.AddButton(-1, IDS_FOLDSHEETNUMERATION_TITLE,	ID_DO_NUMBER,	rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_foldSheetListToolBar.Hide();

	m_pagesListToolBar.Create(this, GetParentFrame(), NULL, IDB_PRODUCTVIEW_TOOLBAR, IDB_PRODUCTVIEW_TOOLBAR, CSize(40, 25), &DrawToolsBackground);
	rcButton = CRect(CPoint(30,0), CSize(40,30));
	m_pagesListToolBar.AddButton	(-1, IDS_PAGENUMERATION_TITLE,		ID_DO_NUMBER,	rcButton, 0);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pagesListToolBar.Hide(); 
	
	m_nToolBarHeight = 34;

	CScrollView::OnInitialUpdate();

	SetScrollSizes(MM_TEXT, CalcDocSize());
	Invalidate();
}

void CPrintSheetPlanningView::DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd)
{
	CRect rcHeader = rcFrame;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);
}

void CPrintSheetPlanningView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_headerColumnWidths.RemoveAll();
	m_HeaderCtrl.DestroyWindow();
	m_nHeaderHeight = 0;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	HD_ITEM hdi;
	hdi.mask = HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
	hdi.fmt	 = HDF_LEFT | HDF_STRING;

	CClientDC dc(this);
	CFont boldFont;
	boldFont.CreateFont(12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	dc.SelectObject(&boldFont);
	int nLongestNum = pDoc->m_PageTemplateList.GetLongestPageNumString(&dc) + 15;
	//if (IsOpenFoldSheetNumeration() || IsOpenPageNumeration())		// 15 = page icon width
	//	nLongestNum += dc.GetTextExtent(_T("(999)")).cx;			// room for 3 digits indices

	int		nIndex = 0;
	CString string = _T("");
	switch (m_nShowPlanningView)
	{
	case ShowImposition:		if (m_HeaderCtrl.GetSafeHwnd())
								{
									while (m_HeaderCtrl.GetItemCount() > 0)
									   m_HeaderCtrl.DeleteItem(0);
								}
								else
								{
									m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, ID_PLANNINGLIST_HEADER);	
									m_HeaderCtrl.SetFont(&m_HeaderFont, FALSE);
								}

								m_headerColumnWidths.RemoveAll();

								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= GetProductionPlanningHeaderWidth(nIndex, 10);
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PRODUCTPOOL);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= GetProductionPlanningHeaderWidth(nIndex, 220);
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PRODUCTION_DATA);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= GetProductionPlanningHeaderWidth(nIndex, 180);
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PROCESS_PRINTING);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= GetProductionPlanningHeaderWidth(nIndex, 250);
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_NUP);//IDS_PROCESS_CUTTING);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= GetProductionPlanningHeaderWidth(nIndex, 280);
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								if (pDoc->m_boundProducts.GetSize())
								{
									string.LoadString(IDS_SECTIONS);
									hdi.pszText		= string.GetBuffer(0);
									hdi.cxy			= GetProductionPlanningHeaderWidth(nIndex, 200);
									hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
									m_headerColumnWidths.Add(hdi.cxy);
									m_HeaderCtrl.InsertItem(nIndex++, &hdi);

									string.LoadString(IDS_BOOKBLOCK);
									hdi.pszText		= string.GetBuffer(0);
									hdi.cxy			= GetProductionPlanningHeaderWidth(nIndex, 300);
									hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
									m_headerColumnWidths.Add(hdi.cxy);
									m_HeaderCtrl.InsertItem(nIndex++, &hdi);
								}
								else
								{
									m_headerColumnWidths.Add(0);
									m_headerColumnWidths.Add(0);
								}

								break;

	case ShowPrintSheetList:	if (m_HeaderCtrl.GetSafeHwnd() && (m_nCurrentHeaderType == m_nShowPlanningView))
									break;	// OK

								if (m_HeaderCtrl.GetSafeHwnd())
								{
									while (m_HeaderCtrl.GetItemCount() > 0)
									   m_HeaderCtrl.DeleteItem(0);
								}
								else
								{
									m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, ID_PLANNINGLIST_HEADER);	
									m_HeaderCtrl.SetFont(&m_HeaderFont, FALSE);
								}

								m_headerColumnWidths.RemoveAll();

								string = _T("");
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 20;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_SHEET_TEXT);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 100;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_TYPE_TEXT);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 100;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PRESS);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 120;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PLATECOLOR_STRING);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 60;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_NUP);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 240;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string = _T("");//.LoadString(IDS_N_UP);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 50;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);	
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_NUMSHEETS_NET);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 60;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PAPERLIST_EXTRA);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 60;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_NUMSHEETS_TOTAL);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 60;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PAPER);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 180;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								break;

	case ShowFoldSheetList:		//if (IsOpenPageNumeration())		 
									//m_dlgNumeration.DestroyWindow();	
								
								if (m_HeaderCtrl.GetSafeHwnd() && (m_nCurrentHeaderType == m_nShowPlanningView))
									break;	// OK

								if (m_HeaderCtrl.GetSafeHwnd())
								{
									while (m_HeaderCtrl.GetItemCount() > 0)
									   m_HeaderCtrl.DeleteItem(0);
								}
								else
								{
									m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, ID_PLANNINGLIST_HEADER);	
									m_HeaderCtrl.SetFont(&m_HeaderFont, FALSE);
								}

								m_headerColumnWidths.RemoveAll();

								string.LoadString(IDS_PARTPRODUCT);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 80;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_NUMBER_TEXT);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= max(nLongestNum + 20, 40);
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_SCHEME);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 200;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_QUANTITY_PLANNED);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 60;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_BINDING_EXTRA);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 60;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_QUANTITY_ACTUAL);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 60;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_TYPEOF_PAPER);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 180;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								break;

	case ShowPaperList:			if (m_HeaderCtrl.GetSafeHwnd() && (m_nCurrentHeaderType == m_nShowPlanningView))
									break;	// OK

								if (m_HeaderCtrl.GetSafeHwnd())
								{
									while (m_HeaderCtrl.GetItemCount() > 0)
									   m_HeaderCtrl.DeleteItem(0);
								}
								else
								{
									m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, ID_PLANNINGLIST_HEADER);	
									m_HeaderCtrl.SetFont(&m_HeaderFont, FALSE);
								}

								m_headerColumnWidths.RemoveAll();

								nIndex = 0;
								string.LoadString(IDS_PAPER);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 150;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PRESS);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 120;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PRINTSHEETLAYOUT);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 120;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PRINTFORMAT);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 100;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PAPERLIST_FORMS);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 50;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);	
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_NUMSHEETS_TOTAL);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 80;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PAPERLIST_COMMENT);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 250;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);	
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								break;

	case ShowPages:				//if (IsOpenFoldSheetNumeration()) 
									//m_dlgNumeration.DestroyWindow();	
								
								if (m_HeaderCtrl.GetSafeHwnd() && (m_nCurrentHeaderType == m_nShowPlanningView))
									break;	// OK

								if (m_HeaderCtrl.GetSafeHwnd())
								{
									while (m_HeaderCtrl.GetItemCount() > 0)
									   m_HeaderCtrl.DeleteItem(0);
								}
								else
								{
									m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, ID_PLANNINGLIST_HEADER);	
									m_HeaderCtrl.SetFont(&m_HeaderFont, FALSE);
								}

								m_headerColumnWidths.RemoveAll();

								nIndex = 0;
								string.LoadString(IDS_PRODUCT);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 80;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_PAGELISTHEADER_PAGES);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= max(nLongestNum + 20, 80);
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_FOLDSHEET_LABEL);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 120;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								string.LoadString(IDS_OUTPUT_COLOR_SETTINGS);
								hdi.pszText		= string.GetBuffer(0);
								hdi.cxy			= 100;
								hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
								m_headerColumnWidths.Add(hdi.cxy);
								m_HeaderCtrl.InsertItem(nIndex++, &hdi);

								break;
	}

	m_nCurrentHeaderType = m_nShowPlanningView;

	m_nHeaderItemsTotalWidth = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
		m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];

	m_planningToolBar.MoveButtons(CPoint(30,0));
	m_strippingToolBar.MoveButtons(CPoint(400,0));
	m_printSheetListToolBar.MoveButtons(CPoint(30,0));
	m_foldSheetListToolBar.MoveButtons(CPoint(30,0));
	m_pagesListToolBar.MoveButtons(CPoint(30,0));

	switch (m_nShowPlanningView)
	{
	case ShowImposition:		m_planningToolBar.Show(); m_strippingToolBar.Hide(); m_printSheetListToolBar.Hide();	m_foldSheetListToolBar.Hide(); m_pagesListToolBar.Hide(); break;
	case ShowPrintSheetList:	m_planningToolBar.Hide(); m_strippingToolBar.Hide(); m_printSheetListToolBar.Show(); m_foldSheetListToolBar.Hide(); m_pagesListToolBar.Hide(); break;
	case ShowFoldSheetList:		m_planningToolBar.Hide(); m_strippingToolBar.Hide(); m_printSheetListToolBar.Hide(); m_foldSheetListToolBar.Show(); m_pagesListToolBar.Hide(); break;
	case ShowPaperList:			m_planningToolBar.Hide(); m_strippingToolBar.Hide(); m_printSheetListToolBar.Hide(); m_foldSheetListToolBar.Hide(); m_pagesListToolBar.Hide(); break;
	case ShowPages:				m_planningToolBar.Hide(); m_strippingToolBar.Hide(); m_printSheetListToolBar.Hide(); m_foldSheetListToolBar.Hide(); m_pagesListToolBar.Show(); break;
	}

	m_planningToolBar.Redraw(); 
	m_strippingToolBar.Redraw(); 
	m_printSheetListToolBar.Redraw(); 
	m_foldSheetListToolBar.Redraw(); 
	m_pagesListToolBar.Redraw();

	OnSize(0, 0, 0);
			
	ModifyStyle(0, WS_CLIPCHILDREN);	// Clip header control 

	Invalidate();

	m_DisplayList.Invalidate();

	m_docSize = CalcDocSize();
	SetScrollSizes(MM_TEXT, m_docSize);
}

void CPrintSheetPlanningView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);

	if (m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdl;
		WINDOWPOS wp;
		hdl.prc = &clientRect;
		hdl.pwpos = &wp;
		m_HeaderCtrl.Layout(&hdl);	
		m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y + m_nToolBarHeight, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_nHeaderHeight = wp.cy;
	} 
}

// This functions are overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling

void CPrintSheetPlanningView::ScrollToPosition(POINT pt)    // logical coordinates
{
	ASSERT(m_nMapMode > 0);     // not allowed for shrink to fit
	if (m_nMapMode != MM_TEXT)
	{
		CWindowDC dc(NULL);
		dc.SetMapMode(m_nMapMode);
		dc.LPtoDP((LPPOINT)&pt);
	}

	// now in device coordinates - limit if out of range
	int xMax = GetScrollLimit(SB_HORZ);
	int yMax = GetScrollLimit(SB_VERT);
	if (pt.x < 0)
		pt.x = 0;
	else if (pt.x > xMax)
		pt.x = xMax;
	if (pt.y < 0)
		pt.y = 0;
	else if (pt.y > yMax)
		pt.y = yMax;

	ScrollToDevicePosition(pt);
}

void CPrintSheetPlanningView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);
	
	CRect scrollRect, clipRect;
	GetClientRect(scrollRect); clipRect = scrollRect;
	clipRect.top+= m_nToolBarHeight + m_nHeaderHeight;	// Protect header control from being vertically scrolled
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, clipRect);

	if (m_HeaderCtrl.GetSafeHwnd())
		if (ptDev.x != 0)				// Scroll header control horizontally
			m_HeaderCtrl.MoveWindow(-ptDev.x, m_nToolBarHeight, scrollRect.Width() + ptDev.x, m_nHeaderHeight);

	if (m_InplaceEdit.GetSafeHwnd())
		m_InplaceEdit.Deactivate();
}

BOOL CPrintSheetPlanningView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect, clipRect;
		GetClientRect(scrollRect); clipRect = scrollRect;
		//if (m_HeaderCtrl.GetSafeHwnd())
			clipRect.top+= m_nToolBarHeight + m_nHeaderHeight;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, clipRect);

		if (m_HeaderCtrl.GetSafeHwnd())
			if (sizeScroll.cx != 0)				// Scroll header control horizontally
				m_HeaderCtrl.MoveWindow(-x, m_nToolBarHeight, scrollRect.Width() + x, m_nHeaderHeight);

		if (m_InplaceEdit.GetSafeHwnd())
			m_InplaceEdit.Deactivate();
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

CSize CPrintSheetPlanningView::CalcDocSize()
{
	CClientDC cdc(this);
	CDC memDC;
	memDC.CreateCompatibleDC(&cdc);

	//// Select a compatible bitmap into the memory DC
	CBitmap bitmap;
	bitmap.CreateCompatibleBitmap(&memDC, 32000, 32000);
	memDC.SelectObject( &bitmap );

	Draw(&memDC);

	memDC.DeleteDC();

	return m_docSize;
}

int	CPrintSheetPlanningView::GetComponentIndex(CDisplayItem& rDI)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) ||
			 (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent)))	   || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedLabel))) )
		{
			if (&rDI == pDI)	
				return nIndex;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return -1;
}

int	CPrintSheetPlanningView::GetPrintSheetIndex(CDisplayItem& rDI)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, SheetFrame)))
		{
			if (&rDI == pDI)	
				return nIndex;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return -1;
}

int CPrintSheetPlanningView::GetSelectedComponentIndex()
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				return nIndex;
			nIndex++;
		}
		else
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedLabel))) )
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				return nIndex;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return -1;
}

BOOL CPrintSheetPlanningView::IsSelectedComponentIndex(int nComponentIndex)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				if (nComponentIndex == nIndex)
					return TRUE;
			nIndex++;
		}
		else
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedLabel))) )
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				if (nComponentIndex == nIndex)
					return TRUE;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return FALSE;
}

void CPrintSheetPlanningView::SelectComponentByIndex(int nSelIndex)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (nIndex == nSelIndex)
				pDI->m_nState = CDisplayItem::Selected;
			else
				pDI->m_nState = CDisplayItem::Normal;
			nIndex++;
		}
		else
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedLabel))) )
		{
			if (nIndex == nSelIndex)
				pDI->m_nState = CDisplayItem::Selected;
			else
				pDI->m_nState = CDisplayItem::Normal;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
}

void CPrintSheetPlanningView::SelectComponentByFoldSheet(CFoldSheet* pFoldSheet, BOOL bScrollTo)
{
	CDisplayItem* pDIScrollTo	  = NULL;
	CDisplayItem* pDI			  = m_DisplayList.GetFirstItem();
	int			  nComponentIndex = 0;
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (pDI->m_pData == pFoldSheet)
			{
				pDI->m_nState = CDisplayItem::Selected;
				pDIScrollTo	  = pDI;
				m_nLastSelectedComponentIndex = nComponentIndex;
				break;
			}
			else
				pDI->m_nState = CDisplayItem::Normal;
			nComponentIndex++;
		}
		else
			if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedLabel))) )
				nComponentIndex++;

		pDI = m_DisplayList.GetNextItem(pDI);
	}

	if (pDIScrollTo)	
		if ( ! pDIScrollTo->m_bIsVisible)	
		{
			Invalidate();
			OnHScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.left - 20, NULL);
			OnVScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.top  - 20, NULL);
		}
}

void CPrintSheetPlanningView::SelectComponentByFlatProduct(CFlatProduct* pFlatProduct, BOOL bScrollTo)
{
	CDisplayItem* pDIScrollTo	  = NULL;
	CDisplayItem* pDI			  = m_DisplayList.GetFirstItem();
	int			  nComponentIndex = 0;
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedLabel))) )
		{
			if (pDI->m_pData == pFlatProduct)
			{
				pDI->m_nState = CDisplayItem::Selected;
				pDIScrollTo	  = pDI;
				m_nLastSelectedComponentIndex = nComponentIndex;
				break;
			}
			else
				pDI->m_nState = CDisplayItem::Normal;
			nComponentIndex++;
		}
		else
			if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
				nComponentIndex++;
		pDI = m_DisplayList.GetNextItem(pDI);
	}

	if (pDIScrollTo)	
		if ( ! pDIScrollTo->m_bIsVisible)	
		{
			Invalidate();
			OnHScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.left - 20, NULL);
			OnVScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.top  - 20, NULL);
		}
}

void CPrintSheetPlanningView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 20)
		hdn->pitem->cxy = 20;
}

void CPrintSheetPlanningView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	SetProductionPlanningHeaderWidth(hdn->iItem, hdn->pitem->cxy);

	if (m_headerColumnWidths.GetSize() > hdn->iItem)
		m_headerColumnWidths[hdn->iItem] = hdn->pitem->cxy; 

	m_nHeaderItemsTotalWidth = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
		m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];

	m_DisplayList.Invalidate();

	m_docSize = CalcDocSize();

	Invalidate();
	SetScrollSizes(MM_TEXT, m_docSize);
}

void CPrintSheetPlanningView::SetProductionPlanningHeaderWidth(int nIndex, int nValue)
{
	if (nIndex >= 20)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\ProductionPlanningHeader");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			columnWidthArray[nIndex] = nValue;			
		else
		{
			ZeroMemory(columnWidthArray, sizeof(columnWidthArray));
			columnWidthArray[nIndex] = nValue;			
		}

		dwSize = sizeof(columnWidthArray);
		RegSetValueEx(hKey, _T("Columns"), 0, REG_BINARY, (LPBYTE)&columnWidthArray, dwSize);

		RegCloseKey(hKey);
	}
}

int CPrintSheetPlanningView::GetProductionPlanningHeaderWidth(int nIndex, int nDefault)
{
	if (nIndex >= 20)
		return nDefault;

	int nWidth = nDefault;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\ProductionPlanningHeader");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			nWidth = columnWidthArray[nIndex];			
		else
			nWidth = nDefault;

		RegCloseKey(hKey);
	}

	if ( (nWidth <= 10) || (nWidth > 1000) )
		nWidth = nDefault;

	return nWidth;
}

int CPrintSheetPlanningView::GetSelectedPrintSheetIndex()
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, SheetFrame)))
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				return nIndex;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return -1;
}

BOOL CPrintSheetPlanningView::IsSelectedPrintSheetIndex(int nPrintSheetIndex)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, SheetFrame)))
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				if (nPrintSheetIndex == nIndex)
					return TRUE;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return FALSE;
}

void CPrintSheetPlanningView::ClipHeader(CDC* pDC)
{
	CRect rcClientRect, rcClip;
	GetClientRect(rcClientRect); rcClip = rcClientRect;
	rcClip.top = m_nToolBarHeight + m_nHeaderHeight;
	rcClip.OffsetRect(-pDC->GetViewportOrg());
	pDC->IntersectClipRect(rcClip);
}

void CPrintSheetPlanningView::OnDraw(CDC* pDC)
{
	ClipHeader(pDC);

	m_separatorBars.m_rectList.RemoveAll();

	Draw(pDC, &m_DisplayList);
}

//#include <d2d1.h>
//#include <d2d1helper.h>
//#include <dwrite.h>
//#include <wincodec.h>

//#pragma comment (lib, "d2d1.lib")
//
//extern ID2D1Factory* pDirect2dFactory;
//extern ID2D1DCRenderTarget* pRenderTarget;

void CPrintSheetPlanningView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	//if ( ! pDisplayList)
	//	return;

	//	HDC hdc = pDC->GetSafeHdc();
	//	CRect rcBox1(0,0,1000,1000);
	//	pRenderTarget->BindDC(hdc, &rcBox1);
	//	COLORREF cr = LIGHTGRAY;
	//	D2D1_COLOR_F cc;
	//	cc.a = 80;//GetAValue(c)/255.0f;
	//	if (cc.a == 0)
	//		cc.a = 1.0f; 
	//	cc.r = GetRValue(cr)/255.0f;
	//	cc.g = GetGValue(cr)/255.0f;
	//	cc.b = GetBValue(cr)/255.0f;
	//	ID2D1SolidColorBrush* pBlackBrush = NULL;
	//	pRenderTarget->CreateSolidColorBrush(cc, &pBlackBrush); 

	//CRect rect(10,10,500,500);
	//pDC->SetTextColor(BLACK);
	//CString strOut; 
	//pRenderTarget->BeginDraw();
	//for (int i = 0; i < 1000; i++)
	//{
	//	//pDC->FillSolidRect(rect, LIGHTGRAY);
	//	//CPrintSheetView::FillTransparent(pDC, CRect(10,10, 500,500), LIGHTGRAY, TRUE);
	//	//strOut.Format("%d", i + 1);
	//	//pDC->DrawText(strOut, CRect(10,10,500,500), DT_VCENTER | DT_CENTER);

	//	pRenderTarget->FillRectangle(D2D1::RectF((float)rect.left, (float)rect.top, (float)rect.right, (float)rect.bottom), pBlackBrush);

	//	rect.OffsetRect(1,1);

	//	//pRenderTarget->Release();
	//	//pBlackBrush->Release();
	//}
	//pRenderTarget->EndDraw();

	//return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (m_headerColumnWidths.GetSize() <= 0)
		return;

	CRect rcClientRect;
	GetClientRect(rcClientRect);

	CRect rcFrame = rcClientRect; rcFrame.right = rcFrame.left + m_docSize.cx;

	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	switch (m_nShowPlanningView)
	{
	case ShowImposition:		rcBox = DrawPlanningTable (pDC, rcFrame, pDisplayList);	break;

	case ShowPrintSheetList:	rcBox = DrawPrintSheetList(pDC, rcFrame, pDisplayList);	break;

	case ShowFoldSheetList:		rcBox = DrawFoldSheetList (pDC, rcFrame, pDisplayList);	break;

	case ShowPaperList:			rcBox = DrawPaperList	  (pDC, rcFrame, pDisplayList);	break;

	case ShowPages:				rcBox = DrawPageList	  (pDC, rcFrame, pDisplayList);	break;
	}

	m_docSize = rcBox.Size();
}

extern double g_duration[20];

CRect CPrintSheetPlanningView::DrawPlanningTable(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	rcFrame.top += m_nToolBarHeight + m_nHeaderHeight;	
	//rcFrame.right = rcFrame.left + 300;
	rcFrame.top += 5;

	int nIndex = 0;
	ResetDisplayColums();
	AddDisplayColumn(-1,					m_headerColumnWidths[nIndex++]);
	AddDisplayColumn(ColPrintGroup,			m_headerColumnWidths[nIndex++]);
	AddDisplayColumn(ColPrintingProfile,	m_headerColumnWidths[nIndex++]);
	AddDisplayColumn(ColPrinting,			m_headerColumnWidths[nIndex++]);
	AddDisplayColumn(ColCutting,			m_headerColumnWidths[nIndex++]);
	AddDisplayColumn(ColCollecting,			m_headerColumnWidths[nIndex++]);
	AddDisplayColumn(ColBinding,			m_headerColumnWidths[nIndex++]);

	for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
		pDoc->m_printGroupList[i].SetDisplayed(FALSE);

	for (int i = 0; i < 20; i++)
		g_duration[i] = 0;

	CRect rcSection2 = DrawGroupsAndProfiles		(pDC, rcFrame, pDisplayList);	rcBox.UnionRect(rcBox, rcSection2);	rcFrame.top += 5;
	CRect rcSection3 = DrawPrintingAndCutting		(pDC, rcFrame, pDisplayList);	rcBox.UnionRect(rcBox, rcSection3);	rcFrame.top += 5;
	CRect rcSection4 = DrawCollecting				(pDC, rcFrame, pDisplayList);	rcBox.UnionRect(rcBox, rcSection4);
	CRect rcSection5 = DrawBinding					(pDC, rcFrame, pDisplayList);	rcBox.UnionRect(rcBox, rcSection5); rcFrame.top = rcBox.bottom + 20;
	CRect rcSection1 = DrawUnassignedProductGroups	(pDC, rcFrame, pDisplayList);	rcBox.UnionRect(rcBox, rcSection1);	rcFrame.top = rcBox.bottom + 10;
	DrawPlanningTableLinks(pDC, pDisplayList);

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CPrintSheetPlanningView::DrawUnassignedProductGroups(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTop = rcFrame.top;

	BOOL bHasAssigned	= FALSE;
	BOOL bHasUnassigned = FALSE;
	rcFrame.top += 10; 
	for (int nPrintGroupIndex = 0; nPrintGroupIndex < pDoc->m_printGroupList.GetSize(); nPrintGroupIndex++)
	{
		CPrintGroup& rPrintGroup = pDoc->m_printGroupList[nPrintGroupIndex];
		if (rPrintGroup.GetFirstPrintSheet())
		{
			bHasAssigned = TRUE;
			continue;
		}

		bHasUnassigned = TRUE;

		CPrintingProfile& rPrintingProfile = rPrintGroup.GetPrintingProfile();
		//if (PrintingProfile.IsNull())
		//{
		//	if (rPrintGroup.m_strDefaultProfile.IsEmpty())
		//		PrintingProfile = theApp.m_printingProfiles.FindSuitable(rPrintGroup);
		//	else
		//		PrintingProfile.m_strName = rPrintGroup.m_strDefaultProfile;
		//	if (PrintingProfile.IsNull())
		//		PrintingProfile.m_strName	= _T("Profil w�hlen ...");
		//	else
		//		rPrintGroup.m_strDefaultProfile = PrintingProfile.m_strName;
		//}

		/////// columns
		CRect rcColumn = rcFrame;
		rcColumn.left += 5; 

		int	  nRight = 0;
		CRect rcRet;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight - 10;
			rcRet = CRect(rcColumn.TopLeft(), CSize(1,1));											
			switch (m_displayColumns[i].nID)
			{
			case ColPrintGroup:			rcColumn.right -= 20;
										rcRet = rPrintGroup.DrawInfo		(pDC, rcColumn, pDisplayList, RGB(255,150,100));	
										break;
			case ColPrintingProfile:	rcColumn.right -= 20;
										rcRet = rPrintingProfile.DrawInfo	(pDC, rcColumn, NULL,		  RGB(255,150,100));
										if (pDisplayList)
										{
											CRect rcDI = rcRet; 
											CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nPrintGroupIndex, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, DefaultPrintingProfile), NULL, CDisplayItem::ForceRegister); // register item into display list
											if (pDI)
											{
												pDI->SetMouseOverFeedback();
												pDI->SetNoStateFeedback();
												if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
												{
													CRect rcFeedback = pDI->m_BoundingRect; rcFeedback.left++; rcFeedback.top++; rcFeedback.right -=2; rcFeedback.bottom -= 2;
													rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
													CGraphicComponent gc;
													gc.DrawTransparentFrame(pDC, rcFeedback, RGB(255,195,15));
												}
											}
										}
										break;
			default:			break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + 5;
		}

		rcFrame.top = rcBox.bottom + 10;

		rPrintGroup.SetDisplayed(TRUE);
	}

	if (bHasAssigned && bHasUnassigned)
	{
		CPen   pen(PS_SOLID, 1, LIGHTGRAY);
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		pDC->MoveTo(rcFrame.left + 5,  nTop);//rcBox.bottom + 10);
		pDC->LineTo(2000,			   nTop);//rcBox.bottom + 10);
		pDC->SelectObject(pOldPen);

		rcBox.bottom += 20;
	}

	return rcBox;
}

CRect CPrintSheetPlanningView::DrawGroupsAndProfiles(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nRowHeight = m_headerColumnWidths[3];

	rcFrame.top += 5; 
	CRect rcRow = rcFrame; rcRow.top += 5; rcRow.bottom = rcRow.top + nRowHeight;

	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet&		rPrintSheet			= pDoc->m_PrintSheetList.GetNext(pos);
		CLayout*			pLayout				= rPrintSheet.GetLayout();
		CPrintGroup&		rPrintGroup			= rPrintSheet.GetPrintGroup();
		CPrintingProfile&	rPrintingProfile	= pLayout->GetPrintingProfile();

		/////// columns
		CRect rcColumn = rcRow; 
		//rcColumn.bottom = rcColumn.top + rcSheet.Height();
		rcColumn.left += 5; 

		int	  nRight = 0;
		CRect rcRet, rcPrintGroup;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight - 10;
			rcRet = CRect(rcColumn.TopLeft(), CSize(1,1));											
			switch (m_displayColumns[i].nID)
			{
			case ColPrintGroup:			rcColumn.right -= 20;
										if ( ! rPrintGroup.IsDisplayed()) 
										{
											rcRet = rPrintGroup.DrawInfo	(pDC, rcColumn, pDisplayList, m_boxFrameColor);										
											rcPrintGroup = rcRet;
										}
										break;
			case ColPrintingProfile:	rcColumn.right -= 20;
										if ( ! rPrintGroup.IsDisplayed()) 
											rcRet = rPrintingProfile.DrawInfo	(pDC, rcColumn, pDisplayList, m_boxFrameColor);										
										break;
			default:					break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + 5;
		}

		if (pos)
		{
			POSITION tmpPos = pos;
			if (&pDoc->m_PrintSheetList.GetNext(tmpPos).GetPrintGroup() != &rPrintGroup)	// look, if printgroup is going to change
				rcBox.UnionRect(rcBox, rcPrintGroup);
		}
		rcRow.top	 = rcBox.bottom + 20;
		rcRow.bottom = rcRow.top + nRowHeight;

		rPrintGroup.SetDisplayed(TRUE);
	}

	dc.DeleteDC();

	return rcBox;
}

CRect CPrintSheetPlanningView::DrawPrintingAndCutting(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nRowHeight = m_headerColumnWidths[3];

	rcFrame.top += 5; 
	CRect rcRow = rcFrame; rcRow.top += 5; rcRow.bottom = rcRow.top + nRowHeight;

	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet&		rPrintSheet			= pDoc->m_PrintSheetList.GetNext(pos);
		CLayout*			pLayout				= rPrintSheet.GetLayout();
		CPrintGroup&		rPrintGroup			= rPrintSheet.GetPrintGroup();
		CPrintingProfile& rPrintingProfile	= pLayout->GetPrintingProfile();

		if (rPrintSheet.IsEmpty())
			continue;

		//CRect rcSheet(CPoint(0,0), CSize(m_headerColumnWidths[3], nRowHeight));
		//rcSheet = (pDisplayList) ? rPrintSheet.GetDisplayBoxSheet() : rPrintSheet.DrawPlanningView(&dc, rcSheet, NULL, WHITE);

		/////// columns
		CRect rcColumn = rcRow; 
		//rcColumn.bottom = rcColumn.top + rcSheet.Height();
		rcColumn.left += 5; 

		int	  nRight = 0;
		CRect rcRet, rcSheet;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight - 10;
			rcRet = CRect(rcColumn.TopLeft(), CSize(1,1));											
			switch (m_displayColumns[i].nID)
			{
			case ColPrinting:			rcColumn.right -= 20; 
										rcRet = rPrintSheet.DrawPlanningView(pDC, rcColumn, pDisplayList, m_boxFrameColor);	
										rcSheet = rcRet;
										break;
			case ColCutting:			rcColumn.right -= 30;
										rcColumn.bottom = rcSheet.bottom;
										rcRet = rPrintSheet.DrawComponents	(pDC, rcColumn, pDisplayList, m_boxFrameColor);										
										break;
			default:					break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + 5;
		}

		//if (pos)
		//{
		//	POSITION tmpPos = pos;
		//	if (&pDoc->m_PrintSheetList.GetNext(tmpPos).GetPrintGroup() != &rPrintGroup)	// look, if printgroup is going to change
		//		rcBox.UnionRect(rcBox, rcPrintGroup);
		//}
		rcRow.top	 = rcBox.bottom + 20;
		rcRow.bottom = rcRow.top + nRowHeight;

		rPrintGroup.SetDisplayed(TRUE);
	}

	dc.DeleteDC();

	return rcBox;
}

CRect CPrintSheetPlanningView::DrawCollecting(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	rcFrame.left += m_headerColumnWidths[0] + m_headerColumnWidths[1] + m_headerColumnWidths[2] + m_headerColumnWidths[3] + m_headerColumnWidths[4];
	rcFrame.right = rcFrame.left + m_headerColumnWidths[5] - 30;
	rcFrame.top  += 5; 

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	///pDoc->m_boundProducts.ResetProductPartDisplayBoxes((pDisplayList) ? FALSE : TRUE);
	pDoc->m_boundProducts.ResetProductPartDisplayBoxes(TRUE);

	CRect			   rcDisplayBox		= CRect(rcFrame.TopLeft(), CSize(1, 1));
	CBoundProductPart* pPrevProductPart = NULL;
	for (int nProductIndex = 0; nProductIndex < pDoc->m_boundProducts.GetSize(); nProductIndex++)
	{
		POSITION pos = pDoc->m_Bookblock.GetHeadPosition_DescriptionList(nProductIndex);
		while (pos)
		{
			CDescriptionItem& rItem	= pDoc->m_Bookblock.GetNext_DescriptionList(nProductIndex, pos);
			if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
				continue;
			CFoldSheet&	rFoldSheet = rItem.GetFoldSheet(&pDoc->m_Bookblock);
			int nLayerIndex = 0;
			if ( ! rFoldSheet.GetPrintSheet(nLayerIndex))
				continue;
			CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();

			if (pPrevProductPart != &rProductPart)
			{
				if (pDisplayList)
				{
					//CBrush brush(RGB(240,245,250));
					//CPen   pen(PS_SOLID, 1, LIGHTGRAY);
					//CBrush* pOldBrush = pDC->SelectObject(&brush);
					//CPen*	pOldPen	  = pDC->SelectObject(&pen);
					//pDC->RoundRect(rProductPart.GetNextDisplayBox(), CPoint(10,10));
					//pDC->SelectObject(pOldBrush);
					//pDC->SelectObject(pOldPen);
				}
				if (pPrevProductPart)
				{
					rcDisplayBox = CRect(rcDisplayBox.left, rcDisplayBox.top, rcBox.right, rcBox.bottom);
					rcFrame.top = rcBox.bottom + 10;
					pPrevProductPart->AddDisplayBox(rcDisplayBox);
					CGraphicComponent gc;
					int nColorIndex = pDoc->m_boundProducts.TransformPartToProductIndex(pPrevProductPart->GetIndex());
					nColorIndex = nColorIndex % MAX_OBJECT_COLORS;
					COLORREF crDisplay = (nColorIndex == 0) ? RGB(200,200,200) : theApp.m_objectColors[nColorIndex];
					gc.DrawFrame(pDC, rcDisplayBox, crDisplay, TRUE, TRUE);
					rcDisplayBox = CRect(rcFrame.TopLeft(), CSize(1, 1));
				}

				CRect rcRet = rProductPart.DrawInfo (pDC, rcFrame, pDisplayList, RGB(240,245,250));
				rcBox.UnionRect(rcBox, rcRet);
			}

			/////// columns
			CRect rcColumn = rcFrame;
			if (pPrevProductPart != &rProductPart)
				rcColumn.top += 20;
			rcColumn.left += 5; 

			int	  nRight = 0;
			CRect rcRet;
			for (int i = 0; i < m_displayColumns.GetSize(); i++)
			{
				nRight += m_displayColumns[i].nWidth;
				rcColumn.right = nRight - 10;
				rcRet = CRect(rcColumn.TopLeft(), CSize(1,1));											
				switch (m_displayColumns[i].nID)
				{
				case ColCollecting:	rcColumn.right -= 50;
									rcRet = rFoldSheet.DrawSection(pDC, rcColumn, pDisplayList); 
									rcBox.UnionRect(rcBox, rcRet);
									break;
				default:			break;
				}

				rcColumn.left = nRight + 5;
			}

			pPrevProductPart = &rProductPart;
			rcFrame.OffsetRect(0, rcBox.bottom - rcFrame.top);
			//rcFrame.top = rcBox.bottom;
		}
	}

	if (pPrevProductPart)
	{
		rcDisplayBox = CRect(rcDisplayBox.left, rcDisplayBox.top, rcBox.right, rcBox.bottom);
		pPrevProductPart->AddDisplayBox(rcDisplayBox);

		int nColorIndex = pPrevProductPart->GetBoundProduct().GetIndex();
		nColorIndex = nColorIndex % MAX_OBJECT_COLORS;
		COLORREF crDisplay = (nColorIndex == 0) ? RGB(200,200,200) : theApp.m_objectColors[nColorIndex];
		CGraphicComponent gc;
		gc.DrawFrame(pDC, rcDisplayBox, crDisplay, TRUE, TRUE);
	}

	return rcBox;
}

CRect CPrintSheetPlanningView::DrawBinding(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	rcFrame.left += m_headerColumnWidths[0] + m_headerColumnWidths[1] + m_headerColumnWidths[2] + m_headerColumnWidths[3] + m_headerColumnWidths[4] + m_headerColumnWidths[5];
	rcFrame.right = rcFrame.left + m_headerColumnWidths[5] + m_headerColumnWidths[6] - 30;
	rcFrame.top  += 5; 

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CRect		   rcDisplayBox	= CRect(rcFrame.TopLeft(), CSize(1, 1));
	CBoundProduct* pPrevProduct	= NULL;
	for (int nProductIndex = 0; nProductIndex < pDoc->m_boundProducts.GetSize(); nProductIndex++)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetProduct(nProductIndex);
		if ( ! pDoc->m_Bookblock.GetFirstFoldSheet(-rBoundProduct.GetIndex() - 1))
			continue;

		if ( (pPrevProduct != &rBoundProduct) && pDoc->m_Bookblock.GetFirstFoldSheet(-nProductIndex - 1))
		{
			if (pPrevProduct)
			{
				rcFrame.top = rcBox.bottom + 10;
				pPrevProduct->SetDisplayBox(rcDisplayBox);
				int nColorIndex = pPrevProduct->GetIndex();
				nColorIndex = nColorIndex % MAX_OBJECT_COLORS;
				COLORREF crDisplay = (nColorIndex == 0) ? RGB(200,200,200) : theApp.m_objectColors[nColorIndex];
				CGraphicComponent gc;
				gc.DrawFrame(pDC, rcDisplayBox, crDisplay, TRUE, TRUE);
				rcDisplayBox = CRect(rcFrame.TopLeft(), CSize(1, 1));
			}

			CRect rcRet = rBoundProduct.DrawInfo(pDC, rcFrame, pDisplayList, m_boxFrameColor);
			rcBox.UnionRect(rcBox, rcRet);
		}

		/////// columns
		CRect rcColumn = rcFrame;
		if (pPrevProduct != &rBoundProduct)
			rcColumn.top += 20;
		rcColumn.left += 5; 

		int		nRight = 0;
		CRect	rcRet;
		CRect	rcBookblock;
		//CPoint	scrollPos = GetScrollPosition();;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight - 10;	
			rcColumn.bottom = rcColumn.top + rcColumn.Width() / 4;								
			rcRet = CRect(rcColumn.TopLeft(), CSize(1,1));											
			switch (m_displayColumns[i].nID)
			{					// sleepy
			case ColBinding:	rcRet = pDoc->m_Bookblock.Draw(pDC, rcColumn, -nProductIndex - 1, _T("")); 
								rcBox.UnionRect(rcBox, rcRet);
								break;
			default:			break;
			}

			rcColumn.left = nRight + 5;
		}

		pPrevProduct = &rBoundProduct;
		rcDisplayBox = CRect(rcDisplayBox.left, rcDisplayBox.top, rcBox.right + 20, rcBox.bottom);	// + 20 -> enough room for shingling

		rcFrame.OffsetRect(0, (rcBox.bottom - rcFrame.top) + 10);
		//rcFrame.top = rcBox.bottom + 10;
	}


	if (pPrevProduct)
	{
		rcDisplayBox = CRect(rcDisplayBox.left, rcDisplayBox.top, rcBox.right + 20, rcBox.bottom);	// + 20 -> enough room for shingling
		pPrevProduct->SetDisplayBox(rcDisplayBox);

		int nColorIndex = pPrevProduct->GetIndex();
		nColorIndex = nColorIndex % MAX_OBJECT_COLORS;
		COLORREF crDisplay = (nColorIndex == 0) ? RGB(200,200,200) : theApp.m_objectColors[nColorIndex];
		CGraphicComponent gc;
		gc.DrawFrame(pDC, rcDisplayBox, crDisplay, TRUE, TRUE);
	}

	return rcBox;
}

void CPrintSheetPlanningView::DrawPlanningTableLinks(CDC* pDC, CDisplayList* pDisplayList)
{
	if ( ! pDisplayList)
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
		pDoc->m_printGroupList[i].DrawPlanningTableLinks(pDC, FALSE, pDisplayList);

	int nVerticalAxis	= 0;
	int nMaxSourceRight = 0;
	POSITION pos = pDoc->m_printingProfiles.GetHeadPosition();
	while (pos)
	{
		CPrintingProfile& rProfile = pDoc->m_printingProfiles.GetNext(pos);
		nMaxSourceRight = max(nMaxSourceRight, rProfile.GetDisplayBox().right);
	}
	pos = pDoc->m_printingProfiles.GetHeadPosition();
	while (pos)
		pDoc->m_printingProfiles.GetNext(pos).DrawPlanningTableLinks(pDC, FALSE, nVerticalAxis, nMaxSourceRight, pDisplayList);

	pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
		pDoc->m_PrintSheetList.GetNext(pos).DrawPlanningTableLinks(pDC, FALSE, pDisplayList);

	nMaxSourceRight = nVerticalAxis	= 0;
	pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		nMaxSourceRight = max(nMaxSourceRight, rPrintSheet.GetDisplayBoxComponents().right);
	}

	pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			BOOL			   bFirst		= TRUE;
			CBoundProductPart& rProductPart = rBoundProduct.m_parts[i];
			CFoldSheet* pFoldSheet = rProductPart.GetFirstFoldSheet();
			while (pFoldSheet)
			{
				int nFoldSheetIndex = pFoldSheet->GetIndex();
				CDisplayItem* pDI = pDisplayList->GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintFoldSheetComponent));
				while (pDI)
				{
					CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
					if ((int)pDI->m_pData != nFoldSheetIndex)
					{
						pDI = pDisplayList->GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintFoldSheetComponent));
						continue;
					}
					int nColorIndex = pDoc->m_boundProducts.TransformPartToProductIndex(rProductPart.GetIndex());
					nColorIndex = nColorIndex % MAX_OBJECT_COLORS;
					COLORREF crDisplay = (nColorIndex > 0) ? theApp.m_objectColors[nColorIndex] : RGB(200,200,200);
					crDisplay = CGraphicComponent::IncreaseColorBrightness(crDisplay, 1.05f);
					CPen  pen(PS_SOLID, 2, crDisplay);
					CPen* pOldPen = pDC->SelectObject(&pen);

					CDisplayItem* pDITarget = pDisplayList->GetItemFromData((void*)rProductPart.GetIndex(), NULL, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, BoundProductPart));
					CRect rcSource = pDI->m_BoundingRect; rcSource.right = (pPrintSheet) ? pPrintSheet->GetDisplayBoxComponents().right : rcSource.right;
					CRect rcTarget = (rProductPart.m_dspi.rcDisplayBoxes.GetSize()) ? rProductPart.m_dspi.rcDisplayBoxes[0] : CRect();

					CPoint scrollPos = GetScrollPosition();
					rcSource.OffsetRect(scrollPos);

					if (bFirst)
					{
						nVerticalAxis = (nVerticalAxis == 0) ? rcTarget.left - nMaxSourceRight - 30 : nVerticalAxis - 5;
						nVerticalAxis = max(nVerticalAxis, 30);	// not under 30
						bFirst = FALSE;
					}
					int nR = 15;
					if (rcSource.CenterPoint().y < rcTarget.CenterPoint().y)	// draw downward
					{
						int nYY = rcTarget.CenterPoint().y;
						nR = min(nR, (rcTarget.CenterPoint().y - rcSource.CenterPoint().y)/2);
						CPoint pt1(rcSource.right,	   rcSource.CenterPoint().y);
						CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
						CPoint pt3(pt2.x,		  nYY);
						CPoint pt4(rcTarget.left, nYY);
						CPoint cp1 = pt2 + CSize(-nR,  nR);
						CPoint cp2 = pt3 + CSize( nR, -nR);
						CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
						CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

						pDC->MoveTo(pt1);
						pDC->LineTo(pt2 - CSize(nR, 0));
						pDC->AngleArc(cp1.x, cp1.y, nR, 90, -90);
						pDC->LineTo(pt3 - CSize(0, nR));
						pDC->AngleArc(cp2.x, cp2.y, nR, 180, 90);
						pDC->LineTo(pt4);
					}
					else	// draw upward
					{
						int nYY = rcTarget.CenterPoint().y;
						nR = min(nR, (rcSource.CenterPoint().y - rcTarget.CenterPoint().y)/2);
						CPoint pt1(rcSource.right,	   rcSource.CenterPoint().y);
						CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
						CPoint pt3(pt2.x,		  nYY);
						CPoint pt4(rcTarget.left, nYY);
						CPoint cp1 = pt2 + CSize(-nR, -nR);
						CPoint cp2 = pt3 + CSize( nR,  nR);
						CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
						CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

						pDC->MoveTo(pt1);
						pDC->LineTo(pt2 - CSize(nR, 0));
						pDC->AngleArc(cp1.x, cp1.y, nR, -90,  90);
						pDC->LineTo(pt3 + CSize(0, nR));
						pDC->AngleArc(cp2.x, cp2.y, nR, 180, -90);
						pDC->LineTo(pt4);
					}

					pDC->SelectObject(pOldPen);
					pen.DeleteObject();

					pDI = pDisplayList->GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintFoldSheetComponent));
				}

				pFoldSheet = rProductPart.GetNextFoldSheet(pFoldSheet);
			}
		}
	}

	nVerticalAxis = 0;
	pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = pDoc->m_boundProducts.GetNext(pos);
		for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
		{
			CBoundProductPart& rPart = rProduct.m_parts[i];
			for (int j = 0; j < rPart.m_dspi.rcDisplayBoxes.GetSize(); j++)
				nMaxSourceRight = max(nMaxSourceRight, rPart.m_dspi.rcDisplayBoxes[j].right);
		}
	}
	
	pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = pDoc->m_boundProducts.GetNext(pos);

		int nColorIndex = rProduct.GetIndex();
		nColorIndex = nColorIndex % MAX_OBJECT_COLORS;
		COLORREF crDisplay = (nColorIndex > 0) ? theApp.m_objectColors[nColorIndex] : RGB(200,200,200);
		crDisplay = CGraphicComponent::IncreaseColorBrightness(crDisplay, 1.05f);
		CPen  pen(PS_SOLID, 2, crDisplay);
		CPen* pOldPen = pDC->SelectObject(&pen);

		for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
		{
			CBoundProductPart& rPart = rProduct.m_parts[i];
			for (int j = 0; j < rPart.m_dspi.rcDisplayBoxes.GetSize(); j++)
			{
				CRect rcSource = rPart.m_dspi.rcDisplayBoxes[j];
				CRect rcTarget = rProduct.GetDisplayBox();

				nVerticalAxis = (nVerticalAxis == 0) ? rcTarget.left - nMaxSourceRight - 30 : nVerticalAxis - 5;
				nVerticalAxis = max(nVerticalAxis, 30);	// not under 30
				int nR = 15;
				if (rcSource.CenterPoint().y < rcTarget.CenterPoint().y)	// draw downward
				{
					nR = min(nR, (rcTarget.CenterPoint().y - rcSource.CenterPoint().y)/2);
					CPoint pt1(rcSource.right,				  rcSource.CenterPoint().y);
					CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
					CPoint pt3(pt2.x,		  rcTarget.CenterPoint().y);
					CPoint pt4(rcTarget.left, rcTarget.CenterPoint().y);
					CPoint cp1 = pt2 + CSize(-nR,  nR);
					CPoint cp2 = pt3 + CSize( nR, -nR);
					CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
					CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

					pDC->MoveTo(pt1);
					pDC->LineTo(pt2 - CSize(nR, 0));
					pDC->AngleArc(cp1.x, cp1.y, nR, 90, -90);
					pDC->LineTo(pt3 - CSize(0, nR));
					pDC->AngleArc(cp2.x, cp2.y, nR, 180, 90);
					pDC->LineTo(pt4);
				}
				else	// draw upward
				{
					nR = min(nR, (rcSource.CenterPoint().y - rcTarget.CenterPoint().y)/2);
					CPoint pt1(rcSource.right,				  rcSource.CenterPoint().y);
					CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
					CPoint pt3(pt2.x,		  rcTarget.CenterPoint().y);
					CPoint pt4(rcTarget.left, rcTarget.CenterPoint().y);
					CPoint cp1 = pt2 + CSize(-nR, -nR);
					CPoint cp2 = pt3 + CSize( nR,  nR);
					CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
					CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

					pDC->MoveTo(pt1);
					pDC->LineTo(pt2 - CSize(nR, 0));
					pDC->AngleArc(cp1.x, cp1.y, nR, -90,  90);
					pDC->LineTo(pt3 + CSize(0, nR));
					pDC->AngleArc(cp2.x, cp2.y, nR, 180, -90);
					pDC->LineTo(pt4);
				}
			}
		}
		pDC->SelectObject(pOldPen);
		pen.DeleteObject();
	}
}

	//pDI = pDisplayList->GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintFoldSheetComponent));
	//while (pDI)
	//{
	//	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
	//	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex((int)pDI->m_pData);
	//	if (pos)
	//	{
	//		CFoldSheet&		   rFoldSheet	= pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);
	//		CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();

	//		int nColorIndex = pDoc->m_boundProducts.TransformPartToProductIndex(rProductPart.GetIndex());
	//		nColorIndex = nColorIndex % MAX_OBJECT_COLORS;
	//		COLORREF crDisplay = (nColorIndex > 0) ? theApp.m_objectColors[nColorIndex] : RGB(200,200,200);
	//		crDisplay = CGraphicComponent::IncreaseColorBrightness(crDisplay, 1.05f);
	//		CPen  pen(PS_SOLID, 2, crDisplay);
	//		CPen* pOldPen = pDC->SelectObject(&pen);

	//		CDisplayItem* pDITarget = pDisplayList->GetItemFromData((void*)rProductPart.GetIndex(), NULL, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, BoundProductPart));
	//		CRect rcSource = pDI->m_BoundingRect; rcSource.right = (pPrintSheet) ? pPrintSheet->GetDisplayBoxComponents().right : rcSource.right;
	//		CRect rcTarget = (rProductPart.m_dspi.rcDisplayBoxes.GetSize()) ? rProductPart.m_dspi.rcDisplayBoxes[0] : CRect();

	//		CPoint scrollPos = GetScrollPosition();
	//		rcSource.OffsetRect(scrollPos);

	//		nVerticalAxis = (nVerticalAxis == 0) ? rcTarget.left - nMaxSourceRight - 30 : nVerticalAxis - 5;
	//		nVerticalAxis = max(nVerticalAxis, 30);	// not under 30
	//		int nR = 15;
	//		if (rcSource.CenterPoint().y < rcTarget.CenterPoint().y)	// draw downward
	//		{
	//			int nYY = rcTarget.CenterPoint().y;
	//			nR = min(nR, (rcTarget.CenterPoint().y - rcSource.CenterPoint().y)/2);
	//			CPoint pt1(rcSource.right,	   rcSource.CenterPoint().y);
	//			CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
	//			CPoint pt3(pt2.x,		  nYY);
	//			CPoint pt4(rcTarget.left, nYY);
	//			CPoint cp1 = pt2 + CSize(-nR,  nR);
	//			CPoint cp2 = pt3 + CSize( nR, -nR);
	//			CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
	//			CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

	//			pDC->MoveTo(pt1);
	//			pDC->LineTo(pt2 - CSize(nR, 0));
	//			pDC->AngleArc(cp1.x, cp1.y, nR, 90, -90);
	//			pDC->LineTo(pt3 - CSize(0, nR));
	//			pDC->AngleArc(cp2.x, cp2.y, nR, 180, 90);
	//			pDC->LineTo(pt4);
	//		}
	//		else	// draw upward
	//		{
	//			int nYY = rcTarget.CenterPoint().y;
	//			nR = min(nR, (rcSource.CenterPoint().y - rcTarget.CenterPoint().y)/2);
	//			CPoint pt1(rcSource.right,	   rcSource.CenterPoint().y);
	//			CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
	//			CPoint pt3(pt2.x,		  nYY);
	//			CPoint pt4(rcTarget.left, nYY);
	//			CPoint cp1 = pt2 + CSize(-nR, -nR);
	//			CPoint cp2 = pt3 + CSize( nR,  nR);
	//			CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
	//			CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

	//			pDC->MoveTo(pt1);
	//			pDC->LineTo(pt2 - CSize(nR, 0));
	//			pDC->AngleArc(cp1.x, cp1.y, nR, -90,  90);
	//			pDC->LineTo(pt3 + CSize(0, nR));
	//			pDC->AngleArc(cp2.x, cp2.y, nR, 180, -90);
	//			pDC->LineTo(pt4);
	//		}

	//		pDC->SelectObject(pOldPen);
	//		pen.DeleteObject();
	//	}
	//	pDI = pDisplayList->GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintFoldSheetComponent));
	//}

CRect CPrintSheetPlanningView::DrawPrintSheetList(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC, TRUE);

	rcFrame.top += m_nToolBarHeight + m_nHeaderHeight;
	rcFrame.left += 5; 
	rcBox = pDoc->m_PrintSheetList.Draw(pDC, rcFrame, m_headerColumnWidths, pDisplayList);

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CPrintSheetPlanningView::DrawFoldSheetList(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc* pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return rcBox;

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC, TRUE);

	rcFrame.top += m_nToolBarHeight + m_nHeaderHeight;
	rcFrame.left += 5; 
	rcBox = pDoc->m_Bookblock.DrawFoldSheetList(pDC, rcFrame, m_headerColumnWidths, pDisplayList);

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CPrintSheetPlanningView::DrawPaperList(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	rcFrame.top += m_nToolBarHeight + m_nHeaderHeight;
	rcFrame.left += 5; 
	rcBox = pDoc->m_productionData.m_paperList.Draw(pDC, rcFrame, m_headerColumnWidths, pDisplayList);

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CPrintSheetPlanningView::DrawPageList(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc* pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return rcBox;

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC, TRUE);

	rcFrame.top += m_nToolBarHeight + m_nHeaderHeight;
	rcFrame.left += 5; 
	rcBox = pDoc->m_PageTemplateList.DrawPagesList(pDC, rcFrame, m_headerColumnWidths, pDisplayList);

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	rcBox.bottom += Pixel2MM(pDC, 50);

	return rcBox;
}

// CPrintSheetPlanningView-Diagnose

#ifdef _DEBUG
void CPrintSheetPlanningView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetPlanningView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrintSheetPlanningView-Meldungshandler

int CPrintSheetPlanningView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CREATE_INPLACE_EDIT_EX(InplaceEdit);
	CREATE_INPLACE_COMBOBOX(InplaceComboBox, 0);

	if( m_dropTarget.Register( this ) )
		return 0;
	else
		return -1;

	return 0;
}

void CPrintSheetPlanningView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
	pDC->SetMapMode(MM_TEXT); 

	CScrollView::OnPrepareDC(pDC, pInfo);
}

///////////////////////////////////////////////////////////////////
// mouse events

void CPrintSheetPlanningView::OnLButtonDown(UINT nFlags, CPoint point) 
{
    SetFocus();	// In order to deactivate eventually active InplaceEdit

	if (m_separatorBars.m_bMouseIsOver)
		m_separatorBars.BeginDrag(point, this);
	else
		m_DisplayList.OnLButtonDown(point);

    CScrollView::OnLButtonDown(nFlags, point);
}

void CPrintSheetPlanningView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (m_separatorBars.m_bDragging)
		m_separatorBars.OnDrop(point, this);

    CScrollView::OnLButtonUp(nFlags, point);
}

void CPrintSheetPlanningView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	//m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list

	//CPoint pt = point;
	//ClientToScreen(&pt);
	//OnContextMenu(NULL, pt);
}

void CPrintSheetPlanningView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	if (nFlags & MK_CONTROL)
		if (GetCursor() == theApp.m_CursorMagnifyDecrease)
		{
			ZoomWindow(point);
			SetCursor(theApp.m_CursorStandard);
		}
}

void CPrintSheetPlanningView::ZoomWindow(const CPoint& rPoint, BOOL bRButton) 
{
	if (GetCursor() == theApp.m_CursorMagnifyDecrease)
	{
		//if ( ! m_zoomSteps.GetCount())
		//{
		//	SetZoomMode(FALSE);
		//	m_nZoomToFitMode = TRUE;
		//	CRect  clientRect;
		//	GetClientRect(&clientRect);
		//	float fRatio = (float)m_docSize.cx / (float)m_docSize.cy;

		//	m_viewPortSize.cy = clientRect.Height() - 2 * m_nViewBorder;
		//	SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));
		//}
		//else
		//{
		//	m_nZoomToFitMode = FALSE;
		//	m_viewPortSize   = m_zoomSteps.GetTail().m_viewportSize;
		//	CPoint scrollPos = m_zoomSteps.GetTail().m_scrollPos;
		//	m_zoomSteps.RemoveTail();
		//	SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));
		//	Invalidate();
  //     		ScrollToPosition(scrollPos);
		//}
		//GetBookBlockSize(-1);
	}
	else
	{
		BOOL		   bTracked = FALSE;
	    CRectTracker   tracker;
	    CMyRectTracker rbTracker;
		bTracked = (bRButton) ? rbTracker.TrackRubberBand(this, rPoint, TRUE) : tracker.TrackRubberBand(this, rPoint, TRUE);

		if (bTracked)
		{
			CRect trackRect = (bRButton) ? rbTracker.m_rect : tracker.m_rect;
			trackRect.NormalizeRect(); 

			CPoint ScrollPos = GetScrollPosition() + trackRect.TopLeft() - CSize(0, 50);
			CRect  clientRect;
			GetClientRect(&clientRect);
			float fRatio = (float)((clientRect.Height()) / (float)trackRect.Height());
			m_nPrintSheetRowHeight = (int)((float)m_nPrintSheetRowHeight * fRatio);

			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), NULL, 0, NULL, TRUE);	

			ScrollPos.x = 0;	//(long)((float)ScrollPos.x * factor);
			ScrollPos.y = (long)((float)ScrollPos.y * fRatio);//factor);
			ScrollToPosition(ScrollPos);
		}
	}
}

void CPrintSheetPlanningView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	//m_separatorBars.OnMouseMove(nFlags&MK_LBUTTON, point, this);

	CScrollView::OnMouseMove(nFlags, point);
}

LRESULT CPrintSheetPlanningView::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	m_DisplayList.OnMouseLeave();
	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// The default call of OnMouseWheel causes an assertion
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion

//	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}

CPrintGroupComponent& CPrintSheetPlanningView::GetSelPrintGroupComponent()
{
	static private CPrintGroupComponent nullPrintGroupComponent;
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullPrintGroupComponent;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintGroupComponent));
	if ( ! pDI)
		return nullPrintGroupComponent;

	CPrintGroup&		  rPrintGroup = pDoc->m_printGroupList.GetPrintGroup((int)pDI->m_pData);
	return rPrintGroup.GetComponent((int)pDI->m_pAuxData);
}

CFoldSheet* CPrintSheetPlanningView::GetSelFoldSheet()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if ( ! pDI)
	{
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages));
		if (pDI)
		{
			CFoldSheet*	pDummyFoldSheet = new CFoldSheet;
			pDummyFoldSheet->m_nProductPartIndex = (int)pDI->m_pData;
			return pDummyFoldSheet;
		}
		else
			return NULL;
	}
	else
		return (CFoldSheet*)pDI->m_pData;
}

int	CPrintSheetPlanningView::GetSelLayerIndex()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if ( ! pDI)
		return 0;
	return (int)pDI->m_pAuxData;
}

CFlatProduct* CPrintSheetPlanningView::GetSelFlatProduct()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent));
	if ( ! pDI)
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedLabel));

	if ( ! pDI)
		return NULL;
	else
		return (CFlatProduct*)pDI->m_pData;
}

CPrintSheet* CPrintSheetPlanningView::GetSelPrintSheet()
{
	CFoldSheet* pFoldSheet  = GetFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			return (pDoc) ? pDoc->m_PrintSheetList.GetBlankSheet(pFoldSheet->m_nProductPartIndex) : NULL;
		}
		else
			return pFoldSheet->GetPrintSheet(GetLayerIndex());
	else
		return NULL;
}

CLayout* CPrintSheetPlanningView::GetSelLayout()
{
	CFoldSheet* pFoldSheet  = GetFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
		{
			CPrintSheet* pPrintSheet = GetPrintSheet();
			return (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		}

	if (pFoldSheet)
		return pFoldSheet->GetLayout(GetLayerIndex());
	else
		return NULL;
}

CPressDevice* CPrintSheetPlanningView::GetSelPressDevice()
{
	CFoldSheet* pFoldSheet  = GetFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
		{
			CLayout* pLayout = GetSelLayout();
			return (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
		}

	if (pFoldSheet)
	{
		CLayout* pLayout = pFoldSheet->GetLayout(GetLayerIndex());
		return (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	}
	else
		return NULL;
}

int CPrintSheetPlanningView::GetActiveProductGroupIndex()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, ProductGroup));
	return (pDI) ? (int)pDI->m_pData : -1;
}

int CPrintSheetPlanningView::GetActiveLabelIndex()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, ProductGroup));
	if ( ! pDI)
		return -1;
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;
	CPrintGroup& rPrintGroup = pDoc->m_printGroupList.GetPrintGroup((int)pDI->m_pData);
	return (int)pDI->m_pAuxData;
}

BOOL CPrintSheetPlanningView::OnSelectLayoutObject(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMultiSelect()
{
    CDisplayItem* pDI = m_DisplayList.GetLastSelectedItem();
    if (pDI)
    {
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber)))
			OnActivatePrintSheetNumber(pDI, CPoint(0,0), 0);
		else
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantityExtra)))
			OnActivatePrintSheetQuantityExtra(pDI, CPoint(0,0), 0);
		else
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber)))
			OnActivatePageNumber(pDI, CPoint(0,0), 0);
		else
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber)))
			OnActivateFoldSheetNumber(pDI, CPoint(0,0), 0);
		else
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetPaperName)))
			OnActivateFoldSheetPaperName(pDI, CPoint(0,0), 0);
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnSelectProductGroup(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	pDI->m_nState = CDisplayItem::Normal;

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	CPrintGroup&		rPrintGroup		   = pDoc->m_printGroupList.GetPrintGroup((int)pDI->m_pData);
	CPrintingProfile& rPrintingProfile = rPrintGroup.GetPrintingProfile();
	
	//BOOL bChanged = FALSE;
	//CDlgProductionManager dlg;
	//dlg.m_productionProfile = rProductionProfile;
	//dlg.m_pPrintGroup		= &rPrintGroup;
	//if (dlg.DoModal() == IDOK)
	//{
	//	CProductionProfile oldProductionProfile = rProductionProfile;
	//	rProductionProfile = dlg.m_productionProfile;
	//	if (rProductionProfile.ApplyModifications(oldProductionProfile))
	//		bChanged = TRUE;

	//	if (bChanged)
	//	{
	//		pDoc->SetModifiedFlag();
	//		Invalidate();
	//		UpdateWindow();
	//	}
	//}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveProductGroup(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				//Graphics graphics(pDC->m_hDC);
				//Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				//COLORREF crHighlightColor = RGB(165,175,200);
				//SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				//graphics.FillRectangle(&br, rc);
				//graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.top++; rcRect.right -=2; rcRect.bottom -= 2;
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcRect, LIGHTBLUE);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnSelectPrintingProfile(CDisplayItem* pDI, CPoint point, LPARAM)	
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CPrintingProfile& rPrintingProfile	= pDoc->m_printingProfiles.GetPrintingProfile((int)pDI->m_pData);
	CLayout*	      pLayout			= rPrintingProfile.GetLayout();
	CPrintSheet* 	  pPrintSheet		= (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
	if ( ! pPrintSheet)
		return FALSE;

	if ( ! rPrintingProfile.IsNull() && pPrintSheet->IsEmpty())
	{
		CRect rcDeleteProfile = pDI->m_BoundingRect;
		rcDeleteProfile.left = rcDeleteProfile.right - 35; rcDeleteProfile.bottom = rcDeleteProfile.top + 35;
		if (rcDeleteProfile.PtInRect(point))
		{
			pDI->m_nState = CDisplayItem::Normal;
			rPrintingProfile = CPrintingProfile();
			pDoc->SetModifiedFlag();
			Invalidate();
			UpdateWindow();
			return FALSE;
		}
	}
	
	m_DisplayList.InvalidateItem(pDI);	// in order to display selected state immediately
	UpdateWindow();

	CPrintGroup&	   rPrintGroup  = pPrintSheet->GetPrintGroup();
	CBoundProductPart* pProductPart = rPrintGroup.GetFirstBoundProductPart();

	CDlgProductionManager dlg;
	dlg.m_productionProfile.m_strName				= rPrintingProfile.m_strName;
	dlg.m_productionProfile.m_strComment			= rPrintingProfile.m_strComment;
	dlg.m_productionProfile.m_nProductionType		= rPrintingProfile.m_nProductionType;
	dlg.m_productionProfile.m_layoutPool			= rPrintingProfile.m_layoutPool;
	dlg.m_productionProfile.m_prepress				= rPrintingProfile.m_prepress;
	dlg.m_productionProfile.m_press					= rPrintingProfile.m_press;
	dlg.m_productionProfile.m_postpress.m_cutting	= rPrintingProfile.m_cutting;

	BOOL bChanged = FALSE;
	CBindingDefs prevBindingDefs;
	if (pProductPart)
	{
		CBoundProduct& rProduct	= pProductPart->GetBoundProduct();
		dlg.m_productionProfile.m_postpress.m_binding  = rProduct.m_bindingParams;
		dlg.m_productionProfile.m_postpress.m_trimming = pProductPart->m_trimmingParams;
		dlg.m_productionProfile.m_postpress.m_folding  = pProductPart->m_foldingParams;
		prevBindingDefs								   = rProduct.m_bindingParams.m_bindingDefs;
	}
	dlg.m_pPrintGroup = &rPrintGroup;
	
	int nRet = dlg.DoModal();
	
	pDI->m_nState = CDisplayItem::Normal;
	
	if (nRet == IDOK)
	{
		CPrintingProfile oldPrintingProfile = rPrintingProfile;

		rPrintingProfile.AssignProductionProfilePool(dlg.m_productionProfile);

		if (rPrintingProfile.ApplyModifications(oldPrintingProfile))
			bChanged = TRUE;

		while (pProductPart)
		{
			CBoundProduct& rProduct	= pProductPart->GetBoundProduct();
			rProduct.AssignProductionProfilePool(dlg.m_productionProfile);
			pProductPart->AssignProductionProfilePool(dlg.m_productionProfile);
			rProduct.m_bindingParams.m_bindingDefs	= prevBindingDefs;
			pProductPart = rPrintGroup.GetNextBoundProductPart(pProductPart);
		}

		if (bChanged)
		{
			pDoc->SetModifiedFlag();
			Invalidate();
			UpdateWindow();
		}
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMovePrintingProfile(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC();
				ClipHeader(pDC);
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.top++; rcRect.right -=2; rcRect.bottom -= 2;

				CPrintingProfile& rPrintingProfile	= pDoc->m_printingProfiles.GetPrintingProfile((int)pDI->m_pData);
				CLayout*	 		pLayout				= rPrintingProfile.GetLayout();
				CPrintSheet* 		pPrintSheet			= (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
				if (pPrintSheet)
				{
					if ( ! rPrintingProfile.IsNull() && pPrintSheet->IsEmpty())
					{
						CImage img;
						img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_CLOSE_WINDOW));
						CRect rcImg = pDI->m_BoundingRect; rcImg.left = rcImg.right - 25;
						img.TransparentBlt(pDC->m_hDC, rcImg.left + 3, rcImg.top + 5, img.GetWidth(), img.GetHeight(), WHITE);
					}
				}

				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcRect, LIGHTBLUE);
				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnSelectDefaultPrintingProfile(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	
	m_DisplayList.InvalidateItem(pDI);	// in order to display selected state immediately
	UpdateWindow();

	//CPrintGroup&		rPrintGroup		   = pDoc->m_printGroupList.GetPrintGroup((int)pDI->m_pData);
	//CProductionProfile& rProductionProfile = rPrintGroup.GetProductionProfile();//(rPrintGroup.m_strDefaultProfile.IsEmpty()) ? theApp.m_productionProfiles.FindSuitable(rPrintGroup) : theApp.m_productionProfiles.FindProfile(rPrintGroup.m_strDefaultProfile);
	//
	//pDI->m_nState = CDisplayItem::Normal;

	//CDlgProductionManager dlg;
	//dlg.m_productionProfile = rProductionProfile;
	//dlg.m_pPrintGroup		= &rPrintGroup;
	//if (dlg.DoModal() == IDOK)
	//{
	//	rPrintGroup.m_strDefaultProfile = dlg.m_productionProfile.m_strName;
	//	rProductionProfile = dlg.m_productionProfile;
	//	pDoc->m_printGroupList.Analyze();
	//	m_DisplayList.Invalidate();
	//	Invalidate();
	//	UpdateWindow();
	//}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveDefaultPrintingProfile(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.top++; rcRect.right -= 2; rcRect.bottom -= 2;
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcRect, LIGHTBLUE);
				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnSelectPrintGroup(CDisplayItem* pDI, CPoint point, LPARAM)	
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	pDI->m_nState = CDisplayItem::Normal;

	CPrintGroup& rPrintGroup = pDoc->m_printGroupList.GetPrintGroup((int)pDI->m_pData);
	CPrintSheet* pPrintSheet = rPrintGroup.GetFirstPrintSheet();
	BOOL		 bIsEmpty	 = (pPrintSheet) ? pPrintSheet->IsEmpty() : TRUE;

	if (pPrintSheet && pDI && ! bIsEmpty)		
	{
		CRect rcDeleteLayout = pDI->m_BoundingRect;
		rcDeleteLayout.left = rcDeleteLayout.right - 35; rcDeleteLayout.bottom = rcDeleteLayout.top + 35;
		if (rcDeleteLayout.PtInRect(point))
		{
			RemovePrintGroupPrintSheets(rPrintGroup);
			return FALSE;
		}
	}

	m_DisplayList.InvalidateItem(pDI);	// in order to display selected state immediately
	UpdateWindow();

	//CProductionProfile& rProductionProfile	= rPrintGroup.GetProductionProfile();
	//if (rProductionProfile.IsNull())
	//{
	//	CDlgProductionManager dlg;
	//	dlg.m_productionProfile = rProductionProfile;
	//	dlg.m_pPrintGroup		= &rPrintGroup;
	//	
	//	if (dlg.DoModal() == IDOK)
	//	{
	//		CProductionProfile oldProductionProfile = rProductionProfile;
	//		rProductionProfile = dlg.m_productionProfile;
	//		if (rProductionProfile.ApplyModifications(oldProductionProfile))
	//			pDoc->SetModifiedFlag();
	//	}
	//}

	CDlgOptimizationMonitor dlg;
	dlg.Create(IDD_OPTIMIZATION_MONITOR);
	dlg.OptimizeGroup(rPrintGroup);
	dlg.DestroyWindow();

	pDoc->UpdateAllViews(NULL);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMovePrintGroup(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	CImage img;
	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IMPOSE_AUTO));
	CRect rcImg(CPoint(pDI->m_BoundingRect.right - img.GetHeight() - 6, pDI->m_BoundingRect.top + 6), CSize(img.GetWidth(), img.GetHeight()));
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				SaveBitmap(&m_bkMouseMove, rcImg);

				CImpManDoc*	pDoc = CImpManDoc::GetDoc();
				if (pDoc)
				{
					CPrintGroup& rPrintGroup = pDoc->m_printGroupList.GetPrintGroup((int)pDI->m_pData);
					CLayout*	 pLayout	 = rPrintGroup.GetLayout();
					CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
					BOOL bIsEmpty = (pPrintSheet) ? pPrintSheet->IsEmpty() : TRUE;
 					CDC* pDC = GetDC(); 
					pDC->SaveDC(); 
					ClipHeader(pDC);
					if (bIsEmpty)
					{
						CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.top++; rcRect.right -=2; rcRect.bottom -= 2;
						CGraphicComponent gc;
						gc.DrawTransparentFrame(pDC, rcRect, LIGHTBLUE);
					}
					else
					{
						img.Destroy();
						img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IMPOSE_REMOVE));
					}

					CRect rcFrame = rcImg; rcFrame.InflateRect(4, 4);
					CBrush brush(WHITE);
					CPen   pen(PS_SOLID, 2, MIDDLEGRAY);
					CBrush* pOldBrush = pDC->SelectObject(&brush);
					CPen*	pOldPen	  = pDC->SelectObject(&pen);
					pDC->RoundRect(rcFrame, CPoint(5,5));
					pDC->SelectObject(pOldBrush);
					pDC->SelectObject(pOldPen);

					img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
					pDC->RestoreDC(-1);
				}

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			RestoreBitmap(&m_bkMouseMove, rcImg);
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnSelectPrintGroupComponent(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	m_DisplayList.InvalidateItem(pDI);	// in order to display selected state immediately
	UpdateWindow();

	CPrintGroup&		  rPrintGroup = pDoc->m_printGroupList.GetPrintGroup((int)pDI->m_pData);
	CPrintGroupComponent& rComponent  = rPrintGroup.GetComponent((int)pDI->m_pAuxData);
	if (rComponent.m_nProductClass == CPrintGroupComponent::Bound)
		HandleSelectBoundPrintGroupComponent(rPrintGroup, rComponent);
	else
		HandleSelectFlatPrintGroupComponent(rPrintGroup, rComponent);

	//pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

void CPrintSheetPlanningView::HandleSelectBoundPrintGroupComponent(CPrintGroup& rPrintGroup, CPrintGroupComponent& rComponent)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheet* pPrintSheet = rPrintGroup.GetFirstPrintSheet();

	BOOL bDoImposeClassic = FALSE;

	if ( ! pPrintSheet)		// do impose
	{
		CDlgProductionManager dlg;
		dlg.m_pPrintGroup	  = &rPrintGroup;
		dlg.m_nComponentIndex = rComponent.GetIndex();
		if (dlg.DoModal() == IDCANCEL)
			return;

		theApp.UndoSave();

		//CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(rComponent.m_nProductClassIndex);
		//int nNumUnassignedPages = rProductPart.GetNumUnassignedPages();
		//while (nNumUnassignedPages > 0)
		//{
		//	if (dlg.m_productionProfile.m_postpress.m_folding.m_foldSchemeList.GetSize())
		//	{
		//		int nPoolIndex = -1;
		//		pPrintSheet = pDoc->m_PrintSheetList.AddBlankSheet(dlg.m_productionProfile);
		//		CFoldSheet newFoldSheet = dlg.m_productionProfile.m_postpress.m_folding.FindBestFoldScheme(nNumUnassignedPages, pPrintSheet, dlg.m_productionProfile.m_layoutPool, nPoolIndex);
		//		if (newFoldSheet.IsEmpty())
		//		{
		//			pDoc->m_PrintSheetList.RemovePrintSheet(pPrintSheet);
		//			break;
		//		}
		//		newFoldSheet.m_nProductPartIndex = rComponent.m_nProductClassIndex;
		//		pDoc->m_PrintSheetList.AddFoldSheet(newFoldSheet, INT_MAX, pPrintSheet, NULL);
		//		if (nPoolIndex >= 0)
		//			if (pPrintSheet)
		//			{
		//				CLayout* pLayout = pPrintSheet->GetLayout();
		//				if (pLayout)
		//				{
		//					pLayout->Copy(dlg.m_productionProfile.m_layoutPool[nPoolIndex].m_layout);
		//					pLayout->m_FrontSide.Invalidate();
		//					pLayout->m_BackSide.Invalidate();
		//					pLayout->m_FrontSide.Analyse();
		//					pLayout->m_BackSide.Analyse();
		//					pLayout->RearrangeGeometry(rProductPart, dlg.m_productionProfile);
		//					pLayout->AnalyzeMarks();
		//				}
		//			}
		//	}
		//	else
		//	{
		//		pPrintSheet = NULL;
		//		break;
		//	}
	
		//	pDoc->m_PrintSheetList.ReNumberPrintSheets();
		//	if (pPrintSheet)
		//		if (pPrintSheet->GetLayout())
		//			pPrintSheet->GetLayout()->AnalyzeMarks();
		//	
		//	nNumUnassignedPages = rProductPart.GetNumUnassignedPages();
		//}
		//pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	}
	else
		bDoImposeClassic = TRUE;

	if (bDoImposeClassic && pPrintSheet)
	{
		switch (rComponent.m_nProductClass)
		{
		case CPrintGroupComponent::Bound:	{
												CRect rcFrame, rcWin;
												GetWindowRect(rcFrame);
												CFoldSheet* pFoldSheet = pPrintSheet->GetFirstFoldSheetProductPart(rComponent.m_nProductClassIndex);
												//if ( ! pFoldSheet)
												//{
												//	pFoldSheet = new CFoldSheet;
												//	pFoldSheet->m_nProductPartIndex = rComponent.m_nProductClassIndex;
												//}
												if ( ! m_dlgFoldSchemeSelector.m_hWnd)
												{
													m_dlgFoldSchemeSelector.Create(IDD_SELECT_FOLDSCHEME_POPUP, this);
													if ( ! rcFrame.IsRectNull())
													{
														m_dlgFoldSchemeSelector.GetWindowRect(rcWin);
														if (GetPrintSheetViewStrippingData())
															m_dlgFoldSchemeSelector.MoveWindow(rcFrame.left, rcFrame.top, rcWin.Width(), rcWin.Height());
														else
														{
															int nXPos = max(10, rcFrame.left - rcWin.Width() - 20);
															int nYPos = 100;//rcFrame.top;
															m_dlgFoldSchemeSelector.MoveWindow(nXPos, nYPos, rcWin.Width(), rcWin.Height());
														}
													}
													m_dlgFoldSchemeSelector.ShowWindow(SW_SHOW);
												}
												
											}
											break;
		case CPrintGroupComponent::Flat:	break;
		}
	}

	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();

	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->SetModifiedFlag(TRUE);

	pDoc->UpdateAllViews(NULL);
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		 NULL, 0, NULL);	
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));	
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				 NULL, 0, NULL);	
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));	
	//theApp.UpdateView(RUNTIME_CLASS(CProductsView));	
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		 NULL, 0, NULL, TRUE);	
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));	
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData), NULL, 0, NULL);	
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
	//theApp.OnUpdateView(RUNTIME_CLASS(CProdDataPrintToolsView),		 NULL, 0, NULL);	
	//theApp.UpdateView(RUNTIME_CLASS(CProdDataPrintToolsView));	
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	
	//theApp.UpdateView(RUNTIME_CLASS(CProductsView));	
}

void CPrintSheetPlanningView::HandleSelectFlatPrintGroupComponent(CPrintGroup& rPrintGroup, CPrintGroupComponent& rComponent)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheet* pPrintSheet = rPrintGroup.GetFirstPrintSheet();

	if ( ! pPrintSheet)		// do impose
	{
		CDlgProductionManager dlg;
		if (dlg.DoModal() == IDCANCEL)
			return;

		CDlgOptimizationMonitor dlgOptimize;
		dlgOptimize.Create(IDD_OPTIMIZATION_MONITOR);
		dlgOptimize.OptimizeGroup(rPrintGroup);
		dlgOptimize.DestroyWindow();

		pDoc->UpdateAllViews(NULL);
	}
}

BOOL CPrintSheetPlanningView::OnMouseMovePrintGroupComponent(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	CImage img;
	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_IMPOSE));
	CRect rcImg(CPoint(pDI->m_BoundingRect.right + 6, pDI->m_BoundingRect.top), CSize(img.GetWidth(), img.GetHeight()));
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.top++; rcRect.right -=2; rcRect.bottom -= 2;
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcRect, LIGHTBLUE);

				SaveBitmap(&m_bkMouseMove, rcImg);
		
				img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			RestoreBitmap(&m_bkMouseMove, rcImg);
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivateLayoutObject(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnSelectSheetFrame(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	HandleOnSelectPrintSheet();

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivateSheetFrame(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	pDI->m_nState = CDisplayItem::Selected;

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnDeselectSheetFrame(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	HandleOnSelectPrintSheet();

	return TRUE;
}
//
//void CPrintSheetPlanningView::OnMouseMoveSheetFrame(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
//{
//	switch (lParam)
//	{
//	case 1:	// mouse enter
//			{
//				CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
//				if (pPrintSheet)
//					if (pPrintSheet->m_bDisplayOpen)
//						break;
//
//				//CRect rcButton(pDI->m_BoundingRect.left + 5, pDI->m_BoundingRect.top + 10, pDI->m_BoundingRect.left + 145, pDI->m_BoundingRect.top + 30);
//				//rcButton.left = rcButton.right - 20;
//				//if ( ! m_magnifyButton.m_hWnd)
//				//{
//				//	m_magnifyButton.Create(_T(""), WS_CHILD | BS_OWNERDRAW, rcButton, this, ID_MAGNIFY_SHEET);
//				//	m_magnifyButton.SetBitmap(IDB_MAGNIFY, IDB_MAGNIFY, -1, XCENTER);
//				//	m_magnifyButton.SetTransparent(TRUE);
//				//	m_magnifyButton.m_lParam = (LPARAM)pDI->m_pData;
//				//}
//
//				//if ( ! m_gotoDetailsButton.m_hWnd)
//				//{
//				//	rcButton.OffsetRect(-rcButton.Width() - 5, 0);
//				//	m_gotoDetailsButton.Create(_T(""), WS_CHILD | BS_OWNERDRAW, rcButton, this, ID_GOTO_DETAILS);
//				//	m_gotoDetailsButton.SetBitmap(IDB_ARROW_RIGHT_HOT, IDB_ARROW_RIGHT_COLD, -1, XCENTER);
//				//	m_gotoDetailsButton.SetTransparent(TRUE);
//				//	m_gotoDetailsButton.m_lParam = (LPARAM)pDI->m_pData;
//				//}
//				//m_magnifyButton.ShowWindow(SW_SHOW);
//				//m_gotoDetailsButton.ShowWindow(SW_SHOW);
//			}
//			break;
//
//	case 2:	// mouse leave
//			//if (m_magnifyButton.m_hWnd)
//			//	m_magnifyButton.DestroyWindow();
//			//if (m_gotoDetailsButton.m_hWnd)
//			//	m_gotoDetailsButton.DestroyWindow();
//			break;
//
//	default:// mouse move
//			break;
//	}
//}


BOOL CPrintSheetPlanningView::OnSelectLayoutData(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	CDlgSelectPrintLayoutPopup dlg(this, TRUE);
	dlg.m_foldSheet = (dlg.GetActPrintSheet()) ? *dlg.GetActPrintSheet()->GetFoldSheet(0) : CFoldSheet();
	dlg.Create(IDD_SELECTPRINTLAYOUTPOPUP, this);
	CRect rcWin;
	dlg.GetWindowRect(rcWin);
	CRect rcTarget = pDI->m_BoundingRect;
	ClientToScreen(rcTarget);
	dlg.MoveWindow(rcTarget.left - 10 - rcWin.Width(), rcTarget.top, rcWin.Width(), rcWin.Height());
	dlg.ShowWindow(SW_SHOW);
	int nRet = dlg.RunModalLoop();

	//if (nRet != IDCANCEL)
	//{
	//	rNewPrintSheet = *dlg.m_sampleList[-nRet].pPrintSheet;
	//	rNewLayout	   = *dlg.m_sampleList[-nRet].pLayout;
	//	return TRUE;
	//}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveLayoutData(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				CRect rcDI = pDI->m_BoundingRect;
				Rect rc(rcDI.left, rcDI.top, rcDI.Width(), rcDI.Height());
				COLORREF crHighlightColor = RGB(180,200,220);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				CImage img;
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_ARROW_DROPDOWN));
				CRect rcImg = pDI->m_BoundingRect; 
				img.TransparentBlt(pDC->m_hDC, rcImg.CenterPoint().x - img.GetWidth()/2, rcImg.CenterPoint().y - img.GetHeight()/2, img.GetWidth(), img.GetHeight(), WHITE);

				graphics.DrawLine(&Pen(Color::LightBlue),	rcDI.right,		rcDI.top + 1, rcDI.right,	  rcDI.bottom - 1);
				graphics.DrawLine(&Pen(Color::White),		rcDI.right + 1,	rcDI.top + 1, rcDI.right + 1, rcDI.bottom - 1);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

void CPrintSheetPlanningView::HandleOnSelectFoldSheetComponent()
{
	if (GetParentFrame()->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))	
	{
		HandleOnSelectFoldSheetComponentPrintSheetFrame();
		return;
	}

	int			nLayerIndex = GetSelLayerIndex();
	CFoldSheet* pFoldSheet  = GetSelFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
			pFoldSheet = NULL;

	if (pFoldSheet)
	{
		m_DisplayList.DeselectAll(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));

		CDisplayItem*	pDIScrollTo = NULL;
		CPrintSheet*	pPrintSheet	= pFoldSheet->GetPrintSheet(nLayerIndex);
		while (pPrintSheet)
		{
			for (int nComponentRefIndex = 0; nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
			{
				if (pPrintSheet->GetFoldSheet(nComponentRefIndex) == pFoldSheet)
				{
					CDisplayItem* pDISelect = m_DisplayList.GetItemFromData((void*)nComponentRefIndex, (void*)pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
					if (pDISelect)
					{
						pDISelect->m_nState = CDisplayItem::Selected;
						m_DisplayList.InvalidateItem(pDISelect);
						if ( ! pDIScrollTo)
							pDIScrollTo = pDISelect;
					}
				}
			}
			pPrintSheet	= pFoldSheet->GetNextPrintSheet(pPrintSheet, nLayerIndex);
		}
		if (pDIScrollTo)	
			if ( ! pDIScrollTo->m_bIsVisible)	
			{
				Invalidate();
				OnHScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.left - 20, NULL);
				OnVScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.top  - 20, NULL);
			}
	}

	m_DisplayList.InvalidateItem();
	Invalidate();
	UpdateWindow();

	if (m_dlgFoldSchemeSelector.m_hWnd)		// is open
	{
		m_dlgFoldSchemeSelector.InitData();	
		m_dlgFoldSchemeSelector.InitSchemes();
		m_dlgFoldSchemeSelector.SetActiveWindow();
	}
}

void CPrintSheetPlanningView::HandleOnSelectFoldSheetComponentPrintSheetFrame()
{
	int			nLayerIndex = GetSelLayerIndex();
	CFoldSheet* pFoldSheet  = GetSelFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
			pFoldSheet = NULL;

	CPrintSheetViewStrippingData* pPrintSheetViewStrippingData = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	if (pFoldSheet)
	{
		CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		CPrintSheet* pPrevPrintSheet = (pPrintSheetView) ? pPrintSheetView->GetActPrintSheet() : NULL;

		int nTemp = m_nLastSelectedComponentIndex;

		if ( ! pFoldSheet->GetNUpPrintLayout(pPrevPrintSheet, nLayerIndex))	// if objects located at this printsheet, dont change to first printsheet
		{
			CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
			if (pNavigationView)
				pNavigationView->SetActPrintSheet(pFoldSheet->GetPrintSheet(nLayerIndex), TRUE);
		}

		m_nLastSelectedComponentIndex = nTemp;	// m_nLastSelectedComponentIndex get lost in SetActPrintSheet()

		if (pPrintSheetViewStrippingData)
		{
			pPrintSheetViewStrippingData->OnUpdate(NULL, 0, NULL);
			pPrintSheetViewStrippingData->UpdateWindow();
		}

		if (pPrintSheetView)
		{
			CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)pPrintSheetView->GetParentFrame();
			if (pFrame)
				if ( ! pFrame->m_bShowFoldSheet)
				{
					pFrame->OnFoldsheetCheck();
					pPrintSheetView->UpdateWindow();
				}

			pPrintSheetView->m_DisplayList.DeselectAll(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
			m_nLastSelectedComponentIndex = nTemp;	// m_nLastSelectedComponentIndex get lost in DeselectAll()

			pPrintSheetView->m_DisplayList.Invalidate();
			pPrintSheetView->m_DisplayList.InvalidateItem(); 
			if (pPrintSheetView->ViewMode() != ID_VIEW_ALL_SHEETS)
				if (pPrevPrintSheet != pPrintSheetView->GetActPrintSheet())
				{
					pPrintSheetView->Invalidate();
					pPrintSheetView->OnUpdate(NULL, 0, NULL);
					pPrintSheetView->UpdateWindow();
				}

			CDisplayItem*	pDIScrollTo = NULL;
			CPrintSheet*	pPrintSheet	= pFoldSheet->GetPrintSheet(nLayerIndex);
			while (pPrintSheet)
			{
				for (int nComponentRefIndex = 0; nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
				{
					if (pPrintSheet->GetFoldSheet(nComponentRefIndex) == pFoldSheet)
					{
						CDisplayItem* pDISelect = pPrintSheetView->m_DisplayList.GetItemFromData((void*)nComponentRefIndex, (void*)pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
						if (pDISelect)
						{
							pDISelect->m_nState = CDisplayItem::Selected;
							pPrintSheetView->m_DisplayList.InvalidateItem(pDISelect);
							if ( ! pDIScrollTo)
								pDIScrollTo = pDISelect;
						}
					}
				}
				pPrintSheet	= pFoldSheet->GetNextPrintSheet(pPrintSheet, nLayerIndex);
			}
		}
	}

	Invalidate();
	UpdateWindow();

	//if (pPrintSheetViewStrippingData)		
	//	pPrintSheetViewStrippingData->SwitchImpositionDialog();
}

void CPrintSheetPlanningView::HandleOnSelectPrintSheet()
{
}

BOOL CPrintSheetPlanningView::OnSelectFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	//if ( ! pDI)
	//	return;

	//int			 nComponentRefIndex = (int)pDI->m_pData;
	//CPrintSheet* pPrintSheet	= (CPrintSheet*)pDI->m_pAuxData;
	//if ( ! pPrintSheet)
	//	return;
	//CFoldSheet* pFoldSheet = pPrintSheet->GetFoldSheet(nComponentRefIndex); 
	//if ( ! pFoldSheet)
	//	return;

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnDeselectFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM)		
{
	m_DisplayList.InvalidateItem(pDI);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivateFoldSheet(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	
{
	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				CRect rcDI = pDI->m_BoundingRect;
				Rect rc(rcDI.left, rcDI.top, rcDI.Width()-2, rcDI.Height()-2);
				COLORREF crHighlightColor = RGB(255,185,50);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnDeactivateFoldSheet(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)		
{
	return TRUE;
}
//
//void CPrintSheetPlanningView::OnMouseMoveFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
//{
//	switch (lParam)
//	{
//	case 1:	// mouse enter
//			{
//	 		//	CDC* pDC = GetDC(); 
//				//CRect trackerRect = pDI->m_BoundingRect;
//				//trackerRect.left   -= (trackerRect.left%2)   ? 1 : 0;
//				//trackerRect.top    -= (trackerRect.top%2)    ? 1 : 0;
//				//trackerRect.right  -= (trackerRect.right%2)  ? 1 : 1;
//				//trackerRect.bottom -= (trackerRect.bottom%2) ? 1 : 1;
//				//trackerRect.DeflateRect(2, 2);
//				//CRectTracker tracker(trackerRect, CRectTracker::dottedLine);
//				//tracker.Draw(pDC);
//				//ReleaseDC(pDC);
//				pDI->m_nLastPaintedState = -1;
//			}
//			break;
//
//	case 2:	// mouse leave
//			m_DisplayList.InvalidateItem(pDI);
//			break;
//
//	default:// mouse move
//			break;
//	}
//}

BOOL CPrintSheetPlanningView::OnSelectPrintSheetDetails(CDisplayItem* pDI, CPoint point, LPARAM)	
{
	pDI->m_nState = CDisplayItem::Normal;

	CRect rcGotoDetails  = pDI->m_BoundingRect;
	rcGotoDetails.left  += 0;  rcGotoDetails.right  = rcGotoDetails.left  + 25; rcGotoDetails.bottom  = rcGotoDetails.top  + 25;
	CRect rcDeleteLayout = pDI->m_BoundingRect;
	rcDeleteLayout.left += 20; rcDeleteLayout.right = rcDeleteLayout.left + 25; rcDeleteLayout.bottom = rcDeleteLayout.top + 25;
	CRect rcSelectLayout = pDI->m_BoundingRect;
	rcSelectLayout.left = rcSelectLayout.right - 30; rcSelectLayout.bottom = rcSelectLayout.top + 25;
	if (rcGotoDetails.PtInRect(point))
	{
		CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
		if ( ! pPrintSheet)
			return FALSE;

		CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
		theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETLIST);
		pFrame->SelectPrintSheet(pPrintSheet);
	}
	else
	if (rcDeleteLayout.PtInRect(point))
	{
		CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
		if ( ! pPrintSheet)
			return FALSE;

		RemovePrintSheet(pPrintSheet);
	}
	else
	if (rcSelectLayout.PtInRect(point))
	{	
		CImpManDoc* pDoc = CImpManDoc::GetDoc(); 
		if ( ! pDoc)
			return FALSE;
		CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
		CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		if ( ! pLayout)
			return FALSE;

		CDlgSelectPrintLayout dlg;
		dlg.m_strCurLayoutName = pPrintSheet->GetLayout()->m_strLayoutName;

		for (int i = 0; i < pLayout->m_variants.GetSize(); i++)
		{
			CPrintSheet printSheet = *pPrintSheet;
			CLayout		layout	   = *pLayout;

			layout.SelectLayoutVariant(printSheet, i);
			dlg.AddPrintLayout(printSheet, layout);
		}

		if (dlg.DoModal() == IDOK)
			if (dlg.m_nSelection >= 0)
			{
				pLayout->SelectLayoutVariant(*pPrintSheet, dlg.m_nSelection);
				pDoc->SetModifiedFlag();
				m_DisplayList.Invalidate();
				Invalidate();
				UpdateWindow();
				theApp.UpdateView(RUNTIME_CLASS(CProductsView));
			}

		//CPrintGroup&		  rPrintGroup = pPrintSheet->GetPrintGroup();
		//CPrintGroupComponent* pComponent  = rPrintGroup.GetFirstBoundComponent();
		//if (pComponent)
		//	HandleSelectBoundPrintGroupComponent(rPrintGroup, *pComponent);
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMovePrintSheetDetails(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);

				CRect rcDI = pDI->m_BoundingRect; rcDI.right--; rcDI.bottom--;
				CGraphicComponent gc;
				gc.DrawFrame(pDC, rcDI, RGB(140,160,180), TRUE);

				CImage img;
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_ARROW_RIGHT_HOT));
				CRect rcImg = pDI->m_BoundingRect; rcImg.left += 5;
				img.TransparentBlt(pDC->m_hDC, rcImg.left + 3, rcImg.top + 5, img.GetWidth(), img.GetHeight(), WHITE);

				img.Destroy();
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_CLOSE_WINDOW));
				rcImg = pDI->m_BoundingRect; rcImg.left += 25;
				img.TransparentBlt(pDC->m_hDC, rcImg.left + 3, rcImg.top + 5, img.GetWidth(), img.GetHeight(), WHITE);

				CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
				if (pPrintSheet)
				{
					CLayout* pLayout = pPrintSheet->GetLayout();
					if (pLayout)
					{
						if (pLayout->m_variants.GetSize() > 1)
						{
							img.Destroy();
							img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_ARROW_DROPDOWN));
							rcImg = pDI->m_BoundingRect; rcImg.left = rcImg.right - img.GetWidth() - 10; 
							img.TransparentBlt(pDC->m_hDC, rcImg.left + 6, rcImg.top + 4, img.GetWidth(), img.GetHeight(), WHITE);
						}
					}
				}

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnSelectControlMarks(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
	if ( ! pPrintSheet)
		return FALSE;

	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETMARKS);
	pFrame->SelectPrintSheet(pPrintSheet);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveControlMarks(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width()-1, pDI->m_BoundingRect.Height()-1);
				COLORREF crHighlightColor = RGB(180,200,220);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				CImage img;
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GOTO_NEXT_HOT));
				CRect rcImg = pDI->m_BoundingRect; 
				img.TransparentBlt(pDC->m_hDC, rcImg.CenterPoint().x - img.GetWidth()/2, rcImg.CenterPoint().y - img.GetHeight()/2, img.GetWidth(), img.GetHeight(), WHITE);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

void CPrintSheetPlanningView::OnKillFocusInplaceEdit() 		// Message from InplaceEdit
{
}

void CPrintSheetPlanningView::OnCharInplaceEdit(UINT nChar) // Message from InplaceEdit
{
	if (nChar == VK_RETURN)	
	{
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber))
		{
			UpdatePrintSheetNumber();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
			{
				m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
				m_DisplayList.InvalidateItem();
				OnActivatePrintSheetNumber(pDI, CPoint(0,0), 0);
				pDI->m_nState = CDisplayItem::Selected;
			}
			return;
		}
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutName))
		{
			UpdateLayoutName();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
			{
				m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
				m_DisplayList.InvalidateItem();
				OnActivateLayoutName(pDI, CPoint(0,0), 0);
				pDI->m_nState = CDisplayItem::Selected;
			}
			return;
		}
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantity))
		{
			UpdatePrintSheetQuantity();
			//CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			//if (pDI)
			//{
			//	m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
			//	m_DisplayList.InvalidateItem();
			//	OnActivatePrintSheetQuantity(pDI, CPoint(0,0), 0);
			//	pDI->m_nState = CDisplayItem::Selected;
			//}
			return;
		}
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantityExtra))
		{
			UpdatePrintSheetQuantityExtra();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
			{
				m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
				m_DisplayList.InvalidateItem();
				OnActivatePrintSheetQuantityExtra(pDI, CPoint(0,0), 0);
				pDI->m_nState = CDisplayItem::Selected;
			}
			return;
		}
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetQuantityPlanned))
		{
			UpdateFoldSheetQuantityPlanned();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
			{
				m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
				m_DisplayList.InvalidateItem();
				OnActivateFoldSheetQuantityPlanned(pDI, CPoint(0,0), 0);
				pDI->m_nState = CDisplayItem::Selected;
			}
			return;
		}
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetQuantityExtra))
		{
			UpdateFoldSheetQuantityExtra();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
			{
				m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
				m_DisplayList.InvalidateItem();
				OnActivateFoldSheetQuantityExtra(pDI, CPoint(0,0), 0);
				pDI->m_nState = CDisplayItem::Selected;
			}
			return;
		}
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber))
		{
			UpdatePageNumber();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
			{
				m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
				m_DisplayList.InvalidateItem();
				OnActivatePageNumber(pDI, CPoint(0,0), 0);
				pDI->m_nState = CDisplayItem::Selected;
			}
			return;
		}
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber))
		{
			UpdateFoldSheetNumber();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
			{
				m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
				m_DisplayList.InvalidateItem();
				OnActivateFoldSheetNumber(pDI, CPoint(0,0), 0);
				pDI->m_nState = CDisplayItem::Selected;
			}
			return;
		}
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PaperListEntryComment))
		{
			UpdatePaperListEntryComment();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
				OnActivatePaperListEntryComment(pDI, CPoint(0,0), 0);
			return;
		}
	}
}

BOOL CPrintSheetPlanningView::OnMouseMoveLayout(CDisplayItem* pDI, CPoint point, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
 				CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_AuxRect.left, pDI->m_AuxRect.top, pDI->m_AuxRect.Width(), pDI->m_AuxRect.Height());
				rc.Inflate(-1, -1);
				COLORREF crHighlightColor = RED;//RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				//graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);

				if (GetParentFrame()->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))	
				{
					CImage img;
					img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GOTO_SMALL));
					CRect rcImg = pDI->m_BoundingRect; 
					img.TransparentBlt(pDC->m_hDC, rcImg.left + 4, rcImg.top + 12, img.GetWidth(), img.GetHeight(), WHITE);
				}

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			//InvalidateRect(pDI->m_AuxRect);
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMovePrintSheetNumber(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveLayoutName(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMovePrintSheetQuantity(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMovePrintSheetQuantityExtra(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveFoldSheetNumber(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveFoldSheetQuantityPlanned(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveFoldSheetQuantityExtra(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMoveFoldSheetPaperName(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMovePageNumber(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseMovePaperListEntryComment(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				pDC->SaveDC(); 
				ClipHeader(pDC);
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
				pDC->RestoreDC(-1);
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnSelectLayout(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
	if ( ! pPrintSheet)
		return FALSE;
	if (GetParentFrame()->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
	{
		if (point.x < 50)
		{
			CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
			theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETLIST);
			pFrame->SelectPrintSheet(pPrintSheet);
		}
	}
	else
		if (GetParentFrame()->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		{
			CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
			if (pParentFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
			{
				CPrintSheetView* pView		   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
				CLayoutSide*	 pLayoutSide   = &((CLayout*)pDI->m_pData)->m_FrontSide;
				CPrintSheet*	 pPrintSheet   = (CPrintSheet*)pDI->m_pAuxData;
				CDisplayItem*	 pDILayoutSide = NULL;
				if (pLayoutSide && pView)
				{
					if (pParentFrame->m_bShowPlate)	
						pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
					else
						pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
					if (pDILayoutSide)
					{
						pView->m_DisplayList.DeselectAll();
						pView->m_DisplayList.SelectItem(*pDILayoutSide);
						pView->m_DisplayList.InvalidateItem(NULL);
					}
				}

				if (pDILayoutSide)
				{
					if ( ! pDILayoutSide->m_bIsVisible)	
					{
						pView->Invalidate();
						pView->m_DisplayList.Invalidate();
						CPoint ScrollPos = pView->GetScrollPosition();
						pView->ScrollToPosition(CPoint(ScrollPos.x + pDILayoutSide->m_BoundingRect.left - 20, 0));
					}
				}
			}
			else
				pParentFrame->SelectPrintSheet(pPrintSheet);
		}

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivateLayout(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnSelectPrintSheetNumber(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	if (m_dlgNumeration.m_hWnd)
	{
		m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
		m_dlgNumeration.m_pfnInitCallback(&m_dlgNumeration);
	}

	if ( ! m_DisplayList.m_bIsInsideMultiSelect)
		OnActivatePrintSheetNumber(pDI, point, lParam);
	else
	    m_DisplayList.InvalidateItem(pDI);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivatePrintSheetNumber(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	if (m_dlgNumeration.m_hWnd)
	{
		m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
		m_dlgNumeration.m_pfnInitCallback(&m_dlgNumeration);
	}

	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
	pDI->m_nState = CDisplayItem::Selected;
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CPrintSheet* pPrintSheet = (CPrintSheet*)di.m_pData;
	m_InplaceEdit.Activate(rect, (pPrintSheet) ? pPrintSheet->GetNumber() : _T(""), CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::UpdatePrintSheetNumber() 		
{
	m_InplaceEdit.Deactivate();

	CPrintSheet* pPrintSheet = (CPrintSheet*)m_InplaceEdit.m_di.m_pData;

	CString string = m_InplaceEdit.GetString();

	if ( (pPrintSheet->GetNumber() == string) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber)) == 1) )
		return;

	InvalidateRect(m_InplaceEdit.m_di.m_BoundingRect);

	int nCurNum = 1;
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
	while (pDI)
	{
		pPrintSheet = (CPrintSheet*)pDI->m_pData;
		if ( (string.Find(_T("$n")) >= 0) || (string.Find(_T("$r")) >= 0) )
		{
			pPrintSheet->m_strNumerationStyle = string;
			pPrintSheet->m_nNumerationOffset	 = -pPrintSheet->m_nPrintSheetNumber + nCurNum;
			nCurNum++;
		}
		else
		{
			pPrintSheet->m_strNumerationStyle = string;
		}
	
		InvalidateRect(pDI->m_BoundingRect);
		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
	}

	CImpManDoc::GetDoc()->SetModifiedFlag(TRUE, NULL, FALSE);										

	UpdateWindow();	

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	
}

BOOL CPrintSheetPlanningView::OnSelectLayoutName(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	OnActivateLayoutName(pDI, point, lParam);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivateLayoutName(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CLayout* pLayout = (CLayout*)di.m_pData;
	if (pLayout)
		m_InplaceEdit.Activate(rect, pLayout->m_strLayoutName, CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::UpdateLayoutName() 		
{
	CLayout* pLayout = (CLayout*)m_InplaceEdit.m_di.m_pData;

	if (pLayout->m_strLayoutName == m_InplaceEdit.GetString())
	{
		m_InplaceEdit.Deactivate();
		return;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if (&rLayout != pLayout)
			if (rLayout.m_strLayoutName == m_InplaceEdit.GetString())
			{
				CString strMsg;
				AfxFormatString1(strMsg, IDS_RENAME_LAYOUT_NAME_EXISTING, m_InplaceEdit.GetString()); 
				AfxMessageBox(strMsg);
				return;
			}
	}

	m_InplaceEdit.LockKillFocusHandler();	// to prevent inplace edit from being closed

	int nRet;
	if (pLayout->GetNumPrintSheetsBasedOn() > 1)
		nRet = AfxMessageBox(IDS_RENAME_LAYOUT_PROMPT, MB_YESNOCANCEL);
	else
		nRet = IDYES;

	m_InplaceEdit.UnlockKillFocusHandler();

	CRect rcInvalidate;
	switch (nRet)
	{
	case IDYES: 
			pLayout->m_strLayoutName = m_InplaceEdit.GetString();
			rcInvalidate = CRect(m_InplaceEdit.m_di.m_BoundingRect.left, 0, m_InplaceEdit.m_di.m_BoundingRect.right, m_docSize.cy);
			break;
	case IDNO:
		{
			CPrintSheet* pPrintSheet = (CPrintSheet*)m_InplaceEdit.m_di.m_pAuxData;
			CLayout*	 pLayout	 = pPrintSheet->GetLayout();
			if (pLayout)
			{
				CPrintingProfile profile =  pLayout->GetPrintingProfile();
				CLayout			 layout	 = *pLayout;
				layout.m_strLayoutName = m_InplaceEdit.GetString();
				pDoc->m_PrintSheetList.m_Layouts.AddTail(layout);
				pDoc->m_printingProfiles.AddTail(profile);

				CLayout* pNewLayout		 = &pDoc->m_PrintSheetList.m_Layouts.GetTail();
				int		 nNewLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;

				pPrintSheet->LinkLayout(pNewLayout);
				pPrintSheet->m_nLayoutIndex = nNewLayoutIndex;

				pDoc->m_printGroupList.Add(CPrintGroup());

				pDoc->m_PrintSheetList.ReNumberPrintSheets();
				rcInvalidate = m_InplaceEdit.m_di.m_BoundingRect;
			}
		}
		break;
	case IDCANCEL: 
		m_InplaceEdit.Deactivate();
		return;
	}

	m_InplaceEdit.Deactivate();

	CImpManDoc::GetDoc()->SetModifiedFlag();										

	InvalidateRect(rcInvalidate);
	UpdateWindow();	

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),		NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),					NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);	
}

BOOL CPrintSheetPlanningView::OnSelectPrintSheetQuantity(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	if (1)//( ! m_DisplayList.m_bIsInsideMultiSelect)
		OnActivatePrintSheetQuantity(pDI, point, lParam);
	else
	    m_DisplayList.InvalidateItem(pDI);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivatePrintSheetQuantity(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect; rect.InflateRect(1,1);

	CPrintSheet* pPrintSheet = (CPrintSheet*)di.m_pData;
	CString strQuantity; strQuantity.Format(_T("%d"), pPrintSheet->m_nQuantityTotal);

	m_InplaceEdit.Activate(rect, strQuantity, CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::UpdatePrintSheetQuantity()
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_InplaceEdit.m_di.m_pData;
	int nValue = _ttoi(m_InplaceEdit.GetString());

	m_InplaceEdit.Deactivate();

	if ( ! pPrintSheet)
		return;
	if ( (pPrintSheet->m_nQuantityTotal == nValue) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantity)) == 1) )
		return;

	int nQuantityTotal = nValue;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantity));
	while (pDI)
	{
		pPrintSheet = (CPrintSheet*)pDI->m_pData;

		pPrintSheet->m_nQuantityTotal = nValue;
		pPrintSheet->CalcTotalQuantity();

		pDI->m_nState = 0;

		InvalidateRect(pDI->m_BoundingRect);
		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantity));
		break;
	}

	CImpManDoc::GetDoc()->SetModifiedFlag(TRUE, NULL, FALSE);										

	//CRect rcInvalidate(0, m_InplaceEdit.m_di.m_BoundingRect.top, 10000, m_InplaceEdit.m_di.m_BoundingRect.bottom);
	//InvalidateRect(rcInvalidate);
	m_DisplayList.InvalidateItem(&m_InplaceEdit.m_di);
	UpdateWindow();	

	theApp.UpdateView(RUNTIME_CLASS(CProductsView));
}

BOOL CPrintSheetPlanningView::OnSelectPrintSheetQuantityExtra(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	if ( ! m_DisplayList.m_bIsInsideMultiSelect)
		OnActivatePrintSheetQuantityExtra(pDI, point, lParam);
	else
	    m_DisplayList.InvalidateItem(pDI);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivatePrintSheetQuantityExtra(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CPrintSheet* pPrintSheet = (CPrintSheet*)di.m_pData;
	CString strQuantity; strQuantity.Format(_T("%d"), pPrintSheet->m_nQuantityExtra);

	m_InplaceEdit.Activate(rect, strQuantity, CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::UpdatePrintSheetQuantityExtra()
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_InplaceEdit.m_di.m_pData;
	int nValue = _ttoi(m_InplaceEdit.GetString());

	m_InplaceEdit.Deactivate();

	if ( ! pPrintSheet)
		return;
	if ( (pPrintSheet->m_nQuantityExtra == nValue) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantityExtra)) == 1) )
		return;

	int nQuantityExtra = nValue;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantityExtra));
	while (pDI)
	{
		pPrintSheet = (CPrintSheet*)pDI->m_pData;

		pPrintSheet->m_nQuantityExtra = nValue;
		pPrintSheet->CalcTotalQuantity();

		InvalidateRect(pDI->m_BoundingRect);
		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantityExtra));
	}

	CImpManDoc::GetDoc()->SetModifiedFlag(TRUE, NULL, FALSE);										

	CRect rcInvalidate(0, m_InplaceEdit.m_di.m_BoundingRect.top, 10000, m_InplaceEdit.m_di.m_BoundingRect.bottom);
	InvalidateRect(rcInvalidate);
	UpdateWindow();	

	theApp.OnUpdateView(RUNTIME_CLASS(CProductsView), NULL, 0, NULL);	
}

BOOL CPrintSheetPlanningView::OnSelectFoldSheetNumber(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	if (m_dlgNumeration.m_hWnd)
	{
		m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
		m_dlgNumeration.m_pfnInitCallback(&m_dlgNumeration);
	}

	if ( ! m_DisplayList.m_bIsInsideMultiSelect)
		OnActivateFoldSheetNumber(pDI, point, lParam);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivateFoldSheetNumber(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	if (m_dlgNumeration.m_hWnd)
	{
		m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
		m_dlgNumeration.m_pfnInitCallback(&m_dlgNumeration);
	}

	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
	pDI->m_nState = CDisplayItem::Selected;
    m_DisplayList.InvalidateItem(NULL);//pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CFoldSheet* pFoldSheet = (CFoldSheet*)di.m_pData;
	m_InplaceEdit.Activate(rect, (pFoldSheet) ? pFoldSheet->GetNumber() : _T(""), CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::UpdateFoldSheetNumber()
{
	m_InplaceEdit.Deactivate();

	CFoldSheet* pFoldSheet = (CFoldSheet*)m_InplaceEdit.m_di.m_pData;
	if ( ! pFoldSheet)
		return;

	CString string = m_InplaceEdit.GetString();

	if ( (pFoldSheet->GetNumber() == string) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber)) == 1) )
		return;

	InvalidateRect(m_InplaceEdit.m_di.m_BoundingRect);

	int nCurNum = 1;
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
	while (pDI)
	{
		pFoldSheet = (CFoldSheet*)pDI->m_pData;
		if ( (string.Find(_T("$n")) >= 0) || (string.Find(_T("$r")) >= 0) )
		{
			pFoldSheet->m_strNumerationStyle = string;
			pFoldSheet->m_nNumerationOffset	 = -pFoldSheet->m_nFoldSheetNumber + nCurNum;
			nCurNum++;
		}
		else
		{
			int nValue = atoi(string);
			if (nValue != 0)
				pFoldSheet->m_nNumerationOffset	 = -pFoldSheet->m_nFoldSheetNumber + nValue;
			else
				pFoldSheet->m_strNumerationStyle = string;
		}
	
		InvalidateRect(pDI->m_BoundingRect);
		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
	}

	CImpManDoc::GetDoc()->SetModifiedFlag(TRUE, NULL, FALSE);	

	UpdateWindow();	
}

BOOL CPrintSheetPlanningView::OnSelectFoldSheetQuantityPlanned(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	OnActivateFoldSheetQuantityPlanned(pDI, point, lParam);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivateFoldSheetQuantityPlanned(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
	pDI->m_nState = CDisplayItem::Selected;
    m_DisplayList.InvalidateItem(pDI);
	//UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CFoldSheet* pFoldSheet = (CFoldSheet*)di.m_pData;
	CString strQuantity; strQuantity.Format(_T("%d"), pFoldSheet->GetQuantityPlanned());

	m_InplaceEdit.Activate(rect, strQuantity, CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::UpdateFoldSheetQuantityPlanned()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return;

	CFoldSheet* pFoldSheet = (CFoldSheet*)m_InplaceEdit.m_di.m_pData;
	int nValue = _ttoi(m_InplaceEdit.GetString());

	m_InplaceEdit.Deactivate();

	if ( ! pFoldSheet)
		return;

	if ( (pFoldSheet->GetQuantityPlanned() == nValue) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetQuantityPlanned)) == 1) )
		return;

	int nQuantityPlanned = _ttoi(m_InplaceEdit.GetString());

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetQuantityPlanned));
	while (pDI)
	{
		pFoldSheet = (CFoldSheet*)pDI->m_pData;

		pFoldSheet->SetQuantityPlanned(nQuantityPlanned);

		pDoc->m_Bookblock.m_FoldSheetList.TakeOverProductPartQuantityPlanned(pFoldSheet->m_nProductPartIndex, nQuantityPlanned);

		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetQuantityPlanned));
	}

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);										

	UpdateWindow();	
}

BOOL CPrintSheetPlanningView::OnSelectFoldSheetQuantityExtra(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	OnActivateFoldSheetQuantityExtra(pDI, point, lParam);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivateFoldSheetQuantityExtra(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
	pDI->m_nState = CDisplayItem::Selected;
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CFoldSheet* pFoldSheet = (CFoldSheet*)di.m_pData;
	CString strQuantity; strQuantity.Format(_T("%d"), pFoldSheet->GetQuantityExtra());

	m_InplaceEdit.Activate(rect, strQuantity, CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::UpdateFoldSheetQuantityExtra()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return;

	CFoldSheet* pFoldSheet = (CFoldSheet*)m_InplaceEdit.m_di.m_pData;
	int nValue = _ttoi(m_InplaceEdit.GetString());

	m_InplaceEdit.Deactivate();

	if ( ! pFoldSheet)
		return;

	if ( (pFoldSheet->GetQuantityExtra() == nValue) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetQuantityExtra)) == 1) )
		return;

	int nQuantityExtra = _ttoi(m_InplaceEdit.GetString());

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetQuantityExtra));
	while (pDI)
	{
		pFoldSheet = (CFoldSheet*)pDI->m_pData;

		pFoldSheet->SetQuantityExtra(nQuantityExtra);

		pDoc->m_Bookblock.m_FoldSheetList.TakeOverProductPartQuantityExtra(pFoldSheet->m_nProductPartIndex, nQuantityExtra);

		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetQuantityExtra));
	}

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);										

	UpdateWindow();	
}

void CPrintSheetPlanningView::OnSelChangeInplaceComboBox() // Message from InplaceComboBox
{
	if (m_InplaceComboBox.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetPaperName))
		UpdateFoldSheetPaperName();
}

BOOL CPrintSheetPlanningView::OnSelectFoldSheetPaperName(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	if ( ! m_DisplayList.m_bIsInsideMultiSelect)
		OnActivateFoldSheetPaperName(pDI, point, lParam);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivateFoldSheetPaperName(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
	pDI->m_nState = CDisplayItem::Selected;
    m_DisplayList.InvalidateItem(NULL);//pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CFoldSheet* pFoldSheet = (CFoldSheet*)di.m_pData;

	m_InplaceComboBox.Activate(rect, InitializePaperNameCombo, (pFoldSheet) ? pFoldSheet->m_strPaperName : _T(""), 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::InitializePaperNameCombo(CComboBox* pBox)
{
	POSITION pos = theApp.m_paperDefsList.GetHeadPosition();
	while (pos)
		pBox->AddString(theApp.m_paperDefsList.GetNext(pos).m_strName);
}

void CPrintSheetPlanningView::UpdateFoldSheetPaperName()
{
	m_InplaceComboBox.Deactivate();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetPaperName));
	while (pDI)
	{
		CFoldSheet* pFoldSheet = (CFoldSheet*)pDI->m_pData;
		if (pFoldSheet)
		{
			pFoldSheet->m_strPaperName = m_InplaceComboBox.GetSelString();
			pDoc->m_Bookblock.m_FoldSheetList.TakeOverProductPartPaperName(pFoldSheet->m_nProductPartIndex, pFoldSheet->m_strPaperName);
		}

		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetPaperName));
	}

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);										

	//InvalidateRect(m_InplaceComboBox.m_di.m_BoundingRect);
	UpdateWindow();	
}

BOOL CPrintSheetPlanningView::OnSelectPageNumber(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	if (m_dlgNumeration.m_hWnd)
	{
		m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
		m_dlgNumeration.m_pfnInitCallback(&m_dlgNumeration);
	}

	if ( ! m_DisplayList.m_bIsInsideMultiSelect)
		OnActivatePageNumber(pDI, point, lParam);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivatePageNumber(CDisplayItem* pDI, CPoint, LPARAM)	// object(s) has been selected
{
	if (m_dlgNumeration.m_hWnd)
	{
		m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
		m_dlgNumeration.m_pfnInitCallback(&m_dlgNumeration);
	}

	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
	pDI->m_nState = CDisplayItem::Selected;
    m_DisplayList.InvalidateItem(NULL);//pDI);
	UpdateWindow();

	CRect rect = di.m_AuxRect;

	CPageTemplate* pTemplate = (CPageTemplate*)di.m_pData;
	m_InplaceEdit.Activate(rect, (pTemplate) ? pTemplate->m_strPageID : _T(""), CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::UpdatePageNumber()
{
	m_InplaceEdit.Deactivate();

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPageTemplate* pTemplate = (CPageTemplate*)m_InplaceEdit.m_di.m_pData;
	if ( ! pTemplate)
		return;

	CString string = m_InplaceEdit.GetString();

	if ( (pTemplate->m_strPageID == string) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber)) == 1) )
		return;

	InvalidateRect(m_InplaceEdit.m_di.m_BoundingRect);

	int	nCurNum = 1;
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	while (pDI)
	{
		pTemplate = (CPageTemplate*)pDI->m_pData;
		if ( (string.Find(_T("$n")) >= 0) || (string.Find(_T("$r")) >= 0) )
		{
			int nIndex = pDoc->m_PageTemplateList.GetIndex(pTemplate, -1);
			pTemplate->m_strPageID		= CDlgNumeration::GenerateNumber(nIndex, string, nCurNum - nIndex);
			nCurNum++;
			pTemplate->m_bPageNumLocked = TRUE;
		}
		else
		{
			pTemplate->m_strPageID = string;
			pTemplate->m_bPageNumLocked = FALSE;
		}
	
		InvalidateRect(pDI->m_BoundingRect);
		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	}

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);										

	UpdateWindow();	
}

BOOL CPrintSheetPlanningView::OnSelectPaperListEntryComment(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	OnActivatePaperListEntryComment(pDI, point, lParam);

	return TRUE;
}

BOOL CPrintSheetPlanningView::OnActivatePaperListEntryComment(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
	pDI->m_nState = CDisplayItem::Selected;
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CPaperListEntry* pEntry = (CPaperListEntry*)di.m_pData;

	m_InplaceEdit.Activate(rect, (pEntry) ? pEntry->m_strComment : _T(""), CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetPlanningView::UpdatePaperListEntryComment()
{
	CPaperListEntry* pEntry = (CPaperListEntry*)m_InplaceEdit.m_di.m_pData;
	CString string = m_InplaceEdit.GetString();

	m_InplaceEdit.Deactivate();

	if ( ! pEntry || (pEntry->m_strComment == string))
		return;

	pEntry->m_strComment = string;

	CImpManDoc::GetDoc()->SetModifiedFlag(TRUE, NULL, FALSE);										
	
	InvalidateRect(m_InplaceEdit.m_di.m_AuxRect);
	UpdateWindow();	
}

void CPrintSheetPlanningView::OnMagnifySheet()
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_magnifyButton.m_lParam;
	if ( ! pPrintSheet)
		return;
	pPrintSheet->m_bDisplayOpen = TRUE;
	OnUpdate(NULL, 0, NULL);
	Invalidate();
	if (m_magnifyButton.m_hWnd)
		m_magnifyButton.DestroyWindow();
	if (m_gotoDetailsButton.m_hWnd)
		m_gotoDetailsButton.DestroyWindow();
}

void CPrintSheetPlanningView::OnGotoDetails()
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_gotoDetailsButton.m_lParam;
	if ( ! pPrintSheet)
		return;

	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETLIST);
	pFrame->SelectPrintSheet(pPrintSheet);

	if (m_magnifyButton.m_hWnd)
		m_magnifyButton.DestroyWindow();
	if (m_gotoDetailsButton.m_hWnd)
		m_gotoDetailsButton.DestroyWindow();
}

DROPEFFECT CPrintSheetPlanningView::OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintGroupComponent)))
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return DROPEFFECT_NONE;

		CPrintGroup&		  rPrintGroup = pDoc->m_printGroupList.GetPrintGroup((int)sourceDI.m_pData);
		CPrintGroupComponent& rComponent  = rPrintGroup.GetComponent((int)sourceDI.m_pData);
		CClientDC dc(this);
		m_rectDragIcon.left   = sourceDI.m_BoundingRect.left;
		m_rectDragIcon.right  = sourceDI.m_BoundingRect.left + 100; 
		m_rectDragIcon.top	  = sourceDI.m_BoundingRect.top - 10;
		m_rectDragIcon.bottom = sourceDI.m_BoundingRect.bottom + 10;
		m_rectDragIcon.OffsetRect(point - m_rectDragIcon.CenterPoint());

		m_ptDragIcon = CPoint(0,0);
		SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
		CRect rcFrame = m_rectDragIcon; rcFrame.DeflateRect(1,1);
		rPrintGroup.DrawFlatComponentStack(&dc, rcFrame, NULL);
		m_ptDragIcon = point;

		return DROPEFFECT_MOVE;
	}

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet)))
	{
		int nRefIndex			 = (int)sourceDI.m_pData;
		CPrintSheet* pPrintSheet = (CPrintSheet*)sourceDI.m_pAuxData;
		CFoldSheet*  pFoldSheet  = pPrintSheet->GetFoldSheet(nRefIndex);
		m_strDragIcon.Format(_T("%s [%s]"), pFoldSheet->GetNumber(), pFoldSheet->m_strFoldSheetTypeName);
		CClientDC dc(this);
		dc.SelectStockObject(ANSI_VAR_FONT);
		dc.SetTextColor(BLACK);
		dc.SetBkColor(LIGHTGRAY);
		CSize textExtent = dc.GetTextExtent(m_strDragIcon);
		m_rectDragIcon.left   = min(point.x - 40, point.x - 24 - textExtent.cx/2 - 1);
		m_rectDragIcon.right  = max(point.x + 8,  point.x - 24 + textExtent.cx/2 + 1); 
		m_rectDragIcon.top	  = point.y - 16;
		m_rectDragIcon.bottom = point.y + 16 + 2 * textExtent.cy;

		m_ptDragIcon = CPoint(0,0);
		SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
		dc.DrawIcon(point - CSize(40,16), m_hDragIcon);
		dc.TextOut(point.x - 24 - textExtent.cx/2, point.y + 5 + textExtent.cy, m_strDragIcon);
		m_ptDragIcon = point;

		if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
			return DROPEFFECT_COPY;
		else 
			return DROPEFFECT_MOVE;
	}

	DROPEFFECT	 de;
	CClientDC    dc(this);
	CDisplayItem targetDI = m_DisplayList.GetItemPtInside(point, TRUE);

	if (targetDI.IsNull())
	{	
		m_dragPoint = CPoint(0,0);
		de = DROPEFFECT_NONE;
	}
	else
	{
		m_dragPoint = CPoint(targetDI.m_AuxRect.CenterPoint().x, targetDI.m_AuxRect.CenterPoint().y - 1);

        if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout)))
			de = DROPEFFECT_NONE;	// don't drag to printsheet based on same layout
		else
			de = DROPEFFECT_MOVE;
	}										 

	if (de != DROPEFFECT_NONE)
	{
		CRect clientRect;
		GetClientRect(clientRect);
		m_rectBk = CRect(clientRect.left, m_dragPoint.y - 16, clientRect.right, m_dragPoint.y + 16);
		SaveBackground(&dc, m_rectBk);
		dc.DrawIcon(m_rectBk.TopLeft(), m_hArrowIcon);
	}

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout)))
	{
		m_hDragIcon	= theApp.LoadIcon(IDI_DRAG_LAYOUT);

		CLayout*	 pLayout	 = (CLayout*)sourceDI.m_pData;
		CPrintSheet* pPrintSheet = (CPrintSheet*)sourceDI.m_pAuxData;
		if (pLayout && pPrintSheet)
			m_strDragIcon.Format(_T(" %s  [%s] "), pPrintSheet->GetNumber(), pLayout->m_strLayoutName);
	}

	dc.SelectObject(&m_HeaderFont);
	dc.SetTextColor(BLACK);
	dc.SetBkColor(WHITE);
	CSize textExtent = dc.GetTextExtent(m_strDragIcon);
	m_rectDragIcon.left   = point.x + 23; //min(point.x - 40, point.x - 24 - textExtent.cx/2 - 1);
	m_rectDragIcon.right  = point.x + 23 + textExtent.cx + 2; 
	m_rectDragIcon.top	  = point.y - 16;
	m_rectDragIcon.bottom = point.y + 16 + 2 * textExtent.cy;

	m_ptDragIcon = CPoint(0,0);
	SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
	dc.DrawIcon(point + CSize(40,-16), m_hDragIcon);
	dc.TextOut(point.x + 24, point.y + 5 + textExtent.cy, m_strDragIcon);
	m_ptDragIcon = point;

	return de;
}


DROPEFFECT CPrintSheetPlanningView::OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintGroupComponent)))
		return OnDragOverPrintGroupComponent(sourceDI, dwKeyState, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet)))
		return OnDragOverFoldSheet(sourceDI, dwKeyState, point);

	if ( ! (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber)) ||
	        sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout))	) )
		return DROPEFFECT_NONE;

	int			 nSide	  = -1;
	CDisplayItem targetDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout), TRUE, 5);
	if (targetDI.IsNull())
	{
	   if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber)))
			return DROPEFFECT_NONE;
		 targetDI = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout), TRUE);
	}

 	CClientDC dc(this);

	BOOL bBitmapRestored = FALSE;
	if (point != m_ptDragIcon)
		RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	DROPEFFECT   de;
	if (targetDI.IsNull())
	{
		if (m_pBkBitmap)	// insertion arrow was shown - so hide now by restoring background
			RestoreBackground(&dc, m_rectBk);

		de = DROPEFFECT_NONE;
	}
	else
	{
        if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout)))
		{
			if ((nSide <= 0) && ((CPrintSheet*)sourceDI.m_pAuxData)->m_nLayoutIndex == ((CPrintSheet*)targetDI.m_pAuxData)->m_nLayoutIndex)
				de = DROPEFFECT_NONE;	// don't drag to printsheet based on same layout
			else
				de = DROPEFFECT_MOVE;
		}
		else
			if ((sourceDI.m_AuxRect == targetDI.m_AuxRect) && (nSide <= 0))	// drag and drop on same printsheet -> only COPY makes sense
				de = DROPEFFECT_COPY;
			else
				if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
					de = DROPEFFECT_COPY;
				else 
					if ((dwKeyState & MK_ALT) == MK_ALT)
						de = DROPEFFECT_MOVE;
					else
						de = DROPEFFECT_MOVE;
	}

	if (de != DROPEFFECT_NONE)
	{
		CPoint newDragPoint;
		if (nSide > 0)	// we are closely between two sheets
		{
			if (nSide == TOP)
				newDragPoint = CPoint(targetDI.m_AuxRect.CenterPoint().x, targetDI.m_AuxRect.top);
			else
				newDragPoint = CPoint(targetDI.m_AuxRect.CenterPoint().x, targetDI.m_AuxRect.bottom - 1);
		}
		else
			newDragPoint = CPoint(targetDI.m_AuxRect.CenterPoint().x, targetDI.m_AuxRect.top + 20);

		if ((m_dragPoint != newDragPoint))
		{
			if (point == m_ptDragIcon)	// bitmap needs to be restored even if point has not changed
			{
				RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);
				bBitmapRestored = TRUE;
			}

			if (m_pBkBitmap)	// insertion arrow was shown - so hide now by restoring background
				RestoreBackground(&dc, m_rectBk);

			CRect clientRect;
			GetClientRect(clientRect);
			m_rectBk = CRect(clientRect.left, newDragPoint.y - 16, clientRect.right, newDragPoint.y + 16);
			SaveBackground(&dc, m_rectBk);
			dc.DrawIcon(m_rectBk.TopLeft(), m_hArrowIcon);
			if (nSide > 0)	// show insertion line
			{
				dc.SelectObject(&m_redPen);
				dc.MoveTo(m_rectBk.left,  m_rectBk.CenterPoint().y);
				dc.LineTo(m_rectBk.right, m_rectBk.CenterPoint().y);
			}

			m_dragPoint = newDragPoint;
		}										 
	}
	else
	{
		if (m_pBkBitmap)	// insertion arrow was shown - so hide now by restoring background
			RestoreBackground(&dc, m_rectBk);

		m_dragPoint = CPoint(0,0);
	}

	if ((point != m_ptDragIcon) || bBitmapRestored)
	{
		m_rectDragIcon += point - m_ptDragIcon;
		SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
		dc.DrawIcon(point + CSize(40,-16), m_hDragIcon);
		dc.SelectObject(&m_HeaderFont);
		dc.SetTextColor(BLACK);
		dc.SetBkColor(WHITE);
		CSize textExtent = dc.GetTextExtent(m_strDragIcon);
		dc.TextOut(point.x + 24, point.y + 5 + textExtent.cy, m_strDragIcon);
		m_ptDragIcon = point;
	}
 
	return de;
}

DROPEFFECT CPrintSheetPlanningView::OnDragOverPrintGroupComponent(CDisplayItem& di, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT de;
	de = DROPEFFECT_MOVE;

	CClientDC dc(this);
	BOOL bBitmapRestored = FALSE;
	if (point != m_ptDragIcon)
		RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	if ((point != m_ptDragIcon) || bBitmapRestored)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return DROPEFFECT_NONE;

		CPrintGroup& rPrintGroup = pDoc->m_printGroupList.GetPrintGroup((int)di.m_pData);
		
		m_rectDragIcon += point - m_ptDragIcon;
		SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
		CRect rcFrame = m_rectDragIcon; rcFrame.DeflateRect(1,1);
		rPrintGroup.DrawFlatComponentStack(&dc, rcFrame, NULL);
		dc.SelectStockObject(ANSI_VAR_FONT);
		dc.SetTextColor(BLACK);
		dc.SetBkColor(LIGHTGRAY);
		CSize textExtent = dc.GetTextExtent(m_strDragIcon);
		dc.TextOut(point.x - 24 - textExtent.cx/2, point.y + 5 + textExtent.cy, m_strDragIcon);
		m_ptDragIcon = point;
	}

	return de;
}

DROPEFFECT CPrintSheetPlanningView::OnDragOverFoldSheet(CDisplayItem& di, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT de;
	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		de = DROPEFFECT_MOVE;

	CClientDC dc(this);
	BOOL bBitmapRestored = FALSE;
	if (point != m_ptDragIcon)
		RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	int			 nSide;
	CDisplayItem targetDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
	if (!targetDI.IsNull())
	{
		if ((di.m_pData == targetDI.m_pData) && (di.m_pAuxData == targetDI.m_pAuxData) && (de == DROPEFFECT_MOVE))	// drag and drop on same printsheet
			de = DROPEFFECT_COPY;																					// and with same foldsheet 
																													// -> only COPY makes sense
		CRect rect = targetDI.m_BoundingRect;
		switch (nSide)
		{
		case LEFT:		rect.left  -= 2; rect.right  = rect.left   + 4; break;
		case RIGHT:		rect.right += 2; rect.left   = rect.right  - 4; break;
		case BOTTOM:	rect.bottom-= 2; rect.top    = rect.bottom + 4; break;
		case TOP:		rect.top   += 2; rect.bottom = rect.top    - 4; break;
		}

		if (m_catchRect != rect)
		{
			if (point == m_ptDragIcon)	// bitmap needs to be restored even if point has not changed
			{
				RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);
				bBitmapRestored = TRUE;
			}
			dc.InvertRect(m_catchRect);
			dc.InvertRect(m_catchRect = rect);
		}										 
	}
	else
	{
		CDisplayItem closestDI, trimDI;
		CDisplayItem insideDI = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
		if (insideDI.IsNull())
		{
			//if (ViewMode() == ID_VIEW_ALL_SHEETS)
				closestDI = m_DisplayList.GetClosestItem(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Sheet));
		}
		else
		{
			trimDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutObject));
			if ( ! trimDI.IsNull())
			{
				CRect rect = trimDI.m_BoundingRect;
				switch (nSide)
				{
				case LEFT:		rect.left  -= 2; rect.right  = rect.left   + 4; rect.top  = insideDI.m_BoundingRect.top;  rect.bottom = insideDI.m_BoundingRect.bottom; break;
				case RIGHT:		rect.right += 2; rect.left   = rect.right  - 4; rect.top  = insideDI.m_BoundingRect.top;  rect.bottom = insideDI.m_BoundingRect.bottom; break;
				case BOTTOM:	rect.bottom-= 2; rect.top    = rect.bottom + 4; rect.left = insideDI.m_BoundingRect.left; rect.right  = insideDI.m_BoundingRect.right;  break;
				case TOP:		rect.top   += 2; rect.bottom = rect.top    - 4; rect.left = insideDI.m_BoundingRect.left; rect.right  = insideDI.m_BoundingRect.right;  break;
				}

				if (m_catchRect != rect)
				{
					if (point == m_ptDragIcon)	// bitmap needs to be restored even if point has not changed
					{
						RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);
						bBitmapRestored = TRUE;
					}
					dc.InvertRect(m_catchRect);
					dc.InvertRect(m_catchRect = rect);
				}										 
			}
		}

		if (trimDI.IsNull())
			if (!m_catchRect.IsRectNull())
			{
				dc.InvertRect(m_catchRect);
				m_catchRect = CRect(0,0,0,0);
			}

		if (closestDI.IsNull() && insideDI.IsNull())
			de = DROPEFFECT_NONE;
		
		if ( ! closestDI.IsNull())
			if (insideDI.IsNull())
				if ((nSide == LEFT) || (nSide == RIGHT))
					de = DROPEFFECT_NONE;

		if ( ! insideDI.IsNull())
			if (insideDI.IsEqual(&di) && (de == DROPEFFECT_MOVE))	// drag and drop on same printsheet -> only COPY makes sense
				de = DROPEFFECT_COPY;
	}

	if ((point != m_ptDragIcon) || bBitmapRestored)
	{
		m_rectDragIcon += point - m_ptDragIcon;
		SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
		dc.DrawIcon(point - CSize(40,16), m_hDragIcon);
		dc.SelectStockObject(ANSI_VAR_FONT);
		dc.SetTextColor(BLACK);
		dc.SetBkColor(LIGHTGRAY);
		CSize textExtent = dc.GetTextExtent(m_strDragIcon);
		dc.TextOut(point.x - 24 - textExtent.cx/2, point.y + 5 + textExtent.cy, m_strDragIcon);
		m_ptDragIcon = point;
	}
 
	return de;
}

void CPrintSheetPlanningView::OnDragLeave() 
{
	CClientDC  dc(this);

	RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	if (m_pBkBitmap)	// separation arrow was shown - so hide now by restoring background
	{
		CClientDC dc(this);
		RestoreBackground(&dc, m_rectBk);
	}

	if (!m_catchRect.IsRectNull())
	{
		dc.InvertRect(m_catchRect);
		m_catchRect = CRect(0,0,0,0);
	}
}

BOOL CPrintSheetPlanningView::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point) 
{
	RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	if (m_pBkBitmap)	// separation arrow was shown - so hide now by restoring background
	{
		CClientDC dc(this);
		RestoreBackground(&dc, m_rectBk);
	}

	if (dropEffect == DROPEFFECT_NONE)
		return FALSE;

	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return FALSE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if ( ! (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet)) ||
	        sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout)) ) )
		return FALSE;

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet)))
		return OnDropFoldSheet(sourceDI, dropEffect, point);

	int			 nSide	  = -1;
	CDisplayItem targetDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout), TRUE, 5);
	if (targetDI.IsNull())
	{
	   if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber)))
			return DROPEFFECT_NONE;
		 targetDI = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout), TRUE);
	}

	if (targetDI.IsNull())
		return FALSE;

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout)))
		return MoveCopyLayout(sourceDI, targetDI, nSide, dropEffect);

	return FALSE;
}

BOOL CPrintSheetPlanningView::OnDropFoldSheet(CDisplayItem& di, DROPEFFECT dropEffect, CPoint point)
{
	CClientDC dc(this);

	RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	if (!m_catchRect.IsRectNull())
	{
		dc.InvertRect(m_catchRect);
		m_catchRect = CRect(0,0,0,0);
	}

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL		 bFurtherQuestions = TRUE;
	int			 nSide;
	CDisplayItem closestDI;
	CDisplayItem insideDI;
	CDisplayItem targetDI  = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
	if (targetDI.IsNull())
		closestDI = m_DisplayList.GetClosestItem(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Sheet));
	if (closestDI.IsNull())
		insideDI = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));

	CLayoutObject* pSpreadObject = NULL;
	if ( targetDI.IsNull() && !insideDI.IsNull())
	{
		CDisplayItem trimDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutObject));
		if ( ! trimDI.IsNull())
		{
			targetDI	  = insideDI;
			pSpreadObject = (CLayoutObject*)trimDI.m_pData;
		}
	}

	if ( ! targetDI.IsNull())
	{
		CPrintSheet* pSourcePrintSheet = (CPrintSheet*)di.m_pAuxData;
		CPrintSheet* pTargetPrintSheet = (CPrintSheet*)targetDI.m_pAuxData;
		BOOL		 bSourceInnerForm  = (di.m_AuxRect.Width()		  <= 2) ? TRUE : FALSE; // trick: use auxrect width to indicate if source is outer or inner form.
		BOOL		 bTargetBackSide   = (targetDI.m_AuxRect.Height() <= 2) ? TRUE : FALSE; // trick: use auxrect height to indicate if target is on layout backside
		BOOL		 bAutoAssign	   = FALSE;										  

		CFoldSheetDropTargetInfo dropTargetInfo(CFoldSheetDropTargetInfo::DropTargetLayoutObject, pTargetPrintSheet, NULL, NULL, (int)targetDI.m_pData, 0.0f, 0.0f, 0.0f, 0.0f, nSide, BOTTOM, FALSE);

		if (pSourcePrintSheet->GetLayout() == pTargetPrintSheet->GetLayout())
		{
			if (pSourcePrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
			{
				int ret = IDNO;
				// If source and target layout are equal -> ask user if he wants
				// this operation be performed on all printsheets of this type   
				if (pSourcePrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 3)		// in order to perform this action to multiple sheets, we need min. 2 pairs of sheets
					ret = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNOCANCEL);
				switch (ret)
				{
				case IDYES	  :	bAutoAssign = TRUE;		
								if (pSourcePrintSheet != pTargetPrintSheet)	// If different printsheets and AutoAssign 
									dropEffect = DROPEFFECT_MOVE;				// -> only MOVE makes sense
								break;
				case IDNO	  : bFurtherQuestions = FALSE;
								break;
				case IDCANCEL : return FALSE;
				}
			}
		}

		if (dropEffect == DROPEFFECT_COPY)
		{
			//if (bFurtherQuestions)
			//	if (AfxMessageBox(IDS_FOLDSHEET_REALLY_COPY, MB_YESNO) == IDNO)
			//		return FALSE;

			UndoSave();
			BeginWaitCursor();	// copy/cut and paste could take a while, in case of many printsheets
			pTargetPrintSheet->CopyAndPasteFoldSheet(pSourcePrintSheet, (int)di.m_pData, bSourceInnerForm, dropTargetInfo, bTargetBackSide, bAutoAssign, pSpreadObject);
			if (bAutoAssign)
				pDoc->m_PrintSheetList.CopyAndPasteFoldSheet(pTargetPrintSheet, (int)di.m_pData);
			EndWaitCursor();
		}
		else
		{
			//if (bFurtherQuestions)
			//	if (AfxMessageBox(IDS_FOLDSHEET_REALLY_MOVE, MB_YESNO) == IDNO)
			//		return FALSE;

			BOOL bReverse = (pSourcePrintSheet->m_nPrintSheetNumber <				// Determine direction of foldsheet move
							 pTargetPrintSheet->m_nPrintSheetNumber) ? TRUE : FALSE;
			UndoSave();
			BeginWaitCursor();
			pTargetPrintSheet->CutAndPasteFoldSheet (pSourcePrintSheet, (int)di.m_pData, bSourceInnerForm, dropTargetInfo, bTargetBackSide, bAutoAssign, pSpreadObject);
			if (bAutoAssign)
				pDoc->m_PrintSheetList.CutAndPasteFoldSheet(pTargetPrintSheet, bReverse);
			EndWaitCursor();
		}

		pDoc->SetModifiedFlag();										
	}
	else
	{
		if ( ! closestDI.IsNull() && (nSide != LEFT) && (nSide != RIGHT))
		{
			UndoSave();

			CPrintSheet* pSourcePrintSheet = (CPrintSheet*)di.m_pAuxData;
			CPrintSheet* pTargetPrintSheet = (CPrintSheet*)closestDI.m_pAuxData;
			BOOL		 bSourceInnerForm  = (di.m_AuxRect.Width()		  <= 2) ? TRUE : FALSE; // trick: use auxrect width to indicate if source is outer or inner form.
			BOOL		 bTargetBackSide   = (targetDI.m_AuxRect.Height() <= 2) ? TRUE : FALSE; // trick: use auxrect height to indicate if target is on layout backside
			CFoldSheet*  pSourceFoldSheet  = pSourcePrintSheet->GetFoldSheet((int)di.m_pData);
			CPrintSheet  newPrintSheet;																					
			CFoldSheetDropTargetInfo dropTargetInfo(CFoldSheetDropTargetInfo::DropTargetLayoutObject, pTargetPrintSheet, NULL, NULL, -1, 0.0f, 0.0f, 0.0f, 0.0f, -1, -1, FALSE);

			newPrintSheet.Create(CFoldSheetLayerRef(-1, -1), -1, (pSourceFoldSheet) ? pSourceFoldSheet->m_nProductPartIndex : 0);	
			if (nSide == BOTTOM)
				pTargetPrintSheet = pDoc->m_PrintSheetList.InsertBefore(pTargetPrintSheet, newPrintSheet);
			else
				pTargetPrintSheet = pDoc->m_PrintSheetList.InsertAfter(pTargetPrintSheet, newPrintSheet);

			if (dropEffect == DROPEFFECT_COPY)
			{
				//if (bFurtherQuestions)
				//	if (AfxMessageBox(IDS_FOLDSHEET_REALLY_COPY, MB_YESNO) == IDNO)
				//		return FALSE;

				BeginWaitCursor();
				pTargetPrintSheet->CopyAndPasteFoldSheet(pSourcePrintSheet, (int)di.m_pData, bSourceInnerForm, dropTargetInfo, bTargetBackSide, FALSE, NULL);
				EndWaitCursor();
			}
			else
			{
				if (bFurtherQuestions)
					if (AfxMessageBox(IDS_FOLDSHEET_REALLY_MOVE, MB_YESNO) == IDNO)
						return FALSE;

				BeginWaitCursor();
				pTargetPrintSheet->CutAndPasteFoldSheet (pSourcePrintSheet, (int)di.m_pData, bSourceInnerForm, dropTargetInfo, bTargetBackSide, FALSE, NULL);
				EndWaitCursor();
			}

			pDoc->SetModifiedFlag();										
		}
	}

	if (pDoc->IsModified())
	{
		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	// because while drawing printsheets GetAct...() calls will be made
		Invalidate();
		UpdateWindow();

		pDoc->UpdateAllViews(NULL);
	}

	return TRUE;
}

BOOL CPrintSheetPlanningView::MoveCopyLayout(CDisplayItem& sourceDI, CDisplayItem& targetDI, int nSide, DROPEFFECT /*dropEffect*/)
{
	CImpManDoc*  pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return FALSE;

	if (nSide <= 0)		// dropped on top of a sheet - not between two ones
	{
		CPrintSheet* pSourcePrintSheet  = (CPrintSheet*)sourceDI.m_pAuxData;
		CPrintSheet* pTargetPrintSheet  = (CPrintSheet*)targetDI.m_pAuxData;

		CString strMsg;
		if ( ! pTargetPrintSheet->IsSuitableLayout(pSourcePrintSheet->GetLayout()))
		{
			AfxFormatString2(strMsg, IDS_LAYOUT_NOT_SUITABLE, pTargetPrintSheet->GetLayout()->m_strLayoutName, 
															  pSourcePrintSheet->GetLayout()->m_strLayoutName); 
			AfxMessageBox(strMsg);
			return FALSE;
		}

		int nRet;
		if (pTargetPrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
		{
			AfxFormatString2(strMsg, IDS_REPLACE_LAYOUT_ALL_PROMPT, pTargetPrintSheet->GetLayout()->m_strLayoutName, 
																	pSourcePrintSheet->GetLayout()->m_strLayoutName); 
			nRet = AfxMessageBox(strMsg, MB_YESNOCANCEL);

			if (nRet == IDCANCEL)
				return FALSE;
		}
		else
		{
			AfxFormatString2(strMsg, IDS_REPLACE_LAYOUT_PROMPT, pTargetPrintSheet->GetLayout()->m_strLayoutName, 
																pSourcePrintSheet->GetLayout()->m_strLayoutName); 
			nRet = AfxMessageBox(strMsg, MB_YESNO);
			if (nRet == IDNO)
				return FALSE;
		}

		switch (nRet)
		{
		case IDYES: // replace all -> old target layout needs to be removed
			{
				int		 nTargetLayoutIndex = pTargetPrintSheet->m_nLayoutIndex;
				POSITION pos			    = pDoc->m_PrintSheetList.GetHeadPosition();
				while (pos)
				{
					CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
					if (rPrintSheet.m_nLayoutIndex == nTargetLayoutIndex)
						rPrintSheet.m_nLayoutIndex = pSourcePrintSheet->m_nLayoutIndex;
				}

				// remove old target layout
				pos = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nTargetLayoutIndex);
				pDoc->m_PrintSheetList.m_Layouts.RemoveAt(pos);	// overloaded version recalcs indices automatically

				pos = pDoc->m_printingProfiles.FindIndex(nTargetLayoutIndex);
				pDoc->m_printingProfiles.RemoveAt(pos);
				pDoc->m_printGroupList.RemoveAt(nTargetLayoutIndex);
			}
			break;
		case IDNO:
			pTargetPrintSheet->m_nLayoutIndex = pSourcePrintSheet->m_nLayoutIndex;
			break;
		}

		pDoc->SetModifiedFlag();										
	}
	else	// dropped between two sheets
	{
		if (!targetDI.IsNull() && (nSide != LEFT) && (nSide != RIGHT))
		{
			CPrintSheet* pSourcePrintSheet = (CPrintSheet*)sourceDI.m_pAuxData;
			CPrintSheet* pTargetPrintSheet = (CPrintSheet*)targetDI.m_pAuxData;
			if (nSide == TOP)
				pDoc->m_PrintSheetList.InsertBefore(pTargetPrintSheet, *pSourcePrintSheet);
			else
				pDoc->m_PrintSheetList.InsertAfter (pTargetPrintSheet, *pSourcePrintSheet);

			pDoc->m_PrintSheetList.RemoveAt(pSourcePrintSheet);

			pDoc->SetModifiedFlag();										
		}
	}

	if (pDoc->IsModified())
	{
		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL, TRUE);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL, TRUE);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL, TRUE);	// because while drawing printsheets GetAct...() calls will be made
		m_DisplayList.Invalidate();
		Invalidate();
		UpdateWindow();
	}

	return TRUE;
}


///////////////////////////////////////////////////////////////////
// bitmap save/restore
void CPrintSheetPlanningView::SaveBitmap(CBitmap* pBitmap, const CRect& rect)
{
	if (!pBitmap)
		return;

	pBitmap->DeleteObject();

	CClientDC dc(this);
 	CDC		  memDC;
	memDC.CreateCompatibleDC(&dc);
	pBitmap->CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(pBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), &dc, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void CPrintSheetPlanningView::RestoreBitmap(CBitmap* pBitmap, const CRect& rect)
{
	if (!pBitmap)
		return;

	CClientDC dc(this);
	CDC		  memDC;
	memDC.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = memDC.SelectObject(pBitmap);
	dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void CPrintSheetPlanningView::SaveBackground(CDC* pDC, const CRect& rect)
{
	if (m_pBkBitmap)
	{
		m_pBkBitmap->DeleteObject();
		delete m_pBkBitmap;
	}
	m_pBkBitmap = new CBitmap;

 	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	m_pBkBitmap->CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBkBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void CPrintSheetPlanningView::RestoreBackground(CDC* pDC, const CRect& rect)
{
	if (!m_pBkBitmap)
		return;

	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBkBitmap);
	pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();

	m_pBkBitmap->DeleteObject();
	delete m_pBkBitmap;
	m_pBkBitmap = NULL;

	ValidateRect(rect);
}

///////////////////////////////////////////////////////////////////

void CPrintSheetPlanningView::UndoSave()
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	theApp.m_undoBuffer.SeekToBegin();
	CArchive archive(&theApp.m_undoBuffer, CArchive::store);
	theApp.m_undoBuffer.SetFilePath(_T("UndoRestore"));

	pDoc->Serialize(archive);

	archive.Close();

	//GetOrderDataFrame()->m_bUndoActive = TRUE;
}

void CPrintSheetPlanningView::UndoRestore()
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (theApp.m_undoBuffer.GetLength() <= 0)
		return;

	theApp.m_undoBuffer.SeekToBegin();
	CArchive archive(&theApp.m_undoBuffer, CArchive::load);
	theApp.m_undoBuffer.SetFilePath(_T("UndoRestore"));

	m_DisplayList.Invalidate();
	m_DisplayList.Clear();

	pDoc->DeleteContents();
	pDoc->Serialize(archive);

	archive.Close();
	theApp.m_undoBuffer.SetLength(0);

	pDoc->SetModifiedFlag(FALSE);

	pDoc->m_PageTemplateList.InvalidateProductRefs();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView), NULL, 0, NULL, TRUE);	// invalidate display list
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	// reorganize pointers to foldsheets in table
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView), NULL, 0, NULL, TRUE);		// invalidate display list

	pDoc->UpdateAllViews(NULL);
}

int CPrintSheetPlanningView::GetPrintGroupIndex()
{
	CFoldSheet* pFoldSheet = GetFoldSheet();
	return (pFoldSheet) ? pFoldSheet->m_nProductPartIndex : 0;
}

CFoldSheet* CPrintSheetPlanningView::GetFoldSheet()
{
	if (m_DisplayList.IsInvalid())		// if UndoRestore() happens before, display list is invalid since foldsheet pointer has changed, so reinit (this method needs to be improved later)
	{
		int nIndex = GetSelectedComponentIndex();
		UpdateWindow();
		SelectComponentByIndex(nIndex);
	}

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if ( ! pDI)
	{
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages));
		if (pDI)
		{
			CFoldSheet*	pDummyFoldSheet = new CFoldSheet;
			pDummyFoldSheet->m_nProductPartIndex = (int)pDI->m_pData;
			return pDummyFoldSheet;
		}
		else
			return NULL;
	}
	else
		return (CFoldSheet*)pDI->m_pData;
}

CFoldSheet* CPrintSheetPlanningView::GetPrevFoldSheet(CFoldSheet* pFoldSheet)
{
	if (m_DisplayList.IsInvalid())		// if UndoRestore() happens before, display list is invalid since foldsheet pointer has changed, so reinit (this method needs to be improved later)
	{
		int nIndex = GetSelectedComponentIndex();
		UpdateWindow();
		SelectComponentByIndex(nIndex);
	}

	CDisplayItem* pPrevDI = NULL;
	CDisplayItem* pDI	  = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	while (pDI)
	{
		if ((CFoldSheet*)pDI->m_pData == pFoldSheet)
			return (pPrevDI) ? (CFoldSheet*)pPrevDI->m_pData : NULL;

		pPrevDI = pDI;
		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	}

	return NULL;
}

CFoldSheet* CPrintSheetPlanningView::GetNextFoldSheet(CFoldSheet* pFoldSheet)
{
	if (m_DisplayList.IsInvalid())		// if UndoRestore() happens before, display list is invalid since foldsheet pointer has changed, so reinit (this method needs to be improved later)
	{
		int nIndex = GetSelectedComponentIndex();
		UpdateWindow();
		SelectComponentByIndex(nIndex);
	}

	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	while (pDI)
	{
		if ((CFoldSheet*)pDI->m_pData == pFoldSheet)
		{
			pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
			if (pDI)
				return (CFoldSheet*)pDI->m_pData;
			else
				return NULL;
		}
		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	}

	return NULL;
}

int	CPrintSheetPlanningView::GetLayerIndex()
{
	if (m_DisplayList.IsInvalid())		// if UndoRestore() happens before, display list is invalid since foldsheet pointer has changed, so reinit (this method needs to be improved later)
	{
		int nIndex = GetSelectedComponentIndex();
		UpdateWindow();
		SelectComponentByIndex(nIndex);
	}

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if ( ! pDI)
		return NULL;
	return (int)pDI->m_pAuxData;
}

CPrintSheet* CPrintSheetPlanningView::GetPrintSheet()
{
	CFoldSheet* pFoldSheet  = GetFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			return (pDoc) ? pDoc->m_PrintSheetList.GetBlankSheet(pFoldSheet->m_nProductPartIndex) : NULL;
		}
		else
			return pFoldSheet->GetPrintSheet(GetLayerIndex());
	else
	{
		CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent));
		if (pDI)
			return (CPrintSheet*)pDI->m_pAuxData;
		else
			return NULL;
	}
}

CPageTemplate* CPrintSheetPlanningView::GetPageTemplate()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	if ( ! pDI)
		return NULL;
	else
		return (CPageTemplate*)pDI->m_pData;
}

void CPrintSheetPlanningView::OnNcMouseMove(UINT nHitTest, CPoint point)
{
	// TODO: F�gen Sie hier Ihren Meldungsbehandlungscode ein, und/oder benutzen Sie den Standard.

	CScrollView::OnNcMouseMove(nHitTest, point);
}

void CPrintSheetPlanningView::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{
	//if (bCalcValidRects && GetPrintSheet())
	//{
	//	lpncsp->rgrc[0].right = 860;		
	//}

	CScrollView::OnNcCalcSize(bCalcValidRects, lpncsp);
}

void CPrintSheetPlanningView::OnNcPaint()
{
	 CScrollView::OnNcPaint();// soll zum Zeichnen von Meldungen nicht aufgerufen werden.
}

void CPrintSheetPlanningView::OnProductGroupRules()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgProductionSetups dlg;
	dlg.m_strName		  = pDoc->m_productionSetup.m_strName;
	dlg.m_productionSetup = pDoc->m_productionSetup;
	if (dlg.DoModal() == IDCANCEL)
		return;

	theApp.UndoSave();

	pDoc->m_productionSetup = dlg.m_productionSetup;

	CProductionProfile productionProfile = theApp.m_productionProfiles.FindProfile(dlg.m_productionSetup.m_strProductionProfile);
	POSITION pos = pDoc->m_printingProfiles.GetHeadPosition();
	while (pos)
	{
		CPrintingProfile& rProfile		= pDoc->m_printingProfiles.GetNext(pos);
		CPrintingProfile oldProfile 	= rProfile;

		rProfile.AssignProductionProfilePool(productionProfile);
		rProfile.ApplyModifications(oldProfile);
	}

	pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		rBoundProduct.AssignProductionProfilePool(productionProfile);
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			CBoundProductPart& rProductPart = rBoundProduct.m_parts[i];
			rProductPart.AssignProductionProfilePool(productionProfile);
		}
	}

	pDoc->SetModifiedFlag(TRUE);
	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
}

void CPrintSheetPlanningView::OnDoImpose()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgOptimizationMonitor dlg;
	if ( (strlen(theApp.settings.m_szSPrintOneTenantID) <= 0) || (strlen(theApp.settings.m_szSPrintOneUserID) <= 0) || (strlen(theApp.settings.m_szSPrintOnePassword) <= 0) ) 
	{
		dlg.Create(IDD_OPTIMIZATION_MONITOR);
		dlg.OptimizeAll();
		dlg.DestroyWindow();
	}
	else
	{
		dlg.m_bRunModal = TRUE;
		dlg.Create(IDD_OPTIMIZATION_MONITOR);
		dlg.OptimizeAll();
		dlg.RunModalLoop();
	}

	pDoc->UpdateAllViews(NULL);
}

void CPrintSheetPlanningView::OnDoImposeRemoveAll()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strMsg;
	strMsg.LoadString(IDS_IMPOSE_REMOVE_ALL);
	if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
		return;

	UndoSave();

	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		rLayout.RemovePrintSheetInstance(INT_MAX, TRUE);
	}

	pDoc->m_Bookblock.Reorganize(NULL, NULL);
	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();

	pDoc->SetModifiedFlag();										

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	theApp.OnUpdateView(RUNTIME_CLASS(CProductsView),				NULL, 0, NULL);
}

void CPrintSheetPlanningView::ImposeLabels()
{
	if (GetParentFrame()->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))	
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			pView->UndoSave();
	}
	else
		UndoSave();

	CDlgOptimizationMonitor dlg;
	dlg.Create(IDD_OPTIMIZATION_MONITOR);
	dlg.OptimizeAll();
	dlg.DestroyWindow();
}

void CPrintSheetPlanningView::OnUpdateProductGroupRules(CCmdUI* pCmdUI)
{
	//if (NoFoldSheetComponents() && NoLabelComponents())
	//	m_planningToolBar.ShowButton(ID_DO_IMPOSE, SW_HIDE);

	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);//(GetSelectedComponentIndex() >= 0) ? TRUE : FALSE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetPlanningView::OnUpdateDoImpose(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	COwnerDrawnButtonList::Show(pCmdUI, (pDoc->m_printGroupList.GetSize() <= 0) ? SW_HIDE : SW_SHOW);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetPlanningView::OnUpdateDoImposeRemoveAll(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	COwnerDrawnButtonList::Show(pCmdUI, (pDoc->m_printGroupList.GetSize() <= 0) ? SW_HIDE : SW_SHOW);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetPlanningView::OnDeleteSelection() 
{
	//if (GetParentFrame()->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
	//	return;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout));
	if (pDI)
	{
		RemovePrintSheet((CPrintSheet*)pDI->m_pAuxData);
		return;
	}

	pDI = m_DisplayList.GetFirstSelectedItem();
	if (pDI)
	{
		if (AfxMessageBox(IDS_DELETE_SELECTIONS_WARNING, MB_YESNO) == IDNO)
			return;
	}

	BOOL		 bSaved				= FALSE;
	int			 nRet				= -1;
	BOOL		 bFoldSheetDeleted	= FALSE;
	BOOL		 bObjectDeleted		= FALSE;
	CImpManDoc*	 pDoc				= CImpManDoc::GetDoc();
	CLayout*	 pLayout			= NULL;
	CLayoutSide* pLayoutSide		= NULL;
	int			 nDeleteSide		= -1;
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet)))
		{
			if ( ! bSaved)
			{
				UndoSave();
				bSaved = TRUE;
			}

			pLayout = ((CPrintSheet*)pDI->m_pAuxData) ? ((CPrintSheet*)pDI->m_pAuxData)->GetLayout() : NULL;
			if (((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
			{
				if (nRet == -1)
					nRet = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNO);
				if (nRet == IDYES)
				{
					BOOL bFirst			= TRUE;
					int nLayoutIndex	= ((CPrintSheet*)pDI->m_pAuxData)->m_nLayoutIndex;
					int nNewLayoutIndex = nLayoutIndex;
					POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
					while (pos)
					{
						CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
						if (rPrintSheet.m_nLayoutIndex == nLayoutIndex)
						{
							if (!rPrintSheet.RemoveFoldSheetLayer((int)pDI->m_pData, FALSE, (bFirst) ? TRUE : FALSE, (bFirst) ? FALSE : TRUE) )
								break;
							else
							{
								m_DisplayList.DeselectItemsByData(pDI->m_pData, (void*)&rPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
								bFoldSheetDeleted = TRUE;
							}
							if (bFirst)
								nNewLayoutIndex = rPrintSheet.m_nLayoutIndex;
							else
								rPrintSheet.m_nLayoutIndex = nNewLayoutIndex;
							bFirst = FALSE;
						}
					}
					break;
				}
				else
					bFoldSheetDeleted = ((CPrintSheet*)pDI->m_pAuxData)->RemoveFoldSheetLayer((int)pDI->m_pData, FALSE, FALSE, FALSE);
			}
			else
				bFoldSheetDeleted = ((CPrintSheet*)pDI->m_pAuxData)->RemoveFoldSheetLayer((int)pDI->m_pData, FALSE, TRUE, FALSE);

			if (bFoldSheetDeleted)	// Deselect corresponding display items
			{
				m_DisplayList.DeselectItemsByData(pDI->m_pData, pDI->m_pAuxData, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
				//pLayout = ((CPrintSheet*)pDI->m_pAuxData) ? ((CPrintSheet*)pDI->m_pAuxData)->GetLayout() : NULL;
			}
		}
		else
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutObject)))
			{
				if ( ! bSaved)
				{
					UndoSave();
					bSaved = TRUE;
				}

				pLayoutSide  = ((CLayoutObject*)pDI->m_pData)->GetLayoutSide();
				POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
				while (pos)
				{
					POSITION	   prevPos = pos;
					CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
					if (&rObject == (CLayoutObject*)pDI->m_pData)
					{
						CLayoutObject* pCounterPartObject = pCounterPartObject = rObject.FindCounterpartObject();
						pLayoutSide->m_ObjectList.RemoveAt(prevPos);
						if (pCounterPartObject)
						{
							CLayoutSide* pCounterPartSide = pCounterPartObject->GetLayoutSide();
							POSITION pos = pCounterPartSide->m_ObjectList.GetHeadPosition();
							while (pos)
							{
								POSITION	   prevPos = pos;
								CLayoutObject& rCounterObject = pCounterPartSide->m_ObjectList.GetNext(pos);
								if (&rCounterObject == pCounterPartObject)
									pCounterPartSide->m_ObjectList.RemoveAt(prevPos);
							}
						}

						bObjectDeleted = TRUE;
						break;
					}
				}

				pLayout = pLayoutSide->GetLayout();
			}

		pDI = m_DisplayList.GetNextSelectedItem(pDI);
	}

	if (bFoldSheetDeleted || bObjectDeleted)
	{
		pDoc->m_Bookblock.RemoveUnusedFoldSheets();

		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();

		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

		pDoc->m_MarkSourceList.RemoveUnusedSources();
		
		POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
		while (pos)
		{
			if (&pDoc->m_PrintSheetList.m_Layouts.GetNext(pos) == pLayout)	// layout still there (it is possible that it has been completely removed)
			{
				pLayout->AnalyzeMarks();
				break;
			}
		}

		pDoc->SetModifiedFlag();										

		pDoc->UpdateAllViews(NULL);

		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), NULL, 0, NULL, TRUE);
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	}
}

void CPrintSheetPlanningView::OnUndo() 
{
	UndoRestore();

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));	
}

void CPrintSheetPlanningView::OnLoadStandard()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout));
	if (pDI)
		LoadStandardLayout((CLayout*)pDI->m_pData, (CPrintSheet*)pDI->m_pAuxData);
}

void CPrintSheetPlanningView::OnStoreStandard()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout));
	if (pDI)
		SaveStandardLayout((CLayout*)pDI->m_pData, (CPrintSheet*)pDI->m_pAuxData);
}

void CPrintSheetPlanningView::OnStoreCFF()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (pDoc->m_printGroupList.GetSize() <= 0)
		return;

	CString strInitialFolder = theApp.GetDefaultInstallationFolder();
	CString strFilter, strTitle, strFileName;
	strFilter.LoadString(IDS_CFFFILE_FILTER);
	strTitle.LoadString(IDS_CFF_STORE);

	int nNumLayouts = 0;
	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		if ( ! pDoc->m_PrintSheetList.m_Layouts.GetNext(pos).IsEmpty())
			nNumLayouts++;
	}

	int	nLayoutIndex = 1;
	pos	= pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if (rLayout.IsEmpty())
			continue;

		CFileDialog dlg(FALSE, _T("cf2"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING, strFilter);

		if ( ! pDoc->m_GlobalData.m_strJob.IsEmpty())
			if (nNumLayouts > 1)
				strFileName.Format(_T("%s_%d.cf2"), pDoc->m_GlobalData.m_strJob, nLayoutIndex);
			else
				strFileName = pDoc->m_GlobalData.m_strJob + _T(".cf2");
		else
			strFileName = rLayout.m_strLayoutName + _T(".cf2");
		dlg.m_ofn.lpstrTitle	  = strTitle;
		dlg.m_ofn.lpstrInitialDir = strInitialFolder;
		dlg.m_ofn.lpstrFile		  = strFileName.GetBuffer(MAX_PATH);

		if (dlg.DoModal() == IDCANCEL)
			return;

		CCFFOutputImposition cffOutput;
		cffOutput.DoExport(rLayout, dlg.m_ofn.lpstrFile);

		nLayoutIndex++;
	}
}

void CPrintSheetPlanningView::OnDoNumber()
{
	switch (m_nShowPlanningView)
	{
	case ShowPrintSheetList:	NumberPrintSheets();	break;
	case ShowFoldSheetList:		NumberFoldSheets();		break;
	case ShowPages:				NumberPages();			break;
	}
}

void CPrintSheetPlanningView::OnUpdateDeleteSelection(CCmdUI* pCmdUI) 
{
	m_strippingToolBar.ShowButton(ID_DELETE_SELECTION, (NoFoldSheetComponents()) ? SW_HIDE : SW_SHOW);

	if (m_nShowPlanningView == ShowImposition)
	{
		BOOL bIsSelected = (m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet)) || 
							m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutObject)) ) ? TRUE : FALSE;
		COwnerDrawnButtonList::Enable(pCmdUI, bIsSelected);
	}
	else
		if (m_nShowPlanningView == ShowPrintSheetList)
			COwnerDrawnButtonList::Enable(pCmdUI, (m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout))) ? TRUE : FALSE);
		else
			COwnerDrawnButtonList::Enable(pCmdUI, FALSE);

	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetPlanningView::OnUpdateUndo(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (pDoc->m_printGroupList.GetSize() <= 0)
		COwnerDrawnButtonList::Show(pCmdUI, SW_HIDE);
	else
	{
		COwnerDrawnButtonList::Show(pCmdUI, SW_SHOW);
		COwnerDrawnButtonList::Enable(pCmdUI, (theApp.m_undoBuffer.GetLength() > 0) ? TRUE : FALSE);
		COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	}
}

void CPrintSheetPlanningView::OnUpdateLoadStandard(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, (m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout))) ? TRUE : FALSE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetPlanningView::OnUpdateStoreStandard(CCmdUI* pCmdUI) 
{
	m_planningToolBar.ShowButton(ID_STANDARDLAYOUT_STORE, (NoFoldSheetComponents()) ? SW_HIDE : SW_SHOW);

	//COwnerDrawnButtonList::Enable(pCmdUI, (m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Layout))) ? TRUE : FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetPlanningView::OnUpdateStoreCFF(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetPlanningView::OnUpdateDoNumber(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

BOOL CPrintSheetPlanningView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_planningToolBar.m_pToolTip)            
		m_planningToolBar.m_pToolTip->RelayEvent(pMsg);	
	if (m_strippingToolBar.m_pToolTip)            
		m_strippingToolBar.m_pToolTip->RelayEvent(pMsg);	
	if (m_printSheetListToolBar.m_pToolTip)            
		m_printSheetListToolBar.m_pToolTip->RelayEvent(pMsg);	
	if (m_foldSheetListToolBar.m_pToolTip)            
		m_foldSheetListToolBar.m_pToolTip->RelayEvent(pMsg);	
	if (m_pagesListToolBar.m_pToolTip)            
		m_pagesListToolBar.m_pToolTip->RelayEvent(pMsg);	

	return CScrollView::PreTranslateMessage(pMsg);
}

BOOL CPrintSheetPlanningView::SelectUnassignedPages()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages));
	if (pDI)
	{
		m_DisplayList.DeselectAll();
		pDI->m_nState = CDisplayItem::Selected;

		if ( ! pDI->m_bIsVisible)	
		{
			Invalidate();
			OnHScroll(SB_THUMBTRACK, pDI->m_BoundingRect.left - 20, NULL);
			OnVScroll(SB_THUMBTRACK, pDI->m_BoundingRect.top  - 20, NULL);
		}
		else
		{
			m_DisplayList.InvalidateItem(pDI);
			UpdateWindow();
		}
		return TRUE;
	}
	return FALSE;
}

BOOL CPrintSheetPlanningView::SelectFirstComponent()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if (pDI)
	{
		m_DisplayList.DeselectAll();
		pDI->m_nState = CDisplayItem::Selected;
		m_DisplayList.InvalidateItem(pDI);
		return TRUE;
	}

	return FALSE;
}

void CPrintSheetPlanningView::AddNewPrintSheet(int nPressDeviceIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	//CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.GetBlankSheet(GetPrintGroupIndex());

	//CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;//(pDoc->m_PrintSheetList.GetCount() > 0) ? pDoc->m_PrintSheetList.GetTail().GetLayout() : NULL;
	//if (pLayout)
	//{
	//	if (pLayout->HasPages() || pLayout->HasFlatProducts())
	//		pDoc->m_PrintSheetList.AddBlankSheet(GetPrintGroupIndex());
	//}
	//else
	//	pDoc->m_PrintSheetList.AddBlankSheet(GetPrintGroupIndex());

	//pPrintSheet = pDoc->m_PrintSheetList.GetBlankSheet(GetPrintGroupIndex());
	//pLayout	= (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	//if (! pLayout)
	//	return;

	//CPDFTargetList pdfTargetList;
	//pdfTargetList.Load();
	//pDoc->m_PrintSheetList.m_pdfTargets.RemoveAll();
	//for (int i = 0; i < pdfTargetList.GetSize(); i++)
	//	pDoc->m_PrintSheetList.m_pdfTargets.Add(pdfTargetList.ElementAt(i));
	//pPrintSheet->m_strPDFTarget = pDoc->m_printGroupList[GetPrintGroupIndex()].GetDefaultOutputProfileName();

	//pDoc->SetPrintSheetPressDevice(nPressDeviceIndex, BOTHSIDES, pDoc->m_PrintSheetList.m_Layouts.FindLayout(pLayout->m_strLayoutName));

	//CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
	//if (! pPressDevice)
	//	return;

	//CSheet sheet;
	//if (pDoc->m_printGroupList[GetPrintGroupIndex()].GetDefaultPaperName().IsEmpty())
	//{
	//	if ( ! pPressDevice->FindLargestPaper(sheet))
	//		return;
	//}
	//else
	//	CPressDevice::FindPaper(sheet, pDoc->m_printGroupList[GetPrintGroupIndex()].GetDefaultPaperName());

	//pLayout->AssignSheet(sheet);

	//pDoc->m_PrintSheetList.ReNumberPrintSheets();

	//if (pDoc->m_PrintSheetList.GetCount())
	//{
	//	CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetTail();
	//	CString strMarkset = pDoc->m_printGroupList[GetPrintGroupIndex()].GetProductionProfile().m_strMarkset;
	//	if ( ! strMarkset.IsEmpty())
	//	{
	//		CMarkList markList;
	//		markList.Load(strMarkset);
	//		for (int i = 0; i < markList.GetSize(); i++)
	//			CDlgMarkSet::PositionAndCreateMarkAuto(markList[i], &markList, _T(""), &rPrintSheet);
	//	}
	//}

	//pDoc->SetModifiedFlag();

	//pDoc->UpdateAllViews(NULL);
	//
	//CPrintSheetNavigationView* pNavigationView = GetPrintSheetNavigationView();
	//if (pNavigationView)
	//{
	//	pNavigationView->m_DisplayList.Invalidate();
	//	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	//	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));

	//	if (pNavigationView->GetParentFrame()->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
	//	{
	//		CNewPrintSheetFrame* pNewPrintSheetFrame = (CNewPrintSheetFrame*)pNavigationView->GetParentFrame();
	//		if (pNewPrintSheetFrame)
	//			if (pNewPrintSheetFrame->m_nViewMode != ID_VIEW_ALL_SHEETS)
	//				if (pNavigationView)
	//					pNavigationView->SetActPrintSheet(pPrintSheet);
	//	}
	//}

	//if (m_dlgFoldSchemeSelector.m_hWnd)		// is open
	//	m_dlgFoldSchemeSelector.InitData();	

	////m_pPrintSheet	= pPrintSheet;
	////m_pLayout		= pLayout;
	////m_pPressDevice	= (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	////m_pFoldSheet	= NULL;
	////m_nLayerIndex	= 0;
	//theApp.UpdateView(RUNTIME_CLASS(CProductsView));
}

void CPrintSheetPlanningView::AddNewPrintSheetPickupPressDevice(CPoint ptPos)
{
	CRect rcWin;
	m_dlgPressDeviceSelector.m_pParent	  			= this;
	m_dlgPressDeviceSelector.m_pPressDevice 		= GetPressDevice();
	m_dlgPressDeviceSelector.m_pLayout	  			= GetLayout();
	m_dlgPressDeviceSelector.m_pFnSelChangeHandler	= &OnSelchangePressDeviceSelector;
	if ( ! m_dlgPressDeviceSelector.m_hWnd)
	{
		m_dlgPressDeviceSelector.Create(IDD_SELECT_PRESSDEVICE_POPUP, this);
		//m_dlgPressDeviceSelector.GetWindowRect(rcWin);
		//m_dlgPressDeviceSelector.MoveWindow(ptPos.x, ptPos.y, rcWin.Width(), rcWin.Height());
		m_dlgPressDeviceSelector.CenterWindow();
		m_dlgPressDeviceSelector.ShowWindow(SW_SHOW);
	}
	else
	{
		//m_dlgPressDeviceSelector.GetWindowRect(rcWin);
		//m_dlgPressDeviceSelector.MoveWindow(ptPos.x, ptPos.y, rcWin.Width(), rcWin.Height());
		m_dlgPressDeviceSelector.CenterWindow();
		m_dlgPressDeviceSelector.ShowWindow(SW_SHOW);
	}
}

void CPrintSheetPlanningView::OnSelchangePressDeviceSelector(CPressDevice* /*pNewPressDevice*/, int nPressDeviceIndex, CDlgSelectPressDevicePopup* pDlgSelector)
{
	CPrintSheetPlanningView* pView = (CPrintSheetPlanningView*)pDlgSelector->m_pParent;
	pView->AddNewPrintSheet(nPressDeviceIndex);
	
	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( pDoc)
	//{
	//	CPrintGroup* pProductGroup = pDoc->m_printGroupList.GetPart(pView->GetPrintGroupIndex());
	//	if (pProductGroup)
	//		pProductGroup->SetDefaultPressDeviceName((pView->GetPressDevice()) ? pView->GetPressDevice()->m_strName : _T(""));
	//}

	pView->OnDoImpose();
}

CLayout* CPrintSheetPlanningView::GetLayout()
{
	CFoldSheet* pFoldSheet  = GetFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
		{
			CPrintSheet* pPrintSheet = GetPrintSheet();
			return (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		}

	if (pFoldSheet)
		return pFoldSheet->GetLayout(GetLayerIndex());
	else
		return NULL;
}

CPressDevice* CPrintSheetPlanningView::GetPressDevice()
{
	CFoldSheet* pFoldSheet  = GetFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
		{
			CLayout* pLayout = GetLayout();
			return (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
		}

	if (pFoldSheet)
	{
		CLayout* pLayout = pFoldSheet->GetLayout(GetLayerIndex());
		return (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	}
	else
		return NULL;
}

CPrintSheetView* CPrintSheetPlanningView::GetPrintSheetView()
{
	if ( ! GetParent())
		return NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		return NULL;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetView();

	return NULL;
}

CPrintSheetPlanningView* CPrintSheetPlanningView::GetPrintSheetPlanningView()
{
	return this;

	//if ( ! GetParent())
	//	return NULL;
	//CFrameWnd* pFrame = GetParent()->GetParentFrame();
	//if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
	//	return ((COrderDataFrame*)pFrame)->GetPrintSheetPlanningView();
	//else
	//	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
	//		return NULL;

	//return NULL;
}

CPrintSheetNavigationView* CPrintSheetPlanningView::GetPrintSheetNavigationView()
{
	if ( ! GetParent())
		return NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		return NULL;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetNavigationView();

	return NULL;
}

CPrintSheetViewStrippingData* CPrintSheetPlanningView::GetPrintSheetViewStrippingData()
{
	if ( ! GetParent())
		return NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		return NULL;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetViewStrippingData();

	return NULL;
}

CProductsView* CPrintSheetPlanningView::GetProductsView()
{
	if ( ! GetParent())
		return NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		return ((COrderDataFrame*)pFrame)->GetProductsView();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetProductsView();

	return NULL;
}

void CPrintSheetPlanningView::OnCommandNextStep()
{
	if ( ! SelectUnassignedPages())
		return;

	OnDoImpose();
}

BOOL CPrintSheetPlanningView::NoFoldSheetComponents()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintFoldSheetComponent));
	if ( ! pDI)
		return TRUE;
	else
		return FALSE;
}

BOOL CPrintSheetPlanningView::NoLabelComponents()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent));
	if ( ! pDI)
		pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedLabel));
	if ( ! pDI)
		return TRUE;
	else
		return FALSE;
}

void CPrintSheetPlanningView::LoadStandardLayout(CLayout* pLayout, CPrintSheet* pPrintSheet)
{
	CString strFilter, strTitle;
	CFile file;
	CFileException fileException;

	strFilter.LoadString(IDS_LOAD_STANDARD_FILTER);
	CFileDialog dlg(TRUE, _T("lay"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING,
					strFilter);

	if ( ! pLayout)
	{
		AfxMessageBox(IDS_NOLAYOUT_CHOOSED_FORLOAD, MB_OK);
		return;
	}

	//strTitle.Format(IDS_LOADSTANDARD_TITLE, pLayout->m_strLayoutName);
	strTitle.LoadString(IDS_LOADSTANDARD_TITLE);
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = theApp.settings.m_szStandardsDir;
	if (dlg.DoModal() == IDCANCEL)
		return;

	if (!file.Open(dlg.m_ofn.lpstrFile, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", dlg.m_ofn.lpstrFile, fileException.m_cause);
		return; 
	}
	
	// check for layout with same name as desired standard
	// if there is any, this is not permitted
	CImpManDoc* pDoc		  = ((CImpManDoc*)CImpManDoc::GetDoc());
	CString		strLayoutName = ExtractStandardName(dlg.m_ofn.lpstrFileTitle);
	POSITION	pos		      = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if (&rLayout != pLayout)
			if (rLayout.m_strLayoutName.CompareNoCase(strLayoutName) == 0)
			{
				CString strMsg;
				AfxFormatString1(strMsg, IDS_REPLACE_LAYOUT_NAME_EXISTING, strLayoutName); 
				AfxMessageBox(strMsg);
				return;
			}
	}

	CString strMsg;
	int		nRet;
	if (pPrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 1)	// if more than one sheet based on that layout exists, 
	{																// ask if all of them to be replaced or only the selected
		AfxFormatString2(strMsg, IDS_REPLACE_LAYOUT_ALL_PROMPT, pLayout->m_strLayoutName, 
																dlg.m_ofn.lpstrFile); 
		nRet = AfxMessageBox(strMsg, MB_YESNOCANCEL);

		if (nRet == IDCANCEL)
			return;
	}
	else
	{
		AfxFormatString2(strMsg, IDS_REPLACE_LAYOUT_PROMPT, pLayout->m_strLayoutName,	// if there is only one sheet based on that layout,
															dlg.m_ofn.lpstrFile);		// ask if really want to replace.
		nRet = AfxMessageBox(strMsg, MB_YESNO);
		if (nRet == IDNO)
			return;
	}

	UndoSave();

	CArchive archive(&file, CArchive::load);
	CLayout			  layout;
	CPageTemplateList markTemplateList;
	CPageSourceList	  markSourceList;
	CPressDevice	  pressDeviceFront, pressDeviceBack;

	SerializeElements(archive, &layout, 1);
	markTemplateList.Serialize(archive);
	markSourceList.Serialize(archive);

	// load press device(s)
	SerializeElements(archive, &pressDeviceFront, 1);
	SerializeElements(archive, &pressDeviceBack,  1);

	if (pPrintSheet->IsSuitableLayout(&layout))
	{
		pDoc->m_MarkSourceList.MergeElements  (markSourceList,	 markTemplateList);	
 		pDoc->m_MarkTemplateList.MergeElements(markTemplateList, layout);

		BOOL bFrontRenamed = FALSE, bBackRenamed = FALSE;
		//if (pressDeviceFront == pressDeviceBack)
		//{
			//layout.m_FrontSide.m_nPressDeviceIndex = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.AddDevice(pressDeviceFront, bFrontRenamed);
			//layout.m_BackSide.m_nPressDeviceIndex  = layout.m_FrontSide.m_nPressDeviceIndex;
			//layout.GetProductionProfile().m_press.m_pressDevice		  = pressDeviceFront;
			//layout.GetProductionProfile().m_press.m_backSidePressDevice = pressDeviceBack;
		//}
		//else
		//{
		//	layout.m_FrontSide.m_nPressDeviceIndex = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.AddDevice(pressDeviceFront, bFrontRenamed);
		//	layout.m_BackSide.m_nPressDeviceIndex  = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.AddDevice(pressDeviceBack,  bBackRenamed);
		//}

		if (bFrontRenamed || bBackRenamed)
			AfxMessageBox(IDS_PRESSDEVICE_RENAMED);

		switch (nRet)
		{
		case IDYES: pLayout->Copy(layout); 
					pLayout->UpdatePressParams(pressDeviceFront, pressDeviceBack);
					break;	// replace
		case IDNO:
					{														// keep old layout for rest of sheets, based on that layout
						pDoc->m_PrintSheetList.m_Layouts.AddTail(layout);	// add new layout for the selected sheet
						pPrintSheet->m_nLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;
						pDoc->m_printingProfiles.AddTail(CPrintingProfile());
						pDoc->m_PrintSheetList.m_Layouts.GetTail().UpdatePressParams(pressDeviceFront, pressDeviceBack);
						pDoc->m_PrintSheetList.ReNumberPrintSheets();
					}
					break;
		}

		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results

		pDoc->ReorganizePressDeviceIndices(BOTHSIDES); // Look for double or unused entries

		if (pPrintSheet->GetLayout())
			pPrintSheet->GetLayout()->SetPrintSheetDefaultPlates();	// set all printsheet default plates to layout default plates

		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates(TRUE);	// TRUE = use plate defaults
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results
		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results

		pDoc->SetModifiedFlag();										
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL);	
	}
	else
	{
		CString strMsg;
		AfxFormatString2(strMsg, IDS_LAYOUT_NOT_SUITABLE, pLayout->m_strLayoutName, 
														  layout.m_strLayoutName); 
		AfxMessageBox(strMsg);
	}

	archive.Close();
	file.Close();
}

void CPrintSheetPlanningView::SaveStandardLayout(CLayout* pLayout, CPrintSheet* pPrintSheet)
{
	CString strFilter, strTitle;
	CFile file;
	CFileException fileException;

	strFilter.LoadString(IDS_LOAD_STANDARD_FILTER);
	CFileDialog dlg(FALSE, _T("lay"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING,
					strFilter);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pLayout)
	{
		AfxMessageBox(IDS_NOLAYOUT_CHOOSED_FORSTORE, MB_OK);
		return;
	}

	//strTitle.Format(IDS_STORESTANDARD_TITLE, pLayout->m_strLayoutName);
	strTitle.LoadString(IDS_STORESTANDARD_TITLE);
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = theApp.settings.m_szStandardsDir;
	if (dlg.DoModal() == IDCANCEL)
		return;

	if (!file.Open(dlg.m_ofn.lpstrFile, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", dlg.m_ofn.lpstrFile, fileException.m_cause);
		return; 
	}

	CArchive archive(&file, CArchive::store);
	CLayout  layout;
	layout.Copy(*pLayout);
	// set indices to unassigned
	layout.m_FrontSide.m_nPressDeviceIndex = -1;
	layout.m_BackSide.m_nPressDeviceIndex  = -1;
	// update layoutname

	layout.m_strLayoutName	 = ExtractStandardName(dlg.m_ofn.lpstrFileTitle);	
	pLayout->m_strLayoutName = layout.m_strLayoutName;

	// save actual printsheet default plates 
	layout.m_FrontSide.m_DefaultPlates.RemoveAll();
	layout.m_BackSide.m_DefaultPlates.RemoveAll();
	layout.m_FrontSide.m_DefaultPlates.AddTail(pPrintSheet->m_FrontSidePlates.m_defaultPlate);
	layout.m_BackSide.m_DefaultPlates.AddTail (pPrintSheet->m_BackSidePlates.m_defaultPlate);

	CPageTemplateList markTemplateList;
	CPageSourceList	  markSourceList;

	layout.ExtractMarkTemplates		   (markTemplateList, pDoc->m_MarkTemplateList);
	markTemplateList.ExtractMarkSources(markSourceList,   pDoc->m_MarkSourceList);

	SerializeElements(archive, &layout, 1);
	markTemplateList.Serialize(archive);
	markSourceList.Serialize(archive);

	// serialize press device(s)
	SerializeElements(archive, pLayout->m_FrontSide.GetPressDevice(), 1);
	SerializeElements(archive, pLayout->m_BackSide.GetPressDevice(),  1);

	archive.Close();
	file.Close();

	pDoc->SetModifiedFlag();										
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	// update views, because layout name has changed
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL);	
}	

// remove extension from filename
CString CPrintSheetPlanningView::ExtractStandardName(CString strFileTitle)
{
	CString string = strFileTitle; string.MakeLower();
	int nIndex = string.Find(_T(".lay"));	
	return (nIndex != -1) ? strFileTitle.Left(nIndex) : strFileTitle;
}


CPrintSheetPlanningView* g_pThisPrintSheetPlanningView = NULL;

////////////////////////// print sheet numbering /////////////////////////////////////////////////////////////////////////////////////////
void CPrintSheetPlanningView::NumberPrintSheets() 
{
	g_pThisPrintSheetPlanningView = this;

	if (g_pThisPrintSheetPlanningView)
		g_pThisPrintSheetPlanningView->m_DisplayList.Invalidate();

	UndoSave();

	if (InitPrintSheetNumerationDialog(&m_dlgNumeration))
		if ( ! m_dlgNumeration.m_hWnd)
		{
			m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
			if ( ! GetPrintSheet())
				m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionAll;
			m_dlgNumeration.Create(IDD_NUMERATION_DIALOG, this);
			m_dlgNumeration.GetDlgItem(IDC_SELECT_PRODUCTGROUP)->ShowWindow(SW_HIDE);
			m_dlgNumeration.GetDlgItem(IDC_SELECT_PRODUCTGROUP_COMBO)->ShowWindow(SW_HIDE);
			m_dlgNumeration.m_nRangeFrom = 1;
			if (CImpManDoc::GetDoc())
				m_dlgNumeration.m_nRangeTo = CImpManDoc::GetDoc()->m_PrintSheetList.GetCount();
			else
				m_dlgNumeration.m_nRangeTo = 1;
	
			CRect rcWin;
			m_dlgNumeration.GetWindowRect(rcWin);
			CRect rcTarget; GetClientRect(rcTarget);
			ClientToScreen(rcTarget);
			m_dlgNumeration.MoveWindow(rcTarget.left + 200, rcTarget.top + 50, rcWin.Width(), rcWin.Height());

			OnUpdate(NULL, 0, NULL);
			Invalidate();
			UpdateWindow();
		}
}

BOOL CPrintSheetPlanningView::InitPrintSheetNumerationDialog(CDlgNumeration* pDlg)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	
	if (pDlg->m_nSelection == CDlgNumeration::SelectionAll)
		g_pThisPrintSheetPlanningView->SelectAllPrintSheets();
	else
		if (pDlg->m_nSelection == CDlgNumeration::SelectionRange)
			g_pThisPrintSheetPlanningView->SelectRangePrintSheets(pDlg->m_nRangeFrom, pDlg->m_nRangeTo);
		//else
		//	if (pDlg->m_nSelection == CDlgNumeration::SelectionProductGroup)
		//		g_pThisPrintSheetPlanningView->SelectProductGroupFoldSheets(pDlg->m_nProductPartIndex);

	pDlg->m_nProductPartIndex = -1;

	CDisplayItem*  pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));

	pDlg->m_strTitle.LoadString(IDS_PRINTSHEETNUMERATION_TITLE);
	pDlg->m_pfnApplyCallback = ChangePrintSheetNumeration;
	pDlg->m_pfnInitCallback  = InitPrintSheetNumerationDialog;
	if (pDI)
	{
		CPrintSheet* pFirstPrintSheet = (CPrintSheet*)pDI->m_pData;
		pDlg->m_nRangeFrom = (int)pDI->m_pAuxData + 1;

		pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetLastSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
		if (pDI)
		{
			CPrintSheet* pLastPrintSheet = (CPrintSheet*)pDI->m_pData;
			pDlg->m_nRangeTo   = (int)pDI->m_pAuxData + 1;
			pDlg->m_nStartNum  = (pDlg->m_bReverse) ? pLastPrintSheet->GetNumberOnly() : pFirstPrintSheet->GetNumberOnly();
		}
	}
	else
	{
		pDlg->m_nRangeFrom = 1; 
		pDlg->m_nRangeTo   = pDoc->m_PrintSheetList.GetCount();

		pDlg->m_nStartNum = 1;
		if (pDoc->m_PrintSheetList.GetCount())
		{
			CPrintSheet& rFirstPrintSheet = pDoc->m_PrintSheetList.GetHead();
			CPrintSheet& rLastPrintSheet  = pDoc->m_PrintSheetList.GetTail();
			pDlg->m_nStartNum = (pDlg->m_bReverse) ? rLastPrintSheet.GetNumberOnly() : rFirstPrintSheet.GetNumberOnly();
		}
	}

	pDlg->m_pViewToUpdate1 = g_pThisPrintSheetPlanningView;

	if (pDlg->m_hWnd)
		pDlg->UpdateData(FALSE);

	return TRUE;
}

void CPrintSheetPlanningView::ChangePrintSheetNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if ( ! g_pThisPrintSheetPlanningView)
		return;

	int nCurNum = (nStartNum - 1) * CDlgNumeration::GetSeqLen(strStyle) + 1;
	switch (nSelection)
	{
	case CDlgNumeration::SelectionAll:
	case CDlgNumeration::SelectionRange:	
		{
			int			  nIndex = 0;
			CDisplayItem* pDI	 = g_pThisPrintSheetPlanningView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
			while (pDI)
			{
				CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
				if (pPrintSheet)
				{
					if ( ( nSelection == CDlgNumeration::SelectionAll) ||
						 ((nSelection == CDlgNumeration::SelectionRange) && (nIndex + 1 >= nRangeFrom) && (nIndex + 1 <= nRangeTo)) )
					{
						if (strStyle == _T("reset"))
						{
							pPrintSheet->m_strNumerationStyle = _T("$n");
							pPrintSheet->m_nNumerationOffset = 0;
						}
						else
						{
							pPrintSheet->m_strNumerationStyle = strStyle;
							pPrintSheet->m_nNumerationOffset  = nCurNum - pPrintSheet->m_nPrintSheetNumber;
						}
						nCurNum += (bReverse) ? -1 : 1;
	
						g_pThisPrintSheetPlanningView->InvalidateRect(pDI->m_BoundingRect);
					}
				}
				pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
				nIndex++;
			}
		}
		break;

	case CDlgNumeration::SelectionMarked:	
	case CDlgNumeration::SelectionProductPart:
		{
			CDisplayItem* pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
			while (pDI)
			{
				CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
				
				if (pPrintSheet)	
				{
					if (strStyle == _T("reset"))
					{
						pPrintSheet->m_strNumerationStyle = _T("$n");
						pPrintSheet->m_nNumerationOffset = 0;
					}
					else
					{
						pPrintSheet->m_strNumerationStyle = strStyle;
						pPrintSheet->m_nNumerationOffset  = nCurNum - pPrintSheet->m_nPrintSheetNumber;
					}
					g_pThisPrintSheetPlanningView->InvalidateRect(pDI->m_BoundingRect);
				}
				nCurNum += (bReverse) ? -1 : 1;
				pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
			}
		}
		break;
	}

	pDoc->SetModifiedFlag(TRUE);

	g_pThisPrintSheetPlanningView->UpdateWindow();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView), NULL, 0, NULL);
}

void CPrintSheetPlanningView::SelectAllPrintSheets()
{
	m_DisplayList.SelectAll(FALSE, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
	Invalidate();
	UpdateWindow();
}

void CPrintSheetPlanningView::SelectRangePrintSheets(int nFrom, int nTo)
{
	int			  nIndex = 0;
	CDisplayItem* pDI	 = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
	while (pDI)
	{
		if ((nIndex + 1 >= nFrom) && (nIndex + 1 <= nTo))
			pDI->m_nState = CDisplayItem::Selected;
		else
			pDI->m_nState = CDisplayItem::Normal;

		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
	}

	Invalidate();
	UpdateWindow();
}

BOOL CPrintSheetPlanningView::IsOpenPrintSheetNumeration()
{
	if (m_dlgNumeration.m_hWnd)
		if (m_dlgNumeration.m_pfnApplyCallback == ChangePrintSheetNumeration)
			return TRUE;
	return FALSE;
}

BOOL CPrintSheetPlanningView::IsOpenFoldSheetSelector()
{
	if (m_dlgFoldSchemeSelector.m_hWnd)
		return TRUE;
	return FALSE;
}

BOOL CPrintSheetPlanningView::IsSelectedPrintSheetNumber(int nPrintSheetIndex)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
	while (pDI)
	{
		if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
			if (nPrintSheetIndex == nIndex)
				return TRUE;
		nIndex++;

		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetNumber));
	}
	return FALSE;
}

void CPrintSheetPlanningView::RemovePrintSheet(CPrintSheet* pPrintSheet)
{
	if ( ! pPrintSheet)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strMsg;
	AfxFormatString1(strMsg, IDS_DELETE_PRINTSHEET_MSG, pPrintSheet->GetNumber());
	if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
		return;

	UndoSave();

	pDoc->m_PrintSheetList.RemovePrintSheet(pPrintSheet);

	pDoc->SetModifiedFlag();										

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	theApp.OnUpdateView(RUNTIME_CLASS(CProductsView),				NULL, 0, NULL);
}

void CPrintSheetPlanningView::RemovePrintGroupPrintSheets(CPrintGroup& rPrintGroup)
{
	if (rPrintGroup.IsNull())
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strMsg;
	strMsg.LoadString(IDS_DELETE_PRINTGROUP_PRINTSHEETS_MSG);
	if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
		return;

	UndoSave();

	CPrintSheet* pPrintSheet = rPrintGroup.GetFirstPrintSheet();
	CLayout*	 pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return;
	pLayout->RemovePrintSheetInstance(INT_MAX, TRUE);

	pDoc->m_Bookblock.Reorganize(NULL, NULL);
	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->SetModifiedFlag();										
	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	pDoc->m_PrintSheetList.InitializeFlatProductTypeLists();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	theApp.OnUpdateView(RUNTIME_CLASS(CProductsView),				NULL, 0, NULL);
}

////////////////////////// fold sheet numbering /////////////////////////////////////////////////////////////////////////////////////////
void CPrintSheetPlanningView::NumberFoldSheets() 
{
	g_pThisPrintSheetPlanningView = this;

	if (g_pThisPrintSheetPlanningView)
		g_pThisPrintSheetPlanningView->m_DisplayList.Invalidate();

	if (InitFoldSheetNumerationDialog(&m_dlgNumeration))
		if ( ! m_dlgNumeration.m_hWnd)
		{
			m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
			if ( ! GetFoldSheet())
				m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionAll;
			m_dlgNumeration.Create(IDD_NUMERATION_DIALOG, this);
			m_dlgNumeration.m_nRangeFrom = 1;
			if (CImpManDoc::GetDoc())
				m_dlgNumeration.m_nRangeTo = CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.GetCount();
			else
				m_dlgNumeration.m_nRangeTo = 1;
	
			OnUpdate(NULL, 0, NULL);
			Invalidate();
			UpdateWindow();
		}
}

BOOL CPrintSheetPlanningView::InitFoldSheetNumerationDialog(CDlgNumeration* pDlg)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	
	if (pDlg->m_nSelection == CDlgNumeration::SelectionAll)
		g_pThisPrintSheetPlanningView->SelectAllFoldSheets();
	else
		if (pDlg->m_nSelection == CDlgNumeration::SelectionRange)
			g_pThisPrintSheetPlanningView->SelectRangeFoldSheets(pDlg->m_nRangeFrom, pDlg->m_nRangeTo);
		else
			if (pDlg->m_nSelection == CDlgNumeration::SelectionProductPart)
				g_pThisPrintSheetPlanningView->SelectProductPartFoldSheets(pDlg->m_nProductPartIndex);

	pDlg->m_nProductPartIndex = -1;

	CDisplayItem*  pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));

	pDlg->m_strTitle.LoadString(IDS_FOLDSHEETNUMERATION_TITLE);
	pDlg->m_pfnApplyCallback = ChangeFoldSheetNumeration;
	pDlg->m_pfnInitCallback  = InitFoldSheetNumerationDialog;
	//pDlg->m_nSelection = (pDI) ? CDlgNumeration::SelectionMarked : CDlgNumeration::SelectionAll;
	if (pDI)
	{
		CFoldSheet* pFirstFoldSheet = (CFoldSheet*)pDI->m_pData;
		pDlg->m_nRangeFrom = (int)pDI->m_pAuxData + 1;

		pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetLastSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
		if (pDI)
		{
			CFoldSheet* pLastFoldSheet = (CFoldSheet*)pDI->m_pData;
			pDlg->m_nRangeTo   = (int)pDI->m_pAuxData + 1;
			pDlg->m_nStartNum  = (pDlg->m_bReverse) ? pLastFoldSheet->GetNumberOnly() : pFirstFoldSheet->GetNumberOnly();
		}
	}
	else
	{
		pDlg->m_nRangeFrom = 1; 
		pDlg->m_nRangeTo   = pDoc->m_Bookblock.GetNumFoldSheets(pDlg->m_nProductPartIndex);

		pDlg->m_nStartNum = 1;
		if (pDoc->m_Bookblock.m_DescriptionList.GetCount())
		{
			CDescriptionItem& rFirstItem = pDoc->m_Bookblock.m_DescriptionList.GetHead();
			CDescriptionItem& rLastItem  = pDoc->m_Bookblock.m_DescriptionList.GetTail();
			CFoldSheet* pFirstFoldSheet  = rFirstItem.GetFoldSheetPointer(&pDoc->m_Bookblock);
			CFoldSheet* pLastFoldSheet   = rLastItem.GetFoldSheetPointer(&pDoc->m_Bookblock);
			if (pFirstFoldSheet && pLastFoldSheet)
				pDlg->m_nStartNum = (pDlg->m_bReverse) ? pLastFoldSheet->GetNumberOnly() : pFirstFoldSheet->GetNumberOnly();
		}
	}

	pDlg->m_pViewToUpdate1 = g_pThisPrintSheetPlanningView;

	if (pDlg->m_hWnd)
		pDlg->UpdateData(FALSE);

	return TRUE;
}

void CPrintSheetPlanningView::ChangeFoldSheetNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if ( ! g_pThisPrintSheetPlanningView)
		return;

	int nCurNum = (nStartNum - 1) * CDlgNumeration::GetSeqLen(strStyle) + 1;
	switch (nSelection)
	{
	case CDlgNumeration::SelectionAll:
	case CDlgNumeration::SelectionRange:	
		{
			int			  nIndex = 0;
			CDisplayItem* pDI	 = g_pThisPrintSheetPlanningView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
			while (pDI)
			{
				CFoldSheet* pFoldSheet = (CFoldSheet*)pDI->m_pData;
				if (pFoldSheet)
				{
					if ( ( nSelection == CDlgNumeration::SelectionAll) ||
						 ((nSelection == CDlgNumeration::SelectionRange) && (nIndex + 1 >= nRangeFrom) && (nIndex + 1 <= nRangeTo)) )
					{
						if (strStyle == _T("reset"))
						{
							pFoldSheet->m_strNumerationStyle = _T("$n");
							pFoldSheet->m_nNumerationOffset = 0;
							pFoldSheet->m_bFoldSheetNumLocked = FALSE;
						}
						else
						{
							pFoldSheet->m_strNumerationStyle = strStyle;
							pFoldSheet->m_nNumerationOffset  = nCurNum - pFoldSheet->m_nFoldSheetNumber;
						}
						nCurNum += (bReverse) ? -1 : 1;
	
						g_pThisPrintSheetPlanningView->InvalidateRect(pDI->m_BoundingRect);
					}
				}
				pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
				nIndex++;
			}
		}
		break;

	case CDlgNumeration::SelectionMarked:	
	case CDlgNumeration::SelectionProductPart:
		{
			CDisplayItem* pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
			while (pDI)
			{
				CFoldSheet* pFoldSheet = (CFoldSheet*)pDI->m_pData;
				
				if (pFoldSheet)	
				{
					if (strStyle == _T("reset"))
					{
						pFoldSheet->m_strNumerationStyle = _T("$n");
						pFoldSheet->m_nNumerationOffset = 0;
						pFoldSheet->m_bFoldSheetNumLocked = FALSE;
					}
					else
					{
						pFoldSheet->m_strNumerationStyle = strStyle;
						pFoldSheet->m_nNumerationOffset  = nCurNum - pFoldSheet->m_nFoldSheetNumber;
					}
					g_pThisPrintSheetPlanningView->InvalidateRect(pDI->m_BoundingRect);
				}
				nCurNum += (bReverse) ? -1 : 1;
				pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
			}
		}
		break;
	}

	pDoc->SetModifiedFlag(TRUE);

	g_pThisPrintSheetPlanningView->UpdateWindow();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView), NULL, 0, NULL);
}

void CPrintSheetPlanningView::SelectAllFoldSheets()
{
	m_DisplayList.SelectAll(FALSE, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	Invalidate();
	UpdateWindow();
}

void CPrintSheetPlanningView::SelectRangeFoldSheets(int nFrom, int nTo)
{
	int			  nIndex = 0;
	CDisplayItem* pDI	 = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
	while (pDI)
	{
		if ((nIndex + 1 >= nFrom) && (nIndex + 1 <= nTo))
			pDI->m_nState = CDisplayItem::Selected;
		else
			pDI->m_nState = CDisplayItem::Normal;

		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
	}

	Invalidate();
	UpdateWindow();
}

void CPrintSheetPlanningView::SelectProductPartFoldSheets(int nProductPartIndex)
{
	CDisplayItem* pDI	 = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
	while (pDI)
	{
		CFoldSheet* pFoldSheet = (CFoldSheet*)pDI->m_pData;
		if (pFoldSheet->m_nProductPartIndex == nProductPartIndex)
			pDI->m_nState = CDisplayItem::Selected;
		else
			pDI->m_nState = CDisplayItem::Normal;

		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
	}

	Invalidate();
	UpdateWindow();
}

BOOL CPrintSheetPlanningView::IsOpenFoldSheetNumeration()
{
	if (m_dlgNumeration.m_hWnd)
		if (m_dlgNumeration.m_pfnApplyCallback == ChangeFoldSheetNumeration)
			return TRUE;
	return FALSE;
}

BOOL CPrintSheetPlanningView::IsSelectedFoldSheetNumber(int nFoldSheetIndex)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
	while (pDI)
	{
		if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
			if (nFoldSheetIndex == nIndex)
				return TRUE;
		nIndex++;

		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetNumber));
	}
	return FALSE;
}
////////////////////////// page numbering /////////////////////////////////////////////////////////////////////////////////////////
void CPrintSheetPlanningView::NumberPages() 
{
	g_pThisPrintSheetPlanningView = this;

	if (g_pThisPrintSheetPlanningView)
		g_pThisPrintSheetPlanningView->m_DisplayList.Invalidate();

	if (InitPageNumerationDialog(&m_dlgNumeration))
		if ( ! m_dlgNumeration.m_hWnd)
		{
			m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
			if ( ! GetPageTemplate())
				m_dlgNumeration.m_nSelection = CDlgNumeration::SelectionAll;
			m_dlgNumeration.Create(IDD_NUMERATION_DIALOG, this);
			m_dlgNumeration.m_nRangeFrom = 1;
			if (CImpManDoc::GetDoc())
				m_dlgNumeration.m_nRangeTo = CImpManDoc::GetDoc()->m_PageTemplateList.GetCount();
			else
				m_dlgNumeration.m_nRangeTo = 1;

			OnUpdate(NULL, 0, NULL);
			Invalidate();
			UpdateWindow();
		}
}

BOOL CPrintSheetPlanningView::InitPageNumerationDialog(CDlgNumeration* pDlg)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	
	if (pDlg->m_nSelection == CDlgNumeration::SelectionAll)
		g_pThisPrintSheetPlanningView->SelectAllPages();
	else
		if (pDlg->m_nSelection == CDlgNumeration::SelectionRange)
			g_pThisPrintSheetPlanningView->SelectRangePages(pDlg->m_nRangeFrom, pDlg->m_nRangeTo);
		else
			if (pDlg->m_nSelection == CDlgNumeration::SelectionProductPart)
				g_pThisPrintSheetPlanningView->SelectProductPartPages(pDlg->m_nProductPartIndex);

	pDlg->m_nProductPartIndex = -1;
	//if (pPageListView)
	//{
	//	CPageListFrame* pFrame = (CPageListFrame*)pPageListView->GetParentFrame();
	//	if (pFrame)
	//		pDlg->m_nProductPartIndex = pFrame->m_nProductPartIndex;
	//}

	CDisplayItem*  pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));

	pDlg->m_strTitle.LoadString(IDS_PAGENUMERATION_TITLE);
	pDlg->m_pfnApplyCallback = ChangePageNumeration;
	pDlg->m_pfnInitCallback  = InitPageNumerationDialog;
	//pDlg->m_nSelection = (pDI) ? CDlgNumeration::SelectionMarked : CDlgNumeration::SelectionAll;
	if (pDI)
	{
		CPageTemplate* pFirstPageTemplate = (CPageTemplate*)pDI->m_pData;
		pDlg->m_nRangeFrom = (int)pDI->m_pAuxData + 1;

		pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetLastSelectedItem(DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));
		if (pDI)
		{
			CPageTemplate* pLastPageTemplate = (CPageTemplate*)pDI->m_pData;
			pDlg->m_nRangeTo   = (int)pDI->m_pAuxData + 1;
			pDlg->m_nStartNum  = (pDlg->m_bReverse) ? pLastPageTemplate->GetPageNum() : pFirstPageTemplate->GetPageNum();
		}
	}
	else
	{
		pDlg->m_nRangeFrom = 1; 
		pDlg->m_nRangeTo   = pDoc->m_PageTemplateList.GetCount(pDlg->m_nProductPartIndex);

		CPageTemplate& rFirstPageTemplate = pDoc->m_PageTemplateList.GetHead(pDlg->m_nProductPartIndex);
		CPageTemplate& rLastPageTemplate  = pDoc->m_PageTemplateList.GetTail(pDlg->m_nProductPartIndex);
		pDlg->m_nStartNum  = (pDlg->m_bReverse) ? rLastPageTemplate.GetPageNum() : rFirstPageTemplate.GetPageNum();
	}

	pDlg->m_pViewToUpdate1 = g_pThisPrintSheetPlanningView;

	if (pDlg->m_hWnd)
		pDlg->UpdateData(FALSE);

	return TRUE;
}

void CPrintSheetPlanningView::ChangePageNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if ( ! g_pThisPrintSheetPlanningView)
		return;

	int nCurNum = (nStartNum - 1) * CDlgNumeration::GetSeqLen(strStyle) + 1;
	switch (nSelection)
	{
	case CDlgNumeration::SelectionAll:
	case CDlgNumeration::SelectionRange:	
		{
			int		 i   = 0;
			POSITION pos = pDoc->m_PageTemplateList.GetHeadPosition();
			while (pos)
			{
				CPageTemplate& rTemplate = pDoc->m_PageTemplateList.CList::GetNext(pos);
				if ( ( nSelection == CDlgNumeration::SelectionAll) ||
					 ((nSelection == CDlgNumeration::SelectionRange) && (i + 1 >= nRangeFrom) && (i + 1 <= nRangeTo)) )
				{
					if (strStyle == _T("reset"))
						rTemplate.m_bPageNumLocked = FALSE;
					else
					{
						int nIndex = pDoc->m_PageTemplateList.GetIndex(&rTemplate, nProductPartIndex) + 1;
						rTemplate.m_strPageID	   = CDlgNumeration::GenerateNumber(nIndex, strStyle, nCurNum - nIndex);
						rTemplate.m_bPageNumLocked = TRUE;
						nCurNum += (bReverse) ? -1 : 1;
					}
				}
				i++;
			}
			g_pThisPrintSheetPlanningView->Invalidate();
		}
		break;

	case CDlgNumeration::SelectionMarked:	
	case CDlgNumeration::SelectionProductPart:
		{
			CDisplayItem* pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
			while (pDI)
			{
				if ((int)(pDI->m_pAuxData) != -1)	// -1 means frame of multipage
				{
					CPageTemplate* pPageTemplate = (CPageTemplate*)pDI->m_pData;
					if (strStyle == _T("reset"))
						pPageTemplate->m_bPageNumLocked = FALSE;
					else
					{
						int nIndex = pDoc->m_PageTemplateList.GetIndex(pPageTemplate, nProductPartIndex) + 1;
						pPageTemplate->m_strPageID		= CDlgNumeration::GenerateNumber(nIndex, strStyle, nCurNum - nIndex);
						pPageTemplate->m_bPageNumLocked = TRUE;
						nCurNum += (bReverse) ? -1 : 1;
					}
					g_pThisPrintSheetPlanningView->InvalidateRect(pDI->m_BoundingRect);
				}
				pDI = g_pThisPrintSheetPlanningView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
			}

			g_pThisPrintSheetPlanningView->UpdateWindow();
		}
		break;
	}

	if (strStyle == _T("reset"))
		pDoc->m_PageTemplateList.SortPages();

	pDoc->SetModifiedFlag(TRUE);

	g_pThisPrintSheetPlanningView->UpdateWindow();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView), NULL, 0, NULL);
}

void CPrintSheetPlanningView::SelectAllPages()
{
	m_DisplayList.SelectAll(FALSE, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	Invalidate();
	UpdateWindow();
}

void CPrintSheetPlanningView::SelectRangePages(int nFrom, int nTo)
{
	int			  nIndex = 0;
	CDisplayItem* pDI	 = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	while (pDI)
	{
		if ((nIndex + 1 >= nFrom) && (nIndex + 1 <= nTo))
			pDI->m_nState = CDisplayItem::Selected;
		else
			pDI->m_nState = CDisplayItem::Normal;

		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	}

	Invalidate();
	UpdateWindow();
}

void CPrintSheetPlanningView::SelectProductPartPages(int nProductPartIndex)
{
	int			  nIndex = 0;
	CDisplayItem* pDI	 = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	while (pDI)
	{
		CPageTemplate* pPageTemplate = (CPageTemplate*)pDI->m_pData;
		if (pPageTemplate->m_nProductPartIndex == nProductPartIndex)
			pDI->m_nState = CDisplayItem::Selected;
		else
			pDI->m_nState = CDisplayItem::Normal;

		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	}

	Invalidate();
	UpdateWindow();
}

BOOL CPrintSheetPlanningView::IsOpenPageNumeration()
{
	if (m_dlgNumeration.m_hWnd)
		if (m_dlgNumeration.m_pfnApplyCallback == ChangePageNumeration)
			return TRUE;
	return FALSE;
}

BOOL CPrintSheetPlanningView::IsSelectedPageNumber(int nPageIndex)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	while (pDI)
	{
		if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
			if (nPageIndex == nIndex)
				return TRUE;
		nIndex++;

		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PageNumber));
	}
	return FALSE;
}

BOOL CPrintSheetPlanningView::IsInsideNumerationRange(int nIndex)
{
	switch (m_dlgNumeration.m_nSelection)
	{
	case CDlgNumeration::SelectionAll:			return TRUE; 

	case CDlgNumeration::SelectionRange:		if ((nIndex + 1 >= (int)m_dlgNumeration.m_nRangeFrom) && (nIndex + 1 <= (int)m_dlgNumeration.m_nRangeTo))
													return TRUE;
												break;

	case CDlgNumeration::SelectionMarked:	
	case CDlgNumeration::SelectionProductPart:	if (m_dlgNumeration.m_pfnApplyCallback == ChangePrintSheetNumeration)
												{
													if (IsSelectedPrintSheetNumber(nIndex))
														return TRUE;
												}
												else
												if (m_dlgNumeration.m_pfnApplyCallback == ChangePageNumeration)
												{
													if (IsSelectedPageNumber(nIndex))
														return TRUE;
												}
												else
												if (m_dlgNumeration.m_pfnApplyCallback == ChangeFoldSheetNumeration)
												{
													if (IsSelectedFoldSheetNumber(nIndex))
														return TRUE;
												}
												break;
	}

	return FALSE;
}
