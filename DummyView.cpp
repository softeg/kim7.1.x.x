// DummyView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DummyView.h"


// CDummyView

IMPLEMENT_DYNCREATE(CDummyView, CView)

CDummyView::CDummyView()
{

}

CDummyView::~CDummyView()
{
}

BEGIN_MESSAGE_MAP(CDummyView, CView)
END_MESSAGE_MAP()


// CDummyView-Zeichnung

void CDummyView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: Hier Code zum Zeichnen einf�gen
}


// CDummyView-Diagnose

#ifdef _DEBUG
void CDummyView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CDummyView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CDummyView-Meldungshandler
