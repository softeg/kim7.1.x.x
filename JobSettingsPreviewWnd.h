// JobSettingsPreviewWnd.h : header file
//
// This file contains the preview window used by the 
// CJobSettingsSheet property sheet.

/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPreviewWnd window

#ifndef __JOBSETTINGSPREVIEWWND_H__
#define __JOBSETTINGSPREVIEWWND_H__

class CJobSettingsPreviewWnd : public CWnd
{
// Construction
public:
	CJobSettingsPreviewWnd();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CJobSettingsPreviewWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CJobSettingsPreviewWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CJobSettingsPreviewWnd)
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif		// __JOBSETTINGSPREVIEWWND_H__