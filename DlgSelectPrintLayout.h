#pragma once


typedef struct
{
	CPrintSheet* pPrintSheet;
	CLayout*	 pLayout;
}_PRINTLAYOUT;


// CDlgSelectPrintLayout-Dialogfeld

class CDlgSelectPrintLayout : public CDialog
{
	DECLARE_DYNAMIC(CDlgSelectPrintLayout)

public:
	CDlgSelectPrintLayout(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgSelectPrintLayout();

// Dialogfelddaten
	enum { IDD = IDD_SELECT_PRINTLAYOUT };


public:
	CFont								m_smallFont;
	CArray<_PRINTLAYOUT, _PRINTLAYOUT>	m_sampleList;
	CString								m_strCurLayoutName;
	int									m_nSelection;

protected:
	CImageList*		m_pPrintLayoutImageList;
	CListCtrl		m_printLayoutList;


public:
	void AddPrintLayout(CPrintSheet& rPrintSheet, CLayout& rLayout);
	void InitPrintLayoutList();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnNMCustomdrawPrintLayoutlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedPrintLayoutlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk();
};
