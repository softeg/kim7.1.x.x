// ProductsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "ProductsView.h"
#include "OrderDataFrame.h"
#include "GraphicComponent.h"
#include "DlgJobData.h"
#include "NewPrintSheetFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetPlanningView.h"
#include "PrintSheetViewStrippingData.h"
#include "PrintSheetComponentsView.h"
#include "DlgFlatProductData.h"
#include "CSVInput.h"
#include "CFFInput.h"
#include "DlgPDFLibStatus.h"
#include "DlgBoundProduct.h"
#include "BookblockFrame.h"
#include "BookBlockDataView.h"
#include "BookBlockView.h"
#include "PageListFrame.h"
#include "PageListView.h"
#include "PrintComponentsView.h"


#define IDC_PRODUCTSVIEW_HEADER 11113


// CProductsView

IMPLEMENT_DYNCREATE(CProductsView, CScrollView)

CProductsView::CProductsView()
{
    m_DisplayList.Create(this, FALSE, DISPLAY_LIST_TYPE(CProductsView));
	m_DisplayList.SetClassName(_T("CProductsView"));

	m_nHeaderItemsTotalWidth = 0;
	m_nHeaderHeight = 0;
	m_headerFont.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_nProductPartIndex = 0;
	m_pPageSource = NULL;
}

CProductsView::~CProductsView()
{
	m_headerFont.DeleteObject();
	m_pPageSource = NULL;
	theApp.m_pDlgPDFLibStatus->m_pPageSource = NULL;
}

BEGIN_MESSAGE_MAP(CProductsView, CScrollView)
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
    ON_WM_CONTEXTMENU()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_ERASEBKGND()
	ON_COMMAND(ID_BOUNDPRODUCT_DUPLICATE, OnBoundProductDuplicate)
	ON_NOTIFY(HDN_TRACK,	IDC_PRODUCTSVIEW_HEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK, IDC_PRODUCTSVIEW_HEADER, OnHeaderEndTrack)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_DEL_PRODUCT, OnDeleteProduct)
END_MESSAGE_MAP()


BOOL CProductsView::OnEraseBkgnd(CDC* pDC)
{
	m_toolBar.ClipButtons(pDC);

	CRect rect;
    GetClientRect(rect);
	rect.top = 0;
    //CBrush brush(RGB(230,233,238));
    //CBrush brush(RGB(240,241,244));//237,239,243));
    CBrush brush(WHITE);
    pDC->FillRect(&rect, &brush);

    brush.DeleteObject();

	return TRUE;

//	return CScrollView::OnEraseBkgnd(pDC);
}

int CProductsView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CREATE_INPLACE_EDIT_EX(InplaceEdit);
	CREATE_INPLACE_COMBOBOX(InplaceComboBox, 0);

	return 0;
}

void CProductsView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
}

void CProductsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_headerColumnWidths.RemoveAll();
	m_HeaderCtrl.DestroyWindow();
	m_nHeaderHeight = 0;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if ( ! m_HeaderCtrl.GetSafeHwnd())
	{
		m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, IDC_PRODUCTSVIEW_HEADER);	
		m_HeaderCtrl.SetFont(&m_headerFont, FALSE);

		HD_ITEM hdi;
		hdi.mask		= HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
		hdi.fmt			= HDF_LEFT | HDF_STRING;

		m_headerColumnWidths.RemoveAll();

		int		nIndex = 0;
		CString string;
		string.LoadString(IDS_PRODUCT);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetProductTableHeaderWidth(nIndex, 200);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_PAPERLIST_FORMAT); 
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetProductTableHeaderWidth(nIndex, 80);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_QUANTITY); 
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetProductTableHeaderWidth(nIndex, 80);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		if (pDoc->m_flatProducts.GetSize() > 0)
		{
			string.LoadString(IDS_OVERPRODUCTION); 
			hdi.pszText		= string.GetBuffer(0);
			hdi.cxy			= GetProductTableHeaderWidth(nIndex, 120);
			hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
			m_headerColumnWidths.Add(hdi.cxy);
			m_HeaderCtrl.InsertItem(nIndex++, &hdi);

			string.LoadString(IDS_PAPER_GRAIN);
			hdi.pszText		= string.GetBuffer(0);
			hdi.cxy			= GetProductTableHeaderWidth(nIndex, 200);
			hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
			m_headerColumnWidths.Add(hdi.cxy);
			m_HeaderCtrl.InsertItem(nIndex++, &hdi);

			string.LoadString(IDS_TRIMMINGS);
			hdi.pszText		= string.GetBuffer(0);
			hdi.cxy			= GetProductTableHeaderWidth(nIndex, 200);
			hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
			m_headerColumnWidths.Add(hdi.cxy);
			m_HeaderCtrl.InsertItem(nIndex++, &hdi);
		}

		string.LoadString(IDS_PAPER);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetProductTableHeaderWidth(nIndex, 200);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		if (pDoc->m_flatProducts.GetSize() > 0)
		{
			string = _T("Zone");//.LoadString(IDS_PUNCHTOOL);
			hdi.pszText		= string.GetBuffer(0);
			hdi.cxy			= GetProductTableHeaderWidth(nIndex, 100);
			hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
			m_headerColumnWidths.Add(hdi.cxy);
			m_HeaderCtrl.InsertItem(nIndex++, &hdi);

			string.LoadString(IDS_PUNCHTOOL);
			hdi.pszText		= string.GetBuffer(0);
			hdi.cxy			= GetProductTableHeaderWidth(nIndex, 200);
			hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
			m_headerColumnWidths.Add(hdi.cxy);
			m_HeaderCtrl.InsertItem(nIndex++, &hdi);
		}

		m_nHeaderItemsTotalWidth = 0;
		for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
			m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];
	}

	OnSize(0, 0, 0);

	ModifyStyle(0, WS_CLIPCHILDREN);	// Clip header control 

	Invalidate();

	m_DisplayList.Invalidate();

	m_docSize = CalcExtents();
	SetScrollSizes(MM_TEXT, m_docSize);
}

void CProductsView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);

	if (m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdl;
		WINDOWPOS wp;
		hdl.prc = &clientRect;
		hdl.pwpos = &wp;
		m_HeaderCtrl.Layout(&hdl);	
		m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_nHeaderHeight = wp.cy;
	} 
}

// This functions are overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling

void CProductsView::ScrollToPosition(POINT pt)    // logical coordinates
{
	ASSERT(m_nMapMode > 0);     // not allowed for shrink to fit
	if (m_nMapMode != MM_TEXT)
	{
		CWindowDC dc(NULL);
		dc.SetMapMode(m_nMapMode);
		dc.LPtoDP((LPPOINT)&pt);
	}

	// now in device coordinates - limit if out of range
	int xMax = GetScrollLimit(SB_HORZ);
	int yMax = GetScrollLimit(SB_VERT);
	if (pt.x < 0)
		pt.x = 0;
	else if (pt.x > xMax)
		pt.x = xMax;
	if (pt.y < 0)
		pt.y = 0;
	else if (pt.y > yMax)
		pt.y = yMax;

	ScrollToDevicePosition(pt);
}

void CProductsView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);
	
	CRect scrollRect, clipRect;
	GetClientRect(scrollRect); clipRect = scrollRect;
	clipRect.top+= m_nHeaderHeight;	// Protect header control from being vertically scrolled
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, clipRect);

	if (m_HeaderCtrl.GetSafeHwnd())
		if (ptDev.x != 0)				// Scroll header control horizontally
			m_HeaderCtrl.MoveWindow(-ptDev.x, 0, scrollRect.Width() + ptDev.x, m_nHeaderHeight);

	if (m_InplaceEdit.GetSafeHwnd())
		m_InplaceEdit.Deactivate();
}

BOOL CProductsView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect, clipRect;
		GetClientRect(scrollRect); clipRect = scrollRect;
		//if (m_HeaderCtrl.GetSafeHwnd())
			clipRect.top+= m_nHeaderHeight;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, clipRect);

		if (m_HeaderCtrl.GetSafeHwnd())
			if (sizeScroll.cx != 0)				// Scroll header control horizontally
				m_HeaderCtrl.MoveWindow(-x, 0, scrollRect.Width() + x, m_nHeaderHeight);

		if (m_InplaceEdit.GetSafeHwnd())
			m_InplaceEdit.Deactivate();
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

void CProductsView::DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd)
{
	CRect rcHeader = rcFrame;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);
}


// CProductsView-Zeichnung

CSize CProductsView::CalcExtents()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CRect rcBox = Draw(&dc, NULL);

	dc.DeleteDC();

	return CSize(rcBox.right, rcBox.bottom + 20);
}

void CProductsView::OnDraw(CDC* pDC)
{
	CRect rcClientRect, rcClip;
	GetClientRect(rcClientRect); rcClip = rcClientRect;
	rcClip.top = m_nHeaderHeight;
	rcClip.OffsetRect(-pDC->GetViewportOrg());
	pDC->IntersectClipRect(rcClip);

	Draw(pDC, &m_DisplayList);
}

CRect CProductsView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(0,0,1,1);

	if (m_headerColumnWidths.GetSize() <= 0)
		return CRect(0,0,1,1);

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	CRect rcFrame;
	GetClientRect(rcFrame); 
	
	rcFrame.top += m_nHeaderHeight;	
	//rcFrame.right = rcFrame.left + 500;
	rcFrame.top += 5;
	rcFrame.left += 5;

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nColIndex = 0;
	ResetDisplayColums();
	AddDisplayColumn(ColThumbnail,					m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColProductSize,				m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColProductQuantity,			m_headerColumnWidths[nColIndex++]);
	if (pDoc->m_flatProducts.GetSize() > 0)
	{
		AddDisplayColumn(ColProductOverProduction, 	m_headerColumnWidths[nColIndex++]);
		AddDisplayColumn(ColProductGrain,			m_headerColumnWidths[nColIndex++]);
		AddDisplayColumn(ColProductTrims,			m_headerColumnWidths[nColIndex++]);
	}
	AddDisplayColumn(ColProductPaper,				m_headerColumnWidths[nColIndex++]);
	if (pDoc->m_flatProducts.GetSize() > 0)
	{
		AddDisplayColumn(ColProductZone,			m_headerColumnWidths[nColIndex++]);
		AddDisplayColumn(ColProductPunch,			m_headerColumnWidths[nColIndex++]);
	}

	CGraphicComponent gc;
	CPen linePen(PS_SOLID, 1, RGB(240,240,240));
	CPen* pOldPen = pDC->SelectObject(&linePen);

	int		 nGlobalProductPartIndex = 0;
	CRect	 rcRow = rcFrame; rcRow.top += 5; rcRow.bottom = rcRow.top + 20;
	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		int			   nProductIndex = rBoundProduct.GetIndex();

		for (int nProductPartIndex = 0; nProductPartIndex < rBoundProduct.m_parts.GetSize(); nProductPartIndex++)
		{
			CBoundProductPart& rProductPart = rBoundProduct.m_parts[nProductPartIndex];

			/////// columns
			CRect rcColumn = rcRow; 
			rcColumn.left += 5;

			int	  nRight = 0;
			CRect rcRet, rcThumbnail;
			for (int i = 0; i < m_displayColumns.GetSize(); i++)
			{
				nRight += m_displayColumns[i].nWidth;
				rcColumn.right = nRight;
				switch (m_displayColumns[i].nID)
				{
				case ColThumbnail:				rcRet = rProductPart.DrawThumbnail		(pDC, rcColumn, -1, 0,	  pDisplayList);	rcThumbnail = rcRet; rcThumbnail.left = rcRow.left; rcThumbnail.right = rcColumn.right; break;
				case ColProductSize:			rcRet = rProductPart.DrawSize			(pDC, rcColumn, NULL,	  pDisplayList);	break;
				case ColProductQuantity:		rcRet = rProductPart.DrawQuantity		(pDC, rcColumn, NULL,	  pDisplayList);	break;
				case ColProductOverProduction:	rcRet = rProductPart.DrawOverProduction	(pDC, rcColumn, 		  pDisplayList);	break;
				case ColProductGrain:			rcRet = rProductPart.DrawPaperGrain		(pDC, rcColumn, 		  pDisplayList);	break;
				case ColProductTrims:			rcRet = rProductPart.DrawTrims			(pDC, rcColumn, 		  pDisplayList);	break;
				case ColProductPaper:			rcRet = rProductPart.DrawPaperInfo		(pDC, rcColumn, 		  pDisplayList);	break;
				}
				rcBox.UnionRect(rcBox, rcRet);

				rcColumn.left = nRight + 5;
			}

			if (pDisplayList)
			{
				CRect rcDI = rcThumbnail; rcDI.top -= 3; rcDI.bottom += 1;
				CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nGlobalProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductsView, BoundProductPart), (void*)nProductIndex, CDisplayItem::ForceRegister); // register item into display list
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
					if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
					{
						CRect rcFeedback = pDI->m_BoundingRect; 
						rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
						COLORREF crHighlightColor = RGB(255,195,15);
						gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
					}
				}
			}

			nGlobalProductPartIndex++;

			pDC->MoveTo(rcRow.left,  rcRow.bottom);
			pDC->LineTo(rcRow.right, rcRow.bottom);

			rcRow.OffsetRect(0, rcRow.Height() + 5);
		}
	}

	//if (pDoc->m_boundProducts.GetSize())
	//	if (pDoc->m_flatProducts.GetSize())
	//		rcRow.OffsetRect(0, 10);

	/////// columns
	rcRow.bottom = rcRow.top + 20;
	CRect rcColumn = rcRow; 
	rcColumn.left += 5;

	int	  nTop   = rcRow.top;
	int	  nRight = 0;
	CRect rcRet, rcThumbnail;
	for (int i = 0; i < m_displayColumns.GetSize(); i++)
	{
		nRight += m_displayColumns[i].nWidth;
		rcColumn.right = nRight;

		pos = pDoc->m_flatProducts.GetHeadPosition();
		while (pos)
		{
			rcColumn.top    = rcRow.top;
			rcColumn.bottom = rcFrame.bottom;
			CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);
			switch (m_displayColumns[i].nID)
			{
			case ColThumbnail:				rcRet = rFlatProduct.DrawThumbnail		(pDC, rcColumn, pDisplayList);	rcThumbnail = rcRet; rcThumbnail.left = rcRow.left; rcThumbnail.right = rcColumn.right;	break;
			case ColProductSize:			rcRet = rFlatProduct.DrawSize			(pDC, rcColumn, pDisplayList);	break;
			case ColProductQuantity:		rcRet = rFlatProduct.DrawTargetQuantity	(pDC, rcColumn, pDisplayList);	break;
			case ColProductOverProduction:	rcRet = rFlatProduct.DrawOverProduction	(pDC, rcColumn, pDisplayList);	break;
			case ColProductGrain:			rcRet = rFlatProduct.DrawPaperGrain		(pDC, rcColumn, pDisplayList);	break;
			case ColProductTrims:			rcRet = rFlatProduct.DrawTrims			(pDC, rcColumn, pDisplayList);	break;
			case ColProductPaper:			rcRet = rFlatProduct.DrawPaperInfo		(pDC, rcColumn, pDisplayList);	break;
			case ColProductZone:			rcRet = rFlatProduct.DrawZone			(pDC, rcColumn, pDisplayList);	break;
			case ColProductPunch:			rcRet = rFlatProduct.DrawPunchInfo		(pDC, rcColumn, pDisplayList);	break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			if (pDisplayList && (i == 0))
			{
				int nFlatProductIndex = pDoc->m_flatProducts.FindProductIndex(&rFlatProduct);
				CRect rcDI = rcThumbnail; rcDI.top = rcRow.top - 3; rcDI.bottom = rcRow.bottom - 2; 
				CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nFlatProductIndex, DISPLAY_ITEM_REGISTRY(CProductsView, FlatProduct), NULL, CDisplayItem::ForceRegister); // register item into display list
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
					if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
					{
						CRect rcFeedback = pDI->m_BoundingRect; 
						rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
						COLORREF crHighlightColor = RGB(255,195,15);
						gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
					}
				}
			}

			pDC->MoveTo(rcRow.left,  rcRow.bottom);
			pDC->LineTo(rcRow.right, rcRow.bottom);

			rcRow.OffsetRect(0, rcRow.Height() + 5);
		}

		rcRow.OffsetRect(0, -(rcRow.top - nTop));
		rcColumn.left = nRight + 5;
	}

	pDC->SelectObject(pOldPen);
	linePen.DeleteObject();

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}


void CProductsView::ResetDisplayColums()
{
	m_displayColumns.RemoveAll();
}

void CProductsView::AddDisplayColumn(int nColID, int nWidth)
{
	struct displayColumn dc = {nColID, nWidth};
	m_displayColumns.Add(dc);
}


// CProductsView-Diagnose

#ifdef _DEBUG
void CProductsView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CProductsView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG


// CProductsView-Meldungshandler

void CProductsView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_DisplayList.OnLButtonDown(point);

	CScrollView::OnLButtonDown(nFlags, point);
}

void CProductsView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
    OnLButtonDown(nFlags, point);	// Handle second click 

	CScrollView::OnLButtonDblClk(nFlags, point);
}

void CProductsView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list
	CPoint pt = point;
	ClientToScreen(&pt);
	OnContextMenu(NULL, pt);
}

void CProductsView::OnContextMenu(CWnd*, CPoint point)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int			  nMenuID = -1;
	CDisplayItem* pDI	  = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPart));
	if (pDI)
		nMenuID = IDR_PRODUCTSVIEW_POPUP;

	if (nMenuID < 0)
		return;

    CMenu menu;
	VERIFY(menu.LoadMenu(nMenuID));

	CMenu* pPopup = menu.GetSubMenu(0);
	if ( ! pPopup)
		return;

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
}

void CProductsView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

void CProductsView::OnMouseLeave()
{
	m_DisplayList.OnMouseLeave();

	CScrollView::OnMouseLeave();
}

BOOL CProductsView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// The default call of OnMouseWheel causes an assertion
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion

//	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}

BOOL CProductsView::OnSelectBookblock(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheetPlanningView* pView = NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		pView = ((COrderDataFrame*)pFrame)->GetPrintSheetPlanningView();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			pView = ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView();

	pView->m_DisplayList.Invalidate();
	pView->OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CProductsView::OnDeselectBookblock(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheetPlanningView* pView = NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		pView = ((COrderDataFrame*)pFrame)->GetPrintSheetPlanningView();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			pView = ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView();

	pView->m_DisplayList.Invalidate();
	pView->OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CProductsView::OnMouseMoveBookblock(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CProductsView::OnSelectProductGroupBound(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheetPlanningView* pView = NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		pView = ((COrderDataFrame*)pFrame)->GetPrintSheetPlanningView();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			pView = ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView();

	pView->m_DisplayList.Invalidate();
	pView->OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CProductsView::OnDeselectProductGroupBound(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheetPlanningView* pView = NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		pView = ((COrderDataFrame*)pFrame)->GetPrintSheetPlanningView();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			pView = ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView();

	pView->m_DisplayList.Invalidate();
	pView->OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CProductsView::OnActivateProductGroupBound(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	//CDlgJobData dlg(IDD_PRODUCTDATA, 0, (ULONG_PTR)pDI->m_pData, NULL);

	//dlg.DoModal();

	return TRUE;
}

BOOL CProductsView::OnMouseMoveProductGroupBound(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CProductsView::OnSelectProductGroupUnbound(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheetPlanningView* pView = NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		pView = ((COrderDataFrame*)pFrame)->GetPrintSheetPlanningView();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			pView = ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView();

	pView->m_DisplayList.Invalidate();
	pView->OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CProductsView::OnDeselectProductGroupUnbound(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheetPlanningView* pView = NULL;
	CFrameWnd* pFrame = GetParent()->GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		pView = ((COrderDataFrame*)pFrame)->GetPrintSheetPlanningView();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			pView = ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView();

	pView->m_DisplayList.Invalidate();
	pView->OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CProductsView::OnActivateProductGroupUnbound(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	//CDlgJobData dlg(IDD_PRODUCTDATA, 1, (ULONG_PTR)pDI->m_pData, NULL);

	//dlg.DoModal();

	return TRUE;
}

BOOL CProductsView::OnMouseMoveProductGroupUnbound(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CProductsView::OnSelectFlatProduct(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CFrameWnd* pFrame = GetParentFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPageListFrame)))
	{
		CPageListFrame* pPageListFrame = (CPageListFrame*)pFrame;
		pPageListFrame->m_nProductPartIndex = pDoc->GetFlatProductsIndex();
		((CPageListView*)pPageListFrame->m_wndSplitter.GetPane(0, 1))->m_DisplayList.Invalidate();
		((CPageListView*)pPageListFrame->m_wndSplitter.GetPane(0, 1))->OnUpdate(NULL, 0, NULL);
	}

	//if (GetParentFrame()->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))	
	//{
	//	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	//	if (pFrame->m_dlgAddFlatProducts.m_hWnd)
	//	{
	//		int	nFlatProductIndex = (pDI) ? (int)pDI->m_pData : 0;
	//		pFrame->m_dlgAddFlatProducts.InitData(nFlatProductIndex);
	//		return;
	//	}
	//}

	CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex((int)pDI->m_pData);//GetSelFlatProduct();
	if (rFlatProduct.IsNull())
		return FALSE;

	CPrintSheetPlanningView* pPlanningView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	if (pPlanningView)
	{
		CPrintSheet*  pPrintSheet = rFlatProduct.GetFirstPrintSheet();
		while (pPrintSheet)
		{
			CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			if ( ! pLayout)
				return FALSE;

			POSITION pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
				if ( ! rObject.IsFlatProduct())
					continue;
				if (&rObject.GetCurrentFlatProduct(pPrintSheet) == &rFlatProduct)
				{
					CDisplayItem* pDISelect = pPlanningView->m_DisplayList.GetItemFromData((void*)&rObject, (void*)pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutObject));
					if (pDISelect)
					{
						pDISelect->m_nState = CDisplayItem::Selected;
						pPlanningView->m_DisplayList.InvalidateItem(pDISelect);
					}
				}
			}

			pPrintSheet	= rFlatProduct.GetNextPrintSheet(pPrintSheet);
		}
	}

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CPrintSheet*  pPrintSheet = rFlatProduct.GetFirstPrintSheet();
		while (pPrintSheet)
		{
			CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			if ( ! pLayout)
				return FALSE;

			POSITION pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
				if ( ! rObject.IsFlatProduct())
					continue;
				if (&rObject.GetCurrentFlatProduct(pPrintSheet) == &rFlatProduct)
				{
					CDisplayItem* pDISelect = pView->m_DisplayList.GetItemFromData((void*)&rObject, (void*)pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
					if (pDISelect)
					{
						pDISelect->m_nState = CDisplayItem::Selected;
						pView->m_DisplayList.InvalidateItem(pDISelect);
					}
				}
			}

			pPrintSheet	= rFlatProduct.GetNextPrintSheet(pPrintSheet);
		}
	}

	return TRUE;
}

BOOL CProductsView::OnDeselectFlatProduct(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheetPlanningView* pPrintSheetPlanningView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	if (pPrintSheetPlanningView)
	{
		pPrintSheetPlanningView->m_DisplayList.DeselectAll();
		pPrintSheetPlanningView->m_DisplayList.InvalidateItem();
		pPrintSheetPlanningView->UpdateWindow();
	}
	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pPrintSheetView)
	{
		pPrintSheetView->m_DisplayList.DeselectAll();
		pPrintSheetView->m_DisplayList.InvalidateItem();
		pPrintSheetView->UpdateWindow();
	}

	return TRUE;
}

BOOL CProductsView::OnActivateFlatProduct(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CDlgFlatProductData dlg;

	if (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct)) == 1)
	{
		dlg.m_pFlatProductList	= &pDoc->m_flatProducts;
		dlg.m_nFlatProductIndex	= (int)pDI->m_pData;
		dlg.DoModal();
		pDI->m_nState = CDisplayItem::Normal;
	}
	else
	{
		CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
		while (pDI)
		{
			CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex((int)pDI->m_pData);
			dlg.m_product.m_strProductName += CString(_T("\"")) + rFlatProduct.m_strProductName + CString(_T("\" "));
			pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
		}

		dlg.m_nPaperGrain = dlg.m_product.m_nPaperGrain = -1;
		dlg.m_nPriority   = dlg.m_product.m_nPriority   = -1;

		if (dlg.DoModal() == IDCANCEL)
			return FALSE;

		dlg.m_product.m_strProductName = _T("");
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
		while (pDI)
		{
			CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex((int)pDI->m_pData);
			rFlatProduct.Copy(dlg.m_product, TRUE);		// TRUE: copy only changed (initialized) fields
			pDI->m_nState = CDisplayItem::Normal;
			pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
		}
	}

	pDoc->SetModifiedFlag();
	pDoc->UpdateAllViews(NULL, 0, NULL);

	//m_DisplayList.Invalidate();
	//Invalidate();
	//UpdateWindow();

	//CPrintSheetPlanningView* pPrintSheetPlanningView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	//if (pPrintSheetPlanningView)
	//{
	//	pPrintSheetPlanningView->m_DisplayList.InvalidateItem();
	//	pPrintSheetPlanningView->Invalidate();
	//	pPrintSheetPlanningView->UpdateWindow();
	//}

	return TRUE;
}

BOOL CProductsView::OnSelectProductGroupExpander(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;

	//CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart((int)pDI->m_pData);
	//rProductGroup.SetShowProductsExpanded( ! rProductGroup.DoShowProductsExpanded());

	//pDI->m_nState = CDisplayItem::Normal;

	//m_DisplayList.Invalidate();
	//Invalidate();
	//UpdateWindow();

	return TRUE;
}

BOOL CProductsView::OnMouseMoveProductGroupExpander(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if ( ! pDoc)
					return FALSE;
	 			CDC* pDC = GetDC(); 
				CPrintGroup& rPrintGroup = pDoc->m_printGroupList.GetPrintGroup((int)pDI->m_pData);
				CRect rc = pDI->m_BoundingRect;
				CRect rcIcon = pDI->m_BoundingRect; rcIcon.left += 5; rcIcon.top += 5; rcIcon.right = rcIcon.left + 9; rcIcon.bottom = rcIcon.top + 9;
				pDC->DrawIcon(rcIcon.TopLeft(), (rPrintGroup.DoShowProductsExpanded()) ? theApp.LoadIcon(IDI_MINUS_HOT) : theApp.LoadIcon(IDI_PLUS_HOT));
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

CFlatProduct& CProductsView::GetActiveFlatProduct()
{
	static private CFlatProduct nullFlatProduct;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
	if ( ! pDI)
		return nullFlatProduct;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		return pDoc->m_flatProducts.FindByIndex((int)pDI->m_pData);
	else
		return nullFlatProduct;
}

BOOL CProductsView::OnMouseMoveFlatProduct(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height() - 1);
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CProductsView::OnMouseMoveBoundProductPartDesiredQuantity(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CProductsView::OnMouseMoveFlatProductDesiredQuantity(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CProductsView::OnMouseMoveBoundProductPartPaperInfo(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CProductsView::OnMouseMoveFlatProductPaperInfo(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

void CProductsView::OnKillFocusInplaceEdit() 		// Message from InplaceEdit
{
}

void CProductsView::OnCharInplaceEdit(UINT nChar) // Message from InplaceEdit
{
	if (nChar == VK_RETURN)	
	{
		CDisplayItem* pDI = NULL;
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPartDesiredQuantity))
		{
			UpdateBoundProductPartDesiredQuantity();
			pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if ( ! pDI)
				pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, DISPLAY_ITEM_TYPEID(CProductsView, FlatProductDesiredQuantity));
		}
		else
			if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CProductsView, FlatProductDesiredQuantity))
			{
				UpdateFlatProductDesiredQuantity();
				pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
				if ( ! pDI)
					pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPartDesiredQuantity));
			}
			else
				return;

		if (pDI)
		{
			m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
			m_DisplayList.InvalidateItem();
			if (pDI->m_strItemTypeID == DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPartDesiredQuantity))
				OnActivateBoundProductPartDesiredQuantity(pDI, CPoint(0,0), 0);
			else
				OnActivateFlatProductDesiredQuantity(pDI, CPoint(0,0), 0);
			pDI->m_nState = CDisplayItem::Selected;
		}
	}
}

BOOL CProductsView::OnSelectBoundProductPartDesiredQuantity(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	OnActivateBoundProductPartDesiredQuantity(pDI, point, lParam);

	return TRUE;
}

BOOL CProductsView::OnDeselectBoundProductPartDesiredQuantity(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	m_InplaceEdit.Deactivate();

	return TRUE;
}

BOOL CProductsView::OnActivateBoundProductPartDesiredQuantity(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return FALSE;

	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CBoundProductPart& rBoundProductPart = pDoc->m_boundProducts.GetProductPart((int)di.m_pData);
	CString strQuantity; strQuantity.Format(_T("%d"), rBoundProductPart.m_nDesiredQuantity);

	m_InplaceEdit.Activate(rect, strQuantity, CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CProductsView::UpdateBoundProductPartDesiredQuantity()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return;

	CBoundProductPart& rBoundProductPart = pDoc->m_boundProducts.GetProductPart((int)m_InplaceEdit.m_di.m_pData);
	int nValue = _ttoi(m_InplaceEdit.GetString());

	m_InplaceEdit.Deactivate();

	if (rBoundProductPart.IsNull())
		return;

	if ( (rBoundProductPart.m_nDesiredQuantity == nValue) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPartDesiredQuantity)) == 1) )
		return;

	int nDesiredQuantity = _ttoi(m_InplaceEdit.GetString());

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPartDesiredQuantity));
	while (pDI)
	{
		CBoundProductPart& rBoundProductPart = pDoc->m_boundProducts.GetProductPart((int)pDI->m_pData);
		if (rBoundProductPart.IsNull())
			continue;

		rBoundProductPart.m_nDesiredQuantity = nDesiredQuantity;
		rBoundProductPart.SetFoldSheetsDesiredQuantity(nDesiredQuantity);

		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPartDesiredQuantity));
	}

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);										

	Invalidate();
	UpdateWindow();	

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetComponentsView),	NULL, 0, NULL);	
}

BOOL CProductsView::OnSelectBoundProductPartPaperInfo(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	if ( ! m_DisplayList.m_bIsInsideMultiSelect)
		OnActivateBoundProductPartPaperInfo(pDI, point, lParam);

	return TRUE;
}

BOOL CProductsView::OnDeselectBoundProductPartPaperInfo(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	//m_InplaceComboBox.Deactivate();

	return TRUE;
}

BOOL CProductsView::OnActivateBoundProductPartPaperInfo(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(NULL);//pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CBoundProductPart& rBoundProductPart = pDoc->m_boundProducts.GetProductPart((int)di.m_pData);

	m_InplaceComboBox.Activate(rect, InitializePaperInfoCombo, rBoundProductPart.m_strPaper, 0, di);

	return TRUE;
}

BOOL CProductsView::OnSelectFlatProductDesiredQuantity(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	OnActivateFlatProductDesiredQuantity(pDI, point, lParam);

	return TRUE;
}

BOOL CProductsView::OnDeselectFlatProductDesiredQuantity(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	m_InplaceEdit.Deactivate();

	return TRUE;
}

BOOL CProductsView::OnActivateFlatProductDesiredQuantity(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return FALSE;

	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex((int)di.m_pData);
	CString strQuantity; strQuantity.Format(_T("%d"), rFlatProduct.m_nDesiredQuantity);

	m_InplaceEdit.Activate(rect, strQuantity, CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CProductsView::UpdateFlatProductDesiredQuantity()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return;

	CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex((int)m_InplaceEdit.m_di.m_pData);
	int nValue = _ttoi(m_InplaceEdit.GetString());

	m_InplaceEdit.Deactivate();

	if (rFlatProduct.IsNull())
		return;

	if ( (rFlatProduct.m_nDesiredQuantity == nValue) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CProductsView, FlatProductDesiredQuantity)) == 1) )
		return;

	int nDesiredQuantity = _ttoi(m_InplaceEdit.GetString());

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, FlatProductDesiredQuantity));
	while (pDI)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex((int)pDI->m_pData);
		if (rFlatProduct.IsNull())
			continue;

		rFlatProduct.m_nDesiredQuantity = nDesiredQuantity;

		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CProductsView, FlatProductDesiredQuantity));
	}

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);										

	Invalidate();
	UpdateWindow();	

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetComponentsView),	NULL, 0, NULL);	
}

BOOL CProductsView::OnSelectFlatProductPaperInfo(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	if ( ! m_DisplayList.m_bIsInsideMultiSelect)
		OnActivateFlatProductPaperInfo(pDI, point, lParam);

	return TRUE;
}

BOOL CProductsView::OnDeselectFlatProductPaperInfo(CDisplayItem* pDI, CPoint point, LPARAM lParam)	// object(s) has been selected
{
	//m_InplaceComboBox.Deactivate();

	return TRUE;
}

BOOL CProductsView::OnActivateFlatProductPaperInfo(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(NULL);//pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect;

	CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex((int)di.m_pData);

	m_InplaceComboBox.Activate(rect, InitializePaperInfoCombo, rFlatProduct.m_strPaper, 0, di);

	return TRUE;
}

void CProductsView::OnSelChangeInplaceComboBox() // Message from InplaceComboBox
{
	if (m_InplaceComboBox.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPartPaperInfo))
		UpdateBoundProductPartPaperInfo();
	else
	if (m_InplaceComboBox.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CProductsView, FlatProductPaperInfo))
		UpdateFlatProductPaperInfo();
}

void CProductsView::InitializePaperInfoCombo(CComboBox* pBox)
{
	POSITION pos = theApp.m_paperDefsList.GetHeadPosition();
	while (pos)
		pBox->AddString(theApp.m_paperDefsList.GetNext(pos).m_strName);
}

void CProductsView::UpdateBoundProductPartPaperInfo()
{
	m_InplaceComboBox.Deactivate();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPartPaperInfo));
	while (pDI)
	{
		int nProductPartIndex = (int)pDI->m_pData;
		CBoundProductPart& rBoundProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
		if ( ! rBoundProductPart.IsNull())
		{
			rBoundProductPart.m_strPaper = m_InplaceComboBox.GetSelString();
			pDoc->m_Bookblock.m_FoldSheetList.TakeOverProductPartPaperName(nProductPartIndex, rBoundProductPart.m_strPaper);
		}

		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPartPaperInfo));
	}

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);										

	//InvalidateRect(m_InplaceComboBox.m_di.m_BoundingRect);
	Invalidate();
	UpdateWindow();	

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintComponentsView),		NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL);	
}

void CProductsView::UpdateFlatProductPaperInfo()
{
	m_InplaceComboBox.Deactivate();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, FlatProductPaperInfo));
	while (pDI)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex((int)pDI->m_pData);
		if ( ! rFlatProduct.IsNull())
		{
			rFlatProduct.m_strPaper = m_InplaceComboBox.GetSelString();
		}

		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CProductsView, FlatProductPaperInfo));
	}

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);										

	//InvalidateRect(m_InplaceComboBox.m_di.m_BoundingRect);
	Invalidate();
	UpdateWindow();	

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintComponentsView),		NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),	NULL, 0, NULL);	
}

BOOL CProductsView::OnMultiSelect()
{
    CDisplayItem* pDI = m_DisplayList.GetLastSelectedItem();
    if (pDI)
    {
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CProductsView, FlatProductDesiredQuantity)))
			OnActivateFlatProductDesiredQuantity(pDI, CPoint(0,0), 0);
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CProductsView, FlatProductPaperInfo)))
			OnActivateFlatProductPaperInfo(pDI, CPoint(0,0), 0);
	}

	return TRUE;
}

CBoundProduct& CProductsView::GetActiveBoundProduct()
{
	static private CBoundProduct nullBoundProduct;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullBoundProduct;
	if (pDoc->m_boundProducts.GetSize() <= 0)
		return nullBoundProduct;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPart));
	if ( ! pDI)
	{
		pDI = m_DisplayList.GetItemFromData((void*)0, (void*)0, DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPart));
		if (pDI)
			m_DisplayList.SelectItem(*pDI);
	}
	if ( ! pDI)
		return pDoc->GetBoundProduct(0);
	else
		return pDoc->GetBoundProduct((int)pDI->m_pAuxData);
}

CBoundProductPart& CProductsView::GetActiveBoundProductPart()
{
	static private CBoundProductPart nullBoundProductPart;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullBoundProductPart;
	if (pDoc->m_boundProducts.GetSize() <= 0)
		return nullBoundProductPart;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPart));
	if ( ! pDI)
	{
		pDI = m_DisplayList.GetItemFromData((void*)0, (void*)0, DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPart));
		if (pDI)
			m_DisplayList.SelectItem(*pDI);
	}
	if ( ! pDI)
		return pDoc->GetBoundProductPart(0);
	else
		return pDoc->GetBoundProductPart((int)pDI->m_pData);
}

BOOL CProductsView::OnSelectBoundProductPart(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex((int)pDI->m_pData);//GetSelFlatProduct();
	if (rFlatProduct.IsNull())
		return FALSE;

	CPrintSheetPlanningView* pPlanningView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	if (pPlanningView)
	{
		int			  nComponentRefIndex	= (int)pDI->m_pData;//pDoc->m_flatProducts.FindProductIndex(pFlatProduct);
		CPrintSheet*  pPrintSheet			= rFlatProduct.GetFirstPrintSheet();
		while (pPrintSheet)
		{
			CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			if ( ! pLayout)
				return FALSE;

			POSITION pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
				if ( ! rObject.IsFlatProduct())
					continue;
				if (rObject.m_nComponentRefIndex == nComponentRefIndex)
				{
					CDisplayItem* pDISelect = pPlanningView->m_DisplayList.GetItemFromData((void*)&rObject, (void*)pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutObject));
					if (pDISelect)
					{
						pDISelect->m_nState = CDisplayItem::Selected;
						pPlanningView->m_DisplayList.InvalidateItem(pDISelect);
					}
				}
			}

			pPrintSheet	= rFlatProduct.GetNextPrintSheet(pPrintSheet);
		}
	}

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		int			  nComponentRefIndex	= (int)pDI->m_pData;//pDoc->m_flatProducts.FindProductIndex(pFlatProduct);
		CPrintSheet*  pPrintSheet			= rFlatProduct.GetFirstPrintSheet();
		while (pPrintSheet)
		{
			CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			if ( ! pLayout)
				return FALSE;

			POSITION pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
				if ( ! rObject.IsFlatProduct())
					continue;
				if (rObject.m_nComponentRefIndex == nComponentRefIndex)
				{
					CDisplayItem* pDISelect = pView->m_DisplayList.GetItemFromData((void*)&rObject, (void*)pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
					if (pDISelect)
					{
						pDISelect->m_nState = CDisplayItem::Selected;
						pView->m_DisplayList.InvalidateItem(pDISelect);
					}
				}
			}

			pPrintSheet	= rFlatProduct.GetNextPrintSheet(pPrintSheet);
		}
	}

	return TRUE;
}

BOOL CProductsView::OnDeselectBoundProductPart(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheetPlanningView* pPrintSheetPlanningView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	if (pPrintSheetPlanningView)
	{
		pPrintSheetPlanningView->m_DisplayList.DeselectAll();
		pPrintSheetPlanningView->m_DisplayList.InvalidateItem();
		pPrintSheetPlanningView->UpdateWindow();
	}
	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pPrintSheetView)
	{
		pPrintSheetView->m_DisplayList.DeselectAll();
		pPrintSheetView->m_DisplayList.InvalidateItem();
		pPrintSheetView->UpdateWindow();
	}

	return TRUE;
}

BOOL CProductsView::OnActivateBoundProductPart(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	theApp.UndoSave();

	CBoundProduct& rBoundProduct = GetActiveBoundProduct();

	pDI->m_nState = CDisplayItem::Normal;

	CString strOldFormatName	= rBoundProduct.m_strFormatName;
	float	fOldWidth			= rBoundProduct.m_fTrimSizeWidth;
	float	fOldHeight			= rBoundProduct.m_fTrimSizeHeight;
	int		nOldPageOrientation = rBoundProduct.m_nPageOrientation;
	int		nOldDesiredQuantity = rBoundProduct.GetDesiredQuantity();
	int		nOldNumProductParts = rBoundProduct.m_parts.GetSize();
	
	CDlgBoundProduct dlgBoundProduct;
	dlgBoundProduct.m_boundProduct = rBoundProduct;
	if (dlgBoundProduct.DoModal() == IDCANCEL)
		return FALSE;

	rBoundProduct = dlgBoundProduct.m_boundProduct;

	BOOL bReject = FALSE;
	if (dlgBoundProduct.PageFormatChanged())
		if (AfxMessageBox(IDS_MODIFY_PRODUCTPART_FORMAT, MB_YESNO) == IDNO)
			bReject = TRUE;
	if (bReject)
	{
		rBoundProduct.m_strFormatName		= strOldFormatName;
		rBoundProduct.m_fTrimSizeWidth		= fOldWidth;	
		rBoundProduct.m_fTrimSizeHeight		= fOldHeight;					
		rBoundProduct.m_nPageOrientation	= nOldPageOrientation;
	}
	else
	{
		if (dlgBoundProduct.PageFormatChanged())
			rBoundProduct.ChangePageFormat(rBoundProduct.m_strFormatName, rBoundProduct.m_fTrimSizeWidth, rBoundProduct.m_fTrimSizeHeight, rBoundProduct.m_nPageOrientation, -rBoundProduct.GetIndex() - 1);
	}

	//BOOL bBindingChanged = (rBoundProduct.m_bindingDefs.m_nBinding != dlgBoundProduct.m_boundProduct.m_bindingDefs.m_nBinding) ? TRUE : FALSE;
	//
	//if (bBindingChanged)
	//{
	//	for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
	//	{
	//		switch(rBoundProduct.m_parts[i].m_bindingDefs.m_nBinding)
	//		{
	//		case 0:	 pDoc->m_Bookblock.ModifyBindingMethod(CBindingDefs::PerfectBound,		rBoundProduct.TransformLocalToGlobalPartIndex(i)); break;
	//		case 1:	 pDoc->m_Bookblock.ModifyBindingMethod(CBindingDefs::SaddleStitched,	rBoundProduct.TransformLocalToGlobalPartIndex(i)); break;
	//		default: return;
	//		}
	//		pDoc->m_Bookblock.Reorganize(NULL, NULL);
	//		pDoc->m_Bookblock.ReNumberFoldSheets(-rBoundProduct.GetIndex() - 1);
	//		pDoc->m_PageTemplateList.SortPages();
	//	}
	//}

	if (nOldDesiredQuantity != dlgBoundProduct.m_boundProduct.GetDesiredQuantity())
	{
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			rBoundProduct.m_parts[i].SetDesiredQuantity(dlgBoundProduct.m_boundProduct.GetDesiredQuantity());
			rBoundProduct.m_parts[i].SetFoldSheetsDesiredQuantity(dlgBoundProduct.m_boundProduct.GetDesiredQuantity());
		}
	}

	int nNewProductPartIndex = nOldNumProductParts;
	pDoc->m_PageTemplateList.ShiftProductPartIndices(nNewProductPartIndex, rBoundProduct.m_parts.GetSize() - nOldNumProductParts);
	pDoc->m_Bookblock.ShiftProductPartIndices(nNewProductPartIndex, rBoundProduct.m_parts.GetSize() - nOldNumProductParts);

	int nProductIndex = rBoundProduct.GetIndex();
	for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
	{
		if (i >= nOldNumProductParts)
			pDoc->m_PageTemplateList.InsertProductPartPages(rBoundProduct.m_parts[i].m_nNumPages, nProductIndex, rBoundProduct.m_parts[i].GetIndex());
		else
			pDoc->m_PageTemplateList.AdjustNumPages(rBoundProduct.m_parts[i].m_nNumPages, nProductIndex, rBoundProduct.m_parts[i].GetIndex());
	}
	pDoc->m_PageSourceList.InitializePageTemplateRefs();

	pDoc->SetModifiedFlag();

	m_DisplayList.Invalidate();
	pDoc->UpdateAllViews(NULL);

	return TRUE;
}

BOOL CProductsView::OnMouseMoveBoundProductPart(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CProductsView::GetBookblockSelected()
{
	return (m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, Bookblock))) ? TRUE : FALSE;
}

BOOL CProductsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	return CScrollView::PreTranslateMessage(pMsg);
}

void CProductsView::OnBoundProductDuplicate()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPart));
	if ( ! pDI)
		return;
	CBoundProductPart& rProductPart = pDoc->GetBoundProductPart((int)pDI->m_pData);
	CBoundProduct&	   rProduct		= rProductPart.GetBoundProduct();
	
	pDoc->m_boundProducts.AddTail(rProduct);

	CBoundProduct& rNewBoundProduct = pDoc->m_boundProducts.GetTail();
	CString strDuplicate; strDuplicate.LoadString(IDS_DUPLICATE);
	rNewBoundProduct.m_strProductName += _T(" - ") + strDuplicate;

	int nProductIndex = rNewBoundProduct.GetIndex();
	for (int i = 0; i < rNewBoundProduct.m_parts.GetSize(); i++)
	{
		pDoc->m_PageTemplateList.AdjustNumPages(rNewBoundProduct.m_parts[i].m_nNumPages, nProductIndex, rNewBoundProduct.m_parts[i].GetIndex());
	}
	pDoc->m_PageSourceList.InitializePageTemplateRefs();

	pDoc->SetModifiedFlag();

	pDoc->UpdateAllViews(NULL);
}

void CProductsView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 20)
		hdn->pitem->cxy = 20;
}

void CProductsView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	SetProductTableHeaderWidth(hdn->iItem, hdn->pitem->cxy);

	if (m_headerColumnWidths.GetSize() > hdn->iItem)
		m_headerColumnWidths[hdn->iItem] = hdn->pitem->cxy; 

	m_nHeaderItemsTotalWidth = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
		m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];

	CRect clientRect;
	GetClientRect(&clientRect);

	InvalidateRect(clientRect);
	UpdateWindow(); 
}

void CProductsView::SetProductTableHeaderWidth(int nIndex, int nValue)
{
	if (nIndex >= 20)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\ProductViewHeader");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			columnWidthArray[nIndex] = nValue;			
		else
		{
			ZeroMemory(columnWidthArray, sizeof(columnWidthArray));
			columnWidthArray[nIndex] = nValue;			
		}

		dwSize = sizeof(columnWidthArray);
		RegSetValueEx(hKey, _T("Columns"), 0, REG_BINARY, (LPBYTE)&columnWidthArray, dwSize);

		RegCloseKey(hKey);
	}
}

int CProductsView::GetProductTableHeaderWidth(int nIndex, int nDefault)
{
	if (nIndex >= 20)
		return nDefault;

	int nWidth = nDefault;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\ProductViewHeader");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			nWidth = columnWidthArray[nIndex];			
		else
			nWidth = nDefault;

		RegCloseKey(hKey);
	}

	if ( (nWidth <= 10) || (nWidth > 1000) )
		nWidth = nDefault;

	return nWidth;
}

void CProductsView::OnDeleteProduct()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	theApp.UndoSave();

	int				nNumRemoved = 0;
	BOOL			bUserAsked  = FALSE;
	CDisplayItem*	pDI			= m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPart));
	while (pDI)
	{
		CBoundProductPart& rBoundProductPart = pDoc->m_boundProducts.GetProductPart((int)pDI->m_pData - nNumRemoved);
		if ( ! bUserAsked)
		{
			bUserAsked = TRUE;
			CString strMsg;
			AfxFormatString1(strMsg, IDS_DELETE_PRODUCTPART_MSG, rBoundProductPart.m_strName);
			if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
				break;
		}

		pDoc->m_boundProducts.RemoveProductPart(rBoundProductPart);
		nNumRemoved++;

		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CProductsView, BoundProductPart));
	}

	nNumRemoved	= 0;
	bUserAsked	= FALSE;
	pDI			= m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
	while (pDI)
	{
		if ( ! bUserAsked)
		{ 
			bUserAsked = TRUE;
			if (AfxMessageBox(IDS_REALLY_REMOVE_LABELTYPE, MB_YESNO) == IDNO)
				break;
		}

		int nIndex = (int)pDI->m_pData - nNumRemoved;
		POSITION pos = pDoc->m_flatProducts.FindIndex(nIndex);
		if (pos)
		{
			pDoc->m_flatProducts.RemoveAt(pos);
			pDoc->m_PrintSheetList.RemoveFlatProductType(nIndex);
			POSITION pos1 = pDoc->m_PageTemplateList.FindLabelPos(nIndex, FRONTSIDE);
			POSITION pos2 = pDoc->m_PageTemplateList.FindLabelPos(nIndex, BACKSIDE);
			pDoc->m_PageTemplateList.RemoveAt(pos1);
			pDoc->m_PageTemplateList.RemoveAt(pos2);
			nNumRemoved++;
		}

		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
	}

	pDoc->m_PageTemplateList.FindPrintSheetLocations();

	m_DisplayList.DeselectAll();
	m_DisplayList.Invalidate();

	pDoc->SetModifiedFlag();
	pDoc->UpdateAllViews(NULL);
}
