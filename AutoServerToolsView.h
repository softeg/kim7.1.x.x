#pragma once

#include "OwnerDrawnButtonList.h"


// CAutoServerToolsView-Formularansicht

class CAutoServerToolsView : public CFormView
{
	DECLARE_DYNCREATE(CAutoServerToolsView)

protected:
	CAutoServerToolsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CAutoServerToolsView();

public:
	enum { IDD = IDD_AUTOSERVERTOOLSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


public:
	CStatic					m_toolBarFrame;
	COwnerDrawnButtonList	m_toolBarButtons;


public:
	static void	DrawItemToolBar(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void	UpdateBackgroundToolBar(CDC* pDC, CRect rcRect, CWnd* pWnd);
	int			GetActJobStatus();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnSelectView();
	afx_msg void OnNew();
	afx_msg void OnRemove();
	afx_msg void OnStartOutput();
	afx_msg void OnRestartJob();
	afx_msg void OnAbort();
	afx_msg void OnUpdateSelectView(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNew(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemove(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStartOutput(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRestartJob(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAbort(CCmdUI* pCmdUI);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


