#if !defined(AFX_DLGPDFOUTPUTMEDIASETTINGS_H__CE248372_853A_11D3_8749_204C4F4F5020__INCLUDED_)
#define AFX_DLGPDFOUTPUTMEDIASETTINGS_H__CE248372_853A_11D3_8749_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPDFOutputMediaSettings.h : Header-Datei
//


class CCellDataForExposer
{
// Construction
public:
	CCellDataForExposer() {m_pPlate = NULL; m_lGridRow = m_lGridCol = 0; m_bDisabled = FALSE; m_bProcessed = FALSE;};
	CCellDataForExposer(class CPlate* pPlate, long lGridRow, long lGridCol, BOOL bDisabled = FALSE, BOOL bProcessed = FALSE) 
	{
		m_pPlate	 = pPlate; 
		m_lGridRow	 = lGridRow;
		m_lGridCol	 = lGridCol;
		m_bDisabled  = bDisabled;
		m_bProcessed = bProcessed;
	};

	class CPlate* m_pPlate;
	long		  m_lGridRow, m_lGridCol;
	BOOL		  m_bDisabled;
	BOOL		  m_bProcessed;
};


class CCellDataListForExposer : public CArray <CCellDataForExposer, CCellDataForExposer&>
{
public:
	long m_lRows,		m_lCols;
	long m_lOffsetRows, m_lOffsetCols;


// Implementation
public:
	void Reset(long lOffsetRows, long lOffsetCols)
	{
		m_lRows = m_lCols = 0;
		m_lOffsetRows = lOffsetRows; m_lOffsetCols = lOffsetCols;
		CArray <CCellDataForExposer, CCellDataForExposer&>::RemoveAll();
	};

	CCellDataForExposer* GetData(long lRow, long lCol) 
	{
		return &ElementAt(lRow * m_lCols + lCol);
	};

	int Add(CCellDataForExposer& rCellDataForExposer) 
	{
		m_lRows = max(m_lRows, rCellDataForExposer.m_lGridRow - m_lOffsetRows + 1);
		m_lCols = max(m_lCols, rCellDataForExposer.m_lGridCol - m_lOffsetCols + 1);
		return CArray <CCellDataForExposer, CCellDataForExposer&>::Add(rCellDataForExposer);
	};
};


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DlgPDFOutputMediaSettings

class CDlgPDFOutputMediaSettings : public CDialog
{
// Konstruktion
public:
	CDlgPDFOutputMediaSettings(CWnd* pParent = NULL, BOOL bRunModal = FALSE);   // Standardkonstruktor
	~CDlgPDFOutputMediaSettings();


//Attributes:
public:
	BOOL				m_bRunModal;
	CWnd*				m_pWndMsgTarget;
	BOOL				m_bNoSheetSelection;
	int					m_nOutputTarget;
	CPDFTargetList		m_pdfTargetList;
	CString				m_strPDFTargetName;
	BOOL				m_bOutputCuttingData;

protected:
	CDlgPDFTargetProps	m_dlgPDFTargetProps;
	CString				m_strMediaName;
	float				m_fMediaWidth;
	float				m_fMediaHeight;


friend class CPrintSheetTableView;
friend class CBVFoldSheetListCtrl;

//Operations:
protected:
	void		InitExposerComboBox();
	BOOL		DifferentTargetsSelected(CString& strSelectedTargets);
	void		SelectPDFTargetCombo(CString strPDFTargetName = _T(""));
	void		UpdatePrintSheetView();
	void		UpdatePrintSheetTableView();
	static void	PDFOutputThread(void* pParam);
	CString		BuildNewPDFTargetName(int nTargetType, const CString strName = _T(""));
	static void	NewPDFTargetPropsCallback   (CDlgPDFTargetProps* pDlg, CWnd* pCallWnd, int nIDReturn);
	static void	ModifyPDFTargetPropsCallback(CDlgPDFTargetProps* pDlg, CWnd* pCallWnd, int nIDReturn);
	void		AddNewTarget(int nTargetType);
public:
	CDlgPDFTargetProps* GetDlgPDFTargetProps();
	BOOL				PrintSheetIsSelected(CPrintSheet* pPrintSheet, int nSide = 0);
	BOOL				PrintSheetIsEffected(CPrintSheet* pPrintSheet);
	CPrintSheet*		GetFirstEffectedPrintSheet();
	CMediaSize*			GetCurMedia(CPrintSheet* pPrintSheet);
	CString				GetCurPDFTargetName(CPrintSheet* pPrintSheet);
	float				GetCurMediaWidth();
	float				GetCurMediaHeight();
	int					GetCurTileIndex();
	void				GetTileBBox	   (CPrintSheet* pPrintSheet, int nSide, int nTileIndex, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop);
	void				GetTileBBox	   (CPrintSheet* pPrintSheet, int nSide,				 float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop);
	void				GetAllTilesBBox(CPrintSheet* pPrintSheet, int nSide,				 float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop);
	void				DoOutputBookblock();
	void				DoOutputSheets();
	static BOOL			OutputFormatExceeded(float fWidth, float fHeight);


//	m_fXPos  m_fYPos  m_nPortraitLandscape  m_nScale	-> needed in PrintSheetView
//	m_fXPos  m_fYPos									-> needed in PrintSheetViewDragDrop
//	m_fXPos  m_fYPos  m_nPortraitLandscape  m_nScale	-> needed in PrintSheetViewDraw

public:
// Dialogfelddaten
	//{{AFX_DATA(CDlgPDFOutputMediaSettings)
	enum { IDD = IDD_PDFOUTPUT_MEDIA_SETTINGS };
	CComboBox	m_sheetTypesCombo;
	CComboBoxEx	m_pdfTargetCombo;
	CString	m_strMediaLongName;
	CString	m_strLocation;
	int		m_nWhatSheets;
	CString m_strWhatLayout;
	CString m_strJobNotes;
	CString m_strCuttingDataFolder;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPDFOutputMediaSettings)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPDFOutputMediaSettings)
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSelchangeExposerCombo();
	afx_msg void OnPortrait();
	afx_msg void OnLandscape();
	afx_msg void OnPrinterProps();
	afx_msg void OnNew();
	afx_msg void OnCopy();
	afx_msg void OnDel();
	afx_msg void OnSetfocusSheettypesCombo();
	afx_msg void OnSheetsAll();
	afx_msg void OnSheetsAllOfType();
	afx_msg void OnSheetsSelected();
	afx_msg void OnSelchangeSheettypesCombo();
	afx_msg void OnPickOutputLocation();
	afx_msg void OnUpdateCopyButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDelButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePrinterProps(CCmdUI* pCmdUI);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
#ifdef CTP
	afx_msg void OnIpuAllcolors();
	afx_msg void OnIpuSinglecolors();
#endif
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnNewPDFTargetFolder();
	afx_msg void OnNewPDFTargetPrinter();
	afx_msg void OnNewJDFTargetFolder();
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPDFOUTPUTMEDIASETTINGS_H__CE248372_853A_11D3_8749_204C4F4F5020__INCLUDED_
