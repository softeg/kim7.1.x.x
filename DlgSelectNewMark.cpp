// DlgSelectNewMark.cpp: Implementierungsdatei
//
// (C) Tegeler-Software-Engineering 02/21/2000


#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"
#include "PrintSheetMarksView.h"
#include "DlgMarkSet.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSelectNewMark 


CDlgSelectNewMark::CDlgSelectNewMark(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSelectNewMark::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSelectNewMark)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void CDlgSelectNewMark::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSelectNewMark)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}

inline BOOL CDlgSelectNewMark::IsChildOfMarksView()
{
	if (m_pParent)
		if (m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetMarksView)))
			return TRUE;

	return FALSE;
}

BEGIN_MESSAGE_MAP(CDlgSelectNewMark, CDialog)
	//{{AFX_MSG_MAP(CDlgSelectNewMark)
	ON_COMMAND(ID_NEW_CUSTOMMARK,	OnNewCustomMark)
	ON_COMMAND(ID_NEW_TEXTMARK,		OnNewTextMark)
	ON_COMMAND(ID_NEW_SECTIONMARK,	OnNewSectionMark)
	ON_COMMAND(ID_NEW_CROPMARK,		OnNewCropMark)
	ON_COMMAND(ID_NEW_FOLDMARK,		OnNewFoldMark)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgSelectNewMark 

BOOL CDlgSelectNewMark::OnInitDialog() 
{
	const int nNumButtons = 5;

	CDialog::OnInitDialog();

	m_toolBar.Create(WS_VISIBLE | WS_CHILD | CCS_NODIVIDER | CCS_VERT | CCS_NOPARENTALIGN | TBSTYLE_FLAT | TBSTYLE_LIST | TBSTYLE_WRAPABLE, 
					 CRect(0,0,0,0), this, IDR_NEWMARK_TYPES);
	
	m_toolBar.SetBitmapSize(CSize(20,20));

	VERIFY(m_toolBar.AddBitmap(nNumButtons, IDR_NEWMARK_TYPES) != -1);

	m_pTBButtons = new TBBUTTON[nNumButtons];

	for (int nIndex = 0; nIndex < nNumButtons; nIndex++)
	{
		m_pTBButtons[nIndex].fsState = TBSTATE_ENABLED;
		m_pTBButtons[nIndex].fsStyle = TBSTYLE_BUTTON;		
		m_pTBButtons[nIndex].dwData  = 0;
		m_pTBButtons[nIndex].iBitmap = nIndex;
		m_pTBButtons[nIndex].iString = nIndex;
	}

	CString strCustomMark, strTextMark, strSectionMark, strCropMark, strFoldMark;
#ifdef PDF
	strCustomMark.LoadString(IDS_MARKTYPE_CUSTOM_PDF);
#else
#ifdef POSTSCRIPT
	strCustomMark.LoadString(IDS_MARKTYPE_CUSTOM_PS);
#else
	strCustomMark.LoadString(IDS_MARKTYPE_CUSTOM_BMP);
#endif
#endif
	strTextMark.LoadString	 (IDS_MARKTYPE_TEXT);
	strSectionMark.LoadString(IDS_MARKTYPE_SECTION);
	strCropMark.LoadString	 (IDS_MARKTYPE_CROP);
	strFoldMark.LoadString	 (IDS_MARKTYPE_FOLD);

	int nStringLength = strCustomMark.GetLength() + 1;
	TCHAR * pString0  = strCustomMark.GetBufferSetLength(nStringLength);	pString0[nStringLength] = 0;

		nStringLength = strTextMark.GetLength() + 1;
	TCHAR * pString1  = strTextMark.GetBufferSetLength(nStringLength);		pString1[nStringLength] = 0;

		nStringLength = strSectionMark.GetLength() + 1;
	TCHAR * pString2  = strSectionMark.GetBufferSetLength(nStringLength);	pString2[nStringLength] = 0;

		nStringLength = strCropMark.GetLength() + 1;
	TCHAR * pString3  = strCropMark.GetBufferSetLength(nStringLength);		pString3[nStringLength] = 0;

		nStringLength = strFoldMark.GetLength() + 1;
	TCHAR * pString4  = strFoldMark.GetBufferSetLength(nStringLength);		pString4[nStringLength] = 0;

	VERIFY((m_pTBButtons[0].iString = m_toolBar.AddStrings(pString0)) != -1);
	VERIFY((m_pTBButtons[1].iString = m_toolBar.AddStrings(pString1)) != -1);
	VERIFY((m_pTBButtons[2].iString = m_toolBar.AddStrings(pString2)) != -1);
	VERIFY((m_pTBButtons[3].iString = m_toolBar.AddStrings(pString3)) != -1);
	VERIFY((m_pTBButtons[4].iString = m_toolBar.AddStrings(pString4)) != -1);

	strCustomMark.ReleaseBuffer(); strTextMark.ReleaseBuffer(); strSectionMark.ReleaseBuffer(); strCropMark.ReleaseBuffer(); strFoldMark.ReleaseBuffer();

	m_pTBButtons[0].idCommand = ID_NEW_CUSTOMMARK;
	m_pTBButtons[1].idCommand = ID_NEW_TEXTMARK;
	m_pTBButtons[2].idCommand = ID_NEW_SECTIONMARK;
	m_pTBButtons[3].idCommand = ID_NEW_CROPMARK;
	m_pTBButtons[4].idCommand = ID_NEW_FOLDMARK;

	VERIFY(m_toolBar.AddButtons(nNumButtons, m_pTBButtons));

	m_toolBar.SetRows(nNumButtons, TRUE, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
} 

void CDlgSelectNewMark::OnNewCustomMark() 
{
	ShowWindow(SW_HIDE);
	if (IsChildOfMarksView())
		((CPrintSheetMarksView*)m_pParent)->AddNewMark(ID_NEW_CUSTOMMARK);
	else
		((CDlgMarkSet*)m_pParent)->AddNewMark(ID_NEW_CUSTOMMARK);
}

void CDlgSelectNewMark::OnNewTextMark() 
{
	ShowWindow(SW_HIDE);
	if (IsChildOfMarksView())
		((CPrintSheetMarksView*)m_pParent)->AddNewMark(ID_NEW_TEXTMARK);
	else
		((CDlgMarkSet*)m_pParent)->AddNewMark(ID_NEW_TEXTMARK);
}

void CDlgSelectNewMark::OnNewSectionMark() 
{
	ShowWindow(SW_HIDE);
	if (IsChildOfMarksView())
		((CPrintSheetMarksView*)m_pParent)->AddNewMark(ID_NEW_SECTIONMARK);
	else
		((CDlgMarkSet*)m_pParent)->AddNewMark(ID_NEW_SECTIONMARK);
}

void CDlgSelectNewMark::OnNewCropMark() 
{
	ShowWindow(SW_HIDE);
	if (IsChildOfMarksView())
		((CPrintSheetMarksView*)m_pParent)->AddNewMark(ID_NEW_CROPMARK);
	else
		((CDlgMarkSet*)m_pParent)->AddNewMark(ID_NEW_CROPMARK);
}

void CDlgSelectNewMark::OnNewFoldMark() 
{
	ShowWindow(SW_HIDE);
	if (IsChildOfMarksView())
		((CPrintSheetMarksView*)m_pParent)->AddNewMark(ID_NEW_FOLDMARK);
	else
		((CDlgMarkSet*)m_pParent)->AddNewMark(ID_NEW_FOLDMARK);
}

BOOL CDlgSelectNewMark::PreTranslateMessage(MSG* pMsg) 
{
	// If we get a keyboard or mouse message, hide the toolbar.
	if (pMsg->message == WM_LBUTTONDOWN || pMsg->message == WM_RBUTTONDOWN || pMsg->message == WM_MBUTTONDOWN ||
	    pMsg->message == WM_NCLBUTTONDOWN || pMsg->message == WM_NCRBUTTONDOWN || pMsg->message == WM_NCMBUTTONDOWN)
	{
		CPoint ptMouse;
		GetCursorPos(&ptMouse);
		CRect rect;
		GetWindowRect(rect);
		if ( ! rect.PtInRect(ptMouse))
		{
			ShowWindow(SW_HIDE); 
			return TRUE;	// message handled here
		}
	}
	else
		if (pMsg->message == WM_KEYDOWN || pMsg->message == WM_SYSKEYDOWN)
		{
			ShowWindow(SW_HIDE);
			return TRUE;	// message handled here
		}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgSelectNewMark::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	if (bShow)
	{
		m_toolBar.SetCapture();

		CSize sizeMax;
		CRect rect;
		m_toolBar.GetMaxSize(&sizeMax);
		GetWindowRect(rect);
		MoveWindow(rect.left, rect.top, sizeMax.cx + 6, sizeMax.cy + 6, FALSE);
	}
	else
	{
		ReleaseCapture();
		((CButton*)m_pParent->GetDlgItem(IDC_MARK_NEW))->SetCheck(0);
		m_pParent->GetDlgItem(IDC_MARK_LIST)->SetFocus();
	}

	CDialog::OnShowWindow(bShow, nStatus);
}
