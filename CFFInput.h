// CFFInput.h: Schnittstelle f�r die Klasse CCFFInput.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CFFINPUT_H__51F0C743_621E_4272_92D9_BDB99D0CB495__INCLUDED_)
#define AFX_CFFINPUT_H__51F0C743_621E_4272_92D9_BDB99D0CB495__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define ZWEI_PI 6.283185307f

#define BEZLAENGE 256
#define ANZSYM    16
#define EOL       0   /* end of line */


/* ACHTUNG! Beim hinzufuegen neuer Symbole beachten: ANZSYM erhoehen und */
/*          Symbol in First-Liste eintragen */

enum cffsymbole {EOFSYM, MAINSYM, ENDSYM, UMSYM, UISYM, LLSYM, URSYM,
SCALESYM, LINESYM, ARCSYM, TEXTSYM, CALLSYM, SUBSYM,
FINISHSYM, LAYOUTSYM, KOMMA, IDENT, NUM, NUL};

extern int cffsymtab[];
extern TCHAR *cffreswort[];

enum zustand {MAINCOMMAND, SUBCOMMAND};


typedef
struct sub
{
	TCHAR name[10];
	float maxx;
	float maxy;
	float minx;
	float miny;
}SUBDATA;





class CCFFInput  
{
public:
	CCFFInput();
	CCFFInput(int nProductPartIndex);
	virtual ~CCFFInput();
	
public:
	CStdioFile m_CFFFile;
	
	SUBDATA subdata[25];
	TCHAR	ch, id[BEZLAENGE+1], last_string[BEZLAENGE+1];
	int		zeilennummer, sym, nsub, linetype;
	float	zahl, startx, starty, endx, endy, centx, centy, direct,
		angle, scalex, scaley;
	float	vorx, vory; /* Ende der vorigen Linie */
	float	ges_netto_xw0, ges_netto_yw0, ges_netto_xw1, ges_netto_yw1, global_scalex, global_scaley;
	
	CGraphicList m_graphicList;
	int			 m_nProductPartIndex;
	
public:
	BOOL DoImport(const CString& strFilePath);
	void ProcessSubroutines();
	void ProcessMainprocedure();
	void Mainprocedure();
	int	 Subroutine();
	int	 Syntaxcheck();
	void Maincommand();
	void Subcommand(CGraphicPath* pGraphicPath);
	void Lineparams(float *startx,float *starty,float *endx,float *endy);
	void Arcparams(float *startx,float *starty,float *endx,float *endy,float *centx,float *centy,float *direct);
	void Textparams();
	void Callparams(float *startx,float *starty,float *angle,float *scalex,float *scaley);
	void Attributes();
	void Coordinate(float *coordx,float *coordy);
	int  Brigde();
	int  Firstcff(int zustand,int symbol);
	int  SucheSymbol(TCHAR *string, int symtab[], TCHAR *reswort[]);
	int	 LinetypeUebernehmen(int linetype);
	void BoxForLine(float startx,float starty,float endx,float endy);
	void BoxForArc(float x1,float y1,float x2,float y2,float xc,float yc,float direction);
	float FindLength(float x1,float y1,float x2,float y2);
	float FindAngle(float x1,float y1,float x2,float y2);
	int   Quadrant(float angle);
	float Maximum(float x1,float x2,float x3,float x4,float x5,float x6);
	float Minimum(float x1,float x2,float x3,float x4,float x5,float x6);
	void  ProcessCall(TCHAR *subname,float absx,float absy,float angle,float scalex,float scaley);
	void  DoTransformation(float absx,float absy,float angle,float scalex,float scaley,float *firstx,float *firsty);
	void  CFFToLabel(float xw0,float yw0,float xw1,float yw1,float angle,float scalex,float scaley,TCHAR *name, int sorte);
	void  GetSymbol();
	void  GetString(int ende);
	void  GetNextCh();
	void  Error();
};

#endif // !defined(AFX_CFFINPUT_H__51F0C743_621E_4272_92D9_BDB99D0CB495__INCLUDED_)
