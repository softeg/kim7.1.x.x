// JobSettingsPreviewWnd.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "JobSettingsPreviewWnd.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPreviewWnd

CJobSettingsPreviewWnd::CJobSettingsPreviewWnd()
{
}

CJobSettingsPreviewWnd::~CJobSettingsPreviewWnd()
{
}


BEGIN_MESSAGE_MAP(CJobSettingsPreviewWnd, CWnd)
	//{{AFX_MSG_MAP(CJobSettingsPreviewWnd)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPreviewWnd message handlers

void CJobSettingsPreviewWnd::OnPaint() 
{
	// TODO: Add your custom preview paint code.
	// For an example, we are drawing a blue ellipse.

	CPaintDC dc(this); // device context for painting
	CRect rect;
	GetClientRect(rect);

	CBrush brushNew(RGB(0,0,255));
	CBrush* pBrushOld = dc.SelectObject(&brushNew);
	dc.Ellipse(rect);
	dc.SelectObject(pBrushOld);
}

BOOL CJobSettingsPreviewWnd::OnEraseBkgnd(CDC* pDC) 
{
	// Use the same background color as that of the dialog
	//  (property sheet).

	CWnd* pParentWnd = GetParent();
	HBRUSH hBrush = (HBRUSH)pParentWnd->SendMessage(WM_CTLCOLORDLG,
		(WPARAM)pDC->m_hDC, (LPARAM)pParentWnd->m_hWnd);
	CRect rect;
	GetClientRect(rect);
	pDC->FillRect(&rect, CBrush::FromHandle(hBrush));
	return TRUE;
}
