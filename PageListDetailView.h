#if !defined(AFX_PAGELISTDETAILVIEW_H__AD192F81_204B_11D1_BDCB_444553540000__INCLUDED_)
#define AFX_PAGELISTDETAILVIEW_H__AD192F81_204B_11D1_BDCB_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PageListDetailView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageListDetailView view

class CPageListDetailView : public CScrollView
{
protected:
	CPageListDetailView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPageListDetailView)

// Attributes
public:
	int					m_nColorFrameSelection;	// needed by DlgPageTemplateProps
	int					m_nColorSel;			// needed by DlgPageTemplateProps, PageListFrame, PageListView

protected:
	CPageSource*		m_pPageSource;
	BOOL				m_bInitialize;
	float				m_fScale;									 
	float				m_fDocSizeX;
	float				m_fDocSizeY;
	float				m_fBitmapLeft; 
	float				m_fBitmapTop; 
	float				m_fBitmapRight;
	float				m_fBitmapBottom; 
	float				m_fPageLeft; 
	float				m_fPageTop; 
	float				m_fPageRight;
	float				m_fPageBottom; 
	float				m_fTrimLeft; 
	float				m_fTrimTop; 
	float				m_fTrimRight;
	float				m_fTrimBottom; 
	float				m_fTrimBoxWidth;
	float				m_fTrimBoxHeight;
	int					m_nColorSeparationIndex;
	BOOL				m_bCheckRedFrame;
	BOOL				m_bCheckBlackFrame;
	BOOL				m_bCheckYellowFrame;
	BOOL				m_bCheckCyanFrame;
	CRect				m_RightStatusBar;
	CToolTipCtrl		m_tooltip;
#ifdef PDF
	CPdfReader			m_pdfReader;
#endif


// Operations
public:
	CPageTemplate*	GetPageTemplate();
	CLayoutObject*	GetLayoutObject();
	//CPageSource*	GetPageSource();
	CImpManDoc*		GetDocument();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageListDetailView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnInitialUpdate();     // first time after construct
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void PostNcDestroy();
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	//}}AFX_VIRTUAL

// Implementation
protected:
	void SetDocSize();
	virtual ~CPageListDetailView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:
	DECLARE_DISPLAY_LIST(CPageListDetailView, OnMultiSelect, NO, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CPageListDetailView, ColorButton, OnSelect, YES, OnDeselect, NO, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)

	static long	WorldToLP(float fValue) 
				{	switch (theApp.m_dwPlatformId)
					{
					case VER_PLATFORM_WIN32_NT:		 return (long)((float)(fValue * 100.0f + 0.5f));
					case VER_PLATFORM_WIN32_WINDOWS: return (long)((float)(fValue		   + 0.5f));
					default:						 return (long)((float)(fValue		   + 0.5f));
					};
				}

	void InitStatusBar(CDC* pDC, BOOL bCompleteWithDraw = TRUE);
	void ZoomWindow(const CPoint& rPoint, BOOL bRButton = FALSE);

	DECLARE_MESSAGE_MAP()
	// Generated message map functions
	//{{AFX_MSG(CPageListDetailView)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnBlackFrame();
	afx_msg void OnUpdateBlackFrame(CCmdUI* pCmdUI);
	afx_msg void OnCyanFrame();
	afx_msg void OnUpdateCyanFrame(CCmdUI* pCmdUI);
	afx_msg void OnRedFrame();
	afx_msg void OnUpdateRedFrame(CCmdUI* pCmdUI);
	afx_msg void OnYellowFrame();
	afx_msg void OnUpdateYellowFrame(CCmdUI* pCmdUI);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	afx_msg void OnNcPaint();
};

#ifndef _DEBUG  // debug version in PageListView.cpp
inline CImpManDoc* CPageListDetailView::GetDocument()
   { return (CImpManDoc*)m_pDocument; }
#endif


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAGELISTDETAILVIEW_H__AD192F81_204B_11D1_BDCB_444553540000__INCLUDED_)
