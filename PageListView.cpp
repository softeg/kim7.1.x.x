// PageListView.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PageListFrame.h"
#include "PageSourceListView.h"
#include "PageListTableView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#ifdef PDF
#include "PDFReader.h"
#endif
#include "PageListDetailView.h"
#include "PageListView.h" 
#include "InplaceEdit.h"	// needed in 'PrintSheetView.h'
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "DlgJobColorDefinitions.h"
#include "DlgPageTemplateProps.h"
#include "DlgTrimboxWarning.h"
#include "GraphicComponent.h"
#include "PageListTopToolsView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPageListView

IMPLEMENT_DYNCREATE(CPageListView, CScrollView)

CPageListView::CPageListView()
{
	m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CPageListView));
}			  

CPageListView::~CPageListView()
{
}


BEGIN_MESSAGE_MAP(CPageListView, CScrollView)
	//{{AFX_MSG_MAP(CPageListView)
	ON_WM_LBUTTONDOWN()
	ON_WM_CREATE()
	ON_WM_KEYDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_SETCURSOR()
	ON_WM_CONTEXTMENU()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageListView printing

BOOL CPageListView::OnPreparePrinting(CPrintInfo* pInfo)
{
    pInfo->SetMaxPage(1);	// TODO: Make correct print
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CPageListView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CPageListView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CPageListView drawing


void CPageListView::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return;

	BeginWaitCursor();

	m_DisplayList.BeginRegisterItems(pDC);

	int nProductPartIndex = GetCurProductPartIndex();
	//if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_printGroupList.GetSize()) )	// show all product parts
	//{
	//	int nBottom = DrawPagesApplicationBook(pDC);
	//			      DrawPagesApplicationLabels(pDC, nBottom - 40);
	//}
	//else
	{
		if (pDoc->GetFlatProductsIndex() == nProductPartIndex)
			DrawPagesApplicationLabels(pDC);
		else
			DrawPagesApplicationBook(pDC);
	}

	m_DisplayList.EndRegisterItems(pDC);

	EndWaitCursor();
}

int CPageListView::GetCurProductPartIndex()
{
	CPageListView* pView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));	// this function needs to be static -> get view 
	if (pView)
	{
		CPageListFrame* pFrame = (CPageListFrame*)pView->GetParentFrame();
		if (pFrame)
			return pFrame->GetActProductPartIndex();
		else
			return 0;
	}
	else
		return 0;
}

int g_nPrevProductGroupIndex = -1;

int CPageListView::DrawPagesApplicationBook(CDC* pDC)
{
	CImpManDoc* pDoc = GetDocument();
	if (pDoc->m_PageTemplateList.IsEmpty())
	{
		m_TemplateListSize = CSize(0,0);
		return 0;
	}

	int nProductPartIndex = GetCurProductPartIndex();

	int		 nIndex		= 0;
	int		 nTop		= m_TemplateListSize.cy - 10;
	int		 nMinBottom = nTop;
	POSITION pos		= pDoc->m_PageTemplateList.GetHeadPosition(nProductPartIndex);
	if (pos)
	{
		g_nPrevProductGroupIndex = -1;

		CPageTemplate& rFirstTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
		CPageTemplate& rLastTemplate  = pDoc->m_PageTemplateList.GetTail(nProductPartIndex);

		if (rFirstTemplate.m_nMultipageGroupNum >= 0)
			DrawPage(pDC, rLastTemplate, &nTop, &nMinBottom, LEFT, nIndex++);

		DrawPage(pDC, rFirstTemplate, &nTop, &nMinBottom, RIGHT, nIndex++);

		g_nPrevProductGroupIndex = rFirstTemplate.m_nProductPartIndex;

		while (pos)
		{
			CPageTemplate& rLeftTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
			if (!pos)	// don't draw last page here
				break;

			DrawPage(pDC, rLeftTemplate, &nTop, &nMinBottom, LEFT, nIndex++);
			g_nPrevProductGroupIndex = rLeftTemplate.m_nProductPartIndex;

			if (!pos)
				break;

			CPageTemplate& rRightTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);

			DrawPage(pDC, rRightTemplate, &nTop, &nMinBottom, RIGHT, nIndex++);
			g_nPrevProductGroupIndex = rRightTemplate.m_nProductPartIndex;
		}

		if (rFirstTemplate.m_nMultipageGroupNum < 0)
			DrawPage(pDC, rLastTemplate, &nTop, &nMinBottom, LEFT, nIndex++);
	}
	
	return nMinBottom;
}

int CPageListView::DrawPagesApplicationLabels(CDC* pDC, int nTopInit)
{
	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return nTopInit;
	if (pDoc->m_PageTemplateList.IsEmpty())
	{
		m_TemplateListSize = CSize(0,0);
		return nTopInit;
	}

	int nProductPartIndex = GetCurProductPartIndex();

	int		 nIndex		= 0;
	int		 nTop		= (nTopInit <= 0) ? m_TemplateListSize.cy - 10 : nTopInit;
	int		 nMinBottom = nTop + 10;
	POSITION pos		= pDoc->m_PageTemplateList.GetHeadPosition(nProductPartIndex);
	while (pos)
	{
		CPageTemplate& rFrontTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
		CPageTemplate& rBackTemplate  = (pos) ? pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex) : pDoc->m_PageTemplateList.GetAt(pos);
		if (rFrontTemplate.FlatProductHasBackSide())
		{
			DrawLabel(pDC, rFrontTemplate, &nTop, &nMinBottom, LEFT,  nIndex); nIndex++;
			DrawLabel(pDC, rBackTemplate,  &nTop, &nMinBottom, RIGHT, nIndex); nIndex++;
		}
		else
		{
			DrawLabel(pDC, rFrontTemplate, &nTop, &nMinBottom, LEFT, nIndex); nIndex += 2;
		}
	}

	return nTop;
}

BOOL CPageListView::DrawPage(CDC* pDC, CPageTemplate& rTemplate, int* nTop, int* nMinBottom, int nSide, int nIndex)
{
	if ( ! rTemplate.IsPage())
		return FALSE;
	else
		return DrawObject(pDC, rTemplate, nTop, nMinBottom, nSide, nIndex);
}

BOOL CPageListView::DrawLabel(CDC* pDC, CPageTemplate& rTemplate, int* nTop, int* nMinBottom, int nSide, int nIndex)
{
	if ( ! rTemplate.IsFlatProduct())
		return FALSE;
	else
		return DrawObject(pDC, rTemplate, nTop, nMinBottom, nSide, nIndex);
}

BOOL CPageListView::DrawObject(CDC* pDC, CPageTemplate& rTemplate, int* nTop, int* nMinBottom, int nSide, int nIndex)
{
	CImpManDoc*	   pDoc	   = GetDocument();
	if ( ! pDoc)
		return FALSE;

	BOOL bNewProductGroup = (rTemplate.m_nProductPartIndex != g_nPrevProductGroupIndex) ? TRUE : FALSE;

	CLayoutObject* pObject = rTemplate.GetLayoutObject();
	float		   fWidth = 0.0f, fHeight = 0.0f;

	if (nIndex >= pDoc->m_PageTemplateList.GetCount())
		return FALSE;

	if (pObject)
	{
		fWidth  = pObject->GetRealWidth();
		fHeight = pObject->GetRealHeight();
	}
	else
	{
		if ( ! rTemplate.IsFlatProduct())
		{
			CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(rTemplate.m_nProductPartIndex);
			fWidth  = rProductPart.m_fPageWidth;
			fHeight = rProductPart.m_fPageHeight;
		}
		else
		{
			int nFlatProductIndex = pDoc->m_PageTemplateList.GetIndex(&rTemplate, rTemplate.m_nProductPartIndex); 
			if (nFlatProductIndex < 2*pDoc->m_flatProducts.GetSize())
			{
				fWidth  = pDoc->m_flatProducts.FindByIndex(nFlatProductIndex/2).m_fTrimSizeWidth;
				fHeight = pDoc->m_flatProducts.FindByIndex(nFlatProductIndex/2).m_fTrimSizeHeight;
			}
		}
	}

	CFont fontSmall, fontBold;
	CSize szFont(13, 13);
	pDC->DPtoLP(&szFont);
	fontSmall.CreateFont(szFont.cy, 0,   0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	//fontBold.CreateFont (30, 0,   0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, "Verdana");	
	CFont* pOldFont = pDC->SelectObject(&fontSmall);

	// Draw page
	CSize textExt = pDC->GetTextExtent(_T("X"),1);
	int   nFlag = DT_LEFT, nTextY = 0;
	CRect rect;
	switch (nSide)
	{
	case LEFT:	*nTop	   = *nMinBottom - 10;
				rect.left  = (rTemplate.IsFlatProduct()) ? 10 : m_lTemplateListCenterX - (long)fWidth; 
				rect.top   = *nTop;
				rect.right = rect.left + (long)fWidth;				rect.bottom = rect.top - (long)fHeight;
				nFlag	   = DT_LEFT;
				nTextY	   = rect.top;// - textExt.cy/4;
				break;
	case RIGHT:	rect.left  = m_lTemplateListCenterX;				rect.top	= *nTop;
				rect.right = rect.left + (long)fWidth;				rect.bottom = rect.top - (long)fHeight;
				nFlag	   = DT_RIGHT;
				nTextY	   = rect.top;// - textExt.cy/4;
				//nTextY	   = rect.top - (long)(fHeight/2.0f);	// changed because pages are no longer synchronized
				break;												// to page list table entries
	}
	*nMinBottom = min(*nMinBottom, rect.bottom);

/*	break painting not possible yet, because reregistering of display items is needed for bounding box calculation in order to scroll to linked
	pages, when link symbol is clicked in PageSourceListView
	if (!m_DisplayList.IsInvalid())
	{
		CRect clientRect;
		GetClientRect(clientRect);
		pDC->DPtoLP(clientRect);
		if (rect.top < clientRect.bottom)	// break painting
			return FALSE;
	}
*/

	CPageTemplate* p2ndTemplate  = NULL;
	CRect		   multipageRect = rect;
	if (rTemplate.m_nMultipageGroupNum >= 0)
	{
		p2ndTemplate		   = rTemplate.GetFirstMultipagePartner();
		CLayoutObject* pObject = p2ndTemplate->GetLayoutObject();
		float		   fWidth, fHeight;
		if (pObject)
		{
			fWidth  = pObject->GetRealWidth();
			fHeight = pObject->GetRealHeight();
		}
		else
		{
			CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(p2ndTemplate->m_nProductPartIndex);
			fWidth  = rProductPart.m_fPageWidth;
			fHeight = rProductPart.m_fPageHeight;
		}
		multipageRect.top	 = max(*nTop, rect.top);
		multipageRect.bottom = min(multipageRect.top - (long)fHeight, rect.bottom);
		if (nSide == RIGHT)
		{
			multipageRect.left = m_lTemplateListCenterX - (long)fWidth; multipageRect.right = rect.right;
		}
		else
		{
			multipageRect.left = rect.left; multipageRect.right = m_lTemplateListCenterX + (long)fWidth;
		}
	}

	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();

	BOOL bBitmapDrawn = FALSE;
	if (pDC->RectVisible(rect) && (rTemplate.GetMultipageSide() != RIGHT))
	{
		if (rTemplate.m_ColorSeparations.GetSize() > 0)
			pDC->FillSolidRect(multipageRect, RGB(245,245,245)); 
		else
			pDC->FillSolidRect(multipageRect, RGB(220,220,220)); 

		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
		CPen*	pOldPen	  = (CPen*)pDC->SelectStockObject(BLACK_PEN);
		CPrintSheetView::DrawRectangleExt(pDC, multipageRect);

		if ( ! rTemplate.IsFlatProduct())
		{
			if (rTemplate.GetProductPart().GetBoundProduct().m_parts.GetSize() > 1)
			{
				CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(rTemplate.m_nProductPartIndex);
				CSize szSize1(2,2);		pDC->DPtoLP(&szSize1);
				CSize szSize2(15,15);	pDC->DPtoLP(&szSize2);
				CRect rcTitle = multipageRect; rcTitle.left += szSize1.cx; rcTitle.right -= szSize1.cx; rcTitle.top -= szSize1.cy; rcTitle.bottom = rcTitle.top - szSize2.cy;
				CGraphicComponent gp;
				CString string; string.Format(_T("%s"), rProductPart.m_strName);	
				//gp.DrawTitleBar(pDC, rcTitle, string, RGB(233,240,130), RGB(210,225,25), 0, FALSE);
				gp.DrawTitleBar(pDC, rcTitle, string, RGB(240,240,240), RGB(220,220,220), 0, FALSE);
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO: Show Colors ??
		if (pParentFrame->m_bShowBitmap)
		{
			if (rTemplate.m_ColorSeparations.GetSize() > 0)
			{
				if ((rTemplate.GetMultipageSide() == LEFT) || (rTemplate.m_nMultipageGroupNum == -1))
				{
					int				   nColorDefinitionIndex = rTemplate.m_ColorSeparations[0].m_nColorDefinitionIndex;
					CPageSource*	   pPageSource			 = rTemplate.GetObjectSource	  (nColorDefinitionIndex);
					CPageSourceHeader* pPageSourceHeader	 = rTemplate.GetObjectSourceHeader(nColorDefinitionIndex);
					if (pPageSource && pPageSourceHeader)
					{
						CRect outBBox, pageBBox = (rTemplate.m_nMultipageGroupNum >= 0) ? multipageRect : rect;
						int	  nOutBBoxWidth, nOutBBoxHeight;
						if ((rTemplate.m_fContentRotation == 0.0f) || (rTemplate.m_fContentRotation == 180.0f))
						{
							nOutBBoxWidth  = (int)((pPageSourceHeader->m_fBBoxRight - pPageSourceHeader->m_fBBoxLeft)   * rTemplate.m_fContentScaleX);
							nOutBBoxHeight = (int)((pPageSourceHeader->m_fBBoxTop   - pPageSourceHeader->m_fBBoxBottom) * rTemplate.m_fContentScaleY);
						}
						else
						{
							nOutBBoxWidth  = (int)((pPageSourceHeader->m_fBBoxTop   - pPageSourceHeader->m_fBBoxBottom) * rTemplate.m_fContentScaleX);
							nOutBBoxHeight = (int)((pPageSourceHeader->m_fBBoxRight - pPageSourceHeader->m_fBBoxLeft)   * rTemplate.m_fContentScaleY);
						}
						outBBox.left   = pageBBox.CenterPoint().x - nOutBBoxWidth  / 2;
						outBBox.bottom = pageBBox.CenterPoint().y - nOutBBoxHeight / 2;
						outBBox.right  = pageBBox.CenterPoint().x + nOutBBoxWidth  / 2;
						outBBox.top    = pageBBox.CenterPoint().y + nOutBBoxHeight / 2;
						outBBox		  += CSize((int)rTemplate.GetContentOffsetX(CPageTemplate::AddShinglingOffset), (int)rTemplate.GetContentOffsetY(CPageTemplate::AddShinglingOffset));
						
						POSITION pos = pDoc->m_PageSourceList.FindIndex(rTemplate.m_ColorSeparations[0].m_nPageSourceIndex);
						if (pos)
						{
							CPageListDetailView* pPageListDetailView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
							int nColorSel = (pPageListDetailView) ? pPageListDetailView->m_nColorSel : 0;
							if (nColorSel >= 0 && nColorSel < rTemplate.m_ColorSeparations.GetSize())
							{
								CString strPreviewFile;
								strPreviewFile = pDoc->m_PageSourceList.GetAt(pos).GetPreviewFileName(rTemplate.m_ColorSeparations[nColorSel].m_nPageNumInSourceFile);
								pDC->IntersectClipRect(multipageRect);
								bBitmapDrawn = DrawPreview(pDC, outBBox, TOP, rTemplate.m_fContentRotation, strPreviewFile);
								pDC->SelectClipRgn(NULL);
							}
						}
					}
				}
			}
		}
		CPrintSheetView::DrawRectangleExt(pDC, multipageRect);
		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);

		if (rTemplate.m_ColorSeparations.GetSize())
		{
			int				   nColorDefinitionIndex = rTemplate.m_ColorSeparations[0].m_nColorDefinitionIndex;
			CPageSourceHeader* pPageSourceHeader	 = rTemplate.GetObjectSourceHeader(nColorDefinitionIndex);
			if (pPageSourceHeader)
				if (pPageSourceHeader->m_bOutputBlocked)
				{
					CPen pen(PS_SOLID, 3, RED);
					pDC->SelectObject(&pen);
					pDC->MoveTo(multipageRect.left,  multipageRect.bottom);
					pDC->LineTo(multipageRect.right, multipageRect.top);
					pen.DeleteObject();
				}
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

	//if (pDC->RectVisible(rect))
	//{
	//	CGraphicComponent gp;
	//	CRect rcFrame = rect; rcFrame.NormalizeRect();
	//	if (nSide == LEFT)
	//	{
	//		rcFrame.right = rcFrame.left + (rect.Width() / 3) * 2;
	//		gp.DrawTransparentSmoothFrame(pDC, rcFrame, MIDDLEGRAY, WHITE, 0, -1);
	//		rcFrame.left  = rcFrame.right; rcFrame.right = rect.right;
	//		gp.DrawTransparentSmoothFrame(pDC, rcFrame, WHITE, MIDDLEGRAY, 0, -1);
	//	}
	//	else
	//	{
	//		rcFrame.right = rcFrame.left + rect.Width() / 3;
	//		gp.DrawTransparentSmoothFrame(pDC, rcFrame, WHITE, MIDDLEGRAY, 0, -1);
	//		rcFrame.left = rcFrame.right; rcFrame.right = rect.right;
	//		gp.DrawTransparentSmoothFrame(pDC, rcFrame, MIDDLEGRAY, WHITE, 0, -1);
	//	}
	//}

//	m_DisplayList.RegisterItem(pDC, rect, rect, (void*)&rTemplate, DISPLAY_ITEM_REGISTRY(CPageListView, PageTemplate), (void*)nIndex);
	m_DisplayList.RegisterItem(pDC, rect, rect, (void*)&rTemplate, DISPLAY_ITEM_REGISTRY(CPageListView, PageTemplate), (void*)nIndex, CDisplayItem::ForceRegister, NULL);

	if (rTemplate.GetMultipageSide() == RIGHT)
		//m_DisplayList.RegisterItem(pDC, multipageRect, multipageRect, (void*)&rTemplate, DISPLAY_ITEM_REGISTRY(CPageListView, PageTemplate), (void*)-1);
		m_DisplayList.RegisterItem(pDC, multipageRect, multipageRect, (void*)&rTemplate, DISPLAY_ITEM_REGISTRY(CPageListView, PageTemplate), (void*)-1, CDisplayItem::ForceRegister, NULL);

	pDC->SetBkMode(TRANSPARENT);

	if (pDC->RectVisible(rect))
	{
		const int nColorRectHeight = 10;
/*
		CRect colorRect = ((rTemplate.m_nMultipageGroupNum >= 0) && (nSide == RIGHT)) ? 
							CRect(rect.left - textExt.cx/2 - nColorRectHeight/2, nTextY + nColorRectHeight, 
								  rect.left - textExt.cx/2 + nColorRectHeight/2, nTextY) :
							CRect(rect.left, nTextY + nColorRectHeight, rect.left + nColorRectHeight, nTextY);
*/
//		CRect colorRect = CRect(rect.left + 1, nTextY + nColorRectHeight, rect.left + nColorRectHeight + 1, nTextY);
		CRect colorRect = (textExt.cy <= nColorRectHeight) ? CRect(rect.left + 1, nTextY, rect.left + textExt.cy + 1, nTextY - textExt.cy) :
															 CRect(rect.left + 1, nTextY, rect.left + nColorRectHeight + 1, nTextY - nColorRectHeight);

		rect.DeflateRect(textExt.cx/2, -textExt.cx/2);
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(220,220,220));
		pDC->SetTextColor(BLACK);
		pDC->DrawText(rTemplate.m_strPageID, rect, DT_SINGLELINE|DT_BOTTOM|nFlag);
		CPageListFrame* pFrame = (CPageListFrame*)GetParentFrame();
		if (pFrame)
			if ( pFrame->m_pDlgFoldSheetNumeration)
				if ( pFrame->m_pDlgFoldSheetNumeration->m_hWnd)
				{
					CString strText;
					strText.Format(_T("  (%d)"), nIndex + 1);
					int nXPos = (nSide == LEFT) ? (rect.left  + pDC->GetTextExtent(rTemplate.m_strPageID).cx) 
												: (rect.right - pDC->GetTextExtent(rTemplate.m_strPageID).cx - pDC->GetTextExtent(strText).cx - textExt.cx);
					pDC->SetTextColor(MIDDLEGRAY);
					pDC->TextOut(nXPos, rect.bottom + textExt.cy, strText);
				}

		CRect textRectColor(colorRect.right + 5, nTextY, colorRect.right + textExt.cx * 10, nTextY - textExt.cy);
		CRect textRectPath (textRectColor.right + 5, nTextY, rect.right, nTextY - textExt.cy);

		if ( ! ((rTemplate.m_nMultipageGroupNum >= 0) && (nSide == RIGHT)))
		{
			CString strUnknown;
			strUnknown.LoadString(IDS_UNKNOWN_STR);

			CString	strColorName, strPath, strPrevPath;
			for (int i = 0; i < rTemplate.m_ColorSeparations.GetSize(); i++)
			{
//				BOOL bShowColorRect = (!pParentFrame->m_bShowBitmap || !bBitmapDrawn) ? TRUE : FALSE;
				BOOL bShowColorRect = TRUE;
				if (!bShowColorRect)
				{
					POSITION pos = pDoc->m_PageSourceList.FindIndex(rTemplate.m_ColorSeparations[i].m_nPageSourceIndex);
					if (pos)
						if (pDoc->m_PageSourceList.GetAt(pos).m_strPreviewFilesPath.IsEmpty())
							bShowColorRect = TRUE;
				}
				
				if (bShowColorRect)
				{
					BOOL bDisabled = FALSE;
					CColorDefinition colorDef("Unknown", LIGHTGRAY, CColorDefinition::Spot, 0);
					if ( (rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex >= 0) && 
						 (rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
						colorDef = pDoc->m_ColorDefinitionTable[rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex];
					else
					{
						CPageSourceHeader* pPageSourceHeader = rTemplate.GetObjectSourceHeader(rTemplate.m_ColorSeparations[i]);
						colorDef.m_strColorName = pPageSourceHeader->m_strColorName;
						bDisabled = TRUE;
					}

					CBrush colorBrush(colorDef.m_rgb);
					pDC->FillRect(colorRect, &colorBrush);
					colorBrush.DeleteObject();

		 			if (textExt.cy <= nColorRectHeight)
					{
						if ( ! bBitmapDrawn)
						{
							int				   nColorDefinitionIndex = rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex;
							CPageSource*	   pPageSource			 = rTemplate.GetObjectSource	  (nColorDefinitionIndex);
							CPageSourceHeader* pPageSourceHeader	 = rTemplate.GetObjectSourceHeader(nColorDefinitionIndex);
							if (pPageSource && pPageSourceHeader )
#if defined(POSTSCRIPT) || defined(PDF)
								strPath.Format(_T("%s [%d]"), (LPCTSTR)((pPageSource->m_strDocumentFullpath.IsEmpty()) ? strUnknown : pPageSource->m_strDocumentFullpath), pPageSourceHeader->m_nPageNumber);
#else
							strPath.Format(_T("%s [%d]"), (LPCTSTR)((pPageSource->m_strContentFilesPath.IsEmpty()) ? strUnknown : pPageSource->m_strContentFilesPath), pPageSourceHeader->m_nPageNumber);
#endif
							else
								strPath = "";
							
							if (i == 0)
							{
								CRect bkRect(colorRect.right, colorRect.top, rect.right, colorRect.top - rTemplate.m_ColorSeparations.GetSize() * textExt.cy - 5);
								pDC->FillSolidRect(bkRect, RGB(245,245,245));
							}
							
							strColorName.Format(_T("%s"), colorDef.m_strColorName);
							CPageListTableView::FitStringToWidth(pDC, strColorName, textRectColor.Width());
							
							COLORREF crOldTextColor = pDC->SetTextColor((bDisabled) ? DARKGRAY : BLACK);
							pDC->DrawText(strColorName, textRectColor, DT_LEFT | DT_VCENTER);
							pDC->SetTextColor(crOldTextColor);
							
							if (strPath != strPrevPath)
								pDC->DrawText(strPath,	textRectPath,  DT_LEFT | DT_VCENTER);
							
							strPrevPath = strPath;
						}

						colorRect.right		 = colorRect.left	 + textExt.cy;
						colorRect.top		 = colorRect.bottom;
						colorRect.bottom	 = colorRect.top	 - textExt.cy;
						textRectColor.top	 = textRectColor.bottom;
						textRectColor.bottom = textRectColor.top - textExt.cy;
						textRectPath.top	 = textRectPath.bottom;
						textRectPath.bottom  = textRectPath.top  - textExt.cy;
					}
					else
					{
						colorRect.top		 = colorRect.bottom;
						colorRect.bottom	 = colorRect.top	 - nColorRectHeight;
						textRectColor.top	 = textRectColor.bottom;
						textRectColor.bottom = textRectColor.top - nColorRectHeight;
						textRectPath.top	 = textRectPath.bottom;
						textRectPath.bottom  = textRectPath.top  - nColorRectHeight;
					}
				}
			}
		}
	}

	int		nFlatProductIndex = pDoc->m_PageTemplateList.GetIndex(&rTemplate, rTemplate.m_nProductPartIndex); 
	CString strObjectName	  = pDoc->m_flatProducts.FindByIndex(nFlatProductIndex/2).m_strProductName;
	CGraphicObject* pgObject  = pDoc->m_flatProducts.FindByIndex(nFlatProductIndex/2).m_graphicList.GetObject(strObjectName);
	if (pgObject)
	{
		g_bIsContentView = TRUE;
		pgObject->Draw(pDC, multipageRect.left, multipageRect.bottom, multipageRect.Width(), multipageRect.Height(), TOP, -1);
	}

	pDC->SelectObject(pOldFont);
	fontSmall.DeleteObject();
	fontBold.DeleteObject();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CPageListView diagnostics

#ifdef _DEBUG
void CPageListView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CPageListView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CImpManDoc* CPageListView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CImpManDoc)));
	return (CImpManDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPageListView message handlers

void CPageListView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	CScrollView::OnPrepareDC(pDC, pInfo);

	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(m_TemplateListSize);
	pDC->SetWindowOrg(0, m_TemplateListSize.cy);

	if (pInfo != NULL)
	{	// It's like "GetClientRect()" for a printer and ScaleToFit
		pDC->SetViewportExt( pDC->GetDeviceCaps(HORZRES), -(pDC->GetDeviceCaps(VERTRES)));         
	}
	else
	{
		CRect clientRect;
		GetClientRect(&clientRect);
		float fFactor = (float)clientRect.Width()/(float)m_TemplateListSize.cx;
		pDC->SetViewportExt(clientRect.Width(), -(int)((float)m_TemplateListSize.cy * fFactor));    
	}
}

void CPageListView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/) 
{
	CalcTemplateListSize();
	CRect clientRect;
	GetClientRect(&clientRect);
	float fFactor   = (float)clientRect.Width()/(float)m_TemplateListSize.cx;
	CSize sizeTotal = CSize(clientRect.Width(), (int)((float)m_TemplateListSize.cy * fFactor));
	int	  nNumPages = (clientRect.Height() > 0) ? sizeTotal.cy / clientRect.Height() : 0;
	int   nNumLines = (GetDocument()->m_PageTemplateList.GetCount() + 2) / 2;
	CSize sizePage  = CSize(sizeTotal.cx, (nNumPages) ? sizeTotal.cy / nNumPages : sizeTotal.cy);
	CSize sizeLine  = CSize(sizeTotal.cx, (nNumLines) ? sizeTotal.cy / nNumLines : sizeTotal.cy);
	SetScrollSizes(MM_TEXT, sizeTotal, sizePage, sizeLine);
	Invalidate();	
}

void CPageListView::OnSize(UINT nType, int cx, int cy) 
{
	CScrollView::OnSize(nType, cx, cy);
	
	CalcTemplateListSize();
	CRect clientRect;
	GetClientRect(&clientRect);
	float fFactor   = (float)clientRect.Width()/(float)m_TemplateListSize.cx;
	CSize sizeTotal = CSize(clientRect.Width(), (int)((float)m_TemplateListSize.cy * fFactor));
	int	  nNumPages = (clientRect.Height() > 0) ? (sizeTotal.cy / clientRect.Height()) : 0;
	int   nNumLines = (GetDocument()->m_PageTemplateList.GetCount() + 2) / 2;
	CSize sizePage  = CSize(sizeTotal.cx, (nNumPages) ? sizeTotal.cy / nNumPages : sizeTotal.cy);
	CSize sizeLine  = CSize(sizeTotal.cx, (nNumLines) ? sizeTotal.cy / nNumLines : sizeTotal.cy);
	SetScrollSizes(MM_TEXT, sizeTotal, sizePage, sizeLine);
}

void CPageListView::CalcTemplateListSize()
{
	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return;

	int nProductPartIndex = GetCurProductPartIndex();

	if (GetCurProductPartIndex() == pDoc->GetFlatProductsIndex())
		CalcTemplateListSizeLabels();
	else
		CalcTemplateListSizeBook();
}

void CPageListView::CalcTemplateListSizeBook()
{
	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return;
	if (pDoc->m_PageTemplateList.IsEmpty())
	{
		m_TemplateListSize = CSize(10,10);
		return;
	}

	int				nProductPartIndex = GetCurProductPartIndex();
	CBoundProduct&	rBoundProduct	  = (nProductPartIndex < 0) ? pDoc->m_boundProducts.GetProduct(abs(nProductPartIndex) - 1) : pDoc->m_boundProducts.GetProductPart(nProductPartIndex).GetBoundProduct();

	float		   fMaxLeftWidth = 10.0f, fMaxRightWidth = 10.0f, fLeftHeight = 10.0f, fRightHeight = 10.0f, fTotalHeight = 10.0f;
	CLayoutObject* pObject;
	POSITION	   pos	  = pDoc->m_PageTemplateList.GetHeadPosition(nProductPartIndex);
	while (pos)
	{
		CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
		pObject = rPageTemplate.GetLayoutObject();
		if (pObject)
		{
			fMaxRightWidth = max(fMaxRightWidth, pObject->GetRealWidth()); // + pObject->GetRealBorder(LEFT)));
			fRightHeight   = pObject->GetRealHeight();
		}
		else
		{
			fMaxRightWidth = max(fMaxRightWidth, rBoundProduct.m_fTrimSizeWidth);
			fRightHeight   = rBoundProduct.m_fTrimSizeHeight;
		}

		fTotalHeight+= max(fLeftHeight, fRightHeight) + 10.0f;

		if (pos)
		{
			CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
			pObject = rPageTemplate.GetLayoutObject();
			if ( ! pos && (rPageTemplate.m_nMultipageGroupNum >= 0) )	// if last page is partner of 1st page
			{
				fTotalHeight += 10.0f;
				break;
			}
			else
				if (pObject)
				{
					fMaxLeftWidth = max(fMaxLeftWidth, pObject->GetRealWidth());// + pObject->GetRealBorder(RIGHT)));
					fLeftHeight	  = pObject->GetRealHeight();
				}
				else
				{
					fMaxLeftWidth = max(fMaxLeftWidth, rBoundProduct.m_fTrimSizeWidth);
					fLeftHeight   = rBoundProduct.m_fTrimSizeHeight;
				}

				if (!pos || rPageTemplate.IsFlatProduct() )
				fTotalHeight+= max(fLeftHeight, fRightHeight) + 10.0f;
		}
		else
			fLeftHeight = 10;
	}

	m_TemplateListSize	   = CSize((long)(fMaxLeftWidth + fMaxRightWidth + 40.0f), (long)fTotalHeight);
	m_lTemplateListCenterX = (long)(fMaxLeftWidth + 20.0f);
}

void CPageListView::CalcTemplateListSizeLabels()
{
	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return;
	if (pDoc->m_PageTemplateList.IsEmpty())
	{
		m_TemplateListSize = CSize(10,10);
		return;
	}

	int nProductPartIndex = GetCurProductPartIndex();

	BOOL		   bFlatProductHasBackSide = FALSE;
	float		   fMaxWidth = 10.0f, fHeight = 10.0f, fTotalHeight = 10.0f;
	CLayoutObject* pObject;
	POSITION	   pos	  = pDoc->m_PageTemplateList.GetHeadPosition(nProductPartIndex);
	int			   nIndex = 0;
	while (pos)
	{
		CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
		pObject = rPageTemplate.GetLayoutObject();
		if (pObject)
		{
			fMaxWidth = max(fMaxWidth, pObject->GetRealWidth()); 
			fHeight   = pObject->GetRealHeight();
		}
		else
		{
			if (nIndex/2 < pDoc->m_flatProducts.GetSize())
			{
				fMaxWidth = max(fMaxWidth, pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fTrimSizeWidth);
				fHeight   = pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fTrimSizeHeight;
			}
		}

		fTotalHeight += fHeight + 10.0f;

		if (rPageTemplate.FlatProductHasBackSide())
			bFlatProductHasBackSide = TRUE;

		pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);	// skip label backside

		nIndex += 2;
	}

	m_TemplateListSize	   = CSize((long)((bFlatProductHasBackSide) ? 2 * fMaxWidth + 30.0f : fMaxWidth + 20.0f), (long)fTotalHeight);
	m_lTemplateListCenterX = (long)((bFlatProductHasBackSide) ? fMaxWidth + 20.0f : fMaxWidth/2);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Display list message handlers

void CPageListView::OnLButtonDown(UINT /*nFlags*/, CPoint point) 
{
	// First of all deselect items in counterpart pane
	CPageSourceListView* pView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	if (pView)
	{
		pView->m_DisplayList.DeselectAll();			
		pView->m_DisplayList.InvalidateItem(NULL);
		pView->UpdateWindow();
	}
	CPageListTableView* pView2 = (CPageListTableView*)theApp.GetView(RUNTIME_CLASS(CPageListTableView));
	if (pView2)
	{
		pView2->m_DisplayList.DeselectAll();			
		pView2->m_DisplayList.InvalidateItem(NULL);
		pView2->UpdateWindow();
	}
	//////////////////////////////////////////////////

	int			 nSide;
	CDisplayItem itemFound = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPageListView, PageTemplate));

	if (!itemFound.IsNull())
	{
		CImpManDoc*	   pDoc				= GetDocument();
		CPageTemplate* pClickedTemplate = (CPageTemplate*)itemFound.m_pData;
		if ( ! SetDoublePage(pClickedTemplate, nSide, TRUE)) //Ask the user for re/setting doublepage
		{
			m_DisplayList.OnLButtonDown(point);
			return;
		}

		CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem();
		while (pDI)
		{
			CPageTemplate* pPageTemplate = (CPageTemplate*)pDI->m_pData;

			if (pClickedTemplate != pPageTemplate)
			{
				int nIndex = pDoc->m_PageTemplateList.GetIndex(pPageTemplate, pPageTemplate->m_nProductPartIndex);
				if (nIndex%2)	// If odd -> left page 
				{
					if ( ((pClickedTemplate->m_nMultipageGroupNum >=  0) && (pPageTemplate->m_nMultipageGroupNum == -1)) ||
						 ((pClickedTemplate->m_nMultipageGroupNum == -1) && (pPageTemplate->m_nMultipageGroupNum >=  0)) )
					{
						if ( ! SetDoublePage(pPageTemplate, RIGHT, FALSE)) //Don't ask the user
						{
							m_DisplayList.OnLButtonDown(point);
							return;
						}
					}
				}
			}

			pDI = m_DisplayList.GetNextSelectedItem(pDI);
		}

		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->SetModifiedFlag();

		theApp.OnUpdateView(RUNTIME_CLASS(CPageListView),		NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListTableView),	NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListDetailView),	NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),	NULL, 0, NULL);

		Invalidate();
		UpdateWindow();
	}
	else
		m_DisplayList.OnLButtonDown(point);

	CPageListTopToolsView* pTopToolsView = (CPageListTopToolsView*)theApp.GetView(RUNTIME_CLASS(CPageListTopToolsView));
	if (pTopToolsView)
		pTopToolsView->UpdateDialogControls(pTopToolsView, TRUE);
}

BOOL CPageListView::OnSelectPageTemplate(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	if (pParentFrame)
		pParentFrame->m_bDeleteSelectionMenuActive = TRUE;

	CPageListDetailView* pPageListDetailView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
	if (pPageListDetailView)
	{
		//pPageListDetailView->m_pPageTemplate = (CPageTemplate*)pDI->m_pData;
		//pPageListDetailView->m_pLayoutObject = pPageListDetailView->m_pPageTemplate->GetLayoutObject();

		if (pPageListDetailView->GetPageTemplate())
		{
			if (pParentFrame)
			{
				/////////////////////////////////////////////////////////////////////////////
				// Initialize the PageTemplate-Dialog
				if (pParentFrame->m_pDlgPageTemplateProps->m_hWnd)
				{
					pParentFrame->m_pDlgPageTemplateProps->m_pPageTemplate = pPageListDetailView->GetPageTemplate();
					pParentFrame->m_pDlgPageTemplateProps->m_pLayoutObject = pPageListDetailView->GetLayoutObject();
					pParentFrame->m_pDlgPageTemplateProps->UpdateDialogFromPageListView();
				}
			}
		}
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListDetailView), NULL, 0, NULL);
//		GetDocument()->UpdateAllViews(this);
	}
	else
	{
		CPageSourceListView* pPageSourceListView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
		CPageListTableView*  pPageListTableView	 = (CPageListTableView*) theApp.GetView(RUNTIME_CLASS(CPageListTableView));
		if (!pPageSourceListView && !pPageListTableView)
			return FALSE;

		CPageTemplate* pTemplate = (CPageTemplate*)pDI->m_pData;
		if (!pTemplate)
			return FALSE;

		CDisplayItem* pDIScrollTo = NULL;
		CDisplayItem* pDISelect	  = NULL;
		for (int i = 0; i < pTemplate->m_ColorSeparations.GetSize(); i++)
		{
			if (pPageSourceListView)
			{
				CPageSourceHeader* pPageSourceHeader = pTemplate->GetObjectSourceHeader(pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex);
				if (pPageSourceHeader)
					pDISelect = pPageSourceListView->m_DisplayList.GetItemFromData(pPageSourceHeader);
			}
			else
				pDISelect = pPageListTableView->m_DisplayList.GetItemFromData((void*)pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex,
														 					  (void*)pTemplate, DISPLAY_ITEM_TYPEID(CPageListTableView, PDFDocument));
			if (pDISelect)
			{
				if (pPageSourceListView) 
				{
					pPageSourceListView->m_DisplayList.SelectItem(*pDISelect);
					pPageSourceListView->m_DisplayList.InvalidateItem(pDISelect);
				}
				else
				{
					pPageListTableView->m_DisplayList.SelectItem(*pDISelect);
					pPageListTableView->m_DisplayList.InvalidateItem(pDISelect);
				}

				if (!pDIScrollTo)
					pDIScrollTo = pDISelect;
			}
		}

		CScrollView* pView = (CScrollView*)((DWORD)pPageSourceListView | (DWORD)pPageListTableView);
		if (pDIScrollTo)
		{
			if ( ! pDIScrollTo->m_bIsVisible)	
			{
				pView->Invalidate();
				pView->OnVScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.top - 20, NULL);
			}
		}
	}

	return TRUE;
}

BOOL CPageListView::OnDeselectPageTemplate(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bDeleteSelectionMenuActive = FALSE;

	CPageListDetailView* pPageListDetailView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
	if (pPageListDetailView)
	{
		//pPageListDetailView->m_pPageTemplate = NULL;
		//pPageListDetailView->m_pLayoutObject = NULL;
		pPageListDetailView->Invalidate();
		pPageListDetailView->UpdateWindow();

		if (pParentFrame)
		{
			// Make the PageTemplate-Dialog empty
			if (pParentFrame->m_pDlgPageTemplateProps->m_hWnd)
			{
				pParentFrame->m_pDlgPageTemplateProps->m_pPageTemplate = NULL;//pPageListDetailView->m_pPageTemplate;
				pParentFrame->m_pDlgPageTemplateProps->m_pLayoutObject = NULL;//pPageListDetailView->m_pLayoutObject;
				pParentFrame->m_pDlgPageTemplateProps->UpdateDialogFromPageListView();
			}
		}
	}

	return TRUE;
}

BOOL CPageListView::OnStatusChanged()	// display list status has changed (any item selected / deselected)
{
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	if (pParentFrame)
		if (pParentFrame->m_pDlgFoldSheetNumeration->m_hWnd)
			pParentFrame->InitNumerationDialog(pParentFrame->m_pDlgFoldSheetNumeration);

	return TRUE;
}

void CPageListView::OnRButtonDown(UINT /*nFlags*/, CPoint point) 
{
	// First of all deselect items in counterpart pane
	CPageSourceListView* pView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	if (pView)
	{
		pView->m_DisplayList.DeselectAll();			
		pView->m_DisplayList.InvalidateItem(NULL);
		pView->UpdateWindow();
	}
	//////////////////////////////////////////////////
	m_DisplayList.OnLButtonDown(point, TRUE);
}

BOOL CPageListView::SetDoublePage(CPageTemplate* pPageTemplate, int nSide, BOOL bAskForChanges, BOOL bDontSetIfOccupied)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	int nIndexL = pDoc->m_PageTemplateList.GetIndex(pPageTemplate, pPageTemplate->m_nProductPartIndex);
	int nIndexR;

	ASSERT(nIndexL >= 0);

	if (nIndexL%2)	// If odd -> left page clicked
	{
		if (nSide != RIGHT)	// Selection only valid if right side was clicked
			return FALSE;
		if (nIndexL == pDoc->m_PageTemplateList.GetCount(pPageTemplate->m_nProductPartIndex) - 1)
			nIndexR = 0;
		else
			nIndexR = nIndexL + 1;
	}
	else			// Right page clicked
	{
		if (nSide != LEFT)	// Selection only valid if left side was clicked
			return FALSE;
		nIndexR = nIndexL;
		if (nIndexL > 0)
			nIndexL--;
		else				// if 1st page clicked on left, take last page as partner
			nIndexL = pDoc->m_PageTemplateList.GetCount(pPageTemplate->m_nProductPartIndex) - 1;
	}

	if ((nIndexL < 0) || (nIndexR >= pDoc->m_PageTemplateList.GetCount(pPageTemplate->m_nProductPartIndex)))
		return FALSE;

	POSITION posL = pDoc->m_PageTemplateList.FindIndex(nIndexL, pPageTemplate->m_nProductPartIndex);
	POSITION posR = pDoc->m_PageTemplateList.FindIndex(nIndexR, pPageTemplate->m_nProductPartIndex);

	if (bDontSetIfOccupied)
	{
		if (pDoc->m_PageTemplateList.GetAt(posL).m_ColorSeparations.GetSize())
			return FALSE;
		if (pDoc->m_PageTemplateList.GetAt(posR).m_ColorSeparations.GetSize())
			return FALSE;
	}

	if (pDoc->m_PageTemplateList.GetAt(posL).m_nMultipageGroupNum >= 0)	// Already multipage -> convert to single pages
	{
		if (bAskForChanges)
		{
			if (AfxMessageBox(IDS_RESET_TO_SINGLEPAGE, MB_YESNO) == IDNO)
				return FALSE;
		}
		pDoc->m_PageTemplateList.GetAt(posL).m_nMultipageGroupNum = -1;
		pDoc->m_PageTemplateList.GetAt(posR).m_nMultipageGroupNum = -1;
		pDoc->m_PageTemplateList.GetAt(posR).m_ColorSeparations.RemoveAll();
	}
	else
	{
		if (bAskForChanges)
		{
			if (AfxMessageBox(IDS_SET_TO_MULTIPAGE, MB_YESNO) == IDNO)
				return FALSE;
		}
		short nGroupNum = pDoc->m_PageTemplateList.GetNextFreeGroupNum();
		pDoc->m_PageTemplateList.GetAt(posL).m_nMultipageGroupNum = nGroupNum;
		pDoc->m_PageTemplateList.GetAt(posR).m_nMultipageGroupNum = nGroupNum;
		pDoc->m_PageTemplateList.GetAt(posR).SetContentOffsetX(pDoc->m_PageTemplateList.GetAt(posL).GetContentOffsetX());
		pDoc->m_PageTemplateList.GetAt(posR).SetContentOffsetY(pDoc->m_PageTemplateList.GetAt(posL).GetContentOffsetY());
		pDoc->m_PageTemplateList.GetAt(posR).m_fContentScaleX	  = pDoc->m_PageTemplateList.GetAt(posL).m_fContentScaleX;
		pDoc->m_PageTemplateList.GetAt(posR).m_fContentScaleY	  = pDoc->m_PageTemplateList.GetAt(posL).m_fContentScaleY;
		pDoc->m_PageTemplateList.GetAt(posR).m_fContentRotation	  = pDoc->m_PageTemplateList.GetAt(posL).m_fContentRotation;
		pDoc->m_PageTemplateList.GetAt(posR).m_ColorSeparations.RemoveAll();
		for (int i = 0; i < pDoc->m_PageTemplateList.GetAt(posL).m_ColorSeparations.GetSize(); i++)
			pDoc->m_PageTemplateList.GetAt(posR).m_ColorSeparations.Add(pDoc->m_PageTemplateList.GetAt(posL).m_ColorSeparations[i]);
	}

	return TRUE;
}

BOOL CPageListView::OnMultiSelect()	// multiple items has been selected
{
	CDisplayItem* pFirstDI = m_DisplayList.GetFirstSelectedItem();
	if (!pFirstDI)
		return FALSE;

	CDisplayItem* pDI = m_DisplayList.GetNextSelectedItem(pFirstDI);
	while (pDI)
	{
		if (pDI->m_pFnMsgHandler != pFirstDI->m_pFnMsgHandler)
			m_DisplayList.DeselectItem(*pDI);

		pDI = m_DisplayList.GetNextSelectedItem(pDI);
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////
// enable OLE DragDrop functionality - needed in conjunction with display list of PageSourceListView

int CPageListView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if( m_dropTarget.Register( this ) )
		return 0;
	else
		return -1;
}

DROPEFFECT CPageListView::OnDragOver(COleDataObject* /*pDataObject*/, DWORD dwKeyState, CPoint /*point*/) 
{
	DROPEFFECT de;

	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_MOVE;
	else 
		if ((dwKeyState & MK_ALT) == MK_ALT)
			de = DROPEFFECT_MOVE;
		else
			de = DROPEFFECT_MOVE;

	return de;
}

BOOL CPageListView::OnDrop(COleDataObject* /*pDataObject*/, DROPEFFECT dropEffect, CPoint point) 
{
	if (dropEffect == DROPEFFECT_NONE)
		return FALSE;

	CDisplayItem targetDI = m_DisplayList.GetItemPtInside(point, FALSE);
	if (targetDI.IsNull())
		return FALSE;

	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return FALSE;

	CPageSourceListView* pView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	if ( ! pView)
		return FALSE;

	// Search for pPageTemplate in list
	CPageTemplate* pPageTemplate = (CPageTemplate*)targetDI.m_pData;
	POSITION	   startPos		 = pDoc->m_PageTemplateList.GetHeadPosition();
	while (startPos)
	{
		if (&(pDoc->m_PageTemplateList.CList::GetNext(startPos)) == pPageTemplate)
			break;
	}

	CDisplayItem* pDI		  = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader));
	int			  nCurPageNum = (pDI) ? ((CPageSourceHeader*)pDI->m_pData)->m_nPageNumber : 0;
	CPageSource*  pPageSource = (pDI) ? (CPageSource*)pDI->m_pAuxData : NULL;

	pDoc->m_PageTemplateList.ResetFlags();

	CPageTemplate* p2ndPageTemplate = NULL;
	if (pPageTemplate->CheckMultipage((pDI) ? (CPageSourceHeader*)pDI->m_pData : NULL))
	{
		p2ndPageTemplate = pPageTemplate->GetFirstMultipagePartner();
		p2ndPageTemplate->m_bFlag = TRUE;
	}

	if ( ! pPageTemplate->FlatProductHasBackSide())
	{
		POSITION pos = startPos;
		while (pos)
		{
			CPageTemplate* pPageTemplate = &(pDoc->m_PageTemplateList.CList::GetNext(pos));
			if (pPageTemplate->m_bLabelBackSide)
				pPageTemplate->m_bFlag = TRUE;
		}
	}

	int  nProductPartIndex  = GetCurProductPartIndex();
	BOOL bKeepContentOffset = UNDEFINED;
	BOOL bTrimBoxWarned		= FALSE;
	BOOL bOccupiedWarned	= FALSE;
	while (pDI)
	{
		CPageSourceHeader* pPageSourceHeader = (CPageSourceHeader*)pDI->m_pData;

		if ( (nCurPageNum != pPageSourceHeader->m_nPageNumber) ||
			 (pPageSource != (CPageSource*)pDI->m_pAuxData) )
		{
			if (!startPos)
				break;

			do
				pPageTemplate = &(pDoc->m_PageTemplateList.GetNext(startPos, nProductPartIndex));
			while (pPageTemplate->m_bFlag && startPos);

			if (pPageTemplate->m_bFlag && ! startPos)	// if last template has flag
				break;

			if (pPageTemplate->CheckMultipage(pPageSourceHeader))
			{
				p2ndPageTemplate = pPageTemplate->GetFirstMultipagePartner();
				if (p2ndPageTemplate->m_bFlag)
					p2ndPageTemplate = NULL;
				else
					p2ndPageTemplate->m_bFlag = TRUE;
			}
			else
				p2ndPageTemplate = NULL;

			pPageTemplate->m_bFlag = TRUE;
		}

		nCurPageNum = pPageSourceHeader->m_nPageNumber; 
		pPageSource	= (CPageSource*)pDI->m_pAuxData;

		SEPARATIONINFO* pExistSepInfo = NULL;
		BOOL			bBreak		  = FALSE;
		if (pExistSepInfo = pPageTemplate->GetSeparationInfo(pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(pPageSourceHeader->m_strColorName)))	// separation already existing?
		{
			if ( ! bOccupiedWarned)
			{
				CString strMsg, strPageInfo = pPageTemplate->m_strPageID;
				if (pPageSourceHeader->m_strColorName != _T("Composite"))
					strPageInfo += _T(" (") + pPageSourceHeader->m_strColorName + _T(")");
				AfxFormatString1(strMsg, IDS_PAGEOCCUPIED_MESSAGE, strPageInfo);
				switch (AfxMessageBox(strMsg, MB_YESNO))
				{
					case IDYES:		bOccupiedWarned = TRUE;	break;
					case IDNO:		bBreak = TRUE; 			break;
				}
				if (bBreak)
					break;
			}
		}

		CColorDefinition colorDef(pPageSourceHeader->m_strColorName, pPageSourceHeader->m_rgb, CColorDefinition::Spot, pPageSourceHeader->m_nColorIndex);
		SEPARATIONINFO	 sepInfo;
		sepInfo.m_nPageSourceIndex		= pDoc->m_PageSourceList.GetPageSourceIndex(pPageSource);
		sepInfo.m_nPageNumInSourceFile	= (short)(pPageSource->GetHeaderIndex(pPageSourceHeader) + 1);
		sepInfo.m_nColorDefinitionIndex = pDoc->m_ColorDefinitionTable.InsertColorDef(colorDef);

		if (pPageTemplate)
		{
			if (pExistSepInfo)
				*pExistSepInfo = sepInfo;
			else
				pPageTemplate->m_ColorSeparations.Add(sepInfo);

			if (bKeepContentOffset == UNDEFINED)	// not yet prompted
				if (pPageTemplate->HasContentOffset() || pPageTemplate->IsScaled())
					if (KIMOpenLogMessage(IDS_KEEP_CONTENTOFFSET_PROMPT, MB_YESNO, IDYES) == IDYES)
						bKeepContentOffset = TRUE;
					else
						bKeepContentOffset = FALSE;

			if ( ! bKeepContentOffset || (bKeepContentOffset == UNDEFINED) )
			{
				if (pPageSourceHeader->HasTrimBox())
				{
					if ( ! bTrimBoxWarned && theApp.settings.m_bTrimboxPagesizeMismatchWarning)
					{
						CLayoutObject* pObject = pPageTemplate->GetLayoutObject();
						if (pObject)
						{
							if ( (fabs( (pPageSourceHeader->m_fTrimBoxRight - pPageSourceHeader->m_fTrimBoxLeft)   - pObject->GetRealWidth())  >= 0.5) ||
								 (fabs( (pPageSourceHeader->m_fTrimBoxTop   - pPageSourceHeader->m_fTrimBoxBottom) - pObject->GetRealHeight()) >= 0.5) )
							{
								CDlgTrimboxWarning dlg;
								dlg.m_strText = (pPageSource) ? pPageSource->m_strDocumentFullpath : "";
								dlg.DoModal();
								bTrimBoxWarned = TRUE;
							}
						}
					}
					float fTrimBoxCenterX = pPageSourceHeader->m_fTrimBoxLeft   + (pPageSourceHeader->m_fTrimBoxRight - pPageSourceHeader->m_fTrimBoxLeft)  / 2.0f;
					float fTrimBoxCenterY = pPageSourceHeader->m_fTrimBoxBottom + (pPageSourceHeader->m_fTrimBoxTop   - pPageSourceHeader->m_fTrimBoxBottom)/ 2.0f;
					float fBBoxCenterX	  = pPageSourceHeader->m_fBBoxLeft		+ (pPageSourceHeader->m_fBBoxRight	  - pPageSourceHeader->m_fBBoxLeft)		/ 2.0f;
					float fBBoxCenterY	  = pPageSourceHeader->m_fBBoxBottom	+ (pPageSourceHeader->m_fBBoxTop	  - pPageSourceHeader->m_fBBoxBottom)	/ 2.0f;
					pPageTemplate->SetContentOffsetX(fBBoxCenterX - fTrimBoxCenterX);
					pPageTemplate->SetContentOffsetY(fBBoxCenterY - fTrimBoxCenterY);
					if ( (pPageTemplate->GetContentOffsetX() != 0.0f) || (pPageTemplate->GetContentOffsetY() != 0.0f) )
						pPageTemplate->m_nAttribute |= CPageTemplate::ScaleOffsetModified;
				}
				else
				{
					pPageTemplate->SetContentOffsetX(0.0f);
					pPageTemplate->SetContentOffsetY(0.0f);
					pPageTemplate->m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
				}
				pPageTemplate->m_fContentScaleX = 1.0f;
				pPageTemplate->m_fContentScaleY = 1.0f;
			}
		}

		if (p2ndPageTemplate)
		{
			if (pExistSepInfo)
				*pExistSepInfo = sepInfo;
			else
				p2ndPageTemplate->m_ColorSeparations.Add(sepInfo);

			if ( ! bKeepContentOffset || (bKeepContentOffset == UNDEFINED) )
			{
				if (pPageSourceHeader->HasTrimBox())
				{
					float fTrimBoxCenterX = pPageSourceHeader->m_fTrimBoxLeft   + (pPageSourceHeader->m_fTrimBoxRight - pPageSourceHeader->m_fTrimBoxLeft)  / 2.0f;
					float fTrimBoxCenterY = pPageSourceHeader->m_fTrimBoxBottom + (pPageSourceHeader->m_fTrimBoxTop   - pPageSourceHeader->m_fTrimBoxBottom)/ 2.0f;
					float fBBoxCenterX	  = pPageSourceHeader->m_fBBoxLeft		+ (pPageSourceHeader->m_fBBoxRight	  - pPageSourceHeader->m_fBBoxLeft)		/ 2.0f;
					float fBBoxCenterY	  = pPageSourceHeader->m_fBBoxBottom	+ (pPageSourceHeader->m_fBBoxTop	  - pPageSourceHeader->m_fBBoxBottom)	/ 2.0f;
					p2ndPageTemplate->SetContentOffsetX(fBBoxCenterX - fTrimBoxCenterX);
					p2ndPageTemplate->SetContentOffsetY(fBBoxCenterY - fTrimBoxCenterY);
					if ( (p2ndPageTemplate->GetContentOffsetX() != 0.0f) || (p2ndPageTemplate->GetContentOffsetY() != 0.0f) )
						p2ndPageTemplate->m_nAttribute |= CPageTemplate::ScaleOffsetModified;
				}
				else
				{
					p2ndPageTemplate->SetContentOffsetX(0.0f);
					p2ndPageTemplate->SetContentOffsetY(0.0f);
					p2ndPageTemplate->m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
				}
				p2ndPageTemplate->m_fContentScaleX = 1.0f;
				p2ndPageTemplate->m_fContentScaleY = 1.0f;
			}
		}

		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader));
	}

	pDoc->m_PageTemplateList.ResetFlags();

	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.ReorganizeColorInfos();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();
	pDoc->SetModifiedFlag();										
	Invalidate();
	theApp.OnUpdateView(RUNTIME_CLASS(CPageListTableView),	NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPageSourceListView),	NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),		NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),	NULL, 0, NULL);

	return TRUE;
}

void CPageListView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
	{
	case VK_HOME	: OnVScroll(SB_TOP, 0, NULL);
					  OnHScroll(SB_LEFT, 0, NULL);
					  break;
	case VK_END		: OnVScroll(SB_BOTTOM, 0, NULL);
					  OnHScroll(SB_RIGHT, 0, NULL);
					  break;
	case VK_UP		: OnVScroll(SB_LINEUP, 0, NULL);
					  break;
	case VK_DOWN	: OnVScroll(SB_LINEDOWN, 0, NULL);
					  break;
	case VK_PRIOR	: OnVScroll(SB_PAGEUP, 0, NULL);
					  break;
	case VK_NEXT	: OnVScroll(SB_PAGEDOWN, 0, NULL);
					  break;
	case VK_LEFT	: OnHScroll(SB_LINELEFT, 0, NULL);
					  break;
	case VK_RIGHT	: OnHScroll(SB_LINERIGHT, 0, NULL);
					  break;
	default			: break;
	}
	
	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CPageListView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// The default call of OnMouseWheel causes an assertion or doesn't work
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion
}

void CPageListView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

BOOL CPageListView::DrawPreview(CDC* pDC, CRect rect, int nHeadPosition, float fRotation, CString strFileTitle, BOOL bKeepRatio, BOOL bDimmed, Image** pThumbnailImage)
{
	CString strFileName;
	BOOL bLoadFromFile = TRUE;
	if (pThumbnailImage)
		if (*pThumbnailImage)
			bLoadFromFile = FALSE;

	if (bLoadFromFile)
	{
		CFileStatus status;
		strFileName = strFileTitle + _T(".png");	// first look for PNG file
		if ( ! CFile::GetStatus(strFileName, status))
		{
			strFileName = strFileTitle + _T(".jpg");		// then look for JPEG file
			if ( ! CFile::GetStatus(strFileName, status))
				return FALSE;
		}
		if ( ! CFile::GetStatus(strFileName, status))
		{
			strFileName = strFileTitle + _T(".bmp");		// then look for BMP file
			if ( ! CFile::GetStatus(strFileName, status))
				return FALSE;
		}
	}

	pDC->LPtoDP(&rect);
	int nDC = pDC->SaveDC();
	pDC->SetMapMode(MM_TEXT);
	pDC->SetWindowOrg(0,0);
	pDC->SetViewportOrg(0,0);

	CSize size(1, 1);
	rect.left  += size.cx;
	rect.top   += size.cy;
	rect.right += size.cx;
	rect.bottom+= size.cy;

	Graphics graphics(pDC->m_hDC);

	//LPWSTR lpszW = new WCHAR[255];
	//WCHAR wStr[255];
	//LPCSTR lpStr = strFileName.GetBuffer( strFileName.GetLength() );
	//int nLen = MultiByteToWideChar(CP_ACP, 0,lpStr, -1, NULL, NULL);
	//MultiByteToWideChar(CP_ACP, 0, lpStr, -1, wStr, nLen);

	Image* pImage	  = NULL;
	Image* pThumbnail = NULL;
	if (pThumbnailImage)
	{
		if ( ! *pThumbnailImage)
		{
#ifdef UNICODE
			pImage = Image::FromFile(strFileName.GetBuffer( strFileName.GetLength())); 
#else
			pImage = Image::FromFile(CA2W(strFileName.GetBuffer( strFileName.GetLength()) )); 
#endif
			pThumbnail = *pThumbnailImage = pImage->GetThumbnailImage(rect.Width(), rect.Height());
		}
		else
			pThumbnail = *pThumbnailImage;
	}
	else
	{
#ifdef UNICODE
		pThumbnail = pImage = Image::FromFile(strFileName.GetBuffer( strFileName.GetLength())); 
#else
		pThumbnail = pImage = Image::FromFile(CA2W(strFileName.GetBuffer( strFileName.GetLength()) ));
#endif
	}

	if ( ! pThumbnail)
		return FALSE; 
	if ( (pThumbnail->GetWidth() == 0) || (pThumbnail->GetHeight() == 0) )
		return FALSE;

	if (bKeepRatio)		// keep ratio of x and y edges while scaling
	{
		CSize imgSize(pThumbnail->GetWidth(), pThumbnail->GetHeight());
		float fScale;
		if ((float)imgSize.cy / (float)imgSize.cx >= (float)rect.Height() / (float)rect.Width())
			fScale = (float)imgSize.cy / (float)rect.Height();
		else
			fScale = (float)imgSize.cx / (float)rect.Width();
		imgSize.cx = min((long)((float)imgSize.cx / fScale), rect.Width());
		imgSize.cy = min((long)((float)imgSize.cy / fScale), rect.Height());
		rect.right  = rect.left + imgSize.cx;
		rect.bottom = rect.top  + imgSize.cy;
	}

	double fIntPart;
	double fAngle = fRotation;
	switch (nHeadPosition)
	{
	case LEFT:	fAngle = (float)modf(( 90.0f + fRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case BOTTOM:fAngle = (float)modf((180.0f + fRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case RIGHT: fAngle = (float)modf((270.0f + fRotation) / 360.0f, &fIntPart) * 360.0f; break;
	}

	if ( fabs(fAngle - 90.0f) < 0.1f)
		pThumbnail->RotateFlip(Gdiplus::Rotate270FlipNone);
	else
		if ( fabs(fAngle - 180.0f) < 0.1f)
			pThumbnail->RotateFlip(Gdiplus::Rotate180FlipNone);
		else
			if ( fabs(fAngle - 270.0f) < 0.1f)
				pThumbnail->RotateFlip(Gdiplus::Rotate90FlipNone);

	if (pDC->IsPrinting())	// workaround, because graphics.DrawImage() does not work correct in printing mode, but img.AlphaBlend() does
	{
		CLSID pngClsid;
#ifdef UNICODE
		GetEncoderClsid(_T("image/png"), &pngClsid);
#else
		GetEncoderClsid(CA2W(_T("image/png")), &pngClsid);
#endif

		IStream* pStream = SHCreateMemStream(NULL, 0);
		pThumbnail->Save(pStream, &pngClsid); 

		Bitmap bitmap(pStream); 
		CImage img;
		HBITMAP hBMP;
		bitmap.GetHBITMAP(WHITE, &hBMP);
		img.Attach((HBITMAP)hBMP);

		rect.NormalizeRect();
		img.AlphaBlend(pDC->m_hDC, rect.left, rect.top, rect.Width(), rect.Height(), 0, 0, pThumbnail->GetWidth(), pThumbnail->GetHeight());
		pStream->Release();
	}
	else
	{
		rect.NormalizeRect();
		Rect RECT; RECT.X = rect.left; RECT.Y = rect.top; RECT.Width = rect.Width(); RECT.Height = rect.Height();
		if (bDimmed)
		{
			ImageAttributes attributes; attributes.SetGamma(0.2f); 
			graphics.DrawImage(pThumbnail, RECT, 0, 0, pThumbnail->GetWidth(), pThumbnail->GetHeight(), Gdiplus::UnitPixel, &attributes);
		}
		else
		{
			graphics.DrawImage(pThumbnail, RECT);
		}
	}

	pDC->RestoreDC(nDC);

	if (pImage)
		delete pImage;

	return TRUE;
}

int CPageListView::GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
   UINT  num = 0;          // number of image encoders
   UINT  size = 0;         // size of the image encoder array in bytes

   ImageCodecInfo* pImageCodecInfo = NULL;

   GetImageEncodersSize(&num, &size);
   if(size == 0)
      return -1;  // Failure

   pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
   if(pImageCodecInfo == NULL)
      return -1;  // Failure

   GetImageEncoders(num, size, pImageCodecInfo);

   for(UINT j = 0; j < num; ++j)
   {
      if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
      {
         *pClsid = pImageCodecInfo[j].Clsid;
         free(pImageCodecInfo);
         return j;  // Success
      }    
   }

   free(pImageCodecInfo);
   return -1;  // Failure
}

BOOL CPageListView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// 2nd parameter must be 0 (means no standard-cursor will be registered), 
	// if we want draw out own cursors
	CBrush* pBkgrdBrush = new CBrush;
	pBkgrdBrush->CreateSolidBrush(LIGHTGRAY); 

	lpszClassName = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(*pBkgrdBrush), 0);

	BOOL ret = CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	pBkgrdBrush->Detach();
	delete pBkgrdBrush;

	return ret;
}

BOOL CPageListView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	SetCursor(theApp.m_CursorStandard);
	
	return CScrollView::OnSetCursor(pWnd, nHitTest, message);
}

void CPageListView::OnContextMenu(CWnd* /*pWnd*/, CPoint point) 
{
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_PAGELIST_LEFTVIEW_POPUP));

	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);

	CWnd* pWndPopupOwner = this;
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
}
