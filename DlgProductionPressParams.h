#pragma once

#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"


// CDlgProductionPressParams-Dialogfeld

class CDlgProductionPressParams : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionPressParams)

public:
	CDlgProductionPressParams(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionPressParams();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTION_PRESSPARAMS };

public:
	CPoint			m_ptInitialPos;
	BOOL			m_bLocked;
	CString			m_strPressDevice;
	CString			m_strPaper;
	CImageList		m_refEdgeImageList;
	CDlgMarkSet*	m_pDlgMarkSet;

protected:
	CComboBox m_pressDeviceCombo;
	CComboBox m_paperCombo;
	CString m_strPlateFormat;
	int	m_nWorkStyle;
	int	m_nTurnStyle;
	int m_nPaperGrain;
	int m_nRefEdge;
	int m_nNumColorStationsFront;
	int m_nNumColorStationsBack;
	CString m_strMarkset;

public:
	CPressParams& GetPressParams();
	void		  InitData();
	void		  SaveData();


public:
	virtual BOOL Create(UINT nIDTemplate, CWnd* pParentWnd = NULL);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedOk();
	afx_msg void OnCbnSelchangePdPressdeviceCombo();
	afx_msg void OnBnClickedPdPressdeviceButton();
	afx_msg void OnCbnSelchangePdPaperCombo();
	afx_msg void OnBnClickedPdAddPaperButton();
	afx_msg void OnBnClickedPdRemovePaperButton();
	afx_msg void OnCbnSelchangePdWorkstyleCombo();
	afx_msg void OnCbnSelchangePdPaperGrainCombo();
	afx_msg void OnBnClickedPdBrowseMarkset();
	afx_msg void OnBnClickedFpRemoveMarkset();
};
