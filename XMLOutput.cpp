#include "stdafx.h"
#include "Imposition Manager.h"
#include "XMLOutput.h"
#include "JDFOutputImposition.h"
#include "XMLCommon.h"


CXMLOutput::CXMLOutput(void)
{
	setlocale(LC_NUMERIC, "english");	// because when german setted, sscanf() does expect numbers with Comma like "0,138"
}

CXMLOutput::~CXMLOutput(void)
{
	switch (theApp.settings.m_iLanguage)
	{
		case 0:  setlocale(LC_ALL, "german");	break; 
		case 1:  setlocale(LC_ALL, "english");	break;
		default: setlocale(LC_ALL, "english");	break;
	}
}

BOOL CXMLOutput::UpdateXML(const CString& strFilePath)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	JDF::XMLDoc*	pXMLDoc = new JDF::XMLDoc();
	CFileStatus	fileStatus;
	if (CFile::GetStatus(strFilePath, fileStatus))	
	{
		try
		{
			pXMLDoc->Parse((JDF::WString)strFilePath);
		}
		catch (...)
		{
			CString strMsg;
			strMsg.Format(_T("XML output error"));//: %s"), e.getMessage().getBytes());
			KIMOpenLogMessage(strMsg);
		}
		if (pXMLDoc->isNull())
		{
			CString strMsg;
			strMsg.Format(_T("XML output error"));//: %s"), e.getMessage().getBytes());
			KIMOpenLogMessage(strMsg);
		}
    }
	//else
	//	pXMLDoc = new XMLDoc(_T("KIMJob"));

	m_domDoc = pXMLDoc->GetDOMDocument();
 
	DOMNode* jobNode	 = m_domDoc->getFirstChild();
	CString  strNodeName = jobNode->getNodeName();
	if (strNodeName != _T("KIMJob"))
	{
		delete pXMLDoc;
		return FALSE;
	}

	CString strJobName = _T("");
	DOMNamedNodeMap* nodeMap = jobNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"Name"); 
		strJobName = (item) ? (CString)item->getNodeValue() : _T("");
	}

	DOMNode* pComponentsSectionNode = NULL;
	BOOL	 bResult = FALSE;
	DOMNode* node	 = jobNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		//if (strNodeName == _T("JobDataSection"))
		//	bResult = ProcessJobDataSectionNode(node);
		//else
		//if (strNodeName == _T("ResourceSection"))
		//	bResult = ProcessResourceSectionNode(node);
		//else
		if (strNodeName == _T("ProductSection"))
			bResult = ProcessProductSectionNode(node);
		//else
		//if (strNodeName == _T("StrippingSection"))
		//	bResult = ProcessStrippingSectionNode(node); 
		else
		if (strNodeName == _T("ComponentsSection"))
			pComponentsSectionNode = node; 

		node = node->getNextSibling();
	}

	DOMElement* pNewNode = m_domDoc->createElement(CA2W("ComponentsSection"));
	ProcessComponentsSectionNode((DOMNode*)pNewNode); 
	if (pComponentsSectionNode)
		jobNode->replaceChild((DOMNode*)pNewNode, pComponentsSectionNode);
	else
		jobNode->appendChild((DOMNode*)pNewNode);

	try
	{
		pXMLDoc->Write2URL((WString)WindowsPath2URL(strFilePath));
	}
	catch (JDF::JDFException e)
	{
		CString strMsg;
		strMsg.Format(_T("XML output error: %s"), e.getMessage().getBytes());
		KIMOpenLogMessage(strMsg);
	}

	delete pXMLDoc;
	return TRUE;
}


///////// ProductSection

BOOL CXMLOutput::ProcessProductSectionNode(DOMNode* productSectionNode)
{
	BOOL	 bResult = FALSE;
	CString  strNodeName;
	DOMNode* node = productSectionNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("Product"))
			bResult = ProcessProductNode(node);

		node = node->getNextSibling();
	}
	return bResult;
}

BOOL CXMLOutput::ProcessProductNode(DOMNode* productNode)
{
	BOOL bResult = FALSE;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return bResult;

	CString strID, strName, strType;
	DOMNamedNodeMap* nodeMap = productNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"ID");
		strID	= (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Name");
		strName = (item) ? (CString)item->getNodeValue() : _T("");

		item = nodeMap->getNamedItem(L"Type");
		strType = (item) ? (CString)item->getNodeValue() : _T("");
	}

	if (strType.CompareNoCase(_T("Brochure")) == 0)
	{
		ProcessBoundProductNode(productNode, pDoc->m_boundProducts.FindProduct(strName));
	}

	return bResult;
}

BOOL CXMLOutput::ProcessBoundProductNode(DOMNode* pBoundProductNode, CBoundProduct& rBoundProduct)
{
	DOMNode* pBindingInfoNode = NULL;
	CString strNodeName;
	DOMNode* node = pBoundProductNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		//if (strNodeName == _T("ProductLayout"))
		//	bResult = ProcessBoundProductLayoutNode(node, rBoundProduct);
		//else
		//if (strNodeName == _T("Quantity"))
		//	bResult = ProcessBoundProductQuantityNode(node, rBoundProduct);
		//else
		//if (strNodeName == _T("Delivery"))
		//	bResult = ProcessBoundProductDeliveryNode(node, rBoundProduct);
		//else
		//if (strNodeName == _T("ContentFile"))
		//	bResult = ProcessBoundProductContentNode(node, rBoundProduct);
		//else
		//if (strNodeName == _T("ProductPart"))
		//	bResult = ProcessBoundProductPartNode(node, rBoundProduct);
		//else
		if (strNodeName == _T("BindingInfo"))
			pBindingInfoNode = node;

		node = node->getNextSibling();
	}

	DOMElement* pNewNode = m_domDoc->createElement(CA2W("BindingInfo"));
	pNewNode->setAttribute(CA2W("BindingProcessRefID"), CA2W(rBoundProduct.m_bindingParams.GetID()));
	pNewNode->setAttribute(CA2W("BindingStyleName"),	CA2W(rBoundProduct.m_bindingParams.m_bindingDefs.m_strName));
	ProcessBoundProductBindingInfoNode((DOMNode*)pNewNode, rBoundProduct);
	if (pBindingInfoNode)
		pBoundProductNode->replaceChild((DOMNode*)pNewNode, pBindingInfoNode);
	else
		pBoundProductNode->appendChild((DOMNode*)pNewNode);

	return TRUE;
}

BOOL CXMLOutput::ProcessBoundProductBindingInfoNode(DOMNode* pBoundProductBindingInfoNode, CBoundProduct& rBoundProduct)
{
	if ( ! pBoundProductBindingInfoNode)
		return FALSE;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CString		strRefID;
	int			nProductIndex = rBoundProduct.GetIndex();
	CFoldSheet* pFoldSheet	  = pDoc->m_Bookblock.GetFirstFoldSheet(-nProductIndex - 1);
	while (pFoldSheet)
	{
		DOMElement* foldSheetNode = m_domDoc->createElement(CA2W("FoldSheet"));

		CBoundProductPart& rProductPart = pFoldSheet->GetProductPart();
		foldSheetNode->setAttribute(CA2W("ID"),					CA2W(pFoldSheet->GetID()));
		foldSheetNode->setAttribute(CA2W("Number"),				CA2W(pFoldSheet->GetNumber()));
		foldSheetNode->setAttribute(CA2W("Scheme"),				CA2W(pFoldSheet->m_strFoldSheetTypeName));
		foldSheetNode->setAttribute(CA2W("ProductPartRefID"),	CA2W(rProductPart.GetID()));
		pBoundProductBindingInfoNode->appendChild((DOMNode*)foldSheetNode);

		pFoldSheet = pDoc->m_Bookblock.GetNextFoldSheet(pFoldSheet, -nProductIndex - 1);
	}

	return TRUE;
}

BOOL CXMLOutput::ProcessComponentsSectionNode(DOMNode* componentsSectionNode)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CString strWidth, strHeight;
	float	fWidth, fHeight;
	CString strComponentID, strProductRefID, strFoldSheetRefID, strGrain;
	int		nComponentID = 0;
	for (int nComponentGroupIndex = 0; nComponentGroupIndex < pDoc->m_printComponentsList.GetSize(); nComponentGroupIndex++)
	{
		CPrintComponentsGroup& rComponentGroup = pDoc->m_printComponentsList[nComponentGroupIndex];

		for (int nComponentIndex = 0; nComponentIndex < rComponentGroup.m_printComponents.GetSize(); nComponentIndex++)
		{
			CPrintComponent& rComponent = rComponentGroup.m_printComponents[nComponentIndex];

			DOMElement* componentElement = m_domDoc->createElement(CA2W("PrintComponent"));

			strComponentID.Format(_T("%d"), nComponentID++);
			componentElement->setAttribute(CA2W("ID"), CA2W(strComponentID));
			if (rComponent.m_nType == CPrintComponent::FoldSheet) 
			{
				CBoundProduct*	pBoundProduct = rComponent.GetBoundProduct();
				CFoldSheet*		pFoldSheet	  = rComponent.GetFoldSheet(); 
				strProductRefID	  = (pBoundProduct) ? pBoundProduct->GetProductID() : _T("");
				strFoldSheetRefID = (pFoldSheet)	? pFoldSheet->GetID()			: _T("");
				componentElement->setAttribute(CA2W("Type"),			CA2W("FoldSheetComponent"));
				componentElement->setAttribute(CA2W("FoldSheetRefID"),	CA2W(strFoldSheetRefID));
				switch ( ((pFoldSheet) ? pFoldSheet->GetPaperGrain() : GRAIN_EITHER) )
				{
					case GRAIN_EITHER:		strGrain = _T("Either");		break;
					case GRAIN_HORIZONTAL:	strGrain = _T("Horizontal");	break;
					case GRAIN_VERTICAL:	strGrain = _T("Vertical");		break;
				}
			}
			else
			{
				CFlatProduct* pFlatProduct = rComponent.GetFlatProduct();
				strProductRefID = (pFlatProduct) ? pFlatProduct->GetProductID() : _T("");
				componentElement->setAttribute(CA2W("Type"), CA2W("FlatComponent"));
				switch ( ((pFlatProduct) ? pFlatProduct->m_nPaperGrain : GRAIN_EITHER) )
				{
					case GRAIN_EITHER:		strGrain = _T("Either");		break;
					case GRAIN_HORIZONTAL:	strGrain = _T("Horizontal");	break;
					case GRAIN_VERTICAL:	strGrain = _T("Vertical");		break;
				}
			}

			componentElement->setAttribute(CA2W("ProductRefID"),	CA2W(strProductRefID));
			componentElement->setAttribute(CA2W("Grain"),			CA2W(strGrain));

			DOMNode* componentNode = componentsSectionNode->appendChild((DOMNode*)componentElement);

			rComponent.GetCutSize(fWidth, fHeight); strWidth.Format(_T("%.2f"), fWidth); strHeight.Format(_T("%.2f"), fHeight); 
			DOMElement* grossFormatElement = m_domDoc->createElement(CA2W("GrossFormat"));
			grossFormatElement->setAttribute(CA2W("Width"),		CA2W(strWidth));
			grossFormatElement->setAttribute(CA2W("Height"),	CA2W(strHeight));
			componentNode->appendChild((DOMNode*)grossFormatElement);
			
			//DOMElement* marginsElement = m_domDoc->createElement(CA2W("Margins"));
			//marginsElement->setAttribute(CA2W("Left"),		CA2W(""));
			//marginsElement->setAttribute(CA2W("Right"),		CA2W(""));
			//marginsElement->setAttribute(CA2W("Top"),		CA2W(""));
			//marginsElement->setAttribute(CA2W("Bottom"),	CA2W(""));
			//componentNode->appendChild((DOMNode*)marginsElement);
		}
	}

	return TRUE;
}
