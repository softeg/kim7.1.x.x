#pragma once


#include "OwnerDrawnButtonList.h"
#include "DlgSelectFoldSchemePopup.h"
#include "ExtendedListCtrl.h"
#include "MyBitmapButton.h"
#include "OwnerDrawnButtonList.h"
#include "LayoutDataWindow.h"
#include "DlgComponentImposer.h"



// CPrintSheetViewStrippingData-Formularansicht

class CPrintSheetViewStrippingData : public CFormView
{
	DECLARE_DYNCREATE(CPrintSheetViewStrippingData)

public:
	CPrintSheetViewStrippingData(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CPrintSheetViewStrippingData();

// Dialogfelddaten
	enum { IDD = IDD_PRINTSHEETVIEW_STRIPPINGDATA };


public:
	COwnerDrawnButtonList	m_toolBar;
	CFont					m_boldFont;
	int						m_nToolBarHeight;
	CLayoutDataWindow		m_layoutDataWindow;


public:
	static void							DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd);
	class CPrintSheetNavigationView*	GetPrintSheetNavigationView();
	class CPrintComponentsView*			GetPrintComponentsView();
	CPrintSheet*						GetActPrintSheet();
	CLayout*							GetActLayout();
	CPressDevice*						GetActPressDevice();
	int									GetActFlatProductIndex();
	CString								GetActPrintingProfileName();
	CPrintComponent						GetActComponent();
	CPrintGroup&						GetActPrintGroup();
	CColorantList						GetActColorants();


public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnRemovePrintSheet() ;
	afx_msg void OnRemoveLayoutPrintSheets();
	afx_msg void OnUpdateBlankSheet(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemovePrintSheet(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemoveLayoutPrintSheets(CCmdUI* pCmdUI);
public:
	afx_msg void OnBlankSheet();
};
