#pragma once
#include "afxwin.h"
#include "OwnerDrawnButtonList.h"
#include "DlgProductionPrepressParams.h"
#include "DlgProductionPressParams.h"
#include "DlgProductionCuttingParams.h"
#include "DlgProductionFoldingParams.h"
#include "DlgProductionBindingParams.h"
#include "DlgProductionTrimmingParams.h"
#include "DlgProductionPunchingParams.h"
#include "DlgProductionProfileRules.h"
#include "DlgProductionLayoutPool.h"



class CPMStatic : public CStatic
{
public:
	CPMStatic();
	~CPMStatic();

public:
	CFont m_font;

public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
};


// CDlgProductionManager-Dialogfeld

class CDlgProductionManager : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionManager)

public:
	CDlgProductionManager(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionManager();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTIONMANAGER };

public:
	CString							m_strTitle;
	CPMStatic						m_productionStepFrame;
	CPMStatic						m_productionParamsFrame;
	COwnerDrawnButtonList			m_productionStepButtons;
	CDlgProductionPrepressParams	m_dlgProductionPrepressParams;
	CDlgProductionPressParams		m_dlgProductionPressParams;
	CDlgProductionCuttingParams		m_dlgProductionCuttingParams;
	CDlgProductionFoldingParams		m_dlgProductionFoldingParams;
	CDlgProductionBindingParams		m_dlgProductionBindingParams;
	CDlgProductionTrimmingParams	m_dlgProductionTrimmingParams;
	CDlgProductionPunchingParams	m_dlgProductionPunchingParams;
	CDlgProductionProfileRules		m_dlgProductionProfileRules;
	CDlgProductionLayoutPool		m_dlgProductionLayoutPool;
	CProductionProfile				m_productionProfile, m_oldProductionProfile;
	CPrintGroup*					m_pPrintGroup;
	int								m_nComponentIndex;
	BOOL							m_bEditChanged;
	CString							m_strProductionProfilesPath;



public:
	void InitProcessButtons();
	void InitData();
	void SaveData();
	void CheckModified();

	
public:
	static BOOL		LoadProfileList();
	static BOOL		StoreProfileList();
	static void DrawItemProductionStep(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void DrawBkItemProductionStep(CDC* pDC, CRect rcItem, CWnd* pWnd = NULL);


	CString m_strProfileName;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSelchangeProductionProfileList();
	afx_msg void OnEditchangeProductionProfileList();
	afx_msg void OnNew();
	afx_msg void OnDel();
	afx_msg void OnChange();
	afx_msg void OnSelectProductionStep();
	afx_msg void OnBnClickedOk();
	CString m_strComment;
	afx_msg void OnBnClickedProductionprofileSelectProcesses();
};
