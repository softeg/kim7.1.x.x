// DlgProductionCuttingParams.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionCuttingParams.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgProductionManager.h"
#include "LayoutDataWindow.h"
#include "GraphicComponent.h"



// CDlgProductionCuttingParams-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionCuttingParams, CDialog)

CDlgProductionCuttingParams::CDlgProductionCuttingParams(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionCuttingParams::IDD, pParent)
{
	m_nOptimizeColumns = 0;	  // vertical
	m_nOptimizeAlignment = 0; // bottom
	m_fTopTrim = 0.0f;
	m_fBottomTrim = 0.0f;
	m_fRightTrim = 0.0f;
	m_fLeftTrim = 0.0f;
	m_pParentWnd = pParent;
	m_bLocked = FALSE;
	m_ptInitialPos = CPoint(0,0);
}

CDlgProductionCuttingParams::~CDlgProductionCuttingParams()
{
}

int CDlgProductionCuttingParams::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		SetParent(m_pParentWnd);
		ModifyStyle(WS_OVERLAPPEDWINDOW | WS_POPUPWINDOW, WS_CHILD);
		ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);
	}

	return 0;
}

BOOL CDlgProductionCuttingParams::Create(UINT nIDTemplate, CWnd* pParentWnd)
{
	m_pParentWnd = pParentWnd;
	return CDialog::Create(nIDTemplate, pParentWnd);
}

BOOL CDlgProductionCuttingParams::OnEraseBkgnd(CDC* pDC)
{
	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		return CDialog::OnEraseBkgnd(pDC);

	CRect rcFrame;
	GetClientRect(rcFrame);

	CGraphicComponent gc;
	gc.DrawPopupWindowBackground(pDC, rcFrame);

	return TRUE;
}

void CDlgProductionCuttingParams::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
		{
			if (m_hWnd)
			{
				if ( ! m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
				{
					m_bLocked = TRUE;
					EndModalLoop(IDCANCEL);
					DestroyWindow();
				}
			}
		}
	}
	else
		m_bLocked = FALSE;
}

void CDlgProductionCuttingParams::OnBnClickedOk()
{
	SaveData();
	EndModalLoop(IDOK);
	m_bLocked = TRUE;
	DestroyWindow();
}

CProductionProfile& CDlgProductionCuttingParams::GetProductionProfile()
{
	static CProductionProfile nullProductionProfile;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile;
	}
	//else
	//	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
	//	{
	//		CLayoutDataWindow*	pParent	= (CLayoutDataWindow*)m_pParentWnd;
	//		return pParent->GetProductionProfile();
	//	}

	return nullProductionProfile;
}

void CDlgProductionCuttingParams::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_OPTIMIZE_COLUMNS_RADIO, m_nOptimizeColumns);
	DDX_Radio(pDX, IDC_OPTIMIZE_ALIGNBOTTOM_RADIO, m_nOptimizeAlignment);
	DDX_Control(pDX, IDC_OPTIMIZE_RIGHTTRIM_SPIN, m_rightTrimSpin);
	DDX_Control(pDX, IDC_OPTIMIZE_LEFTTRIM_SPIN, m_leftTrimSpin);
	DDX_Control(pDX, IDC_OPTIMIZE_TOPTRIM_SPIN, m_topTrimSpin);
	DDX_Control(pDX, IDC_OPTIMIZE_BOTTOMTRIM_SPIN, m_bottomTrimSpin);
	DDX_Measure(pDX, IDC_OPTIMIZE_BOTTOMTRIM_EDIT, m_fBottomTrim);
	DDX_Measure(pDX, IDC_OPTIMIZE_TOPTRIM_EDIT, m_fTopTrim);
	DDX_Measure(pDX, IDC_OPTIMIZE_RIGHTTRIM_EDIT, m_fRightTrim);
	DDX_Measure(pDX, IDC_OPTIMIZE_LEFTTRIM_EDIT, m_fLeftTrim);
}


BEGIN_MESSAGE_MAP(CDlgProductionCuttingParams, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_OPTIMIZE_COLUMNS_RADIO, OnBnClickedOptimizeColumnsRadio)
	ON_BN_CLICKED(IDC_OPTIMIZE_ROWS_RADIO, OnBnClickedOptimizeRowsRadio)
	ON_BN_CLICKED(IDC_OPTIMIZE_ALIGNTOP_RADIO, OnBnClickedOptimizeAlignTopRadio)
	ON_BN_CLICKED(IDC_OPTIMIZE_ALIGNBOTTOM_RADIO, OnBnClickedOptimizeAlignBottomRadio)
	ON_BN_CLICKED(IDC_OPTIMIZE_ALIGNLEFT_RADIO, OnBnClickedOptimizeAlignLeftRadio)
	ON_BN_CLICKED(IDC_OPTIMIZE_ALIGNRIGHT_RADIO, OnBnClickedOptimizeAlignRightRadio)
	ON_NOTIFY(UDN_DELTAPOS, IDC_OPTIMIZE_BOTTOMTRIM_SPIN, OnDeltaposBottomtrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_OPTIMIZE_TOPTRIM_SPIN, OnDeltaposToptrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_OPTIMIZE_RIGHTTRIM_SPIN, OnDeltaposRighttrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_OPTIMIZE_LEFTTRIM_SPIN, OnDeltaposLefttrimSpin)
END_MESSAGE_MAP()


// CDlgProductionCuttingParams-Meldungshandler

BOOL CDlgProductionCuttingParams::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_ptInitialPos != CPoint(0,0))
		SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);

	m_topTrimSpin.SetRange(0, 1);  
	m_rightTrimSpin.SetRange(0, 1);  
	m_leftTrimSpin.SetRange(0, 1);  
	m_bottomTrimSpin.SetRange(0, 1);  

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProductionCuttingParams::InitData()
{
	CCuttingProcess& rCuttingProcess = GetProductionProfile().m_postpress.m_cutting;

	m_nOptimizeColumns = (rCuttingProcess.m_nBaseCutDirection == VERTICAL) ? 0 : 1;
	GetDlgItem(IDC_OPTIMIZE_ALIGNTOP_RADIO   )->EnableWindow((m_nOptimizeColumns == 0) ? TRUE  : FALSE);
	GetDlgItem(IDC_OPTIMIZE_ALIGNBOTTOM_RADIO)->EnableWindow((m_nOptimizeColumns == 0) ? TRUE  : FALSE);
	GetDlgItem(IDC_OPTIMIZE_ALIGNLEFT_RADIO  )->EnableWindow((m_nOptimizeColumns == 0) ? FALSE : TRUE);
	GetDlgItem(IDC_OPTIMIZE_ALIGNRIGHT_RADIO )->EnableWindow((m_nOptimizeColumns == 0) ? FALSE : TRUE);

	switch (rCuttingProcess.m_nBaseCutAlignment)
	{
	case BOTTOM:	m_nOptimizeAlignment = 0;	break;
	case TOP:		m_nOptimizeAlignment = 1;	break;
	case LEFT:		m_nOptimizeAlignment = 2;	break;
	case RIGHT:		m_nOptimizeAlignment = 3;	break;
	}	

	m_fLeftTrim		= rCuttingProcess.m_fLeftBorder;
	m_fBottomTrim	= rCuttingProcess.m_fLowerBorder;
	m_fRightTrim	= rCuttingProcess.m_fRightBorder;
	m_fTopTrim		= rCuttingProcess.m_fUpperBorder;

	UpdateData(FALSE);
}

void CDlgProductionCuttingParams::SaveData()
{
	UpdateData(TRUE);

	CCuttingProcess& rCuttingProcess = GetProductionProfile().m_postpress.m_cutting;

	rCuttingProcess.m_nBaseCutDirection = (m_nOptimizeColumns == 0) ? VERTICAL : HORIZONTAL;

	switch (m_nOptimizeAlignment)
	{
	case 0:	rCuttingProcess.m_nBaseCutAlignment = BOTTOM;	break;
	case 1:	rCuttingProcess.m_nBaseCutAlignment = TOP;		break;
	case 2:	rCuttingProcess.m_nBaseCutAlignment = LEFT;		break;
	case 3:	rCuttingProcess.m_nBaseCutAlignment = RIGHT;	break;
	}

	rCuttingProcess.m_fLeftBorder	= m_fLeftTrim;
	rCuttingProcess.m_fLowerBorder	= m_fBottomTrim;
	rCuttingProcess.m_fRightBorder	= m_fRightTrim;
	rCuttingProcess.m_fUpperBorder	= m_fTopTrim;

}

void CDlgProductionCuttingParams::OnBnClickedOptimizeColumnsRadio()
{
	UpdateData(TRUE);
	m_nOptimizeAlignment = 0;
	GetDlgItem(IDC_OPTIMIZE_ALIGNTOP_RADIO   )->EnableWindow((m_nOptimizeColumns == 0) ? TRUE  : FALSE);
	GetDlgItem(IDC_OPTIMIZE_ALIGNBOTTOM_RADIO)->EnableWindow((m_nOptimizeColumns == 0) ? TRUE  : FALSE);
	GetDlgItem(IDC_OPTIMIZE_ALIGNLEFT_RADIO  )->EnableWindow((m_nOptimizeColumns == 0) ? FALSE : TRUE);
	GetDlgItem(IDC_OPTIMIZE_ALIGNRIGHT_RADIO )->EnableWindow((m_nOptimizeColumns == 0) ? FALSE : TRUE);
	UpdateData(FALSE);
}

void CDlgProductionCuttingParams::OnBnClickedOptimizeRowsRadio()
{
	UpdateData(TRUE);
	m_nOptimizeAlignment = 2;
	GetDlgItem(IDC_OPTIMIZE_ALIGNTOP_RADIO   )->EnableWindow((m_nOptimizeColumns == 0) ? TRUE  : FALSE);
	GetDlgItem(IDC_OPTIMIZE_ALIGNBOTTOM_RADIO)->EnableWindow((m_nOptimizeColumns == 0) ? TRUE  : FALSE);
	GetDlgItem(IDC_OPTIMIZE_ALIGNLEFT_RADIO  )->EnableWindow((m_nOptimizeColumns == 0) ? FALSE : TRUE);
	GetDlgItem(IDC_OPTIMIZE_ALIGNRIGHT_RADIO )->EnableWindow((m_nOptimizeColumns == 0) ? FALSE : TRUE);
	UpdateData(FALSE);
}

void CDlgProductionCuttingParams::OnBnClickedOptimizeAlignTopRadio()
{
	UpdateData(TRUE);
}

void CDlgProductionCuttingParams::OnBnClickedOptimizeAlignBottomRadio()
{
	UpdateData(TRUE);
}

void CDlgProductionCuttingParams::OnBnClickedOptimizeAlignLeftRadio()
{
	UpdateData(TRUE);
}

void CDlgProductionCuttingParams::OnBnClickedOptimizeAlignRightRadio()
{
	UpdateData(TRUE);
}

void CDlgProductionCuttingParams::OnDeltaposToptrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_topTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	*pResult = 0;
}

void CDlgProductionCuttingParams::OnDeltaposBottomtrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_bottomTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	*pResult = 0;
}

void CDlgProductionCuttingParams::OnDeltaposRighttrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_rightTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	*pResult = 0;
}

void CDlgProductionCuttingParams::OnDeltaposLefttrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_leftTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	*pResult = 0;
}
