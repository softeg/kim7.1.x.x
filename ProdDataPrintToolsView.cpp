// ProdDataPrintToolsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "ProdDataPrintToolsView.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "NewPrintSheetFrame.h"
#include "MainFrm.h"




// CProdDataPrintToolsView

IMPLEMENT_DYNCREATE(CProdDataPrintToolsView, CFormView)

CProdDataPrintToolsView::CProdDataPrintToolsView()
	: CFormView(CProdDataPrintToolsView::IDD)
{
}

CProdDataPrintToolsView::~CProdDataPrintToolsView()
{
}

void CProdDataPrintToolsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CProdDataPrintToolsView, CFormView)
	ON_UPDATE_COMMAND_UI(ID_DEFINE_TRIM, OnUpdateDefineTrim)
	ON_UPDATE_COMMAND_UI(ID_MOVE_OBJECT, OnUpdateMoveObject)
	ON_UPDATE_COMMAND_UI(ID_SWAP_OBJECTS, OnUpdateSwapObjects)
	ON_UPDATE_COMMAND_UI(ID_OBJECTS_TURN, OnUpdateObjectsTurn)
	ON_UPDATE_COMMAND_UI(ID_OBJECTS_TUMBLE, OnUpdateObjectsTumble)
	ON_UPDATE_COMMAND_UI(ID_OBJECTSROTATE_CLOCK, OnUpdateObjectsrotateClock)
	ON_UPDATE_COMMAND_UI(ID_OBJECTSROTATE_COUNTERCLOCK, OnUpdateObjectsrotateCounterclock)
	ON_UPDATE_COMMAND_UI(IDC_RELEASE_BLEED, OnUpdateReleaseBleed)
	ON_UPDATE_COMMAND_UI(ID_DELETE_SELECTION, OnUpdateDeleteSelection)
	ON_UPDATE_COMMAND_UI(ID_UNDO, OnUpdateUndo)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_LEFT,				OnUpdateAlignLeft)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_HCENTER,			OnUpdateAlignHCenter)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_RIGHT,			OnUpdateAlignRight)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_TOP,				OnUpdateAlignTop)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_VCENTER,			OnUpdateAlignVCenter)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_BOTTOM,			OnUpdateAlignBottom)
	ON_UPDATE_COMMAND_UI(ID_LOCK_UNLOCK_FOLDSHEET,	OnUpdateLockUnlockFoldSheet)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CProdDataPrintToolsView-Diagnose

#ifdef _DEBUG
void CProdDataPrintToolsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CProdDataPrintToolsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CProdDataPrintToolsView-Meldungshandler

void CProdDataPrintToolsView::OnInitialUpdate()
{
	m_pNewPrintSheetFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! m_pNewPrintSheetFrame)
		return;

	CRect	rcButton; 
	m_modifyToolBar.Create(this, GetParentFrame(), NULL, IDB_PRINTVIEW_MODIFY_TOOLBAR, IDB_PRINTVIEW_MODIFY_TOOLBAR_COLD, CSize(30, 24));
	GetDlgItem(IDC_PRINTTOOL_MODIFY_FRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton);	rcButton.bottom = rcButton.top + 30;
	m_modifyToolBar.AddButton(-1, IDS_CHANNELS,					ID_DEFINE_TRIM,					rcButton, 0);	rcButton.OffsetRect(0, rcButton.Height());
	//m_modifyToolBar.AddButton(-1, IDS_MOVE_OBJECTS,				ID_MOVE_OBJECT,					rcButton, 1);	rcButton.OffsetRect(0, rcButton.Height());
	m_modifyToolBar.AddButton(-1, IDS_OBJECTS_EXCHANGE,			ID_SWAP_OBJECTS,			rcButton, 2);	rcButton.OffsetRect(0, rcButton.Height());
	m_modifyToolBar.AddButton(-1, IDS_TURN,						ID_OBJECTS_TURN,				rcButton, 3);	rcButton.OffsetRect(0, rcButton.Height());
	m_modifyToolBar.AddButton(-1, IDS_TUMBLE,					ID_OBJECTS_TUMBLE,				rcButton, 4);	rcButton.OffsetRect(0, rcButton.Height());
	m_modifyToolBar.AddButton(-1, IDS_ROTATE_RIGHT,				ID_OBJECTSROTATE_CLOCK,			rcButton, 5);	rcButton.OffsetRect(0, rcButton.Height());
	m_modifyToolBar.AddButton(-1, IDS_ROTATE_LEFT,				ID_OBJECTSROTATE_COUNTERCLOCK,	rcButton, 6);	rcButton.OffsetRect(0, rcButton.Height());
	m_modifyToolBar.AddButton(-1, IDS_DELETE,					ID_DELETE_SELECTION,			rcButton, 8);	rcButton.OffsetRect(0, rcButton.Height());
	m_modifyToolBar.AddButton(-1, IDS_LOCK_FOLDSHEET,			ID_LOCK_UNLOCK_FOLDSHEET,		rcButton, 11);


	m_alignToolBar.Create(this, GetParentFrame(), NULL, IDB_ALIGN_TOOLBAR, IDB_ALIGN_TOOLBAR, CSize(28, 22));
	GetDlgItem(IDC_PRINTTOOL_ALIGN_FRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton);	rcButton.bottom = rcButton.top + 30;
	//m_alignToolBar.AddButton(-1, IDS_ALIGN_LEFT,	ID_ALIGN_LEFT,		rcButton, 0);	rcButton.OffsetRect(0, rcButton.Height());
	//m_alignToolBar.AddButton(-1, IDS_ALIGN_XCENTER,	ID_ALIGN_HCENTER,	rcButton, 1);	rcButton.OffsetRect(0, rcButton.Height());
	//m_alignToolBar.AddButton(-1, IDS_ALIGN_RIGHT,	ID_ALIGN_RIGHT,		rcButton, 2);	rcButton.OffsetRect(0, rcButton.Height());
	//m_alignToolBar.AddButton(-1, IDS_ALIGN_BOTTOM,	ID_ALIGN_BOTTOM,	rcButton, 3);	rcButton.OffsetRect(0, rcButton.Height());
	//m_alignToolBar.AddButton(-1, IDS_ALIGN_YCENTER,	ID_ALIGN_VCENTER,	rcButton, 4);	rcButton.OffsetRect(0, rcButton.Height());
	//m_alignToolBar.AddButton(-1, IDS_ALIGN_TOP,		ID_ALIGN_TOP,		rcButton, 5);	
							 
	CFormView::OnInitialUpdate(); 

	SetScaleToFitSize(CSize(rcButton.left + rcButton.right, 1));	//suppress scrollbars
}

void CProdDataPrintToolsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	if ( ! m_pNewPrintSheetFrame)
		return;

	if (m_pNewPrintSheetFrame->PrintSheetViewIsEmpty())
	{
		m_modifyToolBar.Hide();
		m_alignToolBar.Hide();
	}
	else
		if (m_pNewPrintSheetFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
		{
			m_modifyToolBar.Show();
			m_alignToolBar.Hide();
		}
		else
		{
			m_modifyToolBar.Show();
			m_alignToolBar.Show();
		}

	if (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionOutput)
	{
		m_modifyToolBar.Hide();
		m_alignToolBar.Hide();
	}
	else
	{
		m_modifyToolBar.Show();
	}

	COwnerDrawnButton* pButton = m_modifyToolBar.GetButtonByCommand(ID_LOCK_UNLOCK_FOLDSHEET);
	if (pButton && m_modifyToolBar.m_pToolTip)
	{
		CString strTipText; m_modifyToolBar.m_pToolTip->GetText(strTipText, pButton);
		int nBitmapIndex = pButton->m_nBitmapIndex;
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
		{
			CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
			if (pDI)
			{
				CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
				if (pPrintSheet)
				{
					CFoldSheetLayer* pFoldSheetLayer = pPrintSheet->GetFoldSheetLayer((int)pDI->m_pData);
					if (pFoldSheetLayer)
					{
						nBitmapIndex = (pFoldSheetLayer->m_bPagesLocked) ? 11 : 10;
						strTipText.LoadString( (pFoldSheetLayer->m_bPagesLocked) ? IDS_UNLOCK_FOLDSHEET : IDS_LOCK_FOLDSHEET);
					}
				}

				pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
			}
		}

		pButton->m_nBitmapIndex = nBitmapIndex;
		m_modifyToolBar.m_pToolTip->UpdateTipText(strTipText, pButton);
	}
}

BOOL CProdDataPrintToolsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_modifyToolBar.m_pToolTip)            
		m_modifyToolBar.m_pToolTip->RelayEvent(pMsg);	
	if (m_alignToolBar.m_pToolTip)            
		m_alignToolBar.m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

void CProdDataPrintToolsView::OnUpdateDefineTrim(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, (m_pNewPrintSheetFrame->m_dlgTrimming.m_hWnd) ? TRUE : FALSE);
}

void CProdDataPrintToolsView::OnUpdateSwapObjects(CCmdUI* pCmdUI) 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	BOOL bEnable = (pView) ? ( (pView->m_DisplayList.GetNumGroups() == 2) ? TRUE : FALSE) : FALSE;
	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CProdDataPrintToolsView::OnUpdateMoveObject(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, m_pNewPrintSheetFrame->m_bLayoutObjectSelected);
}

void CProdDataPrintToolsView::OnUpdateObjectsTurn(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetTurnActive) );
}

void CProdDataPrintToolsView::OnUpdateObjectsTumble(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetTumbleActive) );
}

void CProdDataPrintToolsView::OnUpdateObjectsrotateClock(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetRotateActive) );
}

void CProdDataPrintToolsView::OnUpdateObjectsrotateCounterclock(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetRotateActive) );
}

void CProdDataPrintToolsView::OnUpdateReleaseBleed(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, m_pNewPrintSheetFrame->m_bEnableReleaseBleedButton);
}

void CProdDataPrintToolsView::OnUpdateDeleteSelection(CCmdUI* pCmdUI) 
{
	if (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetSelected || m_pNewPrintSheetFrame->m_bMarksViewActive)
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	else
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
}

void CProdDataPrintToolsView::OnUpdateUndo(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, m_pNewPrintSheetFrame->m_bUndoActive);
}

void CProdDataPrintToolsView::OnUpdateAlignLeft(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CProdDataPrintToolsView::OnUpdateAlignHCenter(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CProdDataPrintToolsView::OnUpdateAlignRight(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CProdDataPrintToolsView::OnUpdateAlignTop(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CProdDataPrintToolsView::OnUpdateAlignVCenter(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CProdDataPrintToolsView::OnUpdateAlignBottom(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CProdDataPrintToolsView::OnUpdateLockUnlockFoldSheet(CCmdUI* pCmdUI)
{
	BOOL bEnable = FALSE;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
		if (pDI)
			bEnable = TRUE;
		else
		{
			CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			while (pDI)
			{
				CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
				if ( ! pLayoutObject->IsFlatProduct())
				{
					bEnable = TRUE;
					break;
				}
				pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			}
		}
	}

	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

HBRUSH CProdDataPrintToolsView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

    if(nCtlColor == CTLCOLOR_STATIC)
    {
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		//UINT nID = pWnd->GetDlgCtrlID();
		//switch (nID)
		//{
		//default:break;
		//}
	}

	return hbr;
}
