#pragma once


// CDlgPDFImportSettings-Dialogfeld

class CDlgPDFImportSettings : public CDialog
{
	DECLARE_DYNAMIC(CDlgPDFImportSettings)

public:
	CDlgPDFImportSettings(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgPDFImportSettings();

// Dialogfelddaten
	enum { IDD = IDD_PDF_IMPORT_SETTINGS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

public:
	CString m_strFind;
	CString m_strReplace;


public:
	int		LoadSettings();
	int		SaveSettings();
	CString FindAndReplace(const CString& strSource);


DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
