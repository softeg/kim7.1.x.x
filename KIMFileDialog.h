#pragma once


// CKIMFileDialog

class CKIMFileDialog : public CFileDialog
{
	DECLARE_DYNAMIC(CKIMFileDialog)

public:
	CKIMFileDialog(BOOL bOpenFileDialog, // TRUE f�r FileOpen, FALSE f�r FileSaveAs
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL);
	virtual ~CKIMFileDialog();

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


