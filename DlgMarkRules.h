#pragma once



// CDlgMarkRules-Dialogfeld

class CDlgMarkRules : public CDialog
{
	DECLARE_DYNAMIC(CDlgMarkRules)

public:
	CDlgMarkRules(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgMarkRules();

// Dialogfelddaten
	enum { IDD = IDD_MARK_RULES };

public:
	class CDlgLayoutObjectControl*	m_pParent;


protected:
	CListBox	  m_markRulesList;
	CRichEditCtrl m_markRulesEdit;
	CComboBox	  m_markRulesAction;
	int			  m_nMarkRulesRelation;
	CString		  m_strPDFDocFullpath;


public:
	void LoadData();
	void SaveData();
	void Apply();
	BOOL CheckSelection();
	void UpdateEnableState();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedAddMarkruleButton();
	afx_msg void OnBnClickedModifyMarkruleButton();
	afx_msg void OnBnClickedRemoveMarkruleButton();
	afx_msg void OnUpdateModifyMarkruleButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemoveMarkruleButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMarkrulesActionCombo(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMarkruleRelationAnd(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMarkruleRelationOr(CCmdUI* pCmdUI);
	afx_msg void OnLbnDblclkMarkrulesList();
	afx_msg void OnCbnSelchangeMarkrulesActionCombo();
	afx_msg void OnBnClickedMarkruleRelationAnd();
	afx_msg void OnBnClickedMarkruleRelationOr();
	afx_msg void OnBnClickedMarkrulesBrowsePdfpath();
};
