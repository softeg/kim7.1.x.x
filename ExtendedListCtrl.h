#pragma once


// CInplaceListItemControl

class CInplaceListItemControl : public CWnd
{
	DECLARE_DYNAMIC(CInplaceListItemControl)

public:
	CInplaceListItemControl();
	virtual ~CInplaceListItemControl();

friend class CExtendedListCtrl;

protected:
//	BOOL m_bIsLocked;

protected:
	//void Lock()		{ m_bIsLocked = TRUE;  };
	//void Release()	{ m_bIsLocked = FALSE; };

public:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
};


// CInplaceListItemComboBox

class CInplaceListItemComboBox : public CComboBox
{
	DECLARE_DYNAMIC(CInplaceListItemComboBox)

public:
	CInplaceListItemComboBox();
	virtual ~CInplaceListItemComboBox();

public:
	CBrush	m_bkBrush;

public:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/);
};


// CInplaceListItemEdit

class CInplaceListItemEdit : public CEdit
{
	DECLARE_DYNAMIC(CInplaceListItemEdit)

public:
	CInplaceListItemEdit();
	virtual ~CInplaceListItemEdit();

public:
	CBrush	m_bkBrush;

public:
	DECLARE_MESSAGE_MAP()
};



// CInplaceListItemDropDownButton

class CInplaceListItemDropDownButton : public CButton
{
	DECLARE_DYNAMIC(CInplaceListItemDropDownButton)

public:
	CInplaceListItemDropDownButton();
	virtual ~CInplaceListItemDropDownButton();

public:
	BOOL m_bMouseOverButton;

public:
	static void ThemeButton  (CDC* pDC, CRect rcItem, BOOL bMouseOverButton, BOOL bSelected, CWnd* pParentWnd);
	static void ThemeDropDown(CDC* pDC, CRect rcItem, BOOL bMouseOverButton, BOOL bSelected, CWnd* pParentWnd);


public:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
};





// CExtendedListCtrl

typedef struct
{
	int   nInplaceControlType;
	int   nInplaceControlPos;
	BOOL  bLocked;
	CWnd* m_pInplaceControl;
}INPLACECONTROL_INFO[50];


class CExtendedListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CExtendedListCtrl)

public:
	CExtendedListCtrl();
	virtual ~CExtendedListCtrl();

enum {InplaceNone, InplaceEdit, InplaceSpinEdit, InplaceDropDown, InplaceComboBox, InplaceExternal};

friend class CInplaceListItemControl;

private:
	INPLACECONTROL_INFO			m_inplaceControlInfo;
	CBrush						m_bkBrush;

public:
	int							m_iItem, m_iSubItem;
	BOOL						m_bMouseIsOver;


public:
	CWnd*						GetInplaceControl(int nID);
	BOOL 						InplaceControlIsVisible();
	void 						OpenInplaceControl();
	void 						CloseInplaceControl();
	virtual void				InitInplaceControl(int /*iItem*/, int /*iSubItem*/, int /*nCtrlType*/, CWnd* /*pCtrl*/) { };
	void 						LockInplaceControl();
	void 						ReleaseInplaceControl();
	BOOL						MouseOverInplaceControl();

	int InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat = LVCFMT_LEFT, int nWidth = -1, int nSubItem = -1, int nInplaceControlType = InplaceNone, int nInplaceControlPos = 0, int nIDInplaceControl = 0);


protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
};


