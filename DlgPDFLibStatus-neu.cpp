// DlgPDFLibStatus.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "MainFrm.h"
#include "PageListTableView.h"
#ifdef PDF
#include "PDFReader.h"
#endif
#include "PageListDetailView.h"
#include "PageListView.h"
#include "PageSourceListView.h"
#include "DlgLayoutObjectControl.h"
#include "PrintSheetMarksView.h"
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"
#include "DlgPDFEngineSettings.h"
#include "Acrodist.h"
#include "DistillerSink.h"
#include "distctrl.h" 
#include "PageListFrame.h"
#include "DlgSelectJobColors.h"
#include "DlgPDFObjectContent.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "ProductDataView.h"

#include "ptbError.hpp"
#include "ptbPreflight.hpp"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFLibStatus 


CDlgPDFLibStatus::CDlgPDFLibStatus(CWnd* pParent, BOOL bRunModal)
	: CDialog(CDlgPDFLibStatus::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPDFLibStatus)
	m_bRunModal = bRunModal;
	m_pParent = pParent;
	m_strFilename = _T("");
	m_strStatusOutput = _T("");
	m_strCurrentPage = _T("");
	m_strFileSize = _T("");
	m_pPageSource = NULL;
	//}}AFX_DATA_INIT
}


CDlgPDFLibStatus::~CDlgPDFLibStatus()
{
	if (theApp.m_pMainWnd)
		if (theApp.m_pMainWnd->m_hWnd)
			theApp.m_pMainWnd->SetActiveWindow();
	if (  ! m_bRunModal)
		DestroyWindow();
}


void CDlgPDFLibStatus::LaunchPDFEngine(CString strInitialFolder, CWnd* pParent, int nType, CString strPDFFile, BOOL bOpenFileDlg)
{
	m_nType			   = nType;
	m_pParent		   = pParent;
	m_strInitialFolder = strInitialFolder;
	m_bDistillerError  = FALSE;

	m_strStatusOutput.Empty();
	m_strCurrentPage.Empty();
	m_strFilename.Empty();
	m_strFileSize.Empty();

	if ( ! strPDFFile.IsEmpty())
	{
		m_strFilename	  = strPDFFile;
		m_strFullFilename = strPDFFile;
		CFileStatus fileStatus;
		CFile::GetStatus(m_strFilename, fileStatus);
		m_strFileSize.Format(_T("%d KB"), fileStatus.m_size / 1024);
	}

	if ( ! m_hWnd)
		Create(IDD_PDFLIB_STATUS, pParent);

	if ( (m_nType == RegisterMarks) || bOpenFileDlg)
		OnPdflibStatusOpenfile();
}


void CDlgPDFLibStatus::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPDFLibStatus)
	DDX_Control(pDX, IDC_PDFLIB_STATUS_PROGRESS, m_progressBar);
	DDX_Control(pDX, IDC_PDFLIB_STATUS_PREVIEW, m_previewFrame);
	DDX_Text(pDX, IDC_PDFLIB_STATUS_FILENAME, m_strFilename);
	DDX_Text(pDX, IDC_PDFLIB_STATUS_OUTPUT, m_strStatusOutput);
	DDV_MaxChars(pDX, m_strStatusOutput, 65535);
	DDX_Text(pDX, IDC_PDFLIB_CURRENT_PAGE, m_strCurrentPage);
	DDX_Text(pDX, IDC_PDFLIB_STATUS_FILESIZE, m_strFileSize);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPDFLibStatus, CDialog)
	//{{AFX_MSG_MAP(CDlgPDFLibStatus)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_PDFLIB_STATUS_CANCEL, OnPdflibStatusCancel)
	ON_BN_CLICKED(IDC_PDFLIB_STATUS_OPENFILE, OnPdflibStatusOpenfile)
	ON_WM_SIZE()
	ON_WM_DESTROY()
    ON_MESSAGE(WM_COPYDATA, OnCopyData)  	
	ON_BN_CLICKED(IDC_PDF_ENGINE_SETTINGS, OnPdfEngineSettings)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPDFLibStatus 

BOOL CDlgPDFLibStatus::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_progressBar.SetPos(0);

	GetDlgItem(IDC_PDFLIB_STATUS_FILENAME)->ModifyStyle(0, SS_PATHELLIPSIS);

	GetDlgItem(IDC_PDFLIB_STATUS_OPENFILE		)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_PDFLIB_STATUS_PROGRESS		)->ShowWindow(SW_SHOW);

	m_currentClientRect = CRect(0,0,0,0);	// to prevent OnSize() being processed to early
	switch (m_nType)
	{
	case RegisterMarks:	break;
	case RegisterPages: break;
	case UpdatePages:	GetDlgItem(IDC_PDFLIB_STATUS_OPENFILE		)->ShowWindow(SW_HIDE);
						GetDlgItem(IDC_PDFLIB_STATUS_CANCEL			)->ShowWindow(SW_HIDE);
						GetDlgItem(IDCANCEL							)->ShowWindow(SW_HIDE);
						break;
	}

	GetClientRect(m_currentClientRect);
	
	m_nCurrentPage	  = -1;
	m_nTotalPages	  = 0;
	m_bCancel		  = FALSE;
	m_strStatusOutput.Empty();

	DWORD	  dwSize = 255;
	TCHAR	  szValue[255];
	HKEY	  hKey;
	CString	  strKey = theApp.m_strRegistryKey;

	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	dwSize = 10;
	if (RegQueryValueEx(hKey, _T("PDFEngineCreatePreviews"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		m_bCreatePreviews = (_tcscmp(szValue, _T("TRUE")) == 0) ? TRUE : FALSE;
	else
		m_bCreatePreviews = TRUE;
	dwSize = 10;
	if (RegQueryValueEx(hKey, _T("PDFEngineAutoAssignPages"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		m_bAutoAssign = (_tcscmp(szValue, _T("TRUE")) == 0) ? TRUE : FALSE;
	else
		m_bAutoAssign = FALSE;
	dwSize = 10;
	if (RegQueryValueEx(hKey, _T("PDFEnginePreviewResolution"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		m_strResolution = szValue;
	else
		m_strResolution = _T("72");
	dwSize = 255;
	if (RegQueryValueEx(hKey, _T("PDFEngineDistillerSettings"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		m_strDistillerSettings = szValue;
	else
		m_strDistillerSettings = _T("");
	RegCloseKey(hKey);

	//GetDlgItem(IDC_PDFLIB_STATUS_CANCEL)->EnableWindow(FALSE);

	m_strStatusOutput = _T("");

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgPDFLibStatus::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	if (m_currentClientRect.IsRectEmpty())
		return;

	int nDiffX = cx - m_currentClientRect.Width();
	int nDiffY = cy - m_currentClientRect.Height();
	
	CRect rectMove;
	GetDlgItem(IDC_PDFLIB_STATUS_OUTPUT)->GetWindowRect(rectMove); ScreenToClient(rectMove);
	rectMove.right += nDiffX, rectMove.bottom += nDiffY;
	GetDlgItem(IDC_PDFLIB_STATUS_OUTPUT)->MoveWindow(rectMove);

	GetDlgItem(IDC_PDFLIB_STATUS_OPENFILE)->GetWindowRect(rectMove); ScreenToClient(rectMove);
	rectMove += CSize(nDiffX, 0);
	GetDlgItem(IDC_PDFLIB_STATUS_OPENFILE)->MoveWindow(rectMove);

	GetDlgItem(IDC_PDFLIB_STATUS_PROGRESS)->GetWindowRect(rectMove); ScreenToClient(rectMove);
	rectMove += CSize(0, nDiffY);
	GetDlgItem(IDC_PDFLIB_STATUS_PROGRESS)->MoveWindow(rectMove);

	GetDlgItem(IDC_PDFLIB_CURRENT_PAGE)->GetWindowRect(rectMove); ScreenToClient(rectMove);
	rectMove += CSize(0, nDiffY);
	GetDlgItem(IDC_PDFLIB_CURRENT_PAGE)->MoveWindow(rectMove);

	GetDlgItem(IDCANCEL)->GetWindowRect(rectMove); ScreenToClient(rectMove);
	rectMove += CSize(0, nDiffY);
	GetDlgItem(IDCANCEL)->MoveWindow(rectMove);

	GetDlgItem(IDC_PDFLIB_STATUS_CANCEL)->GetWindowRect(rectMove); ScreenToClient(rectMove);
	rectMove += CSize(0, nDiffY);
	GetDlgItem(IDC_PDFLIB_STATUS_CANCEL)->MoveWindow(rectMove);

	GetDlgItem(IDC_PDF_ENGINE_SETTINGS)->GetWindowRect(rectMove); ScreenToClient(rectMove);
	rectMove += CSize(0, nDiffY);
	GetDlgItem(IDC_PDF_ENGINE_SETTINGS)->MoveWindow(rectMove);

	GetDlgItem(IDC_PDFLIB_STATUS_PREVIEW)->GetWindowRect(rectMove); ScreenToClient(rectMove);
	rectMove += CSize(0, nDiffY);
	GetDlgItem(IDC_PDFLIB_STATUS_PREVIEW)->MoveWindow(rectMove);

	m_currentClientRect.right  = cx; m_currentClientRect.bottom = cy;
}

void CDlgPDFLibStatus::OnPdflibStatusOpenfile() 
{
	UpdateData(TRUE);

	CString strFilter;
	strFilter.LoadString(IDS_REGISTER_PDFDOC_FILTER);
	CFileDialog dlg(TRUE, _T("PDF"), NULL, 
					OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ALLOWMULTISELECT | OFN_EXPLORER | OFN_ENABLESIZING,
					strFilter, this);

	dlg.m_ofn.lpstrInitialDir = m_strInitialFolder;
	CString   filenameBuffer;
	const int nMaxFiles = 256; 
	const int nBufSize	= (nMaxFiles * (MAX_PATH + 1)) + 1;
	dlg.m_ofn.nMaxFile  = nBufSize;
	dlg.m_ofn.lpstrFile = filenameBuffer.GetBuffer(nBufSize);
	dlg.m_ofn.lpstrFile[0] = NULL;

	if (dlg.DoModal() == IDCANCEL)
 		return;

	CArray <CString, CString&> pathNameList;
	pathNameList.RemoveAll();
	POSITION pos = dlg.GetStartPosition();
	while (pos)
	{
		CString strPathName = dlg.GetNextPathName(pos);
		pathNameList.Add(strPathName);
	}
 
	filenameBuffer.ReleaseBuffer();

	// TODO: eventually sort pathname list

	m_bDistillerError = FALSE;
	for (int i = 0; (i < pathNameList.GetSize()) && ! m_bDistillerError; i++)
	{
		TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
		_tsplitpath(pathNameList[i], szDrive, szDir, szFname, szExt);
		CString	strFileExt(szExt);
		strFileExt.MakeUpper();
		if (strFileExt != ".PDF")	// assume file is postscript file
		{
			m_strFilename = CString(szDrive) + CString(szDir) + CString(szFname) + _T(".pdf");

//			m_handleDistillerEvent = CreateEvent(NULL, TRUE, FALSE, NULL);	// synchronization does not work for any reason
																			// so it's not possible to import multiple PS files yet
			DistillAndProcess(pathNameList[i]);

//			WaitForSingleObject(m_handleDistillerEvent, INFINITE);
//			CloseHandle(m_handleDistillerEvent);

			break;		// TODO: look for synchronization to support multiple PS file import
		}
		else
		{
			m_nCurrentPage = -1;

			if (m_nType == RegisterPages)
				ProcessPDFPages(pathNameList[i]);
			else
				ProcessPDFMarks(pathNameList[i]);
		}
	}
}

/*
void CDlgPDFLibStatus::DoDistill(CString strFileName)
{
	IPdfDistiller*  pDistiller = new IPdfDistiller;
	CDistillerSink* pSink	   = new CDistillerSink;

	if ( ! pDistiller->CreateDispatch("PdfDistiller.PdfDistiller.1"))
	{
		AfxMessageBox("Unable to open Distiller!", MB_OK);
		return;
	}

	LPUNKNOWN pUnkSink = pSink->GetIDispatch(FALSE);
	DWORD	  dwCookie;
	AfxConnectionAdvise(pDistiller->m_lpDispatch, IID_IDistillerSink, pUnkSink, FALSE, &dwCookie);

	pDistiller->SetBShowWindow(TRUE);

	pDistiller->FileToPDF(strFileName, "", "");

	AfxMessageBox("Test", MB_OK);

	AfxConnectionUnadvise(pDistiller->m_lpDispatch, IID_IDistillerSink, pUnkSink, FALSE, dwCookie);

	pDistiller->ReleaseDispatch();
	delete pDistiller;

	return;
}
*/

void CDlgPDFLibStatus::DistillAndProcess(CString strPSFilename)
{
	COPYDATASTRUCT	cds;
	BOOL			bOK;
	UINT			nRes = 0; 
	long			lRetCode;
	HKEY			hkey;
	TCHAR			szBuffer[255];
	DWORD			dwSize = 255;
	DWORD			dwType;

	lRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,	_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\AcroDist.exe"), 0,	KEY_READ, &hkey);

	if (lRetCode == ERROR_SUCCESS)
	{
		lRetCode = RegQueryValueEx(hkey, _T(""), 0, &dwType, (LPBYTE)szBuffer, &dwSize);

		nRes = WinExec((LPCSTR)szBuffer, SW_SHOWNORMAL);
		if (nRes < 32)
			return;
	}
	else
	{
		AfxMessageBox(IDS_MSG_DISTILLER_NOT_FOUND, MB_OK);
		return;
	}
	
	CWnd *hDistillerCWnd = FindWindow(_T("Distiller"), NULL);

	if (hDistillerCWnd != NULL) 
	{
		TCHAR szAcroDistPath[255];
		RegQueryValueEx(hkey, _T("Path"), 0, &dwType, (LPBYTE)szAcroDistPath, &dwSize);

		CString strJobOptions = CString(szAcroDistPath) + _T("\\Settings\\") + m_strDistillerSettings + _T(".joboptions");
		TCHAR szCmdLine[256];
	    _stprintf(szCmdLine, _T("/J \"%s\" /O \"%s\" \"%s\""), strJobOptions, m_strFilename, strPSFilename);		

		cds.dwData = DM_CMDLINE;
		cds.cbData = _tcslen(szCmdLine) + sizeof(TCHAR);
		cds.lpData = szCmdLine;

		bOK = (BOOL)hDistillerCWnd->SendMessage(WM_COPYDATA, (WPARAM)m_hWnd, (LPARAM)&cds);
		if (bOK)
			hDistillerCWnd->PostMessage(WM_TIMER, ID_TIMER, 0L); 
	}
}

long CDlgPDFLibStatus::OnCopyData(WPARAM /*wparam*/, LPARAM lparam)
{
	int nErrorcode;
	if (((COPYDATASTRUCT *)lparam)->dwData == DM_DONE)
	{
		PDISTILLRECORD pDistRec = (DISTILLRECORD *)((COPYDATASTRUCT *)lparam)->lpData;
		nErrorcode				= pDistRec->param;
		if (nErrorcode == 0) 
		{
			m_nCurrentPage = -1;

			if (m_nType == RegisterPages)
				ProcessPDFPages(m_strFilename);
			else
				ProcessPDFMarks(m_strFilename);

			CWnd *hDistillerCWnd = FindWindow(_T("Distiller"), NULL);
			hDistillerCWnd->ShowWindow(SW_SHOWMINIMIZED);
			m_bDistillerError = FALSE;
		}
		else
			m_bDistillerError = TRUE;
		// if error, do nothing and leave window open
	}

//	SetEvent(m_handleDistillerEvent);
	return lparam;
}

class DynParamCallback: public PTB::PRC::EnumCB<PTB_PRCDynParamID>
{
	virtual void operator()( PTB_PRCDynParamID idParam ) const
	{
	}
}cbDynParam;

class FixupCallback : public PTB::FixupCallback {
public:
	virtual void operator()( const PTB::PRC::Engine&   prcEng  
	                       , PTB_PRCFxupID             idFixup 
	                       , PTB_EFixupState           state   
	                       , const PTB::UTF8_str_t&    reason
	                       ) const
	{
		CString strReason(reason.c_str());

	}
}cbFixup;

class HitCallback : public PTB::HitCallback {
public:
	virtual PTB_EHitCBResult operator()( const PTB::PRC::Engine&   prcEng
	                                     , PTB_PRCProfID             /*idProf*/
	                                     , PTB_PRCRuleID             idRule
	                                     , PTB_PreflightResultID     /*idResult*/
	                                     , PTB_PreflightHitID        /*idHit*/
	                                     , PTB_PRCERuleCheckSeverity ruleSev
	                                     , PTB_uint32_t              pageNum ) const
	{
		return PTB_ehcbrContinue;
	}
}cbHit; 

CDlgPDFLibStatus* p_dlgPreflightParent = NULL;

class ProgressCB : public PTB::ProgressCB {
	virtual bool operator()(       PTB_uint32_t       max
	                       ,       PTB_uint32_t       value
                           ,       PTB_EPreflightPart part
	                       , const PTB::UTF8_str_t&   msg ) const
	{
		return progress( max, value, part, msg);
	}
	virtual bool operator()(       PTB_uint32_t     max
	                       ,       PTB_uint32_t     value
	                       , const PTB::UTF8_str_t& msg ) const
	{
		return progress( max, value, PTB_eppPostCheck, msg);
	}
	virtual bool operator()(       PTB_uint32_t     max
	                       ,       PTB_uint32_t     value ) const
	{
		return progress( max, value, PTB_eppPostCheck, PTB::UTF8_str_t());
	}
private:	
	bool progress(PTB_uint32_t max, PTB_uint32_t value, PTB_EPreflightPart /*part*/, const PTB::UTF8_str_t& msg) const
	{
		CDlgPDFLibStatus* pDlg = p_dlgPreflightParent;

		if ( ! pDlg)
			return FALSE;
		if ( ! pDlg->m_hWnd)
			return FALSE;

		pDlg->UpdateData(TRUE);

		float fRatio  = (float)max / 100.0f;
		int	  nActual = (int)((float)value / fRatio);

		pDlg->m_strCurrentPage = msg.c_str();//pDlg->m_strCurrentPage.Format(IDS_PDFLIB_CURRENT_PAGE, value, max);
		pDlg->m_strCurrentPage += "\r\n";

		pDlg->UpdateData(FALSE);

		pDlg->m_progressBar.SetRange((short)1, (short)100);
		pDlg->m_progressBar.SetPos(nActual);

		if (pDlg->m_bCancel)
			return FALSE;
		else
			return TRUE;
	}
}cbProg; 

class SaveCallback : public PTB::SaveCallback {
public:
	virtual PTB::Path_t operator()( PTB_PreflightResultID /*idResult*/
	                              , PTB_ESaveAsReason     reason
	                              , PTB_ESaveAsReasonPDFX reasonPDFX
	                              , PTB_EPDFXVersion      versionPDFX
	                              , PTB_ESaveAsReasonPDFA reasonPDFA
	                              , PTB_EPDFAVersion      versionPDFA
	                              , const PTB::Path_t&    currentPath ) const
	{
		return currentPath;
	}
}cbSave; 


void CDlgPDFLibStatus::ProcessPDFPages(CString strPDFFile, CPageSource* pPageSourceToUpdate, BOOL bPreviewsOnly)
{
	CMainFrame* pMainFrame = (CMainFrame*)theApp.m_pMainWnd;
	if ( ! pMainFrame)
		return;

	if (IPL_GetPDFImpositionDLLVersion() != 0x00020003) 
	{
		AfxMessageBox(_T("pdfToolbox library version incorrect, must be 0x00020003!"), MB_OK | MB_ICONSTOP);
		return;
	}

	m_bCancel = FALSE;

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath(strPDFFile, szDrive, szDir, szFname, szExt);
	CString strPDFInfoFile(CString(szDrive) + CString(szDir) + CString(szFname) + _T(".pdfinfo"));

	if (theApp.settings.m_bPDFPreviewsOrigPlace)
		m_strPreviewsFolder = CString(szDrive) + CString(szDir) + CString(szFname) + _T(".preview") + '\\';
	else
		m_strPreviewsFolder = theApp.settings.m_szPDFPreviewFolder + CString("\\") + CString(szFname) + _T(".preview") + '\\';
	m_strInitialFolder  = CString(szDrive) + CString(szDir);

	CFileStatus statusPDFFile;
	if ( ! CFile::GetStatus(strPDFFile, statusPDFFile))
		return;
	m_strFilename	  = CString(szFname) + szExt;
	m_strFullFilename = strPDFFile;
	m_strFileSize.Format(_T("%d KB"), statusPDFFile.m_size / 1024);

	pMainFrame->EnableWindow(FALSE);

	COleDateTime timeStart(COleDateTime::GetCurrentTime());
	IPL_int32_t	 nResult = 0;
	if ( ! bPreviewsOnly)
	{
		BOOL bDoAnalyse = TRUE;
		CFileStatus statusPDFInfoFile;
		if (CFile::GetStatus(strPDFInfoFile, statusPDFInfoFile))		
			if ( (statusPDFInfoFile.m_mtime == statusPDFFile.m_mtime) && CPageSource::IsPdfToolboxReport(strPDFInfoFile) )		// pdf info is up to date -> no need to analyse
				bDoAnalyse = FALSE;
		if (bDoAnalyse)
		{
			//IPL_SetProgressFunction( CDlgPDFLibStatus::ProgressListContents, (void*)this);

			BeginWaitCursor();
			UpdateStatus(CDlgPDFLibStatus::OpenDocument);
			UpdateStatus(CDlgPDFLibStatus::AnalyseContent);

			CSysToUTF8Converter s2utf8;
			//nResult = IPL_PDFDocument_ListContents( s2utf8.Convert(strPDFFile), s2utf8.Convert(strPDFInfoFile), s2utf8.Convert(theApp.settings.m_szTempDir), IPL_ePostScriptInfo);

			//////////// preflight report
			p_dlgPreflightParent = this;
			PTB::PRC::Engine prcEng;
			PTB_PreflightResultID idRes = 0;
			CString strKfpxFile1, strKfpxFile2; 
			//strKfpxFile1.Format(_T("%s\\var\\Profiles\\Ebenen_reduzieren.kfpx"), theApp.settings.m_szWorkDir);
			strKfpxFile2.Format(_T("%s\\var\\Profiles\\Apply rotation parameter.kfpx"), theApp.settings.m_szWorkDir);
			try 
			{
				prcEng.importPackage( PTB::string_cast<PTB::Path_t>(s2utf8.Convert(strKfpxFile1)), ProgressCB() );
			}
			catch ( const PTB::exc& x) 
			{
				CString strMsg; strMsg.Format(_T("%s: %s"), strKfpxFile1, x.what());
				AfxMessageBox(strMsg);
			}
			try 
			{
				prcEng.importPackage( PTB::string_cast<PTB::Path_t>(s2utf8.Convert(strKfpxFile2)), ProgressCB() );
			}
			catch ( const PTB::exc& x) 
			{
				CString strMsg; strMsg.Format(_T("%s: %s"), strKfpxFile2, x.what());
				AfxMessageBox(strMsg);
			}
			PTB::PRC::ListEnumCB<PTB_PRCProfID> cbProfs;
			prcEng.enumObjects(cbProfs);
			PTB_PRCProfID idProf;
			if( ! cbProfs.objects.empty() ) 
				idProf = *cbProfs.objects.begin();
			else
			{
				PTB::PRC::ProfData data( PTB::string_cast<PTB::UTF8_str_t>("Name"), PTB::string_cast<PTB::UTF8_str_t>("Comment"), PTB_epdfxaNoConversions, PTB::string_cast<PTB::UTF8_str_t>("") ); // empty profile
				idProf = prcEng.addObject( data);
			}

			BOOL bPreflightError = FALSE;
			try 
			{
				idRes = PTB::preflight( s2utf8.Convert(strPDFFile), prcEng, idProf, cbDynParam, cbFixup, cbHit, cbProg, cbSave, false, 0, PTB_MAX_END_PAGE );
			}
			catch ( const PTB::exc& x) 
			{
				AfxMessageBox(x.what());
				bPreflightError = TRUE;
			}

			if ( ! bPreflightError)
			{
				try 
				{
					PTB::report( idRes, s2utf8.Convert(strPDFInfoFile), PTB_eXMLResults_V2, PTB_eCR, PTB_eUTF8, PTB_eHits | PTB_ePage | PTB_eDoc, PTB_eAllDetails, cbProg, 0, PTB_MAX_END_PAGE);
				}
				catch ( const PTB::exc& x) 
				{
					AfxMessageBox(x.what());
					bPreflightError = TRUE;
				}
			}
			PTB_PreflightRelease( idRes, NULL );
			p_dlgPreflightParent = NULL;
			//////////////////////////////

			//UpdateStatus((nResult) ? CDlgPDFLibStatus::IPL_Error : CDlgPDFLibStatus::OK, nResult);
			if ( ! bPreflightError)
				UpdateStatus(CDlgPDFLibStatus::OK, 0);
			EndWaitCursor();

			if (CFile::GetStatus(strPDFInfoFile, statusPDFInfoFile))	// allways give pdfinfo file same timestamp as pdf file
			{															// otherwise we could run in a loop when pdf file is younger than system date/time
				if (CFile::GetStatus(strPDFFile, statusPDFFile))		// -> pdfinfo allways out of date
					if (statusPDFFile.m_mtime != -1)
					{														// this solves the problem of asynchronous system times in network environments
						statusPDFInfoFile.m_mtime = statusPDFFile.m_mtime;		
						CFile::SetStatus(strPDFInfoFile, statusPDFInfoFile);	
					}
			}
		}
	}

	if ( ! nResult)
	{
		BOOL bPreviewsPresent = FALSE;
		CString strFolder;
		strFolder = m_strPreviewsFolder.Left(m_strPreviewsFolder.ReverseFind('\\'));
		CFileStatus fileStatus;
		if (CFile::GetStatus(strFolder, fileStatus))
			bPreviewsPresent = TRUE;

		CImpManDoc* pDoc	  = CImpManDoc::GetDoc();
		int			nNumPages = 0;
		if ( (m_nType == RegisterPages) && ! bPreviewsOnly)
		{
			if ( ! m_pPageSource)
			{
				CPageSource pageSource;
				pageSource.Create(CPageSource::PDFFile, strPDFFile, strPDFInfoFile, m_strPreviewsFolder);
				pageSource.RegisterPDFPages(strPDFFile, strPDFInfoFile);
				UpdateStatus(CDlgPDFLibStatus::ContentInfo, pageSource.m_PageSourceHeaders.GetSize());
				pDoc->m_PageSourceList.AddTail(pageSource);
				nNumPages = pageSource.m_PageSourceHeaders.GetSize();
			}
			else
			{
				m_pPageSource->Create(CPageSource::PDFFile, strPDFFile, strPDFInfoFile, m_strPreviewsFolder);
				m_pPageSource->RegisterPDFPages(strPDFFile, strPDFInfoFile);
				UpdateStatus(CDlgPDFLibStatus::ContentInfo, m_pPageSource->m_PageSourceHeaders.GetSize());
				nNumPages = m_pPageSource->m_PageSourceHeaders.GetSize();
			}
		}
		else
			if (pPageSourceToUpdate && ! bPreviewsOnly)
			{
				pPageSourceToUpdate->UpdatePDFInfo(strPDFFile, strPDFInfoFile);
				nNumPages = pPageSourceToUpdate->m_PageSourceHeaders.GetSize();
			}

		if ( ! bPreviewsOnly)
		{
			pDoc->SetModifiedFlag(TRUE);

			theApp.OnUpdateView(RUNTIME_CLASS(CPageListDetailView), NULL, 0, NULL);
			theApp.OnUpdateView(RUNTIME_CLASS(CPageListTableView),	NULL, 0, NULL);
			theApp.OnUpdateView(RUNTIME_CLASS(CPageSourceListView),	NULL, 0, NULL);
		}

		BOOL bUpdatePreviews = FALSE;
		if (m_bCreatePreviews && (m_nType == UpdatePages) )
		{
			CString strMsg;
			AfxFormatString1(strMsg, IDS_UPDATEPREVIEWS_PROMPT, strPDFFile);
			if (AfxMessageBox(strMsg, MB_YESNO) == IDYES)
				bUpdatePreviews = TRUE;
			SetFocus();
		}

		if ( ! bPreviewsPresent || bUpdatePreviews || bPreviewsOnly)
		{
			if (m_bCreatePreviews || bUpdatePreviews || bPreviewsOnly)
			{
				int nRes = _ttoi((LPCTSTR)m_strResolution);
				if (nRes <= 0) nRes = 72;
				UpdateStatus(CDlgPDFLibStatus::CreatePreviews);

				if (pPageSourceToUpdate)
				{
					///// delete and reset thumbnails if present
					for (int i = 0; i < pPageSourceToUpdate->m_PageSourceHeaders.GetSize(); i++)
					{
						if (pPageSourceToUpdate->m_PageSourceHeaders[i].m_pThumbnailImage)
						{
							delete pPageSourceToUpdate->m_PageSourceHeaders[i].m_pThumbnailImage;
							pPageSourceToUpdate->m_PageSourceHeaders[i].m_pThumbnailImage = NULL;
							CString strPreviewFileTitle = GetPreviewFilename(m_strPreviewsFolder, i) + _T(".jpg");
							remove(strPreviewFileTitle);
						}
					}
				}

				BeginWaitCursor();

				m_nTotalPages = nNumPages;
				PTB_sys_char_t szRes[20];
				itoa(nRes, szRes, 10);
				CSysToUTF8Converter s2utf8;
				nResult = PTB_SaveAsImage(s2utf8.Convert(strPDFFile), PTB_eJPEG, szRes, PTB_eBaseline_Standard, PTB_eJPEG_high, 0, PTB_MAX_END_PAGE, s2utf8.Convert(m_strPreviewsFolder), 
										   SaveAsImageCallBack, (void*)this, ProgressCreatePreviews, (void*)this);

				EndWaitCursor();

				UpdateStatus((nResult) ? CDlgPDFLibStatus::PTB_Error : CDlgPDFLibStatus::OK, nResult);
				if ( ! nResult)
				{
					if ( ! bPreviewsOnly)
						UpdateStatus(CDlgPDFLibStatus::PreviewInfo, nNumPages);
					else
						UpdateStatus(CDlgPDFLibStatus::PreviewInfo, m_nCurrentPage);
					COleDateTime timeEnd(COleDateTime::GetCurrentTime());
					COleDateTimeSpan duration(timeEnd - timeStart);
					CString strDuration = duration.Format(_T("%H:%M:%S"));
					UpdateStatus(CDlgPDFLibStatus::Duration, -1, strDuration);
				}
			}
			else
				UpdateStatus(CDlgPDFLibStatus::NoPreviews);
		}

		if (m_pParent && m_pPageSource)
			if (m_pParent->IsKindOf(RUNTIME_CLASS(CDlgSelectJobColors)))
			{
				( (CDlgSelectJobColors*)m_pParent)->PDFEngineDataExchange(m_pPageSource);
				return;
			}
			else
			if (m_pParent->IsKindOf(RUNTIME_CLASS(CProductDataView)))
			{
				( (CProductDataView*)m_pParent)->PDFEngineDataExchange(m_pPageSource);
				return;
			}

		if (m_bAutoAssign && (m_nType == RegisterPages) && ! bPreviewsOnly && ! m_pPageSource)
		{
			int nProductPartIndex = 0;
			if (m_pParent)
				if (m_pParent->IsKindOf(RUNTIME_CLASS(CPageListFrame)))
					nProductPartIndex = ((CPageListFrame*)m_pParent)->m_nProductPartIndex;
			pDoc->m_PageTemplateList.AutoAssignPageSource(&pDoc->m_PageSourceList.GetTail(), nProductPartIndex);
			theApp.OnUpdateView(RUNTIME_CLASS(CPageListView), NULL, 0, NULL);
			theApp.OnUpdateView(RUNTIME_CLASS(CPageSourceListView),	NULL, 0, NULL);
		}
	}

	pMainFrame->EnableWindow(TRUE);
}


void CDlgPDFLibStatus::ProcessPDFMarks(CString strPDFFile)
{
	CMainFrame* pMainFrame = (CMainFrame*)theApp.m_pMainWnd;
	if ( ! pMainFrame)
		return;

	if (IPL_GetPDFImpositionDLLVersion() != 0x00020003) 
	{
		AfxMessageBox(_T("pdfToolbox library version incorrect, must be 0x00020003!"), MB_OK | MB_ICONSTOP);
		return;
	}

	m_bCancel = FALSE;

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath(strPDFFile, szDrive, szDir, szFname, szExt);
	CString strPDFInfoFile(CString(szDrive) + CString(szDir) + CString(szFname) + _T(".pdfinfo"));
	
	if (theApp.settings.m_bPDFPreviewsOrigPlace)
		m_strPreviewsFolder = CString(szDrive) + CString(szDir) + CString(szFname) + _T(".preview") + '\\';
	else
		m_strPreviewsFolder = theApp.settings.m_szPDFPreviewFolder + CString("\\") + CString(szFname) + _T(".preview") + '\\';
	m_strInitialFolder  = CString(szDrive) + CString(szDir);

	CFileStatus statusPDFFile;
	if ( ! CFile::GetStatus(strPDFFile, statusPDFFile))
		return;
	m_strFilename	  = CString(szFname) + szExt;
	m_strFullFilename = strPDFFile;
	m_strFileSize.Format(_T("%d KB"), statusPDFFile.m_size / 1024);

	pMainFrame->EnableWindow(FALSE);

	IPL_int32_t nResult = 0;
	BOOL  bDoAnalyse = TRUE;
	CFileStatus statusPDFInfoFile;
	if (CFile::GetStatus(strPDFInfoFile, statusPDFInfoFile))		
		if ( (statusPDFInfoFile.m_mtime == statusPDFFile.m_mtime) && CPageSource::IsPdfToolboxReport(strPDFInfoFile) )		// pdf info is up to date -> no need to analyse
			bDoAnalyse = FALSE;

	CSysToUTF8Converter s2utf8;
	if (bDoAnalyse)
	{
		//IPL_SetProgressFunction( CDlgPDFLibStatus::ProgressListContents, (void*)this);

		BeginWaitCursor();
		UpdateStatus(CDlgPDFLibStatus::OpenDocument);
		UpdateStatus(CDlgPDFLibStatus::AnalyseContent);
		//nResult = IPL_PDFDocument_ListContents(s2utf8.Convert(strPDFFile), s2utf8.Convert(strPDFInfoFile), s2utf8.Convert(theApp.settings.m_szTempDir), IPL_ePostScriptInfo);

		//////////// preflight report
		p_dlgPreflightParent = this;
		PTB::PRC::Engine prcEng;
		PTB::PRC::ProfData data( PTB::string_cast<PTB::UTF8_str_t>("Name"), PTB::string_cast<PTB::UTF8_str_t>("Comment"), PTB_epdfxaNoConversions, PTB::string_cast<PTB::UTF8_str_t>("") ); // empty profile
		PTB_PRCProfID idProf = prcEng.addObject( data);
		PTB_PreflightResultID idRes = 0;
		BOOL bPreflightError = FALSE;
		try 
		{
			idRes = PTB::preflight( s2utf8.Convert(strPDFFile), prcEng, idProf, cbDynParam, cbFixup, cbHit, cbProg, cbSave, false, 0, PTB_MAX_END_PAGE );
		}
		catch ( const PTB::exc& x) 
		{
			AfxMessageBox(x.what());
			bPreflightError = TRUE;
		}

		if ( ! bPreflightError)
		{
			try 
			{
				PTB::report( idRes, s2utf8.Convert(strPDFInfoFile), PTB_eXMLResults_V2, PTB_eCR, PTB_eUTF8, PTB_eHits | PTB_ePage | PTB_eDoc, PTB_eAllDetails, cbProg, 0, PTB_MAX_END_PAGE);
			}
			catch ( const PTB::exc& x) 
			{
				AfxMessageBox(x.what());
				bPreflightError = TRUE;
			}
		}
		PTB_PreflightRelease( idRes, NULL );
		p_dlgPreflightParent = NULL;
		//////////////////////////////

		//UpdateStatus((nResult) ? CDlgPDFLibStatus::IPL_Error : CDlgPDFLibStatus::OK, nResult);
		if ( ! bPreflightError)
			UpdateStatus(CDlgPDFLibStatus::OK, 0);
		EndWaitCursor();
		if (CFile::GetStatus(strPDFInfoFile, statusPDFInfoFile))	// allways give pdfinfo file same timestamp as pdf file
		{															// otherwise we could run in a loop when pdf file is younger than system date/time
			if (CFile::GetStatus(strPDFFile, statusPDFFile))		// -> pdfinfo allways out of date
				if (statusPDFFile.m_mtime != -1)
				{														// this solves the problem of asynchronous system times in network environments
					statusPDFInfoFile.m_mtime = statusPDFFile.m_mtime;		
					CFile::SetStatus(strPDFInfoFile, statusPDFInfoFile);	
				}
		}
	}

	if ( ! nResult)
	{
		CPageSource markSource;
		markSource.Create(CPageSource::PDFFile, strPDFFile, strPDFInfoFile, m_strPreviewsFolder);
		markSource.RegisterPDFPages(strPDFFile, strPDFInfoFile);
		UpdateStatus(CDlgPDFLibStatus::ContentInfo, markSource.m_PageSourceHeaders.GetSize());

		if (m_bCreatePreviews)
		{
			UpdateStatus(CDlgPDFLibStatus::CreatePreviews);
			m_nTotalPages = markSource.m_PageSourceHeaders.GetSize();
			PTB_sys_char_t szRes[20];
			itoa(CDlgPDFObjectContent::GetOptPreviewResolution(markSource), szRes, 10);

			BeginWaitCursor();

			nResult = PTB_SaveAsImage(s2utf8.Convert(strPDFFile), PTB_eJPEG, szRes, PTB_eBaseline_Standard, PTB_eJPEG_high, 0, PTB_MAX_END_PAGE, s2utf8.Convert(m_strPreviewsFolder), 
									   SaveAsImageCallBack, (void*)this, ProgressCreatePreviews, (void*)this);

			EndWaitCursor();

			UpdateStatus((nResult) ? CDlgPDFLibStatus::PTB_Error : CDlgPDFLibStatus::OK, nResult);
			if ( ! nResult)
				UpdateStatus(CDlgPDFLibStatus::PreviewInfo, markSource.m_PageSourceHeaders.GetSize());
		}
		else
			UpdateStatus(CDlgPDFLibStatus::NoPreviews);

		if (m_pParent)
			if (m_pParent->IsKindOf(RUNTIME_CLASS(CDlgPDFObjectContent)))
				( (CDlgPDFObjectContent*)m_pParent)->PDFEngineDataExchange(&markSource);
	}

	pMainFrame->EnableWindow(TRUE);
}


IPL_bool_t CDlgPDFLibStatus::ProgressListContents( IPL_int32_t lowrange, IPL_int32_t highrange, IPL_int32_t counter, void* lparam)
{
	CDlgPDFLibStatus* pDlg = (CDlgPDFLibStatus*)lparam;

	if ( ! pDlg->m_hWnd)
		return FALSE;

	pDlg->UpdateData(TRUE);

	pDlg->m_strCurrentPage.Format(IDS_PDFLIB_CURRENT_PAGE, counter, highrange);
	pDlg->m_strCurrentPage += "\r\n";

	pDlg->UpdateData(FALSE);

	pDlg->m_progressBar.SetRange((short)lowrange, (short)highrange);
	pDlg->m_progressBar.SetPos(counter);

	if (pDlg->m_bCancel)
		return FALSE;
	else
		return TRUE;
}

IPL_bool_t CDlgPDFLibStatus::ProgressCreatePreviews(  PTB_uint32_t max, PTB_uint32_t current, void* userData )
{
	if ( ! userData)
		return FALSE;

	CDlgPDFLibStatus* pDlg = (CDlgPDFLibStatus*)userData;
	if ( ! pDlg->m_hWnd)
		return FALSE;

	int lowrange  = 1;
	int highrange = 100;
	int nPercent  = 100*current/max;
	int nCurPage  = pDlg->m_nTotalPages * nPercent/100;
	if (pDlg->m_nCurrentPage == nCurPage)
		return TRUE;

	pDlg->UpdateData(TRUE); 

	pDlg->m_strCurrentPage.Format(IDS_PDFLIB_CURRENT_PAGE, nCurPage, pDlg->m_nTotalPages);
	pDlg->m_strCurrentPage += "\r\n";

	pDlg->UpdateData(FALSE);

	pDlg->m_nCurrentPage = nCurPage;
	CRect previewFrameRect;
	pDlg->m_previewFrame.GetClientRect(previewFrameRect);
	pDlg->m_previewFrame.MapWindowPoints(pDlg, previewFrameRect);
	pDlg->InvalidateRect(previewFrameRect);
	pDlg->UpdateWindow();

	pDlg->m_progressBar.SetRange((short)lowrange, (short)highrange);
	pDlg->m_progressBar.SetPos(nPercent);

	MSG msg;
	if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		if (msg.message == WM_LBUTTONDOWN)
		{
			CRect rcCancel;
			pDlg->GetDlgItem(IDC_PDFLIB_STATUS_CANCEL)->GetWindowRect(rcCancel);
			CPoint ptMousePos;
			GetCursorPos(&ptMousePos);
			if (rcCancel.PtInRect(ptMousePos))
				if (AfxMessageBox(IDS_PDFLIB_REALLY_CANCEL, MB_YESNO) == IDYES)
					return FALSE;
		}
	}

	if ( ! pDlg->m_bCreatePreviews)
		pDlg->m_bCancel = TRUE;

	if (pDlg->m_bCancel)
		return FALSE;
	else
		return TRUE;
}

void CDlgPDFLibStatus::SaveAsImageCallBack( PTB_StringID idPath, PTB_int32_t page, void* userData)
{
	if ( ! userData)
		return;

	CDlgPDFLibStatus* pDlg = (CDlgPDFLibStatus*)userData;
	if ( ! pDlg->m_hWnd)
		return;

	PTB_size_t		nBufSize = 1024;
	IPL_utf8_char_t	szOldPathUTF8[1024];
	TCHAR			szOldPathSys[1024];
	CString			strNewPath; strNewPath.Format(_T("%sKim%05d.jpg"), pDlg->m_strPreviewsFolder, page);
	PTB_StringGetAndRelease(idPath, szOldPathUTF8, &nBufSize); nBufSize = 1024;
	PTB_ConvertUTF8ToSys(szOldPathUTF8, szOldPathSys, &nBufSize);

	remove(strNewPath);
	rename(szOldPathSys, strNewPath);

	int lowrange  = 1;
	int highrange = pDlg->m_nTotalPages;
	int counter	  = page + 1;

	pDlg->UpdateData(TRUE);

	pDlg->m_strCurrentPage.Format(IDS_PDFLIB_CURRENT_PAGE, counter, highrange);
	pDlg->m_strCurrentPage += "\r\n";

	pDlg->UpdateData(FALSE);

	pDlg->m_progressBar.SetRange((short)lowrange, (short)highrange);
	pDlg->m_progressBar.SetPos(counter);
}

void CDlgPDFLibStatus::UpdateStatus(int nStatus, int nCode, CString strOut, CString strOut2)
{
	if ( ! m_hWnd)
		return;

	if (nStatus != OpenDocument)
		m_strStatusOutput += "   ";

	if (nStatus == OutputComplete)
	{
		CFileStatus fileStatus;
		CFile::GetStatus(m_strFullFilename, fileStatus);
		m_strFileSize.Format(_T("%d KB"), fileStatus.m_size / 1024);
	}

	CString			strMsg;
	IPL_utf8_char_t	szMsgUTF8[1024];
	PTB_StringID	nStringID;
	PTB_size_t		nBufSize = 1024;
	switch (nStatus)
	{
	case OpenDocument:	strMsg.Format(IDS_PDFLIB_OPEN_DOCUMENT, m_strFullFilename);	m_strStatusOutput += strMsg + "\r\n";		 	m_nStatus = nStatus;	break;
	case AnalyseContent:strMsg.LoadString(IDS_PDFLIB_ANALYSE_CONTENT);				m_strStatusOutput += strMsg;				 	m_nStatus = nStatus;	break;
	case CreatePreviews:strMsg.LoadString(IDS_PDFLIB_CREATE_PREVIEWS);				m_strStatusOutput += strMsg;				 	m_nStatus = nStatus;	break;
	case NoPreviews:	strMsg.LoadString(IDS_PDFLIB_NO_PREVIEWS);					m_strStatusOutput += strMsg + "\r\n";		 	m_nStatus = Idle;		break;
	case PreviewInfo:	strMsg.Format(IDS_PDFLIB_PREVIEW_INFO, nCode);				m_strStatusOutput += strMsg + "\r\n";		 	m_nStatus = Idle;		break;
	case ContentInfo:	strMsg.Format(IDS_PDFLIB_CONTENT_INFO, nCode);				m_strStatusOutput += strMsg + "\r\n";		 	m_nStatus = Idle;		break;
	case OutputSheet:	strMsg.Format(IDS_PDFLIB_OUTPUT_SHEET, nCode);				m_strStatusOutput += strMsg + "\r\n";		 	m_nStatus = nStatus;	break;
	case OutputPlate:   strMsg.Format(_T("   %s (%s): "), strOut, strOut2);			m_strStatusOutput += strMsg + "\r\n";		 	m_nStatus = nStatus;	break;
	case OutputTile:	strMsg.Format(IDS_PDFLIB_OUTPUT_TILE,  nCode);				m_strStatusOutput += "    " + strMsg + "\r\n";	m_nStatus = nStatus;	break;
	case OutputPage:	strMsg.Format((nCode) ? _T("%s") : _T("%s"), strOut);		m_strStatusOutput += _T("    ") + strMsg;		m_nStatus = nStatus;	break;
	case OutputMarks:	strMsg.LoadString(IDS_PDFLIB_OUTPUT_MARKS);					m_strStatusOutput += "   " + strMsg + "\r\n";	m_nStatus = nStatus;	break;
	case OutputComplete:strMsg.LoadString(IDS_PDFLIB_OUTPUT_COMPLETE);				m_strStatusOutput += (strMsg + "\r\n");		 
																																 	m_nStatus = Idle;		break;
	case Idle:			m_strStatusOutput += _T("\r\n");																		 	m_nStatus = Idle;		break;
	case OK:			m_strStatusOutput += _T("  OK\r\n");																	 	m_nStatus = Idle;		break;
	case IPL_Error:		IPL_GetErrorStringFromErrorCode( nCode, szMsgUTF8, 1024, MapLanguageID()); 
						PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();
						m_strStatusOutput += _T("  Error!\r\n   ") + strMsg + "\r\n";  											 	m_nStatus = Idle;		break;

	case PTB_Error:		PTB_GetErrorMessage( (PTB_EError)nCode, (PTB_StringID*)&nStringID); 
						PTB_StringGetAndRelease(nStringID, szMsgUTF8, &nBufSize); nBufSize = 1024;				
						PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();
						m_strStatusOutput += _T("  Error!\r\n   ") + strMsg + "\r\n";  											 	m_nStatus = Idle;		break;
	case Duration:		strMsg.Format(IDS_PDFLIB_DURATION, strOut);					m_strStatusOutput += strMsg + "\r\n";		 	m_nStatus = Idle;		break;
	}

	if (m_nStatus == Idle)
	{
		m_strCurrentPage.Empty();
		m_progressBar.SetPos(0);
		//GetDlgItem(IDC_PDFLIB_STATUS_CANCEL)->EnableWindow(FALSE);
		GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_PDFLIB_STATUS_CANCEL)->EnableWindow(TRUE);
		GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
	}

	UpdateData(FALSE);
	
	int nCaretPos = m_strStatusOutput.GetLength();
	( (CEdit*)GetDlgItem(IDC_PDFLIB_STATUS_OUTPUT) )->SetSel(nCaretPos, nCaretPos);

	GetDlgItem(IDC_PDFLIB_STATUS_OUTPUT)->Invalidate();
	GetDlgItem(IDC_PDFLIB_STATUS_OUTPUT)->UpdateWindow();
}

int CDlgPDFLibStatus::MapLanguageID() 
{
	switch (theApp.settings.m_iLanguage)
	{
	case 0:  return 1;	// german
	case 1:  return 0;	// english
	default: return 0;	// default english
	}
}

void CDlgPDFLibStatus::OnCancel() 
{
	m_bCancel = TRUE;
	if (theApp.m_pMainWnd)
		if (theApp.m_pMainWnd->m_hWnd)
			theApp.m_pMainWnd->SetActiveWindow();
	if (  ! m_bRunModal)
		DestroyWindow();

	if (m_pParent)
		if (m_pParent->GetSafeHwnd())
			m_pParent->SetFocus();

	if (m_bRunModal)
		CDialog::OnCancel();
}


void CDlgPDFLibStatus::OnPdflibStatusCancel() 
{
	m_bCancel = TRUE;
}

CString CDlgPDFLibStatus::GetPreviewFilename(CString strPreviewsFolder, int nPageIndex)
{
	CString strPreviewFileTitle;
	strPreviewFileTitle.Format(_T("%sKim%05d"), strPreviewsFolder, nPageIndex);

	CFileStatus status;
	CString strFileName = strPreviewFileTitle + _T(".jpg");	
	if (CFile::GetStatus(strFileName, status))
		return strPreviewFileTitle;

	CString strFolder = strPreviewsFolder;
	strFolder.MakeLower();
	int nIndex = strFolder.Find(_T(".preview"));
	if (nIndex < 0)
		return _T("");

	strFolder = strFolder.Left(nIndex);
	nIndex = strFolder.ReverseFind('\\');
	if (nIndex < 0)
		nIndex = strFolder.ReverseFind(':');
	if (nIndex >= 0)
		strFolder = strFolder.Right(strFolder.GetLength() - nIndex - 1);

	strPreviewFileTitle.Format(_T("%s%s_%d"), strPreviewsFolder, strFolder, nPageIndex + 1);
	strFileName = strPreviewFileTitle + _T(".jpg");	
	if (CFile::GetStatus(strFileName, status))
		return strPreviewFileTitle;
	else
		return _T("");
}

void CDlgPDFLibStatus::OnPaint() 
{
	if ( ! m_hWnd)
		return;

	CPaintDC dc(this); // device context for painting
	
	CRect previewFrameRect;
	m_previewFrame.GetClientRect(previewFrameRect);
	m_previewFrame.MapWindowPoints(this, previewFrameRect);

	CString strPreviewFileTitle = GetPreviewFilename(m_strPreviewsFolder, m_nCurrentPage - 1);

	CPageListView::DrawPreview(&dc, previewFrameRect, TOP, 0.0f, strPreviewFileTitle, TRUE);		// TRUE = keep ratio of x and y edges
}

void CDlgPDFLibStatus::OnDestroy()  
{
	CDialog::OnDestroy();

	HKEY	hKey;
	DWORD	dwDisposition; 
	CString	strKey = theApp.m_strRegistryKey;
	RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, &dwDisposition); 
	RegSetValueEx(hKey, _T("PDFEngineCreatePreviews"),		0, REG_SZ, (LPBYTE)((m_bCreatePreviews)		 ? _T("TRUE") : _T("FALSE")), 6);
	RegSetValueEx(hKey, _T("PDFEngineAutoAssignPages"),		0, REG_SZ, (LPBYTE)((m_bAutoAssign)			 ? _T("TRUE") : _T("FALSE")), 6);
	RegSetValueEx(hKey, _T("PDFEnginePreviewResolution"),	0, REG_SZ, (LPBYTE)m_strResolution.GetBuffer(MAX_TEXT), m_strResolution.GetLength() + 1);
	RegSetValueEx(hKey, _T("PDFEngineDistillerSettings"),	0, REG_SZ, (LPBYTE)m_strDistillerSettings.GetBuffer(MAX_TEXT), m_strDistillerSettings.GetLength() + 1);
	RegCloseKey(hKey);
}

void CDlgPDFLibStatus::OnPdfEngineSettings() 
{
	CDlgPDFEngineSettings dlg;

	dlg.m_bCreatePreviews		= m_bCreatePreviews;
	dlg.m_strResolution			= m_strResolution;
	dlg.m_bAutoAssign			= m_bAutoAssign;
	dlg.m_strDistillerSettings	= m_strDistillerSettings;
	
	if (dlg.DoModal() == IDCANCEL)
		return;

	if (m_strResolution != dlg.m_strResolution)
	{
		if ( (_ttoi(dlg.m_strResolution) >= 5) && (_ttoi(dlg.m_strResolution) <= 300) )
		{
			m_strResolution	= dlg.m_strResolution;
			if (dlg.m_bCreatePreviews)
			{
				CFileStatus fileStatus;
				if (CFile::GetStatus(m_strFullFilename, fileStatus))
				{
					ProcessPDFPages(m_strFullFilename, NULL, TRUE);
					theApp.OnUpdateView(RUNTIME_CLASS(CPageListView),		NULL, 0, NULL);
					theApp.OnUpdateView(RUNTIME_CLASS(CPageListDetailView), NULL, 0, NULL);
				}
			}
		}
	}

	m_bCreatePreviews		= dlg.m_bCreatePreviews;
	m_bAutoAssign			= dlg.m_bAutoAssign;
	m_strDistillerSettings	= dlg.m_strDistillerSettings;
}
