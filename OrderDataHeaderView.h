#pragma once


// CODHCustomPaintFrame

class CODHCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CODHCustomPaintFrame)

public:
	CODHCustomPaintFrame();
	virtual ~CODHCustomPaintFrame();

public:

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};



// COrderDataHeaderView

class COrderDataHeaderView : public CFormView
{
	DECLARE_DYNCREATE(COrderDataHeaderView)

public:
	COrderDataHeaderView();
	virtual ~COrderDataHeaderView();

public:
	enum { IDD = IDD_ORDERDATAHEADERVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


public:
	CODHCustomPaintFrame m_jobTitleHeader;
	CFont				 m_bigFont;
	CBrush				 m_hollowBrush;


protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedOdhModifyOrderdata();
};


