// DlgSectionMarkGeometry.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"
#include "PrintSheetMarksView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSectionMarkGeometry 


CDlgSectionMarkGeometry::CDlgSectionMarkGeometry(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSectionMarkGeometry::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSectionMarkGeometry)
	m_fFootMargin = 0.0f;
	m_fHeadMargin = 0.0f;
	m_fSafetyDistance = 0.0f;
	//}}AFX_DATA_INIT

	m_pParent = NULL;
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
}

CDlgSectionMarkGeometry::~CDlgSectionMarkGeometry()
{
	m_hollowBrush.DeleteObject();
}

BOOL CDlgSectionMarkGeometry::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}	

void CDlgSectionMarkGeometry::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSectionMarkGeometry)
	DDX_Control(pDX, IDC_SECTIONMARK_HEADMARGIN_SPIN, m_headMarginSpin);
	DDX_Control(pDX, IDC_SECTIONMARK_FOOTMARGIN_SPIN, m_footMarginSpin);
	DDX_Control(pDX, IDC_SECTIONMARK_SAFETYDISTANCE_SPIN, m_safetyDistanceSpin);
	DDX_Measure(pDX, IDC_SECTIONMARK_FOOTMARGIN, m_fFootMargin);
	DDX_Measure(pDX, IDC_SECTIONMARK_HEADMARGIN, m_fHeadMargin);
	DDX_Measure(pDX, IDC_SECTIONMARK_SAFETYDISTANCE, m_fSafetyDistance);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgSectionMarkGeometry, CDialog)
	//{{AFX_MSG_MAP(CDlgSectionMarkGeometry)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SECTIONMARK_FOOTMARGIN_SPIN, OnDeltaposSectionmarkFootmarginSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SECTIONMARK_HEADMARGIN_SPIN, OnDeltaposSectionmarkHeadmarginSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SECTIONMARK_SAFETYDISTANCE_SPIN, OnDeltaposSectionmarkSafetyDistanceSpin)
	ON_BN_CLICKED(IDC_SECTIONMARK_SPINE, OnSectionMarkSpine)
	ON_BN_CLICKED(IDC_SECTIONMARK_HEAD_FOOT, OnSectionMarkHeadFoot)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgSectionMarkGeometry 

BOOL CDlgSectionMarkGeometry::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	m_headMarginSpin.SetRange(0, 1);  
	m_footMarginSpin.SetRange(0, 1);  
	m_safetyDistanceSpin.SetRange(0, 1);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgSectionMarkGeometry::OnDeltaposSectionmarkFootmarginSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_footMarginSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	if (m_pParent)
		if (m_pParent->GetParent())
			m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);

	*pResult = 0;
}

void CDlgSectionMarkGeometry::OnDeltaposSectionmarkHeadmarginSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_headMarginSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	if (m_pParent)
		if (m_pParent->GetParent())
			m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);

	*pResult = 0;
}

void CDlgSectionMarkGeometry::OnDeltaposSectionmarkSafetyDistanceSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_safetyDistanceSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	if (m_pParent)
		if (m_pParent->GetParent())
			m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);

	*pResult = 0;
}

void CDlgSectionMarkGeometry::LoadData()
{
	if ( ! m_hWnd)
		return;

	CMark* pMark = m_pParent->GetFirstMark();
	while (pMark)
	{
		if (pMark->m_nType == CMark::SectionMark)
		{
			CSectionMark* pSectionMark = (CSectionMark*)pMark;
			m_fFootMargin		= pSectionMark->m_fFootMargin;
			m_fHeadMargin		= pSectionMark->m_fHeadMargin;
			m_fSafetyDistance	= pSectionMark->m_fSafetyDistance;
			m_nPosition			= pSectionMark->m_nPosition;

			if ((HBITMAP)m_bmSectionMarkPreview)
				m_bmSectionMarkPreview.DeleteObject();
			switch (m_nPosition)
			{
			case CSectionMark::Spine:		m_bmSectionMarkPreview.LoadBitmap(IDB_SECTIONMARK_SPINE);	((CButton*)GetDlgItem(IDC_SECTIONMARK_SPINE))->SetCheck(BST_CHECKED);	break;
			case CSectionMark::Head_Foot:	m_bmSectionMarkPreview.LoadBitmap(IDB_SECTIONMARK_HEAD);	((CButton*)GetDlgItem(IDC_SECTIONMARK_HEAD))->SetCheck(BST_CHECKED);	break;
			default:						break;
			}
			((CStatic*)GetDlgItem(IDC_SECTIONMARK_PREVIEW))->SetBitmap((HBITMAP)m_bmSectionMarkPreview);
		}

		pMark = m_pParent->GetNextMark(pMark);
	}

	UpdateData(FALSE);
}

void CDlgSectionMarkGeometry::SaveData()
{
	if ( ! m_hWnd)
		return;

	UpdateData(TRUE);

	CMark* pMark = m_pParent->GetFirstMark();
	while (pMark)
	{
		if (pMark->m_nType == CMark::SectionMark)
		{
			CSectionMark* pSectionMark		= (CSectionMark*)pMark;
			pSectionMark->m_fFootMargin		= m_fFootMargin;
			pSectionMark->m_fHeadMargin		= m_fHeadMargin;
			pSectionMark->m_fSafetyDistance = m_fSafetyDistance;
			pSectionMark->m_nPosition		= m_nPosition;
		}

		pMark = m_pParent->GetNextMark(pMark);
	}
}

void CDlgSectionMarkGeometry::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}

void CDlgSectionMarkGeometry::OnSectionMarkSpine()
{
	m_nPosition = CSectionMark::Spine;
	Apply();
	LoadData();
	if (m_pParent)
		if (m_pParent->GetParent())
			m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
}

void CDlgSectionMarkGeometry::OnSectionMarkHeadFoot()
{
	m_nPosition = CSectionMark::Head_Foot;
	Apply();
	LoadData();
	if (m_pParent)
		if (m_pParent->GetParent())
			m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
}
