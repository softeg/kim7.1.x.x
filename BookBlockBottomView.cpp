// BookBlockBottomView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "BookBlockBottomView.h"
#include "BookblockFrame.h"


// CBookBlockBottomView

IMPLEMENT_DYNCREATE(CBookBlockBottomView, CFormView)

CBookBlockBottomView::CBookBlockBottomView()
	: CFormView(CBookBlockBottomView::IDD)
{

}

CBookBlockBottomView::~CBookBlockBottomView()
{
}

void CBookBlockBottomView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CBookBlockBottomView, CFormView)
	ON_BN_CLICKED(IDOK, &CBookBlockBottomView::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CBookBlockBottomView::OnBnClickedCancel)
END_MESSAGE_MAP()


// CBookBlockBottomView-Diagnose

#ifdef _DEBUG
void CBookBlockBottomView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CBookBlockBottomView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CBookBlockBottomView-Meldungshandler

void CBookBlockBottomView::OnBnClickedOk()
{
	CBookblockFrame* pFrame = (CBookblockFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	if ( ! pFrame->IsKindOf(RUNTIME_CLASS(CBookblockFrame)))
		return;
	pFrame->m_nModalResult = IDOK;
	pFrame->SendMessage(WM_CLOSE);
}

void CBookBlockBottomView::OnBnClickedCancel()
{
	CBookblockFrame* pFrame = (CBookblockFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	if ( ! pFrame->IsKindOf(RUNTIME_CLASS(CBookblockFrame)))
		return;
	pFrame->m_nModalResult = IDCANCEL;
	pFrame->SendMessage(WM_CLOSE);
}