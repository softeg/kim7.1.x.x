#if !defined(AFX_DLGLAYOUTOBJECTCONTROL_H__3552A58B_5024_49B4_8787_F4FD223F7DAB__INCLUDED_)
#define AFX_DLGLAYOUTOBJECTCONTROL_H__3552A58B_5024_49B4_8787_F4FD223F7DAB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgLayoutObjectControl.h : Header-Datei
//


//#ifndef OBJECTPROPS
#include "PrintColorList.h"
#include "PlaceholderEdit.h"
#include "DlgObjectPreviewControl.h"
#include "DlgSystemGenMarkColorControl.h"
#include "DlgSelectSectionMark.h"
#include "DlgSectionMarkGeometry.h"
#include "DlgSectionMarkContent.h"
#include "DlgCropMarkGeometry.h"
#include "DlgCropMarkContent.h"
#include "DlgFoldMarkGeometry.h"
#include "DlgFoldMarkContent.h"
#include "MarkSourceWindow.h"
#include "DlgSelectNewMark.h"
#include "MarkPivotControl.h"
#include "ObjectPivotControl.h"
#include "DlgLayoutObjectGeometry.h"
#include "DlgMarkPlacement.h"
#include "DlgObjectZOrder.h"
#include "DlgMoveObjects.h"
#include "DlgPDFObjectContent.h"
#include "DlgTextMarkObjectContent.h"
#include "DlgObjectPreviewWindow.h"
#include "DlgBarcodeMarkContent.h"
#include "DlgDrawingMarkContent.h"
#include "DlgMarkRules.h"
//#define OBJECTPROPS
//#endif


#define ID_UPDATE_SUBDIALOGS 9998
#define ID_UPDATE_MARKNAME	 9999


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgLayoutObjectControl 

class CDlgLayoutObjectControl : public CDialog
{
	DECLARE_DYNCREATE(CDlgLayoutObjectControl)

// Konstruktion
public:
	CDlgLayoutObjectControl(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgLayoutObjectControl();

// Dialogfelddaten
	//{{AFX_DATA(CDlgLayoutObjectControl)
	enum { IDD = IDD_LAYOUTOBJECT_CONTROL };
	CTabCtrl	m_subDialogTab;
	//}}AFX_DATA

	enum ObjectType {ObjectTypeError = -1, PageObject = -2};

public:
	CLayoutObject*				m_pLayoutObject;
	CPageTemplate*				m_pObjectTemplate;
	CPageSource*				m_pObjectSource;
	CPageSourceHeader*			m_pObjectSourceHeader;
	CPrintSheet*				m_pPrintSheet;
	CMark*						m_pMark;
	int							m_nObjectType;
	CString						m_strInitialTitle;
	CRect						m_tabRect;
	CDlgLayoutObjectGeometry	m_dlgLayoutObjectGeometry;
	CDlgLayoutObjectGeometry	m_dlgCustomMarkGeometry;
	CDlgLayoutObjectGeometry	m_dlgTextMarkGeometry;
	CDlgLayoutObjectGeometry	m_dlgBarcodeMarkGeometry;
	CDlgLayoutObjectGeometry	m_dlgDrawingMarkGeometry;
	CDlgSectionMarkGeometry		m_dlgSectionMarkGeometry;
	CDlgCropMarkGeometry		m_dlgCropMarkGeometry;
	CDlgFoldMarkGeometry		m_dlgFoldMarkGeometry;
	CDlgPDFObjectContent		m_dlgPageObjectContent;
	CDlgPDFObjectContent		m_dlgCustomMarkContent;
	CDlgTextMarkObjectContent	m_dlgTextMarkContent;
	CDlgSectionMarkContent		m_dlgSectionMarkContent;
	CDlgCropMarkContent			m_dlgCropMarkContent;
	CDlgFoldMarkContent			m_dlgFoldMarkContent;
	CDlgBarcodeMarkContent		m_dlgBarcodeMarkContent;
	CDlgDrawingMarkContent		m_dlgDrawingMarkContent;
	CDlgMarkPlacement			m_dlgMarkPlacement;
	CDlgObjectZOrder			m_dlgObjectZOrder;
	CDlgMarkRules				m_dlgMarkRules;
	CColorDefinitionTable*		m_pColorDefTable;
	int							m_nInitialPage;
	CSize						m_sizeViewport;
	CBrush*						m_pBkBrush;
	BOOL						m_bLockUpdateView;



// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgLayoutObjectControl)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void						Initialize(int nIDFrame, CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet, CWnd* pParent, int nInitialPage = 0);
	void						Initialize(int nIDFrame, CMark* pMark, CPrintSheet* pPrintSheet, CColorDefinitionTable* pColorDefTable, CWnd* pParent, int nInitialPage = 0);
	void						ReinitDialog(CLayoutObject* pNewLayoutObject, CPrintSheet* pNewPrintSheet, BOOL bDoUpdateView = TRUE);
	void						ReinitDialog(CMark* pNewMark, CPrintSheet* pNewPrintSheet, CColorDefinitionTable* pColorDefTable, BOOL bDoUpdateView = TRUE);
	CDlgObjectPreviewWindow*	GetObjectPreviewWindow();
	void						CreateObjectPreviewWindow();
	int							GetObjectType();
	void						DataExchangeLoad();
	void						DataExchangeSave();
	void						Apply();
	BOOL						IsActiveReflineControl();
	CLayoutObject*				GetFirstLayoutObject();
	CLayoutObject*				GetNextLayoutObject(CLayoutObject* pLayoutObject);
	CMark*						GetFirstMark();
	CMark*						GetNextMark(CMark* pMark);
	CPrintSheet*				GetPrintSheet();
	void						UpdateView();
	CColorDefinitionTable*		GetActColorDefTable();
	int							ColorDefTable_GetSize();
	CColorDefinition*			ColorDefTable_GetColorDef(int nColorDefinitionIndex);
	int							ColorDefTable_FindColorDefinitionIndex(const CString& strColorName);
	int							ColorDefTable_InsertColorDef(CColorDefinition& rColorDef);
	BOOL						IsEditMode();
	BOOL						IsOpenDlgGeometry();
	CDlgLayoutObjectGeometry*	GetDlgGeometry();
	BOOL						IsOpenDlgPDFObjectContent();
	BOOL						IsOpenDlgObjectContent();
	void						DlgHideAll();
	void						ObjectPreviewWindow_LoadData();
protected:
	void					ReinitDialog(CLayoutObject* pNewLayoutObject, CMark* pNewMark, CPrintSheet* pNewPrintSheet, CColorDefinitionTable* pColorDefTable, BOOL bDoUpdateView = TRUE);
	inline BOOL				EnableRulesTab();
	void					InitSubDialogTab(int nID1, int nID2 = -1, int nID3 = -1, int nID4 = -1);
	void					InitPageObject();
	void					LoadPageObjectData();
	void					SavePageObjectData();
	void					InitCustomMark();
	void					LoadCustomMarkData();
	void					SaveCustomMarkData();
	void					InitTextMark();
	void					LoadTextMarkData();
	void					SaveTextMarkData();
	void					InitSectionMark();
	void					LoadSectionMarkData();
	void					SaveSectionMarkData();
	void					InitCropMark();
	void					LoadCropMarkData();
	void					SaveCropMarkData();
	void					InitFoldMark();
	void					LoadFoldMarkData();
	void					SaveFoldMarkData();
	void					InitBarcodeMark();
	void					LoadBarcodeMarkData();
	void					SaveBarcodeMarkData();
	void					InitDrawingMark();
	void					LoadDrawingMarkData();
	void					SaveDrawingMarkData();
	void					OK();
	void					Cancel();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgLayoutObjectControl)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeSubdialogTab(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGLAYOUTOBJECTCONTROL_H__3552A58B_5024_49B4_8787_F4FD223F7DAB__INCLUDED_
