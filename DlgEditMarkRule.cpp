// DlgEditMarkRule.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgEditMarkRule.h"
#include "BindingDefsSelection.h"
#include "DlgSheet.h"
#include "PaperDefsSelection.h"


// CDlgEditMarkRule-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgEditMarkRule, CDialog)

CDlgEditMarkRule::CDlgEditMarkRule(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgEditMarkRule::IDD, pParent)
{
	m_nRuleSet = MarkRules;
}	

CDlgEditMarkRule::~CDlgEditMarkRule()
{
}

void CDlgEditMarkRule::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RULE_LEFTOPERAND_COMBO, m_leftOperandCombo);
	DDX_Control(pDX, IDC_RULE_OPERATOR_COMBO, m_operatorCombo);
	DDX_Control(pDX, IDC_RULE_RIGHTOPERAND_COMBO, m_rightOperandCombo);
	DDX_CBString(pDX, IDC_RULE_OPERATOR_COMBO, m_strOperator);
	DDX_CBString(pDX, IDC_RULE_RIGHTOPERAND_COMBO, m_strValue);
}


BEGIN_MESSAGE_MAP(CDlgEditMarkRule, CDialog)
	ON_CBN_SELCHANGE(IDC_RULE_LEFTOPERAND_COMBO, &CDlgEditMarkRule::OnCbnSelchangeRuleLeftoperandCombo)
END_MESSAGE_MAP()


// CDlgEditMarkRule-Meldungshandler

BOOL CDlgEditMarkRule::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitVariableCombo();
	if (m_leftOperandCombo.SelectString(0, m_strVariable) == CB_ERR)
	{
		m_leftOperandCombo.SetWindowText(m_strVariable);
		m_rightOperandCombo.SetWindowText(m_strValue);
	}
	else
		InitValueCombo();

	if (m_strOperator.IsEmpty())
		m_strOperator = _T("=");	// default

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgEditMarkRule::InitVariableCombo()
{
	switch (m_nRuleSet)
	{
	case MarkRules:					InitVariableComboMarkRules();				break;
	case ProductionProfileRules:	InitVariableComboProductionProfileRules();	break;
	}
}

void CDlgEditMarkRule::InitVariableComboMarkRules()
{
	m_leftOperandCombo.AddString(_T("NoRule"));
	m_leftOperandCombo.AddString(_T("PrintingMachine"));
	m_leftOperandCombo.AddString(_T("WorkStyle"));
	m_leftOperandCombo.AddString(_T("BindingStyle"));
	m_leftOperandCombo.AddString(_T("NumColors"));
	m_leftOperandCombo.AddString(_T("NumSpotColors"));
	m_leftOperandCombo.AddString(_T("SheetFormat"));
	m_leftOperandCombo.AddString(_T("SheetSide"));
	m_leftOperandCombo.AddString(_T("SheetWidth"));
	m_leftOperandCombo.AddString(_T("SheetHeight"));
	m_leftOperandCombo.AddString(_T("PaperName"));
	m_leftOperandCombo.AddString(_T("PaperType"));
	m_leftOperandCombo.AddString(_T("Color"));
	m_leftOperandCombo.AddString(_T("DeviceID_PrintingMachine"));
	m_leftOperandCombo.AddString(_T("ProductType"));
	m_leftOperandCombo.AddString(_T("ProductPartName"));
	m_leftOperandCombo.AddString(_T("PageFormat"));
	m_leftOperandCombo.AddString(_T("PageNum@Product"));
	m_leftOperandCombo.AddString(_T("PageNum@ProductPart"));
	m_leftOperandCombo.AddString(_T("PageNum@FoldSheet"));
	m_leftOperandCombo.AddString(_T("FoldSheetSide"));
}

void CDlgEditMarkRule::InitVariableComboProductionProfileRules()
{
	m_leftOperandCombo.AddString(_T("ProductType"));
	m_leftOperandCombo.AddString(_T("ProductPartName"));
	m_leftOperandCombo.AddString(_T("BindingStyle"));
	m_leftOperandCombo.AddString(_T("NumColors"));
	m_leftOperandCombo.AddString(_T("NumSpotColors"));
	m_leftOperandCombo.AddString(_T("PaperName"));
	m_leftOperandCombo.AddString(_T("PaperType"));
	m_leftOperandCombo.AddString(_T("Color"));
}

void CDlgEditMarkRule::InitValueCombo()
{
	switch (m_nRuleSet)
	{
	case MarkRules:					InitValueComboMarkRules();				break;
	case ProductionProfileRules:	InitValueComboProductionProfileRules();	break;
	}
}

void CDlgEditMarkRule::InitValueComboMarkRules()
{
	m_operatorCombo.ShowWindow(SW_SHOW);
	m_rightOperandCombo.ShowWindow(SW_SHOW);
	m_rightOperandCombo.ResetContent();

	if (m_strVariable == _T("NoRule"))
	{
		m_operatorCombo.ShowWindow(SW_HIDE);
		m_rightOperandCombo.ShowWindow(SW_HIDE);
	}
	else
	if (m_strVariable == _T("PrintingMachine"))
	{
		POSITION pos = theApp.m_PressDeviceList.GetHeadPosition();
		while (pos) 
			m_rightOperandCombo.AddString(theApp.m_PressDeviceList.GetNext(pos).m_strName);

		if (m_rightOperandCombo.FindStringExact(-1, m_strValue) == CB_ERR)
			m_rightOperandCombo.AddString(m_strValue);
	}
	else
	if (m_strVariable == _T("WorkStyle"))
	{
		CString string;
		string.LoadString(IDS_WORK_AND_BACK);
		m_rightOperandCombo.AddString(string);
		m_rightOperandCombo.AddString(theApp.settings.m_szWorkAndTurnName);	
		m_rightOperandCombo.AddString(theApp.settings.m_szWorkAndTumbleName);
		m_rightOperandCombo.AddString(theApp.settings.m_szSingleSidedName);
		m_rightOperandCombo.AddString(string);
	}
	else
	if (m_strVariable == _T("BindingStyle"))
	{
		CBindingDefsSelection bindingDefs;
		bindingDefs.LoadData();
		POSITION pos = bindingDefs.m_bindingDefsList.GetHeadPosition();
		while (pos)
		{
			m_rightOperandCombo.AddString(bindingDefs.m_bindingDefsList.GetNext(pos).m_strName);
		}
		if (m_rightOperandCombo.FindStringExact(-1, m_strValue) == CB_ERR)
			m_rightOperandCombo.AddString(m_strValue);
	}
	else
	if (m_strVariable == _T("NumColors"))
	{
		for (int i = 0; i < 6; i++)
			m_rightOperandCombo.AddString(Int2Ascii(i + 1));
		m_rightOperandCombo.SetCurSel(3);
	}
	else
	if (m_strVariable == _T("NumSpotColors"))
	{
		for (int i = 0; i <= 6; i++)
			m_rightOperandCombo.AddString(Int2Ascii(i));
		m_rightOperandCombo.SetCurSel(1);
	}
	else
	if (m_strVariable == _T("SheetFormat"))
	{
		CDlgSheet dlgSheet;
		dlgSheet.LoadSheets();
		POSITION pos = dlgSheet.m_sheetList.GetHeadPosition();
		while (pos) 
		{
			CSheet& rSheet = dlgSheet.m_sheetList.GetNext(pos);
			m_rightOperandCombo.AddString(rSheet.m_strSheetName);
		}
	}
	else
	if (m_strVariable == _T("SheetSide"))
	{
		m_rightOperandCombo.AddString(theApp.settings.m_szFrontName);
		m_rightOperandCombo.AddString(theApp.settings.m_szBackName);
		m_rightOperandCombo.SetCurSel(0);
	}
	else
	if (m_strVariable == _T("SheetWidth"))
	{
	}
	else
	if (m_strVariable == _T("SheetHeight"))
	{
	}
	else
	if (m_strVariable == _T("PaperName"))
	{
		POSITION pos = theApp.m_paperDefsList.GetHeadPosition();
		while (pos)
			m_rightOperandCombo.AddString(theApp.m_paperDefsList.GetNext(pos).m_strName);

		if (m_rightOperandCombo.FindStringExact(-1, m_strValue) == CB_ERR)
			m_rightOperandCombo.AddString(m_strValue);
	}
	else
	if (m_strVariable == _T("PaperType"))
	{
	}
	else
	if (m_strVariable == _T("Color"))
	{
		CColorDefinitionTable colorDefTable; colorDefTable.Load();
		for (int i = 0; i < colorDefTable.GetSize(); i++)
			m_rightOperandCombo.AddString(colorDefTable[i].m_strColorName);

		if (m_rightOperandCombo.FindStringExact(-1, m_strValue) == CB_ERR)
			m_rightOperandCombo.AddString(m_strValue);
	}
	else
	if (m_strVariable == _T("DeviceID_PrintingMachine"))
	{
		POSITION pos = theApp.m_PressDeviceList.GetHeadPosition();
		while (pos) 
		{
			CString strDeviceID = theApp.m_PressDeviceList.GetNext(pos).m_strDeviceID;
			if ( ! strDeviceID.IsEmpty())
				m_rightOperandCombo.AddString(strDeviceID);
		}
		if (m_rightOperandCombo.FindStringExact(-1, m_strValue) == CB_ERR)
			m_rightOperandCombo.AddString(m_strValue);
	}
	else
	if (m_strVariable == _T("ProductType"))
	{
		m_rightOperandCombo.AddString(_T("Bound"));
		m_rightOperandCombo.AddString(_T("Brochure"));
		m_rightOperandCombo.AddString(_T("Flyer"));
		m_rightOperandCombo.AddString(_T("Label"));
		m_rightOperandCombo.SetCurSel(0);
	}
	else
	if (m_strVariable == _T("ProductPartName"))
	{
		switch (theApp.settings.m_iLanguage)
		{
		case 0:	m_rightOperandCombo.AddString(_T("Umschlag"));
				m_rightOperandCombo.AddString(_T("Inhalt"));
				m_rightOperandCombo.AddString(_T("Beihefter"));
				break;
		case 1:	m_rightOperandCombo.AddString(_T("Cover"));
				m_rightOperandCombo.AddString(_T("Body"));
				m_rightOperandCombo.AddString(_T("Insert"));
				break;
		}
	}
	else
	if (m_strVariable == _T("PageFormat"))
	{
		CPageFormatSelection pageFormats;
		pageFormats.LoadData();
		POSITION pos = pageFormats.m_PageFormatList.GetHeadPosition();
		while (pos) 
		{
			CPageFormat& rPageFormat = pageFormats.m_PageFormatList.GetNext(pos);
			m_rightOperandCombo.AddString(rPageFormat.m_szName);
		}
		if (m_rightOperandCombo.FindStringExact(-1, m_strValue) == CB_ERR)
			m_rightOperandCombo.AddString(m_strValue);
	}
	else
	if ( (m_strVariable == _T("PageNum@Product")) ||  (m_strVariable == _T("PageNum@ProductPart")) ||  (m_strVariable == _T("PageNum@FoldSheet")) )
	{
		m_rightOperandCombo.AddString(_T("First"));
		m_rightOperandCombo.AddString(_T("Last"));
		m_rightOperandCombo.SetCurSel(0);
	}
	else
	if ( (m_strVariable == _T("FoldSheetSide")) )
	{
		m_rightOperandCombo.AddString(_T("Front"));
		m_rightOperandCombo.AddString(_T("Back"));
		m_rightOperandCombo.SetCurSel(0);
	}
}

void CDlgEditMarkRule::InitValueComboProductionProfileRules()
{
	m_operatorCombo.ShowWindow(SW_SHOW);
	m_rightOperandCombo.ShowWindow(SW_SHOW);
	m_rightOperandCombo.ResetContent();

	if (m_strVariable == _T("ProductType"))
	{
		m_rightOperandCombo.AddString(_T("Bound"));
		m_rightOperandCombo.AddString(_T("Label"));
		m_rightOperandCombo.AddString(_T("Punched Product"));
		m_rightOperandCombo.SetCurSel(0);
	}
	else
	if (m_strVariable == _T("ProductPartName"))
	{
		switch (theApp.settings.m_iLanguage)
		{
		case 0:	m_rightOperandCombo.AddString(_T("Umschlag"));
				m_rightOperandCombo.AddString(_T("Inhalt"));
				m_rightOperandCombo.AddString(_T("Beihefter"));
				break;
		case 1:	m_rightOperandCombo.AddString(_T("Cover"));
				m_rightOperandCombo.AddString(_T("Body"));
				m_rightOperandCombo.AddString(_T("Insert"));
				break;
		}
	}
	else
	if (m_strVariable == _T("BindingStyle"))
	{
		CBindingDefsSelection bindingDefs;
		bindingDefs.LoadData();
		POSITION pos = bindingDefs.m_bindingDefsList.GetHeadPosition();
		while (pos)
		{
			m_rightOperandCombo.AddString(bindingDefs.m_bindingDefsList.GetNext(pos).m_strName);
		}
		if (m_rightOperandCombo.FindStringExact(-1, m_strValue) == CB_ERR)
			m_rightOperandCombo.AddString(m_strValue);
	}
	else
	if (m_strVariable == _T("NumColors"))
	{
		for (int i = 0; i < 6; i++)
			m_rightOperandCombo.AddString(Int2Ascii(i + 1));
		m_rightOperandCombo.SetCurSel(3);
	}
	else
	if (m_strVariable == _T("NumSpotColors"))
	{
		for (int i = 0; i < 6; i++)
			m_rightOperandCombo.AddString(Int2Ascii(i + 1));
		m_rightOperandCombo.SetCurSel(0);
	}
	else
	if (m_strVariable == _T("PaperName"))
	{
		POSITION pos = theApp.m_paperDefsList.GetHeadPosition();
		while (pos)
			m_rightOperandCombo.AddString(theApp.m_paperDefsList.GetNext(pos).m_strName);

		if (m_rightOperandCombo.FindStringExact(-1, m_strValue) == CB_ERR)
			m_rightOperandCombo.AddString(m_strValue);
	}
	else
	if (m_strVariable == _T("PaperType"))
	{
	}
	else
	if (m_strVariable == _T("Color"))
	{
		CColorDefinitionTable colorDefTable; colorDefTable.Load();
		for (int i = 0; i < colorDefTable.GetSize(); i++)
			m_rightOperandCombo.AddString(colorDefTable[i].m_strColorName);

		if (m_rightOperandCombo.FindStringExact(-1, m_strValue) == CB_ERR)
			m_rightOperandCombo.AddString(m_strValue);
	}
}

void CDlgEditMarkRule::OnCbnSelchangeRuleLeftoperandCombo()
{
	int nSel = m_leftOperandCombo.GetCurSel();
	m_leftOperandCombo.GetLBText(nSel, m_strVariable);
	InitValueCombo();
}
