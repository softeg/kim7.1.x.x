#pragma once


// CPPBCustomPaintFrame

class CPBPCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CPBPCustomPaintFrame)

public:
	CPBPCustomPaintFrame();
	virtual ~CPBPCustomPaintFrame();

public:
	void DrawPages(CDC* pDC, CRect frameRect);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};


// CDlgProductionBindingParams-Dialogfeld

class CDlgProductionBindingParams : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionBindingParams)

public:
	CDlgProductionBindingParams(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionBindingParams();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTION_BINDINGPARAMS };

public:
	CPoint				 m_ptInitialPos;
	BOOL				 m_bLocked;
	CPBPCustomPaintFrame m_pagesPreviewFrame;
	int					 m_nPagePreviewIndex;

public:
	CPrepressParams&	GetPrepressParams();
	CBindingProcess&	GetBindingProcess();
	CFoldingProcess&	GetFoldingProcess();
	CBoundProductPart&	GetBoundProductPart();
	void 				InitData();
	void 				SaveData();
	void				UpdateDetailsString();
	void 				UpdatePreview();


protected:
	CEdit			m_spineAreaEdit;
	CSpinButtonCtrl	m_spineAreaSpin;
	BOOL			m_bSpineAreaAuto;
	CEdit			m_millingDepthEdit;
	CSpinButtonCtrl	m_millingDepthSpin;
	CSpinButtonCtrl	m_overFoldSpin;
	float	m_fSpineArea;
	float	m_fMillingDepth;
	float	m_fOverfold;
	BOOL	m_bOverfoldFront;
	BOOL	m_bOverfoldRear;
	CString m_strDetails;


public:
	virtual BOOL Create(UINT nIDTemplate, CWnd* pParentWnd = NULL);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedOk();
	afx_msg void OnDeltaposSpineAreaSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposMillingDepthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposOverfoldSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusSpineAreaEdit();
	afx_msg void OnKillfocusMillingDepthEdit();
	afx_msg void OnKillfocusOverfoldEdit();
	afx_msg void OnOverfoldFront();
	afx_msg void OnOverfoldRear();
	afx_msg void OnBnClickedPdDetailsButton();
	afx_msg void OnDeltaposPdPagespreviewSpin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedPdSpineareaAuto();
};
