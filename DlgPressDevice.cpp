// DlgPressDevice.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgPressDevice.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "ThemedTabCtrl.h"
#include "PrintSheetViewStrippingData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// CPDPreviewFrame

IMPLEMENT_DYNAMIC(CPDPreviewFrame, CStatic)

CPDPreviewFrame::CPDPreviewFrame()
{
}

CPDPreviewFrame::~CPDPreviewFrame()
{
}


BEGIN_MESSAGE_MAP(CPDPreviewFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()

// CPDPreviewFrame-Meldungshandler

void CPDPreviewFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rcFrame;
	GetClientRect(rcFrame);
	dc.FillSolidRect(rcFrame, LIGHTGRAY);//RGB(144, 153, 174));

	CDlgPressDevice* pParentDlg = (CDlgPressDevice*)GetParent();
	float fPaperWidth  = (pParentDlg->GetActLayout()) ? pParentDlg->GetActLayout()->m_FrontSide.m_fPaperWidth  : 0.0f;
	float fPaperHeight = (pParentDlg->GetActLayout()) ? pParentDlg->GetActLayout()->m_FrontSide.m_fPaperHeight : 0.0f;

	CPressDevice pressDevice;
	pressDevice.Create();

	pressDevice.m_strName		= pParentDlg->m_strName;
	pressDevice.m_strDeviceID	= pParentDlg->m_strDeviceID;
	pressDevice.m_fPlateWidth	= pParentDlg->m_fWidth;
	pressDevice.m_fPlateHeight	= pParentDlg->m_fHeight;
	pressDevice.m_strNotes		= pParentDlg->m_strNotes;

	CButton* pBut = (CButton*)pParentDlg->GetDlgItem (IDC_SHEET);
	if (pBut->GetCheck() == 1)	
	{
		pressDevice.m_nPressType			= CPressDevice::SheetPress;
		pressDevice.m_fPlateEdgeToPrintEdge	= pParentDlg->m_fPlateEdge;
		pressDevice.m_fGripper				= pParentDlg->m_fGripper;
		pressDevice.m_fXRefLine				= 0.0f;
		pressDevice.m_fYRefLine1			= 0.0f;
		pressDevice.m_fYRefLine2			= 0.0f;
	}
	else						
	{
		pressDevice.m_fPlateEdgeToPrintEdge	= 0.0f;
		pressDevice.m_fGripper				= 0.0f;
		pressDevice.m_nPressType			= CPressDevice::WebPress;
		pressDevice.m_fXRefLine				= pParentDlg->m_fPlateEdge;
		pressDevice.m_fYRefLine1			= pParentDlg->m_fGripper;
		pressDevice.m_fYRefLine2			= pParentDlg->m_fYRefLine2;
	}

	pressDevice.DrawPlate(&dc, rcFrame, fPaperWidth, fPaperHeight);			
}




/////////////////////////////////////////////////////////////////////////////
// CDlgPressDevice dialog


CDlgPressDevice::CDlgPressDevice(BOOL bModifyDirect /*=TRUE*/, int nActSight /*= -1*/, CPressDevice* pActPressDevice /*= NULL*/, CLayout* pActLayout /*= NULL*/, CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPressDevice::IDD, pParent)
	, m_bShow2YRefLines(FALSE)
{
	//{{AFX_DATA_INIT(CDlgPressDevice)
	m_fGripper = 0.0f;
	m_fHeight = 0.0f;
	m_fWidth = 0.0f;
	m_fPlateEdge = 0.0f;
	m_strNotes = _T("");
	m_strName = _T("");
	m_fSheetEdge = 0.0f;
	m_nActSight			= nActSight;
	m_pActPressDevice	= pActPressDevice;
	m_pActLayout		= pActLayout;
	m_bModified = FALSE;
	m_bEditChanged = FALSE;
	m_bModifyDirect = bModifyDirect;
	m_nPressDeviceIndex = -1;
	m_strDeviceID.Empty();
	m_fYRefLine1 = 0.0f;
	m_fYRefLine2 = 0.0f;
	m_bShow2YRefLines = FALSE;
	//}}AFX_DATA_INIT
}


void CDlgPressDevice::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CDlgPressDevice)
	CDialog::DoDataExchange(pDX);
	DDX_Measure(pDX, IDC_GRIPPER, m_fGripper);
	DDX_Measure(pDX, IDC_PLATE_HEIGHT, m_fHeight);
	DDX_Measure(pDX, IDC_PLATE_WIDTH, m_fWidth);
	DDX_Measure(pDX, IDC_PLATEEDGE, m_fPlateEdge);
	DDX_Text(pDX, IDC_NOTES, m_strNotes);
	DDV_MaxChars(pDX, m_strNotes, 1000);
	DDX_CBString(pDX, IDC_PRESSDEVICE_LIST, m_strName);
	DDX_Measure(pDX, IDC_SHEETEDGE, m_fSheetEdge);
	DDX_Text(pDX, IDC_PRESSDEVICE_ID_EDIT, m_strDeviceID);
	DDX_Measure(pDX, IDC_Y_REFLINE_2, m_fYRefLine2);
	DDX_Check(pDX, IDC_SHOW_2Y_REFLINES, m_bShow2YRefLines);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPressDevice, CDialog)
	//{{AFX_MSG_MAP(CDlgPressDevice)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_SHEET, OnSheet)
	ON_BN_CLICKED(IDC_WEB, OnWeb)
	ON_LBN_SELCHANGE(IDC_PRESSDEVICE_LIST, OnSelchangePressDeviceList)
	ON_CBN_EDITCHANGE(IDC_PRESSDEVICE_LIST, OnEditchangePressDeviceList)
	ON_BN_CLICKED(IDC_CHANGE, OnChange)
	ON_EN_CHANGE(IDC_SHEETEDGE, OnChangeSheetedge)
	ON_EN_CHANGE(IDC_PLATEEDGE, OnChangePlateedge)
	ON_EN_CHANGE(IDC_GRIPPER, OnChangeGripper)
	ON_EN_CHANGE(IDC_PLATE_HEIGHT, OnChangePlateHeight)
	ON_EN_CHANGE(IDC_PLATE_WIDTH, OnChangePlateWidth)
	ON_EN_KILLFOCUS(IDC_SHEETEDGE, OnKillfocusSheetedge)
	ON_EN_KILLFOCUS(IDC_PLATEEDGE, OnKillfocusPlateedge)
	ON_EN_KILLFOCUS(IDC_GRIPPER, OnKillfocusGripper)
	ON_EN_KILLFOCUS(IDC_PLATE_HEIGHT, OnKillfocusPlateHeight)
	ON_EN_KILLFOCUS(IDC_PLATE_WIDTH, OnKillfocusPlateWidth)
	ON_EN_KILLFOCUS(IDC_Y_REFLINE_2, OnKillfocusYRefLine2)
	ON_BN_CLICKED(IDC_SHOW_2Y_REFLINES, &CDlgPressDevice::OnBnClickedYReflines)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPressDevice message handlers

BOOL CDlgPressDevice::OnInitDialog() 
{
	CPressDevice PressDeviceBuf;
	UINT numberDevices;
	POSITION pos;
	CString s;
	CButton *pBut;
	int nIndex;

	CDialog::OnInitDialog();

	m_pressDevicePreviewFrame.SubclassDlgItem(IDC_PRESSDEVICE_PREVIEWFRAME,this);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);

	if ( ! (theApp.m_PressDeviceList.IsEmpty()))
	{
		numberDevices = theApp.m_PressDeviceList.GetCount();
		pos = theApp.m_PressDeviceList.GetHeadPosition();
		for (UINT i = 0; i < numberDevices; i ++)                        
		{
			PressDeviceBuf = theApp.m_PressDeviceList.GetAt(pos);
			pBox->AddString(PressDeviceBuf.m_strName);
			theApp.m_PressDeviceList.GetNext(pos);
		}

		if (pDoc)
		{
			if (pDoc->MoreThanOnePressdevice())
			{
				CString string;
				string.LoadString(IDS_MULTIPLE_PRESSDEVICES_TEXT);
				SetWindowText(string);
				UpdateData(FALSE);      
				return TRUE;
			}
		}

		if (pDoc)
		{
			switch(GetActSight())
			{
			case BOTHSIDES :
			case FRONTSIDE :
			case BACKSIDE  : nIndex = pBox->FindStringExact(-1, ((GetActPressDevice())) ? (GetActPressDevice())->m_strName : "");
							 if (nIndex != CB_ERR)
							 {
								pBox->SetCurSel(nIndex);
								PressDeviceBuf = *theApp.m_PressDeviceList.GetDevice(((GetActPressDevice())) ? (GetActPressDevice())->m_strName : "");
							 }
							 else
							 {
								pBox->SetCurSel(-1);
								if (GetActPressDevice())
									PressDeviceBuf = *(GetActPressDevice());
							 }
							 break;
			case ALLSIDES  : /*pos = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(pDoc->m_nDefaultPressDeviceIndex);
							 if (pos)
							 {
								 pos = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(pDoc->m_nDefaultPressDeviceIndex);
								 if (pos)
									PressDeviceBuf = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
								 nIndex = pBox->FindStringExact(-1, PressDeviceBuf.m_strName);
								 if (nIndex != CB_ERR)
								 {
									pBox->SetCurSel(nIndex);
									PressDeviceBuf = *theApp.m_PressDeviceList.GetDevice(PressDeviceBuf.m_strName);
								 }
								 else
								 {
									pBox->SetCurSel(-1);
									pos = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(pDoc->m_nDefaultPressDeviceIndex);
									if (pos)
										PressDeviceBuf = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
								 }
							 }*/
							 break;
			default :		 //TODO  Errormessage
							 break;
			}
		}
	}
	else
	{
		if (pDoc)
		{
			switch(GetActSight())
			{
			case BOTHSIDES :
			case FRONTSIDE : 
			case BACKSIDE  : if (GetActPressDevice())
								PressDeviceBuf = *(GetActPressDevice());
							 break;
			case ALLSIDES  : /*pos = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(pDoc->m_nDefaultPressDeviceIndex);
							 if (pos)
								PressDeviceBuf = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
							 */break; 
			default :		 //TODO  Errormessage
							 break;
			}
		}
		else
		{	// No document open and no "PressDevice.def"
			PressDeviceBuf.m_strName			   = "Leermaschine";
			PressDeviceBuf.m_fPlateWidth		   = 1030.0f;
			PressDeviceBuf.m_fPlateHeight		   =  880.0f;
			PressDeviceBuf.m_strNotes			   = "Ohne abgespeicherte Druckmaschine kann kein Job erstellt werden!";
			PressDeviceBuf.m_nPressType			   = CPressDevice::SheetPress;
			PressDeviceBuf.m_fPlateEdgeToPrintEdge = 43.0f;
			PressDeviceBuf.m_fGripper			   = 10.0f;
		}
	}

	m_strName     = PressDeviceBuf.m_strName;
	m_strDeviceID = PressDeviceBuf.m_strDeviceID;
	m_fWidth      = PressDeviceBuf.m_fPlateWidth;
	m_fHeight     = PressDeviceBuf.m_fPlateHeight;
	m_strNotes	  = PressDeviceBuf.m_strNotes;
	if (PressDeviceBuf.m_nPressType == CPressDevice::SheetPress)
	{
		m_fSheetEdge = PressDeviceBuf.m_fPlateEdgeToPrintEdge - PressDeviceBuf.m_fGripper;
		m_fPlateEdge = PressDeviceBuf.m_fPlateEdgeToPrintEdge;
		m_fGripper   = PressDeviceBuf.m_fGripper;
		s.LoadString(IDS_PLATEEDGE_SHEETTEXT);
		SetDlgItemText(IDC_PLATEEDGE_TEXT, s);
		s.LoadString(IDS_GRIPPER_SHEETTEXT);
		SetDlgItemText(IDC_GRIPPER_TEXT, s);
		pBut = (CButton*)GetDlgItem (IDC_SHEET);
		pBut->SetCheck(1);
		pBut = (CButton*)GetDlgItem (IDC_WEB);
		pBut->SetCheck(0);
		m_bShow2YRefLines = FALSE;
		GetDlgItem(IDC_SHOW_2Y_REFLINES)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_Y_REFLINE_2)->ShowWindow(SW_HIDE);
	}
	else	// WebPress
	{
		GetDlgItem(IDC_SHEETEDGE_TEXT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SHEETEDGE     )->ShowWindow(SW_HIDE);

		m_fSheetEdge = 0.0f;
		m_fPlateEdge = PressDeviceBuf.m_fXRefLine;
		m_fGripper   = PressDeviceBuf.m_fYRefLine1;
		m_fYRefLine2 = PressDeviceBuf.m_fYRefLine2;
		s.LoadString(IDS_PLATEEDGE_WEBTEXT);
		SetDlgItemText(IDC_PLATEEDGE_TEXT, s);
		s.LoadString(IDS_GRIPPER_WEBTEXT);
		SetDlgItemText(IDC_GRIPPER_TEXT, s);
		pBut = (CButton*)GetDlgItem (IDC_WEB);
		pBut->SetCheck(1);
		pBut = (CButton*)GetDlgItem (IDC_SHEET);
		pBut->SetCheck(0);
		m_bShow2YRefLines = PressDeviceBuf.Has2YRefLines();
		GetDlgItem(IDC_SHOW_2Y_REFLINES)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_Y_REFLINE_2)->ShowWindow((PressDeviceBuf.Has2YRefLines())  ? SW_SHOW : SW_HIDE);
	}

	UpdateData(FALSE);      
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CDlgPressDevice::OnLoadingData()
{
	TCHAR			pszFileName[MAX_PATH], pszOldFileName[MAX_PATH];
	CFile			PressDeviceDefFile;
	CFileException	fileException;

	_tcscpy_s(pszOldFileName, MAX_PATH, theApp.GetDataFolder()); // TODO: Verify the path
	_tcscpy_s(pszFileName,	  MAX_PATH, theApp.GetDataFolder()); // TODO: Remove this construct when old versions (< 3.0.1.0) are updated
	_tcscat_s(pszOldFileName, MAX_PATH, _T("\\PressDevice.def"));
	_tcscat_s(pszFileName,	  MAX_PATH, _T("\\PressDevices.def"));

	CFileStatus fileStatus;
	if (CFile::GetStatus(pszOldFileName, fileStatus))
		MoveFile(pszOldFileName, pszFileName);

	if (!PressDeviceDefFile.Open(pszFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&PressDeviceDefFile, CArchive::load);
		theApp.m_PressDeviceList.RemoveAll();

		theApp.m_PressDeviceList.Serialize(archive);
		archive.Close();
		PressDeviceDefFile.Close();
		if (theApp.m_PressDeviceList.IsEmpty()) 
			return FALSE; 
		else
			return TRUE;  	
	}
}

BOOL CDlgPressDevice::OnUpdateData()
{
	TCHAR pszFileName[MAX_PATH];
	CFile PressDeviceDefFile;
	CFileException fileException;

	_tcscpy_s(pszFileName, MAX_PATH, theApp.GetDataFolder()); // TODO: Verify the path
	_tcscat_s(pszFileName, MAX_PATH, _T("\\PressDevices.def"));

	if (!PressDeviceDefFile.Open(pszFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		UpdateData(TRUE);

		CArchive archive(&PressDeviceDefFile, CArchive::store);
		theApp.m_PressDeviceList.Serialize(archive);
		archive.Close();
		PressDeviceDefFile.Close();

		return TRUE;
	}
}

void CDlgPressDevice::OnDel() 
{
	UINT nIndex;
	POSITION pos;

	if (theApp.m_PressDeviceList.IsEmpty()) 
		return;
	if (AfxMessageBox(IDS_DEL_LISTENTRY, MB_YESNO) == IDYES)	//TODO: 0=No help available
	{
		CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);
		nIndex = pBox->GetCurSel();
		CString strName;
		pBox->GetLBText(nIndex, strName);
		pos = theApp.m_PressDeviceList.GetDevicePos(strName);
		theApp.m_PressDeviceList.RemoveAt(pos);
		pBox->DeleteString(nIndex);
		if ( ! (theApp.m_PressDeviceList.IsEmpty())) 
		{
			pBox->SetCurSel(0);
			OnSelchangePressDeviceList();	
		}
		else
		{
			CString  s;
			CButton* pBut;

			m_strName	  = _T("");
			m_strDeviceID = _T("");
			m_fHeight	  = 0.0f;
			m_fWidth	  = 0.0f;
			m_fSheetEdge  = 0.0f;
			m_fPlateEdge  = 0.0f;
			m_fGripper    = 0.0f;
			m_strNotes	  = _T("");
			m_fYRefLine1  = 0.0f;
			m_fYRefLine2  = 0.0f;
			s.LoadString(IDS_PLATEEDGE_SHEETTEXT);
			SetDlgItemText(IDC_PLATEEDGE_TEXT, s);
			s.LoadString(IDS_GRIPPER_SHEETTEXT);
			SetDlgItemText(IDC_GRIPPER_TEXT, s);
			pBut = (CButton*)GetDlgItem (IDC_SHEET);
			pBut->SetCheck(1);
			pBut = (CButton*)GetDlgItem (IDC_WEB);
			pBut->SetCheck(0);
			UpdateData(FALSE);      
		}
		if ( ! OnUpdateData())
		{
			AfxMessageBox(IDS_DEL_NOTOK, MB_OK);
			OnLoadingData();
			UpdateData(FALSE);      
		}
		m_bModified = TRUE;
		m_pressDevicePreviewFrame.Invalidate();
		m_pressDevicePreviewFrame.UpdateWindow();
	}
}

void CDlgPressDevice::OnNew() 
{
	UpdateData(TRUE);

	CString strUserDefined; 
	strUserDefined.LoadString(IDS_USERDEFINED);
	int nRet = (m_strName != strUserDefined) ? AfxMessageBox(IDS_NEW_LISTENTRY, MB_YESNO) : IDYES;
	if (nRet == IDYES)	
	{
		CPressDevice  PressDeviceBuf, PressDeviceBufApp;
		BOOL DoubleEntry = FALSE; 

		PressDeviceBuf.m_strName	  = m_strName;
		PressDeviceBuf.m_strDeviceID  = m_strDeviceID;
		PressDeviceBuf.m_fPlateWidth  = m_fWidth;
		PressDeviceBuf.m_fPlateHeight = m_fHeight;
		PressDeviceBuf.m_strNotes	  = m_strNotes;

		CButton* pBut = (CButton*)GetDlgItem (IDC_SHEET);
		if (pBut->GetCheck() == 1)	
		{
			PressDeviceBuf.m_nPressType				= CPressDevice::SheetPress;
			PressDeviceBuf.m_fPlateEdgeToPrintEdge	= m_fPlateEdge;
			PressDeviceBuf.m_fGripper				= m_fGripper;
			PressDeviceBuf.m_fXRefLine				= 0.0f;
			PressDeviceBuf.m_fYRefLine1				= 0.0f;
			PressDeviceBuf.m_fYRefLine2				= 0.0f;
		}
		else						
		{
			PressDeviceBuf.m_fPlateEdgeToPrintEdge	= 0.0f;
			PressDeviceBuf.m_fGripper				= 0.0f;
			PressDeviceBuf.m_nPressType				= CPressDevice::WebPress;
			PressDeviceBuf.m_fXRefLine				= m_fPlateEdge;
			PressDeviceBuf.m_fYRefLine1				= m_fGripper;
			PressDeviceBuf.m_fYRefLine2				= m_fYRefLine2;
		}

		int numberDevices = theApp.m_PressDeviceList.GetCount();
		POSITION pos  = theApp.m_PressDeviceList.GetHeadPosition();

		for (int i = 0; i < numberDevices; i ++)                        
		{
			PressDeviceBufApp = theApp.m_PressDeviceList.GetAt(pos);

			if (PressDeviceBuf.m_strName == PressDeviceBufApp.m_strName)
				DoubleEntry = TRUE;
			//else
			//	if ( ! PressDeviceBuf.m_strDeviceID.IsEmpty())
			//		if (PressDeviceBuf.m_strDeviceID == PressDeviceBufApp.m_strDeviceID)
			//			DoubleEntry = TRUE;
			theApp.m_PressDeviceList.GetNext(pos);
		}

		if ( ! DoubleEntry)
		{
			theApp.m_PressDeviceList.AddTail(PressDeviceBuf);
			if ( ! OnUpdateData())
				AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

			OnLoadingData(); //reload data
			CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);
			pBox->AddString(PressDeviceBuf.m_strName);
			pBox->SetCurSel(pBox->FindStringExact(-1, PressDeviceBuf.m_strName));
			OnSelchangePressDeviceList();	

			m_bModified = TRUE;
			m_pressDevicePreviewFrame.Invalidate();
			m_pressDevicePreviewFrame.UpdateWindow();
		}
		else // Name already exists
			if ((m_strName == strUserDefined))
				OnChange();
			else
				AfxMessageBox(IDS_DOUBLEENTRY_NOTOK, MB_OK);
	}
}

void CDlgPressDevice::OnOK() 
{
	if (m_bModified)	
	{
		if ( ! theApp.m_PressDeviceList.GetDevice(m_strName))
			OnNew();
		else
			OnChange();

		if (m_bModifyDirect)
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			if ( ! pDoc)
			{
				CDialog::OnOK();
				return;
			}
			if ( ! pDoc->m_PrintSheetList.m_Layouts.GetCount())
			{
				CDialog::OnOK();
				return;
			}

			CComboBox *pBox = (CComboBox*)GetDlgItem(IDC_PRESSDEVICE_LIST);
			int		 nIndex = pBox->GetCurSel();
			if (nIndex != CB_ERR) // CB_ERR == Nothing selected
			{
				CString strName;
				pBox->GetLBText(nIndex, strName);
				nIndex = theApp.m_PressDeviceList.GetDeviceIndex(strName);

				CString strMessage;
				int		nSide = GetActSight();
				switch (nSide)
				{
				case FRONTSIDE:	
				case BACKSIDE:	nSide = BOTHSIDES;	break;
				default:							break;
				}

				//switch (nSide)
				//{
				//case FRONTSIDE:	AfxFormatString1(strMessage, IDS_REALLY_SET_PRESSDEVICE_TEXT, (GetActLayout())->m_FrontSide.m_strSideName);
				//				if (AfxMessageBox(strMessage, MB_YESNO) == IDNO)
				//					return;
				//				break;
				//case BACKSIDE:	AfxFormatString1(strMessage, IDS_REALLY_SET_PRESSDEVICE_TEXT, (GetActLayout())->m_BackSide.m_strSideName);
				//				if (AfxMessageBox(strMessage, MB_YESNO) == IDNO)
				//					return;
				//				break;
				//}

				CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
				if (pView)
					pView->UndoSave();

				SetPressDevice(nSide, nIndex);
			}
		}
		else
		{
			UpdateData(TRUE);

			CComboBox *pBox = (CComboBox*)GetDlgItem(IDC_PRESSDEVICE_LIST);
			int nIndex = pBox->GetCurSel();
			CString strName;
			pBox->GetLBText(nIndex, strName);
			m_nPressDeviceIndex = theApp.m_PressDeviceList.GetDeviceIndex(strName);

			m_pressDevice.m_strName		 = m_strName;
			m_pressDevice.m_strDeviceID	 = m_strDeviceID;
			m_pressDevice.m_fPlateWidth  = m_fWidth;
			m_pressDevice.m_fPlateHeight = m_fHeight;
			m_pressDevice.m_strNotes	 = m_strNotes;

			CButton* pBut = (CButton*)GetDlgItem (IDC_SHEET);
			if (pBut->GetCheck() == 1)	
			{
				m_pressDevice.m_nPressType				= CPressDevice::SheetPress;
				m_pressDevice.m_fPlateEdgeToPrintEdge	= m_fPlateEdge;
				m_pressDevice.m_fGripper				= m_fGripper;
				m_pressDevice.m_fXRefLine				= 0.0f;
				m_pressDevice.m_fYRefLine1				= 0.0f;
				m_pressDevice.m_fYRefLine2				= 0.0f;
			}
			else						
			{
				m_pressDevice.m_fPlateEdgeToPrintEdge	= 0.0f;
				m_pressDevice.m_fGripper				= 0.0f;
				m_pressDevice.m_nPressType				= CPressDevice::WebPress;
				m_pressDevice.m_fXRefLine				= m_fPlateEdge;
				m_pressDevice.m_fYRefLine1				= m_fGripper;
				m_pressDevice.m_fYRefLine2				= m_fYRefLine2;
			}
		}
	}

	CDialog::OnOK();
}

void CDlgPressDevice::SetPressDevice(int nSide, int nIndex)
{
	if (nIndex == CB_ERR)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CLayout* pLayout = (nSide == ALLSIDES) ? &pDoc->m_PrintSheetList.m_Layouts.GetHead() : GetActLayout();
	if ( ! pLayout)
		return;

	CPressDevice* pOldPressDevice = (nSide == BACKSIDE) ? pLayout->m_BackSide.GetPressDevice() : pLayout->m_FrontSide.GetPressDevice();
	pDoc->SetPrintSheetPressDevice(nIndex, nSide, pLayout);
	CPressDevice* pNewPressDevice = (nSide == BACKSIDE) ? pLayout->m_BackSide.GetPressDevice() : pLayout->m_FrontSide.GetPressDevice();
	if ( ! pOldPressDevice || ! pNewPressDevice)
		return;
													
	float fGripper = CheckGripper(pOldPressDevice, pNewPressDevice, (nSide == BACKSIDE) ? &pLayout->m_BackSide : &pLayout->m_FrontSide);

	switch (nSide)
	{
	case FRONTSIDE:	GetActLayout()->m_FrontSide.PositionLayoutSide(fGripper); 
					GetActLayout()->AnalyzeMarks();							  break;

	case BACKSIDE:	GetActLayout()->m_BackSide.PositionLayoutSide (fGripper); 
					GetActLayout()->AnalyzeMarks();							  break;

	case BOTHSIDES:	GetActLayout()->m_FrontSide.PositionLayoutSide(fGripper);
					GetActLayout()->m_BackSide.PositionLayoutSide (fGripper); 
					GetActLayout()->AnalyzeMarks();							  break;

	case ALLSIDES:	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
					while (pos)
					{
						CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
						rLayout.m_FrontSide.PositionLayoutSide(fGripper);
						rLayout.m_BackSide.PositionLayoutSide (fGripper);
						rLayout.AnalyzeMarks();							  
					}
					break;
	}

	pDoc->SetModifiedFlag();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),					NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),			NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
}

float CDlgPressDevice::CheckGripper(CPressDevice* pOldPressDevice, CPressDevice* pNewPressDevice, CLayoutSide* pLayoutSide, BOOL bDoWarn)
{
	if ( (pOldPressDevice->m_nPressType == CPressDevice::WebPress) || (pNewPressDevice->m_nPressType == CPressDevice::WebPress) )
		return pNewPressDevice->m_fGripper;
	if ( (fabs(pLayoutSide->m_fPaperWidth) < 0.01) || (fabs(pLayoutSide->m_fPaperHeight) < 0.01) )
		return pNewPressDevice->m_fGripper;

	float fBoundingRectLeft, fBoundingRectBottom, fBoundingRectRight, fBoundingRectTop, fBoundingRectWidth, fBoundingRectHeight;
	pLayoutSide->CalcBoundingRectObjects(&fBoundingRectLeft, &fBoundingRectBottom, &fBoundingRectRight, &fBoundingRectTop, 
										 &fBoundingRectWidth, &fBoundingRectHeight, FALSE, -1, FALSE, NULL);

	float fGripper = pNewPressDevice->m_fGripper;
	if (bDoWarn)
	{
		CLayout* pLayout = pLayoutSide->GetLayout();
		BOOL bHasObjects = (pLayout) ? ((pLayout->HasPages() || pLayout->HasFlatProducts()) ? TRUE : FALSE) : TRUE;
		if (bHasObjects)
		{
			if ( fabs(fBoundingRectBottom - pLayoutSide->m_fPaperBottom) != fGripper)
			{
				if (KIMOpenLogMessage(IDS_GRIPPER_CHANGED_WARNING, MB_YESNO, IDNO) == IDNO)
					fGripper = (float)fabs(fBoundingRectBottom - pLayoutSide->m_fPaperBottom);	// keep existing gripper value
			}
		}
	}

	return fGripper;
}

void CDlgPressDevice::OnSheet() 
{
	if ( ! (theApp.m_PressDeviceList.IsEmpty())) 
	{
		CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);

		if (( ! m_bEditChanged) && (pBox->GetCurSel() != CB_ERR))
		{
			CPressDevice  PressDeviceBuf;

			UINT nIndex = pBox->GetCurSel();
			CString strName;
			pBox->GetLBText(nIndex, strName);
			PressDeviceBuf = *theApp.m_PressDeviceList.GetDevice(strName);

			m_fSheetEdge = PressDeviceBuf.m_fPlateEdgeToPrintEdge - PressDeviceBuf.m_fGripper;
			m_fPlateEdge = PressDeviceBuf.m_fPlateEdgeToPrintEdge;
			m_fGripper   = PressDeviceBuf.m_fGripper;

			m_bModified = TRUE;
			UpdateData(FALSE);
			m_pressDevicePreviewFrame.Invalidate();
			m_pressDevicePreviewFrame.UpdateWindow();
		}
	}
	CString s;
	s.LoadString(IDS_PLATEEDGE_SHEETTEXT);
	SetDlgItemText(IDC_PLATEEDGE_TEXT, s);
	s.LoadString(IDS_GRIPPER_SHEETTEXT);
	SetDlgItemText(IDC_GRIPPER_TEXT, s);

	GetDlgItem(IDC_SHEETEDGE_TEXT)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_SHEETEDGE     )->ShowWindow(SW_SHOW);

	m_bShow2YRefLines = FALSE;
	GetDlgItem(IDC_SHOW_2Y_REFLINES)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_Y_REFLINE_2)->ShowWindow(SW_HIDE);

	UpdateData(FALSE);
}

void CDlgPressDevice::OnWeb() 
{
	BOOL bHas2YRefLines = FALSE;
	if ( ! (theApp.m_PressDeviceList.IsEmpty())) 
	{
		CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);

		if (( ! m_bEditChanged) && (pBox->GetCurSel() != CB_ERR))
		{
			CPressDevice  PressDeviceBuf;

			UINT nIndex     = pBox->GetCurSel();
			CString strName;
			pBox->GetLBText(nIndex, strName);
			PressDeviceBuf = *theApp.m_PressDeviceList.GetDevice(strName);

			m_fSheetEdge = 0.0f;
			m_fPlateEdge = PressDeviceBuf.m_fXRefLine;
			m_fGripper   = PressDeviceBuf.m_fYRefLine1;
			m_fYRefLine2 = PressDeviceBuf.m_fYRefLine2;

			m_bModified = TRUE;
			UpdateData(FALSE);
			m_pressDevicePreviewFrame.Invalidate();
			m_pressDevicePreviewFrame.UpdateWindow();

			bHas2YRefLines = PressDeviceBuf.Has2YRefLines();
		}
	}
	CString s;
	s.LoadString(IDS_PLATEEDGE_WEBTEXT);
	SetDlgItemText(IDC_PLATEEDGE_TEXT, s);
	s.LoadString(IDS_GRIPPER_WEBTEXT);
	SetDlgItemText(IDC_GRIPPER_TEXT, s);

	GetDlgItem(IDC_SHEETEDGE_TEXT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_SHEETEDGE     )->ShowWindow(SW_HIDE);

	m_bShow2YRefLines = bHas2YRefLines;
	GetDlgItem(IDC_SHOW_2Y_REFLINES)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_Y_REFLINE_2)->ShowWindow((bHas2YRefLines)  ? SW_SHOW : SW_HIDE);

	UpdateData(FALSE);
}

void CDlgPressDevice::OnSelchangePressDeviceList() 
{
	CPressDevice  PressDeviceBuf;
	CString s;
	CButton *pBut;

	CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);
	UINT nIndex    = pBox->GetCurSel();
	CString strName;
	pBox->GetLBText(nIndex, strName);
	PressDeviceBuf = *theApp.m_PressDeviceList.GetDevice(strName);
	m_strName     = PressDeviceBuf.m_strName;
	m_strDeviceID = PressDeviceBuf.m_strDeviceID;
	m_fWidth      = PressDeviceBuf.m_fPlateWidth;
	m_fHeight     = PressDeviceBuf.m_fPlateHeight;
	m_strNotes	  = PressDeviceBuf.m_strNotes;
	if (PressDeviceBuf.m_nPressType == CPressDevice::SheetPress)
	{
		GetDlgItem(IDC_SHEETEDGE_TEXT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SHEETEDGE     )->ShowWindow(SW_SHOW);

		m_fSheetEdge = PressDeviceBuf.m_fPlateEdgeToPrintEdge - PressDeviceBuf.m_fGripper;
		m_fPlateEdge = PressDeviceBuf.m_fPlateEdgeToPrintEdge;
		m_fGripper   = PressDeviceBuf.m_fGripper;
		s.LoadString(IDS_PLATEEDGE_SHEETTEXT);
		SetDlgItemText(IDC_PLATEEDGE_TEXT, s);
		s.LoadString(IDS_GRIPPER_SHEETTEXT);
		SetDlgItemText(IDC_GRIPPER_TEXT, s);
		pBut = (CButton*)GetDlgItem (IDC_SHEET);
		pBut->SetCheck(1);
		pBut = (CButton*)GetDlgItem (IDC_WEB);
		pBut->SetCheck(0);
		m_bShow2YRefLines = FALSE;
		GetDlgItem(IDC_SHOW_2Y_REFLINES)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_Y_REFLINE_2)->ShowWindow(SW_HIDE);
	}
	else
	{
		GetDlgItem(IDC_SHEETEDGE_TEXT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SHEETEDGE     )->ShowWindow(SW_HIDE);

		m_fSheetEdge = 0.0f;
		m_fPlateEdge = PressDeviceBuf.m_fXRefLine;
		m_fGripper   = PressDeviceBuf.m_fYRefLine1;
		m_fYRefLine2 = PressDeviceBuf.m_fYRefLine2;
		s.LoadString(IDS_PLATEEDGE_WEBTEXT);
		SetDlgItemText(IDC_PLATEEDGE_TEXT, s);
		s.LoadString(IDS_GRIPPER_WEBTEXT);
		SetDlgItemText(IDC_GRIPPER_TEXT, s);
		pBut = (CButton*)GetDlgItem (IDC_WEB);
		pBut->SetCheck(1);
		pBut = (CButton*)GetDlgItem (IDC_SHEET);
		pBut->SetCheck(0);
		m_bShow2YRefLines = PressDeviceBuf.Has2YRefLines();
		GetDlgItem(IDC_SHOW_2Y_REFLINES)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_Y_REFLINE_2)->ShowWindow((PressDeviceBuf.Has2YRefLines()) ? SW_SHOW : SW_HIDE);
	}
	
	m_bEditChanged = FALSE;
	CButton *pButton = (CButton*)GetDlgItem(IDC_CHANGE);
	pButton->EnableWindow(TRUE);
	//pButton = (CButton*)GetDlgItem(IDC_DEL);
	//pButton->EnableWindow(TRUE);
	m_bModified = TRUE;
	m_pressDevicePreviewFrame.Invalidate();
	m_pressDevicePreviewFrame.UpdateWindow();
	UpdateData(FALSE);
}

void CDlgPressDevice::OnEditchangePressDeviceList() 
{
	m_bEditChanged = TRUE;
	CButton *pButton = (CButton*)GetDlgItem(IDC_CHANGE);
	pButton->EnableWindow(FALSE);
	//pButton = (CButton*)GetDlgItem(IDC_DEL);
	//pButton->EnableWindow(FALSE);
}

void CDlgPressDevice::OnChange() 
{
	UpdateData(TRUE);

	if ( ! theApp.m_PressDeviceList.GetDevice(m_strName))
		return;

	CButton* pBut		= (CButton*)GetDlgItem (IDC_SHEET);
	unsigned nPressType = (pBut->GetCheck() == 1) ? CPressDevice::SheetPress : CPressDevice::WebPress;

	CPressDevice* pPressDevice = theApp.m_PressDeviceList.GetDevice(m_strName);
	if (nPressType == CPressDevice::SheetPress)
		if ( (pPressDevice->m_fPlateWidth == m_fWidth) && (pPressDevice->m_fPlateHeight == m_fHeight) && (pPressDevice->m_fPlateEdgeToPrintEdge == m_fPlateEdge) && (pPressDevice->m_nPressType == nPressType) &&
			(pPressDevice->m_fGripper == m_fGripper) && (pPressDevice->m_strNotes == m_strNotes) && (pPressDevice->m_strDeviceID == m_strDeviceID) )	// nothing changed
			return;
	if (nPressType == CPressDevice::WebPress)
		if ( (pPressDevice->m_fPlateWidth == m_fWidth) && (pPressDevice->m_fPlateHeight == m_fHeight) && (pPressDevice->m_nPressType == nPressType) &&
			 (pPressDevice->m_fYRefLine1 == m_fGripper) && (pPressDevice->m_fXRefLine == m_fPlateEdge) && (pPressDevice->m_fYRefLine2 == m_fYRefLine2) && 
			 (pPressDevice->m_strNotes == m_strNotes) && (pPressDevice->m_strDeviceID == m_strDeviceID) )	// nothing changed
			return;

	CString strUserDefined; 
	strUserDefined.LoadString(IDS_USERDEFINED);
	int nRet = (m_strName != strUserDefined) ? AfxMessageBox(IDS_CHANGE_ENTRY, MB_YESNO) : IDYES;
	if (nRet == IDYES)	
	{
		CPressDevice PressDeviceBuf;

		PressDeviceBuf.m_strName	  = m_strName;
		PressDeviceBuf.m_strDeviceID  = m_strDeviceID;
		PressDeviceBuf.m_fPlateWidth  = m_fWidth;
		PressDeviceBuf.m_fPlateHeight = m_fHeight;
		PressDeviceBuf.m_strNotes	  = m_strNotes;

		PressDeviceBuf.m_nPressType	  = nPressType;
		if (nPressType == CPressDevice::SheetPress)	
		{
			PressDeviceBuf.m_fPlateEdgeToPrintEdge	= m_fPlateEdge;
			PressDeviceBuf.m_fGripper				= m_fGripper;
			PressDeviceBuf.m_fXRefLine				= 0.0f;
			PressDeviceBuf.m_fYRefLine1				= 0.0f;
			PressDeviceBuf.m_fYRefLine2				= 0.0f;
		}
		else						
		{
			PressDeviceBuf.m_fPlateEdgeToPrintEdge	= 0.0f;
			PressDeviceBuf.m_fGripper				= 0.0f;
			PressDeviceBuf.m_fXRefLine				= m_fPlateEdge;
			PressDeviceBuf.m_fYRefLine1				= m_fGripper;
			PressDeviceBuf.m_fYRefLine2				= m_fYRefLine2;
		}

		POSITION pos = theApp.m_PressDeviceList.GetDevicePos(m_strName);
		if (pos)
		{
			theApp.m_PressDeviceList.SetAt(pos, PressDeviceBuf);
			if ( ! OnUpdateData())
				AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

			OnLoadingData(); //reload data
			CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);
			pBox->SetCurSel(pBox->FindStringExact(-1, PressDeviceBuf.m_strName));
			OnSelchangePressDeviceList();	

			m_bModified = TRUE;
			m_pressDevicePreviewFrame.Invalidate();
			m_pressDevicePreviewFrame.UpdateWindow();
		}
	}
	else
	{
		m_strName.LoadString(IDS_USERDEFINED);
		UpdateData(FALSE);
		OnNew();
	}
}

void CDlgPressDevice::OnChangeSheetedge() 
{
	m_bModified = TRUE;
}

void CDlgPressDevice::OnChangePlateedge() 
{
	m_bModified = TRUE;
}

void CDlgPressDevice::OnChangeGripper() 
{
	m_bModified = TRUE;
}

void CDlgPressDevice::OnChangePlateHeight() 
{
	m_bModified = TRUE;
}

void CDlgPressDevice::OnChangePlateWidth() 
{
	m_bModified = TRUE;
}

void CDlgPressDevice::OnKillfocusSheetedge() 
{
	UpdateData(TRUE);
	CButton* pBut = (CButton*)GetDlgItem (IDC_SHEET);
	if (pBut->GetCheck() == 1)	
	{
		m_fPlateEdge = m_fSheetEdge + m_fGripper;
		UpdateData(FALSE);
	}
	m_pressDevicePreviewFrame.Invalidate();
	m_pressDevicePreviewFrame.UpdateWindow();
}

void CDlgPressDevice::OnKillfocusPlateedge() 
{
	UpdateData(TRUE);
	CButton* pBut = (CButton*)GetDlgItem (IDC_SHEET);
	if (pBut->GetCheck() == 1)	
	{
		m_fSheetEdge = m_fPlateEdge - m_fGripper;
		UpdateData(FALSE);
	}
	m_pressDevicePreviewFrame.Invalidate();
	m_pressDevicePreviewFrame.UpdateWindow();
}

void CDlgPressDevice::OnKillfocusGripper() 
{
	UpdateData(TRUE);
	CButton* pBut = (CButton*)GetDlgItem (IDC_SHEET);
	if (pBut->GetCheck() == 1)	
	{
		m_fPlateEdge = m_fSheetEdge + m_fGripper;
		UpdateData(FALSE);
	}
	m_pressDevicePreviewFrame.Invalidate();
	m_pressDevicePreviewFrame.UpdateWindow();
}

void CDlgPressDevice::OnKillfocusPlateHeight() 
{
	UpdateData(TRUE);
	m_pressDevicePreviewFrame.Invalidate();
	m_pressDevicePreviewFrame.UpdateWindow();
}

void CDlgPressDevice::OnKillfocusPlateWidth() 
{
	UpdateData(TRUE);
	m_pressDevicePreviewFrame.Invalidate();
	m_pressDevicePreviewFrame.UpdateWindow();
}

void CDlgPressDevice::OnKillfocusYRefLine2() 
{
	UpdateData(TRUE);
	m_pressDevicePreviewFrame.Invalidate();
	m_pressDevicePreviewFrame.UpdateWindow();
}

int CDlgPressDevice::GetActSight()
{
	if (m_nActSight >= 0)
		return m_nActSight;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	return (pDoc) ? pDoc->GetActSight() : -1;
}

CPressDevice* CDlgPressDevice::GetActPressDevice()
{
	if (m_pActPressDevice)
		return m_pActPressDevice;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	return (pDoc) ? pDoc->GetActPressDevice() : NULL;
}

CLayout* CDlgPressDevice::GetActLayout()
{
	if (m_pActLayout)
		return m_pActLayout;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	return (pDoc) ? pDoc->GetActLayout() : NULL;
}

void CDlgPressDevice::OnBnClickedYReflines()
{
	UpdateData(TRUE);

	if (m_bShow2YRefLines)
	{
		m_fYRefLine1 = m_fGripper * 0.5f;
		m_fYRefLine2 = m_fGripper * 1.5f;
		GetDlgItem(IDC_Y_REFLINE_2)->ShowWindow(SW_SHOW);
	}
	else
	{
		m_fYRefLine1 = m_fYRefLine2 = 0.0f;
		GetDlgItem(IDC_Y_REFLINE_2)->ShowWindow(SW_HIDE);
	}

	UpdateData(FALSE);

	m_pressDevicePreviewFrame.Invalidate();
	m_pressDevicePreviewFrame.UpdateWindow();
}
