// DlgLayoutPoolSelection.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgLayoutPoolSelection.h"


// CDlgLayoutPoolSelection-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgLayoutPoolSelection, CDialog)

CDlgLayoutPoolSelection::CDlgLayoutPoolSelection(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgLayoutPoolSelection::IDD, pParent)
	, m_nFoldSchemeIndex(0)
{
	m_smallFont.CreateFont (11, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CDlgLayoutPoolSelection::~CDlgLayoutPoolSelection()
{
	for (int i = 0; i < m_sampleList1.GetSize(); i++)
	{
		if (m_sampleList1[i].pPrintSheet)
			delete m_sampleList1[i].pPrintSheet;
		if (m_sampleList1[i].pLayout)
			delete m_sampleList1[i].pLayout;
	}
	m_sampleList1.RemoveAll();

	for (int i = 0; i < m_sampleList2.GetSize(); i++)
	{
		if (m_sampleList2[i].pPrintSheet)
			delete m_sampleList2[i].pPrintSheet;
		if (m_sampleList2[i].pLayout)
			delete m_sampleList2[i].pLayout;
	}
	m_sampleList2.RemoveAll();

	m_smallFont.DeleteObject();
	m_layoutImageList.DeleteImageList();
}

void CDlgLayoutPoolSelection::OnNMCustomdrawDefaultLayoutList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);
			CPrintSheet* pPrintSheet = (lvitem.iItem < m_sampleList1.GetSize()) ? m_sampleList1[lvitem.iItem].pPrintSheet : NULL;
			CLayout*	 pLayout	 = (lvitem.iItem < m_sampleList1.GetSize()) ? m_sampleList1[lvitem.iItem].pLayout	  : NULL;
			if ( ! pLayout || ! pPrintSheet)
				break;

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			float fPaperWidth  = (pLayout) ? pLayout->m_FrontSide.m_fPaperWidth  : 0.0f;
			float fPaperHeight = (pLayout) ? pLayout->m_FrontSide.m_fPaperHeight : 0.0f;

			POSITION	pos		  = m_productionProfile.m_postpress.m_folding.m_foldSchemeList.FindIndex(m_nFoldSchemeIndex);
			CFoldSheet	foldSheet = (pos) ? m_productionProfile.m_postpress.m_folding.m_foldSchemeList.GetAt(pos) : CFoldSheet();

			rcItem.DeflateRect(5, 5);
			pLayout->Draw(pDC, rcItem, pPrintSheet, NULL, TRUE, FALSE, FALSE, &foldSheet, TRUE);		

			CRect rcLabel;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			if (rcLabel.IsRectEmpty())
				break;

			COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

			CFont* pOldFont = pDC->SelectObject(&m_smallFont);
			CRect rcText = rcLabel; rcText.left += 5; rcText.top = rcText.CenterPoint().y - 20; rcText.bottom = rcText.top + 50;
			//pDC->DrawText(pLayout->m_strLayoutName, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);
			pDC->DrawText(pLayout->m_strLayoutName, -1, rcText, DT_VCENTER | DT_LEFT | DT_END_ELLIPSIS);
			rcText.OffsetRect(0, 15);

			pDC->SetTextColor(DARKGRAY);
			CString string;
			//string.Format(_T("%.1f x %.1f"), pPressDevice->m_fPlateWidth, pPressDevice->m_fPlateHeight);
			//pDC->DrawText(string, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);

			pDC->SetTextColor(crOldTextColor);
			pDC->SelectObject(pOldFont);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}

void CDlgLayoutPoolSelection::OnNMCustomdrawJobLayoutList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);
			CPrintSheet* pPrintSheet = (lvitem.iItem < m_sampleList2.GetSize()) ? m_sampleList2[lvitem.iItem].pPrintSheet : NULL;
			CLayout*	 pLayout	 = (lvitem.iItem < m_sampleList2.GetSize()) ? m_sampleList2[lvitem.iItem].pLayout	  : NULL;
			if ( ! pLayout || ! pPrintSheet)
				break;

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			float fPaperWidth  = (pLayout) ? pLayout->m_FrontSide.m_fPaperWidth  : 0.0f;
			float fPaperHeight = (pLayout) ? pLayout->m_FrontSide.m_fPaperHeight : 0.0f;

			POSITION	pos		  = m_productionProfile.m_postpress.m_folding.m_foldSchemeList.FindIndex(m_nFoldSchemeIndex);
			CFoldSheet	foldSheet = (pos) ? m_productionProfile.m_postpress.m_folding.m_foldSchemeList.GetAt(pos) : CFoldSheet();

			rcItem.DeflateRect(5, 5);
			pLayout->Draw(pDC, rcItem, pPrintSheet, NULL, TRUE, FALSE, FALSE, &foldSheet, TRUE);		

			CRect rcLabel;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			if (rcLabel.IsRectEmpty())
				break;

			COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

			CFont* pOldFont = pDC->SelectObject(&m_smallFont);
			CRect rcText = rcLabel; rcText.left += 5; rcText.top = rcText.CenterPoint().y - 20; rcText.bottom = rcText.top + 50;
			//pDC->DrawText(pLayout->m_strLayoutName, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);
			pDC->DrawText(pLayout->m_strLayoutName, -1, rcText, DT_VCENTER | DT_LEFT | DT_END_ELLIPSIS);
			rcText.OffsetRect(0, 15);

			pDC->SetTextColor(DARKGRAY);
			CString string;
			//string.Format(_T("%.1f x %.1f"), pPressDevice->m_fPlateWidth, pPressDevice->m_fPlateHeight);
			//pDC->DrawText(string, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);

			pDC->SetTextColor(crOldTextColor);
			pDC->SelectObject(pOldFont);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}

void CDlgLayoutPoolSelection::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LAYOUTSELECTION_SCHEME_COMBO, m_foldSchemeCombo);
	DDX_CBIndex(pDX, IDC_LAYOUTSELECTION_SCHEME_COMBO, m_nFoldSchemeIndex);
	DDX_Control(pDX, IDC_LAYOUTSELECTION_NUP_COMBO, m_nUpCombo);
	DDX_Control(pDX, IDC_LAYOUTSELECTION_LIST1, m_defaultLayoutList);
	DDX_Control(pDX, IDC_LAYOUTSELECTION_LIST2, m_jobLayoutList);
}


BEGIN_MESSAGE_MAP(CDlgLayoutPoolSelection, CDialog)
	ON_CBN_SELCHANGE(IDC_LAYOUTSELECTION_SCHEME_COMBO, OnCbnSelchangeLayoutselectionSchemeCombo)
	ON_CBN_SELCHANGE(IDC_LAYOUTSELECTION_NUP_COMBO, OnCbnSelchangeNUpCombo)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LAYOUTSELECTION_LIST1, OnNMCustomdrawDefaultLayoutList)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LAYOUTSELECTION_LIST2, OnNMCustomdrawJobLayoutList)
	ON_BN_CLICKED(IDOK, &CDlgLayoutPoolSelection::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgLayoutPoolSelection-Meldungshandler

BOOL CDlgLayoutPoolSelection::OnInitDialog()
{
	CDialog::OnInitDialog();

	if ( ! m_layoutImageList.m_hImageList)
	{
		CBitmap bitmap;
		m_layoutImageList.Create(96, 96, ILC_COLOR8, 1, 0);	// List not to grow
		bitmap.LoadBitmap(IDB_BKSCHEMELIST);
		m_layoutImageList.Add(&bitmap, BLACK);
		m_defaultLayoutList.SetImageList(&m_layoutImageList, LVSIL_SMALL);
		m_jobLayoutList.SetImageList(&m_layoutImageList, LVSIL_SMALL);
		bitmap.DeleteObject();
	}

	CFoldingProcess& rFoldingProcess = m_productionProfile.m_postpress.m_folding;
	POSITION pos = rFoldingProcess.m_foldSchemeList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = rFoldingProcess.m_foldSchemeList.GetNext(pos);
		m_foldSchemeCombo.AddString(rFoldSheet.m_strFoldSheetTypeName);
	}
	m_foldSchemeCombo.SetCurSel(0);

	m_nNup = 1;
	InitDefaultLayoutList();
	InitNUp();

	InitJobLayoutList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgLayoutPoolSelection::InitNUp()
{
	m_nUpCombo.ResetContent();

	CPrintSheet* pPrintSheet = (m_sampleList1.GetSize()) ? m_sampleList1[0].pPrintSheet : NULL;
	CLayout*	 pLayout	 = (m_sampleList1.GetSize()) ? m_sampleList1[0].pLayout		: NULL;
	if ( ! pLayout || ! pPrintSheet)
	{
		m_nNup = 1;
		CString string, strNUp; strNUp.LoadString(IDS_NUP);
		string.Format(_T("1 %s"), strNUp);
		m_nUpCombo.AddString(string);
		return;
	}

	POSITION		 pos				= m_productionProfile.m_postpress.m_folding.m_foldSchemeList.FindIndex(m_nFoldSchemeIndex);
	CFoldSheet		 foldSheet			= (pos) ? m_productionProfile.m_postpress.m_folding.m_foldSchemeList.GetAt(pos) : CFoldSheet();
	int				 nProductPartIndex	= 0;

	theApp.m_defaultBoundProductPart  = m_productPart;
	theApp.m_defaultProductionProfile = m_productionProfile;

	foldSheet.m_nProductPartIndex = -1;	// use defaults

	pPrintSheet->SetFoldSheetVisible(foldSheet, FALSE);
	
	int	nTotalNUp = max(pLayout->GetMaxNUp(*pPrintSheet, &foldSheet, FALSE), pLayout->GetMaxNUp(*pPrintSheet, &foldSheet, TRUE));

	pPrintSheet->SetFoldSheetVisible(foldSheet, TRUE);

	CString string, strNUp; strNUp.LoadString(IDS_NUP);
	for (int i = 0; i < nTotalNUp; i++)
	{
		string.Format(_T("%d %s"), i + 1, strNUp);
		m_nUpCombo.InsertString(i, string);
	}

	m_nNup = min(m_nNup, nTotalNUp);
	m_nUpCombo.SetCurSel(m_nNup - 1);
}

void CDlgLayoutPoolSelection::InitDefaultLayoutList()
{
	UpdateData(TRUE);

	for (int i = 0; i < m_sampleList1.GetSize(); i++)
	{
		if (m_sampleList1[i].pPrintSheet)
			delete m_sampleList1[i].pPrintSheet;
		if (m_sampleList1[i].pLayout)
			delete m_sampleList1[i].pLayout;
	}
	m_sampleList1.RemoveAll();
	m_defaultLayoutList.DeleteAllItems();

	BeginWaitCursor();

	int				 nProductPartIndex	= 0;
	POSITION		 pos				= m_productionProfile.m_postpress.m_folding.m_foldSchemeList.FindIndex(m_nFoldSchemeIndex);
	CFoldSheet		 foldSheet			= (pos) ? m_productionProfile.m_postpress.m_folding.m_foldSchemeList.GetAt(pos) : CFoldSheet();
	foldSheet.m_nProductPartIndex = -1;	// use defaults
	foldSheet.m_nFoldSheetNumber  = 0;

	CPrintSheet printSheet;
	CLayout		layout;

	theApp.m_defaultBoundProductPart  = m_productPart;
	theApp.m_defaultProductionProfile = m_productionProfile;

	CPrintingProfile printingProfile;
	printingProfile.m_strName			= m_productionProfile.m_strName;
	printingProfile.m_strComment		= m_productionProfile.m_strComment;
	printingProfile.m_nProductionType	= m_productionProfile.m_nProductionType;
	printingProfile.m_layoutPool		= m_productionProfile.m_layoutPool;
	printingProfile.m_prepress			= m_productionProfile.m_prepress;
	printingProfile.m_press				= m_productionProfile.m_press;
	printingProfile.m_cutting			= m_productionProfile.m_postpress.m_cutting;
	layout.Create(printingProfile);
	printSheet.Create(0, nProductPartIndex, &layout);

	LV_ITEM item;
	item.mask		= LVIF_PARAM | LVIF_IMAGE | LVIF_STATE;
	item.stateMask	= LVIS_SELECTED;
	item.state		= 0;
	item.cchTextMax = MAX_TEXT;
	item.iItem		= 0; 
	item.iSubItem	= 0;
	item.lParam		= 0;
	item.iImage		= 0;

	LPS_PRINTLAYOUT pl1  = {new (CPrintSheet), new (CLayout)};	pl1.pPrintSheet->LinkLayout(pl1.pLayout);
	*pl1.pPrintSheet = printSheet;
	*pl1.pLayout	 = layout;
	if (pl1.pLayout->MakeNUp(TRUE, *pl1.pPrintSheet, &foldSheet, 0, m_nNup, TOP, &printingProfile, TRUE))
	{
		m_sampleList1.Add(pl1);
		item.iItem = m_defaultLayoutList.GetItemCount(); 
		m_defaultLayoutList.InsertItem(&item);
	}

	LPS_PRINTLAYOUT pl1r  = {new (CPrintSheet), new (CLayout)};	pl1r.pPrintSheet->LinkLayout(pl1r.pLayout);
	*pl1r.pPrintSheet = printSheet;
	*pl1r.pLayout	  = layout;
	if (pl1r.pLayout->MakeNUp(TRUE, *pl1r.pPrintSheet, &foldSheet, 0, m_nNup, LEFT, &printingProfile, TRUE))
	{
		m_sampleList1.Add(pl1r);
		item.iItem = m_defaultLayoutList.GetItemCount(); 
		m_defaultLayoutList.InsertItem(&item);
	}

	LPS_PRINTLAYOUT pl2  = {new (CPrintSheet), new (CLayout)};	pl2.pPrintSheet->LinkLayout(pl2.pLayout);
	*pl2.pPrintSheet = printSheet;
	*pl2.pLayout	 = layout;
	if (pl2.pLayout->MakeSequential(TRUE, *pl2.pPrintSheet, &foldSheet, 0, m_nNup, TOP, &printingProfile, TRUE))
	{
		m_sampleList1.Add(pl2);
		item.iItem = m_defaultLayoutList.GetItemCount(); 
		m_defaultLayoutList.InsertItem(&item);
	}

	LPS_PRINTLAYOUT pl2r  = {new (CPrintSheet), new (CLayout)};	pl2r.pPrintSheet->LinkLayout(pl2r.pLayout);
	*pl2r.pPrintSheet = printSheet;
	*pl2r.pLayout	  = layout;
	if (pl2r.pLayout->MakeSequential(TRUE, *pl2r.pPrintSheet, &foldSheet, 0, m_nNup, LEFT, &printingProfile, TRUE))
	{
		m_sampleList1.Add(pl2r);
		item.iItem = m_defaultLayoutList.GetItemCount(); 
		m_defaultLayoutList.InsertItem(&item);
	}

	LPS_PRINTLAYOUT pl3  = {new (CPrintSheet), new (CLayout)};	pl3.pPrintSheet->LinkLayout(pl3.pLayout);
	*pl3.pPrintSheet = printSheet;
	*pl3.pLayout	 = layout;
	if (pl3.pLayout->MakeWorkAndTurn(TRUE, *pl3.pPrintSheet, &foldSheet, 0, m_nNup, TOP, &printingProfile, TRUE))
	{
		m_sampleList1.Add(pl3);
		item.iItem = m_defaultLayoutList.GetItemCount(); 
		m_defaultLayoutList.InsertItem(&item);
	}

	LPS_PRINTLAYOUT pl3r  = {new (CPrintSheet), new (CLayout)};	pl3r.pPrintSheet->LinkLayout(pl3r.pLayout);
	*pl3r.pPrintSheet = printSheet;
	*pl3r.pLayout	  = layout;
	if (pl3r.pLayout->MakeWorkAndTurn(TRUE, *pl3r.pPrintSheet, &foldSheet, 0, m_nNup, LEFT, &printingProfile, TRUE))
	{
		m_sampleList1.Add(pl3r);
		item.iItem = m_defaultLayoutList.GetItemCount(); 
		m_defaultLayoutList.InsertItem(&item);
	}

	LPS_PRINTLAYOUT pl4  = {new (CPrintSheet), new (CLayout)};	pl4.pPrintSheet->LinkLayout(pl4.pLayout);
	*pl4.pPrintSheet = printSheet;
	*pl4.pLayout	 = layout;
	if (pl4.pLayout->MakeWorkAndTumble(TRUE, *pl4.pPrintSheet, &foldSheet, 0, m_nNup, TOP, &printingProfile, TRUE))
	{
		m_sampleList1.Add(pl4);
		item.iItem = m_defaultLayoutList.GetItemCount(); 
		m_defaultLayoutList.InsertItem(&item);
	}

	LPS_PRINTLAYOUT pl4r  = {new (CPrintSheet), new (CLayout)};	pl4r.pPrintSheet->LinkLayout(pl4r.pLayout);
	*pl4r.pPrintSheet = printSheet;
	*pl4r.pLayout	  = layout;
	if (pl4r.pLayout->MakeWorkAndTumble(TRUE, *pl4r.pPrintSheet, &foldSheet, 0, m_nNup, LEFT, &printingProfile, TRUE))
	{
		m_sampleList1.Add(pl4r);
		item.iItem = m_defaultLayoutList.GetItemCount(); 
		m_defaultLayoutList.InsertItem(&item);
	}

	m_defaultLayoutList.EnsureVisible(0, FALSE);
	EndWaitCursor();
}

void CDlgLayoutPoolSelection::InitJobLayoutList()
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (pDoc->m_PrintSheetList.m_Layouts.GetSize() <= 0)
		return;

	for (int i = 0; i < m_sampleList2.GetSize(); i++)
	{
		if (m_sampleList2[i].pPrintSheet)
			delete m_sampleList2[i].pPrintSheet;
		if (m_sampleList2[i].pLayout)
			delete m_sampleList2[i].pLayout;
	}
	m_sampleList2.RemoveAll();
	m_jobLayoutList.DeleteAllItems();

	BeginWaitCursor();

	LV_ITEM item;
	item.mask		= LVIF_PARAM | LVIF_IMAGE | LVIF_STATE;
	item.stateMask	= LVIS_SELECTED;
	item.state		= 0;
	item.cchTextMax = MAX_TEXT;
	item.iItem		= 0; 
	item.iSubItem	= 0;
	item.lParam		= 0;//(LPARAM)&pl;
	item.iImage		= 0;

	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout&	 rLayout	 = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		CPrintSheet* pPrintSheet = rLayout.GetFirstPrintSheet();
		if (pPrintSheet->IsEmpty())
			continue;

		LPS_PRINTLAYOUT pl1r  = {new (CPrintSheet), new (CLayout)};
		if (pPrintSheet)
			*pl1r.pPrintSheet = *pPrintSheet;
		*pl1r.pLayout = rLayout;
		m_sampleList2.Add(pl1r);
		item.iItem = m_jobLayoutList.GetItemCount(); 
		m_jobLayoutList.InsertItem(&item);
	}

	m_jobLayoutList.EnsureVisible(0, FALSE);

	EndWaitCursor();
}

void CDlgLayoutPoolSelection::OnCbnSelchangeLayoutselectionSchemeCombo()
{
	UpdateData(TRUE);

	InitNUp();
	InitDefaultLayoutList();
}

void CDlgLayoutPoolSelection::OnCbnSelchangeNUpCombo()
{
	UpdateData(TRUE);

	int nSel = m_nUpCombo.GetCurSel();
	if (nSel == CB_ERR)
		return; 

	m_nNup = nSel + 1;

	InitDefaultLayoutList();
}

void CDlgLayoutPoolSelection::OnBnClickedOk()
{
	m_productionProfile.m_layoutPool.RemoveAll();

	POSITION pos = m_defaultLayoutList.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nSel = m_defaultLayoutList.GetNextSelectedItem(pos);
		CProductionProfileLayoutTemplate layoutTemplate;
		layoutTemplate.m_layout		= *m_sampleList1[nSel].pLayout;
		layoutTemplate.m_printSheet = *m_sampleList1[nSel].pPrintSheet;
		m_productionProfile.m_layoutPool.Add(layoutTemplate);
	}

	pos = m_jobLayoutList.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nSel = m_jobLayoutList.GetNextSelectedItem(pos);
		CProductionProfileLayoutTemplate layoutTemplate;
		layoutTemplate.m_layout		= *m_sampleList2[nSel].pLayout;
		layoutTemplate.m_printSheet = *m_sampleList2[nSel].pPrintSheet;
		m_productionProfile.m_layoutPool.Add(layoutTemplate);
	}

	OnOK();
}
