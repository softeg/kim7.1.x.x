// PrintSheetShowAllToolsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PrintSheetShowAllToolsView.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "NewPrintSheetFrame.h"


// CPrintSheetShowAllToolsView

IMPLEMENT_DYNCREATE(CPrintSheetShowAllToolsView, CFormView)

CPrintSheetShowAllToolsView::CPrintSheetShowAllToolsView()
	: CFormView(CPrintSheetShowAllToolsView::IDD)
{

}

CPrintSheetShowAllToolsView::~CPrintSheetShowAllToolsView()
{
}

void CPrintSheetShowAllToolsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPrintSheetShowAllToolsView, CFormView)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FRONTSIDE, OnUpdateViewFrontside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BACKSIDE, OnUpdateViewBackside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BOTHSIDES, OnUpdateViewBothsides)
	ON_UPDATE_COMMAND_UI(IDC_FOLDSHEETCHECK, OnUpdateFoldsheetCheck)
	ON_UPDATE_COMMAND_UI(IDC_PLATECHECK, OnUpdatePlateCheck)
	ON_UPDATE_COMMAND_UI(IDC_BITMAPCHECK, OnUpdateBitmapCheck)
	ON_UPDATE_COMMAND_UI(ID_OBJECTS_TURN, OnUpdateObjectsTurn)
	ON_UPDATE_COMMAND_UI(ID_OBJECTS_TUMBLE, OnUpdateObjectsTumble)
	ON_UPDATE_COMMAND_UI(ID_OBJECTSROTATE_CLOCK, OnUpdateObjectsrotateClock)
	ON_UPDATE_COMMAND_UI(ID_OBJECTSROTATE_COUNTERCLOCK, OnUpdateObjectsrotateCounterclock)
	ON_UPDATE_COMMAND_UI(ID_DELETE_SELECTION, OnUpdateDeleteSelection)
	ON_UPDATE_COMMAND_UI(ID_UNDO, OnUpdateUndo)
	ON_WM_CTLCOLOR()
	ON_WM_DRAWITEM()
END_MESSAGE_MAP()


// CPrintSheetShowAllToolsView-Diagnose

#ifdef _DEBUG
void CPrintSheetShowAllToolsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetShowAllToolsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrintSheetShowAllToolsView-Meldungshandler

void CPrintSheetShowAllToolsView::OnInitialUpdate()
{
	m_pNewPrintSheetFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! m_pNewPrintSheetFrame)
		return;

	m_displayToolBarGroup.SubclassDlgItem(IDC_PRINTTOOL_DISPLAY_FRAME,	this);
	m_modifyToolBarGroup.SubclassDlgItem(IDC_PRINTTOOL_MODIFY_FRAME,	this);

	CFormView::OnInitialUpdate(); 

	CString string;
	string.LoadString(IDS_SHOW);
	m_displayToolBarGroup.SetWindowText(string);
	m_displayToolBar.Create(IDC_PRINTTOOL_DISPLAY_FRAME, ODBL_VERTICAL, this, GetParentFrame(), NULL, IDB_FOLDPRINTVIEW_TOOLBAR, IDB_FOLDPRINTVIEW_TOOLBAR_COLD, CSize(40, 26), FALSE);
	m_displayToolBar.AddButton(-1, IDS_FOLDSHEET_LABEL,	IDC_FOLDSHEETCHECK, 35, 3);
	m_displayToolBar.AddButton(-1, IDS_MEASURING,		IDC_MEASURECHECK,	35, 4);
	m_displayToolBar.AddButton(-1, IDS_PLATE,			IDC_PLATECHECK,		35, 5);
	m_displayToolBar.AddButton(-1, IDS_CONTENTS,		IDC_BITMAPCHECK,	35, 6);

	string.LoadString(IDS_MODIFY);
	m_modifyToolBarGroup.SetWindowText(string);
	m_modifyToolBar.Create(IDC_PRINTTOOL_MODIFY_FRAME, ODBL_VERTICAL, this, GetParentFrame(), NULL, IDB_FOLDPRINTVIEW_TOOLBAR, IDB_FOLDPRINTVIEW_TOOLBAR_COLD, CSize(40, 26), FALSE);
	m_modifyToolBar.AddButton(-1, IDS_TURN,			ID_OBJECTS_TURN,				35, 1);
	m_modifyToolBar.AddButton(-1, IDS_TUMBLE,		ID_OBJECTS_TUMBLE,				35, 2);
	m_modifyToolBar.AddButton(-1, IDS_ROTATE_RIGHT,	ID_OBJECTSROTATE_CLOCK,			35, 9);
	m_modifyToolBar.AddButton(-1, IDS_ROTATE_LEFT,	ID_OBJECTSROTATE_COUNTERCLOCK,	35, 10);
	m_modifyToolBar.AddButton(-1, IDS_DELETE,		ID_DELETE_SELECTION,			35, 11);
}

BOOL CPrintSheetShowAllToolsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_displayToolBar.m_pToolTip)            
		m_displayToolBar.m_pToolTip->RelayEvent(pMsg);	
	if (m_modifyToolBar.m_pToolTip)            
		m_modifyToolBar.m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

void CPrintSheetShowAllToolsView::OnUpdateViewFrontside(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_nViewSide == FRONTSIDE);
}

void CPrintSheetShowAllToolsView::OnUpdateViewBackside(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_nViewSide == BACKSIDE);
}

void CPrintSheetShowAllToolsView::OnUpdateViewBothsides(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_nViewSide == BOTHSIDES);
}

void CPrintSheetShowAllToolsView::OnUpdateFoldsheetCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowFoldSheet);
}

void CPrintSheetShowAllToolsView::OnUpdatePlateCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowPlate);
}

void CPrintSheetShowAllToolsView::OnUpdateBitmapCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowBitmap);
}

void CPrintSheetShowAllToolsView::OnUpdateObjectsTurn(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetTurnActive) );
}

void CPrintSheetShowAllToolsView::OnUpdateObjectsTumble(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetTumbleActive) );
}

void CPrintSheetShowAllToolsView::OnUpdateObjectsrotateClock(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetRotateActive) );
}

void CPrintSheetShowAllToolsView::OnUpdateObjectsrotateCounterclock(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetRotateActive) );
}

void CPrintSheetShowAllToolsView::OnUpdateDeleteSelection(CCmdUI* pCmdUI) 
{
	if (m_pNewPrintSheetFrame->m_bLayoutObjectSelected || m_pNewPrintSheetFrame->m_bFoldSheetSelected || m_pNewPrintSheetFrame->m_bMarksViewActive)
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	else
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
}

void CPrintSheetShowAllToolsView::OnUpdateUndo(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, m_pNewPrintSheetFrame->m_bUndoActive);
}

HBRUSH CPrintSheetShowAllToolsView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

    if(nCtlColor == CTLCOLOR_STATIC)
    {
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		//UINT nID = pWnd->GetDlgCtrlID();
		//switch (nID)
		//{
		//default:break;
		//}
	}

	return hbr;
}
