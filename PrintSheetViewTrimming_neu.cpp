// PrintSheetViewTrimming.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"

const float TOLERANCE = 0.01f;	// Needed in "SearchOn....Line" - functions

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


////////////////////////////////////////////////////////////////////////////////////////////////
//	Calculations for Trimmings

BOOL CPrintSheetView::CalculateAndShowTrimChannel(CTrimChannel *pTrimChannel, CLayoutObject* pObject, int n_Side, CPoint point, CPrintSheet* pPrintSheet)
{
	CLayoutObject *pObjectNeighbour;

	SetTrimChannelMovingDirections(pTrimChannel, n_Side);
	pObjectNeighbour = FindOppositeObject(pObject, n_Side, point);

	pTrimChannel->pObject		   = pObject;
	pTrimChannel->pObjectNeighbour = pObjectNeighbour;

	FindTrimChannel(pTrimChannel, pObject, pObjectNeighbour, n_Side);
	CalculateTrimChannelRect(pTrimChannel, pPrintSheet, &m_DisplayList);
	DetermineChannelWidth(pTrimChannel);

	return TRUE;
}
	  
BOOL CPrintSheetView::CalculateCounterpartTrimChannel(CTrimChannel *pTrimChannel, CLayoutObject* pObject, CLayoutObject* pObjectNeighbour, int n_Side)
{
	int nCounterpartSide;

	if (pObject != NULL)
		nCounterpartSide = pObject->FindCounterpartClickSide(n_Side);
	else
		return FALSE;

	SetTrimChannelMovingDirections(pTrimChannel, nCounterpartSide);
	FindTrimChannel(pTrimChannel, pObject, pObjectNeighbour, nCounterpartSide);

	return TRUE;
}

void CPrintSheetView::SetTrimChannelMovingDirections(CTrimChannel *pTrimChannel, int n_Side)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	switch (n_Side)
	{
		case LEFT	: 
		case RIGHT	: pTrimChannel->n_Direction = VERTICAL;
					  if(pDoc->m_TrimOptions.m_bVertSym)
						  pTrimChannel->n_MoveDirection = LEFT | RIGHT;
					  else
						  if (pDoc->m_TrimOptions.m_bVertLeft)
							  pTrimChannel->n_MoveDirection = LEFT;
						  else
							  pTrimChannel->n_MoveDirection = RIGHT;
					  break;
		case TOP	: 
		case BOTTOM	: pTrimChannel->n_Direction = HORIZONTAL;
					  if(pDoc->m_TrimOptions.m_bHoriSym)
						  pTrimChannel->n_MoveDirection = BOTTOM | TOP;
					  else
						  if (pDoc->m_TrimOptions.m_bHoriUp)
							  pTrimChannel->n_MoveDirection = TOP;
						  else
							  pTrimChannel->n_MoveDirection = BOTTOM;
					  break;
	}
}

CLayoutObject* CPrintSheetView::FindOppositeObject(CLayoutObject* pObject, int n_Side, CPoint point)
{
	int i;
	float fXDiffMin, fYDiffMin;
	CLayoutObject *pObjectOpposite, *pObjectTemp;

	pObjectOpposite = NULL;
	fXDiffMin = fYDiffMin = FLT_MAX;

	switch (n_Side)
	{
	case LEFT  : for (i = 0; i < pObject->m_LeftNeighbours.GetSize(); i++)
				 {
					pObjectTemp = pObject->m_LeftNeighbours.GetAt(i);
					if ( (pObjectTemp->GetBottom() <= point.y) && (pObjectTemp->GetTop() >= point.y) )
					{
						if (pObjectTemp->m_nType != CLayoutObject::ControlMark)
							pObjectOpposite = pObjectTemp;
						break;
					}
					else
						if ((pObject->GetLeft() - pObjectTemp->GetRight()) < fXDiffMin)
						{
							fXDiffMin = pObject->GetLeft() - pObjectTemp->GetRight();
							if (pObjectTemp->m_nType != CLayoutObject::ControlMark)
								pObjectOpposite = pObjectTemp;
						}
				 }
				 break;
	case RIGHT : for (i = 0; i < pObject->m_RightNeighbours.GetSize(); i++)
				 {
					pObjectTemp = pObject->m_RightNeighbours.GetAt(i);
					if ( (pObjectTemp->GetBottom() <= point.y) && (pObjectTemp->GetTop() >= point.y) )
					{
						if (pObjectTemp->m_nType != CLayoutObject::ControlMark)
							pObjectOpposite = pObjectTemp;
						break;
					}
					else
						if ((pObjectTemp->GetLeft() - pObject->GetRight()) < fXDiffMin)
						{
							fXDiffMin = pObjectTemp->GetLeft() - pObject->GetRight();
							if (pObjectTemp->m_nType != CLayoutObject::ControlMark)
								pObjectOpposite = pObjectTemp;
						}
				 }
				 break;
	case BOTTOM: for (i = 0; i < pObject->m_LowerNeighbours.GetSize(); i++)
				 {
					pObjectTemp = pObject->m_LowerNeighbours.GetAt(i);
					if ( (pObjectTemp->GetLeft() <= point.x) && (pObjectTemp->GetRight() >= point.x) )
					{
						if (pObjectTemp->m_nType != CLayoutObject::ControlMark)
							pObjectOpposite = pObjectTemp;
						break;
					}
					else
						if ((pObject->GetBottom() - pObjectTemp->GetTop()) < fYDiffMin)
						{
							fYDiffMin = pObject->GetBottom() - pObjectTemp->GetTop();
							if (pObjectTemp->m_nType != CLayoutObject::ControlMark)
								pObjectOpposite = pObjectTemp;
						}
				 }
				 break;
	case TOP   : for (i = 0; i < pObject->m_UpperNeighbours.GetSize(); i++)
				 {
					pObjectTemp = pObject->m_UpperNeighbours.GetAt(i);
					if ( (pObjectTemp->GetLeft() <= point.x) && (pObjectTemp->GetRight() >= point.x) )
					{
						if (pObjectTemp->m_nType != CLayoutObject::ControlMark)
							pObjectOpposite = pObjectTemp;
						break;
					}
					else
						if ((pObjectTemp->GetBottom() - pObject->GetTop()) < fYDiffMin)
						{
							fYDiffMin = pObjectTemp->GetBottom() - pObject->GetTop();
							if (pObjectTemp->m_nType != CLayoutObject::ControlMark)
								pObjectOpposite = pObjectTemp;
						}
				 }
				 break;
	}
	return pObjectOpposite;
}

BOOL CPrintSheetView::FindTrimChannel(CTrimChannel *pTrimChannel, CLayoutObject* pObject, CLayoutObject* pObjectNeighbour, int n_Side)
{
	CObList* pObjectListLeft   = new CObList;
	CObList* pObjectListRight  = new CObList;
	CObList* pObjectListTop	   = new CObList;
	CObList* pObjectListBottom = new CObList;

	switch (n_Side)
	{
		case LEFT	: FindChannelPartner(pObjectListLeft,  pObjectNeighbour, LEFT);
					  FindChannelPartner(pObjectListRight, pObject,			 RIGHT);
					  FindUltimateChannelPartner(pTrimChannel, pObjectListLeft, pObjectListRight, VERTICAL);
					  pObjectListLeft->RemoveAll();
					  pObjectListRight->RemoveAll();
					  break;
		case TOP	: FindChannelPartner(pObjectListBottom, pObject,		  BOTTOM);
					  FindChannelPartner(pObjectListTop,	pObjectNeighbour, TOP);
					  FindUltimateChannelPartner(pTrimChannel, pObjectListBottom, pObjectListTop, HORIZONTAL);
					  pObjectListBottom->RemoveAll();
					  pObjectListTop->RemoveAll();
					  break;
		case RIGHT	: FindChannelPartner(pObjectListLeft,  pObject,			 LEFT);
					  FindChannelPartner(pObjectListRight, pObjectNeighbour, RIGHT);
					  FindUltimateChannelPartner(pTrimChannel, pObjectListLeft, pObjectListRight, VERTICAL);
					  pObjectListLeft->RemoveAll();
					  pObjectListRight->RemoveAll();
					  break;
		case BOTTOM	: FindChannelPartner(pObjectListBottom, pObjectNeighbour, BOTTOM);
					  FindChannelPartner(pObjectListTop,	pObject,		  TOP);
					  FindUltimateChannelPartner(pTrimChannel, pObjectListBottom, pObjectListTop, HORIZONTAL);
					  pObjectListBottom->RemoveAll();
					  pObjectListTop->RemoveAll();
					  break;
	}
	delete pObjectListLeft;
	delete pObjectListRight;
	delete pObjectListTop;
	delete pObjectListBottom;

	return TRUE;
}

BOOL CPrintSheetView::FindChannelPartner(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	if (pObject != NULL)
		pObjectList->AddTail(pObject);

	if (pObject == NULL || (pDoc->m_TrimOptions.m_bChannelSingle))
		return FALSE;

	switch (n_ChannelSide)
	{
		case LEFT	:
		case RIGHT	: SearchOnUpperLine(pObjectList, pObject, n_ChannelSide);
					  SearchOnLowerLine(pObjectList, pObject, n_ChannelSide);
					  break;
		case TOP	:
		case BOTTOM	: SearchOnRightLine(pObjectList, pObject, n_ChannelSide);
					  SearchOnLeftLine(pObjectList, pObject, n_ChannelSide);
					  break;
	}
	return TRUE;
}

BOOL CPrintSheetView::FindUltimateChannelPartner(CTrimChannel *pTrimChannel, CObList *pObjectList1, CObList *pObjectList2, int nDirection)
{
	POSITION	  pos1, pos2;
	CLayoutObject *pObject1, *pObject2;

	int n_SizeList1  = pObjectList1->GetCount();
	int n_SizeList2  = pObjectList2->GetCount();

	pTrimChannel->pObjectList1->AddTail(pObjectList1);
	pTrimChannel->pObjectList2->AddTail(pObjectList2);

	return TRUE;

	//switch(nDirection)
	//{
	//case HORIZONTAL : if (pObjectList1->IsEmpty() && ! pObjectList2->IsEmpty())
	//				  {
	//					  pTrimChannel->n_MoveDirection = TOP;
	//					  pos2 = pObjectList2->GetHeadPosition();
	//					  for (int i = 0; i < n_SizeList2; i++)
	//					  {
	//						  pObject2 = (CLayoutObject*)pObjectList2->GetAt(pos2);
	//						  pTrimChannel->pObjectList2->AddTail(pObject2);
	//						  pObjectList2->GetNext(pos2);
	//					  }
	//				  }
	//				  else
	//				  {
	//					  pos1 = pObjectList1->GetHeadPosition();
	//					  for (int i = 0; i < n_SizeList1; i++)
	//					  {
	//						  pObject1 = (CLayoutObject*)pObjectList1->GetAt(pos1);

	//						  pos2 = pObjectList2->GetHeadPosition();
	//						  for (int j = 0; j < n_SizeList2; j++)
	//						  {
	//							  pObject2 = (CLayoutObject*)pObjectList2->GetAt(pos2);
	//							  if ( Neighbours(&(pObject1->m_UpperNeighbours), pObject2))
	//							  {
	//								  pTrimChannel->pObjectList1->AddTail(pObject1);
	//								  break;
	//							  }
	//							  pObjectList2->GetNext(pos2);
	//						  }
	//						  pObjectList1->GetNext(pos1);
	//					  }
	//				  }
	//				  if (pObjectList2->IsEmpty() && ! pObjectList1->IsEmpty())
	//				  {
	//					  pTrimChannel->n_MoveDirection = BOTTOM;
	//					  pos1 = pObjectList1->GetHeadPosition();
	//					  for (int i = 0; i < n_SizeList1; i++)
	//					  {
	//						  pObject1 = (CLayoutObject*)pObjectList1->GetAt(pos1);
	//						  pTrimChannel->pObjectList1->AddTail(pObject1);
	//						  pObjectList1->GetNext(pos1);
	//					  }
	//				  }
	//				  else
	//				  {
	//					  pos2 = pObjectList2->GetHeadPosition();
	//					  for (int i = 0; i < n_SizeList2; i++)
	//					  {
	//						  pObject2 = (CLayoutObject*)pObjectList2->GetAt(pos2);

	//						  pos1 = pObjectList1->GetHeadPosition();
	//						  for (int j = 0; j < n_SizeList1; j++)
	//						  {
	//							  pObject1 = (CLayoutObject*)pObjectList1->GetAt(pos1);
	//							  if ( Neighbours(&(pObject2->m_LowerNeighbours), pObject1))
	//							  {
	//								  pTrimChannel->pObjectList2->AddTail(pObject2);
	//								  break;
	//							  }
	//							  pObjectList1->GetNext(pos1);
	//						  }
	//						  pObjectList2->GetNext(pos2);
	//					  }
	//				  }
	//				  break;
	//case VERTICAL	: if (pObjectList1->IsEmpty() && ! pObjectList2->IsEmpty())
	//				  {
	//					  pTrimChannel->n_MoveDirection = RIGHT;
	//					  pos2 = pObjectList2->GetHeadPosition();
	//					  for (int i = 0; i < n_SizeList2; i++)
	//					  {
	//						  pObject2 = (CLayoutObject*)pObjectList2->GetAt(pos2);
	//						  pTrimChannel->pObjectList2->AddTail(pObject2);
	//						  pObjectList2->GetNext(pos2);
	//					  }
	//				  }
	//				  else
	//				  {
	//					  pos1 = pObjectList1->GetHeadPosition();
	//					  for (int i = 0; i < n_SizeList1; i++)
	//					  {
	//						  pObject1 = (CLayoutObject*)pObjectList1->GetAt(pos1);

	//						  pos2 = pObjectList2->GetHeadPosition();
	//						  for (int j = 0; j < n_SizeList2; j++)
	//						  {
	//							  pObject2 = (CLayoutObject*)pObjectList2->GetAt(pos2);
	//							  if ( Neighbours(&(pObject1->m_RightNeighbours), pObject2))
	//							  {
	//								  pTrimChannel->pObjectList1->AddTail(pObject1);
	//								  break;
	//							  }
	//							  pObjectList2->GetNext(pos2);
	//						  }
	//						  pObjectList1->GetNext(pos1);
	//					  }
	//				  }
	//				  if (pObjectList2->IsEmpty() && ! pObjectList1->IsEmpty())
	//				  {
	//					  pTrimChannel->n_MoveDirection = LEFT;
	//					  pos1 = pObjectList1->GetHeadPosition();
	//					  for (int i = 0; i < n_SizeList1; i++)
	//					  {
	//						  pObject1 = (CLayoutObject*)pObjectList1->GetAt(pos1);
	//						  pTrimChannel->pObjectList1->AddTail(pObject1);
	//						  pObjectList1->GetNext(pos1);
	//					  }
	//				  }
	//				  else
	//				  {
	//					  pos2 = pObjectList2->GetHeadPosition();
	//					  for (int i = 0; i < n_SizeList2; i++)
	//					  {
	//						  pObject2 = (CLayoutObject*)pObjectList2->GetAt(pos2);

	//						  pos1 = pObjectList1->GetHeadPosition();
	//						  for (int j = 0; j < n_SizeList1; j++)
	//						  {
	//							  pObject1 = (CLayoutObject*)pObjectList1->GetAt(pos1);
	//							  if ( Neighbours(&(pObject2->m_LeftNeighbours), pObject1))
	//							  {
	//								  pTrimChannel->pObjectList2->AddTail(pObject2);
	//								  break;
	//							  }
	//							  pObjectList1->GetNext(pos1);
	//						  }
	//						  pObjectList2->GetNext(pos2);
	//					  }
	//				  }
	//				  break;
	//}
	//return TRUE;
}

BOOL CPrintSheetView::CalculateTrimChannelRect(CTrimChannel *pTrimChannel, CPrintSheet* pPrintSheet, CDisplayList* pDisplayList)
{
	POSITION	   pos1, pos2;
	CLayoutObject *pObject1, *pObject2;
	int			   n_SizeList1, n_SizeList2, i;
	float fHeightMinLeft, fHeightMinRight, fHeightMaxLeft, fHeightMaxRight,
		  fWidthMinBottom, fWidthMinTop, fWidthMaxBottom, fWidthMaxTop;

	fWidthMinBottom = fWidthMinTop	  =  FLT_MAX;
	fWidthMaxBottom = fWidthMaxTop	  = -FLT_MAX;
	fHeightMinLeft  = fHeightMinRight =  FLT_MAX;
	fHeightMaxLeft  = fHeightMaxRight = -FLT_MAX;

	n_SizeList1  = pTrimChannel->pObjectList1->GetCount();
	n_SizeList2  = pTrimChannel->pObjectList2->GetCount();
	if (n_SizeList1 > 0)
	{
		pos1			  = pTrimChannel->pObjectList1->GetHeadPosition();
		pObject1		  = (CLayoutObject*)pTrimChannel->pObjectList1->GetAt(pos1);
	}
	if (n_SizeList2 > 0)
	{
		pos2			  = pTrimChannel->pObjectList2->GetHeadPosition();
		pObject2		  = (CLayoutObject*)pTrimChannel->pObjectList2->GetAt(pos2);
	}

	switch(pTrimChannel->n_Direction)
	{
	case HORIZONTAL: if (pTrimChannel->pObjectList2->IsEmpty())
						 pTrimChannel->Rect.top = (long)(pObject1->GetLayoutSide()->m_fPaperTop);
					 else
						 pTrimChannel->Rect.top	  = (long)(pObject2->GetBottom());
					 if (pTrimChannel->pObjectList1->IsEmpty())
						 pTrimChannel->Rect.bottom = (long)(pObject2->GetLayoutSide()->m_fPaperBottom);
					 else
						 pTrimChannel->Rect.bottom = (long)(pObject1->GetTop());
					 for (i = 0; i < n_SizeList1; i++)
					 {
						 pObject1 = (CLayoutObject*)pTrimChannel->pObjectList1->GetAt(pos1);
						 if (pObject1->GetLeft() < fWidthMinBottom)
							 fWidthMinBottom = pObject1->GetLeft();
						 if (pObject1->GetRight() > fWidthMaxBottom)
							 fWidthMaxBottom = pObject1->GetRight();
						 pTrimChannel->pObjectList1->GetNext(pos1);
					 }
					 for (i = 0; i < n_SizeList2; i++)
					 {
						 pObject2 = (CLayoutObject*)pTrimChannel->pObjectList2->GetAt(pos2);
						 if (pObject2->GetLeft() < fWidthMinTop)
							 fWidthMinTop = pObject2->GetLeft();
						 if (pObject2->GetRight() > fWidthMaxTop)
							 fWidthMaxTop = pObject2->GetRight();
						 pTrimChannel->pObjectList2->GetNext(pos2);
					 }
					 if (fWidthMinBottom < fWidthMinTop)
						 pTrimChannel->Rect.left   = (long)((fWidthMinTop < FLT_MAX) ? fWidthMinTop : fWidthMinBottom);
					 else
						 pTrimChannel->Rect.left   = (long)((fWidthMinBottom < FLT_MAX) ? fWidthMinBottom : fWidthMinTop);
					 if (fWidthMaxBottom < fWidthMaxTop)
						 pTrimChannel->Rect.right   = (long)((fWidthMaxBottom > -FLT_MAX) ? fWidthMaxBottom : fWidthMaxTop);
					 else
						 pTrimChannel->Rect.right   = (long)((fWidthMaxTop > -FLT_MAX) ? fWidthMaxTop : fWidthMaxBottom);
					 break;
	case VERTICAL  : if (pTrimChannel->pObjectList1->IsEmpty())
						 pTrimChannel->Rect.left = (long)(pObject2->GetLayoutSide()->m_fPaperLeft);
					 else
						 pTrimChannel->Rect.left   = (long)(pObject1->GetRight());
					 if (pTrimChannel->pObjectList2->IsEmpty())
						 pTrimChannel->Rect.right = (long)(pObject1->GetLayoutSide()->m_fPaperRight);
					 else
						 pTrimChannel->Rect.right	= (long)(pObject2->GetLeft());
					 for (i = 0; i < n_SizeList1; i++)
					 {
						 pObject1 = (CLayoutObject*)pTrimChannel->pObjectList1->GetAt(pos1);
						 if (pObject1->GetBottom() < fHeightMinLeft)
							 fHeightMinLeft = pObject1->GetBottom();
						 if (pObject1->GetTop() > fHeightMaxLeft)
							 fHeightMaxLeft = pObject1->GetTop();
						 pTrimChannel->pObjectList1->GetNext(pos1);
					 }
					 for (i = 0; i < n_SizeList2; i++)
					 {
						 pObject2 = (CLayoutObject*)pTrimChannel->pObjectList2->GetAt(pos2);
						 if (pObject2->GetBottom() < fHeightMinRight)
							 fHeightMinRight = pObject2->GetBottom();
						 if (pObject2->GetTop() > fHeightMaxRight)
							 fHeightMaxRight = pObject2->GetTop();
						 pTrimChannel->pObjectList2->GetNext(pos2);
					 }
					 pTrimChannel->Rect.bottom = (long)min(fHeightMinLeft, fHeightMinRight);
					 pTrimChannel->Rect.top	   = (long)max(fHeightMaxLeft, fHeightMaxRight);
					 //if (fHeightMinLeft < fHeightMinRight)
						// pTrimChannel->Rect.bottom = (long)((fHeightMinRight < FLT_MAX) ? fHeightMinRight : fHeightMinLeft);
					 //else
						// pTrimChannel->Rect.bottom = (long)((fHeightMinLeft < FLT_MAX) ? fHeightMinLeft : fHeightMinRight);
					 //if (fHeightMaxLeft < fHeightMaxRight)
						// pTrimChannel->Rect.top      = (long)((fHeightMaxLeft > -FLT_MAX) ? fHeightMaxLeft : fHeightMaxRight);
					 //else
						// pTrimChannel->Rect.top	     = (long)((fHeightMaxRight > -FLT_MAX) ? fHeightMaxRight : fHeightMaxLeft);
					 break;
	}

	CLayoutSide* pLayoutSide = NULL;
	CRect		 rcDisplayObject1;
	CRect		 rcDisplayObject2;
	if (n_SizeList1 > 0)
	{
		pos1			  = pTrimChannel->pObjectList1->GetHeadPosition();
		pObject1		  = (CLayoutObject*)pTrimChannel->pObjectList1->GetAt(pos1);
		pLayoutSide		  = pObject1->GetLayoutSide();
		CDisplayItem* pDI = (pDisplayList) ? pDisplayList->GetItemFromData(pObject1, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)) : NULL;
		rcDisplayObject1  = (pDI) ? pDI->m_AuxRect : CRect();
	}
	if (n_SizeList2 > 0)
	{
		pos2			  = pTrimChannel->pObjectList2->GetHeadPosition();
		pObject2		  = (CLayoutObject*)pTrimChannel->pObjectList2->GetAt(pos2);
		pLayoutSide		  = pObject2->GetLayoutSide();
		CDisplayItem* pDI = (pDisplayList) ? pDisplayList->GetItemFromData(pObject2, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)) : NULL;
		rcDisplayObject2  = (pDI) ? pDI->m_AuxRect : CRect();
	}

	CDisplayItem* pDI				   = (pDisplayList) ? pDisplayList->GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)) : NULL;
	CRect		  rcDisplaySheet	   = (pDI) ? pDI->m_BoundingRect : CRect();
	CRect		  rcDisplayTrimChannel = rcDisplaySheet; 

	int   nHeightMinLeft, nHeightMinRight, nHeightMaxLeft, nHeightMaxRight,
		  nWidthMinBottom, nWidthMinTop, nWidthMaxBottom, nWidthMaxTop;
	nWidthMinBottom = nWidthMinTop	  =  INT_MAX;
	nWidthMaxBottom = nWidthMaxTop	  = -INT_MAX;
	nHeightMinLeft  = nHeightMinRight = -INT_MAX;
	nHeightMaxLeft  = nHeightMaxRight =  INT_MAX;

	switch(pTrimChannel->n_Direction)
	{
	case HORIZONTAL: if (pTrimChannel->pObjectList2->IsEmpty())
						 ;
					 else
					 {
						 pTrimChannel->Rect.top	  = (long)(pObject2->GetBottom());
						 rcDisplayTrimChannel.top = rcDisplayObject2.bottom;
					 }
					 if (pTrimChannel->pObjectList1->IsEmpty())
						 ;
					 else
					 {
						 pTrimChannel->Rect.bottom	 = (long)(pObject1->GetTop());
						 rcDisplayTrimChannel.bottom = rcDisplayObject1.top;
					 }
					 for (i = 0; i < n_SizeList1; i++)
					 {
						 pObject1 = (CLayoutObject*)pTrimChannel->pObjectList1->GetAt(pos1);
						 CDisplayItem* pDI = (pDisplayList) ? pDisplayList->GetItemFromData(pObject1, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)) : NULL;
						 rcDisplayObject1  = (pDI) ? pDI->m_AuxRect : CRect();
						 if (rcDisplayObject1.left < nWidthMinBottom)
							 nWidthMinBottom = rcDisplayObject1.left;
						 if (rcDisplayObject1.right > nWidthMaxBottom)
							 nWidthMaxBottom = rcDisplayObject1.right;
						 pTrimChannel->pObjectList1->GetNext(pos1);
					 }
					 for (i = 0; i < n_SizeList2; i++)
					 {
						 pObject2 = (CLayoutObject*)pTrimChannel->pObjectList2->GetAt(pos2);
						 CDisplayItem* pDI = (pDisplayList) ? pDisplayList->GetItemFromData(pObject2, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)) : NULL;
						 rcDisplayObject2  = (pDI) ? pDI->m_AuxRect : CRect();
						 if (rcDisplayObject2.left < nWidthMinTop)
							 nWidthMinTop = rcDisplayObject2.left;
						 if (rcDisplayObject2.right > nWidthMaxTop)
							 nWidthMaxTop = rcDisplayObject2.right;
						 pTrimChannel->pObjectList2->GetNext(pos2);
					 }
					 if (nWidthMinBottom < nWidthMinTop)
						 rcDisplayTrimChannel.left  = (long)((nWidthMinTop	  !=  INT_MAX) ? nWidthMinTop	 : nWidthMinBottom);
					 else
						 rcDisplayTrimChannel.left  = (long)((nWidthMinBottom !=  INT_MAX) ? nWidthMinBottom : nWidthMinTop);
					 if (nWidthMaxBottom < nWidthMaxTop)
						 rcDisplayTrimChannel.right = (long)((nWidthMaxBottom != -INT_MAX) ? nWidthMaxBottom : nWidthMaxTop);
					 else
						 rcDisplayTrimChannel.right = (long)((nWidthMaxTop	  != -INT_MAX) ? nWidthMaxTop	 : nWidthMaxBottom);
					 break;
	case VERTICAL  : if (pTrimChannel->pObjectList1->IsEmpty())
						 ;
					 else
						 rcDisplayTrimChannel.left = rcDisplayObject1.right;
					 if (pTrimChannel->pObjectList2->IsEmpty())
						 ;
					 else
						 rcDisplayTrimChannel.right = rcDisplayObject2.left;
					 for (i = 0; i < n_SizeList1; i++)
					 {
						 pObject1 = (CLayoutObject*)pTrimChannel->pObjectList1->GetAt(pos1);
						 CDisplayItem* pDI = (pDisplayList) ? pDisplayList->GetItemFromData(pObject1, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)) : NULL;
						 rcDisplayObject1  = (pDI) ? pDI->m_AuxRect : CRect();
						 if (rcDisplayObject1.bottom > nHeightMinLeft)
							 nHeightMinLeft = rcDisplayObject1.bottom;
						 if (rcDisplayObject1.top < nHeightMaxLeft)
							 nHeightMaxLeft = rcDisplayObject1.top;
						 pTrimChannel->pObjectList1->GetNext(pos1);
					 }
					 for (i = 0; i < n_SizeList2; i++)
					 {
						 pObject2 = (CLayoutObject*)pTrimChannel->pObjectList2->GetAt(pos2);
						 CDisplayItem* pDI = (pDisplayList) ? pDisplayList->GetItemFromData(pObject2, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)) : NULL;
						 rcDisplayObject2  = (pDI) ? pDI->m_AuxRect : CRect();
						 if (rcDisplayObject2.bottom > nHeightMinRight)
							 nHeightMinRight = rcDisplayObject2.bottom;
						 if (rcDisplayObject2.top < nHeightMaxRight)
							 nHeightMaxRight = rcDisplayObject2.top;
						 pTrimChannel->pObjectList2->GetNext(pos2);
					 }
					 rcDisplayTrimChannel.bottom = (long)min(fHeightMinLeft, fHeightMinRight);
					 rcDisplayTrimChannel.top	   = (long)max(fHeightMaxLeft, fHeightMaxRight);
					 //if (nHeightMinLeft > nHeightMinRight)
						// rcDisplayTrimChannel.bottom = (long)((nHeightMinRight != -INT_MAX) ? nHeightMinRight : nHeightMinLeft);
					 //else
						// rcDisplayTrimChannel.bottom = (long)((nHeightMinLeft  != -INT_MAX) ? nHeightMinLeft  : nHeightMinRight);
					 //if (nHeightMaxLeft > nHeightMaxRight)
						// rcDisplayTrimChannel.top	 = (long)((nHeightMaxLeft  !=  INT_MAX) ? nHeightMaxLeft  : nHeightMaxRight);
					 //else
						// rcDisplayTrimChannel.top	 = (long)((nHeightMaxRight !=  INT_MAX) ? nHeightMaxRight : nHeightMaxLeft);
					 break;
	}

	pTrimChannel->Rect.left   = WorldToLP((float)pTrimChannel->Rect.left   );
	pTrimChannel->Rect.right  = WorldToLP((float)pTrimChannel->Rect.right  );
	pTrimChannel->Rect.top    = WorldToLP((float)pTrimChannel->Rect.top    );
	pTrimChannel->Rect.bottom = WorldToLP((float)pTrimChannel->Rect.bottom );

	rcDisplayTrimChannel.NormalizeRect();
	pTrimChannel->displayRect = rcDisplayTrimChannel;
	pTrimChannel->displayRect.InflateRect(4, 4);

	return TRUE;
}

BOOL CPrintSheetView::SearchOnUpperLine(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide)
{
	CLayoutObject* pObjectNeighbour;
	BOOL		   found = FALSE;

	for (int i = 0; i < pObject->m_UpperNeighbours.GetSize() && !found; i++)
	{
		switch (n_ChannelSide)
		{
			case RIGHT : pObjectNeighbour = pObject->m_UpperNeighbours.GetAt(i);
						 if (pObjectNeighbour->m_nType != CLayoutObject::ControlMark)
							 if ( fabs(pObjectNeighbour->GetLeft() - pObject->GetLeft()) < TOLERANCE)
								//if (fabs(pObjectNeighbour->m_fLeftBorder - pObject->m_fLeftBorder) < TOLERANCE)
								found = TRUE;
						 break;
			case LEFT  : pObjectNeighbour = pObject->m_UpperNeighbours.GetAt(i);
						 if (pObjectNeighbour->m_nType != CLayoutObject::ControlMark)
							 if ( fabs(pObjectNeighbour->GetRight() - pObject->GetRight()) < TOLERANCE)
								//if (fabs(pObjectNeighbour->m_fRightBorder - pObject->m_fRightBorder) < TOLERANCE)
								found = TRUE;
						 break;
		}
	}
	if (!found)
		return FALSE;
	else
	{
		pObjectList->AddTail(pObjectNeighbour);
		SearchOnUpperLine(pObjectList, pObjectNeighbour, n_ChannelSide);
		return TRUE;
	}
}

BOOL CPrintSheetView::SearchOnLowerLine(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide)
{
	CLayoutObject* pObjectNeighbour;
	BOOL		   found = FALSE;

	for (int i = 0; i < pObject->m_LowerNeighbours.GetSize() && !found; i++)
	{
		switch (n_ChannelSide)
		{
			case RIGHT : pObjectNeighbour = pObject->m_LowerNeighbours.GetAt(i);
						 if (pObjectNeighbour->m_nType != CLayoutObject::ControlMark)
							if ( fabs(pObjectNeighbour->GetLeft() - pObject->GetLeft()) < TOLERANCE)
								//if (fabs(pObjectNeighbour->m_fLeftBorder - pObject->m_fLeftBorder) < TOLERANCE)
								found = TRUE;
						 break;
			case LEFT  : pObjectNeighbour = pObject->m_LowerNeighbours.GetAt(i);
						 if (pObjectNeighbour->m_nType != CLayoutObject::ControlMark)
							 if ( fabs(pObjectNeighbour->GetRight() - pObject->GetRight()) < TOLERANCE)
								//if (fabs(pObjectNeighbour->m_fRightBorder - pObject->m_fRightBorder) < TOLERANCE)
								found = TRUE;
						 break;
		}
	}
	if (!found)
		return FALSE;
	else
	{
		pObjectList->AddTail(pObjectNeighbour);
		SearchOnLowerLine(pObjectList, pObjectNeighbour, n_ChannelSide);
		return TRUE;
	}
}

BOOL CPrintSheetView::SearchOnRightLine(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide)
{
	CLayoutObject* pObjectNeighbour;
	BOOL		   found = FALSE;

	for (int i = 0; i < pObject->m_RightNeighbours.GetSize() && !found; i++)
	{
		switch (n_ChannelSide)
		{
			case TOP   : pObjectNeighbour = pObject->m_RightNeighbours.GetAt(i);
						 if (pObjectNeighbour->m_nType != CLayoutObject::ControlMark)
							 if ( fabs(pObjectNeighbour->GetBottom() - pObject->GetBottom()) < TOLERANCE)
								//if (fabs(pObjectNeighbour->m_fLowerBorder - pObject->m_fLowerBorder) < TOLERANCE)
								found = TRUE;
						 break;
			case BOTTOM: pObjectNeighbour = pObject->m_RightNeighbours.GetAt(i);
						 if (pObjectNeighbour->m_nType != CLayoutObject::ControlMark)
							 if ( fabs(pObjectNeighbour->GetTop() - pObject->GetTop()) < TOLERANCE)
								//if (fabs(pObjectNeighbour->m_fUpperBorder - pObject->m_fUpperBorder) < TOLERANCE)
								found = TRUE;
						 break;
		}
	}
	if (!found)
		return FALSE;
	else
	{
		pObjectList->AddTail(pObjectNeighbour);
		SearchOnRightLine(pObjectList, pObjectNeighbour, n_ChannelSide);
		return TRUE;
	}
}

BOOL CPrintSheetView::SearchOnLeftLine(CObList *pObjectList, CLayoutObject* pObject, int n_ChannelSide)
{
	CLayoutObject  LayoutObject;
	CLayoutObject* pObjectNeighbour;
	BOOL		   found = FALSE;

	for (int i = 0; i < pObject->m_LeftNeighbours.GetSize() && !found; i++)
	{
		switch (n_ChannelSide)
		{
			case TOP   : pObjectNeighbour = pObject->m_LeftNeighbours.GetAt(i);
						 if (pObjectNeighbour->m_nType != CLayoutObject::ControlMark)
							 if ( fabs(pObjectNeighbour->GetBottom() - pObject->GetBottom()) < TOLERANCE)
								//if (fabs(pObjectNeighbour->m_fLowerBorder - pObject->m_fLowerBorder) < TOLERANCE)
								found = TRUE;
						 break;
			case BOTTOM: pObjectNeighbour = pObject->m_LeftNeighbours.GetAt(i);
						 if (pObjectNeighbour->m_nType != CLayoutObject::ControlMark)
							 if ( fabs(pObjectNeighbour->GetTop() - pObject->GetTop()) < TOLERANCE)
								//if (fabs(pObjectNeighbour->m_fUpperBorder - pObject->m_fUpperBorder) < TOLERANCE)
								found = TRUE;
						 break;
		}
	}
	if (!found)
		return FALSE;
	else
	{
		pObjectList->AddTail(pObjectNeighbour);
		SearchOnLeftLine(pObjectList, pObjectNeighbour, n_ChannelSide);
		return TRUE;
	}
	return TRUE;
}

BOOL CPrintSheetView::Neighbours(CObjectNeighbours* pNeighbourList, CLayoutObject* pObject)
{
	return TRUE;

	CLayoutObject* pObjectNeighbour;

	for (int i = 0; i < pNeighbourList->GetSize(); i++)
	{
		pObjectNeighbour = pNeighbourList->GetAt(i);
		if (pObjectNeighbour == pObject)
			return TRUE;
	}
	return FALSE;
}

BOOL CPrintSheetView::DetermineChannelWidth(CTrimChannel *pTrimChannel, float* pfTrim1, float* pfTrim2)
{
	POSITION	  pos1, pos2;
	CLayoutObject *pObject1, *pObject2;
	float		  fBorder1 = 0.0f, fBorder2 = 0.0f;
	BOOL		  bAsymmetric = TRUE;

	int n_SizeList1  = pTrimChannel->pObjectList1->GetCount();
	int n_SizeList2  = pTrimChannel->pObjectList2->GetCount();
	if (n_SizeList1 > 0)
	{
		pos1		= pTrimChannel->pObjectList1->GetHeadPosition();
		pObject1	= (CLayoutObject*)pTrimChannel->pObjectList1->GetAt(pos1);
	}
	else
		bAsymmetric = FALSE;
	if (n_SizeList2 > 0)
	{
		pos2		= pTrimChannel->pObjectList2->GetHeadPosition();
		pObject2	= (CLayoutObject*)pTrimChannel->pObjectList2->GetAt(pos2);
	}
	else
		bAsymmetric = FALSE;

	switch (pTrimChannel->n_Direction)
	{
	case HORIZONTAL: if (n_SizeList1 > 0)
						if (n_SizeList2 > 0)
//							fWidth = pObject2->GetBottom() - pObject1->GetTop();
						{
							fBorder1 = pObject2->m_fLowerBorder; fBorder2 = pObject1->m_fUpperBorder;
						}
						else
							fBorder1 = (pObject1->GetLayoutSide()->m_fPaperBottom + pObject1->GetLayoutSide()->m_fPaperHeight) - pObject1->GetTop();
					 else
						if (n_SizeList2 > 0)
							fBorder1 = pObject2->GetBottom() - pObject2->GetLayoutSide()->m_fPaperBottom;
						else
							fBorder1 = 0.0;
					 break;
	case VERTICAL  : if (n_SizeList1 > 0)
						if (n_SizeList2 > 0)
//							fWidth = pObject2->GetLeft() - pObject1->GetRight();
						{
							fBorder1 = pObject1->m_fRightBorder; fBorder2 = pObject2->m_fLeftBorder; 
						}
						else
							fBorder1 = (pObject1->GetLayoutSide()->m_fPaperLeft + pObject1->GetLayoutSide()->m_fPaperWidth) - pObject1->GetRight();
					 else
						if (n_SizeList2 > 0)
							fBorder1 = pObject2->GetLeft() - pObject2->GetLayoutSide()->m_fPaperLeft;
						else
							fBorder1 = 0.0;
					 break;
	}
	pTrimChannel->fTrim1 = fBorder1;
	pTrimChannel->fTrim2 = fBorder2;

	if (fBorder1 == fBorder2)
		bAsymmetric = FALSE;

	if (pfTrim1)
		*pfTrim1 = fBorder1;
	if (pfTrim2)
		*pfTrim2 = fBorder2;

	return bAsymmetric;
}

BOOL CPrintSheetView::MakeNewChannel(CTrimChannel *pTrimChannel, float fDistance1, float fDistance2)
{
	CLayout* pLayout = pTrimChannel->pObject->GetLayout();
	pLayout->m_FrontSide.ResetPublicFlags();
	pLayout->m_BackSide.ResetPublicFlags();

	BOOL bAsymmetric = FALSE;
	if (pTrimChannel->pObjectList1->GetCount() && pTrimChannel->pObjectList2->GetCount())
		bAsymmetric = (pTrimChannel->fTrim1 != pTrimChannel->fTrim2) ? TRUE : FALSE;

	POSITION pos = NULL;
	float	 fMovingDistance = fDistance1 + fDistance2;
	switch (pTrimChannel->n_Direction)
	{
	case HORIZONTAL: if (pTrimChannel->n_MoveDirection == (BOTTOM | TOP))
						fMovingDistance = fMovingDistance/2.0f;
					 else
						 if ((pLayout->KindOfProduction() == WORK_AND_TUMBLE) && (pTrimChannel->GetLayoutSide() == BACKSIDE) )
							 if (pTrimChannel->pObjectList1->GetCount() && pTrimChannel->pObjectList2->GetCount())
								 if (pTrimChannel->n_MoveDirection & BOTTOM)
									 pTrimChannel->n_MoveDirection = TOP;
								 else
									 pTrimChannel->n_MoveDirection = BOTTOM;

					if (pTrimChannel->n_MoveDirection & BOTTOM)
					{
						pos = pTrimChannel->pObjectList1->GetHeadPosition();
						while (pos)
						{
							CLayoutObject* pObject = (CLayoutObject*)pTrimChannel->pObjectList1->GetNext(pos);
							MoveObjectRecur(pObject, fMovingDistance, BOTTOM);
							if ( ! bAsymmetric)
								pObject->m_nLockBorder &= ~TOP;
						}
					}
					if (pTrimChannel->n_MoveDirection & TOP)
					{
						pos = pTrimChannel->pObjectList2->GetHeadPosition();
						while (pos)
						{
							CLayoutObject* pObject = (CLayoutObject*)pTrimChannel->pObjectList2->GetNext(pos);
							MoveObjectRecur(pObject, fMovingDistance, TOP);
							if ( ! bAsymmetric)
								pObject->m_nLockBorder &= ~BOTTOM;
						}
					}
					if (bAsymmetric)
					{
						pos = pTrimChannel->pObjectList1->GetHeadPosition();
						while (pos)
						{
							CLayoutObject* pObject	= (CLayoutObject*)pTrimChannel->pObjectList1->GetNext(pos);
							pObject->m_fUpperBorder = pTrimChannel->fTrim2;
							pObject->m_nLockBorder |= TOP;
						}
						pos = pTrimChannel->pObjectList2->GetHeadPosition();
						while (pos)
						{
							CLayoutObject* pObject	= (CLayoutObject*)pTrimChannel->pObjectList2->GetNext(pos);
							pObject->m_fLowerBorder = pTrimChannel->fTrim1;
							pObject->m_nLockBorder |= BOTTOM;
						}
					}
					break;
	case VERTICAL:	if (pTrimChannel->n_MoveDirection == (LEFT | RIGHT))
						fMovingDistance = fMovingDistance/2.0f;
					 else
						 if ((pLayout->KindOfProduction() == WORK_AND_TURN) && (pTrimChannel->GetLayoutSide() == BACKSIDE) )
							 if (pTrimChannel->pObjectList1->GetCount() && pTrimChannel->pObjectList2->GetCount())
								 if (pTrimChannel->n_MoveDirection & LEFT)
									 pTrimChannel->n_MoveDirection = RIGHT;
								 else
									 pTrimChannel->n_MoveDirection = LEFT;

					if (pTrimChannel->n_MoveDirection & LEFT)
					{
						pos = pTrimChannel->pObjectList1->GetHeadPosition();
						while (pos)
						{
							CLayoutObject* pObject = (CLayoutObject*)pTrimChannel->pObjectList1->GetNext(pos);
							MoveObjectRecur(pObject, fMovingDistance, LEFT);
							if ( ! bAsymmetric)
								pObject->m_nLockBorder &= ~RIGHT;
						}
					}
					if (pTrimChannel->n_MoveDirection & RIGHT)
					{
						pos = pTrimChannel->pObjectList2->GetHeadPosition();
						while (pos)
						{
							CLayoutObject* pObject = (CLayoutObject*)pTrimChannel->pObjectList2->GetNext(pos);
							MoveObjectRecur(pObject, fMovingDistance, RIGHT);
							if ( ! bAsymmetric)
								pObject->m_nLockBorder &= ~LEFT;
						}
					}
					if (bAsymmetric)
					{
						pos = pTrimChannel->pObjectList1->GetHeadPosition();
						while (pos)
						{
							CLayoutObject* pObject  = (CLayoutObject*)pTrimChannel->pObjectList1->GetNext(pos);
							pObject->m_fRightBorder = pTrimChannel->fTrim1;
							pObject->m_nLockBorder |= RIGHT;
						}
						pos = pTrimChannel->pObjectList2->GetHeadPosition();
						while (pos)
						{
							CLayoutObject* pObject  = (CLayoutObject*)pTrimChannel->pObjectList2->GetNext(pos);
							pObject->m_fLeftBorder  = pTrimChannel->fTrim2;
							pObject->m_nLockBorder |= LEFT;
						}
					}
					break;
	}

	pos = pTrimChannel->pObjectList1->GetHeadPosition();
	if (pos)
	{
		CLayoutObject* pObject = (CLayoutObject*)pTrimChannel->pObjectList1->GetAt(pos);
		CLayoutSide* pLayoutSide = pObject->GetLayoutSide();
		pLayoutSide->Invalidate();
	}
	else
	{
		pos = pTrimChannel->pObjectList2->GetHeadPosition();
		if (pos)
		{
			CLayoutObject* pObject = (CLayoutObject*)pTrimChannel->pObjectList2->GetAt(pos);
			CLayoutSide* pLayoutSide = pObject->GetLayoutSide();
			pLayoutSide->Invalidate();
		}
	}

	return(TRUE);
}

void CPrintSheetView::MoveObjectRecur(CLayoutObject* pObject, float fMovingDistance, int n_Side)
{
	int i;

	if (pObject->m_nPublicFlag)
		return;

	switch (n_Side)
	{
	case LEFT : pObject->SetLeft(pObject->GetLeft() - fMovingDistance, FALSE);
				pObject->SetRight(pObject->GetRight() - fMovingDistance, FALSE);
				//np->xwbel-= fMovingDistance; // TODO :
				MoveObjectInside(pObject, -fMovingDistance, HORIZONTAL);
				for (i = 0; i < pObject->m_LeftNeighbours.GetSize(); i++)
					MoveObjectRecur(pObject->m_LeftNeighbours.GetAt(i), fMovingDistance, n_Side);
				break;
	case RIGHT: pObject->SetLeft(pObject->GetLeft() + fMovingDistance, FALSE);
				pObject->SetRight(pObject->GetRight() + fMovingDistance, FALSE);
				//np->xwbel+= fMovingDistance; // TODO :
				MoveObjectInside(pObject, fMovingDistance, HORIZONTAL);
				for (i = 0; i < pObject->m_RightNeighbours.GetSize(); i++)
					MoveObjectRecur(pObject->m_RightNeighbours.GetAt(i), fMovingDistance, n_Side);
				break;
	case BOTTOM :pObject->SetBottom(pObject->GetBottom() - fMovingDistance, FALSE);
				 pObject->SetTop(pObject->GetTop() - fMovingDistance, FALSE);
				 //np->ywbel-= fMovingDistance; // TODO :
				 MoveObjectInside(pObject, -fMovingDistance, VERTICAL);
				 for (i = 0; i < pObject->m_LowerNeighbours.GetSize(); i++)
					MoveObjectRecur(pObject->m_LowerNeighbours.GetAt(i), fMovingDistance, n_Side);
				break;
	case TOP  : pObject->SetBottom(pObject->GetBottom() + fMovingDistance, FALSE);
				pObject->SetTop(pObject->GetTop() + fMovingDistance, FALSE);
				//np->ywbel+= fMovingDistance; // TODO :
				MoveObjectInside(pObject, fMovingDistance, VERTICAL);
				for (i = 0; i < pObject->m_UpperNeighbours.GetSize(); i++)
					MoveObjectRecur(pObject->m_UpperNeighbours.GetAt(i), fMovingDistance, n_Side);
				break;
	}

	pObject->m_nPublicFlag = TRUE;
}

void CPrintSheetView::MoveObjectInside(CLayoutObject* /*pObject*/, float /*fMovingDistance*/, int nDirection)
{
	//int i;
	// TODO : implement movement of Object inside another Object
	switch (nDirection)
	{
	case HORIZONTAL://for (i = 0; np->inliegend.elem[i]; i++)
					//{
					//	np->inliegend.elem[i]->xw0+= verschiebung;
					//	np->inliegend.elem[i]->xw1+= verschiebung;
					//	np->inliegend.elem[i]->xwbel+= verschiebung;
					//}
					break;
	case VERTICAL  ://for (i = 0; np->inliegend.elem[i]; i++)
					//{
					//	np->inliegend.elem[i]->yw0+= verschiebung;
					//	np->inliegend.elem[i]->yw1+= verschiebung;
					//	np->inliegend.elem[i]->ywbel+= verschiebung;
					//}
					break;
	}
}
