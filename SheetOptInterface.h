
/////// input
typedef struct
{
	int		 	nID;
	float	 	fFormatX;
	float	 	fFormatY;
	unsigned 	nPriority;
	unsigned 	nPivot;

	int		 	nCopies;
	int		 	nPunchID;
	int		 	nPunchX;
	int		 	nPunchY;

	unsigned 	nPunchXOddEven;
	unsigned 	nPunchYOddEven;

	float	 	fOffsetX;
	float	 	fOffsetY;

	float    	fIndentRows;
	float    	fIndentCols;
	BOOL     	bRotateRows;
	BOOL     	bRotateCols;

	int      	nIndentRowsOddEven;
	int      	nIndentColsOddEven;
	int      	nRotateRowsOddEven;
	int      	nRotateColsOddEven;
}SHEETOPT_SORT;

typedef struct
{
	unsigned 		nColumnWay;
	int		 		nCopies;
	float	 		fSheetFormatX;
	float	 		fSheetFormatY;
	int				iConcision;
	int		 		nNumSorts;
	SHEETOPT_SORT	sorts[256];
}SHEETOPT_GLOBAL;


/////// output
typedef struct
{
	int		nID;
	float	fPositionX;
	float	fPositionY;
	int		nTurned;
}SHEETOPT_SHEETOBJECT;

typedef struct
{
	int						nSheetCopies;
	int						nNumSheetObjects;
	SHEETOPT_SHEETOBJECT	sheetObjects[1024];
}SHEETOPT_RESULT;


extern "C" __declspec(dllimport) int  _bo_startOptimizationStruct_DLL(SHEETOPT_GLOBAL *sheetGlobal, SHEETOPT_RESULT *sheetResult, int (*preCutReady)(bool, int, int));
