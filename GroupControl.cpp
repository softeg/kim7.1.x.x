// GroupControl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "GroupControl.h"


// CGroupControl

IMPLEMENT_DYNAMIC(CGroupControl, CStatic)

CGroupControl::CGroupControl()
{
	m_font.CreateFont (13, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CGroupControl::~CGroupControl()
{
	m_font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CGroupControl, CStatic)
	ON_WM_PAINT()
	ON_WM_DRAWITEM()
END_MESSAGE_MAP()



// CGroupControl-Meldungshandler

void CGroupControl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect rcFrame = lpDrawItemStruct->rcItem;
//	rcFrame.top += 10;
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	pDC->SelectObject(&pen);
	CBrush brush(GUICOLOR_GROUP);
	pDC->SelectObject(&brush);
	pDC->RoundRect(rcFrame, CPoint(8, 8));

	CString strText;
	GetWindowText(strText);

	pen.DeleteObject();
	CRect rcHeader = rcFrame; rcHeader.bottom = rcHeader.top + 15;
	pen.CreatePen(PS_SOLID, 1, LIGHTGRAY);
	brush.DeleteObject();
	brush.CreateSolidBrush(RGB(193,211,251));//255, 240, 180));
	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	pDC->RoundRect(rcHeader, CPoint(6,6));
	pen.DeleteObject();
	brush.DeleteObject();

	pDC->SelectObject(m_font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	pDC->DrawText(strText, rcHeader, DT_CENTER | DT_TOP);
}

void CGroupControl::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rcFrame;
	GetClientRect(rcFrame);

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	dc.SelectObject(&pen);
	CBrush brush(GUICOLOR_GROUP);
	dc.SelectObject(&brush);
//	dc.RoundRect(rcFrame, CPoint(8, 8));

	Graphics graphics(dc.m_hDC);
	Rect rc(rcFrame.left, rcFrame.top, rcFrame.Width(), rcFrame.Height());
	LinearGradientBrush br(rc, Color::Color(245,245,240), Color::Color(240,240,230), 0);
	graphics.FillRectangle(&br, rc);

	CString strText;
	GetWindowText(strText);

	pen.DeleteObject();
	CRect rcHeader = rcFrame; rcHeader.bottom = rcHeader.top + 15;
	pen.CreatePen(PS_SOLID, 1, LIGHTGRAY);
	brush.DeleteObject();
	brush.CreateSolidBrush(RGB(193,211,251));//255, 240, 180));
	dc.SelectObject(&pen);
	dc.SelectObject(&brush);
	dc.RoundRect(rcHeader, CPoint(6,6));
	pen.DeleteObject();
	brush.DeleteObject();

	dc.SelectObject(m_font);
	dc.SetBkMode(TRANSPARENT);
	dc.SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	dc.DrawText(strText, rcHeader, DT_CENTER | DT_TOP);
}
