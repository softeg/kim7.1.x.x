// DlgProdDataTool.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProdDataTool.h"


// CDlgProdDataTool-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProdDataTool, CDialog)

CDlgProdDataTool::CDlgProdDataTool(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProdDataTool::IDD, pParent)
{

}

CDlgProdDataTool::~CDlgProdDataTool()
{
}

void CDlgProdDataTool::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgProdDataTool, CDialog)
//	ON_WM_ERASEBKGND()
	ON_WM_MOVE()
END_MESSAGE_MAP()


// CDlgProdDataTool-Meldungshandler

//BOOL CDlgProdDataTool::OnEraseBkgnd( CDC* pDC )
//{
//    CWnd* pParent = GetParent();
//    ASSERT_VALID(pParent);
//    CPoint pt(0, 0);
//    MapWindowPoints(pParent, &pt, 1);
//    pt = pDC->OffsetWindowOrg(pt.x, pt.y);
//    LRESULT lResult = pParent->SendMessage(WM_ERASEBKGND,
//        (WPARAM)pDC->m_hDC, 0L);
//    pDC->SetWindowOrg(pt.x, pt.y);
//    return lResult;
//}

void CDlgProdDataTool::OnMove( int cx, int cy )
{
    Invalidate();
}

