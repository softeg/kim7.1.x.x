#pragma once


#include "OwnerDrawnButtonList.h"


// CAutoServerView-Formularansicht

class CAutoServerJobsView : public CScrollView
{
	DECLARE_DYNCREATE(CAutoServerJobsView)

public:
	DECLARE_DISPLAY_LIST(CAutoServerJobsView, OnMultiSelect, NO, OnStatusChanged, YES)
	DECLARE_DISPLAY_ITEM(CAutoServerJobsView, Job,				OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CAutoServerJobsView, JobStatusField,	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, FALSE)

protected:
	CAutoServerJobsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CAutoServerJobsView();

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


enum displayColIDs {ColJob, ColJobWorkflow, ColJobDate, ColJobStatus, ColJobInputFile, ColJobInputDate, ColJobOutputFile, ColJobOutputDate, ColJobOutputLocation, ColWorkflow, ColWorkflowJobDataSource, ColWorkflowStrippingDataSource, ColWorkflowHotfolder, ColWorkflowJobs};
typedef struct displayColumn
{
	int nID;
	int nWidth;
};
typedef CArray <struct displayColumn, struct displayColumn&> DISPLAY_COLUMN_LIST;

public:
	DISPLAY_COLUMN_LIST		m_displayColumns;


public:
	CSize					m_docSize;
	CHeaderCtrl  			m_HeaderCtrl;
	int			 			m_nHeaderHeight;
	int						m_nHeaderItemsTotalWidth;
	CFont		 			m_headerFont;
	CArray <int, int>		m_headerColumnWidths;
	COwnerDrawnButtonList	m_toolBar;


public:
	void	SetHeaderColumnWidth(int nIndex, int nValue);
	int		GetHeaderColumnWidth(int nIndex, int nDefault);
	int		GetActJobID();
	int		GetActJobStatus();
	CSize	CalcExtents();
	void	ClipHeader(CDC* pDC);
	CRect	Draw(CDC* pDC, CDisplayList* pDisplayList);
	CRect	DrawJobsTable					(CDC* pDC, CDisplayList* pDisplayList);
	CRect	DrawJobInfo						(CDC* pDC, CRect rcFrame, kimAutoDB::_jobs& rJob,			CDisplayList* pDisplayList);
	CRect	DrawJobWorkflowInfo				(CDC* pDC, CRect rcFrame, kimAutoDB::_jobs& rJob,			CDisplayList* pDisplayList);
	CRect	DrawJobDateInfo					(CDC* pDC, CRect rcFrame, kimAutoDB::_jobs& rJob,			CDisplayList* pDisplayList);
	CRect	DrawJobStatusInfo				(CDC* pDC, CRect rcFrame, kimAutoDB::_jobs& rJob,			CDisplayList* pDisplayList);
	void	ResetDisplayColums();
	void	AddDisplayColumn(int nColID, int nWidth);



public:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void OnInitialUpdate();
	virtual void ScrollToPosition(POINT pt);
	virtual void ScrollToDevicePosition(POINT ptDev);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
protected:
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnHeaderTrack	 (NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
};


