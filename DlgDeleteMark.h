#if !defined(AFX_DLGDELETEMARK_H__2E24739F_7559_413B_B012_037F5A35024D__INCLUDED_)
#define AFX_DLGDELETEMARK_H__2E24739F_7559_413B_B012_037F5A35024D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgDeleteMark.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgDeleteMark 

class CDlgDeleteMark : public CDialog
{
// Konstruktion
public:
	CDlgDeleteMark(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgDeleteMark)
	enum { IDD = IDD_DELETE_MARK };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgDeleteMark)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgDeleteMark)
	afx_msg void OnDeleteFrontmarkButton();
	afx_msg void OnDeleteBothmarkButton();
	afx_msg void OnDeleteBackmarkButton();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGDELETEMARK_H__2E24739F_7559_413B_B012_037F5A35024D__INCLUDED_
