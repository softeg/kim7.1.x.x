// DlgBarcodeMarkContent.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetFrame.h"
#include "DlgJobColorDefinitions.h"
#include "DlgBarcodeMarkContent.h"
#include "DlgPlaceholderSyntaxInfo.h"



// CDlgBarcodeMarkContent-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgBarcodeMarkContent, CDialog)

CDlgBarcodeMarkContent::CDlgBarcodeMarkContent(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgBarcodeMarkContent::IDD, pParent)
{
	ERRCODE eCode = S_OK;
	eCode = BCAlloc(&m_pbarCode);
	if (eCode != S_OK)
		MessageBox(_T("BCAlloc failed - Barcode not available"));

	if (theApp.m_nAddOnsLicensed & CImpManApp::Barcode)
		BCLicenseMe(_T("Mem: Tegeler Software-Engineering"), eLicKindDeveloper, 1, _T("9859A097187FB2F2AD47FC15C3ACBFFA"), eLicProd2D);

	m_strOutputText = _T("");
	m_pParent = NULL;
}

CDlgBarcodeMarkContent::~CDlgBarcodeMarkContent()
{
	if (m_pbarCode)
		BCFree(m_pbarCode);
}

BOOL CDlgBarcodeMarkContent::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					Invalidate();
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}	

void CDlgBarcodeMarkContent::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_BARCODEMARKSTRING, m_strOutputText);
	DDX_Control(pDX, IDC_BARCODE_TYPE_COMBO, m_barcodeType);
	DDX_Control(pDX, IDC_BARCODE_TEXT_COMBO, m_barcodeTextCombo);
	DDX_Control(pDX, IDC_BARCODE_TEXTSIZE_SPIN, m_barcodeTextsizeSpin);
	DDX_Measure(pDX, IDC_BARCODE_TEXTSIZE, m_fBarcodeTextsize);
	DDV_MinMaxFloat(pDX, m_fBarcodeTextsize, 0.f, 100.f);
}


BEGIN_MESSAGE_MAP(CDlgBarcodeMarkContent, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_PLACEHOLDER,	OnButtonSelectPlaceholder)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSHEETNUM,			OnPlaceholderFoldsheetNum)
	ON_COMMAND(ID_PLACEHOLDER_COLORNAME,			OnPlaceholderColorName)
	ON_COMMAND(ID_PLACEHOLDER_SHEETSIDE,			OnPlaceholderSheetside)
	ON_COMMAND(ID_PLACEHOLDER_WEBNUM,				OnPlaceholderWebNum)
	ON_COMMAND(ID_PLACEHOLDER_COLORNUM,				OnPlaceholderColorNum)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSHEETNUM_CG,		OnPlaceholderFoldsheetNumCG)
	ON_COMMAND(ID_PLACEHOLDER_JOBTITLE,				OnPlaceholderJobTitle)
	ON_COMMAND(ID_PLACEHOLDER_TILENUM,				OnPlaceholderTileNum)
	ON_COMMAND(ID_PLACEHOLDER_SHEETNUM,				OnPlaceholderSheetNum)
	ON_COMMAND(ID_PLACEHOLDER_FILE,					OnPlaceholderFile)
	ON_COMMAND(ID_PLACEHOLDER_LOWPAGE_SIDE,			OnPlaceholderLowpageSide)
	ON_COMMAND(ID_PLACEHOLDER_LOWPAGE_SHEET,		OnPlaceholderLowpageSheet)
	ON_COMMAND(ID_PLACEHOLDER_HIGHPAGE_SIDE,		OnPlaceholderHighpageSide)
	ON_COMMAND(ID_PLACEHOLDER_HIGHPAGE_SHEET,		OnPlaceholderHighpageSheet)
	ON_COMMAND(ID_PLACEHOLDER_PRESSDEVICE,			OnPlaceholderPressDevice)
	ON_COMMAND(ID_PLACEHOLDER_JOB,					OnPlaceholderJob)
	ON_COMMAND(ID_PLACEHOLDER_JOBNUM,				OnPlaceholderJobNum)
	ON_COMMAND(ID_PLACEHOLDER_CUSTOMER,				OnPlaceholderCustomer)
	ON_COMMAND(ID_PLACEHOLDER_CUSTOMERNUM,			OnPlaceholderCustomerNum)
	ON_COMMAND(ID_PLACEHOLDER_CREATOR,				OnPlaceholderCreator)
	ON_COMMAND(ID_PLACEHOLDER_CREATIONDATE,			OnPlaceholderCreationDate)
	ON_COMMAND(ID_PLACEHOLDER_LASTMODIFIED,			OnPlaceholderLastModified)
	ON_COMMAND(ID_PLACEHOLDER_SCHEDULEDTOPRINT,		OnPlaceholderScheduledToPrint)
	ON_COMMAND(ID_PLACEHOLDER_OUTPUTDATE,			OnPlaceholderOutputDate)
	ON_COMMAND(ID_PLACEHOLDER_NOTES,				OnPlaceholderNotes)
	ON_COMMAND(ID_PLACEHOLDER_ASIR,					OnPlaceholderAsir)
	ON_COMMAND(ID_PLACEHOLDER_PAGENUMLIST,			OnPlaceholderPageNumList)
	ON_COMMAND(ID_PLACEHOLDER_PAGENUM,				OnPlaceholderPageNum)
	ON_COMMAND(ID_PLACEHOLDER_PRODUCTNAME,			OnPlaceholderProductName)
	ON_COMMAND(ID_PLACEHOLDER_PRODUCTPARTNAME,		OnPlaceholderProductPartName)
	ON_COMMAND(ID_PLACEHOLDER_PAPERGRAIN,			OnPlaceholderPaperGrain)
	ON_COMMAND(ID_PLACEHOLDER_PAPERTYPE,			OnPlaceholderPaperType)
	ON_COMMAND(ID_PLACEHOLDER_PAPERGRAMMAGE,		OnPlaceholderPaperGrammage)
	ON_COMMAND(ID_PLACEHOLDER_WORKSTYLE,			OnPlaceholderWorkStyle)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSCHEME,			OnPlaceholderFoldScheme)
	ON_COMMAND(ID_PLACEHOLDER_NUMFOLDSHEETS,		OnPlaceholderNumFoldsheets)
	ON_COMMAND(ID_PLACEHOLDER_BLANK,				OnPlaceholderBlank)
	ON_WM_PAINT()
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNeedText )
	ON_CBN_SELCHANGE(IDC_BARCODE_TYPE_COMBO, &CDlgBarcodeMarkContent::OnCbnSelchangeBarcodeTypeCombo)
	ON_EN_CHANGE(IDC_BARCODEMARKSTRING, &CDlgBarcodeMarkContent::OnEnChangeBarcodemarkstring)
	ON_EN_UPDATE(IDC_BARCODEMARKSTRING, &CDlgBarcodeMarkContent::OnEnUpdateBarcodemarkstring)
	ON_NOTIFY(UDN_DELTAPOS, IDC_BARCODE_TEXTSIZE_SPIN, OnDeltaposBarcodeTextsizeSpin)
	ON_EN_KILLFOCUS(IDC_BARCODE_TEXTSIZE, &CDlgBarcodeMarkContent::OnEnKillfocusBarcodeTextsize)
	ON_CBN_SELCHANGE(IDC_BARCODE_TEXT_COMBO, &CDlgBarcodeMarkContent::OnCbnSelchangeBarcodeTextCombo)
	ON_BN_CLICKED(IDC_BUTTON_PLACEHOLDER_INFO, &CDlgBarcodeMarkContent::OnBnClickedButtonPlaceholderInfo)
END_MESSAGE_MAP()


// CDlgBarcodeMarkContent-Meldungshandler

BOOL CDlgBarcodeMarkContent::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	if ( ! m_placeholderMenu.m_hMenu)
		VERIFY(m_placeholderMenu.LoadMenu(IDR_PLACEHOLDER_POPUP));

	VERIFY(m_placeholderEdit.SubclassDlgItem(IDC_BARCODEMARKSTRING, this));
	m_placeholderEdit.m_pParent			 = this;
	m_placeholderEdit.m_pPlaceholderMenu = &m_placeholderMenu;
	m_placeholderEdit.m_nIDButton		 = IDC_BUTTON_SELECT_PLACEHOLDER;

	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	m_printColorControl.Create(this, m_pParent);

	CRect controlRect, frameRect;
	m_printColorControl.GetWindowRect(controlRect);
	GetDlgItem(IDC_COLORCONTROL_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	m_printColorControl.SetWindowPos(NULL, frameRect.left, frameRect.top, controlRect.Width(), controlRect.Height(), SWP_NOZORDER);
	
	m_bInitialShowFoldSheet = TRUE;

	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_PLACEHOLDER))->SetIcon(theApp.LoadIcon(IDI_DROPDOWN_ARROW));

  // retrieve information from the DLL about the implemented bar code types
	for (int i = 0; i < BCGetBCCount(); i++)
		m_barcodeType.AddString(BCGetBCList()[i]);

	m_barcodeTextsizeSpin.SetRange(0, 1);  
	m_fBarcodeTextsize = 0.0f;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgBarcodeMarkContent::CheckBarcodeForFoldsheetView() 
{
/*
	CPrintSheetView*  pView	 = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetFrame* pFrame = (CPrintSheetFrame*)pView->GetParentFrame();
	CString strTextmark;
	GetDlgItem(IDC_BARCODEMARKSTRING)->GetWindowText(strTextmark);
	if ( (strTextmark.Find("$1_") != -1) || (strTextmark.Find("$4_") != -1) || (strTextmark.Find("$6_") != -1) )
	{
		if ( ! pFrame->m_bShowFoldSheet || m_bInitialShowFoldSheet)
		{
			pFrame->m_bShowFoldSheet = TRUE;
			pView->m_DisplayList.Invalidate();
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
			m_bInitialShowFoldSheet  = FALSE;
		}
	}
	else
	{
		if (pFrame->m_bShowFoldSheet)
		{
			pFrame->m_bShowFoldSheet = FALSE;
			pView->m_DisplayList.Invalidate();
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		}
	}
*/
}

void CDlgBarcodeMarkContent::LoadData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::BarcodeMark)
		return;

	CString strContentType, strBarcodeText, strBarcodeType, strTextOption;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				if (pObjectSource)
				{
					strContentType	   =			 GetStringToken(pObjectSource->m_strSystemCreationInfo, 0, _T("|"));
					strBarcodeText	   =			 GetStringToken(pObjectSource->m_strSystemCreationInfo, 1, _T("|"));
					strBarcodeType	   =			 GetStringToken(pObjectSource->m_strSystemCreationInfo, 2, _T("|"));
					strTextOption	   =			 GetStringToken(pObjectSource->m_strSystemCreationInfo, 3, _T("|"));
					m_fBarcodeTextsize = Ascii2Float(GetStringToken(pObjectSource->m_strSystemCreationInfo, 4, _T("|")));
					int nIndex = m_barcodeType.FindStringExact(-1, strBarcodeType);
					m_barcodeType.SetCurSel(nIndex);
				}
				m_printColorControl.LoadData(pTemplate, pObjectSource, nObjectSourceIndex);
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		strContentType	   =			 GetStringToken(pMark->m_MarkSource.m_strSystemCreationInfo, 0, _T("|"));
		strBarcodeText	   =			 GetStringToken(pMark->m_MarkSource.m_strSystemCreationInfo, 1, _T("|"));
		strTextOption	   =			 GetStringToken(pMark->m_MarkSource.m_strSystemCreationInfo, 3, _T("|"));
		m_fBarcodeTextsize = Ascii2Float(GetStringToken(pMark->m_MarkSource.m_strSystemCreationInfo, 4, _T("|")));
		CBarcodeMark* pBarcodeMark = (CBarcodeMark*)pMark;
		m_barcodeType.SetCurSel(pBarcodeMark->m_nBarcodeType);

		m_printColorControl.LoadData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
	}

	strBarcodeText.TrimLeft(); strBarcodeText.TrimRight();
	if (strContentType == "$BARCODE")
		m_strOutputText = strBarcodeText;

	BOOL	bEnable = TRUE;
	CString strStatic;
	switch (CBarcodeMark::MapTextOption(strTextOption))
	{
	case TOP:		m_barcodeTextCombo.SetCurSel(1);	strStatic.LoadString(IDS_HEIGHT_TEXT);	break;	
	case BOTTOM:	m_barcodeTextCombo.SetCurSel(2);	strStatic.LoadString(IDS_HEIGHT_TEXT);	break;
	case LEFT:		m_barcodeTextCombo.SetCurSel(3);	strStatic.LoadString(IDS_WIDTH_TEXT);	break;
	case RIGHT:		m_barcodeTextCombo.SetCurSel(4);	strStatic.LoadString(IDS_WIDTH_TEXT);	break;
	default:		m_barcodeTextCombo.SetCurSel(0);	strStatic.LoadString(IDS_HEIGHT_TEXT);	bEnable = FALSE; break;
	}
	strStatic.AppendChar(':');
	GetDlgItem(IDC_BARCODE_TEXTSIZE_STATIC)->SetWindowText(strStatic);
	GetDlgItem(IDC_BARCODE_TEXTSIZE_STATIC)->EnableWindow(bEnable);
	GetDlgItem(IDC_BARCODE_TEXTSIZE		  )->EnableWindow(bEnable);
	GetDlgItem(IDC_BARCODE_TEXTSIZE_SPIN  )->EnableWindow(bEnable);

	if (strBarcodeText.Find(_T("$P")) >= 0)		// AsirCode needs Code128C
	{
		m_barcodeType.SetCurSel(eBC_Code128C);
		UpdateData(FALSE);
		Invalidate();
	}

	UpdateData(FALSE);

	//CheckBarcodeForFoldsheetView();
}

void CDlgBarcodeMarkContent::SaveData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::BarcodeMark)
		return;

	UpdateData(TRUE);

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;

				CString strContentType = GetStringToken(pObjectSource->m_strSystemCreationInfo, 0, _T("|"));
				if (strContentType == "$BARCODE")
				{
					CString strBarcodeType;
					if (m_barcodeType.GetCurSel() >= 0)
						m_barcodeType.GetLBText(m_barcodeType.GetCurSel(), strBarcodeType);

					CString strTextOption(_T("TEXT_NONE"));
					switch (m_barcodeTextCombo.GetCurSel())
					{
					case 1:		strTextOption = _T("TEXT_TOP");		break;
					case 2:		strTextOption = _T("TEXT_BOTTOM");		break;
					case 3:		strTextOption = _T("TEXT_LEFT");		break;
					case 4:		strTextOption = _T("TEXT_RIGHT");		break;
					default:	strTextOption = _T("TEXT_NONE");		break;
					}

					pObjectSource->m_strSystemCreationInfo.Format(_T("$BARCODE | %s | %s | %s | %s"), m_strOutputText, strBarcodeType, strTextOption, Float2Ascii(m_fBarcodeTextsize));
				}

				pObjectSource->m_nType = CPageSource::SystemCreated;

				m_printColorControl.SaveData(pTemplate, pObjectSource, nObjectSourceIndex);
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		CString strContentType = GetStringToken(pMark->m_MarkSource.m_strSystemCreationInfo, 0, _T("|"));
		CBarcodeMark* pBarcodeMark = (CBarcodeMark*)pMark;
		pBarcodeMark->m_MarkSource.m_nType = CPageSource::SystemCreated;
		pBarcodeMark->m_strOutputText = m_strOutputText;
		pBarcodeMark->m_nBarcodeType  = (m_barcodeType.GetCurSel() >= 0) ? m_barcodeType.GetCurSel() : eBC_None;

		if (strContentType == "$BARCODE")
		{
			CString strBarcodeType = ( (pBarcodeMark->m_nBarcodeType >= 0) && (pBarcodeMark->m_nBarcodeType < BCGetBCCount()) ) ? BCGetBCList()[pBarcodeMark->m_nBarcodeType] : _T("");
			CString strTextOption(_T("TEXT_NONE"));
			switch (m_barcodeTextCombo.GetCurSel())
			{
			case 1:		strTextOption = _T("TEXT_TOP");		break;
			case 2:		strTextOption = _T("TEXT_BOTTOM");		break;
			case 3:		strTextOption = _T("TEXT_LEFT");		break;
			case 4:		strTextOption = _T("TEXT_RIGHT");		break;
			default:	strTextOption = _T("TEXT_NONE");		break;
			}
			pMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$BARCODE | %s | %s | %s | %s"), m_strOutputText, strBarcodeType, strTextOption, Float2Ascii(m_fBarcodeTextsize));
		}

		m_printColorControl.SaveData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
	}
}

int CDlgBarcodeMarkContent::GetMarkHeadPosition()
{
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
		return pLayoutObject->m_nHeadPosition;
	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
		return pMark->m_nHeadPosition;

	return TOP;
}

float CDlgBarcodeMarkContent::GetMarkWidth()
{
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
		return pLayoutObject->GetWidth();
	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
		return pMark->m_fMarkWidth;

	return 0.0f;
}

float CDlgBarcodeMarkContent::GetMarkHeight()
{
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
		return pLayoutObject->GetHeight();
	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
		return pMark->m_fMarkHeight;

	return 0.0f;
}

void CDlgBarcodeMarkContent::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
		{
			CheckBarcodeForFoldsheetView();
			SaveData();
		}
}

void CDlgBarcodeMarkContent::OnButtonSelectPlaceholder() 
{
	m_placeholderEdit.SetFocus();
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup)
	{
		CRect buttonRect;
		GetDlgItem(IDC_BUTTON_SELECT_PLACEHOLDER)->GetWindowRect(buttonRect);
		pPopup->TrackPopupMenu(TPM_RIGHTALIGN | TPM_RIGHTBUTTON, buttonRect.right, buttonRect.bottom, this);
	}

	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_PLACEHOLDER))->SetCheck(0);
}

void CDlgBarcodeMarkContent::OnSelectPlaceholder(int nID)
{
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if ( ! pPopup)
		return;

	CString strPlaceholder;
	pPopup->GetMenuString(nID, strPlaceholder, MF_BYCOMMAND);
	strPlaceholder = strPlaceholder.Left(3);
	strPlaceholder.Delete(1, 1);	// remove &

	UpdateData(TRUE);

	int nStartChar, nEndChar;
	((CEdit*)GetDlgItem(IDC_BARCODEMARKSTRING))->GetSel(nStartChar, nEndChar);

	if (nStartChar == nEndChar)
		if (nStartChar > 0)
			if (m_strOutputText[nStartChar - 1] == '$')		// assume user already typed $ -> remove $ to prevent from being inserted twice
				nStartChar--;

	m_strOutputText.Delete(nStartChar, nEndChar - nStartChar);
	m_strOutputText.Insert(nStartChar, strPlaceholder);

	UpdateData(FALSE);

	((CEdit*)GetDlgItem(IDC_BARCODEMARKSTRING))->SetSel(nStartChar + 2, nStartChar + 2);

	Apply();
	m_pParent->UpdateView();

	Invalidate();
}

void CDlgBarcodeMarkContent::OnPlaceholderFoldsheetNum()	{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSHEETNUM);		}
void CDlgBarcodeMarkContent::OnPlaceholderColorName()		{ OnSelectPlaceholder(ID_PLACEHOLDER_COLORNAME);		}
void CDlgBarcodeMarkContent::OnPlaceholderSheetside()		{ OnSelectPlaceholder(ID_PLACEHOLDER_SHEETSIDE);		}		
void CDlgBarcodeMarkContent::OnPlaceholderWebNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_WEBNUM);			}
void CDlgBarcodeMarkContent::OnPlaceholderColorNum()		{ OnSelectPlaceholder(ID_PLACEHOLDER_COLORNUM);			}	
void CDlgBarcodeMarkContent::OnPlaceholderFoldsheetNumCG()	{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSHEETNUM_CG);	}
void CDlgBarcodeMarkContent::OnPlaceholderJobTitle()		{ OnSelectPlaceholder(ID_PLACEHOLDER_JOBTITLE);			}
void CDlgBarcodeMarkContent::OnPlaceholderTileNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_TILENUM);			}
void CDlgBarcodeMarkContent::OnPlaceholderSheetNum()		{ OnSelectPlaceholder(ID_PLACEHOLDER_SHEETNUM);			}
void CDlgBarcodeMarkContent::OnPlaceholderFile()			{ OnSelectPlaceholder(ID_PLACEHOLDER_FILE);				}
void CDlgBarcodeMarkContent::OnPlaceholderLowpageSide()		{ OnSelectPlaceholder(ID_PLACEHOLDER_LOWPAGE_SIDE);		}
void CDlgBarcodeMarkContent::OnPlaceholderLowpageSheet()	{ OnSelectPlaceholder(ID_PLACEHOLDER_LOWPAGE_SHEET);	}
void CDlgBarcodeMarkContent::OnPlaceholderHighpageSide()	{ OnSelectPlaceholder(ID_PLACEHOLDER_HIGHPAGE_SIDE);	}
void CDlgBarcodeMarkContent::OnPlaceholderHighpageSheet()	{ OnSelectPlaceholder(ID_PLACEHOLDER_HIGHPAGE_SHEET);	}
void CDlgBarcodeMarkContent::OnPlaceholderPressDevice()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PRESSDEVICE);		}
void CDlgBarcodeMarkContent::OnPlaceholderJob()				{ OnSelectPlaceholder(ID_PLACEHOLDER_JOB);				}
void CDlgBarcodeMarkContent::OnPlaceholderJobNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_JOBNUM);			}
void CDlgBarcodeMarkContent::OnPlaceholderCustomer()		{ OnSelectPlaceholder(ID_PLACEHOLDER_CUSTOMER);			}
void CDlgBarcodeMarkContent::OnPlaceholderCustomerNum()		{ OnSelectPlaceholder(ID_PLACEHOLDER_CUSTOMERNUM);		}
void CDlgBarcodeMarkContent::OnPlaceholderCreator()			{ OnSelectPlaceholder(ID_PLACEHOLDER_CREATOR);			}
void CDlgBarcodeMarkContent::OnPlaceholderCreationDate()	{ OnSelectPlaceholder(ID_PLACEHOLDER_CREATIONDATE);		}
void CDlgBarcodeMarkContent::OnPlaceholderLastModified()	{ OnSelectPlaceholder(ID_PLACEHOLDER_LASTMODIFIED);		}
void CDlgBarcodeMarkContent::OnPlaceholderScheduledToPrint(){ OnSelectPlaceholder(ID_PLACEHOLDER_SCHEDULEDTOPRINT);	}
void CDlgBarcodeMarkContent::OnPlaceholderOutputDate()		{ OnSelectPlaceholder(ID_PLACEHOLDER_OUTPUTDATE);		}
void CDlgBarcodeMarkContent::OnPlaceholderNotes()			{ OnSelectPlaceholder(ID_PLACEHOLDER_NOTES);			}
void CDlgBarcodeMarkContent::OnPlaceholderAsir()			{ OnSelectPlaceholder(ID_PLACEHOLDER_ASIR);				}
void CDlgBarcodeMarkContent::OnPlaceholderPageNumList()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAGENUMLIST);		}
void CDlgBarcodeMarkContent::OnPlaceholderPageNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_PAGENUM);			}
void CDlgBarcodeMarkContent::OnPlaceholderProductName()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PRODUCTNAME);		}
void CDlgBarcodeMarkContent::OnPlaceholderProductPartName()	{ OnSelectPlaceholder(ID_PLACEHOLDER_PRODUCTPARTNAME);	}
void CDlgBarcodeMarkContent::OnPlaceholderPaperGrain()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERGRAIN);		}
void CDlgBarcodeMarkContent::OnPlaceholderPaperType()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERTYPE);		}
void CDlgBarcodeMarkContent::OnPlaceholderPaperGrammage()	{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERGRAMMAGE);	}
void CDlgBarcodeMarkContent::OnPlaceholderWorkStyle()		{ OnSelectPlaceholder(ID_PLACEHOLDER_WORKSTYLE);		}
void CDlgBarcodeMarkContent::OnPlaceholderFoldScheme()		{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSCHEME);		}
void CDlgBarcodeMarkContent::OnPlaceholderNumFoldsheets()	{ OnSelectPlaceholder(ID_PLACEHOLDER_NUMFOLDSHEETS);	}
void CDlgBarcodeMarkContent::OnPlaceholderBlank()			{ OnSelectPlaceholder(ID_PLACEHOLDER_BLANK);			}


BOOL CDlgBarcodeMarkContent::OnToolTipNeedText(UINT /*id*/, NMHDR * pNMHDR, LRESULT * /*pResult*/)
{
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if ( ! pPopup)
		return FALSE; 

	CString strPlaceholder = m_placeholderEdit.GetPlaceholderHit();

	if (strPlaceholder.IsEmpty())
		return FALSE;

	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	pTTT->szText[0] = 0;

	strPlaceholder.Insert(1, '&');
	for (UINT i = 0; i < pPopup->GetMenuItemCount(); i++)
	{
		CString strMenu;
		pPopup->GetMenuString(i, strMenu, MF_BYPOSITION);
		if (strPlaceholder == strMenu.Left(3))
		{
			strMenu = strMenu.Right(strMenu.GetLength() - 4);
			_tcscpy(pTTT->szText, strMenu);
			return TRUE;
		}
	}

	return FALSE;
}

void CDlgBarcodeMarkContent::OnPaint() 
{
	CDialog::OnPaint();

	CRect rcWinRect;
	ERRCODE eCode;
	CDC* pDC = GetDC();

	// get the coordinates (rect) where to draw the bar code
	GetDlgItem(IDC_BARCODE_PREVIEW_FRAME)->GetWindowRect(&rcWinRect);
	ScreenToClient(&rcWinRect);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)m_strOutputText, (pView) ? pView->GetActPrintSheet() : NULL, (pView) ? pView->GetActPlate() : NULL, -1);
	int nIndexBC = m_barcodeType.GetCurSel();
	eCode = CBarcodeMark::GenerateBarcode(m_pbarCode, szDest, (nIndexBC < 0) ? eBC_None : static_cast<e_BarCType>(nIndexBC), TOP); 

	CRgn rgn; 
	rgn.CreateRectRgnIndirect(&rcWinRect);  
	pDC->SelectClipRgn(&rgn);  

	// after generate we can draw the bar code
	if (eCode == S_OK)
	{
		pDC->FillSolidRect(&rcWinRect, WHITE);

		float fMarkWidth  = ( (GetMarkHeadPosition() == TOP) || (GetMarkHeadPosition() == BOTTOM) ) ? GetMarkWidth()  : GetMarkHeight();
		float fMarkHeight = ( (GetMarkHeadPosition() == TOP) || (GetMarkHeadPosition() == BOTTOM) ) ? GetMarkHeight() : GetMarkWidth();
		CRect rcBarcode  = rcWinRect;
		float fMarkRatio = fMarkWidth/fMarkHeight;
		if (fMarkRatio > ((float)rcBarcode.Width()/(float)rcBarcode.Height()) )
			rcBarcode.bottom = rcWinRect.top  + (long)((float)rcWinRect.Width()  / fMarkRatio);
		else
			rcBarcode.right  = rcWinRect.left + (long)((float)rcWinRect.Height() * fMarkRatio);
		CRect rcText = rcBarcode;

		CString strText = szDest;
		if (m_strOutputText.Find(_T("$P")) >= 0)	// includes AsirCode -> show decoded text
		{
			long nCode = _ttoi(szDest);
			strText.Format(_T("%02d"), nCode%100);	// last 2 digits contains sheet number
		}

		switch (m_barcodeTextCombo.GetCurSel())
		{
		case 0:		strText = _T("");	break;
		case 1:		rcText.bottom	 = rcBarcode.top	+ (long)(((float)rcBarcode.Height()/fMarkHeight) * m_fBarcodeTextsize);
					rcBarcode.top	 = rcText.bottom;
					break;
		case 2:		rcText.top		 = rcBarcode.bottom - (long)(((float)rcBarcode.Height()/fMarkHeight) * m_fBarcodeTextsize);
					rcBarcode.bottom = rcText.top;
					break;
		case 3:		rcText.right	 = rcBarcode.left	+ (long)(((float)rcBarcode.Height()/fMarkHeight) * m_fBarcodeTextsize);
					rcBarcode.left	 = rcText.right;
					break;
		case 4:		rcText.left      = rcBarcode.right	- (long)(((float)rcBarcode.Height()/fMarkHeight) * m_fBarcodeTextsize);
					rcBarcode.right  = rcText.left;
					strText.Insert(0, ' ');		// text right -> one char space between text and barcode
					break;
		}

		BCDraw(m_pbarCode, pDC->GetSafeHdc(), &rcBarcode);  // BCDraw draws the bar code to the specified DC
		if ( ! strText.IsEmpty())
			CTextMark::DrawTextfieldPreview(pDC, strText, rcText, TOP, NULL, FALSE, BLACK); 
	}
	else
	{
		// or we show error message
		TCHAR szBuffer[100];
		TCHAR szWrongChar[50];

		int nSize = sizeof(szBuffer) / sizeof(szBuffer[0]);

		// retrieve error information and write it instead of the bar code to the client area
		BCGetErrorText(eCode, szBuffer, nSize);

		// if we have invalid data for the selected bar code type, show it
		if (eCode == HRESULT_FROM_WIN32(ERROR_INVALID_DATA))
		{
			// if BCCheck() fails, we can retrieve the wrong character here
			int nPos = BCGetInvalidDataPos (m_pbarCode);
			LPCSTR szData = BCGetText (m_pbarCode);

			// check if wrong character position is available (-1 = not available)
			if (nPos >= 0 && nPos < (int) _tcslen(szData))
			{
				_stprintf(szWrongChar, _T("'%c' at position %d"), szData[nPos], nPos+1);
				_tcsncat(szBuffer, szWrongChar, nSize-_tcslen(szBuffer)-1);
			}
		}
		else if (eCode == NTE_BAD_LEN)
		{
			// bar code needs a specific number of input characters

			// retrieve number of needed characters (data + check digit)
			INT nLen = BCGetMaxLenOfData(BCGetBCType(m_pbarCode));

			// check if wrong character position is available (-1 = not available)
			if (nLen > 0)
			{
				_stprintf(szWrongChar, _T(" (needs %d chars)"), nLen);
				_tcsncat(szBuffer, szWrongChar, nSize-_tcslen(szBuffer)-1);
			}
		}
		// sometimes the user selects invalid combination of linear and composite symbol
		else if (eCode == HRESULT_FROM_WIN32 (ERROR_NOT_SUPPORTED))
		{
			if (BCGet2DCompositeComponent(m_pbarCode) != eCC_None && BCGet_CompSym_ErrorInCC (m_pbarCode))
			{
				_tcsncpy(szBuffer, _T("Composite Component not supported for the selected bar code type"), nSize);
			}
		}

		pDC->FillSolidRect(&rcWinRect, RGB (255, 240, 240));
		CFont	font;
		font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		CFont* pOldFont = pDC->SelectObject(&font);
		CRect rcText = rcWinRect; rcText.DeflateRect(3,3);
		pDC->DrawText(szBuffer, _tcslen(szBuffer), rcText, DT_LEFT | DT_WORDBREAK);
		pDC->SelectObject(pOldFont);
		font.DeleteObject();
	}

	ReleaseDC (pDC);

	m_placeholderEdit.Invalidate();
	m_placeholderEdit.UpdateWindow();

	m_printColorControl.Invalidate();
	m_printColorControl.UpdateWindow();
}

void CDlgBarcodeMarkContent::OnCbnSelchangeBarcodeTypeCombo()
{
	UpdateData(TRUE);

  // the index of the combobox is initialized according
  // to the enumeration of the bar code types
  //nIndexBC = m_cbTyp.GetCurSel();

  // we display the ratio hint string (how to enter the print ratio)
  // for the selected bar code symbology
  //m_strRatioHint = BCGetRatioHint (static_cast <e_BarCType>(nIndexBC));

	UpdateData(FALSE);
	Invalidate();
}
	
void CDlgBarcodeMarkContent::OnEnChangeBarcodemarkstring()	// message doesn't appear - don't know wy
{
	UpdateData(TRUE);
	Invalidate();
}

void CDlgBarcodeMarkContent::OnEnUpdateBarcodemarkstring()
{
	UpdateData(TRUE);
	Invalidate();
}

void CDlgBarcodeMarkContent::OnDeltaposBarcodeTextsizeSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_barcodeTextsizeSpin.GetBuddy(), pNMUpDown->iDelta, 0.1f);
	//GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow(TRUE);

	UpdateData(TRUE);
	Invalidate();

	*pResult = 0;
}

void CDlgBarcodeMarkContent::OnEnKillfocusBarcodeTextsize()
{
	UpdateData(TRUE);
	Invalidate();
}

void CDlgBarcodeMarkContent::OnCbnSelchangeBarcodeTextCombo()
{
	UpdateData(TRUE);

	int nSel = m_barcodeTextCombo.GetCurSel();
	if ((nSel > 0) && (m_fBarcodeTextsize == 0.0f))
	{
		m_fBarcodeTextsize = (nSel > 2) ? 6.0f : 2.0f;		// nSel > 2 -> text left or right
		UpdateData(FALSE);
	}

	BOOL	bEnable = TRUE;
	CString strStatic;
	switch (nSel)
	{
	case 1:		strStatic.LoadString(IDS_HEIGHT_TEXT);	break;	
	case 2:		strStatic.LoadString(IDS_HEIGHT_TEXT);	break;
	case 3:		strStatic.LoadString(IDS_WIDTH_TEXT);	break;
	case 4:		strStatic.LoadString(IDS_WIDTH_TEXT);	break;
	default:	strStatic.LoadString(IDS_HEIGHT_TEXT);	bEnable = FALSE;	break;
	}
	strStatic.AppendChar(':');
	GetDlgItem(IDC_BARCODE_TEXTSIZE_STATIC)->SetWindowText(strStatic);
	GetDlgItem(IDC_BARCODE_TEXTSIZE_STATIC)->EnableWindow(bEnable);
	GetDlgItem(IDC_BARCODE_TEXTSIZE		  )->EnableWindow(bEnable);
	GetDlgItem(IDC_BARCODE_TEXTSIZE_SPIN  )->EnableWindow(bEnable);

	Apply();
	Invalidate();
}

void CDlgBarcodeMarkContent::OnBnClickedButtonPlaceholderInfo()
{
	CDlgPlaceholderSyntaxInfo dlg;
	dlg.DoModal();
}
