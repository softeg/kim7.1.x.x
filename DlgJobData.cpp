// DlgJobData.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgJobData.h"
#include "ProdDataGraphicOverView.h"
#include "PageListFrame.h"
#include "PageListView.h"
#include "OrderDataFrame.h"
#include "OrderDataView.h"
#include "GraphicComponent.h"

#define ID_SELECT_SUBDLG 9999


// CDlgJobData-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgJobData, CDialog)

CDlgJobData::CDlgJobData(int nInitialDlgID, int nProductType, ULONG_PTR initialData, CWnd* pParent /*=NULL*/, BOOL bWizardMode)
	: CDialog(CDlgJobData::IDD, pParent)
{
	m_nDlgID	  = nInitialDlgID;
	m_initialData = initialData;
	m_font.CreateFont (12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_bWizardMode = bWizardMode;
	m_bFinishEnabled = FALSE;
}

CDlgJobData::~CDlgJobData()
{
	m_font.DeleteObject();
}

void CDlgJobData::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

void CDlgJobData::DrawItemSubDlgSelector(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState)
{
	CDC*	pDC		= CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect	rcItem	= lpDrawItemStruct->rcItem;
	CWnd*	pWnd	= CWnd::FromHandle(lpDrawItemStruct->hwndItem);
	CString	strText;
	if (pWnd)
		pWnd->GetWindowText(strText);
	CDlgJobData* pDlg = (CDlgJobData*)( (pWnd) ? pWnd->GetParent() : NULL);

	CImage img;
	switch ((int)lpDrawItemStruct->itemData)
	{
	case IDD_ORDERDATA:			img.LoadFromResource(theApp.m_hInstance, IDB_JOB);			break;
	default:					break;
	}

	CRect rcImg;
	CRect rcText = rcItem; rcText.left += 5;
	if ( ! img.IsNull())
	{
		rcImg.left = rcItem.left + 10; rcImg.top = rcItem.CenterPoint().y - img.GetHeight()/2; rcImg.right = rcImg.left + img.GetWidth(); rcImg.bottom = rcImg.top + img.GetHeight();
		img.TransparentBlt(lpDrawItemStruct->hDC, rcImg, RGB(192,192,192));
		rcText.left += rcImg.right;
	}

	if (pDlg)
		pDC->SelectObject(pDlg->m_font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor((nState == ODB_DISABLED) ? LIGHTGRAY : GUICOLOR_DLG_STATIC_TEXT);
	pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
}


BEGIN_MESSAGE_MAP(CDlgJobData, CDialog)
	ON_COMMAND(ID_SELECT_SUBDLG, OnSelectSubDialog)
	ON_UPDATE_COMMAND_UI(ID_SELECT_SUBDLG,  OnUpdateSelectSubDialog)
	ON_BN_CLICKED(IDC_PREV_BUTTON, &CDlgJobData::OnBnClickedPrevButton)
	ON_BN_CLICKED(IDCANCEL, &CDlgJobData::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_JD_SETTINGS, &CDlgJobData::OnBnClickedJdProgramSettings)
	ON_UPDATE_COMMAND_UI(IDOK,  OnUpdateOK)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
END_MESSAGE_MAP()


// CDlgJobData-Meldungshandler

BOOL CDlgJobData::OnInitDialog()
{
	CDialog::OnInitDialog();

	theApp.UndoSave(&m_memFile);

	m_subDlgSelectorFrame.SubclassDlgItem(IDC_SUBDLG_SELECTOR, this);

	m_subDlgSelectorButtons.Create(this, this, DrawItemSubDlgSelector);

	if ( ! m_bWizardMode)
		GetDlgItem(IDOK)->SetWindowText(_T("OK"));

	InitSubDlgSelectorButtons();

	OpenSubDialog();

	UpdateToolBarStatus();

	UpdateData(FALSE);

	m_orderDataPage.GetDlgItem(IDC_OD_TITLE)->SetFocus();
	((CEdit*)m_orderDataPage.GetDlgItem(IDC_OD_TITLE))->SetSel(0, -1);

	return FALSE;// TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

BOOL CDlgJobData::QueryJobShowDetails()
{
	const int nValueLenght = _MAX_PATH + 2;
	TCHAR	  szValue[nValueLenght];
	DWORD	  dwSize = sizeof(szValue);
	HKEY	  hKey;
	CString   strKey = theApp.m_strRegistryKey;
	DWORD	  dwDisposition; 
	long ret = RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition); 
	if (RegQueryValueEx(hKey, _T("JobShowDetails"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		return (_tcscmp(szValue, _T("TRUE")) == 0) ? TRUE : FALSE;

	RegCloseKey(hKey);

	return TRUE;
}

void CDlgJobData::InitSubDlgSelectorButtons()
{
	CString strOut;
	CRect	rcButtonFrame;
	m_subDlgSelectorFrame.GetWindowRect(rcButtonFrame);
	ScreenToClient(rcButtonFrame);
	rcButtonFrame.right = rcButtonFrame.left + 170; rcButtonFrame.DeflateRect(0, 2);

	m_subDlgSelectorButtons.RemoveAll();

	//strOut.LoadString(IDS_COMMON);				
	//m_subDlgSelectorButtons.AddButton(strOut, _T(""),	ID_SELECT_SUBDLG, rcButtonFrame, -1, (ULONG_PTR)IDD_ORDERDATA,					FALSE);
	//rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);

	if (m_nDlgID != IDD_ORDERDATA)
	{
	}

	m_subDlgSelectorButtons.SelectButton((ULONG_PTR)m_nDlgID);
}

void CDlgJobData::UpdateToolBarStatus()
{
	if (m_bWizardMode)
	{
		BOOL bAllVisited = TRUE;
		for (int i = 0; i < m_subDlgSelectorButtons.GetSize(); i++)
		{
			CDialog* pDlg = GetSubDialog((int)m_subDlgSelectorButtons[i]->m_itemData);
			m_subDlgSelectorButtons[i]->Enable(FALSE);
			if (pDlg->m_hWnd)
				m_subDlgSelectorButtons[i]->Enable(TRUE);
			else
				bAllVisited = FALSE;
		}
		m_subDlgSelectorButtons.Redraw();

		m_bFinishEnabled = TRUE;//(bAllVisited && (m_nDlgID == IDD_PRODUCTIONPLANNER)) ? TRUE : FALSE;
		CString string;
		if (m_bFinishEnabled) 
			string.LoadString(IDS_FINISHED);
		else
		{
			string.LoadString(IDS_NAV_NEXT); string += _T(" >");
		}
		GetDlgItem(IDOK)->SetWindowText(string);
	}
	else
	{
		GetDlgItem(IDC_JD_PREV_BUTTON)->ShowWindow(SW_HIDE);
		// move OK button to prev_button and CANCEL button to ok_button pos
		CRect rcOKButtonPos, rcCancelButtonPos;
		GetDlgItem(IDC_JD_PREV_BUTTON)->GetWindowRect(rcOKButtonPos);	ScreenToClient(rcOKButtonPos);
		GetDlgItem(IDOK)->GetWindowRect(rcCancelButtonPos);				ScreenToClient(rcCancelButtonPos);
		GetDlgItem(IDOK)->MoveWindow(rcOKButtonPos);
		GetDlgItem(IDCANCEL)->MoveWindow(rcCancelButtonPos);
	}
}

void CDlgJobData::OnSelectSubDialog()
{
	CloseSubDialog();
	m_nDlgID = (ULONG_PTR)m_subDlgSelectorButtons.GetSelectedButtonData();
	OpenSubDialog();
}

void CDlgJobData::OnUpdateSelectSubDialog(CCmdUI* pCmdUI)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! pCmdUI->m_pOther)
		return;

	COwnerDrawnButton* pButton = (COwnerDrawnButton*)CWnd::FromHandle(pCmdUI->m_pOther->m_hWnd);
	CDialog* pDlg = GetSubDialog((int)pButton->m_itemData);

	((COwnerDrawnButton*)pButton)->SetCheck( ((int)pButton->m_itemData == m_nDlgID) ? BST_CHECKED : 0);

	if ( ! m_bWizardMode)
		return;
}

CDialog* CDlgJobData::GetSubDialog(int nID)
{
	switch (nID)
	{
	case IDD_ORDERDATA:					return &m_orderDataPage;
	//case IDD_PRODUCTDATA:				return &m_productDataPage;
	//case IDD_PRODUCTIONPLANNER:			return &m_productionPlannerPage;
	default:							return NULL;
	}
}

void CDlgJobData::OpenSubDialog()
{
	CRect rcFrame, rcDlg;
	GetDlgItem(IDC_SUBDLG_FRAME)->GetWindowRect(rcFrame);
	ScreenToClient(rcFrame);

	CDialog* pDlg = GetSubDialog(m_nDlgID);
	if ( ! pDlg)
		return;
	
	if (pDlg->m_hWnd)
		pDlg->ShowWindow(SW_SHOW);

	switch (m_nDlgID)
	{
	case IDD_ORDERDATA:			if (pDlg->m_hWnd)
									m_orderDataPage.InitData();
								break;
	//case IDD_PRODUCTDATA:		if (pDlg->m_hWnd)
	//								m_productDataPage.InitData();
	//							else
	//							{
	//								m_productDataPage.m_dlgProductGroupBound.m_nProductPartIndex   = (int)m_initialData;
	//							}
	//							break;
	//case IDD_PRODUCTIONPLANNER:	if (pDlg->m_hWnd)
	//								m_productionPlannerPage.InitData();
	//							else
	//								m_productionPlannerPage.m_nProductPartIndex = (m_initialData != -1) ? (int)m_initialData : 0;
	//							break;
	default:					break;
	}

	//m_initialData = (ULONG_PTR)-1;

	if ( ! pDlg->m_hWnd)
	{
		pDlg->Create(m_nDlgID, this);
		pDlg->GetWindowRect(rcDlg);
		pDlg->SetWindowPos(NULL, rcFrame.left, rcFrame.top, rcDlg.Width(), rcDlg.Height(), SWP_NOZORDER);
	}
}

void CDlgJobData::CloseSubDialog()
{
	switch (m_nDlgID)
	{
	case IDD_ORDERDATA:			m_orderDataPage.SaveData();	
								m_orderDataPage.ShowWindow(SW_HIDE);
								break;
	//case IDD_PRODUCTDATA:		m_productDataPage.SaveData();	
	//							m_productDataPage.ShowWindow(SW_HIDE);
	//							break;
	default:					break;
	}
}

void CDlgJobData::OnBnClickedJdProgramSettings()
{
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MAINFRAME));
	CMenu* pPopup = menu.GetSubMenu(2)->GetSubMenu(0);
	if ( ! pPopup)
		return;

    MENUITEMINFO mi;
    mi.cbSize = sizeof(MENUITEMINFO);
    mi.fMask = MIIM_TYPE | MIIM_DATA;
    mi.fType = MFT_OWNERDRAW;	//     Making the menu item ownerdrawn:
	mi.dwItemData = (ULONG_PTR)menu.m_hMenu;
	pPopup->SetMenuItemInfo(ID_SETTINGS_PROGRAM,		&mi);
	pPopup->SetMenuItemInfo(ID_SETTINGS_PRESSDEVICE,	&mi);
	pPopup->SetMenuItemInfo(ID_IMPOSE,					&mi);
	pPopup->SetMenuItemInfo(ID_REFERENCE_COLOR,		&mi);

	CRect rcClient;
	GetDlgItem(IDC_JD_SETTINGS)->GetWindowRect(rcClient);

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rcClient.TopLeft().x, rcClient.bottom, this);
}

void CDlgJobData::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct->CtlType == ODT_MENU)
	{
		int	nMaxLen = 0;
		if(IsMenu((HMENU)lpMeasureItemStruct->itemData))
		{
			CMenu* pMenu = CMenu::FromHandle((HMENU)lpMeasureItemStruct->itemData);
			if (pMenu)
			{
				CString strText;
				pMenu->GetMenuString(lpMeasureItemStruct->itemID, strText, MF_BYCOMMAND);
				CClientDC dc(this);
				dc.SelectObject(GetFont());
				nMaxLen = dc.GetTextExtent(strText).cx + 40;
			}
		}
		lpMeasureItemStruct->itemWidth  = nMaxLen;
		lpMeasureItemStruct->itemHeight = 20;
	}
	else
		CDialog::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CDlgJobData::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType == ODT_MENU)
	{
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		CRect rcItem = lpDrawItemStruct->rcItem;

		if ( ! (lpDrawItemStruct->itemState & ODS_SELECTED) )
			pDC->FillSolidRect(rcItem, RGB(252, 252, 249));//CREME);

		CRect rcIcon = rcItem;	rcIcon.right = rcIcon.left + 20;
		Graphics graphics(lpDrawItemStruct->hDC);
		Rect rc(rcIcon.left, rcIcon.top, rcIcon.Width(), rcIcon.Height());
		LinearGradientBrush br(rc, Color::Color(255,245,210), Color::Color(255,180,100), 0);
		graphics.FillRectangle(&br, rc);

		pDC->SelectObject(GetFont());
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(DARKBLUE);
		CRect rcText = rcItem;
		rcText.left = rcIcon.right + 10;
		if(IsMenu((HMENU)lpDrawItemStruct->hwndItem))
		{
			CMenu* pMenu = CMenu::FromHandle((HMENU)lpDrawItemStruct->hwndItem);
			if (pMenu)
			{
				CString strText; 
				pMenu->GetMenuString(lpDrawItemStruct->itemID, strText, MF_BYCOMMAND);
				pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
			}
		}
		if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			CGraphicComponent gc;
			gc.DrawTransparentFrame(pDC, rcItem, RGB(193,210,238));
		}				
		return;
	}

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CDlgJobData::OnBnClickedPrevButton()
{
	int nButtonIndex = m_subDlgSelectorButtons.GetSelection();
	if (nButtonIndex > 0)
	{
		nButtonIndex--;
		m_subDlgSelectorButtons.SelectButton(nButtonIndex);
		OnSelectSubDialog();
		m_subDlgSelectorButtons.Redraw();
	}
	UpdateToolBarStatus();
}

void CDlgJobData::OnOK()
{
	if (m_bWizardMode)
	{
		if ( ! m_bFinishEnabled)
		{
			int nButtonIndex = m_subDlgSelectorButtons.GetSelection();
			if (nButtonIndex < m_subDlgSelectorButtons.GetSize() - 1)
			{
				nButtonIndex++;
				m_subDlgSelectorButtons.SelectButton(nButtonIndex);
				OnSelectSubDialog();
				m_subDlgSelectorButtons.Redraw();
			}
			UpdateToolBarStatus();
			return;
		}
	}

	CloseSubDialog();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	//if (m_bWizardMode)
	//{
	//	for (int nProductPartIndex = 0; nProductPartIndex < pDoc->m_printGroupList.GetSize(); nProductPartIndex++)
	//	{
	//		pDoc->m_PrintSheetList.AutoImposeProductGroup(nProductPartIndex, theApp.m_PressDeviceList.GetDeviceIndex(pDoc->m_printGroupList[nProductPartIndex].m_strDefaultPressDevice),
	//													 pDoc->m_printGroupList[nProductPartIndex].m_strDefaultOutputProfile,
	//													 pDoc->m_printGroupList[nProductPartIndex].GetProductionProfile().m_strMarkset);
	//	}
	//}

	COrderDataView*  pOrderDataView  = (COrderDataView*)theApp.GetView(RUNTIME_CLASS(COrderDataView));
	COrderDataFrame* pOrderDataFrame = (COrderDataFrame*)((pOrderDataView) ? pOrderDataView->GetParentFrame() : NULL);
	if (pOrderDataFrame)
		pOrderDataFrame->RearrangeSplitters();

	pDoc->SetModifiedFlag();
	pDoc->UpdateAllViews(NULL);

	CDialog::OnOK();
}

void CDlgJobData::OnUpdateOK(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);

	//if (m_nDlgID == IDD_PRODUCTDATA)
	//{
	//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//	if (pDoc)
	//	{
	//		if (m_bWizardMode)
	//		{
	//			CString string;
	//			//if ( ! pDoc->m_printGroupList.HasBoundedProductGroup()) // no bounded part -> no production intents
	//			//{
	//			//	string.LoadString(IDS_FINISHED);
	//			//	m_bFinishEnabled = TRUE;
	//			//}
	//			//else
	//			{
	//				string.LoadString(IDS_NAV_NEXT); string += _T(" >");
	//				m_bFinishEnabled = FALSE;
	//			}
	//			GetDlgItem(IDOK)->SetWindowText(string);
	//		}
	//	}
	//}
}

void CDlgJobData::OnBnClickedCancel()
{
	CDialog::OnCancel();

	theApp.UndoRestore(&m_memFile);
}

BOOL CDlgJobData::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );
	return CDialog::PreTranslateMessage(pMsg);
}
