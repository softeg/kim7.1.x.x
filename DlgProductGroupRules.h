#pragma once
#include "afxcmn.h"


// CDlgProductGroupRules-Dialogfeld

class CDlgProductGroupRules : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductGroupRules)

public:
	CDlgProductGroupRules(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductGroupRules();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTGROUP_RULES };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CSpinButtonCtrl m_printingDuedateSpin;
};
