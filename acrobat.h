// Von Klassen-Assistent automatisch erstellte IDispatch-Kapselungsklasse(n). 
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroApp 

class CAcroApp : public COleDispatchDriver
{
public:
	CAcroApp() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroApp(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroApp(const CAcroApp& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	BOOL Exit();
	BOOL Hide();
	BOOL Show();
	BOOL CloseAllDocs();
	BOOL MenuItemExecute(LPCTSTR szMenuItemName);
	CString GetActiveTool();
	BOOL SetActiveTool(LPCTSTR szButtonName, BOOL bPersistent);
	BOOL ToolButtonRemove(LPCTSTR szButtonName);
	BOOL ToolButtonIsEnabled(LPCTSTR szButtonName);
	BOOL MenuItemRemove(LPCTSTR szMenuItemName);
	BOOL MenuItemIsEnabled(LPCTSTR szMenuItemName);
	BOOL MenuItemIsMarked(LPCTSTR szMenuItemName);
	long GetNumAVDocs();
	LPDISPATCH GetAVDoc(long nIndex);
	LPDISPATCH GetActiveDoc();
	CString GetLanguage();
	BOOL Lock(LPCTSTR szLockedBy);
	BOOL Unlock();
	BOOL SetPreference(short nType, long nValue);
	long GetPreference(short nType);
	BOOL Maximize(BOOL bMaxSize);
	BOOL SetFrame(LPDISPATCH iAcroRect);
	LPDISPATCH GetFrame();
	BOOL Minimize(BOOL bMinimize);
	BOOL Restore(BOOL bRestore);
	BOOL UnlockEx(LPCTSTR szLockedBy);
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroPDDoc 

class CAcroPDDoc : public COleDispatchDriver
{
public:
	CAcroPDDoc() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroPDDoc(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroPDDoc(const CAcroPDDoc& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	BOOL Open(LPCTSTR szFullPath);
	BOOL Close();
	BOOL InsertPages(long nInsertPageAfter, LPDISPATCH iPDDocSource, long nStartPage, long nNumPages, BOOL bBookmarks);
	BOOL ReplacePages(long nStartPage, LPDISPATCH iPDDocSource, long nStartSourcePage, long nNumPages, BOOL bMergeTextAnnotations);
	BOOL DeletePages(long nStartPage, long nEndPage);
	long GetNumPages();
	BOOL Create();
	CString GetInfo(LPCTSTR szInfoKey);
	BOOL SetInfo(LPCTSTR szInfoKey, LPCTSTR szBuffer);
	BOOL DeleteThumbs(long nStartPage, long nEndPage);
	BOOL MovePage(long nMoveAfterThisPage, long nPageToMove);
	CString GetFileName();
	long GetPageMode();
	BOOL SetPageMode(long nPageMode);
	BOOL CreateThumbs(long nFirstPage, long nLastPage);
	LPDISPATCH CreateTextSelect(long nPage, LPDISPATCH iAcroRect);
	LPDISPATCH AcquirePage(long nPage);
	CString GetInstanceID();
	CString GetPermanentID();
	long GetFlags();
	BOOL SetFlags(long nFlags);
	LPDISPATCH OpenAVDoc(LPCTSTR szTempTitle);
	BOOL Save(short nType, LPCTSTR szFullPath);
	BOOL ClearFlags(long nFlags);
	BOOL SetOpenInfo(long pgNum, short viewMode, LPCTSTR magnification);
	BOOL CropPages(long nStartPage, long nEndPage, short nOddOrEvenPagesOnly, LPDISPATCH iAcroRect);
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroAVDoc 

class CAcroAVDoc : public COleDispatchDriver
{
public:
	CAcroAVDoc() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroAVDoc(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroAVDoc(const CAcroAVDoc& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	BOOL Open(LPCTSTR szFullPath, LPCTSTR szTempTitle);
	LPDISPATCH GetPDDoc();
	LPDISPATCH GetAVPageView();
	BOOL SetViewMode(long nType);
	BOOL FindText(LPCTSTR szText, BOOL bCaseSensitive, BOOL bWholeWordsOnly, BOOL bReset);
	BOOL Close(BOOL bNoSave);
	long GetViewMode();
	BOOL PrintPages(long nFirstPage, long nLastPage, long nPSLevel, BOOL bBinaryOk, BOOL bShrinkToFit);
	BOOL ClearSelection();
	BOOL BringToFront();
	CString GetTitle();
	BOOL Maximize(BOOL bMaxSize);
	BOOL SetTitle(LPCTSTR szTitle);
	BOOL OpenInWindow(LPCTSTR szFullPath, short hWnd);
	BOOL SetTextSelection(LPDISPATCH iAcroPDTextSelect);
	BOOL ShowTextSelect();
	BOOL SetFrame(LPDISPATCH iAcroRect);
	LPDISPATCH GetFrame();
	BOOL IsValid();
	BOOL OpenInWindowEx(LPCTSTR szFullPath, long hWnd, long openFlgs, BOOL UseOpenParams, long pgNum, short pageMode, short ZoomType, long Zoom, short Top, short Left);
	BOOL PrintPagesSilent(long nFirstPage, long nLastPage, long nPSLevel, BOOL bBinaryOk, BOOL bShrinkToFit);
	BOOL PrintPagesEx(long nFirstPage, long nLastPage, long nPSLevel, BOOL bBinaryOk, BOOL bShrinkToFit, BOOL bReverse, BOOL bFarEastFontOpt, BOOL bEmitHalftones, long iPageOption);
	BOOL PrintPagesSilentEx(long nFirstPage, long nLastPage, long nPSLevel, BOOL bBinaryOk, BOOL bShrinkToFit, BOOL bReverse, BOOL bFarEastFontOpt, BOOL bEmitHalftones, long iPageOption);
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroAVPageView 

class CAcroAVPageView : public COleDispatchDriver
{
public:
	CAcroAVPageView() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroAVPageView(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroAVPageView(const CAcroAVPageView& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	BOOL GoTo(long nPage);
	BOOL ZoomTo(short nType, short nScale);
	BOOL ScrollTo(short nX, short nY);
	BOOL ReadPageUp();
	BOOL ReadPageDown();
	BOOL DoGoBack();
	BOOL DoGoForward();
	LPDISPATCH GetAVDoc();
	LPDISPATCH GetPage();
	LPDISPATCH GetDoc();
	long GetZoom();
	short GetZoomType();
	long GetPageNum();
	LPDISPATCH PointToDevice(LPDISPATCH iAcroPoint);
	LPDISPATCH DevicePointToPage(LPDISPATCH iAcroPoint);
	LPDISPATCH GetAperture();
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroPDPage 

class CAcroPDPage : public COleDispatchDriver
{
public:
	CAcroPDPage() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroPDPage(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroPDPage(const CAcroPDPage& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	LPDISPATCH GetSize();
	LPDISPATCH GetAnnot(long nIndex);
	LPDISPATCH AddNewAnnot(long nIndexAddAfter, LPCTSTR szSubType, LPDISPATCH iAcroRect);
	BOOL AddAnnot(long nIndexAddAfter, LPDISPATCH i);
	BOOL RemoveAnnot(long nIndex);
	long GetAnnotIndex(LPDISPATCH i);
	long GetNumAnnots();
	LPDISPATCH CreatePageHilite(LPDISPATCH iAcroHiliteList);
	LPDISPATCH CreateWordHilite(LPDISPATCH iAcroHiliteList);
	LPDISPATCH GetDoc();
	long GetNumber();
	BOOL Draw(short nHWND, short nHDC, short nXOrigin, short nYOrigin, short nZoom);
	short GetRotate();
	BOOL SetRotate(short nRotate);
	BOOL DrawEx(long nHWND, long nHDC, LPDISPATCH updateRect, short nXOrigin, short nYOrigin, short nZoom);
	BOOL CopyToClipboard(LPDISPATCH boundRect, short nXOrigin, short nYOrigin, short nZoom);
	BOOL CropPage(LPDISPATCH iAcroRect);
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroPDAnnot 

class CAcroPDAnnot : public COleDispatchDriver
{
public:
	CAcroPDAnnot() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroPDAnnot(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroPDAnnot(const CAcroPDAnnot& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	BOOL IsValid();
	CString GetSubtype();
	BOOL IsEqual(LPDISPATCH PDAnnot);
	long GetColor();
	BOOL SetColor(long nRGBColor);
	CString GetTitle();
	BOOL SetTitle(LPCTSTR szTitle);
	CString GetContents();
	BOOL SetContents(LPCTSTR szContents);
	BOOL IsOpen();
	BOOL SetOpen(BOOL bIsOpen);
	LPDISPATCH GetRect();
	BOOL SetRect(LPDISPATCH iAcroRect);
	LPDISPATCH GetDate();
	BOOL SetDate(LPDISPATCH iAcroTime);
	BOOL Perform(LPDISPATCH iAcroAVDoc);
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroPoint 

class CAcroPoint : public COleDispatchDriver
{
public:
	CAcroPoint() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroPoint(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroPoint(const CAcroPoint& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:
	short GetX();
	void SetX(short);
	short GetY();
	void SetY(short);

// Operationen
public:
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroRect 

class CAcroRect : public COleDispatchDriver
{
public:
	CAcroRect() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroRect(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroRect(const CAcroRect& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:
	short GetLeft();
	void SetLeft(short);
	short GetTop();
	void SetTop(short);
	short GetRight();
	void SetRight(short);
	short GetBottom();
	void SetBottom(short);

// Operationen
public:
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroTime 

class CAcroTime : public COleDispatchDriver
{
public:
	CAcroTime() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroTime(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroTime(const CAcroTime& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:
	short GetYear();
	void SetYear(short);
	short GetMonth();
	void SetMonth(short);
	short GetDate();
	void SetDate(short);
	short GetHour();
	void SetHour(short);
	short GetMinute();
	void SetMinute(short);
	short GetSecond();
	void SetSecond(short);
	short GetMillisecond();
	void SetMillisecond(short);
	short GetDay();
	void SetDay(short);

// Operationen
public:
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroPDTextSelect 

class CAcroPDTextSelect : public COleDispatchDriver
{
public:
	CAcroPDTextSelect() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroPDTextSelect(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroPDTextSelect(const CAcroPDTextSelect& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	BOOL Destroy();
	long GetNumText();
	LPDISPATCH GetBoundingRect();
	long GetPage();
	CString GetText(long nTextIndex);
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroHiliteList 

class CAcroHiliteList : public COleDispatchDriver
{
public:
	CAcroHiliteList() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroHiliteList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroHiliteList(const CAcroHiliteList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	BOOL Add(short nOffset, short nLength);
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CAcroPDBookmark 

class CAcroPDBookmark : public COleDispatchDriver
{
public:
	CAcroPDBookmark() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	CAcroPDBookmark(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAcroPDBookmark(const CAcroPDBookmark& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	BOOL GetByTitle(LPDISPATCH iAcroPDDoc, LPCTSTR szBookmarkTitle);
	BOOL Destroy();
	BOOL IsValid();
	CString GetTitle();
	BOOL SetTitle(LPCTSTR szNewTitle);
	BOOL Perform(LPDISPATCH iAcroAVDoc);
};
