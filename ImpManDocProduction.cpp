// ImpManDocProduction.cpp : implementation of the CImpManDoc class
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetPlanningView.h"
#include "DlgPressDevice.h"
#include "DlgOptimizationMonitor.h"
#include "GraphicComponent.h"
#include "ExpressionParser.h"
#include "PageListView.h"




/////////////////////////////////////////////////////////////////////////////
// CPrintingProfiles

IMPLEMENT_SERIAL(CPrintingProfiles, CObject, VERSIONABLE_SCHEMA | 1)

CPrintingProfiles::CPrintingProfiles()
{
}

CPrintingProfiles::~CPrintingProfiles()
{
}

void CPrintingProfiles::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrintingProfiles));

	CList<CPrintingProfile, CPrintingProfile&>::Serialize(ar);
}

CPrintingProfile& CPrintingProfiles::GetPrintingProfile(int nID)
{
	static CPrintingProfile nullPrintingProfile;

	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPrintingProfile& rPrintingProfile = GetNext(pos);
		if (nIndex == nID)	
			return rPrintingProfile;
		nIndex++;
	}

	return nullPrintingProfile;
}

CPrintingProfile& CPrintingProfiles::FindProfile(CString strProfileName)
{
	static CPrintingProfile nullProfile;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintingProfile& rPrintingProfile = GetNext(pos);
		if (rPrintingProfile.m_strName.CompareNoCase(strProfileName) == 0)
			return rPrintingProfile;
	}

	return nullProfile;
}

void CPrintingProfiles::RemoveProfile(CString strProfileName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintingProfile& rPrintingProfile = GetAt(pos);
		if (rPrintingProfile.m_strName.CompareNoCase(strProfileName) == 0)
		{
			RemoveAt(pos);
			return;
		}
		GetNext(pos);
	}
}



/////////////////////////////////////////////////////////////////////////////
// CPrintingProfile

IMPLEMENT_SERIAL(CPrintingProfile, CObject, VERSIONABLE_SCHEMA | 1)

CPrintingProfile::CPrintingProfile()
{
	m_activeProcesses.RemoveAll();
	m_activeProcesses.Add(Printing);	// printing always needed

	m_press.m_pressDevice.m_fPlateWidth		= 450.0f;
	m_press.m_pressDevice.m_fPlateHeight	= 350.0f;
	m_press.m_sheetData.m_fWidth			= 450.0f;
	m_press.m_sheetData.m_fHeight			= 350.0f;
}

CPrintingProfile::CPrintingProfile(const CPrintingProfile& rPrintingProfile)
{
	Copy(rPrintingProfile);	
}

const CPrintingProfile& CPrintingProfile::operator=(const CPrintingProfile& rPrintingProfile)
{
	Copy(rPrintingProfile);	

	return *this;
}

BOOL CPrintingProfile::operator==(const CPrintingProfile& rPrintingProfile)
{
	if (m_strName		  != rPrintingProfile.m_strName)			return FALSE;
	if (m_strComment	  != rPrintingProfile.m_strComment)			return FALSE;
	if (m_nProductionType != rPrintingProfile.m_nProductionType)	return FALSE;
	if (m_strMarkset	  != rPrintingProfile.m_strMarkset)			return FALSE;
	if (m_layoutPool	  != rPrintingProfile.m_layoutPool)			return FALSE;

	if (m_prepress		  != rPrintingProfile.m_prepress)			return FALSE;
	if (m_press			  != rPrintingProfile.m_press)				return FALSE;
	if (m_cutting		  != rPrintingProfile.m_cutting)			return FALSE;

	//if (m_activeProcesses.GetSize() != rPrintingProfile.m_activeProcesses.GetSize()) return FALSE;
	//for (int i = 0; i < m_activeProcesses.GetSize(); i++)
	//	if (m_activeProcesses[i] != rPrintingProfile.m_activeProcesses[i]) return FALSE;

	return TRUE;
}

BOOL CPrintingProfile::operator!=(const CPrintingProfile& rPrintingProfile)
{
	return (*this == rPrintingProfile) ? FALSE : TRUE;
}

void CPrintingProfile::Copy(const CPrintingProfile& rPrintingProfile)
{
	m_strName		  = rPrintingProfile.m_strName;
	m_strComment	  = rPrintingProfile.m_strComment;
	m_nProductionType = rPrintingProfile.m_nProductionType;
	m_strMarkset	  = rPrintingProfile.m_strMarkset;
	m_layoutPool	  = rPrintingProfile.m_layoutPool;

	m_prepress		  = rPrintingProfile.m_prepress;
	m_press			  = rPrintingProfile.m_press;
	m_cutting		  = rPrintingProfile.m_cutting;

	m_activeProcesses.RemoveAll();
	m_activeProcesses.Append(rPrintingProfile.m_activeProcesses);
}

int	CPrintingProfile::GetIndex()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int		 nIndex = 0;
	POSITION pos	= pDoc->m_printingProfiles.GetHeadPosition();
	while (pos)
	{
		if (&pDoc->m_printingProfiles.GetNext(pos) == this)
			return nIndex;
		nIndex++;
	}

	return -1;
}

CLayout* CPrintingProfile::GetLayout()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int		 nIndex = GetIndex();
	POSITION pos	= pDoc->m_PrintSheetList.m_Layouts.FindIndex(GetIndex());
	if (pos)
		return &pDoc->m_PrintSheetList.m_Layouts.GetAt(pos);
	else
		return NULL;
}

void CPrintingProfile::AssignProductionProfilePool(CProductionProfile& rProductionProfile)
{
	m_strName				= rProductionProfile.m_strName;
	m_strComment			= rProductionProfile.m_strComment;
	m_nProductionType		= rProductionProfile.m_nProductionType;
	m_layoutPool			= rProductionProfile.m_layoutPool;
	m_prepress				= (rProductionProfile.ProcessIsActive(CProductionProfile::Prepress)) ? rProductionProfile.m_prepress			: CPrepressParams();
	m_press					= (rProductionProfile.ProcessIsActive(CProductionProfile::Printing)) ? rProductionProfile.m_press				: CPressParams();
//	m_press.m_strMarkset	= rProductionProfile.m_strMarkset;
	m_cutting				= (rProductionProfile.ProcessIsActive(CProductionProfile::Cutting))  ? rProductionProfile.m_postpress.m_cutting	: CCuttingProcess();
	m_activeProcesses.RemoveAll();
	m_activeProcesses.Append(rProductionProfile.m_activeProcesses);
}

BOOL CPrintingProfile::ApplyModifications(CPrintingProfile& rOldProfile)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL	 bDoReImpose = FALSE;
	BOOL	 bChanged	 = FALSE; 
	CLayout* pLayout	 = GetLayout();

	if (pLayout)
	{
		if (m_prepress.ApplyModifications(pLayout, rOldProfile.m_prepress, bDoReImpose))
			bChanged = TRUE;

		if (m_press.ApplyModifications(pLayout, rOldProfile.m_press.m_pressDevice, bDoReImpose))
			bChanged = TRUE;

		if (m_press.m_strMarkset != rOldProfile.m_strMarkset)
		{
			CString strMarksDir		= CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets");
			CString strRelativePath	= m_press.m_strMarkset;	
			strRelativePath.TrimLeft('.');
			CString strMarksetFullPath = strMarksDir + strRelativePath;
			CMarkList markList;
			markList.Load(strMarksetFullPath);
			pLayout->m_markList.Append(markList);
			pLayout->AnalyzeMarks();
			bChanged = TRUE;
		}

		if (m_cutting.ApplyModifications(pLayout, rOldProfile.m_cutting, bDoReImpose))
			bChanged = TRUE;
	}

	return bChanged;
}

BOOL CPrintingProfile::ProcessIsActive(int nProcess)
{
	if (IsNull())
		return TRUE;

	for (int i = 0; i < m_activeProcesses.GetSize(); i++)
	{
		if (m_activeProcesses[i] == nProcess)
			return TRUE;
	}
	return FALSE;
}

CRect CPrintingProfile::DrawInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CLayout*	 pLayout	 = GetLayout();
	CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
	CPrintGroup& rPrintGroup = pDoc->m_printGroupList.GetPrintGroup(GetIndex());

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);
	
	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT);

	rcFrame.left += 10; rcFrame.top += 5;

	pDC->SetTextColor(BLACK);

	BOOL bDrawLine = FALSE;

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;

	CString strOut = m_strName;
	if (IsNull())
		strOut.LoadString(IDS_SELECT_PROFILE);

	if ( ! strOut.IsEmpty() && (strOut != m_press.m_pressDevice.m_strName) )
	{
		CProductionProfile& rProductionProfile = theApp.m_productionProfiles.FindProfile(m_strName);
		CPrintingProfile sysProfile;
		sysProfile.m_strName			= rProductionProfile.m_strName;
		sysProfile.m_strComment			= rProductionProfile.m_strComment;
		sysProfile.m_nProductionType	= rProductionProfile.m_nProductionType;
		sysProfile.m_layoutPool			= rProductionProfile.m_layoutPool;
		sysProfile.m_prepress			= rProductionProfile.m_prepress;
		sysProfile.m_press				= rProductionProfile.m_press;
		//sysProfile.m_press.m_strMarkset	= rProductionProfile.m_strMarkset;
		sysProfile.m_cutting			= rProductionProfile.m_postpress.m_cutting;

		if ( ! sysProfile.IsNull())
		{
			sysProfile.m_press.m_sheetData = m_press.m_sheetData;	// switch to current sheet 
			if (sysProfile != *this)
				strOut += _T(" *");
		}
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
		rcText.OffsetRect(0, nTextHeight);
		bDrawLine = (IsNull()) ? FALSE : TRUE;
	}

	pDC->SelectObject(&font);

	rcText.bottom = rcFrame.bottom;

	if ( ! m_strComment.IsEmpty())
	{
		pDC->SetTextColor(DARKGRAY);
		DrawTextMeasured(pDC, m_strComment, rcText, DT_LEFT | DT_TOP | DT_END_ELLIPSIS, rcBox);
		bDrawLine = TRUE;
	}

	font.DeleteObject();
	boldFont.DeleteObject();

	int nDrawLineY = rcFrame.top = rcBox.bottom + 5;
	if (bDrawLine)
		rcFrame.top += 5;

	CRect rcRet;
	if (ProcessIsActive(CProductionProfile::Printing))
	{
		rcFrame.left -= 5;
		rcRet = m_press.DrawData(pDC, rcFrame, pLayout, NULL, TRUE);						
		//RegisterProcessData(pDC, rcRet, CProductionProfile::Printing);
		rcBox.UnionRect(rcBox, rcRet); 
	}
	//if (ProcessIsActive(CProductionProfile::Cutting) && rPrintGroup.HasFlatProducts())
	//{
	//	rcFrame.top = rcBox.bottom + 10;
	//	rcRet = m_postpress.m_cutting.DrawData(pDC, rcFrame, pPrintSheet, NULL);	
	//	//RegisterProcessData(pDC, rcRet, CProductionProfile::Cutting);
	//	rcBox.UnionRect(rcBox, rcRet); 
	//}
	//if (rPrintGroup.HasBoundProducts())//ProcessIsActive(CProductionProfile::Binding))
	//{
	//	rcFrame.top = rcBox.bottom + 10;
	//	rcRet = m_postpress.m_binding.DrawData(pDC, rcFrame, pPrintSheet, NULL);	
	//	//RegisterProcessData(pDC, rcRet, CProductionProfile::Binding);
	//	rcBox.UnionRect(rcBox, rcRet); 
	//}
	//if (rPrintGroup.HasBoundProducts())//ProcessIsActive(CProductionProfile::Trimming))
	//{
	//	rcFrame.top = rcBox.bottom + 10;
	//	rcRet = m_postpress.m_trimming.DrawData(pDC, rcFrame, pPrintSheet, NULL);	
	//	//RegisterProcessData(pDC, rcRet, CProductionProfile::Trimming);
	//	rcBox.UnionRect(rcBox, rcRet); 
	//}

	//rcBox.bottom = min(rcBox.bottom, rcFrame.bottom);

	//rcBox.right = rcFrame.right - 5;
	rcBox.right  += 5; 
	rcBox.bottom += 10;

	if (pDisplayList)
	{
		CRect rcDI = rcBox; 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PrintingProfile), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
			{
				CRect rcFeedback = pDI->m_BoundingRect; rcFeedback.left++; rcFeedback.top++; rcFeedback.right -=2; rcFeedback.bottom -= 2;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcFeedback, RGB(255,195,15));
			}
		}
	}

	if (bDrawLine)
	{
		CPen pen(PS_SOLID, 1, LIGHTGRAY);
		CPen* pOldPen = pDC->SelectObject(&pen);
		pDC->MoveTo(rcBox.left + 5, nDrawLineY); pDC->LineTo(rcBox.right - 5, nDrawLineY);
		pen.DeleteObject();
		pDC->SelectObject(pOldPen);
	}

	SetDisplayBox(rcBox);

	BOOL bIsEmpty = (pPrintSheet) ? (pPrintSheet->IsEmpty()) : TRUE;
	CGraphicComponent gc;
	gc.DrawFrame(pDC, rcBox, (bIsEmpty) ? RGB(255,180,180) : crBackground, TRUE);

	return rcBox;
}

void CPrintingProfile::DrawPlanningTableLinks(CDC* pDC, BOOL bHighlight, int& nVerticalAxis, int& nMaxSourceRight, CDisplayList* pDisplayList)
{
	//if (IsNull())
	//	return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPen  pen(PS_SOLID, 2, RGB(210,210,235));
	CPen* pOldPen = pDC->SelectObject(&pen);

	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return;

	BOOL		 bFirst		 = TRUE;
	CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		CRect rcSource = GetDisplayBox();
		CRect rcTarget = pPrintSheet->GetDisplayBoxSheet();

		if ( ! pPrintSheet->IsEmpty())
		{
			if (bFirst)
			{
				nVerticalAxis = (nVerticalAxis == 0) ? rcTarget.left - nMaxSourceRight - 30 : nVerticalAxis - 5;
				nVerticalAxis = max(nVerticalAxis, 30);	// not under 30
				bFirst = FALSE;
			}
			int nR = 15;
			if (rcSource.CenterPoint().y < rcTarget.CenterPoint().y)	// draw downward
			{
				nR = min(nR, (rcTarget.CenterPoint().y - rcSource.CenterPoint().y)/2);
				CPoint pt1(rcSource.right,				  rcSource.CenterPoint().y);
				CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
				CPoint pt3(pt2.x,		  rcTarget.CenterPoint().y);
				CPoint pt4(rcTarget.left, rcTarget.CenterPoint().y);
				CPoint cp1 = pt2 + CSize(-nR,  nR);
				CPoint cp2 = pt3 + CSize( nR, -nR);
				CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
				CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

				pDC->MoveTo(pt1);
				pDC->LineTo(pt2 - CSize(nR, 0));
				pDC->AngleArc(cp1.x, cp1.y, nR, 90, -90);
				pDC->LineTo(pt3 - CSize(0, nR));
				pDC->AngleArc(cp2.x, cp2.y, nR, 180, 90);
				pDC->LineTo(pt4);
			}
			else	// draw upward
			{
				nR = min(nR, (rcSource.CenterPoint().y - rcTarget.CenterPoint().y)/2);
				CPoint pt1(rcSource.right,				  rcSource.CenterPoint().y);
				CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
				CPoint pt3(pt2.x,		  rcTarget.CenterPoint().y);
				CPoint pt4(rcTarget.left, rcTarget.CenterPoint().y);
				CPoint cp1 = pt2 + CSize(-nR, -nR);
				CPoint cp2 = pt3 + CSize( nR,  nR);
				CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
				CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

				pDC->MoveTo(pt1);
				pDC->LineTo(pt2 - CSize(nR, 0));
				pDC->AngleArc(cp1.x, cp1.y, nR, -90,  90);
				pDC->LineTo(pt3 + CSize(0, nR));
				pDC->AngleArc(cp2.x, cp2.y, nR, 180, -90);
				pDC->LineTo(pt4);
			}

			//int nMaxTop	   = max(rcSource.top,	  rcTarget.top);
			//int nMinBottom = min(rcSource.bottom, rcTarget.bottom);

			//if (nMaxTop < nMinBottom)
			//{
			//	int nY = nMaxTop + (nMinBottom - nMaxTop)/2;
			//	pDC->MoveTo(rcSource.right, nY);
			//	pDC->LineTo(rcTarget.left,  nY);
			//}
			//else
			//{
			//	int nR = 15;
			//	if (rcSource.CenterPoint().y < rcTarget.CenterPoint().y)	// draw downward
			//	{
			//		CPoint pt1(rcSource.right,										rcSource.CenterPoint().y);
			//		CPoint pt2(rcSource.right + (rcTarget.left - rcSource.right)/2, rcSource.CenterPoint().y);
			//		CPoint pt3(pt2.x,		  rcTarget.CenterPoint().y);
			//		CPoint pt4(rcTarget.left, rcTarget.CenterPoint().y);
			//		CPoint cp1 = pt2 + CSize(-nR,  nR);
			//		CPoint cp2 = pt3 + CSize( nR, -nR);
			//		CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
			//		CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

			//		pDC->MoveTo(pt1);
			//		pDC->LineTo(pt2 - CSize(nR, 0));
			//		pDC->AngleArc(cp1.x, cp1.y, nR, 90, -90);
			//		pDC->LineTo(pt3 - CSize(0, nR));
			//		pDC->AngleArc(cp2.x, cp2.y, nR, 180, 90);
			//		pDC->LineTo(pt4);
			//	}
			//}
		}

		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();
}

// helper function for CPrintingProfile elements
template <> void AFXAPI SerializeElements <CPrintingProfile> (CArchive& ar, CPrintingProfile* pPrintingProfile, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrintingProfile));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPrintingProfile->m_strName;
			ar << pPrintingProfile->m_strComment;
			ar << pPrintingProfile->m_nProductionType;
			ar << pPrintingProfile->m_strMarkset;
			pPrintingProfile->m_layoutPool.Serialize(ar);
		}	
		else
		{ 
			switch (nVersion)
			{
			case 1:	ar >> pPrintingProfile->m_strName;
					ar >> pPrintingProfile->m_strComment;
					ar >> pPrintingProfile->m_nProductionType;
					ar >> pPrintingProfile->m_strMarkset;
					pPrintingProfile->m_layoutPool.Serialize(ar);
					break;
			}
		}

		pPrintingProfile->m_prepress.Serialize(ar);
		pPrintingProfile->m_press.Serialize(ar);
		pPrintingProfile->m_cutting.Serialize(ar);

		pPrintingProfile->m_activeProcesses.Serialize(ar);

		pPrintingProfile++;
	}
}



/////////////////////////////////////////////////////////////////////////////
// CProductionSetup

IMPLEMENT_SERIAL(CProductionSetup, CObject, VERSIONABLE_SCHEMA | 2)

CProductionSetup::CProductionSetup()
{
	m_strName = _T("");
	m_strComment = _T("");
	m_strProductionProfile = _T("");
	m_rules.RemoveAll();
	m_fMaxOverProduction = 0.0f;
	m_bOneTypePerSheet	 = FALSE;
	m_quantityRanges.RemoveAll();
}

CProductionSetup::CProductionSetup(const CProductionSetup& rProductionSetup)
{
	Copy(rProductionSetup);	
}

const CProductionSetup& CProductionSetup::operator=(const CProductionSetup& rProductionSetup)
{
	Copy(rProductionSetup);	

	return *this;
}

BOOL CProductionSetup::operator==(const CProductionSetup& rProductionSetup)
{
	if (m_strName					!= rProductionSetup.m_strName)					return FALSE;
	if (m_strComment				!= rProductionSetup.m_strComment)				return FALSE;
	if (m_strProductionProfile		!= rProductionSetup.m_strProductionProfile)		return FALSE;
	if (m_rules						!= rProductionSetup.m_rules)					return FALSE;
	if (m_fMaxOverProduction		!= rProductionSetup.m_fMaxOverProduction)		return FALSE;
	if (m_bOneTypePerSheet			!= rProductionSetup.m_bOneTypePerSheet)			return FALSE;
	if (m_quantityRanges.GetSize()	!= rProductionSetup.m_quantityRanges.GetSize())	return FALSE;

	for (int i = 0; i < m_quantityRanges.GetSize(); i++)
	{
		if (m_quantityRanges[i].nQuantityMin != rProductionSetup.m_quantityRanges[i].nQuantityMin)	return FALSE;
		if (m_quantityRanges[i].nQuantityMax != rProductionSetup.m_quantityRanges[i].nQuantityMax)	return FALSE;
	}

	return TRUE;
}

BOOL CProductionSetup::operator!=(const CProductionSetup& rProductionSetup)
{
	return (*this == rProductionSetup) ? FALSE : TRUE;
}

void CProductionSetup::Copy(const CProductionSetup& rProductionSetup)
{
	m_strName				= rProductionSetup.m_strName;
	m_strComment			= rProductionSetup.m_strComment;
	m_strProductionProfile	= rProductionSetup.m_strProductionProfile;
	m_rules					= rProductionSetup.m_rules;
	m_fMaxOverProduction	= rProductionSetup.m_fMaxOverProduction;
	m_bOneTypePerSheet		= rProductionSetup.m_bOneTypePerSheet;

	m_quantityRanges.RemoveAll();
	for (int i = 0; i < rProductionSetup.m_quantityRanges.GetSize(); i++)
	{
		QUANTITY_RANGE range = rProductionSetup.m_quantityRanges[i];
		m_quantityRanges.Add(range);
	}
}

void CProductionSetup::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductionSetup));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		ar << m_strName;
		ar << m_strComment;
		ar << m_strProductionProfile;
		m_rules.Serialize(ar);
		ar << m_fMaxOverProduction;
		ar << m_bOneTypePerSheet;
		m_quantityRanges.Serialize(ar);
	}	
	else
	{ 
		switch (nVersion)
		{
		case 1:	ar >> m_strName;
				ar >> m_strComment;
				ar >> m_strProductionProfile;
				m_rules.Serialize(ar);
				ar >> m_fMaxOverProduction;
				m_quantityRanges.Serialize(ar);
				break;
		case 2:	ar >> m_strName;
				ar >> m_strComment;
				ar >> m_strProductionProfile;
				m_rules.Serialize(ar);
				ar >> m_fMaxOverProduction;
				ar >> m_bOneTypePerSheet;
				m_quantityRanges.Serialize(ar);
				break;
		}
	}
}


void CProductionSetup::Load(const CString& strSetupName )
{
	CFileException fileException;
	CString		   strFullPath;
	
	strFullPath.Format(_T("%s\\ProductionSetups\\%s.set"), theApp.GetDataFolder(), strSetupName); 
	
	CFile file;
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}
	
	CArchive ar(&file, CArchive::load);
	Serialize(ar);
	ar.Close();
	file.Close();

	m_strName = strSetupName;
}

void CProductionSetup::Save(const CString& strSetupName)
{
	CFile		   file;
	CFileException fileException;
	CString		   strFullPath;
	
	strFullPath.Format(_T("%s\\ProductionSetups"), theApp.GetDataFolder()); 
	if ( ! theApp.DirectoryExist(strFullPath))
		CreateDirectory(strFullPath, NULL);
	strFullPath.Format(_T("%s\\ProductionSetups\\%s.set"), theApp.GetDataFolder(), strSetupName); 

	if (!file.Open((LPCTSTR)strFullPath, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}

	CArchive ar(&file, CArchive::store);
	Serialize(ar);
	ar.Close();
	file.Close();
}

void CProductionSetup::Remove(const CString& strSetupName)
{
	CString strFullPath;
	strFullPath.Format(_T("%s\\ProductionSetups\\%s.set"), theApp.GetDataFolder(), strSetupName); 

	DeleteFile(strFullPath);
}

int CProductionSetup::FindResourceIndex(const CString& strID, int nType)
{
	switch (nType)
	{
	case ResourcePaper:		for (int i = 0; i < m_paperData.GetSize(); i++)
								if (m_paperData[i].GetID() == strID)
									return i;
	}
	return -1;
}

int CProductionSetup::FindProcessIndex(const CString& strID, int nType)
{
	switch (nType)
	{
	case ProcessPrepress:		for (int i = 0; i < m_prepressData.GetSize(); i++)
								{
									if (m_prepressData[i].GetID() == strID)
										return i;
								}
								break;
	case ProcessPress:			for (int i = 0; i < m_pressData.GetSize(); i++)
								{
									if (m_pressData[i].GetID() == strID)
										return i;
								}
								break;
	case ProcessCutting:		for (int i = 0; i < m_cuttingData.GetSize(); i++)
								{
									if (m_cuttingData[i].GetID() == strID)
										return i;
								}
								break;
	case ProcessFolding:		for (int i = 0; i < m_foldingData.GetSize(); i++)
								{
									if (m_foldingData[i].GetID() == strID)
										return i;
								}
								break;
	case ProcessBinding:		for (int i = 0; i < m_bindingData.GetSize(); i++)
								{
									if (m_bindingData[i].GetID() == strID)
										return i;
								}
								break;
	case ProcessTrimming:		for (int i = 0; i < m_trimmingData.GetSize(); i++)
								{
									if (m_trimmingData[i].GetID() == strID)
										return i;
								}
								break;
	}
	return -1;
}

CPaperDefs	CProductionSetup::GetPaperResource(const CString& strID)
{
	int nIndex = FindResourceIndex(strID, ResourcePaper);
	if (nIndex < 0)
		return CPaperDefs();
	return m_paperData[nIndex];
}

CPrepressParams CProductionSetup::GetPrepressProcess(const CString& strID)
{
	int nIndex = FindProcessIndex(strID, ProcessPrepress);
	if (nIndex < 0)
		return CPrepressParams();
	return m_prepressData[nIndex];
}

CPressParams CProductionSetup::GetPressProcess(const CString& strID)
{
	int nIndex = FindProcessIndex(strID, ProcessPress);
	if (nIndex < 0)
		return CPressParams();
	return m_pressData[nIndex];
}

CCuttingProcess CProductionSetup::GetCuttingProcess(const CString& strID)
{
	int nIndex = FindProcessIndex(strID, ProcessCutting);
	if (nIndex < 0)
		return CCuttingProcess();
	return m_cuttingData[nIndex];
}

CFoldingProcess CProductionSetup::GetFoldingProcess(const CString& strID)
{
	int nIndex = FindProcessIndex(strID, ProcessFolding);
	if (nIndex < 0)
		return CFoldingProcess();
	return m_foldingData[nIndex];
}

CBindingProcess CProductionSetup::GetBindingProcess(const CString& strID)
{
	int nIndex = FindProcessIndex(strID, ProcessBinding);
	if (nIndex < 0)
		return CBindingProcess();
	return m_bindingData[nIndex];
}

CTrimmingProcess CProductionSetup::GetTrimmingProcess(const CString& strID)
{
	int nIndex = FindProcessIndex(strID, ProcessTrimming);
	if (nIndex < 0)
		return CTrimmingProcess();
	return m_trimmingData[nIndex];
}

void CProductionSetup::AddResource(CObject& rResource)
{
	if (rResource.IsKindOf(RUNTIME_CLASS(CPaperDefs)))
		m_paperData.Add((CPaperDefs&)rResource);
}

void CProductionSetup::AddProcess(CObject& rProcess)
{
	if (rProcess.IsKindOf(RUNTIME_CLASS(CPrepressParams)))
		m_prepressData.Add((CPrepressParams&)rProcess);
	else
	if (rProcess.IsKindOf(RUNTIME_CLASS(CPressParams)))
		m_pressData.Add((CPressParams&)rProcess);
	else
	if (rProcess.IsKindOf(RUNTIME_CLASS(CCuttingProcess)))
		m_cuttingData.Add((CCuttingProcess&)rProcess);
	else
	if (rProcess.IsKindOf(RUNTIME_CLASS(CFoldingProcess)))
		m_foldingData.Add((CFoldingProcess&)rProcess);
	else
	if (rProcess.IsKindOf(RUNTIME_CLASS(CBindingProcess)))
		m_bindingData.Add((CBindingProcess&)rProcess);
	else
	if (rProcess.IsKindOf(RUNTIME_CLASS(CTrimmingProcess)))
		m_trimmingData.Add((CTrimmingProcess&)rProcess);
}

void CProductionSetup::RemoveResource(const CString& strID, int nType)
{
	int nIndex = FindResourceIndex(strID, nType);
	if (nIndex < 0)
		return;
	switch (nType)
	{
	case ResourcePaper:		m_paperData.RemoveAt(nIndex);		break;
	}
}

void CProductionSetup::RemoveProcess(const CString& strID, int nType)
{
	int nIndex = FindProcessIndex(strID, nType);
	if (nIndex < 0)
		return;
	switch (nType)
	{
	case ProcessPrepress:	m_prepressData.RemoveAt(nIndex);	break;
	case ProcessPress:		m_pressData.RemoveAt(nIndex);		break;
	case ProcessCutting:	m_cuttingData.RemoveAt(nIndex);		break;
	case ProcessFolding:	m_foldingData.RemoveAt(nIndex);		break;
	case ProcessBinding:	m_bindingData.RemoveAt(nIndex);		break;
	case ProcessTrimming:	m_trimmingData.RemoveAt(nIndex);	break;
	}
}

void CProductionSetup::ModifyResource(const CString& strID, CObject& rResource)
{
}

void CProductionSetup::ModifyProcess(const CString& strID, CObject& rProcess)
{
}

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CProductionProfiles

IMPLEMENT_SERIAL(CProductionProfiles, CObject, VERSIONABLE_SCHEMA | 1)

CProductionProfiles::CProductionProfiles()
{
}

CProductionProfiles::~CProductionProfiles()
{
}

void CProductionProfiles::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductionProfiles));

	CList<CProductionProfile, CProductionProfile&>::Serialize(ar);
}

CProductionProfile& CProductionProfiles::GetProductionProfile(int nID)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CProductionProfile& rProductionProfile = GetNext(pos);
		if (nIndex == nID)	
			return rProductionProfile;
		nIndex++;
	}

	return m_nullProductionProfile;
}

CProductionProfile& CProductionProfiles::FindSuitable(CPrintGroup& rPrintGroup)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CProductionProfile& rProductionProfile = GetNext(pos);

		CString strExpression = rProductionProfile.m_rules.GetRuleExpression(0);
		if (strExpression.IsEmpty())
			continue;

		CExpressionParser parser;
		BOOL			  bExpressionState = TRUE;
		int				  nRelation		   = CKimRules::RelationNone;
		int				  nCurPos		   = 0;
		CString			  strToken		   = strExpression.Tokenize(_T("\n"), nCurPos);
		while (strToken != _T("") && (strToken.Find(_T("[Action]")) < 0) )
		{
			strToken.TrimLeft(_T("(")); strToken.TrimRight(_T(")"));

			if (strToken.CompareNoCase(_T("and")) == 0) 
			{
				nRelation = CKimRules::RelationAnd;
				strToken = strExpression.Tokenize(_T("\n"), nCurPos);
				continue;
			}
			else
				if (strToken.CompareNoCase(_T("or")) == 0) 
				{
					nRelation = CKimRules::RelationOr;
					strToken = strExpression.Tokenize(_T("\n"), nCurPos);
					continue;
				}

			parser.ParseString(strToken);
			if (nRelation == CKimRules::RelationAnd)
				bExpressionState = bExpressionState & RuleExpressionIsTrue(parser.m_strVariable, parser.m_strOperator, parser.m_strValue, rPrintGroup);
			else
				if (nRelation == CKimRules::RelationOr)
					bExpressionState = bExpressionState | RuleExpressionIsTrue(parser.m_strVariable, parser.m_strOperator, parser.m_strValue, rPrintGroup);
				else
					bExpressionState = RuleExpressionIsTrue(parser.m_strVariable, parser.m_strOperator, parser.m_strValue, rPrintGroup);

			strToken = strExpression.Tokenize(_T("\n"), nCurPos);
		}
		if (bExpressionState)
			return rProductionProfile;
	}

	return m_nullProductionProfile;

	//int		 nMinNumColors = INT_MAX;
	//POSITION posMin		   = NULL;
	//POSITION pos		   = GetHeadPosition();
	//while (pos)
	//{
	//	CProductionProfile& rProductionProfile = GetAt(pos);
	//	if (rProductionProfile.m_press.m_nNumColorStationsFront >= rPrintGroup.m_colorantList.GetSize())
	//	{
	//		if (rProductionProfile.m_press.m_nNumColorStationsFront < nMinNumColors)
	//		{
	//			nMinNumColors = rProductionProfile.m_press.m_nNumColorStationsFront;
	//			posMin = pos;
	//		}
	//	}
	//	GetNext(pos);
	//}

	//if (posMin)
	//	return GetAt(posMin);
	//else
	//	if (GetSize())
	//		return GetHead();
	//	else
	//		return m_nullProductionProfile;
}

CProductionProfile& CProductionProfiles::FindProfile(CString strProfileName)
{
	static CProductionProfile nullProfile;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CProductionProfile& rProductionProfile = GetNext(pos);
		if (rProductionProfile.m_strName.IsEmpty())	// TODO: empty profiles should not be allowed
			continue;
		if (rProductionProfile.m_strName.CompareNoCase(strProfileName) == 0)
			return rProductionProfile;
	}

	return nullProfile;
}

void CProductionProfiles::RemoveProfile(CString strProfileName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CProductionProfile& rProductionProfile = GetAt(pos);
		if (rProductionProfile.m_strName.CompareNoCase(strProfileName) == 0)
		{
			RemoveAt(pos);
			return;
		}
		GetNext(pos);
	}
}

BOOL CProductionProfiles::RuleExpressionIsTrue(const CString& strVariable, const CString& strOperator, const CString& strValue, CPrintGroup& rPrintGroup)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CString strResult;
	CString strPlaceholder;
	int		nResult = 0;
	float	fResult = 0.0f;
	int		nDataType = CKimRules::TypeNone;

	CPrintGroupComponent* pComponent = rPrintGroup.GetFirstBoundComponent();
	CBoundProductPart& rProductPart  = (pComponent) ? pDoc->m_boundProducts.GetProductPart(pComponent->m_nProductClassIndex) : CBoundProductPart();
	CFlatProduct*	   pFlatProduct	 = rPrintGroup.GetFirstFlatProduct();

	if (strVariable == _T("ProductType"))
	{
		if ( ! rProductPart.IsNull())
			strResult = _T("Bound");
		else
			if (pFlatProduct)
			{
				if ( ! pFlatProduct->m_strPunchTool.IsEmpty())
					strResult = _T("Punched Product"); 
				else
					strResult = _T("Label");
			}
		nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("ProductPartName"))
	{
		if ( ! rProductPart.IsNull())
			strResult = rProductPart.m_strName;
		else
			strResult.LoadString(IDS_UNBOUND); 
		nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("BindingStyle"))
	{
		if (rProductPart.IsNull())
			strResult = _T("");
		else
			strResult = rProductPart.GetBoundProduct().m_bindingParams.m_bindingDefs.m_strName; 
		nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("NumColors"))
	{
		nResult = rPrintGroup.m_colorantList.GetSize(); nDataType = CKimRules::TypeInteger;
	}
	else
	if (strVariable == _T("NumSpotColors"))
	{
		CStringArray strSpotColors;
		rPrintGroup.m_colorantList.GetSpotColors(strSpotColors);
		nResult = strSpotColors.GetSize(); nDataType = CKimRules::TypeInteger;
	}
	else
	if (strVariable == _T("PaperName"))
	{
		strResult = rPrintGroup.m_strPaper; nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("PaperType"))
	{
		nResult = rPrintGroup.m_nPaperType; nDataType = CKimRules::TypeInteger;
	}
	else
	if (strVariable == _T("Color"))
	{
		strResult = _T("");//(pPrintSheet) ? pPrintSheet->FindSeparation(strValue, nSide) : _T(""); nDataType = CKimRules::TypeString;
	}

	if (strOperator == _T("="))
	{
		switch (nDataType)
		{
		case CKimRules::TypeString:		if (strResult == strValue)
											return TRUE;
										break;
		case CKimRules::TypeInteger:	if (nResult == _ttoi(strValue))
											return TRUE;
										break;
		case CKimRules::TypeFloat:		if (fResult == Ascii2Float(strValue))
											return TRUE;
										break;
		}
	}
	else
	if (strOperator == _T("!="))
	{
		switch (nDataType)
		{
		case CKimRules::TypeString:		if (strResult != strValue)
											return TRUE;
										break;
		case CKimRules::TypeInteger:	if (nResult != _ttoi(strValue))
											return TRUE;
										break;
		case CKimRules::TypeFloat:		if (fResult != Ascii2Float(strValue))
											return TRUE;
										break;
		}
	}
	else
	if (strOperator == _T("<"))
	{
		switch (nDataType)
		{
		case CKimRules::TypeString:		if (strResult < strValue)
											return TRUE;
										break;
		case CKimRules::TypeInteger:	if (nResult < _ttoi(strValue))
											return TRUE;
										break;
		case CKimRules::TypeFloat:		if (fResult < Ascii2Float(strValue))
											return TRUE;
										break;
		}
	}
	else
	if (strOperator == _T(">"))
	{
		switch (nDataType)
		{
		case CKimRules::TypeString:		if (strResult > strValue)
											return TRUE;
										break;
		case CKimRules::TypeInteger:	if (nResult > _ttoi(strValue))
											return TRUE;
										break;
		case CKimRules::TypeFloat:		if (fResult > Ascii2Float(strValue))
											return TRUE;
										break;
		}
	}

	return FALSE;
}



/////////////////////////////////////////////////////////////////////////////
// CProductionProfile

IMPLEMENT_SERIAL(CProductionProfile, CObject, VERSIONABLE_SCHEMA | 3)

CProductionProfile::CProductionProfile()
{
	m_activeProcesses.RemoveAll();
	m_activeProcesses.Add(Printing);	// printing always needed
	m_rules.RemoveAll();

	m_press.m_pressDevice.m_fPlateWidth		= 450.0f;
	m_press.m_pressDevice.m_fPlateHeight	= 350.0f;
	m_press.m_sheetData.m_fWidth			= 450.0f;
	m_press.m_sheetData.m_fHeight			= 350.0f;
}

CProductionProfile::CProductionProfile(const CProductionProfile& rProductionProfile)
{
	Copy(rProductionProfile);	
}

const CProductionProfile& CProductionProfile::operator=(const CProductionProfile& rProductionProfile)
{
	Copy(rProductionProfile);	

	return *this;
}

BOOL CProductionProfile::operator==(const CProductionProfile& rProductionProfile)
{
	if (m_strName		  != rProductionProfile.m_strName)			return FALSE;
	if (m_strComment	  != rProductionProfile.m_strComment)		return FALSE;
	if (m_nProductionType != rProductionProfile.m_nProductionType)	return FALSE;
//	if (m_strMarkset	  != rProductionProfile.m_strMarkset)		return FALSE;
	if (m_layoutPool	  != rProductionProfile.m_layoutPool)		return FALSE;

	if (m_prepress		  != rProductionProfile.m_prepress)			return FALSE;
	if (m_press			  != rProductionProfile.m_press)			return FALSE;
	if (m_postpress		  != rProductionProfile.m_postpress)		return FALSE;

	if (m_rules			  != rProductionProfile.m_rules)			return FALSE;

	if (m_activeProcesses.GetSize() != rProductionProfile.m_activeProcesses.GetSize()) return FALSE;
	for (int i = 0; i < m_activeProcesses.GetSize(); i++)
		if (m_activeProcesses[i] != rProductionProfile.m_activeProcesses[i]) return FALSE;

	return TRUE;
}

BOOL CProductionProfile::operator!=(const CProductionProfile& rProductionProfile)
{
	return (*this == rProductionProfile) ? FALSE : TRUE;
}

void CProductionProfile::Copy(const CProductionProfile& rProductionProfile)
{
	//m_dspi			  = rProductionProfile.m_dspi;
	m_strName		  = rProductionProfile.m_strName;
	m_strComment	  = rProductionProfile.m_strComment;
	m_nProductionType = rProductionProfile.m_nProductionType;
	m_layoutPool	  = rProductionProfile.m_layoutPool;

	m_prepress		  = rProductionProfile.m_prepress;
	m_press			  = rProductionProfile.m_press;
	m_postpress		  = rProductionProfile.m_postpress;

	m_activeProcesses.RemoveAll();
	m_activeProcesses.Append(rProductionProfile.m_activeProcesses);

	m_rules			  = rProductionProfile.m_rules;
}

int	CProductionProfile::GetIndex()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int		 nIndex = 0;
	POSITION pos	= theApp.m_productionProfiles.GetHeadPosition();
	while (pos)
	{
		if (&theApp.m_productionProfiles.GetNext(pos) == this)
			return nIndex;
		nIndex++;
	}

	return -1;
}

CLayout* CProductionProfile::GetLayout()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int		 nIndex = GetIndex();
	POSITION pos	= pDoc->m_PrintSheetList.m_Layouts.FindIndex(GetIndex());
	if (pos)
		return &pDoc->m_PrintSheetList.m_Layouts.GetAt(pos);
	else
		return NULL;
}

BOOL CProductionProfile::ApplyModifications(CProductionProfile& rOldProfile)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL	 bDoReImpose = FALSE;
	BOOL	 bChanged	 = FALSE; 
	CLayout* pLayout	 = GetLayout();

	if (rOldProfile.m_activeProcesses.GetSize() != m_activeProcesses.GetSize())
		bChanged = TRUE;
	else
	{
		for (int i = 0; i < m_activeProcesses.GetSize(); i++)
		{
			if (m_activeProcesses[i] != rOldProfile.m_activeProcesses[i])
			{
				bChanged = TRUE;
				break;
			}
		}
	}

	if (pLayout)
	{
		//if ( ! ProcessIsActive(CProductionProfile::Prepress) || m_strMarkset.IsEmpty())
		//	bChanged = TRUE;
		//else
		//	if (m_strMarkset != rOldProfile.m_strMarkset)
		//	{
		//		CString strMarksDir		= CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets");
		//		CString strRelativePath	= m_strMarkset;	
		//		strRelativePath.TrimLeft('.');
		//		CString strMarksetFullPath = strMarksDir + strRelativePath;
		//		CMarkList markList;
		//		markList.Load(strMarksetFullPath);
		//		pLayout->m_markList.Append(markList);
		//		pLayout->AnalyzeMarks();
		//		bChanged = TRUE;
		//	}

		if (m_prepress.ApplyModifications(pLayout, rOldProfile.m_prepress, bDoReImpose))
			bChanged = TRUE;
		if (m_press.ApplyModifications(pLayout, rOldProfile.m_press.m_pressDevice, bDoReImpose))
			bChanged = TRUE;
		if (m_postpress.m_cutting.ApplyModifications(pLayout, rOldProfile.m_postpress.m_cutting, bDoReImpose))
			bChanged = TRUE;
		if (m_postpress.m_binding.ApplyModifications(pLayout, rOldProfile.m_postpress.m_binding, bDoReImpose))
			bChanged = TRUE;
		if (m_postpress.m_trimming.ApplyModifications(pLayout, rOldProfile.m_postpress.m_trimming, bDoReImpose))
			bChanged = TRUE;
		if (m_postpress.m_punching.ApplyModifications(pLayout, rOldProfile.m_postpress.m_punching, bDoReImpose))
			bChanged = TRUE;

		if (bDoReImpose)
		{
			//CDlgOptimizationMonitor dlg;
			//dlg.Create(IDD_OPTIMIZATION_MONITOR);
			//dlg.OptimizeGroup(-1);//rOldProfile.GetUnassignedProductGroupID());
			//dlg.DestroyWindow();
		}
	}

	return bChanged;
}

BOOL CProductionProfile::ProcessIsActive(int nProcess)
{
	if (IsNull())
		return TRUE;

	for (int i = 0; i < m_activeProcesses.GetSize(); i++)
	{
		if (m_activeProcesses[i] == nProcess)
			return TRUE;
	}
	return FALSE;
}

CRect CProductionProfile::DrawInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CLayout*	 pLayout	 = GetLayout();
	CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
	CPrintGroup& rPrintGroup = pDoc->m_printGroupList.GetPrintGroup(GetIndex());

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);
	
	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT);

	rcFrame.left += 10; rcFrame.top += 5;

	pDC->SetTextColor(BLACK);

	BOOL bDrawLine = FALSE;

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;

	CString strOut = m_strName;
	if (IsNull())
		strOut.LoadString(IDS_SELECT_PROFILE);
	if ( ! strOut.IsEmpty() && (strOut != m_press.m_pressDevice.m_strName) )
	{
		CProductionProfile& rSysProfile = theApp.m_productionProfiles.FindProfile(m_strName);
		if ( ! rSysProfile.IsNull())
			if (rSysProfile != *this)
				strOut += _T(" *");
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
		rcText.OffsetRect(0, nTextHeight);
		bDrawLine = (IsNull()) ? FALSE : TRUE;
	}

	pDC->SelectObject(&font);

	rcText.bottom = rcFrame.bottom;

	if ( ! m_strComment.IsEmpty())
	{
		pDC->SetTextColor(DARKGRAY);
		DrawTextMeasured(pDC, m_strComment, rcText, DT_LEFT | DT_TOP | DT_END_ELLIPSIS, rcBox);
		bDrawLine = TRUE;
	}

	font.DeleteObject();
	boldFont.DeleteObject();

	int nDrawLineY = rcFrame.top = rcBox.bottom + 5;
	if (bDrawLine)
		rcFrame.top += 5;

	CRect rcRet;
	if (ProcessIsActive(CProductionProfile::Printing))
	{
		rcFrame.left -= 5;
		rcRet = m_press.DrawData(pDC, rcFrame, pLayout, NULL, TRUE);						
		//RegisterProcessData(pDC, rcRet, CProductionProfile::Printing);
		rcBox.UnionRect(rcBox, rcRet); 
	}
	//if (ProcessIsActive(CProductionProfile::Cutting) && rPrintGroup.HasFlatProducts())
	//{
	//	rcFrame.top = rcBox.bottom + 10;
	//	rcRet = m_postpress.m_cutting.DrawData(pDC, rcFrame, pPrintSheet, NULL);	
	//	//RegisterProcessData(pDC, rcRet, CProductionProfile::Cutting);
	//	rcBox.UnionRect(rcBox, rcRet); 
	//}
	//if (rPrintGroup.HasBoundProducts())//ProcessIsActive(CProductionProfile::Binding))
	//{
	//	rcFrame.top = rcBox.bottom + 10;
	//	rcRet = m_postpress.m_binding.DrawData(pDC, rcFrame, pPrintSheet, NULL);	
	//	//RegisterProcessData(pDC, rcRet, CProductionProfile::Binding);
	//	rcBox.UnionRect(rcBox, rcRet); 
	//}
	//if (rPrintGroup.HasBoundProducts())//ProcessIsActive(CProductionProfile::Trimming))
	//{
	//	rcFrame.top = rcBox.bottom + 10;
	//	rcRet = m_postpress.m_trimming.DrawData(pDC, rcFrame, pPrintSheet, NULL);	
	//	//RegisterProcessData(pDC, rcRet, CProductionProfile::Trimming);
	//	rcBox.UnionRect(rcBox, rcRet); 
	//}

	//rcBox.bottom = min(rcBox.bottom, rcFrame.bottom);

	//rcBox.right = rcFrame.right - 5;
	rcBox.right  += 5; 
	rcBox.bottom += 10;

	//if (pDisplayList)
	//{
	//	CRect rcDI = rcBox; 
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, ProductionProfile), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//		if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
	//		{
	//			CRect rcFeedback = pDI->m_BoundingRect; rcFeedback.left++; rcFeedback.top++; rcFeedback.right -=2; rcFeedback.bottom -= 2;
	//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
	//			CGraphicComponent gc;
	//			gc.DrawTransparentFrame(pDC, rcFeedback, RGB(255,195,15));
	//		}
	//	}
	//}

	if (bDrawLine)
	{
		CPen pen(PS_SOLID, 1, LIGHTGRAY);
		CPen* pOldPen = pDC->SelectObject(&pen);
		pDC->MoveTo(rcBox.left + 5, nDrawLineY); pDC->LineTo(rcBox.right - 5, nDrawLineY);
		pen.DeleteObject();
		pDC->SelectObject(pOldPen);
	}

	SetDisplayBox(rcBox);

	CGraphicComponent gc;
	gc.DrawFrame(pDC, rcBox, crBackground, TRUE);

	return rcBox;
}

void CProductionProfile::DrawPlanningTableLinks(CDC* pDC, BOOL bHighlight, int& nVerticalAxis, int& nMaxSourceRight, CDisplayList* pDisplayList)
{
	//if (IsNull())
	//	return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPen  pen(PS_SOLID, 2, RGB(210,210,235));
	CPen* pOldPen = pDC->SelectObject(&pen);

	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return;

	BOOL		 bFirst		 = TRUE;
	CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		CRect rcSource = GetDisplayBox();
		CRect rcTarget = pPrintSheet->GetDisplayBoxSheet();

		if (bFirst)
		{
			nVerticalAxis = (nVerticalAxis == 0) ? rcTarget.left - nMaxSourceRight - 30 : nVerticalAxis - 5;
			nVerticalAxis = max(nVerticalAxis, 30);	// not under 30
			bFirst = FALSE;
		}
		int nR = 15;
		if (rcSource.CenterPoint().y < rcTarget.CenterPoint().y)	// draw downward
		{
			nR = min(nR, (rcTarget.CenterPoint().y - rcSource.CenterPoint().y)/2);
			CPoint pt1(rcSource.right,				  rcSource.CenterPoint().y);
			CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
			CPoint pt3(pt2.x,		  rcTarget.CenterPoint().y);
			CPoint pt4(rcTarget.left, rcTarget.CenterPoint().y);
			CPoint cp1 = pt2 + CSize(-nR,  nR);
			CPoint cp2 = pt3 + CSize( nR, -nR);
			CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
			CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

			pDC->MoveTo(pt1);
			pDC->LineTo(pt2 - CSize(nR, 0));
			pDC->AngleArc(cp1.x, cp1.y, nR, 90, -90);
			pDC->LineTo(pt3 - CSize(0, nR));
			pDC->AngleArc(cp2.x, cp2.y, nR, 180, 90);
			pDC->LineTo(pt4);
		}
		else	// draw upward
		{
			nR = min(nR, (rcSource.CenterPoint().y - rcTarget.CenterPoint().y)/2);
			CPoint pt1(rcSource.right,				  rcSource.CenterPoint().y);
			CPoint pt2(rcTarget.left - nVerticalAxis, rcSource.CenterPoint().y);
			CPoint pt3(pt2.x,		  rcTarget.CenterPoint().y);
			CPoint pt4(rcTarget.left, rcTarget.CenterPoint().y);
			CPoint cp1 = pt2 + CSize(-nR, -nR);
			CPoint cp2 = pt3 + CSize( nR,  nR);
			CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
			CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

			pDC->MoveTo(pt1);
			pDC->LineTo(pt2 - CSize(nR, 0));
			pDC->AngleArc(cp1.x, cp1.y, nR, -90,  90);
			pDC->LineTo(pt3 + CSize(0, nR));
			pDC->AngleArc(cp2.x, cp2.y, nR, 180, -90);
			pDC->LineTo(pt4);
		}

		//int nMaxTop	   = max(rcSource.top,	  rcTarget.top);
		//int nMinBottom = min(rcSource.bottom, rcTarget.bottom);

		//if (nMaxTop < nMinBottom)
		//{
		//	int nY = nMaxTop + (nMinBottom - nMaxTop)/2;
		//	pDC->MoveTo(rcSource.right, nY);
		//	pDC->LineTo(rcTarget.left,  nY);
		//}
		//else
		//{
		//	int nR = 15;
		//	if (rcSource.CenterPoint().y < rcTarget.CenterPoint().y)	// draw downward
		//	{
		//		CPoint pt1(rcSource.right,										rcSource.CenterPoint().y);
		//		CPoint pt2(rcSource.right + (rcTarget.left - rcSource.right)/2, rcSource.CenterPoint().y);
		//		CPoint pt3(pt2.x,		  rcTarget.CenterPoint().y);
		//		CPoint pt4(rcTarget.left, rcTarget.CenterPoint().y);
		//		CPoint cp1 = pt2 + CSize(-nR,  nR);
		//		CPoint cp2 = pt3 + CSize( nR, -nR);
		//		CRect  rc1(cp1 - CSize(nR, nR), cp1 + CSize(nR, nR));
		//		CRect  rc2(cp2 - CSize(nR, nR), cp2 + CSize(nR, nR));

		//		pDC->MoveTo(pt1);
		//		pDC->LineTo(pt2 - CSize(nR, 0));
		//		pDC->AngleArc(cp1.x, cp1.y, nR, 90, -90);
		//		pDC->LineTo(pt3 - CSize(0, nR));
		//		pDC->AngleArc(cp2.x, cp2.y, nR, 180, 90);
		//		pDC->LineTo(pt4);
		//	}
		//}
	
		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();
}

// helper function for CPrintGroup elements
template <> void AFXAPI SerializeElements <CProductionProfile> (CArchive& ar, CProductionProfile* pProductionProfile, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductionProfile));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	CString strMarkset;
	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pProductionProfile->m_strName;
			ar << pProductionProfile->m_strComment;
			ar << pProductionProfile->m_nProductionType;
			pProductionProfile->m_layoutPool.Serialize(ar);
		}	
		else
		{ 
			switch (nVersion)
			{
			case 1:	ar >> pProductionProfile->m_strName;
					ar >> pProductionProfile->m_strComment;
					ar >> pProductionProfile->m_nProductionType;
					ar >> strMarkset;
					break;
			case 2:	ar >> pProductionProfile->m_strName;
					ar >> pProductionProfile->m_strComment;
					ar >> pProductionProfile->m_nProductionType;
					ar >> strMarkset;
					pProductionProfile->m_layoutPool.Serialize(ar);
					break;
			case 3:	ar >> pProductionProfile->m_strName;
					ar >> pProductionProfile->m_strComment;
					ar >> pProductionProfile->m_nProductionType;
					pProductionProfile->m_layoutPool.Serialize(ar);
					break;
			}
		}

		pProductionProfile->m_prepress.Serialize(ar);
		pProductionProfile->m_press.Serialize(ar);
		pProductionProfile->m_postpress.Serialize(ar);

		pProductionProfile->m_activeProcesses.Serialize(ar);
		pProductionProfile->m_rules.Serialize(ar);

		if (ar.IsLoading())
		{
			if (nVersion > 3)
				pProductionProfile->m_press.m_strMarkset = strMarkset;

			pProductionProfile->m_prepress.SetID((pProductionProfile->ProcessIsActive(CProductionProfile::Prepress))			 ? _T("$IsDefault") : _T(""));
			pProductionProfile->m_press.SetID((pProductionProfile->ProcessIsActive(CProductionProfile::Printing))				 ? _T("$IsDefault") : _T(""));
			pProductionProfile->m_postpress.m_cutting.SetID((pProductionProfile->ProcessIsActive(CProductionProfile::Cutting))	 ? _T("$IsDefault") : _T(""));
			pProductionProfile->m_postpress.m_folding.SetID((pProductionProfile->ProcessIsActive(CProductionProfile::Folding))	 ? _T("$IsDefault") : _T(""));
			pProductionProfile->m_postpress.m_binding.SetID((pProductionProfile->ProcessIsActive(CProductionProfile::Binding))	 ? _T("$IsDefault") : _T(""));
			pProductionProfile->m_postpress.m_trimming.SetID((pProductionProfile->ProcessIsActive(CProductionProfile::Trimming)) ? _T("$IsDefault") : _T(""));
		}

		pProductionProfile++;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CProductionProfileLayoutTemplate

IMPLEMENT_SERIAL(CProductionProfileLayoutTemplate, CObject, VERSIONABLE_SCHEMA | 1)

CProductionProfileLayoutTemplate::CProductionProfileLayoutTemplate()
{
}

CProductionProfileLayoutTemplate::CProductionProfileLayoutTemplate(const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate)
{
	*this = rProductionProfileLayoutTemplate;
}

const CProductionProfileLayoutTemplate& CProductionProfileLayoutTemplate::operator=(const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate)
{
	Copy(rProductionProfileLayoutTemplate);	

	return *this;
}

BOOL CProductionProfileLayoutTemplate::operator==(const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate)
{
	return TRUE;
}

BOOL CProductionProfileLayoutTemplate::operator!=(const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate)
{
	return (*this == rProductionProfileLayoutTemplate) ? FALSE : TRUE;
}

void CProductionProfileLayoutTemplate::Copy(const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate)
{
	m_layout	 = rProductionProfileLayoutTemplate.m_layout;
	m_printSheet = rProductionProfileLayoutTemplate.m_printSheet;
}



/////////////////////////////////////////////////////////////////////////////
// CProductionProfileLayoutPool

IMPLEMENT_SERIAL(CProductionProfileLayoutPool, CObject, VERSIONABLE_SCHEMA | 1)

CProductionProfileLayoutPool::CProductionProfileLayoutPool()
{
}

const CProductionProfileLayoutPool& CProductionProfileLayoutPool::operator=(const CProductionProfileLayoutPool& rProductionProfileLayoutPool)
{
	Copy(rProductionProfileLayoutPool);	

	return *this;
}

BOOL CProductionProfileLayoutPool::operator==(const CProductionProfileLayoutPool& rProductionProfileLayoutPool)
{
	if (GetSize() != rProductionProfileLayoutPool.GetSize())
		return FALSE;

	//POSITION pos1 = m_layouts.GetHeadPosition();
	//POSITION pos2 = rProductionProfileLayoutPool.m_layouts.GetHeadPosition();
	//while (pos1 && pos2)
	//{
	//	CLayout& rLayout1 = m_layouts.GetNext(pos1);
	//	CLayout& rLayout2 = rProductionProfileLayoutPool.m_layouts.GetNext(pos2);
	//	if (rLayout1 != rLayout2)
	//		return FALSE;
	//}

	return TRUE;
}

BOOL CProductionProfileLayoutPool::operator!=(const CProductionProfileLayoutPool& rProductionProfileLayoutPool)
{
	return (*this == rProductionProfileLayoutPool) ? FALSE : TRUE;
}

void CProductionProfileLayoutPool::Copy(const CProductionProfileLayoutPool& rProductionProfileLayoutPool)
{
	RemoveAll();
	for (int i = 0; i < rProductionProfileLayoutPool.GetSize(); i++)
	{
		CProductionProfileLayoutTemplate layoutTemplate = rProductionProfileLayoutPool[i];
		Add(layoutTemplate);
	}
}

void CProductionProfileLayoutPool::Add(CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate)
{
	CArray <CProductionProfileLayoutTemplate, CProductionProfileLayoutTemplate&>::Add(rProductionProfileLayoutTemplate);

	///// reinit links/pointer because CArray::Add() moves memory in SetAtGrow() so old pointers could get invalid
	for (int i = 0; i < GetSize(); i++)
	{
		CProductionProfileLayoutTemplate& rLayoutTemplate = GetAt(i);
		rLayoutTemplate.m_layout.ReinitLinks();
		rLayoutTemplate.m_printSheet.ReinitLinks();
	}
}

int CProductionProfileLayoutPool::FindSuitableLayout(CFoldSheet& rFoldSheet, int nNumPagesToImpose)
{
	if (rFoldSheet.m_LayerList.GetSize() <= 0)
		return -1;
	
	CFoldSheetLayer& rFoldSheetLayer	  = rFoldSheet.m_LayerList.GetHead();
	int				 nPagesFoldSheetLayer = rFoldSheetLayer.m_nPagesX * rFoldSheetLayer.m_nPagesY;
	
	if (nPagesFoldSheetLayer <= 0)
		return NULL;

	for (int i = 0; i < GetSize(); i++)
	{
		CLayout&	 rLayout	 = ElementAt(i).m_layout;
		CPrintSheet& rPrintSheet = ElementAt(i).m_printSheet;
		CArray <int, int> differentFoldSheets;
		for (int j = 0; j < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); j++)
		{
			BOOL bFound = FALSE;
			for (int k = 0; (k < differentFoldSheets.GetSize()) && ! bFound; k++)
			{
				if (rPrintSheet.m_FoldSheetLayerRefs[j].m_nFoldSheetIndex == differentFoldSheets[k])
					bFound = TRUE;
			}

			if ( ! bFound)
				differentFoldSheets.Add(rPrintSheet.m_FoldSheetLayerRefs[j].m_nFoldSheetIndex);
		}

		CObArray			objectPtrList;
		CArray<int, int>	layoutFoldSheetNPagesList;
		int					nNumDifferentPages = differentFoldSheets.GetSize() * nPagesFoldSheetLayer * 2;
		for (int j = 0; j < rLayout.m_FoldSheetLayerRotation.GetSize(); j++)
		{
			rLayout.m_FrontSide.GetFoldSheetObjects(j, &objectPtrList);
			layoutFoldSheetNPagesList.Add(objectPtrList.GetSize());
		}

		BOOL bSuitable = TRUE;
		for (int j = 0; j < layoutFoldSheetNPagesList.GetSize(); j++)
		{
			if (layoutFoldSheetNPagesList[j] != nPagesFoldSheetLayer)
			{
				bSuitable = FALSE;
				break;
			}
		}
		if (bSuitable && (nNumPagesToImpose >= nNumDifferentPages))
			return i;
	}
	return -1;
}


// helper function for CProductionProfileLayoutTemplate elements
template <> void AFXAPI SerializeElements <CProductionProfileLayoutTemplate> (CArchive& ar, CProductionProfileLayoutTemplate* pProductionProfileLayoutTemplate, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductionProfileLayoutTemplate));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			SerializeElements(ar, &pProductionProfileLayoutTemplate->m_layout, 1);
			SerializeElements(ar, &pProductionProfileLayoutTemplate->m_printSheet, 1);
		}	
		else
		{ 
			switch (nVersion)
			{
			case 1:	SerializeElements(ar, &pProductionProfileLayoutTemplate->m_layout, 1);
					SerializeElements(ar, &pProductionProfileLayoutTemplate->m_printSheet, 1);
					break;
			}

			//// normalize foldsheet indices and make them negative
			CArray <CFoldSheetLayerRef, CFoldSheetLayerRef&> foldSheetLayerRefs;
			foldSheetLayerRefs.Append(pProductionProfileLayoutTemplate->m_printSheet.m_FoldSheetLayerRefs);
			qsort((void*)foldSheetLayerRefs.GetData(), foldSheetLayerRefs.GetSize(), sizeof(CFoldSheetLayerRef), CFoldSheetLayerRef::CompareFoldSheetIndex);

			const short cOutOfRange = 10000;	// this index is for sure not existent in m_FoldSheetLayerRefs
			int nOldIndex = -cOutOfRange, nNewIndex = -1;
			for (int i = 0; i < foldSheetLayerRefs.GetSize(); i++)
			{
				int nOldIndex = -cOutOfRange;
				if (i == 0)
					nOldIndex = foldSheetLayerRefs[0].m_nFoldSheetIndex;
				else
					if (foldSheetLayerRefs[i - 1].m_nFoldSheetIndex != foldSheetLayerRefs[i].m_nFoldSheetIndex)
						nOldIndex = foldSheetLayerRefs[i].m_nFoldSheetIndex;

				if (nOldIndex != -cOutOfRange)
				{
					nNewIndex++;
					for (int j = 0; j < pProductionProfileLayoutTemplate->m_printSheet.m_FoldSheetLayerRefs.GetSize(); j++)
					{
						if (pProductionProfileLayoutTemplate->m_printSheet.m_FoldSheetLayerRefs[j].m_nFoldSheetIndex == nOldIndex)
							pProductionProfileLayoutTemplate->m_printSheet.m_FoldSheetLayerRefs[j].m_nFoldSheetIndex = nNewIndex + cOutOfRange;
					}
				}
			}

			for (int i = 0; i < pProductionProfileLayoutTemplate->m_printSheet.m_FoldSheetLayerRefs.GetSize(); i++)
				pProductionProfileLayoutTemplate->m_printSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex = -abs(pProductionProfileLayoutTemplate->m_printSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex - cOutOfRange) - 1;
		}

		pProductionProfileLayoutTemplate++;
	}
}


////////////////////////////////////////////////////////////////////////////
// CPunchingTool
IMPLEMENT_SERIAL(CPunchingTool, CObject, VERSIONABLE_SCHEMA | 3)

CPunchingTool::CPunchingTool()
{
	m_strToolName			= _T("");
	m_nToolID				= 0;
	m_nPunchesX				= 1;
	m_nPunchesY				= 1;
	m_nNXOptions			= 0;
	m_nNYOptions			= 0;
	m_fStepX				= 0.0f;
	m_fStepY				= 0.0f;
	m_fGutterX				= 0.0f;
	m_fGutterY				= 0.0f;
	m_fOffsetRows			= 0.0f;
	m_fOffsetCols			= 0.0f;
	m_bRotateRows			= FALSE;
	m_bRotateCols			= FALSE;
	m_nOffsetRowsOddEven 	= 0;
	m_nOffsetColsOddEven 	= 0;
	m_nRotateRowsOddEven 	= 0;
	m_nRotateColsOddEven 	= 0;
}

const CPunchingTool& CPunchingTool::operator=(const CPunchingTool& rPunchingTool)
{
	Copy(rPunchingTool);	

	return *this;
}

BOOL CPunchingTool::operator==(const CPunchingTool& rPunchingTool)
{
	if (m_strToolName			!= rPunchingTool.m_strToolName)			return FALSE;
	if (m_nToolID				!= rPunchingTool.m_nToolID)				return FALSE;
	if (m_nPunchesX				!= rPunchingTool.m_nPunchesX)			return FALSE;
	if (m_nPunchesY				!= rPunchingTool.m_nPunchesY)			return FALSE;
	if (m_nNXOptions			!= rPunchingTool.m_nNXOptions)			return FALSE;
	if (m_nNYOptions			!= rPunchingTool.m_nNYOptions)			return FALSE;
	if (m_fStepX				!= rPunchingTool.m_fStepX)				return FALSE;		
	if (m_fStepY				!= rPunchingTool.m_fStepY)				return FALSE;
	if (m_fGutterX				!= rPunchingTool.m_fGutterX)			return FALSE;		
	if (m_fGutterY				!= rPunchingTool.m_fGutterY)			return FALSE;
	if (m_fOffsetRows			!= rPunchingTool.m_fOffsetRows)			return FALSE;
	if (m_fOffsetCols			!= rPunchingTool.m_fOffsetCols)			return FALSE;
	if (m_bRotateRows			!= rPunchingTool.m_bRotateRows)			return FALSE;
	if (m_bRotateCols			!= rPunchingTool.m_bRotateCols)			return FALSE;
	if (m_nOffsetRowsOddEven 	!= rPunchingTool.m_nOffsetRowsOddEven)	return FALSE;
	if (m_nOffsetColsOddEven 	!= rPunchingTool.m_nOffsetColsOddEven)	return FALSE;
	if (m_nRotateRowsOddEven 	!= rPunchingTool.m_nRotateRowsOddEven)	return FALSE;
	if (m_nRotateColsOddEven 	!= rPunchingTool.m_nRotateColsOddEven)	return FALSE;

	return TRUE;
}

BOOL CPunchingTool::operator!=(const CPunchingTool& rPunchingTool)
{
	return (*this == rPunchingTool) ? FALSE : TRUE;
}

void CPunchingTool::Copy(const CPunchingTool& rPunchingTool)
{
	m_strToolName			= rPunchingTool.m_strToolName;
	m_nToolID				= rPunchingTool.m_nToolID;
	m_nPunchesX				= rPunchingTool.m_nPunchesX;
	m_nPunchesY				= rPunchingTool.m_nPunchesY;
	m_nNXOptions			= rPunchingTool.m_nNXOptions;
	m_nNYOptions			= rPunchingTool.m_nNYOptions;
	m_fStepX				= rPunchingTool.m_fStepX;		
	m_fStepY				= rPunchingTool.m_fStepY;
	m_fGutterX				= rPunchingTool.m_fGutterX;		
	m_fGutterY				= rPunchingTool.m_fGutterY;
	m_fOffsetRows			= rPunchingTool.m_fOffsetRows;
	m_fOffsetCols			= rPunchingTool.m_fOffsetCols;
	m_bRotateRows			= rPunchingTool.m_bRotateRows;
	m_bRotateCols			= rPunchingTool.m_bRotateCols;
	m_nOffsetRowsOddEven 	= rPunchingTool.m_nOffsetRowsOddEven;
	m_nOffsetColsOddEven 	= rPunchingTool.m_nOffsetColsOddEven;
	m_nRotateRowsOddEven 	= rPunchingTool.m_nRotateRowsOddEven;
	m_nRotateColsOddEven 	= rPunchingTool.m_nRotateColsOddEven;
}

void CPunchingTool::Serialize( CArchive& ar )
{
}

// helper function for CPrintGroup elements
template <> void AFXAPI SerializeElements <CPunchingTool> (CArchive& ar, CPunchingTool* pPunchingTool, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPunchingTool));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPunchingTool->m_strToolName;
			ar << pPunchingTool->m_nToolID;
			ar << pPunchingTool->m_nPunchesX;
			ar << pPunchingTool->m_nPunchesY;
			ar << pPunchingTool->m_nNXOptions;
			ar << pPunchingTool->m_nNYOptions;
			ar << pPunchingTool->m_fStepX;
			ar << pPunchingTool->m_fStepY;
			ar << pPunchingTool->m_fGutterX;
			ar << pPunchingTool->m_fGutterY;
			ar << pPunchingTool->m_fOffsetRows;
			ar << pPunchingTool->m_fOffsetCols;
			ar << pPunchingTool->m_bRotateRows;
			ar << pPunchingTool->m_bRotateCols;
			ar << pPunchingTool->m_nOffsetRowsOddEven;
			ar << pPunchingTool->m_nOffsetColsOddEven;
			ar << pPunchingTool->m_nRotateRowsOddEven;
			ar << pPunchingTool->m_nRotateColsOddEven;
		}	
		else
		{ 
			switch (nVersion)
			{
			case 1:	ar >> pPunchingTool->m_strToolName;
					ar >> pPunchingTool->m_nToolID;
					ar >> pPunchingTool->m_nPunchesX;
					ar >> pPunchingTool->m_nPunchesY;
					ar >> pPunchingTool->m_fStepX;
					ar >> pPunchingTool->m_fStepY;
					ar >> pPunchingTool->m_fGutterX;
					ar >> pPunchingTool->m_fGutterY;
					break;
			case 2:	ar >> pPunchingTool->m_strToolName;
					ar >> pPunchingTool->m_nToolID;
					ar >> pPunchingTool->m_nPunchesX;
					ar >> pPunchingTool->m_nPunchesY;
					ar >> pPunchingTool->m_fStepX;
					ar >> pPunchingTool->m_fStepY;
					ar >> pPunchingTool->m_fGutterX;
					ar >> pPunchingTool->m_fGutterY;
					ar >> pPunchingTool->m_fOffsetRows;
					ar >> pPunchingTool->m_fOffsetCols;
					ar >> pPunchingTool->m_bRotateRows;
					ar >> pPunchingTool->m_bRotateCols;
					ar >> pPunchingTool->m_nOffsetRowsOddEven;
					ar >> pPunchingTool->m_nOffsetColsOddEven;
					ar >> pPunchingTool->m_nRotateRowsOddEven;
					ar >> pPunchingTool->m_nRotateColsOddEven;
					break;
			case 3:	ar >> pPunchingTool->m_strToolName;
					ar >> pPunchingTool->m_nToolID;
					ar >> pPunchingTool->m_nPunchesX;
					ar >> pPunchingTool->m_nPunchesY;
					ar >> pPunchingTool->m_nNXOptions;
					ar >> pPunchingTool->m_nNYOptions;
					ar >> pPunchingTool->m_fStepX;
					ar >> pPunchingTool->m_fStepY;
					ar >> pPunchingTool->m_fGutterX;
					ar >> pPunchingTool->m_fGutterY;
					ar >> pPunchingTool->m_fOffsetRows;
					ar >> pPunchingTool->m_fOffsetCols;
					ar >> pPunchingTool->m_bRotateRows;
					ar >> pPunchingTool->m_bRotateCols;
					ar >> pPunchingTool->m_nOffsetRowsOddEven;
					ar >> pPunchingTool->m_nOffsetColsOddEven;
					ar >> pPunchingTool->m_nRotateRowsOddEven;
					ar >> pPunchingTool->m_nRotateColsOddEven;
					break;
			}
		}

		pPunchingTool++;
	}
}

CString CPunchingTool::FindFolder(CString strStartFolder, CString strPunchToolName)
{
	if (FolderHasPunchTool(strStartFolder, strPunchToolName))
		return strStartFolder;

	WIN32_FIND_DATA findData;
	CString			strFilename = strStartFolder + _T("\\*.*");
	HANDLE			hfileSearch = FindFirstFile(strFilename, &findData);
	if (hfileSearch == INVALID_HANDLE_VALUE)
	{
		FindClose(hfileSearch);
		return _T("");
	}

	CString strFoundFolder;
	//BOOL	bFound	  = FALSE;
	CString	strBuffer = findData.cFileName;
	do 
	{
		strBuffer = findData.cFileName;
		if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
		{
			if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				strFoundFolder = FindFolder(strStartFolder + _T("\\") + strBuffer, strPunchToolName);
				if ( ! strFoundFolder.IsEmpty())
				{
					//bFound = TRUE;
					break;
				}
			}
		}
	}
	while ( FindNextFile( hfileSearch, &findData ));
	FindClose(hfileSearch);

	return strFoundFolder;
}

BOOL CPunchingTool::FolderHasPunchTool(const CString& strFolder, const CString& strPunchTool)
{
	WIN32_FIND_DATA findData;
	CString			strFilename = strFolder + _T("\\*.*");
	HANDLE			hfileSearch = FindFirstFile(strFilename, &findData);
	if (hfileSearch == INVALID_HANDLE_VALUE)
	{
		FindClose(hfileSearch);
		return FALSE;
	}

	BOOL	bFound	  = FALSE;
	CString	strBuffer = findData.cFileName;
	do 
	{
		strBuffer = findData.cFileName;
		if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
		{
			if ( ! (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
			{
				CString strExt = strBuffer.Right(4);
				strExt.MakeLower();
				if (strExt == _T(".ptl"))
				{
					strBuffer = strBuffer.SpanExcluding(_T("."));
					if (strBuffer == strPunchTool)
					{
						bFound = TRUE;
						break;
					}
				}
			}
		}
	}
	while ( FindNextFile( hfileSearch, &findData ));
	FindClose(hfileSearch);

	return bFound;
}

void CPunchingTool::Load(CString strPunchToolName, CString strPath)
{
	if (strPath.IsEmpty())
	{
		CString strStartFolder = CString(theApp.GetDataFolder()) + _T("\\PunchTools");
		strPath = FindFolder(strStartFolder, strPunchToolName);
		if (strPath.IsEmpty())
			strPath = strStartFolder;
	}
	CString strFullPath = strPath + _T("\\") + strPunchToolName + _T(".ptl");

	CFileException	fileException;
	CFile			file;
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}
	
	CArchive ar(&file, CArchive::load);
	SerializeElements(ar, this, 1);
	ar.Close();
	file.Close();
}

void CPunchingTool::Save(CString strPunchToolName, CString strPath)
{
	if (strPath.IsEmpty())
	{
		CString strStartFolder = CString(theApp.GetDataFolder()) + _T("\\PunchTools");
		strPath = FindFolder(strStartFolder, strPunchToolName);
		if (strPath.IsEmpty())
			strPath = strStartFolder;
	}
	CString strFullPath = strPath + _T("\\") + strPunchToolName + _T(".ptl");

	CFile		   file;
	CFileException fileException;
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}

	CArchive ar(&file, CArchive::store);
	SerializeElements(ar, this, 1);
	ar.Close();
	file.Close();
}

int CPunchingTool::AssignID()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CArray <CString, CString&> punchNames;

	POSITION pos = pDoc->m_flatProducts.GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);
		if ( ! rFlatProduct.m_strPunchTool.IsEmpty())
		{
			BOOL bFound = FALSE;
			for (int i = 0; i < punchNames.GetSize(); i++)
			{
				if (punchNames[i] == rFlatProduct.m_strPunchTool)
				{
					bFound = TRUE;
					break;
				}
			}
			if ( ! bFound)
				punchNames.Add(rFlatProduct.m_strPunchTool);
		}
	}

	for (int i = 0; i < punchNames.GetSize(); i++)
	{
		if (punchNames[i] == m_strToolName)
		{
			m_nToolID = i;
			return i;
		}
	}

	return 0;
}

float CPunchingTool::GetCenterPosX(int nX, int nY)
{
	float fCenterPosX = nX * m_fStepX;
	if ((m_nOffsetRowsOddEven == 0) && (nY%2))
		fCenterPosX	+= m_fOffsetRows;
	else
		if ((m_nOffsetRowsOddEven == 1) && !(nY%2))
			fCenterPosX	+= m_fOffsetRows;

	return fCenterPosX;
}

float CPunchingTool::GetCenterPosY(int nX, int nY)
{
	float fCenterPosY = nY * m_fStepY;
	if ((m_nOffsetColsOddEven == 0) && (nX%2))
		fCenterPosY	+= m_fOffsetCols;
	else
		if ((m_nOffsetColsOddEven == 1) && !(nX%2))
			fCenterPosY	+= m_fOffsetCols;

	return fCenterPosY;
}

int	CPunchingTool::GetHeadPosition(int nX, int nY)
{
	BOOL bRotate = FALSE;
	if (m_bRotateRows)
	{
		if ((m_nRotateRowsOddEven == 0) && (nY%2))
			bRotate = TRUE;
		else
			if ((m_nRotateRowsOddEven == 1) && !(nY%2))
				bRotate = TRUE;
	}
	if (m_bRotateCols)
	{
		if ((m_nRotateColsOddEven == 0) && (nX%2))
			bRotate = TRUE;
		else
			if ((m_nRotateColsOddEven == 1) && !(nX%2))
				bRotate = TRUE;
	}

	return (bRotate) ? BOTTOM : TOP;
}

BOOL CPunchingTool::CheckToAdd(int nX, int nY, int nPunchesX, int nPunchesY)
{
	if (nX == nPunchesX - 1)
	{
		if (m_nNXOptions >= 2)
		{
			switch (m_nOffsetRowsOddEven)
			{
			case 0:		if (nY%2)	 
							return FALSE;
						break;
			case 1:		if ( ! (nY%2)) 
							return FALSE;
						break;
			default:	break;
			}
		}
	}

	if (nY == nPunchesY - 1)
	{
		if (m_nNYOptions >= 2)
		{
			switch (m_nOffsetColsOddEven)
			{
			case 0:		if (nX%2)	 
							return FALSE;
						break;
			case 1:		if ( ! (nX%2)) 
							return FALSE;
						break;
			default:	break;
			}
		}
	}
	return TRUE;
}

int	CPunchingTool::CalcMaxNUpX(float fBoxWidth, float fObjectWidth)
{
	if (IsNull())
		return 0;

	float fOffsetX = GetCenterPosX(0, 0) - fObjectWidth / 2.0f - m_fGutterX; 
		  fOffsetX = min(fOffsetX, GetCenterPosX(0, 1) - fObjectWidth / 2.0f - m_fGutterX);
	fOffsetX *= -1.0f;

	int nMaxX1 = 0;
	for (int nX = 0; nX < 1000; nX++)
	{
		if ( (m_nNXOptions == 0) || (m_nNXOptions == 2) )	// 0 = nothing, 2 = alternate
			if (nX >= m_nPunchesX)
				break;

		float fRight = fOffsetX + GetCenterPosX(nX, 0) + fObjectWidth / 2.0f + m_fGutterX;
		if (fRight <= fBoxWidth)
			nMaxX1 = max(nMaxX1, nX + 1);
		else
			break;
	}
	if (m_nPunchesY <= 1)
		return nMaxX1;

	int nMaxX2 = 0;
	for (int nX = 0; nX < 1000; nX++)
	{
		if ( (m_nNXOptions == 0) || (m_nNXOptions == 2) )	// 0 = nothing, 2 = alternate
			if (nX >= m_nPunchesX)
				break;

		float fRight = fOffsetX + GetCenterPosX(nX, 1) + fObjectWidth / 2.0f + m_fGutterX;
		if (fRight <= fBoxWidth)
			nMaxX2 = max(nMaxX2, nX + 1);
		else
			break;
	}

	return min(nMaxX1, nMaxX2);
}

int	CPunchingTool::CalcMaxNUpY(float fBoxHeight, float fObjectHeight)
{
	if (IsNull())
		return 0;

	float fOffsetY = GetCenterPosY(0, 0) - fObjectHeight / 2.0f - m_fGutterY; 
		  fOffsetY = min(fOffsetY, GetCenterPosY(1, 0) - fObjectHeight / 2.0f - m_fGutterY);
	fOffsetY *= -1.0f;

	int nMaxY1 = 0;
	for (int nY = 0; nY < 1000; nY++)
	{
		if ( (m_nNYOptions == 0) || (m_nNYOptions == 2) )	// 0 = nothing, 2 = alternate
			if (nY >= m_nPunchesY)
				break;

		float fTop = fOffsetY + GetCenterPosY(0, nY) + fObjectHeight / 2.0f + m_fGutterY;
		if (fTop <= fBoxHeight)
			nMaxY1 = max(nMaxY1, nY + 1);
		else
			break;
	}
	if (m_nPunchesX <= 1)
		return nMaxY1;

	int nMaxY2 = 0;
	for (int nY = 0; nY < 1000; nY++)
	{
		if ( (m_nNYOptions == 0) || (m_nNYOptions == 2) )	// 0 = nothing, 2 = alternate
			if (nY >= m_nPunchesY)
				break;

		float fTop = fOffsetY + GetCenterPosY(1, nY) + fObjectHeight / 2.0f + m_fGutterY;
		if (fTop <= fBoxHeight)
			nMaxY2 = max(nMaxY2, nY + 1);
		else
			break;
	}

	return min(nMaxY1, nMaxY2);
}

float CPunchingTool::CalcBoxWidth(float fObjectWidth, int nNumX)
{
	if (IsNull())
		return 0.0f;

	int nX = (nNumX < 0) ? m_nPunchesX : nNumX;
	if (m_nPunchesY > 1)
	{
		float  fLeft  = GetCenterPosX(0, 0) - fObjectWidth / 2.0f - m_fGutterX; 
			   fLeft  = min(fLeft, GetCenterPosX(0, 1) - fObjectWidth / 2.0f - m_fGutterX);
		float  fRight = GetCenterPosX(nX - 1, 0) + fObjectWidth / 2.0f + m_fGutterX;
			   fRight = max(fRight, GetCenterPosX(nX - 1, 1) + fObjectWidth / 2.0f + m_fGutterX);
		return fRight - fLeft;
	}
	else
	{
		float  fLeft  = GetCenterPosX(0,	     0) - fObjectWidth / 2.0f - m_fGutterX; 
		float  fRight = GetCenterPosX(nX - 1, 0) + fObjectWidth / 2.0f + m_fGutterX;
		return fRight - fLeft;
	}
}

float CPunchingTool::CalcBoxHeight(float fObjectHeight, int nNumY)
{
	if (IsNull())
		return 0.0f;

	int nY = (nNumY < 0) ? m_nPunchesY : nNumY;
	if (m_nPunchesY > 1)
	{
		float  fBottom  = GetCenterPosY(0, 0) - fObjectHeight / 2.0f - m_fGutterY; 
			   fBottom  = min(fBottom, GetCenterPosY(1, 0) - fObjectHeight / 2.0f - m_fGutterY);
		float  fTop		= GetCenterPosY(0, nY - 1) + fObjectHeight / 2.0f + m_fGutterY;
			   fTop		= max(fTop, GetCenterPosY(1, nY - 1) + fObjectHeight / 2.0f + m_fGutterY);
		return fTop - fBottom;
	}
	else
	{
		float  fBottom = GetCenterPosY(0, 0)		 - fObjectHeight / 2.0f - m_fGutterY; 
		float  fTop    = GetCenterPosY(0, nY - 1) + fObjectHeight / 2.0f + m_fGutterY;
		return fTop - fBottom;
	}
}

CRect CPunchingTool::DrawPreview(CDC* pDC, CRect rcFrame, CFlatProduct* pFlatProduct, CPageTemplate* pPageTemplate, BOOL bShowBitmap)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	CRect rcViewPort = rcFrame;

	pDC->LPtoDP(&rcFrame);

	float fProductWidth  = (pFlatProduct) ? pFlatProduct->m_fTrimSizeWidth  : m_fStepX;
	float fProductHeight = (pFlatProduct) ? pFlatProduct->m_fTrimSizeHeight : m_fStepY;
	float fTotalWidth    = fProductWidth  + (m_nPunchesX - 1) * m_fStepX + m_fOffsetRows;
	float fTotalHeight   = fProductHeight + (m_nPunchesY - 1) * m_fStepY + m_fOffsetCols;

	CSize szExtents(CPrintSheetView::WorldToLP(fTotalWidth), CPrintSheetView::WorldToLP(fTotalHeight));
	if ( (szExtents.cx <= 0) || (szExtents.cy <= 0) )
		return rcBox;

	pDC->SaveDC();
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(szExtents);
	pDC->SetWindowOrg(0, szExtents.cy);

	float fFactorX = (float)rcFrame.Width() /(float)szExtents.cx;
	float fFactorY = (float)rcFrame.Height()/(float)szExtents.cy;
	int nViewportWidth  = rcFrame.Width();
	int nViewportHeight = -(int)((float)szExtents.cy * min(fFactorX, fFactorY));
	nViewportWidth  = (abs(nViewportWidth)  <= 0) ?  1 : nViewportWidth;
	nViewportHeight = (abs(nViewportHeight) <= 0) ? -1 : nViewportHeight;
	pDC->SetViewportExt(nViewportWidth, nViewportHeight);  
	pDC->SetViewportOrg(rcViewPort.left, rcViewPort.top);

	for (int nY = 0; nY < m_nPunchesY; nY++)
	{
		for (int nX = 0; nX < m_nPunchesX; nX++)
		{
			if ( ! CheckToAdd(nX, nY, m_nPunchesX, m_nPunchesY))
				continue;

			float fCenterPosX = GetCenterPosX(nX, nY) + fProductWidth /2.0f;
			float fCenterPosY = GetCenterPosY(nX, nY) + fProductHeight/2.0f;
			int nHeadPosition = GetHeadPosition(nX, nY);

			rcBox = DrawSinglePunch(pDC, fCenterPosX, fCenterPosY, nHeadPosition, fProductWidth, fProductHeight, pFlatProduct, pPageTemplate, bShowBitmap);
		}
	}

	pDC->LPtoDP(&rcBox);
	rcBox.NormalizeRect();

	pDC->RestoreDC(-1);

	pDC->DPtoLP(&rcBox);

	return rcBox;
}

CRect CPunchingTool::DrawSinglePunch(CDC* pDC, float fCenterPosX, float fCenterPosY, int nHeadPosition, float fProductWidth, float fProductHeight, CFlatProduct* pFlatProduct, CPageTemplate* pPageTemplate, BOOL bShowBitmap)
{
	float fWidth  = (pFlatProduct) ? pFlatProduct->m_fTrimSizeWidth  : fProductWidth;
	float fHeight = (pFlatProduct) ? pFlatProduct->m_fTrimSizeHeight : fProductHeight;

	// Draw page
	CRect rect;
	rect.left   = CPrintSheetView::WorldToLP(fCenterPosX - fWidth /2.0f); 
	rect.bottom = CPrintSheetView::WorldToLP(fCenterPosY - fHeight/2.0f);
	rect.right = rect.left + CPrintSheetView::WorldToLP(fWidth);	rect.top = rect.bottom + CPrintSheetView::WorldToLP(fHeight);
	int nTextY = rect.top;

	CRect rcBox		= rect;
	CRect pageRect  = rect;

	//pageRect.NormalizeRect();
	rcBox.UnionRect(rcBox, pageRect);

	if (pPageTemplate && bShowBitmap)
	{
		if (pPageTemplate->m_ColorSeparations.GetSize() > 0)
		{
			int				   nColorDefinitionIndex = pPageTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex;
			CPageSource*	   pPageSource			 = pPageTemplate->GetObjectSource	   (nColorDefinitionIndex);
			CPageSourceHeader* pPageSourceHeader	 = pPageTemplate->GetObjectSourceHeader(nColorDefinitionIndex);
			if (pPageSource && pPageSourceHeader)
			{
				CRect outBBox, pageBBox = rect;
				int	  nOutBBoxWidth, nOutBBoxHeight;
				if ((pPageTemplate->m_fContentRotation == 0.0f) || (pPageTemplate->m_fContentRotation == 180.0f))
				{
					nOutBBoxWidth  = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxRight - pPageSourceHeader->m_fBBoxLeft)   * pPageTemplate->m_fContentScaleX);
					nOutBBoxHeight = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxTop   - pPageSourceHeader->m_fBBoxBottom) * pPageTemplate->m_fContentScaleY);
				}
				else
				{
					nOutBBoxWidth  = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxTop   - pPageSourceHeader->m_fBBoxBottom) * pPageTemplate->m_fContentScaleX);
					nOutBBoxHeight = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxRight - pPageSourceHeader->m_fBBoxLeft)   * pPageTemplate->m_fContentScaleY);
				}
				outBBox.left   = pageBBox.CenterPoint().x - nOutBBoxWidth  / 2;
				outBBox.bottom = pageBBox.CenterPoint().y - nOutBBoxHeight / 2;
				outBBox.right  = pageBBox.CenterPoint().x + nOutBBoxWidth  / 2;
				outBBox.top    = pageBBox.CenterPoint().y + nOutBBoxHeight / 2;
				outBBox		  += CSize(CPrintSheetView::WorldToLP(pPageTemplate->GetContentOffsetX()), CPrintSheetView::WorldToLP(pPageTemplate->GetContentOffsetY()));
				
				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if (pDoc)
				{
					POSITION pos = pDoc->m_PageSourceList.FindIndex(pPageTemplate->m_ColorSeparations[0].m_nPageSourceIndex);
					if (pos)
					{
						int nColorSel = 0;
						if (nColorSel >= 0 && nColorSel < pPageTemplate->m_ColorSeparations.GetSize())
						{
							CString strPreviewFile;
							strPreviewFile = pDoc->m_PageSourceList.GetAt(pos).GetPreviewFileName(pPageTemplate->m_ColorSeparations[nColorSel].m_nPageNumInSourceFile);
							//pDC->IntersectClipRect(pageRect);
							CPageListView::DrawPreview(pDC, outBBox, nHeadPosition, pPageTemplate->m_fContentRotation, strPreviewFile);
							//pDC->SelectClipRgn(NULL);
						}
					}
				}
			}
		}
	}

	//if ( ! bShowBitmap)
	{
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
		CPen	pen(PS_SOLID, 1, LIGHTGRAY);
		CPen*	pOldPen	  = pDC->SelectObject(&pen);

		CPrintSheetView::DrawRectangleExt(pDC, pageRect);
		CPrintSheetView::DrawHeadPosition(pDC, pageRect, nHeadPosition, LIGHTGRAY);

		CSize szLen(10,10);
		pDC->DPtoLP(&szLen);
		CPoint ptCrossCenter = pageRect.CenterPoint();
		pDC->MoveTo(ptCrossCenter.x - szLen.cx, ptCrossCenter.y);
		pDC->LineTo(ptCrossCenter.x + szLen.cx, ptCrossCenter.y);
		pDC->MoveTo(ptCrossCenter.x,			ptCrossCenter.y - szLen.cx);
		pDC->LineTo(ptCrossCenter.x,			ptCrossCenter.y + szLen.cx);

		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		pen.DeleteObject();
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	//if (pFlatProduct)
	//{
	//	CFont font;
	//	CSize szFont(1500, 1500);
	//	font.CreateFont(szFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	//	CFont* pOldFont = pDC->SelectObject(&font);

	//	pDC->SetBkMode(OPAQUE);
	//	pDC->SetBkColor(RGB(230,230,230));
	//	CSize szSize(3,3);
	//	pDC->DPtoLP(&szSize);
	//	pageRect.DeflateRect(szSize.cx, -szSize.cy);
	//	CString strOut;
	//	strOut.Format(_T(" %s "), pFlatProduct->m_strProductName);
	//	pDC->SetTextColor(DARKBLUE);
	//	pDC->SetBkMode(OPAQUE);
	//	pDC->SetBkColor(RGB(220,225,240));//RGB(220,220,220));
	//	pDC->DrawText(strOut, pageRect, DT_SINGLELINE|DT_BOTTOM|DT_LEFT);

	//	pDC->SelectObject(pOldFont);
	//	font.DeleteObject();
	//}

	return rcBox;
}

////////////////////////////////////////////////////////////////////////////
// CPunchingProcess
IMPLEMENT_SERIAL(CPunchingProcess, CObject, VERSIONABLE_SCHEMA | 1)

CPunchingProcess::CPunchingProcess()
{
}

CPunchingProcess::CPunchingProcess(const CPunchingProcess& rPunchingProcess)
{
	Copy(rPunchingProcess);	
}

const CPunchingProcess& CPunchingProcess::operator=(const CPunchingProcess& rPunchingProcess)
{
	Copy(rPunchingProcess);	

	return *this;
}

BOOL CPunchingProcess::operator==(const CPunchingProcess& rPunchingProcess)
{
	if (m_punchingTools.GetSize() != rPunchingProcess.m_punchingTools.GetSize())
		return FALSE;
	for (int i = 0; i < m_punchingTools.GetSize(); i++)
		if (m_punchingTools[i] != rPunchingProcess.m_punchingTools[i])
			return FALSE;

	return TRUE;
}

BOOL CPunchingProcess::operator!=(const CPunchingProcess& rPunchingProcess)
{
	return (*this == rPunchingProcess) ? FALSE : TRUE;
}

void CPunchingProcess::Copy(const CPunchingProcess& rPunchingProcess)
{
	m_punchingTools.RemoveAll();
	m_punchingTools.Append(rPunchingProcess.m_punchingTools);
}

void CPunchingProcess::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CPunchingProcess));

	if (ar.IsStoring())
	{
		//ar << ;
	}	
	else
	{ 
		UINT nVersion = 1;
		if (ar.IsLoading())
			nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:	//ar >> ;
				break;
		}
	}

	m_punchingTools.Serialize(ar);
}

BOOL CPunchingProcess::ApplyModifications(CLayout* pLayout, CPunchingProcess& rOldPunchingProcess, BOOL& bDoReImpose)
{
	if ( ! pLayout)
		return FALSE;

	BOOL bChanged = FALSE;

	if (*this != rOldPunchingProcess)
		bChanged = bDoReImpose = TRUE;

	// reimpose means layout must be rearranged

	return bChanged;
}

CPunchingTool& CPunchingProcess::GetPunchingTool(int nID)
{
	static CPunchingTool nullPunchingTool;

	for (int i = 0; i < m_punchingTools.GetSize(); i++)
	{
		if (m_punchingTools[i].m_nToolID == nID)
			return m_punchingTools[i];
	}

	return nullPunchingTool;
}

CRect CPunchingProcess::DrawData(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);
	int nIndent		= Pixel2MM(pDC, 10);

	//CImage img;
	//img.LoadFromResource(theApp.m_hInstance, IDB_FOLD_CUT);		
	//if ( ! img.IsNull())
	//{
	//	CRect rcImg;
	//	rcImg.left = rcFrame.left + 4; rcImg.top = rcFrame.top + 4; rcImg.right = rcImg.left + img.GetWidth(); rcImg.bottom = rcImg.top + img.GetHeight();
	//	img.TransparentBlt(pDC->m_hDC, rcImg, RGB(192,192,192));
	//}

	CFont boldFont, font;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	CRect rcText = rcFrame; rcText.top += 2; rcText.left += 5; rcText.bottom = rcText.top + nTextHeight;

	CString strOut; strOut.LoadString(IDS_PROCESS_PUNCHING);
	pDC->SetTextColor(GUICOLOR_CAPTION);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	//CPen pen(PS_SOLID, 1, LIGHTGRAY);
	//CPen* pOldPen = pDC->SelectObject(&pen);
	//pDC->MoveTo(rcText.left  - 2,	rcText.bottom + 3);
	//pDC->LineTo(rcText.right - 5,	rcText.bottom + 3);
	//pDC->SelectObject(pOldPen);
	//pen.DeleteObject();

	rcText.OffsetRect(0, nTextHeight + 15);

	pDC->SelectObject(&font);

	int nBorder = 5;
	CRect rcRow = rcText; rcRow.bottom = rcRow.top + 20;
	int	  nMaxRight = 0;
	for (int i = 0; i < m_punchingTools.GetSize(); i++)
	{
		CPunchingTool& rPunchingTool = m_punchingTools[i];

		pDC->SetTextColor(BLACK);

		rcText = rcRow; rcText.left += nBorder; 
		rcText.bottom = rcText.top + nTextHeight;

		pDC->SelectObject(&font);

		strOut.Format(_T("%s"), rPunchingTool.m_strToolName);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		rcRow.OffsetRect(0, rcRow.Height());
	}

	rcBox.UnionRect(rcBox, rcText);
	rcBox.bottom += 5;

	boldFont.DeleteObject();
	font.DeleteObject();

	return rcBox;
}

////////////////////////////////////////////////////////////////////////////
// CTrimmingProcess
IMPLEMENT_SERIAL(CTrimmingProcess, CObject, VERSIONABLE_SCHEMA | 2)

CTrimmingProcess::CTrimmingProcess()
{
	m_strID		 = _T("");

	m_fHeadTrim  = 0.0f;
	m_fSideTrim  = 0.0f;
	m_fFootTrim  = 0.0f;
	m_strMarkset = _T("");;

}

CTrimmingProcess::CTrimmingProcess(const CTrimmingProcess& rTrimmingProcess)
{
	Copy(rTrimmingProcess);	
}

const CTrimmingProcess& CTrimmingProcess::operator=(const CTrimmingProcess& rTrimmingProcess)
{
	Copy(rTrimmingProcess);	

	return *this;
}

BOOL CTrimmingProcess::operator==(const CTrimmingProcess& rTrimmingProcess)
{
	if (m_fHeadTrim != rTrimmingProcess.m_fHeadTrim)					return FALSE;
	if (m_fSideTrim != rTrimmingProcess.m_fSideTrim)					return FALSE;
	if (m_fFootTrim != rTrimmingProcess.m_fFootTrim)					return FALSE;
	if (m_strMarkset.CompareNoCase(rTrimmingProcess.m_strMarkset) != 0)	return FALSE;

	return TRUE;
}

BOOL CTrimmingProcess::operator!=(const CTrimmingProcess& rTrimmingProcess)
{
	return (*this == rTrimmingProcess) ? FALSE : TRUE;
}

void CTrimmingProcess::Copy(const CTrimmingProcess& rTrimmingProcess)
{
	m_strID		 = rTrimmingProcess.m_strID;
	m_fHeadTrim  = rTrimmingProcess.m_fHeadTrim;
	m_fSideTrim  = rTrimmingProcess.m_fSideTrim;
	m_fFootTrim  = rTrimmingProcess.m_fFootTrim;
	m_strMarkset = rTrimmingProcess.m_strMarkset;
}

void CTrimmingProcess::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CTrimmingProcess));

	if (ar.IsStoring())
	{
		ar << m_fHeadTrim;
		ar << m_fSideTrim;
		ar << m_fFootTrim;
		ar << m_strMarkset;
	}	
	else
	{ 
		UINT nVersion = 1;
		if (ar.IsLoading())
			nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:	ar >> m_fHeadTrim;
				ar >> m_fSideTrim;
				ar >> m_fFootTrim;
				break;
		case 2:	ar >> m_fHeadTrim;
				ar >> m_fSideTrim;
				ar >> m_fFootTrim;
				ar >> m_strMarkset;
				break;
		}
	}
}

BOOL CTrimmingProcess::ApplyModifications(CLayout* pLayout, CTrimmingProcess& rOldTrimmingProcess, BOOL& bDoReImpose)
{
	if ( ! pLayout)
		return FALSE;

	BOOL bChanged = FALSE;

	if (m_fHeadTrim != rOldTrimmingProcess.m_fHeadTrim)
		bChanged = bDoReImpose = TRUE;
	if (m_fSideTrim != rOldTrimmingProcess.m_fSideTrim)
		bChanged = bDoReImpose = TRUE;
	if (m_fFootTrim != rOldTrimmingProcess.m_fFootTrim)
		bChanged = bDoReImpose = TRUE;
	if (m_strMarkset.CompareNoCase(rOldTrimmingProcess.m_strMarkset) != 0)
		bChanged = bDoReImpose = TRUE;

	// reimpose means layout must be rearranged

	return bChanged;
}

CRect CTrimmingProcess::DrawData(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPrintComponent& rComponent, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);
	int nIndent		= Pixel2MM(pDC, 10);

	CFont boldFont, font;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	rcFrame.top  += 5;
	rcFrame.left += 5;
	CRect rcText = rcFrame; rcText.top += 2; rcText.bottom = rcText.top + nTextHeight;

	CImage img; img.LoadFromResource(theApp.m_hInstance, IDB_TRIMMING);		
	if ( ! img.IsNull())
	{
		CRect rcImg; rcImg.left = rcFrame.left; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcFrame.top + 2; rcImg.bottom = rcImg.top + img.GetHeight();
		img.TransparentBlt(pDC->m_hDC, rcImg, RGB(192,192,192));
		rcText.left = rcFrame.left + 32;
		rcBox.bottom = rcImg.bottom;
	}

	CString strOut; strOut.LoadString(IDS_PROCESS_TRIMMING);
	pDC->SetTextColor(BLACK);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	rcText.OffsetRect(0, nTextHeight + 5);

	pDC->SetTextColor(GUICOLOR_CAPTION);

	CBoundProductPart* pProductPart = rComponent.GetBoundProductPart();
	if (pProductPart && (pDoc->m_boundProducts.GetSize() > 1))
	{
		strOut = pProductPart->GetBoundProduct().m_strProductName;
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		rcText.OffsetRect(0, nTextHeight + 5);
	}

	rcText.left += 2;

	pDC->SelectObject(&font);

	if ( (fabs(m_fHeadTrim) > 0.01f) || (fabs(m_fFootTrim) > 0.01f) || (fabs(m_fSideTrim) > 0.01f) )
	{
		CString strTrims, strHeadTrim, strFootTrim, strSideTrim;
		strTrims.LoadString(IDS_TRIMS);
		strHeadTrim.LoadString(IDS_HEAD);	strHeadTrim += _T(":");
		strFootTrim.LoadString(IDS_FOOT);	strFootTrim += _T(":");
		strSideTrim.LoadString(IDS_SIDE);	strSideTrim += _T(":");
		int	nTab = 65;//max(pDC->GetTextExtent(strHeadTrim).cx, pDC->GetTextExtent(strFootTrim).cx);

		pDC->SetTextColor(DARKGRAY); 
		CRect rcText2 = rcText; rcText2.OffsetRect(nTab + 5, 0);
		CRect rcTrimsBox(rcText.TopLeft(), CSize(1, 1));

		DrawTextMeasured(pDC, strHeadTrim, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
		pDC->SetTextColor(BLACK);
		strOut.Format(_T("%.1f"), m_fHeadTrim);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
		
		rcText.OffsetRect(0, nTextHeight); 
		rcText2.OffsetRect(0, nTextHeight);
		pDC->SetTextColor(DARKGRAY); 
		DrawTextMeasured(pDC, strFootTrim, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
		pDC->SetTextColor(BLACK);
		strOut.Format(_T("%.1f"), m_fFootTrim);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
		rcBox.UnionRect(rcBox, rcText2);
		
		//rcText.OffsetRect (rcTrimsBox.right - rcText.left + 20,  rcTrimsBox.top - rcText.top); rcText2 = rcText; rcText2.OffsetRect(nTab + 5, 0);
		rcText.left = rcFrame.left + 135; rcText.OffsetRect (0, rcTrimsBox.top - rcText.top); rcText2 = rcText; rcText2.left += nTab - 15;
		pDC->SetTextColor(DARKGRAY); 
		DrawTextMeasured(pDC, strSideTrim, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
		pDC->SetTextColor(BLACK);
		strOut.Format(_T("%.1f"), m_fSideTrim);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);

		rcBox.UnionRect(rcBox, rcText2);
	}

	rcBox.bottom += 5;

	boldFont.DeleteObject();
	font.DeleteObject();

	return rcBox;
}


/////////////////////////////////////////////////////////////////////////////
// CBindingProcess
IMPLEMENT_SERIAL(CBindingProcess, CObject, VERSIONABLE_SCHEMA | 4)

CBindingProcess::CBindingProcess()
{
	m_strID					= _T("");

	m_fSpineArea			= 0.0f;
	m_bSpineAuto			= FALSE;
	m_fMillingDepth			= 0.0f;
	m_fGlueLineWidth		= 0.0f;
	m_fOverFold				= 0.0f;
	m_nOverFoldSide			= OverFoldNone;
	m_bComingAndGoing		= FALSE;
	m_strMarkset			= _T("");
}

CBindingProcess::CBindingProcess(const CBindingProcess& rBindingProcess)
{
	Copy(rBindingProcess);	
}

const CBindingProcess& CBindingProcess::operator=(const CBindingProcess& rBindingProcess)
{
	Copy(rBindingProcess);	

	return *this;
}

BOOL CBindingProcess::operator==(const CBindingProcess& rBindingProcess)
{
	if (m_bindingDefs.m_strName		!= rBindingProcess.m_bindingDefs.m_strName)		return FALSE;
	if (m_bindingDefs.m_nBinding	!= rBindingProcess.m_bindingDefs.m_nBinding)	return FALSE;
	if (m_fSpineArea				!= rBindingProcess.m_fSpineArea)				return FALSE;
	if (m_bSpineAuto				!= rBindingProcess.m_bSpineAuto)				return FALSE;
	if (m_fMillingDepth				!= rBindingProcess.m_fMillingDepth)				return FALSE;
	if (m_fGlueLineWidth			!= rBindingProcess.m_fGlueLineWidth)			return FALSE;
	if (m_fOverFold					!= rBindingProcess.m_fOverFold)					return FALSE;
	if (m_nOverFoldSide				!= rBindingProcess.m_nOverFoldSide)				return FALSE;
	if (m_bComingAndGoing			!= rBindingProcess.m_bComingAndGoing)			return FALSE;
	if (m_strMarkset.CompareNoCase(rBindingProcess.m_strMarkset) != 0)				return FALSE;

	return TRUE;
}

BOOL CBindingProcess::operator!=(const CBindingProcess& rBindingProcess)
{
	return (*this == rBindingProcess) ? FALSE : TRUE;
}

void CBindingProcess::Copy(const CBindingProcess& rBindingProcess)
{
	m_strID					= rBindingProcess.m_strID;
	m_bindingDefs			= rBindingProcess.m_bindingDefs;
	m_fSpineArea			= rBindingProcess.m_fSpineArea;
	m_bSpineAuto			= rBindingProcess.m_bSpineAuto;
	m_fMillingDepth			= rBindingProcess.m_fMillingDepth;
	m_fGlueLineWidth		= rBindingProcess.m_fGlueLineWidth;
	m_fOverFold				= rBindingProcess.m_fOverFold;
	m_nOverFoldSide			= rBindingProcess.m_nOverFoldSide;
	m_bComingAndGoing		= rBindingProcess.m_bComingAndGoing;
	m_strMarkset			= rBindingProcess.m_strMarkset;
}

void CBindingProcess::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CBindingProcess));

	if (ar.IsStoring())
	{
		SerializeElements(ar, &m_bindingDefs, 1);
		ar << m_fSpineArea;
		ar << m_bSpineAuto;
		ar << m_fMillingDepth;
		ar << m_fGlueLineWidth;
		ar << m_fOverFold;
		ar << m_nOverFoldSide;
		ar << m_bComingAndGoing;
		ar << m_strMarkset;
	}	
	else
	{ 
		UINT nVersion = 1;
		if (ar.IsLoading())
			nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:	m_bindingDefs.Serialize(ar);
				ar >> m_fSpineArea;
				ar >> m_bSpineAuto;
				ar >> m_fMillingDepth;
				ar >> m_fGlueLineWidth;
				ar >> m_fOverFold;
				ar >> m_nOverFoldSide;
				break;
		case 2:	m_bindingDefs.Serialize(ar);
				ar >> m_fSpineArea;
				ar >> m_bSpineAuto;
				ar >> m_fMillingDepth;
				ar >> m_fGlueLineWidth;
				ar >> m_fOverFold;
				ar >> m_nOverFoldSide;
				ar >> m_bComingAndGoing;
				break;
		case 3:	SerializeElements(ar, &m_bindingDefs, 1);
				ar >> m_fSpineArea;
				ar >> m_bSpineAuto;
				ar >> m_fMillingDepth;
				ar >> m_fGlueLineWidth;
				ar >> m_fOverFold;
				ar >> m_nOverFoldSide;
				ar >> m_bComingAndGoing;
				break;
		case 4:	SerializeElements(ar, &m_bindingDefs, 1);
				ar >> m_fSpineArea;
				ar >> m_bSpineAuto;
				ar >> m_fMillingDepth;
				ar >> m_fGlueLineWidth;
				ar >> m_fOverFold;
				ar >> m_nOverFoldSide;
				ar >> m_bComingAndGoing;
				ar >> m_strMarkset;
				break;
		}
	}
}

BOOL CBindingProcess::ApplyModifications(CLayout* pLayout, CBindingProcess& rOldBindingProcess, BOOL& bDoReImpose)
{
	if ( ! pLayout)
		return FALSE;

	BOOL bChanged = FALSE;

	if (m_fSpineArea != rOldBindingProcess.m_fSpineArea)
	{
		pLayout->AnalyzeMarks();	// spine area changed -> section marks could be effected
		bChanged = bDoReImpose = TRUE;
	}
	if (m_bSpineAuto != rOldBindingProcess.m_bSpineAuto)
	{
		pLayout->AnalyzeMarks();	// spine auto changed -> section marks could be effected
		bChanged = bDoReImpose = TRUE;
	}
	if (m_fMillingDepth	!= rOldBindingProcess.m_fMillingDepth)
	{
		pLayout->AnalyzeMarks();	// milling depth changed -> section marks could be effected
		bChanged = bDoReImpose = TRUE;
	}
	if (m_fGlueLineWidth != rOldBindingProcess.m_fGlueLineWidth)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			pDoc->m_PageTemplateList.ReorganizeGluelineScaling();
			pDoc->m_PrintSheetList.InvalidateAllPlates();
		}
		bChanged = bDoReImpose = TRUE;
	}
	if (m_fOverFold	!= rOldBindingProcess.m_fOverFold)
		bChanged = bDoReImpose = TRUE;
	if (m_nOverFoldSide	!= rOldBindingProcess.m_nOverFoldSide)
		bChanged = bDoReImpose = TRUE;
	if (m_bComingAndGoing	!= rOldBindingProcess.m_bComingAndGoing)
		bChanged = bDoReImpose = TRUE;
	if (m_strMarkset.CompareNoCase(rOldBindingProcess.m_strMarkset) != 0)
		bChanged = bDoReImpose = TRUE;

	// reimpose means layout must be rearranged

	return bChanged;
}

float CBindingProcess::GetSpine(CBoundProductPart& rProductPart) 
{ 
	if (rProductPart.IsCover())
	{
		if (m_bSpineAuto)
		{
			CBoundProduct& rProduct = rProductPart.GetBoundProduct();
			for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
			{
				if (rProduct.m_parts[i].IsCover())
					continue;
				float fSpine = (rProduct.m_parts[i].m_nNumPages/2) * rProduct.m_parts[i].GetShinglingValueX(TRUE);
				fSpine = (float)((int)(fSpine + 0.5f));
				return fSpine;
			}
		}
		else
			return m_fSpineArea; 
	}
	else
		return m_fMillingDepth * 2.0f;

	return 0.0f;
}

float CBindingProcess::GetOverfold(int nSide)
{
	switch (nSide)
	{
	case LEFT:	if (m_nOverFoldSide == CBindingProcess::OverFoldFront)
					return m_fOverFold;
				break;
	case RIGHT:	if (m_nOverFoldSide == CBindingProcess::OverFoldBack)
					return m_fOverFold;
				break;
	}

	return 0.0f;
}

CRect CBindingProcess::DrawData(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPrintComponent& rComponent, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);
	int nIndent		= Pixel2MM(pDC, 10);

	BOOL bHasCoverProduct		 = (pPrintSheet) ? pPrintSheet->HasCoverProduct()		 : TRUE;
	BOOL bHasPerfectBoundProduct = (pPrintSheet) ? pPrintSheet->HasPerfectBoundProduct() : TRUE;

	CBoundProductPart* pProductPart = rComponent.GetBoundProductPart();

	CFont boldFont, font;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	rcFrame.top  += 5;
	rcFrame.left += 5;
	CRect rcText = rcFrame; rcText.top += 2; rcText.bottom = rcText.top + nTextHeight;

	int nIDB = IDB_BINDING;
	if (pProductPart)
		nIDB = (pProductPart->GetBoundProduct().m_bindingParams.m_bindingDefs.m_nBinding == CBindingDefs::SaddleStitched) ? IDB_SADDLE_STITCHED : IDB_PERFECT_BOUND;

	CImage img; img.LoadFromResource(theApp.m_hInstance, nIDB);		
	if ( ! img.IsNull())
	{
		CRect rcImg; rcImg.left = rcFrame.left; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcFrame.top + 5; rcImg.bottom = rcImg.top + img.GetHeight();
		img.TransparentBlt(pDC->m_hDC, rcImg, RGB(192,192,192));
		rcText.left = rcFrame.left + 32;
		rcBox.bottom = rcImg.bottom;
	}

	CString strOut; strOut.LoadString(IDS_PROCESS_BINDING);
	pDC->SetTextColor(BLACK);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	rcText.OffsetRect(0, nTextHeight + 5);

	pDC->SetTextColor(GUICOLOR_CAPTION);

	if (pProductPart && (pDoc->m_boundProducts.GetSize() > 1) )
	{
		strOut = pProductPart->GetBoundProduct().m_strProductName;
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		rcText.OffsetRect(0, nTextHeight + 5);
	}

	pDC->SelectObject(&font);

	CString strType, strSpine, strMilling, strGlueLine, strOverfold, strComingAndGoing;
	strType.LoadString(IDS_BINDINGSTYLE_LABEL); 
	strSpine.LoadString(IDS_SPINE);
	strMilling.LoadString(IDS_MILLING_EDGE);
	strGlueLine.LoadString(IDS_GLUELINE); strGlueLine += _T(":");
	strOverfold.LoadString((m_nOverFoldSide == CBindingProcess::OverFoldFront) ? IDS_OVERFOLD_FRONT : IDS_OVERFOLD_BACK);	strOverfold += _T(":");
	strComingAndGoing.LoadString(IDS_COMING_AND_GOING);
	int nTab = max(pDC->GetTextExtent(strSpine).cx, pDC->GetTextExtent(strMilling).cx);
		nTab = 65;//max(nTab, pDC->GetTextExtent(strOverfold).cx);

	CRect rcText2 = rcText; rcText2.left += nTab + Pixel2MM(pDC, 5); 
	CRect rcBox2 = CRect(rcText.TopLeft(), CSize(1, 1));

	pDC->SetTextColor(BLACK);
	if (pProductPart)
	{
		strOut = pProductPart->GetBoundProduct().m_bindingParams.m_bindingDefs.m_strName;
		pDC->SetTextColor(DARKGRAY); 
		pDC->DrawText(strType, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		pDC->SetTextColor(BLACK);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox2);
		rcBox.UnionRect(rcBox, rcText2);
		rcText.OffsetRect(0, nTextHeight);
		rcText2.OffsetRect(0, nTextHeight); 
	}

	if (bHasCoverProduct && ( (fabs(m_fSpineArea) > 0.01f) || m_bSpineAuto) )
	{
		pDC->SetTextColor(DARKGRAY); 
		pDC->DrawText(strSpine, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		pDC->SetTextColor(BLACK);
		if (m_bSpineAuto)
			strOut.Format(_T("%.1f (auto)"), m_fSpineArea);
		else
			strOut.Format(_T("%.1f"), m_fSpineArea);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox2);
		rcBox.UnionRect(rcBox, rcText2);
		rcText.OffsetRect(0, nTextHeight); 
		rcText2.OffsetRect(0, nTextHeight); 
	}

	BOOL bMilling  = FALSE;
	BOOL bGlueline = FALSE;
	if (bHasPerfectBoundProduct && (fabs(m_fMillingDepth) > 0.01f))
	{
		pDC->SetTextColor(DARKGRAY); 
		pDC->DrawText(strMilling, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		pDC->SetTextColor(BLACK);
		strOut.Format(_T("%.1f"), m_fMillingDepth);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox2);
		rcBox.UnionRect(rcBox, rcText2);
		bMilling = TRUE;
	}

	if (fabs(m_fGlueLineWidth) > 0.01f)
	{
		if (bMilling)
		{
			rcText.left = rcFrame.left + 135; rcText2 = rcText; rcText2.left += nTab - 15;
		}
		pDC->SetTextColor(DARKGRAY); 
		DrawTextMeasured(pDC, strGlueLine, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox2);
		pDC->SetTextColor(BLACK);
		strOut.Format(_T("%.1f"), m_fGlueLineWidth);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox2);
		rcBox.UnionRect(rcBox, rcText2);
		bGlueline = TRUE;
	}

	if (bMilling || bGlueline)
	{
		rcText.OffsetRect(0, nTextHeight); 
		rcText2.OffsetRect(0, nTextHeight); 
	}

	if (m_nOverFoldSide != CBindingProcess::OverFoldNone)
	{
		rcText.OffsetRect ((rcFrame.left + 32) - rcText.left,  0); rcText2 = rcText; rcText2.left += nTab + 5;
		pDC->SetTextColor(DARKGRAY); 
		pDC->DrawText(strOverfold, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		pDC->SetTextColor(BLACK);
		strOut.Format(_T("%.1f"), m_fOverFold);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox2);
		rcBox.UnionRect(rcBox, rcText2);
		rcText.OffsetRect(0, nTextHeight); 
		rcText2.OffsetRect(0, nTextHeight); 
	}

	if (m_bComingAndGoing)
	{
		pDC->SetTextColor(BLACK); 
		pDC->DrawText(strComingAndGoing, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText2);
	}

	rcBox.bottom += 5;

	boldFont.DeleteObject();
	font.DeleteObject();

	return rcBox;
}


/////////////////////////////////////////////////////////////////////////////
// CFoldingProcess
IMPLEMENT_SERIAL(CFoldingProcess, CObject, VERSIONABLE_SCHEMA | 3)

CFoldingProcess::CFoldingProcess()
{
	m_strID					= _T("");

	m_bShinglingActive		= FALSE;
	m_nShinglingMethod		= 0;
	m_nShinglingDirection	= CBookblock::ShinglingDirectionSpine;
	m_fShinglingValueX		= 0.0f;
	m_fShinglingValueXFoot	= 0.0f;
	m_fShinglingValueY		= 0.0f;
	m_bShinglingCropMarks	= FALSE;
	m_strMarkset			= _T("");
}

CFoldingProcess::CFoldingProcess(const CFoldingProcess& rFoldingProcess)
{
	Copy(rFoldingProcess);	
}

const CFoldingProcess& CFoldingProcess::operator=(const CFoldingProcess& rFoldingProcess)
{
	Copy(rFoldingProcess);	

	return *this;
}

BOOL CFoldingProcess::operator==(const CFoldingProcess& rFoldingProcess)
{
	if (m_foldSchemeList.GetSize() != rFoldingProcess.m_foldSchemeList.GetSize())
		return FALSE;
	POSITION pos  = m_foldSchemeList.GetHeadPosition();
	POSITION pos1 = rFoldingProcess.m_foldSchemeList.GetHeadPosition();
	while (pos && pos1)
	{
		CFoldSheet foldSheet  = m_foldSchemeList.GetNext(pos);
		CFoldSheet foldSheet1 = rFoldingProcess.m_foldSchemeList.GetNext(pos1);

		if (foldSheet.m_strFoldSheetTypeName != foldSheet1.m_strFoldSheetTypeName)
			return FALSE;
	}

	if (m_bShinglingActive		!= rFoldingProcess.m_bShinglingActive)		return FALSE;
	if (m_nShinglingMethod		!= rFoldingProcess.m_nShinglingMethod)		return FALSE;
	if (m_nShinglingDirection	!= rFoldingProcess.m_nShinglingDirection)	return FALSE;
	if (m_fShinglingValueX		!= rFoldingProcess.m_fShinglingValueX)		return FALSE;
	if (m_fShinglingValueXFoot	!= rFoldingProcess.m_fShinglingValueXFoot)	return FALSE;
	if (m_fShinglingValueY		!= rFoldingProcess.m_fShinglingValueY)		return FALSE;
	if (m_bShinglingCropMarks	!= rFoldingProcess.m_bShinglingCropMarks)	return FALSE;
	if (m_strMarkset.CompareNoCase(rFoldingProcess.m_strMarkset) != 0)		return FALSE;

	return TRUE;
}

BOOL CFoldingProcess::operator!=(const CFoldingProcess& rFoldingProcess)
{
	return (*this == rFoldingProcess) ? FALSE : TRUE;
}

void CFoldingProcess::Copy(const CFoldingProcess& rFoldingProcess)
{
	m_strID	= rFoldingProcess.m_strID;

	m_foldSchemeList.RemoveAll();
	POSITION pos = rFoldingProcess.m_foldSchemeList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet foldSheet = rFoldingProcess.m_foldSchemeList.GetNext(pos);
		m_foldSchemeList.AddTail(foldSheet);
	}

	m_bShinglingActive		= rFoldingProcess.m_bShinglingActive;
	m_nShinglingMethod		= rFoldingProcess.m_nShinglingMethod;
	m_nShinglingDirection	= rFoldingProcess.m_nShinglingDirection;
	m_fShinglingValueX		= rFoldingProcess.m_fShinglingValueX;
	m_fShinglingValueXFoot	= rFoldingProcess.m_fShinglingValueXFoot;
	m_fShinglingValueY		= rFoldingProcess.m_fShinglingValueY;
	m_bShinglingCropMarks	= rFoldingProcess.m_bShinglingCropMarks;
	m_strMarkset			= rFoldingProcess.m_strMarkset;
}

void CFoldingProcess::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CFoldingProcess));

	if (ar.IsStoring())
	{
		ar << m_bShinglingActive;
		ar << m_nShinglingMethod;
		ar << m_nShinglingDirection;
		ar << m_fShinglingValueX;
		ar << m_fShinglingValueXFoot;
		ar << m_fShinglingValueY;
		ar << m_bShinglingCropMarks;
		ar << m_strMarkset;
	}	
	else
	{ 
		UINT nVersion = 1;
		if (ar.IsLoading())
			nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:	ar >> m_bShinglingActive;
				ar >> m_nShinglingMethod;
				ar >> m_fShinglingValueX;
				ar >> m_fShinglingValueXFoot;
				ar >> m_bShinglingCropMarks;
				m_fShinglingValueY = 0.0f;
				break;
		case 2:	ar >> m_bShinglingActive;
				ar >> m_nShinglingMethod;
				ar >> m_nShinglingDirection;
				ar >> m_fShinglingValueX;
				ar >> m_fShinglingValueXFoot;
				ar >> m_fShinglingValueY;
				ar >> m_bShinglingCropMarks;
				break;
		case 3:	ar >> m_bShinglingActive;
				ar >> m_nShinglingMethod;
				ar >> m_nShinglingDirection;
				ar >> m_fShinglingValueX;
				ar >> m_fShinglingValueXFoot;
				ar >> m_fShinglingValueY;
				ar >> m_bShinglingCropMarks;
				ar >> m_strMarkset;
				break;
		}
	}

	m_foldSchemeList.Serialize(ar);
}

// find biggest foldsheet where all pages to impose can be placed on
CFoldSheet&	CFoldingProcess::FindBestFoldScheme(int nNumPagesToImpose, CPrintSheet* pPrintSheet, CProductionProfileLayoutPool& rLayoutPool, int& nPoolIndex)
{
	static private CFoldSheet nullFoldSheet;

	if ( ! pPrintSheet)
		return nullFoldSheet;

	int		 nMaxFoldSheetPages = INT_MIN;
	POSITION posBest = NULL;
	POSITION pos	 = m_foldSchemeList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_foldSchemeList.GetAt(pos);
		int nNumPages = rFoldSheet.GetNumPages();
		if (nNumPages <= nNumPagesToImpose)
		{
			if (rLayoutPool.GetSize() <= 0)
			{
				float fXPos, fYPos;
				int nNumFoldSheetsX, nNumFoldSheetsY;
				BOOL bRotateBest = pPrintSheet->FindFreePosition(&rFoldSheet, 0, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY, NULL, TRUE);
				if ( (nNumFoldSheetsX * nNumFoldSheetsY) > 0)
					if (nNumPages > nMaxFoldSheetPages)
					{
						posBest = pos;
						nMaxFoldSheetPages = nNumPages;
					}
			}
			else
			{
				if (nNumPages > nMaxFoldSheetPages)
				{
					posBest = pos;
					nMaxFoldSheetPages = nNumPages;
					nPoolIndex = rLayoutPool.FindSuitableLayout(rFoldSheet, nNumPagesToImpose);
				}
			}
		}
		m_foldSchemeList.GetNext(pos);
	}

	if (posBest)
		return m_foldSchemeList.GetAt(posBest);
	else
		return nullFoldSheet;
}

// find biggest foldsheet where all pages to impose can be placed on (just pages w/o printsheet and layout)
CFoldSheet&	CFoldingProcess::FindBestFoldScheme(int nNumPagesToImpose, CSize matrix)
{
	static private CFoldSheet nullFoldSheet;

	int		 nMaxFoldSheetPages = INT_MIN;
	POSITION posBest = NULL;
	POSITION pos	 = m_foldSchemeList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_foldSchemeList.GetAt(pos);
		int nNumPages = rFoldSheet.GetNumPages();
		if (nNumPages <= nNumPagesToImpose)
		{
			BOOL bSkip = FALSE;
			if (matrix != CSize(0, 0))
			{
				CSize curMatrix = rFoldSheet.GetMatrix();
				if (curMatrix != matrix)
					bSkip = TRUE;
			}
			if ( ! bSkip)
			{
				if (nNumPages > nMaxFoldSheetPages)
				{
					posBest = pos;
					nMaxFoldSheetPages = nNumPages;
				}
			}
		}
		m_foldSchemeList.GetNext(pos);
	}

	if (posBest)
		return m_foldSchemeList.GetAt(posBest);
	else
		return nullFoldSheet;
}

float CFoldingProcess::GetShinglingValueX(CBoundProductPart& rProductPart)
{
	return m_fShinglingValueX;//rProductPart.GetShinglingValue();
}

float CFoldingProcess::GetShinglingValueXFoot(CBoundProductPart& rProductPart)
{
	return m_fShinglingValueXFoot;//rProductPart.GetShinglingValue();
}

float CFoldingProcess::GetShinglingValueY(CBoundProductPart& rProductPart)
{
	return m_fShinglingValueY;//rProductPart.GetShinglingValue();
}

CRect CFoldingProcess::DrawData(CDC* pDC, CRect rcFrame, CFoldSheet* pFoldSheet, CBoundProductPart* pProductPart, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int		nTextHeight = 15;
	int		nIndent		= 32;
	CString strOut;

	CFont boldFont, font;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	rcFrame.top  += 5;
	rcFrame.left += 5;
	CRect rcText = rcFrame; rcText.top += 2; rcText.bottom = rcText.top + nTextHeight;

	if (pFoldSheet)
	{
		CRect rcImg; rcImg.left = rcFrame.left + 2; rcImg.right = rcImg.left + 22; rcImg.top = rcFrame.top + 7; rcImg.bottom = rcImg.top + 35;
		pFoldSheet->DrawThumbnail(pDC, rcImg, TRUE, FALSE);
	}
	rcText.left = rcFrame.left + nIndent;

	pDC->SelectObject(&boldFont);
	strOut.LoadString(IDS_PROCESS_FOLDING);
	pDC->SetTextColor(GUICOLOR_CAPTION);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	rcBox.bottom += 5;

	pDC->SelectObject(&font);
	if (m_bShinglingActive)
	{
		pDC->SelectObject(&font);
		pDC->SetTextColor(DARKGRAY);

		rcText.OffsetRect(0, nTextHeight + 5); 

		CString strOut;
		strOut.LoadString(IDS_SHINGLING_ACTIVE); strOut += _T(":");
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		rcText.left += pDC->GetTextExtent(strOut).cx + 10;

		//pDC->SelectObject(&boldFont);
		pDC->SetTextColor(BLACK);

		switch (m_nShinglingMethod)
		{
		case CBookblock::ShinglingMethodMove:		strOut.LoadString(IDS_MOVE);	break;
		case CBookblock::ShinglingMethodScale:		strOut.LoadString(IDS_SCALE);	break;
		}

		pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);
	}

	rcBox.UnionRect(rcBox, rcText);
	rcBox.bottom += 5;

	boldFont.DeleteObject();
	font.DeleteObject();

	return rcBox;
}



/////////////////////////////////////////////////////////////////////////////
// CCuttingProcess
IMPLEMENT_SERIAL(CCuttingProcess, CObject, VERSIONABLE_SCHEMA | 2)

CCuttingProcess::CCuttingProcess()
{
	m_strID				= _T("");

	m_nBaseCutDirection	= VERTICAL;
	m_nBaseCutAlignment	= BOTTOM;
	m_fLeftBorder		= 0.0f;
	m_fLowerBorder		= 0.0f;
	m_fRightBorder		= 0.0f;
	m_fUpperBorder		= 0.0f;
	m_strMarkset		= _T("");
}

CCuttingProcess::CCuttingProcess(const CCuttingProcess& rCuttingProcess)
{
	Copy(rCuttingProcess);	
}

const CCuttingProcess& CCuttingProcess::operator=(const CCuttingProcess& rCuttingProcess)
{
	Copy(rCuttingProcess);	

	return *this;
}

BOOL CCuttingProcess::operator==(const CCuttingProcess& rCuttingProcess)
{
	if (m_nBaseCutDirection	!= rCuttingProcess.m_nBaseCutDirection)		return FALSE;
	if (m_nBaseCutAlignment	!= rCuttingProcess.m_nBaseCutAlignment)		return FALSE;
	if (m_fLeftBorder		!= rCuttingProcess.m_fLeftBorder)			return FALSE;
	if (m_fLowerBorder		!= rCuttingProcess.m_fLowerBorder)			return FALSE;
	if (m_fRightBorder		!= rCuttingProcess.m_fRightBorder)			return FALSE;
	if (m_fUpperBorder		!= rCuttingProcess.m_fUpperBorder)			return FALSE;
	if (m_strMarkset.CompareNoCase(rCuttingProcess.m_strMarkset) != 0)	return FALSE;

	return TRUE;
}

BOOL CCuttingProcess::operator!=(const CCuttingProcess& rCuttingProcess)
{
	return (*this == rCuttingProcess) ? FALSE : TRUE;
}

void CCuttingProcess::Copy(const CCuttingProcess& rCuttingProcess)
{
	m_strID				= rCuttingProcess.m_strID;
	m_nBaseCutDirection	= rCuttingProcess.m_nBaseCutDirection;
	m_nBaseCutAlignment	= rCuttingProcess.m_nBaseCutAlignment;
	m_fLeftBorder		= rCuttingProcess.m_fLeftBorder;
	m_fLowerBorder		= rCuttingProcess.m_fLowerBorder;
	m_fRightBorder		= rCuttingProcess.m_fRightBorder;
	m_fUpperBorder		= rCuttingProcess.m_fUpperBorder;
	m_strMarkset		= rCuttingProcess.m_strMarkset;
}

void CCuttingProcess::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CCuttingProcess));

	if (ar.IsStoring())
	{
		ar << m_nBaseCutDirection;
		ar << m_nBaseCutAlignment;
		ar << m_fLeftBorder;	
		ar << m_fLowerBorder;
		ar << m_fRightBorder;
		ar << m_fUpperBorder;
		ar << m_strMarkset;
	}	
	else
	{ 
		UINT nVersion = 1;
		if (ar.IsLoading())
			nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:	ar >> m_nBaseCutDirection;
				ar >> m_nBaseCutAlignment;
				ar >> m_fLeftBorder;	
				ar >> m_fLowerBorder;
				ar >> m_fRightBorder;
				ar >> m_fUpperBorder;
				break;
		case 2:	ar >> m_nBaseCutDirection;
				ar >> m_nBaseCutAlignment;
				ar >> m_fLeftBorder;	
				ar >> m_fLowerBorder;
				ar >> m_fRightBorder;
				ar >> m_fUpperBorder;
				ar >> m_strMarkset;
				break;
		}
	}
}

BOOL CCuttingProcess::ApplyModifications(CLayout* pLayout, CCuttingProcess& rOldCuttingProcess, BOOL& bDoReImpose)
{
	if ( ! pLayout)
		return FALSE;

	BOOL bChanged = FALSE;

	if (m_nBaseCutDirection != rOldCuttingProcess.m_nBaseCutDirection)
		bChanged = bDoReImpose = TRUE;
	if (m_nBaseCutAlignment != rOldCuttingProcess.m_nBaseCutAlignment)
		bChanged = bDoReImpose = TRUE;
	if (m_fLeftBorder != rOldCuttingProcess.m_fLeftBorder)
		bChanged = bDoReImpose = TRUE;
	if (m_fLowerBorder != rOldCuttingProcess.m_fLowerBorder)
		bChanged = bDoReImpose = TRUE;
	if (m_fRightBorder != rOldCuttingProcess.m_fRightBorder)
		bChanged = bDoReImpose = TRUE;
	if (m_fUpperBorder != rOldCuttingProcess.m_fUpperBorder)
		bChanged = bDoReImpose = TRUE;
	if (m_strMarkset.CompareNoCase(rOldCuttingProcess.m_strMarkset) != 0)
		bChanged = bDoReImpose = TRUE;

	return bChanged;
}

CRect CCuttingProcess::DrawData(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);
	int nIndent		= Pixel2MM(pDC, 10);

	//CImage img;
	//img.LoadFromResource(theApp.m_hInstance, IDB_CUTMACHINE);		
	//if ( ! img.IsNull())
	//{
	//	CRect rcImg;
	//	rcImg.left = rcFrame.left + 4; rcImg.top = rcFrame.top + 4; rcImg.right = rcImg.left + img.GetWidth(); rcImg.bottom = rcImg.top + img.GetHeight();
	//	img.TransparentBlt(pDC->m_hDC, rcImg, RGB(192,192,192));
	//}

	CFont boldFont, font;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	CRect rcText = rcFrame; rcText.top += 2; rcText.left += 5; rcText.bottom = rcText.top + nTextHeight;

	CString strOut; strOut.LoadString(IDS_PROCESS_CUTTING);
	pDC->SetTextColor(GUICOLOR_CAPTION);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	//CPen pen(PS_SOLID, 1, LIGHTGRAY);
	//CPen* pOldPen = pDC->SelectObject(&pen);
	//pDC->MoveTo(rcText.left  - 2,	rcText.bottom + 3);
	//pDC->LineTo(rcText.right - 5,	rcText.bottom + 3);
	//pDC->SelectObject(pOldPen);
	//pen.DeleteObject();

	rcText.left += 12;
	rcText.OffsetRect(0, nTextHeight + 8);

	pDC->SelectObject(&font);

	CString strAlignment, strFirstCut, strBorders;
	strAlignment.LoadString(IDS_ALIGNMENT);
	strFirstCut.LoadString(IDS_FIRSTCUT);
	strBorders.LoadString(IDS_TRIMS);
	int nTab = max(pDC->GetTextExtent(strAlignment).cx, pDC->GetTextExtent(strFirstCut).cx);
	pDC->SetTextColor(DARKGRAY); 
	pDC->DrawText(strAlignment, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	pDC->SetTextColor(BLACK);
	CRect rcText2 = rcText; rcText2.left += nTab + Pixel2MM(pDC, 5); 
	strOut.LoadString((m_nBaseCutDirection == VERTICAL) ? IDS_COLUMNS : IDS_ROWS);
	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcText.OffsetRect(0, nTextHeight);
	rcText2.OffsetRect(0, nTextHeight);
	pDC->SetTextColor(DARKGRAY); 
	pDC->DrawText(strFirstCut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	pDC->SetTextColor(BLACK);
	switch (m_nBaseCutAlignment)
	{
	case LEFT:	 strOut.LoadString(IDS_LEFT);	break;
	case BOTTOM: strOut.LoadString(IDS_BOTTOM);	break;
	case RIGHT:	 strOut.LoadString(IDS_RIGHT);	break;
	case TOP:	 strOut.LoadString(IDS_TOP);	break;
	}
	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcText.OffsetRect(0, nTextHeight + Pixel2MM(pDC, 5));
	rcText2.OffsetRect(0, nTextHeight + Pixel2MM(pDC, 5));
	pDC->SetTextColor(DARKGRAY); 
	pDC->DrawText(strBorders, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	
	rcText.OffsetRect(0, nTextHeight); rcText2 = rcText; rcText2.OffsetRect(40, 0);
	CRect rcTrimsBox(rcText.TopLeft(), CSize(1, 1));

	strOut.LoadString(IDS_LEFT); strOut += _T(":");
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
	pDC->SetTextColor(BLACK);
	strOut.Format(_T("%.1f"), m_fLeftBorder);
	DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
	
	rcText.OffsetRect(0, nTextHeight); 
	rcText2.OffsetRect(0, nTextHeight);
	pDC->SetTextColor(DARKGRAY); 
	strOut.LoadString(IDS_RIGHT); strOut += _T(":");
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
	pDC->SetTextColor(BLACK);
	strOut.Format(_T("%.1f"), m_fRightBorder);
	DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
	
	rcText.OffsetRect (rcTrimsBox.right - rcText.left + 20,  rcTrimsBox.top - rcText.top); rcText2 = rcText; rcText2.OffsetRect(40, 0);
	pDC->SetTextColor(DARKGRAY); 
	strOut.LoadString(IDS_TOP); strOut += _T(":");
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
	pDC->SetTextColor(BLACK);
	strOut.Format(_T("%.1f"), m_fUpperBorder);
	DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
	
	rcText.OffsetRect(0, nTextHeight);
	rcText2.OffsetRect(0, nTextHeight);
	pDC->SetTextColor(DARKGRAY); 
	strOut.LoadString(IDS_BOTTOM); strOut += _T(":");
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);
	pDC->SetTextColor(BLACK);
	strOut.Format(_T("%.1f"), m_fLowerBorder);
	DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcTrimsBox);

	rcBox.UnionRect(rcBox, rcText2);
	rcBox.bottom += 2;

	boldFont.DeleteObject();
	font.DeleteObject();

	return rcBox;
}

/////////////////////////////////////////////////////////////////////////////
// CPressParams
IMPLEMENT_SERIAL(CPressParams, CObject, VERSIONABLE_SCHEMA | 2)

CPressParams::CPressParams()
{
	m_strID					 = _T("");

	m_nWorkStyle			 = WORK_AND_BACK;
	m_nTurnStyle			 = 0;
	m_nRefEdge				 = RIGHT_BOTTOM;
	m_nNumColorStationsFront = 4;
	m_nNumColorStationsBack  = 0;
}

const CPressParams& CPressParams::operator=(const CPressParams& rPressParams)
{
	Copy(rPressParams);	

	return *this;
}

CPressParams::CPressParams(const CPressParams& rPressParams)
{
	Copy(rPressParams);	
}

BOOL CPressParams::operator==(const CPressParams& rPressParams)
{
	if (m_nWorkStyle			 != rPressParams.m_nWorkStyle)						return FALSE;
	if (m_nTurnStyle			 != rPressParams.m_nTurnStyle)						return FALSE;
	if (m_nRefEdge				 != rPressParams.m_nRefEdge)						return FALSE;
	if (m_nNumColorStationsFront != rPressParams.m_nNumColorStationsFront)			return FALSE;
	if (m_nNumColorStationsBack  != rPressParams.m_nNumColorStationsBack)			return FALSE;
	if (m_pressDevice			 != rPressParams.m_pressDevice)						return FALSE;
	if (m_backSidePressDevice	 != rPressParams.m_backSidePressDevice)				return FALSE;
	if (m_sheetData				 != rPressParams.m_sheetData)						return FALSE;
	if (m_strMarkset			 != rPressParams.m_strMarkset)						return FALSE;
	
	if (m_availableSheets.GetSize() != rPressParams.m_availableSheets.GetSize())	return FALSE;
	for (int i = 0; i < m_availableSheets.GetSize(); i++)
		if (m_availableSheets[i] != rPressParams.m_availableSheets[i])				return FALSE;

	return TRUE;
}

BOOL CPressParams::operator!=(const CPressParams& rPressParams)
{
	return (*this == rPressParams) ? FALSE : TRUE;
}

void CPressParams::Copy(const CPressParams& rPressParams)
{
	m_strID					 = rPressParams.m_strID;
	m_nWorkStyle			 = rPressParams.m_nWorkStyle;
	m_nTurnStyle			 = rPressParams.m_nTurnStyle;
	m_nRefEdge				 = rPressParams.m_nRefEdge;
	m_nNumColorStationsFront = rPressParams.m_nNumColorStationsFront;
	m_nNumColorStationsBack  = rPressParams.m_nNumColorStationsBack;
	m_pressDevice			 = rPressParams.m_pressDevice;
	m_backSidePressDevice	 = rPressParams.m_backSidePressDevice;
	m_sheetData				 = rPressParams.m_sheetData;
	m_strMarkset			 = rPressParams.m_strMarkset;
	m_availableSheets.RemoveAll();
	m_availableSheets.Append(rPressParams.m_availableSheets);
}

void CPressParams::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CPressParams));

	if (ar.IsStoring())
	{
		ar << m_nWorkStyle;
		ar << m_nTurnStyle;
		ar << m_nRefEdge;
		ar << m_nNumColorStationsFront;
		ar << m_nNumColorStationsBack;
		ar << m_strMarkset;
	}	
	else
	{ 
		UINT nVersion = 1;
		if (ar.IsLoading())
			nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:	ar >> m_nWorkStyle;
				ar >> m_nTurnStyle;
				ar >> m_nRefEdge;
				ar >> m_nNumColorStationsFront;
				ar >> m_nNumColorStationsBack;
				break;
		case 2:	ar >> m_nWorkStyle;
				ar >> m_nTurnStyle;
				ar >> m_nRefEdge;
				ar >> m_nNumColorStationsFront;
				ar >> m_nNumColorStationsBack;
				ar >> m_strMarkset;
				break;
		}
	}

	SerializeElements(ar, &m_pressDevice, 1);
	SerializeElements(ar, &m_backSidePressDevice, 1);
	SerializeElements(ar, &m_sheetData, 1);
	m_availableSheets.Serialize(ar);
}

BOOL CPressParams::ApplyModifications(CLayout* pLayout, CPressDevice& rOldPressDevice, BOOL& bDoReImpose)
{
	if ( ! pLayout)
		return FALSE;

	BOOL  bChanged	  = FALSE;
	BOOL  bDoPosition = FALSE;
	float fGripper	  = m_pressDevice.m_fGripper;

	if (m_pressDevice != rOldPressDevice)
	{
		fGripper = CDlgPressDevice::CheckGripper(&rOldPressDevice, &m_pressDevice, &pLayout->m_FrontSide);
		bChanged = bDoPosition = bDoReImpose = TRUE;
	}
	if (m_nWorkStyle != pLayout->GetCurrentWorkStyle())
	{
		int nOldWorkStyle = pLayout->GetCurrentWorkStyle();
		pLayout->SetWorkStyle(m_nWorkStyle);
		pLayout->m_bAskForChangeToWorkAndBack = FALSE;
		if ( ( (m_nWorkStyle == WORK_AND_BACK) && (nOldWorkStyle == WORK_SINGLE_SIDE) ) || ( (m_nWorkStyle == WORK_SINGLE_SIDE) && (nOldWorkStyle == WORK_AND_BACK) ) )
			bChanged = TRUE;
		else
			bChanged = bDoReImpose = TRUE;
	}
	int nLayoutTurnStyle = (pLayout->KindOfProduction() == WORK_AND_TURN) ? 0 : 1;
	if (m_nTurnStyle != nLayoutTurnStyle)
	{
		pLayout->SetTurnStyle(m_nTurnStyle);
		bChanged = TRUE;
	}
	if (m_nRefEdge != pLayout->m_FrontSide.m_nPaperRefEdge)
	{
		int	nProductType = pLayout->KindOfProduction();
		pLayout->m_FrontSide.m_nPaperRefEdge = m_nRefEdge;
		switch (pLayout->m_FrontSide.m_nPaperRefEdge)
		{
			case LEFT_BOTTOM  : if (nProductType == WORK_AND_TURN)
									pLayout->m_BackSide.m_nPaperRefEdge = RIGHT_BOTTOM;
								else
									pLayout->m_BackSide.m_nPaperRefEdge = LEFT_TOP;
								break;
			case RIGHT_BOTTOM : if (nProductType == WORK_AND_TURN)
									pLayout->m_BackSide.m_nPaperRefEdge = LEFT_BOTTOM;
								else
									pLayout->m_BackSide.m_nPaperRefEdge = RIGHT_TOP;
								break;
			case LEFT_TOP	  : if (nProductType == WORK_AND_TURN)
									pLayout->m_BackSide.m_nPaperRefEdge = RIGHT_TOP;
								else
									pLayout->m_BackSide.m_nPaperRefEdge = LEFT_BOTTOM;
								break;
			case RIGHT_TOP	  : if (nProductType == WORK_AND_TURN)
									pLayout->m_BackSide.m_nPaperRefEdge = LEFT_TOP;
								else
									pLayout->m_BackSide.m_nPaperRefEdge = RIGHT_BOTTOM;
								break;
		}
		bChanged = TRUE;
	}

	///// front
	if (m_sheetData.m_strSheetName != pLayout->m_FrontSide.m_strFormatName)
	{
		pLayout->m_FrontSide.m_strFormatName = m_sheetData.m_strSheetName;
		bChanged = TRUE;
	}
	if (m_sheetData.m_fWidth != pLayout->m_FrontSide.m_fPaperWidth)
	{
		pLayout->m_FrontSide.m_fPaperWidth		 = m_sheetData.m_fWidth;
		pLayout->m_FrontSide.m_nPaperOrientation = (unsigned char)( (m_sheetData.m_fWidth > m_sheetData.m_fHeight) ? LANDSCAPE : PORTRAIT);
		bChanged = bDoPosition = bDoReImpose = TRUE;
	}
	if (m_sheetData.m_fHeight != pLayout->m_FrontSide.m_fPaperHeight)
	{
		pLayout->m_FrontSide.m_fPaperHeight		 = m_sheetData.m_fHeight;
		pLayout->m_FrontSide.m_nPaperOrientation = (unsigned char)( (m_sheetData.m_fWidth > m_sheetData.m_fHeight) ? LANDSCAPE : PORTRAIT);
		bChanged = bDoPosition = bDoReImpose = TRUE;
	}
	if (m_sheetData.m_nPaperGrain != pLayout->m_FrontSide.m_nPaperGrain)
	{
		pLayout->m_FrontSide.m_nPaperGrain = m_sheetData.m_nPaperGrain;
		bChanged = bDoReImpose = TRUE;
	}

	///// back
	if (m_sheetData.m_strSheetName != pLayout->m_BackSide.m_strFormatName)
	{
		pLayout->m_BackSide.m_strFormatName = m_sheetData.m_strSheetName;
		bChanged = TRUE;
	}
	if (m_sheetData.m_fWidth != pLayout->m_BackSide.m_fPaperWidth)
	{
		pLayout->m_BackSide.m_fPaperWidth		 = m_sheetData.m_fWidth;
		pLayout->m_BackSide.m_nPaperOrientation = (unsigned char)( (m_sheetData.m_fWidth > m_sheetData.m_fHeight) ? LANDSCAPE : PORTRAIT);
		bChanged = bDoPosition = bDoReImpose = TRUE;
	}
	if (m_sheetData.m_fHeight != pLayout->m_BackSide.m_fPaperHeight)
	{
		pLayout->m_BackSide.m_fPaperHeight		 = m_sheetData.m_fHeight;
		pLayout->m_BackSide.m_nPaperOrientation = (unsigned char)( (m_sheetData.m_fWidth > m_sheetData.m_fHeight) ? LANDSCAPE : PORTRAIT);
		bChanged = bDoPosition = bDoReImpose = TRUE;
	}
	if (m_sheetData.m_nPaperGrain != pLayout->m_BackSide.m_nPaperGrain)
	{
		pLayout->m_BackSide.m_nPaperGrain = m_sheetData.m_nPaperGrain;
		bChanged = bDoReImpose = TRUE;
	}

	if (bDoPosition)
	{
		pLayout->m_FrontSide.PositionLayoutSide(fGripper, &m_pressDevice);
		pLayout->m_BackSide.PositionLayoutSide (fGripper, &m_pressDevice);
	}

	if (bDoPosition || bChanged)
		pLayout->AnalyzeMarks();

	return bChanged;
}

CRect CPressParams::DrawData(CDC* pDC, CRect rcFrame, CLayout* pLayout, CDisplayList* pDisplayList, BOOL bSmallVersion)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CPrintSheet*	pPrintSheet	   = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
	CPressDevice*	pPressDevice   = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	BOOL			bIsDefault	   = (pPressDevice) ? FALSE : TRUE;
	int     		nProductType   = (pLayout) ? pLayout->m_nProductType : WORK_AND_BACK;;
	if ( ! pPressDevice)
		pPressDevice = &m_pressDevice;
	CSheet sheet;
	if (pLayout)
	{
		sheet.m_fWidth  = pLayout->m_FrontSide.m_fPaperWidth;
		sheet.m_fHeight = pLayout->m_FrontSide.m_fPaperHeight;
	}
	else
		sheet = m_sheetData;

	int		nPressType				= (pPressDevice) ? pPressDevice->m_nPressType : CPressDevice::SheetPress;
	float	fPlateWidth				= (pPressDevice) ? pPressDevice->m_fPlateWidth  : 0.0f;
	float	fPlateHeight			= (pPressDevice) ? pPressDevice->m_fPlateHeight : 0.0f;

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);

	CString strOut; 
	rcFrame.top  += 5;
	rcFrame.left += 5;
	CRect rcTitle = rcFrame; rcTitle.top += 2; rcTitle.bottom = rcTitle.top + nTextHeight;

	CImage img; img.LoadFromResource(theApp.m_hInstance, IDB_PRINTING_MACHINE);		
	if ( ! img.IsNull())
	{
		CRect rcImg; rcImg.left = rcFrame.left - 2; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcFrame.top + 5; rcImg.bottom = rcImg.top + img.GetHeight();
		img.TransparentBlt(pDC->m_hDC, rcImg, RGB(192,192,192));
		rcTitle.left = rcFrame.left + 32;
		rcBox.bottom = rcImg.bottom;
	}

	CFont boldFont, font;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	if ( ! bSmallVersion)
	{
		strOut.LoadString(IDS_PROCESS_PRINTING);
		pDC->SetTextColor(BLACK);
		DrawTextMeasured(pDC, strOut, rcTitle, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		rcTitle.OffsetRect(0, nTextHeight + 3);
	}

	pDC->SetTextColor(GUICOLOR_CAPTION);

	int	nRight = rcTitle.left;
	int	nLeft  = nRight;

	CString strWidth, strHeight, strWorkStyle, strTurnStyle, strPlateFormat, strSheetFormat, strColorStations = "Farbwerke:", strPaperGrain;
	strWorkStyle.LoadString(IDS_TYPEOFPRINTING);	strWorkStyle   += _T(":");
	strTurnStyle.LoadString(IDS_TYPEOFTURNING);		strTurnStyle   += _T(":");
	strPlateFormat.LoadString((bSmallVersion) ? IDS_PAPERLIST_FORMAT : IDS_PLATE_LABEL);	strPlateFormat += _T(":");
	strSheetFormat.LoadString((nPressType == CPressDevice::SheetPress) ? IDS_PAPER_LABEL : IDS_SECTION_LABEL); strSheetFormat += _T(":");
	strPaperGrain.LoadString(IDS_PAPER_GRAIN); strPaperGrain += _T(":");
	int	nTab = max(max(max(pDC->GetTextExtent(strPlateFormat).cx, pDC->GetTextExtent(strSheetFormat).cx), pDC->GetTextExtent(strColorStations).cx), pDC->GetTextExtent(strPaperGrain).cx);
	//nTab = 67;

	CRect rcText2 = rcTitle; rcText2.left += nTab; 

	strOut = (pPressDevice) ? pPressDevice->m_strName : _T("");
	if ( ! strOut.IsEmpty())
	{
		pDC->SetTextColor(GUICOLOR_CAPTION); 
		DrawTextMeasured(pDC, strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		nRight = rcTitle.left + pDC->GetTextExtent(strOut).cx;
		rcTitle.OffsetRect(0, nTextHeight);
		rcText2.OffsetRect(0, nTextHeight);
	}
	else
	{
		rcBox.bottom += 5;
		rcBox.right  += 5;
		return rcBox;
	}

	nLeft = rcTitle.left;

	pDC->SelectObject(&font);

	pDC->SetTextColor(DARKGRAY); 
	DrawTextMeasured(pDC, strWorkStyle, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	pDC->SetTextColor(BLACK); 
	switch (nProductType)
	{
	case WORK_AND_BACK:		strOut.LoadString(IDS_WORK_AND_BACK);			break;
	case WORK_SINGLE_SIDE:	strOut.LoadString(IDS_WORK_SINGLE_SIDE);		break;
	case WORK_AND_TURN:		strOut = theApp.settings.m_szWorkAndTurnName;   break;
	case WORK_AND_TUMBLE:	strOut = theApp.settings.m_szWorkAndTumbleName; break;
	default:				strOut = _T("");								break;
	}

	m_nWorkStyle = nProductType;	// hold CPressParams compatible to layout
	m_nTurnStyle = (pLayout->KindOfProduction() == WORK_AND_TURN) ? 0 : 1;

	DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	if (pLayout)
	{
		if (nProductType == WORK_AND_BACK)
		{
			rcTitle.OffsetRect(0, nTextHeight);
			rcText2.OffsetRect(0, nTextHeight);
			pDC->SetTextColor(DARKGRAY); 
			DrawTextMeasured(pDC, strTurnStyle, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
			pDC->SetTextColor(BLACK); 
			strOut.LoadString( (pLayout->KindOfProduction() == WORK_AND_TURN) ? IDS_TURN : IDS_TUMBLE);
			DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		}
	}

	///// plate / paper formats
	rcTitle.OffsetRect(0, nTextHeight); 
	rcText2.OffsetRect(0, nTextHeight); 

	pDC->SetTextColor(DARKGRAY); 
	DrawTextMeasured(pDC, strPlateFormat, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	pDC->SetTextColor(BLACK); 
	CPrintSheetView::TruncateNumber(strWidth, fPlateWidth); CPrintSheetView::TruncateNumber(strHeight, fPlateHeight, (bSmallVersion) ? FALSE : TRUE);
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	if ( ! bSmallVersion)
	{
		rcTitle.OffsetRect(0, nTextHeight); 
		rcText2.OffsetRect(0, nTextHeight); 

		pDC->SetTextColor(DARKGRAY); 
		DrawTextMeasured(pDC, strSheetFormat, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

		pDC->SetTextColor(BLACK); 
		CPrintSheetView::TruncateNumber(strWidth, sheet.m_fWidth); CPrintSheetView::TruncateNumber(strHeight, sheet.m_fHeight, TRUE);
		strOut.Format(_T("%s x %s"), strWidth, strHeight);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

		rcTitle.OffsetRect(0, nTextHeight); 
		rcText2.OffsetRect(0, nTextHeight); 
		
		pDC->SetTextColor(DARKGRAY); 
		DrawTextMeasured(pDC, strPaperGrain, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

		CImage img;
		switch (m_sheetData.m_nPaperGrain)
		{
		case GRAIN_HORIZONTAL:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAIN_HORIZONTAL));	break;
		case GRAIN_VERTICAL:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAIN_VERTICAL));	break;
		}
		if ( ! img.IsNull())
		{
			CRect rcImg(CPoint(rcText2.left - 3, rcText2.top + 2), CSize(img.GetWidth(), img.GetHeight()));
			img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
			rcBox.UnionRect(rcBox, rcImg);
		}
	}

	rcBox.bottom += 5;
	rcBox.right  += 5;

	boldFont.DeleteObject();
	font.DeleteObject();

	return rcBox;
}

/////////////////////////////////////////////////////////////////////////////
// CPrepressParams
IMPLEMENT_SERIAL(CPrepressParams, CObject, VERSIONABLE_SCHEMA | 3)

CPrepressParams::CPrepressParams()
{
	m_strID						 = _T("");

	m_fBleed					 = theApp.settings.m_fDefaultBleed;
	m_bAdjustContentToMask		 = FALSE;
	m_strPrintSheetOutputProfile = _T("");
	m_strFoldSheetOutputProfile  = _T("");
	m_strBookletOutputProfile	 = _T("");
	m_fMaxOverProduction		 = 0.0f;
}

CPrepressParams::CPrepressParams(const CPrepressParams& rPrepressParams)
{
	Copy(rPrepressParams);	
}

const CPrepressParams& CPrepressParams::operator=(const CPrepressParams& rPrepressParams)
{
	Copy(rPrepressParams);	

	return *this;
}

BOOL CPrepressParams::operator==(const CPrepressParams& rPrepressParams)
{
	if (m_fBleed					 != rPrepressParams.m_fBleed)						return FALSE;
	if (m_bAdjustContentToMask		 != rPrepressParams.m_bAdjustContentToMask)			return FALSE;
	if (m_strPrintSheetOutputProfile != rPrepressParams.m_strPrintSheetOutputProfile)	return FALSE;
	if (m_strFoldSheetOutputProfile  != rPrepressParams.m_strFoldSheetOutputProfile)	return FALSE;
	if (m_strBookletOutputProfile    != rPrepressParams.m_strBookletOutputProfile)		return FALSE;
	if (m_fMaxOverProduction		 != rPrepressParams.m_fMaxOverProduction)			return FALSE;

	return TRUE;
}

BOOL CPrepressParams::operator!=(const CPrepressParams& rPrepressParams)
{
	return (*this == rPrepressParams) ? FALSE : TRUE;
}

void CPrepressParams::Copy(const CPrepressParams& rPrepressParams)
{
	m_strID						 = rPrepressParams.m_strID;
	m_fBleed					 = rPrepressParams.m_fBleed;
	m_bAdjustContentToMask		 = rPrepressParams.m_bAdjustContentToMask;
	m_strPrintSheetOutputProfile = rPrepressParams.m_strPrintSheetOutputProfile;
	m_strFoldSheetOutputProfile	 = rPrepressParams.m_strFoldSheetOutputProfile;
	m_strBookletOutputProfile	 = rPrepressParams.m_strBookletOutputProfile;
	m_fMaxOverProduction		 = rPrepressParams.m_fMaxOverProduction;
}

BOOL CPrepressParams::ApplyModifications(CLayout* pLayout, CPrepressParams& rOldPrepressParams, BOOL& bDoReImpose)
{
	if ( ! pLayout)
		return FALSE;

	BOOL bChanged = FALSE;

	if (m_fBleed != rOldPrepressParams.m_fBleed)
	{
		CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
		while (pPrintSheet)
		{
			pPrintSheet->m_FrontSidePlates.SetGlobalMaskAllPlates(m_fBleed);
			pPrintSheet->m_BackSidePlates.SetGlobalMaskAllPlates(m_fBleed);
			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}
		bChanged = bDoReImpose = TRUE;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (m_bAdjustContentToMask	!= rOldPrepressParams.m_bAdjustContentToMask)
	{
		if (pDoc)
			pDoc->m_PageTemplateList.ReorganizeGluelineScaling();
		bChanged = TRUE;
	}
	
	if (m_strPrintSheetOutputProfile != rOldPrepressParams.m_strPrintSheetOutputProfile)
	{
		CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
		while (pPrintSheet)
		{
			pPrintSheet->m_strPDFTarget = m_strPrintSheetOutputProfile;
			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}
		bChanged = TRUE;
	}
	
	if (m_strFoldSheetOutputProfile != rOldPrepressParams.m_strFoldSheetOutputProfile)
	{
		if (pDoc)
		{
			CPDFTargetList pdfTargetList;
			pdfTargetList.Load(CPDFTargetList::FoldSheetTarget);
			CPDFTargetProperties* pOutputTarget = pdfTargetList.Find(m_strFoldSheetOutputProfile);
			if (pOutputTarget)
				pDoc->m_Bookblock.m_FoldSheetList.m_outputTargetProps = *pOutputTarget;
		}
		bChanged = TRUE;
	}
	
	if (m_strBookletOutputProfile != rOldPrepressParams.m_strBookletOutputProfile)
	{
		if (pDoc)
		{
			CPDFTargetList pdfTargetList;
			pdfTargetList.Load(CPDFTargetList::BookletTarget);
			CPDFTargetProperties* pOutputTarget = pdfTargetList.Find(m_strBookletOutputProfile);
			if (pOutputTarget)
				pDoc->m_Bookblock.m_outputTargetProps = *pOutputTarget;
		}
		bChanged = TRUE;
	}
	
	if (m_fMaxOverProduction != rOldPrepressParams.m_fMaxOverProduction)
		bChanged = bDoReImpose = TRUE;

	return bChanged;
}

void CPrepressParams::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrepressParams));

	if (ar.IsStoring())
	{
		ar << m_fBleed;
		ar << m_bAdjustContentToMask;
		ar << m_strPrintSheetOutputProfile;
		ar << m_strFoldSheetOutputProfile;
		ar << m_strBookletOutputProfile;
		ar << m_fMaxOverProduction;
	}	
	else
	{ 
		UINT nVersion = 1;
		if (ar.IsLoading())
			nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:	ar >> m_fBleed;
				ar >> m_bAdjustContentToMask;
				ar >> m_strPrintSheetOutputProfile;
				break;
		case 2:	ar >> m_fBleed;
				ar >> m_bAdjustContentToMask;
				ar >> m_strPrintSheetOutputProfile;
				ar >> m_fMaxOverProduction;
				break;
		case 3:	ar >> m_fBleed;
				ar >> m_bAdjustContentToMask;
				ar >> m_strPrintSheetOutputProfile;
				ar >> m_strFoldSheetOutputProfile;
				ar >> m_strBookletOutputProfile;
				ar >> m_fMaxOverProduction;
				break;
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CPostpressParams
IMPLEMENT_SERIAL(CPostpressParams, CObject, VERSIONABLE_SCHEMA | 1)

CPostpressParams::CPostpressParams()
{
}

const CPostpressParams& CPostpressParams::operator=(const CPostpressParams& rPostpressParams)
{
	Copy(rPostpressParams);	

	return *this;
}

BOOL CPostpressParams::operator==(const CPostpressParams& rPostpressParams)
{
	if (m_trimming != rPostpressParams.m_trimming)	return FALSE;
	if (m_binding  != rPostpressParams.m_binding)	return FALSE;
	if (m_folding  != rPostpressParams.m_folding)	return FALSE;
	if (m_cutting  != rPostpressParams.m_cutting)	return FALSE;
	if (m_punching != rPostpressParams.m_punching)	return FALSE;

	return TRUE;
}

BOOL CPostpressParams::operator!=(const CPostpressParams& rPostpressParams)
{
	return (*this == rPostpressParams) ? FALSE : TRUE;	
}

void CPostpressParams::Copy(const CPostpressParams& rPostpressParams)
{
	m_trimming = rPostpressParams.m_trimming;
	m_binding  = rPostpressParams.m_binding;
	m_folding  = rPostpressParams.m_folding;
	m_cutting  = rPostpressParams.m_cutting;
	m_punching = rPostpressParams.m_punching;
}

void CPostpressParams::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CPostpressParams));

	m_trimming.Serialize(ar);
	m_binding.Serialize(ar);
	m_folding.Serialize(ar);
	m_cutting.Serialize(ar);
	m_punching.Serialize(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CProductionSetupDeprecated

IMPLEMENT_SERIAL(CProductionSetupDeprecated, CObject, VERSIONABLE_SCHEMA | 1)

CProductionSetupDeprecated::CProductionSetupDeprecated()
{
	m_nType		= 0;
	m_nSubType	= CBoundProductPart::Body;
	m_strName.Empty();

	m_strDefaultPressDevice.Empty();	
	m_nDefaultWorkStyle	= WORK_AND_BACK; 
	m_nDefaultTurnTumbleMethod = TURN_LEFTBOTTOMSIDE;
	m_strDefaultPaper.Empty();
	m_strDefaultOutputProfile.Empty();	
	m_strDefaultMarkset.Empty();

	m_bIsDefault = FALSE;
}

CProductionSetupDeprecated::CProductionSetupDeprecated(const CProductionSetupDeprecated& rProductionSetup)
{
	Copy(rProductionSetup);
}

const CProductionSetupDeprecated& CProductionSetupDeprecated::operator=(const CProductionSetupDeprecated& rProductionSetup)
{
	Copy(rProductionSetup);

	return *this;
}

void CProductionSetupDeprecated::Copy(const CProductionSetupDeprecated& rProductionSetup)
{
	m_nType				= rProductionSetup.m_nType;
	m_nSubType			= rProductionSetup.m_nSubType;
	m_strName			= rProductionSetup.m_strName;

	m_prodDataBound		= rProductionSetup.m_prodDataBound;
	m_prodDataUnbound	= rProductionSetup.m_prodDataUnbound;

	m_strDefaultPressDevice		= rProductionSetup.m_strDefaultPressDevice;	
	m_nDefaultWorkStyle			= rProductionSetup.m_nDefaultWorkStyle;
	m_nDefaultTurnTumbleMethod	= rProductionSetup.m_nDefaultTurnTumbleMethod;
	m_strDefaultOutputProfile	= rProductionSetup.m_strDefaultOutputProfile;	
	m_strDefaultPaper			= rProductionSetup.m_strDefaultPaper;
	m_strDefaultMarkset			= rProductionSetup.m_strDefaultMarkset;

	m_bIsDefault		= rProductionSetup.m_bIsDefault;
}

void CProductionSetupDeprecated::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductionSetupDeprecated));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		ar << m_nType;
		ar << m_nSubType;
		ar << m_strName;
		ar << m_strDefaultPressDevice;	
		ar << m_nDefaultWorkStyle;
		ar << m_nDefaultTurnTumbleMethod;
		ar << m_strDefaultPaper;			
		ar << m_strDefaultMarkset;		
		ar << m_strDefaultOutputProfile;
		ar << m_bIsDefault;
	}	
	else
	{ 
		ar >> m_nType;
		ar >> m_nSubType;
		ar >> m_strName;
		ar >> m_strDefaultPressDevice;	
		ar >> m_nDefaultWorkStyle;
		ar >> m_nDefaultTurnTumbleMethod;
		ar >> m_strDefaultPaper;			
		ar >> m_strDefaultMarkset;		
		ar >> m_strDefaultOutputProfile;	
		ar >> m_bIsDefault;
	}

	switch (m_nType)
	{
	case 0:		m_prodDataBound.Serialize(ar);		break;
	case 1:		m_prodDataUnbound.Serialize(ar);	break;
	default:										break;
	}
}

void CProductionSetupDeprecated::Load(const CString& strSetupName )
{
	CFileException fileException;
	CString		   strFullPath;
	
	strFullPath.Format(_T("%s\\ProductionSetups\\%s.set"), theApp.GetDataFolder(), strSetupName); 
	
	CFile file;
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}
	
	CArchive ar(&file, CArchive::load);
	Serialize(ar);
	ar.Close();
	file.Close();
}

void CProductionSetupDeprecated::Save(const CString& strSetupName)
{
	CFile		   file;
	CFileException fileException;
	CString		   strFullPath;
	
	strFullPath.Format(_T("%s\\ProductionSetups"), theApp.GetDataFolder()); 
	if ( ! theApp.DirectoryExist(strFullPath))
		CreateDirectory(strFullPath, NULL);
	strFullPath.Format(_T("%s\\ProductionSetups\\%s.set"), theApp.GetDataFolder(), strSetupName); 

	if (!file.Open((LPCTSTR)strFullPath, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}

	CArchive ar(&file, CArchive::store);
	Serialize(ar);
	ar.Close();
	file.Close();
}

void CProductionSetupDeprecated::Remove(const CString& strSetupName)
{
	CString strFullPath;
	strFullPath.Format(_T("%s\\ProductionSetups\\%s.set"), theApp.GetDataFolder(), strSetupName); 

	DeleteFile(strFullPath);
}
/////////////////////////////////////////////////////////////////////////////

