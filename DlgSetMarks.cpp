// DlgSetMarks.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "MarkSourceWindow.h"
#include "MarkPreViewWindow.h"
#include "DlgSetMarks.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "MainFrm.h"
#include "DlgMarkTemplateProps.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgSetMarks dialog


CDlgSetMarks::CDlgSetMarks(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSetMarks::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSetMarks)
	m_strTextmark = _T("");
	m_fMarkWidth = 0.0f;
	m_fMarkHeight = 0.0f;
	m_strMarkSetName = _T("");
	//}}AFX_DATA_INIT
}


void CDlgSetMarks::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSetMarks)
	DDX_Control(pDX, IDC_PREVIEWSCROLLBAR_HEIGHT, m_ScrollBarHeight);
	DDX_Control(pDX, IDC_PREVIEWSCROLLBAR_WIDTH, m_ScrollBarWidth);
	DDX_Control(pDX, IDC_MARKPREVIEW_WINDOW, m_MarkPreViewWindow);
	DDX_Control(pDX, IDC_MARKSOURCE_WINDOW, m_MarkSourceWindow);
	DDX_Text(pDX, IDC_TEXTMARKSTRING, m_strTextmark);
	DDX_Measure(pDX, IDC_MARK_WIDTH, m_fMarkWidth);
	DDX_Measure(pDX, IDC_MARK_HEIGHT, m_fMarkHeight);
	DDX_CBString(pDX, IDC_MARK_SET_LIST, m_strMarkSetName);
	DDV_MaxChars(pDX, m_strMarkSetName, 256);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgSetMarks, CDialog)
	//{{AFX_MSG_MAP(CDlgSetMarks)
	ON_BN_CLICKED(IDC_PROPS_BUTTON, OnPropsButton)
	ON_WM_LBUTTONDOWN()
	ON_LBN_SELCHANGE(IDC_MARK_LIST, OnSelchangeMark)
	ON_CBN_SELCHANGE(IDC_MARK_SET_LIST, OnSelchangeMarkSet)
	ON_WM_SETCURSOR()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgSetMarks message handlers

BOOL CDlgSetMarks::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_MarkPreViewWindow.m_pScrollBarHeight = &m_ScrollBarHeight;
	m_MarkPreViewWindow.m_pScrollBarWidth  = &m_ScrollBarWidth;
//	m_MarkPreViewWindow.m_pMarkList		   = &m_markList;

	RegisterMarkFiles();
	LoadMarkSet();

	CMarkOld markBuffer;

	CListBox *pBox   = (CListBox*)GetDlgItem (IDC_MARK_LIST);
	POSITION pos = m_markList.GetHeadPosition();
	while (pos)
	{
		markBuffer = m_markList.GetNext(pos);
		pBox->AddString(markBuffer.m_strFormatName);
	}	

	m_ScrollBarHeight.ShowScrollBar(FALSE);
	m_ScrollBarWidth.ShowScrollBar(FALSE);

	pBox->SetCurSel(0);
	pos	= m_markList.GetHeadPosition();
	UpdateMembers(pos);
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgSetMarks::OnSelchangeMarkSet() 
{
	UpdateData(TRUE);

	CComboBox *pComboBox = (CComboBox*)GetDlgItem (IDC_MARK_SET_LIST);
	UINT nIndex = pComboBox->GetCurSel();
	if (nIndex == CB_ERR)
		return;

	pComboBox->GetLBText(nIndex, m_strMarkSetName);
	LoadMarkSet(); // MarkSet don't have to be empty

	CMarkOld markBuffer;

	CListBox* pListBox = (CListBox*)GetDlgItem (IDC_MARK_LIST);
	pListBox->ResetContent();
	POSITION pos	 = m_markList.GetHeadPosition();
	while (pos)
	{
		markBuffer = m_markList.GetNext(pos);
		pListBox->AddString(markBuffer.m_strFormatName);
	}	

	pListBox->SetCurSel(0);
	pos = m_markList.GetHeadPosition();
	UpdateMembers(pos);
	UpdateData(FALSE);      
}

void CDlgSetMarks::OnSelchangeMark() 
{
	CListBox *pBox = (CListBox*)GetDlgItem (IDC_MARK_LIST);
	UINT nIndex  = pBox->GetCurSel();
	if (nIndex == CB_ERR)
		return;

	POSITION pos = m_markList.FindIndex(nIndex);
	UpdateMembers(pos);
	UpdateData(FALSE);
}

void CDlgSetMarks::OnPropsButton() 
{
	CListBox *pBox = (CListBox*)GetDlgItem (IDC_MARK_LIST);
	UINT nIndex  = pBox->GetCurSel();
	if (nIndex == CB_ERR)
		return;

	POSITION pos = m_markList.FindIndex(nIndex);
	CMarkOld markBuffer;
	markBuffer = m_markList.GetAt(pos);
//	m_strMarkName	 = markBuffer.m_strFormatName;

	CPageTemplate markTemplate;
	markTemplate.Create(markBuffer.m_strFormatName, CPageTemplate::CustomMark, 0.0f, 0.0f);

	markTemplate.m_ColorSeparations.RemoveAll();
	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
		markTemplate.m_ColorSeparations.Add(m_ColorSeparations[i]);

	CLayoutObject layoutObject;
	CString string = "";
	layoutObject.CreateMark(0.0f, 0.0f, m_fMarkWidth, m_fMarkHeight, string, TOP, -1);//, m_strTextmark);

	CDlgMarkTemplateProps dlg;
	dlg.m_pMarkTemplate = &markTemplate;
	dlg.m_pLayoutObject = &layoutObject;
	dlg.m_pMarkSource	= &m_MarkSource;

	dlg.DoModal();
}

void CDlgSetMarks::OnOK() 
{
	CMarkOld markBuffer;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	int	nSide		 = pDoc->GetActSight();

	POSITION pos = m_markList.GetHeadPosition();
	while (pos)                        
	{
		markBuffer = m_markList.GetNext(pos);

		switch (nSide)
		{
		case BOTHSIDES: PositionAndCreateMarkAuto(&markBuffer, FRONTSIDE);
						PositionAndCreateMarkAuto(&markBuffer, BACKSIDE);
						break;
		case FRONTSIDE: PositionAndCreateMarkAuto(&markBuffer, FRONTSIDE);
						break;
		case BACKSIDE : PositionAndCreateMarkAuto(&markBuffer, BACKSIDE);
						break;
		default		  : return;
		}
	}	

	pDoc->SetModifiedFlag();

	// Don't update PrintSheetTreeView there would be changed the current tree-selection
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);

	CDialog::OnOK();
}

void CDlgSetMarks::PositionAndCreateMarkAuto(CMarkOld *markBuffer, int nSide)
{
	float fLeft, fBottom, fPlateLeft, fPlateBottom, fPlateRight, fPlateTop;
	float BoundingRectLeft, BoundingRectBottom, BoundingRectRight, BoundingRectTop, 
		  BoundingRectWidth, BoundingRectHeight;
	CPressDevice* pPressDevice;
	CLayoutSide* pLayoutSide;
	CLayoutObject NewMarkBuffer;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	CLayout* pActLayout = pDoc->GetActLayout();

	switch (nSide)
	{
	case FRONTSIDE: pPressDevice = pActLayout->m_FrontSide.GetPressDevice();
					pLayoutSide  = &(pActLayout->m_FrontSide);
					pActLayout->m_FrontSide.CalcBoundingRectObjects(&BoundingRectLeft, &BoundingRectBottom, &BoundingRectRight, &BoundingRectTop, &BoundingRectWidth, &BoundingRectHeight, FALSE);
					break;
	case BACKSIDE : pPressDevice = pActLayout->m_BackSide.GetPressDevice();
					pLayoutSide  = &(pActLayout->m_BackSide);
					pActLayout->m_BackSide.CalcBoundingRectObjects(&BoundingRectLeft, &BoundingRectBottom, &BoundingRectRight, &BoundingRectTop, &BoundingRectWidth, &BoundingRectHeight, FALSE);
					break;
	default		  : return;
	}

	fPlateLeft	 = 0.0f;
	fPlateBottom = 0.0f;
	fPlateRight	 = fPlateLeft   + pPressDevice->m_fPlateWidth;
	fPlateTop	 = fPlateBottom + pPressDevice->m_fPlateHeight;

	switch (markBuffer->m_nReferenceLineInX)
	{
		case LEFT : if (markBuffer->m_nReferenceItemInX == PAPER_REF)
						fLeft = pLayoutSide->m_fPaperLeft + markBuffer->m_fDistanceX - markBuffer->m_fMarkWidth/2.0f;
					else
						if (markBuffer->m_nReferenceItemInX == PLATE_REF)
							fLeft = fPlateLeft + markBuffer->m_fDistanceX - markBuffer->m_fMarkWidth/2.0f;
						else
							fLeft = BoundingRectLeft + markBuffer->m_fDistanceX - markBuffer->m_fMarkWidth/2.0f;
						break;
		case RIGHT: if (markBuffer->m_nReferenceItemInX == PAPER_REF)
						fLeft = pLayoutSide->m_fPaperRight - markBuffer->m_fDistanceX - markBuffer->m_fMarkWidth/2.0f;
					else
						if (markBuffer->m_nReferenceItemInX == PLATE_REF)
							fLeft = fPlateRight - markBuffer->m_fDistanceX - markBuffer->m_fMarkWidth/2.0f;
						else
							fLeft = BoundingRectRight - markBuffer->m_fDistanceX - markBuffer->m_fMarkWidth/2.0f;
 						break;
		default	  : fLeft = 0.0f; 
					break;
	}
	switch (markBuffer->m_nReferenceLineInY)
	{
		case TOP  : if (markBuffer->m_nReferenceItemInY == PAPER_REF)
						fBottom = pLayoutSide->m_fPaperTop - markBuffer->m_fDistanceY - markBuffer->m_fMarkHeight/2.0f;
					else
						if (markBuffer->m_nReferenceItemInY == PLATE_REF)
							fBottom = fPlateTop - markBuffer->m_fDistanceY - markBuffer->m_fMarkHeight/2.0f;
						else
							fBottom = BoundingRectTop - markBuffer->m_fDistanceY - markBuffer->m_fMarkHeight/2.0f;
						break;
		case BOTTOM: if (markBuffer->m_nReferenceItemInY == PAPER_REF)
						fBottom = pLayoutSide->m_fPaperBottom + markBuffer->m_fDistanceY - markBuffer->m_fMarkHeight/2.0f;
					else
						if (markBuffer->m_nReferenceItemInY == PLATE_REF)
							fBottom = fPlateBottom + markBuffer->m_fDistanceY - markBuffer->m_fMarkHeight/2.0f;
						else
							fBottom = BoundingRectBottom + markBuffer->m_fDistanceY - markBuffer->m_fMarkHeight/2.0f;
						break;
		default	  : fBottom = 0.0f; 
					break;
	}

// Mark source
	short nObjectSourceIndex = pDoc->m_MarkSourceList.FindObjectSourceIndex(markBuffer->m_MarkSource.m_strContentFilesPath);
	if (nObjectSourceIndex == -1)
	{
		pDoc->m_MarkSourceList.AddTail(markBuffer->m_MarkSource);
		nObjectSourceIndex = (short)(pDoc->m_MarkSourceList.GetCount() - 1);
	}

// Template
	CPageTemplate* pMarkTemplate = pDoc->m_MarkTemplateList.FindTemplate(markBuffer->m_strFormatName);
	if (!pMarkTemplate)
	{
		CPageTemplate markTemplate;
		markTemplate.Create(markBuffer->m_strFormatName, CPageTemplate::CustomMark, 0.0f, 0.0f);

		markTemplate.m_ColorSeparations.RemoveAll();
//		for (short i = 0; i < markBuffer->m_ColorSeparations.GetSize(); i++)
		for (short i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
		{
//			SEPARATIONINFO sepInfo = markBuffer->m_ColorSeparations[i];
			SEPARATIONINFO sepInfo = {nObjectSourceIndex, 1, i};
			markTemplate.m_ColorSeparations.Add(sepInfo);
		}

		pDoc->m_MarkTemplateList.AddTail(markTemplate);
		pMarkTemplate = pDoc->m_MarkTemplateList.FindTemplate(markBuffer->m_strFormatName);
	}
/*
	POSITION objPos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while(objPos)
	{
		CLayoutObject& rLayoutObject = pLayoutSide->m_ObjectList.GetNext(objPos);
		if (rLayoutObject.m_nType == CLayoutObject::Page)
	}
*/
// Create a new Object
	NewMarkBuffer.CreateMark(fLeft, fBottom, markBuffer->m_fMarkWidth, markBuffer->m_fMarkHeight, 
							 markBuffer->m_strFormatName, TOP, pDoc->m_MarkTemplateList.GetIndex(pMarkTemplate));
							 //markBuffer->m_strTextmark);
	pLayoutSide->m_ObjectList.AddTail(NewMarkBuffer);
	short nLayoutObjectIndex = (short)(pLayoutSide->m_ObjectList.GetCount() - 1);

// Add the exposure to the plates
	short		nIndex		 = 0;
	CPrintSheet *pPrintSheet = pActLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		CPlateList* pPlateList = (nSide == FRONTSIDE) ? &pPrintSheet->m_FrontSidePlates : &pPrintSheet->m_BackSidePlates;
		POSITION platePos = pPlateList->GetHeadPosition();
		CPlate *pPlate;
		while (platePos)
		{
			pPlate = &(pPlateList->GetNext(platePos));
			CExposure newExposure;
			newExposure.Create(*pLayoutSide, nLayoutObjectIndex, pPlate->m_nColorDefinitionIndex);

			if (markBuffer->m_bAllColors)
				pPlate->m_ExposureSequence.AddTail(newExposure);
			else
			{
				//if (PlateIndexIsInMarkIndex(pPlate->m_nColorDefinitionIndex, markBuffer->m_strColorIndex))
				pPlate->m_ExposureSequence.AddTail(newExposure);
			}

			CPrintSheetLocation loc;
			loc.m_nPrintSheetIndex	 = nIndex;
			loc.m_nLayoutSide		 = (BYTE)nSide;
			loc.m_nLayoutObjectIndex = nLayoutObjectIndex;
			pMarkTemplate->m_PrintSheetLocations.Add(loc);
		}
		pPrintSheet = pActLayout->GetNextPrintSheet(pPrintSheet);
		nIndex++;
	}
}

BOOL CDlgSetMarks::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	int bRet;

	if (m_MarkPreViewWindow.m_bOnSetCursor)
	{
		SetCursor(theApp.m_CursorMagnifyIncrease);
		bRet = TRUE;
	}
	else
		bRet = CDialog::OnSetCursor(pWnd, nHitTest, message);

	m_MarkPreViewWindow.m_bOnSetCursor = FALSE;
	return bRet;
}

void CDlgSetMarks::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// Deselect selections in m_MarkSourceWindow
	m_MarkSourceWindow.OnLButtonDown(nFlags, point); 
	
	CDialog::OnLButtonDown(nFlags, point);
}
void CDlgSetMarks::PostNcDestroy() 
{
	m_markList.RemoveAll();  // List-contents must be deleted
	CDialog::PostNcDestroy();
}

/////////////////////////////////////////////////////////////////////////////
// CDlgSetMarks file handling

BOOL CDlgSetMarks::RegisterMarkFiles(CString strFileName)
{
	WIN32_FIND_DATA findData;
	TCHAR searchbuffer[MAX_PATH];
	CComboBox *pBox;
	BOOL bFilesExist = FALSE;	
	CString strBuffer;

	strcpy (searchbuffer, theApp.settings.m_szDataDir);
	strcat (searchbuffer, "\\*.drz");
	HANDLE hfileSearch = FindFirstFile(searchbuffer, &findData);
	pBox = (CComboBox*)GetDlgItem (IDC_MARK_SET_LIST);
	pBox->ResetContent();
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		strBuffer = findData.cFileName;
		m_strMarkSetName = strBuffer.SpanExcluding(".");
		bFilesExist = TRUE;
		do {
			strBuffer = findData.cFileName;
			strBuffer = strBuffer.SpanExcluding(".");
			pBox->AddString(strBuffer);
		}while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);

	if (strFileName.IsEmpty())
		pBox->SetCurSel(0);
	else
		if (pBox->SelectString(0, LPCTSTR (strFileName)) != CB_ERR)
			m_strMarkSetName = strFileName;

	return bFilesExist;  	
}

BOOL CDlgSetMarks::LoadMarkSet()
{
	char pszFileName[MAX_PATH];
	CFile MarkDefFile;
	CFileException fileException;

	strcpy(pszFileName, theApp.settings.m_szDataDir); // TODO: Verify the path
	strcat(pszFileName, "\\");
	strcat(pszFileName, m_strMarkSetName);
	strcat(pszFileName, ".drz");
	if (!MarkDefFile.Open(pszFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n",
				pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		m_markList.RemoveAll(); // Be sure to work with an empty list

		CArchive archive(&MarkDefFile, CArchive::load);
		m_markList.Serialize(archive);
		archive.Close();

		MarkDefFile.Close();
		if (m_markList.IsEmpty()) 
			return FALSE; 
		else
			return TRUE;  	
	}
}

void CDlgSetMarks::UpdateMembers(POSITION pos)
{
	if (pos == NULL)
		return;

	CMarkOld markBuffer;

	markBuffer = m_markList.GetAt(pos);
//	m_strMarkName	 = markBuffer.m_strFormatName;
	m_fMarkWidth	 = markBuffer.m_fMarkWidth;
	m_fMarkHeight	 = markBuffer.m_fMarkHeight;
	m_MarkSource	 = markBuffer.m_MarkSource;
	m_strMarkEpsName = markBuffer.m_MarkSource.m_strDocumentFullpath;
	m_strTextmark	 = markBuffer.m_strTextmark;

	if (m_strTextmark.IsEmpty())
	{
		((CStatic*)GetDlgItem(IDC_TEXTMARK_STATIC))->ShowWindow(SW_HIDE);
		((CEdit*)GetDlgItem(IDC_TEXTMARKSTRING))->ShowWindow(SW_HIDE);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_TEXTMARK_STATIC))->ShowWindow(SW_SHOW);
		((CEdit*)GetDlgItem(IDC_TEXTMARKSTRING))->ShowWindow(SW_SHOW);
	}

	m_MarkSourceWindow.m_pMarkSource = &m_MarkSource;
	m_MarkSourceWindow.Invalidate();	
	m_MarkSourceWindow.UpdateWindow();

//	m_MarkPreViewWindow.m_nSelectedMark = ((CListBox*)GetDlgItem(IDC_MARK_LIST))->GetCurSel();
	m_MarkPreViewWindow.m_bLockWindow = FALSE;
	m_MarkPreViewWindow.Invalidate();	
	m_MarkPreViewWindow.UpdateWindow();	
}

void CDlgSetMarks::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (nSBCode == SB_ENDSCROLL)
		return;

	int n_NewPos;
	SCROLLINFO info;
	pScrollBar->GetScrollInfo(&info, SIF_ALL);
	switch(nSBCode)
	{
	case SB_LINERIGHT:	   n_NewPos = m_MarkPreViewWindow.mScrollPos.x + info.nMax / 40; break;
	case SB_LINELEFT:	   n_NewPos = m_MarkPreViewWindow.mScrollPos.x - info.nMax / 40; break;
	case SB_PAGERIGHT:	   n_NewPos = m_MarkPreViewWindow.mScrollPos.x + info.nMax / 10; break;
	case SB_PAGELEFT:	   n_NewPos = m_MarkPreViewWindow.mScrollPos.x - info.nMax / 10; break;
	case SB_THUMBPOSITION: n_NewPos = nPos;	break;
	case SB_THUMBTRACK:	   n_NewPos = nPos;	break;
	default:			   return;
	}

	if (n_NewPos > (m_MarkPreViewWindow.mScrollSize.cx - (int)info.nPage))
		n_NewPos = m_MarkPreViewWindow.mScrollSize.cx - info.nPage;
	if (n_NewPos < 0)
		n_NewPos = 0;
	m_MarkPreViewWindow.mScrollPos.x = n_NewPos;

	pScrollBar->SetScrollPos(n_NewPos);

	m_MarkPreViewWindow.m_bLockWindow = FALSE;
	m_MarkPreViewWindow.Invalidate();	
	m_MarkPreViewWindow.UpdateWindow();	
}

void CDlgSetMarks::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (nSBCode == SB_ENDSCROLL)
		return;

	int nBarPos;
	int nViewPos;
	SCROLLINFO info;
	pScrollBar->GetScrollInfo(&info, SIF_ALL);
	switch(nSBCode)
	{
	case SB_LINEDOWN:	   nViewPos = m_MarkPreViewWindow.mScrollPos.y - info.nMax / 40;
						   nBarPos = info.nPos + info.nMax / 40; 
						   break;
	case SB_LINEUP:		   nViewPos = m_MarkPreViewWindow.mScrollPos.y + info.nMax / 40;
						   nBarPos = info.nPos - info.nMax / 40; 
						   break;
	case SB_PAGEDOWN:	   nViewPos = m_MarkPreViewWindow.mScrollPos.y - info.nMax / 10;
						   nBarPos = info.nPos + info.nMax / 10;  
						   break;
	case SB_PAGEUP:		   nViewPos = m_MarkPreViewWindow.mScrollPos.y + info.nMax / 10;
						   nBarPos = info.nPos - info.nMax / 10;
						   break;
	case SB_THUMBPOSITION: 
	case SB_THUMBTRACK:	   nBarPos = nPos;
						   nViewPos = info.nMax - info.nPage - nPos;
						   break;
	default:			   return;
	}

	if (nViewPos > ((int)info.nMax - (int)info.nPage))
		nViewPos = info.nMax - info.nPage;
	if (nViewPos < info.nMin)
		nViewPos = info.nMin;
	m_MarkPreViewWindow.mScrollPos.y = nViewPos;

	pScrollBar->SetScrollPos(nBarPos);

	m_MarkPreViewWindow.m_bLockWindow = FALSE;
	m_MarkPreViewWindow.Invalidate();	
	m_MarkPreViewWindow.UpdateWindow();	
}
