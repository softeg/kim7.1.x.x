#pragma once
#include "afxwin.h"

class COwnerDrawnMenu :	public CMenu
{
public:
	COwnerDrawnMenu(void);
	~COwnerDrawnMenu(void);

public:
	CFont m_font;

protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
};
