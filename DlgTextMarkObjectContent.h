#if !defined(AFX_DLGTEXTMARKOBJECTCONTENT_H__2FB7E5D2_B6EF_11D5_88C0_0040F68C1EF7__INCLUDED_)
#define AFX_DLGTEXTMARKOBJECTCONTENT_H__2FB7E5D2_B6EF_11D5_88C0_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgTextMarkObjectContent.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgTextMarkObjectContent 

class CDlgTextMarkObjectContent : public CDialog
{
// Konstruktion
public:
	CDlgTextMarkObjectContent(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgTextMarkObjectContent();

// Dialogfelddaten
	//{{AFX_DATA(CDlgTextMarkObjectContent)
	enum { IDD = IDD_TEXTMARKOBJECT_CONTENT };
	CString	m_strOutputText;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgTextMarkObjectContent)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

public:
	class CDlgLayoutObjectControl*	m_pParent;
	BOOL							m_bInitialShowFoldSheet;
	CDlgSystemGenMarkColorControl	m_printColorControl;
	CMenu							m_placeholderMenu;
	CPlaceholderEdit				m_placeholderEdit;
	CBrush							m_hollowBrush, m_grayBrush;


// Implementierung
public:
	void CheckTextmarkForFoldsheetView();
	void LoadData();
	void SaveData();
	void Apply();
	void OnSelectPlaceholder(int nID);


protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgTextMarkObjectContent)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSelectPlaceholder();
	afx_msg void OnPlaceholderFoldsheetNum();
	afx_msg void OnPlaceholderColorName();
	afx_msg void OnPlaceholderSheetside();
	afx_msg void OnPlaceholderWebNum();
	afx_msg void OnPlaceholderColorNum();
	afx_msg void OnPlaceholderFoldsheetNumCG();
	afx_msg void OnPlaceholderJobTitle();
	afx_msg void OnPlaceholderTileNum();
	afx_msg void OnPlaceholderSheetNum();
	afx_msg void OnPlaceholderFile();
	afx_msg void OnPlaceholderLowpageSide();
	afx_msg void OnPlaceholderLowpageSheet();
	afx_msg void OnPlaceholderHighpageSide();
	afx_msg void OnPlaceholderHighpageSheet();
	afx_msg void OnPlaceholderPressDevice();
	afx_msg void OnPlaceholderJob();
	afx_msg void OnPlaceholderJobNum();
	afx_msg void OnPlaceholderCustomer();
	afx_msg void OnPlaceholderCustomerNum();
	afx_msg void OnPlaceholderCreator();
	afx_msg void OnPlaceholderCreationDate();
	afx_msg void OnPlaceholderLastModified();
	afx_msg void OnPlaceholderScheduledToPrint();
	afx_msg void OnPlaceholderOutputDate();
	afx_msg void OnPlaceholderNotes();
	afx_msg void OnPlaceholderAsir();
	afx_msg void OnPlaceholderPageNumList();
	afx_msg void OnPlaceholderPageNum();
	afx_msg void OnPlaceholderProductName();
	afx_msg void OnPlaceholderProductPartName();
	afx_msg void OnPlaceholderPaperGrain();
	afx_msg void OnPlaceholderPaperType();
	afx_msg void OnPlaceholderPaperGrammage();
	afx_msg void OnPlaceholderWorkStyle();
	afx_msg void OnPlaceholderFoldScheme();
	afx_msg void OnPlaceholderNumFoldsheets();
	afx_msg void OnPlaceholderBlank();
	afx_msg void OnPaint();
	//}}AFX_MSG
	afx_msg BOOL OnToolTipNeedText(UINT id, NMHDR * pNMHDR, LRESULT * pResult);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonPlaceholderInfo();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGTEXTMARKOBJECTCONTENT_H__2FB7E5D2_B6EF_11D5_88C0_0040F68C1EF7__INCLUDED_
