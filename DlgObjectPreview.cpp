// DlgObjectPreview.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgObjectPreview 


CDlgObjectPreview::CDlgObjectPreview(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgObjectPreview::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgObjectPreview)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void CDlgObjectPreview::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgObjectPreview)
	DDX_Control(pDX, IDC_OBJECT_PREVIEWFRAME, m_objectPreviewFrame);
	DDX_Control(pDX, IDC_PREVIEW_VSCROLLBAR, m_scrollbarV);
	DDX_Control(pDX, IDC_PREVIEW_HSCROLLBAR, m_scrollbarH);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgObjectPreview, CDialog)
	//{{AFX_MSG_MAP(CDlgObjectPreview)
	ON_WM_PAINT()
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgObjectPreview 

BOOL CDlgObjectPreview::OnInitDialog() 
{
	CDialog::OnInitDialog();

//	m_pParent = (CDlgLayoutObjectProperties*)GetParent();

	if ( ! m_pParent)
		EndDialog(IDCANCEL);
	
	if ( ! m_pParent->m_pMark)
		EndDialog(IDCANCEL);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgObjectPreview::PrepareUpdatePreview()
{
	m_sizeViewport = CSize(1,1);

	CRect previewFrameRect;
	m_objectPreviewFrame.GetClientRect(previewFrameRect);
	m_objectPreviewFrame.MapWindowPoints(this, previewFrameRect);
	InvalidateRect(previewFrameRect);

	CRect outBBox, trimBox;
	GetBoxes(outBBox, trimBox);
	if ( (outBBox.Width() == 0) || (outBBox.Height() == 0) )
	{
		UpdateWindow();
		return;
	}

	m_sizeViewport.cx = previewFrameRect.Width(); m_sizeViewport.cy = previewFrameRect.Height();
	float fThreshold = 15.0f;
	float fScale	 = 1.0f;
	if ( ((float)previewFrameRect.Width() / (float)outBBox.Width()) < ((float)previewFrameRect.Height() / (float)abs(outBBox.Height())) )
		fScale = (float)previewFrameRect.Width() / (float)outBBox.Width();
	else
		fScale = (float)previewFrameRect.Height() / (float)abs(outBBox.Height());
	if (abs(outBBox.Height()) * fScale < fThreshold)
	{
		fScale = fThreshold / ((float)abs(outBBox.Height()) * fScale);
		m_sizeViewport.cx *= fScale; m_sizeViewport.cy = (int)fThreshold;
	}
	else
		if (outBBox.Width() * fScale < fThreshold)
		{
			fScale = fThreshold / ((float)outBBox.Width() * fScale);
			m_sizeViewport.cx = (int)fThreshold; m_sizeViewport.cy *= fScale;
		}

	SCROLLINFO scrollInfo;
	scrollInfo.cbSize = sizeof(scrollInfo); 
    scrollInfo.fMask  = SIF_RANGE | SIF_PAGE | SIF_POS; 
    scrollInfo.nMin   = 0; 
    scrollInfo.nMax   = m_sizeViewport.cx - 1; 
    scrollInfo.nPage  = previewFrameRect.Width(); 
    scrollInfo.nPos   = m_scrollbarH.GetScrollPos(); 	
	m_scrollbarH.SetScrollInfo(&scrollInfo);
	m_scrollbarH.SetScrollRange(0, m_sizeViewport.cx - 1);

    scrollInfo.nMax   = m_sizeViewport.cy - 1; 
    scrollInfo.nPage  = previewFrameRect.Height(); 
    scrollInfo.nPos   = m_scrollbarV.GetScrollPos(); 	
	m_scrollbarV.SetScrollInfo(&scrollInfo);
	m_scrollbarV.SetScrollRange(0, m_sizeViewport.cy - 1);

	if (m_sizeViewport.cx > previewFrameRect.Width())
		m_scrollbarH.ShowWindow(SW_SHOW);
	else
		m_scrollbarH.ShowWindow(SW_HIDE);

	if (m_sizeViewport.cy > previewFrameRect.Height())
		m_scrollbarV.ShowWindow(SW_SHOW);
	else
		m_scrollbarV.ShowWindow(SW_HIDE);

	if ( ! m_pParent->m_pMark)
	{
		UpdateWindow();
		return;
	}
	CPageTemplate* pTemplate = &m_pParent->m_pMark->m_markTemplate;
	if ( ! pTemplate)
	{
		UpdateWindow();
		return;
	}
	if ( ! m_pParent->m_pMark->m_MarkSource.m_PageSourceHeaders.GetSize())
	{
		UpdateWindow();
		return;
	}

	CPageSource* pObjectSource = &m_pParent->m_pMark->m_MarkSource;
	if (pObjectSource)
	{
		if (pObjectSource->m_nType == CPageSource::SystemCreated)
		{
			int	nIndex = pObjectSource->m_strSystemCreationInfo.Find(" ");
			if (nIndex == -1)	// invalid string
			{
				UpdateWindow();
				return;
			}

			CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
			CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
			strParams.TrimLeft(); strParams.TrimRight();
			if (strContentType == "$SECTION_BAR")
			{
				CImpManDoc* pDoc = (CImpManDoc*)CImpManDoc::GetDoc();
				float fBarWidth, fBarHeight;
				sscanf((LPCTSTR)strParams, "%f %f", &fBarWidth, &fBarHeight);
				CRect barRect = CSectionMark::GetSectionBarRect(CPrintSheetView::WorldToLP(fBarWidth), CPrintSheetView::WorldToLP(fBarHeight), outBBox, 
																0, m_pParent->m_pMark->m_nHeadPosition, (pDoc) ? pDoc->GetActPrintSheet() : NULL);
				if (m_scrollbarH.IsWindowVisible())
				{
					float fScale = ((float)m_sizeViewport.cx / outBBox.Width());
					int nPos = (outBBox.left - barRect.left) * fScale;
					int nMinPos = m_scrollbarV.GetScrollPos();
					int nMaxPos = nMinPos + scrollInfo.nPage;
					if ( (nPos < nMinPos) || ((nPos + barRect.Width() * fScale) > nMaxPos) )
						m_scrollbarH.SetScrollPos(nPos); 
				}
				if (m_scrollbarV.IsWindowVisible())
				{
					float fScale = ((float)m_sizeViewport.cy / (float)abs(outBBox.Height()));
					int nPos = (outBBox.top - barRect.top) * fScale;
					int nMinPos = m_scrollbarV.GetScrollPos();
					int nMaxPos = nMinPos + scrollInfo.nPage;
					if ( (nPos < nMinPos) || ((nPos + abs(barRect.Height()) * fScale) > nMaxPos) )
						m_scrollbarV.SetScrollPos(nPos); 
				}
			}
		}
	}

	UpdateWindow();
}

void CDlgObjectPreview::GetBoxes(CRect& outBBox, CRect& trimBox)
{
	if ( ! m_pParent->m_pMark)
		return;
	CPageTemplate* pTemplate = &m_pParent->m_pMark->m_markTemplate;
	if ( ! pTemplate)
		return;
	if ( ! m_pParent->m_pMark->m_MarkSource.m_PageSourceHeaders.GetSize())
		return;

	CPageSource*	   pObjectSource	   = &m_pParent->m_pMark->m_MarkSource;
	CPageSourceHeader* pObjectSourceHeader = &m_pParent->m_pMark->m_MarkSource.m_PageSourceHeaders[0];

	if ( ! pObjectSource || ! pObjectSourceHeader)
		return;

	outBBox.left   = 0; 
	outBBox.bottom = 0; 
	if (m_pParent->m_pMark->m_nType == CMark::CustomMark)
	{
		outBBox.right = (int)((pObjectSourceHeader->m_fBBoxRight - pObjectSourceHeader->m_fBBoxLeft)   * pTemplate->m_fContentScaleX / PICA_POINTS);
		outBBox.top   = (int)((pObjectSourceHeader->m_fBBoxTop   - pObjectSourceHeader->m_fBBoxBottom) * pTemplate->m_fContentScaleY / PICA_POINTS); 
	}
	else
	{
		if (m_pParent->m_pMark->m_nType == CMark::SectionMark)
		{
			CImpManDoc* pDoc = (CImpManDoc*)CImpManDoc::GetDoc();
			float fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;
			CLayoutObject* pSpinePage = (CLayoutObject*)m_pParent->m_pMark->GetSheetTrimBox((void*)(pDoc) ? pDoc->GetActPrintSheet() : NULL, 0, 0, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);
			if (pSpinePage)
			{
				outBBox.right = fTrimBoxRight - fTrimBoxLeft; 
				outBBox.top   = fTrimBoxTop   - fTrimBoxBottom; 
			}
			else
				outBBox.right = outBBox.bottom = 0; 
		}
		else
		{
			outBBox.right = (int)(m_pParent->m_pMark->m_fMarkWidth  * pTemplate->m_fContentScaleX / PICA_POINTS);
			outBBox.top   = (int)(m_pParent->m_pMark->m_fMarkHeight * pTemplate->m_fContentScaleY / PICA_POINTS); 
		}
	}
	outBBox.left   = CPrintSheetView::WorldToLP(outBBox.left);
	outBBox.bottom = CPrintSheetView::WorldToLP(outBBox.bottom);
	outBBox.right  = CPrintSheetView::WorldToLP(outBBox.right);
	outBBox.top    = CPrintSheetView::WorldToLP(outBBox.top);

	trimBox.left   = (int)((pObjectSourceHeader->m_fTrimBoxLeft - pObjectSourceHeader->m_fBBoxLeft)	  / PICA_POINTS);
	trimBox.top	   = (int)((pObjectSourceHeader->m_fBBoxTop		- pObjectSourceHeader->m_fTrimBoxTop) / PICA_POINTS);
	trimBox.right  = trimBox.left + (int)((pObjectSourceHeader->m_fTrimBoxRight - pObjectSourceHeader->m_fTrimBoxLeft)   / PICA_POINTS);	  
	trimBox.bottom = trimBox.top  + (int)((pObjectSourceHeader->m_fTrimBoxTop   - pObjectSourceHeader->m_fTrimBoxBottom) / PICA_POINTS); 
	trimBox.left   = CPrintSheetView::WorldToLP(trimBox.left);
	trimBox.right  = CPrintSheetView::WorldToLP(trimBox.right);
	trimBox.bottom = CPrintSheetView::WorldToLP(trimBox.bottom);
	trimBox.top    = CPrintSheetView::WorldToLP(trimBox.top);
}

void CDlgObjectPreview::OnPaint() 
{
	CPaintDC dc(this);

	CRect previewFrameRect;
	m_objectPreviewFrame.GetClientRect(previewFrameRect);
	m_objectPreviewFrame.MapWindowPoints(this, previewFrameRect);

	CRgn rgn; 
	rgn.CreateRectRgnIndirect(&previewFrameRect);  
	dc.SelectClipRgn(&rgn);  
	dc.FillSolidRect(previewFrameRect, MIDDLEGRAY);

	if ( ! m_pParent->m_pMark)
		return;
	CPageTemplate* pTemplate = &m_pParent->m_pMark->m_markTemplate;
	if ( ! pTemplate)
		return;
	if ( ! m_pParent->m_pMark->m_MarkSource.m_PageSourceHeaders.GetSize())
		return;

	CPageSource*	   pObjectSource	   = &m_pParent->m_pMark->m_MarkSource;
	CPageSourceHeader* pObjectSourceHeader = &m_pParent->m_pMark->m_MarkSource.m_PageSourceHeaders[0];

	if ( ! pObjectSource || ! pObjectSourceHeader)
		return;

	CRect outBBox, trimBox;
	GetBoxes(outBBox, trimBox);

	if ( (outBBox.Width() == 0) || (outBBox.Height() == 0) )
		return;

	dc.SetMapMode(MM_ISOTROPIC);
	dc.SetWindowExt(outBBox.Width(), abs(outBBox.Height()));
	dc.SetViewportExt(m_sizeViewport.cx, -m_sizeViewport.cy);
	CSize sizeOut(outBBox.Width(), abs(outBBox.Height()));
	dc.LPtoDP(&sizeOut);
	dc.SetViewportOrg(previewFrameRect.left - m_scrollbarH.GetScrollPos(), previewFrameRect.top + sizeOut.cy - m_scrollbarV.GetScrollPos());

	CImpManDoc* pDoc = (CImpManDoc*)CImpManDoc::GetDoc();
	m_pParent->DrawPreview(&dc, previewFrameRect, pObjectSource, pObjectSourceHeader, pTemplate, outBBox, trimBox, (pDoc) ? pDoc->GetActPrintSheet() : NULL);
}

void CDlgObjectPreview::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int nOldPos = pScrollBar->GetScrollPos();

	SCROLLINFO scrollInfo;
	pScrollBar->GetScrollInfo(&scrollInfo, SIF_ALL);
	int nNewPos = scrollInfo.nPos;
	switch (nSBCode)
	{
		case SB_LEFT:			break;
		case SB_ENDSCROLL:		break;
		case SB_LINELEFT:		nNewPos -= scrollInfo.nPage/10;	break;
		case SB_LINERIGHT:		nNewPos += scrollInfo.nPage/10;	break;
		case SB_PAGELEFT:		nNewPos -= scrollInfo.nPage;	break;
		case SB_PAGERIGHT:		nNewPos += scrollInfo.nPage;	break;
		case SB_RIGHT:			break;
		case SB_THUMBPOSITION:	
		case SB_THUMBTRACK:		scrollInfo.cbSize = sizeof(SCROLLINFO);
								pScrollBar->GetScrollInfo(&scrollInfo, SIF_TRACKPOS);
						        nNewPos = scrollInfo.nTrackPos;
								break;
	}
	if (nNewPos < 0)
		nNewPos = 0;
	else
		if (nNewPos > pScrollBar->GetScrollLimit())
			nNewPos = pScrollBar->GetScrollLimit();

	pScrollBar->SetScrollPos(nNewPos);

	CRect previewFrameRect;
	m_objectPreviewFrame.GetClientRect(previewFrameRect);
	previewFrameRect.DeflateRect(1, 1);
	m_objectPreviewFrame.ScrollWindow(nOldPos - nNewPos, 0, NULL, previewFrameRect);

	CRect rect;
	m_objectPreviewFrame.GetUpdateRect(rect);
	m_objectPreviewFrame.MapWindowPoints(this, rect);
	InvalidateRect(rect);
	UpdateWindow();
	
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CDlgObjectPreview::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int nOldPos = pScrollBar->GetScrollPos();

	SCROLLINFO scrollInfo;
	pScrollBar->GetScrollInfo(&scrollInfo, SIF_ALL);
	int nNewPos = scrollInfo.nPos;
	switch (nSBCode)
	{
		case SB_TOP:			break;
		case SB_ENDSCROLL:		break;
		case SB_LINEUP:			nNewPos -= scrollInfo.nPage/10;	break;
		case SB_LINEDOWN:		nNewPos += scrollInfo.nPage/10;	break;
		case SB_PAGEUP:			nNewPos -= scrollInfo.nPage;	break;
		case SB_PAGEDOWN:		nNewPos += scrollInfo.nPage;	break;
		case SB_BOTTOM:			break;
		case SB_THUMBPOSITION:	
		case SB_THUMBTRACK:		scrollInfo.cbSize = sizeof(SCROLLINFO);
								pScrollBar->GetScrollInfo(&scrollInfo, SIF_TRACKPOS);
						        nNewPos = scrollInfo.nTrackPos;
								break;
	}
	if (nNewPos < 0)
		nNewPos = 0;
	else
		if (nNewPos > pScrollBar->GetScrollLimit())
			nNewPos = pScrollBar->GetScrollLimit();

	pScrollBar->SetScrollPos(nNewPos);

	CRect previewFrameRect;
	m_objectPreviewFrame.GetClientRect(previewFrameRect);
	previewFrameRect.DeflateRect(1, 1);
	m_objectPreviewFrame.ScrollWindow(0, nOldPos - nNewPos, NULL, previewFrameRect);

	CRect rect;
	m_objectPreviewFrame.GetUpdateRect(rect);
	m_objectPreviewFrame.MapWindowPoints(this, rect);
	InvalidateRect(rect);
	UpdateWindow();
	
	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}
