// DlgSaveProductionSetups.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgSaveProductionSetups.h"


// CDlgSaveProductionSetups-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgSaveProductionSetups, CDialog)

CDlgSaveProductionSetups::CDlgSaveProductionSetups(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSaveProductionSetups::IDD, pParent)
{

}

CDlgSaveProductionSetups::~CDlgSaveProductionSetups()
{
}

void CDlgSaveProductionSetups::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PRODSETUP_NAME_EDIT, m_strProductionSetupName);
	DDX_Check(pDX, IDC_PRODSETUP_DEFAULT_CHECK, m_bIsDefault);
}


BEGIN_MESSAGE_MAP(CDlgSaveProductionSetups, CDialog)
END_MESSAGE_MAP()


// CDlgSaveProductionSetups-Meldungshandler

BOOL CDlgSaveProductionSetups::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bIsDefault = FALSE;
	if ( ! m_strProductionSetupName.IsEmpty())
	{
		CProductionSetupDeprecated productionSetup;
		productionSetup.Load(m_strProductionSetupName);
		m_bIsDefault = productionSetup.m_bIsDefault;
	}

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgSaveProductionSetups::OnOK()
{
	UpdateData(TRUE);

	if (m_bIsDefault)
	{
		CString strWildCard;
		strWildCard.Format(_T("%s\\ProductionSetups\\*.set"), theApp.GetDataFolder()); 

		CProductionSetupDeprecated productionSetup;
		CFileFind		 finder;
		BOOL			 bFound = finder.FindFile(strWildCard);
		while (bFound)
		{
			bFound = finder.FindNextFile();
			productionSetup.Load(finder.GetFileTitle());
			productionSetup.m_bIsDefault = FALSE;
			productionSetup.Save(finder.GetFileTitle());
		}
		finder.Close();
	}

	CDialog::OnOK();
}
