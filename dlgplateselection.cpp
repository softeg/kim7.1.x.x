// DlgPlateSelection.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPlateSelection dialog


CDlgPlateSelection::CDlgPlateSelection(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPlateSelection::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPlateSelection)
	//}}AFX_DATA_INIT
}


CDlgPlateSelection::~CDlgPlateSelection()
{
	DestroyWindow();
}


void CDlgPlateSelection::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPlateSelection)
	DDX_Control(pDX, IDC_MSFLEXGRID_PLATES, m_PlateGrid);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPlateSelection, CDialog)
	//{{AFX_MSG_MAP(CDlgPlateSelection)
	ON_BN_CLICKED(IDC_BLEED_TYPE, OnBleedType)
	ON_BN_CLICKED(IDC_BLEED_SINGLE, OnBleedSingle)
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPlateSelection message handlers

BOOL CDlgPlateSelection::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// First of all remove unused colors from color definition table.
	// In future we should put this function to other locations, i.e. after removing color seps. etc.
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		pDoc->m_ColorDefinitionTable.UpdateColorInfos();

	CString string1, string2;
	string1.LoadString(IDS_PLATE_SUMMARY);
	string2.LoadString(IDS_AUTOMATIC_BLEED);
	SetWindowText(string1 + string2);

	CheckRadioButton(IDC_BLEED_TYPE, IDC_BLEED_SINGLE, IDC_BLEED_TYPE);

	CString stringSheet, stringType, stringNum;
	stringSheet.LoadString(IDS_SHEET_TEXT);
	stringType.LoadString(IDS_TYPE_TEXT);
	stringNum.LoadString(IDS_NUMBER_TEXT);

	m_PlateGrid.SetColWidth(1, m_PlateGrid.GetColWidth(1) / 2);
	m_PlateGrid.SetRow(0); 
	m_PlateGrid.SetCol(0);
	m_PlateGrid.SetCellAlignment(4);
	m_PlateGrid.SetText(stringSheet);
	m_PlateGrid.SetCol(1);
	m_PlateGrid.SetCellAlignment(4);
	m_PlateGrid.SetText(stringSheet);

	m_PlateGrid.SetRow(1); 
	m_PlateGrid.SetCol(0);
	m_PlateGrid.SetCellAlignment(0);
	m_PlateGrid.SetText(stringType);
	m_PlateGrid.SetCol(1);
	m_PlateGrid.SetCellAlignment(0);
	m_PlateGrid.SetText(stringNum);

	m_PlateGrid.SetMergeRow(0, TRUE);
	m_PlateGrid.SetMergeCol(0, TRUE);

	InitCellPictures();
	SynchronizeWithTree();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CDlgPlateSelection::InitCellPictures()
{
	CClientDC	dc(this);
	CDC			memDC1, memDC2;
	CPen		blackPen(PS_SOLID, 1, BLACK);
	memDC1.CreateCompatibleDC(&dc);
	memDC2.CreateCompatibleDC(&dc); 
	memDC1.SelectObject(&blackPen);
	memDC2.SelectObject(&blackPen);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	for (int i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
	{
		COLORREF cr;
		CBrush	 colorBrush(pDoc->m_ColorDefinitionTable[i].m_rgb);
		CBitmap  bitmap1, bitmap2;
		bitmap1.CreateCompatibleBitmap(&dc, 8, 8);
		bitmap2.CreateCompatibleBitmap(&dc, 8, 8);
		memDC1.SelectObject(&bitmap1);
		memDC2.SelectObject(&bitmap2);

		OleTranslateColor(m_PlateGrid.GetBackColorFixed(), NULL, &cr);
		memDC1.FillSolidRect(0,0, 8,8, cr);
		memDC2.FillSolidRect(0,0, 8,8, cr);
		memDC1.SelectObject(&colorBrush);
		memDC2.SelectObject(&colorBrush);
		memDC1.Rectangle(0,0, 8,8);
		memDC2.Rectangle(0,0, 8,8);

		CPictureHolder* pPictHolder = new CPictureHolder;
		pPictHolder->CreateFromBitmap(&bitmap1);
		m_EnabledPictHolderPtr.Add(pPictHolder);

		CPrintSheetView::FillTransparent(&memDC2, CRect(0,0,8,8), WHITE);
		pPictHolder = new CPictureHolder;
		pPictHolder->CreateFromBitmap(&bitmap2);
		m_DisabledPictHolderPtr.Add(pPictHolder);

		colorBrush.DeleteObject();
		bitmap1.DeleteObject();
		bitmap2.DeleteObject();
	}

	blackPen.DeleteObject();
	memDC1.DeleteDC();
	memDC2.DeleteDC();
}

void CDlgPlateSelection::SynchronizeWithTree()
{
	if (CImpManDoc::GetDoc()->GetActPrintSheet())
	{
		GetDlgItem(IDC_BLEED_TYPE)->EnableWindow();
		GetDlgItem(IDC_BLEED_SINGLE)->EnableWindow();
	}
	else
	{
		GetDlgItem(IDC_BLEED_TYPE)->EnableWindow(FALSE);
		GetDlgItem(IDC_BLEED_SINGLE)->EnableWindow(FALSE);
	}

	m_PlateGrid.SetRedraw(FALSE);

	InitPlateGridHeader();
	InitPlateGrid();
	FillPlateGrid();

	m_PlateGrid.SetRedraw(TRUE);
}

void CDlgPlateSelection::InitPlateGridHeader()
{
	m_PlateGrid.SetRows(2);
	m_PlateGrid.SetCols(2);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	int	nTreeSight	 = pDoc->GetActSight();

	for (int nSide = FRONTSIDE; nSide <= BACKSIDE; nSide++)
		if ((nTreeSight == nSide) || (nTreeSight == BOTHSIDES) || (nTreeSight == ALLSIDES))
		{
			if ( ! pDoc->m_ColorDefinitionTable.GetSize())
			{
				m_PlateGrid.SetCols(m_PlateGrid.GetCols() + 1);
				m_PlateGrid.SetCol(m_PlateGrid.GetCols() - 1);
				m_PlateGrid.SetRow(0);
				m_PlateGrid.SetCellAlignment(4);
				m_PlateGrid.SetText((nSide == FRONTSIDE) ? theApp.settings.m_szFrontName : theApp.settings.m_szBackName);
				m_PlateGrid.SetRow(1);
				m_PlateGrid.SetColData(m_PlateGrid.GetCols() - 1, FALSE);	// Column not disabled

				CString string = CString("    ") + _T("Default");
				m_PlateGrid.SetText(string);
			}
			else
				for (int i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
				{
					m_PlateGrid.SetCols(m_PlateGrid.GetCols() + 1);
					m_PlateGrid.SetCol(m_PlateGrid.GetCols() - 1);
					m_PlateGrid.SetRow(0);
					m_PlateGrid.SetCellAlignment(4);
					m_PlateGrid.SetText((nSide == FRONTSIDE) ? theApp.settings.m_szFrontName : theApp.settings.m_szBackName);
					m_PlateGrid.SetRow(1);
					m_PlateGrid.SetCellPictureAlignment(1);	//LeftCenter
					m_PlateGrid.SetRefCellPicture(m_EnabledPictHolderPtr[i]->GetPictureDispatch());
					m_PlateGrid.SetColData(m_PlateGrid.GetCols() - 1, FALSE);	// Column not disabled

					CString string = CString("    ") + pDoc->m_ColorDefinitionTable[i].m_strColorName;
					m_PlateGrid.SetText(string);
				}
		}
}

void CDlgPlateSelection::InitPlateGrid()
{
	m_DataList.Reset(m_PlateGrid.GetFixedRows(), m_PlateGrid.GetFixedCols());

	int nID1 = GetCheckedRadioButton(IDC_BLEED_TYPE, IDC_BLEED_SINGLE);

	CImpManDoc* pDoc			= CImpManDoc::GetDoc();
	CPrintSheetView* pView		= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheet* pActPrintSheet = pDoc->GetActPrintSheet();
	CLayout*	 pActLayout		= pDoc->GetActLayout();
	int			 nTreeSight		= pDoc->GetActSight();
	POSITION	 pos			= pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);

		FeedbackTreeItem(rPrintSheet.m_nPrintSheetNumber, FALSE);

		CLayout* pLayout = rPrintSheet.GetLayout();
		if (pLayout == NULL)
			continue;

		if (pActPrintSheet)
		{
			if (nID1 == IDC_BLEED_TYPE)		// all of actual type
				if (pLayout != pActLayout)
					continue;
			if (nID1 == IDC_BLEED_SINGLE)	// selected sheet - if any
				if (&rPrintSheet != pActPrintSheet)
					continue;
		}

		long lRow = m_PlateGrid.GetRows();
		long lCol = m_PlateGrid.GetFixedCols();

		CString string;
		string.Format("%s\t%s", pLayout->m_strLayoutName, rPrintSheet.GetNumber());
		m_PlateGrid.AddItem(string, COleVariant(lRow));

		if ((nTreeSight == FRONTSIDE) || (nTreeSight == BOTHSIDES) || (nTreeSight == ALLSIDES))
		{
			if ( ! pDoc->m_ColorDefinitionTable.GetSize())
			{
				CPlate* pPlate = &rPrintSheet.m_FrontSidePlates.m_defaultPlate;
				BOOL bDisabled = FALSE;
				if (pPlate)
				{
					CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(pPlate, pPlate->GetPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
					if (pDI)
						bDisabled = (pDI->m_nState == CDisplayItem::Selected) ? TRUE : FALSE;
				}
				CCellData cd(pPlate, lRow, lCol++, bDisabled);
				m_DataList.Add(cd);
			}
			else
				for (short i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
				{											// Get plate with color definition index i
					CPlate* pPlate = rPrintSheet.m_FrontSidePlates.GetPlateByColor(i);
					BOOL bDisabled = FALSE;
					if (pPlate)
					{
						CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(pPlate, pPlate->GetPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
						if (pDI)
							bDisabled = (pDI->m_nState == CDisplayItem::Selected) ? TRUE : FALSE;
					}
					CCellData cd(pPlate, lRow, lCol++, bDisabled);
					m_DataList.Add(cd);
				}
		}
		if ((nTreeSight == BACKSIDE) || (nTreeSight == BOTHSIDES) || (nTreeSight == ALLSIDES))
		{
			if (pLayout->m_nProductType)
			{
				for (short i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
				{
					CPlate* pPlate = NULL;
					BOOL bDisabled = TRUE;
					CCellData cd(pPlate, lRow, lCol++, bDisabled);
					m_DataList.Add(cd);
				}
			}
			else
			{
				if ( ! pDoc->m_ColorDefinitionTable.GetSize())
				{
					CPlate* pPlate = &rPrintSheet.m_BackSidePlates.m_defaultPlate;
					BOOL bDisabled = FALSE;
					if (pPlate)
					{
						CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(pPlate, pPlate->GetPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
						if (pDI)
							bDisabled = (pDI->m_nState == CDisplayItem::Selected) ? TRUE : FALSE;
					}
					CCellData cd(pPlate, lRow, lCol++, bDisabled);
					m_DataList.Add(cd);
				}
				else
					for (short i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
					{											// Get plate with color definition index i
						CPlate* pPlate = rPrintSheet.m_BackSidePlates.GetPlateByColor(i);
						BOOL bDisabled = FALSE;
						if (pPlate)
						{
							CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(pPlate, pPlate->GetPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
							if (pDI)
								bDisabled = (pDI->m_nState == CDisplayItem::Selected) ? TRUE : FALSE;
						}
						CCellData cd(pPlate, lRow, lCol++, bDisabled);
						m_DataList.Add(cd);
					}
			}
		}

		FeedbackTreeItem(rPrintSheet.m_nPrintSheetNumber, TRUE);
	}
}

void CDlgPlateSelection::FillPlateGrid(CExposure* pExposure, int nSide)
{
	CString string, string1, string2;
	string1.LoadString(IDS_PLATE_SUMMARY);

	CLayoutObject *pObject, *pCounterpartObject;
	if (pExposure)
	{
		pObject			   = pExposure->GetLayoutObject();
		pCounterpartObject = pObject->FindCounterpartObject();
		if (!pObject)
			return;
		CPageTemplate* pPageTemplate = pObject->GetCurrentPageTemplate(pExposure->GetPlate()->GetPrintSheet());
		string2.Format(IDS_MANUAL_BLEED, (pPageTemplate) ? pPageTemplate->m_strPageID : "");
		m_bGlobalMaskSelected = FALSE;
	}
	else
	{
		string2.LoadString(IDS_AUTOMATIC_BLEED);
		m_bGlobalMaskSelected = TRUE;
	}

	string = string1 + string2;	// complete string will be used later for set cell
	SetWindowText(string);

	m_PlateGrid.SetRedraw(FALSE);

	for (int nDataListIndex = 0; nDataListIndex < m_DataList.GetSize(); nDataListIndex++)
	{
		m_PlateGrid.SetRow(m_DataList[nDataListIndex].m_lGridRow);
		m_PlateGrid.SetCol(m_DataList[nDataListIndex].m_lGridCol);

		string = "";
		if (m_DataList[nDataListIndex].m_pPlate)
			if (pExposure)
			{
				CExposure* pCorrExposure = pObject->GetExposure(m_DataList[nDataListIndex].m_pPlate);
				if (pCorrExposure)
					CPrintSheetView::TruncateNumber(string, pCorrExposure->GetBleedValue(nSide));
				else
				{
					if (pCounterpartObject)
					{
						pCorrExposure = pCounterpartObject->GetExposure(m_DataList[nDataListIndex].m_pPlate);
						if (pCorrExposure)
							CPrintSheetView::TruncateNumber(string, pCorrExposure->GetBleedValue(pObject->FindCounterpartClickSide(nSide)));
					}
				}
			}
			else
				CPrintSheetView::TruncateNumber(string, m_DataList[nDataListIndex].m_pPlate->m_fOverbleedOutside);

		if (m_DataList[nDataListIndex].m_bDisabled)
			m_PlateGrid.SetCellForeColor(LIGHTGRAY);
		else
			m_PlateGrid.SetCellForeColor(BLACK);

		m_PlateGrid.SetText(string);
	}

	m_PlateGrid.SetRow(0);
	m_PlateGrid.SetCol(0);

	m_PlateGrid.SetRedraw(TRUE);
}

void CDlgPlateSelection::ClearPlateGrid()
{
	m_PlateGrid.SetRedraw(FALSE);

	CString string;
	for (int nDataListIndex = 0; nDataListIndex < m_DataList.GetSize(); nDataListIndex++)
	{
		m_PlateGrid.SetRow(m_DataList[nDataListIndex].m_lGridRow);
		m_PlateGrid.SetCol(m_DataList[nDataListIndex].m_lGridCol);

		string = "";

		m_PlateGrid.SetText(string);
	}

	m_PlateGrid.SetRow(0);
	m_PlateGrid.SetCol(0);

	m_PlateGrid.SetRedraw(TRUE);
}

void CDlgPlateSelection::SetStatusPlateGrid()
{
	m_PlateGrid.SetRedraw(FALSE);

	for (int nDataListIndex = 0; nDataListIndex < m_DataList.GetSize(); nDataListIndex++)
	{
		m_PlateGrid.SetRow(m_DataList[nDataListIndex].m_lGridRow);
		m_PlateGrid.SetCol(m_DataList[nDataListIndex].m_lGridCol);

		BOOL bDisabled = TRUE;
		CPlate* pPlate = m_DataList[nDataListIndex].m_pPlate;
		if (pPlate)
		{
			CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
			if (pView)
			{
				CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(pPlate, pPlate->GetPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
				if (pDI)
					bDisabled = (pDI->m_nState == CDisplayItem::Selected) ? TRUE : FALSE;
			}
		}

		m_DataList[nDataListIndex].m_bDisabled = bDisabled;
		if (bDisabled)
			m_PlateGrid.SetCellForeColor(LIGHTGRAY);
		else
			m_PlateGrid.SetCellForeColor(BLACK);
	}

	m_PlateGrid.SetRow(0);
	m_PlateGrid.SetCol(0);

	m_PlateGrid.SetRedraw(TRUE);
}

void CDlgPlateSelection::SetGlobalMasks(float fValue)
{
	for (int i = 0; i < m_DataList.GetSize(); i++)
	{
		if (m_DataList[i].m_pPlate)
			if ( ! m_DataList[i].m_bDisabled)
			{
				m_DataList[i].m_pPlate->m_fOverbleedOutside = fValue;
				m_DataList[i].m_pPlate->Invalidate();
			}
	}
}

void CDlgPlateSelection::SetLocalMasks(CExposure* pExposure, int nHitPosition, float fValue)
{
	CLayoutObject* pObject			  = pExposure->GetLayoutObject();
	CLayoutObject* pCounterpartObject = pObject->FindCounterpartObject();
	if (!pObject)
		return;

	BOOL		 bPanoramaPageClicked = FALSE;
	CPrintSheet* pPrintSheet		  = pExposure->GetPlate()->GetPrintSheet();
	if (pPrintSheet)
		bPanoramaPageClicked = pObject->IsPanoramaPage(*pPrintSheet);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));

	for (int i = 0; i < m_DataList.GetSize(); i++)
	{
		CPrintSheet* pPrintSheet = m_DataList[i].m_pPlate->GetPrintSheet();
		if ( ! pPrintSheet)
			continue;

		if (m_DataList[i].m_pPlate)
			if ( ! m_DataList[i].m_bDisabled)
			{
				CExposure* pCorrExposure = pObject->GetExposure(m_DataList[i].m_pPlate);
				if (pCorrExposure)
				{
					BOOL bIsPanoramaPage = pCorrExposure->GetLayoutObject()->IsPanoramaPage(*pPrintSheet);
					if (bPanoramaPageClicked)
					{
						if (bIsPanoramaPage)
							pCorrExposure->SetBleedValue(nHitPosition, fValue, TRUE, pView);
					}
					else
					{
						if ( ! bIsPanoramaPage)
							pCorrExposure->SetBleedValue(nHitPosition, fValue, TRUE, pView);
					}
					//pCorrExposure->SetBleedValuePanorama(nHitPosition, fValue, TRUE, pView);
				}
				else
				{
					if (pCounterpartObject)
					{
						pCorrExposure = pCounterpartObject->GetExposure(m_DataList[i].m_pPlate);
						if (pCorrExposure)
						{
							BOOL bIsPanoramaPage = pCorrExposure->GetLayoutObject()->IsPanoramaPage(*pPrintSheet);
							if (bPanoramaPageClicked)
							{
								if (bIsPanoramaPage)
									pCorrExposure->SetBleedValue(pObject->FindCounterpartClickSide(nHitPosition), fValue, TRUE, pView);
							}
							else
							{
								if ( ! bIsPanoramaPage)
									pCorrExposure->SetBleedValue(pObject->FindCounterpartClickSide(nHitPosition), fValue, TRUE, pView);
							}
							//pCorrExposure->SetBleedValuePanorama(pObject->FindCounterpartClickSide(nHitPosition), fValue, TRUE, pView);
						}
					}
				}
			}
	}
}


BEGIN_EVENTSINK_MAP(CDlgPlateSelection, CDialog)
    //{{AFX_EVENTSINK_MAP(CDlgPlateSelection)
	ON_EVENT(CDlgPlateSelection, IDC_MSFLEXGRID_PLATES, -600 /* Click */, OnClickMsflexgridPlates, VTS_NONE)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()
/*
void CDlgPlateSelection::OnClickMsflexgridPlates() 
{
	long lMouseRow	= m_PlateGrid.GetMouseRow();
	long lMouseCol	= m_PlateGrid.GetMouseCol();
	long lFixedRows	= m_PlateGrid.GetFixedRows();
	long lFixedCols	= m_PlateGrid.GetFixedCols();
	long lRow		= lMouseRow - lFixedRows;	
	long lCol		= lMouseCol - lFixedCols;	

	long lPictIndex = lCol % m_EnabledPictHolderPtr.GetSize();
//	BOOL bDisabled  = !m_PlateGrid.GetColData(lMouseCol);

	CPrintSheetView* pView		   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetTreeView* pTreeView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
/////////////////////////////////////////////////////////////////////////////
// A complete col (plate-color) has been selected
	if ((lMouseRow == 1) && (lMouseCol >= lFixedCols))
	{
//		m_PlateGrid.SetColData(lMouseCol, bDisabled);

//		if (bDisabled)
//			m_PlateGrid.SetCellForeColor(LIGHTGRAY);
//		else
//			m_PlateGrid.SetCellForeColor(BLACK);

		m_PlateGrid.SetRow(lMouseRow);
		m_PlateGrid.SetCol(lMouseCol);

//		if (bDisabled)
//		{
//			m_PlateGrid.SetRefCellPicture(m_DisabledPictHolderPtr[lPictIndex]->GetPictureDispatch());
//			m_PlateGrid.SetCellTextStyle(4);
//		}
//		else
//		{
//			m_PlateGrid.SetRefCellPicture(m_EnabledPictHolderPtr[lPictIndex]->GetPictureDispatch());
//			m_PlateGrid.SetCellTextStyle(0);
//		}

		for (long lRow = 0; lRow < m_DataList.m_lRows; lRow++)
		{
			CCellData* pData = m_DataList.GetData(lRow, lCol);
			if (!pData)
				continue;

//			pData->m_bDisabled = bDisabled;
			pData->m_bDisabled = ! pData->m_bDisabled;

			SetCellContents(pData, pView, pTreeView, lPictIndex);
		}
		if (pView)
		{
			pView->m_DisplayList.InvalidateItem(NULL);
			pView->UpdateWindow(); 
		}

		m_PlateGrid.SetRow(2);
	}
/////////////////////////////////////////////////////////////////////////////
// A a single cell (plate) has been selected
	if ((lMouseCol >= lFixedCols) && (lMouseRow >= lFixedRows))
	{
		CCellData* pData   = m_DataList.GetData(lRow, lCol);
		pData->m_bDisabled = !pData->m_bDisabled;

		SetCellContents(pData, pView, pTreeView, lPictIndex);

		if (pView)
		{
			pView->m_DisplayList.InvalidateItem(NULL);
			pView->UpdateWindow(); 
		}

		m_PlateGrid.SetRow(2);
	}
}
*/
void CDlgPlateSelection::SetCellContents(CCellData* pData, CPrintSheetView* pView, CPrintSheetTreeView* pTreeView) 
{
	if (pData->m_pPlate)
	{
		CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(pData->m_pPlate, pData->m_pPlate->GetPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
		if (pDI)
		{
			if (pData->m_bDisabled)
			{
				m_PlateGrid.SetCellForeColor(LIGHTGRAY);
				m_PlateGrid.SetCellTextStyle(4);

				pDI->m_nState = CDisplayItem::Selected;	 
				if (pTreeView)
					pTreeView->EnablePlateTreeItem((CPrintSheet*)pDI->m_pAuxData, (CPlate*)pDI->m_pData, FALSE);
			}
			else
			{
				m_PlateGrid.SetCellForeColor(BLACK);
				m_PlateGrid.SetCellTextStyle(0);

				pDI->m_nState = CDisplayItem::Normal;	 
				if (pTreeView)
					pTreeView->EnablePlateTreeItem((CPrintSheet*)pDI->m_pAuxData, (CPlate*)pDI->m_pData);
			}
		}
	}
}


void CDlgPlateSelection::OnBleedType() 
{
	SynchronizeWithTree();
}

void CDlgPlateSelection::OnBleedSingle() 
{
	SynchronizeWithTree();
}

void CDlgPlateSelection::OnClose() 
{
	CDialog::OnClose();
	DestroyWindow();
}

BOOL CDlgPlateSelection::DestroyWindow() 
{
	if (m_hWnd)
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
		{
			CPrintSheetFrame* pParentFrame = (CPrintSheetFrame*)pView->GetParentFrame();
			pParentFrame->m_bShowPlateTable = FALSE;
			pParentFrame->m_bEnablePlateButton		  = TRUE;
			pParentFrame->m_bEnableExposuresButton	  = TRUE;
			pParentFrame->m_bEnableBitmapButton		  = TRUE;
			pParentFrame->m_bEnableOutputPlatesButton = TRUE;

			ResetPlateTabs();
		}

		CPrintSheetTreeView* pTreeView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
		if (pTreeView)
			pTreeView->LowlightTreeItems(pView->m_DisplayList);

		for (int i = 0; i < m_EnabledPictHolderPtr.GetSize(); i++)
			delete m_EnabledPictHolderPtr[i];
		for (int i = 0; i < m_DisabledPictHolderPtr.GetSize(); i++)
			delete m_DisabledPictHolderPtr[i];

		m_EnabledPictHolderPtr.RemoveAll();
		m_DisabledPictHolderPtr.RemoveAll();
	}

	m_DataList.RemoveAll();
	
	return CDialog::DestroyWindow();
}

void CDlgPlateSelection::OnOK() 
{	// Make sure not to leave the dialog uncontrolled
}

void CDlgPlateSelection::OnCancel()
{	// Make sure not to leave the dialog uncontrolled
}

void CDlgPlateSelection::ResetPlateTabs()
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	for (int i = 0; i < m_DataList.GetSize(); i++)
	{
		if (m_DataList[i].m_pPlate)
		{
			if (!pView)
				continue;
			CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(m_DataList[i].m_pPlate, m_DataList[i].m_pPlate->GetPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
			if (pDI)
				pDI->m_nState = CDisplayItem::Normal;	 
		}
	}
	if (pView)
	{
		pView->m_DisplayList.InvalidateItem(NULL);
		pView->UpdateWindow(); 
	}
}	

void CDlgPlateSelection::FeedbackTreeItem(int nPrintSheetNumber, BOOL bEnabled)
{
	CPrintSheetTreeView* pView  = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
	if ( ! pView)
		return;

	UINT				 nState = (bEnabled) ? TVIS_BOLD : TVIS_CUT;
	POSITION			 pos	= pView->m_TreeItemList.GetHeadPosition();
	while (pos)
	{
		CTreeItem* pItem = &(pView->m_TreeItemList.GetNext(pos));

		if (pItem->nPrintSheetNumber == nPrintSheetNumber)
			pView->GetTreeCtrl().SetItemState(pItem->hTreeItem, nState, TVIS_BOLD | TVIS_CUT);
	}
}

void CDlgPlateSelection::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if (m_PlateGrid.m_hWnd)
		m_PlateGrid.MoveWindow(10, 40, cx - 20, (cy - 40 - 10));	// 10 is the border
}

void CDlgPlateSelection::OnClickMsflexgridPlates() 
{
	long lCol		= m_PlateGrid.GetMouseCol() - m_PlateGrid.GetFixedCols();	
	long lPictIndex = (m_EnabledPictHolderPtr.GetSize()) ? (lCol % m_EnabledPictHolderPtr.GetSize()) : -1;
	BOOL bDisabled  = !m_PlateGrid.GetColData(m_PlateGrid.GetMouseCol());

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));

	if ((m_PlateGrid.GetMouseRow() == 1) && (m_PlateGrid.GetMouseCol() >= m_PlateGrid.GetFixedCols()))
	{
		m_PlateGrid.SetColData(m_PlateGrid.GetMouseCol(), bDisabled);

		if (bDisabled)
			m_PlateGrid.SetCellForeColor(LIGHTGRAY);
		else
			m_PlateGrid.SetCellForeColor(BLACK);

		m_PlateGrid.SetRow(m_PlateGrid.GetMouseRow());
		m_PlateGrid.SetCol(m_PlateGrid.GetMouseCol());

		if (bDisabled)
		{
			if (lPictIndex >= 0)
				m_PlateGrid.SetRefCellPicture(m_DisabledPictHolderPtr[lPictIndex]->GetPictureDispatch());
			m_PlateGrid.SetCellTextStyle(4);
		}
		else
		{
			if (lPictIndex >= 0)
				m_PlateGrid.SetRefCellPicture(m_EnabledPictHolderPtr[lPictIndex]->GetPictureDispatch());
			m_PlateGrid.SetCellTextStyle(0);
		}

		for (long lRow = 0; lRow < m_DataList.m_lRows; lRow++)
		{
			CCellData* pData = m_DataList.GetData(lRow, lCol);
			if (!pData)
				continue;

			pData->m_bDisabled = bDisabled;

			if (pData->m_pPlate)
			{
				if (!pView)
					continue;
				CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(pData->m_pPlate, pData->m_pPlate->GetPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
				if (pDI)
				{
				    CPrintSheetTreeView* pTreeView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
					if (bDisabled)
					{
						pDI->m_nState = CDisplayItem::Selected;	 
				       	if (pTreeView)
       						pTreeView->EnablePlateTreeItem((CPrintSheet*)pDI->m_pAuxData, (CPlate*)pDI->m_pData, FALSE);
					}
					else
					{
						pDI->m_nState = CDisplayItem::Normal;	 
					    if (pTreeView)
       						pTreeView->EnablePlateTreeItem((CPrintSheet*)pDI->m_pAuxData, (CPlate*)pDI->m_pData);
					}
				}
			}
		}
		if (pView)
		{
			pView->m_DisplayList.InvalidateItem(NULL);
			pView->UpdateWindow(); 
		}

		m_PlateGrid.SetRow(2);
	}

/////////////////////////////////////////////////////////////////////////////
// A single cell (plate) has been selected
	if ((m_PlateGrid.GetMouseCol() >= m_PlateGrid.GetFixedCols()) && (m_PlateGrid.GetMouseRow() >= m_PlateGrid.GetFixedRows()))
	{
		long lRow = m_PlateGrid.GetMouseRow() - m_PlateGrid.GetFixedRows();	

		CCellData* pData   = m_DataList.GetData(lRow, lCol);
		pData->m_bDisabled = !pData->m_bDisabled;

		CPrintSheetTreeView* pTreeView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
		SetCellContents(pData, pView, pTreeView);

		if (!pData->m_bDisabled)
		{
			m_PlateGrid.SetRow(m_PlateGrid.GetFixedRows() - 1);
			if (lPictIndex >= 0)
				m_PlateGrid.SetRefCellPicture(m_EnabledPictHolderPtr[lPictIndex]->GetPictureDispatch());
			m_PlateGrid.SetCellTextStyle(0);
			m_PlateGrid.SetColData(m_PlateGrid.GetMouseCol(), FALSE);
			m_PlateGrid.SetRow(m_PlateGrid.GetMouseRow());
		}

		if (pView)
		{
			pView->m_DisplayList.InvalidateItem(NULL);
			pView->UpdateWindow(); 
		}
	}
}

void CDlgPlateSelection::OnApplyButton() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (m_bGlobalMaskSelected)
		pView->OnCharInplaceEditGlobalMask(VK_RETURN);
	else
		pView->OnCharInplaceEditBleed(VK_RETURN);
}
