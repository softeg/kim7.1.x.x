#pragma once



// CPrintSheetComponentsView-Ansicht

class CPrintSheetComponentsView : public CScrollView
{
	DECLARE_DYNCREATE(CPrintSheetComponentsView)

protected:
	CPrintSheetComponentsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetComponentsView();

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


enum displayColIDs {ColComponent, ColPages, ColNUp, ColFormat, ColQuantity, ColSheetCoverage, ColOverProduction};
typedef struct displayColumn
{
	int nID;
	int nWidth;
};
typedef CArray <struct displayColumn, struct displayColumn&> DISPLAY_COLUMN_LIST;

public:
	DISPLAY_COLUMN_LIST		m_displayColumns;


public:
	CSize					m_docSize;
	CHeaderCtrl  			m_HeaderCtrl;
	int			 			m_nHeaderHeight;
	int						m_nHeaderItemsTotalWidth;
	CFont		 			m_headerFont;
	CArray <int, int>		m_headerColumnWidths;
	int						m_nToolBarHeight;

public:
	CSize					CalcExtents();
	void					ResetDisplayColums();
	void					AddDisplayColumn(int nColID, int nWidth);
	CRect					Draw(CDC* pDC, CDisplayList* pDisplayList);
	static CRect			DrawFoldSheetTypeInfo  	   (CDC* pDC, CRect rcFrame, CFoldSheet* pFoldSheet, int nLayerIndex);
	static CRect			DrawFoldSheetTypePages 	   (CDC* pDC, CRect rcFrame, CFoldSheet* pFoldSheet, int nLayerIndex);
	static CRect			DrawComponentNUp	   	   (CDC* pDC, CRect rcFrame, int nNUp);
	static CRect			DrawComponentFormat	   	   (CDC* pDC, CRect rcFrame, float fWidth, float fHeight);
	static CRect			DrawComponentQuantity	   (CDC* pDC, CRect rcFrame, int nQuantity);
	static CRect			DrawComponentCoverage	   (CDC* pDC, CRect rcFrame, float fCoverage);
	static CRect			DrawComponentOverProduction(CDC* pDC, CRect rcFrame, int nOver, float fOverPercent);
	static CRect			DrawFlatProductTypeInfo	   (CDC* pDC, CRect rcFrame, CFlatProduct* pFlatProduct);
	void					SetHeaderWidth(int nIndex, int nValue);
	int						GetHeaderWidth(int nIndex, int nDefault);
	CPrintSheet*			GetActPrintSheet();
	CLayout*				GetActLayout();


protected:
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
public:
	virtual void OnInitialUpdate();     // Erster Aufruf nach Erstellung
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnHeaderTrack	 (NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* result);
};


