#if !defined(AFX_DLGLAYOUTOBJECTGEOMETRY_H__68AB1A03_B0D5_11D5_88B7_0040F68C1EF7__INCLUDED_)
#define AFX_DLGLAYOUTOBJECTGEOMETRY_H__68AB1A03_B0D5_11D5_88B7_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgLayoutObjectGeometry.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgLayoutObjectGeometry 

class CDlgLayoutObjectGeometry : public CDialog
{
// Konstruktion
public:
	CDlgLayoutObjectGeometry(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgLayoutObjectGeometry)
	enum { IDD = IDD_LAYOUTOBJECT_GEOMETRY };
	CSpinButtonCtrl	m_widthSpin;
	CSpinButtonCtrl	m_heightSpin;
	float	m_fMarkHeight;
	float	m_fMarkWidth;
	//}}AFX_DATA

public:
	CLayoutObject*	m_pLayoutObject;
	BYTE			m_nHeadPosition;
	CBitmap			m_bmMarkOrientation;
	int				m_nMarkType;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgLayoutObjectGeometry)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void LoadLayoutObjectData();
	void SaveLayoutObjectData();
	void LoadCustomMarkData(CMark* pMark);
	void SaveCustomMarkData(CMark* pMark);
	void Apply();

protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgLayoutObjectGeometry)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposMarkOrientationSpin(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGLAYOUTOBJECTGEOMETRY_H__68AB1A03_B0D5_11D5_88B7_0040F68C1EF7__INCLUDED_
