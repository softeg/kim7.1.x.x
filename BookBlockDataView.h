
#pragma once
#include "afxcmn.h"
#include "ToolBarGroup.h"
#include "DlgNumeration.h"
#include "OwnerDrawnButtonList.h"
#include "BindingDefsSelection.h"
#include "DlgShinglingAssistent.h"



// CBVFoldSheetListCtrl

class CBVFoldSheetListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CBVFoldSheetListCtrl)

public:
	CBVFoldSheetListCtrl();
	virtual ~CBVFoldSheetListCtrl();

public:
	enum EHighlight {HIGHLIGHT_NORMAL, HIGHLIGHT_ALLCOLUMNS, HIGHLIGHT_ROW};

	int m_nPrevSel;

protected:
	int m_nHighlight;		// Indicate type of selection highlighting

public:
	void SelectAll();
	void SelectRange(int nFrom, int nTo);

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult);
};




// CProdDataBindTrimView-Formularansicht

class CBookBlockDataView : public CFormView
{
	DECLARE_DYNCREATE(CBookBlockDataView)

protected:
	CBookBlockDataView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CBookBlockDataView();

public:
	enum { IDD = IDD_BOOKBLOCKDATA_VIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	CFont						m_smallFont, m_mediumFont, m_bigFont;
	CBrush						m_bkBrush, m_bkGrayBrush, m_hollowBrush;
	CBVFoldSheetListCtrl		m_foldSheetList;
	CImageList					m_productImageList, m_bindingImageList;
	CDlgNumeration*				m_pDlgNumeration;
	CDlgShinglingAssistent		m_dlgShinglingAssistent;


public:
	CString m_strProductionType;
	CString m_strBindingMethod;
	int		m_nBindingMethod;
	int		m_nProductionType;
	CString					m_strBindingName;
	CBindingDefsSelection	m_bindingDefsSelection;

public:
	CBoundProduct&		GetActiveBoundProduct();
	CBoundProductPart&	GetActiveBoundProductPart();
	void				InitializeFoldSheetList();
	int					GetPrintGroupIndex();
	CPrintGroup*		GetProductGroup();
	void				AddRemoveFoldsheet(BOOL bAdd);
	static void			MoveFoldSheet(CFoldSheet* pFoldSheet, BOOL bMoveUp);
	void				SelectFoldSheet(CFoldSheet* pFoldSheetToSelect, int nFoldSheetPart);
	static BOOL			InitFoldSheetNumerationDialog(CDlgNumeration* pDlg);
	static void			ChangeFoldSheetNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex);
	CFoldSheet*			GetFirstSelectedFoldSheet(int* pnSel = NULL);
	CFoldSheet*			GetLastSelectedFoldSheet(int* pnSel = NULL);
	CFoldSheet*			GetFoldSheet (int nSel);
	int					GetLayerIndex(int nSel);
	BOOL				IsInsideNumerationRange(int nItemIndex);


public:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();

	DECLARE_MESSAGE_MAP()
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnCbnSelchangeBvBindingMethod();
	afx_msg void OnCbnSelchangeBvProductionType();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedBvAddFoldsheet();
	afx_msg void OnBnClickedBvRemoveFoldsheet();
	afx_msg void OnLvnItemchangedPpFoldsheetlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBvDonumber();
	afx_msg void OnBnClickedBvStepAbove();
	afx_msg void OnBnClickedBvStepBelow();
public:
	CComboBoxEx m_productCombo;
	afx_msg void OnCbnSelchangeBvProductCombo();
};


