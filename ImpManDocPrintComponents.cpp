// ImpManDocPrintGroups.cpp : implementation of the CImpManDoc class
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PrintComponentsView.h"
#include "GraphicComponent.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "OrderDataFrame.h"
#include "NewPrintSheetFrame.h"
#include "PrintSheetNavigationView.h"
#include "PrintSheetView.h"


/////////////////////////////////////////////////////////////////////////////
// CPrintComponentsList

CPrintComponentsList::CPrintComponentsList()
{
}

void CPrintComponentsList::Analyze()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheetPlanningView*	pView1				  = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	COrderDataFrame*			pFrame1				  = (pView1) ? (COrderDataFrame*)pView1->GetParentFrame() : NULL;
	CPrintComponentsView*		pPrintComponentsView1 = (pFrame1) ? pFrame1->GetPrintComponentsView() : NULL;
	CPrintComponent lastActComponent1;
	if (pPrintComponentsView1)
	{
		CPrintComponent component = pPrintComponentsView1->GetActComponent();
		if ( ! component.IsNull())
			lastActComponent1 = component;
	}
	CPrintSheetNavigationView*	pView2				  = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	CNewPrintSheetFrame*		pFrame2				  = (pView2) ? (CNewPrintSheetFrame*)pView2->GetParentFrame() : NULL;
	CPrintComponentsView*		pPrintComponentsView2 = (pFrame2) ? pFrame2->GetPrintComponentsView() : NULL;
	CPrintComponent lastActComponent2;
	if (pPrintComponentsView2)
	{
		CPrintComponent component = pPrintComponentsView2->GetActComponent();
		if ( ! component.IsNull())
			lastActComponent2 = component;
	}

	RemoveAll();

	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);

		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			CBoundProductPart& rProductPart = rBoundProduct.m_parts[i];

			CPrintComponentsGroup componentsGroup;
			componentsGroup.m_colorantList.MixColors(rProductPart.m_colorantList);
			componentsGroup.m_strPaper = rProductPart.m_strPaper;
			componentsGroup.SetQuantityRange(rProductPart.m_nDesiredQuantity);
			//componentsGroup.m_strPressSetup = rProductPart.GetProductionProfile().m_strName;

			CFoldSheet* pFoldSheet = rProductPart.GetFirstFoldSheet();
			while (pFoldSheet)
			{
				//componentsGroup.m_printComponents.RemoveAll();
				for (int nLayerIndex = 0; nLayerIndex < pFoldSheet->m_LayerList.GetSize(); nLayerIndex++)
				{
					componentsGroup.AddFoldSheetComponent(rProductPart, pFoldSheet, nLayerIndex);
					AddFindComponentsGroup(componentsGroup);
				}
				pFoldSheet = rProductPart.GetNextFoldSheet(pFoldSheet);
			}

			int nNumUnfoldedPages = rProductPart.GetNumUnfoldedPages();
			if (nNumUnfoldedPages > 0)
			{
				componentsGroup.AddPageSectionComponent(rProductPart, nNumUnfoldedPages);
				AddFindComponentsGroup(componentsGroup);
			}
		}
	}

	pos = pDoc->m_flatProducts.GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);
		//if (rFlatProduct.GetNUpAll() <= 0)
		{
			CPrintComponentsGroup componentsGroup;
			componentsGroup.m_colorantList.MixColors(rFlatProduct.m_colorantList);
			componentsGroup.m_strPaper		= rFlatProduct.m_strPaper;
			componentsGroup.SetQuantityRange(rFlatProduct.m_nDesiredQuantity);
			CPrintSheet*		pPrintSheet = rFlatProduct.GetFirstPrintSheet();
			//CProductionProfile& rProfile	= (pPrintSheet) ? pPrintSheet->GetProductionProfile() : CProductionProfile();
			//componentsGroup.m_strPressSetup = rProfile.m_strName;

			componentsGroup.AddFlatProductComponent(rFlatProduct);
			AddFindComponentsGroup(componentsGroup);
		}
	}

	int nIndex = 0;
	for (int i = 0; i < pDoc->m_printComponentsList.GetSize(); i++)
	{
		CPrintComponentsGroup& rGroup = pDoc->m_printComponentsList[i];
		for (int j = 0; j < rGroup.m_printComponents.GetSize(); j++)
		{
			rGroup.m_printComponents[j].SetIndex(nIndex);
			nIndex++;
		}
	}

	if (pPrintComponentsView1)
	{
		pPrintComponentsView1->m_DisplayList.Invalidate();
		pPrintComponentsView1->m_DisplayList.DeselectAll(FALSE);
		pPrintComponentsView1->OnUpdate(NULL, 0, NULL);
		pPrintComponentsView1->UpdateWindow();

		for (int i = 0; i < GetSize(); i++)
		{
			CPrintComponentsGroup& rGroup = GetAt(i);
			for (int j = 0; j < rGroup.m_printComponents.GetSize(); j++)
			{
				CPrintComponent rComponent = rGroup.m_printComponents[j];
				if (rComponent.IsEquivalent(lastActComponent1))
				{
					pPrintComponentsView1->SetActComponent(rComponent);
					return;
				}
			}
		}
	}
	if (pPrintComponentsView2)
	{
		//pPrintComponentsView2->m_DisplayList.Invalidate();
		//pPrintComponentsView2->m_DisplayList.DeselectAll(FALSE);

		//for (int i = 0; i < GetSize(); i++)
		//{
		//	CPrintComponentsGroup& rGroup = GetAt(i);
		//	for (int j = 0; j < rGroup.m_printComponents.GetSize(); j++)
		//	{
		//		CPrintComponent rComponent = rGroup.m_printComponents[j];
		//		if (rComponent.IsEquivalent(lastActComponent2))
		//		{
		//			pPrintComponentsView2->OnUpdate(NULL, 0, NULL);
		//			pPrintComponentsView2->UpdateWindow();

		//			int nIndex = rComponent.GetIndex();
		//			CDisplayItem* pDI = pPrintComponentsView2->m_DisplayList.GetFirstItem();
		//			while (pDI)
		//			{
		//				if (nIndex == (int)pDI->m_pData)
		//					pDI->m_nState = CDisplayItem::Selected;
		//				else
		//					pDI->m_nState = CDisplayItem::Normal;

		//				pPrintComponentsView2->m_DisplayList.InvalidateItem(pDI);

		//				pDI = pPrintComponentsView2->m_DisplayList.GetNextItem(pDI);
		//			}
		//			pPrintComponentsView2->ResizeDetailsView();
		//			return;
		//		}
		//	}
		//}
		pPrintComponentsView2->OnUpdate(NULL, 0, NULL);
		pPrintComponentsView2->UpdateWindow();
		pPrintComponentsView2->ResizeDetailsView();
	}
}

void CPrintComponentsList::AddFindComponentsGroup(CPrintComponentsGroup& rComponentsGroup)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (rComponentsGroup.m_printComponents.GetSize() <= 0)
		return;

	CPrintComponent& rNewComponent = rComponentsGroup.m_printComponents.GetAt(rComponentsGroup.m_printComponents.GetSize() -1);

	for (int i = 0; i < GetSize(); i++)
	{
		CPrintComponentsGroup& rCurComponentsGroup = GetAt(i);
	
		if (rCurComponentsGroup.m_strPaper == rComponentsGroup.m_strPaper)
		{
			if (rCurComponentsGroup.CheckProductionProfile(rComponentsGroup, pDoc->m_productionSetup))
			{
				if (rCurComponentsGroup.CheckInsideQuantityRange(rComponentsGroup, pDoc->m_productionSetup))
				{
					if (rCurComponentsGroup.CheckInsideColorRange(rComponentsGroup, pDoc->m_productionSetup))
					{
						BOOL bAleadyExisting = FALSE;
						for (int j = 0; j < rCurComponentsGroup.m_printComponents.GetSize(); j++)
						{
							if (rCurComponentsGroup.m_printComponents[j].m_nType == rNewComponent.m_nType)
							{
								if (rNewComponent.m_nType == CPrintComponent::FoldSheet)
								{
									if ( (rCurComponentsGroup.m_printComponents[j].m_nFoldSheetIndex == rNewComponent.m_nFoldSheetIndex) && (rCurComponentsGroup.m_printComponents[j].m_nFoldSheetLayerIndex == rNewComponent.m_nFoldSheetLayerIndex) )
									{
										bAleadyExisting = TRUE;
										break;
									}
								}
								else
								if (rNewComponent.m_nType == CPrintComponent::PageSection)
								{
									if (rCurComponentsGroup.m_printComponents[j].m_nProductPartIndex == rNewComponent.m_nProductPartIndex)
									{
										bAleadyExisting = TRUE;
										rCurComponentsGroup.m_printComponents[j].m_nNumPages = rNewComponent.m_nNumPages;
										break;
									}
								}
								else
									if (rNewComponent.m_nType == CPrintComponent::Flat)
								{
									if (rCurComponentsGroup.m_printComponents[j].m_nFlatProductIndex == rNewComponent.m_nFlatProductIndex)
									{
										bAleadyExisting = TRUE;
										break;
									}
								}
							}
						}

						if ( ! bAleadyExisting)
						{
							rCurComponentsGroup.m_printComponents.Add(rNewComponent);
							rCurComponentsGroup.m_colorantList.MixColors(rComponentsGroup.m_colorantList);
						}

						return;
					}
				}
			}
		}
	}

	Add(rComponentsGroup);
}

CPrintComponent CPrintComponentsList::GetComponent(int nComponentIndex)
{
	int nCurIndex = 0;
	for (int i = 0; i < GetSize(); i++)
	{
		CPrintComponentsGroup& rComponentsGroup = GetAt(i);
		for (int j = 0; j < rComponentsGroup.m_printComponents.GetSize(); j++)
		{
			if (nCurIndex == nComponentIndex)
				return rComponentsGroup.m_printComponents[j];

			nCurIndex++;
		}
	}

	return CPrintComponent();
}

CPrintComponent CPrintComponentsList::FindFoldSheetLayerComponent(int nFoldSheetIndex, int nFoldSheetLayerIndex)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CPrintComponentsGroup& rComponentsGroup = GetAt(i);
		for (int j = 0; j < rComponentsGroup.m_printComponents.GetSize(); j++)
		{
			CPrintComponent& rComponent = rComponentsGroup.m_printComponents[j];
			if (rComponent.m_nType == CPrintComponent::FoldSheet)
			{
				if (rComponent.m_nFoldSheetIndex == nFoldSheetIndex)
					if (rComponent.m_nFoldSheetLayerIndex == nFoldSheetLayerIndex)
						return rComponent;
			}
		}
	}

	return CPrintComponent();
}

CPrintComponent CPrintComponentsList::FindPageSectionComponent(int nProductPartIndex)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CPrintComponentsGroup& rComponentsGroup = GetAt(i);
		for (int j = 0; j < rComponentsGroup.m_printComponents.GetSize(); j++)
		{
			CPrintComponent& rComponent = rComponentsGroup.m_printComponents[j];
			if (rComponent.m_nType == CPrintComponent::PageSection)
			{
				if (rComponent.m_nProductPartIndex == nProductPartIndex)
					return rComponent;
			}
		}
	}

	return CPrintComponent();
}

CPrintComponent CPrintComponentsList::FindFlatComponent(int nFlatProductIndex)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CPrintComponentsGroup& rComponentsGroup = GetAt(i);
		for (int j = 0; j < rComponentsGroup.m_printComponents.GetSize(); j++)
		{
			CPrintComponent& rComponent = rComponentsGroup.m_printComponents[j];
			if (rComponent.m_nType == CPrintComponent::Flat)
			{
				if (rComponent.m_nFlatProductIndex == nFlatProductIndex)
					return rComponent;
			}
		}
	}

	return CPrintComponent();
}

CPrintComponent CPrintComponentsList::FindPrintSheet(CPrintSheet* pPrintSheet)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CPrintComponentsGroup& rComponentsGroup = GetAt(i);
		for (int j = 0; j < rComponentsGroup.m_printComponents.GetSize(); j++)
		{
			CPrintComponent& rComponent = rComponentsGroup.m_printComponents[j];
			CPrintSheet* pCurPrintSheet = rComponent.GetFirstPrintSheet();
			while (pCurPrintSheet)
			{
				if (pCurPrintSheet == pPrintSheet)
					return rComponent;
				pCurPrintSheet = rComponent.GetNextPrintSheet(pCurPrintSheet);
			}
		}
	}

	return CPrintComponent();
}

CPrintComponentsGroup& CPrintComponentsList::FindGroup(CPrintComponent& rComponent)
{
	static CPrintComponentsGroup nullGroup;

	for (int i = 0; i < GetSize(); i++)
	{
		CPrintComponentsGroup& rComponentsGroup = GetAt(i);
		for (int j = 0; j < rComponentsGroup.m_printComponents.GetSize(); j++)
		{
			CPrintComponent& rCurComponent = rComponentsGroup.m_printComponents[j];
			if (rCurComponent.IsEquivalent(rComponent))
				return rComponentsGroup;
		}
	}

	return nullGroup;
}

int	CPrintComponentsList::GetNumComponentsAll()
{
	int nNum = 0;
	for (int i = 0; i < GetSize(); i++)
	{
		nNum += GetAt(i).m_printComponents.GetSize();;
	}
	return nNum;
}



/////////////////////////////////////////////////////////////////////////////
// CPrintComponentsGroup

IMPLEMENT_SERIAL(CPrintComponentsGroup, CPrintComponentsGroup, VERSIONABLE_SCHEMA | 1)

CPrintComponentsGroup::CPrintComponentsGroup()
{
	m_nMinQuantity	=  INT_MAX;
	m_nMaxQuantity	= -INT_MAX;
	SetDisplayBoxLong(CRect(0,0,0,0));
	SetDisplayBox(CRect(0,0,0,0));
}

CPrintComponentsGroup::CPrintComponentsGroup(const CPrintComponentsGroup& rPrintComponentsGroup)
{
	Copy(rPrintComponentsGroup);
}

const CPrintComponentsGroup& CPrintComponentsGroup::operator=(const CPrintComponentsGroup& rPrintComponentsGroup)
{
	Copy(rPrintComponentsGroup);

	return *this;
}

void CPrintComponentsGroup::Copy(const CPrintComponentsGroup& rPrintComponentsGroup)
{
	m_strPaper		= rPrintComponentsGroup.m_strPaper;
	m_nMinQuantity	= rPrintComponentsGroup.m_nMinQuantity;
	m_nMaxQuantity	= rPrintComponentsGroup.m_nMaxQuantity;
	m_dueDate		= rPrintComponentsGroup.m_dueDate;
	m_strPressSetup = rPrintComponentsGroup.m_strPressSetup;

	m_colorantList.RemoveAll();
	m_printComponents.RemoveAll();

	m_colorantList.Append(rPrintComponentsGroup.m_colorantList);
	m_printComponents.Append(rPrintComponentsGroup.m_printComponents);

	m_rcDisplayBoxLong = rPrintComponentsGroup.m_rcDisplayBoxLong;
	m_rcDisplayBox	   = rPrintComponentsGroup.m_rcDisplayBox;
}

// helper function for CPrintComponentsGroup elements
template <> void AFXAPI SerializeElements <CPrintComponentsGroup> (CArchive& ar, CPrintComponentsGroup* pPrintComponentsGroup, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrintComponentsGroup));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPrintComponentsGroup->m_strPaper;
			pPrintComponentsGroup->m_colorantList.Serialize(ar);
			ar << pPrintComponentsGroup->m_nMinQuantity;
			ar << pPrintComponentsGroup->m_nMaxQuantity;
			ar << pPrintComponentsGroup->m_dueDate;
			ar << pPrintComponentsGroup->m_strPressSetup;

			pPrintComponentsGroup->m_printComponents.Serialize(ar);
		}	
		else
		{ 
			switch (nVersion)
			{
				case 1:	ar >> pPrintComponentsGroup->m_strPaper;
						pPrintComponentsGroup->m_colorantList.Serialize(ar);
						ar >> pPrintComponentsGroup->m_nMinQuantity;
						ar >> pPrintComponentsGroup->m_nMaxQuantity;
						ar >> pPrintComponentsGroup->m_dueDate;
						ar >> pPrintComponentsGroup->m_strPressSetup;
						pPrintComponentsGroup->m_printComponents.Serialize(ar);
						break;
			}
		}

		pPrintComponentsGroup++;
	}
}

int CPrintComponentsGroup::GetIndex()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	for (int i = 0; i < pDoc->m_printComponentsList.GetSize(); i++)
	{
		CPrintComponentsGroup& rComponentsGroup = pDoc->m_printComponentsList[i];
		if (&rComponentsGroup == this)
			return i;
	}

	return -1;
}

void CPrintComponentsGroup::SetQuantityRange(int nQuantity)
{
	m_nMinQuantity = min(m_nMinQuantity, nQuantity);
	m_nMaxQuantity = max(m_nMaxQuantity, nQuantity);
}

BOOL CPrintComponentsGroup::CheckProductionProfile(CPrintComponentsGroup& rPrintComponentsGroup, CProductionSetup& rProductionSetup)
{
	CPrintComponent& rComponent	= rPrintComponentsGroup.m_printComponents[0];
	CPrintSheet* pPrintSheet = rComponent.GetFirstPrintSheet();
	if ( ! pPrintSheet)
		return TRUE;
	CPrintingProfile& rProfile = pPrintSheet->GetPrintingProfile();

	CPrintComponent& rThisComponent	= m_printComponents[0];
	CPrintSheet* pThisPrintSheet = rThisComponent.GetFirstPrintSheet();
	if ( ! pThisPrintSheet)
		return TRUE;
	CPrintingProfile& rThisProfile = pThisPrintSheet->GetPrintingProfile();

	if (rThisProfile == rProfile)
		return TRUE;
	else
		return FALSE;
}

BOOL CPrintComponentsGroup::CheckInsideQuantityRange(CPrintComponentsGroup& rPrintComponentsGroup, CProductionSetup& rProductionSetup)
{
	if (rProductionSetup.m_quantityRanges.GetSize() <= 0)
	{

		m_nMinQuantity = min(m_nMinQuantity, rPrintComponentsGroup.m_nMinQuantity);
		m_nMaxQuantity = max(m_nMaxQuantity, rPrintComponentsGroup.m_nMaxQuantity);
		return TRUE;
	}

	int nValue	  = rPrintComponentsGroup.m_nMinQuantity; // m_nMinQuantity equals m_nMaxQuantity
	int nRangeLow = 1;
	int nRangeUp  = 1;
	for (int i = 0; i < rProductionSetup.m_quantityRanges.GetSize(); i++)
	{
		if ( (m_nMinQuantity >= rProductionSetup.m_quantityRanges[i].nQuantityMin) && (m_nMaxQuantity <= rProductionSetup.m_quantityRanges[i].nQuantityMax) )
		{
			nRangeLow = rProductionSetup.m_quantityRanges[i].nQuantityMin;
			nRangeUp  = rProductionSetup.m_quantityRanges[i].nQuantityMax;
		}
	}

	if ( (nRangeLow <= nValue) && (nValue <= nRangeUp) )
	{
		m_nMinQuantity = min(m_nMinQuantity, nValue);
		m_nMaxQuantity = max(m_nMaxQuantity, nValue);
		return TRUE;
	}
	else
		return FALSE;
}

BOOL CPrintComponentsGroup::CheckInsideColorRange(CPrintComponentsGroup& rPrintComponentsGroup, CProductionSetup& rProductionSetup)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	if (rPrintComponentsGroup.m_printComponents.GetSize() <= 0)
		return FALSE;

	CColorantList		colorantList	= m_colorantList;
	CPrintComponent&	rComponent		= rPrintComponentsGroup.m_printComponents[0];
	if (rComponent.m_nType == CPrintComponent::Flat)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(rComponent.m_nFlatProductIndex);
		colorantList.MixColors(rFlatProduct.m_colorantList);
	}
	else
	{
		CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetPartTotal(rComponent.m_nProductPartIndex);
		colorantList.MixColors(rProductPart.m_colorantList);
	}

	CProductionProfile& rProfile = theApp.m_productionProfiles.FindProfile(rProductionSetup.m_strProductionProfile);
	if ( ! rProfile.IsNull())
	{
		if (colorantList.GetSize() > rProfile.m_press.m_nNumColorStationsFront)
			return FALSE;
		else
			return TRUE;
	}
	else
		return TRUE;
}

void CPrintComponentsGroup::AddFoldSheetComponent(CBoundProductPart& rProductPart, CFoldSheet* pFoldSheet, int nFoldSheetLayerIndex)
{
	CFoldSheetLayer* pLayer = (pFoldSheet) ? pFoldSheet->GetLayer(nFoldSheetLayerIndex) : NULL;
	if ( ! pLayer)
		return;

	int nProductPartIndex = rProductPart.GetIndex();
	int nFoldSheetIndex	  = pFoldSheet->GetFoldSheetIndex();

	for (int i = 0; i < m_printComponents.GetSize(); i++)
	{
		if (m_printComponents[i].m_nProductPartIndex == nProductPartIndex)
			if (m_printComponents[i].m_nFoldSheetIndex == nFoldSheetIndex)
				if (m_printComponents[i].m_nFoldSheetLayerIndex == nFoldSheetLayerIndex)
					return;
	}

	CPrintComponent component;
	component.m_nType					= CPrintComponent::FoldSheet;
	component.m_nProductPartIndex		= nProductPartIndex;
	component.m_nNumPages				= pLayer->GetRealNumPages();
	component.m_nFoldSheetIndex			= nFoldSheetIndex;
	component.m_nFoldSheetLayerIndex	= nFoldSheetLayerIndex;

	int nQuantityPrinted = 0;
	CPrintSheet* pPrintSheet = pFoldSheet->GetPrintSheet(nFoldSheetLayerIndex);
	while (pPrintSheet)
	{
		int nNumUp	  = pFoldSheet->GetNUpPrintLayout(pPrintSheet, nFoldSheetLayerIndex);
		int nQuantity = (pPrintSheet->m_nQuantityTotal - pPrintSheet->m_nQuantityExtra) * nNumUp;

		PrintComponentSheetRef sheetRef = {pPrintSheet->GetIndex(), nQuantity};
		component.m_sheetRefs.Add(sheetRef);

		nQuantityPrinted += nQuantity;

		pPrintSheet = pFoldSheet->GetNextPrintSheet(pPrintSheet, nFoldSheetLayerIndex);
	}

	component.m_nQuantityDesired = pFoldSheet->GetQuantityPlanned();
	component.m_nQuantityPrinted = nQuantityPrinted;
	
	if (component.m_sheetRefs.GetSize() <= 0)
	{
		CPrintSheet* pPrintSheet = component.GetSuitableEmptyPrintSheet();
		PrintComponentSheetRef sheetRef = {(pPrintSheet) ? pPrintSheet->GetIndex() : -1, -1};
		component.m_sheetRefs.Add(sheetRef);
	}

	m_printComponents.Add(component);
}

void CPrintComponentsGroup::AddPageSectionComponent(CBoundProductPart& rProductPart, int nNumPages)
{
	int nProductPartIndex = rProductPart.GetIndex();

	for (int i = 0; i < m_printComponents.GetSize(); i++)
	{
		if (m_printComponents[i].m_nProductPartIndex == nProductPartIndex)
		{
			m_printComponents[i].m_nNumPages += nNumPages;
			break;
		}
	}

	CPrintComponent component;
	component.m_nType				= CPrintComponent::PageSection;
	component.m_nProductPartIndex	= nProductPartIndex;
	component.m_nNumPages			= nNumPages;

	component.m_nQuantityDesired = rProductPart.GetDesiredQuantity();
	component.m_nQuantityPrinted = 0;

	CPrintSheet* pPrintSheet = component.GetSuitableEmptyPrintSheet();
	PrintComponentSheetRef sheetRef = {(pPrintSheet) ? pPrintSheet->GetIndex() : -1, -1};//component.m_nQuantityDesired};
	//PrintComponentSheetRef sheetRef = {-1, rProductPart.m_nDesiredQuantity};
	component.m_sheetRefs.Add(sheetRef);

	m_printComponents.Add(component);
}

void CPrintComponentsGroup::AddFlatProductComponent(CFlatProduct& rFlatProduct)
{
	int nFlatProductIndex = rFlatProduct.GetIndex();

	for (int i = 0; i < m_printComponents.GetSize(); i++)
	{
		if (m_printComponents[i].m_nFlatProductIndex == nFlatProductIndex)
			return;
	}

	CPrintComponent component;
	component.m_nType				= CPrintComponent::Flat;
	component.m_nFlatProductIndex	= nFlatProductIndex;
	component.m_nNumPages			= 1;

	int nQuantityPrinted = 0;
	CPrintSheet* pPrintSheet = rFlatProduct.GetFirstPrintSheet();
	while (pPrintSheet)
	{
		int nNumUp	  = rFlatProduct.GetNUpPrintLayout(pPrintSheet);
		int nQuantity = (pPrintSheet->m_nQuantityTotal - pPrintSheet->m_nQuantityExtra) * nNumUp;

		PrintComponentSheetRef sheetRef = {pPrintSheet->GetIndex(), nQuantity};
		component.m_sheetRefs.Add(sheetRef);
		
		nQuantityPrinted += nQuantity;

		pPrintSheet = rFlatProduct.GetNextPrintSheet(pPrintSheet);
	}

	component.m_nQuantityDesired = rFlatProduct.m_nDesiredQuantity;
	component.m_nQuantityPrinted = nQuantityPrinted;
	
	if (component.m_sheetRefs.GetSize() <= 0)
	{
		CPrintSheet* pPrintSheet = component.GetSuitableEmptyPrintSheet();
		PrintComponentSheetRef sheetRef = {(pPrintSheet) ? pPrintSheet->GetIndex() : -1, -1};
		component.m_sheetRefs.Add(sheetRef);
	}

	m_printComponents.Add(component);
}

BOOL CPrintComponentsGroup::HasPrinted()
{
	for (int i = 0; i < m_printComponents.GetSize(); i++)
	{
		for (int j = 0; j < m_printComponents[i].m_sheetRefs.GetSize(); j++)
		{
			if (m_printComponents[i].m_sheetRefs[j].m_nQuantity >= 0)
				return TRUE;
		}
	}
	return FALSE;
}

BOOL CPrintComponentsGroup::HasUnprinted()
{
	for (int i = 0; i < m_printComponents.GetSize(); i++)
	{
		for (int j = 0; j < m_printComponents[i].m_sheetRefs.GetSize(); j++)
		{
			if (m_printComponents[i].m_sheetRefs[j].m_nQuantity < 0)
				return TRUE;
		}
	}
	return FALSE;
}

CRect CPrintComponentsGroup::DrawHeader(CDC* pDC, CRect rcFrame, BOOL bEnabled, BOOL bLongVersion, BOOL bShowBackground, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	if (bShowBackground)
	{
		CRect rcHeader = (bLongVersion) ? GetDisplayBoxLong() + rcFrame.TopLeft() : GetDisplayBox() + rcFrame.TopLeft(); rcHeader.right = rcFrame.right;
		if (bLongVersion)
			pDC->FillSolidRect(rcHeader, (bEnabled) ? RGB(240,233,227) : RGB(235,235,235));
		else
			pDC->FillSolidRect(rcHeader, (bEnabled) ? RGB(244,240,236) : RGB(250,250,250));

		CPen pen(PS_SOLID, 1, RGB(220,220,220));
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		int nIndex = GetIndex();
		if (nIndex == (pDoc->m_printComponentsList.GetSize() - 1) )
		{
			pDC->MoveTo(rcHeader.left,  rcHeader.bottom);
			pDC->LineTo(rcHeader.right, rcHeader.bottom);
		}
		if (nIndex > 0)
		{
			pDC->MoveTo(rcHeader.left,  rcHeader.top);
			pDC->LineTo(rcHeader.right, rcHeader.top);
		}

		pen.DeleteObject();
	}

	CFont font, boldFont;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SetBkMode(TRANSPARENT);

	CString strOut, strOut2;

	pDC->SelectObject(&boldFont);
	pDC->SetTextColor(BLACK);

	strOut2.LoadString(IDS_PRODUCTGROUP); 

	int   nIndent = 37;
	int   nTab	  = 62;
	int	  nLeft	  = rcFrame.left + nIndent;
	CRect rcText  = rcFrame; rcText.left += 5; rcText.top += 1; rcText.top += 5; rcText.bottom = rcText.top + nTextHeight; 
	CRect rcText2 = rcText; rcText2.left += nIndent + nTab;

	if (bLongVersion) 
	{
		strOut.Format(_T("%s  %d"), strOut2, GetIndex() + 1);
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
		rcText.OffsetRect(0, nTextHeight + 6); rcText2.OffsetRect(0, nTextHeight + 6);
		rcText.left = nLeft;
	}
	else
	{
		strOut.Format(_T("%d"), GetIndex() + 1);
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
		rcText.left = nLeft;
	}

	pDC->SetTextColor(DARKGRAY);
	pDC->SelectObject(&font);
	if (m_colorantList.GetSize() > 0)
	{
		if (bLongVersion)
		{
			strOut2.LoadString(IDS_PLATECOLOR_STRING); 
			strOut.Format(_T("%s (%d):"), strOut2, m_colorantList.GetSize());
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
			rcBox.UnionRect(rcBox, rcText);
		}
		CRect rcColors = (bLongVersion) ? rcText2 : rcText; 
		rcColors = DrawColorInfo(pDC, rcColors, pDisplayList);
		rcBox.UnionRect(rcBox, rcColors);
		if (bLongVersion)
		{
			rcText.OffsetRect(0, nTextHeight + 2); rcText2.OffsetRect(0, nTextHeight + 2);
		}
		else
			nLeft = rcColors.right + 10;
	}

	if ( ! m_strPaper.IsEmpty())
	{
		if (bLongVersion)
		{
			CString strPaper;
			strPaper.LoadString(IDS_PAPER);	strPaper += _T(":");
			pDC->SetTextColor(DARKGRAY);
			DrawTextMeasured(pDC, strPaper, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
			pDC->SetTextColor(BLACK);
			DrawTextMeasured(pDC, m_strPaper, rcText2, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
			rcText.OffsetRect(0, nTextHeight + 2); rcText2.OffsetRect(0, nTextHeight + 2);
		}
		else
		{		
			rcText.left = nLeft;
			pDC->SetTextColor(BLACK);
			DrawTextMeasured(pDC, m_strPaper, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
			nLeft = rcBox.right + 10;
		}
	}

	if ( ! m_strPressSetup.IsEmpty())
	{
		pDC->SetTextColor(BLACK);
		rcText.left = nLeft;
		strOut = m_strPressSetup;
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
		rcText.OffsetRect(0, nTextHeight);
	}

	font.DeleteObject();

	rcBox.bottom += 5;

	if (bLongVersion)
		SetDisplayBoxLong(CRect(CPoint(0,0), rcBox.Size()));
	else
		SetDisplayBox(CRect(CPoint(0,0), rcBox.Size()));

	return rcBox;
}

CRect CPrintComponentsGroup::DrawColorInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;
	if (m_colorantList.GetSize() <= 0)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(RGB(40,120,30));

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;

	CString strOut;
	pDC->SetTextColor(DARKGRAY);
	//strOut.Format(_T(" |  %d-farbig"), m_colorantList.GetSize()); 
	//pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	//rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx;
	//rcBox.UnionRect(rcBox, rcText);

	CRect rcIcon;
	rcIcon.left = rcText.left/* + 5*/; rcIcon.top = rcFrame.top + 4; rcIcon.right = rcIcon.left + 6; rcIcon.bottom = rcFrame.top + 9;
	CPen pen(PS_SOLID, 1, MIDDLEGRAY);
	CBrush brush;
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CBrush* pOldBrush = pDC->GetCurrentBrush(); 
	for (int i = 0; i < m_colorantList.GetSize(); i++)
	{
		COLORREF crRGB = m_colorantList[i].m_crRGB;
		if (crRGB == WHITE)
			crRGB = pDoc->m_ColorDefinitionTable.FindColorRef(m_colorantList[i].m_strName);
		brush.CreateSolidBrush(crRGB);
		pDC->SelectObject(&brush);
		pDC->Rectangle(rcIcon);
		brush.DeleteObject();

		rcBox.UnionRect(rcBox, rcIcon);
		rcIcon.OffsetRect(7, 0);
	}
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();

	font.DeleteObject();

	rcBox.bottom += 5;

	return rcBox;
}

int	CPrintComponentsGroup::GetNumFoldSheets(CFoldSheet* pRefFoldSheet)
{
	int nNum = 0;
	for (int i = 0; i < m_printComponents.GetSize(); i++)
	{
		CPrintComponent component = m_printComponents[i];
		CFoldSheet* pFoldSheet = component.GetFoldSheet();
		if ( ! pFoldSheet)
			continue;
		if (pRefFoldSheet)
		{
			if (pFoldSheet->m_nProductPartIndex == pRefFoldSheet->m_nProductPartIndex)
				if (pFoldSheet->m_strFoldSheetTypeName == pRefFoldSheet->m_strFoldSheetTypeName)
					nNum++;
		}
		else
			nNum++;
	}

	return nNum;
}


/////////////////////////////////////////////////////////////////////////////
// CPrintComponent

IMPLEMENT_SERIAL(CPrintComponent, CPrintComponent, VERSIONABLE_SCHEMA | 1)

CPrintComponent::CPrintComponent()
{
	m_nType = -1;
	m_nProductPartIndex		= -1;
	//m_nFlatProductIndex	= -1;
	m_nNumPages				= 0;
	m_nFoldSheetIndex		= -1;
	m_nFoldSheetLayerIndex	= -1;
	m_nQuantityDesired		= 0;
	m_nQuantityPrinted		= 0;
}

CPrintComponent::CPrintComponent(const CPrintComponent& rPrintComponent)
{
	Copy(rPrintComponent);
}

const CPrintComponent& CPrintComponent::operator=(const CPrintComponent& rPrintComponent)
{
	Copy(rPrintComponent);

	return *this;
}

void CPrintComponent::Copy(const CPrintComponent& rPrintComponent)
{
	m_nIndex				= rPrintComponent.m_nIndex;
	m_nType					= rPrintComponent.m_nType;
	m_nProductPartIndex		= rPrintComponent.m_nProductPartIndex;
	m_nFlatProductIndex		= rPrintComponent.m_nFlatProductIndex;
	m_nNumPages				= rPrintComponent.m_nNumPages;
	m_nFoldSheetIndex		= rPrintComponent.m_nFoldSheetIndex;
	m_nFoldSheetLayerIndex	= rPrintComponent.m_nFoldSheetLayerIndex;
	m_nQuantityDesired		= rPrintComponent.m_nQuantityDesired;
	m_nQuantityPrinted		= rPrintComponent.m_nQuantityPrinted;

	m_sheetRefs.RemoveAll();
	m_sheetRefs.Append(rPrintComponent.m_sheetRefs);
}

BOOL CPrintComponent::operator==(const CPrintComponent& rPrintComponent)
{
	if (m_nType					!= rPrintComponent.m_nType)
		return FALSE;
	if (m_nProductPartIndex		!= rPrintComponent.m_nProductPartIndex)
		return FALSE;
	if (m_nFlatProductIndex		!= rPrintComponent.m_nFlatProductIndex)
		return FALSE;
	if (m_nNumPages				!= rPrintComponent.m_nNumPages)
		return FALSE;
	if (m_nFoldSheetIndex		!= rPrintComponent.m_nFoldSheetIndex)
		return FALSE;
	if (m_nFoldSheetLayerIndex	!= rPrintComponent.m_nFoldSheetLayerIndex)
		return FALSE;
	if (m_nQuantityDesired		!= rPrintComponent.m_nQuantityDesired)
		return FALSE;
	if (m_nQuantityPrinted		!= rPrintComponent.m_nQuantityPrinted)
		return FALSE;

	if (m_sheetRefs.GetSize()	!= rPrintComponent.m_sheetRefs.GetSize())
		return FALSE;

	for (int i = 0; i < m_sheetRefs.GetSize(); i++)
	{
		if (m_sheetRefs[i].m_nPrintSheetIndex != rPrintComponent.m_sheetRefs[i].m_nPrintSheetIndex)
			return FALSE;
		if (m_sheetRefs[i].m_nQuantity		  != rPrintComponent.m_sheetRefs[i].m_nQuantity)
			return FALSE;
	}

	return TRUE;
}

BOOL CPrintComponent::operator!=(const CPrintComponent& rPrintComponent)
{
	return !(*this == rPrintComponent);
}

BOOL CPrintComponent::IsEquivalent(const CPrintComponent& rPrintComponent)
{
	if (m_nType					!= rPrintComponent.m_nType)
		return FALSE;
	if (m_nProductPartIndex		!= rPrintComponent.m_nProductPartIndex)
		return FALSE;
	if (m_nFlatProductIndex		!= rPrintComponent.m_nFlatProductIndex)
		return FALSE;
	if (m_nNumPages				!= rPrintComponent.m_nNumPages)
		return FALSE;
	if (m_nFoldSheetIndex		!= rPrintComponent.m_nFoldSheetIndex)
		return FALSE;
	if (m_nFoldSheetLayerIndex	!= rPrintComponent.m_nFoldSheetLayerIndex)
		return FALSE;
	if (m_nQuantityDesired		!= rPrintComponent.m_nQuantityDesired)
		return FALSE;

	return TRUE;
}

// helper function for CPrintComponent elements
template <> void AFXAPI SerializeElements <CPrintComponent> (CArchive& ar, CPrintComponent* pPrintComponent, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrintComponent));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPrintComponent->m_nType;
			ar << pPrintComponent->m_nProductPartIndex;
			//ar << pPrintComponent->m_nFlatProductIndex;
			ar << pPrintComponent->m_nNumPages;
			ar << pPrintComponent->m_nFoldSheetIndex;
			ar << pPrintComponent->m_nFoldSheetLayerIndex;
			ar << pPrintComponent->m_nQuantityDesired;
			ar << pPrintComponent->m_nQuantityPrinted;
			pPrintComponent->m_sheetRefs.Serialize(ar);
		}	
		else
		{ 
			switch (nVersion)
			{
				case 1:	ar >> pPrintComponent->m_nType;
						ar >> pPrintComponent->m_nProductPartIndex;
						//ar >> pPrintComponent->m_nFlatProductIndex;
						ar >> pPrintComponent->m_nNumPages;
						ar >> pPrintComponent->m_nFoldSheetIndex;
						ar >> pPrintComponent->m_nFoldSheetLayerIndex;
						ar >> pPrintComponent->m_nQuantityDesired;
						ar >> pPrintComponent->m_nQuantityPrinted;
						pPrintComponent->m_sheetRefs.Serialize(ar);
						break;
			}
		}

		pPrintComponent++;
	}
}

BOOL CPrintComponent::IsPlaced()
{
	if (m_sheetRefs.GetSize() <= 0)
		return FALSE;
	else
		if (m_sheetRefs[0].m_nPrintSheetIndex < 0)
			return FALSE;
		else
			if (m_sheetRefs[0].m_nQuantity < 0)
				return FALSE;

	return TRUE;
}

//BOOL CPrintComponent::GetIndex()
//{
//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
//	if ( ! pDoc)
//		return -1;
//
//	int nIndex = 0;
//	for (int i = 0; i < pDoc->m_printComponentsList.GetSize(); i++)
//	{
//		CPrintComponentsGroup& rGroup = pDoc->m_printComponentsList[i];
//		for (int j = 0; j < rGroup.m_printComponents.GetSize(); j++)
//		{
//			if (&rGroup.m_printComponents[j] == this)
//				return nIndex;
//
//			nIndex++;
//		}
//	}
//	return -1;
//}

CPrintComponentsGroup& CPrintComponent::GetPrintComponentsGroup()
{
	static CPrintComponentsGroup nullGroup;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	return (pDoc) ? pDoc->m_printComponentsList.FindGroup(*this) : nullGroup;
}

CPrintingProfile& CPrintComponent::GetFirstPrintingProfile()
{
	static CPrintingProfile nullPrintingProfile;

	CFlatProduct* pFlatProduct = GetFlatProduct();
	if (pFlatProduct)
	{
		CPrintGroup& rPrintGroup = pFlatProduct->GetFirstPrintGroup();
		if ( ! rPrintGroup.IsNull()) 
			return rPrintGroup.GetPrintingProfile();
	}
	else
	{
		CFoldSheet* pFoldSheet = GetFoldSheet();
		if (pFoldSheet)
		{
			CPrintGroup& rPrintGroup = pFoldSheet->GetFirstPrintGroup();
			if ( ! rPrintGroup.IsNull()) 
				return rPrintGroup.GetPrintingProfile();
		}
		else	// page section
		{
			CBoundProductPart* pProductPart = GetBoundProductPart();
			if (pProductPart)
			{
				CPrintGroup& rPrintGroup = pProductPart->GetLastPrintGroup();	// when we have unassigned page section, we always find the corresponding print group behind all other print groups of this product part -> search for last one
				if ( ! rPrintGroup.IsNull()) 
					return rPrintGroup.GetPrintingProfile();
			}
		}
	}
	return nullPrintingProfile;
}

BOOL CPrintComponent::IsSuitablePrintGroup(CPrintGroup& rPrintGroup)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CPrintComponentsGroup& rPrintComponentsGroup = GetPrintComponentsGroup();
	if (rPrintComponentsGroup.IsNull())
		return FALSE;

	if (rPrintGroup.m_strPaper == rPrintComponentsGroup.m_strPaper)
	{
		CPrintingProfile& rProfile1 = rPrintGroup.GetPrintingProfile();
		CPrintingProfile& rProfile2 = GetFirstPrintingProfile();

		if (rProfile1.m_press.m_nNumColorStationsFront	!= rProfile2.m_press.m_nNumColorStationsFront)			return FALSE;
		if (rProfile1.m_press.m_nNumColorStationsBack	!= rProfile2.m_press.m_nNumColorStationsBack)			return FALSE;
		if (rProfile1.m_press.m_pressDevice				!= rProfile2.m_press.m_pressDevice)						return FALSE;
		if (rProfile1.m_press.m_backSidePressDevice		!= rProfile2.m_press.m_backSidePressDevice)				return FALSE;

		return TRUE;
	}

	return FALSE;
}

CPrintSheet* CPrintComponent::GetSuitablePrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	CPrintComponentsGroup& rPrintComponentsGroup = GetPrintComponentsGroup();
	if (rPrintComponentsGroup.IsNull())
		return NULL;

	for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
	{
		CPrintGroup& rPrintGroup = pDoc->m_printGroupList[i];
		if (IsSuitablePrintGroup(rPrintGroup))
		{
			CPrintSheet* pPrintSheet = rPrintGroup.GetFirstPrintSheet();
			if (pPrintSheet)
			{
				if (m_nType == CPrintComponent::PageSection)
				{
					if (pPrintSheet->IsEmpty())
						return pPrintSheet;
				}
				else
				{
					CLayout* pLayout = pPrintSheet->GetLayout();
					if (pLayout)
					{
						float fWidth, fHeight;
						GetCutSize(fWidth, fHeight);
						if ( ! pLayout->m_FrontSide.FindLargestFreeArea(pPrintSheet, fWidth, fHeight).IsRectNull())
							return pPrintSheet;
						else
							if ( ! pLayout->m_FrontSide.FindLargestFreeArea(pPrintSheet, fHeight, fWidth).IsRectNull())
								return pPrintSheet;
					}
				}
			}
		}
	}
	return NULL;
}

CPrintSheet* CPrintComponent::GetSuitableEmptyPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	CPrintComponentsGroup& rPrintComponentsGroup = GetPrintComponentsGroup();
	if (rPrintComponentsGroup.IsNull())
		return NULL;

	for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
	{
		CPrintGroup& rPrintGroup = pDoc->m_printGroupList[i];
		if ( ! IsSuitablePrintGroup(rPrintGroup))
			continue;
		CPrintSheet* pPrintSheet = rPrintGroup.GetFirstPrintSheet();
		if ( ! pPrintSheet)
			continue;
		if (pPrintSheet->IsEmpty())
			return pPrintSheet;
	}

	return NULL;
}

CFoldSheet* CPrintComponent::GetFoldSheet()
{
	if (m_nType != FoldSheet)
		return NULL;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	return pDoc->m_Bookblock.GetFoldSheetByIndex(m_nFoldSheetIndex);
}

CFoldSheetLayer* CPrintComponent::GetFoldSheetLayer()
{
	if (m_nType != FoldSheet)
		return NULL;

	CFoldSheet* pFoldSheet = GetFoldSheet();
	return (pFoldSheet) ? pFoldSheet->GetLayer(m_nFoldSheetLayerIndex) : NULL;
}

CFlatProduct* CPrintComponent::GetFlatProduct()
{
	if (m_nType != Flat)
		return NULL;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	return &pDoc->m_flatProducts.FindByIndex(m_nFlatProductIndex);
}

CBoundProduct* CPrintComponent::GetBoundProduct()
{
	if (m_nType == Flat)
		return NULL;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	CBoundProductPart* pProductPart = GetBoundProductPart();

	if (pProductPart)
		return &pProductPart->GetBoundProduct();
	else
		return NULL;
}

CBoundProductPart* CPrintComponent::GetBoundProductPart()
{
	if (m_nType == Flat)
		return NULL;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	return &pDoc->m_boundProducts.GetProductPart(m_nProductPartIndex);
}

CPrintSheet* CPrintComponent::GetFirstPrintSheet()
{
	if (m_sheetRefs.GetSize() <= 0)
		return NULL;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	POSITION pos = pDoc->m_PrintSheetList.FindIndex(m_sheetRefs[0].m_nPrintSheetIndex);
	return (pos) ? &pDoc->m_PrintSheetList.GetAt(pos) : NULL;
}

CPrintSheet* CPrintComponent::GetNextPrintSheet(CPrintSheet* pPrintSheet)
{
	if (m_sheetRefs.GetSize() <= 0)
		return NULL;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	for (int i = 0; i < m_sheetRefs.GetSize(); i++)
	{
		POSITION	 pos			= pDoc->m_PrintSheetList.FindIndex(m_sheetRefs[i].m_nPrintSheetIndex);
		CPrintSheet* pCurPrintSheet = (pos) ? &pDoc->m_PrintSheetList.GetAt(pos) : NULL;
		if (pCurPrintSheet == pPrintSheet)
		{
			i++;
			if (i < m_sheetRefs.GetSize())
			{
				pos	= pDoc->m_PrintSheetList.FindIndex(m_sheetRefs[i].m_nPrintSheetIndex);
				return (pos) ? &pDoc->m_PrintSheetList.GetAt(pos) : NULL;
			}
		}
	}

	return NULL;
}

CPrintSheet* CPrintComponent::GetLastPrintSheet()
{
	if (m_sheetRefs.GetSize() <= 0)
		return NULL;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int nLastIndex = m_sheetRefs.GetSize() - 1;
	POSITION pos = pDoc->m_PrintSheetList.FindIndex(m_sheetRefs[nLastIndex].m_nPrintSheetIndex);
	return (pos) ? &pDoc->m_PrintSheetList.GetAt(pos) : NULL;
}

void CPrintComponent::GetCutSize(float& fWidth, float& fHeight)
{
	fWidth = 0.0f; fHeight = 0.0f;
	CFoldSheet* pFoldSheet = GetFoldSheet();
	if (pFoldSheet)
		pFoldSheet->GetCutSize(m_nFoldSheetLayerIndex, fWidth, fHeight);
	else
	{
		CFlatProduct* pFlatProduct = GetFlatProduct();
		if (pFlatProduct)
			pFlatProduct->GetCutSize(fWidth, fHeight);
	}
}

CRect CPrintComponent::DrawInfo(CDC* pDC, CRect rcFrame, int nSheetRefIndex, BOOL bDrawThumbnail, BOOL bDrawProductHeader, BOOL bShowUnprinted, BOOL bShowPrintedOnly, CDisplayList* pDisplayList)
{
	CRect rcBox = CRect(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;
	int nRowHeight  = nTextHeight;

	CFont font, smallFont, boldFont;
	font.CreateFont	    ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 11, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetBkColor(WHITE);

	CString strOut;
	int nIndent = 15;
	CRect rcText = rcFrame; rcText.left += 40 + nIndent;
	if ( (m_nType != CPrintComponent::Flat) && bDrawProductHeader)
	{
		CBoundProductPart& rProductPart  = pDoc->m_boundProducts.GetProductPart(m_nProductPartIndex);
		CBoundProduct&	   rBoundProduct = rProductPart.GetBoundProduct();

		BOOL bLite = (rProductPart.GetNumUnassignedPages() > 0) ? TRUE : FALSE;

		if (bShowUnprinted)
		{
			if (IsPlaced())
			{
				font.DeleteObject();
				smallFont.DeleteObject();
				boldFont.DeleteObject();
				return rcBox;
			}
		}
		if (bShowPrintedOnly)
		{
			if (rProductPart.GetNumPlacedPages() <= 0)
			{
				font.DeleteObject();
				smallFont.DeleteObject();
				boldFont.DeleteObject();
				return rcBox;
			}
		}

		CRect rcImg = rcFrame; //rcImg.left += 3; rcImg.top += 3;
		CImage img;
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE((bLite) ? IDB_SIGNATURE_DISABLED : IDB_SIGNATURE));
		if ( ! img.IsNull())
		{
			rcImg.right = rcImg.left + img.GetWidth(); rcImg.bottom = rcImg.top + img.GetHeight();
			img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, rcImg.Width(), rcImg.Height(), WHITE);
			rcBox.UnionRect(rcBox, rcImg);
		}

		pDC->SelectObject(&smallFont);
		pDC->SetTextColor((bLite) ? DARKGRAY : GUICOLOR_CAPTION);
		strOut.Format(_T(" %d "), rProductPart.m_nNumPages);
		CRect rcPages = rcImg; rcPages.top = rcImg.CenterPoint().y - 6; rcPages.bottom = rcImg.CenterPoint().y + 6; 
		rcPages.left = 35; rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
		rcPages.InflateRect(1, 1);
		CBrush brush(WHITE);
		CPen   pen(PS_SOLID, 1, (bLite) ? LIGHTGRAY : GUICOLOR_CAPTION);
		CBrush* pOldBrush = pDC->SelectObject(&brush);
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		pDC->RoundRect(rcPages, CPoint(8, 8));
		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		pDC->SelectObject(&boldFont);
		pDC->SetBkMode(TRANSPARENT);

		if (pDoc->m_boundProducts.GetSize() > 1)
		{
			pDC->SetTextColor((bLite) ? DARKGRAY : GUICOLOR_CAPTION);
			strOut.Format(_T("%s  "), rBoundProduct.m_strProductName);
			DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
			rcText.left += pDC->GetTextExtent(strOut).cx + 5;
		}

		pDC->SetTextColor((bLite) ? DARKGRAY : RGB(40,120,30));	// green
		strOut.Format(_T("%s "), rProductPart.m_strName);
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);

		rcFrame.top	   = rcBox.bottom;
		rcFrame.top += (bDrawProductHeader) ? 10 : 0; 
	}

	if (bShowUnprinted)
	{
		if (IsPlaced())
		{
			font.DeleteObject();
			smallFont.DeleteObject();
			boldFont.DeleteObject();
			return rcBox;
		}
	}
	if (bShowPrintedOnly)
	{
		if ( ! IsPlaced())
		{
			font.DeleteObject();
			smallFont.DeleteObject();
			boldFont.DeleteObject();
			return rcBox;
		}
	}

	COLORREF crHighlight = BROWN;

	CRect rcClient = (pDisplayList) ? pDisplayList->m_clientRect : CRect(0,0,0,0);

	CDisplayItem* pDI = NULL;
	if (m_nType == CPrintComponent::PageSection)
	{
		CBoundProductPart& rProductPart  = pDoc->m_boundProducts.GetProductPart(m_nProductPartIndex);
		CBoundProduct&	   rBoundProduct = rProductPart.GetBoundProduct();

		if (pDisplayList)
		{
			CRect rcDI	  = rcFrame; rcDI.bottom = rcDI.top + nTextHeight; rcDI.left = 0; rcDI.right = rcClient.right; rcDI.top -= 4; rcDI.bottom += 4;
			CRect rcDIAux = rcDI; rcDIAux.left = rcFrame.right; rcDIAux.right = rcDIAux.left + 58; 
			CPrintComponentsView* pView = (CPrintComponentsView*)pDisplayList->m_pParent;
			if (pView)
			{
				rcDI.OffsetRect(0, -pView->GetScrollPos(SB_VERT)); rcDIAux.OffsetRect(0, -pView->GetScrollPos(SB_VERT));
			}
			pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDIAux, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CPrintComponentsView, PageSection), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
				{
					CRect rcFeedback = pDI->m_BoundingRect;
					rcFeedback.OffsetRect(0, pView->GetScrollPos(SB_VERT));
					pDC->FillSolidRect(rcFeedback, RGB(230,218,205));
				}
			}
		}

		CRect rcImg = rcFrame; rcImg.top -= 2; rcImg.bottom = rcImg.top + nRowHeight; rcImg.left += nIndent; rcImg.top += 3;
		rcImg = pDoc->m_PageTemplateList.DrawSinglePage(pDC, rcImg, 0, rProductPart.GetIndex(), FALSE, FALSE, TRUE, TRUE, TRUE);
		rcBox.UnionRect(rcBox, rcImg);
		if (rcImg.top == rcImg.bottom)
		{
			rcImg.top = rcFrame.top; rcImg.bottom = rcFrame.bottom;
		}

		pDC->SelectObject(&smallFont);
		pDC->SetTextColor(MIDDLEGRAY);
		strOut.Format(_T(" %d "), m_nNumPages);
		CRect rcPages = rcImg; rcPages.top = rcImg.CenterPoint().y - 6; rcPages.bottom = rcImg.CenterPoint().y + 6; 
		rcPages.left = 35; rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
		rcPages.InflateRect(1, 1);
		CBrush brush(WHITE);
		CPen   pen(PS_SOLID, 1, LIGHTGRAY);
		CBrush* pOldBrush = pDC->SelectObject(&brush);
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		pDC->RoundRect(rcPages, CPoint(8, 8));
		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		pDC->SelectObject(&font);
		pDC->SetBkMode(TRANSPARENT);

		rcBox.bottom = rcFrame.top + nTextHeight;
	}
	else
	if (m_nType == CPrintComponent::FoldSheet)
	{
		CBoundProductPart& rProductPart  = pDoc->m_boundProducts.GetProductPart(m_nProductPartIndex);
		CBoundProduct&	   rBoundProduct = rProductPart.GetBoundProduct();

		if (pDisplayList && bDrawThumbnail)
		{
			CRect rcDI	  = rcFrame; rcDI.bottom = rcDI.top + nTextHeight; rcDI.left = 0; rcDI.right = rcClient.right; rcDI.top -= 4; rcDI.bottom += 4;
			CRect rcDIAux = rcDI; rcDIAux.left = rcFrame.right; rcDIAux.right = rcDIAux.left + 58; 
			CPrintComponentsView* pView = (CPrintComponentsView*)pDisplayList->m_pParent;
			if (pView)
			{
				rcDI.OffsetRect(0, -pView->GetScrollPos(SB_VERT)); rcDIAux.OffsetRect(0, -pView->GetScrollPos(SB_VERT));
			}
			pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDIAux, (void*)GetIndex(), SNAP_DISPLAY_ITEM_REGISTRY(CPrintComponentsView, FoldSheet), (void*)NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
				{
					CRect rcFeedback = pDI->m_BoundingRect;
					rcFeedback.OffsetRect(0, pView->GetScrollPos(SB_VERT));
					pDC->FillSolidRect(rcFeedback, RGB(230,218,205));
				}
			}
		}

		pDC->SelectObject(&font);
	
		COLORREF crValueColor = RGB(50,50,50);
		BOOL			 bFirst					= TRUE;	// sleepy
		int				 nMaxFoldSheetNumWidth	= pDoc->m_printGroupList.GetLongestFoldSheetNumString(pDC) + pDC->GetTextExtent(_T("  ")).cx;
		CFoldSheet*		 pFoldSheet				= pDoc->m_Bookblock.GetFoldSheetByIndex(m_nFoldSheetIndex);
		CFoldSheetLayer* pFoldSheetLayer		= (pFoldSheet) ? pFoldSheet->GetLayer(m_nFoldSheetLayerIndex) : NULL;
		if ( ! pFoldSheet || ! pFoldSheetLayer)
			return rcBox;

		BOOL bLite = (pFoldSheet->GetPrintSheet(m_nFoldSheetLayerIndex)) ? FALSE : TRUE;

		CRect rcThumbnail = rcFrame; 
		if (bDrawThumbnail)
		{
			rcThumbnail.bottom = rcThumbnail.top + nRowHeight; rcThumbnail.left += nIndent; rcThumbnail.right = rcThumbnail.left + 23;
			rcThumbnail = pFoldSheet->DrawThumbnail(pDC, rcThumbnail, (bLite) ? FALSE : TRUE, FALSE, m_nFoldSheetLayerIndex);
			rcBox.UnionRect(rcBox, rcThumbnail);

			pDC->SelectObject(&smallFont);
			pDC->SetTextColor((bLite) ? MIDDLEGRAY : LIGHTBLUE);
			strOut.Format(_T(" %d "), pFoldSheetLayer->GetRealNumPages());
			CRect rcPages = rcThumbnail; rcPages.top = rcThumbnail.CenterPoint().y - 6; rcPages.bottom = rcThumbnail.CenterPoint().y + 6; 
			rcPages.left = 35; rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
			rcPages.InflateRect(1, 1);
			CBrush brush(WHITE);
			CPen   pen(PS_SOLID, 1, (bLite) ? LIGHTGRAY : LIGHTBLUE);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			pDC->RoundRect(rcPages, CPoint(8, 8));
			pDC->SelectObject(pOldBrush);
			pDC->SelectObject(pOldPen);
			DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
			pDC->SelectObject(&font);
			pDC->SetBkMode(TRANSPARENT);
		}

		pDC->SelectObject(&font);

		CRect rcText = rcFrame; rcText.left += 40 + nIndent; 
		rcText.top = rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;

		if (bDrawThumbnail)
		{
			pDC->SetTextColor((bLite) ? DARKGRAY : RGB(200,100,0));
			rcText.left = rcFrame.left + 40 + nIndent; 
			if (pFoldSheet->m_LayerList.GetCount() > 1)
				strOut.Format(_T("%s (%d):"), pFoldSheet->GetNumber(), m_nFoldSheetLayerIndex + 1); 
			else
				strOut.Format(_T("%s:"), pFoldSheet->GetNumber()); 
			DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

			pDC->SelectObject(&font);

			pDC->SetTextColor((bLite) ? DARKGRAY : GUICOLOR_CAPTION);
			rcText.left += nMaxFoldSheetNumWidth;
			strOut.Format(_T("%s"), pFoldSheet->m_strFoldSheetTypeName);
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
			rcBox.bottom = max(rcBox.bottom, rcText.bottom);
		}

		rcBox.bottom = rcFrame.top + nTextHeight;
	}
	else
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(m_nFlatProductIndex);

		if (pDisplayList && bDrawThumbnail)
		{
			CRect rcDI	  = rcFrame; rcDI.bottom = rcDI.top + nTextHeight; rcDI.left = 0; rcDI.right = rcClient.right; rcDI.top -= 4; rcDI.bottom += 4;
			CRect rcDIAux = rcDI; rcDIAux.left = rcFrame.right; rcDIAux.right = rcDIAux.left + 58; 
			CPrintComponentsView* pView = (CPrintComponentsView*)pDisplayList->m_pParent;
			if (pView)
			{
				rcDI.OffsetRect(0, -pView->GetScrollPos(SB_VERT)); rcDIAux.OffsetRect(0, -pView->GetScrollPos(SB_VERT));
			}
			pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDIAux, (void*)GetIndex(), SNAP_DISPLAY_ITEM_REGISTRY(CPrintComponentsView, FlatProduct), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
				{
					CRect rcFeedback = pDI->m_BoundingRect;
					rcFeedback.OffsetRect(0, pView->GetScrollPos(SB_VERT));
					pDC->FillSolidRect(rcFeedback, RGB(230,218,205));
				}
			}
		}

		BOOL bLite = (rFlatProduct.GetFirstPrintSheet()) ? FALSE : TRUE;
		
		float fMaxHeight = pDoc->m_flatProducts.GetMaxHeight(-1);
		int nHeight = (int)((float)(rcFrame.Height()) * (rFlatProduct.m_fTrimSizeHeight / fMaxHeight));	
		CRect rcImg = rcFrame; 
		if (bDrawThumbnail)
		{
			rcImg.left += 3; rcImg.bottom = rcImg.top + nHeight; rcImg.DeflateRect(0, 2);
			rcImg = pDoc->m_PageTemplateList.DrawFlatProduct(pDC, rcImg, m_nFlatProductIndex * 2, FALSE, FALSE, FALSE, TRUE, TRUE);
			if (bLite)
				CPrintSheetView::FillTransparent(pDC, rcImg, WHITE, TRUE, 160);
			rcBox.UnionRect(rcBox, rcImg);
		}

		CRect rcText = rcFrame; rcText.left = max(rcImg.right + 10, rcText.left + 40 + nIndent); rcText.bottom = rcText.top + nTextHeight;

		pDC->SetTextColor((bLite) ? DARKGRAY : RGB(200,100,0));
		//rcText.left = rcFrame.left + 40 + nIndent; 
		strOut.Format(_T("%d: "), rFlatProduct.GetIndex() + 1);
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		rcText.left += pDC->GetTextExtent(strOut).cx;

		pDC->SetTextColor((bLite) ? DARKGRAY : GUICOLOR_CAPTION);
		DrawTextMeasured(pDC, rFlatProduct.m_strProductName, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

		rcBox.bottom = rcFrame.top + nTextHeight;
	}

	font.DeleteObject();
	smallFont.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CPrintComponent::DrawPrintSheetLoc(CDC* pDC, CRect rcFrame, int nSheetRefIndex, BOOL bDrawThumbnail, BOOL bDrawProductHeader, BOOL bShowUnprinted, BOOL bShowPrintedOnly, CDisplayList* pDisplayList)
{
	CRect rcBox = CRect(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	if (bShowUnprinted)
	{
		if (IsPlaced())
			return rcBox;
	}
	if (bShowPrintedOnly)
	{
		if ( ! IsPlaced())
			return rcBox;
	}

	CPrintSheetNavigationView*	pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	CPrintSheet*				pActPrintSheet	= (pNavigationView) ? pNavigationView->GetActPrintSheet() : NULL;
	CLayout*					pActLayout		= (pNavigationView) ? pNavigationView->GetActLayout()	  : NULL;

	BOOL	bIsActPrintSheet  = FALSE;
	BOOL	bIsActLayout	  = FALSE;
	int		nPrintSheetNumber = -1;
	CString strPrintSheetNumber;
	PrintComponentSheetRef& rSheetRef = m_sheetRefs[nSheetRefIndex];
	if (rSheetRef.m_nQuantity < 0)
		return rcBox;

	POSITION pos = pDoc->m_PrintSheetList.FindIndex(rSheetRef.m_nPrintSheetIndex);
	if (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetAt(pos);
		nPrintSheetNumber	= rPrintSheet.GetNumberOnly();
		strPrintSheetNumber = rPrintSheet.GetNumber();
		if (rPrintSheet.GetLayout() == pActLayout)
			bIsActLayout = TRUE;
		if (&rPrintSheet == pActPrintSheet)
			bIsActPrintSheet = TRUE;
	}

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, (bIsActPrintSheet) ? FW_BOLD : FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CString strOut;
	if (nPrintSheetNumber >= 0)
		strOut = strPrintSheetNumber;
	else
		strOut = '-';

	if (m_nType != CPrintComponent::Flat)
		if (bDrawProductHeader)
			rcFrame.OffsetRect(0, 30);

	CRect rcText = rcFrame; rcText.left += 7; rcText.bottom = rcText.top + nTextHeight;
	pDC->SetTextColor((IsPlaced()) ? ((bIsActPrintSheet) ? RGB(255,100,15) : BLACK) : MIDDLEGRAY);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	rcBox.bottom = rcFrame.top + nTextHeight;

	font.DeleteObject();

	return rcBox;
}

CRect CPrintComponent::DrawQuantity(CDC* pDC, CRect rcFrame, int nSheetRefIndex, BOOL bDrawThumbnail, BOOL bDrawProductHeader, BOOL bShowUnprinted, BOOL bShowPrintedOnly, CDisplayList* pDisplayList)
{
	CRect rcBox = CRect(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	if (bShowUnprinted)
	{
		if (IsPlaced())
			return rcBox;
	}
	if (bShowPrintedOnly)
	{
		if ( ! IsPlaced())
			return rcBox;
	}

	int nPrintSheetNumber = -1;
	PrintComponentSheetRef& rSheetRef = m_sheetRefs[nSheetRefIndex];
	POSITION pos = pDoc->m_PrintSheetList.FindIndex(rSheetRef.m_nPrintSheetIndex);
	if (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetAt(pos);
		nPrintSheetNumber = rPrintSheet.GetNumberOnly();
	}

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CString strOut;
	if ( (rSheetRef.m_nQuantity < 0) || (m_nQuantityDesired <= 1) )
	{
		strOut = _T("");
	}
	else
		if (nPrintSheetNumber >= 0)
		{
			strOut.Format(_T(" %d"), rSheetRef.m_nQuantity);
			pDC->SetTextColor(BLACK);
		}
		else
		{
			strOut.Format(_T(" %d"), m_nQuantityPrinted);
			pDC->SetTextColor(BLACK);
		}

	if (m_nType != CPrintComponent::Flat)
		if (bDrawProductHeader)
			rcFrame.top += 30;

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CPrintComponent::DrawQuantityDiff(CDC* pDC, CRect rcFrame, int nSheetRefIndex, BOOL bDrawThumbnail, BOOL bDrawProductHeader, BOOL bShowUnprinted, BOOL bShowPrintedOnly, CDisplayList* pDisplayList)
{
	CRect rcBox = CRect(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	if (bShowUnprinted)
	{
		if (IsPlaced())
			return rcBox;
	}
	if (bShowPrintedOnly)
	{
		if ( ! IsPlaced())
			return rcBox;
	}

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	int		 nQuantity = m_nQuantityPrinted - m_nQuantityDesired;
	CString	 strOut1, strOut2;
	COLORREF crColor1 = BLACK;
	COLORREF crColor2 = (nQuantity < 0) ? RED : DARKGREEN;
	if (nSheetRefIndex == 0)
	{
		if (m_nQuantityDesired <= 1)
		{
			strOut1 = strOut2 = _T("");
		}
		else
			if (nQuantity > 0)
			{
				strOut1.Format(_T("+%d"),  nQuantity);
				strOut2.Format(_T("%.1f%%"), fabs(((float)m_nQuantityPrinted/(float)m_nQuantityDesired - 1.0f) * 100.0f));
			}
			else
				if (nQuantity < 0)
				{
					strOut1.Format(_T("%d"),   nQuantity);
					strOut2.Format(_T("%.1f%%"), fabs(((float)m_nQuantityPrinted/(float)m_nQuantityDesired - 1.0f) * 100.0f));
				}
				else
					if (m_sheetRefs[nSheetRefIndex].m_nQuantity < 0)	// not placed
						strOut1 = _T("");
					else
						strOut1 = _T("0");
	}

	if (m_nType != CPrintComponent::Flat)
		if (bDrawProductHeader)
			rcFrame.top += 30;

	pDC->SetTextColor(crColor1);
	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	DrawTextMeasured(pDC, strOut1, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	rcText.left = rcBox.right + 5;

	pDC->SetTextColor(crColor2);
	DrawTextMeasured(pDC, strOut2, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

int CPrintComponent::GetQuantityDiff()
{
	if (m_sheetRefs.GetSize() <= 0)
		return -1;

	int nQuantityDiff = m_nQuantityPrinted - m_nQuantityDesired;
	if (nQuantityDiff == 0)
		if (m_sheetRefs[0].m_nQuantity < 0)	// not placed
			return -1;

	return nQuantityDiff;
}