#pragma once


#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"


class CBookblockSplitter : public CSplitterWnd
{
    DECLARE_DYNAMIC(CBookblockSplitter)
public:
	CBookblockSplitter() { }

protected:
	virtual void OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect);

    DECLARE_MESSAGE_MAP()
    afx_msg LRESULT OnNcHitTest(CPoint point);
};


// CBookblockFrame-Rahmen

class CBookblockFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CBookblockFrame)
protected:
	CBookblockFrame();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CBookblockFrame();

// Attribute
public:
	int							m_nModalResult;
	BOOL						m_bZoomMode;
	BOOL						m_bHandActive;
	BOOL						m_bStatusRestored;
	CBookblockSplitter			m_wndSplitter, m_wndSplitter2;
	CDlgPDFOutputMediaSettings	m_dlgOutput;


public:
	int	GetActProductGroup();


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CBookblockFrame)
	public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL


protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnZoomIncrease();
	afx_msg void OnZoomDecrease();
	afx_msg void OnZoomToFit();
	afx_msg void OnZoomFullview();
	afx_msg void OnScrollhand();
	afx_msg void OnDestroy();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDeleteSelection();
	afx_msg void OnBookblockProof();
	afx_msg void OnBookblockProofOpenfile();
};


