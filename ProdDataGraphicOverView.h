#pragma once

#include "OwnerDrawnButtonList.h"


// CPDGCustomPaintFrame

class CPDGCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CPDGCustomPaintFrame)

public:
	CPDGCustomPaintFrame();
	virtual ~CPDGCustomPaintFrame();

public:
	DWORD m_itemData;

public:
	void Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, DWORD itemData) 
	{
		CStatic::Create(lpszText, dwStyle, rect, pParentWnd, nID); 
		m_itemData = itemData; 
	};

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};


// CPDGCustomPaintFrameList

class CPDGCustomPaintFrameList : public CArray <CPDGCustomPaintFrame*, CPDGCustomPaintFrame*>
{
	DECLARE_DYNAMIC(CPDGCustomPaintFrameList)

public:
	CPDGCustomPaintFrameList();
	virtual ~CPDGCustomPaintFrameList();

	virtual void RemoveAll();
};




// CProdDataGraphicOverView-Formularansicht

class CProdDataGraphicOverView : public CFormView
{
	DECLARE_DYNCREATE(CProdDataGraphicOverView)

protected:
	CProdDataGraphicOverView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CProdDataGraphicOverView();

public:
	enum { IDD = IDD_PRODDATAGRAPHICOVERVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	CPDGCustomPaintFrame		m_jobTitleHeader, m_prodPartHeader, m_prodPartFrame, m_prodDataFrame, m_trimBindDataFrame, m_cutFoldDataFrame, m_printDataFrame, m_prePressDataFrame;
	CPDGCustomPaintFrameList	m_prodPartHeaderList, m_prodPartFrameList, m_dividerFrameList;
	HTHEME						m_hThemeToolbar;
	CFont						m_smallFont, m_mediumFont, m_bigFont;
	CBrush						m_BkBrushBlue, m_BkBrushOrange, m_hollowBrush;
	COwnerDrawnButtonList		m_jobComponentButtons;


public:
	static void DrawItemJobComponent	(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void DrawItemProduct			(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void DrawItemTrimming		(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void DrawItemBinding			(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void DrawItemCutFold			(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void DrawItemPrint			(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void DrawItemPrePress		(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	void		InitFrameRects(	CRect& rcProdPartHeader, CRect& rcProdPartFrame, CRect& rcProductDataFrame, CRect& rcBindDataFrame, CRect& rcCutFoldDataFrame, 
								CRect& rcPrintDataFrame, CRect& rcPrePressDataFrame);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void OnDraw(CDC* /*pDC*/);

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelectProductData();
	afx_msg void OnSelectTrimData();
	afx_msg void OnSelectBindData();
	afx_msg void OnSelectCutFoldData();
	afx_msg void OnDeleteCutFoldData();
	afx_msg void OnSelectPrintData();
	afx_msg void OnSelectPrePressData();
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};


