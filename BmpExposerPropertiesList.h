// BmpExposerPropertiesList.h : header file
//



class CMediaSize : public CObject
{
public:
    DECLARE_SERIAL( CMediaSize )
	CMediaSize()
	{
		m_fMediaWidth  = 0.0f;
		m_fMediaHeight = 0.0f;
	};

	CMediaSize(const CMediaSize& rMediaSize)
	{
		m_strMediaName = rMediaSize.m_strMediaName;
		m_fMediaWidth  = rMediaSize.m_fMediaWidth;
		m_fMediaHeight = rMediaSize.m_fMediaHeight;
	};

// Attributes
public:
	CString m_strMediaName;
	float	m_fMediaWidth;
	float	m_fMediaHeight;

//Operations:
public:
	const CMediaSize& operator=(const CMediaSize& rMediaSize);
};

template <> void AFXAPI SerializeElements <CMediaSize> (CArchive& ar, CMediaSize* pMediaSize, INT_PTR nCount);
void AFXAPI DestructElements(CMediaSize* pMediaSize, INT_PTR nCount);



class CBmpExposerProperties : public CObject
{
public:
    DECLARE_SERIAL( CBmpExposerProperties )
	CBmpExposerProperties();

//Attributes:
public:
	CString							 m_strExposerName;
	CString							 m_strDstPath;
	CArray <CMediaSize, CMediaSize>  m_MediaList;

//Operations:
public:
	const CBmpExposerProperties& operator=(const CBmpExposerProperties& rBmpExposerProperties);
};

void AFXAPI SerializeElements(CArchive& ar, CBmpExposerProperties* pBmpExposerProperties, INT_PTR nCount);
void AFXAPI DestructElements(CBmpExposerProperties* pBmpExposerProperties, INT_PTR nCount);


class CBmpExposerPropertiesList : public CArray <CBmpExposerProperties, CBmpExposerProperties&>	
{																	
public:
    DECLARE_SERIAL( CBmpExposerPropertiesList )
	CBmpExposerPropertiesList();


//Attributes:

//Operations:
	BOOL Load();
	BOOL Save();
};
/////////////////////////////////////////////////////////////////////////////

