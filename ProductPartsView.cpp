// ProductPartsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "BookBlockView.h"
#include "PageListTableView.h"
#include "ThemedTabCtrl.h"
#include "ProductionDataView.h"
#include "DlgBindingAssistent.h"
#include "OrderDataView.h"
#include "DlgPressDevice.h"
#include "PrintSheetTableView.h"
#include "BookBlockDetailView.h"
#include "ProductDataView.h"
#include "DlgImpositionScheme.h"
#include "DlgPrintSheetLayout.h"
#include "DlgShinglingAssistent.h"
#include "MainFrm.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProductPartsView

IMPLEMENT_DYNCREATE(CProductPartsView, CFormView)

CProductPartsView::CProductPartsView()
	: CFormView(CProductPartsView::IDD)
{
	//{{AFX_DATA_INIT(CProductPartsView)
	m_strPrintMachine = _T("");
	m_strPlateFormat = _T("");
	m_nFoldSheetTypeNum = 0;
	m_strNumPages = _T("");
	m_strPagesNotAssigned = _T("");
	m_nBindingMethod = -1;
	m_nProductionType = -1;
	m_strFoldSheet = _T("");
	m_strPrintLayout = _T("");
	m_strMarkset = _T("");
	m_strPaperFormat = _T("");
	m_strPrintingType = _T("");
	m_strTurningType = _T("");
	m_strShinglingFoot = _T("");
	m_strShinglingHead = _T("");
	m_strPageFormat = _T("");
	m_fFootTrim = 0.0f;
	m_fHeadTrim = 0.0f;
	m_bOverFoldBack = FALSE;
	m_fOverFold = 0.0f;
	m_bOverFoldFront = FALSE;
	m_fSideTrim = 0.0f;
	m_bTrimsIdentical = FALSE;
	m_fMillingEdge = 0.0f;
	m_fSpineTrim = 0.0f;
	//}}AFX_DATA_INIT
	m_nLayoutSide = BOTHSIDES;

	m_BkBrush.CreateSolidBrush(RGB(252, 220, 150));
	m_smallFont.CreateFont (14, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Arial");
	m_mediumFont.CreateFont(18, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Arial");
	m_bigFont.CreateFont   (23, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Arial");
}

CProductPartsView::~CProductPartsView()
{
}

void CProductPartsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProductPartsView)
	DDX_Control(pDX, IDC_SPINETRIM_SPIN, m_spineTrimSpin);
	DDX_Control(pDX, IDC_MILLINGEDGE_SPIN, m_millingEdgeSpin);
	DDX_Control(pDX, IDC_SIDETRIM_SPIN, m_sideTrimSpin);
	DDX_Control(pDX, IDC_OVERFOLD_SPIN, m_overFoldSpin);
	DDX_Control(pDX, IDC_HEADTRIM_SPIN, m_headTrimSpin);
	DDX_Control(pDX, IDC_FOOTTRIM_SPIN, m_footTrimSpin);
	DDX_Control(pDX, IDC_PRODUCTPARTS_TAB, m_productPartsTab);
	DDX_Control(pDX, IDC_PP_FOLDSHEETTYPELIST, m_ppFoldSheetTypeList);
	DDX_Control(pDX, IDC_PP_PRINTSHEETLIST, m_ppPrintSheetList);
	DDX_Control(pDX, IDC_PP_FOLDSHEETLIST, m_ppFoldSheetList);
	DDX_Text(pDX, IDC_PP_PRINTMACHINE, m_strPrintMachine);
	DDX_Text(pDX, IDC_PP_PLATEFORMAT, m_strPlateFormat);
	DDX_Text(pDX, IDC_PP_NUMFOLDSHEET_EDIT, m_nFoldSheetTypeNum);
	DDX_Text(pDX, IDC_PP_NUM_PAGES, m_strNumPages);
	DDX_Text(pDX, IDC_PP_NOTASSIGNED_PAGES, m_strPagesNotAssigned);
	DDX_CBIndex(pDX, IDC_PP_BINDING_METHOD, m_nBindingMethod);
	DDX_CBIndex(pDX, IDC_PP_PRODUCTION_TYPE, m_nProductionType);
	DDX_Text(pDX, IDC_PP_FOLDSHEET, m_strFoldSheet);
	DDX_Text(pDX, IDC_PP_PRINTLAYOUT, m_strPrintLayout);
	DDX_Text(pDX, IDC_PP_MARKSET, m_strMarkset);
	DDX_Text(pDX, IDC_PP_PAPERFORMAT, m_strPaperFormat);
	DDX_Text(pDX, IDC_PP_PRINTINGTYPE, m_strPrintingType);
	DDX_Text(pDX, IDC_PP_TURNINGTYPE, m_strTurningType);
	DDX_Text(pDX, IDC_PP_SHINGLING_FOOT, m_strShinglingFoot);
	DDX_Text(pDX, IDC_PP_SHINGLING_HEAD, m_strShinglingHead);
	DDX_Text(pDX, IDC_PP_PAGEFORMAT, m_strPageFormat);
	DDX_Measure(pDX, IDC_FOOTTRIM_EDIT, m_fFootTrim);
	DDX_Measure(pDX, IDC_HEADTRIM_EDIT, m_fHeadTrim);
	DDX_Check(pDX, IDC_OVERFOLD_BACK, m_bOverFoldBack);
	DDX_Measure(pDX, IDC_OVERFOLD_EDIT, m_fOverFold);
	DDX_Check(pDX, IDC_OVERFOLD_FRONT, m_bOverFoldFront);
	DDX_Measure(pDX, IDC_SIDETRIM_EDIT, m_fSideTrim);
	DDX_Check(pDX, IDC_TRIMS_IDENTICAL, m_bTrimsIdentical);
	DDX_Measure(pDX, IDC_MILLINGEDGE_EDIT, m_fMillingEdge);
	DDX_Measure(pDX, IDC_SPINETRIM_EDIT, m_fSpineTrim);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProductPartsView, CFormView)
	//{{AFX_MSG_MAP(CProductPartsView)
	ON_WM_PAINT()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PP_FOLDSHEETTYPELIST, OnItemchangedPpFoldsheettypelist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PP_FOLDSHEETLIST, OnItemchangedPpFoldsheetlist)
	ON_BN_CLICKED(IDC_PP_REMOVE_FOLDSHEET, OnPpRemoveFoldsheet)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PP_NUMFOLDSHEET_SPIN, OnDeltaposPpNumfoldsheetSpin)
	ON_BN_CLICKED(IDC_PP_ADD_FOLDSHEET, OnPpAddFoldsheet)
	ON_CBN_SELCHANGE(IDC_PP_BINDING_METHOD, OnSelchangePpBindingMethod)
	ON_WM_CTLCOLOR()
	ON_NOTIFY(TCN_SELCHANGE, IDC_PRODUCTPARTS_TAB, OnSelchangeProductpartsTab)
	ON_BN_CLICKED(IDC_PP_REMOVE_FOLDSHEET_BUTTON, OnPpRemoveFoldsheetButton)
	ON_BN_CLICKED(IDC_PP_ADD_FOLDSHEET_BUTTON, OnPpAddFoldsheetButton)
	ON_BN_CLICKED(IDC_PP_MODIFY_LAYOUT, OnPpModifyLayout)
	ON_BN_CLICKED(IDC_PP_SHINGLING, OnPpShingling)
	ON_BN_CLICKED(IDC_PP_PRINTSHEET_DETAILS, OnPpPrintsheetDetails)
	ON_BN_CLICKED(IDC_PP_BOOKBLOCK_DETAILS, OnPpBookblockDetails)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FOOTTRIM_SPIN, OnDeltaposFoottrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_HEADTRIM_SPIN, OnDeltaposHeadtrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_MILLINGEDGE_SPIN, OnDeltaposMillingedgeSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_OVERFOLD_SPIN, OnDeltaposOverfoldSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SIDETRIM_SPIN, OnDeltaposSidetrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPINETRIM_SPIN, OnDeltaposSpinetrimSpin)
	ON_BN_CLICKED(IDC_TRIMS_IDENTICAL, OnTrimsIdentical)
	ON_EN_KILLFOCUS(IDC_FOOTTRIM_EDIT, OnKillfocusFoottrimEdit)
	ON_EN_KILLFOCUS(IDC_HEADTRIM_EDIT, OnKillfocusHeadtrimEdit)
	ON_EN_KILLFOCUS(IDC_SPINETRIM_EDIT, OnKillfocusSpinetrimEdit)
	ON_EN_KILLFOCUS(IDC_MILLINGEDGE_EDIT, OnKillfocusMillingedgeEdit)
	ON_EN_KILLFOCUS(IDC_OVERFOLD_EDIT, OnKillfocusOverfoldEdit)
	ON_EN_KILLFOCUS(IDC_SIDETRIM_EDIT, OnKillfocusSidetrimEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Diagnose CProductPartsView

#ifdef _DEBUG
void CProductPartsView::AssertValid() const
{
	CFormView::AssertValid();
}

void CProductPartsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CProductPartsView 


void CProductPartsView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();

	CRect rect;
	GetDlgItem(IDC_PRODUCTPARTS_TAB)->GetWindowRect(rect);
	ScreenToClient(rect);
	m_themedTabCtrl.Create(TCS_FIXEDWIDTH | WS_VISIBLE, rect, this, IDC_PRODUCTPARTS_TAB);
	m_themedTabCtrl.GetThemedTabCtrlBackground();

	GetDlgItem(IDC_LAYOUT_PREVIEW_FRAME)->GetWindowRect(m_layoutPreviewFrame);
	ScreenToClient(m_layoutPreviewFrame);
	GetDlgItem(IDC_FOLDSHEET_PREVIEW_FRAME)->GetWindowRect(m_foldSheetPreviewFrame);
	ScreenToClient(m_foldSheetPreviewFrame);
	GetDlgItem(IDC_BOOKBLOCK_PREVIEW_FRAME)->GetWindowRect(m_bookblockPreviewFrame);
	ScreenToClient(m_bookblockPreviewFrame);

/*	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		for (int i = 0; i < pDoc->m_productPartList.GetSize(); i++)
			m_productPartsTab.InsertItem(i, pDoc->m_productPartList[i].m_strName);
*/
	m_spineTrimSpin.SetRange(0, 1);  
	m_millingEdgeSpin.SetRange(0, 1);  

	m_bTrimsIdentical = TRUE;
	m_headTrimSpin.SetRange(0, 1);  
	m_footTrimSpin.SetRange(0, 1);  
	m_sideTrimSpin.SetRange(0, 1);  

	m_overFoldSpin.SetRange(0, 1);  

	((CSpinButtonCtrl*)GetDlgItem(IDC_PP_NUMFOLDSHEET_SPIN))->SetRange(0, 999);

///// foldsheettype list /////////////////////////////////////////////////////////////////////////////////////////////
	ListView_SetExtendedListViewStyle(m_ppFoldSheetTypeList.m_hWnd, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	CString string;
	string.LoadString(IDS_COUNT_TEXT);
	m_ppFoldSheetTypeList.InsertColumn(0, string, LVCFMT_LEFT,  38, 0);
	string.LoadString(IDS_SCHEME);
	m_ppFoldSheetTypeList.InsertColumn(1, string, LVCFMT_LEFT, 120, 0);
	string.LoadString(IDS_WEB);;
	m_ppFoldSheetTypeList.InsertColumn(2, string, LVCFMT_LEFT,  40, 0);
	string.LoadString(IDS_PRINTSHEETLAYOUT);
	m_ppFoldSheetTypeList.InsertColumn(3, string, LVCFMT_LEFT, 100, 0);

///// foldsheet list /////////////////////////////////////////////////////////////////////////////////////////////
	ListView_SetExtendedListViewStyle(m_ppFoldSheetList.m_hWnd, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	string.LoadString(IDS_NUMBER_TEXT);
	m_ppFoldSheetList.InsertColumn(0, string, LVCFMT_LEFT,  50, 0);
	string.LoadString(IDS_PAGELISTHEADER_PAGES);
	m_ppFoldSheetList.InsertColumn(1, string, LVCFMT_LEFT, 120, 0);
	string.LoadString(IDS_SCHEME);
	m_ppFoldSheetList.InsertColumn(2, string, LVCFMT_LEFT, 150, 0);


///// printsheet list /////////////////////////////////////////////////////////////////////////////////////////////

	ListView_SetExtendedListViewStyle(m_ppPrintSheetList.m_hWnd, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	int nIndex = 0;
	string.LoadString(IDS_NUMBER_TEXT);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,   35, 0);
	string.LoadString(IDS_PRODUCTPART);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_PRINTSHEETLAYOUT);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_MACHINE);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,   90, 0);
	string.LoadString(IDS_PLATESIZE);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_TYPEOFPRINTING);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,   80, 0);
	string.LoadString(IDS_TYPEOFTURNING);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,   80, 0);
	string.LoadString(IDS_PAPERSIZE);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_COLORS);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,   40, 0);
	string.LoadString(IDS_FOLDSHEETNUMBER);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,   50, 0);
	string.LoadString(IDS_PAGELISTHEADER_PAGES);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_SCHEME);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,  150, 0);
	string.LoadString(IDS_FINAL_TRIMSIZE);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,   80, 0);
	string.LoadString(IDS_NUP);
	m_ppPrintSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,   60, 0);

	((CComboBox*)GetDlgItem(IDC_JDF_BINDING_COMBO ))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_JDF_FOLDING_COMBO ))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_JDF_PRINTING_COMBO))->SetCurSel(0);

	UpdateAll();
}

BOOL g_bLock = FALSE;

void CProductPartsView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	if (g_bLock)
		return;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	ShowControls(FALSE, FALSE);

	int nProductPartIndex = GetCurrentProductPartIndex();
	if (nProductPartIndex < 0)
		if ( ! pDoc->m_productPartList.GetSize())
			return;
		else
			nProductPartIndex = 0;

	for (int i = 0; i < pDoc->m_productPartList.GetSize(); i++)
	{
		TCITEM tcitem;
		tcitem.mask = TCIF_TEXT;
		tcitem.pszText = pDoc->m_productPartList[i].m_strName.GetBuffer(MAX_TEXT);
		if (i < m_productPartsTab.GetItemCount())
			m_productPartsTab.SetItem	(i, &tcitem);
		else
			m_productPartsTab.InsertItem(i, &tcitem);
	}
	while (i < m_productPartsTab.GetItemCount())
		m_productPartsTab.DeleteItem(i);

	m_nProductionType = pDoc->m_productPartList[nProductPartIndex].m_nProductionType;
	m_nBindingMethod  = pDoc->m_productPartList[nProductPartIndex].m_nBindingMethod;
	m_fHeadTrim		  = pDoc->m_productPartList[nProductPartIndex].m_fHeadTrim;
	m_fFootTrim		  = pDoc->m_productPartList[nProductPartIndex].m_fFootTrim;
	m_fSideTrim		  = pDoc->m_productPartList[nProductPartIndex].m_fSideTrim;
	m_fSpineTrim	  = pDoc->m_productPartList[nProductPartIndex].m_fSpineTrim;
	m_fMillingEdge	  = pDoc->m_productPartList[nProductPartIndex].m_fMillingEdge;
	switch (pDoc->m_productPartList[nProductPartIndex].m_nOverFoldSide)
	{
	case CProductPart::OverFoldNone:	m_bOverFoldFront = FALSE; m_bOverFoldBack = FALSE;	break;
	case CProductPart::OverFoldFront:	m_bOverFoldFront = TRUE;  m_bOverFoldBack  = FALSE; break;
	case CProductPart::OverFoldBack:	m_bOverFoldFront = FALSE; m_bOverFoldBack = TRUE;	break;
	}
	m_fOverFold = pDoc->m_productPartList[nProductPartIndex].m_fOverFold;

	g_bLock = TRUE;
	InitializeLists(); 
	g_bLock = FALSE;

	CString strMeasureFormat;
	strMeasureFormat.Format("%s x %s", (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
	m_strPageFormat.Format(strMeasureFormat, MeasureFormat((float)pDoc->m_productPartList[nProductPartIndex].m_fPageWidth), MeasureFormat((float)pDoc->m_productPartList[nProductPartIndex].m_fPageHeight));

	CLayout* pLayout = GetCurrentLayout();
	if (pLayout)
	{
		m_strPrintLayout = pLayout->m_strLayoutName;
		CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
		if (pPressDevice)
		{
			m_strPrintMachine = pPressDevice->m_strName;
			m_strPlateFormat.Format((LPCTSTR)strMeasureFormat, MeasureFormat((float)pPressDevice->m_fPlateWidth), MeasureFormat((float)pPressDevice->m_fPlateHeight));
		}
		m_strPaperFormat.Format(strMeasureFormat, MeasureFormat((float)pLayout->m_FrontSide.m_fPaperWidth), MeasureFormat((float)pLayout->m_FrontSide.m_fPaperHeight));
		m_strMarkset	  = "";
		m_strPrintingType = pLayout->GetPrintingType();
		m_strTurningType  = pLayout->GetTurningType();
	}
	else
	{
		m_strPrintLayout  = "";
		m_strPrintMachine = "";
		m_strPlateFormat  = "";
		m_strPaperFormat  = "";
		m_strMarkset	  = "";
		m_strPrintingType = "";
		m_strTurningType  = "";
	}

	int			nNum		= 0;
	CFoldSheet*	pFoldSheet  = GetCurrentFoldSheet(&nNum);
	if (pFoldSheet)
	{
/*		if (pFoldSheet->m_LayerList.GetCount() > 1)
		{
			CFoldSheetLayer* pLayer	= GetCurrentFoldSheetLayer();
			m_strFoldSheet.Format("%s (%c)", pLayer->GetWebName(pFoldSheet));
		}
		else
*/
			m_strFoldSheet = pFoldSheet->m_strFoldSheetTypeName;
			if (fabs(pFoldSheet->m_fShinglingValue) > 0.001f)
				m_strShinglingHead.Format("%.2f", pFoldSheet->m_fShinglingValue);
			else
				m_strShinglingHead = "";

			if (fabs(pFoldSheet->m_fShinglingValueFoot) > 0.001f)
				m_strShinglingFoot.Format("%.2f", pFoldSheet->m_fShinglingValueFoot);
			else
				if (fabs(pFoldSheet->m_fShinglingValue) > 0.001f)
					m_strShinglingFoot = m_strShinglingHead;
				else
					m_strShinglingFoot = "";
	}
	else
	{
		m_strFoldSheet	   = "";
		m_strShinglingHead = "";
		m_strShinglingFoot = "";
	}

	m_nFoldSheetTypeNum = nNum;

	m_strNumPages.Format(" %d", pDoc->m_productPartList[nProductPartIndex].m_nNumPages);

	int nRestPages = pDoc->m_productPartList[nProductPartIndex].m_nNumPages;
	for (i = 0; i < m_foldSheetTypes.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = m_foldSheetTypes[i];
		if ( ! pFoldSheet)
			continue;
		if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
			nRestPages-= pFoldSheet->m_nPagesLeft * m_foldSheetTypeNums[i];
		else
			nRestPages-= (pFoldSheet->m_nPagesLeft + pFoldSheet->m_nPagesRight) * m_foldSheetTypeNums[i];
	}
	m_strPagesNotAssigned.Format(" %d", nRestPages);
	GetDlgItem(IDC_PP_ADD_FOLDSHEET)->EnableWindow((nRestPages));

	ShowControls((pDoc->m_Bookblock.m_bShinglingActive && pFoldSheet), (pFoldSheet) ? TRUE : FALSE);
	
	UpdateData(FALSE);
}
	
void CProductPartsView::ShowControls(BOOL bShow1, BOOL bShow2)
{
	int nShow1 = (bShow1) ? SW_SHOW : SW_HIDE;
	int nShow2 = (bShow2) ? SW_SHOW : SW_HIDE;
	GetDlgItem(IDC_PP_SHINGLING_HEAD_STATIC		)->ShowWindow(nShow1);
	GetDlgItem(IDC_PP_SHINGLING_HEAD			)->ShowWindow(nShow1);
	GetDlgItem(IDC_PP_SHINGLING_FOOT_STATIC		)->ShowWindow(nShow1);
	GetDlgItem(IDC_PP_SHINGLING_FOOT			)->ShowWindow(nShow1);

//	GetDlgItem(IDC_NUMBER_FOLDSHEETS			)->ShowWindow(nShow2);
//	GetDlgItem(IDC_NUMBER_PAGES					)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_ADD_FOLDSHEET_BUTTON		)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_REMOVE_FOLDSHEET_BUTTON	)->ShowWindow(nShow2);
//	GetDlgItem(IDC_FOLDSHEET_UP					)->ShowWindow(nShow2);
//	GetDlgItem(IDC_FOLDSHEET_DOWN				)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_BOOKBLOCK_DETAILS			)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_SHINGLING					)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_FOLDMARKS					)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_SECTIONMARKS				)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_CUTMARKS					)->ShowWindow(nShow2);
	GetDlgItem(IDC_OVERFOLD_FRONT				)->ShowWindow(nShow2);
	GetDlgItem(IDC_OVERFOLD_BACK				)->ShowWindow(nShow2);
	GetDlgItem(IDC_OVERFOLD_EDIT				)->ShowWindow(nShow2);
	GetDlgItem(IDC_OVERFOLD_SPIN				)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_REMOVE_FOLDSHEET			)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_MODIFY_LAYOUT				)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_PRINTSHEET_DETAILS		)->ShowWindow(nShow2);
	GetDlgItem(IDC_JDF_BINDINGICON				)->ShowWindow(nShow2);
	GetDlgItem(IDC_JDF_BINDING_COMBO			)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_JDF_BINDING				)->ShowWindow(nShow2);
	GetDlgItem(IDC_JDF_FOLDINGICON				)->ShowWindow(nShow2);
	GetDlgItem(IDC_JDF_FOLDING_COMBO			)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_JDF_FOLDING				)->ShowWindow(nShow2);
	GetDlgItem(IDC_JDF_PRINTINGICON				)->ShowWindow(nShow2);
	GetDlgItem(IDC_JDF_PRINTING_COMBO			)->ShowWindow(nShow2);
	GetDlgItem(IDC_PP_JDF_PRINTING				)->ShowWindow(nShow2);
}

void CProductPartsView::SetModified()
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = GetCurrentProductPartIndex();
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	pDoc->m_productPartList[nProductPartIndex].m_nProductionType = m_nProductionType;
	pDoc->m_productPartList[nProductPartIndex].m_nBindingMethod  = m_nBindingMethod;
	pDoc->m_productPartList[nProductPartIndex].m_fHeadTrim		 = m_fHeadTrim;
	pDoc->m_productPartList[nProductPartIndex].m_fFootTrim		 = m_fFootTrim;
	pDoc->m_productPartList[nProductPartIndex].m_fSideTrim		 = m_fSideTrim;
	pDoc->m_productPartList[nProductPartIndex].m_fSpineTrim		 = m_fSpineTrim;
	pDoc->m_productPartList[nProductPartIndex].m_fMillingEdge	 = m_fMillingEdge;
	if (m_bOverFoldFront)
		pDoc->m_productPartList[nProductPartIndex].m_nOverFoldSide = CProductPart::OverFoldFront;
	else
		if (m_bOverFoldBack)
			pDoc->m_productPartList[nProductPartIndex].m_nOverFoldSide = CProductPart::OverFoldBack;
		else
			pDoc->m_productPartList[nProductPartIndex].m_nOverFoldSide = CProductPart::OverFoldNone;
	pDoc->m_productPartList[nProductPartIndex].m_fOverFold = m_fOverFold;

	pDoc->SetModifiedFlag();
}

int CProductPartsView::GetCurrentProductPartIndex()
{
	return m_productPartsTab.GetCurSel();
}

void CProductPartsView::InitializeLists()
{
	InitializeFoldSheetTypeList();
	InitializeFoldSheetList();
	InitializePrintSheetList();
}
void CProductPartsView::InitializeFoldSheetList()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nSel = GetCurrentProductPartIndex();
	if ( (nSel < 0) || (nSel >= pDoc->m_productPartList.GetSize()) )
		return;

	POSITION pos = m_ppFoldSheetList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_ppFoldSheetList.GetNextSelectedItem(pos) : 0;

	CString string;
	LVITEM  lvitem;
	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets);

	int nItemIndex = 0;
    for (int i = 0; i < sortedFoldSheets.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = sortedFoldSheets[i];
		if (pFoldSheet->m_nProductPartIndex != nSel)
			continue;

		string = pFoldSheet->GetNumber();
		lvitem.iItem	= nItemIndex;
		lvitem.iSubItem = 0;
		lvitem.mask	    = LVIF_TEXT | LVIF_PARAM;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		lvitem.lParam	= (long)pFoldSheet;
		if (i < m_ppFoldSheetList.GetItemCount())
			m_ppFoldSheetList.SetItem	(&lvitem); 
		else
			m_ppFoldSheetList.InsertItem(&lvitem); 

		string = pFoldSheet->GetPageRange();
		lvitem.iSubItem = 1;
		lvitem.mask	    = LVIF_TEXT;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);		
		m_ppFoldSheetList.SetItem(&lvitem); 

		string = pFoldSheet->m_strFoldSheetTypeName;
		lvitem.iSubItem = 2;
		lvitem.mask	    = LVIF_TEXT;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);		
		m_ppFoldSheetList.SetItem(&lvitem); 

		nItemIndex++;
	}
	while (nItemIndex < m_ppFoldSheetList.GetItemCount())
		m_ppFoldSheetList.DeleteItem(nItemIndex);
}

void CProductPartsView::InitializeFoldSheetTypeList()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = GetCurrentProductPartIndex();
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	POSITION pos = m_ppFoldSheetTypeList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_ppFoldSheetTypeList.GetNextSelectedItem(pos) : 0;

	CString string;
	LVITEM  lvitem;
	pDoc->m_Bookblock.GetFoldSheetTypes(m_foldSheetTypes, m_foldSheetTypeNums, nProductPartIndex);

	int nItemIndex = 0;
    for (int i = 0; i < m_foldSheetTypes.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = m_foldSheetTypes[i];
		if ( ! pFoldSheet)
			continue;

		char cWeb = 'A';
		int  nLayerIndex = 0;
		POSITION pos = pFoldSheet->m_LayerList.GetHeadPosition();
		while (pos)
		{
			CFoldSheetLayer& rLayer = pFoldSheet->m_LayerList.GetNext(pos);

			string.Format("%d", m_foldSheetTypeNums[i]);
			lvitem.iItem	= nItemIndex;
			lvitem.iSubItem = 0;
			lvitem.mask	    = LVIF_TEXT | LVIF_PARAM;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			lvitem.lParam	= (long)&rLayer;
			if (nItemIndex < m_ppFoldSheetTypeList.GetItemCount())
				m_ppFoldSheetTypeList.SetItem	(&lvitem); 
			else
				m_ppFoldSheetTypeList.InsertItem(&lvitem); 

			string = pFoldSheet->m_strFoldSheetTypeName;
			lvitem.iSubItem = 1;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);		
			m_ppFoldSheetTypeList.SetItem(&lvitem); 

			string.Format("%c", (pFoldSheet->m_LayerList.GetCount() > 1) ? cWeb : ' ');
			lvitem.iSubItem = 2;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);		
			m_ppFoldSheetTypeList.SetItem(&lvitem); 

			if (cWeb == 'A')
			{
/*				int nPages = 0;
				if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
					nPages+= pFoldSheet->m_nPagesLeft * m_foldSheetTypeNums[i];
				else
					nPages+= (pFoldSheet->m_nPagesLeft + pFoldSheet->m_nPagesRight) * m_foldSheetTypeNums[i];
				string.Format("%d", nPages);
				lvitem.iSubItem = 3;
				lvitem.mask	    = LVIF_TEXT;
				lvitem.pszText  = string.GetBuffer(MAX_TEXT);		
				m_ppFoldSheetTypeList.SetItem(&lvitem); 
*/
			}
			else
				i++;

			CPrintSheet* pPrintSheet = rLayer.GetFirstPrintSheet();
			CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			string = (pLayout) ? pLayout->m_strLayoutName : "";
			lvitem.iSubItem = 3;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);		
			m_ppFoldSheetTypeList.SetItem(&lvitem); 

			cWeb++;
			nItemIndex++;
		}
	}
	while (nItemIndex < m_ppFoldSheetTypeList.GetItemCount())
		m_ppFoldSheetTypeList.DeleteItem(nItemIndex);
}

void CProductPartsView::InitializePrintSheetList()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = m_ppPrintSheetList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_ppPrintSheetList.GetNextSelectedItem(pos) : 0;

	m_ppPrintSheetList.DeleteAllItems();

	LVITEM   lvitem;
	CString  string, strMeasureFormat;
	pos = pDoc->m_PrintSheetList.GetHeadPosition();
    while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		rPrintSheet.InitializeFoldSheetTypeList();
		CLayout*	  pLayout	   = rPrintSheet.GetLayout();
		CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
		if ( ! pLayout || ! pPressDevice)
			continue;

		for (int j = 0; j < rPrintSheet.m_FoldSheetTypeList.GetSize(); j++)
		{
			CFoldSheet* pFoldSheet = rPrintSheet.m_FoldSheetTypeList[j].m_pFoldSheet;

			if ( ! pFoldSheet)
				continue;
			int nProductPartIndex = pFoldSheet->m_nProductPartIndex;
			if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
				return;

			int nSubItem = 0;

			string.Format("%s", rPrintSheet.GetNumber());
			lvitem.iItem	= m_ppPrintSheetList.GetItemCount();
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT | LVIF_STATE;
			if (lvitem.iItem == nCurSel)
			{
				lvitem.state	= LVIS_SELECTED;
				lvitem.stateMask= LVIS_SELECTED;
			}
			else
			{
				lvitem.state	= 0;
				lvitem.stateMask= 0;
			}
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.InsertItem(&lvitem); 

			string = pDoc->m_productPartList[nProductPartIndex].m_strName;
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string.Format("%s", (pLayout) ? pLayout->m_strLayoutName : "");
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string = pPressDevice->m_strName;
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			strMeasureFormat.Format("%s x %s", (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
			string.Format((LPCTSTR)strMeasureFormat, MeasureFormat((float)pPressDevice->m_fPlateWidth), MeasureFormat((float)pPressDevice->m_fPlateHeight));
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string = pLayout->GetPrintingType();
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string = pLayout->GetTurningType();
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string.Format(strMeasureFormat, MeasureFormat((float)pLayout->m_FrontSide.m_fPaperWidth), MeasureFormat((float)pLayout->m_FrontSide.m_fPaperHeight));
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string.Format("%d/%d", pDoc->m_productPartList[nProductPartIndex].m_colorantList.GetSize(), pDoc->m_productPartList[nProductPartIndex].m_colorantList.GetSize());
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			if (pFoldSheet->m_LayerList.GetCount() > 1)
					string.Format("%s (%d)", pFoldSheet->GetNumber(), rPrintSheet.m_FoldSheetTypeList[j].m_nFoldSheetLayerIndex + 1);
				else
					string.Format("%s", pFoldSheet->GetNumber());
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string = pFoldSheet->GetPageRange();
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string = pFoldSheet->m_strFoldSheetTypeName;
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string.Format(strMeasureFormat, MeasureFormat((float)pDoc->m_productPartList[nProductPartIndex].m_fPageWidth), MeasureFormat((float)pDoc->m_productPartList[nProductPartIndex].m_fPageHeight));
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 

			string.Format("%d", rPrintSheet.m_FoldSheetTypeList[j].m_nNumUp);
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_ppPrintSheetList.SetItem(&lvitem); 
		}
	}

//	m_ppPrintSheetList.SetItemState(nCurSel, LVIS_SELECTED, LVIS_SELECTED);
}

void CProductPartsView::OnDeltaposSpinetrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_spineTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	SetModified();
	
	*pResult = 0;
}

void CProductPartsView::OnDeltaposMillingedgeSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_millingEdgeSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	SetModified();
	
	*pResult = 0;
}

void CProductPartsView::OnDeltaposHeadtrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_headTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);

	OnKillfocusHeadtrimEdit();
	
	*pResult = 0;
}

void CProductPartsView::OnDeltaposFoottrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_footTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);

	OnKillfocusFoottrimEdit();
	
	*pResult = 0;
}

void CProductPartsView::OnDeltaposSidetrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_sideTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);

	OnKillfocusSidetrimEdit();
	
	*pResult = 0;
}

void CProductPartsView::OnDeltaposOverfoldSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_overFoldSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	SetModified();
	
	*pResult = 0;
}

void CProductPartsView::OnTrimsIdentical() 
{
	OnKillfocusHeadtrimEdit();
}

void CProductPartsView::OnKillfocusSpinetrimEdit() 
{
	SetModified();
}

void CProductPartsView::OnKillfocusMillingedgeEdit() 
{
	SetModified();
}

void CProductPartsView::OnKillfocusHeadtrimEdit() 
{
	UpdateData(TRUE);
	if (m_bTrimsIdentical)
	{
		m_fFootTrim = m_fSideTrim = m_fHeadTrim;
		UpdateData(FALSE);
	}
	SetModified();
}

void CProductPartsView::OnKillfocusFoottrimEdit() 
{
	UpdateData(TRUE);
	if (m_bTrimsIdentical)
	{
		m_fHeadTrim = m_fSideTrim = m_fFootTrim;
		UpdateData(FALSE);
	}
	SetModified();
}

void CProductPartsView::OnKillfocusSidetrimEdit() 
{
	UpdateData(TRUE);
	if (m_bTrimsIdentical)
	{
		m_fHeadTrim = m_fFootTrim = m_fSideTrim;
		UpdateData(FALSE);
	}
	SetModified();
}

void CProductPartsView::OnKillfocusOverfoldEdit() 
{
	SetModified();
}



#define HEADLINECOLOR RGB(153, 204, 0) //RGB(213, 237, 82)  //RGB(153, 204, 255)


void CProductPartsView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CRect clientRect;
	GetClientRect(clientRect);
//	pDC->FillSolidRect(clientRect, CREME);

	CFont* pOldFont = dc.GetCurrentFont();

	dc.SelectObject(&m_bigFont);

	CString strOut;
	CRect controlRect;
	GetDlgItem(IDC_PP_HEADER)->GetWindowRect(controlRect); 
	ScreenToClient(controlRect);
	dc.FillSolidRect(controlRect, HEADLINECOLOR);
	strOut.LoadString(IDS_NAV_PRODUCTIONDATA);
	dc.SetTextColor(BLACK);
	dc.SetBkMode(TRANSPARENT);
	dc.TextOut(controlRect.left + 10, controlRect.top + 2, strOut);

	dc.SelectObject(&m_mediumFont);
	
	dc.SetTextColor(BLACK);
	GetDlgItem(IDC_PP_BINDING_HEADER)->GetWindowRect(controlRect); 
	ScreenToClient(controlRect);
	dc.FillSolidRect(controlRect, RGB(153, 204, 255));
	strOut.LoadString(IDS_PROCESS_BINDING);
	dc.TextOut(controlRect.left + 50, controlRect.top + 10, strOut);

	GetDlgItem(IDC_PP_FOLDING_HEADER)->GetWindowRect(controlRect); 
	ScreenToClient(controlRect);
	dc.FillSolidRect(controlRect, RGB(153, 204, 255));
	strOut.LoadString(IDS_PROCESS_FOLDING);
	dc.TextOut(controlRect.left + 70, controlRect.top + 10, strOut);

	GetDlgItem(IDC_PP_PRINTING_HEADER)->GetWindowRect(controlRect); 
	ScreenToClient(controlRect);
	dc.FillSolidRect(controlRect, RGB(153, 204, 255));
	strOut.LoadString(IDS_PROCESS_PRINTING);
	dc.TextOut(controlRect.left + 50, controlRect.top + 10, strOut);

	GetDlgItem(IDC_PP_PRINTSHEETLIST_HEADER)->GetWindowRect(controlRect); 
	ScreenToClient(controlRect);
	dc.FillSolidRect(controlRect, RGB(153, 204, 255));
	strOut.LoadString(IDS_PRINTSHEETLIST);
	dc.TextOut(controlRect.left + 10, controlRect.top + 8, strOut);

	DrawPrintSheetLayout(&dc);
	DrawFoldSheet(&dc);
	DrawBookBlock(&dc);
}

CPrintSheet* CProductPartsView::GetCurrentPrintSheet()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if (pDoc->m_productPartList.GetSize())
	{
		CFoldSheetLayer* pLayer = GetCurrentFoldSheetLayer();
		return (pLayer) ? pLayer->GetFirstPrintSheet() : NULL;
	}
	return NULL;
}

CLayout* CProductPartsView::GetCurrentLayout()
{
	CPrintSheet* pPrintSheet = GetCurrentPrintSheet();
	if (pPrintSheet)
		return pPrintSheet->GetLayout();
	else
		return NULL;
}

int CProductPartsView::GetCurrentLayoutSide()
{
	return m_nLayoutSide;
}

CFoldSheet* CProductPartsView::GetCurrentFoldSheet(int* pNum)
{
	if (pNum)
		*pNum = 0;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if (pDoc->m_productPartList.GetSize())
	{
		int		 nCurSel = -1;
		POSITION pos	 = m_ppFoldSheetTypeList.GetFirstSelectedItemPosition();
		if (pos)
			nCurSel = m_ppFoldSheetTypeList.GetNextSelectedItem(pos);
		if (nCurSel < 0)
			if (m_foldSheetTypes.GetSize())
				nCurSel = 0;
		if (nCurSel >= m_foldSheetTypes.GetSize())
			nCurSel = 0;

		return (nCurSel >= 0) ? (CFoldSheet*)m_foldSheetTypes[nCurSel] : NULL;
	}
	return NULL;
}

CFoldSheetLayer* CProductPartsView::GetCurrentFoldSheetLayer()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if (pDoc->m_productPartList.GetSize())
	{
		int		 nCurSel = -1;
		POSITION pos	 = m_ppFoldSheetTypeList.GetFirstSelectedItemPosition();
		if (pos)
			nCurSel = m_ppFoldSheetTypeList.GetNextSelectedItem(pos);
		if (nCurSel < 0)
			if (m_ppFoldSheetTypeList.GetItemCount())
				nCurSel = 0;
		if (nCurSel >= m_ppFoldSheetTypeList.GetItemCount())
			nCurSel = 0;

		return (nCurSel >= 0) ? (CFoldSheetLayer*)m_ppFoldSheetTypeList.GetItemData(nCurSel) : NULL;
	}
	return NULL;
}

void CProductPartsView::DrawPrintSheetLayout(CDC* pDC)
{
	CRect frameRect;
	GetDlgItem(IDC_LAYOUT_PREVIEW_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	pDC->FillSolidRect(frameRect, RGB(144, 153, 174));

	CLayout* pLayout = GetCurrentLayout();
	if ( ! pLayout)
		return;
	CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
	if ( ! pPressDevice)
		return;

	float fExtendX = pPressDevice->m_fPlateWidth;
	float fExtendY = pPressDevice->m_fPlateHeight;

	pDC->SaveDC();

	pDC->IntersectClipRect(frameRect);

	frameRect.DeflateRect(10, 10);
    pDC->SetMapMode(MM_ISOTROPIC); 
    pDC->SetWindowExt(CPrintSheetView::WorldToLP(fExtendX), CPrintSheetView::WorldToLP(fExtendY));
	pDC->SetWindowOrg(CPrintSheetView::WorldToLP(0), CPrintSheetView::WorldToLP(fExtendY));
	pDC->SetViewportExt(frameRect.Width(), -frameRect.Height());
	pDC->SetViewportOrg(frameRect.left, frameRect.top);

	pLayout->Draw(pDC, NULL, pPressDevice);

	pDC->RestoreDC(-1);
}

void CProductPartsView::DrawBookBlock(CDC* pDC)
{
	CRect frameRect;
	GetDlgItem(IDC_BOOKBLOCK_PREVIEW_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	pDC->FillSolidRect(frameRect, RGB(144, 153, 174));
	frameRect.DeflateRect(10, 10);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nSel = GetCurrentProductPartIndex();
	if ( (nSel < 0) || (nSel >= pDoc->m_productPartList.GetSize()) )
		return;

	CSize extend = pDoc->m_Bookblock.GetExtend(nSel);

	pDC->SaveDC();

	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(extend);
	pDC->SetWindowOrg(0, extend.cy);
	pDC->SetViewportExt(frameRect.Width(), -frameRect.Height());    
	pDC->SetViewportOrg(frameRect.left, frameRect.top);

	CFoldSheet* pFoldSheet = GetCurrentFoldSheet();
	pDoc->m_Bookblock.Draw(pDC, nSel, (pFoldSheet) ? pFoldSheet->m_strFoldSheetTypeName : "");

	pDC->RestoreDC(-1);
}

void CProductPartsView::DrawFoldSheet(CDC* pDC)
{
	return;

	CRect frameRect;
	GetDlgItem(IDC_FOLDSHEET_PREVIEW_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	pDC->FillSolidRect(frameRect, RGB(144, 153, 174));
	frameRect.DeflateRect(10, 10);

	CFoldSheet* pFoldSheet = GetCurrentFoldSheet();
	if ( ! pFoldSheet)
		return;

//	CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
//	float fExtendX = pPressDevice->m_fPlateWidth;
//	float fExtendY = pPressDevice->m_fPlateHeight;

	pDC->SaveDC();

    pDC->SetMapMode(MM_ISOTROPIC); 
//  pDC->SetWindowExt(CPrintSheetView::WorldToLP(fExtendX), CPrintSheetView::WorldToLP(fExtendY));
//	pDC->SetWindowOrg(CPrintSheetView::WorldToLP(0), CPrintSheetView::WorldToLP(fExtendY));
	pDC->SetViewportExt(frameRect.Width(), -frameRect.Height());
	pDC->SetViewportOrg(frameRect.left, frameRect.top);

//	pLayout->Draw(pDC, pPressDevice);

	pDC->RestoreDC(-1);
}

void CProductPartsView::OnSelchangeProductpartsTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnUpdate(NULL, 0, NULL);
	
	InvalidateRect(m_layoutPreviewFrame);
	InvalidateRect(m_foldSheetPreviewFrame);
	InvalidateRect(m_bookblockPreviewFrame);
	UpdateWindow();
	
	*pResult = 0;
}

void CProductPartsView::OnItemchangedPpFoldsheettypelist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	InvalidateRect(m_layoutPreviewFrame);
	InvalidateRect(m_foldSheetPreviewFrame);
	InvalidateRect(m_bookblockPreviewFrame);
	UpdateWindow();

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = GetCurrentProductPartIndex();
	if (nProductPartIndex < 0)
		if ( ! pDoc->m_productPartList.GetSize())
			return;
		else
			nProductPartIndex = 0;

	CString strMeasureFormat;
	strMeasureFormat.Format("%s x %s", (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
	m_strPageFormat.Format(strMeasureFormat, MeasureFormat((float)pDoc->m_productPartList[nProductPartIndex].m_fPageWidth), MeasureFormat((float)pDoc->m_productPartList[nProductPartIndex].m_fPageHeight));

	CLayout* pLayout = GetCurrentLayout();
	if (pLayout)
	{
		m_strPrintLayout = pLayout->m_strLayoutName;
		CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
		if (pPressDevice)
		{
			m_strPrintMachine = pPressDevice->m_strName;
			m_strPlateFormat.Format((LPCTSTR)strMeasureFormat, MeasureFormat((float)pPressDevice->m_fPlateWidth), MeasureFormat((float)pPressDevice->m_fPlateHeight));
		}
		m_strPaperFormat.Format(strMeasureFormat, MeasureFormat((float)pLayout->m_FrontSide.m_fPaperWidth), MeasureFormat((float)pLayout->m_FrontSide.m_fPaperHeight));
		m_strMarkset	  = "";
		m_strPrintingType = pLayout->GetPrintingType();
		m_strTurningType  = pLayout->GetTurningType();
	}
	else
	{
		m_strPrintLayout  = "";
		m_strPrintMachine = "";
		m_strPlateFormat  = "";
		m_strPaperFormat  = "";
		m_strMarkset	  = "";
		m_strPrintingType = "";
		m_strTurningType  = "";
	}

	int			nNum		= 0;
	CFoldSheet*	pFoldSheet  = GetCurrentFoldSheet(&nNum);
	if (pFoldSheet)
	{
/*		if (pFoldSheet->m_LayerList.GetCount() > 1)
		{
			CFoldSheetLayer* pLayer	= GetCurrentFoldSheetLayer();
			m_strFoldSheet.Format("%s (%c)", pLayer->GetWebName(pFoldSheet));
		}
		else
*/
			m_strFoldSheet = pFoldSheet->m_strFoldSheetTypeName;
			if (fabs(pFoldSheet->m_fShinglingValue) > 0.001f)
				m_strShinglingHead.Format("%.2f", pFoldSheet->m_fShinglingValue);
			else
				m_strShinglingHead = "";

			if (fabs(pFoldSheet->m_fShinglingValueFoot) > 0.001f)
				m_strShinglingFoot.Format("%.2f", pFoldSheet->m_fShinglingValueFoot);
			else
				if (fabs(pFoldSheet->m_fShinglingValue) > 0.001f)
					m_strShinglingFoot = m_strShinglingHead;
				else
					m_strShinglingFoot = "";
	}
	else
	{
		m_strFoldSheet	   = "";
		m_strShinglingHead = "";
		m_strShinglingFoot = "";
	}

	m_nFoldSheetTypeNum = nNum;

	m_strNumPages.Format(" %d", pDoc->m_productPartList[nProductPartIndex].m_nNumPages);

	UpdateData(FALSE);

	*pResult = 0;
}

void CProductPartsView::OnItemchangedPpFoldsheetlist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	*pResult = 0;
	
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if ( ! pDoc->m_productPartList.GetSize())
		return;

	int		 nCurSel = -1;
	POSITION pos	 = m_ppFoldSheetList.GetFirstSelectedItemPosition();
	if (pos)
		nCurSel = m_ppFoldSheetList.GetNextSelectedItem(pos);
	if (nCurSel < 0)
		if (m_ppFoldSheetList.GetItemCount())
			nCurSel = 0;
	if (nCurSel >= m_ppFoldSheetList.GetItemCount())
		nCurSel = 0;
	if (nCurSel < 0)
		return;

	CFoldSheet* pFoldSheet = (CFoldSheet*)m_ppFoldSheetList.GetItemData(nCurSel);

	for (int i = 0; i < m_foldSheetTypes.GetSize(); i++)
	{
		if (m_foldSheetTypes[i]->m_strFoldSheetTypeName == pFoldSheet->m_strFoldSheetTypeName)
		{
			m_ppFoldSheetTypeList.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
			break;
		}
	}
}

void CProductPartsView::UpdateAll()
{
	OnUpdate(NULL, 0, NULL);
	InvalidateRect(m_layoutPreviewFrame);
	InvalidateRect(m_foldSheetPreviewFrame);
	InvalidateRect(m_bookblockPreviewFrame);
	UpdateWindow();

	theApp.OnUpdateView(RUNTIME_CLASS(CProductDataView),	NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CProductDataView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CBookBlockView),		NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CBookBlockDetailView),NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockDetailView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),		NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CProductDataView),	NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CProductDataView));	

	UpdateData(FALSE);
}

void CProductPartsView::OnDeltaposPpNumfoldsheetSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	*pResult = 0;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	int nSel = GetCurrentProductPartIndex();
	if (nSel < 0)
		return;

	int nNumOld = 0;
	CFoldSheet* pFoldSheet = GetCurrentFoldSheet(&nNumOld);
	if ( ! pFoldSheet)
		return;

	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int nNum = nNumOld + pNMUpDown->iDelta;
	if ((nNum < 0) || (nNum > 999))
		return;

	int nPages = 0;
	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
		nPages = pFoldSheet->m_nPagesLeft;
	else
		nPages = pFoldSheet->m_nPagesLeft + pFoldSheet->m_nPagesRight;

	if (pNMUpDown->iDelta > 0)
		if (nPages > atoi(m_strPagesNotAssigned))	// too many sheets
			return;

	if (nNumOld < nNum)
		pDoc->m_Bookblock.AddFoldSheets(pFoldSheet, nNum - nNumOld, nSel);
	else
		pDoc->m_Bookblock.RemoveFoldSheets(pFoldSheet, nNumOld - nNum, nSel);
	pDoc->m_Bookblock.Reorganize();
	pDoc->SetModifiedFlag(TRUE);
	UpdateAll();
}

void CProductPartsView::OnPpShingling() 
{
	if ( ! theApp.m_pDlgShinglingAssistent->m_hWnd)
		theApp.m_pDlgShinglingAssistent->Create(IDD_SHINGLING_ASSISTENT, this);
	else
	{
		if (theApp.m_pDlgShinglingAssistent->m_pParentWnd != this)
		{
			theApp.m_pDlgShinglingAssistent->DestroyWindow();
			theApp.m_pDlgShinglingAssistent->Create(IDD_SHINGLING_ASSISTENT, this);
		}
	}
}

void CProductPartsView::OnPpAddFoldsheet() 
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = GetCurrentProductPartIndex();
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	int	nRestPages = atoi((LPCTSTR)m_strPagesNotAssigned);
	if ( ! nRestPages)
		return;

	CPrintSheet*  pPrintSheet  = GetCurrentPrintSheet();
	CFoldSheet*   pFoldSheet   = GetCurrentFoldSheet();
	CLayout*	  pLayout	   = GetCurrentLayout();
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;

	if ( ! pLayout)
	{
		pLayout = new CLayout;
		pLayout->Create();
	}
	if ( ! pPrintSheet)
	{
		pPrintSheet = new CPrintSheet;
		pPrintSheet->Create(-1, pLayout);
	}
	if ( ! pFoldSheet)
	{
		pFoldSheet = new CFoldSheet;
		pFoldSheet->Create(0, 0);
	}
	if ( ! pPressDevice)
	{
		pPressDevice = new CPressDevice;
	}

	CString strFile;
	if (pFoldSheet)
		strFile = pFoldSheet->m_strFoldSheetTypeName;
	else
		strFile = "";
//		strFile.LoadString(IDS_DEFAULTNAME_TEXT);

	CDlgImpositionScheme dlg(TRUE, "isc", (LPCTSTR)strFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
							 " |*.isc|", NULL, NULL);	// 'this' pointer gives DlgImpositionScheme access to BindingAssistent

	dlg.m_nProductPartIndex = GetCurrentProductPartIndex();
	if (pFoldSheet)
		dlg.m_foldSheet   = *pFoldSheet;
	if (pPrintSheet)
		dlg.m_printSheet  = *pPrintSheet;
	if (pLayout)
		dlg.m_layout	  = *pLayout;
	if (pPressDevice)
		dlg.m_pressDevice = *pPressDevice;

	dlg.m_bStandAlone = TRUE;
	if (dlg.DoModal() == IDCANCEL)
		return;

	dlg.m_foldSheet.m_nProductPartIndex = nProductPartIndex;

	int nPages = 0;
	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
		nPages = dlg.m_foldSheet.m_nPagesLeft;
	else
		nPages = dlg.m_foldSheet.m_nPagesLeft + dlg.m_foldSheet.m_nPagesRight;
	int	nSheets	= nRestPages / nPages;
	if (nSheets == 0)	
	{
		AfxMessageBox("Nicht gen�gend unausgeschossene Seiten f�r diesen Falzbogen!", MB_OK);
		return;
	}

	m_strMarkset = dlg.m_strPrintMarkset;

	pDoc->m_Bookblock.AddFoldSheets(&dlg.m_foldSheet, nSheets, nProductPartIndex);
	pDoc->m_Bookblock.Reorganize(&dlg.m_printSheet, &dlg.m_layout);
	pDoc->SetModifiedFlag(TRUE);
	UpdateAll();
}

void CProductPartsView::OnPpRemoveFoldsheet() 
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	int nSel = GetCurrentProductPartIndex();
	if (nSel < 0)
		return;

	if (AfxMessageBox(IDS_DELETE_SELECTIONS_WARNING, MB_YESNO) == IDNO)
		return;

	CFoldSheet* pFoldSheet = GetCurrentFoldSheet();
	pDoc->m_Bookblock.RemoveFoldSheets(pFoldSheet, INT_MAX, nSel);	// INT_MAX = remove all of this type
	pDoc->m_Bookblock.Reorganize();
	pDoc->SetModifiedFlag(TRUE);
	UpdateAll();
}

void CProductPartsView::OnPpModifyLayout() 
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = GetCurrentProductPartIndex();
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CPrintSheet*  pPrintSheet  = GetCurrentPrintSheet();
	CFoldSheet*   pFoldSheet   = GetCurrentFoldSheet();
	CLayout*	  pLayout	   = GetCurrentLayout();
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;

	if ( ! pPrintSheet || ! pFoldSheet || ! pLayout || ! pPressDevice)
		return;

	CString strFile;
	if (pFoldSheet)
		strFile = pFoldSheet->m_strFoldSheetTypeName;
	else
		strFile.LoadString(IDS_DEFAULTNAME_TEXT);

	CDlgPrintSheetLayout dlg;

	if (pFoldSheet)
		dlg.m_foldSheet   = *pFoldSheet;
	if (pPrintSheet)
		dlg.m_printSheet  = *pPrintSheet;
	if (pLayout)
		dlg.m_layout	  = *pLayout;
	if (pPressDevice)
		dlg.m_pressDevice = *pPressDevice;

	if (dlg.DoModal() == IDCANCEL)
		return;

	*pLayout = dlg.m_layout;
///// change pressdevice
	int	nIndex = -1;
	if (pDoc->m_PrintSheetList.m_Layouts.GetCount())
		nIndex = theApp.m_PressDeviceList.GetDeviceIndex(dlg.m_pressDevice.m_strName);
	CDlgPressDevice::SetPressDevice(BOTHSIDES, nIndex);

	pDoc->SetModifiedFlag(TRUE);
	UpdateAll();
}

void CProductPartsView::OnSelchangePpBindingMethod() 
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = GetCurrentProductPartIndex();
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	switch(m_nBindingMethod)
	{
	case 0:	 pDoc->m_Bookblock.ModifyBindingMethod(CFinalProductDescription::PerfectBound,		nProductPartIndex); break;
	case 1:	 pDoc->m_Bookblock.ModifyBindingMethod(CFinalProductDescription::SaddleStitched,	nProductPartIndex); break;
	default: return;
	}

	pDoc->m_productPartList[nProductPartIndex].m_nBindingMethod = m_nBindingMethod;

	pDoc->m_Bookblock.Reorganize();
	pDoc->SetModifiedFlag(TRUE);
	UpdateAll();
}

HBRUSH CProductPartsView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);
	
    if(nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_PP_PRINTLAYOUT:
		case IDC_PP_FOLDSHEET:
		case IDC_BOOKBLOCK_HEADER:
				pDC->SelectObject(&m_smallFont);
				pDC->SetBkMode(TRANSPARENT);
				hbr = (HBRUSH)m_BkBrush.GetSafeHandle();
				break;

		case IDC_PP_NOTASSIGNED_PAGES:
				if (atoi(m_strPagesNotAssigned) != 0)
				{
					pDC->SetBkMode(TRANSPARENT);
					hbr = (HBRUSH)m_BkBrush.GetSafeHandle();
					break;
				}
		default:
				pDC->SetBkMode(TRANSPARENT);
				CRect rc;
				pWnd->GetWindowRect(rc);
				m_themedTabCtrl.MapWindowPoints(pWnd, rc);
				::SetBrushOrgEx(pDC->m_hDC, -rc.left, -rc.top, NULL);
				hbr = m_themedTabCtrl.m_hBrush;
				break;
		}
    }

	return hbr;
}

void CProductPartsView::OnPpAddFoldsheetButton() 
{
	CFoldSheet* pFoldSheet = GetCurrentFoldSheet();
	if ( ! pFoldSheet)
		return;

	int nProductPartIndex = GetCurrentProductPartIndex();
	if (nProductPartIndex < 0)
		return;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nPages = 0;
	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
		nPages = pFoldSheet->m_nPagesLeft;
	else
		nPages = pFoldSheet->m_nPagesLeft + pFoldSheet->m_nPagesRight;

	if (nPages > atoi(m_strPagesNotAssigned))	// too many sheets
		return;

	pDoc->m_Bookblock.AddFoldSheets(pFoldSheet, 1, nProductPartIndex);
	pDoc->m_Bookblock.Reorganize();
	pDoc->SetModifiedFlag(TRUE);
	UpdateAll();
}

void CProductPartsView::OnPpRemoveFoldsheetButton() 
{
	CFoldSheet* pFoldSheet = GetCurrentFoldSheet();
	if ( ! pFoldSheet)
		return;

	int nProductPartIndex = GetCurrentProductPartIndex();
	if (nProductPartIndex < 0)
		return;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	pDoc->m_Bookblock.RemoveFoldSheets(pFoldSheet, 1, nProductPartIndex);
	pDoc->m_Bookblock.Reorganize();
	pDoc->SetModifiedFlag(TRUE);
	UpdateAll();
}

void CProductPartsView::OnPpBookblockDetails() 
{
	CMainFrame frame;
	CSummaryFrame* pFrame = frame.CreateOrActivateSummaryFrame(theApp.m_pBookBlockTemplate, CImpManDoc::GetDoc());
}

void CProductPartsView::OnPpPrintsheetDetails() 
{
	CPrintSheet* pPrintSheet = GetCurrentPrintSheet();
	if ( ! pPrintSheet)
		return;

	CMainFrame frame;
	CPrintSheetFrame* pFrame = frame.CreateOrActivatePrintSheetFrame(theApp.m_pPrintSheetTemplate, CImpManDoc::GetDoc());

	pFrame->OpenDetailedView(pPrintSheet, CPrintSheetFrame::SubViewMarks);
}
