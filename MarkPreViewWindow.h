// MarkPreViewWindow.h : header file
//

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMarkPreViewWindow window

class CMarkPreViewWindow : public CStatic
{
// Construction
public:
	CMarkPreViewWindow();

// Attributes
public:
	CScrollBar* m_pScrollBarHeight;
	CScrollBar* m_pScrollBarWidth;

	CMarkList*  m_pMarkList;

	BOOL m_bOnSetCursor;
	BOOL m_bLockWindow;

	CPoint mScrollPos;	// Must have access from CDlgSetMarks
	CSize  mScrollSize; //		"""
	float  m_fZoomFactor;
	int	   m_nBorder;

protected:
	float m_fScale;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMarkPreViewWindow)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMarkPreViewWindow();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMarkPreViewWindow)
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

