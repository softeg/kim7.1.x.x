// DlgFanout.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgFanout.h"
#include "DlgPrintColorTable.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgFanout 

CDlgFanout::CDlgFanout(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgFanout::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgFanout)
	m_fFanoutX = 0.0f;
	m_fFanoutY = 0.0f;
	//}}AFX_DATA_INIT
}


void CDlgFanout::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgFanout)
	DDX_Control(pDX, IDC_FANOUT_SPIN_Y, m_FanoutSpinY);
	DDX_Control(pDX, IDC_FANOUT_SPIN_X, m_FanoutSpinX);
	DDX_Measure(pDX, IDC_FANOUT_EDIT_X, m_fFanoutX);
	DDX_Measure(pDX, IDC_FANOUT_EDIT_Y, m_fFanoutY);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgFanout, CDialog)
	//{{AFX_MSG_MAP(CDlgFanout)
	ON_BN_CLICKED(IDC_NEW_COLOR_BUTTON, OnNewColorButton)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FANOUT_SPIN_X, OnDeltaposFanoutSpinX)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FANOUT_SPIN_Y, OnDeltaposFanoutSpinY)
	ON_LBN_SELCHANGE(IDC_JOB_COLORS, OnSelchangeJobColors)
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	ON_EN_CHANGE(IDC_FANOUT_EDIT_X, OnChangeFanoutEditX)
	ON_EN_CHANGE(IDC_FANOUT_EDIT_Y, OnChangeFanoutEditY)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgFanout 

BOOL CDlgFanout::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString strTitle;
	GetWindowText(strTitle);
	strTitle+= " - " + m_strPaperName;
	SetWindowText(strTitle);

	// subclass the control
	VERIFY(m_JobColorListBox.SubclassDlgItem(IDC_JOB_COLORS, this));
//	m_JobColorListBox.m_ColorDefTable.Load();
	m_JobColorListBox.m_ColorDefTable.Append(theApp.m_colorDefTable);

	for (int i = 0; i < m_JobColorListBox.m_ColorDefTable.GetSize(); i++)
		m_JobColorListBox.AddString((LPCTSTR)0);

	if (m_JobColorListBox.GetCount() > 0)
	{
		m_JobColorListBox.SetCurSel(0);	
		CColorDefinition& rColorDef = m_JobColorListBox.m_ColorDefTable[0];
		m_strColorName = rColorDef.m_strColorName;

		m_fFanoutX = m_FanoutList.GetFanoutX(m_strColorName);
		m_fFanoutY = m_FanoutList.GetFanoutY(m_strColorName);
	}

	m_FanoutSpinX.SetRange(0, 1);  
	m_FanoutSpinY.SetRange(0, 1);  

	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow(FALSE);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgFanout::OnNewColorButton() 
{
	CDlgPrintColorTable dlg;
	if (dlg.DoModal() == IDCANCEL)
		return;

	//Clear JobColorListBox
	int nCount = m_JobColorListBox.GetCount();
	for (int i = 0; i < nCount; i++)
		m_JobColorListBox.DeleteColorItem(i);

	//Refill JobColorListBox
	m_JobColorListBox.m_ColorDefTable.Load();
	for (int i = 0; i < m_JobColorListBox.m_ColorDefTable.GetSize(); i++)
		m_JobColorListBox.AddString((LPCTSTR)0);

	if (m_JobColorListBox.GetCount() > 0)
	{
		m_JobColorListBox.SetCurSel(0);	
		CColorDefinition& rColorDef = m_JobColorListBox.m_ColorDefTable[0];
		m_strColorName = rColorDef.m_strColorName;
	}

	UpdateData(FALSE);
}

void CDlgFanout::OnChangeFanoutEditX() 
{
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow(TRUE);
}

void CDlgFanout::OnChangeFanoutEditY() 
{
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow(TRUE);
}

void CDlgFanout::OnDeltaposFanoutSpinX(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_FanoutSpinX.GetBuddy(), pNMUpDown->iDelta, 0.1f);
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow(TRUE);
	
	*pResult = 0;
}

void CDlgFanout::OnDeltaposFanoutSpinY(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_FanoutSpinY.GetBuddy(), pNMUpDown->iDelta, 0.1f);
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow(TRUE);
	
	*pResult = 0;
}

void CDlgFanout::OnSelchangeJobColors() 
{
	int nSel;
	if ((nSel = m_JobColorListBox.GetCurSel()) == LB_ERR)
		return;

	CColorDefinition& rColorDef = m_JobColorListBox.m_ColorDefTable[nSel];
	m_strColorName = rColorDef.m_strColorName;

	m_fFanoutX = m_FanoutList.GetFanoutX(m_strColorName);
	m_fFanoutY = m_FanoutList.GetFanoutY(m_strColorName);
	
	UpdateData(FALSE);
}

void CDlgFanout::OnApplyButton() 
{
	UpdateData(TRUE);

	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_JOB_COLORS)->SetFocus();

	int nIndex = m_FanoutList.FindColor(m_strColorName);
	if (nIndex == -1)
	{
		if ((m_fFanoutX != 0.0f) || (m_fFanoutY != 0.0f))
		{
			CFanoutDefinition fanoutDef(m_strColorName, m_fFanoutX, m_fFanoutY);
			m_FanoutList.Add(fanoutDef);
		}
	}
	else
	{
		if ((m_fFanoutX != 0.0f) || (m_fFanoutY != 0.0f))
		{
			m_FanoutList[nIndex].m_fFanoutX = m_fFanoutX;
			m_FanoutList[nIndex].m_fFanoutY = m_fFanoutY;
		}
		else
			m_FanoutList.RemoveAt(nIndex);
	}
}
