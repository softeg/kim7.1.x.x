#if !defined(AFX_DLGTOOLBARPOPUP_H__63E593C3_F9AF_11D3_8799_0040F68C1EF7__INCLUDED_)
#define AFX_DLGTOOLBARPOPUP_H__63E593C3_F9AF_11D3_8799_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgToolBarPopup.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgToolBarPopup 

class CDlgToolBarPopup : public CDialog
{
// Konstruktion
public:
	CDlgToolBarPopup(CPoint ptDlgTopLeft, int nIDColdToolBar, int nIDHotToolBar, CArray <int, int>&	stringIDs, CSize sizeBitmap, int nBitmaps, int nRows, int nTBStyle, CWnd* pParent = NULL);   // Standardkonstruktor


protected:
	CWnd*				m_pParent;
	CPoint				m_ptDlgTopLeft;
	CToolBarCtrl		m_toolBarCtrl;
	TBBUTTON*			m_pTBButtons;
	int					m_nIDColdToolBar, m_nIDHotToolBar;
	int					m_nBitmaps;
	CSize				m_sizeBitmap;
	int					m_nTBStyle;
	int					m_nRows;
	CArray <int, int>	m_stringIDs;


// Dialogfelddaten
	//{{AFX_DATA(CDlgToolBarPopup)
	enum { IDD = IDD_TOOLBAR_POPUP };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgToolBarPopup)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgToolBarPopup)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGTOOLBARPOPUP_H__63E593C3_F9AF_11D3_8799_0040F68C1EF7__INCLUDED_
