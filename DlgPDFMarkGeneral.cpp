// DlgPDFMarkGeneral.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "ObjectPivotControl.h"
#include "DlgMoveObjects.h"
#include "DlgPDFMarkGeneral.h"
#include "DlgMarkPositioning.h"
#include "DlgMarkProperties.h"
#include "DlgChangeFormats.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PageListView.h"
#include "PageListTableView.h"
#ifdef PDF
#include "PDFReader.h"
#endif
#include "PageListDetailView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFMarkGeneral 


CDlgPDFMarkGeneral::CDlgPDFMarkGeneral(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPDFMarkGeneral::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPDFMarkGeneral)
	m_fMarkHeight = 0.0f;
	m_fMarkWidth = 0.0f;
	//}}AFX_DATA_INIT
}


void CDlgPDFMarkGeneral::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPDFMarkGeneral)
	DDX_Control(pDX, IDC_WIDTH_SPIN, m_widthSpin);
	DDX_Control(pDX, IDC_HEIGHT_SPIN, m_heightSpin);
	DDX_Measure(pDX, IDC_MARK_HEIGHT, m_fMarkHeight);
	DDX_Measure(pDX, IDC_MARK_WIDTH, m_fMarkWidth);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPDFMarkGeneral, CDialog)
	//{{AFX_MSG_MAP(CDlgPDFMarkGeneral)
	ON_NOTIFY(UDN_DELTAPOS, IDC_HEIGHT_SPIN, OnDeltaposHeightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_WIDTH_SPIN, OnDeltaposWidthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_MARK_ORIENTATION_SPIN, OnDeltaposMarkOrientationSpin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPDFMarkGeneral 

BOOL CDlgPDFMarkGeneral::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_widthSpin.SetRange (0, 1);  
	m_heightSpin.SetRange(0, 1);  

	if (GetMarkObject())
	{
		m_fMarkWidth	= GetMarkObject()->GetWidth();
		m_fMarkHeight	= GetMarkObject()->GetHeight();
		m_nHeadPosition = GetMarkObject()->m_nHeadPosition;
	}
	
	int nIDBitmap;
	switch (m_nHeadPosition)
	{
	case LEFT:	 nIDBitmap = IDB_PORTRAIT_90;  break;
	case BOTTOM: nIDBitmap = IDB_PORTRAIT_180; break;
	case RIGHT:	 nIDBitmap = IDB_PORTRAIT_270; break;
	case TOP:	 nIDBitmap = IDB_PORTRAIT;	   break;
	default:	 nIDBitmap = -1;			   break;
	}
	if ((HBITMAP)m_bmMarkOrientation)
		m_bmMarkOrientation.DeleteObject();
	m_bmMarkOrientation.LoadBitmap(nIDBitmap); 
	((CStatic*)GetDlgItem(IDC_MARK_ORIENTATION))->SetBitmap((HBITMAP)m_bmMarkOrientation);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

CLayoutObject* CDlgPDFMarkGeneral::GetMarkObject()
{
	if ( ! m_pParent)
		return NULL;
	if ( ! m_pParent->m_pMarkObject)
		return NULL;

	return m_pParent->m_pMarkObject;
}

void CDlgPDFMarkGeneral::OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_widthSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgPDFMarkGeneral::OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_heightSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgPDFMarkGeneral::OnDeltaposMarkOrientationSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int nIDBitmap;
	if (pNMUpDown->iDelta < 0)
	{
		switch (m_nHeadPosition)
		{
		case TOP:	 nIDBitmap = IDB_PORTRAIT_90;  m_nHeadPosition = LEFT;	break;
		case LEFT:	 nIDBitmap = IDB_PORTRAIT_180; m_nHeadPosition = BOTTOM;break;
		case BOTTOM: nIDBitmap = IDB_PORTRAIT_270; m_nHeadPosition = RIGHT;	break;
		case RIGHT:  nIDBitmap = IDB_PORTRAIT;	   m_nHeadPosition = TOP;	break;
		default:	 nIDBitmap = -1;			   m_nHeadPosition = TOP;	break;
		}
	}
	else
	{
		switch (m_nHeadPosition)
		{
		case TOP:	 nIDBitmap = IDB_PORTRAIT_270; m_nHeadPosition = RIGHT;	break;
		case LEFT:	 nIDBitmap = IDB_PORTRAIT;	   m_nHeadPosition = TOP;	break;
		case BOTTOM: nIDBitmap = IDB_PORTRAIT_90;  m_nHeadPosition = LEFT;	break;
		case RIGHT:  nIDBitmap = IDB_PORTRAIT_180; m_nHeadPosition = BOTTOM;break;
		default:	 nIDBitmap = -1;			   m_nHeadPosition = TOP;	break;
		}
	}

	if ((HBITMAP)m_bmMarkOrientation)
		m_bmMarkOrientation.DeleteObject();
	m_bmMarkOrientation.LoadBitmap(nIDBitmap); 
	((CStatic*)GetDlgItem(IDC_MARK_ORIENTATION))->SetBitmap((HBITMAP)m_bmMarkOrientation);

	*pResult = 1;
}

void CDlgPDFMarkGeneral::Apply() 
{
	if ( ! GetMarkObject())
		return;

	UpdateData(TRUE);
	
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CLayoutObject* pCounterpartObject = (theApp.settings.m_bFrontRearParallel) ? GetMarkObject()->FindCounterpartObject() : NULL;	

		GetMarkObject()->m_LeftNeighbours.GetSize();			// To start an analyzing-process
		if (pCounterpartObject)
			pCounterpartObject->m_LeftNeighbours.GetSize();	// To start an analyzing-process for the other layoutside

		GetMarkObject()->m_nHeadPosition = m_nHeadPosition;

		float fDiffX, fDiffY;
		fDiffX = (GetMarkObject()->GetWidth()  - m_fMarkWidth)  / 2.0f;
		fDiffY = (GetMarkObject()->GetHeight() - m_fMarkHeight) / 2.0f;
		GetMarkObject()->SetWidth(m_fMarkWidth);
		GetMarkObject()->SetHeight(m_fMarkHeight);

		GetMarkObject()->SetLeft  (GetMarkObject()->GetLeft()	+ fDiffX, FALSE);
		GetMarkObject()->SetRight (GetMarkObject()->GetRight()	- fDiffX, FALSE);
		GetMarkObject()->SetBottom(GetMarkObject()->GetBottom()	+ fDiffY, FALSE);
		GetMarkObject()->SetTop	(GetMarkObject()->GetTop()	- fDiffY, FALSE);

		CDlgChangeFormats::RecalcObject(GetMarkObject(), LEFT);
		CDlgChangeFormats::RecalcObject(GetMarkObject(), RIGHT);
		CDlgChangeFormats::RecalcObject(GetMarkObject(), TOP);
		CDlgChangeFormats::RecalcObject(GetMarkObject(), BOTTOM);

		if (pCounterpartObject)
		{
			switch (pCounterpartObject->GetLayout()->KindOfProduction())
			{
			case WORK_AND_TURN  : pCounterpartObject->m_nHeadPosition = (m_nHeadPosition == LEFT) ? RIGHT  : (m_nHeadPosition == RIGHT)  ? LEFT : m_nHeadPosition; break;
        	case WORK_AND_TUMBLE: pCounterpartObject->m_nHeadPosition = (m_nHeadPosition == TOP)  ? BOTTOM : (m_nHeadPosition == BOTTOM) ? TOP  : m_nHeadPosition; break;
        	}

			pCounterpartObject->SetLeft  (pCounterpartObject->GetLeft()		+ fDiffX, FALSE);
			pCounterpartObject->SetRight (pCounterpartObject->GetRight()	- fDiffX, FALSE);
			pCounterpartObject->SetBottom(pCounterpartObject->GetBottom()	+ fDiffY, FALSE);
			pCounterpartObject->SetTop	 (pCounterpartObject->GetTop()		- fDiffY, FALSE);
			CDlgChangeFormats::RecalcObject(pCounterpartObject, LEFT);
			CDlgChangeFormats::RecalcObject(pCounterpartObject, RIGHT);
			CDlgChangeFormats::RecalcObject(pCounterpartObject, TOP);
			CDlgChangeFormats::RecalcObject(pCounterpartObject, BOTTOM);
		}

/*		if (m_nMarkType == CMark::TextMark)
		{
			CPageTemplate* pMarkTemplate = GetMarkObject()->GetCurrentMarkTemplate();
			if (pMarkTemplate)
				pMarkTemplate->SetMarkText(m_strOutputText);
		}*/

		pDoc->SetModifiedFlag();

		theApp.OnUpdateView(RUNTIME_CLASS(CPageListView),		NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListDetailView), NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListTableView),	NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),		NULL, 0, NULL);
	}
}
