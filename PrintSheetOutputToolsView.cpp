// PrintSheetOutputToolsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PrintSheetOutputToolsView.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "NewPrintSheetFrame.h"


// CPrintSheetOutputToolsView

IMPLEMENT_DYNCREATE(CPrintSheetOutputToolsView, CFormView)

CPrintSheetOutputToolsView::CPrintSheetOutputToolsView()
	: CFormView(CPrintSheetOutputToolsView::IDD)
{

}

CPrintSheetOutputToolsView::~CPrintSheetOutputToolsView()
{
}

void CPrintSheetOutputToolsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPrintSheetOutputToolsView, CFormView)
	ON_UPDATE_COMMAND_UI(ID_ZOOM, OnUpdateZoom)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_DECREASE, OnUpdateZoomDecrease)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_FULLVIEW, OnUpdateZoomFullview)
	ON_UPDATE_COMMAND_UI(ID_SCROLLHAND, OnUpdateScrollHand)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FRONTSIDE, OnUpdateViewFrontside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BACKSIDE, OnUpdateViewBackside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BOTHSIDES, OnUpdateViewBothsides)
	ON_UPDATE_COMMAND_UI(IDC_FOLDSHEETCHECK, OnUpdateFoldsheetCheck)
	ON_UPDATE_COMMAND_UI(IDC_MEASURECHECK, OnUpdateMeasureCheck)
	ON_UPDATE_COMMAND_UI(IDC_EXPOSURECHECK, OnUpdateExposuresCheck)
	ON_UPDATE_COMMAND_UI(IDC_RELEASE_BLEED, OnUpdateReleaseBleed)
	ON_UPDATE_COMMAND_UI(IDC_PLATECHECK, OnUpdatePlateCheck)
	ON_UPDATE_COMMAND_UI(IDC_BITMAPCHECK, OnUpdateBitmapCheck)
END_MESSAGE_MAP()


// CPrintSheetOutputToolsView-Diagnose

#ifdef _DEBUG
void CPrintSheetOutputToolsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetOutputToolsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrintSheetOutputToolsView-Meldungshandler

void CPrintSheetOutputToolsView::OnInitialUpdate()
{
	m_pNewPrintSheetFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! m_pNewPrintSheetFrame)
		return;

	m_zoomToolBarGroup.SubclassDlgItem(IDC_PRINTTOOL_ZOOM_FRAME,	this);
	m_displayToolBarGroup.SubclassDlgItem(IDC_PRINTTOOL_DISPLAY_FRAME,	this);

	CFormView::OnInitialUpdate(); 

	InitializeToolbars();
}

void CPrintSheetOutputToolsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	InitializeToolbars();
}

void CPrintSheetOutputToolsView::InitializeToolbars()
{
	CString string;
	string.LoadString(IDS_ZOOM);
	m_zoomToolBarGroup.SetWindowText(string);
	if (m_zoomToolBar.m_pWndParent)
		m_zoomToolBar.RemoveAll();
	else
		m_zoomToolBar.Create(IDC_PRINTTOOL_ZOOM_FRAME, ODBL_VERTICAL, this, GetParentFrame(), NULL, IDB_ZOOM_TOOLBAR, IDB_ZOOM_TOOLBAR_COLD, CSize(40, 26), FALSE);
	if (m_pNewPrintSheetFrame->GetActSight() != ALLSIDES)
	{
		CRect rcWin;
		m_zoomToolBarGroup.GetWindowRect(rcWin); 
		ScreenToClient(rcWin); rcWin.left += 5; rcWin.top += 25;
		CRect rcButton; rcButton = CRect(rcWin.TopLeft(), CSize(40, 30)); 

		m_zoomToolBar.AddButton(-1, IDS_ZOOMIN_TOOLTIP,		ID_ZOOM,			rcButton, 0); rcButton.OffsetRect(  rcButton.Width() + 10, 0);
		m_zoomToolBar.AddButton(-1,	IDS_ZOOMOUT_TOOLTIP,	ID_ZOOM_DECREASE,	rcButton, 1); rcButton.OffsetRect(-(rcButton.Width() + 10), rcButton.Height() + 5);
		m_zoomToolBar.AddButton(-1,	IDS_ZOOMALL_TOOLTIP,	ID_ZOOM_FULLVIEW,	rcButton, 2); rcButton.OffsetRect(  rcButton.Width() + 10, 0);
		m_zoomToolBar.AddButton(-1,	IDS_ZOOMMOVE_TOOLTIP,	ID_SCROLLHAND,		rcButton, 3);
	}

	string.LoadString(IDS_SHOW);
	m_zoomToolBarGroup.SetWindowText(string);
	m_displayToolBarGroup.SetWindowText(string);
	if (m_displayToolBar.m_pWndParent)
		m_displayToolBar.RemoveAll();
	else
		m_displayToolBar.Create(IDC_PRINTTOOL_DISPLAY_FRAME, ODBL_VERTICAL, this, GetParentFrame(), NULL, IDB_FOLDPRINTVIEW_TOOLBAR, IDB_FOLDPRINTVIEW_TOOLBAR_COLD, CSize(40, 26), FALSE);
	m_displayToolBar.AddButton(-1, IDS_FOLDSHEET_LABEL,	IDC_FOLDSHEETCHECK, 35, 3);
	m_displayToolBar.AddButton(-1, IDS_MEASURING,	IDC_MEASURECHECK,	35, 4);
	m_displayToolBar.AddButton(-1, IDS_MASKING,		IDC_EXPOSURECHECK,	35, 13);
	m_displayToolBar.AddButton(-1, IDS_CONTENTS,	IDC_BITMAPCHECK,	35, 6);
}

BOOL CPrintSheetOutputToolsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_zoomToolBar.m_pToolTip)            
		m_zoomToolBar.m_pToolTip->RelayEvent(pMsg);	
	if (m_displayToolBar.m_pToolTip)            
		m_displayToolBar.m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

void CPrintSheetOutputToolsView::OnUpdateZoom(CCmdUI* pCmdUI) 
{
	if (m_pNewPrintSheetFrame->m_nViewMode == ID_ZOOM)
		COwnerDrawnButtonList::SetCheck(pCmdUI, TRUE);
	else
		COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetOutputToolsView::OnUpdateZoomDecrease(CCmdUI* pCmdUI) 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (m_pNewPrintSheetFrame->m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetOutputToolsView::OnUpdateZoomFullview(CCmdUI* pCmdUI) 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (m_pNewPrintSheetFrame->m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetOutputToolsView::OnUpdateScrollHand(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bHandActive);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (m_pNewPrintSheetFrame->m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		m_pNewPrintSheetFrame->m_bHandActive = FALSE;
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		m_pNewPrintSheetFrame->m_bHandActive = FALSE;
	}
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetOutputToolsView::OnUpdateViewFrontside(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_nViewSide == FRONTSIDE);
}

void CPrintSheetOutputToolsView::OnUpdateViewBackside(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_nViewSide == BACKSIDE);
}

void CPrintSheetOutputToolsView::OnUpdateViewBothsides(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_nViewSide == BOTHSIDES);
}

void CPrintSheetOutputToolsView::OnUpdateFoldsheetCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowFoldSheet);
}

void CPrintSheetOutputToolsView::OnUpdatePlateCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowPlate);
}

void CPrintSheetOutputToolsView::OnUpdateBitmapCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowBitmap);
}

void CPrintSheetOutputToolsView::OnUpdateMeasureCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowMeasure);
}

void CPrintSheetOutputToolsView::OnUpdateExposuresCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowExposures);
//	pCmdUI->Enable(m_bEnableExposuresButton);
}

void CPrintSheetOutputToolsView::OnUpdateReleaseBleed(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, m_pNewPrintSheetFrame->m_bEnableReleaseBleedButton);
}
