// ImpManDocDraw.cpp : implementation of the CImpManDoc class - Drawing functions
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"	// needed by PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "GraphicComponent.h"
#include "PrintSheetPlanningView.h"
#include "OrderDataView.h"
#include "NewPrintSheetFrame.h"
#include "ProductsView.h"



CRect CGlobalData::Draw(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, BOOL bShowExtended)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	CFont	smallFont, boldFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 14), 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);
	CString	strOut;
	int		nTextHeight = Pixel2MM(pDC, 15);
	int		nRowSpace  = Pixel2MM(pDC, 20);
	CRect	rcTitle, rcTitle1, rcText1, rcTitle2, rcText2, rcTitle3, rcText3, rcTitle4, rcText4, rcTitle5, rcText5;
	int		nXStart	= rcFrame.left + Pixel2MM(pDC, 10);
	int		nYStart	= rcFrame.top;
	COLORREF crValueColor = RGB(50,50,50);

	CPen pen(PS_SOLID, Pixel2MM(pDC, 1), RGB(70,125,245));
	CPen* pOldPen = pDC->SelectObject(&pen);

	float fRatio = (pDC->IsPrinting()) ? 0.7f : 1.0f;
	int nLen1 = (int)(250.0f * fRatio);
	int nLen2 = (int)(100.0f * fRatio);
	int nLen3 = (int)(100.0f * fRatio);

	rcTitle.left	= nXStart;		 rcTitle.top	= nYStart;
	rcTitle.right   = rcFrame.right; rcTitle.bottom = rcTitle.top + Pixel2MM(pDC, 20);
	
	nYStart += Pixel2MM(pDC, 25);

	rcTitle1.left	= nXStart;  rcTitle1.top = nYStart;
	rcTitle1.right	= rcTitle1.left + Pixel2MM(pDC, 60); rcTitle1.bottom = rcTitle1.top + nTextHeight;
	rcText1 = rcTitle1; rcText1.left = rcTitle1.right + Pixel2MM(pDC, 5); rcText1.right = rcText1.left + Pixel2MM(pDC, nLen1);

	rcTitle2.left	= rcText1.right + Pixel2MM(pDC, 20);  rcTitle2.top = nYStart;
	rcTitle2.right	= rcTitle2.left + Pixel2MM(pDC, 80); rcTitle2.bottom = rcTitle2.top + nTextHeight;
	rcText2 = rcTitle2; rcText2.left = rcTitle2.right + Pixel2MM(pDC, 5); rcText2.right = rcText2.left + Pixel2MM(pDC, nLen2);
	
	rcTitle3.left	= rcText2.right + Pixel2MM(pDC, 20);  rcTitle3.top = nYStart;
	rcTitle3.right	= rcTitle3.left + Pixel2MM(pDC, 100); rcTitle3.bottom = rcTitle3.top + nTextHeight;
	rcText3 = rcTitle2; rcText3.left = rcTitle3.right + Pixel2MM(pDC, 5); rcText3.right = rcText3.left + Pixel2MM(pDC, nLen3);
	
	rcTitle4.left	= rcText3.right + Pixel2MM(pDC, 20);  rcTitle4.top = rcTitle.top + Pixel2MM(pDC, 3);
	rcTitle4.right	= rcTitle4.left + Pixel2MM(pDC, 70); rcTitle4.bottom = rcTitle4.top + nTextHeight;
	rcText4 = rcTitle4; rcText4.left = rcTitle4.right + Pixel2MM(pDC, 5); rcText4.right = rcFrame.right;

	rcTitle5.left	= nXStart;  rcTitle5.top = nYStart;
	rcTitle5.right	= rcTitle1.left + Pixel2MM(pDC, 80); rcTitle5.bottom = rcTitle5.top + nTextHeight;
	rcText5 = rcTitle5; rcText5.left = rcTitle1.right + Pixel2MM(pDC, 5); rcText5.right = rcFrame.right;

	rcTitle.right = rcText3.right;

	if ( ! pDC->IsPrinting())
	{
		pDC->SelectObject(&boldFont);
		//strOut.LoadString(IDS_TITLE_LABEL);
		//pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
		//pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		strOut = pDoc->GetTitle();
		theApp.RemoveExtension(strOut);
		CGraphicComponent gp;
		gp.DrawTitleBar(pDC, CRect(rcFrame.left, rcFrame.top, rcFrame.right, rcTitle.bottom), _T(""), RGB(231,234,239), RGB(209,215,226), 90, FALSE);
		pDC->DrawText(strOut, rcTitle,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		rcBox.UnionRect(rcBox, rcTitle);
		//pDC->MoveTo(rcTitle.left, rcTitle.bottom); pDC->LineTo(rcFrame.right, rcTitle.bottom);
		nYStart = rcTitle.bottom + Pixel2MM(pDC, 5);
		if (pDisplayList)
		{
			CRect rcDI = rcTitle; rcDI.InflateRect(5, 0); rcDI.bottom -= 3;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)0, DISPLAY_ITEM_REGISTRY(COrderDataView, OrderData), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
				pDI->SetMouseOverFeedback();
		}
	}

	pDC->SelectObject(&smallFont);

	//if ( ! pDC->IsPrinting())
	//{
	//	//pDC->MoveTo(rcTitle4.left - Pixel2MM(pDC, 10), rcFrame.top + Pixel2MM(pDC, 5)); pDC->LineTo(rcTitle4.left - Pixel2MM(pDC, 10), rcFrame.bottom - Pixel2MM(pDC, 5));

	//	if ( ! pDoc->m_GlobalData.m_strJobTicketFile.IsEmpty())
	//	{
	//		strOut = _T("Job Ticket:");
	//		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	//		pDC->DrawText(strOut, rcTitle4, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	//		pDC->SetTextColor(crValueColor);
	//		pDC->DrawText(pDoc->m_GlobalData.m_strJobTicketFile, rcText4,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	//		rcBox.UnionRect(rcBox, rcText4);
	//		//pDC->MoveTo(rcTitle4.left - Pixel2MM(pDC, 5), rcText4.bottom + Pixel2MM(pDC, 2)); pDC->LineTo(rcText4.right, rcText4.bottom + Pixel2MM(pDC, 2));
	//		// newline
	//		nRowSpace = Pixel2MM(pDC, 20);
	//	}
	//	rcTitle4.OffsetRect(0, nRowSpace); rcText4.OffsetRect(0, nRowSpace);

	//	strOut.LoadString(IDS_NOTES_TITLE);
	//	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	//	pDC->DrawText(strOut, rcTitle4, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	//	rcBox.UnionRect(rcBox, rcTitle4);
	//	pDC->SetTextColor(crValueColor);
	//	strOut = pDoc->m_GlobalData.m_strNotes;
	//	int nNumLines = GetNumTextLines(pDC, strOut);
	//	rcText4.right = rcFrame.right; rcText4.bottom = rcText4.top + nNumLines * pDC->GetTextExtent(strOut).cy; //rcFrame.bottom - Pixel2MM(pDC, 5);
	//	pDC->DrawText(strOut, rcText4,  DT_LEFT | DT_VCENTER);
	//	rcBox.UnionRect(rcBox, rcText4);
	//}

// newline
	//rcTitle1.OffsetRect(0, nRowSpace); rcText1.OffsetRect(0, nRowSpace); rcTitle2.OffsetRect(0, nRowSpace); rcText2.OffsetRect(0, nRowSpace); rcTitle3.OffsetRect(0, nRowSpace); rcText3.OffsetRect(0, nRowSpace);
	//nRowSpace = Pixel2MM(pDC, 15);

	pDC->SelectObject(&boldFont);

	strOut.LoadString(IDS_JOBNAME_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle1);
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strJob, rcText1,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	rcBox.UnionRect(rcBox, rcText1);

	strOut.LoadString(IDS_JOBNUMBER_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle2, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle2);
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strJobNumber, rcText2,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	rcBox.UnionRect(rcBox, rcText2);

	strOut.LoadString(IDS_SCHEDULEDTOPRINT_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle3, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle3);
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strScheduledToPrint, rcText3,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	rcBox.UnionRect(rcBox, rcText3);

	if ( ! bShowExtended)
	{
		pDC->SelectObject(pOldPen);
		pen.DeleteObject();
		smallFont.DeleteObject();
		boldFont.DeleteObject();
		if (pDisplayList)
			pDisplayList->EndRegisterItems(pDC);
		return rcBox;
	}

	pDC->SelectObject(&smallFont);

// newline
	rcTitle1.OffsetRect(0, nRowSpace); rcText1.OffsetRect(0, nRowSpace); rcTitle2.OffsetRect(0, nRowSpace); rcText2.OffsetRect(0, nRowSpace); rcTitle3.OffsetRect(0, nRowSpace); rcText3.OffsetRect(0, nRowSpace);

	strOut.LoadString(IDS_CUSTOMER_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle1);
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strCustomer, rcText1,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	rcBox.UnionRect(rcBox, rcText1);

	strOut.LoadString(IDS_CUSTOMERNUMBER_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle2, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle2);
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strCustomerNumber, rcText2,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	rcBox.UnionRect(rcBox, rcText2);

	strOut.LoadString(IDS_CREATIONDATE_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle3, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle3);
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(CPrintSheetView::ConvertToString(pDoc->m_GlobalData.m_CreationDate), rcText3,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	rcBox.UnionRect(rcBox, rcText3);

// newline
	rcTitle1.OffsetRect(0, nRowSpace); rcText1.OffsetRect(0, nRowSpace); rcTitle2.OffsetRect(0, nRowSpace); rcText2.OffsetRect(0, nRowSpace); rcTitle3.OffsetRect(0, nRowSpace); rcText3.OffsetRect(0, nRowSpace);

	strOut.LoadString(IDS_CREATOR_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle1);
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strCreator, rcText1,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	rcBox.UnionRect(rcBox, rcText1);

	strOut.LoadString(IDS_PHONENUMBER_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle2, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle2);
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strPhoneNumber, rcText2,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	rcBox.UnionRect(rcBox, rcText2);

	strOut.LoadString(IDS_LASTMODIFIED_LABEL); strOut += _T(":");
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle3, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle3);
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(CPrintSheetView::ConvertToString(pDoc->m_GlobalData.m_LastModifiedDate), rcText3,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	rcBox.UnionRect(rcBox, rcText3);

	rcTitle5.top = rcTitle1.bottom + Pixel2MM(pDC, 15); rcTitle5.bottom = rcTitle5.top + nTextHeight;
	rcText5.top = rcTitle5.top;  rcText5.right = rcFrame.right; rcText5.bottom = rcText5.top + nTextHeight;
	if ( ! pDoc->m_GlobalData.m_strJobTicketFile.IsEmpty())
	{
		strOut = _T("Job Ticket:");
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
		pDC->DrawText(strOut, rcTitle5, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
		rcBox.UnionRect(rcBox, rcTitle5);
		pDC->SetTextColor(crValueColor);
		pDC->DrawText(pDoc->m_GlobalData.m_strJobTicketFile, rcText5,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		rcBox.UnionRect(rcBox, rcText5);
		//pDC->MoveTo(rcTitle5.left - Pixel2MM(pDC, 5), rcText5.bottom + Pixel2MM(pDC, 2)); pDC->LineTo(rcText5.right, rcText5.bottom + Pixel2MM(pDC, 2));
		// newline
		nRowSpace = Pixel2MM(pDC, 20);
		rcTitle5.OffsetRect(0, nRowSpace); rcText5.OffsetRect(0, nRowSpace);
	}

	if ( ! pDC->IsPrinting() && (rcFrame.right - rcBox.right > 500) )
	{
		rcTitle5.left = rcBox.right + Pixel2MM(pDC, 20); rcTitle5.top = nYStart; rcTitle5.right = rcFrame.right; rcTitle5.bottom = rcTitle5.top + nTextHeight;
		rcText5.top = rcTitle5.bottom + Pixel2MM(pDC, 10);  rcText5.left = rcTitle5.left;
	}

	strOut.LoadString(IDS_NOTES_TITLE);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle5, DT_LEFT | DT_TOP | DT_SINGLELINE);	
	rcBox.UnionRect(rcBox, rcTitle5);
	pDC->SetTextColor(crValueColor);
	strOut = pDoc->m_GlobalData.m_strNotes;
	int nNumLines = GetNumTextLines(pDC, strOut);
	rcText5.bottom = rcText5.top + nNumLines * pDC->GetTextExtent(strOut).cy; 
	pDC->DrawText(strOut, rcText5,  DT_LEFT | DT_TOP | DT_WORDBREAK);
	rcBox.UnionRect(rcBox, rcText5);

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();
	smallFont.DeleteObject();
	boldFont.DeleteObject();

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// PrintGroupList 

int CPrintGroupList::GetLongestFoldSheetNumString(CDC* pDC, CPrintSheet* pPrintSheet, int nComponentIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CString  string, string1;
	int		 nMaxLen = 0;
	POSITION pos	 = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (pPrintSheet)
			if (pPrintSheet != rFoldSheet.GetPrintSheet(0))
				continue;

		if (nComponentIndex >= 0)
			string.Format(_T("%s  (%d)"), rFoldSheet.GetNumber(), nComponentIndex);
		else
			string = rFoldSheet.GetNumber();

		if (rFoldSheet.m_LayerList.GetCount() > 1)
		{
			string1.Format(_T(" (%d)"), rFoldSheet.m_LayerList.GetCount() - 1);
			string += string1;
		}
		int nLen = pDC->GetTextExtent(string).cx;
		nMaxLen = max(nMaxLen, nLen);
	}

	return nMaxLen;
}

int CPrintGroupList::GetLongestFoldSheetPageRange(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CString strPages;
	strPages.LoadString(IDS_PAGES_HEADER);
	CString  string;
	int		 nMaxLen = 0;
	POSITION pos	 = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		string.Format(_T("%-2d %s | %s"), rFoldSheet.GetNumPages(), strPages, rFoldSheet.GetPageRange());
		int nLen = pDC->GetTextExtent(string).cx;
		nMaxLen = max(nMaxLen, nLen);
	}

	return nMaxLen;
}

int CPrintGroupList::GetLongestLabelNameString(CDC* pDC, BOOL bAssignedOnly)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CString  string;
	int		 nMaxLen = 0;
	for (int i = 0; i < pDoc->m_flatProducts.GetSize(); i++)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(i);
		if (bAssignedOnly)
			if ( ! rFlatProduct.GetFirstPrintSheet())
				continue;

		int nLen = pDC->GetTextExtent(rFlatProduct.m_strProductName).cx;
		nMaxLen = max(nMaxLen, nLen);
	}

	return nMaxLen;
}

CRect CPrintSheet::DrawPlanningView(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground)
{
	CRect rcBox = CRect(rcFrame.TopLeft(), CSize(1,1));
	if (this == NULL)
		return rcBox;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return rcBox;

	CPrintingProfile& rPrintingProfile = GetPrintingProfile();

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);
	int	nBorder		= Pixel2MM(pDC, 10);

	COLORREF crValueColor = RGB(50,50,50);

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 10) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT); 

	CRect rcTitle = rcFrame; rcTitle.DeflateRect(nBorder/2, 0); rcTitle.top += nBorder/2; rcTitle.bottom = rcTitle.top + nTextHeight;

	CString strOut, strOut2;
	pDC->SetTextColor(BLACK);
	//strOut.Format(_T(" %s "), (IsEmpty()) ? _T("") : GetNumber());
	strOut.Format(_T(" %s "), GetNumber());
	DrawTextMeasured(pDC, strOut, rcTitle, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	int nRight = rcTitle.left + pDC->GetTextExtent(strOut).cx;

	pDC->SetTextColor(OCKER);
	rcTitle.left = nRight + 10;
	strOut.Format(_T("%s "), (GetLayout()) ? GetLayout()->m_strLayoutName : _T(""));
	DrawTextMeasured(pDC, strOut, rcTitle, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	nRight = rcTitle.left + pDC->GetTextExtent(strOut).cx;

	CRect rcLayout   = rcFrame; 
	rcLayout.top     = rcBox.bottom + 10;
	rcLayout.left   += nBorder;
	rcLayout.right  -= 120;
	rcLayout.bottom -= nBorder;
	rcLayout = (pLayout) ? pLayout->Draw(pDC, rcLayout, this, NULL, TRUE, FALSE, FALSE, NULL, TRUE, FRONTSIDE, NULL, NULL, pDisplayList, TRUE, FALSE) : CRect(0,0,0,0);

	rcBox.right = max(rcBox.right, rcLayout.right + 30);

	//CRect rcPressParams = rcFrame; rcPressParams.left = rcLayout.right + 10; rcPressParams.top = rcBox.top + 5;
	//rcPressParams = rProductionProfile.m_press.DrawData(pDC, rcPressParams, pLayout, NULL, TRUE);						
	//if (pDisplayList)
	//{
	//	CRect rcDI = rcPressParams; 
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, ProductionProfile), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//		if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
	//		{
	//			CRect rcFeedback = pDI->m_BoundingRect; rcFeedback.left++; rcFeedback.top++; rcFeedback.right -=2; rcFeedback.bottom -= 2;
	//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
	//			CGraphicComponent gc;
	//			gc.DrawTransparentFrame(pDC, rcFeedback, RGB(255,195,15));
	//		}
	//	}
	//}
	
	//rcBox.UnionRect(rcBox, rcPressParams);

	pDC->SetTextColor(DARKGRAY);

	CRect rcText = rcFrame; rcText.left = rcLayout.right + 15; rcText.top = rcBox.bottom; rcText.bottom = rcText.top + nTextHeight;
	if (pLayout)
	{
		pDC->SetTextColor(DARKGRAY);
		strOut.Format(_T("%.1f x %.1f"), GetLayout()->m_FrontSide.m_fPaperWidth, pLayout->m_FrontSide.m_fPaperHeight);
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	}

	if (m_nQuantityTotal > 0)
	{
		rcText.OffsetRect(0, nTextHeight);
		strOut2.LoadString(IDS_SHEET_TEXT);
		strOut.Format(_T("%d  %s"), m_nQuantityTotal - m_nQuantityExtra, strOut2);
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		int nRight = rcText.left + pDC->GetTextExtent(strOut).cx;
		CRect rcDI = rcText; rcDI.right = nRight;

		rcText.OffsetRect(0, nTextHeight);
		strOut.Format(_T("%.1f %%"), (GetLayout()) ? GetLayout()->CalcUtilization() * 100.0f : 0.0f);
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

		if (pDisplayList)
		{
			rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom -= 1;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcFrame, (void*)this, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PrintSheetQuantity), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)
				{
					CRect rcFeedback = pDI->m_BoundingRect;
					rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
					CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
				}
			}
		}
	}

	CPrintingProfile& rProfile = GetPrintingProfile();
	CImage img;
	switch (rProfile.m_press.m_sheetData.m_nPaperGrain)
	{
	case GRAIN_HORIZONTAL:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAIN_HORIZONTAL));	break;
	case GRAIN_VERTICAL:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAIN_VERTICAL));	break;
	}
	if ( ! img.IsNull())
	{
		CRect rcImg(CPoint(rcText.left, rcText.bottom + 5), CSize(img.GetWidth(), img.GetHeight()));
		img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
		rcBox.UnionRect(rcBox, rcImg);
	}

	rcBox.bottom = rcLayout.bottom + nBorder;

	if (pLayout)
	{
		if (pLayout->m_variants.GetSize() > 1)
		{
			CString strOut;
			pDC->SelectObject(&boldFont);
			pDC->SetTextColor(GUICOLOR_CAPTION);
			strOut.Format(_T(" %d "), pLayout->m_variants.GetSize());
			CRect rcVariants = rcBox; rcVariants.top += 5; rcVariants.bottom = rcVariants.top + 16; rcVariants.right = rcBox.right - 5; rcVariants.left = rcVariants.right - pDC->GetTextExtent(strOut).cx;
			rcVariants.InflateRect(1, 1);
			CBrush brush(WHITE);
			CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			pDC->RoundRect(rcVariants, CPoint(8, 8));
			pDC->SelectObject(pOldBrush);
			pDC->SelectObject(pOldPen);
			DrawTextMeasured(pDC, strOut, rcVariants, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
			pDC->SelectObject(&font);
			pDC->SetBkMode(TRANSPARENT);
		}
	}

	pDC->SelectObject(pOldFont);
	font.DeleteObject();
	boldFont.DeleteObject();

	rcBox.right += 10;

	if (pDisplayList)
	{
		CRect rcDI = rcBox;  //rcDI.right = rcLayout.right + 10;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)this, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PrintSheetDetails), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
		}
	}

	SetDisplayBoxSheet(rcBox);

	if (pDisplayList)
	{
		CGraphicComponent gc;
		CRect rcRect = rcBox; 
		gc.DrawFrame(pDC, rcRect, crBackground, TRUE);
	}

	return rcBox;
}

CRect CPrintSheet::DrawComponents(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground)
{
	CRect rcBox = CRect(rcFrame.TopLeft(), CSize(1,1));
	if (this == NULL)
		return rcBox;
	if (IsEmpty())
		return rcBox;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	nTextHeight  = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 20;

	rcFrame.left += 5; rcFrame.top += 5;
	rcBox.bottom = rcFrame.top;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	int nYPosLine = 0;

	rcFrame.right -= 10;

	float	fMaxHeightFlat		  = pDoc->m_flatProducts.GetMaxHeight(-1);
	int		nFoldSheetTypeIndex   = 0;
	int		nFlatProductTypeIndex = 0;
	BOOL	bNotAllDrawn		  = FALSE;
	int		i;
	while ( (nFoldSheetTypeIndex < m_FoldSheetTypeList.GetSize()) || (nFlatProductTypeIndex < m_flatProductTypeList.GetSize()) )
	{
		////// folded components thumbnails
		////////////////////////////////////////////////////////////////////////////////////////////////
		CString strOut;
		CRect rcRow = rcFrame; rcRow.bottom = rcRow.top + nTextHeight;
		for (i = nFoldSheetTypeIndex; i < m_FoldSheetTypeList.GetSize(); i++)
		{
			CFoldSheetType& rFoldSheetType = m_FoldSheetTypeList[i];
			CFoldSheet*		pFoldSheet	   = rFoldSheetType.m_pFoldSheet;
			if ( ! pFoldSheet)
				continue;
			CRect rcThumbnail = pFoldSheet->DrawThumbnail(pDC, rcRow, TRUE, FALSE);
			rcBox.UnionRect(rcBox, rcThumbnail);

			rcRow.OffsetRect(0, rcRow.Height());
			if (rcRow.bottom > rcFrame.bottom)
				break;
		}

		////// flat components thumbnails
		////////////////////////////////////////////////////////////////////////////////////////////////
		for (i = nFlatProductTypeIndex; i < m_flatProductTypeList.GetSize(); i++)
		{
			CFlatProductType& rFlatProductType = m_flatProductTypeList[i];
			CFlatProduct*	  pFlatProduct	   = rFlatProductType.m_pFlatProduct;
			if ( ! pFlatProduct)
				continue;
			int nHeight = (int)((float)(rcRow.Height()) * (pFlatProduct->m_fTrimSizeHeight / fMaxHeightFlat));	
			CRect rcThumbnail = rcRow; rcThumbnail.left += 3; rcThumbnail.bottom = rcThumbnail.top + nHeight; rcThumbnail.DeflateRect(0, 2);
			rcThumbnail = pDoc->m_PageTemplateList.DrawFlatProduct(pDC, rcThumbnail, rFlatProductType.m_nFlatProductIndex * 2, FALSE, FALSE, FALSE, TRUE, TRUE);
			//CRect rcThumbnail = pFlatProduct->DrawThumbnail(pDC, rcRow, pDisplayList, FALSE, FALSE);	// w/o name
			rcBox.UnionRect(rcBox, rcThumbnail);

			rcRow.OffsetRect(0, rcRow.Height());
			if (rcRow.bottom > rcFrame.bottom)
				break;
		}

		rcFrame.left = rcBox.right + 10;
		if (rcFrame.left >= rcFrame.right)
		{
			if ( (nFoldSheetTypeIndex < m_FoldSheetTypeList.GetSize() - 1) || (nFlatProductTypeIndex < m_flatProductTypeList.GetSize() - 1) )
				bNotAllDrawn = TRUE;
			break;
		}

		////// folded components names
		////////////////////////////////////////////////////////////////////////////////////////////////
		CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
		for (i = nFoldSheetTypeIndex; i < m_FoldSheetTypeList.GetSize(); i++)
		{
			CFoldSheetType& rFoldSheetType = m_FoldSheetTypeList[i];
			CFoldSheet*		pFoldSheet	   = rFoldSheetType.m_pFoldSheet;
			if ( ! pFoldSheet)
				continue;

			pDC->SetTextColor(BLACK);
			strOut = pFoldSheet->m_strFoldSheetTypeName;
			DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

			rcText.OffsetRect(0, rcText.Height());
			if (rcText.bottom > rcFrame.bottom)
				break;
		}

		////// flat components names
		////////////////////////////////////////////////////////////////////////////////////////////////
		for (i = nFlatProductTypeIndex; i < m_flatProductTypeList.GetSize(); i++)
		{
			CFlatProductType& rFlatProductType = m_flatProductTypeList[i];
			CFlatProduct*	  pFlatProduct	   = m_flatProductTypeList[i].m_pFlatProduct;
			if ( ! pFlatProduct)
				continue;

			pDC->SetTextColor(BLACK);
			strOut = pFlatProduct->m_strProductName;
			DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

			rcText.OffsetRect(0, rcRow.Height());
			if (rcText.bottom > rcFrame.bottom)
				break;
		}

		rcFrame.left = rcBox.right + 5;
		if (rcFrame.left >= rcFrame.right)
		{
			if ( (nFoldSheetTypeIndex < m_FoldSheetTypeList.GetSize() - 1) || (nFlatProductTypeIndex < m_flatProductTypeList.GetSize() - 1) )
				bNotAllDrawn = TRUE;
			break;
		}

		////// folded components ups
		////////////////////////////////////////////////////////////////////////////////////////////////
		int nFieldWidth = pDC->GetTextExtent(_T(" 888 x")).cx + 3;
		rcText = rcFrame; rcText.right = rcText.left + nFieldWidth; rcText.bottom = rcText.top + nTextHeight;
		for (i = nFoldSheetTypeIndex; i < m_FoldSheetTypeList.GetSize(); i++)
		{
			CFoldSheetType& rFoldSheetType = m_FoldSheetTypeList[i];
			CFoldSheet*		pFoldSheet	   = rFoldSheetType.m_pFoldSheet;

			pDC->SetTextColor(OCKER);
			strOut.Format(_T("%d x "), rFoldSheetType.m_nNumUp);
			pDC->DrawText(strOut, rcText, DT_RIGHT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
			rcBox.UnionRect(rcBox, rcText);

			if (pDisplayList)
			{
				CRect rcDI = rcText; 
				CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)((pFoldSheet) ? pFoldSheet->GetFoldSheetIndex() : -1), DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PrintFoldSheetComponent), (void*)this, CDisplayItem::ForceRegister); // register item into display list
			}

			rcText.OffsetRect(0, rcRow.Height());
			if (rcText.bottom > rcFrame.bottom)
			{
				i++; break;
			}
		}
		nFoldSheetTypeIndex = i;

		////// flat components ups
		////////////////////////////////////////////////////////////////////////////////////////////////
		for (i = nFlatProductTypeIndex; i < m_flatProductTypeList.GetSize(); i++)
		{
			CFlatProductType& rFlatProductType = m_flatProductTypeList[i];
			CFlatProduct*	  pFlatProduct	   = m_flatProductTypeList[i].m_pFlatProduct;
			if ( ! pFlatProduct)
				continue;

			pDC->SetTextColor(OCKER);
			strOut.Format(_T("%d x "), rFlatProductType.m_nNumUp);
			pDC->DrawText(strOut, rcText, DT_RIGHT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
			rcBox.UnionRect(rcBox, rcText);

			rcText.OffsetRect(0, rcRow.Height());
			if (rcText.bottom > rcFrame.bottom)
			{
				i++; break;
			}
		}
		nFlatProductTypeIndex = i;

		rcFrame.left = rcBox.right + 20;
		if (rcFrame.left >= rcFrame.right)
		{
			if ( (nFoldSheetTypeIndex < m_FoldSheetTypeList.GetSize() - 1) || (nFlatProductTypeIndex < m_flatProductTypeList.GetSize() - 1) )
				bNotAllDrawn = TRUE;
			break;
		}
	}

	font.DeleteObject();

	rcBox.right  += 10;
	rcBox.bottom += 5;

	SetDisplayBoxComponents(rcBox);

	CGraphicComponent gc;
	gc.DrawFrame(pDC, rcBox, crBackground, TRUE);

	if (bNotAllDrawn)
	{
		pDC->SetTextColor(BLACK);
		CRect rcText = rcBox; rcText.right -= 5;
		pDC->DrawText(_T(">>"), rcText, DT_RIGHT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS);
	}

	return rcBox;
}

void CPrintSheet::DrawPlanningTableLinks(CDC* pDC, BOOL bHighlight, CDisplayList* pDisplayList)
{
	if (IsEmpty())
		return;

	CRect rcSource = GetDisplayBoxSheet();
	CRect rcTarget = GetDisplayBoxComponents();

	CPen  pen(PS_SOLID, 2, RGB(210,210,235));
	CPen* pOldPen = pDC->SelectObject(&pen);

	int nY = min(rcSource.CenterPoint().y, rcTarget.CenterPoint().y);

	pDC->MoveTo(rcSource.right, nY);
	pDC->LineTo(rcTarget.left,  nY);
	
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();
}

int CPrintGroupList::GetLongestPrintSheetNumString(CDC* pDC, int nPrintSheetIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CString  string;
	int		 nMaxLen = 0;
	POSITION pos	 = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (nPrintSheetIndex >= 0)
			string.Format(_T("%s  (%d)"), rPrintSheet.GetNumber(), nPrintSheetIndex);
		else
			string = rPrintSheet.GetNumber();
		int nLen = pDC->GetTextExtent(string).cx;
		nMaxLen = max(nMaxLen, nLen);
	}

	return nMaxLen;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// ProductGroup Drawing



////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// Plate Drawing

void CPlate::Draw(CDC* pDC, CPressDevice* pPressDevice, BOOL bShowMeasures)
{
	if ( ! pPressDevice)
		return;

	CRect plateRect;
	plateRect.left	 = CPrintSheetView::WorldToLP(0);
	plateRect.top	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateHeight);
	plateRect.right	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth);
	plateRect.bottom = CPrintSheetView::WorldToLP(0);

	// Draw the plate rectangle
	if (pDC->RectVisible(plateRect))
	{
		CPen   whitePen, grayPen, blackPen, *pOldPen;
		CBrush plateBrush, bkgrdBrush, *pOldBrush;

		pOldPen	  = pDC->GetCurrentPen();
		pOldBrush = pDC->GetCurrentBrush();

		whitePen.CreatePen(PS_SOLID, 0, WHITE);
		grayPen.CreatePen (PS_SOLID, 0, LIGHTGRAY);
		blackPen.CreatePen(PS_SOLID, 0, BLACK);
		plateBrush.CreateSolidBrush(PLATE_COLOR); 
		bkgrdBrush.CreateSolidBrush(MIDDLEGRAY);

		pDC->FillRect(plateRect, &plateBrush);

		pDC->SelectObject(&grayPen);
		//pDC->SelectObject(&whitePen);
		pDC->MoveTo(plateRect.left,  plateRect.bottom);
		pDC->LineTo(plateRect.left,  plateRect.top);
		pDC->LineTo(plateRect.right, plateRect.top);

		//pDC->SelectObject(&blackPen);
		pDC->LineTo(plateRect.right, plateRect.bottom);
		pDC->LineTo(plateRect.left,  plateRect.bottom);

		// Draw two holes on the bottom of the plate
		pDC->SelectObject(&bkgrdBrush);
		CRect holeRect;
		holeRect.left	= CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth / 4);
		holeRect.top	= CPrintSheetView::WorldToLP(15);
		holeRect.right	= holeRect.left + CPrintSheetView::WorldToLP(25);
		holeRect.bottom	= CPrintSheetView::WorldToLP(5);
		pDC->RoundRect(&holeRect, CPoint(CPrintSheetView::WorldToLP(10), CPrintSheetView::WorldToLP(10)) );
		holeRect.left	= (holeRect.left*3) - CPrintSheetView::WorldToLP(25);
		holeRect.top	= CPrintSheetView::WorldToLP(15);
		holeRect.right	= holeRect.left + CPrintSheetView::WorldToLP(25);
		holeRect.bottom	= CPrintSheetView::WorldToLP(5);
		pDC->RoundRect(&holeRect, CPoint(CPrintSheetView::WorldToLP(10), CPrintSheetView::WorldToLP(10)) );

		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);

		whitePen.DeleteObject(); 
		grayPen.DeleteObject();
		blackPen.DeleteObject();
		plateBrush.DeleteObject();
		bkgrdBrush.DeleteObject();

		if (bShowMeasures)
		{
			CSize szFont(11,11);
			pDC->DPtoLP(&szFont);
			CString strOut; strOut.Format(_T("%.1f x %.1f"), pPressDevice->m_fPlateWidth, pPressDevice->m_fPlateHeight);
			CRect rcText(plateRect.left + (int)(0.2f * (float)szFont.cx), plateRect.top - (int)(0.2f * (float)szFont.cy), plateRect.right, plateRect.top - 2 * szFont.cy);
			CFont font;
			font.CreateFont (szFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
			CFont* pOldFont		= pDC->SelectObject(&font);
			int nOldBkMode		= pDC->SetBkMode(TRANSPARENT);
			COLORREF crOldColor = pDC->SetTextColor(DARKGRAY);
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
			pDC->SelectObject(pOldFont);
			font.DeleteObject();
			pDC->SetBkMode(nOldBkMode);
			pDC->SetTextColor(crOldColor);
		}
	}
}

void CPlate::DrawMeasuring(CDC* pDC, CPressDevice* pPressDevice)
{
	if ( ! pPressDevice)
		return;

	CRect plateRect;
	plateRect.left	 = CPrintSheetView::WorldToLP(0);
	plateRect.top	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateHeight);
	plateRect.right	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth);
	plateRect.bottom = CPrintSheetView::WorldToLP(0);
	CRect sheetRect;
	sheetRect.left   = CPrintSheetView::WorldToLP(0);
	sheetRect.top	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateHeight);
	sheetRect.right  = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth);
	sheetRect.bottom = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge - pPressDevice->m_fGripper);

	CSize sizeDistance(10, 10);
	pDC->DPtoLP(&sizeDistance);

	// plate width
	CPoint pt[2]  = {CPoint(plateRect.left, plateRect.bottom - 2 * sizeDistance.cy), CPoint(plateRect.right, plateRect.bottom - 2 * sizeDistance.cy)};
	CPoint ptRef1 = CPoint(plateRect.left,  plateRect.bottom);
	CPoint ptRef2 = CPoint(plateRect.right, plateRect.bottom);
	CString strOut, strMeasure;
	SetMeasure(0, pPressDevice->m_fPlateWidth, strMeasure);
	strOut.Format(IDS_PLATEWIDTH, strMeasure);
	DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

	// plate height
	pt[0]  = CPoint(plateRect.right + 2 * sizeDistance.cx, plateRect.top); pt[1] = CPoint(plateRect.right + 2 * sizeDistance.cx, plateRect.bottom);
	ptRef1 = CPoint(plateRect.right, plateRect.top);
	ptRef2 = CPoint(plateRect.right, plateRect.bottom); 
	SetMeasure(0, pPressDevice->m_fPlateHeight, strMeasure);
	strOut.Format(IDS_PLATEHEIGHT, strMeasure);
	DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

	if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
	{
		// plate center X
		pt[0]  = CPoint(plateRect.left, plateRect.top + sizeDistance.cy); pt[1] = CPoint(plateRect.left + (plateRect.right - plateRect.left)/2, plateRect.top + sizeDistance.cy);
		ptRef1 = CPoint(plateRect.left, plateRect.top);
		ptRef2 = CPoint(plateRect.left + (plateRect.right - plateRect.left)/2, plateRect.bottom - sizeDistance.cy/4);
		SetMeasure(0, pPressDevice->m_fPlateWidth/2.0f, strMeasure);
		strOut.Format(IDS_PLATECENTERX, strMeasure);
		DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

		// gripper
		int nXPos = CPrintSheetView::WorldToLP(0.0f) - sizeDistance.cx;
		pt[0]  = CPoint(nXPos, sheetRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fGripper)); pt[1] = CPoint(nXPos, sheetRect.bottom);
		ptRef1 = CPoint(sheetRect.right + sizeDistance.cx/4, sheetRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fGripper));
		ptRef2 = CPoint(sheetRect.right + sizeDistance.cx/4, sheetRect.bottom);
		SetMeasure(0, pPressDevice->m_fGripper, strMeasure);
		strOut.Format(IDS_GRIPPER, strMeasure);
		DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

		// begin of printing area in Y
		nXPos = CPrintSheetView::WorldToLP(0.0f) - 3 * sizeDistance.cx;
		pt[0]  = CPoint(nXPos,								 CPrintSheetView::WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge)); 
		pt[1]  = CPoint(nXPos,								 CPrintSheetView::WorldToLP(0.0f));
		ptRef1 = CPoint(sheetRect.right + sizeDistance.cx/4, CPrintSheetView::WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge));
		ptRef2 = CPoint(CPrintSheetView::WorldToLP(0.0f),	 CPrintSheetView::WorldToLP(0.0f));
		SetMeasure(0, pPressDevice->m_fPlateEdgeToPrintEdge, strMeasure);
		strOut.Format(IDS_PRINTBEGIN, strMeasure);
		DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
	}
	else
	{
		// web refline X
		pt[0]  = CPoint(plateRect.left, plateRect.top + sizeDistance.cy); pt[1] = CPoint(plateRect.left + CPrintSheetView::WorldToLP(pPressDevice->m_fXRefLine), plateRect.top + sizeDistance.cy);
		ptRef1 = CPoint(plateRect.left, plateRect.top);
		ptRef2 = CPoint(plateRect.left + CPrintSheetView::WorldToLP(pPressDevice->m_fXRefLine), plateRect.bottom - sizeDistance.cy/4);
		SetMeasure(0, pPressDevice->m_fXRefLine, strMeasure);
		strOut.Format(IDS_WEBREFLINEX, strMeasure);
		DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

		int nLeft = plateRect.left - sizeDistance.cx;
		// web Y refline 1
		pt[0] = CPoint(nLeft, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fYRefLine1));
		pt[1]  = CPoint(nLeft, plateRect.bottom); 
		ptRef1 = CPoint(plateRect.right + sizeDistance.cx/4, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fYRefLine1));
		ptRef2 = CPoint(plateRect.left, plateRect.bottom);
		SetMeasure(0, pPressDevice->m_fYRefLine1, strMeasure);
		//strOut.Format(IDS_WEBREFLINEY, strMeasure);
		strOut = strMeasure;
		DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
		nLeft -= 2 * sizeDistance.cx;

		if (pPressDevice->Has2YRefLines())
		{
			// web Y refline 2
			pt[0] = CPoint(nLeft, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fYRefLine2));
			pt[1]  = CPoint(nLeft, plateRect.bottom); 
			ptRef1 = CPoint(plateRect.right + sizeDistance.cx/4, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fYRefLine2));
			ptRef2 = CPoint(plateRect.left, plateRect.bottom);
			SetMeasure(0, pPressDevice->m_fYRefLine2, strMeasure);
			//strOut.Format(IDS_WEBREFLINEY, strMeasure);
			strOut = strMeasure;
			DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
		}
	}
}

void CPlate::DrawHMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1, CPoint ptRef2, BOOL bChain)
{
	CSize sizeFont(12,12);
	pDC->DPtoLP(&sizeFont);
	CFont font, *pOldFont;
	font.CreateFont(sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	//if (pDC->IsPrinting())
		pOldFont = pDC->SelectObject(&font);
	//else
		//pOldFont = (CFont*)pDC->SelectStockObject(ANSI_VAR_FONT);
	int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor	= pDC->SetBkColor(LIGHTGRAY);
	COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

	CString strOut;
	strOut.Format(_T(" %s "), strMeasure);

	pDC->LPtoDP(pt, 2);

	CSize textExtent = pDC->GetTextExtent(strOut);
	pDC->LPtoDP(&textExtent);

	CPen  linePen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&linePen);
	if ( (ptRef1.x != INT_MAX) && (ptRef1.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef1);
		pDC->LPtoDP(&ptRef1);
		CPoint ptTo(pt[0] + CSize(0, (pt[0].y > ptRef1.y) ? 10 : -10));
		pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	if ( (ptRef2.x != INT_MAX) && (ptRef2.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef2);
		pDC->LPtoDP(&ptRef2);
		CPoint ptTo(pt[1] + CSize(0, (pt[1].y > ptRef2.y) ? 10 : -10));
		pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	pDC->SelectObject(pOldPen);
	linePen.DeleteObject();

	BOOL bFit = TRUE;
	int	 nLen = abs(pt[1].x - pt[0].x);
	if (nLen - 10 < textExtent.cx)	// if measurement arrow too short, flip arrows and use fix length
	{
		bFit = FALSE;
		nLen = 15;
		CPrintSheetView::DrawArrow(pDC, LEFT,   pt[1], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
		CPrintSheetView::DrawArrow(pDC, RIGHT,  pt[0], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
	}
	else
	{
		CPrintSheetView::DrawArrow(pDC, LEFT,   pt[0], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
		CPrintSheetView::DrawArrow(pDC, RIGHT,  pt[1], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
	}

	CPoint ptTextCenter = CPoint(( ! bFit) ? pt[1].x + textExtent.cx/2 + ((bChain) ? nLen/2 : 2*nLen/3) : pt[0].x + (pt[1].x - pt[0].x)/2, 
								 ( ! bFit) ? pt[0].y - ((bChain) ? textExtent.cy/2 : 0) : pt[0].y);

	CRect textRect(ptTextCenter, ptTextCenter);
	textRect.InflateRect(textExtent.cx/2 + 5, textExtent.cy/2);
	pDC->DPtoLP(textRect);
	pDC->DrawText(strOut, textRect, DT_CENTER | DT_VCENTER);

	pDC->SelectObject(pOldFont);
	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetTextColor(crOldTextColor);

	font.DeleteObject();
}

void CPlate::DrawVMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1, CPoint ptRef2, BOOL bChain)
{
	CSize sizeFont(12,12);
	pDC->DPtoLP(&sizeFont);
	CFont font270;
	font270.CreateFont(sizeFont.cy, 0, 2700, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	CFont*	 pOldFont		= pDC->SelectObject(&font270);
	int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor	= pDC->SetBkColor(LIGHTGRAY);
	COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

	CString strOut;
	strOut.Format(_T(" %s "), strMeasure);

	pDC->LPtoDP(pt, 2);

	CSize textExtent = pDC->GetTextExtent(strOut);
	pDC->LPtoDP(&textExtent);

	CPen  linePen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&linePen);
	if ( (ptRef1.x != INT_MAX) && (ptRef1.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef1);
		pDC->LPtoDP(&ptRef1);
		CPoint ptTo(pt[0] + CSize((pt[0].x > ptRef1.x) ? 10 : -10, 0));
		pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	if ( (ptRef2.x != INT_MAX) && (ptRef2.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef2);
		pDC->LPtoDP(&ptRef2);
		CPoint ptTo(pt[1] + CSize((pt[1].x > ptRef1.x) ? 10 : -10, 0));
		pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	pDC->SelectObject(pOldPen);
	linePen.DeleteObject();

	BOOL bFit = TRUE;
	int	 nLen = abs(pt[1].y - pt[0].y);
	if (nLen - 10 < textExtent.cx)	// if measurement arrow too short, flip arrows and use fix length
	{
		bFit = FALSE;
		nLen = 15;
		CPrintSheetView::DrawArrow(pDC, TOP,    pt[1], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
		CPrintSheetView::DrawArrow(pDC, BOTTOM, pt[0], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
	}
	else
	{
		CPrintSheetView::DrawArrow(pDC, TOP,    pt[0], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
		CPrintSheetView::DrawArrow(pDC, BOTTOM, pt[1], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
	}

	CPoint ptTextCenter = CPoint(( ! bFit) ? pt[0].x - ((bChain) ? textExtent.cy/2 : 0) : pt[0].x, 
								 ( ! bFit) ? pt[0].y - textExtent.cx/2 - ((bChain) ? nLen/2 : 2*nLen/3) : pt[0].y + (pt[1].y - pt[0].y)/2);

	CRect textRect(ptTextCenter, ptTextCenter);
	textRect.InflateRect(textExtent.cy/2, textExtent.cx/2);
	pDC->DPtoLP(textRect);
//	pDC->DrawText(strMeasure, textRect, DT_SINGLELINE|DT_BOTTOM|DT_LEFT);
	pDC->TextOut(textRect.left, textRect.bottom, strOut);

	pDC->SelectObject(pOldFont);
	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetTextColor(crOldTextColor);

	font270.DeleteObject();
}



/////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// PressDevice Drawing

void CPressDevice::DrawPlate(CDC* pDC, CRect rcFrame, float fPaperWidth, float fPaperHeight)
{
	float platemid = m_fPlateWidth / 2.0f;
	float fPaperLeft  = platemid - (fPaperWidth / 2.0f);
	float fPaperRight = fPaperLeft +  fPaperWidth;
	float fPaperBottom, fPaperTop;
	if (m_nPressType == SheetPress)
		fPaperBottom = m_fPlateEdgeToPrintEdge - m_fGripper;
	else
		fPaperBottom = GetYCenterOfSection() - (fPaperHeight / 2.0f);
	fPaperTop = fPaperBottom + fPaperHeight;

	BOOL bShowMeasuring = ( (rcFrame.Width() > 150) && (rcFrame.Height() > 100) ) ? TRUE : FALSE;

	float fExtentX = max(m_fPlateWidth,  fPaperWidth);
	float fExtentY = max(m_fPlateHeight, fPaperHeight);
	pDC->SaveDC();
	CRect rcClip = rcFrame; 
	pDC->IntersectClipRect(rcClip);
	rcFrame.DeflateRect((bShowMeasuring) ? 60 : 5, (bShowMeasuring) ? 40 : 5);
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(CPrintSheetView::WorldToLP(fExtentX), CPrintSheetView::WorldToLP(fExtentY));
	pDC->SetWindowOrg(CPrintSheetView::WorldToLP(min(fPaperLeft, 0.0f)),	CPrintSheetView::WorldToLP(max(fPaperTop, fExtentY)));
	pDC->SetViewportExt(rcFrame.Width(), -rcFrame.Height());
	pDC->SetViewportOrg(rcFrame.left,	  rcFrame.top);

	CPlate::Draw(pDC, this);

	if ( (fabs(fPaperWidth) > 0.001f) && (fabs(fPaperHeight) > 0.001f) )
	{
		CRect sheetRect;
		sheetRect.left	 = CPrintSheetView::WorldToLP(fPaperLeft);
		sheetRect.top	 = CPrintSheetView::WorldToLP(fPaperBottom);
		sheetRect.right	 = CPrintSheetView::WorldToLP(fPaperRight);
		sheetRect.bottom = CPrintSheetView::WorldToLP(fPaperTop);

		CBrush paperBrush;
		CPen   framePen, shadowPen;
		paperBrush.CreateSolidBrush(WHITE);
		framePen.CreatePen (PS_SOLID, 0, LIGHTGRAY);
		shadowPen.CreatePen(PS_SOLID, 0, DARKGRAY);
		CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&paperBrush);	
		CPen*   pOldPen	  = pDC->SelectObject(&framePen);	
		CPrintSheetView::DrawRectangleExt(pDC, &sheetRect);

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);		
		paperBrush.DeleteObject();
		shadowPen.DeleteObject();
	}

	if (bShowMeasuring)
		CPlate::DrawMeasuring(pDC, this);

	pDC->RestoreDC(-1);
}



//////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// PrintSheetList Drawing

CRect CPrintSheetList::Draw(CDC* pDC, CRect rcFrame, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList, CPrintSheet** ppPrintSheet)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	if (pDC->IsPrinting())
	{
		CRect rcHeader = PrintHeader(pDC, rcFrame);
		rcFrame.top = rcHeader.bottom + Pixel2MM(pDC, 10);
	}

	int	  nTextHeight = Pixel2MM(pDC, 35);
	int	  nBorder	  = Pixel2MM(pDC,  5);
	CRect rcRow		  = rcFrame; rcRow.bottom = rcRow.top + nTextHeight;

	CPen grayPen(PS_SOLID, Pixel2MM(pDC, 1), (pDC->IsPrinting()) ? MIDDLEGRAY : TABLECOLOR_SEPARATION_LINE);
	CPen*  pOldPen	  = pDC->SelectObject(&grayPen);

	int		 nRowIndex  = 0;
	POSITION pos		= pDoc->m_PrintSheetList.GetHeadPosition();
	if (ppPrintSheet)
	{
		if (*ppPrintSheet)
		{
			while (pos)		// goto start pos
			{
				if (*ppPrintSheet == &pDoc->m_PrintSheetList.GetAt(pos))
					break;
				pDoc->m_PrintSheetList.GetNext(pos);
			}
		}
	}

	nTextHeight = Pixel2MM(pDC, 15);
	int nPrintSheetIndex = 0;
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);

		if (rPrintSheet.IsEmpty())
			continue;

		if (ppPrintSheet)
			*ppPrintSheet = &rPrintSheet;

		rcRow.bottom = rcRow.top + nTextHeight * max(rPrintSheet.GetNumComponents(), 1) + nBorder;

		if (pDC->IsPrinting())
			if (rcRow.bottom > rcFrame.bottom)
			{
				if (nPrintSheetIndex == 0)	// if first printsheet on this page and components even don't fit, draw anyway 
					;//*ppPrintSheet = NULL;
				else
					break;
			}

		CRect rcBkgnd = rcRow; rcBkgnd.left = pDC->IsPrinting() ? rcRow.left : 0;
		if ( ! pDC->IsPrinting())
			pDC->FillSolidRect(rcBkgnd, (nRowIndex%2) ? TABLECOLOR_BACKGROUND : TABLECOLOR_BACKGROUND_DARK);
		pDC->MoveTo(rcBkgnd.left,  rcBkgnd.top);
		pDC->LineTo(rcBkgnd.right, rcBkgnd.top);
		nRowIndex++;

		CRect rcPrintSheetRow = DrawPrintSheetRow(pDC, rcRow, rPrintSheet, nPrintSheetIndex, rColumnWidths, pDisplayList);
		rcBox.UnionRect(rcBox, rcPrintSheetRow);

		rcRow.top = rcPrintSheetRow.bottom;
		nPrintSheetIndex++;
	}

	if (ppPrintSheet && ! pos)	// ! pos = reached end of list
		*ppPrintSheet = NULL;

	rcBox.bottom += 60;		// ???

	pDC->SelectObject(pOldPen);
	grayPen.DeleteObject();

	return rcBox;
}

CRect CPrintSheetList::PrintHeader(CDC* pDC, CRect rcFrame)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nSheetNumWidth				= Pixel2MM(pDC, 100);
	int nLayoutNameWidth			= Pixel2MM(pDC, 100);
	int nPressDeviceWidth			= Pixel2MM(pDC, 120);
	int nColorsWidth				= Pixel2MM(pDC,  60);
	int nComponentWidth				= Pixel2MM(pDC, 240);
	int nNumUpWidth					= Pixel2MM(pDC,  50);
	int nPrintSheetAmountWidth		= Pixel2MM(pDC,  60);	
	int nPrintSheetExtraWidth		= Pixel2MM(pDC,  60);
	int nPrintSheetTotalAmountWidth	= Pixel2MM(pDC,  60);
	int nPrintSheetPaperWidth		= Pixel2MM(pDC, 180);
	int nBorder						= Pixel2MM(pDC, 5);
	int nTextHeight					= Pixel2MM(pDC, 15);

	float fRatio = (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nSheetNumWidth				= (int)((float)nSheetNumWidth				 * fRatio);			
	nLayoutNameWidth			= (int)((float)nLayoutNameWidth				 * fRatio);
	nPressDeviceWidth			= (int)((float)nPressDeviceWidth			 * fRatio);
	nColorsWidth				= (int)((float)nColorsWidth					 * fRatio);
	nComponentWidth				= (int)((float)nComponentWidth				 * fRatio);
	nNumUpWidth					= (int)((float)nNumUpWidth					 * fRatio);
	nPrintSheetAmountWidth		= (int)((float)nPrintSheetAmountWidth		 * fRatio);
	nPrintSheetExtraWidth		= (int)((float)nPrintSheetExtraWidth		 * fRatio);
	nPrintSheetTotalAmountWidth	= (int)((float)nPrintSheetTotalAmountWidth	 * fRatio);
	nPrintSheetPaperWidth		= (int)((float)nPrintSheetPaperWidth		 * fRatio);
	nBorder						= (int)((float)nBorder						 * fRatio);
	nTextHeight					= (int)((float)nTextHeight					 * fRatio);

	CString strBounded, strUnbounded, strComponent; 
	strBounded.LoadString(IDS_FOLDSHEET_LABEL); strUnbounded.LoadString(IDS_LABELTYPES); 
	if (pDoc->m_boundProducts.GetSize() && pDoc->m_flatProducts.GetSize())
		strComponent.Format(_T("%s / %s"), strBounded, strUnbounded); 
	else
		if (pDoc->m_boundProducts.GetSize())
			strComponent = strBounded;
		else
			if (pDoc->m_flatProducts.GetSize())
				strComponent = strUnbounded;

	COLORREF crValueColor = RGB(50,50,50);

	CFont	boldFont;
	boldFont.CreateFont (Pixel2MM(pDC, 9), 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(crValueColor);

	CRect rcText = rcFrame; rcText.left += nBorder; rcText.right = rcFrame.left + nSheetNumWidth - nBorder; rcText.bottom = rcText.top + nTextHeight;
	CString strOut; strOut.LoadString(IDS_SHEET_TEXT);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nLayoutNameWidth - nBorder;
	strOut.LoadString(IDS_TYPE_TEXT);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPressDeviceWidth - nBorder;
	strOut.LoadString(IDS_PRESS);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nColorsWidth - nBorder;
	strOut.LoadString(IDS_PLATECOLOR_STRING);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nComponentWidth - nBorder;
	strOut = strComponent;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nNumUpWidth - nBorder;
	strOut.LoadString(IDS_N_UP);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPrintSheetAmountWidth - nBorder;
	strOut.LoadString(IDS_NUMSHEETS_NET);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPrintSheetExtraWidth - nBorder;
	strOut.LoadString(IDS_PAPERLIST_EXTRA);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPrintSheetTotalAmountWidth - nBorder;
	strOut.LoadString(IDS_NUMSHEETS_TOTAL);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPrintSheetPaperWidth - nBorder;
	strOut.LoadString(IDS_PAPER);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	boldFont.DeleteObject();

	return rcBox;
}

CRect CPrintSheetList::DrawPrintSheetRow(CDC* pDC, CRect rcFrame, CPrintSheet& rPrintSheet, int nPrintSheetIndex, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	CLayout*	  pLayout	   = rPrintSheet.GetLayout();
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	if ( ! pLayout || ! pPressDevice)
		return rcBox;

	COLORREF crValueColor = RGB(50,50,50);

	CPrintSheetPlanningView* pView				   = NULL;
	BOOL					 bPrintSheetNumeration = FALSE; 
	if (pDisplayList)
		if (pDisplayList->m_pParent)
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
			{
				pView					= (CPrintSheetPlanningView*)pDisplayList->m_pParent;
				bPrintSheetNumeration	= (pView->IsOpenPrintSheetNumeration()) ? TRUE : FALSE;
			}

	int nSymbolWidth				= (pDC->IsPrinting()) ? 0 : Pixel2MM(pDC, (rColumnWidths.GetSize() > 0)  ? rColumnWidths[0]  :  20);
	int nSheetNumWidth				= Pixel2MM(pDC, (rColumnWidths.GetSize() > 1)  ? rColumnWidths[1]  : 100);
	int nLayoutNameWidth			= Pixel2MM(pDC, (rColumnWidths.GetSize() > 2)  ? rColumnWidths[2]  : 100);
	int nPressDeviceWidth			= Pixel2MM(pDC, (rColumnWidths.GetSize() > 3)  ? rColumnWidths[3]  : 120);
	int nColorsWidth				= Pixel2MM(pDC, (rColumnWidths.GetSize() > 4)  ? rColumnWidths[4]  :  60);
	// 5, 6 components data
	int nPrintSheetAmountWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 7)  ? rColumnWidths[7]  :  60);	
	int nPrintSheetExtraWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 8)  ? rColumnWidths[8]  :  60);
	int nPrintSheetTotalAmountWidth	= Pixel2MM(pDC, (rColumnWidths.GetSize() > 9)  ? rColumnWidths[9]  :  60);
	int nPrintSheetPaperWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 10) ? rColumnWidths[10] : 180);
	int nTextHeight					= Pixel2MM(pDC, 15);
	int nBorder						= Pixel2MM(pDC, 5);
	int	nPressType					= (pPressDevice) ? pPressDevice->m_nPressType : CPressDevice::SheetPress;

	float fRatio = (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nSheetNumWidth				= (int)((float)nSheetNumWidth				 * fRatio);			
	nLayoutNameWidth			= (int)((float)nLayoutNameWidth				 * fRatio);
	nPressDeviceWidth			= (int)((float)nPressDeviceWidth			 * fRatio);
	nColorsWidth				= (int)((float)nColorsWidth					 * fRatio);
	nPrintSheetAmountWidth		= (int)((float)nPrintSheetAmountWidth		 * fRatio);
	nPrintSheetExtraWidth		= (int)((float)nPrintSheetExtraWidth		 * fRatio);
	nPrintSheetTotalAmountWidth	= (int)((float)nPrintSheetTotalAmountWidth	 * fRatio);
	nPrintSheetPaperWidth		= (int)((float)nPrintSheetPaperWidth		 * fRatio);
	nTextHeight					= (int)((float)nTextHeight					 * fRatio);
	nBorder						= (int)((float)nBorder						 * fRatio);

	rcFrame.bottom = rcFrame.top + (int)((float)rcFrame.Height() * fRatio);

	CFont	smallFont, boldFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 9) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 8) : 12, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_CAPTION);

	CRect rcLayout = rcFrame; rcLayout.left = (pDC->IsPrinting()) ? (rcFrame.left + nBorder) : (rcFrame.left + nSymbolWidth); rcLayout.right = rcLayout.left + nSheetNumWidth;
	if (pLayout)
	{
		CRect rcThumbnail(rcLayout.TopLeft() + CSize(0, nBorder), CSize(min(nSheetNumWidth - nBorder, Pixel2MM(pDC, (int)(35.0f * fRatio))), rcFrame.Height() - 2 * nBorder));
		rcLayout = pLayout->Draw(pDC, rcThumbnail, &rPrintSheet, pPressDevice, FALSE, FALSE, FALSE, NULL, TRUE);
		rcBox.UnionRect(rcBox, rcLayout);
		if (pDisplayList)
		{
			CRect rcDI1 = CRect(0, rcFrame.top + 1, rcLayout.right + 3, rcFrame.bottom - 1);
			CRect rcDI2 = rcFrame; rcDI2.left = 0; rcDI2.right -= 1; rcDI2.bottom -= 1;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI2, rcDI2, (void*)pLayout, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, Layout), (void*)&rPrintSheet, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				//pDI->SetMouseOverCursor(CDisplayItem::cursorMove);
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)
				{
					CRect rcFeedback = pDI->m_BoundingRect;
					rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
					Graphics graphics(pDC->m_hDC);
					Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
					rc.Inflate(-1, -1);
					COLORREF crHighlightColor = RED;
					SolidBrush br(Color::Color(20, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
					graphics.FillRectangle(&br, rc);
					graphics.DrawRectangle(&Pen(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 2), rc);
				}
			}
		}
	}

	CRect rcText = rcFrame; rcText.left = rcLayout.right + nBorder; rcText.right = rcFrame.left + nSymbolWidth + nSheetNumWidth - nBorder;
	rcText.top += nBorder; rcText.bottom = rcText.top + nTextHeight;

	CString strOut = rPrintSheet.GetNumber();
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	pDC->SelectObject(&smallFont);

	if (bPrintSheetNumeration)
	{
		strOut.Format(_T(" (%d)"), nPrintSheetIndex + 1);
		CRect rcText2 = rcText; rcText2.left = rcText2.right - pDC->GetTextExtent(strOut).cx - 2 * nBorder;	
		pDC->SetTextColor(RED);
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	}
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 2; rcDI.right -= 2; 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcFrame, (void*)&rPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PrintSheetNumber), (void*)nPrintSheetIndex, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (bPrintSheetNumeration)
				pDI->m_nState = (pView->IsInsideNumerationRange(nPrintSheetIndex)) ? CDisplayItem::Selected : CDisplayItem::Normal;
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	pDC->SetTextColor(crValueColor);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nLayoutNameWidth - nBorder;
	strOut = pLayout->m_strLayoutName;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcFrame, (void*)pLayout, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, LayoutName), (void*)&rPrintSheet, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
		}
	}

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPressDeviceWidth - nBorder;
	strOut = pPressDevice->m_strName;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	pDC->SetTextColor(crValueColor);
	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nColorsWidth - nBorder;
	if (rPrintSheet.HasObjects())
		strOut.Format(_T("%d/%d"), rPrintSheet.GetNumColors(FRONTSIDE), rPrintSheet.GetNumColors(BACKSIDE));
	else
		strOut = _T("");
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	CRect rcComponents = DrawPrintSheetComponents(pDC, CRect(rcText.right + nBorder, rcFrame.top, rcFrame.right, rcFrame.bottom), rPrintSheet, rColumnWidths);
	rcBox.UnionRect(rcBox, rcComponents);

	rcText.left = rcComponents.right + nBorder; rcText.right = rcText.left + nPrintSheetAmountWidth - nBorder;
	strOut.Format(_T("%d"), rPrintSheet.m_nQuantityTotal - rPrintSheet.m_nQuantityExtra);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPrintSheetExtraWidth - nBorder;
	strOut.Format(_T("%d"), rPrintSheet.m_nQuantityExtra);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcFrame, (void*)&rPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PrintSheetQuantityExtra), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPrintSheetTotalAmountWidth - nBorder;
	strOut.Format(_T("%d"), rPrintSheet.m_nQuantityTotal);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPrintSheetPaperWidth - nBorder;
	BOOL bError = rPrintSheet.GetPaperName(strOut); 
	if (bError)
	{
		pDC->SetTextColor(RED);
		pDC->DrawText(_T("Mehrere Sorten!!!"), rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		pDC->SetTextColor(crValueColor);
	}
	else
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcBox.UnionRect(rcBox, rcText);

	if ( ! strOut.IsEmpty())
	{
		rcText.OffsetRect(0, nTextHeight);
		pDC->SetTextColor(DARKGRAY);
	}
	CString strWidth, strHeight;
	CPrintSheetView::TruncateNumber(strWidth, pLayout->m_FrontSide.m_fPaperWidth); CPrintSheetView::TruncateNumber(strHeight, pLayout->m_FrontSide.m_fPaperHeight, TRUE);
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	smallFont.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CPrintSheetList::DrawPrintSheetComponents(CDC* pDC, CRect rcFrame, CPrintSheet& rPrintSheet, CArray <int, int>& rColumnWidths)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	COLORREF crValueColor = RGB(50,50,50);

	int	nGap				  = Pixel2MM(pDC, 10);
	int nComponentWidth		  = Pixel2MM(pDC, (rColumnWidths.GetSize() > 5) ? rColumnWidths[5] : 240);
	int nNumUpWidth			  = Pixel2MM(pDC, (rColumnWidths.GetSize() > 6) ? rColumnWidths[6] :  50);
	int nBorder				  = Pixel2MM(pDC, 5);
	int nTextHeight			  = Pixel2MM(pDC, 15);
	int nOffset				  = Pixel2MM(pDC, 35);

	if ( ! rPrintSheet.HasObjects())
	{
		rcBox.right = rcFrame.left + nComponentWidth + nNumUpWidth - nBorder;
		return rcBox;
	}

	float fRatio = (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nGap			= (int)((float)nGap				* fRatio);
	nComponentWidth	= (int)((float)nComponentWidth	* fRatio);
	nNumUpWidth		= (int)((float)nNumUpWidth		* fRatio);
	nBorder			= (int)((float)nBorder			* fRatio);	
	nTextHeight		= (int)((float)nTextHeight		* fRatio);
	nOffset			= (int)((float)nOffset			* fRatio);

	int nNameLeft = rcFrame.left + nOffset + 2 * nBorder;
	
	CFont	smallFont, boldFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 9) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 8) : 12, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(crValueColor);

	int nMaxFoldSheetNumWidth = pDoc->m_printGroupList.GetLongestFoldSheetNumString(pDC) + pDC->GetTextExtent(_T("  ")).cx;

	CString strOut;
	rPrintSheet.InitializeFoldSheetTypeList();
	for (int i = 0; i < rPrintSheet.m_FoldSheetTypeList.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet  = rPrintSheet.m_FoldSheetTypeList[i].m_pFoldSheet;
		int			nNumUp	    = rPrintSheet.m_FoldSheetTypeList[i].m_nNumUp;
		int			nLayerIndex = rPrintSheet.m_FoldSheetTypeList[i].m_nFoldSheetLayerIndex;
		if ( ! pFoldSheet)
			continue;

		CRect rcThumbnail(rcFrame.TopLeft() + CSize(nBorder, nBorder), CSize(rcFrame.Height(), rcFrame.Height() - 2 * nBorder));
		pFoldSheet->DrawThumbnail(pDC, rcThumbnail, (pFoldSheet->m_LayerList.GetSize() > 1) ? TRUE : FALSE, FALSE, nLayerIndex);

		pDC->SelectObject(&boldFont);
		pDC->SetTextColor(RGB(255, 150, 50));

		CRect rcText = rcFrame; rcText.left += nOffset + 2 * nBorder; rcText.right = rcText.left + min(nMaxFoldSheetNumWidth, nComponentWidth) - nBorder; 
		rcText.top = rcFrame.top + nBorder; rcText.bottom = rcText.top + nTextHeight;
	
		strOut.Format(_T("%s"), pFoldSheet->GetNumber());
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		pDC->SelectObject(&smallFont);

		rcText.left = rcText.right + nGap; rcText.right = rcFrame.left + nComponentWidth - nBorder; 
		strOut.Format(_T("%s"), pFoldSheet->m_strFoldSheetTypeName);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);
		nNameLeft = rcText.left;

		//pDC->SetTextColor(DARKGRAY);
		pDC->SetTextColor(GUICOLOR_CAPTION);

		rcText.OffsetRect(0, nTextHeight);
		CString strOut2; strOut2.LoadString(IDS_PAGES_HEADER);
		strOut.Format(_T("%-2d %s | %s"), (pFoldSheet) ? pFoldSheet->GetNumPages() : 0, strOut2, (pFoldSheet) ? pFoldSheet->GetPageRange() : _T(""));
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		pDC->SetTextColor(crValueColor);

		rcText.top = rcFrame.top + nBorder; rcText.bottom = rcText.top + nTextHeight;
		rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nNumUpWidth - nBorder; 
		strOut.Format(_T("%d"), nNumUp);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.right	 = max(rcBox.right, rcText.right);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		rcFrame.top = rcBox.bottom;
	}


	pDC->SelectObject(&boldFont);

	float fMaxHeight			  = pDoc->m_flatProducts.GetMaxHeight();
	int   nMaxLabelNameWidth	  = pDoc->m_printGroupList.GetLongestLabelNameString(pDC) + pDC->GetTextExtent(_T("  ")).cx;
	CString strWidth, strHeight;
	rPrintSheet.InitializeFlatProductTypeList();
	for (int i = 0; i < rPrintSheet.m_flatProductTypeList.GetSize(); i++)
	{
		CFlatProduct* pFlatProduct = rPrintSheet.m_flatProductTypeList[i].m_pFlatProduct;
		int				 nNumUp	   = rPrintSheet.m_flatProductTypeList[i].m_nNumUp;
		if ( ! pFlatProduct)
			continue;

		int m_nFlatProduct = pDoc->m_flatProducts.FindProductIndex(pFlatProduct);
		if (m_nFlatProduct < 0)
			continue;

		CRect rcThumbnail(rcFrame.TopLeft() + CSize(nBorder, 2 * nBorder), CSize(nOffset, nOffset - 2 * nBorder));
		rcThumbnail.bottom = rcThumbnail.top + (int)((float)rcThumbnail.Height() * (pFlatProduct->m_fTrimSizeHeight / fMaxHeight));		
		pDoc->m_PageTemplateList.DrawFlatProduct(pDC, rcThumbnail, m_nFlatProduct*2);

		CRect rcText = rcFrame; rcText.left = nNameLeft; rcText.right = rcFrame.left + nComponentWidth - nBorder; 
		rcText.top = rcFrame.top + nBorder; rcText.bottom = rcText.top + nTextHeight;
	
		pDC->SelectObject(&boldFont);
		pDC->SetTextColor(GUICOLOR_CAPTION);
		
		strOut.Format(_T("%s"), pFlatProduct->m_strProductName);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		pDC->SelectObject(&smallFont);
		pDC->SetTextColor(crValueColor);

		rcText.OffsetRect(0, nTextHeight);
		CPrintSheetView::TruncateNumber(strWidth,  pFlatProduct->m_fTrimSizeWidth);
		CPrintSheetView::TruncateNumber(strHeight, pFlatProduct->m_fTrimSizeHeight, TRUE);
		strOut.Format(_T("%s x %s"), strWidth, strHeight);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		rcText.top = rcFrame.top + nBorder; rcText.bottom = rcText.top + nTextHeight;
		rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nNumUpWidth - nBorder; 
		strOut.Format(_T("%d"), nNumUp);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.right	 = max(rcBox.right, rcText.right);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		rcFrame.top = rcBox.bottom;
	}

	rcBox.bottom += nBorder;

	pDC->SelectObject(&smallFont);

	smallFont.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}



///////////////////// paper list drawing 
/////////////////////////////////////////////////////////////////////////////////////

CRect CPaperList::Draw(CDC* pDC, CRect rcFrame, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	if (pDC->IsPrinting())
	{
		CRect rcHeader = PrintHeader(pDC, rcFrame);
		rcFrame.top = rcHeader.bottom + Pixel2MM(pDC, 10);
	}

	int	  nTextHeight = Pixel2MM(pDC, 25);
	CRect rcRow = rcFrame; rcRow.bottom = rcRow.top + nTextHeight;

	CPen grayPen(PS_SOLID, Pixel2MM(pDC, 1), (pDC->IsPrinting()) ? LIGHTGRAY : TABLECOLOR_SEPARATION_LINE);
	CPen*  pOldPen	  = pDC->SelectObject(&grayPen);

	BOOL bDrawTotal		 = FALSE;
	int  nPaperTypeTotal = 0;
	for (int i = 0; i < pDoc->m_productionData.m_paperList.GetSize(); i++)
	{
		CPaperListEntry& rFlatProduct = pDoc->m_productionData.m_paperList[i];
		nPaperTypeTotal += rFlatProduct.m_nQuantityTotal;

		if (i == (pDoc->m_productionData.m_paperList.GetSize() - 1))
			bDrawTotal = TRUE;
		else
			if (rFlatProduct.m_strPaperName != pDoc->m_productionData.m_paperList[i + 1].m_strPaperName)
				bDrawTotal = TRUE;

		CRect bkRect(pDC->IsPrinting() ? rcFrame.left : 0, rcRow.top, rcRow.right, (bDrawTotal) ? (rcRow.top + 2*nTextHeight) : (rcRow.top + nTextHeight));
		if ( ! pDC->IsPrinting())
			pDC->FillSolidRect(bkRect, (i%2) ? TABLECOLOR_BACKGROUND : TABLECOLOR_BACKGROUND_DARK);
		pDC->MoveTo(bkRect.left,  bkRect.top);
		pDC->LineTo(bkRect.right, bkRect.top);

		CRect rcPaperListEntryRow = DrawPaperListEntryRow(pDC, rcRow, rFlatProduct, rColumnWidths, (bDrawTotal) ? nPaperTypeTotal : 0, pDisplayList);
		rcBox.UnionRect(rcBox, rcPaperListEntryRow);

		rcRow.top	 = rcPaperListEntryRow.bottom;
		rcRow.bottom = rcRow.top + nTextHeight;

		if (bDrawTotal)
		{
			rcRow.OffsetRect(0, nTextHeight);
			nPaperTypeTotal = 0;
			bDrawTotal = FALSE;
		}
	}

	pDC->SelectObject(pOldPen);
	grayPen.DeleteObject();


	return rcBox;
}

CRect CPaperList::PrintHeader(CDC* pDC, CRect rcFrame)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	int nPaperNameWidth			= Pixel2MM(pDC, 150);
	int nPressNameWidth			= Pixel2MM(pDC, 120);
	int nLayoutNameWidth		= Pixel2MM(pDC, 120);
	int nPaperFormatWidth		= Pixel2MM(pDC, 100);
	int nNumFormsWidth			= Pixel2MM(pDC,  50);
	int nQuantityTotalWidth		= Pixel2MM(pDC,  80);
	int nCommentWidth			= Pixel2MM(pDC, 250);
	int nTextHeight				= Pixel2MM(pDC, 15);
	int nBorder					= Pixel2MM(pDC, 5);

	float fRatio = (pDC->IsPrinting()) ? 0.9f : 1.0f;
	nPaperNameWidth			= (int)((float)nPaperNameWidth		* fRatio);
	nPressNameWidth			= (int)((float)nPressNameWidth		* fRatio);
	nLayoutNameWidth		= (int)((float)nLayoutNameWidth		* fRatio);
	nPaperFormatWidth		= (int)((float)nPaperFormatWidth	* fRatio);
	nNumFormsWidth			= (int)((float)nNumFormsWidth		* fRatio);
	nQuantityTotalWidth		= (int)((float)nQuantityTotalWidth	* fRatio);
	nCommentWidth			= (int)((float)nCommentWidth		* fRatio);
	nTextHeight				= (int)((float)nTextHeight			* fRatio);
	nBorder					= (int)((float)nBorder				* fRatio);


	COLORREF crValueColor = RGB(50,50,50);

	CFont	boldFont;
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 12, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(crValueColor);

	CRect rcText = rcFrame; rcText.left += nBorder; rcText.right = rcFrame.left + nPaperNameWidth - nBorder; rcText.bottom = rcText.top + nTextHeight;
	CString strOut; strOut.LoadString(IDS_PAPER);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPressNameWidth - nBorder;
	strOut.LoadString(IDS_PRESS);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nLayoutNameWidth - nBorder;
	strOut.LoadString(IDS_PRINTSHEETLAYOUT);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPaperFormatWidth - nBorder;
	strOut.LoadString(IDS_PRINTFORMAT);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nNumFormsWidth - nBorder;
	strOut.LoadString(IDS_PAPERLIST_FORMS);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nQuantityTotalWidth - nBorder;
	strOut.LoadString(IDS_NUMSHEETS_TOTAL);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nCommentWidth - nBorder;
	strOut.LoadString(IDS_PAPERLIST_COMMENT);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	boldFont.DeleteObject();

	return rcBox;
}

CRect CPaperList::DrawPaperListEntryRow(CDC* pDC, CRect rcFrame, CPaperListEntry& rFlatProduct, CArray <int, int>& rColumnWidths, int nPaperTypeTotal, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	COLORREF crValueColor = RGB(50,50,50);

	int nPaperNameWidth				= Pixel2MM(pDC, (rColumnWidths.GetSize() > 0) ? rColumnWidths[0] : 150);
	int nPressNameWidth				= Pixel2MM(pDC, (rColumnWidths.GetSize() > 1) ? rColumnWidths[1] : 120);
	int nLayoutNameWidth			= Pixel2MM(pDC, (rColumnWidths.GetSize() > 2) ? rColumnWidths[2] : 120);
	int nPaperFormatWidth			= Pixel2MM(pDC, (rColumnWidths.GetSize() > 3) ? rColumnWidths[3] : 100);
	int nNumFormsWidth				= Pixel2MM(pDC, (rColumnWidths.GetSize() > 4) ? rColumnWidths[4] :  50);
	int nQuantityTotalWidth			= Pixel2MM(pDC, (rColumnWidths.GetSize() > 5) ? rColumnWidths[5] :  80);
	int nCommentWidth				= Pixel2MM(pDC, (rColumnWidths.GetSize() > 6) ? rColumnWidths[6] : 250);
	int nTextHeight					= Pixel2MM(pDC, 15);
	int nBorder						= Pixel2MM(pDC, 5);

	float fRatio = (pDC->IsPrinting()) ? 0.9f : 1.0f;
	nPaperNameWidth			= (int)((float)nPaperNameWidth		* fRatio);
	nPressNameWidth			= (int)((float)nPressNameWidth		* fRatio);
	nLayoutNameWidth		= (int)((float)nLayoutNameWidth		* fRatio);
	nPaperFormatWidth		= (int)((float)nPaperFormatWidth	* fRatio);
	nNumFormsWidth			= (int)((float)nNumFormsWidth		* fRatio);
	nQuantityTotalWidth		= (int)((float)nQuantityTotalWidth	* fRatio);
	nCommentWidth			= (int)((float)nCommentWidth		* fRatio);
	nTextHeight				= (int)((float)nTextHeight			* fRatio);
	nBorder					= (int)((float)nBorder				* fRatio);

	CFont	smallFont, boldFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 10) : 12, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(crValueColor);

	CRect rcText = rcFrame; rcText.left += nBorder; rcText.right = rcFrame.left + nPaperNameWidth - nBorder;
	rcText.top += nBorder; rcText.bottom = rcText.top + nTextHeight;
	CString strOut = rFlatProduct.m_strPaperName;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPressNameWidth - nBorder;
	strOut = rFlatProduct.m_strPressName;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nLayoutNameWidth - nBorder;
	strOut = rFlatProduct.m_strLayoutName;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPaperFormatWidth - nBorder;
	strOut = rFlatProduct.m_strFormatName;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nNumFormsWidth - nBorder;
	strOut.Format(_T("%d"), rFlatProduct.m_nNumSheets);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nQuantityTotalWidth - nBorder;
	strOut.Format(_T("%d"), rFlatProduct.m_nQuantityTotal);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	if (nPaperTypeTotal > 0)
	{
		pDC->SelectObject(&boldFont);
		strOut.Format(_T("%d"), nPaperTypeTotal);
		CRect rcText2 = rcText; rcText2.OffsetRect(0, nTextHeight + 2 * nBorder);
		pDC->SetTextColor(GUICOLOR_CAPTION);
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		pDC->SelectObject(&smallFont);
		pDC->SetTextColor(crValueColor);

		CPen pen(PS_SOLID, (pDC->IsPrinting()) ? Pixel2MM(pDC, 2) : 3, (pDC->IsPrinting()) ? LIGHTGRAY : TABLECOLOR_SEPARATION_LINE);
		CPen*  pOldPen	  = pDC->SelectObject(&pen);
		pDC->MoveTo(pDC->IsPrinting() ? rcFrame.left : 0, rcText.bottom + nBorder + 1); pDC->LineTo(rcFrame.right, rcText.bottom + nBorder + 1);
		pen.DeleteObject();
		pen.CreatePen(PS_SOLID, Pixel2MM(pDC, 2), LIGHTBLUE);
		pDC->SelectObject(&pen);
		pDC->MoveTo(rcText.left - nBorder, rcText.bottom + nBorder + 2); pDC->LineTo(rcText.right - nBorder, rcText.bottom + nBorder + 2);
		pDC->SelectObject(pOldPen);
		pen.DeleteObject();
	}

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nCommentWidth - nBorder;
	strOut = rFlatProduct.m_strComment;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.right	 = max(rcBox.right, rcText.right);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcFrame, (void*)&rFlatProduct, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PaperListEntryComment), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
		}
	}

	rcBox.bottom += nBorder;

	smallFont.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}
