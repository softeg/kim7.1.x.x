#pragma once

class CCSVInput
{
public:
	CCSVInput(void);
	CCSVInput(int nProductPartIndex);
	~CCSVInput(void);


public:
	CStdioFile		m_CSVFile;
	int				m_nProductPartIndex;
	CPageSource*	m_pPageSource;

	CString			m_strProductID;
	CString			m_strSubProductName;
	CString			m_strProductType;
	CString			m_strComment;
	float			m_fTrimSizeWidth;
	float			m_fTrimSizeHeight;
	int				m_nDesiredQuantity;
	CString			m_strPaper;
	int				m_nPaperGrain;
	int				m_nSides;
	int				m_nNumPages;	
	int				m_nStartPage;
	CColorantList	m_colorantList;
	CString			m_strContentFilePath;
	float			m_fLeftTrim;	
	float			m_fBottomTrim;
	float			m_fRightTrim;
	float			m_fTopTrim;
	COleDateTime	m_printDeadline;
	CString			m_strBindingOrder;
	CString			m_strPunchTool;
	
public:
	BOOL			DoImport(const CString& strFilePath);
	CString			GetNextToken(CString& strData, int& nStartPos);
	int				GetPaperGrain(CString strPaperGrain);
	CColorantList	GetColors(CString strColorCode);
};
