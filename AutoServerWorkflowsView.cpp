// AutoServerView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "AutoServerWorkflowsView.h"
#include "AutoServerLogsView.h"
#include "AutoServerDetailsView.h"
#include "AutoServerToolsView.h"
#include "DlgAutoServerWorkflow.h"
#include "GraphicComponent.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PageListFrame.h"


#define IDC_AUTOSERVERVIEW_HEADER 12121


// CAutoServerWorkflowsView

IMPLEMENT_DYNCREATE(CAutoServerWorkflowsView, CScrollView)

CAutoServerWorkflowsView::CAutoServerWorkflowsView()
{
    m_DisplayList.Create(this, FALSE, DISPLAY_LIST_TYPE(CAutoServerWorkflowsView));
	m_DisplayList.SetClassName(_T("CAutoServerWorkflowsView"));

	m_nHeaderItemsTotalWidth = 0;
	m_nHeaderHeight = 0;
	m_headerFont.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CAutoServerWorkflowsView::~CAutoServerWorkflowsView()
{
	m_headerFont.DeleteObject();
}

int	CAutoServerWorkflowsView::GetActJobID()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerWorkflowsView, Job));
	if (pDI)
		return (int)pDI->m_pData;

	kimAutoDB::_jobs jobs[1000];
	theApp.m_kaDB.ka_GetJobsAll(jobs, 1000);
	if (jobs[0].id >= 0)
		return jobs[0].id;

	return -1;
}

void CAutoServerWorkflowsView::DoDataExchange(CDataExchange* pDX)
{
	CScrollView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAutoServerWorkflowsView, CScrollView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY(HDN_TRACK,	IDC_AUTOSERVERVIEW_HEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK, IDC_AUTOSERVERVIEW_HEADER, OnHeaderEndTrack)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CAutoServerWorkflowsView-Diagnose

#ifdef _DEBUG
void CAutoServerWorkflowsView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CAutoServerWorkflowsView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG


// CAutoServerWorkflowsView-Meldungshandler

BOOL CAutoServerWorkflowsView::OnEraseBkgnd(CDC* pDC)
{
	m_toolBar.ClipButtons(pDC);

	CRect rect;
    GetClientRect(rect);
	rect.top = 0;
    //CBrush brush(RGB(230,233,238));
    //CBrush brush(RGB(240,241,244));//237,239,243));
    CBrush brush(WHITE);
    pDC->FillRect(&rect, &brush);

    brush.DeleteObject();

	return TRUE;

//	return CScrollView::OnEraseBkgnd(pDC);
}

int CAutoServerWorkflowsView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	//CREATE_INPLACE_EDIT_EX(InplaceEdit);
	//CREATE_INPLACE_COMBOBOX(InplaceComboBox, 0);

	return 0;
}

void CAutoServerWorkflowsView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
}

void CAutoServerWorkflowsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_headerColumnWidths.RemoveAll();
	m_HeaderCtrl.DestroyWindow();
	m_nHeaderHeight = 0;

	if ( ! m_HeaderCtrl.GetSafeHwnd())
	{
		m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, IDC_AUTOSERVERVIEW_HEADER);	
		m_HeaderCtrl.SetFont(&m_headerFont, FALSE);

		HD_ITEM hdi;
		hdi.mask		= HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
		hdi.fmt			= HDF_LEFT | HDF_STRING;

		m_headerColumnWidths.RemoveAll();

		int		nIndex = 0;
		CString string;
		string = "Workflow";//.LoadString(IDS_PRODUCT);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 200);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string = "Quelle";//.LoadString();
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 80);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_PRODUCTION_DATA);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 250);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string = "Anzahl Jobs";//.LoadString(IDS_PRODUCT);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 80);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string = "Hotfolder";//.LoadString(IDS_PRODUCT);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 300);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		m_nHeaderItemsTotalWidth = 0;
		for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
			m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];
	}

	OnSize(0, 0, 0);

	ModifyStyle(0, WS_CLIPCHILDREN);	// Clip header control 

	Invalidate();

	m_DisplayList.Invalidate();

	m_docSize = CalcExtents();
	SetScrollSizes(MM_TEXT, m_docSize);
}

void CAutoServerWorkflowsView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);

	if (m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdl;
		WINDOWPOS wp;
		hdl.prc = &clientRect;
		hdl.pwpos = &wp;
		m_HeaderCtrl.Layout(&hdl);	
		m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_nHeaderHeight = wp.cy;
	} 
}
// CAutoServerWorkflowsView-Zeichnung

CSize CAutoServerWorkflowsView::CalcExtents()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CRect rcBox = Draw(&dc, NULL);

	dc.DeleteDC();

	return CSize(rcBox.right + 10, rcBox.bottom + 10);
}

void CAutoServerWorkflowsView::OnDraw(CDC* pDC)
{
	CRect rcClientRect, rcClip;
	GetClientRect(rcClientRect); rcClip = rcClientRect;
	rcClip.top = m_nHeaderHeight;
	rcClip.OffsetRect(-pDC->GetViewportOrg());
	pDC->IntersectClipRect(rcClip);

	Draw(pDC, &m_DisplayList);
}

CRect CAutoServerWorkflowsView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	CRect rcBox(0,0,0,0);

	return DrawWorkflowsTable(pDC, pDisplayList);
}

CRect CAutoServerWorkflowsView::DrawWorkflowsTable(CDC* pDC, CDisplayList* pDisplayList)
{
	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	CRect rcFrame;
	GetClientRect(rcFrame); 
	
	rcFrame.top += m_nHeaderHeight;	
	rcFrame.right = rcFrame.left + 500;
	rcFrame.top += 5;
	rcFrame.left += 5;

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nColIndex = 0;
	ResetDisplayColums();
	AddDisplayColumn(ColWorkflow,						m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColWorkflowJobDataSource,			m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColWorkflowStrippingDataSource,	m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColWorkflowJobs,					m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColWorkflowHotfolder,				m_headerColumnWidths[nColIndex++]);

	CGraphicComponent gc;

	kimAutoDB::_workflows workflows[1000];
	theApp.m_kaDB.ka_GetWorkflows(workflows, 1000);

	CRect rcRow = rcFrame; rcRow.top += 5; rcRow.bottom = rcRow.top + 25;
	for (int nWorkflowIndex = 0; nWorkflowIndex < 1000; nWorkflowIndex++)
	{
		if (workflows[nWorkflowIndex].name.empty())
			continue;

		/////// columns
		CRect rcColumn = rcRow;  
		rcColumn.left += 5;

		int	  nRight = 0;
		CRect rcRet, rcThumbnail;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColWorkflow:						rcRet = DrawWorkflowInfo				(pDC, rcColumn, workflows[nWorkflowIndex], pDisplayList); break;
			case ColWorkflowJobDataSource:			rcRet = DrawWorkflowJobDataSource		(pDC, rcColumn, workflows[nWorkflowIndex], pDisplayList); break;
			case ColWorkflowStrippingDataSource:	rcRet = DrawWorkflowStrippingDataSource	(pDC, rcColumn, workflows[nWorkflowIndex], pDisplayList); break;
			case ColWorkflowJobs:					rcRet = DrawWorkflowJobsInfo			(pDC, rcColumn, workflows[nWorkflowIndex], pDisplayList); break;
			case ColWorkflowHotfolder:				rcRet = DrawWorkflowHotfolderInfo		(pDC, rcColumn, workflows[nWorkflowIndex], pDisplayList); break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + 5;
		}

		//if (pDisplayList)
		//{
		//	CRect rcDI = rcThumbnail; 
		//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nGlobalProductPartIndex, DISPLAY_ITEM_REGISTRY(CAutoServerWorkflowsView, BoundProductPart), (void*)nProductIndex, CDisplayItem::ForceRegister); // register item into display list
		//	if (pDI)
		//	{
		//		pDI->SetMouseOverFeedback();
		//		pDI->SetNoStateFeedback();
		//		if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
		//		{
		//			CRect rcFeedback = pDI->m_BoundingRect; 
		//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
		//			COLORREF crHighlightColor = RGB(255,195,15);
		//			gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
		//		}
		//	}
		//}

		rcRow.OffsetRect(0, rcRow.Height() + 5);
	}


	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CAutoServerWorkflowsView::DrawWorkflowInfo(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CImage img;
	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_WORKFLOW));
	CRect rcImg(CPoint(rcFrame.left, rcFrame.top), CSize(img.GetWidth(), img.GetHeight()));
	img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_CAPTION);

	CRect rcTitle = rcFrame; rcTitle.left += 20; rcTitle.bottom = rcTitle.top + nTextHeight;
	DrawTextMeasured(pDC, rWorkflow.name.c_str(), rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	if (pDisplayList)
	{
		CRect rcDI = rcBox; rcDI.left -= 4; rcDI.top -= 3; rcDI.right += 2; rcDI.bottom += 1; 
		rcDI.OffsetRect(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)); 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)rWorkflow.id, DISPLAY_ITEM_REGISTRY(CAutoServerWorkflowsView, Workflow), (void*)0, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
			{
				CRect rcFeedback = pDI->m_BoundingRect; 
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				COLORREF crHighlightColor = RGB(255,195,15);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
			}
		}
	}

	return rcBox;
}

CRect CAutoServerWorkflowsView::DrawWorkflowJobDataSource(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString strOut;
	switch (rWorkflow.jobDataSource)
	{
	case kimAutoDB::CSVFile:	strOut = "CSV"; break;
	case kimAutoDB::XMLFile:	strOut = "XML"; break;
	case kimAutoDB::JDFFile:	strOut = "JDF"; break;
	}

	CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + nTextHeight;
	DrawTextMeasured(pDC, strOut, rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CAutoServerWorkflowsView::DrawWorkflowStrippingDataSource(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString strOut, strProfile; strProfile.LoadString(IDS_PRODUCTION_DATA);
	switch (rWorkflow.strippingDataSource)
	{
	//case kimAutoDB::ProductionProfile:	strOut.Format("%s [%s]",		rWorkflow.strippingDataSourceName.c_str(), strProfile);		break;
	case kimAutoDB::ProductionProfile:	strOut.Format("%s",				rWorkflow.strippingDataSourceName.c_str());		break;
	case kimAutoDB::JobDataSource:		strOut = "-";																	break;
	case kimAutoDB::JobTemplate:		strOut.Format("%s [Template]",	rWorkflow.strippingDataSourceName.c_str());		break;
	}

	CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + nTextHeight;
	DrawTextMeasured(pDC, strOut, rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CAutoServerWorkflowsView::DrawWorkflowHotfolderInfo(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString strPath(rWorkflow.hotFolderPath.c_str());
	TCHAR szPath[_MAX_PATH]; strcpy(szPath, strPath.GetBuffer());
	PathStripPath(szPath);
	strPath.Format(_T("\\\\%s\\KimAutoInput\\%s"), theApp.settings.m_szAutoServerNameIP, szPath);

	CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + nTextHeight;
	DrawTextMeasured(pDC, strPath, rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CAutoServerWorkflowsView::DrawWorkflowJobsInfo(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	kimAutoDB::_jobs jobs[1000];
	theApp.m_kaDB.ka_GetJobs(jobs, rWorkflow.id, 1000);
	int nNumJobs = 0;
	for (int nJobIndex = 0; ( ! jobs[nJobIndex].Name.empty()) && (nJobIndex < 1000); nJobIndex++)
	{
		nNumJobs++;
	}

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	CString strOut; strOut.Format(_T("%d"), nNumJobs);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();


	return rcBox;
}

void CAutoServerWorkflowsView::ResetDisplayColums()
{
	m_displayColumns.RemoveAll();
}

void CAutoServerWorkflowsView::AddDisplayColumn(int nColID, int nWidth)
{
	struct displayColumn dc = {nColID, nWidth};
	m_displayColumns.Add(dc);
}
// CAutoServerWorkflowsView-Meldungshandler

void CAutoServerWorkflowsView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_DisplayList.OnLButtonDown(point);

	CScrollView::OnLButtonDown(nFlags, point);
}

void CAutoServerWorkflowsView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
    OnLButtonDown(nFlags, point);	// Handle second click 

	CScrollView::OnLButtonDblClk(nFlags, point);
}

void CAutoServerWorkflowsView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list
	CPoint pt = point;
	ClientToScreen(&pt);
	OnContextMenu(NULL, pt);
}

void CAutoServerWorkflowsView::OnContextMenu(CWnd*, CPoint point)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int			  nMenuID = -1;
	CDisplayItem* pDI	  = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerWorkflowsView, BoundProduct));
	if (pDI)
	{
		nMenuID = IDR_PRODUCTSVIEW_POPUP;
	}
	//else
	//{
	//	pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerWorkflowsView, BoundProduct));
	//	if (pDI)
	//		nMenuID = ;
	//}

	if (nMenuID < 0)
		return;

    CMenu menu;
	VERIFY(menu.LoadMenu(nMenuID));

	CMenu* pPopup = menu.GetSubMenu(0);
	if ( ! pPopup)
		return;

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
}

void CAutoServerWorkflowsView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

void CAutoServerWorkflowsView::OnMouseLeave()
{
	m_DisplayList.OnMouseLeave();

	CScrollView::OnMouseLeave();
}


void CAutoServerWorkflowsView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 20)
		hdn->pitem->cxy = 20;
}

void CAutoServerWorkflowsView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	SetHeaderColumnWidth(hdn->iItem, hdn->pitem->cxy);

	if (m_headerColumnWidths.GetSize() > hdn->iItem)
		m_headerColumnWidths[hdn->iItem] = hdn->pitem->cxy; 

	m_nHeaderItemsTotalWidth = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
		m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];

	CRect clientRect;
	GetClientRect(&clientRect);

	InvalidateRect(clientRect);
	UpdateWindow(); 
}

void CAutoServerWorkflowsView::SetHeaderColumnWidth(int nIndex, int nValue)
{
	if (nIndex >= 20)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\AutoServerWorkflowsViewHeader");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			columnWidthArray[nIndex] = nValue;			
		else
		{
			ZeroMemory(columnWidthArray, sizeof(columnWidthArray));
			columnWidthArray[nIndex] = nValue;			
		}

		dwSize = sizeof(columnWidthArray);
		RegSetValueEx(hKey, _T("Columns"), 0, REG_BINARY, (LPBYTE)&columnWidthArray, dwSize);

		RegCloseKey(hKey);
	}
}

int CAutoServerWorkflowsView::GetHeaderColumnWidth(int nIndex, int nDefault)
{
	if (nIndex >= 20)
		return nDefault;

	int nWidth = nDefault;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\AutoServerWorkflowsViewHeader");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			nWidth = columnWidthArray[nIndex];			
		else
			nWidth = nDefault;

		RegCloseKey(hKey);
	}

	if ( (nWidth <= 10) || (nWidth > 1000) )
		nWidth = nDefault;

	return nWidth;
}

BOOL CAutoServerWorkflowsView::OnSelectWorkflow(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	return TRUE;
}

BOOL CAutoServerWorkflowsView::OnDeselectWorkflow(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	return TRUE;
}

BOOL CAutoServerWorkflowsView::OnActivateWorkflow(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	pDI->m_nState = CDisplayItem::Normal;

	kimAutoDB::_workflows workflow;

	theApp.m_kaDB.ka_GetWorkflow(&workflow, (int)pDI->m_pData);

	CDlgAutoServerWorkflow dlg;
	dlg.m_strName 				= workflow.name.c_str();
	dlg.m_nJobdataSource		= workflow.jobDataSource;
	dlg.m_strProductionProfile	= workflow.strippingDataSourceName.c_str();
	dlg.m_nDBAttribute			= workflow.attribute;
	dlg.m_nDBActionWhenFinished	= workflow.actionWhenFinished;
	dlg.m_nDBActionDelayMinutes = workflow.actionDelayMinutes;

	if (dlg.DoModal() == IDOK)
	{
		workflow.actionWhenFinished = dlg.m_nDBActionWhenFinished;
		workflow.actionDelayMinutes = dlg.m_nDBActionDelayMinutes;
		theApp.m_kaDB.ka_UpdateWorkflow(workflow.id, dlg.m_strName.GetBuffer(), workflow.status, kimAutoDB::Hotfolder, dlg.m_nJobdataSource, kimAutoDB::ProductionProfile, dlg.m_strProductionProfile.GetBuffer(), _T(""), workflow.hotFolderPath, 
										workflow.fileNameFilter, dlg.m_nDBAttribute, workflow.actionWhenFinished, workflow.actionDelayMinutes, workflow.numJobs);
		theApp.m_kaDB.ka_SetDataChangedByClient();
	}

	Invalidate();
	m_DisplayList.Invalidate();
	UpdateWindow();

	return TRUE;
}

BOOL CAutoServerWorkflowsView::OnMouseMoveWorkflow(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				COLORREF crHighlightColor = RGB(165,175,200);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, pDI->m_BoundingRect, crHighlightColor, TRUE);	
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CAutoServerWorkflowsView::OnStatusChanged()	
{
	return TRUE;
}
