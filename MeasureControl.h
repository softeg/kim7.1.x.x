
/////////////////////////////////////////////////////////////////////////////
// special data exchange for measurement edit controls


// declarations


#define NO_SUPPORTED_UNITS 3
#define MILLIMETER 0
#define CENTIMETER 1
#define INCH	   2
#define NO_UNIT	   3



extern TCHAR   szSupportedUnits[NO_SUPPORTED_UNITS][3];
extern CString strDummy;

void AFXAPI DDX_Measure(CDataExchange* pDX, int nIDC, float& fValue, int nPrecision = -1);
BOOL GetMeasure(HWND hWnd, float& fValue, CString& rstrValue = strDummy);
BOOL SeparateMeasureString(TCHAR *szValuePart, TCHAR *szUnitPart);
BOOL InFirstOfUnit(TCHAR cChar);
int DetermineUnit(TCHAR *szString);
void SetMeasure(HWND hWnd, float fValue, CString& rstrValue = strDummy, BOOL bNoUnit = FALSE, int nPrecision = -1);
CString GetValueString(float fValue, int nPrecision = -1);
CString GetUnitString();
void TrimMeasure(TCHAR *szValueString);
void SpinMeasure(CWnd *pWnd, int iDelta, float fStep = 0.5f, CString& rstrValue = strDummy, BOOL bNoUnit = FALSE);
float MeasureFormat(float fFloatValue);
CString MeasureFormat(CString strFormat);
