#if !defined(AFX_THEMEDTABCTRL_H__BDBF7DF1_036D_4F3C_9D6B_9FE798948686__INCLUDED_)
#define AFX_THEMEDTABCTRL_H__BDBF7DF1_036D_4F3C_9D6B_9FE798948686__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThemedTabCtrl.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Fenster CThemedTabCtrl 

class CThemedTabCtrl : public CTabCtrl
{
// Konstruktion
public:
	CThemedTabCtrl();

// Attribute
public:
	HBRUSH m_hBrush;

// Operationen
public:
	void GetThemedTabCtrlBackground();


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CThemedTabCtrl)
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~CThemedTabCtrl();

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CThemedTabCtrl)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_THEMEDTABCTRL_H__BDBF7DF1_036D_4F3C_9D6B_9FE798948686__INCLUDED_
