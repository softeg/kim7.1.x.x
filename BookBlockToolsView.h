#pragma once


#include "ToolBarGroup.h"
#include "OwnerDrawnButtonList.h"
#include "MyBitmapButton.h"


// CBookBlockToolsView-Formularansicht

class CBookBlockToolsView : public CFormView
{
	DECLARE_DYNCREATE(CBookBlockToolsView)

protected:
	CBookBlockToolsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CBookBlockToolsView();

public:
	enum { IDD = IDD_BOOKBLOCKTOOLSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	class CBookblockFrame*		m_pBookblockFrame;
	//CToolBarGroup				m_toolBarGroup;
	COwnerDrawnButtonList		m_toolBar;
	CMyBitmapButton				m_gotoPrevPageButton, m_gotoNextPageButton;



protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnUpdateZoomIncrease(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomDecrease(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomToFit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScrollHand(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBookblockProof(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBookblockProofOpenfile(CCmdUI* pCmdUI);
	afx_msg void OnGotoPrevPage() ;
	afx_msg void OnGotoNextPage() ;
	afx_msg void OnUpdateGotoPrevButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateGotoNextButton(CCmdUI* pCmdUI);
};


