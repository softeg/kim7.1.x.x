// PrintSheetNavigationView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "PrintSheetNavigationView.h"
#include "NewPrintSheetFrame.h"
#include "MainFrm.h"
#include "GraphicComponent.h"
#include "DlgPressDevice.h"
#include "DlgJobData.h"
#include "BookBlockView.h"
#include "BookBlockFrame.h"
#include "ModalFrame.h"
#include "PrintSheetTableView.h"
#include "PrintComponentsView.h"


// CPrintSheetNavigationView

IMPLEMENT_DYNCREATE(CPrintSheetNavigationView, CScrollView)

CPrintSheetNavigationView::CPrintSheetNavigationView() 
{
	m_boldFont.CreateFont (12, 0, 0, 0, FW_BOLD,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_smallFont.CreateFont(11, 0, 0, 0, FW_NORMAL,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
    m_DisplayList.Create(this, TRUE);
	m_DisplayList.SetClassName(_T("CPrintSheetNavigationView"));
	m_nToolBarHeight = 34;
	m_nProductPartIndex = -1;
	m_nActPrintSheetIndex = -1;
	m_nLastPrintSheetIndex = -1;
	m_nLastComponentIndex = -1;
}

CPrintSheetNavigationView::~CPrintSheetNavigationView()
{
	m_boldFont.DeleteObject();
	m_smallFont.DeleteObject();
}


BEGIN_MESSAGE_MAP(CPrintSheetNavigationView, CScrollView)
    ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_ERASEBKGND()
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	ON_WM_CREATE()
END_MESSAGE_MAP()


// CPrintSheetNavigationView-Zeichnung

int CPrintSheetNavigationView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	if( m_dropTarget.Register( this ) )
		return 0;
	else
		return -1;

	return 0;
}

BOOL CPrintSheetNavigationView::OnEraseBkgnd(CDC* pDC)
{
    CRect rcFrame;
	GetClientRect(rcFrame); rcFrame.bottom++;

	rcFrame.top = m_nToolBarHeight;

//    pDC->FillSolidRect(rcFrame, RGB(227,232,238));//RGB(211,218,228));

	CRect rcHeader = rcFrame;
	rcHeader.top = 0; rcHeader.bottom = m_nToolBarHeight;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);

	rcHeader.top = rcHeader.bottom - 3;
	pDC->FillSolidRect(rcHeader, RGB(215,220,225));

	return TRUE; //CScrollView::OnEraseBkgnd(pDC);
}

void CPrintSheetNavigationView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
}

void CPrintSheetNavigationView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	CalcExtents();

	CSize sizeAll(m_docTotalSize.cx, m_docTotalSize.cy + m_nToolBarHeight);
	SetScrollSizes(MM_TEXT, sizeAll, m_docPageSize, m_docLineSize);

	if (GetActSight() == ALLSIDES)
	{
		CDisplayItem* pDICurSel= m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame));
		while (pDICurSel)
		{
			pDICurSel->m_nState = CDisplayItem::Normal;	 
			m_DisplayList.InvalidateItem(pDICurSel, FALSE, FALSE);
			pDICurSel= m_DisplayList.GetNextSelectedItem(pDICurSel, DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame));
		}
	}

	Invalidate();
}

CSize CPrintSheetNavigationView::CalcExtents()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	m_docTotalSize = Draw(&dc, NULL).Size();

	CRect rcClient;
	GetClientRect(rcClient);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	int nNumSheets = (pDoc) ? max(1, pDoc->m_PrintSheetList.GetSize()) : 1;

	m_docPageSize.cx = m_docLineSize.cx = m_docTotalSize.cx = 1;

	m_docPageSize.cy = rcClient.Height() - m_nToolBarHeight;
	m_docLineSize.cy = m_docTotalSize.cy / nNumSheets;

	dc.DeleteDC();

	return m_docTotalSize;
}

void CPrintSheetNavigationView::ClipHeader(CDC* pDC)
{
	CRect rcClientRect;
	GetClientRect(rcClientRect); 
	CRect clipBox	= rcClientRect;
	clipBox.top		= m_nToolBarHeight;
	clipBox.OffsetRect(-pDC->GetViewportOrg());
	pDC->IntersectClipRect(clipBox);
}

void CPrintSheetNavigationView::OnDraw(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(rcClient);
	CRect rcFrame(CPoint(0, 0), CSize(rcClient.Width(), max(rcClient.Height(), m_docTotalSize.cy + m_nToolBarHeight)));

	CDC	memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap bitmap;	bitmap.CreateCompatibleBitmap(pDC, rcFrame.Width(), rcFrame.Height());
	memDC.SelectObject(&bitmap);

	memDC.FillSolidRect(rcFrame, RGB(227,232,238));

	memDC.SaveDC();
	Draw(&memDC, &m_DisplayList);
	memDC.RestoreDC(-1);
	
	pDC->SaveDC();	// save current clipping region
	ClipHeader(pDC);

	pDC->BitBlt(0, m_nToolBarHeight, rcFrame.Width(), rcFrame.Height(), &memDC, 0, m_nToolBarHeight, SRCCOPY);

	bitmap.DeleteObject();
	memDC.DeleteDC();

	pDC->RestoreDC(-1);

	if (GetActPrintSheet())
	{
		if (GetActPrintSheet()->IsEmpty())
		{
			m_nActPrintSheetIndex = -1;
			for (int i = 0; i < m_DisplayList.m_List.GetSize(); i++)
				m_DisplayList.m_List[i].m_nState = CDisplayItem::Normal;
		}
	}
}

CRect CPrintSheetNavigationView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	CRect rcClient; GetClientRect(rcClient);
	CRect rcFrame = rcClient;
	rcFrame.top += m_nToolBarHeight; 

	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	rcBox = DrawSheets(pDC, rcFrame, pDisplayList);

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CPrintSheetNavigationView::DrawSheets(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CPrintSheet*			pActPrintSheet		  = GetActPrintSheet();
	CLayout*				pActLayout			  = GetActLayout();
	CPrintComponent			component			  = GetActPrintComponent();
	CPrintComponentsGroup&	rGroup				  = component.GetPrintComponentsGroup();
	int						nActPrintSheetNum	  = (pActPrintSheet) ? pActPrintSheet->GetNumberOnly() : 0;
	BOOL					bActPrintSheetIsEmpty = (pActPrintSheet) ? ((pActPrintSheet->IsEmpty()) ? TRUE : FALSE) : TRUE;

	CPen   pen		(PS_SOLID, 4, RGB(255,220,180));
	CPen   groupPen	(PS_SOLID, 4, RGB(244,240,236));
	CPen*  pOldPen  = pDC->SelectObject(&pen);
	CFont* pOldFont = pDC->SelectObject(&m_smallFont);
	pDC->SelectStockObject(HOLLOW_BRUSH);
	pDC->SetTextColor(BLACK);
	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(WHITE);

	CGraphicComponent gp;
	CLayout* pPrevLayout = NULL;
	int		 nPrintSheetIndex = -1, nPrevPrintSheetNumber = 0;
	CRect	 rcSheetBox = rcFrame; rcSheetBox.top;// += 10;

	BOOL	 bImposing = FALSE;
	BOOL	 bIsFirst  = TRUE;
	POSITION pos	   = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet&  rPrintSheet  = pDoc->m_PrintSheetList.GetNext(pos);
		CLayout*	  pLayout	   = rPrintSheet.GetLayout();
		nPrintSheetIndex = rPrintSheet.GetNumberOnly();

		if ( rPrintSheet.IsEmpty())
			continue;

		nPrevPrintSheetNumber = nPrintSheetIndex;

		rcSheetBox.DeflateRect(10,10);

		if (pDoc->m_PrintSheetList.GetSize() > 0)
			rcSheetBox = pLayout->Draw(pDC, rcSheetBox, &rPrintSheet, NULL, TRUE, FALSE, FALSE, NULL, TRUE, FRONTSIDE, NULL, NULL, pDisplayList, (pLayout == pActLayout) ? TRUE : FALSE);
		else
			rcSheetBox.bottom = rcSheetBox.top;

		CRect rcBorder = rcSheetBox; rcBorder.InflateRect(4,4); rcBorder.bottom++;
		if ( (pLayout == pActLayout) || (&rGroup == &rPrintSheet.GetFirstPrintComponentsGroup()) )
		{
			pDC->SelectObject(&groupPen);
			CPrintSheetView::DrawRectangleExt(pDC, rcBorder);
			rcBorder.InflateRect(4,4);
			CPrintSheetView::DrawRectangleExt(pDC, rcBorder);
		}
		if ( (nActPrintSheetNum == rPrintSheet.GetNumberOnly()) && ! bActPrintSheetIsEmpty)
		{
			rcBorder = rcSheetBox; rcBorder.InflateRect(4,4); rcBorder.left++; rcBorder.top++; 
			pDC->SelectObject(&pen);
			CPrintSheetView::DrawRectangleExt(pDC, rcBorder);
			pDC->SetTextColor(RGB(255,100,15));
			pDC->SelectObject(&m_boldFont);
		}
		else
		{
			pDC->SetTextColor(BLACK);
			pDC->SelectObject(&m_smallFont);
		}

		if (pDisplayList)
		{
			CRect rcDI = rcBorder; rcDI.left--; rcDI.top--; rcDI.InflateRect(4,4);
			int nItemType = (nActPrintSheetNum == rPrintSheet.GetNumberOnly()) ? CDisplayItem::CtrlDeselectType | CDisplayItem::ForceRegister | CDisplayItem::SetSelected | CDisplayItem::NoFeedback
																  			   : CDisplayItem::CtrlDeselectType | CDisplayItem::ForceRegister | CDisplayItem::NoFeedback;
			rcDI.OffsetRect(0, -GetScrollPos(SB_VERT));
			pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nPrintSheetIndex, DISPLAY_ITEM_REGISTRY(CPrintSheetNavigationView, SheetFrame), (void*)pLayout, nItemType); 
		}

		CRect rcTitle = rcSheetBox; rcTitle.DeflateRect(2, 1);
		pDC->DrawText(rPrintSheet.GetNumber(), rcTitle, DT_LEFT | DT_BOTTOM | DT_SINGLELINE | DT_END_ELLIPSIS);

		if (pLayout != pActLayout)
		{
			CRect rcFrame = rcSheetBox; rcFrame.InflateRect(5,5);
			int nAlpha = (bActPrintSheetIsEmpty) ? 180 : 50;
			CPrintSheetView::FillTransparent(pDC, rcFrame, RGB(220,230,250), TRUE, nAlpha);
		}

		rcBox.UnionRect(rcBox, rcSheetBox);

		rcSheetBox = rcFrame; rcSheetBox.top = rcBox.bottom + 10; rcSheetBox.bottom = rcSheetBox.top + rcFrame.Width();

		pPrevLayout = pLayout;
	}

	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pOldPen);

	pen.DeleteObject();
	groupPen.DeleteObject();

	rcBox.bottom += 20;

	return rcBox;
}


// This functions are overridden in order to:
//	- implement header control's horizontal scrolling

void CPrintSheetNavigationView::ScrollToPosition(POINT pt)    // logical coordinates
{
	ASSERT(m_nMapMode > 0);     // not allowed for shrink to fit
	if (m_nMapMode != MM_TEXT)
	{
		CWindowDC dc(NULL);
		dc.SetMapMode(m_nMapMode);
		dc.LPtoDP((LPPOINT)&pt);
	}

	// now in device coordinates - limit if out of range
	int xMax = GetScrollLimit(SB_HORZ);
	int yMax = GetScrollLimit(SB_VERT);
	if (pt.x < 0)
		pt.x = 0;
	else if (pt.x > xMax)
		pt.x = xMax;
	if (pt.y < 0)
		pt.y = 0;
	else if (pt.y > yMax)
		pt.y = yMax;

	ScrollToDevicePosition(pt);
}

void CPrintSheetNavigationView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);
	
	CRect scrollRect, clipRect;
	GetClientRect(scrollRect); clipRect = scrollRect;
	clipRect.top += m_nToolBarHeight;	// Protect header control from being vertically scrolled
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, clipRect);

	//if (m_HeaderCtrl.GetSafeHwnd())
	//	if (ptDev.x != 0)				// Scroll header control horizontally
	//		m_HeaderCtrl.MoveWindow(-ptDev.x, m_nToolBarHeight, scrollRect.Width() + ptDev.x, 0);

	//if (m_InplaceEdit.GetSafeHwnd())
	//	m_InplaceEdit.Deactivate();
}

BOOL CPrintSheetNavigationView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect, clipRect;
		GetClientRect(scrollRect); clipRect = scrollRect;
		//if (m_HeaderCtrl.GetSafeHwnd())
			clipRect.top += m_nToolBarHeight;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, clipRect);

		//if (m_HeaderCtrl.GetSafeHwnd())
		//	if (sizeScroll.cx != 0)				// Scroll header control horizontally
		//		m_HeaderCtrl.MoveWindow(-x, m_nToolBarHeight, scrollRect.Width() + x, 0);

		//if (m_InplaceEdit.GetSafeHwnd())
		//	m_InplaceEdit.Deactivate();
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

// CPrintSheetNavigationView-Diagnose

#ifdef _DEBUG
void CPrintSheetNavigationView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetNavigationView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrintSheetNavigationView-Meldungshandler

BOOL CPrintSheetNavigationView::IsOpenDlgComponentImposer()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	return pFrame->IsOpenDlgComponentImposer();
}

CDlgComponentImposer* CPrintSheetNavigationView::GetDlgComponentImposer()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	return pFrame->GetDlgComponentImposer();
}

BOOL CPrintSheetNavigationView::IsOpenDlgFoldSchemeSelector()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	return pFrame->IsOpenDlgFoldSchemeSelector();
}

CPrintSheet* CPrintSheetNavigationView::GetActPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if (pDoc->m_PrintSheetList.GetSize() <= 0)
		return NULL;

	int nIndex = m_nActPrintSheetIndex;

	if (nIndex < 0)		// nothing selected
		if (m_nLastPrintSheetIndex > 0)//>= 0)
			nIndex = m_nLastPrintSheetIndex;

	if (nIndex <= 0)//< 0)		// nothing selected
	{
		//nIndex = 0;
		CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
		if (pFrame)
		{
			if (pFrame->m_nViewSide == ALLSIDES)
				return NULL;

			CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame));
			if (pDI)
			{
				pDI->m_nState = CDisplayItem::Selected;
				m_DisplayList.InvalidateItem(pDI, FALSE, FALSE);
				nIndex = (int)pDI->m_pData;
			}
		}
		if (nIndex <= 0)
		{
			BOOL bNonEmptyFound = FALSE;
			POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
			while (pos)
			{
				CPrintSheet&  rPrintSheet  = pDoc->m_PrintSheetList.GetNext(pos);
				if ( ! rPrintSheet.IsEmpty())
				{
					nIndex = rPrintSheet.GetNumberOnly();
					bNonEmptyFound = TRUE;
					break;
				}
			}
			if ( ! bNonEmptyFound)
				nIndex = pDoc->m_PrintSheetList.GetHead().GetNumberOnly();
		}
	}

	if (nIndex > pDoc->m_PrintSheetList.GetSize())
		nIndex = pDoc->m_PrintSheetList.GetTail().GetNumberOnly();

	m_nLastPrintSheetIndex = nIndex;
	m_nActPrintSheetIndex  = nIndex;

	return pDoc->m_PrintSheetList.GetPrintSheetFromNumber(nIndex);
}

CLayout* CPrintSheetNavigationView::GetActLayout()
{
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	return (pPrintSheet) ? (CLayout*)pPrintSheet->GetLayout() : NULL;
}

CPressDevice* CPrintSheetNavigationView::GetActPressDevice()
{
	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
		return NULL;

	switch (GetActSight())
	{
	case FRONTSIDE:
	case BOTHSIDES:		return pLayout->m_FrontSide.GetPressDevice();
	case BACKSIDE:		return pLayout->m_BackSide.GetPressDevice();
	}

	return NULL;
}

int CPrintSheetNavigationView::GetActSight()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if (pFrame)
		return pFrame->m_nViewSide;
	else
		return FRONTSIDE;
}

BOOL CPrintSheetNavigationView::SetActPrintSheet(CPrintSheet* pActPrintSheet, BOOL bNoUpdate)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CPrintSheet* pPrevPrintSheet = GetActPrintSheet();

	int nPrintSheetIndex = (pActPrintSheet) ? pActPrintSheet->GetNumberOnly() : 0;//pDoc->m_PrintSheetList.GetPrintSheetIndex(pActPrintSheet);

	m_nActPrintSheetIndex = nPrintSheetIndex;

	CDisplayItem* pDI = (pActPrintSheet) ? m_DisplayList.GetItemFromData((void*)nPrintSheetIndex, (void*)pActPrintSheet->GetLayout(), DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame)) : NULL;
	//if ( ! pDI)
	//{
	//	Invalidate();		// if switching to an empty sheet -> update all in order to lowlight them
	//	UpdateWindow();
	//	return FALSE; // printsheet view needs to be updated
	//}

	bNoUpdate = FALSE;
	
	m_DisplayList.Invalidate();
	OnSelectSheetFrame(pDI, CPoint(0,0), (LPARAM)bNoUpdate);

	EnsureVisiblePrintSheet(pActPrintSheet);

	return TRUE;	
}

void CPrintSheetNavigationView::EnsureVisiblePrintSheet(CPrintSheet* pPrintSheet)
{
	if ( ! pPrintSheet)
	{
		Invalidate();
		UpdateWindow();
		return;
	}

	CDisplayItem* pDI = m_DisplayList.GetItemFromData((void*)pPrintSheet->GetNumberOnly(), (void*)pPrintSheet->GetLayout(), DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame));
	if ( ! pDI)
		return;

	CRect rcClient; GetClientRect(rcClient); rcClient.top += m_nToolBarHeight;

	if ( ! rcClient.IntersectRect(pDI->m_BoundingRect, rcClient))// ! pDI->m_bIsVisible)	
	{
		Invalidate();
		OnVScroll(SB_THUMBTRACK, pDI->m_BoundingRect.top - 20, NULL);
	}
	else
	{
		Invalidate();
		UpdateWindow();
	}
}

CPrintComponent CPrintSheetNavigationView::GetActPrintComponent()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CPrintComponent();

	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return CPrintComponent();

	CPrintComponentsView* pView = pFrame->GetPrintComponentsView();
	if (pView)
		return pView->GetActComponent();
	else
		return CPrintComponent();
}

///////////////////////////////////////////////////////////////////
// mouse events

void CPrintSheetNavigationView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point);

    CScrollView::OnLButtonDown(nFlags, point);
}

void CPrintSheetNavigationView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

BOOL CPrintSheetNavigationView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// The default call of OnMouseWheel causes an assertion
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion

//	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}

BOOL CPrintSheetNavigationView::OnSelectLayoutBar(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	POSITION pos = pDoc->m_PrintSheetList.FindIndex((int)pDI->m_pData);
	if ( ! pos)
		return FALSE;

	SetActPrintSheet(&pDoc->m_PrintSheetList.GetAt(pos), FALSE);

	return TRUE;
}

BOOL CPrintSheetNavigationView::OnDeselectLayoutBar(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)		
{
	return TRUE;
}

BOOL CPrintSheetNavigationView::OnMouseMoveLayoutBar(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height() - 1);
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

extern BOOL g_bLockPrintGroupList;

BOOL CPrintSheetNavigationView::OnSelectSheetFrame(CDisplayItem* pDI, CPoint point, LPARAM lParam)	
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;

	if (point != CPoint(0,0) && ! IsOpenDlgComponentImposer())	// not coming from SetActPrintSheet()
	{
		CPrintComponentsView* pPrintComponentsView = pFrame->GetPrintComponentsView();
		if (pPrintComponentsView)
			pPrintComponentsView->m_DisplayList.DeselectAll(FALSE);
	}

	CPrintSheet* pPrevPrintSheet = pDoc->m_PrintSheetList.GetPrintSheetFromNumber(m_nLastPrintSheetIndex);
	CLayout*	 pPrevLayout	 = (pPrevPrintSheet) ? pPrevPrintSheet->GetLayout() : NULL;

	CDisplayItem* pDICurSel= m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame));
	while (pDICurSel)
	{
		m_DisplayList.InvalidateItem(pDICurSel, FALSE, FALSE);
		pDICurSel->m_nState = CDisplayItem::Normal;	 
		pDICurSel= m_DisplayList.GetNextSelectedItem(pDICurSel, DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame));
	}
	if (pDI)
	{
		m_DisplayList.InvalidateItem(pDI, FALSE, FALSE);
		pDI->m_nState = CDisplayItem::Selected;	 
		m_nActPrintSheetIndex = (int)pDI->m_pData;
	}

	CPrintSheet* pActPrintSheet  = pDoc->m_PrintSheetList.GetPrintSheetFromNumber(m_nActPrintSheetIndex);
	CLayout*	 pActLayout		 = (pActPrintSheet) ? pActPrintSheet->GetLayout() : NULL;

	CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	int bOutputProfileChecked = (pView) ? pView->m_bOutputProfileChecked : FALSE;	// if UpdateFrameLayout() this value gets lost

	if ( ! lParam)	// ! NoUpdate
	{
		if ( (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection != CMainFrame::NavSelectionPrintSheets) && 
			 (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection != CMainFrame::NavSelectionPrintSheetMarks) &&
			 (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection != CMainFrame::NavSelectionPrepress) &&
			 (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection != CMainFrame::NavSelectionOutput) )
			((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection = CMainFrame::NavSelectionPrintSheets; 
		
		if (pFrame->m_nViewSide == ALLSIDES)
			pFrame->OnViewFrontside();
		else
			pFrame->UpdateFrameLayout(FALSE);
	}

	pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pView)
	{
		pView->SelectPrintSheet(GetActPrintSheet());
		pView->m_bOutputProfileChecked = bOutputProfileChecked;
	}

	if (point != CPoint(0,0) && ! IsOpenDlgFoldSchemeSelector())	// not coming from SetActPrintSheet()
	{
		CPrintComponentsView* pPrintComponentsView = pFrame->GetPrintComponentsView();
		if (pPrintComponentsView)
		{
			CPrintComponent component = pDoc->m_printComponentsList.FindPrintSheet(pActPrintSheet);
			pPrintComponentsView->SetActComponent(component);
		}
	}

	Invalidate();
	UpdateWindow();

	if (IsOpenDlgComponentImposer())
	{
		if (GetDlgComponentImposer())
			GetDlgComponentImposer()->InitData(FALSE);
	}
	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pPrintSheetView)
	{
		pPrintSheetView->m_DisplayList.Invalidate();
		pPrintSheetView->Invalidate();
		pPrintSheetView->UpdateWindow();
	}
	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	theApp.UpdateView(RUNTIME_CLASS(CPrintComponentsDetailView));

	return TRUE;
}

BOOL CPrintSheetNavigationView::OnDeselectSheetFrame(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)		
{
	return TRUE;
}

BOOL CPrintSheetNavigationView::OnMouseMoveSheetFrame(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 		//	CDC* pDC = GetDC(); 
				//CRect trackerRect = pDI->m_BoundingRect;
				//trackerRect.left   -= (trackerRect.left%2)   ? 1 : 0;
				//trackerRect.top    -= (trackerRect.top%2)    ? 1 : 0;
				//trackerRect.right  -= (trackerRect.right%2)  ? 1 : 1;
				//trackerRect.bottom -= (trackerRect.bottom%2) ? 1 : 1;
				//trackerRect.DeflateRect(2, 2);
				////CRectTracker tracker(trackerRect, CRectTracker::dottedLine);
				////tracker.Draw(pDC);
				//pDI->m_nLastPaintedState = -1;

				//m_rcDIBackground = trackerRect;
				//m_rcDIBackground.InflateRect(18,18);
				//SaveBitmap(&m_bmDIBackground, m_rcDIBackground);
				//((CLayout*)pDI->m_pAuxData)->Draw(pDC, m_rcDIBackground, (CPrintSheet*)pDI->m_pData, NULL, FALSE, FALSE, NULL, TRUE, FRONTSIDE, NULL, NULL, NULL);				
				////RestoreBitmap(&m_bmDIBackground, m_rcDIBackground, TRUE);
				//ReleaseDC(pDC);
			}
			break;

	case 2:	// mouse leave
			//RestoreBitmap(&m_bmDIBackground, m_rcDIBackground);
			//m_DisplayList.InvalidateItem(pDI);
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

// bitmap save/restore
void CPrintSheetNavigationView::SaveBitmap(CBitmap* pBitmap, const CRect& rect)
{
	if (!pBitmap)
		return;

	pBitmap->DeleteObject();

	CClientDC dc(this);
 	CDC		  memDC;
	memDC.CreateCompatibleDC(&dc);
	pBitmap->CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(pBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), &dc, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void CPrintSheetNavigationView::RestoreBitmap(CBitmap* pBitmap, CRect rect, BOOL bInflate)
{
	if (!pBitmap)
		return;

	CClientDC dc(this);
	CDC		  memDC;
	memDC.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = memDC.SelectObject(pBitmap);
	if (bInflate)
		dc.StretchBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 3, 3, rect.Width() - 6, rect.Height() - 6, SRCCOPY);
	else
		dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}
///////////////////////////////////////////////////////////////////


DROPEFFECT CPrintSheetNavigationView::OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	return DROPEFFECT_NONE;
}

DROPEFFECT CPrintSheetNavigationView::OnDragEnterPrintComponentFoldSheet(CDisplayItem& sourceDI, DWORD dwKeyState, CPoint point)
{
	return DROPEFFECT_NONE;
}

DROPEFFECT CPrintSheetNavigationView::OnDragEnterPrintComponentPageSection(CDisplayItem& sourceDI, DWORD dwKeyState, CPoint point)
{
	return DROPEFFECT_NONE;
}

DROPEFFECT CPrintSheetNavigationView::OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	return DROPEFFECT_NONE;
}

DROPEFFECT CPrintSheetNavigationView::OnDragOverPrintComponentFoldSheet(CDisplayItem& sourceDI, DWORD dwKeyState, CPoint point)
{
	return DROPEFFECT_NONE;
}

void CPrintSheetNavigationView::OnDragLeave()
{
}

BOOL CPrintSheetNavigationView::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
{
	// TODO: F�gen Sie hier Ihren spezialisierten Code ein, und/oder rufen Sie die Basisklasse auf.

	return CScrollView::OnDrop(pDataObject, dropEffect, point);
}

void CPrintSheetNavigationView::UpdatePrintSheets(CLayout* pLayout)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	BOOL bDoUpdate = FALSE;
	if ( ! pLayout)
	{
		POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
		while (pos)
		{
			CPrintSheet&  rPrintSheet	    = pDoc->m_PrintSheetList.GetNext(pos);
			int			  nPrintSheetIndex	= rPrintSheet.GetNumberOnly();
			CDisplayItem* pDI				= m_DisplayList.GetItemFromData((void*)nPrintSheetIndex, (void*)rPrintSheet.GetLayout(), DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame));
			if (pDI)
			{
				bDoUpdate = TRUE;
				m_DisplayList.InvalidateItem(pDI, TRUE, FALSE);		// just repaint this printsheets instead of repainting the whole view (looks smarter)
			}
		}
	}
	else
	{
		CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
		while (pPrintSheet)
		{
			int			  nPrintSheetIndex	= pPrintSheet->GetNumberOnly();
			CDisplayItem* pDI				= m_DisplayList.GetItemFromData((void*)nPrintSheetIndex, (void*)pLayout, DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame));
			if (pDI)
			{
				bDoUpdate = TRUE;
				m_DisplayList.InvalidateItem(pDI, TRUE, FALSE);		// just repaint this printsheets instead of repainting the whole view (looks smarter)
			}
			else
				Invalidate();	// if no di -> update all

			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}
	}

	if (bDoUpdate)
		UpdateWindow();
}

void CPrintSheetNavigationView::InvalidateSheetFrame(CPrintSheet* pPrintSheet)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! pPrintSheet)
	{
		Invalidate();
		return;
	}

	int			  nPrintSheetIndex	= pPrintSheet->GetNumberOnly();
	CDisplayItem* pDI				= m_DisplayList.GetItemFromData((void*)nPrintSheetIndex, (void*)pPrintSheet->GetLayout(), DISPLAY_ITEM_TYPEID(CPrintSheetNavigationView, SheetFrame));
	if (pDI)
		pDI->Invalidate(m_DisplayList, TRUE, FALSE);		
}