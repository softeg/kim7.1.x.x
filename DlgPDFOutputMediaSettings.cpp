// DlgPDFOutputMediaSettings.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "DlgDirPicker.h"
#ifdef PDF
#include "PDFEngineInterface.h"
#include "PDFOutput.h"
#include "DlgPDFLibStatus.h"
#endif
#include "NewPrintSheetFrame.h"
#include "BookBlockDataView.h"
#include "GraphicComponent.h"


#ifdef CTP
#include "BmpOutput.h"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFOutputMediaSettings 


CDlgPDFOutputMediaSettings::CDlgPDFOutputMediaSettings(CWnd* pParent /*=NULL*/, BOOL bRunModal)
	: CDialog(CDlgPDFOutputMediaSettings::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPDFOutputMediaSettings)
	m_strMediaLongName = _T("");
	m_strLocation = _T("");
	m_nWhatSheets = 0;
	m_strWhatLayout = _T("");
	m_strPDFTargetName = _T("");
	//}}AFX_DATA_INIT
	m_bRunModal			= bRunModal;
	m_pWndMsgTarget		= NULL;
	m_bNoSheetSelection = FALSE;
	m_nOutputTarget	= CPDFTargetList::PrintSheetTarget;
	m_strJobNotes = _T("");
	m_bOutputCuttingData = FALSE;
	m_strCuttingDataFolder = _T("");
}

CDlgPDFOutputMediaSettings::~CDlgPDFOutputMediaSettings()
{
	if (  ! m_bRunModal)
		DestroyWindow();
}

BOOL CDlgPDFOutputMediaSettings::DestroyWindow() 
{
	if (m_hWnd)
	{
		CPrintSheetView*	 pView			   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		CNewPrintSheetFrame* pParentFrame	   = (pView) ? (CNewPrintSheetFrame*)pView->GetParentFrame() : NULL;
		if (pParentFrame)
			if (pParentFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			{
				//pParentFrame->m_bShowPrintMenu	   = TRUE;
				//pParentFrame->m_bEnablePlateButton = TRUE;
				//if (pParentFrame->m_bShowExposures)
				//	pParentFrame->m_bEnablePlateTableButton = TRUE;
			}

		if (pView)
			pView->m_DisplayList.Invalidate();

		UpdatePrintSheetTableView();
	}

	m_pdfTargetList.RemoveAll();

	return CDialog::DestroyWindow();
}

void CDlgPDFOutputMediaSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPDFOutputMediaSettings)
	DDX_Control(pDX, IDC_SHEETTYPES_COMBO, m_sheetTypesCombo);
	DDX_Control(pDX, IDC_EXPOSER_COMBO, m_pdfTargetCombo);
	DDX_Text(pDX, IDC_MEDIA_NAME, m_strMediaLongName);
	DDX_Text(pDX, IDC_PDF_OUPUT_FOLDER, m_strLocation);
	DDX_Radio(pDX, IDC_SHEETS_ALL, m_nWhatSheets);
	DDX_Text(pDX, IDC_OUTPUT_JOBNOTES_EDIT, m_strJobNotes);
	DDX_Text(pDX, IDC_CUTTINGDATA_FOLDER_STATIC, m_strCuttingDataFolder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPDFOutputMediaSettings, CDialog)
	//{{AFX_MSG_MAP(CDlgPDFOutputMediaSettings)
	ON_CBN_SELCHANGE(IDC_EXPOSER_COMBO, OnSelchangeExposerCombo)
	ON_BN_CLICKED(IDC_PORTRAIT, OnPortrait)
	ON_BN_CLICKED(IDC_LANDSCAPE, OnLandscape)
	ON_BN_CLICKED(IDC_PRINTER_PROPS, OnPrinterProps)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_COPY, OnCopy)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_CBN_SETFOCUS(IDC_SHEETTYPES_COMBO, OnSetfocusSheettypesCombo)
	ON_BN_CLICKED(IDC_SHEETS_ALL, OnSheetsAll)
	ON_BN_CLICKED(IDC_SHEETS_ALLOFTYPE, OnSheetsAllOfType)
	ON_BN_CLICKED(IDC_SHEETS_SELECTED, OnSheetsSelected)
	ON_CBN_SELCHANGE(IDC_SHEETTYPES_COMBO, OnSelchangeSheettypesCombo)
	ON_BN_CLICKED(IDC_PICK_OUTPUT_LOCATION, OnPickOutputLocation)
	ON_UPDATE_COMMAND_UI(IDC_COPY, OnUpdateCopyButton)
	ON_UPDATE_COMMAND_UI(IDC_DEL, OnUpdateDelButton)
	ON_UPDATE_COMMAND_UI(IDC_PRINTER_PROPS, OnUpdatePrinterProps)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_NEW_PDFTARGET_FOLDER, &CDlgPDFOutputMediaSettings::OnNewPDFTargetFolder)
	ON_COMMAND(ID_NEW_PDFTARGET_PRINTER, &CDlgPDFOutputMediaSettings::OnNewPDFTargetPrinter)
	ON_COMMAND(ID_NEW_JDFTARGET_FOLDER, &CDlgPDFOutputMediaSettings::OnNewJDFTargetFolder)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPDFOutputMediaSettings 
BEGIN_EVENTSINK_MAP(CDlgPDFOutputMediaSettings, CDialog)
    //{{AFX_EVENTSINK_MAP(CPrintSheetTableView)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()


BOOL CDlgPDFOutputMediaSettings::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CPrintSheetView*	  pPrintSheetView		= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CNewPrintSheetFrame*  pParentFrame			= (pPrintSheetView) ? (CNewPrintSheetFrame*)pPrintSheetView->GetParentFrame() : NULL;
	CPrintSheetTableView* pPrintSheetTableView	= (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));

	if (pPrintSheetView)
		pPrintSheetView->m_DisplayList.Invalidate();

	if (m_bNoSheetSelection || (m_nOutputTarget == CPDFTargetList::FoldSheetTarget))
	{
		CRect winRect, divRect, okRect, propsRect;
		GetWindowRect(winRect);
		GetDlgItem(IDC_DIVIDER)->GetWindowRect(divRect);
		winRect.bottom = divRect.top;
		GetDlgItem(IDOK)->GetWindowRect(okRect);					ScreenToClient(okRect);
		GetDlgItem(IDC_PRINTER_PROPS)->GetWindowRect(propsRect);	ScreenToClient(propsRect);
		okRect = propsRect; okRect.OffsetRect(propsRect.Width() + 20, 0); okRect.right = okRect.left + 60;
		GetDlgItem(IDOK)->MoveWindow(okRect);
		MoveWindow(winRect);
	}

	CString strDlgTitle;
	if (pPrintSheetTableView)
	{
		if (pPrintSheetTableView->m_bOutputProfileChecked)
			strDlgTitle.LoadString(IDS_ASSIGN_OUTPUTPROFILE);
		else
			strDlgTitle.LoadString(IDS_START_OUTPUT);

		SetWindowText(strDlgTitle);
	}
	else
	{
		CString strOutputTarget; 
		switch (m_nOutputTarget)
		{
		case CPDFTargetList::PrintSheetTarget:	strOutputTarget.LoadString(IDS_PRINTSHEET_TITLE);	break;
		case CPDFTargetList::FoldSheetTarget:	strOutputTarget.LoadString(IDS_FOLDSHEET_LABEL);	break;
		case CPDFTargetList::BookletTarget:		strOutputTarget = _T("Booklet");					break;
		}
		GetWindowText(strDlgTitle);
		SetWindowText(strDlgTitle + _T(" - ") + strOutputTarget);
	}


	// First of all remove unused colors from color definition table.
	// In future we should put this function to other locations, i.e. after removing color seps. etc.
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		pDoc->m_ColorDefinitionTable.UpdateColorInfos();

	CImageList imageList;
	imageList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 3, 0);	// List not to grow
	imageList.Add(theApp.LoadIcon(IDI_PDF));
	imageList.Add(theApp.LoadIcon(IDI_PRINTER));
	imageList.Add(theApp.LoadIcon(IDI_JDF));
	m_pdfTargetCombo.SetImageList(&imageList);
	imageList.Detach();

	m_pdfTargetList.Load(m_nOutputTarget);
	if (m_pdfTargetList.GetSize() == 0)
		GetDlgItem(IDC_PRINTER_PROPS)->EnableWindow(FALSE);

	GetDlgItem(IDC_PDF_OUPUT_FOLDER)->ModifyStyle(0, SS_PATHELLIPSIS);

	if (pPrintSheetTableView)
	{
		m_nWhatSheets = 0;
		if (pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem())
			m_nWhatSheets = 2;
	}

	m_strJobNotes = (pDoc) ? pDoc->m_GlobalData.m_strNotes : _T("");

	m_sheetTypesCombo.ResetContent();
	switch (m_nOutputTarget)
	{
	case CPDFTargetList::PrintSheetTarget:
		{
			if (pDoc)
			{
				if (pDoc->m_PrintSheetList.m_Layouts.GetCount() > 1)
				{
					POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
					while (pos)
					{
						CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
						m_sheetTypesCombo.AddString(rLayout.m_strLayoutName);
					}
					if ( ! m_strWhatLayout.IsEmpty())
						m_sheetTypesCombo.SelectString(-1, m_strWhatLayout);
					else
						m_sheetTypesCombo.SetCurSel(0);
				}
				else
				{
					GetDlgItem(IDC_SHEETS_ALLOFTYPE)->EnableWindow(FALSE);
					GetDlgItem(IDC_SHEETTYPES_COMBO)->EnableWindow(FALSE);
				}
			}
			else
			{
				GetDlgItem(IDC_SHEETS_ALLOFTYPE)->EnableWindow(FALSE);
				GetDlgItem(IDC_SHEETTYPES_COMBO)->EnableWindow(FALSE);
			}
		}
		break;

	case CPDFTargetList::BookletTarget:
		{
			if (pDoc)
			{
				CArray <CFoldSheet*, CFoldSheet*> foldSheetTypes;
				CArray <int, int>				  foldSheetTypeNums;
				pDoc->m_Bookblock.GetFoldSheetTypes(foldSheetTypes, foldSheetTypeNums, -1);
				if (foldSheetTypes.GetSize() > 1)
				{
					for (int i = 0; i < foldSheetTypes.GetSize(); i++)
					{
						m_sheetTypesCombo.AddString(foldSheetTypes[i]->m_strFoldSheetTypeName);
					}
					if ( ! m_strWhatLayout.IsEmpty())
						m_sheetTypesCombo.SelectString(-1, m_strWhatLayout);
					else
						m_sheetTypesCombo.SetCurSel(0);
				}
				else
				{
					GetDlgItem(IDC_SHEETS_ALLOFTYPE)->EnableWindow(FALSE);
					GetDlgItem(IDC_SHEETTYPES_COMBO)->EnableWindow(FALSE);
				}
			}
			else
			{
				GetDlgItem(IDC_SHEETS_ALLOFTYPE)->EnableWindow(FALSE);
				GetDlgItem(IDC_SHEETTYPES_COMBO)->EnableWindow(FALSE);
			}
		}
		break;

	case CPDFTargetList::FoldSheetTarget:	break;
	}


	UpdateData(FALSE);

	InitExposerComboBox();

	switch (m_nWhatSheets)
	{
	case 0:		OnSheetsAll();								break;
	case 1:		OnSheetsAllOfType();						break;
	case 2:		OnSheetsSelected();							break;
	default:	SelectPDFTargetCombo(m_strPDFTargetName);	break;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgPDFOutputMediaSettings::OnNew() 
{
    CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_NEW_TARGET));
	CMenu* pPopup = menu.GetSubMenu(0);
	if ( ! pPopup)
		return;

    MENUITEMINFO mi;
    mi.cbSize = sizeof(MENUITEMINFO);
    mi.fMask = MIIM_TYPE | MIIM_DATA;
    mi.fType = MFT_OWNERDRAW;	//     Making the menu item ownerdrawn:
	mi.dwItemData = (ULONG_PTR)menu.m_hMenu;
	pPopup->SetMenuItemInfo(ID_NEW_PDFTARGET_FOLDER,	&mi);
	pPopup->SetMenuItemInfo(ID_NEW_PDFTARGET_PRINTER,	&mi);
	pPopup->SetMenuItemInfo(ID_NEW_JDFTARGET_FOLDER,	&mi);

	CRect rcClient;
	GetDlgItem(IDC_NEW)->GetWindowRect(rcClient);

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rcClient.TopLeft().x, rcClient.TopLeft().y, this);

	((CButton*)GetDlgItem(IDC_NEW))->SetCheck(0);
}

void CDlgPDFOutputMediaSettings::OnCopy() 
{
	int	nSel = m_pdfTargetCombo.GetCurSel();
	if ((nSel >= 0) && (nSel < m_pdfTargetList.GetSize()))
	{
		CPDFTargetProperties pdfTargetProps;
		pdfTargetProps = m_pdfTargetList[nSel];
		pdfTargetProps.m_strTargetName = m_strPDFTargetName = BuildNewPDFTargetName(pdfTargetProps.m_nType, m_strPDFTargetName);
		m_pdfTargetList.InsertAt(nSel + 1, pdfTargetProps);

		COMBOBOXEXITEM item;
		item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;
		item.iItem			= nSel + 1;
		item.pszText		= m_strPDFTargetName.GetBuffer(MAX_TEXT);
		item.iImage			= (pdfTargetProps.m_nType == CPDFTargetProperties::TypePDFFolder) ? 0 : (pdfTargetProps.m_nType == CPDFTargetProperties::TypePDFPrinter) ? 1 : 2;
		item.iSelectedImage = (pdfTargetProps.m_nType == CPDFTargetProperties::TypePDFFolder) ? 0 : (pdfTargetProps.m_nType == CPDFTargetProperties::TypePDFPrinter) ? 1 : 2;
		m_pdfTargetCombo.InsertItem(&item);

		m_pdfTargetCombo.SetCurSel(m_pdfTargetCombo.FindStringExact(-1, m_strPDFTargetName));

		UpdateData(FALSE);
		UpdatePrintSheetView();
	}
}

void CDlgPDFOutputMediaSettings::AddNewTarget(int nTargetType)
{
	if (nTargetType == -1)
		return;

	CSize plateSize(0,0);
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CLayout*	 pLayout		   = pDoc->GetActLayout();
		CLayoutSide* pLayoutSide	   = (pLayout) ? &pLayout->m_FrontSide : NULL;
		//int			 nPressDeviceIndex = (pLayoutSide) ? pLayoutSide->m_nPressDeviceIndex : pDoc->m_nDefaultPressDeviceIndex;
		//POSITION pos = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(nPressDeviceIndex);
		//if (pos)
		if (pLayoutSide)
		{
			CPressDevice* pPressDevice = pLayoutSide->GetPressDevice();//pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
			if (pPressDevice)
				plateSize = CSize((int)(pPressDevice->m_fPlateWidth  * 10.0f), (int)(pPressDevice->m_fPlateHeight * 10.0f) );
		}
	}

	CString strMeasureFormat, strPlateSize;
	strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
	strPlateSize.Format((LPCTSTR)strMeasureFormat, MeasureFormat((float)plateSize.cx / 10.0f), MeasureFormat((float)plateSize.cy / 10.0f));

	CPDFTargetProperties pdfTargetProps(nTargetType, BuildNewPDFTargetName(nTargetType), "",
										CMediaSize(strPlateSize, (float)plateSize.cx / 10.0f, (float)plateSize.cy / 10.0f, CMediaSize::Portrait, 
										0.0f, 0.0f, FALSE));

	switch (m_nOutputTarget)
	{
	case CPDFTargetList::PrintSheetTarget:	break;
	case CPDFTargetList::FoldSheetTarget:	pdfTargetProps.m_mediaList[0].m_nAutoAlignTo = CMediaSize::AlignNone;	
											pdfTargetProps.m_strOutputFilename = (pdfTargetProps.m_nType == CPDFTargetProperties::TypeJDFFolder) ? _T("KIM_$7.jdf") : _T("KIM_$7.pdf");
											break;
	case CPDFTargetList::BookletTarget:		pdfTargetProps.m_mediaList[0].m_nAutoAlignTo = CMediaSize::AlignToBookblockDoublePage;
											pdfTargetProps.m_strOutputFilename = (pdfTargetProps.m_nType == CPDFTargetProperties::TypeJDFFolder) ? _T("KIM_$7.jdf") : _T("KIM_$7.pdf");
											break;
	}

	m_dlgPDFTargetProps.m_nOutputTarget	 = m_nOutputTarget;
	m_dlgPDFTargetProps.m_pdfTargetProps = pdfTargetProps;
	m_dlgPDFTargetProps.m_defaultFormats.RemoveAll();
	m_dlgPDFTargetProps.m_defaultFormats.Append(m_pdfTargetList.m_defaultFormats);
	m_dlgPDFTargetProps.m_pCallWnd		 = this;
	m_dlgPDFTargetProps.m_pfnCallback	 = NewPDFTargetPropsCallback;
	m_dlgPDFTargetProps.m_pWndMsgTarget	 = m_pWndMsgTarget;
	m_dlgPDFTargetProps.Create(IDD_PDFTARGET_PROPS, this);

	ShowWindow(SW_HIDE);
}

void CDlgPDFOutputMediaSettings::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType == ODT_MENU)
	{
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		CRect rcItem = lpDrawItemStruct->rcItem;

		pDC->FillSolidRect(rcItem, RGB(252, 252, 249));//CREME);

		CRect rcIcon = rcItem;
		rcIcon.right = rcIcon.left + 45;

		if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			CGraphicComponent gc;
			gc.DrawTransparentFrame(pDC, rcItem, RGB(120,160,220), 2);
		}				

		switch (lpDrawItemStruct->itemID)
		{
		case ID_NEW_PDFTARGET_FOLDER:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 5, theApp.LoadIcon(IDI_PDF_LARGE));		break;
		case ID_NEW_PDFTARGET_PRINTER:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 5, theApp.LoadIcon(IDI_PRINTER_LARGE));	break;
		case ID_NEW_JDFTARGET_FOLDER:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 5, theApp.LoadIcon(IDI_JDF_LARGE));		break;
		}

		pDC->SelectObject(GetFont());
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(DARKBLUE);
		CRect rcText = rcItem;
		rcText.left = rcIcon.right + 5;
		if(IsMenu((HMENU)lpDrawItemStruct->hwndItem))
		{
			CMenu* pMenu = CMenu::FromHandle((HMENU)lpDrawItemStruct->hwndItem);
			if (pMenu)
			{
				CString strText;
				pMenu->GetMenuString(lpDrawItemStruct->itemID, strText, MF_BYCOMMAND);
				pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
			}
		}
	}
	else
		CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CDlgPDFOutputMediaSettings::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct->CtlType == ODT_MENU)
	{
		int	nMaxLen = 0;
		if(IsMenu((HMENU)lpMeasureItemStruct->itemData))
		{
			CMenu* pMenu = CMenu::FromHandle((HMENU)lpMeasureItemStruct->itemData);
			if (pMenu)
			{
				CString strText;
				pMenu->GetMenuString(lpMeasureItemStruct->itemID, strText, MF_BYCOMMAND);
				CClientDC dc(this);
				dc.SelectObject(GetFont());
				nMaxLen = dc.GetTextExtent(strText).cx + 50;
			}
		}
		lpMeasureItemStruct->itemWidth  = nMaxLen;
		lpMeasureItemStruct->itemHeight = 45;
	}
	else
		CDialog::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CDlgPDFOutputMediaSettings::OnNewPDFTargetFolder() { AddNewTarget(CPDFTargetProperties::TypePDFFolder);  }
void CDlgPDFOutputMediaSettings::OnNewPDFTargetPrinter(){ AddNewTarget(CPDFTargetProperties::TypePDFPrinter); }
void CDlgPDFOutputMediaSettings::OnNewJDFTargetFolder() { AddNewTarget(CPDFTargetProperties::TypeJDFFolder);  }

void CDlgPDFOutputMediaSettings::NewPDFTargetPropsCallback(CDlgPDFTargetProps* pDlg, CWnd* pCallWnd, int nIDReturn)
{
	CDlgPDFOutputMediaSettings* pThis = (CDlgPDFOutputMediaSettings*)pCallWnd;

	pThis->ShowWindow(SW_SHOW);

	if (nIDReturn == IDCANCEL)
	{
		pThis->UpdatePrintSheetView();
		return;
	}

	CPDFTargetProperties pdfTargetProps;
	pdfTargetProps = pDlg->m_pdfTargetProps;

	// check if target name is already in list. If so, rename
	CString strPDFTargetName;
	strPDFTargetName = pdfTargetProps.m_strTargetName;
	for (int i = 2; i < 100; i++)
	{
		if (pThis->m_pdfTargetCombo.FindStringExact(-1, strPDFTargetName) != CB_ERR)	// name exists
			strPDFTargetName.Format(_T("%s(%d)"), pdfTargetProps.m_strTargetName, i);
		else
			break;
	}
	pdfTargetProps.m_strTargetName = strPDFTargetName;

	pThis->m_pdfTargetList.m_defaultFormats.RemoveAll();
	pThis->m_pdfTargetList.m_defaultFormats.Append(pDlg->m_defaultFormats);

	pThis->m_strPDFTargetName		= pdfTargetProps.m_strTargetName;
	pThis->m_strLocation			= pdfTargetProps.m_strLocation;
	pThis->m_strMediaName			= pdfTargetProps.m_mediaList[0].m_strMediaName;
	pThis->m_fMediaWidth			= pdfTargetProps.m_mediaList[0].GetMediaWidth(pThis->GetFirstEffectedPrintSheet());
	pThis->m_fMediaHeight			= pdfTargetProps.m_mediaList[0].GetMediaHeight(pThis->GetFirstEffectedPrintSheet());
	pThis->m_bOutputCuttingData		= pdfTargetProps.m_bOutputCuttingData;
	pThis->m_strCuttingDataFolder	= pdfTargetProps.m_strCuttingDataFolder;

	pThis->GetDlgItem(IDC_CUTTINGDATA_STATIC)->ShowWindow		((pThis->m_bOutputCuttingData) ? SW_SHOW : SW_HIDE);
	pThis->GetDlgItem(IDC_CUTTINGDATA_FOLDER_STATIC)->ShowWindow((pThis->m_bOutputCuttingData) ? SW_SHOW : SW_HIDE);

	// Sets the MediaFormatString dependent on unit
	CString strMeasureFormat;
	strMeasureFormat.Format(_T("%s (%s x %s)"), pThis->m_strMediaName, (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
	pThis->m_strMediaLongName.Format((LPCTSTR)strMeasureFormat, MeasureFormat(pThis->m_fMediaWidth), MeasureFormat(pThis->m_fMediaHeight));

	pThis->m_pdfTargetList.Add(pdfTargetProps);

	COMBOBOXEXITEM item;
	item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;
	item.iItem			= pThis->m_pdfTargetCombo.GetCount();
	item.pszText		= pThis->m_strPDFTargetName.GetBuffer(MAX_TEXT);
	item.iImage			= (pdfTargetProps.m_nType == CPDFTargetProperties::TypePDFFolder) ? 0 : (pdfTargetProps.m_nType == CPDFTargetProperties::TypePDFPrinter) ? 1 : 2;
	item.iSelectedImage = (pdfTargetProps.m_nType == CPDFTargetProperties::TypePDFFolder) ? 0 : (pdfTargetProps.m_nType == CPDFTargetProperties::TypePDFPrinter) ? 1 : 2;
	pThis->m_pdfTargetCombo.InsertItem(&item);

	pThis->m_pdfTargetCombo.SetCurSel(pThis->m_pdfTargetCombo.FindStringExact(-1, pThis->m_strPDFTargetName));

	pThis->UpdateData(FALSE);
	pThis->UpdatePrintSheetView();
}

 
void CDlgPDFOutputMediaSettings::OnPrinterProps() 
{
	UpdateData(TRUE);

	int nSel = m_pdfTargetCombo.GetCurSel();
	if (nSel == CB_ERR)
		return;

	m_dlgPDFTargetProps.m_nOutputTarget	 = m_nOutputTarget;
	m_dlgPDFTargetProps.m_pdfTargetProps = m_pdfTargetList[nSel];
	m_dlgPDFTargetProps.m_defaultFormats.RemoveAll();
	m_dlgPDFTargetProps.m_defaultFormats.Append(m_pdfTargetList.m_defaultFormats);
	m_dlgPDFTargetProps.m_pCallWnd		 = this;
	m_dlgPDFTargetProps.m_pfnCallback	 = ModifyPDFTargetPropsCallback;
	m_dlgPDFTargetProps.m_pWndMsgTarget	 = m_pWndMsgTarget;
	m_dlgPDFTargetProps.Create(IDD_PDFTARGET_PROPS, this);

	ShowWindow(SW_HIDE);
}


void CDlgPDFOutputMediaSettings::ModifyPDFTargetPropsCallback(CDlgPDFTargetProps* pDlg, CWnd* pCallWnd, int nIDReturn)
{
	CDlgPDFOutputMediaSettings* pThis = (CDlgPDFOutputMediaSettings*)pCallWnd;

	pThis->ShowWindow(SW_SHOW);

	if (nIDReturn == IDCANCEL)
	{
		pThis->UpdatePrintSheetView();
		return;
	}

	CPDFTargetProperties pdfTargetProps;
	pdfTargetProps = pDlg->m_pdfTargetProps;

	int nSel = pThis->m_pdfTargetCombo.GetCurSel();
	if (nSel == CB_ERR)
		return;

	pThis->m_pdfTargetList[nSel] = pdfTargetProps;
	pThis->m_pdfTargetList.m_defaultFormats.RemoveAll();
	pThis->m_pdfTargetList.m_defaultFormats.Append(pDlg->m_defaultFormats);

	pThis->m_strPDFTargetName		= pdfTargetProps.m_strTargetName;
	pThis->m_strLocation			= pdfTargetProps.m_strLocation;
	pThis->m_strMediaName			= pdfTargetProps.m_mediaList[0].m_strMediaName;
	pThis->m_fMediaWidth			= pdfTargetProps.m_mediaList[0].GetMediaWidth(pThis->GetFirstEffectedPrintSheet());
	pThis->m_fMediaHeight			= pdfTargetProps.m_mediaList[0].GetMediaHeight(pThis->GetFirstEffectedPrintSheet());
	pThis->m_bOutputCuttingData		= pdfTargetProps.m_bOutputCuttingData;
	pThis->m_strCuttingDataFolder	= pdfTargetProps.m_strCuttingDataFolder;

	pThis->GetDlgItem(IDC_CUTTINGDATA_STATIC)->ShowWindow		((pThis->m_bOutputCuttingData) ? SW_SHOW : SW_HIDE);
	pThis->GetDlgItem(IDC_CUTTINGDATA_FOLDER_STATIC)->ShowWindow((pThis->m_bOutputCuttingData) ? SW_SHOW : SW_HIDE);

	// Sets the MediaFormatString dependent on unit
	CString strMeasureFormat;
	if (pdfTargetProps.m_mediaList[0].m_nAutoAlignTo == CMediaSize::AlignNone)
		strMeasureFormat.Format(_T("%s (%s x %s)"), pThis->m_strMediaName, (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
	else
		strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
	pThis->m_strMediaLongName.Format((LPCTSTR)strMeasureFormat, MeasureFormat(pThis->m_fMediaWidth), MeasureFormat(pThis->m_fMediaHeight));
//	m_strMediaLongName.Format(_T("%s (%.1f x %.1f)"), m_strMediaName, m_fMediaWidth, m_fMediaHeight);

	pThis->UpdateData(FALSE);
	pThis->UpdatePrintSheetView();
}


CString CDlgPDFOutputMediaSettings::BuildNewPDFTargetName(int nTargetType, const CString strName)
{
	CString strPDFTargetName, strReturn;
	if (strName.IsEmpty())
	{
		switch (nTargetType)
		{
		case CPDFTargetProperties::TypePDFFolder:	strPDFTargetName.LoadString(IDS_NEW_PDFTARGET_FOLDER);	break;
		case CPDFTargetProperties::TypePDFPrinter:	strPDFTargetName.LoadString(IDS_NEW_PDFTARGET_PRINTER);	break;
		case CPDFTargetProperties::TypeJDFFolder:	strPDFTargetName.LoadString(IDS_NEW_JDFTARGET_FOLDER);	break;
		}
	}
	else
		strPDFTargetName = strName;

	strReturn = strPDFTargetName;
	int i;
	for (i = 2; i < 100; i++)
	{
		if (m_pdfTargetCombo.FindStringExact(-1, strReturn) != CB_ERR)	// name exists
			strReturn.Format(_T("%s(%d)"), strPDFTargetName, i);
		else
			break;
	}
	if (i >= 100)	// max. 100 tries
		return CString("");
	else
		return strReturn;
}


void CDlgPDFOutputMediaSettings::OnDel() 
{
	if (m_pdfTargetList.GetSize() == 0) 
		return;

	if (AfxMessageBox(IDS_DEL_LISTENTRY, MB_YESNO) == IDYES)	//TODO: 0=No help available
	{
		int	nSel = m_pdfTargetCombo.GetCurSel();
		if ((nSel >= 0) && (nSel < m_pdfTargetList.GetSize()))
			m_pdfTargetList.RemoveAt(nSel);
		else
			return;

		m_pdfTargetCombo.DeleteString(nSel);
		if (m_pdfTargetList.GetSize() > 0) 
		{
			if (nSel >= m_pdfTargetList.GetSize())
				nSel--;
			m_pdfTargetCombo.SetCurSel(nSel);
			OnSelchangeExposerCombo();	
		}
		else
		{
			m_strPDFTargetName = _T("");
			m_strLocation	 = _T("");
			UpdateData(FALSE);      
			GetDlgItem(IDC_PRINTER_PROPS)->EnableWindow(FALSE);
		}

		UpdateData(TRUE);
	}
}
 
void CDlgPDFOutputMediaSettings::InitExposerComboBox()
{
	m_pdfTargetCombo.ResetContent();

	if ( ! m_pdfTargetList.GetSize())
		return;

	for (int i = 0; i < m_pdfTargetList.GetSize(); i++)
	{
		COMBOBOXEXITEM item;
		item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;
		item.iItem			= i;
		item.pszText		=  m_pdfTargetList[i].m_strTargetName.GetBuffer(MAX_TEXT);
		item.iImage			= (m_pdfTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 0 : (m_pdfTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 1 : 2;
		item.iSelectedImage = (m_pdfTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 0 : (m_pdfTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 1 : 2;
		m_pdfTargetCombo.InsertItem(&item);
	}

	UpdateData(FALSE);
}

void CDlgPDFOutputMediaSettings::SelectPDFTargetCombo(CString strPDFTargetName)
{
	m_strPDFTargetName   =  strPDFTargetName;
	m_strLocation	     =  "";
	m_strMediaLongName   =  "";
	m_fMediaWidth	     =  0.0f;
	m_fMediaHeight	     =  0.0f;

	CPrintSheet* pPrintSheet = NULL;
	switch (m_nOutputTarget)
	{
	case CPDFTargetList::PrintSheetTarget:
		{
			pPrintSheet = GetFirstEffectedPrintSheet();
			if (strPDFTargetName.IsEmpty())
			{
				CString strSelectedTargets;
				if (DifferentTargetsSelected(strSelectedTargets))
				{
					m_strLocation.LoadString(IDS_MULTIPLE_PROFILES_SELECTED);
					m_strMediaLongName =  strSelectedTargets;
					GetDlgItem(IDC_STATIC_LOC)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_STATIC_MEDIA_NAME)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_PICK_OUTPUT_LOCATION)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_CUTTINGDATA_STATIC)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_CUTTINGDATA_FOLDER_STATIC)->ShowWindow(SW_HIDE);
				}
				else
				{
					m_strPDFTargetName = (pPrintSheet) ? pPrintSheet->m_strPDFTarget : "";
					GetDlgItem(IDC_STATIC_LOC)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_STATIC_MEDIA_NAME)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_PICK_OUTPUT_LOCATION)->ShowWindow(SW_SHOW);
					GetDlgItem(IDC_CUTTINGDATA_STATIC)->ShowWindow(SW_HIDE);
					GetDlgItem(IDC_CUTTINGDATA_FOLDER_STATIC)->ShowWindow(SW_HIDE);
				}
			}
		}
		break;

	case CPDFTargetList::FoldSheetTarget:
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			if (pDoc)
				m_strPDFTargetName = pDoc->m_Bookblock.m_FoldSheetList.m_outputTargetProps.m_strTargetName;
		}
		break;

	case CPDFTargetList::BookletTarget:
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			if (pDoc)
				m_strPDFTargetName = pDoc->m_Bookblock.m_outputTargetProps.m_strTargetName;
		}
		break;
	}

	int nIndex = m_pdfTargetList.FindIndex(m_strPDFTargetName);
	if (nIndex >= 0)
	{
		m_strPDFTargetName		= m_pdfTargetList[nIndex].m_strTargetName;
		m_strLocation			= m_pdfTargetList[nIndex].m_strLocation;
		m_fMediaWidth			= m_pdfTargetList[nIndex].m_mediaList[0].GetMediaWidth(pPrintSheet);
		m_fMediaHeight			= m_pdfTargetList[nIndex].m_mediaList[0].GetMediaHeight(pPrintSheet);
		m_bOutputCuttingData	= m_pdfTargetList[nIndex].m_bOutputCuttingData;
		m_strCuttingDataFolder	= m_pdfTargetList[nIndex].m_strCuttingDataFolder;
		CString strMeasureFormat;
		if (m_pdfTargetList[nIndex].m_mediaList[0].m_nAutoAlignTo == CMediaSize::AlignNone)
			strMeasureFormat.Format(_T("%s (%s x %s)"), m_strMediaName, (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
		else
			strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
		m_strMediaLongName.Format((LPCTSTR)strMeasureFormat, MeasureFormat(m_fMediaWidth), MeasureFormat(m_fMediaHeight));
		
		GetDlgItem(IDC_CUTTINGDATA_STATIC)->ShowWindow((m_bOutputCuttingData) ? SW_SHOW : SW_HIDE);
		GetDlgItem(IDC_CUTTINGDATA_FOLDER_STATIC)->ShowWindow((m_bOutputCuttingData) ? SW_SHOW : SW_HIDE);
	}

	m_pdfTargetCombo.SetCurSel(nIndex);

	if (m_pdfTargetList.GetSize())
		if ( (nIndex >= 0) && (nIndex < m_pdfTargetList.GetSize()) )
			GetDlgItem(IDC_PICK_OUTPUT_LOCATION)->ShowWindow((m_pdfTargetList[nIndex].m_nType == CPDFTargetProperties::TypePDFPrinter) ? SW_HIDE : SW_SHOW);

	UpdateData(FALSE);
}

BOOL CDlgPDFOutputMediaSettings::DifferentTargetsSelected(CString& strSelectedTargets)
{
	//if (m_pWndMsgTarget)
	//	if (m_pWndMsgTarget->IsKindOf(RUNTIME_CLASS(CDlgProdDataPrepress)))
	//		return FALSE;

	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if ( ! pPrintSheetTableView)
		return FALSE;

	CDisplayItem* pDI = pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem();
	if ( ! pDI)
		return FALSE;

	CString	strFirstPDFTarget = ((CPrintSheet*)pDI->m_pData)->m_strPDFTarget;
	pDI = pPrintSheetTableView->m_DisplayList.GetNextSelectedItem(pDI);
	while (pDI)
	{
		if ( ((CPrintSheet*)pDI->m_pData)->m_strPDFTarget != strFirstPDFTarget)
		{
			strSelectedTargets = strFirstPDFTarget + _T(", ") + ((CPrintSheet*)pDI->m_pData)->m_strPDFTarget + _T(" ...");
			return TRUE;
		}
		pDI = pPrintSheetTableView->m_DisplayList.GetNextSelectedItem(pDI);
	}

	return FALSE;
}

BOOL CDlgPDFOutputMediaSettings::PrintSheetIsSelected(CPrintSheet* pPrintSheet, int nSide)
{
	//if (m_pWndMsgTarget)
	//	if (m_pWndMsgTarget->IsKindOf(RUNTIME_CLASS(CDlgProdDataPrepress)))
	//		return TRUE;

	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if ( ! pPrintSheetTableView)
		return FALSE;

	CDisplayItem* pDI = pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem();
	while (pDI)
	{
		if ( (CPrintSheet*)pDI->m_pData == pPrintSheet)
			if (pDI->m_pAuxData == NULL)
				return TRUE;
			else
				if ( ((int)pDI->m_pAuxData == nSide) || (nSide == 0) )
					return TRUE;

		pDI = pPrintSheetTableView->m_DisplayList.GetNextSelectedItem(pDI);
	}
	return FALSE;
}

void CDlgPDFOutputMediaSettings::OnPortrait() 
{
	UpdateData(TRUE);
	UpdatePrintSheetView();
}

void CDlgPDFOutputMediaSettings::OnLandscape() 
{
	UpdateData(TRUE);
	UpdatePrintSheetView();
}
 
void CDlgPDFOutputMediaSettings::OnSelchangeExposerCombo() 
{
	int	nSel = m_pdfTargetCombo.GetCurSel();
	m_pdfTargetCombo.GetLBText(nSel, m_strPDFTargetName);

	if (nSel != CB_ERR)
	{
		m_strPDFTargetName		= m_pdfTargetList[nSel].m_strTargetName;
		m_strLocation			= m_pdfTargetList[nSel].m_strLocation;
		m_strMediaName			= m_pdfTargetList[nSel].m_mediaList[0].m_strMediaName;
		m_fMediaWidth			= m_pdfTargetList[nSel].m_mediaList[0].GetMediaWidth(GetFirstEffectedPrintSheet());
		m_fMediaHeight			= m_pdfTargetList[nSel].m_mediaList[0].GetMediaHeight(GetFirstEffectedPrintSheet());
		m_bOutputCuttingData	= m_pdfTargetList[nSel].m_bOutputCuttingData;
		m_strCuttingDataFolder	= m_pdfTargetList[nSel].m_strCuttingDataFolder;
		CString strMeasureFormat;
		if (m_pdfTargetList[nSel].m_mediaList[0].m_nAutoAlignTo == CMediaSize::AlignNone)
			strMeasureFormat.Format(_T("%s (%s x %s)"), m_strMediaName, (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
		else
			strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
		m_strMediaLongName.Format((LPCTSTR)strMeasureFormat, MeasureFormat(m_fMediaWidth), MeasureFormat(m_fMediaHeight));
	//	m_strMediaLongName.Format(_T("%s (%.1f x %.1f)"), m_strMediaName, m_fMediaWidth, m_fMediaHeight);

		if (m_pdfTargetList.GetSize())
			if ( (nSel >= 0) && (nSel < m_pdfTargetList.GetSize()) )
			{
				GetDlgItem(IDC_STATIC_LOC)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_STATIC_MEDIA_NAME)->ShowWindow(SW_SHOW);
				GetDlgItem(IDC_PICK_OUTPUT_LOCATION)->ShowWindow((m_pdfTargetList[nSel].m_nType == CPDFTargetProperties::TypePDFPrinter) ? SW_HIDE : SW_SHOW);
				GetDlgItem(IDC_CUTTINGDATA_STATIC)->ShowWindow((m_bOutputCuttingData) ? SW_SHOW : SW_HIDE);
				GetDlgItem(IDC_CUTTINGDATA_FOLDER_STATIC)->ShowWindow((m_bOutputCuttingData) ? SW_SHOW : SW_HIDE);
			}

		UpdateData(FALSE);
	}

	UpdatePrintSheetView();
	UpdatePrintSheetTableView();
}

void CDlgPDFOutputMediaSettings::UpdatePrintSheetView()
{
	if (m_hWnd)
		UpdateData(TRUE);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		pView->m_DisplayList.Invalidate();
		pView->OnUpdate(NULL, 0, NULL); 
	}

	//if (m_pWndMsgTarget)
	//	m_pWndMsgTarget->PostMessage(WM_COMMAND, ID_UPDATE_OUTPUTMEDIA);
}

void CDlgPDFOutputMediaSettings::UpdatePrintSheetTableView()
{
	CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pView)
	{
		pView->m_DisplayList.Invalidate();
		pView->Invalidate();
		pView->UpdateWindow(); 
		pView->m_PrintSheetTable.Initialize(CPrintSheetTable::ShowOutputList);
	}
}

CDlgPDFTargetProps* CDlgPDFOutputMediaSettings::GetDlgPDFTargetProps()
{
	if (this == NULL)
		return NULL;

	if (m_dlgPDFTargetProps.m_hWnd)
		return &m_dlgPDFTargetProps;
	else
		return NULL;
}

BOOL CDlgPDFOutputMediaSettings::PrintSheetIsEffected(CPrintSheet* pPrintSheet)
{
	if ( ! pPrintSheet)
		return FALSE;

	switch (m_nWhatSheets)
	{
	case 0:			// all sheets
		break;	
	case 1:			// all sheets of type 
		{
			CString strLayout;
			if (m_sheetTypesCombo.GetCurSel() >= 0)
				m_sheetTypesCombo.GetLBText(m_sheetTypesCombo.GetCurSel(), strLayout);
			if (pPrintSheet->GetLayout()->m_strLayoutName != strLayout)
				return FALSE;
		}
		break;
	case 2:			// selected sheets
		{
			CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
			if ( ! pPrintSheetTableView)
				return FALSE;

			BOOL bFound = FALSE;
			CDisplayItem* pDI = pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem();
			while (pDI)
			{
				if (pDI->m_pData == (void*)pPrintSheet)
				{
					bFound = TRUE;
					break;
				}
				pDI = pPrintSheetTableView->m_DisplayList.GetNextSelectedItem(pDI);
			}
			if ( ! bFound)
				return FALSE;
		}
		break;
	}

	return TRUE;
}

CPrintSheet* CDlgPDFOutputMediaSettings::GetFirstEffectedPrintSheet()
{
	//if (m_pWndMsgTarget)
	//	if (m_pWndMsgTarget->IsKindOf(RUNTIME_CLASS(CDlgProdDataPrepress)))
	//		return ((CDlgProdDataPrepress*)m_pWndMsgTarget)->m_pPrintSheet;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	POSITION	pos	 = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet&  rPrintSheet  = pDoc->m_PrintSheetList.GetNext(pos);
		if (PrintSheetIsEffected(&rPrintSheet))
			return &rPrintSheet;
	}

	return NULL;
}

CMediaSize* CDlgPDFOutputMediaSettings::GetCurMedia(CPrintSheet* pPrintSheet)
{
	if ( ! pPrintSheet)
		return NULL;

	UpdateData(TRUE);

	if (m_nWhatSheets == 2)
	{
		if ( ! PrintSheetIsSelected(pPrintSheet)) 
			return NULL;
	}
	else
		if (m_nWhatSheets == 1)
		{
			CString strLayout;
			m_sheetTypesCombo.GetLBText(m_sheetTypesCombo.GetCurSel(), strLayout);
			if (pPrintSheet->GetLayout())
				if (pPrintSheet->GetLayout()->m_strLayoutName != strLayout)
					return NULL;
		}

/*
	if (m_bNoTarget)
		return NULL;
*/
	if (m_dlgPDFTargetProps.m_hWnd)
		return &m_dlgPDFTargetProps.m_pdfTargetProps.m_mediaList[0];

	if (m_pdfTargetCombo.GetCurSel() == CB_ERR)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(pPrintSheet->m_strPDFTarget);
		if ( ! pTargetProps)
			return NULL;
		else
			return (pTargetProps->m_mediaList.GetSize()) ? &pTargetProps->m_mediaList[0] : NULL;
	}
	else
	{
		int nSel = m_pdfTargetCombo.GetCurSel();
		if (nSel == CB_ERR)
			return NULL;
		return &m_pdfTargetList[nSel].m_mediaList[0];
	}
}

CString CDlgPDFOutputMediaSettings::GetCurPDFTargetName(CPrintSheet* pPrintSheet)
{
	if (m_hWnd)
		UpdateData(TRUE);

	if ( ! PrintSheetIsSelected(pPrintSheet))
		return "";

/*
	if (m_bNoTarget)
		return NULL;
*/
	if (m_dlgPDFTargetProps.m_hWnd)
		return m_dlgPDFTargetProps.m_pdfTargetProps.m_strTargetName;

	if (m_pdfTargetCombo.GetCurSel() == CB_ERR)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(pPrintSheet->m_strPDFTarget);
		if ( ! pTargetProps)
			return "";
		else
			return pTargetProps->m_strTargetName;
	}
	else
	{
		int nSel = m_pdfTargetCombo.GetCurSel();
		if (nSel == CB_ERR)
			return "";
		return m_pdfTargetList[nSel].m_strTargetName;
	}
}

float CDlgPDFOutputMediaSettings::GetCurMediaWidth()
{
	if (this == NULL)
		return 0.0f;

	if (m_dlgPDFTargetProps.m_hWnd)
	{
		m_dlgPDFTargetProps.UpdateData(TRUE);
		return m_dlgPDFTargetProps.m_fMediaWidth;
	}
	else
	{
		int nSel = m_pdfTargetCombo.GetCurSel();
		if (nSel == CB_ERR)
			return 0.0f;
		return m_pdfTargetList[nSel].m_mediaList[0].GetMediaWidth(GetFirstEffectedPrintSheet());
	}
}

float CDlgPDFOutputMediaSettings::GetCurMediaHeight()
{
	if (this == NULL)
		return 0.0f;

	if (m_dlgPDFTargetProps.m_hWnd)
	{
		m_dlgPDFTargetProps.UpdateData(TRUE);
		return m_dlgPDFTargetProps.m_fMediaHeight;
	}
	else
	{
		int nSel = m_pdfTargetCombo.GetCurSel();
		if (nSel == CB_ERR)
			return 0.0f;
		return m_pdfTargetList[nSel].m_mediaList[0].GetMediaHeight(NULL);
	}
}

int CDlgPDFOutputMediaSettings::GetCurTileIndex()
{
	if (m_dlgPDFTargetProps.m_hWnd)
		return m_dlgPDFTargetProps.m_nCurTileIndex;
	else
		return -1;
}

void CDlgPDFOutputMediaSettings::GetTileBBox(CPrintSheet* pPrintSheet, int nSide, int nTileIndex, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop)
{
	if (this == NULL)
		return;

	CMediaPosInfo* pPosInfo;
	float		   fMediaWidth, fMediaHeight;
	if (m_dlgPDFTargetProps.m_hWnd)
	{
		pPosInfo	 = &m_dlgPDFTargetProps.m_pdfTargetProps.m_mediaList[0].m_posInfos[nTileIndex];
		fMediaWidth  =  m_dlgPDFTargetProps.m_pdfTargetProps.m_mediaList[0].GetMediaWidth(pPrintSheet);
		fMediaHeight =  m_dlgPDFTargetProps.m_pdfTargetProps.m_mediaList[0].GetMediaHeight(pPrintSheet);
	}
	else
	{
		int nSel = m_pdfTargetCombo.GetCurSel();
		if (nSel == CB_ERR)
			return;
		pPosInfo	 = &m_pdfTargetList[nSel].m_mediaList[0].m_posInfos[nTileIndex];
		fMediaWidth  =  m_pdfTargetList[nSel].m_mediaList[0].GetMediaWidth(pPrintSheet);
		fMediaHeight =  m_pdfTargetList[nSel].m_mediaList[0].GetMediaHeight(pPrintSheet);
	}

	pPosInfo->m_reflinePosParams.GetSheetPosition(pPrintSheet, nSide, fBBoxLeft, fBBoxBottom, fMediaWidth, fMediaHeight);
	fBBoxRight = fBBoxLeft   + fMediaWidth;
	fBBoxTop   = fBBoxBottom + fMediaHeight;
}

// tile index not given -> use current tile if props dialog present or 1st tile if not
void CDlgPDFOutputMediaSettings::GetTileBBox(CPrintSheet* pPrintSheet, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop)
{
	if (this == NULL)
		return;

	if (m_dlgPDFTargetProps.m_hWnd)
		GetTileBBox(pPrintSheet, nSide, m_dlgPDFTargetProps.m_nCurTileIndex, fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop);
	else
		GetTileBBox(pPrintSheet, nSide, 0, fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop);
}

void CDlgPDFOutputMediaSettings::GetAllTilesBBox(CPrintSheet* pPrintSheet, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop)
{
	if (this == NULL)
		return;

	CMediaSize* pMedia = GetCurMedia(pPrintSheet);
	if ( ! pMedia)
		return;

	float fTileBBoxLeft, fTileBBoxBottom, fTileBBoxRight, fTileBBoxTop;
	fBBoxLeft = FLT_MAX; fBBoxBottom = FLT_MAX; fBBoxRight = -FLT_MAX; fBBoxTop = -FLT_MAX;
	for (int i = 0; i < pMedia->m_posInfos.GetSize(); i++)
	{
//		float fMediaWidth  = (pMedia->m_nOrientation == CMediaSize::Portrait) ? pMedia->m_fMediaWidth  : pMedia->m_fMediaHeight;
//		float fMediaHeight = (pMedia->m_nOrientation == CMediaSize::Portrait) ? pMedia->m_fMediaHeight : pMedia->m_fMediaWidth;
		float fMediaWidth  = pMedia->GetMediaWidth(pPrintSheet);
		float fMediaHeight = pMedia->GetMediaHeight(pPrintSheet);
		pMedia->m_posInfos[i].m_reflinePosParams.GetSheetPosition(pPrintSheet, nSide, fTileBBoxLeft, fTileBBoxBottom, fMediaWidth, fMediaHeight);
		fTileBBoxRight = fTileBBoxLeft   + fMediaWidth;
		fTileBBoxTop   = fTileBBoxBottom + fMediaHeight;

		fBBoxLeft	= min(fBBoxLeft,	fTileBBoxLeft);	
		fBBoxBottom = min(fBBoxBottom,	fTileBBoxBottom);
		fBBoxRight	= max(fBBoxRight,   fTileBBoxRight);
		fBBoxTop	= max(fBBoxTop,		fTileBBoxTop);
	}
}


void CDlgPDFOutputMediaSettings::OnCancel()
{	// Make sure not to leave the dialog uncontrolled
	CPrintSheetTableView*  pPrintSheetTableView		= (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pPrintSheetTableView)
	{
		pPrintSheetTableView->m_bStartOutputChecked   = FALSE;
		pPrintSheetTableView->m_bOutputProfileChecked = FALSE;
		pPrintSheetTableView->m_outputListToolBarCtrl.CheckButton(ID_START_OUTPUT,	 FALSE);
		pPrintSheetTableView->m_outputListToolBarCtrl.CheckButton(ID_OUTPUT_PROFILE, FALSE);
	}

	if ( ! m_bRunModal)
	{
		if (theApp.m_pMainWnd)
			if (theApp.m_pMainWnd->m_hWnd)
				theApp.m_pMainWnd->SetActiveWindow();
		DestroyWindow();
	}

	UpdatePrintSheetView();

	if (pPrintSheetTableView)
	{
		pPrintSheetTableView->SetFocus();
		pPrintSheetTableView->m_DisplayList.DeselectAll();
		pPrintSheetTableView->Invalidate();
		pPrintSheetTableView->UpdateWindow();
	}

	if (m_bRunModal)
		CDialog::OnCancel();
}


void CDlgPDFOutputMediaSettings::OnOK() 
{
	UpdateData(TRUE);

	int	nSel = m_pdfTargetCombo.GetCurSel();
	if ( (theApp.m_nDongleStatus == CImpManApp::SmallFormatDongle) || (theApp.m_nDongleStatus == CImpManApp::NewsDongle) || (theApp.m_nDongleStatus == CImpManApp::DigitalDongle) )
	{
		if ( (nSel >= 0) && (nSel < m_pdfTargetList.GetSize()) )
		{
			BOOL  bFormatExceeded = FALSE;
			float fMediaWidth	  = m_pdfTargetList[nSel].m_mediaList[0].GetMediaWidth(GetFirstEffectedPrintSheet());
			float fMediaHeight	  = m_pdfTargetList[nSel].m_mediaList[0].GetMediaHeight(GetFirstEffectedPrintSheet());
			if ( OutputFormatExceeded(fMediaWidth, fMediaHeight) )
				bFormatExceeded = TRUE;
			else
				if (m_pdfTargetList[nSel].m_mediaList[0].m_posInfos.GetSize() > 1)
				{
					float fXMin = FLT_MAX, fXMax = -FLT_MAX;
					float fYMin = FLT_MAX, fYMax = -FLT_MAX;
					CPrintSheet* pPrintSheet = GetFirstEffectedPrintSheet();
					for (int nTileIndex = 0; (nTileIndex < m_pdfTargetList[nSel].m_mediaList[0].m_posInfos.GetSize()); nTileIndex++)
					{
						float fXPos, fYPos;
						m_pdfTargetList[nSel].m_mediaList[0].m_posInfos[nTileIndex].m_reflinePosParams.GetSheetPosition(pPrintSheet, FRONTSIDE, fXPos, fYPos, fMediaWidth, fMediaHeight);
						fXMin = min(fXMin, fXPos);
						fYMin = min(fYMin, fYPos);
						fXMax = max(fXMax, fXPos + fMediaWidth);
						fYMax = max(fYMax, fYPos + fMediaHeight);
					}

					fMediaWidth  = fXMax - fXMin;
					fMediaHeight = fYMax - fYMin;
					if ( OutputFormatExceeded(fMediaWidth, fMediaHeight) )
						bFormatExceeded = TRUE;
				}

			if (bFormatExceeded)
			{
				if (theApp.m_nDongleStatus == CImpManApp::SmallFormatDongle)
					AfxMessageBox(IDS_SMALLFORMAT_EDITION_HINT);
				else
					if (theApp.m_nDongleStatus == CImpManApp::NewsDongle)
						AfxMessageBox(IDS_NEWS_EDITION_HINT);
					else
						AfxMessageBox(IDS_DIGITAL_EDITION_HINT);
				return;
			}
		}
	}

	if (m_bRunModal)
		m_pdfTargetList.Save(m_nOutputTarget);
	else
	{
		switch (m_nOutputTarget)
		{
		case CPDFTargetList::PrintSheetTarget:	DoOutputSheets();		break;
		case CPDFTargetList::BookletTarget:		DoOutputBookblock();	break;
		default:														break;
		}
	}

	if (m_bRunModal)
		CDialog::OnOK();
}

void CDlgPDFOutputMediaSettings::DoOutputBookblock()
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int	nSel = m_pdfTargetCombo.GetCurSel();
	if ( (nSel < 0) || (nSel >= m_pdfTargetList.GetSize()) )
		return;

	ShowWindow(SW_HIDE);

	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
		pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos).m_bOutputDisabled = TRUE;

	CBookBlockDataView* pView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
	if (pView)
	{
		pos = pView->m_foldSheetList.GetFirstSelectedItemPosition();
		while (pos)
		{
			int nItem = pView->m_foldSheetList.GetNextSelectedItem(pos);
			if (nItem < 0)
				continue;
			CFoldSheet* pFoldSheet = (CFoldSheet*)pView->m_foldSheetList.GetItemData(nItem);
			if (pFoldSheet)
				pFoldSheet->m_bOutputDisabled = FALSE;
		}
	}

	pDoc->m_Bookblock.m_outputTargetProps = m_pdfTargetList[nSel];
	pDoc->m_Bookblock.OutputProof(pDoc->GetTitle());

	pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
		pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos).m_bOutputDisabled = TRUE;

	pDoc->SetModifiedFlag();

	m_pdfTargetList.Save(CPDFTargetList::BookletTarget);

	if ( ! m_bRunModal)
		DestroyWindow();
}

void CDlgPDFOutputMediaSettings::DoOutputSheets()
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	pDoc->m_GlobalData.m_strNotes = m_strJobNotes;

	CPrintSheetTableView* pPrintSheetTableView	= (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));

	if (pPrintSheetTableView)
	{
		pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_START_OUTPUT,		FALSE);
		pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_OUTPUT_PROFILE,	FALSE);
		pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_OPEN_OUTPUT_FILE, FALSE);
		pPrintSheetTableView->m_outputListToolBarCtrl.Invalidate();
		pPrintSheetTableView->m_outputListToolBarCtrl.UpdateWindow();
		pPrintSheetTableView->m_DisplayList.Invalidate();
	}

	pDoc->m_PrintSheetList.m_pdfTargets.RemoveAll();
	for (int i = 0; i < m_pdfTargetList.GetSize(); i++)
		pDoc->m_PrintSheetList.m_pdfTargets.Add(m_pdfTargetList.ElementAt(i));

	if (  ! m_bRunModal)
	{
		ShowWindow(SW_HIDE);
		AfxGetMainWnd()->UpdateWindow();	// otherwise window is not closed immediately
	}

	//CPrintSheetView*	  pPrintSheetView	= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));	// removed on 07/23/2014 due to problems with multiple profiles assigned to sheets
	//CNewPrintSheetFrame*  pFrame			= (pPrintSheetView) ? (CNewPrintSheetFrame*)pPrintSheetView->GetParentFrame() : NULL;
	//if (pFrame)	
	//{
	//	((CNewPrintSheetFrame*)pFrame)->AssignOutputProfile(m_strPDFTargetName);
	//	pDoc->SetModifiedFlag();
	//}

	if (pPrintSheetTableView && ! m_bRunModal)
	{
		if (m_pdfTargetCombo.GetCurSel() != CB_ERR)
		{
			POSITION	pos		  = pDoc->m_PrintSheetList.GetHeadPosition();
			BOOL		bModified = FALSE;
			while (pos)
			{
				CPrintSheet&  rPrintSheet  = pDoc->m_PrintSheetList.GetNext(pos);
				if (PrintSheetIsSelected(&rPrintSheet))
				{
					rPrintSheet.m_strPDFTarget = m_strPDFTargetName;
					bModified = TRUE;
				}
			}

			if (bModified)
			{
				pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
				UpdatePrintSheetTableView();
			}
		}

		if ( ! pPrintSheetTableView->m_bOutputProfileChecked)
		{
			pDoc->m_PrintSheetList.EnableOutput(FALSE);

			CDisplayItem* pDI = pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem();
			while (pDI)
			{
				if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheet)) ||
					pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetFile)) ) 
					((CPrintSheet*)pDI->m_pData)->EnableOutput();
				else
					if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetSide)) ||
						pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetSideFile)) )
						((CPrintSheet*)pDI->m_pData)->EnableOutput(TRUE, (int)pDI->m_pAuxData);
					else
						if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetPlate)) ||
							pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetPlateFile)) )
							((CPlate*)pDI->m_pAuxData)->m_bOutputDisabled = FALSE;

				pDI = pPrintSheetTableView->m_DisplayList.GetNextSelectedItem(pDI);
			}

			//CPrintSheetView* pView	= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
			//if (pView)
			//{
			//	pView->m_DisplayList.DeselectAll(FALSE);	
			//	pView->Invalidate();
			//	pView->UpdateWindow();
			//}
			if (pDoc->m_PrintSheetList.OutputPDFCheckPageSourceLinks(pDoc->GetTitle()))
			{
				if (pDoc->m_PrintSheetList.OutputImposition(pDoc->GetTitle()) != 2)
					pDoc->m_PrintSheetList.OutputCuttingData(pDoc->GetTitle());
			}

			pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
		}
	}
	//else
	//{
	//	if ( ! pFrame)	// if we are in production data frame, don't output - just assign profiles
	//		if (pDoc->m_PrintSheetList.OutputPDFCheckPageSourceLinks(pDoc->GetTitle()))
	//		{
	//			if (pDoc->m_PrintSheetList.OutputImposition(pDoc->GetTitle()) != 2)
	//				pDoc->m_PrintSheetList.OutputCuttingData(pDoc->GetTitle());
	//		}

	//	pDoc->SetModifiedFlag();
	//}

	m_pdfTargetList.Save(CPDFTargetList::PrintSheetTarget);

	if (pPrintSheetTableView)
	{
		if ( (theApp.m_nDongleStatus == CImpManApp::ClientDongle) && (theApp.m_nGUIStyle == CImpManApp::GUIStandard) )
			pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_START_OUTPUT,  FALSE);
		else
			pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_START_OUTPUT,	 TRUE);
		pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_OUTPUT_PROFILE,	TRUE);
		pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_OPEN_OUTPUT_FILE, TRUE);
		pPrintSheetTableView->m_bStartOutputChecked	  = FALSE;
		pPrintSheetTableView->m_bOutputProfileChecked = FALSE;
		pPrintSheetTableView->m_outputListToolBarCtrl.CheckButton(ID_START_OUTPUT,		FALSE);
		pPrintSheetTableView->m_outputListToolBarCtrl.CheckButton(ID_OUTPUT_PROFILE,	FALSE);
		pPrintSheetTableView->m_strPDFOutputMessage = "";
		pPrintSheetTableView->UpdateData(FALSE);
	}

	if (  ! m_bRunModal)
		DestroyWindow();

	UpdatePrintSheetView();

	if (pPrintSheetTableView)
	{
		pPrintSheetTableView->SetFocus();
		pPrintSheetTableView->m_DisplayList.DeselectAll();
		pPrintSheetTableView->Invalidate();
		pPrintSheetTableView->UpdateWindow();
	}
}

BOOL CDlgPDFOutputMediaSettings::OutputFormatExceeded(float fWidth, float fHeight)
{
	if (theApp.m_nDongleStatus == CImpManApp::SmallFormatDongle)
	{
		if (fWidth > fHeight)	// landscape
		{
			if ( (fWidth > CDlgPDFTargetProps::_SFW()) || (fHeight > CDlgPDFTargetProps::_SFH()) )
				return TRUE;
		}
		else	// portrait
		{
			if ( (fWidth > CDlgPDFTargetProps::_SFH()) || (fHeight > CDlgPDFTargetProps::_SFW()) )
				return TRUE;
		}
		return FALSE;
	}

	if (theApp.m_nDongleStatus == CImpManApp::NewsDongle)
	{
		if (fWidth > fHeight)	// landscape
		{
			if ( (fWidth > CDlgPDFTargetProps::_NEWSW()) || (fHeight > CDlgPDFTargetProps::_NEWSH()) )
				return TRUE;
		}
		else	// portrait
		{
			if ( (fWidth > CDlgPDFTargetProps::_NEWSH()) || (fHeight > CDlgPDFTargetProps::_NEWSW()) )
				return TRUE;
		}
		return FALSE;
	}

	if (theApp.m_nDongleStatus == CImpManApp::DigitalDongle)
	{
		if (fWidth > fHeight)	// landscape
		{
			if ( (fWidth > CDlgPDFTargetProps::_DIGITALW()) || (fHeight > CDlgPDFTargetProps::_DIGITALH()) )
				return TRUE;
		}
		else	// portrait
		{
			if ( (fWidth > CDlgPDFTargetProps::_DIGITALH()) || (fHeight > CDlgPDFTargetProps::_DIGITALW()) )
				return TRUE;
		}
		return FALSE;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////


void CDlgPDFOutputMediaSettings::OnSetfocusSheettypesCombo() 
{
	m_nWhatSheets = 1;
	UpdateData(FALSE);
}

void CDlgPDFOutputMediaSettings::OnSelchangeSheettypesCombo() 
{
	OnSheetsAllOfType();
}

void CDlgPDFOutputMediaSettings::OnSheetsAll() 
{
	UpdateData(TRUE);
	switch (m_nOutputTarget)
	{
	case CPDFTargetList::PrintSheetTarget:
		{
			CPrintSheetTableView* pPrintSheetTableView	= (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
			if (pPrintSheetTableView)
			{
				pPrintSheetTableView->m_DisplayList.SelectAll(FALSE, DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheet));	// don't notify
				SelectPDFTargetCombo();
				UpdatePrintSheetView();
				UpdatePrintSheetTableView();
			}
			else
				SelectPDFTargetCombo();
		}
		break;

	case CPDFTargetList::BookletTarget:
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			CBookBlockDataView* pView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
			if (pView)
			{
				for (int i = 0; i < pView->m_foldSheetList.GetItemCount(); i++)
				{
					CFoldSheet* pFoldSheet = (CFoldSheet*)pView->m_foldSheetList.GetItemData(i);
					if ( ! pFoldSheet)
						continue;
					pFoldSheet->m_bOutputDisabled = FALSE;
					pView->m_foldSheetList.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
				}
			}
			SelectPDFTargetCombo();
		}
		break;
	}
}

void CDlgPDFOutputMediaSettings::OnSheetsAllOfType() 
{
	UpdateData(TRUE);

	switch (m_nOutputTarget)
	{
	case CPDFTargetList::PrintSheetTarget:
		{
			CPrintSheetTableView* pPrintSheetTableView	= (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
			if (pPrintSheetTableView)
			{
				CString strLayout;
				m_sheetTypesCombo.GetLBText(m_sheetTypesCombo.GetCurSel(), strLayout);

				pPrintSheetTableView->m_DisplayList.DeselectAll(FALSE);	// don't notify
				for (int i = 0; i < pPrintSheetTableView->m_DisplayList.m_List.GetSize(); i++)
				{
					CDisplayItem& rDI = pPrintSheetTableView->m_DisplayList.m_List[i];
					if (rDI.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheet))
					{
						CPrintSheet* pPrintSheet = (CPrintSheet*)rDI.m_pData;
						if (pPrintSheet->GetLayout()->m_strLayoutName == strLayout)
							rDI.m_nState = CDisplayItem::Selected;
					}
				}
			}
			SelectPDFTargetCombo();
			UpdatePrintSheetView();
			UpdatePrintSheetTableView();
		}
		break;

	case CPDFTargetList::BookletTarget:
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			CBookBlockDataView* pView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
			if (pView)
			{
				CString strFoldSheetType;
				m_sheetTypesCombo.GetLBText(m_sheetTypesCombo.GetCurSel(), strFoldSheetType);
				for (int i = 0; i < pView->m_foldSheetList.GetItemCount(); i++)
				{
					CFoldSheet* pFoldSheet = (CFoldSheet*)pView->m_foldSheetList.GetItemData(i);
					if ( ! pFoldSheet)
						continue;
					if (pFoldSheet->m_strFoldSheetTypeName == strFoldSheetType)
					{
						pFoldSheet->m_bOutputDisabled = FALSE;
						pView->m_foldSheetList.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
					}
					else
					{
						pFoldSheet->m_bOutputDisabled = TRUE;
						pView->m_foldSheetList.SetItemState(i, ~LVIS_SELECTED, LVIS_SELECTED);
					}
				}
			}
		}
		break;
	}
}

void CDlgPDFOutputMediaSettings::OnSheetsSelected() 
{
	UpdateData(TRUE);
	SelectPDFTargetCombo();
	if (CPDFTargetList::PrintSheetTarget)
	{
		UpdatePrintSheetView();
		UpdatePrintSheetTableView();
	}
}

void CDlgPDFOutputMediaSettings::OnPickOutputLocation() 
{
	int nSel = m_pdfTargetCombo.GetCurSel();
	if ( (nSel < 0) || (nSel >= m_pdfTargetList.GetSize()) )
		return;

	CDlgDirPicker dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, _T(" |*.___||"), this);
	CString strTitle;
	strTitle.LoadString(IDS_DIRECTORY_STRING);
	dlg.m_ofn.lpstrTitle	  = strTitle;
	dlg.m_ofn.lpstrInitialDir = (m_strLocation.IsEmpty()) ? (LPCSTR)theApp.settings.m_szWorkDir : m_strLocation;

	if (dlg.DoModal() == IDCANCEL)
		return;

	CPDFTargetProperties& rTargetProps = m_pdfTargetList[nSel];
	rTargetProps.m_strLocation = m_strLocation = dlg.m_strPath;

	UpdateData(FALSE);
}

void CDlgPDFOutputMediaSettings::OnUpdateCopyButton(CCmdUI* pCmdUI)
{
	int nSel = m_pdfTargetCombo.GetCurSel();
	pCmdUI->Enable( ((nSel >= 0) && (nSel < m_pdfTargetList.GetSize())) ? TRUE : FALSE);
}

void CDlgPDFOutputMediaSettings::OnUpdateDelButton(CCmdUI* pCmdUI)
{
	int nSel = m_pdfTargetCombo.GetCurSel();
	pCmdUI->Enable( ((nSel >= 0) && (nSel < m_pdfTargetList.GetSize())) ? TRUE : FALSE);
}

void CDlgPDFOutputMediaSettings::OnUpdatePrinterProps(CCmdUI* pCmdUI)
{
	int nSel = m_pdfTargetCombo.GetCurSel();
	pCmdUI->Enable( ((nSel >= 0) && (nSel < m_pdfTargetList.GetSize())) ? TRUE : FALSE);
}

BOOL CDlgPDFOutputMediaSettings::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls(this, TRUE);

	return CDialog::PreTranslateMessage(pMsg);
}
