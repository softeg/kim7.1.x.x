#pragma once

#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"



// CPBDCustomPaintFrame

class CPBDCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CPBDCustomPaintFrame)

public:
	CPBDCustomPaintFrame();
	virtual ~CPBDCustomPaintFrame();

public:
	void DrawPages(CDC* pDC, CRect frameRect);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};


// CDlgProductionBoundDetails-Dialogfeld

class CDlgProductionBoundDetails : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionBoundDetails)

public:
	CDlgProductionBoundDetails(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionBoundDetails();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTIONBOUNDDETAILS };

public:
	CPBDCustomPaintFrame	m_pagesPreviewFrame;
	int						m_nProductPartIndex;
	CDlgMarkSet*			m_pDlgMarkSet;
	BOOL					m_bShinglingActive;
	float					m_fSpineArea;

public:
	void UpdateMaskingPreview();
	void UpdateCutScaleContent();

public:
	CString m_strPressMarkset;
	int		m_nProductionType;
	CString	m_strProductPartComment;
	float	m_fBleed;
	float	m_fGlueLine;
	CSpinButtonCtrl	m_bleedSpin;
	CSpinButtonCtrl	m_glueLineSpin;
	BOOL m_bAdjustContentToMask;



protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedFpBrowseMarkset();
	afx_msg void OnBnClickedPdShinglingCheck();
	afx_msg void OnBnClickedPdShinglingButton();
	afx_msg void OnBnClickedOk();
	afx_msg void OnDeltaposBleedSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusBleedEdit();
	afx_msg void OnDeltaposGluelineSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusGluelineEdit();
	afx_msg void OnBnClickedCutContent();
	afx_msg void OnBnClickedScaleContent();
};
