// DlgObjectPreviewWindow.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetMarksView.h"
#include "PrintSheetTableView.h"
#include "NewPrintSheetFrame.h"
#include "DlgMarkSet.h"
#include "ProdDataPrintView.h"
#include "GraphicComponent.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgObjectPreviewWindow 


CDlgObjectPreviewWindow::CDlgObjectPreviewWindow(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgObjectPreviewWindow::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgObjectPreviewWindow)
	//}}AFX_DATA_INIT
}


void CDlgObjectPreviewWindow::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgObjectPreviewWindow)
	DDX_Control(pDX, IDC_OBJECT_PREVIEWFRAME, m_previewFrame);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgObjectPreviewWindow, CDialog)
	//{{AFX_MSG_MAP(CDlgObjectPreviewWindow)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgObjectPreviewWindow 

BOOL CDlgObjectPreviewWindow::OnInitDialog() 
{
	CDialog::OnInitDialog();

	ModifyStyle(WS_OVERLAPPEDWINDOW | WS_POPUPWINDOW, WS_CHILD);
	ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	m_dlgObjectPreviewControl.Initialize(IDC_OBJECT_PREVIEWFRAME, this);

	RepositionControls();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgObjectPreviewWindow::OnDestroy() 
{
	theApp.SaveWindowPlacement(this, _T("CDlgObjectPreviewWindow"));

	CDialog::OnDestroy();
}

void CDlgObjectPreviewWindow::OnSize(UINT /*nType*/, int /*cx*/, int /*cy*/) 
{
	//CDialog::OnSize(nType, cx, cy);

	if ( ! m_previewFrame.m_hWnd)
		return;
	if ( ! m_dlgObjectPreviewControl.m_hWnd)
		return;

	RepositionControls();
}

void CDlgObjectPreviewWindow::RepositionControls()
{	
	CDC dc;
	dc.CreateCompatibleDC(NULL);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CRect rcClient; GetClientRect(rcClient);

	int nMaxColorNameLength = m_jobColorListFront.GetMaxColorNameLength(&dc);
		nMaxColorNameLength = max(nMaxColorNameLength, m_jobColorListBack.GetMaxColorNameLength(&dc));
	int nColumn				= min(35 + nMaxColorNameLength, rcClient.right - 35);
		nColumn				= max(nColumn, 28);

	CRect		 rcBox, rcBox1, rcBox2;
	CPrintSheet* pPrintSheet = (pView) ? pView->GetActPrintSheet() : NULL;
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if (pLayout)
		if (pLayout->GetCurrentWorkStyle() == WORK_AND_BACK)
		{
			rcBox1 = DrawColorData(&dc, m_jobColorListFront, rcClient, GetLayoutObject(GetMark()), GetMark(), (pView) ? pView->GetActPrintSheet() : NULL, FRONTSIDE, nColumn); rcClient.top = rcBox1.bottom;
			rcBox2 = DrawColorData(&dc, m_jobColorListBack,  rcClient, GetLayoutObject(GetMark()), GetMark(), (pView) ? pView->GetActPrintSheet() : NULL, BACKSIDE,  nColumn);
			rcBox.UnionRect(rcBox1, rcBox2);
		}
		else
			rcBox = DrawColorData(&dc, m_jobColorListFront, rcClient, GetLayoutObject(GetMark()), GetMark(), (pView) ? pView->GetActPrintSheet() : NULL, -1, nColumn);

	rcBox.OffsetRect(0, 40);

	CRect rcFrame = rcClient; rcFrame.top = rcBox.bottom + 10;
	m_previewFrame.MoveWindow(rcFrame);
	m_dlgObjectPreviewControl.Move(IDC_OBJECT_PREVIEWFRAME, this);

	m_dlgObjectPreviewControl.PrepareUpdatePreview();
}

void CDlgObjectPreviewWindow::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CRect rcClient; GetClientRect(rcClient);

	CRect		   rcTitle;
	int			   nTextHeight	 = 20;
	CLayoutObject* pLayoutObject = GetLayoutObject(GetMark());
	CMark*		   pMark		 = GetMark();
	CString		   strName		 = (pLayoutObject) ? pLayoutObject->m_strFormatName : ((pMark) ? pMark->m_strMarkName : _T(""));
	rcTitle.left  = rcClient.left; rcTitle.top = rcClient.top; rcTitle.bottom = rcTitle.top + nTextHeight;  rcTitle.right = rcClient.right;

	CString strOut, strOut1;
	strOut1.LoadString(IDS_COLOR_PREVIEW); strOut.Format(_T(" %s:"), strOut1);
	CGraphicComponent gp;
	gp.DrawTitleBar(&dc, rcTitle, strOut, WHITE, RGB(230,230,230), 90, -1);	

	if ( (m_jobColorListFront.GetSize() <= 0) && (m_jobColorListBack.GetSize() <= 0) )
		return;

	CRect rcText; rcText.left = rcClient.left; rcText.top = rcClient.top + 30; rcText.right = rcClient.right; rcText.bottom = rcText.top + 18;

	rcClient.top = rcText.bottom;

	CRect		 rcBox, rcBox1, rcBox2;
	CPrintSheet* pPrintSheet = (pView) ? pView->GetActPrintSheet() : NULL;
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;

	CFont font;
	font.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = dc.SelectObject(&font);
	int nFrontSideNameLength = dc.GetTextExtent((pLayout) ? pLayout->m_FrontSide.m_strSideName : _T("")).cx;
	int nBackSideNameLength  = dc.GetTextExtent((pLayout) ? pLayout->m_BackSide.m_strSideName  : _T("")).cx;
	dc.SelectObject(pOldFont);
	font.DeleteObject();
	if (pLayout)
		if (pLayout->GetCurrentWorkStyle() != WORK_AND_BACK)
			nFrontSideNameLength = nBackSideNameLength;

	int nMaxColorNameLength = m_jobColorListFront.GetMaxColorNameLength(&dc);
		nMaxColorNameLength = max(nMaxColorNameLength, m_jobColorListBack.GetMaxColorNameLength(&dc));
		nMaxColorNameLength = max(nMaxColorNameLength, nFrontSideNameLength);
		nMaxColorNameLength = max(nMaxColorNameLength, nBackSideNameLength);
	int nColumn				= min(35 + nMaxColorNameLength, rcClient.right - 35);
		nColumn				= max(nColumn, 28);

	if (pLayout)
		if (pLayout->GetCurrentWorkStyle() == WORK_AND_BACK)
		{
			rcBox1 = DrawColorData(&dc, m_jobColorListFront, rcClient, pLayoutObject, pMark, (pView) ? pView->GetActPrintSheet() : NULL, FRONTSIDE, nColumn); rcClient.top = rcBox1.bottom;
			rcBox2 = DrawColorData(&dc, m_jobColorListBack,  rcClient, pLayoutObject, pMark, (pView) ? pView->GetActPrintSheet() : NULL, BACKSIDE,  nColumn);
			rcBox.UnionRect(rcBox1, rcBox2);
		}
		else
			rcBox = DrawColorData(&dc, m_jobColorListFront, rcClient, pLayoutObject, pMark, (pView) ? pView->GetActPrintSheet() : NULL, -1, nColumn);

	BOOL		bNewMark		= (pView) ? ((pView->IsOpenDlgLayoutObjectProperties()) ? (pView->GetDlgLayoutObjectProperties()->m_bModeModify ? FALSE : TRUE) : FALSE) : FALSE;
	BOOL		bUnassignedMark = (pView) ? ((pView->IsOpenMarkSetDlg() || bNewMark) ? TRUE : FALSE) : TRUE;
	HICON		hIcon			= NULL;
	COLORREF	crColor			= (bUnassignedMark) ? RGB(255,235,200) : RGB(255,220,220);
	rcText.right = nColumn;
	strOut.LoadString(IDS_SEPARATION);
	gp.DrawTitleBar(&dc, rcText, strOut, WHITE, RGB(230,230,230), 90, -1);	
	rcText.left = nColumn; rcText.right = rcClient.right;
	gp.DrawTitleBar(&dc, rcText, _T("       ") + strName, WHITE, crColor, 90, -1);	// blanks for icon

	switch (GetMarkType(pLayoutObject, pMark))
	{
	case CMark::CustomMark:		hIcon = theApp.LoadIcon((bUnassignedMark) ? IDI_CUSTOM_MARK_UNASSIGNED	: IDI_CUSTOM_MARK);		break;
	case CMark::TextMark:		hIcon = theApp.LoadIcon((bUnassignedMark) ? IDI_TEXT_MARK_UNASSIGNED	: IDI_TEXT_MARK);		break;
	case CMark::SectionMark:	hIcon = theApp.LoadIcon((bUnassignedMark) ? IDI_SECTION_MARK_UNASSIGNED : IDI_SECTION_MARK);	break;
	case CMark::CropMark:		hIcon = theApp.LoadIcon((bUnassignedMark) ? IDI_CROP_MARK_UNASSIGNED	: IDI_CROP_MARK);		break;
	case CMark::FoldMark:		hIcon = theApp.LoadIcon((bUnassignedMark) ? IDI_FOLD_MARK_UNASSIGNED	: IDI_FOLD_MARK);		break;
	case CMark::BarcodeMark:	hIcon = theApp.LoadIcon((bUnassignedMark) ? IDI_BARCODE_MARK_UNASSIGNED : IDI_BARCODE_MARK);	break;
	case CMark::DrawingMark:	hIcon = theApp.LoadIcon((bUnassignedMark) ? IDI_DRAWING_MARK_UNASSIGNED : IDI_DRAWING_MARK);	break;
	default:					break;
	}
	dc.DrawIcon(rcText.left + 5, rcText.top + 2, hIcon);

	CPen  pen(PS_SOLID, 1, RGB(220,220,220));
	dc.SelectObject(&pen);
	dc.MoveTo(nColumn, rcText.top);
	dc.LineTo(nColumn, rcBox.bottom);
	pen.DeleteObject();
}

CRect CDlgObjectPreviewWindow::DrawColorData(CDC* pDC, CJobColorList& rColorList, CRect rcFrame, CLayoutObject* pLayoutObject, CMark* pMark, CPrintSheet* pPrintSheet, int nSide, int nColumn)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int		 nTextHeight = 20;
	COLORREF crColor	 = RGB(50,50,50);

	CRect rcText;
	rcText.left = rcFrame.left + 10; rcText.top = rcFrame.top + 10; rcText.right = rcFrame.right; rcText.bottom = rcText.top + nTextHeight;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);

	if (nSide != -1)
	{
		CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		if (pLayout)
		{
			pDC->SetTextColor(GUICOLOR_CAPTION);
			CString strOut = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_strSideName : pLayout->m_BackSide.m_strSideName;
			CRect rcText1 = rcText; rcText1.right = nColumn;
			pDC->DrawText(strOut, rcText1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		}
	}

	pDC->SetTextColor(crColor);
	rcBox.UnionRect(rcBox, rcText);

	int nOff			 = 4;
	int nColorRectWidth  = 11;
	int nColorRectHeight = 11;
	for (int nIndex = 0; nIndex < rColorList.GetSize(); nIndex++)
	{
		rcText.OffsetRect(0, nTextHeight);

		int nPosLeft = rcText.left;

		/////////// 1nd column
		// draw color rect
		CRect rect;
		rect.left	= nPosLeft + 5;
		rect.right	= rect.left + nColorRectWidth;
		rect.top	= rcText.top;
		rect.bottom = rect.top + nColorRectHeight;
		rect.OffsetRect(0, 1);
		CBrush colorBrush(rColorList[nIndex].m_crRGB);
		CPen   framePen(PS_SOLID, 1, DARKGRAY);
		CBrush* pOldBrush = pDC->SelectObject(&colorBrush);
		CPen*	pOldPen	  = pDC->SelectObject(&framePen);
		if (rColorList[nIndex].m_strColorName.CompareNoCase(_T("Composite")) == 0)
			pDC->DrawIcon(rect.left - 1, rect.top + 1, theApp.LoadIcon(IDI_CMYK));
		else
			pDC->Rectangle(rect);
		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		colorBrush.DeleteObject();
		framePen.DeleteObject();
		rcBox.UnionRect(rcBox, rect);

		// draw text
		rect.left   = nPosLeft + nOff + nColorRectWidth + nOff;
		rect.right  = nColumn;
		rect.bottom	= rcText.bottom;

		CString strColorName = rColorList[nIndex].m_strColorName;
		if (strColorName.CompareNoCase(_T("Composite")) == 0)
			strColorName = _T("CMYK");
		pDC->DrawText(strColorName, rect, DT_LEFT | DT_VCENTER);
		rcBox.UnionRect(rcBox, rect);
		/////////// end of 2nd column

		if ( ! pMark && ! pLayoutObject)
			continue;

		/////////// 2nd column
		///// draw link symbol
		rect.left  = rect.right + 5;
		rect.right = rcFrame.right;
		if ( ! rColorList[nIndex].m_strRemapped.IsEmpty())
		{
			pDC->DrawIcon(rect.left, rect.top, theApp.LoadIcon(IDI_LINK));
			rect.OffsetRect(20, 0); rect.right = rcFrame.right;
			pDC->DrawText(rColorList[nIndex].m_strRemapped, rect, DT_LEFT | DT_VCENTER);
			rcBox.UnionRect(rcBox, rect);
		}
		else
			if (rColorList[nIndex].m_bToPrint)
			{
				pDC->DrawIcon(rect.left + 1, rect.top + 1, theApp.LoadIcon(IDI_CHECK));
				if (pMark)
					if (pMark->m_nType == CMark::DrawingMark)
					{
						CPageTemplate* pMarkTemplate = GetObjectTemplate();
						CPageSource*   pMarkSource	 = GetObjectSource();
						if (pMarkTemplate && pMarkSource)
						{
							float fColorIntensity = 1.0f;
							if (pMarkTemplate->m_bMap2AllColors || (pMarkTemplate->m_bMap2SpotColors && ! pDoc->m_ColorDefinitionTable.IsProcessColor(strColorName)) )
							{
								for (int i = 0; i < pMarkSource->m_PageSourceHeaders.GetSize(); i++)
									if (pMarkSource->m_PageSourceHeaders[i].m_strColorName.CompareNoCase(_T("Composite")) == 0)
									{
										fColorIntensity = pMarkSource->m_PageSourceHeaders[i].m_fColorIntensity;
										break;
									}
							}
							else
							{
								for (int i = 0; i < pMarkSource->m_PageSourceHeaders.GetSize(); i++)
								{
									if (pMarkSource->m_PageSourceHeaders[i].m_strColorName == strColorName)
									{
										fColorIntensity = pMarkSource->m_PageSourceHeaders[i].m_fColorIntensity;
										break;
									}
								}
							}
							CRect rcTextIntensity = rect; rcTextIntensity.left = rect.left + 25; rcTextIntensity.right = rcTextIntensity.left + 50;
							CString strIntensity;
							strIntensity.Format(_T("%.0f %%"), fColorIntensity * 100.0f);
							pDC->DrawText(strIntensity, rcTextIntensity, DT_LEFT | DT_VCENTER);
						}
					}
			}
		//////////////////////////////
	}

	font.DeleteObject();

	return rcBox;
}

int CDlgObjectPreviewWindow::GetMarkType(CLayoutObject* pLayoutObject, CMark* pMark)
{
	if (pLayoutObject)
	{
		if (pLayoutObject->m_nType == CLayoutObject::Page)
			return -1;
		else
		{
			CPageTemplate* pTemplate = pLayoutObject->GetCurrentMarkTemplate();
			return (pTemplate) ? pTemplate->GetMarkType() : -1;
		}
	}

	if (pMark)
		return pMark->m_nType;

	return -1;
}

CDlgLayoutObjectControl* CDlgObjectPreviewWindow::GetActiveDlgLayoutObjectControl()
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return FALSE;
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)pView->GetParentFrame();
	if ( ! pFrame)
		return FALSE;

	CWnd* pActiveWnd = GetActiveWindow();
	if (pFrame->m_pDlgLayoutObjectProperties)
		if (pFrame->m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_hWnd)
			return &pFrame->m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl;

	if (pFrame->m_pDlgMarkSet)
		if (pFrame->m_pDlgMarkSet->m_dlgLayoutObjectControl.m_hWnd)
//			if (pFrame->m_pDlgMarkSet == pActiveWnd)
				return &pFrame->m_pDlgMarkSet->m_dlgLayoutObjectControl;

	CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pMarksView)
		if (pMarksView->m_dlgLayoutObjectControl.m_hWnd)
			if (pMarksView == pActiveWnd)
				return &pMarksView->m_dlgLayoutObjectControl;

	return NULL;
}

CLayoutObject* CDlgObjectPreviewWindow::GetLayoutObject(CMark* pMark)
{
	BOOL bNewMark = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CDlgLayoutObjectProperties * pDlg = pView->GetDlgNewMark();
		BOOL bDlgNewMark = (pDlg) ? (( ! pDlg->m_bModeModify) ? TRUE : FALSE) : FALSE;
		if (bDlgNewMark || pView->IsOpenMarkSetDlg())
			bNewMark = TRUE;
	}
	if (bNewMark)
	{
		CDlgLayoutObjectControl* pDlg = GetActiveDlgLayoutObjectControl();
		if (pDlg)
			if (pDlg->m_pLayoutObject)
				return pDlg->m_pLayoutObject; 
	}

	if ( ! pView)
		return NULL;

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	if (pDI)
	{
		CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		while (pDI)
		{
			if ( ((CLayoutObject*)pDI->m_pData)->m_strFormatName != pLayoutObject->m_strFormatName)
				return NULL;
			pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		}
		return pLayoutObject;
	}
	else
	{
		if (pMark)
		{
			CPrintSheet* pPrintSheet = GetPrintSheet();
			CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			if (pLayout)
				return pLayout->FindMarkObject(pMark->m_strMarkName);
		}
	}
	return NULL;
}

CPageTemplate* CDlgObjectPreviewWindow::GetObjectTemplate()
{
	BOOL bNewMark = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CDlgLayoutObjectProperties * pDlg = pView->GetDlgNewMark();
		BOOL bDlgNewMark = (pDlg) ? (( ! pDlg->m_bModeModify) ? TRUE : FALSE) : FALSE;
		if (bDlgNewMark || pView->IsOpenMarkSetDlg())
			bNewMark = TRUE;
	}
	if (bNewMark)
	{
		CDlgLayoutObjectControl* pDlg = GetActiveDlgLayoutObjectControl();
		if (pDlg)
			if (pDlg->m_pObjectTemplate)
				return pDlg->m_pObjectTemplate; 
	}

	CLayoutObject* pObject = GetLayoutObject(GetMark());
	if (pObject)
	{
		if (GetMark())
			return &GetMark()->m_markTemplate;
		else
			return (pObject->m_nType == CLayoutObject::ControlMark) ? pObject->GetCurrentMarkTemplate() : pObject->GetCurrentObjectTemplate(GetPrintSheet());
	}

	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pProdDataPrintView)
		if (pProdDataPrintView->GetCurrentMark())
			return &pProdDataPrintView->GetCurrentMark()->m_markTemplate;

	return NULL;
}

CPageSource* CDlgObjectPreviewWindow::GetObjectSource()
{
	BOOL bNewMark = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CDlgLayoutObjectProperties * pDlg = pView->GetDlgNewMark();
		BOOL bDlgNewMark = (pDlg) ? (( ! pDlg->m_bModeModify) ? TRUE : FALSE) : FALSE;
		if (bDlgNewMark || pView->IsOpenMarkSetDlg())
			bNewMark = TRUE;
	}
	if (bNewMark)
	{
		CDlgLayoutObjectControl* pDlg = GetActiveDlgLayoutObjectControl();
		if (pDlg)
			if (pDlg->m_pObjectSource)
				return pDlg->m_pObjectSource; 
	}

	CLayoutObject* pObject = GetLayoutObject(GetMark());
	if (pObject)
	{
		CPageTemplate*	pTemplate	  = (pObject->m_nType == CLayoutObject::ControlMark) ? pObject->GetCurrentMarkTemplate() : pObject->GetCurrentObjectTemplate(GetPrintSheet());
		CPageSource*	pObjectSource = NULL;
		if (pTemplate)
			pObjectSource = (pTemplate->m_ColorSeparations.GetSize()) ? pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex) : NULL;
		return pObjectSource;
	}

	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pProdDataPrintView)
		if (pProdDataPrintView->GetCurrentMark())
			return &pProdDataPrintView->GetCurrentMark()->m_MarkSource;

	return NULL;
}

CPageSourceHeader* CDlgObjectPreviewWindow::GetObjectSourceHeader()
{
	BOOL bNewMark = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CDlgLayoutObjectProperties * pDlg = pView->GetDlgNewMark();
		BOOL bDlgNewMark = (pDlg) ? (( ! pDlg->m_bModeModify) ? TRUE : FALSE) : FALSE;
		if (bDlgNewMark || pView->IsOpenMarkSetDlg())
			bNewMark = TRUE;
	}
	if (bNewMark)
	{
		CDlgLayoutObjectControl* pDlg = GetActiveDlgLayoutObjectControl();
		if (pDlg)
			if (pDlg->m_pObjectSourceHeader)
				return pDlg->m_pObjectSourceHeader; 
	}

	CLayoutObject* pObject = GetLayoutObject(GetMark());
	if (pObject)
	{
		CPageTemplate*		pTemplate			= (pObject->m_nType == CLayoutObject::ControlMark) ? pObject->GetCurrentMarkTemplate() : pObject->GetCurrentObjectTemplate(GetPrintSheet());
		CPageSourceHeader*	pObjectSourceHeader = (pTemplate) ? ( (pTemplate->m_ColorSeparations.GetSize()) ? pTemplate->GetObjectSourceHeader(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex) : NULL) : NULL;
		return pObjectSourceHeader;
	}

	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pProdDataPrintView)
		if (pProdDataPrintView->GetCurrentMark())
			return &pProdDataPrintView->GetCurrentMark()->m_MarkSource.m_PageSourceHeaders[0];

	return NULL;
}
	
CMark* CDlgObjectPreviewWindow::GetMark()
{
	BOOL bNewMark = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CDlgLayoutObjectProperties * pDlg = pView->GetDlgNewMark();
		BOOL bDlgNewMark = (pDlg) ? (( ! pDlg->m_bModeModify) ? TRUE : FALSE) : FALSE;
		if (bDlgNewMark || pView->IsOpenMarkSetDlg())
			bNewMark = TRUE;
	}
	if (bNewMark)
	{
		CDlgLayoutObjectControl* pDlg = GetActiveDlgLayoutObjectControl();
		if (pDlg)
			if (pDlg->m_pMark)
				return pDlg->m_pMark; 
	}

	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if ( ! pProdDataPrintView)
		return NULL;
	return pProdDataPrintView->GetCurrentMark();
}
	
CPrintSheet* CDlgObjectPreviewWindow::GetPrintSheet()
{
	//CDlgLayoutObjectControl* pDlg = GetActiveDlgLayoutObjectControl();
	//if (pDlg)
	//	if (pDlg->m_pPrintSheet)
	//		return pDlg->m_pPrintSheet; 

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return NULL;
	return pView->GetActPrintSheet();
}

CColorDefinitionTable* CDlgObjectPreviewWindow::GetColorDefTable()
{
	BOOL bNewMark = FALSE;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CDlgLayoutObjectProperties * pDlg = pView->GetDlgNewMark();
		BOOL bDlgNewMark = (pDlg) ? (( ! pDlg->m_bModeModify) ? TRUE : FALSE) : FALSE;
		if (bDlgNewMark || pView->IsOpenMarkSetDlg())
			bNewMark = TRUE;
	}

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	return (bNewMark) ? &theApp.m_colorDefTable : ((pDoc) ? &pDoc->m_ColorDefinitionTable : NULL);
}

void CDlgObjectPreviewWindow::LoadData()
{
	if (m_hWnd)
	{
		CImpManDoc*	pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return;

		CColorDefinitionTable* pColorDefTable = &pDoc->m_ColorDefinitionTable;
		CMark*				   pMark		  = GetMark();
		CPrintSheetView*	   pView		  = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
		{
			CDlgLayoutObjectProperties * pDlg = pView->GetDlgNewMark();
			BOOL bDlgNewMark = (pDlg) ? (( ! pDlg->m_bModeModify) ? TRUE : FALSE) : FALSE;
			if (bDlgNewMark || pView->IsOpenMarkSetDlg())
			{
				pMark = NULL;
				if (pDlg)
					pColorDefTable = pDlg->m_dlgLayoutObjectControl.GetActColorDefTable();
				else
					if (pView->GetDlgMarkSet())
						pColorDefTable = &pView->GetDlgMarkSet()->m_markList.m_colorDefTable;
			}
		}

		pDoc->m_ColorDefinitionTable.UpdateColorInfos();

		m_jobColorListFront.RemoveAll();
		m_jobColorListBack.RemoveAll();
		m_jobColorListFront.LoadData(GetObjectTemplate(), GetLayoutObject(pMark), GetMark(), GetPrintSheet(), FRONTSIDE, pColorDefTable);
		m_jobColorListBack.LoadData(GetObjectTemplate(), GetLayoutObject(pMark), GetMark(), GetPrintSheet(), BACKSIDE,  pColorDefTable);

		Invalidate();

		m_dlgObjectPreviewControl.ReinitDialog();
	}
}

// CJobColorList //////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CJobColorList::AddColorItem(COLORREF crRGB, CString strColorName, BOOL bToPrint, CString strSepStatus, CString strRemapped)
{
	CJobColorListItem clItem(crRGB, strColorName, bToPrint, strSepStatus, strRemapped);
	Add(clItem);
}

void CJobColorList::LoadData(CPageTemplate* pObjectTemplate, CLayoutObject* pLayoutObject, CMark* pMark, CPrintSheet* pPrintSheet, int nSide, CColorDefinitionTable* pColorDefTable)
{
	if ( ! pPrintSheet)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CStringArray sheetSideColors;
	CStringArray sheetSideProcessColors;
	CPlateList& rPlateList = (nSide == FRONTSIDE) ? pPrintSheet->m_FrontSidePlates : pPrintSheet->m_BackSidePlates;
	CPlate* pPlate = rPlateList.GetTopmostPlate();
	if (pPlate)
	{
		CString strMappedColor = theApp.MapPrintColor(pPlate->m_strPlateName);
		sheetSideColors.Add(strMappedColor);
		if (strMappedColor.CompareNoCase(_T("Composite")) == 0)
		{
			for (int i = 0; i < pPlate->m_processColors.GetSize(); i++)
				sheetSideProcessColors.Add(pPlate->m_processColors[i]);
			for (int i = 0; i < pPlate->m_spotColors.GetSize(); i++)
				sheetSideColors.Add(pPlate->m_spotColors[i]);
		}
	}

	for (int i = 0; i < sheetSideColors.GetSize(); i++)
	{
		BOOL	bToPrint		   = FALSE;
		int		nSepStatusTemplate = 0;
		CString strSepStatus	   = _T("");

		CString strMappedColor   = sheetSideColors[i];
		CString strRemappedColor = strMappedColor;
		int nDocColorDefinitionIndex = pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strMappedColor);
		if ( ! pDoc->m_PageTemplateList.ColorInUse(pDoc, (short)nDocColorDefinitionIndex) &&	// if color not in page template list but on plate (default), we have no pages assigned - so we take definition from product part
		     ! pDoc->m_MarkTemplateList.ColorInUse(pDoc, (short)nDocColorDefinitionIndex) )		// if color not in page template list but on plate (default), we have no pages assigned - so we take definition from product part
		{
			if (strMappedColor.CompareNoCase(_T("Composite")) == 0)		
				AddProcessColors(sheetSideProcessColors, pObjectTemplate, pMark, pColorDefTable);
			else
			{
				CColorant* pColorant = NULL;
				if (pObjectTemplate)
				{
					if (pObjectTemplate->IsFlatProduct())
						pColorant = pObjectTemplate->GetFlatProduct().m_colorantList.FindColorant(strMappedColor);
					else
						pColorant = pDoc->m_boundProducts.GetProductPart(pObjectTemplate->m_nProductPartIndex).m_colorantList.FindColorant(strMappedColor);
				}
				else
				{
					//CBoundProductPart& rProductPart = (pPrintSheet) ? pPrintSheet->GetProductPart() : CBoundProductPart();
					//if ( ! rProductPart.IsNull())
					//	pColorant = rProductPart.m_colorantList.FindColorant(strMappedColor);
				}
				if (pColorant)
				{
					if (pLayoutObject && ! pMark)
						nSepStatusTemplate = (pObjectTemplate) ? pObjectTemplate->IsColorPrinted(nDocColorDefinitionIndex, pPrintSheet, pLayoutObject, strRemappedColor) : 0;
					else
						nSepStatusTemplate = (pObjectTemplate) ? pObjectTemplate->IsColorPrinted(nDocColorDefinitionIndex, pPrintSheet, pMark, strRemappedColor, pColorDefTable) : 0;
					AddColorItem(pColorant->m_crRGB,  strMappedColor, (nSepStatusTemplate > 0) ? TRUE : FALSE, strSepStatus, strRemappedColor);
				}
			}
			continue;
		}

		///// todo: this section slows down switching bettween marks rapidly when job is complex 

		int	nSepStatusSheet	= pPrintSheet->ContainsPrintColor(nDocColorDefinitionIndex, BOTHSIDES);
		if (nSepStatusSheet)
		{
			if (pLayoutObject && ! pMark)
				nSepStatusTemplate = (pObjectTemplate) ? pObjectTemplate->IsColorPrinted(nDocColorDefinitionIndex, pPrintSheet, pLayoutObject, strRemappedColor) : 0;
			else
				nSepStatusTemplate = (pObjectTemplate) ? pObjectTemplate->IsColorPrinted(nDocColorDefinitionIndex, pPrintSheet, pMark, strRemappedColor, pColorDefTable) : 0;

			if (nSepStatusTemplate)
			{
				switch (nSepStatusTemplate)
				{
				case CColorDefinition::Separated:	if (nSepStatusSheet == CColorDefinition::Unseparated) 
														nSepStatusTemplate = 0;	
													break;
				case CColorDefinition::Unseparated:	if (nSepStatusSheet == CColorDefinition::Separated) 
														nSepStatusTemplate = 0;	
													break;
				case CColorDefinition::Mixed:		if (nSepStatusSheet != CColorDefinition::Mixed) 
														nSepStatusTemplate = nSepStatusSheet;	
													break;
				}
				if (nSepStatusTemplate)	
				{
					switch (nSepStatusTemplate)
					{
					case CColorDefinition::Separated:	strSepStatus += "s";	break;
					case CColorDefinition::Unseparated:	strSepStatus += "";		break;
					case CColorDefinition::Mixed:		strSepStatus += "s+c";	break;
					}
					bToPrint = TRUE;
				}
			}
		}

		if (strMappedColor.CompareNoCase(_T("Composite")) == 0)
			AddProcessColors(sheetSideProcessColors, pObjectTemplate, pMark, pColorDefTable);
		else
			AddColorItem(pDoc->m_ColorDefinitionTable[nDocColorDefinitionIndex].m_rgb,  pDoc->m_ColorDefinitionTable[nDocColorDefinitionIndex].m_strColorName, bToPrint, strSepStatus, strRemappedColor);
	}
}

void CJobColorList::AddProcessColors(CStringArray& sheetSideProcessColors, CPageTemplate* pObjectTemplate, CMark *pMark, CColorDefinitionTable* pColorDefTable)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	BOOL bCyanToPrint = FALSE, bMagentaToPrint = FALSE, bYellowToPrint = FALSE, bKeyToPrint = FALSE;
	if (pObjectTemplate)
	{
		pColorDefTable = ( ! pColorDefTable) ? &pDoc->m_ColorDefinitionTable : pColorDefTable;
		//BOOL bAllToPrint = (pObjectTemplate->m_bMap2AllColors || ! pObjectTemplate->IsSystemCreated(pMark) ) ? TRUE : FALSE;
		BOOL bAllToPrint = (pObjectTemplate->m_bMap2AllColors) ? TRUE : FALSE;
		bCyanToPrint	 = (pObjectTemplate->HasProcessColor(pMark, CColorDefinition::ProcessC, pColorDefTable) || bAllToPrint) ? TRUE : FALSE;
		bMagentaToPrint  = (pObjectTemplate->HasProcessColor(pMark, CColorDefinition::ProcessM, pColorDefTable) || bAllToPrint) ? TRUE : FALSE;
		bYellowToPrint   = (pObjectTemplate->HasProcessColor(pMark, CColorDefinition::ProcessY, pColorDefTable) || bAllToPrint) ? TRUE : FALSE;
		bKeyToPrint		 = (pObjectTemplate->HasProcessColor(pMark, CColorDefinition::ProcessK, pColorDefTable) || bAllToPrint) ? TRUE : FALSE;
	}
	for (int i = 0; i < sheetSideProcessColors.GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsCyan(sheetSideProcessColors[i]))
			AddColorItem(CYAN,		theApp.m_colorDefTable.GetCyanName(),	 bCyanToPrint);
		else
			if (theApp.m_colorDefTable.IsMagenta(sheetSideProcessColors[i]))
				AddColorItem(MAGENTA,	theApp.m_colorDefTable.GetMagentaName(), bMagentaToPrint);
			else
				if (theApp.m_colorDefTable.IsYellow(sheetSideProcessColors[i]))
					AddColorItem(YELLOW,	theApp.m_colorDefTable.GetYellowName(),  bYellowToPrint);
				else
					if (theApp.m_colorDefTable.IsKey(sheetSideProcessColors[i]))
						AddColorItem(BLACK,		theApp.m_colorDefTable.GetKeyName(),	 bKeyToPrint);
	}
}

int CJobColorList::GetMaxColorNameLength(CDC* pDC)
{
	CFont font;
	font.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);

	int nMaxLen = 0;
	for (int i = 0; i < GetSize(); i++)
	{
		nMaxLen = max(pDC->GetTextExtent(GetAt(i).m_strColorName).cx, nMaxLen);
	}

	font.DeleteObject();

	return nMaxLen;
}
