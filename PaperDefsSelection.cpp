#include "stdafx.h"
#include "Imposition Manager.h"
#include "PaperDefsSelection.h"


IMPLEMENT_SERIAL(CPaperDefs, CObject, VERSIONABLE_SCHEMA | 2)

CPaperDefs::CPaperDefs() 
{ 
	m_strID		 = _T("");
	m_strName	 = _T("");
	m_nGrammage	 = 0;
	m_fVolume	 = 0.0f;
	m_fThickness = 0.0f;
	m_nGrade	 = 0;
	m_nType		 = 0;
	m_fShingling = 0.0f;
}

CPaperDefs::CPaperDefs(CPaperDefs& rPaperDefs) 
{ 
	*this = rPaperDefs;
}

CPaperDefs::~CPaperDefs()
{
}

const CPaperDefs& CPaperDefs::operator=(const CPaperDefs& rPaperDefs)
{
	m_strID		 = rPaperDefs.m_strID;
	m_strName	 = rPaperDefs.m_strName;
	m_nGrammage	 = rPaperDefs.m_nGrammage;
	m_fVolume	 = rPaperDefs.m_fVolume;
	m_fThickness = rPaperDefs.m_fThickness;
	m_nGrade	 = rPaperDefs.m_nGrade;
	m_nType		 = rPaperDefs.m_nType;
	m_fShingling = rPaperDefs.m_fShingling;

	return *this;
}
/////////////////////////////////////////////////////////////////////////////////////

CPaperDefsList::CPaperDefsList()
{
}

BOOL CPaperDefsList::LoadData()
{
	TCHAR			pszFileName[MAX_PATH];
	CFile			file;
	CFileException	fileException;

	_tcscpy(pszFileName, theApp.GetDataFolder()); 
	_tcscat(pszFileName, _T("\\PaperDefs.def")); 

	if ( ! file.Open(pszFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n",
				pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		RemoveAll(); // Be sure to work with an empty list
		CArchive archive(&file, CArchive::load);
		Serialize(archive);

		archive.Close();
		file.Close();
		return TRUE;  	
	}
}

BOOL CPaperDefsList::SaveData()
{
	TCHAR		   pszFileName[MAX_PATH];
	CFile		   file;
	CFileException fileException;

	_tcscpy(pszFileName, theApp.GetDataFolder()); 
	_tcscat(pszFileName, _T("\\PaperDefs.def"));
	if ( ! file.Open(pszFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n",
				pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&file, CArchive::store);
		Serialize(archive);

		archive.Close();
		file.Close();

		return TRUE;
	}
}

////////////////////////////////////////////////////////////////////////////////////////




CPaperDefsSelection::CPaperDefsSelection(void)
{
}

CPaperDefsSelection::~CPaperDefsSelection(void)
{
}

BOOL CPaperDefsSelection::InitComboBox(CWnd* pDlg, CString& rstrPaperName, int* pnGrammage, float* pfVolume, float* pfShingling, int nIDComboBox)
{
	if (theApp.m_paperDefsList.LoadData() == TRUE)
	{
		CPaperDefs paperDefsBuffer;

		CComboBox	*pBox    = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);
		UINT		nFormats = theApp.m_paperDefsList.GetCount(); 
		POSITION	pos      = theApp.m_paperDefsList.GetHeadPosition();

		pBox->ResetContent();
		for (UINT i = 0; i < nFormats; i++)    // fill combo box list 
		{
			paperDefsBuffer = theApp.m_paperDefsList.GetAt(pos);
			pBox->AddString(paperDefsBuffer.m_strName);
			theApp.m_paperDefsList.GetNext(pos);
		}	

		int nIndex = pBox->FindStringExact(-1, (LPCTSTR)rstrPaperName);

		if (nIndex == CB_ERR)
		{
			CString strString;

			paperDefsBuffer.m_strName	 = strString;
			paperDefsBuffer.m_nGrammage  = (pnGrammage)  ? *pnGrammage  : 0;
			paperDefsBuffer.m_fVolume	 = (pfVolume)    ? *pfVolume    : 0.0f;
			paperDefsBuffer.m_fShingling = (pfShingling) ? *pfShingling : 0.0f;
			nIndex = -1;
		}
		pBox->SetCurSel(nIndex);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
}

void CPaperDefsSelection::SelchangeFormatList(CWnd* pDlg, int* pnGrammage, float* pfVolume, float* pfShingling, CString& rstrPaperName, int nIDComboBox)
{
	CPaperDefs paperDefsBuffer;

	CComboBox *pBox	= (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

	int nIndex = pBox->GetCurSel();
	if (nIndex == CB_ERR)	// no item selected
		return;

	POSITION pos = theApp.m_paperDefsList.FindIndex(nIndex);
	if (pos == NULL)
		return;

	paperDefsBuffer = theApp.m_paperDefsList.GetAt(pos);

	rstrPaperName = paperDefsBuffer.m_strName;
	if (pnGrammage)
		*pnGrammage	 = paperDefsBuffer.m_nGrammage;
	if (pfVolume)
		*pfVolume	 = paperDefsBuffer.m_fVolume;
	if (pfShingling)
		*pfShingling = paperDefsBuffer.m_fShingling;

	pDlg->UpdateData(FALSE);
}

void CPaperDefsSelection::DeleteEntry(CWnd* pDlg, int* pnGrammage, float* pfVolume, float* pfShingling, CString& rstrPaperName, int nIDComboBox)
{
	if (theApp.m_paperDefsList.IsEmpty()) 
		return;

	CString strMessage;
	AfxFormatString1(strMessage, IDS_DEL_LISTENTRY, rstrPaperName);

	if (AfxMessageBox(strMessage, MB_YESNO) == IDYES)	
	{
		CComboBox *pBox  = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

		int nIndex = pBox->FindStringExact(-1, rstrPaperName);
		if (nIndex == CB_ERR)
			return;

		POSITION pos = theApp.m_paperDefsList.FindIndex(nIndex);
		if (pos == NULL)
			return;

		theApp.m_paperDefsList.RemoveAt(pos);
		pBox->DeleteString(nIndex);

		if (theApp.m_paperDefsList.SaveData() == FALSE)
		{
			AfxMessageBox(IDS_DEL_NOTOK, MB_OK);
			theApp.m_paperDefsList.LoadData();
		}
		if (!theApp.m_paperDefsList.IsEmpty()) 
		{
			if (pBox->GetCount() - 1 < nIndex)	// last entry was deleted
				nIndex = pBox->GetCount() - 1;

			pBox->SetCurSel(nIndex);
			SelchangeFormatList(pDlg, pnGrammage, pfVolume, pfShingling, rstrPaperName, nIDComboBox);	
		}
		else
		{
			rstrPaperName = "";
			if (pnGrammage)
				*pnGrammage	 = 0;
			if (pfVolume)
				*pfVolume	 = 0.0f;
			if (pfShingling)
				*pfShingling = 0.0f;
		}
	}
	pDlg->UpdateData(FALSE);      
}

void CPaperDefsSelection::NewEntry(CWnd* pDlg, int* pnGrammage, float* pfVolume, float* pfShingling, CString& rstrPaperName, int nIDComboBox)
{
	CPaperDefs paperDefsBuffer;
	CComboBox  *pBox = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

	pDlg->UpdateData(TRUE);      

	rstrPaperName.TrimLeft();
	rstrPaperName.TrimRight();
	if (rstrPaperName.IsEmpty())
	{
		AfxMessageBox(IDS_INVALID_NAME);
		return;
	}

	CString strMessage;
	AfxFormatString1(strMessage, IDS_NEW_LISTENTRY, rstrPaperName);

	if (AfxMessageBox(strMessage, MB_YESNO) == IDYES)	
	{
		paperDefsBuffer.m_strName	 = rstrPaperName;
		paperDefsBuffer.m_nGrammage  = (pnGrammage)  ? *pnGrammage  : 0;
		paperDefsBuffer.m_fVolume	 = (pfVolume)    ? *pfVolume    : 0.0f;
		paperDefsBuffer.m_fShingling = (pfShingling) ? *pfShingling : 0.0f;
		theApp.m_paperDefsList.AddTail(paperDefsBuffer);

		if (theApp.m_paperDefsList.SaveData() == FALSE)
			AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

		theApp.m_paperDefsList.LoadData(); //reload data
		pBox->AddString(rstrPaperName);
		pBox->SetCurSel(pBox->GetCount() - 1);
		SelchangeFormatList(pDlg, pnGrammage, pfVolume, pfShingling, rstrPaperName, nIDComboBox);	
	}

	pDlg->UpdateData(FALSE);      
}

void CPaperDefsSelection::ChangeEntry(CWnd* pDlg, int* pnGrammage, float* pfVolume, float* pfShingling, CString& rstrPaperName, int nIDComboBox)
{
	CPaperDefs paperDefsBuffer;
	CComboBox  *pBox = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

	pDlg->UpdateData(TRUE);      

	if (AfxMessageBox(IDS_CHANGE_ENTRY, MB_YESNO) == IDYES)	
	{
		paperDefsBuffer.m_strName	 = rstrPaperName;
		paperDefsBuffer.m_nGrammage  = (pnGrammage)  ? *pnGrammage  : 0;
		paperDefsBuffer.m_fVolume	 = (pfVolume)    ? *pfVolume    : 0.0f;
		paperDefsBuffer.m_fShingling = (pfShingling) ? *pfShingling : 0.0f;

		int nIndex = pBox->GetCurSel();
		if (nIndex != CB_ERR)
		{
			POSITION pos = theApp.m_paperDefsList.FindIndex(nIndex);
			theApp.m_paperDefsList.SetAt(pos, paperDefsBuffer);
			if (theApp.m_paperDefsList.SaveData() == FALSE)
				AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

			theApp.m_paperDefsList.LoadData(); //reload data
			pBox->SetCurSel(pBox->FindStringExact(-1, paperDefsBuffer.m_strName));
			SelchangeFormatList(pDlg, pnGrammage, pfVolume, pfShingling, rstrPaperName, nIDComboBox);	
		}
	}
	pDlg->UpdateData(FALSE);      
}

// helper function for PDFTargetList elements
template <> void AFXAPI SerializeElements <CPaperDefs> (CArchive& ar, CPaperDefs* pPaperDefs, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		ar.SerializeClass(RUNTIME_CLASS(CPaperDefs));

		if (ar.IsStoring())
		{
			ar << pPaperDefs->m_strName;
			ar << pPaperDefs->m_nGrammage;
			ar << pPaperDefs->m_fVolume;
			ar << pPaperDefs->m_fShingling;
		}
		else
		{
			UINT nVersion = ar.GetObjectSchema();
			switch (nVersion)
			{
			case 1:	ar >> pPaperDefs->m_strName;
					ar >> pPaperDefs->m_nGrammage;
					ar >> pPaperDefs->m_fVolume;
					pPaperDefs->m_fShingling = ((float)pPaperDefs->m_nGrammage / 1000.0f) * pPaperDefs->m_fVolume;
					break;
			case 2:	ar >> pPaperDefs->m_strName;
					ar >> pPaperDefs->m_nGrammage;
					ar >> pPaperDefs->m_fVolume;
					ar >> pPaperDefs->m_fShingling;
					break;
			default:break;
			}
		}
		
		pPaperDefs++;
	}
}

void AFXAPI CPaperDefsSelection::DDX_PaperDefsCombo(CDataExchange* pDX, int nIDC, CString& strValue)
{
	TCHAR szValue[MAX_TEXT];

	HWND hWndCtrl = pDX->PrepareEditCtrl(nIDC);
	if (pDX->m_bSaveAndValidate)
	{
		::GetWindowText(hWndCtrl, szValue, MAX_TEXT);  
		strValue = szValue;
	}
	else
		::SetWindowText(hWndCtrl, (LPCTSTR)strValue);
}
