// PrintSheetTreeViewOneLayout.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PrintSheetTreeView.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTreeViewOneLayout	for use with CipCut ...

void CPrintSheetTreeView::ShowTreeCtrlOneLayout() 
{
	TV_INSERTSTRUCT TreeCtrlItem;
	HTREEITEM hTreeItem1;
	HTREEITEM hTreeItem2;
	HTREEITEM hTreeItemPrev;
	CString	  string; 

	GetTreeCtrl().DeleteAllItems();	// Only delete tree-items -> NOT update the view

	CImpManDoc* pDoc = GetDocument();
	if ((pDoc->m_PrintSheetList.IsEmpty()) || pDoc->m_Bookblock.IsEmpty())
		return;
  
	CGlobalData* pGD = &pDoc->m_GlobalData;
	TreeCtrlItem.hParent			 = TVI_ROOT;
	TreeCtrlItem.hInsertAfter		 = TVI_LAST;
	TreeCtrlItem.item.iImage		 = 0;
	TreeCtrlItem.item.iSelectedImage = 0;
	TreeCtrlItem.item.mask			 = TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	TreeCtrlItem.item.cchTextMax	 = MAX_PATH;
	string = pDoc->GetTitle();
	TreeCtrlItem.item.pszText		 = string.GetBuffer(MAX_PATH);
	TreeCtrlItem.item.lParam		 = -1;  // -1 means root/Document
	hTreeItem1 = GetTreeCtrl().InsertItem(&TreeCtrlItem);

	m_hTreeItemList.RemoveAll();
	m_hTreeItemList.AddTail(hTreeItem1);

	CLayout	 ActLayout;
	POSITION pos;

	pos  = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	if (pos)
		ActLayout  = pDoc->m_PrintSheetList.m_Layouts.GetAt(pos);
	else
		return;

	string.Format("%s", ActLayout.m_strLayoutName);
	TreeCtrlItem.hParent			 = hTreeItem1;
	TreeCtrlItem.item.iImage		 = 1;
	TreeCtrlItem.item.iSelectedImage = 1;
	TreeCtrlItem.item.cchTextMax	 = MAX_TEXT;
	TreeCtrlItem.item.pszText		 = string.GetBuffer(MAX_TEXT);
	TreeCtrlItem.item.lParam		 = 1000; // >= 0 means printsheetlist
	hTreeItem2 = GetTreeCtrl().InsertItem(&TreeCtrlItem);
	m_hTreeItemList.AddTail(hTreeItem2);

	pos = m_hTreeItemList.FindIndex(m_ActPrintSheetNumber);
	hTreeItemPrev = m_hTreeItemList.GetAt(pos);
	GetTreeCtrl().Select(hTreeItemPrev, TVGN_CARET);
	GetTreeCtrl().Expand(hTreeItemPrev, TVE_EXPAND);
}
