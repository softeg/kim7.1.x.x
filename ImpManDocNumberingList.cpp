#include "stdafx.h"

#include "Imposition Manager.h"


/////////////////////////////////////////////////////////////////////////////
// CNumberingList

IMPLEMENT_SERIAL(CNumberingList, CObject, VERSIONABLE_SCHEMA | 1)

CNumberingList::CNumberingList()
{
}

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CNumbering

IMPLEMENT_SERIAL(CNumbering, CNumbering, VERSIONABLE_SCHEMA | 1)

CNumbering::CNumbering()
{
	m_nStartNum			= 1;
	m_nRangeTo			= 1;
	m_nRangeFrom		= 1;
	m_strNumberingStyle = _T("$n");
	m_bReverse			= FALSE;
}

CNumbering::CNumbering(const CNumbering& rNumbering)
{
	Copy(rNumbering);
}

const CNumbering& CNumbering::operator=(const CNumbering& rNumbering)
{
	Copy(rNumbering);

	return *this;
}

void CNumbering::Copy(const CNumbering& rNumbering)
{
	m_nStartNum			= rNumbering.m_nStartNum;
	m_nRangeTo			= rNumbering.m_nRangeTo;
	m_nRangeFrom		= rNumbering.m_nRangeFrom;
	m_strNumberingStyle = rNumbering.m_strNumberingStyle;
	m_bReverse			= rNumbering.m_bReverse;
}

// helper function for CNumbering elements
template <> void AFXAPI SerializeElements <CNumbering> (CArchive& ar, CNumbering* pNumbering, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CNumbering));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pNumbering->m_nStartNum;
			ar << pNumbering->m_nRangeTo;
			ar << pNumbering->m_nRangeFrom;
			ar << pNumbering->m_strNumberingStyle;	
			ar << pNumbering->m_bReverse;
		}	
		else
		{ 
			switch (nVersion)
			{
			case 1:	ar >> pNumbering->m_nStartNum;
					ar >> pNumbering->m_nRangeTo;
					ar >> pNumbering->m_nRangeFrom;
					ar >> pNumbering->m_strNumberingStyle;
					ar >> pNumbering->m_bReverse;
					break;
			}
		}

		pNumbering++;
	}
}

