// DlgMarkSet.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "PrintSheetMarksView.h" 
#include "Shlwapi.h"
#include "DlgMarkSet.h"
#include "ProdDataPrintView.h"
#include "NewPrintSheetFrame.h"
#include "PrintSheetObjectPreview.h"
#include "GraphicComponent.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



CString	CDirList::FindMark(CMark* pMark, CString strMarkName, CString strFolder, CPrintSheet* pPrintSheet, int nSide)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_nAttribute == CDirItem::IsMarkset)
		{
			CString			strFileName; strFileName.Format(_T("%s\\%s.mks"), strFolder, GetAt(i).m_strFileName);
			CFile			markDefFile;
			CFileException  fileException;
			if (markDefFile.Open(strFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
			{
				CMarkList tmpMarkList;
				CArchive archive(&markDefFile, CArchive::load);
				tmpMarkList.Serialize(archive);
				archive.Close();
				markDefFile.Close();

				if (pMark)
				{
					for (int j = 0; j < tmpMarkList.GetSize(); j++)
						if (*tmpMarkList[j] == *pMark)
							//if (tmpMarkList[j]->IsEnabled(pPrintSheet, nSide))
								return GetAt(i).m_strFileName;
				}
				else
				{
					for (int j = 0; j < tmpMarkList.GetSize(); j++)
						if (tmpMarkList[j]->m_strMarkName == strMarkName)
							//if (tmpMarkList[j]->IsEnabled(pPrintSheet, nSide))
								return GetAt(i).m_strFileName;
				}
			}
		}
	}

	return _T("");
}


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMarkSet 

IMPLEMENT_DYNCREATE(CDlgMarkSet, CDialog)


CDlgMarkSet::CDlgMarkSet(CWnd* pParent /*=NULL*/, BOOL bRunModal)
	: CDialog(CDlgMarkSet::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgMarkSet)
	//}}AFX_DATA_INIT

	m_bRunModal			  = bRunModal;
	m_bBrowseOnly		  = FALSE;
	m_bOpenAutoMarks	  = FALSE;
	m_bIsEditMode		  = FALSE;

	m_pToolTip			  = NULL;
	m_pMarkImageList	  = new CImageList();
	m_pBrowserImageList   = new CImageList();
	m_pBrowserStaticBrush = new CBrush(RGB(200,200,210));
	m_pBrowserHeaderBrush = new CBrush(RGB(220,220,230));		
	m_pMarksetHeaderBrush = new CBrush(RGB(255,228,136));	
	m_pActPrintSheet	  = NULL;

	m_pWnd				  = NULL;
	m_pNewMarkPopupMenu	  = NULL;
}

CDlgMarkSet::~CDlgMarkSet()
{
	delete m_pToolTip;
	delete m_pMarkImageList;
	delete m_pBrowserImageList;
	delete m_pBrowserStaticBrush;
	delete m_pBrowserHeaderBrush;
	delete m_pMarksetHeaderBrush;
	if ( ! m_bRunModal)
		DestroyWindow();
}

HBRUSH CDlgMarkSet::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	switch (nCtlColor)
	{
	case CTLCOLOR_STATIC:	
		{
			int nID = pWnd->GetDlgCtrlID(); 
			switch (nID)
			{
			case IDC_BROWSER_STATIC:	pDC->SetBkMode(TRANSPARENT);
										return (HBRUSH)(m_pBrowserStaticBrush->GetSafeHandle());
			case IDC_BROWSER_HEADER:	pDC->SetBkMode(TRANSPARENT);
										return (HBRUSH)(m_pBrowserHeaderBrush->GetSafeHandle());
			case IDC_MARKSET_HEADER:	pDC->SetBkMode(TRANSPARENT);
										return (HBRUSH)(m_pMarksetHeaderBrush->GetSafeHandle());
			default:					break;
			}
		}
		break;

	default:				
		break;
	}
	return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
}

void CDlgMarkSet::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType == ODT_MENU)
	{
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		CRect rcItem = lpDrawItemStruct->rcItem;

		pDC->FillSolidRect(rcItem, RGB(252, 252, 249));//CREME);

		CRect rcIcon = rcItem;
		rcIcon.right = rcIcon.left + 25;

		if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			CGraphicComponent gc;
			gc.DrawTransparentFrame(pDC, rcItem, RGB(120,160,220), 2);
		}				

		switch (lpDrawItemStruct->itemID)
		{
		case ID_NEWMARK_PDF:		pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_CUSTOM_MARK_UNASSIGNED));	break;
		case ID_NEWMARK_TEXT:		pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_TEXT_MARK_UNASSIGNED));		break;
		case ID_NEWMARK_SECTION:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_SECTION_MARK_UNASSIGNED));	break;
		case ID_NEWMARK_CROP:		pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_CROP_MARK_UNASSIGNED));		break;
		case ID_NEWMARK_FOLD:		pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_FOLD_MARK_UNASSIGNED));		break;
		case ID_NEWMARK_BARCODE:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_BARCODE_MARK_UNASSIGNED));	break;
		case ID_NEWMARK_RECTANGLE:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_DRAWING_MARK_UNASSIGNED));	break;
		}

		pDC->SelectObject(GetFont());
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(DARKBLUE);
		CRect rcText = rcItem;
		rcText.left = rcIcon.right + 5;
		if (m_pNewMarkPopupMenu)
		{
			CString strText;
			m_pNewMarkPopupMenu->GetMenuString(lpDrawItemStruct->itemID, strText, MF_BYCOMMAND);
			pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		}
	}
	else
		CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CDlgMarkSet::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct->CtlType == ODT_MENU)
	{
		int	nMaxLen = 0;
		if (m_pNewMarkPopupMenu)
		{
			CString strText;
			m_pNewMarkPopupMenu->GetMenuString(lpMeasureItemStruct->itemID, strText, MF_BYCOMMAND);
			CClientDC dc(this);
			dc.SelectObject(GetFont());
			nMaxLen = dc.GetTextExtent(strText).cx + 40;
		}
		lpMeasureItemStruct->itemWidth  = nMaxLen;
		lpMeasureItemStruct->itemHeight = 22;
	}
	else
		CDialog::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CDlgMarkSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgMarkSet)
	DDX_Control(pDX, IDC_MARKSET_BROWSER, m_marksetBrowser);
	DDX_Control(pDX, IDC_MARK_LIST, m_markListCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgMarkSet, CDialog)
	//{{AFX_MSG_MAP(CDlgMarkSet)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_MARK_NEW, OnMarkNew)
	ON_NOTIFY(NM_CLICK, IDC_MARK_LIST, OnClickMarkList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_MARK_LIST, OnItemchangedMarkList)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_MARK_LIST, OnEndlabeleditMarkList)
	ON_BN_CLICKED(IDC_OPEN_MARKSET, OnOpenMarkset)
	ON_BN_CLICKED(IDC_CLOSE_MARKSETDLG, OnCloseMarksetDlg)
	ON_BN_CLICKED(IDC_MARK_DUPLICATE, OnMarkDuplicate)
	ON_BN_CLICKED(IDC_MARK_UP, OnMarkUp)
	ON_BN_CLICKED(IDC_MARK_DOWN, OnMarkDown)
	ON_BN_CLICKED(IDC_ASSIGN_MARKSET, OnAssignMarkset)
	ON_WM_CLOSE()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_MARK_DELETE, OnMarkDelete)
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	ON_BN_CLICKED(IDC_ASSIGN_SELECTEDMARK, OnAssignSelectedmark)
	ON_NOTIFY(NM_DBLCLK, IDC_MARKSET_BROWSER, OnDblclkMarksetBrowser)
	ON_BN_CLICKED(IDC_FOLDER_UP, OnFolderUp)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_MARKSET_BROWSER, OnEndlabeleditMarksetBrowser)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_FOLDER_NEW, OnFolderNew)
	ON_BN_CLICKED(IDC_MARKSET_NEW, OnMarksetNew)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_MARKSET_BROWSER, OnItemchangedMarksetBrowser)
	ON_NOTIFY(NM_DBLCLK, IDC_MARK_LIST, OnDblclkMarkList)
	ON_NOTIFY(NM_CLICK, IDC_MARKSET_BROWSER, OnClickMarksetBrowser)
	ON_NOTIFY(LVN_KEYDOWN, IDC_MARKSET_BROWSER, OnKeydownMarksetBrowser)
	ON_NOTIFY(LVN_KEYDOWN, IDC_MARK_LIST, OnKeydownMarkList)
	ON_BN_CLICKED(IDC_MARKSET_DUPLICATE, OnMarksetDuplicate)
	ON_NOTIFY(NM_SETFOCUS, IDC_MARKSET_BROWSER, OnSetfocusMarksetBrowser)
	ON_BN_CLICKED(IDCANCEL, OnCancel)
	ON_NOTIFY(NM_SETFOCUS, IDC_MARK_LIST, OnSetfocusMarkList)
	ON_NOTIFY(NM_KILLFOCUS, IDC_MARK_LIST, OnNMKillfocusMarkList)
	//}}AFX_MSG_MAP
	ON_WM_ACTIVATE()
	ON_COMMAND(ID_UPDATE_SUBDIALOGS, OnUpdateSubDialogs)
	ON_COMMAND(ID_UPDATE_MARKNAME, OnUpdateMarkname)
	ON_STN_CLICKED(IDC_MARKPROPS_FRAME, &CDlgMarkSet::OnStnClickedMarkpropsFrame)
	ON_COMMAND(ID_NEWMARK_PDF, &CDlgMarkSet::OnNewMarkPDF)
	ON_COMMAND(ID_NEWMARK_TEXT, &CDlgMarkSet::OnNewMarkText)
	ON_COMMAND(ID_NEWMARK_SECTION, &CDlgMarkSet::OnNewMarkSection)
	ON_COMMAND(ID_NEWMARK_CROP, &CDlgMarkSet::OnNewMarkCrop)
	ON_COMMAND(ID_NEWMARK_FOLD, &CDlgMarkSet::OnNewMarkFold)
	ON_COMMAND(ID_NEWMARK_BARCODE, &CDlgMarkSet::OnNewMarkBarcode)
	ON_COMMAND(ID_NEWMARK_RECTANGLE, &CDlgMarkSet::OnNewMarkRectangle)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgMarkSet 

BOOL CDlgMarkSet::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( ! m_bBrowseOnly)
		theApp.RestoreWindowPlacement(this, _T("CDlgMarkSet"));

	m_nCurMarkSel = -1;
	m_markListCtrl.DeleteAllItems();
	m_markList.RemoveAll();

	m_dlgSelectNewMark.Create(IDD_SELECT_NEW_MARK, this);
	m_dlgSelectNewMark.m_pParent = this;

	SetIcon(theApp.LoadIcon(IDI_MARKSET), TRUE);

// Set up button icons
	//((CButton*)GetDlgItem(IDC_ASSIGN_MARKSET	 ))->SetIcon(theApp.LoadIcon(IDI_ASSIGN_MARKSET));
	//((CButton*)GetDlgItem(IDC_ASSIGN_SELECTEDMARK))->SetIcon(theApp.LoadIcon(IDI_ASSIGN_SELECTEDMARK));
	((CButton*)GetDlgItem(IDC_MARK_NEW			 ))->SetIcon(theApp.LoadIcon(IDI_MARK_NEW));
	((CButton*)GetDlgItem(IDC_MARK_DUPLICATE	 ))->SetIcon(theApp.LoadIcon(IDI_MARK_DUPLICATE));
	((CButton*)GetDlgItem(IDC_MARK_DELETE		 ))->SetIcon(theApp.LoadIcon(IDI_DELETE));

	((CButton*)GetDlgItem(IDC_FOLDER_UP		   ))->SetIcon(theApp.LoadIcon(IDI_FOLDER_UP));
	((CButton*)GetDlgItem(IDC_FOLDER_NEW	   ))->SetIcon(theApp.LoadIcon(IDI_FOLDER_NEW));
	((CButton*)GetDlgItem(IDC_MARKSET_NEW	   ))->SetIcon(theApp.LoadIcon(IDI_MARKSET_NEW));
	((CButton*)GetDlgItem(IDC_MARKSET_DUPLICATE))->SetIcon(theApp.LoadIcon(IDI_MARKSET_DUPLICATE));
	((CButton*)GetDlgItem(IDC_DELETE		   ))->SetIcon(theApp.LoadIcon(IDI_DELETE));

	m_markUpButton.SubclassDlgItem	(IDC_MARK_UP, this);
	m_markUpButton.SetBitmap		(IDB_ARROW_UP, IDB_ARROW_UP, IDB_ARROW_UP_DISABLED, XCENTER);
	m_markDownButton.SubclassDlgItem(IDC_MARK_DOWN, this);
	m_markDownButton.SetBitmap		(IDB_ARROW_DOWN, IDB_ARROW_DOWN, IDB_ARROW_DOWN_DISABLED, XCENTER);

// Set up tooltips       
	m_pToolTip = new CToolTipCtrl;
    m_pToolTip->Create(this);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_ASSIGN_MARKSET)),			IDS_ASSIGN_MARKSET_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_ASSIGN_SELECTEDMARK)),	IDS_ASSIGN_SELECTEDMARK_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_NEW)),				IDS_MARK_NEW_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_DUPLICATE)),			IDS_MARK_DUPLICATE_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_DELETE)),			IDS_MARK_DELETE_TIP);

	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_FOLDER_UP)),				IDS_FOLDER_UP_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_FOLDER_NEW)),				IDS_NEW_FOLDER);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARKSET_NEW)),			IDS_NEW_MARKSET);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARKSET_DUPLICATE)),		IDS_MARKSET_DUPLICATE_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_DELETE)),					IDS_MARKSET_FOLDER_DELETE_TIP);

	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_UP)),	IDS_MARK_UP_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_DOWN)),	IDS_MARK_DOWN_TIP);

	m_pToolTip->Activate(TRUE);

// mark list image list
	CBitmap bitmap;
	m_pMarkImageList->Create(16, 16, ILC_COLOR8, 3, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_CUSTOM_MARK_UNASSIGNED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TEXT_MARK_UNASSIGNED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_SECTION_MARK_UNASSIGNED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CROP_MARK_UNASSIGNED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_FOLD_MARK_UNASSIGNED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_BARCODE_MARK_UNASSIGNED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_DRAWING_MARK_UNASSIGNED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CUSTOM_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TEXT_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_SECTION_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CROP_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_FOLD_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_BARCODE_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	m_markListCtrl.SetImageList(m_pMarkImageList, LVSIL_SMALL);

	bitmap.LoadBitmap(IDB_DRAWING_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	m_markListCtrl.SetImageList(m_pMarkImageList, LVSIL_SMALL);
////////////////////////

// browser image list
	m_pBrowserImageList->Create(16, 16, ILC_COLOR8, 3, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_FOLDER);
	m_pBrowserImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_MARKSET);
	m_pBrowserImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	m_marksetBrowser.SetImageList(m_pBrowserImageList, LVSIL_SMALL);
/////////////////////

	GetDlgItem(IDC_FOLDER_UP)->EnableWindow(FALSE);
	if (m_strCurrentFolder.IsEmpty())
		m_strCurrentFolder = theApp.GetMarksetInitialFolder(m_bOpenAutoMarks);

	CString strShortCurrentFolder, strShortInitialFolder;
	GetShortPathName(m_strCurrentFolder, strShortCurrentFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortCurrentFolder.ReleaseBuffer();
	GetShortPathName(theApp.GetMarksetInitialFolder(m_bOpenAutoMarks), strShortInitialFolder.GetBuffer(MAX_TEXT), MAX_TEXT); strShortInitialFolder.ReleaseBuffer();
	strShortCurrentFolder.MakeUpper();
	strShortInitialFolder.MakeUpper();
	if (strShortCurrentFolder == strShortInitialFolder)
		GetDlgItem(IDC_FOLDER_UP)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_FOLDER_UP)->EnableWindow(TRUE);

	if (m_bRunModal)
	{
		GetDlgItem(IDC_ASSIGN_SELECTEDMARK)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_OPEN_MARKSET)->EnableWindow(TRUE);
		LoadMarkSet();
		InitMarkList();
	}

	m_bIsEditMode = FALSE;
	if (m_bOpenAutoMarks && ! m_strMarkName.IsEmpty())
	{
		m_bIsEditMode = TRUE;
		CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
		CMark* pAutoMark = NULL;
		if ( m_strMarkName.IsEmpty())
			pAutoMark = (pProdDataPrintView) ? pProdDataPrintView->GetCurrentMark() : NULL;
		InitMarksetBrowser(m_strCurrentMarkset, pAutoMark, m_strMarkName);
	}
	else
		InitMarksetBrowser(m_strCurrentMarkset);

	m_dlgLayoutObjectControl.Initialize(IDC_MARKPROPS_FRAME,   GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable, this);

	GetDlgItem(IDC_MARKSET_HEADER	)->GetWindowRect(m_marksetHeaderRectBrowserMode);
	GetDlgItem(IDC_MARK_LIST		)->GetWindowRect(m_markListRectBrowserMode);
	GetDlgItem(IDC_MARKPROPS_PREVIEW)->GetWindowRect(m_previewRectBrowserMode);
	ScreenToClient(m_marksetHeaderRectBrowserMode);
	ScreenToClient(m_markListRectBrowserMode);
	ScreenToClient(m_previewRectBrowserMode);

	if 	(m_bIsEditMode && ! m_strCurrentMarkset.IsEmpty())
	{
		SetEditMode();
		//m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable, FALSE);
	}
	else
		SetBrowserMode();

	GetDlgItem(IDC_DELETE		 )->EnableWindow(FALSE);
	GetDlgItem(IDC_ASSIGN_MARKSET)->EnableWindow((m_marksetBrowser.GetSelectedCount() > 0) ? TRUE : FALSE);
	GetDlgItem(IDC_OPEN_MARKSET	 )->EnableWindow((m_marksetBrowser.GetSelectedCount() > 0) ? TRUE : FALSE);

	CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pView)
		pView->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgMarkSet::OnDestroy() 
{
	if ( ! m_bBrowseOnly)
		theApp.SaveWindowPlacement(this, _T("CDlgMarkSet"));

	m_pMarkImageList->DeleteImageList();
	m_pBrowserImageList->DeleteImageList();

	CPrintSheetMarksView* pView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pView)
		((CButton*)pView->GetDlgItem(IDC_OPEN_MARKLIB))->SetCheck(0);

	m_bOpenAutoMarks = FALSE;

	CDialog::OnDestroy();
}

void CDlgMarkSet::PostNcDestroy()
{
	CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pView)
		pView->EnableWindow(TRUE);

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));

	CDialog::PostNcDestroy();
}

void CDlgMarkSet::SetBrowserMode()
{
	m_bIsEditMode = FALSE;

	GetDlgItem(IDC_MARKSET_HEADER	)->MoveWindow(m_marksetHeaderRectBrowserMode);
	GetDlgItem(IDC_MARK_LIST		)->MoveWindow(m_markListRectBrowserMode);
	GetDlgItem(IDC_MARKPROPS_PREVIEW)->MoveWindow(m_previewRectBrowserMode);
	//m_dlgObjectPreviewControl.Move(IDC_MARKPROPS_PREVIEW, this);

	GetDlgItem(IDC_MARK_NEW		  )->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MARK_DUPLICATE )->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MARK_DELETE	  )->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MARK_UP		  )->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MARK_DOWN	  )->ShowWindow(SW_HIDE);

	GetDlgItem(IDOK				  )->ShowWindow(SW_HIDE);
	GetDlgItem(IDCANCEL			  )->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_APPLY_BUTTON	  )->ShowWindow(SW_HIDE);

	GetDlgItem(IDC_FOLDER_UP		)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_FOLDER_NEW		)->ShowWindow((m_bBrowseOnly) ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_MARKSET_NEW		)->ShowWindow((m_bBrowseOnly) ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_MARKSET_DUPLICATE)->ShowWindow((m_bBrowseOnly) ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_DELETE			)->ShowWindow((m_bBrowseOnly) ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_BROWSER_STATIC	)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BROWSER_HEADER	)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_MARKSET_BROWSER	)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_OPEN_MARKSET		)->ShowWindow((m_bBrowseOnly && ! m_bOpenAutoMarks) ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_CLOSE_MARKSETDLG	)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_ASSIGN_STATIC	)->ShowWindow((m_bBrowseOnly || m_bOpenAutoMarks)   ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_ASSIGN2_STATIC	)->ShowWindow((m_bBrowseOnly || m_bOpenAutoMarks)   ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_ASSIGN_MARKSET	)->ShowWindow((m_bBrowseOnly || m_bOpenAutoMarks)   ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_ASSIGN_SELECTEDMARK)->ShowWindow((m_bBrowseOnly || m_bOpenAutoMarks) ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_ASSIGN_SELECTEDMARK)->EnableWindow((m_markListCtrl.GetItemCount() > 0) ? TRUE : FALSE);

	if (m_dlgLayoutObjectControl.m_hWnd)	// maybe not yet initialized
		m_dlgLayoutObjectControl.ShowWindow(SW_HIDE);

	CRect winRect, browserRect;
	GetWindowRect(winRect);
	GetDlgItem(IDC_OPEN_MARKSET)->GetWindowRect(browserRect);
	MoveWindow(winRect.left, winRect.top, browserRect.right - winRect.left + 20, winRect.Height());
}

void CDlgMarkSet::SetEditMode()
{
	m_bIsEditMode = TRUE;

	CRect rectPropsFrame, headerRect, buttonRect;
	GetDlgItem(IDC_MARKPROPS_FRAME	)->GetWindowRect(rectPropsFrame);
	GetDlgItem(IDC_MARKSET_HEADER	)->GetWindowRect(headerRect);
	GetDlgItem(IDC_FOLDER_UP		)->GetWindowRect(buttonRect);
	ScreenToClient(rectPropsFrame);
	ScreenToClient(headerRect);
	ScreenToClient(buttonRect);

	GetDlgItem(IDC_MARKSET_HEADER)->MoveWindow(headerRect.left, rectPropsFrame.top + 2, headerRect.Width(), headerRect.Height());
	int nLeft = m_markListRectBrowserMode.left;
	int nTop  = rectPropsFrame.top + 2 + headerRect.Height();
	GetDlgItem(IDC_MARK_LIST		)->MoveWindow(nLeft, nTop, rectPropsFrame.left - nLeft - 5, m_markListRectBrowserMode.bottom - nTop);
	GetDlgItem(IDC_MARKSET_HEADER	)->MoveWindow(nLeft, nTop - m_marksetHeaderRectBrowserMode.Height(), rectPropsFrame.left - nLeft - 5, m_marksetHeaderRectBrowserMode.Height());
	//GetDlgItem(IDC_MARKPROPS_PREVIEW)->MoveWindow(m_markListRectBrowserMode.left, m_previewRectBrowserMode.top, m_markListRectBrowserMode.Width(), m_previewRectBrowserMode.Height());
	//m_dlgObjectPreviewControl.Move(IDC_MARKPROPS_PREVIEW, this);

	GetDlgItem(IDC_MARK_NEW		  )->MoveWindow(buttonRect);	buttonRect.OffsetRect(0, buttonRect.Height());
	GetDlgItem(IDC_MARK_DUPLICATE )->MoveWindow(buttonRect);	buttonRect.OffsetRect(0, buttonRect.Height());
	GetDlgItem(IDC_MARK_DELETE	  )->MoveWindow(buttonRect);	buttonRect.OffsetRect(0, buttonRect.Height() + 20);	buttonRect.DeflateRect(1, 1);
	GetDlgItem(IDC_MARK_UP		  )->MoveWindow(buttonRect);	buttonRect.OffsetRect(0, buttonRect.Height());
	GetDlgItem(IDC_MARK_DOWN	  )->MoveWindow(buttonRect);	

	GetDlgItem(IDC_MARK_NEW		  )->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_MARK_DUPLICATE )->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_MARK_DELETE	  )->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_MARK_UP		  )->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_MARK_DOWN	  )->ShowWindow(SW_SHOW);

	GetDlgItem(IDOK				  )->ShowWindow(SW_SHOW);
	GetDlgItem(IDCANCEL			  )->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_APPLY_BUTTON	  )->ShowWindow(SW_SHOW);

	GetDlgItem(IDC_FOLDER_UP		)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_FOLDER_NEW		)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MARKSET_NEW		)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MARKSET_DUPLICATE)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_DELETE			)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BROWSER_STATIC	)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BROWSER_HEADER	)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MARKSET_BROWSER	)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_OPEN_MARKSET		)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_CLOSE_MARKSETDLG	)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_ASSIGN_STATIC	)->ShowWindow((m_bBrowseOnly || m_bOpenAutoMarks)   ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_ASSIGN2_STATIC	)->ShowWindow((m_bBrowseOnly || m_bOpenAutoMarks)   ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_ASSIGN_MARKSET	)->ShowWindow((m_bBrowseOnly || m_bOpenAutoMarks)   ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_ASSIGN_SELECTEDMARK)->ShowWindow((m_bBrowseOnly || m_bOpenAutoMarks) ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_ASSIGN_SELECTEDMARK)->EnableWindow((m_markListCtrl.GetItemCount() > 0) ? TRUE : FALSE);

	if (m_dlgLayoutObjectControl.m_hWnd)
		m_dlgLayoutObjectControl.ShowWindow(SW_SHOW);

	CRect winRect, okRect, marksViewRect;
	GetWindowRect(winRect);
	GetDlgItem(IDOK)->GetWindowRect(okRect);
	nLeft  = winRect.left;
	int nWidth = okRect.right - winRect.left + 10;
	CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pMarksView)
	{
		pMarksView->GetWindowRect(marksViewRect);
		if (nLeft + nWidth > marksViewRect.Width())
			nLeft = marksViewRect.Width() - nWidth;
	}
	MoveWindow(nLeft, winRect.top, nWidth, winRect.Height());

	m_markListCtrl.ModifyStyle(0, LVS_SHOWSELALWAYS);
}

// markset browser ////////////////////////////////////////////////////////////////////////////////////////////////////
//
int CDlgMarkSet::InitMarksetBrowser(CString strMarksetToSelect, CMark* pMark, CString strMarkName)
{
	WIN32_FIND_DATA findData;
	BOOL			bFilesExist = FALSE;	

	CString strFilename = m_strCurrentFolder; strFilename += CString("\\*.*");
	HANDLE  hfileSearch = FindFirstFile(strFilename, &findData);

	m_dirList.RemoveAll();
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		CString	strBuffer	= findData.cFileName;
		m_strCurrentMarkset = strBuffer.SpanExcluding(_T("."));
		bFilesExist			= TRUE;
		do 
		{
			strBuffer = findData.cFileName;
			if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
			{
				if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					CDirItem dirItem(strBuffer, CDirItem::IsFolder);
					m_dirList.Add(dirItem);
				}
				else 
				{
					CString strExt = strBuffer.Right(4);
					strExt.MakeLower();
					if (strExt == _T(".mks"))
					{
						strBuffer = theApp.RemoveFileExtension(strBuffer, _T(".mks"));
						CDirItem dirItem(strBuffer, CDirItem::IsMarkset);
						m_dirList.Add(dirItem);
					}
				}
			}
		}
		while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);

	m_dirList.Sort();

	m_marksetBrowser.DeleteAllItems();
	int nSel = -1;
	for (int i = 0; i < m_dirList.GetSize(); i++)
	{
		LV_ITEM item;
		item.mask		= LVIF_TEXT | LVIF_IMAGE;
		item.cchTextMax = MAX_TEXT;
		item.iItem		= i; 
		item.iSubItem	= 0;
		item.pszText	= m_dirList[i].m_strFileName.GetBuffer(MAX_TEXT);
		item.iImage		= (m_dirList[i].m_nAttribute == CDirItem::IsFolder) ? 0 : 1;
		m_marksetBrowser.InsertItem(&item);
		if (strMarksetToSelect == m_dirList[i].m_strFileName)
			nSel = i;
	}
	if ( (nSel >= 0) && (nSel < m_dirList.GetSize()) )
	{
		if (m_dirList[nSel].m_nAttribute == CDirItem::IsMarkset)
		{
			m_strCurrentMarkset = m_dirList[nSel].m_strFileName;
			LoadMarkSet();
		}
		else
			m_strCurrentMarkset = "";
	}
	else
		if (pMark || ! strMarkName.IsEmpty())
		{
			m_strCurrentMarkset = m_dirList.FindMark(pMark, strMarkName, m_strCurrentFolder, GetPrintSheet(), GetActSight(NULL));
			LoadMarkSet();
		}
		else
			m_strCurrentMarkset = "";

	if (nSel >= 0)
	{
		m_marksetBrowser.SetItemState(nSel, LVIS_SELECTED, LVIS_SELECTED);
		m_marksetBrowser.EnsureVisible(nSel, FALSE);
	}

	if (m_strCurrentMarkset.IsEmpty())
		m_markList.RemoveAll();

	CRect headerRect;
	GetDlgItem(IDC_BROWSER_HEADER)->GetWindowRect(headerRect);
	CString strRelativePath;
	PathRelativePathTo(strRelativePath.GetBuffer(MAX_PATH), (LPCTSTR)theApp.GetMarksetInitialFolder(m_bOpenAutoMarks), FILE_ATTRIBUTE_DIRECTORY, (LPCTSTR)m_strCurrentFolder, FILE_ATTRIBUTE_DIRECTORY);
	strRelativePath.ReleaseBuffer();
	CString strOut;
	strOut.Format(_T("  %s"), strRelativePath);
	CClientDC clientDC(this);
	PathCompactPath(clientDC.m_hDC, strOut.GetBuffer(MAX_PATH), headerRect.Width());

	GetDlgItem(IDC_BROWSER_HEADER)->SetWindowText(strOut);

	InitMarkList(NULL, (pMark) ? pMark->m_strMarkName : strMarkName);

	return nSel;
}

void CDlgMarkSet::OnDblclkMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	*pResult = 0;
	
	if ( (pNMListView->iItem >= 0) && (pNMListView->iItem < m_dirList.GetSize()) )
	{
		CDirItem dirItem = m_dirList[pNMListView->iItem];
		if (dirItem.m_nAttribute == CDirItem::IsFolder)
		{
			m_strCurrentFolder += _T("\\") + dirItem.m_strFileName;
			InitMarksetBrowser();
			m_strCurrentMarkset = "";
			m_markList.RemoveAll();
			InitMarkList();

			GetDlgItem(IDC_FOLDER_UP)->EnableWindow(TRUE);
		}
		else
			OnOpenMarkset();
	}
}

void CDlgMarkSet::OnClickMarksetBrowser(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	if (m_marksetBrowser.GetNextItem(-1, LVIS_SELECTED) == -1)	// clicked outside any item
		InitMarksetBrowser((m_strCurrentMarkset.IsEmpty()) ? m_strCurrentFolder : m_strCurrentMarkset);

	*pResult = 0;
}

void CDlgMarkSet::OnEndlabeleditMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	
	*pResult = 0;

	if ( (pDispInfo->item.iItem < 0) || (pDispInfo->item.iItem >= m_dirList.GetSize()) )
		return;

	CString strNewName;
	m_marksetBrowser.GetEditControl()->GetWindowText(strNewName);

	CString strOldPath, strNewPath;
	if (m_dirList[pDispInfo->item.iItem].m_nAttribute == CDirItem::IsMarkset)
	{
		strOldPath.Format(_T("%s\\%s.mks"), m_strCurrentFolder, m_dirList[pDispInfo->item.iItem].m_strFileName);
		strNewPath.Format(_T("%s\\%s.mks"), m_strCurrentFolder, strNewName);
		m_strCurrentMarkset = strNewName;
	}
	else
	{
		strOldPath.Format(_T("%s\\%s"), m_strCurrentFolder, m_dirList[pDispInfo->item.iItem].m_strFileName);
		strNewPath.Format(_T("%s\\%s"), m_strCurrentFolder, strNewName);
	}
	_trename(strOldPath, strNewPath);

	InitMarksetBrowser(strNewName);
}

void CDlgMarkSet::OnItemchangedMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	
	*pResult = 0;

	int nNewSel;
	if (pNMListView->uNewState & LVNI_SELECTED)
		nNewSel = pNMListView->iItem;
	else
		return;
	if ((nNewSel < 0) || (nNewSel >= m_dirList.GetSize()))
		return;
	if (m_marksetBrowser.GetSelectedCount() > 1)
		return;

	if (m_dlgLayoutObjectControl.m_hWnd)
		if (m_dlgLayoutObjectControl.IsWindowVisible())
			m_dlgLayoutObjectControl.DataExchangeSave();

	if (m_dirList[nNewSel].m_nAttribute == CDirItem::IsFolder)
	{
		m_strCurrentMarkset = "";
		m_markList.RemoveAll();
		GetDlgItem(IDC_ASSIGN_MARKSET)->EnableWindow(FALSE);
		GetDlgItem(IDC_OPEN_MARKSET)->EnableWindow(FALSE);
	}
	else
	{
		m_strCurrentMarkset = m_dirList[nNewSel].m_strFileName;
		LoadMarkSet();
		GetDlgItem(IDC_ASSIGN_MARKSET)->EnableWindow(TRUE);
		GetDlgItem(IDC_OPEN_MARKSET)->EnableWindow(TRUE);
	}

	InitMarkList();

	if (m_dlgLayoutObjectControl.m_hWnd)
		m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CDlgMarkSet::OnKeydownMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;

	if (pLVKeyDow->wVKey == VK_DELETE)
		OnDelete();
	
	*pResult = 0;
}

void CDlgMarkSet::OnFolderUp() 
{
	CString strShortCurrentFolder, strShortInitialFolder;
	GetShortPathName(m_strCurrentFolder,		strShortCurrentFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortCurrentFolder.ReleaseBuffer();
	GetShortPathName(theApp.GetMarksetInitialFolder(m_bOpenAutoMarks), strShortInitialFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortInitialFolder.ReleaseBuffer();
	strShortCurrentFolder.MakeUpper();
	strShortInitialFolder.MakeUpper();
	if (strShortCurrentFolder == strShortInitialFolder)
		return;

	int nIndex = m_strCurrentFolder.ReverseFind(_T('\\'));	
	if (nIndex == -1)
		return;

	m_strCurrentFolder = m_strCurrentFolder.Left(nIndex);

	GetShortPathName(m_strCurrentFolder, strShortCurrentFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortCurrentFolder.ReleaseBuffer();
	strShortCurrentFolder.MakeUpper();
	if (strShortCurrentFolder == strShortInitialFolder)
		GetDlgItem(IDC_FOLDER_UP)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_FOLDER_UP)->EnableWindow(TRUE);

	InitMarksetBrowser();
}

void CDlgMarkSet::OnDelete() 
{
	BOOL bDoPrompt = TRUE;
	if ( ! m_marksetBrowser.GetSelectedCount())
		return;
	else
		if (m_marksetBrowser.GetSelectedCount() > 1)
		{
			if (AfxMessageBox(IDS_DELETE_SELECTIONS_WARNING, MB_YESNO) == IDNO)
			{
				m_marksetBrowser.SetFocus();
				return;
			}
			else
				bDoPrompt = FALSE;
		}

	CString strMsg;
	for (int i = 0; i < m_dirList.GetSize(); i++)
	{
		if (m_marksetBrowser.GetItemState(i, LVIS_SELECTED))
			if (m_dirList[i].m_nAttribute == CDirItem::IsMarkset)
			{
				if (bDoPrompt)
				{
					strMsg.Format(IDS_DELETE_MARKSET_WARNING, m_dirList[i].m_strFileName);
					if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
					{
						m_marksetBrowser.SetFocus();
						return;
					}
				}
				CString strFileToRemove;
				strFileToRemove.Format(_T("%s\\%s.mks"), m_strCurrentFolder, m_dirList[i].m_strFileName);
				DeleteFile(strFileToRemove);
			}
			else
				if (m_dirList[i].m_nAttribute == CDirItem::IsFolder)
				{
					if (bDoPrompt)
					{
						strMsg.Format(IDS_DELETE_MARKSET_FOLDER_WARNING, m_dirList[i].m_strFileName);
						if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
						{
							m_marksetBrowser.SetFocus();
							return;
						}
					}
					CString strFolderToRemove;
					strFolderToRemove.Format(_T("%s\\%s"), m_strCurrentFolder, m_dirList[i].m_strFileName);
					DeleteFolder(strFolderToRemove);
				}
	}

	InitMarksetBrowser();
}

void CDlgMarkSet::DeleteFolder(const CString& strFolderToRemove) 
{
	WIN32_FIND_DATA findData;
	BOOL			bFilesExist = FALSE;	

	CString strFilename = strFolderToRemove + _T("\\*.*");
	HANDLE  hfileSearch = FindFirstFile(strFilename, &findData);

	BOOL bHasSubDir = FALSE;
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		CString	strBuffer	= findData.cFileName;
		m_strCurrentMarkset = strBuffer.SpanExcluding(_T("."));
		bFilesExist			= TRUE;
		do 
		{
			strBuffer = findData.cFileName;
			if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
			{
				if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					bHasSubDir = TRUE;
				else 
					DeleteFile(strFolderToRemove + _T("\\") + strBuffer);
			}
		}
		while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);

	RemoveDirectory(strFolderToRemove);
	if (bHasSubDir)
		AfxMessageBox(IDS_DELETEFOLDER_HASSUBDIRS, MB_OK);
}

void CDlgMarkSet::OnFolderNew() 
{
	CString strNewFolderName, strFolderName, strFullpath;
	strNewFolderName.LoadString(IDS_NEW_FOLDER);
	strFolderName = strNewFolderName;
	strFullpath.Format(_T("%s\\%s"), m_strCurrentFolder, strFolderName);
	int i;
	for (i = 2; i < 100; i++)
	{
		CFileStatus fileStatus;
		if (CFile::GetStatus(strFullpath, fileStatus))	// file exists
			strFolderName.Format(_T("%s (%d)"), strNewFolderName, i);
		else
			break;
		strFullpath.Format(_T("%s\\%s"), m_strCurrentFolder, strFolderName);
	}
	if (i >= 100)	// max. 100 tries
		return;

	CreateDirectory(strFullpath, NULL);

	int nIndex = InitMarksetBrowser(strFolderName);
	if (nIndex >= 0)
	{
		m_marksetBrowser.SetFocus();
		m_marksetBrowser.EditLabel(nIndex);
	}
}

void CDlgMarkSet::OnMarksetNew() 
{
	CString strNewMarksetName, strMarksetName, strFullpath;
	strNewMarksetName.LoadString(IDS_NEW_MARKSET);
	strMarksetName = strNewMarksetName;
	strFullpath.Format(_T("%s\\%s.mks"), m_strCurrentFolder, strMarksetName);
	int i;
	for (i = 2; i < 100; i++)
	{
		CFileStatus fileStatus;
		if (CFile::GetStatus(strFullpath, fileStatus))	// file exists
			strMarksetName.Format(_T("%s (%d)"), strNewMarksetName, i);
		else
			break;
		strFullpath.Format(_T("%s\\%s.mks"), m_strCurrentFolder, strMarksetName);
	}
	if (i >= 100)	// max. 100 tries
		return;

	m_strCurrentMarkset = strMarksetName;
	m_markList.RemoveAll();
	StoreMarkSet();		// store new markset

	int nIndex = InitMarksetBrowser(m_strCurrentMarkset);
	if (nIndex >= 0)
	{
		m_marksetBrowser.SetFocus();
		m_marksetBrowser.EditLabel(nIndex);
	}
}

void CDlgMarkSet::OnMarksetDuplicate() 
{
	CString strOldName = m_strCurrentMarkset;
	AfxFormatString1(m_strCurrentMarkset, IDS_COPY_MARKSET, strOldName);

	StoreMarkSet();			// store duplicate

	int nIndex = InitMarksetBrowser(m_strCurrentMarkset);
	if (nIndex >= 0)
	{
		m_marksetBrowser.SetFocus();
		m_marksetBrowser.EditLabel(nIndex);
	}
}

void CDlgMarkSet::OnSetfocusMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if ( ! m_strCurrentMarkset.IsEmpty())
		GetDlgItem(IDC_DELETE)->EnableWindow(TRUE);

	*pResult = 0;
}

void CDlgMarkSet::OnSetfocusMarkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	GetDlgItem(IDC_DELETE)->EnableWindow(FALSE);

	GetDlgItem(IDC_ASSIGN_SELECTEDMARK)->EnableWindow((m_markListCtrl.GetItemCount() > 0) ? TRUE : FALSE);
	
	*pResult = 0;
}

void CDlgMarkSet::OnNMKillfocusMarkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;
	GetDlgItem(IDC_ASSIGN_SELECTEDMARK)->EnableWindow((m_markListCtrl.GetItemCount() > 0) ? TRUE : FALSE);
}

void CDlgMarkSet::OnOpenMarkset() 
{
	SetEditMode();
	if ( ! m_hWnd)
		m_dlgLayoutObjectControl.Initialize(IDC_MARKPROPS_FRAME,   GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable, this);
	else
		m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void CDlgMarkSet::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized )
{
	if ( (nState == WA_ACTIVE) || (nState == WA_CLICKACTIVE) )
		m_dlgLayoutObjectControl.ObjectPreviewWindow_LoadData();
}

void CDlgMarkSet::OnUpdateSubDialogs()
{
	m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
	//m_dlgObjectPreviewControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
}

void CDlgMarkSet::OnUpdateMarkname()
{
	CMark* pMark = GetCurrentMark();
	if ( ! pMark)
		return;

	pMark->m_markTemplate.m_strPageID = m_markList.FindFreeMarkName(pMark->m_markTemplate.m_strPageID);
	pMark->m_strMarkName = pMark->m_markTemplate.m_strPageID;
	m_markListCtrl.SetItem(m_nCurMarkSel, 0, LVIF_TEXT, (LPCTSTR)pMark->m_markTemplate.m_strPageID, 0, 0, 0, 0L);

	m_MarkSource.m_strDocumentTitle = pMark->m_markTemplate.m_strPageID;
	m_dlgLayoutObjectControl.m_pMark->m_strMarkName	= pMark->m_markTemplate.m_strPageID;
	InitMarkList(GetCurrentMark());
}

CMark* CDlgMarkSet::GetCurrentMark()
{
	return ((m_nCurMarkSel >= 0) && (m_nCurMarkSel < m_markList.GetSize()) ) ? m_markList[m_nCurMarkSel] : NULL;
}

CPrintSheet* CDlgMarkSet::GetPrintSheet()
{
	CImpManDoc*	 pDoc		 = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	CPrintSheet* pPrintSheet = GetActPrintSheet(m_pActPrintSheet);
	if ( ! pPrintSheet)
		pPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
	return pPrintSheet;
}

BOOL CDlgMarkSet::MarkListChanged()
{
	CMarkList tmpMarkList;
	CString   strFilename;
	strFilename.Format(_T("%s\\%s.mks"), m_strCurrentFolder, m_strCurrentMarkset);

	LoadMarkSet(strFilename, &tmpMarkList);
	tmpMarkList.Sort();

	if (m_markList == tmpMarkList)
		return FALSE;
	else
		return TRUE;
}

BOOL CDlgMarkSet::LoadMarkSet(CString strTmp, CMarkList* pTmpMarkList)
{
	CFile			markDefFile;
	CFileException  fileException;

	CString strFilename;
	if (strTmp.IsEmpty())
		strFilename.Format(_T("%s\\%s.mks"), m_strCurrentFolder, m_strCurrentMarkset);
	else
		strFilename = strTmp;

	if (!markDefFile.Open(strFilename, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n",
				strFilename, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&markDefFile, CArchive::load);

		if ( ! pTmpMarkList)
		{
			m_markList.RemoveAll();		
			m_markList.Serialize(archive);
		}
		else
		{
			pTmpMarkList->RemoveAll(); 
			pTmpMarkList->Serialize(archive);
		}

		archive.Close();


		//if ( ! m_bOpenAutoMarks)		// if not auto mode, don't use rules
		//{
		//	for (int i = 0; i < m_markList.GetSize(); i++)
		//	{
		//		if ( ! m_markList[i]->m_rules.GetRuleExpression(0).IsEmpty())
		//			m_markList[i]->m_rules.SetRuleExpression(0, _T(""));
		//	}
		//}

		markDefFile.Close();

		if (strTmp.IsEmpty())
		{
			if ( ! m_markList.GetSize()) 
			{
				GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(FALSE);
				GetDlgItem(IDC_MARK_DELETE   )->EnableWindow(FALSE);
				return FALSE; 
			}
			else
			{
				GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(TRUE);
				GetDlgItem(IDC_MARK_DELETE   )->EnableWindow(TRUE);
				return TRUE;  	
			}
		}
	}
	return TRUE;
}

BOOL CDlgMarkSet::StoreMarkSet(BOOL bDoPrompt)
{
	if (m_strCurrentMarkset.IsEmpty())
		return FALSE;

	if (bDoPrompt)
	{
		if (MarkListChanged())	
		{
			int nRet = AfxMessageBox(IDS_SAVE_MARKSET_MODIFICATIONS_PROMPT, MB_YESNOCANCEL);
			switch (nRet)
			{
				case IDYES:		break;
				case IDNO:		LoadMarkSet(); 
								InitMarkList();
								m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
								//m_dlgObjectPreviewControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
								return TRUE;		// restore old states
				case IDCANCEL:	return FALSE;
			}
		}
	}

	CFile			markDefFile;
	CFileException	fileException;
	CString			strFilename;
	strFilename.Format(_T("%s\\%s.mks"), m_strCurrentFolder, m_strCurrentMarkset);

	if (!markDefFile.Open(strFilename, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFilename, fileException.m_cause);
		return TRUE; 
	}
	else
	{
		CArchive archive(&markDefFile, CArchive::store);
		m_markList.Serialize(archive);
		archive.Close();
	}
	markDefFile.Close();

	return TRUE;
}

void CDlgMarkSet::UpdateSheetPreview(BOOL bUpdateMarkOnly)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CSize  oldDocSize = pView->m_docSize;
	CPoint oldDocOrg  = pView->m_docOrg;
	pView->CalcDocExtents();
	pView->CalcViewportExtents();

	if (bUpdateMarkOnly && (oldDocSize == pView->m_docSize) && (oldDocOrg == pView->m_docOrg) )	// updatemarkonly only if docsize hasn't changed
	{
		pView->LockUpdateStatusBars();
		if (pView->m_bReflineUpdateEraseFront)
			pView->RedrawWindow(pView->m_reflineUpdateRectFront);
		else
			pView->RedrawWindow(pView->m_reflineUpdateRectFront, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
		if (pView->m_bReflineUpdateEraseBack)
			pView->RedrawWindow(pView->m_reflineUpdateRectBack);
		else
			pView->RedrawWindow(pView->m_reflineUpdateRectBack,  NULL, RDW_INVALIDATE | RDW_UPDATENOW);
		pView->UnlockUpdateStatusBars();
	}
	else
	{
		pView->CalcScrollSizes();	
		pView->SetScrollSizes(MM_TEXT, pView->m_sizeTotal, pView->m_sizePage, pView->m_sizeLine);

		pView->m_DisplayList.Invalidate();
		pView->Invalidate();
		pView->UpdateWindow();
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CDlgMarkSet::PreTranslateMessage(MSG* pMsg) 
{
	if (NULL != m_pToolTip)            
		m_pToolTip->RelayEvent(pMsg);	

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgMarkSet::OnMarkNew() 
{
	//CRect rect;
	//GetDlgItem(IDC_MARK_NEW)->GetWindowRect(rect);
	//m_dlgSelectNewMark.MoveWindow(rect.right, rect.top-50, 0, 0);
	//m_dlgSelectNewMark.ShowWindow(SW_SHOW);

    CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_NEW_MARK));
	m_pNewMarkPopupMenu = menu.GetSubMenu(0);
	if ( ! m_pNewMarkPopupMenu)
		return;

    MENUITEMINFO mi;
    mi.cbSize = sizeof(MENUITEMINFO);
    mi.fMask = MIIM_TYPE;
    mi.fType = MFT_OWNERDRAW;
//     Making the menu item ownerdrawn:
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_PDF,	   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_TEXT,	   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_SECTION,   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_CROP,	   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_FOLD,	   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_BARCODE,   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_RECTANGLE, &mi);

	CRect rcClient;
	GetDlgItem(IDC_MARK_NEW)->GetWindowRect(rcClient);

	m_pNewMarkPopupMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rcClient.TopLeft().x, rcClient.TopLeft().y, this);
	m_pNewMarkPopupMenu = NULL;
}

void CDlgMarkSet::OnNewMarkPDF()		{	AddNewMark(ID_NEWMARK_PDF);			}
void CDlgMarkSet::OnNewMarkText()		{	AddNewMark(ID_NEWMARK_TEXT);		}
void CDlgMarkSet::OnNewMarkSection()	{	AddNewMark(ID_NEWMARK_SECTION);		}
void CDlgMarkSet::OnNewMarkCrop()		{	AddNewMark(ID_NEWMARK_CROP);		}
void CDlgMarkSet::OnNewMarkFold()		{	AddNewMark(ID_NEWMARK_FOLD);		}
void CDlgMarkSet::OnNewMarkBarcode()	{	AddNewMark(ID_NEWMARK_BARCODE);		}
void CDlgMarkSet::OnNewMarkRectangle()	{	AddNewMark(ID_NEWMARK_RECTANGLE);	}


void CDlgMarkSet::AddNewMark(int nMarkType) 
{
	m_dlgLayoutObjectControl.DataExchangeSave();

	CMark*					pMark = NULL;
	switch (nMarkType)
	{
	case ID_NEWMARK_PDF:
	case ID_NEWMARK_TEXT:
		{
			if (nMarkType == ID_NEWMARK_PDF)
			{
				pMark = (CCustomMark*) new CCustomMark;
				((CCustomMark*)pMark)->Create(m_markList.m_colorDefTable, &m_markList);
			}
			else
			{
				pMark = (CTextMark*) new CTextMark;
				((CTextMark*)pMark)->Create(m_markList.m_colorDefTable, &m_markList);
			}
		}
		break;

	case ID_NEWMARK_SECTION:
		pMark = (CSectionMark*) new CSectionMark;
		((CSectionMark*)pMark)->Create(m_markList.m_colorDefTable, &m_markList);
		break;

	case ID_NEWMARK_CROP:
		pMark = (CCropMark*) new CCropMark;
		((CCropMark*)pMark)->Create(m_markList.m_colorDefTable, &m_markList);
		break;

	case ID_NEWMARK_FOLD:
		pMark = (CFoldMark*) new CFoldMark;
		((CFoldMark*)pMark)->Create(m_markList.m_colorDefTable, &m_markList);
		break;

	case ID_NEWMARK_BARCODE:
		pMark = (CBarcodeMark*) new CBarcodeMark;
		((CBarcodeMark*)pMark)->Create(m_markList.m_colorDefTable, &m_markList);
		break;

	case ID_NEWMARK_RECTANGLE:
		pMark = (CDrawingMark*) new CDrawingMark;
		((CDrawingMark*)pMark)->Create(m_markList.m_colorDefTable, &m_markList);
		break;
	}

	if ( ! pMark)
		return;

	m_markList.Add(pMark);
	InitMarkList(pMark);

	m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable); 
	//m_dlgObjectPreviewControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);

	UpdateSheetPreview();

#ifdef PDF
	if (nMarkType == ID_NEWMARK_PDF)
		m_dlgLayoutObjectControl.m_dlgCustomMarkContent.OnRegisterPDF();
	else
#endif
	{
		m_markListCtrl.SetFocus();
		m_markListCtrl.EditLabel(m_nCurMarkSel);
	}

	GetDlgItem(IDC_MARK_NEW		 )->EnableWindow(TRUE);
	GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(TRUE);
	GetDlgItem(IDC_MARK_DELETE   )->EnableWindow(TRUE);
}

void CDlgMarkSet::OnMarkDelete() 
{
	if ( ! m_markList.GetSize()) 
		return;

	if (AfxMessageBox(IDS_DELETE_SELECTIONS_WARNING, MB_YESNO) == IDNO)	
		return;

	int nIndex = m_markListCtrl.GetNextItem(-1, LVNI_SELECTED);
	if (nIndex == -1)
		return;

	for (int i = 0; i < m_markList.GetSize(); i++)
	{
		if (m_markListCtrl.GetItemState(i, LVIS_SELECTED))
		{
			m_markList.RemoveAt(i);
			m_markListCtrl.DeleteItem(i);
			i--;
		}
	}

	if (nIndex >= m_markListCtrl.GetItemCount())
		nIndex--;
	if (nIndex >= 0)
		m_markListCtrl.SetItemState(nIndex, LVIS_SELECTED, LVIS_SELECTED);

	m_nCurMarkSel = nIndex;

	m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
	//m_dlgObjectPreviewControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);

	if ( ! m_markList.GetSize())
	{
		GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(FALSE);
		GetDlgItem(IDC_MARK_DELETE   )->EnableWindow(FALSE);
	}
}

void CDlgMarkSet::OnMarkDuplicate() 
{
	DuplicateCurrentMark();//(TRUE);

	UpdateSheetPreview();
}

void CDlgMarkSet::DuplicateCurrentMark(BOOL bOffsetMark)
{
	if ((m_nCurMarkSel < 0) || (m_nCurMarkSel >= m_markList.GetSize()))
		return;

	switch (m_markList[m_nCurMarkSel]->m_nType)
	{
	case CMark::CustomMark:  
		{
			CCustomMark* pMark	 = new CCustomMark;
			*pMark			     = *(CCustomMark*)m_markList[m_nCurMarkSel];
			pMark->m_strMarkName = m_markList.FindFreeMarkName(pMark->m_strMarkName);
			pMark->m_markTemplate.m_strPageID	   = pMark->m_strMarkName;
			pMark->m_MarkSource.m_strDocumentTitle = pMark->m_strMarkName;
			if (bOffsetMark)
			{
				pMark->m_reflinePosParamsAnchor.m_fDistanceX += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsAnchor.m_fDistanceY += pMark->m_fMarkHeight * 2.0f;
				pMark->m_reflinePosParamsLL.m_fDistanceX	 += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsLL.m_fDistanceY	 += pMark->m_fMarkHeight * 2.0f;
				pMark->m_reflinePosParamsUR.m_fDistanceX	 += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsUR.m_fDistanceY	 += pMark->m_fMarkHeight * 2.0f;
			}
			m_markList.Add(pMark);	
			InitMarkList(pMark);
		}
		break;
	case CMark::TextMark:	 
		{
			CTextMark* pMark	 = new CTextMark;
			*pMark				 = *(CTextMark*)m_markList[m_nCurMarkSel];
			pMark->m_strMarkName = m_markList.FindFreeMarkName(pMark->m_strMarkName);
			pMark->m_markTemplate.m_strPageID	   = pMark->m_strMarkName;
			pMark->m_MarkSource.m_strDocumentTitle = pMark->m_strMarkName;
			if (bOffsetMark)
			{
				pMark->m_reflinePosParamsAnchor.m_fDistanceX += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsAnchor.m_fDistanceY += pMark->m_fMarkHeight * 2.0f;
				pMark->m_reflinePosParamsLL.m_fDistanceX	 += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsLL.m_fDistanceY	 += pMark->m_fMarkHeight * 2.0f;
				pMark->m_reflinePosParamsUR.m_fDistanceX	 += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsUR.m_fDistanceY	 += pMark->m_fMarkHeight * 2.0f;
			}
			m_markList.Add(pMark);	
			InitMarkList(pMark);
		}
		break;
	case CMark::BarcodeMark:	 
		{
			CBarcodeMark* pMark	 = new CBarcodeMark;
			*pMark				 = *(CBarcodeMark*)m_markList[m_nCurMarkSel];
			pMark->m_strMarkName = m_markList.FindFreeMarkName(pMark->m_strMarkName);
			pMark->m_markTemplate.m_strPageID	   = pMark->m_strMarkName;
			pMark->m_MarkSource.m_strDocumentTitle = pMark->m_strMarkName;
			if (bOffsetMark)
			{
				pMark->m_reflinePosParamsAnchor.m_fDistanceX += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsAnchor.m_fDistanceY += pMark->m_fMarkHeight * 2.0f;
				pMark->m_reflinePosParamsLL.m_fDistanceX	 += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsLL.m_fDistanceY	 += pMark->m_fMarkHeight * 2.0f;
				pMark->m_reflinePosParamsUR.m_fDistanceX	 += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsUR.m_fDistanceY	 += pMark->m_fMarkHeight * 2.0f;
			}
			m_markList.Add(pMark);	
			InitMarkList(pMark);
		}
		break;
	case CMark::DrawingMark:	 
		{
			CDrawingMark* pMark	 = new CDrawingMark;
			*pMark				 = *(CDrawingMark*)m_markList[m_nCurMarkSel];
			pMark->m_strMarkName = m_markList.FindFreeMarkName(pMark->m_strMarkName);
			pMark->m_markTemplate.m_strPageID	   = pMark->m_strMarkName;
			pMark->m_MarkSource.m_strDocumentTitle = pMark->m_strMarkName;
			if (bOffsetMark)
			{
				pMark->m_reflinePosParamsAnchor.m_fDistanceX += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsAnchor.m_fDistanceY += pMark->m_fMarkHeight * 2.0f;
				pMark->m_reflinePosParamsLL.m_fDistanceX	 += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsLL.m_fDistanceY	 += pMark->m_fMarkHeight * 2.0f;
				pMark->m_reflinePosParamsUR.m_fDistanceX	 += pMark->m_fMarkWidth  * 2.0f;
				pMark->m_reflinePosParamsUR.m_fDistanceY	 += pMark->m_fMarkHeight * 2.0f;
			}
			m_markList.Add(pMark);	
			InitMarkList(pMark);
		}
		break;
	case CMark::SectionMark:	// Duplicate section mark not allowed (makes no sense). Duplicate button is disabled
		break;
	case CMark::CropMark:		// Duplicate crop mark not allowed (makes no sense). Duplicate button is disabled
		break;
	case CMark::FoldMark:		// Duplicate fold mark not allowed (makes no sense). Duplicate button is disabled
		break;
	}
	m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
}

void CDlgMarkSet::OnMarkUp() 
{
	if ((m_nCurMarkSel <= 0) || (m_nCurMarkSel >= m_markList.GetSize()))
		return;

	m_markList[m_nCurMarkSel - 1]->m_nZOrderIndex++;
	m_markList[m_nCurMarkSel]->m_nZOrderIndex--;

	InitMarkList(m_markList[m_nCurMarkSel]);
	m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
}

void CDlgMarkSet::OnMarkDown() 
{
	if ((m_nCurMarkSel < 0) || (m_nCurMarkSel >= m_markList.GetSize() - 1))
		return;

	m_markList[m_nCurMarkSel]->m_nZOrderIndex++;
	m_markList[m_nCurMarkSel + 1]->m_nZOrderIndex--;

	InitMarkList(m_markList[m_nCurMarkSel]);
	m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
}

void CDlgMarkSet::OnItemchangedMarkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	
	*pResult = 0;

	int nNewSel;
	if (pNMListView->uNewState & LVNI_SELECTED)
		nNewSel = pNMListView->iItem;
	else
		return;
	if (nNewSel == m_nCurMarkSel)
		return;
	if ((nNewSel < 0) || (nNewSel >= m_markListCtrl.GetItemCount()))
		return;
	if ((m_nCurMarkSel < 0) || (m_nCurMarkSel >= m_markListCtrl.GetItemCount()))
		return;
	if (m_markListCtrl.GetSelectedCount() > 1)
		return;
	UpdateData(TRUE);

	m_dlgLayoutObjectControl.DataExchangeSave();
	m_nCurMarkSel = nNewSel;
	m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
	//m_dlgObjectPreviewControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CRect clientRect;
	pView->GetClientRect(clientRect);
    if ( (pView->GetTotalSize().cx > clientRect.Width()) || (pView->GetTotalSize().cy > clientRect.Height()) )
	{
		if ( (m_nCurMarkSel >= 0) && (m_nCurMarkSel < m_markList.GetSize()) )
		{
			CMark* pMark = m_markList[m_nCurMarkSel];
			if ( (pMark->m_nType != CMark::CropMark) && (pMark->m_nType != CMark::FoldMark) )
			{
				CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(pMark);
				if (pDI)
					if (pDI->m_bIsVisible != CDisplayItem::isCompleteVisible) 
					{
						pView->Invalidate();
						CPoint scrollPos = pView->GetScrollPosition();
						int nOffX = (clientRect.Width()  - pDI->m_BoundingRect.Width()) /2;
						int nOffY = (clientRect.Height() - pDI->m_BoundingRect.Height())/2;
						pView->ScrollToPosition(CPoint(scrollPos.x + pDI->m_BoundingRect.left - nOffX, scrollPos.y + pDI->m_BoundingRect.top - nOffY));
					}
			}
		}
	}

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CDlgMarkSet::OnClickMarkList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	if (m_markListCtrl.GetNextItem(-1, LVIS_SELECTED) == -1)	// clicked outside any item
		m_markListCtrl.SetItemState(m_nCurMarkSel, LVIS_SELECTED, LVIS_SELECTED);

	*pResult = 0;
}

void CDlgMarkSet::OnEndlabeleditMarkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	
	*pResult = 0;

	if ( (pDispInfo->item.iItem < 0) || (pDispInfo->item.iItem >= m_markList.GetSize()) )
		return;

	m_markListCtrl.GetEditControl()->GetWindowText(m_strMarkName);
	m_markList[pDispInfo->item.iItem]->m_strMarkName = m_strMarkName;
	m_markListCtrl.SetItemText(pDispInfo->item.iItem, 0, (LPCTSTR)m_strMarkName);

	InitMarkList(m_markList[pDispInfo->item.iItem]);

	m_MarkSource.m_strDocumentTitle = m_strMarkName;

	m_dlgLayoutObjectControl.m_pMark->m_strMarkName				 = m_strMarkName;
	m_dlgLayoutObjectControl.m_pMark->m_markTemplate.m_strPageID = m_strMarkName;
}

void CDlgMarkSet::OnDblclkMarkList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	*pResult = 0;

	if ( ! m_dlgLayoutObjectControl.IsWindowVisible())
		OnOpenMarkset();
}

void CDlgMarkSet::OnKeydownMarkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
	
	if (pLVKeyDow->wVKey == VK_DELETE)
		if (m_dlgLayoutObjectControl.IsWindowVisible())
			OnMarkDelete();

	*pResult = 0;
}

void CDlgMarkSet::InitMarkList(CMark* pMark, CString strMarkToFind)
{
	m_markListCtrl.DeleteAllItems();

	m_markList.Sort();

	int nZOrderIndex = 0;
	for (int i = 0; i < m_markList.GetSize(); i++)                        
		m_markList[i]->m_nZOrderIndex = nZOrderIndex++;

	CPrintSheet* pPrintSheet = GetPrintSheet();
	int			 nSide		 = GetActSight(NULL);
	for (int i = 0; i < m_markList.GetSize(); i++)                        
	{
		if ( ! m_markList[i])
			continue;

		LV_ITEM item;
		item.mask		= LVIF_TEXT | LVIF_IMAGE;
		item.cchTextMax = MAX_TEXT;
		item.iItem		= i; 
		item.iSubItem	= 0;
		item.pszText	= m_markList[i]->m_strMarkName.GetBuffer(MAX_TEXT);
		CMark* pCurMark	= m_markList[i];
		if (pCurMark)
		{
			switch (pCurMark->m_nType)
			{
			case CMark::CustomMark:		item.iImage	= 0;  break;
			case CMark::TextMark:		item.iImage	= 1;  break;
			case CMark::SectionMark:	item.iImage	= 2;  break;
			case CMark::CropMark:		item.iImage	= 3;  break;
			case CMark::FoldMark:		item.iImage	= 4;  break;
			case CMark::BarcodeMark:	item.iImage	= 5;  break;
			case CMark::DrawingMark:	item.iImage	= 6;  break;
			}
		}
		m_markListCtrl.InsertItem(&item);

		if ( (pMark == m_markList[i]) || (strMarkToFind == m_markList[i]->m_strMarkName) )
			m_nCurMarkSel = i;
	}

	if ( ! m_markList.GetSize())
		m_nCurMarkSel = -1;
	else
		if ( ! pMark && strMarkToFind.IsEmpty() )		// if no mark -> keep current selection if inside valid range
			if ( (m_nCurMarkSel < 0) || (m_nCurMarkSel >= m_markList.GetSize()) )
				m_nCurMarkSel = 0;

	if ( (m_nCurMarkSel >= 0) && (m_nCurMarkSel < m_markList.GetSize()) )
		m_markListCtrl.SetItemState(m_nCurMarkSel, LVIS_SELECTED, LVIS_SELECTED);

	m_markListCtrl.EnsureVisible(m_nCurMarkSel, FALSE);

	GetDlgItem(IDC_MARKSET_HEADER)->SetWindowText(m_strCurrentMarkset);

	CString strMarksetFullPath;
	if (m_strCurrentMarkset.IsEmpty())
		strMarksetFullPath = m_strCurrentFolder;
	else
		strMarksetFullPath.Format(_T("%s\\%s.mks"), m_strCurrentFolder, m_strCurrentMarkset);
	//SetWindowText(strMarksetFullPath);
	CString strTitle;
	strTitle.LoadString((m_bOpenAutoMarks) ? IDS_AUTO_MARKS : IDS_MANUAL_MARKS);
	strTitle += _T("  - ") + m_strCurrentFolder;
	SetWindowText(strTitle);

	m_strMarksetFullPath = strMarksetFullPath;

	UpdateEnableStates();
}

//void CMarkList::InsertMark(CMark* pMark)
//{
//}

void CDlgMarkSet::UpdateEnableStates()
{
	//if ( ! m_bOpenAutoMarks)
	//	return;

	CPrintSheet* pPrintSheet = GetPrintSheet();
	int			 nSide		 = GetActSight(NULL);

	for (int i = 0; i < m_markList.GetSize(); i++)                        
	{
		if ( ! m_markList[i])
			continue;

		LV_ITEM item;
		item.mask		= LVIF_IMAGE;
		item.iItem		= i; 
		item.iSubItem	= 0;
		m_markListCtrl.GetItem(&item);
		
		CMark* pMark = m_markList[i];
		switch (pMark->m_nType)
		{
		case CMark::CustomMark:		item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 0 :  7;  break;
		case CMark::TextMark:		item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 1 :  8;  break;
		case CMark::SectionMark:	item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 2 :  9;  break;
		case CMark::CropMark:		item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 3 : 10;  break;
		case CMark::FoldMark:		item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 4 : 11;  break;
		case CMark::BarcodeMark:	item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 5 : 12;  break;
		case CMark::DrawingMark:	item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 6 : 13;  break;
		}
		m_markListCtrl.SetItem(&item);
	}
}

void CDlgMarkSet::OnAssignMarkset() 
{
	if (m_bRunModal)
		return;	

	if (m_dlgLayoutObjectControl.IsWindowVisible())
		m_dlgLayoutObjectControl.DataExchangeSave();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	
	CPrintSheet* pPrintSheet = GetPrintSheet();
	int			 nSide		 = GetActSight(NULL);

	CString strLayout;
	for (int i = 0; i < m_markList.GetSize(); i++)
	{
		//if ( ! m_markList[i]->IsEnabled(pPrintSheet, nSide))
		//	continue;

		if ( (m_markList[i]->m_nType == CMark::TextMark) || (m_markList[i]->m_nType == CMark::BarcodeMark) )
			if ( ! CheckTextmarkPlausibility(m_markList[i], strLayout, m_pActPrintSheet))
			{
				m_markListCtrl.SetItemState(m_nCurMarkSel, 0, LVIS_SELECTED);
				m_nCurMarkSel = i;
				m_markListCtrl.SetItemState(m_nCurMarkSel, LVIS_SELECTED, LVIS_SELECTED);
				m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);

				CString strMsg;
				strMsg.Format(IDS_TEXTMARK_UNPLAUSIBLE, strLayout);
				AfxMessageBox(strMsg, MB_OK);

				if (m_dlgLayoutObjectControl.m_dlgTextMarkContent.m_hWnd)
					if (m_dlgLayoutObjectControl.m_dlgTextMarkContent.GetDlgItem(IDC_TEXTMARKSTRING))
					{
						m_dlgLayoutObjectControl.m_dlgTextMarkContent.GetDlgItem(IDC_TEXTMARKSTRING)->SetFocus();
						((CEdit*)m_dlgLayoutObjectControl.m_dlgTextMarkContent.GetDlgItem(IDC_TEXTMARKSTRING))->SetSel(0, -1);
					}

				return;
			}
	}	

	for (int i = 0; i < m_markList.GetSize(); i++)
	{
		//if ( ! m_markList[i]->IsEnabled(pPrintSheet, nSide))
		//	continue;

		if ( ! CheckMarkWidthHeight(m_markList[i]))
		{
			m_markListCtrl.SetItemState(m_nCurMarkSel, 0, LVIS_SELECTED);
			m_nCurMarkSel = i;
			m_markListCtrl.SetItemState(m_nCurMarkSel, LVIS_SELECTED, LVIS_SELECTED);
			m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);

			AfxMessageBox(IDS_MARK_DIMS_UNPLAUSIBLE, MB_OK);

			return;
		}
	}	

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	for (int i = 0; i < m_markList.GetSize(); i++)
		if (m_markList[i]->IsEnabled(GetPrintSheet(), BOTHSIDES))
		{
			PositionAndCreateMarkAuto(m_markList[i], &m_markList, m_strMarksetFullPath, m_pActPrintSheet);
		}

	pDoc->SetModifiedFlag();	// DoAutoMarks is needed, because otherwise new mark will be added at the end of the auto marks list
	pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();	// in order to ClipToObjects() for Crop and FoldMarks regarding to CustomMarks
	//pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
	//pDoc->m_PrintSheetList.ReorganizeColorInfos();

	CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pMarksView)
	{
		pMarksView->InitObjectList();
		pMarksView->m_dlgLayoutObjectControl.ReinitDialog(pMarksView->GetCurrentLayoutObject(), pMarksView->GetPrintSheet());
		UpdateSheetPreview();
	}
	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pProdDataPrintView)
	{
		pProdDataPrintView->InitObjectList();
		if (pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_hWnd)
			pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.ReinitDialog(pProdDataPrintView->GetCurrentMark(), pProdDataPrintView->GetActPrintSheet(), &pProdDataPrintView->m_objectList.m_colorDefTable);
		UpdateSheetPreview();
	}
}

void CDlgMarkSet::OnAssignSelectedmark() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheet* pPrintSheet = GetPrintSheet();
	int			 nSide		 = GetActSight(NULL);

	for (int i = 0; i < m_markList.GetSize(); i++)
	{
		//if ( ! m_markList[i]->IsEnabled(pPrintSheet, nSide))
		//	continue;

		if (m_markListCtrl.GetItemState(i, LVIS_SELECTED))
		{
			if ( (m_markList[i]->m_nType == CMark::TextMark) || (m_markList[i]->m_nType == CMark::BarcodeMark) )
			{
				CString strLayout;
				if ( ! CDlgMarkSet::CheckTextmarkPlausibility(m_markList[i], strLayout, m_pActPrintSheet))
				{
					m_markListCtrl.SetItemState(m_nCurMarkSel, 0, LVIS_SELECTED);
					m_nCurMarkSel = i;
					m_markListCtrl.SetItemState(m_nCurMarkSel, LVIS_SELECTED, LVIS_SELECTED);
					m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);

					CString strMsg;
					strMsg.Format(IDS_TEXTMARK_UNPLAUSIBLE, strLayout);
					AfxMessageBox(strMsg, MB_OK);

					if (m_dlgLayoutObjectControl.m_dlgTextMarkContent.m_hWnd)
						if (m_dlgLayoutObjectControl.m_dlgTextMarkContent.GetDlgItem(IDC_TEXTMARKSTRING))
						{
							m_dlgLayoutObjectControl.m_dlgTextMarkContent.GetDlgItem(IDC_TEXTMARKSTRING)->SetFocus();
							((CEdit*)m_dlgLayoutObjectControl.m_dlgTextMarkContent.GetDlgItem(IDC_TEXTMARKSTRING))->SetSel(0, -1);
						}

					continue;
				}
			}

			if ( ! CheckMarkWidthHeight(m_markList[i]))
			{
				m_markListCtrl.SetItemState(m_nCurMarkSel, 0, LVIS_SELECTED);
				m_nCurMarkSel = i;
				m_markListCtrl.SetItemState(m_nCurMarkSel, LVIS_SELECTED, LVIS_SELECTED);
				m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);

				AfxMessageBox(IDS_MARK_DIMS_UNPLAUSIBLE, MB_OK);

				return;
			}

			CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
			if (pView)
				pView->UndoSave();

			if (m_markList[i]->IsEnabled(GetPrintSheet(), BOTHSIDES))
				PositionAndCreateMarkAuto(m_markList[i], &m_markList, m_strMarksetFullPath, m_pActPrintSheet);
		}
	}

	pDoc->SetModifiedFlag();	// DoAutoMarks is needed, because otherwise new mark will be added at the end of the auto marks list
	pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();	
	//pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
	//pDoc->m_PrintSheetList.ReorganizeColorInfos();

	CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pMarksView)
	{
		pMarksView->InitObjectList();
		pMarksView->m_dlgLayoutObjectControl.ReinitDialog(pMarksView->GetCurrentLayoutObject(), pMarksView->GetPrintSheet());
		UpdateSheetPreview();
	}
	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pProdDataPrintView)
	{
		pProdDataPrintView->InitObjectList();
		if (pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_hWnd)
			pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.ReinitDialog(pProdDataPrintView->GetCurrentMark(), pProdDataPrintView->GetActPrintSheet(), &pProdDataPrintView->m_objectList.m_colorDefTable);
		UpdateSheetPreview();
	}
}

BOOL CDlgMarkSet::CheckTextmarkPlausibility(CMark* pMark, CString& strLayout, CPrintSheet* pActPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	TCHAR* pszTextString	= NULL;
	if (pMark->m_nType == CMark::TextMark)
		pszTextString = ((CTextMark*)pMark)->m_strOutputText.GetBuffer(((CTextMark*)pMark)->m_strOutputText.GetLength() + 1);
	else
		if (pMark->m_nType == CMark::BarcodeMark)
			pszTextString = ((CBarcodeMark*)pMark)->m_strOutputText.GetBuffer(((CBarcodeMark*)pMark)->m_strOutputText.GetLength() + 1);
		else
			return TRUE;

	if (((CReflineMark*)pMark)->m_reflinePosParamsAnchor.m_nAnchor != REFLINE_ANCHOR)
		return TRUE;

	if (GetActSight(pActPrintSheet) == ALLSIDES)
	{
		POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
		while (pos)
		{
			CLayout& rLayout		= pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
			int		 nNumFoldSheets = rLayout.GetFirstPrintSheet()->m_FoldSheetLayerRefs.GetSize();
			strLayout = rLayout.m_strLayoutName;
			if ( ! CheckTextmarkLayerRef(pszTextString, _T("$1"), nNumFoldSheets))
				return FALSE;
			if ( ! CheckTextmarkLayerRef(pszTextString, _T("$4"), nNumFoldSheets))
				return FALSE;
			if ( ! CheckTextmarkLayerRef(pszTextString, _T("$6"), nNumFoldSheets))
				return FALSE;
			if ( ! CheckTextmarkLayerRef(pszTextString, _T("$P"), nNumFoldSheets))
				return FALSE;
		}
	}
	else
	{
		if (GetActPrintSheet(pActPrintSheet))
		{
			int	nNumFoldSheets = GetActPrintSheet(pActPrintSheet)->m_FoldSheetLayerRefs.GetSize();
			if (GetActPrintSheet(pActPrintSheet)->GetLayout())
				strLayout = GetActPrintSheet(pActPrintSheet)->GetLayout()->m_strLayoutName;
			if ( ! CheckTextmarkLayerRef(pszTextString, _T("$1"), nNumFoldSheets))
				return FALSE;
			if ( ! CheckTextmarkLayerRef(pszTextString, _T("$4"), nNumFoldSheets))
				return FALSE;
			if ( ! CheckTextmarkLayerRef(pszTextString, _T("$6"), nNumFoldSheets))
				return FALSE;
			if ( ! CheckTextmarkLayerRef(pszTextString, _T("$P"), nNumFoldSheets))
				return FALSE;
		}
	}

	return TRUE;
}


BOOL CDlgMarkSet::CheckMarkWidthHeight(CMark* pMark)
{
	switch (pMark->m_nType)
	{
	case CMark::CustomMark:
	case CMark::TextMark:		
	case CMark::BarcodeMark:	
	case CMark::DrawingMark:	if ( (pMark->m_fMarkWidth == 0.0f) || (pMark->m_fMarkHeight == 0.0f) )
									return FALSE;
								break;

	case CMark::SectionMark:	if ( (((CSectionMark*)pMark)->m_fBarWidth == 0.0f) || (((CSectionMark*)pMark)->m_fBarHeight == 0.0f) )
									return FALSE;
								break;

	case CMark::CropMark:		if ( (((CCropMark*)pMark)->m_fLineWidth == 0.0f) || (((CCropMark*)pMark)->m_fLength == 0.0f) )
									return FALSE;
								break;

	case CMark::FoldMark:		if ( (((CFoldMark*)pMark)->m_fLineWidth == 0.0f) || (((CFoldMark*)pMark)->m_fLineLength == 0.0f) )
									return FALSE;
								break;
	}

	return TRUE;
}


BOOL CDlgMarkSet::CheckTextmarkLayerRef(LPCTSTR pszTextString, LPCTSTR pszPlaceholder, int nNumFoldSheets)
{
	const TCHAR* pPoi = _tcsstr(pszTextString, pszPlaceholder);
	while (pPoi)
	{
		if (nNumFoldSheets > 1)
		{
			if (*(pPoi + 2) != '_')
				return FALSE;

			TCHAR szNumString[3];
			szNumString[0] = (isdigit(*(pPoi + 3))) ? *(pPoi + 3) : '\0';
			szNumString[1] = (isdigit(*(pPoi + 4))) ? *(pPoi + 4) : '\0';
			szNumString[2] = '\0';
			if (_tcslen(szNumString))
			{
				int nComponentRefIndex = _ttoi(szNumString) - 1;
				if ((nComponentRefIndex < 0) || (nComponentRefIndex >= nNumFoldSheets))
					return FALSE;
			}
			else
				return FALSE;

		}
		pszTextString = pPoi + 2;
		pPoi		  = _tcsstr(pszTextString, pszPlaceholder);
	}

	return TRUE;
}


BOOL CDlgMarkSet::PositionAndCreateMarkAuto(CMark *pMark, CMarkList* pMarkList, CString strMarkSetFullPath, CPrintSheet* pActPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	if (pMark->m_nStatus == CMark::Inactive)
		return TRUE;
	
	CColorDefinitionTable& rColorDefTable = (pMarkList) ? pMarkList->m_colorDefTable : theApp.m_colorDefTable;

// Add mark source, if not existent
	BOOL  bMarkSourceAdded = FALSE;
	short nMarkSourceIndex = pDoc->m_MarkSourceList.FindMarkSourceIndex(pMark);
	if (nMarkSourceIndex == -2)		// invalid mark source
		return TRUE;
	if (nMarkSourceIndex == -1)
	{
		pDoc->m_MarkSourceList.AddTail(pMark->m_MarkSource);
		nMarkSourceIndex = (short)(pDoc->m_MarkSourceList.GetCount() - 1);
		bMarkSourceAdded = TRUE;
	}

// Create template
	BOOL		  bMarkTemplateAdded = FALSE;
	CPageTemplate markTemplateToAdd  = pMark->TransformColorDefs(rColorDefTable, nMarkSourceIndex);

	CString strMsg;	
	CPageTemplate* pMarkTemplate = pDoc->m_MarkTemplateList.FindTemplate(pMark->m_strMarkName);
	if ( ! pMarkTemplate)
	{
		pDoc->m_MarkTemplateList.AddTail(markTemplateToAdd);
		pMarkTemplate = pDoc->m_MarkTemplateList.FindTemplate(pMark->m_strMarkName);
		bMarkTemplateAdded = TRUE;
	}
	else
	{
		if (markTemplateToAdd == *pMarkTemplate)
		{
			for (int i = 0; i < pMarkTemplate->m_ColorSeparations.GetSize(); i++)
				pMarkTemplate->m_ColorSeparations[i].m_nPageSourceIndex = nMarkSourceIndex;
		}
		else
		{
			//AfxFormatString1(strMsg, IDS_MARKTEMPLATE_TOADD_EXISTS_MSG, pMark->m_strMarkName);
			//AfxMessageBox(strMsg, MB_OK);
			//if (bMarkSourceAdded)
			//	if ( ! pDoc->m_MarkSourceList.IsEmpty())
			//		pDoc->m_MarkSourceList.RemoveTail();
			//return FALSE;
			markTemplateToAdd.m_strPageID = pMark->m_strMarkName = pMark->m_markTemplate.m_strPageID = pDoc->m_MarkTemplateList.FindFreeMarkName(pMark->m_strMarkName);
			pDoc->m_MarkTemplateList.AddTail(markTemplateToAdd);
			pMarkTemplate = pDoc->m_MarkTemplateList.FindTemplate(markTemplateToAdd.m_strPageID);
			bMarkTemplateAdded = TRUE;
		}
	}

	int nObjectsAddedOnFront = 0;
	int nObjectsAddedOnBack  = 0;
	if (GetActSight(pActPrintSheet) == ALLSIDES)
	{
		POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
		while (pos)
		{
			CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
			nObjectsAddedOnFront += AddObjectsAndExposures(pMark, FRONTSIDE, pMarkTemplate, rLayout.GetFirstPrintSheet(), &rLayout, pMarkList);
			nObjectsAddedOnBack  += AddObjectsAndExposures(pMark, BACKSIDE,  pMarkTemplate, rLayout.GetFirstPrintSheet(), &rLayout, pMarkList);
			if (nObjectsAddedOnFront || nObjectsAddedOnBack)
				if ( ! strMarkSetFullPath.IsEmpty())
					rLayout.m_strLastAssignedMarkset = strMarkSetFullPath;
		}
	}
	else
	{
		nObjectsAddedOnFront = AddObjectsAndExposures(pMark, FRONTSIDE, pMarkTemplate, GetActPrintSheet(pActPrintSheet), GetActLayout(pActPrintSheet), pMarkList);
		nObjectsAddedOnBack  = AddObjectsAndExposures(pMark, BACKSIDE,  pMarkTemplate, GetActPrintSheet(pActPrintSheet), GetActLayout(pActPrintSheet), pMarkList);
		if (nObjectsAddedOnFront || nObjectsAddedOnBack)
			if (GetActLayout(pActPrintSheet))
				if ( ! strMarkSetFullPath.IsEmpty())
					GetActLayout(pActPrintSheet)->m_strLastAssignedMarkset = strMarkSetFullPath;
	}

	if ( (nMarkSourceIndex >= 0) && (pMark->m_nType != CMark::CustomMark) ) 
	{
		POSITION pos = pDoc->m_MarkSourceList.FindIndex(nMarkSourceIndex);
		if (pos)
		{
			CPageSource& rMarkSource = pDoc->m_MarkSourceList.GetAt(pos);
			rMarkSource = pMark->m_MarkSource;	// if mark has variable size, update bbox here
		}
	}

	if ( ! nObjectsAddedOnFront && ! nObjectsAddedOnBack)
	{
		if (bMarkSourceAdded)
			if ( ! pDoc->m_MarkSourceList.IsEmpty())
				pDoc->m_MarkSourceList.RemoveTail();

		if (bMarkTemplateAdded)
			if ( ! pDoc->m_MarkTemplateList.IsEmpty())
				pDoc->m_MarkTemplateList.RemoveTail();
	}

	return TRUE;
}


int CDlgMarkSet::AddObjectsAndExposures(CMark *pMark2Add, int nSide, CPageTemplate* pMarkTemplate, CPrintSheet* pActPrintSheet, CLayout* pActLayout, CMarkList* pMarkList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pMark2Add || ! pMarkTemplate || ! pActPrintSheet || ! pActLayout)
		return 0;

	CLayoutSide* pLayoutSide = NULL;
	switch (nSide)
	{
	case FRONTSIDE: pLayoutSide = &(pActLayout->m_FrontSide);
					break;
	case BACKSIDE : pLayoutSide = &(pActLayout->m_BackSide);
					break;
	default		  : return 0;
	}

	//if ( ! pMark2Add->IsEnabled(pActPrintSheet, nSide))
	//	return 0;

// Create new layout object(s)
	int nObjectsAdded = pMark2Add->AddToSheet(pActPrintSheet, nSide, pMarkTemplate, pMarkList);

	if ( ! nObjectsAdded)
		return 0;

	CMark* pMark = pActLayout->m_markList.FindMark(pMark2Add->m_strMarkName);
	if ( ! pMark)
	{
		pActLayout->m_markList.AddMark(pMark2Add);
		if (pActLayout->m_markList.GetSize())
			pActLayout->m_markList[pActLayout->m_markList.GetSize() - 1]->m_markTemplate = pMark2Add->TransformColorDefs((pMarkList) ? pMarkList->m_colorDefTable : pDoc->m_ColorDefinitionTable/*theApp.m_colorDefTable*/, -1);
	}

// Add the exposure(s) to the plates
	for (short nObjectIndex = (short)(pLayoutSide->m_ObjectList.GetCount() - nObjectsAdded); nObjectIndex < pLayoutSide->m_ObjectList.GetCount(); nObjectIndex++)
	{
		short		nIndex		 = 0;
		CPrintSheet *pPrintSheet = pActLayout->GetFirstPrintSheet();
		while (pPrintSheet)
		{
			CPlateList* pPlateList = (nSide == FRONTSIDE) ? &pPrintSheet->m_FrontSidePlates : &pPrintSheet->m_BackSidePlates;
			POSITION platePos = pPlateList->GetHeadPosition();
			CPlate *pPlate;
			while (platePos)
			{
				pPlate = &(pPlateList->GetNext(platePos));

				for (short i = 0; i < pMarkTemplate->m_ColorSeparations.GetSize(); i++)
				{
					SEPARATIONINFO sepInfo = pMarkTemplate->m_ColorSeparations[i];
					if (pPlate->m_nColorDefinitionIndex == sepInfo.m_nColorDefinitionIndex)
					{
						CExposure newExposure;
						newExposure.Create(&pPlateList->m_defaultPlate, nObjectIndex);
						pPlate->m_ExposureSequence.AddTail(newExposure);
						break;
					}
				}

				CPrintSheetLocation loc;
				loc.m_nPrintSheetIndex	 = nIndex;
				loc.m_nLayoutSide		 = (BYTE)nSide;
				loc.m_nLayoutObjectIndex = nObjectIndex;
				pMarkTemplate->m_PrintSheetLocations.Add(loc);
			}

			//// do the same with default plate
			pPlate = &pPlateList->m_defaultPlate;
			for (short i = 0; i < pMarkTemplate->m_ColorSeparations.GetSize(); i++)
			{
				SEPARATIONINFO sepInfo = pMarkTemplate->m_ColorSeparations[i];
				if (pPlate->m_nColorDefinitionIndex == sepInfo.m_nColorDefinitionIndex)
				{
					CExposure newExposure;
					newExposure.Create(&pPlateList->m_defaultPlate, nObjectIndex);
					pPlate->m_ExposureSequence.AddTail(newExposure);
					break;
				}
			}

			pPrintSheet = pActLayout->GetNextPrintSheet(pPrintSheet);
			nIndex++;
		}
	}

	//if (nObjectsAdded > 0)
	//{
	//	CString strMsg;	
	//	CMark* pMark = pActLayout->m_markList.FindMark(pMark2Add->m_strMarkName);
	//	if ( ! pMark)
	//	{
	//		pActLayout->m_markList.AddMark(pMark2Add);
	//	}
		//else
		//{
		//	if (markTemplateToAdd == *pMarkTemplate)
		//	{
		//		for (int i = 0; i < pMarkTemplate->m_ColorSeparations.GetSize(); i++)
		//			pMarkTemplate->m_ColorSeparations[i].m_nPageSourceIndex = nMarkSourceIndex;
		//	}
		//}
	//}

	return nObjectsAdded;
}

void CDlgMarkSet::OnApplyButton() 
{
	if (m_dlgLayoutObjectControl.IsWindowVisible())
	{
		m_dlgLayoutObjectControl.Apply();
		m_dlgLayoutObjectControl.UpdateView();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
	}
	else
		if (m_marksetBrowser.GetSelectedCount() == 1)
		{
			LRESULT		lResult;
			NM_LISTVIEW nMListView;
			nMListView.iItem = m_marksetBrowser.GetNextItem(-1, LVIS_SELECTED);
			OnDblclkMarksetBrowser((NMHDR*)&nMListView, &lResult);
		}
}

void CDlgMarkSet::OnCloseMarksetDlg()	// close completely
{
	if (m_bOpenAutoMarks)
	{
		CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
		if (pProdDataPrintView)
		{
			pProdDataPrintView->m_strAutoMarksFolder = m_strCurrentFolder;
			pProdDataPrintView->SaveAutoMarksState();
			pProdDataPrintView->LoadAutoMarksState();
			CImpManDoc* pDoc = CImpManDoc::GetDoc();	
			if (pDoc)
				pDoc->m_PrintSheetList.DoAutoMarks();
		}
	}

	CDialog::OnOK();
	if ( ! m_bRunModal)
		DestroyWindow();

	UpdateSheetPreview();
}

void CDlgMarkSet::OnOK()			// close and go back to browser
{
	if (m_dlgLayoutObjectControl.IsWindowVisible())
	{
		m_dlgLayoutObjectControl.DataExchangeSave();
		StoreMarkSet();
		SetBrowserMode();
		CImpManDoc* pDoc = CImpManDoc::GetDoc();	
		if (pDoc)
			pDoc->m_PrintSheetList.DoAutoMarks();
	}

	UpdateSheetPreview();
}

void CDlgMarkSet::OnCancel()		
{
	if (m_dlgLayoutObjectControl.IsWindowVisible())		// cancel and go back to browser
	{
		LoadMarkSet();
		InitMarkList();
		m_dlgLayoutObjectControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
		//m_dlgObjectPreviewControl.ReinitDialog(GetCurrentMark(), GetPrintSheet(), &m_markList.m_colorDefTable);
		SetBrowserMode();
	}
	else
	{
		m_pWnd = NULL;
		if ( ! m_bRunModal)
			DestroyWindow();		// close completely
		else
			CDialog::OnCancel();
	}

	UpdateSheetPreview();
}

void CDlgMarkSet::OnClose()			// close completely
{
	if (m_pWnd)
		m_pWnd->SetWindowText(m_strCurrentMarkset);

	if (m_dlgLayoutObjectControl.IsWindowVisible())
	{
		m_dlgLayoutObjectControl.DataExchangeSave();
		if ( ! StoreMarkSet(TRUE))
			return;
	}

	m_pWnd = NULL;
	if ( ! m_bRunModal)
		DestroyWindow();	
	else
		CDialog::OnCancel();
	UpdateSheetPreview();
}

void CDlgMarkSet::OnStnClickedMarkpropsFrame()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

CPrintSheet* CDlgMarkSet::GetActPrintSheet(CPrintSheet* pActPrintSheet)
{
	if (pActPrintSheet)
		return pActPrintSheet;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	return pDoc->GetActPrintSheet();
}

CLayout* CDlgMarkSet::GetActLayout(CPrintSheet* pActPrintSheet)
{
	if (pActPrintSheet)
		return pActPrintSheet->GetLayout();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	return pDoc->GetActLayout();
}

int CDlgMarkSet::GetActSight(CPrintSheet* pActPrintSheet)
{
	if (pActPrintSheet)
		return BOTHSIDES;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return BOTHSIDES;
	return pDoc->GetActSight();
}
