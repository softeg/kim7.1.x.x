// DlgSelectPrintLayout.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgSelectPrintLayout.h"


// CDlgSelectPrintLayout-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgSelectPrintLayout, CDialog)

CDlgSelectPrintLayout::CDlgSelectPrintLayout(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSelectPrintLayout::IDD, pParent)
{
	m_smallFont.CreateFont (13, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_pPrintLayoutImageList = new CImageList();

	m_nSelection = -1;
}

CDlgSelectPrintLayout::~CDlgSelectPrintLayout()
{
	for (int i = 0; i < m_sampleList.GetSize(); i++)
	{
		if (m_sampleList[i].pPrintSheet)
			delete m_sampleList[i].pPrintSheet;
		if (m_sampleList[i].pLayout)
			delete m_sampleList[i].pLayout;
	}

	m_smallFont.DeleteObject();
	delete m_pPrintLayoutImageList;
}

void CDlgSelectPrintLayout::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PRINTLAYOUTLIST, m_printLayoutList);
}


BEGIN_MESSAGE_MAP(CDlgSelectPrintLayout, CDialog)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_PRINTLAYOUTLIST, OnNMCustomdrawPrintLayoutlist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PRINTLAYOUTLIST, OnLvnItemchangedPrintLayoutlist)
	ON_BN_CLICKED(IDOK, &CDlgSelectPrintLayout::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgSelectPrintLayout-Meldungshandler

BOOL CDlgSelectPrintLayout::OnInitDialog()
{
	CDialog::OnInitDialog();

	CRect rcList;
	m_printLayoutList.GetClientRect(rcList);

	if ( ! m_pPrintLayoutImageList->m_hImageList)
	{
		CBitmap bitmap;
		m_pPrintLayoutImageList->Create(rcList.Width() - 50, 200, ILC_COLOR8, 1, 0);	// List not to grow
		bitmap.LoadBitmap(IDB_BKSCHEMELIST);
		m_pPrintLayoutImageList->Add(&bitmap, BLACK);
		m_printLayoutList.SetImageList(m_pPrintLayoutImageList, LVSIL_SMALL);
		bitmap.DeleteObject();
	}

	CString strOldTitle, strNewTitle;
	GetWindowText(strOldTitle);
	strNewTitle.Format(strOldTitle, CString(_T("\"") + m_strCurLayoutName + CString(_T("\""))));
	SetWindowText(strNewTitle);

	m_printLayoutList.InsertColumn(0, "",  LVCFMT_LEFT, rcList.Width() - 20, 0);

	InitPrintLayoutList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgSelectPrintLayout::AddPrintLayout(CPrintSheet& rPrintSheet, CLayout& rLayout)
{
	_PRINTLAYOUT pl1r  = {new (CPrintSheet), new (CLayout)};
	*pl1r.pPrintSheet  = rPrintSheet;
	*pl1r.pLayout	   = rLayout;
	pl1r.pLayout->m_FrontSide.RemoveMarkObjects(-1, TRUE);
	pl1r.pLayout->m_BackSide.RemoveMarkObjects(-1, TRUE);
	pl1r.pLayout->m_markList.RemoveAll(FALSE);
	m_sampleList.Add(pl1r);
}

void CDlgSelectPrintLayout::InitPrintLayoutList()
{
	m_printLayoutList.DeleteAllItems();

	LV_ITEM item;
	item.mask		= LVIF_PARAM | LVIF_IMAGE | LVIF_STATE;
	item.stateMask	= LVIS_SELECTED;
	item.state		= 0;
	item.cchTextMax = MAX_TEXT;
	item.iSubItem	= 0;
	item.lParam		= 0;//(LPARAM)&pl;
	item.iImage		= 0;

	for (int i = 0; i < m_sampleList.GetSize(); i++)
	{
		item.iItem = i; 
		m_printLayoutList.InsertItem(&item);
	}

	m_printLayoutList.EnsureVisible(0, FALSE);
}

void CDlgSelectPrintLayout::OnNMCustomdrawPrintLayoutlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);
			CPrintSheet* pPrintSheet = (lvitem.iItem < m_sampleList.GetSize()) ? m_sampleList[lvitem.iItem].pPrintSheet : NULL;
			CLayout*	 pLayout	 = (lvitem.iItem < m_sampleList.GetSize()) ? m_sampleList[lvitem.iItem].pLayout		: NULL;
			if ( ! pLayout || ! pPrintSheet)
				break;

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			float fPaperWidth  = pLayout->m_FrontSide.m_fPaperWidth;
			float fPaperHeight = pLayout->m_FrontSide.m_fPaperHeight;

			rcItem.DeflateRect(5, 5);
			CRect rcLayout = rcItem; rcLayout.DeflateRect(5, 5); rcLayout.right -= 50;
			rcLayout = pLayout->Draw(pDC, rcLayout, pPrintSheet, NULL, TRUE, FALSE, FALSE);//m_pFoldSheet);		

			CRect rcLabel;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			if (rcLabel.IsRectEmpty())
				break;

			rcLabel.left = rcLayout.right + 20;

			COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

			CFont* pOldFont = pDC->SelectObject(&m_smallFont);
			CRect rcText = rcLabel; rcText.left = rcLayout.right + 10; //rcText.top = rcText.CenterPoint().y - 20; rcText.bottom = rcText.top + 50;
			rcText.OffsetRect(0, 10);
			//pDC->DrawText(pLayout->m_strLayoutName, -1, rcText, DT_VCENTER | DT_LEFT | DT_END_ELLIPSIS);
			//rcText.OffsetRect(0, 15);

			int nTextHeight = 15;
			rcText.bottom = rcText.top + nTextHeight;
			pDC->SetTextColor(BLACK);
			CString strOut = pLayout->m_FrontSide.m_strFormatName;	//strOut.Format(_T("%.1f x %.1f"), pLayout->m_FrontSide.m_fPaperWidth, pLayout->m_FrontSide.m_fPaperHeight);
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
			rcText.OffsetRect(0, nTextHeight);

			if (pPrintSheet->m_nQuantityTotal > 0)
			{
				CString strOut2; strOut2.LoadString(IDS_SHEET_TEXT);
				strOut.Format(_T("%d  %s"), pPrintSheet->m_nQuantityTotal - pPrintSheet->m_nQuantityExtra, strOut2);
				pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
				int nRight = rcText.left + pDC->GetTextExtent(strOut).cx;
				CRect rcDI = rcText; rcDI.right = nRight;

				rcText.OffsetRect(0, nTextHeight);
				strOut.Format(_T("%.1f %%"), pLayout->CalcUtilization() * 100.0f);
				pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
			}

			pDC->SetTextColor(crOldTextColor);
			pDC->SelectObject(pOldFont);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}

void CDlgSelectPrintLayout::OnLvnItemchangedPrintLayoutlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	//if (m_bLocked)
	//	return;

    HWND hWndListCtrl = pNMHDR->hwndFrom;
	if (pNMLV->iItem < 0)
		return;
	if (pNMLV->uNewState == pNMLV->uOldState)
		return;
	if ( ! (pNMLV->uNewState & LVIS_SELECTED) )
		return;

	LVITEM lvitem;
	lvitem.mask	= LVIF_PARAM;
	lvitem.iItem = pNMLV->iItem;
	lvitem.iSubItem = 0;
	ListView_GetItem(hWndListCtrl, &lvitem);
	CPressDevice* pPressDevice = (CPressDevice*)lvitem.lParam;

	//m_bLocked = TRUE;
	//if (m_hWnd)
	//	if (m_bRunModal)
	//	{
	//		EndDialog(-lvitem.iItem);
	//	}
	//	else
	//		DestroyWindow();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	//CDlgSelectFoldSchemePopup* pParent = (CDlgSelectFoldSchemePopup*)GetParent();
	//if ( ! pParent)
	//	return;
	//CPrintSheetPlanningView* pPlanningView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));

	//CPrintSheet newPrintSheet	= *m_sampleList[lvitem.iItem].pPrintSheet;
	//CLayout		newLayout	    = *m_sampleList[lvitem.iItem].pLayout;
	//CFoldSheet  newFoldSheet	= *pParent->m_pFoldSheet;

	//CString strSequentially; strSequentially.LoadString(IDS_SEQUENTIALLY);
	//if (newLayout.m_strLayoutName.Find(strSequentially) >= 0)
	//{
	//	int nNumOnLayout   = newLayout.m_FoldSheetLayerRotation.GetSize();
	//	int nNumFoldSheets = pParent->m_pFoldSheet->GetNumSamePrintLayout();
	//	if (nNumOnLayout > nNumFoldSheets)
	//		pParent->m_pLayout->AddPrintSheetsBasedOn(nNumOnLayout - nNumFoldSheets);
	//}

	//newLayout.m_strLayoutName = (pParent->m_pLayout) ? pParent->m_pLayout->m_strLayoutName : _T("");
	//if (pParent->m_pLayout)
	//{
	//	newLayout.m_markList.RemoveAll();
	//	newLayout.m_markList.Copy(pParent->m_pLayout->m_markList);
	//}

	//CFoldSheet* pLastAddedFoldSheet = pDoc->m_PrintSheetList.ReImposePrintSheetType(pParent->m_pFoldSheet, &newFoldSheet, m_pPrintSheet, newPrintSheet, newLayout);

	//pDoc->SetModifiedFlag();										

	//if (pParent->m_pLayout)
	//	pParent->m_pLayout->AnalyzeMarks();

	//pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));	
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),	NULL, 0, NULL);
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	//theApp.UpdateView(RUNTIME_CLASS(CProductsView));	

	//if (pPlanningView)
	//{
	//	pPlanningView->SelectComponentByFoldSheet(pLastAddedFoldSheet);
	//	pParent->m_pFoldSheet = pPlanningView->GetFoldSheet();
	//}

	//pParent->InitData();
	//pParent->UpdateViews();
}

void CDlgSelectPrintLayout::OnBnClickedOk()
{
	UpdateData(TRUE);

	POSITION pos = m_printLayoutList.GetFirstSelectedItemPosition();
	if (pos)
		m_nSelection = m_printLayoutList.GetNextSelectedItem(pos);
	else
		m_nSelection = -1;

	OnOK();
}
