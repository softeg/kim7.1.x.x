// DlgNotes.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgNotes dialog

class CDlgNotes : public CDialog
{
// Construction
public:
	CDlgNotes(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgNotes)
	enum { IDD = IDD_NOTES };
	CString	m_strNotes;
	//}}AFX_DATA

public:
	CLayout* m_pLayout;
protected:
	BOOL m_NotesChanged;
	CFoldSheet* m_pFoldSheet;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgNotes)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgNotes)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnChangeNotesEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
