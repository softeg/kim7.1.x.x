// DlgCropMarkContent.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgCropMarkContent 


CDlgCropMarkContent::CDlgCropMarkContent(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgCropMarkContent::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgCropMarkContent)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	m_pParent = NULL;
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
}

CDlgCropMarkContent::~CDlgCropMarkContent()
{
	m_hollowBrush.DeleteObject();
}

BOOL CDlgCropMarkContent::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgCropMarkContent::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgCropMarkContent)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgCropMarkContent, CDialog)
	//{{AFX_MSG_MAP(CDlgCropMarkContent)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgCropMarkContent 

BOOL CDlgCropMarkContent::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	m_colorControl.Create(this, m_pParent);
	
	CRect controlRect, frameRect;
	m_colorControl.GetWindowRect(controlRect);
	GetDlgItem(IDC_COLORCONTROL_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	m_colorControl.SetWindowPos(NULL, frameRect.left, frameRect.top, controlRect.Width(), controlRect.Height(), SWP_NOZORDER);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgCropMarkContent::LoadData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::CropMark)
		return;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				if (pObjectSource)
				{
					int	nIndex = pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
					if (nIndex != -1)	// invalid string
					{
						CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
						CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
						strParams.TrimLeft(); strParams.TrimRight();
	/*
						if (strContentType == "$CROP_LINE")
							;*/
					}
					m_colorControl.LoadData(pTemplate, pObjectSource, nObjectSourceIndex);
				}
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex != -1)	// invalid string
		{
			CString strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);
			CString strParams	   = pMark->m_MarkSource.m_strSystemCreationInfo.Right(pMark->m_MarkSource.m_strSystemCreationInfo.GetLength() - nIndex);
			strParams.TrimLeft(); strParams.TrimRight();
		}

		m_colorControl.LoadData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
	}

	UpdateData(FALSE);
}

void CDlgCropMarkContent::SaveData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::CropMark)
		return;

	UpdateData(TRUE);

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				int			 nIndex				= pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
				if (nIndex != -1)	// invalid string
				{
					CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
					CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
					strParams.TrimLeft(); strParams.TrimRight();

					if (strContentType == "$CROP_LINE")
						pObjectSource->m_strSystemCreationInfo.Format(_T("$CROP_LINE %.2f %.2f"), 
						min(pLayoutObject->GetWidth(), pLayoutObject->GetHeight()), max(pLayoutObject->GetWidth(), pLayoutObject->GetHeight()));

					pObjectSource->m_nType = CPageSource::SystemCreated;

					m_colorControl.SaveData(pTemplate, pObjectSource, nObjectSourceIndex);
				}
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex != -1)	// invalid string
		{
			CString strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);
			CString strParams	   = pMark->m_MarkSource.m_strSystemCreationInfo.Right(pMark->m_MarkSource.m_strSystemCreationInfo.GetLength() - nIndex);
			strParams.TrimLeft(); strParams.TrimRight();

			CCropMark* pCropMark = (CCropMark*)pMark;
			pCropMark->m_MarkSource.m_nType = CPageSource::SystemCreated;

			if (strContentType == "$CROP_LINE")
				pCropMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$CROP_LINE %.2f %.2f"), pCropMark->m_fLineWidth, pCropMark->m_fLength);
		}

		m_colorControl.SaveData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
	}
}

void CDlgCropMarkContent::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}

void CDlgCropMarkContent::OnPaint() 
{
	CDialog::OnPaint();

	m_colorControl.Invalidate();
	m_colorControl.UpdateWindow();
}
