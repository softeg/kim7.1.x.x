#pragma once

#include "PaperDefsSelection.h"



// CDlgPaperDefs-Dialogfeld

class CDlgPaperDefs : public CDialog
{
	DECLARE_DYNAMIC(CDlgPaperDefs)

public:
	CDlgPaperDefs(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgPaperDefs();

// Dialogfelddaten
	enum { IDD = IDD_PAPER_DEFS };

public:
	CString				m_strPaperName;
	int					m_nPaperGrammage;
	float				m_fPaperVolume;
	CPaperDefsSelection	m_paperSelection;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangePdPapername();
	afx_msg void OnBnClickedPdSave();
	afx_msg void OnBnClickedPdDelete();
	afx_msg void OnBnClickedPdChange();
	virtual BOOL OnInitDialog();
	float m_fPageShingling;
	afx_msg void OnEnKillfocusPdPapergrammage();
	afx_msg void OnEnKillfocusPdPapervolume();
};
