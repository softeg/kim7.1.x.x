// PrintComponentsDetailView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintComponentsView.h"
#include "PrintComponentsDetailView.h"
#include "PrintSheetComponentsView.h"
#include "NewPrintSheetFrame.h"
#include "GraphicComponent.h"
#include "DlgFlatProductData.h"
#include "DlgBoundProduct.h"


// CPrintComponentsDetailView

IMPLEMENT_DYNCREATE(CPrintComponentsDetailView, CScrollView)

CPrintComponentsDetailView::CPrintComponentsDetailView()
{
    m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CPrintComponentsDetailView));
	m_DisplayList.SetClassName(_T("CPrintComponentsDetailView"));
}

CPrintComponentsDetailView::~CPrintComponentsDetailView()
{
}

void CPrintComponentsDetailView::DoDataExchange(CDataExchange* pDX)
{
	CScrollView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPrintComponentsDetailView, CScrollView)
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
END_MESSAGE_MAP()



// CPrintComponentsDetailView-Meldungshandler

BOOL CPrintComponentsDetailView::OnEraseBkgnd(CDC* pDC)
{
    CRect rcFrame;
    GetClientRect(rcFrame);
	pDC->FillSolidRect(rcFrame, RGB(227,232,238));

	return TRUE; 	//CScrollView::OnEraseBkgnd(pDC);
}

void CPrintComponentsDetailView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

}

void CPrintComponentsDetailView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	CScrollView::OnUpdate(pSender, lHint, pHint);

	SetScrollSizes(MM_TEXT, CSize(1,1));	// suppress scrollbars
}

void CPrintComponentsDetailView::ResizeView()
{
	CalcExtents();
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if (pFrame)
	{
		CRect rcFrame;
		pFrame->GetClientRect(rcFrame);
		int nHeight2 = rcFrame.Height() - 10;	
		int nHeight1 = m_docSize.cy;
		int nHeight0 = nHeight2 - nHeight1; 
		pFrame->m_wndSplitter5.SetRowInfo(0, (nHeight0 >= 0) ? nHeight0 : 0, 10);
		pFrame->m_wndSplitter5.SetRowInfo(1, (nHeight1 >= 0) ? nHeight1 : 0, 10);
		pFrame->m_wndSplitter5.RecalcLayout();
		Invalidate();
		UpdateWindow();
	}
}

CPrintSheet* CPrintComponentsDetailView::GetActPrintSheet()
{
	return CNewPrintSheetFrame::GetActPrintSheet();
}

CPrintComponent CPrintComponentsDetailView::GetActComponent()
{
	CPrintComponentsView* pView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));

	return (pView) ? pView->GetActComponent() : CPrintComponent();
}

CDlgComponentImposer* CPrintComponentsDetailView::GetDlgComponentImposer()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	return pFrame->GetDlgComponentImposer();
}

BOOL CPrintComponentsDetailView::IsOpenDlgComponentImposer()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	return pFrame->IsOpenDlgComponentImposer();
}

CSize CPrintComponentsDetailView::CalcExtents()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CRect rcBox = Draw(&dc, NULL);

	dc.DeleteDC();

	m_docSize = rcBox.Size();
	m_docSize.cy += 5;

	return m_docSize;
}

void CPrintComponentsDetailView::OnDraw(CDC* pDC)
{
	Draw(pDC, &m_DisplayList);
}

CRect CPrintComponentsDetailView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	CRect rcFrame;
	GetClientRect(&rcFrame);

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	rcFrame.DeflateRect(5, 5);

	if (pDisplayList)
	{
		pDisplayList->Invalidate();	// always full item registering - not time critical, otherwise invalidation needed when switching between front/back/both
		pDisplayList->BeginRegisterItems(pDC);
	}

	CPrintSheet*		pPrintSheet = GetActPrintSheet();
	CPrintComponent		component   = GetActComponent();
	if ( ! component.IsNull())
	{
		if (component.m_nType == CPrintComponent::Flat)
		{
			CFlatProduct* pFlatProduct = component.GetFlatProduct();
			if (pFlatProduct)
			{
				CRect rcRet = pFlatProduct->DrawSummary(pDC, rcFrame, pDisplayList, pPrintSheet);//	rcRet.right = rcFrame.right;
				rcBox.UnionRect(rcBox, rcRet); 
			}
		}
		else
			if (component.m_nType == CPrintComponent::PageSection)
			{
				CBoundProductPart* pProductPart = component.GetBoundProductPart();
				if (pProductPart)
				{
					CRect rcRet = pProductPart->DrawSummary(pDC, rcFrame, pPrintSheet, component.m_nNumPages, pDisplayList);//	rcRet.right = rcFrame.right;
					rcBox.UnionRect(rcBox, rcRet); 
				}
			}
			else
				if (component.m_nType == CPrintComponent::FoldSheet)
				{
					CFoldSheet* pFoldSheet = component.GetFoldSheet();
					if (pFoldSheet)
					{
						CRect rcRet = pFoldSheet->DrawSummary(pDC, rcFrame, pDisplayList, pPrintSheet);//	rcRet.right = rcFrame.right;
						rcBox.UnionRect(rcBox, rcRet); 
					}
				}
	}

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

void CPrintComponentsDetailView::OnLButtonDown(UINT nFlags, CPoint point) 
{
    SetFocus();	// In order to deactivate eventually active InplaceEdit

	m_DisplayList.OnLButtonDown(point); // pass message to display list

	CScrollView::OnLButtonDown(nFlags, point);
}

void CPrintComponentsDetailView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

void CPrintComponentsDetailView::OnMouseLeave()
{
	m_DisplayList.OnMouseLeave();
}


/////////////////////////////////////////////////////////////////////////////
// Messages back from display list

BOOL CPrintComponentsDetailView::OnSelectFlatProduct(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{
	pDI->m_nState = CDisplayItem::Normal;

	UndoSave();

	CDlgFlatProductData dlg;
	dlg.m_pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));;
	dlg.DoModal();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	pDoc->SetModifiedFlag();

	BOOL bDoUpdate = TRUE;
	if (IsOpenDlgComponentImposer())
		if (GetDlgComponentImposer())
		{
			GetDlgComponentImposer()->InitData(TRUE);
			bDoUpdate = FALSE;
		}

	if (bDoUpdate)
	{
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView));	
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView), TRUE);	
	}
	theApp.UpdateView(RUNTIME_CLASS(CProductsView));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintComponentsView));	

	ResizeView();

	return TRUE;
}

BOOL CPrintComponentsDetailView::OnDeselectFlatProduct(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{
	return TRUE;
}

BOOL CPrintComponentsDetailView::OnMouseMoveFlatProduct(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; 
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintComponentsDetailView::OnSelectPageSectionProps(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{
	pDI->m_nState = CDisplayItem::Normal;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	UndoSave();

	CPrintComponent		component	 = GetActComponent();
	CBoundProductPart*	pProductPart = component.GetBoundProductPart();
	CBoundProduct&		rProduct	 = pProductPart->GetBoundProduct();

	CString strOldFormatName	= rProduct.m_strFormatName;
	float	fOldWidth			= rProduct.m_fTrimSizeWidth;
	float	fOldHeight			= rProduct.m_fTrimSizeHeight;
	int		nOldPageOrientation = rProduct.m_nPageOrientation;
	int		nOldDesiredQuantity = rProduct.GetDesiredQuantity();
	int		nOldNumProductParts = rProduct.m_parts.GetSize();
	
	CDlgBoundProduct dlgBoundProduct;
	dlgBoundProduct.m_boundProduct = rProduct;
	if (dlgBoundProduct.DoModal() == IDCANCEL)
		return FALSE;

	rProduct = dlgBoundProduct.m_boundProduct;

	BOOL bReject = FALSE;
	if (dlgBoundProduct.PageFormatChanged())
		if (AfxMessageBox(IDS_MODIFY_PRODUCTPART_FORMAT, MB_YESNO) == IDNO)
			bReject = TRUE;
	if (bReject)
	{
		rProduct.m_strFormatName	= strOldFormatName;
		rProduct.m_fTrimSizeWidth	= fOldWidth;	
		rProduct.m_fTrimSizeHeight	= fOldHeight;					
		rProduct.m_nPageOrientation	= nOldPageOrientation;
	}
	else
	{
		if (dlgBoundProduct.PageFormatChanged())
			rProduct.ChangePageFormat(rProduct.m_strFormatName, rProduct.m_fTrimSizeWidth, rProduct.m_fTrimSizeHeight, rProduct.m_nPageOrientation, -rProduct.GetIndex() - 1);
	}

	BOOL bBindingChanged = (rProduct.m_bindingParams.m_bindingDefs.m_nBinding != dlgBoundProduct.m_boundProduct.m_bindingParams.m_bindingDefs.m_nBinding) ? TRUE : FALSE;
	
	if (bBindingChanged)
	{
		for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
		{
			switch(rProduct.m_parts[i].m_bindingDefs.m_nBinding)
			{
			case 0:	 pDoc->m_Bookblock.ModifyBindingMethod(CBindingDefs::PerfectBound,		rProduct.TransformLocalToGlobalPartIndex(i)); break;
			case 1:	 pDoc->m_Bookblock.ModifyBindingMethod(CBindingDefs::SaddleStitched,	rProduct.TransformLocalToGlobalPartIndex(i)); break;
			default: return FALSE;
			}
			pDoc->m_Bookblock.Reorganize(NULL, NULL);
			pDoc->m_Bookblock.ReNumberFoldSheets(-rProduct.GetIndex() - 1);
			pDoc->m_PageTemplateList.SortPages();
		}
	}

	if (nOldDesiredQuantity != dlgBoundProduct.m_boundProduct.GetDesiredQuantity())
	{
		for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
		{
			rProduct.m_parts[i].SetDesiredQuantity(dlgBoundProduct.m_boundProduct.GetDesiredQuantity());
			rProduct.m_parts[i].SetFoldSheetsDesiredQuantity(dlgBoundProduct.m_boundProduct.GetDesiredQuantity());
		}
	}

	int nNewProductPartIndex = nOldNumProductParts;
	pDoc->m_PageTemplateList.ShiftProductPartIndices(nNewProductPartIndex, rProduct.m_parts.GetSize() - nOldNumProductParts);
	pDoc->m_Bookblock.ShiftProductPartIndices(nNewProductPartIndex, rProduct.m_parts.GetSize() - nOldNumProductParts);

	int nProductIndex = rProduct.GetIndex();
	for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
	{
		if (i >= nOldNumProductParts)
			pDoc->m_PageTemplateList.InsertProductPartPages(rProduct.m_parts[i].m_nNumPages, nProductIndex, rProduct.m_parts[i].GetIndex());
		else
			pDoc->m_PageTemplateList.AdjustNumPages(rProduct.m_parts[i].m_nNumPages, nProductIndex, rProduct.m_parts[i].GetIndex());
	}
	pDoc->m_PageSourceList.InitializePageTemplateRefs();

	pDoc->SetModifiedFlag();

	BOOL bDoUpdate = TRUE;
	if (IsOpenDlgComponentImposer())
		if (GetDlgComponentImposer())
		{
			GetDlgComponentImposer()->InitData(TRUE);
			bDoUpdate = FALSE;
		}

	if (bDoUpdate)
	{
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView));	
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView), TRUE);	
	}
	theApp.UpdateView(RUNTIME_CLASS(CProductsView));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintComponentsView));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));	

	m_DisplayList.Invalidate();
	ResizeView();

	return TRUE;
}

BOOL CPrintComponentsDetailView::OnDeselectPageSectionProps(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{
	return TRUE;
}

BOOL CPrintComponentsDetailView::OnMouseMovePageSectionProps(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; 
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintComponentsDetailView::OnSelectFoldSheetProps(CDisplayItem* pDI, CPoint point, LPARAM lParam)	
{
	OnSelectPageSectionProps(pDI, point, lParam);

	return TRUE;
}

BOOL CPrintComponentsDetailView::OnDeselectFoldSheetProps(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{

	return TRUE;
}

BOOL CPrintComponentsDetailView::OnMouseMoveFoldSheetProps(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; 
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

void CPrintComponentsDetailView::UndoSave()
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));	
	if (pView)
		pView->UndoSave();
}