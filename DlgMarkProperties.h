#if !defined(AFX_DLGMARKPROPERTIES_H__C01FC225_7C68_11D5_8877_204C4F4F5020__INCLUDED_)
#define AFX_DLGMARKPROPERTIES_H__C01FC225_7C68_11D5_8877_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgMarkProperties.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMarkProperties 

class CDlgMarkProperties : public CDialog
{
// Konstruktion
public:
	CDlgMarkProperties(CWnd* pParent = NULL);   // Standardkonstruktor

// Destruktion
public:
	~CDlgMarkProperties();

// Dialogfelddaten
	//{{AFX_DATA(CDlgMarkProperties)
	enum { IDD = IDD_MARK_PROPERTIES };
	CTabCtrl	m_markPropsTab;
	//}}AFX_DATA

public:
	CLayoutObject*		m_pMarkObject;
	CDlgPDFMarkGeneral	m_dlgPDFMarkGeneral;
	CDlgMoveObjects		m_dlgMoveMark;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgMarkProperties)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	void InitPDFMarkDialog();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgMarkProperties)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeMarkpropsTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnApplyButton();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGMARKPROPERTIES_H__C01FC225_7C68_11D5_8877_204C4F4F5020__INCLUDED_
