#pragma once


#include "OwnerDrawnButtonList.h"


// CAutoServerWorkflowsView-Formularansicht

class CAutoServerWorkflowsView : public CScrollView
{
	DECLARE_DYNCREATE(CAutoServerWorkflowsView)

public:
	DECLARE_DISPLAY_LIST(CAutoServerWorkflowsView, OnMultiSelect, NO, OnStatusChanged, YES)
	DECLARE_DISPLAY_ITEM(CAutoServerWorkflowsView, Workflow,		OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CAutoServerWorkflowsView, JobStatusField,	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, FALSE)

protected:
	CAutoServerWorkflowsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CAutoServerWorkflowsView();

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


enum displayColIDs {ColJob, ColJobWorkflow, ColJobDate, ColJobStatus, ColJobInputFile, ColJobInputDate, ColJobOutputFile, ColJobOutputDate, ColJobOutputLocation, ColWorkflow, ColWorkflowJobDataSource, ColWorkflowStrippingDataSource, ColWorkflowHotfolder, ColWorkflowJobs};
typedef struct displayColumn
{
	int nID;
	int nWidth;
};
typedef CArray <struct displayColumn, struct displayColumn&> DISPLAY_COLUMN_LIST;

public:
	DISPLAY_COLUMN_LIST		m_displayColumns;



public:
	CSize					m_docSize;
	CHeaderCtrl  			m_HeaderCtrl;
	int			 			m_nHeaderHeight;
	int						m_nHeaderItemsTotalWidth;
	CFont		 			m_headerFont;
	CArray <int, int>		m_headerColumnWidths;
	COwnerDrawnButtonList	m_toolBar;


public:
	void	SetHeaderColumnWidth(int nIndex, int nValue);
	int		GetHeaderColumnWidth(int nIndex, int nDefault);
	int		GetActJobID();
	CSize	CalcExtents();
	CRect	Draw(CDC* pDC, CDisplayList* pDisplayList);
	CRect	DrawWorkflowsTable				(CDC* pDC, CDisplayList* pDisplayList);
	CRect	DrawWorkflowInfo				(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList);
	CRect	DrawWorkflowJobDataSource		(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList);
	CRect	DrawWorkflowStrippingDataSource	(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList);
	CRect	DrawWorkflowHotfolderInfo		(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList);
	CRect	DrawWorkflowJobsInfo			(CDC* pDC, CRect rcFrame, kimAutoDB::_workflows& rWorkflow, CDisplayList* pDisplayList);
	void	ResetDisplayColums();
	void	AddDisplayColumn(int nColID, int nWidth);



	DECLARE_MESSAGE_MAP()
public:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void OnInitialUpdate();
protected:
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnHeaderTrack	 (NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


