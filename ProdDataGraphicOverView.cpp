// ProdDataGraphicOverView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "MainFrm.h"
#include "ProdDataGraphicOverView.h"
#include "ProdDataFrame.h"
#include "MediaPivotControl.h"
#include "ProdDataPrintView.h"
#include "BookblockFrame.h"
#include "ModalFrame.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "BookBlockView.h"
#include "DlgShinglingAssistent.h"
#include "GraphicComponent.h"
#include "DlgJobData.h"


// CPDGCustomPaintFrame

IMPLEMENT_DYNAMIC(CPDGCustomPaintFrame, CStatic)

CPDGCustomPaintFrame::CPDGCustomPaintFrame()
{
	m_itemData = -1;
}

CPDGCustomPaintFrame::~CPDGCustomPaintFrame()
{
}

BEGIN_MESSAGE_MAP(CPDGCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CPDGCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CProdDataGraphicOverView* pView = (CProdDataGraphicOverView*)GetParent();
	if ( ! pView)
		return;
	CRect rcFrame;
	GetClientRect(rcFrame);
	int nProductPartIndex = (int)m_itemData;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	switch (GetDlgCtrlID())
	{
	case IDC_PDH_JOBTITLE_HEADER:			{
												CGraphicComponent gp;
												gp.DrawTitleBar(&dc, rcFrame, L"", RGB(160,180,255), RGB(120,140,230), 90);
												//gp.DrawTitleBar(&dc, rcFrame, L"", RGB(255, 240, 180), RGB(235, 225, 155), 90, FALSE);

												dc.SetTextColor(RGB(100, 120, 200));
												dc.SetBkMode(TRANSPARENT);
												dc.SelectObject(&pView->m_bigFont);
												CString strOut;
												rcFrame.OffsetRect(15, 0);
												if (pDoc)
													dc.DrawText(pDoc->GetTitle(), rcFrame, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
											}
											break;

	case IDC_PDG_PRODPART_HEADER:			{	
												CString strOut;
												strOut = ( (nProductPartIndex >= 0) && (nProductPartIndex < pDoc->m_productPartList.GetSize()) ) ? pDoc->m_productPartList[nProductPartIndex].m_strName : _T("");
												CGraphicComponent gp;
												gp.DrawTitleBar(&dc, rcFrame, strOut, RGB(180,245,0), RGB(160,215,0), 90);
											}
											break;

	case IDC_PDG_PRODPART_FRAME:			{	
												CPen pen(PS_SOLID, 1, MIDDLEGRAY);
												dc.SelectObject(&pen);
												//CBrush brush(RGB(250, 250, 245));//241, 241, 235));
												//dc.SelectObject(&brush);
												dc.SelectStockObject(HOLLOW_BRUSH);
												dc.RoundRect(rcFrame, CPoint(6,6));
												CGraphicComponent gp;
												//gp.DrawTitleBar(&dc, rcFrame, L"", RGB(250,250,245), RGB(230,230,220), 90);
											}
											break;

	case IDC_PDG_DIVIDER0:					
	case IDC_PDG_DIVIDER1:					
	case IDC_PDG_DIVIDER2:					
	case IDC_PDG_DIVIDER3:					{	CPen pen(PS_SOLID, 1, RGB(200,200,200));
												dc.SelectObject(&pen);
												dc.MoveTo(rcFrame.left, rcFrame.top);
												dc.LineTo(rcFrame.left, rcFrame.bottom);
											}
											break;

	default:								break;
	}
}


// CPDGCustomPaintFrameList

IMPLEMENT_DYNAMIC(CPDGCustomPaintFrameList, CStatic)

CPDGCustomPaintFrameList::CPDGCustomPaintFrameList()
{
}

CPDGCustomPaintFrameList::~CPDGCustomPaintFrameList()
{
	for (int i = 0; i < GetSize(); i++)
		delete ElementAt(i);
}

void CPDGCustomPaintFrameList::RemoveAll()
{
	for (int i = 0; i < GetSize(); i++)
		delete ElementAt(i);

	CArray::RemoveAll();
}




// CProdDataGraphicOverView

IMPLEMENT_DYNCREATE(CProdDataGraphicOverView, CFormView)

CProdDataGraphicOverView::CProdDataGraphicOverView()
	: CFormView(CProdDataGraphicOverView::IDD)
{
	m_bigFont.CreateFont   (18, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	m_mediumFont.CreateFont(13, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	m_smallFont.CreateFont (10, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	m_BkBrushBlue.CreateSolidBrush(RGB(193, 211, 251));
	m_BkBrushOrange.CreateSolidBrush(RGB(252, 220, 150));
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);

	m_hThemeToolbar	= theApp._OpenThemeData(this, L"TOOLBAR");
}

CProdDataGraphicOverView::~CProdDataGraphicOverView()
{
	m_bigFont.DeleteObject();
	m_mediumFont.DeleteObject();
	m_smallFont.DeleteObject();
	m_BkBrushBlue.DeleteObject();
	m_BkBrushOrange.DeleteObject();
	m_hollowBrush.DeleteObject();

	theApp._CloseThemeData(m_hThemeToolbar);
}

void CProdDataGraphicOverView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CProdDataGraphicOverView, CFormView)
	ON_WM_SIZE()
	ON_COMMAND(ID_SELECT_PRODUCTPART,	OnSelectProductData)
	ON_COMMAND(ID_SELECT_TRIMMINGDATA,	OnSelectTrimData)
	ON_COMMAND(ID_SELECT_BINDINGDATA,	OnSelectBindData)
	ON_COMMAND(ID_SELECT_FOLDSCHEME,	OnSelectCutFoldData)
	ON_COMMAND(ID_SELECT_PRINTLAYOUT,	OnSelectPrintData)
	ON_COMMAND(ID_SELECT_PREPRESSDATA,	OnSelectPrePressData)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CProdDataOverView-Diagnose

#ifdef _DEBUG
void CProdDataGraphicOverView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CProdDataGraphicOverView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CProdDataGraphicOverView::DrawItemJobComponent(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState)
{
//	nState = ODB_HOT;

	int nProductPartIndex = (int)lpDrawItemStruct->itemData;
	if (nProductPartIndex == -1)
		return;

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem = lpDrawItemStruct->rcItem;

	CPen pen(PS_SOLID, 0, RGB(200,200,200));
	pDC->SelectObject(&pen);
	pDC->SelectStockObject(HOLLOW_BRUSH);
	pDC->RoundRect(rcItem, CPoint(2, 2));
	pen.DeleteObject();

	CRect rcHeader = rcItem; rcHeader.bottom = rcHeader.top + 15;
	pen.CreatePen(PS_SOLID, 1, LIGHTGRAY);
	CBrush brush(RGB(255, 240, 180));
	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	pDC->RoundRect(rcHeader, CPoint(6,6));
	pen.DeleteObject();
	brush.DeleteObject();

	switch (lpDrawItemStruct->CtlID)
	{
	case ID_SELECT_PRODUCTPART:		DrawItemProduct(lpDrawItemStruct, nState);	
									break;
	case ID_SELECT_TRIMMINGDATA:	DrawItemTrimming(lpDrawItemStruct, nState);	
									break;
	case ID_SELECT_BINDINGDATA:		DrawItemBinding(lpDrawItemStruct, nState);	
									break;
	case ID_SELECT_FOLDSCHEME:		DrawItemCutFold(lpDrawItemStruct, nState);
									break;
	case ID_SELECT_PRINTLAYOUT:		DrawItemPrint(lpDrawItemStruct, nState);
									break;
	case ID_SELECT_PREPRESSDATA:	DrawItemPrePress(lpDrawItemStruct, nState);
									break;
	}
}

void CProdDataGraphicOverView::DrawItemProduct(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState)
{
	nState = ODB_HOT;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	int nProductPartIndex = lpDrawItemStruct->itemData;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem = lpDrawItemStruct->rcItem;

	pDoc->m_productPartList[nProductPartIndex].DrawProductData(CDC::FromHandle(lpDrawItemStruct->hDC), rcItem, nProductPartIndex);
}

void CProdDataGraphicOverView::DrawItemTrimming(LPDRAWITEMSTRUCT lpDrawItemStruct, int /*nState*/)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = lpDrawItemStruct->itemData;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcFrame = lpDrawItemStruct->rcItem;

	CProductPart& rProductPart = pDoc->m_productPartList[nProductPartIndex];

	int nXPos = rcFrame.left + 10;
	int nYPos = rcFrame.top  + 1;

	pDC->SetTextColor(GUICOLOR_VALUE);
	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(255, 240, 180));
	CFont font;
	font.CreateFont (12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
	CString strOut;
	strOut.Format(_T("%d"), rProductPart.m_nNumPages); 
	pDC->TextOut(nXPos, nYPos, strOut);
	pDC->SetTextColor(GUICOLOR_CAPTION); 
	strOut.LoadStringA(IDS_PAGES);
	pDC->TextOut(nXPos + pDC->GetTextExtent(strOut).cx + 10, nYPos, strOut);
	pDC->SetTextColor(GUICOLOR_VALUE);
	font.DeleteObject();
	font.CreateFont (12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);

	nXPos = rcFrame.left + 10;
	nYPos = rcFrame.top  + 25;

	CString strMeasureFormat;
	strMeasureFormat.Format("%s", (LPCTSTR)MeasureFormat("%.1f"));
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_CAPTION); 
	pDC->TextOut(nXPos, nYPos, _T("Format:"));
	nYPos += 12;
	strOut.Format(_T("%s (%.1f x %.1f)"), rProductPart.m_strFormatName, rProductPart.m_fPageWidth, rProductPart.m_fPageHeight); 
	pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos, nYPos, strOut);

	CRect rcThumbnail;
	rcThumbnail.left = rcFrame.left;
	rcThumbnail.top = rcFrame.top + 60;
	rcThumbnail.bottom = rcThumbnail.top + 60;
	rcThumbnail.right = rcThumbnail.left + 50;
	//rcThumbnail.OffsetRect(5, 25);
	pDoc->m_PageTemplateList.Draw(pDC, rcThumbnail, 0, nProductPartIndex, FALSE, FALSE, TRUE, FALSE);

	pDC->SetTextColor(GUICOLOR_VALUE); 
	nXPos = rcThumbnail.right + 5; nYPos = rcThumbnail.top;
	CString string;
	string.LoadStringA(IDS_TRIMMINGS); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, string);		
	nYPos += 15;
	strOut.Format(strMeasureFormat, MeasureFormat(rProductPart.m_prodDataBound.m_fHeadTrim));
	string.LoadStringA(IDS_HEAD); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, string);		pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 30, nYPos, strOut);
	
	nYPos += 15;
	strOut.Format(strMeasureFormat, MeasureFormat(rProductPart.m_prodDataBound.m_fSideTrim));
	string.LoadStringA(IDS_SIDE); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, string);		pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 30, nYPos, strOut);
	
	nYPos += 15;
	strOut.Format(strMeasureFormat, MeasureFormat(rProductPart.m_prodDataBound.m_fFootTrim));
	string.LoadStringA(IDS_FOOT); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, string);		pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 30, nYPos, strOut);

	nXPos = rcFrame.left + 10; nYPos = rcFrame.bottom - 20;
	if (rProductPart.HasMarks(CMark::CropMark))
	{
		pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, _T("Schneidmarken")); 
		pDC->DrawIcon(nXPos + 80, nYPos + 2, theApp.LoadIconA(IDI_CHECK));
	}

	font.DeleteObject();
}

void CProdDataGraphicOverView::DrawItemBinding(LPDRAWITEMSTRUCT lpDrawItemStruct, int /*nState*/)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = lpDrawItemStruct->itemData;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcFrame = lpDrawItemStruct->rcItem;

	CProductPart& rProductPart = pDoc->m_productPartList[nProductPartIndex];

	rProductPart.DrawProductionData(pDC, rcFrame, nProductPartIndex, TRUE);

	//CFont font;
	//font.CreateFont (12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	//pDC->SelectObject(&font);
	//CString strOut, strOut2, strMeasureFormat;

	//int nXPos = rcFrame.left + 15;
	//int nYPos = rcFrame.top + 4;

	//pDC->SetTextColor(GUICOLOR_VALUE);
	//pDC->SetBkMode(TRANSPARENT);
	//pDC->SetBkColor(RGB(255, 240, 180));

	//strOut.LoadStringA( (rProductPart.m_nBindingMethod == CFinalProductDescription::PerfectBound) ? IDS_PERFECTBOUND_TOOLTIP : IDS_SADDLESTITCHED_TOOLTIP);
	//pDC->TextOut(nXPos, nYPos, strOut);

	//CRect rcThumbnail = rcFrame;
	//rcThumbnail.right = rcFrame.left + 32; rcThumbnail.bottom = rcThumbnail.top + 20;
	//rcThumbnail.OffsetRect(15, 25);

	//pDC->DrawIcon(rcThumbnail.left, rcThumbnail.top, theApp.LoadIconA( (rProductPart.m_nBindingMethod == CFinalProductDescription::PerfectBound) ? IDI_PERFECT_BOUND : IDI_SADDLE_STITCHED) );

	//font.DeleteObject();
	//font.CreateFont (12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	//pDC->SelectObject(&font);

	//nYPos = rcThumbnail.bottom + 5;
	//strOut.Format( (rProductPart.m_nProductionType == CFinalProductDescription::Single) ? _T("Einfach") : _T("Kommen und Gehen"));
	//pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, _T("Produktionsart:"));	
	//nYPos += 15;
	//pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos, nYPos, strOut);

	//nYPos += 20; 
	//strMeasureFormat.Format("%s", (LPCTSTR)MeasureFormat("%.1f"));
	//if (rProductPart.m_nOverFoldSide != CProductPart::OverFoldNone)
	//{
	//	strOut.Format(strMeasureFormat, rProductPart.m_fOverFold);
	//	if (rProductPart.m_nOverFoldSide == CProductPart::OverFoldFront)
	//		strOut2 = _T("Vorfalz:");
	//	else
	//		strOut2 = _T("Nachfalz:");
	//	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, strOut2); pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 60, nYPos, strOut);
	//	nYPos += 15; 
	//}
	//if ( (rProductPart.m_fMillingEdge != 0.0f) || (rProductPart.m_fSpineTrim != 0.0f) )
	//{
	//	if (rProductPart.m_fMillingEdge > rProductPart.m_fSpineTrim)
	//		strOut2 = _T("Fr�srand:");
	//	else
	//		strOut2 = _T("Bundsteg:");
	//	strOut.Format(strMeasureFormat, MeasureFormat(max(rProductPart.m_fMillingEdge, rProductPart.m_fSpineTrim)));
	//	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, strOut2); pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 60, nYPos, strOut);
	//	nYPos += 20; 
	//}

	//nYPos = rcFrame.bottom - 20;
	//if (rProductPart.HasMarks(CMark::SectionMark))
	//{
	//	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, _T("Flattermarke")); 
	//	pDC->DrawIcon(nXPos + 60, nYPos + 2, theApp.LoadIconA(IDI_CHECK));
	//}

	//font.DeleteObject();
}

void CProdDataGraphicOverView::DrawItemCutFold(LPDRAWITEMSTRUCT lpDrawItemStruct, int /*nState*/)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CFoldSheet* pFoldSheet = (CFoldSheet*)lpDrawItemStruct->itemData;
	if ( ! pFoldSheet)
		return;
	CString	 strMeasureFormat;

	if ( (pFoldSheet->m_nProductPartIndex < 0) || (pFoldSheet->m_nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;
	CProductPart& rProductPart = pDoc->m_productPartList[pFoldSheet->m_nProductPartIndex];

	int nUnImposedPages = 0;
	if (pFoldSheet->m_strFoldSheetTypeName.IsEmpty())	// dummy -> placeholder for unassigned pages
		nUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(pFoldSheet->m_nProductPartIndex);

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcFrame = lpDrawItemStruct->rcItem;

	CFont font, boldFont;
	font.CreateFont	   (12, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	boldFont.CreateFont(12, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&boldFont);
	pDC->SetTextColor(GUICOLOR_CAPTION);
	pDC->SetBkMode(TRANSPARENT);

	CString strOut, strOut2;
	CRect rcText(rcFrame.left + 10, rcFrame.top + 1, rcFrame.right, rcFrame.top + 30);
	CRect rcText2 = rcText; rcText2.OffsetRect(70, 0);

	pDC->SetTextColor(GUICOLOR_CAPTION);	strOut2.LoadStringA(IDS_SCHEME); pDC->DrawText(strOut2, rcText, DT_LEFT | DT_TOP);	

	if (nUnImposedPages == 0)
		strOut.Format(_T("%s  "), pFoldSheet->m_strFoldSheetTypeName);
	else
		strOut.LoadStringA(IDS_NOT_DEFINED);

	pDC->SelectObject(&boldFont);
	pDC->SetTextColor((nUnImposedPages == 0) ? GUICOLOR_VALUE : LIGHTRED);	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);

	rcText.OffsetRect(0, 20); 
	pDC->SelectObject(&font);

	int nNumSheets = pFoldSheet->GetNumSamePrintLayout();
	CRect rcThumbnail(rcFrame.CenterPoint().x + 10, rcFrame.top + 30, rcFrame.right - 5, rcFrame.top + 70);
	if (nUnImposedPages > 0)
	{
		//rcThumbnail.OffsetRect(0, 10);
		pDoc->m_Bookblock.DrawFoldSheetList(pDC, CRect(rcFrame.left + 10, rcFrame.top + 30, rcFrame.right - 5, rcFrame.top + 70), pFoldSheet->m_nProductPartIndex, pFoldSheet, TRUE);
	}
	else
	{
		rcThumbnail.right = rcThumbnail.left + 40;
		pFoldSheet->DrawThumbnail(pDC, rcThumbnail);

		CString string;
		string.LoadStringA(IDS_SHEETS);
		strOut.Format(_T("%d %s"), nNumSheets, string);

		CRect rcText;
		rcText.left = rcThumbnail.left; rcText.top = rcThumbnail.top - 5; rcText.bottom = rcText.top + 15; rcText.right = rcFrame.right;
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP);

		int nLayerIndex = 0;
		int nNUp = pFoldSheet->GetNUpPrintLayout(NULL, nLayerIndex);
		if (nNUp > 1)
		{
			CRect rcSheet(rcFrame.left + 10, rcFrame.top + 50, rcFrame.CenterPoint().x - 5, rcFrame.top + 110);

			string.LoadStringA(IDS_NUP);
			strOut.Format(_T("%d %s"), nNUp, string);
			rcText.left = rcSheet.left; rcText.top = rcSheet.top - 25; rcText.right = rcFrame.right; rcText.bottom = rcText.top + 15;
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP);

			int nLayerIndex = 0;
			CLayout* pLayout = pFoldSheet->GetLayout(nLayerIndex);
			if (pLayout)
				pLayout->Draw(pDC, rcSheet, pFoldSheet->GetPrintSheet(nLayerIndex), NULL, TRUE, TRUE, FALSE, NULL, TRUE);
		}
	}

	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	if (nUnImposedPages > 0)
	{
		rcText = CRect(rcThumbnail.left, rcThumbnail.bottom + 8, rcFrame.right, rcThumbnail.bottom + 23);
		rcText.OffsetRect(0, 15); rcText.left = rcFrame.left + 10;
		//pDC->DrawText(_T("Anklicken zum Zuweisen eines Falzschemas."), rcText, DT_LEFT | DT_TOP);
		return;
	}

	rcText.OffsetRect(0, 40); rcText.left = rcFrame.CenterPoint().x + 10;
	rcText2 = rcText; rcText.right = rcThumbnail.right; rcText2.OffsetRect(70, 0);
	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, pFoldSheet->m_nProductPartIndex, pFoldSheet);
	if (sortedFoldSheets.GetSize() > 0)
		if (sortedFoldSheets.GetSize() > 1)
			strOut.Format(_T("%s - %s"), sortedFoldSheets[0]->GetNumber(), sortedFoldSheets[sortedFoldSheets.GetSize() - 1]->GetNumber());
		else
			strOut.Format(_T("%s"),		 sortedFoldSheets[0]->GetNumber());

	pDC->SetTextColor(GUICOLOR_VALUE); pDC->DrawText(strOut, rcText, DT_LEFT | DT_CENTER);	

	rcText.right = rcFrame.right;
	rcText.OffsetRect(0, 25); rcText2.OffsetRect(0, 25); 
	int nNumPages = pFoldSheet->GetNumPages();
	strOut.Format(_T("%d"), nNumPages);
	CString string, string2;
	string.LoadStringA(IDS_PAGES); string2.LoadStringA(IDS_SHEET_TEXT);
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string + string2 + L":",	rcText, DT_LEFT | DT_TOP);		pDC->SetTextColor(GUICOLOR_VALUE); pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);

	BOOL bShowShingling = rProductPart.m_prodDataBound.m_bShinglingActive;
	if (rProductPart.m_prodDataBound.m_nShinglingMethod == CBookblock::ShinglingMethodBottling)
	{
		if ( (pFoldSheet->m_fShinglingValue == 0.0f) && (pFoldSheet->m_fShinglingValueFoot == 0.0f) )
			bShowShingling = FALSE;
	}
	else
		if (pFoldSheet->m_fShinglingValue == 0.0f)
			bShowShingling = FALSE;

	if (bShowShingling)
	{
		rcText.OffsetRect(0, 20); rcText.right = rcText.left + 60; rcText2.OffsetRect(0, 20); 
		strMeasureFormat.Format("%s", (LPCTSTR)MeasureFormat("%.1f"));
	//	rcText.left = rcText2.left + 50; rcText.right = rcText.left + 60; rcText.top = rcThumbnail.bottom + 15; rcText.bottom = rcText.top + 20;
	//	rcText2 = rcText; rcText2.OffsetRect(70, 0); 
		strOut.LoadStringA(IDS_SHINGLING);
		pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_END_ELLIPSIS);
		float fValue = CDlgShinglingAssistent::ShinglingValue2Display(*pFoldSheet, pFoldSheet->m_fShinglingValue,	  FALSE);
		strOut.Format (strMeasureFormat, fValue);
		fValue		 = CDlgShinglingAssistent::ShinglingValue2Display(*pFoldSheet, pFoldSheet->m_fShinglingValueFoot, FALSE);
		strOut2.Format(strMeasureFormat, fValue);
		if (rProductPart.m_prodDataBound.m_nShinglingMethod == CBookblock::ShinglingMethodBottling)
		{
			CRect rcText3 = rcText, rcText4;
			CString string;
			string.LoadStringA(IDS_HEAD); string += L":";
			rcText3.OffsetRect(0, 15); rcText4 = rcText3; rcText4.OffsetRect(pDC->GetTextExtent(string).cx + 5, 0);
			pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string, rcText3, DT_LEFT | DT_TOP);			pDC->SetTextColor(GUICOLOR_VALUE); pDC->DrawText(strOut,  rcText4, DT_LEFT | DT_TOP);
			rcText3.OffsetRect(60, 0); rcText4.OffsetRect(60, 0);
			string.LoadStringA(IDS_FOOT); string += L":";
			pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string,  rcText3, DT_LEFT | DT_TOP);			pDC->SetTextColor(GUICOLOR_VALUE); pDC->DrawText(strOut2, rcText4, DT_LEFT | DT_TOP);
		}
		else
		{
			pDC->SetTextColor(GUICOLOR_VALUE); pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);
		}
	}

	font.DeleteObject();
	boldFont.DeleteObject();
}

void CProdDataGraphicOverView::DrawItemPrint(LPDRAWITEMSTRUCT lpDrawItemStruct, int /*nState*/)
{
	CFoldSheet*	  pFoldSheet   = (CFoldSheet*)lpDrawItemStruct->itemData;
	if ( ! pFoldSheet)
		return;
	int				 nLayerIndex = 0;
	CFoldSheetLayer* pLayer		 = pFoldSheet->GetLayer(nLayerIndex);
	if ( ! pLayer)
		return;
	CPrintSheet*  pPrintSheet  = pLayer->GetFirstPrintSheet();
	CLayout*	  pLayout	   = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	if ( ! pPrintSheet || ! pLayout || ! pPressDevice)
		return;
	CString strOut, strOut2, strMeasureFormat;
	strMeasureFormat.Format("%s x %s", (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem = lpDrawItemStruct->rcItem;

	CRect rcText(rcItem.left + 10, rcItem.top + 1, rcItem.right, rcItem.top + 30);
	CFont font;
	font.CreateFont(12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
	pDC->SetTextColor(GUICOLOR_CAPTION);
	pDC->SetBkMode(TRANSPARENT);
	strOut.LoadStringA(IDS_PRINTSHEETLAYOUT); strOut += L" ";
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP);

	CRect rcThumbnail(rcItem.left + 5, rcItem.top + 40, rcItem.left + 75, rcItem.top + 90);
	pLayout->Draw(pDC, rcThumbnail, pPrintSheet, pPressDevice, TRUE, FALSE, FALSE, NULL, TRUE);

	pDC->SetTextColor(GUICOLOR_VALUE);
	pDC->SetBkMode(TRANSPARENT);
	CRect rcText2 = rcText; rcText2.OffsetRect(75, 0);
	pDC->DrawText(pLayout->m_strLayoutName, rcText2, DT_LEFT | DT_TOP);

	rcText.OffsetRect(0, 20); 
	font.DeleteObject();
	font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	strOut.Format(_T("%s"), pPressDevice->m_strName);
	CString string; string.LoadStringA(IDS_PRESS); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string, rcText, DT_LEFT | DT_TOP);	

	rcText2 = rcText; rcText2.OffsetRect(80, 0);
	font.DeleteObject();
	font.CreateFont(12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
	pDC->SetTextColor(GUICOLOR_VALUE);	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);

	font.DeleteObject();
	font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);

	rcText.left = rcText2.left; rcText.top = rcThumbnail.top + 5; rcText.bottom = rcText.top + 15;
	rcText2 = rcText; rcText2.OffsetRect(80, 0);
	pDC->SetBkMode(TRANSPARENT);
	strOut.Format(strMeasureFormat, pPressDevice->m_fPlateWidth, pPressDevice->m_fPlateHeight);
	string.LoadStringA(IDS_PLATESIZE); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string, rcText, DT_LEFT | DT_TOP);	pDC->SetTextColor(GUICOLOR_VALUE);	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);

	rcText.OffsetRect(0, 15); rcText2.OffsetRect(0, 15); 
	pDC->SetBkMode(TRANSPARENT);
	strOut.Format(strMeasureFormat, pLayout->m_FrontSide.m_fPaperWidth, pLayout->m_FrontSide.m_fPaperHeight);
	string.LoadStringA(IDS_PAPERSIZE); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string, rcText, DT_LEFT | DT_TOP);		pDC->SetTextColor(GUICOLOR_VALUE);	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);

	rcText.OffsetRect(0, 15); rcText2.OffsetRect(0, 15); 
	pDC->SetBkMode(TRANSPARENT);
	string.LoadStringA(IDS_TYPEOFPRINTING); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string, rcText, DT_LEFT | DT_TOP);			pDC->SetTextColor(GUICOLOR_VALUE);	pDC->DrawText(pLayout->GetPrintingType(), rcText2, DT_LEFT | DT_TOP);

	rcText.OffsetRect(0, 15); rcText2.OffsetRect(0, 15); 
	pDC->SetBkMode(TRANSPARENT);
	string.LoadStringA(IDS_TYPEOFTURNING); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string, rcText, DT_LEFT | DT_TOP);			pDC->SetTextColor(GUICOLOR_VALUE);	pDC->DrawText(pLayout->GetTurningType(), rcText2, DT_LEFT | DT_TOP);

	if ( ! pLayout->HasMarks())
	{
		rcText.OffsetRect(0, 15); rcText2.OffsetRect(0, 15); 
		pDC->SetBkMode(TRANSPARENT);
		strOut2.LoadStringA(IDS_NOT_DEFINED);
		string.LoadStringA(IDS_NAV_PRINTSHEETMARKS); string += L":";
		pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string, rcText, DT_LEFT | DT_TOP);	pDC->SetTextColor(LIGHTRED);	pDC->DrawText(strOut2, rcText2, DT_LEFT | DT_TOP);
	}

	font.DeleteObject();
}

void CProdDataGraphicOverView::DrawItemPrePress(LPDRAWITEMSTRUCT lpDrawItemStruct, int /*nState*/)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CFoldSheet*	  pFoldSheet   = (CFoldSheet*)lpDrawItemStruct->itemData;
	if ( ! pFoldSheet)
		return;
	if ( (pFoldSheet->m_nProductPartIndex < 0) || (pFoldSheet->m_nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;
	int				 nLayerIndex = 0;
	CFoldSheetLayer* pLayer		 = pFoldSheet->GetLayer(nLayerIndex);
	if ( ! pLayer)
		return;
	CPrintSheet*  pPrintSheet  = (CPrintSheet*)pLayer->GetFirstPrintSheet();
	CLayout*	  pLayout	   = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	if ( ! pPrintSheet || ! pLayout || ! pPressDevice)
		return;
	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(pPrintSheet->m_strPDFTarget);
	
	CString strOut, strOut2, strMeasureFormat;
	strMeasureFormat.Format("%s x %s", (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem = lpDrawItemStruct->rcItem;

	CFont font;
	font.CreateFont(12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
	pDC->SetTextColor(GUICOLOR_CAPTION);
	pDC->SetBkMode(TRANSPARENT);

	CRect rcText(rcItem.left + 10, rcItem.top + 1, rcItem.right, rcItem.top + 30);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	strOut.LoadStringA(IDS_OUTPUT_TARGET); strOut += L":"; 
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP);

	pDC->SetTextColor(DARKBLUE);

	if (pTargetProps)
		pDC->SetTextColor(GUICOLOR_VALUE);
	else
		pDC->SetTextColor(LIGHTRED);

	font.DeleteObject();
	font.CreateFont(12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);

	CRect rcText2 = rcText; rcText2.OffsetRect(75, 0);
	if (pTargetProps)
		pDC->DrawText(pPrintSheet->m_strPDFTarget, rcText2, DT_LEFT | DT_TOP);
	else
	{
		strOut.LoadStringA(IDS_NOT_DEFINED), 
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);
		font.DeleteObject();
		//return;
	}

	CRect rcThumbnail(rcItem.left + 10, rcItem.top + 30, rcItem.left + 42, rcItem.top + 60);
	HICON hIcon = NULL;
	if (pTargetProps)
	{
		switch (pTargetProps->m_nType)
		{
		case CPDFTargetProperties::TypePDFFolder:	hIcon = theApp.LoadIcon(IDI_PDF_LARGE);		break;
		case CPDFTargetProperties::TypeJDFFolder:	hIcon = theApp.LoadIcon(IDI_JDF_);			break;
		case CPDFTargetProperties::TypePDFPrinter:	hIcon = theApp.LoadIcon(IDI_PRINTER_LARGE);	break;
		}
		pDC->DrawIcon(rcThumbnail.TopLeft(), hIcon);
	}

	pDC->SetBkMode(TRANSPARENT);
	font.DeleteObject();
	font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
//	rcText.left = rcThumbnail.right + 5; rcText.top = rcThumbnail.top; rcText.right = rcItem.right - 5; rcText.bottom = rcText.top + 15;
	rcText.left = rcItem.left + 12; rcText.top = rcThumbnail.bottom + 15; rcText.right = rcItem.right - 5; rcText.bottom = rcText.top + 15;
	rcText2 = rcText; rcText2.OffsetRect(40, 0); rcText2.right = rcItem.right - 5;
	if (pTargetProps)
	{
		strOut.Format(strMeasureFormat, pTargetProps->m_mediaList[0].GetMediaWidth(pPrintSheet), pTargetProps->m_mediaList[0].GetMediaHeight(pPrintSheet));
		CString string; string.LoadStringA(IDS_PAPERLIST_FORMAT); string += L":";
		pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string, rcText, DT_LEFT | DT_TOP);	pDC->SetTextColor(GUICOLOR_VALUE);	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);
	}

	//rcText.left = rcItem.left + 10; rcText.top = rcThumbnail.bottom + 20; rcText.right = rcItem.right - 5; rcText.bottom = rcText.top + 15;
	//rcText2.left = rcText.left; rcText2.OffsetRect(0, 15);
	rcText.OffsetRect(0, 15); rcText2.OffsetRect(0, 15);
	if (pTargetProps)
	{
		CString string; string.LoadStringA(IDS_LOCATION); string += L":";
		pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(string, rcText, DT_LEFT | DT_TOP);	
		pDC->SetTextColor(GUICOLOR_VALUE); pDC->DrawText(pTargetProps->m_strLocation, rcText2, DT_LEFT | DT_TOP | DT_PATH_ELLIPSIS);
	}

	rcText.OffsetRect(0, 10); rcText2.OffsetRect(0, 10);
	rcText.left = rcItem.left + 10; rcText2.left = rcText.left + 50;
	rcText.OffsetRect(0, 20); rcText2.OffsetRect(0, 20);
	CPlate* pPlate = pPrintSheet->GetTopmostPlate(FRONTSIDE);
	strMeasureFormat.Format("%s", (LPCTSTR)MeasureFormat("%.1f"));
	strOut.LoadStringA(IDS_MASKING);
	rcText2.left = rcText.left + pDC->GetTextExtent(strOut).cx + 5; 
	strOut2.Format(strMeasureFormat, pPlate->m_fOverbleedOutside);
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP); pDC->SetTextColor(GUICOLOR_VALUE); pDC->DrawText(strOut2, rcText2, DT_LEFT | DT_TOP);

	font.DeleteObject();
}


// CProdDataGraphicOverView-Meldungshandler

void CProdDataGraphicOverView::OnInitialUpdate()
{
	m_jobTitleHeader.SubclassDlgItem	(IDC_PDH_JOBTITLE_HEADER,		this);
	m_prodPartHeader.SubclassDlgItem	(IDC_PDG_PRODPART_HEADER,		this);
	m_prodPartFrame.SubclassDlgItem		(IDC_PDG_PRODPART_FRAME,		this);
	m_prodDataFrame.SubclassDlgItem		(IDC_PDG_PRODUCTDATA_FRAME,		this);
	m_trimBindDataFrame.SubclassDlgItem	(IDC_PDG_BINDDATA_FRAME,		this);
	m_cutFoldDataFrame.SubclassDlgItem	(IDC_PDG_CUTFOLDDATA_FRAME,		this);
	m_printDataFrame.SubclassDlgItem	(IDC_PDG_PRINTDATA_FRAME,		this);
	m_prePressDataFrame.SubclassDlgItem	(IDC_PDG_PREPRESSDATA_FRAME,	this);

	m_jobComponentButtons.Create(this, GetParentFrame(), &CProdDataGraphicOverView::DrawItemJobComponent);

	CFormView::OnInitialUpdate();
}

void CProdDataGraphicOverView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

//	int nCurSel = m_jobComponentButtons.GetSelection();

	m_prodPartHeaderList.RemoveAll();
	m_prodPartFrameList.RemoveAll();
	m_jobComponentButtons.RemoveAll();
	m_dividerFrameList.RemoveAll();

	CRect rcProdPartHeader, rcProdPartFrame, rcProductDataFrame, rcTrimBindDataFrame, rcCutFoldDataFrame, rcPrintDataFrame, rcPrePressDataFrame;
	InitFrameRects(rcProdPartHeader, rcProdPartFrame, rcProductDataFrame, rcTrimBindDataFrame, rcCutFoldDataFrame, rcPrintDataFrame, rcPrePressDataFrame);

	int nOffset = 0;
	for (int nProductPartIndex = 0; nProductPartIndex < pDoc->m_productPartList.GetSize(); nProductPartIndex++)
	{
		if (pDoc->m_productPartList[nProductPartIndex].m_nType == CProductPart::Unbound)
			continue;

		// product data
		m_jobComponentButtons.AddButton(L"", L"", ID_SELECT_PRODUCTPART, rcProductDataFrame, -1, (ULONG_PTR)nProductPartIndex, FALSE);

		// trim/bind data
		m_jobComponentButtons.AddButton(L"", L"", ID_SELECT_BINDINGDATA,  rcTrimBindDataFrame, -1, (ULONG_PTR)nProductPartIndex, FALSE);

		int nCutFoldDataOffset		 = rcProdPartFrame.bottom - rcCutFoldDataFrame.top;
		int nPrintDataOffset		 = rcProdPartFrame.bottom - rcPrintDataFrame.top;
		int nPrePressDataOffset		 = rcProdPartFrame.bottom - rcPrePressDataFrame.top;
		CArray<CFoldSheet*, CFoldSheet*> foldSheetGroups;
		CBookblock::GetFoldSheetGroups(foldSheetGroups, nProductPartIndex);
		for (int i = 0; i < foldSheetGroups.GetSize(); i++)
		{
			// cut/fold data
			m_jobComponentButtons.AddButton(L"", L"", ID_SELECT_FOLDSCHEME,   rcCutFoldDataFrame,		-1, (ULONG_PTR)foldSheetGroups[i], FALSE);
			// print data
			m_jobComponentButtons.AddButton(L"", L"", ID_SELECT_PRINTLAYOUT,  rcPrintDataFrame,			-1, (ULONG_PTR)foldSheetGroups[i], FALSE);
			// prepress data
			m_jobComponentButtons.AddButton(L"", L"", ID_SELECT_PREPRESSDATA, rcPrePressDataFrame,		-1, (ULONG_PTR)foldSheetGroups[i], FALSE);

			rcCutFoldDataFrame.OffsetRect		(0, nCutFoldDataOffset);
			rcPrintDataFrame.OffsetRect			(0, nPrintDataOffset);
			rcPrePressDataFrame.OffsetRect		(0, nPrePressDataOffset);
		}

		int nUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(nProductPartIndex);
		if (nUnImposedPages > 0)
		{
			CFoldSheet* pDummyFoldSheet = new CFoldSheet;
			pDummyFoldSheet->m_nProductPartIndex = nProductPartIndex;
			m_jobComponentButtons.AddButton(L"", L"", ID_SELECT_FOLDSCHEME, rcCutFoldDataFrame, -1, (ULONG_PTR)pDummyFoldSheet, FALSE);	
			rcCutFoldDataFrame.OffsetRect (0, nCutFoldDataOffset);
		}

		// product part frame
		rcProdPartFrame.bottom = rcCutFoldDataFrame.top;
		CPDGCustomPaintFrame* pFrameWnd = new CPDGCustomPaintFrame;
		pFrameWnd->Create(_T(""), WS_CHILD | WS_VISIBLE, rcProdPartFrame,  this, IDC_PDG_PRODPART_FRAME,  (DWORD)nProductPartIndex);
		m_prodPartFrameList.Add(pFrameWnd);
		pFrameWnd = new CPDGCustomPaintFrame;
		pFrameWnd->Create(_T(""), WS_CHILD | WS_VISIBLE, rcProdPartHeader, this, IDC_PDG_PRODPART_HEADER, (DWORD)nProductPartIndex);
		m_prodPartHeaderList.Add(pFrameWnd);

		nOffset += rcProdPartFrame.Height() + 20;
		InitFrameRects(rcProdPartHeader, rcProdPartFrame, rcProductDataFrame, rcTrimBindDataFrame, rcCutFoldDataFrame, rcPrintDataFrame, rcPrePressDataFrame);
		rcProdPartHeader.OffsetRect	  		(0, nOffset);
		rcProdPartFrame.OffsetRect	  		(0, nOffset);
		rcProductDataFrame.OffsetRect		(0, nOffset);
		rcTrimBindDataFrame.OffsetRect	  	(0, nOffset);
		rcCutFoldDataFrame.OffsetRect		(0, nOffset);
		rcPrintDataFrame.OffsetRect			(0, nOffset);
		rcPrePressDataFrame.OffsetRect		(0, nOffset);
	}

	if (m_prodPartFrameList.GetSize())
	{
		CRect rcFrame;
		m_prodPartFrameList[m_prodPartFrameList.GetSize() - 1]->GetWindowRect(rcFrame); ScreenToClient(rcFrame);
		CRect rcDividerFrame;
		GetDlgItem(IDC_PDG_DIVIDER0)->GetWindowRect(rcDividerFrame); ScreenToClient(rcDividerFrame); rcDividerFrame.bottom = rcFrame.bottom + 20;
		CPDGCustomPaintFrame* pFrameWnd = new CPDGCustomPaintFrame;
		pFrameWnd->Create(_T(""), WS_CHILD | WS_VISIBLE, rcDividerFrame,  this, IDC_PDG_DIVIDER0,  0);
		m_dividerFrameList.Add(pFrameWnd);
		GetDlgItem(IDC_PDG_DIVIDER1)->GetWindowRect(rcDividerFrame); ScreenToClient(rcDividerFrame); rcDividerFrame.bottom = rcFrame.bottom + 20;
		pFrameWnd = new CPDGCustomPaintFrame;
		pFrameWnd->Create(_T(""), WS_CHILD | WS_VISIBLE, rcDividerFrame,  this, IDC_PDG_DIVIDER1,  0);
		m_dividerFrameList.Add(pFrameWnd);
		GetDlgItem(IDC_PDG_DIVIDER2)->GetWindowRect(rcDividerFrame); ScreenToClient(rcDividerFrame); rcDividerFrame.bottom = rcFrame.bottom + 20;
		pFrameWnd = new CPDGCustomPaintFrame;
		pFrameWnd->Create(_T(""), WS_CHILD | WS_VISIBLE, rcDividerFrame,  this, IDC_PDG_DIVIDER2,  0);
		m_dividerFrameList.Add(pFrameWnd);
		GetDlgItem(IDC_PDG_DIVIDER3)->GetWindowRect(rcDividerFrame); ScreenToClient(rcDividerFrame); rcDividerFrame.bottom = rcFrame.bottom + 20;
		pFrameWnd = new CPDGCustomPaintFrame;
		pFrameWnd->Create(_T(""), WS_CHILD | WS_VISIBLE, rcDividerFrame,  this, IDC_PDG_DIVIDER3,  0);
		m_dividerFrameList.Add(pFrameWnd);
	}
}

void CProdDataGraphicOverView::InitFrameRects(CRect& rcProdPartHeader, CRect& rcProdPartFrame, CRect& rcProductDataFrame, CRect& rcTrimBindDataFrame, CRect& rcCutFoldDataFrame, 
											  CRect& rcPrintDataFrame, CRect& rcPrePressDataFrame)
{
	BOOL bDetails = TRUE;

	m_prodPartHeader.GetWindowRect	 		(rcProdPartHeader);			ScreenToClient(rcProdPartHeader);		
	m_prodPartFrame.GetWindowRect	 		(rcProdPartFrame);			ScreenToClient(rcProdPartFrame);		if ( ! bDetails)	rcProdPartFrame.bottom		= rcProdPartFrame.CenterPoint().y;
	m_prodDataFrame.GetWindowRect	 		(rcProductDataFrame);		ScreenToClient(rcProductDataFrame);		if ( ! bDetails)	rcProductDataFrame.bottom	= rcProdPartFrame.bottom - 10;
	m_trimBindDataFrame.GetWindowRect	 	(rcTrimBindDataFrame);		ScreenToClient(rcTrimBindDataFrame);	if ( ! bDetails)	rcTrimBindDataFrame.bottom	= rcProdPartFrame.bottom - 10;
	m_cutFoldDataFrame.GetWindowRect 		(rcCutFoldDataFrame);		ScreenToClient(rcCutFoldDataFrame);		if ( ! bDetails)	rcCutFoldDataFrame.bottom	= rcProdPartFrame.bottom - 10;
	m_printDataFrame.GetWindowRect	 		(rcPrintDataFrame);			ScreenToClient(rcPrintDataFrame);		if ( ! bDetails)	rcPrintDataFrame.bottom		= rcProdPartFrame.bottom - 10;
	m_prePressDataFrame.GetWindowRect		(rcPrePressDataFrame);		ScreenToClient(rcPrePressDataFrame);	if ( ! bDetails)	rcPrePressDataFrame.bottom	= rcProdPartFrame.bottom - 10;
}

void CProdDataGraphicOverView::OnDraw(CDC* pDC)
{
	CView::OnDraw(pDC);
}

void CProdDataGraphicOverView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	//if ( ! m_jobTitleHeader.m_hWnd)
	//	return;

	//CRect rcTitle, rcTarget;
	//m_jobTitleHeader.GetClientRect(rcTitle);
	//m_jobTitleHeader.ClientToScreen(rcTitle);
	//ScreenToClient(rcTitle);
	//GetClientRect(rcTarget);
	//m_jobTitleHeader.SetWindowPos(NULL, rcTitle.left, rcTitle.top, rcTarget.Width(), rcTitle.Height(), SWP_NOZORDER);
}
	
void CProdDataGraphicOverView::OnSelectProductData()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = (int)m_jobComponentButtons.GetSelectedButtonData();
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CDlgJobData dlgJobData(IDD_PRODUCTDATA, (ULONG_PTR)0);
	if (dlgJobData.DoModal() == IDCANCEL)
		return;

	pDoc->SetModifiedFlag();
	pDoc->UpdateAllViews(NULL);
}

void CProdDataGraphicOverView::OnSelectTrimData()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = (int)m_jobComponentButtons.GetSelectedButtonData();
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CDlgJobData dlgJobData(IDD_PRODUCTIONPLANNER, (ULONG_PTR)nProductPartIndex);
	if (dlgJobData.DoModal() == IDCANCEL)
		return;

	pDoc->SetModifiedFlag();
	pDoc->UpdateAllViews(NULL);
}
	
void CProdDataGraphicOverView::OnSelectBindData()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = (int)m_jobComponentButtons.GetSelectedButtonData();
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CDlgJobData dlgJobData(IDD_PRODUCTIONPLANNER, (ULONG_PTR)nProductPartIndex);
	if (dlgJobData.DoModal() == IDCANCEL)
		return;

	pDoc->SetModifiedFlag();

	pDoc->UpdateAllViews(NULL);
}

void CProdDataGraphicOverView::OnSelectCutFoldData()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CFoldSheet* pFoldSheet = (CFoldSheet*)m_jobComponentButtons.GetSelectedButtonData();
	if ( ! pFoldSheet)
		return;

	CDlgJobData dlgJobData(IDD_PRODUCTIONPLANNER, (ULONG_PTR)pFoldSheet->m_nProductPartIndex);
	if (dlgJobData.DoModal() == IDCANCEL)
		return;

	pDoc->SetModifiedFlag();

	pDoc->UpdateAllViews(NULL);
}

void CProdDataGraphicOverView::OnSelectPrintData()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int			 nLayerIndex = 0;
	CFoldSheet*  pFoldSheet  = (CFoldSheet*)m_jobComponentButtons.GetSelectedButtonData();
	CPrintSheet* pPrintSheet = (pFoldSheet) ? pFoldSheet->GetPrintSheet(nLayerIndex) : NULL;
	if ( ! pPrintSheet)
		return;

	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETLIST);
	pFrame->SelectPrintSheet(pPrintSheet);
}

void CProdDataGraphicOverView::OnSelectPrePressData()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int			 nLayerIndex = 0;
	CFoldSheet*  pFoldSheet  = (CFoldSheet*)m_jobComponentButtons.GetSelectedButtonData();
	CPrintSheet* pPrintSheet = (pFoldSheet) ? pFoldSheet->GetPrintSheet(nLayerIndex) : NULL;
	if ( ! pPrintSheet)
		return;

	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_OUTPUT);
	pFrame->SelectPrintSheet(pFoldSheet->GetPrintSheet(nLayerIndex));
}

HBRUSH CProdDataGraphicOverView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

    if (nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_STATIC:
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(DARKBLUE);
				hbr = (HBRUSH)m_hollowBrush.GetSafeHandle();
				break;

		default:break;
		}
    }

	return hbr;
}
