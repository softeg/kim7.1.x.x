#if !defined(AFX_DLGPAPERLISTENTRY_H__77FFE49F_7785_40A8_A5B5_18EEF66335B5__INCLUDED_)
#define AFX_DLGPAPERLISTENTRY_H__77FFE49F_7785_40A8_A5B5_18EEF66335B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaperListEntry.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPaperListEntry 

class CDlgPaperListEntry : public CDialog
{
// Konstruktion
public:
	CDlgPaperListEntry(CWnd* pParent = NULL);   // Standardkonstruktor

friend class CDlgPaperList;

// Dialogfelddaten
	//{{AFX_DATA(CDlgPaperListEntry)
	enum { IDD = IDD_PAPERLIST_ENTRY };
	CString	m_strComment;
	long	m_nExtra;
	CString	m_strName;
	int		m_nNumSheets;
	long	m_nQuantity;
	long	m_nTotal;
	CString	m_strFormat;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPaperListEntry)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	BOOL Position();


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPaperListEntry)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPAPERLISTENTRY_H__77FFE49F_7785_40A8_A5B5_18EEF66335B5__INCLUDED_
