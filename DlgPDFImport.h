#pragma once

#include "DlgPDFLibStatus.h"
#include "afxcmn.h"
#include "DlgPDFImportSettings.h"


typedef struct
{
	CPageSource		m_pageSource;
	int				m_nNumPages;
	int				m_nProductType;		// 0 = bound product, 1 = flat product
	int				m_nSides;
}PAGESOURCE_PROPS;


// CDlgPDFImport-Dialogfeld

class CDlgPDFImport : public CDialog
{
	DECLARE_DYNAMIC(CDlgPDFImport)

public:
	CDlgPDFImport(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgPDFImport();

// Dialogfelddaten
	enum { IDD = IDD_PDF_IMPORT };

public:
	CImageList*									 m_pImageList;
	CArray <PAGESOURCE_PROPS, PAGESOURCE_PROPS&> m_pageSourceList;
	CDlgPDFImportSettings						 m_dlgPDFImportSettings;

public:
	void PDFEngineDataExchange(CPageSource* pNewObjectSource);
	int  ImportPDFFlats(CPageSource* pNewObjectSource, int nNumPDFPages, int nSides);
	int  ImportPDFBound(CPageSource* pNewObjectSource);


protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedPdfimportOpen();
	CListCtrl m_pageSourcesCtrl;
	afx_msg void OnBnClickedPdfimportAcrobat();
	afx_msg void OnBnClickedPdfimportBoundproduct();
	afx_msg void OnBnClickedPdfimportFlatproduct();
	afx_msg void OnBnClickedPdfimportSinglesided();
	afx_msg void OnBnClickedPdfimportDoublesided();
	afx_msg void OnBnClickedPdfimportSettings();
};
