// DlgProductionPunchingDetails.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionPunchingDetails.h"


// CPunchToolDirList

CString	CPunchToolDirList::FindTool(CString strPunchToolName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_nAttribute == CPunchToolDirItem::IsPunchTool)
		{
			if (strPunchToolName == GetAt(i).m_strFileName)
				return GetAt(i).m_strFileName;
		}
	}

	return _T("");
}


// CDlgProductionPunchingDetails-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionPunchingDetails, CDialog)

CDlgProductionPunchingDetails::CDlgProductionPunchingDetails(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionPunchingDetails::IDD, pParent)
	, m_nPunchToolNX(1)
	, m_nPunchToolNY(1)
	, m_nPunchToolNXOptions(0)
	, m_nPunchToolNYOptions(0)
	, m_fPunchToolStepX(0)
	, m_fPunchToolStepY(0)
	, m_fPunchToolOffsetRows(0)
	, m_fPunchToolOffsetCols(0)
	, m_bPunchToolRotateRows(FALSE)
	, m_bPunchToolRotateCols(FALSE)
	, m_nPunchToolOffsetRowsOddEven(0)
	, m_nPunchToolOffsetColsOddEven(0)
	, m_nPunchToolRotateRowsOddEven(0)
	, m_nPunchToolRotateColsOddEven(0)
	, m_bShowPreviews(FALSE)
	, m_strPunchToolName(_T(""))
	, m_nFlatProductIndex(-1)
{
	m_pPunchToolImageList  = new CImageList();
	m_boldFont.CreateFont(13, 0, 0, 0, FW_BOLD,	 FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CDlgProductionPunchingDetails::~CDlgProductionPunchingDetails()
{
	delete m_pPunchToolImageList;
	m_boldFont.DeleteObject();
}

void CDlgProductionPunchingDetails::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (nIDCtl == IDC_PUNCHTOOL_PREVIEWFRAME)
	{
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		CRect rcItem = lpDrawItemStruct->rcItem;

		pDC->FillSolidRect(rcItem, WHITE);

		rcItem.DeflateRect(10, 10);

		CImpManDoc*	   pDoc			 = CImpManDoc::GetDoc();
		POSITION	   pos			 = (pDoc) ? (pDoc->m_PageTemplateList.FindIndex(m_nFlatProductIndex * 2, pDoc->GetFlatProductsIndex())) : NULL;
		CPageTemplate* pPageTemplate = (pos) ? &pDoc->m_PageTemplateList.GetAt(pos) : NULL;

		m_punchingTool.DrawPreview(pDC, rcItem, &m_flatProduct, pPageTemplate, m_bShowPreviews);
	}
	else
		CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CDlgProductionPunchingDetails::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PT_BROWSER_LIST, m_punchToolBrowser);
	DDX_Text(pDX, IDC_PT_NAME_EDIT, m_strPunchToolName);
	DDX_Control(pDX, IDC_PUNCHTOOL_NXSPIN, m_punchToolNXSpin);
	DDX_Control(pDX, IDC_PUNCHTOOL_NYSPIN, m_punchToolNYSpin);
	DDX_Text(pDX, IDC_PUNCHTOOL_NX, m_nPunchToolNX);
	DDV_MinMaxUInt(pDX, m_nPunchToolNX, 1, 100);
	DDX_Text(pDX, IDC_PUNCHTOOL_NY, m_nPunchToolNY);
	DDV_MinMaxUInt(pDX, m_nPunchToolNY, 1, 100);
	DDX_CBIndex(pDX, IDC_PUNCHTOOL_NX_OPTIONS, m_nPunchToolNXOptions);
	DDX_CBIndex(pDX, IDC_PUNCHTOOL_NY_OPTIONS, m_nPunchToolNYOptions);
	DDX_Measure(pDX, IDC_PUNCHTOOL_STEPX, m_fPunchToolStepX);
	DDX_Measure(pDX, IDC_PUNCHTOOL_STEPY, m_fPunchToolStepY);
	DDX_Measure(pDX, IDC_PUNCHTOOL_OFFSET_ROWS, m_fPunchToolOffsetRows);
	DDX_Measure(pDX, IDC_PUNCHTOOL_OFFSET_COLS, m_fPunchToolOffsetCols);
	DDX_Check(pDX, IDC_PUNCHTOOL_ROTATE_ROWS, m_bPunchToolRotateRows);
	DDX_Check(pDX, IDC_PUNCHTOOL_ROTATE_COLS, m_bPunchToolRotateCols);
	DDX_Measure(pDX, IDC_PUNCHTOOL_GUTTER_X, m_fPunchToolGutterX);
	DDX_Measure(pDX, IDC_PUNCHTOOL_GUTTER_Y, m_fPunchToolGutterY);
	DDX_Check(pDX, IDC_PUNCHTOOL_SHOWPREVIEWS, m_bShowPreviews);
}

BEGIN_MESSAGE_MAP(CDlgProductionPunchingDetails, CDialog)
	ON_WM_DRAWITEM()
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_PT_BROWSER_LIST, OnNMCustomdrawPtBrowserlist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PT_BROWSER_LIST, OnLvnItemchangedPtBrowserlist)
	ON_NOTIFY(NM_DBLCLK, IDC_PT_BROWSER_LIST, OnNMDblclkPtBrowserlist)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_PT_BROWSER_LIST, OnEndlabeleditPunchtoolBrowser)
	ON_BN_CLICKED(IDC_PT_FOLDER_UP, OnBnClickedPtFolderUp)
	ON_UPDATE_COMMAND_UI(IDC_PT_FOLDER_UP,  OnUpdatePtFolderUp)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PUNCHTOOL_NXSPIN, &CDlgProductionPunchingDetails::OnDeltaposPunchtoolNxspin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PUNCHTOOL_NYSPIN, &CDlgProductionPunchingDetails::OnDeltaposPunchtoolNyspin)
	ON_CBN_SELCHANGE(IDC_PUNCHTOOL_NX_OPTIONS, &CDlgProductionPunchingDetails::OnCbnSelchangePunchtoolNxOptions)
	ON_CBN_SELCHANGE(IDC_PUNCHTOOL_NY_OPTIONS, &CDlgProductionPunchingDetails::OnCbnSelchangePunchtoolNyOptions)
	ON_EN_KILLFOCUS(IDC_PT_NAME_EDIT, &CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolNameEdit)
	ON_EN_KILLFOCUS(IDC_PUNCHTOOL_STEPX, &CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolStepx)
	ON_EN_KILLFOCUS(IDC_PUNCHTOOL_STEPY, &CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolStepy)
	ON_EN_KILLFOCUS(IDC_PUNCHTOOL_NX, &CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolNx)
	ON_EN_KILLFOCUS(IDC_PUNCHTOOL_NY, &CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolNy)
	ON_BN_CLICKED(IDOK, &CDlgProductionPunchingDetails::OnBnClickedOk)
	ON_BN_CLICKED(IDC_PUNCHTOOL_ROTATE_ROWS, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolRotateRows)
	ON_BN_CLICKED(IDC_PUNCHTOOL_ROTATE_COLS, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolRotateCols)
	ON_BN_CLICKED(IDC_PUNCHTOOL_OFFSET_ROWS_ODD, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolOffsetRowsOdd)
	ON_BN_CLICKED(IDC_PUNCHTOOL_OFFSET_COLS_ODD, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolOffsetColsOdd)
	ON_BN_CLICKED(IDC_PUNCHTOOL_ROTATE_ROWS_ODD, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolRotateRowsOdd)
	ON_BN_CLICKED(IDC_PUNCHTOOL_ROTATE_COLS_ODD, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolRotateColsOdd)
	ON_EN_KILLFOCUS(IDC_PUNCHTOOL_OFFSET_ROWS, &CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolOffsetRows)
	ON_EN_KILLFOCUS(IDC_PUNCHTOOL_OFFSET_COLS, &CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolOffsetCols)
	ON_BN_CLICKED(IDC_PUNCHTOOL_SHOWPREVIEWS, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolShowpreviews)
	ON_BN_CLICKED(IDC_PUNCHTOOL_ADD, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolAdd)
	ON_BN_CLICKED(IDC_PUNCHTOOL_REMOVE, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolRemove)
	ON_BN_CLICKED(IDC_PUNCHTOOL_NEWFOLDER, &CDlgProductionPunchingDetails::OnBnClickedPunchtoolNewfolder)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CDlgProductionPunchingDetails-Meldungshandler

BOOL CDlgProductionPunchingDetails::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_punchToolNXSpin.SetRange(0, 100);
	m_punchToolNYSpin.SetRange(0, 100);

	CBitmap bitmap;
	if ( ! m_pPunchToolImageList->m_hImageList)
	{
		m_pPunchToolImageList->Create(20, 20, ILC_COLOR8, 1, 0);	// List not to grow
		bitmap.LoadBitmap(IDB_BKSCHEMELIST);
		m_pPunchToolImageList->Add(&bitmap, BLACK);
		m_punchToolBrowser.SetImageList(m_pPunchToolImageList, LVSIL_SMALL);bitmap.DeleteObject();
	}

	((CButton*)GetDlgItem(IDC_PT_FOLDER_UP))->SetIcon(theApp.LoadIcon(IDI_FOLDER_UP));
	GetDlgItem(IDC_PT_FOLDER_UP)->EnableWindow(FALSE);

	InitPunchTools();

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProductionPunchingDetails::InitData()
{
	m_punchingTool.Load(m_strPunchToolName, m_strCurrentPunchToolFolder);

	m_strPunchToolName				= m_punchingTool.m_strToolName;
	m_nPunchToolNX					= m_punchingTool.m_nPunchesX;
	m_nPunchToolNY					= m_punchingTool.m_nPunchesY;
	m_nPunchToolNXOptions			= m_punchingTool.m_nNXOptions;
	m_nPunchToolNYOptions			= m_punchingTool.m_nNYOptions;
	m_fPunchToolStepX				= m_punchingTool.m_fStepX;
	m_fPunchToolStepY				= m_punchingTool.m_fStepY;
	m_fPunchToolGutterX				= m_punchingTool.m_fGutterX;
	m_fPunchToolGutterY				= m_punchingTool.m_fGutterY;
	m_fPunchToolOffsetRows 			= m_punchingTool.m_fOffsetRows;
	m_fPunchToolOffsetCols 			= m_punchingTool.m_fOffsetCols;
	m_bPunchToolRotateRows 			= m_punchingTool.m_bRotateRows;
	m_bPunchToolRotateCols 			= m_punchingTool.m_bRotateCols;
	m_nPunchToolOffsetRowsOddEven 	= m_punchingTool.m_nOffsetRowsOddEven;
	m_nPunchToolOffsetColsOddEven 	= m_punchingTool.m_nOffsetColsOddEven;
	m_nPunchToolRotateRowsOddEven 	= m_punchingTool.m_nRotateRowsOddEven;
	m_nPunchToolRotateColsOddEven 	= m_punchingTool.m_nRotateColsOddEven;

	UpdateData(FALSE);

	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::SaveData()
{
	UpdateData(TRUE);

	m_punchingTool.m_strToolName		= m_strPunchToolName;
	m_punchingTool.m_nPunchesX			= m_nPunchToolNX;		
	m_punchingTool.m_nPunchesY			= m_nPunchToolNY;		
	m_punchingTool.m_nNXOptions			= m_nPunchToolNXOptions;
	m_punchingTool.m_nNYOptions			= m_nPunchToolNYOptions;
	m_punchingTool.m_fStepX				= m_fPunchToolStepX;
	m_punchingTool.m_fStepY				= m_fPunchToolStepY;
	m_punchingTool.m_fGutterX			= m_fPunchToolGutterX;
	m_punchingTool.m_fGutterY			= m_fPunchToolGutterY;
	m_punchingTool.m_fOffsetRows 		= m_fPunchToolOffsetRows;
	m_punchingTool.m_fOffsetCols 		= m_fPunchToolOffsetCols;
	m_punchingTool.m_bRotateRows 		= m_bPunchToolRotateRows;
	m_punchingTool.m_bRotateCols 		= m_bPunchToolRotateCols;
	m_punchingTool.m_nOffsetRowsOddEven = m_nPunchToolOffsetRowsOddEven;
	m_punchingTool.m_nOffsetColsOddEven = m_nPunchToolOffsetColsOddEven;
	m_punchingTool.m_nRotateRowsOddEven = m_nPunchToolRotateRowsOddEven;
	m_punchingTool.m_nRotateColsOddEven = m_nPunchToolRotateColsOddEven;

	m_punchingTool.Save(m_strPunchToolName, m_strCurrentPunchToolFolder);
}

void CDlgProductionPunchingDetails::InitPunchTools()
{
	CString strStartFolder      = CString(theApp.GetDataFolder()) + _T("\\PunchTools");
	m_strCurrentPunchToolFolder = CPunchingTool::FindFolder(strStartFolder, m_strPunchToolName);
	if (m_strCurrentPunchToolFolder.IsEmpty())
		m_strCurrentPunchToolFolder = strStartFolder;

	InitPunchToolBrowser(m_strPunchToolName);
}

int CDlgProductionPunchingDetails::InitPunchToolBrowser(CString strSelectPunchTool)
{
	WIN32_FIND_DATA findData;
	CString			strFilename = m_strCurrentPunchToolFolder + _T("\\*.*");
	HANDLE			hfileSearch = FindFirstFile(strFilename, &findData);

	m_punchToolDirList.RemoveAll();
	if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		CString	strBuffer = findData.cFileName;
		do 
		{
			strBuffer = findData.cFileName;
			if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
			{
				if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					CPunchToolDirItem dirItem(strBuffer, CPunchToolDirItem::IsFolder);
					m_punchToolDirList.Add(dirItem);
				}
				else 
				{
					CString strExt = strBuffer.Right(4);
					strExt.MakeLower();
					if (strExt == _T(".ptl"))
					{
						strBuffer = strBuffer.SpanExcluding(_T("."));
						CPunchToolDirItem dirItem(strBuffer, CPunchToolDirItem::IsPunchTool);
						m_punchToolDirList.Add(dirItem);
					}
				}
			}
		}
		while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);

	m_punchToolDirList.Sort();

	POSITION pos  = m_punchToolBrowser.GetFirstSelectedItemPosition();
	int		 nSel = (pos) ? m_punchToolBrowser.GetNextSelectedItem(pos) : -1;
	nSel = min(nSel, m_punchToolDirList.GetSize() - 1);

	m_punchToolBrowser.DeleteAllItems();

	LV_ITEM item;
	for (int i = 0; i < m_punchToolDirList.GetSize(); i++)
	{
		BOOL bSelect = FALSE;
		if (strSelectPunchTool.IsEmpty())
			bSelect = (i == nSel) ? TRUE : FALSE;
		else
			if (strSelectPunchTool == m_punchToolDirList[i].m_strFileName)
			{
				bSelect = TRUE;
				nSel = i;
			}

		item.mask		= LVIF_TEXT | LVIF_IMAGE | LVIF_STATE | LVIF_PARAM;
		item.cchTextMax = MAX_TEXT;
		item.iItem		= i; 
		item.iSubItem	= 0;
		item.pszText	= m_punchToolDirList[i].m_strFileName.GetBuffer(MAX_TEXT);
		item.iImage		= 0;
		item.state		= (bSelect) ? LVIS_SELECTED : 0;
		item.stateMask	= LVIS_SELECTED;
		item.lParam		= m_punchToolDirList[i].m_nAttribute;// == CPunchToolDirItem::IsFolder) ? 0 : 1;
		m_punchToolBrowser.InsertItem(&item);
	}

	m_punchToolBrowser.EnsureVisible(nSel, FALSE);

	CString strPunchToolFolder = CString(theApp.GetDataFolder()) + _T("\\PunchTools");
	CRect headerRect;
	GetDlgItem(IDC_PT_BROWSER_HEADER)->GetWindowRect(headerRect);
	CString strRelativePath;
	PathRelativePathTo(strRelativePath.GetBuffer(MAX_PATH), (LPCTSTR)CString(strPunchToolFolder), FILE_ATTRIBUTE_DIRECTORY, (LPCTSTR)m_strCurrentPunchToolFolder, FILE_ATTRIBUTE_DIRECTORY);
	strRelativePath.ReleaseBuffer();
	CString strOut;
	strOut.Format(_T("  %s"), strRelativePath);
	CClientDC clientDC(this);
	PathCompactPath(clientDC.m_hDC, strOut.GetBuffer(MAX_PATH), headerRect.Width());

	GetDlgItem(IDC_PT_BROWSER_HEADER)->SetWindowText(strOut);

	return nSel;
}

void CDlgProductionPunchingDetails::OnNMCustomdrawPtBrowserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW	 pNMCD	 = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			CString strItem;
			ListView_GetItemText(hWndListCtrl, pNMCD->dwItemSpec, 0, strItem.GetBuffer(MAX_TEXT), MAX_TEXT);
			strItem.ReleaseBuffer();

			CRect rcLabel;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			if (rcLabel.IsRectEmpty())
				break;

			CImage img;
			if (lvitem.lParam == CPunchToolDirItem::IsPunchTool)
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_PUNCHING));				
			else
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_FOLDER));				

			if ( ! img.IsNull())
				img.TransparentBlt(pDC->m_hDC, rcItem.left + 2, rcItem.top + 5, img.GetWidth(), img.GetHeight(), WHITE);
			rcLabel.left += 5;

			COLORREF crOldTextColor = pDC->SetTextColor(BLACK);//DARKBLUE);

			pDC->DrawText(strItem, -1, rcLabel, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);

			pDC->SetTextColor(crOldTextColor);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}

void CDlgProductionPunchingDetails::OnLvnItemchangedPtBrowserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	if (pNMLV->iItem < 0)
		return;
	if (pNMLV->uNewState == pNMLV->uOldState)
		return;
	if ( ! (pNMLV->uNewState & LVIS_SELECTED) )
		return;
	if (pNMLV->lParam == 0)		// folder
		return;

	m_strPunchToolName = m_punchToolBrowser.GetItemText(pNMLV->iItem, pNMLV->iSubItem);

	InitData();
}

void CDlgProductionPunchingDetails::OnNMDblclkPtBrowserlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	if ( (pNMItemActivate->iItem >= 0) && (pNMItemActivate->iItem < m_punchToolDirList.GetSize()) )
	{
		CPunchToolDirItem dirItem = m_punchToolDirList[pNMItemActivate->iItem];
		if (dirItem.m_nAttribute == CPunchToolDirItem::IsFolder)
		{
			m_strCurrentPunchToolFolder += _T("\\") + dirItem.m_strFileName;
			InitPunchToolBrowser();

			GetDlgItem(IDC_PT_FOLDER_UP)->EnableWindow(TRUE);
		}
	}

	*pResult = 0;
}

void CDlgProductionPunchingDetails::OnEndlabeleditPunchtoolBrowser(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	
	*pResult = 0;

	if ( (pDispInfo->item.iItem < 0) || (pDispInfo->item.iItem >= m_punchToolDirList.GetSize()) )
		return;

	CString strNewName;
	m_punchToolBrowser.GetEditControl()->GetWindowText(strNewName);

	BOOL bDoSave = FALSE;
	CString strOldPath, strNewPath;
	if (m_punchToolDirList[pDispInfo->item.iItem].m_nAttribute == CPunchToolDirItem::IsPunchTool)
	{
		strOldPath.Format(_T("%s\\%s.ptl"), m_strCurrentPunchToolFolder, m_punchToolDirList[pDispInfo->item.iItem].m_strFileName);
		strNewPath.Format(_T("%s\\%s.ptl"), m_strCurrentPunchToolFolder, strNewName);
		bDoSave = TRUE;
	}
	else
	{
		strOldPath.Format(_T("%s\\%s"), m_strCurrentPunchToolFolder, m_punchToolDirList[pDispInfo->item.iItem].m_strFileName);
		strNewPath.Format(_T("%s\\%s"), m_strCurrentPunchToolFolder, strNewName);
	}
	_trename(strOldPath, strNewPath);

	InitPunchToolBrowser(strNewName);

	if (bDoSave)
	{
		m_strPunchToolName = strNewName;
		UpdateData(FALSE);
		SaveData();
	}
}

void CDlgProductionPunchingDetails::OnBnClickedPtFolderUp()
{
	CString strPunchToolFolder = CString(theApp.GetDataFolder()) + _T("\\PunchTools");
	CString strShortCurrentFolder, strShortInitialFolder;
	GetShortPathName(m_strCurrentPunchToolFolder,	strShortCurrentFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortCurrentFolder.ReleaseBuffer();
	GetShortPathName(strPunchToolFolder,			strShortInitialFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortInitialFolder.ReleaseBuffer();
	strShortCurrentFolder.MakeUpper();
	strShortInitialFolder.MakeUpper();
	if (strShortCurrentFolder == strShortInitialFolder)
		return;

	int nIndex = m_strCurrentPunchToolFolder.ReverseFind(_T('\\'));	
	if (nIndex == -1)
		return;

	m_strCurrentPunchToolFolder = m_strCurrentPunchToolFolder.Left(nIndex);

	GetShortPathName(m_strCurrentPunchToolFolder, strShortCurrentFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortCurrentFolder.ReleaseBuffer();
	strShortCurrentFolder.MakeUpper();
	if (strShortCurrentFolder == strShortInitialFolder)
		GetDlgItem(IDC_PT_FOLDER_UP)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PT_FOLDER_UP)->EnableWindow(TRUE);

	InitPunchToolBrowser();
}

void CDlgProductionPunchingDetails::OnUpdatePtFolderUp(CCmdUI* pCmdUI)
{
	CString strPunchToolFolder = CString(theApp.GetDataFolder()) + _T("\\PunchTools");
	CString strShortCurrentFolder, strShortInitialFolder;
	GetShortPathName(m_strCurrentPunchToolFolder,	strShortCurrentFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortCurrentFolder.ReleaseBuffer();
	GetShortPathName(strPunchToolFolder,			strShortInitialFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortInitialFolder.ReleaseBuffer();
	strShortCurrentFolder.MakeUpper();
	strShortInitialFolder.MakeUpper();

	if (strShortCurrentFolder == strShortInitialFolder)
		GetDlgItem(IDC_PT_FOLDER_UP)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PT_FOLDER_UP)->EnableWindow(TRUE);
}

void CDlgProductionPunchingDetails::OnDeltaposPunchtoolNxspin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	UpdateData(TRUE);

	m_nPunchToolNX += pNMUpDown->iDelta;
	m_nPunchToolNX = max(m_nPunchToolNX, 1);
	m_nPunchToolNX = min(m_nPunchToolNX, 100);

	UpdateData(FALSE);

	SaveData();

	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnDeltaposPunchtoolNyspin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	UpdateData(TRUE);

	m_nPunchToolNY += pNMUpDown->iDelta;
	m_nPunchToolNY = max(m_nPunchToolNY, 1);
	m_nPunchToolNY = min(m_nPunchToolNY, 100);

	UpdateData(FALSE);
	SaveData();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnCbnSelchangePunchtoolNxOptions()
{
	UpdateData(TRUE);
	SaveData();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnCbnSelchangePunchtoolNyOptions()
{
	UpdateData(TRUE);
	SaveData();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

BOOL CDlgProductionPunchingDetails::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN)
		{
			if (GetFocus() == GetDlgItem(IDC_PUNCHTOOL_STEPX))
			{
				OnEnKillfocusPunchtoolStepx();
				pMsg->wParam = VK_TAB;
			}
			else
				if (GetFocus() == GetDlgItem(IDC_PUNCHTOOL_STEPY))
				{
					OnEnKillfocusPunchtoolStepy();
					pMsg->wParam = VK_TAB;
				}
				else
					if (GetFocus() == GetDlgItem(IDC_PUNCHTOOL_NX))
					{
						OnEnKillfocusPunchtoolNx();
						pMsg->wParam = VK_TAB;
					}
					else
						if (GetFocus() == GetDlgItem(IDC_PUNCHTOOL_NY))
						{
							OnEnKillfocusPunchtoolNy();
							pMsg->wParam = VK_TAB;
						}
						else
							if (GetFocus() == GetDlgItem(IDC_PUNCHTOOL_OFFSET_ROWS))
							{
								OnEnKillfocusPunchtoolOffsetRows();
								pMsg->wParam = VK_TAB;
							}
							else
								if (GetFocus() == GetDlgItem(IDC_PUNCHTOOL_OFFSET_COLS))
								{
									OnEnKillfocusPunchtoolOffsetCols();
									pMsg->wParam = VK_TAB;
								}
								else
									if (GetFocus() == GetDlgItem(IDC_PUNCHTOOL_GUTTER_X))
										pMsg->wParam = VK_TAB;
									else
										if (GetFocus() == GetDlgItem(IDC_PUNCHTOOL_GUTTER_Y))
											pMsg->wParam = VK_TAB;

		}
	}

	UpdateDialogControls(this, FALSE);

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolNameEdit()
{
}

void CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolStepx()
{
	float fOldValue = m_fPunchToolStepX;
	SaveData();
	if (fOldValue != m_fPunchToolStepX)
	{
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
	}
}

void CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolStepy()
{
	float fOldValue = m_fPunchToolStepY;
	SaveData();
	if (fOldValue != m_fPunchToolStepY)
	{
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
	}
}

void CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolNx()
{
	int nOldValue = m_nPunchToolNX;
	SaveData();
	if (nOldValue != m_nPunchToolNX)
	{
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
	}
}

void CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolNy()
{
	int nOldValue = m_nPunchToolNY;
	SaveData();
	if (nOldValue != m_nPunchToolNY)
	{
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
	}
}

void CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolOffsetRows()
{
	float fOldValue = m_fPunchToolOffsetRows;
	SaveData();
	if (fOldValue != m_fPunchToolOffsetRows)
	{
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
	}
}

void CDlgProductionPunchingDetails::OnEnKillfocusPunchtoolOffsetCols()
{
	float fOldValue = m_fPunchToolOffsetCols;
	SaveData();
	if (fOldValue != m_fPunchToolOffsetCols)
	{
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
		GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
	}
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolRotateRows()
{
	SaveData();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolRotateCols()
{
	SaveData();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolOffsetRowsOdd()
{
	m_nPunchToolOffsetRowsOddEven = (m_nPunchToolOffsetRowsOddEven == 0) ? 1 : 0;
	SaveData();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolOffsetColsOdd()
{
	m_nPunchToolOffsetColsOddEven = (m_nPunchToolOffsetColsOddEven == 0) ? 1 : 0;
	SaveData();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolRotateRowsOdd()
{
	m_nPunchToolRotateRowsOddEven = (m_nPunchToolRotateRowsOddEven == 0) ? 1 : 0;
	SaveData();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolRotateColsOdd()
{
	m_nPunchToolRotateColsOddEven = (m_nPunchToolRotateColsOddEven == 0) ? 1 : 0;
	SaveData();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolAdd()
{
	CString strNewPunchToolName, strPunchToolName, strFullpath;
	strNewPunchToolName.LoadString(IDS_NEW_PUNCHTOOL);
	strPunchToolName = strNewPunchToolName;
	strFullpath.Format(_T("%s\\%s.ptl"), m_strCurrentPunchToolFolder, strPunchToolName);
	int i;
	for (i = 2; i < 100; i++)
	{
		CFileStatus fileStatus;
		if (CFile::GetStatus(strFullpath, fileStatus))	// file exists
			strPunchToolName.Format(_T("%s (%d)"), strNewPunchToolName, i);
		else
			break;
		strFullpath.Format(_T("%s\\%s.ptl"), m_strCurrentPunchToolFolder, strPunchToolName);
	}
	if (i >= 100)	// max. 100 tries
		return;

	m_strPunchToolName = strPunchToolName;
	UpdateData(FALSE);
	SaveData();		

	int nIndex = InitPunchToolBrowser(strPunchToolName);
	if (nIndex >= 0)
	{
		m_punchToolBrowser.SetFocus();
		m_punchToolBrowser.EditLabel(nIndex);
	}
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolRemove()
{
	BOOL bDoPrompt = TRUE;
	if ( ! m_punchToolBrowser.GetSelectedCount())
		return;
	else
		if (m_punchToolBrowser.GetSelectedCount() > 1)
		{
			if (AfxMessageBox(IDS_DELETE_SELECTIONS_WARNING, MB_YESNO) == IDNO)
			{
				m_punchToolBrowser.SetFocus();
				return;
			}
			else
				bDoPrompt = FALSE;
		}

	CString strMsg;
	for (int i = 0; i < m_punchToolDirList.GetSize(); i++)
	{
		if (m_punchToolBrowser.GetItemState(i, LVIS_SELECTED))
			if (m_punchToolDirList[i].m_nAttribute == CPunchToolDirItem::IsPunchTool)
			{
				if (bDoPrompt)
				{
					strMsg.Format(IDS_DELETE_PUNCHTOOL_WARNING, m_punchToolDirList[i].m_strFileName);
					if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
					{
						m_punchToolBrowser.SetFocus();
						return;
					}
				}
				CString strFileToRemove;
				strFileToRemove.Format(_T("%s\\%s.ptl"), m_strCurrentPunchToolFolder, m_punchToolDirList[i].m_strFileName);
				DeleteFile(strFileToRemove);
			}
			else
				if (m_punchToolDirList[i].m_nAttribute == CPunchToolDirItem::IsFolder)
				{
					if (bDoPrompt)
					{
						strMsg.Format(IDS_DELETE_MARKSET_FOLDER_WARNING, m_punchToolDirList[i].m_strFileName);
						if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
						{
							m_punchToolBrowser.SetFocus();
							return;
						}
					}
					CString strFolderToRemove;
					strFolderToRemove.Format(_T("%s\\%s"), m_strCurrentPunchToolFolder, m_punchToolDirList[i].m_strFileName);
					DeleteFolder(strFolderToRemove);
				}
	}

	InitPunchToolBrowser();
}

void CDlgProductionPunchingDetails::DeleteFolder(const CString& strFolderToRemove) 
{
	WIN32_FIND_DATA findData;
	BOOL			bFilesExist = FALSE;	

	CString strFilename = strFolderToRemove + _T("\\*.*");
	HANDLE  hfileSearch = FindFirstFile(strFilename, &findData);

	BOOL bHasSubDir = FALSE;
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		CString	strBuffer	= findData.cFileName;
		m_strPunchToolName  = strBuffer.SpanExcluding(_T("."));
		bFilesExist			= TRUE;
		do 
		{
			strBuffer = findData.cFileName;
			if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
			{
				if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					bHasSubDir = TRUE;
				else 
					DeleteFile(strFolderToRemove + _T("\\") + strBuffer);
			}
		}
		while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);

	RemoveDirectory(strFolderToRemove);
	if (bHasSubDir)
		AfxMessageBox(IDS_DELETEFOLDER_HASSUBDIRS, MB_OK);
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolNewfolder()
{
	CString strNewFolderName, strFolderName, strFullpath;
	strNewFolderName.LoadString(IDS_NEW_FOLDER);
	strFolderName = strNewFolderName;
	strFullpath.Format(_T("%s\\%s"), m_strCurrentPunchToolFolder, strFolderName);
	int i;
	for (i = 2; i < 100; i++)
	{
		CFileStatus fileStatus;
		if (CFile::GetStatus(strFullpath, fileStatus))	// file exists
			strFolderName.Format(_T("%s (%d)"), strNewFolderName, i);
		else
			break;
		strFullpath.Format(_T("%s\\%s"), m_strCurrentPunchToolFolder, strFolderName);
	}
	if (i >= 100)	// max. 100 tries
		return;

	CreateDirectory(strFullpath, NULL);

	int nIndex = InitPunchToolBrowser();
	if (nIndex >= 0)
	{
		m_punchToolBrowser.SetFocus();
		m_punchToolBrowser.EditLabel(nIndex);
	}
}

void CDlgProductionPunchingDetails::OnBnClickedPunchtoolShowpreviews()
{
	UpdateData(TRUE);
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->Invalidate();
	GetDlgItem(IDC_PUNCHTOOL_PREVIEWFRAME)->UpdateWindow();
}

void CDlgProductionPunchingDetails::OnBnClickedOk()
{
	SaveData();

	OnOK();
}

HBRUSH CDlgProductionPunchingDetails::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if(nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_PT_NAME_EDIT:
					pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
					pDC->SelectObject(&m_boldFont);
					break;

		default:	break;
		}
    }


	return hbr;
}
