// DlgLayoutData.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "DlgLayoutData.h"
#include "LayoutDataWindow.h"
#include "PrintSheetNavigationView.h"
#include "PrintSheetViewStrippingData.h"
#include "PrintSheetPlanningView.h"
#include "PrintSheetView.h"
#include "PageListView.h"
#include "PrintSheetComponentsView.h"
#include "NewPrintSheetFrame.h"
#include "GraphicComponent.h"
#include "PrintComponentsView.h"


// CDlgLayoutData-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgLayoutData, CDialog)

CDlgLayoutData::CDlgLayoutData(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgLayoutData::IDD, pParent)
{
	m_bLocked = FALSE;
	m_ptInitialPos = CPoint(0,0);
}

CDlgLayoutData::~CDlgLayoutData()
{
}

BOOL CDlgLayoutData::OnEraseBkgnd(CDC* pDC)
{
	CRect rcFrame;
	GetClientRect(rcFrame);

	CGraphicComponent gc;
	gc.DrawPopupWindowBackground(pDC, rcFrame);

	return TRUE;
}

void CDlgLayoutData::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgLayoutData)
	DDX_Text(pDX, IDC_LAYOUTDATA_NAME_EDIT, m_strLayoutName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgLayoutData, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgLayoutData-Meldungshandler

int CDlgLayoutData::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

BOOL CDlgLayoutData::Create(UINT nIDTemplate, CWnd* pParentWnd)
{
	m_pParentWnd = pParentWnd;
	return CDialog::Create(nIDTemplate, pParentWnd);
}

void CDlgLayoutData::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
		{
			if (m_hWnd)
			{
				m_bLocked = TRUE;
				EndModalLoop(IDCANCEL);
				DestroyWindow();
			}
		}
	}
	else
		m_bLocked = FALSE;
}

CPrintingProfile& CDlgLayoutData::GetPrintingProfile()
{
	static CPrintingProfile nullPrintingProfile;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
	{
		CLayoutDataWindow*	pParent	= (CLayoutDataWindow*)m_pParentWnd;
		return pParent->GetPrintingProfile();
	}

	return nullPrintingProfile;
}

CBoundProductPart& CDlgLayoutData::GetBoundProductPart()
{
	static CBoundProductPart nullProductPart;

	CLayoutDataWindow*		pParent		= (CLayoutDataWindow*)m_pParentWnd;
	CPrintSheet*			pPrintSheet = pParent->GetPrintSheet();
	return (pPrintSheet) ? *pPrintSheet->GetFirstBoundProductPart() : nullProductPart;
}

CPrintSheet* CDlgLayoutData::GetActPrintSheet()
{
	CLayoutDataWindow*		pParent		= (CLayoutDataWindow*)m_pParentWnd;
	CPrintSheet*			pPrintSheet = (pParent) ? pParent->GetPrintSheet() : NULL;
	return pPrintSheet;
}

CLayout* CDlgLayoutData::GetActLayout()
{
	CLayoutDataWindow*		pParent		= (CLayoutDataWindow*)m_pParentWnd;
	CPrintSheet*			pPrintSheet = (pParent) ? pParent->GetPrintSheet() : NULL;
	return (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
}

CFoldSheet* CDlgLayoutData::GetActFoldSheet()
{
	CNewPrintSheetFrame* pFrame = NULL;
	CPrintSheetView*	 pView  = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pFrame = (CNewPrintSheetFrame*)pView->GetParentFrame();

	CPrintComponentsView* pPrintComponentsView = (pFrame) ? pFrame->GetPrintComponentsView() : NULL;
	if ( ! pPrintComponentsView)
		return NULL;

	CPrintComponent component = pPrintComponentsView->GetActComponent();
	return component.GetFoldSheet();
}

// CDlgLayoutData-Meldungshandler

BOOL CDlgLayoutData::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_ptInitialPos != CPoint(0,0))
		SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgLayoutData::InitData()
{
	CFoldSheet*  pFoldSheet  = GetActFoldSheet();
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	CLayout*	 pLayout	 = GetActLayout();
	m_strLayoutName = (pLayout) ? pLayout->m_strLayoutName : _T("");

	UpdateData(FALSE);
}

void CDlgLayoutData::SaveData()
{
	CString strOldName = m_strLayoutName;

	UpdateData(TRUE);

	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
	{
		m_strLayoutName = strOldName;
		UpdateData(FALSE);
		return;
	}

	if (strOldName == m_strLayoutName)
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	POSITION	pos	 = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if (&rLayout != pLayout)
			if (rLayout.m_strLayoutName == m_strLayoutName)
			{
				CString strMsg;
				AfxFormatString1(strMsg, IDS_RENAME_LAYOUT_NAME_EXISTING, m_strLayoutName); 
				AfxMessageBox(strMsg);
				m_strLayoutName = strOldName;
				UpdateData(FALSE);
				return;
			}
	}

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	int nRet = 0;
	if (pLayout->GetNumPrintSheetsBasedOn() > 1)
		nRet = AfxMessageBox(IDS_RENAME_LAYOUT_PROMPT, MB_YESNOCANCEL);
	else
		nRet = IDYES;

	switch (nRet)
	{
	case IDYES: 
		pLayout->m_strLayoutName = m_strLayoutName;
		break;
	case IDNO:
		{
			CPrintSheet* pPrintSheet = GetActPrintSheet();
			CPrintingProfile profile =  pLayout->GetPrintingProfile();
			CLayout			 layout	 = *pLayout;
			layout.m_strLayoutName   = m_strLayoutName;
			pDoc->m_PrintSheetList.m_Layouts.AddTail(layout);
			pDoc->m_printingProfiles.AddTail(profile);

			CLayout* pNewLayout		 = &pDoc->m_PrintSheetList.m_Layouts.GetTail();
			int		 nNewLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;

			pPrintSheet->LinkLayout(pNewLayout);
			pPrintSheet->m_nLayoutIndex = nNewLayoutIndex;

			pDoc->m_printGroupList.Add(CPrintGroup());

			pDoc->m_PrintSheetList.ReNumberPrintSheets();
		}
		break;
	case IDCANCEL: 
		m_strLayoutName = strOldName;
		UpdateData(FALSE);
		return;
	}
}

void CDlgLayoutData::OnBnClickedOk()
{
	m_bLocked = TRUE;
	SaveData();
	EndModalLoop(IDOK);
	DestroyWindow();
}
