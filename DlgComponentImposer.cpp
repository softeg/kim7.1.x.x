// DlgComponentImposer.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgComponentImposer.h"
#include "GraphicComponent.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "NewPrintSheetFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetViewStrippingData.h"
#include "PageListView.h"
#include "PrintComponentsView.h"
#include "PrintSheetComponentsView.h"
#include "PrintSheetNavigationView.h"


// CDlgComponentImposer-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgComponentImposer, CDialog)

CDlgComponentImposer::CDlgComponentImposer(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgComponentImposer::IDD, pParent)
	, m_nPlaceMethod(0)
{
	m_bkBrush.CreateSolidBrush(RGB(225,234,253));
	m_ptInitialPos			= CPoint(0,0);
	m_bLockDrawPreviews 	= FALSE;
	m_rcPlaceArea.SetRectEmpty();
	m_rcOldPlaceArea.SetRectEmpty();
	m_nRotation				= 0;
	m_nNup					= 0;
	m_bSheetGrainOn			= TRUE;
}

CDlgComponentImposer::~CDlgComponentImposer()
{
	m_bkBrush.DeleteObject();
	m_bSheetGrainOn = FALSE;
}

CPrintSheetView* CDlgComponentImposer::GetPrintSheetView()
{
	return (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
}

CPrintSheetViewStrippingData* CDlgComponentImposer::GetPrintSheetViewStrippingData()
{
	return (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
}

CPrintComponentsView* CDlgComponentImposer::GetPrintComponentsView()
{
	return (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
}

CNewPrintSheetFrame* CDlgComponentImposer::GetNewPrintSheetFrame()
{
	CPrintSheetView* pView = GetPrintSheetView();
	return (pView) ? (CNewPrintSheetFrame*)pView->GetParentFrame() : NULL;
}

BOOL CDlgComponentImposer::IsOpenDlgFoldSchemeSelector()
{
	if ( ! GetNewPrintSheetFrame())
		return NULL;

	return (GetNewPrintSheetFrame()->m_dlgFoldSchemeSelector.m_hWnd) ? TRUE : FALSE;
}

CDlgSelectFoldSchemePopup* CDlgComponentImposer::GetDlgFoldSchemeSelector()
{
	if ( ! GetNewPrintSheetFrame())
		return NULL;

	return &GetNewPrintSheetFrame()->m_dlgFoldSchemeSelector;
}

CPrintSheet* CDlgComponentImposer::GetActPrintSheet()
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return NULL;

	return pView->GetActPrintSheet();
}

CLayout* CDlgComponentImposer::GetActLayout()
{
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	return (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
}

CPrintingProfile* CDlgComponentImposer::GetActPrintingProfile()
{
	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
		return NULL;
	else
		return &pLayout->GetPrintingProfile();
}

CPrintComponent CDlgComponentImposer::GetActComponent()
{
	CNewPrintSheetFrame* pFrame = GetNewPrintSheetFrame();
	if ( ! pFrame)
		return CPrintComponent();

	CPrintComponentsView* pView = pFrame->GetPrintComponentsView();
	if (pView)
		return pView->GetActComponent();

	return CPrintComponent();
}

CFoldSheet* CDlgComponentImposer::GetActFoldSheet()
{
	if (IsOpenDlgFoldSchemeSelector())
	{
		CFoldSheet* pSelectedFoldSheet = GetDlgFoldSchemeSelector()->GetSelectedFoldSheet();
		if (pSelectedFoldSheet)
			return pSelectedFoldSheet;
	}

	return GetActComponent().GetFoldSheet();
}

int CDlgComponentImposer::GetActLayerIndex()
{
	CPrintComponent component = GetActComponent();
	if (component.IsNull())
		return 0;
	if (component.m_nType != CPrintComponent::FoldSheet)
		return 0;
	else
		return component.m_nFoldSheetLayerIndex;
}

CBoundProductPart* CDlgComponentImposer::GetActBoundProductPart()
{
	return GetActComponent().GetBoundProductPart();
}

BOOL CDlgComponentImposer::GetActProductPartIndex()
{
	return GetActComponent().m_nProductPartIndex;
}

CFlatProduct* CDlgComponentImposer::GetActFlatProduct()
{
	return GetActComponent().GetFlatProduct();
}

void CDlgComponentImposer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CI_NUP_COMBO, m_NUpCombo);
	DDX_CBIndex(pDX, IDC_CI_PLACE_METHOD, m_nPlaceMethod);
}

BOOL CDlgComponentImposer::OnEraseBkgnd(CDC* pDC)
{
	CRect rcFrame;
	GetClientRect(rcFrame);
	pDC->FillSolidRect(rcFrame, RGB(225,234,253));
	return TRUE;

	//return CDialog::OnEraseBkgnd(pDC);
}

HBRUSH CDlgComponentImposer::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

    if(nCtlColor == CTLCOLOR_STATIC)
    {
		pDC->SetBkMode(TRANSPARENT);
		hbr = (HBRUSH)m_bkBrush.GetSafeHandle();
    }

	return hbr;
}


BEGIN_MESSAGE_MAP(CDlgComponentImposer, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CI_ADD_PRODUCT, OnAddProduct)
	ON_BN_CLICKED(IDC_CI_NEXT_PRODUCT, OnNextProduct)
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_CI_PLACE_METHOD, OnCbnSelchangePlaceMethod)
	ON_CBN_SELCHANGE(IDC_CI_NUP_COMBO, OnCbnSelchangeNUpCombo)
	ON_BN_CLICKED(IDC_CI_FILLUP, OnBnClickedComponentsFillup)
	ON_BN_CLICKED(IDC_CI_OPTIMUM_FILLUP, OnBnClickedComponentsOptimumFillup)
	ON_CBN_SELCHANGE(IDC_COMPONENT_ROTATION_COMBO, OnSelchangeComponentRotationCombo)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CDlgComponentImposer-Meldungshandler


BOOL CDlgComponentImposer::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if (m_ptInitialPos != CPoint(0, 0))
		SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	m_nPlaceMethod = 0;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		pView->m_DisplayList.Invalidate();
		pView->m_DisplayList.DeselectAll(FALSE, TRUE);	// remove object selections
	}

	if (GetActComponent().IsNull())		// select first component on act sheet
	{
		CPrintSheet* pPrintSheet = GetActPrintSheet();
		if (pPrintSheet)
		{
			CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
			if (pPrintComponentsView)
				pPrintComponentsView->SetActComponent(pDoc->m_printComponentsList.FindPrintSheet(pPrintSheet));
		}
	}

	m_nextProductButton.SubclassDlgItem(IDC_CI_NEXT_PRODUCT, this);
	m_nextProductButton.SetBitmap(IDB_ARROW_DOWN, IDB_ARROW_DOWN, IDB_ARROW_DOWN_DISABLED, XCENTER);

	UpdateData(FALSE);

	InitData(FALSE);

	SwitchPrintSheetViewSheetGrain(TRUE);
	
	return FALSE;//TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgComponentImposer::InitData(BOOL bUpdatePrintSheetViewAll)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheet*	 pPrintSheet = GetActPrintSheet();
	CLayout*		 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout()	 : NULL;
	if ( ! pLayout)
		return;
	CFlatProduct* pFlatProduct = GetActFlatProduct();
	CFoldSheet*	  pFoldSheet   = GetActFoldSheet();

	int		nShow = SW_HIDE;
	CString strMinNum, strMaxNum;	
	strMinNum = _T(""); 
	strMaxNum = _T(""); 

	if (pFlatProduct)
	{
		m_nRotation	= 0;
		if ( (pFlatProduct->m_nPaperGrain != GRAIN_EITHER) && pLayout)
		{
			if ( (pFlatProduct->m_nPaperGrain == GRAIN_VERTICAL) && (pLayout->m_FrontSide.m_nPaperGrain == GRAIN_HORIZONTAL) )
				m_nRotation = 1;
			else
				if ( (pFlatProduct->m_nPaperGrain == GRAIN_HORIZONTAL) && (pLayout->m_FrontSide.m_nPaperGrain == GRAIN_VERTICAL) )
					m_nRotation = 1;
		}
		((CComboBox*)GetDlgItem(IDC_COMPONENT_ROTATION_COMBO))->SetCurSel(m_nRotation);
		InitComponentRotation();

		m_nPlaceMethod = 0;
	}
	else
	{
		if (pFoldSheet)
		{
			int nPaperGrain = (pFoldSheet->GetSpineOrientation() == VERTICAL) ? GRAIN_VERTICAL : GRAIN_HORIZONTAL;

			CPrintSheetView* pView		 = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
			CPrintSheet*	 pPrintSheet = (pView)		 ? pView->GetActPrintSheet()		   : NULL;
			CLayout*		 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout()			   : NULL;
			m_nRotation	= 0;
			if ( (nPaperGrain != GRAIN_EITHER) && pLayout)
			{
				if ( (nPaperGrain == GRAIN_VERTICAL) && (pLayout->m_FrontSide.m_nPaperGrain == GRAIN_HORIZONTAL) )
					m_nRotation = 1;
				else
					if ( (nPaperGrain == GRAIN_HORIZONTAL) && (pLayout->m_FrontSide.m_nPaperGrain == GRAIN_VERTICAL) )
						m_nRotation = 1;
			}
			((CComboBox*)GetDlgItem(IDC_COMPONENT_ROTATION_COMBO))->SetCurSel(m_nRotation);
			InitComponentRotation();
		}
	}

	UpdateData(FALSE);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();	// free areas do vary

	m_rcOldPlaceArea = m_rcPlaceArea;
	float fWidth = 0.0f, fHeight = 0.0f;  
	GetMinSize(fWidth, fHeight);
	m_rcPlaceArea = pLayout->m_FrontSide.FindLargestFreeArea(pPrintSheet, fWidth, fHeight);

	InitUps(TRUE, bUpdatePrintSheetViewAll);

	EnableAll();
}

void CDlgComponentImposer::InitUps(BOOL bReset, BOOL bUpdatePrintSheetViewAll, int nOptNup)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	CLayout*	 pLayout	 = GetActLayout();
	if ( ! pPrintSheet || ! pLayout)
		return;

	CFoldSheet*		pFoldSheet   = GetActFoldSheet();
	CFlatProduct*	pFlatProduct = GetActFlatProduct();
	CPrintComponent component	 = GetActComponent();

	BOOL bDefaultDoRotate = ( (m_nRotation == 1) || (m_nRotation == 3) ) ? TRUE : FALSE;
	BOOL bHasRotated	  = bDefaultDoRotate;

	int nNumX = 0, nNumY = 0;
	if (pFoldSheet)
	{
		pLayout->GetMaxNUp(*pPrintSheet, pFoldSheet, bDefaultDoRotate, &nNumX, &nNumY, m_rcPlaceArea);
		if ( ( (nNumX == 0) || (nNumY == 0) ) && bReset)
			pLayout->GetMaxNUp(*pPrintSheet, pFoldSheet, bHasRotated = ! bDefaultDoRotate, &nNumX, &nNumY, m_rcPlaceArea);
	}
	else
	{
		if (pFlatProduct)
		{
			pLayout->GetMaxNUp(*pPrintSheet, *pFlatProduct, bDefaultDoRotate, &nNumX, &nNumY, m_rcPlaceArea);
			if ( ( (nNumX == 0) || (nNumY == 0) ) && bReset)
				pLayout->GetMaxNUp(*pPrintSheet, *pFlatProduct, bHasRotated = ! bDefaultDoRotate, &nNumX, &nNumY, m_rcPlaceArea);
		}
	}
	int nMaxNup = nNumX * nNumY;
	if ( (m_nPlaceMethod == 1) && pFoldSheet)	// sequential
		nMaxNup = min(nMaxNup, pDoc->m_Bookblock.GetNumUnplacedFoldSheets(pFoldSheet, pFoldSheet->m_nProductPartIndex, pFoldSheet->m_strFoldSheetTypeName, TRUE));

	if (nOptNup >= 0)
		nMaxNup = min(nMaxNup, nOptNup);

	m_nNup = nMaxNup;

	if ( (bHasRotated != bDefaultDoRotate) && (m_nNup > 0))
	{
		m_nRotation = (bHasRotated) ? 1 : 0;  
		((CComboBox*)GetDlgItem(IDC_COMPONENT_ROTATION_COMBO))->SetCurSel(m_nRotation);
	}

	if ( (m_nNup <= 0) && pPrintSheet->IsEmpty())	// if printsheet is empty, force one component to be shown 
	{
		float fWidth = 0.0f, fHeight = 0.0f; 
		GetMinSize(fWidth, fHeight);

		float fXPos = 0.0f;
		float fYPos = 0.0f;
		m_rcOldPlaceArea = ( ! m_rcPlaceArea.IsRectNull()) ? m_rcPlaceArea : m_rcOldPlaceArea;
		m_rcPlaceArea = CRect((long)(fXPos * 1000), (long)(fYPos * 1000), (long)((fXPos + fWidth) * 1000), (long)((fYPos + fHeight) * 1000) );
		m_nNup = 1;
	}

	m_NUpCombo.ResetContent();
	CString string, strNUp; strNUp.LoadString(IDS_NUP);
	for (int i = 0; i < m_nNup; i++)
	{
		string.Format(_T("%d"), i + 1);
		m_NUpCombo.InsertString(i, string);
	}

	m_NUpCombo.SetCurSel(m_nNup - 1);

	CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
	if (pPrintComponentsView && pDoc)
	{
		if (m_nPlaceMethod == 1)	// sequential
		{
			int				nNum		 = m_nNup;
			CPrintComponent actComponent = GetActComponent();	// get first selected component
			pPrintComponentsView->m_DisplayList.DeselectAll(FALSE, TRUE);
			CDisplayItem* pDI = pPrintComponentsView->m_DisplayList.GetItemFromData((void*)(actComponent.GetIndex()));
			while (pDI)
			{
				pPrintComponentsView->m_DisplayList.SelectItem(*pDI, FALSE);
				pDI->Invalidate(pPrintComponentsView->m_DisplayList, TRUE, FALSE);
				if (pDoc->m_printComponentsList.GetComponent((int)pDI->m_pData).m_nType != CPrintComponent::FoldSheet)
					break;
				pDI = pPrintComponentsView->m_DisplayList.GetNextItem(pDI);
				nNum--;
				if (nNum <= 0)
					break;
			}
		}
		else
		{
			CPrintComponent actComponent = GetActComponent();	// get first selected component
			pPrintComponentsView->m_DisplayList.DeselectAll(FALSE, TRUE);
			CDisplayItem* pDI = pPrintComponentsView->m_DisplayList.GetItemFromData((void*)(actComponent.GetIndex()));
			if (pDI)
			{
				pPrintComponentsView->m_DisplayList.SelectItem(*pDI, FALSE);
				pDI->Invalidate(pPrintComponentsView->m_DisplayList, TRUE, FALSE);
			}
		}
		pPrintComponentsView->UpdateWindow();
	}

	InitPrintLayoutTemplate();
	UpdatePrintSheetView(bUpdatePrintSheetViewAll);

	EnableAll();
}

int CDlgComponentImposer::CalcOptimumNup(CPrintComponent& rComponent, CPrintSheet* pPrintSheet, int nDefaultNup)
{
	int nNup = 1;

	if (m_nPlaceMethod == 1)	// sequential
	{
		CFoldSheet*	pFoldSheet = GetActFoldSheet();
		if (pFoldSheet)
		{
			CImpManDoc*  pDoc = CImpManDoc::GetDoc();
			if (pDoc)
				nNup = pDoc->m_Bookblock.GetNumUnplacedFoldSheets(pFoldSheet, pFoldSheet->m_nProductPartIndex, pFoldSheet->m_strFoldSheetTypeName, TRUE);
		}
	}
	else
	{
		int nQuantityDiff = rComponent.GetQuantityDiff();
		if (nQuantityDiff >= 0)		// we have already enough
			return 0;
		if ( ! pPrintSheet)
			return 0;
		if (pPrintSheet->m_nQuantityTotal <= 1)
			return nDefaultNup;

		nNup = abs(nQuantityDiff) / pPrintSheet->m_nQuantityTotal;
	}

	return max(nNup, 1);
}

void CDlgComponentImposer::InitPrintLayoutTemplate()
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! GetActPrintSheet() || ! GetActLayout())
		return;

	if (GetActFoldSheet())
		InitFoldSheetImposingSamples();
	else
		if (GetActFlatProduct())
			InitFlatProductImposingSamples();
}

void CDlgComponentImposer::InitFoldSheetImposingSamples()
{
	m_printSheetTemplate.DeleteContents();
	m_layoutTemplate.DeleteContents();
	m_printSheetTemplate.LinkLayout(&m_layoutTemplate);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (m_rcPlaceArea.IsRectEmpty())
		return;
	CLayout*	 pLayout	 = GetActLayout();
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	CFoldSheet*  pFoldSheet  = GetActFoldSheet();
	int			 nLayerIndex = GetActLayerIndex();
	if ( ! pLayout || ! pPrintSheet || ! pFoldSheet)
		return;

	BeginWaitCursor();

	m_layoutTemplate.Create((float)m_rcPlaceArea.left / 1000.0f, (float)m_rcPlaceArea.top / 1000.0f, (float)m_rcPlaceArea.right / 1000.0f, (float)m_rcPlaceArea.bottom / 1000.0f, pLayout->GetIndex());	
	m_layoutTemplate.SetObjectsStatusAll(FALSE);

	int nHeadPosition = TOP;
	switch (m_nRotation)
	{
	case 0: nHeadPosition = TOP;	break;
	case 1: nHeadPosition = LEFT;	break;
	case 2: nHeadPosition = BOTTOM;	break;
	case 3: nHeadPosition = RIGHT;	break;
	}

	int	 nOldWorkStyle	= pLayout->GetCurrentWorkStyle();
	BOOL bReposition	= (pPrintSheet->IsEmpty()) ? TRUE : FALSE;
	switch (m_nPlaceMethod)
	{
	case 0:	// single
		{
			if (m_layoutTemplate.MakeNUp(FALSE,				m_printSheetTemplate, pFoldSheet, nLayerIndex, m_nNup, nHeadPosition, GetActPrintingProfile(), bReposition, m_rcPlaceArea))
			{
				m_layoutTemplate.m_markList.Copy(GetActLayout()->m_markList);
				if (bReposition)
					pLayout->SetWorkStyle(WORK_AND_BACK);
			}
		}
		break;

	case 1:	// sequential
		{
			if (m_layoutTemplate.MakeSequential(FALSE,		m_printSheetTemplate, pFoldSheet, nLayerIndex, m_nNup, nHeadPosition, GetActPrintingProfile(), bReposition, m_rcPlaceArea))
			{
				m_layoutTemplate.m_markList.Copy(GetActLayout()->m_markList);
				if (bReposition)
					pLayout->SetWorkStyle(WORK_AND_BACK);
			}
		}
		break;

	case 2:	// WorkAndTurn
		{
			if (m_layoutTemplate.MakeWorkAndTurn(FALSE,		m_printSheetTemplate, pFoldSheet, nLayerIndex, m_nNup, nHeadPosition, GetActPrintingProfile(), bReposition, m_rcPlaceArea))
			{
				m_layoutTemplate.m_markList.Copy(GetActLayout()->m_markList);
				if (bReposition)
					pLayout->SetWorkStyle(WORK_AND_TURN);
			}
		}
		break;

	case 3:	// WorkAndTumble
		{
			if (m_layoutTemplate.MakeWorkAndTumble(FALSE,	m_printSheetTemplate, pFoldSheet, nLayerIndex, m_nNup, nHeadPosition, GetActPrintingProfile(), bReposition, m_rcPlaceArea))
			{
				m_layoutTemplate.m_markList.Copy(GetActLayout()->m_markList);
				if (bReposition)
					pLayout->SetWorkStyle(WORK_AND_TUMBLE);
			}
		}
		break;
	}

	if (bReposition)
	{
		CPrintingProfile* pPrintingProfile = GetActPrintingProfile();
		float fGripper = (pPrintingProfile) ? ((pPrintingProfile->m_press.m_pressDevice.m_nPressType == CPressDevice::SheetPress) ? pPrintingProfile->m_press.m_pressDevice.m_fGripper : 0.0f) : 0.0f;
		m_layoutTemplate.MoveLayoutObjects(-m_layoutTemplate.m_FrontSide.m_fPaperLeft, -m_layoutTemplate.m_FrontSide.m_fPaperBottom - fGripper);
		if (nOldWorkStyle != pLayout->GetCurrentWorkStyle())
		{
			CPrintSheetViewStrippingData* pStrippingView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
			if (pStrippingView)
			{
				pStrippingView->m_layoutDataWindow.Invalidate();
				pStrippingView->m_layoutDataWindow.UpdateWindow();
			}
		}
	}

	EndWaitCursor();
}

void CDlgComponentImposer::InitFlatProductImposingSamples()
{
	m_printSheetTemplate.DeleteContents();
	m_layoutTemplate.DeleteContents();
	m_printSheetTemplate.LinkLayout(&m_layoutTemplate);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (m_rcPlaceArea.IsRectEmpty())
		return;
	CLayout*	 pLayout	 = GetActLayout();
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	if ( ! pLayout || ! pPrintSheet)
		return;

	BeginWaitCursor();

	m_layoutTemplate.Create((float)m_rcPlaceArea.left / 1000.0f, (float)m_rcPlaceArea.top / 1000.0f, (float)m_rcPlaceArea.right / 1000.0f, (float)m_rcPlaceArea.bottom / 1000.0f, pLayout->GetIndex());	
	m_layoutTemplate.SetObjectsStatusAll(FALSE);

	int nHeadPosition = TOP;
	switch (m_nRotation)
	{
	case 0: nHeadPosition = TOP;	break;
	case 1: nHeadPosition = LEFT;	break;
	case 2: nHeadPosition = BOTTOM;	break;
	case 3: nHeadPosition = RIGHT;	break;
	}

	if (m_layoutTemplate.MakeNUp(FALSE, m_printSheetTemplate, GetActFlatProduct(), m_nNup, nHeadPosition, GetActPrintingProfile(), FALSE, m_rcPlaceArea))
		m_layoutTemplate.m_markList.Copy(GetActLayout()->m_markList);

	EndWaitCursor();
}

void CDlgComponentImposer::SelectPlaceArea(CPoint point)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));	
	if ( ! pView)
		return;

	CLayout* pLayout = GetActLayout();
	if (pLayout) 
	{
		pView->m_DisplayList.OnMouseMove(point);	// make sure m_bMouseOver is up to date

		CDisplayItem* pDI = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FreeArea));
		while (pDI)
		{
			if (pDI->m_bMouseOver)
			{
				int nPlaceAreaIndex = (int)pDI->m_pData;
				if ( (nPlaceAreaIndex >= 0) && (nPlaceAreaIndex < pLayout->m_FrontSide.m_freeAreas.GetSize()) )
				{
					m_rcOldPlaceArea = m_rcPlaceArea;
					m_rcPlaceArea = pLayout->m_FrontSide.m_freeAreas[nPlaceAreaIndex];
					InitUps(TRUE, FALSE);
				}
			}
			pDI = pView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, FreeArea));
		}
	}
}

void CDlgComponentImposer::SelectPlaceArea(CRect rcPlaceArea)
{
	m_rcOldPlaceArea = m_rcPlaceArea;
	m_rcPlaceArea = rcPlaceArea;

	InitUps(FALSE, FALSE);
}

void CDlgComponentImposer::GetMinSize(float& fMinWidth, float& fMinHeight)
{
	BOOL bDoRotate = ( (m_nRotation == 1) || (m_nRotation == 3) ) ? TRUE : FALSE;

	fMinWidth = fMinHeight = 0.0f; 

	CFoldSheet*	  pFoldSheet	= GetActFoldSheet();
	int			  nLayerIndex	= GetActLayerIndex();
	CFlatProduct* pFlatProduct	= GetActFlatProduct();
	if ( ! pFoldSheet && ! pFlatProduct)
		return;

	if (pFoldSheet) 
		pFoldSheet->GetCutSize(nLayerIndex, fMinWidth, fMinHeight);
	else
		if (pFlatProduct)
		{
			fMinWidth  = pFlatProduct->m_fTrimSizeWidth  + pFlatProduct->m_fLeftTrim   + pFlatProduct->m_fRightTrim;
			fMinHeight = pFlatProduct->m_fTrimSizeHeight + pFlatProduct->m_fBottomTrim + pFlatProduct->m_fTopTrim;
		}

	if (bDoRotate)
	{
		float fTemp = fMinWidth; fMinWidth = fMinHeight; fMinHeight = fTemp;
	}
}

void CDlgComponentImposer::EnableAll()
{
	//EnumChildWindows(GetSafeHwnd(), EnumChildProc, bEnable);
	BOOL bEnable = (m_nNup > 0) ? TRUE : FALSE;
	GetDlgItem(IDC_CI_ADD_PRODUCT)->EnableWindow(bEnable);
	GetDlgItem(IDC_CI_NUP_COMBO)->EnableWindow(bEnable);
	GetDlgItem(IDC_CI_FILLUP)->EnableWindow(bEnable);
	GetDlgItem(IDC_CI_OPTIMUM_FILLUP)->EnableWindow(bEnable);
	GetDlgItem(IDC_COMPONENT_ROTATION_COMBO)->EnableWindow(bEnable);	// we always need rotation, i.e. when component gets invisible after rotation we want to rotate back

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		GetDlgItem(IDC_CI_NEXT_PRODUCT)->EnableWindow((GetActComponent().GetIndex() < pDoc->m_printComponentsList.GetNumComponentsAll() - 1) ? TRUE : FALSE);

	if (bEnable)
		GetDlgItem(IDC_CI_PLACE_METHOD)->EnableWindow((GetActFlatProduct()) ? FALSE : TRUE);
	else
		GetDlgItem(IDC_CI_PLACE_METHOD)->EnableWindow(FALSE);
}

BOOL CALLBACK CDlgComponentImposer::EnumChildProc(HWND hwndChild, LPARAM lParam) 
{ 
	::EnableWindow(hwndChild, lParam); 
 
    return TRUE;
}

void CDlgComponentImposer::OnClose()
{
	CDialog::OnClose();

	m_bLockDrawPreviews = TRUE;		// to avoid that preview frames will be displayed while printsheetview is drawn within DestroyWindow()
									// otherwise an additional UpdateView would be necessary after DestroyWindow
	CLayout* pLayout = GetActLayout();
	if (pLayout)
	{
		pLayout->m_FrontSide.SetAllVisible();
		pLayout->m_BackSide.SetAllVisible();
	}

	DestroyWindow();

	m_bLockDrawPreviews = FALSE;
}

void CDlgComponentImposer::DrawPreviewFrame(CDC* pDC)
{
	if (m_bLockDrawPreviews)
		return;

	if (m_hWnd)
		UpdateData(TRUE);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CPrintComponent component = GetActComponent();
	if (component.IsNull())
		return;

	CPrintSheet*	pPrintSheet  = pView->GetActPrintSheet();
	CLayout*		pLayout		 = pView->GetActLayout();

	switch (component.m_nType)
	{
	case CPrintComponent::Flat:			DrawPreviewFrameFlat	 (pDC, pPrintSheet, pLayout, &pView->m_DisplayList);	break;
	case CPrintComponent::FoldSheet:	
	case CPrintComponent::PageSection:	DrawPreviewFrameFoldSheet(pDC, pPrintSheet, pLayout, &pView->m_DisplayList);	break;
	}
}

void CDlgComponentImposer::DrawPreviewFrameFlat(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CDisplayList* pDisplayList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! pDoc->m_PrintSheetList.GetCount())
		return; 

	CPrintSheetView* pView = GetPrintSheetView();
	if ( ! pView)
		return;

	CFlatProduct* pFlatProduct = GetActFlatProduct();

	if ( ! pLayout || ! pFlatProduct)
		return;

	int	nFlatProductIndex = pFlatProduct->GetIndex();

	float fLeft   = m_rcPlaceArea.left / 1000.0f;
	float fBottom = m_rcPlaceArea.top  / 1000.0f;

	int nPaperGrainPrintSheet = pLayout->m_FrontSide.m_nPaperGrain;
	int nPaperGrainProduct	  = pFlatProduct->m_nPaperGrain;
	if ( (m_nRotation == 1) || (m_nRotation == 3) )
	{
		if (nPaperGrainProduct == GRAIN_VERTICAL)
			nPaperGrainProduct = GRAIN_HORIZONTAL;
		else
			nPaperGrainProduct = GRAIN_VERTICAL;
	}

	int nHatchStyle = (nPaperGrainProduct == GRAIN_EITHER) ? HS_CROSS : ( (nPaperGrainProduct == GRAIN_VERTICAL) ? HS_VERTICAL : HS_HORIZONTAL);
	CBrush  brush(nHatchStyle, ( (nPaperGrainProduct == GRAIN_EITHER) || (nPaperGrainPrintSheet == nPaperGrainProduct) ) ? DARKGREEN : RGB(255,80,80));
	CBrush* pOldBrush  = pDC->GetCurrentBrush();
	CPen*	pOldPen	   = pDC->GetCurrentPen();
	int		nOldBkMode = pDC->GetBkMode();

	CRect rcBoxAll(0,0,0,0);
	CRect rcGrossBoxAll(0,0,0,0);
	POSITION pos = m_layoutTemplate.m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_layoutTemplate.m_FrontSide.m_ObjectList.GetNext(pos);
		rObject.Offset(fLeft, fBottom);

		float fGrossLeft = 0.0f, fGrossBottom = 0.0f, fGrossRight = 0.0f, fGrossTop = 0.0f; 
		rObject.GetCurrentGrossBox(&m_printSheetTemplate, fGrossLeft, fGrossBottom, fGrossRight, fGrossTop);

		CRect rcObject, rcGrossObject;
		rcObject.left		 = CPrintSheetView::WorldToLP(rObject.GetLeft());
		rcObject.bottom 	 = CPrintSheetView::WorldToLP(rObject.GetBottom());
		rcObject.right  	 = rcObject.left   + CPrintSheetView::WorldToLP(rObject.GetWidth());
		rcObject.top		 = rcObject.bottom + CPrintSheetView::WorldToLP(rObject.GetHeight());
		rcGrossObject.left	 = CPrintSheetView::WorldToLP(fGrossLeft);
		rcGrossObject.bottom = CPrintSheetView::WorldToLP(fGrossBottom);
		rcGrossObject.right  = CPrintSheetView::WorldToLP(fGrossRight);
		rcGrossObject.top	 = CPrintSheetView::WorldToLP(fGrossTop);

		pDC->SelectObject(&brush);
		pDC->SelectStockObject(NULL_PEN);
		pDC->SetBkMode(TRANSPARENT);
		CPrintSheetView::DrawRectangleExt(pDC, rcObject);

		rObject.m_nStatus |= CLayoutObject::Highlighted;
		pView->DrawPage(pDC, rcObject, rObject, NULL, &m_printSheetTemplate);
		rObject.m_nStatus &= ~CLayoutObject::Highlighted;

		CRect rcObjectNormalized	  = rcObject;	   rcObjectNormalized.NormalizeRect();
		CRect rcGrossObjectNormalized = rcGrossObject; rcGrossObjectNormalized.NormalizeRect();
		rcBoxAll.UnionRect(rcBoxAll, rcObjectNormalized);
		rcGrossBoxAll.UnionRect(rcGrossBoxAll, rcGrossObjectNormalized);

		rObject.Offset(-fLeft, -fBottom);
	}

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	pDC->SetBkMode(nOldBkMode);
	brush.DeleteObject();

	if ( ! rcBoxAll.IsRectEmpty())
	{
		int nTmp = rcGrossBoxAll.top; rcGrossBoxAll.top = rcGrossBoxAll.bottom; rcGrossBoxAll.bottom = nTmp;
		CGraphicComponent gc;
		gc.DrawShadowFrame(pDC, rcGrossBoxAll, 8);

		CRect rcTracker = rcGrossBoxAll;
		pDC->LPtoDP(rcTracker);
		COLORREF crColor = ( (nPaperGrainProduct == GRAIN_EITHER) || (nPaperGrainPrintSheet == nPaperGrainProduct) ) ? DARKGREEN : RGB(255,80,80);
		pView->DrawResizeTracker(pDC, rcTracker, crColor);

		nTmp = rcBoxAll.top; rcBoxAll.top = rcBoxAll.bottom; rcBoxAll.bottom = nTmp;
		CLayoutObject layoutObject; 
		CLayoutObject::DrawObjectID(pDC, rcBoxAll, layoutObject, pFlatProduct->m_strProductName, "", 0, NULL, TRUE, 0);
	}
}

void CDlgComponentImposer::DrawPreviewFrameFoldSheet(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CDisplayList* pDisplayList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! pDoc->m_PrintSheetList.GetCount())
		return; 

	CPrintSheetView* pView = GetPrintSheetView();
	if ( ! pView)
		return;

	CFoldSheet* pFoldSheet = GetActFoldSheet();

	if ( ! pLayout || ! pFoldSheet)
		return;
	if (m_layoutTemplate.m_FoldSheetLayerRotation.GetSize() <= 0)
		return;

	CFoldSheet sampleFoldSheet = *pFoldSheet;

	float fLeft   = m_rcPlaceArea.left / 1000.0f;		
	float fBottom = m_rcPlaceArea.top  / 1000.0f;

	int nPaperGrainPrintSheet = pLayout->m_FrontSide.m_nPaperGrain;
	int nPaperGrainFoldSheet  = (pFoldSheet->GetSpineOrientation() == VERTICAL) ? GRAIN_VERTICAL : GRAIN_HORIZONTAL;
	if ( (m_layoutTemplate.m_FoldSheetLayerRotation[0] == 90) ||  (m_layoutTemplate.m_FoldSheetLayerRotation[0] == 270) )
	{
		if (nPaperGrainFoldSheet == GRAIN_VERTICAL)
			nPaperGrainFoldSheet = GRAIN_HORIZONTAL;
		else
			nPaperGrainFoldSheet = GRAIN_VERTICAL;
	}

	int nHatchStyle = (nPaperGrainFoldSheet == GRAIN_VERTICAL) ? HS_VERTICAL : HS_HORIZONTAL;
	CBrush  brush(nHatchStyle, (nPaperGrainPrintSheet == nPaperGrainFoldSheet) ? DARKGREEN : RGB(255,80,80));
	CBrush* pOldBrush  = pDC->GetCurrentBrush();
	CPen*	pOldPen	   = pDC->GetCurrentPen();
	int		nOldBkMode = pDC->GetBkMode();

	CRect rcBoxAll(0,0,0,0);
	CRect rcGrossBoxAll(0,0,0,0);
	POSITION pos = m_layoutTemplate.m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_layoutTemplate.m_FrontSide.m_ObjectList.GetNext(pos);
		rObject.Offset(fLeft, fBottom);

		float fGrossLeft = 0.0f, fGrossBottom = 0.0f, fGrossRight = 0.0f, fGrossTop = 0.0f; 
		rObject.GetCurrentGrossBox(&m_printSheetTemplate, fGrossLeft, fGrossBottom, fGrossRight, fGrossTop);

		CRect rcObject, rcGrossObject;
		rcObject.left		 = CPrintSheetView::WorldToLP(rObject.GetLeft());
		rcObject.bottom 	 = CPrintSheetView::WorldToLP(rObject.GetBottom());
		rcObject.right  	 = rcObject.left   + CPrintSheetView::WorldToLP(rObject.GetWidth());
		rcObject.top		 = rcObject.bottom + CPrintSheetView::WorldToLP(rObject.GetHeight());
		rcGrossObject.left	 = CPrintSheetView::WorldToLP(fGrossLeft);
		rcGrossObject.bottom = CPrintSheetView::WorldToLP(fGrossBottom);
		rcGrossObject.right  = CPrintSheetView::WorldToLP(fGrossRight);
		rcGrossObject.top	 = CPrintSheetView::WorldToLP(fGrossTop);
		
		pDC->SelectObject(&brush);
		pDC->SelectStockObject(NULL_PEN);
		pDC->SetBkMode(TRANSPARENT);
		CPrintSheetView::DrawRectangleExt(pDC, rcObject);

		rObject.m_nStatus |= CLayoutObject::Highlighted;
		pView->DrawPage(pDC, rcObject, rObject, 0, &m_printSheetTemplate);
		rObject.m_nStatus &= ~CLayoutObject::Highlighted;

		CRect rcObjectNormalized	  = rcObject;	   rcObjectNormalized.NormalizeRect();
		CRect rcGrossObjectNormalized = rcGrossObject; rcGrossObjectNormalized.NormalizeRect();
		rcBoxAll.UnionRect(rcBoxAll, rcObjectNormalized);
		rcGrossBoxAll.UnionRect(rcGrossBoxAll, rcGrossObjectNormalized);
	}

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	pDC->SetBkMode(nOldBkMode);
	brush.DeleteObject();

	if ( ! rcBoxAll.IsRectEmpty())
	{
		int nTmp = rcGrossBoxAll.top; rcGrossBoxAll.top = rcGrossBoxAll.bottom; rcGrossBoxAll.bottom = nTmp;
		CGraphicComponent gc;
		gc.DrawShadowFrame(pDC, rcGrossBoxAll, 8);

		CPen    pen(PS_SOLID, 1, (nPaperGrainPrintSheet == nPaperGrainFoldSheet) ? RGB(70,160,30) : RED);
		pDC->SelectStockObject(HOLLOW_BRUSH);
		CPen*	pOldPen   = pDC->SelectObject(&pen);
		pDC->SetBkMode(TRANSPARENT);
		CPrintSheetView::DrawRectangleExt(pDC, rcGrossBoxAll);
		pen.DeleteObject();
	}

	for (int i = 0; i < m_printSheetTemplate.m_FoldSheetLayerRefs.GetSize(); i++)
	{
		if (m_printSheetTemplate.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex < 0)
		{
			sampleFoldSheet.m_nFoldSheetNumber = abs(m_printSheetTemplate.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex);
			pView->DrawFoldSheetRect(pDC, FRONTSIDE, &m_printSheetTemplate, i, &sampleFoldSheet);
		}
		else
		{
			pFoldSheet = pDoc->m_Bookblock.GetFoldSheetByIndex(m_printSheetTemplate.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex);
			pView->DrawFoldSheetRect(pDC, FRONTSIDE, &m_printSheetTemplate, i, pFoldSheet);
		}
	}

	pos = m_layoutTemplate.m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_layoutTemplate.m_FrontSide.m_ObjectList.GetNext(pos);
		rObject.Offset(-fLeft, -fBottom);
	}
}

void CDlgComponentImposer::AddProduct(BOOL bGotoNext)
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CPrintComponent component = GetActComponent();
	if (component.IsNull())
		return;

	CPrintSheet* pPrintSheet = GetActPrintSheet();

	CFoldSheet* pFirstAddedFoldSheet = NULL;
	switch (component.m_nType)
	{
	case CPrintComponent::Flat:			AddFlatProduct();						break;
	case CPrintComponent::PageSection:	
	case CPrintComponent::FoldSheet:	pFirstAddedFoldSheet = AddFoldSheet();	break;
	}

	int nNumSheets = 1;
	if (GetDlgFoldSchemeSelector())
	{
		if (GetDlgFoldSchemeSelector()->m_hWnd)
			nNumSheets = GetDlgFoldSchemeSelector()->m_nNumSheets;
	}

	//if (pFirstAddedFoldSheet)		// 05/19/2015: removed because it seems to be more practical when initially only 1 sheet
	//{
	//	CLayout* pLayout = GetActLayout();
	//	nNumSheets = (pLayout) ? pLayout->GetMaxNumPrintSheetsBasedOn() : nNumSheets;
	//	//nNumSheets *= pFirstAddedFoldSheet->m_LayerList.GetSize();	// if multiple layer add one sheet for each 
	//}

	pDoc->SetModifiedFlag();		

	CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
	if (pPrintComponentsView)
	{
		pPrintComponentsView->m_DisplayList.DeselectAll(FALSE, TRUE);
		int	nLayerIndex	= GetActLayerIndex();
		CPrintComponent component = pDoc->m_printComponentsList.FindFoldSheetLayerComponent(pFirstAddedFoldSheet->GetIndex(), nLayerIndex);
		pPrintComponentsView->SetActComponent(component);
		if (pFirstAddedFoldSheet)
			pPrintSheet = pFirstAddedFoldSheet->GetPrintSheet(nLayerIndex);
	}

	CPrintSheetNavigationView*	pNavigationView	= (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
		pNavigationView->SetActPrintSheet(pPrintSheet);

	if ( (nNumSheets > 1) )// && (m_nPlaceMethod != 1) )		// not in sequential mode
	{
		CPrintSheetViewStrippingData* pStrippingView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
		if (pStrippingView)
			pStrippingView->m_layoutDataWindow.ChangeNumSheets(nNumSheets);
	}

	theApp.UpdateView(RUNTIME_CLASS(CProductsView));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), TRUE);	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));	

	CLayout* pLayout = GetActLayout();
	float	 fWidth  = 0.0f, fHeight = 0.0f; 
	GetMinSize(fWidth, fHeight);
	if (pLayout && pPrintSheet)
	{
		m_rcOldPlaceArea = m_rcPlaceArea;
		m_rcPlaceArea = pLayout->m_FrontSide.FindLargestFreeArea(pPrintSheet, fWidth, fHeight);
	}

	InitUps();
}

void CDlgComponentImposer::OnAddProduct()
{
	AddProduct(FALSE);
}

void CDlgComponentImposer::OnNextProduct()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
	if (pPrintComponentsView)
	{
		CPrintComponent actComponent  = GetActComponent();
		CPrintComponent nextComponent = pDoc->m_printComponentsList.GetComponent(actComponent.GetIndex() + 1);
		CDisplayItem* pDI = pPrintComponentsView->m_DisplayList.GetItemFromData((void*)(nextComponent.GetIndex()));
		if (pDI)
		{
			pPrintComponentsView->m_DisplayList.DeselectAll();
			//pPrintComponentsView->m_DisplayList.Invalidate();		// should not be invalidated here, because selected state will be lost
			pDI->m_nState = CDisplayItem::Selected;
			CRect rcClick = pDI->m_BoundingRect; rcClick.right = rcClick.left + 20;	// avoid toolrect is being "clicked"
			pPrintComponentsView->HandleOnSelectComponent(pDI, rcClick.CenterPoint());
			pPrintComponentsView->Invalidate();
			pPrintComponentsView->UpdateWindow();

			if (nextComponent.m_nType == CPrintComponent::PageSection)
			{
				CNewPrintSheetFrame* pFrame = GetNewPrintSheetFrame();
				if (pFrame)
				{
					if ( ! pFrame->m_dlgFoldSchemeSelector.m_hWnd)
					{
						CRect rcTarget = pDI->m_BoundingRect; rcTarget.left += 60; pPrintComponentsView->ClientToScreen(rcTarget);
						pFrame->m_dlgFoldSchemeSelector.m_ptInitialPos = CPoint(0, 0);
						pFrame->m_dlgFoldSchemeSelector.m_bVisible	   = FALSE;
						pFrame->m_dlgFoldSchemeSelector.Create(IDD_SELECT_FOLDSCHEME_POPUP, pFrame);
						CRect rcDlg; pFrame->m_dlgFoldSchemeSelector.GetClientRect(rcDlg);
						rcTarget.right = rcTarget.left + rcDlg.Width(); rcTarget.bottom = rcTarget.top + rcDlg.Height();
						pFrame->m_dlgFoldSchemeSelector.MoveWindow(rcTarget);
						pFrame->m_dlgFoldSchemeSelector.m_bVisible = TRUE;
						pFrame->m_dlgFoldSchemeSelector.ShowWindow(SW_SHOW);
					}
					else
					{
						pFrame->m_dlgFoldSchemeSelector.InitData();
						pFrame->m_dlgFoldSchemeSelector.InitSchemes();
					}
				}
			}
		}
	}
	InitData(TRUE);
}

void CDlgComponentImposer::AddFlatProduct()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! pDoc->m_PrintSheetList.GetCount())
		return; 

	UndoSave();

	CPrintSheetView* pView = GetPrintSheetView();
	if ( ! pView)
		return;

	CPrintSheet*	pPrintSheet  = pView->GetActPrintSheet();
	CLayout*		pLayout		 = pPrintSheet->GetLayout();
	CFlatProduct*	pFlatProduct = GetActFlatProduct();

	if ( ! pLayout || ! pFlatProduct)
		return;

	if (pLayout->GetNumPrintSheetsBasedOn() > 1)
	{
		pLayout = pPrintSheet->SplitLayout();
		if ( ! pLayout)
			return;
	}

	int	nFlatProductIndex = pFlatProduct->GetIndex();

	float fLeft   = m_rcPlaceArea.left / 1000.0f;
	float fBottom = m_rcPlaceArea.top  / 1000.0f;

	POSITION pos = m_layoutTemplate.m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_layoutTemplate.m_FrontSide.m_ObjectList.GetNext(pos);

		int nComponentRefIndex = pPrintSheet->MakeFlatProductRefIndex(nFlatProductIndex);
		CLayoutObject layoutObject;
		layoutObject.CreateFlatProduct(fLeft + rObject.GetLeft(), fBottom + rObject.GetBottom(), rObject.GetWidth(), rObject.GetHeight(), rObject.m_nHeadPosition, nComponentRefIndex);
		pLayout->m_FrontSide.m_ObjectList.AddTail(layoutObject);
		CLayoutObject* pFrontLayoutObject = &pLayout->m_FrontSide.m_ObjectList.GetTail();

		layoutObject.CreateFlatProduct(fLeft + rObject.GetLeft(), fBottom + rObject.GetBottom(), rObject.GetWidth(), rObject.GetHeight(), rObject.m_nHeadPosition, nComponentRefIndex);
		pLayout->m_BackSide.m_ObjectList.AddTail(layoutObject);
		CLayoutObject* pBackLayoutObject = &pLayout->m_BackSide.m_ObjectList.GetTail();

		pBackLayoutObject->AlignToCounterPart(pFrontLayoutObject);
	}

	pPrintSheet->CalcTotalQuantity(TRUE);

	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();

	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();
}

CFoldSheet* CDlgComponentImposer::AddFoldSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if ( ! pDoc->m_PrintSheetList.GetCount())
		return NULL; 

	CPrintSheetView* pPrintSheetView = GetPrintSheetView();
	if ( ! pPrintSheetView)
		return NULL;

	CFoldSheet*		 pFoldSheet	 = GetActFoldSheet();
	int				 nLayerIndex = GetActLayerIndex();
	CFoldSheetLayer* pLayer		 = pFoldSheet->GetLayer(nLayerIndex);

	if ( ! pFoldSheet || ! pLayer)
		return NULL;

	UndoSave();

	CPrintSheet* pPrintSheet = pPrintSheetView->GetActPrintSheet();
	CLayout*	 pLayout	 = pPrintSheet->GetLayout();
	if ( ! pLayout || ! pPrintSheet)
		return NULL;

	float fLeft   = m_rcPlaceArea.left / 1000.0f;		
	float fBottom = m_rcPlaceArea.top  / 1000.0f;

	int			nFirstAddedFoldSheetIndex = (pFoldSheet->m_nFoldSheetNumber > 0) ? pFoldSheet->GetIndex() : -1;
	CFoldSheet	newFoldSheet			  = *pFoldSheet;
	int			nProductPartIndex		  = pFoldSheet->m_nProductPartIndex;
	for (int nSampleComponentRefIndex = 0; nSampleComponentRefIndex < m_printSheetTemplate.m_FoldSheetLayerRefs.GetSize(); nSampleComponentRefIndex++)
	{
		CFoldSheetLayerRef layerRef = m_printSheetTemplate.m_FoldSheetLayerRefs[nSampleComponentRefIndex];
		if (layerRef.m_nFoldSheetIndex < 0)
		{
			CFoldSheet* pCurFoldSheet = pDoc->m_Bookblock.GetFoldSheetByNum(-layerRef.m_nFoldSheetIndex, nProductPartIndex);
			if ( ! pCurFoldSheet)
			{
				newFoldSheet.m_nProductPartIndex = nProductPartIndex;
				int nNumPages = (pFoldSheet) ? pFoldSheet->GetNumPages() : 0;
				int nNumPagesImposed = pDoc->m_Bookblock.ImposePageSection(&pDoc->GetBoundProductPart(nProductPartIndex), newFoldSheet, nNumPages, 1);
				layerRef.m_nFoldSheetIndex = pDoc->m_Bookblock.m_FoldSheetList.GetSize() - 1;
			}
			else
				layerRef.m_nFoldSheetIndex = pCurFoldSheet->GetIndex();
			if (nFirstAddedFoldSheetIndex < 0)
				nFirstAddedFoldSheetIndex = layerRef.m_nFoldSheetIndex;
		}
		pPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);
	}

	if (pLayout->GetNumPrintSheetsBasedOn() > 1)
	{
		pLayout = pPrintSheet->SplitLayout();
		if ( ! pLayout)
			return NULL;
	}

	for (int nSampleComponentRefIndex = 0; nSampleComponentRefIndex < m_printSheetTemplate.m_FoldSheetLayerRefs.GetSize(); nSampleComponentRefIndex++)
	{
		pLayout->m_FoldSheetLayerRotation.Add(m_layoutTemplate.m_FoldSheetLayerRotation[nSampleComponentRefIndex]);
		pLayout->m_FoldSheetLayerTurned.Add(m_layoutTemplate.m_FoldSheetLayerTurned[nSampleComponentRefIndex]);

		POSITION pos = m_layoutTemplate.m_FrontSide.m_ObjectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = m_layoutTemplate.m_FrontSide.m_ObjectList.GetNext(pos);
			if (rObject.m_nComponentRefIndex != nSampleComponentRefIndex)
				continue;

			CLayoutObject layoutObject = rObject;
			layoutObject.m_nComponentRefIndex = pLayout->m_FoldSheetLayerRotation.GetSize() - 1;
			layoutObject.Offset(fLeft, fBottom);

			pLayout->m_FrontSide.m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pFrontLayoutObject = &pLayout->m_FrontSide.m_ObjectList.GetTail();

			pLayout->m_BackSide.m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pBackLayoutObject = &pLayout->m_BackSide.m_ObjectList.GetTail();

			pBackLayoutObject->AlignToCounterPart(pFrontLayoutObject);
		}
	}

	pPrintSheet->CalcTotalQuantity(TRUE);

	pPrintSheet->GetLayout()->CheckForProductType();

	CPrintingProfile& rProfile = pLayout->GetPrintingProfile();
	//pLayout->UpdatePressParams(rProfile.m_press.m_pressDevice, rProfile.m_press.m_backSidePressDevice);

	pDoc->m_Bookblock.ArrangeCover();

	//pDoc->m_Bookblock.RemoveUnusedFoldSheets();
	pDoc->m_Bookblock.ReNumberFoldSheets(ALL_PARTS);
	pDoc->m_PageTemplateList.SortPages();
	pDoc->m_PrintSheetList.RemoveUnneededFoldSheetsComingAndGoing();
	//pDoc->m_PrintSheetList.SortByFoldSheets();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_PrintSheetList.ReNumberPrintSheets();

	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();

	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	if (nFirstAddedFoldSheetIndex > -1)
		return pDoc->m_Bookblock.GetFoldSheetByIndex(nFirstAddedFoldSheetIndex);
	else
		return NULL;
}

void CDlgComponentImposer::PostNcDestroy()
{
	CDialog::PostNcDestroy();
}

void CDlgComponentImposer::OnCbnSelchangePlaceMethod()
{
	UpdateData(TRUE);

	InitUps(TRUE);
}

void CDlgComponentImposer::OnCbnSelchangeNUpCombo()
{
	UpdateData(TRUE);

	int nSel = m_NUpCombo.GetCurSel();
	if (nSel == CB_ERR)
		return; 

	m_nNup = nSel + 1;

	InitPrintLayoutTemplate();
	UpdatePrintSheetView();
}

void CDlgComponentImposer::OnBnClickedComponentsFillup()
{
	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
		return;

	BOOL bFound = FALSE;
	for (int i = 0; i < pLayout->m_FrontSide.m_freeAreas.GetSize(); i++)
	{
		CRect rcFreeArea = pLayout->m_FrontSide.m_freeAreas[i];
		if (rcFreeArea == m_rcPlaceArea)
		{
			bFound = TRUE;
			break;
		}
	}

	int	nIndexMax = -1;
	if ( ! bFound)	// current m_rcPlaceArea has been created by mouse tracker, or has been calculated on sheet center, because no fitting area found
	{
		long	nMaxArea  = 0;
		CPoint	ptCenter  = m_rcPlaceArea.CenterPoint();
		for (int i = 0; i < pLayout->m_FrontSide.m_freeAreas.GetSize(); i++)
		{
			CRect rcFreeArea = pLayout->m_FrontSide.m_freeAreas[i];
			if (rcFreeArea.PtInRect(ptCenter))
			{
				long nWidth  = rcFreeArea.Width()  / 1000;
				long nHeight = rcFreeArea.Height() / 1000;
				if (nWidth * nHeight > nMaxArea)
				{
					nMaxArea = nWidth * nHeight;
					nIndexMax = i;
				}
			}
		}
	}

	if (nIndexMax >= 0)
	{
		m_rcOldPlaceArea = m_rcPlaceArea;
		m_rcPlaceArea = pLayout->m_FrontSide.m_freeAreas[nIndexMax];
	}
	InitUps();
}

void CDlgComponentImposer::OnBnClickedComponentsOptimumFillup()
{
	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
		return;

	BOOL bFound = FALSE;
	for (int i = 0; i < pLayout->m_FrontSide.m_freeAreas.GetSize(); i++)
	{
		CRect rcFreeArea = pLayout->m_FrontSide.m_freeAreas[i];
		if (rcFreeArea == m_rcPlaceArea)
		{
			bFound = TRUE;
			break;
		}
	}

	int	nIndexMax = -1;
	if ( ! bFound)	// current m_rcPlaceArea has been created by mouse tracker, or has been calculated on sheet center, because no fitting area found
	{
		long	nMaxArea  = 0;
		CPoint	ptCenter  = m_rcPlaceArea.CenterPoint();
		for (int i = 0; i < pLayout->m_FrontSide.m_freeAreas.GetSize(); i++)
		{
			CRect rcFreeArea = pLayout->m_FrontSide.m_freeAreas[i];
			if (rcFreeArea.PtInRect(ptCenter))
			{
				long nWidth  = rcFreeArea.Width()  / 1000;
				long nHeight = rcFreeArea.Height() / 1000;
				if (nWidth * nHeight > nMaxArea)
				{
					nMaxArea = nWidth * nHeight;
					nIndexMax = i;
				}
			}
		}
	}

	if (nIndexMax >= 0)
	{
		m_rcOldPlaceArea = m_rcPlaceArea;
		m_rcPlaceArea = pLayout->m_FrontSide.m_freeAreas[nIndexMax];
	}

	int nOptNup = CalcOptimumNup(GetActComponent(), GetActPrintSheet());

	InitUps(TRUE, TRUE, nOptNup);
}

void CDlgComponentImposer::OnSelchangeComponentRotationCombo() 
{
	InitComponentRotation();

	InitUps(FALSE, FALSE);
}

void CDlgComponentImposer::InitComponentRotation() 
{
	int nIDBitmap;
	int nSel = ((CComboBox*)GetDlgItem(IDC_COMPONENT_ROTATION_COMBO))->GetCurSel();
	if (nSel == CB_ERR)
		return;

	switch (nSel)
	{
	case 0: nIDBitmap = IDB_PORTRAIT;	  break;
	case 1: nIDBitmap = IDB_PORTRAIT_90;  break;
	case 2: nIDBitmap = IDB_PORTRAIT_180; break;
	case 3: nIDBitmap = IDB_PORTRAIT_270; break;
	default:nIDBitmap = -1;				  break;
	}
	m_nRotation = nSel;
}

BOOL CDlgComponentImposer::PreTranslateMessage(MSG* pMsg)
{
	//UpdateDialogControls( this, TRUE );
	EnableAll();

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgComponentImposer::UndoSave()
{
	CPrintSheetView* pView = GetPrintSheetView();
	if (pView)
		pView->UndoSave();
}

void CDlgComponentImposer::UpdatePrintSheetView(BOOL bInvalidateAll)
{
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));	
	if ( ! pView)
		return;

	pView->m_DisplayList.Invalidate();

	//CRect rcOldRect = m_rcOldPlaceArea; 
	//CRect rcNewRect = m_rcPlaceArea; 

	//CClientDC cdc(pView);
	//CDC dc;
	//dc.CreateCompatibleDC(&cdc);
	//pView->OnPrepareDC(&dc);
	//CPoint ptVpOrg( -pView->GetScrollPos(SB_HORZ) + pView->STATUSBAR_THICKNESS(HORIZONTAL), -pView->GetScrollPos(SB_VERT) + pView->STATUSBAR_THICKNESS(VERTICAL));
	//switch (pView->GetActSight())
	//{
	//	case FRONTSIDE : dc.SetWindowOrg(pView->m_docOrg.x, pView->m_docSize.cy + pView->m_docOrg.y);
	//			 		 dc.SetViewportOrg(ptVpOrg);
 //   					 break;
	//	case BACKSIDE  : dc.SetWindowOrg(pView->m_docOrg.x, pView->m_docSize.cy + pView->m_docOrg.y);
	//			 		 dc.SetViewportOrg(ptVpOrg);
 //   					 break;
	//	case BOTHSIDES : dc.SetWindowOrg(pView->m_docOrg.x, pView->m_docSize.cy + pView->m_docOrg.y);
	//			 		 dc.SetViewportOrg(ptVpOrg);
 //   					 break;
	//	default		:	break;
	//}
	//rcOldRect.left	 /= 10;
	//rcOldRect.bottom /= 10;
	//rcOldRect.right  /= 10;
	//rcOldRect.top	 /= 10;
	//dc.LPtoDP(&rcOldRect);
	//rcOldRect.NormalizeRect();

	//rcNewRect.left	 /= 10;
	//rcNewRect.bottom /= 10;
	//rcNewRect.right  /= 10;
	//rcNewRect.top	 /= 10;
	//dc.LPtoDP(&rcNewRect);
	//rcNewRect.NormalizeRect();

	//dc.DeleteDC();
	//
	//rcOldRect.InflateRect(8,8);
	//rcNewRect.InflateRect(8,8);
	//pView->ValidateRect(NULL);
	//if (bInvalidateAll)
		pView->Invalidate();
	//else
	//{
	//	pView->InvalidateRect(rcOldRect);
	//	pView->InvalidateRect(rcNewRect);
	//}
	pView->UpdateWindow();
}

// draw/remove hatched sheet w/o blinking
void CDlgComponentImposer::SwitchPrintSheetViewSheetGrain(BOOL bSheetGrainOn)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));	
	if ( ! pView)
		return;

	m_bSheetGrainOn = bSheetGrainOn;
	//CDisplayItem* pDI = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
	//if (pDI)
	//	pView->InvalidateRect(pDI->m_BoundingRect);
	//pDI = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	//while (pDI)
	//{
	//	CRect rcRect = pDI->m_BoundingRect; rcRect.DeflateRect(6,6);
	//	pView->ValidateRect(rcRect);
	//	pDI = pView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	//}
	pView->Invalidate();
	pView->UpdateWindow();
}

void CDlgComponentImposer::OnDestroy() 
{
	m_bSheetGrainOn = FALSE;
	m_printSheetTemplate.DeleteContents();
	m_layoutTemplate.DeleteContents();

	CLayout* pLayout = GetActLayout();
	if (pLayout)
	{
		pLayout->m_FrontSide.SetAllVisible();
		pLayout->m_BackSide.SetAllVisible();
		CPrintSheetView* pView = GetPrintSheetView();
		if (pView)
			pView->m_DisplayList.Invalidate();

	}

	if (GetDlgFoldSchemeSelector())
		if (GetDlgFoldSchemeSelector()->m_hWnd)
			GetDlgFoldSchemeSelector()->DestroyWindow();

	m_rcPlaceArea.SetRectEmpty();
	m_rcOldPlaceArea.SetRectEmpty();

	CDialog::OnDestroy();
}
