#include "stdafx.h"
#include "Imposition Manager.h"
#include "PDFEngineXMLParser.h"


CPDFEngineXMLParser::CPDFEngineXMLParser(void)
{
	setlocale(LC_NUMERIC, "english");	// because when german setted, sscanf() does expect numbers with Comma like "0,138"
	m_strDocumentTitle.Empty();
	m_strCreator.Empty();
	m_strCreationDate.Empty();
}

CPDFEngineXMLParser::~CPDFEngineXMLParser(void)
{
	switch (theApp.settings.m_iLanguage)
	{
		case 0:  setlocale(LC_ALL, "german");	break;
		case 1:  setlocale(LC_ALL, "english");	break;
		default: setlocale(LC_ALL, "english");	break;
	}
	m_strDocumentTitle.Empty();
	m_strCreator.Empty();
	m_strCreationDate.Empty();
}

BOOL CPDFEngineXMLParser::ParseXML(const CString& strFilePath)
{
	int			nCount = 0;
	CFileStatus status;
	while ( ! CFile::GetStatus(strFilePath, status))		
	{
		if (nCount > 20)
		{
			CString strMsg;
			strMsg.Format(_T("Error!\nCannot open file %s"), strFilePath);
			KIMOpenLogMessage(strMsg, MB_ICONERROR);
			return FALSE;
		}

		Sleep(100L);
		nCount++;
	}

	m_pJDFDoc = new JDFDoc(0);
	parser	  = new JDFParser();

	try
	{
		parser->Parse((WString)strFilePath);
	}catch (JDFException e)
	{
		CString strMsg;
		strMsg.Format(_T("xml parsing error: %s"), e.getMessage().getBytes());
		KIMOpenLogMessage(strMsg, MB_ICONERROR);
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	*m_pJDFDoc = parser->GetRoot();

	if (m_pJDFDoc->isNull())
	{
		KIMOpenLogMessage(_T("xml parsing error: Root is NULL"), MB_ICONERROR);
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	m_spotcolorDefList.RemoveAll();

	XERCES_CPP_NAMESPACE::DOMDocument* domDoc = m_pJDFDoc->GetDOMDocument(); 

	DOMNode* reportNode = domDoc->getFirstChild();
	CString strNodeName = reportNode->getNodeName();
	if (strNodeName != _T("report"))
	{
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	BOOL	 bResult = FALSE;
	DOMNode* node	 = reportNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("document"))
		{
			bResult = ProcessDocumentNode(node);
			break;
		}

		node = node->getNextSibling();
	}

	delete parser;
	delete m_pJDFDoc;

	return bResult;
}


///////// document

BOOL CPDFEngineXMLParser::ProcessDocumentNode(DOMNode* documentNode)
{
	BOOL	 bResult = FALSE;
	CString  strNodeName;
	DOMNode* node = documentNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("doc_info"))
			bResult = ProcessDocInfoNode(node);
		else
			if (strNodeName == _T("pages"))
				bResult = ProcessPagesNode(node);
			else
				if (strNodeName == _T("resources"))
					bResult = ProcessResourcesNode(node);

		node = node->getNextSibling();
	}

	return bResult;
}

BOOL CPDFEngineXMLParser::ProcessDocInfoNode(DOMNode* pagesNode)
{
	CString  strNodeName;
	DOMNode* node = pagesNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("title"))
			m_strDocumentTitle = node->getTextContent();
		else
		if (strNodeName == _T("creator"))
			m_strCreator = node->getTextContent();
		else
		if (strNodeName == _T("created"))
			m_strCreationDate = node->getTextContent();

		node = node->getNextSibling();
	}

	return TRUE;
}

BOOL CPDFEngineXMLParser::ProcessPagesNode(DOMNode* pagesNode)
{
	CString  strNodeName;
	DOMNode* node = pagesNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("page"))
			ProcessPageNode(node);

		node = node->getNextSibling();
	}

	return TRUE;
}

BOOL CPDFEngineXMLParser::ProcessPageNode(DOMNode* pageNode)
{
	CString strNr = _T("-1");
	DOMNamedNodeMap* nodeMap = pageNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"nr");
		if ( ! item)
			return FALSE;
		strNr = item->getNodeValue();
	}

	int nIndex = _ttoi(strNr) - 1;
	if ( (nIndex < 0) || (nIndex >= m_pdfPageList.GetSize()) )
	{
		CPDFEnginePageListEntry pageEntry;
		if (nIndex < 0)
		{
			pageEntry.m_nIndex = nIndex = m_pdfPageList.GetSize();
			m_pdfPageList.Add(pageEntry);
		}
		else
		{
			pageEntry.m_nIndex = nIndex;
			m_pdfPageList.SetAtGrow(nIndex, pageEntry);
		}
	}

	BOOL	 bResult = FALSE;
	CString  strNodeName;
	DOMNode* node = pageNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("platenames"))
		{
			bResult = ProcessPlatenamesNode(node, m_pdfPageList[nIndex].m_plateNames);
			break;
		}

		node = node->getNextSibling();
	}

	return TRUE;
}

BOOL CPDFEngineXMLParser::ProcessPlatenamesNode(DOMNode* platenamesNode, CStringArray& plateNames)
{
	plateNames.RemoveAll();

	CString  strNodeName;
	DOMNode* node = platenamesNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("platename"))
		{
			CString strText = node->getTextContent();
			plateNames.Add(strText);
		}

		node = node->getNextSibling();
	}

	return TRUE;
}

void CPDFEngineXMLParser::CopyOldPDFPageList(CPostScriptParser& postScriptParser)
{
	m_pdfPageList.RemoveAll();

	for (int i = 0; i < postScriptParser.m_pdfPageList.GetSize(); i++)
	{
		CPDFEnginePageListEntry newEntry;
		CPDFPageListEntry&		 rOldEntry = postScriptParser.m_pdfPageList[i];
		newEntry.m_nIndex			  = rOldEntry.m_nIndex;
		newEntry.m_nPageNum			  = rOldEntry.m_nPageNum;
		newEntry.m_strPageLabel		  = rOldEntry.m_strPageLabel;
		newEntry.m_strSeparationColor = rOldEntry.m_strSeparationColor;
		newEntry.m_fTrimBoxLeft	 = rOldEntry.m_fTrimBoxLeft;  newEntry.m_fTrimBoxBottom  = rOldEntry.m_fTrimBoxBottom;  newEntry.m_fTrimBoxRight  = rOldEntry.m_fTrimBoxRight;  newEntry.m_fTrimBoxTop  = rOldEntry.m_fTrimBoxTop;
		newEntry.m_fBleedBoxLeft = rOldEntry.m_fBleedBoxLeft; newEntry.m_fBleedBoxBottom = rOldEntry.m_fBleedBoxBottom; newEntry.m_fBleedBoxRight = rOldEntry.m_fBleedBoxRight; newEntry.m_fBleedBoxTop = rOldEntry.m_fBleedBoxTop;
		newEntry.m_fCropBoxLeft	 = rOldEntry.m_fCropBoxLeft;  newEntry.m_fCropBoxBottom	 = rOldEntry.m_fCropBoxBottom;  newEntry.m_fCropBoxRight  = rOldEntry.m_fCropBoxRight;  newEntry.m_fCropBoxTop  = rOldEntry.m_fCropBoxTop;
		newEntry.m_fMediaBoxLeft = rOldEntry.m_fMediaBoxLeft; newEntry.m_fMediaBoxBottom = rOldEntry.m_fMediaBoxBottom; newEntry.m_fMediaBoxRight = rOldEntry.m_fMediaBoxRight; newEntry.m_fMediaBoxTop = rOldEntry.m_fMediaBoxTop;
		newEntry.m_fArtBoxLeft	 = rOldEntry.m_fArtBoxLeft;   newEntry.m_fArtBoxBottom	 = rOldEntry.m_fArtBoxBottom;	newEntry.m_fArtBoxRight   = rOldEntry.m_fArtBoxRight;   newEntry.m_fArtBoxTop   = rOldEntry.m_fArtBoxTop;
		newEntry.m_plateNames.RemoveAll();
	}
}

////////// resources

BOOL CPDFEngineXMLParser::ProcessResourcesNode(DOMNode* pagesNode)
{
	CString  strNodeName;
	DOMNode* node = pagesNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("colorspaces"))
			ProcessColorspacesNode(node);

		node = node->getNextSibling();
	}

	return TRUE;
}

BOOL CPDFEngineXMLParser::ProcessColorspacesNode(DOMNode* colorspacesNode)
{
	CString  strNodeName;
	DOMNode* node = colorspacesNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("colorspace"))
			ProcessColorspaceNode(node);

		node = node->getNextSibling();
	}

	return TRUE;
}

BOOL CPDFEngineXMLParser::ProcessColorspaceNode(DOMNode* colorspaceNode)
{
	CString strType = _T("");
	DOMNamedNodeMap* nodeMap = colorspaceNode->getAttributes();
	if (nodeMap)
	{
		DOMNode* item = nodeMap->getNamedItem(L"type");
		if ( ! item)
			return FALSE;
		strType = item->getNodeValue();
	}
	if (strType.IsEmpty())
		return FALSE;

	if ((strType.CompareNoCase(_T("Separation")) == 0) )
		return ProcessColorspaceSeparationNode(colorspaceNode);
	else
		if ((strType.CompareNoCase(_T("DeviceN")) == 0) )
			return ProcessColorspaceDeviceNNode(colorspaceNode);

	return TRUE;
}

BOOL CPDFEngineXMLParser::ProcessColorspaceSeparationNode(DOMNode* separationNode)
{
	BOOL	 bResult = FALSE;
	CString	 strNodeName;
	DOMNode* node = separationNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("name"))
		{
			CSpotcolorDefs entry;
			entry.m_strSpotColorName = node->getTextContent();
			if (ColorSpaceIsSpot(entry.m_strSpotColorName))
			{
				CString strAlternateColorspace;
				node = node->getNextSibling();
				while (node)
				{
					strNodeName = node->getNodeName();
					if (strNodeName == _T("alternate_colorspace"))
					{
						strAlternateColorspace = node->getTextContent();
					}
					if (strNodeName == _T("alternate_values"))
					{
						bResult = ProcessAlternateValuesNode(node, entry, strAlternateColorspace);
						m_spotcolorDefList.AddEntry(entry);
						break;
					}
					node = node->getNextSibling();
				}
			}
		}

		node = node->getNextSibling();
	}

	return TRUE;
}

BOOL CPDFEngineXMLParser::ProcessColorspaceDeviceNNode(DOMNode* deviceNNode)
{
	BOOL	 bResult = FALSE;
	CString	 strNodeName;
	DOMNode* node = deviceNNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("alternate_colorants"))
		{
			DOMNode* childNode = node->getFirstChild();
			while (childNode)
			{
				strNodeName = childNode->getNodeName();
				if (strNodeName == _T("colorant"))
					bResult = ProcessColorspaceColorantNode(childNode);

				childNode = childNode->getNextSibling();
			}
		}

		node = node->getNextSibling();
	}

	return TRUE;
}

BOOL CPDFEngineXMLParser::ProcessColorspaceColorantNode(DOMNode* colorantNode)
{
	BOOL	 bResult = FALSE;
	CString	 strNodeName;
	DOMNode* node = colorantNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("name"))
		{
			CSpotcolorDefs entry;
			entry.m_strSpotColorName = node->getTextContent();
			if (ColorSpaceIsSpot(entry.m_strSpotColorName))
			{
				CString strAlternateColorspace;
				node = node->getNextSibling();
				while (node)
				{
					strNodeName = node->getNodeName();
					if (strNodeName == _T("alternate_colorspace"))
					{
						strAlternateColorspace = node->getTextContent();
					}
					if (strNodeName == _T("alternate_values"))
					{
						bResult = ProcessAlternateValuesNode(node, entry, strAlternateColorspace);
						m_spotcolorDefList.AddEntry(entry);
						break;
					}
					node = node->getNextSibling();
				}
			}
		}

		node = node->getNextSibling();
	}

	return TRUE;
}

BOOL CPDFEngineXMLParser::ProcessAlternateValuesNode(DOMNode* alternateValuesNode, CSpotcolorDefs& entry, const CString& strAlternateColorspace)
{
	float fLi1 = 0.0f, fLi2 = 0.0f, fLi3 = 0.0f, fLi4 = 0.0f;

	int		 nIndex = 0;
	CString  strNodeName;
	DOMNode* node = alternateValuesNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName == _T("li"))
		{
			switch (nIndex)
			{
			case 0:	fLi1 = (float)_tstof(CString(node->getTextContent()));	break;
			case 1:	fLi2 = (float)_tstof(CString(node->getTextContent()));	break;
			case 2:	fLi3 = (float)_tstof(CString(node->getTextContent()));	break;
			case 3:	fLi4 = (float)_tstof(CString(node->getTextContent()));	break;
			}
			nIndex++;
		}

		node = node->getNextSibling();
	}

	if (strAlternateColorspace.CompareNoCase(_T("DeviceCMYK")) == 0)
	{
		entry.m_alternateValues.fC = fLi1;
		entry.m_alternateValues.fM = fLi2; 
		entry.m_alternateValues.fY = fLi3; 
		entry.m_alternateValues.fK = fLi4; 
	}
	else
	if (strAlternateColorspace.CompareNoCase(_T("DeviceRGB")) == 0)
	{
		COLORREF crCMYK = CColorDefinition::RGB2CMYK(RGB(fLi1*255, fLi3*255, fLi3*255));
		entry.m_alternateValues.fC = (float)GetCValue(crCMYK)/100.0f;
		entry.m_alternateValues.fM = (float)GetMValue(crCMYK)/100.0f; 
		entry.m_alternateValues.fY = (float)GetYValue(crCMYK)/100.0f; 
		entry.m_alternateValues.fK = (float)GetKValue(crCMYK)/100.0f; 
	}
	else
	if (strAlternateColorspace.CompareNoCase(_T("DeviceGray")) == 0)
	{
		entry.m_alternateValues.fK = fLi1;
	}
	else
	if (strAlternateColorspace.CompareNoCase(_T("LAB")) == 0)
	{
		COLORREF crCMYK = CColorDefinition::LAB2CMYK(fLi1, fLi3, fLi3);
		entry.m_alternateValues.fC = (float)GetCValue(crCMYK)/100.0f;
		entry.m_alternateValues.fM = (float)GetMValue(crCMYK)/100.0f; 
		entry.m_alternateValues.fY = (float)GetYValue(crCMYK)/100.0f; 
		entry.m_alternateValues.fK = (float)GetKValue(crCMYK)/100.0f; 
	}
	else
	if (strAlternateColorspace.CompareNoCase(_T("Separation")) == 0)
	{
		entry.m_alternateValues.fK = fLi1;
	}
	else
	{
		entry.m_alternateValues.fC = fLi1;
		entry.m_alternateValues.fM = fLi2; 
		entry.m_alternateValues.fY = fLi3; 
		entry.m_alternateValues.fK = fLi4; 
	}

	return TRUE;
}

BOOL CPDFEngineXMLParser::ColorSpaceIsSpot(const CString& strColorSpace)
{
	BOOL bIsSpotColor = TRUE;
	if (strColorSpace == _T("DeviceCMYK"))				 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("DeviceRGB"))				 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("DeviceGray"))				 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("CalRGB"))					 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("CalGray"))					 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("Lab"))						 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("ICCBased"))				 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("Separation"))				 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("DeviceN"))					 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("Indexed"))					 bIsSpotColor = FALSE; else
	if (strColorSpace == _T("Pattern"))					 bIsSpotColor = FALSE; else
	if (strColorSpace.CompareNoCase(_T("Black"))   == 0) bIsSpotColor = FALSE; else
	if (strColorSpace.CompareNoCase(_T("Cyan"))	   == 0) bIsSpotColor = FALSE; else
	if (strColorSpace.CompareNoCase(_T("Magenta")) == 0) bIsSpotColor = FALSE; else
	if (strColorSpace.CompareNoCase(_T("Yellow"))  == 0) bIsSpotColor = FALSE; else
	if (strColorSpace.CompareNoCase(_T("All"))	   == 0) bIsSpotColor = FALSE; else
	if (strColorSpace.CompareNoCase(_T("None"))	   == 0) bIsSpotColor = FALSE;

	return bIsSpotColor;
}
