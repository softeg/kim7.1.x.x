// DistillerSink.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DistillerSink.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDistillerSink

IMPLEMENT_DYNCREATE(CDistillerSink, CCmdTarget)

CDistillerSink::CDistillerSink()
{
	EnableAutomation();
}

CDistillerSink::~CDistillerSink()
{
}


void CDistillerSink::OnFinalRelease()
{
	// Nachdem die letzte Referenz auf ein Automatisierungsobjekt freigegeben wurde,
	// wird OnFinalRelease aufgerufen. Die Basisklasse l�scht das Objekt
	// automatisch. F�gen Sie zus�tzlichen Bereinigungscode f�r Ihr Objekt
	// hinzu, bevor Sie die Basisklasse aufrufen.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(CDistillerSink, CCmdTarget)
	//{{AFX_MSG_MAP(CDistillerSink)
		// HINWEIS - Der Klassen-Assistent f�gt hier Zuordnungsmakros ein und entfernt diese.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CDistillerSink, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CDistillerSink)
	DISP_FUNCTION(CDistillerSink, "OnJobDone", OnJobDone, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CDistillerSink, "OnJobStart", OnJobStart, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CDistillerSink, "OnLogMessage", OnLogMessage, VT_EMPTY, VTS_BSTR)
	DISP_FUNCTION(CDistillerSink, "OnPageNumber", OnPageNumber, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CDistillerSink, "OnJobFail", OnJobFail, VT_EMPTY, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CDistillerSink, "OnPercentDone", OnPercentDone, VT_EMPTY, VTS_I4)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Hinweis: Wir stellen Unterst�tzung f�r IID_IDistillerSink zur Verf�gung, um typsicheres Binden
//  von VBA zu erm�glichen. Diese IID muss mit der GUID �bereinstimmen, die in der
//  Disp-Schnittstelle in der .ODL-Datei angegeben ist.

// moved to header file for use in other files
// {F093C491-ED00-11D1-B976-00600802DB86}
//static const IID IID_IDistillerSink =
//{ 0xF093C491, 0xED00, 0x11D1, { 0xB9, 0x76, 0x0, 0x60, 0x08, 0x02, 0xDB, 0x86 } };

BEGIN_INTERFACE_MAP(CDistillerSink, CCmdTarget)
	INTERFACE_PART(CDistillerSink, IID_IDistillerSink, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDistillerSink 


void CDistillerSink::OnJobDone(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF) 
{
	// ZU ERLEDIGEN: F�gen Sie hier den Code f�r Ihre Dispatch-Behandlungsroutine ein

}

void CDistillerSink::OnJobStart(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF) 
{
	// ZU ERLEDIGEN: F�gen Sie hier den Code f�r Ihre Dispatch-Behandlungsroutine ein

}

void CDistillerSink::OnLogMessage(LPCTSTR strMessage) 
{
	// ZU ERLEDIGEN: F�gen Sie hier den Code f�r Ihre Dispatch-Behandlungsroutine ein

}

void CDistillerSink::OnPageNumber(long nPageNumber) 
{
	// ZU ERLEDIGEN: F�gen Sie hier den Code f�r Ihre Dispatch-Behandlungsroutine ein

}

void CDistillerSink::OnJobFail(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF) 
{
	// ZU ERLEDIGEN: F�gen Sie hier den Code f�r Ihre Dispatch-Behandlungsroutine ein

}

void CDistillerSink::OnPercentDone(long nPercentDone) 
{
	// ZU ERLEDIGEN: F�gen Sie hier den Code f�r Ihre Dispatch-Behandlungsroutine ein

}
