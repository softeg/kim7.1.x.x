// DlgProductionBindingParams.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionBindingParams.h"
#include "DlgProductionManager.h"
#include "DlgProductionBoundDetails.h"
#include "GraphicComponent.h"
#include "PrintComponentsView.h"
#include "InplaceEdit.h"
#include "LayoutDataWindow.h"


// CPBPCustomPaintFrame

IMPLEMENT_DYNAMIC(CPBPCustomPaintFrame, CStatic)

CPBPCustomPaintFrame::CPBPCustomPaintFrame()
{
}

CPBPCustomPaintFrame::~CPBPCustomPaintFrame()
{
}


BEGIN_MESSAGE_MAP(CPBPCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CPPBCustomPaintFrame-Meldungshandler

void CPBPCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CString strText;
	CRect rcFrame;
	GetClientRect(rcFrame);

	switch (GetDlgCtrlID())
	{
	case IDC_PP_PAGES_PREVIEWFRAME:		DrawPages(&dc,	rcFrame);	break;
	default:														break;
	}
}


void CPBPCustomPaintFrame::DrawPages(CDC* pDC, CRect frameRect)
{
	CDlgProductionBindingParams* pDlg = (CDlgProductionBindingParams*)GetParent();
	if ( ! pDlg)
		return;
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CFoldingProcess&	rFoldingProcess	   = pDlg->GetFoldingProcess();
	CBoundProductPart&	rProductPart	   = pDlg->GetBoundProductPart();
	int					nProductPartIndex  = rProductPart.GetIndex();

	CBrush brush(RGB(127,162,207));
	CPen   pen(PS_SOLID, 1, LIGHTGRAY);
	CBrush* pOldBrush = pDC->SelectObject(&brush);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);

	pDC->SaveDC();	// save current clipping region
	CRect rcClip;
	pDlg->GetDlgItem(IDC_PD_PAGESPREVIEW_SPIN)->GetClientRect(rcClip);
	rcClip.OffsetRect(1, 1);
	pDC->ExcludeClipRect(rcClip);
	pDC->Rectangle(frameRect);
	pDC->RestoreDC(-1);;

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	brush.DeleteObject();
	pen.DeleteObject();

	if (rFoldingProcess.m_bShinglingActive)
	{
		CRect rcText = frameRect; 
		rcText.left += 2; rcText.bottom -= 2;
		CFont font;
		font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		CFont* pOldFont = pDC->SelectObject(&font);
		pDC->SetTextColor(BROWN);
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(255,230,180));
		CString string; string.LoadString(IDS_SHINGLING_ACTIVE);
		pDC->DrawText(string, rcText, DT_LEFT | DT_BOTTOM | DT_SINGLELINE);
		pDC->SelectObject(pOldFont);
		font.DeleteObject();
	}

	frameRect.DeflateRect(15, 15);

	if (nProductPartIndex >= 0)
	{
		if (pDlg->m_nPagePreviewIndex < 0)
			pDlg->m_nPagePreviewIndex = 0;
		else
			if (pDlg->m_nPagePreviewIndex >= rProductPart.m_nNumPages)
				pDlg->m_nPagePreviewIndex = rProductPart.m_nNumPages - 1;

		frameRect.DeflateRect(5,10); frameRect.left += 10; frameRect.top += 5;
		pDoc->m_PageTemplateList.Draw(pDC, frameRect, pDlg->m_nPagePreviewIndex, nProductPartIndex, TRUE, FALSE, TRUE, TRUE, NULL, FALSE, FALSE, pDlg->GetPrepressParams().m_fBleed);
	}
}




// CDlgProductionBindingParams-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionBindingParams, CDialog)

CDlgProductionBindingParams::CDlgProductionBindingParams(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionBindingParams::IDD, pParent)
{
	m_fSpineArea		= 0.0f;
	m_bSpineAreaAuto	= FALSE;
	m_fMillingDepth	 	= 0.0f;
	m_fOverfold		 	= 0.0f;
	m_bOverfoldFront 	= FALSE;
	m_bOverfoldRear	 	= FALSE;

	m_nPagePreviewIndex = 0;
	m_bLocked = FALSE;	
	m_ptInitialPos = CPoint(0,0);
}

CDlgProductionBindingParams::~CDlgProductionBindingParams()
{
}

int CDlgProductionBindingParams::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		SetParent(m_pParentWnd);
		ModifyStyle(WS_OVERLAPPEDWINDOW | WS_POPUPWINDOW, WS_CHILD);
		ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);
	}

	return 0;
}

BOOL CDlgProductionBindingParams::Create(UINT nIDTemplate, CWnd* pParentWnd)
{
	m_pParentWnd = pParentWnd;
	return CDialog::Create(nIDTemplate, pParentWnd);
}

BOOL CDlgProductionBindingParams::OnEraseBkgnd(CDC* pDC)
{
	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		return CDialog::OnEraseBkgnd(pDC);

	CRect rcFrame;
	GetClientRect(rcFrame);

	CGraphicComponent gc;
	gc.DrawPopupWindowBackground(pDC, rcFrame);

	return TRUE;
}

void CDlgProductionBindingParams::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
		{
			if (m_hWnd)
			{
				if ( ! m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
				{
					m_bLocked = TRUE;
					EndModalLoop(IDCANCEL);
					DestroyWindow();
				}
			}
		}
	}
	else
		m_bLocked = FALSE;
}

void CDlgProductionBindingParams::OnBnClickedOk()
{
	SaveData();
	EndModalLoop(IDOK);
	m_bLocked = TRUE;
	DestroyWindow();
}

CPrepressParams& CDlgProductionBindingParams::GetPrepressParams()
{
	static CPrepressParams nullPrepressParams;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager* pParent = (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile.m_prepress;
	}
	else
		if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
		{
			CLayoutDataWindow* pParent = (CLayoutDataWindow*)m_pParentWnd;
			return pParent->GetPrintingProfile().m_prepress;
		}

	return nullPrepressParams;
}

CBindingProcess& CDlgProductionBindingParams::GetBindingProcess()
{
	static CBindingProcess nullBindingProcess;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile.m_postpress.m_binding;
	}
	else
		if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
		{
			return GetBoundProductPart().GetBoundProduct().m_bindingParams;
		}

	return nullBindingProcess;
}

CFoldingProcess& CDlgProductionBindingParams::GetFoldingProcess()
{
	static CFoldingProcess nullFoldingProcess;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile.m_postpress.m_folding;
	}
	else
		if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
		{
			return GetBoundProductPart().m_foldingParams;
		}

	return nullFoldingProcess;
}

CBoundProductPart& CDlgProductionBindingParams::GetBoundProductPart()
{
	static CBoundProductPart nullProductPart;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent				= (CDlgProductionManager*)m_pParentWnd;
		CProductionProfile&		rProductionProfile	= pParent->m_productionProfile;
		CPrintGroup*			pPrintGroup			= pParent->m_pPrintGroup;
		int						nComponentIndex		= pParent->m_nComponentIndex;
		CPrintGroupComponent*	pComponent			= (pPrintGroup) ? ( (nComponentIndex == -1) ? pPrintGroup->GetFirstBoundComponent() : &pPrintGroup->m_componentList[nComponentIndex]) : NULL;
		int						nProductPartIndex	= (pComponent) ? pComponent->m_nProductClassIndex : -1;
		return (CImpManDoc::GetDoc()) ? CImpManDoc::GetDoc()->GetBoundProductPart(nProductPartIndex) : nullProductPart;
	}
	else
		if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
		{
			CPrintComponentsView* pView		   = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
			CPrintComponent		  actComponent = pView->GetActComponent();
			CBoundProductPart*	  pProductPart = actComponent.GetBoundProductPart();
			return (pProductPart) ? *pProductPart : nullProductPart;
		}

	return nullProductPart;
}

void CDlgProductionBindingParams::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_PD_SPINEAREA_SPIN, m_spineAreaSpin);
	DDX_Check(pDX, IDC_PD_SPINEAREA_AUTO, m_bSpineAreaAuto);
	DDX_Control(pDX, IDC_PD_MILLINGDEPTH_SPIN, m_millingDepthSpin);
	DDX_Control(pDX, IDC_PD_OVERFOLD_SPIN, m_overFoldSpin);
	DDX_Measure(pDX, IDC_PD_SPINEAREA_EDIT, m_fSpineArea);
	DDX_Measure(pDX, IDC_PD_MILLINGDEPTH_EDIT, m_fMillingDepth);
	DDX_Measure(pDX, IDC_PD_OVERFOLD_EDIT, m_fOverfold);
	DDX_Check(pDX, IDC_PD_OVERFOLD_FRONT, m_bOverfoldFront);
	DDX_Check(pDX, IDC_PD_OVERFOLD_REAR, m_bOverfoldRear);
	DDX_Text(pDX, IDC_PD_DETAILS_STATIC, m_strDetails);
}


BEGIN_MESSAGE_MAP(CDlgProductionBindingParams, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_SPINEAREA_SPIN, OnDeltaposSpineAreaSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_MILLINGDEPTH_SPIN, OnDeltaposMillingDepthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_OVERFOLD_SPIN, OnDeltaposOverfoldSpin)
	ON_EN_KILLFOCUS(IDC_PD_SPINEAREA_EDIT, OnKillfocusSpineAreaEdit)
	ON_EN_KILLFOCUS(IDC_PD_MILLINGDEPTH_EDIT, OnKillfocusMillingDepthEdit)
	ON_EN_KILLFOCUS(IDC_PD_OVERFOLD_EDIT, OnKillfocusOverfoldEdit)
	ON_BN_CLICKED(IDC_PD_OVERFOLD_FRONT, OnOverfoldFront)
	ON_BN_CLICKED(IDC_PD_OVERFOLD_REAR, OnOverfoldRear)
	ON_BN_CLICKED(IDC_PD_DETAILS_BUTTON, OnBnClickedPdDetailsButton)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_PAGESPREVIEW_SPIN, OnDeltaposPdPagespreviewSpin)
	ON_BN_CLICKED(IDC_PD_SPINEAREA_AUTO, OnBnClickedPdSpineareaAuto)
END_MESSAGE_MAP()


// CDlgProductionBindingParams-Meldungshandler

BOOL CDlgProductionBindingParams::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_pagesPreviewFrame.SubclassDlgItem	(IDC_PP_PAGES_PREVIEWFRAME,		this);

	if (m_ptInitialPos != CPoint(0,0))
	{
		CSize szScreen = CSize(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
		CRect rcDlg; GetWindowRect(rcDlg);
		if ( (m_ptInitialPos.y + rcDlg.Height()) <= szScreen.cy)
			SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		else
			SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y - rcDlg.Height() - 12, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
	}

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);

	m_spineAreaSpin.SetRange(0, 1);  
	m_millingDepthSpin.SetRange(0, 1);  
	m_overFoldSpin.SetRange(0, 1);  

	((CSpinButtonCtrl*)GetDlgItem(IDC_PD_PAGESPREVIEW_SPIN))->SetRange(0, 1);

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProductionBindingParams::InitData()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBoundProductPart& rProductPart		 = GetBoundProductPart();
	CBindingProcess&   rBindingProcess	 = GetBindingProcess();
	
	if (rProductPart.IsCover())
		m_fSpineArea = rBindingProcess.GetSpine(rProductPart);
	else
		m_fSpineArea = rBindingProcess.m_fSpineArea;
	m_bSpineAreaAuto = rBindingProcess.m_bSpineAuto;

	m_fMillingDepth  = rBindingProcess.m_fMillingDepth;
	m_fOverfold		 = rBindingProcess.m_fOverFold;
	switch (rBindingProcess.m_nOverFoldSide)
	{
	case CProductPart::OverFoldNone:	m_bOverfoldFront = FALSE; m_bOverfoldRear = FALSE; break;
	case CProductPart::OverFoldFront:	m_bOverfoldFront = TRUE;  m_bOverfoldRear = FALSE; break;
	case CProductPart::OverFoldBack:	m_bOverfoldFront = FALSE; m_bOverfoldRear = TRUE;  break;
	}

	UpdateDetailsString();
	UpdateData(FALSE);

	GetDlgItem(IDC_PD_SPINEAREA_EDIT)->EnableWindow((m_bSpineAreaAuto) ? FALSE : TRUE);
	GetDlgItem(IDC_PD_SPINEAREA_SPIN)->EnableWindow((m_bSpineAreaAuto) ? FALSE : TRUE);

	m_pagesPreviewFrame.Invalidate();
}

void CDlgProductionBindingParams::SaveData()
{
	UpdateData(TRUE);

	CBindingProcess& rBindingProcess = GetBindingProcess();

	rBindingProcess.m_fSpineArea	= m_fSpineArea;
	rBindingProcess.m_bSpineAuto	= m_bSpineAreaAuto;
	rBindingProcess.m_fMillingDepth = m_fMillingDepth;
	rBindingProcess.m_fOverFold		= m_fOverfold;
	if (m_bOverfoldFront && ! m_bOverfoldRear)
		rBindingProcess.m_nOverFoldSide = CProductPart::OverFoldFront;
	else
		if ( ! m_bOverfoldFront && m_bOverfoldRear)
			rBindingProcess.m_nOverFoldSide = CProductPart::OverFoldBack;
		else
			rBindingProcess.m_nOverFoldSide = CProductPart::OverFoldNone;
}

void CDlgProductionBindingParams::UpdateDetailsString()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBindingProcess& rBindingProcess = GetBindingProcess();

	m_strDetails.Empty();
	if (fabs(rBindingProcess.m_fGlueLineWidth) > 0.01f)
	{
		CString strGlueLine; strGlueLine.LoadString(IDS_GLUELINE);
		CString strMeasureFormat;
		strMeasureFormat.Format(_T("%s: %s %s\n"), strGlueLine, (LPCTSTR)MeasureFormat("%.1f"), GetUnitString());
		m_strDetails.Format(strMeasureFormat, rBindingProcess.m_fGlueLineWidth);
		CString strContentCutScale; 
		if (GetPrepressParams().m_bAdjustContentToMask)
			strContentCutScale.LoadString(IDS_SCALE_CONTENT);
		else
			strContentCutScale.LoadString(IDS_CUT_CONTENT);
		m_strDetails += strContentCutScale + _T("\n\n");
	}

	if (rBindingProcess.m_bComingAndGoing)	
	{
		CString strCG; strCG.LoadString(IDS_COMING_AND_GOING);
		m_strDetails += strCG + _T("\n\n");
	}
	//m_strDetails += pDoc->m_productPartList[m_nProductPartIndex].m_strComment;
}

void CDlgProductionBindingParams::OnDeltaposSpineAreaSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_spineAreaSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusSpineAreaEdit();
	*pResult = 0;
}

void CDlgProductionBindingParams::OnDeltaposMillingDepthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_millingDepthSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusMillingDepthEdit();
	*pResult = 0;
}

void CDlgProductionBindingParams::OnDeltaposOverfoldSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_overFoldSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	*pResult = 0;

	UpdateData(TRUE);
	if (m_fOverfold == 0.0f)
		m_bOverfoldFront = m_bOverfoldRear = FALSE;
	else
		if ( ! m_bOverfoldFront && ! m_bOverfoldRear)
			m_bOverfoldFront = TRUE;
	UpdateData(FALSE);

	UpdatePreview();
}

void CDlgProductionBindingParams::OnKillfocusSpineAreaEdit() 
{
	UpdateData(TRUE);
	if (fabs(m_fMillingDepth) <= 0.01)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			UpdateDetailsString();
			UpdateData(FALSE);
		}
	}
	UpdatePreview();
}

void CDlgProductionBindingParams::OnBnClickedPdSpineareaAuto()
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	UpdateData(TRUE);

	CBindingProcess&	rBindingProcess = GetBindingProcess();
	CBoundProductPart&	rProductPart	= GetBoundProductPart();
	
	rBindingProcess.m_bSpineAuto = m_bSpineAreaAuto;
	if (rProductPart.IsCover())
		m_fSpineArea = rBindingProcess.GetSpine(rProductPart);

	InitData();
	UpdatePreview();
}

void CDlgProductionBindingParams::OnKillfocusMillingDepthEdit() 
{
	UpdateData(TRUE);
	if (fabs(m_fMillingDepth) <= 0.01)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			UpdateDetailsString();
			UpdateData(FALSE);
		}
	}
	UpdatePreview();
}

void CDlgProductionBindingParams::OnKillfocusOverfoldEdit() 
{
	UpdateData(TRUE);
	if (m_fOverfold == 0.0f)
		m_bOverfoldFront = m_bOverfoldRear = FALSE;
	else
		if ( ! m_bOverfoldFront && ! m_bOverfoldRear)
			m_bOverfoldFront = TRUE;
	UpdateData(FALSE);
	UpdatePreview();
}

void CDlgProductionBindingParams::OnOverfoldFront()
{
	UpdateData(TRUE);

	if (m_bOverfoldFront)
		m_bOverfoldRear = FALSE;
	else
		if ( ! m_bOverfoldRear)
			m_fOverfold = 0.0f;

	UpdateData(FALSE);
	UpdatePreview();
}

void CDlgProductionBindingParams::OnOverfoldRear()
{
	UpdateData(TRUE);

	if (m_bOverfoldRear)
		m_bOverfoldFront = FALSE;
	else
		if ( ! m_bOverfoldFront)
			m_fOverfold = 0.0f;

	UpdateData(FALSE);
	UpdatePreview();
}

void CDlgProductionBindingParams::OnBnClickedPdDetailsButton()
{
	UpdateData(TRUE);

	SaveData();

	CBoundProductPart&  rProductPart	  = GetBoundProductPart();
	int					nProductPartIndex = rProductPart.GetIndex();
	CPrepressParams&	rPrepressParams	  = GetPrepressParams();
	CBindingProcess&	rBindingProcess	  = GetBindingProcess();
	CFoldingProcess&	rFoldingProcess   = GetFoldingProcess();

	CDlgProductionBoundDetails dlg;
	dlg.m_nProductPartIndex = nProductPartIndex;

	dlg.m_fBleed				=  rPrepressParams.m_fBleed;
	dlg.m_fGlueLine				=  rBindingProcess.m_fGlueLineWidth;
	dlg.m_fSpineArea			=  rBindingProcess.m_fSpineArea;
	dlg.m_bAdjustContentToMask	=  rPrepressParams.m_bAdjustContentToMask;
	dlg.m_nProductionType		= (rBindingProcess.m_bComingAndGoing) ? 1 : 0;
	dlg.m_bShinglingActive		=  rFoldingProcess.m_bShinglingActive;

	m_bLocked = TRUE;

	int nRet = dlg.DoModal();

	m_bLocked = FALSE;

	if (nRet == IDCANCEL)
		return;

	rPrepressParams.m_fBleed				= dlg.m_fBleed;
	rBindingProcess.m_fGlueLineWidth		= dlg.m_fGlueLine;
	rPrepressParams.m_bAdjustContentToMask	= dlg.m_bAdjustContentToMask;
	rBindingProcess.m_bComingAndGoing		= (dlg.m_nProductionType == 1) ? TRUE : FALSE;

	UpdateDetailsString();	

	UpdateData(FALSE);

	m_pagesPreviewFrame.Invalidate();
}

void CDlgProductionBindingParams::UpdatePreview()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBindingProcess& rBindingProcess = GetBindingProcess();

	rBindingProcess.m_fSpineArea	= m_fSpineArea;
	rBindingProcess.m_fMillingDepth = m_fMillingDepth;

	rBindingProcess.m_fOverFold = m_fOverfold;
	if (m_bOverfoldFront)
		rBindingProcess.m_nOverFoldSide = CProductPart::OverFoldFront;
	else
		if (m_bOverfoldRear)
			rBindingProcess.m_nOverFoldSide = CProductPart::OverFoldBack;
		else
			rBindingProcess.m_nOverFoldSide = CProductPart::OverFoldNone;

	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgProductionBindingParams::OnDeltaposPdPagespreviewSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;

	m_nPagePreviewIndex += (pNMUpDown->iDelta > 0) ? 2 : -2;

	if (m_nPagePreviewIndex < 0)
		m_nPagePreviewIndex = 0;
	else
	{
		CBoundProductPart& rProductPart	= GetBoundProductPart();
		if ( ! rProductPart.IsNull())
		{
			if (m_nPagePreviewIndex >= rProductPart.m_nNumPages)
				m_nPagePreviewIndex = rProductPart.m_nNumPages - 1;
		}
	}

	if (m_nPagePreviewIndex > 0)
		if ( ! (m_nPagePreviewIndex%2) )	// even
			m_nPagePreviewIndex--;			// always set to odd index -> left page

	UpdateData(FALSE);

	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}
