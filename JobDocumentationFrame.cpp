// JobDocumentationFrame.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "JobDocumentationFrame.h"
#include "JobDocumentationView.h"
#include "JobDocumentationSettingsView.h"



IMPLEMENT_DYNAMIC(CJobDocumentationSplitter, CSplitterWnd)

BEGIN_MESSAGE_MAP(CJobDocumentationSplitter, CSplitterWnd)
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()


// CJobDocumentationSplitter message handlers


afx_msg LRESULT CJobDocumentationSplitter::OnNcHitTest(CPoint point)
{
    return HTNOWHERE;
//	return CSplitterWnd::OnNcHitTest(point);
}

void CJobDocumentationSplitter::OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect)
{
	if (pDC == NULL)
	{
		RedrawWindow(rect, NULL, RDW_INVALIDATE|RDW_NOCHILDREN);
		return;
	}

	CRect rcRect = rect;
	switch (nType)
	{
	case splitBorder:
		pDC->Draw3dRect(rcRect, MIDDLEGRAY, LIGHTGRAY);
		rcRect.InflateRect(-1, -1);
		pDC->Draw3dRect(rcRect, RGB(225,230,235), RGB(225,230,235));
		return;

	case splitIntersection:
		break;

	case splitBox:
		pDC->Draw3dRect(rcRect, LIGHTGRAY, WHITE);
		rcRect.InflateRect(-1, -1);
		pDC->Draw3dRect(rcRect, LIGHTGRAY, MIDDLEGRAY);
		rcRect.InflateRect(-1, -1);
		break;

	case splitBar:
		{
			CPen pen(PS_SOLID, 1, RGB(165,190,250));
			CBrush brush(RGB(193,211,251)); 
			pDC->SelectObject(&pen);
			pDC->SelectObject(&brush);
			pDC->Rectangle(rcRect);
		}
		break;

	default:
		ASSERT(FALSE);  // unknown splitter type
	}
}



// CJobDocumentationFrame

IMPLEMENT_DYNCREATE(CJobDocumentationFrame, CMDIChildWnd)

CJobDocumentationFrame::CJobDocumentationFrame()
{

}

CJobDocumentationFrame::~CJobDocumentationFrame()
{
}


BEGIN_MESSAGE_MAP(CJobDocumentationFrame, CMDIChildWnd)
	ON_COMMAND(ID_FILE_PAGE_SETUP, OnFilePageSetup)
END_MESSAGE_MAP()

// CJobDocumentationFrame-Meldungshandler

BOOL CJobDocumentationFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	if ( ! m_wndSplitter.CreateStatic(this, 2, 1))
		return FALSE;

	if ( ! m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CJobDocumentationSettingsView), CSize(0, 50), pContext))
		return FALSE;

	if ( ! m_wndSplitter.CreateView(1, 0, pContext->m_pNewViewClass, CSize(0, 0), pContext))
		return FALSE;

	return TRUE; 
}

void CJobDocumentationFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	//CMDIChildWnd::OnUpdateFrameTitle(bAddToTitle);    // do nothing (done in CMainFrame)
	::SetWindowText(this->GetSafeHwnd(), _T(""));

	//CDocument* pDoc = GetActiveDocument();
	//if (pDoc)
	//{
	//	CString title = pDoc->GetTitle();
	//	//title += _T(" - ");
	//	//CString string;
	//	//string.LoadString(IDS_NAV_JOBDOCUMENTATION);
	//	//title += string;
	//	::SetWindowText(this->GetSafeHwnd(), title);
	//}
}

void CJobDocumentationFrame::OnFilePageSetup() 
{
	CJobDocumentationSettingsView* pView = (CJobDocumentationSettingsView*)m_wndSplitter.GetPane(0, 0);
	if (pView)
		pView->OnBnClickedJobdocPagesetupButton();
}