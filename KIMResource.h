
/// manually defined resource ids
#define ID_BASE 50000
#define ID_SELECT_PRINTSHEET					ID_BASE + 2
#define ID_SELECT_PRINTSHEET_ALL				ID_BASE + 3
#define ID_SELECT_TRIMMINGDATA					ID_BASE + 4
#define ID_SELECT_BINDINGDATA					ID_BASE + 5
#define ID_SELECT_FOLDSCHEME					ID_BASE + 6
#define ID_SELECT_PRINTLAYOUT					ID_BASE + 7
#define ID_SELECT_PREPRESSDATA					ID_BASE + 8
#define ID_SELECT_PRINTSHEETVIEW_STRIPPINGDATA	ID_BASE + 10
#define ID_NAV_PREPRESS							ID_BASE + 15
#define ID_NAV_PRODDATA_GRAPHIC					ID_BASE + 16
#define ID_NAV_PRODDATA_TABLE					ID_BASE + 17
#define ID_NAV_PRODDATA_STRIPPING				ID_BASE + 18
#define ID_NAV_PRODDATA_JOBTICKET				ID_BASE + 19
#define ID_PROCESS_PREPRESS						ID_BASE + 20
#define ID_RIPMESSAGES							ID_BASE + 21
#define ID_DO_IMPOSE							ID_BASE + 22
#define ID_DO_IMPOSE_AUTO						ID_BASE + 23
#define ID_DO_IMPOSE_REMOVE_ALL					ID_BASE + 24
#define ID_ADDNEW_PRINTSHEET					ID_BASE + 25
#define ID_REMOVE_PRINTSHEET					ID_BASE + 26
#define ID_ADDLABEL_THISSHEET					ID_BASE + 27
#define ID_IA_NEXT_STEP							ID_BASE + 28
#define ID_OUTPUT_PROFILE_SETTINGS				ID_BASE + 29
#define ID_OUTPUT_MASK_SETTINGS					ID_BASE + 30
#define ID_OUTPUT_COLOR_SETTINGS				ID_BASE + 31
#define ID_SHOW_CUTBLOCKS						ID_BASE + 32
#define ID_SWAP_OBJECTS							ID_BASE + 33
#define ID_JOBDOC_ZOOM_MULTIPAGE				ID_BASE + 34
#define ID_JOBDOC_ZOOM_PAGEFIT					ID_BASE + 35
#define ID_JOBDOC_ZOOM_PAGEWIDTH				ID_BASE + 36
#define ID_MODIFY_SHINGLING						ID_BASE + 37
#define ID_FOLDSHEET_STEP_UP					ID_BASE + 38
#define ID_FOLDSHEET_STEP_DOWN					ID_BASE + 39
#define ID_FOLDSHEET_ADD						ID_BASE + 40
#define ID_DO_NUMBER							ID_BASE + 41
#define ID_BOOKBLOCK_PROOF						ID_BASE + 42
#define ID_BOOKBLOCK_PROOF_OPENFILE				ID_BASE + 43
#define ID_NEW_PRODUCT							ID_BASE + 44
#define ID_DEL_PRODUCT							ID_BASE + 45
#define ID_IMPORT_PRODUCTS						ID_BASE + 46
#define ID_AUTOSERVERVIEW_SELECT_TOOLBAR		ID_BASE + 47
#define ID_AUTOSERVERVIEW_NEW					ID_BASE + 48
#define ID_AUTOSERVERVIEW_REMOVE				ID_BASE + 49
#define ID_AUTOSERVERVIEW_STARTOUTPUT			ID_BASE + 50
#define ID_AUTOSERVERVIEW_ABORT					ID_BASE + 51
#define ID_AUTOSERVERVIEW_RESTARTJOB			ID_BASE + 52
#define ID_LOCK_UNLOCK_FOLDSHEET				ID_BASE + 53
