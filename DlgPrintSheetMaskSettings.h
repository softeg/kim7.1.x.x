#pragma once


// CDlgPrintSheetMaskSettings-Dialogfeld

class CDlgPrintSheetMaskSettings : public CDialog
{
	DECLARE_DYNAMIC(CDlgPrintSheetMaskSettings)

public:
	CDlgPrintSheetMaskSettings(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgPrintSheetMaskSettings();

// Dialogfelddaten
	enum { IDD = IDD_PRINTSHEETMASKSETTINGS };


public:
	void InitData();


protected:
	float m_fOverbleed;


protected:
	DECLARE_MESSAGE_MAP()
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnKillFocusMaskSettingsOverbleedEdit();
	afx_msg void OnDeltaposMaskSettingsOverbleedSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/);
};
