// DlgPDFImportSettings.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgPDFImportSettings.h"
#include "JDFOutputImposition.h"
#include "XMLCommon.h"



// CDlgPDFImportSettings-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgPDFImportSettings, CDialog)

CDlgPDFImportSettings::CDlgPDFImportSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPDFImportSettings::IDD, pParent)
	, m_strFind(_T(""))
	, m_strReplace(_T(""))
{

}

CDlgPDFImportSettings::~CDlgPDFImportSettings()
{
}

void CDlgPDFImportSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PDFIMPORTSETTINGS_FIND, m_strFind);
	DDX_Text(pDX, IDC_PDFIMPORTSETTINGS_REPLACE, m_strReplace);
}


BEGIN_MESSAGE_MAP(CDlgPDFImportSettings, CDialog)
	ON_BN_CLICKED(IDOK, &CDlgPDFImportSettings::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgPDFImportSettings-Meldungshandler

BOOL CDlgPDFImportSettings::OnInitDialog()
{
	CDialog::OnInitDialog();

	LoadSettings();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgPDFImportSettings::OnBnClickedOk()
{
	UpdateData(TRUE);
	SaveSettings();
	OnOK();
}

int CDlgPDFImportSettings::LoadSettings()
{
	CString	strFile;
	strFile.Format(_T("%s\\PDFImportSettings.xml"), theApp.GetDataFolder());

	JDFDoc*	   pJDFDoc = new JDFDoc(0);
	JDFParser* pParser = new JDFParser();

	try
	{
		pParser->Parse((WString)strFile);
	}catch (JDFException e)
	{
		//CString strMsg;
		//strMsg.Format(_T("xml parsing error: %s"), e.getMessage().getBytes());
		//AfxMessageBox(strMsg);
		delete pParser;
		delete pJDFDoc;
		return -1;
	}

	*pJDFDoc = pParser->GetRoot();

	if (pJDFDoc->isNull())
	{
		//AfxMessageBox(_T("xml parsing error: Root is NULL"));
		delete pParser;
		delete pJDFDoc;
		return -1;
	}

	XERCES_CPP_NAMESPACE::DOMDocument* domDoc = pJDFDoc->GetDOMDocument(); 

	DOMNode* settingsNode = domDoc->getFirstChild();
	CString strNodeName = settingsNode->getNodeName();
	if (strNodeName.CompareNoCase(_T("KIM-PDFImportSettings")) != 0)
	{
		delete pParser;
		delete pJDFDoc;
		return -1;
	}

	CString  strFolder;
	CString	 strJobNameTemplate;
	BOOL	 bResult = FALSE;
	DOMNode* node	 = settingsNode->getFirstChild();
	while (node)
	{
		strNodeName = node->getNodeName();
		if (strNodeName.CompareNoCase(_T("ProductNaming")) == 0)
		{
			DOMNode* productNamingNode = node->getFirstChild();
			while (productNamingNode)
			{
				strNodeName = productNamingNode->getNodeName();
				if (strNodeName.CompareNoCase(_T("FindAndReplace")) == 0)
				{
					DOMNode* findAndReplaceNode = productNamingNode->getFirstChild();
					while (findAndReplaceNode)
					{
						strNodeName = findAndReplaceNode->getNodeName();
						if (strNodeName.CompareNoCase(_T("Find")) == 0)
						{
							m_strFind = findAndReplaceNode->getTextContent();
						}
						else
						if (strNodeName.CompareNoCase(_T("Replace")) == 0)
						{
							m_strReplace = findAndReplaceNode->getTextContent();
						}
						findAndReplaceNode = findAndReplaceNode->getNextSibling();
					}
				}
				productNamingNode = productNamingNode->getNextSibling();
			}
		}

		node = node->getNextSibling();
	}

	delete pParser;
	delete pJDFDoc;
	return 0;
}

int CDlgPDFImportSettings::SaveSettings()
{
	XMLDoc* pXMLDoc = new XMLDoc(_T("KIM-PDFImportSettings"));
	KElement root = pXMLDoc->GetRoot();

	KElement productNamingElement	= root.AppendElement(_T("ProductNaming"));
	KElement findAndReplaceElement	= productNamingElement.AppendElement(_T("FindAndReplace"));
	KElement findElement			= findAndReplaceElement.AppendTextElement(_T("Find"), (WString)m_strFind);
	KElement replaceElement			= findAndReplaceElement.AppendTextElement(_T("Replace"), (WString)m_strReplace);

	try
	{
		CString	strFile;
		strFile.Format(_T("%s\\PDFImportSettings.xml"), theApp.GetDataFolder());
		pXMLDoc->Write2URL((WString)WindowsPath2URL(strFile));
	}
	catch (JDF::JDFException e)
	{
		CString strMsg;
		strMsg.Format(_T("JDF API error: %s"), e.getMessage().getBytes());
		AfxMessageBox(strMsg);
	}

	delete pXMLDoc;

	return 0;
}

CString CDlgPDFImportSettings::FindAndReplace(const CString& strSource)
{
	CString strTarget;

	strTarget = strSource;

	strTarget.Replace(m_strFind, m_strReplace);

	return strTarget;
}

