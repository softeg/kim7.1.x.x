// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afx.h>
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC OLE automation classes
#include <afxtempl.h>		// Used for CList/CArray operations
#include <afxcview.h>		// Used for CTreeView
#include <afxadv.h>			// Used for DisplayList

#include <Uxtheme.h>
#include "vssym32.h"

#ifndef _WITHOUT_VFW_
#include <vfw.h>			// DrawDib support
#endif

#include <ddeml.h>
#include <dde.h>

#include <afxdlgs.h>
#include <afxdhtml.h>





