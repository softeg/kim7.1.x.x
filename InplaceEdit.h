// InplaceEdit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInplaceEdit window


#define DECLARE_INPLACE_EDIT(CtrlName, CWndName) \
	CInplaceEdit m_##CtrlName; \
	void CWndName::OnKillFocus##CtrlName(); \
	void CWndName::OnChar##CtrlName(UINT nChar); \
	static void FnKillFocusHandler##CtrlName	(CWnd* pWnd)			 {((CWndName*)pWnd)->OnKillFocus##CtrlName();}; \
	static void FnCharHandler##CtrlName 		(CWnd* pWnd, UINT nChar) {((CWndName*)pWnd)->OnChar##CtrlName(nChar);}; \


#define CREATE_INPLACE_EDIT(CtrlName) \
	m_##CtrlName.Create(WS_CHILD|WS_BORDER|ES_AUTOHSCROLL, CRect(0,0,0,0), this, 0); \
	m_##CtrlName.m_pFnKillFocusHandler	  = &FnKillFocusHandler##CtrlName; \
	m_##CtrlName.m_pFnOnCharHandler		  = &FnCharHandler##CtrlName; \

#define CREATE_INPLACE_EDIT_EX(CtrlName) \
	m_##CtrlName.Create(WS_CHILD|WS_BORDER|ES_AUTOHSCROLL|ES_MULTILINE|ES_WANTRETURN, CRect(0,0,0,0), this, 0); \
	m_##CtrlName.m_pFnKillFocusHandler	  = &FnKillFocusHandler##CtrlName; \
	m_##CtrlName.m_pFnOnCharHandler		  = &FnCharHandler##CtrlName; \

 


class CInplaceEdit : public CEdit 
{
// Construction
public:
	CInplaceEdit();

enum Growing { FixedSize, GrowToRight, GrowToLeft };

// Attributes
public:
	CFont         m_Font;
	int	          m_nGrowTo;
	LPARAM        m_lParam;
	CBitmap*	  m_pBitmap;
	BOOL		  m_bKillFocusHandlerLocked;
	CDisplayItem  m_di;

// Operations
public:
	void	Activate(CRect rect, const CString& rString, int nGrowTo = FixedSize, LPARAM lParam = 0, CDisplayItem di = CDisplayItem());
	void	Move(CRect rect, const CString& rString, LPARAM lParam);
	void	Deactivate();
	void	SaveBackground(const CRect& rect);
	void	RestoreBackground(const CRect& rect);
	CString GetString();
	float	GetFloat() ;
	void	LockKillFocusHandler()   {m_bKillFocusHandlerLocked = TRUE;};
	void	UnlockKillFocusHandler() {m_bKillFocusHandlerLocked = FALSE;};


	void    (*m_pFnKillFocusHandler)	(CWnd* pWnd);
	void	(*m_pFnOnCharHandler)		(CWnd* pWnd, UINT nChar);


// Overrides
	BOOL Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID );

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInplaceEdit)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CInplaceEdit();

	// Generated message map functions
protected:
	//{{AFX_MSG(CInplaceEdit)
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdate();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////




