#pragma once

#include "DlgComponentImposer.h"
#include "DlgAlignObjects.h"


// CLayoutDataWindow

class CLayoutDataWindow : public CStatic
{
	DECLARE_DYNAMIC(CLayoutDataWindow)

public:
	CLayoutDataWindow();
	virtual ~CLayoutDataWindow();


// Attributes
public:
	DECLARE_INPLACE_EDIT(InplaceEdit, CLayoutDataWindow);
	DECLARE_DISPLAY_LIST(CLayoutDataWindow, OnMultiSelect, NO, OnStatusChanged, YES)
	DECLARE_DISPLAY_ITEM(CLayoutDataWindow, LayoutData,				OnSelect, YES, OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CLayoutDataWindow, LayoutNumSheets,		OnSelect, YES, OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CLayoutDataWindow, PrintColor,				OnSelect, YES, OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CLayoutDataWindow, PrintSheetQuantity,		OnSelect, YES, OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CLayoutDataWindow, ExpandColors,			OnSelect, YES, OnDeselect, NO, OnActivate, NO,  OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CLayoutDataWindow, OpenComponentImposer,	OnSelect, YES, OnDeselect, NO, OnActivate, NO,  OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CLayoutDataWindow, OpenAlignObjects,		OnSelect, YES, OnDeselect, NO, OnActivate, NO,  OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)


	enum Section{Impose, LayoutData, Press, Cutting, Binding, Trimming};

public:
	CRect						m_rcImposeBox, m_rcAlignObjectsBox, m_rcLayoutDataBox, m_rcPressBox, m_rcFoldingBox, m_rcBindingBox, m_rcTrimmingBox;
	CColorantList				m_colorantList;
	BOOL						m_bColorsExpanded;
	CDlgComponentImposer		m_dlgComponentImposer;
	CDlgAlignObjects			m_dlgAlignObjects;


// Operations
public:
	void			   Draw(CDC* pDC);
	CPrintingProfile&  GetPrintingProfile();
	CPrintSheet*	   GetPrintSheet();
	CLayout*		   GetLayout();
	CPrintComponent	   GetActComponent();
	CBoundProduct*	   GetActProduct();
	CBoundProductPart* GetActProductPart();
	CFoldSheet*		   GetActFoldSheet();
	CFlatProduct*	   GetActFlatProduct();
	void	           CalcSectionBoxes		 (CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CLayout* pLayout = NULL, CLayoutObject* pLayoutObject = NULL, int nFoldSheetRefIndex = -1, CPrintingProfile* pPrintingProfile = NULL, CBoundProduct* pProduct = NULL, CBoundProductPart* pProductPart = NULL, CFoldSheet* pFoldShee = NULL, CPrintGroup* pPrintGroup = NULL, CPrintComponent* pComponent = NULL);	
	void			   DrawSectionBackground	 (CDC* pDC, CRect rcFrame, CRect rcSectionBox, COLORREF crColor = RGB(245,245,245));	
	CRect			   DrawSectionImpose		 (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet);
	CRect			   DrawSectionAlignObjects(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CLayoutObject* pLayoutObject, int nFoldSheetRefIndex);
	CRect			   DrawSectionLayoutData	 (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CLayout* pLayout, CPrintComponent* pComponent);
	CRect			   DrawSectionPress		 (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CLayout* pLayout, CPrintingProfile* pPrintingProfile);
	CRect			   DrawSectionFolding	 (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CBoundProduct* pProduct, CBoundProductPart* pProductPart, CFoldSheet* pFoldSheet, CPrintGroup* pPrintGroup);
	CRect			   DrawSectionBinding	 (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CBoundProduct* pProduct,														   CPrintComponent* pComponent);
	CRect			   DrawSectionTrimming	 (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CBoundProductPart* pProductPart,												   CPrintComponent* pComponent);
	void			   RegisterSection(CDC* pDC, CRect rcDI, int nSection);
	static void		   OnCharEdit(CWnd* pWnd, UINT nChar);
	void			   UpdatePrintSheetQuantity();
	void			   ShowHideComponentImposer();
	void			   ChangeNumSheets(int nNewNumSheets);


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSchemeWindow)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL


// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnDestroy();
};


