#pragma once

#include "OwnerDrawnButtonList.h"
#include "InplaceComboBox.h"


// CProductsView-Ansicht

class CProductsView : public CScrollView
{
	DECLARE_DYNCREATE(CProductsView)

protected:
	CProductsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CProductsView();

	DECLARE_INPLACE_EDIT(InplaceEdit, CProductsView);
	DECLARE_INPLACE_COMBOBOX(InplaceComboBox, CProductsView);

public:
	DECLARE_DISPLAY_LIST(CProductsView, OnMultiSelect, YES, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CProductsView, Bookblock,							OnSelect, YES, OnDeselect, YES, OnActivate, NO,  OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CProductsView, ProductGroupBound,					OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CProductsView, ProductGroupUnbound,				OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CProductsView, BoundProductPart,					OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CProductsView, BoundProductPartDesiredQuantity,	OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CProductsView, BoundProductPartPaperInfo,			OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CProductsView, FlatProduct,						OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CProductsView, FlatProductDesiredQuantity,			OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CProductsView, FlatProductPaperInfo,				OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CProductsView, ProductGroupExpander,				OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)


enum displayColIDs {ColThumbnail, ColProductPart, ColProductInfo, ColProductSize, ColProductQuantity, ColProductOverProduction, ColProductColors, ColProductPunch, ColProductZone, ColProductPaper, ColProductGrain, ColProductTrims};
typedef struct displayColumn
{
	int nID;
	int nWidth;
};
typedef CArray <struct displayColumn, struct displayColumn&> DISPLAY_COLUMN_LIST;

public:
	DISPLAY_COLUMN_LIST		m_displayColumns;


public:
	CSize					m_docSize;
	CHeaderCtrl  			m_HeaderCtrl;
	int			 			m_nHeaderHeight;
	int						m_nHeaderItemsTotalWidth;
	CFont		 			m_headerFont;
	CArray <int, int>		m_headerColumnWidths;
	COwnerDrawnButtonList	m_toolBar;
	CPageSource*			m_pPageSource;
	int						m_nProductPartIndex;
	CFlatProductList		m_flatProducts;


public:
	CSize				CalcExtents();
	CRect				Draw(CDC* pDC, CDisplayList* pDisplayList);
	void				ResetDisplayColums();
	void				AddDisplayColumn(int nColID, int nWidth);
	CBoundProduct&		GetActiveBoundProduct();
	CBoundProductPart&	GetActiveBoundProductPart();
	CFlatProduct&		GetActiveFlatProduct();
	BOOL				GetBookblockSelected();
	static void			DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd);
	void				PDFEngineDataExchange(CPageSource* pNewObjectSource);
	void				UpdateBoundProductPartDesiredQuantity();
	void				UpdateFlatProductDesiredQuantity();
	static void	 		InitializePaperInfoCombo(CComboBox* pBox);
	void		 		UpdateBoundProductPartPaperInfo();
	void		 		UpdateFlatProductPaperInfo();
	void				SetProductTableHeaderWidth(int nIndex, int nValue);
	int					GetProductTableHeaderWidth(int nIndex, int nDefault);



public:
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	DECLARE_MESSAGE_MAP()
public: 
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void ScrollToPosition(POINT pt);
	virtual void ScrollToDevicePosition(POINT ptDev);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBoundProductDuplicate();
	afx_msg void OnHeaderTrack	 (NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDeleteProduct();
};


