// AutoServerView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "AutoServerJobsView.h"
#include "AutoServerDetailsView.h"
#include "AutoServerLogsView.h"
#include "AutoServerToolsView.h"
#include "DlgAutoServerWorkflow.h"
#include "GraphicComponent.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PageListFrame.h"


#define IDC_AUTOSERVERVIEW_HEADER 12121


// CAutoServerJobsView

IMPLEMENT_DYNCREATE(CAutoServerJobsView, CScrollView)

CAutoServerJobsView::CAutoServerJobsView()
{
    m_DisplayList.Create(this, FALSE, DISPLAY_LIST_TYPE(CAutoServerJobsView));
	m_DisplayList.SetClassName(_T("CAutoServerJobsView"));

	m_nHeaderItemsTotalWidth = 0;
	m_nHeaderHeight = 0;
	m_headerFont.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CAutoServerJobsView::~CAutoServerJobsView()
{
	m_headerFont.DeleteObject();
}

int	CAutoServerJobsView::GetActJobID()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerJobsView, Job));
	if (pDI)
		return (int)pDI->m_pData;

	kimAutoDB::_jobs jobs[1000];
	theApp.m_kaDB.ka_GetJobsAll(jobs, 1000);
	if (jobs[0].id >= 0)
		return jobs[0].id;

	return -1;
}

int	CAutoServerJobsView::GetActJobStatus()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerJobsView, Job));
	if (pDI)
		return (int)pDI->m_AuxAuxRect.left;
	else
		return -2;
}

void CAutoServerJobsView::DoDataExchange(CDataExchange* pDX)
{
	CScrollView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAutoServerJobsView, CScrollView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY(HDN_TRACK,	IDC_AUTOSERVERVIEW_HEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK, IDC_AUTOSERVERVIEW_HEADER, OnHeaderEndTrack)
END_MESSAGE_MAP()


// CAutoServerJobsView-Diagnose

#ifdef _DEBUG
void CAutoServerJobsView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CAutoServerJobsView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG


// CAutoServerJobsView-Meldungshandler

BOOL CAutoServerJobsView::OnEraseBkgnd(CDC* pDC)
{
	//m_toolBar.ClipButtons(pDC);

	//CRect rect;
 //   GetClientRect(rect);
	//rect.top = 0;
 //   //CBrush brush(RGB(230,233,238));
 //   //CBrush brush(RGB(240,241,244));//237,239,243));
 //   CBrush brush(WHITE);
 //   pDC->FillRect(&rect, &brush);

 //   brush.DeleteObject();

	return TRUE;

//	return CScrollView::OnEraseBkgnd(pDC);
}

int CAutoServerJobsView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	//CREATE_INPLACE_EDIT_EX(InplaceEdit);
	//CREATE_INPLACE_COMBOBOX(InplaceComboBox, 0);

	return 0;
}

void CAutoServerJobsView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
}

void CAutoServerJobsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_headerColumnWidths.RemoveAll();
	m_HeaderCtrl.DestroyWindow();
	m_nHeaderHeight = 0;

	if ( ! m_HeaderCtrl.GetSafeHwnd())
	{
		m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, IDC_AUTOSERVERVIEW_HEADER);	
		m_HeaderCtrl.SetFont(&m_headerFont, FALSE);

		HD_ITEM hdi;
		hdi.mask		= HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
		hdi.fmt			= HDF_LEFT | HDF_STRING;

		m_headerColumnWidths.RemoveAll();

		int		nIndex = 0;

		CString string;

		string = "Job";//.LoadString(IDS_PRODUCT);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 200);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string = "Workflow";//.LoadString(IDS_PRODUCT);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 150);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_LASTMODIFIED_LABEL);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 120);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string = "Status";
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 120);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		m_nHeaderItemsTotalWidth = 0;
		for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
			m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];
	}

	OnSize(0, 0, 0);

	ModifyStyle(0, WS_CLIPCHILDREN);	// Clip header control 

	Invalidate();

	m_DisplayList.Invalidate();

	m_docSize = CalcExtents();
	SetScrollSizes(MM_TEXT, m_docSize);
}

void CAutoServerJobsView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);

	if (m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdl;
		WINDOWPOS wp;
		hdl.prc = &clientRect;
		hdl.pwpos = &wp;
		m_HeaderCtrl.Layout(&hdl);	
		m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_nHeaderHeight = wp.cy;
	} 
}
// CAutoServerJobsView-Zeichnung

CSize CAutoServerJobsView::CalcExtents()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CRect rcBox = Draw(&dc, NULL);

	dc.DeleteDC();

	return CSize(rcBox.right + 10, rcBox.bottom + 10);
}

void CAutoServerJobsView::OnDraw(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(rcClient); 
	CRect rcFrame(CPoint(0, 0), CSize(rcClient.Width(), max(rcClient.Height(), m_docSize.cy + m_nHeaderHeight)));

	CDC	memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap bitmap;	bitmap.CreateCompatibleBitmap(pDC, rcFrame.Width(), rcFrame.Height());
	memDC.SelectObject(&bitmap);

	memDC.FillSolidRect(rcFrame, WHITE);//RGB(227,232,238));

	memDC.SaveDC();
	Draw(&memDC, &m_DisplayList);
	memDC.RestoreDC(-1);
	
	pDC->SaveDC();	// save current clipping region
	ClipHeader(pDC);

	pDC->BitBlt(0, m_nHeaderHeight, rcFrame.Width(), rcFrame.Height(), &memDC, 0, m_nHeaderHeight, SRCCOPY);

	bitmap.DeleteObject();
	memDC.DeleteDC();

	pDC->RestoreDC(-1);
}

void CAutoServerJobsView::ClipHeader(CDC* pDC)
{
	CRect rcClientRect, rcClipRect;
	GetClientRect(rcClientRect); rcClipRect = rcClientRect;
	rcClipRect.top = m_nHeaderHeight;
	rcClipRect.OffsetRect(-pDC->GetViewportOrg());
	pDC->IntersectClipRect(rcClipRect);
}

CRect CAutoServerJobsView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	CRect rcBox(0,0,0,0);
	return DrawJobsTable(pDC, pDisplayList);;
}

CRect CAutoServerJobsView::DrawJobsTable(CDC* pDC, CDisplayList* pDisplayList)
{
	BOOL bSelectFirstItem = FALSE;
	if (pDisplayList)
	{
		if ( ! pDisplayList->GetFirstSelectedItem((DISPLAY_ITEM_TYPEID(CAutoServerJobsView, Job))))
			bSelectFirstItem = TRUE;
		pDisplayList->BeginRegisterItems(pDC);
	}

	CRect rcFrame;
	GetClientRect(rcFrame); 
	
	rcFrame.top += m_nHeaderHeight;	
	//rcFrame.right = rcFrame.left + 500;
	rcFrame.top += 5;
	rcFrame.left += 5;

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nColIndex = 0;
	ResetDisplayColums();
	AddDisplayColumn(ColJob,				m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColJobWorkflow,		m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColJobDate,			m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColJobStatus,			m_headerColumnWidths[nColIndex++]);

	CGraphicComponent gc;

	kimAutoDB::_jobs jobs[1000];
	theApp.m_kaDB.ka_GetJobsAll(jobs, 1000);

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);

	CRect rcClient	  = (pDisplayList) ? pDisplayList->m_clientRect : CRect(0,0,0,0);
	int	  nTextHeight = 15;

	int nRowHeight = 25;
	CRect rcRow = rcFrame; rcRow.top += 5; rcRow.bottom = rcRow.top + nRowHeight;
	for (int nJobIndex = 0; ( ! jobs[nJobIndex].Name.empty()) && (nJobIndex < 1000); nJobIndex++)
	{
		kimAutoDB::_jobs& rJob = jobs[nJobIndex];

		/////// columns
		CRect rcColumn = rcRow; 
		rcColumn.left += 5;

		int	  nRight = 0;
		CRect rcRet, rcTools;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColJob:				rcRet = DrawJobInfo			 (pDC, rcColumn, rJob, pDisplayList); rcTools = rcColumn; rcTools.left = rcTools.right - 30; break;
			case ColJobWorkflow:		rcRet = DrawJobWorkflowInfo	 (pDC, rcColumn, rJob, pDisplayList); break;
			case ColJobDate:			rcRet = DrawJobDateInfo		 (pDC, rcColumn, rJob, pDisplayList); break;
			case ColJobStatus:			rcRet = DrawJobStatusInfo	 (pDC, rcColumn, rJob, pDisplayList); break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + 5;
		}

		if (pDisplayList)
		{
			CRect rcDI = rcRow; rcDI.bottom = rcDI.top + nTextHeight; rcDI.left = 0; rcDI.right = rcClient.right; rcDI.top -= 4; rcDI.bottom += 4;
			CRect rcDIAux = rcDI; rcDIAux.left = rcTools.left, rcDIAux.right = rcTools.right; 
			rcDI.OffsetRect(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)); rcDIAux.OffsetRect(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT));
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDIAux, (void*)rJob.id, DISPLAY_ITEM_REGISTRY(CAutoServerJobsView, Job), (void*)NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->m_AuxAuxRect = CRect(rJob.status, rJob.status, rJob.status, rJob.status);
	
				if (bSelectFirstItem && (nJobIndex == 0) )
					pDI->m_nState = CDisplayItem::Selected;

				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
				{
					CRect rcFeedback = pDI->m_BoundingRect; 
					rcFeedback.OffsetRect(GetScrollPos(SB_HORZ), GetScrollPos(SB_VERT));
					COLORREF crHighlightColor = RGB(255,195,15);
					CGraphicComponent gc;
					gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, 0);	
				}
			}
		}

		rcRow.top = rcBox.bottom + 10;
		rcRow.bottom = rcRow.top + nRowHeight;
	}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CAutoServerJobsView::DrawJobInfo(CDC* pDC, CRect rcFrame, kimAutoDB::_jobs& rJob, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CImage img;
	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_KIM_JOB));
	CRect rcImg(CPoint(rcFrame.left, rcFrame.top), CSize(img.GetWidth(), img.GetHeight()));
	img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_CAPTION);

	CRect rcTitle = rcFrame; rcTitle.left += 20; rcTitle.bottom = rcTitle.top + nTextHeight;
	DrawTextMeasured(pDC, rJob.Name.c_str(), rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CAutoServerJobsView::DrawJobWorkflowInfo(CDC* pDC, CRect rcFrame, kimAutoDB::_jobs& rJob, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	kimAutoDB::_workflows workflow;
	theApp.m_kaDB.ka_GetWorkflow(&workflow, rJob.WorkflowID);

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	CString strOut = workflow.name.c_str();
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CAutoServerJobsView::DrawJobDateInfo(CDC* pDC, CRect rcFrame, kimAutoDB::_jobs& rJob, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	COleDateTime dateTime(rJob.ModifiedDate);
	CString strOut = CPrintSheetView::ConvertToString(dateTime, TRUE, TRUE);

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CAutoServerJobsView::DrawJobStatusInfo(CDC* pDC, CRect rcFrame, kimAutoDB::_jobs& rJob, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CString strOut; 
	int		nBmpID = 0;
	switch (rJob.status)
	{
	case kimAutoDB::jobError:		nBmpID = IDB_ERROR;			strOut.LoadString(IDS_ERROR);	break;
	case kimAutoDB::jobIdle:																	break;
	case kimAutoDB::jobBusy:																	break;
	case kimAutoDB::jobAborted:		nBmpID = IDB_ABORT;			strOut.LoadString(IDS_ABORTED); break;
	case kimAutoDB::jobToDelete:	nBmpID = IDB_REMOVE;		strOut = "wird gel�scht";		break;//strOut.LoadString(IDS_ABORTED); break;
	case kimAutoDB::jobToRestart:	nBmpID = IDB_RESTART;		strOut = "neu starten";			break;//strOut.LoadString(IDS_ABORTED); break;
	case kimAutoDB::jobToAbort:		nBmpID = IDB_ABORT;			strOut = "wird abgebrochen";	break;//strOut.LoadString(IDS_ABORTED); break;
	case kimAutoDB::jobToOutput:	nBmpID = IDB_START_SMALL;	strOut = "wird ausgegeben";		break;//strOut.LoadString(IDS_ABORTED); break;
	default:						return rcBox;
	}

	if (nBmpID != 0)
	{
		CImage img;
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(nBmpID));
		CRect rcImg(CPoint(rcFrame.left, rcFrame.top), CSize(img.GetWidth(), img.GetHeight()));
		img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
		rcBox = rcImg;
	}

	if ( ! strOut.IsEmpty())
	{
		int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

		CFont font;
		font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		pDC->SelectObject(&font);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(GUICOLOR_CAPTION);

		CRect rcText = rcFrame; rcText.left += 20; rcText.bottom = rcText.top + nTextHeight;
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

		font.DeleteObject();
	}

	if (pDisplayList)
	{
		CRect rcDI = rcFrame; rcDI.bottom = rcBox.bottom;
		rcDI.OffsetRect(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)); 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)rJob.id, DISPLAY_ITEM_REGISTRY(CAutoServerJobsView, JobStatusField), (void*)NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			if (rJob.status == kimAutoDB::jobBusy)// && (rJob.id != GetActJobID()) )
			{
				pDI->OpenAnimationCtrl(pDC, pDI->m_BoundingRect, _T("GIF"), _T("IDR_ACTIVITY_ANIMATION"));
				rcBox = pDI->m_BoundingRect;
			}
			else
				pDI->CloseAnimationCtrl();

			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
		}
	}

	return rcBox;
}

void CAutoServerJobsView::ResetDisplayColums()
{
	m_displayColumns.RemoveAll();
}

void CAutoServerJobsView::AddDisplayColumn(int nColID, int nWidth)
{
	struct displayColumn dc = {nColID, nWidth};
	m_displayColumns.Add(dc);
}
// CAutoServerJobsView-Meldungshandler

void CAutoServerJobsView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_DisplayList.OnLButtonDown(point);

	CScrollView::OnLButtonDown(nFlags, point);
}

void CAutoServerJobsView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
    OnLButtonDown(nFlags, point);	// Handle second click 

	CScrollView::OnLButtonDblClk(nFlags, point);
}

void CAutoServerJobsView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list
	CPoint pt = point;
	ClientToScreen(&pt);
	OnContextMenu(NULL, pt);
}

void CAutoServerJobsView::OnContextMenu(CWnd*, CPoint point)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int			  nMenuID = -1;
	CDisplayItem* pDI	  = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerJobsView, BoundProduct));
	if (pDI)
	{
		nMenuID = IDR_PRODUCTSVIEW_POPUP;
	}
	//else
	//{
	//	pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerJobsView, BoundProduct));
	//	if (pDI)
	//		nMenuID = ;
	//}

	if (nMenuID < 0)
		return;

    CMenu menu;
	VERIFY(menu.LoadMenu(nMenuID));

	CMenu* pPopup = menu.GetSubMenu(0);
	if ( ! pPopup)
		return;

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
}

void CAutoServerJobsView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

void CAutoServerJobsView::OnMouseLeave()
{
	m_DisplayList.OnMouseLeave();

	CScrollView::OnMouseLeave();
}


// This functions are overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling

void CAutoServerJobsView::ScrollToPosition(POINT pt)    // logical coordinates
{
	ASSERT(m_nMapMode > 0);     // not allowed for shrink to fit
	if (m_nMapMode != MM_TEXT)
	{
		CWindowDC dc(NULL);
		dc.SetMapMode(m_nMapMode);
		dc.LPtoDP((LPPOINT)&pt);
	}

	// now in device coordinates - limit if out of range
	int xMax = GetScrollLimit(SB_HORZ);
	int yMax = GetScrollLimit(SB_VERT);
	if (pt.x < 0)
		pt.x = 0;
	else if (pt.x > xMax)
		pt.x = xMax;
	if (pt.y < 0)
		pt.y = 0;
	else if (pt.y > yMax)
		pt.y = yMax;

	ScrollToDevicePosition(pt);
}

void CAutoServerJobsView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);
	
	CRect scrollRect, clipRect;
	GetClientRect(scrollRect); clipRect = scrollRect;
	clipRect.top += m_nHeaderHeight;	// Protect header control from being vertically scrolled
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, clipRect);

	if (m_HeaderCtrl.GetSafeHwnd())
		if (ptDev.x != 0)				// Scroll header control horizontally
			m_HeaderCtrl.MoveWindow(-ptDev.x, 0, scrollRect.Width() + ptDev.x, m_nHeaderHeight);

	//if (m_InplaceEdit.GetSafeHwnd())
	//	m_InplaceEdit.Deactivate();
}

BOOL CAutoServerJobsView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect, clipRect;
		GetClientRect(scrollRect); clipRect = scrollRect;
		//if (m_HeaderCtrl.GetSafeHwnd())
			clipRect.top += m_nHeaderHeight;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, clipRect);

		if (m_HeaderCtrl.GetSafeHwnd())
			if (sizeScroll.cx != 0)				// Scroll header control horizontally
				m_HeaderCtrl.MoveWindow(-x, 0, scrollRect.Width() + x, m_nHeaderHeight);

		//if (m_InplaceEdit.GetSafeHwnd())
		//	m_InplaceEdit.Deactivate();
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

void CAutoServerJobsView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 20)
		hdn->pitem->cxy = 20;
}

void CAutoServerJobsView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	SetHeaderColumnWidth(hdn->iItem, hdn->pitem->cxy);

	if (m_headerColumnWidths.GetSize() > hdn->iItem)
		m_headerColumnWidths[hdn->iItem] = hdn->pitem->cxy; 

	m_nHeaderItemsTotalWidth = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
		m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];

	CRect clientRect;
	GetClientRect(&clientRect);

	InvalidateRect(clientRect);
	UpdateWindow(); 
}

void CAutoServerJobsView::SetHeaderColumnWidth(int nIndex, int nValue)
{
	if (nIndex >= 20)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\AutoServerJobsViewHeader");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			columnWidthArray[nIndex] = nValue;			
		else
		{
			ZeroMemory(columnWidthArray, sizeof(columnWidthArray));
			columnWidthArray[nIndex] = nValue;			
		}

		dwSize = sizeof(columnWidthArray);
		RegSetValueEx(hKey, _T("Columns"), 0, REG_BINARY, (LPBYTE)&columnWidthArray, dwSize);

		RegCloseKey(hKey);
	}
}

int CAutoServerJobsView::GetHeaderColumnWidth(int nIndex, int nDefault)
{
	if (nIndex >= 20)
		return nDefault;

	int nWidth = nDefault;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\AutoServerJobsViewHeader");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			nWidth = columnWidthArray[nIndex];			
		else
			nWidth = nDefault;

		RegCloseKey(hKey);
	}

	if ( (nWidth <= 10) || (nWidth > 1000) )
		nWidth = nDefault;

	return nWidth;
}

BOOL CAutoServerJobsView::OnSelectJob(CDisplayItem* pDI, CPoint point, LPARAM)	
{
	CRect rcGoto = pDI->m_AuxRect;
	rcGoto.left += 5; rcGoto.right = rcGoto.left + 20; rcGoto.bottom = rcGoto.top + 25;

	if (rcGoto.PtInRect(point))
	{
		kimAutoDB::_jobs job;
		theApp.m_kaDB.ka_GetJob(&job, (int)pDI->m_pData);
		kimAutoDB::_workflows workflow;
		theApp.m_kaDB.ka_GetWorkflow(&workflow, job.WorkflowID);

		CString strPath; strPath.Format(_T("%s\\%s\\%s\\%s.job"), theApp.GetAutoServerJobFolderRoot(), workflow.name.c_str(), job.Name.c_str(), job.Name.c_str());

		theApp.OpenDocumentFile(strPath);
	}

	return TRUE;
}

BOOL CAutoServerJobsView::OnDeselectJob(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	return TRUE;
}

BOOL CAutoServerJobsView::OnActivateJob(CDisplayItem* pDI, CPoint point, LPARAM)	
{
	pDI->m_nState = CDisplayItem::Selected;

	OnSelectJob(pDI, point, 0);

	return TRUE;
}

BOOL CAutoServerJobsView::OnMouseMoveJob(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				COLORREF crHighlightColor = RGB(165,175,200);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, pDI->m_BoundingRect, crHighlightColor, 0);	

				CRect rcTools = pDI->m_AuxRect; rcTools.DeflateRect(0, 1);

				pDC->FillSolidRect(rcTools, WHITE);//RGB(246,240,235));

				CImage img;
				CRect rcImg = rcTools; 
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_ARROW_RIGHT_HOT));
				img.TransparentBlt(pDC->m_hDC, rcImg.left + 8, rcImg.top + 4, img.GetWidth(), img.GetHeight(), WHITE);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}
	return TRUE;
}

BOOL CAutoServerJobsView::OnStatusChanged()	
{
	if ( ! m_DisplayList.GetFirstSelectedItem((DISPLAY_ITEM_TYPEID(CAutoServerJobsView, Job))))
	{
		CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CAutoServerJobsView, Job));
		if (pDI)
		{
			pDI->m_nState = CDisplayItem::Selected;
			m_DisplayList.InvalidateItem(pDI, FALSE, FALSE);
		}
	}

	CAutoServerDetailsView* pDetailsView = theApp.GetAutoServerDetailsView();
	if (pDetailsView)
	{
		pDetailsView->m_DisplayList.Invalidate();
		pDetailsView->OnUpdate(NULL, 0, NULL);
		pDetailsView->Invalidate();
		pDetailsView->UpdateWindow();
	}

	return TRUE;
}
