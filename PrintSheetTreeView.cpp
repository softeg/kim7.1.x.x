// PrintSheetTreeView.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PrintSheetTreeView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "PrintSheetView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTreeView

IMPLEMENT_DYNCREATE(CPrintSheetTreeView, CTreeView)

CPrintSheetTreeView::CPrintSheetTreeView()
{
	m_pimagelist = new CImageList();
	m_ActPrintSheetNumber = 0; // NO PrintSheet selected
	m_nActSight		  = ALLSIDES;

	m_pBkBrush = new CBrush(TABLECOLOR_BACKGROUND);
}

CPrintSheetTreeView::~CPrintSheetTreeView()
{
}

BEGIN_MESSAGE_MAP(CPrintSheetTreeView, CTreeView)
	//{{AFX_MSG_MAP(CPrintSheetTreeView)
	ON_WM_CONTEXTMENU()
	ON_WM_DESTROY()
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelchanged)
	ON_WM_CTLCOLOR_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTreeView drawing

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTreeView diagnostics

#ifdef _DEBUG
void CPrintSheetTreeView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CPrintSheetTreeView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}

CImpManDoc* CPrintSheetTreeView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CImpManDoc)));
	return (CImpManDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTreeView message handlers


BOOL CPrintSheetTreeView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CTreeView::PreCreateWindow(cs))
		return FALSE;

	cs.style = WS_CHILD | WS_VISIBLE | WS_BORDER | 
			   TVS_LINESATROOT | TVS_HASLINES | TVS_HASBUTTONS | 
			   TVS_SHOWSELALWAYS;

	return TRUE;
}


void CPrintSheetTreeView::OnInitialUpdate() 
{
	CBitmap	bitmap;
	CString	strPrintSheet;
	CString	strFoldSheet;

	//CTreeView::OnInitialUpdate();

	m_pimagelist->Create(16, 16, TRUE, 10, 1);

	bitmap.LoadBitmap(IDB_TREE_JOB);
	m_pimagelist->Add(&bitmap, (COLORREF)0xFFFFFF);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TREE_LAYOUT);
	m_pimagelist->Add(&bitmap, (COLORREF)0xFFFFFF);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TREE_FRONTSIDE);
	m_pimagelist->Add(&bitmap, (COLORREF)0xFFFFFF);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TREE_BACKSIDE);
	m_pimagelist->Add(&bitmap, (COLORREF)0xFFFFFF);
	bitmap.DeleteObject();

	AddPlateColorsToTree();

	ShowTreeCtrlAllSides();
}

void CPrintSheetTreeView::AddPlateColorsToTree() 
{
	// Add documents color definition table to image list
	CClientDC dc(this);
	CDC		  memDC;
	CBitmap	  bitmap;

	GetTreeCtrl().SetImageList(NULL, TVSIL_NORMAL);

	CImpManDoc* pDoc = GetDocument();
	int nSize = pDoc->m_ColorDefinitionTable.GetSize();

	bitmap.CreateCompatibleBitmap(&dc, 16, 16);
	memDC.CreateCompatibleDC(&dc);
	for (int i = 0; i < nSize; i++)
	{
		CBitmap *pOldBmp = memDC.SelectObject(&bitmap);

		// erase background
		memDC.FillSolidRect(0,0, 16,16, dc.GetBkColor());

		// draw plate
		memDC.SelectStockObject(BLACK_PEN);
		memDC.Rectangle(1,3,15,14);
		memDC.FillSolidRect( 2, 4, 12,9, LIGHTGRAY);
		memDC.FillSolidRect( 3,12,  2,1, BLACK);
		memDC.FillSolidRect(11,12,  2,1, BLACK);

		// draw plate color
		memDC.Rectangle(2,1, 9,5);
		memDC.FillSolidRect(3,2, 5,4, pDoc->m_ColorDefinitionTable[i].m_rgb);
		memDC.SelectObject(pOldBmp);

		m_pimagelist->Add(&bitmap, (COLORREF)0xFFFFFF);
	}
	memDC.DeleteDC();
	bitmap.DeleteObject();

	GetTreeCtrl().SetImageList(m_pimagelist, TVSIL_NORMAL);
}

void CPrintSheetTreeView::ShowTreeCtrlAllSides() 
{
	TV_INSERTSTRUCT TreeCtrlItem;
	HTREEITEM hTreeItem1;
	HTREEITEM hTreeItem2;
	HTREEITEM hTreeItem3;
	HTREEITEM hTreeItem4;
	CString	  string, string1; 


	int nPrevPrintSheet = m_ActPrintSheetNumber;	// m_ActPrintSheetNumber will be reset
	int nPrevSight		= m_nActSight;				// in "GetTreeCtrl().DeleteAllItems()" WHY ?????

	GetTreeCtrl().DeleteAllItems();	// Only delete tree-items -> NOT update the view
	m_TreeItemList.RemoveAll(); 

	CImpManDoc* pDoc = GetDocument();
	if (pDoc->m_PrintSheetList.IsEmpty()) // || pDoc->m_Bookblock.IsEmpty())
		return;

	TreeCtrlItem.hParent			 = TVI_ROOT;
	TreeCtrlItem.hInsertAfter		 = TVI_LAST;
	TreeCtrlItem.item.iImage		 = 0;
	TreeCtrlItem.item.iSelectedImage = 0;
	TreeCtrlItem.item.mask			 = TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	TreeCtrlItem.item.cchTextMax	 = MAX_PATH;
	string1 = pDoc->GetTitle();
	int nInd = string1.Find('.');
	string = (nInd >= 0) ? string1.Left(nInd) : string1;
	TreeCtrlItem.item.pszText		 = string.GetBuffer(MAX_PATH);
	TreeCtrlItem.item.lParam		 = -1;  // -1 means root/Document
	hTreeItem1 = GetTreeCtrl().InsertItem(&TreeCtrlItem);
	TreeCtrlItem.item.state			 = 0;

	CTreeItem ItemBuffer;
	ItemBuffer.hTreeItem = hTreeItem1;
	ItemBuffer.nType	 = CTreeItem::Root;	//Root || PrintSheet || FrontSide || BackSide || FrontPlate || BackPlate
	ItemBuffer.nPrintSheetNumber = 0;
	ItemBuffer.nPlateIndex = -1;
	m_TreeItemList.AddTail(ItemBuffer);

	m_hTreeItemList.RemoveAll();
	m_hTreeItemList.AddTail(hTreeItem1);

	CPrintSheet			PrintSheetBuf;
	CLayout*			pActLayout;
	int					i, j, nColors, numberPrintSheets;
	CPlate				PlateBuf;
	CFoldSheet			FoldSheetBuf;
	CFoldSheetLayerRef	LayerRefBuf;
	POSITION			pos, pos2;

	numberPrintSheets = pDoc->m_PrintSheetList.GetCount(); 
	pos = pDoc->m_PrintSheetList.GetHeadPosition();
	for (i = 0; i < numberPrintSheets; i ++)                        
	{
		PrintSheetBuf = pDoc->m_PrintSheetList.GetAt(pos);
		pActLayout = PrintSheetBuf.GetLayout();
		if (pActLayout == NULL)
			continue;
		string.Format(_T("%s  [%s]"), PrintSheetBuf.GetNumber(), pActLayout->m_strLayoutName);
		TreeCtrlItem.hParent			 = hTreeItem1;
		TreeCtrlItem.item.iImage		 = 1;
		TreeCtrlItem.item.iSelectedImage = 1;
		TreeCtrlItem.item.cchTextMax	 = MAX_TEXT;
		TreeCtrlItem.item.pszText		 = string.GetBuffer(MAX_TEXT);
		TreeCtrlItem.item.lParam		 = i; // >= 0 means printsheetlist
		hTreeItem2 = GetTreeCtrl().InsertItem(&TreeCtrlItem);
		m_hTreeItemList.AddTail(hTreeItem2);

		ItemBuffer.hTreeItem = hTreeItem2;
		ItemBuffer.nType	 = CTreeItem::PrintSheet;//Root || PrintSheet || FrontSide || BackSide || FrontPlate || BackPlate
		ItemBuffer.nPrintSheetNumber = PrintSheetBuf.m_nPrintSheetNumber;
		ItemBuffer.nPlateIndex = -1;
		m_TreeItemList.AddTail(ItemBuffer);

			// FrontSide
			TreeCtrlItem.hParent			 = hTreeItem2;
			TreeCtrlItem.item.iImage		 = 2;
			TreeCtrlItem.item.iSelectedImage = 2;
			TreeCtrlItem.item.cchTextMax	 = MAX_TEXT;
			if (pActLayout->m_nProductType)
			{
				switch(pActLayout->m_nProductType)
				{
				case WORK_AND_TURN  : TreeCtrlItem.item.pszText = theApp.settings.m_szWorkAndTurnName;
					break;
				case WORK_AND_TUMBLE: TreeCtrlItem.item.pszText = theApp.settings.m_szWorkAndTumbleName;
					break;
				default:			  TreeCtrlItem.item.pszText = _T("");
					break;
				}
			}
			else
				TreeCtrlItem.item.pszText = pActLayout->m_FrontSide.m_strSideName.GetBuffer(MAX_TEXT);
			TreeCtrlItem.item.lParam		 = 1000 + i; //  1000 means frontsidename
			hTreeItem3 = GetTreeCtrl().InsertItem(&TreeCtrlItem);

			ItemBuffer.hTreeItem = hTreeItem3;
			ItemBuffer.nType	 = CTreeItem::FrontSide;//Root || PrintSheet || FrontSide || BackSide || FrontPlate || BackPlate
			ItemBuffer.nPrintSheetNumber = PrintSheetBuf.m_nPrintSheetNumber;
			ItemBuffer.nPlateIndex = -1;
			m_TreeItemList.AddTail(ItemBuffer);

				// Colors
				nColors = PrintSheetBuf.m_FrontSidePlates.GetCount();
				if (nColors > 0)
				{
					pos2 = PrintSheetBuf.m_FrontSidePlates.GetHeadPosition();
					for (j = 0; j < nColors; j++)
					{
						PlateBuf = PrintSheetBuf.m_FrontSidePlates.GetAt(pos2);
						TreeCtrlItem.hParent			 = hTreeItem3;
						TreeCtrlItem.item.iImage		 = 4 + PlateBuf.m_nColorDefinitionIndex;
						TreeCtrlItem.item.iSelectedImage = 4 + PlateBuf.m_nColorDefinitionIndex;
						TreeCtrlItem.item.cchTextMax = MAX_TEXT;
						TreeCtrlItem.item.pszText	 = PlateBuf.m_strPlateName.GetBuffer(MAX_TEXT);
						TreeCtrlItem.item.lParam	 = 10000 + ((j+1)*1000) + i; // > 10000 means colornames for frontside
						hTreeItem4 = GetTreeCtrl().InsertItem(&TreeCtrlItem);
						PlateBuf = PrintSheetBuf.m_FrontSidePlates.GetNext(pos2);

						ItemBuffer.hTreeItem = hTreeItem4;
						ItemBuffer.nType	 = CTreeItem::FrontPlate;//Root || PrintSheet || FrontSide || BackSide || FrontPlate || BackPlate
						ItemBuffer.nPrintSheetNumber = PrintSheetBuf.m_nPrintSheetNumber;
						ItemBuffer.nPlateIndex = j;
						m_TreeItemList.AddTail(ItemBuffer);
					}
				}
			// BackSide
			if ( ! pActLayout->m_nProductType)
			//if ( ! pActLayout->m_FrontSide.IsIdentical(pActLayout->m_BackSide))
			{
				TreeCtrlItem.hParent			 = hTreeItem2;
				TreeCtrlItem.item.iImage		 = 3;
				TreeCtrlItem.item.iSelectedImage = 3;
				TreeCtrlItem.item.cchTextMax	 = MAX_TEXT;
				TreeCtrlItem.item.pszText		 = pActLayout->m_BackSide.m_strSideName.GetBuffer(MAX_TEXT);
				TreeCtrlItem.item.lParam		 = 2000 + i; //  2000 means backsidename
				hTreeItem3 = GetTreeCtrl().InsertItem(&TreeCtrlItem);

				ItemBuffer.hTreeItem = hTreeItem3;
				ItemBuffer.nType	 = CTreeItem::BackSide;//Root || PrintSheet || FrontSide || BackSide || FrontPlate || BackPlate
				ItemBuffer.nPrintSheetNumber = PrintSheetBuf.m_nPrintSheetNumber;
				ItemBuffer.nPlateIndex = -1;
				m_TreeItemList.AddTail(ItemBuffer);

					// Colors
					nColors = PrintSheetBuf.m_BackSidePlates.GetCount();
					if (nColors > 0)
					{
						pos2 = PrintSheetBuf.m_BackSidePlates.GetHeadPosition();
						for (j = 0; j < nColors; j++)
						{
							PlateBuf = PrintSheetBuf.m_BackSidePlates.GetAt(pos2);
							TreeCtrlItem.hParent			 = hTreeItem3;
							TreeCtrlItem.item.iImage		 = 4 + PlateBuf.m_nColorDefinitionIndex;
							TreeCtrlItem.item.iSelectedImage = 4 + PlateBuf.m_nColorDefinitionIndex;
							TreeCtrlItem.item.cchTextMax = MAX_TEXT;
							TreeCtrlItem.item.pszText	 = PlateBuf.m_strPlateName.GetBuffer(MAX_TEXT);
							TreeCtrlItem.item.lParam	 = 40000 + ((j+1)*1000) + i; // > 40000 means colornames for backside
							hTreeItem4 = GetTreeCtrl().InsertItem(&TreeCtrlItem);
							PlateBuf = PrintSheetBuf.m_BackSidePlates.GetNext(pos2);

							ItemBuffer.hTreeItem = hTreeItem4;
							ItemBuffer.nType	 = CTreeItem::BackPlate;//Root || PrintSheet || FrontSide || BackSide || FrontPlate || BackPlate
							ItemBuffer.nPrintSheetNumber = PrintSheetBuf.m_nPrintSheetNumber;
							ItemBuffer.nPlateIndex = j;
							m_TreeItemList.AddTail(ItemBuffer);
						}
					}
			}
		pDoc->m_PrintSheetList.GetNext(pos);
	}	

	SelectTreeItem(nPrevPrintSheet, nPrevSight);
}


void CPrintSheetTreeView::OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CString			  strItemText; 
	int				  nIndex, i;
	BOOL			  bChangePlateTab = FALSE;
	NM_TREEVIEW*	  pNMTreeView	  = (NM_TREEVIEW*)pNMHDR;
	CPrintSheetFrame* pParent		  = (CPrintSheetFrame*)GetParentFrame();

	m_bAllSidesClicked = FALSE;

	CImpManDoc* pDoc = GetDocument();

	m_nActSight		  = -1;
	pDoc->m_nActPlate = -1;
	nIndex			  = -1;

	pParent->m_bPressDeviceMenuActive	= TRUE;
	pParent->m_bTrimToolMenuActive		= TRUE;
	pParent->m_bTurnTumbleMenuActive	= TRUE;
	pParent->m_bShowCenterMenu			= TRUE;
	pParent->m_bAlignSectionMenuActive	= FALSE;

	// printsheetlist /  -> frontsheet + backsheet
	if (pNMTreeView->itemNew.lParam >= 0 && pNMTreeView->itemNew.lParam < 1000)
	{
		nIndex		= pNMTreeView->itemNew.lParam;
		m_nActSight	= BOTHSIDES;
	}
	// frontsidename
	if (pNMTreeView->itemNew.lParam >= 1000 && pNMTreeView->itemNew.lParam < 2000)
	{
		nIndex = pNMTreeView->itemNew.lParam- 1000;
		m_nActSight = FRONTSIDE;
		if (pParent->m_bShowPlate || pParent->m_bShowFoldSheet)
			pParent->CloseTrimTool();	// Hide active TrimToolBar and disable Trim-Menu 
	}
	// frontsidecolors / plates
	if (pNMTreeView->itemNew.lParam >= 10000 && pNMTreeView->itemNew.lParam < 40000)
	{
		nIndex = pNMTreeView->itemNew.lParam - 10000;
		for (i = 0; nIndex >= 1000; i++)
			nIndex-= 1000;
		m_nActSight						  = FRONTSIDE;
		pDoc->m_nActPlate				  = i-1;
		pParent->m_bPressDeviceMenuActive = FALSE; // PressDevice-Menu set to disable
		pParent->m_bTurnTumbleMenuActive  = FALSE; // TurnTumblePrintSheet-Menu set to disable
		if (pParent->m_bShowPlate || pParent->m_bShowFoldSheet)
			pParent->CloseTrimTool();	// Hide active TrimToolBar and disable Trim-Menu 

		bChangePlateTab = TRUE;
	}
	// backsidename
	if (pNMTreeView->itemNew.lParam >= 2000 && pNMTreeView->itemNew.lParam < 3000)
	{
		nIndex = pNMTreeView->itemNew.lParam - 2000;
		m_nActSight	= BACKSIDE;
		if (pParent->m_bShowPlate || pParent->m_bShowFoldSheet)
			pParent->CloseTrimTool();	// Hide active TrimToolBar and disable Trim-Menu 
	}
	// backsidecolors / plates
	if (pNMTreeView->itemNew.lParam >= 40000 && pNMTreeView->itemNew.lParam < 60000)
	{
		nIndex = pNMTreeView->itemNew.lParam - 40000;
		for (i = 0; nIndex >= 1000; i++)
			nIndex-= 1000;
		m_nActSight						  = BACKSIDE;
		pDoc->m_nActPlate				  = i-1;
		pParent->m_bPressDeviceMenuActive = FALSE; // PressDevice-Menu set to disable
		pParent->m_bTurnTumbleMenuActive  = FALSE; // TurnTumblePrintSheet-Menu set to disable
		if (pParent->m_bShowPlate || pParent->m_bShowFoldSheet)
			pParent->CloseTrimTool();	// Hide active TrimToolBar and disable Trim-Menu 

		bChangePlateTab = TRUE;
	}

	if (nIndex != -1)
	{	// Means NOT ALLSIDES
		m_ActPrintSheetNumber = nIndex + 1;

		pParent->OnUpdateSectionMenu();
		if (pParent->m_nViewMode == ID_VIEW_ALL_SHEETS)	// if view mode is ZOOM, do not change mode
			pParent->m_nViewMode = ID_VIEW_SINGLE_SHEETS;

		pParent->m_bShowPrintMenu = TRUE;
	}
	else
	{												 // NO m_nActLayoutSide|m_nActPlate
		m_nActSight						 = ALLSIDES; // NO pActLayout|pActPressDevice
		pParent->m_bTurnTumbleMenuActive = FALSE;	 // TurnTumblePrintSheet-Menu set to disable
		pParent->m_bShowCenterMenu		 = FALSE;	 // Center-Menues set to disable
		pParent->m_bAlignSectionActive	 = FALSE;	 // Align-Section set to unchecked
		m_ActPrintSheetNumber			 = 0;		 // NO PrintSheet selected
		pParent->CloseTrimTool();					 // Hide active TrimToolBar and disable Trim-Menu 

		if ( ! bChangePlateTab)	// Set all plates to the same drawing color
		{
			CPrintSheet* pPrintSheet;
			POSITION	  pos = pDoc->m_PrintSheetList.GetHeadPosition();
			UINT nPrintSheets = pDoc->m_PrintSheetList.GetCount(); 
			for (UINT i = 0; i < nPrintSheets; i ++)                        
			{
				pPrintSheet = &(pDoc->m_PrintSheetList.GetAt(pos));

				pPrintSheet->m_FrontSidePlates.m_nTopmostPlate = 0;
				pPrintSheet->m_BackSidePlates.m_nTopmostPlate  = 0;

				pDoc->m_PrintSheetList.GetNext(pos);
			}
		}

		pParent->m_nViewMode = ID_VIEW_ALL_SHEETS;
		pParent->m_bShowPrintMenu = FALSE;
	}

	*pResult = 0;

	if (bChangePlateTab)
	{
		CPrintSheet* pPrintSheet = pDoc->GetActPrintSheet();
		if (pPrintSheet)
		{
			pPrintSheet->m_FrontSidePlates.m_nTopmostPlate = pDoc->m_nActPlate;
			pPrintSheet->m_BackSidePlates.m_nTopmostPlate  = pDoc->m_nActPlate;
		}
		if ( ! pParent->m_bShowPlate)
			pParent->OnExposuresCheck(); 
	}

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		pView->m_bPaintStatusChanged = TRUE;
		pView->m_DisplayList.Invalidate();
	}

	// Don't update PrintSheetTreeView there would be changed the current tree-selection
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	//pDoc->UpdateAllViews(this);

#ifdef CTP
	if (pParent->m_pDlgTableCtp)
		if (pParent->m_pDlgTableCtp->m_hWnd)
			pParent->m_pDlgTableCtp->FillListCtrl();
#endif

	if (pParent->IsOpenMarkSetDlg() || pParent->m_bMarksViewActive)
		pParent->UpdateMarkDialogs();
	else
		if (pParent->m_pDlgLayoutObjectProperties)
			if (pParent->m_pDlgLayoutObjectProperties->m_hWnd)
				pParent->UpdateMarkDialogs();
}

void CPrintSheetTreeView::OnContextMenu(CWnd* /*pWnd*/, CPoint point) 
{
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_PRINTSHEETTABVIEW_POPUP));

	CMenu* m_pPopup = menu.GetSubMenu(0);
	ASSERT(m_pPopup != NULL);

	switch (m_nActSight)
	{
	  case ALLSIDES : m_pPopup->EnableMenuItem(ID_DEFINE_SHEET, MF_GRAYED);
					  break;
	  case BOTHSIDES :m_pPopup->EnableMenuItem(ID_DEFINE_SHEET, MF_GRAYED);
					  m_pPopup->EnableMenuItem(ID_SETTINGS_PRESSDEVICE, MF_GRAYED);
					  break;
	  case FRONTSIDE :
	  case BACKSIDE  :
					  break;
	}

	CWnd* pWndPopupOwner = this;
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	m_pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
}

void CPrintSheetTreeView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/) 
{
	AddPlateColorsToTree();

	ShowTreeCtrlAllSides();
}

void CPrintSheetTreeView::OnDestroy() 
{
	CTreeView::OnDestroy();
	
	m_pimagelist->DeleteImageList();
	delete m_pimagelist;

	delete m_pBkBrush;

	m_hTreeItemList.RemoveAll();
}

void CPrintSheetTreeView::SelectTreeItem(int nPrintSheetNumber, int nSight)
{
	if (nSight == ALLSIDES)
		SelectRootTreeItem();
	else
	{
		CTreeItem* pItem = FindPrintSheetTreeItem(nPrintSheetNumber, nSight);
		if (pItem)
			GetTreeCtrl().Select(pItem->hTreeItem, TVGN_CARET);
	}
}

void CPrintSheetTreeView::SelectRootTreeItem()
{
	CTreeItem* pItem = FindRootTreeItem();
	if (pItem)
	{
		GetTreeCtrl().Select(pItem->hTreeItem, TVGN_CARET);
		GetTreeCtrl().Expand(pItem->hTreeItem, TVE_EXPAND);
	}
}

void CPrintSheetTreeView::SelectPrintSheetTreeItem(int nPrintSheetNumber)
{
	CTreeItem* pItem = FindPrintSheetTreeItem(nPrintSheetNumber);
	if (pItem)
		GetTreeCtrl().Select(pItem->hTreeItem, TVGN_CARET);
}

void CPrintSheetTreeView::SelectPlateTreeItem(CPrintSheet* pPrintSheet, CPlate* pPlate) 
{
	CTreeItem* pItem = FindPlateTreeItem(pPrintSheet, pPlate);
	if (pItem)
		GetTreeCtrl().Select(pItem->hTreeItem, TVGN_CARET);
}

void CPrintSheetTreeView::EnablePlateTreeItem(CPrintSheet* pPrintSheet, CPlate* pPlate, BOOL bEnable) 
{
	CTreeItem* pItem = FindPlateTreeItem(pPrintSheet, pPlate);
	if (pItem)
		GetTreeCtrl().SetItemState(pItem->hTreeItem, (bEnable) ? 0 : TVIS_CUT, TVIS_CUT);
}

CTreeItem* CPrintSheetTreeView::FindRootTreeItem()
{
	CTreeItem* pItem;
	POSITION   pos = m_TreeItemList.GetHeadPosition();
	while (pos)
	{
		pItem = &m_TreeItemList.GetNext(pos);
		if (pItem->nType == CTreeItem::Root)
			return pItem;
	}

	return NULL;
}

CTreeItem* CPrintSheetTreeView::FindPrintSheetTreeItem(int nPrintSheetNumber, int nSight)
{
	CTreeItem* pItem;
	POSITION   pos = m_TreeItemList.GetHeadPosition();
	if (nSight == -1)
	{
		while (pos)
		{
			pItem = &m_TreeItemList.GetNext(pos);
			if (pItem->nPrintSheetNumber == nPrintSheetNumber && pItem->nType == CTreeItem::PrintSheet)
				return pItem;
		}
	}
	else
	{
		while (pos)
		{
			pItem = &m_TreeItemList.GetNext(pos);
			if (pItem->nPrintSheetNumber == nPrintSheetNumber)
				if ( ((pItem->nType == CTreeItem::FrontSide)  && (nSight == FRONTSIDE)) ||
				     ((pItem->nType == CTreeItem::BackSide)   && (nSight == BACKSIDE))  ||
					 ((pItem->nType == CTreeItem::PrintSheet) && (nSight == BOTHSIDES)) )
				return pItem;
		}
	}

	return NULL;
}

CTreeItem* CPrintSheetTreeView::FindPlateTreeItem(CPrintSheet* pPrintSheet, CPlate* pPlate) 
{
	int nPrintSheetNumber = pPrintSheet->m_nPrintSheetNumber;
	int	nSheetSide		  = pPrintSheet->GetPlatesSheetSide(pPlate);
	int nPlateIndex		  = -1;
	switch (nSheetSide)
	{
	case FRONTSIDE : nPlateIndex = pPrintSheet->m_FrontSidePlates.GetPlateIndex(pPlate); break;
	case BACKSIDE  : nPlateIndex = pPrintSheet->m_BackSidePlates.GetPlateIndex(pPlate);  break;
	}	
	
	POSITION pos = m_TreeItemList.GetHeadPosition();
	while (pos)
	{
		CTreeItem* pItem = &m_TreeItemList.GetNext(pos);
		if (pItem->nPrintSheetNumber == nPrintSheetNumber && pItem->nType == CTreeItem::PrintSheet)
		{
			while (pos)
			{
				pItem = &m_TreeItemList.GetNext(pos);
				switch (nSheetSide)
				{
				case FRONTSIDE:	if (pItem->nPrintSheetNumber == nPrintSheetNumber && pItem->nType == CTreeItem::FrontSide)
								{
									while (pos)
									{
										pItem = &m_TreeItemList.GetNext(pos);
										if (pItem->nPlateIndex == nPlateIndex)
											return pItem;
									}
								}
								break;
				case BACKSIDE:	if (pItem->nPrintSheetNumber == nPrintSheetNumber && pItem->nType == CTreeItem::BackSide)
								{
									while (pos)
									{
										pItem = &m_TreeItemList.GetNext(pos);
										if (pItem->nPlateIndex == nPlateIndex)
											return pItem;
									}
								}
								break;
				}
			}
		}
	}

	return NULL;
}

void CPrintSheetTreeView::SynchronizePlateTreeItems(CDisplayList& displayList)
{
	CImpManDoc* pDoc = GetDocument();

    POSITION pos = m_TreeItemList.GetHeadPosition();
    while (pos)
    {
    	CTreeItem* pItem = &m_TreeItemList.GetNext(pos);
		if ((pItem->nType != CTreeItem::FrontPlate) && (pItem->nType != CTreeItem::BackPlate))
			continue;

    	CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.GetPrintSheetFromNumber(pItem->nPrintSheetNumber);
    	if (pPrintSheet)
		{
			CPlate* pPlate = (pItem->nType == CTreeItem::FrontPlate) ? pPrintSheet->m_FrontSidePlates.GetPlateByIndex(pItem->nPlateIndex) : 
																	   pPrintSheet->m_BackSidePlates.GetPlateByIndex(pItem->nPlateIndex);
			CDisplayItem* pDI = displayList.GetItemFromData((void*)pPlate, (void*)pPrintSheet,
															DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
			if (pDI)
				if (pDI->m_nState == CDisplayItem::Selected)
					GetTreeCtrl().SetItemState(pItem->hTreeItem, TVIS_CUT, TVIS_CUT);
				else
					GetTreeCtrl().SetItemState(pItem->hTreeItem, 0, TVIS_CUT);
			else
				GetTreeCtrl().SetItemState(pItem->hTreeItem, 0, TVIS_CUT);
		}
    }
}

void CPrintSheetTreeView::HighlightTreeItems(CLayout* pLayout) 
{
	CImpManDoc* pDoc = GetDocument();

    POSITION pos = m_TreeItemList.GetHeadPosition();
    while (pos)
    {
    	CTreeItem* pItem = &m_TreeItemList.GetNext(pos);

    	CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.GetPrintSheetFromNumber(pItem->nPrintSheetNumber);
    	if (pPrintSheet)
		{
			if ((pItem->nType == CTreeItem::FrontPlate) || (pItem->nType == CTreeItem::BackPlate))
				if (GetTreeCtrl().GetItemState(pItem->hTreeItem, TVIS_CUT) == TVIS_CUT)
					continue;

    		if (pLayout)
				if (pPrintSheet->GetLayout() == pLayout)
	    			GetTreeCtrl().SetItemState(pItem->hTreeItem, TVIS_BOLD, TVIS_BOLD | TVIS_CUT);
    			else
    				GetTreeCtrl().SetItemState(pItem->hTreeItem, TVIS_CUT, TVIS_BOLD | TVIS_CUT);
			else
   				GetTreeCtrl().SetItemState(pItem->hTreeItem, TVIS_BOLD, TVIS_BOLD | TVIS_CUT);
		}
    }
}

void CPrintSheetTreeView::LowlightTreeItems(CDisplayList& displayList)
{
	CImpManDoc* pDoc = GetDocument();

    POSITION pos = m_TreeItemList.GetHeadPosition();
    while (pos)
    {
    	CTreeItem*	 pItem		 = &m_TreeItemList.GetNext(pos);
    	CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.GetPrintSheetFromNumber(pItem->nPrintSheetNumber);
    	if (pPrintSheet)
		{
			if ((pItem->nType == CTreeItem::FrontPlate) || (pItem->nType == CTreeItem::BackPlate))
			{
				CPlate* pPlate = (pItem->nType == CTreeItem::FrontPlate) ? pPrintSheet->m_FrontSidePlates.GetPlateByIndex(pItem->nPlateIndex) : 
																		   pPrintSheet->m_BackSidePlates.GetPlateByIndex(pItem->nPlateIndex);
				CDisplayItem* pDI = displayList.GetItemFromData((void*)pPlate, (void*)pPrintSheet,
																DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
				if (pDI)
					if (pDI->m_nState == CDisplayItem::Selected)
						GetTreeCtrl().SetItemState(pItem->hTreeItem, TVIS_CUT, TVIS_CUT | TVIS_BOLD);
					else
						GetTreeCtrl().SetItemState(pItem->hTreeItem, 0, TVIS_CUT | TVIS_BOLD);
				else
					GetTreeCtrl().SetItemState(pItem->hTreeItem, 0, TVIS_CUT | TVIS_BOLD);
			}
			else
				GetTreeCtrl().SetItemState(pItem->hTreeItem, 0, TVIS_CUT | TVIS_BOLD);
		}
    }
}

HBRUSH CPrintSheetTreeView::CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/) 
{
	return (HBRUSH)(m_pBkBrush->GetSafeHandle());
}
