// CFFInput.cpp: Implementierung der Klasse CCFFInput.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Imposition Manager.h"
#include "GraphicList.h"
#include "CFFInput.h"
#include "setjmp.h"
#include "DlgProductionManager.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


int cffsymtab[] = {EOFSYM, MAINSYM, ENDSYM, UMSYM, UISYM, LLSYM, URSYM,
SCALESYM, LINESYM, ARCSYM, TEXTSYM, CALLSYM, SUBSYM,
FINISHSYM, LAYOUTSYM , KOMMA};

TCHAR *cffreswort[] = {_T("$EOF"), _T("MAIN"), _T("END"), _T("UM"), _T("UI"), _T("LL"), _T("UR"), _T("SCALE"),
_T("L"), _T("A"), _T("T"), _T("C"), _T("SUB"), _T("FINISH"), _T("LAYOUT"), _T(",")};



static jmp_buf cffcompile_error;


//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion 
//////////////////////////////////////////////////////////////////////

CCFFInput::CCFFInput()
{
	m_nProductPartIndex = -1;
}

CCFFInput::CCFFInput(int nProductPartIndex)
{
	m_nProductPartIndex = nProductPartIndex;
}

CCFFInput::~CCFFInput()
{
	m_graphicList.Destruct();	
}

BOOL CCFFInput::DoImport(const CString& strFilePath)
{
	if (setjmp(cffcompile_error) != 0) /* hierhin springen wenn Fehler */
		return FALSE;
	
	if (!m_CFFFile.Open(strFilePath, CFile::modeRead))
		return FALSE;
	
	ges_netto_xw0 = ges_netto_yw0 = FLT_MAX;
	ges_netto_xw1 = ges_netto_yw1 = -FLT_MAX;
	global_scalex = global_scaley = 1.0;
	nsub = 0;
	
	vorx = vory = FLT_MAX;
	
	ProcessSubroutines();
	ProcessMainprocedure();
	
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CPrintSheet &rPrintSheet = pDoc->m_PrintSheetList.GetTail();
		CLayout* pLayout = rPrintSheet.GetLayout();
		if (pLayout)
		{
			CPrintingProfile& rPrintingProfile = pLayout->GetPrintingProfile();
			pLayout->m_FrontSide.PositionLayoutSide(rPrintingProfile.m_press.m_pressDevice.m_fGripper, &rPrintingProfile.m_press.m_pressDevice);
			pLayout->m_BackSide.PositionLayoutSide(rPrintingProfile.m_press.m_pressDevice.m_fGripper,  &rPrintingProfile.m_press.m_pressDevice);
		}
		pDoc->m_PageTemplateList.SynchronizeToFlatProductList(pDoc->m_flatProducts, m_nProductPartIndex);
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
	}
	
	return TRUE;
}

void CCFFInput::ProcessSubroutines()
{
	m_CFFFile.SeekToBegin();
	zeilennummer = 1;
	
	ch = ' ';
	GetSymbol();
	
	while (sym != SUBSYM)
		GetSymbol();
	
	while (sym == SUBSYM)
		Subroutine();
}

void CCFFInput::ProcessMainprocedure()
{
	m_CFFFile.SeekToBegin();
	zeilennummer = 1;
	
	ch = ' ';
	GetSymbol();
	
	while (sym != MAINSYM)
		GetSymbol();
	
	Mainprocedure();
}

void CCFFInput::Mainprocedure()
{
	if (sym != MAINSYM)
		Error();
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetString(EOL);
	if (sym != IDENT)
		Error();
	
	GetSymbol();
	while (Firstcff(MAINCOMMAND, sym))
		Maincommand();
	
	if (sym != ENDSYM)
		Error();
	
	GetSymbol();
}

int CCFFInput::Subroutine()
{
	if (sym != SUBSYM)
		Error();
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetString(EOL);
	if (sym != IDENT)
		Error();
	
	_tcscpy(subdata[nsub].name, id);
	subdata[nsub].maxx = subdata[nsub].maxy = -FLT_MAX;
	subdata[nsub].minx = subdata[nsub].miny = FLT_MAX;
	
	//  create_konturliste(&konturliste);
	CGraphicPath* pGraphicPath = new CGraphicPath;
	
	GetSymbol();
	while (Firstcff(SUBCOMMAND, sym))
		Subcommand(pGraphicPath);
	
	if (sym != ENDSYM)
		Error();
	
	//  schiebe_nach_null(konturliste, subdata[nsub].minx, subdata[nsub].miny);
	//  drucke_liste_datei(konturliste);
	pGraphicPath->MoveToOrigin(subdata[nsub].minx, subdata[nsub].miny);
	
	m_graphicList.AddObject(subdata[nsub].name, pGraphicPath);

	//  if (!(kontur_in_tabelle(subdata[nsub].name, konturliste)))
	//    return(FALSE);
	
	if (subdata[nsub].minx == FLT_MAX)
		subdata[nsub].name[0] = '\0';  /* Unterprogramm loeschen, da keine geometr. Daten */
	else
		nsub++;
	
	GetSymbol();
	return(TRUE);
}

int CCFFInput::Syntaxcheck()
{
	ch = ' ';
	GetSymbol(); 
	
	while (sym != MAINSYM)
		GetSymbol();
	
	Mainprocedure();
	while (sym == SUBSYM)
		Subroutine();
	
	if (sym == FINISHSYM)
	{
		GetSymbol();
		if (sym == LAYOUTSYM)
			GetSymbol();
		else
			Error();
	}
	if (sym != EOFSYM)
		Error();
	
	return(TRUE);
}

void CCFFInput::Maincommand()
{
	TCHAR subname[10];
	
	
	switch (sym)
	{
	case UMSYM    : GetSymbol();
		break;
		
	case UISYM    : GetSymbol();
		break;
		
	case LLSYM    : GetSymbol();
		if (sym != KOMMA)
			if (sym == URSYM)  // Wenn keine Koordinaten
				break;           // f�r 'LL' angegeben wurden
			else
				Error();
			else
			{
				GetSymbol();
				if (sym != NUM)
                    Error();
				GetSymbol();
				if (sym != KOMMA)
                    Error();
				GetSymbol();
				if (sym != NUM)
                    Error();
				GetSymbol();
				break;
			}
			
	case URSYM    : GetSymbol();
		if (sym != KOMMA)
			if (sym == SCALESYM)  // Wenn keine Koordinaten
				break;              // f�r 'UR' angegeben wurden
			else
				Error();
			else
			{
				GetSymbol();
				if (sym != NUM)
                    Error();
				GetSymbol();
				if (sym != KOMMA)
                    Error();
				GetSymbol();
				if (sym != NUM)
                    Error();
				GetSymbol();
				break;
			}
			
	case SCALESYM : GetSymbol();
		if (sym != KOMMA)
			Error();
		GetSymbol();
		if (sym != NUM)
			Error();
		global_scalex = zahl;
		GetSymbol();
		if (sym != KOMMA)
			Error();
		GetSymbol();
		if (sym != NUM)
			Error();
		global_scaley = zahl;
		GetSymbol();
		break;
		
	case LINESYM  : GetSymbol();
		if (sym != KOMMA)
			Error();
		GetSymbol();
		Lineparams(&startx, &starty, &endx, &endy);
		break;
		
	case ARCSYM   : GetSymbol();
		if (sym != KOMMA)
			Error();
		GetSymbol();
		Arcparams(&startx, &starty, &endx, &endy, &centx, &centy, &direct);
		break;
		
	case TEXTSYM  : GetSymbol();
		if (sym != KOMMA)
			Error();
		GetSymbol();
		Textparams();
		if (sym != IDENT)
			Error();
		GetSymbol();
		break;
		
	case CALLSYM  : GetSymbol();
		if (sym != KOMMA)
			Error();
		GetString(KOMMA);
		_tcscpy(subname, id);
		Callparams(&startx, &starty, &angle, &scalex, &scaley);
		ProcessCall(subname, startx, starty, angle, scalex, scaley);
		break;
	}
}

void CCFFInput::Subcommand(CGraphicPath* pGraphicPath)
{
	//  KLELEM *klelem;
	float  aw, sw, radius, ausbucht;
	
	switch (sym)
	{
	case LINESYM  : GetSymbol();
					if (sym != KOMMA)
						Error();
					GetSymbol();
					Lineparams(&startx, &starty, &endx, &endy);
					
					if (LinetypeUebernehmen(linetype))
					{
						if ( ! pGraphicPath->PosIdentical(startx, starty, vorx, vory))
						{
							pGraphicPath->AppendSeqend();
							pGraphicPath->AppendVertex(startx, starty, linetype);
						}
						pGraphicPath->AppendVertex(endx, endy, linetype);
						
						vorx = endx; vory = endy;
						
						BoxForLine(startx, starty, endx, endy);
					}
					
					break;
		
	case ARCSYM   : GetSymbol();
					if (sym != KOMMA)
						Error();
					GetSymbol();
					Arcparams(&startx, &starty, &endx, &endy, &centx, &centy, &direct);
					
					if (LinetypeUebernehmen(linetype))
					{
						aw = FindAngle(centx, centy, startx, starty);
						sw = FindAngle(centx, centy, endx, endy);
						aw = (aw/360.0f) * (float)CGraphicPath::PI_2();
						sw = (sw/360.0f) * (float)CGraphicPath::PI_2();
						
						if (aw == sw)
						{
							radius = FindLength(centx, centy, startx, starty);
							pGraphicPath->AppendSeqend();
							pGraphicPath->AppendCircle(centx, centy, radius, linetype);
							pGraphicPath->AppendSeqend();
						}
						else
						{
							if ((direct == 1) && (aw > sw))
								sw+= (float)CGraphicPath::PI_2();
							if ((direct == -1) && (aw < sw))
								aw+= (float)CGraphicPath::PI_2();
							
							ausbucht = (float)tan(fabs(sw - aw)/4.0f) * direct;
							if ( ! pGraphicPath->PosIdentical(startx, starty, vorx, vory))
							{
								pGraphicPath->AppendSeqend();
								pGraphicPath->AppendVertex(startx, starty, linetype);
							}
							
							if (pGraphicPath->m_elements.GetSize())
							{
								CGraphicPathElement* pLastElem = pGraphicPath->m_elements[pGraphicPath->m_elements.GetSize() - 1];
								if (pLastElem->IsVertex())
									pLastElem->SetVertexIndent(ausbucht);
							}
							
							pGraphicPath->AppendVertex(endx, endy, linetype);
						}
						
						vorx = endx; vory = endy;
						
						BoxForArc(startx, starty, endx, endy, centx, centy, direct);
					}
					
					break;
		
	case TEXTSYM  : GetSymbol();
					if (sym != KOMMA)
						Error();
					GetSymbol();
					Textparams();
					if (sym != IDENT)
						Error();
					GetSymbol();
					break;
	}
}

void CCFFInput::Lineparams(float *startx,float *starty,float *endx,float *endy)
{
	Attributes();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	Coordinate(startx, starty);
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	Coordinate(endx, endy);
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	Brigde();
}

void CCFFInput::Arcparams(float *startx,float *starty,float *endx,float *endy,float *centx,float *centy,float *direct)
{
	Attributes();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	Coordinate(startx, starty);
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	Coordinate(endx, endy);
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	Coordinate(centx, centy);
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	
	*direct = zahl;
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	Brigde();
}

void CCFFInput::Textparams()
{
	float dummy;
	
	
	Attributes();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	Coordinate(&dummy, &dummy);
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	
	GetString(EOL);
}

void CCFFInput::Callparams(float *startx,float *starty,float *angle,float *scalex,float *scaley)
{
	if (sym != IDENT)
		Error();
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	Coordinate(startx, starty);
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	*angle = zahl;
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	*scalex = zahl;
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	*scaley = zahl;
	
	GetSymbol();
}

void CCFFInput::Attributes()
{
	if (sym != NUM)
		Error();
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	
	linetype = (int)zahl;
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	
	GetSymbol();
}

void CCFFInput::Coordinate(float *coordx,float *coordy)
{
	if (sym != NUM)
		Error();
	*coordx = zahl;
	
	GetSymbol();
	if (sym != KOMMA)
		Error();
	
	GetSymbol();
	if (sym != NUM)
		Error();
	*coordy = zahl;
	
	GetSymbol();
}

int CCFFInput::Brigde()
{
	if (sym != NUM)
		return(FALSE);	 /* Brigde-Werte sind nicht unbedingt gegeben */
	
	GetSymbol();
	if (sym != KOMMA)
		return(FALSE);
	
	GetSymbol();
	if (sym != NUM)
		return(FALSE);
	
	GetSymbol();
	return(TRUE);
}

int CCFFInput::Firstcff(int zustand,int symbol)
{
	switch (zustand)
	{
	case MAINCOMMAND: switch (symbol)
					  {
	case UMSYM:    case UISYM:   case LLSYM:  case URSYM:
	case SCALESYM: case LINESYM: case ARCSYM: case TEXTSYM:
	case CALLSYM:
		return(1);
	default     :  return(0);
					  }
		
	case SUBCOMMAND : switch (symbol)
					  {
		  case LINESYM :
		  case ARCSYM  :
		  case TEXTSYM : return(1);
		  default      : return(0);
					  }
	default	    : return(0);
	}
}

int CCFFInput::SucheSymbol(TCHAR *string, int symtab[], TCHAR *reswort[])
{
	int i, gef;
	
	
	i = 0; gef = FALSE;
	while (!gef && (i < ANZSYM))
	{
		if (_tcscmp(string, reswort[i]) == 0)
			gef = TRUE;
		i++;
	}
	
	if (gef)
		return(symtab[i - 1]);
	else
		return(NUL);  /* NUL ist ein cff-symbol */
}

int CCFFInput::LinetypeUebernehmen(int linetype)
{
	int bit;
	
	
	switch (linetype)
	{
	case 1 : bit = 1;	 break;
	case 2 : bit = 2;	 break;
	case 3 : bit = 4;	 break;
	case 4 : bit = 8;	 break;
	case 40: bit = 16;	 break;
	case 41: bit = 32;	 break;
	case 42: bit = 64;	 break;
	case 43: bit = 128;  break;
	case 44: bit = 256;  break;
	case 45: bit = 512;  break;
	case 46: bit = 1024; break;
	case 99: bit = 2048; break;
	}
	
	//  if (opt.kont_cfflinetypes & bit)
	if (1)
		return(TRUE);
	else
		return(FALSE);
}


/***************************** Analyse-Funktionen ***************************/

#define TODEGS 180/3.14159265
#define TORADS 3.14159265/180.0

void CCFFInput::BoxForLine(float startx,float starty,float endx,float endy)
/*
create the smallest box possible for each line
*/
{
	float maxx,maxy,minx,miny;
	
	
	maxx = max(startx,endx);
	maxy = max(starty,endy);
	minx = min(startx,endx);
	miny = min(starty,endy);
	subdata[nsub].maxx = max(subdata[nsub].maxx,maxx);
	subdata[nsub].minx = min(subdata[nsub].minx,minx);
	subdata[nsub].maxy = max(subdata[nsub].maxy,maxy);
	subdata[nsub].miny = min(subdata[nsub].miny,miny);
}

void CCFFInput::BoxForArc(float x1,float y1,float x2,float y2,float xc,float yc,float direction)
/*
find the smallest box required to
contain the arc
verify for all arcs !!!!!!!!!!!!!
*/
{
	float radius,alpha1,alpha2;  /*angle of start, end points*/
	float qx[4],qy[4];		 /*vertices of the circle*/
	float maxx,maxy,minx,miny;	 /*size of box*/
	int   quadstart,quadend;	 /*quadrants of the angles*/
	int   i;
	/* float angle;		  angular difference between the two angles*/
	float temp; 		 /*for swapping x values*/
	
	
	if(direction == -1) 	 /*swap the values*/
	{
		temp = x1;
		x1 = x2;
		x2 = temp;
		temp = y1;
		y1 = y2;
		y2 = temp;
	}
	
	radius = FindLength(xc,yc,x1,y1);
	qx[0] = xc; 	  qy[0] = yc + radius;
	qx[1] = xc - radius ; qy[1] = yc;
	qx[2] = xc;           qy[2] = yc - radius;
	qx[3] = xc + radius ; qy[3] = yc;
	
	alpha1 = FindAngle(xc,yc,x1,y1);
	alpha2 = FindAngle(xc,yc,x2,y2);
	
	quadstart = Quadrant(alpha1);
	quadend   = Quadrant(alpha2);
	
	if (quadstart == quadend)
		if (alpha1 >= alpha2)  /* wenn (alpha1 == alpha2) ==> Vollkreis */
			quadend+= 4;
		
		if (quadstart > quadend)
			quadend+= 4;
		
		maxx = max(x1, x2);
		maxy = max(y1, y2);
		minx = min(x1, x2);
		miny = min(y1, y2);
		
		for (i = quadstart; i < quadend; i++)
		{
			maxx = max(maxx, qx[i%4]);
			maxy = max(maxy, qy[i%4]);
			minx = min(minx, qx[i%4]);
			miny = min(miny, qy[i%4]);
		}
		
		subdata[nsub].maxx = (float)max(subdata[nsub].maxx,maxx);
		subdata[nsub].maxy = (float)max(subdata[nsub].maxy,maxy);
		subdata[nsub].minx = (float)min(subdata[nsub].minx,minx);
		subdata[nsub].miny = (float)min(subdata[nsub].miny,miny);
}

float CCFFInput::FindLength(float x1,float y1,float x2,float y2)
/*
find length of line given by coordinates
*/
{
	double lx,ly,lengthF;
	
	lx = fabs((double)x1 - (double)x2);
	ly = fabs((double)y1 - (double)y2);
	
	lengthF = hypot(lx,ly);
	
	return((float)lengthF);
}

float CCFFInput::FindAngle(float x1,float y1,float x2,float y2)
/*
find the angle between the 2 points
take x1,y1 as the first point
return the angle as integer
*/
{
	double deltax, deltay, angle;
	
	
	deltax = x2 - x1; deltay = y2 - y1;
	
	if ((deltax == 0.0) && (deltay == 0.0))
		return(0.0);
	
	angle = (atan2(deltay, deltax)/(ZWEI_PI)) * 360.0;
	if (angle < 0.0)
		angle+= 360.0;
	
	return((float)angle);
}

int CCFFInput::Quadrant(float angle)
/*
find which quadrant the angle is in
return 0 --> 3
*/
{
	int quad = 0;
	
	if((angle >= 0) && (angle <= 90)) quad = 0;
	else if((angle > 90) && (angle <= 180)) quad = 1;
	else if((angle > 180) && (angle <= 270)) quad = 2;
	else if((angle > 270) && (angle <= 360)) quad = 3;
	return(quad);
}

float CCFFInput::Maximum(float x1,float x2,float x3,float x4,float x5,float x6)
/*
find max of range of values
*/
{
	float maxx;
	
	maxx = max(x1,x2);
	maxx = max(maxx,x3);
	maxx = max(maxx,x4);
	maxx = max(maxx,x5);
	maxx = max(maxx,x6);
	
	return(maxx);
}

float CCFFInput::Minimum(float x1,float x2,float x3,float x4,float x5,float x6)
/*
find the min of a range of values
*/
{
	float minx;
	
	minx = min(x1,x2);
	minx = min(minx,x3);
	minx = min(minx,x4);
	minx = min(minx,x5);
	minx = min(minx,x6);
	
	return(minx);
}

void CCFFInput::ProcessCall(TCHAR *subname,float absx,float absy,float angle,float scalex,float scaley)
/*
fill up the command structure  (during main program)

  *(x2,y2)        *(x1,y1)
  
	
	  
		*(x0,y0)        *(x3,y3)
		
		  */
{
	int    i,found;
	float  xx2,yy2,xx3,yy3;
	float  mminx = 0, mminy = 0, mmaxx = 0, mmaxy = 0;
	float  x0, y0, x1, y1;
	
	
	scalex*= global_scalex; scaley*= global_scaley;
	
	found = FALSE;
	for(i=0;i<nsub;i++)                      /*find the right subroutine*/
	{
		if(_tcscmp(subname,subdata[i].name) == 0)
		{
			mminx = subdata[i].minx;
			mminy = subdata[i].miny;
			mmaxx = subdata[i].maxx;
			mmaxy = subdata[i].maxy;
			found = TRUE;
			break;
		}
	}
	if (!found)  /* skip the call, because there is no subroutine */
		return;
	
	xx2 = mminx ; yy2 = mmaxy;
	xx3 = mmaxx ; yy3 = mminy;
	DoTransformation(absx,absy,angle,scalex,scaley,&mminx,&mminy);
	DoTransformation(absx,absy,angle,scalex,scaley,&mmaxx,&mmaxy);
	DoTransformation(absx,absy,angle,scalex,scaley,&xx2,&yy2);
	DoTransformation(absx,absy,angle,scalex,scaley,&xx3,&yy3);
	x0 = Minimum(mminx,mmaxx,xx2,xx3,xx2,xx3);
	y0 = Minimum(mminy,mmaxy,yy2,yy3,yy2,yy3);
	x1 = Maximum(mminx,mmaxx,xx2,xx3,xx2,xx3);
	y1 = Maximum(mminy,mmaxy,yy2,yy3,yy2,yy3);
	
	CFFToLabel(x0, y0, x1, y1, angle, scalex, scaley, subname, i+1);
	
	ges_netto_xw1 = max(ges_netto_xw1, x1);
	ges_netto_yw1 = max(ges_netto_yw1, y1);
	ges_netto_xw0 = min(ges_netto_xw0, x0);
	ges_netto_yw0 = min(ges_netto_yw0, y0);
}

void CCFFInput::DoTransformation(float absx,float absy,float angle,float scalex,float scaley,float *firstx,float *firsty)
/*
rotate the x data by the required angle
translate
scale
*/
{
	long double a11,a12,a21,a22;
	long double angle_in_radians;     /*angle in radians*/
	long double inx,iny;
	
	angle_in_radians = (long double)angle * (long double)TORADS;
	inx = *firstx;
	iny = *firsty;
	a11 = (long double)cos(angle_in_radians); if (fabs(a11) < 0.00001) a11 = 0.0f;
	a12 = (long double)sin(angle_in_radians); if (fabs(a12) < 0.00001) a12 = 0.0f;
	a21 = -a12;
	a22 = a11;
	*firstx = (float)(((inx * a11) + (iny * a21)) * scalex  + absx);
	*firsty = (float)(((inx * a12) + (iny * a22)) * scaley  + absy);
}

void CCFFInput::CFFToLabel(float xw0,float yw0,float xw1,float yw1,float angle,float scalex,float scaley,TCHAR *name, int sorte)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	
	if (angle < -0.01)  // neg. Winkel in pos. umrechnen 
		angle+= 360.0;
	
	int nHeadPosition = TOP;
	if ((angle > -0.01) && (angle < 0.01))  // Toleranz zulaessig 
		nHeadPosition = TOP;
	else
		if ((angle > 89.91) && (angle < 90.01))
		{
			nHeadPosition = LEFT;
			if (scalex < 0.0)
				nHeadPosition = RIGHT;
		}
		else
			if ((angle > 179.91) && (angle < 180.01))
				nHeadPosition = BOTTOM;
			else
				if ((angle > 269.91) && (angle < 270.01))
				{
					nHeadPosition = RIGHT;
					if (scalex < 0.0)
						nHeadPosition = LEFT;
				}
				else
					nHeadPosition = TOP;

	float fWidth  = ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? xw1 - xw0 : yw1 - yw0;
	float fHeight = ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? yw1 - yw0 : xw1 - xw0;
	int nFlatProductIndex	 = pDoc->m_flatProducts.FindProductIndex(name);
	if (nFlatProductIndex == -1)
	{
		CGraphicObject* pgObject = m_graphicList.GetObject(name);
		pgObject->SetScale(scalex, scaley);

		CFlatProduct flatProduct(name, fWidth, fHeight, 0, _T(""));
		flatProduct.AddGraphicObject(pgObject);

		pDoc->m_flatProducts.AddTail(flatProduct);
		nFlatProductIndex = pDoc->m_flatProducts.GetSize() - 1;
	}
				
	if ( ! pDoc->m_PrintSheetList.GetCount())
	{
		CPrintingProfile printingProfile;// = (rPrintGroup.m_strDefaultProfile.IsEmpty()) ? theApp.m_productionProfiles.FindSuitable(rPrintGroup) : theApp.m_productionProfiles.FindProfile(rPrintGroup.m_strDefaultProfile);
		//if (productionProfile.IsNull())
		//{
		//	CDlgProductionManager dlgFlat;
		//	if (dlgFlat.DoModal() == IDOK)
		//		productionProfile = dlgFlat.m_productionProfile;
		//}

		pDoc->m_PrintSheetList.AddBlankSheet(printingProfile);
	}
	
	CPrintSheet &rPrintSheet = pDoc->m_PrintSheetList.GetTail();
	CLayout* pLayout = rPrintSheet.GetLayout();
	if ( ! pLayout)
		return;

	int nComponentRefIndex = rPrintSheet.MakeFlatProductRefIndex(nFlatProductIndex);

	CLayoutObject layoutObject;
	layoutObject.CreateFlatProduct(xw0, yw0, xw1 - xw0, yw1 - yw0, nHeadPosition, nComponentRefIndex);
	pLayout->m_FrontSide.m_ObjectList.AddTail(layoutObject);
	layoutObject.CreateFlatProduct(xw0, yw0, xw1 - xw0, yw1 - yw0, nHeadPosition, nComponentRefIndex);
	pLayout->m_BackSide.m_ObjectList.AddTail(layoutObject);
	
	CLayoutObject& rFrontObject = pLayout->m_FrontSide.m_ObjectList.GetTail();
	CLayoutObject& rBackObject  = pLayout->m_BackSide.m_ObjectList.GetTail();
	rBackObject.AlignToCounterPart(&rFrontObject);
}

void CCFFInput::GetSymbol()
{
	int i;
	TCHAR string[BEZLAENGE+1];
	
	
	while ((ch == ' ') || (ch == '\n') || (ch == '\r'))
	{
		if (ch == '\n')
			zeilennummer++;
		
		GetNextCh();
	}
	
	if (isdigit(ch) || (ch == '.') || (ch == '-') || (ch == '+'))
	{
		i = 0;
		do
		{
			if (i < BEZLAENGE)
			{
				string[i] = ch;
				i++;
			}
			GetNextCh();
		}while ((ch == '.') || isdigit(ch));
		
		string[i] = '\0';
		_tcscpy(id, string);
		_tcscpy(last_string, string);
		Ascii2Float(string, "%f", &zahl);
		sym = NUM;
	}
	else
		if (isalpha(ch) || (ch == '$'))
		{
			i = 0;
			do
			{
				if (i < BEZLAENGE)
				{
					string[i] = ch;
					i++;
				}
				GetNextCh();
			}while (isalpha(ch) || isdigit(ch));
			
			string[i] = '\0';
			_tcscpy(id, string);
			_tcscpy(last_string, string);
			sym = SucheSymbol(string, cffsymtab, cffreswort);
			/*printf(id);*/
			if (sym == NUL)
				sym = IDENT;
		}
		else
		{
			switch (ch)
			{
			case ',': sym = KOMMA; GetNextCh(); break;
			default : sym = NUL; GetNextCh(); break;
			}
			/* putch(ch); */
		}
}

void CCFFInput::GetString(int ende)
{
	int i;
	TCHAR string[BEZLAENGE+1];
	
	
	while ((ch == ' ') || (ch == '\n') || (ch == '\r'))
	{
		if (ch == '\n')
			zeilennummer++;
		
		if (ende == EOL) 
		{
			GetNextCh();
			if ((ch == '\n') || (ch == '\r'))	// leerer Text
			{
				ende = -1;
				GetNextCh();
				break;
			}
		}
		else
			GetNextCh();
	}
	
	i = 0;
	switch (ende)
	{
	case EOL  : while ((ch != '\n') && (ch != '\r'))
				{
					if (i < BEZLAENGE)
					{
						string[i] = ch;
						i++;
					}
					GetNextCh();
				}
		break;
		
	case KOMMA: while (ch != ',')
				{
					if (i < BEZLAENGE)
					{
						string[i] = ch;
						i++;
					}
					GetNextCh();
				}
		break;
		
	case -1: break;
	}
	
	string[i] = '\0';
	_tcscpy(id, string);
	_tcscpy(last_string, string);
	sym = IDENT;
}

void CCFFInput::GetNextCh()
{
	ch = getc(m_CFFFile.m_pStream);
	if (ch == EOF)
		Error();
	
	if (ch == '@')  // Kommentar bis Zeilenende 
	{
		ch = getc(m_CFFFile.m_pStream);
		while ((ch != '\n') && (ch != '\r'))
			ch = getc(m_CFFFile.m_pStream);
	}
}

void CCFFInput::Error()
{
	CString strMsg;
	strMsg.Format(_T("Error in CFF-File (%d): %s!"), zeilennummer, last_string);
	int nRet = KIMOpenLogMessage(strMsg, MB_ICONERROR);
	
	longjmp(cffcompile_error, -1); 
}


