// MediaPivotControl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMediaPivotControl

CMediaPivotControl::CMediaPivotControl()
{
	m_bmMediaPivot1.LoadBitmap(IDB_MEDIA_PIVOT1); 
	m_bmMediaPivot2.LoadBitmap(IDB_MEDIA_PIVOT2); 
	m_bmMediaPivot3.LoadBitmap(IDB_MEDIA_PIVOT3); 
	m_bmMediaPivot4.LoadBitmap(IDB_MEDIA_PIVOT4); 
	m_bmMediaPivot5.LoadBitmap(IDB_MEDIA_PIVOT5); 
	m_bmMediaPivot6.LoadBitmap(IDB_MEDIA_PIVOT6); 
	m_bmMediaPivot7.LoadBitmap(IDB_MEDIA_PIVOT7); 
	m_bmMediaPivot8.LoadBitmap(IDB_MEDIA_PIVOT8); 
	m_bmMediaPivot9.LoadBitmap(IDB_MEDIA_PIVOT9); 

	m_bLockCheckButton = FALSE;
	m_nCurNX	  = -1;
	m_nCurNY	  = -1;
	m_nActiveCell = -1;
}

CMediaPivotControl::~CMediaPivotControl()
{
	m_bmMediaPivot1.DeleteObject(); 
	m_bmMediaPivot2.DeleteObject(); 
	m_bmMediaPivot3.DeleteObject(); 
	m_bmMediaPivot4.DeleteObject(); 
	m_bmMediaPivot5.DeleteObject(); 
	m_bmMediaPivot6.DeleteObject(); 
	m_bmMediaPivot7.DeleteObject(); 
	m_bmMediaPivot8.DeleteObject(); 
	m_bmMediaPivot9.DeleteObject(); 
}


BEGIN_MESSAGE_MAP(CMediaPivotControl, CStatic)
	//{{AFX_MSG_MAP(CMediaPivotControl)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CMediaPivotControl 

void CMediaPivotControl::OnMouseMove(UINT nFlags, CPoint point) 
{
	CPoint ptMouse;
	GetCursorPos(&ptMouse);
	ScreenToClient(&ptMouse);

	CRect rectPivot;
	GetClientRect(rectPivot);
	if ( ! rectPivot.PtInRect(ptMouse))
	{
		Update();
		m_nCurNX = -1; m_nCurNY = -1;
		return;
	}

	int nDX = rectPivot.Width() /3;
	int nDY = rectPivot.Height()/3;
	int nNX = (ptMouse.x < rectPivot.left + nDX) ? 0 : ((ptMouse.x < rectPivot.left + 2*nDX) ? 1 : 2);
	int nNY = (ptMouse.y < rectPivot.top  + nDY) ? 0 : ((ptMouse.y < rectPivot.top  + 2*nDY) ? 1 : 2);

	CRect rectButton  = rectPivot;
	rectButton.right  = rectButton.left + rectPivot.Width() / 3;
	rectButton.bottom = rectButton.top  + rectPivot.Height()/ 3;
	rectButton.OffsetRect(nNX * rectButton.Width(), nNY * rectButton.Height());	
	rectButton.DeflateRect(3, 3);

	if ( (nNX != m_nCurNX) || (nNY != m_nCurNY) )
	{
		m_bLockCheckButton = TRUE;

		ChooseBitmap(GetCurrentCell());
		Invalidate();
		UpdateWindow();

		CClientDC dc(this);
		dc.Draw3dRect(rectButton, RGB(231,231,214), DARKGRAY);

		m_bLockCheckButton = FALSE;

		m_nCurNX = nNX; m_nCurNY = nNY;

		m_nActiveCell = -1;
	}

	CStatic::OnMouseMove(nFlags, point);
}

int CMediaPivotControl::GetCurrentCell()
{
	if ( ! m_pParent)
		return 0;
	if (m_pParent->m_nCurTileIndex >= m_pParent->m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize())
		return 0;

	int nXRef = m_pParent->m_pdfTargetProps.m_mediaList[0].m_posInfos[m_pParent->m_nCurTileIndex].m_reflinePosParams.m_nObjectReferenceLineInX;
	int nYRef = m_pParent->m_pdfTargetProps.m_mediaList[0].m_posInfos[m_pParent->m_nCurTileIndex].m_reflinePosParams.m_nObjectReferenceLineInY;
	int nCell = 1;
	switch (nYRef)
	{
	case TOP:		switch (nXRef)
					{
					case LEFT:		nCell = 1; break;
					case XCENTER:	nCell = 2; break;
					case RIGHT:		nCell = 3; break;
					}
					break;
	case YCENTER:	switch (nXRef)
					{
					case LEFT:		nCell = 4; break;
					case XCENTER:	nCell = 5; break;
					case RIGHT:		nCell = 6; break;
					}
					break;
	case BOTTOM:	switch (nXRef)
					{
					case LEFT:		nCell = 7; break;
					case XCENTER:	nCell = 8; break;
					case RIGHT:		nCell = 9; break;
					}
					break;
	}

	return nCell;
}

void CMediaPivotControl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CPoint ptMouse;
	GetCursorPos(&ptMouse);

	CRect rectPivot;
	GetWindowRect(rectPivot);

	int nDX = rectPivot.Width() /3;
	int nDY = rectPivot.Height()/3;
	int nNX = (ptMouse.x < rectPivot.left + nDX) ? 1 : ((ptMouse.x < rectPivot.left + 2*nDX) ? 2 : 3);
	int nNY = (ptMouse.y < rectPivot.top  + nDY) ? 1 : ((ptMouse.y < rectPivot.top  + 2*nDY) ? 2 : 3);
	int nCell = (nNY - 1) * 3 + nNX;

	int nXRef = LEFT;
	int nYRef = TOP;
	switch (nCell)
	{
	case 1:	nXRef = LEFT;		nYRef = TOP;		break;
	case 2:	nXRef = XCENTER;	nYRef = TOP;		break;
	case 3:	nXRef = RIGHT;		nYRef = TOP;		break;
	case 4:	nXRef = LEFT;		nYRef = YCENTER;	break;
	case 5:	nXRef = XCENTER;	nYRef = YCENTER;	break;
	case 6:	nXRef = RIGHT;		nYRef = YCENTER;	break;
	case 7:	nXRef = LEFT;		nYRef = BOTTOM;		break;
	case 8:	nXRef = XCENTER;	nYRef = BOTTOM;		break;
	case 9:	nXRef = RIGHT;		nYRef = BOTTOM;		break;
	default:return;
	}

	m_pParent->m_pdfTargetProps.m_mediaList[0].m_posInfos[m_pParent->m_nCurTileIndex].m_reflinePosParams.m_nObjectReferenceLineInX = (unsigned char)nXRef;
	m_pParent->m_pdfTargetProps.m_mediaList[0].m_posInfos[m_pParent->m_nCurTileIndex].m_reflinePosParams.m_nObjectReferenceLineInY = (unsigned char)nYRef;

	ChooseBitmap(nCell);	// forces OnPaint() to be called, which then calls CheckButton()

	m_pParent->UpdatePrintSheetView();

	CStatic::OnLButtonDown(nFlags, point);
}

void CMediaPivotControl::ChooseBitmap(int nCell)
{
	switch (nCell)
	{
	case 1:	SetBitmap((HBITMAP)m_bmMediaPivot1); break;
	case 2:	SetBitmap((HBITMAP)m_bmMediaPivot2); break;
	case 3:	SetBitmap((HBITMAP)m_bmMediaPivot3); break;
	case 4:	SetBitmap((HBITMAP)m_bmMediaPivot4); break;
	case 5:	SetBitmap((HBITMAP)m_bmMediaPivot5); break;
	case 6:	SetBitmap((HBITMAP)m_bmMediaPivot6); break;
	case 7:	SetBitmap((HBITMAP)m_bmMediaPivot7); break;
	case 8:	SetBitmap((HBITMAP)m_bmMediaPivot8); break;
	case 9:	SetBitmap((HBITMAP)m_bmMediaPivot9); break;
	}
}

void CMediaPivotControl::CheckButton(int nCell)
{
	if (m_bLockCheckButton)
		return;

	CRect rectPivot;
	GetClientRect(rectPivot);
	CRect rectButton  = rectPivot;
	rectButton.right  = rectButton.left + rectPivot.Width() / 3;
	rectButton.bottom = rectButton.top  + rectPivot.Height()/ 3;

	int nNX	  = 0;
	int nNY   = 0;
	switch (nCell)
	{
	case 1:	nNX = 0; nNY = 0; break;
	case 2:	nNX = 1; nNY = 0; break;
	case 3:	nNX = 2; nNY = 0; break;
	case 4:	nNX = 0; nNY = 1; break;
	case 5:	nNX = 1; nNY = 1; break;
	case 6:	nNX = 2; nNY = 1; break;
	case 7:	nNX = 0; nNY = 2; break;
	case 8:	nNX = 1; nNY = 2; break;
	case 9:	nNX = 2; nNY = 2; break;
	default:return;
	}

	rectButton.OffsetRect(nNX * rectButton.Width(), nNY * rectButton.Height());	
	rectButton.DeflateRect(3, 3);

	CClientDC dc(this);
	dc.Draw3dRect(rectButton, DARKGRAY, RGB(231,231,214));

	m_nActiveCell = nCell;
}

void CMediaPivotControl::Update()
{
	int nCell = GetCurrentCell();
	if (nCell != m_nActiveCell)
	{
		ChooseBitmap(nCell);
		CheckButton(nCell);
	}
}

void CMediaPivotControl::OnPaint() 
{
	CStatic::OnPaint();		// to draw selected bitmap

	CheckButton(GetCurrentCell());
}
