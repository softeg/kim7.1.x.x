// DlgSheetObjectProperties.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "ObjectPivotControl.h"
#include "DlgSheetObjectPDFProps.h"
#include "DlgSheetObjectPositioning.h"
#include "DlgMarkPlacement.h"
#include "DlgMoveObjects.h"
#include "DlgSheetObjectProperties.h"
#include "DlgLayoutObjectProperties.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSheetObjectProperties 


CDlgSheetObjectProperties::CDlgSheetObjectProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSheetObjectProperties::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSheetObjectProperties)
	//}}AFX_DATA_INIT

	m_pLayoutObject = NULL;
	m_pTemplate		= NULL;
	m_pMark			= NULL;
}

CDlgSheetObjectProperties::~CDlgSheetObjectProperties()
{
	DestroyWindow();
}

void CDlgSheetObjectProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSheetObjectProperties)
	DDX_Control(pDX, IDC_SHEETOBJECTPROPS_TAB, m_sheetObjectPropsTab);
	//}}AFX_DATA_MAP
}

BOOL CDlgSheetObjectProperties::Create(CLayoutObject* pLayoutObject, CPageTemplate* pTemplate, CWnd* pParent)
{
	m_nObjectCategory = LayoutObject;
	m_nObjectType	  = pLayoutObject->m_nType;
	m_pLayoutObject	  = pLayoutObject;
	m_pTemplate		  = pTemplate;
	return CDialog::Create(IDD_SHEETOBJECT_PROPERTIES, pParent);
}

BOOL CDlgSheetObjectProperties::Create(CMark* pMark, CWnd* pParent)
{
	m_nObjectCategory = UnassignedMark;
	m_nObjectType	  = pMark->m_nType;
	m_pMark			  = pMark;
	return CDialog::Create(IDD_SHEETOBJECT_PROPERTIES, pParent);
}

BEGIN_MESSAGE_MAP(CDlgSheetObjectProperties, CDialog)
	//{{AFX_MSG_MAP(CDlgSheetObjectProperties)
	ON_NOTIFY(TCN_SELCHANGE, IDC_SHEETOBJECTPROPS_TAB, OnSelchangeSheetObjectPropsTab)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgSheetObjectProperties 

BOOL CDlgSheetObjectProperties::OnInitDialog() 
{
	CDialog::OnInitDialog();

	switch (m_nObjectCategory)
	{
	case LayoutObject:		switch (m_nObjectType)
							{
							case CLayoutObject::Page:			InitLayoutObjectDialog();
																break;
							case CLayoutObject::ControlMark:	switch (m_pTemplate->GetMarkType())
																{
																case CMark::CustomMark:	InitLayoutObjectDialog();	break;
																case CMark::TextMark:								break;
																case CMark::SectionMark:							break;
																case CMark::CropMark:								break;
																}
																break;
							}
							break;
		
	case UnassignedMark:	switch (m_nObjectType)
							{
							case CMark::CustomMark:		InitCustomMarkDialog();	break;
							case CMark::TextMark:		InitTextMarkDialog();	break;
							case CMark::SectionMark:	InitSectionMarkDialog();break;
							case CMark::CropMark:		InitCropMarkDialog();	break;
							}
							break;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgSheetObjectProperties::InitCustomMarkDialog()
{
	m_sheetObjectPropsTab.DeleteAllItems();

	CString strGeneral("Allgemein");
	CString strPositioning("Positionierung");
	CString strPlacement("Plazierung");

	TC_ITEM tcItem;
	tcItem.mask			= TCIF_TEXT;
	tcItem.cchTextMax	= 0;
	tcItem.iImage		= -1;
	tcItem.lParam		= 0;
	tcItem.pszText		= strGeneral.GetBuffer(MAX_TEXT);
	m_sheetObjectPropsTab.InsertItem(m_sheetObjectPropsTab.GetItemCount(), &tcItem);

	tcItem.pszText		= strPositioning.GetBuffer(MAX_TEXT);
	m_sheetObjectPropsTab.InsertItem(m_sheetObjectPropsTab.GetItemCount(), &tcItem);

	tcItem.pszText		= strPlacement.GetBuffer(MAX_TEXT);
	m_sheetObjectPropsTab.InsertItem(m_sheetObjectPropsTab.GetItemCount(), &tcItem);

	m_sheetObjectPropsTab.SetCurSel(0);


	m_dlgSheetObjectPDFProps.Create(IDD_SHEETOBJECT_PDFPROPS, this);

	CRect dlgRect, tabRect;
	m_sheetObjectPropsTab.GetWindowRect(tabRect);
	TabCtrl_AdjustRect(m_sheetObjectPropsTab.m_hWnd, FALSE, &tabRect);
	ScreenToClient(tabRect);
	m_dlgSheetObjectPDFProps.GetWindowRect(dlgRect);
	m_dlgSheetObjectPDFProps.SetWindowPos(NULL, tabRect.left, tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);


	m_dlgSheetObjectPosition.m_objectPivotCtrl.Reinit(IDD_SHEETOBJECT_POSITIONING);
	m_dlgSheetObjectPosition.Create(IDD_SHEETOBJECT_POSITIONING, this);

	m_dlgSheetObjectPosition.GetWindowRect(dlgRect);
	m_dlgSheetObjectPosition.SetWindowPos(NULL, tabRect.left, tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);

	m_dlgSheetObjectPosition.ShowWindow(SW_HIDE);


	m_dlgMarkPlacement.Create(IDD_MARK_PLACEMENT, this);

	m_sheetObjectPropsTab.GetWindowRect(tabRect);
	TabCtrl_AdjustRect(m_sheetObjectPropsTab.m_hWnd, FALSE, &tabRect);
	ScreenToClient(tabRect);
	m_dlgMarkPlacement.GetWindowRect(dlgRect);
	m_dlgMarkPlacement.SetWindowPos(NULL, tabRect.left, tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);

	m_dlgMarkPlacement.ShowWindow(SW_HIDE);

	LoadCustomMarkData();		
}

void CDlgSheetObjectProperties::InitTextMarkDialog()
{
}

void CDlgSheetObjectProperties::InitSectionMarkDialog()
{
}

void CDlgSheetObjectProperties::InitCropMarkDialog()
{
}

void CDlgSheetObjectProperties::InitLayoutObjectDialog()
{
	m_sheetObjectPropsTab.DeleteAllItems();

	CString strGeneral("Allgemein");
	CString strPositioning("Positionierung");

	TC_ITEM tcItem;
	tcItem.mask			= TCIF_TEXT;
	tcItem.cchTextMax	= 0;
	tcItem.iImage		= -1;
	tcItem.lParam		= 0;
	tcItem.pszText		= strGeneral.GetBuffer(MAX_TEXT);
	m_sheetObjectPropsTab.InsertItem(m_sheetObjectPropsTab.GetItemCount(), &tcItem);

	tcItem.pszText		= strPositioning.GetBuffer(MAX_TEXT);
	m_sheetObjectPropsTab.InsertItem(m_sheetObjectPropsTab.GetItemCount(), &tcItem);

	m_sheetObjectPropsTab.SetCurSel(0);


	m_dlgSheetObjectPDFProps.Create(IDD_SHEETOBJECT_PDFPROPS, this);

	CRect dlgRect, tabRect;
	m_sheetObjectPropsTab.GetWindowRect(tabRect);
	TabCtrl_AdjustRect(m_sheetObjectPropsTab.m_hWnd, FALSE, &tabRect);
	ScreenToClient(tabRect);
	m_dlgSheetObjectPDFProps.GetWindowRect(dlgRect);
	m_dlgSheetObjectPDFProps.SetWindowPos(NULL, tabRect.left, tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);


	m_dlgSheetObjectPosition.m_objectPivotCtrl.Reinit(IDD_SHEETOBJECT_POSITIONING);
	m_dlgSheetObjectPosition.Create(IDD_SHEETOBJECT_POSITIONING, this);

	m_dlgSheetObjectPosition.GetWindowRect(dlgRect);
	m_dlgSheetObjectPosition.SetWindowPos(NULL, tabRect.left, tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);

	m_dlgSheetObjectPosition.ShowWindow(SW_HIDE);

	LoadLayoutObjectData();		
}

void CDlgSheetObjectProperties::LoadLayoutObjectData()
{
	m_dlgSheetObjectPDFProps.LoadLayoutObjectData();
	m_dlgSheetObjectPosition.LoadLayoutObjectData();
}

void CDlgSheetObjectProperties::SaveLayoutObjectData()
{
	if (m_dlgSheetObjectPDFProps.IsWindowVisible())
		m_dlgSheetObjectPDFProps.SaveLayoutObjectData();
	if (m_dlgSheetObjectPosition.IsWindowVisible())
		m_dlgSheetObjectPosition.SaveLayoutObjectData(HORIZONTAL | VERTICAL);
}

void CDlgSheetObjectProperties::LoadCustomMarkData()
{
	m_dlgSheetObjectPDFProps.LoadCustomMarkData(m_pMark);
	m_dlgSheetObjectPosition.LoadCustomMarkData(m_pMark);
//	m_dlgMarkPlacement.LoadCustomMarkData(m_pMark);
}

void CDlgSheetObjectProperties::OnSelchangeSheetObjectPropsTab(NMHDR* /* pNMHDR */, LRESULT* pResult) 
{
	int nSel = m_sheetObjectPropsTab.GetCurSel();

	switch (nSel)
	{
	case 0:	if (m_dlgSheetObjectPDFProps.m_hWnd)	m_dlgSheetObjectPDFProps.ShowWindow(SW_SHOW);	
			if (m_dlgSheetObjectPosition.m_hWnd)	m_dlgSheetObjectPosition.ShowWindow(SW_HIDE);	
			if (m_dlgMarkPlacement.m_hWnd)			m_dlgMarkPlacement.ShowWindow	   (SW_HIDE);
			break;
	case 1: if (m_dlgSheetObjectPDFProps.m_hWnd)	m_dlgSheetObjectPDFProps.ShowWindow(SW_HIDE);	
			if (m_dlgSheetObjectPosition.m_hWnd)	m_dlgSheetObjectPosition.ShowWindow(SW_SHOW);	
			if (m_dlgMarkPlacement.m_hWnd)			m_dlgMarkPlacement.ShowWindow	   (SW_HIDE);
			break;
	case 2: if (m_dlgSheetObjectPDFProps.m_hWnd)	m_dlgSheetObjectPDFProps.ShowWindow(SW_HIDE);	
			if (m_dlgSheetObjectPosition.m_hWnd)	m_dlgSheetObjectPosition.ShowWindow(SW_HIDE);	
			if (m_dlgMarkPlacement.m_hWnd)			m_dlgMarkPlacement.ShowWindow	   (SW_SHOW);
			break;
	}
	
	*pResult = 0;
}

void CDlgSheetObjectProperties::OnOK() 
{
	if (GetFocus() == GetDlgItem(IDOK))
		OnCancel();
	else
	{
		if (m_dlgSheetObjectPDFProps.IsWindowVisible())
			m_dlgSheetObjectPDFProps.Apply();
		if (m_dlgSheetObjectPosition.IsWindowVisible())
			m_dlgSheetObjectPosition.Apply();
	}
}

void CDlgSheetObjectProperties::OnCancel() 
{
	((CDlgLayoutObjectProperties*)GetParent())->OnCancel();
	DestroyWindow();
}
