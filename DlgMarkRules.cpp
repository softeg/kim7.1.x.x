// DlgMarkRules.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgMarkRules.h"
#include "DlgLayoutObjectControl.h"
#include "DlgEditMarkRule.h"
#include "ExpressionParser.h"
#include "DlgMarkSet.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"


// CDlgMarkRules-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgMarkRules, CDialog)

CDlgMarkRules::CDlgMarkRules(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgMarkRules::IDD, pParent)
{
	m_pParent = NULL;
	m_nMarkRulesRelation = 0;
}

CDlgMarkRules::~CDlgMarkRules()
{
}

void CDlgMarkRules::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MARKRULES_LIST, m_markRulesList);
	DDX_Control(pDX, IDC_MARKRULES_ACTION_COMBO, m_markRulesAction);
	DDX_Radio(pDX, IDC_MARKRULE_RELATION_AND, m_nMarkRulesRelation);
	DDX_Text(pDX, IDC_MARKRULES_PDFPATH, m_strPDFDocFullpath);
}


BEGIN_MESSAGE_MAP(CDlgMarkRules, CDialog)
	ON_BN_CLICKED(IDC_ADD_MARKRULE_BUTTON, OnBnClickedAddMarkruleButton)
	ON_BN_CLICKED(IDC_MODIFY_MARKRULE_BUTTON, OnBnClickedModifyMarkruleButton)
	ON_BN_CLICKED(IDC_REMOVE_MARKRULE_BUTTON, OnBnClickedRemoveMarkruleButton)
	ON_UPDATE_COMMAND_UI(IDC_MODIFY_MARKRULE_BUTTON, OnUpdateModifyMarkruleButton)
	ON_UPDATE_COMMAND_UI(IDC_REMOVE_MARKRULE_BUTTON, OnUpdateRemoveMarkruleButton)
	ON_UPDATE_COMMAND_UI(IDC_MARKRULES_ACTION_COMBO, OnUpdateMarkrulesActionCombo)
	ON_UPDATE_COMMAND_UI(IDC_MARKRULE_RELATION_AND, OnUpdateMarkruleRelationAnd)
	ON_UPDATE_COMMAND_UI(IDC_MARKRULE_RELATION_OR, OnUpdateMarkruleRelationOr)
	ON_LBN_DBLCLK(IDC_MARKRULES_LIST, OnLbnDblclkMarkrulesList)
	ON_CBN_SELCHANGE(IDC_MARKRULES_ACTION_COMBO, &CDlgMarkRules::OnCbnSelchangeMarkrulesActionCombo)
	ON_BN_CLICKED(IDC_MARKRULE_RELATION_AND, &CDlgMarkRules::OnBnClickedMarkruleRelationAnd)
	ON_BN_CLICKED(IDC_MARKRULE_RELATION_OR, &CDlgMarkRules::OnBnClickedMarkruleRelationOr)
	ON_BN_CLICKED(IDC_MARKRULES_BROWSE_PDFPATH, &CDlgMarkRules::OnBnClickedMarkrulesBrowsePdfpath)
END_MESSAGE_MAP()


// CDlgMarkRules-Meldungshandler

BOOL CDlgMarkRules::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls(this, TRUE);

	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CDlgMarkRules::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_markRulesAction.SetCurSel(0);

	GetDlgItem(IDC_MODIFY_MARKRULE_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_REMOVE_MARKRULE_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_MARKRULES_ACTION_COMBO)->EnableWindow((m_markRulesList.GetCount() > 0));

	GetDlgItem(IDC_MARKRULES_PDFPATH)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MARKRULES_BROWSE_PDFPATH)->ShowWindow(SW_HIDE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgMarkRules::LoadData()
{
	m_markRulesList.ResetContent();

	CMark* pMark = m_pParent->GetFirstMark();
	if ( ! pMark)
		return;

	if (pMark->m_nType == CMark::CustomMark)
		m_strPDFDocFullpath = ((CCustomMark*)pMark)->m_MarkSource.m_strContentFilesPath;
	else
		m_strPDFDocFullpath = _T("");

	CString strExpression = pMark->m_rules.GetRuleExpression(0);
	if ( ! strExpression.IsEmpty())
	{
		m_nMarkRulesRelation = 0;

		CString strAction;
		int		nCurPos	 = 0;
		CString strToken = strExpression.Tokenize(_T("\n"), nCurPos);
		while (strToken != _T(""))
		{
			strToken.TrimLeft(_T("(")); strToken.TrimRight(_T(")"));
			if (strToken.CompareNoCase(_T("and")) == 0)
				m_nMarkRulesRelation = 0;
			else
			if (strToken.CompareNoCase(_T("or")) == 0)
				m_nMarkRulesRelation = 1;
			else
			if (strToken.Find(_T("[Action]")) >= 0)
			{
				strAction = strToken;
				break;
			}
			m_markRulesList.AddString(strToken);
			strToken = strExpression.Tokenize(_T("\n"), nCurPos);
		}

		CExpressionParser parser;
		parser.ParseString(strAction);

		if (parser.m_strVariable == _T("Action"))
		{
			if (parser.m_strValue == _T("DoAddMark"))
				m_markRulesAction.SetCurSel(0);
			else
			if (parser.m_strValue == _T("DoNotAddMark"))
				m_markRulesAction.SetCurSel(1);
		}

		GetDlgItem(IDC_MARKRULES_ACTION_COMBO)->EnableWindow((m_markRulesList.GetCount() > 0));
	}

	UpdateData(FALSE);
}

void CDlgMarkRules::SaveData()
{
	UpdateData(TRUE);

	CMark* pMark = m_pParent->GetFirstMark();
	if ( ! pMark)
		return;

	CString strExpression;
	for (int i = 0; i < m_markRulesList.GetCount(); i++)
	{
		CString strLine;
		m_markRulesList.GetText(i, strLine);
	
		CString strOut;
		if ( (strLine != _T("and")) && (strLine != _T("or")) )
			strOut.Format(_T("(%s)\n"), strLine);
		else
			strOut.Format(_T("%s\n"), strLine);

		strExpression += strOut;
	}
	strExpression.TrimLeft(_T("\n "));
	strExpression.TrimRight(_T("\n "));

	if ( ! strExpression.IsEmpty())
	{
		int nSel = m_markRulesAction.GetCurSel();
		switch (nSel)
		{
		case 0:	strExpression += _T("\n[Action] = \"DoAddMark\"");		break;
		case 1:	strExpression += _T("\n[Action] = \"DoNotAddMark\"");		break;
		}
	}

	pMark->m_rules.SetRuleExpression(0, strExpression);
}

void CDlgMarkRules::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}

void CDlgMarkRules::OnBnClickedAddMarkruleButton()
{
	CDlgEditMarkRule dlg;

	if (dlg.DoModal() == IDCANCEL)
		return;

	CString strExpression;
	if (dlg.m_strVariable == _T("NoRule"))
		strExpression.Format(_T("[%s]"), dlg.m_strVariable);
	else
		strExpression.Format(_T("[%s] %s \"%s\""), dlg.m_strVariable, dlg.m_strOperator, dlg.m_strValue);
	if (m_markRulesList.GetCount() <= 0)
		m_markRulesList.AddString(strExpression);
	else
	{
		m_markRulesList.AddString((m_nMarkRulesRelation == 0) ? _T("and") : _T("or"));
		m_markRulesList.AddString(strExpression);
	}

	UpdateEnableState();
}

void CDlgMarkRules::OnBnClickedModifyMarkruleButton()
{
	int nSel = m_markRulesList.GetCurSel();
	if (nSel == LB_ERR)
		return;

	CDlgEditMarkRule dlg;

	CString strExpression;
	m_markRulesList.GetText(nSel, strExpression);

	CExpressionParser parser;
	parser.ParseString(strExpression);

	dlg.m_strVariable = parser.m_strVariable;
	dlg.m_strOperator = parser.m_strOperator;
	dlg.m_strValue	  = parser.m_strValue;

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_markRulesList.DeleteString(nSel);
	if (dlg.m_strVariable == _T("NoRule"))
		strExpression.Format(_T("[%s]"), dlg.m_strVariable);
	else
		strExpression.Format(_T("[%s] %s \"%s\""), dlg.m_strVariable, dlg.m_strOperator, dlg.m_strValue);
	m_markRulesList.InsertString(nSel, strExpression);

	UpdateEnableState();
}

void CDlgMarkRules::OnBnClickedRemoveMarkruleButton()
{
	int nSel = m_markRulesList.GetCurSel();
	if (nSel == LB_ERR)
		return;

	if (m_markRulesList.GetCount() == 1)
		m_markRulesList.DeleteString(nSel);
	else
		if (nSel == 0)
		{
			m_markRulesList.DeleteString(nSel);
			m_markRulesList.DeleteString(nSel);
		}
		else
		{
			m_markRulesList.DeleteString(nSel-1);
			m_markRulesList.DeleteString(nSel-1);
		}

	UpdateEnableState();
}

void CDlgMarkRules::OnUpdateModifyMarkruleButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CheckSelection());
}

void CDlgMarkRules::OnUpdateRemoveMarkruleButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CheckSelection());
}

void CDlgMarkRules::OnUpdateMarkrulesActionCombo(CCmdUI* pCmdUI)
{
	pCmdUI->Enable((m_markRulesList.GetCount() > 0));
}

void CDlgMarkRules::OnUpdateMarkruleRelationAnd(CCmdUI* pCmdUI)
{
	pCmdUI->Enable((m_markRulesList.GetCount() > 1));
}

void CDlgMarkRules::OnUpdateMarkruleRelationOr(CCmdUI* pCmdUI)
{
	pCmdUI->Enable((m_markRulesList.GetCount() > 1));
}

BOOL CDlgMarkRules::CheckSelection()
{
	UpdateData(TRUE);

	int nSel = m_markRulesList.GetCurSel();
	if (nSel == LB_ERR)
		return FALSE;

	CString strExpression;
	m_markRulesList.GetText(nSel, strExpression);

	if ( (strExpression == _T("and")) || (strExpression == _T("or")) )
		return FALSE;
	else
		return TRUE;
}

void CDlgMarkRules::OnLbnDblclkMarkrulesList()
{
	OnBnClickedModifyMarkruleButton();
}

void CDlgMarkRules::OnCbnSelchangeMarkrulesActionCombo()
{
	int nSel = m_markRulesAction.GetCurSel();
	switch (nSel)
	{
	case 0:
	case 1:	GetDlgItem(IDC_MARKRULES_PDFPATH)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_MARKRULES_BROWSE_PDFPATH)->ShowWindow(SW_HIDE);
			break;
	case 2:	GetDlgItem(IDC_MARKRULES_PDFPATH)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_MARKRULES_BROWSE_PDFPATH)->ShowWindow(SW_SHOW);
			break;
	}

	UpdateEnableState();
}

void CDlgMarkRules::UpdateEnableState()
{
	SaveData();

	if ( ! m_pParent)
		return;

	CDlgMarkSet* pMarksetDlg = (CDlgMarkSet*)m_pParent->GetParent();
	if ( ! pMarksetDlg->IsKindOf(RUNTIME_CLASS(CDlgMarkSet)))
		return;
	pMarksetDlg->UpdateEnableStates();

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView), TRUE);

	//int nSel = pMarksetDlg->m_nCurMarkSel;
	//if (nSel == LB_ERR)
	//	return;
	//CMark* pMark = pMarksetDlg->m_markList[nSel];
	//if ( ! pMark)
	//	return;

	//CPrintSheet* pPrintSheet = pMarksetDlg->GetPrintSheet();
	//int			 nSide		 = pMarksetDlg->GetActSight(NULL);

	//LV_ITEM item;
	//item.mask		= LVIF_IMAGE;
	//item.iItem		= nSel; 
	//item.iSubItem	= 0;
	//pMarksetDlg->m_markListCtrl.GetItem(&item);
	//switch (pMark->m_nType)
	//{
	//case CMark::CustomMark:		item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 0 :  6;  break;
	//case CMark::TextMark:		item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 1 :  7;  break;
	//case CMark::SectionMark:	item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 2 :  8;  break;
	//case CMark::CropMark:		item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 3 :  9;  break;
	//case CMark::FoldMark:		item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 4 : 10;  break;
	//case CMark::BarcodeMark:	item.iImage	= (pMark->IsEnabled(pPrintSheet, nSide)) ? 5 : 11;  break;
	//}
	//pMarksetDlg->m_markListCtrl.SetItem(&item);
}

void CDlgMarkRules::OnBnClickedMarkruleRelationAnd()
{
	UpdateData(TRUE);

	CString strExpression;
	for (int i = 0; i < m_markRulesList.GetCount(); i++)
	{
		CString strLine;
		m_markRulesList.GetText(i, strLine);
		if (strLine == _T("or"))
		{
			m_markRulesList.DeleteString(i);
			m_markRulesList.InsertString(i, _T("and"));
		}
	}
	UpdateEnableState();

	SaveData();
}

void CDlgMarkRules::OnBnClickedMarkruleRelationOr()
{
	UpdateData(TRUE);

	CString strExpression;
	for (int i = 0; i < m_markRulesList.GetCount(); i++)
	{
		CString strLine;
		m_markRulesList.GetText(i, strLine);
		if (strLine == _T("and"))
		{
			m_markRulesList.DeleteString(i);
			m_markRulesList.InsertString(i, _T("or"));
		}
	}
	UpdateEnableState();

	SaveData();
}

void CDlgMarkRules::OnBnClickedMarkrulesBrowsePdfpath()
{
	CMark* pMark = m_pParent->GetFirstMark();
	if ( ! pMark)
		return;

	if (pMark->m_nType == CMark::CustomMark)
	{
		m_pParent->m_dlgCustomMarkContent.OnRegisterPDF();
		m_strPDFDocFullpath = ((CCustomMark*)pMark)->m_MarkSource.m_strContentFilesPath;
		UpdateData(FALSE);
	}
}
