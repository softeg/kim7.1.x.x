#pragma once

#ifdef _WIN32_WCE
#error "CMDIChildWnd wird f�r Windows CE nicht unterst�tzt."
#endif 

#include "TrueColorToolBar.h"
#include "PrintSheetNavigationView.h"
#include "ProdDataPrintView.h"
#include "ProdDataPrintToolsView.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "ProdDataGraphicOverView.h"
#include "ProdDataPrepressToolsView.h"
#include "PrintSheetViewStrippingData.h"
#include "PrintComponentsDetailView.h"
#include "PrintSheetPlanningView.h"
#include "DlgTrimming.h"
#include "DlgShinglingAssistent.h"
#include "DlgComponentImposer.h"
#include "ProductsView.h"



class CPrintSheetFrameSplitter : public CSplitterWnd
{
    DECLARE_DYNAMIC(CPrintSheetFrameSplitter)
public:
	CPrintSheetFrameSplitter() { }



protected:
	virtual void DrawAllSplitBars(CDC* pDC, int cxInside, int cyInside);
	virtual void OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect, int nRow, int nCol);
	virtual void StopTracking(BOOL bAccept);

    DECLARE_MESSAGE_MAP()
    afx_msg LRESULT OnNcHitTest(CPoint point);
};



// CNewPrintSheetFrame-Rahmen mit Aufteilung

class CNewPrintSheetFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CNewPrintSheetFrame)
protected:
	CNewPrintSheetFrame();           // Dynamische Erstellung verwendet gesch�tzten Konstruktor
	virtual ~CNewPrintSheetFrame();


public:
	enum {ShowPrintSheetList = 1, ShowProducts = 2, ShowBookBlock = 4};


// Attribute
public:
	int							m_nModalResult;
	BOOL						m_bStatusRestored;
	CPrintSheetFrameSplitter	m_wndSplitter, m_wndSplitter1, m_wndSplitter2, m_wndSplitter3, m_wndSplitter4, m_wndSplitter5;
	CReBar						m_reBar;
	CTrueColorToolBar			m_toolBarDisplay, m_toolBarModify;
	BOOL						m_bShowPrintMenu;
	BOOL						m_bShowStrippingData;
	BOOL						m_bShowGripper;
	BOOL						m_bShowFoldSheet;
	BOOL						m_bShowMeasure;
	BOOL						m_bShowPlate;
	BOOL						m_bShowColorants;
	BOOL						m_bShowExposures;
	BOOL						m_bShowBitmap;
	BOOL						m_bShowCutBlocks;
	BOOL						m_bShowBoxShapes;
	BOOL						m_bLayoutObjectSelected;
	BOOL						m_bFoldSheetSelected;
	BOOL						m_bMarksViewActive;
	BOOL						m_bTrimToolActive;
	BOOL						m_bFoldSheetRotateActive;
	BOOL						m_bFoldSheetTurnActive;
	BOOL						m_bFoldSheetTumbleActive;
	BOOL						m_bUndoActive;
	BOOL						m_bEnableReleaseBleedButton;
	BOOL						m_bHandActive;
	BOOL						m_bShowImpositionAssistent;
	BOOL						m_bShowOutputSettingsView;
	BOOL						m_bShowOutputMaskSettings;
	BOOL						m_bShowOutputColorSettings;
	BOOL						m_bAlignSectionMenuActive;
	BOOL						m_bAlignSectionActive;
	int							m_nViewMode, m_nViewSide;
	CDlgPDFOutputMediaSettings* m_pDlgOutputMedia;
	CDlgLayoutObjectProperties*	m_pDlgLayoutObjectProperties;
	CDlgMarkSet*				m_pDlgMarkSet;
	CDlgMoveObjects*			m_pDlgMoveObjects;
	CDlgTrimming				m_dlgTrimming;
	CDlgShinglingAssistent		m_dlgShinglingAssistent;
	CDlgSelectFoldSchemePopup	m_dlgFoldSchemeSelector;
	struct s_browserInfo
	{
		int	m_nShowInfo;
		int m_nProductsViewHeight;
		int m_nListViewHeight;
		int m_nObjectPreviewWidth;
		int m_nComponentsViewWidth;
		int	m_nNavigationViewWidth;
	}m_browserInfo;


// Operationen
public:
	void								RearrangeSplitters();
	void								SetupViewProducts();
	void								SetupViewPrintSheetList(BOOL bRearrange = TRUE);
	BOOL								SetupViewSingleSheet(BOOL bResetAnyway = FALSE);
	BOOL								SetupViewAllSheets(BOOL bResetAnyway = FALSE);
	BOOL								SetupViewMarksSingleSheet();
	BOOL								SetupViewMarksAllSheets();
	BOOL								SetupViewPrepressSingleSheet();
	BOOL								SetupViewPrepressAllSheets();
	BOOL								SetupViewOutputSingleSheet();
	BOOL								SetupViewOutputAllSheets();
	void								UpdateFrameLayout(BOOL bChangeFrameLayout);
	void								InitPrintToolBar();
	void								AssignOutputProfile(const CString& strPDFTargetName);
	CPrintSheetView*					GetPrintSheetView();
	CPrintComponentsView*				GetPrintComponentsView();
	CPrintSheetNavigationView*			GetPrintSheetNavigationView();
	CPrintSheetPlanningView*			GetPrintSheetPlanningView();
	CPrintSheetViewStrippingData*		GetPrintSheetViewStrippingData();
	CPrintComponentsDetailView*			GetPrintComponentsDetailView();
	CProdDataPrintView*					GetProdDataPrintView();
	CProdDataPrintToolsView*			GetProdDataPrintToolsView();
	CProdDataPrepressToolsView*			GetProdDataPrepressToolsView();
	CPrintSheetTableView*				GetPrintSheetTableView();
	CProductsView*						GetProductsView();
	static CPrintSheet*					GetActPrintSheet();
	static CLayout*						GetActLayout();
	static CPressDevice*				GetActPressDevice();
	int									GetActSight();
	CColorantList						GetActColorants();
	BOOL								IsOpenMarkSetDlg();
	BOOL								IsOpenDlgNewMark();
	BOOL								IsOpenDlgMaskSettings();
	BOOL								IsOpenDlgNewMarkGeometry();
	BOOL								IsOpenDlgLayoutObjectGeometry();
	BOOL								IsOpenDlgLayoutObjectProperties();
	BOOL								IsOpenDlgPDFObjectContent();
	BOOL								IsOpenDlgObjectContent();
	BOOL								IsOpenDlgFoldSchemeSelector();
	CDlgSelectFoldSchemePopup*			GetDlgFoldSchemeSelector();
	CDlgComponentImposer*				GetDlgComponentImposer();
	BOOL								IsOpenDlgComponentImposer();
	CDlgLayoutObjectProperties*			GetDlgNewMark();
	CDlgMarkSet*						GetMarkSetDlg();
	CDlgLayoutObjectGeometry*			GetDlgLayoutObjectGeometry();
	void								SelectPrintSheet(CPrintSheet* pPrintSheet);
	void								InitialUnboundAddNewPrintSheet();
	void								OpenImpositionAssistent();
	void								CloseImpositionAssistent();
	BOOL								PrintSheetViewIsEmpty();
	void								OnUpdateSectionMenu();	 
	void								SwapFoldSheets(int nComponentRefIndex1, CPrintSheet* pPrintSheet1, int nComponentRefIndex2, CPrintSheet* pPrintSheet2);						
	void								SwapFlatProducts(CObArray& rObjectPtrList1, CPrintSheet* pPrintSheet1, CObArray& rObjectPtrList2, CPrintSheet* pPrintSheet2);						


	friend class CPrintSheetNavigationView;		// access to OnViewAllsides()

	afx_msg void OnViewAllsides();
	afx_msg void OnViewFrontside();
	afx_msg void OnViewBackside();
	afx_msg void OnViewBothsides();

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);


	DECLARE_MESSAGE_MAP()
	afx_msg void OnFilePageSetup();
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrintPreview(CCmdUI* pCmdUI);
	afx_msg void OnZoom();
	afx_msg void OnUpdateZoom(CCmdUI* pCmdUI);
	afx_msg void OnZoomDecrease();
	afx_msg void OnZoomFullview();
	afx_msg void OnScrollhand();
	afx_msg void OnDefineTrim();
	afx_msg void OnMoveObjects();
	afx_msg void OnSwapObjects();
	afx_msg void OnChangeFormats();
	afx_msg void OnRenameLayoutObject();
	afx_msg void OnObjectProperties();
	afx_msg void OnObjectsrotateClock();
	afx_msg void OnObjectsrotateCounterclock();
	afx_msg void OnObjectsTurn();
	afx_msg void OnObjectsTumble();
	afx_msg void OnDeleteSelection();
	afx_msg void OnLockUnlockFoldSheet();
	afx_msg void OnUndo();
	afx_msg void OnReleaseBleed();
	afx_msg void OnAlignLeft();
	afx_msg void OnAlignHCenter();
	afx_msg void OnAlignRight();
	afx_msg void OnAlignTop();
	afx_msg void OnAlignVCenter();
	afx_msg void OnAlignBottom();
	afx_msg void OnCenterBoth();
	afx_msg void OnUpdateUndo(CCmdUI* pCmdUI);
	afx_msg void OnSelectPrintSheetViewStrippingData();
	afx_msg void OnDestroy();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnFoldsheetCheck();
	afx_msg void OnUpdateFoldsheetcheck(CCmdUI* pCmdUI);
	afx_msg void OnMeasureCheck();
	afx_msg void OnUpdateMeasurecheck(CCmdUI* pCmdUI);
	afx_msg void OnExposuresCheck();
	afx_msg void OnUpdateExposurescheck(CCmdUI* pCmdUI);
	afx_msg void OnBitmapCheck();
	afx_msg void OnUpdateBitmapcheck(CCmdUI* pCmdUI);
	afx_msg void OnShowCutBlocks();
	afx_msg void OnUpdateCutBlockscheck(CCmdUI* pCmdUI);
	afx_msg void OnShowBoxshapes();
	afx_msg void OnPlatecheck();
	afx_msg void OnUpdatePlatecheck(CCmdUI* pCmdUI);
	afx_msg void OnClose();
	afx_msg void OnProcessPrePress();
	afx_msg void OnDoImpose();
	afx_msg void OnDoImposeAuto();
	afx_msg void OnAddNewPrintSheet();
	afx_msg void OnRemovePrintSheet();
	afx_msg void OnModifyShingling();
	afx_msg void OnOutputMaskSettings();
	afx_msg void OnOutputColorSettings();
	afx_msg void OnAlignSection();
	afx_msg void OnUpdateAlignSection(CCmdUI* pCmdUI);
	afx_msg void OnResetSection();
	afx_msg void OnUpdateResetSection(CCmdUI* pCmdUI);
	afx_msg void OnNotes();
	afx_msg void OnDefineSheet();
	afx_msg void OnTurnTumblePrintSheet();
	afx_msg void OnUpdateTurnTumble(CCmdUI* pCmdUI);
};


