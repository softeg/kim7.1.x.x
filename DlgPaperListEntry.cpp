// DlgPaperListEntry.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgPaperList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPaperListEntry 


CDlgPaperListEntry::CDlgPaperListEntry(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaperListEntry::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPaperListEntry)
	m_strComment = _T("");
	m_nExtra = 0;
	m_strName = _T("");
	m_nNumSheets = 0;
	m_nQuantity = 0;
	m_nTotal = 0;
	m_strFormat = _T("");
	//}}AFX_DATA_INIT
}


void CDlgPaperListEntry::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPaperListEntry)
	DDX_Text(pDX, IDC_PAPERLIST_ENTRY_COMMENT, m_strComment);
	DDX_Text(pDX, IDC_PAPERLIST_ENTRY_EXTRA, m_nExtra);
	DDX_Text(pDX, IDC_PAPERLIST_ENTRY_NAME, m_strName);
	DDX_Text(pDX, IDC_PAPERLIST_ENTRY_NUMSHEETS, m_nNumSheets);
	DDX_Text(pDX, IDC_PAPERLIST_ENTRY_QUANTITY, m_nQuantity);
	DDX_Text(pDX, IDC_PAPERLIST_ENTRY_TOTAL, m_nTotal);
	DDX_Text(pDX, IDC_PAPERLIST_ENTRY_FORMAT, m_strFormat);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPaperListEntry, CDialog)
	//{{AFX_MSG_MAP(CDlgPaperListEntry)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPaperListEntry 

BOOL CDlgPaperListEntry::OnInitDialog() 
{
	CDialog::OnInitDialog();

	Position();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

BOOL CDlgPaperListEntry::Position()
{
	CWnd* pWnd = GetParent();
	if ( ! pWnd)
	{
		EndDialog(IDCANCEL);
		return FALSE;
	}
	CDlgPaperList* pDlg = (CDlgPaperList*)pWnd;
	if ( ! pDlg->IsKindOf(RUNTIME_CLASS(CDlgPaperList)))
	{
		EndDialog(IDCANCEL);
		return FALSE;
	}

	CListCtrl* pPaperListTable = &pDlg->m_paperListTable;

	POSITION pos = pPaperListTable->GetFirstSelectedItemPosition();
	int nItem = pPaperListTable->GetNextSelectedItem(pos);
	if (nItem < 0)
	{
		EndDialog(IDCANCEL);
		return FALSE;
	}

	pPaperListTable->Scroll(CSize(-10000, 0));

	CRect itemRect, listRect;
	pPaperListTable->GetItemRect(nItem, itemRect, LVIR_BOUNDS);
	pPaperListTable->MapWindowPoints(pDlg, itemRect);
	pDlg->ClientToScreen(itemRect);
	pPaperListTable->GetWindowRect(listRect);

	int nHeight = itemRect.Height() + 2;
	MoveWindow(listRect.left + 2, itemRect.top, listRect.Width() - 2, nHeight + 2);

	ScreenToClient(itemRect);
	int nLeft  = itemRect.left;
	int nWidth = pPaperListTable->GetColumnWidth(0);
	GetDlgItem(IDC_PAPERLIST_ENTRY_NAME		)->MoveWindow(nLeft, itemRect.top, nWidth, nHeight);
	nLeft += nWidth;
	nWidth = pPaperListTable->GetColumnWidth(1);
	GetDlgItem(IDC_PAPERLIST_ENTRY_FORMAT	)->MoveWindow(nLeft, itemRect.top, nWidth, nHeight);
	nLeft += nWidth;
	nWidth = pPaperListTable->GetColumnWidth(2);
	GetDlgItem(IDC_PAPERLIST_ENTRY_NUMSHEETS)->MoveWindow(nLeft, itemRect.top, nWidth, nHeight);
	nLeft += nWidth;
	nWidth = pPaperListTable->GetColumnWidth(3);
	GetDlgItem(IDC_PAPERLIST_ENTRY_QUANTITY	)->MoveWindow(nLeft, itemRect.top, nWidth, nHeight);
	nLeft += nWidth;
	nWidth = pPaperListTable->GetColumnWidth(4);
	GetDlgItem(IDC_PAPERLIST_ENTRY_EXTRA	)->MoveWindow(nLeft, itemRect.top, nWidth, nHeight);
	nLeft += nWidth;
	nWidth = pPaperListTable->GetColumnWidth(5);
	GetDlgItem(IDC_PAPERLIST_ENTRY_TOTAL	)->MoveWindow(nLeft, itemRect.top, nWidth, nHeight);
	nLeft += nWidth;
	nWidth = pPaperListTable->GetColumnWidth(6);
	GetDlgItem(IDC_PAPERLIST_ENTRY_COMMENT	)->MoveWindow(nLeft, itemRect.top, nWidth, nHeight);

	//m_strName	 = pDlg->m_paperList[nItem].m_strName;
	//m_strFormat	 = pDlg->m_paperList[nItem].m_strFormat;
	//m_nNumSheets = pDlg->m_paperList[nItem].m_nNumSheets;
	//m_nQuantity  = pDlg->m_paperList[nItem].m_nQuantity;
	//m_nExtra	 = pDlg->m_paperList[nItem].m_nExtra;
	//m_nTotal	 = pDlg->m_paperList[nItem].m_nTotal;
	//m_strComment = pDlg->m_paperList[nItem].m_strComment;

	UpdateData(FALSE);

	SetFocus();

	return TRUE;
}

void CDlgPaperListEntry::OnOK() 
{
	CWnd* pWnd = GetParent();
	if (pWnd)
	{
		CDlgPaperList* pDlg = (CDlgPaperList*)pWnd;
		if (pDlg->IsKindOf(RUNTIME_CLASS(CDlgPaperList)))
		{
			CListCtrl* pPaperListTable = &pDlg->m_paperListTable;
			POSITION   pos			   = pPaperListTable->GetFirstSelectedItemPosition();
			int		   nItem		   = pPaperListTable->GetNextSelectedItem(pos);
			if (nItem >= 0)
			{
				UpdateData(TRUE);

				//pDlg->m_paperList[nItem].m_strName	  = m_strName;
				//pDlg->m_paperList[nItem].m_strFormat  = m_strFormat;
				//pDlg->m_paperList[nItem].m_nNumSheets = m_nNumSheets;
				//pDlg->m_paperList[nItem].m_nQuantity  = m_nQuantity;
				//pDlg->m_paperList[nItem].m_nExtra	  = m_nExtra;
				//pDlg->m_paperList[nItem].m_nTotal	  = m_nTotal;
				//pDlg->m_paperList[nItem].m_strComment = m_strComment;

				//CString string;
				//LVITEM  lvitem;
				//lvitem.iItem	= nItem;
				//lvitem.iSubItem = 0;
				//lvitem.mask	    = LVIF_TEXT;
				//lvitem.pszText  = pDlg->m_paperList[nItem].m_strName.GetBuffer(MAX_TEXT);		
				//pDlg->m_paperListTable.SetItem(&lvitem); 

				//lvitem.iSubItem = 1;
				//lvitem.pszText  = pDlg->m_paperList[nItem].m_strFormat.GetBuffer(MAX_TEXT);
				//pDlg->m_paperListTable.SetItem(&lvitem);

				//string.Format(_T("%3d"), pDlg->m_paperList[nItem].m_nNumSheets);
				//lvitem.iSubItem = 2;
				//lvitem.pszText  = string.GetBuffer(MAX_TEXT);
				//pDlg->m_paperListTable.SetItem(&lvitem);

				//string.Format(_T("%8d"), pDlg->m_paperList[nItem].m_nQuantity);
				//lvitem.iSubItem = 3;
				//lvitem.pszText  = string.GetBuffer(MAX_TEXT);
				//pDlg->m_paperListTable.SetItem(&lvitem);

				//string.Format(_T("%8d"), pDlg->m_paperList[nItem].m_nExtra);
				//lvitem.iSubItem = 4;
				//lvitem.pszText  = string.GetBuffer(MAX_TEXT);
				//pDlg->m_paperListTable.SetItem(&lvitem);

				//pDlg->m_paperList[nItem].m_nTotal = pDlg->m_paperList[nItem].m_nNumSheets * (pDlg->m_paperList[nItem].m_nQuantity + pDlg->m_paperList[nItem].m_nExtra);
				//string.Format(_T("%8d"), pDlg->m_paperList[nItem].m_nTotal);
				//lvitem.iSubItem = 5;
				//lvitem.pszText  = string.GetBuffer(MAX_TEXT);
				//pDlg->m_paperListTable.SetItem(&lvitem);

				//lvitem.iSubItem = 6;
				//lvitem.pszText  = pDlg->m_paperList[nItem].m_strComment.GetBuffer(MAX_TEXT);
				//pDlg->m_paperListTable.SetItem(&lvitem);
			}

			pDlg->UpdateData(FALSE);
		}
	}

	DestroyWindow();	
}

void CDlgPaperListEntry::OnCancel() 
{
	DestroyWindow();	
}

BOOL CDlgPaperListEntry::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if ( (pMsg->wParam == VK_DOWN) || (pMsg->wParam == VK_UP) )
		{
			CWnd* pWnd = GetParent();
			if (pWnd)
			{
				CDlgPaperList* pDlg = (CDlgPaperList*)pWnd;
				if (pDlg->IsKindOf(RUNTIME_CLASS(CDlgPaperList)))
				{
					CListCtrl* pPaperListTable = &pDlg->m_paperListTable;
					POSITION   pos			   = pPaperListTable->GetFirstSelectedItemPosition();
					int		   nItem		   = pPaperListTable->GetNextSelectedItem(pos);
					if (nItem >= 0)
					{
						if (pMsg->wParam == VK_DOWN)
						{
							if (nItem < pPaperListTable->GetItemCount() - 1)
							{
								pPaperListTable->SetItemState(nItem,	 LVIS_SELECTED, 0);
								pPaperListTable->SetItemState(nItem + 1, LVIS_SELECTED, LVIS_SELECTED);
								Position();
							}
						}
						else
						{
							if (nItem > 0)
							{
								pPaperListTable->SetItemState(nItem,	 LVIS_SELECTED, 0);
								pPaperListTable->SetItemState(nItem - 1, LVIS_SELECTED, LVIS_SELECTED);
								Position();
							}
						}
					}
				}
			}
		}
	
	return CDialog::PreTranslateMessage(pMsg);
}
