// DlgAlignObjects.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgAlignObjects.h"
#include "GraphicComponent.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetNavigationView.h"
#include "NewPrintSheetFrame.h"


// CDlgAlignObjects-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgAlignObjects, CDialog)

CDlgAlignObjects::CDlgAlignObjects(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgAlignObjects::IDD, pParent)
{
	m_bkBrush.CreateSolidBrush(RGB(225,234,253));
	m_ptInitialPos = CPoint(0,0);
	m_fDistance	   = 0.0f;
}

CDlgAlignObjects::~CDlgAlignObjects()
{
	m_bkBrush.DeleteObject();
}

void CDlgAlignObjects::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Measure(pDX, IDC_ALIGN_DISTANCE, m_fDistance);
	DDX_Control(pDX, IDC_ALIGN_DISTANCE_SPIN, m_distanceSpin);
}


BEGIN_MESSAGE_MAP(CDlgAlignObjects, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()
	ON_COMMAND(ID_ALIGN_LEFT,							OnAlignLeft)
	ON_COMMAND(ID_ALIGN_HCENTER,						OnAlignHCenter)
	ON_COMMAND(ID_ALIGN_RIGHT,							OnAlignRight)
	ON_COMMAND(ID_ALIGN_TOP,							OnAlignTop)
	ON_COMMAND(ID_ALIGN_VCENTER,						OnAlignVCenter)
	ON_COMMAND(ID_ALIGN_BOTTOM,							OnAlignBottom)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_LEFT,					OnUpdateAlignLeft)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_HCENTER,				OnUpdateAlignHCenter)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_RIGHT,				OnUpdateAlignRight)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_TOP,					OnUpdateAlignTop)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_VCENTER,				OnUpdateAlignVCenter)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_BOTTOM,				OnUpdateAlignBottom)
	ON_UPDATE_COMMAND_UI(IDC_ALIGN_DISTANCE_STATIC,		OnUpdateAlignDistanceStatic)
	ON_UPDATE_COMMAND_UI(IDC_ALIGN_DISTANCE,			OnUpdateAlignDistance)
	ON_UPDATE_COMMAND_UI(IDC_ALIGN_DISTANCE_SPIN,		OnUpdateAlignDistanceSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_ALIGN_DISTANCE_SPIN,	OnDeltaposDistanceSpin)
END_MESSAGE_MAP()


BOOL CDlgAlignObjects::OnEraseBkgnd(CDC* pDC)
{
	CRect rcFrame;
	GetClientRect(rcFrame);
	pDC->FillSolidRect(rcFrame, RGB(225,234,253));
	return TRUE;

	//return CDialog::OnEraseBkgnd(pDC);
}

HBRUSH CDlgAlignObjects::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

    if(nCtlColor == CTLCOLOR_STATIC)
    {
		pDC->SetBkMode(TRANSPARENT);
		hbr = (HBRUSH)m_bkBrush.GetSafeHandle();
    }

	return hbr;
}


void CDlgAlignObjects::DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd)
{
	pDC->FillSolidRect(rcFrame, RGB(225,234,253));
}


#define ID_SHOW_UNPRINTED	   10000


// CDlgAlignObjects-Meldungshandler

BOOL CDlgAlignObjects::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (m_ptInitialPos != CPoint(0, 0))
		SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	m_toolBar.Create(this, this, NULL, IDB_ALIGN_OBJECTS_TOOLBAR, IDB_ALIGN_OBJECTS_TOOLBAR, CSize(28, 22), &DrawToolsBackground);

	CRect rcToolFrame, rcButton; 
	GetDlgItem(IDC_ALIGN_BUTTONS_FRAME)->GetWindowRect(rcToolFrame); ScreenToClient(rcToolFrame);
	rcButton = CRect(rcToolFrame.TopLeft(), CSize(28, 32));

	m_toolBar.AddButton(-1, IDS_ALIGN_LEFT,		ID_ALIGN_LEFT,		rcButton, 0);	rcButton.OffsetRect(rcButton.Width(), 0);	
	m_toolBar.AddButton(-1, IDS_ALIGN_XCENTER,	ID_ALIGN_HCENTER,	rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);	
	m_toolBar.AddButton(-1, IDS_ALIGN_RIGHT,	ID_ALIGN_RIGHT,		rcButton, 2);	//rcButton.OffsetRect(rcButton.Width(), 0);	

	rcButton = CRect(rcToolFrame.TopLeft(), CSize(28, 32)); rcButton.OffsetRect(0, 35);
	m_toolBar.AddButton(-1, IDS_ALIGN_BOTTOM,	ID_ALIGN_BOTTOM,	rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);	
	m_toolBar.AddButton(-1, IDS_ALIGN_YCENTER,	ID_ALIGN_VCENTER,	rcButton, 4);	rcButton.OffsetRect(rcButton.Width(), 0);	
	m_toolBar.AddButton(-1, IDS_ALIGN_TOP,		ID_ALIGN_TOP,		rcButton, 5);	

	m_distanceSpin.SetRange(0, 1);  

	InitData();

	return FALSE;//TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgAlignObjects::InitData(BOOL bReset)
{
	if (bReset)
	{
		m_reflinePosParams.m_nObjectReferenceLineInX = 255;
		m_reflinePosParams.m_nObjectReferenceLineInY = 255;
	}
	InitReflinePosParams();
}

BOOL CDlgAlignObjects::ObjectReferenceIsUndefined()
{
	if ( (m_reflinePosParams.m_nObjectReferenceLineInX == 255) && (m_reflinePosParams.m_nObjectReferenceLineInY == 255) ) 
		return TRUE;
	else
		return FALSE;
}

int CDlgAlignObjects::ObjectReferenceDirection()
{
	 if ( (m_reflinePosParams.m_nObjectReferenceLineInX == 255) && (m_reflinePosParams.m_nObjectReferenceLineInY != 255) )	
		 return HORIZONTAL;
	 else
		 if ( (m_reflinePosParams.m_nObjectReferenceLineInY == 255) && (m_reflinePosParams.m_nObjectReferenceLineInX != 255) ) 
			 return VERTICAL;
		 else
			 return 255;
}

int CDlgAlignObjects::ReferenceDirection()
{
	 if ( (m_reflinePosParams.m_nReferenceLineInX == 255) && (m_reflinePosParams.m_nReferenceLineInY != 255) )	
		 return HORIZONTAL;
	 else
		 if ( (m_reflinePosParams.m_nReferenceLineInY == 255) && (m_reflinePosParams.m_nReferenceLineInX != 255) ) 
			 return VERTICAL;
		 else
			 return 255;
}

void CDlgAlignObjects::InitReflinePosParams(int nGroupNum, BOOL bInit)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CDisplayItem* pDI		  = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	CLayoutSide*  pLayoutSide = (pDI) ? ((CLayoutObject*)pDI->m_pData)->GetLayoutSide() : NULL;
	if ( ! pLayoutSide)
		return;

	float fSheetXRefline = pLayoutSide->GetRefline(m_reflinePosParams.m_nReferenceItemInX, m_reflinePosParams.m_nReferenceLineInX, &m_reflinePosParams.m_hReferenceObjectInX);
	float fSheetYRefline = pLayoutSide->GetRefline(m_reflinePosParams.m_nReferenceItemInY, m_reflinePosParams.m_nReferenceLineInY, &m_reflinePosParams.m_hReferenceObjectInY);

	CPrintSheet* pPrintSheet = pView->GetActPrintSheet();

	float		  fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight;
	float		  fObjectXRefline = 0.0f;
	float		  fObjectYRefline = 0.0f;

	if (nGroupNum >= 1)
		GetGroupBox(nGroupNum, fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight, NULL);
	else
		GetAllBox(fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight, NULL);

	if (ObjectReferenceIsUndefined() && ! bInit)
	{
		GetDlgItem(IDC_ALIGN_DISTANCE)->SetWindowText(_T(""));
		UpdatePrintSheetView();
		return;
	}

	if ( (ObjectReferenceIsUndefined() || (ObjectReferenceDirection() != ReferenceDirection()) ) && bInit)	// initialize object reference automatically if not be done before by clicking 
	{
		if (m_reflinePosParams.m_nReferenceLineInX == 255)
		{
			if (fSheetYRefline > (fBoxBottom + fBoxHeight / 2.0f))
				m_reflinePosParams.m_nObjectReferenceLineInY = TOP;
			else
				m_reflinePosParams.m_nObjectReferenceLineInY = BOTTOM;
			m_reflinePosParams.m_nObjectReferenceLineInX = 255;
		}
		else
		{
			if (fSheetXRefline > (fBoxLeft + fBoxWidth / 2.0f))
				m_reflinePosParams.m_nObjectReferenceLineInX = RIGHT;
			else
				m_reflinePosParams.m_nObjectReferenceLineInX = LEFT;
			m_reflinePosParams.m_nObjectReferenceLineInY = 255;
		}
	}

	switch (m_reflinePosParams.m_nObjectReferenceLineInX)
	{
	case LEFT:		fObjectXRefline = fBoxLeft;										break;
	case RIGHT:		fObjectXRefline = fBoxRight;									break;
	case XCENTER:	fObjectXRefline = fBoxLeft	+ (fBoxRight - fBoxLeft) / 2.0f;	break;
	default:		fObjectXRefline = fBoxLeft	+ (fBoxRight - fBoxLeft) / 2.0f;	break;
	}

	switch (m_reflinePosParams.m_nObjectReferenceLineInY)
	{
	case BOTTOM:	fObjectYRefline = fBoxBottom;									break;
	case TOP:		fObjectYRefline = fBoxTop;										break;
	case YCENTER:	fObjectYRefline = fBoxBottom + (fBoxTop	- fBoxBottom) /2.0f;	break;
	default:		fObjectYRefline = fBoxBottom + (fBoxTop	- fBoxBottom) /2.0f;	break;
	}

	float fDistanceX = fObjectXRefline - ( (fSheetXRefline != FLT_MAX) ? fSheetXRefline : 0.0f );
	float fDistanceY = fObjectYRefline - ( (fSheetYRefline != FLT_MAX) ? fSheetYRefline : 0.0f );
	m_reflinePosParams.m_fDistanceX = fDistanceX;
	m_reflinePosParams.m_fDistanceY = fDistanceY;

	m_fDistance = fabs((GetXReference() != 255) ? fDistanceX : fDistanceY);		// always show pos. distance
	UpdateData(FALSE);

	UpdatePrintSheetView();
}

void CDlgAlignObjects::ChangeDistance()
{
	int nDirection = (GetXReference() != 255) ? HORIZONTAL : VERTICAL;

	float fNewDistance = 0.0f;
	if (nDirection == HORIZONTAL)
		fNewDistance = (m_reflinePosParams.m_fDistanceX < 0.0f) ? -m_fDistance : m_fDistance;
	if (nDirection == VERTICAL)
		fNewDistance = (m_reflinePosParams.m_fDistanceY < 0.0f) ? -m_fDistance : m_fDistance;

	MoveObjects(nDirection, fNewDistance);

	if (IsOpenDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);
}

void CDlgAlignObjects::MoveObjects(int nDirection, float fNewDistance)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	if ( ! pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, GroupTracker)))
		return;
	CPrintSheet* pPrintSheet = pView->GetActPrintSheet();
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return;

	pView->UndoSave();

	BOOL		  bMoved  = FALSE;
	CDisplayItem* pDI	  = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	while (pDI)
	{
		CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
		if (pObject)
		{
			CLayoutObject* pCounterPartObject = (theApp.settings.m_bFrontRearParallel) ? pObject->FindCounterpartObject() : NULL;
			
			if (nDirection & HORIZONTAL)
				pObject->Offset(fNewDistance - m_reflinePosParams.m_fDistanceX, 0.0f);
			if (nDirection & VERTICAL)
				pObject->Offset(0.0f, fNewDistance - m_reflinePosParams.m_fDistanceY);

			if (pCounterPartObject)
				pCounterPartObject->AlignToCounterPart(pObject);

			bMoved	= TRUE;
		}
		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}

	pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	while (pDI)
	{
		int			 nComponentRefIndex = (int)pDI->m_pData;
		CLayoutSide* pLayoutSide		= (pDI->m_AuxRect.Height() > 1) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;

		if (nDirection & HORIZONTAL)
			pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, fNewDistance - m_reflinePosParams.m_fDistanceX, 0.0f);		
		if (nDirection & VERTICAL)
			pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, 0.0f, fNewDistance - m_reflinePosParams.m_fDistanceY);		

		bMoved	= TRUE;
		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	}

	if (bMoved)
	{
		if (nDirection & HORIZONTAL)
			m_reflinePosParams.m_fDistanceX = fNewDistance;
		if (nDirection & VERTICAL)
			m_reflinePosParams.m_fDistanceY = fNewDistance;

		if (pLayout)
			pLayout->CheckForProductType();

		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();	// because panorama pages could be arised or destroyed
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
			pDoc->CDocument::SetModifiedFlag();	// use base class method to prevent display list from being invalidated
												// in order to keep foldsheet selected
		}
		if (pLayout)
			pLayout->AnalyzeMarks();

		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);

		CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
		if (pNavigationView)
			pNavigationView->UpdatePrintSheets(pLayout);
	}

	if (nDirection & HORIZONTAL)
		m_fDistance = fabs(m_reflinePosParams.m_fDistanceX);
	if (nDirection & VERTICAL)
		m_fDistance = fabs(m_reflinePosParams.m_fDistanceY);
	UpdateData(FALSE);
}

CLayoutObject* CDlgAlignObjects::FindSelectedGroupClosestNeighbour(int nSide, int nGroupNum, CPoint point)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));	
	if ( ! pView)
		return NULL;
	CDisplayItem* pDI		  = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	CLayoutSide*  pLayoutSide = (pDI) ? ((CLayoutObject*)pDI->m_pData)->GetLayoutSide() : NULL;
	if ( ! pLayoutSide)
		return NULL;

	pDI = pView->m_DisplayList.GetFirstGroupMember(nGroupNum);
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
			((CLayoutObject*)pDI->m_pData)->m_bHidden = TRUE;
		else
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
				pLayoutSide->SetFoldSheetLayerVisible((int)pDI->m_pData, FALSE);
		pDI = pView->m_DisplayList.GetNextGroupMember(pDI, nGroupNum);
	}

	float fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight;
	GetGroupBox(nGroupNum, fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight, NULL);

	CLayoutObject blockObject; 
	blockObject.CreateCutBlockLabel(fBoxLeft, fBoxBottom, fBoxWidth, fBoxHeight, -1, TOP, 0);
	
	pLayoutSide->m_ObjectList.AddTail(blockObject);
	pLayoutSide->Validate();
	pLayoutSide->Analyse();

	CLayoutObject* pOppositeObject = CPrintSheetView::FindOppositeObject(&pLayoutSide->m_ObjectList.GetTail(), nSide, point);

	pDI = pView->m_DisplayList.GetFirstGroupMember(nGroupNum);
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
			((CLayoutObject*)pDI->m_pData)->m_bHidden = FALSE;
		else
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
				pLayoutSide->SetFoldSheetLayerVisible((int)pDI->m_pData, TRUE);
		pDI = pView->m_DisplayList.GetNextGroupMember(pDI, nGroupNum);
	}

	POSITION pos = pLayoutSide->m_ObjectList.GetTailPosition();
	if (pos)
		pLayoutSide->m_ObjectList.RemoveAt(pos);

	pLayoutSide->Validate();
	pLayoutSide->Analyse();

	return pOppositeObject;
}

void CDlgAlignObjects::UpdatePrintSheetView(BOOL bUpdateAll)
{
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));	
	if ( ! pView)
		return;

	//CDisplayItem* pDI = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
	//if (pDI)
	//	pView->InvalidateRect(pDI->m_BoundingRect);
	//CDisplayItem* pDI = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	//while (pDI)
	//{
	//	CRect rcRect = pDI->m_BoundingRect;
	//	rcRect.InflateRect(5,5);
	//	pView->InvalidateRect(rcRect, FALSE);
	//	rcRect.DeflateRect(10,10);
	//	pView->ValidateRect(rcRect);
	//	pDI = pView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	//}
	pView->Invalidate();
	pView->UpdateWindow();
}

int CDlgAlignObjects::GetXReference()
{
	return m_reflinePosParams.m_nObjectReferenceLineInX;
}

int CDlgAlignObjects::GetYReference()
{
	return m_reflinePosParams.m_nObjectReferenceLineInY;
}

void CDlgAlignObjects::DoAlign(int nDirection)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	int nNumSelectedItems = pView->m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	nNumSelectedItems += pView->m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));

	if (nNumSelectedItems <= 0)
	{
		CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)pView->GetParentFrame();
		DoAlignLayout(nDirection);
	}
	else
		if (pView->m_DisplayList.GetNumGroups() == 1)
			DoAlignObjects(1, nDirection);
		else
			DoAlignGroups(nDirection);
}

void CDlgAlignObjects::DoAlignLayout(int nDirection)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CPrintSheet* pPrintSheet = pView->GetActPrintSheet();
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return;

	if (pLayout)
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			pView->UndoSave();

		pLayout->AlignLayoutObjects(nDirection, pPrintSheet);

		pLayout->m_FrontSide.Invalidate();
		pLayout->m_BackSide.Invalidate();
		pLayout->CheckForProductType();
		pLayout->AnalyzeMarks();

		if (IsOpenDlgComponentImposer())
			GetDlgComponentImposer()->InitData(TRUE);

		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
			pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);

		CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
		if (pNavigationView)
			pNavigationView->UpdatePrintSheets(pLayout);
	}
}

void CDlgAlignObjects::DoAlignObjects(int nGroupNum, int nDirection)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CPrintSheet* pPrintSheet = pView->GetActPrintSheet();
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return;

	pView->UndoSave();

	float fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight;

	if (pView->m_DisplayList.GetNumGroupMembers(nGroupNum) == 1)	// if only one object selected, align to paper
	{
		CLayoutSide* pLayoutSide = &pLayout->m_FrontSide;
		CDisplayItem* pDI = pView->m_DisplayList.GetFirstGroupMember(nGroupNum);
		if (pDI)
		{
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
				pLayoutSide = (pDI) ? ((CLayoutObject*)pDI->m_pData)->GetLayoutSide() : NULL;
			else
				if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
					pLayoutSide	= (pDI->m_AuxRect.Height() > 1) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;
		}
		fBoxLeft = pLayoutSide->m_fPaperLeft; fBoxBottom = pLayoutSide->m_fPaperBottom; fBoxRight = pLayoutSide->m_fPaperRight; fBoxTop = pLayoutSide->m_fPaperTop; 
		fBoxWidth = pLayoutSide->m_fPaperWidth; fBoxHeight = pLayoutSide->m_fPaperHeight;
	}
	else
		GetGroupBox(nGroupNum, fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight, pPrintSheet);
	
	float fBoxXCenter = fBoxLeft   + (fBoxRight - fBoxLeft)  /2.0f;
	float fBoxYCenter = fBoxBottom + (fBoxTop   - fBoxBottom)/2.0f;

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstGroupMember(nGroupNum);
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
		{
			CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
			float		   fObjectLeft, fObjectBottom, fObjectRight, fObjectTop, fObjectWidth, fObjectHeight;
			pObject->GetCurrentGrossBox(pPrintSheet, fObjectLeft, fObjectBottom, fObjectRight, fObjectTop); fObjectWidth = fObjectRight - fObjectLeft; fObjectHeight = fObjectTop - fObjectBottom;
			switch (nDirection)
			{
			case LEFT:		pObject->Offset(fBoxLeft	-  fObjectLeft,							0.0f);	break;
			case XCENTER:	pObject->Offset(fBoxXCenter - (fObjectLeft + fObjectWidth/2.0f),	0.0f);	break;
			case RIGHT:		pObject->Offset(fBoxRight	-  fObjectRight,						0.0f);	break;
			case BOTTOM:	pObject->Offset(0.0f, fBoxBottom  -  fObjectBottom);						break;
			case YCENTER:	pObject->Offset(0.0f, fBoxYCenter - (fObjectBottom + fObjectHeight/2.0f));	break;
			case TOP:		pObject->Offset(0.0f, fBoxTop	  -  fObjectTop);							break;
			}
		}
		else
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		{
			int			 nComponentRefIndex = (int)pDI->m_pData;
			CLayoutSide* pLayoutSide		= (pDI->m_AuxRect.Height() > 1) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;
			float		 fObjectLeft, fObjectBottom, fObjectRight, fObjectTop, fObjectWidth, fObjectHeight;
			pLayoutSide->CalcBoundingRectObjects(&fObjectLeft, &fObjectBottom, &fObjectRight, &fObjectTop, &fObjectWidth, &fObjectHeight, FALSE, nComponentRefIndex, FALSE, pPrintSheet);
			switch (nDirection)
			{
			case LEFT:		pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, fBoxLeft	  -  fObjectLeft,						0.0f);		break;
			case XCENTER:	pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, fBoxXCenter - (fObjectLeft + fObjectWidth/2.0f),	0.0f);		break;
			case RIGHT:		pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, fBoxRight	  -  fObjectRight,						0.0f);		break;
			case BOTTOM:	pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, 0.0f, fBoxBottom  -  fObjectBottom);							break;
			case YCENTER:	pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, 0.0f, fBoxYCenter - (fObjectBottom + fObjectHeight/2.0f));	break;
			case TOP:		pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, 0.0f, fBoxTop		-  fObjectTop);								break;
			}
		}

		pDI = pView->m_DisplayList.GetNextGroupMember(pDI, nGroupNum);
	}

	if (IsOpenDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);

	pView->Invalidate();
	pView->UpdateWindow();

	CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
		pNavigationView->UpdatePrintSheets(pLayout);
}

void CDlgAlignObjects::DoAlignGroups(int nDirection)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CPrintSheet* pPrintSheet = pView->GetActPrintSheet();
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return;

	pView->UndoSave();

	float fAllBoxLeft, fAllBoxBottom, fAllBoxRight, fAllBoxTop, fAllBoxWidth, fAllBoxHeight;
	GetAllBox(fAllBoxLeft, fAllBoxBottom, fAllBoxRight, fAllBoxTop, fAllBoxWidth, fAllBoxHeight, pPrintSheet);
	float fAllBoxXCenter = fAllBoxLeft   + (fAllBoxRight - fAllBoxLeft)  /2.0f;
	float fAllBoxYCenter = fAllBoxBottom + (fAllBoxTop   - fAllBoxBottom)/2.0f;

	for (int nGroupNum = 1; nGroupNum <= pView->m_DisplayList.GetNumGroups(); nGroupNum++)
	{
		float fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight;
		GetGroupBox(nGroupNum, fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight, pPrintSheet);
		float fBoxXCenter = fBoxLeft   + (fBoxRight - fBoxLeft)  /2.0f;
		float fBoxYCenter = fBoxBottom + (fBoxTop   - fBoxBottom)/2.0f;

		CDisplayItem* pDI = pView->m_DisplayList.GetFirstGroupMember(nGroupNum);
		while (pDI)
		{
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
			{
				CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
				switch (nDirection)
				{
				case LEFT:		pObject->Offset(fAllBoxLeft		- fBoxLeft,		0.0f);	break;
				case XCENTER:	pObject->Offset(fAllBoxXCenter	- fBoxXCenter,	0.0f);	break;
				case RIGHT:		pObject->Offset(fAllBoxRight	- fBoxRight,	0.0f);	break;
				case BOTTOM:	pObject->Offset(0.0f, fAllBoxBottom  - fBoxBottom);		break;
				case YCENTER:	pObject->Offset(0.0f, fAllBoxYCenter - fBoxYCenter);	break;
				case TOP:		pObject->Offset(0.0f, fAllBoxTop	 - fBoxTop);		break;
				}
			}
			else
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
			{
				int			 nComponentRefIndex = (int)pDI->m_pData;
				CLayoutSide* pLayoutSide		= (pDI->m_AuxRect.Height() > 1) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;
				switch (nDirection)
				{
				case LEFT:		pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, fAllBoxLeft		- fBoxLeft,		0.0f);	break;
				case XCENTER:	pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, fAllBoxXCenter	- fBoxXCenter,	0.0f);	break;
				case RIGHT:		pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, fAllBoxRight		- fBoxRight,	0.0f);	break;
				case BOTTOM:	pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, 0.0f, fAllBoxBottom  - fBoxBottom);		break;
				case YCENTER:	pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, 0.0f, fAllBoxYCenter - fBoxYCenter);		break;
				case TOP:		pLayoutSide->OffsetFoldSheetLayer(nComponentRefIndex, 0.0f, fAllBoxTop	   - fBoxTop);			break;
				}
			}

			pDI = pView->m_DisplayList.GetNextGroupMember(pDI, nGroupNum);
		}
	}

	if (IsOpenDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);

	pView->Invalidate();
	pView->UpdateWindow();

	CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
		pNavigationView->UpdatePrintSheets(pLayout);
}

void CDlgAlignObjects::GetGroupBox(int nGroupNum, float& fBoxLeft, float& fBoxBottom, float& fBoxRight, float& fBoxTop, float& fBoxWidth, float& fBoxHeight, CPrintSheet* pPrintSheet)
{
	fBoxLeft = FLT_MAX; fBoxBottom = FLT_MAX; fBoxRight = -FLT_MAX; fBoxTop = -FLT_MAX; fBoxWidth = 0.0f; fBoxHeight = 0.0f;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CLayout* pLayout = pView->GetActLayout();
	if ( ! pLayout)
		return;

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstGroupMember(nGroupNum);
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
		{
			CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
			float fLeft, fBottom, fRight, fTop;
			pLayoutObject->GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
			fBoxLeft	= min(fBoxLeft,		fLeft);
			fBoxBottom	= min(fBoxBottom,	fBottom);
			fBoxRight	= max(fBoxRight,	fRight);
			fBoxTop		= max(fBoxTop,		fTop);
		}
		pDI = pView->m_DisplayList.GetNextGroupMember(pDI, nGroupNum);
	}

	pDI = pView->m_DisplayList.GetFirstGroupMember(nGroupNum);
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		{
			int			 nComponentRefIndex = (int)pDI->m_pData;
			CLayoutSide* pLayoutSide		= (pDI->m_AuxRect.Height() > 1) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;
			float fFoldSheetLeft, fFoldSheetBottom, fFoldSheetRight, fFoldSheetTop, fFoldSheetWidth, fFoldSheetHeight;
			pLayoutSide->CalcBoundingRectObjects(&fFoldSheetLeft, &fFoldSheetBottom, &fFoldSheetRight, &fFoldSheetTop, &fFoldSheetWidth, &fFoldSheetHeight, FALSE, nComponentRefIndex, FALSE, pPrintSheet);
			fBoxLeft	= min(fBoxLeft,		fFoldSheetLeft);
			fBoxBottom	= min(fBoxBottom,	fFoldSheetBottom);
			fBoxRight	= max(fBoxRight,	fFoldSheetRight);
			fBoxTop		= max(fBoxTop,		fFoldSheetTop);
		}
		pDI = pView->m_DisplayList.GetNextGroupMember(pDI, nGroupNum);
	}

	fBoxWidth  = fBoxRight - fBoxLeft;
	fBoxHeight = fBoxTop   - fBoxBottom;
}

void CDlgAlignObjects::GetAllBox(float& fBoxLeft, float& fBoxBottom, float& fBoxRight, float& fBoxTop, float& fBoxWidth, float& fBoxHeight, CPrintSheet* pPrintSheet)
{
	fBoxLeft = FLT_MAX; fBoxBottom = FLT_MAX; fBoxRight = -FLT_MAX; fBoxTop = -FLT_MAX; fBoxWidth = 0.0f; fBoxHeight = 0.0f;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CLayout* pLayout = pView->GetActLayout();
	if ( ! pLayout)
		return;

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	while (pDI)
	{
		CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
		float fLeft, fBottom, fRight, fTop;
		pLayoutObject->GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
		fBoxLeft	= min(fBoxLeft,		fLeft);
		fBoxBottom	= min(fBoxBottom,	fBottom);
		fBoxRight	= max(fBoxRight,	fRight);
		fBoxTop		= max(fBoxTop,		fTop);

		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}

	pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	while (pDI)
	{
		int			 nComponentRefIndex = (int)pDI->m_pData;
		CLayoutSide* pLayoutSide		= (pDI->m_AuxRect.Height() > 1) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;
		float fFoldSheetLeft, fFoldSheetBottom, fFoldSheetRight, fFoldSheetTop, fFoldSheetWidth, fFoldSheetHeight;
		pLayoutSide->CalcBoundingRectObjects(&fFoldSheetLeft, &fFoldSheetBottom, &fFoldSheetRight, &fFoldSheetTop, &fFoldSheetWidth, &fFoldSheetHeight, FALSE, nComponentRefIndex, FALSE, pPrintSheet);
		fBoxLeft	= min(fBoxLeft,		fFoldSheetLeft);
		fBoxBottom	= min(fBoxBottom,	fFoldSheetBottom);
		fBoxRight	= max(fBoxRight,	fFoldSheetRight);
		fBoxTop		= max(fBoxTop,		fFoldSheetTop);

		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	}
}

void CDlgAlignObjects::OnAlignLeft()
{
	DoAlign(LEFT);
}

void CDlgAlignObjects::OnAlignHCenter()
{
	DoAlign(XCENTER);
}

void CDlgAlignObjects::OnAlignRight()
{
	DoAlign(RIGHT);
}

void CDlgAlignObjects::OnAlignTop()
{
	DoAlign(TOP);
}

void CDlgAlignObjects::OnAlignVCenter()
{
	DoAlign(YCENTER);
}

void CDlgAlignObjects::OnAlignBottom()
{
	DoAlign(BOTTOM);
}

void CDlgAlignObjects::OnUpdateAlignLeft(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CDlgAlignObjects::OnUpdateAlignHCenter(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CDlgAlignObjects::OnUpdateAlignRight(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CDlgAlignObjects::OnUpdateAlignTop(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CDlgAlignObjects::OnUpdateAlignVCenter(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CDlgAlignObjects::OnUpdateAlignBottom(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CDlgAlignObjects::OnUpdateAlignDistanceStatic(CCmdUI* pCmdUI)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	int nNumItems = pView->m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	nNumItems += pView->m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));

	pCmdUI->Enable((nNumItems > 0) ? TRUE : FALSE);
}

void CDlgAlignObjects::OnUpdateAlignDistance(CCmdUI* pCmdUI)
{
	if ( (m_reflinePosParams.m_nObjectReferenceLineInX == 255) && (m_reflinePosParams.m_nObjectReferenceLineInY == 255) )
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

void CDlgAlignObjects::OnUpdateAlignDistanceSpin(CCmdUI* pCmdUI)
{
	if ( (m_reflinePosParams.m_nObjectReferenceLineInX == 255) && (m_reflinePosParams.m_nObjectReferenceLineInY == 255) )
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

void CDlgAlignObjects::OnDeltaposDistanceSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_distanceSpin.GetBuddy(), pNMUpDown->iDelta);
}

BOOL CDlgAlignObjects::IsOpenDlgComponentImposer()
{
	CPrintSheetViewStrippingData* pView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	if ( ! pView)
		return FALSE;

	if (pView->m_layoutDataWindow.m_dlgComponentImposer.m_hWnd)
		return TRUE;
	else
		return FALSE;
}

CDlgComponentImposer* CDlgAlignObjects::GetDlgComponentImposer()
{
	CPrintSheetViewStrippingData* pView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	if ( ! pView)
		return NULL;

	if (pView->m_layoutDataWindow.m_dlgComponentImposer.m_hWnd)
		return &pView->m_layoutDataWindow.m_dlgComponentImposer;
	else
		return NULL;
}

BOOL CDlgAlignObjects::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_toolBar.m_pToolTip)            
		m_toolBar.m_pToolTip->RelayEvent(pMsg);	

	if(pMsg->message == WM_KEYDOWN)
    {
		if(pMsg->wParam == VK_RETURN)
		{
			UpdateData(TRUE);

			ChangeDistance();

			pMsg->wParam = VK_TAB;
		}
    }

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgAlignObjects::OnDestroy()
{
	CDialog::OnDestroy();

	m_toolBar.Destroy();
}

