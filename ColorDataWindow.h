#pragma once


// CColorDataWindow

class CColorDataWindow : public CStatic
{
	DECLARE_DYNAMIC(CColorDataWindow)

public:
	CColorDataWindow();
	virtual ~CColorDataWindow();


// Attributes
public:
	DECLARE_DISPLAY_LIST(CColorDataWindow, OnMultiSelect, YES, OnStatusChanged, YES)
	DECLARE_DISPLAY_ITEM(CColorDataWindow, PrintColor, OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, IsSelectable, TRUE)


public:
	CColorantList		m_colorantList;

// Operations
public:
	CProductionProfile& GetProductionProfile();
	CPrintSheet*		GetPrintSheet();
	CRect				DrawColorList(CDC* pDC, CRect rcFrame, int nSide);


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSchemeWindow)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL


// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnDestroy();
};


