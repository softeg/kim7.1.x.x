// GraphicList.cpp: Implementierung der Klasse CGraphicList.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "GraphicList.h"
#include "GraphicPath.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

// CGraphicItem ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CGraphicItem, CObject, VERSIONABLE_SCHEMA | 1)

CGraphicItem::CGraphicItem()
{
}

CGraphicItem::~CGraphicItem()
{
}

void CGraphicItem::Copy(CGraphicItem* pgItem)
{
}

void CGraphicItem::Draw(CDC* pDC, double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis)
{
}

CString	CGraphicItem::MakePDFStream(double fOutputMediaX, double fOutputMediaY, double fOutputMediaWidth, double fOutputMediaHeight, int nOutputMediaOrientation,
								    double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis)
{
	return _T("");
}


// CGraphicObject ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CGraphicObject, CObject, VERSIONABLE_SCHEMA | 1)

CGraphicObject::CGraphicObject()
{
	m_pgItem = NULL;
}

CGraphicObject::CGraphicObject(const CGraphicObject& gObject)
{
	*this = gObject;
}

CGraphicObject::CGraphicObject(const CString& strID, CGraphicItem* pgItem)
{
	m_strID	 = strID;
	m_pgItem = pgItem;
}

void CGraphicObject::Copy(CGraphicObject* pgObject) 
{
	m_strID	= pgObject->m_strID;

	if (pgObject->m_pgItem->IsKindOf(RUNTIME_CLASS(CGraphicPath)))
		m_pgItem = new CGraphicPath;
	else
		m_pgItem = new CGraphicItem;

	m_pgItem->Copy(pgObject->m_pgItem); 
};

CGraphicObject::~CGraphicObject()
{
}

const CGraphicObject& CGraphicObject::operator=(const CGraphicObject& gObject)
{
	m_strID	 = gObject.m_strID;
	if (gObject.m_pgItem->IsKindOf(RUNTIME_CLASS(CGraphicPath)))
		m_pgItem = new CGraphicPath;
	else
		m_pgItem = new CGraphicItem;

	m_pgItem->Copy((CGraphicPath*)gObject.m_pgItem); 
	
	return *this;
}

void CGraphicObject::Draw(CDC* pDC, double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis)
{
	if (m_pgItem)
	{
		CSize size(1, 1);
		pDC->DPtoLP(&size);
		size.cy /= CPrintSheetView::WorldToLP(1);
		fYPos += size.cy;		// correction as in DrawRectangleExt()

		m_pgItem->Draw(pDC, fXPos, fYPos, fWidth, fHeight, nOrientation, nMirrorAxis);
	}
}

CString	CGraphicObject::MakePDFStream(double fOutputMediaX, double fOutputMediaY, double fOutputMediaWidth, double fOutputMediaHeight, int nOutputMediaOrientation,
									  double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis)
{
	if (m_pgItem)
		return m_pgItem->MakePDFStream(fOutputMediaX, fOutputMediaY, fOutputMediaWidth, fOutputMediaHeight, nOutputMediaOrientation, fXPos, fYPos, fWidth, fHeight, nOrientation, nMirrorAxis);
	else
		return _T("");
}





// CGraphicList ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CGraphicList, CObject, VERSIONABLE_SCHEMA | 1)

CGraphicList::CGraphicList()
{

}

CGraphicList::~CGraphicList()
{

}

void CGraphicList::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CGraphicList));

	m_elements.Serialize(ar);
}

template <> void AFXAPI SerializeElements(CArchive& ar, CGraphicObject* pGraphicObject, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CGraphicObject));

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pGraphicObject->m_strID;
			ar << pGraphicObject->m_pgItem;
		}
		else
		{
			ar >> pGraphicObject->m_strID;
			ar >> pGraphicObject->m_pgItem;
		}

		pGraphicObject++;
	}
}

void CGraphicList::Destruct()
{
	for (int i = 0; i < m_elements.GetSize(); i++)
	{
		m_elements[i].m_strID.Empty();
		delete m_elements[i].m_pgItem;
	}
}

void CGraphicList::AddObject(const CString& strName, CGraphicItem* pgItem)
{
	CGraphicObject graphicObject(strName, pgItem);
	m_elements.Add(graphicObject);
}

void CGraphicList::CopyGraphicObject(CGraphicObject* pgObject)
{
	CGraphicObject graphicObject;
	graphicObject.Copy(pgObject);
	m_elements.Add(graphicObject);
}

CGraphicObject* CGraphicList::GetObject(const CString& strID)
{
	for (int i = 0; i < m_elements.GetSize(); i++)
	{
		if (m_elements[i].m_strID == strID)
			return &m_elements[i];
	}

	return NULL;
}
