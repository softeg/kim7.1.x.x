
// ImpManDocCLayoutList.cpp : implementation of CLayout 
//

#include "stdafx.h"

#include "Imposition Manager.h"
#include "ImpManDoc.h"
#include "InplaceEdit.h"	// needed by PrintSheetView.h
#include "InplaceButton.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "PrintSheetFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetMarksView.h"
#include "PrintSheetTreeView.h"
#include "DlgTrimming.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "PrintSheetPlanningView.h"
#include "DlgSheet.h"	
#include "PageListView.h"
#include "GraphicComponent.h"
#include "PrintSheetTableView.h"
#include "PrintSheetMaskSettingsView.h"
#include "ProdDataPrintView.h"
#include "PrintComponentsView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif





/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Implementation of CLayoutList

IMPLEMENT_SERIAL(CLayoutList, CObject, VERSIONABLE_SCHEMA | 1)

CLayoutList::CLayoutList()
{
}


void CLayoutList::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CLayoutList));

	CList <CLayout, CLayout&>::Serialize(ar);
	//m_PressDevices.Serialize(ar);
}


void CLayoutList::RemoveAt(POSITION pos)
{
	int		 nLayoutIndex = 0;
	POSITION layoutPos	  = GetHeadPosition();
	while (layoutPos != pos)
	{
		GetNext(layoutPos);
		nLayoutIndex++;
	}

	CList <CLayout, CLayout&>::RemoveAt(pos); 

	if ( ! CImpManDoc::GetDoc())
		return;

	// Decrement all layout indices above the one being removed 
	pos = CImpManDoc::GetDoc()->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = CImpManDoc::GetDoc()->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.m_nLayoutIndex > nLayoutIndex)
		{
			rPrintSheet.m_nLayoutIndex--;
			POSITION posLayout = FindIndex(rPrintSheet.m_nLayoutIndex);
			rPrintSheet.LinkLayout(NULL);
		}
	}
}


int CLayoutList::GetSimilarLayout(CLayout* pLayout, BOOL bSearchBefore)	// TODO: Improve this function - search for similar foldsheet (not complete layout)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = GetAt(pos);
		if (&rLayout != pLayout)
		{
			if (rLayout.IsSimilar(*pLayout))	  
				break;
		}
		else
		{
			if (bSearchBefore)	// only search for layouts before pLayout in list
				return -1;
		}
		GetNext(pos);
		nIndex++;
	}

	if (pos)
		return nIndex;
	else
		return -1;
}


void CLayoutList::RearrangeMarkTemplateIndices(int nIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = GetNext(pos);
		POSITION objectPos = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = rLayout.m_FrontSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::ControlMark)
				if (rObject.m_nMarkTemplateIndex >= nIndex)
					rObject.m_nMarkTemplateIndex--;
		}

		objectPos = rLayout.m_BackSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = rLayout.m_BackSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::ControlMark)
				if (rObject.m_nMarkTemplateIndex >= nIndex)
					rObject.m_nMarkTemplateIndex--;
		}
	}
}

CLayout* CLayoutList::FindLayout(const CString& strLayoutName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = GetNext(pos);
		if (rLayout.m_strLayoutName == strLayoutName)
			return &rLayout;
	}
	return NULL;
}

void CLayoutList::AnalyzeMarks()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = GetNext(pos);
		rLayout.AnalyzeMarks(FALSE);
	}

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pPrintSheetView)
	{
		//// remove all mark items in display list, because m_pData pointers are no longer valid
		//// this is not perfect, because mark objects will be not clickable up to the next PrintSheetView update
		//// so this needs to be solved in future
		for (int i = 0; i < pPrintSheetView->m_DisplayList.m_List.GetSize(); i++)
		{
			if (pPrintSheetView->m_DisplayList.m_List[i].m_strItemTypeID != DISPLAY_ITEM_TYPEID(CPrintSheetView, CLayoutObject))
				continue;
			CLayoutObject* pObject = (CLayoutObject*)pPrintSheetView->m_DisplayList.m_List[i].m_pData;
			if (pObject)
				if (pObject->m_nType == CLayoutObject::Page)
					continue;
			pPrintSheetView->m_DisplayList.m_List.RemoveAt(i);
			i--;
		}

		for (int i = 0; i < pPrintSheetView->m_DisplayList.m_ListOld.GetSize(); i++)
		{
			if (pPrintSheetView->m_DisplayList.m_ListOld[i].m_strItemTypeID != DISPLAY_ITEM_TYPEID(CPrintSheetView, CLayoutObject))
				continue;
			CLayoutObject* pObject = (CLayoutObject*)pPrintSheetView->m_DisplayList.m_ListOld[i].m_pData;
			if (pObject)
				if (pObject->m_nType == CLayoutObject::Page)
					continue;
			pPrintSheetView->m_DisplayList.m_ListOld.RemoveAt(i);
			i--;
		}
		//pPrintSheetView->m_DisplayList.Invalidate();	// clear, because layout object pointers has changed
		//pPrintSheetView->m_DisplayList.Clear();
	}

	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.CImpManApp::GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pProdDataPrintView)
		pProdDataPrintView->InitObjectList();
}

void CLayoutList::DecrementMarkListColorDefIndices(int nColorDefinitionIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = GetNext(pos);
		rLayout.DecrementMarkListColorDefIndices(nColorDefinitionIndex);
	}
}

BOOL CLayoutList::ColorInUseMarkList(CImpManDoc* pDoc, int nColorDefinitionIndex, BOOL bSeparationOnly, BOOL bCompositeOnly)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = GetNext(pos);
		if (rLayout.ColorInUseMarkList(pDoc, nColorDefinitionIndex, bSeparationOnly, bCompositeOnly))
			return TRUE;
	}

	return FALSE;
}

void CLayoutList::SetAllFoldSheetLayerVisible()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = GetNext(pos);
		for (int i = 0; i < rLayout.m_FoldSheetLayerRotation.GetSize(); i++)
		{
			rLayout.m_FrontSide.SetFoldSheetLayerVisible(i, TRUE);
			rLayout.m_BackSide.SetFoldSheetLayerVisible(i, TRUE);
		}
	}
}




/////////////////////////////////////////////////////////////////////////////
// CLayout

IMPLEMENT_SERIAL(CLayout, CObject, VERSIONABLE_SCHEMA | 8)

CLayout::CLayout()
{
	m_FrontSide.m_pLayout = this;
	m_BackSide.m_pLayout  = this;

	m_bAskForChangeToWorkAndBack = TRUE;
	m_bAskForChangeToWorkAndTurnTumble = TRUE;
	m_nProductType = WORK_AND_BACK;
	m_nPrintingProfileRef = -1;
}

CLayout::CLayout(const CLayout& rLayout) 
{
	m_FrontSide.m_pLayout = this;
	m_BackSide.m_pLayout  = this;

	m_bAskForChangeToWorkAndBack = TRUE;
	m_bAskForChangeToWorkAndTurnTumble = TRUE;
	m_nProductType = WORK_AND_BACK;
	m_nPrintingProfileRef = -1;

	Copy(rLayout);
}

CLayout::~CLayout()
{
	DeleteContents();
}

// operator overload for CLayout - needed by CList functions:

const CLayout& CLayout::operator=(const CLayout& rLayout)
{
	Copy(rLayout);	

	return *this;
}

void CLayout::ReinitLinks()
{
	m_FrontSide.ReinitLinks(this);
	m_BackSide.ReinitLinks(this);
}

void CLayout::Copy(const CLayout& rLayout)
{
	m_strLayoutName						= rLayout.m_strLayoutName;
	m_strNotes							= rLayout.m_strNotes;
	m_strLastAssignedMarkset			= rLayout.m_strLastAssignedMarkset;
	m_bAskForChangeToWorkAndBack		= rLayout.m_bAskForChangeToWorkAndBack;
	m_bAskForChangeToWorkAndTurnTumble	= rLayout.m_bAskForChangeToWorkAndTurnTumble;
	m_nProductType						= rLayout.m_nProductType;
	m_nPrintingProfileRef				= rLayout.m_nPrintingProfileRef;

	m_FoldSheetLayerRotation.RemoveAll();
	m_FoldSheetLayerRotation.Copy(rLayout.m_FoldSheetLayerRotation);
	m_FoldSheetLayerTurned.RemoveAll();
	m_FoldSheetLayerTurned.Copy(rLayout.m_FoldSheetLayerTurned);

	const CLayoutSide& rFrontSide = rLayout.m_FrontSide;
	const CLayoutSide& rBackSide = rLayout.m_BackSide;
	m_FrontSide.Copy(rFrontSide);
	m_BackSide.Copy(rBackSide);

	m_markList.RemoveAll();
	m_markList.Copy(rLayout.m_markList);

	m_variants.RemoveAll();
	m_variants.Append(rLayout.m_variants);
}

void CLayout::Create(float fPaperLeft, float fPaperBottom, float fPaperRight, float fPaperTop, int nPrintingProfileIndex, CString strMarkset)
{
	m_nPrintingProfileRef = nPrintingProfileIndex;

	AssignDefaultName();
	m_strNotes = "";
	m_strLastAssignedMarkset = "";
	m_bAskForChangeToWorkAndBack = TRUE;
	m_bAskForChangeToWorkAndTurnTumble = TRUE;
	m_nProductType = WORK_AND_BACK;

	// Front side
	m_FrontSide.m_strSideName       = theApp.settings.m_szFrontName;
	m_FrontSide.m_nLayoutSide       = FRONTSIDE;
	m_FrontSide.m_nPaperRefEdge		= RIGHT_BOTTOM;
	m_FrontSide.m_nPressDeviceIndex = - 1;
	m_FrontSide.m_bAdjustFanout		= FALSE;
	m_FrontSide.m_FanoutList.RemoveAll();

	m_FrontSide.m_strFormatName		= _T("");
	m_FrontSide.m_fPaperLeft		= fPaperLeft;
	m_FrontSide.m_fPaperBottom		= fPaperBottom;
	m_FrontSide.m_fPaperRight		= fPaperRight;
	m_FrontSide.m_fPaperTop			= fPaperTop;
	m_FrontSide.m_fPaperWidth		= fPaperRight - fPaperLeft;
	m_FrontSide.m_fPaperHeight		= fPaperTop	  - fPaperBottom;
	m_FrontSide.m_nPaperGrain		= GRAIN_VERTICAL;

	// Back side
	m_BackSide.m_strSideName		= theApp.settings.m_szBackName;
	m_BackSide.m_nLayoutSide		= BACKSIDE;
	m_BackSide.m_nPaperRefEdge		= LEFT_BOTTOM;	// default is WorkAndTurn
	m_BackSide.m_nPressDeviceIndex	= -1;
	m_BackSide.m_bAdjustFanout		= FALSE;
	m_BackSide.m_FanoutList.RemoveAll();
	m_BackSide.m_strFormatName		= _T("");
	m_BackSide.m_fPaperLeft			= fPaperLeft;
	m_BackSide.m_fPaperBottom		= fPaperBottom;
	m_BackSide.m_fPaperRight		= fPaperRight;
	m_BackSide.m_fPaperTop			= fPaperTop;
	m_BackSide.m_fPaperWidth		= fPaperRight - fPaperLeft;
	m_BackSide.m_fPaperHeight		= fPaperTop	  - fPaperBottom;
	m_BackSide.m_nPaperGrain		= GRAIN_VERTICAL;

	if ( ! strMarkset.IsEmpty())
	{
		CString strMarksDir		= CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets\\");
		CString strRelativePath	= strMarkset;	
		strRelativePath.TrimLeft('.');		// trim .\ and .mks do get equal strings when imported from xml or from ProductionProfile
		strRelativePath.TrimLeft('\\');
		CString strMarksetFullPath = strMarksDir + strRelativePath;
		if (strRelativePath.Right(4).CompareNoCase(_T(".mks")) != 0)
			 strMarksetFullPath += _T(".mks");
		m_markList.RemoveAll();
		m_markList.Load(strMarksetFullPath);
	}
}

void CLayout::Create(CFoldSheet& rFoldSheet, int nLayerIndex, int nProductPartIndex)
{
	POSITION pos = rFoldSheet.m_LayerList.FindIndex(nLayerIndex);
	if (!pos)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);

	int nPressDeviceIndex = 0;

	CSheet sheet;
	sheet.m_fWidth  = m_FrontSide.m_fPaperWidth;	// in case of paper is already defined
	sheet.m_fHeight = m_FrontSide.m_fPaperHeight;

	float fPageWidth  = rProductPart.m_fPageWidth;
	float fPageHeight = rProductPart.m_fPageHeight;

	CFoldSheetLayer& rLayer = rFoldSheet.m_LayerList.GetAt(pos);

	AssignDefaultName();
	m_strNotes = "";
	m_strLastAssignedMarkset = "";
	m_bAskForChangeToWorkAndBack = TRUE;
	m_bAskForChangeToWorkAndTurnTumble = TRUE;
	m_nProductType = WORK_AND_BACK;

	//m_FoldSheetLayerRotation.SetAtGrow(0, 0);
	//m_FoldSheetLayerTurned.SetAtGrow(0, FALSE);

	// Front side
	m_FrontSide.m_strSideName       = theApp.settings.m_szFrontName;
	m_FrontSide.m_nLayoutSide       = FRONTSIDE;
	m_FrontSide.m_nPaperRefEdge		= rFoldSheet.m_nFoldSheetRefEdge;
	m_FrontSide.m_nPressDeviceIndex = nPressDeviceIndex;
	m_FrontSide.m_bAdjustFanout		= FALSE;
	m_FrontSide.m_FanoutList.RemoveAll();

	pDoc->SetPrintSheetPressDevice(nPressDeviceIndex, FRONTSIDE, this);
	CPressDevice* pPressDevice = m_FrontSide.GetPressDevice();
	if (pPressDevice)
		if ((sheet.m_fWidth == 0.0f) || (sheet.m_fHeight == 0.0f))
			pPressDevice->FindLargestPaper(sheet);

	m_FrontSide.m_strFormatName		= sheet.m_strSheetName;
	m_FrontSide.m_fPaperWidth		= sheet.m_fWidth;
	m_FrontSide.m_fPaperHeight		= sheet.m_fHeight;
	m_FrontSide.m_nPaperGrain		= sheet.m_nPaperGrain;

	BOOL bPageUp  = ((rLayer.m_HeadPositionArray[0] == TOP) || (rLayer.m_HeadPositionArray[0] == BOTTOM)) ? TRUE : FALSE;
	float fWidth  = (bPageUp) ? fPageWidth  : fPageHeight;
	float fHeight = (bPageUp) ? fPageHeight : fPageWidth;

	// Back side
	m_BackSide.m_strSideName       = theApp.settings.m_szBackName;
	m_BackSide.m_nLayoutSide       = BACKSIDE;
	switch (m_FrontSide.m_nPaperRefEdge)	// default is WorkAndTurn
	{
	case LEFT_BOTTOM:  m_BackSide.m_nPaperRefEdge = RIGHT_BOTTOM;	break;
	case RIGHT_BOTTOM: m_BackSide.m_nPaperRefEdge = LEFT_BOTTOM;	break;
	case LEFT_TOP:	   m_BackSide.m_nPaperRefEdge = RIGHT_TOP;		break;
	case RIGHT_TOP:    m_BackSide.m_nPaperRefEdge = LEFT_TOP;		break;
	}

	m_BackSide.m_strFormatName		= sheet.m_strSheetName;
	m_BackSide.m_fPaperWidth		= sheet.m_fWidth;
	m_BackSide.m_fPaperHeight		= sheet.m_fHeight;
	m_BackSide.m_nPaperGrain		= sheet.m_nPaperGrain;
	m_BackSide.m_nPressDeviceIndex = nPressDeviceIndex;
	m_BackSide.m_bAdjustFanout	   = FALSE;
	m_BackSide.m_FanoutList.RemoveAll();

	pDoc->SetPrintSheetPressDevice(nPressDeviceIndex, BACKSIDE, this);

	float fOffsetX = (pPressDevice) ? (pPressDevice->m_fPlateWidth - rLayer.m_nPagesX * fWidth)/2.0f : 0.0f;
	float fOffsetY;
	if (pPressDevice)
		if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
			fOffsetY = pPressDevice->m_fPlateEdgeToPrintEdge;
		else
			fOffsetY = pPressDevice->GetYCenterOfSection() - (rLayer.m_nPagesY * fHeight)/2.0f;

	AddFoldSheetLayer(&rFoldSheet, nLayerIndex, fOffsetX, fOffsetY, rProductPart, ROTATE0);

	m_FrontSide.PositionLayoutSide((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);
	m_BackSide.PositionLayoutSide((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);
}


// Create layout with foldsheet layer delivered by any other layout
void CLayout::Create(CLayout* pSourceLayout, int nSourceLayerRefIndex, int nProductPartIndex)	
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nDefaultWorkStyle = WORK_AND_BACK;

	int nPressDeviceIndex = -1;//(pProductGroup) ? theApp.m_PressDeviceList.GetDeviceIndex(pProductGroup->GetDefaultPressDeviceName()) : -1;
	if (nPressDeviceIndex == -1)
		nPressDeviceIndex = 0;

	CSheet sheet;
	//if (pProductGroup)
	//	if ( ! pProductGroup->GetDefaultPaperName().IsEmpty())
	//		CPressDevice::FindPaper(sheet, pProductGroup->GetDefaultPaperName());

	float fPageWidth  = pDoc->m_boundProducts.GetProductPart(nProductPartIndex).m_fPageWidth;
	float fPageHeight = pDoc->m_boundProducts.GetProductPart(nProductPartIndex).m_fPageHeight;

	AssignDefaultName();
	m_strNotes = "";
	m_strLastAssignedMarkset = "";
	m_bAskForChangeToWorkAndBack = TRUE;
	m_bAskForChangeToWorkAndTurnTumble = TRUE;
	m_nProductType = WORK_AND_BACK;

	m_FoldSheetLayerRotation.Add(pSourceLayout->m_FoldSheetLayerRotation[nSourceLayerRefIndex]);
	m_FoldSheetLayerTurned.Add(pSourceLayout->m_FoldSheetLayerTurned[nSourceLayerRefIndex]);

	// Front side
	m_FrontSide.m_strSideName       = theApp.settings.m_szFrontName;
	m_FrontSide.m_nLayoutSide       = FRONTSIDE;
	m_FrontSide.m_nPaperRefEdge		= RIGHT_BOTTOM;
	m_FrontSide.m_nPressDeviceIndex = nPressDeviceIndex;
	m_FrontSide.m_bAdjustFanout		= FALSE;
	m_FrontSide.m_FanoutList.RemoveAll();
	m_FrontSide.m_ObjectList.RemoveAll(); 

	pDoc->SetPrintSheetPressDevice(nPressDeviceIndex, FRONTSIDE, this);
	CPressDevice* pPressDevice = m_FrontSide.GetPressDevice();
	if (pPressDevice)
		if ((sheet.m_fWidth == 0.0f) || (sheet.m_fHeight == 0.0f))
			pPressDevice->FindLargestPaper(sheet);

	m_FrontSide.m_strFormatName		= sheet.m_strSheetName;
	m_FrontSide.m_fPaperWidth		= sheet.m_fWidth;
	m_FrontSide.m_fPaperHeight		= sheet.m_fHeight;
	m_FrontSide.m_nPaperGrain		= sheet.m_nPaperGrain;

	CObArray objectPtrList;
	float	 fSourceBBoxLeft, fSourceBBoxBottom, fSourceBBoxRight, fSourceBBoxTop;
	pSourceLayout->m_FrontSide.GetFoldSheetObjects(nSourceLayerRefIndex, &objectPtrList, &fSourceBBoxLeft,  &fSourceBBoxBottom, &fSourceBBoxRight, &fSourceBBoxTop);
	// TODO: Improve this offset calculations
	float fOffsetX = (pPressDevice) ? (pPressDevice->m_fPlateWidth - (fSourceBBoxRight - fSourceBBoxLeft))/2.0f : 0.0f;
	float fOffsetY;
	if (pPressDevice)
		if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
			fOffsetY = pPressDevice->m_fPlateEdgeToPrintEdge;
		else
			fOffsetY = pPressDevice->GetYCenterOfSection() - (fSourceBBoxTop - fSourceBBoxBottom)/2.0f;

	fOffsetX-= fSourceBBoxLeft; fOffsetY-= fSourceBBoxBottom;

	for (int i = 0; i < objectPtrList.GetSize(); i++)
	{
		// Insert before modify, because Offset function needs pointer to layout side for invalidation
		POSITION pos = m_FrontSide.m_ObjectList.AddTail(*((CLayoutObject*)objectPtrList[i]));	 
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetAt(pos); 
		rLayoutObject.Offset(fOffsetX, fOffsetY);
		rLayoutObject.m_nComponentRefIndex = 0;
	}

//	m_FrontSide.m_MarkTemplateList.RemoveAll();
	m_FrontSide.PositionLayoutSide((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);

	
	// Back side
	m_BackSide.m_strSideName       = theApp.settings.m_szBackName;
	m_BackSide.m_nLayoutSide       = BACKSIDE;
	m_BackSide.m_nPaperRefEdge	   = LEFT_BOTTOM;	// default is WorkAndTurn
	m_BackSide.m_nPressDeviceIndex = nPressDeviceIndex;
	m_BackSide.m_bAdjustFanout	   = FALSE;
	m_BackSide.m_FanoutList.RemoveAll();
	m_BackSide.m_ObjectList.RemoveAll(); // Be sure to work with an empty list
	m_BackSide.m_strFormatName		= sheet.m_strSheetName;
	m_BackSide.m_fPaperWidth		= sheet.m_fWidth;
	m_BackSide.m_fPaperHeight		= sheet.m_fHeight;
	m_BackSide.m_nPaperGrain		= sheet.m_nPaperGrain;
	pDoc->SetPrintSheetPressDevice(nPressDeviceIndex, BACKSIDE, this);

	pSourceLayout->m_BackSide.GetFoldSheetObjects(nSourceLayerRefIndex, &objectPtrList, &fSourceBBoxLeft,  &fSourceBBoxBottom, &fSourceBBoxRight, &fSourceBBoxTop);
	fOffsetX = (pPressDevice) ? (pPressDevice->m_fPlateWidth - (fSourceBBoxRight - fSourceBBoxLeft))/2.0f : 0.0f;
	if (pPressDevice)
		if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
			fOffsetY = pPressDevice->m_fPlateEdgeToPrintEdge;
		else
			fOffsetY = pPressDevice->GetYCenterOfSection() - (fSourceBBoxTop - fSourceBBoxBottom)/2.0f;

	fOffsetX-= fSourceBBoxLeft; fOffsetY-= fSourceBBoxBottom;

	for (int i = 0; i < objectPtrList.GetSize(); i++)
	{
		// Insert before modify, because Offset function needs pointer to layout side for invalidation
		POSITION pos = m_BackSide.m_ObjectList.AddTail(*((CLayoutObject*)objectPtrList[i]));	 
		CLayoutObject& rLayoutObject = m_BackSide.m_ObjectList.GetAt(pos); 
		rLayoutObject.Offset(fOffsetX, fOffsetY);
		rLayoutObject.m_nComponentRefIndex = 0;
	}

//	m_BackSide.m_MarkTemplateList.RemoveAll();
	m_BackSide.PositionLayoutSide((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);

	SetWorkStyle(nDefaultWorkStyle);
}

void CLayout::Create(CPrintingProfile& rPrintingProfile)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	m_nPrintingProfileRef = rPrintingProfile.GetIndex();

	AssignDefaultName();
	m_strNotes = "";
	m_strLastAssignedMarkset = "";
	m_bAskForChangeToWorkAndBack = TRUE;
	m_bAskForChangeToWorkAndTurnTumble = TRUE;
	m_nProductType = rPrintingProfile.m_press.m_nWorkStyle;

	CSheet& rSheet = rPrintingProfile.m_press.m_sheetData;

	// Front side
	m_FrontSide.m_strSideName       = theApp.settings.m_szFrontName;
	m_FrontSide.m_nLayoutSide       = FRONTSIDE;
	m_FrontSide.m_nPaperRefEdge		= RIGHT_BOTTOM;
	m_FrontSide.m_nPressDeviceIndex = (pDoc) ? pDoc->m_nDefaultPressDeviceIndex : - 1;
	m_FrontSide.m_bAdjustFanout		= FALSE;
	m_FrontSide.m_FanoutList.RemoveAll();

	m_FrontSide.m_strFormatName		= rSheet.m_strSheetName;
	m_FrontSide.m_fPaperWidth		= rSheet.m_fWidth;
	m_FrontSide.m_fPaperHeight		= rSheet.m_fHeight;
	m_FrontSide.m_nPaperGrain		= rSheet.m_nPaperGrain;
	m_FrontSide.PositionLayoutSide(rPrintingProfile.m_press.m_pressDevice.m_fGripper, &rPrintingProfile.m_press.m_pressDevice);

	// Back side
	m_BackSide.m_strSideName       = theApp.settings.m_szBackName;
	m_BackSide.m_nLayoutSide       = BACKSIDE;
	m_BackSide.m_nPaperRefEdge	   = LEFT_BOTTOM;	// default is WorkAndTurn
	m_BackSide.m_nPressDeviceIndex = (pDoc) ? pDoc->m_nDefaultPressDeviceIndex : -1;
	m_BackSide.m_bAdjustFanout	   = FALSE;
	m_BackSide.m_FanoutList.RemoveAll();
	m_BackSide.m_strFormatName		= rSheet.m_strSheetName;
	m_BackSide.m_fPaperWidth		= rSheet.m_fWidth;
	m_BackSide.m_fPaperHeight		= rSheet.m_fHeight;
	m_BackSide.m_nPaperGrain		= rSheet.m_nPaperGrain;
	m_BackSide.PositionLayoutSide(rPrintingProfile.m_press.m_backSidePressDevice.m_fGripper, &rPrintingProfile.m_press.m_backSidePressDevice);

	if ( ! rPrintingProfile.m_press.m_strMarkset.IsEmpty())
	{
		CString strMarksDir		= CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets\\");
		CString strRelativePath	= rPrintingProfile.m_press.m_strMarkset;	
		strRelativePath.TrimLeft('.');		// trim .\ and .mks do get equal strings when imported from xml or from ProductionProfile
		strRelativePath.TrimLeft('\\');
		CString strMarksetFullPath = strMarksDir + strRelativePath;
		if (strRelativePath.Right(4).CompareNoCase(_T(".mks")) != 0)
			 strMarksetFullPath += _T(".mks");
		m_markList.RemoveAll();
		m_markList.Load(strMarksetFullPath);
	}

	SetWorkStyle(rPrintingProfile.m_press.m_nWorkStyle);
}

int CLayout::GetIndex()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	int		 nIndex = -1;
	POSITION pos	= pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		nIndex++;
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if (&rLayout == this)
			return nIndex;
	}
	return -1;
}

CPrintingProfile& CLayout::GetPrintingProfile()
{
	static CPrintingProfile nullPrintingProfile;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return nullPrintingProfile;
	if (this == NULL)
		return nullPrintingProfile;

	int nLayoutIndex = GetIndex();
	if (nLayoutIndex < 0)
		nLayoutIndex = m_nPrintingProfileRef;
	return pDoc->m_printingProfiles.GetPrintingProfile(nLayoutIndex);
}

CPrintGroup& CLayout::GetPrintGroup()
{
	static CPrintGroup nullPrintGroup;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullPrintGroup;
	if (this == NULL)
		return nullPrintGroup;

	int nLayoutIndex = GetIndex();
	if (nLayoutIndex < 0)
		return nullPrintGroup;
	else
		return pDoc->m_printGroupList.GetPrintGroup(nLayoutIndex);
}

BOOL CLayout::AssignAutoMarks()
{
	HKEY	  hKey;
	DWORD	  dwDisposition; 
	const int nValueLenght = max(_MAX_PATH, MAX_TEXT) + 2;
	TCHAR	  szValue[nValueLenght];
	DWORD	  dwSize;
	CString	  strKey = theApp.m_strRegistryKey;
	RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition); 

	BOOL bSetAutoMarks = FALSE;
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("SetAutoMarks"), 0, NULL, (LPBYTE)szValue, &dwSize) != ERROR_SUCCESS)
		_tcsncpy(szValue, _T("FALSE"), 5);
	if (_tcscmp(szValue, _T("TRUE")) == 0)
		bSetAutoMarks = TRUE;

	CString strFolder;
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("AutoMarkFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		strFolder = szValue;
	else
		strFolder = _T("");

	BOOL bChanged = FALSE;

	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	while (pPrintSheet)
	{
		pPrintSheet->m_markStates.RemoveAll();
		pPrintSheet = GetNextPrintSheet(pPrintSheet);
	}

	for (int i = 0; i < m_markList.GetSize(); i++)	// remove auto marks 
	{
		BOOL bRemove = FALSE;
		if (m_markList[i]->IsAutoMark())	
		{
			if ( ! bSetAutoMarks)
				bRemove = TRUE;
			else
				if ( ! m_markList[i]->GlobalCheckRules(this, i))	// don't remove all marks in order to keep auto mark changes
					bRemove = TRUE;
			if (bRemove)
			{
				m_FrontSide.RemoveMarkObject(m_markList[i]->m_strMarkName);
				m_BackSide.RemoveMarkObject(m_markList[i]->m_strMarkName);
				m_markList.RemoveAt(i);
				bChanged = TRUE;
				i--;
			}
		}
		else
		{
			pPrintSheet = GetFirstPrintSheet();
			while (pPrintSheet)
			{
				pPrintSheet->m_markStates.InsertAt(i, BOTHSIDES);
				pPrintSheet = GetNextPrintSheet(pPrintSheet);
			}
		}
	}

	if ( ! bSetAutoMarks)
		return bChanged;

	WIN32_FIND_DATA findData;
	BOOL			bFilesExist = FALSE;	
	CString strMarksetFile;
	CString strFilename = strFolder + _T("\\*.*");
	HANDLE  hfileSearch = FindFirstFile(strFilename, &findData);

	CDirList dirList;
	if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		CString	strBuffer	= findData.cFileName;
		strMarksetFile		= strBuffer.SpanExcluding(_T("."));
		bFilesExist			= TRUE;
		do 
		{
			strBuffer = findData.cFileName;
			if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
			{
				if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					CDirItem dirItem(strBuffer, CDirItem::IsFolder);
					dirList.Add(dirItem);
				}
				else 
				{
					CString strExt = strBuffer.Right(4);
					strExt.MakeLower();
					if (strExt == _T(".mks"))
					{
						strBuffer = strBuffer.SpanExcluding(_T("."));
						CDirItem dirItem(strBuffer, CDirItem::IsMarkset);
						dirList.Add(dirItem);
					}
				}
			}
		}
		while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);

	dirList.Sort();

	if (CImpManDoc::GetDoc())
	{
		m_markList.m_colorDefTable.RemoveAll();
		m_markList.m_colorDefTable.Append(CImpManDoc::GetDoc()->m_ColorDefinitionTable);
	}

	for (int nDirIndex = 0; nDirIndex < dirList.GetSize(); nDirIndex++)
	{
		if (dirList[nDirIndex].m_nAttribute == CDirItem::IsFolder)
			continue;

		CMarkList autoMarkList;
		autoMarkList.Load(strFolder + _T("\\") + dirList[nDirIndex].m_strFileName + _T(".mks"));
		for (int i = 0; i < autoMarkList.GetSize(); i++)
		{
			//if (autoMarkList[i]->IsAutoMark())	// is really auto mark
			{
				if ( ! m_markList.FindMark(autoMarkList[i]->m_strMarkName))	// mark not yet present
					if (autoMarkList[i]->GlobalCheckRules(this, m_markList.GetSize()))
					{
						autoMarkList[i]->SetAutoMark(TRUE);
						autoMarkList[i]->m_markTemplate = autoMarkList[i]->TransformColorDefs(autoMarkList.m_colorDefTable, -1);
						CDlgMarkSet::PositionAndCreateMarkAuto(autoMarkList[i], &m_markList, _T(""), GetFirstPrintSheet());
						bChanged = TRUE;
					}
			}
		}
		autoMarkList.RemoveAll();
	}

	int nZOrderIndex = 0;
	for (int i = 0; i < m_markList.GetSize(); i++)
		m_markList[i]->m_nZOrderIndex = nZOrderIndex++;

	dirList.RemoveAll();

	return bChanged;
}

void CLayout::DeleteContents()
{
	m_FoldSheetLayerRotation.RemoveAll();
	m_FoldSheetLayerTurned.RemoveAll();
	m_markList.RemoveAll();
	m_FrontSide.m_ObjectList.RemoveAll();
	m_FrontSide.m_FanoutList.RemoveAll();
	m_FrontSide.m_DefaultPlates.RemoveAll();
	m_FrontSide.m_cutBlocks.RemoveAll();
	m_BackSide.m_ObjectList.RemoveAll();
	m_BackSide.m_FanoutList.RemoveAll();
	m_BackSide.m_DefaultPlates.RemoveAll();
	m_BackSide.m_cutBlocks.RemoveAll();
}

BOOL CLayout::IsEmpty()
{
	if (GetNumFoldSheets() > 0)
		return FALSE;
	if (GetNumFlatProducts() > 0)
		return FALSE;
	return TRUE;
}

BOOL CLayout::IsSimilar(CLayout& rLayout) 
{
	if (rLayout.IsEmpty())
		return FALSE;
	if (IsEmpty())
		return FALSE;
	if (m_FoldSheetLayerRotation.GetSize() != rLayout.m_FoldSheetLayerRotation.GetSize())
		return FALSE;
	if (m_FoldSheetLayerTurned.GetSize() != rLayout.m_FoldSheetLayerTurned.GetSize())
		return FALSE;
	for (int i = 0; i < m_FoldSheetLayerRotation.GetSize(); i++)
		if (m_FoldSheetLayerRotation[i] != rLayout.m_FoldSheetLayerRotation[i])
			return FALSE;
	for (int i = 0; i < m_FoldSheetLayerTurned.GetSize(); i++)
		if (m_FoldSheetLayerTurned[i] != rLayout.m_FoldSheetLayerTurned[i])
			return FALSE;

	if (!m_FrontSide.IsSimilar(rLayout.m_FrontSide))
		return FALSE;
	if (!m_BackSide.IsSimilar(rLayout.m_BackSide))
		return FALSE;

	return TRUE;
}


// Get first printsheet based on this layout
CPrintSheet* CLayout::GetFirstPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)		// Document not yet present - i.e. we are serializing (loading)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return NULL;	

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.GetLayout() == this)
			return &rPrintSheet;
	}
	return NULL;
}

// Get last printsheet based on this layout
CPrintSheet* CLayout::GetLastPrintSheet(int nLayerIndex, BOOL bIgnoreEmpty)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)		// Document not yet present - i.e. we are serializing (loading)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return NULL;	

	if (nLayerIndex >= 0)	// when multilayer foldsheets will be added, look for printsheet with first layer
	{
		POSITION pos = pDoc->m_PrintSheetList.GetTailPosition();
		while (pos)
		{
			CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetPrev(pos);
			if (rPrintSheet.IsEmpty())
				continue;
			if (rPrintSheet.GetLayout() == this)
				if (rPrintSheet.m_FoldSheetLayerRefs.GetSize())
					if (rPrintSheet.m_FoldSheetLayerRefs[0].m_nFoldSheetLayerIndex == nLayerIndex)
						return &rPrintSheet;
		}
	}

	POSITION pos = pDoc->m_PrintSheetList.GetTailPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetPrev(pos);
		if (rPrintSheet.IsEmpty())
			continue;
		if (rPrintSheet.GetLayout() == this)
			return &rPrintSheet;
	}
	return NULL;
}

CPrintSheet* CLayout::GetLastEmptyPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)		// Document not yet present - i.e. we are serializing (loading)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return NULL;	

	POSITION pos = pDoc->m_PrintSheetList.GetTailPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetPrev(pos);
		if (rPrintSheet.IsEmpty())
			if (rPrintSheet.GetLayout() == this)
				return &rPrintSheet;
	}

	return NULL;
}

// Get next printsheet based on this layout
CPrintSheet* CLayout::GetNextPrintSheet(CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)		// Document not yet present - i.e. we are serializing (loading)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return NULL;	

	BOOL	 bBegin = FALSE;
	POSITION pos	= pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (bBegin)
			if (rPrintSheet.GetLayout() == this)
				return &rPrintSheet;
		if (pPrintSheet == &rPrintSheet)
			bBegin = TRUE;
	}
	return NULL;
}

// Get previous printsheet based on this layout
CPrintSheet* CLayout::GetPrevPrintSheet(CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)		// Document not yet present - i.e. we are serializing (loading)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return NULL;	

	BOOL	 bBegin = FALSE;
	POSITION pos	= pDoc->m_PrintSheetList.GetTailPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetPrev(pos);
		if (bBegin)
			if (rPrintSheet.GetLayout() == this)
				return &rPrintSheet;
		if (pPrintSheet == &rPrintSheet)
			bBegin = TRUE;
	}
	return NULL;
}


void CLayout::AssignDefaultName()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	for (int i = 1; i <= pDoc->m_PrintSheetList.m_Layouts.GetCount(); i++) 
	{
		BOOL bOccupied = FALSE;
		POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
		while (pos)
		{
			CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
			if (rLayout.GetNumDefaultName() == i)
			{
				bOccupied = TRUE;
				break;
			}
		}
		if ( ! bOccupied)
		{
			m_strLayoutName.Format(_T("%s-%d"), theApp.settings.m_szLayoutName, i);
			return;
		}
	}

	m_strLayoutName.Format(_T("%s-%d"), theApp.settings.m_szLayoutName, pDoc->m_PrintSheetList.m_Layouts.GetCount() + 1);
}


int CLayout::GetNumDefaultName()
{
	CString  string = CString(theApp.settings.m_szLayoutName) + "-";
	const TCHAR* poi = _tcsstr((LPCTSTR)m_strLayoutName, (LPCTSTR)string);
	if (poi)
	{
		poi = _tcschr(poi, '-');
		poi++;
		if (isdigit(*poi))	
			return _ttoi(poi);
	}

	return -1;
}


void CLayout::IncreaseNumLayoutName()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	BOOL	 bFound  = FALSE;
	int		 nMaxNum = 0;
	CString  string  = m_strLayoutName + _T("-");
	POSITION pos	 = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if (&rLayout == this)
			continue;

		if (rLayout.m_strLayoutName.Find(m_strLayoutName) >= 0)
		{
			bFound = TRUE;;
			const TCHAR* poi = _tcsstr((LPCTSTR)rLayout.m_strLayoutName, (LPCTSTR)string);
			if (poi)
			{
				poi = _tcschr(poi, '-');
				poi++;
				if (isdigit(*poi))	
					max(nMaxNum, _ttoi(poi));
			}
		}
	}
	if (bFound)
	{
		string.Format(_T("%s-%d"), m_strLayoutName, nMaxNum + 1);
		m_strLayoutName = string;
	}
}


int CLayout::GetNumLayoutName()
{
	int nIndex = m_strLayoutName.ReverseFind('-');
	if ( (nIndex < 0) || (nIndex >= m_strLayoutName.GetLength() - 1) )
		return -1;

	const TCHAR* poi = m_strLayoutName.GetBuffer();
	poi += nIndex + 1;
	while (poi)
	{
		if ( ! isdigit(*poi))
			return -1;
		poi++;
	}

	poi = m_strLayoutName.GetBuffer();
	poi += nIndex + 1;
	return atoi(poi);
}


int CLayout::KindOfProduction()
{
	switch (m_FrontSide.m_nPaperRefEdge)
	{
		case LEFT_BOTTOM : if (m_BackSide.m_nPaperRefEdge == RIGHT_BOTTOM)
							   return WORK_AND_TURN;
						   else
							   return WORK_AND_TUMBLE;
						   break;
		case RIGHT_BOTTOM: if (m_BackSide.m_nPaperRefEdge == LEFT_BOTTOM)
							   return WORK_AND_TURN;
						   else
							   return WORK_AND_TUMBLE;
						   break;
		case LEFT_TOP	 : if (m_BackSide.m_nPaperRefEdge == RIGHT_TOP)
							   return WORK_AND_TURN;
						   else
							   return WORK_AND_TUMBLE;
						   break;
		case RIGHT_TOP	 : if (m_BackSide.m_nPaperRefEdge == LEFT_TOP)
							   return WORK_AND_TURN;
						   else
							   return WORK_AND_TUMBLE;
						   break;

		default : return 0;
	}
}


void CLayout::RotateFoldSheet90(int nComponentRefIndex, BOOL bClockwise, CPrintSheet* pDefaultPrintSheet, int nSide, BOOL bInnerForm)
{
	CObArray objectPtrList;
	float	 fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop;

	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	if (nSide == BACKSIDE)
		m_BackSide.GetFoldSheetObjects(nComponentRefIndex, &objectPtrList, &fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, FALSE, pPrintSheet);
	else
		m_FrontSide.GetFoldSheetObjects(nComponentRefIndex, &objectPtrList, &fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, FALSE, pPrintSheet);

	// Rotate foldsheet by 90 degree on front side
	for (int i = 0; i < objectPtrList.GetSize(); i++)
	{
		CLayoutObject* pObj = (CLayoutObject*)objectPtrList[i];
		CLayoutObject* pCounterPartObj = pObj->FindCounterpartObject();
		float		   fTemp;

		if ( ! bClockwise)
		{
			pObj->SetBottom(fBBoxBottom + pObj->GetLeft() - fBBoxLeft);
			pObj->SetLeft(	fBBoxLeft	+ fBBoxTop		  - pObj->GetTop());
			fTemp = pObj->GetWidth(); pObj->SetWidth(pObj->GetHeight()); pObj->SetHeight(fTemp);
			pObj->SetRight(pObj->GetLeft()	 + pObj->GetWidth());
			pObj->SetTop  (pObj->GetBottom() + pObj->GetHeight());

			switch (pObj->m_nHeadPosition)
			{
			case LEFT:		pObj->m_nHeadPosition = BOTTOM; break;
			case RIGHT:		pObj->m_nHeadPosition = TOP;	break;
			case BOTTOM:	pObj->m_nHeadPosition = RIGHT;	break;
			case TOP:		pObj->m_nHeadPosition = LEFT;	break;
			}
		}
		else
		{
			float fTempBottom = pObj->GetBottom();
			pObj->SetBottom(fBBoxBottom + fBBoxRight	- pObj->GetRight());
			pObj->SetLeft  (fBBoxLeft	+ fTempBottom	- fBBoxBottom);
			fTemp = pObj->GetWidth(); pObj->SetWidth(pObj->GetHeight()); pObj->SetHeight(fTemp);
			pObj->SetRight(pObj->GetLeft()	 + pObj->GetWidth());
			pObj->SetTop  (pObj->GetBottom() + pObj->GetHeight());

			switch (pObj->m_nHeadPosition)
			{
			case LEFT:		pObj->m_nHeadPosition = TOP;	break;
			case RIGHT:		pObj->m_nHeadPosition = BOTTOM;	break;
			case BOTTOM:	pObj->m_nHeadPosition = LEFT;	break;
			case TOP:		pObj->m_nHeadPosition = RIGHT;	break;
			}
		}

		if (pCounterPartObj)
			pCounterPartObj->AlignToCounterPart(pObj);
	}

// TODO: What happens if counterpart objects are not found (are not aligned)

	int nRotation = (bClockwise) ? -90 : 90;
	if (bInnerForm)
		nRotation *= -1;

	m_FoldSheetLayerRotation[nComponentRefIndex] += nRotation;
	if (m_FoldSheetLayerRotation[nComponentRefIndex] == 360)
		m_FoldSheetLayerRotation[nComponentRefIndex] = 0;
	else
		if (m_FoldSheetLayerRotation[nComponentRefIndex] == -90)
			m_FoldSheetLayerRotation[nComponentRefIndex] = 270;

	if ( ! pPrintSheet)
		pPrintSheet = pDefaultPrintSheet;
	while (pPrintSheet)
	{
		POSITION pos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = pPrintSheet->m_FrontSidePlates.GetNext(pos);
			rPlate.RotateLocalMaskAllExposures(nComponentRefIndex, bClockwise);
		}
		pPrintSheet->m_FrontSidePlates.m_defaultPlate.RotateLocalMaskAllExposures(nComponentRefIndex, bClockwise);

		pos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = pPrintSheet->m_BackSidePlates.GetNext(pos);
			rPlate.RotateLocalMaskAllExposures(nComponentRefIndex, ! bClockwise);
		}
		pPrintSheet->m_BackSidePlates.m_defaultPlate.RotateLocalMaskAllExposures(nComponentRefIndex,  ! bClockwise);

		pPrintSheet = GetNextPrintSheet(pPrintSheet);
	}
}

void CLayout::TurnFoldSheet(int nComponentRefIndex, CPrintSheet* pDefaultPrintSheet)
{
	CObArray objectPtrList; 
	float	 fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop;

	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	m_FrontSide.GetFoldSheetObjects(nComponentRefIndex, &objectPtrList, &fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, FALSE, pPrintSheet);

	float fFlipAxis = fBBoxLeft + (fBBoxRight - fBBoxLeft)/2.0f;
	for (int i = 0; i < objectPtrList.GetSize(); i++)
	{
		CLayoutObject* pObj			   = (CLayoutObject*)objectPtrList[i];
		CLayoutObject* pCounterPartObj = pObj->FindCounterpartObject();

		//float fLeft			= 2.0f * fFlipAxis - pObj->GetRight();
		//float fRight		= 2.0f * fFlipAxis - pObj->GetLeft();
		float fLeft			= fFlipAxis - (pObj->GetRight() - fFlipAxis);
		float fRight		= fFlipAxis - (pObj->GetLeft()  - fFlipAxis);
		float fBottom		= pObj->GetBottom();
		float fTop			= pObj->GetTop();
		int	  nHeadPosition = pObj->m_nHeadPosition;

		if (pCounterPartObj)
		{
			CLayoutObject tempObj = *pCounterPartObj;	// Change objects contents ...
			*pCounterPartObj	  = *pObj;
			*pObj				  = tempObj;
		}

		pObj->SetLeft(fLeft); pObj->SetRight(fRight); pObj->SetBottom(fBottom); pObj->SetTop(fTop);
		switch (nHeadPosition)
		{
		case LEFT:	pObj->m_nHeadPosition = RIGHT;			break;
		case RIGHT:	pObj->m_nHeadPosition = LEFT;			break;
		default:	pObj->m_nHeadPosition = nHeadPosition;	break;
		}

		if (pCounterPartObj)
			pCounterPartObj->AlignToCounterPart(pObj);
	}

	m_FoldSheetLayerTurned[nComponentRefIndex] = !m_FoldSheetLayerTurned[nComponentRefIndex];

	if ( ! pPrintSheet)
		pPrintSheet = pDefaultPrintSheet;
	while (pPrintSheet)
	{
		POSITION pos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = pPrintSheet->m_FrontSidePlates.GetNext(pos);
			rPlate.TurnLocalMaskAllExposures(nComponentRefIndex);
		}
		pPrintSheet->m_FrontSidePlates.m_defaultPlate.TurnLocalMaskAllExposures(nComponentRefIndex);

		pos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = pPrintSheet->m_BackSidePlates.GetNext(pos);
			rPlate.TurnLocalMaskAllExposures(nComponentRefIndex);
		}
		pPrintSheet->m_BackSidePlates.m_defaultPlate.TurnLocalMaskAllExposures(nComponentRefIndex);

		pPrintSheet = GetNextPrintSheet(pPrintSheet);
	}
}

void CLayout::TurnSelectedObjects()
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CObArray objectPtrList, counterObjectPtrList;
	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	while (pDI)
	{
		CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
		objectPtrList.Add((CObject*)pLayoutObject);
		CLayoutObject* pCounterPartObj = pLayoutObject->FindCounterpartObject();
		counterObjectPtrList.Add((CObject*)pCounterPartObj);

		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}

	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop, fBBoxWidth, fBBoxHeight;
	m_FrontSide.CalcBoundingRectObjects(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, &fBBoxWidth, &fBBoxHeight, FALSE, objectPtrList, FALSE);

	float fFlipAxis = fBBoxLeft + (fBBoxRight - fBBoxLeft)/2.0f;
	for (int i = 0; i < objectPtrList.GetSize(); i++)
	{
		CLayoutObject* pObj			   = (CLayoutObject*)objectPtrList[i];
		CLayoutObject* pCounterPartObj = (CLayoutObject*)counterObjectPtrList[i];

		float fLeft			= fFlipAxis - (pObj->GetRight() - fFlipAxis);
		float fRight		= fFlipAxis - (pObj->GetLeft()  - fFlipAxis);
		float fBottom		= pObj->GetBottom();
		float fTop			= pObj->GetTop();
		int	  nHeadPosition = pObj->m_nHeadPosition;
		BOOL  bObjectTurned = pObj->m_bObjectTurned;

		if (pCounterPartObj)
		{
			CLayoutObject tempObj = *pCounterPartObj;	// Change objects contents ...
			*pCounterPartObj	  = *pObj;
			*pObj				  = tempObj;
		}

		pObj->SetLeft(fLeft); pObj->SetRight(fRight); pObj->SetBottom(fBottom); pObj->SetTop(fTop);
		switch (nHeadPosition)
		{
		case LEFT:	pObj->m_nHeadPosition = RIGHT;			break;
		case RIGHT:	pObj->m_nHeadPosition = LEFT;			break;
		default:	pObj->m_nHeadPosition = nHeadPosition;	break;
		}

		pObj->m_bObjectTurned = ! bObjectTurned;
		if (pCounterPartObj)
		{
			pCounterPartObj->m_bObjectTurned = ! bObjectTurned;
			pCounterPartObj->AlignToCounterPart(pObj);
		}
	}
}

void CLayout::RemoveFoldSheetLayer(int nComponentRefIndex, CPressDevice* pPressDevice, BOOL bRearrange)
{
	CPressDevice* pFrontPressDevice = NULL;
	CPressDevice* pBackPressDevice  = NULL;
	if ( ! pPressDevice)
	{
		pFrontPressDevice = m_FrontSide.GetPressDevice();
		pBackPressDevice  = m_BackSide.GetPressDevice();
	}
	else
		pFrontPressDevice = pBackPressDevice = pPressDevice;
	if ( ! pFrontPressDevice || ! pBackPressDevice)
		return;

	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop; 
	m_FrontSide.CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);
	float fGripper = (pFrontPressDevice->m_nPressType == CPressDevice::SheetPress) ? abs(fBBoxBottom - m_FrontSide.m_fPaperBottom) : 0.0f;

	if ( (nComponentRefIndex >= m_FoldSheetLayerRotation.GetSize()) || (nComponentRefIndex >= m_FoldSheetLayerTurned.GetSize()) )
		return;
	m_FoldSheetLayerRotation.RemoveAt(nComponentRefIndex);
	m_FoldSheetLayerTurned.RemoveAt(nComponentRefIndex);

	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_FrontSide.m_ObjectList.GetAt(pos);
		if ( ! rObject.IsFlatProduct())
			if (rObject.m_nComponentRefIndex == nComponentRefIndex)
			{
				POSITION newPos = pos;
				m_FrontSide.m_ObjectList.GetNext(newPos);
				m_FrontSide.m_ObjectList.RemoveAt(pos);
				pos = newPos;
				continue;
			}
		
		if ( ! rObject.IsFlatProduct())
			if (rObject.m_nComponentRefIndex > nComponentRefIndex)
				rObject.m_nComponentRefIndex--;

		m_FrontSide.m_ObjectList.GetNext(pos);
	}

	//if (m_FrontSide.FindSuitablePaper())
	if (bRearrange)
		m_FrontSide.PositionLayoutSide(fGripper, &GetPrintingProfile().m_press.m_pressDevice);

	m_BackSide.CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);
	fGripper = (pBackPressDevice->m_nPressType == CPressDevice::SheetPress) ? abs(fBBoxBottom - m_BackSide.m_fPaperBottom) : 0.0f;

	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_BackSide.m_ObjectList.GetAt(pos);
		if ( ! rObject.IsFlatProduct())
			if (rObject.m_nComponentRefIndex == nComponentRefIndex)
			{
				POSITION newPos = pos;
				m_BackSide.m_ObjectList.GetNext(newPos);
				m_BackSide.m_ObjectList.RemoveAt(pos);
				pos = newPos;
				continue;
			}
		
		if ( ! rObject.IsFlatProduct())
			if (rObject.m_nComponentRefIndex > nComponentRefIndex)
				rObject.m_nComponentRefIndex--;

		m_BackSide.m_ObjectList.GetNext(pos);
	}

	//if (m_BackSide.FindSuitablePaper())
	if (bRearrange)
		m_BackSide.PositionLayoutSide(fGripper, &GetPrintingProfile().m_press.m_pressDevice);

//	if (!CheckForProductType())	// if TreeView not already updated in CheckForProductType() - do now
//		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),	NULL, 0, NULL);
}

void CLayout::RemoveFoldSheetLayerAll()
{
	m_FoldSheetLayerRotation.RemoveAll();
	m_FoldSheetLayerTurned.RemoveAll();

	m_FrontSide.m_ObjectList.RemoveAll();
	m_BackSide.m_ObjectList.RemoveAll();
}

void CLayout::CopyAndPasteFoldSheet(CPrintSheet* pSourcePrintSheet, CPrintSheet* pTargetPrintSheet, int nSourceLayerRefIndex, BOOL bSourceInnerForm, 
									CFoldSheetDropTargetInfo dropTargetInfo, BOOL bTargetBackSide, CLayoutObject* pSpreadObject, BOOL bRearrange)
{
	CLayout* pSourceLayout = (pSourcePrintSheet) ? pSourcePrintSheet->GetLayout() : NULL;
	if ( ! pSourceLayout)
		return;

	CLayoutObject*	pTargetLayoutObject		= dropTargetInfo.m_pTargetLayoutObject;
	int				nDockingLayerRefIndex	= dropTargetInfo.m_nTargetLayerRefIndex;
	int				nDockingSide			= dropTargetInfo.m_nDockingDirection;
	int				nDockingAlignment		= dropTargetInfo.m_nDockingAlignment;

	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop; 
	m_FrontSide.CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);	// need to calc gripper 
	float fGripper = (m_FrontSide.GetPressDevice()->m_nPressType == CPressDevice::SheetPress) ? abs(fBBoxBottom - m_FrontSide.m_fPaperBottom) : 0.0f;

	m_FoldSheetLayerRotation.Add(pSourceLayout->m_FoldSheetLayerRotation[nSourceLayerRefIndex]);
	m_FoldSheetLayerTurned.Add  (pSourceLayout->m_FoldSheetLayerTurned[nSourceLayerRefIndex]);

	CObArray		  sourceObjectPointers,  targetObjectPointers;
	CLayoutObjectList frontSourceObjectList;

	float fFrontSourceBBoxLeft, fFrontSourceBBoxBottom, fFrontSourceBBoxRight, fFrontSourceBBoxTop;
	float fFrontTargetBBoxLeft, fFrontTargetBBoxBottom, fFrontTargetBBoxRight, fFrontTargetBBoxTop;

	// FRONTSIDE
	if ( (dropTargetInfo.m_nTargetType == CFoldSheetDropTargetInfo::DropTargetFoldSheet) && (dropTargetInfo.m_nDockingDirection != -1) )
		m_FrontSide.GetFoldSheetObjects(nDockingLayerRefIndex, &targetObjectPointers, &fFrontTargetBBoxLeft,  &fFrontTargetBBoxBottom, &fFrontTargetBBoxRight, &fFrontTargetBBoxTop, FALSE, pTargetPrintSheet);
	else
	{
		fFrontTargetBBoxLeft	= dropTargetInfo.m_fTargetLeft;		fFrontTargetBBoxBottom	= dropTargetInfo.m_fTargetBottom; 
		fFrontTargetBBoxRight	= dropTargetInfo.m_fTargetRight;	fFrontTargetBBoxTop		= dropTargetInfo.m_fTargetTop; 
	}

	pSourceLayout->m_FrontSide.GetFoldSheetObjects(nSourceLayerRefIndex, &sourceObjectPointers, &fFrontSourceBBoxLeft,  &fFrontSourceBBoxBottom, &fFrontSourceBBoxRight, &fFrontSourceBBoxTop, FALSE, pSourcePrintSheet);

	if ( ! sourceObjectPointers.GetSize())
		return;
	for (int i = 0; i < sourceObjectPointers.GetSize(); i++)
		frontSourceObjectList.AddTail(*((CLayoutObject*)sourceObjectPointers[i]));

	if (bTargetBackSide)
	{
		if (KindOfProduction() == WORK_AND_TURN)
			switch (nDockingSide)
			{
			case LEFT:	nDockingSide = RIGHT;	break;
			case RIGHT:	nDockingSide = LEFT;	break;
			}
		else
			switch (nDockingSide)
			{
			case BOTTOM:	nDockingSide = TOP;		break;
			case TOP:		nDockingSide = BOTTOM;	break;
			}
	}

	// FRONTSIDE
	if ( ! pSpreadObject)
	{
		if (bRearrange)
		{
			RearrangeToPasteLayer(nDockingLayerRefIndex, nDockingSide, fFrontSourceBBoxLeft, fFrontSourceBBoxBottom, fFrontSourceBBoxRight, fFrontSourceBBoxTop, pTargetPrintSheet);

			// calc target bboxes once more, because positions could be changed by RearrangeToPasteLayer()
			m_FrontSide.GetFoldSheetObjects(nDockingLayerRefIndex, &targetObjectPointers, &fFrontTargetBBoxLeft,  &fFrontTargetBBoxBottom, &fFrontTargetBBoxRight, &fFrontTargetBBoxTop, FALSE, pTargetPrintSheet);
		}
	}
	else
	{
		switch (nDockingSide)
		{
		case LEFT:	m_FrontSide.SpreadSheet(pSpreadObject, LEFT,   fFrontSourceBBoxRight - fFrontSourceBBoxLeft);	break;
		case RIGHT:	m_FrontSide.SpreadSheet(pSpreadObject, RIGHT,  fFrontSourceBBoxRight - fFrontSourceBBoxLeft);	break;
		case BOTTOM:m_FrontSide.SpreadSheet(pSpreadObject, BOTTOM, fFrontSourceBBoxTop	 - fFrontSourceBBoxBottom); break;
		case TOP:	m_FrontSide.SpreadSheet(pSpreadObject, TOP,    fFrontSourceBBoxTop	 - fFrontSourceBBoxBottom); break;
		}
		// calc target bboxes once more, because positions could be changed by RearrangeToPasteLayer()
		m_FrontSide.GetFoldSheetObjects(nDockingLayerRefIndex, &targetObjectPointers, &fFrontTargetBBoxLeft,  &fFrontTargetBBoxBottom, &fFrontTargetBBoxRight, &fFrontTargetBBoxTop, FALSE, pTargetPrintSheet);
		switch (nDockingSide)
		{
		case LEFT:	fFrontTargetBBoxLeft   = pSpreadObject->GetLeft();	break;
		case RIGHT:	fFrontTargetBBoxRight  = pSpreadObject->GetRight();	break;
		case BOTTOM:fFrontTargetBBoxBottom = pSpreadObject->GetBottom();break;
		case TOP:	fFrontTargetBBoxTop    = pSpreadObject->GetTop();	break;
		}
	}

	float fOffsetX, fOffsetY;
	switch (nDockingSide)
	{
	case LEFT:		fOffsetX = fFrontTargetBBoxLeft	 - fFrontSourceBBoxRight; 
					fOffsetY = (nDockingAlignment == BOTTOM) ? fFrontTargetBBoxBottom - fFrontSourceBBoxBottom : fFrontTargetBBoxTop   - fFrontSourceBBoxTop;		
					break;
	case RIGHT:		fOffsetX = fFrontTargetBBoxRight - fFrontSourceBBoxLeft;  
					fOffsetY = (nDockingAlignment == BOTTOM) ? fFrontTargetBBoxBottom - fFrontSourceBBoxBottom : fFrontTargetBBoxTop   - fFrontSourceBBoxTop;		
					break;
	case BOTTOM:	fOffsetX = (nDockingAlignment == LEFT)	 ? fFrontTargetBBoxLeft	  - fFrontSourceBBoxLeft   : fFrontTargetBBoxRight - fFrontSourceBBoxRight;  
					fOffsetY = fFrontTargetBBoxBottom - fFrontSourceBBoxTop;	   
					break;
	case TOP:		fOffsetX = (nDockingAlignment == LEFT)	 ? fFrontTargetBBoxLeft   - fFrontSourceBBoxLeft   : fFrontTargetBBoxRight - fFrontSourceBBoxRight;  
					fOffsetY = fFrontTargetBBoxTop	 - fFrontSourceBBoxBottom; 
					break;
	case -1:		fOffsetX = fFrontTargetBBoxLeft	  - fFrontSourceBBoxLeft;
					fOffsetY = fFrontTargetBBoxBottom - fFrontSourceBBoxBottom;
					break;
	default:		m_FrontSide.CalcBoundingBoxAll(&fFrontTargetBBoxLeft, &fFrontTargetBBoxBottom, &fFrontTargetBBoxRight, &fFrontTargetBBoxTop, TRUE);
					fOffsetX = fFrontTargetBBoxLeft	 - fFrontSourceBBoxLeft;  fOffsetY = fFrontTargetBBoxBottom - fFrontSourceBBoxTop;	  break;
	}

	int		 nTargetLayerRefIndex = m_FoldSheetLayerRotation.GetSize() - 1;
	POSITION srcPos				  = frontSourceObjectList.GetHeadPosition();
	while (srcPos)
	{
		// Insert before modify, because Offset function needs pointer to layout side for invalidation
		CLayoutObject& rSourceLayoutObject  = frontSourceObjectList.GetNext(srcPos);
		POSITION	   dstPos			    = m_FrontSide.m_ObjectList.AddTail(rSourceLayoutObject);	 
		CLayoutObject& rFrontLayoutObject   = m_FrontSide.m_ObjectList.GetAt(dstPos); 
		rFrontLayoutObject.Offset(fOffsetX, fOffsetY);
		rFrontLayoutObject.m_nComponentRefIndex	= nTargetLayerRefIndex;

		dstPos							    = m_BackSide.m_ObjectList.AddTail(rSourceLayoutObject);	 
		CLayoutObject& rBackLayoutObject    = m_BackSide.m_ObjectList.GetAt(dstPos); 
		rBackLayoutObject.AlignToCounterPart(&rFrontLayoutObject);
		rBackLayoutObject.m_nComponentRefIndex	= nTargetLayerRefIndex;
	}

	if (dropTargetInfo.m_bDockingRotate)
		RotateFoldSheet90(nTargetLayerRefIndex);

	if ( (bSourceInnerForm && ! bTargetBackSide) || ( ! bSourceInnerForm && bTargetBackSide) )
		TurnFoldSheet(m_FoldSheetLayerRotation.GetSize() - 1);

	//m_FrontSide.PositionLayoutSide();
	//m_BackSide.PositionLayoutSide();

	AnalyzeMarks();
}

void CLayout::CutAndPasteFoldSheet(CPrintSheet* pSourcePrintSheet, CPrintSheet* pTargetPrintSheet, int nSourceLayerRefIndex, BOOL bSourceInnerForm, 
								   CFoldSheetDropTargetInfo dropTargetInfo, BOOL bTargetBackSide, CLayoutObject* pSpreadObject, BOOL bRearrange)
{
	CLayout* pSourceLayout = (pSourcePrintSheet) ? pSourcePrintSheet->GetLayout() : NULL;
	if ( ! pSourceLayout)
		return;

	CLayoutObject*	pTargetLayoutObject		= dropTargetInfo.m_pTargetLayoutObject;
	int				nDockingLayerRefIndex	= dropTargetInfo.m_nTargetLayerRefIndex;
	int				nDockingSide			= dropTargetInfo.m_nDockingDirection;
	int				nDockingAlignment		= dropTargetInfo.m_nDockingAlignment;

	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop; 
	m_FrontSide.CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);	// need to calc gripper 
	float fGripper = (m_FrontSide.GetPressDevice()->m_nPressType == CPressDevice::SheetPress) ? abs(fBBoxBottom - m_FrontSide.m_fPaperBottom) : 0.0f;

	CObArray		  sourceObjectPointers,  targetObjectPointers;
	CLayoutObjectList frontSourceObjectList;

	float fFrontSourceBBoxLeft, fFrontSourceBBoxBottom, fFrontSourceBBoxRight, fFrontSourceBBoxTop;
	float fFrontTargetBBoxLeft, fFrontTargetBBoxBottom, fFrontTargetBBoxRight, fFrontTargetBBoxTop;

	// FRONTSIDE
	if ( (dropTargetInfo.m_nTargetType == CFoldSheetDropTargetInfo::DropTargetFoldSheet) && (dropTargetInfo.m_nDockingDirection != -1) )
		m_FrontSide.GetFoldSheetObjects(nDockingLayerRefIndex, &targetObjectPointers, &fFrontTargetBBoxLeft,  &fFrontTargetBBoxBottom, &fFrontTargetBBoxRight, &fFrontTargetBBoxTop, FALSE, pTargetPrintSheet);
	else
	{
		fFrontTargetBBoxLeft	= dropTargetInfo.m_fTargetLeft;		fFrontTargetBBoxBottom	= dropTargetInfo.m_fTargetBottom; 
		fFrontTargetBBoxRight	= dropTargetInfo.m_fTargetRight;	fFrontTargetBBoxTop		= dropTargetInfo.m_fTargetTop; 
	}

	pSourceLayout->m_FrontSide.GetFoldSheetObjects(nSourceLayerRefIndex, &sourceObjectPointers, &fFrontSourceBBoxLeft,  &fFrontSourceBBoxBottom, &fFrontSourceBBoxRight, &fFrontSourceBBoxTop, FALSE, pSourcePrintSheet);

	if ( ! sourceObjectPointers.GetSize())
		return;
	for (int i = 0; i < sourceObjectPointers.GetSize(); i++)
		frontSourceObjectList.AddTail(*((CLayoutObject*)sourceObjectPointers[i]));

	if (bTargetBackSide)
	{
		if (KindOfProduction() == WORK_AND_TURN)
			switch (nDockingSide)
			{
			case LEFT:	nDockingSide = RIGHT;	break;
			case RIGHT:	nDockingSide = LEFT;	break;
			}
		else
			switch (nDockingSide)
			{
			case BOTTOM:	nDockingSide = TOP;		break;
			case TOP:		nDockingSide = BOTTOM;	break;
			}
	}


	int nFoldSheetLayerRotation = pSourceLayout->m_FoldSheetLayerRotation[nSourceLayerRefIndex];
	int nFoldSheetLayerTurned   = pSourceLayout->m_FoldSheetLayerTurned[nSourceLayerRefIndex];

	//// remove source foldsheet 
	pSourceLayout->RemoveFoldSheetLayer(nSourceLayerRefIndex);

	// FRONTSIDE
	if ( ! pSpreadObject)
	{
		if (bRearrange)
		{
			RearrangeToPasteLayer(nDockingLayerRefIndex, nDockingSide, fFrontSourceBBoxLeft, fFrontSourceBBoxBottom, fFrontSourceBBoxRight, fFrontSourceBBoxTop, pTargetPrintSheet);

			// calc target bboxes once more, because positions could be changed by RearrangeToPasteLayer()
			m_FrontSide.GetFoldSheetObjects(nDockingLayerRefIndex, &targetObjectPointers, &fFrontTargetBBoxLeft,  &fFrontTargetBBoxBottom, &fFrontTargetBBoxRight, &fFrontTargetBBoxTop, FALSE, pTargetPrintSheet);
		}
	}
	else
	{
		switch (nDockingSide)
		{
		case LEFT:	m_FrontSide.SpreadSheet(pSpreadObject, LEFT,   fFrontSourceBBoxRight - fFrontSourceBBoxLeft);	break;
		case RIGHT:	m_FrontSide.SpreadSheet(pSpreadObject, RIGHT,  fFrontSourceBBoxRight - fFrontSourceBBoxLeft);	break;
		case BOTTOM:m_FrontSide.SpreadSheet(pSpreadObject, BOTTOM, fFrontSourceBBoxTop	 - fFrontSourceBBoxBottom); break;
		case TOP:	m_FrontSide.SpreadSheet(pSpreadObject, TOP,    fFrontSourceBBoxTop	 - fFrontSourceBBoxBottom); break;
		}
		// calc target bboxes once more, because positions could be changed by RearrangeToPasteLayer()
		m_FrontSide.GetFoldSheetObjects(nDockingLayerRefIndex, &targetObjectPointers, &fFrontTargetBBoxLeft,  &fFrontTargetBBoxBottom, &fFrontTargetBBoxRight, &fFrontTargetBBoxTop, FALSE, pTargetPrintSheet);
		switch (nDockingSide)
		{
		case LEFT:	fFrontTargetBBoxLeft   = pSpreadObject->GetLeft();	break;
		case RIGHT:	fFrontTargetBBoxRight  = pSpreadObject->GetRight();	break;
		case BOTTOM:fFrontTargetBBoxBottom = pSpreadObject->GetBottom();break;
		case TOP:	fFrontTargetBBoxTop    = pSpreadObject->GetTop();	break;
		}
	}

	float fOffsetX, fOffsetY;
	switch (nDockingSide)
	{
	case LEFT:		fOffsetX = fFrontTargetBBoxLeft	 - fFrontSourceBBoxRight; 
					fOffsetY = (nDockingAlignment == BOTTOM) ? fFrontTargetBBoxBottom - fFrontSourceBBoxBottom : fFrontTargetBBoxTop   - fFrontSourceBBoxTop;		
					break;
	case RIGHT:		fOffsetX = fFrontTargetBBoxRight - fFrontSourceBBoxLeft;  
					fOffsetY = (nDockingAlignment == BOTTOM) ? fFrontTargetBBoxBottom - fFrontSourceBBoxBottom : fFrontTargetBBoxTop   - fFrontSourceBBoxTop;		
					break;
	case BOTTOM:	fOffsetX = (nDockingAlignment == LEFT)	 ? fFrontTargetBBoxLeft	  - fFrontSourceBBoxLeft   : fFrontTargetBBoxRight - fFrontSourceBBoxRight;  
					fOffsetY = fFrontTargetBBoxBottom - fFrontSourceBBoxTop;	   
					break;
	case TOP:		fOffsetX = (nDockingAlignment == LEFT)	 ? fFrontTargetBBoxLeft   - fFrontSourceBBoxLeft   : fFrontTargetBBoxRight - fFrontSourceBBoxRight;  
					fOffsetY = fFrontTargetBBoxTop	 - fFrontSourceBBoxBottom; 
					break;
	case -1:		fOffsetX = fFrontTargetBBoxLeft	  - fFrontSourceBBoxLeft;
					fOffsetY = fFrontTargetBBoxBottom - fFrontSourceBBoxBottom;
					break;
	default:		m_FrontSide.CalcBoundingBoxAll(&fFrontTargetBBoxLeft, &fFrontTargetBBoxBottom, &fFrontTargetBBoxRight, &fFrontTargetBBoxTop, TRUE);
					fOffsetX = fFrontTargetBBoxLeft	 - fFrontSourceBBoxLeft;  fOffsetY = fFrontTargetBBoxBottom - fFrontSourceBBoxTop;	  break;
	}

	///// insert
	m_FoldSheetLayerRotation.Add(nFoldSheetLayerRotation);
	m_FoldSheetLayerTurned.Add  (nFoldSheetLayerTurned);

	int		 nTargetLayerRefIndex = m_FoldSheetLayerRotation.GetSize() - 1;
	POSITION srcPos				  = frontSourceObjectList.GetHeadPosition();
	while (srcPos)
	{
		// Insert before modify, because Offset function needs pointer to layout side for invalidation
		CLayoutObject& rSourceLayoutObject  = frontSourceObjectList.GetNext(srcPos);
		POSITION	   dstPos			    = m_FrontSide.m_ObjectList.AddTail(rSourceLayoutObject);	 
		CLayoutObject& rFrontLayoutObject   = m_FrontSide.m_ObjectList.GetAt(dstPos); 
		rFrontLayoutObject.Offset(fOffsetX, fOffsetY);
		rFrontLayoutObject.m_nComponentRefIndex	= nTargetLayerRefIndex;

		dstPos							    = m_BackSide.m_ObjectList.AddTail(rSourceLayoutObject);	 
		CLayoutObject& rBackLayoutObject    = m_BackSide.m_ObjectList.GetAt(dstPos); 
		rBackLayoutObject.AlignToCounterPart(&rFrontLayoutObject);
		rBackLayoutObject.m_nComponentRefIndex	= nTargetLayerRefIndex;
	}


	if (dropTargetInfo.m_bDockingRotate)
		RotateFoldSheet90(nTargetLayerRefIndex);

	if ( (bSourceInnerForm && ! bTargetBackSide) || ( ! bSourceInnerForm && bTargetBackSide) )
		TurnFoldSheet(m_FoldSheetLayerRotation.GetSize() - 1);

	m_FrontSide.PositionLayoutSide();
	m_BackSide.PositionLayoutSide();

	AnalyzeMarks();
}


// Rearrange objects to fit layout for taking over new foldsheet layer with source bbox extents
void CLayout::RearrangeToPasteLayer(int nDockingLayerRefIndex, int nDockingSide, float fSourceBBoxLeft,  float fSourceBBoxBottom, 
																				 float fSourceBBoxRight, float fSourceBBoxTop, CPrintSheet* pTargetPrintSheet)
{
	switch (nDockingSide)
	{
	case LEFT:	
		{
			CLayoutObject* pNeighbour = m_FrontSide.GetFirstFoldSheetLayerNeighbour(nDockingLayerRefIndex, LEFT);
			CLayoutObject* pLeftmost  = m_FrontSide.GetLeftmostFoldSheetLayerObject(nDockingLayerRefIndex); 
			if (pLeftmost)
			{
				float fMargin	   = pLeftmost->GetCurrentGrossMargin(pTargetPrintSheet, LEFT);
				float fTargetSpace = (pNeighbour) ? (pLeftmost->GetLeft() - pNeighbour->GetRight()) : pLeftmost->m_fLeftBorder - fMargin;
				float fSourceSpace = fSourceBBoxRight - fSourceBBoxLeft;
				if (fTargetSpace < fSourceSpace)
					m_FrontSide.SpreadSheet(pLeftmost, LEFT, fSourceSpace + fMargin);
			}
		}
		break;

	case RIGHT:	
		{
			CLayoutObject* pNeighbour = m_FrontSide.GetFirstFoldSheetLayerNeighbour(nDockingLayerRefIndex, RIGHT);
			CLayoutObject* pRightmost = m_FrontSide.GetRightmostFoldSheetLayerObject(nDockingLayerRefIndex); 
			if (pRightmost)
			{
				float fMargin	   = pRightmost->GetCurrentGrossMargin(pTargetPrintSheet, RIGHT);
				float fTargetSpace = (pNeighbour) ? (pNeighbour->GetLeft() - pRightmost->GetRight()) : pRightmost->m_fRightBorder - fMargin;
				float fSourceSpace = fSourceBBoxRight - fSourceBBoxLeft;
				if (fTargetSpace < fSourceSpace)
					m_FrontSide.SpreadSheet(pRightmost, RIGHT, fSourceSpace + fMargin);
			}
		}
		break;

	case BOTTOM:	
		{
			CLayoutObject* pNeighbour  = m_FrontSide.GetFirstFoldSheetLayerNeighbour(nDockingLayerRefIndex, BOTTOM);
			CLayoutObject* pBottommost = m_FrontSide.GetBottommostFoldSheetLayerObject(nDockingLayerRefIndex); 
			if (pBottommost)
			{
				float fMargin	   = pBottommost->GetCurrentGrossMargin(pTargetPrintSheet, BOTTOM);
				float fTargetSpace = (pNeighbour) ? (pBottommost->GetBottom() - pNeighbour->GetTop()) : pBottommost->m_fLowerBorder - fMargin;
				float fSourceSpace = fSourceBBoxTop - fSourceBBoxBottom;
				if (fTargetSpace < fSourceSpace)
					m_FrontSide.SpreadSheet(pBottommost, BOTTOM, fSourceSpace + fMargin);
			}
		}
		break;

	case TOP:	
		{
			CLayoutObject* pNeighbour = m_FrontSide.GetFirstFoldSheetLayerNeighbour(nDockingLayerRefIndex, TOP);
			CLayoutObject* pTopmost   = m_FrontSide.GetTopmostFoldSheetLayerObject(nDockingLayerRefIndex); 
			if (pTopmost)
			{
				float fMargin	   = pTopmost->GetCurrentGrossMargin(pTargetPrintSheet, TOP);
				float fTargetSpace = (pNeighbour) ? (pNeighbour->GetBottom() - pTopmost->GetTop()) : pTopmost->m_fUpperBorder - fMargin;
				float fSourceSpace = fSourceBBoxTop - fSourceBBoxBottom;
				if (fTargetSpace < fSourceSpace)
					m_FrontSide.SpreadSheet(pTopmost, TOP, fSourceSpace + fMargin);
			}
		}
		break;
	}
}

BOOL CLayout::SetWorkStyle(int nWorkStyle)
{
	if ( (nWorkStyle == WORK_AND_TURN) && (m_nProductType != WORK_AND_TURN) )
	{
		float fSheetCenterX = m_FrontSide.m_fPaperLeft	 + m_FrontSide.m_fPaperWidth /2.0f;
		float fSheetCenterY = m_FrontSide.m_fPaperBottom + m_FrontSide.m_fPaperHeight/2.0f;
		for (int i = 0; i < m_FoldSheetLayerTurned.GetSize(); i++)
		{
			float fLeft, fBottom, fRight, fTop, fWidth, fHeight;
			m_FrontSide.CalcBoundingRectObjects(&fLeft, &fBottom, &fRight, &fTop, &fWidth, &fHeight, FALSE, i, FALSE, NULL);
			float fCenterX = fLeft	 + (fRight - fLeft)  /2.0f;
			float fCenterY = fBottom + (fTop   - fBottom)/2.0f;
			if (fCenterX < fSheetCenterX)
			{
				if ( ! m_FoldSheetLayerTurned[i])
					TurnFoldSheet(i);
			}
			else
				if (fCenterX >= fSheetCenterX)
				{
					if (m_FoldSheetLayerTurned[i])
						TurnFoldSheet(i);
				}
		}
		if (KindOfProduction() == WORK_AND_TUMBLE)
		{
			m_BackSide.TumbleAll();
			m_BackSide.TurnAll();
		}
		m_nProductType = nWorkStyle;
	}
	else
	if ( (nWorkStyle == WORK_AND_TUMBLE) && (m_nProductType != WORK_AND_TUMBLE) )
	{
		float fSheetCenterX = m_FrontSide.m_fPaperLeft	 + m_FrontSide.m_fPaperWidth /2.0f;
		float fSheetCenterY = m_FrontSide.m_fPaperBottom + m_FrontSide.m_fPaperHeight/2.0f;
		for (int i = 0; i < m_FoldSheetLayerTurned.GetSize(); i++)
		{
			float fLeft, fBottom, fRight, fTop, fWidth, fHeight;
			m_FrontSide.CalcBoundingRectObjects(&fLeft, &fBottom, &fRight, &fTop, &fWidth, &fHeight, FALSE, i, FALSE, NULL);
			float fCenterX = fLeft	 + (fRight - fLeft)  /2.0f;
			float fCenterY = fBottom + (fTop   - fBottom)/2.0f;
			if (fCenterY < fSheetCenterY)
			{
				if ( ! m_FoldSheetLayerTurned[i])
				{
					TurnFoldSheet(i);
					RotateFoldSheet90(i);
					RotateFoldSheet90(i);
				}
			}
			else
				if (fCenterY >= fSheetCenterY)
				{
					if (m_FoldSheetLayerTurned[i])
					{
						TurnFoldSheet(i);
						RotateFoldSheet90(i);
						RotateFoldSheet90(i);
					}
				}
		}
		if (KindOfProduction() == WORK_AND_TURN)
		{
			m_BackSide.TurnAll();
			m_BackSide.TumbleAll();
		}
		m_nProductType = nWorkStyle;
	}
	else
	if ( (nWorkStyle == WORK_AND_BACK) && (m_nProductType != WORK_AND_BACK) )
	{
		if (KindOfProduction() == WORK_AND_TUMBLE)
		{
			m_BackSide.TumbleAll();
			m_BackSide.TurnAll();
		}
		m_nProductType = nWorkStyle;
	}
	else
		m_nProductType = nWorkStyle;

	return TRUE;
}

void CLayout::SetTurnStyle(int nTurnStyle)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	if (m_nProductType == WORK_SINGLE_SIDE)
		return;
	if ( (KindOfProduction() == WORK_AND_TURN) && (nTurnStyle == 0) )
		return;
	if ( (KindOfProduction() == WORK_AND_TUMBLE) && (nTurnStyle == 1) )
		return;

	if (nTurnStyle == 0)
	{
		m_BackSide.TurnAll();
		m_BackSide.TumbleAll();
	}
	else
	{
		m_BackSide.TumbleAll();
		m_BackSide.TurnAll();
	}
}

int	CLayout::GetCurrentWorkStyle()
{
	return m_nProductType;
}

BOOL CLayout::CheckForProductType_Old(CImpManDoc* pDoc)
{
	return TRUE;	// from version 6 and higher, we no longer need this mechanism
}

BOOL CLayout::CheckForProductType(CImpManDoc* pDoc)
{
	if (this == NULL)
		return FALSE;

	if ( ! HasPages() && ! HasFlatProducts())	// if empty
		return FALSE;
	if (HasFlatProducts())
		return FALSE;

	int nOldType = m_nProductType;

	//if (m_bNOCheckForProductType)
	//	return TRUE;
	//else
	{
		int nTurned	   = 0;
		int nNotTurned = 0;
		for (int i = 0; i < m_FoldSheetLayerTurned.GetSize(); i++)
			if (m_FoldSheetLayerTurned[i])
				nTurned++;
			else
				nNotTurned++;

		if (nTurned == nNotTurned)	// Only when no. turned and not turned foldsheets is equal, 
		{							// we can have a 'work and turn' or 'work and tumble' form.
			if (m_FrontSide.IsIdentical(m_BackSide))
			{
				if (m_nProductType == WORK_AND_BACK)
					if (m_bAskForChangeToWorkAndTurnTumble)
					{
						CString strMsg;
						int		nProductType = KindOfProduction();  // WORK_AND_TURN   WORK_AND_TUMBLE
						switch (nProductType)
						{
						case WORK_AND_TURN:		strMsg.LoadString(IDS_ASKFOR_WORK_AND_TURN);	break;
						case WORK_AND_TUMBLE:	strMsg.LoadString(IDS_ASKFOR_WORK_AND_TUMBLE);	break;
						}
						if ( ! strMsg.IsEmpty())
						{
							if (KIMOpenLogMessage(strMsg, MB_YESNO, IDNO) == IDYES)
								m_nProductType = nProductType;
							m_bAskForChangeToWorkAndTurnTumble = FALSE;
						}
					}
			}
			else
			{
				if ( (m_nProductType == WORK_AND_TURN) || (m_nProductType == WORK_AND_TUMBLE) )
					if (m_bAskForChangeToWorkAndBack)
					{
						CString strMsg;
						strMsg.LoadString(IDS_ASKFOR_WORK_AND_BACK);
						if (KIMOpenLogMessage(strMsg, MB_YESNO, IDNO) == IDYES)
							m_nProductType = FALSE;
						m_bAskForChangeToWorkAndBack = FALSE;
					}
			}
		}
		else
			if ( (m_nProductType == WORK_AND_TURN) || (m_nProductType == WORK_AND_TUMBLE) )
				if (m_bAskForChangeToWorkAndBack)
				{
					CString strMsg;
					strMsg.LoadString(IDS_ASKFOR_WORK_AND_BACK);
					if (KIMOpenLogMessage(strMsg, MB_YESNO, IDNO) == IDYES)
						m_nProductType = FALSE;
					m_bAskForChangeToWorkAndBack = FALSE;
				}
	}

	if (nOldType != m_nProductType)
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			pView->m_DisplayList.Invalidate();

		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),			 NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData), NULL, 0, NULL);
		return TRUE;
	}
	else
		return FALSE;
}

// Return the number of printsheets which are based on this layout
int	CLayout::GetNumPrintSheetsBasedOn()
{
	//CPrintSheet* pPrintSheet = GetLastPrintSheet();
	//if ( ! pPrintSheet)
	//	return 0;
	//CFoldSheet* pFoldSheet = pPrintSheet->GetFoldSheet(0);
	//if (pFoldSheet)
	//	if (pFoldSheet->m_LayerList.GetSize() > 1)
	//	{
	//		int		 nNum = 0;
	//		POSITION pos  = CImpManDoc::GetDoc()->m_PrintSheetList.GetHeadPosition();
	//		while (pos)
	//		{
	//			CPrintSheet& rPrintSheet = CImpManDoc::GetDoc()->m_PrintSheetList.GetNext(pos);
	//			if (rPrintSheet.GetFoldSheet(0))
	//				if (rPrintSheet.GetFoldSheet(0)->m_strFoldSheetTypeName == pFoldSheet->m_strFoldSheetTypeName)
	//					nNum++;
	//		}
	//		return nNum;
	//	}
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)		// Document not yet present - i.e. we are serializing (loading)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return 0;	

	int		 nLayoutIndex = GetIndex();
	int		 nNumSheets   = 0;
	POSITION pos		  = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.m_nLayoutIndex == nLayoutIndex)
			if ( ! rPrintSheet.IsEmpty())
				nNumSheets++;
	}

	return max(nNumSheets, 1);
}

int	CLayout::GetNumEmptyPrintSheetsBasedOn()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)		// Document not yet present - i.e. we are serializing (loading)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return 0;	

	int nLayoutIndex = GetIndex();
	int nNumSheets   = 0;

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.m_nLayoutIndex == nLayoutIndex)
			if (rPrintSheet.IsEmpty())
				nNumSheets++;
	}

	return nNumSheets;
}

int	CLayout::GetMaxNumPrintSheetsBasedOn()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	CPrintSheet* pPrintSheet = GetLastPrintSheet(-1, TRUE);
	if ( ! pPrintSheet)
		return 0;

	//CFoldSheet* pFoldSheet = pPrintSheet->GetFoldSheet(0);
	//if (pFoldSheet)
	//	if (pFoldSheet->m_LayerList.GetSize() > 1)
	//	{
	//		int nNumUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(pFoldSheet->m_nProductPartIndex);
	//		int nNumFoldSheets	   = nNumUnImposedPages / pFoldSheet->GetNumPages();
	//		return GetNumPrintSheetsBasedOn() + nNumFoldSheets * pFoldSheet->m_LayerList.GetSize();
	//	}

	int nNumPrintSheetsAdd = INT_MAX;

	for (int nProductPartIndex = 0; nProductPartIndex < pDoc->m_boundProducts.GetNumPartsTotal(); nProductPartIndex++)
	{
		int nNumPagesPerSheet  = pPrintSheet->GetNumPagesProductPart(nProductPartIndex);
		if (nNumPagesPerSheet == 0)
			continue;
		int nNumUnplacedPages  = pDoc->m_PageTemplateList.GetNumUnplacedPages(nProductPartIndex);
		int nNumSheets		   = nNumUnplacedPages / nNumPagesPerSheet;
		nNumPrintSheetsAdd	   = min(nNumPrintSheetsAdd, nNumSheets);
	}

	CFlatProductType* pFlatProductType = pPrintSheet->GetFirstFlatFormatType();
	while (pFlatProductType)
	{
		CFlatProduct* pFlatProduct			   = (pFlatProductType) ? pFlatProductType->m_pFlatProduct : NULL;
		int			  nNumFormatsPerSheet	   = (pFlatProductType) ? pFlatProductType->m_nNumUp : 0;
		if (nNumFormatsPerSheet > 0)
		{
			int nNumUnImposedFlatFormats = (pFlatProduct) ? pDoc->m_flatProducts.GetNumUnImposedFormats(pFlatProduct->m_fTrimSizeWidth, pFlatProduct->m_fTrimSizeHeight) : 0;
			int	nNumSheets			     = nNumUnImposedFlatFormats / nNumFormatsPerSheet;
			nNumPrintSheetsAdd			 = min(nNumPrintSheetsAdd, nNumSheets);
		}
		pFlatProductType = pPrintSheet->GetNextFlatFormatType(pFlatProductType);
	}

	return (nNumPrintSheetsAdd == INT_MAX) ? GetNumPrintSheetsBasedOn() : GetNumPrintSheetsBasedOn() + nNumPrintSheetsAdd;
}

int CLayout::AddPrintSheetInstance(int nNum)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nNumAdded = 0;
	while (nNum >= 1)
	{
		CPrintSheet* pPrintSheet = GetLastPrintSheet();
		if ( ! pPrintSheet)
			return 0;

		CArray <CPrintSheetComponentRef, CPrintSheetComponentRef&> componentRefList; 
		for (int nComponentRefIndex = 0; nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
		{
			CPrintSheetComponentRef componentRef; 
			componentRef.m_nType			  = CPrintSheetComponentRef::FoldSheetLayer;
			componentRef.m_layerRef			  = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex]; 
			componentRef.m_nComponentRefIndex = nComponentRefIndex;
			componentRefList.Add(componentRef);
		}
		qsort((void*)componentRefList.GetData(), componentRefList.GetSize(), sizeof(CPrintSheetComponentRef), CPrintSheetComponentRef::Compare);

		CPrintSheet  newPrintSheet	   = *pPrintSheet;
		CPrintSheet* pInsertPrintSheet = pPrintSheet;

		int				 nPrevSrcFoldSheetIndex		  = -1;
		int				 nPrevSrcFoldSheetLayerIndex  = -1;
		int				 nNewFoldSheetIndex			  = -1; 
		int				 nNewFoldSheetLayerIndex	  = -1;
		CFoldSheet*		 pSrcFoldSheet				  = NULL;
		CFoldSheet*		 pNewFoldSheet				  = NULL;

		newPrintSheet.m_FoldSheetLayerRefs.SetSize(pPrintSheet->m_FoldSheetLayerRefs.GetSize());
		pPrintSheet = pDoc->m_PrintSheetList.InsertAfter(pInsertPrintSheet, newPrintSheet);

		for (int i = 0; i < componentRefList.GetSize(); i++)
		{
			int			nComponentRefIndex		= componentRefList[i].m_nComponentRefIndex;
			int			nSrcFoldSheetIndex		= componentRefList[i].m_layerRef.m_nFoldSheetIndex;
			int			nSrcFoldSheetLayerIndex = componentRefList[i].m_layerRef.m_nFoldSheetLayerIndex;
			if ( (nSrcFoldSheetIndex != nPrevSrcFoldSheetIndex) || (nSrcFoldSheetLayerIndex != nPrevSrcFoldSheetLayerIndex) )
			{
				nPrevSrcFoldSheetIndex		= nSrcFoldSheetIndex;
				nPrevSrcFoldSheetLayerIndex	= nSrcFoldSheetLayerIndex;
				pSrcFoldSheet			= pDoc->m_Bookblock.GetFoldSheetByIndex(nSrcFoldSheetIndex);
				pNewFoldSheet			= pDoc->m_Bookblock.GetFirstUnplacedFoldSheetLayer(pSrcFoldSheet, nNewFoldSheetLayerIndex, TRUE);
				if ( ! pNewFoldSheet)
					return 0;
				nNewFoldSheetIndex	    = pNewFoldSheet->GetIndex();
			}

			pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex		 = nNewFoldSheetIndex;
			pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex = nNewFoldSheetLayerIndex;
		}

		nNumAdded++;
		nNum--;
	}

	return nNumAdded;
}

int CLayout::RemovePrintSheetInstance(int nNum, BOOL bRemoveFromBookblock)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	BOOL bRemoveAll = FALSE;
	if (nNum == INT_MAX)	// remove all sheets of this layout
	{
		nNum = GetNumPrintSheetsBasedOn();
		bRemoveAll = TRUE;
	}

	int nRemoved = 0;
	CPrintSheet* pPrintSheet = GetLastPrintSheet();
	while (pPrintSheet && (nNum > 0) )
	{
		BOOL bPrintSheetReallyRemoved = FALSE;
		while (pPrintSheet->m_FoldSheetLayerRefs.GetSize() > 0)
		{
			pPrintSheet->RemoveFoldSheetLayer(0, FALSE, FALSE, TRUE, TRUE, NULL, bPrintSheetReallyRemoved, bRemoveFromBookblock); 
			if (bPrintSheetReallyRemoved)		// break here, because pPrintSheet pointer no longer valid after removal
				break;
		}
		
		if ( ! bPrintSheetReallyRemoved)
		{
			while (pPrintSheet->m_flatProductRefs.GetSize() > 0)
			{
				pPrintSheet->RemoveFlatProductType(0, FALSE, FALSE, TRUE, TRUE, NULL, bPrintSheetReallyRemoved); 
				if (bPrintSheetReallyRemoved)	// break here, because pPrintSheet pointer no longer valid after removal
					break;
			}
		}

		nRemoved++; 
		nNum--;
		pPrintSheet = GetLastPrintSheet();
	}
		
	if (GetNumEmptyPrintSheetsBasedOn() >= 1)	// all printsheets has been removed (one empty sheet remains) -> clear layout
		bRemoveAll = TRUE;

	if (bRemoveAll)
		DeleteContents();

	return nRemoved;
}

void CLayout::AlignLayoutObjects(int nDirection, CPrintSheet* pPrintSheet)
{
	if ( (nDirection == XCENTER) || (nDirection == YCENTER) )	// don't recognize trims when center in X and Y
		pPrintSheet = NULL;

	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop, fBBoxWidth, fBBoxHeight;
	m_FrontSide.CalcBoundingRectObjects(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, &fBBoxWidth, &fBBoxHeight, FALSE, -1, FALSE, pPrintSheet);	// foldsheet version
	
	CPressDevice* pPressDevice = m_FrontSide.GetPressDevice();
	if (nDirection == BOTTOM)
	{
		if (pPressDevice)
			if (pPressDevice->m_nPressType == CPressDevice::SheetPress)	// calc net bbox, because bottom must be aligned to gripper w/o trim (trim can be inside gripper)
			{
				float fNetBoxLeft, fNetBoxBottom, fNetBoxRight, fNetBoxTop, fNetBoxWidth, fNetBoxHeight;
				m_FrontSide.CalcBoundingRectObjects(&fNetBoxLeft, &fNetBoxBottom, &fNetBoxRight, &fNetBoxTop, &fNetBoxWidth, &fNetBoxHeight, FALSE, -1, FALSE, NULL);	// foldsheet version
				fBBoxBottom = fNetBoxBottom - pPressDevice->m_fGripper;
			}
	}

	float fBBoxLeftLabels, fBBoxBottomLabels, fBBoxRightLabels, fBBoxTopLabels, fBBoxWidthLabels, fBBoxHeightLabels;
	m_FrontSide.CalcBoundingRectObjects(&fBBoxLeftLabels, &fBBoxBottomLabels, &fBBoxRightLabels, &fBBoxTopLabels, &fBBoxWidthLabels, &fBBoxHeightLabels, FALSE, FALSE, pPrintSheet);	// label version

	if ( (fBBoxLeftLabels == 0) && (fBBoxRightLabels == 0) && (fBBoxBottomLabels == 0) && (fBBoxTopLabels == 0) )	// no labels
		;
	else
	{
		if (nDirection == BOTTOM)
			if (pPressDevice)
				if (pPressDevice->m_nPressType == CPressDevice::SheetPress)	// calc net bbox, because bottom must be aligned to gripper w/o trim (trim can be inside gripper)
					fBBoxBottomLabels = fBBoxBottomLabels - pPressDevice->m_fGripper;
	}

	if ((fBBoxWidth == 0.0f) || (fBBoxHeight == 0.0f))
	{
		fBBoxLeft	= fBBoxLeftLabels;
		fBBoxBottom = fBBoxBottomLabels;
		fBBoxRight	= fBBoxRightLabels;
		fBBoxTop    = fBBoxTopLabels;
	}
	else
	{
		if ( (fBBoxLeftLabels == 0) && (fBBoxRightLabels == 0) && (fBBoxBottomLabels == 0) && (fBBoxTopLabels == 0) )	// no labels
			;
		else
		{
			fBBoxLeft	= min(fBBoxLeft,	fBBoxLeftLabels);
			fBBoxBottom = min(fBBoxBottom,	fBBoxBottomLabels);
			fBBoxRight	= max(fBBoxRight,	fBBoxRightLabels);
			fBBoxTop    = max(fBBoxTop,		fBBoxTopLabels);
		}
	}
	fBBoxWidth  = fBBoxRight - fBBoxLeft;
	fBBoxHeight = fBBoxTop   - fBBoxBottom;

	float fXDiff = 0.0f, fYDiff = 0.0f;
	switch (nDirection)
	{
	case LEFT:		fXDiff = m_FrontSide.m_fPaperLeft - fBBoxLeft;																
					break;
	case XCENTER:	fXDiff = (m_FrontSide.m_fPaperLeft + (m_FrontSide.m_fPaperWidth - fBBoxWidth) / 2.0f) - fBBoxLeft;			
					break;
	case RIGHT:		fXDiff = m_FrontSide.m_fPaperRight - fBBoxRight;																
					break;
	case BOTTOM:	fYDiff = m_FrontSide.m_fPaperBottom - fBBoxBottom;															
					break;
	case YCENTER:	fYDiff = (m_FrontSide.m_fPaperBottom + (m_FrontSide.m_fPaperHeight - fBBoxHeight) / 2.0f) - fBBoxBottom;	
					break;
	case TOP:		fYDiff = m_FrontSide.m_fPaperTop - fBBoxTop;																
					break;
	}

	MoveLayoutObjects(fXDiff, fYDiff);
}

void CLayout::MoveLayoutObjects(float fXDiff, float fYDiff)
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject		= m_FrontSide.m_ObjectList.GetNext(pos);
		CLayoutObject* pCounterPart = rObject.FindCounterpartObject();

		rObject.SetLeft	 (rObject.GetLeft()   + fXDiff, FALSE);
		rObject.SetBottom(rObject.GetBottom() + fYDiff, FALSE);
		rObject.SetRight (rObject.GetRight()  + fXDiff, FALSE);
		rObject.SetTop   (rObject.GetTop()    + fYDiff, FALSE);

		pCounterPart->AlignToCounterPart(&rObject);
	}
}

void CLayout::RearrangePaper(BOOL bDontChangeIfFit, BOOL bPromptUser)
{
	if (bPromptUser)
	{
		if ( ! m_FrontSide.CheckPaper())
			if (KIMOpenLogMessage(IDS_PAPER_DONT_FIT_MESSAGE, MB_YESNO | MB_ICONWARNING, IDYES) == IDNO)
				return;
			else
				;//bDontChangeIfFit = FALSE;	// change anyway
		else
			return;
	}
	if (m_FrontSide.FindSuitablePaper(bDontChangeIfFit))
		m_FrontSide.PositionLayoutSide();

	if (m_BackSide.FindSuitablePaper(bDontChangeIfFit))
		m_BackSide.PositionLayoutSide();
}

void CLayout::RearrangeGeometry(CBoundProductPart& rProductPart, CPrintSheet* pPrintSheet)
{
	CObArray objectPtrList;

	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_FrontSide.m_ObjectList.GetNext(pos);
		objectPtrList.Add(rObject.FindCounterpartObject());

		BOOL bRotate = ((rObject.m_nHeadPosition == LEFT) || (rObject.m_nHeadPosition == RIGHT)) ? TRUE : FALSE;
		rObject.ModifySize(rProductPart.m_fPageWidth, rProductPart.m_fPageHeight, pPrintSheet, bRotate);
	}

	int nIndex = 0;
	pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (nIndex < objectPtrList.GetSize())
			((CLayoutObject*)objectPtrList[nIndex])->AlignToCounterPart(&rObject);
		nIndex++;
	}

	//m_FrontSide.PositionLayoutSide(rProductionProfile.m_press.m_pressDevice.m_fGripper, &rProductionProfile.m_press.m_pressDevice);
	//m_BackSide.PositionLayoutSide(rProductionProfile.m_press.m_pressDevice.m_fGripper, &rProductionProfile.m_press.m_pressDevice);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Gets the rectangle area of the foldsheet's spine (width/height can be zero)
// Returns the first foldsheet page
CLayoutObject* CLayout::GetSectionMarkSpineArea(CPrintSheet* pPrintSheet, int nComponentRefIndex, float& fSpineLeft, float& fSpineBottom, float& fSpineRight, float& fSpineTop, BOOL bComingAndGoing, int nInstance)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pPrintSheet)
		return NULL;
	if ( (nComponentRefIndex < 0) || (nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize()) )
		return NULL;

	int	nIndex = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;
	if (nIndex < 0)
	{
		fSpineLeft = fSpineBottom = fSpineRight = fSpineTop = 0.0f;
		return NULL;
	}

	POSITION	pos			= pDoc->m_Bookblock.m_FoldSheetList.FindIndex(nIndex);
	CFoldSheet&	rFoldSheet	= pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);

	CBoundProductPart& rPart = rFoldSheet.GetProductPart();
	if (rPart.m_strName.CompareNoCase(theApp.m_strCoverName) == 0)		// no section mark on cover
		return NULL;

	int nSpineIndex = 0, nSpineSide = FRONTSIDE;
	if (bComingAndGoing)
	{
	// Find the last page of foldsheet in the layout
		int nMaxPage = -INT_MAX;
		pos	= rFoldSheet.m_LayerList.GetHeadPosition();
		while (pos)
		{
			CFoldSheetLayer& rLayer	= rFoldSheet.m_LayerList.GetNext(pos);
			for (int i = rLayer.m_FrontPageArray.GetSize() - 1; i >= 0; i--)
				if (rLayer.m_FrontPageArray[i] > nMaxPage)
				{
					nSpineIndex = i; nMaxPage = rLayer.m_FrontPageArray[i]; 
				}
			for (int i = rLayer.m_BackPageArray.GetSize() - 1; i >= 0; i--)
				if (rLayer.m_BackPageArray[i] > nMaxPage)
				{
					nSpineIndex = i; nMaxPage = rLayer.m_BackPageArray[i]; nSpineSide = BACKSIDE;
				}
		}
	}
	else
	{
	// Find the first page of foldsheet in the layout
		int nMinPage = INT_MAX;
		pos	= rFoldSheet.m_LayerList.GetHeadPosition();
		while (pos)
		{
			CFoldSheetLayer& rLayer	= rFoldSheet.m_LayerList.GetNext(pos);
			for (int i = 0; i < rLayer.m_FrontPageArray.GetSize(); i++)
				if (rLayer.m_FrontPageArray[i] < nMinPage)
				{
					nSpineIndex = i; nMinPage = rLayer.m_FrontPageArray[i]; 
				}
			for (int i = 0; i < rLayer.m_BackPageArray.GetSize(); i++)
				if (rLayer.m_BackPageArray[i] < nMinPage)
				{
					nSpineIndex = i; nMinPage = rLayer.m_BackPageArray[i]; nSpineSide = BACKSIDE;
				}
		}
		if (nInstance > 1)
		{
			pos	= rFoldSheet.m_LayerList.GetHeadPosition();
			while (pos && (nInstance > 0) )
			{
				CFoldSheetLayer& rLayer	= rFoldSheet.m_LayerList.GetNext(pos);
				if (nSpineSide == FRONTSIDE)
				{
					for (int i = 0; i < rLayer.m_FrontPageArray.GetSize(); i++)
						if (rLayer.m_FrontPageArray[i] == nMinPage)
						{
							nSpineIndex = i; nInstance--; 
						}
				}
				else
				{
					for (int i = 0; i < rLayer.m_BackPageArray.GetSize(); i++)
						if (rLayer.m_BackPageArray[i] == nMinPage)
						{
							nSpineIndex = i; nInstance--; 
						}
				}
			}
			if (nInstance > 0)	// nth instance not found
				return NULL;
		}
	}

	if ( (nComponentRefIndex < 0) || (nComponentRefIndex >= m_FoldSheetLayerTurned.GetCount()) )
		return NULL;

	CLayoutSide* pLayoutSide;
	if (((nSpineSide == FRONTSIDE) && (m_FoldSheetLayerTurned[nComponentRefIndex])) ||
		((nSpineSide == BACKSIDE) && (!m_FoldSheetLayerTurned[nComponentRefIndex])))
		pLayoutSide = &m_BackSide;
	else
		pLayoutSide = &m_FrontSide;

	CLayoutObject* pSpinePage = NULL;
	pos						  = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)	
	{
		CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
		if ((rObject.m_nLayerPageIndex == nSpineIndex) && (rObject.m_nComponentRefIndex == nComponentRefIndex))
		{
			pSpinePage = &rObject;
			break;
		}
	}

	if ( ! pSpinePage)
	{
		fSpineLeft = fSpineBottom = fSpineRight = fSpineTop = 0.0f;
		return NULL;
	}
///////////////////////////////////////////////////////////////////////////////////////////////

// Calculate area between pages 
	CLayoutObject* pSpineBuddy = NULL;
	switch (pSpinePage->m_nHeadPosition)			
	{
	case TOP:		
	case BOTTOM:	if ( ((pSpinePage->m_nHeadPosition == TOP)    && (pSpinePage->m_nIDPosition == RIGHT)) ||
					     ((pSpinePage->m_nHeadPosition == BOTTOM) && (pSpinePage->m_nIDPosition == LEFT )) )
					{
						pSpineBuddy = (pSpinePage->m_LeftNeighbours.GetSize()) ? pSpinePage->m_LeftNeighbours[0] : NULL;
						if (pSpineBuddy)
							if (pSpineBuddy->m_nComponentRefIndex != pSpinePage->m_nComponentRefIndex)
								pSpineBuddy = NULL;
						fSpineRight = pSpinePage->GetLeft();
						fSpineLeft  = (pSpineBuddy) ? pSpineBuddy->GetRight() - 0.01f : fSpineRight;	// trick: -0.01f is an info for GetSheetTrimBox() that there is a buddy even if (fSpineLeft == fSpineRight) -> center mark instead of placing beside
					}
					else
					{
						pSpineBuddy = (pSpinePage->m_RightNeighbours.GetSize()) ? pSpinePage->m_RightNeighbours[0] : NULL;
						if (pSpineBuddy)
							if (pSpineBuddy->m_nComponentRefIndex != pSpinePage->m_nComponentRefIndex)
								pSpineBuddy = NULL;
						fSpineLeft  = pSpinePage->GetRight(); 
						fSpineRight = (pSpineBuddy) ? pSpineBuddy->GetLeft() - 0.01f : fSpineLeft;
					}
					fSpineBottom = min(pSpinePage->GetBottom(), (pSpineBuddy) ? pSpineBuddy->GetBottom() : pSpinePage->GetBottom());
					fSpineTop	 = max(pSpinePage->GetTop(),	(pSpineBuddy) ? pSpineBuddy->GetTop()	 : pSpinePage->GetTop());
					break;

	case LEFT:		
	case RIGHT:		if ( ((pSpinePage->m_nHeadPosition == RIGHT)  && (pSpinePage->m_nIDPosition == LEFT )) ||
					     ((pSpinePage->m_nHeadPosition == LEFT)   && (pSpinePage->m_nIDPosition == RIGHT)) )
					{
						pSpineBuddy  = (pSpinePage->m_LowerNeighbours.GetSize()) ? pSpinePage->m_LowerNeighbours[0] : NULL;
						if (pSpineBuddy)
							if (pSpineBuddy->m_nComponentRefIndex != pSpinePage->m_nComponentRefIndex)
								pSpineBuddy = NULL;
						fSpineTop	 = pSpinePage->GetBottom();
						fSpineBottom = (pSpineBuddy) ? pSpineBuddy->GetTop() - 0.01f : fSpineTop;
					}
					else
					{
						pSpineBuddy  = (pSpinePage->m_UpperNeighbours.GetSize()) ? pSpinePage->m_UpperNeighbours[0] : NULL;
						if (pSpineBuddy)
							if (pSpineBuddy->m_nComponentRefIndex != pSpinePage->m_nComponentRefIndex)
								pSpineBuddy = NULL;
						fSpineBottom = pSpinePage->GetTop();
						fSpineTop	 = (pSpineBuddy) ? pSpineBuddy->GetBottom() - 0.01f : fSpineBottom;
					}
					fSpineLeft  = min(pSpinePage->GetLeft(),  (pSpineBuddy) ? pSpineBuddy->GetLeft()  : pSpinePage->GetLeft());
					fSpineRight = max(pSpinePage->GetRight(), (pSpineBuddy) ? pSpineBuddy->GetRight() : pSpinePage->GetRight());
					break;
	}

	return pSpinePage;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Gets the rectangle area of the foldsheet's head or foot area for section mark (width/height can be zero)
// head or foot depends on foldsheet scheme (if foot to foot or head to head)
// Returns the first foldsheet page
CLayoutObject* CLayout::GetSectionMarkHeadFootArea(CPrintSheet* pPrintSheet, int nComponentRefIndex, float& fRefLeft, float& fRefBottom, float& fRefRight, float& fRefTop, BOOL bComingAndGoing, int nInstance)
{
	CImpManDoc*	pDoc		= CImpManDoc::GetDoc();
	if ( ! pDoc || ! pPrintSheet)
		return NULL;

	if ( (nComponentRefIndex < 0) || (nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize()) )
		return NULL;
	int	nIndex = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;
	if (nIndex < 0)
	{
		fRefLeft = fRefBottom = fRefRight = fRefTop = 0.0f;
		return NULL;
	}

	POSITION	pos			= pDoc->m_Bookblock.m_FoldSheetList.FindIndex(nIndex);
	CFoldSheet&	rFoldSheet	= pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);

	int nLayerPageIndex = 0, nLayerPageSide = FRONTSIDE;
	if (bComingAndGoing)
	{
	// Find the last page of foldsheet in the layout
		int nMaxPage = -INT_MAX;
		pos	= rFoldSheet.m_LayerList.GetHeadPosition();
		while (pos)
		{
			CFoldSheetLayer& rLayer	= rFoldSheet.m_LayerList.GetNext(pos);
			for (int i = rLayer.m_FrontPageArray.GetSize() - 1; i >= 0; i--)
				if (rLayer.m_FrontPageArray[i] > nMaxPage)
				{
					nLayerPageIndex = i; nMaxPage = rLayer.m_FrontPageArray[i]; 
				}
			for (int i = rLayer.m_BackPageArray.GetSize() - 1; i >= 0; i--)
				if (rLayer.m_BackPageArray[i] > nMaxPage)
				{
					nLayerPageIndex = i; nMaxPage = rLayer.m_BackPageArray[i]; nLayerPageSide = BACKSIDE;
				}
		}
	}
	else
	{
	// Find the first page of foldsheet in the layout
		int nMinPage = INT_MAX;
		pos	= rFoldSheet.m_LayerList.GetHeadPosition();
		while (pos)
		{
			CFoldSheetLayer& rLayer	= rFoldSheet.m_LayerList.GetNext(pos);
			for (int i = 0; i < rLayer.m_FrontPageArray.GetSize(); i++)
				if (rLayer.m_FrontPageArray[i] < nMinPage)
				{
					nLayerPageIndex = i; nMinPage = rLayer.m_FrontPageArray[i]; 
				}
			for (int i = 0; i < rLayer.m_BackPageArray.GetSize(); i++)
				if (rLayer.m_BackPageArray[i] < nMinPage)
				{
					nLayerPageIndex = i; nMinPage = rLayer.m_BackPageArray[i]; nLayerPageSide = BACKSIDE;
				}
		}
		if (nInstance > 1)
		{
			pos	= rFoldSheet.m_LayerList.GetHeadPosition();
			while (pos && (nInstance > 0) )
			{
				CFoldSheetLayer& rLayer	= rFoldSheet.m_LayerList.GetNext(pos);
				if (nLayerPageSide == FRONTSIDE)
				{
					for (int i = 0; i < rLayer.m_FrontPageArray.GetSize(); i++)
						if (rLayer.m_FrontPageArray[i] == nMinPage)
						{
							nLayerPageIndex = i; nInstance--; 
						}
				}
				else
				{
					for (int i = 0; i < rLayer.m_BackPageArray.GetSize(); i++)
						if (rLayer.m_BackPageArray[i] == nMinPage)
						{
							nLayerPageIndex = i; nInstance--; 
						}
				}
			}
			if (nInstance > 0)	// nth instance not found
				return NULL;
		}
	}

	if ( (nComponentRefIndex < 0) || (nComponentRefIndex >= m_FoldSheetLayerTurned.GetCount()) )
		return NULL;

	CLayoutSide* pLayoutSide;
	if (((nLayerPageSide == FRONTSIDE) && (m_FoldSheetLayerTurned[nComponentRefIndex])) ||
		((nLayerPageSide == BACKSIDE) && (!m_FoldSheetLayerTurned[nComponentRefIndex])))
		pLayoutSide = &m_BackSide;
	else
		pLayoutSide = &m_FrontSide;

	CLayoutObject* pRefPage = NULL;
	pos						= pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)	
	{
		CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
		if ((rObject.m_nLayerPageIndex == nLayerPageIndex) && (rObject.m_nComponentRefIndex == nComponentRefIndex))
		{
			pRefPage = &rObject;
			break;
		}
	}

	if ( ! pRefPage)
	{
		fRefLeft = fRefBottom = fRefRight = fRefTop = 0.0f;
		return NULL;
	}
///////////////////////////////////////////////////////////////////////////////////////////////

// Calculate area for section mark
	CLayoutObject* pRefBuddy = NULL;
	switch (pRefPage->m_nHeadPosition)			
	{
	case TOP:		
	case BOTTOM:	if (pRefPage->m_nHeadPosition == TOP)	// section mark position at head has higher priority -> so if head is TOP, first try upper neighbours, than lower
					{
						pRefBuddy = (pRefPage->m_UpperNeighbours.GetSize()) ? pRefPage->m_UpperNeighbours[0] : NULL;
						if (pRefBuddy) 
							if (pRefBuddy->m_nComponentRefIndex == pRefPage->m_nComponentRefIndex)
							{
								fRefBottom = pRefPage->GetTop(); fRefTop = pRefBuddy->GetBottom();
							}
							else
								pRefBuddy = NULL;
						if ( ! pRefBuddy)
						{
							pRefBuddy = (pRefPage->m_LowerNeighbours.GetSize()) ? pRefPage->m_LowerNeighbours[0] : NULL;
							if (pRefBuddy) 
								if (pRefBuddy->m_nComponentRefIndex == pRefPage->m_nComponentRefIndex)
								{
									fRefTop	= pRefPage->GetBottom(); fRefBottom = pRefBuddy->GetTop();
								}
								else
									pRefBuddy = NULL;
						}
					}
					else	// if head is BOTTOM, first try lower neighbours, than upper
					{
						pRefBuddy = (pRefPage->m_LowerNeighbours.GetSize()) ? pRefPage->m_LowerNeighbours[0] : NULL;
						if (pRefBuddy) 
							if (pRefBuddy->m_nComponentRefIndex == pRefPage->m_nComponentRefIndex)
							{
								fRefTop	= pRefPage->GetBottom(); fRefBottom = pRefBuddy->GetTop();
							}
							else
								pRefBuddy = NULL;
						if ( ! pRefBuddy)
						{
							pRefBuddy = (pRefPage->m_UpperNeighbours.GetSize()) ? pRefPage->m_UpperNeighbours[0] : NULL;
							if (pRefBuddy) 
								if (pRefBuddy->m_nComponentRefIndex == pRefPage->m_nComponentRefIndex)
								{
									fRefBottom = pRefPage->GetTop(); fRefTop = pRefBuddy->GetBottom();
								}
								else
									pRefBuddy = NULL;
						}
					}

					if (pRefBuddy)
					{
						fRefLeft   = min(pRefPage->GetLeft(),  pRefBuddy->GetLeft());
						fRefRight  = max(pRefPage->GetRight(), pRefBuddy->GetRight());
					}
					else
					{
						fRefLeft   = pRefPage->GetLeft();
						fRefRight  = pRefPage->GetRight();
						if (pRefPage->m_nHeadPosition == TOP)
							fRefTop	= fRefBottom = pRefPage->GetTop();
						else
							fRefTop	= fRefBottom = pRefPage->GetBottom();
						return pRefPage;
					}
					break;

	case LEFT:		
	case RIGHT:		if (pRefPage->m_nHeadPosition == LEFT)	// section mark position at head has higher priority -> so if head is LEFT, first try left neighbours, than roght
					{
						pRefBuddy = (pRefPage->m_LeftNeighbours.GetSize()) ? pRefPage->m_LeftNeighbours[0] : NULL;
						if (pRefBuddy)
							if (pRefBuddy->m_nComponentRefIndex == pRefPage->m_nComponentRefIndex)
							{
								fRefRight = pRefPage->GetLeft(); fRefLeft = pRefBuddy->GetRight();
							}
							else
								pRefBuddy = NULL;
					
						if ( ! pRefBuddy)
						{
							pRefBuddy = (pRefPage->m_RightNeighbours.GetSize()) ? pRefPage->m_RightNeighbours[0] : NULL;
							if (pRefBuddy)
								if (pRefBuddy->m_nComponentRefIndex == pRefPage->m_nComponentRefIndex)
								{
									fRefLeft = pRefPage->GetRight(); fRefRight = pRefBuddy->GetLeft();
								}
								else
									pRefBuddy = NULL;
						}
					}
					else	// if head is RIGHT, first try right neighbours, than left
					{
						pRefBuddy = (pRefPage->m_RightNeighbours.GetSize()) ? pRefPage->m_RightNeighbours[0] : NULL;
						if (pRefBuddy)
							if (pRefBuddy->m_nComponentRefIndex == pRefPage->m_nComponentRefIndex)
							{
								fRefLeft = pRefPage->GetRight(); fRefRight = pRefBuddy->GetLeft();
							}
							else
								pRefBuddy = NULL;
						if ( ! pRefBuddy)
						{
							pRefBuddy = (pRefPage->m_LeftNeighbours.GetSize()) ? pRefPage->m_LeftNeighbours[0] : NULL;
							if (pRefBuddy)
								if (pRefBuddy->m_nComponentRefIndex == pRefPage->m_nComponentRefIndex)
								{
									fRefRight = pRefPage->GetLeft(); fRefLeft = pRefBuddy->GetRight();
								}
								else
									pRefBuddy = NULL;
						}
					}

					if (pRefBuddy)
					{
						fRefBottom = min(pRefPage->GetBottom(), pRefBuddy->GetBottom());
						fRefTop    = max(pRefPage->GetTop(),	pRefBuddy->GetTop());
					}
					else
					{
						fRefBottom = pRefPage->GetBottom();
						fRefTop    = pRefPage->GetTop();
						if (pRefPage->m_nHeadPosition == LEFT)
							fRefLeft = fRefRight = pRefPage->GetLeft();
						else
							fRefLeft = fRefRight = pRefPage->GetRight();
						return pRefPage;
					}
					break;
	}

	return pRefPage;
}

int	CLayout::GetNumFlatProducts()
{
	CArray <BOOL, BOOL> componentRefs;

	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType != CLayoutObject::Page)
			continue;
		if (rLayoutObject.m_nLayerPageIndex >= 0)
			continue;
		if (rLayoutObject.m_nComponentRefIndex < 0)
			continue;

		componentRefs.SetAtGrow(rLayoutObject.m_nComponentRefIndex, TRUE);
	}

	int nNum = 0;
	for (int i = 0; i < componentRefs.GetSize(); i++)
	{
		if (componentRefs[i] == TRUE)
			nNum++;
	}

	return nNum;
}

 
// Calculate layout extents and origin in logical coordinates (inclusive WorldToLP())
CSize CLayout::GetExtents(CPrintSheet* pPrintSheet, int nSide, BOOL bShowMeasuring, CPoint* pPtOrg, BOOL bInsidePrintSheetView, CPressDevice* pPressDevice)
{
	CPrintSheetView*			pView			= (CPrintSheetView*	 )theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetMarksView*		pMarksView		= (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	CPrintSheetFrame*			pParentFrame	= (pView) ? ((CPrintSheetFrame*)pView->GetParentFrame()) : NULL;
	CDlgPDFOutputMediaSettings* pOutputMediaDlg = NULL;
	if (pView)
		if (pView->GetDlgOutputMedia())
			if (pView->GetDlgOutputMedia()->m_hWnd)
				pOutputMediaDlg = pView->GetDlgOutputMedia();

	if ((nSide == BOTHSIDES) && m_nProductType)	// work and turn / work and tumble / flat work
		nSide = FRONTSIDE;

	if (nSide == BOTHSIDES)
	{ 
		CPoint ptFrontOrg, ptBackOrg;
		CSize frontExtents = GetExtents(pPrintSheet, FRONTSIDE, bShowMeasuring, &ptFrontOrg, bInsidePrintSheetView, pPressDevice);
		CSize backExtents  = GetExtents(pPrintSheet, BACKSIDE,  bShowMeasuring, &ptBackOrg,	 bInsidePrintSheetView, pPressDevice);

		if (pPtOrg) 
			*pPtOrg = CPoint(ptFrontOrg.x, min(ptFrontOrg.y, ptBackOrg.y)); 

		int nMinY = min(ptFrontOrg.y, ptBackOrg.y);
		int nMaxY = max(ptFrontOrg.y + frontExtents.cy, ptBackOrg.y + backExtents.cy);
		return CSize(frontExtents.cx + CPrintSheetView::FRONT_BACK_GAP(bShowMeasuring) + backExtents.cx, nMaxY - nMinY);	
	}
	else
	{
		BOOL bShowPlate		= FALSE;
		BOOL bShowExposures = (pView) ? pView->ShowExposures() : FALSE;
		BOOL bShowBitmaps	= (pView) ? pView->ShowBitmap()    : FALSE;
		if (bInsidePrintSheetView)
			bShowPlate	= (pView) ? pView->ShowPlate() : FALSE;
		else
			bShowPlate = (pPressDevice) ? TRUE : FALSE;

		CLayoutSide& rLayoutSide = (nSide == FRONTSIDE) ? m_FrontSide : m_BackSide;

		float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop;
		rLayoutSide.CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, (pPressDevice) ? FALSE : TRUE, pPrintSheet, nSide);	// if we have a plate, don't exclude marks in order to get those exceeding the sheet
		fBBoxLeft	= min(fBBoxLeft,	rLayoutSide.m_fPaperLeft); 
		fBBoxBottom = min(fBBoxBottom,	rLayoutSide.m_fPaperBottom);
		fBBoxRight	= max(fBBoxRight,   rLayoutSide.m_fPaperRight);
		fBBoxTop	= max(fBBoxTop,		rLayoutSide.m_fPaperTop);
		if (bShowPlate)
		{
			if ( ! pPressDevice)
				pPressDevice = (nSide == FRONTSIDE) ? m_FrontSide.GetPressDevice() : m_BackSide.GetPressDevice();
			if (pPressDevice)
			{
				BOOL bShowPlateTab = FALSE;//(bShowExposures || bShowBitmaps) ? TRUE : FALSE;

				fBBoxLeft	= min(fBBoxLeft,   0.0f);	// plate position is 0/0
				fBBoxBottom = min(fBBoxBottom, 0.0f);
				fBBoxRight	= max(fBBoxRight,  pPressDevice->m_fPlateWidth);
				fBBoxTop	= max(fBBoxTop,	   (bShowPlateTab) ? pPressDevice->m_fPlateHeight + 40.0f : pPressDevice->m_fPlateHeight);
			}
		}

		if (pMarksView)	// take marks into account
		{
			float fMarksBBoxLeft = FLT_MAX, fMarksBBoxBottom = FLT_MAX, fMarksBBoxRight = -FLT_MAX, fMarksBBoxTop = -FLT_MAX;
			if (pParentFrame)
			{
				if (pParentFrame->IsOpenMarkSetDlg())
					pParentFrame->GetMarkSetDlg()->m_markList.GetBBoxAll(pPrintSheet, nSide, fMarksBBoxLeft, fMarksBBoxBottom, fMarksBBoxRight, fMarksBBoxTop);
				if (pParentFrame->IsOpenDlgNewMark())
					if (pParentFrame->GetDlgNewMark()->m_pMark)
						pParentFrame->GetDlgNewMark()->m_pMark->GetBBoxAll(pPrintSheet, nSide, fMarksBBoxLeft, fMarksBBoxBottom, fMarksBBoxRight, fMarksBBoxTop);
			}
			fBBoxLeft	= min(fBBoxLeft,	fMarksBBoxLeft);	
			fBBoxBottom = min(fBBoxBottom,	fMarksBBoxBottom);
			fBBoxRight	= max(fBBoxRight,   fMarksBBoxRight);
			fBBoxTop	= max(fBBoxTop,		fMarksBBoxTop);
		}

		if (pOutputMediaDlg)	// take output media into account
		{
			float fMediaBBoxLeft = FLT_MAX, fMediaBBoxBottom = FLT_MAX, fMediaBBoxRight = -FLT_MAX, fMediaBBoxTop = -FLT_MAX;
			pOutputMediaDlg->GetAllTilesBBox(pPrintSheet, nSide, fMediaBBoxLeft, fMediaBBoxBottom, fMediaBBoxRight, fMediaBBoxTop);
			fBBoxLeft	= min(fBBoxLeft,	fMediaBBoxLeft);	
			fBBoxBottom = min(fBBoxBottom,	fMediaBBoxBottom);
			fBBoxRight	= max(fBBoxRight,   fMediaBBoxRight);
			fBBoxTop	= max(fBBoxTop,		fMediaBBoxTop);
		}

		if (bShowMeasuring)
		{
			fBBoxLeft   -= 50.0f; fBBoxRight += 50.0f; 
			fBBoxBottom -= 50.0f; fBBoxTop   += 50.0f; 
		}

		if (pPtOrg) 
			*pPtOrg = CPoint(CPrintSheetView::WorldToLP(fBBoxLeft), CPrintSheetView::WorldToLP(fBBoxBottom)); 
		return CSize(CPrintSheetView::WorldToLP(fBBoxRight - fBBoxLeft), CPrintSheetView::WorldToLP(fBBoxTop - fBBoxBottom));
	}
}


void CLayout::UpdateMarkTemplateLinks(short nOldMarkTemplateIndex, short nNewMarkTemplateIndex)
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if ( ! rLayoutObject.m_nPublicFlag)
			if (rLayoutObject.m_nMarkTemplateIndex == nOldMarkTemplateIndex)
			{
				rLayoutObject.m_nMarkTemplateIndex = nNewMarkTemplateIndex;
				rLayoutObject.m_nPublicFlag = 1;
			}
	}

	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_BackSide.m_ObjectList.GetNext(pos);
		if ( ! rLayoutObject.m_nPublicFlag)
			if (rLayoutObject.m_nMarkTemplateIndex == nOldMarkTemplateIndex)
			{
				rLayoutObject.m_nMarkTemplateIndex = nNewMarkTemplateIndex;
				rLayoutObject.m_nPublicFlag = 1;
			}
	}
}


void CLayout::ExtractMarkTemplates(CPageTemplateList& rTargetList, CPageTemplateList& rSourceList)
{
	rTargetList.RemoveAll();
	m_FrontSide.m_ObjectList.ResetPublicFlags();
	m_BackSide.m_ObjectList.ResetPublicFlags();

	short	 nOldMarkTemplateIndex = 0;
	short	 nNewMarkTemplateIndex = 0;
	POSITION pos				   = rSourceList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = rSourceList.CList::GetNext(pos);
		if (MarkTemplateExists(nOldMarkTemplateIndex))
		{
			rTargetList.AddTail(rTemplate);
			UpdateMarkTemplateLinks(nOldMarkTemplateIndex, nNewMarkTemplateIndex);
			nNewMarkTemplateIndex++;
		}
		nOldMarkTemplateIndex++;
	}

	m_FrontSide.m_ObjectList.ResetPublicFlags();
	m_BackSide.m_ObjectList.ResetPublicFlags();
}


BOOL CLayout::MarkTemplateExists(short nMarkTemplateIndex)
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		if (m_FrontSide.m_ObjectList.GetNext(pos).m_nMarkTemplateIndex == nMarkTemplateIndex)
			return TRUE;
	}
	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		if (m_BackSide.m_ObjectList.GetNext(pos).m_nMarkTemplateIndex == nMarkTemplateIndex)
			return TRUE;
	}
	return FALSE;
}


CLayoutObject* CLayout::FindSimilarObject(CLayoutObject* pLayoutObject)
{
	if (!pLayoutObject)
		return NULL;

	CLayoutSide& rLayoutSide = (pLayoutObject->GetLayoutSideIndex() == FRONTSIDE) ? m_FrontSide : m_BackSide;

	POSITION pos = rLayoutSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rTestObject = rLayoutSide.m_ObjectList.GetNext(pos);
		if (rTestObject.IsSimilar(*pLayoutObject))
			return &rTestObject;
	}

	return NULL;
}

int CLayout::RemoveMarkObjects(int nMarkType, BOOL bRemoveAll)
{
	int nRemoved = m_FrontSide.RemoveMarkObjects(nMarkType, bRemoveAll);
	return (nRemoved + m_BackSide.RemoveMarkObjects(nMarkType, bRemoveAll));
}

void CLayout::ResetMarkInstanceDefs(const CString strMarkName)
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::ControlMark)
			continue;

		if (strMarkName.IsEmpty())
			rLayoutObject.m_markInstanceDefs.RemoveAll();
		else
			for (int i = 0; i < rLayoutObject.m_markInstanceDefs.GetSize(); i++)
			{
				if (rLayoutObject.m_markInstanceDefs[i].m_strParentMark == strMarkName)
				{
					rLayoutObject.m_markInstanceDefs.RemoveAt(i); i--;
				}
			}
	}

	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_BackSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::ControlMark)
			continue;

		if (strMarkName.IsEmpty())
			rLayoutObject.m_markInstanceDefs.RemoveAll();
		else
			for (int i = 0; i < rLayoutObject.m_markInstanceDefs.GetSize(); i++)
			{
				if (rLayoutObject.m_markInstanceDefs[i].m_strParentMark == strMarkName)
				{
					rLayoutObject.m_markInstanceDefs.RemoveAt(i); i--;
				}
			}
	}
}

void CLayout::RemoveFlatProductType(int nRefIndex)
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		POSITION	   prevPos = pos;
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if ( ! rLayoutObject.IsFlatProduct())
			continue;
		if (rLayoutObject.m_nComponentRefIndex == nRefIndex)
			m_FrontSide.m_ObjectList.RemoveAt(prevPos);
		else
			if (rLayoutObject.m_nComponentRefIndex > nRefIndex)
				rLayoutObject.m_nComponentRefIndex--;
	}

	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		POSITION	   prevPos = pos;
		CLayoutObject& rLayoutObject = m_BackSide.m_ObjectList.GetNext(pos);
		if ( ! rLayoutObject.IsFlatProduct())
			continue;
		if (rLayoutObject.m_nComponentRefIndex == nRefIndex)
			m_BackSide.m_ObjectList.RemoveAt(prevPos);
		else
			if (rLayoutObject.m_nComponentRefIndex > nRefIndex)
				rLayoutObject.m_nComponentRefIndex--;
	}
}

void CLayout::CheckLayoutObjectFormats(int nRefIndex, float fWidth, float fHeight, const CString strFormatName, int nOrientation, BOOL bTypeLabel)
{
	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	if ( ! pPrintSheet)
		return;

	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (bTypeLabel)
			if ( ! rLayoutObject.IsFlatProduct())
				continue;

		int nIndex = (bTypeLabel) ? pPrintSheet->m_flatProductRefs[rLayoutObject.m_nComponentRefIndex].m_nFlatProductIndex : rLayoutObject.m_nComponentRefIndex;
		if (nIndex == nRefIndex)
			if ( (fabs(rLayoutObject.GetRealWidth() - fWidth) > 0.01f) || (fabs(rLayoutObject.GetRealHeight() - fHeight) > 0.01f) )
			{
				BOOL bRotate = ( (nOrientation == -1) && ( (rLayoutObject.m_nHeadPosition == LEFT) || (rLayoutObject.m_nHeadPosition == RIGHT)) ) ? TRUE : FALSE;
				rLayoutObject.ModifySize(fWidth, fHeight, pPrintSheet, bRotate);
				if (nOrientation != -1)
					rLayoutObject.m_nObjectOrientation = nOrientation;
				if (strFormatName.GetLength())
					rLayoutObject.m_strFormatName = strFormatName;
		}
	}

	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_BackSide.m_ObjectList.GetNext(pos);
		if (bTypeLabel)
			if ( ! rLayoutObject.IsFlatProduct())
				continue;

		int nIndex = (bTypeLabel) ? pPrintSheet->m_flatProductRefs[rLayoutObject.m_nComponentRefIndex].m_nFlatProductIndex : rLayoutObject.m_nComponentRefIndex;
		if (nIndex == nRefIndex)
			if ( (fabs(rLayoutObject.GetRealWidth() - fWidth) > 0.01f) || (fabs(rLayoutObject.GetRealHeight() - fHeight) > 0.01f) )
			{
				BOOL bRotate = ( (nOrientation == -1) && ( (rLayoutObject.m_nHeadPosition == LEFT) || (rLayoutObject.m_nHeadPosition == RIGHT)) ) ? TRUE : FALSE;
				rLayoutObject.ModifySize(fWidth, fHeight, pPrintSheet, bRotate);
				if (nOrientation != -1)
					rLayoutObject.m_nObjectOrientation = nOrientation;
				if (strFormatName.GetLength())
					rLayoutObject.m_strFormatName = strFormatName;
			}
	}
}

void CLayout::SetPrintSheetDefaultPlates()
{
	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	while (pPrintSheet)
	{
		if (m_FrontSide.m_DefaultPlates.GetCount())
			pPrintSheet->m_FrontSidePlates.m_defaultPlate = m_FrontSide.m_DefaultPlates.GetHead();
		if (m_BackSide.m_DefaultPlates.GetCount())
			pPrintSheet->m_BackSidePlates.m_defaultPlate  = m_BackSide.m_DefaultPlates.GetHead();

		pPrintSheet = GetNextPrintSheet(pPrintSheet);
	}
}

void CLayout::RotateLayoutObjects(BOOL bClockwise)
{
	CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	if ( ! m_FrontSide.GetSelectedObjectRefList(pView->m_DisplayList, &objectRefList))
		if ( ! m_BackSide.GetSelectedObjectRefList(pView->m_DisplayList, &objectRefList))
			return;

	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	// Calculate bounding box 
	float fBBoxLeft = FLT_MAX, fBBoxBottom = FLT_MAX, fBBoxRight = -FLT_MAX, fBBoxTop = -FLT_MAX;
	for (int i = 0; i < objectRefList.GetSize(); i++)
	{
		CLayoutObject* pObj = objectRefList[i].m_pLayoutObject;
		if ( ! pObj)
			continue;

		float fLeft, fBottom, fRight, fTop;
		pObj->GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
		fBBoxLeft	= min(fBBoxLeft,   fLeft);
		fBBoxBottom = min(fBBoxBottom, fBottom);
		fBBoxRight	= max(fBBoxRight,  fRight);
		fBBoxTop	= max(fBBoxTop,	   fTop);
	}

	float fBBoxCenterX = fBBoxLeft   + (fBBoxRight - fBBoxLeft)  /2.0f;
	float fBBoxCenterY = fBBoxBottom + (fBBoxTop   - fBBoxBottom)/2.0f;
	for (int i = 0; i < objectRefList.GetSize(); i++)
	{
		CLayoutObject* pObj = objectRefList[i].m_pLayoutObject;
		if ( ! pObj)
			continue;

		CLayoutObject* pCounterPartObj = pObj->FindCounterpartObject();
		float		   fTemp;

		fTemp = pObj->GetWidth(); pObj->SetWidth(pObj->GetHeight()); pObj->SetHeight(fTemp);
		if (bClockwise)
		{
			pObj->SetRight(fBBoxCenterX + (pObj->GetTop()  - fBBoxCenterY));
			pObj->SetTop  (fBBoxCenterY - (pObj->GetLeft() - fBBoxCenterX));
			pObj->SetLeft  (pObj->GetRight() - pObj->GetWidth());
			pObj->SetBottom(pObj->GetTop()   - pObj->GetHeight());
			switch (pObj->m_nHeadPosition)
			{
			case LEFT:		pObj->m_nHeadPosition = TOP;	break;
			case RIGHT:		pObj->m_nHeadPosition = BOTTOM;	break;
			case BOTTOM:	pObj->m_nHeadPosition = LEFT;	break;
			case TOP:		pObj->m_nHeadPosition = RIGHT;	break;
			}
		}
		else
		{
			pObj->SetBottom(fBBoxCenterY + pObj->GetLeft() - fBBoxCenterX);
			pObj->SetLeft  (fBBoxCenterX - pObj->GetTop()  + fBBoxCenterY);
			pObj->SetRight(pObj->GetLeft()	 + pObj->GetWidth());
			pObj->SetTop  (pObj->GetBottom() + pObj->GetHeight());
			switch (pObj->m_nHeadPosition)
			{
			case LEFT:		pObj->m_nHeadPosition = BOTTOM; break;
			case RIGHT:		pObj->m_nHeadPosition = TOP;	break;
			case BOTTOM:	pObj->m_nHeadPosition = RIGHT;	break;
			case TOP:		pObj->m_nHeadPosition = LEFT;	break;
			}
		}

		if (pCounterPartObj)
			pCounterPartObj->AlignToCounterPart(pObj);
	}

	AnalyzeMarks();
}

CSize CLayout::FindFoldSheetMatrix(float fFoldSheetWidth, float fFoldSheetHeight, CPressDevice* pPressDevice, BOOL& bRotate)
{
	float fPrintingAreaWidth  = m_FrontSide.m_fPaperWidth;
	float fPrintingAreaHeight = (pPressDevice->m_nPressType == CPressDevice::SheetPress) ? m_FrontSide.m_fPaperHeight - pPressDevice->m_fGripper : m_FrontSide.m_fPaperHeight;

	int nx = (int)(fPrintingAreaWidth  / fFoldSheetWidth);
	int ny = (int)(fPrintingAreaHeight / fFoldSheetHeight);
	int nNumFoldSheets = nx * ny;

	int nxRot = (int)(fPrintingAreaWidth  / fFoldSheetHeight);
	int nyRot = (int)(fPrintingAreaHeight / fFoldSheetWidth);
	int nNumFoldSheetsRot = nxRot * nyRot;

	if (nNumFoldSheetsRot > nNumFoldSheets)
	{
		bRotate = TRUE;
		return CSize(nxRot, nyRot);
	}
	else
	{
		bRotate = FALSE;
		return CSize(nx, ny);
	}
}

void CLayout::AddFoldSheetLayer(CFoldSheet* pFoldSheet, int nLayerIndex, float fFoldSheetPosX, float fFoldSheetPosY, CBoundProductPart& rProductPart, int nOrientation, CPrintSheet* pDefaultPrintSheet)
{
	CFoldSheetLayer* pLayer = pFoldSheet->GetLayer(nLayerIndex);
	if ( ! pLayer)
		return;
	if ( ! pLayer->m_HeadPositionArray.GetSize())
		return;
	if ( (fabs(rProductPart.m_fPageWidth) < 0.1f) || (fabs(rProductPart.m_fPageHeight) < 0.1f) )
		return;

	CString strPage;
	int		nPageIndex = -1;
	int		nComponentRefIndex = m_FoldSheetLayerRotation.GetSize();
	int		nNumX = pLayer->m_nPagesX;
	int		nNumY = pLayer->m_nPagesY;
	float	fLeft = fFoldSheetPosX, fBottom = fFoldSheetPosY;
	for (int ny = 0; ny < nNumY; ny++)
	{
		float fPageRowHeight = 0.0f;
		for (int nx = 0; nx < nNumX; nx++)
		{
			nPageIndex++;

			float fPageBoxWidth, fPageBoxHeight, fPageLeft, fPageBottom, fPageRight, fPageTop;
			pFoldSheet->GetPageCutBox(pLayer, nPageIndex, rProductPart, fPageBoxWidth, fPageBoxHeight, fPageLeft, fPageBottom, fPageRight, fPageTop);

			float fPageWidth  = rProductPart.m_fPageWidth;
			float fPageHeight = rProductPart.m_fPageHeight;
			if ( (pLayer->m_HeadPositionArray[nPageIndex] == LEFT) || (pLayer->m_HeadPositionArray[nPageIndex] == RIGHT) )
			{
				float fTemp = fPageWidth; fPageWidth = fPageHeight; fPageHeight = fTemp;
			}

			///// frontside
			strPage.Format(_T("%d"), pLayer->m_FrontPageArray[nPageIndex]);
			CLayoutObject layoutObjectFront;
			layoutObjectFront.CreatePage(fLeft + fPageLeft, fBottom + fPageBottom, fPageWidth, fPageHeight, strPage, rProductPart.m_strFormatName, rProductPart.m_nPageOrientation, *pLayer, nPageIndex, nComponentRefIndex, FRONTSIDE);

			m_FrontSide.m_ObjectList.AddTail(layoutObjectFront);

			///// backside
			strPage.Format(_T("%d"), pLayer->m_BackPageArray[nPageIndex]);
			CLayoutObject layoutObjectBack;
			layoutObjectBack.CreatePage(fLeft + fPageLeft, fBottom + fPageBottom, fPageWidth, fPageHeight, strPage, rProductPart.m_strFormatName, rProductPart.m_nPageOrientation, *pLayer, nPageIndex, nComponentRefIndex, BACKSIDE);

			m_BackSide.m_ObjectList.AddTail(layoutObjectBack);
			m_BackSide.m_ObjectList.GetTail().AlignToCounterPart(&m_FrontSide.m_ObjectList.GetTail());

			fLeft += fPageBoxWidth;
			fPageRowHeight = max(fPageRowHeight, fPageBoxHeight);
		}

		fLeft	 = fFoldSheetPosX;
		fBottom += fPageRowHeight;
	}

	m_FoldSheetLayerRotation.Add(0);
	m_FoldSheetLayerTurned.Add(FALSE);

	switch (nOrientation)
	{
	case ROTATE0:	break;

	case ROTATE90:	RotateFoldSheet90(nComponentRefIndex, FALSE, pDefaultPrintSheet);	break;

	case ROTATE180: RotateFoldSheet90(nComponentRefIndex, FALSE, pDefaultPrintSheet); 
					RotateFoldSheet90(nComponentRefIndex, FALSE, pDefaultPrintSheet);	break;

	case ROTATE270: RotateFoldSheet90(nComponentRefIndex, TRUE,  pDefaultPrintSheet);	break;

	case FLIP0:		TurnFoldSheet	 (nComponentRefIndex,		 pDefaultPrintSheet);
					RotateFoldSheet90(nComponentRefIndex, FALSE, pDefaultPrintSheet);
					RotateFoldSheet90(nComponentRefIndex, FALSE, pDefaultPrintSheet);	break;

	case FLIP90:	RotateFoldSheet90(nComponentRefIndex, FALSE, pDefaultPrintSheet);
					TurnFoldSheet	 (nComponentRefIndex,		 pDefaultPrintSheet);
					RotateFoldSheet90(nComponentRefIndex, FALSE, pDefaultPrintSheet);
					RotateFoldSheet90(nComponentRefIndex, FALSE, pDefaultPrintSheet);	break;

	case FLIP180:	TurnFoldSheet	 (nComponentRefIndex,		 pDefaultPrintSheet);	break;

	case FLIP270:	RotateFoldSheet90(nComponentRefIndex, FALSE, pDefaultPrintSheet);
					TurnFoldSheet	 (nComponentRefIndex,		 pDefaultPrintSheet);	break;
	}

	m_FrontSide.FindCutBlocks();
	m_FrontSide.AnalyseCutblocks();
	m_BackSide.FindCutBlocks();
	m_BackSide.AnalyseCutblocks();
}

CString CLayout::GetPrintingType()
{
	CString string;
	switch(m_nProductType)
	{
	case WORK_AND_TURN:		return theApp.settings.m_szWorkAndTurnName;		break;
	case WORK_AND_TUMBLE:	return theApp.settings.m_szWorkAndTumbleName;	break;
	case WORK_SINGLE_SIDE:	return theApp.settings.m_szSingleSidedName;		break;
	default:				string.LoadString(IDS_WORK_AND_BACK);			break;
	}
	return string;
}

CString	CLayout::GetTurningType()
{
	CString string;
	switch (KindOfProduction())
	{
	case WORK_AND_TURN:		string.LoadString(IDS_TURN);	break;
	case WORK_AND_TUMBLE:	string.LoadString(IDS_TUMBLE);	break;
	}
	return string;
}

CRect CLayout::GetDrawingBox(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice, int nSide, BOOL bShowMeasures, BOOL bInsidePrintSheetView)
{
	if ( ! pDC)
		return CRect(0,0,0,0);

	rcFrame.InflateRect(5,5);

	long nExtentX = (pPressDevice) ? CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth)  : CPrintSheetView::WorldToLP(m_FrontSide.m_fPaperWidth);
	long nExtentY = (pPressDevice) ? CPrintSheetView::WorldToLP(pPressDevice->m_fPlateHeight) : CPrintSheetView::WorldToLP(m_FrontSide.m_fPaperHeight);
	long nXOrg	   = (pPressDevice) ? CPrintSheetView::WorldToLP(0.0f) : CPrintSheetView::WorldToLP(m_FrontSide.m_fPaperLeft);
	long nYOrg	   = (pPressDevice) ? CPrintSheetView::WorldToLP(0.0f) : CPrintSheetView::WorldToLP(m_FrontSide.m_fPaperBottom);
	if (1)//( (nExtentX <= 0) || (nExtentY <= 0) )
	{
		CSize szExtents;
		szExtents = GetExtents(pPrintSheet, nSide, bShowMeasures, 0, bInsidePrintSheetView, pPressDevice);
		nExtentX = szExtents.cx; 
		nExtentY = szExtents.cy;
	}
	if ( (nExtentX <= 0) || (nExtentY <= 0) )
		return CRect(0,0,0,0);

	CRect rcViewPort = rcFrame;
	//if (nSide == BOTHSIDES)
	//	rcViewPort.right = rcFrame.CenterPoint().x - 5;

	rcViewPort.DeflateRect(5, 5);

	CDC dc;
	dc.CreateCompatibleDC(pDC);

	dc.SetMapMode(MM_ISOTROPIC); 
	dc.SetWindowExt(nExtentX, nExtentY);
	dc.SetWindowOrg(nXOrg,	nExtentY + nYOrg);
	dc.SetViewportExt(rcViewPort.Width(), -rcViewPort.Height());
	dc.SetViewportOrg(rcViewPort.left,	 rcViewPort.top);

	CRect rcBox = CRect(CPoint(nXOrg, nYOrg), CSize(nExtentX, nExtentY));
	dc.LPtoDP(&rcBox);
	rcBox.NormalizeRect();

	return rcBox;
}

CRect CLayout::GetDrawingBox(CWnd* pWnd, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice, int nSide, BOOL bShowMeasures, BOOL bInsidePrintSheetView)
{
	if ( ! pWnd)
		return CRect(0,0,0,0);

	CDC* pDC = pWnd->GetWindowDC();
	CRect rcBox = GetDrawingBox(pDC, rcFrame, pPrintSheet, pPressDevice, nSide, bShowMeasures, bInsidePrintSheetView);
	pWnd->ReleaseDC(pDC);

	return rcBox;
}

void CLayout::DrawHeader(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice, BOOL bShort)
{
	CFont	smallFont;
	smallFont.CreateFont (Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);
	CString	strOut, strWidth, strHeight;
	int		nTextHeight = Pixel2MM(pDC, 15);
	int		nRowSpace  = Pixel2MM(pDC, 25);
	CRect	rcTitle1, rcText1, rcTitle2, rcText2, rcTitle3, rcText3, rcTitle4, rcText4, rcTitle5, rcText5;
	int		nXStart	= rcFrame.left + Pixel2MM(pDC, 10);
	int		nYStart	= rcFrame.top + Pixel2MM(pDC, 5);
	
	rcTitle1.left	= nXStart;  rcTitle1.top = nYStart;
	rcTitle1.right	= rcTitle1.left + Pixel2MM(pDC, 70); rcTitle1.bottom = rcTitle1.top + nTextHeight;
	rcText1 = rcTitle1; rcText1.left = rcTitle1.right + Pixel2MM(pDC, 5); rcText1.right = rcText1.left + Pixel2MM(pDC, 110);

	rcTitle2.left	= rcText1.right + Pixel2MM(pDC, 20);  rcTitle2.top = nYStart;
	rcTitle2.right	= rcTitle2.left + Pixel2MM(pDC, 70); rcTitle2.bottom = rcTitle2.top + nTextHeight;
	rcText2 = rcTitle2; rcText2.left = rcTitle2.right + Pixel2MM(pDC, 5); rcText2.right = rcText2.left + Pixel2MM(pDC, 100);
	
	rcTitle3.left	= rcText2.right + Pixel2MM(pDC, 20);  rcTitle3.top = nYStart;
	rcTitle3.right	= rcTitle3.left + Pixel2MM(pDC, 80); rcTitle3.bottom = rcTitle3.top + nTextHeight;
	rcText3 = rcTitle2; rcText3.left = rcTitle3.right + Pixel2MM(pDC, 5); rcText3.right = rcFrame.right;

	if (pPrintSheet)
	{
		strOut.LoadString(IDS_PRINTSHEET_TITLE); strOut += _T(":");
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
		pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
		pDC->SetTextColor(GUICOLOR_VALUE);
		strOut = pPrintSheet->GetNumber();
		pDC->DrawText(strOut, rcText1,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	}
	else
	{
		int nOffset2 = rcTitle2.left - rcTitle1.left;
		int nOffset3 = rcTitle3.left - rcTitle2.left;
		rcTitle2.OffsetRect(-nOffset2, 0); rcText2.OffsetRect(-nOffset2, 0);
		rcTitle3.OffsetRect(-nOffset3, 0); rcText3.OffsetRect(-nOffset3, 0);
	}

	strOut.LoadString(IDS_PRINTSHEETLAYOUT); strOut += _T(":");
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle2, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(GUICOLOR_VALUE);
	pDC->DrawText(m_strLayoutName, rcText2,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	if (pPressDevice)
	{
		strOut.LoadString(IDS_PRESS); strOut += _T(":");
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
		pDC->DrawText(strOut, rcTitle3, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
		pDC->SetTextColor(GUICOLOR_VALUE);
		pDC->DrawText(pPressDevice->m_strName, rcText3,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	}

	
	if ( ! bShort)
	{
		rcTitle1.OffsetRect(0, nTextHeight);
		rcText1.OffsetRect (0, nTextHeight);

		rcTitle2.OffsetRect(0, nTextHeight);
		rcText2.OffsetRect (0, nTextHeight);

		strOut.LoadString(IDS_PAPERSIZE); strOut += _T(":");
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
		pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
		pDC->SetTextColor(GUICOLOR_VALUE);
		CPrintSheetView::TruncateNumber(strWidth, m_FrontSide.m_fPaperWidth); CPrintSheetView::TruncateNumber(strHeight, m_FrontSide.m_fPaperHeight, TRUE);
		strOut.Format(_T("%s x %s"), strWidth, strHeight);
		pDC->DrawText(strOut, rcText1, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

		if (pPrintSheet->GetMaxProductQuantity() > 0)
		{
			int nNumSheets = pPrintSheet->m_nQuantityTotal - pPrintSheet->m_nQuantityExtra;
			CString strQuantity; strQuantity.LoadString(IDS_QUANTITY); strQuantity += _T(":");
			pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
			pDC->DrawText(strQuantity, rcTitle2, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
			strOut.Format(_T("%d"), nNumSheets);
			pDC->SetTextColor(GUICOLOR_VALUE);
			pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		}
	}
}

CRect CLayout::DrawData(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPrintComponent* pComponent, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;
	if ( ! pPrintSheet)
		return rcBox;

	CPrintGroup& rPrintGroup = GetPrintGroup();

	int		nTextHeight = 15;
	int		nIndent		= 32;
	CString strOut;

	CFont boldFont, font;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT); 

	rcFrame.top  += 5;
	rcFrame.left += 5; rcFrame.right -= 5;
	CRect rcText = rcFrame; rcText.top += 2; rcText.bottom = rcText.top + nTextHeight;

	int nNumSheets	  = GetNumPrintSheetsBasedOn();
	int nMaxNumSheets = GetMaxNumPrintSheetsBasedOn();

	CRect rcImg; rcImg.left = rcFrame.left; rcImg.right = rcImg.left + 22; rcImg.top = rcFrame.top + 7; rcImg.bottom = rcImg.top + 35;
	if (nNumSheets > 1)
	{
		rcImg.DeflateRect(1,1);
		rcImg.OffsetRect(1,1);
		Draw(pDC, rcImg, pPrintSheet, NULL, FALSE, FALSE, FALSE, NULL, TRUE);
		rcImg.OffsetRect(-2,-2);
		Draw(pDC, rcImg, pPrintSheet, NULL, FALSE, FALSE, FALSE, NULL, TRUE);
	}
	else
		Draw(pDC, rcImg, pPrintSheet, NULL, FALSE, FALSE, FALSE, NULL, TRUE);

	rcText.left = rcFrame.left + nIndent;

	pDC->SelectObject(&boldFont);
	pDC->SetTextColor(BLACK);
	CString strNewLayout; strNewLayout.LoadString(IDS_NEW_LAYOUT);
	strOut = ( ! pPrintSheet->IsEmpty()) ? m_strLayoutName : strNewLayout;
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	rcBox.bottom += 5;

	int nTab = 60;

	pDC->SetTextColor(GUICOLOR_CAPTION);
	rcText.OffsetRect(0, 10);
	CRect rcText2 = rcText; rcText2.left += nTab + Pixel2MM(pDC, 5);

	pDC->SelectObject(&font);

	rcFrame.top = rcBox.bottom + 5;
	if (nMaxNumSheets > 1)
	{
		CString strNumSheets; strNumSheets.LoadString(IDS_COUNT_TEXT); strOut.LoadString(IDS_SHEETS); strNumSheets += _T(" ") + strOut + _T(":");
		rcText.OffsetRect(0, nTextHeight + 2); rcText2.OffsetRect(0, nTextHeight + 2);
		pDC->SetTextColor(DARKGRAY);
		DrawTextMeasured(pDC, strNumSheets, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

		strOut.Format(_T("%d"), nNumSheets);
		pDC->SelectObject(&boldFont);
		pDC->SetTextColor(RGB(255,100,15));
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
		
		pDC->SelectObject(&font);
		if (pDisplayList)
		{
			CRect rcDI = rcText; rcDI.left -= 5; rcDI.top -= 2; rcDI.bottom += 2;
			CRect rcDIAux = rcDI; rcDIAux.left = rcText2.left - 3; rcDIAux.right = rcDIAux.left + pDC->GetTextExtent(_T("99999")).cx + 5;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDIAux, (void*)pPrintSheet, DISPLAY_ITEM_REGISTRY(CLayoutDataWindow, LayoutNumSheets), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();

				CGraphicComponent	gc;
				CImage				img;
				COLORREF			crColor = RGB(250, 230, 200);
				CString				strMaxNum; strMaxNum.Format(_T("%d"), GetMaxNumPrintSheetsBasedOn());

				CRect rcImg = pDI->m_BoundingRect; rcImg.left = pDI->m_AuxRect.right + 3; rcImg.right = rcImg.right = rcImg.left + pDC->GetTextExtent(strMaxNum).cx + 10; rcImg.bottom -= 2;
				gc.DrawTitleBar(pDC, rcImg, _T("1"), crColor, crColor, 0, 1);

				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MINUS_1));
				rcImg.OffsetRect(23, 0);
				img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top + 2, img.GetWidth(), img.GetHeight(), WHITE);

				img.Destroy();
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_PLUS_1));
				rcImg.OffsetRect(16, 0);
				img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top + 2, img.GetWidth(), img.GetHeight(), WHITE);

				rcImg.OffsetRect(21, 0); rcImg.right = rcImg.left + pDC->GetTextExtent(strMaxNum).cx + 10;
				gc.DrawTitleBar(pDC, rcImg, strMaxNum, crColor, crColor, 0, 1);
			}
		}
	}

	rcText.OffsetRect(0, 5); rcText2.OffsetRect(0, 5);

	float fUtilization = CalcUtilization() * 100.0f;
	if (fUtilization > 0.0f)
	{
		CString strCoverage; strCoverage.LoadString(IDS_COVERAGE);	strCoverage += _T(":");
		strOut.Format(_T("%.1f %%"), fUtilization);
		
		rcText.OffsetRect(0, nTextHeight + 2); rcText2.OffsetRect(0, nTextHeight + 2);
		pDC->SetTextColor(DARKGRAY);
		DrawTextMeasured(pDC, strCoverage, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
		pDC->SetTextColor(BLACK);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
	}
	
	int nNumFrontColors = pPrintSheet->GetNumColors(FRONTSIDE);
	int nNumBackColors  = pPrintSheet->GetNumColors(BACKSIDE);
	if ( (nNumFrontColors > 0) || (nNumBackColors > 0) )
	{
		CString strColors; strColors.LoadString(IDS_PLATECOLOR_STRING); strColors	+= _T(":");
		rcText.OffsetRect(0, nTextHeight + 2); rcText2.OffsetRect(0, nTextHeight + 2);
		pDC->SetTextColor(DARKGRAY);
		DrawTextMeasured(pDC, strColors, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
		strOut.Format(_T("%d/%d"), nNumFrontColors, nNumBackColors);
		pDC->SetTextColor(BLACK);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

		CPrintSheetViewStrippingData* pView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
		BOOL bExpanded = (pView) ? pView->m_layoutDataWindow.m_bColorsExpanded : FALSE;
		if (pDisplayList)
		{
			CRect rcDI = rcText; rcDI.left -= 15; rcDI.right = rcFrame.right - 5; rcDI.top -= 4; rcDI.bottom += 2;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pPrintSheet, DISPLAY_ITEM_REGISTRY(CLayoutDataWindow, ExpandColors), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				CImage img; img.LoadFromResource(theApp.m_hInstance, (bExpanded) ? IDB_COLLAPSE : IDB_EXPAND);		
				if ( ! img.IsNull())
				{
					CRect rcImg = rcText; rcImg.left -= 10;rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcText.CenterPoint().y - 4; rcImg.bottom = rcImg.top + img.GetHeight();
					img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
				}
			}
		}
		if (bExpanded)
		{
			CRect rcColors = rcFrame; rcColors.left = rcText2.left; rcColors.top = rcBox.bottom + 5;
			rcColors = DrawColorList(pDC, rcColors, pPrintSheet, BOTHSIDES, pDisplayList);
			rcBox.UnionRect(rcBox, rcColors);
			int nOffset = rcBox.bottom - rcText.top - nTextHeight;
			rcText.OffsetRect(0, nOffset); rcText2.OffsetRect(0, nOffset);
		}
	}
	
	if (pPrintSheet->GetMaxProductQuantity() > 0)
	{
		int nNumSheets = pPrintSheet->m_nQuantityTotal - pPrintSheet->m_nQuantityExtra;
		CString strQuantity; strQuantity.LoadString(IDS_QUANTITY); strQuantity += _T(":");
		rcText.OffsetRect(0, nTextHeight + 2); rcText2.OffsetRect(0, nTextHeight + 2);
		pDC->SetTextColor(DARKGRAY);
		DrawTextMeasured(pDC, strQuantity, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
		strOut.Format(_T("%d"), nNumSheets);
		pDC->SetTextColor(BLACK);
		DrawTextMeasured(pDC, strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

		if (pDisplayList)
		{
			CRect rcDI = rcText; rcDI.left -= 5; rcDI.top -= 2; rcDI.bottom += 2;
			CRect rcDIAux = rcDI; rcDIAux.left = rcText2.left - 3;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDIAux, (void*)pPrintSheet, DISPLAY_ITEM_REGISTRY(CLayoutDataWindow, PrintSheetQuantity), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				//if (pDI->m_nState == CDisplayItem::Selected)
				//{
				//	CRect rcFeedback = pDI->m_BoundingRect;
				//	rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				//	CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
				//}
			}
		}
	}

	rcBox.bottom += 8;

	boldFont.DeleteObject();
	font.DeleteObject();

	return rcBox;
}

CRect CLayout::DrawColorList(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, int nSide, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CRect rcText = rcFrame; rcText.top += 3; rcText.bottom = rcText.top + nTextHeight;

	CColorantList colorantList, colorantList2;

	CPlate* pPlate = NULL;
	CStringArray strSeps;
	switch (nSide)
	{
	case FRONTSIDE:	pPlate = pPrintSheet->m_FrontSidePlates.GetTopmostPlate(); 
					if (pPlate)
					{
						strSeps.Append(pPlate->m_processColors);
						strSeps.Append(pPlate->m_spotColors);
						colorantList.AddSeparations(strSeps);
					}
					break;
	case BACKSIDE:	pPlate = pPrintSheet->m_BackSidePlates.GetTopmostPlate(); 
					if (pPlate)
					{
						strSeps.Append(pPlate->m_processColors);
						strSeps.Append(pPlate->m_spotColors);
						colorantList2.AddSeparations(strSeps);
					}
					break;
	case BOTHSIDES:	pPlate = pPrintSheet->m_FrontSidePlates.GetTopmostPlate(); 
					if (pPlate)
					{
						strSeps.Append(pPlate->m_processColors);
						strSeps.Append(pPlate->m_spotColors);
						colorantList.AddSeparations(strSeps);
					}
					pPlate = pPrintSheet->m_BackSidePlates.GetTopmostPlate(); 
					if (pPlate)
					{
						strSeps.Append(pPlate->m_processColors);
						strSeps.Append(pPlate->m_spotColors);
						colorantList2.AddSeparations(strSeps);
					}
					break;
	}

	colorantList.MixColors(colorantList2);

	if (colorantList.GetSize() <= 0)
		return rcBox;

	pDC->SelectObject(&font);

	CPen	pen(PS_SOLID, 1, LIGHTGRAY);
	CBrush	brush(RGB(250,250,250));
	CPen*	pOldPen	  = pDC->GetCurrentPen();
	CBrush* pOldBrush = pDC->GetCurrentBrush();

	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	brush.DeleteObject();

	CRect rcIcon = rcFrame; rcIcon.top += 4; rcIcon.right = rcIcon.left + 8; rcIcon.bottom = rcIcon.top + 8;
		  rcText = rcFrame; rcText.left = rcIcon.right + 5; rcText.bottom = rcText.top + nTextHeight;

	for (int i = 0; i < colorantList.GetSize(); i++)
	{
		CColorant& rColorant = colorantList[i];
		brush.CreateSolidBrush(rColorant.m_crRGB);
		pDC->SelectObject(&brush);
		pDC->Rectangle(rcIcon);
		brush.DeleteObject();
		rcBox.UnionRect(rcBox, rcIcon);

		DrawTextMeasured(pDC, rColorant.m_strName, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE, rcBox);

		if (pDisplayList)
		{
			CRect rcDI = rcText; rcDI.left = rcIcon.left - 15; rcDI.right -= 5;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)i, DISPLAY_ITEM_REGISTRY(CLayoutDataWindow, PrintColor), NULL, CDisplayItem::CtrlDeselectType | CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
				{
					CRect rcIcon = pDI->m_BoundingRect; 
					pDC->DrawIcon(rcIcon.left + 3, rcIcon.top + 2, theApp.LoadIcon(IDI_CHECK));
				}
			}
		}

		rcIcon.OffsetRect(0, nTextHeight);
		rcText.OffsetRect(0, nTextHeight);
	}
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();
	font.DeleteObject();

	pDC->SelectObject(pOldFont);

	if (pDisplayList)
	{
		CLayoutDataWindow* pWnd = (CLayoutDataWindow*)pDisplayList->m_pParent;
		if (pWnd)
			pWnd->m_colorantList = colorantList;
	}

	return rcBox;
}

CRect CLayout::Draw(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice, BOOL bShowFoldSheets, BOOL bShowCutlines, BOOL bShowMeasures, CFoldSheet* pFoldSheet, BOOL bThumbnail, 
				   int nSide, CMarkList* pMarkList, CMediaSize* pMediaSize, CDisplayList* pDisplayList, BOOL bHighlight, BOOL bShowPreviews, BOOL bShowSelected, BOOL bShowOccupied, BOOL bShowNoPageTemplate)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));
	if (this == NULL)
		return rcBox;

	if (nSide == BACKSIDE)
		if (m_nProductType)
			return rcBox;

	pDC->LPtoDP(&rcFrame);
	CSize sizeBorder(Pixel2MM(pDC, 15), Pixel2MM(pDC, 15));
	pDC->LPtoDP(&sizeBorder);

	if (nSide == BOTHSIDES)
		if (GetCurrentWorkStyle() == WORK_SINGLE_SIDE)
			nSide = FRONTSIDE;

	CRect rcBoxFront(0, 0, 0, 0), rcBoxBack(0, 0, 0, 0);
	if ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) )
	{
		CSize  szExtents;
		CPoint ptOrg;
		szExtents = GetExtents(pPrintSheet, FRONTSIDE, bShowMeasures, &ptOrg, FALSE, pPressDevice);
		long nExtentX = szExtents.cx; 
		long nExtentY = szExtents.cy;
		long nXOrg	  = ptOrg.x; 
		long nYOrg	  = ptOrg.y; 
		if (pMediaSize)
		{
			nExtentX = max(nExtentX, CPrintSheetView::WorldToLP(pMediaSize->GetMediaWidth(pPrintSheet)));
			nExtentY = max(nExtentY, CPrintSheetView::WorldToLP(pMediaSize->GetMediaHeight(pPrintSheet)));
		}
		if ( (nExtentX <= 0) || (nExtentY <= 0) )
			return CRect(0,0,0,0);

		CRect rcViewPort = rcFrame;
		if ( (nSide == BOTHSIDES) && (m_nProductType == WORK_AND_BACK) )
			rcViewPort.right = rcFrame.CenterPoint().x - sizeBorder.cx;

		pDC->SaveDC();
		CRect rcClip = rcViewPort; 
		//pDC->IntersectClipRect(rcClip);
		//rcViewPort.DeflateRect(5, 5);
		pDC->SetMapMode(MM_ISOTROPIC); 
		pDC->SetWindowExt(nExtentX, nExtentY);
		pDC->SetWindowOrg(nXOrg,	nExtentY + nYOrg);
		pDC->SetViewportExt(rcViewPort.Width(), -rcViewPort.Height());
		pDC->SetViewportOrg(rcViewPort.left, rcViewPort.top);
		
		DrawSide(pDC, rcViewPort, pPrintSheet, pPressDevice, bShowFoldSheets, bShowCutlines, bShowMeasures, pFoldSheet, bThumbnail, FRONTSIDE, pMarkList, pMediaSize, pDisplayList, bHighlight, bShowPreviews, bShowOccupied, bShowNoPageTemplate);

		rcBoxFront = CRect(CPoint(nXOrg, nYOrg), CSize(nExtentX, nExtentY));
		pDC->LPtoDP(&rcBoxFront);
		rcBoxFront.NormalizeRect();

		pDC->RestoreDC(-1);
	}

	if ( (nSide == BACKSIDE) || (nSide == BOTHSIDES) && (m_nProductType == FALSE) )
	{
		CSize  szExtents;
		CPoint ptOrg;
		szExtents = GetExtents(pPrintSheet, BACKSIDE, bShowMeasures, &ptOrg, FALSE, pPressDevice);
		long nExtentX = szExtents.cx; 
		long nExtentY = szExtents.cy;
		long nXOrg	  = ptOrg.x;
		long nYOrg	  = ptOrg.y;
		if (pMediaSize)
		{
			nExtentX = max(nExtentX, CPrintSheetView::WorldToLP(pMediaSize->GetMediaWidth(pPrintSheet)));
			nExtentY = max(nExtentY, CPrintSheetView::WorldToLP(pMediaSize->GetMediaHeight(pPrintSheet)));
		}
		if ( (nExtentX <= 0) || (nExtentY <= 0) )
			return CRect(0,0,0,0);

		CRect rcViewPort = rcFrame;
		if (nSide == BOTHSIDES)
		{
			rcViewPort.left  = rcViewPort.left + rcBoxFront.Width() + 2*sizeBorder.cx; 
			rcViewPort.right = rcViewPort.left + rcBoxFront.Width();
		}

		pDC->SaveDC();
		CRect rcClip = rcViewPort; 
		//pDC->IntersectClipRect(rcClip);
		//rcViewPort.DeflateRect(5, 5);
		pDC->SetMapMode(MM_ISOTROPIC); 
		pDC->SetWindowExt(nExtentX, nExtentY);
		pDC->SetWindowOrg(nXOrg,	nExtentY + nYOrg);
		pDC->SetViewportExt(rcViewPort.Width(), -rcViewPort.Height());
		pDC->SetViewportOrg(rcViewPort.left, rcViewPort.top);
		
		DrawSide(pDC, rcViewPort, pPrintSheet, pPressDevice, bShowFoldSheets, bShowCutlines, bShowMeasures, pFoldSheet, bThumbnail, BACKSIDE, pMarkList, pMediaSize, pDisplayList, bHighlight, bShowPreviews, bShowNoPageTemplate);

		rcBoxBack = CRect(CPoint(nXOrg, nYOrg), CSize(nExtentX, nExtentY));
		pDC->LPtoDP(&rcBoxBack);
		rcBoxBack.NormalizeRect();

		pDC->RestoreDC(-1);
	}

	CRect rcBoxAll;
	rcBoxAll.UnionRect(rcBoxFront, rcBoxBack);

	if (bShowSelected)
	{
		CGraphicComponent gp;
		CRect rcSelect = rcBoxAll;
		rcSelect.InflateRect(5, 5);
		gp.DrawTransparentFrame(pDC, rcSelect, RGB(193,211,251));
	}

	if (nSide == BOTHSIDES)
		rcBoxAll.right += sizeBorder.cx;

	pDC->DPtoLP(&rcBoxAll);

	return rcBoxAll;
}

void CLayout::DrawSide(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice, BOOL bShowFoldSheets, BOOL bShowCutlines, BOOL bShowMeasures, CFoldSheet* pFoldSheet, BOOL bThumbnail, 
				   int nSide, CMarkList* pMarkList, CMediaSize* pMediaSize, CDisplayList* pDisplayList, BOOL bHighlight, BOOL bShowPreviews, BOOL bShowOccupied, BOOL bShowNoPageTemplate)
{
	CLayoutSide* pLayoutSide = NULL;
	int			 nActLayoutSide = nSide;
	CRect		 sheetRect;
	CPoint		 pt1, pt2;
	switch (nActLayoutSide)
	{
	case FRONTSIDE : pt1.x = CPrintSheetView::WorldToLP(m_FrontSide.m_fPaperLeft);
					 pt1.y = CPrintSheetView::WorldToLP(m_FrontSide.m_fPaperTop);
					 pt2.x = CPrintSheetView::WorldToLP(m_FrontSide.m_fPaperRight);
					 pt2.y = CPrintSheetView::WorldToLP(m_FrontSide.m_fPaperBottom);
					 pLayoutSide = &m_FrontSide;
					 break;
	case BACKSIDE :	 pt1.x = CPrintSheetView::WorldToLP(m_BackSide.m_fPaperLeft);
					 pt1.y = CPrintSheetView::WorldToLP(m_BackSide.m_fPaperTop);
					 pt2.x = CPrintSheetView::WorldToLP(m_BackSide.m_fPaperRight);
					 pt2.y = CPrintSheetView::WorldToLP(m_BackSide.m_fPaperBottom);
					 pLayoutSide = &m_BackSide;
					 break;
	}
	sheetRect.left   = pt1.x;
	sheetRect.top	 = pt1.y;
	sheetRect.right  = pt2.x;
	sheetRect.bottom = pt2.y;
	CRect rcTitle = sheetRect;
	sheetRect.NormalizeRect();
	CSize sizePixel(1,1);
	pDC->DPtoLP(&sizePixel);
	sheetRect.InflateRect(sizePixel);

	if ( ! pPrintSheet)
		pPrintSheet = GetFirstPrintSheet();
	if (pPrintSheet && pPressDevice)
	{
		CPlate* pPlate = pPrintSheet->GetTopmostPlate(FRONTSIDE);
		if (pPlate)
			pPlate->Draw(pDC, pPressDevice);//, bShowMeasures);
	}
//	else
//		pLayoutSide->m_DefaultPlates.m_defaultPlate.Draw(pDC, pPressDevice);

	if ( ! pPrintSheet)
	{
		pDC->RestoreDC(-1);
		return;
	}

	//// draw sheet
	if (pDC->RectVisible(sheetRect))
	{
		CBrush brush((bHighlight) ? WHITE : RGB(240,240,240));
		CBrush* pOldBrush = pDC->SelectObject(&brush);	
		CPen*   pOldPen	  = (CPen*)pDC->SelectStockObject(NULL_PEN);	

		CPrintSheetView::DrawRectangleExt(pDC, &sheetRect);

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);		
		brush.DeleteObject();
	}

	CSize sizeViewport = pDC->GetViewportExt();
	if ( (sizeViewport.cx < 10)  || (abs(sizeViewport.cy) < 10) )	// makes no sense to draw details
		return;

	BOOL bShowMarks = ( ! pPressDevice) ? 2 : TRUE;		// 2: if no plate, don't show marks beyond sheet
	pLayoutSide->Draw(pDC, bShowMarks, TRUE, bShowCutlines, bShowMeasures, bThumbnail, pPrintSheet, (pFoldSheet) ? pFoldSheet->m_nProductPartIndex : -1, pFoldSheet, bHighlight, bShowPreviews, pDisplayList, bShowOccupied, bShowNoPageTemplate);	

	//// draw sheet frame
	if (pDC->RectVisible(sheetRect))
	{
		CPen	framePen(PS_SOLID, Pixel2ConstMM(pDC, 1), (pPressDevice) ? RGB(210,210,210) : MIDDLEGRAY);
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);	
		CPen*   pOldPen	  = pDC->SelectObject(&framePen);	

		CPrintSheetView::DrawRectangleExt(pDC, &sheetRect);

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);		
		framePen.DeleteObject();
	}

	if (bShowFoldSheets)
		if ( (rcFrame.Width() > 45) && (rcFrame.Height() > 30) )
		{
			for (int nComponentRefIndex = 0; nComponentRefIndex < GetNumFoldSheets(); nComponentRefIndex++)
			{
				CFoldSheet* pActFoldSheet = pPrintSheet->GetFoldSheet(nComponentRefIndex);
				if ( ! pActFoldSheet)
				{
					if (pFoldSheet)		// helper foldsheet - number corresponds to neg. foldsheetindex
				 		pLayoutSide->DrawFoldSheet(pDC, nActLayoutSide, pPrintSheet, nComponentRefIndex, pFoldSheet, bThumbnail, pDisplayList);
				}
				else
			 		pLayoutSide->DrawFoldSheet(pDC, nActLayoutSide, pPrintSheet, nComponentRefIndex, pActFoldSheet, bThumbnail, pDisplayList);
			}
		}

	if (pMarkList)
		pMarkList->DrawSheetPreviews(pDC, pPrintSheet, nActLayoutSide);
	if (pMediaSize)
		for (int i = 0; i < pMediaSize->m_posInfos.GetSize(); i++)	
			pMediaSize->DrawTile(pDC, pPrintSheet, nActLayoutSide, i, FALSE, FALSE);

	if (bShowMeasures)
	{
		CPrintSheetView::DrawMeasure(pDC, nActLayoutSide, pPrintSheet);
		if ( ! bThumbnail)
		{
			CPrintSheetView::DrawSheetMeasuring(pDC, nActLayoutSide, this, (pPressDevice) ? TRUE : FALSE);
			if (pPressDevice)
				CPrintSheetView::DrawPlateMeasuring(pDC, nActLayoutSide, this);
		}
	}
}

void CLayout::CreateThumbnail(CThumbnailBitmap& thumbnailBitmap, CPrintSheet& rPrintSheet, CSize size)
{
	CRect rcFrame(CPoint(0,0), size);
	rcFrame = GetDrawingBox(theApp.m_pMainWnd, rcFrame, &rPrintSheet);

	CDC* pDC = theApp.m_pMainWnd->GetDC();
	CDC		memDC;		
	CBitmap bitmap;	
	memDC.CreateCompatibleDC(pDC);
	bitmap.CreateCompatibleBitmap(pDC, rcFrame.Width(), rcFrame.Height());
	memDC.SelectObject(&bitmap);

	memDC.FillSolidRect(rcFrame, WHITE);	
	CRect rcSheetBox = Draw(&memDC, rcFrame, &rPrintSheet, NULL, TRUE, FALSE, FALSE, NULL, TRUE, FRONTSIDE, NULL, NULL, NULL, FALSE, FALSE, FALSE, TRUE);

	CFont smallFont;
	smallFont.CreateFont(11, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	memDC.SelectObject(&smallFont);
	memDC.SetBkMode(OPAQUE);
	memDC.SetBkColor(WHITE);
	memDC.SetTextColor(BLACK);
	CRect rcTitle = rcSheetBox; rcTitle.DeflateRect(2, 1);
	memDC.DrawText(rPrintSheet.GetNumber(), rcTitle, DT_LEFT | DT_BOTTOM | DT_SINGLELINE | DT_END_ELLIPSIS);
	smallFont.DeleteObject();

	CImage img;
	img.Attach((HBITMAP)bitmap);
	thumbnailBitmap.SaveThumbnail(img);
	img.Detach();
	img.Destroy();

	bitmap.DeleteObject();
	memDC.DeleteDC();
	theApp.m_pMainWnd->ReleaseDC(pDC);
}

int	CLayout::TransformFontSize(CDC* pDC, float fWorld, int nScreenMin, int nWorldMin)
{
	CSize fontSize = CSize(0, CPrintSheetView::WorldToLP(fWorld));	// fWorld = size when layout plotted 1:1

	if ( ! pDC->IsPrinting())
	{
		CSize sizeViewport = pDC->GetViewportExt();

		pDC->LPtoDP(&fontSize);
		if (nScreenMin > 0)
		{
			if (fontSize.cy < nScreenMin)
				fontSize.cy = nScreenMin;
		}
		else
		if ( (abs(sizeViewport.cx) < 50) || (abs(sizeViewport.cy) < 50) )	// thumbnail
		{
			if (fontSize.cy < 5)
				fontSize.cy = 5;
			if (fontSize.cy > 8)
				fontSize.cy = 8;
		}
		else
		if ( (abs(sizeViewport.cx) < 150) || (abs(sizeViewport.cy) < 150) )	// thumbnail
		{
			if (fontSize.cy < 8)
				fontSize.cy = 8;
			if (fontSize.cy > 11)
				fontSize.cy = 11;
		}
		else
		{
			if (fontSize.cy < 11)
				fontSize.cy = 11;
			if (fontSize.cy > 13)
				fontSize.cy = 13;
		}
		pDC->DPtoLP(&fontSize);
	}
	else
	{
		if ( (nWorldMin > 0) && (pDC->GetDeviceCaps(LOGPIXELSX) != theApp.m_nScreenDPI) )	// if not inside screen preview routine -> set fontsize to minimum nWorldMin for printout
		{
			CSize sizeDP = fontSize;
			pDC->LPtoDP(&sizeDP);
			int nMM = DP2MM(pDC, sizeDP.cy);
			if (nMM < nWorldMin)	
				fontSize.cy = (int)(((float)nWorldMin / (float)nMM) * (float)fontSize.cy);
		}
	}
	return fontSize.cy;
}

BOOL CLayout::HasMarks(int nMarkType)
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
		{
			CPageTemplate* pTemplate = rObject.GetCurrentMarkTemplate();
			switch (nMarkType)
			{
			case -1:	return TRUE;
			case CMark::CropMark:		if (pTemplate->GetMarkType() == CMark::CropMark)
											return TRUE;
										break;
			case CMark::SectionMark:	if (pTemplate->GetMarkType() == CMark::SectionMark)
											return TRUE;
										break;
			case CMark::FoldMark:		if (pTemplate->GetMarkType() == CMark::FoldMark)
											return TRUE;
										break;
			case CMark::CustomMark:		if (pTemplate->GetMarkType() == CMark::CustomMark)
											return TRUE;
										break;
			case CMark::TextMark:		if (pTemplate->GetMarkType() == CMark::TextMark)
											return TRUE;
										break;
			case CMark::BarcodeMark:	if (pTemplate->GetMarkType() == CMark::BarcodeMark)
											return TRUE;
										break;
			case CMark::DrawingMark:	if (pTemplate->GetMarkType() == CMark::DrawingMark)
											return TRUE;
										break;
			}
		}
	}
	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_BackSide.m_ObjectList.GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
		{
			CPageTemplate* pTemplate = rObject.GetCurrentMarkTemplate();
			switch (nMarkType)
			{
			case -1:	return TRUE;
			case CMark::CropMark:		if (pTemplate->GetMarkType() == CMark::CropMark)
											return TRUE;
										break;
			case CMark::SectionMark:	if (pTemplate->GetMarkType() == CMark::SectionMark)
											return TRUE;
										break;
			case CMark::FoldMark:		if (pTemplate->GetMarkType() == CMark::FoldMark)
											return TRUE;
										break;
			case CMark::CustomMark:		if (pTemplate->GetMarkType() == CMark::CustomMark)
											return TRUE;
										break;
			case CMark::TextMark:		if (pTemplate->GetMarkType() == CMark::TextMark)
											return TRUE;
										break;
			case CMark::BarcodeMark:	if (pTemplate->GetMarkType() == CMark::BarcodeMark)
											return TRUE;
										break;
			case CMark::DrawingMark:	if (pTemplate->GetMarkType() == CMark::DrawingMark)
											return TRUE;
										break;
			}
		}
	}

	return FALSE;
}

BOOL CLayout::HasPages()
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::Page)
			if ( ! rLayoutObject.IsFlatProduct())
				return TRUE;
	}
	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_BackSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::Page)
			if ( ! rLayoutObject.IsFlatProduct())
				return TRUE;
	}
	return FALSE;
}

BOOL CLayout::HasFlatProducts()
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.IsFlatProduct())
			return TRUE;
	}
	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_BackSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.IsFlatProduct())
			return TRUE;
	}
	return FALSE;
}

CLayoutObject* CLayout::GetFirstFlatObject()
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.IsFlatProduct())
			return &rLayoutObject;
	}
	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_BackSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.IsFlatProduct())
			return &rLayoutObject;
	}
	return NULL;
}

void CLayout::AssignSheet(CSheet& rSheet)
{
	m_FrontSide.m_fPaperWidth		= rSheet.m_fWidth;
	m_FrontSide.m_fPaperHeight		= rSheet.m_fHeight;
	m_FrontSide.m_nPaperOrientation	= (unsigned char)( (rSheet.m_fWidth > rSheet.m_fHeight) ? LANDSCAPE : PORTRAIT);
	m_FrontSide.m_strFormatName		= rSheet.m_strSheetName;
	m_FrontSide.m_bAdjustFanout		= (rSheet.m_FanoutList.GetSize()) ? TRUE : FALSE;
	m_FrontSide.m_FanoutList		= rSheet.m_FanoutList;
	m_FrontSide.PositionLayoutSide(FLT_MAX, m_FrontSide.GetPressDevice());

	m_BackSide.m_fPaperWidth		= rSheet.m_fWidth;
	m_BackSide.m_fPaperHeight		= rSheet.m_fHeight;
	m_BackSide.m_nPaperOrientation	= m_FrontSide.m_nPaperOrientation;
	m_BackSide.m_strFormatName		= rSheet.m_strSheetName;
	m_BackSide.m_bAdjustFanout		= m_FrontSide.m_bAdjustFanout;
	m_BackSide.m_FanoutList			= rSheet.m_FanoutList;
	m_BackSide.PositionLayoutSide(FLT_MAX, m_BackSide.GetPressDevice());
}


void CLayout::AnalyzeMarks(BOOL bUpdateViews, CMark* pMark, CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_markList.m_strAnalyzeReport.Empty();

	CMarkList tmpMarkList;
	for (int i = 0; i < m_markList.GetSize(); i++)
	{
		if (m_markList[i]->IsAutoMark())	// move auto marks to end of list
		{
			tmpMarkList.Add(m_markList[i]);
			m_markList.RemoveAt(i, FALSE);
			i--;
		} 
	}
	m_markList.Append(tmpMarkList);

	m_FrontSide.RemoveMarkObjects();
	m_BackSide.RemoveMarkObjects();
	//pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
	pDoc->m_MarkSourceList.RemoveUnusedSources();
	ReorganizeColorInfos();		// colors on sheets needs to be updated, because of eventually unmapped custom mark colors that has been removed from plates
								// this is important for color rule based marks when beeing checked for assignment

	//m_markList.m_colorDefTable.RemoveAll();
	//m_markList.m_colorDefTable.Append(pDoc->m_ColorDefinitionTable);//theApp.m_colorDefTable);

	// first position custom marks, because ClipToObjects() for CropMarks and FoldMarks refers to CustomMarks
	for (int i = 0; i < m_markList.GetSize(); i++)
	{
		if (m_markList[i]->m_nType == CMark::CustomMark)
		{
			if ( ! m_markList[i]->m_rules.GetRuleExpression(0).IsEmpty())	// mark has rules
			{
				if (m_markList[i]->GlobalCheckRules(this, m_markList.GetSize()))
					CDlgMarkSet::PositionAndCreateMarkAuto(m_markList[i], &m_markList, _T(""), (pPrintSheet) ? pPrintSheet : GetFirstPrintSheet());
				else
				{
					m_markList.RemoveAt(i);
					i--;
				}
			}
			else
				CDlgMarkSet::PositionAndCreateMarkAuto(m_markList[i], &m_markList, _T(""), (pPrintSheet) ? pPrintSheet : GetFirstPrintSheet());
		}
	}
	// now position all others
	for (int i = 0; i < m_markList.GetSize(); i++)
	{
		if (m_markList[i]->m_nType != CMark::CustomMark)
		{
			if ( ! m_markList[i]->m_rules.GetRuleExpression(0).IsEmpty())	// mark has rules
			{
				if (m_markList[i]->GlobalCheckRules(this, m_markList.GetSize()))
					CDlgMarkSet::PositionAndCreateMarkAuto(m_markList[i], &m_markList, _T(""), (pPrintSheet) ? pPrintSheet : GetFirstPrintSheet());
				else
				{
					m_markList.RemoveAt(i);
					i--;
				}
			}
			else
				CDlgMarkSet::PositionAndCreateMarkAuto(m_markList[i], &m_markList, _T(""), (pPrintSheet) ? pPrintSheet : GetFirstPrintSheet());
		}
	}

#ifdef KIM_ENGINE
	if ( ! m_markList.m_strAnalyzeReport.IsEmpty())
		KIMLogMessage(m_markList.m_strAnalyzeReport, MB_ICONWARNING);
#endif

	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	ReorganizeColorInfos();

	if (bUpdateViews)
	{
		CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pPrintSheetView)
		{
			//// remove all mark items in display list, because m_pData pointers are no longer valid
			//// this is not perfect, because mark objects will be not clickable up to the next PrintSheetView update
			//// so this needs to be solved in future
			for (int i = 0; i < pPrintSheetView->m_DisplayList.m_List.GetSize(); i++)
			{
				if (pPrintSheetView->m_DisplayList.m_List[i].m_strItemTypeID != DISPLAY_ITEM_TYPEID(CPrintSheetView, CLayoutObject))
					continue;
				CLayoutObject* pObject = (CLayoutObject*)pPrintSheetView->m_DisplayList.m_List[i].m_pData;
				if (pObject)
					if (pObject->m_nType == CLayoutObject::Page)
						continue;
				pPrintSheetView->m_DisplayList.m_List.RemoveAt(i);
				i--;
			}

			for (int i = 0; i < pPrintSheetView->m_DisplayList.m_ListOld.GetSize(); i++)
			{
				if (pPrintSheetView->m_DisplayList.m_ListOld[i].m_strItemTypeID != DISPLAY_ITEM_TYPEID(CPrintSheetView, CLayoutObject))
					continue;
				CLayoutObject* pObject = (CLayoutObject*)pPrintSheetView->m_DisplayList.m_ListOld[i].m_pData;
				if (pObject)
					if (pObject->m_nType == CLayoutObject::Page)
						continue;
				pPrintSheetView->m_DisplayList.m_ListOld.RemoveAt(i);
				i--;
			}

			//pPrintSheetView->m_DisplayList.Invalidate();	// clear, because layout object pointers of marks has changed - needed by MarkInfoFields
			//pPrintSheetView->m_DisplayList.Clear();
			//pPrintSheetView->Invalidate();
			//pPrintSheetView->UpdateWindow();
		}

		CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.CImpManApp::GetView(RUNTIME_CLASS(CProdDataPrintView));
		if (pProdDataPrintView)
			pProdDataPrintView->InitObjectList();
	}
}

void CLayout::DecrementMarkListColorDefIndices(int nColorDefinitionIndex)
{
	for (int i = 0; i < m_markList.GetSize(); i++)
	{
		CPageTemplate& rTemplate = m_markList[i]->m_markTemplate;
		for (int i = 0; i < rTemplate.m_ColorSeparations.GetSize(); i++)
			if (rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex > nColorDefinitionIndex)
				rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex--;
	}
}

BOOL CLayout::ColorInUseMarkList(CImpManDoc* pDoc, int nColorDefinitionIndex, BOOL bSeparationOnly, BOOL bCompositeOnly)
{
	for (int i = 0; i < m_markList.GetSize(); i++)
	{
		CPageTemplate& rTemplate = m_markList[i]->m_markTemplate;
		for (int i = 0; i < rTemplate.m_ColorSeparations.GetSize(); i++)
			if (rTemplate.ColorInUse(pDoc, nColorDefinitionIndex, bSeparationOnly, bCompositeOnly))
				return TRUE;
	}

	return FALSE;
}

//int	CLayout::GetMaxNUp(CPrintSheet& rPrintSheet, CFoldSheet* pRemoveFoldSheet, CFoldSheet* pNewFoldSheet, BOOL bAllowRotation, int* pnNumFoldSheetsX, int* pnNumFoldSheetsY, CRect rcPlaceArea)
//{
//	if ( ! pNewFoldSheet)
//		return 0;
//
//	int	  nLayerIndex = 0;
//	int	  nTotalNUp   = 0;
//	float fXPos, fYPos;
//	if (pRemoveFoldSheet)
//	{
//		for (int nComponentRefIndex = 0; nComponentRefIndex < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
//		{
//			BOOL bRemove = FALSE;
//			CFoldSheet* pFoldSheet = rPrintSheet.GetFoldSheet(nComponentRefIndex);
//			if ( ! pFoldSheet)
//				bRemove = TRUE;
//			else
//				if (pFoldSheet)
//					if (pFoldSheet->m_strFoldSheetTypeName == pRemoveFoldSheet->m_strFoldSheetTypeName)
//						if (pFoldSheet->m_nProductPartIndex == pRemoveFoldSheet->m_nProductPartIndex)
//							bRemove = TRUE;
//			if (bRemove)
//			{
//				m_FrontSide.SetFoldSheetLayerVisible(nComponentRefIndex, FALSE);
//				m_BackSide.SetFoldSheetLayerVisible(nComponentRefIndex, FALSE);
//			}
//		}
//	}
//
//	rPrintSheet.FindFreePosition(pNewFoldSheet, nLayerIndex, fXPos, fYPos, pnNumFoldSheetsX, pnNumFoldSheetsY, &nTotalNUp, ! bAllowRotation);
//	
//	if (pRemoveFoldSheet)
//	{
//		for (int nComponentRefIndex = 0; nComponentRefIndex < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
//		{
//			BOOL bRestore = FALSE;
//			CFoldSheet* pFoldSheet = rPrintSheet.GetFoldSheet(nComponentRefIndex);
//			if ( ! pFoldSheet)
//				bRestore = TRUE;
//			else
//				if (pFoldSheet)
//					if (pFoldSheet->m_strFoldSheetTypeName == pRemoveFoldSheet->m_strFoldSheetTypeName)
//						if (pFoldSheet->m_nProductPartIndex == pRemoveFoldSheet->m_nProductPartIndex)
//							bRestore = TRUE;
//			if (bRestore)
//			{
//				m_FrontSide.SetFoldSheetLayerVisible(nComponentRefIndex, TRUE);
//				m_BackSide.SetFoldSheetLayerVisible(nComponentRefIndex, TRUE);
//			}
//		}
//	}
//
//	return nTotalNUp;
//}

int	CLayout::GetMaxNUp(CPrintSheet& rPrintSheet, CFoldSheet* pFoldSheet, BOOL bDoRotate, int* pnNumFoldSheetsX, int* pnNumFoldSheetsY, CRect rcPlaceArea)
{
	if (pnNumFoldSheetsX && pnNumFoldSheetsY)
	{
		*pnNumFoldSheetsX = 0;
		*pnNumFoldSheetsY = 0;
	}

	int	  nTotalNUp   = 0;
	float fXPos, fYPos, fGrossWidth, fGrossHeight;
	int nLayerIndex = 0;
	pFoldSheet->GetCutSize(nLayerIndex, fGrossWidth, fGrossHeight);

	if (bDoRotate)
	{
		float fTemp = fGrossWidth; fGrossWidth = fGrossHeight; fGrossHeight = fTemp;
	}

	if (rcPlaceArea.IsRectEmpty())
		rPrintSheet.FindFreePosition(fGrossWidth, fGrossHeight, fXPos, fYPos, pnNumFoldSheetsX, pnNumFoldSheetsY, &nTotalNUp);
	else
	{
		long nWidth  = (long)(fGrossWidth  * 1000.0f);
		long nHeight = (long)(fGrossHeight * 1000.0f);
		if ( (nWidth != 0) && (nHeight != 0) )
		{
			int nNumX = rcPlaceArea.Width() / nWidth; int nNumY = rcPlaceArea.Height() / nHeight;
			nTotalNUp = nNumX * nNumY;
			if (pnNumFoldSheetsX && pnNumFoldSheetsY)
			{
				*pnNumFoldSheetsX = nNumX;
				*pnNumFoldSheetsY = nNumY;
			}
		}
	}

	return nTotalNUp;
}

int	CLayout::GetMaxNUp(CPrintSheet& rPrintSheet, CFlatProduct& rFlatProduct, BOOL bDoRotate, int* pnNumProductsX, int* pnNumProductsY, CRect rcPlaceArea)
{
	if (pnNumProductsX && pnNumProductsY)
	{
		*pnNumProductsX = 0;
		*pnNumProductsY = 0;
	}

	int	  nTotalNUp   = 0;
	float fXPos, fYPos;
	float fGrossWidth  = rFlatProduct.m_fTrimSizeWidth  + rFlatProduct.m_fLeftTrim   + rFlatProduct.m_fRightTrim;
	float fGrossHeight = rFlatProduct.m_fTrimSizeHeight + rFlatProduct.m_fBottomTrim + rFlatProduct.m_fTopTrim;
	if (bDoRotate)
	{
		float fTemp = fGrossWidth; fGrossWidth = fGrossHeight; fGrossHeight = fTemp;
	}

	if (rcPlaceArea.IsRectEmpty())
		rPrintSheet.FindFreePosition(fGrossWidth, fGrossHeight, fXPos, fYPos, pnNumProductsX, pnNumProductsY, &nTotalNUp);
	else
	{
		long nWidth  = (long)(fGrossWidth  * 1000.0f);
		long nHeight = (long)(fGrossHeight * 1000.0f);
		if ( (nWidth != 0) && (nHeight != 0) )
		{
			int nNumX = rcPlaceArea.Width() / nWidth; int nNumY = rcPlaceArea.Height() / nHeight;
			nTotalNUp = nNumX * nNumY;
			if (pnNumProductsX && pnNumProductsY)
			{
				*pnNumProductsX = nNumX;
				*pnNumProductsY = nNumY;
			}
		}
	}

	return nTotalNUp;
}

int CLayout::MakeNUp(BOOL bSample, CPrintSheet& rPrintSheet, CFoldSheet* pFoldSheet, int nLayerIndex, int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition, CRect rcPlaceArea, int* pnNumFoldSheetsX, int* pnNumFoldSheetsY)
{
	if ( ! pFoldSheet)
		return 0;

	rPrintSheet.LinkLayout(this);

	RemoveMarkObjects(-1, TRUE);

	SetWorkStyle(WORK_AND_BACK);

	int		nNumFoldSheetsX, nNumFoldSheetsY;
	float	fXPos, fYPos, fGrossWidth, fGrossHeight;
	pFoldSheet->GetCutSize(nLayerIndex, fGrossWidth, fGrossHeight);

	if ( (nHeadPosition == LEFT) || nHeadPosition == RIGHT)
	{
		float fTemp = fGrossWidth; fGrossWidth = fGrossHeight; fGrossHeight = fTemp;
	}

	if (rcPlaceArea.IsRectEmpty())
		rPrintSheet.FindFreePosition(fGrossWidth, fGrossHeight, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY, NULL);
	else
	{
		long nWidth  = (long)(fGrossWidth  * 1000.0f);
		long nHeight = (long)(fGrossHeight * 1000.0f);
		if ( (nWidth != 0) && (nHeight != 0) )
		{
			nNumFoldSheetsX = rcPlaceArea.Width()  / nWidth; 
			nNumFoldSheetsY = rcPlaceArea.Height() / nHeight;
		}
	}

	if ( (nNumFoldSheetsX * nNumFoldSheetsY) <= 0)
		return 0;

	if (nNUp != INT_MAX)
		if (nNUp > (nNumFoldSheetsX * nNumFoldSheetsY) )
			return 0;

	CString strNUp; strNUp.LoadString(IDS_NUP);
	m_strLayoutName.Format(_T("%d %s"), nNUp, strNUp);

	int nRotate = ROTATE0;
	switch (nHeadPosition)
	{
	case TOP:		nRotate = ROTATE0;		break;
	case LEFT:		nRotate = ROTATE90;		break;
	case BOTTOM:	nRotate = ROTATE180;	break;
	case RIGHT:		nRotate = ROTATE270;	break;
	}

	int nProductIndex = pFoldSheet->GetBoundProduct().GetIndex();
	int	nFoldSheetIndex = -1;
	if (pFoldSheet->m_nFoldSheetNumber > 0) 
		nFoldSheetIndex = pFoldSheet->GetIndex();
	else
	{
		CImpManDoc*	pDoc = CImpManDoc::GetDoc();
		if (pDoc && ! bSample)	// this function is used even w/o doc in production profiles layout pool -> use locally here
		{
			CFoldSheet* pLastFoldSheet = pDoc->m_Bookblock.GetLastFoldSheet(-nProductIndex - 1);
			if (pLastFoldSheet)
				nFoldSheetIndex = -(pLastFoldSheet->m_nFoldSheetNumber + 1);
		}
	}

	if (pnNumFoldSheetsX)
		*pnNumFoldSheetsX = 0;
	if (pnNumFoldSheetsY)
		*pnNumFoldSheetsY = 0;

	int	  nNumAdded = 0;
	float fFoldSheetPosX  = fXPos = 0.0f;
	float fFoldSheetPosY  = fYPos = 0.0f;
	for (int ny = 0; (ny < nNumFoldSheetsY) && (nNUp > 0); ny++)
	{
		fFoldSheetPosX = fXPos;
		for (int nx = 0; (nx < nNumFoldSheetsX) && (nNUp > 0); nx++)
		{
			AddFoldSheetLayer(pFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, pFoldSheet->GetProductPart(), nRotate, &rPrintSheet);
			fFoldSheetPosX += fGrossWidth;

			CFoldSheetLayerRef layerRef(nFoldSheetIndex, nLayerIndex);
			rPrintSheet.m_FoldSheetLayerRefs.Add(layerRef);
			nNumAdded++;
			nNUp--;
			if (pnNumFoldSheetsX)
				*pnNumFoldSheetsX = max(*pnNumFoldSheetsX, nx + 1);
			if (pnNumFoldSheetsY)
				*pnNumFoldSheetsY = max(*pnNumFoldSheetsY, ny + 1);
		}
		fFoldSheetPosY += fGrossHeight;
	}

	if (bReposition)
	{
		if (pPrintingProfile)
		{
			m_FrontSide.PositionLayoutSide(pPrintingProfile->m_press.m_pressDevice.m_fGripper, &pPrintingProfile->m_press.m_pressDevice);
			m_BackSide.PositionLayoutSide(pPrintingProfile->m_press.m_pressDevice.m_fGripper, &pPrintingProfile->m_press.m_pressDevice);
		}
	}

	return nNumAdded;
}

int	CLayout::MakeNUp(BOOL bSample, CPrintSheet& rPrintSheet, CFlatProduct* pFlatProduct, int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition, CRect rcPlaceArea, int* pnNumProductsX, int* pnNumProductsY)
{
	if ( ! pFlatProduct)
		return 0;

	rPrintSheet.LinkLayout(this);

	RemoveMarkObjects(-1, TRUE);

	SetWorkStyle(WORK_AND_BACK);

	float fXPos, fYPos;
	int nNumProductsX, nNumProductsY;
	float fGrossWidth  = pFlatProduct->m_fTrimSizeWidth  + pFlatProduct->m_fLeftTrim   + pFlatProduct->m_fRightTrim;
	float fGrossHeight = pFlatProduct->m_fTrimSizeHeight + pFlatProduct->m_fBottomTrim + pFlatProduct->m_fTopTrim;
	if ( (nHeadPosition == LEFT) || nHeadPosition == RIGHT)
	{
		float fTemp = fGrossWidth; fGrossWidth = fGrossHeight; fGrossHeight = fTemp;
	}

	if (rcPlaceArea.IsRectEmpty())
		rPrintSheet.FindFreePosition(fGrossWidth, fGrossHeight, fXPos, fYPos, &nNumProductsX, &nNumProductsY, NULL);
	else
	{
		long nWidth  = (long)(fGrossWidth  * 1000.0f);
		long nHeight = (long)(fGrossHeight * 1000.0f);
		if ( (nWidth != 0) && (nHeight != 0) )
		{
			nNumProductsX = rcPlaceArea.Width()  / nWidth; 
			nNumProductsY = rcPlaceArea.Height() / nHeight;
		}
		fXPos = (float)rcPlaceArea.left / 1000.0f;
		fYPos = (float)rcPlaceArea.top  / 1000.0f;
	}

	if ( (nNumProductsX * nNumProductsY) <= 0)
		return 0;

	if (nNUp != INT_MAX)
		if (nNUp > (nNumProductsX * nNumProductsY) )
			return 0;

	CString strNUp; strNUp.LoadString(IDS_NUP);
	m_strLayoutName.Format(_T("%d %s"), nNUp, strNUp);

	float fLeftBorder = 0.0f, fBottomBorder = 0.0f, fRightBorder = 0.0f, fTopBorder = 0.0f; 
	switch (nHeadPosition)
	{
	case TOP:		fLeftBorder	  = pFlatProduct->m_fLeftTrim;
					fBottomBorder = pFlatProduct->m_fBottomTrim;
					fRightBorder  = pFlatProduct->m_fRightTrim;
					fTopBorder	  = pFlatProduct->m_fTopTrim;
					break;			
	case RIGHT:		fLeftBorder	  = pFlatProduct->m_fBottomTrim;
					fBottomBorder = pFlatProduct->m_fRightTrim;
					fRightBorder  = pFlatProduct->m_fTopTrim;
					fTopBorder	  = pFlatProduct->m_fLeftTrim;
					break;			
	case BOTTOM:	fLeftBorder	  = pFlatProduct->m_fRightTrim;
					fBottomBorder = pFlatProduct->m_fTopTrim;
					fRightBorder  = pFlatProduct->m_fLeftTrim;
					fTopBorder	  = pFlatProduct->m_fBottomTrim;
					break;			
	case LEFT:		fLeftBorder	  = pFlatProduct->m_fTopTrim;
					fBottomBorder = pFlatProduct->m_fLeftTrim;
					fRightBorder  = pFlatProduct->m_fBottomTrim;
					fTopBorder	  = pFlatProduct->m_fRightTrim;
					break;
	}
	float fWidth  = ( (nHeadPosition == TOP) ||  (nHeadPosition == BOTTOM) ) ? pFlatProduct->m_fTrimSizeWidth  : pFlatProduct->m_fTrimSizeHeight;
	float fHeight = ( (nHeadPosition == TOP) ||  (nHeadPosition == BOTTOM) ) ? pFlatProduct->m_fTrimSizeHeight : pFlatProduct->m_fTrimSizeWidth;

	float fNetWidth    = fWidth;
	float fNetHeight   = fHeight;


	if (pnNumProductsX)
		*pnNumProductsX = 0;
	if (pnNumProductsY)
		*pnNumProductsY = 0;

	int nNumAdded = 0;
	int	nFlatProductIndex = pFlatProduct->GetIndex();
	for (int i = 0; (i < nNumProductsY) && (nNUp > 0); i++)
	{
		for (int j = 0; (j < nNumProductsX) && (nNUp > 0); j++)
		{
			float fObjectPosX = j * fGrossWidth  + fLeftBorder;
			float fObjectPosY = i * fGrossHeight + fBottomBorder;

			int nComponentRefIndex = rPrintSheet.MakeFlatProductRefIndex(nFlatProductIndex);
			CLayoutObject layoutObject;
			layoutObject.CreateFlatProduct(fObjectPosX, fObjectPosY, fNetWidth, fNetHeight, nHeadPosition, nComponentRefIndex);
			m_FrontSide.m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pFrontLayoutObject = &m_FrontSide.m_ObjectList.GetTail();

			layoutObject.CreateFlatProduct(fObjectPosX, fObjectPosY, fNetWidth, fNetHeight, nHeadPosition, nComponentRefIndex);
			m_BackSide.m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pBackLayoutObject = &m_BackSide.m_ObjectList.GetTail();

			pBackLayoutObject->AlignToCounterPart(pFrontLayoutObject);
			nNUp--;
			nNumAdded++;
			if (pnNumProductsX)
				*pnNumProductsX = max(*pnNumProductsX, j + 1);
			if (pnNumProductsY)
				*pnNumProductsY = max(*pnNumProductsY, i + 1);
		}
	}

	if (bReposition)
	{
		if (pPrintingProfile)
		{
			m_FrontSide.PositionLayoutSide(pPrintingProfile->m_press.m_pressDevice.m_fGripper, &pPrintingProfile->m_press.m_pressDevice);
			m_BackSide.PositionLayoutSide(pPrintingProfile->m_press.m_pressDevice.m_fGripper, &pPrintingProfile->m_press.m_pressDevice);
		}
	}

	return nNumAdded;
}

int CLayout::MakeSequential(BOOL bSample, CPrintSheet& rPrintSheet, CFoldSheet* pFoldSheet, int nLayerIndex, int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition, CRect rcPlaceArea)
{
	if (nNUp <= 1)	
		return 0;

	rPrintSheet.LinkLayout(this);

	SetWorkStyle(WORK_AND_BACK);

	int		nNumFoldSheetsX, nNumFoldSheetsY;
	float	fXPos, fYPos, fGrossWidth, fGrossHeight;
	pFoldSheet->GetCutSize(nLayerIndex, fGrossWidth, fGrossHeight);

	if ( (nHeadPosition == LEFT) || nHeadPosition == RIGHT)
	{
		float fTemp = fGrossWidth; fGrossWidth = fGrossHeight; fGrossHeight = fTemp;
	}

	if (rcPlaceArea.IsRectEmpty())
		rPrintSheet.FindFreePosition(fGrossWidth, fGrossHeight, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY, NULL);
	else
	{
		long nWidth  = (long)(fGrossWidth  * 1000.0f);
		long nHeight = (long)(fGrossHeight * 1000.0f);
		if ( (nWidth != 0) && (nHeight != 0) )
		{
			nNumFoldSheetsX = rcPlaceArea.Width()  / nWidth; 
			nNumFoldSheetsY = rcPlaceArea.Height() / nHeight;
		}
		fXPos = (float)rcPlaceArea.left / 1000.0f;
		fYPos = (float)rcPlaceArea.top  / 1000.0f;
	}

	if ( (nNumFoldSheetsX * nNumFoldSheetsY) <= 1)
		return 0;
	if ( (nNumFoldSheetsX * nNumFoldSheetsY) < nNUp)
		return 0;

	CString strNUp, strSequentially; strNUp.LoadString(IDS_NUP); strSequentially.LoadString(IDS_SEQUENTIALLY);
	m_strLayoutName.Format(_T("%d %s %s"), nNUp, strNUp, strSequentially);

	int nRotate = ROTATE0;
	switch (nHeadPosition)
	{
	case TOP:		nRotate = ROTATE0;		break;
	case LEFT:		nRotate = ROTATE90;		break;
	case BOTTOM:	nRotate = ROTATE180;	break;
	case RIGHT:		nRotate = ROTATE270;	break;
	}

	int					nProductPartIndex	= pFoldSheet->m_nProductPartIndex;
	CBoundProductPart&	rProductPart		= pFoldSheet->GetProductPart();
	int					nProductIndex		= rProductPart.GetBoundProduct().GetIndex();
	CString				strFoldScheme		= pFoldSheet->m_strFoldSheetTypeName;
	CFoldSheet			startFoldSheet		= *pFoldSheet;
	float				fFoldSheetPosX		= fXPos = 0.0f;
	float				fFoldSheetPosY		= fYPos = 0.0f;
	BOOL				bHasRotated			= FALSE;

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();

	int	nFoldSheetIndex = -1;
	if (pFoldSheet->m_nFoldSheetNumber > 0) 
		nFoldSheetIndex = pFoldSheet->GetIndex();
	else
	{
		if (bSample)
		{
		}
		else
			if (pDoc)	// this function is used even w/o doc in production profiles layout pool -> use locally here
			{
				CFoldSheet* pLastFoldSheet = pDoc->m_Bookblock.GetLastFoldSheet(-nProductIndex - 1);
				if (pLastFoldSheet)
					nFoldSheetIndex = -(pLastFoldSheet->m_nFoldSheetNumber + 1);
			}
	}
	
	int nOffset = (pFoldSheet->m_nFoldSheetNumber > 0) ? 0 : 1;
	if (pFoldSheet->m_nFoldSheetNumber < 0)
		pFoldSheet = NULL;

	int nNumAdded = 0;
	for (int ny = 0; (ny < nNumFoldSheetsY) && (nNUp > 0); ny++)
	{
		fFoldSheetPosX = fXPos;
		for (int nx = 0; (nx < nNumFoldSheetsX) && (nNUp > 0); nx++)
		{
			AddFoldSheetLayer(&startFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, rProductPart, nRotate, &rPrintSheet);
			fFoldSheetPosX += fGrossWidth;

			CFoldSheetLayerRef layerRef(nFoldSheetIndex, nLayerIndex);
			rPrintSheet.m_FoldSheetLayerRefs.Add(layerRef);
			nNumAdded++;

			if (pFoldSheet)
			{
				if (bSample)
				{
					nFoldSheetIndex--;
				}
				else
					if (pDoc)	// this function is used even w/o doc in production profiles layout pool -> use locally here
					{
						pFoldSheet = pDoc->m_Bookblock.GetNextFoldSheetLayer(pFoldSheet, nLayerIndex, nProductPartIndex, strFoldScheme);
						if (pFoldSheet)
							nFoldSheetIndex = pFoldSheet->GetIndex();
					}
			}

			if ( ! pFoldSheet)
			{
				if (bSample)
				{
					nFoldSheetIndex--;
				}
				else
					if (pDoc)	// this function is used even w/o doc in production profiles layout pool -> use locally here
					{
						nFoldSheetIndex = -(pDoc->m_Bookblock.GetNumFoldSheets(-nProductIndex - 1) + 1) - nOffset;
						nOffset++;
					}
			}

			nNUp--;
		}
		fFoldSheetPosY += fGrossHeight;
	}

	if (bReposition)
	{
		if (pPrintingProfile)
		{
			m_FrontSide.PositionLayoutSide(pPrintingProfile->m_press.m_pressDevice.m_fGripper, &pPrintingProfile->m_press.m_pressDevice);
			m_BackSide.PositionLayoutSide(pPrintingProfile->m_press.m_pressDevice.m_fGripper, &pPrintingProfile->m_press.m_pressDevice);
		}
	}

	return nNumAdded;
}

int CLayout::MakeWorkAndTurn(BOOL bSample, CPrintSheet& rPrintSheet, CFoldSheet* pFoldSheet, int nLayerIndex, int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition, CRect rcPlaceArea)
{
	int nNumFoldSheetsX, nNumFoldSheetsY;
	nNUp = MakeNUp(bSample, rPrintSheet, pFoldSheet, nLayerIndex, nNUp, nHeadPosition, pPrintingProfile, bReposition, rcPlaceArea, &nNumFoldSheetsX, &nNumFoldSheetsY);
	if (nNUp <= 1)
		return 0;
	if (nNUp % 2)
		return 0;

	if ( (nNumFoldSheetsX % 2) || (nNumFoldSheetsX == 1) )
		return 0;

	SetWorkStyle(WORK_AND_TURN);

	m_strLayoutName = theApp.settings.m_szWorkAndTurnName;

	return nNUp;
}

int CLayout::MakeWorkAndTumble(BOOL bSample, CPrintSheet& rPrintSheet, CFoldSheet* pFoldSheet, int nLayerIndex, int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition, CRect rcPlaceArea)
{
	int nNumFoldSheetsX, nNumFoldSheetsY;
	nNUp = MakeNUp(bSample, rPrintSheet, pFoldSheet, nLayerIndex, nNUp, nHeadPosition, pPrintingProfile, bReposition, rcPlaceArea, &nNumFoldSheetsX, &nNumFoldSheetsY);
	if (nNUp <= 1)
		return 0;
	if (nNUp % 2)
		return 0;

	if ( (nNumFoldSheetsY % 2) || (nNumFoldSheetsY == 1) )
		return 0;

	SetWorkStyle(WORK_AND_TUMBLE);

	if (bReposition)
		AlignLayoutObjects(YCENTER);

	m_strLayoutName = theApp.settings.m_szWorkAndTumbleName;

	return nNUp;
}

CLayoutObject* CLayout::FindMarkObject(CString strMarkName)
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_strFormatName == strMarkName)
			return &rLayoutObject;
	}
	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_BackSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_strFormatName == strMarkName)
			return &rLayoutObject;
	}
	return NULL;
}

float CLayout::CalcUtilization(CPrintSheet* pPrintSheet, CFlatProduct* pFlatProduct)
{
	float	 fSurface = 0.0f;
	POSITION pos	  = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::ControlMark)
			continue;
		if (pFlatProduct)
			if (&rLayoutObject.GetCurrentFlatProduct(pPrintSheet) != pFlatProduct)
				continue;

		float fLeft = 0.0f, fBottom = 0.0f, fRight = 0.0f, fTop = 0.0f;
		fLeft	= (rLayoutObject.m_LeftNeighbours.GetSize())  ? rLayoutObject.GetLeftWithBorder()	: rLayoutObject.GetLeft();		
		fBottom = (rLayoutObject.m_LowerNeighbours.GetSize()) ? rLayoutObject.GetBottomWithBorder() : rLayoutObject.GetBottom();
		fRight  = (rLayoutObject.m_RightNeighbours.GetSize()) ? rLayoutObject.GetRightWithBorder()	: rLayoutObject.GetRight();
		fTop    = (rLayoutObject.m_UpperNeighbours.GetSize()) ? rLayoutObject.GetTopWithBorder()	: rLayoutObject.GetTop();
		fSurface += (fRight - fLeft) * (fTop - fBottom);
	}

	//CProductionProfile& rProfile = GetProductionProfile();
	//float fPaperSurface = (m_FrontSide.m_fPaperWidth  - rProfile.m_postpress.m_cutting.m_fLeftBorder  - rProfile.m_postpress.m_cutting.m_fRightBorder) * 
	//					  (m_FrontSide.m_fPaperHeight - rProfile.m_postpress.m_cutting.m_fLowerBorder - rProfile.m_postpress.m_cutting.m_fUpperBorder);
	float fPaperSurface = m_FrontSide.m_fPaperWidth * m_FrontSide.m_fPaperHeight;
	if (fPaperSurface > 0.0f)
		return fSurface / fPaperSurface;
	else
		return 0.0f;
}

float CLayout::CalcUtilization(CPrintSheet* pPrintSheet, CFoldSheet* pFoldSheet)
{
	float fSurface = 0.0f;
	for (int i = 0; i < m_FoldSheetLayerRotation.GetSize(); i++)
	{
		if (pFoldSheet)
			if (pPrintSheet->GetFoldSheet(i) != pFoldSheet)
				continue;

		float fLeft, fTop, fRight, fBottom, fWidth, fHeight;
		fLeft = fTop = fRight = fBottom = fWidth = fHeight = 0.0f;
		m_FrontSide.CalcBoundingRectObjects(&fLeft, &fBottom, &fRight, &fTop, &fWidth, &fHeight, FALSE, i, FALSE, NULL);

		fSurface += (fRight - fLeft) * (fTop - fBottom);
	}

	CPrintingProfile& rProfile = GetPrintingProfile();
	float fPaperSurface = (m_FrontSide.m_fPaperWidth  - rProfile.m_cutting.m_fLeftBorder  - rProfile.m_cutting.m_fRightBorder) * 
						  (m_FrontSide.m_fPaperHeight - rProfile.m_cutting.m_fLowerBorder - rProfile.m_cutting.m_fUpperBorder);
	if (fPaperSurface > 0.0f)
		return fSurface / fPaperSurface;
	else
		return 0.0f;
}

int CLayout::GetTotalNUp()
{
	int nNUp = 0;
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::ControlMark)
			continue;
		nNUp++;
	}
	return nNUp;
}

void CLayout::SelectLayoutVariant(CPrintSheet& rPrintSheet, int nIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( (nIndex < 0) || (nIndex >= m_variants.GetSize()) )
		return;

	rPrintSheet.LinkLayout(this);

	RemoveMarkObjects(-1, TRUE);

	while (m_FoldSheetLayerRotation.GetSize())
	{
		RemoveFoldSheetLayer(0, NULL, FALSE);
		if (rPrintSheet.m_FoldSheetLayerRefs.GetSize())
			rPrintSheet.m_FoldSheetLayerRefs.RemoveAt(0);
	}

	m_FrontSide.m_ObjectList.RemoveAll();
	m_BackSide.m_ObjectList.RemoveAll();

	rPrintSheet.m_flatProductRefs.RemoveAll();

	CPressParams& rPressParams = GetPrintingProfile().m_press;

	CLayoutVariant& variant = m_variants.GetAt(nIndex);

	m_FrontSide.m_fPaperWidth		 = variant.m_fWidth;
	m_FrontSide.m_fPaperHeight		 = variant.m_fHeight;
	m_FrontSide.m_strFormatName		 = variant.m_strFormatName;
	m_FrontSide.m_nPaperGrain		 = variant.m_nPaperGrain;

	m_BackSide.m_fPaperWidth		 = variant.m_fWidth;
	m_BackSide.m_fPaperHeight		 = variant.m_fHeight;
	m_BackSide.m_strFormatName		 = variant.m_strFormatName;
	m_BackSide.m_nPaperGrain		 = variant.m_nPaperGrain;

	CArray <int, int> foldSheetIDList;
	BOOL bHasBackSide = FALSE;
	for (int i = 0; i < variant.m_objects.GetSize(); i++)
	{
		bool bTurned = (variant.m_objects[i].m_nRotation == 1) ? true : false;
		if (variant.m_objects[i].m_nObjectType == CLayoutVariantObject::FoldSheet)
		{
			POSITION pos = variant.m_foldSheetList.FindIndex(variant.m_objects[i].m_nObjectID);
			if (pos)
			{
				float fXPos = m_FrontSide.m_fPaperLeft   + variant.m_objects[i].m_fPositionX;
				float fYPos = m_FrontSide.m_fPaperBottom + variant.m_objects[i].m_fPositionY;

				CFoldSheet& rNewFoldSheet = variant.m_foldSheetList.GetAt(pos);
				int nLayerIndex = 0;
				BOOL bFound = FALSE;
				for (int nIndex = 0; nIndex < foldSheetIDList.GetSize(); nIndex++)
				{
					if (foldSheetIDList[nIndex] == variant.m_objects[i].m_nObjectID)
					{
						bFound = TRUE;
						break;
					}
				}
				if ( ! bFound)
				{
					pDoc->m_Bookblock.AddFoldSheetsSimple(&rNewFoldSheet, 1);
					foldSheetIDList.Add(variant.m_objects[i].m_nObjectID);
				}

				int nFoldSheetGrain = GRAIN_EITHER;
				CFoldSheetLayer* pLayer = rNewFoldSheet.GetLayer(0);
				if (pLayer)
				{
					if (pLayer->m_HeadPositionArray.GetSize())
					{
						int nHeadPosition = pLayer->m_HeadPositionArray[0];
						switch (nHeadPosition)
						{
						case TOP:
						case BOTTOM:	nFoldSheetGrain = GRAIN_VERTICAL;	break;
						case LEFT:
						case RIGHT:		nFoldSheetGrain = GRAIN_HORIZONTAL; break;
						}
					}
				}
				if ( (nFoldSheetGrain != GRAIN_EITHER) && (nFoldSheetGrain != m_FrontSide.m_nPaperGrain) )
					bTurned = true;

				AddFoldSheetLayer(&rNewFoldSheet, nLayerIndex, fXPos, fYPos, rNewFoldSheet.GetProductPart(), (bTurned) ? ROTATE90 : ROTATE0);

				CFoldSheetLayerRef layerRef(pDoc->m_Bookblock.m_FoldSheetList.GetSize() - 1, nLayerIndex);
				rPrintSheet.m_FoldSheetLayerRefs.Add(layerRef);
				bHasBackSide = TRUE;
			}
		}
		else
		{
			int			  nFlatProductIndex		= variant.m_objects[i].m_nObjectID; 
			int			  nFlatProductRefIndex	= rPrintSheet.MakeFlatProductRefIndex(nFlatProductIndex);
			CFlatProduct& rFlatProduct			= pDoc->m_flatProducts.FindByIndex(nFlatProductIndex);
			if ( (rFlatProduct.m_nPaperGrain != GRAIN_EITHER) && (rFlatProduct.m_nPaperGrain != m_FrontSide.m_nPaperGrain) )
				bTurned = true;
			int	nHeadPosition = (bTurned) ? LEFT : TOP;
			
			if (rFlatProduct.m_nNumPages > 1)
				bHasBackSide = TRUE;

			if ( (nFlatProductIndex < 0) || (nFlatProductIndex >= pDoc->m_flatProducts.GetSize()) )
				continue;

			float fWidth  = ( (nHeadPosition == TOP) ||  (nHeadPosition == BOTTOM) ) ? rFlatProduct.m_fTrimSizeWidth  : rFlatProduct.m_fTrimSizeHeight;
			float fHeight = ( (nHeadPosition == TOP) ||  (nHeadPosition == BOTTOM) ) ? rFlatProduct.m_fTrimSizeHeight : rFlatProduct.m_fTrimSizeWidth;
			float fLeftBorder = 0.0f, fBottomBorder = 0.0f; 
			switch (nHeadPosition)
			{
			case TOP:		fLeftBorder	  = rFlatProduct.m_fLeftTrim;
							fBottomBorder = rFlatProduct.m_fBottomTrim;
							break;
			case RIGHT:		fLeftBorder	  = rFlatProduct.m_fBottomTrim;
							fBottomBorder = rFlatProduct.m_fRightTrim;
							break;
			case BOTTOM:	fLeftBorder	  = rFlatProduct.m_fRightTrim;
							fBottomBorder = rFlatProduct.m_fTopTrim;
							break;
			case LEFT:		fLeftBorder	  = rFlatProduct.m_fTopTrim;
							fBottomBorder = rFlatProduct.m_fLeftTrim;
							break;
			}

			float fXPos = m_FrontSide.m_fPaperLeft   + variant.m_objects[i].m_fPositionX + fLeftBorder;
			float fYPos = m_FrontSide.m_fPaperBottom + variant.m_objects[i].m_fPositionY + fBottomBorder;
			CLayoutObject layoutObject; 
			layoutObject.CreateFlatProduct(fXPos, fYPos, fWidth, fHeight, nHeadPosition, nFlatProductRefIndex);
			m_FrontSide.m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pFrontLayoutObject = &m_FrontSide.m_ObjectList.GetTail(); 

			layoutObject.CreateFlatProduct(fXPos, fYPos, fWidth, fHeight, nHeadPosition, nFlatProductRefIndex);
			m_BackSide.m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pBackLayoutObject = &m_BackSide.m_ObjectList.GetTail();

			pBackLayoutObject->AlignToCounterPart(pFrontLayoutObject);
		}
	}

	m_FrontSide.PositionLayoutSide();
	m_BackSide.PositionLayoutSide();

	m_FrontSide.Invalidate();
	m_BackSide.Invalidate();

	int nWorkStyle = (bHasBackSide) ? WORK_AND_BACK : WORK_SINGLE_SIDE;
	SetWorkStyle(nWorkStyle);	

	rPrintSheet.m_nQuantityTotal = variant.m_nNumSheets;

	rPressParams.m_sheetData.m_fWidth		= variant.m_fWidth;
	rPressParams.m_sheetData.m_fHeight		= variant.m_fHeight; 
	rPressParams.m_sheetData.m_strSheetName = variant.m_strFormatName; 
	rPressParams.m_sheetData.m_nPaperGrain	= variant.m_nPaperGrain;
	BOOL bFound = FALSE;
	for (int i = 0; i < rPressParams.m_availableSheets.GetSize(); i++)
	{
		if (rPressParams.m_availableSheets[i].m_strSheetName == variant.m_strFormatName)
		{
			bFound = TRUE;
			break;
		}
	}
	if ( ! bFound)
		rPressParams.m_availableSheets.Add(rPressParams.m_sheetData);

	rPressParams.m_nWorkStyle = nWorkStyle;

	AnalyzeMarks(FALSE);

	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();	
	pDoc->m_PrintSheetList.ReorganizeColorInfos();
	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	pDoc->m_PrintSheetList.InitializeFlatProductTypeLists();
	pDoc->m_Bookblock.Reorganize();//NULL/*pTargetPrintSheet*/, NULL, rNewFoldSheet.m_nProductPartIndex);//, pTargetLayout);
}

int	CLayout::GetSuitableFoldSheetObjects(int nNX, int nNY)
{
	CLayoutObjectList objectList;
	for (int nComponentRefIndex = 0; nComponentRefIndex < m_FoldSheetLayerRotation.GetSize(); nComponentRefIndex++)
	{
		objectList.RemoveAll();
		POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
			if (rLayoutObject.IsFlatProduct())
				continue;
			if (rLayoutObject.m_nComponentRefIndex == nComponentRefIndex)
				objectList.AddTail(rLayoutObject);
		}

		CLayoutObjectMatrix objectMatrix(objectList, CLayoutObjectMatrix::TakePages);
		int nCurNY = objectMatrix.GetSize();
		int nCurNX = (nCurNY > 0) ? objectMatrix[0].GetSize() : 0;
		if ( (nCurNX == nNX) && (nCurNY == nNY) )
			return nComponentRefIndex;
	}

	return -1;
}

CSize CLayout::GetFoldSheetLayerMatrix(int nComponentRefIndex)
{
	if (nComponentRefIndex >= m_FoldSheetLayerRotation.GetSize())
		return CSize(0, 0);

	CLayoutObjectList objectList;
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.IsFlatProduct())
			continue;
		if (rLayoutObject.m_nComponentRefIndex == nComponentRefIndex)
			objectList.AddTail(rLayoutObject);
	}

	CLayoutObjectMatrix objectMatrix(objectList, CLayoutObjectMatrix::TakePages);
	int nNY = objectMatrix.GetSize();
	int nNX = (nNY > 0) ? objectMatrix[0].GetSize() : 0;

	return CSize(nNX, nNY);
}

void CLayout::UpdatePressParams(CPressDevice& rPressDeviceFront, CPressDevice& rPressDeviceBack)
{
	CPressParams& rPressParams = GetPrintingProfile().m_press;

	rPressParams.m_sheetData.m_fWidth		= m_FrontSide.m_fPaperWidth;
	rPressParams.m_sheetData.m_fHeight		= m_FrontSide.m_fPaperHeight; 
	rPressParams.m_sheetData.m_strSheetName = m_FrontSide.m_strFormatName; 
	rPressParams.m_sheetData.m_nPaperGrain	= m_FrontSide.m_nPaperGrain;
	BOOL bFound = FALSE;
	for (int i = 0; i < rPressParams.m_availableSheets.GetSize(); i++)
	{
		if (rPressParams.m_availableSheets[i].m_strSheetName == m_FrontSide.m_strFormatName)
		{
			bFound = TRUE;
			break;
		}
	}
	if ( ! bFound)
		rPressParams.m_availableSheets.Add(rPressParams.m_sheetData);

	rPressParams.m_nWorkStyle = GetCurrentWorkStyle();

	rPressParams.m_pressDevice		   = rPressDeviceFront;
	rPressParams.m_backSidePressDevice = rPressDeviceBack;
}

void CLayout::SetObjectsStatusAll(int nStatus)
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		rLayoutObject.m_nStatus = nStatus;
	}
	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		rLayoutObject.m_nStatus = nStatus;
	}
}

void CLayout::SetObjectsStatusFoldSheetLayer(int nComponentRefIndex, int nStatus)
{
	POSITION pos = m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.IsFlatProduct())
			continue;
		if (rLayoutObject.m_nComponentRefIndex != nComponentRefIndex)
			continue;
		rLayoutObject.m_nStatus = nStatus;
	}
	pos = m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_FrontSide.m_ObjectList.GetNext(pos);
		if (rLayoutObject.IsFlatProduct())
			continue;
		if (rLayoutObject.m_nComponentRefIndex != nComponentRefIndex)
			continue;
		rLayoutObject.m_nStatus = nStatus;
	}
}

void CLayout::ReorganizeColorInfos()
{
	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	while (pPrintSheet)
	{
		pPrintSheet->ReorganizeColorInfos();
		pPrintSheet = GetNextPrintSheet(pPrintSheet);
	}
}

BOOL CLayout::IsUniform()
{
	return TRUE;

	//if (HasFlatProducts())
	//	return FALSE;
	//else
	//	if (m_FoldSheetLayerRotation.GetSize() > 1)
	//		return FALSE;
	//	else
	//		return TRUE;
}


// helper function for LayoutList elements
template <> void AFXAPI SerializeElements <CLayout> (CArchive& ar, CLayout* pLayout, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CLayout));

	if (ar.IsStoring())
	{
		ar << pLayout->m_strLayoutName;
		ar << pLayout->m_strNotes;
		ar << pLayout->m_strLastAssignedMarkset;
		ar << pLayout->m_nProductType;
		ar << pLayout->m_bAskForChangeToWorkAndBack;
		ar << pLayout->m_bAskForChangeToWorkAndTurnTumble;
		ar << pLayout->m_nPrintingProfileRef;

		pLayout->m_FoldSheetLayerRotation.Serialize(ar);
		pLayout->m_FoldSheetLayerTurned.Serialize(ar);
		pLayout->m_markList.Serialize(ar);
		pLayout->m_FrontSide.Serialize(ar);
		pLayout->m_BackSide.Serialize(ar);
		pLayout->m_variants.Serialize(ar);
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		BOOL bDummy;
		switch (nVersion)
		{
		case 1:
			ar >> pLayout->m_strLayoutName;
			ar >> pLayout->m_strNotes;
			pLayout->m_strLastAssignedMarkset = "";
			pLayout->m_nProductType = 0;
			pLayout->m_bAskForChangeToWorkAndBack = TRUE;
			pLayout->m_bAskForChangeToWorkAndTurnTumble = TRUE;
			break;
		case 2:
			ar >> pLayout->m_strLayoutName;
			ar >> pLayout->m_strNotes;
			ar >> pLayout->m_strLastAssignedMarkset;
			pLayout->m_nProductType = 0;
			pLayout->m_bAskForChangeToWorkAndBack = TRUE;
			pLayout->m_bAskForChangeToWorkAndTurnTumble = TRUE;
			break;
		case 3:
			ar >> pLayout->m_strLayoutName;
			ar >> pLayout->m_strNotes;
			ar >> pLayout->m_strLastAssignedMarkset;
			ar >> pLayout->m_nProductType;
			pLayout->m_bAskForChangeToWorkAndBack = TRUE;
			pLayout->m_bAskForChangeToWorkAndTurnTumble = TRUE;
			break;
		case 4:
		case 5:
			ar >> pLayout->m_strLayoutName;
			ar >> pLayout->m_strNotes;
			ar >> pLayout->m_strLastAssignedMarkset;
			ar >> pLayout->m_nProductType;
			ar >> bDummy; //pLayout->m_bNOCheckForProductType;
			pLayout->m_bAskForChangeToWorkAndBack		= (bDummy) ? FALSE : TRUE;
			pLayout->m_bAskForChangeToWorkAndTurnTumble = (bDummy) ? FALSE : TRUE;
			break;
		case 6:
			ar >> pLayout->m_strLayoutName;
			ar >> pLayout->m_strNotes;
			ar >> pLayout->m_strLastAssignedMarkset;
			ar >> pLayout->m_nProductType;
			ar >> pLayout->m_bAskForChangeToWorkAndBack;
			ar >> pLayout->m_bAskForChangeToWorkAndTurnTumble;
			break;
		case 7:
		case 8:
			ar >> pLayout->m_strLayoutName;
			ar >> pLayout->m_strNotes;
			ar >> pLayout->m_strLastAssignedMarkset;
			ar >> pLayout->m_nProductType;
			ar >> pLayout->m_bAskForChangeToWorkAndBack;
			ar >> pLayout->m_bAskForChangeToWorkAndTurnTumble;
			ar >> pLayout->m_nPrintingProfileRef;
			break;
		}

		pLayout->m_FoldSheetLayerRotation.Serialize(ar);
		pLayout->m_FoldSheetLayerTurned.Serialize(ar);
		if (nVersion > 4)
			pLayout->m_markList.Serialize(ar);
		pLayout->m_FrontSide.Serialize(ar);
		pLayout->m_BackSide.Serialize(ar);
		if (nVersion > 7)
			pLayout->m_variants.Serialize(ar);

		pLayout->m_FrontSide.m_nLayoutSide	= FRONTSIDE;
		pLayout->m_BackSide.m_nLayoutSide	= BACKSIDE;
		if (pLayout->m_FrontSide.m_strSideName.IsEmpty())
			pLayout->m_FrontSide.m_strSideName = theApp.settings.m_szFrontName;
		if (pLayout->m_BackSide.m_strSideName.IsEmpty())
			pLayout->m_BackSide.m_strSideName = theApp.settings.m_szBackName;
	}
}




/////////////////////////////////////////////////////////////////////////////
// CLayoutVariant

IMPLEMENT_SERIAL(CLayoutVariant, CLayoutVariant, VERSIONABLE_SCHEMA | 1)

CLayoutVariant::CLayoutVariant() 
{ 
	m_strFormatName = _T("");
	m_fWidth		= 0.0f; 
	m_fHeight		= 0.0f; 
	m_nPaperGrain	= GRAIN_EITHER; 
	m_nNumSheets	= 0;
	m_objects.RemoveAll();
	m_foldSheetList.RemoveAll();
}

CLayoutVariant::CLayoutVariant(const CLayoutVariant& rLayoutVariant)
{
	*this = rLayoutVariant;
}

const CLayoutVariant& CLayoutVariant::operator=(const CLayoutVariant& rLayoutVariant)
{
	m_strFormatName = rLayoutVariant.m_strFormatName;
	m_fWidth		= rLayoutVariant.m_fWidth; 
	m_fHeight		= rLayoutVariant.m_fHeight; 
	m_nPaperGrain	= rLayoutVariant.m_nPaperGrain; 
	m_nNumSheets	= rLayoutVariant.m_nNumSheets; 

	m_objects.RemoveAll();
	m_objects.Append(rLayoutVariant.m_objects);

	m_foldSheetList.RemoveAll();
	POSITION pos = rLayoutVariant.m_foldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet foldSheet = rLayoutVariant.m_foldSheetList.GetNext(pos);
		m_foldSheetList.AddTail(foldSheet);
	}

	return *this;
}

template <> void AFXAPI SerializeElements <CLayoutVariant> (CArchive& ar, CLayoutVariant* pLayoutVariant, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CLayoutVariant));
	UINT nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pLayoutVariant->m_strFormatName;
			ar << pLayoutVariant->m_fWidth;
			ar << pLayoutVariant->m_fHeight;
			ar << pLayoutVariant->m_nPaperGrain;
			ar << pLayoutVariant->m_nNumSheets;
			pLayoutVariant->m_objects.Serialize(ar);
		}
		else
		{
			switch (nVersion)
			{
			case 1:	ar >> pLayoutVariant->m_strFormatName;
					ar >> pLayoutVariant->m_fWidth;
					ar >> pLayoutVariant->m_fHeight;
					ar >> pLayoutVariant->m_nPaperGrain;
					ar >> pLayoutVariant->m_nNumSheets;
					pLayoutVariant->m_objects.Serialize(ar);
					break;
			}
		}

		pLayoutVariant++;
	}
}
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CLayoutVariantObject

IMPLEMENT_SERIAL(CLayoutVariantObject, CLayoutVariantObject, VERSIONABLE_SCHEMA | 1)

CLayoutVariantObject::CLayoutVariantObject() 
{ 
	m_nObjectType	= FlatProduct;
	m_nObjectID		= -1;
	m_fPositionX	= 0.0f;
	m_fPositionY	= 0.0f;
	m_nRotation		= 0;
}

CLayoutVariantObject::CLayoutVariantObject(const CLayoutVariantObject& rLayoutVariantObject)
{
	*this = rLayoutVariantObject;
}

const CLayoutVariantObject& CLayoutVariantObject::operator=(const CLayoutVariantObject& rLayoutVariantObject)
{
	m_nObjectType	= rLayoutVariantObject.m_nObjectType;
	m_nObjectID		= rLayoutVariantObject.m_nObjectID; 
	m_fPositionX	= rLayoutVariantObject.m_fPositionX; 
	m_fPositionY	= rLayoutVariantObject.m_fPositionY; 
	m_nRotation		= rLayoutVariantObject.m_nRotation;

	return *this;
}

template <> void AFXAPI SerializeElements <CLayoutVariantObject> (CArchive& ar, CLayoutVariantObject* pLayoutVariantObject, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CLayoutVariantObject));
	UINT nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pLayoutVariantObject->m_nObjectType;
			ar << pLayoutVariantObject->m_nObjectID; 
			ar << pLayoutVariantObject->m_fPositionX; 
			ar << pLayoutVariantObject->m_fPositionY; 
			ar << pLayoutVariantObject->m_nRotation;
		}
		else
		{
			switch (nVersion)
			{
			case 1:	ar >> pLayoutVariantObject->m_nObjectType;
					ar >> pLayoutVariantObject->m_nObjectID; 
					ar >> pLayoutVariantObject->m_fPositionX; 
					ar >> pLayoutVariantObject->m_fPositionY; 
					ar >> pLayoutVariantObject->m_nRotation;
					break;
			}
		}

		pLayoutVariantObject++;
	}
}
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CSheet

IMPLEMENT_SERIAL(CSheet, CSheet, VERSIONABLE_SCHEMA | 2)

CSheet::CSheet()
{
	m_strSheetName.Empty();
	m_fWidth  = 0.0f;
	m_fHeight = 0.0f;
	m_nPaperGrain = GRAIN_VERTICAL;
}

CSheet::CSheet(CSheet& rSheet)
{
	*this = rSheet;
}

const CSheet& CSheet::operator=(const CSheet& rSheet)
{
	Copy(rSheet);

	return *this;
}

BOOL CSheet::operator==(const CSheet& rSheet)
{
	if (m_strSheetName != rSheet.m_strSheetName)	return FALSE;
	if (m_fWidth	   != rSheet.m_fWidth)			return FALSE;
	if (m_fHeight	   != rSheet.m_fHeight)			return FALSE;
	if (m_nPaperGrain  != rSheet.m_nPaperGrain)		return FALSE;
	if (m_FanoutList   != rSheet.m_FanoutList)		return FALSE;

	return TRUE;
}

BOOL CSheet::operator!=(const CSheet& rSheet)
{
	return (*this == rSheet) ? FALSE : TRUE;
}

void CSheet::Copy(const CSheet& rSheet)
{
	m_strSheetName = rSheet.m_strSheetName;
	m_fWidth	   = rSheet.m_fWidth;
	m_fHeight	   = rSheet.m_fHeight;
	m_nPaperGrain  = rSheet.m_nPaperGrain;
	m_FanoutList   = rSheet.m_FanoutList;
}

// helper function for CSheet list elements
template <> void AFXAPI SerializeElements <CSheet> (CArchive& ar, CSheet* pSheet, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CSheet));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();
		
	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pSheet->m_strSheetName;
			ar << pSheet->m_fWidth;
			ar << pSheet->m_fHeight;
			ar << pSheet->m_nPaperGrain;
			pSheet->m_FanoutList.Serialize(ar);
		}
		else
		{
			switch (nVersion)
			{
			case 1:
				ar >> pSheet->m_strSheetName;
				ar >> pSheet->m_fWidth;
				ar >> pSheet->m_fHeight;
				pSheet->m_FanoutList.Serialize(ar);
				break;
			case 2:
				ar >> pSheet->m_strSheetName;
				ar >> pSheet->m_fWidth;
				ar >> pSheet->m_fHeight;
				ar >> pSheet->m_nPaperGrain;
				pSheet->m_FanoutList.Serialize(ar);
				break;
			}
		}
		pSheet++;
	}
}


void AFXAPI DestructElements(CSheet* pSheet, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pSheet->m_strSheetName.Empty();
		pSheet->m_FanoutList.RemoveAll();
		pSheet++;
	}
}


POSITION CSheetList::FindByName(const CString& strName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		if (GetAt(pos).m_strSheetName == strName)
			return pos;
		GetNext(pos);
	}

	return NULL;
}

CSheet* CSheetList::FindBySize(float fWidth, float fHeight)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CSheet& rSheet = GetNext(pos);
		if ( (fabs(rSheet.m_fWidth - fWidth) < 0.01) && (fabs(rSheet.m_fHeight - fHeight) < 0.01) )
			return &rSheet;
	}

	return NULL;
}

int CSheetList::FindIndexByName(const CString& strName)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (GetAt(pos).m_strSheetName == strName)
			return nIndex;
		nIndex++;
		GetNext(pos);
	}

	return -1;
}

BOOL CSheetList::Load()
{
	if (theApp.strVersion < "3.0.0.6")
		return FALSE;
	else
	{
		CFileException fileException;
		CString		   strFullPath;
		
		strFullPath = theApp.GetDataFolder(); // TODO: Verify the path
		strFullPath+= "\\Sheets.def";
		
		CFile file;
		if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
		{
			TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
			return FALSE; 
		}
		
		CArchive archive(&file, CArchive::load);
		
		RemoveAll();
		Serialize(archive);
		
		archive.Close();
		file.Close();
		
		return TRUE;
	}
}


/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CFanoutList

IMPLEMENT_SERIAL(CFanoutDefinition, CFanoutDefinition, VERSIONABLE_SCHEMA | 1)


CFanoutDefinition::CFanoutDefinition() 
{
}

CFanoutDefinition::CFanoutDefinition(CString& strColorName, float fFanoutX, float fFanoutY) 
{
	m_strColorName = strColorName; 
	m_fFanoutX	   = fFanoutX; 
	m_fFanoutY	   = fFanoutY;
}

const CFanoutDefinition& CFanoutDefinition::operator=(const CFanoutDefinition& rFanoutDefinition)
{
	m_strColorName = rFanoutDefinition.m_strColorName; 
	m_fFanoutX	   = rFanoutDefinition.m_fFanoutX; 
	m_fFanoutY	   = rFanoutDefinition.m_fFanoutY;

	return *this;
}

// helper function for FanoutList elements
template <> void AFXAPI SerializeElements <CFanoutDefinition> (CArchive& ar, CFanoutDefinition* pFanoutDefinition, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CFanoutDefinition));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pFanoutDefinition->m_strColorName;
			ar << pFanoutDefinition->m_fFanoutX;
			ar << pFanoutDefinition->m_fFanoutY;
		}
		else
		{
			switch (nVersion)
			{
			case 1:
				ar >> pFanoutDefinition->m_strColorName;
				ar >> pFanoutDefinition->m_fFanoutX;
				ar >> pFanoutDefinition->m_fFanoutY;
				break;
			}
		}
		pFanoutDefinition++;
	}
}

void AFXAPI DestructElements(CFanoutDefinition* pFanoutDefinition, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pFanoutDefinition->m_strColorName.Empty();
		pFanoutDefinition++;
	}
}


const CFanoutList& CFanoutList::operator=(const CFanoutList& rFanoutList)
{
	Copy(rFanoutList);

	return *this;
}


BOOL CFanoutList::operator==(const CFanoutList& rFanoutList)
{
	if (GetSize() != rFanoutList.GetSize())		
		return FALSE;
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_strColorName != rFanoutList[i].m_strColorName)	return FALSE; 
		if (GetAt(i).m_fFanoutX	    != rFanoutList[i].m_fFanoutX)		return FALSE; 
		if (GetAt(i).m_fFanoutY	    != rFanoutList[i].m_fFanoutY)		return FALSE;
	}

	return TRUE;
}


BOOL CFanoutList::operator!=(const CFanoutList& rFanoutList)
{
	return (*this == rFanoutList) ? FALSE : TRUE;
}


int CFanoutList::FindColor(const CString& strColorName)
{
	for (int i = 0; i < GetSize(); i++)
		if (ElementAt(i).m_strColorName == strColorName)
			return i;

	return -1;
}


float CFanoutList::GetFanoutX(const CString& strColorName)
{
	int nIndex = FindColor(strColorName);

	if (nIndex == -1)
		return 0.0f;
	else
		return ElementAt(nIndex).m_fFanoutX;
}


float CFanoutList::GetFanoutY(const CString& strColorName)
{
	int nIndex = FindColor(strColorName);

	if (nIndex == -1)
		return 0.0f;
	else
		return ElementAt(nIndex).m_fFanoutY;
}

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CLayoutSide

IMPLEMENT_SERIAL(CLayoutSide, CObject, VERSIONABLE_SCHEMA | 4)

CLayoutSide::CLayoutSide()
{
	m_fPaperLeft	= 0.0f;
	m_fPaperBottom	= 0.0f;
	m_fPaperRight	= 0.0f;
	m_fPaperTop		= 0.0f;
	m_fPaperWidth	= 0.0f;
	m_fPaperHeight	= 0.0f;
	m_nPaperGrain	= GRAIN_VERTICAL;	   

	m_bInvalid = TRUE;
	m_ObjectList.m_pLayoutSide = this;
	m_cutBlocks.m_pLayoutSide  = this;
}

// operator overload for CLayoutSide - needed by CList functions:

const CLayoutSide& CLayoutSide::operator=(const CLayoutSide& rLayoutSide)
{
	Copy(rLayoutSide);	

	return *this;
}

void CLayoutSide::ReinitLinks(CLayout* pLayout)
{
	m_pLayout = pLayout;
	m_ObjectList.m_pLayoutSide = this;
}

void CLayoutSide::Copy(const CLayoutSide& rLayoutSide)
{
	m_strSideName		   = rLayoutSide.m_strSideName;
	m_nLayoutSide          = rLayoutSide.m_nLayoutSide;
	m_fPaperLeft		   = rLayoutSide.m_fPaperLeft;
	m_fPaperBottom		   = rLayoutSide.m_fPaperBottom;
	m_fPaperRight		   = rLayoutSide.m_fPaperRight;
	m_fPaperTop			   = rLayoutSide.m_fPaperTop;
	m_fPaperWidth		   = rLayoutSide.m_fPaperWidth;
	m_fPaperHeight		   = rLayoutSide.m_fPaperHeight;
	m_strFormatName		   = rLayoutSide.m_strFormatName;
	m_nPaperOrientation    = rLayoutSide.m_nPaperOrientation;
	m_nPaperGrain		   = rLayoutSide.m_nPaperGrain;
	m_nPaperRefEdge		   = rLayoutSide.m_nPaperRefEdge;
 	m_nPressDeviceIndex    = rLayoutSide.m_nPressDeviceIndex;
	m_bAdjustFanout		   = rLayoutSide.m_bAdjustFanout;

	m_FanoutList.RemoveAll();
 	m_FanoutList		   = rLayoutSide.m_FanoutList;

 	m_ObjectList.RemoveAll();  
 	m_ObjectList.Copy(rLayoutSide.m_ObjectList);  

	m_DefaultPlates.RemoveAll();
	m_DefaultPlates.Copy(rLayoutSide.m_DefaultPlates);

	m_cutBlocks.RemoveAll();
	m_cutBlocks.Copy(rLayoutSide.m_cutBlocks);

	m_freeAreas.RemoveAll();
	m_freeAreas.Copy(rLayoutSide.m_freeAreas);
}


void CLayoutSide::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CLayoutSide));

	if (ar.IsStoring())
	{
		ar << m_strSideName;
		ar << m_nLayoutSide;
		ar << m_fPaperLeft;
		ar << m_fPaperBottom;
		ar << m_fPaperRight;
		ar << m_fPaperTop;
		ar << m_fPaperWidth;
		ar << m_fPaperHeight;
		ar << m_strFormatName;
		ar << m_nPaperOrientation;
		ar << m_nPaperGrain;
		ar << m_nPaperRefEdge;	
		ar << m_nPressDeviceIndex;  
		ar << m_bAdjustFanout;
		m_FanoutList.Serialize(ar);
		m_DefaultPlates.Serialize(ar);
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();
		
		m_ObjectList.RemoveAll();
		m_FanoutList.RemoveAll();
		m_DefaultPlates.RemoveAll();
		switch (nVersion)
		{
		case 1:
			ar >> m_strSideName;
			ar >> m_nLayoutSide;
			ar >> m_fPaperLeft;
			ar >> m_fPaperBottom;
			ar >> m_fPaperRight;
			ar >> m_fPaperTop;
			ar >> m_fPaperWidth;
			ar >> m_fPaperHeight;
			ar >> m_strFormatName;
			ar >> m_nPaperOrientation;
			ar >> m_nPaperRefEdge;	
			ar >> m_nPressDeviceIndex;  
			m_bAdjustFanout = FALSE;
			break;
		case 2:
			ar >> m_strSideName;
			ar >> m_nLayoutSide;
			ar >> m_fPaperLeft;
			ar >> m_fPaperBottom;
			ar >> m_fPaperRight;
			ar >> m_fPaperTop;
			ar >> m_fPaperWidth;
			ar >> m_fPaperHeight;
			ar >> m_strFormatName;
			ar >> m_nPaperOrientation;
			ar >> m_nPaperRefEdge;	
			ar >> m_nPressDeviceIndex;  
			ar >> m_bAdjustFanout;
			m_FanoutList.Serialize(ar);
			break;
		case 3:
			ar >> m_strSideName;
			ar >> m_nLayoutSide;
			ar >> m_fPaperLeft;
			ar >> m_fPaperBottom;
			ar >> m_fPaperRight;
			ar >> m_fPaperTop;
			ar >> m_fPaperWidth;
			ar >> m_fPaperHeight;
			ar >> m_strFormatName;
			ar >> m_nPaperOrientation;
			ar >> m_nPaperRefEdge;	
			ar >> m_nPressDeviceIndex;  
			ar >> m_bAdjustFanout;
			m_FanoutList.Serialize(ar);
			m_DefaultPlates.Serialize(ar);
			break;
		case 4:
			ar >> m_strSideName;
			ar >> m_nLayoutSide;
			ar >> m_fPaperLeft;
			ar >> m_fPaperBottom;
			ar >> m_fPaperRight;
			ar >> m_fPaperTop;
			ar >> m_fPaperWidth;
			ar >> m_fPaperHeight;
			ar >> m_strFormatName;
			ar >> m_nPaperOrientation;
			ar >> m_nPaperGrain;
			ar >> m_nPaperRefEdge;	
			ar >> m_nPressDeviceIndex;  
			ar >> m_bAdjustFanout;
			m_FanoutList.Serialize(ar);
			m_DefaultPlates.Serialize(ar);
			break;
		}
	}

	m_ObjectList.Serialize(ar, this);	// Object list pointers of layout objects are not yet initialized.
										// But this is no problem because they will be initialized in CLayout::Copy()
										// when the layout is inserted into the layout list.
}


void CLayoutSide::Invalidate() 
{ 
	m_bInvalid = TRUE; 

	// When layout geometry is changed all plates referring this layout 
	// needs to be invalidated in order to process auto bleeding
	CLayout* pLayout = GetLayout();
	if (!pLayout)
		return;

	CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		CPlateList* pPlateList = (m_nLayoutSide == FRONTSIDE) ? &pPrintSheet->m_FrontSidePlates : &pPrintSheet->m_BackSidePlates;
		POSITION platePos = pPlateList->GetHeadPosition();
		while (platePos)
			pPlateList->GetNext(platePos).Invalidate();

		pPlateList->m_defaultPlate.Invalidate();

		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}
}


BOOL CLayoutSide::IsSimilar(CLayoutSide& rLayoutSide) 
{
	if (m_fPaperLeft   != rLayoutSide.m_fPaperLeft)
		return FALSE;
	if (m_fPaperBottom != rLayoutSide.m_fPaperBottom)
		return FALSE;
	if (m_fPaperRight  != rLayoutSide.m_fPaperRight)
		return FALSE;
	if (m_fPaperTop	   != rLayoutSide.m_fPaperTop)
		return FALSE;
	if (m_fPaperWidth  != rLayoutSide.m_fPaperWidth)
		return FALSE;
	if (m_fPaperHeight != rLayoutSide.m_fPaperHeight)
		return FALSE;

	if (GetTotalNUp() != rLayoutSide.GetTotalNUp())
		return FALSE;

	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::ControlMark)
			continue;

		BOOL	 bFoundEqual = FALSE;
		POSITION refPos		 = rLayoutSide.m_ObjectList.GetHeadPosition();
		while (refPos)
		{
			CLayoutObject& rRefLayoutObject = rLayoutSide.m_ObjectList.GetNext(refPos);
			if (rRefLayoutObject.m_nType == CLayoutObject::ControlMark)
				continue;
			if (rLayoutObject.IsSimilar(rRefLayoutObject))
			{
				bFoundEqual = TRUE;
				break;
			}
		}
		if (!bFoundEqual)
			return FALSE;
	}

	return TRUE;
}

BOOL CLayoutSide::IsIdentical(CLayoutSide& rLayoutSide) 
{
	if (m_fPaperLeft   != rLayoutSide.m_fPaperLeft)
		return FALSE;
	if (m_fPaperBottom != rLayoutSide.m_fPaperBottom)
		return FALSE;
	if (m_fPaperRight  != rLayoutSide.m_fPaperRight)
		return FALSE;
	if (m_fPaperTop	   != rLayoutSide.m_fPaperTop)
		return FALSE;
	if (m_fPaperWidth  != rLayoutSide.m_fPaperWidth)
		return FALSE;
	if (m_fPaperHeight != rLayoutSide.m_fPaperHeight)
		return FALSE;
	if (m_nPaperGrain  != rLayoutSide.m_nPaperGrain)
		return FALSE;
	if (m_nPressDeviceIndex != rLayoutSide.m_nPressDeviceIndex)
		return FALSE;

	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::ControlMark)
			continue;

		BOOL	 bFoundIdentical = FALSE;
		POSITION refPos			 = rLayoutSide.m_ObjectList.GetHeadPosition();
		while (refPos)
		{
			CLayoutObject& rRefLayoutObject = rLayoutSide.m_ObjectList.GetNext(refPos);
			if (rRefLayoutObject.m_nType == CLayoutObject::ControlMark)
				continue;
			if (rLayoutObject.IsIdentical(rRefLayoutObject))
			{
				bFoundIdentical = TRUE;
				break;
			}
		}
		if (!bFoundIdentical)
			return FALSE;
	}

	return TRUE;
}


CPressDevice* CLayoutSide::GetPressDevice(CImpManDoc* pAuxDoc)	// get layout side's PressDevice
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = pAuxDoc;
	if ( ! pDoc)
		return NULL;

	if (GetLayout())
	{
		CPrintingProfile& rProfile = GetLayout()->GetPrintingProfile();
		return (m_nLayoutSide == FRONTSIDE) ? &rProfile.m_press.m_pressDevice : &rProfile.m_press.m_backSidePressDevice;
	}
	else
		return NULL;

	//POSITION pos = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(m_nPressDeviceIndex);

	//if (pos == NULL)
	//	return NULL;

	//return &(pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos));
}


void CLayoutSide::GetFoldSheetObjects(int nComponentRefIndex, CObArray* pObjectPtrList, float* fBBoxLeft, float* fBBoxBottom, float* fBBoxRight, float* fBBoxTop, 
									  BOOL bPagesOnly, CPrintSheet* pPrintSheet)
{
	pObjectPtrList->RemoveAll();

	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)	// initialize Pointer array with layout objects belonging to 
	{			// desired foldsheet (nComponentRefIndex)
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.IsFlatProduct())
			continue;

		if (bPagesOnly)
		{
			if (rObject.m_nType == CLayoutObject::Page)
				if (rObject.m_nComponentRefIndex == nComponentRefIndex)
					pObjectPtrList->Add(&rObject);
		}
		else
		{
			if (rObject.m_nComponentRefIndex == nComponentRefIndex)
				pObjectPtrList->Add(&rObject);
		}
	}

	if (fBBoxLeft == NULL)	// Function called without bbox parameters
		return;

	*fBBoxLeft = FLT_MAX; *fBBoxBottom = FLT_MAX; *fBBoxRight = -FLT_MAX; *fBBoxTop = -FLT_MAX;
	if (pPrintSheet)
	{
		if (IsInvalid())
			FindCutBlocks();
		CLayoutObject* pCutBlock = GetFoldSheetCutBlock(nComponentRefIndex);
		if (pCutBlock)
		{
			float fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight; 
			pCutBlock->GetCurrentGrossCutBlock(pPrintSheet, fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight, FALSE);
			*fBBoxLeft  = fCropBoxX;					*fBBoxBottom = fCropBoxY; 
			*fBBoxRight = fCropBoxX + fCropBoxWidth;	*fBBoxTop	 = fCropBoxY + fCropBoxHeight; 
		}
	}
	else
	{
		// Calculate bounding box of foldsheet
		for (int i = 0; i < pObjectPtrList->GetSize(); i++)
		{
			CLayoutObject* pObj = (CLayoutObject*)(*pObjectPtrList)[i];
			*fBBoxLeft	 = min(*fBBoxLeft,	 pObj->GetLeft());
			*fBBoxBottom = min(*fBBoxBottom, pObj->GetBottom());
			*fBBoxRight	 = max(*fBBoxRight,	 pObj->GetRight());
			*fBBoxTop	 = max(*fBBoxTop,	 pObj->GetTop());
		}
	}
}


CLayoutObject* CLayoutSide::GetFirstFoldSheetLayerNeighbour(int nComponentRefIndex, int nSide)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)	
	{			
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.m_nComponentRefIndex == nComponentRefIndex)
		{
			CObjectNeighbours* pNeighbours = &rObject.m_LeftNeighbours;
			switch (nSide)
			{
			case LEFT:	pNeighbours = &rObject.m_LeftNeighbours;  break;
			case RIGHT:	pNeighbours = &rObject.m_RightNeighbours; break;
			case BOTTOM:pNeighbours = &rObject.m_LowerNeighbours; break;
			case TOP:	pNeighbours = &rObject.m_UpperNeighbours; break;
			}
			for (int i = 0; i < pNeighbours->GetSize(); i++)
				if (pNeighbours->ElementAt(i)->m_nComponentRefIndex != nComponentRefIndex)
					return pNeighbours->ElementAt(i);
		}
	}
	return NULL;
}


CLayoutObject* CLayoutSide::GetLeftmostFoldSheetLayerObject(int nComponentRefIndex)
{
	CLayoutObject* pLeftmost = NULL;
	float		   fMinLeft  = FLT_MAX;
	POSITION	   pos		 = m_ObjectList.GetHeadPosition();
	while (pos)	
	{			
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.m_nComponentRefIndex == nComponentRefIndex)
			if (rObject.GetLeft() < fMinLeft)
			{
				fMinLeft  = rObject.GetLeft();
				pLeftmost = &rObject;
			}
	}
	return pLeftmost;
}

CLayoutObject* CLayoutSide::GetRightmostFoldSheetLayerObject(int nComponentRefIndex)
{
	CLayoutObject* pRightmost = NULL;
	float		   fMaxRight  = -FLT_MAX;
	POSITION	   pos		  = m_ObjectList.GetHeadPosition();
	while (pos)	
	{			
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.m_nComponentRefIndex == nComponentRefIndex)
			if (rObject.GetRight() > fMaxRight)
			{
				fMaxRight  = rObject.GetRight();
				pRightmost = &rObject;
			}
	}
	return pRightmost;
}

CLayoutObject* CLayoutSide::GetBottommostFoldSheetLayerObject(int nComponentRefIndex)
{
	CLayoutObject* pBottommost = NULL;
	float		   fMinBottom  = FLT_MAX;
	POSITION	   pos		   = m_ObjectList.GetHeadPosition();
	while (pos)	
	{			
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.m_nComponentRefIndex == nComponentRefIndex)
			if (rObject.GetBottom() < fMinBottom)
			{
				fMinBottom  = rObject.GetBottom();
				pBottommost = &rObject;
			}
	}
	return pBottommost;
}

CLayoutObject* CLayoutSide::GetTopmostFoldSheetLayerObject(int nComponentRefIndex)
{
	CLayoutObject* pTopmost = NULL;
	float		   fMaxTop  = -FLT_MAX;
	POSITION	   pos		= m_ObjectList.GetHeadPosition();
	while (pos)	
	{			
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.m_nComponentRefIndex == nComponentRefIndex)
			if (rObject.GetTop() > fMaxTop)
			{
				fMaxTop  = rObject.GetTop();
				pTopmost = &rObject;
			}
	}
	return pTopmost;
}


void CLayoutSide::SpreadSheet(CLayoutObject* pObject, int nSide, float fSpace)
{
    CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bChannelThrough = TRUE;
	pDoc->m_TrimOptions.m_bChannelSingle  = FALSE;

	CTrimChannel trimChannel;			
    trimChannel.pObjectList1 = new CObList;
    trimChannel.pObjectList2 = new CObList;

    trimChannel.n_Side = nSide;

	if ((nSide == LEFT) || (nSide == RIGHT))
	{
		trimChannel.n_Direction		= VERTICAL;
		trimChannel.n_MoveDirection = LEFT | RIGHT;
	}
	else
	{
		trimChannel.n_Direction		= HORIZONTAL;
		trimChannel.n_MoveDirection = BOTTOM | TOP;
	}

	CPoint point;
	switch (nSide)
	{
	case LEFT:	point.x = (long)(pObject->GetLeft());	point.y = (long)(pObject->GetBottom() + pObject->GetHeight() / 2); break;
	case RIGHT:	point.x = (long)(pObject->GetRight());	point.y = (long)(pObject->GetBottom() + pObject->GetHeight() / 2); break;
	case BOTTOM:point.x = (long)(pObject->GetLeft() + pObject->GetWidth() / 2);	point.y = (long)(pObject->GetBottom());	   break;
	case TOP:	point.x = (long)(pObject->GetLeft() + pObject->GetWidth() / 2);	point.y = (long)(pObject->GetTop());	   break;
	}
	trimChannel.pObject			 = pObject;
	trimChannel.pObjectNeighbour = CPrintSheetView::FindOppositeObject(pObject, nSide, point);

	CPrintSheetView::FindTrimChannel(&trimChannel, pObject, trimChannel.pObjectNeighbour, nSide);
	CPrintSheetView::CalculateTrimChannelRect(&trimChannel);
	CPrintSheetView::DetermineChannelWidth(&trimChannel);

    float fOldTrim1 = trimChannel.fTrim1;
    float fOldTrim2 = trimChannel.fTrim2;

//    trimChannel.OldRect = updateTempRect;

	trimChannel.fTrim1 = fSpace/2.0f;
	trimChannel.fTrim2 = fSpace/2.0f;

    if ( (fabs(trimChannel.fTrim1 - fOldTrim1) > 0.009) || (fabs(trimChannel.fTrim2 - fOldTrim2) > 0.009) )
    {
	    CLayoutObject *pCounterpartObject = NULL, *pCounterpartObjectNeighbour = NULL;

        if (theApp.settings.m_bFrontRearParallel)
        {
        	pCounterpartObject			= trimChannel.pObject->FindCounterpartObject();
        	pCounterpartObjectNeighbour = trimChannel.pObjectNeighbour->FindCounterpartObject();
        }

        CPrintSheetView::MakeNewChannel(&trimChannel, trimChannel.fTrim1 - fOldTrim1, trimChannel.fTrim2 - fOldTrim2);

        if (theApp.settings.m_bFrontRearParallel)
        {
        	trimChannel.pObjectList1->RemoveAll();
        	trimChannel.pObjectList2->RemoveAll();
			trimChannel.pObject			 = pCounterpartObject;
			trimChannel.pObjectNeighbour = pCounterpartObjectNeighbour;
        	if (CPrintSheetView::CalculateCounterpartTrimChannel(&trimChannel, pCounterpartObject, pCounterpartObjectNeighbour, nSide))
        		CPrintSheetView::MakeNewChannel(&trimChannel, trimChannel.fTrim1 - fOldTrim1, trimChannel.fTrim2 - fOldTrim2);
        }


		//trimChannel.pObject->GetLayout()->RearrangePaper(TRUE);	// TRUE = don't change if paper size fits
		if (trimChannel.pObject)
		{
			trimChannel.pObject->GetLayout()->CheckForProductType();
			trimChannel.pObject->GetLayout()->AnalyzeMarks();
		}
	
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();	// because panorama pages could be arised or destroyed
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
        pDoc->SetModifiedFlag();
    }

    delete trimChannel.pObjectList1;
    delete trimChannel.pObjectList2;
}


// Calculate bounding box for all layout objects (pages and marks)
void CLayoutSide::CalcBoundingBoxAll(float* fBBoxLeft, float* fBBoxBottom, float* fBBoxRight, float* fBBoxTop, BOOL bExcludeMarks, CPrintSheet* pPrintSheet, int nSide)
{
	*fBBoxLeft = FLT_MAX; *fBBoxBottom = FLT_MAX; *fBBoxRight = -FLT_MAX; *fBBoxTop = -FLT_MAX;
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (bExcludeMarks)
			if (rObject.m_nType == CLayoutObject::ControlMark)
				continue;

		float fLeft, fBottom, fRight, fTop;
		if (pPrintSheet)
			rObject.GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
		else
		{
			fLeft	= rObject.GetLeft();
			fBottom	= rObject.GetBottom();
			fRight	= rObject.GetRight();
			fTop	= rObject.GetTop();
		}

		*fBBoxLeft	 = min(*fBBoxLeft,	 fLeft);
		*fBBoxBottom = min(*fBBoxBottom, fBottom);
		*fBBoxRight	 = max(*fBBoxRight,	 fRight);
		*fBBoxTop	 = max(*fBBoxTop,	 fTop);
	}
}

BOOL CLayoutSide::CheckPaper()
{
	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop; 
	CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);
	if ( (fBBoxLeft >= m_fPaperLeft) && (fBBoxRight <= m_fPaperRight) && (fBBoxBottom >= m_fPaperBottom) &&( fBBoxTop <= m_fPaperTop) )	// does fit
		return TRUE;
	else
		return FALSE;
}


BOOL CLayoutSide::FindSuitablePaper(BOOL bDontChangeIfFit, float fFoldSheetWidth, float fFoldSheetHeight, CPressDevice* pPressDevice)
{
	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop, fBBoxWidth, fBBoxHeight;
	if ( (fFoldSheetWidth > 0.01f) && (fFoldSheetHeight > 0.01f) )
	{
		fBBoxLeft = fBBoxBottom = 0.0f; fBBoxRight = fFoldSheetWidth; fBBoxTop = fFoldSheetHeight;
	}
	else
		CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);

	if ( ! pPressDevice)
		pPressDevice = GetPressDevice();
	if ( ! pPressDevice)
		return FALSE;

	float fGripper = 0.0f;
	if ( (m_nLayoutSide == BACKSIDE) && (m_pLayout->KindOfProduction() == WORK_AND_TUMBLE) )
		fGripper = (pPressDevice->m_nPressType == CPressDevice::SheetPress) ? abs(m_fPaperTop - fBBoxTop) : 0.0f;
	else
		fGripper = (pPressDevice->m_nPressType == CPressDevice::SheetPress) ? abs(fBBoxBottom - m_fPaperBottom) : 0.0f;

	fBBoxWidth  = fBBoxRight - fBBoxLeft;
	fBBoxHeight = fBBoxTop   - fBBoxBottom;

	if ( (m_fPaperWidth == 0.0f) && (m_fPaperHeight == 0.0f) )	// new sheet
	{
		m_fPaperLeft   = fBBoxLeft;   m_fPaperRight  = fBBoxRight;
		m_fPaperBottom = fBBoxBottom; m_fPaperTop    = fBBoxTop;
		m_fPaperWidth  = fBBoxWidth;  m_fPaperHeight = fBBoxHeight;
	}

	if ( ! bDontChangeIfFit)
	{
		if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
			fBBoxBottom = m_fPaperBottom;
		else
			fBBoxBottom = pPressDevice->GetYCenterOfSection() - fBBoxHeight/2.0f;
		fBBoxTop = fBBoxBottom + fBBoxHeight;
	}
	else
		if ( (fBBoxLeft >= m_fPaperLeft) && (fBBoxRight <= m_fPaperRight) && (fBBoxBottom >= m_fPaperBottom) &&( fBBoxTop <= m_fPaperTop) )	// does fit
			return FALSE;

	CDlgSheet dlg;		// TODO: provisional solution to get sheet sizes -> has to be changed later
	dlg.LoadSheets();
	CSheet	 minSheet;
	float	 fMinPlane = FLT_MAX;
	POSITION minPos    = NULL;
	POSITION pos	   = dlg.m_sheetList.GetHeadPosition();
	while (pos)
	{
		CSheet& rSheet = dlg.m_sheetList.GetAt(pos);
		BOOL bFit = FALSE;
		if ( ! bDontChangeIfFit)
		{
			if ((rSheet.m_fWidth >= fBBoxWidth) && (rSheet.m_fHeight >= fBBoxHeight))
				bFit = TRUE;
		}
		else
		{
			if ((rSheet.m_fWidth >= fBBoxWidth) && (rSheet.m_fHeight >= fGripper + fBBoxHeight))
				bFit = TRUE;
		}
		if (bFit)
			if (rSheet.m_fWidth * rSheet.m_fHeight < fMinPlane)
			{
				fMinPlane = rSheet.m_fWidth * rSheet.m_fHeight;
				minPos	  = pos;
			}
		dlg.m_sheetList.GetNext(pos);
	}

	BOOL bRotated = FALSE;
	pos			  = dlg.m_sheetList.GetHeadPosition();
	while (pos)
	{
		CSheet& rSheet = dlg.m_sheetList.GetAt(pos);
		BOOL bFit = FALSE;
		if ( ! bDontChangeIfFit)
		{
			if ((rSheet.m_fWidth >= fBBoxHeight) && (rSheet.m_fHeight >= fBBoxWidth))
				bFit = TRUE;
		}
		else
		{
			if ((rSheet.m_fWidth >= fGripper + fBBoxHeight) && (rSheet.m_fHeight >= fBBoxWidth))
				bFit = TRUE;
		}
		if (bFit)
			if (rSheet.m_fWidth * rSheet.m_fHeight < fMinPlane)
			{
				fMinPlane = rSheet.m_fWidth * rSheet.m_fHeight;
				minPos    = pos;
				bRotated  = TRUE;
			}
		dlg.m_sheetList.GetNext(pos);
	}
	if (minPos)
		minSheet = dlg.m_sheetList.GetAt(minPos);

	m_nPaperOrientation = LANDSCAPE; 
	if (bRotated)
	{
		float fTemp			= minSheet.m_fHeight;
		minSheet.m_fHeight	= minSheet.m_fWidth;
		minSheet.m_fWidth	= fTemp;
		m_nPaperOrientation = PORTRAIT;
	}

	if (minPos)
	{
		m_fPaperLeft    = fBBoxLeft  - (minSheet.m_fWidth  - fBBoxWidth )/2.0f;
		m_fPaperRight   = fBBoxRight + (minSheet.m_fWidth  - fBBoxWidth )/2.0f;
		if (pPressDevice->m_nPressType == CPressDevice::WebPress)
		{
			m_fPaperBottom = pPressDevice->GetYCenterOfSection() - minSheet.m_fHeight/2.0f;
			m_fPaperTop	   = m_fPaperBottom + minSheet.m_fHeight;
		}
		else
			if ((m_nLayoutSide == BACKSIDE) && (m_pLayout->KindOfProduction() == WORK_AND_TUMBLE))
				m_fPaperBottom = m_fPaperTop - minSheet.m_fHeight;
			else
				m_fPaperTop	   = m_fPaperBottom + minSheet.m_fHeight;

		m_fPaperWidth   = minSheet.m_fWidth;
		m_fPaperHeight  = minSheet.m_fHeight;
		m_strFormatName = minSheet.m_strSheetName;
	}

	return TRUE;
}


void CLayoutSide::TurnAll()
{
	float	 fFlipAxis = m_fPaperLeft + m_fPaperWidth/2.0f;
	POSITION pos	   = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if ( (rObject.m_nType == CLayoutObject::ControlMark) && (rObject.m_reflinePosParams.m_nPlaceOn == CReflinePositionParams::PlaceOnBothSamePos) )
		{
			CLayoutObject* pOppObject = rObject.FindCounterpartControlMark();
			if (pOppObject)
			{
				rObject.SetLeft	 (pOppObject->GetLeft());
				rObject.SetRight (pOppObject->GetRight());
				rObject.SetBottom(pOppObject->GetBottom());
				rObject.SetTop	 (pOppObject->GetTop());
				rObject.m_nHeadPosition		= pOppObject->m_nHeadPosition;
				rObject.m_reflinePosParams	= pOppObject->m_reflinePosParams;
			}
			continue;
		}
	}

	pos	= m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if ( (rObject.m_nType == CLayoutObject::ControlMark) && (rObject.m_reflinePosParams.m_nPlaceOn == CReflinePositionParams::PlaceOnBothSamePos) )
			continue;

		float fTemp = rObject.GetLeft();
		rObject.SetLeft (2.0f * fFlipAxis - rObject.GetRight());
		rObject.SetRight(2.0f * fFlipAxis - fTemp);
		switch (rObject.m_nHeadPosition)
		{
		case LEFT:	rObject.m_nHeadPosition = RIGHT;	break;
		case RIGHT:	rObject.m_nHeadPosition = LEFT;		break;
		}
		if (rObject.m_nType == CLayoutObject::Page)
		{
			SwapValues(rObject.m_fLeftBorder, rObject.m_fRightBorder);
			BOOL bLeftLocked  = (rObject.m_nLockBorder & LEFT);
			BOOL bRightLocked = (rObject.m_nLockBorder & RIGHT);
			if (bRightLocked)
				rObject.m_nLockBorder |= LEFT;
			if (bLeftLocked)
				rObject.m_nLockBorder |= RIGHT;

			switch (rObject.m_nIDPosition)
			{
			case LEFT:	rObject.m_nIDPosition = RIGHT;	break;
			case RIGHT:	rObject.m_nIDPosition = LEFT;	break;
			}
		}
	}

	switch (m_nPaperRefEdge)
	{
	case LEFT_BOTTOM:	m_nPaperRefEdge = RIGHT_BOTTOM;	break;
	case RIGHT_BOTTOM:	m_nPaperRefEdge = LEFT_BOTTOM;	break;
	case LEFT_TOP:		m_nPaperRefEdge = RIGHT_TOP;	break;
	case RIGHT_TOP:		m_nPaperRefEdge = LEFT_TOP;		break;
	}

	pos	= m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if ( (rObject.m_nType == CLayoutObject::ControlMark) && (rObject.m_reflinePosParams.m_nPlaceOn == CReflinePositionParams::PlaceOnBothSamePos) )
		{
			CLayoutObject* pOppObject = rObject.FindCounterPart(rObject.GetLayoutSideIndex(), rObject.GetLeft(), rObject.GetBottom(), rObject.GetRight(), rObject.GetTop());
			rObject.AlignToCounterPart(pOppObject);
			continue;
		}
	}

	TurnLocalMaskAllExposures();
}


void CLayoutSide::TumbleAll()
{
	float	 fFlipAxis = m_fPaperBottom + m_fPaperHeight/2.0f;
	POSITION pos	   = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if ( (rObject.m_nType == CLayoutObject::ControlMark) && (rObject.m_reflinePosParams.m_nPlaceOn == CReflinePositionParams::PlaceOnBothSamePos) )
		{
			CLayoutObject* pOppObject = rObject.FindCounterpartControlMark();
			if (pOppObject)
			{
				rObject.SetLeft	 (pOppObject->GetLeft());
				rObject.SetRight (pOppObject->GetRight());
				rObject.SetBottom(pOppObject->GetBottom());
				rObject.SetTop	 (pOppObject->GetTop());
				rObject.m_nHeadPosition		= pOppObject->m_nHeadPosition;
				rObject.m_reflinePosParams	= pOppObject->m_reflinePosParams;
			}
			continue;
		}
	}

	pos	= m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if ( (rObject.m_nType == CLayoutObject::ControlMark) && (rObject.m_reflinePosParams.m_nPlaceOn == CReflinePositionParams::PlaceOnBothSamePos) )
			continue;

		float fTemp = rObject.GetBottom();
		rObject.SetBottom(2.0f * fFlipAxis - rObject.GetTop());
		rObject.SetTop   (2.0f * fFlipAxis - fTemp);
		switch (rObject.m_nHeadPosition)
		{
		case BOTTOM: rObject.m_nHeadPosition = TOP;		break;
		case TOP:	 rObject.m_nHeadPosition = BOTTOM;	break;
		}
		if (rObject.m_nType == CLayoutObject::Page)
		{
			SwapValues(rObject.m_fUpperBorder, rObject.m_fLowerBorder);
			BOOL bUpperLocked = (rObject.m_nLockBorder & TOP);
			BOOL bLowerLocked = (rObject.m_nLockBorder & BOTTOM);
			if (bLowerLocked)
				rObject.m_nLockBorder |= TOP;
			if (bUpperLocked)
				rObject.m_nLockBorder |= BOTTOM;

			switch (rObject.m_nIDPosition)
			{
			case LEFT:	rObject.m_nIDPosition = RIGHT;	break;
			case RIGHT:	rObject.m_nIDPosition = LEFT;	break;
			}
		}
	}

	switch (m_nPaperRefEdge)
	{
	case LEFT_BOTTOM:	m_nPaperRefEdge = LEFT_TOP;		break;
	case RIGHT_BOTTOM:	m_nPaperRefEdge = RIGHT_TOP;	break;
	case LEFT_TOP:		m_nPaperRefEdge = LEFT_BOTTOM;	break;
	case RIGHT_TOP:		m_nPaperRefEdge = RIGHT_BOTTOM;	break;
	}

	pos	= m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if ( (rObject.m_nType == CLayoutObject::ControlMark) && (rObject.m_reflinePosParams.m_nPlaceOn == CReflinePositionParams::PlaceOnBothSamePos) )
		{
			CLayoutObject* pOppObject = rObject.FindCounterPart(rObject.GetLayoutSideIndex(), rObject.GetLeft(), rObject.GetBottom(), rObject.GetRight(), rObject.GetTop());
			rObject.AlignToCounterPart(pOppObject);
			continue;
		}
	}

	TumbleLocalMaskAllExposures();
}

void CLayoutSide::TurnLocalMaskAllExposures()
{
	CPlateList*  pPlateList;
	CPrintSheet* pPrintSheet = GetLayout()->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		pPlateList	 = &pPrintSheet->m_BackSidePlates;
		POSITION pos = pPlateList->GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate	= pPlateList->GetNext(pos);
			rPlate.TurnLocalMaskAllExposures();	
			rPlate.Invalidate();	// plate needs to be invalidated because only locked masks are flipped - 
		}							// unlocked masks must be recalculated
		pPrintSheet->m_BackSidePlates.m_defaultPlate.TurnLocalMaskAllExposures();	
		pPrintSheet->m_BackSidePlates.m_defaultPlate.Invalidate();

		pPrintSheet = GetLayout()->GetNextPrintSheet(pPrintSheet);
	}
}

void CLayoutSide::TumbleLocalMaskAllExposures()
{
	CPlateList*  pPlateList;
	CPrintSheet* pPrintSheet = GetLayout()->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		pPlateList	 = &pPrintSheet->m_BackSidePlates;
		POSITION pos = pPlateList->GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate	= pPlateList->GetNext(pos);
			rPlate.TumbleLocalMaskAllExposures();	
			rPlate.Invalidate();	// plate needs to be invalidated because only locked masks are flipped - 
		}							// unlocked masks must be recalculated
		pPrintSheet->m_BackSidePlates.m_defaultPlate.TumbleLocalMaskAllExposures();	
		pPrintSheet->m_BackSidePlates.m_defaultPlate.Invalidate();

		pPrintSheet = GetLayout()->GetNextPrintSheet(pPrintSheet);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Calculate the position of the LayoutObjects after changing Sheet,PressDevice... 
void CLayoutSide::PositionLayoutSide(float fGripper, CPressDevice* pPressDevice)
{
	float fXOff = 0.0f, fYOff = 0.0f, left, bottom, platemid;
	float fBoundingRectLeft, fBoundingRectBottom, fBoundingRectRight, fBoundingRectTop, fBoundingRectWidth, fBoundingRectHeight;

	if ( ! pPressDevice)
	{
		pPressDevice = GetPressDevice();
		if ( ! pPressDevice)
			return;
	}

	CalcBoundingRectObjects(&fBoundingRectLeft, &fBoundingRectBottom, &fBoundingRectRight, &fBoundingRectTop, &fBoundingRectWidth, &fBoundingRectHeight, FALSE, -1, FALSE, NULL);

	float fBBoxLeftLabels, fBBoxBottomLabels, fBBoxRightLabels, fBBoxTopLabels, fBBoxWidthLabels, fBBoxHeightLabels;
	CalcBoundingRectObjects(&fBBoxLeftLabels, &fBBoxBottomLabels, &fBBoxRightLabels, &fBBoxTopLabels, &fBBoxWidthLabels, &fBBoxHeightLabels, FALSE, FALSE, NULL);	// label version

	if ((fBoundingRectWidth == 0.0f) || (fBoundingRectHeight == 0.0f))
	{
		fBoundingRectLeft	= fBBoxLeftLabels;
		fBoundingRectBottom = fBBoxBottomLabels;
		fBoundingRectRight	= fBBoxRightLabels;
		fBoundingRectTop    = fBBoxTopLabels;
	}
	else
	{
		if ( (fBBoxLeftLabels == 0) && (fBBoxRightLabels == 0) && (fBBoxBottomLabels == 0) && (fBBoxTopLabels == 0) )	// no labels
			;
		else
		{
			fBoundingRectLeft	= min(fBoundingRectLeft,	fBBoxLeftLabels);
			fBoundingRectBottom = min(fBoundingRectBottom,	fBBoxBottomLabels);
			fBoundingRectRight	= max(fBoundingRectRight,	fBBoxRightLabels);
			fBoundingRectTop    = max(fBoundingRectTop,		fBBoxTopLabels);
		}
	}
	fBoundingRectWidth  = fBoundingRectRight - fBoundingRectLeft;
	fBoundingRectHeight = fBoundingRectTop   - fBoundingRectBottom;

	if (fGripper == FLT_MAX)
		if ((m_nLayoutSide == BACKSIDE) && (m_pLayout->KindOfProduction() == WORK_AND_TUMBLE))
			fGripper = (float)fabs(m_fPaperTop - fBoundingRectTop);	// keep existing gripper value
		else
			fGripper = (float)fabs(fBoundingRectBottom - m_fPaperBottom);	// keep existing gripper value

	platemid	  = pPressDevice->m_fPlateWidth / 2.0f;
	m_fPaperLeft  = platemid - (m_fPaperWidth / 2.0f);
	m_fPaperRight = m_fPaperLeft +  m_fPaperWidth;
	if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
		m_fPaperBottom = pPressDevice->m_fPlateEdgeToPrintEdge - pPressDevice->m_fGripper;
	else
		m_fPaperBottom = pPressDevice->GetYCenterOfSection() - (m_fPaperHeight / 2.0f);
	m_fPaperTop = m_fPaperBottom + m_fPaperHeight;

	left = m_fPaperLeft + (m_fPaperWidth - fBoundingRectWidth)/2.0f;
	if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
		bottom = m_fPaperBottom + fGripper;
	 else
		bottom = m_fPaperBottom + (m_fPaperHeight - fBoundingRectHeight)/2.0f;
	fXOff = left - fBoundingRectLeft;
	if ((m_nLayoutSide == BACKSIDE) && (m_pLayout->KindOfProduction() == WORK_AND_TUMBLE))
		fYOff = m_fPaperTop - fBoundingRectTop - bottom + m_fPaperBottom;	// Align to top
	else // TODO: Improve: Is that also correct for WebPress ?
		fYOff = bottom - fBoundingRectBottom;								// Align to bottom
	
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject &rLayoutObject = m_ObjectList.GetNext(pos);
		rLayoutObject.SetLeft	(rLayoutObject.GetLeft()   + fXOff);
		rLayoutObject.SetTop	(rLayoutObject.GetTop()    + fYOff);
		rLayoutObject.SetRight	(rLayoutObject.GetRight()  + fXOff);
		rLayoutObject.SetBottom	(rLayoutObject.GetBottom() + fYOff);
	}
}

void CLayoutSide::RecenterPaperY()
{
	float fBoundingRectLeft, fBoundingRectBottom, fBoundingRectRight, fBoundingRectTop, fBoundingRectWidth, fBoundingRectHeight;

	CalcBoundingRectObjects(&fBoundingRectLeft, &fBoundingRectBottom, &fBoundingRectRight, &fBoundingRectTop, &fBoundingRectWidth, &fBoundingRectHeight, FALSE, -1, FALSE, NULL);

	float fBBoxLeftLabels, fBBoxBottomLabels, fBBoxRightLabels, fBBoxTopLabels, fBBoxWidthLabels, fBBoxHeightLabels;
	CalcBoundingRectObjects(&fBBoxLeftLabels, &fBBoxBottomLabels, &fBBoxRightLabels, &fBBoxTopLabels, &fBBoxWidthLabels, &fBBoxHeightLabels, FALSE, FALSE, NULL);	// label version

	if ((fBoundingRectWidth == 0.0f) || (fBoundingRectHeight == 0.0f))
	{
		fBoundingRectLeft	= fBBoxLeftLabels;
		fBoundingRectBottom = fBBoxBottomLabels;
		fBoundingRectRight	= fBBoxRightLabels;
		fBoundingRectTop    = fBBoxTopLabels;
	}
	else
	{
		if ( (fBBoxLeftLabels == 0) && (fBBoxRightLabels == 0) && (fBBoxBottomLabels == 0) && (fBBoxTopLabels == 0) )	// no labels
			;
		else
		{
			fBoundingRectLeft	= min(fBoundingRectLeft,	fBBoxLeftLabels);
			fBoundingRectBottom = min(fBoundingRectBottom,	fBBoxBottomLabels);
			fBoundingRectRight	= max(fBoundingRectRight,	fBBoxRightLabels);
			fBoundingRectTop    = max(fBoundingRectTop,		fBBoxTopLabels);
		}
	}
	fBoundingRectHeight = fBoundingRectTop   - fBoundingRectBottom;

	m_fPaperBottom = fBoundingRectBottom + (fBoundingRectHeight / 2.0f) - m_fPaperHeight / 2.0f;
	m_fPaperTop	   = m_fPaperBottom + m_fPaperHeight;
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Calculates the outer rectangle of the LayoutObjects
void CLayoutSide::CalcBoundingRectObjects(float* left, float* bottom, float* right, float* top, float* width, float* height, BOOL bWithMarks, int nComponentRefIndex, 
										  BOOL bWithBorders, CPrintSheet* pPrintSheet)
{
	*left  = *bottom =  FLT_MAX;
	*right = *top    = -FLT_MAX;

	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		 if (rObject.IsFlatProduct())
			 continue;

		if (nComponentRefIndex != -1)
			if (rObject.m_nType == CLayoutObject::Page)
				if (rObject.m_nComponentRefIndex != nComponentRefIndex)
					continue;

		if ( ! (rObject.m_nType == CLayoutObject::ControlMark && ! bWithMarks))
		{
			if (bWithBorders)
			{
				*left	= min(rObject.GetLeft()		- rObject.m_fLeftBorder,  *left);
				*bottom = min(rObject.GetBottom()	- rObject.m_fLowerBorder, *bottom);
				*right	= max(rObject.GetRight()	+ rObject.m_fRightBorder, *right);
				*top	= max(rObject.GetTop()		+ rObject.m_fUpperBorder, *top);
			}
			else
			{
				float fLeft, fBottom, fRight, fTop;
				rObject.GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
				*left	= min(fLeft,	*left);
				*bottom = min(fBottom,	*bottom);
				*right	= max(fRight,	*right);
				*top	= max(fTop,		*top);
			}
		}
	}

	if ( (*left == FLT_MAX) && (*bottom == FLT_MAX) && (*right == -FLT_MAX) && (*top == -FLT_MAX) )
		*left = *bottom = *right = *top = 0.0f;

	*width  = *right  - *left;
	*height = *top	  - *bottom;
	*right  = *left	  + *width;
	*top    = *bottom + *height;
}

//// label version
BOOL CLayoutSide::CalcBoundingRectObjects(float* left, float* bottom, float* right, float* top, float* width, float* height, BOOL bWithMarks, BOOL bWithBorders, CPrintSheet* pPrintSheet)
{
	*left  = *bottom =  FLT_MAX;
	*right = *top    = -FLT_MAX;

	BOOL	 bFound = FALSE;
	POSITION pos	= m_ObjectList.GetHeadPosition();
	while (pos)
	{
		 CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		 if ( ! rObject.IsFlatProduct())
			 continue;

		 bFound = TRUE;
		 if ( ! (rObject.m_nType == CLayoutObject::ControlMark && ! bWithMarks))
		 {
			 if (bWithBorders)
			 {
				 *left	 = min(rObject.GetLeft()	- rObject.m_fLeftBorder,  *left);
				 *bottom = min(rObject.GetBottom()	- rObject.m_fLowerBorder, *bottom);
				 *right	 = max(rObject.GetRight()	+ rObject.m_fRightBorder, *right);
				 *top	 = max(rObject.GetTop()		+ rObject.m_fUpperBorder, *top);
			 }
			 else
			 {
				float fLeft, fBottom, fRight, fTop;
				rObject.GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
				*left	= min(fLeft,	*left);
				*bottom = min(fBottom,	*bottom);
				*right	= max(fRight,	*right);
				*top	= max(fTop,		*top);
			 }
		 }
	}

	if ( (*left == FLT_MAX) && (*bottom == FLT_MAX) && (*right == -FLT_MAX) && (*top == -FLT_MAX) )
		*left = *bottom = *right = *top = 0.0f;

	*width  = *right  - *left;
	*height = *top	  - *bottom;
	*right  = *left	  + *width;
	*top    = *bottom + *height;

	return bFound;
}
//// label version
BOOL CLayoutSide::CalcBoundingRectObjects(float* left, float* bottom, float* right, float* top, float* width, float* height, BOOL bWithMarks, CObArray& rObjectPtrList, BOOL bWithBorders, CPrintSheet* pPrintSheet)
{
	*left  = *bottom =  FLT_MAX;
	*right = *top    = -FLT_MAX;

	BOOL		  bFound = FALSE;
	for (int i = 0; i < rObjectPtrList.GetSize(); i++)
	{
		CLayoutObject* pLayoutObject = (CLayoutObject*)rObjectPtrList.GetAt(i);
		if ( ! pLayoutObject->IsFlatProduct())
			continue;

		bFound = TRUE;
		if ( ! (pLayoutObject->m_nType == CLayoutObject::ControlMark && ! bWithMarks))
		{
			if (bWithBorders)
			{
				*left	 = min(pLayoutObject->GetLeft()		- pLayoutObject->m_fLeftBorder,  *left);
				*bottom  = min(pLayoutObject->GetBottom()	- pLayoutObject->m_fLowerBorder, *bottom);
				*right	 = max(pLayoutObject->GetRight()	+ pLayoutObject->m_fRightBorder, *right);
				*top	 = max(pLayoutObject->GetTop()		+ pLayoutObject->m_fUpperBorder, *top);
			}
			else
			{
				float fLeft, fBottom, fRight, fTop;
				pLayoutObject->GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
				*left	= min(fLeft,	*left);
				*bottom = min(fBottom,	*bottom);
				*right	= max(fRight,	*right);
				*top	= max(fTop,		*top);
			}
		}
	}

	if ( (*left == FLT_MAX) && (*bottom == FLT_MAX) && (*right == -FLT_MAX) && (*top == -FLT_MAX) )
		*left = *bottom = *right = *top = 0.0f;

	*width  = *right  - *left;
	*height = *top	  - *bottom;
	*right  = *left	  + *width;
	*top    = *bottom + *height;

	return bFound;
}

void CLayoutSide::OffsetFoldSheetLayer(int nComponentRefIndex, float fOffsetX, float fOffsetY)
{
	POSITION pos	= m_ObjectList.GetHeadPosition();
	while (pos)
	{
		 CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		 if (rObject.IsFlatProduct())
			 continue;
		 if (rObject.m_nType == CLayoutObject::Page)
			if (rObject.m_nComponentRefIndex != nComponentRefIndex)
				continue;
		 rObject.Offset(fOffsetX, fOffsetY);
	}
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Implementation of CLayoutObjectList

IMPLEMENT_SERIAL(CLayoutObjectList, CObject, VERSIONABLE_SCHEMA | 1)

CLayoutObjectList::CLayoutObjectList()
{
	m_pLayoutSide  = NULL;
	m_bDisplayOpen = FALSE;
}

CLayoutObjectList::CLayoutObjectList(const CLayoutObjectList& rLayoutObjectList)
{
	Copy(rLayoutObjectList);
}

const CLayoutObjectList& CLayoutObjectList::operator=(const CLayoutObjectList& rLayoutObjectList)
{
	Copy(rLayoutObjectList);	

	return *this;
}

void CLayoutObjectList::Copy(const CLayoutObjectList& rLayoutObjectList)
{
	CLayoutObject layoutObject;
 	POSITION pos = rLayoutObjectList.GetHeadPosition();
 	while (pos)
	{
		layoutObject = rLayoutObjectList.GetNext(pos);
 		AddTail(layoutObject);
	}
	
	m_bDisplayOpen = rLayoutObjectList.m_bDisplayOpen;
}


void CLayoutObjectList::Serialize(CArchive& ar, CLayoutSide* pLayoutSide)
{
	ASSERT_VALID(this);

	CObject::Serialize(ar);

	if (ar.IsStoring())
	{
		ar.WriteCount(m_nCount);
		for (CNode* pNode = m_pNodeHead; pNode != NULL; pNode = pNode->pNext)
		{
			ASSERT(AfxIsValidAddress(pNode, sizeof(CNode)));
			SerializeElements(ar, &pNode->data, 1);
		}
	}
	else
	{
		DWORD nNewCount = ar.ReadCount();
		while (nNewCount--)
		{
			CLayoutObject newData;
			SerializeElements(ar, &newData, 1);
			AddTail(newData);
		}
	}
}


POSITION CLayoutObjectList::AddTail(CLayoutObject& rLayoutObject)
{
	if (rLayoutObject.m_nType == CLayoutObject::Page)	// only pages has neighbour relations
		if (GetLayoutSide())
			GetLayoutSide()->Invalidate();

	Invalidate();

	POSITION pos = CList <CLayoutObject, CLayoutObject&>::AddTail(rLayoutObject);
	CList <CLayoutObject, CLayoutObject&>::GetAt(pos).m_pObjectList = this;
	return pos;
}


void CLayoutObjectList::RemoveAll()
{
	if ( ! GetCount())
		return;

	Invalidate();

	CList <CLayoutObject, CLayoutObject&>::RemoveAll();

	if (GetLayoutSide())
		GetLayoutSide()->Invalidate();
}


void CLayoutObjectList::RemoveAt(POSITION posToRemove)
{
	int			   nLayoutObjectIndex = GetIndex(posToRemove);
	CLayoutObject& rLayoutObject	  = GetAt(posToRemove);
	rLayoutObject.RemoveCorrespondingExposures(nLayoutObjectIndex);

	if (rLayoutObject.m_nType == CLayoutObject::ControlMark)	// marks are not involved into neighbour mechanism
		CList <CLayoutObject, CLayoutObject&>::RemoveAt(posToRemove);
	else
	{
		POSITION pos = GetHeadPosition();	// Remove object from neighbour lists, because in analysation previous neighbours will be used for recalculation.
		while (pos)							// This is correct and efficient if just positions has changed. But if objects has been removed all links to
		{									// those objects are no longer valid and has to be removed from neighbour lists.
			CLayoutObject& rCurLayoutObject = GetNext(pos);
			for (int i = 0; i < rCurLayoutObject.m_LeftNeighbours.GetSize(); i++)
			{
				if (rCurLayoutObject.m_LeftNeighbours[i] == &rLayoutObject)
					rCurLayoutObject.m_LeftNeighbours.RemoveAt(i);
			}
			for (int i = 0; i < rCurLayoutObject.m_RightNeighbours.GetSize(); i++)
			{
				if (rCurLayoutObject.m_RightNeighbours[i] == &rLayoutObject)
					rCurLayoutObject.m_RightNeighbours.RemoveAt(i);
			}
			for (int i = 0; i < rCurLayoutObject.m_LowerNeighbours.GetSize(); i++)
			{
				if (rCurLayoutObject.m_LowerNeighbours[i] == &rLayoutObject)
					rCurLayoutObject.m_LowerNeighbours.RemoveAt(i);
			}
			for (int i = 0; i < rCurLayoutObject.m_UpperNeighbours.GetSize(); i++)
			{
				if (rCurLayoutObject.m_UpperNeighbours[i] == &rLayoutObject)
					rCurLayoutObject.m_UpperNeighbours.RemoveAt(i);
			}
		}

		CList <CLayoutObject, CLayoutObject&>::RemoveAt(posToRemove);

		Invalidate();

		if (GetLayoutSide())
			GetLayoutSide()->Invalidate();
	}
}

void CLayoutObjectList::RemoveObject(CLayoutObject* pObject)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		POSITION prevPos = pos;
		if (pObject == &GetNext(pos))
		{
			RemoveAt(prevPos);
			break;
		}
	}
}

int CLayoutObjectList::GetIndex(POSITION posRef)
{
	int nIndex = 0;
	POSITION pos =  GetHeadPosition();
	while (pos)
	{
		if (pos == posRef)
			return nIndex;
		GetNext(pos);
		nIndex++;
	}
	return -1;
}


void CLayoutObjectList::RemoveUnplausibleObjects()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		POSITION	   prevPos = pos;
		CLayoutObject& rObject = GetNext(pos);

		if (!rObject.IsPlausible())
			RemoveAt(prevPos);
	} 

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();

	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

	pDoc->m_MarkSourceList.RemoveUnusedSources();
	
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->SetModifiedFlag();										
}

void CLayoutObjectList::Invalidate() 
{ 
	m_bInvalid = TRUE; 

	// When layout objects has been added or removed, printsheet default plates must be updated
	CLayoutSide* pLayoutSide = GetLayoutSide();
	if ( ! pLayoutSide)
		return;
	CLayout* pLayout = pLayoutSide->GetLayout();
	if ( ! pLayout)
		return;

	CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		CPlateList* pPlateList = (pLayoutSide->m_nLayoutSide == FRONTSIDE) ? &pPrintSheet->m_FrontSidePlates : &pPrintSheet->m_BackSidePlates;

		pPlateList->m_defaultPlate.m_ExposureSequence.Invalidate();

		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}
}

int	CLayoutObjectList::GetSheetObjectRefList(CPrintSheet* pPrintSheet, CArray <CSheetObjectRefs, CSheetObjectRefs&>* pObjectRefList, int nType, BOOL bLabel)
{
	if (pObjectRefList)
		pObjectRefList->RemoveAll();

	int						   nCount = 0;
	CArray <CString, CString&> sectionMarkGroups;
	CArray <CString, CString&> cropMarkGroups;
	CArray <CString, CString&> foldMarkGroups;
	POSITION				   pos = GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject		   = GetNext(pos);
		CPageTemplate* pObjectTemplate = (rObject.m_nType == CLayoutObject::ControlMark) ?
										  rObject.GetCurrentMarkTemplate() :
										  rObject.GetCurrentPageTemplate(pPrintSheet);
		if ( ! pObjectTemplate)
			continue;
		if (nType != -1)
			if (nType != rObject.m_nType)
				continue;
		if (bLabel)
			if ( ! rObject.IsFlatProduct())
				continue;

		if (rObject.m_nType == CLayoutObject::ControlMark)
		{
			if (pObjectTemplate)
			{
				int nMarkType = pObjectTemplate->GetMarkType();
				CSheetObjectRefs objectRef(CLayoutObject::ControlMark, nMarkType, &rObject, pObjectTemplate);
				switch (nMarkType)
				{
				case CMark::SectionMark:	if ( ! FindMarkGroup(sectionMarkGroups, pObjectTemplate->m_strPageID)) 
											{
												if (pObjectRefList) 
													pObjectRefList->Add(objectRef); 
												nCount++;
												sectionMarkGroups.Add(pObjectTemplate->m_strPageID);
											}
											break;
				case CMark::CropMark:		if ( ! FindMarkGroup(cropMarkGroups, pObjectTemplate->m_strPageID)) 
											{
												if (pObjectRefList) 
													pObjectRefList->Add(objectRef); 
												nCount++;
												cropMarkGroups.Add(pObjectTemplate->m_strPageID);
											}
											break;
				case CMark::FoldMark:		if ( ! FindMarkGroup(foldMarkGroups, pObjectTemplate->m_strPageID)) 
											{
												if (pObjectRefList) 
													pObjectRefList->Add(objectRef); 
												nCount++;
												foldMarkGroups.Add(pObjectTemplate->m_strPageID);
											}
											break;
				default:					if (pObjectRefList) 
												pObjectRefList->Add(objectRef); 
											nCount++;
											break;
				}
			}
		}
		else
		{
			if (pObjectRefList) 
			{
				CSheetObjectRefs objectRef(CLayoutObject::Page, -1, &rObject, pObjectTemplate);
				pObjectRefList->Add(objectRef);
			}
			nCount++;
		}
	}

	if (pObjectRefList)
	{
		qsort((void*)pObjectRefList->GetData(), pObjectRefList->GetSize(), sizeof(CSheetObjectRefs), CSheetObjectRefs::Compare);
	}

	return nCount;
}

BOOL CLayoutObjectList::FindMarkGroup(const CArray <CString, CString&>& markGroups, const CString& strName)
{
	for (int i = 0; i < markGroups.GetSize(); i++)
	{
		if (strName.Compare(markGroups[i]) == 0)
			return TRUE;
	}
	return FALSE;
}

int	CLayoutObjectList::GetObjectIndex(CLayoutObject* pObject)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (&GetNext(pos) == pObject)
			return nIndex;
		nIndex++;
	}
	return -1;
}

int CSheetObjectRefs::Compare(const void *p1, const void *p2)
{
	CSheetObjectRefs* pObjectRef1 = (CSheetObjectRefs*)p1;
	CSheetObjectRefs* pObjectRef2 = (CSheetObjectRefs*)p2;

	if ( (pObjectRef1->m_nObjectType == CLayoutObject::Page) && (pObjectRef2->m_nObjectType == CLayoutObject::ControlMark) )
		return -1;
	else
		if ( (pObjectRef1->m_nObjectType == CLayoutObject::ControlMark) && (pObjectRef2->m_nObjectType == CLayoutObject::Page) )
			return 1;
		else
			if ( (pObjectRef1->m_nObjectType == CLayoutObject::Page) && (pObjectRef2->m_nObjectType == CLayoutObject::Page) )
			{
				if (_ttoi(pObjectRef1->m_pObjectTemplate->m_strPageID) < _ttoi(pObjectRef2->m_pObjectTemplate->m_strPageID))
					return -1;
				else
					if (_ttoi(pObjectRef1->m_pObjectTemplate->m_strPageID) > _ttoi(pObjectRef2->m_pObjectTemplate->m_strPageID))
						return 1;
					else
						if ( (_ttoi(pObjectRef1->m_pObjectTemplate->m_strPageID) == 0) && (_ttoi(pObjectRef2->m_pObjectTemplate->m_strPageID) == 0) )
							if (pObjectRef1->m_pObjectTemplate->m_strPageID < pObjectRef2->m_pObjectTemplate->m_strPageID)
								return -1;
							else
								return 1;
						else
							return 0;
			}
			else	// must be marks 
			{
				switch (pObjectRef1->m_nMarkType)
				{
				case CMark::CustomMark:		if (pObjectRef2->m_nMarkType == CMark::CustomMark) 
												if (pObjectRef1->m_pObjectTemplate->m_strPageID.CompareNoCase(pObjectRef2->m_pObjectTemplate->m_strPageID) > 0)
													return 1;
											return -1;

				case CMark::TextMark:		if (pObjectRef2->m_nMarkType == CMark::CustomMark) 
												return 1;
											else
												if (pObjectRef2->m_nMarkType == CMark::TextMark) 
													if (pObjectRef1->m_pObjectTemplate->m_strPageID.CompareNoCase(pObjectRef2->m_pObjectTemplate->m_strPageID) > 0)
														return 1;
											return -1;

				case CMark::SectionMark:	if ( (pObjectRef2->m_nMarkType == CMark::CustomMark) || 
												 (pObjectRef2->m_nMarkType == CMark::TextMark) )
												return 1;
											else
												if (pObjectRef2->m_nMarkType == CMark::SectionMark) 
													if (pObjectRef1->m_pObjectTemplate->m_strPageID.CompareNoCase(pObjectRef2->m_pObjectTemplate->m_strPageID) > 0)
														return 1;
											return -1;

				case CMark::CropMark:		if ( (pObjectRef2->m_nMarkType == CMark::CustomMark) || 
												 (pObjectRef2->m_nMarkType == CMark::TextMark)   ||
												 (pObjectRef2->m_nMarkType == CMark::SectionMark) )
												return 1;
											else
												if (pObjectRef2->m_nMarkType == CMark::CropMark) 
													if (pObjectRef1->m_pObjectTemplate->m_strPageID.CompareNoCase(pObjectRef2->m_pObjectTemplate->m_strPageID) > 0)
														return 1;
											return -1;

				case CMark::FoldMark:		if ( (pObjectRef2->m_nMarkType == CMark::CustomMark)  || 
												 (pObjectRef2->m_nMarkType == CMark::TextMark)    ||
												 (pObjectRef2->m_nMarkType == CMark::SectionMark) ||
												 (pObjectRef2->m_nMarkType == CMark::CropMark) )
												return 1;
											else
												if (pObjectRef2->m_nMarkType == CMark::FoldMark) 
													if (pObjectRef1->m_pObjectTemplate->m_strPageID.CompareNoCase(pObjectRef2->m_pObjectTemplate->m_strPageID) > 0)
														return 1;
											return -1;

				case CMark::BarcodeMark:	if ( (pObjectRef2->m_nMarkType == CMark::CustomMark)  || 
												 (pObjectRef2->m_nMarkType == CMark::TextMark)    ||
												 (pObjectRef2->m_nMarkType == CMark::SectionMark) ||
												 (pObjectRef2->m_nMarkType == CMark::CropMark)	  ||
												 (pObjectRef2->m_nMarkType == CMark::FoldMark) )
												return 1;
											else
												if (pObjectRef2->m_nMarkType == CMark::BarcodeMark) 
													if (pObjectRef1->m_pObjectTemplate->m_strPageID.CompareNoCase(pObjectRef2->m_pObjectTemplate->m_strPageID) > 0)
														return 1;
											return -1;

				case CMark::DrawingMark:	if ( (pObjectRef2->m_nMarkType == CMark::CustomMark)  || 
												 (pObjectRef2->m_nMarkType == CMark::TextMark)    ||
												 (pObjectRef2->m_nMarkType == CMark::SectionMark) ||
												 (pObjectRef2->m_nMarkType == CMark::CropMark)	  ||
												 (pObjectRef2->m_nMarkType == CMark::FoldMark)	  ||
												 (pObjectRef2->m_nMarkType == CMark::BarcodeMark) )
												return 1;
											else
												if (pObjectRef2->m_nMarkType == CMark::DrawingMark) 
													if (pObjectRef1->m_pObjectTemplate->m_strPageID.CompareNoCase(pObjectRef2->m_pObjectTemplate->m_strPageID) > 0)
														return 1;
											return -1;
				}
			}

			return 0;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CLayoutObject

IMPLEMENT_SERIAL(CLayoutObject, CObject, VERSIONABLE_SCHEMA | 7)


CLayoutObject::CLayoutObject()
{
	m_LeftNeighbours.RemoveAll();  m_LeftNeighbours.m_pLayoutObject  = this;
	m_RightNeighbours.RemoveAll(); m_RightNeighbours.m_pLayoutObject = this;
	m_LowerNeighbours.RemoveAll(); m_LowerNeighbours.m_pLayoutObject = this; 
	m_UpperNeighbours.RemoveAll(); m_UpperNeighbours.m_pLayoutObject = this;

	m_nHeadPosition = TOP;
	m_nIDPosition	= LEFT;
	m_nLockBorder   = 0;

	m_nIndex	   = 0;
	m_nPublicFlag  = 0;
	m_nAnalyseFlag = 0;
	m_pObjectList  = NULL;

	m_bHidden	   = FALSE;
	m_nStatus	   = Normal;
}


// copy constructor for CLayoutObject - needed by CList functions:

CLayoutObject::CLayoutObject(const CLayoutObject& rLayoutObject) 
{
	m_pObjectList  = NULL;

	Copy(rLayoutObject);
}

int CLayoutObject::GetIndex()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc == NULL)
		return NULL;

	int		 nIndex = 0;
	POSITION pos	= GetLayoutSide()->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = GetLayoutSide()->m_ObjectList.GetNext(pos);
		if (&rLayoutObject == this)
			return nIndex;

		nIndex++;
	}
	return -1;
}

CLayout* CLayoutObject::GetLayout()		  
{ 
	if (this == NULL) 
		return NULL; 
	else 
		if (m_pObjectList == NULL)
			return NULL;
		else
			if (m_pObjectList->GetLayoutSide())
				return m_pObjectList->GetLayoutSide()->GetLayout(); 
			else
				return NULL;
}

CLayoutSide* CLayoutObject::GetLayoutSide()	  
{ 
	if (this == NULL) 
		return NULL; 
	else 
		if (m_pObjectList == NULL)
			return NULL;
		else
			return m_pObjectList->GetLayoutSide(); 
}

BYTE CLayoutObject::GetLayoutSideIndex()	  
{ 
	if (this == NULL) 
		return 0; 
	else 
		if (m_pObjectList == NULL)
			return 0;
		else
			if (m_pObjectList->GetLayoutSide())
				return m_pObjectList->GetLayoutSide()->m_nLayoutSide; 
			else
				return 0;
}

void  CLayoutObject::SetLeft  (float fLeft,   BOOL bInvalidate)   
	{ m_fObjectLeft   = fLeft;   if (bInvalidate && (m_nType == CLayoutObject::Page) ) GetLayoutSide()->Invalidate(); };

void  CLayoutObject::SetRight (float fRight,  BOOL bInvalidate)  
	{ m_fObjectRight  = fRight;  if (bInvalidate && (m_nType == CLayoutObject::Page) ) GetLayoutSide()->Invalidate(); };

void  CLayoutObject::SetBottom(float fBottom, BOOL bInvalidate) 
	{ m_fObjectBottom = fBottom; if (bInvalidate && (m_nType == CLayoutObject::Page) ) GetLayoutSide()->Invalidate(); };

void  CLayoutObject::SetTop   (float fTop,    BOOL bInvalidate)    
	{ m_fObjectTop	  = fTop;	 if (bInvalidate && (m_nType == CLayoutObject::Page) ) GetLayoutSide()->Invalidate(); };

void  CLayoutObject::SetWidth (float fWidth,  BOOL bInvalidate)  
	{ m_fObjectWidth  = fWidth;  if (bInvalidate && (m_nType == CLayoutObject::Page) ) GetLayoutSide()->Invalidate(); };

void  CLayoutObject::SetHeight(float fHeight, BOOL bInvalidate) 
	{ m_fObjectHeight = fHeight; if (bInvalidate && (m_nType == CLayoutObject::Page) ) GetLayoutSide()->Invalidate(); };


float CLayoutObject::GetLeftWithBorder()	
{ 
	if (GetLayoutSide()->IsInvalid())
	{
		GetLayoutSide()->Validate();	// Validate in order to prevent for recursive calls
		GetLayoutSide()->Analyse();
	}

	return GetLeft() - m_fLeftBorder;  
}

float CLayoutObject::GetRightWithBorder()	
{ 
	if (GetLayoutSide()->IsInvalid())
	{
		GetLayoutSide()->Validate();	// Validate in order to prevent for recursive calls
		GetLayoutSide()->Analyse();
	}

	return GetRight() + m_fRightBorder; 
}

float CLayoutObject::GetBottomWithBorder()	
{
	if (GetLayoutSide()->IsInvalid())
	{
		GetLayoutSide()->Validate();	// Validate in order to prevent for recursive calls
		GetLayoutSide()->Analyse();
	}

	return GetBottom() - m_fLowerBorder; 
}

float CLayoutObject::GetTopWithBorder()		
{ 
	if (GetLayoutSide()->IsInvalid())
	{
		GetLayoutSide()->Validate();	// Validate in order to prevent for recursive calls
		GetLayoutSide()->Analyse();
	}

	return GetTop()	+ m_fUpperBorder; 
}

float CLayoutObject::GetRealWidth()
{
	switch (m_nHeadPosition)
	{
	case TOP:	
	case BOTTOM:	return m_fObjectWidth;  
	case LEFT:	
	case RIGHT:		return m_fObjectHeight; 
	}
	return 0.0f;
}

float CLayoutObject::GetRealHeight()
{
	switch (m_nHeadPosition)
	{
	case TOP:	
	case BOTTOM:	return m_fObjectHeight; 
	case LEFT:	
	case RIGHT:		return m_fObjectWidth;  
	}
	return 0.0f;
}

float CLayoutObject::GetRealBorder(BYTE nBorder)
{
	if (GetLayoutSide()->IsInvalid())
	{
		GetLayoutSide()->Validate();	// Validate in order to prevent for recursive calls
		GetLayoutSide()->Analyse();
	}

	switch (nBorder)
	{
	case TOP:
		switch (m_nHeadPosition)
		{
		case TOP:		return m_fUpperBorder; 
		case BOTTOM:	return m_fLowerBorder; 
		case LEFT:		return m_fLeftBorder;  
		case RIGHT:		return m_fRightBorder; 
		}
	case BOTTOM:
		switch (m_nHeadPosition)
		{
		case TOP:		return m_fLowerBorder; 
		case BOTTOM:	return m_fUpperBorder; 
		case LEFT:		return m_fRightBorder; 
		case RIGHT:		return m_fLeftBorder;  
		}
	case LEFT:
		switch (m_nHeadPosition)
		{
		case TOP:		return m_fLeftBorder;  
		case BOTTOM:	return m_fRightBorder; 
		case LEFT:		return m_fLowerBorder; 
		case RIGHT:		return m_fUpperBorder; 
		}
	case RIGHT:
		switch (m_nHeadPosition)
		{
		case TOP:		return m_fRightBorder; 
		case BOTTOM:	return m_fLeftBorder;  
		case LEFT:		return m_fUpperBorder; 
		case RIGHT:		return m_fLowerBorder; 
		}
	}
	return 0.0f;
}


void CLayoutObject::Offset(float fOffsetX, float fOffsetY)
{
	m_fObjectLeft	+= fOffsetX;
	m_fObjectBottom += fOffsetY;
	m_fObjectRight	+= fOffsetX;
	m_fObjectTop	+= fOffsetY;

	if (m_nType == CLayoutObject::Page) 
		if (GetLayoutSide())
			GetLayoutSide()->Invalidate();
}

void CLayoutObject::ModifySize(float fWidth, float fHeight, CPrintSheet* pPrintSheet, BOOL bRotate)
{
	if (GetLayoutSide()->IsInvalid())
	{
		GetLayoutSide()->Validate();
		GetLayoutSide()->Analyse();
	}

	if (bRotate)
	{
		float fTemp = fWidth; fWidth = fHeight; fHeight = fTemp; 
	}

	float fDiffX, fDiffY;

	fDiffX = (GetWidth()  - fWidth)  / 2.0f;
	fDiffY = (GetHeight() - fHeight) / 2.0f;
	SetWidth (fWidth,  FALSE);
	SetHeight(fHeight, FALSE);
	SetLeft	 (GetLeft()	  + fDiffX, FALSE);
	SetRight (GetRight()  - fDiffX, FALSE);
	SetBottom(GetBottom() + fDiffY, FALSE);
	SetTop	 (GetTop()	  - fDiffY, FALSE);

	RecalcObject(pPrintSheet, LEFT);
	RecalcObject(pPrintSheet, RIGHT);
	RecalcObject(pPrintSheet, TOP);
	RecalcObject(pPrintSheet, BOTTOM);
}

void CLayoutObject::RecalcObject(CPrintSheet* pPrintSheet, int nDirection)
{
	int i;

	CLayout* pLayout = GetLayout();
	pLayout->m_FrontSide.ResetPublicFlags();
	pLayout->m_BackSide.ResetPublicFlags();

	switch (nDirection)
	{
		case LEFT : for (i = 0; i < m_LeftNeighbours.GetSize(); i++)
						CalcObjectPos(m_LeftNeighbours.GetAt(i),  pPrintSheet, LEFT);
					break;
		case RIGHT: for (i = 0; i < m_RightNeighbours.GetSize(); i++)
						CalcObjectPos(m_RightNeighbours.GetAt(i), pPrintSheet, RIGHT);
					break;
		case BOTTOM:for (i = 0; i < m_LowerNeighbours.GetSize(); i++)
						CalcObjectPos(m_LowerNeighbours.GetAt(i), pPrintSheet, BOTTOM);
					break;
		case TOP  : for (i = 0; i < m_UpperNeighbours.GetSize(); i++)
						CalcObjectPos(m_UpperNeighbours.GetAt(i), pPrintSheet, TOP);
					break;
	}

	GetLayoutSide()->Validate();
}

void CLayoutObject::CalcObjectPos(CLayoutObject *pObject, CPrintSheet* pPrintSheet, int nDirection)
{
	int i;

	if (pObject->m_nPublicFlag)	
		return;
	if (IsFlatProduct())
	{
		if ( ! pObject->IsFlatProduct())
			return;
		if (&GetCurrentFlatProduct(pPrintSheet) != &pObject->GetCurrentFlatProduct(pPrintSheet))
			return;
	}
	else
	{
		if (pObject->IsFlatProduct())
			return;
		if (&GetCurrentBoundProduct(pPrintSheet) != &pObject->GetCurrentBoundProduct(pPrintSheet))
			return;
	}

	switch (nDirection)
	{
	case LEFT : pObject->SetRight(GetLeft() - m_fLeftBorder - pObject->m_fRightBorder, FALSE);
				pObject->SetLeft(pObject->GetRight() - pObject->GetWidth(), FALSE);
				for (i = 0; i < pObject->m_LeftNeighbours.GetSize(); i++)
					pObject->CalcObjectPos(pObject->m_LeftNeighbours.GetAt(i), pPrintSheet, LEFT);
				break;
	case RIGHT: pObject->SetLeft(GetRight() + m_fRightBorder + pObject->m_fLeftBorder, FALSE);
				pObject->SetRight(pObject->GetLeft() + pObject->GetWidth(), FALSE);
				for (i = 0; i < pObject->m_RightNeighbours.GetSize(); i++)
					pObject->CalcObjectPos(pObject->m_RightNeighbours.GetAt(i), pPrintSheet, RIGHT);
				break;
	case BOTTOM:pObject->SetTop(GetBottom() - m_fLowerBorder - pObject->m_fUpperBorder, FALSE);
				pObject->SetBottom(pObject->GetTop() - pObject->GetHeight(), FALSE);
				for (i = 0; i < pObject->m_LowerNeighbours.GetSize(); i++)
					pObject->CalcObjectPos(pObject->m_LowerNeighbours.GetAt(i), pPrintSheet, BOTTOM);
				break;
	case TOP  : pObject->SetBottom(GetTop() + m_fUpperBorder + pObject->m_fLowerBorder, FALSE);
				pObject->SetTop(pObject->GetBottom() + pObject->GetHeight(), FALSE);
				for (i = 0; i < pObject->m_UpperNeighbours.GetSize(); i++)
					pObject->CalcObjectPos(pObject->m_UpperNeighbours.GetAt(i), pPrintSheet, TOP);
				break;
	}

	pObject->m_nPublicFlag = TRUE;	
}

// operator overload for CLayoutObject - needed by CList functions:
const CLayoutObject& CLayoutObject::operator=(const CLayoutObject& rLayoutObject)
{
	Copy(rLayoutObject);	

	return *this;
}


BOOL CLayoutObject::IsSimilar(const CLayoutObject& rLayoutObject) 
{
	if (fabs(m_fObjectLeft     - rLayoutObject.m_fObjectLeft)		> 0.001)
		return FALSE;
	if (fabs(m_fObjectBottom   - rLayoutObject.m_fObjectBottom)		> 0.001)
		return FALSE;
	if (fabs(m_fObjectRight	   - rLayoutObject.m_fObjectRight)		> 0.001)
		return FALSE;
	if (fabs(m_fObjectTop	   - rLayoutObject.m_fObjectTop)		> 0.001)
		return FALSE;
	if (fabs(m_fObjectWidth	   - rLayoutObject.m_fObjectWidth)		> 0.001)
		return FALSE;
	if (fabs(m_fObjectHeight   - rLayoutObject.m_fObjectHeight)		> 0.001)
		return FALSE;
	if (m_nIDPosition		  != rLayoutObject.m_nIDPosition)
		return FALSE;
	if (m_nHeadPosition		  != rLayoutObject.m_nHeadPosition)
		return FALSE;
	//if (m_nComponentRefIndex  != rLayoutObject.m_nComponentRefIndex)
	//	return FALSE;
	if (m_nLayerPageIndex	  != rLayoutObject.m_nLayerPageIndex)
		return FALSE;
	if (m_bObjectTurned		  != rLayoutObject.m_bObjectTurned)
		return FALSE;

	return TRUE;
}


BOOL CLayoutObject::IsIdentical(CLayoutObject& rLayoutObject) 
{
	if (fabs(m_fObjectLeft	  - rLayoutObject.m_fObjectLeft)	> 0.01)
		return FALSE;
	if (fabs(m_fObjectBottom  - rLayoutObject.m_fObjectBottom)	> 0.01)
		return FALSE;
	if (fabs(m_fObjectRight	  - rLayoutObject.m_fObjectRight)	> 0.01)
		return FALSE;
	if (fabs(m_fObjectTop	  - rLayoutObject.m_fObjectTop)		> 0.01)
		return FALSE;
	if (fabs(m_fObjectWidth	  - rLayoutObject.m_fObjectWidth)	> 0.01)
		return FALSE;
	if (fabs(m_fObjectHeight  - rLayoutObject.m_fObjectHeight)	> 0.01)
		return FALSE;
	if (m_nIDPosition		 != rLayoutObject.m_nIDPosition)
		return FALSE;
	if (m_nHeadPosition		 != rLayoutObject.m_nHeadPosition)
		return FALSE;

	CPrintSheet* pPrintSheet = GetLayout()->GetFirstPrintSheet();
	if (pPrintSheet)
	{
		if (rLayoutObject.m_nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize())
			return FALSE;
		if (pPrintSheet->m_FoldSheetLayerRefs[m_nComponentRefIndex].m_nFoldSheetIndex != 
			pPrintSheet->m_FoldSheetLayerRefs[rLayoutObject.m_nComponentRefIndex].m_nFoldSheetIndex)
			return FALSE;
		if (pPrintSheet->m_FoldSheetLayerRefs[m_nComponentRefIndex].m_nFoldSheetLayerIndex != 
			pPrintSheet->m_FoldSheetLayerRefs[rLayoutObject.m_nComponentRefIndex].m_nFoldSheetLayerIndex)
			return FALSE;
	}

	if (m_nLayerPageIndex != rLayoutObject.m_nLayerPageIndex) 
		return FALSE;
	if (m_bObjectTurned	  != rLayoutObject.m_bObjectTurned)
		return FALSE;

	return TRUE;
}

void CLayoutObject::Copy(const CLayoutObject& rLayoutObject)
{
	m_nType					 = rLayoutObject.m_nType;
	m_fObjectLeft			 = rLayoutObject.m_fObjectLeft;
	m_fObjectBottom			 = rLayoutObject.m_fObjectBottom;
	m_fObjectRight			 = rLayoutObject.m_fObjectRight;
	m_fObjectTop			 = rLayoutObject.m_fObjectTop;
	m_fObjectWidth			 = rLayoutObject.m_fObjectWidth;
	m_fObjectHeight			 = rLayoutObject.m_fObjectHeight;
	m_strObjectName			 = rLayoutObject.m_strObjectName;
	m_strFormatName			 = rLayoutObject.m_strFormatName;
	m_nObjectOrientation	 = rLayoutObject.m_nObjectOrientation;
	m_fLeftBorder			 = rLayoutObject.m_fLeftBorder;
	m_fRightBorder			 = rLayoutObject.m_fRightBorder;
	m_fLowerBorder			 = rLayoutObject.m_fLowerBorder;
	m_fUpperBorder			 = rLayoutObject.m_fUpperBorder;
	m_nLockBorder			 = rLayoutObject.m_nLockBorder;
	m_nIDPosition			 = rLayoutObject.m_nIDPosition;
	m_nHeadPosition			 = rLayoutObject.m_nHeadPosition;
	m_nBlockNumber			 = rLayoutObject.m_nBlockNumber;
	m_nAttribute			 = rLayoutObject.m_nAttribute;
	m_strNotes				 = rLayoutObject.m_strNotes;
	m_nDisplayColorIndex	 = rLayoutObject.m_nDisplayColorIndex;
	m_nComponentRefIndex	 = rLayoutObject.m_nComponentRefIndex;
	m_nLayerPageIndex	     = rLayoutObject.m_nLayerPageIndex;
	m_bObjectTurned			 = rLayoutObject.m_bObjectTurned;
	m_nMarkTemplateIndex	 = rLayoutObject.m_nMarkTemplateIndex;
	m_fDefaultContentOffsetX = rLayoutObject.m_fDefaultContentOffsetX;
	m_fDefaultContentOffsetY = rLayoutObject.m_fDefaultContentOffsetY;
	m_reflinePosParams		 = rLayoutObject.m_reflinePosParams;
	m_nPublicFlag			 = rLayoutObject.m_nPublicFlag;
	m_nAnalyseFlag			 = rLayoutObject.m_nAnalyseFlag;
	m_nIndex				 = rLayoutObject.m_nIndex;
	m_fObjectCenterX		 = rLayoutObject.m_fObjectCenterX;
	m_fObjectCenterY		 = rLayoutObject.m_fObjectCenterY;
	m_markBuddyLink			 = rLayoutObject.m_markBuddyLink;
	m_nStatus				 = rLayoutObject.m_nStatus;

	m_markInstanceDefs.RemoveAll();
	for (int i = 0; i < rLayoutObject.m_markInstanceDefs.GetSize(); i++)
	{
		CMarkInstanceDefs defs = rLayoutObject.m_markInstanceDefs[i];
		m_markInstanceDefs.Add(defs);
	}

	// The object list pointer (which will be initialized when object is inserted into list)
	// represents a strong link to this list  -	 so copy only the contents but not the object list pointer 
}


void CLayoutObject::CreatePage(float fLeft, float fBottom, float fWidth, float fHeight, CString strObjectName, CString& strFormatName, BYTE nOrientation, 
							   CFoldSheetLayer& rLayer, BYTE nPageIndex, BYTE nComponentRefIndex, BYTE nLayerSide)
{
	m_nType				 = CLayoutObject::Page;
	m_fObjectLeft        = fLeft;
	m_fObjectBottom      = fBottom;
	m_fObjectRight       = fLeft + fWidth;
	m_fObjectTop         = fBottom + fHeight;
	m_fObjectWidth       = fWidth;
	m_fObjectHeight      = fHeight;
	m_nLockBorder		 = 0;
	m_strObjectName		 = strObjectName;
	m_strFormatName		 = strFormatName;
	m_nObjectOrientation = nOrientation;
	m_fLeftBorder	     = 0.0f;
	m_fRightBorder       = 0.0f;
	m_fLowerBorder       = 0.0f;
	m_fUpperBorder       = 0.0f;
	if (nLayerSide == FRONTSIDE)
		if (rLayer.m_FrontPageArray[nPageIndex] % 2)
			m_nIDPosition = RIGHT;
		else
			m_nIDPosition = LEFT;
	else
		if (rLayer.m_BackPageArray[nPageIndex] % 2)
			m_nIDPosition = RIGHT;
		else
			m_nIDPosition = LEFT;

	m_nHeadPosition      = (BYTE)rLayer.m_HeadPositionArray[nPageIndex];
	if (nLayerSide == BACKSIDE)
	{
		switch (m_nHeadPosition)	// For backside flip left/right head position
		{
		case LEFT: 	m_nHeadPosition = RIGHT; break;
		case RIGHT:	m_nHeadPosition = LEFT;  break;
		default:							 break;
		}
	}

	m_nBlockNumber			 = 0;
	m_nAttribute			 = 0;	
	m_strNotes				 = "";
	m_nDisplayColorIndex	 = 0;
	m_nComponentRefIndex		 = nComponentRefIndex;
	m_nLayerPageIndex		 = nPageIndex;
	m_bObjectTurned			 = FALSE;
	m_nMarkTemplateIndex	 = -1;
	m_fDefaultContentOffsetX = 0.0f;
	m_fDefaultContentOffsetY = 0.0f;

	m_reflinePosParams.SetUndefined();
}


void CLayoutObject::CreateFlatProduct(float fLeft, float fBottom, float fWidth, float fHeight, int nHeadPosition, int nFlatProductRefIndex)
{
	m_nType					 = CLayoutObject::Page;
	m_fObjectLeft			 = fLeft;
	m_fObjectBottom			 = fBottom;
	m_fObjectRight			 = fLeft + fWidth;
	m_fObjectTop			 = fBottom + fHeight;
	m_fObjectWidth			 = fWidth;
	m_fObjectHeight			 = fHeight;
	m_strObjectName			 = "";
	m_strFormatName			 = "";
	m_nObjectOrientation	 = PORTRAIT;
	m_fLeftBorder			 = 0.0f;
	m_fRightBorder			 = 0.0f;
	m_fLowerBorder			 = 0.0f;
	m_fUpperBorder			 = 0.0f;
	m_nLockBorder			 = 0;
	m_nIDPosition			 = LEFT;
	m_nHeadPosition			 = nHeadPosition;
	m_nBlockNumber			 = 0;
	m_nAttribute			 = 0;	
	m_strNotes				 = "";
	m_nDisplayColorIndex	 = 0;
	m_nComponentRefIndex	 = nFlatProductRefIndex;
	m_nLayerPageIndex		 = -1;
	m_bObjectTurned			 = FALSE;
	m_nMarkTemplateIndex	 = -1;
	m_fDefaultContentOffsetX = 0.0f;
	m_fDefaultContentOffsetY = 0.0f;

	m_reflinePosParams.SetUndefined();
}


void CLayoutObject::CreateMark(float fLeft, float fBottom, float fWidth, float fHeight, const CString& strName, BYTE nHeadPosition, short nCountMarkTemplate)
{
	m_nType				 = CLayoutObject::ControlMark;
	m_fObjectLeft        = fLeft;
	m_fObjectBottom      = fBottom;
	m_fObjectRight       = fLeft + fWidth;
	m_fObjectTop         = fBottom + fHeight;
	m_fObjectWidth       = fWidth;
	m_fObjectHeight      = fHeight;
	m_strObjectName		 = "";
	m_strFormatName		 = strName;
	m_nObjectOrientation = PORTRAIT;
	m_fLeftBorder	     = 0.0f;
	m_fRightBorder       = 0.0f;
	m_fLowerBorder       = 0.0f;
	m_fUpperBorder       = 0.0f;
	m_nLockBorder		 = 0;
	m_nIDPosition		 = LEFT;
	m_nHeadPosition      = nHeadPosition;
	m_nBlockNumber       = 0;
	m_nAttribute	     = 0;	
	m_strNotes			 = "";
	m_nDisplayColorIndex = 3;	// RED
	m_nComponentRefIndex     = -1;
	m_nLayerPageIndex    = -1;
	m_bObjectTurned		 = FALSE;
	m_nMarkTemplateIndex = nCountMarkTemplate;
	m_fDefaultContentOffsetX = 0.0f;
	m_fDefaultContentOffsetY = 0.0f;

	m_reflinePosParams.SetUndefined();
}


void CLayoutObject::CreateMark(float fLeft, float fBottom, float fWidth, float fHeight, const CString& strName, BYTE nHeadPosition, short nCountMarkTemplate, CReflinePositionParams* pReflinePosParams)
{
	CreateMark(fLeft, fBottom, fWidth, fHeight, strName, nHeadPosition, nCountMarkTemplate);
	if (pReflinePosParams)
		m_reflinePosParams = *pReflinePosParams;
}


void CLayoutObject::CreateCutBlockFoldSheet(float fLeft, float fBottom, float fWidth, float fHeight, int nComponentRefIndex, BYTE nHeadPosition)
{
	m_nType				 = CLayoutObject::CutBlock;
	m_fObjectLeft        = fLeft;
	m_fObjectBottom      = fBottom;
	m_fObjectRight       = fLeft + fWidth;
	m_fObjectTop         = fBottom + fHeight;
	m_fObjectWidth       = fWidth;
	m_fObjectHeight      = fHeight;
	m_strObjectName		 = _T("CUTBLOCK");
	m_strFormatName		 = _T("");
	m_nObjectOrientation = PORTRAIT;
	m_fLeftBorder	     = 0.0f;
	m_fRightBorder       = 0.0f;
	m_fLowerBorder       = 0.0f;
	m_fUpperBorder       = 0.0f;
	m_nLockBorder		 = 0;
	m_nIDPosition		 = LEFT;
	m_nHeadPosition      = nHeadPosition;
	m_nBlockNumber       = 0;
	m_nAttribute	     = 0;	
	m_strNotes			 = "";
	m_nDisplayColorIndex = 3;	// RED
	m_nComponentRefIndex     = nComponentRefIndex;
	m_nLayerPageIndex    = 0;
	m_bObjectTurned		 = FALSE;
	m_nMarkTemplateIndex = -1;
	m_fDefaultContentOffsetX = 0.0f;
	m_fDefaultContentOffsetY = 0.0f;

	m_reflinePosParams.SetUndefined();
}

void CLayoutObject::CreateCutBlockLabel(float fLeft, float fBottom, float fWidth, float fHeight, int nComponentRefIndex, BYTE nHeadPosition, int nBlockNumber)
{
	m_nType				 = CLayoutObject::CutBlock;
	m_fObjectLeft        = fLeft;
	m_fObjectBottom      = fBottom;
	m_fObjectRight       = fLeft + fWidth;
	m_fObjectTop         = fBottom + fHeight;
	m_fObjectWidth       = fWidth;
	m_fObjectHeight      = fHeight;
	m_strObjectName		 = _T("CUTBLOCK");
	m_strFormatName		 = _T("");
	m_nObjectOrientation = PORTRAIT;
	m_fLeftBorder	     = 0.0f;
	m_fRightBorder       = 0.0f;
	m_fLowerBorder       = 0.0f;
	m_fUpperBorder       = 0.0f;
	m_nLockBorder		 = 0;
	m_nIDPosition		 = LEFT;
	m_nHeadPosition      = nHeadPosition;
	m_nBlockNumber       = nBlockNumber;
	m_nAttribute	     = 0;	
	m_strNotes			 = "";
	m_nDisplayColorIndex = 3;	// RED
	m_nComponentRefIndex     = nComponentRefIndex;
	m_nLayerPageIndex    = -1;
	m_bObjectTurned		 = FALSE;
	m_nMarkTemplateIndex = -1;
	m_fDefaultContentOffsetX = 0.0f;
	m_fDefaultContentOffsetY = 0.0f;

	m_reflinePosParams.SetUndefined();
}

CPageTemplate* CLayoutObject::GetCurrentObjectTemplate(CPrintSheet* pPrintSheet)
{
	return ( (m_nType == CLayoutObject::Page) ? GetCurrentPageTemplate(pPrintSheet) : GetCurrentMarkTemplate() );
}

// Retrieve the corresponding page template
// Since layouts can be assigned to multiple printsheets (and foldsheets) 
// we need the current printsheet pointer to get the requested page
CPageTemplate* CLayoutObject::GetCurrentPageTemplate(CPrintSheet* pPrintSheet)
{
	if (IsFlatProduct())
		return GetCurrentFlatProductTemplate(pPrintSheet);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if (m_nType == CLayoutObject::ControlMark)
		return NULL;
	if (!pPrintSheet)
		return NULL;
	if (pPrintSheet->m_FoldSheetLayerRefs.GetSize() == 0)
		return NULL;
	if ((m_nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize()) || (m_nComponentRefIndex < 0))
		return NULL;

	int  nFoldSheetIndex	  = pPrintSheet->m_FoldSheetLayerRefs[m_nComponentRefIndex].m_nFoldSheetIndex;
	int  nFoldSheetLayerIndex = pPrintSheet->m_FoldSheetLayerRefs[m_nComponentRefIndex].m_nFoldSheetLayerIndex;

	if ((nFoldSheetIndex < 0) || (nFoldSheetIndex >= pDoc->m_Bookblock.m_FoldSheetList.GetCount()))
		return NULL;

	POSITION pos;
	pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(nFoldSheetIndex);
	if ( ! pos)
		return NULL;
	CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);
	if ((nFoldSheetLayerIndex < 0) || (nFoldSheetLayerIndex >= rFoldSheet.m_LayerList.GetCount()))
		return NULL;
	pos = rFoldSheet.m_LayerList.FindIndex(nFoldSheetLayerIndex);
	if ( ! pos)
		return NULL;
	CFoldSheetLayer& rLayer = rFoldSheet.m_LayerList.GetAt(pos);

	int nPageNum;
	int nSide = GetLayoutSide()->m_nLayoutSide;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return NULL;
	if (m_nComponentRefIndex >= pLayout->m_FoldSheetLayerTurned.GetSize())
		return NULL;
	if (pLayout->m_FoldSheetLayerTurned[m_nComponentRefIndex])	// -> Front/Back changed
		if (nSide == FRONTSIDE)
			nSide = BACKSIDE;
		else
			nSide = FRONTSIDE;

	if (nSide == FRONTSIDE)
	{
		if (m_nLayerPageIndex >= rLayer.m_FrontPageArray.GetSize())
			return NULL;
		nPageNum = rLayer.m_FrontPageArray[m_nLayerPageIndex];
	}
	else
	{
		if (m_nLayerPageIndex >= rLayer.m_BackPageArray.GetSize())
			return NULL;
		nPageNum = rLayer.m_BackPageArray[m_nLayerPageIndex];
	}

	int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(rFoldSheet.m_nProductPartIndex);
	pos = pDoc->m_PageTemplateList.FindIndex(nPageNum - 1, -nProductIndex - 1);
	if ( ! pos)
		return NULL;
	CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
	
	return &rPageTemplate;
}

CPageTemplate* CLayoutObject::GetCurrentPageTemplate(int nPrintSheetIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (!pDoc)
		return NULL;

	POSITION pos = pDoc->m_PrintSheetList.FindIndex(nPrintSheetIndex);
	if (!pos)
		return NULL;
	return GetCurrentPageTemplate(&(pDoc->m_PrintSheetList.GetAt(pos)));
}

CPageTemplate* CLayoutObject::GetCurrentFlatProductTemplate(CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if ( ! IsFlatProduct())
		return NULL;
	if ( ! pPrintSheet)
		return NULL;

	int nSide  = GetLayoutSideIndex();
	if (m_bObjectTurned)
		nSide = (nSide == FRONTSIDE) ? BACKSIDE : FRONTSIDE;

	if (m_nComponentRefIndex < 0)
		return NULL;

	int nFlatProductIndex = (m_nComponentRefIndex >= pPrintSheet->m_flatProductRefs.GetSize()) ? m_nComponentRefIndex : pPrintSheet->m_flatProductRefs[m_nComponentRefIndex].m_nFlatProductIndex;

	CPageTemplate* pPageTemplate = pDoc->m_PageTemplateList.FindFlatProductTemplate(nFlatProductIndex, nSide);
	
	return pPageTemplate;
}

CPageTemplate* CLayoutObject::GetCurrentMarkTemplate()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (!pDoc)
		return NULL;

	if ((m_nMarkTemplateIndex < 0) || (m_nMarkTemplateIndex >= pDoc->m_MarkTemplateList.GetCount()))
		return NULL;

	POSITION pos = pDoc->m_MarkTemplateList.FindIndex(m_nMarkTemplateIndex);
	if (pos)
	{
		CPageTemplate& rMarkTemplate = pDoc->m_MarkTemplateList.GetAt(pos);
		return &rMarkTemplate;
	}
	else
		return NULL;
}


void CLayoutObject::RemoveCorrespondingExposures(int nLayoutObjectIndex)
{
	CLayout* pLayout = GetLayout();
	if (!pLayout)
		return;

	CPlateList*  pPlateList;
	int			 nSide		 = GetLayoutSide()->m_nLayoutSide;
	CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		if (nSide == FRONTSIDE)
			pPlateList = &pPrintSheet->m_FrontSidePlates;
		else
			pPlateList = &pPrintSheet->m_BackSidePlates;

		POSITION pos = pPlateList->GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = pPlateList->GetNext(pos);
			rPlate.RemoveExposure(nLayoutObjectIndex);
		}

		pPlateList->m_defaultPlate.RemoveExposure(nLayoutObjectIndex);

		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}
}

void CLayoutObject::SetLocalMaskAllExposures(int nSide, float fValue, BOOL bPanoramaPageClicked)
{
	CLayout* pLayout = GetLayout();
	if (!pLayout)
		return;

	CPrintSheetView* pView						= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetMaskSettingsView* pSettingsView	= (CPrintSheetMaskSettingsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMaskSettingsView));
	CPrintSheet* pActPrintSheet  = (pView) ? pView->GetActPrintSheet() : NULL;
	CPlateList*		 pPlateList;
	int				 nLayoutSide = GetLayoutSide()->m_nLayoutSide;
	CPrintSheet*	 pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		if (pSettingsView)
		{
			if ( ! pSettingsView->IsSheetSideEffected(*pPrintSheet, nLayoutSide, TRUE))
				if ( ! pSettingsView->IsSheetEffected(*pPrintSheet, TRUE))
				{
					pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
					continue;
				}
		}

		if (nLayoutSide == FRONTSIDE)
			pPlateList = &pPrintSheet->m_FrontSidePlates;
		else
			pPlateList = &pPrintSheet->m_BackSidePlates;

		POSITION pos = pPlateList->GetHeadPosition();
		while (pos)
		{
			BOOL	bChange	= TRUE;
			CPlate& rPlate	= pPlateList->GetNext(pos);
			if (pView)
			{
				CDisplayItem* pPlateTabDI = pView->m_DisplayList.GetItemFromData((void*)&rPlate, (void*)rPlate.GetPrintSheet(),
																				 DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
				if (pPlateTabDI)
					if (pPlateTabDI->m_nState == CDisplayItem::Selected)	// if plate tab is selected with ctrl key 
						bChange = FALSE;									// assume plate is disabled
			}
			if (bChange)
				rPlate.SetBleedValue(nSide, fValue, this, bPanoramaPageClicked, pView);	
		}

		pPlateList->m_defaultPlate.SetBleedValue(nSide, fValue, this, bPanoramaPageClicked, pView);	

		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}
}

void CLayoutObject::UnlockLocalMaskAllExposures(BOOL bPanoramaPageClicked)
{
	CLayout* pLayout = GetLayout();
	if (!pLayout)
		return;

	CPlateList*		 pPlateList;
	CPrintSheetView*			 pView			= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetMaskSettingsView* pSettingsView	= (CPrintSheetMaskSettingsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMaskSettingsView));
	int				 nSide		 = GetLayoutSide()->m_nLayoutSide;
	CPrintSheet*	 pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		if (pSettingsView)
		{
			if ( ! pSettingsView->IsSheetSideEffected(*pPrintSheet, nSide, TRUE))
				if ( ! pSettingsView->IsSheetEffected(*pPrintSheet, TRUE))
				{
					pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
					continue;
				}
		}

		if (nSide == FRONTSIDE)
			pPlateList = &pPrintSheet->m_FrontSidePlates;
		else
			pPlateList = &pPrintSheet->m_BackSidePlates;

		POSITION pos = pPlateList->GetHeadPosition();
		while (pos)
		{
			BOOL	bChange	= TRUE;
			CPlate& rPlate	= pPlateList->GetNext(pos);
			if (pView)
			{
				CDisplayItem* pPlateTabDI = pView->m_DisplayList.GetItemFromData((void*)&rPlate, (void*)rPlate.GetPrintSheet(),
					DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
				if (pPlateTabDI)
					if (pPlateTabDI->m_nState == CDisplayItem::Selected)	// if plate tab is selected with ctrl key 
						bChange = FALSE;									// assume plate is disabled
			}
			if (bChange)
				rPlate.UnlockLocalMask(this, bPanoramaPageClicked, pView);	
		}

		pPlateList->m_defaultPlate.UnlockLocalMask(this, bPanoramaPageClicked, pView);	

		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}
}

void CLayoutObject::AdjustContent2LocalMaskAllExposures(BOOL bPanoramaPageClicked)
{
	CLayout* pLayout = GetLayout();
	if (!pLayout)
		return;

	CPlateList*		 pPlateList;
	CPrintSheetView*			 pView			= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetMaskSettingsView* pSettingsView	= (CPrintSheetMaskSettingsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMaskSettingsView));
	int				 nSide		 = GetLayoutSide()->m_nLayoutSide;
	CPrintSheet*	 pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		if (pSettingsView)
		{
			if ( ! pSettingsView->IsSheetSideEffected(*pPrintSheet, nSide, TRUE))
				if ( ! pSettingsView->IsSheetEffected(*pPrintSheet, TRUE))
				{
					pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
					continue;
				}
		}

		if (nSide == FRONTSIDE)
			pPlateList = &pPrintSheet->m_FrontSidePlates;
		else
			pPlateList = &pPrintSheet->m_BackSidePlates;

		POSITION pos = pPlateList->GetHeadPosition();
		while (pos)
		{
			BOOL	bChange	= TRUE;
			CPlate& rPlate	= pPlateList->GetNext(pos);
			if (pView)
			{
				CDisplayItem* pPlateTabDI = pView->m_DisplayList.GetItemFromData((void*)&rPlate, (void*)rPlate.GetPrintSheet(),
					DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
				if (pPlateTabDI)
					if (pPlateTabDI->m_nState == CDisplayItem::Selected)	// if plate tab is selected with ctrl key 
						bChange = FALSE;									// assume plate is disabled
			}
			if (bChange) 
				rPlate.AdjustContent2LocalMask(this, bPanoramaPageClicked, pView);	
		}

		pPlateList->m_defaultPlate.AdjustContent2LocalMask(this, bPanoramaPageClicked, pView);	

		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}
}

BOOL CLayoutObject::IsPlausible()
{
	if ((fabs(GetWidth()) < 0.01) || (fabs(GetHeight()) < 0.01))
		return FALSE;
	else
		if ((fabs(GetWidth()) > 5000.0f) || (fabs(GetHeight()) > 5000.0f))
			return FALSE;									
		else
			if ((GetRight() < -50000.0f) || (GetTop()    < -50000.0f) ||
				(GetLeft()  >  50000.0f) || (GetBottom() >  50000.0f))
				return FALSE;									

	return TRUE;
}

CExposure* CLayoutObject::GetExposure(CPlate* pPlate, CLayout* pLayout, int nSide)
{
	if ( ! pPlate)
		return NULL;

	POSITION pos = pPlate->m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = pPlate->m_ExposureSequence.GetNext(pos);
		if ( ! pLayout)
		{
			if (rExposure.GetLayoutObject(pPlate->GetPrintSheet()->m_nLayoutIndex, GetLayoutSideIndex()) == this)
				return &rExposure;
		}
		else
		{
			if (rExposure.GetLayoutObject(pLayout, nSide) == this)
				return &rExposure;
		}
	}

	CPrintSheet* pPrintSheet = pPlate->GetPrintSheet();
	if (pPrintSheet)
	{
		if (IsPanoramaPage(*pPrintSheet))
		{
			CPageTemplate* pPageTemplate = GetCurrentPageTemplate(pPrintSheet);
			if (IsMultipagePart(*pPageTemplate, RIGHT))
			{
				CLayoutObject* p2ndObject = GetCurrentMultipagePartner(pPageTemplate, TRUE);
				if (p2ndObject)
					return p2ndObject->GetExposure(pPlate);
			}
		}
	}

	return NULL;
}

CExposure* CLayoutObject::GetExposure(CPrintSheet& rPrintSheet, int nColorDefinitionIndex)
{
	CPlateList& rPlateList = (GetLayoutSideIndex() == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;
	POSITION	pos		   = rPlateList.GetHeadPosition();
	while (pos)
	{
		CPlate& rPlate = rPlateList.GetNext(pos);
		if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
			return GetExposure(&rPlate);
	}
	return NULL;
}

CExposure* CLayoutObject::GetTopmostExposure(CPrintSheet* pPrintSheet)
{
	CPlate* pPlate;
	int		nSide = GetLayoutSideIndex();
	if (nSide == FRONTSIDE)
		pPlate = pPrintSheet->m_FrontSidePlates.GetTopmostPlate(); 
	else
		pPlate = pPrintSheet->m_BackSidePlates.GetTopmostPlate();  

	POSITION pos = pPlate->m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure&	   rExposure = pPlate->m_ExposureSequence.GetNext(pos);
		if (rExposure.GetLayoutObject(pPrintSheet->m_nLayoutIndex, nSide) == this)
			return &rExposure;
	}
	return NULL;
}

int CLayoutObject::RealSide2LayoutSide(int nRealSide)
{
	switch (nRealSide)
	{
	case TOP:
		switch (m_nHeadPosition)
		{
		case TOP:    return TOP;
		case BOTTOM: return BOTTOM;
		case LEFT:	 return LEFT;
		case RIGHT:	 return RIGHT;
		}
	case BOTTOM:
		switch (m_nHeadPosition)
		{
		case TOP:    return BOTTOM;
		case BOTTOM: return TOP;
		case LEFT:	 return RIGHT;
		case RIGHT:	 return LEFT;
		}
	case LEFT:
		switch (m_nHeadPosition)
		{
		case TOP:    return LEFT;
		case BOTTOM: return RIGHT;
		case LEFT:	 return BOTTOM;
		case RIGHT:	 return TOP;
		}
	case RIGHT:
		switch (m_nHeadPosition)
		{
		case TOP:    return RIGHT;
		case BOTTOM: return LEFT;
		case LEFT:	 return TOP;
		case RIGHT:	 return BOTTOM;
		}
	}
	return TOP;
}

int	CLayoutObject::LayoutSide2RealSide(int nLayoutSide)
{
	switch (nLayoutSide)
	{
	case TOP:
		switch (m_nHeadPosition)
		{
		case TOP:    return TOP;
		case BOTTOM: return BOTTOM;
		case LEFT:	 return RIGHT;
		case RIGHT:	 return LEFT;
		}
	case BOTTOM:
		switch (m_nHeadPosition)
		{
		case TOP:    return BOTTOM;
		case BOTTOM: return TOP;
		case LEFT:	 return LEFT;
		case RIGHT:	 return RIGHT;
		}
	case LEFT:
		switch (m_nHeadPosition)
		{
		case TOP:    return LEFT;
		case BOTTOM: return RIGHT;
		case LEFT:	 return TOP;
		case RIGHT:	 return BOTTOM;
		}
	case RIGHT:
		switch (m_nHeadPosition)
		{
		case TOP:    return RIGHT;
		case BOTTOM: return LEFT;
		case LEFT:	 return BOTTOM;
		case RIGHT:	 return TOP;
		}
	}
	return TOP;
}


BOOL CLayoutObject::IsMultipagePart(CPrintSheet& rPrintSheet, int nSide)
{
	CPageTemplate* pTemplate = GetCurrentPageTemplate(&rPrintSheet);
	if (!pTemplate)
		return FALSE;
	else
		return IsMultipagePart(*pTemplate, nSide);
}


BOOL CLayoutObject::IsMultipagePart(CPageTemplate& rTemplate, int nSide)
{
	if (rTemplate.m_nMultipageGroupNum == -1)
		return FALSE;

	if (nSide == -1)
		return TRUE;

	switch (nSide)
	{
	case LEFT:	return (rTemplate.GetMultipageSide() == LEFT)	? TRUE : FALSE;
	case RIGHT:	return (rTemplate.GetMultipageSide() == RIGHT)	? TRUE : FALSE;
	}

	return FALSE;
}


BOOL CLayoutObject::IsPanoramaPage(CPrintSheet& rPrintSheet)
{
	if ( ! this)
		return FALSE;
	if (m_nType == CLayoutObject::ControlMark)
		return FALSE;

	CPageTemplate* pTemplate = GetCurrentPageTemplate(&rPrintSheet);
	if (!pTemplate)
		return FALSE;
	if (pTemplate->m_nMultipageGroupNum == -1)
		return FALSE;

	CLayoutObject* pNeighbour = NULL;

	if (pTemplate->GetMultipageSide() == LEFT)
	{
		if (GetRealBorder(RIGHT) != 0.0f)		// 11/09/2010 - now by definition we can have a panorama page even if we have a trim
			return FALSE;						// 06/06/2012 - recorrect this behaviour again as it was originally (because Theiss-Druck intervened)

		switch (m_nHeadPosition)
		{
		case TOP:		pNeighbour = (m_RightNeighbours.GetSize() == 0) ? NULL : m_RightNeighbours[0]; break;
		case RIGHT:		pNeighbour = (m_LowerNeighbours.GetSize() == 0) ? NULL : m_LowerNeighbours[0]; break;
		case BOTTOM:	pNeighbour = (m_LeftNeighbours.GetSize()  == 0) ? NULL : m_LeftNeighbours[0];  break;
		case LEFT:		pNeighbour = (m_UpperNeighbours.GetSize() == 0) ? NULL : m_UpperNeighbours[0]; break;
		}
	}
	else
	{
		if (GetRealBorder(LEFT) != 0.0f)
			return FALSE;

		switch (m_nHeadPosition)
		{
		case TOP:		pNeighbour = (m_LeftNeighbours.GetSize()  == 0) ? NULL : m_LeftNeighbours[0];  break;
		case RIGHT:		pNeighbour = (m_UpperNeighbours.GetSize() == 0) ? NULL : m_UpperNeighbours[0]; break;
		case BOTTOM:	pNeighbour = (m_RightNeighbours.GetSize() == 0) ? NULL : m_RightNeighbours[0]; break;
		case LEFT:		pNeighbour = (m_LowerNeighbours.GetSize() == 0) ? NULL : m_LowerNeighbours[0]; break;
		}
	}

	if (!pNeighbour)
		return FALSE;
	if (m_nHeadPosition != pNeighbour->m_nHeadPosition)
		return FALSE;

	CPageTemplate* pNeighbourTemplate = pNeighbour->GetCurrentPageTemplate(&rPrintSheet);
	if (pTemplate->GetFirstMultipagePartner() == pNeighbourTemplate)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			CBoundProductPart& rProductPart = pTemplate->GetProductPart();
			if (rProductPart.m_foldingParams.m_bShinglingActive)	
				if ( (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottling) || (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale) )
				{
					if ( (fabs(pTemplate->m_fShinglingOffsetX)	  >= 0.01f)  || (fabs(pNeighbourTemplate->m_fShinglingOffsetX)	   >= 0.01f) ||
						 (fabs(pTemplate->m_fShinglingOffsetXFoot) >= 0.01f) || (fabs(pNeighbourTemplate->m_fShinglingOffsetXFoot) >= 0.01f) )
						return FALSE;
				}
				else
					if ( (fabs(pTemplate->m_fShinglingOffsetX) >= 0.01) || (fabs(pNeighbourTemplate->m_fShinglingOffsetX) >= 0.01f) )
						return FALSE;
		}

		return TRUE;
	}
	else
		return FALSE;
}

BOOL CLayoutObject::PanoramaHasNeighbour(CLayoutObject* pBuddy, int nSide)
{
	CObjectNeighbours* pThisNeighbours  = NULL;
	CObjectNeighbours* pBuddyNeighbours = NULL;
	switch (nSide)
	{
	case TOP:		pThisNeighbours  =  &m_UpperNeighbours;
					pBuddyNeighbours =  &pBuddy->m_UpperNeighbours;
					break;
	case BOTTOM:	pThisNeighbours  =  &m_LowerNeighbours;
					pBuddyNeighbours =  &pBuddy->m_LowerNeighbours;
					break;
	case LEFT:		pThisNeighbours  =  &m_LeftNeighbours;
					pBuddyNeighbours =  &pBuddy->m_LeftNeighbours;
					break;
	case RIGHT:		pThisNeighbours  =  &m_RightNeighbours;
					pBuddyNeighbours =  &pBuddy->m_RightNeighbours;
					break;
	}

	if (pThisNeighbours)
	{
		for (int i = 0; i < pThisNeighbours->GetSize(); i++)
			if ((*pThisNeighbours)[i] != pBuddy)	// min. one neighbour != buddy
				return TRUE;	
	}
	if (pBuddyNeighbours)
	{
		for (int i = 0; i < pBuddyNeighbours->GetSize(); i++)
			if ((*pBuddyNeighbours)[i] != this)		// min. one neighbour != this
				return TRUE;	
	}
	return FALSE;
}

CLayoutObject* CLayoutObject::GetCurrentMultipagePartner(CPageTemplate* pPageTemplate, BOOL bIsPanoramaPage)
{
	if (m_nType == CLayoutObject::ControlMark)
		return this;

	if ( ! pPageTemplate)
		return this;

	if (bIsPanoramaPage)
	{
		CLayoutObject* pNeighbour = NULL;

		if (pPageTemplate->GetMultipageSide() == LEFT)
		{
			if (GetRealBorder(RIGHT) != 0.0f)		// 11/09/2010 - now by definition we can have a panorama page even if we have a trim
				return FALSE;						// 06/06/2012 - recorrect this behaviour again as it was originally (because Theiss-Druck intervened)

			switch (m_nHeadPosition)
			{
			case TOP:		pNeighbour = (m_RightNeighbours.GetSize() == 0) ? NULL : m_RightNeighbours[0]; break;
			case RIGHT:		pNeighbour = (m_LowerNeighbours.GetSize() == 0) ? NULL : m_LowerNeighbours[0]; break;
			case BOTTOM:	pNeighbour = (m_LeftNeighbours.GetSize()  == 0) ? NULL : m_LeftNeighbours[0];  break;
			case LEFT:		pNeighbour = (m_UpperNeighbours.GetSize() == 0) ? NULL : m_UpperNeighbours[0]; break;
			}
		}
		else
		{
			if (GetRealBorder(LEFT) != 0.0f)
				return FALSE;

			switch (m_nHeadPosition)
			{
			case TOP:		pNeighbour = (m_LeftNeighbours.GetSize()  == 0) ? NULL : m_LeftNeighbours[0];  break;
			case RIGHT:		pNeighbour = (m_UpperNeighbours.GetSize() == 0) ? NULL : m_UpperNeighbours[0]; break;
			case BOTTOM:	pNeighbour = (m_RightNeighbours.GetSize() == 0) ? NULL : m_RightNeighbours[0]; break;
			case LEFT:		pNeighbour = (m_LowerNeighbours.GetSize() == 0) ? NULL : m_LowerNeighbours[0]; break;
			}
		}

		if (!pNeighbour)
			return this;
		if (m_nHeadPosition != pNeighbour->m_nHeadPosition)
			return this;

		return pNeighbour;
	}
	else
	{
		CPageTemplate* pPartnerTemplate = pPageTemplate->GetFirstMultipagePartner();

		for (int i = 0; i < pPartnerTemplate->m_PrintSheetLocations.GetSize(); i++)
		{
			POSITION pos = CImpManDoc::GetDoc()->m_PrintSheetList.FindIndex(pPartnerTemplate->m_PrintSheetLocations[i].m_nPrintSheetIndex);
			if (!pos)
				continue;

			CPrintSheet& rPrintSheet = CImpManDoc::GetDoc()->m_PrintSheetList.GetAt(pos);
			CLayout*	 pLayout	 = rPrintSheet.GetLayout();
			if ( ! pLayout)
				continue;

			CLayoutSide* pLayoutSide = (pPartnerTemplate->m_PrintSheetLocations[i].m_nLayoutSide == FRONTSIDE) ? pLayoutSide = &pLayout->m_FrontSide 
																											   : pLayoutSide = &pLayout->m_BackSide;
			if ( ! pLayoutSide)
				continue;

			pos = pLayoutSide->m_ObjectList.FindIndex(pPartnerTemplate->m_PrintSheetLocations[i].m_nLayoutObjectIndex);
			if (pos)
			{
				CLayoutObject& rBuddyObject = pLayoutSide->m_ObjectList.GetAt(pos);
				if (rBuddyObject.GetLayout() == GetLayout())
					if (rBuddyObject.m_nComponentRefIndex == m_nComponentRefIndex)	// same layout and foldsheet layer
						return &rBuddyObject;
			}
		}
	}

	return this;
}


float CLayoutObject::GetObjectRefline(int nReferenceLine, BOOL bBorder)
{
	if (bBorder)
	{
		switch (nReferenceLine)
		{
		case LEFT:		return (m_LeftNeighbours.GetSize())  ? GetLeftWithBorder()	 : GetLeft();
		case RIGHT:		return (m_RightNeighbours.GetSize()) ? GetRightWithBorder()  : GetRight();
		case XCENTER:	return GetLeft()   + GetWidth()  / 2;
		case BOTTOM:	return (m_LowerNeighbours.GetSize()) ? GetBottomWithBorder() : GetBottom();
		case TOP:		return (m_UpperNeighbours.GetSize()) ? GetTopWithBorder()	 : GetTop();
		case YCENTER:	return GetBottom() + GetHeight() / 2;
		}
	}
	else
	{
		switch (nReferenceLine)
		{
		case LEFT:		return GetLeft();
		case RIGHT:		return GetRight();
		case XCENTER:	return GetLeft()   + GetWidth()  / 2;
		case BOTTOM:	return GetBottom();
		case TOP:		return GetTop();
		case YCENTER:	return GetBottom() + GetHeight() / 2;
		}
	}

	return 0.0f;
}

void CLayoutObject::UpdateReflineDistances(BYTE nDefaultReferenceLineInX, BYTE nDefaultReferenceLineInY)
{
	float fObjectXRefline = GetLayoutObjectRefline(m_reflinePosParams.m_nObjectReferenceLineInX, nDefaultReferenceLineInX);
	float fObjectYRefline = GetLayoutObjectRefline(m_reflinePosParams.m_nObjectReferenceLineInY, nDefaultReferenceLineInY);

	CLayoutSide* pLayoutSide = GetLayoutSide();
	if (pLayoutSide)
	{
		float fSheetXRefline = pLayoutSide->GetRefline(m_reflinePosParams.m_nReferenceItemInX, m_reflinePosParams.m_nReferenceLineInX, &m_reflinePosParams.m_hReferenceObjectInX);
		float fSheetYRefline = pLayoutSide->GetRefline(m_reflinePosParams.m_nReferenceItemInY, m_reflinePosParams.m_nReferenceLineInY, &m_reflinePosParams.m_hReferenceObjectInY);
		m_reflinePosParams.m_fDistanceX = fObjectXRefline - ( (fSheetXRefline != FLT_MAX) ? fSheetXRefline : 0.0f );
		m_reflinePosParams.m_fDistanceY = fObjectYRefline - ( (fSheetYRefline != FLT_MAX) ? fSheetYRefline : 0.0f );
	}
}

float CLayoutObject::GetLayoutObjectRefline(BYTE nReferenceLine, BYTE nDefaultReferenceLine)
{
	switch (nReferenceLine)
	{
	case LEFT:		return GetLeft();										
	case RIGHT:		return GetRight();										
	case XCENTER:	return GetLeft()   + GetWidth()  / 2.0f;	
	case BOTTOM:	return GetBottom();										
	case TOP:		return GetTop();											
	case YCENTER:	return GetBottom() + GetHeight() / 2.0f;	
	default:		{
						switch (nDefaultReferenceLine)
						{
						case LEFT:		return GetLeft();										
						case RIGHT:		return GetRight();										
						case XCENTER:	return GetLeft()   + GetWidth()  / 2.0f;	
						case BOTTOM:	return GetBottom();										
						case TOP:		return GetTop();											
						case YCENTER:	return GetBottom() + GetHeight() / 2.0f;	
						}
					}
					return 0.0f;
	}
}

int	CLayoutObject::GetMirrorHeadPosition(int nMirrorAxis)
{
	switch (nMirrorAxis)
	{
	case HORIZONTAL:	switch (m_nHeadPosition)
						{
						case TOP:		return BOTTOM; 
						case BOTTOM:	return TOP;	
						}
						break;
	case VERTICAL:		switch (m_nHeadPosition)
						{
						case LEFT:		return RIGHT;	
						case RIGHT:		return LEFT;	
						}
						break;
	}

	return m_nHeadPosition;
}

void CLayoutObject::Draw(CDC* pDC, BOOL bHighlight, BOOL bThumbnail, BOOL bShowMark, CPrintSheet* pPrintSheet, int nProductPartIndex, BOOL bShowShingling, BOOL bShowMeasures, CDisplayList* pDisplayList, BOOL bShowNoPageTemplate)
{
	if (m_nType == CLayoutObject::ControlMark)
	{
		if (bShowMark)
		{
			BOOL bShow = TRUE;
			if (bThumbnail)
			{
				CPageTemplate* pObjectTemplate = GetCurrentMarkTemplate();
				if (pObjectTemplate)
					if (pObjectTemplate->GetMarkType() == CMark::CropMark)	// don't show crop marks in thumbnails, because it is slow for complicated layouts while they cannot be seen
						bShow = FALSE;
			}

			if (bShow)
				DrawMark(pDC);
		}
		return;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pPrintSheet)
		pPrintSheet = (pDoc) ? pDoc->GetActPrintSheet() : NULL;
	if ( ! pPrintSheet)
		return;
	CPageTemplate* pObjectTemplate = GetCurrentPageTemplate(pPrintSheet);

	BOOL bIsFlatProduct = IsFlatProduct();

	CRect rcObject;
	rcObject.left	 = CPrintSheetView::WorldToLP(GetLeft());
	rcObject.top	 = CPrintSheetView::WorldToLP(GetTop());
	rcObject.right	 = CPrintSheetView::WorldToLP(GetRight());
	rcObject.bottom	 = CPrintSheetView::WorldToLP(GetBottom());

	CRect rcGrossFrame = CalcGrossFrame(pPrintSheet);
	pDC->SaveDC();
	pDC->BeginPath();
		pDC->Rectangle(rcObject);	
	pDC->EndPath();
	pDC->SelectClipPath(RGN_DIFF);
	COLORREF crGross = (bThumbnail) ? RGB(205,235,255) : RGB(50,170,255);
	CPrintSheetView::FillTransparent(pDC, rcGrossFrame, crGross, (bThumbnail) ? FALSE : TRUE, 35);
	pDC->RestoreDC(-1);

	CSize sizeObject(abs(rcObject.Width()), abs(rcObject.Height()));
	pDC->LPtoDP(&sizeObject);
	sizeObject.cx = DP2MM(pDC, sizeObject.cx); sizeObject.cy = DP2MM(pDC, sizeObject.cy);
	int nPenWidth = ( (sizeObject.cx < 2000) || (sizeObject.cy < 2000) ) ?  Pixel2ConstMM(pDC, 1)/2 : Pixel2ConstMM(pDC, 1);	// smaller than 20 mm -> take half line width
	CPen	lightBluePen(PS_SOLID, nPenWidth, (m_nStatus & Disabled) ? RGB(220,220,220) : RGB(160,160,255));	//LIGHTBLUE);
	CPen*	pOldPen = pDC->GetCurrentPen();

	pDC->SelectObject(lightBluePen);
	int nColorIndex = 0;
	if (bIsFlatProduct)
		if (bShowNoPageTemplate)
			nColorIndex = m_nGroupIndex;
		else
			nColorIndex = ( (m_nComponentRefIndex < 0) || (m_nComponentRefIndex >= pPrintSheet->m_flatProductRefs.GetSize()) ) ? m_nComponentRefIndex : pPrintSheet->m_flatProductRefs[m_nComponentRefIndex].m_nFlatProductIndex;
	else
		nColorIndex = (pDoc) ? pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex) : 0;
	nColorIndex = nColorIndex % MAX_OBJECT_COLORS;

	COLORREF crPageColor = (m_nStatus & Highlighted) ? LIGHTRED : theApp.m_objectColors[nColorIndex];
	float	 fDimmFactor = (bHighlight) ? 1.2f : 1.0f;
	CBrush*  pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);		// draw pages transparent in order to make visible overlappings
																			// disadvantage: slower display procedure
	crPageColor = CGraphicComponent::IncreaseColorBrightness(crPageColor, fDimmFactor);

	if (m_nStatus & Disabled)
		;
	else
		CPrintSheetView::FillTransparent(pDC,  rcObject, crPageColor, (bThumbnail) ? FALSE : TRUE, 150);
	CPrintSheetView::DrawRectangleExt(pDC, rcObject);
	CPrintSheetView::DrawHeadPosition(pDC, rcObject, m_nHeadPosition, (m_nStatus & Disabled) ? RGB(220,220,220) : CGraphicComponent::IncreaseColorBrightness(RGB(160,160,255), 1.05f));

	int nFontSize = CLayout::TransformFontSize(pDC, 18);	// 18 mm when plotted 1:1
	CFont font, boldFont, *pOldFont;
	font.CreateFont	   (nFontSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	boldFont.CreateFont(nFontSize, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	pOldFont = pDC->SelectObject(&font);
	COLORREF crOldTextColor = pDC->SetTextColor(BLUE);

	if ( ! bThumbnail)
	{
		if (bIsFlatProduct)
		{
			CString strOut;
			if (bShowNoPageTemplate)
				strOut.Format((pObjectTemplate->m_bLabelBackSide) ? _T(" %d* ") : _T(" %d "), m_nGroupIndex + 1);
			else
			{
				CFlatProduct& rFlatProduct = GetCurrentFlatProduct(pPrintSheet);
				if ( ! rFlatProduct.IsNull())
					strOut.Format((pObjectTemplate->m_bLabelBackSide) ? _T(" %d* ") : _T(" %d "), rFlatProduct.GetIndex() + 1);
			}
			CRect rcText = rcObject;
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(200,100,0));
			pDC->SelectObject(( (rcText.Width() < nFontSize * 3) || (abs(rcText.Height()) < nFontSize * 3)) ? &font : &boldFont);
			pDC->DrawText(strOut, rcText, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		}
		else
			// show format here only when not flat. Formats of flat products will be handled in DrawObjectIDFlatProduct() because of special spacing when objects are small and grouped
			if ( ! bThumbnail && bShowMeasures && (GetCurrentAttribute(pPrintSheet) & CLayoutObject::GroupLowerLeft) )
			{
				CString strWidth, strHeight, strOut;
				CPrintSheetView::TruncateNumber(strWidth,  GetWidth());
				CPrintSheetView::TruncateNumber(strHeight, GetHeight());
				strOut.Format(_T("%s x %s"), strWidth, strHeight);
				CRect rcText = rcObject; rcText.top -= nFontSize/2;
				pDC->SetBkMode(OPAQUE);
				pDC->SetBkColor(WHITE);
				pDC->SelectObject(&font);
				pDC->SetTextColor(BLACK);
				pDC->DrawText(strOut, rcText, DT_CENTER | DT_TOP | DT_SINGLELINE);
			}
	}

	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldTextColor);
	font.DeleteObject();
	boldFont.DeleteObject();

	if (pDisplayList)
	{
		if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)) && bIsFlatProduct)
		{
			CRect rcDI = rcObject;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)this, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, LayoutObject), (void*)pPrintSheet, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
					CPrintSheetView::FillTransparent(pDC,  rcObject, RED, TRUE);
			}
		}
		else
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetNavigationView)) && bIsFlatProduct)
			{
				CPrintComponentsView* pComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
				if (pComponentsView)
				{
					CPrintComponent& rPrintComponent = pComponentsView->GetActComponent();
					CFlatProduct* pFlatProduct = rPrintComponent.GetFlatProduct();
					if (pFlatProduct)
					{
						if (pFlatProduct == &GetCurrentFlatProduct(pPrintSheet))
						{
							CGraphicComponent gc;
							gc.DrawSelectionFrame(pDC, rcObject, TRUE, RGB(188,156,120));
						}
					}
				}
			}
	}

	CWnd* pWnd = (pDisplayList) ? pDisplayList->m_pParent : NULL;
	CPrintSheetView* pPrintSheetView = NULL;
	if (pWnd)
		pPrintSheetView = (pWnd->IsKindOf(RUNTIME_CLASS(CPrintSheetView))) ? (CPrintSheetView*)pWnd : NULL;

	CRect rcRect = rcObject; 
	pDC->LPtoDP(rcRect);
	rcRect.NormalizeRect();
	//if ( ! bShowMeasures)
	{
		if ( ( (rcRect.Width() > 10) && (rcRect.Height() > 10) ) || pDC->IsPrinting())
		{
			CString strObjectName = (pObjectTemplate) ? pObjectTemplate->m_strPageID : m_strObjectName;
			if (m_nType == CLayoutObject::Page) 
			{
				if (pObjectTemplate != NULL)
					DrawPageID(pDC, rcObject, *this, pPrintSheet, *pObjectTemplate, pPrintSheetView, crPageColor, bShowMeasures, bShowNoPageTemplate);
			}
			else
				DrawObjectID(pDC, rcObject, *this, strObjectName, _T(""), 0, pPrintSheetView, bHighlight, m_nStatus); 

		//	if ( (m_nType == CLayoutObject::Page) && pDoc->m_Bookblock.m_bShinglingActive && bShowShingling)
		//	{
		//		if (pDoc->m_Bookblock.m_nShinglingMethod == CBookblock::ShinglingMethodBottling)
		//		{
		//			DrawPageShingling(pDC, pObjectTemplate, TOP);
		//			DrawPageShingling(pDC, pObjectTemplate, BOTTOM);
		//		}
		//		else
		//			DrawPageShingling(pDC, pObjectTemplate, YCENTER);
		//	}
		}
	}

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);

	lightBluePen.DeleteObject();

	if (m_nStatus & Disabled)
		;
	else
		DrawOverFold(pDC, pPrintSheet, bThumbnail);
}

void CLayoutObject::DrawPageID(CDC* pDC, CRect& rPageRect, CLayoutObject& rObject, CPrintSheet* pActPrintSheet, CPageTemplate& rPageTemplate, CPrintSheetView* pView, COLORREF crBackground, BOOL bShowMeasures, BOOL bShowNoPageTemplate) 
{
	CString strOutput;
	if (rPageTemplate.m_nMultipageGroupNum == -1)
	{
		if (rObject.IsFlatProduct())
		{
			if ( ! (rObject.GetCurrentAttribute(pActPrintSheet) & CLayoutObject::GroupLowerLeft))
			{
				BOOL bBackside = FALSE;
				if ((rObject.GetLayoutSideIndex() == FRONTSIDE)  && rObject.m_bObjectTurned)
					bBackside = TRUE;
				else
					if ((rObject.GetLayoutSideIndex() == BACKSIDE)  && ! rObject.m_bObjectTurned)
						bBackside = TRUE;
				if (bBackside)
					DrawObjectIDFlatProduct(pDC, rPageRect, rObject, _T("*"), pActPrintSheet,  pView, crBackground, bShowMeasures, bShowNoPageTemplate);
			}
			else
				DrawObjectIDFlatProduct(pDC, rPageRect, rObject, rPageTemplate.m_strPageID, pActPrintSheet, pView, crBackground, bShowMeasures, bShowNoPageTemplate);
		}
		else
			DrawObjectID(pDC, rPageRect, rObject, rPageTemplate.m_strPageID, "", 0, pView, TRUE, rObject.m_nStatus);
	}
	else
		if (rObject.IsPanoramaPage(*pActPrintSheet))
		{
			DrawObjectID(pDC, rPageRect, rObject, rPageTemplate.m_strPageID, "", 0, pView, TRUE, rObject.m_nStatus);

			if (pView)
				if (pView->ShowBitmap())	
				{
					CLayoutObject* pBuddyObject	  = rObject.GetCurrentMultipagePartner(&rPageTemplate, TRUE);
					CPageTemplate* pBuddyTemplate = rPageTemplate.GetFirstMultipagePartner();
					CRect buddyPageRect;
					buddyPageRect.left	  = CPrintSheetView::WorldToLP(pBuddyObject->GetLeft());
					buddyPageRect.top	  = CPrintSheetView::WorldToLP(pBuddyObject->GetTop());
					buddyPageRect.right   = CPrintSheetView::WorldToLP(pBuddyObject->GetRight());
					buddyPageRect.bottom  = CPrintSheetView::WorldToLP(pBuddyObject->GetBottom());
					DrawObjectID(pDC, buddyPageRect, *pBuddyObject, pBuddyTemplate->m_strPageID, "", 0, pView, TRUE, rObject.m_nStatus);
				}
		}
		else
		{
			CPageTemplate* pBuddyTemplate = rPageTemplate.GetFirstMultipagePartner();
			if (rObject.IsMultipagePart(rPageTemplate, LEFT))
			{
				DrawObjectID(pDC, rPageRect, rObject, rPageTemplate.m_strPageID, (pBuddyTemplate) ? pBuddyTemplate->m_strPageID : "", 2, pView, TRUE, rObject.m_nStatus);
			}
			else
				if (rObject.IsMultipagePart(rPageTemplate, RIGHT))
				{
					DrawObjectID(pDC, rPageRect, rObject, (pBuddyTemplate) ? pBuddyTemplate->m_strPageID : "", rPageTemplate.m_strPageID, 1, pView, TRUE, rObject.m_nStatus);
				}
		}
}

void CLayoutObject::DrawObjectID(CDC* pDC, CRect& rObjectRect, CLayoutObject& rObject, const CString& strID, const CString& strID2, int nDimmedText, CPrintSheetView* pView, BOOL bHighlight, int nStatus) 
{
	if ( ! pDC->RectVisible(rObjectRect))
		return;

	BOOL bThumbnail = (pView || pDC->IsPrinting()) ? FALSE : TRUE;
	CFont pageIDFont;
	CreatePageIDFont(pDC, &pageIDFont, rObject.m_nHeadPosition, bThumbnail);
	CFont*		pOldFont = pDC->SelectObject(&pageIDFont);

	BOOL bShowMeasure	  = (pView) ? pView->ShowMeasure() : FALSE;
	BOOL bShowFoldSheet   = (pView) ? pView->ShowFoldSheet() : FALSE;
	BOOL bMarksViewActive = (pView) ? pView->MarksViewActive() : FALSE;
	BOOL bShowExposures   = (pView) ? pView->ShowExposures() : FALSE;
	BOOL bShowBitmap	  = (pView) ? pView->ShowBitmap() : FALSE;

	COLORREF	crOldColor   = pDC->GetTextColor(), crDimmedText;
	int			nOldBkMode   = pDC->SetBkMode((bShowBitmap) ? OPAQUE : TRANSPARENT);
	COLORREF	crOldBkColor = pDC->SetBkColor(RGB(242,242,255));

	if (bShowMeasure || bShowFoldSheet || bMarksViewActive || ! bHighlight)
	{
		pDC->SetTextColor((rObject.m_nType == CLayoutObject::Page) ? RGB(50,50,255) : BORDEAUX);
		crDimmedText = (rObject.m_nType == CLayoutObject::Page) ? HEAVENBLUE : RED;
	}
	else
		if (bShowExposures)
		{
			pDC->SetTextColor(BLACK);
			crDimmedText = DARKGRAY;
		}
		else
		{
			pDC->SetTextColor((rObject.m_nType == CLayoutObject::Page) ? BLUE : BORDEAUX);
			crDimmedText = (rObject.m_nType == CLayoutObject::Page) ? HEAVENBLUE : LIGHTRED;
		}

	CSize   textOffset;
	CString strOutput;
	if (strID2.IsEmpty())
		strOutput.Format(_T("  %s  "), strID);
	else
	{
		strOutput.Format(_T(" %s / %s "), strID, strID2);
		textOffset = pDC->GetTextExtent((nDimmedText == 1) ? strID + " / " : strID + "  ");
	}

	CSize textExtent = pDC->GetTextExtent(strOutput);
	int	  nXPos = 0, nYPos = 0, nXOff = 0, nYOff = 0;
	switch (rObject.m_nHeadPosition)
	{
		case LEFT  : switch(rObject.m_nIDPosition)
					 {
						case LEFT  : nXPos = rObjectRect.right - textExtent.cy; nYPos = rObjectRect.bottom;					break;
						case RIGHT : nXPos = rObjectRect.right - textExtent.cy; nYPos = rObjectRect.top - textExtent.cx;	break;
					 }
					 nXOff = 0;	nYOff = textOffset.cx;
					 break;
		case TOP   : switch(rObject.m_nIDPosition)
					 {
						case LEFT  : nXPos = rObjectRect.left;				    nYPos = rObjectRect.bottom + textExtent.cy; break;
						case RIGHT : nXPos = rObjectRect.right - textExtent.cx; nYPos = rObjectRect.bottom + textExtent.cy; break;
					 }
					 nXOff = textOffset.cx;	nYOff = 0;
					 break;
		case RIGHT : switch(rObject.m_nIDPosition)
					 {
						case LEFT  : nXPos = rObjectRect.left  + textExtent.cy; nYPos = rObjectRect.top;					break;
						case RIGHT : nXPos = rObjectRect.left  + textExtent.cy; nYPos = rObjectRect.bottom + textExtent.cx;	break;
					 }
					 nXOff = 0;	nYOff = -textOffset.cx;
					 break;
		case BOTTOM: switch(rObject.m_nIDPosition)
					 {
						case LEFT  : nXPos = rObjectRect.right;				    nYPos = rObjectRect.top - textExtent.cy;	break;
						case RIGHT : nXPos = rObjectRect.left  + textExtent.cx; nYPos = rObjectRect.top - textExtent.cy;	break;
					 }
					 nXOff = -textOffset.cx; nYOff = 0;
					 break;
	}

	pDC->SaveDC();	// save current clipping region
	pDC->IntersectClipRect(rObjectRect);

	switch (nDimmedText)
	{
	case 0:
		if (rObject.m_nStatus & CLayoutObject::Disabled)
			pDC->SetTextColor(RGB(220,220,220));
		pDC->TextOut(nXPos, nYPos, "  " + strID); 
		if ( ! strID2.IsEmpty()) 
			pDC->TextOut(nXPos + nXOff, nYPos + nYOff, " / " + strID2);
		break;
	case 1:
		if ( ! strID2.IsEmpty()) 
			pDC->TextOut(nXPos + nXOff, nYPos + nYOff, strID2);
		pDC->SetTextColor(crDimmedText);
		pDC->TextOut(nXPos, nYPos, strID + "  ");
		break;
	case 2:
		pDC->TextOut(nXPos, nYPos, "  " + strID);
		pDC->SetTextColor(crDimmedText);
		if ( ! strID2.IsEmpty()) 
			pDC->TextOut(nXPos + nXOff, nYPos + nYOff, " / " + strID2);
		break;
	}

	pDC->RestoreDC(-1);

	pDC->SetTextColor(crOldColor);
	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);
	pDC->SelectObject(pOldFont);
	pageIDFont.DeleteObject();

}

void CLayoutObject::DrawObjectIDFlatProduct(CDC* pDC, CRect& rObjectRect, CLayoutObject& rObject, const CString& strID, CPrintSheet* pPrintSheet, CPrintSheetView* pView, COLORREF crBackground, BOOL bShowMeasures, BOOL bShowNoPageTemplate) 
{
	if ( ! pDC->RectVisible(rObjectRect))
		return;

	CRect rcOutrect = rObjectRect;
	pDC->LPtoDP(rcOutrect);
	rcOutrect.NormalizeRect();
	rcOutrect.DeflateRect(5, 5);
	pDC->DPtoLP(rcOutrect);

	BOOL		bThumbnail	 = (pView || pDC->IsPrinting())	? FALSE : TRUE;
	BOOL		bShowBitmap	 = (pView) ? pView->ShowBitmap(): FALSE;
	int			nViewMode	 = (pView) ? pView->ViewMode()	: ID_VIEW_SINGLE_SHEETS;

	int			nOldBkMode   = pDC->SetBkMode(TRANSPARENT);
	COLORREF	crOldColor   = pDC->SetTextColor(RGB(0,0,255));
	CFont*		pOldFont	 = pDC->GetCurrentFont();

	int	   					nXPos1 = 0, nYPos1 = 0, nXPos2 = 0, nYPos2 = 0;
	CFont  					pageIDFont;
	BOOL   					bDoFit = FALSE;
	CRect  					rcCliprect1, rcCliprect2, rcBox;
	CStepAndRepeatMatrix	stepAndRepeatMatrix;

	CString strFormat;
	if ( (nViewMode != ID_VIEW_ALL_SHEETS) && bShowMeasures)
	{
		CString strWidth, strHeight;
		CPrintSheetView::TruncateNumber(strWidth,  rObject.GetRealWidth()); 
		CPrintSheetView::TruncateNumber(strHeight, rObject.GetRealHeight());
		strFormat.Format(_T(" %s x %s "), strWidth, strHeight);
	}

	CreatePageIDFont(pDC, &pageIDFont, TOP, bThumbnail);
	pDC->SelectObject(&pageIDFont);

	CSize  textExtent1 = pDC->GetTextExtent(strID);	
	CSize  textExtent2 = pDC->GetTextExtent(strFormat);
	nXPos1 = rcOutrect.left;						
	nYPos1 = rcOutrect.bottom + textExtent1.cy;	
	rcCliprect1 = CRect(nXPos1, nYPos1 - textExtent1.cy, nXPos1 + textExtent1.cx, nYPos1);
	rcCliprect1.right = min(rcCliprect1.right, rcOutrect.right);
	bDoFit = (nXPos1 + textExtent1.cx <= rcOutrect.right) ? TRUE : FALSE;

	nXPos2 = rcOutrect.left;						
	nYPos2 = rcOutrect.top;	
	rcCliprect2 = CRect(nXPos2, nYPos2 - textExtent2.cy, nXPos2 + textExtent2.cx, nYPos2);

	if ( ! bDoFit)
	{
		stepAndRepeatMatrix.Create(&rObject, CPoint(0, 0), pPrintSheet, CStepAndRepeatMatrix::TakePages, FALSE);
		rcBox = CRect(CPrintSheetView::WorldToLP(stepAndRepeatMatrix.GetBoxLeft()),  CPrintSheetView::WorldToLP(stepAndRepeatMatrix.GetBoxBottom()),
					  CPrintSheetView::WorldToLP(stepAndRepeatMatrix.GetBoxRight()), CPrintSheetView::WorldToLP(stepAndRepeatMatrix.GetBoxTop()));

		pDC->LPtoDP(rcBox);
		rcBox.NormalizeRect();
		rcBox.DeflateRect(5,5);
		pDC->DPtoLP(rcBox);
		rcOutrect.right = rcBox.right; rcOutrect.top = rcBox.top;

		rcCliprect1 = CRect(nXPos1, nYPos1 - textExtent1.cy, nXPos1 + textExtent1.cx, nYPos1);
		rcCliprect1.right = min(rcCliprect1.right, rcOutrect.right);
		bDoFit = (nXPos1 + textExtent1.cx <= rcOutrect.right) ? TRUE : FALSE;

		nYPos2 = rcOutrect.top;	
		rcCliprect2 = CRect(nXPos2, nYPos2 - textExtent2.cy, nXPos2 + textExtent2.cx, nYPos2);

		if ( ! bDoFit)
		{
			pageIDFont.DeleteObject();
			CreatePageIDFont(pDC, &pageIDFont, LEFT, bThumbnail);
			pDC->SelectObject(&pageIDFont);
			
			nXPos1 = rcOutrect.left;
			nYPos1 = rcOutrect.bottom;					
			rcCliprect1 = CRect(nXPos1, nYPos1, nXPos1 + textExtent1.cy, nYPos1 + textExtent1.cx);
			rcCliprect1.bottom = min(rcCliprect1.bottom, rcOutrect.top);
			bDoFit = (nYPos1 + textExtent1.cx <= rcOutrect.top) ? TRUE : FALSE;
			
			if ( ! bDoFit && (rcOutrect.Width() > abs(rcOutrect.Height())) )	// if also don't fit but head on top is better -> go back 
			{
				pageIDFont.DeleteObject();
				CreatePageIDFont(pDC, &pageIDFont, TOP, bThumbnail);
				pDC->SelectObject(&pageIDFont);
				nYPos1 = rcOutrect.bottom + textExtent1.cy;	
				rcCliprect1 = CRect(nXPos1, nYPos1 - textExtent1.cy, nXPos1 + textExtent1.cx, nYPos1);
				rcCliprect1.right = min(rcCliprect1.right, rcOutrect.right);
			}
		}
	}

	if (rObject.m_nStatus & CLayoutObject::Disabled)
		pDC->SetTextColor(RGB(220,220,220));

	if ( ! rcCliprect1.IsRectEmpty() && ! bShowNoPageTemplate)
	{
		pDC->FillSolidRect(rcCliprect1, WHITE);
		CPrintSheetView::FillTransparent(pDC, rcCliprect1, crBackground, TRUE, 200);
		pDC->SaveDC();
		pDC->IntersectClipRect(rcCliprect1);
		pDC->TextOut(nXPos1, nYPos1, strID); 
		pDC->RestoreDC(-1);

		pDC->ExcludeClipRect(rcCliprect1);	// prevent text from being covered by following drawings
	}

	if ( (nViewMode != ID_VIEW_ALL_SHEETS) && bShowMeasures)
	{
		pageIDFont.DeleteObject();
		CreatePageIDFont(pDC, &pageIDFont, TOP, bThumbnail);
		pDC->SelectObject(&pageIDFont);
		pDC->SetTextColor(BLACK);

		if ( ! rcCliprect2.IsRectEmpty())
		{
			pDC->FillSolidRect(rcCliprect2, WHITE);
			//CPrintSheetView::FillTransparent(pDC, rcCliprect2, crBackground, TRUE, 200);
			pDC->SaveDC();
			pDC->IntersectClipRect(rcCliprect2);
			pDC->TextOut(nXPos2, nYPos2, strFormat);
			pDC->RestoreDC(-1);

			pDC->ExcludeClipRect(rcCliprect2);	// prevent text from being covered by following drawings
		}
	}

	pDC->SetTextColor(crOldColor);
	pDC->SetBkMode(nOldBkMode);
	pDC->SelectObject(pOldFont);
	pageIDFont.DeleteObject();
}

BOOL CLayoutObject::TextFits(CDC* pDC, int nXPos, int nYPos, CRect rcObject, int nHeadPosition, const CString& string)
{
	CRect rcText;
	CSize extent = pDC->GetTextExtent(string);
	switch (nHeadPosition)
	{
		case LEFT  : rcText.left = nXPos; rcText.bottom = nYPos; rcText.right = nXPos + extent.cy; rcText.top = nYPos + extent.cx;	 break;
		case TOP   : rcText.left = nXPos; rcText.bottom = nYPos - extent.cy; rcText.right = nXPos + extent.cx; rcText.top = nYPos;	 break;
		case RIGHT : rcText.left = nXPos - extent.cy; rcText.bottom = nYPos - extent.cx; rcText.right = nXPos; rcText.top = nYPos;	 break;
		case BOTTOM: rcText.left = nXPos - extent.cx; rcText.bottom = nYPos; rcText.right = nXPos; rcText.top = nYPos + extent.cy;	 break;
	}

	rcObject.NormalizeRect();
	rcText.NormalizeRect();
	CSize size(3, 3);
	pDC->DPtoLP(&size);
	rcObject.InflateRect(size);

	if ( ! rcObject.PtInRect(rcText.TopLeft()))
		return FALSE;
	if ( ! rcObject.PtInRect(rcText.BottomRight()))
		return FALSE;

	return TRUE;
}

void CLayoutObject::DrawPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nZOrder) 
{
	CLayoutSide* pLayoutSide = GetLayoutSide();
	if ( ! pLayoutSide)
		return;

	if (IsAutoMarkDisabled(pPrintSheet, pLayoutSide->m_nLayoutSide))
		return;

	CString	 strPreviewFile;
	CRect	 exposureRect, objectRect;
	CPlate*  pPlate	= pPrintSheet->GetTopmostPlate(pLayoutSide->m_nLayoutSide);

	if ( ! pPlate)
		return;

	int nColorDefinitionIndex = (pPlate->m_nColorDefinitionIndex == -1) ? 0 : pPlate->m_nColorDefinitionIndex;	// -1 = default plate -> show 1st colorant
	
	CExposure* pExposure = GetExposure(pPlate, GetLayout(), GetLayoutSideIndex());
	if ( ! pExposure)
		return;

	CPageTemplate* pObjectTemplate = (m_nType == CLayoutObject::ControlMark) ? GetCurrentMarkTemplate() : GetCurrentPageTemplate(pPrintSheet);

	if (pObjectTemplate)
	{
		float fSheetClipBoxLeft	  = pExposure->GetSheetBleedBox(LEFT,   this, pObjectTemplate);
		float fSheetClipBoxBottom = pExposure->GetSheetBleedBox(BOTTOM, this, pObjectTemplate);
		float fSheetClipBoxRight  = pExposure->GetSheetBleedBox(RIGHT,  this, pObjectTemplate);
		float fSheetClipBoxTop	  = pExposure->GetSheetBleedBox(TOP,	this, pObjectTemplate);

		pObjectTemplate->GetGridClipBox(fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop);

		exposureRect.left	= CPrintSheetView::WorldToLP(fSheetClipBoxLeft);
		exposureRect.bottom = CPrintSheetView::WorldToLP(fSheetClipBoxBottom);
		exposureRect.right	= CPrintSheetView::WorldToLP(fSheetClipBoxRight);
		exposureRect.top	= CPrintSheetView::WorldToLP(fSheetClipBoxTop);
		exposureRect.NormalizeRect();
	}

	if (pDC->RectVisible(exposureRect))
	{
		if (pObjectTemplate)
		{
			if ( (pPlate->m_nColorDefinitionIndex == -1) && (m_nType == CLayoutObject::ControlMark) )	// if no plates, show 1st sep of marks
				nColorDefinitionIndex = (pObjectTemplate->m_ColorSeparations.GetSize()) ? pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1;

			CPageSource*	   pObjectSource		= pObjectTemplate->GetObjectSource		(nColorDefinitionIndex, TRUE, pPrintSheet, pLayoutSide->m_nLayoutSide);
			CPageSourceHeader* pObjectSourceHeader	= pObjectTemplate->GetObjectSourceHeader(nColorDefinitionIndex, TRUE, pPrintSheet, pLayoutSide->m_nLayoutSide);
			SEPARATIONINFO*	   pSepInfo				= pObjectTemplate->GetSeparationInfo	(nColorDefinitionIndex, TRUE, pPrintSheet, pLayoutSide->m_nLayoutSide);

			if (pObjectSource && pSepInfo)
			{
				if (m_nType == CLayoutObject::ControlMark)
					if (pObjectSource->GetSystemGeneratedMarkType() == CMark::FoldMark)
					{
						if (nZOrder == -1)
							return;
						int	nIndex = pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
						if (nIndex != -1)
						{
							CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
							CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
							strParams.TrimLeft(); strParams.TrimRight();
							int nZOrderMark = (strParams.Find(_T("/Background")) != -1) ? CFoldMark::Background : CFoldMark::Foreground;
							if (nZOrder != nZOrderMark)
								return;
						}
					}
					else
						if (nZOrder != -1)	
							return;

				CRect outBBox, objectBBox;
				objectBBox.left	   = CPrintSheetView::WorldToLP(pExposure->GetContentTrimBox(LEFT,   this, pObjectTemplate));
				objectBBox.top	   = CPrintSheetView::WorldToLP(pExposure->GetContentTrimBox(TOP,	this, pObjectTemplate));
				objectBBox.right   = CPrintSheetView::WorldToLP(pExposure->GetContentTrimBox(RIGHT,  this, pObjectTemplate));
				objectBBox.bottom  = CPrintSheetView::WorldToLP(pExposure->GetContentTrimBox(BOTTOM, this, pObjectTemplate));

				int	nOutBBoxWidth, nOutBBoxHeight;
				if ((pObjectTemplate->m_fContentRotation == 0.0f) || (pObjectTemplate->m_fContentRotation == 180.0f))
				{
					nOutBBoxWidth  = CPrintSheetView::WorldToLP((pObjectSource->GetBBox(RIGHT, pObjectSourceHeader) - pObjectSource->GetBBox(LEFT,   pObjectSourceHeader)) * pObjectTemplate->m_fContentScaleX);
					nOutBBoxHeight = CPrintSheetView::WorldToLP((pObjectSource->GetBBox(TOP,   pObjectSourceHeader) - pObjectSource->GetBBox(BOTTOM, pObjectSourceHeader)) * pObjectTemplate->m_fContentScaleY);
				}
				else
				{
					nOutBBoxWidth  = CPrintSheetView::WorldToLP((pObjectSource->GetBBox(TOP,   pObjectSourceHeader) - pObjectSource->GetBBox(BOTTOM, pObjectSourceHeader)) * pObjectTemplate->m_fContentScaleX);
					nOutBBoxHeight = CPrintSheetView::WorldToLP((pObjectSource->GetBBox(RIGHT, pObjectSourceHeader) - pObjectSource->GetBBox(LEFT,   pObjectSourceHeader)) * pObjectTemplate->m_fContentScaleY);
				}
				if ((m_nHeadPosition == LEFT) || (m_nHeadPosition == RIGHT))
				{
					int nTemp = nOutBBoxWidth; nOutBBoxWidth = nOutBBoxHeight; nOutBBoxHeight = nTemp;
				}

				outBBox.left   = objectBBox.CenterPoint().x - nOutBBoxWidth  / 2;
				outBBox.bottom = objectBBox.CenterPoint().y - nOutBBoxHeight / 2;
				outBBox.right  = objectBBox.CenterPoint().x + nOutBBoxWidth  / 2;
				outBBox.top    = objectBBox.CenterPoint().y + nOutBBoxHeight / 2;

				switch (m_nHeadPosition)
				{
				case TOP:		outBBox	+= CSize(CPrintSheetView::WorldToLP( pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset)), 
											     CPrintSheetView::WorldToLP( pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset))); 
								break;
				case RIGHT:		outBBox	+= CSize(CPrintSheetView::WorldToLP( pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset)), 
											     CPrintSheetView::WorldToLP(-pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset))); 
								break;
				case BOTTOM:	outBBox	+= CSize(CPrintSheetView::WorldToLP(-pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset)), 
											     CPrintSheetView::WorldToLP(-pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset))); 
								break;
				case LEFT:		outBBox	+= CSize(CPrintSheetView::WorldToLP(-pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset)), 
											     CPrintSheetView::WorldToLP( pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset))); 
								break;
				}

				if (pObjectSource->m_nType == CPageSource::SystemCreated)
					CPrintSheetView::DrawSystemCreatedContentPreview(pDC, pObjectTemplate, pObjectSource, objectBBox, this, m_nHeadPosition, pObjectSource->m_strSystemCreationInfo, pPrintSheet);
				else
				{
					strPreviewFile = pObjectSource->GetPreviewFileName(pSepInfo->m_nPageNumInSourceFile);
					pDC->SaveDC();	// save current clipping region
					pDC->IntersectClipRect(exposureRect);

					CPageListView::DrawPreview(pDC, outBBox, m_nHeadPosition, pObjectTemplate->m_fContentRotation, strPreviewFile);

					pDC->RestoreDC(-1);;
				}
			}
		}

		//m_DisplayList.RegisterItem(pDC, exposureRect, exposureRect, (void*)&rExposure, 
		//						   DISPLAY_ITEM_REGISTRY(CPrintSheetView, Exposure), (void*)pActPrintSheet, CDisplayItem::ForceRegister); 
	}
}

void CLayoutObject::DrawMark(CDC* pDC)
{
	CRect rcObject;
	rcObject.left	 = CPrintSheetView::WorldToLP(GetLeft());
	rcObject.top	 = CPrintSheetView::WorldToLP(GetTop());
	rcObject.right	 = CPrintSheetView::WorldToLP(GetRight());
	rcObject.bottom	 = CPrintSheetView::WorldToLP(GetBottom());

	if (pDC->RectVisible(rcObject))
	{
		BOOL bDoHighLightMark = FALSE;
		CPen	markPen (PS_SOLID, Pixel2ConstMM(pDC, 1), (bDoHighLightMark) ? BORDEAUX : RGB(255,200,200));//LIGHTRED);	
		CBrush  markBrush((bDoHighLightMark) ? RGB(255,128,128) : RGB(255,220,220));
		CBrush* pOldBrush = pDC->SelectObject(&markBrush);
		CPen*	pOldPen	  = pDC->SelectObject(&markPen);

		CPrintSheetView::DrawRectangleExt(pDC, rcObject);
		//CPrintSheetView::DrawHeadPosition(pDC, rcObject, m_nHeadPosition, /*(bDoHighLightMark) ? BORDEAUX : */LIGHTRED);

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		markBrush.DeleteObject();
		markPen.DeleteObject();
	}
}

CRect CLayoutObject::CalcGrossFrame(CPrintSheet* pPrintSheet)
{
	CRect rcGrossFrame(0,0,0,0);

	if ( ! pPrintSheet)
		return rcGrossFrame;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcGrossFrame;
	if (m_nType != CLayoutObject::Page)
		return rcGrossFrame;
	CPageTemplate* pPageTemplate = GetCurrentPageTemplate(pPrintSheet);
	if ( ! pPageTemplate)
		return rcGrossFrame;

	CBoundProductPart&  rProductPart = pPageTemplate->GetProductPart();
	CBoundProduct&		rProduct	 = rProductPart.GetBoundProduct();

	CRect rcObject;
	rcObject.left	 = CPrintSheetView::WorldToLP(GetLeft());
	rcObject.top	 = CPrintSheetView::WorldToLP(GetTop());
	rcObject.right	 = CPrintSheetView::WorldToLP(GetRight());
	rcObject.bottom	 = CPrintSheetView::WorldToLP(GetBottom());

	CRect rcTrimFrame = rcObject;
	float fLeftTrim	 = 0.0f, fBottomTrim = 0.0f, fRightTrim = 0.0f, fTopTrim  = 0.0f;
	if ( ! IsFlatProduct())
	{
		fLeftTrim	= rProductPart.m_trimmingParams.m_fSideTrim;
		fBottomTrim = rProductPart.m_trimmingParams.m_fFootTrim;
		fRightTrim  = rProductPart.m_trimmingParams.m_fSideTrim;
		fTopTrim    = rProductPart.m_trimmingParams.m_fHeadTrim;

		switch (m_nHeadPosition)
		{
		case TOP:		rcTrimFrame.top    += CPrintSheetView::WorldToLP(fTopTrim); 
						rcTrimFrame.bottom -= CPrintSheetView::WorldToLP(fBottomTrim); 
						rcTrimFrame.left   -= ( (m_nIDPosition == LEFT)  || (IsFlatProduct()) ) ? CPrintSheetView::WorldToLP(fLeftTrim)  : 0L; 
						rcTrimFrame.right  += ( (m_nIDPosition == RIGHT) || (IsFlatProduct()) ) ? CPrintSheetView::WorldToLP(fRightTrim) : 0L;  
						break;
		case BOTTOM:	rcTrimFrame.top    += CPrintSheetView::WorldToLP(fBottomTrim); 
						rcTrimFrame.bottom -= CPrintSheetView::WorldToLP(fTopTrim); 
						rcTrimFrame.left   -= ( (m_nIDPosition == RIGHT) || (IsFlatProduct()) ) ? CPrintSheetView::WorldToLP(fRightTrim) : 0L; 
						rcTrimFrame.right  += ( (m_nIDPosition == LEFT)  || (IsFlatProduct()) ) ? CPrintSheetView::WorldToLP(fLeftTrim)  : 0L;  
						break;
		case LEFT:		rcTrimFrame.left   -= CPrintSheetView::WorldToLP(fTopTrim); 
						rcTrimFrame.right  += CPrintSheetView::WorldToLP(fBottomTrim); 
						rcTrimFrame.top    += ( (m_nIDPosition == RIGHT) || (IsFlatProduct()) ) ? CPrintSheetView::WorldToLP(fRightTrim) : 0L; 
						rcTrimFrame.bottom -= ( (m_nIDPosition == LEFT)  || (IsFlatProduct()) ) ? CPrintSheetView::WorldToLP(fLeftTrim)  : 0L;  
						break;
		case RIGHT:		rcTrimFrame.left   -= CPrintSheetView::WorldToLP(fBottomTrim); 
						rcTrimFrame.right  += CPrintSheetView::WorldToLP(fTopTrim); 
						rcTrimFrame.top    += ( (m_nIDPosition == LEFT)  || (IsFlatProduct()) ) ? CPrintSheetView::WorldToLP(fLeftTrim)  : 0L; 
						rcTrimFrame.bottom -= ( (m_nIDPosition == RIGHT) || (IsFlatProduct()) ) ? CPrintSheetView::WorldToLP(fRightTrim) : 0L;  
						break;
		}

		long  lSpine	   = CPrintSheetView::WorldToLP(rProduct.m_bindingParams.GetSpine(rProductPart) / 2.0f);
		CRect rcSpineFrame = rcObject;
		switch (m_nHeadPosition)
		{
		case TOP:		rcSpineFrame.left   -= (m_nIDPosition == RIGHT) ? lSpine : 0; 
						rcSpineFrame.right  += (m_nIDPosition == LEFT)  ? lSpine : 0;  
						break;
		case BOTTOM:	rcSpineFrame.left   -= (m_nIDPosition == LEFT)  ? lSpine : 0; 
						rcSpineFrame.right  += (m_nIDPosition == RIGHT) ? lSpine : 0;  
						break;
		case LEFT:		rcSpineFrame.top    += (m_nIDPosition == LEFT)  ? lSpine : 0; 
						rcSpineFrame.bottom -= (m_nIDPosition == RIGHT) ? lSpine : 0;  
						break;
		case RIGHT:		rcSpineFrame.top    += (m_nIDPosition == RIGHT) ? lSpine : 0; 
						rcSpineFrame.bottom -= (m_nIDPosition == LEFT)  ? lSpine : 0;  
						break;
		}
		rcTrimFrame.NormalizeRect(); rcSpineFrame.NormalizeRect();
		rcGrossFrame.UnionRect(rcTrimFrame, rcSpineFrame);
	}
	else
	{
		int  nSide = GetLayoutSideIndex();
		BOOL bTurned  = ( (nSide == FRONTSIDE) && m_bObjectTurned ) || ( (nSide == BACKSIDE) && ! m_bObjectTurned) ? TRUE : FALSE;
		CFlatProduct& rFlatProduct = GetCurrentFlatProduct(pPrintSheet);
		fLeftTrim	= (bTurned) ? rFlatProduct.m_fRightTrim : rFlatProduct.m_fLeftTrim;
		fBottomTrim = rFlatProduct.m_fBottomTrim;
		fRightTrim  = (bTurned) ? rFlatProduct.m_fLeftTrim  : rFlatProduct.m_fRightTrim;
		fTopTrim    = rFlatProduct.m_fTopTrim;

		switch (m_nHeadPosition)
		{
		case TOP:		rcTrimFrame.top    += CPrintSheetView::WorldToLP(fTopTrim); 
						rcTrimFrame.bottom -= CPrintSheetView::WorldToLP(fBottomTrim); 
						rcTrimFrame.left   -= CPrintSheetView::WorldToLP(fLeftTrim); 
						rcTrimFrame.right  += CPrintSheetView::WorldToLP(fRightTrim); 
						break;
		case BOTTOM:	rcTrimFrame.top    += CPrintSheetView::WorldToLP(fBottomTrim); 
						rcTrimFrame.bottom -= CPrintSheetView::WorldToLP(fTopTrim); 
						rcTrimFrame.left   -= CPrintSheetView::WorldToLP(fRightTrim); 
						rcTrimFrame.right  += CPrintSheetView::WorldToLP(fLeftTrim); 
						break;
		case LEFT:		rcTrimFrame.top    += CPrintSheetView::WorldToLP(fRightTrim); 
						rcTrimFrame.bottom -= CPrintSheetView::WorldToLP(fLeftTrim); 
						rcTrimFrame.left   -= CPrintSheetView::WorldToLP(fTopTrim); 
						rcTrimFrame.right  += CPrintSheetView::WorldToLP(fBottomTrim); 
						break;
		case RIGHT:		rcTrimFrame.top    += CPrintSheetView::WorldToLP(fLeftTrim); 
						rcTrimFrame.bottom -= CPrintSheetView::WorldToLP(fRightTrim); 
						rcTrimFrame.left   -= CPrintSheetView::WorldToLP(fBottomTrim); 
						rcTrimFrame.right  += CPrintSheetView::WorldToLP(fTopTrim); 
						break;
		}

		rcTrimFrame.NormalizeRect();
		rcGrossFrame = rcTrimFrame;
	}

	return rcGrossFrame;
}

CRect CLayoutObject::CalcOverFoldFrame(CPrintSheet* pPrintSheet)
{
	CRect rcOverFold(0,0,0,0);

	if ( ! pPrintSheet)
		return rcOverFold;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcOverFold;
	if (m_nType != CLayoutObject::Page)
		return rcOverFold;
	if (IsFlatProduct())
		return rcOverFold;
	CPageTemplate* pPageTemplate = GetCurrentPageTemplate(pPrintSheet);
	if ( ! pPageTemplate)
		return rcOverFold;
	CBoundProduct& rProduct = pPageTemplate->GetProductPart().GetBoundProduct();
	if (rProduct.m_bindingParams.m_nOverFoldSide == CBindingProcess::OverFoldNone)
		return rcOverFold;
	CFoldSheet*		 pFoldSheet = pPageTemplate->GetFoldSheet();
	CFoldSheetLayer* pLayer		= pPrintSheet->GetFoldSheetLayer(m_nComponentRefIndex);
	if ( ! pFoldSheet || ! pLayer)
		return rcOverFold;
	if ( (rProduct.m_bindingParams.m_nOverFoldSide == CBindingProcess::OverFoldFront) && (pLayer->m_FrontSpineArray[m_nLayerPageIndex] == RIGHT) )
		return rcOverFold;
	if ( (rProduct.m_bindingParams.m_nOverFoldSide == CBindingProcess::OverFoldBack)  && (pLayer->m_FrontSpineArray[m_nLayerPageIndex] == LEFT) )
		return rcOverFold;
	if (rProduct.m_bindingParams.m_fOverFold == 0.0f)
		return rcOverFold;

	CRect rcGrossFrame	= CalcGrossFrame(pPrintSheet);
	long lOverfold		= CPrintSheetView::WorldToLP(rProduct.m_bindingParams.m_fOverFold);

	switch (m_nHeadPosition)
	{
	case TOP:		rcOverFold.top    = rcGrossFrame.top; 
					rcOverFold.bottom = rcGrossFrame.bottom; 
					rcOverFold.left   = (m_nIDPosition == LEFT)  ? rcGrossFrame.left   - lOverfold : rcGrossFrame.right; 
					rcOverFold.right  = (m_nIDPosition == LEFT)  ? rcGrossFrame.left			   : rcGrossFrame.right  + lOverfold; 
					break; 
	case BOTTOM:	rcOverFold.top    = rcGrossFrame.top; 
					rcOverFold.bottom = rcGrossFrame.bottom; 
					rcOverFold.left   = (m_nIDPosition == RIGHT) ? rcGrossFrame.left   - lOverfold : rcGrossFrame.right; 
					rcOverFold.right  = (m_nIDPosition == RIGHT) ? rcGrossFrame.left			   : rcGrossFrame.right  + lOverfold; 
					break;
	case LEFT:		rcOverFold.left   = rcGrossFrame.left; 
					rcOverFold.right  = rcGrossFrame.right; 
					rcOverFold.bottom = (m_nIDPosition == LEFT)  ? rcGrossFrame.top				   : rcGrossFrame.bottom + lOverfold; 
					rcOverFold.top    = (m_nIDPosition == LEFT)  ? rcGrossFrame.top	   - lOverfold : rcGrossFrame.bottom; 
					break;
	case RIGHT:		rcOverFold.left   = rcGrossFrame.left; 
					rcOverFold.right  = rcGrossFrame.right; 
					rcOverFold.bottom = (m_nIDPosition == RIGHT) ? rcGrossFrame.top				   : rcGrossFrame.bottom + lOverfold; 
					rcOverFold.top    = (m_nIDPosition == RIGHT) ? rcGrossFrame.top	   - lOverfold : rcGrossFrame.bottom; 
					break;
	}

	return rcOverFold;
}

void CLayoutObject::DrawOverFold(CDC* pDC, CPrintSheet* pPrintSheet, BOOL bThumbnail)
{
	CRect rcOverFoldFrame = CalcOverFoldFrame(pPrintSheet);
	if (rcOverFoldFrame.IsRectEmpty())
		return;

	//CSize szSize(1, 1);
	//pDC->DPtoLP(&szSize);
	//rcOverfold.InflateRect(szSize);

	CPrintSheetView::FillTransparent(pDC, rcOverFoldFrame, RGB(210,240,5), (bThumbnail) ? FALSE : TRUE, 50, TRUE);
}

void CLayoutObject::DrawPageShingling(CDC* pDC, CPageTemplate* pPageTemplate, int nPosition)
{
	if ( ! pPageTemplate)
		return;

	float fShinglingOffset = (nPosition == BOTTOM) ? pPageTemplate->m_fShinglingOffsetXFoot : pPageTemplate->m_fShinglingOffsetX;
	CString strShinglingOffset, strString;
	if (fabs(fShinglingOffset) > 0.009f)
		CPrintSheetView::TruncateNumber(strString, (float)fabs(fShinglingOffset));
	if (pPageTemplate->m_bShinglingOffsetLocked) 
		if (strString.IsEmpty())
			strShinglingOffset = _T(" * ");
		else
			strShinglingOffset.Format(_T(" %s * "), strString);
	else
		strShinglingOffset.Format(_T(" %s "), strString);

	if ( ! strShinglingOffset.IsEmpty())
	{
		CRect pageRect;
		pageRect.left	= CPrintSheetView::WorldToLP(GetLeft());
		pageRect.top	= CPrintSheetView::WorldToLP(GetTop());
		pageRect.right	= CPrintSheetView::WorldToLP(GetRight());
		pageRect.bottom	= CPrintSheetView::WorldToLP(GetBottom());

		CRect rcRect = pageRect; pDC->LPtoDP(rcRect);
		if ( (rcRect.Width() < 50) || (rcRect.Height() < 50) )	// don't draw if page is too small
			return;

		CSize textExtent = pDC->GetTextExtent(strShinglingOffset);
		int nXPos = pageRect.CenterPoint().x - textExtent.cx/2;				  
		int nYPos = pageRect.CenterPoint().y + textExtent.cy/2; 
		CSize szOffset(0,0);
		switch (nPosition)
		{
		case TOP   : szOffset.cx =  textExtent.cx; szOffset.cy =  textExtent.cy; break;
		case BOTTOM: szOffset.cx = -textExtent.cx; szOffset.cy = -textExtent.cy; break;
		}
		szOffset.cy = (long)((float)(szOffset.cy) * 1.5f + 0.5f);
		switch (m_nHeadPosition)
		{
		case TOP:		nYPos += szOffset.cy; break;
		case RIGHT:		nXPos += szOffset.cx; break;
		case BOTTOM:	nYPos -= szOffset.cy; break;
		case LEFT:		nXPos -= szOffset.cx; break;
		}

		if (pDC->RectVisible(pageRect))
		{
			CSize fontSize(0, Pixel2MM(pDC, 11));
			//pDC->DPtoLP(&fontSize);
			CFont font;
			font.CreateFont(fontSize.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));	
			CFont* pOldFont = pDC->SelectObject(&font);
			pDC->SetTextColor(BLACK);
			pDC->SetBkColor  (WHITE);
			pDC->SetBkMode   (OPAQUE);
			
			pDC->TextOut(nXPos, nYPos, strShinglingOffset);

			pDC->SelectObject(pOldFont);
			font.DeleteObject();

			if (fabs(fShinglingOffset) > 0.009f)
			{
				pDC->LPtoDP(pageRect);	
				pDC->LPtoDP(&szOffset);

				int	   nDirection = TOP;
				CPoint hotSpot	  = pageRect.CenterPoint();
				switch (m_nHeadPosition)
				{
				case TOP:	nDirection = (fShinglingOffset < 0.0f) ? LEFT   : RIGHT;  
							hotSpot	  += CPoint((nDirection == LEFT) ? -4 : 4, 12);					
							hotSpot.y -= szOffset.cy;									break;
				case BOTTOM:nDirection = (fShinglingOffset < 0.0f) ? RIGHT  : LEFT;	
							hotSpot	  += CPoint((nDirection == LEFT) ? -4 : 4, 12);					
							hotSpot.y += szOffset.cy;									break;
				case LEFT:	nDirection = (fShinglingOffset < 0.0f) ? BOTTOM : TOP;		
							hotSpot	  += CPoint(2, (nDirection == TOP) ? 10 : 18);					
							hotSpot.x -= szOffset.cx;									break;
				case RIGHT: nDirection = (fShinglingOffset < 0.0f) ? TOP	: BOTTOM;	
							hotSpot	  += CPoint(2, (nDirection == TOP) ? 10 : 18);					
							hotSpot.x += szOffset.cx;									break;
				}

				CPrintSheetView::DrawArrow(pDC, nDirection, hotSpot, 8, BLACK, YELLOW);
			}
		}
	}
}

void CLayoutObject::CreatePageIDFont(CDC* pDC, CFont* pFont, int nHeadPosition, BOOL bThumbnail, int nDefaultFontSize)
{
	int nOrientation;
	switch (nHeadPosition)
	{
	case TOP:	 nOrientation = 0;    break;
	case RIGHT:  nOrientation = 900;  break;
	case BOTTOM: nOrientation = 1800; break;
	case LEFT:	 nOrientation = 2700; break;
	default:	 nOrientation = 0;    break;
	}

	int nFontSize = (nDefaultFontSize > 0) ? nDefaultFontSize : CLayout::TransformFontSize(pDC, 18, (bThumbnail) ? 2 : 0, (bThumbnail) ? 0 : 250);
	pFont->CreateFont(nFontSize, 0, nOrientation, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));	
}

int CLayoutObject::RotateHeadPosition(int nHeadPosition, int nAngle)
{
	switch (nAngle)
	{
	case 0:		break;
	case 90:	switch (nHeadPosition)
				{
					case TOP:	 nHeadPosition = LEFT;		break;
					case BOTTOM: nHeadPosition = RIGHT;		break;
					case LEFT:	 nHeadPosition = BOTTOM;	break;
					case RIGHT:  nHeadPosition = TOP;		break;
				}
				break;
	case 180:	switch (nHeadPosition)
				{
					case TOP:	 nHeadPosition = BOTTOM;	break;
					case BOTTOM: nHeadPosition = TOP;		break;
					case LEFT:	 nHeadPosition = RIGHT;		break;
					case RIGHT:  nHeadPosition = LEFT;		break;
				}
				break;
	case 270:	switch (nHeadPosition)
				{
					case TOP:	 nHeadPosition = RIGHT;		break;
					case BOTTOM: nHeadPosition = LEFT;		break;
					case LEFT:	 nHeadPosition = TOP;		break;
					case RIGHT:  nHeadPosition = BOTTOM;	break;
				}
				break;
	default:	break;
	}

	return nHeadPosition;
}

void CLayoutObject::GetCurrentGrossCutBlock(CPrintSheet* pPrintSheet, float& fX, float& fY, float& fWidth, float& fHeight, BOOL bRelativToPaper)
{
	fX = fY = fWidth = fHeight = 0.0f;

	CLayoutSide* pLayoutSide = GetLayoutSide();
	if ( ! pLayoutSide)
		return;

	if (m_nLayerPageIndex < 0)
	{
		fX		= GetLeft()	  - pLayoutSide->m_fPaperLeft;
		fY		= GetBottom() - pLayoutSide->m_fPaperBottom;
		fWidth	= GetWidth();
		fHeight = GetHeight();
	}
	else
	{
		if ( ! pPrintSheet)
			return;

		float fXMin = FLT_MAX, fYMin = FLT_MAX, fXMax = -FLT_MAX, fYMax = -FLT_MAX;
		POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rLayoutObject = pLayoutSide->m_ObjectList.GetNext(pos);
			if (rLayoutObject.IsFlatProduct())
				continue;
			if (rLayoutObject.m_nComponentRefIndex != m_nComponentRefIndex)
				continue;

			float fLeft, fBottom, fRight, fTop;
			rLayoutObject.GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);

			fXMin = min(fXMin, fLeft);  fYMin = min(fYMin, fBottom);
			fXMax = max(fXMax, fRight); fYMax = max(fYMax, fTop);
		}

		if (bRelativToPaper)
		{
			fX = fXMin - pLayoutSide->m_fPaperLeft; 
			fY = fYMin - pLayoutSide->m_fPaperBottom;
		}
		else
		{
			fX = fXMin;
			fY = fYMin;
		}
		fWidth	= fXMax - fXMin; 
		fHeight = fYMax - fYMin;
	}
}

void CLayoutObject::GetCurrentPageCutSize(CPrintSheet* pPrintSheet, float& fWidth, float& fHeight)
{
	float fLeft, fBottom, fRight, fTop;
	GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
	fWidth  = fRight - fLeft;
	fHeight = fTop	 - fBottom;
}

float CLayoutObject::GetCurrentGrossMargin(CPrintSheet* pPrintSheet, int nDirection)
{
	float fLeft, fBottom, fRight, fTop;
	GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
	switch (nDirection)
	{
	case LEFT:		return GetLeft()   - fLeft;
	case BOTTOM:	return GetBottom() - fBottom;
	case RIGHT:		return fRight	   - GetRight();
	case TOP:		return fTop		   - GetTop();
	}
	return 0.0f;
}

void CLayoutObject::GetCurrentGrossBox(CPrintSheet* pPrintSheet, float& fLeft, float& fBottom, float& fRight, float& fTop)
{
	fLeft = GetLeft(); fBottom = GetBottom(); fRight = GetRight(); fTop = GetTop();
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || (m_nType != CLayoutObject::Page) )
		return;
	CPageTemplate* pPageTemplate = GetCurrentPageTemplate(pPrintSheet);
	if ( ! pPageTemplate)
		return;
	CBoundProductPart&	rProductPart = pPageTemplate->GetProductPart();
	CBoundProduct&		rProduct	 = rProductPart.GetBoundProduct();

	if ( ! IsFlatProduct())
	{

		CFoldSheet*		 pFoldSheet = pPageTemplate->GetFoldSheet();
		CFoldSheetLayer* pLayer		= (pPrintSheet) ? pPrintSheet->GetFoldSheetLayer(m_nComponentRefIndex) : NULL;
		if ( ! pLayer)
			return;
		float fOverFold = 0.0f; 
		if (m_nLayerPageIndex < pLayer->m_FrontSpineArray.GetSize())
		{
			switch (rProduct.m_bindingParams.m_nOverFoldSide)
			{
			case CBindingProcess::OverFoldNone:		break;
			case CBindingProcess::OverFoldFront:	fOverFold = (pLayer->m_FrontSpineArray[m_nLayerPageIndex] == LEFT)  ? rProduct.m_bindingParams.m_fOverFold : 0.0f;	break;
			case CBindingProcess::OverFoldBack:		fOverFold = (pLayer->m_FrontSpineArray[m_nLayerPageIndex] == RIGHT) ? rProduct.m_bindingParams.m_fOverFold : 0.0f;	break;
			}
		}

		switch (m_nHeadPosition)
		{
		case TOP:		fTop	= GetTop()	  + rProductPart.m_trimmingParams.m_fHeadTrim; 
						fBottom = GetBottom() - rProductPart.m_trimmingParams.m_fFootTrim; 
						fLeft   = GetLeft()	  - ( (m_nIDPosition == LEFT)  ? (rProductPart.m_trimmingParams.m_fSideTrim + fOverFold) : 0.0f); 
						fRight  = GetRight()  + ( (m_nIDPosition == RIGHT) ? (rProductPart.m_trimmingParams.m_fSideTrim + fOverFold) : 0.0f);  
						break;
		case BOTTOM:	fTop	= GetTop()	  + rProductPart.m_trimmingParams.m_fFootTrim; 
						fBottom = GetBottom() - rProductPart.m_trimmingParams.m_fHeadTrim; 
						fLeft   = GetLeft()	  - ( (m_nIDPosition == RIGHT) ? (rProductPart.m_trimmingParams.m_fSideTrim + fOverFold) : 0.0f); 
						fRight  = GetRight()  + ( (m_nIDPosition == LEFT)  ? (rProductPart.m_trimmingParams.m_fSideTrim + fOverFold) : 0.0f);  
						break;
		case LEFT:		fLeft	= GetLeft()	  - rProductPart.m_trimmingParams.m_fHeadTrim; 
						fRight  = GetRight()  + rProductPart.m_trimmingParams.m_fFootTrim; 
						fTop    = GetTop()	  + ( (m_nIDPosition == RIGHT) ? (rProductPart.m_trimmingParams.m_fSideTrim + fOverFold) : 0.0f); 
						fBottom = GetBottom() - ( (m_nIDPosition == LEFT)  ? (rProductPart.m_trimmingParams.m_fSideTrim + fOverFold) : 0.0f);  
						break;
		case RIGHT:		fLeft	= GetLeft()	  - rProductPart.m_trimmingParams.m_fFootTrim; 
						fRight  = GetRight()  + rProductPart.m_trimmingParams.m_fHeadTrim; 
						fTop    = GetTop()	  + ( (m_nIDPosition == LEFT)  ? (rProductPart.m_trimmingParams.m_fSideTrim + fOverFold) : 0.0f); 
						fBottom = GetBottom() - ( (m_nIDPosition == RIGHT) ? (rProductPart.m_trimmingParams.m_fSideTrim + fOverFold) : 0.0f);  
						break;
		}

		float fSpine = rProduct.m_bindingParams.GetSpine(rProductPart) / 2.0f;
		switch (m_nHeadPosition)
		{
		case TOP:		fLeft   -= (m_nIDPosition == RIGHT) ? fSpine : 0; 
						fRight  += (m_nIDPosition == LEFT)  ? fSpine : 0;  
						break;
		case BOTTOM:	fLeft   -= (m_nIDPosition == LEFT)  ? fSpine : 0; 
						fRight  += (m_nIDPosition == RIGHT) ? fSpine : 0;  
						break;
		case LEFT:		fTop    += (m_nIDPosition == LEFT)  ? fSpine : 0; 
						fBottom -= (m_nIDPosition == RIGHT) ? fSpine : 0;  
						break;
		case RIGHT:		fTop    += (m_nIDPosition == RIGHT) ? fSpine : 0; 
						fBottom -= (m_nIDPosition == LEFT)  ? fSpine : 0;  
						break;
		}
	}
	else
	{
		float fLeftTrim	 = 0.0f, fBottomTrim = 0.0f, fRightTrim = 0.0f, fTopTrim  = 0.0f;
		int  nSide = GetLayoutSideIndex();
		BOOL bTurned  = ( (nSide == FRONTSIDE) && m_bObjectTurned ) || ( (nSide == BACKSIDE) && ! m_bObjectTurned) ? TRUE : FALSE;
		CFlatProduct& rFlatProduct = GetCurrentFlatProduct(pPrintSheet);
		fLeftTrim	= (bTurned) ? rFlatProduct.m_fRightTrim : rFlatProduct.m_fLeftTrim;
		fBottomTrim = rFlatProduct.m_fBottomTrim;
		fRightTrim  = (bTurned) ? rFlatProduct.m_fLeftTrim  : rFlatProduct.m_fRightTrim;
		fTopTrim    = rFlatProduct.m_fTopTrim;

		switch (m_nHeadPosition)
		{
		case TOP:		fTop	= GetTop()	  + fTopTrim; 
						fBottom = GetBottom() - fBottomTrim; 
						fLeft   = GetLeft()	  - fLeftTrim; 
						fRight  = GetRight()  + fRightTrim;  
						break;
		case BOTTOM:	fTop	= GetTop()	  + fBottomTrim; 
						fBottom = GetBottom() - fTopTrim; 
						fLeft   = GetLeft()	  - fRightTrim; 
						fRight  = GetRight()  + fLeftTrim;  
						break;
		case LEFT:		fLeft	= GetLeft()	  - fTopTrim; 
						fRight  = GetRight()  + fBottomTrim; 
						fTop    = GetTop()	  + fRightTrim; 
						fBottom = GetBottom() - fLeftTrim;  
						break;
		case RIGHT:		fLeft	= GetLeft()	  - fBottomTrim; 
						fRight  = GetRight()  + fTopTrim; 
						fTop    = GetTop()	  + fLeftTrim; 
						fBottom = GetBottom() - fRightTrim;  
						break;
		}
	}
}

CFoldSheet*	CLayoutObject::GetCurrentFoldSheet(CPrintSheet* pPrintSheet)
{
	if (IsFlatProduct())
		return NULL;
	else
		return pPrintSheet->GetFoldSheet(m_nComponentRefIndex);
}

CBoundProduct& CLayoutObject::GetCurrentBoundProduct(CPrintSheet* pPrintSheet)
{
	static CBoundProduct nullBoundProduct;

	if (IsFlatProduct())
		return nullBoundProduct;
	else
	{
		CFoldSheet* pFoldSheet = pPrintSheet->GetFoldSheet(m_nComponentRefIndex);
		return (pFoldSheet) ? pFoldSheet->GetBoundProduct() : nullBoundProduct;
	}
}

CFlatProduct& CLayoutObject::GetCurrentFlatProduct(CPrintSheet* pPrintSheet)
{
	static CFlatProduct nullFlatProduct;

	if ( ! IsFlatProduct())
		return nullFlatProduct;
	else
		return pPrintSheet->GetFlatProduct(m_nComponentRefIndex);
}

void CLayoutObject::GetCutblockMatrix(int& nNX, int& nNY)
{
	CLayoutSide* pLayoutSide = GetLayoutSide();
	if ( ! pLayoutSide)
		return;

	CLayoutObjectList objectList;
	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = pLayoutSide->m_ObjectList.GetNext(pos);
		if (rLayoutObject.IsFlatProduct())
		{
			if (rLayoutObject.m_nComponentRefIndex != m_nComponentRefIndex)
				continue;
		}
		else
			continue;

		objectList.AddTail(rLayoutObject);
	}
	
	CLayoutObjectMatrix objectMatrix(objectList, CLayoutObjectMatrix::TakePages);
	nNY = objectMatrix.GetSize();
	nNX = (nNY > 0) ? objectMatrix[0].GetSize() : 0;
	if (nNX == 0)
		nNX = 1;
	if (nNY == 0)
		nNY = 1;
}

BOOL CLayoutObject::IsDynamicMark()
{
	if (m_nType != ControlMark)
		return FALSE;

	CPageTemplate* pTemplate = GetCurrentMarkTemplate();
	if ( ! pTemplate)
		return FALSE;

	CLayout* pLayout = GetLayout();
	CMark*	 pMark	 = (pLayout) ? pLayout->m_markList.FindMark(pTemplate->m_strPageID) : NULL;
	
	return (pMark) ? TRUE : FALSE;
}

BOOL CLayoutObject::IsAutoMarkDisabled(CPrintSheet* pPrintSheet, int nSide)
{
	if (m_nType != ControlMark)
		return FALSE;

	CPageTemplate* pTemplate = GetCurrentMarkTemplate();
	if ( ! pTemplate)
		return FALSE;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CLayout* pLayout = GetLayout();
	if ( ! pLayout || ! pPrintSheet)
		return FALSE;

	int nMarkIndex = pLayout->m_markList.FindMarkIndex(pTemplate->m_strPageID);
	if ( (nMarkIndex < 0) || (nMarkIndex >= pPrintSheet->m_markStates.GetSize()) )
		return FALSE;

	int nState = pPrintSheet->m_markStates[nMarkIndex];
	switch (nSide)
	{
	case FRONTSIDE: if (nState == FRONTSIDE)
						return FALSE;
					else
						if (nState == BOTHSIDES)
							return FALSE;
						else
							return TRUE;
					break;
	case BACKSIDE:	if (nState == BACKSIDE)
						return FALSE;
					else
						if (nState == BOTHSIDES)
							return FALSE;
						else
							return TRUE;
					break;
	case BOTHSIDES:	if (nState == 0)
						return TRUE;
					else
						return FALSE;
					break;
	default:		return FALSE;
	}
}

void CLayoutObject::RemoveMarkInstance()
{
	if (m_nType != ControlMark)
		return;

	CPageTemplate* pTemplate = GetCurrentMarkTemplate();
	if ( ! pTemplate)
		return;
	switch (pTemplate->GetMarkType())
	{
	case CMark::CropMark:		break; 
	case CMark::SectionMark:	break;
	case CMark::FoldMark:		break;

	case CMark::CustomMark:
	case CMark::TextMark:
	case CMark::BarcodeMark:	
	case CMark::DrawingMark:	{	
									CLayout*	 pLayout	 = GetLayout();
									CMark*		 pMark		 = (pLayout) ? pLayout->m_markList.FindMark(pTemplate->m_strPageID) : NULL;
									CLayoutSide* pLayoutSide = GetLayoutSide();
									if (pMark)
									{
										int			  nSide		   = GetLayoutSideIndex();
										CReflineMark* pReflineMark = (CReflineMark*)pMark;
										switch (pReflineMark->m_reflinePosParamsAnchor.m_nPlaceOn)
										{
										case CReflinePositionParams::PlaceOnFront:			pReflineMark->m_reflinePosParamsAnchor.m_nPlaceOn = -1;	
																							break;
										case CReflinePositionParams::PlaceOnBack:			pReflineMark->m_reflinePosParamsAnchor.m_nPlaceOn = -1;
																							break;
										case CReflinePositionParams::PlaceOnBothMirror:	
										case CReflinePositionParams::PlaceOnBothSamePos:	pReflineMark->m_reflinePosParamsAnchor.m_nPlaceOn = (nSide == FRONTSIDE) ?	CReflinePositionParams::PlaceOnBack : 
																																										CReflinePositionParams::PlaceOnFront;
																							break;
										}
										if (pReflineMark->m_reflinePosParamsAnchor.m_nPlaceOn == -1)
										{
											int	nIndex = (pLayout) ? pLayout->m_markList.GetIndex(pMark) : -1;
											if (nIndex >= 0)
												pLayout->m_markList.RemoveAt(nIndex);
										}
									}
									if (pLayoutSide)
										pLayoutSide->RemoveMarkObject(pTemplate->m_strPageID);
								}
								return;
	default:					return;
	}

	if ( ! GetLayoutSide())
		return;

	POSITION pos = GetLayoutSide()->m_ObjectList.FindIndex(m_markBuddyLink.m_nLinkedObjectIndex);
	if ( ! pos)
		return;
	CLayoutObject& rBuddy = GetLayoutSide()->m_ObjectList.GetAt(pos);
	CMarkInstanceDefs defs;
	defs.m_nAction		 = CMarkInstanceDefs::Disable;
	defs.m_nPosition	 = m_markBuddyLink.m_nLinkedObjectPosition;
	defs.m_nOrientation	 = m_markBuddyLink.m_nLinkedObjectOrientation;
	defs.m_strParentMark = pTemplate->m_strPageID;
	for (int i = 0; i < rBuddy.m_markInstanceDefs.GetSize(); i++)
	{
		if ( (rBuddy.m_markInstanceDefs[i].m_nPosition == defs.m_nPosition) && (rBuddy.m_markInstanceDefs[i].m_nOrientation == defs.m_nOrientation) && 
			 (rBuddy.m_markInstanceDefs[i].m_strParentMark == defs.m_strParentMark) )
		{
			rBuddy.m_markInstanceDefs[i].m_nAction = defs.m_nAction;
			return;
		}
	}

	rBuddy.m_markInstanceDefs.Add(defs);
}

int	CLayoutObject::GetSpineSide()
{
	switch (m_nHeadPosition)
	{
	case	TOP:	return (m_nIDPosition == LEFT) ? RIGHT  : LEFT;
	case	BOTTOM:	return (m_nIDPosition == LEFT) ? LEFT   : RIGHT;
	case	LEFT:	return (m_nIDPosition == LEFT) ? TOP    : BOTTOM;
	case	RIGHT:	return (m_nIDPosition == LEFT) ? BOTTOM : TOP;
	}
	return 0;
}

int	CLayoutObject::GetDisplayColorIndex(CPrintSheet* pPrintSheet)
{
	if ( ! IsFlatProduct())
		return m_nDisplayColorIndex;

	return GetCurrentFlatProduct(pPrintSheet).GetIndex();
}

int	CLayoutObject::GetCurrentAttribute(CPrintSheet* pPrintSheet)
{
	if ( ! IsFlatProduct())
		return m_nAttribute;
	else
		if ( (m_nComponentRefIndex >= 0) && (m_nComponentRefIndex < pPrintSheet->m_flatProductRefs.GetSize()) )
			return pPrintSheet->m_flatProductRefs[m_nComponentRefIndex].m_nAttribute;
		else
			return m_nAttribute;
}

void CLayoutObject::FindLayoutObjectCluster(CObArray& rObjectPtrList, CPrintSheet* pPrintSheet, BOOL bNormalize)
{
	int i;
	CLayout* pLayout = GetLayout();
	pLayout->m_FrontSide.ResetPublicFlags();
	pLayout->m_BackSide.ResetPublicFlags();

	rObjectPtrList.Add(this);
	m_nPublicFlag = TRUE;	

	float fStepX = 0.0f;
	float fStepY = 0.0f;

	for (i = 0; i < m_LeftNeighbours.GetSize(); i++)
		FindClusterNeighbours(m_LeftNeighbours.GetAt(i),	LEFT,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
	for (i = 0; i < m_RightNeighbours.GetSize(); i++)
		FindClusterNeighbours(m_RightNeighbours.GetAt(i),	RIGHT,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
	for (i = 0; i < m_LowerNeighbours.GetSize(); i++)
		FindClusterNeighbours(m_LowerNeighbours.GetAt(i),	BOTTOM,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
	for (i = 0; i < m_UpperNeighbours.GetSize(); i++)
		FindClusterNeighbours(m_UpperNeighbours.GetAt(i),	TOP,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
}

void CLayoutObject::FindClusterNeighbours(CLayoutObject *pObject, int nDirection, float& fStepX, float& fStepY, CObArray& rObjectPtrList, CPrintSheet* pPrintSheet, BOOL bNormalize)
{
	int i;

	if (pObject->m_nPublicFlag)	
		return;
	if ( ! IsFlatProduct())
		return;
	int nFlatProductIndex = (pPrintSheet) ? pPrintSheet->GetFlatProductIndex(m_nComponentRefIndex) : -1;
	if (pPrintSheet)
		if (nFlatProductIndex != pPrintSheet->GetFlatProductIndex(pObject->m_nComponentRefIndex))
			return;

	switch (nDirection)
	{
	case LEFT : if (fabs(m_fObjectCenterY - pObject->m_fObjectCenterY) > 0.001f)
					break;
				if (fabs(GetHeight() - pObject->GetHeight()) > 0.001f)
					break;
				if (bNormalize)
					if (fStepX > 0.0f)
						if (fabs(fStepX - fabs(m_fObjectCenterX - pObject->m_fObjectCenterX)) > 0.001f)		// step differs
							break;

				fStepX = fabs(m_fObjectCenterX - pObject->m_fObjectCenterX);
				rObjectPtrList.Add(pObject);
				pObject->m_nPublicFlag = TRUE;	
				for (i = 0; i < pObject->m_LeftNeighbours.GetSize(); i++)
					pObject->FindClusterNeighbours(pObject->m_LeftNeighbours.GetAt(i),	LEFT,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				for (i = 0; i < pObject->m_LowerNeighbours.GetSize(); i++)																   
					pObject->FindClusterNeighbours(pObject->m_LowerNeighbours.GetAt(i), BOTTOM,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				for (i = 0; i < pObject->m_UpperNeighbours.GetSize(); i++)																   
					pObject->FindClusterNeighbours(pObject->m_UpperNeighbours.GetAt(i), TOP,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				break;

	case RIGHT: if (fabs(m_fObjectCenterY - pObject->m_fObjectCenterY) > 0.001f)
					break;
				if (fabs(GetHeight() - pObject->GetHeight()) > 0.001f)
					break;
				if (bNormalize)
					if (fStepX > 0.0f)
						if (fabs(fStepX - fabs(m_fObjectCenterX - pObject->m_fObjectCenterX)) > 0.001f)		// step differs
							break;

				fStepX = fabs(m_fObjectCenterX - pObject->m_fObjectCenterX);
				rObjectPtrList.Add(pObject);
				pObject->m_nPublicFlag = TRUE;	
				for (i = 0; i < pObject->m_RightNeighbours.GetSize(); i++)
					pObject->FindClusterNeighbours(pObject->m_RightNeighbours.GetAt(i), RIGHT,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				for (i = 0; i < pObject->m_LowerNeighbours.GetSize(); i++)									  							   
					pObject->FindClusterNeighbours(pObject->m_LowerNeighbours.GetAt(i), BOTTOM,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				for (i = 0; i < pObject->m_UpperNeighbours.GetSize(); i++)									  							   
					pObject->FindClusterNeighbours(pObject->m_UpperNeighbours.GetAt(i), TOP,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				break;

	case BOTTOM:if (fabs(m_fObjectCenterX - pObject->m_fObjectCenterX) > 0.001f)
					break;
				if (fabs(GetWidth() - pObject->GetWidth()) > 0.001f)
					break;
				if (bNormalize)
					if (fStepY > 0.0f)
						if (fabs(fStepY - fabs(m_fObjectCenterY - pObject->m_fObjectCenterY)) > 0.001f)		// step differs
							break;

				fStepY = fabs(m_fObjectCenterY - pObject->m_fObjectCenterY);
				rObjectPtrList.Add(pObject);
				pObject->m_nPublicFlag = TRUE;	
				for (i = 0; i < pObject->m_LeftNeighbours.GetSize(); i++)
					pObject->FindClusterNeighbours(pObject->m_LeftNeighbours.GetAt(i),	LEFT,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				for (i = 0; i < pObject->m_RightNeighbours.GetSize(); i++)									  							   
					pObject->FindClusterNeighbours(pObject->m_RightNeighbours.GetAt(i), RIGHT,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				for (i = 0; i < pObject->m_LowerNeighbours.GetSize(); i++)									  							   
					pObject->FindClusterNeighbours(pObject->m_LowerNeighbours.GetAt(i), BOTTOM,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				break;

	case TOP  : if (fabs(m_fObjectCenterX - pObject->m_fObjectCenterX) > 0.001f)
					break;
				if (fabs(GetWidth() - pObject->GetWidth()) > 0.001f)
					break;
				if (bNormalize)
					if (fStepY > 0.0f)
						if (fabs(fStepY - fabs(m_fObjectCenterY - pObject->m_fObjectCenterY)) > 0.001f)		// step differs
							break;

				fStepY = fabs(m_fObjectCenterY - pObject->m_fObjectCenterY);
				rObjectPtrList.Add(pObject);
				pObject->m_nPublicFlag = TRUE;	
				for (i = 0; i < pObject->m_LeftNeighbours.GetSize(); i++)
					pObject->FindClusterNeighbours(pObject->m_LeftNeighbours.GetAt(i),	LEFT,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				for (i = 0; i < pObject->m_RightNeighbours.GetSize(); i++)									  							   
					pObject->FindClusterNeighbours(pObject->m_RightNeighbours.GetAt(i), RIGHT,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				for (i = 0; i < pObject->m_UpperNeighbours.GetSize(); i++)									  							   
					pObject->FindClusterNeighbours(pObject->m_UpperNeighbours.GetAt(i), TOP,	fStepX, fStepY, rObjectPtrList, pPrintSheet, bNormalize);
				break;
	}
}


// helper function for layout object elements
template <> void AFXAPI SerializeElements <CLayoutObject> (CArchive& ar, CLayoutObject* pLayoutObject, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pLayoutObject->Serialize(ar);
}

void CLayoutObject::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CLayoutObject));

	if (ar.IsStoring())
	{
		ar << m_nType;			
		ar << m_fObjectLeft;
		ar << m_fObjectBottom;
		ar << m_fObjectRight;
		ar << m_fObjectTop;
		ar << m_fObjectWidth;
		ar << m_fObjectHeight;
		ar << m_strObjectName;
		ar << m_strFormatName;
		ar << m_nObjectOrientation;
		ar << m_fLeftBorder;
		ar << m_fRightBorder;
		ar << m_fLowerBorder;
		ar << m_fUpperBorder;
		ar << m_nLockBorder;
		ar << m_nIDPosition;	
		ar << m_nHeadPosition;	
		ar << m_nBlockNumber;
		ar << m_nAttribute;		
		ar << m_strNotes;
		ar << m_nDisplayColorIndex;
		ar << m_nComponentRefIndex;
		ar << m_nLayerPageIndex;
		ar << m_bObjectTurned;
		ar << m_nMarkTemplateIndex;
		ar << m_fDefaultContentOffsetX;
		ar << m_fDefaultContentOffsetY;
		m_reflinePosParams.Serialize(ar);
		m_markInstanceDefs.Serialize(ar);
		m_markBuddyLink.Serialize(ar);
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		BYTE nTemp;
		switch (nVersion)
		{
		case 1:
			ar >> m_nType;			
			ar >> m_fObjectLeft;  
			ar >> m_fObjectBottom;
			ar >> m_fObjectRight; 
			ar >> m_fObjectTop;   
			ar >> m_fObjectWidth; 
			ar >> m_fObjectHeight;
			ar >> m_strFormatName;
			ar >> m_nObjectOrientation;
			ar >> m_fLeftBorder;
			ar >> m_fRightBorder;
			ar >> m_fLowerBorder;
			ar >> m_fUpperBorder;
			ar >> m_nIDPosition;	
			ar >> m_nHeadPosition;	
			ar >> m_nBlockNumber;
			ar >> m_nAttribute;		
			ar >> m_strNotes;
			ar >> m_nDisplayColorIndex;
			ar >> m_nComponentRefIndex;
			ar >> m_nLayerPageIndex;
			ar >> m_nMarkTemplateIndex;
			ar >> nTemp;	// former pLayoutObject->m_nBottomCutNr;
			ar >> nTemp;	// former pLayoutObject->m_nLeftCutNr;
			ar >> nTemp;	// former pLayoutObject->m_nTopCutNr;
			ar >> nTemp;	// former pLayoutObject->m_nRightCutNr;
			m_fDefaultContentOffsetX = 0.0f;
			m_fDefaultContentOffsetY = 0.0f;
			m_reflinePosParams.SetUndefined();
			m_bObjectTurned = FALSE;
			break;
		case 2:
			ar >> m_nType;			
			ar >> m_fObjectLeft;  
			ar >> m_fObjectBottom;
			ar >> m_fObjectRight; 
			ar >> m_fObjectTop;   
			ar >> m_fObjectWidth; 
			ar >> m_fObjectHeight;
			ar >> m_strFormatName;
			ar >> m_nObjectOrientation;
			ar >> m_fLeftBorder;
			ar >> m_fRightBorder;
			ar >> m_fLowerBorder;
			ar >> m_fUpperBorder;
			ar >> m_nIDPosition;	
			ar >> m_nHeadPosition;	
			ar >> m_nBlockNumber;
			ar >> m_nAttribute;		
			ar >> m_strNotes;
			ar >> m_nDisplayColorIndex;
			ar >> m_nComponentRefIndex;
			ar >> m_nLayerPageIndex;
			ar >> m_nMarkTemplateIndex;
			ar >> m_fDefaultContentOffsetX;
			ar >> m_fDefaultContentOffsetY;
			m_reflinePosParams.SetUndefined();
			m_bObjectTurned = FALSE;
			break;
		case 3:
			ar >> m_nType;			
			ar >> m_fObjectLeft;  
			ar >> m_fObjectBottom;
			ar >> m_fObjectRight; 
			ar >> m_fObjectTop;   
			ar >> m_fObjectWidth; 
			ar >> m_fObjectHeight;
			ar >> m_strFormatName;
			ar >> m_nObjectOrientation;
			ar >> m_fLeftBorder;
			ar >> m_fRightBorder;
			ar >> m_fLowerBorder;
			ar >> m_fUpperBorder;
			ar >> m_nIDPosition;	
			ar >> m_nHeadPosition;	
			ar >> m_nBlockNumber;
			ar >> m_nAttribute;		
			ar >> m_strNotes;
			ar >> m_nDisplayColorIndex;
			ar >> m_nComponentRefIndex;
			ar >> m_nLayerPageIndex;
			ar >> m_nMarkTemplateIndex;
			ar >> m_fDefaultContentOffsetX;
			ar >> m_fDefaultContentOffsetY;
			m_nLockBorder = 0;
			m_reflinePosParams.Serialize(ar);
			m_bObjectTurned = FALSE;
			break;
		case 4:
			ar >> m_nType;			
			ar >> m_fObjectLeft;  
			ar >> m_fObjectBottom;
			ar >> m_fObjectRight; 
			ar >> m_fObjectTop;   
			ar >> m_fObjectWidth; 
			ar >> m_fObjectHeight;
			ar >> m_strFormatName;
			ar >> m_nObjectOrientation;
			ar >> m_fLeftBorder;
			ar >> m_fRightBorder;
			ar >> m_fLowerBorder;
			ar >> m_fUpperBorder;
			ar >> m_nLockBorder;
			ar >> m_nIDPosition;	
			ar >> m_nHeadPosition;	
			ar >> m_nBlockNumber;
			ar >> m_nAttribute;		
			ar >> m_strNotes;
			ar >> m_nDisplayColorIndex;
			ar >> m_nComponentRefIndex;
			ar >> m_nLayerPageIndex;
			ar >> m_nMarkTemplateIndex;
			ar >> m_fDefaultContentOffsetX;
			ar >> m_fDefaultContentOffsetY;
			m_reflinePosParams.Serialize(ar);
			m_bObjectTurned = FALSE;
			break;
		case 5:
			ar >> m_nType;			
			ar >> m_fObjectLeft;  
			ar >> m_fObjectBottom;
			ar >> m_fObjectRight; 
			ar >> m_fObjectTop;   
			ar >> m_fObjectWidth; 
			ar >> m_fObjectHeight;
			ar >> m_strObjectName;
			ar >> m_strFormatName;
			ar >> m_nObjectOrientation;
			ar >> m_fLeftBorder;
			ar >> m_fRightBorder;
			ar >> m_fLowerBorder;
			ar >> m_fUpperBorder;
			ar >> m_nLockBorder;
			ar >> m_nIDPosition;	
			ar >> m_nHeadPosition;	
			ar >> m_nBlockNumber;
			ar >> m_nAttribute;		
			ar >> m_strNotes;
			ar >> m_nDisplayColorIndex;
			ar >> m_nComponentRefIndex;
			ar >> m_nLayerPageIndex;
			ar >> m_nMarkTemplateIndex;
			ar >> m_fDefaultContentOffsetX;
			ar >> m_fDefaultContentOffsetY;
			m_reflinePosParams.Serialize(ar);
			m_bObjectTurned = FALSE;
			break;
		case 6:
			ar >> m_nType;			
			ar >> m_fObjectLeft;  
			ar >> m_fObjectBottom;
			ar >> m_fObjectRight; 
			ar >> m_fObjectTop;   
			ar >> m_fObjectWidth; 
			ar >> m_fObjectHeight;
			ar >> m_strObjectName;
			ar >> m_strFormatName;
			ar >> m_nObjectOrientation;
			ar >> m_fLeftBorder;
			ar >> m_fRightBorder;
			ar >> m_fLowerBorder;
			ar >> m_fUpperBorder;
			ar >> m_nLockBorder;
			ar >> m_nIDPosition;	
			ar >> m_nHeadPosition;	
			ar >> m_nBlockNumber;
			ar >> m_nAttribute;		
			ar >> m_strNotes;
			ar >> m_nDisplayColorIndex;
			ar >> m_nComponentRefIndex;
			ar >> m_nLayerPageIndex;
			ar >> m_nMarkTemplateIndex;
			ar >> m_fDefaultContentOffsetX;
			ar >> m_fDefaultContentOffsetY;
			m_reflinePosParams.Serialize(ar);
			m_markInstanceDefs.Serialize(ar);
			m_markBuddyLink.Serialize(ar);
			m_bObjectTurned = FALSE;
			break;
		case 7:
			ar >> m_nType;			
			ar >> m_fObjectLeft;  
			ar >> m_fObjectBottom;
			ar >> m_fObjectRight; 
			ar >> m_fObjectTop;   
			ar >> m_fObjectWidth; 
			ar >> m_fObjectHeight;
			ar >> m_strObjectName;
			ar >> m_strFormatName;
			ar >> m_nObjectOrientation;
			ar >> m_fLeftBorder;
			ar >> m_fRightBorder;
			ar >> m_fLowerBorder;
			ar >> m_fUpperBorder;
			ar >> m_nLockBorder;
			ar >> m_nIDPosition;	
			ar >> m_nHeadPosition;	
			ar >> m_nBlockNumber;
			ar >> m_nAttribute;		
			ar >> m_strNotes;
			ar >> m_nDisplayColorIndex;
			ar >> m_nComponentRefIndex;
			ar >> m_nLayerPageIndex;
			ar >> m_bObjectTurned;
			ar >> m_nMarkTemplateIndex;
			ar >> m_fDefaultContentOffsetX;
			ar >> m_fDefaultContentOffsetY;
			m_reflinePosParams.Serialize(ar);
			m_markInstanceDefs.Serialize(ar);
			m_markBuddyLink.Serialize(ar);
			break;
		}
	}
}



/////////////////////////////////////////////////////////////////////////////
// CObjectNeighbours

CObjectNeighbours::CObjectNeighbours()
{
}

int CObjectNeighbours::GetSize(BOOL bDontAnalyze) const
{
	if ( ! bDontAnalyze)
	{
		if (m_pLayoutObject->GetLayoutSide()->IsInvalid())
		{
			m_pLayoutObject->GetLayoutSide()->Validate();	// Validate in order to prevent for recursive calls
			m_pLayoutObject->GetLayoutSide()->Analyse();
		}
	}

	return CArray <CLayoutObject*, CLayoutObject*>::GetSize();
}

CLayoutObject* CObjectNeighbours::GetAt(int nIndex) const
{
	if (m_pLayoutObject->GetLayoutSide()->IsInvalid())
	{
		m_pLayoutObject->GetLayoutSide()->Validate();	// Validate in order to prevent for recursive calls
		m_pLayoutObject->GetLayoutSide()->Analyse();
	}

	return CArray <CLayoutObject*, CLayoutObject*>::GetAt(nIndex);
}

CLayoutObject*& CObjectNeighbours::ElementAt(int nIndex)
{ 
	if (m_pLayoutObject->GetLayoutSide()->IsInvalid())
	{
		m_pLayoutObject->GetLayoutSide()->Validate();	// Validate in order to prevent for recursive calls
		m_pLayoutObject->GetLayoutSide()->Analyse();
	}

	return CArray <CLayoutObject*, CLayoutObject*>::ElementAt(nIndex); 
}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// Analysation of layout object neighbourhoods and borders

CArray <CLayoutObject*, CLayoutObject*> SortLeft;
CArray <CLayoutObject*, CLayoutObject*> SortBottom;
CArray <CLayoutObject*, CLayoutObject*> SortRight;
CArray <CLayoutObject*, CLayoutObject*> SortTop;

class CLocList : public CArray <int, int> 
{
//Override:
public:
	int& ElementAt(int nIndex);
	inline int& operator[](int nIndex)
	{ 
		return ElementAt(nIndex); 
	}
};

int& CLocList::ElementAt(int nIndex)	// We get assertions with invalid indices but were not able to find out why.
{										// So for safety reasons we implement this override.
	if (nIndex >= 0 && nIndex < m_nSize)
		return m_pData[nIndex]; 
	else
		return m_pData[0]; 
}

CLocList LocLeft;
CLocList LocBottom;
CLocList LocRight;
CLocList LocTop;



////////////////////////////////////////////////////////////////////////////////////////////////////
// CLayoutSide member functions

void CLayoutSide::Analyse(BOOL bAnalyseBorders)
{
	if (m_ObjectList.IsEmpty())
		return;

	SortLeft.RemoveAll(); SortBottom.RemoveAll(); SortRight.RemoveAll(); SortTop.RemoveAll();
								
	short nIndex = 0;
	int   nSize  = 0;
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);

		if (rObject.m_bHidden)
			continue;
		if (rObject.m_nType == CLayoutObject::ControlMark)
		{
			rObject.m_nIndex = -1;
			continue;
		}

		rObject.m_nAnalyseFlag	 = FALSE;
		rObject.m_nIndex		 = nIndex++;	// nIndex for identification by loc lists
		rObject.m_fObjectCenterX = rObject.GetLeft()   + (rObject.GetRight() - rObject.GetLeft())/2.0f;
		rObject.m_fObjectCenterY = rObject.GetBottom() + (rObject.GetTop()   - rObject.GetBottom())/2.0f;

		SortLeft.Add(&rObject);
		SortBottom.Add(&rObject);
		SortRight.Add(&rObject);
		SortTop.Add(&rObject);

		nSize++;
	}
/*
	for (int i = 0; i < SortLeft.GetSize(); i++)
		TRACE("%d ", SortLeft[i]->m_nIndex);
	TRACE("\n");
*/

	qsort((void*)SortLeft.GetData(),   nSize, sizeof(CLayoutObject*), CLayoutObject::CompareLeft);
	qsort((void*)SortBottom.GetData(), nSize, sizeof(CLayoutObject*), CLayoutObject::CompareBottom);
	qsort((void*)SortRight.GetData(),  nSize, sizeof(CLayoutObject*), CLayoutObject::CompareRight);
	qsort((void*)SortTop.GetData(),	   nSize, sizeof(CLayoutObject*), CLayoutObject::CompareTop);
/*
	for (i = 0; i < SortLeft.GetSize(); i++)
		TRACE("%d ", SortLeft[i]->m_nIndex);
	TRACE("\n");
*/
	LocLeft.SetSize(nSize);  LocBottom.SetSize(nSize);  LocRight.SetSize(nSize);  LocTop.SetSize(nSize);

    for (nIndex = 0; nIndex < nSize; nIndex++)	
	{
		LocLeft[SortLeft[nIndex]->m_nIndex]		= nIndex;
    	LocBottom[SortBottom[nIndex]->m_nIndex] = nIndex;
    	LocRight[SortRight[nIndex]->m_nIndex]	= nIndex;
    	LocTop[SortTop[nIndex]->m_nIndex]		= nIndex;
	}

	pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);

		if (rObject.m_bHidden)
			continue;
		if (rObject.m_nType == CLayoutObject::ControlMark)
		{
			rObject.m_nIndex = -1;
			continue;
		}

		rObject.FindLeftNeighbours();
		rObject.FindLowerNeighbours();
		rObject.FindRightNeighbours();
		rObject.FindUpperNeighbours();
	}

	ResetAnalyseFlags();

	if (bAnalyseBorders)
	{
		pos = m_ObjectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = m_ObjectList.GetNext(pos);

			if (rObject.m_bHidden)
				continue;
			if (rObject.m_nType == CLayoutObject::ControlMark)
				continue;

			rObject.CalculateBorder(LEFT);
			rObject.CalculateBorder(BOTTOM);
			rObject.CalculateBorder(RIGHT);
			rObject.CalculateBorder(TOP);
		}
	}

	ResetAnalyseFlags();

	SortLeft.RemoveAll(); SortBottom.RemoveAll(); SortRight.RemoveAll(); SortTop.RemoveAll();
	LocLeft.RemoveAll();  LocBottom.RemoveAll();  LocRight.RemoveAll();  LocTop.RemoveAll();

	if (m_nLayoutSide == FRONTSIDE)
	{
		CLayout*	 pLayout	 = GetLayout();
		CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
		while (pPrintSheet)
		{
			AnalyseObjectGroups(pPrintSheet);
			if (pPrintSheet->m_flatProductRefs.GetSize() <= 0)
				break;
			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}
	}
	//AnalyseObjectGroups();

	ResetAnalyseFlags();

	FindCutBlocks();
	AnalyseCutblocks();

	m_bInvalid = FALSE;	// Declare layout side as valid

	return;
}

void CLayoutSide::FindCutBlocks()
{
	m_cutBlocks.RemoveAll();

	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return;

	for (int nComponentRefIndex = 0; nComponentRefIndex < pLayout->m_FoldSheetLayerRotation.GetSize(); nComponentRefIndex++)
	{
		float fLeft, fTop, fRight, fBottom, fWidth, fHeight;
		fLeft = fTop = fRight = fBottom = fWidth = fHeight = 0.0f;
		CalcBoundingRectObjects(&fLeft, &fBottom, &fRight, &fTop, &fWidth, &fHeight, FALSE, nComponentRefIndex, FALSE, NULL);
		int nHeadPosition = TOP;
		switch (pLayout->m_FoldSheetLayerRotation[nComponentRefIndex])
		{
		case 0:			nHeadPosition = TOP;	break;
		case 90:		nHeadPosition = LEFT;	break;
		case 180:		nHeadPosition = BOTTOM;	break;
		case 270:		nHeadPosition = RIGHT;	break;
		}
		CLayoutObject cutBlock;
		cutBlock.CreateCutBlockFoldSheet(fLeft, fBottom, fWidth, fHeight, nComponentRefIndex, nHeadPosition);

		m_cutBlocks.AddTail(cutBlock);
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
	{
		Validate();
		return;
	}

	//for (int nComponentRefIndex = 0; nComponentRefIndex < pDoc->m_flatProducts.GetSize(); nComponentRefIndex++)
	//{
	//	float fLeft, fTop, fRight, fBottom, fWidth, fHeight;
	//	fLeft = fTop = fRight = fBottom = fWidth = fHeight = 0.0f;
	//	if ( ! CalcBoundingRectObjects(&fLeft, &fBottom, &fRight, &fTop, &fWidth, &fHeight, FALSE, nComponentRefIndex, FALSE))
	//		continue;

	//	CLayoutObject cutBlock;
	//	cutBlock.CreateCutBlockLabel(fLeft, fBottom, fWidth, fHeight, nComponentRefIndex, TOP, 0);
	//	m_cutBlocks.AddTail(cutBlock);
	//}

	Validate();	// in m_cutBlocks.AddTail(cutBlock) layout side has been invalidated. So validate here in order to prevent from recursiv calls from AnalyseCutblocks()
}

void CLayoutSide::AnalyseCutblocks()
{
	BOOL bAnalyseBorders = TRUE;

	SortLeft.RemoveAll(); SortBottom.RemoveAll(); SortRight.RemoveAll(); SortTop.RemoveAll();
								
	short nIndex = 0;
	int   nSize  = 0;
	POSITION pos = m_cutBlocks.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_cutBlocks.GetNext(pos);

		rObject.m_nAnalyseFlag	 = FALSE;
		rObject.m_nIndex		 = nIndex++;	// nIndex for identification by loc lists
		rObject.m_fObjectCenterX = rObject.GetLeft()   + (rObject.GetRight() - rObject.GetLeft())/2.0f;
		rObject.m_fObjectCenterY = rObject.GetBottom() + (rObject.GetTop()   - rObject.GetBottom())/2.0f;

		SortLeft.Add(&rObject);
		SortBottom.Add(&rObject);
		SortRight.Add(&rObject);
		SortTop.Add(&rObject);

		nSize++;
	}

	qsort((void*)SortLeft.GetData(),   nSize, sizeof(CLayoutObject*), CLayoutObject::CompareLeft);
	qsort((void*)SortBottom.GetData(), nSize, sizeof(CLayoutObject*), CLayoutObject::CompareBottom);
	qsort((void*)SortRight.GetData(),  nSize, sizeof(CLayoutObject*), CLayoutObject::CompareRight);
	qsort((void*)SortTop.GetData(),	   nSize, sizeof(CLayoutObject*), CLayoutObject::CompareTop);

	LocLeft.SetSize(nSize);  LocBottom.SetSize(nSize);  LocRight.SetSize(nSize);  LocTop.SetSize(nSize);

    for (nIndex = 0; nIndex < nSize; nIndex++)	
	{
		LocLeft[SortLeft[nIndex]->m_nIndex]		= nIndex;
    	LocBottom[SortBottom[nIndex]->m_nIndex] = nIndex;
    	LocRight[SortRight[nIndex]->m_nIndex]	= nIndex;
    	LocTop[SortTop[nIndex]->m_nIndex]		= nIndex;
	}

	pos = m_cutBlocks.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_cutBlocks.GetNext(pos);

		rObject.FindLeftNeighbours();
		rObject.FindLowerNeighbours();
		rObject.FindRightNeighbours();
		rObject.FindUpperNeighbours();
	}

	ResetAnalyseFlags();

	if (bAnalyseBorders)
	{
		pos = m_cutBlocks.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = m_cutBlocks.GetNext(pos);

			rObject.CalculateBorder(LEFT);
			rObject.CalculateBorder(BOTTOM);
			rObject.CalculateBorder(RIGHT);
			rObject.CalculateBorder(TOP);
		}
	}

	ResetAnalyseFlags();

	SortLeft.RemoveAll(); SortBottom.RemoveAll(); SortRight.RemoveAll(); SortTop.RemoveAll();
	LocLeft.RemoveAll();  LocBottom.RemoveAll();  LocRight.RemoveAll();  LocTop.RemoveAll();
}

void CLayoutSide::FindFreeAreas(CPrintSheet* pPrintSheet, float fMinWidth, float fMinHeight)
{
	m_freeAreas.RemoveAll();

	CPressDevice* pPressDevice = GetPressDevice();
	float fGripper = (pPressDevice) ? ((pPressDevice->m_nPressType == CPressDevice::SheetPress) ? pPressDevice->m_fGripper : 0.0f) : 0.0f;

	CRect rcArea((long)(m_fPaperLeft * 1000), (long)((m_fPaperBottom + fGripper) * 1000), (long)(m_fPaperRight * 1000), (long)(m_fPaperTop * 1000) );
	m_freeAreas.Add(rcArea);

	CFreeAreas tempAreas;

	float fGrossBoxLeft, fGrossBoxBottom, fGrossBoxRight, fGrossBoxTop;
	CRect rcInter, rcUnion, rcNewArea;
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::ControlMark)
			continue;
		if (rLayoutObject.m_bHidden)
			continue;
		if (rLayoutObject.m_nStatus & CLayoutObject::Disabled)
			continue;

		rLayoutObject.GetCurrentGrossBox(pPrintSheet, fGrossBoxLeft, fGrossBoxBottom, fGrossBoxRight, fGrossBoxTop);
		CRect rcObject((long)(fGrossBoxLeft * 1000), (long)(fGrossBoxBottom * 1000), (long)(fGrossBoxRight * 1000) , (long)(fGrossBoxTop * 1000));

		tempAreas.RemoveAll();
		for (int i = 0; i < m_freeAreas.GetSize(); i++)
		{
			CRect& rcFreeArea = m_freeAreas[i];
			if (rcInter.IntersectRect(rcFreeArea, rcObject))
			{
				if (rcFreeArea.left < rcInter.left)
				{
					rcNewArea = rcFreeArea; rcNewArea.right = rcInter.left; tempAreas.Add(rcNewArea);
				}
				if (rcFreeArea.top < rcInter.top)
				{
					rcNewArea = rcFreeArea; rcNewArea.bottom = rcInter.top; tempAreas.Add(rcNewArea);
				}
				if (rcFreeArea.right > rcInter.right)
				{
					rcNewArea = rcFreeArea; rcNewArea.left = rcInter.right; tempAreas.Add(rcNewArea);
				}
				if (rcFreeArea.bottom > rcInter.bottom)
				{
					rcNewArea = rcFreeArea; rcNewArea.top = rcInter.bottom; tempAreas.Add(rcNewArea);
				}
			}
			else
				 tempAreas.Add(rcFreeArea);
		}

		UnifyFreeAraes(tempAreas);

		m_freeAreas.RemoveAll();
		m_freeAreas.Append(tempAreas);
	}

	UnifyFreeAraes(m_freeAreas);

	if ( (fMinWidth > 0.0f) && (fMinHeight > 0.0f) )
	{
		long nMinWidth  = (long)(fMinWidth  * 1000);
		long nMinHeight = (long)(fMinHeight * 1000);
		for (int i = 0; i < m_freeAreas.GetSize(); i++)
		{
			CRect& rcFreeArea = m_freeAreas[i];
			if ( (rcFreeArea.Width() < nMinWidth) || (rcFreeArea.Height() < nMinHeight) )
			{
				m_freeAreas.RemoveAt(i);
				i--;
			}
		}
	}

	//// find overlapping areas and eliminate
	//for (int i = 0; i < m_freeAreas.GetSize(); i++)
	//{
	//	CRect& rcFreeArea1 = m_freeAreas[i];
	//	for (int j = i + 1; j < m_freeAreas.GetSize(); j++)
	//	{
	//		CRect& rcFreeArea2 = m_freeAreas[j];
	//		if (rcInter.IntersectRect(rcFreeArea1, rcFreeArea2))
	//		{
	//			rcFreeArea1.SubtractRect(rcFreeArea1, rcFreeArea2);
	//		}
	//	}
	//}
}

void CLayoutSide::UnifyFreeAraes(CFreeAreas& rFreeAreas)
{
	CFreeAreas tempAreas;
	CRect	   rcInter, rcUnion;

	for (int i = 0; i < rFreeAreas.GetSize(); i++)
	{
		tempAreas.RemoveAll();
		BOOL bUnified = FALSE;
		CRect& rcFreeArea1 = rFreeAreas[i];
		for (int j = i + 1; j < rFreeAreas.GetSize(); j++)
		{
			CRect& rcFreeArea2 = rFreeAreas[j];
			if ( (abs(rcFreeArea1.left - rcFreeArea2.left) < 2) &&  (abs(rcFreeArea1.right - rcFreeArea2.right) < 2) )
				if (rcInter.IntersectRect(rcFreeArea1, rcFreeArea2))
				{
					rcUnion.UnionRect(rcFreeArea1, rcFreeArea2);
					tempAreas.Add(rcUnion);
					bUnified = TRUE; 
					rFreeAreas.RemoveAt(j--); 
					continue;
				}
			if ( (abs(rcFreeArea1.bottom - rcFreeArea2.bottom) < 2) &&  (abs(rcFreeArea1.top - rcFreeArea2.top) < 2) )
				if (rcInter.IntersectRect(rcFreeArea1, rcFreeArea2))
				{
					rcUnion.UnionRect(rcFreeArea1, rcFreeArea2);
					tempAreas.Add(rcUnion);
					bUnified = TRUE; 
					rFreeAreas.RemoveAt(j--); 
					continue;
				}
		}

		if (bUnified)
			rFreeAreas.RemoveAt(i--); 

		rFreeAreas.Append(tempAreas);
	}
}

void CLayoutSide::FindLargestFreeArea(CPrintSheet* pPrintSheet, float& fLeft, float& fBottom, float& fRight, float& fTop)
{
	FindFreeAreas(pPrintSheet);
	
	fLeft = fBottom = fRight = fTop = 0.0f;

	long nMaxArea  = 0;
	int  nIndexMax = -1;
	for (int i = 0; i < m_freeAreas.GetSize(); i++)
	{
		CRect& rcFreeArea = m_freeAreas[i];
		long nWidth  = rcFreeArea.Width()  / 1000;
		long nHeight = rcFreeArea.Height() / 1000;
		if (nWidth * nHeight > nMaxArea)
		{
			nMaxArea = nWidth * nHeight;
			nIndexMax = i;
		}
	}
	if (nIndexMax >= 0)
	{
		CRect& rcFreeArea = m_freeAreas[nIndexMax];
		fLeft	= (float)rcFreeArea.left   / 1000.0f; 
		fBottom = (float)rcFreeArea.top    / 1000.0f; 
		fRight	= (float)rcFreeArea.right  / 1000.0f; 
		fTop    = (float)rcFreeArea.bottom / 1000.0f; 
	}
}

CRect CLayoutSide::FindLargestFreeArea(CPrintSheet* pPrintSheet, float fMinWidth, float fMinHeight, int* pnIndex)		// fMinWidth/-Height is the minimum size, the free area must have
{
	FindFreeAreas(pPrintSheet, fMinWidth, fMinHeight);
	
	long nMaxArea  = 0;
	int  nIndexMax = -1;
	for (int i = 0; i < m_freeAreas.GetSize(); i++)
	{
		CRect& rcFreeArea = m_freeAreas[i];
		long nWidth  = rcFreeArea.Width()  / 1000;
		long nHeight = rcFreeArea.Height() / 1000;
		if (nWidth * nHeight > nMaxArea)
		{
			nMaxArea = nWidth * nHeight;
			nIndexMax = i;
		}
	}
	if (pnIndex)
		*pnIndex = nIndexMax;

	if ( (nIndexMax >= 0) && (nIndexMax < m_freeAreas.GetSize()) )
		return m_freeAreas[nIndexMax];
	else
		return CRect(0,0,0,0);
}

BOOL CLayoutSide::IsInsideFreeArea(CPrintSheet* pPrintSheet, float fLeft, float fBottom, float fRight, float fTop)
{
	FindFreeAreas(pPrintSheet);

	CRect rcRect((long)(fLeft * 1000.0f), (long)(fTop * 1000.0f), (long)(fRight * 1000.0f), (long)(fBottom * 1000.0f) );
	rcRect.NormalizeRect();
	rcRect.DeflateRect(2, 2);
	for (int i = 0; i < m_freeAreas.GetSize(); i++)
	{
		CRect& rcFreeArea = m_freeAreas[i];
		if (rcFreeArea.PtInRect(rcRect.TopLeft()) && rcFreeArea.PtInRect(rcRect.BottomRight()) )
			return TRUE;
	}
	return FALSE;
}

void CLayoutSide::AnalyseObjectGroups(CPrintSheet* pPrintSheet)
{
	ResetAnalyseFlags();
	ResetObjectGroupAttributes(pPrintSheet);
	if (GetOppositeLayoutSide())
	{
		GetOppositeLayoutSide()->ResetObjectGroupAttributes(pPrintSheet);
		GetOppositeLayoutSide()->ResetAnalyseFlags();
	}

	int		 nDisplayColorIndex = 0;
	POSITION refPos				= m_ObjectList.GetHeadPosition();
	while (refPos)
	{
		CLayoutObject& rRefObject = m_ObjectList.GetNext(refPos);
		if (rRefObject.m_nAnalyseFlag)
			continue;
		if (rRefObject.m_nType == CLayoutObject::ControlMark)
			continue;

		rRefObject.m_nDisplayColorIndex   = nDisplayColorIndex;
		rRefObject.m_nAnalyseFlag		  = TRUE;
		CLayoutObject* pCounterpartObject = rRefObject.FindCounterpartObject();
		if (pCounterpartObject)
		{
			pCounterpartObject->m_nDisplayColorIndex = nDisplayColorIndex;
			pCounterpartObject->m_nAnalyseFlag		 = TRUE;
		}
		nDisplayColorIndex++;

		POSITION pos = refPos;
		while (pos)
		{
			CLayoutObject& rObject = m_ObjectList.GetNext(pos);
			if (rObject.m_nAnalyseFlag)
				continue;
			if (rObject.m_nType == CLayoutObject::ControlMark)
				continue;

			BOOL bSameGroup = FALSE;
			if ( ! rRefObject.IsFlatProduct())
			{
				//if ( (fabs(rRefObject.GetRealWidth() - rObject.GetRealWidth()) < 0.01f) && (fabs(rRefObject.GetRealHeight() - rObject.GetRealHeight()) < 0.01f) && (rRefObject.m_nComponentRefIndex == rObject.m_nComponentRefIndex) )
				if ( (rRefObject.GetRealWidth() == rObject.GetRealWidth()) && (rRefObject.GetRealHeight() == rObject.GetRealHeight()) )
					bSameGroup = TRUE;
			}
			else
			{
				if ( (fabs(rRefObject.GetRealWidth() - rObject.GetRealWidth()) < 0.01f) && (fabs(rRefObject.GetRealHeight() - rObject.GetRealHeight()) < 0.01f) )
					if ( (rRefObject.m_nComponentRefIndex >= 0) && (rRefObject.m_nComponentRefIndex < pPrintSheet->m_flatProductRefs.GetSize()) && (rObject.m_nComponentRefIndex >= 0) && (rObject.m_nComponentRefIndex < pPrintSheet->m_flatProductRefs.GetSize()) )
						if ( (pPrintSheet->m_flatProductRefs[rRefObject.m_nComponentRefIndex].m_nFlatProductIndex == pPrintSheet->m_flatProductRefs[rObject.m_nComponentRefIndex].m_nFlatProductIndex) )
							bSameGroup = TRUE;
			}

			if (bSameGroup)
			{
				rObject.m_nDisplayColorIndex = rRefObject.m_nDisplayColorIndex;
				rObject.m_nAnalyseFlag		 = TRUE;
				CLayoutObject* pCounterpartObject = rObject.FindCounterpartObject();
				if (pCounterpartObject)
				{
					pCounterpartObject->m_nDisplayColorIndex = rRefObject.m_nDisplayColorIndex;
					pCounterpartObject->m_nAnalyseFlag		 = TRUE;
				}
			}
		}

		CLayoutObject* pLowerLeft = FindGroupLowerLeft(rRefObject.m_nDisplayColorIndex);
		if (pLowerLeft)
		{
			pLowerLeft->m_nAttribute |= CLayoutObject::GroupLowerLeft;
			if (m_nLayoutSide == FRONTSIDE)
				if (pLowerLeft->IsFlatProduct())
					if ( (pLowerLeft->m_nComponentRefIndex >= 0) && (pLowerLeft->m_nComponentRefIndex < pPrintSheet->m_flatProductRefs.GetSize()) )
						pPrintSheet->m_flatProductRefs[pLowerLeft->m_nComponentRefIndex].m_nAttribute |= CLayoutObject::GroupLowerLeft;
		}
		if (GetOppositeLayoutSide())
		{
			pLowerLeft = GetOppositeLayoutSide()->FindGroupLowerLeft(rRefObject.m_nDisplayColorIndex);
			if (pLowerLeft)
				pLowerLeft->m_nAttribute |= CLayoutObject::GroupLowerLeft;
		}
	}
}

CLayoutObject* CLayoutSide::FindGroupLowerLeft(int nGroupIndex)
{
	CLayoutObject* pMinObject = NULL;
	POSITION	   pos		  = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if ( (rObject.m_nDisplayColorIndex != nGroupIndex) || ! rObject.m_nAnalyseFlag)
			continue;
		if ( ! pMinObject)
		{
			pMinObject = &rObject;
			continue;
		}

		if (rObject.m_fObjectCenterY < pMinObject->m_fObjectCenterY)
			pMinObject = &rObject;
		else
			if (rObject.m_fObjectCenterX < pMinObject->m_fObjectCenterX)
				pMinObject = &rObject;
	}

	return pMinObject;
}

CLayoutSide* CLayoutSide::GetOppositeLayoutSide()
{
	if ( ! GetLayout())
		return NULL;

	if (m_nLayoutSide == FRONTSIDE)
		return &GetLayout()->m_BackSide;
	else
		return &GetLayout()->m_FrontSide;

	return NULL;
}

void CLayoutSide::ResetObjectGroupAttributes(CPrintSheet* pPrintSheet)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		rObject.m_nAttribute &= ~CLayoutObject::GroupLowerLeft;
	}

	if (m_nLayoutSide == FRONTSIDE)
	{
		if (pPrintSheet)
		{
			for (int i = 0; i < pPrintSheet->m_flatProductRefs.GetSize(); i++)
				pPrintSheet->m_flatProductRefs[i].m_nAttribute &= ~CLayoutObject::GroupLowerLeft;
		}
	}
}

void CLayoutSide::ResetAnalyseFlags(BOOL bCutBlocks)
{
	if (bCutBlocks)
	{
		POSITION pos = m_cutBlocks.GetHeadPosition();
		while (pos)
			m_cutBlocks.GetNext(pos).m_nAnalyseFlag = 0;
	}
	else
	{
		POSITION pos = m_ObjectList.GetHeadPosition();
		while (pos)
			m_ObjectList.GetNext(pos).m_nAnalyseFlag = 0;
	}
}

void CLayoutSide::ResetPublicFlags()
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
		m_ObjectList.GetNext(pos).m_nPublicFlag = 0;
}


float CLayoutSide::GetFanoutOffsetX(const CString& strColorName, float fXPos)
{
	float fFanoutX = 0.0f;

	if (m_bAdjustFanout)
		fFanoutX = m_FanoutList.GetFanoutX(strColorName);

	if (fFanoutX == 0.0f)
		return 0.0f; 

	float fSheetCenterX = m_fPaperLeft + m_fPaperWidth/2.0f;

	return ((fXPos - fSheetCenterX) / (m_fPaperWidth/2.0f)) * fFanoutX;
}


float CLayoutSide::GetFanoutOffsetY(const CString& strColorName, float fYPos)
{
	float fFanoutY = 0.0f;

	if (m_bAdjustFanout)
		fFanoutY = m_FanoutList.GetFanoutY(strColorName);

	if (fFanoutY == 0.0f)
		return 0.0f; 

	float fSheetCenterY = m_fPaperBottom + m_fPaperHeight/2.0f;

	return ((fYPos - fSheetCenterY) / (m_fPaperHeight/2.0f)) * fFanoutY;
}


float CLayoutSide::GetRefline(int nReferenceItem, int nReferenceLine, CObjectRefParams* pObjectRefParams)
{
	switch (nReferenceItem)
	{
	case PLATE_REFLINE:		{
								CPressDevice* pPressDevice = GetPressDevice();
								return ((pPressDevice) ? pPressDevice->GetPlateRefline(nReferenceLine) : FLT_MAX);
							}
							break;
	case PAPER_REFLINE:		{
								switch (nReferenceLine)
								{
								case LEFT:		return m_fPaperLeft;
								case RIGHT:		return m_fPaperRight;
								case XCENTER:	return m_fPaperLeft   + m_fPaperWidth  / 2;
								case BOTTOM:	return m_fPaperBottom;
								case TOP:		return m_fPaperTop;
								case YCENTER:	return m_fPaperBottom + m_fPaperHeight / 2;
								}
							}
							break;
	case BBOXFRAME_REFLINE:	{
								float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop, fBBoxWidth, fBBoxHeight; 
								CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);
								fBBoxWidth  = fBBoxRight - fBBoxLeft;
								fBBoxHeight = fBBoxTop	 - fBBoxBottom;
								switch (nReferenceLine)
								{
								case LEFT:		return fBBoxLeft;
								case RIGHT:		return fBBoxRight;
								case XCENTER:	return fBBoxLeft   + fBBoxWidth  / 2;
								case BOTTOM:	return fBBoxBottom;
								case TOP:		return fBBoxTop;
								case YCENTER:	return fBBoxBottom + fBBoxHeight / 2;
								}
							}
							break;
	case OBJECT_REFLINE:	{
								CLayoutObjectMatrix	objectMatrix(m_ObjectList, CLayoutObjectMatrix::TakePages);	
								CLayoutObject* pObject = objectMatrix.GetObject(pObjectRefParams->m_nRowIndex, pObjectRefParams->m_nColIndex);

								return ((pObject) ? pObject->GetObjectRefline(nReferenceLine, pObjectRefParams->m_bBorder) : FLT_MAX);
							}
							break;
	}

	return 0.0f;
}

int CLayoutSide::RemoveMarkObjects(int nMarkType, BOOL bRemoveAll)
{
	CLayout* pLayout  = GetLayout();
	if ( ! pLayout)
		return 0;

	int		 nRemoved = 0;
	POSITION pos	  = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		POSITION	   prevPos = pos;
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
		{
			if ( ! bRemoveAll)	// only remove marks with corresponding CMark object
				if ( ! pLayout->m_markList.FindMark(rObject.m_strFormatName))
					continue;

			BOOL bDoRemove = FALSE;
			if (nMarkType == -1)
				bDoRemove = TRUE;
			else
			{
				CPageTemplate* pTemplate = rObject.GetCurrentMarkTemplate();
				switch (nMarkType)
				{
				case CMark::CropMark:		if (pTemplate->GetMarkType() == CMark::CropMark)
												bDoRemove = TRUE;
											break;
				case CMark::SectionMark:	if (pTemplate->GetMarkType() == CMark::SectionMark)
												bDoRemove = TRUE;
											break;
				case CMark::FoldMark:		if (pTemplate->GetMarkType() == CMark::FoldMark)
												bDoRemove = TRUE;
											break;
				case CMark::CustomMark:		if (pTemplate->GetMarkType() == CMark::CustomMark)
												bDoRemove = TRUE;
											break;
				case CMark::TextMark:		if (pTemplate->GetMarkType() == CMark::TextMark)
												bDoRemove = TRUE;
											break;
				case CMark::BarcodeMark:	if (pTemplate->GetMarkType() == CMark::BarcodeMark)
												bDoRemove = TRUE;
											break;
				case CMark::DrawingMark:	if (pTemplate->GetMarkType() == CMark::DrawingMark)
												bDoRemove = TRUE;
											break;
				}
			}

			if (bDoRemove)
			{
				m_ObjectList.RemoveAt(prevPos);
				nRemoved++;
			}
		}
	}

	return nRemoved;
}

void CLayoutSide::RemoveMarkObject(const CString strName)
{
	CLayout* pLayout  = GetLayout();
	if ( ! pLayout)
		return;

	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		POSITION	   prevPos = pos;
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
		{
			if (rObject.m_strFormatName != strName)
					continue;

			m_ObjectList.RemoveAt(prevPos);
		}
	}
}

// Search for any identical mark (same size/position, same mark source) at the layout side
BOOL CLayoutSide::FindIdenticalMark(float fLeft, float fBottom, float fRight, float fTop, CPageTemplate* pMarkTemplate)
{
	if ( ! pMarkTemplate)
		return FALSE;

	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.m_nType != CLayoutObject::ControlMark)
			continue;

		if (fabs(fLeft   - rObject.GetLeft())   > 0.01)
			continue;
		if (fabs(fRight  - rObject.GetRight())  > 0.01)
			continue;
		if (fabs(fBottom - rObject.GetBottom()) > 0.01)
			continue;
		if (fabs(fTop	 - rObject.GetTop())	> 0.01)
			continue;

		CPageTemplate* pRefTemplate = rObject.GetCurrentMarkTemplate();
		if ( ! pRefTemplate)
			continue;
		//if (pMarkTemplate->m_strPageID != pRefTemplate->m_strPageID)
		//	continue;
		if (pMarkTemplate->m_ColorSeparations.GetSize() != pRefTemplate->m_ColorSeparations.GetSize())
			continue;
		if (pMarkTemplate->m_ColorSeparations.GetSize() == 0)	// per definition: both marks w/o seps -> identical 
			return TRUE;

		CPageSource* pMarkSource = pMarkTemplate->GetObjectSource(pMarkTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
		CPageSource* pRefSource  = pRefTemplate->GetObjectSource (pRefTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
		if (pMarkSource && pRefSource)
		{
			if (pMarkSource == pRefSource)	// same address -> identical
				return TRUE;
			if (pMarkSource->m_nType != pRefSource->m_nType)
				continue;
			if (pMarkSource->m_nType == CPageSource::SystemCreated)
				if (pMarkSource->m_strSystemCreationInfo == pRefSource->m_strSystemCreationInfo)
					return TRUE;
				else
					continue;
		}

		for (int i = 0; i < pMarkTemplate->m_ColorSeparations.GetSize(); i++)
		{
			CPageSourceHeader* pMarkSourceHeader = pMarkTemplate->GetObjectSourceHeader(pMarkTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex);
			CPageSourceHeader* pRefSourceHeader  = pRefTemplate->GetObjectSourceHeader (pRefTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex);

			if (pMarkSourceHeader && pRefSourceHeader)
				if (pMarkSourceHeader == pRefSourceHeader)	// same address -> identical
					return TRUE;
				else
					if (*pMarkSourceHeader == *pRefSourceHeader)
						return TRUE;
		}
	}

	return FALSE;
}

int	CLayoutSide::GetSelectedObjectRefList(CDisplayList& rDisplayList, CArray <CSheetObjectRefs, CSheetObjectRefs&>* pObjectRefList)
{
	if (! pObjectRefList)
		return 0;

	pObjectRefList->RemoveAll();

	int			  nCount = 0;
	CDisplayItem* pDI	 = rDisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	while (pDI)
	{
		if (pDI->m_nState == CDisplayItem::Selected)
		{
			CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
			if (pObject->GetLayoutSide() == this)
			{
				CSheetObjectRefs objectRef((pObject->m_nType == CLayoutObject::Page) ? CLayoutObject::Page : CLayoutObject::ControlMark, 
											-1, pObject, NULL);

				pObjectRefList->Add(objectRef); 
				nCount++;
			}
		}

		pDI = rDisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}

	return nCount;
}

CLayoutObject* CLayoutSide::FindOverlappingPage(float fLeft, float fBottom, float fRight, float fTop)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_ObjectList.GetNext(pos);
		if ((fLeft - rLayoutObject.GetRight()) > -0.01) continue;
		else
			if ((fRight - rLayoutObject.GetLeft()) < 0.01) continue;
			else
				if ((fBottom - rLayoutObject.GetTop()) > -0.01) continue;
				else
					if ((fTop - rLayoutObject.GetBottom()) < 0.01) continue;
					else
						if (rLayoutObject.m_nType == CLayoutObject::Page)
							return &rLayoutObject;
	}
	return NULL;
}

BOOL CLayoutSide::ClipToObjects(float& fLeft, float& fBottom, float& fRight, float& fTop, float fBorder, int nDirect, BOOL bPagesOnly)
{
	fLeft	= max(fLeft,	m_fPaperLeft);
	fBottom = max(fBottom,	m_fPaperBottom);
	fRight  = min(fRight,	m_fPaperRight);
	fTop	= min(fTop,		m_fPaperTop);

	if (fRight - 0.01f < fLeft)
		return FALSE;
	if (fTop - 0.01f  < fBottom)
		return FALSE;

	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::ControlMark)
		{
			if (bPagesOnly)
				continue;
			CPageTemplate* pTemplate = rLayoutObject.GetCurrentMarkTemplate();
			if ( ! pTemplate)
				continue;
			if (pTemplate->GetMarkType() != CMark::CustomMark)
				continue;
			if (rLayoutObject.m_bHidden)
				continue;
			fBorder = 0.1f;		// by definition: 0.1mm distance to pdf custom marks
		}

		float fObjectLeft	= rLayoutObject.GetLeft()	- fBorder;
		float fObjectBottom = rLayoutObject.GetBottom() - fBorder;
		float fObjectRight	= rLayoutObject.GetRight()	+ fBorder;
		float fObjectTop	= rLayoutObject.GetTop()	+ fBorder;

		if ((fLeft - fObjectRight) > -0.01) continue;
		else
			if ((fRight - fObjectLeft) < 0.01) continue;
			else
				if ((fBottom - fObjectTop) > -0.01) continue;
				else
					if ((fTop - fObjectBottom) < 0.01) continue;
					else
					{
						if (nDirect == HORIZONTAL)
						{
							if ( (fObjectLeft < fLeft) && (fLeft < fObjectRight) )
								fLeft = fObjectRight;
							else
								if ( (fObjectLeft < fRight) && (fRight < fObjectRight) )
									fRight = fObjectLeft;

							if ( (fRight - fLeft) < 0.01f)
								return FALSE;
							else
								if ( (fLeft < fObjectLeft) && (fRight > fObjectRight) )
									return FALSE;
						}
						else
						{
							if ( (fObjectBottom < fTop) && (fTop < fObjectTop) )
								fTop = fObjectBottom;
							else
								if ( (fObjectBottom < fBottom) && (fBottom < fObjectTop) )
									fBottom = fObjectTop;
						}

						if ( (fTop - fBottom) < 0.01f)
							return FALSE;
						else
							if ( (fBottom < fObjectBottom) && (fTop > fObjectTop) )
								return FALSE;
					}
	}

	if (fRight - 0.01f < fLeft)
		return FALSE;
	if (fTop - 0.01f  < fBottom)
		return FALSE;

	return TRUE;
}

void CLayoutSide::Draw(CDC *pDC, BOOL bShowMarks, BOOL bShowShingling, BOOL bShowCutlines, BOOL bShowMeasures, BOOL bThumbnail, CPrintSheet* pPrintSheet, int nProductPartIndex, CFoldSheet* pFoldSheet, BOOL bHighlight, BOOL bShowPreviews, CDisplayList* pDisplayList, BOOL bShowOccupied, BOOL bShowNoPageTemplate)
{
	ResetPublicFlags();

	BOOL bClipMarksOutsideSheet = (bShowMarks == 2) ? TRUE : FALSE;

	if (bShowPreviews || bThumbnail)
		bShowMarks = FALSE;		// show mark previews only - not frames

	CRect outObjectRect;
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);

		if (rObject.IsAutoMarkDisabled(pPrintSheet, m_nLayoutSide))
			continue;

		if ( (rObject.m_nType == CLayoutObject::ControlMark) && bClipMarksOutsideSheet)
		{
			if (rObject.GetLeft()	> m_fPaperRight)
				continue;
			if (rObject.GetRight()	< m_fPaperLeft)
				continue;
			if (rObject.GetBottom() > m_fPaperTop)
				continue;
			if (rObject.GetTop()	< m_fPaperBottom)
				continue;
		}

		outObjectRect.left	 = CPrintSheetView::WorldToLP(rObject.GetLeftWithBorder());		outObjectRect.top	 = CPrintSheetView::WorldToLP(rObject.GetTopWithBorder());
		outObjectRect.right	 = CPrintSheetView::WorldToLP(rObject.GetRightWithBorder());	outObjectRect.bottom = CPrintSheetView::WorldToLP(rObject.GetBottomWithBorder());
		if ( ( ! rObject.IsFlatProduct()) && (rObject.m_nType == CLayoutObject::Page) && ! rObject.HasLabelNeighbour())
		{
			CPen pen(PS_SOLID, Pixel2ConstMM(pDC, 1), RGB(240,240,240));	
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
			CPrintSheetView::DrawRectangleExt(pDC, outObjectRect);
			pen.DeleteObject();
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);
		}
		CFoldSheet* pCurrentFoldSheet = rObject.GetCurrentFoldSheet(pPrintSheet);
		BOOL bDimmed = (pCurrentFoldSheet && pFoldSheet) ? ( (pCurrentFoldSheet->m_strFoldSheetTypeName == pFoldSheet->m_strFoldSheetTypeName) ? FALSE : TRUE ) : FALSE;
		if ( ! bHighlight) 
			bDimmed = TRUE;
		nProductPartIndex = (pCurrentFoldSheet) ? pCurrentFoldSheet->m_nProductPartIndex : nProductPartIndex;
		rObject.Draw(pDC, ! bDimmed, bThumbnail, bShowMarks, pPrintSheet, nProductPartIndex, bShowShingling, bShowMeasures, pDisplayList, bShowNoPageTemplate);	
	}

	if ( ! bThumbnail)
		DrawGripper(pDC, pPrintSheet);

	if (bShowPreviews)
	{
		DrawPreviews(pDC, pPrintSheet, CLayoutObject::Page);
		DrawPreviews(pDC, pPrintSheet, CLayoutObject::ControlMark, CFoldMark::Background);
		DrawPreviews(pDC, pPrintSheet, CLayoutObject::ControlMark);
		DrawPreviews(pDC, pPrintSheet, CLayoutObject::ControlMark, CFoldMark::Foreground);
	}

	DrawRefEdge(pDC, pPrintSheet);

	if (bShowCutlines)
		DrawCutLines(pDC, pPrintSheet, nProductPartIndex, bThumbnail);

	if (bShowOccupied)
	{
		if (pPrintSheet)
		{
			BOOL bIsOutputted = TRUE;//(pPrintSheet->GetLastOutputDate() != 0) ? TRUE : FALSE;
			DrawOccupied(pDC, pPrintSheet, bIsOutputted);
		}
	}
}

void CLayoutSide::DrawOccupied(CDC* pDC, CPrintSheet* pPrintSheet, BOOL bOutputted) 
{
	if ( ! pPrintSheet)
		return;
	CPlate*	pPlate = pPrintSheet->GetTopmostPlate(m_nLayoutSide);
	if ( ! pPlate)
		return;
	if ( ! pPlate->GetPlateList())
		return;
	if (pPlate == &pPlate->GetPlateList()->m_defaultPlate)
		return;

	CBitmap bm;
	bm.LoadBitmap(IDB_OCCUPIED);
	CBrush brush;
	brush.CreatePatternBrush(&bm);

	CPen	 pen(PS_SOLID, 1, WHITE);
	CPen*    pOldPen		= pDC->SelectObject(&pen);
	CBrush*	 pOldBrush		= pDC->SelectObject(&brush);

	//BOOL bIsDefaultPlate = (pPlate == &pPlate->GetPlateList()->m_defaultPlate)? TRUE : FALSE;

	CSize sizeDeflate(3, 3);
	pDC->DPtoLP(&sizeDeflate);

	int nLayoutObjectIndex = 0;
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject* pObject = &m_ObjectList.GetNext(pos);
		if ( ! pObject)
			continue;

		CExposure* pExposure = pPlate->m_ExposureSequence.GetExposure(nLayoutObjectIndex);
		nLayoutObjectIndex++;
		if ( ! pExposure)
			continue;
		//if ( ! bIsDefaultPlate)
		//	if (pExposure)
		//		continue;
		if (pObject->m_nType == CLayoutObject::ControlMark)
			continue;

		CPageTemplate* pObjectTemplate = pObject->GetCurrentPageTemplate(pPlate->GetPrintSheet());
		if (pObjectTemplate)
		{
			CRect pageRect;
			pageRect.left	= CPrintSheetView::WorldToLP(pObject->GetLeft());
			pageRect.top	= CPrintSheetView::WorldToLP(pObject->GetTop());
			pageRect.right	= CPrintSheetView::WorldToLP(pObject->GetRight());
			pageRect.bottom	= CPrintSheetView::WorldToLP(pObject->GetBottom());
			pageRect.NormalizeRect();
			pageRect.DeflateRect(sizeDeflate);
			CPrintSheetView::DrawRectangleExt(pDC, pageRect);
		}
	}

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);

	pen.DeleteObject();
	brush.DeleteObject();
}

void CLayoutSide::DrawGripper(CDC* pDC, CPrintSheet* pPrintSheet) 
{
	CLayout* pActLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pActLayout)
		return;
	CPressDevice* pPressDevice = GetPressDevice();
	if ( ! pPressDevice)
		return;
	if (pPressDevice->m_nPressType != CPressDevice::SheetPress)
		return;

	CRect rcGripper;
	rcGripper.left	 = CPrintSheetView::WorldToLP(m_fPaperLeft  );
	rcGripper.top	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge);
	rcGripper.right	 = CPrintSheetView::WorldToLP(m_fPaperRight );
	rcGripper.bottom = CPrintSheetView::WorldToLP(m_fPaperBottom);

	CPrintSheetView::FillTransparent(pDC, rcGripper, RGB(255, 150, 20), TRUE, 50);

	CSize sizeFont;
	int nFontSize = CLayout::TransformFontSize(pDC, 18);
	//sizeFont = CSize(0, WorldToLP(pPressDevice->m_fGripper));
	//pDC->LPtoDP(&sizeFont);
	//if (sizeFont.cy > 12)
	//	sizeFont.cy = 12;
	//pDC->DPtoLP(&sizeFont);

	CFont font;
	font.CreateFont(nFontSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	CFont* pOldFont = pDC->SelectObject(&font);
	CString string, strMeasure;
	SetMeasure(0, pPressDevice->m_fGripper, strMeasure);
	string.Format(IDS_GRIPPER, strMeasure);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BROWN);
	CSize sizeExtent = pDC->GetTextExtent(string);
	if ( (sizeExtent.cx < rcGripper.Width()) && (sizeExtent.cy < abs(rcGripper.Height())) )
		pDC->DrawText(string, rcGripper, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	pDC->SelectObject(pOldFont);
	font.DeleteObject();

	CRect rcSheet;
	rcSheet.left	= CPrintSheetView::WorldToLP(m_fPaperLeft  );
	rcSheet.top		= CPrintSheetView::WorldToLP(m_fPaperTop   );
	rcSheet.right	= CPrintSheetView::WorldToLP(m_fPaperRight );
	rcSheet.bottom	= CPrintSheetView::WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge);
}

void CLayoutSide::DrawPreviews(CDC* pDC, CPrintSheet* pPrintSheet, BYTE nType, int nZOrder) 
{
	CString	 strPreviewFile;
	CRect	 exposureRect, objectRect;
	CPlate*  pPlate	= pPrintSheet->GetTopmostPlate(m_nLayoutSide);

	if (pPlate)
	{
		int nColorDefinitionIndex = (pPlate->m_nColorDefinitionIndex == -1) ? 0 : pPlate->m_nColorDefinitionIndex;	// -1 = default plate -> show 1st colorant

		POSITION pos = pPlate->m_ExposureSequence.GetHeadPosition();
		while (pos)
		{
			CExposure&	   rExposure = pPlate->m_ExposureSequence.GetNext(pos);
			CLayoutObject* pObject	 = rExposure.GetLayoutObject(pPrintSheet->m_nLayoutIndex, m_nLayoutSide);
			if (!pObject)
				continue;
			if (pObject->m_nType != nType) 
				continue;									   
			if (pObject->IsAutoMarkDisabled(pPrintSheet, m_nLayoutSide))
				continue;

			CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
											  pObject->GetCurrentMarkTemplate() :
											  pObject->GetCurrentPageTemplate(pPrintSheet);

			if (pObjectTemplate)
			{
				float fSheetClipBoxLeft	  = rExposure.GetSheetBleedBox(LEFT,   pObject, pObjectTemplate);
				float fSheetClipBoxBottom = rExposure.GetSheetBleedBox(BOTTOM, pObject, pObjectTemplate);
				float fSheetClipBoxRight  = rExposure.GetSheetBleedBox(RIGHT,  pObject, pObjectTemplate);
				float fSheetClipBoxTop	  = rExposure.GetSheetBleedBox(TOP,	   pObject, pObjectTemplate);

				pObjectTemplate->GetGridClipBox(fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop);

				exposureRect.left	= CPrintSheetView::WorldToLP(fSheetClipBoxLeft);
				exposureRect.bottom = CPrintSheetView::WorldToLP(fSheetClipBoxBottom);
				exposureRect.right	= CPrintSheetView::WorldToLP(fSheetClipBoxRight);
				exposureRect.top	= CPrintSheetView::WorldToLP(fSheetClipBoxTop);
				exposureRect.NormalizeRect();
			}

			if (pDC->RectVisible(exposureRect))
			{
				if (pObjectTemplate)
				{
					if ( (pPlate->m_nColorDefinitionIndex == -1) && (pObject->m_nType == CLayoutObject::ControlMark) )	// if no plates, show 1st sep of marks
						nColorDefinitionIndex = (pObjectTemplate->m_ColorSeparations.GetSize()) ? pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1;

					CPageSource*	   pObjectSource		= pObjectTemplate->GetObjectSource		(nColorDefinitionIndex, TRUE, pPrintSheet, m_nLayoutSide);
					CPageSourceHeader* pObjectSourceHeader	= pObjectTemplate->GetObjectSourceHeader(nColorDefinitionIndex, TRUE, pPrintSheet, m_nLayoutSide);
					SEPARATIONINFO*	   pSepInfo				= pObjectTemplate->GetSeparationInfo	(nColorDefinitionIndex, TRUE, pPrintSheet, m_nLayoutSide);

					if (pObjectSource && pSepInfo)
					{
						if (pObject->m_nType == CLayoutObject::ControlMark)
							if (pObjectSource->GetSystemGeneratedMarkType() == CMark::FoldMark)
							{
								if (nZOrder == -1)
									continue;
								int	nIndex = pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
								if (nIndex != -1)
								{
									CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
									CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
									strParams.TrimLeft(); strParams.TrimRight();
									int nZOrderMark = (strParams.Find(_T("/Background")) != -1) ? CFoldMark::Background : CFoldMark::Foreground;
									if (nZOrder != nZOrderMark)
										continue;
								}
							}
							else
								if (nZOrder != -1)	
									continue;

						CRect outBBox, objectBBox;
						objectBBox.left	   = CPrintSheetView::WorldToLP(rExposure.GetContentTrimBox(LEFT,   pObject, pObjectTemplate));
						objectBBox.top	   = CPrintSheetView::WorldToLP(rExposure.GetContentTrimBox(TOP,	pObject, pObjectTemplate));
						objectBBox.right   = CPrintSheetView::WorldToLP(rExposure.GetContentTrimBox(RIGHT,  pObject, pObjectTemplate));
						objectBBox.bottom  = CPrintSheetView::WorldToLP(rExposure.GetContentTrimBox(BOTTOM, pObject, pObjectTemplate));

						int	nOutBBoxWidth, nOutBBoxHeight;
						if ((pObjectTemplate->m_fContentRotation == 0.0f) || (pObjectTemplate->m_fContentRotation == 180.0f))
						{
							nOutBBoxWidth  = CPrintSheetView::WorldToLP((pObjectSource->GetBBox(RIGHT, pObjectSourceHeader) - pObjectSource->GetBBox(LEFT,   pObjectSourceHeader)) * pObjectTemplate->m_fContentScaleX);
							nOutBBoxHeight = CPrintSheetView::WorldToLP((pObjectSource->GetBBox(TOP,   pObjectSourceHeader) - pObjectSource->GetBBox(BOTTOM, pObjectSourceHeader)) * pObjectTemplate->m_fContentScaleY);
						}
						else
						{
							nOutBBoxWidth  = CPrintSheetView::WorldToLP((pObjectSource->GetBBox(TOP,   pObjectSourceHeader) - pObjectSource->GetBBox(BOTTOM, pObjectSourceHeader)) * pObjectTemplate->m_fContentScaleX);
							nOutBBoxHeight = CPrintSheetView::WorldToLP((pObjectSource->GetBBox(RIGHT, pObjectSourceHeader) - pObjectSource->GetBBox(LEFT,   pObjectSourceHeader)) * pObjectTemplate->m_fContentScaleY);
						}
						if ((pObject->m_nHeadPosition == LEFT) || (pObject->m_nHeadPosition == RIGHT))
						{
							int nTemp = nOutBBoxWidth; nOutBBoxWidth = nOutBBoxHeight; nOutBBoxHeight = nTemp;
						}

						outBBox.left   = objectBBox.CenterPoint().x - nOutBBoxWidth  / 2;
						outBBox.bottom = objectBBox.CenterPoint().y - nOutBBoxHeight / 2;
						outBBox.right  = objectBBox.CenterPoint().x + nOutBBoxWidth  / 2;
						outBBox.top    = objectBBox.CenterPoint().y + nOutBBoxHeight / 2;

						switch (pObject->m_nHeadPosition)
						{
						case TOP:		outBBox	+= CSize(CPrintSheetView::WorldToLP( pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset)), 
													     CPrintSheetView::WorldToLP( pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset))); 
										break;
						case RIGHT:		outBBox	+= CSize(CPrintSheetView::WorldToLP( pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset)), 
													     CPrintSheetView::WorldToLP(-pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset))); 
										break;
						case BOTTOM:	outBBox	+= CSize(CPrintSheetView::WorldToLP(-pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset)), 
													     CPrintSheetView::WorldToLP(-pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset))); 
										break;
						case LEFT:		outBBox	+= CSize(CPrintSheetView::WorldToLP(-pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset)), 
													     CPrintSheetView::WorldToLP( pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset))); 
										break;
						}

						if (pObjectSource->m_nType == CPageSource::SystemCreated)
							CPrintSheetView::DrawSystemCreatedContentPreview(pDC, pObjectTemplate, pObjectSource, objectBBox, pObject, pObject->m_nHeadPosition, pObjectSource->m_strSystemCreationInfo, pPrintSheet);
						else
						{
							strPreviewFile = pObjectSource->GetPreviewFileName(pSepInfo->m_nPageNumInSourceFile);
							pDC->SaveDC();	// save current clipping region
							pDC->IntersectClipRect(exposureRect);

							CPageListView::DrawPreview(pDC, outBBox, pObject->m_nHeadPosition, pObjectTemplate->m_fContentRotation, strPreviewFile);
							//if ( ! CPageListView::DrawPreview(pDC, outBBox, pObject->m_nHeadPosition, pObjectTemplate->m_fContentRotation, strPreviewFile))
							//	DrawPreviewNotFoundMessage(pDC, objectBBox, strPreviewFile);

							//if (pObject->m_nType == CLayoutObject::Page)
							//	DrawPageID(pDC, objectBBox, *pObject, pPrintSheet, *pObjectTemplate);

							pDC->RestoreDC(-1);;
						}
					}
				}
			}

			//m_DisplayList.RegisterItem(pDC, exposureRect, exposureRect, (void*)&rExposure, 
			//						   DISPLAY_ITEM_REGISTRY(CPrintSheetView, Exposure), (void*)pActPrintSheet, CDisplayItem::ForceRegister); 
		}
	}

////	Create the pen for the LayoutObject-frame from choosen ColorComboBox-entry
//	CPen pen;
//	switch(m_nFrameColorIndex)
//	{
//	case 0: pen.CreatePen(PS_SOLID, 0, MARINE); break;
//	case 1: pen.CreatePen(PS_SOLID, 0, CYAN);	break;
//	case 2: pen.CreatePen(PS_SOLID, 0, RED);	break;
//	case 3: pen.CreatePen(PS_SOLID, 0, YELLOW); break;
//	default:pen.CreatePen(PS_SOLID, 0, MARINE); break;
//	}
////
//	CPen*	 pOldPen   = pDC->SelectObject(&pen);
//	CBrush*  pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
//	POSITION pos	   = m_ObjectList.GetHeadPosition();
//	while (pos)
//	{
//		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
//		if (rObject.m_nType != nType)
//			continue;
//
//		objectRect.left	  = CPrintSheetView::WorldToLP(rObject.GetLeft());
//		objectRect.top	  = CPrintSheetView::WorldToLP(rObject.GetTop());
//		objectRect.right  = CPrintSheetView::WorldToLP(rObject.GetRight());
//		objectRect.bottom = CPrintSheetView::WorldToLP(rObject.GetBottom());
//		if (pDC->RectVisible(objectRect))
//			DrawRectangleExt(pDC, objectRect);
//	}
//	pDC->SelectObject(pOldPen);
//	pDC->SelectObject(pOldBrush);
//	pen.DeleteObject();
}

void CLayoutSide::DrawRefEdge(CDC* pDC, CPrintSheet* pPrintSheet) 
{
	CRect rect;

	rect.left	= CPrintSheetView::WorldToLP(m_fPaperLeft  );
	rect.top	= CPrintSheetView::WorldToLP(m_fPaperTop   );
	rect.right	= CPrintSheetView::WorldToLP(m_fPaperRight );
	rect.bottom = CPrintSheetView::WorldToLP(m_fPaperBottom);

	CPen refEdgePenPaper;
	CPen* pOldPen;

	pOldPen = pDC->GetCurrentPen();

	refEdgePenPaper.CreatePen(PS_SOLID, Pixel2ConstMM(pDC, 2), OCKER);

	pDC->SelectObject(&refEdgePenPaper);

	CPrintSheetView::DrawEdge(pDC, &rect, m_nPaperRefEdge, GetLayout(), pPrintSheet, 0, PRINTSHEET, m_nLayoutSide);

	pDC->SelectObject(pOldPen);
	refEdgePenPaper.DeleteObject();
}

void CLayoutSide::DrawCutLines(CDC* pDC, CPrintSheet* pPrintSheet, int nProductPartIndex, BOOL bThumbnail)
{
	CPen pen(PS_DOT, 1, BLACK);
	CPen* pOldPen = pDC->SelectObject(&pen);

	float	fCutLine = 0.0f;
	CImage  imgHori, imgVert;
	imgHori.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_CUT_HORIZONTAL));
	imgVert.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_CUT_VERTICAL));

	CSize szImgHori( (bThumbnail) ? 10 : imgHori.GetWidth(), (bThumbnail) ? 10 : imgHori.GetHeight());
	pDC->DPtoLP(&szImgHori);
	CSize szImgVert( (bThumbnail) ? 10 : imgVert.GetWidth(), (bThumbnail) ? 10 : imgVert.GetHeight());
	pDC->DPtoLP(&szImgVert);

	long nOffset = szImgVert.cy;
	POSITION pos = m_cutBlocks.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_cutBlocks.GetNext(pos);

		if (rObject.m_LeftNeighbours.GetSize())
		{
			fCutLine = rObject.GetLeftWithBorder();
			pDC->MoveTo(CPrintSheetView::WorldToLP(fCutLine), CPrintSheetView::WorldToLP(m_fPaperTop)	 + nOffset);
			pDC->LineTo(CPrintSheetView::WorldToLP(fCutLine), CPrintSheetView::WorldToLP(m_fPaperBottom) - nOffset);
			if ( ! bThumbnail)
				imgVert.TransparentBlt(pDC->m_hDC, CPrintSheetView::WorldToLP(fCutLine), CPrintSheetView::WorldToLP(m_fPaperTop), szImgVert.cx, szImgVert.cy, WHITE);
		}
		if (rObject.m_RightNeighbours.GetSize())
		{
			fCutLine = rObject.GetRightWithBorder();
			pDC->MoveTo(CPrintSheetView::WorldToLP(fCutLine), CPrintSheetView::WorldToLP(m_fPaperTop)	 + nOffset);
			pDC->LineTo(CPrintSheetView::WorldToLP(fCutLine), CPrintSheetView::WorldToLP(m_fPaperBottom) - nOffset);
			if ( ! bThumbnail)
				imgVert.TransparentBlt(pDC->m_hDC, CPrintSheetView::WorldToLP(fCutLine), CPrintSheetView::WorldToLP(m_fPaperTop), szImgVert.cx, szImgVert.cy, WHITE);
		}
		if (rObject.m_LowerNeighbours.GetSize())
		{
			fCutLine = rObject.GetBottomWithBorder();
			pDC->MoveTo(CPrintSheetView::WorldToLP(m_fPaperLeft)  - nOffset, CPrintSheetView::WorldToLP(fCutLine));
			pDC->LineTo(CPrintSheetView::WorldToLP(m_fPaperRight) + nOffset, CPrintSheetView::WorldToLP(fCutLine));
			if ( ! bThumbnail)
				imgHori.TransparentBlt(pDC->m_hDC, CPrintSheetView::WorldToLP(m_fPaperLeft) - szImgHori.cx, CPrintSheetView::WorldToLP(fCutLine), szImgHori.cx, szImgHori.cy, WHITE);
		}
		if (rObject.m_UpperNeighbours.GetSize())
		{
			fCutLine = rObject.GetTopWithBorder();
			pDC->MoveTo(CPrintSheetView::WorldToLP(m_fPaperLeft)  - nOffset, CPrintSheetView::WorldToLP(fCutLine));
			pDC->LineTo(CPrintSheetView::WorldToLP(m_fPaperRight) + nOffset, CPrintSheetView::WorldToLP(fCutLine));
			if ( ! bThumbnail)
				imgHori.TransparentBlt(pDC->m_hDC, CPrintSheetView::WorldToLP(m_fPaperLeft) - szImgHori.cx, CPrintSheetView::WorldToLP(fCutLine), szImgHori.cx, szImgHori.cy, WHITE);
		}
	}
	pDC->SelectObject(pOldPen);
}

//void CLayoutSide::DrawFoldSheet(CDC* pDC, int nComponentRefIndex, CString strFoldSheetNumber, CFoldSheet* pActFoldSheet, CLayout* pActLayout, CPrintSheet* pActPrintSheet, BOOL bThumbnail, CDisplayList* pDisplayList, BOOL bDimmed)
//{
//	if ( ! m_ObjectList.GetCount())
//		return;
//
//	CString strPageRange		 = pActFoldSheet->GetPageRange(); 
//	CString strFoldSheetTypeName = pActFoldSheet->m_strFoldSheetTypeName;
//
//	CObArray objectPtrList;
//	float	 fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop;
//	GetFoldSheetObjects(nComponentRefIndex, &objectPtrList, &fBBoxLeft,  &fBBoxBottom, &fBBoxRight, &fBBoxTop);
//
//	BOOL bDisabled = (objectPtrList.GetSize()) ? ( ( ((CLayoutObject*)objectPtrList[0])->m_nStatus & CLayoutObject::Disabled) ? TRUE : FALSE ) : FALSE;
//
//	CRect foldsheetRect;
//	foldsheetRect.left	 = CPrintSheetView::WorldToLP(fBBoxLeft);
//	foldsheetRect.bottom = CPrintSheetView::WorldToLP(fBBoxBottom);
//	foldsheetRect.right	 = CPrintSheetView::WorldToLP(fBBoxRight);
//	foldsheetRect.top	 = CPrintSheetView::WorldToLP(fBBoxTop);
//
//	CLayoutObject& rObject = m_ObjectList.GetHead();
//	long fDeflateBy = CPrintSheetView::WorldToLP(rObject.GetWidth())/3;
//
//	foldsheetRect.left	 += fDeflateBy;
//	foldsheetRect.bottom += fDeflateBy;
//	foldsheetRect.right	 -= fDeflateBy;
//	foldsheetRect.top	 -= fDeflateBy;
//
//	BOOL bInnerForm = FALSE;
//	if (((m_nLayoutSide == FRONTSIDE) && (pActLayout->m_FoldSheetLayerTurned[nComponentRefIndex])) ||
//		((m_nLayoutSide == BACKSIDE) && (!pActLayout->m_FoldSheetLayerTurned[nComponentRefIndex])))
//		bInnerForm = TRUE;
//
//	CPen foldsheetPen;
//	if (bInnerForm)
//		foldsheetPen.CreatePen(PS_DOT, /*Pixel2ConstMM(pDC, 1)/2*/1, (bDisabled) ? RGB(220,220,220) : ( (bDimmed) ? RGB(255,220,180) : RGB(255, 180, 80)) );	// take 1 pixel since otherwise printout doesn't work in dotted style
//	else
//		foldsheetPen.CreatePen(PS_SOLID, Pixel2ConstMM(pDC, 1)/2, (bDisabled) ? RGB(220,220,220) : ( (bDimmed) ? RGB(255,220,180) : RGB(255, 180, 80)) ); 
//	CPen*	pOldPen	  =			 pDC->SelectObject(&foldsheetPen);	// Solid line
//	CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
//
//	pDC->Rectangle(foldsheetRect);
//
//	CPen refEdgePen;
//	CSize sizeMinEdgeWidth(Pixel2ConstMM(pDC, 3), Pixel2ConstMM(pDC, 3));
//	//pDC->LPtoDP(&sizeMinEdgeWidth); 
//	//if (sizeMinEdgeWidth.cx < 3)
//	//	sizeMinEdgeWidth = CSize(3, 3);
//	//pDC->DPtoLP(&sizeMinEdgeWidth);
//
//	//if (bInnerForm)
//	{
//		refEdgePen.CreatePen(PS_DOT, sizeMinEdgeWidth.cx, (bDimmed) ? LIGHTGRAY : RGB(255, 180, 80));
//		pDC->SelectObject(&refEdgePen);
//	}
//	//else
//	//{
//	//	refEdgePen.CreatePen(PS_SOLID, sizeMinEdgeWidth.cx, (bDimmed) ? LIGHTGRAY : RGB(255, 180, 80));
//	//	pDC->SelectObject(&refEdgePen);
//	//}
//
//	//if ( ! bThumbnail)
//		CPrintSheetView::DrawEdge(pDC, &foldsheetRect, pActFoldSheet->m_nFoldSheetRefEdge, pActLayout, pActPrintSheet, nComponentRefIndex, FOLDSHEET, m_nLayoutSide);
//
//	refEdgePen.DeleteObject();
//
//	if (pDisplayList)
//	{
//		if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
//		{
//			CRect auxRect = foldsheetRect;
//			if (bInnerForm) 
//				auxRect.left = auxRect.right = 0;	// trick: use display items auxrect width to indicate if it is the inner form
//			if ((m_nLayoutSide == BACKSIDE))		// - used by CopyAndPasteFoldSheet()
//				auxRect.bottom = auxRect.top = 0;	// trick: use display items auxrect height to indicate if foldsheet is on layout backside 
//													// - used by CopyAndPasteFoldSheet()
//			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, foldsheetRect, auxRect, (void*)nComponentRefIndex, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, FoldSheet), (void*)pActPrintSheet, CDisplayItem::ForceRegister); // register item into display list
//			if (pDI)
//			{
//				pDI->SetMouseOverFeedback();
//				pDI->SetHighlightColor(BLUE);
//			}
//		}
//	}
//
//	CString strOut;
//	int nFontSize = CLayout::TransformFontSize(pDC, 18, 10, 200);	// 18 mm when plotted 1:1 - 8 pixel min size, when drawn to screen
//	CFont font;
//	font.CreateFont (nFontSize, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
//	CFont* pOldFont = pDC->SelectObject(&font);
//
//	pDC->SetBkColor(RGB(245,245,245));
//
//	CRect rcText2 = foldsheetRect; rcText2.left += nFontSize/6; rcText2.top -= nFontSize/6; 
//
//	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
//	strOut = _T("");
//	int nLayerIndex	= (pActPrintSheet->m_FoldSheetLayerRefs.GetSize() > 0) ? pActPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex : 0;
//	if (pActFoldSheet)// && pDoc)
//	{
//		if (pActFoldSheet->m_LayerList.GetCount() > 1)
//			strOut.Format(_T(" %s (%d)"), (strFoldSheetNumber.IsEmpty()) ? pActFoldSheet->GetNumber() : strFoldSheetNumber, nLayerIndex + 1); 
//		else
//			strOut.Format(_T(" %s"), (strFoldSheetNumber.IsEmpty()) ? pActFoldSheet->GetNumber() : strFoldSheetNumber); 
//
//
//		pDC->SetTextColor((bDisabled) ? RGB(220,220,220) : RGB(255, 150, 50));//GUICOLOR_VALUE);
//		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);
//		//pDC->TextOutA(rcText2.left, rcText2.top, strOut);
//
//		if ( pDC->GetViewportExt().cx > 150)
//		{
//			font.DeleteObject();
//			font.CreateFont (nFontSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
//			pDC->SelectObject(&font);
//			rcText2.left += pDC->GetTextExtent(strOut).cx;
//			strOut = _T("  [") + pActFoldSheet->m_strFoldSheetTypeName + _T("]");
//			pDC->SetTextColor((bDisabled) ? RGB(220,220,220) : RGB(255, 150, 50));//GUICOLOR_VALUE);
//			pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP);
//		}
//	}
//
//	if (pDisplayList)
//	{
//		if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetNavigationView)))
//		{
//			CPrintComponentsView* pComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
//			if (pComponentsView)
//			{
//				CPrintComponent& rPrintComponent = pComponentsView->GetActComponent();
//				CFoldSheet* pFoldSheet = rPrintComponent.GetFoldSheet();
//				if (pFoldSheet)
//				{
//					if (pFoldSheet == pActPrintSheet->GetFoldSheet(nComponentRefIndex))
//					{
//						CGraphicComponent gc;
//						gc.DrawSelectionFrame(pDC, foldsheetRect, TRUE, RGB(188,156,120));
//					}
//				}
//			}
//		}
//	}
//
//	pDC->SelectObject(pOldFont);
//	font.DeleteObject();
//
//	pDC->SelectObject(pOldPen);
//	pDC->SelectObject(pOldBrush);
//	foldsheetPen.DeleteObject();
//}

void CLayoutSide::DrawFoldSheet(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, int nActFoldSheetRefIndex, CFoldSheet* pFoldSheet, BOOL bThumbnail, CDisplayList* pDisplayList) 
{
	if ( ! pFoldSheet)
		return;

	CRect objectRect, foldsheetRect, deflateRect;
	BOOL  bDrawFoldsheet	 = FALSE;
	BOOL  bFirstLoop		 = TRUE;
	CImpManDoc*  pDoc		 = CImpManDoc::GetDoc();
	CLayout*	 pActLayout  = m_pLayout;//pActPrintSheet->GetLayout();

	BOOL bDisabled = FALSE;
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
			continue;
		if ( ! rObject.IsFlatProduct() && (rObject.m_nComponentRefIndex == nActFoldSheetRefIndex) )
		{
			if (rObject.m_bHidden)
				return;

			if (bFirstLoop)
			{
				foldsheetRect.left	 = CPrintSheetView::WorldToLP(rObject.GetLeft());
				foldsheetRect.top	 = CPrintSheetView::WorldToLP(rObject.GetTop());
				foldsheetRect.right  = CPrintSheetView::WorldToLP(rObject.GetRight());
				foldsheetRect.bottom = CPrintSheetView::WorldToLP(rObject.GetBottom());
				deflateRect = foldsheetRect;
				bFirstLoop = FALSE;
			}
			bDrawFoldsheet = TRUE;
			foldsheetRect.left	 = min(CPrintSheetView::WorldToLP(rObject.GetLeft()),   foldsheetRect.left);
			foldsheetRect.bottom = min(CPrintSheetView::WorldToLP(rObject.GetBottom()), foldsheetRect.bottom);
			foldsheetRect.right	 = max(CPrintSheetView::WorldToLP(rObject.GetRight()),  foldsheetRect.right);
			foldsheetRect.top	 = max(CPrintSheetView::WorldToLP(rObject.GetTop()),	foldsheetRect.top);
			if (rObject.m_nStatus & CLayoutObject::Disabled)
				bDisabled = TRUE;
		}
	}

	if (bDrawFoldsheet)
	{
		BOOL  bMarksViewActive = FALSE;
		CPen* pOldPen;
		CPen  refEdgePenOuterFoldSheet;
		CPen  refEdgePenInnerFoldSheet;
		CPen  foldsheetPen;
		BOOL  bInnerForm;
		COLORREF crPenColor;
		int		 nPenStyle;

		pDC->SelectStockObject(HOLLOW_BRUSH);	// Predefined brush with no filling of rectangle
		pOldPen = pDC->GetCurrentPen();
// TODO: How shall we draw the foldsheet-parts
		if (((nActLayoutSide == FRONTSIDE) && (pActLayout->m_FoldSheetLayerTurned[nActFoldSheetRefIndex])) ||
			((nActLayoutSide == BACKSIDE) && (!pActLayout->m_FoldSheetLayerTurned[nActFoldSheetRefIndex])))
		{
			nPenStyle = PS_DOT;
			crPenColor = RGB(255, 180, 80);//LIGHTRED);
			bInnerForm = TRUE;
		}
		else
		{
			nPenStyle = PS_SOLID;
			crPenColor = (bMarksViewActive) ? RGB(255, 200, 120) : RGB(255, 180, 80);
			bInnerForm = FALSE;
		}
		BOOL bEnabled = TRUE;

		foldsheetPen.CreatePen(nPenStyle, Pixel2ConstMM(pDC, 1), (bDisabled) ? RGB(220,220,220) : crPenColor);

		pDC->SelectObject(&foldsheetPen);	
		
		foldsheetRect.left	 = foldsheetRect.left	+ ((deflateRect.right - deflateRect.left)  / 4);
		foldsheetRect.top	 = foldsheetRect.top	- ((deflateRect.top	  - deflateRect.bottom)/ 4);
		foldsheetRect.right	 = foldsheetRect.right	- ((deflateRect.right - deflateRect.left)  / 4);
		foldsheetRect.bottom = foldsheetRect.bottom + ((deflateRect.top	  - deflateRect.bottom)/ 4);

		if (pDC->RectVisible(foldsheetRect))
		{
			pDC->Rectangle(foldsheetRect);

			int nLayerIndex	= pActPrintSheet->m_FoldSheetLayerRefs[nActFoldSheetRefIndex].m_nFoldSheetLayerIndex;

			int nFontSize = CLayout::TransformFontSize(pDC, 18);	// 18 mm when plotted 1:1
			CFont font, boldFont;
			font.CreateFont	   (nFontSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
			boldFont.CreateFont(nFontSize, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
			CFont* pOldFont = pDC->SelectObject(&boldFont);
			COLORREF oldColor;
			oldColor = pDC->SetTextColor(RGB(200,100,0));//OCKER);	
			if ( ! bEnabled)
				pDC->SetTextColor(LIGHTGRAY);

			if (bDisabled)
				pDC->SetTextColor(RGB(220,220,220));

			CString strOut;
			if (pFoldSheet->m_nFoldSheetNumber > 0)		// foldsheetnumber can be <= 0, i.e. in LayoutPool
			{
				if (pFoldSheet->m_LayerList.GetCount() > 1)
					strOut.Format(_T("%s (%d) "), pFoldSheet->GetNumber(), nLayerIndex + 1); 
				else
					strOut.Format(_T("%s "), pFoldSheet->GetNumber()); 
			}
			else
			{
				int nNumber = ( (nActFoldSheetRefIndex >= 0) && (nActFoldSheetRefIndex < pActPrintSheet->m_FoldSheetLayerRefs.GetSize()) ) ? abs(pActPrintSheet->m_FoldSheetLayerRefs[nActFoldSheetRefIndex].m_nFoldSheetIndex) : 0;
				strOut.Format(_T("%d "), nNumber); 
			}

			CRect textRect = foldsheetRect;
			textRect.DeflateRect(CPrintSheetView::WorldToLP(10), CPrintSheetView::WorldToLP(-10));

			pDC->SetBkMode(TRANSPARENT);
			pDC->DrawText(strOut, textRect, DT_SINGLELINE|DT_LEFT|DT_TOP);

			if (! bThumbnail)
			{
				pDC->SetBkMode(OPAQUE);
				pDC->SetBkColor(RGB(255,240,225));
				pDC->SelectObject(&font);
				strOut.Format(_T("%s "), pFoldSheet->m_strFoldSheetTypeName); 
				pDC->SetTextColor(GUICOLOR_CAPTION);
				pDC->DrawText(strOut, textRect, DT_SINGLELINE|DT_LEFT|DT_BOTTOM);
				if (bMarksViewActive)
				{
					pDC->SetTextColor(RED);	
					if (bDisabled)
						pDC->SetTextColor(RGB(220,220,220));
					strOut.Format(_T(" $*_%d "), nActFoldSheetRefIndex + 1);
					pDC->DrawText(strOut, textRect, DT_SINGLELINE|DT_RIGHT|DT_TOP);	// Index for Textmarks ($1_....)
				}
				
				CBoundProductPart& rProductPart = pFoldSheet->GetProductPart();
				pDC->SetTextColor(GUICOLOR_CAPTION);

				if (bDisabled)
					pDC->SetTextColor(RGB(220,220,220));

				textRect.bottom += pDC->GetTextExtent(_T("XXX")).cy;
				pDC->SelectObject(&boldFont);
				if (pDoc->m_boundProducts.GetSize() > 1)
				{
					strOut = rProductPart.GetBoundProduct().m_strProductName;
					pDC->DrawText(strOut, textRect, DT_LEFT | DT_SINGLELINE | DT_BOTTOM | DT_END_ELLIPSIS);
					textRect.left += pDC->GetTextExtent(_T("   ") + strOut).cx;
				}
		
				pDC->SetTextColor(RGB(40,120,30));	// green
		
				if (bDisabled)
					pDC->SetTextColor(RGB(220,220,220));

				strOut.Format(_T("%s"), rProductPart.m_strName);
				pDC->DrawText(strOut, textRect, DT_LEFT | DT_SINGLELINE | DT_BOTTOM | DT_END_ELLIPSIS);
			}

			pDC->SelectObject(pOldFont);
			pDC->SetTextColor(oldColor);
			font.DeleteObject();
			boldFont.DeleteObject();
		}

		pDC->SelectObject(pOldPen);
		foldsheetPen.DeleteObject();
// TODO: How shall we draw the foldsheet-parts
		CSize szEdge(3,3);
		pDC->DPtoLP(&szEdge);
		if (((nActLayoutSide == FRONTSIDE) && (pActLayout->m_FoldSheetLayerTurned[nActFoldSheetRefIndex])) ||
			((nActLayoutSide == BACKSIDE) && (!pActLayout->m_FoldSheetLayerTurned[nActFoldSheetRefIndex])))
		{
			if ( ! bEnabled)
				refEdgePenInnerFoldSheet.CreatePen(PS_DOT, szEdge.cx, (bDisabled) ? RGB(220,220,220) : LIGHTGRAY);
			else
				refEdgePenInnerFoldSheet.CreatePen(PS_DOT, szEdge.cx, (bDisabled) ? RGB(220,220,220) : ((bMarksViewActive || bInnerForm) ? RGB(255, 200, 120) : RGB(255, 180, 80)));
			pDC->SelectObject(&refEdgePenInnerFoldSheet);
		}
		else
		{
			if ( ! bEnabled)
				refEdgePenOuterFoldSheet.CreatePen(PS_SOLID, szEdge.cx, (bDisabled) ? RGB(220,220,220) : LIGHTGRAY);
			else
				refEdgePenOuterFoldSheet.CreatePen(PS_SOLID, szEdge.cx, (bDisabled) ? RGB(220,220,220) : ((bMarksViewActive || bInnerForm) ? RGB(255, 200, 120) : RGB(255, 180, 80)));
			pDC->SelectObject(&refEdgePenOuterFoldSheet);
		}

		if ( ! bThumbnail)
			CPrintSheetView::DrawEdge(pDC, &foldsheetRect, pFoldSheet->m_nFoldSheetRefEdge, pActLayout, pActPrintSheet, nActFoldSheetRefIndex, FOLDSHEET, nActLayoutSide, pDisplayList, TRUE);
		//DrawFoldSheetSectionBar(pDC, pActPrintSheet, nActFoldSheetRefIndex, nActLayoutSide);

		CRect auxRect = foldsheetRect;
		if (bInnerForm) 
			auxRect.left = auxRect.right = 0;	// trick: use display items auxrect width to indicate if it is the inner form
		if ((nActLayoutSide == BACKSIDE))		// - used by CopyAndPasteFoldSheet()
			auxRect.bottom = auxRect.top = 0;	// trick: use display items auxrect height to indicate if foldsheet is on layout backside 
												// - used by CopyAndPasteFoldSheet()
		CLayoutObject* pCutBlock = GetFoldSheetCutBlock(nActFoldSheetRefIndex);
		CRect auxAuxRect;
		if (pCutBlock)
		{
			float fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight; 
			pCutBlock->GetCurrentGrossCutBlock(pActPrintSheet, fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight, FALSE);
			auxAuxRect.left	  = CPrintSheetView::WorldToLP(fCropBoxX);
			auxAuxRect.top	  = CPrintSheetView::WorldToLP(fCropBoxY);
			auxAuxRect.right  = CPrintSheetView::WorldToLP(fCropBoxX + fCropBoxWidth);
			auxAuxRect.bottom = CPrintSheetView::WorldToLP(fCropBoxY + fCropBoxHeight);
		}
		CFoldSheetLayer* pLayer = pActPrintSheet->GetFoldSheetLayer(nActFoldSheetRefIndex);
		BOOL bLocked = (pLayer) ? pLayer->m_bPagesLocked : FALSE;
		//if ( ! MarksViewActive() && ! OutputViewActive() && bLocked)
		//{
		//	int nItemType = CDisplayItem::ForceRegister | CDisplayItem::GroupMember;
		//	CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, (bLocked) ? auxAuxRect : foldsheetRect, auxRect, (void*)nActFoldSheetRefIndex, SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, FoldSheet), (void*)pActPrintSheet, nItemType, NULL, auxAuxRect); // register item into display list
		//	if (pDI)
		//	{
		//		pDI->SetHighlightColor(RGB(190,112,85));
		//		pDI->SetMouseOverFeedback();
		//		if (pDI->m_nState != CDisplayItem::Normal)
		//		{
		//			FillTransparent(pDC, auxAuxRect, RGB(230,218,205), TRUE, 80);
		//			int nHatchStyle = HS_FDIAGONAL;
		//			CBrush  brush(nHatchStyle, RGB(220,190,65));
		//			CBrush* pOldBrush  = pDC->SelectObject(&brush);
		//			CPen*	pOldPen	   = (CPen*)pDC->SelectStockObject(NULL_PEN);
		//			int		nOldBkMode = pDC->SetBkMode(TRANSPARENT);
		//			DrawRectangleExt(pDC, auxAuxRect);
		//			pDC->SelectObject(pOldBrush);
		//			pDC->SelectObject(pOldPen);
		//			pDC->SetBkMode(nOldBkMode);
		//			brush.DeleteObject();
		//		}
		//	}
		//}

		pDC->SelectObject(pOldPen);
		refEdgePenOuterFoldSheet.DeleteObject();
		refEdgePenInnerFoldSheet.DeleteObject();
	}
}

void CLayoutSide::DrawCutBlock(CDC* pDC, CRect rcFrame, CLayoutObject& rCutBlock, CLayout* pActLayout, BOOL bThumbnail, CDisplayList* pDisplayList, BOOL bDimmed)
{
	long nExtentX = CPrintSheetView::WorldToLP(rCutBlock.GetWidth()); 
	long nExtentY = CPrintSheetView::WorldToLP(rCutBlock.GetHeight());
	long nXOrg	  = CPrintSheetView::WorldToLP(rCutBlock.GetLeft());
	long nYOrg	  = CPrintSheetView::WorldToLP(rCutBlock.GetBottom());

	CRect rcViewPort = rcFrame;

	CPoint ptVPOrg = pDC->GetViewportOrg();

	pDC->SaveDC();
	CRect rcClip = rcViewPort; 
	pDC->IntersectClipRect(rcClip);
	rcViewPort.DeflateRect(5, 5);
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(nExtentX, nExtentY);
	pDC->SetWindowOrg(nXOrg,	nExtentY + nYOrg);
	pDC->SetViewportExt(rcViewPort.Width(), -rcViewPort.Height());
	pDC->SetViewportOrg(rcViewPort.left + ptVPOrg.x, rcViewPort.top + ptVPOrg.y);

	float fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight; 
	rCutBlock.GetCurrentGrossCutBlock((pActLayout) ? pActLayout->GetFirstPrintSheet() : NULL, fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight);
	CRect sheetRect;
	sheetRect.left   = CPrintSheetView::WorldToLP(pActLayout->m_FrontSide.m_fPaperLeft   + fCropBoxX);
	sheetRect.bottom = CPrintSheetView::WorldToLP(pActLayout->m_FrontSide.m_fPaperBottom + fCropBoxY);
	sheetRect.right	 = CPrintSheetView::WorldToLP(pActLayout->m_FrontSide.m_fPaperLeft   + fCropBoxX + fCropBoxWidth);
	sheetRect.top	 = CPrintSheetView::WorldToLP(pActLayout->m_FrontSide.m_fPaperBottom + fCropBoxY + fCropBoxHeight);

	CSize sizeOffset(1,1);
	pDC->DPtoLP(&sizeOffset);
	sheetRect.InflateRect(sizeOffset);

	//// draw sheet
	//if (pDC->RectVisible(sheetRect))
	//{
	//	CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(WHITE_BRUSH);	
	//	CPen*   pOldPen	  = (CPen*)	 pDC->SelectStockObject(NULL_PEN);	
	//	CPrintSheetView::DrawRectangleExt(pDC, &sheetRect);
	//	pDC->SelectObject(pOldBrush);
	//	pDC->SelectObject(pOldPen);		
	//}

	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_ObjectList.GetNext(pos);
		if (rObject.IsFlatProduct())
			if ( (rObject.m_nComponentRefIndex == rCutBlock.m_nComponentRefIndex) && (rObject.m_nBlockNumber == rCutBlock.m_nBlockNumber) )
				rObject.Draw(pDC, TRUE, bThumbnail, FALSE, (pActLayout) ? pActLayout->GetFirstPrintSheet() : NULL);
	}
	
	//// draw sheet frame
	//if (pDC->RectVisible(sheetRect))
	//{
	//	CPen framePen;
	//	framePen.CreatePen (PS_SOLID, Pixel2ConstMM(pDC, 1), RGB(230,230,230));
	//	CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);	
	//	CPen*   pOldPen	  = pDC->SelectObject(&framePen);	

	//	CPrintSheetView::DrawRectangleExt(pDC, &sheetRect);

	//	pDC->SelectObject(pOldBrush);
	//	pDC->SelectObject(pOldPen);		
	//	framePen.DeleteObject();
	//}

	pDC->RestoreDC(-1);
}

CLayoutObject* CLayoutSide::GetFoldSheetCutBlock(int nComponentRefIndex)
{
	POSITION pos = m_cutBlocks.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_cutBlocks.GetNext(pos);
		if ( ! rObject.IsFlatProduct())
			if (rObject.m_nComponentRefIndex == nComponentRefIndex)
				return &rObject;
	}

	return NULL;
}

CLayoutObject* CLayoutSide::GetLabelCutBlock(int nComponentRefIndex, int nBlockNumber)
{
	POSITION pos = m_cutBlocks.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = m_cutBlocks.GetNext(pos);
		if (rObject.IsFlatProduct())
			if ( (rObject.m_nComponentRefIndex == nComponentRefIndex) && (rObject.m_nBlockNumber == nBlockNumber) )
				return &rObject;
	}

	return NULL;
}

void CLayoutSide::SetAllVisible(CPrintSheetView* pView)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
		if (pView)
		{
			if (rObject.m_bHidden || rObject.m_nStatus & CLayoutObject::Disabled)
			{
				CDisplayItem*pDI = pView->m_DisplayList.GetItemFromData((void*)&rObject, (void*)pView->GetActPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
				if (pDI)
					pView->m_DisplayList.InvalidateItem(pDI, TRUE);
			}
		}

		rObject.m_bHidden = FALSE;
		rObject.m_nStatus &= ~CLayoutObject::Disabled;
	}
}

void CLayoutSide::SetFoldSheetLayerVisible(int nComponentRefIndex, BOOL bShow)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
		if (rObject.m_nType != CLayoutObject::Page)
			continue;
		if (rObject.IsFlatProduct())
			continue;
		if (rObject.m_nComponentRefIndex != nComponentRefIndex)
			continue;
	
		rObject.m_bHidden = ! bShow;
	}
}

void CLayoutSide::SetFlatProductVisible(int nComponentRefIndex, BOOL bShow)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
		if (rObject.m_nType != CLayoutObject::Page)
			continue;
		if ( ! rObject.IsFlatProduct())
			continue;
		if (rObject.m_nComponentRefIndex != nComponentRefIndex)
			continue;
	
		rObject.m_bHidden = ! bShow;
	}
}

void CLayoutSide::SetMarkVisible(CString strMarkName, BOOL bShow)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
		if (rObject.m_nType != CLayoutObject::ControlMark)
			continue;
		if (rObject.m_strFormatName != strMarkName)
			continue;
	
		rObject.m_bHidden = ! bShow;
	}
}

CLayoutObject* CLayoutSide::FindFirstLayoutObject(CString strName, int nType)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
		if (rObject.m_nType != nType)
			continue;
		if (rObject.m_strFormatName == strName)
			return &rObject;
	}
	return NULL;
}

CLayoutObject* CLayoutSide::FindNextLayoutObject(CLayoutObject* pLayoutObject, CString strName, int nType)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
		if (rObject.m_nType != nType)
			continue;
		if (&rObject == pLayoutObject)
			break;
	}
	
	while (pos)
	{
		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
		if (rObject.m_nType != nType)
			continue;
		if (rObject.m_strFormatName == strName)
			return &rObject;
	}
	return NULL;
}

int CLayoutSide::GetTotalNUp()
{
	int nNUp = 0;
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::ControlMark)
			continue;
		nNUp++;
	}
	return nNUp;
}

CLayoutObject* CLayoutSide::GetLayoutObjectByIndex(int nLayoutObjectIndex)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
		if (rObject.m_nIndex == nLayoutObjectIndex)
			return &rObject;
	}
	return NULL;
}

CLayoutObject* CLayoutSide::FindObjectSamePosition(CLayoutObject& rLayoutObject)
{
	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= m_ObjectList.GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
			continue;

		if (fabs(rObject.GetLeft() - rLayoutObject.GetLeft()) < 0.01f)
			if (fabs(rObject.GetRight() - rLayoutObject.GetRight()) < 0.01f)
				if (fabs(rObject.GetBottom() - rLayoutObject.GetBottom()) < 0.01f)
					if (fabs(rObject.GetTop() - rLayoutObject.GetTop()) < 0.01f)
						return &rObject;
	}
	return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// CLayoutObjectMatrix member functions

CLayoutObjectMatrix::CLayoutObjectMatrix(CLayoutObjectList& objectList, int nTakeWhat)
{
	if (nTakeWhat & TakePages)
	{
		POSITION pos = objectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = objectList.GetNext(pos);
			if (rObject.m_nType == CLayoutObject::Page)
			{
				rObject.m_fObjectCenterX = rObject.GetLeft()   + (rObject.GetRight() - rObject.GetLeft()  ) / 2.0f;
				rObject.m_fObjectCenterY = rObject.GetBottom() + (rObject.GetTop()   - rObject.GetBottom()) / 2.0f;
				CLayoutObjectMatrixElem elem(&rObject, 0);
				m_elems.Add(elem);
			}
		}
	}
	else
		return;	// only TakePages implemented yet

	CLayoutObject* pNext = FindLowerLeft();
	while (pNext)
	{
		CLayoutObjectMatrixRow matrixRow;
		while (pNext)
		{
			matrixRow.Add(pNext);
			pNext = FindNextOnRight(pNext);
		}
		Add(matrixRow);

		pNext = FindLowerLeft();
	}
}

CLayoutObject* CLayoutObjectMatrix::GetObject(int nRowIndex, int nColIndex)
{
	if ((nRowIndex < 0) || (nColIndex < 0))
		return NULL;

	if (nRowIndex % 2)	// odd
		nRowIndex = (GetSize() - 1) - (nRowIndex - 1) / 2;
	else
		nRowIndex /= 2;

	if ( (nRowIndex < 0) || (nRowIndex >= GetSize()) )
		return NULL;

	CLayoutObjectMatrixRow& rRow = ElementAt(nRowIndex);

	if (nColIndex % 2)	// odd
		nColIndex = (rRow.GetSize() - 1) - (nColIndex - 1) / 2;
	else
		nColIndex /= 2;

	if ( (nColIndex < 0) || (nColIndex >= rRow.GetSize()) )
		return NULL;
 
	return rRow.ElementAt(nColIndex);
}

void CLayoutObjectMatrix::GetIndices(CLayoutObject* pObject, BYTE& nRowIndex, BYTE& nColIndex)
{
	for (nRowIndex = 0; nRowIndex < GetSize(); nRowIndex++)
	{
		CLayoutObjectMatrixRow& rRow = ElementAt(nRowIndex);
		for (nColIndex = 0; nColIndex < rRow.GetSize(); nColIndex++)
		{
			if (rRow[nColIndex] == pObject)
			{
				int nHalf = GetSize() / 2 + GetSize() % 2;		// transform matrix row/col representation into special representation
				if (nRowIndex < nHalf)							// where increasing indices switches between left and right (or (bottom/top)
					nRowIndex *= 2;								// to get a form which is more independend from matrix dimensions (layout dimensions)
				else											// Example:  0 1 2 3 4 5  ->	0 2 4 5 3 1
					nRowIndex = (BYTE)( ((GetSize() - 1) - nRowIndex) * 2 + 1 );

				nHalf = rRow.GetSize() / 2 + rRow.GetSize() % 2;
				if (nColIndex < nHalf)
					nColIndex *= 2;
				else
					nColIndex = (BYTE)( ((rRow.GetSize() - 1) - nColIndex) * 2 + 1 );

				return;
			}
		}
	}
	nRowIndex = nColIndex = 0;
}

void CLayoutObjectMatrix::GetMatrixIndices(CLayoutObject* pObject, BYTE& nRowIndex, BYTE& nColIndex)
{
	for (nRowIndex = 0; nRowIndex < GetSize(); nRowIndex++)
	{
		CLayoutObjectMatrixRow& rRow = ElementAt(nRowIndex);
		for (nColIndex = 0; nColIndex < rRow.GetSize(); nColIndex++)
		{
			if (rRow[nColIndex] == pObject)
				return;
		}
	}
	nRowIndex = nColIndex = 0;
}

CLayoutObject* CLayoutObjectMatrix::FindLowerLeft()
{
	if ( ! m_elems.GetSize())
		return NULL;

	CLayoutObject* pLowerLeft	   = m_elems[0].m_pObject;
	int			   nLowerLeftIndex = 0;
	for (int i = 1; i < m_elems.GetSize(); i++)
	{
		CLayoutObject* pObject = m_elems[i].m_pObject;
		if (pObject->m_fObjectCenterY < pLowerLeft->GetBottom())
		{
			pLowerLeft		= pObject;
			nLowerLeftIndex = i;
		}
		else
			if (pObject->m_fObjectCenterY < pLowerLeft->GetTop())
				if (pObject->m_fObjectCenterX < pLowerLeft->m_fObjectCenterX)
				{
					pLowerLeft		= pObject;
					nLowerLeftIndex = i;
				}
	}

	m_elems.RemoveAt(nLowerLeftIndex);

	return pLowerLeft;
}


CLayoutObject* CLayoutObjectMatrix::FindNextOnRight(CLayoutObject* pRefObject)
{
	CLayoutObject* pNext	  = NULL;
	int			   nNextIndex = -1;
	float		   fMinX	  = FLT_MAX;
	for (int i = 0; i < m_elems.GetSize(); i++)
	{
		CLayoutObject* pObject = m_elems[i].m_pObject;
		if (pObject->m_fObjectCenterX > pRefObject->m_fObjectCenterX)
			if (pObject->m_fObjectCenterX < fMinX)
				if ( (pObject->m_fObjectCenterY < pRefObject->GetTop()) && (pObject->m_fObjectCenterY > pRefObject->GetBottom()) )
				{
					fMinX	   = pObject->m_fObjectCenterX;
					pNext	   = pObject;
					nNextIndex = i;
				}
	}

	if (nNextIndex != -1)
		m_elems.RemoveAt(nNextIndex);

	return pNext;
}


CLayoutObjectMatrixRow::CLayoutObjectMatrixRow(const CLayoutObjectMatrixRow& rLayoutObjectMatrixRow) // copy constructor
{
	Copy(rLayoutObjectMatrixRow);
}


const CLayoutObjectMatrixRow& CLayoutObjectMatrixRow::operator=(const CLayoutObjectMatrixRow& rLayoutObjectMatrixRow)
{
	Copy(rLayoutObjectMatrixRow);

	return *this;
}


CLayoutObjectMatrixRow::CLayoutObjectMatrixRow()
{
}


CLayoutObjectMatrixElem::CLayoutObjectMatrixElem()
{
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// CStepAndRepeatMatrix member functions

CStepAndRepeatMatrix::CStepAndRepeatMatrix()
{
}

void CStepAndRepeatMatrix::Reset()
{
	RemoveAll();
	m_elems.RemoveAll();
	m_nNx = 0;
	m_nNy = 0;
	m_fXPos = 0.0f;
	m_fYPos = 0.0f;
	m_fStepX = 0.0f;
	m_fStepY = 0.0f;
	m_nHeadPosition = TOP;
	m_ptInitial = CPoint(0,0);
}

void CStepAndRepeatMatrix::Create(CLayoutObject* pInitialObject, CPoint ptInitial, CPrintSheet* pPrintSheet, int nTakeWhat, BOOL bNormalize)
{
	m_bNormalize = bNormalize;

	Reset();

	if ( ! pInitialObject)
		return;

	CRect rcInitial((long)(pInitialObject->GetLeft() * 1000), (long)((pInitialObject->GetBottom()) * 1000), (long)(pInitialObject->GetRight() * 1000), (long)(pInitialObject->GetTop() * 1000) );
	m_ptInitial = rcInitial.CenterPoint();

	CObArray objectPtrList;
	pInitialObject->FindLayoutObjectCluster(objectPtrList, pPrintSheet, bNormalize);

	m_fBoxLeft = 0.0f, m_fBoxBottom = 0.0f, m_fBoxRight = 0.0f, m_fBoxTop = 0.0f;
	if (nTakeWhat & TakePages)
	{
		float fMinLeft = FLT_MAX, fMinBottom = FLT_MAX, fMaxRight = -FLT_MAX, fMaxTop = -FLT_MAX;
		for (int i = 0; i < objectPtrList.GetSize(); i++)
		{
			CLayoutObject* pObject = (CLayoutObject*)objectPtrList[i];
			if (pObject->m_nType == CLayoutObject::Page)
			{
				pObject->m_fObjectCenterX = pObject->GetLeft()   + (pObject->GetRight() - pObject->GetLeft()  ) / 2.0f;
				pObject->m_fObjectCenterY = pObject->GetBottom() + (pObject->GetTop()   - pObject->GetBottom()) / 2.0f;
				CStepAndRepeatElem elem(pObject, 0);
				m_elems.Add(elem);

				float fLeft, fBottom, fRight, fTop;
				if (nTakeWhat & TakeGrossBox)
					pObject->GetCurrentGrossBox(pPrintSheet, fLeft, fBottom, fRight, fTop);
				else
				{
					fLeft	= pObject->GetLeft();
					fBottom = pObject->GetBottom();
					fRight	= pObject->GetRight();
					fTop	= pObject->GetTop();
				}
				fMinLeft   = min(fMinLeft,   fLeft);
				fMinBottom = min(fMinBottom, fBottom);
				fMaxRight  = max(fMaxRight,  fRight);
				fMaxTop	   = max(fMaxTop,	 fTop);
			}
		}
		if (m_elems.GetSize() > 0)
		{
			m_fBoxLeft = fMinLeft; m_fBoxBottom = fMinBottom; m_fBoxRight = fMaxRight; m_fBoxTop = fMaxTop;
		}
	}
	else
		return;	// only TakePages implemented yet

	CLayoutObject* pNext = FindLowerLeft();
	while (pNext)
	{
		CStepAndRepeatRow matrixRow;
		while (pNext)
		{
			matrixRow.Add(pNext);
			pNext = FindNextOnRight(pNext);
		}
		Add(matrixRow);

		pNext = FindLowerLeft();
	}

	if ( ! m_bNormalize)
		return;

/////////////////////////////////////////////////////////////////
////// normalize

	//float fMaxLeft  = -FLT_MAX;
	//float fMinRight =  FLT_MAX;
	//for (int nRow = 0; nRow < GetSize() - 1; nRow++)
	//{
	//	CStepAndRepeatRow row = GetAt(nRow);
	//	if (row.GetSize() >= 1)
	//	{
	//		fMaxLeft  = max(fMaxLeft,  row[0]->GetLeft());
	//		fMinRight = min(fMinRight, row[row.GetSize() - 1]->GetRight());
	//	}
	//}

	//int nInitialRow = -1;
	//for (int nRow = 0; nRow < GetSize(); nRow++)
	//{
	//	CStepAndRepeatRow row = GetAt(nRow);

	//	BOOL bFound = FALSE;
	//	for (int nCol = 0; nCol < row.GetSize(); nCol++)
	//	{
	//		if (row[nCol] == pInitialObject)
	//		{
	//			nInitialRow = nRow;
	//			break;;
	//		}
	//	}
	//	if (bFound)
	//		continue;

	//	if (row.GetSize() >= 1)
	//	{
	//		if (row[0]->GetLeft() < fMaxLeft)
	//			row.RemoveAt(0);
	//		if (row.GetSize() >= 1)
	//			if (row[row.GetSize() - 1]->GetRight() > fMinRight)
	//				row.RemoveAt(row.GetSize() - 1);
	//	}
	//}

	//if (nInitialRow >= 0)
	//{
	//	for (int nRow = 0; nRow < GetSize(); nRow++)
	//	{
	//		CStepAndRepeatRow row = GetAt(nRow);
	//		if (nRow == nInitialRow)
	//			continue;

	//		if (row.GetSize() != GetAt(nInitialRow).GetSize())
	//		{
	//			RemoveAt(nRow); 
	//			if (nRow < nInitialRow)
	//				nInitialRow--;
	//			nRow--; 
	//		}
	//	}
	//}
}

CLayoutObject* CStepAndRepeatMatrix::GetObject(int nRowIndex, int nColIndex)
{
	if ((nRowIndex < 0) || (nColIndex < 0) || (nRowIndex >= GetSize()))
		return NULL;

	CStepAndRepeatRow& rRow = ElementAt(nRowIndex);

	if (nColIndex >= rRow.GetSize())
		return NULL;
 
	return rRow.ElementAt(nColIndex);
}

void CStepAndRepeatMatrix::GetMatrixIndices(CLayoutObject* pObject, BYTE& nRowIndex, BYTE& nColIndex)
{
	for (nRowIndex = 0; nRowIndex < GetSize(); nRowIndex++)
	{
		CStepAndRepeatRow& rRow = ElementAt(nRowIndex);
		for (nColIndex = 0; nColIndex < rRow.GetSize(); nColIndex++)
		{
			if (rRow[nColIndex] == pObject)
				return;
		}
	}
	nRowIndex = nColIndex = 0;
}

int	CStepAndRepeatMatrix::GetNx()
{
	return (GetSize() > 0) ? GetAt(0).GetSize() : m_nNx;
}

int	CStepAndRepeatMatrix::GetNy()
{
	return (GetSize() > 0) ? GetSize() : m_nNy;
}

float CStepAndRepeatMatrix::GetXPos()
{
	CLayoutObject* pObject = GetObject(0, 0);
	if ( ! pObject)
		return m_fXPos;
	return pObject->GetLeft();
}

float CStepAndRepeatMatrix::GetYPos()
{
	CLayoutObject* pObject = GetObject(0, 0);
	if ( ! pObject)
		return m_fYPos;
	return pObject->GetBottom();
}

float CStepAndRepeatMatrix::GetStepX()
{
	CLayoutObject* pObject1 = GetObject(0, 0);
	if ( ! pObject1)
		return m_fStepX;
	CLayoutObject* pObject2 = GetObject(0, 1);
	if ( ! pObject2)
		return m_fStepX;
	return fabs(pObject2->GetObjectCenterX() - pObject1->GetObjectCenterX());
}

float CStepAndRepeatMatrix::GetStepY()
{
	CLayoutObject* pObject1 = GetObject(0, 0);
	if ( ! pObject1)
		return m_fStepY;
	CLayoutObject* pObject2 = GetObject(1, 0);
	if ( ! pObject2)
		return m_fStepY;
	return fabs(pObject2->GetObjectCenterY() - pObject1->GetObjectCenterY());
}

int	CStepAndRepeatMatrix::GetHeadPosition(int nRow, int nCol)
{
	CLayoutObject* pObject = GetObject(0, 0);
	if ( ! pObject)
		return m_nHeadPosition;
	return pObject->m_nHeadPosition;
}

CLayoutObject* CStepAndRepeatMatrix::FindLowerLeft()
{
	if ( ! m_elems.GetSize())
		return NULL;

	CLayoutObject* pLowerLeft	   = m_elems[0].m_pObject;
	int			   nLowerLeftIndex = 0;
	for (int i = 1; i < m_elems.GetSize(); i++)
	{
		CLayoutObject* pObject = m_elems[i].m_pObject;
		if (pObject->m_fObjectCenterY < pLowerLeft->GetBottom())
		{
			pLowerLeft		= pObject;
			nLowerLeftIndex = i;
		}
		else
			if (pObject->m_fObjectCenterY < pLowerLeft->GetTop())
				if (pObject->m_fObjectCenterX < pLowerLeft->m_fObjectCenterX)
				{
					pLowerLeft		= pObject;
					nLowerLeftIndex = i;
				}
	}

	m_elems.RemoveAt(nLowerLeftIndex);

	return pLowerLeft;
}


CLayoutObject* CStepAndRepeatMatrix::FindNextOnRight(CLayoutObject* pRefObject)
{
	CLayoutObject* pNext	  = NULL;
	int			   nNextIndex = -1;
	float		   fMinX	  = FLT_MAX;
	for (int i = 0; i < m_elems.GetSize(); i++)
	{
		CLayoutObject* pObject = m_elems[i].m_pObject;
		if (pObject->m_fObjectCenterX > pRefObject->m_fObjectCenterX)
			if (pObject->m_fObjectCenterX < fMinX)
				if ( (pObject->m_fObjectCenterY < pRefObject->GetTop()) && (pObject->m_fObjectCenterY > pRefObject->GetBottom()) )
				{
					fMinX	   = pObject->m_fObjectCenterX;
					pNext	   = pObject;
					nNextIndex = i;
				}
	}

	if (nNextIndex != -1)
		m_elems.RemoveAt(nNextIndex);

	return pNext;
}


CStepAndRepeatRow::CStepAndRepeatRow(const CStepAndRepeatRow& rStepAndRepeatRow) // copy constructor
{
	Copy(rStepAndRepeatRow);
}


const CStepAndRepeatRow& CStepAndRepeatRow::operator=(const CStepAndRepeatRow& rStepAndRepeatRow)
{
	Copy(rStepAndRepeatRow);

	return *this;
}


CStepAndRepeatRow::CStepAndRepeatRow()
{
}


CStepAndRepeatElem::CStepAndRepeatElem()
{
}
////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////
// CLayoutObject member functions

void CLayoutObject::FindLeftNeighbours()
{
	register int i, j;


	if ( ! LocRight.GetSize())
		return;
	int	  nStartIndex = -1, nEndIndex = -1;
	float fRight	  = 0.0f;
	for (j = LocRight[m_nIndex] - 1; j >= 0; j--)	// check if old left neighbour is the only new left neighbour
	{
		if (SortRight[j]->GetRight() <= m_fObjectCenterX)
		{
			fRight = SortRight[j]->GetRight();
			nEndIndex = nStartIndex = j;
			break;
		}
	}
	for (j = j - 1; j >= 0; j--)
	{
		if (SortRight[j]->GetRight() == fRight)
			nStartIndex--;
		else
			break;
	}

	CLayoutObject *pTestObject;
	if (m_LeftNeighbours.GetSize(TRUE))
		pTestObject = m_LeftNeighbours.CArray<CLayoutObject*, CLayoutObject*>::GetAt(0);
	else
		pTestObject = NULL;

	if (!pTestObject)
		if (nEndIndex == -1)
			return;
		else ;
	else
	{
		int nLoc = LocRight[pTestObject->m_nIndex];
		if ((nLoc >= nStartIndex) && (nLoc <= nEndIndex))
			if ((pTestObject->GetBottom() <= GetBottom()) && (pTestObject->GetTop() >= GetTop()))
			{
				m_LeftNeighbours.SetSize(1);  // m_LeftNeighbours[0] is the only left neighbour 
				return;						  // Before this analyses there could be more than one -> so truncate the array
			}
			m_LeftNeighbours.RemoveAll();
	}


	// 'this' LayoutObject could got new neighbours or old ones are no longer valid
  
	for (j = LocRight[m_nIndex] - 1; j >= 0; j--) // Search StartIndex in SortRight list
	{
		if (SortRight[j]->GetRight() <= m_fObjectCenterX)
			break;
	}
	nStartIndex = j;

	BOOL bFound, bTmpNeighbour = FALSE;
	for (i = nStartIndex; i >= 0; i--)
	{
		if (!OverlappingInY(SortRight[i]))
			continue;

		// Y-overlapping object is left from 'this' one
		pTestObject = SortRight[i];
		if (OverlappingInX(pTestObject))
			if ((GetTop() < pTestObject->m_fObjectCenterY) || (GetBottom() > pTestObject->m_fObjectCenterY))
			{
				pTestObject->m_nAnalyseFlag  = TRUE;	// Not yet a neighbour, because Y overlapping has to exceed 
				bTmpNeighbour = TRUE;			// the center if we have an X overlapping as well.        
			} 									// Nevertheless the object will be taken into account,    
												// because probably it is a potential right neighbour     
		bFound = FALSE;							// of the following pTestObject.                          
		for (j = 0; j < m_LeftNeighbours.GetSize(TRUE); j++)
		{
			if (m_LeftNeighbours[j]->IsPotentialRightNeighbour(pTestObject))	
			{																
				bFound = TRUE; break;										
			}																
		}																	

		if (!bFound)
		{
			m_LeftNeighbours.Add(pTestObject);

			if ((pTestObject->GetBottom() <= GetBottom()) && (pTestObject->GetTop() >= GetTop()))
				break;
		}
	}

	if (bTmpNeighbour)  // remove temporary neighbours
	{
		i = 0;
		while (i < m_LeftNeighbours.GetSize(TRUE))
		{
			if (m_LeftNeighbours[i]->m_nAnalyseFlag)
			{
				m_LeftNeighbours[i]->m_nAnalyseFlag = 0;
				m_LeftNeighbours.RemoveAt(i);
			}
			else
				i++;
		}
	}
}


void CLayoutObject::FindRightNeighbours()
{
	register int i, j;


	if ( ! LocLeft.GetSize())
		return;
	int	  nStartIndex = -1, nEndIndex = -1;
	float fLeft		  = 0.0f;
	for (j = LocLeft[m_nIndex] + 1; j < SortLeft.GetSize(); j++)	// check if old right neighbour is the only new right neighbour
	{
		if (SortLeft[j]->GetLeft() >= m_fObjectCenterX)
		{
			fLeft = SortLeft[j]->GetLeft();
			nEndIndex = nStartIndex = j;
			break;
		}
	}
	for (j = j + 1; j < SortLeft.GetSize(); j++)
	{
		if (SortLeft[j]->GetLeft() == fLeft)
			nEndIndex++;
		else
			break;
	}

	CLayoutObject *pTestObject;
	if (m_RightNeighbours.GetSize(TRUE))
		pTestObject = m_RightNeighbours.CArray<CLayoutObject*, CLayoutObject*>::GetAt(0);
	else
		pTestObject = NULL;

	if (!pTestObject)
		if (nEndIndex == -1)
			return;
		else ;
	else
	{
		int nLoc = LocLeft[pTestObject->m_nIndex];
		if ((nLoc >= nStartIndex) && (nLoc <= nEndIndex))
			if ((pTestObject->GetBottom() <= GetBottom()) && (pTestObject->GetTop() >= GetTop()))
			{
				m_RightNeighbours.SetSize(1);	// m_RightNeighbours[0] is the only right neighbour 
				return;							// Before this analyses there could be more than one -> so truncate the array
			}
			m_RightNeighbours.RemoveAll();
	}


	// 'this' LayoutObject could got new neighbours or old ones are no longer valid
  
	for (j = LocLeft[m_nIndex] + 1; j < SortLeft.GetSize(); j++) // Search StartIndex in SortLeft list
	{
		if (SortLeft[j]->GetLeft() >= m_fObjectCenterX)
			break;
	}
	nStartIndex = j;

	BOOL bFound, bTmpNeighbour = FALSE;
	for (i = nStartIndex; i < SortLeft.GetSize(); i++)
	{
		if (!OverlappingInY(SortLeft[i]))
			continue;

		// Y-overlapping object is right from 'this' one
		pTestObject = SortLeft[i];
		if (OverlappingInX(pTestObject))
			if ((GetTop() < pTestObject->m_fObjectCenterY) || (GetBottom() > pTestObject->m_fObjectCenterY))
			{
				pTestObject->m_nAnalyseFlag = TRUE;	// Not yet a neighbour, because Y overlapping has to exceed 
				bTmpNeighbour = TRUE;			// the center if we have an X overlapping as well.        
			} 									// Nevertheless the object will be taken into account,    
												// because probably it is a potential right neighbour     
		bFound = FALSE;							// of the following pTestObject.                          
		for (j = 0; j < m_RightNeighbours.GetSize(TRUE); j++)
		{
			if (m_RightNeighbours[j]->IsPotentialLeftNeighbour(pTestObject))	
			{																
				bFound = TRUE; break;										
			}																
		}																	

		if (!bFound)
		{
			m_RightNeighbours.Add(pTestObject);

			if ((pTestObject->GetBottom() <= GetBottom()) && (pTestObject->GetTop() >= GetTop()))
				break;
		}
	}

	if (bTmpNeighbour)  // remove temporary neighbours
	{
		i = 0;
		while (i < m_RightNeighbours.GetSize(TRUE))
		{
			if (m_RightNeighbours[i]->m_nAnalyseFlag)
			{
				m_RightNeighbours[i]->m_nAnalyseFlag = 0;
				m_RightNeighbours.RemoveAt(i);
			}
			else
				i++;
		}
	}
}


void CLayoutObject::FindLowerNeighbours()
{
	register int i, j;


	if ( ! LocTop.GetSize())
		return;
	int	  nStartIndex = -1, nEndIndex = -1;
	float fTop		  = 0.0f;
	for (j = LocTop[m_nIndex] - 1; j >= 0; j--)	// check if old lower neighbour is the only new lower neighbour
	{
		if (SortTop[j]->GetTop() <= m_fObjectCenterY)
		{
			fTop = SortTop[j]->GetTop();
			nEndIndex = nStartIndex = j;
			break;
		}
	}
	for (j = j - 1; j >= 0; j--)
	{
		if (SortTop[j]->GetTop() == fTop)
			nStartIndex--;
		else
			break;
	}

	CLayoutObject *pTestObject;
	if (m_LowerNeighbours.GetSize(TRUE))
		pTestObject = m_LowerNeighbours.CArray<CLayoutObject*, CLayoutObject*>::GetAt(0);
	else
		pTestObject = NULL;

	if (!pTestObject)
		if (nEndIndex == -1)
			return;
		else ;
	else
	{
		int nLoc = LocTop[pTestObject->m_nIndex];
		if ((nLoc >= nStartIndex) && (nLoc <= nEndIndex))
			if ((pTestObject->GetLeft() <= GetLeft()) && (pTestObject->GetRight() >= GetRight()))
			{
				m_LowerNeighbours.SetSize(1);  // m_LowerNeighbours[0] is the only lower neighbour 
				return;						  // Before this analyses there could be more than one -> so truncate the array
			}
			m_LowerNeighbours.RemoveAll();
	}


	// 'this' LayoutObject could got new neighbours or old ones are no longer valid
  
	for (j = LocTop[m_nIndex] - 1; j >= 0; j--) // Search StartIndex in SortTop list
	{
		if (SortTop[j]->GetTop() <= m_fObjectCenterY)
			break;
	}
	nStartIndex = j;

	BOOL bFound, bTmpNeighbour = FALSE;
	for (i = nStartIndex; i >= 0; i--)
	{
		if (!OverlappingInX(SortTop[i]))
			continue;

		// X-overlapping object is below from 'this' one
		pTestObject = SortTop[i];
		if (OverlappingInY(pTestObject))
			if ((GetRight() < pTestObject->m_fObjectCenterX) || (GetLeft() > pTestObject->m_fObjectCenterX))
			{
				pTestObject->m_nAnalyseFlag = TRUE;	// Not yet a neighbour, because X overlapping has to exceed 
				bTmpNeighbour = TRUE;			// the center if we have an Y overlapping as well.        
			} 									// Nevertheless the object will be taken into account,    
												// because probably it is a potential upper neighbour     
		bFound = FALSE;							// of the following pTestObject.                          
		for (j = 0; j < m_LowerNeighbours.GetSize(TRUE); j++)
		{
			if (m_LowerNeighbours[j]->IsPotentialUpperNeighbour(pTestObject))	
			{																
				bFound = TRUE; break;										
			}																
		}																	

		if (!bFound)
		{
			m_LowerNeighbours.Add(pTestObject);

			if ((pTestObject->GetLeft() <= GetLeft()) && (pTestObject->GetRight() >= GetRight()))
				break;
		}
	}

	if (bTmpNeighbour)  // remove temporary neighbours
	{
		i = 0;
		while (i < m_LowerNeighbours.GetSize(TRUE))
		{
			if (m_LowerNeighbours[i]->m_nAnalyseFlag)
			{
				m_LowerNeighbours[i]->m_nAnalyseFlag = 0;
				m_LowerNeighbours.RemoveAt(i);
			}
			else
				i++;
		}
	}
}


void CLayoutObject::FindUpperNeighbours()
{
	register int i, j;


	if ( ! LocBottom.GetSize())
		return;
	int	  nStartIndex = -1, nEndIndex = -1;
	float fBottom	  = 0.0f;
	for (j = LocBottom[m_nIndex] + 1; j < SortBottom.GetSize(); j++)	// check if old upper neighbour is the only new upper neighbour
	{
		if (SortBottom[j]->GetBottom() >= m_fObjectCenterY)
		{
			fBottom = SortBottom[j]->GetBottom();
			nEndIndex = nStartIndex = j;
			break;
		}
	}
	for (j = j + 1; j < SortBottom.GetSize(); j++)
	{
		if (SortBottom[j]->GetBottom() == fBottom)
			nEndIndex++;
		else
			break;
	}

	CLayoutObject *pTestObject;
	if (m_UpperNeighbours.GetSize(TRUE))
		pTestObject = m_UpperNeighbours.CArray<CLayoutObject*, CLayoutObject*>::GetAt(0);
	else
		pTestObject = NULL;

	if (!pTestObject)
		if (nEndIndex == -1)
			return;
		else ;
	else
	{
		int nLoc = LocBottom[pTestObject->m_nIndex];
		if ((nLoc >= nStartIndex) && (nLoc <= nEndIndex))
			if ((pTestObject->GetLeft() <= GetLeft()) && (pTestObject->GetRight() >= GetRight()))
			{
				m_UpperNeighbours.SetSize(1);	// m_UpperNeighbours[0] is the only upper neighbour 
				return;							// Before this analyses there could be more than one -> so truncate the array
			}
			m_UpperNeighbours.RemoveAll();
	}


	// 'this' LayoutObject could got new neighbours or old ones are no longer valid
  
	for (j = LocBottom[m_nIndex] + 1; j < SortBottom.GetSize(); j++) // Search StartIndex in SortBottom list
	{
		if (SortBottom[j]->GetBottom() >= m_fObjectCenterY)
			break;
	}
	nStartIndex = j;

	BOOL bFound, bTmpNeighbour = FALSE;
	for (i = nStartIndex; i < SortBottom.GetSize(); i++)
	{
		if (!OverlappingInX(SortBottom[i]))
			continue;

		// X-overlapping object is above from 'this' one
		pTestObject = SortBottom[i];
		if (OverlappingInY(pTestObject))
			if ((GetRight() < pTestObject->m_fObjectCenterX) || (GetLeft() > pTestObject->m_fObjectCenterX))
			{
				pTestObject->m_nAnalyseFlag = TRUE;	// Not yet a neighbour, because X overlapping has to exceed 
				bTmpNeighbour = TRUE;			// the center if we have an Y overlapping as well.        
			} 									// Nevertheless the object will be taken into account,    
												// because probably it is a potential lower neighbour     
		bFound = FALSE;							// of the following pTestObject.                          
		for (j = 0; j < m_UpperNeighbours.GetSize(TRUE); j++)
		{
			if (m_UpperNeighbours[j]->IsPotentialLowerNeighbour(pTestObject))	
			{																
				bFound = TRUE; break;										
			}																
		}																	

		if (!bFound)
		{
			m_UpperNeighbours.Add(pTestObject);

			if ((pTestObject->GetLeft() <= GetLeft()) && (pTestObject->GetRight() >= GetRight()))
				break;
		}
	}

	if (bTmpNeighbour)  // remove temporary neighbours
	{
		i = 0;
		while (i < m_UpperNeighbours.GetSize(TRUE))
		{
			if (m_UpperNeighbours[i]->m_nAnalyseFlag)
			{
				m_UpperNeighbours[i]->m_nAnalyseFlag = 0;
				m_UpperNeighbours.RemoveAt(i);
			}
			else
				i++;
		}
	}
}


inline BOOL CLayoutObject::IsPotentialLeftNeighbour(CLayoutObject* pObject)
{
	if (GetBottom() >= pObject->GetTop()) return FALSE;
	else
		if (GetTop() <= pObject->GetBottom()) return FALSE;
		else
			if (GetRight() > pObject->GetLeft()/*m_fObjectCenterX*/) return FALSE;
			else
				return TRUE;
}


inline BOOL CLayoutObject::IsPotentialRightNeighbour(CLayoutObject* pObject)
{
	if (GetBottom() >= pObject->GetTop()) return FALSE;
	else
		if (GetTop() <= pObject->GetBottom()) return FALSE;
		else
			if (GetLeft() < pObject->GetRight()/*m_fObjectCenterX*/) return FALSE;
			else
				return TRUE;
}


inline BOOL CLayoutObject::IsPotentialLowerNeighbour(CLayoutObject* pObject)
{
	if (GetLeft() >= pObject->GetRight()) return FALSE;
	else
		if (GetRight() <= pObject->GetLeft()) return FALSE;
		else
			if (GetTop() > pObject->GetBottom()/*m_fObjectCenterY*/) return FALSE;
			else
				return TRUE;
}


inline BOOL CLayoutObject::IsPotentialUpperNeighbour(CLayoutObject* pObject)
{
	if (GetLeft() >= pObject->GetRight()) return FALSE;
	else
		if (GetRight() <= pObject->GetLeft()) return FALSE;
		else
			if (GetBottom() < pObject->GetTop()/*m_fObjectCenterY*/) return FALSE;
			else
				return TRUE;
}


inline BOOL CLayoutObject::OverlappingInX(CLayoutObject* pObject)
{
	if ((GetLeft() - pObject->GetRight()) > -0.01) return FALSE;
	else
		if ((GetRight() - pObject->GetLeft()) < 0.01) return FALSE;
		else
			return TRUE;
}


inline BOOL CLayoutObject::OverlappingInY(CLayoutObject* pObject)
{
	if ((GetBottom() - pObject->GetTop()) > -0.01) return FALSE;
	else
		if ((GetTop() - pObject->GetBottom()) < 0.01) return FALSE;
		else
			return TRUE;
}



BOOL CLayoutObject::HasLabelNeighbour()
{
	for (int i = 0; i < m_LeftNeighbours.GetSize(); i++)
	{
		if (m_LeftNeighbours[i]->IsFlatProduct())
			return TRUE;
	}
	for (int i = 0; i < m_UpperNeighbours.GetSize(); i++)
	{
		if (m_UpperNeighbours[i]->IsFlatProduct())
			return TRUE;
	}
	for (int i = 0; i < m_RightNeighbours.GetSize(); i++)
	{
		if (m_RightNeighbours[i]->IsFlatProduct())
			return TRUE;
	}
	for (int i = 0; i < m_LowerNeighbours.GetSize(); i++)
	{
		if (m_LowerNeighbours[i]->IsFlatProduct())
			return TRUE;
	}

	return FALSE;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
// Compare functions for quicksort procedure

int CLayoutObject::CompareLeft(const void *p1, const void *p2)
{
	CLayoutObject* pObject1 = (CLayoutObject*)*((CLayoutObject**)p1);
	CLayoutObject* pObject2 = (CLayoutObject*)*((CLayoutObject**)p2);

	if (pObject1->GetLeft() < pObject2->GetLeft())
		return -1;
	else
		if (pObject1->GetLeft() > pObject2->GetLeft())
			return 1;
		else
			return 0;
}


int CLayoutObject::CompareBottom(const void *p1, const void *p2)
{
	CLayoutObject* pObject1 = (CLayoutObject*)*((CLayoutObject**)p1);
	CLayoutObject* pObject2 = (CLayoutObject*)*((CLayoutObject**)p2);

	if (pObject1->GetBottom() < pObject2->GetBottom())
		return -1;
	else
		if (pObject1->GetBottom() > pObject2->GetBottom())
			return 1;
		else
			return 0;
}


int CLayoutObject::CompareRight(const void *p1, const void *p2)
{
	CLayoutObject* pObject1 = (CLayoutObject*)*((CLayoutObject**)p1);
	CLayoutObject* pObject2 = (CLayoutObject*)*((CLayoutObject**)p2);

	if (pObject1->GetRight() < pObject2->GetRight())
		return -1;
	else
		if (pObject1->GetRight() > pObject2->GetRight())
			return 1;
		else
			return 0;
}


int CLayoutObject::CompareTop(const void *p1, const void *p2)
{
	CLayoutObject* pObject1 = (CLayoutObject*)*((CLayoutObject**)p1);
	CLayoutObject* pObject2 = (CLayoutObject*)*((CLayoutObject**)p2);

	if (pObject1->GetTop() < pObject2->GetTop())
		return -1;
	else
		if (pObject1->GetTop() > pObject2->GetTop())
			return 1;
		else
			return 0;
}

void CLayoutObject::CalculateBorder(int nDirection)
{
	int	  i;
	float fDistance = FLT_MAX;
	float fOppositeBorder;

	switch (nDirection)
	{
	case LEFT:
		if (!m_LeftNeighbours.GetSize())
		{
			m_fLeftBorder = GetLeft() - GetLayoutSide()->m_fPaperLeft;
			if (m_fLeftBorder < 0.0f)
				m_fLeftBorder = 0.0f;
			m_nLockBorder &= ~LEFT;
		}
		else
		{
			for (i = 0; i < m_LeftNeighbours.GetSize(); i++)
			{
				if ((GetLeft() - m_LeftNeighbours[i]->GetRight()) < fDistance)
				{
					fDistance		= GetLeft() - m_LeftNeighbours[i]->GetRight();
					fOppositeBorder	= m_LeftNeighbours[i]->m_fRightBorder;
				}
			}
			if (m_nLockBorder & LEFT)
				if (fabs(fOppositeBorder + m_fLeftBorder - fDistance) > 0.009)
					m_nLockBorder &= ~LEFT;
				else
					return;

			m_fLeftBorder = fDistance/2.0f;
		}
		break;

	case RIGHT: 
		if (!m_RightNeighbours.GetSize())
		{
			m_fRightBorder = GetLayoutSide()->m_fPaperRight - GetRight();
			if (m_fRightBorder < 0.0f)
				m_fRightBorder = 0.0f;
			m_nLockBorder &= ~RIGHT;
		}
		else
		{
			for (i = 0; i < m_RightNeighbours.GetSize(); i++)
			{
				if ((m_RightNeighbours[i]->GetLeft() - GetRight()) < fDistance)
				{
					fDistance		= m_RightNeighbours[i]->GetLeft() - GetRight();
					fOppositeBorder	= m_RightNeighbours[i]->m_fLeftBorder;
				}
			}
			if (m_nLockBorder & RIGHT)
				if (fabs(fOppositeBorder + m_fRightBorder - fDistance) > 0.009)
					m_nLockBorder &= ~RIGHT;
				else
					return;

			m_fRightBorder = fDistance/2.0f;
		}
		break;

	case BOTTOM:
		if (!m_LowerNeighbours.GetSize())
		{
			m_fLowerBorder = GetBottom() - GetLayoutSide()->m_fPaperBottom;
			if (m_fLowerBorder < 0.0f)
				m_fLowerBorder = 0.0f;
			m_nLockBorder &= ~BOTTOM;
		}
		else
		{
			for (i = 0; i < m_LowerNeighbours.GetSize(); i++)
			{
				if ((GetBottom() - m_LowerNeighbours[i]->GetTop()) < fDistance)
				{
					fDistance		= GetBottom() - m_LowerNeighbours[i]->GetTop();
					fOppositeBorder	= m_LowerNeighbours[i]->m_fUpperBorder;
				}
			}
			if (m_nLockBorder & BOTTOM)
				if (fabs(fOppositeBorder + m_fLowerBorder - fDistance) > 0.009)
					m_nLockBorder &= ~BOTTOM;
				else
					return;

			m_fLowerBorder = fDistance/2.0f;
		}
		break;

	case TOP:
		if (!m_UpperNeighbours.GetSize())
		{
			m_fUpperBorder = GetLayoutSide()->m_fPaperTop - GetTop();
			if (m_fUpperBorder < 0.0f)
				m_fUpperBorder = 0.0f;
			m_nLockBorder &= ~TOP;
		}
		else
		{
			for (i = 0; i < m_UpperNeighbours.GetSize(); i++)
			{
				if ((m_UpperNeighbours[i]->GetBottom() - GetTop()) < fDistance)
				{
					fDistance		= m_UpperNeighbours[i]->GetBottom() - GetTop();
					fOppositeBorder	= m_UpperNeighbours[i]->m_fLowerBorder;
				}
			}
			if (m_nLockBorder & TOP)
				if (fabs(fOppositeBorder + m_fUpperBorder - fDistance) > 0.009)
					m_nLockBorder &= ~TOP;
				else
					return;

			m_fUpperBorder = fDistance/2.0f;
		}
		break;
	}
}

CLayoutObject* CLayoutObject::FindCounterpartObject()
{
	if (this == NULL)
		return NULL;

	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return NULL;

	CLayoutSide* pLayoutSide	= GetLayoutSide();
	CLayoutSide* pOppLayoutSide = (pLayoutSide->m_nLayoutSide == FRONTSIDE) ? &pLayout->m_BackSide : &pLayout->m_FrontSide;

	if ( ! pLayoutSide || ! pOppLayoutSide)
		return NULL;

	if (m_nType == Page)
	{
		if ( ! IsFlatProduct())
		{
			POSITION pos = pOppLayoutSide->m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pOppLayoutSide->m_ObjectList.GetNext(pos);
				if ( ! rObject.IsFlatProduct() && (rObject.m_nType == Page) )
					if ( (rObject.m_nComponentRefIndex == m_nComponentRefIndex) && (rObject.m_nLayerPageIndex == m_nLayerPageIndex) )
						return &rObject;
			}
		}
		else
		{
			POSITION pos = pOppLayoutSide->m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pOppLayoutSide->m_ObjectList.GetNext(pos);
				if (rObject.IsFlatProduct() && (rObject.m_nType == Page) )
					if (rObject.m_nComponentRefIndex == m_nComponentRefIndex)
						return &rObject;
			}
		}
	}
	else
		return FindCounterpartControlMark();

	return NULL;
}

CLayoutObject* CLayoutObject::FindCounterpartControlMark()
{
	float fCounterpartLeft, fCounterpartRight, fCounterpartTop, fCounterpartBottom;
	CLayoutObject* pCounterpartObject = NULL;

	if (this == NULL)
		return pCounterpartObject;

	CLayoutSide* pLayoutSide = GetLayoutSide();
	if ( ! pLayoutSide)
		return NULL;

	if ( ! m_reflinePosParams.IsUndefined() && (m_reflinePosParams.m_nPlaceOn == CReflinePositionParams::PlaceOnBothSamePos) )
	{
		switch (GetLayout()->KindOfProduction())
		{
		case WORK_AND_TURN  : 
		case WORK_AND_TUMBLE: fCounterpartLeft	 = GetLeft()	+ m_reflinePosParams.GetCounterpartOffsetX(this);
							  fCounterpartRight	 = GetRight()	+ m_reflinePosParams.GetCounterpartOffsetX(this);
							  fCounterpartTop	 = GetTop()		+ m_reflinePosParams.GetCounterpartOffsetY(this);
							  fCounterpartBottom = GetBottom()	+ m_reflinePosParams.GetCounterpartOffsetY(this);
							  break;
		}
		// search for samepos counterpart
		pCounterpartObject = FindCounterPart(pLayoutSide->m_nLayoutSide, fCounterpartLeft, fCounterpartBottom, fCounterpartRight, fCounterpartTop, m_strFormatName);
		if (pCounterpartObject)
			pCounterpartObject->m_reflinePosParams.m_nPlaceOn = CReflinePositionParams::PlaceOnBothSamePos;	// find at samepos -> define
	}
	else
	{
		switch (GetLayout()->KindOfProduction())
		{
		case WORK_AND_TURN  : fCounterpartLeft	 = pLayoutSide->m_fPaperRight - GetLeft() + pLayoutSide->m_fPaperLeft - GetWidth();
							  fCounterpartRight	 = fCounterpartLeft + GetWidth();
							  fCounterpartTop	 = GetTop();
							  fCounterpartBottom = GetBottom();
							  break;
		// TODO : Calculation is not yet OK ????
		case WORK_AND_TUMBLE: fCounterpartBottom = pLayoutSide->m_fPaperTop - GetBottom() + pLayoutSide->m_fPaperBottom - GetHeight();
							  fCounterpartTop	 = fCounterpartBottom + GetHeight();
							  fCounterpartLeft	 = GetLeft();
							  fCounterpartRight	 = GetRight();
							  break;
		}
		// search for mirrored counterpart
		pCounterpartObject = FindCounterPart(pLayoutSide->m_nLayoutSide, fCounterpartLeft, fCounterpartBottom, fCounterpartRight, fCounterpartTop, m_strFormatName);
		if (pCounterpartObject)
			m_reflinePosParams.m_nPlaceOn = pCounterpartObject->m_reflinePosParams.m_nPlaceOn = CReflinePositionParams::PlaceOnBothMirror;	// find at mirrored pos -> define
	}

	if ( ! pCounterpartObject)
	{
		fCounterpartLeft	= GetLeft()		+ m_reflinePosParams.GetCounterpartOffsetX(this);
		fCounterpartRight	= GetRight()	+ m_reflinePosParams.GetCounterpartOffsetX(this);
		fCounterpartTop		= GetTop()		+ m_reflinePosParams.GetCounterpartOffsetY(this);
		fCounterpartBottom	= GetBottom()	+ m_reflinePosParams.GetCounterpartOffsetY(this);
		pCounterpartObject	= FindCounterPart(pLayoutSide->m_nLayoutSide, fCounterpartLeft, fCounterpartBottom, fCounterpartRight, fCounterpartTop, m_strFormatName);	// search for samepos counterpart
		if (pCounterpartObject)
			m_reflinePosParams.m_nPlaceOn = pCounterpartObject->m_reflinePosParams.m_nPlaceOn = CReflinePositionParams::PlaceOnBothSamePos;	// find at same pos -> define
		else
		{
			switch (GetLayoutSideIndex())
			{
			case FRONTSIDE:	m_reflinePosParams.m_nPlaceOn = CReflinePositionParams::PlaceOnFront;	break;
			case BACKSIDE:	m_reflinePosParams.m_nPlaceOn = CReflinePositionParams::PlaceOnBack; 	break;
			default:		m_reflinePosParams.m_nPlaceOn = -1; break;
			}
		}
	}

	return pCounterpartObject;
}

CLayoutObject* CLayoutObject::FindCounterPart(int nSide, float fCounterpartLeft, float fCounterpartBottom, float fCounterpartRight, float fCounterpartTop, CString strFormatName)
{
	switch(nSide)
	{
	case FRONTSIDE :	
		{
			CLayout* pLayout = GetLayout();
			POSITION pos = pLayout->m_BackSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pLayout->m_BackSide.m_ObjectList.GetNext(pos);
				if ( (fabs(fCounterpartLeft		- rObject.GetLeft())	< 0.01) &&
					 (fabs(fCounterpartRight	- rObject.GetRight())	< 0.01) &&
					 (fabs(fCounterpartTop		- rObject.GetTop())		< 0.01) &&
					 (fabs(fCounterpartBottom	- rObject.GetBottom())	< 0.01) )
					{
						if ( ! IsFlatProduct())
						{
							if (strFormatName.IsEmpty())
								return &rObject;
							else
								if (strFormatName == rObject.m_strFormatName)
									return &rObject;
						}
						else
							//if (m_nComponentRefIndex == rObject.m_nComponentRefIndex)
								return &rObject;
					}
			}
		 }
		 break;

	case BACKSIDE  : 
		{
			CLayout* pLayout = GetLayout();
			POSITION pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
				if ( (fabs(fCounterpartLeft		- rObject.GetLeft())	< 0.01) &&
					 (fabs(fCounterpartRight	- rObject.GetRight())	< 0.01) &&
					 (fabs(fCounterpartTop		- rObject.GetTop())		< 0.01) &&
					 (fabs(fCounterpartBottom	- rObject.GetBottom())	< 0.01) )
					{
						if ( ! IsFlatProduct())
						{
							if (strFormatName.IsEmpty())
								return &rObject;
							else
								if (strFormatName == rObject.m_strFormatName)
									return &rObject;
						}
						else
							//if (m_nComponentRefIndex == rObject.m_nComponentRefIndex)
								return &rObject;
					}
			}
		 }
		 break;
	}

	return NULL;
}

int CLayoutObject::FindCounterpartClickSide(int n_Side)
{
	switch (GetLayout()->KindOfProduction())
	{
	case WORK_AND_TURN  : if (n_Side == LEFT)
							 n_Side = RIGHT;
						  else
							 if (n_Side == RIGHT)
								n_Side = LEFT;
						  break;
	case WORK_AND_TUMBLE: if (n_Side == TOP)
							 n_Side = BOTTOM;
						  else
							 if (n_Side == BOTTOM)
								n_Side = TOP;
						  break;
	}
	return n_Side;
}

// Aligns this object to the Object (where pObject points to) on the opposite layout side
void CLayoutObject::AlignToCounterPart(CLayoutObject* pOppObject)
{
	if (!pOppObject)
		return;

	CLayoutSide* pLayoutSide	= GetLayoutSide();
	CLayoutSide* pOppLayoutSide = pOppObject->GetLayoutSide();
	if ( ! pLayoutSide || ! pOppLayoutSide)
		return;

	int nKindOfProduction = (GetLayout()) ? GetLayout()->KindOfProduction() : WORK_AND_TURN;

	if ( (m_nType == ControlMark) && (m_reflinePosParams.m_nPlaceOn == CReflinePositionParams::PlaceOnBothSamePos) )
	{
		switch (nKindOfProduction)
		{
		case WORK_AND_TURN  :	
		case WORK_AND_TUMBLE:	SetLeft	 (pOppObject->GetLeft()		+ pOppObject->m_reflinePosParams.GetCounterpartOffsetX(pOppObject));
								SetRight (pOppObject->GetRight()	+ pOppObject->m_reflinePosParams.GetCounterpartOffsetX(pOppObject));
								SetBottom(pOppObject->GetBottom()	+ pOppObject->m_reflinePosParams.GetCounterpartOffsetY(pOppObject));
								SetTop	 (pOppObject->GetTop()		+ pOppObject->m_reflinePosParams.GetCounterpartOffsetY(pOppObject));
								m_nHeadPosition = pOppObject->m_nHeadPosition;
								m_reflinePosParams = pOppObject->m_reflinePosParams;
								break;
		}
	}
	else
	{
		switch (nKindOfProduction)
		{
		case WORK_AND_TURN  :	SetLeft	 (pLayoutSide->m_fPaperLeft   + (pOppLayoutSide->m_fPaperRight - pOppObject->GetRight()));
								SetRight (GetLeft() + pOppObject->GetWidth());
								SetBottom(pLayoutSide->m_fPaperBottom + (pOppObject->GetBottom() - pOppLayoutSide->m_fPaperBottom));
								SetTop	 (GetBottom() + pOppObject->GetHeight());
								switch (pOppObject->m_nHeadPosition)
								{
								case LEFT:	m_nHeadPosition = RIGHT; break;
								case RIGHT:	m_nHeadPosition = LEFT;	 break;
								default:	m_nHeadPosition = pOppObject->m_nHeadPosition; break;
								}
								switch (pOppObject->m_nIDPosition)
								{
								case LEFT:	m_nIDPosition = RIGHT; break;
								case RIGHT:	m_nIDPosition = LEFT;	 break;
								default:	m_nIDPosition = pOppObject->m_nIDPosition; break;
								}
								break;
		case WORK_AND_TUMBLE:	SetBottom(pLayoutSide->m_fPaperTop - (pOppObject->GetTop() - pOppLayoutSide->m_fPaperBottom));
								SetTop	 (GetBottom() + pOppObject->GetHeight());
								SetLeft	 (pOppObject->GetLeft());
								SetRight (pOppObject->GetRight());
								switch (pOppObject->m_nHeadPosition)
								{
								case TOP:	 m_nHeadPosition = BOTTOM; break;
								case BOTTOM: m_nHeadPosition = TOP;	   break;
								default:	 m_nHeadPosition = pOppObject->m_nHeadPosition; break;
								}
								switch (pOppObject->m_nIDPosition)
								{
								case LEFT:	m_nIDPosition = RIGHT; break;
								case RIGHT:	m_nIDPosition = LEFT;	 break;
								default:	m_nIDPosition = pOppObject->m_nIDPosition; break;
								}
								break;
		}
	}

	SetWidth(pOppObject->GetWidth());
	SetHeight(pOppObject->GetHeight());
}

