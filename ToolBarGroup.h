#pragma once


// CToolBarGroup

class CToolBarGroup : public CStatic
{
	DECLARE_DYNAMIC(CToolBarGroup)

public:
	CToolBarGroup();
	virtual ~CToolBarGroup();

public:
	CFont m_font;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void DrawItem(LPDRAWITEMSTRUCT);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};


