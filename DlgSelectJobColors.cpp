// DlgSelectJobColors.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgSelectJobColors.h"
#include "DlgJobColorDefinitions.h"




// CDlgSelectJobColors-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgSelectJobColors, CDialog)

CDlgSelectJobColors::CDlgSelectJobColors(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSelectJobColors::IDD, pParent)
{
	m_pPageSource = NULL;
}

CDlgSelectJobColors::~CDlgSelectJobColors()
{
	if (m_pPageSource)
	{
		delete m_pPageSource;
		m_pPageSource = NULL;
	}
}

void CDlgSelectJobColors::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgSelectJobColors, CDialog)
	ON_BN_CLICKED(IDC_OD_ADD_COLOR, OnOdAddColor)
	ON_BN_CLICKED(IDC_OD_COLOR_SINGLE, OnOdColorSingle)
	ON_BN_CLICKED(IDC_OD_COLOR_CMYK, OnOdColorCmyk)
	ON_BN_CLICKED(IDC_OD_REMOVE_COLOR, OnOdRemoveColor)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SYSGENMARK_COLORLIST, OnLvnItemchangedSysgenmarkColorlist)
	ON_BN_CLICKED(IDC_OD_COLOR_IMPORT, OnBnClickedOdColorImport)
END_MESSAGE_MAP()


// CDlgSelectJobColors-Meldungshandler

BOOL CDlgSelectJobColors::OnInitDialog()
{
	CDialog::OnInitDialog();

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	m_printColorList.Initialize(this);

	for (int i = 0; i < m_colorantList.GetSize(); i++)
	{
		CColorant colorant(m_colorantList[i].m_crRGB, m_colorantList[i].m_strName);
		m_printColorList.AddColorItem(0, colorant.m_crRGB, colorant.m_strName);

	}

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}


void CDlgSelectJobColors::OnOdAddColor() 
{
	CDlgJobColorDefinitions dlg(FALSE, TRUE);	// no modify mode, hide composite button

	for (int i = 0; i < m_colorantList.GetSize(); i++)
	{
		CColorDefinition colorDef;
		colorDef.m_strColorName = m_colorantList[i].m_strName;
		colorDef.m_rgb			= m_colorantList[i].m_crRGB;
		colorDef.m_nColorIndex	= 0;
		dlg.m_PSColorDefTable.InsertColorDef(colorDef);
	}

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_colorantList.RemoveAll();
	m_printColorList.DeleteAllItems();
	m_printColorList.m_printColorListItems.RemoveAll();
	for (int i = 0; i < dlg.m_PSColorDefTable.GetSize(); i++)
	{
		m_colorantList.Add(CColorant(dlg.m_PSColorDefTable[i].m_rgb, dlg.m_PSColorDefTable[i].m_strColorName));
		m_printColorList.AddColorItem(0, dlg.m_PSColorDefTable[i].m_rgb, dlg.m_PSColorDefTable[i].m_strColorName);
	}

	UpdateData(FALSE);
}

void CDlgSelectJobColors::OnOdColorSingle() 
{
	m_colorantList.RemoveAll();
	m_colorantList.Add(CColorant(BLACK, "Black"));
	m_printColorList.DeleteAllItems();
	m_printColorList.m_printColorListItems.RemoveAll();
	m_printColorList.AddColorItem(0, m_colorantList[0].m_crRGB, m_colorantList[0].m_strName);

	UpdateData(FALSE);
}

void CDlgSelectJobColors::OnOdColorCmyk() 
{
	m_colorantList.RemoveAll();
	m_colorantList.Add(CColorant(CYAN,		"Cyan"));
	m_colorantList.Add(CColorant(MAGENTA,	"Magenta"));
	m_colorantList.Add(CColorant(YELLOW,	"Yellow"));
	m_colorantList.Add(CColorant(BLACK,		"Black"));

	m_printColorList.DeleteAllItems();
	m_printColorList.m_printColorListItems.RemoveAll();
	for (int i = 0; i < m_colorantList.GetSize(); i++)
		m_printColorList.AddColorItem(0, m_colorantList[i].m_crRGB, m_colorantList[i].m_strName);

	UpdateData(FALSE);
}

void CDlgSelectJobColors::OnOdRemoveColor() 
{
	int		 nOffset = 0;
	POSITION pos	 = m_printColorList.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nCurSel = m_printColorList.GetNextSelectedItem(pos);
		if (nCurSel != -1)
		{
			nCurSel -= nOffset;
			m_colorantList.RemoveAt(nCurSel);
			m_printColorList.m_printColorListItems.RemoveAt(nCurSel);
			nOffset++;
		}
	}

	m_printColorList.DeleteAllItems();
	for (int i = 0; i < m_colorantList.GetSize(); i++)
		m_printColorList.AddColorItem(0, m_colorantList[i].m_crRGB, m_colorantList[i].m_strName);

	UpdateData(FALSE);
}

void CDlgSelectJobColors::OnLvnItemchangedSysgenmarkColorlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	GetDlgItem(IDC_OD_REMOVE_COLOR)->EnableWindow( (pNMLV->uNewState & LVIS_SELECTED) );

	*pResult = 0;
}

void CDlgSelectJobColors::OnBnClickedOdColorImport()
{
	UpdateData(TRUE);

	CString strInitialFolder;
	if (CImpManDoc::GetDoc()->m_PageSourceList.IsEmpty())	// Take path from settings
		strInitialFolder = theApp.settings.m_szPDFInputFolder;
	else
	{														// Take path from last page source
		CPageSource& rPageSource  = CImpManDoc::GetDoc()->m_PageSourceList.GetTail();
		TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
		_tsplitpath((LPCTSTR)rPageSource.m_strDocumentFullpath, szDrive, szDir, szFname, szExt);
		strInitialFolder  = szDrive;
		strInitialFolder += szDir;
		CFileStatus status;
		if ( ! CFile::GetStatus(strInitialFolder + _T("."), status))	// if folder not present, take from settings
			strInitialFolder = theApp.settings.m_szPDFInputFolder;
	}

	if ( ! m_pPageSource)
		m_pPageSource = new CPageSource;
	else
		m_pPageSource->m_PageSourceHeaders.RemoveAll();

	theApp.m_pDlgPDFLibStatus->m_pPageSource = m_pPageSource;
	theApp.m_pDlgPDFLibStatus->LaunchPDFEngine(strInitialFolder, this, CDlgPDFLibStatus::RegisterPages, _T(""), TRUE);
}

void CDlgSelectJobColors::PDFEngineDataExchange(CPageSource* pNewObjectSource)
{
//	if ( ! m_pPageSource)
//		return;
//
//	m_colorantList.RemoveAll();
//	OnOdColorCmyk();
//
////	m_strPDFDocFullpath = pNewObjectSource->m_strDocumentFullpath;
//
//	for (int i = 0; i < pNewObjectSource->m_PageSourceHeaders.GetSize(); i++)
//	{
//		for (int j = 0; j < pNewObjectSource->m_PageSourceHeaders[i].m_spotColors.GetSize(); j++)
//		{
//			CString strColorName = pNewObjectSource->m_PageSourceHeaders[i].m_spotColors[j];
//			if ( ! m_printColorList.GetColorItem(strColorName))
//			{
//				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
//				m_colorantList.Add(CColorant((pColorDef) ? pColorDef->m_rgb : WHITE, strColorName));
//				m_printColorList.AddColorItem(i, (pColorDef) ? pColorDef->m_rgb : WHITE, strColorName);
//			}
//		}
//	}
//
//	UpdateData(FALSE); 
//
//	if (m_pPageSource)
//		if (m_pPageSource->m_PageSourceHeaders.GetSize())
//		{
//			CImpManDoc*  pDoc = CImpManDoc::GetDoc();
//			if (pDoc)
//			{
//				pDoc->m_PageSourceList.AddTail(*m_pPageSource);
//				pDoc->m_PageTemplateList.AutoAssignPageSource(&pDoc->m_PageSourceList.GetTail(), m_nProductPartIndex);
//				pDoc->m_PageSourceList.InitializePageTemplateRefs();
//				pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
//				pDoc->m_PrintSheetList.ReorganizeColorInfos();
//			}
//			m_pPageSource->m_PageSourceHeaders.RemoveAll();
//		}
//
//
	theApp.m_pDlgPDFLibStatus->m_pPageSource = NULL;
}
