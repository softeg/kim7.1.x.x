#if !defined(AFX_OBJECTPIVOTCONTROL_H__138D2F11_9569_11D5_8892_0040F68C1EF7__INCLUDED_)
#define AFX_OBJECTPIVOTCONTROL_H__138D2F11_9569_11D5_8892_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ObjectPivotControl.h : Header-Datei
//

#define ID_CHANGED_PIVOT 64000



/////////////////////////////////////////////////////////////////////////////
// Fenster CObjectPivotControl 

class CObjectPivotControl : public CStatic
{
// Konstruktion
public:
	CObjectPivotControl();



// Attribute
public:
	CDialog*	m_pParent;
	int			m_nXRef;
	int			m_nYRef;
	CBitmap		m_bmObjectPivot1;
	CBitmap		m_bmObjectPivot2;
	CBitmap		m_bmObjectPivot3;
	CBitmap		m_bmObjectPivot4;
	CBitmap		m_bmObjectPivot5;
	CBitmap		m_bmObjectPivot6;
	CBitmap		m_bmObjectPivot7;
	CBitmap		m_bmObjectPivot8;
	CBitmap		m_bmObjectPivot9;
	CBitmap		m_bmObjectPivoth1;
	CBitmap		m_bmObjectPivoth2;
	CBitmap		m_bmObjectPivoth3;
	CBitmap		m_bmObjectPivotv1;
	CBitmap		m_bmObjectPivotv2;
	CBitmap		m_bmObjectPivotv3;
	BOOL		m_bLockCheckButton;
	int			m_nCurNX;
	int			m_nCurNY;
	int			m_nActiveCell;
	BOOL		m_bHorizontalOnly;
	BOOL		m_bVerticalOnly;


// Operationen
public:
	void  LoadData(unsigned char  nXRef, unsigned char  nYRef, BOOL bHorizontalOnly = FALSE, BOOL bVerticalOnly = FALSE);
	void  SaveData(unsigned char& nXRef, unsigned char& nYRef);
	int	  GetCurrentCell();
	void  ChooseBitmap(int nCell);
	void  CheckButton(int nCell);
	void  Update();
	void  Reinit(int nIDD);
	CRect ConsolidateButtonRect(CRect rectButton, CRect rectPivot);

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CObjectPivotControl)
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~CObjectPivotControl();

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CObjectPivotControl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	//}}AFX_MSG
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_OBJECTPIVOTCONTROL_H__138D2F11_9569_11D5_8892_0040F68C1EF7__INCLUDED_
