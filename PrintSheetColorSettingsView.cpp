// PrintSheetColorSettingsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetTableView.h"
#include "PrintSheetColorSettingsView.h"
#include "PrintSheetView.h"
#include "NewPrintSheetFrame.h"
#include "PageListTableView.h"
#include "GraphicComponent.h"
#include "DlgPrintColorTable.h"


// CColorSettingsTableHeader

IMPLEMENT_DYNAMIC(CColorSettingsTableHeader, CStatic)

CColorSettingsTableHeader::CColorSettingsTableHeader()
{
	m_font.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CColorSettingsTableHeader::~CColorSettingsTableHeader()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CColorSettingsTableHeader, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CColorSettingsTableHeader::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheetColorSettingsView* pView = (CPrintSheetColorSettingsView*)GetParent();
	if ( ! pView)
		return;
	CPrintSheetColorSettingsTable* pTable = &pView->m_colorSettingsTable;
	if ( ! pTable)
		return;

	CRect rcFrame;
	GetClientRect(rcFrame);

	CGraphicComponent gp;
	switch (GetDlgCtrlID())
	{
	case IDC_PRINTVIEW_SETTINGS_TITLE_FRAME:
		{
			CString string; string.LoadString(IDS_COLOR_PREVIEW);
			CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + 20;
			gp.DrawTitleBar(&dc, rcTitle, string, RGB(242,243,240), RGB(238,235,217), 90, -1);
		}
		break;
	case IDC_PRINTVIEW_SETTINGS_HEADER_FRAME:
		{
			CPen pen(PS_SOLID, 1, LIGHTGRAY);
			dc.SelectObject(&pen);
			dc.MoveTo(rcFrame.left,  rcFrame.top);
			dc.LineTo(rcFrame.right, rcFrame.top);

			CString string; 
			gp.DrawTitleBar(&dc, rcFrame, _T(""), RGB(242,243,240), RGB(238,235,217), 90, -1);

			dc.MoveTo(rcFrame.left,  rcFrame.bottom);
			dc.LineTo(rcFrame.right, rcFrame.bottom);

			rcFrame.top += 5;

			dc.SelectObject(&m_font);
			dc.SetBkMode(TRANSPARENT);
			CString strColor;

			int nOffsetY = 0;
			for (int i = 0; i < pTable->m_ColumnRefList.GetSize(); i++)
			{
				int	nIndex = pTable->m_ColumnRefList[i].m_pColumn->m_dwData;
				if (nIndex < 0)
				{
					CRect rcText(pTable->m_ColumnRefList[i].m_pColumn->m_nColumnLeft + 5, rcFrame.bottom - 18, rcFrame.right, rcFrame.bottom);
					dc.SetTextColor(BLACK);
					CString string = pTable->m_ColumnRefList[i].m_pColumn->m_strColumnLabel;
					CPageListTableView::FitStringToWidth(&dc, string, rcText.Width());
					dc.DrawText(string, rcText, DT_LEFT | DT_TOP);
					continue;
				}

				CRect rect(CPoint(pTable->m_ColumnRefList[i].m_pColumn->m_nColumnLeft + 5, rcFrame.bottom - 16), CSize(10,10));
				CRect rcText(rect.right + 5, rcFrame.top + 1, rcFrame.right, rcFrame.top + 15);
				rcText.OffsetRect(0, nOffsetY);
				nOffsetY += 18;

				dc.SelectObject(&pen);
				if (rcText.bottom < rect.top)
				{
					dc.MoveTo(rect.CenterPoint());
					dc.LineTo(rect.CenterPoint().x, rcText.CenterPoint().y);
					dc.LineTo(rcText.left, rcText.CenterPoint().y);
				}

				BOOL bIsLinked = FALSE;
				if ( (nIndex >= 0) && (nIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
				{
					COLORREF crRGB = WHITE;
					strColor = pDoc->m_ColorDefinitionTable[nIndex].m_strColorName;
					if ( (nIndex >= 0) && (nIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
					{
						CString strMappedColor = theApp.MapPrintColor(pDoc->m_ColorDefinitionTable[nIndex].m_strColorName);
						if (strMappedColor == _T("Composite"))
						{
							strColor = pTable->m_ColumnRefList[i].m_pColumn->m_strColumnLabel;
							crRGB = theApp.m_colorDefTable.FindColorRef(strColor);
						}
						else
						{
							if (strMappedColor != pDoc->m_ColorDefinitionTable[nIndex].m_strColorName)
							{
								CRect rcText = rect; rcText.OffsetRect(-rect.Width() - 4, -1);
								dc.SetTextColor(BLACK);
								dc.DrawText(_T("="), rcText, DT_LEFT | DT_TOP);
								bIsLinked = TRUE;
							}
							crRGB = pDoc->m_ColorDefinitionTable[nIndex].m_rgb;
						}

						dc.FillSolidRect(rect, crRGB);
						dc.SelectStockObject(BLACK_PEN);
						dc.SelectStockObject(HOLLOW_BRUSH);
						dc.Rectangle(rect);
						if (bIsLinked)
							CPrintSheetView::FillTransparent(&dc, rect, WHITE, TRUE, 150);
					}
				}
				
				dc.SetTextColor((bIsLinked) ? DARKGRAY : BLACK);
				CPageListTableView::FitStringToWidth(&dc, strColor, rcText.Width());
				dc.DrawText(strColor, rcText, DT_LEFT | DT_TOP);
			}

			pen.DeleteObject();
		}
		break;
	}
}


//////////////////////////////////

void CPrintSheetColorSettingsTable::Create(CPrintSheetColorSettingsView* pParent)
{
	m_pParent	  = pParent;
	m_nWhatToShow = UnInitialized;
}

void CPrintSheetColorSettingsTable::Initialize(int nWhatToShow)
{
	//if (m_nWhatToShow == nWhatToShow)	// already initialized
	//	return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CRect rcTitleFrame;
	m_pParent->GetDlgItem(IDC_PRINTVIEW_SETTINGS_TITLE_FRAME)->GetWindowRect(rcTitleFrame);
	m_pParent->ScreenToClient(rcTitleFrame);
	m_pParent->GetDlgItem(IDC_PRINTVIEW_SETTINGS_HEADER_FRAME)->GetWindowRect(m_headerFrameRect);
	m_pParent->ScreenToClient(m_headerFrameRect);

	int nNumPrintedColors = 0;
	pDoc->m_ColorDefinitionTable.UpdateColorInfos();
	for (int i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
	{
		CString strMappedColor = theApp.MapPrintColor(pDoc->m_ColorDefinitionTable[i].m_strColorName);
		if (strMappedColor.CompareNoCase(_T("Composite")) == 0)
		{
			CStringArray processColors;
			pDoc->m_PrintSheetList.GetProcessColors(processColors);
			nNumPrintedColors += processColors.GetSize();
		}
		else
			nNumPrintedColors++;
	}
	int nHeaderHeight = max(20, 5 + 18 * nNumPrintedColors);	// + 5 = upper space

	m_nWhatToShow = nWhatToShow;

	for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
		delete m_ColumnRefList[i].m_pColumn;

	m_ColumnRefList.RemoveAll();

	CArray<CPrintSheetTableColumn*, CPrintSheetTableColumn*> pColumns;

	CString string;
	string.LoadString(IDS_SHEET_TEXT);
	pColumns.Add(new CColumnSheetColorSettingsPDFSheet(180, string, this, -1));

	if (pDoc)
	{
		pDoc->m_ColorDefinitionTable.UpdateColorInfos();
		for (int i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
		{
			CColorDefinition& rColorDef = pDoc->m_ColorDefinitionTable[i];
			CString strMappedColor = theApp.MapPrintColor(rColorDef.m_strColorName);
			if (strMappedColor.CompareNoCase(_T("Composite")) == 0)
			{
				CStringArray processColors;
				pDoc->m_PrintSheetList.GetProcessColors(processColors);
				for (int j = 0; j < processColors.GetSize(); j++)
					pColumns.Add(new CColumnSheetColorSettingsInfo(30, processColors[j], this, i));
			}
			else
				if (strMappedColor == rColorDef.m_strColorName)	// Basic colors 
				{
					if (pDoc->m_PageTemplateList.ColorInUse(pDoc, pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strMappedColor)) )
					{
						pColumns.Add(new CColumnSheetColorSettingsInfo(30, rColorDef.m_strColorName, this, i));
						CColorDefinition* pSystemColorDef = theApp.m_colorDefTable.FindColorDefinition(rColorDef.m_strColorName);
						if (pSystemColorDef)
						{
							POSITION pos = pSystemColorDef->m_AliasNameList.GetHeadPosition();
							while (pos)
							{
								CString strAlias = pSystemColorDef->m_AliasNameList.GetNext(pos);
								if (pDoc->m_PageTemplateList.ColorInUse(pDoc, pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strAlias)) )
									pColumns.Add(new CColumnSheetColorSettingsInfo(30, strAlias, this, pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strAlias)));
							}
						}
					}
				}
		}
	}

	for (int i = 0; i < pColumns.GetSize(); i++)
	{
		CColumnRef column(i, pColumns[i]);
		m_ColumnRefList.Add(column);
	}

	CClientDC dc(m_pParent);
	dc.SelectObject(&((CPrintSheetColorSettingsView*)m_pParent)->m_colorSettingsTableHeader.m_font);
	TEXTMETRIC tm;
	dc.GetTextMetrics(&tm); 
	m_nLineHeight = max(14, tm.tmHeight) + 6;	// 14 = sheet icon height

	int nXPos = 0;
	int nMaxX = 200;
	for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
	{
		m_ColumnRefList[i].m_pColumn->m_nColumnLeft = nXPos;
		nMaxX = max(nMaxX, nXPos + dc.GetTextExtent(m_ColumnRefList[i].m_pColumn->m_strColumnLabel).cx + 35);

		nXPos += m_ColumnRefList[i].m_pColumn->m_nColumnWidth;

		m_ColumnRefList[i].m_pColumn->m_pDisplayList = &((CPrintSheetColorSettingsView*)m_pParent)->m_DisplayList;
	}

	m_nHeaderWidth = nMaxX;

	rcTitleFrame.right = nMaxX;
	m_pParent->GetDlgItem(IDC_PRINTVIEW_SETTINGS_TITLE_FRAME)->MoveWindow(rcTitleFrame);

	m_headerFrameRect.bottom = m_headerFrameRect.top  + nHeaderHeight;
	m_headerFrameRect.right  = nMaxX;
	m_pParent->GetDlgItem(IDC_PRINTVIEW_SETTINGS_HEADER_FRAME)->MoveWindow(m_headerFrameRect);

	if (pDoc)
		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();

	((CPrintSheetColorSettingsView*)m_pParent)->OnUpdate(NULL, 0, 0);
	m_pParent->UpdateWindow();
}

int CPrintSheetColorSettingsTable::GetCellLines(CPrintSheet& rPrintSheet)
{
	if ( ! rPrintSheet.GetLayout())
		return 0;

	int nLines = 1;
	if (rPrintSheet.m_bDisplayOpen)
	{
		nLines++;

		if (m_nWhatToShow == CPrintSheetTable::ShowOutputList)
		{
			if (rPrintSheet.m_FrontSidePlates.GetCount())
			{
				if ( (rPrintSheet.m_FrontSidePlates.GetHead().m_strPlateName != "Composite") || (rPrintSheet.m_FrontSidePlates.GetCount() > 1) )
					if (rPrintSheet.m_FrontSidePlates.m_bDisplayOpen)
						nLines += rPrintSheet.m_FrontSidePlates.GetCount();	// front colors 
			}

			if ( ! rPrintSheet.GetLayout()->m_nProductType)	// if product type, we have only one side
			{
				nLines++;
				if (rPrintSheet.m_BackSidePlates.GetCount())
				{
					if ( (rPrintSheet.m_BackSidePlates.GetHead().m_strPlateName  != "Composite") || (rPrintSheet.m_BackSidePlates.GetCount() > 1) )
						if (rPrintSheet.m_BackSidePlates.m_bDisplayOpen)
							nLines += rPrintSheet.m_BackSidePlates.GetCount();	// back colors 
				}
			}
		}
		else
		{
			if ( (rPrintSheet.GetLayout()->m_FrontSide.m_ObjectList.m_bDisplayOpen))
				nLines += rPrintSheet.GetLayout()->m_FrontSide.m_ObjectList.GetSheetObjectRefList(&rPrintSheet);
			if ( ! rPrintSheet.GetLayout()->m_nProductType)	// if product type, we have only one side
			{
				nLines++;
				if (rPrintSheet.GetLayout()->m_BackSide.m_ObjectList.m_bDisplayOpen)
					nLines += rPrintSheet.GetLayout()->m_BackSide.m_ObjectList.GetSheetObjectRefList(&rPrintSheet);
			}
		}
	}

	return nLines;
}

void CColumnSheetColorSettingsPDFSheet::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	HICON	 hIconPDF		  = theApp.LoadIcon(IDI_OUT_SHEET);
	HICON	 hIconFront		  = theApp.LoadIcon(IDI_OUT_SHEET_FRONT);
	HICON	 hIconBack		  = theApp.LoadIcon(IDI_OUT_SHEET_BACK);
	HICON	 hIconPage		  = theApp.LoadIcon(IDI_SHEET_PAGE);
	HICON	 hIconCustomMark  = theApp.LoadIcon(IDI_CUSTOM_MARK);
	HICON	 hIconCropMark	  = theApp.LoadIcon(IDI_CROP_MARK);
	HICON	 hIconFoldMark	  = theApp.LoadIcon(IDI_FOLD_MARK);
	HICON	 hIconBarcodeMark = theApp.LoadIcon(IDI_BARCODE_MARK);
	HICON	 hIconDrawingMark = theApp.LoadIcon(IDI_DRAWING_MARK);
	HICON	 hIconSectionMark = theApp.LoadIcon(IDI_SECTION_MARK);
	HICON	 hIconTextMark	  = theApp.LoadIcon(IDI_TEXT_MARK);
	HICON	 hIconPlus		  = theApp.LoadIcon(IDI_PLUS);
	HICON	 hIconMinus		  = theApp.LoadIcon(IDI_MINUS);
	int		 nLine			  = 0;
	POSITION pos			  = pDoc->m_PrintSheetList.GetHeadPosition();
	CString	 strOut;
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		CLayout*	 pLayout	 = rPrintSheet.GetLayout();
		if ( ! pLayout)
			continue;

		pDC->DrawIcon(CPoint(0, nLine * m_pParent->m_nLineHeight + 5), (rPrintSheet.m_bDisplayOpen) ? hIconMinus : hIconPlus);

		pDC->DrawIcon(CPoint(12, nLine * m_pParent->m_nLineHeight + 2), hIconPDF);

		strOut.Format(_T("%s (%s)"), rPrintSheet.GetNumber(), pLayout->m_strLayoutName);
		CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 40);
		pDC->TextOut(40, GETTEXTPOS_VERT(nLine), strOut);

		CSize textExtent = pDC->GetTextExtent(strOut);
		CRect regRect(CPoint(0, nLine * m_pParent->m_nLineHeight), CSize(40 + textExtent.cx, 20));
		m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetColorSettingsView, PDFSheet), NULL, CDisplayItem::ForceRegister);

		int	nCount = 0;
		if (rPrintSheet.m_bDisplayOpen)
		{
			int nSide = -1;
			do
			{
				nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;

				nLine++;

				pDC->DrawIcon(CPoint(30, nLine * m_pParent->m_nLineHeight + 2), (nSide == FRONTSIDE) ? hIconFront : hIconBack);

				switch(pLayout->m_nProductType)
				{
				case WORK_AND_BACK:		strOut = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_strSideName : pLayout->m_BackSide.m_strSideName; break;
				case WORK_AND_TURN  :	strOut = theApp.settings.m_szWorkAndTurnName;		break;
				case WORK_AND_TUMBLE:	strOut = theApp.settings.m_szWorkAndTumbleName;		break;
				case WORK_SINGLE_SIDE:	strOut = theApp.settings.m_szSingleSidedName;		break;
				default:				strOut = "";										break;
				}

				CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 50);
				pDC->TextOut(50, GETTEXTPOS_VERT(nLine), strOut);

				textExtent = pDC->GetTextExtent(strOut);
				CRect regRect(CPoint(17, nLine * m_pParent->m_nLineHeight), CSize(17 + 20 + textExtent.cx, 15));
				m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetColorSettingsView, PDFSheetSide), (void*)nSide, CDisplayItem::ForceRegister);

				nCount++;
				if (m_pParent->m_nWhatToShow == CPrintSheetTable::ShowOutputList)
				{
					CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;
					if (rPlateList.GetCount())
					{
						if ( (rPlateList.GetHead().m_strPlateName != "Composite") || (rPlateList.GetCount() > 1) )
						{
							pDC->DrawIcon(CPoint(17, nLine * m_pParent->m_nLineHeight + 5), (rPlateList.m_bDisplayOpen) ? hIconMinus : hIconPlus);

							if (rPlateList.m_bDisplayOpen)
							{
								POSITION pos = rPlateList.GetHeadPosition();
								while (pos)
								{
									CPlate& rPlate = rPlateList.GetNext(pos);

									nLine++;
								
									CRect colorRect(CPoint(30, nLine * m_pParent->m_nLineHeight + 5), CSize(10, 10));
									if (pDC->RectVisible(colorRect))
									{
										CColorDefinition& rColorDef	= pDoc->m_ColorDefinitionTable[rPlate.m_nColorDefinitionIndex];
										CBrush  colorBrush(rColorDef.m_rgb);
										CPen	grayPen(PS_SOLID, 1, DARKGRAY);
										CBrush* pOldBrush = pDC->SelectObject(&colorBrush);
										CPen*	pOldPen	  = pDC->SelectObject(&grayPen);
										pDC->Rectangle(colorRect);
										pDC->SelectObject(pOldBrush);
										pDC->SelectObject(pOldPen);
										colorBrush.DeleteObject();
									}

									strOut = rPlate.m_strPlateName;
									CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 43);
									pDC->TextOut(43, GETTEXTPOS_VERT(nLine), strOut);

									textExtent = pDC->GetTextExtent(strOut);
									CRect regRect(CPoint(30, nLine * m_pParent->m_nLineHeight), CSize(18 + textExtent.cx, 15));
									m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet,
								   								 DISPLAY_ITEM_REGISTRY(CPrintSheetColorSettingsView, PDFSheetPlate), (void*)&rPlate, CDisplayItem::ForceRegister);

									nCount++;
								}
							}
						}
					}
				}
				else
				{
					CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;
					CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
					rObjectList.GetSheetObjectRefList(&rPrintSheet, &objectRefList);

					if (objectRefList.GetSize())
					{
						pDC->DrawIcon(CPoint(17, nLine * m_pParent->m_nLineHeight + 5), (rObjectList.m_bDisplayOpen) ? hIconMinus : hIconPlus);

						if ( (rObjectList.m_bDisplayOpen))
						{
							for (int i = 0; i < objectRefList.GetSize(); i++)
							{
								CLayoutObject* pObject		   = objectRefList[i].m_pLayoutObject;
								CPageTemplate* pObjectTemplate = objectRefList[i].m_pObjectTemplate;
								if ( ! pObject || ! pObjectTemplate)
									continue;

								nLine++;
							
								if (objectRefList[i].m_nObjectType == CLayoutObject::Page)
									pDC->DrawIcon(CPoint(40, nLine * m_pParent->m_nLineHeight + 3), hIconPage);
								else
								{
									HICON hIcon = hIconCustomMark;
									switch (objectRefList[i].m_nMarkType)
									{
									case CMark::CustomMark:		hIcon = hIconCustomMark;	break;
									case CMark::TextMark:		hIcon = hIconTextMark;		break;
									case CMark::SectionMark:	hIcon = hIconSectionMark;	break;
									case CMark::CropMark:		hIcon = hIconCropMark;		break;
									case CMark::FoldMark:		hIcon = hIconFoldMark;		break;
									case CMark::BarcodeMark:	hIcon = hIconBarcodeMark;	break;
									case CMark::DrawingMark:	hIcon = hIconDrawingMark;	break;
									}
									pDC->DrawIcon(CPoint(38, nLine * m_pParent->m_nLineHeight + 2), hIcon);
								}

								strOut = pObjectTemplate->m_strPageID;
								CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 43);
								pDC->TextOut(55, GETTEXTPOS_VERT(nLine), strOut);

								textExtent = pDC->GetTextExtent(strOut);
								CRect regRect(CPoint(35, nLine * m_pParent->m_nLineHeight), CSize(25 + textExtent.cx, 15));
								regRect.right = min(regRect.right, m_columnClipRect.Width());
								m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)pObject,
								 							 DISPLAY_ITEM_REGISTRY(CPrintSheetColorSettingsView, PDFSheetObject), (void*)&rPrintSheet, CDisplayItem::ForceRegister);

								nCount++;
							}
						}
					}
				}

				if (pLayout->m_nProductType)
					nSide = BACKSIDE;	// skip backside
			}
			while (nSide != BACKSIDE);
		}
	
		nLine += GetCellLines(rPrintSheet) - nCount;
	}
}

void CColumnSheetColorSettingsInfo::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	HICON	 hCheckIcon	= theApp.LoadIcon(IDI_CHECK);
	HICON	 hLinkIcon	= theApp.LoadIcon(IDI_PRINTCOLOR_LINK);
	CString  strOut;
	int		 nLine				   = 0;
	int		 nSepStatus			   = 0;
	int		 nColorDefinitionIndex = (int)m_dwData;
	CString  strColor			   = m_strColumnLabel;
	POSITION pos   = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet&  rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		CLayout* pLayout = rPrintSheet.GetLayout();
		if ( ! pLayout)
			continue;

		if ( ! rPrintSheet.m_bDisplayOpen)
		{
			if ( nSepStatus = rPrintSheet.ContainsPrintColor((int)m_dwData, BOTHSIDES, m_strColumnLabel) )
			{
				if ( ((int)m_dwData == nColorDefinitionIndex) || (nColorDefinitionIndex == -1) )
				{
					pDC->DrawIcon(-2, GETTEXTPOS_VERT(nLine) + 2, hCheckIcon);
					switch (nSepStatus)
					{
					case CColorDefinition::Separated:	strOut = _T("s");		break;
					case CColorDefinition::Unseparated:	strOut = _T("");		break;	//strOut = "c";	break;
					case CColorDefinition::Mixed:		strOut = _T("s+c");	break;
					}
					CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 18);
					pDC->TextOut(10, GETTEXTPOS_VERT(nLine), strOut);
				}
				else
				{
					pDC->DrawIcon(0, GETTEXTPOS_VERT(nLine), hLinkIcon);
					if ( (nColorDefinitionIndex >= 0) && (nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
						strOut = pDoc->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName;
					CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 18);
					pDC->TextOut(18, GETTEXTPOS_VERT(nLine), strOut);
				}
			}
			nLine += GetCellLines(rPrintSheet);
		}
		else
		{
			int	nCount = 0;
			int nSide  = -1;
			do
			{
				nLine++;
				nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;
				CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;

				if ( nSepStatus = rPrintSheet.ContainsPrintColor((int)m_dwData, nSide, m_strColumnLabel) )
				{
					if ( ! rObjectList.m_bDisplayOpen)
						if ( ((int)m_dwData == nColorDefinitionIndex) || (nColorDefinitionIndex == -1) )
						{
							pDC->DrawIcon(-2, GETTEXTPOS_VERT(nLine) + 2, hCheckIcon);
							switch (nSepStatus)
							{
							case CColorDefinition::Separated:	strOut = _T("s");		break;
							case CColorDefinition::Unseparated:	strOut = _T("");		break;	//strOut = "c";	break;
							case CColorDefinition::Mixed:		strOut = _T("s+c");	break;
							}
							CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 18);
							pDC->TextOut(10, GETTEXTPOS_VERT(nLine), strOut);
						}
						else
						{
							pDC->DrawIcon(0, GETTEXTPOS_VERT(nLine), hLinkIcon);
							if ( (nColorDefinitionIndex >= 0) && (nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
								strOut = pDoc->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName;
							CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 18);
							pDC->TextOut(18, GETTEXTPOS_VERT(nLine), strOut);
						}
				}

				nCount++;

				CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
				rObjectList.GetSheetObjectRefList(&rPrintSheet, &objectRefList);

				if (objectRefList.GetSize())
				{
					if (rObjectList.m_bDisplayOpen)
					{
						for (int i = 0; i < objectRefList.GetSize(); i++)
						{
							CPageTemplate* pObjectTemplate = objectRefList[i].m_pObjectTemplate;
							if ( ! pObjectTemplate)
								continue;

							nLine++;

							CLayoutObject* pObject = objectRefList[i].m_pLayoutObject;
							if ( ! pObject)
								continue;
							if (nColorDefinitionIndex == -2)	// none printed colors
							{
								strOut.Empty();
								strOut = pObjectTemplate->GetNonePrintedColors(&rPrintSheet, pObject);
								if (strOut.GetLength())
								{
									COLORREF crOldColor = pDC->SetTextColor(DARKGRAY);
									CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 5);
									pDC->TextOut(5, GETTEXTPOS_VERT(nLine), strOut);
									pDC->SetTextColor(crOldColor);
								}
								//else
								//{	///// 06/28/08	to display mapped colors - not sure if this is correct
								//	strOut = pObjectTemplate->GetMappedColorsComposite();
								//	if (strOut.GetLength())
								//	{
								//		pDC->DrawIcon(20, GETTEXTPOS_VERT(nLine) + 1, hLinkIcon);
								//		CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 35);
								//		pDC->TextOut(35, GETTEXTPOS_VERT(nLine), strOut);
								//	}
								//}
							}
							else	// printed colors
							{
								CString strRemappedColor;
								if (pObject->m_nType == CLayoutObject::Page)
									nSepStatus = pObjectTemplate->IsColorPrinted(nColorDefinitionIndex, &rPrintSheet, pObject, strRemappedColor);
								else
								{
									CMark* pMark = pLayout->m_markList.FindMark(pObject->m_strFormatName);
									nSepStatus = pObjectTemplate->IsColorPrinted(nColorDefinitionIndex, &rPrintSheet, pMark, strRemappedColor, &pLayout->m_markList.m_colorDefTable);
								}
								if (nSepStatus)
								{
									if ( (pDoc->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName.CompareNoCase(_T("Composite")) == 0) && theApp.m_colorDefTable.IsProcessColor(m_strColumnLabel) )
									{
										if ( (pObject->m_nType == CLayoutObject::ControlMark) && pObjectTemplate->m_bMap2AllColors)
											;
										else
										{
											int nColor = -1;
											if (theApp.m_colorDefTable.IsCyan(m_strColumnLabel))
												nColor = CColorDefinition::ProcessC;
											else
												if (theApp.m_colorDefTable.IsMagenta(m_strColumnLabel))
													nColor = CColorDefinition::ProcessM;
												else
													if (theApp.m_colorDefTable.IsYellow(m_strColumnLabel))
														nColor = CColorDefinition::ProcessY;
													else
														if (theApp.m_colorDefTable.IsKey(m_strColumnLabel))
															nColor = CColorDefinition::ProcessK;
											CMark* pMark = pLayout->m_markList.FindMark(pObject->m_strFormatName);
											if ( ! pObjectTemplate->HasProcessColor(pMark, nColor, &pDoc->m_ColorDefinitionTable))
												nSepStatus = FALSE;
										}
									}

									if (nSepStatus)
									{
										strOut.Empty();
										if (strRemappedColor.GetLength())	// has been remapped
											pDC->DrawIcon( 0, GETTEXTPOS_VERT(nLine) + 1, hLinkIcon);
										else
											pDC->DrawIcon(-2, GETTEXTPOS_VERT(nLine) + 2, hCheckIcon);
										//switch (nSepStatus)
										//{
										//case CColorDefinition::Separated:	strOut += _T("s");		break;
										//case CColorDefinition::Unseparated:	strOut += _T("");		break;	//strOut += "c";		break;
										//case CColorDefinition::Mixed:		strOut += _T("s+c");	break;
										//}
										pDC->TextOut(10, GETTEXTPOS_VERT(nLine), strOut);
									}
								}
							}

							nCount++;
						}
					}
				}

				if (rPrintSheet.GetLayout())
					if (rPrintSheet.GetLayout()->m_nProductType)
						nSide = BACKSIDE;	// skip backside
			}
			while (nSide != BACKSIDE);
		
			nLine += GetCellLines(rPrintSheet) - nCount;
		}
	}
}


// CPrintSheetColorSettingsView

IMPLEMENT_DYNCREATE(CPrintSheetColorSettingsView, CFormView)

CPrintSheetColorSettingsView::CPrintSheetColorSettingsView()
	: CFormView(CPrintSheetColorSettingsView::IDD)
{
	m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CPrintSheetColorSettingsView));
}

CPrintSheetColorSettingsView::~CPrintSheetColorSettingsView()
{
}

void CPrintSheetColorSettingsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPrintSheetColorSettingsView, CFormView)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_COLORSETTINGS_PRINTCOLORS_BUTTON, OnBnClickedColorsettingsPrintcolorsButton)
END_MESSAGE_MAP()


// CPrintSheetColorSettingsView-Diagnose

#ifdef _DEBUG
void CPrintSheetColorSettingsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetColorSettingsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrintSheetColorSettingsView-Meldungshandler

void CPrintSheetColorSettingsView::OnInitialUpdate()
{
	m_colorSettingsTableTitle.SubclassDlgItem (IDC_PRINTVIEW_SETTINGS_TITLE_FRAME,  this);
	m_colorSettingsTableHeader.SubclassDlgItem(IDC_PRINTVIEW_SETTINGS_HEADER_FRAME, this);

	m_colorSettingsTable.Create(this);
	m_colorSettingsTable.Initialize(-1);//CPrintSheetTable::ShowPrintColorInfo);	

	OnUpdate(NULL, 0, NULL); 

	CFormView::OnInitialUpdate();
}

void CPrintSheetColorSettingsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_DisplayList.Invalidate();	// for now it's easier always to invalidate, because no. items changes when items are expanded

	m_colorSettingsTable.CalcTableLenght();

	CRect clientRect;
	GetClientRect(clientRect);

	CSize scrollSize(m_colorSettingsTable.m_nHeaderWidth, m_colorSettingsTable.m_nTableLenght + m_colorSettingsTable.m_headerFrameRect.bottom);
	CSize pageSize  (m_colorSettingsTable.m_nHeaderWidth, clientRect.Height() - m_colorSettingsTable.m_nHeaderHeight);
	CSize lineSize  (m_colorSettingsTable.m_nHeaderWidth, m_colorSettingsTable.m_nLineHeight);
	SetScrollSizes(MM_TEXT, scrollSize, pageSize, lineSize);

	Invalidate();
}

void CPrintSheetColorSettingsView::OnDraw(CDC* pDC)
{
	if ( ! m_colorSettingsTable.m_ColumnRefList.GetSize())
		return;

	m_DisplayList.BeginRegisterItems(pDC);

	m_colorSettingsTable.OnDraw(pDC);

	m_DisplayList.EndRegisterItems(pDC);
}

BOOL CPrintSheetColorSettingsView::OnActivatePDFSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been activated
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
	pPrintSheet->m_bDisplayOpen = ! pPrintSheet->m_bDisplayOpen;

	pDI->m_nState = CDisplayItem::Normal;

	OnUpdate(NULL, 0, 0);

	return TRUE;
}

BOOL CPrintSheetColorSettingsView::OnSelectPDFSheet(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;
	CRect rect(pDI->m_BoundingRect.TopLeft(), CSize(12, pDI->m_BoundingRect.Height()));
	if (rect.PtInRect(point))
		OnActivatePDFSheet(pDI, point, 0);

	return TRUE;
}

BOOL CPrintSheetColorSettingsView::OnActivatePDFSheetSide(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been activated
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if (pLayout)
		if ((int)pDI->m_pAuxData == FRONTSIDE)
			pLayout->m_FrontSide.m_ObjectList.m_bDisplayOpen = ! pLayout->m_FrontSide.m_ObjectList.m_bDisplayOpen;
		else
			pLayout->m_BackSide.m_ObjectList.m_bDisplayOpen  = ! pLayout->m_BackSide.m_ObjectList.m_bDisplayOpen;

	pDI->m_nState = CDisplayItem::Normal;

	OnUpdate(NULL, 0, 0);

	return TRUE;
}

BOOL CPrintSheetColorSettingsView::OnSelectPDFSheetSide(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;
	CRect rect(pDI->m_BoundingRect.TopLeft(), CSize(12, pDI->m_BoundingRect.Height()));
	if (rect.PtInRect(point))
		OnActivatePDFSheetSide(pDI, point, 0);

	return TRUE;
}

BOOL CPrintSheetColorSettingsView::OnActivatePDFSheetObject(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
    CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if (pParentFrame)
		if ( ! pParentFrame->m_pDlgLayoutObjectProperties->m_hWnd)
			pParentFrame->m_pDlgLayoutObjectProperties->Create((CLayoutObject*)pDI->m_pData, NULL, (CPrintSheet*)pDI->m_pAuxData, this, 1, TRUE);

	return TRUE;
}

void CPrintSheetColorSettingsView::OnLButtonDown(UINT nFlags, CPoint point)
{
    SetFocus();	// In order to deactivate eventually active InplaceEdit

	m_DisplayList.OnLButtonDown(point); // pass message to display list

	CFormView::OnLButtonDown(nFlags, point);
}

void CPrintSheetColorSettingsView::OnBnClickedColorsettingsPrintcolorsButton()
{
	CDlgPrintColorTable dlg;
	dlg.DoModal();

	m_colorSettingsTable.Initialize(-1);
}


// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
BOOL CPrintSheetColorSettingsView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect;
		GetClientRect(scrollRect);
		scrollRect.top+= m_colorSettingsTable.m_headerFrameRect.bottom + 1;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, scrollRect);

		if (sizeScroll.cx != 0)				// Scroll header control horizontally
			m_colorSettingsTableHeader.MoveWindow(-x, m_colorSettingsTable.m_headerFrameRect.top, scrollRect.Width() + x, m_colorSettingsTable.m_nHeaderHeight);
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

#include "Afxpriv.h"

// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
void CPrintSheetColorSettingsView::UpdateBars()
{
	// UpdateBars may cause window to be resized - ignore those resizings
	if (m_bInsideUpdate)
		return;         // Do not allow recursive calls

	// Lock out recursion
	m_bInsideUpdate = TRUE;

	// update the horizontal to reflect reality
	// NOTE: turning on/off the scrollbars will cause 'OnSize' callbacks
	ASSERT(m_totalDev.cx >= 0 && m_totalDev.cy >= 0);

	CRect rectClient;
	BOOL bCalcClient = TRUE;

	// allow parent to do inside-out layout first
	CWnd* pParentWnd = GetParent();
	if (pParentWnd != NULL)
	{
		// if parent window responds to this message, use just
		//  client area for scroll bar calc -- not "true" client area
		if ((BOOL)pParentWnd->SendMessage(WM_RECALCPARENT, 0,
			(LPARAM)(LPCRECT)&rectClient) != 0)
		{
			// use rectClient instead of GetTrueClientSize for
			//  client size calculation.
			bCalcClient = FALSE;
		}
	}

	CSize sizeClient;
	CSize sizeSb;

	if (bCalcClient)
	{
		// get client rect
		if (!GetTrueClientSize(sizeClient, sizeSb))
		{
			// no room for scroll bars (common for zero sized elements)
			CRect rect;
			GetClientRect(&rect);
			if (rect.right > 0 && rect.bottom > 0)
			{
				// if entire client area is not invisible, assume we have
				//  control over our scrollbars
				EnableScrollBarCtrl(SB_BOTH, FALSE);
			}
			m_bInsideUpdate = FALSE;
			return;
		}
	}
	else
	{
		// let parent window determine the "client" rect
		GetScrollBarSizes(sizeSb);
		sizeClient.cx = rectClient.right - rectClient.left;
		sizeClient.cy = rectClient.bottom - rectClient.top;
	}

	// enough room to add scrollbars
	CSize sizeRange;
	CPoint ptMove;
	CSize needSb;

	// get the current scroll bar state given the true client area
	GetScrollBarState(sizeClient, needSb, sizeRange, ptMove, bCalcClient);
	if (needSb.cx)
		sizeClient.cy -= sizeSb.cy;
	if (needSb.cy)
		sizeClient.cx -= sizeSb.cx;

	// first scroll the window as needed
	ScrollToDevicePosition(ptMove); // will set the scroll bar positions too

	// this structure needed to update the scrollbar page range
	SCROLLINFO info;
	info.fMask = SIF_PAGE|SIF_RANGE;
	info.nMin = 0;

	// now update the bars as appropriate
	EnableScrollBarCtrl(SB_HORZ, needSb.cx);
	if (needSb.cx)
	{
		info.nPage = sizeClient.cx;
		info.nMax = m_totalDev.cx-1;
		if (!SetScrollInfo(SB_HORZ, &info, TRUE))
			SetScrollRange(SB_HORZ, 0, sizeRange.cx, TRUE);
	}
	EnableScrollBarCtrl(SB_VERT, needSb.cy);
	if (needSb.cy)
	{
		info.nPage = sizeClient.cy;
		info.nMax = m_totalDev.cy-1;
		if (!SetScrollInfo(SB_VERT, &info, TRUE))
			SetScrollRange(SB_VERT, 0, sizeRange.cy, TRUE);
	}

	// remove recursion lockout
	m_bInsideUpdate = FALSE;
}

// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
void CPrintSheetColorSettingsView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);

	// This part is changed - rest is taken from MFC
	CRect scrollRect;
	GetClientRect(scrollRect);
	scrollRect.top+= m_colorSettingsTable.m_headerFrameRect.bottom;	// Protect header control from being vertically scrolled
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, scrollRect);
//	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y);	// original implementation
}

void CPrintSheetColorSettingsView::SetScrollSizes(int nMapMode, SIZE sizeTotal,
	const SIZE& sizePage, const SIZE& sizeLine)
{
	ASSERT(sizeTotal.cx >= 0 && sizeTotal.cy >= 0);
	ASSERT(nMapMode > 0);
	ASSERT(nMapMode != MM_ISOTROPIC && nMapMode != MM_ANISOTROPIC);

	int nOldMapMode = m_nMapMode;
	m_nMapMode = nMapMode;
	m_totalLog = sizeTotal;

	//BLOCK: convert logical coordinate space to device coordinates
	{
		CWindowDC dc(NULL);
		ASSERT(m_nMapMode > 0);
		dc.SetMapMode(m_nMapMode);

		// total size
		m_totalDev = m_totalLog;
		dc.LPtoDP((LPPOINT)&m_totalDev);
		m_pageDev = sizePage;
		dc.LPtoDP((LPPOINT)&m_pageDev);
		m_lineDev = sizeLine;
		dc.LPtoDP((LPPOINT)&m_lineDev);
		if (m_totalDev.cy < 0)
			m_totalDev.cy = -m_totalDev.cy;
		if (m_pageDev.cy < 0)
			m_pageDev.cy = -m_pageDev.cy;
		if (m_lineDev.cy < 0)
			m_lineDev.cy = -m_lineDev.cy;
	} // release DC here

	// now adjust device specific sizes
	ASSERT(m_totalDev.cx >= 0 && m_totalDev.cy >= 0);
	if (m_pageDev.cx == 0)
		m_pageDev.cx = m_totalDev.cx / 10;
	if (m_pageDev.cy == 0)
		m_pageDev.cy = m_totalDev.cy / 10;
	if (m_lineDev.cx == 0)
		m_lineDev.cx = m_pageDev.cx / 10;
	if (m_lineDev.cy == 0)
		m_lineDev.cy = m_pageDev.cy / 10;

	if (m_hWnd != NULL)
	{
		// window has been created, invalidate now
		UpdateBars();
		if (nOldMapMode != m_nMapMode)
			Invalidate(TRUE);
	}
}
