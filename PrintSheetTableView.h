#if !defined(AFX_PRINTSHEETTABLEVIEW_H__D98F1C73_1F0B_11D3_9567_204C4F4F5020__INCLUDED_)
#define AFX_PRINTSHEETTABLEVIEW_H__D98F1C73_1F0B_11D3_9567_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintSheetTableView.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Formularansicht CPrintSheetTableView 

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif



class CPrintSheetTableColumn
{
public:
	CPrintSheetTableColumn();           
	CPrintSheetTableColumn(int nColumnWidth, CString strColumnLabel, class CPrintSheetTable* pParent, DWORD dwData = 0);


#define GETTEXTPOS_VERT(nLine) ((nLine) * m_pParent->m_nLineHeight + columnMargin/2)

enum {columnMargin = 6};
	
// Attributes
public:
	int				m_nColumnLeft;
	int				m_nColumnWidth;
	CRect			m_columnClipRect;
	CString			m_strColumnLabel;
	CDisplayList*	m_pDisplayList;
	DWORD			m_dwData;


protected:
	class CPrintSheetTable* m_pParent;



// Operations
public:
	virtual void OnDraw(CDC* pDC);
	int			 GetCellLines(CPrintSheet& rPrintSheet);
};


class CColumnRef
{
public:
	CColumnRef() {m_iItem = -1; m_pColumn = NULL;};
	CColumnRef(int iItem, CPrintSheetTableColumn* pColumn) {m_iItem = iItem; m_pColumn = pColumn;};

public:
	int						m_iItem;
	CPrintSheetTableColumn* m_pColumn;
};


typedef
CArray<CColumnRef, CColumnRef&> COLUMN_REFLIST;



/////////////////////////////////////////////////////////////////////////////
// classes for columns

#define DECLARE_TABLECOLUMN(ColumnClass)\
			class ColumnClass : public CPrintSheetTableColumn\
			{\
			public:\
				ColumnClass(int nColumnWidth, CString strColumnLabel, class CPrintSheetTable* pParent, DWORD dwData = 0)\
					: CPrintSheetTableColumn(nColumnWidth, strColumnLabel, pParent, dwData) {};\
			public:\
				virtual void OnDraw(CDC* pDC);\
			};
			

DECLARE_TABLECOLUMN(CColumnPrintSheetNumber);
DECLARE_TABLECOLUMN(CColumnPrintSheetLayout);
DECLARE_TABLECOLUMN(CColumnLayoutNotes);
DECLARE_TABLECOLUMN(CColumnFoldSheetPages);
DECLARE_TABLECOLUMN(CColumnFoldSheetNumPages);
DECLARE_TABLECOLUMN(CColumnFoldSheet);
DECLARE_TABLECOLUMN(CColumnFoldSheetsNUp);
DECLARE_TABLECOLUMN(CColumnPressDevice);
DECLARE_TABLECOLUMN(CColumnPDFSheet);
DECLARE_TABLECOLUMN(CColumnPDFFilename);
DECLARE_TABLECOLUMN(CColumnOutputStatus);
DECLARE_TABLECOLUMN(CColumnPDFTarget);
DECLARE_TABLECOLUMN(CColumnPDFLocation);
DECLARE_TABLECOLUMN(CColumnPrintColorInfo);
/////////////////////////////////////////////////////////////////////////////



class CPrintSheetTableHeaderCtrl : public CHeaderCtrl
{
public:
	CPrintSheetTableHeaderCtrl();

friend class CPrintSheetTable;

protected:
	class CPrintSheetTable* m_pParent;

protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
};



class CPrintSheetTable
{
public:
	CPrintSheetTable();
	~CPrintSheetTable();


#define IDC_PRINTSHEETTABLEHEADER 3001

	enum {UnInitialized = -1, ShowPrintSheetList = 0, ShowOutputList = 1, ShowPrintColorInfo = 2};

//Attributes:
	CPrintSheetTableHeaderCtrl	m_HeaderCtrl;
	CFont						m_HeaderFont;
	int							m_nHeaderWidth, m_nHeaderHeight;
	CRect						m_headerFrameRect;
	int							m_nLineHeight;
	int							m_nTableLenght;
	HICON						m_hIcon;
	CBitmap*					m_pBkBitmap;
	CPen						m_redPen;
	CRect						m_rectBk;
	COLUMN_REFLIST				m_ColumnRefList;
	int							m_nWhatToShow;

	CWnd*						m_pParent;


//Operations:
public:
	virtual void Create(class CPrintSheetTableView* pParent);
	virtual void Initialize(int nWhatToShow);
	virtual int	 GetCellLines(CPrintSheet& rPrintSheet);
	void CalcTableLenght();
	void SaveBackground	  (CDC* pDC, const CRect& rect);
	void RestoreBackground(CDC* pDC, const CRect& rect);
	void ResizeColumns (int nItem, int nNewWidth);
	BOOL ReorderColumns(int nItem, int nNewOrder);
	void OnDraw(CDC* pDC);
};






class CPrintSheetTableView : public CFormView
{
protected:
	CPrintSheetTableView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	DECLARE_DYNCREATE(CPrintSheetTableView)

// Formulardaten
public:
	//{{AFX_DATA(CPrintSheetTableView)
	enum { IDD = IDD_PRINTSHEETTABLEVIEW_FORM };
	CStatic	m_headerFrame;
	CProgressCtrl	m_pdfOutputProgress;
	CString	m_strPDFOutputMessage;
	CString	m_strCurrentOutputPage;
	//}}AFX_DATA

// Attribute
public:
	COleDropTarget	 m_dropTarget;
	CPrintSheetTable m_PrintSheetTable;
	CPoint			 m_dragPoint;
	CPoint			 m_ptDragIcon;
	HICON			 m_hDragIcon;
	CBitmap			 m_bmDragBackground;
	CRect			 m_rectDragIcon;
	CString			 m_strDragIcon;
	CAnimateCtrl	 m_wndAnimate;
	CToolBarCtrl	 m_printSheetListToolBarCtrl, m_printColorInfoToolBarCtrl, m_outputListToolBarCtrl;
	CToolTipCtrl	 m_printSheetListToolTipCtrl, m_printColorInfoToolTipCtrl, m_outputListToolTipCtrl;
	BOOL			 m_bOutputProfileChecked;
	BOOL			 m_bStartOutputChecked;
	CDlgNumeration*  m_pDlgFoldSheetNumeration;

public:
	DECLARE_INPLACE_EDIT(InplaceEdit, CPrintSheetTableView);


	DECLARE_DISPLAY_LIST(CPrintSheetTableView, OnMultiSelect, NO, OnStatusChanged, YES)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, SheetNumber,		  OnSelect, YES,  OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, Layout,			  OnSelect, YES,  OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, LayoutNotes,		  OnSelect, NO,   OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, FoldSheetType,	  OnSelect, YES,  OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, PDFSheet,		  OnSelect, YES,  OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, PDFSheetSide,	  OnSelect, YES,  OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, PDFSheetPlate,	  OnSelect, NO,   OnDeselect, NO, OnActivate, NO,  OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, PDFSheetFile,	  OnSelect, YES,  OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, PDFSheetSideFile,  OnSelect, YES,  OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, PDFSheetPlateFile, OnSelect, NO,   OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetTableView, PDFSheetObject,	  OnSelect, NO,   OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO,  IsSelectable, TRUE)


// Operationen
public:
	void			SaveBitmap	 (CBitmap* pBitmap, const CRect& rect);
	void			RestoreBitmap(CBitmap* pBitmap, const CRect& rect);
	static void		LoadStandardLayout(CLayout* pLayout, CPrintSheet* pPrintSheet);
	static void		SaveStandardLayout(CLayout* pLayout, CPrintSheet* pPrintSheet);
	static CString	ExtractStandardName(CString strFileTitle);
	BOOL			MoveCopyFoldSheet(CDisplayItem& sourceDI, CDisplayItem& targetDI, int nSide, DROPEFFECT dropEffect);
	BOOL			MovePrintSheet   (CDisplayItem& sourceDI, CDisplayItem& targetDI, int nSide, DROPEFFECT dropEffect);
	BOOL			MoveCopyLayout   (CDisplayItem& sourceDI, CDisplayItem& targetDI, int nSide, DROPEFFECT dropEffect);
	void			DeleteFoldSheet(CPrintSheet* pPrintSheet, int nLayerTypeIndex);
	void			UpdatePrintSheetNumber();
	void			UpdateLayoutName();
	void			UpdateFoldSheetNumber();
	void			HighlightPDFOutFile(CPrintSheet* pPrintSheet, int nSide, CPlate* pPlate);
	void			LowlightPDFOutFile();
	void			StartPDFOutFeedback(int nPages);
	void			StopPDFOutFeedback();
	CDisplayItem*	FindDisplayItem	  (CPrintSheet* pPrintSheet, int nSide, CPlate* pPlate);
	void			CreatePrintSheetListToolbar();
	void			CreatePrintColorInfoToolbar();
	void			CreateOutputListToolbar();
	void			LaunchNotepad(const CString& strOutFile);
	static BOOL		InitNumerationDialog(CDlgNumeration* pDlg);
	static void		ChangeNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse = FALSE, int nProductPartIndex = -1);
	void			SelectPrintSheet(CPrintSheet* pPrintSheet);
	static int		GetMaxFoldSheetNumberLen(CDC* pDC, BOOL bIncludeLayerNum);



// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPrintSheetTableView)
	public:
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual void OnDragLeave();
	virtual DROPEFFECT OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnDraw(CDC* pDC);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	//}}AFX_VIRTUAL
	void UpdateBars();
	void ScrollToDevicePosition(POINT ptDev); // explicit scrolling no checking
	void SetScrollSizes(int nMapMode, SIZE sizeTotal, const SIZE& sizePage, const SIZE& sizeLine);

// Implementierung
protected:
	virtual ~CPrintSheetTableView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CPrintSheetTableView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPrintsheetDelFoldsheet();
	afx_msg void OnPrintsheetDuplicateFoldsheet();
	afx_msg void OnPrintsheetNumeration();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnCloseButton();
	afx_msg void OnEditFoldSheetNumber();
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMaxminButton();
	afx_msg void OnPrintsheetList();
protected:
	afx_msg void OnHeaderTrack	  (NMHDR* pNotifyStruct, LRESULT* pResult);
	afx_msg void OnHeaderEndTrack (NMHDR* pNotifyStruct, LRESULT* pResult);
	afx_msg void OnHeaderEndDrag  (NMHDR* pNotifyStruct, LRESULT* pResult);
	afx_msg void OnHeaderItemClick(NMHDR* pNotifyStruct, LRESULT* pResult);
	afx_msg void OnPrintcolorInfo();
	afx_msg void OnPdfOutput();
	afx_msg void OnBmpOutput();
	afx_msg void OnLoadStandardLayout();
	afx_msg void OnSaveStandardLayout();
	afx_msg void OnRenameLayout();
	afx_msg void OnStartOutput();
	afx_msg void OnCancelOutput();
	afx_msg void OnOutputProfile();
	afx_msg void OnOpenOutputFile();
	afx_msg void OnPrintColorTable();
	afx_msg void OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	afx_msg void OnNcPaint();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_PRINTSHEETTABLEVIEW_H__D98F1C73_1F0B_11D3_9567_204C4F4F5020__INCLUDED_
