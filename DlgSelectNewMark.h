#if !defined(AFX_DLGSELECTNEWMARK_H__E0CDBFC4_E83C_11D3_8792_0040F68C1EF7__INCLUDED_)
#define AFX_DLGSELECTNEWMARK_H__E0CDBFC4_E83C_11D3_8792_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSelectNewMark.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSelectNewMark 

class CDlgSelectNewMark : public CDialog
{
// Konstruktion
public:
	CDlgSelectNewMark(CWnd* pParent = NULL);   // Standardkonstruktor

public:
	CWnd*		 m_pParent;
protected:
	CToolBarCtrl m_toolBar;
	TBBUTTON*	 m_pTBButtons;
	int			 m_nSectionMarkTypeID;


// Dialogfelddaten
	//{{AFX_DATA(CDlgSelectNewMark)
	enum { IDD = IDD_SELECT_NEW_MARK };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgSelectNewMark)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	inline BOOL IsChildOfMarksView();


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgSelectNewMark)
	virtual BOOL OnInitDialog();
	afx_msg void OnNewCustomMark();
	afx_msg void OnNewTextMark();
	afx_msg void OnNewSectionMark();
	afx_msg void OnNewCropMark();
	afx_msg void OnNewFoldMark();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGSELECTNEWMARK_H__E0CDBFC4_E83C_11D3_8792_0040F68C1EF7__INCLUDED_
