#if !defined(AFX_DLGPDFLIBSTATUS_H__0D2832A3_9CE1_11D3_876C_204C4F4F5020__INCLUDED_)
#define AFX_DLGPDFLIBSTATUS_H__0D2832A3_9CE1_11D3_876C_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPDFLibStatus.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFLibStatus 

class CDlgPDFLibStatus : public CDialog
{
// Konstruktion
public:
	CDlgPDFLibStatus(CWnd* pParent = NULL, BOOL bRunModal = FALSE);   // Standardkonstruktor
	~CDlgPDFLibStatus();


	enum {OpenDocument, AnalyseContent, CreatePreviews, NoPreviews, PreviewInfo, ContentInfo, 
		  OutputSheet, OutputPlate, OutputTile, OutputPage, OutputMarks, OutputComplete,
		  OK, IPL_Error, PTB_Error, Idle, Duration};
	enum {RegisterPages, RegisterMarks, UpdatePages};

	BOOL			m_bRunModal;
	int				m_nType;
	CString			m_strInitialFolder;
	CString			m_strFullFilename;
	CString			m_strPreviewsFolder;
	int				m_nCurrentPage;
	int				m_nTotalPages;
	BOOL			m_bCancel;
	int				m_nStatus;
	CWnd*			m_pParent;
	CRect			m_currentClientRect;
	BOOL			m_bCreatePreviews;
	BOOL			m_bAutoAssign;
	CString			m_strResolution;
	CString 		m_strDistillerSettings;
	BOOL			m_bDistillerError;
	HANDLE			m_handleDistillerEvent;		// synchronization object
	CPageSource*	m_pPageSource;


// Dialogfelddaten
	//{{AFX_DATA(CDlgPDFLibStatus)
	enum { IDD = IDD_PDFLIB_STATUS };
	CProgressCtrl	m_progressBar;
	CStatic	m_previewFrame;
	CString	m_strFilename;
	CString	m_strStatusOutput;
	CString	m_strCurrentPage;
	CString	m_strFileSize;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPDFLibStatus)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void			  LaunchPDFEngine(CString strInitialFolder, CWnd* pParent, int nType, CString strPDFFile = "", BOOL bOpenFileDlg = FALSE, BOOL bQuietMode = FALSE);
	void			  DistillAndProcess(CString strPSFilename);
	void			  ProcessPDFPages(CString strPDFFileName, CPageSource* pPageSourceToUpdate = NULL, BOOL bPreviewsOnly = FALSE, BOOL bAnalyze = TRUE);
	void			  ProcessPDFMarks(CString strPDFFileName);
	static IPL_bool_t ProgressListContents( IPL_int32_t lowrange, IPL_int32_t highrange, IPL_int32_t counter, void* lparam);
	static void		  SaveAsImageCallBack( PTB_StringID idPath, PTB_int32_t page, void* userData  );
	static PTB_bool_t ProgressCreatePreviews( PTB_uint32_t max, PTB_uint32_t current, void* userData);
	void			  UpdateStatus(int nStatus, int nCode = 0, CString strOut = "", CString strOut2 = "");
	static int		  MapLanguageID();
	CString			  GetPreviewFilename(CString strPreviewsFolder, int nPageIndex);
	static int		  ComparePathName(const void *p1, const void *p2);

protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPDFLibStatus)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnPdflibStatusCancel();
	afx_msg void OnPdflibStatusOpenfile();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnCopyData(WPARAM, LPARAM );
	afx_msg void OnPdfEngineSettings();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnCancel();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPDFLIBSTATUS_H__0D2832A3_9CE1_11D3_876C_204C4F4F5020__INCLUDED_
