// PageListView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageListView view

class CPageListView : public CScrollView
{
protected:
	CPageListView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPageListView)

// Attributes
protected:
	CSize 		 m_TemplateListSize;
	long		 m_lTemplateListCenterX;
	//CPen		 m_MultipagePen;

	COleDropTarget  m_dropTarget;
public:
	DECLARE_DISPLAY_LIST(CPageListView, OnMultiSelect, YES, OnStatusChanged, YES)
protected:
	DECLARE_DISPLAY_ITEM(CPageListView, PageTemplate, OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)


// Operations
protected:
	CImpManDoc* GetDocument();
	static int	GetCurProductPartIndex();
	BOOL		DrawPage(CDC* pDC, CPageTemplate& rTemplate, int* nTop, int* nMinBottom, int nSide, int nIndex);
	BOOL		DrawLabel(CDC* pDC, CPageTemplate& rTemplate, int* nTop, int* nMinBottom, int nSide, int nIndex);
	BOOL		DrawObject(CDC* pDC, CPageTemplate& rTemplate, int* nTop, int* nMinBottom, int nSide, int nIndex);
	int			DrawPagesApplicationBook(CDC* pDC);
	int			DrawPagesApplicationLabels(CDC* pDC, int nTopInit = 0);
	void		CalcTemplateListSize();
	void		CalcTemplateListSizeBook();
	void		CalcTemplateListSizeLabels();
public:
	static BOOL	SetDoublePage(CPageTemplate* pPageTemplate, int nSide, BOOL bAskForChanges, BOOL bDontSetIfOccupied = FALSE);
	static BOOL DrawPreview(CDC* pDC, CRect rect, int nHeadPosition, float fRotation, CString strFileTitle, BOOL bKeepRatio = FALSE, BOOL bDimmed = FALSE, Image** pThumbnailImage = NULL);
	static int  GetEncoderClsid(const WCHAR* format, CLSID* pClsid);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageListView)
	public:
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPageListView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CPageListView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PageListView.cpp
inline CImpManDoc* CPageListView::GetDocument()
   { return (CImpManDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
