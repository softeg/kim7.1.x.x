// DlgMoveObjects.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "ObjectPivotControl.h"
#include "DlgMoveObjects.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetNavigationView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgMoveObjects dialog


CDlgMoveObjects::CDlgMoveObjects(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgMoveObjects::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgMoveObjects)
	m_nMoveXY	 = 0;
	m_fDistanceX = 0.0f;
	m_fDistanceY = 0.0f;
	//}}AFX_DATA_INIT
}

CDlgMoveObjects::~CDlgMoveObjects()
{
	DestroyWindow();
}

void CDlgMoveObjects::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgMoveObjects)
	DDX_Control(pDX, IDC_DISTANCEX_SPIN, m_distanceXSpin);
	DDX_Control(pDX, IDC_DISTANCEY_SPIN, m_distanceYSpin);
	DDX_Measure(pDX, IDC_DISTANCEX, m_fDistanceX);
	DDX_Measure(pDX, IDC_DISTANCEY, m_fDistanceY);
	//}}AFX_DATA_MAP
	DDX_Radio(pDX, IDC_MOVEX, m_nMoveXY);
}


BEGIN_MESSAGE_MAP(CDlgMoveObjects, CDialog)
	//{{AFX_MSG_MAP(CDlgMoveObjects)
	ON_NOTIFY(UDN_DELTAPOS, IDC_DISTANCEX_SPIN, OnDeltaposDistanceXSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_DISTANCEY_SPIN, OnDeltaposDistanceYSpin)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	ON_COMMAND(ID_CHANGED_PIVOT, OnChangedPivot)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_MOVEX, &CDlgMoveObjects::OnBnClickedMovex)
	ON_BN_CLICKED(IDC_MOVEY, &CDlgMoveObjects::OnBnClickedMovey)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgMoveObjects message handlers

BOOL CDlgMoveObjects::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_objectPivotCtrl.SubclassDlgItem(IDC_MEDIA_PIVOT, this);
	m_objectPivotCtrl.m_pParent = this;
//	m_objectPivotCtrl.m_pReflinePosParams = &m_reflinePosParams;

	m_distanceXSpin.SetRange(0, 1);  
	m_distanceYSpin.SetRange(0, 1);  

	switch (m_nMoveXY)
	{
	case 0:	GetDlgItem(IDC_DISTANCEX)->EnableWindow(TRUE);
			GetDlgItem(IDC_DISTANCEX_SPIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_DISTANCEY)->EnableWindow(FALSE);
			GetDlgItem(IDC_DISTANCEY_SPIN)->EnableWindow(FALSE);
			break;
	case 1:	GetDlgItem(IDC_DISTANCEX)->EnableWindow(FALSE);
			GetDlgItem(IDC_DISTANCEX_SPIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_DISTANCEY)->EnableWindow(TRUE);
			GetDlgItem(IDC_DISTANCEY_SPIN)->EnableWindow(TRUE);
			break;
	}
	m_objectPivotCtrl.m_nXRef = LEFT;
	m_objectPivotCtrl.m_nYRef = YCENTER;
	m_objectPivotCtrl.Update();
	m_reflinePosParams.m_nObjectReferenceLineInX = LEFT;
	m_reflinePosParams.m_nObjectReferenceLineInY = YCENTER;

	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_DISTANCEX);
	pEdit->SetSel(0, -1);

	InitReflinePosParams();

	UpdateData(FALSE);

	m_objectPivotCtrl.m_nActiveCell = -1;
	m_objectPivotCtrl.Update();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgMoveObjects::OnMouseMove(UINT nFlags, CPoint point) 
{
	m_objectPivotCtrl.OnMouseMove(nFlags, point);

	CDialog::OnMouseMove(nFlags, point);
}

void CDlgMoveObjects::OnDeltaposDistanceXSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_distanceXSpin.GetBuddy(), pNMUpDown->iDelta);
}

void CDlgMoveObjects::OnDeltaposDistanceYSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_distanceYSpin.GetBuddy(), pNMUpDown->iDelta);
}

void CDlgMoveObjects::InitReflinePosParams()
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	if (pView->m_DisplayList.GetNumSelectedItems() > 1)
	{
		float		  fBBoxLeft = FLT_MAX, fBBoxBottom = FLT_MAX, fBBoxRight = -FLT_MAX, fBBoxTop = -FLT_MAX;
		float		  fObjectXRefline = 0.0f;
		float		  fObjectYRefline = 0.0f;
		CLayoutSide*  pLayoutSide	  = NULL;
		CDisplayItem* pDI			  = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		while (pDI)
		{
			CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
			if (pObject)
			{
				fBBoxLeft	= min(fBBoxLeft,	pObject->GetLeft());
				fBBoxBottom = min(fBBoxBottom,  pObject->GetBottom());
				fBBoxRight	= max(fBBoxRight,	pObject->GetRight());
				fBBoxTop	= max(fBBoxTop,		pObject->GetTop());
				pLayoutSide = pObject->GetLayoutSide();
			}
			pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		}

		switch (m_reflinePosParams.m_nObjectReferenceLineInX)
		{
		case LEFT:		fObjectXRefline = fBBoxLeft;									break;
		case RIGHT:		fObjectXRefline = fBBoxRight;									break;
		case XCENTER:	fObjectXRefline = fBBoxLeft	+ (fBBoxRight - fBBoxLeft) / 2.0f;	break;
		}

		switch (m_reflinePosParams.m_nObjectReferenceLineInY)
		{
		case BOTTOM:	fObjectYRefline = fBBoxBottom;										break;
		case TOP:		fObjectYRefline = fBBoxTop;											break;
		case YCENTER:	fObjectYRefline = fBBoxBottom + (fBBoxTop	- fBBoxBottom) /2.0f;	break;
		}

		if (pLayoutSide)
		{
			float fSheetXRefline = pLayoutSide->GetRefline(m_reflinePosParams.m_nReferenceItemInX, m_reflinePosParams.m_nReferenceLineInX, &m_reflinePosParams.m_hReferenceObjectInX);
			float fSheetYRefline = pLayoutSide->GetRefline(m_reflinePosParams.m_nReferenceItemInY, m_reflinePosParams.m_nReferenceLineInY, &m_reflinePosParams.m_hReferenceObjectInY);
			m_fDistanceX = fObjectXRefline - ( (fSheetXRefline != FLT_MAX) ? fSheetXRefline : 0.0f );
			m_fDistanceY = fObjectYRefline - ( (fSheetYRefline != FLT_MAX) ? fSheetYRefline : 0.0f );
			m_reflinePosParams.m_fDistanceX = m_fDistanceX;
			m_reflinePosParams.m_fDistanceY = m_fDistanceY;
			UpdateData(FALSE);
		}
	}
	else
	{
		CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		if (pDI)
		{
			CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
			if (pObject)
			{
				pObject->UpdateReflineDistances();
				m_reflinePosParams = pObject->m_reflinePosParams;
				m_fDistanceX = m_reflinePosParams.m_fDistanceX;
				m_fDistanceY = m_reflinePosParams.m_fDistanceY;
				UpdateData(FALSE);
			}
		}
	}
	m_objectPivotCtrl.LoadData(m_reflinePosParams.m_nObjectReferenceLineInX, m_reflinePosParams.m_nObjectReferenceLineInY);

	pView->Invalidate();
	pView->UpdateWindow();
}

void CDlgMoveObjects::MoveObjects(int nDirection)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CLayout*	  pLayout = NULL;
	BOOL		  bMoved  = FALSE;
	CDisplayItem* pDI	  = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	while (pDI)
	{
		CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
		if (pObject)
		{
			CLayoutObject* pCounterPartObject = (theApp.settings.m_bFrontRearParallel) ? pObject->FindCounterpartObject() : NULL;
			if (nDirection & HORIZONTAL)
				pObject->Offset(m_reflinePosParams.m_fDistanceX - m_fDistanceX, 0.0f);
			if (nDirection & VERTICAL)
				pObject->Offset(0.0f, m_reflinePosParams.m_fDistanceY - m_fDistanceY);
			if (pCounterPartObject)
				pCounterPartObject->AlignToCounterPart(pObject);

			bMoved	= TRUE;
			pLayout = pObject->GetLayout();
		}
		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}

	if (bMoved)
	{
		if (pLayout)
			pLayout->CheckForProductType();

		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();	// because panorama pages could be arised or destroyed
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
			pDoc->SetModifiedFlag();										
		}
		if (pLayout)
			pLayout->AnalyzeMarks();

		// Don't update Tree to prevent changing the tree-selection
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);

		CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
		if (pNavigationView)
			pNavigationView->UpdatePrintSheets(pLayout);
	}

	if (nDirection & HORIZONTAL)
		m_fDistanceX = m_reflinePosParams.m_fDistanceX;
	if (nDirection & VERTICAL)
		m_fDistanceY = m_reflinePosParams.m_fDistanceY;
	UpdateData(FALSE);
}

void CDlgMoveObjects::OnApplyButton() 
{
	float fOldXOffset = m_fDistanceX;
	float fOldYOffset = m_fDistanceY;

	UpdateData(TRUE);
	m_reflinePosParams.m_fDistanceX = m_fDistanceX;
	m_reflinePosParams.m_fDistanceY = m_fDistanceY;
	m_fDistanceX = fOldXOffset;
	m_fDistanceY = fOldYOffset;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	MoveObjects(HORIZONTAL | VERTICAL);

	InitReflinePosParams();
}

void CDlgMoveObjects::OnChangedPivot()
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	if (pView->m_DisplayList.GetNumSelectedItems() == 1)
	{
		CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		if (pDI)
		{
			CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
			m_objectPivotCtrl.SaveData(pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInX, pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInY);
			m_fDistanceX = pLayoutObject->m_reflinePosParams.m_fDistanceX;
			m_fDistanceY = pLayoutObject->m_reflinePosParams.m_fDistanceY;
			m_reflinePosParams.m_fDistanceX = m_fDistanceX;
			m_reflinePosParams.m_fDistanceY = m_fDistanceY;
		}
	}
	else
		m_objectPivotCtrl.SaveData(m_reflinePosParams.m_nObjectReferenceLineInX, m_reflinePosParams.m_nObjectReferenceLineInY);

	InitReflinePosParams();
}

void CDlgMoveObjects::OnCancel() 
{
	DestroyWindow();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	pView->m_DisplayList.Invalidate();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CDlgMoveObjects::OnBnClickedMovex()
{
	UpdateData(TRUE);

	switch (m_nMoveXY)
	{
	case 0:	GetDlgItem(IDC_DISTANCEX)->EnableWindow(TRUE);
			GetDlgItem(IDC_DISTANCEX_SPIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_DISTANCEY)->EnableWindow(FALSE);
			GetDlgItem(IDC_DISTANCEY_SPIN)->EnableWindow(FALSE);
			break;
	case 1:	GetDlgItem(IDC_DISTANCEX)->EnableWindow(FALSE);
			GetDlgItem(IDC_DISTANCEX_SPIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_DISTANCEY)->EnableWindow(TRUE);
			GetDlgItem(IDC_DISTANCEY_SPIN)->EnableWindow(TRUE);
			break;
	}

	m_objectPivotCtrl.m_nXRef = LEFT;
	m_objectPivotCtrl.m_nYRef = YCENTER;
	m_objectPivotCtrl.Update();
	SendMessage(WM_COMMAND, ID_CHANGED_PIVOT);
}

void CDlgMoveObjects::OnBnClickedMovey()
{
	UpdateData(TRUE);

	switch (m_nMoveXY)
	{
	case 0:	GetDlgItem(IDC_DISTANCEX)->EnableWindow(TRUE);
			GetDlgItem(IDC_DISTANCEX_SPIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_DISTANCEY)->EnableWindow(FALSE);
			GetDlgItem(IDC_DISTANCEY_SPIN)->EnableWindow(FALSE);
			break;
	case 1:	GetDlgItem(IDC_DISTANCEX)->EnableWindow(FALSE);
			GetDlgItem(IDC_DISTANCEX_SPIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_DISTANCEY)->EnableWindow(TRUE);
			GetDlgItem(IDC_DISTANCEY_SPIN)->EnableWindow(TRUE);
			break;
	}

	m_objectPivotCtrl.m_nXRef = XCENTER;
	m_objectPivotCtrl.m_nYRef = BOTTOM;
	m_objectPivotCtrl.Update();
	SendMessage(WM_COMMAND, ID_CHANGED_PIVOT);
}
