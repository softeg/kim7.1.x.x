// ImpManDocCBookblock.cpp : implementation of the CImpManDoc subclass CBookblock
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "BookBlockView.h"
#include "DlgShinglingAssistent.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "GraphicComponent.h"
#include "BookBlockDataView.h"
#include "PDFOutput.h"
#include "DlgBookblockProofProgress.h"
#include "PrintSheetPlanningView.h"
#include "PrintComponentsDetailView.h"
#include "JDFOutputImposition.h"
#include "KimSocketsCommon.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CBookblock

IMPLEMENT_SERIAL(CBookblock, CObject, VERSIONABLE_SCHEMA | 8)


CBookblock::CBookblock()
{
	m_bShinglingActive = FALSE;
	m_nShinglingMethod = ShinglingMethodMove;
	m_OutputDate.m_dt  = 0;		
}

void CBookblock::Copy(const CBookblock& rBookblock)
{
	m_bShinglingActive = rBookblock.m_bShinglingActive;
	m_nShinglingMethod = rBookblock.m_nShinglingMethod;
	m_OutputDate	   = rBookblock.m_OutputDate;		


	m_DescriptionList.RemoveAll();
	POSITION pos = rBookblock.m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem item = rBookblock.m_DescriptionList.GetNext(pos);
		m_DescriptionList.AddHead(item);
	}

	pos = rBookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet foldSheet = rBookblock.m_FoldSheetList.GetNext(pos);
		m_FoldSheetList.AddHead(foldSheet);
	}
}

void CBookblock::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CBookblock));
	UINT nVersion =	(ar.IsLoading()) ? ar.GetObjectSchema() : -1;

	if (nVersion < 4)
	{
		CList <COldDescriptionItem, COldDescriptionItem&> oldDescriptionList;
		oldDescriptionList.Serialize(ar);
		POSITION pos = oldDescriptionList.GetHeadPosition();
		while (pos)
		{
			COldDescriptionItem& rOldItem = oldDescriptionList.GetNext(pos);
			CDescriptionItem newItem;
			newItem.m_nFoldSheetIndex = rOldItem.m_nFoldSheetIndex;
			newItem.m_nFoldSheetPart  = rOldItem.m_nFoldSheetPart;
			m_DescriptionList.AddTail(newItem);
		}
		oldDescriptionList.RemoveAll();
	}
	else
		m_DescriptionList.Serialize(ar);

	if (ar.IsStoring())
	{
		m_FoldSheetList.Serialize(ar);
		SerializeElements(ar, &m_FoldSheetList.m_outputTargetProps, 1);

		ar << m_bShinglingActive;
		ar << m_nShinglingMethod;
		SerializeElements(ar, &m_outputTargetProps, 1);
		ar << m_OutputDate;
	}
	else
	{
		m_FoldSheetList.Serialize(ar);
		switch (nVersion)
		{
		case 1:
			m_bShinglingActive = FALSE;
			m_nShinglingMethod = 0;
			m_OutputDate.m_dt  = 0;
			break;
		case 2:
			ar >> m_bShinglingActive;
			m_nShinglingMethod = 0;
			m_OutputDate.m_dt  = 0;
			break;
		case 3:
		case 4:
			ar >> m_bShinglingActive;
			ar >> m_nShinglingMethod;
			m_OutputDate.m_dt  = 0;
			break;
		case 5:		// in version 5 only CDespriptionItem has changed
			ar >> m_bShinglingActive;
			ar >> m_nShinglingMethod;
			m_OutputDate.m_dt  = 0;
			break;
		case 6:		
			ar >> m_bShinglingActive;
			ar >> m_nShinglingMethod;
			SerializeElements(ar, &m_outputTargetProps, 1);
			m_OutputDate.m_dt  = 0;
			break;
		case 7:		
			ar >> m_bShinglingActive;
			ar >> m_nShinglingMethod;
			SerializeElements(ar, &m_outputTargetProps, 1);
			ar >> m_OutputDate;
			break;
		case 8:		
			SerializeElements(ar, &m_FoldSheetList.m_outputTargetProps, 1);
			ar >> m_bShinglingActive;
			ar >> m_nShinglingMethod;
			SerializeElements(ar, &m_outputTargetProps, 1);
			ar >> m_OutputDate;
			break;
		}
		if (nVersion < 4)
		{
			switch (m_nShinglingMethod)
			{
			case 0:	
			case 1:	m_nShinglingMethod = CBookblock::ShinglingMethodMove;	  break;
			case 2:	m_nShinglingMethod = CBookblock::ShinglingMethodBottling; break;
			}
		}
	}
}

POSITION CBookblock::GetHeadPosition_DescriptionList(int nBoundProductIndex)
{
	POSITION pos = m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem = m_DescriptionList.GetAt(pos);
		CFoldSheet& rFoldSheet = rItem.GetFoldSheet(this);
		if (rFoldSheet.IsPartOfBoundProduct(-nBoundProductIndex - 1))
			return pos;

		m_DescriptionList.GetNext(pos);
	}
	return NULL;
}

POSITION CBookblock::GetTailPosition_DescriptionList(int nBoundProductIndex)
{
	POSITION pos		 = m_DescriptionList.GetHeadPosition();
	POSITION lastItemPos = NULL;
	while (pos)
	{
		CDescriptionItem& rItem = m_DescriptionList.GetAt(pos);
		CFoldSheet& rFoldSheet = rItem.GetFoldSheet(this);
		if (rFoldSheet.IsPartOfBoundProduct(-nBoundProductIndex - 1))
			lastItemPos = pos;

		m_DescriptionList.GetNext(pos);
	}
	return lastItemPos;
}

CDescriptionItem& CBookblock::GetPrev_DescriptionList(int nBoundProductIndex, POSITION& rPosition)
{
	POSITION pos = rPosition;
	m_DescriptionList.GetPrev(rPosition);
	while (rPosition)
	{
		CDescriptionItem& rItem = m_DescriptionList.GetAt(rPosition);
		CFoldSheet& rFoldSheet = rItem.GetFoldSheet(this);
		if (rFoldSheet.IsPartOfBoundProduct(-nBoundProductIndex - 1))
			return rItem;

		m_DescriptionList.GetPrev(rPosition);
	}

	if (pos)
		return m_DescriptionList.GetAt(pos);
	else
	{
		static private CDescriptionItem nullDescriptionItem;
		return nullDescriptionItem;
	}
}

CDescriptionItem& CBookblock::GetNext_DescriptionList(int nBoundProductIndex, POSITION& rPosition)
{
	POSITION pos = rPosition;
	m_DescriptionList.GetNext(rPosition);
	while (rPosition)
	{
		CDescriptionItem& rItem = m_DescriptionList.GetAt(rPosition);
		CFoldSheet& rFoldSheet = rItem.GetFoldSheet(this);
		if (rFoldSheet.IsPartOfBoundProduct(-nBoundProductIndex - 1))
			break;

		m_DescriptionList.GetNext(rPosition);
	}
	if (pos)
		return m_DescriptionList.GetAt(pos);
	else
	{
		static private CDescriptionItem nullDescriptionItem;
		return nullDescriptionItem;
	}
}

void CBookblock::AddHead_DescriptionList(int nBoundProductIndex, CDescriptionItem& rItem)
{
	POSITION pos = GetHeadPosition_DescriptionList(nBoundProductIndex);
	if ( ! pos)
		m_DescriptionList.AddHead(rItem);
	else
		m_DescriptionList.InsertBefore(pos, rItem);
}

void CBookblock::AddTail_DescriptionList(int nBoundProductIndex, CDescriptionItem& rItem)
{
	POSITION pos = GetTailPosition_DescriptionList(nBoundProductIndex);
	if ( ! pos)
		m_DescriptionList.AddTail(rItem);
	else
		m_DescriptionList.InsertAfter(pos, rItem);
}

void CBookblock::CheckFoldSheetQuantities()
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();
		if (rFoldSheet.GetQuantityPlanned() == -1)
			rFoldSheet.SetQuantityPlanned(rProductPart.m_nDesiredQuantity, FALSE);
		if (rFoldSheet.GetQuantityExtra()   == -1)
			rFoldSheet.SetQuantityExtra  (rProductPart.m_nExtra, TRUE);
		rFoldSheet.CalcQuantityActual();
	}
}




/////////////////////////////////////////////////////////////////////////////////////////////////////
// Bookblock modification 

int CBookblock::ImposePageSection(CBoundProductPart* pProductPart, CFoldSheet newFoldSheet, int nNumPages, int nNumSheets, CArray <int, int>* pNewFoldSheetIndices)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nNumPagesImposed	= 0;
	int nNumUnassignedPages = nNumPages;
	while ( (nNumUnassignedPages > 0) && (nNumUnassignedPages >= newFoldSheet.GetNumPages()) && (nNumSheets > 0) )
	{
		int nNumAddedFoldSheets = pDoc->m_Bookblock.AddFoldSheetsSimple(&newFoldSheet, 1, pNewFoldSheetIndices, -1, -1, NULL);
		if (nNumAddedFoldSheets <= 0)
			break;
		nNumPagesImposed	+= nNumAddedFoldSheets * newFoldSheet.GetNumPages();
		nNumUnassignedPages -= nNumAddedFoldSheets * newFoldSheet.GetNumPages();

		pDoc->m_Bookblock.ReNumberFoldSheets(ALL_PARTS);
		nNumSheets--;
	}

	if (pProductPart)
	{
		if (pProductPart->m_foldingParams.m_bShinglingActive)
		{
			pProductPart->m_foldingParams.SetShinglingValueX(pProductPart->GetShinglingValueX(TRUE));
			pProductPart->m_foldingParams.SetShinglingValueXFoot(pProductPart->GetShinglingValueXFoot(TRUE));
			pProductPart->m_foldingParams.SetShinglingValueY(pProductPart->GetShinglingValueY(TRUE));
			pDoc->m_Bookblock.SetShinglingValuesX(pProductPart->m_foldingParams.GetShinglingValueX(*pProductPart), FLT_MAX, newFoldSheet.m_nProductPartIndex);
			pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, pProductPart->m_foldingParams.GetShinglingValueXFoot(*pProductPart), newFoldSheet.m_nProductPartIndex);
			pDoc->m_Bookblock.SetShinglingValuesY(pProductPart->m_foldingParams.GetShinglingValueY(*pProductPart), newFoldSheet.m_nProductPartIndex);
			float fDummy;
			pDoc->m_PageTemplateList.CalcShinglingOffsets(fDummy, fDummy, fDummy, newFoldSheet.m_nProductPartIndex);
		}
	}

	pDoc->m_Bookblock.ReNumberFoldSheets(ALL_PARTS);

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);

	return nNumPagesImposed;
}

// Add nCount foldsheets of type *pFoldSheet to bookblock
int CBookblock::AddFoldSheets(CFoldSheet* pFoldSheet, int nCount, int nProductPartIndex, CArray <int, int>* pNewFoldSheets) 
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	if ( ! pFoldSheet)
		return 0;

	CBoundProduct& rBoundProduct = (nProductPartIndex >= 0) ? pDoc->m_boundProducts.GetProductPart(nProductPartIndex).GetBoundProduct() : pDoc->GetBoundProduct(-nProductPartIndex - 1);

	int nBindingMethod = rBoundProduct.m_bindingParams.m_bindingDefs.m_nBinding;
	int nProductIndex  = rBoundProduct.GetIndex();

	// new foldsheets will be appended after last existing foldsheet of same type (if there is any)
	// if job's binding method is saddlestitched, new foldsheets will be wrapped around
	CDescriptionItem RightItem; 
	POSITION		 insertPosRight = GetTailPosition_DescriptionList(nProductIndex); 

	// Search for last occurence of foldsheet in bookblock - save position in insertPosRight and item in RightItem
	while (insertPosRight)
	{
		RightItem = m_DescriptionList.GetAt(insertPosRight); 
		if (nProductPartIndex >= 0)
			if (RightItem.GetFoldSheet(this).m_nProductPartIndex != nProductPartIndex)
			{
				GetPrev_DescriptionList(nProductIndex, insertPosRight); 
				continue;
			}
		if (RightItem.GetFoldSheet(this).m_strFoldSheetTypeName == pFoldSheet->m_strFoldSheetTypeName)
			break;
		GetPrev_DescriptionList(nProductIndex, insertPosRight); 
	}

	CDescriptionItem LeftItem;
	POSITION		 insertPosLeft = GetHeadPosition_DescriptionList(nProductIndex); 

	// If binding method in job settings is saddlestitched we search for corresponding left item of above founded RightItem
	// in order to wrap the new foldsheets around the founded foldsheet
	// If binding method is PerfectBound, we don't need the left item's position
	if (nBindingMethod == CFinalProductDescription::SaddleStitched)
	{
		while (insertPosLeft)
		{
			LeftItem = m_DescriptionList.GetAt(insertPosLeft); 
			if (nProductPartIndex >= 0)
				if (LeftItem.GetFoldSheet(this).m_nProductPartIndex != nProductPartIndex)
				{
					GetNext_DescriptionList(nProductIndex, insertPosLeft); 
					continue;
				}
			if (LeftItem.m_nFoldSheetIndex == RightItem.m_nFoldSheetIndex)
				break;
			GetNext_DescriptionList(nProductIndex, insertPosLeft); 
		}
	}

	int nIndex = m_FoldSheetList.GetCount();
	LeftItem.m_nFoldSheetIndex = nIndex;
	LeftItem.m_nFoldSheetPart  = CDescriptionItem::FoldSheetLeftPart;
	RightItem.m_nFoldSheetIndex = nIndex;
	RightItem.m_nFoldSheetPart  = CDescriptionItem::FoldSheetRightPart;

	int nNumAdded = 0;
	if (!insertPosRight) // No foldsheets of same type found or bookblock is empty
	{					 // - so add first new foldsheet and initialize insertPosLeft and insertPosRight
		m_FoldSheetList.AddTail(*pFoldSheet);
		if (pNewFoldSheets)
			pNewFoldSheets->Add(nIndex);

		if (nBindingMethod == CFinalProductDescription::SaddleStitched)
		{
			AddHead_DescriptionList(nProductIndex, LeftItem);
			insertPosLeft  = GetHeadPosition_DescriptionList(nProductIndex); 
		}
		else
		{
			AddTail_DescriptionList(nProductIndex, LeftItem);
			insertPosLeft  = GetTailPosition_DescriptionList(nProductIndex); 
		}

		AddTail_DescriptionList(nProductIndex, RightItem);
		insertPosRight = GetTailPosition_DescriptionList(nProductIndex); 

		nNumAdded++;
		nCount--;
		nIndex++;
	}

	while (nCount > 0)
	{
		m_FoldSheetList.AddTail(*pFoldSheet);
		if (pNewFoldSheets)
			pNewFoldSheets->Add(nIndex);

		LeftItem.m_nFoldSheetIndex = nIndex;
		if (nBindingMethod == CFinalProductDescription::SaddleStitched)
		{
			m_DescriptionList.InsertBefore(insertPosLeft, LeftItem);	// wrap around
			GetPrev_DescriptionList(nProductIndex, insertPosLeft); 
		}
		else
		{
			m_DescriptionList.InsertAfter(insertPosRight, LeftItem);
			GetNext_DescriptionList(nProductIndex, insertPosRight); 
		}

		RightItem.m_nFoldSheetIndex = nIndex;
		m_DescriptionList.InsertAfter(insertPosRight, RightItem);
		GetNext_DescriptionList(nProductIndex, insertPosRight); 

		nNumAdded++;
		nCount--;
		nIndex++;
	}

	return nNumAdded;
}

// Add nCount foldsheets of type *pFoldSheet to bookblock
int CBookblock::AddFoldSheetsSimple(CFoldSheet* pFoldSheet, int nCount, CArray <int, int>* pNewFoldSheets, int nPart, int nSection, CFoldSheet* pFoldSheetInsertAfter) 
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	if ( ! pFoldSheet)
		return 0;
	if (nCount <= 0)
		return 0;

	if (pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing)	
		return AddFoldSheetsSimpleCG(pFoldSheet, nCount, pNewFoldSheets, nPart, nSection); 

	// new foldsheets will be appended always to the end (perfect bound) or into the inner fold sheet (saddle stitched)

	CFoldSheet* pLastFoldSheet = (pFoldSheetInsertAfter) ? pFoldSheetInsertAfter : ( (pDoc->m_Bookblock.m_FoldSheetList.GetSize()) ? &pDoc->m_Bookblock.m_FoldSheetList.GetTail() : NULL);

	POSITION insertPosLeft  = NULL; 
	POSITION insertPosRight = NULL;
	if (pLastFoldSheet)
	{
		CDescriptionItem item; 
		POSITION		 pos = m_DescriptionList.GetHeadPosition(); 
		while (pos)
		{
			item = m_DescriptionList.GetAt(pos); 
			if (item.GetFoldSheetPointer(this) == pLastFoldSheet)
			{
				if (item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
					insertPosLeft  = pos;
				else
					insertPosRight = pos;
			}
			m_DescriptionList.GetNext(pos); 
			if (insertPosLeft && insertPosRight)
				break;
		}
	}

	if ( ( ! insertPosLeft && insertPosRight) || (insertPosLeft && ! insertPosRight) )	// must find either no parts or both parts
		return 0;

	int nIndex = m_FoldSheetList.GetCount();
	CDescriptionItem LeftItem, RightItem;
	LeftItem.m_nFoldSheetIndex = nIndex;
	LeftItem.m_nFoldSheetPart  = CDescriptionItem::FoldSheetLeftPart;
	LeftItem.m_nPart		   = nPart;
	LeftItem.m_nSection		   = nSection;
	RightItem.m_nFoldSheetIndex = nIndex;
	RightItem.m_nFoldSheetPart  = CDescriptionItem::FoldSheetRightPart;
	RightItem.m_nPart			= nPart;
	RightItem.m_nSection		= nSection;

	int nNumAdded = 0;
	if ( ! insertPosRight) // No foldsheets there 
	{					
		m_DescriptionList.AddHead(LeftItem);
		insertPosLeft  = m_DescriptionList.GetHeadPosition(); 
		m_DescriptionList.AddTail(RightItem);
		insertPosRight = m_DescriptionList.GetTailPosition(); 

		m_FoldSheetList.AddTail(*pFoldSheet);
		if (pNewFoldSheets)
			pNewFoldSheets->Add(nIndex);
		nNumAdded++;
		nCount--;
		nIndex++;
	}

	while (nCount > 0)
	{
		LeftItem.m_nFoldSheetIndex  = nIndex;
		RightItem.m_nFoldSheetIndex = nIndex;
		if (pFoldSheet->GetBoundProduct().m_bindingParams.m_bindingDefs.m_nBinding == CFinalProductDescription::SaddleStitched)
		{
			m_DescriptionList.InsertAfter(insertPosLeft, LeftItem);	// stick into
			m_DescriptionList.GetNext(insertPosLeft); 
			m_DescriptionList.InsertBefore(insertPosRight, RightItem);
			m_DescriptionList.GetPrev(insertPosRight); 
		}
		else
		{
			m_DescriptionList.InsertAfter(insertPosRight, LeftItem);
			m_DescriptionList.GetNext(insertPosRight); 
			m_DescriptionList.InsertAfter(insertPosRight, RightItem);
			m_DescriptionList.GetNext(insertPosRight); 
		}

		m_FoldSheetList.AddTail(*pFoldSheet);
		if (pNewFoldSheets)
			pNewFoldSheets->Add(nIndex);
		nNumAdded++;
		nCount--;
		nIndex++;
	}

	return nNumAdded;
}

// Add nCount foldsheets of type *pFoldSheet to bookblock under ComingAndGoing conditions
int CBookblock::AddFoldSheetsSimpleCG(CFoldSheet* pFoldSheet, int nCount, CArray <int, int>* pNewFoldSheets, int nPart, int nSection) 
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	if ( ! pFoldSheet)
		return 0;
	if (nCount <= 0)
		return 0;

	CFoldSheet* pFirstFoldSheetInstance = pDoc->m_Bookblock.FindFoldSheet(pFoldSheet->m_nProductPartIndex, pFoldSheet->m_strFoldSheetTypeName);
	int nFirstInstanceIndex				= pDoc->m_Bookblock.GetFoldSheetIndex(pFirstFoldSheetInstance);
	int nInsertionIndex					= (nFirstInstanceIndex >= 0) ? (pDoc->m_Bookblock.m_FoldSheetList.GetCount() - nFirstInstanceIndex) : (pDoc->m_Bookblock.m_FoldSheetList.GetCount()/2);
	if (nFirstInstanceIndex < 0)
		if (pDoc->m_Bookblock.m_FoldSheetList.GetCount() % 2)
			nInsertionIndex++;
	POSITION foldSheetInsertPos	= pDoc->m_Bookblock.m_FoldSheetList.FindIndex(nInsertionIndex);

	// new foldsheets will be inserted mirrorwise, so we get symmetrical bookblock 

	POSITION insertPosLeft  = NULL; 
	POSITION insertPosRight = NULL;
	CDescriptionItem item; 
	POSITION pos = m_DescriptionList.GetTailPosition(); 
	while (pos)
	{
		item = m_DescriptionList.GetAt(pos); 
		if (item.m_nFoldSheetIndex == nInsertionIndex)
		{
			if (item.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
				insertPosRight  = pos;
		}
		m_DescriptionList.GetPrev(pos); 
		if (insertPosRight)
			break;
	}

	pos = m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& item = m_DescriptionList.GetNext(pos);
		if (item.m_nFoldSheetIndex >= nInsertionIndex)
			item.m_nFoldSheetIndex++;
	}

	CDescriptionItem LeftItem, RightItem;
	LeftItem.m_nFoldSheetIndex = nInsertionIndex;
	LeftItem.m_nFoldSheetPart  = CDescriptionItem::FoldSheetLeftPart;
	LeftItem.m_nPart		   = nPart;
	LeftItem.m_nSection		   = nSection;
	RightItem.m_nFoldSheetIndex = nInsertionIndex;
	RightItem.m_nFoldSheetPart  = CDescriptionItem::FoldSheetRightPart;
	RightItem.m_nPart			= nPart;
	RightItem.m_nSection		= nSection;

	int nNumAdded = 0;
	if ( ! insertPosRight) // No foldsheets there 
	{					
		m_DescriptionList.AddTail(LeftItem);
		m_DescriptionList.AddTail(RightItem);
		insertPosRight = m_DescriptionList.GetTailPosition(); 

		m_FoldSheetList.AddTail(*pFoldSheet);
		if (pNewFoldSheets)
			pNewFoldSheets->Add(nInsertionIndex);
		nNumAdded++;
		nCount--;
		nInsertionIndex++;
	}

	while (nCount > 0)
	{
		LeftItem.m_nFoldSheetIndex  = nInsertionIndex;
		RightItem.m_nFoldSheetIndex = nInsertionIndex;

		m_DescriptionList.InsertAfter(insertPosRight, LeftItem);
		m_DescriptionList.GetNext(insertPosRight); 
		m_DescriptionList.InsertAfter(insertPosRight, RightItem);
		m_DescriptionList.GetNext(insertPosRight); 

		if (foldSheetInsertPos)
			m_FoldSheetList.InsertAfter(foldSheetInsertPos, *pFoldSheet);
		else
			m_FoldSheetList.AddTail(*pFoldSheet);
		if (pNewFoldSheets)
			pNewFoldSheets->Add(nInsertionIndex);
		nNumAdded++;
		nCount--;
		nInsertionIndex++;
	}

	return nNumAdded;
}

void CBookblock::InsertFoldSheetAfter(CFoldSheet* pInsertAfterFoldSheet, CFoldSheet* pNewFoldSheet)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! pNewFoldSheet || ! pInsertAfterFoldSheet)
		return;

	int nBindingMethod = pDoc->m_boundProducts.GetProductPart(pInsertAfterFoldSheet->m_nProductPartIndex).GetBoundProduct().m_bindingParams.m_bindingDefs.m_nBinding;

	// new foldsheets will be appended after last existing foldsheet of same type (if there is any)
	// if job's binding method is saddlestitched, new foldsheets will be wrapped around
	CDescriptionItem RightItem; 
	POSITION		 insertPosRight = m_DescriptionList.GetTailPosition(); 

	// Search for foldsheet in bookblock - save position in insertPosRight and item in RightItem
	while (insertPosRight)
	{
		RightItem = m_DescriptionList.GetAt(insertPosRight); 
		if (RightItem.GetFoldSheetPointer(this) == pInsertAfterFoldSheet)
			break;
		m_DescriptionList.GetPrev(insertPosRight); 
	}

	CDescriptionItem LeftItem;
	POSITION		 insertPosLeft = m_DescriptionList.GetHeadPosition(); 

	// If binding method in job settings is saddlestitched we search for corresponding left item of above founded RightItem
	// in order to wrap the new foldsheets around the founded foldsheet
	// If binding method is PerfectBound, we don't need the left item's position
	if (nBindingMethod == CFinalProductDescription::SaddleStitched)
	{
		while (insertPosLeft)
		{
			LeftItem = m_DescriptionList.GetAt(insertPosLeft); 
			if (LeftItem.m_nFoldSheetIndex == RightItem.m_nFoldSheetIndex)
				break;
			m_DescriptionList.GetNext(insertPosLeft); 
		}
	}

	int nIndex = m_FoldSheetList.GetCount();
	LeftItem.m_nFoldSheetIndex = nIndex;
	LeftItem.m_nFoldSheetPart  = CDescriptionItem::FoldSheetLeftPart;
	RightItem.m_nFoldSheetIndex = nIndex;
	RightItem.m_nFoldSheetPart  = CDescriptionItem::FoldSheetRightPart;

	if (!insertPosRight) // No foldsheets of same type found or bookblock is empty
	{					 // - so add first new foldsheet and initialize insertPosLeft and insertPosRight
		if (nBindingMethod == CFinalProductDescription::SaddleStitched)
		{
			m_DescriptionList.AddHead(LeftItem);
			insertPosLeft  = m_DescriptionList.GetHeadPosition(); 
		}
		else
		{
			m_DescriptionList.AddTail(LeftItem);
			insertPosLeft  = m_DescriptionList.GetTailPosition(); 
		}

		m_DescriptionList.AddTail(RightItem);
		insertPosRight = m_DescriptionList.GetTailPosition(); 

		m_FoldSheetList.AddTail(*pNewFoldSheet);
		nIndex++;
	}
	else
	{
		LeftItem.m_nFoldSheetIndex = nIndex;
		if (nBindingMethod == CFinalProductDescription::SaddleStitched)
		{
			m_DescriptionList.InsertBefore(insertPosLeft, LeftItem);	// wrap around
			m_DescriptionList.GetPrev(insertPosLeft); 
		}
		else
		{
			m_DescriptionList.InsertAfter(insertPosRight, LeftItem);
			m_DescriptionList.GetNext(insertPosRight); 
		}

		RightItem.m_nFoldSheetIndex = nIndex;
		m_DescriptionList.InsertAfter(insertPosRight, RightItem);
		m_DescriptionList.GetNext(insertPosRight); 

		m_FoldSheetList.AddTail(*pNewFoldSheet);
		nIndex++;
	}
}

void CBookblock::ChangeFoldSheet(int nFoldSheetIndex, CFoldSheet* pNewFoldSheet) 
{
	if ( ! pNewFoldSheet)
		return;

	CFoldSheet* pFoldSheet = GetFoldSheetByIndex(nFoldSheetIndex);
	if ( ! pFoldSheet)
		return;

	int nNum = pFoldSheet->GetNumberOnly();
	*pFoldSheet = *pNewFoldSheet;
	pFoldSheet->m_nFoldSheetNumber = nNum;
}

void CBookblock::ChangeFoldSheets(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, int nProductPartIndex, CLayout* pLayout, CArray <int, int>* pNewFoldSheets) 
{
	if ( ! pNewFoldSheet)
		return;

	if (pOldFoldSheet != pNewFoldSheet)
	{
		int		 nLayerIndex		  = 0;
		int		 nIndex				  = -1;
		CString  strFoldSheetTypeName = pOldFoldSheet->m_strFoldSheetTypeName;
		POSITION pos				  = m_FoldSheetList.GetHeadPosition(); 
		while (pos)
		{
			nIndex++;
			CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
			if (nProductPartIndex >= 0)
				if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
					continue;

			if ( (rFoldSheet.m_strFoldSheetTypeName == strFoldSheetTypeName) && (rFoldSheet.GetLayout(nLayerIndex) == pLayout) )
			{
				int nNum = rFoldSheet.GetNumberOnly();
				rFoldSheet = *pNewFoldSheet;
				rFoldSheet.m_nFoldSheetNumber = nNum;
				if (pNewFoldSheets)
					pNewFoldSheets->Add(nIndex);
			}
		}
	}
}

// Remove foldsheets not placed on any printsheet
void CBookblock::RemoveUnplacedFoldSheets(int nProductPartIndex)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int		 nIndex = -1;
	POSITION pos	= m_FoldSheetList.GetHeadPosition(); 
	while (pos)
	{
		nIndex++;
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if (nProductPartIndex >= 0)
			if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
				continue;

		int nLayerIndex = 0;

		if ( ! rFoldSheet.GetPrintSheet(nLayerIndex))
		{
			RemoveFoldSheet(nIndex);

			if (rFoldSheet.GetBoundProduct().m_bindingParams.m_bComingAndGoing)	
			{
				if (nIndex > (m_FoldSheetList.GetSize() + 1) / 2)
					break;
				int nIndexCGLast  = m_FoldSheetList.GetCount() - 1;
				int nIndexCGGoing = nIndexCGLast - nIndex; 
				RemoveFoldSheet(nIndexCGGoing);
			}
		}
	}

	RemoveUnusedFoldSheets();
}


// Remove foldsheet corresponding to index from BookBlock
void CBookblock::RemoveFoldSheet(int nIndex) 
{												  
	POSITION itemPos = m_DescriptionList.GetHeadPosition();
	while (itemPos)
	{
		POSITION pos = itemPos;
		CDescriptionItem& rItem = m_DescriptionList.GetNext(itemPos);	// remove only from description list
		if (rItem.m_nFoldSheetIndex == nIndex)							// removal of corresponding foldsheet
			m_DescriptionList.RemoveAt(pos);							// happens later (in reorganization phase)
	}
} 


// Remove last nCount foldsheets from BookBlock
void CBookblock::RemoveFoldSheets(CFoldSheet* pFoldSheet, int nCount, int nProductPartIndex) 
{
	if ( ! pFoldSheet)
		return;

	int	nIndex = m_FoldSheetList.GetCount() - 1;
	
	ASSERT(nIndex >= 0);

	if (nIndex == -1)
		return;

	POSITION pos = m_FoldSheetList.GetTailPosition(); 
	while (pos && nCount)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetPrev(pos); 
		if (nProductPartIndex >= 0)
			if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
			{
				nIndex--;
				continue;
			}

		if (rFoldSheet.m_strFoldSheetTypeName == pFoldSheet->m_strFoldSheetTypeName)
		{
			RemoveFoldSheet(nIndex);
			nCount--; 
		}
		nIndex--;
	}
}

// Remove all foldsheets belonging to product part from BookBlock
void CBookblock::RemoveFoldSheets(int nProductPartIndex) 
{
	int	nIndex = m_FoldSheetList.GetCount() - 1;
	
	if (nIndex < 0)
		return;

	POSITION pos = m_FoldSheetList.GetTailPosition(); 
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetPrev(pos); 
		if (nProductPartIndex >= 0)
			if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
			{
				nIndex--;
				continue;
			}

		RemoveFoldSheet(nIndex);
		nIndex--;
	}
}


// Remove foldsheets selected in display list
void CBookblock::RemoveSelectedFoldSheets(CDisplayList& rDisplayList) 
{
	for (int i = 0; i < rDisplayList.m_List.GetSize(); i++)
	{
		if (rDisplayList.m_List[i].m_nState != CDisplayItem::Normal)
		{
			CDescriptionItem* pItem  = (CDescriptionItem*)rDisplayList.m_List[i].m_pData;
			if (pItem->m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
				RemoveFoldSheet(pItem->m_nFoldSheetIndex);
		}
	}
}


void CBookblock::ReNumberFoldSheets(int nProductPartIndex)	// assign new foldsheet numbers and page indices
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (nProductPartIndex == ALL_PARTS)
	{
		for (int nProductIndex = 0; nProductIndex < pDoc->m_boundProducts.GetSize(); nProductIndex++)
		{
			ReNumberFoldSheets(-nProductIndex - 1);
		}
		return;
	}

	CArray <short*, short*> PageSequence;

/*	POSITION				pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);	// 'Normalize' foldsheets - give right parts high enough indices
		rFoldSheet.GetPageSequence(PageSequence, -1);			// otherwise in the loop below the left parts could get
		nPageIndex = 0;											// higher indices than the corresponding right parts, what would
		for (unsigned i = 0; i < rFoldSheet.m_nPagesLeft; i++)	// bring problems when sorting the sequences
			*PageSequence[i] = ++nPageIndex;
		nPageIndex = (short)(SHRT_MAX - PageSequence.GetSize());
		for (i = rFoldSheet.m_nPagesLeft; i < (unsigned)PageSequence.GetSize(); i++)
			*PageSequence[i] = ++nPageIndex;
	}*/

	CBoundProduct& rBoundProduct = (nProductPartIndex < 0) ? pDoc->GetBoundProduct(abs(nProductPartIndex) - 1) : pDoc->GetBoundProductPart(nProductPartIndex).GetBoundProduct();
	short nPageIndex			 = 0;
	int	  nCoverProductPartIndex = rBoundProduct.GetCoverProductPartIndex();
	if (nCoverProductPartIndex >= 0)
		if ( ! pDoc->m_Bookblock.GetFirstFoldSheet(nCoverProductPartIndex))		// if job has cover but bookblock has no corresponding foldsheet 
			nPageIndex = 2;														// -> pagination starts with page number 3

	POSITION pos = m_DescriptionList.GetHeadPosition();

	if ( ! GetBindingProcess(nProductPartIndex).m_bComingAndGoing)	// Normal
	{
		int	nPrevPageNum	= -1;
		int nFoldSheetNum	= 1;
		while (pos)
		{
			CDescriptionItem& rItem = m_DescriptionList.GetNext(pos);
			CFoldSheet&	rFoldSheet = rItem.GetFoldSheet(this);
			if ( ! rItem.GetFoldSheet(this).IsPartOfBoundProduct(nProductPartIndex))
				continue;

			rFoldSheet.GetPageSequence(PageSequence, rItem.m_nFoldSheetPart);

			//if ( (rFoldSheet.IsCover()) && (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart) )
			//	nPageIndex = rFoldSheet.GetBoundProduct().m_nNumPages - PageSequence.GetSize();

			for (int i = 0; i < PageSequence.GetSize(); i++)
				if (*PageSequence[i] > 0)
				{
					if (*PageSequence[i] != nPrevPageNum)
						nPageIndex++;
					nPrevPageNum = *PageSequence[i];
					*PageSequence[i] = nPageIndex;
				}

			if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
				if ( ! rFoldSheet.m_bFoldSheetNumLocked)
					rFoldSheet.m_nFoldSheetNumber = nFoldSheetNum++;
		}
	}
	else	// Coming And Going
	{
		int	nPrevPageNum  = -1;
		int nFoldSheetNum = 1;
		while (pos)
		{
			CDescriptionItem& rItem = m_DescriptionList.GetNext(pos);
			CFoldSheet&	rFoldSheet = rItem.GetFoldSheet(this);
			if ( ! rItem.GetFoldSheet(this).IsPartOfBoundProduct(nProductPartIndex))
				continue;
			//if (nProductPartIndex != -1)
			//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
			//		continue;

			rFoldSheet.GetPageSequence(PageSequence, rItem.m_nFoldSheetPart);

			for (int i = 0; i < PageSequence.GetSize(); i++)
			{
				if (*PageSequence[i] > 0)
				{
					if (*PageSequence[i] != nPrevPageNum)
						nPageIndex++;
					nPrevPageNum = *PageSequence[i];
					*PageSequence[i] = nPageIndex;
				}
			}

			if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
				if ( ! rFoldSheet.m_bFoldSheetNumLocked)
					rFoldSheet.m_nFoldSheetNumber = nFoldSheetNum++;

			if (pos)
				m_DescriptionList.GetNext(pos);
		}

		nPageIndex++;
		pos			 = m_DescriptionList.GetHeadPosition();
		nPrevPageNum = -1;
		while (pos)
		{
			m_DescriptionList.GetNext(pos);
			if (!pos)
				break;

			CDescriptionItem& rItem = m_DescriptionList.GetNext(pos);
			CFoldSheet&	rFoldSheet = rItem.GetFoldSheet(this);
			if ( ! rItem.GetFoldSheet(this).IsPartOfBoundProduct(nProductPartIndex))
				continue;
			//if (nProductPartIndex != -1)
			//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
			//		continue;

			rFoldSheet.GetPageSequence(PageSequence, rItem.m_nFoldSheetPart);

			for (int i = PageSequence.GetSize() - 1; i >= 0; i--)
				if (*PageSequence[i] > 0)
				{
					if (*PageSequence[i] != nPrevPageNum)
						nPageIndex--;
					nPrevPageNum = *PageSequence[i];
					*PageSequence[i] = nPageIndex;
				}
		}

		pos = m_FoldSheetList.GetHeadPosition();
		while (pos)
		{
			CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
			CFoldSheet* pSimilarFoldSheet = m_FoldSheetList.CGGetSimilarFoldSheet(rFoldSheet, INT_MAX);
			if (pSimilarFoldSheet)
				rFoldSheet.m_strNumerationStyle.Format(_T(" %d/%d "), rFoldSheet.m_nFoldSheetNumber, pSimilarFoldSheet->GetNumberOnly()); 
		}
	}
}


// Remove eventually no longer used foldsheets (deleted in bookblock) from bookblock
// and from printsheet list
void CBookblock::RemoveUnusedFoldSheets(BOOL bDontRemoveCGGoing, BOOL bNeverRemovePrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nIndex = GetNextUnusedFoldSheet(0);
	while (nIndex >= 0)
	{
		CFoldSheet* pFoldSheet = GetFoldSheetByIndex(nIndex);
		BOOL bComingAndGoing = (pFoldSheet) ? pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing : FALSE;
		BOOL bRemovePrintSheet = TRUE;
		if (bComingAndGoing && bDontRemoveCGGoing)		// coming and going
		{
			if ( ! pFoldSheet)
				continue;
			int	nNum = GetNumFoldSheets(pFoldSheet->m_nProductPartIndex);
			bRemovePrintSheet = (nNum % 2) ? TRUE : FALSE;
		}

		BOOL bPrintSheetsReindexed = FALSE;
		if (bNeverRemovePrintSheet)
			;
		else
			if (bRemovePrintSheet)
			{
				pDoc->m_PrintSheetList.RemoveFoldSheet(nIndex);
				pDoc->m_PrintSheetList.ReIndexFoldSheets(nIndex);
				bPrintSheetsReindexed = TRUE;
			}

		POSITION pos = m_FoldSheetList.FindIndex(nIndex);
		if (pos)
		{
			m_FoldSheetList.RemoveAt(pos);
			ReIndexFoldSheets(nIndex);
			if ( ! bPrintSheetsReindexed)
				pDoc->m_PrintSheetList.ReIndexFoldSheets(nIndex);
		}
		nIndex = GetNextUnusedFoldSheet(nIndex);
	}
}


int CBookblock::GetNextUnusedFoldSheet(int nIndex)
{
	for (; nIndex < m_FoldSheetList.GetCount(); nIndex++)
	{
		BOOL	 bFound  = FALSE;
		POSITION itemPos = m_DescriptionList.GetHeadPosition();
		while (itemPos)
		{
			CDescriptionItem& rItem = m_DescriptionList.GetNext(itemPos);
			if (rItem.m_nFoldSheetIndex == nIndex)
			{
				bFound = TRUE;
				break;
			}
		}
		if (!bFound)
			return nIndex;
	}
	return -1;
}


// After a foldsheet has been removed we have to decrement all indices above the index of deleted foldsheet
void CBookblock::ReIndexFoldSheets(int nIndex)
{
	POSITION pos = m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem = m_DescriptionList.GetNext(pos);
		if (rItem.m_nFoldSheetIndex > nIndex)
			rItem.m_nFoldSheetIndex--;
	}
}


// select all foldsheets in bookblock of type strName
void CBookblock::SelectFoldSheets(CString& strName, CDisplayList& rDisplayList)	
{																				
	POSITION pos = m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem = m_DescriptionList.GetNext(pos);
		CFoldSheet&		  rFoldSheet = rItem.GetFoldSheet(this);

		if (rFoldSheet.m_strFoldSheetTypeName == strName)
		{
			CDisplayItem* pDI = rDisplayList.GetItemFromData((void*)&rItem);
			if (pDI)
				if (pDI->m_nState == CDisplayItem::Normal)
				{
					pDI->m_nState = CDisplayItem::Selected;
					rDisplayList.GiveStateFeedback(*pDI);	
				}
		}
	}
}


// select foldsheet (nIndex) in bookblock 
void CBookblock::SelectFoldSheet(int nIndex, CDisplayList& rDisplayList)	
{																				
	POSITION pos = m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem = m_DescriptionList.GetNext(pos);

		if (rItem.m_nFoldSheetIndex == nIndex)
		{
			CDisplayItem* pDI = rDisplayList.GetItemFromData((void*)&rItem);
			if (pDI)
				if (pDI->m_nState == CDisplayItem::Normal)
				{
					pDI->m_nState = CDisplayItem::Selected;
					rDisplayList.GiveStateFeedback(*pDI);	
				}
		}
	}
}

// deselect foldsheet (nIndex) in bookblock 
void CBookblock::DeselectFoldSheet(int nIndex, CDisplayList& rDisplayList)	
{																				
	POSITION pos = m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem = m_DescriptionList.GetNext(pos);

		if (rItem.m_nFoldSheetIndex == nIndex)
		{
			CDisplayItem* pDI = rDisplayList.GetItemFromData((void*)&rItem);
			if (pDI)
				if (pDI->m_nState != CDisplayItem::Normal)
				{
					pDI->m_nState = CDisplayItem::Normal;
					rDisplayList.InvalidateItem(pDI);	
				}
		}
	}
}


// For all selected items set binding method to 'nBindingMethod'
void CBookblock::ModifyBindingMethod(int nBindingMethod, CDisplayList& rDisplayList) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int	  nInsertIndex = -1;
	CList <CDescriptionItem, CDescriptionItem&> tempDList;
	CList <CFoldSheet, CFoldSheet&> tempFList;
	tempDList.RemoveAll();
	tempFList.RemoveAll();
	for (int i = 0; i < rDisplayList.m_List.GetSize(); i++)
	{
		if (rDisplayList.m_List[i].m_nState != CDisplayItem::Normal)
		{
			CDescriptionItem* pItem = (CDescriptionItem*)rDisplayList.m_List[i].m_pData;
			tempDList.AddTail(*pItem);

			if (nInsertIndex == -1)	// index where modified foldsheets will be inserted
				nInsertIndex = i;

			if (pItem->m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			{
				CFoldSheet& rFoldSheet = pItem->GetFoldSheet(this);
				tempFList.AddTail(rFoldSheet);
			}
		}
	}

	for (int i = 0; i < rDisplayList.m_List.GetSize(); i++)
	{
		if (rDisplayList.m_List[i].m_nState != CDisplayItem::Normal)
		{
			CDescriptionItem* pItem = (CDescriptionItem*)rDisplayList.m_List[i].m_pData;
			if (pItem->m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
				RemoveFoldSheet(pItem->m_nFoldSheetIndex);
		}
	}

	if (nInsertIndex == -1)
		return;

	BindingMethodReorganize(nBindingMethod, nInsertIndex, tempFList, ALL_PARTS);
}

// For all items of producttype set binding method to 'nBindingMethod'
void CBookblock::ModifyBindingMethod(int nBindingMethod, int nProductPartIndex) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int	  nInsertIndex = -1;
	CList <CDescriptionItem, CDescriptionItem&> tempDList;
	CList <CFoldSheet, CFoldSheet&> tempFList;
	CArray <int, int> foldSheetIndices;
	tempDList.RemoveAll();
	tempFList.RemoveAll();
	POSITION pos	= pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	int		 nIndex = 0;
	while (pos)
	{
		CDescriptionItem& rItem = pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		if ( ! rItem.GetFoldSheet(this).IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rItem.GetFoldSheet(this).m_nProductPartIndex != nProductPartIndex)
		//		continue;

		tempDList.AddTail(rItem);

		if (nInsertIndex == -1)	// index where modified foldsheets will be inserted
			nInsertIndex = nIndex;

		if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		{
			CFoldSheet& rFoldSheet = rItem.GetFoldSheet(this);
			//if ( (rFoldSheet.m_nProductPartIndex == nProductPartIndex) || (nProductPartIndex == -1) )
			if (rItem.GetFoldSheet(this).IsPartOfBoundProduct(nProductPartIndex))
			{
				tempFList.AddTail(rFoldSheet);
				foldSheetIndices.Add(rItem.m_nFoldSheetIndex);
			}
		}
		nIndex++;
	}

	pos	= pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem = pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		if ( ! rItem.GetFoldSheet(this).IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rItem.GetFoldSheet(this).m_nProductPartIndex != nProductPartIndex)
		//		continue;

		if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
			RemoveFoldSheet(rItem.m_nFoldSheetIndex);
	}

	if (nBindingMethod == CFinalProductDescription::SaddleStitched)
	{
		CBoundProductPart* pProductPart = pDoc->m_boundProducts.GetFirstPartOfBoundProduct(nProductPartIndex);
		while (pProductPart)
		{
			//if (nProductPartIndex != -1)
			//	if (nProductPartIndex != i)
			//		continue;

			//pProductPart->GetProductionProfile().m_postpress.m_binding.m_fMillingDepth	= 0.0f;
			//pProductPart->GetProductionProfile().m_postpress.m_binding.m_fGlueLineWidth	= 0.0f;

			pProductPart = pDoc->m_boundProducts.GetNextPartOfBoundProduct(pProductPart, nProductPartIndex);
		}
	}
	
	if (nInsertIndex == -1)
		return;

	BindingMethodReorganize(nBindingMethod, nInsertIndex, tempFList, nProductPartIndex, &foldSheetIndices);
}

void CBookblock::BindingMethodReorganize(int nBindingMethod, int nInsertIndex, CList <CFoldSheet, CFoldSheet&>& tempFList, int nProductPartIndex, CArray <int, int>* pFoldSheetIndices)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int				 nCount = m_FoldSheetList.GetCount(); 
	CDescriptionItem Item;
	POSITION		 insertPos = m_DescriptionList.FindIndex(nInsertIndex); 
	POSITION		 pos	   = tempFList.GetHeadPosition(); 
	int				 nIndex	   = 0;

	if (nBindingMethod == CFinalProductDescription::PerfectBound)
	{
		while (pos)
		{
			CFoldSheet& rFoldSheet = tempFList.GetNext(pos);

			if (pFoldSheetIndices)
				Item.m_nFoldSheetIndex = pFoldSheetIndices->GetAt(nIndex++);
			else
				Item.m_nFoldSheetIndex = nCount;
			Item.m_nFoldSheetPart  = CDescriptionItem::FoldSheetLeftPart;
			if (insertPos)
				m_DescriptionList.InsertBefore(insertPos, Item); 
			else
				m_DescriptionList.AddTail(Item); 

			Item.m_nFoldSheetPart  = CDescriptionItem::FoldSheetRightPart;
			if (insertPos)
				m_DescriptionList.InsertBefore(insertPos, Item); 
			else
				m_DescriptionList.AddTail(Item); 
			
			//if (nProductPartIndex >= 0)
			//	m_FoldSheetList.AddTail(rFoldSheet); 
			nCount++;
		}

		if ( (nProductPartIndex < 0) && (nProductPartIndex != ALL_PARTS) )
			pDoc->GetBoundProduct(-nProductPartIndex - 1).m_bindingParams.m_bindingDefs.m_nBinding = CFinalProductDescription::PerfectBound;
	}
	else
	{
		nIndex = 0;
		while (pos)
		{
			CFoldSheet& rFoldSheet = tempFList.GetNext(pos);

			if (pFoldSheetIndices)
				Item.m_nFoldSheetIndex = pFoldSheetIndices->GetAt(nIndex++);
			else
				Item.m_nFoldSheetIndex = nCount;
			Item.m_nFoldSheetPart  = CDescriptionItem::FoldSheetLeftPart;
			if (insertPos)
				m_DescriptionList.InsertBefore(insertPos, Item); 
			else
				m_DescriptionList.AddTail(Item); 

			//if (nProductPartIndex < 0)
			//	m_FoldSheetList.AddTail(rFoldSheet); 
			nCount++;
		}
		nCount--;

		nIndex = (pFoldSheetIndices) ? pFoldSheetIndices->GetSize() - 1 : 0;
		pos	= tempFList.GetHeadPosition(); 
		while (pos)
		{
			tempFList.GetNext(pos);

			if (pFoldSheetIndices)
				Item.m_nFoldSheetIndex = pFoldSheetIndices->GetAt(nIndex--);
			else
				Item.m_nFoldSheetIndex = nCount;
			Item.m_nFoldSheetPart  = CDescriptionItem::FoldSheetRightPart;
			if (insertPos)
				m_DescriptionList.InsertBefore(insertPos, Item); 
			else
				m_DescriptionList.AddTail(Item); 

			nCount--;
		}

		if ( (nProductPartIndex < 0) && (nProductPartIndex != ALL_PARTS) )
			pDoc->GetBoundProduct(-nProductPartIndex - 1).m_bindingParams.m_bindingDefs.m_nBinding = CFinalProductDescription::SaddleStitched;
	}
}


// Cut selected items and paste to position nIndexDI
void CBookblock::CutAndPaste(int nIndexDI, BOOL bInsertAfter, CDisplayList& rDisplayList) 
{
	CList <CDescriptionItem, CDescriptionItem&> tempDList;
	tempDList.RemoveAll();
	for (int i = 0; i < rDisplayList.m_List.GetSize(); i++)
	{
		if (rDisplayList.m_List[i].m_nState != CDisplayItem::Normal)
		{
			CDescriptionItem* pItem	= (CDescriptionItem*)rDisplayList.m_List[i].m_pData;
			tempDList.AddTail(*pItem);
		}
	}
	// tempDList holds all selected items now

	
	POSITION pos = m_DescriptionList.FindIndex(nIndexDI);
	if (!pos)
		return;

	POSITION srcPos = tempDList.GetHeadPosition();
	while (srcPos)
	{
		CDescriptionItem Item  = tempDList.GetNext(srcPos);
		if (bInsertAfter)
		{
			m_DescriptionList.InsertAfter(pos, Item);
			m_DescriptionList.GetNext(pos);
		}
		else
			m_DescriptionList.InsertBefore(pos, Item);
	}

	tempDList.RemoveAll();

	for (int i = 0; i < rDisplayList.m_List.GetSize(); i++)
	{
		if (rDisplayList.m_List[i].m_nState != CDisplayItem::Normal)
		{
			CDescriptionItem* pItem = (CDescriptionItem*)rDisplayList.m_List[i].m_pData;

			pos = m_DescriptionList.GetHeadPosition();
			while (pos)
			{
				CDescriptionItem& rItem = m_DescriptionList.GetAt(pos);
				if (pItem == &rItem)
				{
					m_DescriptionList.RemoveAt(pos);
					break;
				}
				m_DescriptionList.GetNext(pos);
			}
		}
	}
}


// Copy selected items and paste to position nIndexDI
void CBookblock::CopyAndPaste(int nIndexDI, BOOL bInsertAfter, CDisplayList& rDisplayList, int nProductIndex) 
{
	if ( ! CImpManDoc::GetDoc())
		return;

	int nRestPages = CImpManDoc::GetDoc()->GetBoundProduct(nProductIndex).GetNumPages();	
	POSITION pos = m_FoldSheetList.GetHeadPosition();	// can be added to bookblock
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.GetBoundProduct().m_bindingParams.m_bComingAndGoing)	
			nRestPages-= rFoldSheet.m_nPagesLeft;
		else
			nRestPages-= rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight;
	}

	CList <CDescriptionItem, CDescriptionItem&> tempDList;
	tempDList.RemoveAll();
	for (int i = 0; i < rDisplayList.m_List.GetSize(); i++)
	{
		if (rDisplayList.m_List[i].m_nState != CDisplayItem::Normal)
		{
			CDescriptionItem* pItem	= (CDescriptionItem*)rDisplayList.m_List[i].m_pData;
			CFoldSheet&	rFoldSheet = pItem->GetFoldSheet(this);
			if (pItem->m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
				if (rFoldSheet.GetBoundProduct().m_bindingParams.m_bComingAndGoing)	
					nRestPages-= rFoldSheet.m_nPagesLeft;
				else
					nRestPages-= rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight;
			if (nRestPages >= 0)
				tempDList.AddTail(*pItem);
			else
			{
				AfxMessageBox(IDS_BOOKBLOCK_DROP_INVALID);
				break;
			}
		}
	}
	// tempDList holds all selected items now

	
	// Assign new foldsheet indices to description items and append new foldsheets to foldsheet list
	int		 nIndex = m_FoldSheetList.GetCount();
	POSITION srcPos = tempDList.GetHeadPosition();
	while (srcPos)
	{
		CDescriptionItem& rItem = tempDList.GetNext(srcPos);
		if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		{
			pos = srcPos;
			while (pos)
			{
				CDescriptionItem& rItemRight = tempDList.GetNext(pos);
				if ((rItemRight.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart) &&
				    (rItemRight.m_nFoldSheetIndex == rItem.m_nFoldSheetIndex))
				{
					rItemRight.m_nFoldSheetIndex = nIndex;
					break;
				}
			}
			m_FoldSheetList.AddTail(rItem.GetFoldSheet(this));
			rItem.m_nFoldSheetIndex = nIndex++;
		}
	}
	 
	pos = m_DescriptionList.FindIndex(nIndexDI);
	if (!pos)
		return;

	// Insert new description items into description list
	srcPos = tempDList.GetHeadPosition();
	while (srcPos)
	{
		CDescriptionItem Item = tempDList.GetNext(srcPos);
		if (bInsertAfter)
		{
			m_DescriptionList.InsertAfter(pos, Item);
			m_DescriptionList.GetNext(pos);
		}
		else
			m_DescriptionList.InsertBefore(pos, Item);
	}

	tempDList.RemoveAll();
}


// Reorganization of fold- and printsheets after bookblock manipulation
void CBookblock::Reorganize(CPrintSheet* pPrintSheet, CLayout* pLayout, int nProductPartIndex, BOOL bDontRemoveCGGoing)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	ArrangeCover();

	if (nProductPartIndex == ALL_PARTS)
	{
		nProductPartIndex = 0;
		POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
		while (pos)
		{
			CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
			for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
			{
				int nProductIndex = pDoc->GetBoundProductPart(nProductPartIndex).GetBoundProduct().GetIndex();
				RemoveUnusedFoldSheets(bDontRemoveCGGoing);
				ReNumberFoldSheets(-nProductIndex - 1);
				pDoc->m_PageTemplateList.SortPages();
				pDoc->m_PrintSheetList.TakeOverNewFoldSheets(pPrintSheet, pLayout, nProductPartIndex);

				nProductPartIndex++;
			}
		}
	}
	else
	{
		int nProductIndex = pDoc->GetBoundProductPart(nProductPartIndex).GetBoundProduct().GetIndex();
		RemoveUnusedFoldSheets(bDontRemoveCGGoing);
		ReNumberFoldSheets(-nProductIndex - 1);
		pDoc->m_PageTemplateList.SortPages();
		pDoc->m_PrintSheetList.TakeOverNewFoldSheets(pPrintSheet, pLayout, nProductPartIndex);
	}

	pDoc->m_PrintSheetList.RemoveUnneededFoldSheetsComingAndGoing();
	pDoc->m_PrintSheetList.SortByFoldSheets();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();

	CBookBlockDataView* pView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
	if (pView)
		if (pView->m_dlgShinglingAssistent.m_hWnd)
		{
			pView->m_dlgShinglingAssistent.InitShinglingValuesXTotal();
			pView->m_dlgShinglingAssistent.InitShinglingValuesYTotal();
		}

	pDoc->m_PageTemplateList.ReorganizeGluelineScaling();	
}

void CBookblock::ShiftProductPartIndices(int nNewIndex, int nNumNewParts)
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.m_nProductPartIndex >= nNewIndex)
			rFoldSheet.m_nProductPartIndex += nNumNewParts;
	}
}

void CBookblock::ArrangeCover()
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		
		int nProductIndex	  = rBoundProduct.GetIndex();
		int nProductPartIndex = rBoundProduct.GetCoverProductPartIndex();
		if (nProductPartIndex < 0)	// no cover
			continue;

		POSITION posLeftPart  = NULL;
		POSITION posRightPart = NULL;
		POSITION pos		  = GetHeadPosition_DescriptionList(nProductIndex); 
		while (pos)
		{
			CDescriptionItem& rItem = m_DescriptionList.GetAt(pos);
			CFoldSheet& rFoldSheet = rItem.GetFoldSheet(this);
			if (rFoldSheet.m_nProductPartIndex == nProductPartIndex)
				if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
					posLeftPart = pos;
				else
					if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
						posRightPart = pos;

			GetNext_DescriptionList(nProductIndex, pos);
		}

		if ( ! posLeftPart || ! posRightPart)
			continue;

		if ( (posLeftPart == GetHeadPosition_DescriptionList(nProductIndex)) && (posRightPart == GetTailPosition_DescriptionList(nProductIndex)) )	// OK so far
			continue;

		if (posLeftPart != GetHeadPosition_DescriptionList(nProductIndex))	
		{
			CDescriptionItem item = m_DescriptionList.GetAt(posLeftPart);
			m_DescriptionList.RemoveAt(posLeftPart);
			AddHead_DescriptionList(nProductIndex, item);
		}
		
		if (posRightPart != GetTailPosition_DescriptionList(nProductIndex))	
		{
			CDescriptionItem item = m_DescriptionList.GetAt(posRightPart);
			m_DescriptionList.RemoveAt(posRightPart);
			AddTail_DescriptionList(nProductIndex, item);
		}
	}
}

CFoldSheet* CBookblock::GetFoldSheetByNum(int nFoldSheetNumber, int nProductPartIndex)
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		if (rFoldSheet.m_nFoldSheetNumber == nFoldSheetNumber)
			return &rFoldSheet;
	}

	return NULL;
}

void CBookblock::SetShinglingValuesX(float fValue, float fValueFoot, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
	if (fValue != FLT_MAX)
		rProductPart.m_foldingParams.SetShinglingValueX(fValue);
	if (fValueFoot != FLT_MAX)
		rProductPart.m_foldingParams.SetShinglingValueXFoot(fValueFoot);

	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
//		if (rFoldSheet.m_nProductPartIndex == nProductPartIndex)
		{
			if (fValue != FLT_MAX)
				rFoldSheet.m_fShinglingValueX	 = fValue;
			if (fValueFoot != FLT_MAX)
				rFoldSheet.m_fShinglingValueXFoot = fValueFoot;
		}
	}
}

void CBookblock::SetShinglingValuesY(float fValue, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
	if (fValue != FLT_MAX)
		rProductPart.m_foldingParams.SetShinglingValueY(fValue);

	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
//		if (rFoldSheet.m_nProductPartIndex == nProductPartIndex)
		{
			if (fValue != FLT_MAX)
				rFoldSheet.m_fShinglingValueY	 = fValue;
		}
	}
}

int	CBookblock::GetFoldSheetIndex(CFoldSheet* pFoldSheet)
{
	int		 nIndex = 0;
	POSITION pos	= m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if (&rFoldSheet == pFoldSheet)
			return nIndex;
		nIndex++;
	}
	return -1;
}

void CBookblock::InitSortedFoldSheets(CArray <CFoldSheet*, CFoldSheet*>& sortedFoldSheets, int nProductPartIndex, CFoldSheet* pFoldSheet, CLayout* pLayout)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	sortedFoldSheets.RemoveAll();

	if (nProductPartIndex == ALL_PARTS)
	{
		CArray <CFoldSheet*, CFoldSheet*> tmpSortedFoldSheets;
		for (int nProductIndex = 0; nProductIndex < pDoc->m_boundProducts.GetSize(); nProductIndex++)
		{
			InitSortedFoldSheets(tmpSortedFoldSheets, -nProductIndex - 1, pFoldSheet, pLayout);
			qsort((void*)tmpSortedFoldSheets.GetData(), tmpSortedFoldSheets.GetSize(), sizeof(CFoldSheet*), CompareFoldSheetNum);
			sortedFoldSheets.Append(tmpSortedFoldSheets);
		}
		return;
	}

	ReNumberFoldSheets(-pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex) - 1);	// initialize m_nFoldSheetNumber member of CFoldSheet objects

	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		if (pFoldSheet)
			if (pFoldSheet->m_strFoldSheetTypeName != rFoldSheet.m_strFoldSheetTypeName)
				continue;
		if (pLayout)
		{
			int nLayerIndex = 0;
			if (pFoldSheet->GetLayout(nLayerIndex) != rFoldSheet.GetLayout(nLayerIndex))
				continue;
		}

		sortedFoldSheets.Add(&rFoldSheet);
	}

	qsort((void*)sortedFoldSheets.GetData(), sortedFoldSheets.GetSize(), sizeof(CFoldSheet*), CompareFoldSheetNum);
}

int CBookblock::CompareFoldSheetNum(const void *p1, const void *p2)
{
	CFoldSheet* pFoldSheet1 = *(CFoldSheet**)p1;
	CFoldSheet* pFoldSheet2 = *(CFoldSheet**)p2;

	if (pFoldSheet1->m_nFoldSheetNumber < pFoldSheet2->m_nFoldSheetNumber)
		return -1;
	else
		if (pFoldSheet1->m_nFoldSheetNumber > pFoldSheet2->m_nFoldSheetNumber)
			return 1;
		else
			return 0;
}

void CBookblock::GetFoldSheetTypes(CArray <CFoldSheet*, CFoldSheet*>& foldSheetTypes, CArray <int, int>& foldSheetTypeNums, int nProductPartIndex)
{
	foldSheetTypes.RemoveAll();
	foldSheetTypeNums.RemoveAll();
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
		//		continue;

		BOOL bFound = FALSE;
		for (int i = 0; i < foldSheetTypes.GetSize(); i++)
			if (foldSheetTypes[i]->m_strFoldSheetTypeName == rFoldSheet.m_strFoldSheetTypeName)	// already in list
			{
				POSITION pos = rFoldSheet.m_LayerList.GetHeadPosition();
				while (pos)
				{
					if (&rFoldSheet.m_LayerList.GetAt(pos) == &rFoldSheet.m_LayerList.GetHead())
						foldSheetTypeNums[i]++;
					rFoldSheet.m_LayerList.GetNext(pos);
				}
				bFound = TRUE;
				break;
			}

		if ( ! bFound)
		{
			POSITION pos = rFoldSheet.m_LayerList.GetHeadPosition();
			while (pos)
			{
				foldSheetTypes.Add(&rFoldSheet);
				if (&rFoldSheet.m_LayerList.GetAt(pos) == &rFoldSheet.m_LayerList.GetHead())
					foldSheetTypeNums.Add(1);
				else
					foldSheetTypeNums.Add(0);
				rFoldSheet.m_LayerList.GetNext(pos);
			}
		}
	}

//	qsort((void*)foldSheetTypes.GetData(), foldSheetTypes.GetSize(), sizeof(CFoldSheet*), CompareFoldSheetNum);		foldSheetTypeNums need also to be sorted
}



CPoint 		_ptLeftTop;
CPoint 		_ptLeftBottom;
CPoint 		_ptRightTop;
CPoint 		_ptRightBottom;
int	   		_nPageNum;
BOOL   		_bBreakPainting = FALSE;
BOOL   		_bCoverPainted  = FALSE;
int	   		_nViewBorder    = 5;
CSize  		_maxNumberSize;
CSize  		_maxNameSize;
CSize		_maxPageRangeSize;
CSize  		_extend;
CString		_strSelectedFoldSheet = "";
COLORREF	_gPenColor = RGB(120,120,180);//RGB(180,180,180); //RGB(210,210,210);
int			_nShingling;
CSize		_viewPortSize;
int			_nMaxPaperThickness	= 5;
int			_nShinglingValueXTotal = 0;
CRect		_rcBox(0,0,0,0);
CSize		_sizeRealPage;
CSize		_sizePreviewPage;


CSize CBookblock::GetMaxFoldSheetNumberExtent(CDC* pDC)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CSize(0,0);

	CFont font;
	int nSizeFont	  = (pDC->IsPrinting()) ? Pixel2MM(pDC, 6) : Pixel2ConstMM(pDC, 13);
	int nThresholdMax = (int)((float)_nMaxPaperThickness * 2.0f);
	int nThresholdMin = (int)((float)_nMaxPaperThickness * 1.5f);
	nSizeFont		  = min(nSizeFont, (pDC->IsPrinting()) ? nThresholdMax : Pixel2ConstMM(pDC, 13));
	nSizeFont		  = max(nSizeFont, (pDC->IsPrinting()) ? nThresholdMin : Pixel2ConstMM(pDC, 11));
	font.CreateFont(nSizeFont, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = (CFont*)pDC->SelectObject(&font);

	CString  strText;
	CSize	 maxExtent(0,0);
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		strText.Format(_T(" %s "), rFoldSheet.GetNumber()); 
		CSize extent = pDC->GetTextExtent(rFoldSheet.GetNumber());
		maxExtent.cx = max(maxExtent.cx, extent.cx);
		maxExtent.cy = max(maxExtent.cy, extent.cy);
	}

	pDC->SelectObject(pOldFont);
	font.DeleteObject();

	return maxExtent;
}

CSize CBookblock::GetMaxFoldSheetNameExtent(CDC* pDC)
{
	CFont font;
	int nSizeFont	  = (pDC->IsPrinting()) ? Pixel2MM(pDC, 6) : Pixel2ConstMM(pDC, 13);
	int nThresholdMax = (int)((float)_nMaxPaperThickness * 2.0f);
	int nThresholdMin = (int)((float)_nMaxPaperThickness * 1.5f);
	nSizeFont		  = min(nSizeFont, (pDC->IsPrinting()) ? nThresholdMax : Pixel2ConstMM(pDC, 13));
	nSizeFont		  = max(nSizeFont, (pDC->IsPrinting()) ? nThresholdMin : Pixel2ConstMM(pDC, 11));
	font.CreateFont(nSizeFont, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = (CFont*)pDC->SelectObject(&font);

	CSize maxExtent(0,0);
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		CSize extent = pDC->GetTextExtent(rFoldSheet.m_strFoldSheetTypeName);
		maxExtent.cx = max(maxExtent.cx, extent.cx);
		maxExtent.cy = max(maxExtent.cy, extent.cy);
	}

	pDC->SelectObject(pOldFont);
	font.DeleteObject();

	return maxExtent;
}

CSize CBookblock::GetMaxFoldSheetPageRangeExtent(CDC* pDC, int nProductPartIndex)
{
	CFont font;
	int nSizeFont	  = (pDC->IsPrinting()) ? Pixel2MM(pDC, 6) : Pixel2ConstMM(pDC, 13);
	int nThresholdMax = (int)((float)_nMaxPaperThickness * 2.0f);
	int nThresholdMin = (int)((float)_nMaxPaperThickness * 1.5f);
	nSizeFont		  = min(nSizeFont, (pDC->IsPrinting()) ? nThresholdMax : Pixel2ConstMM(pDC, 13));
	nSizeFont		  = max(nSizeFont, (pDC->IsPrinting()) ? nThresholdMin : Pixel2ConstMM(pDC, 11));
	font.CreateFont(nSizeFont, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = (CFont*)pDC->SelectObject(&font);

	CSize	 maxExtent(0,0);
	CString  strText;
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
		//		continue;

		strText.Format(_T(" %s "), rFoldSheet.GetPageRange(LEFT));
		CSize extent = pDC->GetTextExtent(strText);
		maxExtent.cx = max(maxExtent.cx, extent.cx);
		maxExtent.cy = max(maxExtent.cy, extent.cy);
	}

	pDC->SelectObject(pOldFont);
	font.DeleteObject();

	return maxExtent;
}

CRect CBookblock::DrawTitle(CDC* pDC, CRect rcFrame, BOOL bShowIcon)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	return rcBox;
}

CRect CBookblock::Draw(CDC* pDC, CRect rcFrame, int nProductPartIndex, CString strSelectedFoldSheet, COLORREF crBkColor)
{
	if ( ! pDC->RectVisible(rcFrame))
		return rcFrame;//CRect(rcFrame.TopLeft(), CSize(1, 1));

	CheckFoldSheetsPlausibility();

	rcFrame.right -= 5; rcFrame.bottom -= 5;

	_sizeRealPage	 = pDC->GetWindowExt();
	_sizePreviewPage = pDC->GetViewportExt();

	int nID = pDC->SaveDC();

	CRect rcClip = rcFrame; 
	pDC->IntersectClipRect(rcClip);

	CRect rcBackground = rcFrame; //rcBackground.NormalizeRect(); //rcBackground.top += 20;
	if (crBkColor != 0)
		pDC->FillSolidRect(rcBackground, crBkColor);

	pDC->LPtoDP(&rcFrame);

	CSize sizeViewBorder(Pixel2MM(pDC, _nViewBorder), Pixel2MM(pDC, _nViewBorder));
	pDC->LPtoDP(&sizeViewBorder);

	_viewPortSize.cx = max(rcFrame.Width()  - 2 * sizeViewBorder.cx, sizeViewBorder.cx);
	_viewPortSize.cy = max(rcFrame.Height() - 2 * sizeViewBorder.cy, sizeViewBorder.cy);

	_extend = GetExtend(nProductPartIndex);
	if (_extend == CSize(0,0))
	{
		pDC->RestoreDC(nID);
		return CRect(0,0,0,0);
	}

	int nThreshold = (pDC->IsPrinting()) ? 100 : 4;	// 1 mm or 4 screen pixels
	if (pDC->IsPrinting())
		nThreshold = (int)((float)nThreshold * ((float)_sizePreviewPage.cy / (_sizeRealPage.cy)) + 0.5f);		// adopt to preview page

	float fScalFactor = ((float)_viewPortSize.cy / (float)_extend.cy);
	if ( fScalFactor * _nMaxPaperThickness > nThreshold )	// limit paper thickness to threshold when ZoomToFit
	{
		_viewPortSize.cy = ((nThreshold * _extend.cy ) / _nMaxPaperThickness);	
		_extend = GetExtend(nProductPartIndex);
	}

	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(_extend);
	pDC->SetWindowOrg(0, _extend.cy);
	pDC->SetViewportExt(_viewPortSize.cx, -_viewPortSize.cy);    
	pDC->SetViewportOrg(rcFrame.left + sizeViewBorder.cx, rcFrame.top + sizeViewBorder.cy);

	_strSelectedFoldSheet = strSelectedFoldSheet;
	_ptLeftTop = _ptLeftBottom = _ptRightTop = _ptRightBottom = CPoint(0,0);
	CPoint startPoint(sizeViewBorder.cx, _extend.cy - sizeViewBorder.cy);

	_rcBox = CRect(startPoint, CSize(0,0));

	_bBreakPainting = FALSE;
	_bCoverPainted  = FALSE;

	_maxNumberSize	  = GetMaxFoldSheetNumberExtent(pDC); 
	_maxNameSize	  = GetMaxFoldSheetNameExtent(pDC); 
	_maxPageRangeSize = GetMaxFoldSheetPageRangeExtent(pDC, nProductPartIndex);

	_nShingling = 0;
	POSITION pos = GetHeadPosition_DescriptionList(-nProductPartIndex - 1);
	while (pos && !_bBreakPainting)
		DrawFoldSheet(pDC, pos, startPoint, nProductPartIndex);

	CPoint p[4];
	p[0] = _ptLeftBottom;
	p[1] = _ptLeftTop; 
	p[2] = _ptRightTop;
	p[3] = _ptRightBottom;

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		if (GetBindingProcess(nProductPartIndex).m_bComingAndGoing)	
		{
			CPen dashedPen;
			dashedPen.CreatePen(PS_DOT,   1, MIDDLEGRAY);
			CPen* pOldPen = (CPen*)pDC->SelectObject(&dashedPen);
			CPoint p1(p[0].x + ((p[1].x - p[0].x)*2)/3, p[0].y + ((p[1].y - p[0].y)*2)/3);
			CPoint p2(p[3].x + (p[2].x - p[3].x)/2, p1.y);
			pDC->MoveTo(p1);
			pDC->LineTo(p2);
			pDC->SelectObject(pOldPen);
			dashedPen.DeleteObject();
		}

	pDC->LPtoDP(_rcBox);
	_rcBox.NormalizeRect();

	pDC->RestoreDC(nID);

	pDC->DPtoLP(&_rcBox);

	_rcBox.right += 10, _rcBox.bottom += 10;

	return _rcBox;
}

void CBookblock::DrawFoldSheet(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex)
{
	CDescriptionItem Item = m_DescriptionList.GetAt(pos);

 	CPoint endPointLeft = DrawFoldSheetLeftPart(pDC, pos, startPoint, nProductPartIndex);

	while ((GetFoldSheetPart(pos) == CDescriptionItem::FoldSheetLeftPart) && !_bBreakPainting)
		DrawFoldSheet(pDC, pos, startPoint, nProductPartIndex);

	CPoint endPointRight = DrawFoldSheetRightPart(pDC, pos, startPoint, nProductPartIndex);

	DrawConnectionLeftRight(pDC, Item, endPointLeft, endPointRight, nProductPartIndex);
}

int CBookblock::GetFoldSheetPart(POSITION& pos)
{
	if (!pos)
		return CDescriptionItem::FoldSheetRightPart;

	CDescriptionItem Item = m_DescriptionList.GetAt(pos);
	return Item.m_nFoldSheetPart;
}

int	CBookblock::TransformFontSize(CDC* pDC, float fWorld, int nScreenMin)
{
	CSize fontSize = CSize(0, (int)fWorld);	// fWorld = size when layout plotted 1:1

	if ( ! pDC->IsPrinting())
	{
		pDC->LPtoDP(&fontSize);
		CSize sizeViewport = pDC->GetViewportExt();
		if (nScreenMin > 0)
		{
			if (fontSize.cy < nScreenMin)
				fontSize.cy = nScreenMin;
		}
		else
		if ( (abs(sizeViewport.cx) < 50) || (abs(sizeViewport.cy) < 50) )	// thumbnail
		{
			if (fontSize.cy < 5)
				fontSize.cy = 5;
			if (fontSize.cy > 8)
				fontSize.cy = 8;
		}
		else
		if ( (abs(sizeViewport.cx) < 150) || (abs(sizeViewport.cy) < 150) )	// thumbnail
		{
			if (fontSize.cy < 8)
				fontSize.cy = 8;
			if (fontSize.cy > 11)
				fontSize.cy = 11;
		}
		else
		{
			if (fontSize.cy < 11)
				fontSize.cy = 11;
			if (fontSize.cy > 13)
				fontSize.cy = 13;
		}
		pDC->DPtoLP(&fontSize);
	}
	return fontSize.cy;
}


CPoint CBookblock::DrawFoldSheetLeftPart(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex)
{
	if (!pos)
		return startPoint;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return startPoint;

	CDescriptionItem& rItem			  = m_DescriptionList.GetAt(pos);
	CFoldSheet&		  rFoldSheet	  = rItem.GetFoldSheet(this);

	int			r	 = GetFoldSheetRadiusLeftPart(pos, rFoldSheet.m_nProductPartIndex);
	CPoint		cp	 = startPoint + CSize(r, -r);

	if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
	{
		pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		return startPoint;
	}
	//if (nProductPartIndex != -1)
	//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
	//	{
	//		pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
	//		return startPoint;
	//	}

	//int nPages = rFoldSheet.m_nPagesLeft/2;
	int nPages = max(rFoldSheet.m_nPagesLeft/2, rFoldSheet.m_nPagesRight/2);
	int	nSheetThickness = nPages * CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex);	
	//if (nSheetThickness == 0)	// if only one page is on the sheet
	//	nSheetThickness = CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex) / 2;
	r-= CBookBlockView::GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nSheetThickness/2;

	RenderFoldSheet(pDC, rItem, nSheetThickness, cp, r, nProductPartIndex);

	BOOL bAboutToClose = FALSE;
	POSITION pos2nd = pos; 
	GetNext_DescriptionList(-nProductPartIndex - 1, pos2nd);
	if (pos2nd)
	{
		CDescriptionItem& r2ndItem = m_DescriptionList.GetAt(pos2nd);
		if ( (r2ndItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart) && 
			 (r2ndItem.m_nFoldSheetIndex == r2ndItem.m_nFoldSheetIndex) )		
			bAboutToClose = TRUE;
	}
	if ( ! bAboutToClose || (rFoldSheet.GetNumPages() <= 2))	// if sheet will be closed in next step, we draw text when right part is painted (see DrawFoldSheetRightPart())
	{															// or we have just one page
		CFont font, boldFont;
		int nFontSize = (pDC->IsPrinting()) ? 200 : 20;	// 2.5 mm or 11 screen pixels
		if (pDC->IsPrinting())
			nFontSize = (int)((float)nFontSize * ((float)_sizePreviewPage.cy / (_sizeRealPage.cy)) + 0.5f);		// adopt to preview page
		CSize sizeFont(nFontSize, nFontSize);
		font.CreateFont	   (sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		boldFont.CreateFont(sizeFont.cy, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));

		BOOL bDontDrawText = FALSE;
		CSize szSize = sizeFont;
		pDC->LPtoDP(&szSize);
		if (szSize.cy < 1)
			bDontDrawText = TRUE;

		CFont* pOldFont = (CFont*)pDC->SelectObject(&boldFont);

		CString  strText;
		int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
		COLORREF crOldBkColor	= pDC->SetBkColor(WHITE);
		COLORREF crOldTextColor = pDC->SetTextColor(RGB(200, 100, 30));
		strText.Format(_T(" %s "), rFoldSheet.GetNumber()); 

		//int nXStart = 5*nThresholdMax;
		//int nXPos = nXStart;
		CSize szDistance(20,20);
		pDC->DPtoLP(&szDistance);
		int nXPos = cp.x + szDistance.cx;
		int nYPos = (rFoldSheet.GetNumPages() <= 2) ? (cp.y + 2*(sizeFont.cy/3)) : (cp.y + r + sizeFont.cy/2);
		if ( ! bDontDrawText)
			pDC->TextOut(nXPos, nYPos, strText);

		pDC->SelectObject(&font);

		strText.Format(_T("%s"), rFoldSheet.m_strFoldSheetTypeName); 

		pDC->SetBkColor(WHITE);
		pDC->SetTextColor(BLACK);
		//nXPos += _maxNumberSize.cx + 3*nThresholdMax;
		nXPos += 4 * sizeFont.cy;
		if ( ! bDontDrawText)
			pDC->TextOut(nXPos, nYPos, strText);

		pDC->SetTextColor(DARKBLUE);
		strText.Format(_T(" %s "), rFoldSheet.GetPageRange(LEFT));
		nXPos += _maxNameSize.cx + sizeFont.cy;//3*nThresholdMax;
		if ( ! bDontDrawText)
			pDC->TextOut(nXPos, nYPos, strText);

		CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();
		if ( ! rProductPart.m_strName.IsEmpty() && ! bDontDrawText)
		{
			strText.Format(_T("%s"), rProductPart.m_strName); 
			CSize sizeText = pDC->GetTextExtent(strText);
			int   nXPosText = nXPos + _maxPageRangeSize.cx + 5 * sizeFont.cy;//3*nThresholdMax;
			if (nXPosText + sizeText.cx < _ptRightBottom.x)
			{
				pDC->SetBkColor(RGB(233,240,130));
				if ( ! bDontDrawText)
					pDC->TextOut(nXPosText, nYPos, strText);
			}
		}

		pDC->SetBkMode(nOldBkMode);
		pDC->SetBkColor(crOldBkColor);
		pDC->SetTextColor(crOldTextColor);

		pDC->SelectObject(pOldFont);
		font.DeleteObject();
		boldFont.DeleteObject();
	}

	int d = 2*CBookBlockView::GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nSheetThickness; 
	startPoint+= CSize(d,-d);

//	TRACE("<%d ", Item.m_nFoldSheetIndex); 

	GetNext_DescriptionList(-nProductPartIndex - 1, pos);

	return CPoint(cp.x - r, cp.y);
}

CPoint CBookblock::DrawFoldSheetRightPart(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex)
{
	if (!pos)
		return startPoint;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return startPoint;

	CDescriptionItem& rItem			  = m_DescriptionList.GetAt(pos);
	CFoldSheet&		  rFoldSheet	  = rItem.GetFoldSheet(this);
	if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
	{
		pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		return startPoint;
	}
	//if (nProductPartIndex != -1)
	//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
	//	{
	//		m_DescriptionList.GetNext(pos);
	//		return startPoint;
	//	}

	int nPages = max(rFoldSheet.m_nPagesLeft/2, rFoldSheet.m_nPagesRight/2);
	int	nSheetThickness = nPages * CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex);	
	//if (nSheetThickness == 0)	// if only one page is on the sheet
	//	nSheetThickness = CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex) / 2;

	int d = 2*CBookBlockView::GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nSheetThickness; 
	startPoint+= CSize(-d,-d);

	int	   r  = GetFoldSheetRadiusRightPart(pos, rFoldSheet.m_nProductPartIndex);
	CPoint cp = startPoint + CSize(r, r);

	r-= CBookBlockView::GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nSheetThickness/2;

	RenderFoldSheet(pDC, rItem, nSheetThickness, cp, r, nProductPartIndex);

	BOOL bJustClosed = FALSE;
	POSITION pos2nd = pos;
	GetPrev_DescriptionList(-nProductPartIndex - 1, pos2nd);
	if (pos2nd)
	{
		CDescriptionItem& r2ndItem = m_DescriptionList.GetAt(pos2nd);
		if ( (r2ndItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) && 
			 (r2ndItem.m_nFoldSheetIndex == rItem.m_nFoldSheetIndex) )		
			bJustClosed = TRUE;
	}
	BOOL bAddText = (rFoldSheet.GetNumPages() > 2) ? TRUE : FALSE;

	CFont font, boldFont;
	int nFontSize = (pDC->IsPrinting()) ? 200 : 20;	// 2.5 mm or 11 screen pixels
	if (pDC->IsPrinting())
		nFontSize = (int)((float)nFontSize * ((float)_sizePreviewPage.cy / (_sizeRealPage.cy)) + 0.5f);		// adopt to preview page
	CSize sizeFont(nFontSize, nFontSize);
	//pDC->DPtoLP(&sizeFont);
	font.CreateFont	   (sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont(sizeFont.cy, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = (CFont*)pDC->SelectObject(&boldFont);

	BOOL bDontDrawText = FALSE;
	CSize szSize = sizeFont;
	pDC->LPtoDP(&szSize);
	if (szSize.cy < 1)
		bDontDrawText = TRUE;

	CString  strText;
	int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor	= pDC->SetBkColor(WHITE);
	COLORREF crOldTextColor = pDC->SetTextColor(RGB(200, 100, 30));
	strText.Format(_T(" %s "), rFoldSheet.GetNumber()); 

	//int nXStart = 5*nThresholdMax; 
	//int nXPos = nXStart;
	CSize szDistance(20,20);
	pDC->DPtoLP(&szDistance);
	int nXPos = cp.x + szDistance.cx;
	int nYPos = (bJustClosed) ? (cp.y + sizeFont.cy/2) : (cp.y - r + sizeFont.cy/2);
	if (bAddText)
		if ( ! bDontDrawText)
			pDC->TextOut(nXPos, nYPos, strText);

	pDC->SelectObject(&font);
	
	nXPos += 4 * sizeFont.cy;//_maxNumberSize.cx + 3*nThresholdMax;
	if (bAddText)
	{
		if (bJustClosed)
		{
			strText.Format(_T("%s"), rFoldSheet.m_strFoldSheetTypeName); 
			pDC->SetBkColor(WHITE);
			pDC->SetTextColor(BLACK);
			if ( ! bDontDrawText)
				pDC->TextOut(nXPos, nYPos, strText);
		}

		nXPos += _maxNameSize.cx + sizeFont.cy;//3*nThresholdMax;

		pDC->SetTextColor(DARKBLUE);
		strText.Format(_T(" %s "), rFoldSheet.GetPageRange((bJustClosed) ? BOTHSIDES : RIGHT));
		if ( ! bDontDrawText)
			pDC->TextOut(nXPos, nYPos, strText);

		CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();
		if ( ! rProductPart.m_strName.IsEmpty() && ! bDontDrawText)
		{
			strText.Format(_T("%s"), rProductPart.m_strName); 
			CSize sizeText = pDC->GetTextExtent(strText);
			int   nXPosText = nXPos + _maxPageRangeSize.cx + 5 * sizeFont.cy;//3*nThresholdMax;
			if (nXPosText + sizeText.cx < _ptRightBottom.x)
			{
				pDC->SetBkColor(RGB(233,240,130));
				if ( ! bDontDrawText)
					pDC->TextOut(nXPosText, nYPos, strText);
			}
		}
	}

	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetTextColor(crOldTextColor);

	pDC->SelectObject(pOldFont);
	font.DeleteObject();
	boldFont.DeleteObject();

//	TRACE("%d> ", Item.m_nFoldSheetIndex); // close foldsheet 

	GetNext_DescriptionList(-nProductPartIndex - 1, pos);

	return CPoint(cp.x - r, cp.y);
}


void CBookblock::DrawConnectionLeftRight(CDC* pDC, CDescriptionItem& Item, CPoint endPointLeft, CPoint endPointRight, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CFoldSheet&	rFoldSheet = Item.GetFoldSheet(this);
	if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
		return;
	//if (nProductPartIndex != -1)
	//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
	//		return;

	if ((rFoldSheet.m_nPagesLeft == 0) || (rFoldSheet.m_nPagesRight == 0))
		return;

	if (endPointLeft == endPointRight)
		return;

	int	nSheetThickness = min((rFoldSheet.m_nPagesLeft/2)*CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex), (rFoldSheet.m_nPagesRight/2)*CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex));	
	//if (nSheetThickness == 0)	// if only one page is on the sheet
	//	nSheetThickness = CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex) / 2;

	int nPages;
	if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		nPages = rFoldSheet.m_nPagesLeft;
	else
		nPages = rFoldSheet.m_nPagesRight;

	BOOL bIsCover	  = (rFoldSheet.GetProductPart().IsCover()) ? TRUE : FALSE;
	int nPenThickness = max(CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex), 1);
	CPen whitePen;
	whitePen.CreatePen(PS_SOLID, nPenThickness, (bIsCover) ? LIGHTGRAY : WHITE);
	CPen* pOldPen = (CPen*)pDC->SelectObject(&whitePen);

	endPointLeft.x+=  nSheetThickness/2;
	endPointRight.x+= nSheetThickness/2;
	for (int i = 0; i < nPages; i++)
	{
		pDC->MoveTo(endPointLeft);
		if (i%2)
			pDC->LineTo(endPointRight);

		endPointLeft.x-=  nSheetThickness/nPages;
		endPointRight.x-= nSheetThickness/nPages;
	}

	pDC->SelectObject(pOldPen);
	whitePen.DeleteObject();
}

void CBookblock::RenderFoldSheet(CDC* pDC, CDescriptionItem& Item, int nSheetThickness, CPoint& cp, int nRadius, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CFoldSheet&	rFoldSheet = Item.GetFoldSheet(this);
	int			nPages;
	if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		nPages = rFoldSheet.m_nPagesLeft;
	else
		nPages = rFoldSheet.m_nPagesRight;

	if (nPages == 0)
		return;

	CBoundProductPart& rProductPart	= rFoldSheet.GetProductPart();

	BOOL  bIsCover		   = (rProductPart.IsCover()) ? TRUE : FALSE;
	int   nSideTrim		   = (int)(rProductPart.m_trimmingParams.m_fSideTrim * 1000.0f + 0.5f);
	BOOL  bShinglingActive = rProductPart.m_foldingParams.m_bShinglingActive;

	if (GetBindingProcess(nProductPartIndex).m_bComingAndGoing)	
		nPages/= 2;

	if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		nRadius+= nSheetThickness/2;
	else
		nRadius-= nSheetThickness/2;

	CRect  arcRect;
	CPoint startPt, endPt;

	int nPenThickness = max(CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex), 1) * (nPages/2);
	CPoint pt(_extend.cx - _nShinglingValueXTotal, 0);

	pt.x = max(pt.x, cp.x);

	CSize szPenDP(3, 3);
	pDC->DPtoLP(&szPenDP);
	szPenDP.cx = max(min(nPenThickness/2, szPenDP.cx), 1);
	szPenDP.cy = max(min(nPenThickness/2, szPenDP.cy), 1);

	CPen whitePen, lightgrayPen, grayPen, yellowPen, lightYellowPen, bluePen, darkBluePen, greenPen;
	whitePen.CreatePen		(PS_SOLID, nPenThickness, (pDC->IsPrinting()) ? RGB(235,235,235) : RGB(220,220,220));//RGB(250,250,255));
	lightgrayPen.CreatePen	(PS_SOLID, nPenThickness, LIGHTGRAY);
	yellowPen.CreatePen		(PS_SOLID, szPenDP.cy,	  RGB(130,90,   3));
	lightYellowPen.CreatePen(PS_SOLID, szPenDP.cy,	  RGB(252,211,129));
	darkBluePen.CreatePen	(PS_SOLID, szPenDP.cy,	  DARKBLUE);
	grayPen.CreatePen		(PS_SOLID, 1,			  MIDDLEGRAY);
	CPen* pOldPen = (CPen*)pDC->SelectObject(&whitePen);

	CArray <short*, short*> PageSequence;
	int						nPageSeqIndex = 0;

	if (GetBindingProcess(nProductPartIndex).m_bComingAndGoing)	
	{
		rFoldSheet.GetPageSequence(PageSequence, CDescriptionItem::FoldSheetLeftPart);
		rFoldSheet.ReducePageSequence(PageSequence);
		if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			PageSequence.RemoveAt(rFoldSheet.m_nPagesLeft/2, rFoldSheet.m_nPagesLeft/2); 
		else
			PageSequence.RemoveAt(0, rFoldSheet.m_nPagesLeft/2); 
	}
	else
	{
		rFoldSheet.GetPageSequence(PageSequence, Item.m_nFoldSheetPart);
		rFoldSheet.ReducePageSequence(PageSequence);
	}

	nPages = 2;
	CString strMinPage, strMaxPage;
	for (int i = 0; i < nPages; i++)
	{
		if (PageSequence.GetSize() == 0) // If the foldsheet isn't correct signed
			break;						 // -> you can get this effect

		if (i%2 || nPages == 1)
		{
			if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
				_nShingling -= (int)((float)rProductPart.m_foldingParams.GetShinglingValueX(rProductPart)*1000.0f + 0.5f);

			if ( (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) || (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight <= 2) )
			{
				pt.y = cp.y + nRadius;

				if (_ptLeftBottom == _ptLeftTop)	// points not yet initialized
				{
					CSize size3D(15, 8);
					pDC->DPtoLP(&size3D);
					int delta	  = (int)((float)nRadius/4.0f);
					_ptLeftBottom  = cp + CSize(-delta, nRadius);//CSize(-delta, delta);
					_ptLeftTop	  = cp + ( (pDC->IsPrinting()) ? CSize(0, nRadius) : CSize(size3D.cx, nRadius + size3D.cy) );
					_ptRightTop	  = pt + ( (pDC->IsPrinting()) ? CSize(0, 0)	   : CSize(-size3D.cx, size3D.cy) );
					if (_ptLeftTop.x < _ptRightTop.x)
					{
						_ptRightBottom = CPoint(pt.x, _ptLeftBottom.y);
						CPoint ptCover[5];
						ptCover[0] = _ptLeftBottom;
						ptCover[1] = _ptLeftTop;
						ptCover[2] = _ptRightTop;
						ptCover[3] = _ptRightBottom;
						ptCover[4] = _ptLeftBottom;//cp + CSize(0, nRadius);
						CBrush fillBrush;
						fillBrush.CreateSolidBrush(RGB(235,235,235));
						CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&fillBrush);
						pDC->BeginPath();
						pDC->Polyline(ptCover, 5);
						//pDC->Arc(arcRect, cp + CSize(0, nRadius), cp + CSize(-nRadius, nRadius));
						pDC->EndPath();
						pDC->FillPath();
						pDC->SelectObject(pOldBrush);
						fillBrush.DeleteObject();
					}
					_bCoverPainted = TRUE;
				}
			}
			else
			{
				if (nPages < 1)	// It's a very special case
					break;		// where the right part doesn't exist  

				pt.y = cp.y - nRadius;
			}

			int	  nCurPageIndex1   = -1;
			int	  nCurPageIndex2   = -1;
			float fShinglingValue1 = 0.0f;
			float fShinglingValue2 = 0.0f;
			int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(rFoldSheet.m_nProductPartIndex);
			POSITION pos = (nPageSeqIndex < PageSequence.GetSize()) ? pDoc->m_PageTemplateList.FindIndex(*PageSequence[nPageSeqIndex] - 1, -nProductIndex - 1) : NULL;
			if (pos)
			{
				CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
				if ( i == 1)
					strMinPage   = rPageTemplate.m_strPageID;
				nCurPageIndex1	 = pDoc->m_PageTemplateList.GetIndex(&rPageTemplate);
				fShinglingValue1 = rPageTemplate.m_fShinglingOffsetX;
			}
			pos = (nPageSeqIndex + 1 < PageSequence.GetSize()) ? pDoc->m_PageTemplateList.FindIndex(*PageSequence[nPageSeqIndex+1] - 1, -nProductIndex - 1) : NULL;
			if (pos)
			{
				CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
				if (i == nPages - 1)
					strMaxPage = rPageTemplate.m_strPageID;
				nCurPageIndex2	 = pDoc->m_PageTemplateList.GetIndex(&rPageTemplate);
				fShinglingValue2 = rPageTemplate.m_fShinglingOffsetX;
			}

			CPoint ptShinglingRight (pt.x + _nShingling, pt.y);
			CPoint ptNetRight		(ptShinglingRight);// - CSize(nShinglingValue, 0));

			pDC->MoveTo(ptShinglingRight);	

			pDC->SelectObject((bIsCover) ? &lightgrayPen : &whitePen);
			pDC->LineTo(CPoint(cp.x, pt.y));

			nPageSeqIndex+= 2;

			int nPenOffset = (int)((float)nPenThickness / 2.0f + 0.5f);
			arcRect.TopLeft()	  = cp + CSize(-nRadius, nRadius);
			arcRect.BottomRight() = cp + CSize(nRadius, -nRadius);
			CRect  rcArc1 = arcRect, rcArc2 = arcRect; 
			CPoint startPt1, startPt2, endPt1, endPt2;
			if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			{
				rcArc1.left -= nPenOffset; rcArc1.top += nPenOffset; rcArc1.bottom -= nPenOffset; 
				rcArc2.left += nPenOffset; rcArc2.top -= nPenOffset; rcArc2.bottom += nPenOffset;
				startPt  = CPoint(cp.x, arcRect.top); endPt  = CPoint(arcRect.left, cp.y);
				startPt1 = CPoint(cp.x, rcArc1.top);  endPt1 = CPoint(rcArc1.left, cp.y);
				startPt2 = CPoint(cp.x, rcArc2.top);  endPt2 = CPoint(rcArc2.left, cp.y);
				_nShingling += (int)((float)rProductPart.m_foldingParams.GetShinglingValueX(rProductPart)*1000.0f + 0.5f);
			}
			else
			{
				rcArc1.left += nPenOffset; rcArc1.top -= nPenOffset; rcArc1.bottom += nPenOffset; 
				rcArc2.left -= nPenOffset; rcArc2.top += nPenOffset; rcArc2.bottom -= nPenOffset;
				startPt = cp + CSize(-nRadius, 0);	endPt   = cp + CSize(0, -nRadius);
				startPt  = CPoint(arcRect.left, cp.y); endPt  = CPoint(cp.x, arcRect.bottom);
				startPt1 = CPoint(rcArc1.left,  cp.y); endPt1 = CPoint(cp.x, rcArc1.bottom);
				startPt2 = CPoint(rcArc2.left,  cp.y); endPt2 = CPoint(cp.x, rcArc2.bottom);
			}
			//if (pDC->RectVisible(arcRect))
				if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)		// if <= 2 we do have no spine
					pDC->Arc(arcRect, startPt, endPt);

			BOOL bHighlight1 = FALSE;//((nPagePreviewIndex1 == nCurPageIndex1) || (nPagePreviewIndex2 == nCurPageIndex1)) ? TRUE : FALSE;
			BOOL bHighlight2 = FALSE;//((nPagePreviewIndex1 == nCurPageIndex2) || (nPagePreviewIndex2 == nCurPageIndex2)) ? TRUE : FALSE;
			CPoint ptNetRight1, ptNetRight2;
			if (bShinglingActive)
			{
				ptNetRight1 = CPoint(ptShinglingRight - CSize((int)(fabs(fShinglingValue1) * 1000.0f + 0.5f), -nPenOffset )); ptNetRight1.x = max(ptNetRight1.x, cp.x);
				ptNetRight2 = CPoint(ptShinglingRight - CSize((int)(fabs(fShinglingValue2) * 1000.0f + 0.5f),  nPenOffset )); ptNetRight2.x = max(ptNetRight2.x, cp.x);
	
				pDC->SelectObject(&grayPen);
				pDC->MoveTo(ptShinglingRight.x, ptNetRight1.y);	
				pDC->LineTo(cp.x, ptNetRight1.y);

				pDC->SelectObject((bHighlight1) ? &yellowPen : &lightYellowPen);
				pDC->MoveTo(CPoint(ptNetRight1));	
				pDC->LineTo(CPoint(cp.x, ptNetRight1.y));
				pDC->SelectObject((bHighlight2) ? &yellowPen : &lightYellowPen);
				pDC->MoveTo(CPoint(ptNetRight2));	
				pDC->LineTo(CPoint(cp.x, ptNetRight2.y));
				if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)		// if <= 2 we do have no spine
				{
					pDC->SelectObject((bHighlight1) ? &yellowPen : &lightYellowPen);
					pDC->Arc(rcArc1, startPt1, endPt1);
					pDC->SelectObject((bHighlight2) ? &yellowPen : &lightYellowPen);
					pDC->Arc(rcArc2, startPt2, endPt2);
				}
				if (bHighlight1 || bHighlight2)
				{
					CString string;
					CPrintSheetView::TruncateNumber(string, abs((bHighlight1) ? fShinglingValue1 : fShinglingValue2));
					CPageTemplate::DrawSingleMeasure(pDC, string, CPoint(ptShinglingRight.x + nPenThickness/2, ptShinglingRight.y), LEFT, RIGHT, TRUE);
				}

				_rcBox.right  = max(_rcBox.right,  ptNetRight1.x);
				_rcBox.right  = max(_rcBox.right,  ptNetRight2.x);
				_rcBox.bottom = min(_rcBox.bottom, ptNetRight1.y);
				_rcBox.bottom = min(_rcBox.bottom, ptNetRight2.y);
			}
			else
			{
				ptNetRight1 = CPoint(ptShinglingRight - CSize(0, -nPenOffset));
				ptNetRight2 = CPoint(ptShinglingRight - CSize(0,  nPenOffset));

				pDC->SelectObject(&grayPen);
				pDC->MoveTo(ptShinglingRight.x, ptNetRight1.y);	
				pDC->LineTo(cp.x, ptNetRight1.y);

				pDC->SelectObject((bHighlight1) ? &darkBluePen : &grayPen);
				if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)		// if <= 2 we do have no spine
					pDC->Arc(rcArc1, startPt1, endPt1);
				pDC->MoveTo(CPoint(ptNetRight1));	
				pDC->LineTo(CPoint(cp.x, ptNetRight1.y));
				pDC->SelectObject((bHighlight2) ? &darkBluePen : &grayPen);
				if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)		// if <= 2 we do have no spine
					pDC->Arc(rcArc2, startPt2, endPt2);
				pDC->MoveTo(CPoint(ptNetRight2));	
				pDC->LineTo(CPoint(cp.x, ptNetRight2.y));

				_rcBox.right  = max(_rcBox.right, ptNetRight1.x);
				_rcBox.right  = max(_rcBox.right, ptNetRight2.x);
				_rcBox.bottom = min(_rcBox.bottom, ptNetRight1.y);
				_rcBox.bottom = min(_rcBox.bottom, ptNetRight2.y);
			}
			pDC->SelectObject((bIsCover) ? &lightgrayPen : &whitePen);
		}

		if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			nRadius-= nSheetThickness/nPages;
		else
			nRadius+= nSheetThickness/nPages;
	}

	pDC->SelectObject(pOldPen);

	whitePen.DeleteObject();
	lightgrayPen.DeleteObject();
	yellowPen.DeleteObject();
	lightYellowPen.DeleteObject();
	darkBluePen.DeleteObject();
	grayPen.DeleteObject();
}

int CBookblock::GetFoldSheetRadiusLeftPart(POSITION pos, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	int				 r = 0;
	CDescriptionItem Item;
	int				 nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex);
	
	while (pos)
	{
		Item = GetNext_DescriptionList(nProductIndex, pos);

		if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
			break;
		else
		{
			CFoldSheet& rFoldSheet = Item.GetFoldSheet(this);
			int nPages = rFoldSheet.m_nPagesLeft/2;
			if (nPages == 0)
				;
			else
				r+= 2*CBookBlockView::GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nPages * CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex); 
		}	
	}

	return r;
}

int CBookblock::GetFoldSheetRadiusRightPart(POSITION pos, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	int				 r = 0;
	CDescriptionItem Item;
	int				 nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex);
	
	Item = m_DescriptionList.GetAt(pos);
	while (pos)
	{
		if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			break;
		else
		{
			CFoldSheet& rFoldSheet = Item.GetFoldSheet(this);
			int nPages = rFoldSheet.m_nPagesRight/2;
			if (nPages == 0)
				;
			else
				r+= 2*CBookBlockView::GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nPages * CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex); 
		}

		Item = GetPrev_DescriptionList(nProductIndex, pos);
	}

	return r;
}

CPoint g_ptFoldSheetProductGroup(0,0);

CRect CBookblock::DrawFoldSheetList(CDC* pDC, CRect rcFrame, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	  nTextHeight = Pixel2MM(pDC, 40);
	CRect rcRow = rcFrame; rcRow.bottom = rcRow.top + nTextHeight;

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	InitSortedFoldSheets(sortedFoldSheets);

	CPen grayPen(PS_SOLID, 1, TABLECOLOR_SEPARATION_LINE);
	CPen*  pOldPen	  = pDC->SelectObject(&grayPen);

	BOOL			   bShowProductPart  = FALSE;
	CBoundProductPart* pProductPart  	 = NULL;
	for (int i = 0; i < sortedFoldSheets.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = sortedFoldSheets[i];
		if ( ! pFoldSheet)
			continue;

		if (&pFoldSheet->GetProductPart() != pProductPart)
		{
			pProductPart = &pFoldSheet->GetProductPart();
			bShowProductPart = TRUE;
		}

		CRect bkRect(0, rcRow.top, rcFrame.right, rcRow.bottom);
		pDC->FillSolidRect(bkRect, (i%2) ? TABLECOLOR_BACKGROUND : TABLECOLOR_BACKGROUND_DARK);
		pDC->MoveTo(bkRect.left,  bkRect.top);
		pDC->LineTo(bkRect.right, bkRect.top);

		CRect rcFoldSheetRow = DrawFoldSheetRow(pDC, rcRow, *pFoldSheet, i, bShowProductPart, rColumnWidths, pDisplayList);

		rcRow.top	 = rcFoldSheetRow.bottom;
		rcRow.bottom = rcRow.top + nTextHeight;
		rcBox.UnionRect(rcBox, rcRow);
		rcBox.UnionRect(rcBox, rcFoldSheetRow);

		bShowProductPart = FALSE;
	}

	pDC->SelectObject(pOldPen);
	grayPen.DeleteObject();

	return rcBox;
}

CRect CBookblock::DrawFoldSheetRow(CDC* pDC, CRect rcFrame, CFoldSheet& rFoldSheet, int nFoldSheetIndex, BOOL bShowProductPart, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	COLORREF crValueColor = RGB(50,50,50);

	CPrintSheetPlanningView* pView				  = NULL;
	BOOL					 bFoldSheetNumeration = FALSE; 
	if (pDisplayList)
		if (pDisplayList->m_pParent)
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
			{
				pView					= (CPrintSheetPlanningView*)pDisplayList->m_pParent;
				bFoldSheetNumeration	= (pView->IsOpenFoldSheetNumeration()) ? TRUE : FALSE;
			}

	int nProductGroupWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 0) ? rColumnWidths[0] :  80);
	int nFoldSheetNumWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 1) ? rColumnWidths[1] :  80);
	int nFoldSchemeWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 2) ? rColumnWidths[2] : 200);
	int nQuantityPlannedWidth	= Pixel2MM(pDC, (rColumnWidths.GetSize() > 3) ? rColumnWidths[3] :  60);
	int nQuantityExtraWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 4) ? rColumnWidths[4] :  60);
	int nQuantityActualWidth	= Pixel2MM(pDC, (rColumnWidths.GetSize() > 5) ? rColumnWidths[5] :  60);
	int nPaperTypeWidth			= Pixel2MM(pDC, (rColumnWidths.GetSize() > 6) ? rColumnWidths[6] : 180);
	int nBorder					= Pixel2MM(pDC, 5);
	int nTextHeight				= Pixel2MM(pDC, 15);

	CFont	smallFont, boldFont;
	smallFont.CreateFont(Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont (Pixel2MM(pDC, 12), 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(crValueColor);

	CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();

	CString strOut;
	CRect rcText = rcFrame; rcText.top += nBorder; rcText.right = rcText.left + nProductGroupWidth - nBorder;
	if (bShowProductPart)
	{
		pDC->SelectObject(&boldFont);
		strOut = rProductPart.m_strName;
		CGraphicComponent gp;
		CRect rcTitle = rcText; rcTitle.bottom = rcTitle.top + nTextHeight; rcTitle.right -= nBorder;
		gp.DrawTitleBar(pDC, rcTitle, _T(""), RGB(242, 246, 184), RGB(222, 233, 77), 90, FALSE);
		rcTitle.left += nBorder;
		pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);
		g_ptFoldSheetProductGroup.x = rcTitle.right - nBorder;
		g_ptFoldSheetProductGroup.y = rcTitle.bottom;
	}
	else
	{
		CPen pen(PS_SOLID, Pixel2MM(pDC, 1), RGB(180, 190, 50));
		CPen* pOldPen = pDC->SelectObject(&pen);
		pDC->MoveTo(g_ptFoldSheetProductGroup.x, g_ptFoldSheetProductGroup.y);
		pDC->LineTo(g_ptFoldSheetProductGroup.x, rcText.top + nTextHeight/2);
		pDC->LineTo(rcText.right, rcText.top + nTextHeight/2);
		pDC->SelectObject(pOldPen);
		pen.DeleteObject();
	}

	pDC->SetTextColor(RGB(255, 150, 50));
	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nFoldSheetNumWidth - nBorder;
	strOut = rFoldSheet.GetNumber();
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);

	pDC->SelectObject(&smallFont);

	if (bFoldSheetNumeration)
	{
		strOut.Format(_T("(%d)"), nFoldSheetIndex + 1);
		CRect rcText2 = rcText; rcText2.left = rcText2.right - pDC->GetTextExtent(strOut).cx - 2 * nBorder;	
		pDC->SetTextColor(RED);
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	}
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.OffsetRect(-nBorder, 0); rcDI.bottom = rcDI.top + nTextHeight + nBorder;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)&rFoldSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, FoldSheetNumber), (void*)nFoldSheetIndex, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (bFoldSheetNumeration)
				pDI->m_nState = (pView->IsInsideNumerationRange(nFoldSheetIndex)) ? CDisplayItem::Selected : CDisplayItem::Normal;
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	CRect rcThumbnail(CPoint(rcText.right + nBorder/2, rcText.top + nBorder/2), CSize(Pixel2MM(pDC, rcFrame.Height()/2), rcFrame.Height() - 2 * nBorder));
	CRect rcFoldSheet = rFoldSheet.DrawThumbnail(pDC, rcThumbnail, FALSE, FALSE, 0);

	pDC->SetTextColor(RGB(255, 150, 50));

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nFoldSchemeWidth - nBorder;
	rcText.left += Pixel2MM(pDC, 35); 
	strOut = rFoldSheet.m_strFoldSheetTypeName;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	pDC->SetTextColor(GUICOLOR_CAPTION);

	CRect rcText2 = rcText;
	rcText2.OffsetRect(0, nTextHeight);
	CString strOut2; strOut2.LoadString(IDS_PAGES_HEADER);
	strOut.Format(_T("%-2d %s | %s"), rFoldSheet.GetNumPages(), strOut2, rFoldSheet.GetPageRange());
	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.right  = rcText2.left + pDC->GetTextExtent(strOut).cx;
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	pDC->SetTextColor(crValueColor);

	int   nQuantityPlanned = rFoldSheet.GetQuantityPlanned();
	int   nQuantityExtra   = rFoldSheet.GetQuantityExtra();
	int   nQuantityActual  = rFoldSheet.GetQuantityActual();
	float fPercentDiff     = ( (nQuantityPlanned + nQuantityExtra) > 0) ? ( ((float)nQuantityActual/float(nQuantityPlanned + nQuantityExtra) - 1.0f) * 100.0f) : 0.0f;
	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nQuantityPlannedWidth - nBorder;
	strOut.Format(_T("%d"), nQuantityPlanned);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)&rFoldSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, FoldSheetQuantityPlanned), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nQuantityExtraWidth - nBorder;
	strOut.Format(_T("%d"), nQuantityExtra);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)&rFoldSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, FoldSheetQuantityExtra), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nQuantityActualWidth - nBorder;
	if (abs(fPercentDiff) > 0.01f)
		if (fPercentDiff > 0.0f)
			strOut.Format(_T("%d (+%.1f%%)"), nQuantityActual, fPercentDiff);
		else
			strOut.Format(_T("%d (%.1f%%)"),  nQuantityActual, fPercentDiff);
	else
		strOut.Format(_T("%d"), nQuantityActual);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nPaperTypeWidth - nBorder;
	strOut = rFoldSheet.GetPaperName();
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top += 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 5;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)&rFoldSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, FoldSheetPaperName), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	smallFont.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}
 
CSize CBookblock::GetExtend(int nProductPartIndex)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CSize(0, 0);

	CheckFoldSheetsPlausibility();

	_nMaxPaperThickness = 5;

	int		 nProductIndex = (nProductPartIndex < 0) ? abs(nProductPartIndex) - 1 : pDoc->GetBoundProductPart(nProductPartIndex).GetBoundProduct().GetIndex();
	long	 lHeight = 0;
	POSITION pos	 = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem		 = pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		CFoldSheet&		  rFoldSheet = rItem.GetFoldSheet(&(pDoc->m_Bookblock));
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
		//		continue;

		int nPaperThickness = CBookBlockView::GetPaperThickness(rFoldSheet.m_nProductPartIndex);
		_nMaxPaperThickness = max(_nMaxPaperThickness, nPaperThickness);

		if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			lHeight+= 2*CBookBlockView::GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + (rFoldSheet.m_nPagesLeft/2)*nPaperThickness;	
		else
			if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)
				lHeight+= 2*CBookBlockView::GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + (rFoldSheet.m_nPagesRight/2)*nPaperThickness;	
			else
				lHeight+= 3*CBookBlockView::GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nPaperThickness;	
	}

	float fWidth = 0;
	for (int i = 0; i < pDoc->GetBoundProduct(nProductIndex).m_parts.GetSize(); i++)
	{
		CBoundProductPart& rProductPart = pDoc->GetBoundProduct(nProductIndex).m_parts[i];
		
		fWidth = max(fWidth, rProductPart.m_fPageWidth);

		int nThisProductPartIndex = rProductPart.GetIndex();
		pDoc->m_Bookblock.SetShinglingValuesX(rProductPart.m_foldingParams.GetShinglingValueX(rProductPart), FLT_MAX,		nThisProductPartIndex);
		pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, rProductPart.m_foldingParams.GetShinglingValueXFoot(rProductPart),	nThisProductPartIndex);
		pDoc->m_Bookblock.SetShinglingValuesY(rProductPart.m_foldingParams.GetShinglingValueY(rProductPart),				nThisProductPartIndex);
	}

	CSize size((int)(fWidth * 1000.0f + 0.5f), lHeight);

	float fShinglingValueXTotal, fShinglingValueXFootTotal, fShinglingValueYTotal;
	pDoc->m_PageTemplateList.CalcShinglingOffsets(fShinglingValueXTotal, fShinglingValueXFootTotal, fShinglingValueYTotal, nProductPartIndex);
	_nShinglingValueXTotal = (int)((float)fShinglingValueXTotal * 1000.0f + 0.5f);//, (int)((float)fShinglingValueFootTotal * 1000.0f + 0.5f));

	size.cx += _nShinglingValueXTotal;
	size.cx = (int)((float)size.cy / (float)_viewPortSize.cy * (float)_viewPortSize.cx + 0.5f);

	if ((size.cx == 0) || (size.cy == 0))
		return CSize(_nViewBorder, _nViewBorder);
	else
		return size;
}

CFoldSheet* CBookblock::FindFoldSheet(int nProductPartIndex, CString strName)
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if ( (rFoldSheet.m_strFoldSheetTypeName == strName) && (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex)) )
			return &rFoldSheet;
	}
	return NULL;
}

int	CBookblock::GetNumFoldSheets(int nProductPartIndex, CString strName)
{
	int nNum = 0;
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
		{
			if ( ! strName.IsEmpty())
			{
				if (rFoldSheet.m_strFoldSheetTypeName == strName)
					nNum++;
			}
			else
				nNum++;
		}
	}
	return nNum;
}

void CBookblock::DoNumber(BOOL bRangeAll, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nCurNum = (nStartNum - 1) * CDlgNumeration::GetSeqLen(strStyle) + 1;
	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, nProductPartIndex);
	for (int i = 0; i < sortedFoldSheets.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = sortedFoldSheets[i];
		if ( bRangeAll || ((i + 1 >= nRangeFrom) && (i + 1 <= nRangeTo)) )
		{
			pFoldSheet->m_strNumerationStyle = strStyle;
			pFoldSheet->m_nNumerationOffset  = nCurNum - pFoldSheet->m_nFoldSheetNumber;
			nCurNum += (bReverse) ? -1 : 1;
		}
	}
}

int	CBookblock::GetDescriptionItemIndex(CFoldSheet* pFoldSheet, int nFoldSheetPart)
{
	int nFoldSheetIndex = GetFoldSheetIndex(pFoldSheet);

	int		 nIndex = 0;
	POSITION pos	= m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem	= m_DescriptionList.GetNext(pos);
		if (nFoldSheetIndex == rItem.m_nFoldSheetIndex)
			if (nFoldSheetPart == rItem.m_nFoldSheetPart)
				return nIndex;
		nIndex++;
	}
	return -1;
}

CFoldSheet* CBookblock::GetFoldSheetByID(CString& strID)
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if (rFoldSheet.GetID() == strID)
			return &rFoldSheet;
	}
	return NULL;
}

CFoldSheet* CBookblock::GetFoldSheetByIndex(int nFoldSheetIndex)
{
	POSITION pos = m_FoldSheetList.FindIndex(nFoldSheetIndex);
	return (pos) ? &m_FoldSheetList.GetAt(pos) : NULL;
}

void CBookblock::GetFoldSheetGroups(CArray<CFoldSheet*, CFoldSheet*>& foldSheetGroups, int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	foldSheetGroups.RemoveAll();

	CArray<CLayout*, CLayout*> layouts;
	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos); 
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
		//		continue;

		BOOL bFound = FALSE;
		int  i = 0;
		for (; i < foldSheetGroups.GetSize(); i++)
		{
			if (foldSheetGroups[i]->m_strFoldSheetTypeName == rFoldSheet.m_strFoldSheetTypeName)	// already in list
			{
				bFound = TRUE;
				break;
			}
		}
		CLayout* pLayout = rFoldSheet.GetLayout(0);
		if (bFound)
		{
			bFound = FALSE;
			if (layouts[i] == pLayout)	// already in list
				bFound = TRUE;
		}

		if ( ! bFound)
		{
			foldSheetGroups.Add(&rFoldSheet);
			layouts.Add(pLayout);
		}
	}

	qsort((void*)foldSheetGroups.GetData(), foldSheetGroups.GetSize(), sizeof(CFoldSheet*), CompareFoldSheetNum);
}

CString	CBookblock::GetFoldSheetRange(CFoldSheet* pFoldSheet, CLayout* pLayout, int* pNumFoldSheets)
{
	CArray <CFoldSheet*, CFoldSheet*> foldSheets;

	int nLayerIndex = 0;
	InitSortedFoldSheets(foldSheets, pFoldSheet->m_nProductPartIndex, pFoldSheet, pFoldSheet->GetLayout(nLayerIndex));

	if (pNumFoldSheets)
		*pNumFoldSheets = foldSheets.GetSize();

	if ( ! foldSheets.GetSize())
		return _T("");

	CString strSheetRange;
	CString strSheetRanges;
	int		nStartIndex = 0;
	int		i = 1;
	for (; i < foldSheets.GetSize(); i++)
	{
		if (foldSheets[i - 1]->GetNumberOnly() != foldSheets[i]->GetNumberOnly() - 1)
		{
			if (foldSheets[nStartIndex]->GetNumber() == foldSheets[i - 1]->GetNumber())
				strSheetRange.Format(_T("%s%s"), (nStartIndex == 0) ? _T("") : _T(" + "), foldSheets[nStartIndex]->GetNumber() );
			else
				strSheetRange.Format(_T("%s%s - %s"), (nStartIndex == 0) ? _T("") : _T(" + "), foldSheets[nStartIndex]->GetNumber(), foldSheets[i - 1]->GetNumber() );

			strSheetRanges += strSheetRange;

			nStartIndex = i;
		}
	}

	if (foldSheets[nStartIndex]->GetNumber() == foldSheets[i - 1]->GetNumber())
		strSheetRange.Format(_T("%s%s"), (nStartIndex == 0) ? _T("") : _T(" + "), foldSheets[nStartIndex]->GetNumber() );
	else
		strSheetRange.Format(_T("%s%s - %s"), (nStartIndex == 0) ? _T("") : _T(" + "), foldSheets[nStartIndex]->GetNumber(), foldSheets[i - 1]->GetNumber() );
	strSheetRanges += strSheetRange;

	return strSheetRanges;
}

BOOL CBookblock::FoldSheetExist(CFoldSheet* pFoldSheet)
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if (&rFoldSheet == pFoldSheet)
			return TRUE;
	}

	return FALSE;
}

CFoldSheet* CBookblock::GetFirstFoldSheet(int nProductPartIndex)
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			return &rFoldSheet;
	}
	return NULL;
}

CFoldSheet* CBookblock::GetNextFoldSheet(CFoldSheet* pFoldSheet, int nProductPartIndex, CString strFoldScheme)
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if (&rFoldSheet == pFoldSheet)
			break;
	}

	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if ( ! strFoldScheme.IsEmpty())
			if (strFoldScheme != rFoldSheet.m_strFoldSheetTypeName)
				continue;
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			return &rFoldSheet;
	}
	return NULL;
}

CFoldSheet* CBookblock::GetNextFoldSheetLayer(CFoldSheet* pFoldSheet, int& nLayerIndex, int nProductPartIndex, CString strFoldScheme)
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if (&rFoldSheet == pFoldSheet)
		{
			if (nLayerIndex < rFoldSheet.m_LayerList.GetSize() - 1)
			{
				nLayerIndex++;
				return &rFoldSheet;
			}
			else
			{
				nLayerIndex = 0;
				break;
			}
		}
	}

	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if ( ! strFoldScheme.IsEmpty())
			if (strFoldScheme != rFoldSheet.m_strFoldSheetTypeName)
				continue;
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			return &rFoldSheet;
	}
	return NULL;
}

CFoldSheet* CBookblock::GetLastFoldSheet(int nProductPartIndex)
{
	POSITION pos = m_FoldSheetList.GetTailPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetPrev(pos); 
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			return &rFoldSheet;
	}
	return NULL;
}

CFoldSheet* CBookblock::GetFirstUnplacedFoldSheet(CFoldSheet* pFoldSheet, int nStartAt, BOOL bAddSchemeIfRequired)
{
	if ( ! pFoldSheet)
		return NULL;

	int		 nLayerIndex = 0;
	POSITION pos		 = (nStartAt == -1) ? m_FoldSheetList.GetHeadPosition() : m_FoldSheetList.FindIndex(nStartAt);
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.m_strFoldSheetTypeName != pFoldSheet->m_strFoldSheetTypeName)
			continue;
		if ( ! rFoldSheet.IsPartOfBoundProduct(pFoldSheet->m_nProductPartIndex))
			continue;
		if ( ! rFoldSheet.GetPrintSheet(nLayerIndex))
			return &rFoldSheet;
	}

	if (bAddSchemeIfRequired)
	{
		CBoundProductPart&	rProductPart =  pFoldSheet->GetProductPart();
		CFoldSheet			newFoldSheet = *pFoldSheet;
		int					nNumPages	 = rProductPart.GetNumUnfoldedPages();
		CArray <int, int>   newFoldSheetIndices;
		ImposePageSection(&rProductPart, newFoldSheet, nNumPages, 1, &newFoldSheetIndices);
		if (newFoldSheetIndices.GetSize() > 0)
			return GetFoldSheetByIndex(newFoldSheetIndices[0]);
	}

	return NULL;
}

CFoldSheet* CBookblock::GetFirstUnplacedFoldSheetLayer(CFoldSheet* pFoldSheet, int& nLayerIndex, BOOL bAddSchemeIfRequired)
{
	if ( ! pFoldSheet)
		return NULL;

	POSITION pos = m_FoldSheetList.FindIndex(pFoldSheet->GetIndex());
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.m_strFoldSheetTypeName != pFoldSheet->m_strFoldSheetTypeName)
			continue;
		if ( ! rFoldSheet.IsPartOfBoundProduct(pFoldSheet->m_nProductPartIndex))
			continue;
		for (int i = 0; i < rFoldSheet.m_LayerList.GetSize(); i++)
		{
			if ( ! rFoldSheet.GetPrintSheet(i))
			{
				nLayerIndex = i;
				return &rFoldSheet;
			}
		}
	}

	nLayerIndex = 0;

	if (bAddSchemeIfRequired)
	{
		CBoundProductPart&	rProductPart =  pFoldSheet->GetProductPart();
		CFoldSheet			newFoldSheet = *pFoldSheet;
		int					nNumPages	 = rProductPart.GetNumUnfoldedPages();
		CArray <int, int>   newFoldSheetIndices;
		ImposePageSection(&rProductPart, newFoldSheet, nNumPages, 1, &newFoldSheetIndices);
		if (newFoldSheetIndices.GetSize() > 0)
			return GetFoldSheetByIndex(newFoldSheetIndices[0]);
	}

	return NULL;
}

CFoldSheet* CBookblock::GetFirstUnassignedFoldSheet()
{
	int		 nLayerIndex = 0;
	POSITION pos		 = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if ( ! rFoldSheet.GetPrintSheet(nLayerIndex))
			return &rFoldSheet;
	}
	return NULL;
}

CFoldSheet* CBookblock::GetNextUnassignedFoldSheet(CFoldSheet* pFoldSheet)
{
	if ( ! pFoldSheet)
		return NULL;

	int		 nLayerIndex = 0;
	POSITION pos		 = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if (&rFoldSheet == pFoldSheet)
			break;
	}
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if ( ! rFoldSheet.GetPrintSheet(nLayerIndex))
			return &rFoldSheet;
	}
	return NULL;
}

int	CBookblock::GetNumUnplacedFoldSheets(CFoldSheet* pStartFoldSheet, int nProductPartIndex, CString strSchemeName, BOOL bIncludeUnfoldedPages)
{
	int		 nIndex		 = (pStartFoldSheet) ? GetFoldSheetIndex(pStartFoldSheet) : 0;
	int		 nNumSheets	 = 0;
	POSITION pos		 = m_FoldSheetList.FindIndex(nIndex);
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if ( ! strSchemeName.IsEmpty())
			if (rFoldSheet.m_strFoldSheetTypeName != strSchemeName)
				continue;
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		for (int i = 0; i < rFoldSheet.m_LayerList.GetSize(); i++)
		{
			if ( ! rFoldSheet.GetPrintSheet(i))
				nNumSheets++;
		}
	}
	if (bIncludeUnfoldedPages && pStartFoldSheet)
	{
		CImpManDoc*	pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			int nNumUnfoldedPages = pDoc->GetBoundProductPart(nProductPartIndex).GetNumUnfoldedPages();
			nNumSheets += (nNumUnfoldedPages / pStartFoldSheet->GetNumPages()) * pStartFoldSheet->m_LayerList.GetSize();
		}
	}

	return nNumSheets;
}

CBindingProcess& CBookblock::GetBindingProcess(int nProductPartIndex)
{
	static CBindingProcess nullBindingProcess;

	if (nProductPartIndex == ALL_PARTS)
		return nullBindingProcess;
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullBindingProcess;

	if (nProductPartIndex >= 0)
		return pDoc->GetBoundProductPart(nProductPartIndex).GetBoundProduct().m_bindingParams;
	else
		return pDoc->GetBoundProduct(abs(nProductPartIndex) - 1).m_bindingParams;

	return nullBindingProcess;
}

void CBookblock::ArrangeAssemblySections() 
{
	POSITION pos1 = m_DescriptionList.GetHeadPosition();
	while (pos1)
	{
		CDescriptionItem& item1 = m_DescriptionList.GetNext(pos1);
		if (item1.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		{
			POSITION pos2 = pos1;
			while (pos2)
			{
				BOOL bChange = FALSE;
				CDescriptionItem& item2 = m_DescriptionList.GetNext(pos2);
				if (item2.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
					if (item1.m_nPart > item2.m_nPart)
						bChange = TRUE;
					else
						if (item1.m_nPart == item2.m_nPart)
							if (item1.m_nSection > item2.m_nSection)
								bChange = TRUE;

				if (bChange)
				{
					CDescriptionItem* pItem1Right = FindDescriptionItem(item1.m_nFoldSheetIndex, CDescriptionItem::FoldSheetRightPart);
					CDescriptionItem* pItem2Right = FindDescriptionItem(item2.m_nFoldSheetIndex, CDescriptionItem::FoldSheetRightPart);
					CDescriptionItem tmpItem = item1; item1 = item2; item2 = tmpItem;
					if (pItem1Right && pItem2Right)
					{
						tmpItem = *pItem1Right; *pItem1Right = *pItem2Right; *pItem2Right = tmpItem;
					}
				}
			}
		}
	}
}

CDescriptionItem* CBookblock::FindDescriptionItem(int nFoldSheetIndex, int nFoldSheetPart)
{
	POSITION pos = m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& item = m_DescriptionList.GetNext(pos);
		if (item.m_nFoldSheetPart == nFoldSheetPart)
			if (item.m_nFoldSheetIndex == nFoldSheetIndex)
				return &item;
	}
	return NULL;
}

void CBookblock::CheckFoldSheetsPlausibility()
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos); 
		if (rFoldSheet.GetNumPages() == 2)	// just one page - make sure is defined as 2 + 0
		{
			rFoldSheet.m_nPagesLeft  = 2;
			rFoldSheet.m_nPagesRight = 0;
			CFoldSheetLayer& rLayer = rFoldSheet.m_LayerList.GetHead();
			for (int i = 0; i < rLayer.m_FrontSpineArray.GetSize(); i++)
				rLayer.m_FrontSpineArray[i] = LEFT;
			for (int i = 0; i < rLayer.m_BackSpineArray.GetSize(); i++)
				rLayer.m_BackSpineArray[i] = LEFT;
		}
	}
}

int	CBookblock::GetNumPages(int nProductPartIndex, BOOL bEnabledOnly)
{
	int nNum = 0;
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
		//		continue;
		if (bEnabledOnly)
			if (rFoldSheet.m_bOutputDisabled)
				continue;

		nNum += rFoldSheet.GetNumPages();
	}
	return nNum;
}

CDlgBookblockProofProgress g_dlgBookblockProofProgress;

BOOL CBookblock::OutputProofProgessCallback(int nTotalPages, int nCurrentPage, const CString& strFoldSheetNumber, LPVOID lpData)
{
	if (g_dlgBookblockProofProgress.m_strProgressText != strFoldSheetNumber)
	{
		g_dlgBookblockProofProgress.m_strProgressText = strFoldSheetNumber;
		g_dlgBookblockProofProgress.UpdateData(FALSE);
	}
	g_dlgBookblockProofProgress.m_progressBar.SetRange((short)1, (short)nTotalPages);
	g_dlgBookblockProofProgress.m_progressBar.SetPos(nCurrentPage);

	MSG msg;
	if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		if (msg.message == WM_KEYDOWN && msg.wParam == VK_ESCAPE)
			if (KIMOpenLogMessage(IDS_PDFLIB_REALLY_CANCEL, MB_YESNO, IDNO) == IDYES)
				return TRUE;
	}

	return FALSE;
}

BOOL CBookblock::OutputProof(const CString strDocTitle)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	BOOL		 bAlreadyWarned = FALSE;
	BOOL		 bBreak		    = FALSE;
	BOOL		 bOverwriteAll  = -1;		// unknown yet
	if ( (m_outputTargetProps.m_nType == CPDFTargetProperties::TypeJDFFolder) && ! bAlreadyWarned )
	{
		if ( ! (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp) || (pDoc->m_nDongleStatus == CImpManApp::NoDongle) || (pDoc->m_nDongleStatus == CImpManApp::ClientDongle))
		{
			if (KIMOpenLogMessage(IDS_DEMO_JDFOUTPUT_WARNING, MB_OKCANCEL | MB_ICONWARNING, IDOK) == IDCANCEL)
				return 0;
			bAlreadyWarned = TRUE;
		}
	}

	g_dlgBookblockProofProgress.Create(IDD_BOOKBLOCK_PROOF_PROGRESS);
 
	if (m_outputTargetProps.m_nType == CPDFTargetProperties::TypeJDFFolder)
	{
		CJDFOutputImposition jdfOutput(m_outputTargetProps);
		bBreak = jdfOutput.ProcessBookblockJobInOne(this, bOverwriteAll, FALSE, OutputProofProgessCallback);
	}
	else
	{
		CPDFOutput pdfOutput(m_outputTargetProps, strDocTitle);
		bBreak = pdfOutput.ProcessBookblockJobInOne(this, bOverwriteAll, FALSE, TRUE, FALSE, OutputProofProgessCallback);
	}

	g_dlgBookblockProofProgress.DestroyWindow();

	return 0;
}

BOOL CBookblock::OutputProofAuto(const CString strDocTitle)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	if (m_outputTargetProps.m_strLocation.IsEmpty() || m_outputTargetProps.m_strOutputFilename.IsEmpty())
		return 0;

	EnableProofOutput();

	BOOL bOverwriteAll = TRUE;
	CPDFOutput pdfOutput(m_outputTargetProps, strDocTitle);
	return pdfOutput.ProcessBookblockJobInOne(this, bOverwriteAll, FALSE, TRUE, FALSE, NULL);
}

void CBookblock::EnableProofOutput()
{
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		rFoldSheet.m_bOutputDisabled = FALSE;
	}
}

int	CBookblock::GetOutfileRelation(const CString& strOutputFilename)
{
	if (strOutputFilename.Find("$1") >= 0)				// $1 = foldsheet number
		return FoldSheetPerFile;
	else
		return JobPerFile;
}

CString CBookblock::GetOutFilename(int nSide, CFoldSheet* pFoldSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return "";
	if (m_outputTargetProps.m_strTargetName.IsEmpty())
		return _T("");

	int nOutFileRelation = CPrintSheet::GetOutfileRelation(m_outputTargetProps.m_strOutputFilename);
	switch (nSide)
	{
	case BOTHSIDES:	if ( (nOutFileRelation != CPrintSheet::SheetPerFile) && (nOutFileRelation != CPrintSheet::JobPerFile) )
						return "";
					break;
	case FRONTSIDE: 
	case BACKSIDE:	if (nOutFileRelation != CPrintSheet::SidePerFile)
						return "";
					break;
	default:		if (nOutFileRelation != CPrintSheet::PlatePerFile)
						return "";
					break;
	}

	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)m_outputTargetProps.m_strOutputFilename, NULL, nSide, NULL, -1, "", -1, -1, pFoldSheet);	
	
	return szDest;
}

CString	CBookblock::GetOutFileFullPath(int nSide, CFoldSheet* pFoldSheet, BOOL bJDFMarksOutput, BOOL bCreateMissingPath)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return "";
	if (m_outputTargetProps.m_strTargetName.IsEmpty())
		return _T("");
	
	CString strLocation = m_outputTargetProps.m_strLocation;
	if (bJDFMarksOutput)
	{
		if (m_outputTargetProps.m_bJDFMarksInFolder && ! m_outputTargetProps.m_strJDFMarksFolder.IsEmpty()) 
		{
			if (PathIsRelative(m_outputTargetProps.m_strJDFMarksFolder))
				strLocation = m_outputTargetProps.m_strLocation + _T("\\") + m_outputTargetProps.m_strJDFMarksFolder;
			else
				strLocation = m_outputTargetProps.m_strJDFMarksFolder;
		}
		else
			if (m_outputTargetProps.m_bJDFOutputMime)
				strLocation = theApp.settings.m_szTempDir;
	}

	CString strOutputPDFFile; 
	if ( (m_outputTargetProps.m_nType == CPDFTargetProperties::TypePDFFolder) || (m_outputTargetProps.m_nType == CPDFTargetProperties::TypeJDFFolder) )
	{
		TCHAR szDest[1024];
		CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)m_outputTargetProps.m_strLocation, NULL, nSide, NULL, -1, "", -1, -1, pFoldSheet);	
		if (bCreateMissingPath)
			if ( ! theApp.DirectoryExist(szDest))
				SHCreateDirectoryEx(NULL, szDest, NULL);
		strOutputPDFFile.Format(_T("%s\\%s"), szDest, GetOutFilename(nSide, pFoldSheet));
	}
	else
		strOutputPDFFile.Format(_T("%s\\%s"), theApp.settings.m_szTempDir, GetOutFilename(nSide, pFoldSheet));

	return strOutputPDFFile;
}

CString	CBookblock::GetOutFileTmpFullPath(int nSide, CFoldSheet* pFoldSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return ""; 
	if (m_outputTargetProps.m_strTargetName.IsEmpty())
		return _T("");

	CString strTmpOutputPDFFile; 
	strTmpOutputPDFFile.Format(_T("%s\\%s"), theApp.settings.m_szTempDir, GetOutFilename(nSide, pFoldSheet));

	return strTmpOutputPDFFile;
}

BOOL CBookblock::OutFileExists(int nSide, CFoldSheet* pFoldSheet)
{
	CFileStatus fileStatus;
	if (CFile::GetStatus(GetOutFileFullPath(nSide, pFoldSheet), fileStatus))	
		return TRUE;	
	else
		return FALSE;
}

BOOL CBookblock::NotYetOutputted()
{
	if (m_OutputDate.m_dt == 0.0)	
		return TRUE;
	else
		return FALSE;
}

BOOL CBookblock::ReadyForOutput()
{
	if ( ! NotYetOutputted())
		return FALSE;

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CArray <short*, short*> pageSequence;
	POSITION pos = m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_FoldSheetList.GetNext(pos);
		int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(rFoldSheet.m_nProductPartIndex);

		rFoldSheet.GetPageSequence(pageSequence,  -1);
		for (int nPageSeqIndex = 0; nPageSeqIndex < pageSequence.GetSize(); nPageSeqIndex++)
		{
			POSITION posPageTemplate = pDoc->m_PageTemplateList.FindIndex(*pageSequence[nPageSeqIndex] - 1, -nProductIndex - 1);
			if ( ! posPageTemplate)
				return FALSE;

			CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(posPageTemplate);
			CLayoutObject* pLayoutObject = rPageTemplate.GetLayoutObject();
			CPrintSheet*   pPrintSheet   = rPageTemplate.GetPrintSheet();
			if ( ! pLayoutObject || ! pPrintSheet)
				return FALSE;
			CPlateList&	plateList = (pLayoutObject->GetLayoutSideIndex() == FRONTSIDE) ? pPrintSheet->m_FrontSidePlates : pPrintSheet->m_BackSidePlates;
			//CPlate*		pPlate	  = ( ! plateList.GetCount()) ? &plateList.m_defaultPlate : &plateList.GetHead(); 
			CPlate*		pPlate	  = (plateList.GetCount()) ? &plateList.GetHead() : NULL;	// ReadyForOutput() only with auto server; so only if pdf pages are there 
			if ( ! pLayoutObject->GetExposure(pPlate))
				return FALSE;
		}
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////
// CFoldSheet

IMPLEMENT_SERIAL(CFoldSheet, CObject, VERSIONABLE_SCHEMA | 10)

CFoldSheet::CFoldSheet(): m_nFoldSheetNumber(0), m_nPagesLeft(0), m_nPagesRight(0),	m_nFoldSheetRefEdge(0), 
m_fShinglingValueX(0), m_fShinglingValueXFoot(0), m_fShinglingValueY(0), m_bShinglingValueLocked(FALSE), m_nProductPartIndex(0), m_bOutputDisabled(TRUE)
{
	m_strID = _T("");
	m_bFoldSheetNumLocked = FALSE;
	m_nProductIndex = -1;
}

CFoldSheet::CFoldSheet(const CFoldSheet& rFoldSheet)
{
	Copy(rFoldSheet);
}

CFoldSheet::~CFoldSheet()
{
	m_strFoldSheetTypeName.Empty();
	m_strNotes.Empty();

	m_LayerList.RemoveAll();
}



// operator overload for CFoldSheet - needed by CList functions:

const CFoldSheet& CFoldSheet::operator=(const CFoldSheet& rFoldSheet)
{
	Copy(rFoldSheet);	

	return *this;
}


void CFoldSheet::Copy(const CFoldSheet& rFoldSheet)
{
	m_strID					= rFoldSheet.m_strID;
	m_nFoldSheetNumber		= rFoldSheet.m_nFoldSheetNumber;
	m_bFoldSheetNumLocked	= rFoldSheet.m_bFoldSheetNumLocked;
	m_strNumerationStyle	= rFoldSheet.m_strNumerationStyle;
	m_nNumerationOffset		= rFoldSheet.m_nNumerationOffset;
	m_strFoldSheetTypeName	= rFoldSheet.m_strFoldSheetTypeName;
	m_nPagesLeft			= rFoldSheet.m_nPagesLeft;
	m_nPagesRight			= rFoldSheet.m_nPagesRight; 
	m_nFoldSheetRefEdge		= rFoldSheet.m_nFoldSheetRefEdge;
	m_strNotes				= rFoldSheet.m_strNotes;
	m_fShinglingValueX		= rFoldSheet.m_fShinglingValueX;
	m_fShinglingValueXFoot	= rFoldSheet.m_fShinglingValueXFoot;
	m_fShinglingValueY		= rFoldSheet.m_fShinglingValueY;
	m_bShinglingValueLocked	= rFoldSheet.m_bShinglingValueLocked;
	m_nProductIndex			= rFoldSheet.m_nProductIndex;
	m_nProductPartIndex		= rFoldSheet.m_nProductPartIndex;
	m_strPaperName			= rFoldSheet.m_strPaperName;
	m_nQuantityPlanned		= rFoldSheet.m_nQuantityPlanned;
	m_nQuantityExtra		= rFoldSheet.m_nQuantityExtra;
	m_nQuantityActual		= rFoldSheet.m_nQuantityActual;

	m_LayerList.RemoveAll();  // no append, so target list has to be empty

	POSITION pos = rFoldSheet.m_LayerList.GetHeadPosition();
	while (pos)
	{
		CFoldSheetLayer rFoldSheetLayer = rFoldSheet.m_LayerList.GetNext(pos);
		m_LayerList.AddTail(rFoldSheetLayer);
	}

	//CFoldSheetLayer  FoldSheetLayerBuf;
	//CFoldSheetLayer& rFoldSheetLayerBuf = FoldSheetLayerBuf;

	//for (unsigned i = 0; i < nLayers; i++)                        
	//{
	//	rFoldSheetLayerBuf = rFoldSheet.m_LayerList.GetAt(pos);
	//	m_LayerList.AddTail(rFoldSheetLayerBuf);
	//	rFoldSheet.m_LayerList.GetNext(pos);
	//}	
}


void CFoldSheet::Create(int countX, int countY, CString FoldSheetName, int nProductPartIndex)
{
	m_strNumerationStyle	= _T("$n");
	m_nNumerationOffset		= 0;
	if (FoldSheetName.IsEmpty())
		m_strFoldSheetTypeName = theApp.settings.m_szFoldSheetName;
	else
		m_strFoldSheetTypeName = FoldSheetName;
	m_nPagesLeft			= countX * countY;
	m_nPagesRight			= countX * countY; 
	m_nFoldSheetRefEdge		= LEFT_BOTTOM;
	m_strNotes				= "";
	m_fShinglingValueX		= 0.0f;
	m_fShinglingValueXFoot	= 0.0f;
	m_bShinglingValueLocked = FALSE;
	m_fShinglingValueY		= 0.0f;
	m_nProductPartIndex		= nProductPartIndex;

	CImpManDoc*	pDoc = (CImpManDoc*)CImpManDoc::GetDoc();
	if (pDoc)
	{
		CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
		m_strPaperName	   = rProductPart.m_strPaper;
		m_nQuantityPlanned = rProductPart.m_nDesiredQuantity;
		m_nQuantityExtra   = rProductPart.m_nExtra;
		m_nQuantityActual  = 0;
	}

	m_LayerList.RemoveAll(); 

	CFoldSheetLayer FoldSheetLayerBuf;

	FoldSheetLayerBuf.m_nPagesX = countX;
	FoldSheetLayerBuf.m_nPagesY = countY;

	for (int i = 0; i < countX * countY; i++)
	{
		FoldSheetLayerBuf.m_FrontPageArray.Add(1);
		FoldSheetLayerBuf.m_BackPageArray.Add(2);
		FoldSheetLayerBuf.m_HeadPositionArray.Add(TOP);
		FoldSheetLayerBuf.m_FrontSpineArray.Add(LEFT);
		FoldSheetLayerBuf.m_BackSpineArray.Add(LEFT);
	}

	m_LayerList.AddTail(FoldSheetLayerBuf);
}

BOOL CFoldSheet::CreateFromISC(const CString& strFullPath, CString strName, int nProductPartIndex, BOOL bSearchSubFolders)
{
	CFileException fileException;
	CFile		   file;
	if ( ! file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
	{
		if (bSearchSubFolders)
		{
			TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
			_tsplitpath(strFullPath, szDrive, szDir, szFname, szExt);
			CString strTargetPath;
			if ( ! theApp.FindFileFolder(CString(szDrive) + CString(szDir), CString(szFname) + CString(szExt), strTargetPath))
				return FALSE;
			else
			{
				strTargetPath += CString(szFname) + CString(szExt);
				if ( ! file.Open((LPCTSTR)strTargetPath, CFile::modeRead, &fileException))
					return FALSE;
			}
	
		}
		else
		{
			CString strMsg; strMsg.Format(_T("Can't open file %s, error = %u\n"), strFullPath, fileException.m_cause);
			KIMLogMessage(strMsg, KIM_MSGTYPE_INFO);
			return FALSE; 
		}
	}

	CArchive archive(&file, CArchive::load);
	DestructElements(this, 1);
	TRY	
	{
		SerializeElements(archive, this, 1);
	}
	CATCH (CException, e)	// catch exception, because when file is empty we get problems in DlgSelectFoldSchemePopup
	{
		return FALSE;
	}
	END_CATCH

	archive.Close();
	file.Close();

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath(strFullPath, szDrive, szDir, szFname, szExt);
	if ( ! strName.IsEmpty())
		m_strFoldSheetTypeName = strName;
	else
		m_strFoldSheetTypeName = szFname;


	CImpManDoc*	pDoc = (CImpManDoc*)CImpManDoc::GetDoc();
	if (pDoc)
	{
		CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
		m_nProductPartIndex = nProductPartIndex;
		m_strPaperName	    = rProductPart.m_strPaper;
		m_nQuantityPlanned  = rProductPart.m_nDesiredQuantity;
		m_nQuantityExtra    = rProductPart.m_nExtra;
		m_nQuantityActual   = 0;
	}

	return TRUE;
}

void CFoldSheet::DeleteContents()
{
	m_LayerList.RemoveAll();
}

BOOL CFoldSheet::IsEmpty()
{
	if ( ! m_LayerList.GetSize())
		return TRUE;

	BOOL	 bIsEmpty = TRUE;
	POSITION pos	  = m_LayerList.GetHeadPosition();
	while (pos)
	{
		CFoldSheetLayer& rLayer = m_LayerList.GetNext(pos);
		if (rLayer.m_HeadPositionArray.GetSize())
			bIsEmpty = FALSE;
	}

	return bIsEmpty;
}


int CFoldSheet::GetIndex()
{
	CImpManDoc*	pDoc = (CImpManDoc*)CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	int		 nFoldSheetIndex = 0;
	POSITION pos			 = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (&rFoldSheet == this)
			return nFoldSheetIndex;

		nFoldSheetIndex++;
	}
	return -1;
}

void CFoldSheet::SetDefaultPagesLeftRight()
{
	unsigned nLayers = m_LayerList.GetCount(); 
	POSITION pos     = m_LayerList.GetHeadPosition();
	int		 nPages  = 0;

	for (unsigned i = 0; i < nLayers; i++)                        
	{
		CFoldSheetLayer& rLayer = m_LayerList.GetAt(pos);
		nPages+= rLayer.m_nPagesX * rLayer.m_nPagesY * 2;
		m_LayerList.GetNext(pos);
	}	
	m_nPagesLeft = m_nPagesRight = nPages/2;
}

CString strDefLowRange, strDefHighRange;

CString CFoldSheet::GetPageRange(int nSide, CString& strLowRange, CString& strHighRange)
{
	CImpManDoc*	pDoc = (CImpManDoc*)CImpManDoc::GetDoc();
	if ( ! pDoc)
		return _T("");
	if (pDoc->m_Bookblock.IsEmpty())
		return _T("");

	CArray <short*, short*> leftPageSequence, rightPageSequence;
	GetPageSequence(leftPageSequence,  CDescriptionItem::FoldSheetLeftPart);
	GetPageSequence(rightPageSequence, CDescriptionItem::FoldSheetRightPart);

	int nMinLeft = -1, nMaxLeft = -1, nMinRight = -1, nMaxRight = -1;
	if (leftPageSequence.GetSize())
	{
		nMinLeft = *leftPageSequence[0];
		nMaxLeft = *leftPageSequence[leftPageSequence.GetSize() - 1];
	}
	if (rightPageSequence.GetSize()) 
	{
		nMinRight = *rightPageSequence[0];
		nMaxRight = *rightPageSequence[rightPageSequence.GetSize() - 1];
	}
	if (nMinLeft == -1)
	{
		nMinLeft = nMinRight;
		nMaxLeft = nMaxRight;
	}
	if (nMinRight == -1)
	{
		nMinRight = nMinLeft;
		nMaxRight = nMaxLeft;
	}

	if ( (nMinLeft != -1) && (nMaxLeft != -1) && (nMinRight != -1) && (nMaxRight != -1) )
	{
		int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(m_nProductPartIndex);
		POSITION posMinLeft  = pDoc->m_PageTemplateList.FindIndex(nMinLeft  - 1, -nProductIndex - 1);
		POSITION posMaxLeft  = pDoc->m_PageTemplateList.FindIndex(nMaxLeft  - 1, -nProductIndex - 1);
		POSITION posMinRight = pDoc->m_PageTemplateList.FindIndex(nMinRight - 1, -nProductIndex - 1);
		POSITION posMaxRight = pDoc->m_PageTemplateList.FindIndex(nMaxRight - 1, -nProductIndex - 1);
		if (posMinLeft && posMaxLeft && posMinRight && posMaxRight)
		{
			CString strMinLeft  = pDoc->m_PageTemplateList.GetAt(posMinLeft ).m_strPageID + " - ";
			CString strMaxLeft  = pDoc->m_PageTemplateList.GetAt(posMaxLeft ).m_strPageID;
			CString strMinRight = pDoc->m_PageTemplateList.GetAt(posMinRight).m_strPageID + " - ";
			CString strMaxRight = pDoc->m_PageTemplateList.GetAt(posMaxRight).m_strPageID;
			CString strSep		= " + ";

			if ( ((nMaxLeft == nMinRight - 1) || ((nMinLeft == nMinRight) && (nMaxLeft == nMaxRight))) && ! GetBoundProduct().m_bindingParams.m_bComingAndGoing )		// paste together only if not coming and going
			{
				//strLowRange = strMinLeft  + strMaxLeft; strHighRange = strMinRight + strMaxRight;
				strLowRange = strMinLeft + strMaxRight; strHighRange = _T("");
				return strMinLeft + strMaxRight;
			}
			else
			{
				switch (nSide)
				{
				case BOTHSIDES:	strLowRange = strMinLeft  + strMaxLeft; strHighRange = strMinRight + strMaxRight;
								return strLowRange + strSep + strHighRange;
				case LEFT:		return strMinLeft  + strMaxLeft;
				case RIGHT:		return strMinRight + strMaxRight;
				default:		return strMinLeft  + strMaxLeft + strSep + strMinRight + strMaxRight;
				}
			}
		}
	}

	return "";
}

int	CFoldSheet::GetMinPageNum()
{
	CImpManDoc*	pDoc = (CImpManDoc*)CImpManDoc::GetDoc();
	if (pDoc->m_Bookblock.IsEmpty())
		return 0;

	CArray <short*, short*> leftPageSequence;
	GetPageSequence(leftPageSequence,  CDescriptionItem::FoldSheetLeftPart);

	int nMinLeft = -1, nMaxLeft = -1, nMinRight = -1, nMaxRight = -1;
	if (leftPageSequence.GetSize())
		return *leftPageSequence[0];
	else
		return 0;
}

int	CFoldSheet::GetPageIndex(CPageTemplate* pPageTemplate)
{
	if ( ! pPageTemplate)
		return 0;
	CImpManDoc*	pDoc = (CImpManDoc*)CImpManDoc::GetDoc();
	if (pDoc->m_Bookblock.IsEmpty())
		return 0;

	CArray <short*, short*> PageSequence;
	GetPageSequence(PageSequence,  -1);
	ReducePageSequence(PageSequence);

	for (int i = 0; i < PageSequence.GetSize(); i++)
	{
		POSITION pos = pDoc->m_PageTemplateList.FindIndex(*PageSequence[i] - 1, -pPageTemplate->m_nProductIndex - 1);
		if ( ! pos)
			continue;
		CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
		if (&rPageTemplate == pPageTemplate)
			return i;
	}
	return -1;
}

CString	CFoldSheet::GetNumber()		
{ 
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return _T("");

	//if (pDoc->m_productionData.m_nProductionType == 1)		// Coming and going
	//{
	//	CFoldSheet* pSimilarFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.CGGetSimilarFoldSheet(*this, INT_MAX);
	//	if (pSimilarFoldSheet)
	//	{
	//		CString string;
	//		string.Format(_T(" %d/%d "), GetNumberOnly(), pSimilarFoldSheet->GetNumberOnly()); 
	//		return string;
	//	}
	//	else
	//		return CDlgNumeration::GenerateNumber(m_nFoldSheetNumber, m_strNumerationStyle, m_nNumerationOffset); 
	//}
	//else
		return CDlgNumeration::GenerateNumber(m_nFoldSheetNumber, m_strNumerationStyle, m_nNumerationOffset); 
}

void CFoldSheet::DrawPageNumRange(CDC* pDC, int nLeft, int nTop, int nBottom, int nClipWidth, float fScreenPrinterRatioX)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CArray <short*, short*> leftPageSequence, rightPageSequence;
	GetPageSequence(leftPageSequence,  CDescriptionItem::FoldSheetLeftPart);
	GetPageSequence(rightPageSequence, CDescriptionItem::FoldSheetRightPart);

	int nMinLeft = -1, nMaxLeft = -1, nMinRight = -1, nMaxRight = -1;
	if (leftPageSequence.GetSize())
	{
		nMinLeft = *leftPageSequence[0];
		nMaxLeft = *leftPageSequence[leftPageSequence.GetSize() - 1];
	}
	if (rightPageSequence.GetSize()) 
	{
		nMinRight = *rightPageSequence[0];
		nMaxRight = *rightPageSequence[rightPageSequence.GetSize() - 1];
	}
	if (nMinLeft == -1)
	{
		nMinLeft = nMinRight;
		nMaxLeft = nMaxRight;
	}
	if (nMinRight == -1)
	{
		nMinRight = nMinLeft;
		nMaxRight = nMaxLeft;
	}

	if ( (nMinLeft != -1) && (nMaxLeft != -1) && (nMinRight != -1) && (nMaxRight != -1) )
	{
		int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(m_nProductPartIndex);
		POSITION posMinLeft  = pDoc->m_PageTemplateList.FindIndex(nMinLeft  - 1, -nProductIndex - 1);
		POSITION posMaxLeft  = pDoc->m_PageTemplateList.FindIndex(nMaxLeft  - 1, -nProductIndex - 1);
		POSITION posMinRight = pDoc->m_PageTemplateList.FindIndex(nMinRight - 1, -nProductIndex - 1);
		POSITION posMaxRight = pDoc->m_PageTemplateList.FindIndex(nMaxRight - 1, -nProductIndex - 1);
		if (posMinLeft && posMaxLeft && posMinRight && posMaxRight)
		{
			CString strMinLeft  = pDoc->m_PageTemplateList.GetAt(posMinLeft ).m_strPageID + " - ";
			CString strMaxLeft  = pDoc->m_PageTemplateList.GetAt(posMaxLeft ).m_strPageID;
			CString strMinRight = pDoc->m_PageTemplateList.GetAt(posMinRight).m_strPageID + " - ";
			CString strMaxRight = pDoc->m_PageTemplateList.GetAt(posMaxRight).m_strPageID;
			CString strSep		= " + ";

			int nExtent1   = pDC->GetTextExtent("0000 - ").cx;
			int nExtent2   = pDC->GetTextExtent("0000").cx;
			int nMinLeftX  = nExtent1, nMaxLeftX = nMinLeftX + nExtent2;
			int nSepX	   = nMaxLeftX + (int)(20.0f * fScreenPrinterRatioX);
			int nMinRightX = nSepX + nExtent1, nMaxRightX = nMinRightX + nExtent2;

			if (nMinLeftX  >= nClipWidth)
				strMinLeft = ".......";
			if (nMaxLeftX  >= nClipWidth)
				strMaxLeft = ".......";
			if (nSepX	   >= nClipWidth)
				strSep		= "...";
			if (nMinRightX >= nClipWidth)
				strMinRight = ".......";
			if (nMaxRightX >= nClipWidth)
				strMaxRight = ".......";

			if ((nMaxLeft == nMinRight - 1) || ((nMinLeft == nMinRight) && (nMaxLeft == nMaxRight)) )
			{
				pDC->DrawText(strMinLeft,  CRect(nLeft, nTop, nLeft + nMinLeftX, nBottom), DT_VCENTER | DT_RIGHT);
				pDC->DrawText(strMaxRight, CRect(nLeft, nTop, nLeft + nMaxLeftX, nBottom), DT_VCENTER | DT_RIGHT);
			}
			else
			{
				pDC->DrawText(strMinLeft,  CRect(nLeft, nTop, nLeft + nMinLeftX,  nBottom), DT_VCENTER | DT_RIGHT);
				pDC->DrawText(strMaxLeft,  CRect(nLeft, nTop, nLeft + nMaxLeftX,  nBottom), DT_VCENTER | DT_RIGHT);
				pDC->DrawText(strSep,	   CRect(nLeft, nTop, nLeft + nSepX,	  nBottom), DT_VCENTER | DT_RIGHT);
				pDC->DrawText(strMinRight, CRect(nLeft, nTop, nLeft + nMinRightX, nBottom), DT_VCENTER | DT_RIGHT);
				pDC->DrawText(strMaxRight, CRect(nLeft, nTop, nLeft + nMaxRightX, nBottom), DT_VCENTER | DT_RIGHT);
			}
		}
	}
}

int	CFoldSheet::GetLayerIndex(CFoldSheetLayer* pLayer)
{
	if (m_LayerList.GetCount() <= 0)
		return -1;

	int		 nIndex = 0;
	POSITION pos	= m_LayerList.GetHeadPosition();
	while (pos)
	{
		if ( & m_LayerList.GetNext(pos) == pLayer)
			return nIndex;
		nIndex++;
	}

	return -1;
}

CFoldSheetLayer* CFoldSheet::GetLayer(int nIndex)
{
	if (m_LayerList.GetCount() <= 0)
		return NULL;

	POSITION pos = m_LayerList.FindIndex(nIndex);
	if ( ! pos)
		return NULL;

	return &m_LayerList.GetAt(pos);
}

CLayout* CFoldSheet::GetLayout(int nLayerIndex)
{
	CFoldSheetLayer* pLayer		 = GetLayer(nLayerIndex);
	CPrintSheet*	 pPrintSheet = (pLayer) ? pLayer->GetFirstPrintSheet(m_nProductPartIndex) : NULL;
	return (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
}

CPrintGroup& CFoldSheet::GetFirstPrintGroup()
{
	static CPrintGroup nullPrintGroup;
	
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return nullPrintGroup;

	for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
	{
		CPrintGroup&  rPrintGroup  = pDoc->m_printGroupList[i];
		CPrintGroupComponent* pComponent = rPrintGroup.GetFirstBoundComponent();
		while (pComponent)
		{
			if (pComponent->m_nProductClassIndex == m_nProductPartIndex)
				return rPrintGroup;
			pComponent = rPrintGroup.GetNextBoundComponent(pComponent);
		}
	}	

	return nullPrintGroup;
}

CPrintSheet* CFoldSheet::GetPrintSheet(int nLayerIndex)
{
	CFoldSheetLayer* pLayer = GetLayer(nLayerIndex);
	return (pLayer) ? pLayer->GetFirstPrintSheet(m_nProductPartIndex) : NULL;
}

CPrintSheet* CFoldSheet::GetNextPrintSheet(CPrintSheet* pPrintSheet, int nLayerIndex)
{
	CFoldSheetLayer* pLayer = GetLayer(nLayerIndex);
	return (pLayer) ? pLayer->GetNextPrintSheet(pPrintSheet, m_nProductPartIndex) : NULL;
}

CSize CFoldSheet::GetMatrix()
{
	int		 nxMax = 0;
	int		 nyMax = 0;
	POSITION pos   = m_LayerList.GetHeadPosition();
	while (pos)
	{
		CFoldSheetLayer& rLayer = m_LayerList.GetNext(pos);
		nxMax = max(nxMax, (int)rLayer.m_nPagesX);
		nyMax = max(nyMax, (int)rLayer.m_nPagesY);
	}	
	return CSize(nxMax, nyMax);
}

int	CFoldSheet::GetNumPages()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
		return m_nPagesLeft;
	else
		return m_nPagesLeft + m_nPagesRight;
}

CSize CFoldSheet::GetExtents(CPoint* pPtOrg, int nComponentRefIndex, CPrintSheet* pPrintSheet, CLayout* pLayout)
{
	if (pPtOrg)
		*pPtOrg = CPoint(0,0);

	CFoldSheetLayer* pLayer		 = GetLayer(0);
	if ( ! pPrintSheet)
		pPrintSheet = (pLayer) ? pLayer->GetFirstPrintSheet(m_nProductPartIndex) : NULL;
	if ( ! pLayout)
		pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return CSize(0, 0);
	if (nComponentRefIndex < 0)
		nComponentRefIndex = pPrintSheet->FoldSheetLayerToLayerRef(pLayer);

	CRect	 sheetRect(INT_MAX, INT_MIN, INT_MIN, INT_MAX);
	POSITION pos			= pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
		if (nComponentRefIndex != -1)
			if (rObject.m_nType == CLayoutObject::Page)
				if (nComponentRefIndex != rObject.m_nComponentRefIndex)
					continue;

		sheetRect.left	 = min(CPrintSheetView::WorldToLP(rObject.GetLeftWithBorder()),   sheetRect.left);
		sheetRect.top	 = max(CPrintSheetView::WorldToLP(rObject.GetTopWithBorder()),    sheetRect.top);
		sheetRect.right	 = max(CPrintSheetView::WorldToLP(rObject.GetRightWithBorder()),  sheetRect.right);
		sheetRect.bottom = min(CPrintSheetView::WorldToLP(rObject.GetBottomWithBorder()), sheetRect.bottom);
	}

	if (sheetRect.left == INT_MAX)
		return CSize(0, 0);

	if (pPtOrg)
	{
		pPtOrg->x = sheetRect.left;
		pPtOrg->y = sheetRect.bottom;
	}
	return CSize(sheetRect.right - sheetRect.left, sheetRect.top - sheetRect.bottom);
}

CRect CFoldSheet::DrawSheetPreview(CDC* pDC, CRect rcFrame, int nLayerIndex, int nRotation, int nTurned, BOOL bShowContent)
{
	pDC->FillSolidRect(rcFrame, WHITE);	//background

	CPrintSheet printSheet;
	CLayout		layout;
	printSheet.Create(0, -1);
	layout.Create(CPrintingProfile());
	printSheet.LinkLayout(&layout);
	layout.SetObjectsStatusAll(FALSE);

	float fWidth, fHeight; 
	if ( (nRotation == 0) || (nRotation == 180) )
		GetCutSize(nLayerIndex, fWidth, fHeight);
	else
		GetCutSize(nLayerIndex, fHeight, fWidth);

	CRect rcPlaceArea = CRect(0, 0, (long)(fWidth * 1000.0f), (long)(fHeight * 1000.0f));

	int nHeadPosition = TOP;
	switch (nRotation)
	{
	case 0:		nHeadPosition = TOP;	break;
	case 90:	nHeadPosition = LEFT;	break;
	case 180:	nHeadPosition = BOTTOM;	break;
	case 270:	nHeadPosition = RIGHT;	break;
	}

	layout.m_FrontSide.m_fPaperLeft = 0.0f; layout.m_FrontSide.m_fPaperBottom = 0.0f; layout.m_FrontSide.m_fPaperRight = fWidth; layout.m_FrontSide.m_fPaperTop = fHeight; layout.m_FrontSide.m_fPaperWidth = fWidth; layout.m_FrontSide.m_fPaperHeight = fHeight; 
	layout.m_BackSide.m_fPaperLeft  = 0.0f; layout.m_BackSide.m_fPaperBottom  = 0.0f; layout.m_BackSide.m_fPaperRight  = fWidth; layout.m_BackSide.m_fPaperTop  = fHeight; layout.m_BackSide.m_fPaperWidth  = fWidth; layout.m_BackSide.m_fPaperHeight  = fHeight; 
	layout.MakeNUp(FALSE, printSheet, this, nLayerIndex, 1, nHeadPosition, NULL, FALSE, rcPlaceArea);

	if (layout.m_FoldSheetLayerRotation.GetSize() > 0)
		layout.m_FoldSheetLayerRotation[0] = nRotation;
	if (layout.m_FoldSheetLayerTurned.GetSize() > 0)
		layout.m_FoldSheetLayerTurned[0]   = nTurned;

	int		nExtentX	= CPrintSheetView::WorldToLP(fWidth);
	int		nExtentY	= CPrintSheetView::WorldToLP(fHeight);
	int		nXOrg		= 0;
	int		nYOrg		= 0;
	CRect	rcViewPort	= rcFrame;
	rcViewPort.DeflateRect(1, 1);

	pDC->SaveDC();
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(nExtentX, nExtentY);
	pDC->SetWindowOrg(nXOrg,	nExtentY + nYOrg);
	pDC->SetViewportExt(rcViewPort.Width(), -rcViewPort.Height());
	pDC->SetViewportOrg(rcViewPort.left, rcViewPort.top);

	POSITION pos = layout.m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = layout.m_FrontSide.m_ObjectList.GetNext(pos);
		rObject.Draw(pDC, TRUE, FALSE, FALSE, &printSheet);
		if (bShowContent)
			rObject.DrawPreview(pDC, &printSheet, -1);
	}

	layout.m_FrontSide.DrawFoldSheet(pDC, FRONTSIDE, &printSheet, 0, this, NULL);

	CRect rcBox = CRect(CPoint(nXOrg, nYOrg), CSize(nExtentX, nExtentY));
	pDC->LPtoDP(&rcBox);
	rcBox.NormalizeRect();

	pDC->RestoreDC(-1);
	printSheet.DeleteContents();
	layout.DeleteContents();

	return rcBox;
}

CRect CFoldSheet::DrawThumbnail(CDC* pDC, CRect rcFrame, BOOL bHighlight, BOOL bDrawCentered, int nLayerHighlightIndex, int nHighlightSpineSide)
{
	if ( (rcFrame.Width() < Pixel2MM(pDC, 10)) || (rcFrame.Height() < Pixel2MM(pDC, 10)) )
		return CRect(0,0,0,0);

	int		 nMaxPagesX		= 0, nMaxPagesY = 0;
	int		 nPageWidth		= Pixel2MM(pDC, 10), nPageHeight = Pixel2MM(pDC, 16);
	int		 nXPos			= 0, nYPos = 0;
	POSITION pos			= m_LayerList.GetHeadPosition();
	int		 nBoundingWidth = 0, nBoundingHeight = 0;
	int		 nLayerIndex	= 0;
//// first calculate width and height of thumbnail
	BOOL  bPortrait = TRUE;
	while (pos)
	{
		CFoldSheetLayer& rLayer = m_LayerList.GetNext(pos);
		if ( ! rLayer.m_HeadPositionArray.GetSize())
			continue;

		bPortrait		 = ( (rLayer.m_HeadPositionArray[0] == TOP) || (rLayer.m_HeadPositionArray[0] == BOTTOM) ) ? TRUE : FALSE;
		CRect pageRect	 = (bPortrait) ? CRect(nXPos,				nYPos + nPageHeight * (rLayer.m_nPagesY - 1), nXPos + nPageWidth,  nYPos + nPageHeight *  rLayer.m_nPagesY) : 
										 CRect(nXPos,				nYPos + nPageWidth  * (rLayer.m_nPagesY - 1), nXPos + nPageHeight, nYPos + nPageWidth  *  rLayer.m_nPagesY);
		pageRect.OffsetRect(nLayerIndex * nPageWidth/2, nLayerIndex * nPageWidth/2);
		short nPageIndex = 0;
		for (unsigned i = 0; i < rLayer.m_nPagesY; i++)
		{
			for (unsigned j = 0; j < rLayer.m_nPagesX; j++, nPageIndex++)
			{
				nBoundingWidth  = max(nBoundingWidth,  pageRect.right);
				nBoundingHeight = max(nBoundingHeight, pageRect.bottom);
				pageRect.OffsetRect(pageRect.Width(), 0);  // move one step rigth
			}
			pageRect.OffsetRect(-((int)rLayer.m_nPagesX * pageRect.Width()), -pageRect.Height());// move to left sheet side and one step down
		}
		nMaxPagesX = max(nMaxPagesX, (int)rLayer.m_nPagesX);
		nMaxPagesY = max(nMaxPagesY, (int)rLayer.m_nPagesY);
	}

	nBoundingWidth  += Pixel2MM(pDC, 4);	// for RefEdge
	nBoundingHeight += Pixel2MM(pDC, 4);	// for RefEdge

//// and fit to rcFrame
	float fRatio = max( ((float)nBoundingWidth / (float)rcFrame.Width()), ((float)nBoundingHeight / (float)rcFrame.Height()) );
//	if (fRatio > 1.0f)
	{
		nPageWidth = (int)((float)nPageWidth / fRatio + 0.5f);	nPageHeight	= (int)((float)nPageHeight / fRatio + 0.5f); 
		float fMaxPageWidth  = (float)Pixel2MM(pDC, 8);
		float fMaxPageHeight = (float)Pixel2MM(pDC, 10);
		if (nPageWidth > fMaxPageWidth)
		{
			fRatio = fMaxPageWidth /   (float)nPageWidth; nPageWidth = (int)((float)nPageWidth * fRatio + 0.5f); nPageHeight = (int)((float)nPageHeight * fRatio + 0.5f); 
		}
		if (nPageHeight > fMaxPageHeight)
		{
			fRatio = fMaxPageHeight / (float)nPageHeight; nPageWidth = (int)((float)nPageWidth * fRatio + 0.5f); nPageHeight = (int)((float)nPageHeight * fRatio + 0.5f); 
		}
			
		nBoundingWidth  = (bPortrait) ? nPageWidth  * nMaxPagesX : nPageHeight * nMaxPagesX; 
		nBoundingHeight = (bPortrait) ? nPageHeight * nMaxPagesY : nPageWidth  * nMaxPagesY;  
	}
///////////////

//// now do render
	if (bDrawCentered)
	{
		nXPos = rcFrame.CenterPoint().x - nBoundingWidth  / 2;
		nYPos = rcFrame.CenterPoint().y - nBoundingHeight / 2;
	}
	else
	{
		nXPos = rcFrame.left; nYPos = rcFrame.top;
	}
	CBrush fillBrush(RGB(242,242,255));
	CBrush blueBrush(RGB(220,220,255));
	CPen   penHighlight(PS_SOLID, 1, (bHighlight) ? BLUE : DARKGRAY);
	CPen   penLowlight(PS_SOLID, 1, (bHighlight) ? RGB(180, 180, 255) : LIGHTGRAY);
	CBrush* pOldBrush = pDC->SelectObject(&fillBrush);
	CPen*	pOldPen	  = pDC->SelectObject(&penHighlight);
	pos	  = m_LayerList.GetHeadPosition();
	CRect rcBounds(nXPos,nYPos,nXPos,nYPos);
	BOOL bHightlight = TRUE;
	nLayerIndex = 0;
	while (pos)
	{
		CFoldSheetLayer& rLayer = m_LayerList.GetNext(pos);
		if ( ! rLayer.m_HeadPositionArray.GetSize())
			continue;

		if (nLayerHighlightIndex != -1)
			if (nLayerIndex == nLayerHighlightIndex)
				pDC->SelectObject(&penHighlight);
			else
				pDC->SelectObject(&penLowlight);

		bPortrait		 = ( (rLayer.m_HeadPositionArray[0] == TOP) || (rLayer.m_HeadPositionArray[0] == BOTTOM) ) ? TRUE : FALSE;
		CRect pageRect	 = (bPortrait) ? CRect(nXPos,				nYPos + nPageHeight * (rLayer.m_nPagesY - 1), nXPos + nPageWidth,  nYPos + nPageHeight *  rLayer.m_nPagesY) : 
										 CRect(nXPos,				nYPos + nPageWidth  * (rLayer.m_nPagesY - 1), nXPos + nPageHeight, nYPos + nPageWidth  *  rLayer.m_nPagesY);
		pageRect.OffsetRect(nLayerIndex * nPageWidth/2, nLayerIndex * nPageWidth/2);
		short nPageIndex = 0;
		for (unsigned i = 0; i < rLayer.m_nPagesY; i++)
		{
			for (unsigned j = 0; j < rLayer.m_nPagesX; j++, nPageIndex++)
			{
				if (nHighlightSpineSide != -1)
				{
					if ( (rLayer.m_FrontSpineArray[nPageIndex] == nHighlightSpineSide) || (nHighlightSpineSide == BOTHSIDES) )
						pDC->SelectObject(&fillBrush);
					else
						pDC->SelectObject(&blueBrush);
				}

				CPrintSheetView::DrawRectangleExt(pDC, pageRect);
				DrawThumbnailHeadPosition(pDC, pageRect, rLayer.m_HeadPositionArray[nPageIndex], FRONTSIDE);

				rcBounds.left   = min(rcBounds.left,   pageRect.left);
				rcBounds.top    = min(rcBounds.top,    pageRect.top);
				rcBounds.right  = max(rcBounds.right,  pageRect.right);
				rcBounds.bottom = max(rcBounds.bottom, pageRect.bottom);

				pageRect.OffsetRect(pageRect.Width(), 0);  // move one step rigth
			}
			pageRect.OffsetRect(-((int)rLayer.m_nPagesX * pageRect.Width()), -pageRect.Height());// move to left sheet side and one step down
		}
		nLayerIndex++;
	}
	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	fillBrush.DeleteObject();
	blueBrush.DeleteObject();
	penHighlight.DeleteObject();
	penLowlight.DeleteObject();

	DrawThumbnailRefEdge(pDC, FRONTSIDE, CRect(nXPos, nYPos, nXPos + nBoundingWidth, nYPos + nBoundingHeight));

	return rcBounds;
}

void CFoldSheet::DrawThumbnailHeadPosition(CDC* pDC, CRect rect, int nHeadPosition, int nForm)
{
	CPoint p[3];
	int	   dx = rect.Width()/3, dy = rect.Height()/3;

	if (nForm == BACKSIDE)
	{
		switch (nHeadPosition)
		{
		case LEFT:  nHeadPosition = RIGHT; break;
		case RIGHT: nHeadPosition = LEFT;  break;
		default: break;
		}
	}

	switch (nHeadPosition)
	{
	case TOP:
		p[0] = rect.TopLeft(); 
		p[1] = rect.CenterPoint() - CSize(0, dy);
		p[2] = rect.BottomRight() - CSize(0, rect.Height());
		break;
	case RIGHT:
		p[0] = rect.BottomRight() - CSize(0, rect.Height());
		p[1] = rect.CenterPoint() + CSize(dx, 0);
		p[2] = rect.BottomRight();
		break;
	case BOTTOM:
		p[0] = rect.BottomRight();
		p[1] = rect.CenterPoint() + CSize(0, dy);
		p[2] = rect.TopLeft() + CSize(0, rect.Height());
		break;
	case LEFT:
		p[0] = rect.TopLeft() + CSize(0, rect.Height());
		p[1] = rect.CenterPoint() - CSize(dx, 0);
		p[2] = rect.TopLeft();
		break;
	default:
		return;	// draw nothing
	}

//	CPen  pen(PS_SOLID, 1, RGB(64,64,255));
//	CPen* pOldPen = pDC->SelectObject(&pen);

	pDC->Polyline(p, 3);

//	pDC->SelectObject(pOldPen);
//	pen.DeleteObject();
}


void CFoldSheet::DrawThumbnailRefEdge(CDC* pDC, int nForm, CRect sheetRect)
{
	CPoint    p[4];
	unsigned  Edge = m_nFoldSheetRefEdge;
	const int nEdgeLen = Pixel2MM(pDC, 6);
	CRect	  rect;

	switch (nForm)
	{
	case FRONTSIDE: 
		break;
	case BACKSIDE:
		switch (m_nFoldSheetRefEdge)
		{
		case LEFT_TOP:     Edge = RIGHT_TOP;    break;
		case RIGHT_TOP:    Edge = LEFT_TOP;     break;
		case LEFT_BOTTOM:  Edge = RIGHT_BOTTOM; break;
		case RIGHT_BOTTOM: Edge = LEFT_BOTTOM;  break;
		default: return;
		}
		break;
		default: return;
	}

	sheetRect.InflateRect(Pixel2MM(pDC, 1), Pixel2MM(pDC, 1));	// distance of edge to pages

	switch (Edge)
	{
	case LEFT_TOP: 
		p[1] = sheetRect.TopLeft();
		p[0] = p[1] + CSize(0, nEdgeLen);
		p[2] = p[1] + CSize(nEdgeLen, 0);
		p[3] = p[1];
		break;
	case RIGHT_TOP: 
		p[1] = sheetRect.TopLeft() + CSize(sheetRect.Width(), 0);
		p[0] = p[1] - CSize(nEdgeLen, 0);
		p[2] = p[1] + CSize(0, nEdgeLen);
		p[3] = p[0];
		break;
	case RIGHT_BOTTOM: 
		p[1] = sheetRect.BottomRight();
		p[0] = p[1] - CSize(0, nEdgeLen);
		p[2] = p[1] - CSize(nEdgeLen, 0);
		p[3] = p[0] - CSize(nEdgeLen, 0);
		break;
	case LEFT_BOTTOM: 
		p[1] = sheetRect.BottomRight() - CSize(sheetRect.Width(), 0);;
		p[0] = p[1] + CSize(nEdgeLen, 0);
		p[2] = p[1] - CSize(0, nEdgeLen);
		p[3] = p[2];
		break;
	default:
		return;
	}

	CPen redPen(PS_SOLID, 1, RGB(200,0,0));
	CPen* pOldPen = pDC->SelectObject(&redPen);
	pDC->Polyline(p, 3);
	pDC->SelectObject(pOldPen);
}

int	CFoldSheet::GetNumSamePrintLayout()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int		 nLayerIndex = 0;
	CLayout* pLayout	 = GetLayout(nLayerIndex);
	int		 nNum		 = 0;
	POSITION pos		 = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos); 
		if (rFoldSheet.m_nProductPartIndex != m_nProductPartIndex)
			continue;
		if (rFoldSheet.m_strFoldSheetTypeName != m_strFoldSheetTypeName)
			continue;

		if (rFoldSheet.GetLayout(nLayerIndex) == pLayout)	
			nNum++;
	}

	return nNum;
}

CRect CFoldSheet::DrawSection(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CBoundProduct& rBoundProduct = GetBoundProduct();

	CRect rcImg = rcFrame; rcImg.DeflateRect(0, 3); rcImg.right = rcImg.left + 32; rcImg.bottom = rcImg.top + 32;
	CImage img;
	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE((rBoundProduct.m_bindingParams.m_bindingDefs.m_nBinding == CBindingDefs::SaddleStitched) ? IDB_SECTION_STITCHED : IDB_SECTION_PERFECT));				
	if ( ! img.IsNull())
		img.TransparentBlt(pDC->m_hDC, rcImg.left + 2, rcImg.top + 5, img.GetWidth(), img.GetHeight(), WHITE);
	rcBox.UnionRect(rcBox, rcImg);

	pDC->SetTextColor(BLACK);
	CRect rcText = rcImg; rcText.left = rcImg.right + 5; rcText.right = rcFrame.right;
	DrawTextMeasured(pDC, GetPageRange(), rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
	rcBox.UnionRect(rcBox, rcImg);

	rcBox.right += 5;

	font.DeleteObject();

	return rcBox;
}

CRect CFoldSheet::DrawSummary(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	CBoundProductPart&  rProductPart = GetProductPart();

	int nTextHeight = 15;

	CString strLabel, strOut; 
	strLabel.LoadString(IDS_PAPERLIST_FORMAT);
	int nLen = pDC->GetTextExtent(strLabel).cx;
	if (GetQuantityPlanned() > 0)
	{
		strLabel.LoadString(IDS_TARGET_QUANTITY);
		nLen = max(nLen, pDC->GetTextExtent(strLabel).cx);
		strLabel.LoadString(IDS_ACTUAL_QUANTITY);
		nLen = max(nLen, pDC->GetTextExtent(strLabel).cx);
		strLabel.LoadString(IDS_DIFFERENCE);
		nLen = max(nLen, pDC->GetTextExtent(strLabel).cx);
	}
	nLen += 5;

	CFont font, boldFont;
	font.CreateFont		(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont (13, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));	
	CFont* pOldFont = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT); 

	rcFrame.left += 10; rcFrame.top += 3;

	CRect rcThumbnail = rcFrame; rcThumbnail.bottom = rcThumbnail.top + 50;
	rcThumbnail = DrawThumbnail(pDC, rcThumbnail, TRUE, FALSE);// pDisplayList, nLen, FALSE);
	rcBox.UnionRect(rcBox, rcThumbnail);

	rcFrame.left = rcThumbnail.right + 30;

	CRect rcText = rcFrame; 
	if (m_LayerList.GetCount() > 1)
	{
		int nActFoldSheetRefIndex = 0;
		int nIndex = (pPrintSheet->m_FoldSheetLayerRefs.GetSize() > 0) ? pPrintSheet->m_FoldSheetLayerRefs[nActFoldSheetRefIndex].m_nFoldSheetLayerIndex : 0;
		strOut.Format(_T("%s (%d):"), GetNumber(), nIndex + 1); 
	}
	else
		strOut.Format(_T("%s:"), GetNumber()); 

	pDC->SetTextColor(RGB(200, 100, 0));	
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
	rcText.left += pDC->GetTextExtent(strOut).cx + 5;

	pDC->SetTextColor(GUICOLOR_CAPTION);	
	DrawTextMeasured(pDC, m_strFoldSheetTypeName, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
	rcText.left += pDC->GetTextExtent(m_strFoldSheetTypeName).cx + 5;

	pDC->SelectObject(&boldFont);

	strOut.Format(_T("  %s "), rProductPart.GetBoundProduct().m_strProductName);
	pDC->SetTextColor(GUICOLOR_CAPTION);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
	rcText.left = rcBox.right + 5;
	pDC->SetTextColor(RGB(40,120,30));	// green
	DrawTextMeasured(pDC, rProductPart.m_strName, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	pDC->SelectObject(&font);

	rcFrame.top += nTextHeight + 5;

	CRect rcRet;
	rcRet = DrawSize(pDC, rcFrame, NULL, nLen, pPrintSheet);
	rcBox.UnionRect(rcBox, rcRet);

	if (GetQuantityPlanned() > 0)
	{
		rcFrame.top = rcRet.bottom + 5;
		rcRet = DrawTargetQuantity(pDC, rcFrame, NULL, nLen);
		rcBox.UnionRect(rcBox, rcRet);

		rcFrame.top = rcRet.bottom + 5;
		rcRet = DrawActualQuantity(pDC, rcFrame, NULL, nLen);
		rcBox.UnionRect(rcBox, rcRet);

		rcFrame.top = rcRet.bottom + 5;
		rcRet = DrawDifferenceQuantity(pDC, rcFrame, NULL, nLen);
		rcBox.UnionRect(rcBox, rcRet);
	}

	rcBox.bottom += 5;

	if (pDisplayList)
	{
		CRect rcDI = rcBox; rcDI.right += 20; rcDI.bottom += 5;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintComponentsDetailView, FoldSheetProps), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			CImage  img; img.LoadFromResource(theApp.m_hInstance, IDB_DROPDOWN);		
			if ( ! img.IsNull())
			{
				CRect rcImg = rcDI; rcImg.left = rcDI.right - 10; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcBox.top + 5; rcImg.bottom = rcImg.top + img.GetHeight();
				img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
			}
		}
	}

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CFoldSheet::DrawSize(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth, CPrintSheet* pPrintSheet)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));

	int nTextValign = DT_VCENTER;

	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_PAPERLIST_FORMAT); strLabel += _(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);
		pDC->SetTextColor(BLACK);
		rcText.left = rcLabel.right + 5;
	}

	CBoundProductPart& rProductPart = GetProductPart();
	CString strOut;
	int		nLayerIndex = 0;
	float	fWidth,	  fHeight;
	CString	strWidth, strHeight, strWidthPage, strHeightPage;
	GetCutSize(nLayerIndex, fWidth, fHeight);
	CPrintSheetView::TruncateNumber(strWidth,		fWidth);
	CPrintSheetView::TruncateNumber(strHeight,		fHeight);
	CPrintSheetView::TruncateNumber(strWidthPage,	rProductPart.m_fPageWidth);
	CPrintSheetView::TruncateNumber(strHeightPage,	rProductPart.m_fPageHeight);
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);
	rcText.left += pDC->GetTextExtent(strOut).cx + 5;
	pDC->SetTextColor(DARKGRAY);
	strOut.Format(_T("(%s x %s)"), strWidthPage, strHeightPage);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CFoldSheet::DrawTargetQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;	rcText.bottom = rcText.top + nTextHeight;

	CString strOut = FormatQuantityString(GetQuantityPlanned());
	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_TARGET_QUANTITY); strLabel += _T(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		rcText.left = rcLabel.right + 5;
	}

	pDC->SetTextColor(BLACK);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	
	font.DeleteObject();

	return rcBox;
}

CRect CFoldSheet::DrawActualQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;
	rcText.bottom = rcText.top + nTextHeight;

	CString strOut = FormatQuantityString(GetQuantityActual());
	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_ACTUAL_QUANTITY); strLabel += _T(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		rcText.left = rcLabel.right + 5;
	}

	pDC->SetTextColor(BLACK);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CFoldSheet::DrawDifferenceQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;
	rcText.bottom = rcText.top + nTextHeight;

	int		nDifference = GetQuantityActual() - GetQuantityPlanned();
	CString strOut1		= FormatQuantityString(nDifference);
	CString strOut2;	  strOut2.Format(_T("%.1f%%"), fabs(((float)GetQuantityActual()/(float)GetQuantityPlanned() - 1.0f) * 100.0f));
	COLORREF crColor1	= BLACK;
	COLORREF crColor2	= (nDifference < 0) ? RED : RGB(80,150,80);

	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_DIFFERENCE); strLabel += _T(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		rcText.left = rcLabel.right + 5;
	}

	if (nDifference < 0)
		rcText.left -= pDC->GetTextExtent(_T("-")).cx;
	else
		if (nDifference > 0)
		{
			rcText.left -= pDC->GetTextExtent(_T("+")).cx;
			strOut1.Insert(0, _T("+"));
		}

	pDC->SetTextColor(crColor1);
	DrawTextMeasured(pDC, strOut1, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	rcText.left = rcBox.right + 5;

	pDC->SetTextColor(crColor2);
	DrawTextMeasured(pDC, strOut2, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CFoldSheet::DrawCoverage(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth, CPrintSheet* pPrintSheet)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	int nTab = rcFrame.Width()/2;
	CRect rcText = rcFrame;
	rcText.bottom = rcText.top + nTextHeight;

	int nTextValign = DT_TOP;

	CString strOut;	
	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_COVERAGE); strLabel += _(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		pDC->DrawText(strLabel, rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
		pDC->SetTextColor(BLACK);
		rcText.left = rcLabel.right + 5;
	}

	CString strPercentage;	strPercentage.Format(_T("%.1f"), CalcCoverage(pPrintSheet) * 100.0f);
	strOut.Format(_T("%s%%"), strPercentage);
	pDC->DrawText(strOut, rcText, DT_LEFT | nTextValign | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.UnionRect(rcBox, rcText);

	font.DeleteObject();

	return rcBox;
}

int	CFoldSheet::GetNUpPrintLayout(CPrintSheet* pPrintSheet, int nLayerIndex)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	if ( ! pPrintSheet)
	{
		CFoldSheetLayer* pLayer			 = GetLayer(nLayerIndex);
		pPrintSheet						 = (pLayer) ? pLayer->GetFirstPrintSheet(m_nProductPartIndex) : NULL;
	}
	if ( ! pPrintSheet)
		return 0;
	int	nFoldSheetIndex = GetFoldSheetIndex();
	int	nNUp			= 0;
	for (int i = 0; i < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); i++)
	{
		if ( (pPrintSheet->m_FoldSheetLayerRefs[i].m_nFoldSheetIndex == nFoldSheetIndex) && (pPrintSheet->m_FoldSheetLayerRefs[i].m_nFoldSheetLayerIndex == nLayerIndex) )
			nNUp++;
	}

	return nNUp;
}

int	CFoldSheet::GetNUpAll(int nLayerIndex)
{
	int nNUp = 0;

	CPrintSheet* pPrintSheet = GetPrintSheet(nLayerIndex);
	while (pPrintSheet)
	{
		nNUp += GetNUpPrintLayout(pPrintSheet, nLayerIndex);

		pPrintSheet = GetNextPrintSheet(pPrintSheet, nLayerIndex);
	}

	return nNUp;
}

int	CFoldSheet::GetFoldSheetIndex()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nIndex = 0;
	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos); 
		if (&rFoldSheet == this)
			return nIndex;
		nIndex++;
	}
	return -1;
}

CBoundProduct& CFoldSheet::GetBoundProduct()
{
	return GetProductPart().GetBoundProduct();
}

CBoundProductPart& CFoldSheet::GetProductPart()
{
	if (m_nProductPartIndex < 0)
		return theApp.m_defaultBoundProductPart;

	static private CBoundProductPart nullBoundProductPart;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullBoundProductPart;

	return pDoc->GetBoundProductPart(m_nProductPartIndex);
}

//double g_duration;

BOOL CFoldSheet::IsPartOfBoundProduct(int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return FALSE;

	//g_duration++;

	if (nProductPartIndex == ALL_PARTS)
		return TRUE;
	else
		if (nProductPartIndex >= 0)
			return (m_nProductPartIndex == nProductPartIndex) ? TRUE : FALSE;
		else
		{
			if (m_nProductIndex < 0)
				m_nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(m_nProductPartIndex);

			if ( (-nProductPartIndex - 1) == m_nProductIndex)
				return TRUE;

			//CBoundProduct&		rBoundProduct = pDoc->GetBoundProduct(-nProductPartIndex - 1);	// nProductPartIndex == -1 means 1st bound product (index 0), nProductPartIndex == -2 means 2nd bound product (index 1), ...
			//CBoundProductPart&	rProductPart  = pDoc->GetBoundProductPart(m_nProductPartIndex);
			//if (&rProductPart.GetBoundProduct() == &rBoundProduct)
			//	return TRUE;
		}
	return FALSE;
}

void CFoldSheet::GetCutSize(int nLayerIndex, float& fCutWidth, float& fCutHeight)
{
	fCutWidth = fCutHeight = 0.0f;
	CBoundProductPart& rProductPart = GetProductPart();
	CFoldSheetLayer*   pLayer		= GetLayer(nLayerIndex);
	if ( ! pLayer)
		return;

	float fLayerWidth = 0.0f, fLayerHeight = 0.0f;
	int	nPageIndex = 0;
	for (unsigned int ny = 0; ny < pLayer->m_nPagesY; ny++)
	{
		float fRowWidth = 0.0f, fRowHeight = 0.0f;
		for (unsigned int nx = 0; nx < pLayer->m_nPagesX; nx++)
		{
			float fWidth, fHeight, fDummy;
			GetPageCutBox(pLayer, nPageIndex, rProductPart, fWidth, fHeight, fDummy, fDummy, fDummy, fDummy);
			fRowHeight = max(fRowHeight, fHeight);
			fRowWidth += fWidth;
			nPageIndex++;
		}
		fLayerWidth   = max(fLayerWidth, fRowWidth);
		fLayerHeight += fRowHeight;
	}
	fCutWidth  = max(fCutWidth,  fLayerWidth);
	fCutHeight = max(fCutHeight, fLayerHeight);
}

float CFoldSheet::GetCutMargin(int nLayerIndex, int nSide)
{
	CBoundProductPart& rProductPart = GetProductPart();
	CFoldSheetLayer* pLayer = GetLayer(nLayerIndex);
	if ( ! pLayer)
		return 0.0f;

	float fWidth, fHeight, fNetLeft, fNetBottom, fNetRight, fNetTop;
	float fNetWidth	 = rProductPart.m_fPageWidth;
	float fNetHeight = rProductPart.m_fPageHeight;
	int   nPageIndex = 0;
	switch (nSide)
	{
	case LEFT:		GetPageCutBox(pLayer, nPageIndex, rProductPart, fWidth, fHeight, fNetLeft, fNetBottom, fNetRight, fNetTop);
					return fNetLeft;
	case BOTTOM:	GetPageCutBox(pLayer, nPageIndex, rProductPart, fWidth, fHeight, fNetLeft, fNetBottom, fNetRight, fNetTop);
					return fNetBottom;
	case RIGHT:		nPageIndex = pLayer->m_nPagesX - 1;
					GetPageCutBox(pLayer, nPageIndex, rProductPart, fWidth, fHeight, fNetLeft, fNetBottom, fNetRight, fNetTop);
					return fWidth  - fNetRight;
	case TOP:		nPageIndex = pLayer->m_nPagesX * pLayer->m_nPagesY - 1;
					GetPageCutBox(pLayer, nPageIndex, rProductPart, fWidth, fHeight, fNetLeft, fNetBottom, fNetRight, fNetTop);
					return fHeight - fNetTop;
	}

	return 0.0f;
}

void CFoldSheet::GetPageCutBox(CFoldSheetLayer* pLayer, int nPageIndex, CBoundProductPart& rProductPart, float& fCutBoxWidth, float& fCutBoxHeight, 
							   float& fPageLeft, float& fPageBottom, float& fPageRight, float& fPageTop, BOOL bRotate)
{
	fCutBoxWidth = fCutBoxHeight = fPageLeft = fPageBottom = fPageRight = fPageTop = 0.0f;

	if ( ! pLayer)
		return;
	if ( ! pLayer->m_HeadPositionArray.GetSize())
		return;

	CBoundProduct& rProduct = rProductPart.GetBoundProduct();

	GetPageCutBox(pLayer, nPageIndex, rProductPart.m_nPageOrientation, rProductPart.m_fPageWidth, rProductPart.m_fPageHeight, rProduct.m_bindingParams.m_fOverFold, rProduct.m_bindingParams.m_nOverFoldSide, 
									  rProductPart.m_trimmingParams.m_fHeadTrim, rProductPart.m_trimmingParams.m_fFootTrim, rProductPart.m_trimmingParams.m_fSideTrim, rProduct.m_bindingParams.GetSpine(rProductPart) / 2.0f, 
									  fCutBoxWidth, fCutBoxHeight, fPageLeft, fPageBottom, fPageRight, fPageTop, bRotate);
}

void CFoldSheet::GetPageCutBox(CFoldSheetLayer* pLayer, int nPageIndex, int nPageOrientation, float fPageWidth, float fPageHeight, float fOverFold, int nOverFoldSide, float fHeadTrim, float fFootTrim, float fSideTrim, float fSpine,
							   float& fCutBoxWidth, float& fCutBoxHeight, float& fPageLeft, float& fPageBottom, float& fPageRight, float& fPageTop, BOOL bRotate)
{
	fCutBoxWidth = fCutBoxHeight = fPageLeft = fPageBottom = fPageRight = fPageTop = 0.0f;

	if ( ! pLayer)
		return;
	if ( ! pLayer->m_HeadPositionArray.GetSize())
		return;

	fOverFold = (nOverFoldSide == CBindingProcess::OverFoldNone) ? 0.0f : fOverFold;
	if ( (nOverFoldSide == CBindingProcess::OverFoldFront) && (pLayer->m_FrontSpineArray[nPageIndex] == RIGHT) )
		fOverFold = 0.0f;
	if ( (nOverFoldSide == CBindingProcess::OverFoldBack)  && (pLayer->m_FrontSpineArray[nPageIndex] == LEFT) )
		fOverFold = 0.0f;

	fCutBoxWidth  = fPageWidth  + fSpine + fSideTrim + fOverFold;
	fCutBoxHeight = fPageHeight + fHeadTrim  + fFootTrim;

	if ( (pLayer->m_HeadPositionArray[nPageIndex] == LEFT) || (pLayer->m_HeadPositionArray[nPageIndex] == RIGHT) )
	{ 
		float fTemp = fCutBoxWidth; fCutBoxWidth = fCutBoxHeight; fCutBoxHeight = fTemp; 
			  fTemp = fPageWidth;	fPageWidth	 = fPageHeight;	  fPageHeight	= fTemp; 
	}

	int nHeadPosition = pLayer->m_HeadPositionArray[nPageIndex];
	if (bRotate)
	{
		switch (nHeadPosition)
		{
		case TOP:		nHeadPosition = LEFT;	break;
		case BOTTOM:	nHeadPosition = RIGHT;	break;
		case LEFT:		nHeadPosition = TOP;	break;
		case RIGHT:		nHeadPosition = BOTTOM;	break;
		}

		float fTemp = fCutBoxWidth; fCutBoxWidth = fCutBoxHeight; fCutBoxHeight = fTemp;
	}

	if (pLayer->m_FrontPageArray[nPageIndex] % 2)	// right page
	{
		switch (nHeadPosition)
		{
		case TOP:		fPageLeft = fSpine;						fPageBottom = fFootTrim;				break;
		case BOTTOM:	fPageLeft = fSideTrim + fOverFold;		fPageBottom = fHeadTrim;				break;
		case LEFT:		fPageLeft = fHeadTrim;					fPageBottom = fSpine;					break;
		case RIGHT:		fPageLeft = fFootTrim;					fPageBottom = fSideTrim + fOverFold;	break;
		}
	}
	else
	{
		switch (nHeadPosition)
		{
		case TOP:		fPageLeft = fSideTrim + fOverFold;		fPageBottom = fFootTrim;				break;
		case BOTTOM:	fPageLeft = fSpine;		 				fPageBottom = fHeadTrim;				break;
		case LEFT:		fPageLeft = fHeadTrim;  				fPageBottom = fSideTrim + fOverFold;	break;
		case RIGHT:		fPageLeft = fFootTrim;  				fPageBottom = fSpine;					break;
		}
	}

	fPageRight = fPageLeft   + fPageWidth;
	fPageTop   = fPageBottom + fPageHeight;
}

void CFoldSheet::SetQuantityPlanned(int nQuantity, BOOL bUpdatePrintSheets)
{
	m_nQuantityPlanned = nQuantity;

	if ( ! bUpdatePrintSheets)
		return;

	for (int i = 0; i < m_LayerList.GetSize(); i++)
	{
		CPrintSheet* pPrintSheet = GetPrintSheet(i);
		while (pPrintSheet)
		{
			pPrintSheet->CalcTotalQuantity();
			pPrintSheet = GetNextPrintSheet(pPrintSheet, i);
		}
	}

	CalcQuantityActual();
}

void CFoldSheet::SetQuantityExtra(int nQuantity, BOOL bUpdatePrintSheets)
{
	m_nQuantityExtra = nQuantity;

	if ( ! bUpdatePrintSheets)
		return;

	for (int i = 0; i < m_LayerList.GetSize(); i++)
	{
		CPrintSheet* pPrintSheet = GetPrintSheet(i);
		while (pPrintSheet)
		{
			pPrintSheet->CalcTotalQuantity();
			pPrintSheet = GetNextPrintSheet(pPrintSheet, i);
		}
	}

	CalcQuantityActual();
}

void CFoldSheet::CalcQuantityActual()
{
	m_nQuantityActual = 0;
	for (int i = 0; i < m_LayerList.GetSize(); i++)
	{
		CPrintSheet* pPrintSheet = GetPrintSheet(i);
		while (pPrintSheet)
		{
			int nNumUp = GetNUpPrintLayout(pPrintSheet, i);
			m_nQuantityActual += (pPrintSheet->m_nQuantityTotal - pPrintSheet->m_nQuantityExtra) * nNumUp;

			pPrintSheet = GetNextPrintSheet(pPrintSheet, i);
		}
	}
}

float CFoldSheet::CalcCoverage(CPrintSheet* pPrintSheet)
{
	CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return 0.0f;
	else
		return pLayout->CalcUtilization(pPrintSheet, this);
}

CString	CFoldSheet::GetPaperName()
{
	if ( ! m_strPaperName.IsEmpty())
		return m_strPaperName;

	CBoundProductPart& rProductPart = GetProductPart();
	return rProductPart.m_strPaper;
}

BOOL CFoldSheet::IsLeftFromSpine(int nPageIndex)
{
	nPageIndex++;
	POSITION pos = m_LayerList.GetHeadPosition();
	while (pos)
	{
		CFoldSheetLayer& rLayer = m_LayerList.GetNext(pos);
		for (int i = 0; i < rLayer.m_FrontPageArray.GetSize(); i++)
		{
			if (rLayer.m_FrontPageArray[i] == nPageIndex)
			{
				if (rLayer.m_FrontSpineArray[i] == LEFT)
					return TRUE;
				else
					return FALSE;
			}
		}
		for (int i = 0; i < rLayer.m_BackPageArray.GetSize(); i++)
		{
			if (rLayer.m_BackPageArray[i] == nPageIndex)
			{
				if (rLayer.m_BackSpineArray[i] == LEFT)
					return TRUE;
				else
					return FALSE;
			}
		}
	}

	return FALSE;
}

BOOL CFoldSheet::HasFoldSheetInside()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	POSITION pos = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem = pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		if ( ! pos)
			break;
		if (rItem.GetFoldSheetPointer(&pDoc->m_Bookblock) == this)	// left part
		{
			CDescriptionItem& rItem2 = pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
			if (rItem2.GetFoldSheetPointer(&pDoc->m_Bookblock) == this)	// next is right part
				return FALSE;
			else
				return TRUE;
		}
	}
	return FALSE;
}

int	CFoldSheet::GetBestRotation90()
{
	switch (m_nFoldSheetRefEdge)
	{
	case LEFT_BOTTOM:	return ROTATE90;
	case LEFT_TOP:		return ROTATE90;
	case RIGHT_TOP:		return ROTATE270;
	case RIGHT_BOTTOM:	return ROTATE270;
	}

	return ROTATE90;
}

BOOL CFoldSheet::IsFoldSchemeClosedOnHead()
{
	return TRUE;
	//if (m_LayerList.GetSize() <= 0)
	//	return FALSE;

	//int nMinPageNum = INT_MAX;
	//int nMinIndex	= -1;
	//CFoldSheetLayer& rLayer = m_LayerList.GetHead();
	//for (int i = 0; i < rLayer.m_FrontPageArray.GetSize(); i++)
	//{
	//	if (rLayer.m_FrontPageArray[i] < nMinPageNum)
	//	{
	//		nMinPageNum = rLayer.m_FrontPageArray[i];
	//		nMinIndex = i;
	//	}
	//}
	//if (nMinIndex < 0)
	//	return FALSE;

	//int nX = nMinIndex % rLayer.m_nPagesX;
	//int nY = nMinIndex / rLayer.m_nPagesX;

	//switch (rLayer.m_FrontPageArray[nMinIndex])
	//{
	//case TOP:	
	//}
}

BOOL CFoldSheet::IsCover()
{
	if (this == NULL)
		return FALSE;

	return GetProductPart().IsCover();
}

CPrintComponentsGroup& CFoldSheet::GetPrintComponentsGroup()
{
	static CPrintComponentsGroup nullGroup;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullGroup;	

	for (int i = 0; i < pDoc->m_printComponentsList.GetSize(); i++)
	{
		CPrintComponentsGroup& rGroup = pDoc->m_printComponentsList[i];
		for (int j = 0; j < rGroup.m_printComponents.GetSize(); j++)
		{
			CPrintComponent component = rGroup.m_printComponents[j];
			if (component.m_nType == CPrintComponent::FoldSheet)
				if (component.m_nFoldSheetIndex == GetIndex())
					return rGroup;
		}
	}

	return nullGroup;	
}

int	CFoldSheet::GetSpineOrientation()
{
	CFoldSheetLayer* pLayer = GetLayer(0);
	if ( ! pLayer)
		return -1;
	if (pLayer->m_HeadPositionArray.GetSize() <= 0)
		return -1;

	if ( (pLayer->m_HeadPositionArray[0] == TOP) || (pLayer->m_HeadPositionArray[0] == BOTTOM) )
		return VERTICAL;
	else
		return HORIZONTAL;
}

int	CFoldSheet::GetPaperGrain()
{
	CBoundProductPart& rProductPart = GetProductPart();
	if (rProductPart.m_nPaperGrain == GRAIN_EITHER)
		return GRAIN_EITHER;

	CFoldSheetLayer* pLayer = GetLayer(0);
	if ( ! pLayer)
		return GRAIN_EITHER;
	if (pLayer->m_HeadPositionArray.GetSize() <= 0)
		return GRAIN_EITHER;

	if ( (pLayer->m_HeadPositionArray[0] == TOP) || (pLayer->m_HeadPositionArray[0] == BOTTOM) )
		return (rProductPart.m_nPaperGrain == GRAIN_VERTICAL) ? VERTICAL   : HORIZONTAL;
	else
		return (rProductPart.m_nPaperGrain == GRAIN_VERTICAL) ? HORIZONTAL : VERTICAL;
}

int	CFoldSheet::SpreadLayer(int nLayerIndex, int nOrientation, int nSpreadAxis, float& fFoldSheetLeftBottomPart, float& fFoldSheetRightTopPart)
{
	fFoldSheetLeftBottomPart = fFoldSheetRightTopPart = 0.0f;

	CBoundProductPart& rProductPart = GetProductPart();
	CFoldSheetLayer*   pLayer		= GetLayer(nLayerIndex);
	if ( ! pLayer)
		return -1;

	BOOL bRotate = FALSE;
	switch (nOrientation)
	{
	case ROTATE90:	
	case ROTATE270: 
	case FLIP90:	
	case FLIP270:	if (nSpreadAxis == VERTICAL)
						nSpreadAxis = HORIZONTAL;
					else
						nSpreadAxis = VERTICAL;
					bRotate = TRUE;
					break;
	default:		break;
	}

	int nNumPagesX = (nSpreadAxis == VERTICAL)	 ? (pLayer->m_nPagesX / 2) : pLayer->m_nPagesX;
	int nNumPagesY = (nSpreadAxis == HORIZONTAL) ? (pLayer->m_nPagesY / 2) : pLayer->m_nPagesY;

	float fLayerWidth = 0.0f, fLayerHeight = 0.0f;
	int	nPageIndex = 0;
	for (unsigned int ny = 0; ny < nNumPagesY; ny++)
	{
		float fRowWidth = 0.0f, fRowHeight = 0.0f;
		for (unsigned int nx = 0; nx < nNumPagesX; nx++)
		{
			float fWidth, fHeight, fDummy;
			GetPageCutBox(pLayer, nPageIndex, rProductPart, fWidth, fHeight, fDummy, fDummy, fDummy, fDummy);
			fRowHeight = max(fRowHeight, fHeight);
			fRowWidth += fWidth;
			nPageIndex++;
		}
		fLayerWidth   = max(fLayerWidth, fRowWidth);
		fLayerHeight += fRowHeight;
	}

	float fCutWidth = 0.0f, fCutHeight = 0.0f;
	GetCutSize(nLayerIndex, fCutWidth, fCutHeight);
	if (bRotate)
	{
		fFoldSheetLeftBottomPart = fLayerHeight;	fFoldSheetRightTopPart = fCutHeight - fLayerHeight;
	}
	else
	{
		fFoldSheetLeftBottomPart = fLayerWidth;		fFoldSheetRightTopPart = fCutWidth  - fLayerWidth;
	}

	return (nSpreadAxis == VERTICAL) ? nNumPagesX - 1 : nNumPagesY - 1;
}


CString CFoldSheet::GetOutFilename(int nSide, CFoldSheet* pFoldSheet, CPlate* pPlate)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return "";

	CPDFTargetProperties& rTargetProps = pDoc->m_Bookblock.m_FoldSheetList.m_outputTargetProps;

	//int nOutFileRelation = GetOutfileRelation(rTargetProps.m_strOutputFilename);
	//switch (nSide)
	//{
	//case BOTHSIDES:	if ( (nOutFileRelation != SheetPerFile) && (nOutFileRelation != JobPerFile) )
	//					return "";
	//				break;
	//case FRONTSIDE: 
	//case BACKSIDE:	if (nOutFileRelation != SidePerFile)
	//					return "";
	//				break;
	//default:		if (nOutFileRelation != PlatePerFile)
	//					return "";
	//				nSide = (pPlate) ? pPlate->GetSheetSide() : -1;
	//				break;
	//}

	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)rTargetProps.m_strOutputFilename, (pPlate) ? pPlate->GetPrintSheet() : NULL, nSide, pPlate, -1, "", -1, -1, pFoldSheet);	
	
	return szDest;
}

CString	CFoldSheet::GetOutFileFullPath(int nSide, CFoldSheet* pFoldSheet, CPlate* pPlate, BOOL bCreateMissingPath)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return "";

	CPDFTargetProperties& rTargetProps = pDoc->m_Bookblock.m_FoldSheetList.m_outputTargetProps;
	if (rTargetProps.m_strTargetName.IsEmpty())
		return _T("");
	
	CString strLocation = rTargetProps.m_strLocation;
	//if (bJDFMarksOutput)
	//{
	//	if (m_outputTargetProps.m_bJDFMarksInFolder && ! m_outputTargetProps.m_strJDFMarksFolder.IsEmpty()) 
	//	{
	//		if (PathIsRelative(m_outputTargetProps.m_strJDFMarksFolder))
	//			strLocation = m_outputTargetProps.m_strLocation + _T("\\") + m_outputTargetProps.m_strJDFMarksFolder;
	//		else
	//			strLocation = m_outputTargetProps.m_strJDFMarksFolder;
	//	}
	//	else
	//		if (m_outputTargetProps.m_bJDFOutputMime)
	//			strLocation = theApp.settings.m_szTempDir;
	//}
	
	CString strOutputPDFFile; 
	if ( (rTargetProps.m_nType == CPDFTargetProperties::TypePDFFolder) )// || (m_outputTargetProps.m_nType == CPDFTargetProperties::TypeJDFFolder) )
	{
		TCHAR szDest[1024];
		CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)strLocation, NULL, nSide, pPlate, -1, "", -1, -1, pFoldSheet);	
		if (bCreateMissingPath)
			if ( ! theApp.DirectoryExist(szDest))
				SHCreateDirectoryEx(NULL, szDest, NULL);
		strOutputPDFFile.Format(_T("%s\\%s"), szDest, GetOutFilename(nSide, pFoldSheet, pPlate));
	}
	else
		strOutputPDFFile.Format(_T("%s\\%s"), theApp.settings.m_szTempDir, GetOutFilename(nSide, pFoldSheet, pPlate));

	return strOutputPDFFile;
}

CString	CFoldSheet::GetOutFileTmpFullPath(int nSide, CFoldSheet* pFoldSheet, CPlate* pPlate)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return ""; 

	CString strTmpOutputPDFFile; 
	strTmpOutputPDFFile.Format(_T("%s\\%s"), theApp.settings.m_szTempDir, GetOutFilename(nSide, pFoldSheet, pPlate));

	return strTmpOutputPDFFile;
}


// helper function for FoldSheetList elements:

template <> void AFXAPI SerializeElements <CFoldSheet> ( CArchive& ar, CFoldSheet* pFoldSheet, INT_PTR nCount )
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CFoldSheet));

	if (ar.IsStoring())
	{
		ar << pFoldSheet->GetID();
		ar << pFoldSheet->m_nFoldSheetNumber;
		ar << pFoldSheet->m_bFoldSheetNumLocked;
		ar << pFoldSheet->m_strNumerationStyle;
		ar << pFoldSheet->m_nNumerationOffset;
		ar << pFoldSheet->m_strFoldSheetTypeName;
		ar << pFoldSheet->m_nPagesLeft;
		ar << pFoldSheet->m_nPagesRight;      
		ar << pFoldSheet->m_nFoldSheetRefEdge;
		ar << pFoldSheet->m_strNotes;
		ar << pFoldSheet->m_fShinglingValueX;
		ar << pFoldSheet->m_fShinglingValueXFoot;
		ar << pFoldSheet->m_fShinglingValueY;
		ar << pFoldSheet->m_bShinglingValueLocked;
		ar << pFoldSheet->m_nProductIndex;
		ar << pFoldSheet->m_nProductPartIndex;
		ar << pFoldSheet->m_strPaperName;
		ar << pFoldSheet->GetQuantityPlanned();
		ar << pFoldSheet->GetQuantityExtra();
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		CString strTemp;
		int		nTemp = 0;
		pFoldSheet->m_LayerList.RemoveAll();  // no append, so target list has to be empty 
		switch (nVersion)
		{
		case 1:
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			pFoldSheet->m_fShinglingValueX		= 0.0f;
			pFoldSheet->m_bShinglingValueLocked = FALSE;
			pFoldSheet->m_strNumerationStyle	= _T("$n");
			pFoldSheet->m_nNumerationOffset		= 0;
			pFoldSheet->m_nProductPartIndex		= 0;
			pFoldSheet->m_strPaperName			= _T("");
			pFoldSheet->SetQuantityPlanned(-1, FALSE);			// -1 means: get numbers from product part after having been loaded (in CheckFoldSheetQuantities())
			pFoldSheet->SetQuantityExtra  (-1, FALSE);
			pFoldSheet->SetQuantityActual (-1);
			break;
		case 2:
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			ar >> pFoldSheet->m_fShinglingValueX;
			ar >> pFoldSheet->m_bShinglingValueLocked;
			pFoldSheet->m_strNumerationStyle = _T("$n");
			pFoldSheet->m_nNumerationOffset	 = 0;
			pFoldSheet->m_nProductPartIndex	 = 0;
			pFoldSheet->m_strPaperName		 = _T("");
			pFoldSheet->SetQuantityPlanned(-1, FALSE);			// -1 means: get numbers from product part after having been loaded (in CheckFoldSheetQuantities())
			pFoldSheet->SetQuantityExtra  (-1, FALSE);
			pFoldSheet->SetQuantityActual (-1);
			break;
		case 3:
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_strNumerationStyle;
			ar >> pFoldSheet->m_nNumerationOffset;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			ar >> pFoldSheet->m_fShinglingValueX;
			ar >> pFoldSheet->m_bShinglingValueLocked;
			pFoldSheet->m_fShinglingValueXFoot = 0.0f;
			pFoldSheet->m_nProductPartIndex    = 0;
			pFoldSheet->m_strPaperName		   = _T("");
			pFoldSheet->SetQuantityPlanned(-1, FALSE);			// -1 means: get numbers from product part after having been loaded (in CheckFoldSheetQuantities())
			pFoldSheet->SetQuantityExtra  (-1, FALSE);
			pFoldSheet->SetQuantityActual (-1);
			break;
		case 4:
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_strNumerationStyle;
			ar >> pFoldSheet->m_nNumerationOffset;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			ar >> pFoldSheet->m_fShinglingValueX;
			ar >> pFoldSheet->m_fShinglingValueXFoot;
			ar >> pFoldSheet->m_bShinglingValueLocked;
			pFoldSheet->m_nProductPartIndex = 0;
			pFoldSheet->m_strPaperName		= _T("");
			pFoldSheet->SetQuantityPlanned(-1, FALSE);			// -1 means: get numbers from product part after having been loaded (in CheckFoldSheetQuantities())
			pFoldSheet->SetQuantityExtra  (-1, FALSE);
			pFoldSheet->SetQuantityActual (-1);
			break;
		case 5:
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_strNumerationStyle;
			ar >> pFoldSheet->m_nNumerationOffset;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			ar >> pFoldSheet->m_fShinglingValueX;
			ar >> pFoldSheet->m_fShinglingValueXFoot;
			ar >> pFoldSheet->m_bShinglingValueLocked;
			ar >> pFoldSheet->m_nProductPartIndex;
			pFoldSheet->m_strPaperName = _T("");
			pFoldSheet->SetQuantityPlanned(-1, FALSE);			// -1 means: get numbers from product part after having been loaded (in CheckFoldSheetQuantities())
			pFoldSheet->SetQuantityExtra  (-1, FALSE);
			pFoldSheet->SetQuantityActual (-1);
			break;
		case 6:
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_strNumerationStyle;
			ar >> pFoldSheet->m_nNumerationOffset;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			ar >> pFoldSheet->m_fShinglingValueX;
			ar >> pFoldSheet->m_fShinglingValueXFoot;
			ar >> pFoldSheet->m_bShinglingValueLocked;
			ar >> pFoldSheet->m_nProductPartIndex;
			ar >> pFoldSheet->m_strPaperName;
			ar >> nTemp; pFoldSheet->SetQuantityPlanned(nTemp);
			ar >> nTemp; pFoldSheet->SetQuantityExtra(nTemp);
			pFoldSheet->SetQuantityActual (-1);					// will be calculated
			break;
		case 7:
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_strNumerationStyle;
			ar >> pFoldSheet->m_nNumerationOffset;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			ar >> pFoldSheet->m_fShinglingValueX;
			ar >> pFoldSheet->m_fShinglingValueXFoot;
			ar >> pFoldSheet->m_fShinglingValueY;
			ar >> pFoldSheet->m_bShinglingValueLocked;
			ar >> pFoldSheet->m_nProductPartIndex;
			ar >> pFoldSheet->m_strPaperName;
			ar >> nTemp; pFoldSheet->SetQuantityPlanned(nTemp);
			ar >> nTemp; pFoldSheet->SetQuantityExtra(nTemp);
			pFoldSheet->SetQuantityActual (-1);					// will be calculated
			break;
		case 8:
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_bFoldSheetNumLocked;
			ar >> pFoldSheet->m_strNumerationStyle;
			ar >> pFoldSheet->m_nNumerationOffset;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			ar >> pFoldSheet->m_fShinglingValueX;
			ar >> pFoldSheet->m_fShinglingValueXFoot;
			ar >> pFoldSheet->m_fShinglingValueY;
			ar >> pFoldSheet->m_bShinglingValueLocked;
			ar >> pFoldSheet->m_nProductPartIndex;
			ar >> pFoldSheet->m_strPaperName;
			ar >> nTemp; pFoldSheet->SetQuantityPlanned(nTemp);
			ar >> nTemp; pFoldSheet->SetQuantityExtra(nTemp);
			pFoldSheet->SetQuantityActual (-1);					// will be calculated
			break;
		case 9:
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_bFoldSheetNumLocked;
			ar >> pFoldSheet->m_strNumerationStyle;
			ar >> pFoldSheet->m_nNumerationOffset;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			ar >> pFoldSheet->m_fShinglingValueX;
			ar >> pFoldSheet->m_fShinglingValueXFoot;
			ar >> pFoldSheet->m_fShinglingValueY;
			ar >> pFoldSheet->m_bShinglingValueLocked;
			ar >> pFoldSheet->m_nProductIndex;
			ar >> pFoldSheet->m_nProductPartIndex;
			ar >> pFoldSheet->m_strPaperName;
			ar >> nTemp; pFoldSheet->SetQuantityPlanned(nTemp);
			ar >> nTemp; pFoldSheet->SetQuantityExtra(nTemp);
			pFoldSheet->SetQuantityActual (-1);					// will be calculated
			break;
		case 10:
			ar >> strTemp; pFoldSheet->SetID(strTemp);
			ar >> pFoldSheet->m_nFoldSheetNumber;
			ar >> pFoldSheet->m_bFoldSheetNumLocked;
			ar >> pFoldSheet->m_strNumerationStyle;
			ar >> pFoldSheet->m_nNumerationOffset;
			ar >> pFoldSheet->m_strFoldSheetTypeName;
			ar >> pFoldSheet->m_nPagesLeft;
			ar >> pFoldSheet->m_nPagesRight;      
			ar >> pFoldSheet->m_nFoldSheetRefEdge;
			ar >> pFoldSheet->m_strNotes;
			ar >> pFoldSheet->m_fShinglingValueX;
			ar >> pFoldSheet->m_fShinglingValueXFoot;
			ar >> pFoldSheet->m_fShinglingValueY;
			ar >> pFoldSheet->m_bShinglingValueLocked;
			ar >> pFoldSheet->m_nProductIndex;
			ar >> pFoldSheet->m_nProductPartIndex;
			ar >> pFoldSheet->m_strPaperName;
			ar >> nTemp; pFoldSheet->SetQuantityPlanned(nTemp);
			ar >> nTemp; pFoldSheet->SetQuantityExtra(nTemp);
			pFoldSheet->SetQuantityActual (-1);					// will be calculated
			break;
		}
	}

	pFoldSheet->m_LayerList.Serialize(ar);
}


void AFXAPI DestructElements(CFoldSheet* pFoldSheet, INT_PTR nCount)
{
	for (int i = 0; i < nCount; i++)
	{
		pFoldSheet->m_strNumerationStyle.Empty();
		pFoldSheet->m_strFoldSheetTypeName.Empty();
		pFoldSheet->m_strNotes.Empty();

		pFoldSheet->m_LayerList.RemoveAll();
		pFoldSheet++;
	}
}

void CFoldSheet::GetPageSequence(CArray <short*, short*>& rPageSequence, int nPart)
{
	CArray <short*, short*> LeftPageSequence, RightPageSequence;

	POSITION layerPos = m_LayerList.GetHeadPosition();
	rPageSequence.RemoveAll();
	while (layerPos)
	{
		CFoldSheetLayer& rLayer = m_LayerList.GetNext(layerPos);		
		for (int i = 0; i < rLayer.m_FrontPageArray.GetSize(); i++)
			if (rLayer.m_FrontPageArray[i] > 0)
				if (rLayer.m_FrontSpineArray[i] == LEFT)
					LeftPageSequence.Add(&rLayer.m_FrontPageArray[i]);
				else
					RightPageSequence.Add(&rLayer.m_FrontPageArray[i]);

		for (int i = 0; i < rLayer.m_BackPageArray.GetSize(); i++)
			if (rLayer.m_BackPageArray[i] > 0)
				if (rLayer.m_BackSpineArray[i] == LEFT)
					LeftPageSequence.Add(&rLayer.m_BackPageArray[i]);
				else
					RightPageSequence.Add(&rLayer.m_BackPageArray[i]);
	}

	qsort((void*)LeftPageSequence.GetData(),  LeftPageSequence.GetSize(),  sizeof(short*), CFoldSheet::ComparePageSeqIndex);
	qsort((void*)RightPageSequence.GetData(), RightPageSequence.GetSize(), sizeof(short*), CFoldSheet::ComparePageSeqIndex);

	switch (nPart)
	{
	case -1: rPageSequence.Append(LeftPageSequence);
			 rPageSequence.Append(RightPageSequence);
			 break;

	case CDescriptionItem::FoldSheetLeftPart:
			 rPageSequence.Append(LeftPageSequence);
			 break;

	case CDescriptionItem::FoldSheetRightPart:
			 rPageSequence.Append(RightPageSequence);
			 break;
	}
}

///// remove multiple ups from page sequence
void CFoldSheet::ReducePageSequence(CArray <short*, short*>& rPageSequence)
{
	for (int i = 1; i < rPageSequence.GetSize(); i++)
	{
		if (*rPageSequence[i] == *rPageSequence[i - 1])
		{
			rPageSequence.RemoveAt(i);
			i--;
		}
	}
}


int CFoldSheet::ComparePageSeqIndex(const void *p1, const void *p2)
{
	short pp1 = **((short**)p1);
	short pp2 = **((short**)p2);

	if (pp1 < pp2)
		return -1;
	else
		if (pp1 > pp2)
			return 1;
		else
			return 0;
}



/////////////////////////////////////////////////////////////////////////////
// CFoldSheetLayer

IMPLEMENT_SERIAL(CFoldSheetLayer, CObject, VERSIONABLE_SCHEMA | 3)

CFoldSheetLayer::CFoldSheetLayer() 
{
	m_nPagesX = 0;
	m_nPagesY = 0;
	m_bPagesLocked = TRUE;;
}

// operator overload for CFoldSheetLayer - needed by CList functions:

const CFoldSheetLayer& CFoldSheetLayer::operator=(const CFoldSheetLayer& rFoldSheetLayer)
{
	Copy(rFoldSheetLayer);

	return *this;
}

// copy constructor for CFoldSheetLayer - needed by CList functions:

CFoldSheetLayer::CFoldSheetLayer(const CFoldSheetLayer& rFoldSheetLayer) 
{
	Copy(rFoldSheetLayer);
}

void CFoldSheetLayer::Copy(const CFoldSheetLayer& rFoldSheetLayer) 
{
	m_nPagesX = rFoldSheetLayer.m_nPagesX;
	m_nPagesY = rFoldSheetLayer.m_nPagesY;

	m_FrontPageArray.RemoveAll(); // no append, so target list has to be empty 
	m_BackPageArray.RemoveAll();
	m_HeadPositionArray.RemoveAll(); 
	m_FrontSpineArray.RemoveAll();
	m_BackSpineArray.RemoveAll();

	// Perform element wise copy because CArray::Copy() does not work since VC 4.2
	// - there is a bug in MFC function CArray::CopyElements(): *dst++ = *src; (file: MFC\INCLUDE\AFXTEMPL.H)
	for (int i = 0; i < rFoldSheetLayer.m_FrontPageArray.GetSize(); i++)
		m_FrontPageArray.Add(rFoldSheetLayer.m_FrontPageArray[i]);
	for (int i = 0; i < rFoldSheetLayer.m_BackPageArray.GetSize(); i++)
		m_BackPageArray.Add(rFoldSheetLayer.m_BackPageArray[i]);
	for (int i = 0; i < rFoldSheetLayer.m_HeadPositionArray.GetSize(); i++)
		m_HeadPositionArray.Add(rFoldSheetLayer.m_HeadPositionArray[i]);
	for (int i = 0; i < rFoldSheetLayer.m_FrontSpineArray.GetSize(); i++)
		m_FrontSpineArray.Add(rFoldSheetLayer.m_FrontSpineArray[i]);
	for (int i = 0; i < rFoldSheetLayer.m_BackSpineArray.GetSize(); i++)
		m_BackSpineArray.Add(rFoldSheetLayer.m_BackSpineArray[i]);
}

// Get first printsheet containing this foldsheetlayer
CPrintSheet* CFoldSheetLayer::GetFirstPrintSheet(int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;	

	// 04/10/2014: removed this tricky mechanism. Makes trouble when foldsheets are being removed, because even then printsheetlocations has to be updated - which is not be done yet.
	// Update of printsheetlocations needs to be verified if it is doen in all cases. So far we do not use this trick.

	//int nPrintSheetIndex = -1;
	//if (m_FrontPageArray.GetSize() > 0)		// first try to get printsheet via page template printsheet locations
	//{
	//	int		 nIndex	  = m_FrontPageArray[0] - 1;
	//	POSITION pos	  = pDoc->m_PageTemplateList.FindIndex(nIndex, nProductPartIndex);
	//	if (pos)
	//	{
	//		CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetAt(pos);
	//		if (rTemplate.m_PrintSheetLocations.GetSize() > 0)
	//		{
	//			POSITION pos = pDoc->m_PrintSheetList.FindIndex(rTemplate.m_PrintSheetLocations[0].m_nPrintSheetIndex);
	//			if (pos)
	//				return &pDoc->m_PrintSheetList.GetAt(pos);
	//		}
	//	}
	//}

	// if no success, try this way
	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		for (int i = 0; i < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
			if (rPrintSheet.GetFoldSheetLayer(i) == this)
				return &rPrintSheet;
	}
	return NULL;
}

// Get next printsheet containing this foldsheetlayer
CPrintSheet* CFoldSheetLayer::GetNextPrintSheet(CPrintSheet* pPrintSheet, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;	

	int nPrintSheetIndex = -1;
	if (m_FrontPageArray.GetSize() > 0)		// first try to get printsheet via page template printsheet locations
	{
		POSITION pos = pDoc->m_PageTemplateList.FindIndex(m_FrontPageArray[0] - 1, nProductPartIndex);
		if (pos)
		{
			CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetAt(pos);
			int nIndex = 0;
			while (nIndex < rTemplate.m_PrintSheetLocations.GetSize())
			{
				POSITION pos = pDoc->m_PrintSheetList.FindIndex(rTemplate.m_PrintSheetLocations[nIndex].m_nPrintSheetIndex);
				if (pos)
				{
					if (pPrintSheet == &pDoc->m_PrintSheetList.GetAt(pos))
					{
						nIndex++;
						while (nIndex < rTemplate.m_PrintSheetLocations.GetSize())		// goto next location different to actual printsheet
						{
							pos = pDoc->m_PrintSheetList.FindIndex(rTemplate.m_PrintSheetLocations[nIndex].m_nPrintSheetIndex);
							if (pos)
								if (pPrintSheet != &pDoc->m_PrintSheetList.GetAt(pos))
									return &pDoc->m_PrintSheetList.GetAt(pos);

							nIndex++;
						}
					}
				}
				nIndex++;
			}
		}
	}

	// if no success, try this way
	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	if (pPrintSheet)	// find startpos
	{
		while (pos)
		{
			CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
			if (&rPrintSheet == pPrintSheet)
				break;
		}
	}

	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		for (int i = 0; i < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
			if (rPrintSheet.GetFoldSheetLayer(i) == this)
				return &rPrintSheet;
	}
	return NULL;
}

CString	CFoldSheetLayer::GetWebName(CFoldSheet* pFoldSheet)
{
	if (pFoldSheet->m_LayerList.GetCount() <= 1)	// no multi web
		return "";

	TCHAR cWeb = 'A';
	POSITION pos = pFoldSheet->m_LayerList.GetHeadPosition();
	while (pos)
	{
		CFoldSheetLayer& rLayer = pFoldSheet->m_LayerList.GetNext(pos);
		if (&rLayer == this)
			return CString(cWeb);
		cWeb++;
	}
	return "";
}

// get num pages by taking nUPs into account (for example: 4x2 layout 2 up -> has 16 physical pages but 8 real different pages)
int	CFoldSheetLayer::GetRealNumPages()
{
	CArray <short*, short*> PageSequence;

	for (int i = 0; i < m_FrontPageArray.GetSize(); i++)
		if (m_FrontPageArray[i] > 0)
			PageSequence.Add(&m_FrontPageArray[i]);

	for (int i = 0; i < m_BackPageArray.GetSize(); i++)
		if (m_BackPageArray[i] > 0)
			PageSequence.Add(&m_BackPageArray[i]);

	if ( ! PageSequence.GetSize())
		return 0;

	qsort((void*)PageSequence.GetData(), PageSequence.GetSize(), sizeof(short*), CFoldSheet::ComparePageSeqIndex);

	int nNumPages = 1;
	for (int i = 1; i < PageSequence.GetSize(); i++)
	{
		if (*(PageSequence[i]) != *(PageSequence[i - 1]))
			nNumPages++;

	}
	return nNumPages;
}


// helper function for serializing FoldSheetLayerList elements:

template <> void AFXAPI SerializeElements <CFoldSheetLayer> ( CArchive& ar, CFoldSheetLayer* pFoldSheetLayer, INT_PTR nCount )
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CFoldSheetLayer));

	if (ar.IsStoring())
	{
		ar << pFoldSheetLayer->m_nPagesX;
		ar << pFoldSheetLayer->m_nPagesY;      
		pFoldSheetLayer->m_FrontPageArray.Serialize(ar);
		pFoldSheetLayer->m_BackPageArray.Serialize(ar);
		pFoldSheetLayer->m_HeadPositionArray.Serialize(ar);
		pFoldSheetLayer->m_FrontSpineArray.Serialize(ar);
		pFoldSheetLayer->m_BackSpineArray.Serialize(ar);
	}
	else
	{
		UINT					nVersion = ar.GetObjectSchema();
		CArray <short*, short*> PageSequence;
		int						i, nMidPage;

		pFoldSheetLayer->m_FrontPageArray.RemoveAll(); // no append, so target list has to be empty
		pFoldSheetLayer->m_BackPageArray.RemoveAll();
		pFoldSheetLayer->m_HeadPositionArray.RemoveAll();
		pFoldSheetLayer->m_FrontSpineArray.RemoveAll();
		pFoldSheetLayer->m_BackSpineArray.RemoveAll();
		switch (nVersion)
		{
		case 1:
			ar >> pFoldSheetLayer->m_nPagesX;
			ar >> pFoldSheetLayer->m_nPagesY;      

			pFoldSheetLayer->m_FrontPageArray.Serialize(ar);
			pFoldSheetLayer->m_BackPageArray.Serialize(ar);
			pFoldSheetLayer->m_HeadPositionArray.Serialize(ar);

			for (i = 0; i < pFoldSheetLayer->m_FrontPageArray.GetSize(); i++)
				PageSequence.Add(&pFoldSheetLayer->m_FrontPageArray[i]);
			qsort((void*)PageSequence.GetData(), PageSequence.GetSize(), sizeof(short*), CFoldSheet::ComparePageSeqIndex);
			nMidPage = *PageSequence[PageSequence.GetSize()/2];
			for (i = 0; i < pFoldSheetLayer->m_FrontPageArray.GetSize(); i++)
			{
				if (pFoldSheetLayer->m_FrontPageArray[i] < nMidPage)
					pFoldSheetLayer->m_FrontSpineArray.Add(LEFT);
				else
					pFoldSheetLayer->m_FrontSpineArray.Add(RIGHT);
			}
			for (i = 0; i < pFoldSheetLayer->m_BackPageArray.GetSize(); i++)
				PageSequence.Add(&pFoldSheetLayer->m_BackPageArray[i]);
			qsort((void*)PageSequence.GetData(), PageSequence.GetSize(), sizeof(short*), CFoldSheet::ComparePageSeqIndex);
			for (i = 0; i < pFoldSheetLayer->m_BackPageArray.GetSize(); i++)
			{
				if (pFoldSheetLayer->m_BackPageArray[i] < nMidPage)
					pFoldSheetLayer->m_BackSpineArray.Add(LEFT);
				else
					pFoldSheetLayer->m_BackSpineArray.Add(RIGHT);
			}
			break;
		case 2:
			ar >> pFoldSheetLayer->m_nPagesX;
			ar >> pFoldSheetLayer->m_nPagesY;      

			pFoldSheetLayer->m_FrontPageArray.Serialize(ar);
			pFoldSheetLayer->m_BackPageArray.Serialize(ar);
			pFoldSheetLayer->m_HeadPositionArray.Serialize(ar);
			pFoldSheetLayer->m_FrontSpineArray.Serialize(ar);
			for (i = 0; i < pFoldSheetLayer->m_FrontSpineArray.GetSize(); i++)
				pFoldSheetLayer->m_BackSpineArray.Add(pFoldSheetLayer->m_FrontSpineArray[i]);
			break;
		case 3:
			ar >> pFoldSheetLayer->m_nPagesX;
			ar >> pFoldSheetLayer->m_nPagesY;      

			pFoldSheetLayer->m_FrontPageArray.Serialize(ar);
			pFoldSheetLayer->m_BackPageArray.Serialize(ar);
			pFoldSheetLayer->m_HeadPositionArray.Serialize(ar);
			pFoldSheetLayer->m_FrontSpineArray.Serialize(ar);
			pFoldSheetLayer->m_BackSpineArray.Serialize(ar);
			break;
		}
	}
}

void AFXAPI DestructElements(CFoldSheetLayer* pFoldSheetLayer, INT_PTR nCount)
{
	ASSERT(nCount == 1); // in CList context nCount has to be 1

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pFoldSheetLayer->m_FrontPageArray.RemoveAll();
	pFoldSheetLayer->m_BackPageArray.RemoveAll();
	pFoldSheetLayer->m_HeadPositionArray.RemoveAll();
	pFoldSheetLayer->m_FrontSpineArray.RemoveAll();
	pFoldSheetLayer->m_BackSpineArray.RemoveAll();
}




/////////////////////////////////////////////////////////////////////////////
// COldDescriptionItem

IMPLEMENT_SERIAL(COldDescriptionItem, CObject, VERSIONABLE_SCHEMA | 1)

COldDescriptionItem::COldDescriptionItem()
{
}

// operator overload for COldDescriptionItem - needed by CList functions:

const COldDescriptionItem& COldDescriptionItem::operator=(const COldDescriptionItem& rDescriptionItem)
{
	m_nFoldSheetIndex = rDescriptionItem.m_nFoldSheetIndex;
	m_nFoldSheetPart  = rDescriptionItem.m_nFoldSheetPart;	

	return *this;
}

// copy constructor for COldDescriptionItem - needed by CList functions:

COldDescriptionItem::COldDescriptionItem(const COldDescriptionItem& rDescriptionItem) 
{
	Copy(rDescriptionItem);
}

void COldDescriptionItem::Copy(const COldDescriptionItem& rDescriptionItem) 
{
	m_nFoldSheetIndex = rDescriptionItem.m_nFoldSheetIndex;
	m_nFoldSheetPart  = rDescriptionItem.m_nFoldSheetPart;
}

CFoldSheet& COldDescriptionItem::GetFoldSheet(CBookblock* pBookblock) 
{
	ASSERT(pBookblock);
	POSITION pos = pBookblock->m_FoldSheetList.FindIndex(m_nFoldSheetIndex);
	ASSERT(pos);
	return pBookblock->m_FoldSheetList.GetAt(pos);
}

CFoldSheet* COldDescriptionItem::GetFoldSheetPointer(CBookblock* pBookblock) 
{
	ASSERT(pBookblock);
	POSITION pos = pBookblock->m_FoldSheetList.FindIndex(m_nFoldSheetIndex);
	ASSERT(pos);
	return &pBookblock->m_FoldSheetList.GetAt(pos);
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CDescriptionItem

IMPLEMENT_SERIAL(CDescriptionItem, CObject, VERSIONABLE_SCHEMA | 1)

CDescriptionItem::CDescriptionItem()
{
	m_nPart	   = -1;
	m_nSection = -1;
}

// operator overload for CDescriptionItem - needed by CList functions:

const CDescriptionItem& CDescriptionItem::operator=(const CDescriptionItem& rDescriptionItem)
{
	m_nFoldSheetIndex = rDescriptionItem.m_nFoldSheetIndex;
	m_nFoldSheetPart  = rDescriptionItem.m_nFoldSheetPart;	
	m_nPart			  = rDescriptionItem.m_nPart;
	m_nSection		  = rDescriptionItem.m_nSection;

	return *this;
}

// copy constructor for CDescriptionItem - needed by CList functions:

CDescriptionItem::CDescriptionItem(const CDescriptionItem& rDescriptionItem) 
{
	Copy(rDescriptionItem);
}


void CDescriptionItem::Copy(const CDescriptionItem& rDescriptionItem) 
{
	m_nFoldSheetIndex = rDescriptionItem.m_nFoldSheetIndex;
	m_nFoldSheetPart  = rDescriptionItem.m_nFoldSheetPart;
	m_nPart			  = rDescriptionItem.m_nPart;
	m_nSection		  = rDescriptionItem.m_nSection;
}


CFoldSheet& CDescriptionItem::GetFoldSheet(CBookblock* pBookblock) 
{
	ASSERT(pBookblock);
	POSITION pos = pBookblock->m_FoldSheetList.FindIndex(m_nFoldSheetIndex);
	ASSERT(pos);
	return pBookblock->m_FoldSheetList.GetAt(pos);
}


CFoldSheet* CDescriptionItem::GetFoldSheetPointer(CBookblock* pBookblock) 
{
	ASSERT(pBookblock);
	POSITION pos = pBookblock->m_FoldSheetList.FindIndex(m_nFoldSheetIndex);
	ASSERT(pos);
	return &pBookblock->m_FoldSheetList.GetAt(pos);
}


template <> void AFXAPI SerializeElements <CDescriptionItem> (CArchive& ar, CDescriptionItem* pDescriptionItem, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CDescriptionItem));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pDescriptionItem->m_nFoldSheetIndex;
			ar << pDescriptionItem->m_nFoldSheetPart;
			ar << pDescriptionItem->m_nPart;
			ar << pDescriptionItem->m_nSection;
		}
		else
		{
			switch (nVersion)
			{
			case 1: ar >> pDescriptionItem->m_nFoldSheetIndex;
					ar >> pDescriptionItem->m_nFoldSheetPart;
					ar >> pDescriptionItem->m_nPart;
					ar >> pDescriptionItem->m_nSection;
					break; 
			}
		}
		pDescriptionItem++;
	}
}

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CFoldSheetList

CFoldSheetList::CFoldSheetList()
{
}

void CFoldSheetList::LoadFromDatabase()
{
	WIN32_FIND_DATA findData;
	BOOL			bFilesExist = FALSE;	

	CString strSchemeFolder = theApp.settings.m_szSchemeDir;
	HANDLE  hfileSearch = FindFirstFile(strSchemeFolder + _T("\\*.isc"), &findData);

	RemoveAll();
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		CString	strBuffer	= findData.cFileName;
		bFilesExist			= TRUE;
		do 
		{
			strBuffer = findData.cFileName;
			if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
			{
				if ( ! (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
				{
					CString strExt = strBuffer.Right(4);
					strExt.MakeLower();
					if (strExt == _T(".isc"))
					{
						CFoldSheet foldSheet;
						foldSheet.CreateFromISC(strSchemeFolder + _T("\\") + strBuffer);
						AddTail(foldSheet);
					}
				}
			}
		}
		while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);
}

CFoldSheet* CFoldSheetList::CGGetSimilarFoldSheet(CFoldSheet& rFoldSheet, int nLimitIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	int nFoldSheetIndex = pDoc->m_Bookblock.GetFoldSheetIndex(&rFoldSheet);
	if (nFoldSheetIndex <= pDoc->m_Bookblock.m_FoldSheetList.GetCount()/2)
	{
		POSITION pos = GetTailPosition();
		while (pos)
		{
			CFoldSheet& rFoldSheetRef = GetPrev(pos);
			int nFoldSheetIndexRef = pDoc->m_Bookblock.GetFoldSheetIndex(&rFoldSheetRef);
			if (nFoldSheetIndexRef < nLimitIndex)
				if (CGFoldSheetsSimilar(rFoldSheetRef, rFoldSheet))
					return &rFoldSheetRef;
		}
	}
	else
	{
		POSITION pos = GetHeadPosition();
		while (pos)
		{
			CFoldSheet& rFoldSheetRef = GetNext(pos);
			int nFoldSheetIndexRef = pDoc->m_Bookblock.GetFoldSheetIndex(&rFoldSheetRef);
			if (nFoldSheetIndexRef < nLimitIndex)
				if (CGFoldSheetsSimilar(rFoldSheetRef, rFoldSheet))
					return &rFoldSheetRef;
		}
	}

	return NULL;
}

BOOL CFoldSheetList::CGFoldSheetsSimilar(CFoldSheet& rFoldSheetRef, CFoldSheet& rFoldSheet)
{
	if (rFoldSheetRef.m_strFoldSheetTypeName != rFoldSheet.m_strFoldSheetTypeName)
		return FALSE;

	CString strLeftPages  = rFoldSheetRef.GetPageRange(LEFT);
	CString strRightPages = rFoldSheet.GetPageRange(RIGHT);
	if (strLeftPages != strRightPages)
		return FALSE;

	strLeftPages  = rFoldSheet.GetPageRange(LEFT);
	strRightPages = rFoldSheetRef.GetPageRange(RIGHT);
	if (strLeftPages != strRightPages)
		return FALSE;

	return TRUE;
}

void CFoldSheetList::UpdatePaperNames()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFoldSheet&   rFoldSheet   = GetNext(pos);
		CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();
		if (rFoldSheet.m_strPaperName.IsEmpty())
			rFoldSheet.m_strPaperName = rProductPart.m_strPaper;
	}
}

void CFoldSheetList::TakeOverProductPartQuantityPlanned(int nProductPartIndex, int nProductPartAmount)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = GetNext(pos);
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
//		if (rFoldSheet.m_nProductPartIndex == nProductPartIndex)
		{
			//if (rFoldSheet.GetQuantityPlanned() <= 0)
				rFoldSheet.SetQuantityPlanned(nProductPartAmount);
		}
	}
}

void CFoldSheetList::TakeOverProductPartQuantityExtra(int nProductPartIndex, int nProductPartExtra)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = GetNext(pos);
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
//		if (rFoldSheet.m_nProductPartIndex == nProductPartIndex)
		{
			//if (rFoldSheet.GetQuantityExtra() <= 0)
				rFoldSheet.SetQuantityExtra(nProductPartExtra);
		}
	}
}

void CFoldSheetList::TakeOverProductPartPaperName(int nProductPartIndex, CString strPaperName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = GetNext(pos);
		if (rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
//		if (rFoldSheet.m_nProductPartIndex == nProductPartIndex)
		{
			//if (rFoldSheet.m_strPaperName.IsEmpty())
				rFoldSheet.m_strPaperName = strPaperName;
		}
	}
}

int	CFoldSheetList::GetOutfileRelation(const CString& strOutputFilename)
{
	if (strOutputFilename.Find("$1") >= 0)				// $1 = foldsheet number
		return FoldSheetPerFile;
	else
		return JobPerFile;
}

BOOL CFoldSheetList::OutputSheetsAuto()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	theApp.UndoSave();	// save doc because printsheet list will be used (modified) in order to output foldsheets

	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();		// make current printsheet-list empty
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		rLayout.RemovePrintSheetInstance(INT_MAX, FALSE);
	}
	pDoc->m_Bookblock.Reorganize(NULL, NULL);
	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();

	int			 nError			= 0;
	int			 nSheetsOut		= 0;
	BOOL		 bJobCancelled	= FALSE;
	BOOL		 bOverwriteAll  = TRUE;		

	if (GetOutfileRelation(m_outputTargetProps.m_strOutputFilename) == JobPerFile)	
	{
		CPDFOutput pdfOutput(m_outputTargetProps, _T(""));
		bJobCancelled = pdfOutput.ProcessFoldSheetsJobInOne(this, bOverwriteAll, TRUE);	// TRUE = AutoCmd
		nError = pdfOutput.m_nError;
		if (pdfOutput.m_nPlatesProcessed)
			nSheetsOut++;
	}
	else
	{
		POSITION pos = GetHeadPosition();
		while (pos)
		{
			CFoldSheet& rFoldSheet = GetNext(pos);

			CPDFOutput pdfOutput(m_outputTargetProps, _T(""));
			bJobCancelled = pdfOutput.ProcessFoldSheetAllInOne(&rFoldSheet, bOverwriteAll, TRUE);	// TRUE = AutoCmd
			nError = pdfOutput.m_nError;
			if (pdfOutput.m_nPlatesProcessed)
				nSheetsOut++;

			if (bJobCancelled == 2)
				break;
		}
	}

	theApp.UndoRestore();

	if (bJobCancelled)
		nError = CImpManApp::ProcessCancelled;

	switch (nError)
	{
	case CImpManApp::PDFOutputError:	
		return nError;	

	case CImpManApp::ProcessCancelled:	
		{
			CString strMsg; 
			strMsg.LoadString(IDS_MSG_JOB_CANCELLED);
			theApp.LogEntry(_T("   ") + strMsg + _T("\r\n"));
			return nError;	
		}

	default:							
		return nSheetsOut; 
	}
}
