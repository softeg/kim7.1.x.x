#if !defined(AFX_MARKPIVOTCONTROL_H__B9009F83_04BC_11D5_87F7_0040F68C1EF7__INCLUDED_)
#define AFX_MARKPIVOTCONTROL_H__B9009F83_04BC_11D5_87F7_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MarkPivotControl.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Fenster CMarkPivotControl 

class CMarkPivotControl : public CStatic
{
// Konstruktion
public:
	CMarkPivotControl();

// Attribute
public:
	class CDlgMarkSet*	m_pParent;
	CBitmap				m_bmMarkPivot1;
	CBitmap				m_bmMarkPivot2;
	CBitmap				m_bmMarkPivot3;
	CBitmap				m_bmMarkPivot4;
	CBitmap				m_bmMarkPivot5;
	CBitmap				m_bmMarkPivot6;
	CBitmap				m_bmMarkPivot7;
	CBitmap				m_bmMarkPivot8;
	CBitmap				m_bmMarkPivot9;
	BOOL				m_bLockCheckButton;
	int					m_nCurNX;
	int					m_nCurNY;
	int					m_nActiveCell;

friend class CDlgMarkSet;


// Operationen
public:
	int	 GetCurrentCell();
	void ChooseBitmap(int nCell);
	void CheckButton(int nCell);
	void Update();

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CMarkPivotControl)
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~CMarkPivotControl();

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CMarkPivotControl)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_MARKPIVOTCONTROL_H__B9009F83_04BC_11D5_87F7_0040F68C1EF7__INCLUDED_
