// DlgProductionFoldingParams.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionFoldingParams.h"
#include "DlgProductionManager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "DlgImpositionScheme.h"
#include "GraphicComponent.h"



// CDlgProductionFoldingParams-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionFoldingParams, CDialog)

CDlgProductionFoldingParams::CDlgProductionFoldingParams(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionFoldingParams::IDD, pParent)
{
	m_pSchemeImageList  = new CImageList();
	m_bShinglingActive = FALSE;
	m_bLocked = FALSE;
	m_ptInitialPos = CPoint(0,0);
	m_strMarkset = _T("");
}

CDlgProductionFoldingParams::~CDlgProductionFoldingParams()
{
	delete m_pSchemeImageList;
}

int CDlgProductionFoldingParams::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		SetParent(m_pParentWnd);
		ModifyStyle(WS_OVERLAPPEDWINDOW | WS_POPUPWINDOW, WS_CHILD);
		ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);
	}

	return 0;
}

BOOL CDlgProductionFoldingParams::Create(UINT nIDTemplate, CWnd* pParentWnd)
{
	m_pParentWnd = pParentWnd;
	return CDialog::Create(nIDTemplate, pParentWnd);
}

BOOL CDlgProductionFoldingParams::OnEraseBkgnd(CDC* pDC)
{
	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		return CDialog::OnEraseBkgnd(pDC);

	CRect rcFrame;
	GetClientRect(rcFrame);

	CGraphicComponent gc;
	gc.DrawPopupWindowBackground(pDC, rcFrame);

	return TRUE;
}

void CDlgProductionFoldingParams::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
		{
			if (m_hWnd)
			{
				if ( ! m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
				{
					m_bLocked = TRUE;
					EndModalLoop(IDCANCEL);
					DestroyWindow();
				}
			}
		}
	}
	else
		m_bLocked = FALSE;
}

void CDlgProductionFoldingParams::OnBnClickedOk()
{
	SaveData();
	EndModalLoop(IDOK);
	m_bLocked = TRUE;
	DestroyWindow();
}

CProductionProfile& CDlgProductionFoldingParams::GetProductionProfile()
{
	static CProductionProfile nullProductionProfile;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile;
	}
	//else
	//	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CProfileSummaryWindow)))
	//	{
	//		CProfileSummaryWindow*	pParent	= (CProfileSummaryWindow*)m_pParentWnd;
	//		return pParent->GetProductionProfile();
	//	}

	return nullProductionProfile;
}

void CDlgProductionFoldingParams::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PD_SCHEMELIST, m_schemeList);
	DDX_Control(pDX, IDC_SHINGLING_STYLE_COMBO, m_shinglingStyleCombo);
	DDX_CBIndex(pDX, IDC_SHINGLING_STYLE_COMBO, m_nShinglingStyle);
	DDX_Check(pDX, IDC_SHINGLING_CROPMARKS, m_bShinglingCropMarks);
	DDX_Text(pDX, IDC_PD_PRESSMARKSET, m_strMarkset);
}


BEGIN_MESSAGE_MAP(CDlgProductionFoldingParams, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_FOLDINGPARAMS_ADDSCHEME, OnBnClickedFoldingparamsAddscheme)
	ON_BN_CLICKED(IDC_FOLDINGPARAMS_DELETESCHEME, OnBnClickedFoldingparamsDeletescheme)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_PD_SCHEMELIST, OnNMCustomdrawPdSchemelist)
	ON_CBN_SELCHANGE(IDC_SHINGLING_STYLE_COMBO, OnCbnSelchangeShinglingStyleCombo)
	ON_BN_CLICKED(IDC_PD_BROWSE_MARKSET, OnBnClickedPdBrowseMarkset)
	ON_BN_CLICKED(IDC_PD_REMOVE_MARKSET, OnBnClickedFpRemoveMarkset)
END_MESSAGE_MAP()



void CDlgProductionFoldingParams::OnNMCustomdrawPdSchemelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW	 pNMCD	 = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			CString strItem;
			ListView_GetItemText(hWndListCtrl, pNMCD->dwItemSpec, 0, strItem.GetBuffer(MAX_TEXT), MAX_TEXT);
			strItem.ReleaseBuffer();

			CRect rcLabel;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			if (rcLabel.IsRectEmpty())
				break;

			CFoldingProcess& rFoldingParams	= GetProductionProfile().m_postpress.m_folding;
			POSITION		 pos			= rFoldingParams.m_foldSchemeList.FindIndex(lvitem.iItem);
			if (pos)
				rFoldingParams.m_foldSchemeList.GetAt(pos).DrawThumbnail(pDC, rcItem);

			COLORREF crOldTextColor = pDC->SetTextColor(BLACK);//DARKBLUE);

			pDC->DrawText(strItem, -1, rcLabel, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);

			pDC->SetTextColor(crOldTextColor);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}


// CDlgProductionFoldingParams-Meldungshandler

BOOL CDlgProductionFoldingParams::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_ptInitialPos != CPoint(0,0))
		SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);

	CBitmap bitmap;
	if ( ! m_pSchemeImageList->m_hImageList)
	{
		m_pSchemeImageList->Create(32, 32, ILC_COLOR8, 1, 0);	// List not to grow
		bitmap.LoadBitmap(IDB_BKSCHEMELIST);
		m_pSchemeImageList->Add(&bitmap, BLACK);
		m_schemeList.SetImageList(m_pSchemeImageList, LVSIL_SMALL);bitmap.DeleteObject();
	}

	InitData();

	InitShinglingStyle();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProductionFoldingParams::InitData()
{
	CFoldingProcess& rFoldingParams	= GetProductionProfile().m_postpress.m_folding;

	InitSchemeList(rFoldingParams.m_foldSchemeList);

	m_bShinglingActive	  = rFoldingParams.m_bShinglingActive;
	m_nShinglingMethod	  = rFoldingParams.m_nShinglingMethod;
	m_nShinglingDirection = rFoldingParams.m_nShinglingDirection;
	m_bShinglingCropMarks = rFoldingParams.m_bShinglingCropMarks;
	m_strMarkset		  = rFoldingParams.m_strMarkset;

	UpdateData(FALSE);
}

void CDlgProductionFoldingParams::SaveData()
{
	UpdateData(TRUE);

	CFoldingProcess& rFoldingParams	= GetProductionProfile().m_postpress.m_folding;

	rFoldingParams.m_bShinglingActive		= m_bShinglingActive;
	rFoldingParams.m_nShinglingMethod		= m_nShinglingMethod;
	rFoldingParams.m_nShinglingDirection	= m_nShinglingDirection;
	rFoldingParams.m_bShinglingCropMarks	= m_bShinglingCropMarks;
	rFoldingParams.m_strMarkset				= m_strMarkset;
}

void CDlgProductionFoldingParams::InitSchemeList(CFoldSheetList& rSchemeList)
{
	m_schemeList.DeleteAllItems();

	int  nSel	= -1;
	BOOL bFound = FALSE;
	POSITION pos = rSchemeList.GetHeadPosition();
	for (int i = 0; i < rSchemeList.GetSize(); i++)
	{
		CFoldSheet& rFoldSheet = rSchemeList.GetNext(pos);

		LV_ITEM item;
		item.mask		= LVIF_TEXT | LVIF_IMAGE | LVIF_STATE | LVIF_PARAM;
		item.cchTextMax = MAX_TEXT;
		item.iItem		= i; 
		item.iSubItem	= 0;
		item.pszText	= rFoldSheet.m_strFoldSheetTypeName.GetBuffer(MAX_TEXT);
		item.iImage		= 0;
		item.state		= 0;
		item.stateMask	= LVIS_SELECTED;
		item.lParam		= 1;
		m_schemeList.InsertItem(&item);
	}
}

void CDlgProductionFoldingParams::OnBnClickedFoldingparamsAddscheme()
{
	CString strFile;
	strFile.LoadString(IDS_DEFAULTNAME_TEXT);

	CDlgImpositionScheme dlg(TRUE, _T("isc"), (LPCTSTR)strFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
							 _T(" |*.isc|"), NULL, NULL);	// 'this' pointer gives DlgImpositionScheme access to BindingAssistent
														// NULL if Stand-Alone
	dlg.m_bStandAlone = TRUE;
	m_bLocked = TRUE;
	int nRet = dlg.DoModal();
	m_bLocked =  FALSE;

	if (nRet == IDCANCEL)
		return;
	
	CFoldingProcess& rFoldingParams	= GetProductionProfile().m_postpress.m_folding;
	rFoldingParams.m_foldSchemeList.AddTail(dlg.m_foldSheet);

	InitSchemeList(rFoldingParams.m_foldSchemeList);
}

void CDlgProductionFoldingParams::OnBnClickedFoldingparamsDeletescheme()
{
	POSITION pos = m_schemeList.GetFirstSelectedItemPosition();
	if ( ! pos)
		return;
	int nSel = m_schemeList.GetNextSelectedItem(pos);
	if (nSel < 0)
		return;

	CFoldingProcess& rFoldingParams	= GetProductionProfile().m_postpress.m_folding;
	pos = rFoldingParams.m_foldSchemeList.FindIndex(nSel);
	if (pos)
	{
		rFoldingParams.m_foldSchemeList.RemoveAt(pos);
		InitSchemeList(rFoldingParams.m_foldSchemeList);
	}
}

void CDlgProductionFoldingParams::OnCbnSelchangeShinglingStyleCombo()
{
	UpdateData(TRUE);
	
	CFoldingProcess& rFoldingParams	= GetProductionProfile().m_postpress.m_folding;

	switch (m_nShinglingStyle)
	{
	case 0:	m_bShinglingActive = FALSE;	break;
	case 1: m_bShinglingActive = TRUE;	if (m_nShinglingMethod == CBookblock::ShinglingMethodScale) 
											m_nShinglingMethod = CBookblock::ShinglingMethodMove; 
										else
											if (m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale)
												m_nShinglingMethod = CBookblock::ShinglingMethodBottling; 
											else
												m_nShinglingMethod; 
										break;
	case 2: m_bShinglingActive = TRUE;	if (m_nShinglingMethod == CBookblock::ShinglingMethodBottling) 
											m_nShinglingMethod = CBookblock::ShinglingMethodBottlingScale;
										else
											if (m_nShinglingMethod == CBookblock::ShinglingMethodMove)	
												m_nShinglingMethod = CBookblock::ShinglingMethodScale;
											else
												m_nShinglingMethod;	
										break;
	}

	rFoldingParams.m_bShinglingActive = m_bShinglingActive;
	rFoldingParams.m_nShinglingMethod = m_nShinglingMethod;

	InitShinglingStyle();

	UpdateData(FALSE);
}

void CDlgProductionFoldingParams::InitShinglingStyle()
{
	switch (m_nShinglingMethod)
	{
	case CBookblock::ShinglingMethodMove:		m_nShinglingStyle = 1;	break;
	case CBookblock::ShinglingMethodScale:		m_nShinglingStyle = 2;	break;
	}
	if ( ! m_bShinglingActive)
		m_nShinglingStyle = 0;

	UpdateData(FALSE);
}

void CDlgProductionFoldingParams::OnBnClickedPdBrowseMarkset()
{
	UpdateData(TRUE);

	CString strMarksDir		= CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets");
	CString strRelativePath	= m_strMarkset;	
	strRelativePath.TrimLeft('.');
	CString strMarkset = PathFindFileName(m_strMarkset);
	PathRemoveExtension(strMarkset.GetBuffer(MAX_PATH));

	m_pDlgMarkSet = new CDlgMarkSet(this, TRUE);
	m_pDlgMarkSet->m_bBrowseOnly	    = TRUE;
	m_pDlgMarkSet->m_strMarksetFullPath = strMarksDir + strRelativePath;
	PathRemoveFileSpec(strRelativePath.GetBuffer(MAX_PATH));
	m_pDlgMarkSet->m_strCurrentMarkset  = strMarkset;

	if (m_pDlgMarkSet->DoModal() == IDOK)
	{
		m_strMarkset = m_pDlgMarkSet->m_strMarksetFullPath;

		PathRelativePathTo(strRelativePath.GetBuffer(MAX_PATH), (LPCTSTR)strMarksDir, FILE_ATTRIBUTE_DIRECTORY, (LPCTSTR)m_strMarkset, FILE_ATTRIBUTE_DIRECTORY);
		strRelativePath.ReleaseBuffer();
		m_strMarkset = strRelativePath;

		CString strOut;
		strOut.Format(_T("  %s"), strRelativePath);
		CRect staticRect;
		GetDlgItem(IDC_PD_PRESSMARKSET)->GetWindowRect(staticRect);
		CClientDC clientDC(this);
		PathCompactPath(clientDC.m_hDC, strOut.GetBuffer(MAX_PATH), staticRect.Width());
		UpdateData(FALSE);
	}

	delete m_pDlgMarkSet;
}

void CDlgProductionFoldingParams::OnBnClickedFpRemoveMarkset()
{
	m_strMarkset = _T("");

	UpdateData(FALSE);
}
