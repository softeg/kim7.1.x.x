// GraphicList.h: Schnittstelle f�r die Klasse CGraphicList.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRAPHICLIST_H__DB058ECF_B090_44DF_ADCD_0EDC42A048C0__INCLUDED_)
#define AFX_GRAPHICLIST_H__DB058ECF_B090_44DF_ADCD_0EDC42A048C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CGraphicItem : public CObject
{
public:
    DECLARE_SERIAL( CGraphicItem )
	CGraphicItem();
	virtual ~CGraphicItem();

//Attributes:
public:

//Operations:
public:
	virtual void	SetScale(double /*fScaleX*/, double /*fScaleY*/) {};
	virtual void	Copy(CGraphicItem* pgItem);
	virtual void	Draw(CDC* pDC, double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis);
	virtual CString	MakePDFStream(double fOutputMediaX, double fOutputMediaY, double fOutputMediaWidth, double fOutputMediaHeight, int nOutputMediaOrientation,
								  double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis);
};


#include "GraphicPath.h"


class CGraphicObject : public CObject
{
public:
    DECLARE_SERIAL( CGraphicObject )
	CGraphicObject();
	CGraphicObject(const CString& strID, CGraphicItem* pgItem);
	virtual ~CGraphicObject();
	CGraphicObject(const CGraphicObject& gObject); // copy constructor

enum gObjectType{GOPATH};

//Attributes:
public:
	CString		  m_strID;
	CGraphicItem* m_pgItem;

//Operations:
public:
	const	CGraphicObject& operator= (const CGraphicObject& gObject);
	void	Copy(CGraphicObject* pgObject);
	void	SetScale(double fScaleX, double fScaleY) { if (m_pgItem) m_pgItem->SetScale(fScaleX, fScaleY); };
	void	Draw(CDC* pDC, double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis);
	CString	MakePDFStream(double fOutputMediaX, double fOutputMediaY, double fOutputMediaWidth, double fOutputMediaHeight, int nOutputMediaOrientation,
						  double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis);
};



class CGraphicList : public CObject
{
public:
    DECLARE_SERIAL( CGraphicList )
	CGraphicList();
	virtual ~CGraphicList();

//Attributes:
public:
	CArray<CGraphicObject, CGraphicObject&> m_elements;

//Operations:
public:
	void			Serialize(CArchive& ar);
	void			Destruct();
	void			AddObject(const CString& strName, CGraphicItem* pgItem);
	void			CopyGraphicObject(CGraphicObject* pgObject);
	CGraphicObject* GetObject(const CString& strID);
};

template <> void AFXAPI SerializeElements <CGraphicObject> (CArchive& ar, CGraphicObject* pGraphicObject, INT_PTR nCount);


#endif // !defined(AFX_GRAPHICLIST_H__DB058ECF_B090_44DF_ADCD_0EDC42A048C0__INCLUDED_)
