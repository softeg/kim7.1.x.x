// PDFTargetList.h : header file
//

#pragma once



/////////////////////////////////////////////////////////////////////////////
// CReflinePositionParams 

class CObjectRefParams
{
public:
	CObjectRefParams() { m_nRowIndex = m_nColIndex = 0; m_bBorder = FALSE; };

public:								// if reference item is OBJECT_REFLINE, this is the handle to the corresponding layout object
	BYTE m_nRowIndex, m_nColIndex;	// nRowIndex, nColIndex refers the layout object inside the object matrix
	BOOL m_bBorder;					// Border of object is the reference (i.e. line between two objects was clicked)

public:
	const CObjectRefParams& operator= (const CObjectRefParams& rObjectRefParams) { 	m_nRowIndex = rObjectRefParams.m_nRowIndex; 
																					m_nColIndex = rObjectRefParams.m_nColIndex; 
																					m_bBorder	= rObjectRefParams.m_bBorder; 
																					return *this; };
	BOOL					operator==(const CObjectRefParams& rObjectRefParams) { 	if (m_nRowIndex != rObjectRefParams.m_nRowIndex) return FALSE;
																					if (m_nColIndex != rObjectRefParams.m_nColIndex) return FALSE;
																					if (m_bBorder	!= rObjectRefParams.m_bBorder) return FALSE;
																					return TRUE; };
	BOOL					operator!=(const CObjectRefParams& rObjectRefParams) { 	return !(*this == rObjectRefParams); };
	friend CArchive&		operator<<(CArchive& ar, CObjectRefParams& rObjectRefParams) { ar << rObjectRefParams.m_nRowIndex;
																						   ar << rObjectRefParams.m_nColIndex;
																						   ar << rObjectRefParams.m_bBorder; 
																						   return ar; };
	friend CArchive&		operator>>(CArchive& ar, CObjectRefParams& rObjectRefParams) { ar >> rObjectRefParams.m_nRowIndex;
																						   ar >> rObjectRefParams.m_nColIndex;
																						   ar >> rObjectRefParams.m_bBorder; 
																						   return ar; };
};				


class CReflinePositionParams : public CObject
{
public:
	DECLARE_SERIAL( CReflinePositionParams )
	CReflinePositionParams();
	CReflinePositionParams(CReflinePositionParams& rReflinePositionParams);

	enum PlaceOn {PlaceOnFront, PlaceOnBack, PlaceOnBothMirror, PlaceOnBothSamePos}; 

//Attributes:
public:
	BYTE			 m_nAnchor;					// REFLINE_ANCHOR | PAGE_ANCHOR | FOLDSHEET_ANCHOR
	int				 m_nAnchorReference;		// reference to page or foldsheet anchor (layout object index when PAGE_ANCHOR or nComponentRefIndex when FOLDSHEET_ANCHOR), -1 if REFLINE_ANCHOR
	BYTE			 m_nReferenceItemInX,		// Position rel. to plate, sheet or layout object PLATE_REFLINE | PAPER_REFLINE | BBOXFRAME_REFLINE | OBJECT_REFLINE
					 m_nReferenceItemInY,	
					 m_nReferenceLineInX,		// LEFT|RIGHT|MID|line number
					 m_nReferenceLineInY;		// BOTTOM|TOP|MID|line number
	CObjectRefParams m_hReferenceObjectInX,		
					 m_hReferenceObjectInY;
										
	BYTE			 m_nObjectReferenceLineInX,	// LEFT|RIGHT|MID
					 m_nObjectReferenceLineInY;	// BOTTOM|TOP|MID

	float			 m_fDistanceX,				// Distance from Ref.-lines 
					 m_fDistanceY;

	int				 m_nPlaceOn;

	class CMark*	 m_pParentMark;


//Operations:
public:
	const					CReflinePositionParams& 
							operator= (const CReflinePositionParams& rReflinePositionParams);
	BOOL					operator==(const CReflinePositionParams& rReflinePositionParams);
	BOOL					operator!=(const CReflinePositionParams& rReflinePositionParams);
	void					Copy	 (const CReflinePositionParams& rReflinePositionParams);
	void					Serialize(CArchive& ar);
	float					GetParentWidth (float fDefaultWidth);
	float					GetParentHeight(float fDefaultHeight);
	BOOL					ParentWidthVariable();
	BOOL					ParentHeightVariable();
	int						ParentGetReflinePosParamsID();
	void					GetSheetPosition(class CPrintSheet* pPrintSheet, int nSide, float& rfLeft, float& rfBottom, float fDefaultWidth = 1.0f, float fDefaultHeight = 1.0f, BOOL bDoTransform = TRUE);
	void					SetSheetReference(BYTE nReferenceItem, BYTE nSide, class CLayoutObject* pObject = NULL, BOOL bObjectBorder = FALSE);
	void					SetObjectReference (BYTE nSide);
	void					DrawSheetReflines(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nEdge = 0, float fDefaultWidth = 1.0f, float fDefaultHeight = 1.0f, BOOL bDoTransform = TRUE, int nDirection = -1);
	static void				DrawCenterHandles(CDC* pDC, CRect rect, COLORREF crColor, BOOL bDrawHorizontal = TRUE, BOOL bDrawVertical = TRUE, int nThickness = 1);
	BOOL					LinesIntersect(CPoint lineH[2], CPoint lineV[2]);
	void					SetUndefined();
	BOOL					IsUndefined();
	void					SetDefault(CLayoutObject* pLayoutObject);
	float					GetCounterpartOffsetX(CLayoutObject* pLayoutObject);
	float					GetCounterpartOffsetY(CLayoutObject* pLayoutObject);
	CReflinePositionParams	Rotate(int nHeadPosition);
};	


/////////////////////////////////////////////////////////////////////////////
// COutputFormat

class COutputFormat : CObject
{
public:
    DECLARE_SERIAL( COutputFormat )
	COutputFormat();
	COutputFormat(CString strName, float fWidth, float fHeight);
	COutputFormat(const COutputFormat& rOutputFormat);
	~COutputFormat();


// Attributes
public:
	CString m_strName;
	float	m_fWidth;
	float	m_fHeight;


//Operations:
public:
	const COutputFormat& operator=(const COutputFormat& rOutputFormat);
};

template <> void AFXAPI SerializeElements <COutputFormat> (CArchive& ar, COutputFormat* pOutputFormat, INT_PTR nCount);


/////////////////////////////////////////////////////////////////////////////
// COutputFormatList

class COutputFormatList : public CArray <COutputFormat, COutputFormat&>	
{																	
public:
    DECLARE_SERIAL( COutputFormatList )
	COutputFormatList();


//Attributes:


//Operations:
	BOOL Load();
	BOOL Save();
	int	 GetDefaultFormat	(const CString& strName, float* pWidth = NULL, float* pHeight = NULL);
	void RemoveDefaultFormat(const CString& strName);
};
/////////////////////////////////////////////////////////////////////////////


class CMediaPosInfo : public CObject
{
public:
    DECLARE_SERIAL( CMediaPosInfo )
	CMediaPosInfo();
	CMediaPosInfo(float fCorrectionX, float fCorrectionY, BOOL bRotated);
	~CMediaPosInfo();


// Attributes
public:
	float					m_fCorrectionX;
	float					m_fCorrectionY;
	int						m_nRotation;
	CReflinePositionParams	m_reflinePosParams;


//Operations:
public:
	const CMediaPosInfo& operator=(const CMediaPosInfo& rMediaPosInfo);
};

template <> void AFXAPI SerializeElements <CMediaPosInfo> (CArchive& ar, CMediaPosInfo* pMediaPosInfo, INT_PTR nCount);



class CMediaSize : public CObject
{
public:
    DECLARE_SERIAL( CMediaSize )
	CMediaSize();
	CMediaSize(CString strMediaName, float fWidth, float fHeight, int nReserved, float fCorrectionX, float fCorrectionY, BOOL bRotated);
	CMediaSize(const CMediaSize& rMediaSize);


enum {Portrait = 0, Landscape = 1};
enum {AlignNone, AlignToPlate, AlignToSheet, AlignToBookblockSinglePage, AlignToBookblockDoublePage, AlignToFoldSheet};

// Attributes
public:
	CString		m_strMediaName;
	int			m_nReserved;
	BOOL		m_bShrinkToFit;
	BOOL		m_bPageFrame;
	BOOL		m_bMarkFrame;
	BOOL		m_bSheetFrame;
	BOOL		m_bPlateFrame;
	BOOL		m_bBoxShape;
	BOOL		m_nAutoAlignTo;
	CPageSource m_frameSource;
	CArray <CMediaPosInfo, CMediaPosInfo&>	m_posInfos;

private:
	float		m_fMediaWidth;
	float		m_fMediaHeight;

//Operations:
public:
	const CMediaSize& operator=(const CMediaSize& rMediaSize);
	void  Serialize(CArchive& ar);
	float GetMediaWidth(CPrintSheet* pPrintSheet);
	float GetMediaHeight(CPrintSheet* pPrintSheet);
	void  SetMediaWidth(float fMediaWidth);
	void  SetMediaHeight(float fMediaHeight);
	void  DrawTile(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nTileIndex, BOOL bShowMeasuring, BOOL bShowHighlighted);
	void  DrawCoordSys(CDC* pDC, CRect& mediaRect, int nRotation, float fCorrectionX, float fCorrectionY);
	void  SetAlign(int nAlignTo);
};

template <> void AFXAPI SerializeElements <CMediaSize> (CArchive& ar, CMediaSize* pMediaSize, INT_PTR nCount);


void AFXAPI DestructElements(CMediaSize* pMediaSize, INT_PTR nCount);



class CPDFTargetProperties : public CObject
{
public:
    DECLARE_SERIAL( CPDFTargetProperties )
	CPDFTargetProperties();

	CPDFTargetProperties(int nType, CString strTargetName, CString strLocation, CMediaSize mediaSize);


enum {TypePDFFolder = 0, TypePDFPrinter = 1, TypeJDFFolder = 2};
enum {JDFCompPrinergy2215 = 0, JDFCompPrinergy23 = 1, JDFComp12 = 2, JDFComp13 = 3};

//Attributes:
public:
	int										m_nType;
	CString									m_strTargetName;	// name for folder or printer
	CString									m_strLocation;		// path of folder or name of printer
	CString									m_strLocationBack;	// path of folder of sheet backside
	CString									m_strOutputFilename;
	int										m_nPSLevel;
	BOOL									m_bEmitHalftones;
	int										m_nJDFCompatibility;
	BOOL									m_bJDFMarksInFolder;
	CString									m_strJDFMarksFolder;
	BOOL									m_bJDFOutputMime;
	CString									m_strJDFJobIDMap;
	CString									m_strJDFJobPartIDMap;
	CString									m_strJDFJobDescriptiveNameMap;
	BOOL									m_bOutputCuttingData;
	CString									m_strCuttingDataFolder;
	CString									m_strCuttingDataFilename;
	CArray <CString, CString&>				m_apogeeParameterSets;			
	CArray <CString, CString&>				m_tagList;
	CArray <CMediaSize, CMediaSize&>		m_mediaList;

//Operations:
public:
	const CPDFTargetProperties& operator=(const CPDFTargetProperties& rPDFTargetProperties);
};

template <> void AFXAPI SerializeElements <CPDFTargetProperties> (CArchive& ar, CPDFTargetProperties* pPDFTargetProperties, INT_PTR nCount);
void AFXAPI DestructElements(CPDFTargetProperties* pPDFTargetProperties, INT_PTR nCount);


class CPDFTargetList : public CArray <CPDFTargetProperties, CPDFTargetProperties&>	
{																	
public:
    DECLARE_SERIAL( CPDFTargetList )
	CPDFTargetList();

public:
	enum {PrintSheetTarget = 0, FoldSheetTarget = 2, BookletTarget = 4};

//Attributes:
public:
	COutputFormatList m_defaultFormats;


//Operations:
	BOOL				  Load(int nTarget = PrintSheetTarget);
	BOOL				  Save(int nTarget = PrintSheetTarget);
	BOOL				  SaveAsXML(int nTarget = PrintSheetTarget);
	CPDFTargetProperties* Find(CString& strPDFTargetName);
	int					  FindIndex(CString& strPDFTargetName);
};
/////////////////////////////////////////////////////////////////////////////

