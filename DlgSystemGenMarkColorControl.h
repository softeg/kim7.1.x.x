#if !defined(AFX_DLGSYSTEMGENMARKCOLORCONTROL_H__53F90772_D162_11D5_88E3_0040F68C1EF7__INCLUDED_)
#define AFX_DLGSYSTEMGENMARKCOLORCONTROL_H__53F90772_D162_11D5_88E3_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSystemGenMarkColorControl.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSystemGenMarkColorControl 

class CDlgSystemGenMarkColorControl : public CDialog
{
// Konstruktion
public:
	CDlgSystemGenMarkColorControl(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgSystemGenMarkColorControl();

// Dialogfelddaten
	//{{AFX_DATA(CDlgSystemGenMarkColorControl)
	enum { IDD = IDD_SYSTEMGENMARK_COLORCONTROL };
	BOOL	m_bMap2AllColors;
	BOOL	m_bMap2SpotColors;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl* m_pParent;
	CPrintColorList				   m_printColorList;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgSystemGenMarkColorControl)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	BOOL Create(CWnd* pParent, class CDlgLayoutObjectControl* pGrandParent);
	void AddComposite(CPageTemplate* pTemplate, CPageSource* pObjectSource);
	void LoadData(CPageTemplate* pTemplate, CPageSource* pObjectSource, int nObjectSourceIndex);
	void SaveData(CPageTemplate* pTemplate, CPageSource* pObjectSource, int nObjectSourceIndex);
	void AssignColorspace(int nColorSpace);

protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgSystemGenMarkColorControl)
	virtual BOOL OnInitDialog();
	afx_msg void OnInsertPrintcolor();
	afx_msg void OnItemchangedSysGenMarkColorList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnModifyPrintcolor();
	afx_msg void OnAssignAllPrintcolors();
	afx_msg void OnRemovePrintcolor();
	afx_msg void OnMapSpotcolors();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGSYSTEMGENMARKCOLORCONTROL_H__53F90772_D162_11D5_88E3_0040F68C1EF7__INCLUDED_
