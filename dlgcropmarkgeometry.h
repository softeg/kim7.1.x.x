#if !defined(AFX_DLGCROPMARKGEOMETRY_H__4CCA5E03_96DE_11D3_8762_204C4F4F5020__INCLUDED_)
#define AFX_DLGCROPMARKGEOMETRY_H__4CCA5E03_96DE_11D3_8762_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgCropMarks.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgCropMarkGeometry 

class CDlgCropMarkGeometry : public CDialog
{
// Konstruktion
public:
	CDlgCropMarkGeometry(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgCropMarkGeometry();

// Dialogfelddaten
	//{{AFX_DATA(CDlgCropMarkGeometry)
	enum { IDD = IDD_CROPMARK_GEOMETRY };
	CSpinButtonCtrl	m_cropMarksLengthSpin;
	CSpinButtonCtrl	m_cropMarksLineWidthSpin;
	CSpinButtonCtrl	m_cropMarksDistanceSpin;
	float	m_fCropMarksDistance;
	float	m_fCropMarksLineWidth;
	float	m_fCropMarksLength;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl* m_pParent;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgCropMarkGeometry)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void LoadData();
	void SaveData();
	void Apply();


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgCropMarkGeometry)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposCropmarksDistanceSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposCropmarksLengthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposCropmarksLinewidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGCROPMARKGEOMETRY_H__4CCA5E03_96DE_11D3_8762_204C4F4F5020__INCLUDED_
