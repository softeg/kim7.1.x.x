#include "stdafx.h"
#include "ExpressionParser.h"
#include <locale.h>



CExpressionParser::~CExpressionParser(void)
{
	m_nStreamPos = -1;
}

void CExpressionParser::Reset()
{
	m_strVariable.Empty();
	m_strOperator.Empty();
	m_strValue.Empty();
	m_fValue = 0.0f;
}

void CExpressionParser::Configure(int nSymbol, ...)
{
	va_list marker;
	va_start(marker, nSymbol);
	while (nSymbol != -1)
	{
		if ((nSymbol >= 0) && (nSymbol < NumTokens))
			m_requiredSymbols[nSymbol] = TRUE;

		nSymbol = va_arg(marker, int);
	}
	va_end(marker);
}

void CExpressionParser::ParseFile(const CString& strFilePath)
{
	if (!m_file.Open(strFilePath, CFile::modeRead))
		return;

	Reset();

	GetNextChar();
	GetSymbol();

	while (m_nSymbol != EndOfFileSym) 
	{
		AnalyseSymbol();
	}

	m_file.Close();
}

void CExpressionParser::ParseString(const CString& strInputStream)
{
	m_nStreamPos = -1;
	if (strInputStream.IsEmpty())
		return;

	Reset();

	m_nStreamPos	 = 0;
	m_strInputStream = strInputStream;

	GetNextChar();
	GetSymbol();

	while (m_nSymbol != EndOfFileSym)
	{
		AnalyseSymbol();
	}

	m_strInputStream.Empty();
}

void CExpressionParser::AnalyseSymbol()
{
	switch (m_nSymbol)
	{
	case PrintingMachineSym:
	case WorkStyleSym:
	case BindingStyleSym:		
	case NumColorsSym:			
	case NumSpotColorsSym:			
	case SheetFormatSym:
	case SheetSideSym:
	case SheetWidthSym:
	case SheetHeightSym:
	case PaperNameSym:
	case PaperTypeSym:
	case ColorSym:
	case DeviceID_PrintingMachineSym:
	case ActionSym:				
	case ProductTypeSym:				
	case ProductPartSym:				
	case PageFormatSym:
	case PageNumAtProductSym:
	case PageNumAtProductPartSym:
	case PageNumAtFoldSheetSym:
	case FoldSheetSideSym:
								m_strVariable = m_strID;
								GetSymbol();
								if ( (m_nSymbol == EqualSym) || (m_nSymbol == UnequalSym) || (m_nSymbol == SmallerSym) || (m_nSymbol == GreaterSym) || (m_nSymbol == IncludeSym))
								{
									m_strOperator = m_pTokens[m_nSymbol];
									GetString();
									m_strValue = m_strID;
								}
								else
									m_nSymbol = EndOfFileSym;
								break;

	case NoRuleSym:				m_strVariable = m_strID;
								GetSymbol();
								break;
	default:					GetSymbol(); 
								break;
	}
}


void CExpressionParser::GetSymbol()
{
	int i = 0;
	while ((m_cChar == ' ') || (m_cChar == '\t') || (m_cChar == '\n') || (m_cChar == '\r') || (m_cChar == '[') || (m_cChar == ']'))
	{
		if (m_cChar == '\n')
			m_nLineNum++;

		GetNextChar();
	}

	switch (m_cChar)
	{
	case '=':		m_strID[0] = '=';  m_strID[1] = '\0'; m_nSymbol = EqualSym;				GetNextChar(); return;
	case '<':		m_strID[0] = '<';  m_strID[1] = '\0'; m_nSymbol = SmallerSym;			GetNextChar(); return;
	case '>':		m_strID[0] = '>';  m_strID[1] = '\0'; m_nSymbol = GreaterSym;			GetNextChar(); return;
	case '(':		m_strID[0] = '(';  m_strID[1] = '\0'; m_nSymbol = LeftBracketSym;		GetNextChar(); return;
	case ')':		m_strID[0] = ')';  m_strID[1] = '\0'; m_nSymbol = RightBracketSym;		GetNextChar(); return;
	case '\"':		m_strID[0] = '\"'; m_strID[1] = '\0'; m_nSymbol = QuotationSym;			GetNextChar(); return;
//	case '[':		m_strID[0] = '[';  m_strID[1] = '\0'; m_nSymbol = LeftSquareBracketSym;	GetNextChar(); return;
//	case ']':		m_strID[0] = ']';  m_strID[1] = '\0'; m_nSymbol = RightSquareBracketSym; GetNextChar(); return;
	case '/':		m_strID[0] = '/';  m_strID[1] = '\0'; m_nSymbol = SlashSym;				GetNextChar(); return;
	case EndOfFile:	m_strID[0] = '\0'; 					  m_nSymbol = EndOfFileSym;						   return;
	}

	if (isalpha(m_cChar) || (m_cChar == '!'))
	{
		i = 0;
		do
		{
			if (i < TokenLenght)
			{
				m_strID[i] = m_cChar;
				i++;
			}
			GetNextChar();
		}while ((m_cChar != '\n') && (m_cChar != '\r') && (m_cChar != ' ') &&
				(m_cChar != '{') && (m_cChar != '}') && 
				(m_cChar != '[') && (m_cChar != ']') && 
				(m_cChar != '(') && (m_cChar != ')') && 
				(m_cChar != '/') && (m_cChar != EndOfFile));

		m_strID[i] = '\0';
		m_nSymbol = SearchSymbol(m_strID);
		if (m_nSymbol == NullSym)
			if ((m_strID[0] != '%%') && (m_strID[1] != '%%'))
				m_nSymbol = IdentSym;
	}
	else
		if (isdigit(m_cChar) || (m_cChar == '.') || (m_cChar == '-') || (m_cChar == '+'))
		{
			i = 0;
			do
			{
				if (i < TokenLenght)
				{
					m_strID[i] = m_cChar;
					i++;
				}
				GetNextChar();
			}while (isdigit(m_cChar) || (m_cChar == '.') || (m_cChar == 'e') || (m_cChar == 'E') || (m_cChar == '-') || (m_cChar == '+') );		// exp. notation possible (example -1.52588e-005)

			m_strID[i] = '\0';
			m_fValue = Ascii2Float(m_strID);
			m_nSymbol = NumSym;
		}
		else
		{
			m_strID[0] = m_cChar; m_strID[1] = '\0';
			m_nSymbol = IdentSym;
			GetNextChar();
		}
}


// get string expected inside left and right brackets
void CExpressionParser::GetString()
{
	GetSymbol();

	if (m_nSymbol != QuotationSym)
		return;

	int i = 0;
	while ((m_cChar != '\n') && (m_cChar != '\r') && (m_cChar != '\"') && (m_cChar != EndOfFile))	// scan until next quotation 
	{
		if (m_cChar == '\\')
		{
			GetNextChar();
			if ( (m_cChar == '(') || (m_cChar == ')') )
			{
				if (i < TokenLenght)
				{
					m_strID[i] = m_cChar;	i++;
				}
				GetNextChar();
			}
			else
			{
				if (i < TokenLenght)
				{
					m_strID[i] = '\\';	i++;
				}
			}
		}
		else
		{
			if (i < TokenLenght)
			{
				m_strID[i] = m_cChar;	i++;
			}
			GetNextChar();
		}
	}

	if (m_cChar != EndOfFile)
	{
		m_strID[i] = '\0';
		m_nSymbol  = IdentSym;
		GetNextChar();
	}
	else
		m_nSymbol = EndOfFileSym;
}


// get string up to end of line or end of file
void CExpressionParser::GetLine()
{
	int i = 0;
	do
	{
		if (i < TokenLenght)
		{
			m_strID[i] = m_cChar;
			i++;
		}
		GetNextChar();
	}
	while ((m_cChar != '\n') && (m_cChar != '\r') && (m_cChar != EndOfFile));

	if (m_cChar != EndOfFile)
	{
		m_strID[i] = '\0';
		m_nSymbol  = IdentSym;
		GetNextChar();
	}
	else
		m_nSymbol = EndOfFileSym;
}


void CExpressionParser::GetNextChar()
{
	if (m_nStreamPos < 0)
		m_cChar = getc(m_file.m_pStream);
	else
		if (m_nStreamPos < m_strInputStream.GetLength())
		{
			m_cChar = m_strInputStream[m_nStreamPos];
			m_nStreamPos++;
		}
		else
			m_cChar = EOF;

	if (m_cChar == EOF)
		m_cChar = EndOfFile;
}


int CExpressionParser::SearchSymbol(TCHAR* pString)
{
	BOOL		 bFound = FALSE;
	register int i = 0;;
	while (i < NumTokens)
	{
		if (_tcscmp(pString, m_pTokens[i]) == 0)
		{
			bFound = TRUE;
			break;
		}
		i++;
	}

	if (bFound)
		return(m_symbolTable[i]);
	else
		return(NullSym);
}

BOOL CExpressionParser::RequiredSymbolsFound(int nLastSymbolFound)
{
	if ((nLastSymbolFound < 0) || (nLastSymbolFound >= NumTokens))
		return FALSE;

	m_foundSymbols[nLastSymbolFound] = TRUE;
	for (int i = 0; i < NumTokens; i++)
	{
		if (m_requiredSymbols[i])
			if (!m_foundSymbols[i])
				return FALSE;
	}
	return TRUE;
}

float CExpressionParser::Ascii2Float(CString strBuffer)
{
	if (strBuffer.IsEmpty())
		return 0.0f;

	strBuffer.Replace(',', '.');

	_locale_t locale = _create_locale(LC_NUMERIC, "english");

	float fValue;
	_stscanf_l((LPCTSTR)strBuffer, _T("%f"), locale, &fValue);
	
	_free_locale(locale);

	return fValue;
}
