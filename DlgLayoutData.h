#pragma once


// CDlgLayoutData-Dialogfeld

class CDlgLayoutData : public CDialog
{
	DECLARE_DYNAMIC(CDlgLayoutData)

public:
	CDlgLayoutData(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgLayoutData();

// Dialogfelddaten
	enum { IDD = IDD_LAYOUTDATA };


public:
	CPoint	m_ptInitialPos;
	BOOL	m_bLocked;

public:
	CPrintingProfile&	GetPrintingProfile();
	CBoundProductPart&	GetBoundProductPart();
	CPrintSheet*		GetActPrintSheet();
	CLayout*			GetActLayout();
	CFoldSheet*			GetActFoldSheet();
	void 				InitData();
	void 				SaveData();
	void				ChangeNumSheets(int nNumSheetsOld);


protected:
	CString	m_strLayoutName;

public:
	virtual BOOL Create(UINT nIDTemplate, CWnd* pParentWnd = NULL);
protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedOk();
};
