#if !defined(AFX_DLGCROPMARKCONTENT_H__61BFBB32_BBA9_11D5_88C6_0040F68C1EF7__INCLUDED_)
#define AFX_DLGCROPMARKCONTENT_H__61BFBB32_BBA9_11D5_88C6_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgCropMarkContent.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgCropMarkContent 

class CDlgCropMarkContent : public CDialog
{
// Konstruktion
public:
	CDlgCropMarkContent(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgCropMarkContent();


// Dialogfelddaten
	//{{AFX_DATA(CDlgCropMarkContent)
	enum { IDD = IDD_CROPMARK_CONTENT };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl*	m_pParent;
	CDlgSystemGenMarkColorControl	m_colorControl;
	CBrush							m_hollowBrush;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgCropMarkContent)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void LoadData();
	void SaveData();
	void Apply();

protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgCropMarkContent)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGCROPMARKCONTENT_H__61BFBB32_BBA9_11D5_88C6_0040F68C1EF7__INCLUDED_
