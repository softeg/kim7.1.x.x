// LayoutObjectPreviewFrame.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "LayoutObjectPreviewFrame.h"
#include "DlgObjectPreviewControl.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "PrintSheetFrame.h"
#include "PageListView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLayoutObjectPreviewFrame

CLayoutObjectPreviewFrame::CLayoutObjectPreviewFrame()
{
	m_pParent = NULL;
}

CLayoutObjectPreviewFrame::~CLayoutObjectPreviewFrame()
{
}


BEGIN_MESSAGE_MAP(CLayoutObjectPreviewFrame, CStatic)
	//{{AFX_MSG_MAP(CLayoutObjectPreviewFrame)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CLayoutObjectPreviewFrame 

void CLayoutObjectPreviewFrame::OnPaint() 
{
//	CStatic::OnPaint();

	Invalidate();

	if ( ! m_pParent)
		return;

	CPaintDC dc(this); // device context for painting

	CRect previewFrameRect;
	GetClientRect(previewFrameRect);

	if ( (previewFrameRect.Width() <= 1) || (previewFrameRect.Height() <= 1) )
		return;

	CPen pen(PS_SOLID, 1, DARKGRAY);
	CBrush brush(RGB(200,210,225));
	dc.SelectObject(&pen);
	dc.SelectObject(&brush);
	dc.Rectangle(previewFrameRect);
	//dc.FillSolidRect(previewFrameRect, RGB(200,210,225));
	pen.DeleteObject();
	brush.DeleteObject();

	MapWindowPoints(this, previewFrameRect);
	previewFrameRect.DeflateRect(1,1);

	CRgn rgn; 
	rgn.CreateRectRgnIndirect(&previewFrameRect);  
	dc.SelectClipRgn(&rgn);  

	CRect outBBox, trimBox, clipBox;
	GetBoxes(outBBox, trimBox, clipBox);

	if ( (outBBox.Width() == 0) || (outBBox.Height() == 0) )
		return;

	dc.SetMapMode(MM_ISOTROPIC);
	dc.SetWindowExt(m_docSize);
	dc.SetViewportExt(m_sizeViewport.cx, -m_sizeViewport.cy);
	CSize sizeOut(m_docSize.cx, abs(m_docSize.cy));
	dc.LPtoDP(&sizeOut);
	dc.SetViewportOrg(previewFrameRect.left - m_pParent->m_scrollbarH.GetScrollPos(), previewFrameRect.top + sizeOut.cy - m_pParent->m_scrollbarV.GetScrollPos());

	DrawPreview(&dc, GetObjectSource(), GetObjectSourceHeader(), GetObjectTemplate(), outBBox, trimBox, clipBox, previewFrameRect, GetPrintSheet());
}

void CLayoutObjectPreviewFrame::DrawPreview(CDC* pDC, CPageSource* pObjectSource, CPageSourceHeader* pObjectSourceHeader, CPageTemplate* pTemplate, 
											CRect& outBBox, CRect& trimBox, CRect& clipBox, CRect previewFrameRect, CPrintSheet* pPrintSheet)
{
	if ( ! m_pParent)
		return;
	if ( ! GetObjectSource())
		return;

	CBrush  backGroundBrush(RGB(245,245,245));
	CBrush* pOldBrush = pDC->SelectObject(&backGroundBrush);
	CPen*   pOldPen   = (CPen*)pDC->SelectStockObject(NULL_PEN);
	pDC->Rectangle(clipBox);

	int	nHeadPosition  = (GetMark()) ? GetMark()->m_nHeadPosition : ( (GetLayoutObject()) ? GetLayoutObject()->m_nHeadPosition  : TOP);

	if (pObjectSource->m_nType == CPageSource::SystemCreated)
	{
		pDC->FillSolidRect(outBBox, WHITE);
		if (GetMark())
		{
			if ( (GetMark()->m_nType != CMark::CropMark) && (GetMark()->m_nType != CMark::FoldMark) )
			{
				CImpManDoc*	pDoc = CImpManDoc::GetDoc();
				CMark		markToCheck;
				markToCheck = *GetMark();
				markToCheck.m_markTemplate = markToCheck.TransformColorDefs((GetColorDefTable()) ? *GetColorDefTable() : (pDoc) ? pDoc->m_ColorDefinitionTable : theApp.m_colorDefTable, -1);
				CPageTemplateList::CheckForExposuresToAdd(pPrintSheet, NULL, &markToCheck, markToCheck.m_markTemplate, -1, FALSE, pDoc, -1);
				CPrintSheetView::DrawSystemCreatedContentPreview(pDC, &markToCheck.m_markTemplate, &markToCheck.m_MarkSource, outBBox, GetLayoutObject(), nHeadPosition, markToCheck.m_MarkSource.m_strSystemCreationInfo, pPrintSheet);
			}
		}
		else
		{
			CLayoutObject* pLayoutObject = NULL;
			CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
			if (pView)
			{
				CPrintSheetFrame* pFrame = (CPrintSheetFrame*)pView->GetParentFrame();
				if (pFrame)
					if (pFrame->m_pDlgLayoutObjectProperties)
						if (pFrame->m_pDlgLayoutObjectProperties->m_hWnd)
							pLayoutObject = pFrame->m_pDlgLayoutObjectProperties->m_pLayoutObject;
			}

			CPrintSheetView::DrawSystemCreatedContentPreview(pDC, pTemplate, pObjectSource, outBBox, pLayoutObject, nHeadPosition, pObjectSource->m_strSystemCreationInfo, pPrintSheet);
		}
	}
	else
	{
		CString strPreviewFile;
		int nHeaderIndex = pObjectSource->GetHeaderIndex(pObjectSourceHeader);
		strPreviewFile = pObjectSource->GetPreviewFileName(nHeaderIndex + 1);

		if ( ! CPageListView::DrawPreview(pDC, outBBox, nHeadPosition, (pTemplate) ? pTemplate->m_fContentRotation : 0, strPreviewFile))
			CPrintSheetView::DrawPreviewNotFoundMessage(pDC, outBBox, strPreviewFile);
	}

	CPen	redPen(PS_SOLID, 0, RED);
	pDC->SelectObject(&redPen);
	pDC->SelectStockObject(HOLLOW_BRUSH);

	if (pObjectSourceHeader)
		if (pObjectSourceHeader->HasTrimBox())
		{
			CPen dottedPen(PS_DOT, 0, BLUE);
			pDC->SelectObject(&dottedPen);
			pDC->SetBkColor(WHITE);
			pDC->Rectangle(trimBox);

			dottedPen.DeleteObject();
		}

	CRect clipRect;
	pDC->DPtoLP(previewFrameRect);
	previewFrameRect.NormalizeRect();
	clipBox.NormalizeRect();
	if (clipRect.IntersectRect(previewFrameRect + CSize(m_pParent->m_scrollbarH.GetScrollPos(), m_pParent->m_scrollbarV.GetScrollPos()), clipBox))
		pDC->ExcludeClipRect(clipBox);
	CPrintSheetView::FillTransparent(pDC, outBBox, LIGHTGRAY);
	pDC->SelectClipRgn(NULL);

	pDC->LPtoDP(previewFrameRect);
	CRgn rgn; 
	rgn.CreateRectRgnIndirect(&previewFrameRect);  
	pDC->SelectClipRgn(&rgn);  

	CPen clipPen(PS_SOLID, 0, RED);
	pDC->SelectObject(&clipPen);
	CSize sizeOverlap(10, 10);
	pDC->DPtoLP(&sizeOverlap);
	pDC->MoveTo(clipBox.left  - sizeOverlap.cx,	clipBox.top);
	pDC->LineTo(clipBox.right + sizeOverlap.cx,	clipBox.top);
	pDC->MoveTo(clipBox.right,					clipBox.top	   - sizeOverlap.cy);
	pDC->LineTo(clipBox.right,					clipBox.bottom + sizeOverlap.cy);
	pDC->MoveTo(clipBox.right + sizeOverlap.cx,	clipBox.bottom);
	pDC->LineTo(clipBox.left  - sizeOverlap.cx,	clipBox.bottom);
	pDC->MoveTo(clipBox.left,					clipBox.top	   - sizeOverlap.cy);
	pDC->LineTo(clipBox.left,					clipBox.bottom + sizeOverlap.cy);
	clipPen.DeleteObject();

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	redPen.DeleteObject();
}

void CLayoutObjectPreviewFrame::PrepareUpdatePreview()
{
	if ( ! m_pParent)
		return;
	if ( ! m_hWnd)
		return;

	m_sizeViewport = CSize(1,1);

	CRect previewFrameRect;
	GetClientRect(previewFrameRect);
	MapWindowPoints(this, previewFrameRect);
	InvalidateRect(previewFrameRect);

	CRect outBBox, trimBox, clipBox;
	GetBoxes(outBBox, trimBox, clipBox);
	if ( (outBBox.Width() == 0) || (outBBox.Height() == 0) || (previewFrameRect.Width() == 0) || (previewFrameRect.Height() == 0) )
	{
		UpdateWindow();
		return;
	}

	m_sizeViewport.cx = previewFrameRect.Width(); m_sizeViewport.cy = previewFrameRect.Height();
	float fThreshold = 25.0f;
	float fScale	 =  1.0f;
	if ( ((float)previewFrameRect.Width() / (float)outBBox.Width()) < ((float)previewFrameRect.Height() / (float)abs(outBBox.Height())) )
		fScale = (float)previewFrameRect.Width() / (float)outBBox.Width();
	else
		fScale = (float)previewFrameRect.Height() / (float)abs(outBBox.Height());

	if ( (fScale == 0.0f) || (outBBox.Width() == 0) || (outBBox.Height() == 0) )
		return;

	if (abs(outBBox.Height()) * fScale < fThreshold)
	{
		fScale = fThreshold / ((float)abs(outBBox.Height()) * fScale);
		m_sizeViewport.cx *= fScale; m_sizeViewport.cy = (int)m_docSize.cy;
	}
	else
		if (outBBox.Width() * fScale < fThreshold)
		{
			fScale = fThreshold / ((float)outBBox.Width() * fScale);
			m_sizeViewport.cx = (int)m_docSize.cx; m_sizeViewport.cy *= fScale;
		}

	SCROLLINFO scrollInfo;
	scrollInfo.cbSize = sizeof(scrollInfo); 
    scrollInfo.fMask  = SIF_RANGE | SIF_PAGE | SIF_POS; 
    scrollInfo.nMin   = 0; 
    scrollInfo.nMax   = m_sizeViewport.cx - 1; 
    scrollInfo.nPage  = previewFrameRect.Width(); 
    scrollInfo.nPos   = m_pParent->m_scrollbarH.GetScrollPos(); 	
	m_pParent->m_scrollbarH.SetScrollInfo(&scrollInfo);
	m_pParent->m_scrollbarH.SetScrollRange(0, m_sizeViewport.cx - 1);

    scrollInfo.nMax   = m_sizeViewport.cy - 1; 
    scrollInfo.nPage  = previewFrameRect.Height(); 
    scrollInfo.nPos   = m_pParent->m_scrollbarV.GetScrollPos(); 	
	m_pParent->m_scrollbarV.SetScrollInfo(&scrollInfo);
	m_pParent->m_scrollbarV.SetScrollRange(0, m_sizeViewport.cy - 1);

	if (m_sizeViewport.cx > previewFrameRect.Width())
		m_pParent->m_scrollbarH.EnableScrollBar(ESB_ENABLE_BOTH);
	else
		m_pParent->m_scrollbarH.EnableScrollBar(ESB_DISABLE_BOTH);

	if (m_sizeViewport.cy > previewFrameRect.Height())
		m_pParent->m_scrollbarV.EnableScrollBar(ESB_ENABLE_BOTH);
	else
		m_pParent->m_scrollbarV.EnableScrollBar(ESB_DISABLE_BOTH);

	int	nHeadPosition  = (GetLayoutObject()) ? GetLayoutObject()->m_nHeadPosition  : ( (GetMark()) ? GetMark()->m_nHeadPosition : TOP);
	int	nComponentRefIndex = (GetLayoutObject()) ? GetLayoutObject()->m_nComponentRefIndex : 0;
	if (GetObjectSource())
	{
		if (GetObjectSource()->m_nType == CPageSource::SystemCreated)
		{
			int	nIndex = GetObjectSource()->m_strSystemCreationInfo.Find(_T(" "));
			if (nIndex == -1)	// invalid string
			{
				UpdateWindow();
				return;
			}

			CString strContentType = GetObjectSource()->m_strSystemCreationInfo.Left(nIndex);
			CString strParams	   = GetObjectSource()->m_strSystemCreationInfo.Right(GetObjectSource()->m_strSystemCreationInfo.GetLength() - nIndex);
			strParams.TrimLeft(); strParams.TrimRight();
			if (strContentType == "$SECTION_BAR")
			{
				float fBarWidth, fBarHeight;
				Ascii2Float((LPCTSTR)strParams, "%f %f", &fBarWidth, &fBarHeight);
				CRect barRect = CSectionMark::GetSectionBarRect(CPrintSheetView::WorldToLP(fBarWidth), CPrintSheetView::WorldToLP(fBarHeight), outBBox, nComponentRefIndex, nHeadPosition, GetPrintSheet());
				if (m_pParent->m_scrollbarH.IsWindowVisible())
				{
					float fScale = ((float)m_sizeViewport.cx / m_docSize.cx);
					int nPos = (barRect.left - outBBox.left) * fScale;
					int nMinPos = m_pParent->m_scrollbarH.GetScrollPos(); 
					int nMaxPos = nMinPos + scrollInfo.nPage;
					if ( (nPos < nMinPos) || ((nPos + barRect.Width() * fScale) > nMaxPos) )
						m_pParent->m_scrollbarH.SetScrollPos(nPos); 
				}
				if (m_pParent->m_scrollbarV.IsWindowVisible())
				{
					float fScale = ((float)m_sizeViewport.cy / (float)abs(m_docSize.cy));
					int nPos = (outBBox.top - barRect.top) * fScale;
					int nMinPos = m_pParent->m_scrollbarV.GetScrollPos();
					int nMaxPos = nMinPos + scrollInfo.nPage;
					if ( (nPos < nMinPos) || ((nPos + abs(barRect.Height()) * fScale) > nMaxPos) )
						m_pParent->m_scrollbarV.SetScrollPos(nPos); 
				}
			}
			else
			if (strContentType == "$TEXT")
			{
				int nHeadPosition = (GetLayoutObject()) ? GetLayoutObject()->m_nHeadPosition : (GetMark()) ? GetMark()->m_nHeadPosition : TOP;
				if ( (nHeadPosition == TOP) || (nHeadPosition == RIGHT) )
				{
					m_pParent->m_scrollbarH.SetScrollPos(0); m_pParent->m_scrollbarV.SetScrollPos(0); 
				}
				else
					if (nHeadPosition == LEFT)
					{
						m_pParent->m_scrollbarH.SetScrollPos(0); m_pParent->m_scrollbarV.SetScrollPos(m_sizeViewport.cy); 
					}
					else
					{
						m_pParent->m_scrollbarH.SetScrollPos(m_sizeViewport.cx); m_pParent->m_scrollbarV.SetScrollPos(0); 
					}
			}
			else
			{
				m_pParent->m_scrollbarH.SetScrollPos(0); m_pParent->m_scrollbarV.SetScrollPos(0); 
			}
		}
		else
		{
			m_pParent->m_scrollbarH.SetScrollPos(0); m_pParent->m_scrollbarV.SetScrollPos(0); 
		}
	}

	//UpdateWindow();
}

void CLayoutObjectPreviewFrame::GetBoxes(CRect& outBBox, CRect& trimBox, CRect& clipBox)
{
	m_docSize = CSize(0, 0);
	outBBox	  = CRect(0, 0, 0, 0); 

	if ( ! m_pParent)
		return;

	if ( ! GetObjectTemplate())
		return;

	int nHeadPosition = TOP;
	if (GetLayoutObject() && ! GetMark())
	{
		if (GetObjectTemplate()->GetMarkType() == CMark::CustomMark)
		{
			if (GetObjectSourceHeader())
			{
				outBBox.right = (int)((GetObjectSourceHeader()->m_fBBoxRight - GetObjectSourceHeader()->m_fBBoxLeft)   * GetObjectTemplate()->m_fContentScaleX);
				outBBox.top   = (int)((GetObjectSourceHeader()->m_fBBoxTop   - GetObjectSourceHeader()->m_fBBoxBottom) * GetObjectTemplate()->m_fContentScaleY); 
			}
		}
		else
		{
			outBBox.right = GetLayoutObject()->GetRealWidth();
			outBBox.top   = GetLayoutObject()->GetRealHeight();
		}
		nHeadPosition  = GetLayoutObject()->m_nHeadPosition;
	}
	else
		if (GetMark())
		{
			if (GetMark()->m_nType == CMark::CustomMark)
			{
				if (GetObjectSourceHeader())
				{
					outBBox.right = (int)((GetObjectSourceHeader()->m_fBBoxRight - GetObjectSourceHeader()->m_fBBoxLeft)   * GetObjectTemplate()->m_fContentScaleX);
					outBBox.top   = (int)((GetObjectSourceHeader()->m_fBBoxTop   - GetObjectSourceHeader()->m_fBBoxBottom) * GetObjectTemplate()->m_fContentScaleY); 
				}
			}
			else
			{
				if (GetMark()->m_nType == CMark::SectionMark)
				{
					float fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;
					CLayoutObject* pSpinePage = (CLayoutObject*)GetMark()->GetSheetTrimBox(GetPrintSheet(), NULL, 0, 0, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);
					if (pSpinePage)
					{
						outBBox.right = fTrimBoxRight - fTrimBoxLeft; 
						outBBox.top   = fTrimBoxTop   - fTrimBoxBottom; 
						GetMark()->m_nHeadPosition = pSpinePage->m_nHeadPosition;
					}
					else
						outBBox.right = outBBox.bottom = 0;
					
				}
				else
				{
					outBBox.right = (int)(GetMark()->m_fMarkWidth  * GetObjectTemplate()->m_fContentScaleX);
					outBBox.top   = (int)(GetMark()->m_fMarkHeight * GetObjectTemplate()->m_fContentScaleY); 
				}
				if ( (GetMark()->m_nHeadPosition == LEFT) || (GetMark()->m_nHeadPosition == RIGHT) )
				{
					int nTemp = outBBox.right; outBBox.right = outBBox.top; outBBox.top = nTemp;
				}
			}
			nHeadPosition = GetMark()->m_nHeadPosition;
		}
		else
		{
			outBBox.SetRectEmpty();
			trimBox.SetRectEmpty();
		}

	outBBox.left   = CPrintSheetView::WorldToLP(outBBox.left);
	outBBox.bottom = CPrintSheetView::WorldToLP(outBBox.bottom);
	outBBox.right  = CPrintSheetView::WorldToLP(outBBox.right);
	outBBox.top    = CPrintSheetView::WorldToLP(outBBox.top);

	if (GetObjectSourceHeader())
	{
		trimBox.left   = (int)(GetObjectSourceHeader()->m_fTrimBoxLeft - GetObjectSourceHeader()->m_fBBoxLeft);
		trimBox.top	   = (int)(GetObjectSourceHeader()->m_fBBoxTop		- GetObjectSourceHeader()->m_fTrimBoxTop);
		trimBox.right  = trimBox.left + (int)(GetObjectSourceHeader()->m_fTrimBoxRight - GetObjectSourceHeader()->m_fTrimBoxLeft);	  
		trimBox.bottom = trimBox.top  + (int)(GetObjectSourceHeader()->m_fTrimBoxTop   - GetObjectSourceHeader()->m_fTrimBoxBottom); 
		trimBox.left   = CPrintSheetView::WorldToLP(trimBox.left);
		trimBox.right  = CPrintSheetView::WorldToLP(trimBox.right);
		trimBox.bottom = CPrintSheetView::WorldToLP(trimBox.bottom);
		trimBox.top    = CPrintSheetView::WorldToLP(trimBox.top);
	}
	else
		trimBox.SetRectEmpty();

	long nObjectWidth = 0, nObjectHeight = 0;
	if (GetLayoutObject())
	{
		nObjectWidth  = CPrintSheetView::WorldToLP(GetLayoutObject()->GetRealWidth());
		nObjectHeight = CPrintSheetView::WorldToLP(GetLayoutObject()->GetRealHeight());
	}
	else
		if (GetMark())
		{
			nObjectWidth  = CPrintSheetView::WorldToLP( ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? GetMark()->m_fMarkWidth  : GetMark()->m_fMarkHeight);
			nObjectHeight = CPrintSheetView::WorldToLP( ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? GetMark()->m_fMarkHeight : GetMark()->m_fMarkWidth);
		}

	if (GetMark())
		if (GetMark()->m_nType == CMark::SectionMark)
		{
			nObjectWidth  = outBBox.Width();
			nObjectHeight = outBBox.Height();
		}

	clipBox.left   = (outBBox.left	 + (outBBox.Width()  - nObjectWidth)  / 2.0f - CPrintSheetView::WorldToLP(GetObjectTemplate()->GetContentOffsetX()));
	clipBox.bottom = (outBBox.bottom - (outBBox.Height() + nObjectHeight) / 2.0f - CPrintSheetView::WorldToLP(GetObjectTemplate()->GetContentOffsetY()));
	clipBox.right  = (outBBox.left	 + (outBBox.Width()  + nObjectWidth)  / 2.0f - CPrintSheetView::WorldToLP(GetObjectTemplate()->GetContentOffsetX()));
	clipBox.top	   = (outBBox.bottom - (outBBox.Height() - nObjectHeight) / 2.0f - CPrintSheetView::WorldToLP(GetObjectTemplate()->GetContentOffsetY()));


	CPoint ptOutBBox1 = outBBox.TopLeft(), ptOutBBox2 = outBBox.BottomRight();
	CPoint ptTrimBox1 = trimBox.TopLeft(), ptTrimBox2 = trimBox.BottomRight();
	CPoint ptClipBox1 = clipBox.TopLeft(), ptClipBox2 = clipBox.BottomRight();
	int	   nRotation  = 0;
	switch (nHeadPosition)
	{
	case LEFT:   nRotation =  90; break;				 
	case BOTTOM: nRotation = 180; break;				 
	case RIGHT:  nRotation = 270; break;				 
	}
	ptOutBBox1 = Rotate(outBBox.TopLeft(),	   nRotation, outBBox.CenterPoint());
	ptOutBBox2 = Rotate(outBBox.BottomRight(), nRotation, outBBox.CenterPoint());
	ptTrimBox1 = Rotate(trimBox.TopLeft(),	   nRotation, outBBox.CenterPoint());
	ptTrimBox2 = Rotate(trimBox.BottomRight(), nRotation, outBBox.CenterPoint());
	ptClipBox1 = Rotate(clipBox.TopLeft(),	   nRotation, outBBox.CenterPoint());
	ptClipBox2 = Rotate(clipBox.BottomRight(), nRotation, outBBox.CenterPoint());

	outBBox.left   = min(ptOutBBox1.x, ptOutBBox2.x);
	outBBox.top    = max(ptOutBBox1.y, ptOutBBox2.y);
	outBBox.right  = max(ptOutBBox1.x, ptOutBBox2.x);
	outBBox.bottom = min(ptOutBBox1.y, ptOutBBox2.y);

	trimBox.left   = min(ptTrimBox1.x, ptTrimBox2.x);
	trimBox.top    = max(ptTrimBox1.y, ptTrimBox2.y);
	trimBox.right  = max(ptTrimBox1.x, ptTrimBox2.x);
	trimBox.bottom = min(ptTrimBox1.y, ptTrimBox2.y);

	clipBox.left   = min(ptClipBox1.x, ptClipBox2.x);
	clipBox.top    = max(ptClipBox1.y, ptClipBox2.y);
	clipBox.right  = max(ptClipBox1.x, ptClipBox2.x);
	clipBox.bottom = min(ptClipBox1.y, ptClipBox2.y);

	const long nBorder = CPrintSheetView::WorldToLP(5.0f);

	long nOffsetLeft = nBorder - min(outBBox.left,	 clipBox.left);
	long nOffsetTop  = nBorder - min(outBBox.bottom, clipBox.bottom);
	outBBox.left   += nOffsetLeft; outBBox.top     += nOffsetTop; 
	outBBox.right  += nOffsetLeft; outBBox.bottom  += nOffsetTop; 
	trimBox.left   += nOffsetLeft; trimBox.top     += nOffsetTop; 
	trimBox.right  += nOffsetLeft; trimBox.bottom  += nOffsetTop; 
	clipBox.left   += nOffsetLeft; clipBox.top     += nOffsetTop; 
	clipBox.right  += nOffsetLeft; clipBox.bottom  += nOffsetTop; 

	long nDocMinX = min(outBBox.left,   clipBox.left);
	long nDocMinY = min(outBBox.bottom, clipBox.bottom);
	long nDocMaxX = max(outBBox.right,  clipBox.right);
	long nDocMaxY = max(outBBox.top,	clipBox.top);

	m_docSize.cx = nDocMaxX - nDocMinX + 2 * nBorder;
	m_docSize.cy = nDocMaxY - nDocMinY + 2 * nBorder;
}

CPoint CLayoutObjectPreviewFrame::Rotate(CPoint pt, int nRotation, CPoint ptCenter)
{
	CPoint pt_ = pt;
	switch (nRotation)
	{
	case  90:	pt_.x = ptCenter.x + (ptCenter.y - pt.y); 
				pt_.y = ptCenter.y - (ptCenter.x - pt.x); 
				break;
	case 180:	pt_.x = ptCenter.x + (ptCenter.x - pt.x); 
				pt_.y = ptCenter.y + (ptCenter.y - pt.y); 
				break;
	case 270:	pt_.x = ptCenter.x - (ptCenter.y - pt.y); 
				pt_.y = ptCenter.x + (ptCenter.x - pt.x); 
				break;
	}

	return pt_;
}

CMark* CLayoutObjectPreviewFrame::GetMark()
{
	if (m_pParent)
		return m_pParent->GetMark();
	return NULL;
}

CLayoutObject* CLayoutObjectPreviewFrame::GetLayoutObject()
{
	if (m_pParent)
		return m_pParent->GetLayoutObject();
	return NULL;
}

CPrintSheet* CLayoutObjectPreviewFrame::GetPrintSheet()
{
	if (m_pParent)
	{
		CDlgObjectPreviewWindow* pDlg = (CDlgObjectPreviewWindow*)m_pParent->GetParent();
		if (pDlg)
			return pDlg->GetPrintSheet();
	}
	return NULL;
}

CPageSource* CLayoutObjectPreviewFrame::GetObjectSource()
{
	if (m_pParent)
	{
		CDlgObjectPreviewWindow* pDlg = (CDlgObjectPreviewWindow*)m_pParent->GetParent();
		if (pDlg)
			return pDlg->GetObjectSource();
	}
	return NULL;
}

CPageSourceHeader* CLayoutObjectPreviewFrame::GetObjectSourceHeader() 
{
	if (m_pParent)
	{
		CDlgObjectPreviewWindow* pDlg = (CDlgObjectPreviewWindow*)m_pParent->GetParent();
		if (pDlg)
			return pDlg->GetObjectSourceHeader();
	}
	return NULL;
}

CPageTemplate* CLayoutObjectPreviewFrame::GetObjectTemplate()
{
	if (m_pParent)
	{
		CDlgObjectPreviewWindow* pDlg = (CDlgObjectPreviewWindow*)m_pParent->GetParent();
		if (pDlg)
			return pDlg->GetObjectTemplate();
	}
	return NULL;
}

CColorDefinitionTable* CLayoutObjectPreviewFrame::GetColorDefTable()
{
	if (m_pParent)
	{
		CDlgObjectPreviewWindow* pDlg = (CDlgObjectPreviewWindow*)m_pParent->GetParent();
		if (pDlg)
			return pDlg->GetColorDefTable();
	}
	return NULL;
}