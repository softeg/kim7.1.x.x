// PageFormatSelection.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"



BOOL CPageFormatSelection::InitComboBox(CWnd* pDlg, CString& rstrFormatName, float* pfWidth, float* pfHeight, int nIDComboBox)
{
	if (LoadData() == TRUE)
	{
		CPageFormat PageFormatBuffer;

		CComboBox	*pBox    = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);
		UINT		nFormats = m_PageFormatList.GetCount(); 
		POSITION	pos      = m_PageFormatList.GetHeadPosition();

		pBox->ResetContent();
		for (UINT i = 0; i < nFormats; i++)    // fill combo box list 
		{
			PageFormatBuffer = m_PageFormatList.GetAt(pos);
#ifdef UNICODE
			pBox->AddString(CA2W(PageFormatBuffer.m_szName));
#else
			pBox->AddString(PageFormatBuffer.m_szName);
#endif
			m_PageFormatList.GetNext(pos);
		}	

		CString strUserDefined; strUserDefined.LoadString(IDS_USERDEFINED);
		pBox->AddString(strUserDefined);

		if (rstrFormatName.IsEmpty())
			rstrFormatName = strUserDefined;

		int nIndex = pBox->FindStringExact(-1, (LPCTSTR)rstrFormatName);

		if (nIndex == CB_ERR)
		{
			pBox->AddString(rstrFormatName);
			nIndex = pBox->FindStringExact(-1, (LPCTSTR)rstrFormatName);
		}

		pBox->SetCurSel(nIndex);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
}

void CPageFormatSelection::SelchangeFormatList(CWnd* pDlg, float* pfWidth, float* pfHeight, CString& rstrFormatName, int* nPageOrientation, int nIDComboBox)
{
	CPageFormat PageFormatBuffer;

	CComboBox *pBox	= (CComboBox*)pDlg->GetDlgItem (nIDComboBox);
	int nIndex = pBox->GetCurSel();
	if (nIndex == CB_ERR)	// no item selected
		return;

	pBox->GetLBText(nIndex, rstrFormatName);

	CPageFormat* pPageFormat = FindByName(rstrFormatName);
	if (! pPageFormat)
		return;

	PageFormatBuffer = *pPageFormat;

#ifdef UNICODE
	_tcscpy(rstrFormatName.GetBuffer(MAX_TEXT), CA2W(PageFormatBuffer.m_szName));
#else
	_tcscpy(rstrFormatName.GetBuffer(MAX_TEXT), PageFormatBuffer.m_szName);
#endif

	if (*nPageOrientation == -1) // Not initialized
	{
		*pfWidth  = PageFormatBuffer.m_fWidth;
		*pfHeight = PageFormatBuffer.m_fHeight;
		*nPageOrientation = PORTRAIT;
	}
	else
	{
		if (*nPageOrientation == PORTRAIT)
		{
			*pfWidth  = PageFormatBuffer.m_fWidth;
			*pfHeight = PageFormatBuffer.m_fHeight;
		}
		else // LANDSCAPE
		{
			*pfHeight = PageFormatBuffer.m_fWidth;
			*pfWidth  = PageFormatBuffer.m_fHeight;
		}
	}
	pDlg->UpdateData(FALSE);
}

void CPageFormatSelection::DeleteEntry(CWnd* pDlg, float* pfWidth, float* pfHeight, CString& rstrFormatName, int nPageOrientation, int nIDComboBox)
{
	if (m_PageFormatList.IsEmpty()) 
		return;

	CString strMessage;
	AfxFormatString1(strMessage, IDS_DELETE_FORMAT, rstrFormatName);

	if (AfxMessageBox(strMessage, MB_YESNO) == IDYES)	
	{
		CComboBox *pBox  = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

		int nIndex = pBox->FindStringExact(-1, rstrFormatName);
		if (nIndex == CB_ERR)
			return;

		BOOL	 bFound = FALSE;
		POSITION pos	= m_PageFormatList.GetHeadPosition();
		while (pos)
		{
			if (m_PageFormatList.GetAt(pos).m_szName == rstrFormatName)
			{
				m_PageFormatList.RemoveAt(pos);
				bFound = TRUE;
				break;
			}
			m_PageFormatList.GetNext(pos);
		}

		if ( ! bFound)
			return;

		pBox->DeleteString(nIndex);

		if (SaveData() == FALSE)
		{
			AfxMessageBox(IDS_DEL_NOTOK, MB_OK);
			LoadData();
		}
		if (!m_PageFormatList.IsEmpty()) 
		{
			if (pBox->GetCount() - 1 < nIndex)	// last entry was deleted
				nIndex = pBox->GetCount() - 1;

			pBox->SetCurSel(nIndex);
			SelchangeFormatList(pDlg, pfWidth, pfHeight, rstrFormatName, &nPageOrientation, nIDComboBox);	
		}
		else
		{
			*pfHeight	   = 0.0f;
			*pfWidth	   = 0.0f;
			rstrFormatName = "";
		}
	}
	pDlg->UpdateData(FALSE);      
}

void CPageFormatSelection::NewEntry(CWnd* pDlg, float* pfWidth, float* pfHeight, CString& rstrFormatName, int nPageOrientation, int nIDComboBox)
{
	CPageFormat PageFormatBuffer;
	CComboBox	*pBox = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

	pDlg->UpdateData(TRUE);      

	rstrFormatName.TrimLeft();
	rstrFormatName.TrimRight();
	if (rstrFormatName.IsEmpty())
	{
		AfxMessageBox(IDS_INVALID_NAME);
		return;
	}

	CString strMessage;
	AfxFormatString1(strMessage, IDS_ADD_FORMAT, rstrFormatName);

	if (AfxMessageBox(strMessage, MB_YESNO) == IDYES)	
	{
		PageFormatBuffer.m_fWidth  = *pfWidth;
		PageFormatBuffer.m_fHeight = *pfHeight;
#ifdef UNICODE
		_tcscpy(CA2W(PageFormatBuffer.m_szName), (LPCTSTR)rstrFormatName);
#else
		_tcscpy(PageFormatBuffer.m_szName, (LPCTSTR)rstrFormatName);
#endif
		m_PageFormatList.AddTail(PageFormatBuffer);

		if (SaveData() == FALSE)
			AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

		LoadData(); //reload data
		pBox->AddString(rstrFormatName);
		pBox->SetCurSel(pBox->GetCount() - 1);
		SelchangeFormatList(pDlg, pfWidth, pfHeight, rstrFormatName, &nPageOrientation, nIDComboBox);	
	}

	pDlg->UpdateData(FALSE);      
}

void CPageFormatSelection::ChangeEntry(CWnd* pDlg, float* pfWidth, float* pfHeight, CString& rstrFormatName, int nPageOrientation, int nIDComboBox)
{
	CPageFormat PageFormatBuffer;
	CComboBox	*pBox = (CComboBox*)pDlg->GetDlgItem (nIDComboBox);

	pDlg->UpdateData(TRUE);      

	if (AfxMessageBox(IDS_CHANGE_ENTRY, MB_YESNO) == IDYES)	
	{
		PageFormatBuffer.m_fWidth  = *pfWidth;
		PageFormatBuffer.m_fHeight = *pfHeight;
#ifdef UNICODE
		_tcscpy(CA2W(PageFormatBuffer.m_szName), (LPCTSTR)rstrFormatName);
#else
		_tcscpy(PageFormatBuffer.m_szName, (LPCTSTR)rstrFormatName);
#endif

		int nIndex = pBox->GetCurSel();
		if (nIndex != CB_ERR)
		{
			BOOL	 bFound = FALSE;
			POSITION pos	= m_PageFormatList.GetHeadPosition();
			while (pos)
			{
				if (m_PageFormatList.GetAt(pos).m_szName == rstrFormatName)
				{
					m_PageFormatList.SetAt(pos, PageFormatBuffer);
					bFound = TRUE;
					break;
				}
				m_PageFormatList.GetNext(pos);
			}

			if (SaveData() == FALSE)
				AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

			LoadData(); //reload data
#ifdef UNICODE
			pBox->SetCurSel(pBox->FindStringExact(-1, CA2W(PageFormatBuffer.m_szName)));
#else
			pBox->SetCurSel(pBox->FindStringExact(-1, PageFormatBuffer.m_szName));
#endif
			SelchangeFormatList(pDlg, pfWidth, pfHeight, rstrFormatName, &nPageOrientation, nIDComboBox);	
		}
	}
	pDlg->UpdateData(FALSE);      
}

BOOL CPageFormatSelection::LoadData()
{
	CPageFormat		PageFormatBuffer;
	TCHAR			pszFileName[MAX_PATH];
	CFile			FormatDefFile;
	CFileException	fileException;
	UINT			nBytes = sizeof(CPageFormat);

	_tcscpy(pszFileName,	theApp.GetDataFolder()); 
	_tcscat(pszFileName,	_T("\\PageFormats.def"));

	if (!FormatDefFile.Open(pszFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n",
				pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		m_PageFormatList.RemoveAll(); // Be sure to work with an empty list
		FormatDefFile.Seek(0, CFile::begin);
		while (nBytes == sizeof(CPageFormat))
		{
			nBytes = FormatDefFile.Read(&PageFormatBuffer, sizeof(CPageFormat));
			if (nBytes == sizeof(CPageFormat))
				m_PageFormatList.AddTail(PageFormatBuffer);
		}
		FormatDefFile.Close();  //TODO: Should be closed after closing dialog
		if (m_PageFormatList.IsEmpty())
			return FALSE; 
		else
			return TRUE;  	
	}
}

BOOL CPageFormatSelection::SaveData()
{
	CPageFormat    PageFormatBuffer;
	TCHAR		   pszFileName[MAX_PATH];
	CFile		   FormatDefFile;
	CFileException fileException;

	_tcscpy(pszFileName, theApp.GetDataFolder()); // TODO: Verify the path
	_tcscat(pszFileName, _T("\\PageFormats.def"));
	if (!FormatDefFile.Open(pszFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n",
				pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		UINT	  nFormats = m_PageFormatList.GetCount(); 
		POSITION  pos	   = m_PageFormatList.GetHeadPosition();

		for (UINT i = 0; i < nFormats; i++)                        
		{
			PageFormatBuffer = m_PageFormatList.GetAt(pos);
			FormatDefFile.Write(&PageFormatBuffer, sizeof(CPageFormat));
			m_PageFormatList.GetNext(pos);
		}	
	}
	FormatDefFile.Close();  //TODO: Should be closed after closing dialog
	return TRUE;
}

CPageFormat* CPageFormatSelection::FindBySize(float fWidth, float fHeight)
{
	POSITION pos = m_PageFormatList.GetHeadPosition();
	while (pos)
	{
		CPageFormat& rPageFormat = m_PageFormatList.GetNext(pos);
		if ( (fabs(rPageFormat.m_fWidth - fWidth) < 0.01) && (fabs(rPageFormat.m_fHeight - fHeight) < 0.01) )
			return &rPageFormat;
		else
			if ( (fabs(rPageFormat.m_fWidth - fHeight) < 0.01) && (fabs(rPageFormat.m_fHeight - fWidth) < 0.01) )
				return &rPageFormat;
	}

	return NULL;
}

CPageFormat* CPageFormatSelection::FindByName(CString strName)
{
	POSITION pos = m_PageFormatList.GetHeadPosition();
	while (pos)
	{
		CPageFormat& rPageFormat = m_PageFormatList.GetNext(pos);
		if (strName.CompareNoCase(rPageFormat.m_szName) == 0)
			return &rPageFormat;
	}

	return NULL;
}

void AFXAPI CPageFormatSelection::DDX_PageFormatCombo(CDataExchange* pDX, int nIDC, CString& strValue)
{
	TCHAR szValue[MAX_TEXT + 1];

	HWND hWndCtrl = pDX->PrepareEditCtrl(nIDC);
	if (pDX->m_bSaveAndValidate)
	{
		::GetWindowText(hWndCtrl, szValue, MAX_TEXT);  
		strValue = szValue;
	}
	else
		::SetWindowText(hWndCtrl, (LPCTSTR)strValue);
}
