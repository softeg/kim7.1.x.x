// AutoServerDetailsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "AutoServerJobsView.h"
#include "AutoServerDetailsView.h"
#include "AutoServerToolsView.h"
#include "GraphicComponent.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PageListFrame.h"


#define IDC_AUTOSERVERDETAILSVIEW_HEADER 12122


// CAutoServerDetailsView

IMPLEMENT_DYNCREATE(CAutoServerDetailsView, CScrollView)

CAutoServerDetailsView::CAutoServerDetailsView()
{
    m_DisplayList.Create(this, FALSE, DISPLAY_LIST_TYPE(CAutoServerDetailsView));
	m_DisplayList.SetClassName(_T("CAutoServerDetailsView"));

	m_nHeaderItemsTotalWidth = 0;
	m_nHeaderHeight = 0;
	m_headerFont.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));

}

CAutoServerDetailsView::~CAutoServerDetailsView()
{
	m_headerFont.DeleteObject();
}

void CAutoServerDetailsView::DoDataExchange(CDataExchange* pDX)
{
	CScrollView::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAutoServerDetailsView, CScrollView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY(HDN_TRACK,	IDC_AUTOSERVERDETAILSVIEW_HEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK, IDC_AUTOSERVERDETAILSVIEW_HEADER, OnHeaderEndTrack)
END_MESSAGE_MAP()



// CAutoServerDetailsView-Diagnose

#ifdef _DEBUG
void CAutoServerDetailsView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CAutoServerDetailsView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG


// CAutoServerDetailsView-Meldungshandler

BOOL CAutoServerDetailsView::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;

//	return CScrollView::OnEraseBkgnd(pDC);
}

int CAutoServerDetailsView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	//CREATE_INPLACE_EDIT_EX(InplaceEdit);
	//CREATE_INPLACE_COMBOBOX(InplaceComboBox, 0);

	return 0;
}

void CAutoServerDetailsView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
}

void CAutoServerDetailsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_headerColumnWidths.RemoveAll();
	m_HeaderCtrl.DestroyWindow();
	m_nHeaderHeight = 0;

	if ( ! m_HeaderCtrl.GetSafeHwnd())
	{
		m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, IDC_AUTOSERVERDETAILSVIEW_HEADER);	
		m_HeaderCtrl.SetFont(&m_headerFont, FALSE);

		HD_ITEM hdi;
		hdi.mask		= HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
		hdi.fmt			= HDF_LEFT | HDF_STRING;

		m_headerColumnWidths.RemoveAll();

		int		nIndex = 0;
		CString string;

		string.LoadString(IDS_PRODUCT);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 200);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_PAPERLIST_FORMAT); 
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 80);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_PAGELISTHEADER_PAGES);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 300);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_PRINTSHEET_TITLE);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 400);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_LAST_OUTPUT);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 120);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_LOCATION);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 500);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		m_nHeaderItemsTotalWidth = 0;
		for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
			m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];
	}

	OnSize(0, 0, 0); 

	ModifyStyle(0, WS_CLIPCHILDREN);	// Clip header control 

	Invalidate();

	m_DisplayList.Invalidate();

	m_docSize = CalcExtents();
	SetScrollSizes(MM_TEXT, m_docSize);
}

void CAutoServerDetailsView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);

	if (m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdl;
		WINDOWPOS wp;
		hdl.prc = &clientRect;
		hdl.pwpos = &wp;
		m_HeaderCtrl.Layout(&hdl);	
		m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_nHeaderHeight = wp.cy;
	} 
}
// CAutoServerDetailsView-Zeichnung

int	CAutoServerDetailsView::GetActJobID()
{
	CAutoServerJobsView* pView = theApp.GetAutoServerJobsView();
	return (pView) ? pView->GetActJobID() : -1;
}

CSize CAutoServerDetailsView::CalcExtents()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CRect rcBox = Draw(&dc, NULL);

	dc.DeleteDC();

	return CSize(rcBox.right + 10, rcBox.bottom + 10);
}

void CAutoServerDetailsView::OnDraw(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(rcClient); 
	CRect rcFrame(CPoint(0, 0), CSize(rcClient.Width(), max(rcClient.Height(), m_docSize.cy + m_nHeaderHeight)));

	CDC	memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap bitmap;	bitmap.CreateCompatibleBitmap(pDC, rcFrame.Width(), rcFrame.Height());
	memDC.SelectObject(&bitmap);

	memDC.FillSolidRect(rcFrame, WHITE);//RGB(227,232,238));

	memDC.SaveDC();
	Draw(&memDC, &m_DisplayList);
	memDC.RestoreDC(-1);
	
	pDC->SaveDC();	// save current clipping region
	ClipHeader(pDC);

	pDC->BitBlt(0, m_nHeaderHeight, rcFrame.Width(), rcFrame.Height(), &memDC, 0, m_nHeaderHeight, SRCCOPY);

	bitmap.DeleteObject();
	memDC.DeleteDC();

	pDC->RestoreDC(-1);
}

void CAutoServerDetailsView::ClipHeader(CDC* pDC)
{
	CRect rcClientRect, rcClipRect;
	GetClientRect(rcClientRect); rcClipRect = rcClientRect;
	rcClipRect.top = m_nHeaderHeight;
	rcClipRect.OffsetRect(-pDC->GetViewportOrg());
	pDC->IntersectClipRect(rcClipRect);
}

CRect CAutoServerDetailsView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	CRect rcBox(0,0,0,0);

	return DrawJobDetailsTable(pDC, pDisplayList);	
}

CRect CAutoServerDetailsView::DrawJobDetailsTable(CDC* pDC, CDisplayList* pDisplayList)
{
	CRect rcFrame;
	GetClientRect(rcFrame); 
	
	rcFrame.top += m_nHeaderHeight;	
	//rcFrame.right = rcFrame.left + 500;
	rcFrame.top += 5;
	rcFrame.left += 5;

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	int nColIndex = 0;
	ResetDisplayColums();
	AddDisplayColumn(ColJobProduct,				m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColJobFormat,				m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColJobInputFile,			m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColJobOutputFile,			m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColJobOutputDate,			m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColJobOutputLocation,		m_headerColumnWidths[nColIndex++]);

	int nJobID = GetActJobID();

	kimAutoDB::_jobs job;
	theApp.m_kaDB.ka_GetJob(&job, nJobID);

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);

	int nLinePosX  = 0;
	int nLinePosY  = 0;
	int nRowHeight = 25;
	CRect rcRow = rcFrame; rcRow.top += 5; rcRow.bottom = rcRow.top + nRowHeight;

	kimAutoDB_product products[1000];
	theApp.m_kaDB.ka_GetProducts(products, nJobID, 1000);

	for (int nProductIndex = 0; nProductIndex < 1000; nProductIndex++)
	{
		if (products[nProductIndex].id < 0)
			continue;

		kimAutoDB_product product = products[nProductIndex];
		/////// columns
		CRect rcColumn = rcRow; 
		rcColumn.left += 5;

		BOOL bShowProductName   = TRUE;
		BOOL bShowProductFormat = TRUE;
		if (nProductIndex > 0)
			if (CString(products[nProductIndex - 1].name.c_str()) == CString(products[nProductIndex].name.c_str()))
			{
				bShowProductName = FALSE;
				if ( (products[nProductIndex - 1].width == products[nProductIndex].width) && (products[nProductIndex - 1].height == products[nProductIndex].height) )
					bShowProductFormat = FALSE;
			}

		int	  nRight = 0;
		CRect rcRet(rcBox.TopLeft(), CSize(1,1));
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColJobProduct:		rcRet = DrawJobProduct(pDC, rcColumn, product, bShowProductName, pDisplayList);									break;
			case ColJobFormat:		if (bShowProductFormat)
										rcRet = DrawJobProductFormat (pDC, rcColumn, product, pDisplayList);		 nLinePosX = rcColumn.right;	break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + 5;
		}

		rcRow.top	 = rcBox.bottom + 5;
		rcRow.bottom = rcRow.top + nRowHeight;

		if (nProductIndex < 999)
			if ( (CString(products[nProductIndex].name.c_str()) != CString(products[nProductIndex + 1].name.c_str())) && (products[nProductIndex + 1].id >= 0) )
			{
				//pDC->MoveTo(5, rcRow.top - 2); pDC->LineTo(nLinePosX - 5, rcRow.top - 2);
				rcRow.top += 10;
			}
	}

	pDC->MoveTo(nLinePosX - 1, GetScrollPos(SB_VERT)); pDC->LineTo(nLinePosX - 1, rcFrame.bottom + GetScrollPos(SB_VERT));

	rcRow = rcFrame; rcRow.top += 5; rcRow.bottom = rcRow.top + nRowHeight;
	CRect rcBox2(rcRow.TopLeft(), CSize(1,1));

	kimAutoDB::_inputPDF inputPDFs[1000];
	theApp.m_kaDB.ka_GetInputPDF(inputPDFs, nJobID, 1000);

	for (int nInputPDFIndex = 0; nInputPDFIndex < 1000; nInputPDFIndex++)
	{
		if (inputPDFs[nInputPDFIndex].id < 0)
			continue;

		kimAutoDB::_inputPDF& rInputPDF = inputPDFs[nInputPDFIndex];
		/////// columns
		CRect rcColumn = rcRow; 
		rcColumn.left += 5;

		int	  nRight = 0;
		CRect rcRet(rcBox2.TopLeft(), CSize(1,1));
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColJobInputFile: rcRet = DrawJobInputFile(pDC, rcColumn, rInputPDF, pDisplayList); nLinePosX = rcColumn.right; break;
			}
			rcBox2.UnionRect(rcBox2, rcRet);

			rcColumn.left = nRight + 5;
		}

		rcRow.top	 = rcBox2.bottom + 15;
		rcRow.bottom = rcRow.top + nRowHeight;
	}

	pDC->MoveTo(nLinePosX - 1, GetScrollPos(SB_VERT)); pDC->LineTo(nLinePosX - 1, rcFrame.bottom + GetScrollPos(SB_VERT));

	rcRow = rcFrame; rcRow.top += 5; rcRow.bottom = rcRow.top + nRowHeight;
	CRect rcBox3(rcRow.TopLeft(), CSize(1,1));

	kimAutoDB_outputPDF outputPDFs[1000];
	theApp.m_kaDB.ka_GetOutputPDF(outputPDFs, nJobID, 1000);

	for (int nOutputPDFIndex = 0; nOutputPDFIndex < 1000; nOutputPDFIndex++)
	{
		if (outputPDFs[nOutputPDFIndex].id < 0)
			continue;

		kimAutoDB_outputPDF& rOutputPDF = outputPDFs[nOutputPDFIndex];
		/////// columns
		CRect rcColumn = rcRow; 
		rcColumn.left += 5;

		int	  nRight = 0;
		CRect rcRet(rcBox3.TopLeft(), CSize(1,1));
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			rcRet = rcBox3;
			switch (m_displayColumns[i].nID)
			{
			case ColJobOutputFile:			rcRet = DrawJobOutputFile		 (pDC, rcColumn, rOutputPDF, pDisplayList); break;
			case ColJobOutputDate:			rcRet = DrawJobOutputDate		 (pDC, rcColumn, rOutputPDF, pDisplayList); break;
			case ColJobOutputLocation:		rcRet = DrawJobOutputLocation	 (pDC, rcColumn, rOutputPDF, pDisplayList); break;
			}
			rcBox3.UnionRect(rcBox3, rcRet);

			rcColumn.left = nRight + 5;
		}

		rcRow.top	 = rcBox3.bottom + 10;
		rcRow.bottom = rcRow.top + nRowHeight;
	}

	rcBox.UnionRect(rcBox, rcBox3);

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CAutoServerDetailsView::DrawJobProduct(CDC* pDC, CRect rcFrame, kimAutoDB_product& rProduct, BOOL bShowProductName, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	int nTextHeight = 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CString strOut;
	int nTextValign = DT_TOP;
	if (rProduct.type == kimAutoDB_product::Bound)
	{
		//CRect rcImg = rcFrame; rcImg.DeflateRect(0, 1); rcImg.OffsetRect(-5, -6);
		//rcImg = pDoc->m_PageTemplateList.DrawSinglePage(pDC, rcImg, 0, GetIndex(), FALSE, FALSE, TRUE, TRUE, TRUE);
		//if (rcImg.top == rcImg.bottom)
		//{
		//	rcImg.top = rcFrame.top; rcImg.bottom = rcImg.top + 15;
		//}
		//rcBox.UnionRect(rcBox, rcImg);

		pDC->SelectObject(&boldFont);
		pDC->SetTextColor(GUICOLOR_CAPTION);
		strOut.Format(_T(" %d "), rProduct.numPages);
		CRect rcPages = rcFrame; /*rcPages.top = rcImg.CenterPoint().y - 6; */rcPages.bottom = rcPages.top + 14; rcPages.OffsetRect(6, 0); rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
		rcPages.InflateRect(1, 1);
		CBrush brush(WHITE);
		CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
		CBrush* pOldBrush = pDC->SelectObject(&brush);
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		pDC->RoundRect(rcPages, CPoint(8, 8));
		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		pDC->SelectObject(&font);
		pDC->SetBkMode(TRANSPARENT);

		rcBox.UnionRect(rcBox, rcPages);

		pDC->SetTextColor( (bShowProductName) ? GUICOLOR_CAPTION : WHITE);	
		CRect rcText = rcFrame; rcText.left = rcText.left + 35; rcText.bottom = rcText.top + nTextHeight;
		DrawTextMeasured(pDC, rProduct.name.c_str(), rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);

		rcText.left += pDC->GetTextExtent(rProduct.name.c_str()).cx + 5;
		pDC->SetTextColor(RGB(40,120,30));
		DrawTextMeasured(pDC, CString(_T("| ")) + rProduct.partName.c_str(), rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);
	}
	else
	{
		pDC->SelectObject(&boldFont);
		pDC->SetTextColor(GUICOLOR_CAPTION);
		strOut.Format(_T(" %d "), rProduct.numPages);
		CRect rcPages = rcFrame; /*rcPages.top = rcImg.CenterPoint().y - 6; */rcPages.bottom = rcPages.top + 14; rcPages.OffsetRect(6, 0); rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
		rcPages.InflateRect(1, 1);
		CBrush brush(WHITE);
		CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
		CBrush* pOldBrush = pDC->SelectObject(&brush);
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		pDC->RoundRect(rcPages, CPoint(8, 8));
		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		pDC->SelectObject(&font);
		pDC->SetBkMode(TRANSPARENT);

		rcBox.UnionRect(rcBox, rcPages);

		pDC->SetTextColor( (bShowProductName) ? GUICOLOR_CAPTION : WHITE);	
		CRect rcText = rcFrame; rcText.left = rcText.left + 35; rcText.bottom = rcText.top + nTextHeight;
		DrawTextMeasured(pDC, rProduct.name.c_str(), rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);
	}

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CAutoServerDetailsView::DrawJobProductFormat(CDC* pDC, CRect rcFrame, kimAutoDB_product& rProduct, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString	 strWidth, strHeight;

	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));

	int nTextValign = DT_VCENTER;

	CPrintSheetView::TruncateNumber(strWidth,  rProduct.width);
	CPrintSheetView::TruncateNumber(strHeight, rProduct.height);
	CString strOut;
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CAutoServerDetailsView::DrawJobInputFile(CDC* pDC, CRect rcFrame, kimAutoDB::_inputPDF& rInputPDF, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CImage imgPDF;
	imgPDF.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_PDF_FILE));
	CRect rcImg(CPoint(rcFrame.left + 5, rcFrame.top - 2), CSize(imgPDF.GetWidth(), imgPDF.GetHeight()));

	int nTextHeight = 15;
	CFont font;
	font.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame; rcText.left += 20; rcText.bottom = rcText.top + nTextHeight;
	CString strExtension = PathFindExtension(rInputPDF.name.c_str()); strExtension.MakeLower();
	if ( ! imgPDF.IsNull())
		imgPDF.TransparentBlt(pDC->m_hDC, rcImg, WHITE);

	CString strOut;
	pDC->SetTextColor(GUICOLOR_CAPTION);
	strOut.Format(_T(" %d "), rInputPDF.pages);
	CRect rcPages = rcImg; rcPages.top = rcImg.CenterPoint().y - 6; rcPages.bottom = rcImg.CenterPoint().y + 8; rcPages.OffsetRect(20, 0); rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
	rcPages.InflateRect(1, 1);
	CBrush brush(WHITE);
	CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
	CBrush* pOldBrush = pDC->SelectObject(&brush);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	pDC->RoundRect(rcPages, CPoint(8, 8));
	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	rcText.left += 30;
	pDC->SetTextColor(BLACK);
	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath((LPCTSTR)rInputPDF.name.c_str(), szDrive, szDir, szFname, szExt);
	CString strFileName; strFileName.Format(_T("%s%s"), szFname, szExt);
	DrawTextMeasured(pDC, strFileName, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	if (pDisplayList)
	{
		CRect rcDI = rcBox; rcDI.top = rcText.top; rcDI.left -= 4; rcDI.top -= 3; rcDI.right += 2; rcDI.bottom += 2; 
		rcDI.OffsetRect(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)); 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)rInputPDF.id, DISPLAY_ITEM_REGISTRY(CAutoServerDetailsView, JobInputFile), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
			{
				CRect rcFeedback = pDI->m_BoundingRect; 
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				COLORREF crHighlightColor = RGB(255,195,15);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
			}
			if ( (rInputPDF.status == kimAutoDB::inPDF_analyze) || (rInputPDF.status == kimAutoDB::inPDF_previews) )
			{
				CRect rcRect = pDI->m_BoundingRect; rcRect.OffsetRect(120, 5); rcRect.bottom = rcRect.top + 30;
				pDI->OpenAnimationCtrl(pDC, rcRect, _T("GIF"), _T("IDR_ACTIVITY_ANIMATION"), (rInputPDF.status == kimAutoDB::inPDF_analyze) ? _T("analysiere ...") : _T("erstelle Previews ...") );
			}
			else
				pDI->CloseAnimationCtrl();
		}
	}

	font.DeleteObject();

	return rcBox;
}

CRect CAutoServerDetailsView::DrawJobInputFilePageRange(CDC* pDC, CRect rcFrame, kimAutoDB::_inputPDF& rInputPDF, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;
	CFont font;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText1 = rcFrame; rcText1.bottom = rcText1.top + nTextHeight; rcText1.right = rcText1.left + 30;
	CRect rcText2 = rcText1; rcText2.left = rcText1.right + 10; rcText2.right = rcFrame.right;
	CString strOut1, strOut2;;
	if (rInputPDF.pages <= 0)
		strOut1 = strOut2 = _T("");
	else
	{
		strOut1.Format(_T("%d"), rInputPDF.pages);
		strOut2.Format(_T("(%d - %d)"), rInputPDF.pageNr, rInputPDF.pageNr + rInputPDF.pages - 1);
	}

	DrawTextMeasured(pDC, strOut1, rcText1, DT_RIGHT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
	DrawTextMeasured(pDC, strOut2, rcText2, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	if (pDisplayList)
	{
		CRect rcDI = rcText1; rcDI.right = rcFrame.right; rcDI.bottom = rcDI.top + 25; // room for activity indication
		rcDI.OffsetRect(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)); 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)rInputPDF.id, DISPLAY_ITEM_REGISTRY(CAutoServerDetailsView, JobInputFilePageRange), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
			{
				CRect rcFeedback = pDI->m_BoundingRect; 
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				COLORREF crHighlightColor = RGB(255,195,15);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
			}
			if ( (rInputPDF.status == kimAutoDB::inPDF_analyze) || (rInputPDF.status == kimAutoDB::inPDF_previews) )
				pDI->OpenAnimationCtrl(pDC, pDI->m_BoundingRect, _T("GIF"), _T("IDR_ACTIVITY_ANIMATION"), (rInputPDF.status == kimAutoDB::inPDF_analyze) ? _T("analysiere ...") : _T("erstelle Previews ...") );
			else
				pDI->CloseAnimationCtrl();
		}
	}

	font.DeleteObject();

	return rcBox;
}

CRect CAutoServerDetailsView::DrawJobOutputFile(CDC* pDC, CRect rcFrame, kimAutoDB_outputPDF& rOutputPDF, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	if (rOutputPDF.thumbnailBitmap.m_pStream)
	{
		CImage img;
		rOutputPDF.thumbnailBitmap.LoadThumbnail(img);
		if ( ! img.IsNull())
		{
			CRect rcThumbnail = rcFrame; rcThumbnail.left += 5; rcThumbnail.right = rcThumbnail.left + img.GetWidth(); rcThumbnail.bottom = rcThumbnail.top + img.GetHeight();
			img.Draw(pDC->m_hDC, rcThumbnail.left, rcThumbnail.top, rcThumbnail.Width(), rcThumbnail.Height(), 0, 0, img.GetWidth(), img.GetHeight());
			rcBox.bottom = rcThumbnail.bottom;
		}
	}
	else
		DrawTextMeasured(pDC, rOutputPDF.name.c_str(), rcFrame, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;
	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	BOOL		bDimmed = FALSE;
	CFileStatus fileStatus;
	if (CFile::GetStatus(rOutputPDF.outputPath.c_str(), fileStatus))	
		pDC->SetTextColor(BLACK);
	else
	{
		pDC->SetTextColor(MIDDLEGRAY);
		bDimmed = TRUE;
	}

	CImage imgPDF, imgJDF;
	imgPDF.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(( ! bDimmed) ? IDB_PDF_FILE : IDB_PDF_FILE_DIMMED));
	imgJDF.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_JDF_FILE));
	CRect rcImg(CPoint(rcFrame.left + 115, rcFrame.top), CSize(imgPDF.GetWidth(), imgPDF.GetHeight()));

	CString strExtension = PathFindExtension(rOutputPDF.outputPath.c_str()); strExtension.MakeLower();
	if (strExtension == _T(".pdf"))
	{
		if ( ! imgPDF.IsNull()) 
			imgPDF.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	}
	else
		if (strExtension == _T(".jdf"))
		{
			if ( ! imgJDF.IsNull())
				imgJDF.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
		}
	
	CString strFileName; 
	TCHAR   szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath((LPCTSTR)rOutputPDF.outputPath.c_str(), szDrive, szDir, szFname, szExt);
	strFileName.Format(_T("%s%s"), szFname, szExt);
	if (strFileName.IsEmpty())
		strFileName = _T("kein Ausgabeziel zugewiesen");

	CRect rcText = rcFrame;	rcText.left += 135; rcText.bottom = rcText.top + nTextHeight;
	DrawTextMeasured(pDC, strFileName, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.left = rcImg.left; rcDI.top -= 3; rcDI.right = rcBox.right + 2; rcDI.bottom = max(rcText.bottom, rcImg.bottom) + 2; 
		rcDI.OffsetRect(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)); 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)rOutputPDF.id, DISPLAY_ITEM_REGISTRY(CAutoServerDetailsView, JobOutputFile), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
			{
				CRect rcFeedback = pDI->m_BoundingRect; 
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				COLORREF crHighlightColor = RGB(255,195,15);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
			}
			if (rOutputPDF.status == kimAutoDB::outPDF_writing)
			{
				CRect rcRect = pDI->m_BoundingRect; rcRect.OffsetRect(0, 30);
				pDI->OpenAnimationCtrl(pDC, rcRect, _T("GIF"), _T("IDR_ACTIVITY_ANIMATION"), "Bogen wird ausgegeben ...");
			}
			else
				pDI->CloseAnimationCtrl();
		}
	}

	font.DeleteObject();

	return rcBox;
}

CRect CAutoServerDetailsView::DrawJobOutputDate(CDC* pDC, CRect rcFrame, kimAutoDB_outputPDF& rOutputPDF, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;
	CFont font;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CFileStatus fileStatus;
	if (CFile::GetStatus(rOutputPDF.outputPath.c_str(), fileStatus))	
		pDC->SetTextColor(BLACK);
	else
		pDC->SetTextColor(MIDDLEGRAY);

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	CString strOut;
	if (rOutputPDF.lastDate.wYear == 0)
		strOut = _T("");
	else
	{
		COleDateTime dateTime(rOutputPDF.lastDate);
		strOut = (dateTime != 0) ? CPrintSheetView::ConvertToString(dateTime, TRUE, TRUE) : _T("");
	}

	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CAutoServerDetailsView::DrawJobOutputLocation(CDC* pDC, CRect rcFrame, kimAutoDB_outputPDF& rOutputPDF, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;
	CFont font;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	CString strFullpath = rOutputPDF.outputPath.c_str();
	CFileStatus fileStatus;
	if (CFile::GetStatus(strFullpath, fileStatus))	
		pDC->SetTextColor(BLACK);
	else
		pDC->SetTextColor(MIDDLEGRAY);

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath((LPCTSTR)strFullpath, szDrive, szDir, szFname, szExt);
	CString strPath; strPath.Format(_T("%s\\%s"), szDrive, szDir);

	DrawTextMeasured(pDC, strPath, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	int nOffset = 100;
	rcText.OffsetRect(0, nOffset);

	font.DeleteObject();

	return rcBox;
}

void CAutoServerDetailsView::ResetDisplayColums()
{
	m_displayColumns.RemoveAll();
}

void CAutoServerDetailsView::AddDisplayColumn(int nColID, int nWidth)
{
	struct displayColumn dc = {nColID, nWidth};
	m_displayColumns.Add(dc);
}

int CAutoServerDetailsView::GetHeaderColumnPos(int nCol)
{
	int nPos = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
	{
		if (i == nCol)
			return nPos;
		nPos += m_headerColumnWidths[i];
	}
	return -1;
}

void CAutoServerDetailsView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_DisplayList.OnLButtonDown(point);

	CScrollView::OnLButtonDown(nFlags, point);
}

void CAutoServerDetailsView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
    OnLButtonDown(nFlags, point);	// Handle second click 

	CScrollView::OnLButtonDblClk(nFlags, point);
}

void CAutoServerDetailsView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list
	CPoint pt = point;
	ClientToScreen(&pt);
	OnContextMenu(NULL, pt);
}

void CAutoServerDetailsView::OnContextMenu(CWnd*, CPoint point)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int			  nMenuID = -1;
	CDisplayItem* pDI	  = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerDetailsView, BoundProduct));
	if (pDI)
	{
		nMenuID = IDR_PRODUCTSVIEW_POPUP;
	}
	//else
	//{
	//	pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerDetailsView, BoundProduct));
	//	if (pDI)
	//		nMenuID = ;
	//}

	if (nMenuID < 0)
		return;

    CMenu menu;
	VERIFY(menu.LoadMenu(nMenuID));

	CMenu* pPopup = menu.GetSubMenu(0);
	if ( ! pPopup)
		return;

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
}

void CAutoServerDetailsView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

void CAutoServerDetailsView::OnMouseLeave()
{
	m_DisplayList.OnMouseLeave();

	CScrollView::OnMouseLeave();
}

// This functions are overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling

void CAutoServerDetailsView::ScrollToPosition(POINT pt)    // logical coordinates
{
	ASSERT(m_nMapMode > 0);     // not allowed for shrink to fit
	if (m_nMapMode != MM_TEXT)
	{
		CWindowDC dc(NULL);
		dc.SetMapMode(m_nMapMode);
		dc.LPtoDP((LPPOINT)&pt);
	}

	// now in device coordinates - limit if out of range
	int xMax = GetScrollLimit(SB_HORZ);
	int yMax = GetScrollLimit(SB_VERT);
	if (pt.x < 0)
		pt.x = 0;
	else if (pt.x > xMax)
		pt.x = xMax;
	if (pt.y < 0)
		pt.y = 0;
	else if (pt.y > yMax)
		pt.y = yMax;

	ScrollToDevicePosition(pt);
}

void CAutoServerDetailsView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);
	
	CRect scrollRect, clipRect;
	GetClientRect(scrollRect); clipRect = scrollRect;
	clipRect.top += m_nHeaderHeight;	// Protect header control from being vertically scrolled
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, clipRect);

	if (m_HeaderCtrl.GetSafeHwnd())
		if (ptDev.x != 0)				// Scroll header control horizontally
			m_HeaderCtrl.MoveWindow(-ptDev.x, 0, scrollRect.Width() + ptDev.x, m_nHeaderHeight);

	//if (m_InplaceEdit.GetSafeHwnd())
	//	m_InplaceEdit.Deactivate();
}

BOOL CAutoServerDetailsView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect, clipRect;
		GetClientRect(scrollRect); clipRect = scrollRect;
		//if (m_HeaderCtrl.GetSafeHwnd())
			clipRect.top += m_nHeaderHeight;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, clipRect);

		if (m_HeaderCtrl.GetSafeHwnd())
			if (sizeScroll.cx != 0)				// Scroll header control horizontally
				m_HeaderCtrl.MoveWindow(-x, 0, scrollRect.Width() + x, m_nHeaderHeight);

		//if (m_InplaceEdit.GetSafeHwnd())
		//	m_InplaceEdit.Deactivate();
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}


void CAutoServerDetailsView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 20)
		hdn->pitem->cxy = 20;
}

void CAutoServerDetailsView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	SetHeaderColumnWidth(hdn->iItem, hdn->pitem->cxy);

	if (m_headerColumnWidths.GetSize() > hdn->iItem)
		m_headerColumnWidths[hdn->iItem] = hdn->pitem->cxy; 

	m_nHeaderItemsTotalWidth = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
		m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];

	CRect clientRect;
	GetClientRect(&clientRect);

	InvalidateRect(clientRect);
	UpdateWindow(); 

	CAutoServerToolsView* pView = theApp.GetAutoServerToolsView();
	if (pView)
		pView->SendMessage(WM_SIZE);
}

void CAutoServerDetailsView::SetHeaderColumnWidth(int nIndex, int nValue)
{
	if (nIndex >= 20)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\AutoServerDetailsViewHeader");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			columnWidthArray[nIndex] = nValue;			
		else
		{
			ZeroMemory(columnWidthArray, sizeof(columnWidthArray));
			columnWidthArray[nIndex] = nValue;			
		}

		dwSize = sizeof(columnWidthArray);
		RegSetValueEx(hKey, _T("Columns"), 0, REG_BINARY, (LPBYTE)&columnWidthArray, dwSize);

		RegCloseKey(hKey);
	}
}

int CAutoServerDetailsView::GetHeaderColumnWidth(int nIndex, int nDefault)
{
	if (nIndex >= 20)
		return nDefault;

	int nWidth = nDefault;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\AutoServerDetailsViewHeader");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			nWidth = columnWidthArray[nIndex];			
		else
			nWidth = nDefault;

		RegCloseKey(hKey);
	}

	if ( (nWidth <= 10) || (nWidth > 1000) )
		nWidth = nDefault;

	return nWidth;
}

BOOL CAutoServerDetailsView::OnActivateJobInputFile(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	pDI->m_nState = CDisplayItem::Normal;

	kimAutoDB::_inputPDF inputPDF;
	theApp.m_kaDB.ka_GetInputPDF(&inputPDF, (int)pDI->m_pData);
	if (inputPDF.name.empty())
		return FALSE;

	CString strFullpath = inputPDF.path.c_str() + CString("\\") + inputPDF.name.c_str();
	CPageListFrame::LaunchAcrobat(strFullpath, -1);

	return TRUE;
}

BOOL CAutoServerDetailsView::OnMouseMoveJobInputFile(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				COLORREF crHighlightColor = RGB(165,175,200);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, pDI->m_BoundingRect, crHighlightColor, TRUE);	
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CAutoServerDetailsView::OnActivateJobOutputFile(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	pDI->m_nState = CDisplayItem::Normal;

	kimAutoDB_outputPDF outputPDF;
	theApp.m_kaDB.ka_GetOutputPDF(&outputPDF, (int)pDI->m_pData);
	if (outputPDF.name.empty())
		return FALSE;

	CString strFullpath = outputPDF.outputPath.c_str();
	CString strExtension = PathFindExtension(strFullpath);
	strExtension.MakeLower();
	if (strExtension == _T(".pdf"))
		CPageListFrame::LaunchAcrobat(strFullpath, -1);
	else
		if (strExtension == _T(".jdf"))
			ShellExecute(NULL, _T("open"), strFullpath, NULL, NULL, SW_SHOWNORMAL);	

	return TRUE;
}

BOOL CAutoServerDetailsView::OnMouseMoveJobOutputFile(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				COLORREF crHighlightColor = RGB(165,175,200);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, pDI->m_BoundingRect, crHighlightColor, TRUE);	
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}
