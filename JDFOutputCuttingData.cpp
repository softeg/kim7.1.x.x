// JDFOutputCuttingData.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "JDFOutputCuttingData.h"
#include "JDFOutputImposition.h"
#include "MainFrm.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "DlgConfirmPDFSheetOverwrite.h"
#include "spromeps.h"
#include "DlgDemoOrRegisterDongle.h"
#include "DlgStopOutput.h"
#include <Shlwapi.h>
#include <locale.h>
#include "XMLCommon.h"


// CJDFOutputCuttingData

CJDFOutputCuttingData::CJDFOutputCuttingData(CPDFTargetProperties& rPDFTarget)
{
	m_nTargetType		  			= rPDFTarget.m_nType;
	m_strTargetName		  			= rPDFTarget.m_strTargetName;
	m_strLocation		  			= rPDFTarget.m_strLocation;
	m_strFilenameTemplate 			= rPDFTarget.m_strOutputFilename;
	m_fMediaWidth		  			= 0.0f;
	m_fMediaHeight		  			= 0.0f;
	m_nError			  			= 0;
	m_bCancelJob		  			= FALSE;
	m_media				  			= rPDFTarget.m_mediaList[0];
	m_nCompatibility	  			= rPDFTarget.m_nJDFCompatibility;
	m_bOutputFormatExceeded 		= FALSE;

	setlocale(LC_NUMERIC, "english");	// because when german setted, Format() does produce numbers with Comma like "0,138"
}

CJDFOutputCuttingData::~CJDFOutputCuttingData()
{
	CString strLanguage;
	switch (theApp.settings.m_iLanguage)
	{
		case 0: strLanguage = _T("GER"); setlocale(LC_ALL, "german");	break;
		case 1: strLanguage = _T("ENG"); setlocale(LC_ALL, "english");	break;
		case 2: strLanguage = _T("FRA"); break;
		case 3: strLanguage = _T("ITA"); break;
		case 4: strLanguage = _T("SPA"); break;
		case 5: strLanguage = _T("HOL"); break;	// No holl. MFC4x.dll available
		//Default would already be set at reading ini-file
	}
}

// CJDFOutputCuttingData-Memberfunktionen


BOOL CJDFOutputCuttingData::Open(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return FALSE; 

	m_strTmpOutputJDFPath = pPrintSheet->GetOutFileTmpFullPath(BOTHSIDES);
	m_strOutputJDFPath	  = pPrintSheet->GetCuttingDataOutFileFullPath();
	m_strOutputFilename	  = pPrintSheet->GetCuttingDataOutFilename();

	//CPrintSheetTableView*  pPrintSheetTableView  = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	//if (pPrintSheetTableView)
	//	pPrintSheetTableView->HighlightPDFOutFile(pPrintSheet, BOTHSIDES, pPlate);

	CFileStatus	fileStatus;
	if ( (bOverwriteAll == -1) || ! bOverwriteAll)
	{
		BOOL bFileExists = FALSE;
		if (CFile::GetStatus(m_strOutputJDFPath, fileStatus))	// target file exists
			bFileExists = TRUE;

		if (bFileExists)
		{
			int nRet = IDC_USE_EXISTING;
			if (bOverwriteAll == -1)
			{
				CDlgConfirmPDFSheetOverwrite dlg;
				dlg.m_strPDFFilename = m_strOutputJDFPath;
				nRet = dlg.DoModal();
			}
			switch (nRet)
			{
			case IDC_NEW:			bOverwriteAll = TRUE;
									break;
			case IDC_USE_EXISTING:	bOverwriteAll = FALSE;
									//if (pPrintSheetTableView)
									//	pPrintSheetTableView->StopPDFOutFeedback();
									m_bCancelJob = TRUE;
									return FALSE;
			case IDCANCEL:			m_bCancelJob = TRUE;
									//if (pPrintSheetTableView)
									//	pPrintSheetTableView->StopPDFOutFeedback();
									return FALSE;
			}
		}
	}


	// setup indentation for the output - 2 blanks/level
//	XMLIndent(2,true);

	m_pJDFDoc = new JDFDoc(0);
	JDFNode root = m_pJDFDoc->GetRoot();

	root.init();
	if (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy2215)
		root.SetVersion(_T("1.0"));
	else
		if (m_nCompatibility != CPDFTargetProperties::JDFComp13)
			root.SetVersion(_T("1.2"));

	CString	strJobTitle = theApp.RemoveFileExtension(pDoc->GetTitle(), _T(".job") ); 

	root.SetMaxVersion(_T("1.3"));
	root.SetICSVersions(vWString(_T("Base_L0-1.3 LayCrImp_L1-1.3")));

	//root.SetTypes(vWString(_T("Imposition Cutting")));	//doesn't pass CheckJDF

	switch (m_nCompatibility)
	{
	case CPDFTargetProperties::JDFCompPrinergy2215:		
		root.SetAttribute(_T("xmlns:CPC"), _T("http://www.creo.com/CPC/JDFExtensions.xsd"));
		root.SetAttribute(_T("xmlns:SSi"), _T("//SSiJDFExtensions.xsd"));
		break;
	case CPDFTargetProperties::JDFCompPrinergy23:		
		root.SetAttribute(_T("xmlns"), _T("http://www.CIP4.org/JDFSchema_1_1"));
		root.SetAttribute(_T("xmlns:SSi"), _T("//SSiJDFExtensions.xsd"));
		break;
	case CPDFTargetProperties::JDFComp12:		
		root.SetAttribute(_T("xmlns"), _T("http://www.CIP4.org/JDFSchema_1_1"));
		break;
	}

	//root.SetEnumType(JDFNode::Type_Product);//JDFNode::Type_ProcessGroup);

	//JDFComponent component = root.AppendMatchingResource(JDFElement::elm_Component, JDFNode::ProcessUsage_AnyOutput);
	//component.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	//component.SetComponentType(JDFComponent::ComponentType_FinalProduct);

	root.SetEnumType(JDFNode::Type_ProcessGroup);

	m_cuttingGroup	 = root.AddJDFNode(JDF::JDFNode::Type_Cutting);
	m_cuttingGroup.SetDescriptiveName(_T("Cutting"));
	m_cuttingGroup.SetID(_T("ProcessCutting"));
	m_componentSheet = m_cuttingGroup.AppendMatchingResource(JDFElement::elm_Component, JDFNode::ProcessUsage_AnyInput);
	m_componentSheet.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	m_componentSheet.SetComponentType(JDFComponent::ComponentType_Sheet);
	m_componentSheet.setPartIDKeys(vWString(_T("SignatureName SheetName")));
	JDFComponent compBlock = m_cuttingGroup.AppendMatchingResource(JDFElement::elm_Component, JDFNode::ProcessUsage_AnyOutput);
	compBlock.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	compBlock.SetComponentType(JDFComponent::ComponentType_Block);

	m_cuttingParams = m_cuttingGroup.AppendMatchingResource(JDFElement::elm_CuttingParams, JDFNode::ProcessUsage_AnyInput);
	m_cuttingParams.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	m_cuttingParams.SetStatus(JDF::JDFResource::Status_Available);
	m_cuttingParams.setPartIDKeys(vWString(_T("SignatureName SheetName")));

	JDFAuditPool auditPool=root.GetAuditPool();
	mAttribute attMap;
	JDFAudit audit = auditPool.GetAudit(0, JDFAudit::AuditType_Created, attMap);

	CString strKIMTitle  = theApp.BuildKIMTitle(theApp.strName);
	CString strAgentName = strKIMTitle;
	CString strAgentVersion; strAgentVersion.Format(_T("%s (Build: %s)"), theApp.strVersion, CString(__DATE__));
	switch (m_nCompatibility)
	{
	case CPDFTargetProperties::JDFCompPrinergy2215:		
		{
		CString strAuthor = _T("Creo Preps Version 4.2.1  (407)");
		audit.SetAuthor((KString)strAuthor);
		}
		break;
	case CPDFTargetProperties::JDFCompPrinergy23:		
		{
		CString strAuthor;	strAuthor.Format(_T("%s  %s (Build: %s)"), strKIMTitle, theApp.strVersion, CString(__DATE__));
		audit.SetAuthor((WString)strAuthor);
		}
		break;
	case CPDFTargetProperties::JDFComp12:		
	case CPDFTargetProperties::JDFComp13:		
		audit.SetAgentName((WString)strAgentName);
		audit.SetAgentVersion((WString)strAgentVersion);
		break;
	}

	auditPool = m_cuttingGroup.AppendAuditPool();
	audit	  = auditPool.AddCreated();
	audit.SetAgentName((WString)strAgentName);
	audit.SetAgentVersion((WString)strAgentVersion);
	
	return TRUE;
}

void CJDFOutputCuttingData::Close()
{
	CPrintSheetTableView*  pPrintSheetTableView  = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pPrintSheetTableView)
	{
		pPrintSheetTableView->StopPDFOutFeedback();
		pPrintSheetTableView->m_strPDFOutputMessage.LoadString(IDS_CLOSE_PDF_OUTPUT);
		pPrintSheetTableView->UpdateData(FALSE);
	}

	if (pPrintSheetTableView)
		pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_SHOW);

	//SetProgressFunction( ProgressClose, (long)pPrintSheetTableView);
	if (pPrintSheetTableView)
		pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_HIDE);

	if (pPrintSheetTableView)
		pPrintSheetTableView->LowlightPDFOutFile();

	try
	{
		m_pJDFDoc->Write2URL((WString)WindowsPath2URL(m_strOutputJDFPath));
	}
	catch (JDF::JDFException e)
	{
		CString strMsg;
		strMsg.Format(_T("JDF API error: %s"), e.getMessage().getBytes());
		KIMOpenLogMessage(strMsg, MB_ICONERROR);
	}

	if (pPrintSheetTableView)
	{
		pPrintSheetTableView->m_strPDFOutputMessage  = "";
		pPrintSheetTableView->UpdateData(FALSE);
	}

	delete m_pJDFDoc;
}

void CJDFOutputCuttingData::JDFOUT_AppendCuttingParams(CPrintSheet* pPrintSheet)
{
	if ( ! pPrintSheet)		
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;

	CString strID;
	strID.Format(_T("Signature %s"), pPrintSheet->GetNumber());
	JDFComponent compSignature = m_componentSheet.AddPartition(JDF::JDFResource::PartIDKey_SignatureName, (WString)strID);  
	strID.Format(_T("Sheet %s"), pPrintSheet->GetNumber());
	JDFComponent compSheet = compSignature.AddPartition(JDF::JDFResource::PartIDKey_SheetName, (WString)strID);
	compSheet.SetDimensions(JDFShape(pLayout->m_FrontSide.m_fPaperWidth / fPSPoint, pLayout->m_FrontSide.m_fPaperHeight / fPSPoint, 0.0f));

	strID.Format(_T("Signature %s"), pPrintSheet->GetNumber());
	JDFCuttingParams cuttingParamsSignature = m_cuttingParams.AddPartition(JDF::JDFResource::PartIDKey_SignatureName, (WString)strID);  
	strID.Format(_T("Sheet %s"), pPrintSheet->GetNumber());
	JDFCuttingParams cuttingParamsSheet = cuttingParamsSignature.AddPartition(JDF::JDFResource::PartIDKey_SheetName, (WString)strID);

	if (theApp.m_nAddOnsLicensed & CImpManApp::JDFCutting)
	{
		POSITION pos	   = pLayout->m_FrontSide.m_cutBlocks.GetHeadPosition();
		int		 nBlockNum = 1;
		while (pos)
		{
			int nNX, nNY;
			float fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight; 

			CLayoutObject& rCutBlockObject = pLayout->m_FrontSide.m_cutBlocks.GetNext(pos);
			rCutBlockObject.GetCurrentGrossCutBlock(pPrintSheet, fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight);
			rCutBlockObject.GetCutblockMatrix(nNX, nNY);

			JDFCutBlock cutBlock = cuttingParamsSheet.AppendCutBlock();
			cutBlock.SetBlockType(JDF::JDFAutoCutBlock::BlockType_CutBlock);
			strID.Format(_T("B %d"), nBlockNum++);
			cutBlock.SetBlockName((WString)strID);
			float fBlockSizeX = fCropBoxWidth  / nNX / fPSPoint;
			float fBlockSizeY = fCropBoxHeight / nNY / fPSPoint;
			cutBlock.SetBlockSize(JDFXYPair(fBlockSizeX, fBlockSizeY));
			cutBlock.SetBlockTrf(JDFMatrix(1.0f, 0.0f, 0.0f, 1.0f, fCropBoxX / fPSPoint, fCropBoxY / fPSPoint));
			cutBlock.SetBlockSubdivision(JDFXYPair((double)nNX, (double)nNY));
		}
	}
}

BOOL CJDFOutputCuttingData::ProcessPrintSheet(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll) 
{
	if (!pPrintSheet)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return TRUE;

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(pPrintSheet->m_strPDFTarget);
	if ( ! pTargetProps)
		return TRUE;

	int nRet = TRUE;
	switch (pPrintSheet->GetOutfileRelation(pTargetProps->m_strOutputFilename))
	{
	case CPrintSheet::SheetPerFile: 
	case CPrintSheet::SidePerFile:	
	case CPrintSheet::PlatePerFile:	nRet = ProcessPrintSheetAllInOne(pPrintSheet, bOverwriteAll);	break;
	}

	return nRet;
}

BOOL CJDFOutputCuttingData::ProcessPrintSheetsJobInOne(CPrintSheetList* pPrintSheetList, BOOL& bOverwriteAll, CPrintSheet** pCurPrintSheet) 
{
	CPrintSheet* pPrintSheet = &pPrintSheetList->GetHead();
	if (pCurPrintSheet)
		pPrintSheet = (*pCurPrintSheet) ? *pCurPrintSheet : &pPrintSheetList->GetHead();
	if ( ! pPrintSheet)
		return m_bCancelJob;

	CPrintSheet* pStartPrintSheet = pPrintSheet;
	if ( ! Open(pPrintSheet, bOverwriteAll))
		return m_bCancelJob;

//	JDFOUT_AppendCuttingParams();

	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));

	POSITION pos = pPrintSheetList->GetHeadPosition();
	if (pCurPrintSheet)
		pos = (*pCurPrintSheet) ? pPrintSheetList->GetPos(**pCurPrintSheet) : pPrintSheetList->GetHeadPosition();
	while (pos && ! m_bCancelJob)
	{
		CPrintSheet& rPrintSheet = pPrintSheetList->GetNext(pos);
		if (pCurPrintSheet)
			*pCurPrintSheet = &rPrintSheet;

		if (rPrintSheet.IsOutputDisabled())
			continue;

		if (CheckTargetProps(rPrintSheet))
		{
			m_bCancelJob = 1;	// abort current sheet
			pos = pPrintSheetList->GetPos(rPrintSheet);
			pPrintSheetList->GetPrev(pos);			// go back to previous in order to let calling function resume with this sheet
			if (pos)
				if (pCurPrintSheet)
					*pCurPrintSheet = &pPrintSheetList->GetAt(pos);	
			break;
		}
		
		JDFOUT_AppendCuttingParams(&rPrintSheet);
	}

	Close();

	return m_bCancelJob;
}

BOOL CJDFOutputCuttingData::ProcessPrintSheetAllInOne(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll) 
{
	if (Open(pPrintSheet, bOverwriteAll))
	{
		JDFOUT_AppendCuttingParams(pPrintSheet);
		Close();
	}
	return m_bCancelJob;
}

BOOL CJDFOutputCuttingData::CheckTargetProps(CPrintSheet& rPrintSheet)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
	if ( ! pTargetProps)
		return TRUE;

	if (m_nTargetType		  != pTargetProps->m_nType)
		return TRUE;
	if (m_strFilenameTemplate != pTargetProps->m_strOutputFilename)
		return TRUE;
	if (m_strLocation		  != pTargetProps->m_strLocation)
		return TRUE;

	m_media	= pTargetProps->m_mediaList[0];

	return FALSE;
}
