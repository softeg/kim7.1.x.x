// ImpManDocExpTemplates.cpp : implementation of the CImpManDoc subclasses CPageTemplateList and CMarkTemplateList
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PageListView.h"
#include "DlgTrimboxWarning.h"
#include "InplaceEdit.h"	// needed in 'PrintSheetView.h'
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "PageListTableView.h"
#include "GraphicComponent.h"
#include "PrintSheetPlanningView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


////////////////////////////// CPageTemplateProductRefs ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CPageTemplateProductRefs

CPageTemplatePosList::CPageTemplatePosList()
{
}

CPageTemplatePosList::CPageTemplatePosList(CPageTemplatePosList& rPosList)
{
	RemoveAll();
	Append(rPosList);
}

const CPageTemplatePosList& CPageTemplatePosList::operator=(const CPageTemplatePosList& rPosList)
{
	RemoveAll();
	Append(rPosList);
	return *this;
}

void CPageTemplateProductRefs::Initialize(CPageTemplateList* pPageTemplateList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if ( ! pPageTemplateList)
		pPageTemplateList = &pDoc->m_PageTemplateList;

	m_products.RemoveAll();
	m_productParts.RemoveAll();

	for (int nProductIndex = 0; nProductIndex < pDoc->m_boundProducts.GetSize(); nProductIndex++)
	{
		CPageTemplatePosList posList;
		POSITION pos = pPageTemplateList->CList::GetHeadPosition();
		while (pos)
		{
			CPageTemplate& rTemplate = pPageTemplateList->CList::GetAt(pos);
			if (rTemplate.m_nProductIndex == nProductIndex)
				posList.Add(pos);

			pPageTemplateList->CList::GetNext(pos);
		}
		m_products.Add(posList);
	}

	int nNumPartsTotal = pDoc->m_boundProducts.GetNumPartsTotal();
	for (int nProductPartIndex = 0; nProductPartIndex < nNumPartsTotal; nProductPartIndex++)
	{
		CPageTemplatePosList posList;
		POSITION pos = pPageTemplateList->CList::GetHeadPosition();
		while (pos)
		{
			CPageTemplate& rTemplate = pPageTemplateList->CList::GetAt(pos);
			if (rTemplate.m_nProductPartIndex == nProductPartIndex)
				posList.Add(pos);

			pPageTemplateList->CList::GetNext(pos);
		}
		m_productParts.Add(posList);
	}

	if (pDoc->m_flatProducts.GetSize() > 0)
	{
		int nProductPartIndex = pDoc->GetFlatProductsIndex();
		CPageTemplatePosList posList;
		POSITION pos = pPageTemplateList->CList::GetHeadPosition();
		while (pos)
		{
			CPageTemplate& rTemplate = pPageTemplateList->CList::GetAt(pos);
			if (rTemplate.m_nProductPartIndex == nProductPartIndex)
				posList.Add(pos);

			pPageTemplateList->CList::GetNext(pos);
		}
		m_productParts.Add(posList);
	}

	Validate();
}

POSITION CPageTemplateProductRefs::FindIndex(int nIndex, int nProductPartIndex)
{
	if (nProductPartIndex < 0)
		return FindIndexProduct(nIndex, abs(nProductPartIndex) - 1);
	else
		return FindIndexProductPart(nIndex, nProductPartIndex);
}

POSITION CPageTemplateProductRefs::FindIndexProduct(int nIndex, int nProductIndex)
{
	if (nIndex < 0)
		return NULL;
	if ( (nProductIndex < 0) || (nProductIndex >= m_products.GetSize()) )
		return NULL;
	CPageTemplatePosList& rPosList = m_products[nProductIndex];
	if (nIndex >= rPosList.GetSize())
		return NULL;
	else
		return rPosList[nIndex];
}

POSITION CPageTemplateProductRefs::FindIndexProductPart(int nIndex, int nProductPartIndex)
{
	if (nIndex < 0)
		return NULL;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= m_productParts.GetSize()) )
		return NULL;
	CPageTemplatePosList& rPosList = m_productParts[nProductPartIndex];
	if (nIndex >= rPosList.GetSize())
		return NULL;
	else
		return rPosList[nIndex];
}
//
//short CPageTemplateProductRefs::GetIndex(CPageTemplate* pPageTemplate, int nProductPartIndex)
//{
//	if (nProductPartIndex < 0)
//		return GetIndexProduct(pPageTemplate, abs(nProductPartIndex) - 1);
//	else
//		return GetIndexProductPart(pPageTemplate, nProductPartIndex);
//}
//
//short CPageTemplateProductRefs::GetIndexProduct(CPageTemplate* pPageTemplate, int nProductIndex)
//{
//	if (nIndex < 0)
//		return NULL;
//	if ( (nProductIndex < 0) || (nProductIndex >= m_products.GetSize()) )
//		return NULL;
//	CPageTemplatePosList& rPosList = m_products[nProductIndex];
//	if (nIndex >= rPosList.GetSize())
//		return NULL;
//	else
//		return rPosList[nIndex];
//}
//
//short CPageTemplateProductRefs::GetIndexProductPart(CPageTemplate* pPageTemplate, int nProductPartIndex)
//{
//	if (nIndex < 0)
//		return NULL;
//	if ( (nProductPartIndex < 0) || (nProductPartIndex >= m_productParts.GetSize()) )
//		return NULL;
//	CPageTemplatePosList& rPosList = m_productParts[nProductPartIndex];
//	if (nIndex >= rPosList.GetSize())
//		return NULL;
//	else
//		return rPosList[nIndex];
//}


////////////////////////////// CPageTemplateList ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CPageTemplateList

IMPLEMENT_SERIAL(CPageTemplateList, CObject, VERSIONABLE_SCHEMA | 1)

CPageTemplateList::CPageTemplateList()
{
}

POSITION CPageTemplateList::FindIndex(INT_PTR nIndex, int nProductPartIndex)
{
	if (nProductPartIndex == ALL_PARTS)
		return CList::FindIndex(nIndex);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if (this != &pDoc->m_MarkTemplateList)
	{
		if (m_productRefs.IsInvalid())
			m_productRefs.Initialize(this);
		return m_productRefs.FindIndex(nIndex, nProductPartIndex);
	}
	else
	{
		if (nIndex >= m_nCount || nIndex < 0)
			return NULL;  // went too far

		POSITION pos = CList::GetHeadPosition();
		while (pos)
		{
			CPageTemplate& rTemplate = CList::GetAt(pos);
			if (rTemplate.IsPartOfBoundProduct(nProductPartIndex, 0))
				nIndex--;
			if (nIndex < 0)
				return pos;
			CList::GetNext(pos);
		}
		return NULL;
	}
	return NULL;
}

POSITION CPageTemplateList::FindLabelPos(int nLabelIndex, int nSide)
{
	int nIndex = (nSide == FRONTSIDE) ? 2 * nLabelIndex : 2 * nLabelIndex + 1;
	POSITION pos = CList::GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetAt(pos);
		if (rTemplate.IsFlatProduct())
			nIndex--;
		if (nIndex < 0)
			break;
		CList::GetNext(pos);
	}
	return pos;
}

CPageTemplate* CPageTemplateList::FindFlatProductTemplate(int nLabelIndex, int nSide)
{
	POSITION pos = FindLabelPos(nLabelIndex, nSide);
	if (pos)
		return &GetAt(pos);
	else
		return NULL;
}

POSITION CPageTemplateList::GetHeadPosition(int nProductPartIndex)
{ 
	if (nProductPartIndex == ALL_PARTS)
		return CList::GetHeadPosition();

	POSITION pos = CList::GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = GetAt(pos);
		if (rTemplate.IsPartOfBoundProduct(nProductPartIndex, 1))
			break;
		CList::GetNext(pos);
	}
	return pos;
}

POSITION CPageTemplateList::GetTailPosition(int nProductPartIndex)
{ 
	if (nProductPartIndex == ALL_PARTS)
		return CList::GetTailPosition();

	POSITION pos = CList::GetTailPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = GetAt(pos);
		if (rTemplate.IsPartOfBoundProduct(nProductPartIndex, 2))
			break;
		CList::GetPrev(pos);
	}
	return pos;
}

CPageTemplate& CPageTemplateList::GetHead(int nProductPartIndex)
{ 
	if (nProductPartIndex == ALL_PARTS)
		return CList::GetHead();

	POSITION pos = CList::GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = GetAt(pos);
		if (rTemplate.IsPartOfBoundProduct(nProductPartIndex, 3))
			break;
		CList::GetNext(pos);
	}
	if (pos)
		return CList::GetAt(pos);
	else
	{
		static private CPageTemplate nullPageTemplate;
		return nullPageTemplate;
	}
}

CPageTemplate& CPageTemplateList::GetTail(int nProductPartIndex)
{ 
	if (nProductPartIndex == ALL_PARTS)
		return CList::GetTail();

	POSITION pos = CList::GetTailPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = GetAt(pos);
		if (rTemplate.IsPartOfBoundProduct(nProductPartIndex, 4))
			break;
		CList::GetPrev(pos);
	}
	if (pos)
		return CList::GetAt(pos);
	else
	{
		static private CPageTemplate nullPageTemplate;
		return nullPageTemplate;
	}
}

CPageTemplate& CPageTemplateList::GetNext(POSITION& rPosition, int nProductPartIndex)
{ 
	if (nProductPartIndex == ALL_PARTS)
	{
		CNode* pNode = (CNode*) rPosition;
		rPosition = (POSITION) pNode->pNext;
		return pNode->data;
		//return CList::GetNext(rPosition);
	}

	POSITION pos = rPosition;
	
	CList::GetNext(rPosition);
	while (rPosition)
	{
		CPageTemplate& rTemplate = GetAt(rPosition);
		if (rTemplate.IsPartOfBoundProduct(nProductPartIndex, 5))
			break;
		CList::GetNext(rPosition);
	}
	if (pos)
		return CList::GetAt(pos);
	else
	{
		static private CPageTemplate nullPageTemplate;
		return nullPageTemplate;
	}
}

CPageTemplate& CPageTemplateList::GetPrev(POSITION& rPosition, int nProductPartIndex)
{ 
	if (nProductPartIndex == ALL_PARTS)
		return CList::GetPrev(rPosition);

	POSITION pos = rPosition;
	
	CList::GetPrev(rPosition);
	while (rPosition)
	{
		CPageTemplate& rTemplate = GetAt(rPosition);
		if (rTemplate.IsPartOfBoundProduct(nProductPartIndex, 6))
			break;
		CList::GetPrev(rPosition);
	}
	if (pos)
		return CList::GetAt(pos);
	else
	{
		static private CPageTemplate nullPageTemplate;
		return nullPageTemplate;
	}
}

short CPageTemplateList::GetIndex(CPageTemplate* pPageTemplate, int nProductPartIndex)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition(nProductPartIndex);
	while (pos)
	{
		if (pPageTemplate == &GetNext(pos, nProductPartIndex))
			return nIndex;
		else
			nIndex++;
	}
	return -1;
}

INT_PTR CPageTemplateList::GetCount(int nProductPartIndex)
{
	if (nProductPartIndex == ALL_PARTS)
		return CList::GetCount();

	int		 nCount = 0;
	POSITION pos	= GetHeadPosition(nProductPartIndex);
	while (pos)
	{
		GetNext(pos, nProductPartIndex);
		nCount++;
	}

	return nCount;
}

POSITION CPageTemplateList::AddHead(CPageTemplate& rPageTemplate, int nProductPartIndex)
{
	POSITION pos = NULL;
	if (nProductPartIndex == ALL_PARTS)
	{
		pos = CList::AddHead(rPageTemplate);
		m_productRefs.Invalidate();
		return pos;
	}

	pos = GetHeadPosition(nProductPartIndex);
	if (pos)
		pos = CList::InsertBefore(pos, rPageTemplate);
	else
		pos = CList::AddHead(rPageTemplate);

	m_productRefs.Invalidate();

	return pos;
}

POSITION CPageTemplateList::AddTail(CPageTemplate& rPageTemplate, int nProductPartIndex)
{
	POSITION pos = NULL;
	if (nProductPartIndex == ALL_PARTS)
	{
		pos = CList::AddTail(rPageTemplate);
		m_productRefs.Invalidate();
		return pos;
	}

	pos = GetTailPosition(nProductPartIndex);
	if (pos)
		pos = CList::InsertAfter(pos, rPageTemplate);
	else
		pos = CList::AddTail(rPageTemplate);

	m_productRefs.Invalidate();

	return pos;
}

POSITION CPageTemplateList::InsertBefore(POSITION position, CPageTemplate& rPageTemplate)
{
	POSITION pos = NULL;

	pos = CList::InsertBefore(position, rPageTemplate);

	m_productRefs.Invalidate();

	return pos;
}

POSITION CPageTemplateList::InsertAfter(POSITION position, CPageTemplate& rPageTemplate)
{
	POSITION pos = NULL;

	pos = CList::InsertAfter(position, rPageTemplate);

	m_productRefs.Invalidate();

	return pos;
}

void CPageTemplateList::RemoveTail(int nProductPartIndex)
{
	if (nProductPartIndex == ALL_PARTS)
	{
		CList::RemoveTail();
		m_productRefs.Invalidate();
		return;
	}

	POSITION pos = GetTailPosition(nProductPartIndex);
	if (pos)
	{
		RemoveAt(pos);
		m_productRefs.Invalidate();

	}
}


// Initialize links to printsheets, which means that we determine 
// the appropriate indices for printsheet, layoutside, and layout object index
void CPageTemplateList::FindPrintSheetLocations()
{
	if ( ! GetCount())
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	BOOL bIsPageTemplateList = (this == &pDoc->m_PageTemplateList) ? TRUE : FALSE;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);	
		rPageTemplate.m_PrintSheetLocations.RemoveAll();	// set all pages unassigned
	}

	short		nPrintSheetIndex = 0;
	pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		POSITION layoutPos = pDoc->m_PrintSheetList.m_Layouts.FindIndex(rPrintSheet.m_nLayoutIndex);
		if ( ! layoutPos)
			continue;

		CLayout& rLayout			= pDoc->m_PrintSheetList.m_Layouts.GetAt(layoutPos);
		short	 nLayoutObjectIndex = 0;
		POSITION objectPos			= rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CPageTemplate* pTemplate = NULL;
			CLayoutObject& rObject	 = rLayout.m_FrontSide.m_ObjectList.GetNext(objectPos);

			if ( (rObject.m_nType == CLayoutObject::Page) && bIsPageTemplateList)
				pTemplate = rObject.GetCurrentPageTemplate(&rPrintSheet);
			else
				if ( (rObject.m_nType == CLayoutObject::ControlMark) && ! bIsPageTemplateList)
					pTemplate = rObject.GetCurrentMarkTemplate();

			if (pTemplate)
				//if (pTemplate->IsMemberOf(this))
				{
					CPrintSheetLocation locBuf;
					locBuf.m_nPrintSheetIndex	= nPrintSheetIndex;
					locBuf.m_nLayoutSide		= FRONTSIDE;
					locBuf.m_nLayoutObjectIndex = nLayoutObjectIndex;
					pTemplate->m_PrintSheetLocations.Add(locBuf);
				}
			nLayoutObjectIndex++;
		}

		nLayoutObjectIndex = 0;
		objectPos = rLayout.m_BackSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CPageTemplate* pTemplate = NULL;
			CLayoutObject& rObject	 = rLayout.m_BackSide.m_ObjectList.GetNext(objectPos);

			if ( (rObject.m_nType == CLayoutObject::Page) && bIsPageTemplateList)
				pTemplate = rObject.GetCurrentPageTemplate(&rPrintSheet);
			else
				if ( (rObject.m_nType == CLayoutObject::ControlMark) && ! bIsPageTemplateList)
					pTemplate = rObject.GetCurrentMarkTemplate();

			if (pTemplate)
				//if (pTemplate->IsMemberOf(this))
				{
					CPrintSheetLocation locBuf;
					locBuf.m_nPrintSheetIndex	= nPrintSheetIndex;
					locBuf.m_nLayoutSide		= BACKSIDE;
					locBuf.m_nLayoutObjectIndex = nLayoutObjectIndex;
					pTemplate->m_PrintSheetLocations.Add(locBuf);
				}
			nLayoutObjectIndex++;
		}

		nPrintSheetIndex++;
	}
}

void CPageTemplateList::RemoveUnusedTemplates()
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		POSITION	   prevPos		 = pos;
		CPageTemplate& rPageTemplate = CList::GetNext(pos);	
		BOOL		   bUnused		 = FALSE;
		if (rPageTemplate.m_PrintSheetLocations.GetSize() == 0)
			bUnused = TRUE;
		else
		{ 
			bUnused = TRUE;
			for (int i = 0; i < rPageTemplate.m_PrintSheetLocations.GetSize(); i++)
			{
				if (rPageTemplate.m_PrintSheetLocations[i].GetLayoutObject())
				{
					bUnused = FALSE;
					break;
				}
			}
		}
		if (bUnused)
		{
			RemoveAt(prevPos);
			CImpManDoc::GetDoc()->m_PrintSheetList.m_Layouts.RearrangeMarkTemplateIndices(nIndex);
		}
		else
			nIndex++;
	}
}

void CPageTemplateList::RemoveProductGroupTemplates(int nProductPartIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		POSITION	   prevPos		 = pos;
		CPageTemplate& rPageTemplate = CList::GetNext(pos);	
		if (rPageTemplate.m_nProductPartIndex == nProductPartIndex)
			RemoveAt(prevPos);
	}
}


// If nPages differs from lenght of template list add or remove the appropriate number of templates
void CPageTemplateList::AdjustNumPages(int nPages, int nProductIndex, int nProductPartIndex)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

//	int nProductIndex = pDoc->GetBoundProductPart(nProductPartIndex).GetBoundProduct().GetIndex();
	
	int nPageNum = 1;
	if (GetCount(nProductPartIndex) > 0)
	{
		CPageTemplate& rLastPageTemplate = GetTail(nProductPartIndex);
		if (rLastPageTemplate.PageNumHasAlpha())
			nPageNum = GetIndex(&rLastPageTemplate, nProductPartIndex) + 1;
		else
			nPageNum = (long)Ascii2Float(rLastPageTemplate.m_strPageID) + 1;
	}

	while (GetCount(nProductPartIndex) < nPages)
	{
		CPageTemplate newTemplate;
		CString		  strID;
		strID.Format(_T("%d"), nPageNum++);
		newTemplate.Create(strID, CPageTemplate::SinglePage, 0.0f, 0.0f, nProductIndex, nProductPartIndex);
		AddTail(newTemplate, -nProductIndex - 1);
	}

	while (GetCount(nProductPartIndex) > nPages)
		RemoveTail(-nProductIndex - 1);
}

void CPageTemplateList::InsertProductPartPages(int nPages, int nProductIndex, int nProductPartIndex)
{
	if (nPages <= 0)
		return;
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBoundProduct&		rBoundProduct	= pDoc->m_boundProducts.GetProduct(nProductIndex);
	CBoundProductPart&	rProductPart	= pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
	BOOL				bHasCover		= rBoundProduct.HasCover();
	BOOL				bThisIsCover	= rProductPart.IsCover();

	POSITION insertPos = NULL;
	if (nProductPartIndex > 0)
	{
		POSITION pos = GetHeadPosition();
		while (pos)
		{
			CPageTemplate& rTemplate = GetAt(pos);
			if (nProductPartIndex > rTemplate.m_nProductPartIndex)	// search for last template with smaller nProductPartIndex -> that is the correct insertion pos
				insertPos = pos;

			CList::GetNext(pos);
		}
	}

	INT_PTR nPageNum = 1;
	if (bHasCover && ( ! bThisIsCover))						// product has cover but this is not the cover itself
	{
		CPageTemplate& rTemplate = GetAt(insertPos);
		if (pDoc->m_boundProducts.GetProductPart(rTemplate.m_nProductPartIndex).IsCover())
		{
			int nNumCoverPages = GetCount(rTemplate.m_nProductPartIndex);
			if (nNumCoverPages >= 4)
			{
				insertPos = GetHeadPosition(rTemplate.m_nProductPartIndex); CList::GetNext(insertPos);		// insert content into cover after 2nd cover page
			}
		}
		nPageNum = GetCount(-nProductIndex - 1) + 1 - 2;	// subtract last 2 pages of cover
	}
	else
		nPageNum = GetCount(-nProductIndex - 1) + 1;

	for (int i = 0; i < nPages; i++)
	{
		CPageTemplate newTemplate;
		CString		  strID;
		strID.Format(_T("%d"), nPageNum++);
		newTemplate.Create(strID, CPageTemplate::SinglePage, 0.0f, 0.0f, nProductIndex, nProductPartIndex);
		if (insertPos)
			insertPos = InsertAfter(insertPos, newTemplate); 
		else
			insertPos = AddHead(newTemplate, nProductPartIndex);

	}

	if (bHasCover && ( ! bThisIsCover))						// correct last pages of product/cover
	{
		GetNext(insertPos, -nProductIndex - 1);
		while (insertPos)
		{
			CPageTemplate& rTemplate = GetNext(insertPos, -nProductIndex - 1);
			rTemplate.m_strPageID.Format(_T("%d"), nPageNum++);
		}
	}
}

void CPageTemplateList::SynchronizeToFlatProductList(CFlatProductList& rFlatProductList, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (nProductPartIndex < 0)
		return;

	int nIndex = 0;
	POSITION pos = GetHeadPosition(nProductPartIndex);
	while (pos)
	{
		CPageTemplate& rTemplate = GetNext(pos, nProductPartIndex);
		if (nIndex < rFlatProductList.GetSize())
			rTemplate.m_strPageID = rFlatProductList.FindByIndex(nIndex).m_strProductName;
		if ( ! pos)
			break;
		CPageTemplate& rBackTemplate = GetNext(pos, nProductPartIndex);
		if (nIndex < rFlatProductList.GetSize())
		{
			rBackTemplate.m_strPageID = rTemplate.m_strPageID + _T("*");
			rBackTemplate.m_bLabelBackSide = TRUE;
		}

		nIndex++;
	}

	while (GetCount(nProductPartIndex) < 2*rFlatProductList.GetSize())
	{
		CPageTemplate newTemplate;
		CString		  strID;
		strID = rFlatProductList.FindByIndex(GetCount(nProductPartIndex)/2).m_strProductName;
		newTemplate.Create(strID, CPageTemplate::SinglePage, 0.0f, 0.0f, -1, nProductPartIndex);
		AddTail(newTemplate, nProductPartIndex);
		newTemplate.Create(strID + _T("*"), CPageTemplate::SinglePage, 0.0f, 0.0f, -1, nProductPartIndex);
		newTemplate.m_bLabelBackSide = TRUE;
		AddTail(newTemplate, nProductPartIndex);
	}

	while (GetCount(nProductPartIndex) > 2*rFlatProductList.GetSize())
		RemoveTail(nProductPartIndex);
}

int	CPageTemplateList::GetPageIndex(CPageTemplate* pPageTemplate)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (&CList::GetNext(pos) == pPageTemplate)
			return nIndex;
		nIndex++;
	}

	return -1;
}

// Search for next free MultipageGroupNum
short CPageTemplateList::GetNextFreeGroupNum()	
{
	short	 nGroupNum = -1;
	POSITION pos	   = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);
		nGroupNum = max(nGroupNum, rPageTemplate.m_nMultipageGroupNum);
	}
	
	return (short)(nGroupNum + 1); // "short + 1" can be possible "int"
}

void CPageTemplateList::ReorganizeColorInfos(BOOL bUndefinedOnly)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return;

	int nProductPartIndex = 0;
	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);

		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			CBoundProductPart& rProductPart = rBoundProduct.m_parts[i];
			//if ( (pProductGroup->m_nType == CPrintGroup::Flat) || (pProductGroup->m_colorantList.GetSize() <= 0) || bDoChange)
			//{
			//	if (bUndefinedOnly)		// only color undefined product parts should be changed
			//		if (pProductGroup->m_colorantList.GetSize() > 0)
			//			continue;

			rProductPart.m_colorantList.RemoveAll();
			POSITION pos2 = GetHeadPosition();
			while (pos2)
			{
				CPageTemplate& rTemplate = CList::GetNext(pos2);
				if (nProductPartIndex != rTemplate.m_nProductPartIndex)
					continue;

				for (int i = 0; i < rTemplate.m_ColorSeparations.GetSize(); i++)
				{
					CString strColorName = pDoc->m_ColorDefinitionTable[rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex].m_strColorName;
					if (strColorName.CompareNoCase(_T("Composite")) == 0)
					{
						CPageSource*	   pPageSource		 = rTemplate.GetObjectSource(rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex);
						CPageSourceHeader* pPageSourceHeader = rTemplate.GetObjectSourceHeader(rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex);
						if (pPageSourceHeader)
						{
							for (int s = 0; s < pPageSourceHeader->m_processColors.GetSize(); s++)
								rProductPart.m_colorantList.AddSeparation(pPageSourceHeader->m_processColors[s]);
							for (int s = 0; s < pPageSourceHeader->m_spotColors.GetSize(); s++)
								rProductPart.m_colorantList.AddSeparation(theApp.MapPrintColor(pPageSourceHeader->m_spotColors[s]), (pPageSource) ? &pPageSource->m_spotcolorDefList : NULL);
						}
					}
					else	// separated
						rProductPart.m_colorantList.AddSeparation(theApp.MapPrintColor(strColorName));
				}
			}
			nProductPartIndex++;
			//}
		}
	}
}

// Call this function when separations are assigned to or removed from pages
// or when foldsheets (objects) are added to printsheets
void CPageTemplateList::ReorganizePrintSheetPlates(BOOL bUseDefaults)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (this == &pDoc->m_MarkTemplateList)	
	{
		ReorganizePrintSheetPlatesMarks(bUseDefaults);
		return;
	}

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetNext(pos);
		for (int i = 0; i < rTemplate.m_PrintSheetLocations.GetSize(); i++)
		{
			CLayoutObject* pObject	   = rTemplate.m_PrintSheetLocations[i].GetLayoutObject();
			if ( ! pObject)
				continue;
			CPrintSheet*   pPrintSheet = rTemplate.m_PrintSheetLocations[i].GetPrintSheet();
			if ( ! pPrintSheet)
				continue;
			CheckForExposuresToAdd(pPrintSheet, pObject, NULL, rTemplate, i, bUseDefaults, pDoc, -1);
			CheckForExposuresToRemove(rTemplate, i, pPrintSheet, pObject->GetLayoutSideIndex());
		}
	}
}

void CPageTemplateList::ReorganizePrintSheetPlatesMarks(BOOL bUseDefaults)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetNext(pos);

		BOOL	 bFound = FALSE;
		POSITION pos	= pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
		while (pos)
		{
			if (pDoc->m_PrintSheetList.m_Layouts.GetNext(pos).m_markList.FindMark(rTemplate.m_strPageID))
			{
				bFound = TRUE;
				break;
			} 
		}
		if (bFound)		
			continue;

		// only marks that are not in mark list (KIM5 marks) to be processed here. KIM6 marks are processed in AnalyzeMarks() (see below)
		for (int i = 0; i < rTemplate.m_PrintSheetLocations.GetSize(); i++)
		{
			CLayoutObject* pObject	   = rTemplate.m_PrintSheetLocations[i].GetLayoutObject();
			if ( ! pObject)
				continue;
			CPrintSheet*   pPrintSheet = rTemplate.m_PrintSheetLocations[i].GetPrintSheet();
			if ( ! pPrintSheet)
				continue;
			CheckForExposuresToAdd(pPrintSheet, pObject, NULL, rTemplate, i, bUseDefaults, pDoc, -1);
			CheckForExposuresToRemove(rTemplate, i, pPrintSheet, pObject->GetLayoutSideIndex());
		}
	}

	// due to speed performance (i.e. jobs with 1000+ pages, lot of marks, etc.) we use AnalyzeMarks() to switch on/off exposures - CheckFor..() needs minutes
	pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();
}

// Look if all color separations will be exposed (i.e. are members of exposure sequence)
// if not, add page to exposure sequence
BOOL CPageTemplateList::CheckForExposuresToAdd(CPrintSheet* pPrintSheet, CLayoutObject* pObject, CMark* pMark, CPageTemplate& rTemplate, int nLocIndex, BOOL bUseDefaults, CImpManDoc* pDoc, int nColorDefinitionIndexToCheck)
{
	if ( (nColorDefinitionIndexToCheck < 0) && pObject)
	{
		BOOL bIsRightPanorama = (pObject->IsPanoramaPage(*pPrintSheet) && pObject->IsMultipagePart(*pPrintSheet, RIGHT)) ? TRUE : FALSE;
		BOOL bRemoveExposure = (bIsRightPanorama) ? TRUE : FALSE;	// by definition only left part of panorama pages will be exposed
		if ( ! bRemoveExposure)
			if (rTemplate.IsFlatSingleSidedBackSide())
				bRemoveExposure = TRUE;

		if (bRemoveExposure)
			rTemplate.m_PrintSheetLocations[nLocIndex].RemoveExposure(-1);	// -1 -> remove exposure from default plate

		if ( ! rTemplate.m_ColorSeparations.GetSize())
			if ( ! rTemplate.m_bMap2AllColors)
			{
				if ( ! bRemoveExposure)
				{
					CExposure* pDefaultExposure = rTemplate.m_PrintSheetLocations[nLocIndex].GetExposure(-1);	// get exposure at default plate
					if ( ! pDefaultExposure)
						rTemplate.m_PrintSheetLocations[nLocIndex].AddExposure(-1);	
				}
				return FALSE;
			}
	}

	if ( ! pDoc)
		pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if ( rTemplate.m_bMap2AllColors || (rTemplate.IsSystemCreated(pMark) && rTemplate.m_bMap2SpotColors) )
	{
		// explode template to actual document colorants
		SEPARATIONINFO sepInfo = {0, 1, 0};
		if (rTemplate.m_ColorSeparations.GetSize())
			sepInfo = rTemplate.m_ColorSeparations[0];

		if ( rTemplate.m_bMap2AllColors)
		{
			rTemplate.m_ColorSeparations.RemoveAll();
			rTemplate.m_ColorSeparations.Add(sepInfo);	// always keep 1st separation as master
		}
		else
			rTemplate.RemoveSpotSeps();
		if (pDoc->m_ColorDefinitionTable.GetSize())		
		{
//			if ( (sepInfo.m_nColorDefinitionIndex >= 0) && (sepInfo.m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
//					if (pDoc->m_ColorDefinitionTable[sepInfo.m_nColorDefinitionIndex].m_strColorName != _T("Composite"))
				if (1)
				{
					for (int i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
					{
						CString strMappedColor = theApp.MapPrintColor(pDoc->m_ColorDefinitionTable[i].m_strColorName);
						if (strMappedColor == pDoc->m_ColorDefinitionTable[i].m_strColorName)	// Basic colors only
						{
							BOOL bCheckColor = FALSE;
							if (rTemplate.m_bMap2AllColors)
								bCheckColor = TRUE;
							else	// map to spotcolors
							{
								if (strMappedColor.CompareNoCase(_T("Composite")) != 0) 
								{
									CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strMappedColor);
									if ( ! pColorDef)
										bCheckColor = TRUE;
									else
										if (pColorDef->m_nColorType == CColorDefinition::Spot)
											bCheckColor = TRUE;
								}
							}

							if (bCheckColor)
								if (pDoc->m_PageTemplateList.ColorInUse(pDoc, pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strMappedColor)) )
								{
									if ( rTemplate.m_bMap2AllColors)
									{
										sepInfo.m_nColorDefinitionIndex = i;
										rTemplate.m_ColorSeparations.Add(sepInfo);	
									}
									else	// system created and map to spotcolor
									{
										CPageSource* pObjectSource = (pMark) ? &pMark->m_MarkSource : rTemplate.GetObjectSource(sepInfo.m_nColorDefinitionIndex);
										if (pObjectSource)
										{
											CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strMappedColor);
											CPageSourceHeader objectSourceHeader;
											objectSourceHeader.Create(1, _T("1"), 0.0f, 0.0f, 0.0f, 0.0f, strMappedColor, FALSE);
											objectSourceHeader.m_rgb			  = (pColorDef) ? pColorDef->m_rgb  : WHITE;
											objectSourceHeader.m_bPageNumberFix   = FALSE;
											pObjectSource->m_PageSourceHeaders.Add(objectSourceHeader);

											INT_PTR nPageNumInSourceFile = pObjectSource->m_PageSourceHeaders.GetSize();
											if ( ! rTemplate.GetFirstSepInfo(nPageNumInSourceFile))
											{
												for (int j = 0; j < rTemplate.m_ColorSeparations.GetSize(); j++)
													if (rTemplate.m_ColorSeparations[j].m_nPageNumInSourceFile >= nPageNumInSourceFile)
														rTemplate.m_ColorSeparations[j].m_nPageNumInSourceFile++;

												sepInfo.m_nPageNumInSourceFile  = (int)nPageNumInSourceFile;
												sepInfo.m_nColorDefinitionIndex = i;
												rTemplate.InsertSepInfo(&sepInfo);
											}
										}
									}
								}
						}
					}
				}
				else
				{
					rTemplate.m_ColorSeparations.RemoveAll();
					rTemplate.m_ColorSeparations.Add(sepInfo);	
				}
		}
	}

	for (int nSepIndex = 0; nSepIndex < rTemplate.m_ColorSeparations.GetSize(); nSepIndex++)
	{
		short nColorDefinitionIndex = theApp.MapPrintColor(rTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex, &rTemplate, pPrintSheet, (pObject) ? pObject->GetLayoutSideIndex() : BOTHSIDES);

		if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= pDoc->m_ColorDefinitionTable.GetSize()) )
			continue;

		if (nColorDefinitionIndexToCheck >= 0)	// we are in check mode
		{
			if (nColorDefinitionIndexToCheck == nColorDefinitionIndex)
				return TRUE;
			else
				continue;
		}
		if ( (nLocIndex < 0) || (nLocIndex >= rTemplate.m_PrintSheetLocations.GetSize()) )
			continue;

		CExposure* pExposure = rTemplate.m_PrintSheetLocations[nLocIndex].GetExposure(nColorDefinitionIndex);
		if ( ! pExposure)
		{
			CPageSourceHeader* pPageSourceHeader = rTemplate.GetObjectSourceHeader(nColorDefinitionIndex);
			BOOL bPageOutputBlocked = (pPageSourceHeader) ? pPageSourceHeader->m_bOutputBlocked : FALSE;
			if ( ! bPageOutputBlocked)
				if ( ! rTemplate.IsFlatSingleSidedBackSide())
					rTemplate.m_PrintSheetLocations[nLocIndex].AddExposure(nColorDefinitionIndex);	
		}
		else
			if (bUseDefaults)
			{
				CLayoutSide* pLayoutSide	  = rTemplate.m_PrintSheetLocations[nLocIndex].GetLayoutSide();
				CPlate*		 pDefaultPlate	  = (pLayoutSide->m_nLayoutSide == FRONTSIDE) ? &pPrintSheet->m_FrontSidePlates.m_defaultPlate : 
																							&pPrintSheet->m_BackSidePlates.m_defaultPlate;
				CExposure*	 pDefaultExposure = pDefaultPlate->m_ExposureSequence.GetExposure(rTemplate.m_PrintSheetLocations[nLocIndex].m_nLayoutObjectIndex);

				if (pDefaultExposure)
					*pExposure = *pDefaultExposure;

				CPlate* pPlate = pExposure->GetPlate();
				if (pPlate && pDefaultPlate)
				{
					pPlate->m_nAutoBleedMethod		 = pDefaultPlate->m_nAutoBleedMethod;
					pPlate->m_fOverbleedOutside		 = pDefaultPlate->m_fOverbleedOutside;	 
					pPlate->m_fOverbleedInside		 = pDefaultPlate->m_fOverbleedInside;	  
					pPlate->m_fOverbleedOverlap		 = pDefaultPlate->m_fOverbleedOverlap;	 
					pPlate->m_fOverbleedControlMarks = pDefaultPlate->m_fOverbleedControlMarks;
				}
			}

		if (pObject)
			if (pObject->IsPanoramaPage(*pPrintSheet) && pObject->IsMultipagePart(*pPrintSheet, RIGHT))
			{
				rTemplate.m_PrintSheetLocations[nLocIndex].RemoveExposure(nColorDefinitionIndex);
				rTemplate.m_PrintSheetLocations[nLocIndex].RemoveExposure(-1);
				// by definition only left part of panorama pages will be exposed
			}
	}

	return FALSE;
}

// Look if there are plates with color different to all page separations
// if yes, look if page is member of exposure sequence
// if yes, remove page from sequence
void CPageTemplateList::CheckForExposuresToRemove(CPageTemplate& rTemplate, int nLocIndex, CPrintSheet* pPrintSheet, int nSide)
{
	if (rTemplate.m_bMap2AllColors)	// nothing to remove
		return;

	CPlateList* pPlateList = rTemplate.m_PrintSheetLocations[nLocIndex].GetPlateList();
	POSITION platePos = (pPlateList) ? pPlateList->GetHeadPosition() : NULL;
	while (platePos)
	{
		CPlate& rPlate = pPlateList->GetAt(platePos);
		BOOL bFound = FALSE;
		for (int nSepIndex = 0; nSepIndex < rTemplate.m_ColorSeparations.GetSize(); nSepIndex++)
		{
			if (theApp.MapPrintColor(rTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex, &rTemplate, pPrintSheet, nSide) == rPlate.m_nColorDefinitionIndex)
			{
				bFound = TRUE;
				break;
			}
		}

		CPageSourceHeader* pPageSourceHeader = rTemplate.GetObjectSourceHeader(rPlate.m_nColorDefinitionIndex);
		BOOL bRemoveExposure = (pPageSourceHeader) ? pPageSourceHeader->m_bOutputBlocked : FALSE;
		if ( ! bRemoveExposure)
			if (rTemplate.IsFlatSingleSidedBackSide())
				bRemoveExposure = TRUE;

		if ( ! bFound || bRemoveExposure)
		{
			POSITION seqPos = rPlate.m_ExposureSequence.GetHeadPosition();
			while (seqPos)
			{
				CExposure& rExposure = rPlate.m_ExposureSequence.GetAt(seqPos);
				if (rExposure.m_nLayoutObjectIndex == rTemplate.m_PrintSheetLocations[nLocIndex].m_nLayoutObjectIndex)
				{
					rPlate.m_ExposureSequence.RemoveAt(seqPos);
					break;
				}
				rPlate.m_ExposureSequence.GetNext(seqPos);
			}
		}

		if (rPlate.m_ExposureSequence.HasNoPages())
		{
			POSITION pos = platePos;
			pPlateList->GetNext(platePos);
			pPlateList->RemoveAt(pos);
		}
		else
			pPlateList->GetNext(platePos);
	} 
}

void CPageTemplateList::ReorganizeGluelineScaling()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetNext(pos);
		if (rTemplate.m_nAttribute & CPageTemplate::ScaleOffsetModified)
			if ( (fabs(rTemplate.m_fContentOffsetX) <= 0.001f) && (fabs(rTemplate.m_fContentOffsetY) <= 0.001f) && (fabs(rTemplate.m_fContentScaleX - 1.0f) <= 0.001f) && (fabs(rTemplate.m_fContentScaleY - 1.0f) <= 0.001f) )
				rTemplate.m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
	}

	pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetNext(pos);
		if ( ! (rTemplate.m_nAttribute & CPageTemplate::ScaleOffsetModified) && ! rTemplate.m_bAdjustContentToMask )
		{
			if (rTemplate.m_PrintSheetLocations.GetSize() <= 0)
			{
				rTemplate.SetContentOffsetX(0.0f);
				rTemplate.m_fContentScaleX = 1.0f;
			}
			else
			{
				for (int i = 0; i < rTemplate.m_PrintSheetLocations.GetSize(); i++)
				{
					CLayoutObject* pObject	   = rTemplate.m_PrintSheetLocations[i].GetLayoutObject();
					if ( ! pObject)
						continue;
					if (pObject->IsFlatProduct())
						continue;
					CPrintSheet*   pPrintSheet = rTemplate.m_PrintSheetLocations[i].GetPrintSheet();
					if ( ! pPrintSheet)
						continue;

					if ( ! pObject->IsPanoramaPage(*pPrintSheet))
					{
						CPrintingProfile& rProfile = pPrintSheet->GetPrintingProfile();
						CBoundProduct&	  rProduct = rTemplate.GetProductPart().GetBoundProduct();
						BOOL bAdjustToGlueline = FALSE;
						if (rProfile.m_prepress.m_bAdjustContentToMask && (fabs(rProduct.m_bindingParams.m_fGlueLineWidth) > 0.01) )
						{
							if (rProduct.m_bindingParams.m_bindingDefs.m_nBinding == CFinalProductDescription::PerfectBound)
							{
								if ( ! rTemplate.GetProductPart().IsCover())
									bAdjustToGlueline = TRUE;	
							}
							else
								if ( (rProduct.m_bindingParams.m_bindingDefs.m_nBinding == CFinalProductDescription::SaddleStitched) && ! rTemplate.IsFirstLastOrInnerPage())
									bAdjustToGlueline = TRUE;	
						}

						if (bAdjustToGlueline)
						{
							BOOL bIsLeftPage = (pObject->m_nIDPosition == LEFT) ? TRUE : FALSE;
							rTemplate.m_fContentScaleX = 1.0f - rProduct.m_bindingParams.m_fGlueLineWidth / pObject->GetWidth();
							float fOffsetX = rProduct.m_bindingParams.m_fGlueLineWidth/2.0f;
							rTemplate.SetContentOffsetX((bIsLeftPage) ? -fOffsetX : fOffsetX);
						}
						else
						{
							rTemplate.SetContentOffsetX(0.0f);
							rTemplate.m_fContentScaleX = 1.0f;
						}
					}
				}
			}
		}
	}
}

void CPageTemplateList::ShiftProductPartIndices(int nNewIndex, int nNumNewParts)
{
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);
		if (rPageTemplate.m_nProductPartIndex >= nNewIndex)
			rPageTemplate.m_nProductPartIndex += nNumNewParts;
	}
}


// Remove page source from color separations of page templates
// In case of complete removement of page source (not break links only):
//		Because index of page sources with index greater than nPageSourceIndex has decreased by one 
//		rearrange page source indices of color separations
void CPageTemplateList::RemovePageSource(int nPageSourceIndex, BOOL bBreakLinksOnly, BOOL bResetOutputStamps, BOOL bResetPageNums)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);
		BOOL		   bRemoved		 = FALSE;
		for (int nSepIndex = 0; nSepIndex < rPageTemplate.m_ColorSeparations.GetSize(); nSepIndex++)
		{
			if (rPageTemplate.m_ColorSeparations[nSepIndex].m_nPageSourceIndex == nPageSourceIndex)
			{
				if (bResetOutputStamps)
				{
					for (int i = 0; i < rPageTemplate.m_PrintSheetLocations.GetSize(); i++)
					{
						CPlateList* pPlateList = rPageTemplate.m_PrintSheetLocations[i].GetPlateList();
						POSITION	platePos   = (pPlateList) ? pPlateList->GetHeadPosition() : NULL;
						while (platePos)
						{
							CPlate& rPlate = pPlateList->GetNext(platePos);
							if (rPlate.m_nColorDefinitionIndex == rPageTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex)
								rPlate.m_OutputDate.m_dt = 0.0f;
						}
					}
				}

				rPageTemplate.m_ColorSeparations.RemoveAt(nSepIndex--);
				if ( (theApp.m_nGUIStyle != CImpManApp::GUIStandard) && bResetPageNums)	// AutoServer command -> reset to original page number
					if ( ! rPageTemplate.m_ColorSeparations.GetSize())
						rPageTemplate.m_strPageID.Format(_T("%d"), nIndex + 1);

				bRemoved = TRUE;
			}
			else
				if ( ! bBreakLinksOnly)
					if (rPageTemplate.m_ColorSeparations[nSepIndex].m_nPageSourceIndex > nPageSourceIndex)
						rPageTemplate.m_ColorSeparations[nSepIndex].m_nPageSourceIndex--;
		}

		if (theApp.settings.m_bAutoCheckDoublePages)
			if (bRemoved && ! rPageTemplate.m_ColorSeparations.GetSize())
				if (rPageTemplate.m_nMultipageGroupNum >= 0)
					CPageListView::SetDoublePage(&rPageTemplate, (rPageTemplate.GetMultipageSide() == LEFT) ? RIGHT : LEFT, FALSE);

		nIndex++;
	}
}


void CPageTemplateList::RemovePageSourcesAll()
{
	POSITION pos = GetHeadPosition();
	while (pos)
		CList::GetNext(pos).m_ColorSeparations.RemoveAll();
}


CPageTemplate* CPageTemplateList::FindTemplate(CString strID)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);
		if (rPageTemplate.m_strPageID == strID)
			return &rPageTemplate;
	}

	return NULL;
}


POSITION CPageTemplateList::FindTemplatePos(CString strID, POSITION startPos, int nProductPartIndex)
{
	POSITION pos = (startPos) ? startPos : GetHeadPosition(nProductPartIndex);
	while (pos)
	{
		CPageTemplate& rPageTemplate = GetAt(pos);
		if (rPageTemplate.m_strPageID == strID)
			return pos;
		GetNext(pos, nProductPartIndex);
	}

	return NULL;
}

BOOL CPageTemplateList::ColorInUse(CImpManDoc* pDoc, short nColorDefinitionIndex, BOOL bSeparationOnly, BOOL bCompositeAlso)
{
	if (nColorDefinitionIndex < 0)
		return FALSE;
	if ( ! pDoc)
		pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return FALSE;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetNext(pos);
		if (rTemplate.ColorInUse(pDoc, nColorDefinitionIndex, bSeparationOnly, bCompositeAlso))
			return TRUE;
	}

	return FALSE;
}

void CPageTemplateList::DecrementColorDefIndices(short nColorDefinitionIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetNext(pos);
		for (int i = 0; i < rTemplate.m_ColorSeparations.GetSize(); i++)
			if (rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex > nColorDefinitionIndex)
				rTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex--;
	}
}


void CPageTemplateList::ResetFlags()
{
	POSITION pos = GetHeadPosition();
	while (pos)
		CList::GetNext(pos).m_bFlag = FALSE;
}


void CPageTemplateList::CalcShinglingOffsets(float& fOffsetX, float& fOffsetXFoot, float& fOffsetY, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( (nProductPartIndex >= 0) && (pDoc->m_Bookblock.GetNumFoldSheets(nProductPartIndex) <= 0) )
	{
		CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
		fOffsetX	 = rProductPart.GetShinglingValueX();
		fOffsetXFoot = rProductPart.GetShinglingValueXFoot();
		fOffsetY	 = rProductPart.GetShinglingValueY();
	}
	else
	{
		if ( ! pDoc->m_Bookblock.GetBindingProcess(nProductPartIndex).m_bComingAndGoing)	// Normal
		{
			fOffsetX	 = CalcShinglingOffsetsX(nProductPartIndex, FALSE);
			fOffsetXFoot = CalcShinglingOffsetsX(nProductPartIndex, TRUE);
			fOffsetY	 = CalcShinglingOffsetsY(nProductPartIndex);
			if (nProductPartIndex >= 0)		// if called just for get global offsets for a single product part, also call complete analyzis (including page template calculations)  
			{
				CalcShinglingOffsetsX(-pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex) - 1, FALSE);
				CalcShinglingOffsetsX(-pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex) - 1, TRUE);
				CalcShinglingOffsetsY(-pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex) - 1);
			}
		}
		else
		{
			fOffsetX 	 = CalcShinglingOffsetsComingAndGoing(nProductPartIndex, FALSE);
			fOffsetXFoot = CalcShinglingOffsetsComingAndGoing(nProductPartIndex, TRUE);
			fOffsetY	 = fOffsetX;
			if (nProductPartIndex >= 0)		// if called just for get global offsets for a single product part, also call complete analyzis (including page template calculations)  
			{
				CalcShinglingOffsetsComingAndGoing(-pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex) - 1, FALSE);
				CalcShinglingOffsetsComingAndGoing(-pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex) - 1, TRUE);
			}
 		}
	}
}

float CPageTemplateList::CalcShinglingOffsetsX(int nProductPartIndex, BOOL bFoot)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0.0f;
	CArray <short*, short*> PageSequence;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
	if ( ! rProductPart.IsNull())
		if ( ! rProductPart.m_foldingParams.m_bShinglingActive)
			return 0.0f;
	
	float fOffsetMax = 0.0f;

	float	 fLocalValue = 0.0f, fGlobalValue = 0.0f;
	POSITION posDL	     = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (posDL)
	{
		CDescriptionItem& rItem	= pDoc->m_Bookblock.m_DescriptionList.GetNext(posDL);
		CFoldSheet&	rFoldSheet	= rItem.GetFoldSheet(&pDoc->m_Bookblock);
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;

		rFoldSheet.GetPageSequence(PageSequence, rItem.m_nFoldSheetPart);
		rFoldSheet.ReducePageSequence(PageSequence);

		float fShinglingValue = (bFoot) ? rFoldSheet.m_fShinglingValueXFoot : rFoldSheet.m_fShinglingValueX;

		for (int i = 0; i < PageSequence.GetSize(); i+= 2)
		{
			if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart) 
				fLocalValue = -(i/2 + 1) * fShinglingValue;
			
			float fOffset = fGlobalValue + fLocalValue;
			
			if (nProductPartIndex < 0)	// calc page template offsets only when this analyzis has been called for complete bookblock
			{
				POSITION posTL = FindIndex(*PageSequence[i] - 1, nProductPartIndex);
				if ( ! posTL)
					continue;
				CPageTemplate& r1stPageTemplate = GetAt(posTL);
				if ( ! r1stPageTemplate.m_bShinglingOffsetLocked)
					if (bFoot)
						r1stPageTemplate.m_fShinglingOffsetXFoot = -fOffset;
					else
						r1stPageTemplate.m_fShinglingOffsetX	 = -fOffset;
				
				if ((i + 1) >= PageSequence.GetSize())
					continue;
				posTL = FindIndex(*PageSequence[i + 1] - 1, nProductPartIndex);
				if ( ! posTL)
					continue;
				CPageTemplate& r2ndPageTemplate = GetAt(posTL);
				if ( ! r2ndPageTemplate.m_bShinglingOffsetLocked)
					if (bFoot)
						r2ndPageTemplate.m_fShinglingOffsetXFoot =  fOffset;
					else
						r2ndPageTemplate.m_fShinglingOffsetX	 =  fOffset;
				
				if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
					fLocalValue = (i/2 + 1) * fShinglingValue;
			}

			fOffsetMax = max(fOffsetMax, fOffset);
		}

		if ( ! posDL)
			break;

		int nLayers = (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) ? rFoldSheet.m_nPagesLeft/2 : rFoldSheet.m_nPagesRight/2;
		CDescriptionItem& rNextItem	= pDoc->m_Bookblock.m_DescriptionList.GetAt(posDL);
		if (rItem.m_nFoldSheetIndex == rNextItem.m_nFoldSheetIndex)
			if ( (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) && (rNextItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart) )	// inside foldsheet -> right side pages counts
				nLayers = rFoldSheet.m_nPagesRight/2;

		fGlobalValue+= (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) ? nLayers * fShinglingValue : -nLayers * fShinglingValue;
		fLocalValue = 0.0f;
	}

	return fOffsetMax;
}

float CPageTemplateList::CalcShinglingOffsetsY(int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0.0f;
	CArray <short*, short*> PageSequence;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
	if ( ! rProductPart.IsNull())
	{
		if ( ! rProductPart.m_foldingParams.m_bShinglingActive)
			return 0.0f;
		if (rProductPart.m_foldingParams.m_nShinglingDirection == CBookblock::ShinglingDirectionSpine)
			return 0.0f;
	}

	BOOL  bMoveToHead = (rProductPart.m_foldingParams.m_nShinglingDirection == CBookblock::ShinglingDirectionSpineHead) ? TRUE : FALSE;
	float fOffsetMax  = 0.0f;

	POSITION posDL = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (posDL)
	{
		CDescriptionItem& rItem	= pDoc->m_Bookblock.m_DescriptionList.GetNext(posDL);
		CFoldSheet&	rFoldSheet	= rItem.GetFoldSheet(&pDoc->m_Bookblock);
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;

		rFoldSheet.GetPageSequence(PageSequence, rItem.m_nFoldSheetPart);
		rFoldSheet.ReducePageSequence(PageSequence);
		//BOOL bMoveToHead = rFoldSheet.IsFoldSchemeClosedOnHead();

		float fOffset = 0.0f;
		int i = 0;
		for (i = 0; i < PageSequence.GetSize()/2; i+= 2)
		{
			if (nProductPartIndex < 0)	// calc page template offsets only when this analyzis has been called for complete bookblock
			{
				POSITION posTL = FindIndex(*PageSequence[i] - 1, nProductPartIndex);
				if ( ! posTL)
					continue;
				CPageTemplate& r1stPageTemplate = GetAt(posTL);
				if ( ! r1stPageTemplate.m_bShinglingOffsetLocked)
					r1stPageTemplate.m_fShinglingOffsetY = (bMoveToHead) ? fOffset : -fOffset;
				
				if ((i + 1) >= PageSequence.GetSize())
					continue;
				posTL = FindIndex(*PageSequence[i + 1] - 1, nProductPartIndex);
				if ( ! posTL)
					continue;
				CPageTemplate& r2ndPageTemplate = GetAt(posTL);
				if ( ! r2ndPageTemplate.m_bShinglingOffsetLocked)
					r2ndPageTemplate.m_fShinglingOffsetY = (bMoveToHead) ? fOffset : -fOffset;
			}
			fOffsetMax = max(fOffsetMax, fOffset);
			
			fOffset += rFoldSheet.m_fShinglingValueY;
		}

		for (; i < PageSequence.GetSize(); i+= 2)
		{
			fOffset -= rFoldSheet.m_fShinglingValueY;
			
			if (nProductPartIndex < 0)	// calc page template offsets only when this analyzis has been called for complete bookblock
			{
				POSITION posTL = FindIndex(*PageSequence[i] - 1, nProductPartIndex);
				if ( ! posTL)
					continue;
				CPageTemplate& r1stPageTemplate = GetAt(posTL);
				if ( ! r1stPageTemplate.m_bShinglingOffsetLocked)
					r1stPageTemplate.m_fShinglingOffsetY = (bMoveToHead) ? fOffset : -fOffset;
				
				if ((i + 1) >= PageSequence.GetSize())
					continue;
				posTL = FindIndex(*PageSequence[i + 1] - 1, nProductPartIndex);
				if ( ! posTL)
					continue;
				CPageTemplate& r2ndPageTemplate = GetAt(posTL);
				if ( ! r2ndPageTemplate.m_bShinglingOffsetLocked)
					r2ndPageTemplate.m_fShinglingOffsetY = (bMoveToHead) ? fOffset : -fOffset;
			}

			fOffsetMax = max(fOffsetMax, fOffset);
		}
	}

	return fOffsetMax;
}


float CPageTemplateList::CalcShinglingOffsetsComingAndGoing(int nProductPartIndex, BOOL bFoot)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0.0f;
	CArray <short*, short*> PageSequence;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
	if ( ! rProductPart.m_foldingParams.m_bShinglingActive)
		return 0.0f;
	
	float fOffsetMax = 0.0f;

	float	 fLocalValue = 0.0f, fGlobalValue = 0.0f;
	POSITION posDL	     = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (posDL)
	{
		CDescriptionItem& rItem	= pDoc->m_Bookblock.m_DescriptionList.GetNext(posDL);
		CFoldSheet&	rFoldSheet	= rItem.GetFoldSheet(&pDoc->m_Bookblock);
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
		//		continue;

		rFoldSheet.GetPageSequence(PageSequence, rItem.m_nFoldSheetPart);
		rFoldSheet.ReducePageSequence(PageSequence);
		
		float fShinglingValue = (bFoot) ? rFoldSheet.m_fShinglingValueXFoot : rFoldSheet.m_fShinglingValueX;
		int   nPart			  = CDescriptionItem::FoldSheetLeftPart;
		for (int i = 0, j = 0; i < PageSequence.GetSize(); i+= 2, j+= 2)
		{
			if (i == PageSequence.GetSize()/2)
			{
				j = 0;
				nPart = CDescriptionItem::FoldSheetRightPart;
				fGlobalValue+= PageSequence.GetSize()/4 * fShinglingValue;
			}
			
			if (nPart == CDescriptionItem::FoldSheetRightPart) 
				fLocalValue = -(j/2 + 1) * fShinglingValue;
			
			float fOffset = fGlobalValue + fLocalValue;
			
			if (nProductPartIndex < 0)	// calc page template offsets only when this analyzis has been called for complete bookblock
			{
				POSITION posTL = FindIndex(*PageSequence[i] - 1, nProductPartIndex);
				if (posTL)
					if ( ! GetAt(posTL).m_bShinglingOffsetLocked)
						if (bFoot)
							GetAt(posTL).m_fShinglingOffsetXFoot = -fOffset;
						else
							GetAt(posTL).m_fShinglingOffsetX	 = -fOffset;
				
				if ((i + 1) >= PageSequence.GetSize())
					continue;

				posTL = FindIndex(*PageSequence[i + 1] - 1, nProductPartIndex);
				if (posTL)
					if ( ! GetAt(posTL).m_bShinglingOffsetLocked)
						if (bFoot)
							GetAt(posTL).m_fShinglingOffsetXFoot =  fOffset;
						else
							GetAt(posTL).m_fShinglingOffsetX	 =  fOffset;
			}

			if (nPart == CDescriptionItem::FoldSheetLeftPart)
				fLocalValue = (j/2 + 1) * fShinglingValue;
			
			fOffsetMax = max(fOffsetMax, fOffset);
		}
		fGlobalValue = 0.0f;
		fLocalValue = 0.0f;
	}

	return fOffsetMax;
}


int CPageTemplateList::CalcShinglingMaxLayerNum(int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();

	int	nLayerMax = 0;
	int		 nLayer	= 0;
	POSITION posDL  = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (posDL)
	{
		CDescriptionItem& rItem	= pDoc->m_Bookblock.m_DescriptionList.GetNext(posDL);
		CFoldSheet&	rFoldSheet	= rItem.GetFoldSheet(&pDoc->m_Bookblock);
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
		//		continue;

		if ( ! pDoc->m_Bookblock.GetBindingProcess(nProductPartIndex).m_bComingAndGoing)	
			nLayer+= (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) ? (int)(rFoldSheet.m_nPagesLeft/2) : -(int)(rFoldSheet.m_nPagesRight/2);
		else
			nLayer+= (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) ? (int)(rFoldSheet.m_nPagesLeft/4) : -(int)(rFoldSheet.m_nPagesRight/4);
		nLayerMax = max(nLayer, nLayerMax);
	}

	return nLayerMax;
}


void CPageTemplateList::UpdateMarkSourceLinks(short nOldMarkSourceIndex, short nNewMarkSourceIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);

		for (int i = 0; i < rPageTemplate.m_ColorSeparations.GetSize(); i++)
		{
			if ( ! rPageTemplate.m_bFlag)
				if (rPageTemplate.m_ColorSeparations[i].m_nPageSourceIndex == nOldMarkSourceIndex)
				{
					rPageTemplate.m_ColorSeparations[i].m_nPageSourceIndex = nNewMarkSourceIndex;
					rPageTemplate.m_bFlag = TRUE;
				}
		}
	}
}


// Merge two lists together - only not yet existing elements will be appended
//							- links from layout objects will be updated
// This method is only valid for mark templates!
void CPageTemplateList::MergeElements(CPageTemplateList& rMarkTemplateList, CLayout& rLayout)
{
	rLayout.m_FrontSide.m_ObjectList.ResetPublicFlags();
	rLayout.m_BackSide.m_ObjectList.ResetPublicFlags();

	short	 nOldMarkTemplateIndex = 0;
	POSITION pos				   = rMarkTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = rMarkTemplateList.CList::GetNext(pos);

		BOOL	 bFound				   = FALSE;
		short	 nNewMarkTemplateIndex = 0;
		POSITION pos2				   = GetHeadPosition();
		while (pos2)
		{
			if (rPageTemplate == CList::GetNext(pos2))
			{
				bFound = TRUE;
				rLayout.UpdateMarkTemplateLinks(nOldMarkTemplateIndex, nNewMarkTemplateIndex);
				break;
			}
			nNewMarkTemplateIndex++;
		}

		if ( ! bFound)
		{	
			AddTail(rPageTemplate);
			rLayout.UpdateMarkTemplateLinks(nOldMarkTemplateIndex, (short)(GetCount() - 1));
		}

		nOldMarkTemplateIndex++;
	}

	rLayout.m_FrontSide.m_ObjectList.ResetPublicFlags();
	rLayout.m_BackSide.m_ObjectList.ResetPublicFlags();
}


void CPageTemplateList::ExtractMarkSources(CPageSourceList& rTargetList, CPageSourceList& rSourceList)
{
	rTargetList.RemoveAll();
	ResetFlags();

	short	 nOldMarkSourceIndex = 0;
	short	 nNewMarkSourceIndex = 0;
	POSITION pos				 = rSourceList.GetHeadPosition();
	while (pos)
	{
		CPageSource& rMarkSource = rSourceList.GetNext(pos); 
		if (MarkSourceExists(nOldMarkSourceIndex))
		{
			rTargetList.AddTail(rMarkSource);
			UpdateMarkSourceLinks(nOldMarkSourceIndex, nNewMarkSourceIndex);
			nNewMarkSourceIndex++;
		}
		nOldMarkSourceIndex++;
	}

	ResetFlags();
}

BOOL CPageTemplateList::MarkSourceExists(short nMarkSourceIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);

		for (int i = 0; i < rPageTemplate.m_ColorSeparations.GetSize(); i++)
		{
			if (rPageTemplate.m_ColorSeparations[i].m_nPageSourceIndex == nMarkSourceIndex)
				return TRUE;
		}
	}
	return FALSE;
}

BOOL CPageTemplateList::AutoAssignPageSource(CPageSource* pPageSource, int nProductPartIndex, int nLabelPageIndex, BOOL bReorganize)
{
	if ( ! pPageSource->m_PageSourceHeaders.GetSize())
		return FALSE;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
	{
#ifdef KIM_ENGINE
		theApp.LogEntry(_T("   Error in AutoAssignPageSource(): no doc"), TRUE);
#endif
		return FALSE;
	}

	int			   nCurPageNum	 = pPageSource->m_PageSourceHeaders[0].m_nPageNumber;
	CString		   strStartPage	 = pPageSource->m_PageSourceHeaders[0].m_strPageName;
	CPageTemplate* pPageTemplate = NULL;
	POSITION	   startPos		 = NULL;
	BOOL		   bShift		 = (nCurPageNum == 1) ? FALSE : TRUE;
	if (nLabelPageIndex == -1)	// page, not label
	{
		if ( ! bShift)	// no shift defined
		{
			// Search for first unassigned page template 
			startPos = pDoc->m_PageTemplateList.GetHeadPosition(nProductPartIndex);
			while (startPos)
			{
				pPageTemplate = &pDoc->m_PageTemplateList.GetNext(startPos, nProductPartIndex);
				if (pPageTemplate->m_bLabelBackSide)
				{
					CFlatProduct& rFlatProduct = pPageTemplate->GetFlatProduct();
					if (rFlatProduct.m_nNumPages <= 1)
						continue;
				}
				if ( ! pPageTemplate->m_ColorSeparations.GetSize())
					break;
			}
			if (pPageTemplate)
				if (pPageTemplate->m_ColorSeparations.GetSize())
					pPageTemplate = NULL;
		}
		else
		{
			startPos = pDoc->m_PageTemplateList.FindTemplatePos(strStartPage, NULL, nProductPartIndex);	// 1st find desired position by strings to keep track of modified pagination
			if ( ! startPos)
				startPos = pDoc->m_PageTemplateList.FindIndex(nCurPageNum - 1, nProductPartIndex);	// if not found find desired position by index
			if (startPos)
				pPageTemplate = &pDoc->m_PageTemplateList.GetNext(startPos, nProductPartIndex);
		}
	}
	else
	{
		startPos	  = pDoc->m_PageTemplateList.FindLabelPos(nLabelPageIndex);
		pPageTemplate = &pDoc->m_PageTemplateList.GetAt(startPos);
	}

	if ( ! pPageTemplate)	// if no fitting position found, break and let user define position later
	{
#ifdef KIM_ENGINE
		TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
		_tsplitpath((LPCTSTR)pPageSource->m_strDocumentFullpath, szDrive, szDir, szFname, szExt);
		CString strMsg;
		strMsg.Format(_T("Cannot assign file '%s%s' to job template '%s'"), szFname, szExt, pDoc->GetTitle());
		theApp.LogEntry(_T("   ") + strMsg, TRUE);
#endif
		return FALSE;
	}

	pDoc->m_PageTemplateList.ResetFlags();

	CPageTemplate* p2ndPageTemplate = NULL;
	if (pPageTemplate->CheckMultipage((pPageSource->m_PageSourceHeaders.GetSize()) ? &pPageSource->m_PageSourceHeaders[0] : NULL))
	{
		p2ndPageTemplate = pPageTemplate->GetFirstMultipagePartner();
		p2ndPageTemplate->m_bFlag = TRUE;
	}

	//if ( ! pPageTemplate->FlatProductHasBackSide())
	{
		POSITION pos = startPos;
		while (pos)
		{
			CPageTemplate* pPageTemplate = &(pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex));
			if (pPageTemplate->m_bLabelBackSide)
			{
				CFlatProduct& rFlatProduct = pPageTemplate->GetFlatProduct();
				if (rFlatProduct.m_nNumPages <= 1)
					pPageTemplate->m_bFlag = TRUE;
			}
		}
	}

	BOOL bAssigned			= FALSE;
	BOOL bKeepContentOffset = UNDEFINED;
	BOOL bTrimBoxWarned		= FALSE;
	BOOL bOccupiedWarned	= FALSE;
	do
	{
		for (int i = 0; i < pPageSource->m_PageSourceHeaders.GetSize(); i++)
		{
			CPageSourceHeader* pPageSourceHeader = &pPageSource->m_PageSourceHeaders[i];

			if (nCurPageNum != pPageSourceHeader->m_nPageNumber)
			{
				if (!startPos)
					break;

				do
					pPageTemplate = &(pDoc->m_PageTemplateList.GetNext(startPos, nProductPartIndex));
				while (pPageTemplate->m_bFlag && startPos);

				if (pPageTemplate->m_bFlag && ! startPos)	// if last template has flag
					break;

				if (pPageTemplate->CheckMultipage(pPageSourceHeader))
				{
					p2ndPageTemplate = pPageTemplate->GetFirstMultipagePartner();
					if (p2ndPageTemplate->m_bFlag)
						p2ndPageTemplate = NULL;
					else
						p2ndPageTemplate->m_bFlag = TRUE;
				}
				else
					p2ndPageTemplate = NULL;

				pPageTemplate->m_bFlag = TRUE;
			}

			nCurPageNum = pPageSourceHeader->m_nPageNumber; 

			SEPARATIONINFO* pExistSepInfo = NULL;
			BOOL			bBreak		  = FALSE;
			if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
			{
				if (pExistSepInfo = pPageTemplate->GetSeparationInfo(pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(pPageSourceHeader->m_strColorName)))	// separation already existing?
				{
					if ( ! bOccupiedWarned)
					{
						CString strMsg, strPageInfo = pPageTemplate->m_strPageID;
						if (pPageSourceHeader->m_strColorName != _T("Composite"))
							strPageInfo += _T(" (") + pPageSourceHeader->m_strColorName + _T(")");
						AfxFormatString1(strMsg, IDS_PAGEOCCUPIED_MESSAGE, strPageInfo);
						switch (KIMOpenLogMessage(strMsg, MB_YESNO, IDYES))
						{
							case IDYES:		bOccupiedWarned = TRUE; break;
							case IDNO:		bBreak = TRUE; 			break;
						}
						if (bBreak)
							break;
					}
				}
			}
			else
				if (pPageTemplate->GetSeparationInfo(pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(pPageSourceHeader->m_strColorName)))	
					continue;															// separation already existing -> AutoServer skips

			CColorDefinition colorDef(pPageSourceHeader->m_strColorName, pPageSourceHeader->m_rgb, CColorDefinition::Spot, pPageSourceHeader->m_nColorIndex);
			SEPARATIONINFO	 sepInfo;
			sepInfo.m_nPageSourceIndex		= pDoc->m_PageSourceList.GetPageSourceIndex(pPageSource);
			sepInfo.m_nPageNumInSourceFile	= (short)(pPageSource->GetHeaderIndex(pPageSourceHeader) + 1);
			sepInfo.m_nColorDefinitionIndex = pDoc->m_ColorDefinitionTable.InsertColorDef(colorDef);

			if (pPageTemplate)
			{
//				pPageTemplate->m_strPageID.Format(_T("%d"), pPageSourceHeader->m_nPageNumber);
				if (pExistSepInfo)
					*pExistSepInfo = sepInfo;
				else
					pPageTemplate->m_ColorSeparations.Add(sepInfo);
				bAssigned = TRUE;

				if (theApp.m_nGUIStyle != CImpManApp::GUIStandard)		// called by AutoServer
					bKeepContentOffset = (pPageSourceHeader->HasTrimBox()) ? FALSE : TRUE;
				else
				{
					if (bKeepContentOffset == UNDEFINED)	// not yet prompted
						if (pPageTemplate->HasContentOffset() || pPageTemplate->IsScaled())
							if (KIMOpenLogMessage(IDS_KEEP_CONTENTOFFSET_PROMPT, MB_YESNO, IDYES) == IDYES)
								bKeepContentOffset = TRUE;
							else
								bKeepContentOffset = FALSE;
				}

				if ( ! bKeepContentOffset || (bKeepContentOffset == UNDEFINED) )
				{
					if (pPageSourceHeader->HasTrimBox())
					{
						if ( ! bTrimBoxWarned && theApp.settings.m_bTrimboxPagesizeMismatchWarning)
						{
							CLayoutObject* pObject = pPageTemplate->GetLayoutObject();
							if (pObject)
							{
								if ( (fabs( (pPageSourceHeader->m_fTrimBoxRight - pPageSourceHeader->m_fTrimBoxLeft)   - pObject->GetRealWidth())  >= 0.5) ||
									 (fabs( (pPageSourceHeader->m_fTrimBoxTop   - pPageSourceHeader->m_fTrimBoxBottom) - pObject->GetRealHeight()) >= 0.5) )
								{
									CDlgTrimboxWarning dlg;
									dlg.m_strText = (pPageSource) ? pPageSource->m_strDocumentFullpath : "";
									dlg.DoModal();
									bTrimBoxWarned = TRUE;
								}
							}
						}
						float fTrimBoxCenterX = pPageSourceHeader->m_fTrimBoxLeft   + (pPageSourceHeader->m_fTrimBoxRight - pPageSourceHeader->m_fTrimBoxLeft)  /2.0f;
						float fTrimBoxCenterY = pPageSourceHeader->m_fTrimBoxBottom + (pPageSourceHeader->m_fTrimBoxTop   - pPageSourceHeader->m_fTrimBoxBottom)/2.0f;
						float fBBoxCenterX	  = pPageSourceHeader->m_fBBoxLeft		+ (pPageSourceHeader->m_fBBoxRight	  - pPageSourceHeader->m_fBBoxLeft)		/2.0f;
						float fBBoxCenterY	  = pPageSourceHeader->m_fBBoxBottom	+ (pPageSourceHeader->m_fBBoxTop	  - pPageSourceHeader->m_fBBoxBottom)	/2.0f;
						pPageTemplate->SetContentOffsetX(fBBoxCenterX - fTrimBoxCenterX);
						pPageTemplate->SetContentOffsetY(fBBoxCenterY - fTrimBoxCenterY);
						if ( (pPageTemplate->GetContentOffsetX() != 0.0f) || (pPageTemplate->GetContentOffsetY() != 0.0f) )
							pPageTemplate->m_nAttribute |= CPageTemplate::ScaleOffsetModified;
					}
					else
					{
						pPageTemplate->SetContentOffsetX(0.0f);
						pPageTemplate->SetContentOffsetY(0.0f);
						pPageTemplate->m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
					}
					pPageTemplate->m_fContentScaleX = 1.0f;
					pPageTemplate->m_fContentScaleY = 1.0f;
				}
			}

			if (p2ndPageTemplate)
			{
//				p2ndPageTemplate->m_strPageID.Format(_T("%d"), pPageSourceHeader->m_nPageNumber);
				if (pExistSepInfo)
					*pExistSepInfo = sepInfo;
				else
					p2ndPageTemplate->m_ColorSeparations.Add(sepInfo);
				bAssigned = TRUE;

				if ( ! bKeepContentOffset || (bKeepContentOffset == UNDEFINED) )
				{
					if (pPageSourceHeader->HasTrimBox())
					{
						float fTrimBoxCenterX = pPageSourceHeader->m_fTrimBoxLeft   + (pPageSourceHeader->m_fTrimBoxRight - pPageSourceHeader->m_fTrimBoxLeft)  /2.0f;
						float fTrimBoxCenterY = pPageSourceHeader->m_fTrimBoxBottom + (pPageSourceHeader->m_fTrimBoxTop   - pPageSourceHeader->m_fTrimBoxBottom)/2.0f;
						float fBBoxCenterX	  = pPageSourceHeader->m_fBBoxLeft		+ (pPageSourceHeader->m_fBBoxRight	  - pPageSourceHeader->m_fBBoxLeft)		/2.0f;
						float fBBoxCenterY	  = pPageSourceHeader->m_fBBoxBottom	+ (pPageSourceHeader->m_fBBoxTop	  - pPageSourceHeader->m_fBBoxBottom)	/2.0f;
						p2ndPageTemplate->SetContentOffsetX(fBBoxCenterX - fTrimBoxCenterX);
						p2ndPageTemplate->SetContentOffsetY(fBBoxCenterY - fTrimBoxCenterY);
						if ( (p2ndPageTemplate->GetContentOffsetX() != 0.0f) || (p2ndPageTemplate->GetContentOffsetY() != 0.0f) )
							p2ndPageTemplate->m_nAttribute |= CPageTemplate::ScaleOffsetModified;
					}
					else
					{
						p2ndPageTemplate->SetContentOffsetX(0.0f);
						p2ndPageTemplate->SetContentOffsetY(0.0f);
						p2ndPageTemplate->m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
					}
					p2ndPageTemplate->m_fContentScaleX = 1.0f;
					p2ndPageTemplate->m_fContentScaleY = 1.0f;
				}
			}
			if (nLabelPageIndex >= 0)
				break;
		}
		if (nLabelPageIndex >= 0)
			break;

		if (startPos)
		{
			// look if same startpage exists more than once - if yes assign again
			nCurPageNum	= pPageSource->m_PageSourceHeaders[0].m_nPageNumber;
			startPos = pDoc->m_PageTemplateList.FindTemplatePos(strStartPage, startPos, nProductPartIndex);	
			if (startPos)
				pPageTemplate = &pDoc->m_PageTemplateList.CList::GetNext(startPos);
		}
	}
	while (startPos);

	pDoc->m_PageTemplateList.ResetFlags();

	pDoc->m_PageSourceList.InitializePageTemplateRefs();

	if (bReorganize)
	{
		pDoc->m_PageTemplateList.ReorganizeColorInfos();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->SetModifiedFlag(TRUE, NULL, TRUE, FALSE);										
	}

#ifdef KIM_ENGINE
	if ( ! bAssigned)
	{
		TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
		_tsplitpath((LPCTSTR)pPageSource->m_strDocumentFullpath, szDrive, szDir, szFname, szExt);
		CString strMsg;
		strMsg.Format(_T("Cannot assign file '%s%s' to job template '%s'"), szFname, szExt, pDoc->GetTitle());
		theApp.LogEntry(_T("   ") + strMsg, TRUE);
	}
#endif

	return bAssigned;
}

void CPageTemplateList::CheckPageSourceHeaders()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);

		for (int i = 0; i < rPageTemplate.m_ColorSeparations.GetSize(); i++)
		{
			if ( ! rPageTemplate.GetObjectSourceHeader(rPageTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex) )
			{
				rPageTemplate.m_ColorSeparations.RemoveAt(i);
				i--;
			}
		}
	}
}

int	CPageTemplateList::GetNumUnassignedPages()
{
	int nNum = 0;
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);
		if ( ! rPageTemplate.m_ColorSeparations.GetSize())
			nNum++;
	}
	return nNum;
}

int CPageTemplateList::GetNumUnimposedPages(int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nImposedPages = 0;
	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;

		if (pDoc->m_Bookblock.GetBindingProcess(nProductPartIndex).m_bComingAndGoing)	
			nImposedPages += rFoldSheet.m_nPagesLeft;
		else
			nImposedPages += rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight;
	}

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
	if ( ! rProductPart.IsNull())
		return rProductPart.m_nNumPages - nImposedPages;
	else
		return pDoc->GetBoundProduct((nProductPartIndex < 0) ? -nProductPartIndex - 1 : 0).GetNumPages() - nImposedPages;
}

int CPageTemplateList::GetNumUnplacedPages(int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nNumUnplacedPages = 0;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetNext(pos);
		if (rTemplate.IsPartOfBoundProduct(nProductPartIndex, 7))
			if ( ! rTemplate.GetPrintSheet(0))
				nNumUnplacedPages++;
	}

	return nNumUnplacedPages;
}

void CPageTemplateList::CalcPagePreviewSize(int nPageIndex, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (IsEmpty())
	{
		m_TemplateListSize = CSize(10,10);
		return;
	}

	if (nPageIndex >= 0)
		if ( ! (nPageIndex%2) && (nPageIndex > 0) )		// always start with left page
			nPageIndex--;

	float		   fMaxLeftWidth = 10.0f, fMaxRightWidth = 10.0f, fLeftHeight = 10.0f, fRightHeight = 10.0f, fTotalHeight = 10.0f;
	CLayoutObject* pObject;
	POSITION pos = NULL;
	if (nPageIndex >= 0)
		pos = FindIndex(nPageIndex, nProductPartIndex);
	else
		pos	= pDoc->m_PageTemplateList.GetHeadPosition();
	if (pos)
	{
		float fXMargin, fYMargin;
		if (nPageIndex > 0)
		{
			CPageTemplate&		rLeftTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
			CBoundProductPart&	rProductPart  = pDoc->m_boundProducts.GetProductPart(rLeftTemplate.m_nProductPartIndex);
			CBoundProduct&		rProduct	  = rProductPart.GetBoundProduct();
			pObject							  = rLeftTemplate.GetLayoutObject();
			fXMargin = fYMargin = 0.0f;
				if ( ! rLeftTemplate.IsFlatProduct())
				{
					fXMargin = rProductPart.m_trimmingParams.m_fSideTrim + rProduct.m_bindingParams.GetSpine(rProductPart) + rProduct.m_bindingParams.GetOverfold(LEFT);
					fYMargin = rProductPart.m_trimmingParams.m_fHeadTrim + rProductPart.m_trimmingParams.m_fFootTrim;
					if (pObject)
					{
						fMaxLeftWidth = max(fMaxLeftWidth, pObject->GetRealWidth() + fXMargin);
						fLeftHeight	  = pObject->GetRealHeight() + fYMargin;
					}
					else
					{
						fMaxLeftWidth = max(fMaxLeftWidth, rProductPart.m_fPageWidth + fXMargin);
						fLeftHeight   = rProductPart.m_fPageHeight + fYMargin;
					}
				}
				else
				{
					int nLabelIndex = pDoc->m_PageTemplateList.GetIndex(&rLeftTemplate, rLeftTemplate.m_nProductPartIndex); 
					fXMargin = pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fLeftTrim   + pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fRightTrim;
					fYMargin = pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fBottomTrim + pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fTopTrim;
					if (pObject)
					{
						fMaxLeftWidth = max(fMaxLeftWidth, pObject->GetRealWidth() + fXMargin);
						fLeftHeight	  = pObject->GetRealHeight() + fYMargin;
					}
					else
					{
						fMaxLeftWidth = max(fMaxLeftWidth, pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fTrimSizeWidth + fXMargin);
						fLeftHeight   = pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fTrimSizeHeight + fYMargin;
					}
				}
		}

		if (pos)
		{
			CPageTemplate&		rRightTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
			CBoundProductPart&	rProductPart   = pDoc->m_boundProducts.GetProductPart(rRightTemplate.m_nProductPartIndex);
			CBoundProduct&		rProduct	   = rProductPart.GetBoundProduct();
			pObject							   = rRightTemplate.GetLayoutObject();
			if ( ! pos && (rRightTemplate.m_nMultipageGroupNum >= 0) )	// if last page is partner of 1st page
			{
				fTotalHeight += 10.0f;
			}
			else
			{
				if ( ! rRightTemplate.IsFlatProduct())
				{
					fXMargin = rProductPart.m_trimmingParams.m_fSideTrim + rProduct.m_bindingParams.GetSpine(rProductPart) + rProduct.m_bindingParams.GetOverfold(RIGHT);
					fYMargin = rProductPart.m_trimmingParams.m_fHeadTrim + rProductPart.m_trimmingParams.m_fFootTrim;
				}
				else
				{
					int nLabelIndex = pDoc->m_PageTemplateList.GetIndex(&rRightTemplate, rRightTemplate.m_nProductPartIndex); 
					fXMargin = pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fLeftTrim   + pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fRightTrim;
					fYMargin = pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fBottomTrim + pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fTopTrim;
				}
				if (pObject)
				{
					fMaxRightWidth = max(fMaxRightWidth, pObject->GetRealWidth() + fXMargin); 
					fRightHeight   = pObject->GetRealHeight() + fYMargin;
				}
				else
				{
					if ( ! rRightTemplate.IsFlatProduct())
					{
						fMaxRightWidth = max(fMaxRightWidth, rProductPart.m_fPageWidth + fXMargin);
						fRightHeight   = rProductPart.m_fPageHeight + fYMargin;
					}
					else
					{
						int nLabelIndex = pDoc->m_PageTemplateList.GetIndex(&rRightTemplate, rRightTemplate.m_nProductPartIndex); 
						fMaxRightWidth = max(fMaxRightWidth, pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fTrimSizeWidth + fXMargin);
						fRightHeight   = pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fTrimSizeHeight + fYMargin;
					}
				}
			}
		}
	}

	fTotalHeight = max(fLeftHeight, fRightHeight);// + 10.0f;

	m_TemplateListSize	   = CSize(CPrintSheetView::WorldToLP(fMaxLeftWidth + fMaxRightWidth/* + 80.0f*/), (CPrintSheetView::WorldToLP(fTotalHeight)));
	m_lTemplateListCenterX = CPrintSheetView::WorldToLP(fMaxLeftWidth/* + 40.0f*/);
}

void CPageTemplateList::CalcSinglePagePreviewSize(int nPageIndex, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (IsEmpty())
	{
		m_TemplateListSize = CSize(10,10);
		return;
	}
	if (nPageIndex < 0)
		return;

	float		   fMaxLeftWidth = 10.0f, fMaxRightWidth = 10.0f, fLeftHeight = 10.0f, fRightHeight = 10.0f, fTotalHeight = 10.0f;
	CLayoutObject* pObject;
	POSITION pos = FindIndex(nPageIndex, nProductPartIndex);
	if (pos)
	{
		float fXMargin, fYMargin;
		CPageTemplate&		rTemplate	 = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
		CBoundProductPart&	rProductPart = pDoc->m_boundProducts.GetProductPart(rTemplate.m_nProductPartIndex);
		CBoundProduct&		rProduct	 = rProductPart.GetBoundProduct();
		pObject							 = rTemplate.GetLayoutObject();
		fXMargin = fYMargin = 0.0f;
		if ( ! rTemplate.IsFlatProduct())
		{
			fXMargin = rProductPart.m_trimmingParams.m_fSideTrim + rProduct.m_bindingParams.GetSpine(rProductPart) + rProduct.m_bindingParams.GetOverfold(LEFT);
			fYMargin = rProductPart.m_trimmingParams.m_fHeadTrim + rProductPart.m_trimmingParams.m_fFootTrim;
			if (pObject)
			{
				fMaxLeftWidth = max(fMaxLeftWidth, pObject->GetRealWidth() + fXMargin);
				fLeftHeight	  = pObject->GetRealHeight() + fYMargin;
			}
			else
			{
				fMaxLeftWidth = max(fMaxLeftWidth, rProductPart.m_fPageWidth + fXMargin);
				fLeftHeight   = rProductPart.m_fPageHeight + fYMargin;
			}
		}
		else
		{
			fXMargin = pDoc->m_flatProducts.FindByIndex(nPageIndex/2).m_fLeftTrim   + pDoc->m_flatProducts.FindByIndex(nPageIndex/2).m_fRightTrim;
			fYMargin = pDoc->m_flatProducts.FindByIndex(nPageIndex/2).m_fBottomTrim + pDoc->m_flatProducts.FindByIndex(nPageIndex/2).m_fTopTrim;
			if (pObject)
			{
				fMaxLeftWidth = max(fMaxLeftWidth, pObject->GetRealWidth() + fXMargin);
				fLeftHeight	  = pObject->GetRealHeight() + fYMargin;
			}
			else
			{
				fMaxLeftWidth = max(fMaxLeftWidth, pDoc->m_flatProducts.FindByIndex(nPageIndex/2).m_fTrimSizeWidth + fXMargin);
				fLeftHeight   = pDoc->m_flatProducts.FindByIndex(nPageIndex/2).m_fTrimSizeHeight + fYMargin;
			}
		}
	}

	fTotalHeight = max(fLeftHeight, fRightHeight);// + 10.0f;

	m_TemplateListSize	   = CSize(CPrintSheetView::WorldToLP(fMaxLeftWidth + fMaxRightWidth/* + 80.0f*/), (CPrintSheetView::WorldToLP(fTotalHeight)));
	m_lTemplateListCenterX = CPrintSheetView::WorldToLP(fMaxLeftWidth/* + 40.0f*/);
}

void CPageTemplateList::CalcLabelPreviewSize(int nLabelIndex, BOOL bShowTrims)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (IsEmpty())
	{
		m_TemplateListSize = CSize(10,10);
		return;
	}
	if (nLabelIndex < 0)
		return;

	int nFlatProductsIndex = pDoc->GetFlatProductsIndex();

	float		   fMaxLeftWidth = 10.0f, fMaxRightWidth = 10.0f, fLeftHeight = 10.0f, fRightHeight = 10.0f, fTotalHeight = 10.0f;
	CLayoutObject* pObject;

	int		 nIndex = nLabelIndex;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetAt(pos);
		if (rTemplate.IsFlatProduct())
			nIndex--;
		if (nIndex < 0)
			break;
		CList::GetNext(pos);
	}

	if (pos)
	{
		float fXMargin = 0.0f, fYMargin = 0.0f;
		CPageTemplate&		rTemplate	 = pDoc->m_PageTemplateList.GetNext(pos, nFlatProductsIndex);
		CBoundProductPart&  rProductPart = pDoc->m_boundProducts.GetProductPart(rTemplate.m_nProductPartIndex);
		CBoundProduct&		rProduct	 = rProductPart.GetBoundProduct();
		pObject							 = rTemplate.GetLayoutObject();
		fXMargin = fYMargin = 0.0f;
		if ( ! rTemplate.IsFlatProduct())
		{
			fXMargin = (bShowTrims) ? (rProductPart.m_trimmingParams.m_fSideTrim + rProduct.m_bindingParams.GetSpine(rProductPart) + rProduct.m_bindingParams.GetOverfold(LEFT)) : 0.0f;
			fYMargin = (bShowTrims) ? (rProductPart.m_trimmingParams.m_fHeadTrim + rProductPart.m_trimmingParams.m_fFootTrim) : 0.0f;

			if (pObject)
			{
				fMaxLeftWidth = max(fMaxLeftWidth, pObject->GetRealWidth() + fXMargin);
				fLeftHeight	  = pObject->GetRealHeight() + fYMargin;
			}
			else
			{
				fMaxLeftWidth = max(fMaxLeftWidth, rProductPart.m_fPageWidth + fXMargin);
				fLeftHeight   = rProductPart.m_fPageHeight + fYMargin;
			}
		}
		else
		{
			fXMargin = (bShowTrims) ? (pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fLeftTrim   + pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fRightTrim) : 0.0f;
			fYMargin = (bShowTrims) ? (pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fBottomTrim + pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fTopTrim)	 : 0.0f;
			if (pObject)
			{
				fMaxLeftWidth = max(fMaxLeftWidth, pObject->GetRealWidth() + fXMargin);
				fLeftHeight	  = pObject->GetRealHeight() + fYMargin;
			}
			else
			{
				fMaxLeftWidth = max(fMaxLeftWidth, pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fTrimSizeWidth + fXMargin);
				fLeftHeight   = pDoc->m_flatProducts.FindByIndex(nLabelIndex/2).m_fTrimSizeHeight + fYMargin;
			}
		}
	}

	fTotalHeight = max(fLeftHeight, fRightHeight);// + 10.0f;

	m_TemplateListSize	   = CSize(CPrintSheetView::WorldToLP(fMaxLeftWidth + fMaxRightWidth/* + 80.0f*/), (CPrintSheetView::WorldToLP(fTotalHeight)));
	m_lTemplateListCenterX = CPrintSheetView::WorldToLP(fMaxLeftWidth/* + 40.0f*/);
}

CRect CPageTemplateList::Draw(CDC* pDC, CRect rcFrame, int nPageIndex, int nProductPartIndex, BOOL bShowTrims, BOOL bShowColors, BOOL bShowPageNums, BOOL bShowShingling, CDisplayList* pDisplayList, BOOL bShowTitle, BOOL bShowMasking, 
							  float fBleed, float fGluelineWidth, float fSpineArea, float fHeadTrim, float fFootTrim, float fSideTrim, BOOL bThumbnail)
{
	CalcPagePreviewSize(nPageIndex, nProductPartIndex);

	pDC->LPtoDP(&rcFrame);

	pDC->SaveDC();
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(m_TemplateListSize);
	pDC->SetWindowOrg(0, m_TemplateListSize.cy);

	float fFactorX = (float)rcFrame.Width() /(float)m_TemplateListSize.cx;
	float fFactorY = (float)rcFrame.Height()/(float)m_TemplateListSize.cy;
	CSize sizeViewport(rcFrame.Width(), (int)((float)m_TemplateListSize.cy * min(fFactorX, fFactorY) ));
	pDC->SetViewportExt(sizeViewport.cx, -sizeViewport.cy);  
	pDC->SetViewportOrg(rcFrame.left, rcFrame.CenterPoint().y - sizeViewport.cy / 2);

	int nTop		= m_TemplateListSize.cy;// - CPrintSheetView::WorldToLP(20);
	int nMinBottom	= nTop;// + CPrintSheetView::WorldToLP(20);

	if ( ! (nPageIndex%2) && (nPageIndex > 0) )		// always start with left page
		nPageIndex--;
	POSITION pos = FindIndex(nPageIndex, nProductPartIndex);

	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CRect rcBoxTmp;

	if (pos)
	{
		if (nPageIndex > 0)						// except 1st (title) page: draw on right side
		{
			CPageTemplate& rLeftTemplate = GetNext(pos, nProductPartIndex);
			//nMinBottom = nTop + CPrintSheetView::WorldToLP(10);
			rcBoxTmp = rLeftTemplate.Draw(pDC, &nTop, &nMinBottom, LEFT,  nPageIndex++, m_lTemplateListCenterX, bShowTrims, bShowColors, bShowPageNums, bShowShingling, RGB(255,255,255), pDisplayList, bShowTitle, bShowMasking, fBleed, fGluelineWidth, fSpineArea, fHeadTrim, fFootTrim, fSideTrim, bThumbnail);
			rcBox.UnionRect(rcBox, rcBoxTmp);
		}
		if (pos)
		{
			CPageTemplate& rRightTemplate = GetNext(pos, nProductPartIndex);
			//nMinBottom = nTop + CPrintSheetView::WorldToLP(10);
			rcBoxTmp = rRightTemplate.Draw(pDC, &nTop, &nMinBottom, RIGHT, nPageIndex++, m_lTemplateListCenterX, bShowTrims, bShowColors, bShowPageNums, bShowShingling, RGB(255,255,255), pDisplayList, bShowTitle, bShowMasking, fBleed, fGluelineWidth, fSpineArea, fHeadTrim, fFootTrim, fSideTrim, bThumbnail);
			rcBox.UnionRect(rcBox, rcBoxTmp);
		}
	}

	pDC->LPtoDP(&rcBox);
	rcBox.NormalizeRect();

	pDC->RestoreDC(-1);

	pDC->DPtoLP(&rcBox);

	return rcBox;
}

CRect CPageTemplateList::DrawSinglePage(CDC* pDC, CRect rcFrame, int nPageIndex, int nProductPartIndex, BOOL bShowTrims, BOOL bShowColors, BOOL bShowPageNums, BOOL bShowBitmap, BOOL bThumbnail)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	CalcSinglePagePreviewSize(nPageIndex, nProductPartIndex);

	POSITION pos = FindIndex(nPageIndex, nProductPartIndex);
	if ( ! pos)
		return rcBox;

	CPageTemplate& rTemplate = GetAt(pos);

	pDC->LPtoDP(&rcFrame);

	pDC->SaveDC();
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(m_TemplateListSize);
	pDC->SetWindowOrg(0, m_TemplateListSize.cy);

	float fFactorX = (float)rcFrame.Width() /(float)m_TemplateListSize.cx;
	float fFactorY = (float)rcFrame.Height()/(float)m_TemplateListSize.cy;
	int nViewportWidth  = rcFrame.Width();
	int nViewportHeight = -(int)((float)m_TemplateListSize.cy * min(fFactorX, fFactorY));
	nViewportWidth  = (abs(nViewportWidth)  <= 0) ?  1 : nViewportWidth;
	nViewportHeight = (abs(nViewportHeight) <= 0) ? -1 : nViewportHeight;
	pDC->SetViewportExt(nViewportWidth, nViewportHeight);  
	pDC->SetViewportOrg(rcFrame.TopLeft());

	int nTop		= m_TemplateListSize.cy - CPrintSheetView::WorldToLP(20);
	int nMinBottom	= nTop + CPrintSheetView::WorldToLP(20);

	rcBox = rTemplate.Draw(pDC, &nTop, &nMinBottom, LEFT, nPageIndex, m_lTemplateListCenterX, bShowTrims, bShowColors, bShowPageNums, FALSE, (bShowBitmap) ? WHITE : BLACK, NULL, FALSE, FALSE, NULL, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, bThumbnail);

	pDC->LPtoDP(&rcBox);
	rcBox.NormalizeRect();

	pDC->RestoreDC(-1);

	pDC->DPtoLP(&rcBox);

	return rcBox;
}

CPoint g_ptProductPart(0,0);
CPoint g_ptFoldSheet(0,0);

CRect CPageTemplateList::DrawPagesList(CDC* pDC, CRect rcFrame, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	int	  nTextHeight = Pixel2MM(pDC, 20);
	CRect rcRow = rcFrame; rcRow.bottom = rcRow.top + nTextHeight;

	CPen grayPen(PS_SOLID, 1, TABLECOLOR_SEPARATION_LINE);
	CPen*  pOldPen	  = pDC->SelectObject(&grayPen);

	BOOL				bShowProductPart= FALSE;
	BOOL				bShowPageFormat	= FALSE;
	BOOL				bShowFoldSheet	= FALSE;
	CBoundProductPart*	pProductPart	= NULL;
	CFoldSheet*			pFoldSheet		= NULL;
	int					nRowIndex		= 0;
	int					nPageIndex		= 0;
	POSITION			pos				= GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetNext(pos);
		if (rTemplate.IsFlatProduct())
			continue;

		if (&rTemplate.GetProductPart() != pProductPart)
		{
			pProductPart = &rTemplate.GetProductPart();
			bShowProductPart = TRUE;
		}
		if (rTemplate.GetFoldSheet() != pFoldSheet)
		{
			pFoldSheet = rTemplate.GetFoldSheet();
			bShowFoldSheet = TRUE;
		}
		CRect bkRect(0, rcRow.top, rcFrame.right, rcRow.bottom);
		pDC->FillSolidRect(bkRect, (nRowIndex%2) ? TABLECOLOR_BACKGROUND : TABLECOLOR_BACKGROUND_DARK);
		//pDC->MoveTo(bkRect.left,  bkRect.top);
		//pDC->LineTo(bkRect.right, bkRect.top);

		CRect rcPagesRow = DrawPagesRow(pDC, rcRow, rTemplate, nPageIndex++, bShowProductPart, bShowPageFormat, bShowFoldSheet, rColumnWidths, pDisplayList);
		nRowIndex++;
		rcRow.top	 = rcPagesRow.bottom;
		rcRow.bottom = rcRow.top + nTextHeight;
		rcBox.UnionRect(rcBox, rcRow);
		rcBox.UnionRect(rcBox, rcPagesRow);
	
		bShowPageFormat  = (bShowProductPart) ? TRUE : FALSE;
		bShowProductPart = FALSE;
		bShowFoldSheet   = FALSE;
	}

	pDC->SelectObject(pOldPen);
	grayPen.DeleteObject();

	return rcBox;
}

CRect CPageTemplateList::DrawPagesRow(CDC* pDC, CRect rcFrame, CPageTemplate& rTemplate, int nPageIndex, BOOL bShowProductPart, BOOL bShowPageFormat, BOOL bShowFoldSheet, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	COLORREF crValueColor = RGB(50,50,50);

	CPrintSheetPlanningView* pView			 = NULL;
	BOOL					 bPageNumeration = FALSE; 
	if (pDisplayList)
		if (pDisplayList->m_pParent)
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
			{
				pView			= (CPrintSheetPlanningView*)pDisplayList->m_pParent;
				bPageNumeration	= (pView->IsOpenPageNumeration()) ? TRUE : FALSE;
			}

	CBoundProductPart*	pProductPart = &rTemplate.GetProductPart();
	CFoldSheet*			pFoldSheet   = rTemplate.GetFoldSheet();

	int nProductPartWidth	= Pixel2MM(pDC, (rColumnWidths.GetSize() > 0) ? rColumnWidths[0] :  80);
	int nPagesWidth			= Pixel2MM(pDC, (rColumnWidths.GetSize() > 1) ? rColumnWidths[1] :  80);
	int nFoldSheetWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 2) ? rColumnWidths[2] : 120);
	int nColorsWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 3) ? rColumnWidths[3] : 100);
	int nVarnishWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 4) ? rColumnWidths[4] :  80);
	int nRegisterWidth		= Pixel2MM(pDC, (rColumnWidths.GetSize() > 5) ? rColumnWidths[5] :  80);
	int nBorder				= Pixel2MM(pDC, 5);

	CFont	smallFont, boldFont;
	smallFont.CreateFont(Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont (Pixel2MM(pDC, 12), 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);

	CString strOut;
	pDC->SetTextColor(crValueColor);

	CRect rcText = rcFrame; rcText.right = rcText.left + nProductPartWidth - nBorder;
	if (bShowProductPart)
	{
		pDC->SelectObject(&boldFont);
		strOut = (pProductPart) ? pProductPart->m_strName : _T("");
		CGraphicComponent gp;
		CRect rcTitle = rcText; rcTitle.DeflateRect(0, 2); rcTitle.right -= nBorder;
		gp.DrawTitleBar(pDC, rcTitle, _T(""), RGB(242, 246, 184), RGB(222, 233, 77), 90, FALSE);
		rcTitle.left += nBorder;
		pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);
		pDC->SelectObject(&smallFont);
		g_ptProductPart.x = rcTitle.right - nBorder;
		g_ptProductPart.y = rcTitle.bottom;
	}
	else
	{
		CPen pen(PS_SOLID, Pixel2MM(pDC, 1), RGB(180, 190, 50));
		CPen* pOldPen = pDC->SelectObject(&pen);
		pDC->MoveTo(g_ptProductPart.x, g_ptProductPart.y);
		pDC->LineTo(g_ptProductPart.x, rcFrame.CenterPoint().y);
		pDC->LineTo(rcText.right, rcFrame.CenterPoint().y);
		pDC->SelectObject(pOldPen);
		pen.DeleteObject();
	}

	pDC->SetTextColor(GUICOLOR_CAPTION);
	rcText.left = rcFrame.left + nProductPartWidth + nBorder; rcText.right = rcText.left + nPagesWidth - nBorder;
	CRect rcThumbnail = rcText; rcThumbnail.right = rcThumbnail.left + Pixel2MM(pDC, 12);
	pDC->DrawIcon(rcThumbnail.TopLeft() + CSize(0, Pixel2MM(pDC, 3)), theApp.LoadIcon(IDI_SHEET_PAGE));//IDI_UNIMPOSED_PAGE));
	rcText.left += Pixel2MM(pDC, 18);

	strOut = rTemplate.m_strPageID;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	if (bPageNumeration)
	{
		strOut.Format(_T("(%d)"), nPageIndex + 1);
		CRect rcText2 = rcText; rcText2.left = rcText2.right - pDC->GetTextExtent(strOut).cx - 2 * nBorder;	
		pDC->SetTextColor(LIGHTRED);
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	}
	if (pDisplayList)
	{
		CRect rcDI	  = rcText; rcDI.left = rcThumbnail.left; rcDI.OffsetRect(-nBorder, 0); 
		CRect rcDIAux = rcText; rcDIAux.OffsetRect(-nBorder, 0); 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDIAux, (void*)&rTemplate, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PageNumber), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (bPageNumeration)
				pDI->m_nState = (pView->IsInsideNumerationRange(nPageIndex)) ? CDisplayItem::Selected : CDisplayItem::Normal;
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	pDC->SetTextColor(crValueColor);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nFoldSheetWidth - nBorder;
	if (bShowFoldSheet && pFoldSheet)
	{
		rcThumbnail = CRect(rcText.TopLeft() + CSize(-nBorder, Pixel2MM(pDC, 1)), CSize(rcText.Height(), Pixel2MM(pDC, 35)));
		if (pFoldSheet->HasFoldSheetInside())
			pFoldSheet->DrawThumbnail(pDC, rcText, FALSE, FALSE, -1, (pFoldSheet->IsLeftFromSpine(nPageIndex)) ? LEFT : RIGHT);
		else
			pFoldSheet->DrawThumbnail(pDC, rcText, FALSE, FALSE, -1, BOTHSIDES);

		g_ptFoldSheet.x = rcText.left + nBorder;
		g_ptFoldSheet.y = rcText.bottom;

		pDC->SetTextColor(RGB(255, 150, 50));
		pDC->SelectObject(&boldFont);
		rcText.left += Pixel2MM(pDC, 40); 
		strOut = (pFoldSheet) ? pFoldSheet->GetNumber() : _T("");
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);
		pDC->SelectObject(&smallFont);
	}
	else
		if (pFoldSheet)
		{
			CPen pen(PS_SOLID, Pixel2MM(pDC, 1), RGB(200,200,255));
			CPen* pOldPen = pDC->SelectObject(&pen);
			pDC->MoveTo(g_ptFoldSheet.x, g_ptFoldSheet.y);
			pDC->LineTo(g_ptFoldSheet.x, rcFrame.CenterPoint().y);
			pDC->LineTo(rcText.left - nBorder, rcFrame.CenterPoint().y);
			pDC->SelectObject(pOldPen);
			pen.DeleteObject();
		}

	pDC->SetTextColor(crValueColor);

	rcText.left = rcText.right + nBorder; rcText.right = rcText.left + nColorsWidth - nBorder;
	CRect rcColors = rcText; rcColors.left -= nBorder; rcColors.OffsetRect(0, -nBorder);
	rcColors = rTemplate.DrawColorants(pDC, rcColors);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);
	if (rcColors.Width() < 2)
	{
		strOut = _T("-");
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	}

	//rcText.left = rcText.right; rcText.right = rcText.left + Pixel2MM(pDC, 20);
	//if (rTemplate.GetVarnishInfo())
	//	pDC->DrawIcon(rcText.CenterPoint() - CSize(5, 5), theApp.LoadIcon(IDI_CHECK));
	//else
	//{
	//	strOut = _T("-");
	//	pDC->DrawText(strOut, rcText, DT_CENTER | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	//}
	//rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
	//rcBox.bottom = max(rcBox.bottom, rcText.bottom);
	//if (pDisplayList)
	//{
	//	CRect rcDI = rcText; rcDI.DeflateRect(0, 2);
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)&rTemplate, DISPLAY_ITEM_REGISTRY(CProductDataView, PageVarnish), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//	}
	//}

	//rcText.left = rcText.left + nVarnishWidth + nBorder; rcText.right = rcText.left + nRegisterWidth - nBorder;
	//strOut = _T("-");
	//pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	//rcBox.right  = rcText.left + pDC->GetTextExtent(strOut).cx;
	//rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	smallFont.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CPageTemplateList::DrawFlatProduct(CDC* pDC, CRect rcFrame, int nLabelIndex, BOOL bShowTrims, BOOL bShowColors, BOOL bShowPageNums, BOOL bShowBitmap, BOOL bThumbnail)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	CalcLabelPreviewSize(nLabelIndex, bShowTrims);

	int		 nIndex = nLabelIndex;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = CList::GetAt(pos);
		if (rTemplate.IsFlatProduct())
			nIndex--;
		if (nIndex < 0)
			break;
		CList::GetNext(pos);
	}

	if ( ! pos)
		return rcBox;

	CPageTemplate& rTemplate = GetAt(pos);

	pDC->LPtoDP(&rcFrame);

	float fFactorX = (float)rcFrame.Width() /(float)m_TemplateListSize.cx;
	float fFactorY = (float)rcFrame.Height()/(float)m_TemplateListSize.cy;
	int nViewportWidth  = rcFrame.Width();
	int nViewportHeight = -(int)((float)m_TemplateListSize.cy * min(fFactorX, fFactorY));

	nViewportWidth  = (abs(nViewportWidth)  < 8) ?  8 : nViewportWidth;
	nViewportHeight = (abs(nViewportHeight) < 8) ? -8 : nViewportHeight;

	pDC->SaveDC();
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(m_TemplateListSize);
	pDC->SetWindowOrg(0, m_TemplateListSize.cy);

	pDC->SetViewportExt(nViewportWidth, nViewportHeight);  
	pDC->SetViewportOrg(rcFrame.TopLeft());

	int nTop		= m_TemplateListSize.cy - CPrintSheetView::WorldToLP(20);
	int nMinBottom	= nTop + CPrintSheetView::WorldToLP(20);

	rcBox = rTemplate.Draw(pDC, &nTop, &nMinBottom, LEFT, nLabelIndex, m_lTemplateListCenterX, bShowTrims, bShowColors, bShowPageNums, FALSE, (bShowBitmap) ? WHITE : BLACK, NULL, FALSE, FALSE, NULL, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, bThumbnail);

	pDC->LPtoDP(&rcBox);
	rcBox.NormalizeRect();
	pDC->RestoreDC(-1);

	pDC->DPtoLP(&rcBox);

	return rcBox;
}

void CPageTemplateList::DoNumber(BOOL bRangeAll, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nCurNum = (nStartNum - 1) * CDlgNumeration::GetSeqLen(strStyle) + 1;
	int i		= 0;
	POSITION pos = pDoc->m_PageTemplateList.GetHeadPosition(nProductPartIndex);
	while (pos)
	{
		CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);
		if (rTemplate.PageNumHasAlpha())	// if page id has alpha character(s) -> skip because numeration has been changed (i.e. to roman)
			continue;

		if ( bRangeAll || ((i + 1 >= nRangeFrom) && (i + 1 <= nRangeTo)) )
		{
			int nIndex = pDoc->m_PageTemplateList.GetIndex(&rTemplate/*, nProductPartIndex*/) + 1;
			if (nIndex > nCurNum)
				rTemplate.m_strPageID = CDlgNumeration::GenerateNumber(nIndex, strStyle, 0);
			else
				rTemplate.m_strPageID = CDlgNumeration::GenerateNumber(nIndex, strStyle, nCurNum - nIndex);
			nCurNum += (bReverse) ? -1 : 1;
		}
		i++;
	}
}

CString CPageTemplateList::GetPageRange(int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return _T("");

	CArray <INT_PTR, INT_PTR> pageIndices;
	int		 nPageIndex = -1;
	POSITION pos		= GetHeadPosition();
	while (pos)
	{
		nPageIndex++;
		CPageTemplate& rPageTemplate = CList::GetNext(pos);
		if (rPageTemplate.m_nProductPartIndex != nProductPartIndex)
			continue;
		pageIndices.Add(nPageIndex);
	}

	if ( ! pageIndices.GetSize())
		return _T("");

	CString strPageRange;
	CString strPageRanges;
	int		nStartIndex = 0;
	int		i = 1;
	for (; i < pageIndices.GetSize(); i++)
	{
		if (pageIndices[i - 1] != pageIndices[i] - 1)
		{
			POSITION posStart = FindIndex(pageIndices[nStartIndex], nProductPartIndex);
			POSITION posEnd   = FindIndex(pageIndices[i - 1], nProductPartIndex);
			if (posStart && posEnd)
			{
				strPageRange.Format(_T("%s%s - %s"), (nStartIndex == 0) ? _T("") : _T(" + "), GetAt(posStart).m_strPageID, GetAt(posEnd).m_strPageID );
				strPageRanges += strPageRange;
			}
			nStartIndex = i;
		}
	}

	POSITION posStart = FindIndex(pageIndices[nStartIndex], nProductPartIndex);
	POSITION posEnd   = FindIndex(pageIndices[i - 1], nProductPartIndex);
	if (posStart && posEnd)
	{
		strPageRange.Format(_T("%s%s - %s"), (nStartIndex == 0) ? _T("") : _T(" + "), GetAt(posStart).m_strPageID, GetAt(posEnd).m_strPageID );
		strPageRanges += strPageRange;
	}

	return strPageRanges;
}

CString	CPageTemplateList::GetPageRange(CFoldSheet* pFoldSheet, CLayout* pLayout, INT_PTR* pNumPages)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return _T("");
	if ( ! pFoldSheet)
		return _T("");

	CArray <CFoldSheet*, CFoldSheet*> foldSheetGroup;
	pDoc->m_Bookblock.InitSortedFoldSheets(foldSheetGroup, pFoldSheet->m_nProductPartIndex, pFoldSheet, pLayout);

	CArray <int, int> pageIndices;
	int		 nPageIndex = -1;
	POSITION pos		= GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);
		nPageIndex++;

		CFoldSheet* pRefFoldSheet = rPageTemplate.GetFoldSheet();
		if ( ! pRefFoldSheet)
			continue;
		if (pRefFoldSheet->m_strFoldSheetTypeName != pFoldSheet->m_strFoldSheetTypeName)
			continue;
		if (pLayout)
		{
			int nLayerIndex = 0;
			if (pRefFoldSheet->GetLayout(nLayerIndex) != pLayout)
				continue;
		}
		pageIndices.Add(nPageIndex);
	}

	if (pNumPages)
		*pNumPages = pageIndices.GetSize();

	if ( ! pageIndices.GetSize())
		return _T("");

	CString strPageRange;
	CString strPageRanges;
	int		nStartIndex = 0;
	int		i = 1;
	for (; i < pageIndices.GetSize(); i++)
	{
		if (pageIndices[i - 1] != pageIndices[i] - 1)
		{
			POSITION posStart = FindIndex(pageIndices[nStartIndex]);
			POSITION posEnd   = FindIndex(pageIndices[i - 1]);
			if (posStart && posEnd)
			{
				strPageRange.Format(_T("%s%s - %s"), (nStartIndex == 0) ? _T("") : _T(" + "), GetAt(posStart).m_strPageID, GetAt(posEnd).m_strPageID );
				strPageRanges += strPageRange;
			}
			nStartIndex = i;
		}
	}

	POSITION posStart = FindIndex(pageIndices[nStartIndex]);
	POSITION posEnd   = FindIndex(pageIndices[i - 1]);
	if (posStart && posEnd)
	{
		strPageRange.Format(_T("%s%s - %s"), (nStartIndex == 0) ? _T("") : _T(" + "), GetAt(posStart).m_strPageID, GetAt(posEnd).m_strPageID );
		strPageRanges += strPageRange;
	}

	return strPageRanges;
}

CString CPageTemplateList::FindFreeMarkName(CString strNewMark)
{
	CString strFinal = strNewMark;
	int nCount = 1;
	int i	   = 0;
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = CList::GetNext(pos);
		if (rPageTemplate.IsPage() || rPageTemplate.IsFlatProduct())
			continue;

		if (strFinal == rPageTemplate.m_strPageID)	// mark name exists
		{
			strFinal.Format(_T("%s (%d)"), strNewMark, nCount++);
			i = 0;
		}
		else
			i++;
	}

	return strFinal;
}

void CPageTemplateList::SortPages()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CArray <int, int> pageIndices;
	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		
		if (rBoundProduct.m_bindingParams.m_bComingAndGoing)
			return;

		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
			pageIndices.Add(0);
	}

	CPageTemplateList tmpPageTemplateList;
	tmpPageTemplateList.CList::AddHead(this);
	tmpPageTemplateList.ResetFlags();
	tmpPageTemplateList.InvalidateProductRefs();
	
	RemoveAll();

	CArray <short*, short*> PageSequence;
	pos = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem = pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		CFoldSheet&	rFoldSheet = rItem.GetFoldSheet(&pDoc->m_Bookblock);

		rFoldSheet.GetPageSequence(PageSequence, rItem.m_nFoldSheetPart);

		int nProductIndex = pDoc->GetBoundProductPart(rFoldSheet.m_nProductPartIndex).GetBoundProduct().GetIndex();
		int	nPrevPageNum = -1;
		for (int i = 0; i < PageSequence.GetSize(); i++)
		{
			if (*PageSequence[i] > 0)	// blank page if null
			{
				if (*PageSequence[i] != nPrevPageNum)
				{
					if ( (rFoldSheet.m_nProductPartIndex >= 0) && (rFoldSheet.m_nProductPartIndex < pageIndices.GetSize()) )
					{
						POSITION srcPos = tmpPageTemplateList.FindIndex(pageIndices[rFoldSheet.m_nProductPartIndex], rFoldSheet.m_nProductPartIndex);
						if (srcPos)
						{
							CPageTemplate& rSrcTemplate = tmpPageTemplateList.GetAt(srcPos);
							if ( ! rSrcTemplate.m_bPageNumLocked)	
								rSrcTemplate.m_strPageID.Format(_T("%d"), *PageSequence[i]);
							
							AddTail(rSrcTemplate, -nProductIndex - 1);

							rSrcTemplate.m_bFlag = TRUE;
						}
						nPrevPageNum = *PageSequence[i];
						pageIndices[rFoldSheet.m_nProductPartIndex]++;
					}
				}
			}
		}
	}

	int nProductIndex	  = 0;
	int nProductPartIndex = 0;
	pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);

		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			int nPageNum = 1;
			if (GetCount(nProductPartIndex) > 0)
			{
				CPageTemplate& rLastPageTemplate = GetTail(nProductPartIndex);
				if (rLastPageTemplate.PageNumHasAlpha())
					nPageNum = GetIndex(&rLastPageTemplate, nProductPartIndex) + 1;
				else
					nPageNum = (long)Ascii2Float(rLastPageTemplate.m_strPageID) + 1;
			}
			POSITION pos2 = tmpPageTemplateList.GetHeadPosition(nProductPartIndex);	// append page templates which are not member of the bookblock
			while (pos2)
			{
				CPageTemplate& rSrcTemplate = tmpPageTemplateList.GetNext(pos2, nProductPartIndex);
				if (rSrcTemplate.m_bFlag)
					continue;

				if (rSrcTemplate.IsPage())
					if ( ! rSrcTemplate.m_bPageNumLocked)	
						rSrcTemplate.m_strPageID.Format(_T("%d"), nPageNum);
				nPageNum++;
				AddTail(rSrcTemplate, -nProductIndex - 1);
			}

			nProductPartIndex++;
		}
		nProductIndex++;
	}

	int nFlatProductsClassIndex = pDoc->GetFlatProductsIndex();
	pos = tmpPageTemplateList.GetHeadPosition(nFlatProductsClassIndex);	// append flat products page templates 
	while (pos)
	{
		CPageTemplate& rSrcTemplate = tmpPageTemplateList.GetNext(pos, nFlatProductsClassIndex);
		AddTail(rSrcTemplate, nFlatProductsClassIndex);
	}
}

int CPageTemplateList::GetLongestPageNumString(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int		 nMaxLen = 0;
	POSITION pos	 = pDoc->m_PageTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = pDoc->m_PageTemplateList.CList::GetNext(pos);
		int nLen = pDC->GetTextExtent(rTemplate.m_strPageID).cx;
		nMaxLen = max(nMaxLen, nLen);
	}

	return nMaxLen;
}

BOOL CPageTemplateList::IsUnpopulated()
{
	if (GetNumUnassignedPages() == GetCount())
		return TRUE;
	else
		return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CPageTemplate

IMPLEMENT_SERIAL(CPageTemplate, CObject, VERSIONABLE_SCHEMA | 16)

CPageTemplate::CPageTemplate()
{
	m_bPageNumLocked			= FALSE;
	m_bMap2AllColors			= FALSE;
	m_bMap2SpotColors			= FALSE;
//	m_nMap2SpotColorIndex		= -1;
	m_bLabelBackSide			= FALSE;
	m_bAdjustContentToMask		= FALSE;
	m_nProductIndex				= -1;
	m_nProductPartIndex			= -1;
	m_bContentDuplicate			= FALSE;
	m_bContentTrim				= FALSE;
	m_fContentGridX				= 0.0f;
	m_fContentGridY				= 0.0f;
	m_nContentPivotX			= XCENTER;
	m_nContentPivotY			= YCENTER;
	m_fShinglingOffsetX			= 0.0f;
	m_fShinglingOffsetXFoot		= 0.0f,
	m_fShinglingOffsetY			= 0.0f;
	m_bShinglingOffsetLocked	= FALSE;

	m_pFoldSheet				= NULL;
}


CPageTemplate::CPageTemplate(const CPageTemplate& rPageTemplate) 
{
	Copy(rPageTemplate);
}


// operator overload for CPageTemplate - needed by CList functions:
const CPageTemplate& CPageTemplate::operator=(const CPageTemplate& rPageTemplate)
{
	Copy(rPageTemplate);	

	return *this;
}


BOOL CPageTemplate::operator==(const CPageTemplate& rPageTemplate)
{
	if (m_strPageID					!= rPageTemplate.m_strPageID)
		return FALSE;
	if (m_bPageNumLocked			!= rPageTemplate.m_bPageNumLocked)
		return FALSE;
	if (m_nPageType					!= rPageTemplate.m_nPageType)
		return FALSE;
	if (m_nMultipageGroupNum		!= rPageTemplate.m_nMultipageGroupNum)              
		return FALSE;
	if (m_fContentOffsetX			!= rPageTemplate.m_fContentOffsetX)
		return FALSE;
	if (m_fContentOffsetY			!= rPageTemplate.m_fContentOffsetY)
		return FALSE;
	if (m_fContentScaleX			!= rPageTemplate.m_fContentScaleX)
		return FALSE;
	if (m_fContentScaleY			!= rPageTemplate.m_fContentScaleY)
		return FALSE;
	if (m_bContentDuplicate			!= rPageTemplate.m_bContentDuplicate)
		return FALSE;
	if (m_bContentTrim				!= rPageTemplate.m_bContentTrim)
		return FALSE;
	if (m_fContentGridX				!= rPageTemplate.m_fContentGridX)
		return FALSE;
	if (m_fContentGridY				!= rPageTemplate.m_fContentGridY)
		return FALSE;
	if (m_fContentRotation			!= rPageTemplate.m_fContentRotation)
		return FALSE;
	if (m_fShinglingOffsetX			!= rPageTemplate.m_fShinglingOffsetX)
		return FALSE;
	if (m_fShinglingOffsetXFoot		!= rPageTemplate.m_fShinglingOffsetXFoot)
		return FALSE;
	if (m_fShinglingOffsetY			!= rPageTemplate.m_fShinglingOffsetY)
		return FALSE;
	if (m_bShinglingOffsetLocked	!= rPageTemplate.m_bShinglingOffsetLocked)
		return FALSE;
	if (m_nContentPivotX			!= rPageTemplate.m_nContentPivotX)
		return FALSE;
	if (m_nContentPivotY			!= rPageTemplate.m_nContentPivotY)
		return FALSE;
	if (m_fLeftPageMargin			!= rPageTemplate.m_fLeftPageMargin)
		return FALSE;
	if (m_fRightPageMargin			!= rPageTemplate.m_fRightPageMargin)
		return FALSE;
	if (m_fLowerPageMargin			!= rPageTemplate.m_fLowerPageMargin)
		return FALSE;
	if (m_fUpperPageMargin			!= rPageTemplate.m_fUpperPageMargin)
		return FALSE;
	if (m_nAttribute				!= rPageTemplate.m_nAttribute)                      
		return FALSE;
	if (m_bMap2AllColors			!= rPageTemplate.m_bMap2AllColors)
		return FALSE;
	if (m_bMap2SpotColors			!= rPageTemplate.m_bMap2SpotColors)
		return FALSE;
	if (m_bLabelBackSide			!= rPageTemplate.m_bLabelBackSide)
		return FALSE;
	if (m_bAdjustContentToMask		!= rPageTemplate.m_bAdjustContentToMask)
		return FALSE;
	if (m_nProductIndex				!= rPageTemplate.m_nProductIndex)
		return FALSE;
	if (m_nProductPartIndex			!= rPageTemplate.m_nProductPartIndex)
		return FALSE;

	if (m_bMap2AllColors)
	{
		if ( ! m_ColorSeparations.GetSize() || ! rPageTemplate.m_ColorSeparations.GetSize())
			return FALSE;
		if (m_ColorSeparations[0].m_nPageSourceIndex	  != rPageTemplate.m_ColorSeparations[0].m_nPageSourceIndex)
			return FALSE;
		if (m_ColorSeparations[0].m_nPageNumInSourceFile  != rPageTemplate.m_ColorSeparations[0].m_nPageNumInSourceFile)
			return FALSE;
	}
	else
	{
		if (m_ColorSeparations.GetSize() != rPageTemplate.m_ColorSeparations.GetSize())
			return FALSE;
		for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
		{
			if (m_ColorSeparations[i].m_nPageSourceIndex	  != rPageTemplate.m_ColorSeparations[i].m_nPageSourceIndex)
				return FALSE;
			if (m_ColorSeparations[i].m_nPageNumInSourceFile  != rPageTemplate.m_ColorSeparations[i].m_nPageNumInSourceFile)
				return FALSE;
			if (m_ColorSeparations[i].m_nColorDefinitionIndex != rPageTemplate.m_ColorSeparations[i].m_nColorDefinitionIndex)
				return FALSE;
		}
	}

	if (m_spotColorMappingTable.GetSize() != rPageTemplate.m_spotColorMappingTable.GetSize())
		return FALSE;
	for (int i = 0; i < m_spotColorMappingTable.GetSize(); i++)
		if (m_spotColorMappingTable[i] != rPageTemplate.m_spotColorMappingTable[i])
			return FALSE;

	return TRUE;
}


BOOL CPageTemplate::operator!=(const CPageTemplate& rPageTemplate)
{
	return !(*this == rPageTemplate);
}


void CPageTemplate::Copy(const CPageTemplate& rPageTemplate)
{
	m_strPageID					= rPageTemplate.m_strPageID;
	m_bPageNumLocked			= rPageTemplate.m_bPageNumLocked;
	m_nPageType					= rPageTemplate.m_nPageType;
	m_nMultipageGroupNum		= rPageTemplate.m_nMultipageGroupNum;              
	m_fContentOffsetX			= rPageTemplate.m_fContentOffsetX;
	m_fContentOffsetY			= rPageTemplate.m_fContentOffsetY;
	m_bContentDuplicate			= rPageTemplate.m_bContentDuplicate;
	m_bContentTrim				= rPageTemplate.m_bContentTrim;
	m_fContentScaleX			= rPageTemplate.m_fContentScaleX;
	m_fContentScaleY			= rPageTemplate.m_fContentScaleY;
	m_fContentGridX				= rPageTemplate.m_fContentGridX;
	m_fContentGridY				= rPageTemplate.m_fContentGridY;
	m_fContentRotation			= rPageTemplate.m_fContentRotation;
	m_fShinglingOffsetX			= rPageTemplate.m_fShinglingOffsetX;
	m_fShinglingOffsetXFoot		= rPageTemplate.m_fShinglingOffsetXFoot;
	m_fShinglingOffsetY			= rPageTemplate.m_fShinglingOffsetY;
	m_bShinglingOffsetLocked	= rPageTemplate.m_bShinglingOffsetLocked;
	m_nContentPivotX			= rPageTemplate.m_nContentPivotX;
	m_nContentPivotY			= rPageTemplate.m_nContentPivotY;
	m_fLeftPageMargin			= rPageTemplate.m_fLeftPageMargin;
	m_fRightPageMargin			= rPageTemplate.m_fRightPageMargin;
	m_fLowerPageMargin			= rPageTemplate.m_fLowerPageMargin;
	m_fUpperPageMargin			= rPageTemplate.m_fUpperPageMargin;
	m_nAttribute				= rPageTemplate.m_nAttribute;                      
	m_bMap2AllColors			= rPageTemplate.m_bMap2AllColors;                      
	m_bMap2SpotColors			= rPageTemplate.m_bMap2SpotColors;                      
	m_bLabelBackSide			= rPageTemplate.m_bLabelBackSide;
	m_bAdjustContentToMask		= rPageTemplate.m_bAdjustContentToMask;
	m_nProductIndex				= rPageTemplate.m_nProductIndex;
	m_nProductPartIndex			= rPageTemplate.m_nProductPartIndex;
	m_pFoldSheet				= rPageTemplate.m_pFoldSheet;

	m_ColorSeparations.RemoveAll();
	for (int i = 0; i < rPageTemplate.m_ColorSeparations.GetSize(); i++)
		m_ColorSeparations.Add(rPageTemplate.m_ColorSeparations[i]);	

	CPrintSheetLocation printsheetLoc;
	m_PrintSheetLocations.RemoveAll();
	for (int i = 0; i < rPageTemplate.m_PrintSheetLocations.GetSize(); i++)
	{
		printsheetLoc = rPageTemplate.m_PrintSheetLocations[i];
		m_PrintSheetLocations.Add(printsheetLoc);	
	}

	m_spotColorMappingTable.RemoveAll();
	for (int i = 0; i < rPageTemplate.m_spotColorMappingTable.GetSize(); i++)
		m_spotColorMappingTable.Add(rPageTemplate.m_spotColorMappingTable[i]);	
}

CLayoutObject* CPageTemplate::GetLayoutObject(int nIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if ((nIndex < 0) || (nIndex >= m_PrintSheetLocations.GetSize()) )
		return NULL;

	POSITION pos = pDoc->m_PrintSheetList.FindIndex(m_PrintSheetLocations[nIndex].m_nPrintSheetIndex);
	if (!pos)
		return NULL;

	CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetAt(pos);
	CLayout*	 pLayout	 = rPrintSheet.GetLayout();
	if ( ! pLayout)
		return NULL;

	CLayoutSide* pLayoutSide;
	if (m_PrintSheetLocations[nIndex].m_nLayoutSide == FRONTSIDE)
		pLayoutSide = &pLayout->m_FrontSide;
	else
		pLayoutSide = &pLayout->m_BackSide;

	pos = pLayoutSide->m_ObjectList.FindIndex(m_PrintSheetLocations[nIndex].m_nLayoutObjectIndex);
	if (!pos)
		return NULL;
	else
		return &(pLayoutSide->m_ObjectList.GetAt(pos));
}

CPrintSheet* CPageTemplate::GetPrintSheet(int nIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if ((nIndex < 0) || (nIndex >= m_PrintSheetLocations.GetSize()) )
		return NULL;

	POSITION pos = pDoc->m_PrintSheetList.FindIndex(m_PrintSheetLocations[nIndex].m_nPrintSheetIndex);
	if ( ! pos)
		return NULL;

	return &pDoc->m_PrintSheetList.GetAt(pos);
}

double g_duration[20];

inline BOOL CPageTemplate::IsPartOfBoundProduct(int nProductPartIndex, int nIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	g_duration[nIndex]++;

	if (nProductPartIndex == ALL_PARTS)
		return TRUE;
	else
		if (nProductPartIndex >= 0)
			return (m_nProductPartIndex == nProductPartIndex) ? TRUE : FALSE;
		else
		{
			if (m_nProductIndex < 0)
				m_nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(m_nProductPartIndex);

			if ( (-nProductPartIndex - 1) == m_nProductIndex)
				return TRUE;
		}
	return FALSE;
}

void CPageTemplate::Create(CString& strID, BYTE nType, float fCenterOffsetX, float fCenterOffsetY, int nProductIndex, int nProductPartIndex)
{
	m_strPageID					= strID;
	m_bPageNumLocked			= FALSE;
	m_nPageType					= nType;
	m_nMultipageGroupNum		= -1;
	m_fContentOffsetX			= fCenterOffsetX;
	m_fContentOffsetY			= fCenterOffsetY;
	m_fContentScaleX			= 1.0f;
	m_fContentScaleY			= 1.0f;
	m_bContentDuplicate			= FALSE;
	m_bContentTrim				= FALSE;
	m_fContentGridX				= 0.0f;
	m_fContentGridY				= 0.0f;
	m_fContentRotation			= 0.0f;
	m_fShinglingOffsetX			= 0.0f;
	m_fShinglingOffsetXFoot		= 0.0f;
	m_fShinglingOffsetY			= 0.0f;
	m_bShinglingOffsetLocked	= FALSE;
	m_nContentPivotX			= XCENTER;
	m_nContentPivotY			= YCENTER;
	m_fLeftPageMargin			= 0.0f;
	m_fRightPageMargin			= 0.0f;
	m_fLowerPageMargin			= 0.0f;
	m_fUpperPageMargin			= 0.0f;
	m_nAttribute				= 0;
	m_bMap2AllColors			= FALSE;
	m_bMap2SpotColors			= FALSE;
	m_nProductIndex				= nProductIndex;
	m_nProductPartIndex			= nProductPartIndex;
	m_ColorSeparations.RemoveAll();
	m_PrintSheetLocations.RemoveAll();
	m_spotColorMappingTable.RemoveAll();
}

// helper function for PageTemplateList elements
template <> void AFXAPI SerializeElements <CPageTemplate> (CArchive& ar, CPageTemplate* pPageTemplate, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pPageTemplate->Serialize(ar);
}

void CPageTemplate::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CPageTemplate));

	if (ar.IsStoring())
	{
		ar << m_strPageID;
		ar << m_bPageNumLocked;
		ar << m_nPageType;
		ar << m_nMultipageGroupNum;              
		ar << m_fContentOffsetX;		
		ar << m_fContentOffsetY;		
		ar << m_fContentScaleX;
		ar << m_fContentScaleY;
		ar << m_bContentDuplicate;
		ar << m_bContentTrim;
		ar << m_fContentGridX;
		ar << m_fContentGridY;
		ar << m_fContentRotation;
		ar << m_fShinglingOffsetX;
		ar << m_fShinglingOffsetXFoot;
		ar << m_fShinglingOffsetY;
		ar << m_bShinglingOffsetLocked;
		ar << m_nContentPivotX;
		ar << m_nContentPivotY;
		ar << m_fLeftPageMargin;
		ar << m_fRightPageMargin;
		ar << m_fLowerPageMargin;
		ar << m_fUpperPageMargin;
		ar << m_nAttribute;                      
		ar << m_bMap2AllColors;
		ar << m_bMap2SpotColors;
		ar << m_bLabelBackSide;
		ar << m_bAdjustContentToMask;
		ar << m_nProductIndex;
		ar << m_nProductPartIndex;
		m_spotColorMappingTable.Serialize(ar);
	}	
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		m_spotColorMappingTable.RemoveAll();

		int nTemp = 0;
		m_bPageNumLocked = TRUE;	// for version < 9 always set pagenums locked
		switch (nVersion)
		{
		case 1:
			ar >> m_strPageID;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			m_fContentRotation			= 0.0f;
			m_fShinglingOffsetX			= 0.0f;
			m_fShinglingOffsetXFoot		= 0.0f;
			m_fShinglingOffsetY			= 0.0f;
			m_bShinglingOffsetLocked	= FALSE;
			m_bMap2AllColors			= FALSE;
			m_bMap2SpotColors			= FALSE;
			m_bLabelBackSide			= FALSE;
			m_bAdjustContentToMask		= FALSE;
			m_nProductPartIndex			= 0;
			m_bContentDuplicate			= FALSE;
			m_bContentTrim				= FALSE;
			break;
		case 2:
			ar >> m_strPageID;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentRotation;
			m_fShinglingOffsetX			= 0.0f;
			m_fShinglingOffsetXFoot		= 0.0f;
			m_fShinglingOffsetY			= 0.0f;
			m_bShinglingOffsetLocked	= FALSE;
			m_bMap2AllColors			= FALSE;
			m_bMap2SpotColors			= FALSE;
			m_bLabelBackSide			= FALSE;
			m_bAdjustContentToMask		= FALSE;
			m_fShinglingOffsetXFoot		= 0.0f;
			m_nProductPartIndex			= 0;
			m_bContentDuplicate			= FALSE;
			m_bContentTrim				= FALSE;
			break;
		case 3:
			ar >>m_strPageID;
			ar >>m_nPageType;
			ar >>m_nMultipageGroupNum;              
			ar >>m_fContentOffsetX;		
			ar >>m_fContentOffsetY;		
			ar >>m_fContentScaleX;
			ar >>m_fContentScaleY;
			ar >>m_fContentRotation;
			ar >>m_fShinglingOffsetX;
			ar >>m_bShinglingOffsetLocked;
			ar >>m_fLeftPageMargin;
			ar >>m_fRightPageMargin;
			ar >>m_fLowerPageMargin;
			ar >>m_fUpperPageMargin;
			ar >>m_nAttribute;                      
			m_bMap2AllColors		= FALSE;
			m_bMap2SpotColors		= FALSE;
			m_bLabelBackSide		= FALSE;
			m_bAdjustContentToMask	= FALSE;
			m_fShinglingOffsetXFoot	= 0.0f;
			m_fShinglingOffsetY		= 0.0f;
			m_nProductPartIndex		= 0;
			m_bContentDuplicate		= FALSE;
			m_bContentTrim			= FALSE;
			break;
		case 4:
			ar >> m_strPageID;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;
			ar >> m_bMap2AllColors;
			m_bMap2SpotColors		= FALSE;
			m_bLabelBackSide		= FALSE;
			m_bAdjustContentToMask	= FALSE;
			m_fShinglingOffsetXFoot	= 0.0f;
			m_fShinglingOffsetY		= 0.0f;
			m_nProductPartIndex		= 0;
			m_bContentDuplicate		= FALSE;
			m_bContentTrim			= FALSE;
			break;
		case 5:
			ar >> m_strPageID;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			m_bLabelBackSide		= FALSE;
			m_bAdjustContentToMask	= FALSE;
			m_fShinglingOffsetXFoot	= 0.0f;
			m_fShinglingOffsetY		= 0.0f;
			m_nProductPartIndex		= 0;
			m_bContentDuplicate		= FALSE;
			m_bContentTrim			= FALSE;
			break;
		case 6:
			ar >> m_strPageID;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> m_bLabelBackSide;
			m_bAdjustContentToMask	= FALSE;
			m_fShinglingOffsetXFoot	= 0.0f;
			m_fShinglingOffsetY		= 0.0f;
			m_nProductPartIndex	    = 0;
			m_bContentDuplicate		= FALSE;
			m_bContentTrim			= FALSE;
			break;
		case 7:
			ar >> m_strPageID;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> nTemp;//m_nMap2SpotColorIndex;
			m_spotColorMappingTable.Add(nTemp);
			ar >> m_bLabelBackSide;
			m_bAdjustContentToMask	= FALSE;
			m_fShinglingOffsetXFoot	= 0.0f;
			m_fShinglingOffsetY		= 0.0f;
			m_nProductPartIndex		= 0;
			m_bContentDuplicate		= FALSE;
			m_bContentTrim			= FALSE;
			break;
		case 8:
			ar >> m_strPageID;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_fShinglingOffsetXFoot;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> nTemp;//m_nMap2SpotColorIndex;
			m_spotColorMappingTable.Add(nTemp);
			ar >> m_bLabelBackSide;
			ar >> m_nProductPartIndex;
			m_fShinglingOffsetY		= 0.0f;
			m_bAdjustContentToMask	= FALSE;
			m_bContentDuplicate		= FALSE;
			m_bContentTrim			= FALSE;
			break;
		case 9:
			ar >> m_strPageID;
			ar >> m_bPageNumLocked;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_fShinglingOffsetXFoot;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> nTemp;//m_nMap2SpotColorIndex;
			m_spotColorMappingTable.Add(nTemp);
			ar >> m_bLabelBackSide;
			ar >> m_nProductPartIndex;
			m_fShinglingOffsetY		= 0.0f;
			m_bAdjustContentToMask	= FALSE;
			m_bContentDuplicate		= FALSE;
			m_bContentTrim			= FALSE;
			break;
		case 10:
			ar >> m_strPageID;
			ar >> m_bPageNumLocked;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentGridX;
			ar >> m_fContentGridY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_fShinglingOffsetXFoot;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_nContentPivotX;
			ar >> m_nContentPivotY;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> nTemp;//m_nMap2SpotColorIndex;
			m_spotColorMappingTable.Add(nTemp);
			ar >> m_bLabelBackSide;
			ar >> m_nProductPartIndex;
			m_fShinglingOffsetY		= 0.0f;
			m_bAdjustContentToMask	= FALSE;
			m_bContentDuplicate		= FALSE;
			m_bContentTrim			= ( (m_fContentGridX >= 0.1) || (m_fContentGridY >= 0.1f) ) ? TRUE : FALSE;
			break;
		case 11:
			ar >> m_strPageID;
			ar >> m_bPageNumLocked;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentGridX;
			ar >> m_fContentGridY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_fShinglingOffsetXFoot;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_nContentPivotX;
			ar >> m_nContentPivotY;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> m_bLabelBackSide;
			ar >> m_nProductPartIndex;
			m_spotColorMappingTable.Serialize(ar);
			m_fShinglingOffsetY		= 0.0f;
			m_bAdjustContentToMask	= FALSE;
			m_bContentDuplicate		= FALSE;
			m_bContentTrim			= ( (m_fContentGridX >= 0.1) || (m_fContentGridY >= 0.1f) ) ? TRUE : FALSE;
			break;
		case 12:
			ar >> m_strPageID;
			ar >> m_bPageNumLocked;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_fContentGridX;
			ar >> m_fContentGridY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_fShinglingOffsetXFoot;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_nContentPivotX;
			ar >> m_nContentPivotY;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> m_bLabelBackSide;
			ar >> m_bAdjustContentToMask;
			ar >> m_nProductPartIndex;
			m_spotColorMappingTable.Serialize(ar);
			m_fShinglingOffsetY	= 0.0f;
			m_bContentDuplicate	= FALSE;
			m_bContentTrim		= ( (m_fContentGridX >= 0.1) || (m_fContentGridY >= 0.1f) ) ? TRUE : FALSE;
			break;
		case 13:
			ar >> m_strPageID;
			ar >> m_bPageNumLocked;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_bContentDuplicate;
			ar >> m_bContentTrim;
			ar >> m_fContentGridX;
			ar >> m_fContentGridY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_fShinglingOffsetXFoot;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_nContentPivotX;
			ar >> m_nContentPivotY;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> m_bLabelBackSide;
			ar >> m_bAdjustContentToMask;
			ar >> m_nProductPartIndex;
			m_spotColorMappingTable.Serialize(ar);
			m_fShinglingOffsetY	= 0.0f;
			break;
		case 14:
			ar >> m_strPageID;
			ar >> m_bPageNumLocked;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_bContentDuplicate;
			ar >> m_bContentTrim;
			ar >> m_fContentGridX;
			ar >> m_fContentGridY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_fShinglingOffsetXFoot;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_nContentPivotX;
			ar >> m_nContentPivotY;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> m_bLabelBackSide;
			ar >> m_bAdjustContentToMask;
			ar >> m_nProductIndex;
			ar >> m_nProductPartIndex;
			m_spotColorMappingTable.Serialize(ar);
			m_fShinglingOffsetY = 0.0f;
			break;
		case 15:
			ar >> m_strPageID;
			ar >> m_bPageNumLocked;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_bContentDuplicate;
			ar >> m_bContentTrim;
			ar >> m_fContentGridX;
			ar >> m_fContentGridY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_fShinglingOffsetXFoot;
			ar >> m_fShinglingOffsetY;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_nContentPivotX;
			ar >> m_nContentPivotY;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> m_bLabelBackSide;
			ar >> m_bAdjustContentToMask;
			ar >> m_nProductIndex;
			ar >> m_nProductPartIndex;
			m_spotColorMappingTable.Serialize(ar);
			break;
		case 16:
			ar >> m_strPageID;
			ar >> m_bPageNumLocked;
			ar >> m_nPageType;
			ar >> m_nMultipageGroupNum;              
			ar >> m_fContentOffsetX;		
			ar >> m_fContentOffsetY;		
			ar >> m_fContentScaleX;
			ar >> m_fContentScaleY;
			ar >> m_bContentDuplicate;
			ar >> m_bContentTrim;
			ar >> m_fContentGridX;
			ar >> m_fContentGridY;
			ar >> m_fContentRotation;
			ar >> m_fShinglingOffsetX;
			ar >> m_fShinglingOffsetXFoot;
			ar >> m_fShinglingOffsetY;
			ar >> m_bShinglingOffsetLocked;
			ar >> m_nContentPivotX;
			ar >> m_nContentPivotY;
			ar >> m_fLeftPageMargin;
			ar >> m_fRightPageMargin;
			ar >> m_fLowerPageMargin;
			ar >> m_fUpperPageMargin;
			ar >> m_nAttribute;                      
			ar >> m_bMap2AllColors;
			ar >> m_bMap2SpotColors;
			ar >> m_bLabelBackSide;
			ar >> m_bAdjustContentToMask;
			ar >> m_nProductIndex;
			ar >> m_nProductPartIndex;
			m_spotColorMappingTable.Serialize(ar);
			break;
		}
		if (nVersion < 16)
		{
			if ( (fabs(m_fContentOffsetX) > 0.001f) || (fabs(m_fContentOffsetY) > 0.001f) || (fabs(m_fContentScaleX - 1.0f) > 0.001f) || (fabs(m_fContentScaleY - 1.0f) > 0.001f) )
				m_nAttribute = ScaleOffsetModified;
		}
		m_ColorSeparations.RemoveAll();  // no append, so target list has to be empty 
		m_PrintSheetLocations.RemoveAll();
	}

	m_ColorSeparations.Serialize(ar);
	m_PrintSheetLocations.Serialize(ar);
}


void AFXAPI DestructElements(CPageTemplate* pPageTemplate, INT_PTR nCount)
{
	ASSERT(nCount == 1); // in CList context nCount has to be 1

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pPageTemplate->m_strPageID.Empty();
	pPageTemplate->m_ColorSeparations.RemoveAll();  
	pPageTemplate->m_PrintSheetLocations.RemoveAll();
}


SEPARATIONINFO* CPageTemplate::GetSeparationInfo(int nColorDefinitionIndex, BOOL bDoMapping, CPrintSheet* pPrintSheet, int nSide)
{
	if (bDoMapping)
	{
		for (int nSepIndex = 0; nSepIndex < m_ColorSeparations.GetSize(); nSepIndex++)
			if (theApp.MapPrintColor(m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex, this, pPrintSheet, nSide) == nColorDefinitionIndex)
				return &m_ColorSeparations[nSepIndex];
	}
	else
	{
		for (int nSepIndex = 0; nSepIndex < m_ColorSeparations.GetSize(); nSepIndex++)
			if (m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex == nColorDefinitionIndex)
				return &m_ColorSeparations[nSepIndex];
	}

	return NULL;
}

SEPARATIONINFO*	CPageTemplate::GetSeparationInfo(CPageSource* pPageSource, int nPageNumInSourceFile)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	int nIndex = pDoc->m_PageSourceList.GetPageSourceIndex(pPageSource);

	for (int nSepIndex = 0; nSepIndex < m_ColorSeparations.GetSize(); nSepIndex++)
	{
		if ( (nIndex == m_ColorSeparations[nSepIndex].m_nPageSourceIndex) && (m_ColorSeparations[nSepIndex].m_nPageNumInSourceFile == nPageNumInSourceFile) )
			return &m_ColorSeparations[nSepIndex];
	}
	return NULL;
}

BOOL CPageTemplate::ContainsUnseparatedColor(int nColorDefinitionIndex, BOOL bDoMapping, CPrintSheet* pPrintSheet, CLayoutObject* pObject, CMark* pMark, CString* pstrMappedColor, CColorDefinitionTable* pColorDefTable)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if ( ! pColorDefTable)
		pColorDefTable = &pDoc->m_ColorDefinitionTable;

	//CString strColorName = ( (nColorDefinitionIndex >= 0) && (nColorDefinitionIndex < pColorDefTable->GetSize()) ) ? pColorDefTable->GetAt(nColorDefinitionIndex).m_strColorName : _T("");
	CString strColorName = ( (nColorDefinitionIndex >= 0) && (nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) ) ? pDoc->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName : _T("");
	for (int nSepIndex = 0; nSepIndex < m_ColorSeparations.GetSize(); nSepIndex++)
	{
		if ( (m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex < 0) || (m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex >= pColorDefTable->GetSize()) )
			continue;

		if (pColorDefTable->GetAt(m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex).m_strColorName.CompareNoCase(_T("Composite")) == 0)
		{
			CPageSourceHeader* pObjectSourceHeader = NULL;
			if (pObject) 
				pObjectSourceHeader = GetObjectSourceHeader(m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex);
			else
				if (pMark) 
					if (nSepIndex < pMark->m_MarkSource.m_PageSourceHeaders.GetSize())
						pObjectSourceHeader = &pMark->m_MarkSource.m_PageSourceHeaders[nSepIndex];

			if (pObjectSourceHeader)
			{
				if (bDoMapping)
				{
					if (m_bMap2SpotColors || m_bMap2AllColors)
					{
						CPlate* pPlate = NULL;
						if (pObject)
						{
							CExposure* pExposure = pObject->GetExposure(*pPrintSheet, m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex);
							pPlate = (pExposure) ? pExposure->GetPlate() : NULL;
						}
						else
							if (pMark)
							{
								CMark markToCheck;
								markToCheck						 = *pMark;
								markToCheck.m_markTemplate		 = markToCheck.TransformColorDefs( (pColorDefTable) ? *pColorDefTable : theApp.m_colorDefTable, -1);
								int nColorDefinitionIndexToCheck = markToCheck.m_markTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex;

								if (CPageTemplateList::CheckForExposuresToAdd(pPrintSheet, NULL, &markToCheck, markToCheck.m_markTemplate, -1, FALSE, pDoc, nColorDefinitionIndexToCheck))
									pPlate = pPrintSheet->m_FrontSidePlates.GetPlateByColor(nColorDefinitionIndexToCheck);
								if ( ! pPlate)
									pPlate = pPrintSheet->m_BackSidePlates.GetPlateByColor(nColorDefinitionIndexToCheck);
							}

						if (pPlate)
						{
							for (int i = 0; i < pPlate->m_spotColors.GetSize(); i++)
							{
								if (pPlate->m_spotColors[i] == strColorName)
									if (IsSystemCreated(pMark))	
									{
										CPageSource* pObjectSource = (pObject) ? GetObjectSource(m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex) : (pMark) ? &pMark->m_MarkSource : NULL;
										CString strMappedColor = (m_bMap2AllColors || m_bMap2SpotColors) ? strColorName : pObjectSource->GetSpotColor(i);
										if ( ! strMappedColor.IsEmpty())
											return TRUE;
									}
									else
									{
										if (m_spotColorMappingTable.GetSize())
										{
											if (pstrMappedColor)
											{
												for (int j = 0; j < pObjectSourceHeader->m_spotColors.GetSize(); j++)
												{
													if ( (m_spotColorMappingTable.GetEntry(j) - 1) == i)
													{
														if ( ! pstrMappedColor->IsEmpty())
															(*pstrMappedColor) += _T(" / ");
														(*pstrMappedColor) += pObjectSourceHeader->m_spotColors[j];
													}
												}
												if ( ! pstrMappedColor->IsEmpty())
													return TRUE;
											}
										}
										else
											if (i < pObjectSourceHeader->m_spotColors.GetSize())
											{
												if (pstrMappedColor)
													*pstrMappedColor = pObjectSourceHeader->m_spotColors[i];
												return TRUE;
											}
									}
							}
						}
					}
					else	// m_bMap2SpotColors
					{
						for (int i = 0; i < pObjectSourceHeader->m_spotColors.GetSize(); i++)
							if (theApp.MapPrintColor(pObjectSourceHeader->m_spotColors[i]) == strColorName)
							{
								if (pstrMappedColor)
									*pstrMappedColor = pObjectSourceHeader->m_spotColors[i];
								return TRUE;
							}
					}
				}
				else
				{
					if (pMark && IsSystemCreated(pMark))	
					{
						CMark markToCheck;
						markToCheck				   = *pMark;
						markToCheck.m_markTemplate = markToCheck.TransformColorDefs( (pColorDefTable) ? *pColorDefTable : theApp.m_colorDefTable, -1);
						if (markToCheck.m_markTemplate.GetSeparationInfo(nColorDefinitionIndex))	// if system created mark, separations are valid also for unseparated colors
						{
							int nSepStatus = pPrintSheet->ContainsPrintColor(nColorDefinitionIndex, (pObject) ? pObject->GetLayoutSideIndex() : BOTHSIDES);
							if ( (nSepStatus == CColorDefinition::Unseparated) || (nSepStatus == CColorDefinition::Mixed) )
								return TRUE;
						}
					}
					else
					{
						for (int i = 0; i < pObjectSourceHeader->m_spotColors.GetSize(); i++)
							if (pObjectSourceHeader->m_spotColors[i] == strColorName)
								return TRUE;
						if (pMark)
							if (pMark->m_bAllColors)
								return TRUE;
					}
				}
			}
		}
		else
			if (pMark && IsSystemCreated(pMark))	
			{
				CMark markToCheck;
				markToCheck				   = *pMark;
				markToCheck.m_markTemplate = markToCheck.TransformColorDefs( (pColorDefTable) ? *pColorDefTable : theApp.m_colorDefTable, -1);
				if (markToCheck.m_markTemplate.GetSeparationInfo(nColorDefinitionIndex))	// if system created mark, separations are valid also for unseparated colors
					return TRUE;
			}
			else
				if (pMark)
					if (pMark->m_bAllColors)
						return TRUE;
	}
	return FALSE;
}

CPageSource* CPageTemplate::GetObjectSource(int nColorDefinitionIndex, BOOL bDoMapping, CPrintSheet* pPrintSheet, int nSide)
{
	SEPARATIONINFO* pSepInfo = GetSeparationInfo(nColorDefinitionIndex, bDoMapping, pPrintSheet, nSide);
	if (!pSepInfo)
		return NULL;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int nObjectSourceIndex = pSepInfo->m_nPageSourceIndex;

	CPageSourceList* pObjectSourceList = ((m_nPageType == SinglePage) || (m_nPageType == MultipageMember)) ? &pDoc->m_PageSourceList : &pDoc->m_MarkSourceList;

	if ((nObjectSourceIndex < 0) || (nObjectSourceIndex >= pObjectSourceList->GetCount()))
		return NULL;

	POSITION pos = pObjectSourceList->FindIndex(nObjectSourceIndex);
	if (!pos)
		return NULL;

	CPageSource& rObjectSource = pObjectSourceList->GetAt(pos);

	return &rObjectSource;
}

CPageSourceHeader* CPageTemplate::GetObjectSourceHeader(SEPARATIONINFO& rSepInfo, class CPageSourceList* pObjectSourceList, CColorDefinitionTable* pColorDefinitionTable, CImpManDoc* pDoc)
{
	int nObjectSourceIndex	   = rSepInfo.m_nPageSourceIndex;
	int nObjectNumInSourceFile = rSepInfo.m_nPageNumInSourceFile;

	if ( ! pObjectSourceList)
	{
		if ( ! pDoc)	// pDoc is passed from ReorganizePrintSheetPlates() up to this stage here for performance reasons. Specially when analysing marks (fold and cut) with complex jobs, GetDoc() takes to much time
			pDoc = CImpManDoc::GetDoc();	// because this function runs inside loops
		if ( ! pDoc)
			return NULL;

		pObjectSourceList = ((m_nPageType == SinglePage) || (m_nPageType == MultipageMember)) ? &pDoc->m_PageSourceList : &pDoc->m_MarkSourceList;
	}

	if ((nObjectSourceIndex < 0) || (nObjectSourceIndex >= pObjectSourceList->GetCount()))
		return NULL;

	POSITION pos = pObjectSourceList->FindIndex(nObjectSourceIndex);
	if (!pos)
		return NULL;

	CPageSource& rObjectSource = pObjectSourceList->GetAt(pos);
	if (rObjectSource.m_PageSourceHeaders.GetSize() < nObjectNumInSourceFile)
		return NULL;

	if ( (nObjectNumInSourceFile > 0) && (nObjectNumInSourceFile <= rObjectSource.m_PageSourceHeaders.GetSize()) )
		return &rObjectSource.m_PageSourceHeaders[nObjectNumInSourceFile - 1];
	else
		return NULL;
}

CPageSourceHeader* CPageTemplate::GetObjectSourceHeader(int nColorDefinitionIndex, BOOL bDoMapping, CPrintSheet* pPrintSheet, int nSide, CImpManDoc* pDoc)
{
	SEPARATIONINFO* pSepInfo = GetSeparationInfo(nColorDefinitionIndex, bDoMapping, pPrintSheet, nSide);
	if ( ! pSepInfo)
		return NULL;

	return GetObjectSourceHeader(*pSepInfo, NULL, NULL, pDoc);
}

CPageSource* CPageTemplate::GetVarnishInfo()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		POSITION pos = pDoc->m_PageSourceList.FindIndex(m_ColorSeparations[i].m_nPageSourceIndex);
		if (pos)
		{
			CPageSource& rPageSource = pDoc->m_PageSourceList.GetAt(pos);
			if (rPageSource.m_strSystemCreationInfo == _T("$VARNISH"))
				return &rPageSource;
		}
	}
	return NULL;
}

BOOL CPageTemplate::IsMemberOf(class CPageTemplateList* pTemplateList)
{
	POSITION pos = pTemplateList->GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = pTemplateList->CList::GetNext(pos);
		if (&rTemplate == this)
			return TRUE;
	}
	return FALSE;
}

CPageTemplate* CPageTemplate::GetFirstMultipagePartner()
{
	if (m_nMultipageGroupNum == -1)	// is no multipage
		return this;

	CPageTemplateList* pPageTemplateList = &CImpManDoc::GetDoc()->m_PageTemplateList;
	POSITION		   pos				 = pPageTemplateList->GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = pPageTemplateList->CList::GetNext(pos);
		if (&rTemplate != this)
			if (rTemplate.m_nMultipageGroupNum == m_nMultipageGroupNum)
				return &rTemplate;
	}

	return this;
}

int	CPageTemplate::GetMultipageSide()
{
	if (m_nMultipageGroupNum == -1)	// is no multipage
		return -1;

	int				   nIndex			 = 0;
	CPageTemplateList* pPageTemplateList = &CImpManDoc::GetDoc()->m_PageTemplateList;
	POSITION		   pos				 = pPageTemplateList->GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = pPageTemplateList->CList::GetNext(pos);
		if (&rTemplate == this)
			return (nIndex % 2) ? LEFT : RIGHT;

		nIndex++;
	}

	return -1;
}

float CPageTemplate::GetContentOffsetX(BOOL bAddShinglingOffset, CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet)
{
	if (pLayoutObject)
	{
		float fObjectWidth	  = pLayoutObject->GetWidth();
		float fContentOffsetX = GetContentOffsetX(bAddShinglingOffset, NULL, &fObjectWidth);

		if ( bAddShinglingOffset && (GetMarkType() == CMark::CropMark) )	// shingling for crop marks
		{
			CLayoutSide* pLayoutSide = pLayoutObject->GetLayoutSide();
			if ( ! pLayoutSide)
				return fContentOffsetX;

			POSITION buddyPos = pLayoutSide->m_ObjectList.FindIndex(pLayoutObject->m_markBuddyLink.m_nLinkedObjectIndex);
			if ( ! buddyPos)
				return fContentOffsetX;

			CLayoutObject& rBuddyObject = pLayoutSide->m_ObjectList.GetAt(buddyPos);
			if ( (rBuddyObject.m_nHeadPosition == LEFT) || (rBuddyObject.m_nHeadPosition == RIGHT) )	// lying pages do not have X shingling offset
				return fContentOffsetX;

			CPageTemplate* pBuddyTemplate = rBuddyObject.GetCurrentPageTemplate(pPrintSheet);
			if ( ! pBuddyTemplate)
				return fContentOffsetX;

			if ( ! pBuddyTemplate->GetProductPart().m_foldingParams.m_bShinglingCropMarks)
				return fContentOffsetX;

			//if (pProductGroup->GetProductionProfile().m_postpress.m_folding.m_nShinglingMethod == CBookblock::ShinglingMethodScale)
			{
				if (rBuddyObject.GetSpineSide() == RIGHT)
				{
					if ( (pLayoutObject->m_markBuddyLink.m_nLinkedObjectPosition == RIGHT_TOP) || (pLayoutObject->m_markBuddyLink.m_nLinkedObjectPosition == RIGHT_BOTTOM) )	// crop marks on spine side not to be moved
						return fContentOffsetX;
				}
				else									
				{
					if ( (pLayoutObject->m_markBuddyLink.m_nLinkedObjectPosition == LEFT_TOP)  || (pLayoutObject->m_markBuddyLink.m_nLinkedObjectPosition == LEFT_BOTTOM) )		// crop marks on spine side not to be moved
						return fContentOffsetX;
				}
			}

			switch (rBuddyObject.m_nHeadPosition)
			{
			case TOP:		return fContentOffsetX + pBuddyTemplate->m_fShinglingOffsetX;	
			case BOTTOM:	return fContentOffsetX - pBuddyTemplate->m_fShinglingOffsetX;	
			default:		return fContentOffsetX;
			}
		}
		else
			return fContentOffsetX;
	}
	else
		return GetContentOffsetX(bAddShinglingOffset);
}

float CPageTemplate::GetContentOffsetY(BOOL bAddShinglingOffset, CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet)
{
	if (pLayoutObject)
	{
		float fObjectHeight	  = pLayoutObject->GetHeight();
		float fContentOffsetY = GetContentOffsetY(bAddShinglingOffset, NULL, &fObjectHeight);

		if ( bAddShinglingOffset && (GetMarkType() == CMark::CropMark) )	// shingling for crop marks
		{
			CLayoutSide* pLayoutSide = pLayoutObject->GetLayoutSide();
			if ( ! pLayoutSide)
				return fContentOffsetY;

			POSITION buddyPos = pLayoutSide->m_ObjectList.FindIndex(pLayoutObject->m_markBuddyLink.m_nLinkedObjectIndex);
			if ( ! buddyPos)
				return fContentOffsetY;

			CLayoutObject& rBuddyObject = pLayoutSide->m_ObjectList.GetAt(buddyPos);
			if ( (rBuddyObject.m_nHeadPosition == TOP) || (rBuddyObject.m_nHeadPosition == BOTTOM) )	// standing pages do not have Y shingling offset
				return fContentOffsetY;

			CPageTemplate* pBuddyTemplate = rBuddyObject.GetCurrentPageTemplate(pPrintSheet);
			if ( ! pBuddyTemplate)
				return fContentOffsetY;

			if ( ! pBuddyTemplate->GetProductPart().m_foldingParams.m_bShinglingCropMarks)
				return fContentOffsetY;

			//if (pProductGroup->GetProductionProfile().m_postpress.m_folding.m_nShinglingMethod == CBookblock::ShinglingMethodScale)
			{
				if (rBuddyObject.GetSpineSide() == TOP)
				{
					if ( (pLayoutObject->m_markBuddyLink.m_nLinkedObjectPosition == RIGHT_TOP)   || (pLayoutObject->m_markBuddyLink.m_nLinkedObjectPosition == LEFT_TOP) )			// crop marks on spine side not to be moved
						return fContentOffsetY;
				}
				else									
				{
					if ( (pLayoutObject->m_markBuddyLink.m_nLinkedObjectPosition == LEFT_BOTTOM) || (pLayoutObject->m_markBuddyLink.m_nLinkedObjectPosition == RIGHT_BOTTOM) )		// crop marks on spine side not to be moved
						return fContentOffsetY;
				}
			}

			switch (rBuddyObject.m_nHeadPosition)
			{
			case LEFT:		return fContentOffsetY + pBuddyTemplate->m_fShinglingOffsetX;	
			case RIGHT:		return fContentOffsetY - pBuddyTemplate->m_fShinglingOffsetX;	
			default:		return fContentOffsetY;
			}
		}
		else
			return fContentOffsetY;
	}
	else
		return GetContentOffsetY(bAddShinglingOffset);
}

float CPageTemplate::GetContentOffsetX(BOOL bAddShinglingOffset, CPageSource* pObjectSource, float* pfObjectWidth, BOOL bOutputJDF)
{
	if ((m_nPageType == SinglePage) || (m_nPageType == MultipageMember)) 
	{
		if (bAddShinglingOffset)
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			if ( ! pDoc)
				return m_fContentOffsetX;

			CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nProductPartIndex);
			if (rProductPart.IsNull())
				return m_fContentOffsetX;

			if (rProductPart.m_foldingParams.m_bShinglingActive)	
			{
				if (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottling)
				{
					if ( fabs(m_fShinglingOffsetX) < fabs(m_fShinglingOffsetXFoot) )
						return m_fContentOffsetX + m_fShinglingOffsetX;
					else
						return m_fContentOffsetX + m_fShinglingOffsetXFoot;
				}
				else
					if (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale)
					{
						if ( fabs(m_fShinglingOffsetX) < fabs(m_fShinglingOffsetXFoot) )
							return m_fContentOffsetX + m_fShinglingOffsetX / 2.0f;
						else
							return m_fContentOffsetX + m_fShinglingOffsetXFoot / 2.0f;
					}
					else
						if (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodScale)
							//if (bOutputJDF) 
							//{
							//	CLayoutObject* pLayoutObject = (m_PrintSheetLocations.GetCount()) ? m_PrintSheetLocations[0].GetLayoutObject() : NULL;
							//	BOOL bIsLeftPage = (pLayoutObject) ? ( (pLayoutObject->m_nIDPosition == LEFT) ? TRUE : FALSE) : FALSE;	// offset is a combination of moving and scaling;
							//	return (bIsLeftPage) ? m_fContentOffsetX + m_fShinglingOffset : m_fContentOffsetX;						// JDF: scaling is interpreted as relativ to lower left -> move full shingling offset
							//}																											// when left page, don't move when right
							//else
								if (m_nMultipageGroupNum >= 0)
									return m_fContentOffsetX;																			// PDF: scaling is interpreted as relativ to center -> don't move in case of double page
								else
									return m_fContentOffsetX + m_fShinglingOffsetX / 2.0f;												// PDF: scaling is interpreted as relativ to center -> move half shingling offset
						else																												
							return m_fContentOffsetX + m_fShinglingOffsetX;																	
			}
			else
				return m_fContentOffsetX;
		}
		else
			return m_fContentOffsetX;
	}
	else	// is mark
	{
		if ( ! pObjectSource)
			pObjectSource = (m_ColorSeparations.GetSize()) ? GetObjectSource(m_ColorSeparations[0].m_nColorDefinitionIndex) : NULL;
		if ( ! pObjectSource)
			return m_fContentOffsetX;
	
		float fOutBBoxWidth;
		if ((m_fContentRotation == 0.0f) || (m_fContentRotation == 180.0f))
			fOutBBoxWidth  = (pObjectSource->GetBBox(RIGHT, NULL) - pObjectSource->GetBBox(LEFT,   NULL)) * m_fContentScaleX;
		else
			fOutBBoxWidth  = (pObjectSource->GetBBox(TOP,   NULL) - pObjectSource->GetBBox(BOTTOM, NULL)) * m_fContentScaleX;

		float fObjectWidth = (pfObjectWidth) ? *pfObjectWidth : 0.0f;
		if ( ! pfObjectWidth)
		{
			CLayoutObject* pLayoutObject = GetLayoutObject();
			if (pLayoutObject) 
				fObjectWidth = pLayoutObject->GetWidth();
			else
				return m_fContentOffsetX;
		}

		float fContentPivotOffsetX = 0.0f;
		switch (m_nContentPivotX)
		{
		case LEFT:		fContentPivotOffsetX = -(fObjectWidth - fOutBBoxWidth) / 2.0f;	break;
		case RIGHT:		fContentPivotOffsetX =  (fObjectWidth - fOutBBoxWidth) / 2.0f;	break;
		case XCENTER:	fContentPivotOffsetX = 0.0f;									break;
		}

		return m_fContentOffsetX + fContentPivotOffsetX;
	}
}

float CPageTemplate::GetContentOffsetY(BOOL bAddShinglingOffset, CPageSource* pObjectSource, float* pfObjectHeight, BOOL /*bOutputJDF*/)
{
	if ((m_nPageType == SinglePage) || (m_nPageType == MultipageMember)) 
	{
		if (bAddShinglingOffset)
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			if ( ! pDoc)
				return m_fContentOffsetY;

			CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nProductPartIndex);
			if (rProductPart.IsNull())
				return m_fContentOffsetY;

			if (rProductPart.m_foldingParams.m_bShinglingActive)
			{
				if ( (rProductPart.m_foldingParams.m_nShinglingDirection == CBookblock::ShinglingDirectionSpineHead) || (rProductPart.m_foldingParams.m_nShinglingDirection == CBookblock::ShinglingDirectionSpineFoot) )
				{
					if (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottling)
					{
						return m_fContentOffsetY + m_fShinglingOffsetY;
					}
					else
						if (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale)
						{
							return m_fContentOffsetY + m_fShinglingOffsetY / 2.0f;
						}
						else
							if (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodScale)
								//if (bOutputJDF) 
								//{
								//	CLayoutObject* pLayoutObject = (m_PrintSheetLocations.GetCount()) ? m_PrintSheetLocations[0].GetLayoutObject() : NULL;
								//	BOOL bIsLeftPage = (pLayoutObject) ? ( (pLayoutObject->m_nIDPosition == LEFT) ? TRUE : FALSE) : FALSE;	// offset is a combination of moving and scaling;
								//	return (bIsLeftPage) ? m_fContentOffsetX + m_fShinglingOffset : m_fContentOffsetX;						// JDF: scaling is interpreted as relativ to lower left -> move full shingling offset
								//}																											// when left page, don't move when right
								//else
									//if (m_nMultipageGroupNum >= 0)
									//	return m_fContentOffsetX;																			// PDF: scaling is interpreted as relativ to center -> don't move in case of double page
									//else
										return m_fContentOffsetY + m_fShinglingOffsetY / 2.0f;												// PDF: scaling is interpreted as relativ to center -> move half shingling offset
							else																												
								return m_fContentOffsetY + m_fShinglingOffsetY;																	
				}
				else
					return m_fContentOffsetY;
			}
			else
				return m_fContentOffsetY;
		}
		else
			return m_fContentOffsetY;	
	}
	else	
	{	// is mark
		if ( ! pObjectSource)
			pObjectSource = (m_ColorSeparations.GetSize()) ? GetObjectSource(m_ColorSeparations[0].m_nColorDefinitionIndex) : NULL;
		if ( ! pObjectSource)
			return m_fContentOffsetY;
	
		float fOutBBoxHeight;
		if ((m_fContentRotation == 0.0f) || (m_fContentRotation == 180.0f))
			fOutBBoxHeight = (pObjectSource->GetBBox(TOP,   NULL) - pObjectSource->GetBBox(BOTTOM, NULL)) * m_fContentScaleY;
		else
			fOutBBoxHeight = (pObjectSource->GetBBox(RIGHT, NULL) - pObjectSource->GetBBox(LEFT,   NULL)) * m_fContentScaleY;

		float fObjectHeight = (pfObjectHeight) ? *pfObjectHeight : 0.0f;
		if ( ! pfObjectHeight)
		{
			CLayoutObject* pLayoutObject = GetLayoutObject();
			if (pLayoutObject) 
				fObjectHeight = pLayoutObject->GetHeight();
			else
				return m_fContentOffsetY;
		}

		float fContentPivotOffsetY = 0.0f;
		switch (m_nContentPivotY)
		{
		case BOTTOM:	fContentPivotOffsetY = -(fObjectHeight - fOutBBoxHeight) / 2.0f;	break;
		case TOP:		fContentPivotOffsetY =  (fObjectHeight - fOutBBoxHeight) / 2.0f;	break;
		case YCENTER:	fContentPivotOffsetY = 0.0f;										break;
		}
		return m_fContentOffsetY + fContentPivotOffsetY;
	}
}

void CPageTemplate::GetGridClipBox(float& fClipBoxLeft, float& fClipBoxBottom, float& fClipBoxRight, float& fClipBoxTop)
{
	if ((m_nPageType == SinglePage) || (m_nPageType == MultipageMember)) 
		return;
	if ( ! m_bContentTrim)
		return;

	if (m_fContentGridX > 0.1)
	{
		float fClipWidth	  = fClipBoxRight - fClipBoxLeft;
		float fClipBoxCenterX = fClipBoxLeft  + fClipWidth/2.0f;
		long  nGrids		  = (long)(fClipWidth / m_fContentGridX);
		switch (m_nContentPivotX)
		{
		case LEFT:		fClipBoxRight = fClipBoxLeft	+ (float)nGrids * m_fContentGridX;	break;
		case RIGHT:		fClipBoxLeft  = fClipBoxRight	- (float)nGrids * m_fContentGridX;	break;
		case XCENTER:	fClipBoxLeft  = fClipBoxCenterX - (float)(long)(nGrids/2) * m_fContentGridX;	
						fClipBoxRight = fClipBoxCenterX + (float)(long)(nGrids/2) * m_fContentGridX;	
						break;
		}
	}

	if (m_fContentGridY > 0.1)
	{
		float fClipHeight	  = fClipBoxTop	   - fClipBoxBottom;
		float fClipBoxCenterY = fClipBoxBottom + fClipHeight/2.0f;
		long  nGrids		  = (long)(fClipHeight / m_fContentGridY);
		switch (m_nContentPivotY)
		{
		case BOTTOM:	fClipBoxTop		= fClipBoxBottom  + (float)nGrids * m_fContentGridY;	break;
		case TOP:		fClipBoxBottom  = fClipBoxTop	  - (float)nGrids * m_fContentGridY;	break;
		case YCENTER:	fClipBoxBottom  = fClipBoxCenterY - (float)(long)(nGrids/2) * m_fContentGridY;	
						fClipBoxTop		= fClipBoxCenterY + (float)(long)(nGrids/2) * m_fContentGridY;	
						break;
		}
	}
}

BOOL CPageTemplate::IsSystemCreated(CMark* pMark)
{
	if ( ! m_ColorSeparations.GetSize())
		return FALSE;

	CPageSource* pMarkSource = (pMark) ? &pMark->m_MarkSource : GetObjectSource(m_ColorSeparations[0].m_nColorDefinitionIndex);
	if ( ! pMarkSource)
		return FALSE;

	if (pMarkSource->m_nType == CPageSource::SystemCreated)
		return TRUE;
	return FALSE;
}

int	CPageTemplate::GetMarkType()
{
	if ((m_nPageType == SinglePage) || (m_nPageType == MultipageMember)) 
		return -1;
	if ( ! m_ColorSeparations.GetSize())
		return CMark::CustomMark;

	CPageSource* pMarkSource = GetObjectSource(m_ColorSeparations[0].m_nColorDefinitionIndex);
	if ( ! pMarkSource)
		return -1;

	if (pMarkSource->m_nType == CPageSource::SystemCreated)
	{
		CString strContentType = GetStringToken(pMarkSource->m_strSystemCreationInfo, 0, _T(" |"));	// Barcode has pipe delimiters, all others has blank delimiters
		if (strContentType == "$TEXT")
			return CMark::TextMark;
		else
		if (strContentType == "$SECTION_BAR")
			return CMark::SectionMark;
		else
		if (strContentType == "$CROP_LINE")
			return CMark::CropMark;
		else
		if (strContentType == "$FOLD_MARK")
			return CMark::FoldMark;
		else
		if (strContentType == "$BARCODE")
			return CMark::BarcodeMark;
		else
		if (strContentType == "$RECTANGLE")
			return CMark::DrawingMark;
		else
			return CMark::CustomMark;
	}
	else
		return CMark::CustomMark;
}

CString	CPageTemplate::GetMarkText()
{
	if ( ! m_ColorSeparations.GetSize())
		return "";
	if ((m_nPageType == SinglePage) || (m_nPageType == MultipageMember)) 
		return "";

	CPageSource* pMarkSource = GetObjectSource(m_ColorSeparations[0].m_nColorDefinitionIndex);
	if ( ! pMarkSource)
		return "";

	if (pMarkSource->m_nType == CPageSource::SystemCreated)
	{
		int	nIndex = pMarkSource->m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex == -1)	// invalid string
			return "";
		CString strContentType = pMarkSource->m_strSystemCreationInfo.Left(nIndex);
		CString strParams	   = pMarkSource->m_strSystemCreationInfo.Right(pMarkSource->m_strSystemCreationInfo.GetLength() - nIndex);
		strParams.TrimLeft(); strParams.TrimRight();
		if (strContentType == "$TEXT")
		{
			CString strText;
			nIndex = strParams.Find(_T("/EOT"));
			if (nIndex == -1)	
				return strParams;
			else
			{
				CString strText = strParams.Left(nIndex);
				strText.TrimLeft(); strText.TrimRight();
				return strText;
			}
		}
	}

	return "";
}

BOOL CPageTemplate::SetMarkText(CString strOutputText)
{
	if ( ! m_ColorSeparations.GetSize())
		return FALSE;
	if ((m_nPageType == SinglePage) || (m_nPageType == MultipageMember)) 
		return FALSE;

	CPageSource* pMarkSource = GetObjectSource(m_ColorSeparations[0].m_nColorDefinitionIndex);
	if ( ! pMarkSource)
		return FALSE;

	if (pMarkSource->m_nType == CPageSource::SystemCreated)
	{
		int	nIndex = pMarkSource->m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex == -1)	// invalid string
			return FALSE;
		CString strContentType = pMarkSource->m_strSystemCreationInfo.Left(nIndex);
		CString strParams	   = pMarkSource->m_strSystemCreationInfo.Right(pMarkSource->m_strSystemCreationInfo.GetLength() - nIndex);
		strParams.TrimLeft(); strParams.TrimRight();
		if (strContentType == "$TEXT")
		{
			if (strParams.Find(_T("/Font (Barcode)")) != -1)
				pMarkSource->m_strSystemCreationInfo.Format(_T("$TEXT %s/EOT /Font (Barcode)"), strOutputText);
			else
				if (strParams.Find(_T("/Font (BarcodeText)")) != -1)
					pMarkSource->m_strSystemCreationInfo.Format(_T("$TEXT %s/EOT /Font (BarcodeText)"), strOutputText);
				else
					pMarkSource->m_strSystemCreationInfo.Format(_T("$TEXT %s"), strOutputText);

			return TRUE;
		}
	}

	return FALSE;
}

// get number of spotcolor in list of separations
int	CPageTemplate::GetCurSpotColorNum(int nColorDefinitionIndex, const CString strColorName, CPageSourceHeader* pObjectSourceHeader)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nCurSpotColorNum = 0;
	if (pObjectSourceHeader)
		if ( ! strColorName.IsEmpty())
		{
			for (int i = 0; i < pObjectSourceHeader->m_spotColors.GetSize(); i++)
			{
				nCurSpotColorNum++;
				if (strColorName == pObjectSourceHeader->m_spotColors[i])
					return nCurSpotColorNum;
			}
		}

	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		if ( (m_ColorSeparations[i].m_nColorDefinitionIndex >= 0) && (m_ColorSeparations[i].m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
		{
			CString strColorName = pDoc->m_ColorDefinitionTable[m_ColorSeparations[i].m_nColorDefinitionIndex].m_strColorName;
			if (strColorName.CompareNoCase(_T("Composite")) != 0)
			{
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
				if ( ! pColorDef)
					nCurSpotColorNum++;
				else
					if (pColorDef->m_nColorType == CColorDefinition::Spot)
						nCurSpotColorNum++;
			}
			if (m_ColorSeparations[i].m_nColorDefinitionIndex == nColorDefinitionIndex)
				break;
		}
	}
	return nCurSpotColorNum;
}

CString	CPageTemplate::GetNonePrintedColors(CPrintSheet* pPrintSheet, CLayoutObject* pObject)
{
	if ( ! pPrintSheet || ! pObject)
		return "";
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return "";
	if (m_bMap2AllColors)		// if colorspace all, no noneprinted colors can exist
		return "";

	CString strColors;

/*	if (pObject->m_nType == CLayoutObject::ControlMark)
	{
		CPageSource* pObjectSource = GetObjectSource((m_ColorSeparations.GetSize()) ? m_ColorSeparations[0].m_nColorDefinitionIndex : -1);
		if (pObjectSource)
		{
			for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
			{
				CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[i];
				SEPARATIONINFO* pSepInfo = GetFirstSepInfo(i + 1);
				BOOL bNonPrint = (pSepInfo) ? ((pSepInfo->m_nColorDefinitionIndex == -1) ? TRUE : FALSE) : TRUE;
				if (bNonPrint)
				{
					if (strColors.GetLength())
						strColors += _T("; ");
					strColors += rObjectSourceHeader.m_strColorName;
				}
			}
		}
	}
*/

	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		if (m_ColorSeparations[i].m_nColorDefinitionIndex == -1)
		{
			CPageSourceHeader* pObjectSourceHeader = GetObjectSourceHeader(m_ColorSeparations[i]);
			if (pObjectSourceHeader)
			{
				if (strColors.GetLength())
					strColors += _T("; ");
				strColors += pObjectSourceHeader->m_strColorName;
			}
		}
	}

	BOOL bCheckUnsepSpots = FALSE;
	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		CExposure* pExposure = NULL;
		if ( ! (pExposure = pObject->GetExposure(*pPrintSheet, theApp.MapPrintColor(m_ColorSeparations[i].m_nColorDefinitionIndex, this, pPrintSheet, pObject->GetLayoutSideIndex()))) )
		{
			if ( (m_ColorSeparations[i].m_nColorDefinitionIndex >= 0) && 
				 (m_ColorSeparations[i].m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
			{
				if (pDoc->m_ColorDefinitionTable[m_ColorSeparations[i].m_nColorDefinitionIndex].m_strColorName != _T("Composite") )
				{
					CString strColorName = pDoc->m_ColorDefinitionTable[m_ColorSeparations[i].m_nColorDefinitionIndex].m_strColorName;
					if ( ! IsSystemCreated())
					{
						if (strColors.GetLength())
							strColors += _T("; ");
						strColors += strColorName;
					}
					else
					{	// if system created and process color, it is used in composite pages -> so look if composite is in use 
						// if system created and spot color, look if this color is in use, independent of separation or composite
						int nColorType;
						CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
						if ( ! pColorDef)
							nColorType = CColorDefinition::Spot;
						else
							nColorType = pColorDef->m_nColorType;

						BOOL bCompositeAlso = ( (nColorType != CColorDefinition::Spot) && (nColorType != CColorDefinition::Alternate) ) ? TRUE : FALSE;
						if ( ! pDoc->m_PageTemplateList.ColorInUse(pDoc, pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strColorName), FALSE, bCompositeAlso) )
						{
							if (strColors.GetLength())
								strColors += _T("; ");
							strColors += strColorName;
						}
					}
				}
				else
					bCheckUnsepSpots = TRUE;
			}
		}
		else
			if (pDoc->m_ColorDefinitionTable[m_ColorSeparations[i].m_nColorDefinitionIndex].m_strColorName == _T("Composite"))
				bCheckUnsepSpots = TRUE;

		if (bCheckUnsepSpots)
		{
			CPageSourceHeader* pObjectSourceHeader = GetObjectSourceHeader(m_ColorSeparations[i].m_nColorDefinitionIndex);
			if (pObjectSourceHeader)
			{
				CPlate* pPlate = (pExposure) ? pExposure->GetPlate() : NULL;
				for (int j = 0; j < pObjectSourceHeader->m_spotColors.GetSize(); j++)
				{
					CString strMappedColor = pObjectSourceHeader->m_spotColors[j];
					if (pPlate)		// composite object will be printed -> map spotcolor and check if also printed
					{
						if (m_bMap2SpotColors)
						{
							if (j < pPlate->m_spotColors.GetSize())
								strMappedColor = pPlate->m_spotColors[j];
							else
								strMappedColor = "";	// none printed
						}
						else
							strMappedColor = theApp.MapPrintColor(strMappedColor);
					}
					if ( ! pDoc->m_PageTemplateList.ColorInUse(pDoc, pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strMappedColor)) )
					{
						if (strColors.GetLength())
							strColors += _T("; ");
						strColors += pObjectSourceHeader->m_spotColors[j];
					}
				}
			}
		}
	}

	return strColors;
}

CString CPageTemplate::GetMappedColorsComposite()
{
	CString strColors;
	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		CPageSourceHeader* pObjectSourceHeader = GetObjectSourceHeader(m_ColorSeparations[i].m_nColorDefinitionIndex);
		if (pObjectSourceHeader)
		{
			for (int j = 0; j < pObjectSourceHeader->m_spotColors.GetSize(); j++)
			{
				CString strMappedColor = theApp.MapPrintColor(pObjectSourceHeader->m_spotColors[j]);
				if (strMappedColor != pObjectSourceHeader->m_spotColors[j])
				{
					if (strColors.GetLength())
						strColors += _T("; ");
					strColors += pObjectSourceHeader->m_spotColors[j];
				}
			}
		}
	}

	return strColors;
}

BOOL CPageTemplate::IsColorPrinted(int nColorDefinitionIndex, CPrintSheet* pPrintSheet, CLayoutObject* pObject, CString& strRemappedColor)
{
	strRemappedColor.Empty();

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL bSeparated = FALSE;
	if (pObject->GetExposure(*pPrintSheet, theApp.MapPrintColor(nColorDefinitionIndex, this, pPrintSheet, pObject->GetLayoutSideIndex())) )
	{
		SEPARATIONINFO* pSepInfo = GetSeparationInfo(nColorDefinitionIndex, TRUE, pPrintSheet, pObject->GetLayoutSideIndex());
		if ( ! pSepInfo)
			return FALSE;

		if (pSepInfo->m_nColorDefinitionIndex != nColorDefinitionIndex)		// separation is mapped 
			if ( (pSepInfo->m_nColorDefinitionIndex >= 0) && (pSepInfo->m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
				strRemappedColor = pDoc->m_ColorDefinitionTable[pSepInfo->m_nColorDefinitionIndex].m_strColorName;
		if (pDoc->m_ColorDefinitionTable[pSepInfo->m_nColorDefinitionIndex].m_strColorName == _T("Composite"))
			return CColorDefinition::Unseparated;
		else
			bSeparated = TRUE;
	}

	if (ContainsUnseparatedColor(nColorDefinitionIndex, FALSE, pPrintSheet, pObject))
		return (bSeparated) ? CColorDefinition::Mixed : CColorDefinition::Unseparated;
	else
		if (ContainsUnseparatedColor(nColorDefinitionIndex, TRUE, pPrintSheet, pObject, NULL, &strRemappedColor))
			return (bSeparated) ? CColorDefinition::Mixed : CColorDefinition::Unseparated;

	return bSeparated;
}

BOOL CPageTemplate::IsColorPrinted(int nDocColorDefinitionIndex, CPrintSheet* pPrintSheet, CMark* pMark, CString& strRemappedColor, CColorDefinitionTable* pColorDefTable)
{
	strRemappedColor.Empty();

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pMark)
		return FALSE;

	CMark markToCheck;
	markToCheck = *pMark;
	markToCheck.m_markTemplate = markToCheck.TransformColorDefs( (pColorDefTable) ? *pColorDefTable : theApp.m_colorDefTable, -1);

	BOOL bSeparated = FALSE;
	if (CPageTemplateList::CheckForExposuresToAdd(pPrintSheet, NULL, &markToCheck, markToCheck.m_markTemplate, -1, FALSE, pDoc, nDocColorDefinitionIndex))
	{
		SEPARATIONINFO* pSepInfo = markToCheck.m_markTemplate.GetSeparationInfo(nDocColorDefinitionIndex, TRUE, pPrintSheet, BOTHSIDES);
		if ( ! pSepInfo)
			return FALSE;

		if (pSepInfo->m_nColorDefinitionIndex != nDocColorDefinitionIndex)		// separation is mapped 
			if ( (pSepInfo->m_nColorDefinitionIndex >= 0) && (pSepInfo->m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
				strRemappedColor = pDoc->m_ColorDefinitionTable[pSepInfo->m_nColorDefinitionIndex].m_strColorName;
		if (pDoc->m_ColorDefinitionTable[pSepInfo->m_nColorDefinitionIndex].m_strColorName == _T("Composite"))
			return CColorDefinition::Unseparated;
		else
			bSeparated = TRUE;
	}

	if (ContainsUnseparatedColor(nDocColorDefinitionIndex, FALSE, pPrintSheet, NULL, pMark, NULL, pColorDefTable))
		return (bSeparated) ? CColorDefinition::Mixed : CColorDefinition::Unseparated;
	else
		if (ContainsUnseparatedColor(nDocColorDefinitionIndex, TRUE, pPrintSheet, NULL, pMark, &strRemappedColor, pColorDefTable))
			return (bSeparated) ? CColorDefinition::Mixed : CColorDefinition::Unseparated;

	return bSeparated;
}

// check if multipage member - if not, check if pPageSourceHeader represents double page and if so, set double page now automatically
BOOL CPageTemplate::CheckMultipage(CPageSourceHeader* pPageSourceHeader)
{
	if ((m_nPageType != SinglePage) && (m_nPageType != MultipageMember)) // is control mark
		return FALSE;

	if (m_nMultipageGroupNum >= 0)
		return TRUE;

	if ( ! theApp.settings.m_bAutoCheckDoublePages || IsFlatProduct() )
		return FALSE;

	if ( ! pPageSourceHeader)
		return FALSE;

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CLayoutObject* pObject		 = GetLayoutObject();
	float		   fWidth		 = (pObject) ? pObject->GetRealWidth() : GetProductPart().m_fPageWidth;
	float		   fTrimBoxWidth = pPageSourceHeader->m_fTrimBoxRight - pPageSourceHeader->m_fTrimBoxLeft;
	if (fTrimBoxWidth == 0.0f)	// no trim box
		fTrimBoxWidth = pPageSourceHeader->m_fBBoxRight - pPageSourceHeader->m_fBBoxLeft;

	float fTolerance = 1.0f;	// 1 mm tolerance
	if (fTrimBoxWidth >= (2.0f * fWidth - fTolerance) )
	{
		int nIndex = pDoc->m_PageTemplateList.GetIndex(this, m_nProductPartIndex);
		CPageListView::SetDoublePage(this, (nIndex%2) ? RIGHT : LEFT, FALSE, TRUE);
		return TRUE;
	} 

	return FALSE;
}

SEPARATIONINFO* CPageTemplate::GetFirstSepInfo(INT_PTR nPageNumInSourceFile, INT_PTR* pSepIndex)
{
	if (pSepIndex)
		*pSepIndex = -1;

	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
		if (m_ColorSeparations[i].m_nPageNumInSourceFile == nPageNumInSourceFile)
		{
			if (pSepIndex)
				*pSepIndex = i;
			return &m_ColorSeparations[i];
		}

	return NULL;
}

SEPARATIONINFO* CPageTemplate::GetNextSepInfo(SEPARATIONINFO* pSepInfo, INT_PTR nPageNumInSourceFile, INT_PTR* pSepIndex)
{
	if (pSepIndex)
		*pSepIndex = -1;

	int i;
	for (i = 0; i < m_ColorSeparations.GetSize(); i++)
		if (&m_ColorSeparations[i] == pSepInfo)
			break;
	for (i++; i < m_ColorSeparations.GetSize(); i++)
		if (m_ColorSeparations[i].m_nPageNumInSourceFile == nPageNumInSourceFile)
		{
			if (pSepIndex)
				*pSepIndex = i;
			return &m_ColorSeparations[i];
		}

	return NULL;
}

void CPageTemplate::InsertSepInfo(SEPARATIONINFO* pSepInfo)
{
	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		if (m_ColorSeparations[i].m_nPageNumInSourceFile == pSepInfo->m_nPageNumInSourceFile)
		{
			for (i++; i < m_ColorSeparations.GetSize(); i++)
				if (m_ColorSeparations[i].m_nPageNumInSourceFile != pSepInfo->m_nPageNumInSourceFile)
					break;
			m_ColorSeparations.InsertAt(i, *pSepInfo);
			return;
		}
	}
	m_ColorSeparations.Add(*pSepInfo);
}

void CPageTemplate::RemoveSepInfo(SEPARATIONINFO* pSepInfo, BOOL bKeepLast)
{
	if (bKeepLast)
	{
		if ( ! pSepInfo)
			return;

		int nNum = 0;
		for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
		{
			if ( (m_ColorSeparations[i].m_nPageSourceIndex == pSepInfo->m_nPageSourceIndex) && (m_ColorSeparations[i].m_nPageNumInSourceFile == pSepInfo->m_nPageNumInSourceFile) )
				nNum++;
		}
		if (nNum <= 1)
		{
			pSepInfo->m_nColorDefinitionIndex = -1;
			return;
		}
	}

	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
		if (&m_ColorSeparations[i] == pSepInfo)
		{
			m_ColorSeparations.RemoveAt(i);
			break;
		}
}

void CPageTemplate::RemoveSpotSeps()
{
	if ( ! IsSystemCreated())	// only defined for system created marks
		return;
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! m_ColorSeparations.GetSize())
		return;

	CPageSource* pObjectSource = GetObjectSource(m_ColorSeparations[0].m_nColorDefinitionIndex);

	CString	strColorName;
	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		int nColorDefinitionIndex = m_ColorSeparations[i].m_nColorDefinitionIndex;
		if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= pDoc->m_ColorDefinitionTable.GetSize()) )
		{
			int	nPageNumInSourceFile = m_ColorSeparations[i].m_nPageNumInSourceFile;
			if (pObjectSource)
				if ( (nPageNumInSourceFile >= 1) && (nPageNumInSourceFile - 1 < pObjectSource->m_PageSourceHeaders.GetSize()) )
				{
					CString strUnknown;
					strUnknown.LoadString(IDS_UNKNOWN_STR);
					strColorName = pObjectSource->m_PageSourceHeaders[nPageNumInSourceFile - 1].m_strColorName;
					if ( (strColorName.CompareNoCase(strUnknown) == 0) || (strColorName.CompareNoCase(_T("Composite")) == 0) )
						continue;	// don't remove
				}
			m_ColorSeparations.RemoveAt(i);
			if (pObjectSource)
				if ( (nPageNumInSourceFile >= 1) && (nPageNumInSourceFile - 1 < pObjectSource->m_PageSourceHeaders.GetSize()) )
					pObjectSource->m_PageSourceHeaders.RemoveAt(nPageNumInSourceFile - 1);
			for (int j = 0; j < m_ColorSeparations.GetSize(); j++)
				if (m_ColorSeparations[j].m_nPageNumInSourceFile >= nPageNumInSourceFile)
					m_ColorSeparations[j].m_nPageNumInSourceFile--;
			i--;
			continue;
		}

		strColorName = pDoc->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName;
		if (strColorName.CompareNoCase(_T("Composite")) == 0)	
			continue;

		CColorDefinition* pColorDef	= theApp.m_colorDefTable.FindColorDefinition(strColorName);
		if (pColorDef)
		{
			if (pColorDef->m_nColorType == CColorDefinition::Spot)
			{
				int	nPageNumInSourceFile = m_ColorSeparations[i].m_nPageNumInSourceFile;
				m_ColorSeparations.RemoveAt(i);
				if (pObjectSource)
					if (nPageNumInSourceFile - 1 < pObjectSource->m_PageSourceHeaders.GetSize())
						pObjectSource->m_PageSourceHeaders.RemoveAt(nPageNumInSourceFile - 1);
				for (int j = 0; j < m_ColorSeparations.GetSize(); j++)
					if (m_ColorSeparations[j].m_nPageNumInSourceFile >= nPageNumInSourceFile)
						m_ColorSeparations[j].m_nPageNumInSourceFile--;
				i--;
			}
		}
		else
		{
			int	nPageNumInSourceFile = m_ColorSeparations[i].m_nPageNumInSourceFile;
			m_ColorSeparations.RemoveAt(i);
			if (pObjectSource)
				if (nPageNumInSourceFile - 1 < pObjectSource->m_PageSourceHeaders.GetSize())
					pObjectSource->m_PageSourceHeaders.RemoveAt(nPageNumInSourceFile - 1);
			for (int j = 0; j < m_ColorSeparations.GetSize(); j++)
				if (m_ColorSeparations[j].m_nPageNumInSourceFile >= nPageNumInSourceFile)
					m_ColorSeparations[j].m_nPageNumInSourceFile--;
			i--;
		}
	}
}

BOOL CPageTemplate::HasProcessColors(CMark* pMark, CColorDefinitionTable* pColorDefTable)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	if ( ! m_ColorSeparations.GetSize())
		return FALSE;
	if ( ! pColorDefTable)
		return FALSE;

	if ( ! IsPage())
		if (IsSystemCreated(pMark))
		{
			if (m_bMap2AllColors)
				return TRUE;
		}

	CPageSource* pObjectSource = (pMark) ? &pMark->m_MarkSource : GetObjectSource(m_ColorSeparations[0].m_nColorDefinitionIndex);

	CString	strColorName;
	CString strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);
	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		int	nPageNumInSourceFile  = m_ColorSeparations[i].m_nPageNumInSourceFile;
		int nColorDefinitionIndex = m_ColorSeparations[i].m_nColorDefinitionIndex;
		if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= pColorDefTable->GetSize()) )
		{
			if (pObjectSource)
				if ( (nPageNumInSourceFile >= 1) && (nPageNumInSourceFile - 1 < pObjectSource->m_PageSourceHeaders.GetSize()) )
				{
					CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[nPageNumInSourceFile - 1];
					strColorName = rObjectSourceHeader.m_strColorName;
					if ( (strColorName.CompareNoCase(strUnknown) == 0) || (strColorName.CompareNoCase(_T("Composite")) == 0) )
					{
						if ( ! IsSystemCreated(pMark))	// for syscreated marks composite is obsolete
							return TRUE;				// we only have map2all, map2spots or single colors
					}
					else
						if ( (rObjectSourceHeader.GetColorType() != CColorDefinition::Spot) && (rObjectSourceHeader.GetColorType() != CColorDefinition::Alternate) )
							return TRUE;
				}
		}
		else
		{
			strColorName = pColorDefTable->GetColorName(nColorDefinitionIndex);
			if ( (strColorName.CompareNoCase(strUnknown) == 0) || (strColorName.CompareNoCase(_T("Composite")) == 0) ) 
			{
				if ( ! IsSystemCreated(pMark))
					return TRUE;
			}
			else
			{
				int nColorType = -1;
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
				if ( ! pColorDef)
					nColorType = CColorDefinition::Spot;
				else
					nColorType =  pColorDef->m_nColorType;

				if ( (nColorType != CColorDefinition::Spot) && (nColorType != CColorDefinition::Alternate) )
					return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CPageTemplate::HasProcessColor(CMark* pMark, int nColorType, CColorDefinitionTable* pColorDefTable)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	pColorDefTable = ( ! pColorDefTable) ? ((pDoc) ? &pDoc->m_ColorDefinitionTable : &theApp.m_colorDefTable) : pColorDefTable;

	CString strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);
	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		int nColorDefinitionIndex = m_ColorSeparations[i].m_nColorDefinitionIndex;
		if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= pColorDefTable->GetSize()) )
			continue;

		CString strColorName = pColorDefTable->GetColorName(nColorDefinitionIndex);
		if ( (strColorName.CompareNoCase(strUnknown) == 0) || (strColorName.CompareNoCase(_T("Composite")) == 0) )
		{
			CPageSource* pObjectSource = (pMark) ? &pMark->m_MarkSource : GetObjectSource(m_ColorSeparations[i].m_nColorDefinitionIndex);
			if (pObjectSource)
			{
				int	nPageNumInSourceFile = m_ColorSeparations[i].m_nPageNumInSourceFile;
				if ( (nPageNumInSourceFile >= 1) && (nPageNumInSourceFile - 1 < pObjectSource->m_PageSourceHeaders.GetSize()) )
				{
					CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[nPageNumInSourceFile - 1];
					for (int j = 0; j < rObjectSourceHeader.m_processColors.GetSize(); j++)
					{
						CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(rObjectSourceHeader.m_processColors[j]);
						if (pColorDef)
							if (pColorDef->m_nColorType == nColorType)
								return TRUE;
					}
				}
			}
		}

		CColorDefinition& colorDef = pColorDefTable->GetAt(nColorDefinitionIndex);
		if (colorDef.m_nColorType == nColorType)
			return TRUE;
	}
	return FALSE;
}

BOOL CPageTemplate::HasSpotColors(CMark* pMark, CColorDefinitionTable* pColorDefTable)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	if ( ! m_ColorSeparations.GetSize())
		return FALSE;
	if ( ! pColorDefTable)
		return FALSE;

	CPageSource* pObjectSource = (pMark) ? &pMark->m_MarkSource : GetObjectSource(m_ColorSeparations[0].m_nColorDefinitionIndex);

	CString	strColorName;
	CString strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);
	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		int	nPageNumInSourceFile  = m_ColorSeparations[i].m_nPageNumInSourceFile;
		int nColorDefinitionIndex = m_ColorSeparations[i].m_nColorDefinitionIndex;
		if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= pColorDefTable->GetSize()) )
		{
			if (pObjectSource)
				if ( (nPageNumInSourceFile >= 1) && (nPageNumInSourceFile - 1 < pObjectSource->m_PageSourceHeaders.GetSize()) )
				{
					CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[nPageNumInSourceFile - 1];
					strColorName = rObjectSourceHeader.m_strColorName;
					if ( (strColorName.CompareNoCase(strUnknown) == 0) || (strColorName.CompareNoCase(_T("Composite")) == 0) )
						continue;	
					else
						if (rObjectSourceHeader.GetColorType() == CColorDefinition::Spot)
							return TRUE;
				}
		}
		else
		{
			strColorName = pColorDefTable->GetColorName(nColorDefinitionIndex);
			if ( (strColorName.CompareNoCase(strUnknown) == 0) || (strColorName.CompareNoCase(_T("Composite")) == 0) )
			{
				if ( (nPageNumInSourceFile >= 1) && (nPageNumInSourceFile - 1 < pObjectSource->m_PageSourceHeaders.GetSize()) )
				{
					CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[nPageNumInSourceFile - 1];
					if (rObjectSourceHeader.m_spotColors.GetSize())
						return TRUE;
					else
						continue;
				}
			}
			else
			{
				int nColorType = -1;
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
				if ( ! pColorDef)
					nColorType = CColorDefinition::Spot;
				else
					nColorType =  pColorDef->m_nColorType;

				if (nColorType == CColorDefinition::Spot)
					return TRUE;
			}
		}
	}

	return FALSE;
}

int	CPageTemplate::GetTotalNumColorants()
{
	INT_PTR nNumColorants = 0;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		nNumColorants++;

		if ( ! pDoc )
			continue;

		CPageSourceHeader* pPageSourceHeader;
		if ( (m_ColorSeparations[i].m_nColorDefinitionIndex >= 0) && (m_ColorSeparations[i].m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
			pPageSourceHeader = GetObjectSourceHeader(m_ColorSeparations[i].m_nColorDefinitionIndex);
		else
			pPageSourceHeader = GetObjectSourceHeader(-1);

		if (pPageSourceHeader)
			nNumColorants += pPageSourceHeader->m_spotColors.GetSize();
	}

	return (int)nNumColorants;
}

CBoundProduct& CPageTemplate::GetBoundProduct()
{
	static private CBoundProduct nullBoundProduct;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullBoundProduct;

	return pDoc->m_boundProducts.GetProduct(m_nProductIndex);
}

CBoundProductPart& CPageTemplate::GetProductPart()
{
	static private CBoundProductPart nullBoundProductPart;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullBoundProductPart;

	int		 nProductPartIndex = 0;
	POSITION pos			   = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			if (nProductPartIndex == m_nProductPartIndex)
				return rBoundProduct.m_parts[i];
			nProductPartIndex++;
		}
	}

	return nullBoundProductPart;
}

CFlatProduct& CPageTemplate::GetFlatProduct()
{
	static private CFlatProduct nullFlatProduct;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullFlatProduct;

	if (pDoc->GetFlatProductsIndex() != m_nProductPartIndex)
		nullFlatProduct;

	int nIndex = pDoc->m_PageTemplateList.GetIndex(this, m_nProductPartIndex);
	return pDoc->m_flatProducts.FindByIndex(nIndex/2);
}


////////////////////////////// CPrintSheetLocation ////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// CPrintSheetLocation

IMPLEMENT_SERIAL(CPrintSheetLocation, CObject, VERSIONABLE_SCHEMA | 1)

CPrintSheetLocation::CPrintSheetLocation()
{
}

CPrintSheetLocation::CPrintSheetLocation(const CPrintSheetLocation& rPrintSheetLocation) 
{
	Copy(rPrintSheetLocation);
}

const CPrintSheetLocation& CPrintSheetLocation::operator=(const CPrintSheetLocation& rPrintSheetLocation)
{
	Copy(rPrintSheetLocation);

	return *this;
}


void CPrintSheetLocation::Copy(const CPrintSheetLocation& rPrintSheetLocation)
{
	m_nPrintSheetIndex	 = rPrintSheetLocation.m_nPrintSheetIndex;
	m_nLayoutSide		 = rPrintSheetLocation.m_nLayoutSide;
	m_nLayoutObjectIndex = rPrintSheetLocation.m_nLayoutObjectIndex;
}


// helper function for CPrintSheetLocation elements
template <> void AFXAPI SerializeElements <CPrintSheetLocation> (CArchive& ar, CPrintSheetLocation* pPrintSheetLocation, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrintSheetLocation));

	UINT nVersion = 0;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();
		
	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPrintSheetLocation->m_nPrintSheetIndex;
			ar << pPrintSheetLocation->m_nLayoutSide;
			ar << pPrintSheetLocation->m_nLayoutObjectIndex;
		}
		else
		{
			switch (nVersion)
			{
			case 1:
				ar >> pPrintSheetLocation->m_nPrintSheetIndex;
				ar >> pPrintSheetLocation->m_nLayoutSide;
				ar >> pPrintSheetLocation->m_nLayoutObjectIndex;
				break;
			}
		}
	 	pPrintSheetLocation++;
	}
}


CPrintSheet* CPrintSheetLocation::GetPrintSheet()
{
	POSITION pos = CImpManDoc::GetDoc()->m_PrintSheetList.FindIndex(m_nPrintSheetIndex);
	return (pos) ? &CImpManDoc::GetDoc()->m_PrintSheetList.GetAt(pos) : NULL;
}

CLayoutSide* CPrintSheetLocation::GetLayoutSide()
{
	CLayout* pLayout = GetPrintSheet()->GetLayout();
	if ( ! pLayout)
		return NULL;

	if (m_nLayoutSide == FRONTSIDE)
		return &pLayout->m_FrontSide;
	else
		return &pLayout->m_BackSide;
}

CLayoutObject* CPrintSheetLocation::GetLayoutObject()
{
	CLayout* pLayout = GetPrintSheet()->GetLayout();
	if ( ! pLayout)
		return NULL;

	if (m_nLayoutSide == FRONTSIDE)
	{
		POSITION pos =  pLayout->m_FrontSide.m_ObjectList.FindIndex(m_nLayoutObjectIndex);
		return (pos) ? &pLayout->m_FrontSide.m_ObjectList.GetAt(pos) : NULL;
	}
	else
	{
		POSITION pos =  pLayout->m_BackSide.m_ObjectList.FindIndex(m_nLayoutObjectIndex);
		return (pos) ? &pLayout->m_BackSide.m_ObjectList.GetAt(pos) : NULL;
	}
}

CFoldSheet* CPrintSheetLocation::GetFoldSheet()
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	CLayoutObject* pLayoutObject	 = GetLayoutObject();
	if ( ! pLayoutObject || ! pPrintSheet)
		return NULL;

	return pPrintSheet->GetFoldSheet(pLayoutObject->m_nComponentRefIndex);
}


CPlateList* CPrintSheetLocation::GetPlateList()
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return NULL;

	if (m_nLayoutSide == FRONTSIDE)
		return &pPrintSheet->m_FrontSidePlates;
	else
		return &pPrintSheet->m_BackSidePlates;
}


CExposure* CPrintSheetLocation::GetExposure(short nColorDefinitionIndex)
{
	CPlateList* pPlateList = GetPlateList();
	if ( ! pPlateList)
		return NULL;

	if (nColorDefinitionIndex == -1)	// get exposure at default plate
	{
		POSITION seqPos = pPlateList->m_defaultPlate.m_ExposureSequence.GetHeadPosition();
		while (seqPos)
		{
			CExposure& rExposure = pPlateList->m_defaultPlate.m_ExposureSequence.GetNext(seqPos);
			if (rExposure.m_nLayoutObjectIndex == m_nLayoutObjectIndex)
				return &rExposure;
		}
	}
	else
	{
		POSITION platePos = pPlateList->GetHeadPosition();
		while (platePos)
		{
			CPlate& rPlate = pPlateList->GetNext(platePos);
			if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
			{
				POSITION seqPos = rPlate.m_ExposureSequence.GetHeadPosition();
				while (seqPos)
				{
					CExposure& rExposure = rPlate.m_ExposureSequence.GetNext(seqPos);
					if (rExposure.m_nLayoutObjectIndex == m_nLayoutObjectIndex)
						return &rExposure;
				}
				return NULL;
			}
		}
	}
	return NULL;
}


void CPrintSheetLocation::AddExposure(short nColorDefinitionIndex)
{
	CPlateList* pPlateList = GetPlateList();
	if ( ! pPlateList)
		return;

	CExposure exposure;
	exposure.Create(&pPlateList->m_defaultPlate, m_nLayoutObjectIndex);

	if (nColorDefinitionIndex == -1)	// add exposure at default plate
	{
		pPlateList->m_defaultPlate.m_ExposureSequence.AddTail(exposure);
		return;
	}
	else
	{
		if ( ! pPlateList->m_defaultPlate.m_ExposureSequence.GetExposure(m_nLayoutObjectIndex))		// 18.6.08: always add to default plate also
			pPlateList->m_defaultPlate.m_ExposureSequence.AddTail(exposure);
	
		POSITION platePos = pPlateList->GetHeadPosition();
		while (platePos)
		{
			CPlate& rPlate = pPlateList->GetNext(platePos);
			if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
			{
				rPlate.m_ExposureSequence.AddTail(exposure);
				return;
			}
		}
	}

	CLayoutObject* pLayoutObject = GetLayoutObject();

	if (pLayoutObject)
		if (pLayoutObject->m_nType == CLayoutObject::Page)	// create new plate only for pages
		{
			CPlate plate;
			plate.Create(&pPlateList->m_defaultPlate, exposure, nColorDefinitionIndex, GetPrintSheet());
			pPlateList->AddTail(plate);
		}
}


void CPrintSheetLocation::RemoveExposure(short nColorDefinitionIndex)
{
	CPlateList* pPlateList = GetPlateList();
	if ( ! pPlateList)
		return;

	if (nColorDefinitionIndex == -1)	// remove exposure at default plate
	{
		POSITION seqPos = pPlateList->m_defaultPlate.m_ExposureSequence.GetHeadPosition();
		while (seqPos)
		{
			CExposure& rExposure = pPlateList->m_defaultPlate.m_ExposureSequence.GetAt(seqPos);
			if (rExposure.m_nLayoutObjectIndex == m_nLayoutObjectIndex)
			{
				pPlateList->m_defaultPlate.m_ExposureSequence.RemoveAt(seqPos);
				break;
			}
			pPlateList->m_defaultPlate.m_ExposureSequence.GetNext(seqPos);
		}
	}
	else	
	{
		POSITION platePos = pPlateList->GetHeadPosition();
		while (platePos)
		{
			CPlate& rPlate = pPlateList->GetNext(platePos);
			if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
			{
				POSITION seqPos = rPlate.m_ExposureSequence.GetHeadPosition();
				while (seqPos)
				{
					CExposure& rExposure = rPlate.m_ExposureSequence.GetAt(seqPos);
					if (rExposure.m_nLayoutObjectIndex == m_nLayoutObjectIndex)
					{
						rPlate.m_ExposureSequence.RemoveAt(seqPos);
						break;
					}
					rPlate.m_ExposureSequence.GetNext(seqPos);
				}
			}
		}
	}
}

CRect CPageTemplate::Draw(CDC* pDC, int* nTop, int* nMinBottom, int nSide, int nIndex, long lTemplateListCenterX, BOOL bShowTrims, BOOL bShowColors, BOOL bShowPageNums, BOOL bShowShingling, COLORREF crPage, 
						  CDisplayList* pDisplayList, BOOL bShowTitle, BOOL bShowMasking, float fBleed, float fGluelineWidth, float fSpineArea, float fHeadTrim, float fFootTrim, float fSideTrim, BOOL bThumbnail)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(0,0,0,0);

	CLayoutObject* pObject = GetLayoutObject();
	float		   fWidth = 0.0f, fHeight = 0.0f, fSpine = 0.0f;

	if (nIndex >= pDoc->m_PageTemplateList.GetCount())
		return CRect(0,0,0,0);

	CBoundProductPart& rProductPart	= pDoc->m_boundProducts.GetProductPart(m_nProductPartIndex);
	CBoundProduct&	   rProduct		= rProductPart.GetBoundProduct();

	CRect rcBox(0,0,0,0);

	if (pObject)
	{
		fWidth  = pObject->GetRealWidth();
		fHeight = pObject->GetRealHeight();
		if ( ! IsFlatProduct())
			fSpine = rProduct.m_bindingParams.GetSpine(rProductPart);
	}
	else
	{
		if ( ! IsFlatProduct())
		{
			fWidth  = rProductPart.m_fPageWidth;
			fHeight = rProductPart.m_fPageHeight;
			fSpine  = rProduct.m_bindingParams.GetSpine(rProductPart);
		}
		else
		{
			if (nIndex < 2*pDoc->m_flatProducts.GetSize())
			{
				fWidth  = pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fTrimSizeWidth;
				fHeight = pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fTrimSizeHeight;
			}
		}
	}

	// Draw page
	int   nFlag = DT_LEFT, nTextY = 0;
	CRect rect, trimRect(0,0,0,0), overfoldRect(0,0,0,0), spineRect(0,0,0,0);
	float fOverFold = ( ! IsFlatProduct()) ? rProduct.m_bindingParams.m_fOverFold : 0.0f; 
	switch (nSide)
	{
	case LEFT:	*nTop	   = *nMinBottom - 10;
				rect.left  = lTemplateListCenterX - CPrintSheetView::WorldToLP(fWidth) - CPrintSheetView::WorldToLP(fSpine/2.0f); 
				rect.top   = *nTop;
				rect.right = rect.left + CPrintSheetView::WorldToLP(fWidth);	rect.bottom = rect.top - CPrintSheetView::WorldToLP(fHeight);
				nFlag	   = DT_LEFT;
				nTextY	   = rect.top;
				trimRect   = rect; 
				if ( ! IsFlatProduct())
				{
					trimRect.top	 += CPrintSheetView::WorldToLP( fHeadTrim);
					trimRect.bottom  -= CPrintSheetView::WorldToLP( fFootTrim);
					trimRect.left	 -= CPrintSheetView::WorldToLP( fSideTrim);
				}
				else
				{
					trimRect.top	 += CPrintSheetView::WorldToLP( pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fTopTrim	);
					trimRect.bottom	 -= CPrintSheetView::WorldToLP( pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fBottomTrim);
					trimRect.left	 -= CPrintSheetView::WorldToLP( pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fLeftTrim  );
					trimRect.right	 += CPrintSheetView::WorldToLP( pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fRightTrim );
				}
				spineRect		  = trimRect;
				overfoldRect	  = CRect(rect.left, trimRect.bottom, rect.left, trimRect.top);
				if ( ! IsFlatProduct())
				{
					if (nIndex == 0)
					{
						if (rProduct.m_bindingParams.m_nOverFoldSide != CBindingProcess::OverFoldNone)
							overfoldRect = CRect(trimRect.right, trimRect.bottom, trimRect.right + CPrintSheetView::WorldToLP(fOverFold), trimRect.top);
					}
					else
						if (rProduct.m_bindingParams.m_nOverFoldSide == CBindingProcess::OverFoldFront)
							overfoldRect = CRect(trimRect.left - CPrintSheetView::WorldToLP(fOverFold), trimRect.bottom, trimRect.left, trimRect.top);
				}
				spineRect.right += CPrintSheetView::WorldToLP(fSpine/2.0f);
				break;
	case RIGHT:	rect.left  = lTemplateListCenterX + CPrintSheetView::WorldToLP(fSpine/2.0f);	rect.top	= *nTop;
				rect.right = rect.left + CPrintSheetView::WorldToLP(fWidth);					rect.bottom = rect.top - CPrintSheetView::WorldToLP(fHeight);
				nFlag	   = DT_RIGHT;
				nTextY	   = rect.top;	
				trimRect   = rect; 
				if ( ! IsFlatProduct())
				{
					trimRect.top	 += CPrintSheetView::WorldToLP( fHeadTrim);
					trimRect.bottom  -= CPrintSheetView::WorldToLP( fFootTrim);
					trimRect.right	 += CPrintSheetView::WorldToLP( fSideTrim);
				}
				else
				{
					trimRect.top	 += CPrintSheetView::WorldToLP( pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fTopTrim	);
					trimRect.bottom	 -= CPrintSheetView::WorldToLP( pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fBottomTrim);
					trimRect.left	 -= CPrintSheetView::WorldToLP( pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fRightTrim );
					trimRect.right	 += CPrintSheetView::WorldToLP( pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fLeftTrim  );
				}
				spineRect		  = trimRect;
				overfoldRect	  = CRect(rect.right, trimRect.bottom, rect.right, trimRect.top);
				if ( ! IsFlatProduct())
					if (nIndex == 0)
					{
						if (rProduct.m_bindingParams.m_nOverFoldSide != CBindingProcess::OverFoldNone)
							overfoldRect = CRect(trimRect.right, trimRect.bottom, trimRect.right + CPrintSheetView::WorldToLP(fOverFold), trimRect.top);
					}
					else
						if (rProduct.m_bindingParams.m_nOverFoldSide == CBindingProcess::OverFoldBack)
							overfoldRect = CRect(trimRect.right, trimRect.bottom, trimRect.right + CPrintSheetView::WorldToLP(fOverFold), trimRect.top);
				spineRect.left -= CPrintSheetView::WorldToLP(fSpine/2.0f);
				break;

	}
	*nMinBottom = min(*nMinBottom, rect.bottom);

	int nOffset = trimRect.top - *nTop;
	rect.OffsetRect			(0, -nOffset);
	trimRect.OffsetRect		(0, -nOffset);
	overfoldRect.OffsetRect	(0, -nOffset);
	spineRect.OffsetRect	(0, -nOffset);

	BOOL		   bFirstOrLastPage = ( (nIndex == 0) || (nIndex == pDoc->m_PageTemplateList.GetCount(m_nProductPartIndex) - 1) ) ? TRUE : FALSE;
	CPageTemplate* p2ndTemplate  = NULL;
	CRect		   singlePageRect = rect;
	CRect		   multipageRect = rect;
	if (m_nMultipageGroupNum >= 0)
	{
		p2ndTemplate		   = GetFirstMultipagePartner();
		CLayoutObject* pObject = p2ndTemplate->GetLayoutObject();
		float		   fWidth, fHeight;
		if (pObject)
		{
			fWidth  = pObject->GetRealWidth();
			fHeight = pObject->GetRealHeight();
		}
		else
		{
			fWidth  = rProductPart.m_fPageWidth;
			fHeight = rProductPart.m_fPageHeight;
		}
		multipageRect.top	 = max(*nTop, rect.top);
		multipageRect.bottom = min(multipageRect.top - CPrintSheetView::WorldToLP(fHeight), rect.bottom);
		if (nSide == RIGHT)
		{
			multipageRect.left = lTemplateListCenterX - CPrintSheetView::WorldToLP(fWidth); multipageRect.right = rect.right;
		}
		else
		{
			multipageRect.left = rect.left; multipageRect.right = lTemplateListCenterX + CPrintSheetView::WorldToLP(fWidth + 0.5f);
		}
	}

	BOOL bShowBitmap = (crPage == WHITE) ? TRUE : FALSE;

	if (pDC->RectVisible(rect) && ( ((GetMultipageSide() != RIGHT) || ((GetMultipageSide() == RIGHT) && (nIndex == 0))) ) )
	{
		if (bShowTrims)
		{
			CPen* pOldPen = (CPen*)pDC->SelectStockObject(BLACK_PEN);
			CPrintSheetView::FillRectangleExt(pDC, spineRect,	 (bShowMasking) ? WHITE : RGB(190,220,240));
			CPrintSheetView::FillRectangleExt(pDC, trimRect,	 (bShowMasking) ? WHITE : RGB(221,236,247));
			spineRect.NormalizeRect();
			trimRect.NormalizeRect();
			rcBox.UnionRect(rcBox, spineRect);
			rcBox.UnionRect(rcBox, trimRect);
			if (fOverFold)
			{
				CPrintSheetView::FillRectangleExt(pDC, overfoldRect, WHITE);
				CPrintSheetView::FillTransparent(pDC,  overfoldRect, (bShowMasking) ? WHITE : RGB(170,225,0), TRUE, 70, TRUE);
				overfoldRect.NormalizeRect();
				rcBox.UnionRect(rcBox, overfoldRect);
			}
			pDC->SelectObject(pOldPen);
		}

		CPrintSheetView::FillRectangleExt(pDC, (bFirstOrLastPage) ? singlePageRect : multipageRect, (bShowMasking) ? WHITE : RGB(240,240,240)); 

		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
		CPen	pen(PS_SOLID, 1, LIGHTGRAY);
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		CPrintSheetView::DrawRectangleExt(pDC, (bFirstOrLastPage) ? singlePageRect : multipageRect);

		multipageRect.NormalizeRect();
		rcBox.UnionRect(rcBox, multipageRect);

		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// TODO: Show Colors ??
		if (bShowBitmap)
		{
			if (m_ColorSeparations.GetSize() > 0)
			{
				if ((GetMultipageSide() == LEFT) || ((GetMultipageSide() == RIGHT) && (nIndex == 0)) || (m_nMultipageGroupNum == -1))
				{
					int				   nColorDefinitionIndex = m_ColorSeparations[0].m_nColorDefinitionIndex;
					CPageSource*	   pPageSource			 = GetObjectSource	  (nColorDefinitionIndex);
					CPageSourceHeader* pPageSourceHeader	 = GetObjectSourceHeader(nColorDefinitionIndex);
					if (pPageSource && pPageSourceHeader)
					{
						CRect outBBox, pageBBox = (m_nMultipageGroupNum >= 0) ? multipageRect : rect;
						int	  nOutBBoxWidth, nOutBBoxHeight;
						if ((m_fContentRotation == 0.0f) || (m_fContentRotation == 180.0f))
						{
							nOutBBoxWidth  = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxRight - pPageSourceHeader->m_fBBoxLeft)   * m_fContentScaleX);
							nOutBBoxHeight = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxTop   - pPageSourceHeader->m_fBBoxBottom) * m_fContentScaleY);
						}
						else
						{
							nOutBBoxWidth  = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxTop   - pPageSourceHeader->m_fBBoxBottom) * m_fContentScaleX);
							nOutBBoxHeight = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxRight - pPageSourceHeader->m_fBBoxLeft)   * m_fContentScaleY);
						}
						outBBox.left   = pageBBox.CenterPoint().x - nOutBBoxWidth  / 2;
						outBBox.bottom = pageBBox.CenterPoint().y - nOutBBoxHeight / 2;
						outBBox.right  = pageBBox.CenterPoint().x + nOutBBoxWidth  / 2;
						outBBox.top    = pageBBox.CenterPoint().y + nOutBBoxHeight / 2;
						outBBox		  += CSize(CPrintSheetView::WorldToLP(GetContentOffsetX(CPageTemplate::AddShinglingOffset)), CPrintSheetView::WorldToLP(GetContentOffsetY(CPageTemplate::AddShinglingOffset)));
						
						POSITION pos = pDoc->m_PageSourceList.FindIndex(m_ColorSeparations[0].m_nPageSourceIndex);
						if (pos)
						{
							int nColorSel = 0;
							if (nColorSel >= 0 && nColorSel < m_ColorSeparations.GetSize())
							{
								pDC->IntersectClipRect((bFirstOrLastPage) ? singlePageRect : multipageRect);
								int nHeaderIndex = pPageSource->GetHeaderIndex(pPageSourceHeader);
								CString strPreviewFile = pPageSource->GetPreviewFileName(nHeaderIndex + 1);
								CPageListView::DrawPreview(pDC, outBBox, TOP, m_fContentRotation, strPreviewFile, FALSE, FALSE, (bThumbnail) ? &pPageSourceHeader->m_pThumbnailImage : NULL);
								pDC->SelectClipRgn(NULL);
							}
						}
					}
				}
			}
		}

		if (bShowTrims && ! bShowMasking)
			if ( ! IsFlatProduct())
				DrawMeasuresBound(pDC, nSide, multipageRect, overfoldRect, spineRect, trimRect, nIndex);
			else
				DrawMeasuresUnbound(pDC, multipageRect, trimRect, nIndex);

		if ( (bShowShingling) && ( ! IsFlatProduct()) )
		{
			if (rProductPart.m_foldingParams.m_bShinglingActive)
			{
				if (fabs(m_fShinglingOffsetX) > 0.0001f)
				{
					CSize size(4, 4);
					pDC->DPtoLP(&size);
					CPen pen(PS_SOLID, size.cx, RGB(252, 220, 150));
					pDC->SelectObject(&pen);
					CRect rcShingling = multipageRect; 
					if (nSide == LEFT)
					{
						rcShingling.left  += CPrintSheetView::WorldToLP(m_fShinglingOffsetX);
						if (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodScale)
							rcShingling.right  += CPrintSheetView::WorldToLP(m_fShinglingOffsetX);
					}
					else
					{
						rcShingling.right += CPrintSheetView::WorldToLP(m_fShinglingOffsetX);
						if (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodScale)
							rcShingling.left  += CPrintSheetView::WorldToLP(m_fShinglingOffsetX);
					}
					CPrintSheetView::DrawRectangleExt(pDC, rcShingling);
					pen.DeleteObject();
				}
			}
			CPen bluePen(PS_SOLID, 1, RGB(180,180,255));
			pDC->SelectObject(&bluePen);
			CPrintSheetView::DrawRectangleExt(pDC, (bFirstOrLastPage) ? singlePageRect : multipageRect);
			bluePen.DeleteObject();
		}

		//CPrintSheetView::DrawRectangleExt(pDC, multipageRect);
		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		pen.DeleteObject();
		/////////////////////////////////////////////////////////////////////////////////////////////////////////

		//DrawCropMarks(pDC, pCropMark, multipageRect);
		//if (nSide == RIGHT)
		//	DrawSectionMark(pDC, pSectionMark, multipageRect, fSpine);

		if (bShowColors)
			DrawColorants(pDC, multipageRect);
	}

	//if (pDC->RectVisible(rect))
	{
		CRect rcShingling = singlePageRect; 
		if (nSide == LEFT)
			rcShingling.left  += CPrintSheetView::WorldToLP(m_fShinglingOffsetX);
		else
			rcShingling.right += CPrintSheetView::WorldToLP(m_fShinglingOffsetX);

		CGraphicComponent gp;
		CRect rcFrame = rcShingling; rcFrame.NormalizeRect();
		COLORREF crShingling1((bShowShingling && rProductPart.m_foldingParams.m_bShinglingActive) ? RGB(236,162,  6) : MIDDLEGRAY);
		COLORREF crShingling2((bShowShingling && rProductPart.m_foldingParams.m_bShinglingActive) ? RGB(252,211,129) : WHITE);
		//if (nSide == LEFT)
		//{
		//	rcFrame.right = rcFrame.left + (singlePageRect.Width() / 3) * 2;
		//	gp.DrawTransparentSmoothFrame(pDC, rcFrame, crShingling1, crShingling2, 0, -1);
		//	rcFrame.left  = rcFrame.right; rcFrame.right = rcShingling.right;
		//	gp.DrawTransparentSmoothFrame(pDC, rcFrame, crShingling2, crShingling1, 0, -1);
		//}
		//else
		//{
		//	rcFrame.right = rcFrame.left + singlePageRect.Width() / 3;
		//	gp.DrawTransparentSmoothFrame(pDC, rcFrame, crShingling2, crShingling1, 0, -1);
		//	rcFrame.left  = rcFrame.right; rcFrame.right = rcShingling.right;
		//	gp.DrawTransparentSmoothFrame(pDC, rcFrame, crShingling1, crShingling2, 0, -1);
		//}

		if ( (bShowShingling) && ( ! IsFlatProduct()) )
		{
			if (rProductPart.m_foldingParams.m_bShinglingActive)
			{
				CFont font;
				CSize szFont(13, 13);
				pDC->DPtoLP(&szFont);
				font.CreateFont(szFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
				CFont* pOldFont = pDC->SelectObject(&font);

				COLORREF crTextOld  = pDC->SetTextColor(BLACK);
				int		 nBkModeOld = pDC->SetBkMode(OPAQUE);
				COLORREF crBkOld	= pDC->SetBkColor(WHITE);//RGB(235,220,197));
				if ( (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottling) || (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale) )
				{
					CPrintSheetView::DrawPageShingling(pDC, this, rcShingling, TOP, TOP,	 pDisplayList);
					CPrintSheetView::DrawPageShingling(pDC, this, rcShingling, TOP, BOTTOM,  pDisplayList);
				}
				else
					CPrintSheetView::DrawPageShingling(pDC, this, rcShingling, TOP, YCENTER, pDisplayList);
				pDC->SetTextColor(crTextOld);
				pDC->SetBkMode(nBkModeOld);
				pDC->SetBkColor(crBkOld);
				font.DeleteObject();
				pDC->SelectObject(pOldFont);
			}
		}

		if (bShowTitle)
		{
			if (GetFoldSheet())
			{
				CSize szSize1(30,30); pDC->DPtoLP(&szSize1);
				CSize szSize2(10,10); pDC->DPtoLP(&szSize2);
				CRect rcTitle = singlePageRect; rcTitle.top = *nTop + szSize1.cy; rcTitle.bottom = *nTop + szSize2.cy;
				CGraphicComponent gp;
				CString string; string.Format(_T("%s (%s)"), rProductPart.m_strName, GetFoldSheet()->m_strFoldSheetTypeName);	
				gp.DrawTitleBar(pDC, rcTitle, string, RGB(233,240,130), RGB(210,225,25), 0, FALSE);
			}
		}
	}

	if (/*pDC->RectVisible(rect) && */bShowPageNums)
	{
		CFont font;
		CSize szFont;
		if (bThumbnail)
		{
			szFont = CSize(1500, 1500);
			pDC->LPtoDP(&szFont);
			if ( (szFont.cx > 8) || (szFont.cx < 6) )
			{
				szFont = CSize(8, 8);
				pDC->DPtoLP(&szFont);
			}
			else
				szFont = CSize(1500, 1500);
		}
		else
		{
			szFont = CSize(13, 13);
			pDC->DPtoLP(&szFont);
		}
		font.CreateFont(szFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		CFont* pOldFont = pDC->SelectObject(&font);

		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(230,230,230));
		CSize szSize(3,3);
		pDC->DPtoLP(&szSize);
		singlePageRect.DeflateRect(szSize.cx, -szSize.cy);
		CString strOut;
		strOut.Format(_T(" %s "), m_strPageID);
		pDC->SetTextColor(DARKBLUE);
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(220,225,240));//RGB(220,220,220));
		pDC->DrawText(strOut, singlePageRect, DT_SINGLELINE|DT_BOTTOM|nFlag);

		pDC->SelectObject(pOldFont);
		font.DeleteObject();
	}

	if (bShowMasking)
	{
		CRect exposureRect, pageRect = multipageRect;
		long  nBleed	 = CPrintSheetView::WorldToLP(fBleed);
		long  nGlueLine  = CPrintSheetView::WorldToLP(fGluelineWidth);
		long  nSpineTrim = CPrintSheetView::WorldToLP(fSpineArea);
		if (fabs(rProduct.m_bindingParams.m_fMillingDepth) > 0.01f)
		{
			exposureRect.left	= (nSide == LEFT)  ? pageRect.left  - nBleed : pageRect.left  + nGlueLine;
			exposureRect.right	= (nSide == RIGHT) ? pageRect.right + nBleed : pageRect.right - nGlueLine;
			exposureRect.bottom	= pageRect.bottom + nBleed;
			exposureRect.top	= pageRect.top	  - nBleed;
		}
		else
		{
			exposureRect.left	= (nSide == LEFT)  ? pageRect.left  - nBleed : pageRect.left  - min(nBleed, nSpineTrim/2);
			exposureRect.right	= (nSide == RIGHT) ? pageRect.right + nBleed : pageRect.right + min(nBleed, nSpineTrim/2);
			exposureRect.bottom	= pageRect.bottom + nBleed;
			exposureRect.top	= pageRect.top	  - nBleed;
		}

		CPrintSheetView::FillTransparent(pDC, exposureRect, MARINE, TRUE);
		pDC->SelectStockObject(BLACK_PEN);
		pDC->SelectStockObject(HOLLOW_BRUSH);
		CPrintSheetView::DrawRectangleExt(pDC, exposureRect);

		exposureRect.NormalizeRect();
		pageRect.NormalizeRect();
		pageRect.IntersectRect(exposureRect, pageRect);
		
		CBrush pageBrush(MARINE);		
		pDC->SelectStockObject(NULL_PEN);
		pDC->SelectObject(&pageBrush);
		CPrintSheetView::FillTransparent(pDC, pageRect, MARINE, TRUE, 150);

		pageRect = multipageRect;
		pDC->LPtoDP(exposureRect);	// CalcBleedValueRect() needs device coordinates
		pDC->LPtoDP(pageRect);		// CalcBleedValueRect() needs device coordinates
		CString bleedValueString;
		CRect	bleedValueRect;
		CFont font;
		CSize szFont(13, 13);
		pDC->DPtoLP(&szFont);
		font.CreateFont(szFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		CFont* pOldFont = pDC->SelectObject(&font);
		COLORREF crTextOld   = pDC->SetTextColor(BLACK);
		int		 nBkModeOld  = pDC->SetBkMode(OPAQUE);
		int		 nBkColorOld = pDC->SetBkColor(RGB(0,220,220));

		int nTemp = pageRect.top;	  pageRect.top	   = pageRect.bottom;	  pageRect.bottom	  = nTemp;
			nTemp = exposureRect.top; exposureRect.top = exposureRect.bottom; exposureRect.bottom = nTemp;

		if (fBleed > 0.01)
		{
			bleedValueRect = CPrintSheetView::CalcBleedValueRect(fBleed, pageRect, TOP,    bleedValueString, pDisplayList, pDC);
			pDC->DPtoLP(bleedValueRect);	
			pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
			CPrintSheetView::DrawMaskArrow(pDC, exposureRect, pageRect, TOP);

			bleedValueRect = CPrintSheetView::CalcBleedValueRect(fBleed, pageRect, BOTTOM, bleedValueString, pDisplayList, pDC);
			pDC->DPtoLP(bleedValueRect);	
			pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
			CPrintSheetView::DrawMaskArrow(pDC, exposureRect, pageRect, BOTTOM);
			if (nSide == LEFT)
			{
				bleedValueRect = CPrintSheetView::CalcBleedValueRect(fBleed, pageRect, LEFT,   bleedValueString, pDisplayList, pDC);
				pDC->DPtoLP(bleedValueRect);	
				pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
				CPrintSheetView::DrawMaskArrow(pDC, exposureRect, pageRect, LEFT);
				if ( (fabs(rProduct.m_bindingParams.m_fMillingDepth) <= 0.01f) && (fabs(fSpineArea) >= 0.01f) )
				{
					bleedValueRect = CPrintSheetView::CalcBleedValueRect(min(fBleed, fSpineArea/2.0f), pageRect, RIGHT,   bleedValueString, pDisplayList, pDC);
					pDC->DPtoLP(bleedValueRect);	
					pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
					CPrintSheetView::DrawMaskArrow(pDC, exposureRect, pageRect, RIGHT);
				}
			}
			if (nSide == RIGHT)
			{
				bleedValueRect = CPrintSheetView::CalcBleedValueRect(fBleed, pageRect, RIGHT,  bleedValueString, pDisplayList, pDC);
				pDC->DPtoLP(bleedValueRect);	
				pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
				CPrintSheetView::DrawMaskArrow(pDC, exposureRect, pageRect, RIGHT);
				if ( (fabs(rProduct.m_bindingParams.m_fMillingDepth) <= 0.01f) && (fabs(fSpineArea) >= 0.01f) )
				{
					bleedValueRect = CPrintSheetView::CalcBleedValueRect(min(fBleed, fSpineArea/2.0f), pageRect, LEFT,   bleedValueString, pDisplayList, pDC);
					pDC->DPtoLP(bleedValueRect);	
					pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
					CPrintSheetView::DrawMaskArrow(pDC, exposureRect, pageRect, LEFT);
				}
			}
		}

		if (fGluelineWidth > 0.01)
		{
			if (nSide == RIGHT)
			{
				CRect rcGlueLine = multipageRect; rcGlueLine.right = rcGlueLine.left + CPrintSheetView::WorldToLP(fGluelineWidth);
				pDC->LPtoDP(rcGlueLine); rcGlueLine.InflateRect(1, -2); pDC->DPtoLP(rcGlueLine); 
				pDC->FillSolidRect(rcGlueLine, OCKER);//RGB(250,80,5));
				pDC->SetBkColor(RGB(250,115,55));
				bleedValueRect = CPrintSheetView::CalcBleedValueRect(-fGluelineWidth, pageRect, LEFT,   bleedValueString, pDisplayList, pDC);
				bleedValueRect.OffsetRect(12, 0);
				pDC->DPtoLP(bleedValueRect);	
				pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
				pageRect.left += 4;	exposureRect.left += 8;
				CPrintSheetView::DrawMaskArrow(pDC, exposureRect, pageRect, LEFT);
			}
			if (nSide == LEFT)
			{
				CRect rcGlueLine = multipageRect; rcGlueLine.left = rcGlueLine.right - CPrintSheetView::WorldToLP(fGluelineWidth);
				pDC->LPtoDP(rcGlueLine); rcGlueLine.InflateRect(1, -2); pDC->DPtoLP(rcGlueLine); 
				pDC->FillSolidRect(rcGlueLine, OCKER);//RGB(250,80,5));
				pDC->SetBkColor(RGB(250,115,55));
				bleedValueRect = CPrintSheetView::CalcBleedValueRect(-fGluelineWidth, pageRect, RIGHT,  bleedValueString, pDisplayList, pDC);
				bleedValueRect.OffsetRect(-12, 0);
				pDC->DPtoLP(bleedValueRect);	
				pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
				pageRect.right -= 4;	exposureRect.right -= 10;
				CPrintSheetView::DrawMaskArrow(pDC, exposureRect, pageRect, RIGHT);
			}
		}

		pDC->SelectObject(pOldFont);
		font.DeleteObject();
		pDC->SetTextColor(crTextOld);
		pDC->SetBkMode(nBkModeOld);
		pDC->SetBkColor(nBkColorOld);
	}

	return rcBox;
}

void CPageTemplate::DrawMeasuresBound(CDC* pDC, int nSide, CRect multipageRect, CRect overfoldRect, CRect spineRect, CRect trimRect, int nPageIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CSize sizeDistance(6,6);
	pDC->DPtoLP(&sizeDistance);
	CPen pen(PS_SOLID, 1, MIDDLEGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);

	CBoundProductPart&	rProductPart = GetProductPart();
	CBoundProduct&		rProduct	 = rProductPart.GetBoundProduct();
	float fOverFold = rProduct.m_bindingParams.m_fOverFold;
	if (rProduct.m_bindingParams.m_nOverFoldSide == CBindingProcess::OverFoldNone)
		fOverFold = 0.0f;
	else
		if ( (nPageIndex == 0) || (nPageIndex == pDoc->m_PageTemplateList.GetCount() - 1) )
			;
		else
			if ( (nSide == LEFT) && (rProduct.m_bindingParams.m_nOverFoldSide == CBindingProcess::OverFoldBack) ) 
				fOverFold = 0.0f;
			else
				if ( (nSide == RIGHT) && (rProduct.m_bindingParams.m_nOverFoldSide == CBindingProcess::OverFoldFront) ) 
					fOverFold = 0.0f;

	CString string;
	CPoint  hotSpot1, hotSpot2;
	CPrintSheetView::TruncateNumber(string, rProductPart.m_trimmingParams.m_fSideTrim + fOverFold);
	if ( (nPageIndex > 0) && (nSide == LEFT) )
	{
		hotSpot1 = CPoint(min(trimRect.left, overfoldRect.left),  multipageRect.CenterPoint().y); 
		DrawSingleMeasure(pDC, string, hotSpot1, RIGHT);
	}
	if ( (nPageIndex < pDoc->m_PageTemplateList.GetCount() - 1) && (nSide == RIGHT) )
	{
		hotSpot1 = CPoint(max(trimRect.right, overfoldRect.right), multipageRect.CenterPoint().y); 
		DrawSingleMeasure(pDC, string, hotSpot1, LEFT);
	}

	hotSpot1 = CPoint(multipageRect.CenterPoint().x, trimRect.bottom);  
	hotSpot2 = CPoint(multipageRect.CenterPoint().x, trimRect.top);  
	if ( (nSide == RIGHT) || (nPageIndex == pDoc->m_PageTemplateList.GetCount() - 1) )
	{
		CPrintSheetView::TruncateNumber(string, rProductPart.m_trimmingParams.m_fHeadTrim);
		DrawSingleMeasure(pDC, string, hotSpot1, BOTTOM);
		CPrintSheetView::TruncateNumber(string, rProductPart.m_trimmingParams.m_fFootTrim);
		DrawSingleMeasure(pDC, string, hotSpot2, TOP);
	}

	if ( (nPageIndex > 0) && (nPageIndex < pDoc->m_PageTemplateList.GetCount() - 1) )	// not first nor last
	{
		CPrintSheetView::TruncateNumber(string, rProduct.m_bindingParams.GetSpine(GetProductPart()));
		if (nSide == LEFT)
		{
			hotSpot1 = CPoint(multipageRect.right, multipageRect.top - sizeDistance.cy);  
			DrawSingleMeasure(pDC, string, hotSpot1, RIGHT);
		}
		else
		{
			hotSpot1 = CPoint(multipageRect.left, multipageRect.top - sizeDistance.cy);  
			DrawSingleMeasure(pDC, (string == _T("0")) ? _T("0") : _T(""), hotSpot1, LEFT);
		}
	}
	else
		if (nPageIndex == 0)	// first
		{
			CPrintSheetView::TruncateNumber(string, rProduct.m_bindingParams.GetSpine(GetProductPart())/2.0f);
			hotSpot1 = CPoint(spineRect.left, multipageRect.CenterPoint().y);  
			DrawSingleMeasure(pDC, string, hotSpot1, RIGHT);
		}
		else	// last
		{
			CPrintSheetView::TruncateNumber(string, rProduct.m_bindingParams.GetSpine(GetProductPart())/2.0f);
			hotSpot1 = CPoint(spineRect.right, multipageRect.CenterPoint().y);  
			DrawSingleMeasure(pDC, string, hotSpot1, LEFT);
		}

	pDC->SelectObject(pOldPen);
}

void CPageTemplate::DrawMeasuresUnbound(CDC* pDC, CRect pageRect, CRect trimRect, int nPageIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nFlatProductIndex = nPageIndex/2;
	if ( (nFlatProductIndex < 0) || (nFlatProductIndex >= pDoc->m_flatProducts.GetSize()) )
		return;

	CSize sizeDistance(6,6);
	pDC->DPtoLP(&sizeDistance);
	CPen pen(PS_SOLID, 1, MIDDLEGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);

	CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(nFlatProductIndex);

	CString string;
	CPoint  hotSpot1, hotSpot2;
	CPrintSheetView::TruncateNumber(string, rFlatProduct.m_fLeftTrim);
	hotSpot1 = CPoint(trimRect.left,  pageRect.CenterPoint().y); 
	DrawSingleMeasure(pDC, string, hotSpot1, RIGHT);

	CPrintSheetView::TruncateNumber(string, rFlatProduct.m_fRightTrim);
	hotSpot1 = CPoint(trimRect.right, pageRect.CenterPoint().y); 
	DrawSingleMeasure(pDC, string, hotSpot1, LEFT);

	hotSpot1 = CPoint(pageRect.CenterPoint().x, trimRect.bottom);  
	hotSpot2 = CPoint(pageRect.CenterPoint().x, trimRect.top);  
	CPrintSheetView::TruncateNumber(string, rFlatProduct.m_fTopTrim);
	DrawSingleMeasure(pDC, string, hotSpot1, BOTTOM);
	CPrintSheetView::TruncateNumber(string, rFlatProduct.m_fBottomTrim);
	DrawSingleMeasure(pDC, string, hotSpot2, TOP);

	pDC->SelectObject(pOldPen);
}

void CPageTemplate::DrawSingleMeasure(CDC* pDC, CString string, CPoint hotSpot, int nDirection, int nTextPosition, BOOL bBackground)
{
	if (string == _T("0"))
		return;

	CSize sizeFont(11,11);
	pDC->DPtoLP(&sizeFont);
	CFont font, *pOldFont;
	font.CreateFont(sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	pOldFont = pDC->SelectObject(&font);

	int nOrientation = ( (nDirection == LEFT) || (nDirection == RIGHT) ) ? HORIZONTAL : VERTICAL;

	const int nArrowLen = 10;
	RECT  outputRect;
	CSize textExtent = pDC->GetTextExtent(string);
	CSize sizeDistance((int)(5), (int)(5));
	pDC->DPtoLP(&sizeDistance);

	if (nTextPosition == -1)	// default behaviour
	{
		int XTextPos = hotSpot.x - (textExtent.cx/2);
		int YTextPos = hotSpot.y;
		outputRect.left	  = (nDirection == RIGHT)  ? XTextPos - sizeDistance.cx - textExtent.cx : XTextPos + sizeDistance.cx;
		outputRect.right  = outputRect.left   + textExtent.cx;
		outputRect.bottom = (nDirection != BOTTOM) ? YTextPos - sizeDistance.cy - textExtent.cy : YTextPos + sizeDistance.cy;
		outputRect.top	  = outputRect.bottom + textExtent.cy;
	}
	else
	{
		CSize sizeArrow = (nOrientation == HORIZONTAL) ? CSize(nArrowLen, 5) : CSize(5, nArrowLen);
		pDC->DPtoLP(&sizeArrow);
		CRect rcArrow;
		switch (nDirection)
		{
		case LEFT:		rcArrow = CRect(CPoint(hotSpot.x,				   hotSpot.y - sizeArrow.cy/2),	sizeArrow);	break; 
		case RIGHT:		rcArrow = CRect(CPoint(hotSpot.x - sizeArrow.cx,   hotSpot.y - sizeArrow.cy/2),	sizeArrow);	break; 
		case BOTTOM:	rcArrow = CRect(CPoint(hotSpot.x - sizeArrow.cx/2, hotSpot.y),					sizeArrow);	break; 
		case TOP:		rcArrow = CRect(CPoint(hotSpot.x - sizeArrow.cx/2, hotSpot.y - sizeArrow.cy),	sizeArrow);	break; 
		}
		rcArrow.InflateRect(sizeDistance);

		switch (nTextPosition)	
		{
		case LEFT:	
		case RIGHT:		outputRect.left	  = (nTextPosition == LEFT) ? rcArrow.left : rcArrow.right;
						outputRect.right  = outputRect.left + textExtent.cx;
						outputRect.bottom = hotSpot.y - textExtent.cy / 2;
						outputRect.top	  = outputRect.bottom + textExtent.cy;
						break;

		case BOTTOM:
		case TOP:		outputRect.bottom = (nTextPosition == BOTTOM) ? rcArrow.bottom : rcArrow.top;
						outputRect.top	  = outputRect.bottom + textExtent.cy;
						outputRect.left	  = hotSpot.x - textExtent.cx / 2;
						outputRect.right  = outputRect.left   + textExtent.cx;
						break;			
		}
	}

	COLORREF crOldColor   = pDC->SetTextColor(BLACK);
	int		 nOldBkMode	  = pDC->SetBkMode((bBackground) ? OPAQUE : TRANSPARENT);
	COLORREF crOldBkColor = pDC->SetBkColor(RGB(240,240,240));

	CString strValue;
	strValue.Format(_T(" %s "), string);
	pDC->DrawText(strValue, &outputRect, DT_NOCLIP|DT_SINGLELINE|DT_LEFT|DT_BOTTOM);

	COLORREF  crArrow	= LIGHTBLUE;
	pDC->LPtoDP(&hotSpot, 1);
	CPrintSheetView::DrawArrow(pDC, nDirection, hotSpot, nArrowLen, BLACK, crArrow);	

	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldColor);
	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);

	font.DeleteObject();
}

CRect CPageTemplate::DrawColorants(CDC* pDC, CRect rcRect)
{
	CRect rcBox(rcRect.TopLeft(), CSize(1, 1));

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CSize size(6, 6);
	pDC->DPtoLP(&size);
	CRect rcIcon(rcRect.left, rcRect.bottom - size.cy, rcRect.left + size.cx, rcRect.bottom);
	rcIcon.OffsetRect(0, -size.cy/2);

	CPen pen(PS_SOLID, 1, MIDDLEGRAY);
	CBrush brush;
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CBrush* pOldBrush = pDC->GetCurrentBrush();
	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		if ( (m_ColorSeparations[i].m_nColorDefinitionIndex < 0) || (m_ColorSeparations[i].m_nColorDefinitionIndex >= pDoc->m_ColorDefinitionTable.GetSize()) )
			continue;
		CColorDefinition& rColorDef = pDoc->m_ColorDefinitionTable[m_ColorSeparations[i].m_nColorDefinitionIndex];
		if (rColorDef.m_strColorName.CompareNoCase(_T("Composite")) == 0)
		{
			CPageSourceHeader* pPageSourceHeader = GetObjectSourceHeader(m_ColorSeparations[i].m_nColorDefinitionIndex);
			for (int j = 0; j < pPageSourceHeader->m_processColors.GetSize(); j++)
			{
				COLORREF rcColor = theApp.m_colorDefTable.FindColorRef(pPageSourceHeader->m_processColors[j]);
				brush.CreateSolidBrush(rcColor);
				pDC->SelectObject(&brush);
				pDC->Rectangle(rcIcon);
				rcBox.UnionRect(rcBox, rcIcon);
				brush.DeleteObject();
				rcIcon.OffsetRect(rcIcon.Width(), 0);
			}
			for (int j = 0; j < pPageSourceHeader->m_spotColors.GetSize(); j++)
			{
				COLORREF rcColor = pDoc->m_ColorDefinitionTable.FindColorRef(pPageSourceHeader->m_spotColors[j]);
				if (rcColor == WHITE)
				{
					CPageSource* pPageSource = GetObjectSource(m_ColorSeparations[i].m_nColorDefinitionIndex);
					if (pPageSource)
						rcColor = pPageSource->m_spotcolorDefList.GetAlternateRGB(pPageSourceHeader->m_spotColors[j]);
				}
				brush.CreateSolidBrush(rcColor);
				pDC->SelectObject(&brush);
				pDC->Rectangle(rcIcon);
				rcBox.UnionRect(rcBox, rcIcon);
				brush.DeleteObject();
				rcIcon.OffsetRect(rcIcon.Width(), 0);
			}
		}
		else
		{
			brush.CreateSolidBrush(rColorDef.m_rgb);
			pDC->SelectObject(&brush);
			pDC->Rectangle(rcIcon);
			rcBox.UnionRect(rcBox, rcIcon);
			brush.DeleteObject();
			rcIcon.OffsetRect(rcIcon.Width(), 0);
		}
	}

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();

	return rcBox;
}

BOOL CPageTemplate::HasColorant(const CString& strColorName)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		if ( (m_ColorSeparations[i].m_nColorDefinitionIndex < 0) || (m_ColorSeparations[i].m_nColorDefinitionIndex >= pDoc->m_ColorDefinitionTable.GetSize()) )
			continue;
		CColorDefinition& rColorDef = pDoc->m_ColorDefinitionTable[m_ColorSeparations[i].m_nColorDefinitionIndex];
		if (rColorDef.m_strColorName.CompareNoCase(_T("Composite")) == 0)
		{
			CPageSourceHeader* pPageSourceHeader = GetObjectSourceHeader(m_ColorSeparations[i].m_nColorDefinitionIndex);
			for (int j = 0; j < pPageSourceHeader->m_processColors.GetSize(); j++)
			{
				if (pPageSourceHeader->m_processColors[j].CompareNoCase(strColorName) == 0)
					return TRUE;
			}
			for (int j = 0; j < pPageSourceHeader->m_spotColors.GetSize(); j++)
			{
				if (pPageSourceHeader->m_spotColors[j].CompareNoCase(strColorName) == 0)
					return TRUE;
			}
		}
		else
		{
			if (rColorDef.m_strColorName.CompareNoCase(strColorName) == 0)
				return TRUE;
		}
	}

	return FALSE;
}

CFoldSheet*	CPageTemplate::GetFoldSheet() 
{ 
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	//if (m_pFoldSheet)
	//	return m_pFoldSheet;

	int nPageNum = pDoc->m_PageTemplateList.GetIndex(this, -m_nProductIndex - 1) + 1;

	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if ( ! rFoldSheet.IsPartOfBoundProduct(m_nProductPartIndex))
			continue;

		CArray <short*, short*> PageSequence;
		rFoldSheet.GetPageSequence(PageSequence, -1);
		for (int i = 0; i < PageSequence.GetSize(); i++)
		{
			if (*PageSequence[i] == nPageNum)
			{
				m_pFoldSheet = &rFoldSheet;
				return m_pFoldSheet;
			}
		}
	}

	return NULL;
};


#define MARKCOLOR				 RGB(245,187,0) //RGB(180,180,0)
#define MARKCOLOR_HIGHLIGHT		 RGB(245,187,0) //RGB(180,180,0)
#define MARKFRAMECOLOR_HIGHLIGHT BORDEAUX
#define MIN_MARKSIZE 3
#define MINSIZE_MARK(markRect)\
{\
	CRect testRect = markRect;\
	pDC->LPtoDP(testRect);\
	if (testRect.Height() * testRect.Width() < MIN_MARKSIZE * MIN_MARKSIZE)\
		testRect.InflateRect(MIN_MARKSIZE, MIN_MARKSIZE);\
	pDC->DPtoLP(testRect);\
	markRect = testRect;\
}


void CPageTemplate::DrawCropMarks(CDC* pDC, CCropMark* pMark, CRect rcPage)
{
	if ( ! pMark)
		return;

	long lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop;

	if (GetCropMarkTrimBox(rcPage, pMark, LEFT_TOP,		HORIZONTAL,  lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop))
		DrawCropMarkPreview(pDC, lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop);

	if (GetCropMarkTrimBox(rcPage, pMark, LEFT_TOP,		VERTICAL,    lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop))
		DrawCropMarkPreview(pDC, lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop);

	if (GetCropMarkTrimBox(rcPage, pMark, RIGHT_TOP,	HORIZONTAL,  lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop))
		DrawCropMarkPreview(pDC, lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop);

	if (GetCropMarkTrimBox(rcPage, pMark, RIGHT_TOP,	VERTICAL,	 lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop))
		DrawCropMarkPreview(pDC, lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop);

	if (GetCropMarkTrimBox(rcPage, pMark, RIGHT_BOTTOM,	HORIZONTAL,  lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop))
		DrawCropMarkPreview(pDC, lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop);

	if (GetCropMarkTrimBox(rcPage, pMark, RIGHT_BOTTOM,	VERTICAL,	 lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop))
		DrawCropMarkPreview(pDC, lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop);

	if (GetCropMarkTrimBox(rcPage, pMark, LEFT_BOTTOM,	HORIZONTAL,  lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop))
		DrawCropMarkPreview(pDC, lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop);

	if (GetCropMarkTrimBox(rcPage, pMark, LEFT_BOTTOM,	VERTICAL,	 lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop))
		DrawCropMarkPreview(pDC, lTrimBoxLeft, lTrimBoxBottom, lTrimBoxRight, lTrimBoxTop);
}

void* CPageTemplate::GetCropMarkTrimBox(CRect rcPage, CCropMark* pMark, LPARAM lParam1, LPARAM lParam2, long& lLeft, long& lBottom, long& lRight, long& lTop)
{
	if ( ! pMark)
		return NULL;

	long lPageDistance = CPrintSheetView::WorldToLP(pMark->m_fPageDistance);
	long lLength	   = CPrintSheetView::WorldToLP(pMark->m_fLength);
	long lLineWidth	   = CPrintSheetView::WorldToLP(pMark->m_fLineWidth);

	if (lParam2 == HORIZONTAL)
	{
		switch (lParam1)
		{
		case LEFT_TOP:		lRight = rcPage.left   - lPageDistance;		lLeft   = lRight - lLength;	
							lTop   = rcPage.top    + lLineWidth / 2;	lBottom = lTop	 - lLineWidth; 
							break;

		case RIGHT_TOP:		lLeft  = rcPage.right  + lPageDistance;		lRight  = lLeft  + lLength;	
							lTop   = rcPage.top    + lLineWidth / 2;	lBottom = lTop	 - lLineWidth; 
							break;

		case RIGHT_BOTTOM:	lLeft  = rcPage.right  + lPageDistance;		lRight  = lLeft  + lLength;	
							lTop   = rcPage.bottom + lLineWidth / 2;	lBottom = lTop	 - lLineWidth; 
							break;

		case LEFT_BOTTOM:	lRight = rcPage.left   - lPageDistance;		lLeft   = lRight - lLength;	
							lTop   = rcPage.bottom + lLineWidth / 2;	lBottom = lTop	 - lLineWidth; 
							break;
		}
	}
	else
	{
		switch (lParam1)
		{
		case LEFT_TOP:		lLeft   = rcPage.left   - lLineWidth / 2;	lRight  = lLeft		+ lLineWidth;	
							lBottom = rcPage.top    + lPageDistance;	lTop	= lBottom	+ lLength;	
							break;

		case RIGHT_TOP:		lLeft   = rcPage.right  - lLineWidth / 2;	lRight  = lLeft		+ lLineWidth;	
							lBottom = rcPage.top    + lPageDistance;	lTop	= lBottom	+ lLength;	
							break;

		case RIGHT_BOTTOM:	lLeft   = rcPage.right  - lLineWidth / 2;	lRight  = lLeft		+ lLineWidth;	
							lTop    = rcPage.bottom - lPageDistance;	lBottom = lTop		- lLength;	
							break;

		case LEFT_BOTTOM:	lLeft   = rcPage.left   - lLineWidth / 2;	lRight  = lLeft		+ lLineWidth;	
							lTop    = rcPage.bottom - lPageDistance;	lBottom = lTop		- lLength;	
							break;
		}
	}

	return (void*)TRUE;
}

void CPageTemplate::DrawCropMarkPreview(CDC* pDC, long lTrimBoxLeft, long lTrimBoxBottom, long lTrimBoxRight, long lTrimBoxTop)
{
	CRect markRect;
	markRect.left	= lTrimBoxLeft;
	markRect.bottom	= lTrimBoxBottom;
	markRect.right	= lTrimBoxRight;
	markRect.top	= lTrimBoxTop;

	BOOL bShowHighlighted = TRUE;
	BOOL bShowContent	  = TRUE;

	CPen	framePen(PS_SOLID, 1, RED);//(bShowHighlighted) ? MARKFRAMECOLOR_HIGHLIGHT : MARKCOLOR);	// Lightred
	CBrush  brush(BLACK);
	CPen*	pOldPen	  = pDC->SelectObject(&framePen);
	CBrush* pOldBrush = pDC->SelectObject(&brush);
	//CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

	if (bShowContent)
	{
		//pDC->FillSolidRect(markRect, RED);
		CPrintSheetView::DrawRectangleExt(pDC, markRect);
	}
	else
	{
		MINSIZE_MARK(markRect);
		CPrintSheetView::FillTransparent (pDC, markRect, (bShowHighlighted) ? MARKCOLOR_HIGHLIGHT : MARKCOLOR);
		CPrintSheetView::DrawRectangleExt(pDC, markRect);
	}

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	framePen.DeleteObject();
	brush.DeleteObject();
}	

BOOL CPageTemplate::IsPage()
{
	if (IsFlatProduct())
		return FALSE;
	else
		if ((m_nPageType == SinglePage) || (m_nPageType == MultipageMember)) 
			return TRUE;
		else
			return FALSE;
}

BOOL CPageTemplate::IsFlatProduct()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if (pDoc->GetFlatProductsIndex() == m_nProductPartIndex)
		return TRUE;
	else
		return FALSE;
}

BOOL CPageTemplate::IsFlatSingleSidedBackSide()
{
	if ( ! IsFlatProduct())
		return FALSE;
	if (! m_bLabelBackSide)
		return FALSE;

	CFlatProduct& rFlatProduct = GetFlatProduct();
	if (rFlatProduct.m_nNumPages > 1)
		return FALSE;
	
	return TRUE;
}

BOOL CPageTemplate::FlatProductHasBackSide()
{
	CFlatProduct& rFlatProduct = GetFlatProduct();
	return (rFlatProduct.m_nNumPages > 1) ? TRUE : FALSE;
}

BOOL CPageTemplate::PageNumHasAlpha()
{
	if (IsFlatProduct())
		return TRUE;

	for (int i = 0; i < m_strPageID.GetLength(); i++)
	{
		if ( ! isdigit(m_strPageID[i]))
			return TRUE;
	}

	return FALSE;
}

int	CPageTemplate::GetPageNum()
{
	if (PageNumHasAlpha())
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return 0;
		else
			return pDoc->m_PageTemplateList.GetIndex(this) + 1;
	}
	else
		return (int)Ascii2Float(m_strPageID);
}

float CPageTemplate::CalcShinglingScaleX()
{
	if ((m_nPageType != SinglePage) && (m_nPageType != MultipageMember)) // is control mark
		return 1.0f;

	CBoundProductPart& rProductPart = GetProductPart();
	if ( ! rProductPart.IsNull())
	{
		if ( ! rProductPart.m_foldingParams.m_bShinglingActive)
			return 1.0f;

		int nMethod = rProductPart.m_foldingParams.m_nShinglingMethod;
		if ( (nMethod == CBookblock::ShinglingMethodScale) || (nMethod == CBookblock::ShinglingMethodBottlingScale) )
		{
			float			fScale  = 1.0f;
			float			fOffset = (nMethod == CBookblock::ShinglingMethodScale) ? fabs(m_fShinglingOffsetX) : max(fabs(m_fShinglingOffsetX), fabs(m_fShinglingOffsetXFoot));
			CLayoutObject*	pObject = GetLayoutObject();
			if (pObject)
			{
				switch (pObject->m_nHeadPosition)
				{
				case TOP:		
				case BOTTOM:	fScale = (pObject->GetWidth()  - fOffset) / pObject->GetWidth();	break;
				case LEFT:		
				case RIGHT:		fScale = (pObject->GetHeight() - fOffset) / pObject->GetHeight();	break;
				}
			}
			return fScale;
		}
	}

	return 1.0f;
}

float CPageTemplate::CalcShinglingScaleY()
{
	if ((m_nPageType != SinglePage) && (m_nPageType != MultipageMember)) // is control mark
		return 1.0f;

	CBoundProductPart& rProductPart = GetProductPart();
	if ( ! rProductPart.IsNull())
	{
		if ( ! rProductPart.m_foldingParams.m_bShinglingActive)
			return 1.0f;
		if ( (rProductPart.m_foldingParams.m_nShinglingDirection != CBookblock::ShinglingDirectionSpineHead) && (rProductPart.m_foldingParams.m_nShinglingDirection != CBookblock::ShinglingDirectionSpineFoot) )
			return 1.0f;

		int nMethod = rProductPart.m_foldingParams.m_nShinglingMethod;
		if ( (nMethod == CBookblock::ShinglingMethodScale) || (nMethod == CBookblock::ShinglingMethodBottlingScale) )
		{
			float			fScale  = 1.0f;
			float			fOffset = fabs(m_fShinglingOffsetY);
			CLayoutObject*	pObject = GetLayoutObject();
			if (pObject)
			{
				switch (pObject->m_nHeadPosition)
				{
				case LEFT:		
				case RIGHT:	 fScale = (pObject->GetWidth()  - fOffset) / pObject->GetWidth();	break;
				case TOP:		
				case BOTTOM: fScale = (pObject->GetHeight() - fOffset) / pObject->GetHeight();	break;
				}
			}
			return fScale;
		}
	}

	return 1.0f;
}

BOOL CPageTemplate::ColorInUse(CImpManDoc* pDoc, short nColorDefinitionIndex, BOOL bSeparationOnly, BOOL bCompositeAlso)
{
	CString	 strColor;
	int		 nColorDefIndexComposite = pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(_T("Composite"));

	if (pDoc->m_PageTemplateList.IsUnpopulated() && (nColorDefinitionIndex != nColorDefIndexComposite) )
	{
		CBoundProductPart& rProductPart = GetProductPart();
		CString strColor = theApp.MapPrintColor(pDoc->m_ColorDefinitionTable.GetColorName(nColorDefinitionIndex));
		if (rProductPart.m_colorantList.FindColorant(strColor))
			return TRUE;
	}

	for (int i = 0; i < m_ColorSeparations.GetSize(); i++)
	{
		if (m_ColorSeparations[i].m_nColorDefinitionIndex == nColorDefIndexComposite)		// if composite -> seek spotcolor list
		{
			if (bCompositeAlso)		// composite counts also 
				return TRUE;
			if (bSeparationOnly)
				continue;
			if (nColorDefinitionIndex == nColorDefIndexComposite)	// nColorDefinitionIndex represents Composite (Process)
				return TRUE;

			CPageSourceHeader* pObjectSourceHeader = GetObjectSourceHeader(nColorDefIndexComposite, FALSE, NULL, 4, pDoc);
			if (pObjectSourceHeader)
			{
				if ( (nColorDefinitionIndex >= 0) && (nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
					strColor = pDoc->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName;
				for (int j = 0; j < pObjectSourceHeader->m_spotColors.GetSize(); j++)
				{
					if (pObjectSourceHeader->m_spotColors[j] == strColor)
						return TRUE;
				}
			}
		}
		else
		{
			if (m_bMap2SpotColors)
			{
				strColor = pDoc->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName;
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColor);
				if (pColorDef)
				{
					if (pColorDef->m_nColorType == CColorDefinition::Spot)
						return TRUE;
				}
				else
					return TRUE;	// no colordef -> spotcolor
			}

			if (m_ColorSeparations[i].m_nColorDefinitionIndex == nColorDefinitionIndex)
				return TRUE;
		}
	}
	return FALSE;
}

BOOL CPageTemplate::IsFirstLastOrInnerPage()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;	

	if (IsFirstOrLastPage())
		return TRUE;
	else
		if (IsInnerPage())
			return TRUE;

	return FALSE;
}

BOOL CPageTemplate::IsFirstOrLastPage()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;	

	//if (this == &pDoc->m_PageTemplateList.GetHead(-m_nProductIndex - 1))
	//	return TRUE;
	//else
	//	if (this == &pDoc->m_PageTemplateList.GetTail(-m_nProductIndex - 1))
	//		return TRUE;

	short nPageNum = pDoc->m_PageTemplateList.GetPageIndex(this) + 1;
	if (nPageNum < 0)
		return FALSE;


	POSITION pos = pDoc->m_Bookblock.GetHeadPosition_DescriptionList(m_nProductIndex);
	while (pos)
	{
		CDescriptionItem& rItem	= pDoc->m_Bookblock.m_DescriptionList.GetAt(pos);
		if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		{
			CFoldSheet&	rFoldSheet = rItem.GetFoldSheet(&pDoc->m_Bookblock);
			if (rFoldSheet.GetNumPages() > 0)
			{
				CArray <short*, short*> LeftPageSequence;
				rFoldSheet.GetPageSequence(LeftPageSequence, CDescriptionItem::FoldSheetLeftPart);
				rFoldSheet.ReducePageSequence(LeftPageSequence);
				if (LeftPageSequence.GetSize() > 0)
				{
					if (*LeftPageSequence[0] == nPageNum)
						return TRUE;
				}
				break;
			}
		}
		pDoc->m_Bookblock.GetNext_DescriptionList(m_nProductIndex, pos);
	}

	pos = pDoc->m_Bookblock.GetTailPosition_DescriptionList(m_nProductIndex);
	while (pos)
	{
		CDescriptionItem& rItem	= pDoc->m_Bookblock.m_DescriptionList.GetAt(pos);
		if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
		{
			CFoldSheet&	rFoldSheet = rItem.GetFoldSheet(&pDoc->m_Bookblock);
			if (rFoldSheet.GetNumPages() > 0)
			{
				CArray <short*, short*> RightPageSequence;
				rFoldSheet.GetPageSequence(RightPageSequence, CDescriptionItem::FoldSheetRightPart);
				rFoldSheet.ReducePageSequence(RightPageSequence);
				if (RightPageSequence.GetSize() > 0)
				{
					if (*RightPageSequence[RightPageSequence.GetSize() - 1] == nPageNum)
							return TRUE;
				}
				break;
			}
		}
		pDoc->m_Bookblock.GetPrev_DescriptionList(m_nProductIndex, pos);
	}

	return FALSE;
}

BOOL CPageTemplate::IsInnerPage()
{
	CFoldSheet* pFoldSheet = GetFoldSheet();
	if ( ! pFoldSheet)
		return FALSE;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;	
	short nPageNum = pDoc->m_PageTemplateList.GetPageIndex(this) + 1;
	if (nPageNum < 0)
		return FALSE;

	CArray <short*, short*> LeftPageSequence;
	CArray <short*, short*> RightPageSequence;
	pFoldSheet->GetPageSequence(LeftPageSequence, CDescriptionItem::FoldSheetLeftPart);
	pFoldSheet->ReducePageSequence(LeftPageSequence);
	pFoldSheet->GetPageSequence(RightPageSequence, CDescriptionItem::FoldSheetRightPart);
	pFoldSheet->ReducePageSequence(RightPageSequence);
	if ( (LeftPageSequence.GetSize() <= 0) || (RightPageSequence.GetSize() <= 0) )
		return FALSE;

	if (*LeftPageSequence[LeftPageSequence.GetSize() - 1] == nPageNum)
	{
		if (*RightPageSequence[0] == nPageNum + 1)
			return TRUE;
	}

	if (*RightPageSequence[0] == nPageNum)
	{
		if (*LeftPageSequence[LeftPageSequence.GetSize() - 1] == nPageNum - 1)
			return TRUE;
	}

	return FALSE;
}

CRect CPageTemplate::DrawSheetPreview(CDC* pDC, CRect rcFrame, CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet, BOOL bShowContent)
{
	if ( ! pLayoutObject)
		return CRect(0,0,0,0);
	if (IsFlatProduct())
		return CRect(0,0,0,0);

	pDC->FillSolidRect(rcFrame, WHITE);	//background

	int nHeadPosition = pLayoutObject->m_nHeadPosition;

	float fWidth, fHeight, fNetWidth, fNetHeight; 
	if ( (nHeadPosition == TOP) || (nHeadPosition == BOTTOM) )
		pLayoutObject->GetCurrentPageCutSize(pPrintSheet, fWidth, fHeight);
	else
		pLayoutObject->GetCurrentPageCutSize(pPrintSheet, fHeight, fWidth);
	fNetWidth  = pLayoutObject->GetWidth();
	fNetHeight = pLayoutObject->GetHeight();

	int		nExtentX	= CPrintSheetView::WorldToLP(fWidth);
	int		nExtentY	= CPrintSheetView::WorldToLP(fHeight);
	int		nXOrg		= 0;
	int		nYOrg		= 0;
	CRect	rcViewPort	= rcFrame;
	rcViewPort.DeflateRect(1, 1);

	pDC->SaveDC();
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(nExtentX, nExtentY);
	pDC->SetWindowOrg(nXOrg,	nExtentY + nYOrg);
	pDC->SetViewportExt(rcViewPort.Width(), -rcViewPort.Height());
	pDC->SetViewportOrg(rcViewPort.left, rcViewPort.top);

	CRect rcPlaceArea(0, 0, (long)(fWidth * 1000.0f), (long)(fHeight * 1000.0f));

	pLayoutObject->Draw(pDC, TRUE, FALSE, FALSE, pPrintSheet);
	if (bShowContent)
		pLayoutObject->DrawPreview(pDC, pPrintSheet, -1);

	CRect rcBox = CRect(CPoint(nXOrg, nYOrg), CSize(nExtentX, nExtentY));
	pDC->LPtoDP(&rcBox);
	rcBox.NormalizeRect();

	pDC->RestoreDC(-1);

	return rcBox;
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////




////////////////////////////// CMarkTemplateList ////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMarkTemplateList

IMPLEMENT_SERIAL(CMarkTemplateList, CObject, VERSIONABLE_SCHEMA | 1)

CMarkTemplateList::CMarkTemplateList()
{
}



/////////////////////////////////////////////////////////////////////////////
// CMarkTemplate

IMPLEMENT_SERIAL(CMarkTemplate, CObject, VERSIONABLE_SCHEMA | 1)

CMarkTemplate::CMarkTemplate()
{
}


CMarkTemplate::CMarkTemplate(const CMarkTemplate& rMarkTemplate) 
{
	Copy(rMarkTemplate);
}


// operator overload for CMarkTemplate - needed by CList functions:

const CMarkTemplate& CMarkTemplate::operator=(const CMarkTemplate& rMarkTemplate)
{
	Copy(rMarkTemplate);	

	return *this;
}


void CMarkTemplate::Copy(const CMarkTemplate& rMarkTemplate)
{
	m_strMarkID             = rMarkTemplate.m_strMarkID;
//	m_strContentFile        = rMarkTemplate.m_strContentFile;
	m_nMarkType             = rMarkTemplate.m_nMarkType;
//	m_nLayoutObjectIndex    = rMarkTemplate.m_nLayoutObjectIndex; 
	m_fScaleX				= rMarkTemplate.m_fScaleX;
	m_fScaleY				= rMarkTemplate.m_fScaleY;

	m_ColorSeparations.RemoveAll();
	for (int i = 0; i < rMarkTemplate.m_ColorSeparations.GetSize(); i++)
		m_ColorSeparations.Add(rMarkTemplate.m_ColorSeparations[i]);

	CPrintSheetLocation printsheetLoc;
	m_PrintSheetLocations.RemoveAll();
	for (int i = 0; i < rMarkTemplate.m_PrintSheetLocations.GetSize(); i++)
	{
		printsheetLoc = rMarkTemplate.m_PrintSheetLocations[i];
		m_PrintSheetLocations.Add(printsheetLoc);	
	}
}

// helper function for MarkTemplateList elements
template <> void AFXAPI SerializeElements <CMarkTemplate> (CArchive& ar, CMarkTemplate* pMarkTemplate, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CMarkTemplate));

	if (ar.IsStoring())
	{
		ar << pMarkTemplate->m_strMarkID;
//		ar << pMarkTemplate->m_strContentFile;
		ar << pMarkTemplate->m_nMarkType;
//		ar << pMarkTemplate->m_nLayoutObjectIndex; 
		ar << pMarkTemplate->m_fScaleX;
		ar << pMarkTemplate->m_fScaleY;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> pMarkTemplate->m_strMarkID;
			//		ar >> pMarkTemplate->m_strContentFile;
			ar >> pMarkTemplate->m_nMarkType;
			//		ar >> pMarkTemplate->m_nLayoutObjectIndex; 
			ar >> pMarkTemplate->m_fScaleX;
			ar >> pMarkTemplate->m_fScaleY;
			pMarkTemplate->m_ColorSeparations.RemoveAll();  // no append, so target list has to be empty 
			pMarkTemplate->m_PrintSheetLocations.RemoveAll();
			break;
		}
	}

	pMarkTemplate->m_ColorSeparations.Serialize(ar);
	pMarkTemplate->m_PrintSheetLocations.Serialize(ar);
}

void AFXAPI DestructElements(CMarkTemplate* pMarkTemplate, INT_PTR nCount)
{
	ASSERT(nCount == 1); // in CList context nCount has to be 1

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pMarkTemplate->m_strMarkID.Empty();
//	pMarkTemplate->m_strContentFile.Empty();
	pMarkTemplate->m_ColorSeparations.RemoveAll();
	pMarkTemplate->m_PrintSheetLocations.RemoveAll();
}


CLayoutObject* CMarkTemplate::GetLayoutObject(int /*nLayoutIndex*/, int /*nLayoutSide*/)	// get corresponding layout object
{
	return NULL;
}

void CMarkTemplate::Create(CString& strID, unsigned char nMarkType, int /*nObjectIndex*/, CString& /*strContentFile*/)
{
	m_strMarkID				= strID;
//	m_strContentFile		= strContentFile;
	m_nMarkType				= nMarkType;
	m_fScaleX				= 1.0f;
	m_fScaleY				= 1.0f;
//	m_nLayoutObjectIndex	= nObjectIndex;
	m_ColorSeparations.RemoveAll();
	m_PrintSheetLocations.RemoveAll();
}

/////////////////////////////////////////////////////////////////////////////
