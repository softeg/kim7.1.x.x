// PrintSheetViewDragDrop.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgLayoutObjectControl.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PrintSheetTableView.h"
#include "PrintSheetMarksView.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "DlgMarkSet.h"
#include "ProdDataPrintView.h"
#include "PrintSheetNavigationView.h"
#include "PrintSheetViewStrippingData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



DROPEFFECT CPrintSheetView::OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	DROPEFFECT de = ((dwKeyState & MK_CONTROL) == MK_CONTROL) ? DROPEFFECT_COPY : DROPEFFECT_MOVE;

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
	{
		sourceDI.m_nState = CDisplayItem::Normal;
		m_DisplayList.DeselectItem(sourceDI);
		m_DisplayList.InvalidateItem(&sourceDI);
		UpdateWindow();

		CClientDC dc(this);
		m_dragDropObject.DragInitialize(&dc, sourceDI.m_AuxAuxRect, 220, DrawFeedback, 6);

		if (de == DROPEFFECT_MOVE)
		{
			int			 nLayerRefIndex = (int)sourceDI.m_pData;
			CPrintSheet* pPrintSheet	= (CPrintSheet*)sourceDI.m_pAuxData;
			CLayout*	 pLayout		= (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			if (pLayout)
			{
				pLayout->m_FrontSide.SetFoldSheetLayerVisible(nLayerRefIndex, FALSE);
				pLayout->m_BackSide.SetFoldSheetLayerVisible(nLayerRefIndex, FALSE);
				m_DisplayList.Invalidate();
				CRect rcInvalidate = sourceDI.m_AuxAuxRect;
				rcInvalidate.InflateRect(2,2);
				InvalidateRect(rcInvalidate);
				UpdateWindow();
				//// remove hidden status in OnDragLeave / OnDrop
			}
		}

		m_dragDropObject.DragEnter(&dc);

		m_ptMouse = point;
	}

	return de;
}


DROPEFFECT CPrintSheetView::OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, RefEdgePrintSheet)))
		return OnDragOverRefEdgePrintSheet(sourceDI, dwKeyState, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
		return OnDragOverLayoutObject(sourceDI, dwKeyState, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		return OnDragOverFoldSheet(sourceDI, dwKeyState, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia)))
		return OnDragOverOutputMedia(sourceDI, dwKeyState, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark)))
		return OnDragOverMark(sourceDI, dwKeyState, point);

	return DROPEFFECT_NONE;
}

DROPEFFECT CPrintSheetView::OnDragOverRefEdgePrintSheet(CDisplayItem& /*di*/, DWORD /*dwKeyState*/, CPoint point)
{
	DROPEFFECT de = DROPEFFECT_MOVE;

	m_DisplayList.GiveDragFeedback(de, point);

	return de;
}

DROPEFFECT CPrintSheetView::OnDragOverLayoutObject(CDisplayItem& di, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT de;
	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		de = DROPEFFECT_MOVE;

	// TODO: copying a page does change the foldsheet structure
	//		 so the question is if we have to create a new foldsheet ???
	//		 Actually we do not allow to copy a page!
	if ( ( ! ((CLayoutObject*)di.m_pData)->IsLabel() ) && (de == DROPEFFECT_COPY) && (((CLayoutObject*)di.m_pData)->m_nType == CLayoutObject::Page) )
		de = DROPEFFECT_MOVE;
	else
	{
		// Check if mouse is inside unallowed objects
		CDisplayItem insideDI = m_DisplayList.GetItemPtInside(point, FALSE); 
		if (!insideDI.IsNull())
		{
			if (insideDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
				if (((CLayoutObject*)insideDI.m_pData)->GetLayoutSide() != ((CLayoutObject*)di.m_pData)->GetLayoutSide())
					de = DROPEFFECT_NONE;
				else
					if (insideDI.m_pAuxData != di.m_pAuxData)	// Different print sheets
						de = DROPEFFECT_NONE;

			if ((insideDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet))) ||
				(insideDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate))))
				if ((CLayoutSide*)insideDI.m_pData != ((CLayoutObject*)di.m_pData)->GetLayoutSide())
					de = DROPEFFECT_NONE;
				else
					if (insideDI.m_pAuxData != di.m_pAuxData)	// Different print sheets
						de = DROPEFFECT_NONE;

			if (insideDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, RefEdgePrintSheet)))
				if ((CLayoutSide*)insideDI.m_pData != ((CLayoutObject*)di.m_pData)->GetLayoutSide())
					de = DROPEFFECT_NONE;
				else
					if (insideDI.m_pAuxData != di.m_pAuxData)	// Different print sheets
						de = DROPEFFECT_NONE;
		}
	}

	m_DisplayList.GiveDragFeedback(de, point);

	return de;
}

DROPEFFECT CPrintSheetView::OnDragOverFoldSheet(CDisplayItem& di, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT de;
	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		de = DROPEFFECT_MOVE;

	CRect		 rcNewObjectFrame = m_dragDropObject.GetCurrentObjectFrame() + (point - m_ptMouse);
	CRect		 rcNewFeedbackFrame(0,0,0,0);

	CClientDC dc(this);
	CFoldSheetDropTargetInfo dropTargetInfo = FindFoldSheetDropTarget(rcNewObjectFrame, rcNewFeedbackFrame, di);

	CPrintSheet* pSourcePrintSheet = (CPrintSheet*)di.m_pAuxData;
	CPrintSheet* pTargetPrintSheet = dropTargetInfo.m_pTargetPrintSheet;
	if (pSourcePrintSheet == pTargetPrintSheet)
	{
		int nSourceSide = (di.m_AuxRect.Height() <= 2) ? BACKSIDE : FRONTSIDE;
		int nTargetSide = (dropTargetInfo.m_pTargetLayoutSide) ? dropTargetInfo.m_pTargetLayoutSide->m_nLayoutSide : -1;
		if (nTargetSide != -1)
			if ( (nSourceSide != nTargetSide) && (de == DROPEFFECT_MOVE) )
				de = DROPEFFECT_COPY;
	}

	m_dragDropObject.DragOver(&dc, rcNewObjectFrame, rcNewFeedbackFrame, &dropTargetInfo);

	if (dropTargetInfo.m_nTargetType == -1)
		de = DROPEFFECT_NONE;

	m_ptMouse = point;
 
	return de;
}

CFoldSheetDropTargetInfo CPrintSheetView::FindFoldSheetDropTarget(CRect rcNewObjectFrame, CRect &rcNewFeedbackFrame, CDisplayItem& sourceDI)
{
	int						 nDockingDirection = -1;
	CFoldSheetDropTargetInfo dtInfo;

	CDisplayItem closestDI = m_DisplayList.GetClosestItemEdge(rcNewObjectFrame, &nDockingDirection, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), TRUE,	// approach from outside
																									DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet),		FALSE,	// approach from inside
																									DISPLAY_ITEM_TYPEID(CPrintSheetView, Gripper),		TRUE,	// approach from outside
																									30);
	if (closestDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
		dtInfo = FindFoldSheetDropTargetLayoutObject(rcNewObjectFrame, rcNewFeedbackFrame, sourceDI, closestDI, nDockingDirection);
	else
		if (closestDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
			dtInfo = FindFoldSheetDropTargetSheet(rcNewObjectFrame, rcNewFeedbackFrame, sourceDI, closestDI, nDockingDirection);
		else
			if (closestDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Gripper)))
				dtInfo = FindFoldSheetDropTargetSheet(rcNewObjectFrame, rcNewFeedbackFrame, sourceDI, closestDI, nDockingDirection);

	if (dtInfo.m_nTargetType == -1)
		dtInfo = FindFoldSheetDropTargetNoDocking(rcNewObjectFrame, rcNewFeedbackFrame, sourceDI);

	return dtInfo;
}

CFoldSheetDropTargetInfo CPrintSheetView::FindFoldSheetDropTargetNoDocking(CRect rcNewObjectFrame, CRect &rcNewFeedbackFrame, CDisplayItem& sourceDI)
{
	CFoldSheetDropTargetInfo dtInfo;

	CDisplayItem sheetDI = m_DisplayList.GetItemPtInside(rcNewObjectFrame.CenterPoint(), DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
	if (sheetDI.IsNull())
		return dtInfo;

	CPrintSheet*	pTargetPrintSheet	= (CPrintSheet*)sheetDI.m_pAuxData;
	CLayoutSide*	pTargetLayoutSide	= (CLayoutSide*)sheetDI.m_pData;
	if ( ! pTargetLayoutSide)
		return dtInfo;
	CPrintSheet*	pSourcePrintSheet	= (CPrintSheet*)sourceDI.m_pAuxData;
	CLayoutSide*	pSourceLayoutSide	= (pSourcePrintSheet) ? &pSourcePrintSheet->GetLayout()->m_FrontSide : NULL;
	if ( ! pSourceLayoutSide)
		return dtInfo;

	float fSourceLeft, fSourceBottom, fSourceRight, fSourceTop, fSourceWidth, fSourceHeight;
	pSourceLayoutSide->CalcBoundingRectObjects(&fSourceLeft, &fSourceBottom, &fSourceRight, &fSourceTop, &fSourceWidth, &fSourceHeight, FALSE, (int)sourceDI.m_pData, FALSE, pSourcePrintSheet);

	float fRatioX		= pTargetLayoutSide->m_fPaperWidth  / (float)sheetDI.m_BoundingRect.Width();
	float fRatioY		= pTargetLayoutSide->m_fPaperHeight / (float)sheetDI.m_BoundingRect.Height();
	long  nTargetLeft	= rcNewObjectFrame.left			- sheetDI.m_BoundingRect.left;
	long  nTargetBottom	= sheetDI.m_BoundingRect.bottom - rcNewObjectFrame.bottom;
	float fTargetLeft	= pTargetLayoutSide->m_fPaperLeft	+ (float)nTargetLeft   * fRatioX;
	float fTargetBottom = pTargetLayoutSide->m_fPaperBottom + (float)nTargetBottom * fRatioY;

	dtInfo.m_nTargetType		 	= CFoldSheetDropTargetInfo::DropTargetFoldSheet;
	dtInfo.m_pTargetPrintSheet	 	= pTargetPrintSheet;
	dtInfo.m_pTargetLayoutSide	 	= pTargetLayoutSide;
	dtInfo.m_fTargetLeft			= fTargetLeft;
	dtInfo.m_fTargetBottom			= fTargetBottom;
	dtInfo.m_fTargetRight			= fTargetLeft	+ fSourceWidth;
	dtInfo.m_fTargetTop				= fTargetBottom + fSourceHeight;

	return dtInfo;
}

CFoldSheetDropTargetInfo CPrintSheetView::FindFoldSheetDropTargetLayoutObject(CRect rcNewObjectFrame, CRect &rcNewFeedbackFrame, CDisplayItem& sourceDI, CDisplayItem& closestDI, int nDockingDirection)
{
	CFoldSheetDropTargetInfo dtInfo;

	CPrintSheet*	pTargetPrintSheet	= (CPrintSheet*)closestDI.m_pAuxData;
	CLayoutObject*	pTargetLayoutObject = (CLayoutObject*)closestDI.m_pData;
	CLayoutSide*	pTargetLayoutSide	= (pTargetLayoutObject) ? pTargetLayoutObject->GetLayoutSide() : NULL;
	if ( ! pTargetLayoutSide)
		return dtInfo;
	CPrintSheet*	pSourcePrintSheet	= (CPrintSheet*)sourceDI.m_pAuxData;
	CLayoutSide*	pSourceLayoutSide	= (pSourcePrintSheet) ? &pSourcePrintSheet->GetLayout()->m_FrontSide : NULL;
	if ( ! pSourceLayoutSide)
		return dtInfo;

	float fSourceLeft, fSourceBottom, fSourceRight, fSourceTop, fSourceWidth, fSourceHeight;
	pSourceLayoutSide->CalcBoundingRectObjects(&fSourceLeft, &fSourceBottom, &fSourceRight, &fSourceTop, &fSourceWidth, &fSourceHeight, FALSE, (int)sourceDI.m_pData, FALSE, pSourcePrintSheet);

	float fDockingLeft   = pTargetLayoutObject->GetLeft();
	float fDockingBottom = pTargetLayoutObject->GetBottom(); 
	float fDockingRight  = pTargetLayoutObject->GetRight();
	float fDockingTop	 = pTargetLayoutObject->GetTop();
	float fDockingWidth	 = fDockingRight - fDockingLeft, fDockingHeight = fDockingTop - fDockingBottom;
	CRect rcDockingFrame = closestDI.m_BoundingRect;

	if ( ! pTargetLayoutObject->IsLabel())
	{
		CDisplayItem* pDIFoldSheet = m_DisplayList.GetItemFromData((void*)pTargetLayoutObject->m_nLayerRefIndex, (void*)pTargetPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
		if (pDIFoldSheet)
		{
			pTargetLayoutSide->CalcBoundingRectObjects(&fDockingLeft, &fDockingBottom, &fDockingRight, &fDockingTop, &fDockingWidth, &fDockingHeight, 
												 FALSE, pTargetLayoutObject->m_nLayerRefIndex, FALSE, pTargetPrintSheet);
			rcDockingFrame = pDIFoldSheet->m_AuxAuxRect;
		}
	}

	int	nAlignment = -1;
	if ( (nDockingDirection == LEFT) || (nDockingDirection == RIGHT) )
		nAlignment = (abs(rcNewObjectFrame.top  - rcDockingFrame.top)  < abs(rcNewObjectFrame.bottom - rcDockingFrame.bottom)) ? TOP  : BOTTOM;
	else
		nAlignment = (abs(rcNewObjectFrame.left - rcDockingFrame.left) < abs(rcNewObjectFrame.right  - rcDockingFrame.right))  ? LEFT : RIGHT;

	CPoint ptTopLeft, ptBottomRight;
	float fTargetLeft, fTargetBottom, fTargetRight, fTargetTop;
	int  nTry     = 0;
	BOOL bRotated = FALSE;
	do
	{
		CSize  szFrame((bRotated) ? rcNewObjectFrame.Height() : rcNewObjectFrame.Width(), (bRotated) ? rcNewObjectFrame.Width()  : rcNewObjectFrame.Height());
		fSourceWidth  = (bRotated) ? fSourceTop - fSourceBottom : fSourceRight - fSourceLeft; 
		fSourceHeight = (bRotated) ? fSourceRight - fSourceLeft : fSourceTop - fSourceBottom;
		switch (nDockingDirection)
		{
		case LEFT:		ptTopLeft.x = rcDockingFrame.left - szFrame.cx;
						ptTopLeft.y = (nAlignment == TOP)  ? rcDockingFrame.top  : rcDockingFrame.bottom - szFrame.cy;	
						fTargetLeft = fDockingLeft - fSourceWidth;
						fTargetTop  = (nAlignment == TOP)  ? fDockingTop  : fDockingBottom + fSourceHeight;	
						break;
		case RIGHT:		ptTopLeft.x = rcDockingFrame.right;
						ptTopLeft.y = (nAlignment == TOP)  ? rcDockingFrame.top  : rcDockingFrame.bottom - szFrame.cy;	
						fTargetLeft = fDockingRight;
						fTargetTop  = (nAlignment == TOP)  ? fDockingTop  : fDockingBottom + fSourceHeight;	
						break;
		case TOP:		ptTopLeft.x = (nAlignment == LEFT) ? rcDockingFrame.left : rcDockingFrame.right  - szFrame.cx;
						ptTopLeft.y = rcDockingFrame.top - szFrame.cy;
						fTargetLeft = (nAlignment == LEFT) ? fDockingLeft : fDockingRight  - fSourceWidth;
						fTargetTop  = fDockingTop + fSourceHeight;
						break;
		case BOTTOM:	ptTopLeft.x = (nAlignment == LEFT) ? rcDockingFrame.left : rcDockingFrame.right  - szFrame.cx;
						ptTopLeft.y = rcDockingFrame.bottom;
						fTargetLeft = (nAlignment == LEFT) ? fDockingLeft : fDockingRight  - fSourceWidth;
						fTargetTop  = fDockingBottom;
						break;
		default:		return dtInfo;
		}

		ptBottomRight = ptTopLeft + szFrame;
		fTargetRight  = fTargetLeft + fSourceWidth;
		fTargetBottom = fTargetTop  - fSourceHeight;

		if (pTargetLayoutSide->IsInsideFreeArea(pTargetPrintSheet, fTargetLeft, fTargetBottom, fTargetRight, fTargetTop))
			break;
		
		switch (nTry)
		{
		case 0: bRotated = ! bRotated; break;	// change rotation
		case 1:	switch (nAlignment)				// change alignment
				{
				case LEFT:		nAlignment = RIGHT;  break;
				case RIGHT:		nAlignment = LEFT;   break;
				case BOTTOM:	nAlignment = TOP;    break;
				case TOP:		nAlignment = BOTTOM; break;
				}
				break;
		case 2: bRotated = ! bRotated; break;	// change rotation again
		case 3: return dtInfo;
		}
		nTry++;
	}while (1);

	rcNewFeedbackFrame = CRect(ptTopLeft, ptBottomRight);
	rcNewFeedbackFrame.NormalizeRect();

	dtInfo.m_nTargetType		 	= ( ! pTargetLayoutObject->IsLabel()) ? CFoldSheetDropTargetInfo::DropTargetFoldSheet : CFoldSheetDropTargetInfo::DropTargetLayoutObject;
	dtInfo.m_bDockingRotate			= bRotated;
	dtInfo.m_pTargetPrintSheet	 	= pTargetPrintSheet;
	dtInfo.m_pTargetLayoutSide	 	= pTargetLayoutSide;
	dtInfo.m_pTargetLayoutObject 	= pTargetLayoutObject;
	dtInfo.m_nTargetLayerRefIndex	= ( ! pTargetLayoutObject->IsLabel()) ? pTargetLayoutObject->m_nLayerRefIndex : -1;
	dtInfo.m_fTargetLeft			= fTargetLeft;
	dtInfo.m_fTargetBottom			= fTargetBottom;
	dtInfo.m_fTargetRight			= fTargetRight;
	dtInfo.m_fTargetTop				= fTargetTop;

	return dtInfo;
}

CFoldSheetDropTargetInfo CPrintSheetView::FindFoldSheetDropTargetSheet(CRect rcNewObjectFrame, CRect &rcNewFeedbackFrame, CDisplayItem& sourceDI, CDisplayItem& closestDI, int nDockingDirection)
{
	CFoldSheetDropTargetInfo dtInfo;

	CPrintSheet*	pTargetPrintSheet	= (CPrintSheet*)closestDI.m_pAuxData;
	CLayoutSide*	pTargetLayoutSide	= (CLayoutSide*)closestDI.m_pData;
	if ( ! pTargetLayoutSide)
		return dtInfo;
	CPrintSheet*	pSourcePrintSheet	= (CPrintSheet*)sourceDI.m_pAuxData;
	CLayoutSide*	pSourceLayoutSide	= (pSourcePrintSheet) ? &pSourcePrintSheet->GetLayout()->m_FrontSide : NULL;
	if ( ! pSourceLayoutSide)
		return dtInfo;

	CPressDevice*	pPressDevice	= pTargetLayoutSide->GetPressDevice();
	BOOL  			bSheetPress		= (pPressDevice) ? ( (pPressDevice->m_nPressType == CPressDevice::SheetPress) ? TRUE : FALSE) : TRUE;
	float			fBottomMargin	= 0.0f;

	float fSourceLeft, fSourceBottom, fSourceRight, fSourceTop, fSourceWidth, fSourceHeight;
	pSourceLayoutSide->CalcBoundingRectObjects(&fSourceLeft, &fSourceBottom, &fSourceRight, &fSourceTop, &fSourceWidth, &fSourceHeight, FALSE, (int)sourceDI.m_pData, FALSE, pSourcePrintSheet);
	if (bSheetPress)
	{
		float fNetBoxLeft, fNetBoxBottom, fNetBoxRight, fNetBoxTop, fNetBoxWidth, fNetBoxHeight;
		pSourceLayoutSide->CalcBoundingRectObjects(&fNetBoxLeft, &fNetBoxBottom, &fNetBoxRight, &fNetBoxTop, &fNetBoxWidth, &fNetBoxHeight, FALSE, (int)sourceDI.m_pData);
		fBottomMargin = fNetBoxBottom - fSourceBottom;
	}

	float 			fDockingLeft	= pTargetLayoutSide->m_fPaperLeft;
	float 			fDockingBottom	= (bSheetPress) ? pTargetLayoutSide->m_fPaperBottom + pPressDevice->m_fGripper - fBottomMargin : pTargetLayoutSide->m_fPaperBottom; 
	float 			fDockingRight	= pTargetLayoutSide->m_fPaperRight;
	float 			fDockingTop		= pTargetLayoutSide->m_fPaperTop;
	float 			fDockingWidth	= fDockingRight - fDockingLeft, fDockingHeight = fDockingTop - fDockingBottom;
	CRect 			rcDockingFrame	= closestDI.m_BoundingRect;
	if (closestDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Gripper)))
	{
		rcDockingFrame    = closestDI.m_AuxRect; rcDockingFrame.bottom -= (long)((fBottomMargin / m_DisplayList.m_fViewportRatioY) * m_DisplayList.m_pFnWorldToLP(1));
		nDockingDirection = BOTTOM;
	}

	int	nAlignment = -1;
	if ( (nDockingDirection == LEFT) || (nDockingDirection == RIGHT) )
		nAlignment = (abs(rcNewObjectFrame.top  - rcDockingFrame.top)  < abs(rcNewObjectFrame.bottom - rcDockingFrame.bottom)) ? TOP  : BOTTOM;
	else
		nAlignment = (abs(rcNewObjectFrame.left - rcDockingFrame.left) < abs(rcNewObjectFrame.right  - rcDockingFrame.right))  ? LEFT : RIGHT;

	CPoint ptTopLeft, ptBottomRight;
	float fTargetLeft, fTargetBottom, fTargetRight, fTargetTop;
	int  nTry     = 0;
	BOOL bRotated = FALSE;
	do
	{
		CSize  szFrame((bRotated) ? rcNewObjectFrame.Height() : rcNewObjectFrame.Width(), (bRotated) ? rcNewObjectFrame.Width()  : rcNewObjectFrame.Height());
		fSourceWidth  = (bRotated) ? fSourceTop - fSourceBottom : fSourceRight - fSourceLeft; 
		fSourceHeight = (bRotated) ? fSourceRight - fSourceLeft : fSourceTop - fSourceBottom;
		switch (nDockingDirection)
		{
		case LEFT:		ptTopLeft.x = rcDockingFrame.left - szFrame.cx;
						ptTopLeft.y = (nAlignment == TOP)  ? rcDockingFrame.top  : rcDockingFrame.bottom - szFrame.cy;	
						fTargetLeft = fDockingLeft - fSourceWidth;
						fTargetTop  = (nAlignment == TOP)  ? fDockingTop  : fDockingBottom + fSourceHeight;	
						break;
		case RIGHT:		ptTopLeft.x = rcDockingFrame.right;
						ptTopLeft.y = (nAlignment == TOP)  ? rcDockingFrame.top  : rcDockingFrame.bottom - szFrame.cy;	
						fTargetLeft = fDockingRight;
						fTargetTop  = (nAlignment == TOP)  ? fDockingTop  : fDockingBottom + fSourceHeight;	
						break;
		case TOP:		ptTopLeft.x = (nAlignment == LEFT) ? rcDockingFrame.left : rcDockingFrame.right  - szFrame.cx;
						ptTopLeft.y = rcDockingFrame.top;
						fTargetLeft = (nAlignment == LEFT) ? fDockingLeft : fDockingRight  - fSourceWidth;
						fTargetTop  = fDockingTop;
						break;
		case BOTTOM:	ptTopLeft.x = (nAlignment == LEFT) ? rcDockingFrame.left : rcDockingFrame.right  - szFrame.cx;
						ptTopLeft.y = rcDockingFrame.bottom - szFrame.cy;
						fTargetLeft = (nAlignment == LEFT) ? fDockingLeft : fDockingRight  - fSourceWidth;
						fTargetTop  = fDockingBottom + fSourceHeight;
						break;
		default:		return dtInfo;
		}

		ptBottomRight = ptTopLeft + szFrame;
		fTargetRight  = fTargetLeft + fSourceWidth;
		fTargetBottom = fTargetTop  - fSourceHeight;

		if (pTargetLayoutSide->IsInsideFreeArea(pTargetPrintSheet, fTargetLeft, fTargetBottom, fTargetRight, fTargetTop))
			break;
		
		switch (nTry)
		{
		case 0: bRotated = ! bRotated; break;	// change rotation
		case 1:	switch (nAlignment)				// change alignment
				{
				case LEFT:		nAlignment = RIGHT;  break;
				case RIGHT:		nAlignment = LEFT;   break;
				case BOTTOM:	nAlignment = TOP;    break;
				case TOP:		nAlignment = BOTTOM; break;
				}
				break;
		case 2: bRotated = ! bRotated; break;	// change rotation again
		case 3: return dtInfo;
		}
		nTry++;
	}while (1);

	rcNewFeedbackFrame = CRect(ptTopLeft, ptBottomRight);
	rcNewFeedbackFrame.NormalizeRect();

	dtInfo.m_nTargetType		 	= CFoldSheetDropTargetInfo::DropTargetSheet;
	dtInfo.m_bDockingRotate			= bRotated;
	dtInfo.m_pTargetPrintSheet	 	= pTargetPrintSheet;
	dtInfo.m_pTargetLayoutSide	 	= pTargetLayoutSide;
	dtInfo.m_pTargetLayoutObject 	= NULL;
	dtInfo.m_nTargetLayerRefIndex	= -1;
	dtInfo.m_fTargetLeft			= fTargetLeft;
	dtInfo.m_fTargetBottom			= fTargetBottom;
	dtInfo.m_fTargetRight			= fTargetRight;
	dtInfo.m_fTargetTop				= fTargetTop;

	return dtInfo;
}

void CPrintSheetView::DrawFeedback(CDC* pDC, CRect rcFeedback, void* pDropTargetInfo)
{
	rcFeedback.DeflateRect(1,1);
	CPen pen(PS_SOLID, 2, RED);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

	pDC->Rectangle(rcFeedback);

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();
}

DROPEFFECT CPrintSheetView::OnDragOverOutputMedia(CDisplayItem& /*di*/, DWORD /*dwKeyState*/, CPoint point)
{
	DROPEFFECT de = DROPEFFECT_MOVE;

	m_DisplayList.GiveDragFeedback(de, point);

	return de;
}

DROPEFFECT CPrintSheetView::OnDragOverMark(CDisplayItem& /*di*/, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT de = DROPEFFECT_MOVE;

	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		de = DROPEFFECT_MOVE;

	m_DisplayList.GiveDragFeedback(de, point);

	return de;
}

void CPrintSheetView::OnDragLeave() 
{
	CClientDC  dc(this);
	m_dragDropObject.DragLeave(&dc);

	CImpManDoc*  pDoc = GetDocument();
	if (pDoc)
		pDoc->m_PrintSheetList.m_Layouts.SetAllFoldSheetLayerVisible();

	m_DisplayList.Invalidate();
	Invalidate();
	UpdateWindow();
}

BOOL CPrintSheetView::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point) 
{
	m_DisplayList.Invalidate();

	// clean up focus rect
	m_DisplayList.RemoveDragFeedback();

	if (dropEffect == DROPEFFECT_NONE)
		return FALSE;

	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return FALSE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, RefEdgePrintSheet)))
		return OnDropRefEdgePrintSheet(sourceDI, dropEffect, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
		return OnDropLayoutObject(sourceDI, dropEffect, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		return OnDropFoldSheet(sourceDI, dropEffect, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia)))
		return OnDropOutputMedia(sourceDI, dropEffect, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark)))
		return OnDropMark(sourceDI, dropEffect, point);

	return FALSE;
}

BOOL CPrintSheetView::OnDropRefEdgePrintSheet(CDisplayItem& di, DROPEFFECT /*dropEffect*/, CPoint point) // RefEdge has been dropped
{
	CLayout*	  pLayout;
	CLayoutSide*  pLayoutSide = (CLayoutSide*)di.m_pData;
	int			  nProductType;
	CDisplayItem* pSheetDI = m_DisplayList.GetItemFromData(di.m_pData, di.m_pAuxData, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
	if (!pSheetDI)
		return FALSE;

	unsigned int quadrant = WhichQuadrant(pSheetDI->m_BoundingRect, point);

	if (quadrant == 0)
		return FALSE;

	UndoSave();

	if (theApp.settings.m_bFrontRearParallel)
	{
		pLayout		 = pLayoutSide->GetLayout();
		nProductType = pLayout->KindOfProduction();
		pLayoutSide->m_nPaperRefEdge = (unsigned char)quadrant; //KindOfProduction needs the original m_nPaperRefEdge	

		switch(pLayoutSide->m_nLayoutSide)
		{
			case FRONTSIDE : pLayoutSide = &(pLayout->m_BackSide);
							 break;
			case BACKSIDE  : pLayoutSide = &(pLayout->m_FrontSide);
							 break;
		}
		if (pLayoutSide != NULL)
		{
			switch (quadrant)
			{
				case LEFT_BOTTOM  : if (nProductType == WORK_AND_TURN)
										pLayoutSide->m_nPaperRefEdge = RIGHT_BOTTOM;
									else
										pLayoutSide->m_nPaperRefEdge = LEFT_TOP;
									break;
				case RIGHT_BOTTOM : if (nProductType == WORK_AND_TURN)
										pLayoutSide->m_nPaperRefEdge = LEFT_BOTTOM;
									else
										pLayoutSide->m_nPaperRefEdge = RIGHT_TOP;
									break;
				case LEFT_TOP	  : if (nProductType == WORK_AND_TURN)
										pLayoutSide->m_nPaperRefEdge = RIGHT_TOP;
									else
										pLayoutSide->m_nPaperRefEdge = LEFT_BOTTOM;
									break;
				case RIGHT_TOP	  : if (nProductType == WORK_AND_TURN)
										pLayoutSide->m_nPaperRefEdge = LEFT_TOP;
									else
										pLayoutSide->m_nPaperRefEdge = RIGHT_BOTTOM;
									break;
			}
		}
	}
	else
		pLayoutSide->m_nPaperRefEdge = (unsigned char)quadrant;

	CImpManDoc* pDoc = GetDocument();
	pDoc->SetModifiedFlag(); 

	Invalidate();
	UpdateWindow();

	theApp.OnUpdateView(RUNTIME_CLASS(CProdDataPrintView));
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	

	return TRUE;
}

BOOL CPrintSheetView::OnDropLayoutObject(CDisplayItem& /*di*/, DROPEFFECT dropEffect, CPoint /*point*/)
{
	UndoSave();

	float		fXOffset, fYOffset;
	unsigned	nSnapPosition;
	CSnapPoint* pSnapPoint = m_DisplayList.GetDistanceDragged(&fXOffset, &fYOffset, &nSnapPosition);

	CLayoutObject* pObject	   = NULL;
	CDisplayItem*  pSelectedDI = m_DisplayList.GetFirstSelectedItem();
	if (pSelectedDI)
		if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
			pObject = (CLayoutObject*)pSelectedDI->m_pData;

	BOOL bMarkObjectDuplicated = FALSE;
	while (pObject)
	{
		CLayoutObject* pCounterPartObject = (theApp.settings.m_bFrontRearParallel) ? pObject->FindCounterpartObject() : NULL;

		CReflinePositionParams* pReflinePosParams = &pObject->m_reflinePosParams;
		if ( ! pSnapPoint)
		{
			pReflinePosParams->m_fDistanceX += fXOffset;
			pReflinePosParams->m_fDistanceY += fYOffset;
		}
		else
		{
			pReflinePosParams->m_fDistanceX = 0.0f;
			pReflinePosParams->m_fDistanceY = 0.0f;
			CDisplayItem snapDI = *pSnapPoint->m_pItem;
			int			 nSideX = LEFT;
			int			 nSideY = TOP;
			switch (pSnapPoint->m_nSnapPosition & ~CDisplayItem::snapOutside)
			{
			case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
			case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
			case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
			case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
			case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
			case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
			case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
			case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
			case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
			}

			if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
			{
				pReflinePosParams->SetSheetReference(PLATE_REF, (BYTE)nSideX);
				pReflinePosParams->SetSheetReference(PLATE_REF, (BYTE)nSideY);
			}
			else
				if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
				{
					pReflinePosParams->SetSheetReference(PAPER_REF, (BYTE)nSideX);
					pReflinePosParams->SetSheetReference(PAPER_REF, (BYTE)nSideY);
				}
				else
					if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
					{
						BOOL bObjectBorder = (pSnapPoint->m_nSnapPosition & CDisplayItem::snapOutside) ? TRUE : FALSE;
						pReflinePosParams->SetSheetReference(OBJECT_REF, (BYTE)nSideX, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
						pReflinePosParams->SetSheetReference(OBJECT_REF, (BYTE)nSideY, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
					}

			switch (nSnapPosition & ~CDisplayItem::snapOutside)
			{
			case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
			case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
			case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
			case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
			case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
			case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
			case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
			case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
			case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
			}
			pReflinePosParams->SetObjectReference((BYTE)nSideX);
			pReflinePosParams->SetObjectReference((BYTE)nSideY);
		}

		if (dropEffect == DROPEFFECT_MOVE)
		{
			pObject->Offset(fXOffset, fYOffset);
			if (pCounterPartObject)
				pCounterPartObject->AlignToCounterPart(pObject);
		}
		else
		{
			// TODO: copying a page does change the foldsheet structure
			//		 so the question is if we have to create a new foldsheet ???
			//		 Actually we do not allow to copy a page!
			if ( ( ! pObject->IsLabel()) && (pObject->m_nType == CLayoutObject::Page) )
				;
			else
			{
				CLayoutObject* pCounterPartObject = (pObject) ? pObject->FindCounterpartObject() : NULL;
				CLayoutSide*   pLayoutSide		  = pObject->GetLayoutSide();
				CLayoutSide*   pCounterPartSide	  = (pCounterPartObject) ? pCounterPartObject->GetLayoutSide() : NULL;
				// Insert before modify, because Offset function needs pointer to layout side for invalidation
				POSITION pos = pLayoutSide->m_ObjectList.AddTail(*pObject);	 
				CLayoutObject& rNewLayoutObject = pLayoutSide->m_ObjectList.GetAt(pos); 
				rNewLayoutObject.Offset(fXOffset, fYOffset);
				if (pCounterPartSide)
				{
					pos = pCounterPartSide->m_ObjectList.AddTail(*pCounterPartObject);	 
					CLayoutObject& rNewCounterPartObject = pCounterPartSide->m_ObjectList.GetAt(pos); 
					rNewCounterPartObject.AlignToCounterPart(&rNewLayoutObject);
				}
				pObject = &rNewLayoutObject;
				bMarkObjectDuplicated = TRUE;
			}
		}											

		CPrintSheetFrame* pParentFrame = (CPrintSheetFrame*)GetParentFrame();
		if (pParentFrame)
		{
			if (GetDlgMoveObjects())
				if (GetDlgMoveObjects()->m_hWnd)
				{
					GetDlgMoveObjects()->m_objectPivotCtrl.LoadData(pReflinePosParams->m_nObjectReferenceLineInX, pReflinePosParams->m_nObjectReferenceLineInY);
					GetDlgMoveObjects()->InitReflinePosParams();
				}
			if (IsOpenDlgLayoutObjectGeometry())
			{
				GetDlgLayoutObjectGeometry()->LoadData();
				break;	// attention: PrintSheetView is updated here, so pSelectedItem is no longer valid
			}
		}

		pObject = NULL;
		pSelectedDI = m_DisplayList.GetNextSelectedItem(pSelectedDI);
		if (pSelectedDI)
			if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
				pObject = (CLayoutObject*)pSelectedDI->m_pData;
	}

	CImpManDoc* pDoc = GetDocument();
	pDoc->SetModifiedFlag();

	if (dropEffect != DROPEFFECT_MOVE)
	{
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

		if (bMarkObjectDuplicated)
		{
			CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
			if (pMarksView)
			{
				pMarksView->InitObjectList();
				pMarksView->m_dlgLayoutObjectControl.ReinitDialog(pMarksView->GetCurrentLayoutObject(), pMarksView->GetPrintSheet());
				pMarksView->m_dlgLayoutObjectPreview.ReinitDialog(pMarksView->GetCurrentLayoutObject(), pMarksView->GetPrintSheet());
			}
		}

//		if (theApp.m_nApplication == APPLICATION_LABELS)
		{
			pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
		}
	}

	OnUpdate(NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	

	return TRUE;
}

BOOL CPrintSheetView::OnDropFoldSheet(CDisplayItem& di, DROPEFFECT dropEffect, CPoint point)
{
	CImpManDoc*  pDoc = GetDocument();
	if ( ! pDoc)
		return FALSE;

	CClientDC dc(this);
	m_dragDropObject.DragLeave(&dc);

	CRect					 rcNewObjectFrame = m_dragDropObject.GetCurrentObjectFrame() + (point - m_ptMouse);
	CRect					 rcNewFeedbackFrame(0,0,0,0);
	CFoldSheetDropTargetInfo dropTargetInfo = FindFoldSheetDropTarget(rcNewObjectFrame, rcNewFeedbackFrame, di);

	pDoc->m_PrintSheetList.m_Layouts.SetAllFoldSheetLayerVisible();

	Invalidate();

	//if (dropTargetInfo.m_nTargetType == -1)
	//	return FALSE;

	CPrintSheet* 	pSourcePrintSheet	 = (CPrintSheet*)di.m_pAuxData;
	int			 	nSourceLayerRefIndex = (int)di.m_pData;
	BOOL		 	bSourceInnerForm	 = (di.m_AuxRect.Width()		  <= 2) ? TRUE : FALSE; // trick: use auxrect width to indicate if source is outer or inner form.
	BOOL		 	bTargetBackSide		 = FALSE;//(targetDI.m_AuxRect.Height() <= 2) ? TRUE : FALSE; // trick: use auxrect height to indicate if target is on layout backside
	BOOL		 	bAutoAssign			 = FALSE;										  

	CPrintSheet*	pTargetPrintSheet	 = dropTargetInfo.m_pTargetPrintSheet;
	BOOL			bFurtherQuestions	 = FALSE;
	CLayoutObject*	pSpreadObject		 = NULL;

	if (pSourcePrintSheet->GetLayout() == pTargetPrintSheet->GetLayout())
	{
		if (pSourcePrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
		{
			// If source and target layout are equal -> ask user if he wants
			// this operation be performed on all printsheets of this type   
			int ret = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNOCANCEL);
			switch (ret)
			{
			case IDYES	  :	bAutoAssign = TRUE;		
							if (pSourcePrintSheet != pTargetPrintSheet)	// If different printsheets and AutoAssign 
								dropEffect = DROPEFFECT_MOVE;				// -> only MOVE makes sense
							break;
			case IDNO	  : bFurtherQuestions = FALSE;
							break;
			case IDCANCEL : return FALSE;
			}
		}
	}

	if (dropEffect == DROPEFFECT_COPY)
	{
		if (bFurtherQuestions)
			if (AfxMessageBox(IDS_FOLDSHEET_REALLY_COPY, MB_YESNO) == IDNO)
				return FALSE;

		UndoSave();
		BeginWaitCursor();	// copy/cut and paste could take a while, in case of many printsheets
		pTargetPrintSheet->CopyAndPasteFoldSheet(pSourcePrintSheet, nSourceLayerRefIndex, bSourceInnerForm, dropTargetInfo, bTargetBackSide, bAutoAssign, pSpreadObject, FALSE);
		if (bAutoAssign)
			pDoc->m_PrintSheetList.CopyAndPasteFoldSheet(pTargetPrintSheet, nSourceLayerRefIndex);
		EndWaitCursor();
	}
	else
	{
		if (bFurtherQuestions)
			if (AfxMessageBox(IDS_FOLDSHEET_REALLY_MOVE, MB_YESNO) == IDNO)
				return FALSE;

		BOOL bReverse = (pSourcePrintSheet->m_nPrintSheetNumber <				// Determine direction of foldsheet move
						 pTargetPrintSheet->m_nPrintSheetNumber) ? TRUE : FALSE;
		UndoSave();
		BeginWaitCursor();
		pTargetPrintSheet->CutAndPasteFoldSheet (pSourcePrintSheet, nSourceLayerRefIndex, bSourceInnerForm, dropTargetInfo, bTargetBackSide, bAutoAssign, pSpreadObject, FALSE);
		if (bAutoAssign)
			pDoc->m_PrintSheetList.CutAndPasteFoldSheet(pTargetPrintSheet, bReverse);
		EndWaitCursor();
	}

	pSourcePrintSheet->GetLayout()->CheckForProductType(); 
	pTargetPrintSheet->GetLayout()->CheckForProductType();
									   
	pDoc->SetModifiedFlag();										

	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),	NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),			NULL, 0, NULL);	// because while drawing printsheets GetAct...() calls will be made

	return TRUE;
}

BOOL CPrintSheetView::OnDropOutputMedia(CDisplayItem& /*di*/, DROPEFFECT /*dropEffect*/, CPoint /*point*/)
{
	float fXOffset, fYOffset;

	m_DisplayList.GetDistanceDragged(&fXOffset, &fYOffset);

	CPrintSheetFrame* pParentFrame = (CPrintSheetFrame*)GetParentFrame();

	if ( !pParentFrame)
		return FALSE;
	CDlgPDFTargetProps*		pDlg			  = GetDlgOutputMedia()->GetDlgPDFTargetProps();
	CReflinePositionParams* pReflinePosParams = (pDlg) ? &pDlg->m_pdfTargetProps.m_mediaList[0].m_posInfos[pDlg->m_nCurTileIndex].m_reflinePosParams : NULL;
	if ( ! pReflinePosParams)
		return FALSE;

	unsigned	nMarkSnapPosition;
	CSnapPoint* pSnapPoint = m_DisplayList.GetDistanceDragged(&fXOffset, &fYOffset, &nMarkSnapPosition);

	if ( ! pSnapPoint)
	{
		pReflinePosParams->m_fDistanceX += fXOffset;
		pReflinePosParams->m_fDistanceY += fYOffset;
	}
	else
	{
		pReflinePosParams->m_fDistanceX = 0.0f;
		pReflinePosParams->m_fDistanceY = 0.0f;
		CDisplayItem snapDI = *pSnapPoint->m_pItem;
		int			 nSideX = LEFT;
		int			 nSideY = TOP;
        switch (pSnapPoint->m_nSnapPosition & ~CDisplayItem::snapOutside)
		{
		case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
		case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
		case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
		case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
		case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
		}

		if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
		{
			pReflinePosParams->SetSheetReference(PLATE_REF, (BYTE)nSideX);
			pReflinePosParams->SetSheetReference(PLATE_REF, (BYTE)nSideY);
		}
		else
			if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
			{
				pReflinePosParams->SetSheetReference(PAPER_REF, (BYTE)nSideX);
				pReflinePosParams->SetSheetReference(PAPER_REF, (BYTE)nSideY);
			}
			else
				if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
				{
					BOOL bObjectBorder = (pSnapPoint->m_nSnapPosition & CDisplayItem::snapOutside) ? TRUE : FALSE;
					pReflinePosParams->SetSheetReference(OBJECT_REF, (BYTE)nSideX, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
					pReflinePosParams->SetSheetReference(OBJECT_REF, (BYTE)nSideY, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
				}

        switch (nMarkSnapPosition & ~CDisplayItem::snapOutside)
		{
		case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
		case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
		case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
		case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
		case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
		}
		pReflinePosParams->SetObjectReference((BYTE)nSideX);
		pReflinePosParams->SetObjectReference((BYTE)nSideY);
	}

	if (pDlg)
		pDlg->UpdateControls();

	OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CPrintSheetView::OnDropMark(CDisplayItem& /*di*/, DROPEFFECT /*dropEffect*/, CPoint /*point*/)
{
   	CPrintSheetFrame* pParentFrame = (CPrintSheetFrame*)GetParentFrame();
	if ( ! pParentFrame)
		return FALSE;

	CMark*		 pMark		 = NULL;
	CDlgMarkSet* pDlgMarkSet = GetDlgMarkSet();
	if (pDlgMarkSet)
		if ( (pDlgMarkSet->m_nCurMarkSel >= 0) && (pDlgMarkSet->m_nCurMarkSel < pDlgMarkSet->m_markList.GetSize()) )
		{
		    if (GetKeyState(VK_CONTROL) < 0) // Ctrl key pressed
				pDlgMarkSet->DuplicateCurrentMark();
			pMark = pDlgMarkSet->m_markList[pDlgMarkSet->m_nCurMarkSel];
		}

	CDlgLayoutObjectProperties* pDlgNewMark = NULL;
	if ( ! pMark)
	{
		pDlgNewMark = GetDlgNewMark();
		if (pDlgNewMark)
			pMark = pDlgNewMark->m_pMark;
	}

	if ( ! pMark)
		return FALSE;

	CDlgLayoutObjectGeometry* pDlgObjectGeometry = GetDlgLayoutObjectGeometry();
	int nReflinePosParamsID = (pDlgObjectGeometry) ? pDlgObjectGeometry->GetReflinePosParamsID() : 1;

	CReflinePositionParams* pReflinePosParams = ((CReflineMark*)pMark)->GetReflinePosParams(nReflinePosParamsID);
	if ( ! pReflinePosParams)
		return FALSE;

	float		fXOffset, fYOffset;
	unsigned	nMarkSnapPosition;
	CSnapPoint* pSnapPoint = m_DisplayList.GetDistanceDragged(&fXOffset, &fYOffset, &nMarkSnapPosition);

	if ( ! pSnapPoint)
	{
		pReflinePosParams->m_fDistanceX += fXOffset;
		pReflinePosParams->m_fDistanceY += fYOffset;
	}
	else
	{
		pReflinePosParams->m_fDistanceX = 0.0f;
		pReflinePosParams->m_fDistanceY = 0.0f;
		CDisplayItem snapDI = *pSnapPoint->m_pItem;
		int			 nSideX = LEFT;
		int			 nSideY = TOP;
        switch (pSnapPoint->m_nSnapPosition & ~CDisplayItem::snapOutside)
		{
		case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
		case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
		case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
		case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
		case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
		}

		if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
		{
			pReflinePosParams->SetSheetReference(PLATE_REF, (BYTE)nSideX);
			pReflinePosParams->SetSheetReference(PLATE_REF, (BYTE)nSideY);
		}
		else
			if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
			{
				pReflinePosParams->SetSheetReference(PAPER_REF, (BYTE)nSideX);
				pReflinePosParams->SetSheetReference(PAPER_REF, (BYTE)nSideY);
			}
			else
				if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
				{
					BOOL bObjectBorder = (pSnapPoint->m_nSnapPosition & CDisplayItem::snapOutside) ? TRUE : FALSE;
					pReflinePosParams->SetSheetReference(OBJECT_REF, (BYTE)nSideX, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
					pReflinePosParams->SetSheetReference(OBJECT_REF, (BYTE)nSideY, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
				}

        switch (nMarkSnapPosition & ~CDisplayItem::snapOutside)
		{
		case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
		case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
		case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
		case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
		case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
		}
		pReflinePosParams->SetObjectReference((BYTE)nSideX);
		pReflinePosParams->SetObjectReference((BYTE)nSideY);
	}

	if (pDlgMarkSet)
		pDlgMarkSet->m_dlgLayoutObjectControl.DataExchangeLoad();
	if (pDlgNewMark)
		pDlgNewMark->m_dlgLayoutObjectControl.DataExchangeLoad();

	OnUpdate(NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	

	return TRUE;
}
