// NewPrintSheetFrame.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetPlanningView.h"
#include "PrintComponentsDetailView.h"
#include "PrintComponentsView.h"
#include "NewPrintSheetFrame.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "DummyView.h"
#include "MainFrm.h"
#include "PrintSheetShowAllToolsView.h"
#include "ProdDataPrepressToolsView.h" 
#include "PrintSheetOutputToolsView.h"
#include "DlgChangeFormats.h"
#include "PrintSheetTableView.h"
#include "PrintSheetViewStrippingData.h"
#include "PrintSheetColorSettingsView.h"
#include "PrintSheetMaskSettingsView.h"
#include "PrintSheetFrameLeftToolsView.h"
#include "PrintSheetView.h"
#include "PrintSheetComponentsView.h"
#include "PrintSheetObjectPreview.h"
#include "DlgNotes.h"
#include "DlgSheet.h"
#include "ProductsView.h"
#include "OrderDataView.h"
#include "DlgOptimizationMonitor.h"
#include "DlgProductionManager.h"
#include "GraphicComponent.h"



IMPLEMENT_DYNAMIC(CPrintSheetFrameSplitter, CSplitterWnd)

BEGIN_MESSAGE_MAP(CPrintSheetFrameSplitter, CSplitterWnd)
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()

// CProdDataSplitter message handlers
afx_msg LRESULT CPrintSheetFrameSplitter::OnNcHitTest(CPoint point)
{
	//CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	//if (pFrame)
	//{
	//	if (this == &pFrame->m_wndSplitter)
	//	{
	//		CWnd* pPane = pFrame->m_wndSplitter.GetPane(0,1);
	//		CRect rcWindow1, rcWindow2;
	//		pPane->GetWindowRect(rcWindow1);
	//		pPane = pFrame->m_wndSplitter.GetPane(0,2);
	//		pPane->GetWindowRect(rcWindow2);
	//		if ( (rcWindow1.right <= point.x) && (point.x <= rcWindow2.left) )
	//		{
	//			if (pFrame->m_browserInfo.m_nShowInfo & CNewPrintSheetFrame::ShowProducts)
	//				return CSplitterWnd::OnNcHitTest(point);
	//		}
	//	}

	//	if (this == &pFrame->m_wndSplitter1)
	//	{
	//		CWnd* pPane = pFrame->m_wndSplitter1.GetPane(1,0);
	//		CRect rcWindow1, rcWindow2;
	//		pPane->GetWindowRect(rcWindow1);
	//		pPane = pFrame->m_wndSplitter1.GetPane(2,0);
	//		pPane->GetWindowRect(rcWindow2);
	//		if ( (rcWindow1.bottom <= point.y) && (point.y <= rcWindow2.top) )
	//			return CSplitterWnd::OnNcHitTest(point);
	//	}

	//	if (this == &pFrame->m_wndSplitter2)
	//		if (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionPrintSheetMarks)
	//		{
	//			CWnd* pPane = pFrame->m_wndSplitter2.GetPane(0,1);
	//			CRect rcWindow1, rcWindow2;
	//			pPane->GetWindowRect(rcWindow1);
	//			pPane = pFrame->m_wndSplitter2.GetPane(0,2);
	//			pPane->GetWindowRect(rcWindow2);
	//			if ( (rcWindow1.right <= point.x) && (point.x <= rcWindow2.left) )
	//				return CSplitterWnd::OnNcHitTest(point);
	//		}
	//}

	return CSplitterWnd::OnNcHitTest(point);//HTNOWHERE;
}

void CPrintSheetFrameSplitter::DrawAllSplitBars(CDC* pDC, int cxInside, int cyInside)
{
	ASSERT_VALID(this);

	int col;
	int row;

	// draw column split bars
	CRect rect;
	GetClientRect(rect);
	rect.left += m_cxBorder;
	for (col = 0; col < m_nCols - 1; col++)
	{
		rect.left += m_pColInfo[col].nCurSize + m_cxBorderShare;
		rect.right = rect.left + m_cxSplitter;
		if (rect.left > cxInside)
			break;      // stop if not fully visible
		OnDrawSplitter(pDC, splitBar, rect, -1, col);
		rect.left = rect.right + m_cxBorderShare;
	}

	// draw row split bars
	GetClientRect(rect);
	rect.top += m_cyBorder;
	for (row = 0; row < m_nRows - 1; row++)
	{
		rect.top += m_pRowInfo[row].nCurSize + m_cyBorderShare;
		rect.bottom = rect.top + m_cySplitter;
		if (rect.top > cyInside)
			break;      // stop if not fully visible
		OnDrawSplitter(pDC, splitBar, rect, row, -1);
		rect.top = rect.bottom + m_cyBorderShare;
	}

	// draw pane borders
	GetClientRect(rect);
	int x = rect.left;
	for (col = 0; col < m_nCols; col++)
	{
		int cx = m_pColInfo[col].nCurSize + 2*m_cxBorder;
		if (col == m_nCols-1 && m_bHasVScroll)
			cx += GetSystemMetrics(SM_CXVSCROLL);
		int y = rect.top;
		for (row = 0; row < m_nRows; row++)
		{
			int cy = m_pRowInfo[row].nCurSize + 2*m_cyBorder;
			if (row == m_nRows-1 && m_bHasHScroll)
				cy += GetSystemMetrics(SM_CYHSCROLL);
			OnDrawSplitter(pDC, splitBorder, CRect(x, y, x+cx, y+cy), -1, -1);
			y += cy + m_cySplitterGap - 2*m_cyBorder;
		}
		x += cx + m_cxSplitterGap - 2*m_cxBorder;
	}
}

void CPrintSheetFrameSplitter::OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect, int nRow, int nCol)
{
	if (pDC == NULL)
	{
		RedrawWindow(rect, NULL, RDW_INVALIDATE|RDW_NOCHILDREN);
		return;
	}

	CRect rcRect = rect;
	switch (nType)
	{
	case splitBorder:
		break;

	case splitIntersection:
		break;

	case splitBox:
		break;

	case splitBar:
		{
			CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
			if ( ! pFrame)
				break;

			CPen pen(PS_SOLID, 1, LIGHTGRAY);
			CPen* pOldPen = pDC->SelectObject(&pen);
			if (rcRect.Height() > rcRect.Width())
			{
				rcRect.DeflateRect(0, 2);
				if (this == &pFrame->m_wndSplitter2)
				{
					if (nCol == 3)
					{
						pDC->FillSolidRect(rcRect, ::GetSysColor(CTLCOLOR_DLG));
						pDC->MoveTo(rcRect.left, rcRect.top);
						pDC->LineTo(rcRect.left, rcRect.bottom);
					}
					else
					{
						CRect rcHeader = rcRect; rcHeader.bottom = rcHeader.top + 34;
						CGraphicComponent gp;
						gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);
						rcHeader.top = rcHeader.bottom - 3;
						pDC->FillSolidRect(rcHeader, RGB(215,220,225));
						rcRect.top += 34;
						pDC->FillSolidRect(rcRect, (nCol == 2) ? ::GetSysColor(CTLCOLOR_DLG) : RGB(227,232,238));
						if ( (nCol == 0) || (nCol == 2) )
						{
							pDC->MoveTo(rcRect.left, rcRect.top);
							pDC->LineTo(rcRect.left, rcRect.bottom);
						}
						else
						{
							pDC->MoveTo(rcRect.right-1, rcRect.top);
							pDC->LineTo(rcRect.right-1, rcRect.bottom);
						}
					}
				}
				else
				{
					pDC->FillSolidRect(rcRect, RGB(211,218,228));
					pDC->MoveTo(rcRect.right-1, rcRect.top);
					pDC->LineTo(rcRect.right-1, rcRect.bottom);
				}
			}
			else
			{
				rcRect.DeflateRect(2, 0); 
				pDC->FillSolidRect(rcRect, (this == &pFrame->m_wndSplitter5) ? RGB(227,232,238) : ::GetSysColor(CTLCOLOR_DLG));
				pDC->MoveTo(rcRect.left,  rcRect.top);
				pDC->LineTo(rcRect.right, rcRect.top);
				if (this != &pFrame->m_wndSplitter5)
				{
					pDC->MoveTo(rcRect.left,  rcRect.bottom - 1);
					pDC->LineTo(rcRect.right, rcRect.bottom - 1);
				}
			}
			pDC->SelectObject(pOldPen);
			pen.DeleteObject();
		}
		break;

	default:
		ASSERT(FALSE);  // unknown splitter type
	}
}

void CPrintSheetFrameSplitter::StopTracking(BOOL bAccept)
{
	if (m_bTracking && bAccept)
	{
		CSplitterWnd::StopTracking(bAccept);
	
		CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
		if (pFrame)
			if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			{
				BOOL  bRearrange = FALSE;
				CRect rcView;
				if (pFrame->m_browserInfo.m_nShowInfo & CNewPrintSheetFrame::ShowPrintSheetList)
				{
					pFrame->m_wndSplitter3.GetPane(1, 0)->GetClientRect(rcView);
					if (rcView.Height() < 5) 
						pFrame->m_browserInfo.m_nShowInfo &= ~CNewPrintSheetFrame::ShowPrintSheetList;
					else
						pFrame->m_browserInfo.m_nListViewHeight = rcView.Height();
					bRearrange = TRUE;
				}
				if (pFrame->m_wndSplitter2.GetPane(0, 0))
				{
					pFrame->m_wndSplitter2.GetPane(0, 0)->GetClientRect(rcView);
					pFrame->m_browserInfo.m_nComponentsViewWidth = max(rcView.Width(), 80);
					if (pFrame->GetPrintComponentsView())
					{
						//pFrame->GetPrintComponentsView()->m_DisplayList.Invalidate();
						pFrame->GetPrintComponentsView()->OnUpdate(NULL, 0, NULL);
					}
					bRearrange = TRUE;
				}
				if (pFrame->m_wndSplitter2.GetPane(0, 1)->IsKindOf((RUNTIME_CLASS(CPrintSheetNavigationView))))
				{
					pFrame->m_wndSplitter2.GetPane(0, 1)->GetClientRect(rcView);
					pFrame->m_browserInfo.m_nNavigationViewWidth = max(rcView.Width(), 80);
					if (pFrame->GetPrintSheetNavigationView())
					{
						pFrame->GetPrintSheetNavigationView()->m_DisplayList.Invalidate();
						pFrame->GetPrintSheetNavigationView()->OnUpdate(NULL, 0, NULL);
					}
					bRearrange = TRUE;
				}
				if (pFrame->m_wndSplitter2.GetPane(0, 4)->IsKindOf((RUNTIME_CLASS(CPrintSheetObjectPreview))))
				{
					pFrame->m_wndSplitter2.GetPane(0, 4)->GetClientRect(rcView);
					pFrame->m_browserInfo.m_nObjectPreviewWidth = rcView.Width();
					bRearrange = TRUE;
				}
				if (bRearrange)
					pFrame->RearrangeSplitters();
			}
	}
	else
		CSplitterWnd::StopTracking(bAccept);
}




// CNewPrintSheetFrame

IMPLEMENT_DYNCREATE(CNewPrintSheetFrame, CMDIChildWnd)

CNewPrintSheetFrame::CNewPrintSheetFrame()
{
	m_bShowPrintMenu						= TRUE;
	m_bStatusRestored						= FALSE;
	m_bShowStrippingData					= TRUE;
	m_bShowGripper							= FALSE;
	m_bShowFoldSheet						= TRUE;
	m_bShowMeasure							= FALSE;
	m_bShowPlate							= TRUE;
	m_bShowColorants						= FALSE;
	m_bShowExposures						= FALSE;
	m_bShowBitmap							= FALSE;
	m_bShowBoxShapes						= FALSE;
	m_bShowCutBlocks						= FALSE;
	m_bLayoutObjectSelected 				= FALSE;
	m_bFoldSheetSelected					= FALSE;
	m_bMarksViewActive						= FALSE;
	m_bUndoActive							= FALSE;
	m_bTrimToolActive						= FALSE;
	m_bFoldSheetRotateActive				= FALSE;
	m_bFoldSheetTurnActive					= FALSE;
	m_bFoldSheetTumbleActive				= FALSE;
	m_bEnableReleaseBleedButton 			= FALSE;
	m_bHandActive							= FALSE;
	m_bShowImpositionAssistent				= FALSE;
	m_bShowOutputSettingsView				= FALSE;
	m_bShowOutputMaskSettings				= FALSE;
	m_bShowOutputColorSettings				= FALSE;
	m_bAlignSectionMenuActive				= FALSE;
	m_bAlignSectionActive					= FALSE;
	m_nViewMode								= 0;
	m_nViewSide								= FRONTSIDE;
	m_browserInfo.m_nShowInfo				= ShowProducts;
	m_browserInfo.m_nProductsViewHeight 	= 150;
	m_browserInfo.m_nListViewHeight			= 150;
	m_browserInfo.m_nObjectPreviewWidth		= 150;
	m_browserInfo.m_nComponentsViewWidth	= 300;
	m_browserInfo.m_nNavigationViewWidth	= 150;

	m_pDlgOutputMedia						= new CDlgPDFOutputMediaSettings;
	m_pDlgLayoutObjectProperties			= new CDlgLayoutObjectProperties;
	m_pDlgMarkSet							= new CDlgMarkSet;
	m_pDlgMoveObjects						= new CDlgMoveObjects;
}

CNewPrintSheetFrame::~CNewPrintSheetFrame()
{
	delete m_pDlgOutputMedia;
	delete m_pDlgLayoutObjectProperties;
	delete m_pDlgMarkSet;
	delete m_pDlgMoveObjects;
}

BEGIN_MESSAGE_MAP(CNewPrintSheetFrame, CMDIChildWnd)
	ON_COMMAND(ID_FILE_PAGE_SETUP, OnFilePageSetup)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdateFilePrintPreview)
	ON_COMMAND(ID_PROCESS_PREPRESS, OnProcessPrePress)
	ON_COMMAND(ID_ZOOM, OnZoom)
	ON_UPDATE_COMMAND_UI(ID_ZOOM, OnUpdateZoom)
	ON_COMMAND(ID_ZOOM_DECREASE, OnZoomDecrease)
	ON_COMMAND(ID_ZOOM_FULLVIEW, OnZoomFullview)
	ON_COMMAND(ID_SCROLLHAND, OnScrollhand)
	ON_COMMAND(ID_VIEW_ALLSIDES, OnViewAllsides)
	ON_COMMAND(ID_VIEW_FRONTSIDE, OnViewFrontside)
	ON_COMMAND(ID_VIEW_BACKSIDE, OnViewBackside)
	ON_COMMAND(ID_VIEW_BOTHSIDES, OnViewBothsides)
	ON_COMMAND(IDC_FOLDSHEETCHECK, OnFoldsheetCheck)
	ON_UPDATE_COMMAND_UI(IDC_FOLDSHEETCHECK, OnUpdateFoldsheetcheck)
	ON_COMMAND(IDC_MEASURECHECK, OnMeasureCheck)
	ON_UPDATE_COMMAND_UI(IDC_MEASURECHECK, OnUpdateMeasurecheck)
	ON_COMMAND(IDC_EXPOSURECHECK, OnExposuresCheck)
	ON_UPDATE_COMMAND_UI(IDC_EXPOSURECHECK, OnUpdateExposurescheck)
	ON_COMMAND(IDC_PLATECHECK, OnPlatecheck)
	ON_UPDATE_COMMAND_UI(IDC_PLATECHECK, OnUpdatePlatecheck)
	ON_COMMAND(IDC_BITMAPCHECK, OnBitmapCheck)
	ON_UPDATE_COMMAND_UI(IDC_BITMAPCHECK, OnUpdateBitmapcheck)
	ON_COMMAND(ID_SHOW_CUTBLOCKS, OnShowCutBlocks)
	ON_UPDATE_COMMAND_UI(ID_SHOW_CUTBLOCKS, OnUpdateCutBlockscheck)
	ON_COMMAND(ID_SHOW_BOXSHAPES, OnShowBoxshapes)
	ON_COMMAND(ID_DEFINE_TRIM, OnDefineTrim)
	ON_COMMAND(ID_MOVE_OBJECT, OnMoveObjects)
	ON_COMMAND(ID_SWAP_OBJECTS, OnSwapObjects)
	ON_COMMAND(ID_CHANGE_FORMATS, OnChangeFormats)
	ON_COMMAND(ID_RENAME_LAYOUTOBJECT, OnRenameLayoutObject)
	ON_COMMAND(ID_OBJECT_PROPERTIES, OnObjectProperties)
	ON_COMMAND(ID_OBJECTSROTATE_CLOCK, OnObjectsrotateClock)
	ON_COMMAND(ID_OBJECTSROTATE_COUNTERCLOCK, OnObjectsrotateCounterclock)
	ON_COMMAND(ID_OBJECTS_TURN, OnObjectsTurn)
	ON_COMMAND(ID_OBJECTS_TUMBLE, OnObjectsTumble)
	ON_COMMAND(ID_DELETE_SELECTION, OnDeleteSelection)
	ON_COMMAND(ID_LOCK_UNLOCK_FOLDSHEET, OnLockUnlockFoldSheet)
	ON_COMMAND(ID_UNDO, OnUndo)
	ON_COMMAND(IDC_RELEASE_BLEED, OnReleaseBleed)
	ON_COMMAND(ID_SELECT_PRINTSHEETVIEW_STRIPPINGDATA, OnSelectPrintSheetViewStrippingData)
	ON_COMMAND(ID_DO_IMPOSE, OnDoImpose)
	ON_COMMAND(ID_DO_IMPOSE_AUTO, OnDoImposeAuto)
	ON_COMMAND(ID_ADDNEW_PRINTSHEET, OnAddNewPrintSheet)
	ON_COMMAND(ID_REMOVE_PRINTSHEET, OnRemovePrintSheet)
	ON_COMMAND(ID_MODIFY_SHINGLING, OnModifyShingling)
	ON_COMMAND(ID_OUTPUT_MASK_SETTINGS, OnOutputMaskSettings)
	ON_COMMAND(ID_OUTPUT_COLOR_SETTINGS, OnOutputColorSettings)
	ON_COMMAND(ID_ALIGN_SECTION, OnAlignSection)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_SECTION, OnUpdateAlignSection)
	ON_COMMAND(ID_RESET_SECTION, OnResetSection)
	ON_UPDATE_COMMAND_UI(ID_RESET_SECTION, OnUpdateResetSection)
	ON_COMMAND(ID_NOTES, OnNotes)
	ON_COMMAND(ID_DEFINE_SHEET, OnDefineSheet)
	ON_COMMAND(ID_TURN_TUMBLE, OnTurnTumblePrintSheet)
	ON_UPDATE_COMMAND_UI(ID_TURN_TUMBLE, OnUpdateTurnTumble)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

BOOL CNewPrintSheetFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* pContext)
{
	if ( ! m_wndSplitter.CreateStatic(this, 1, 2))
		return FALSE;
	if ( ! m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CPrintSheetFrameLeftToolsView), CSize(0, 0), pContext))
		return FALSE;

	if ( ! m_wndSplitter2.CreateStatic(&m_wndSplitter, 1, 5, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter.IdFromRowCol(0, 1)))
		return FALSE;
	if ( ! m_wndSplitter2.CreateView(0, 1, RUNTIME_CLASS(CPrintSheetNavigationView), CSize(0, 0), pContext))
		return FALSE;
	if ( ! m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CPrintSheetViewStrippingData), CSize(0, 0), pContext))
		return FALSE;
	if ( ! m_wndSplitter2.CreateView(0, 4, RUNTIME_CLASS(CProdDataPrintToolsView), CSize(0, 0), pContext))
		return FALSE;

	if ( ! m_wndSplitter3.CreateStatic(&m_wndSplitter2, 2, 1, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter2.IdFromRowCol(0, 3)))
		return FALSE;
	if ( ! m_wndSplitter3.CreateView(0, 0, RUNTIME_CLASS(CPrintSheetView), CSize(0, 0), pContext))
		return FALSE;
	if (m_browserInfo.m_nShowInfo & ShowPrintSheetList)
	{
		if ( ! m_wndSplitter3.CreateView(1, 0, RUNTIME_CLASS(CPrintSheetTableView), CSize(0, m_browserInfo.m_nListViewHeight), pContext))
			return FALSE;
	}
	else
		if ( ! m_wndSplitter3.CreateView(1, 0, RUNTIME_CLASS(CDummyView), CSize(0, 0), pContext))
			return FALSE;

	if ( ! m_wndSplitter5.CreateStatic(&m_wndSplitter2, 2, 1, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter2.IdFromRowCol(0, 0)))
		return FALSE;
	if ( ! m_wndSplitter5.CreateView(0, 0, RUNTIME_CLASS(CPrintComponentsView), CSize(m_browserInfo.m_nComponentsViewWidth, 1000), pContext))
		return FALSE;
	if ( ! m_wndSplitter5.CreateView(1, 0, RUNTIME_CLASS(CPrintComponentsDetailView), CSize(0, 90), pContext))
		return FALSE;

	return TRUE;
}


void CNewPrintSheetFrame::OnFilePageSetup() 
{
	PRINTDLG printDlg;
	theApp.GetPrinterDeviceDefaults(&printDlg);

	CPageSetupDialog dlg;
	dlg.m_psd.lStructSize		= sizeof(PAGESETUPDLG);
	dlg.m_psd.hwndOwner			= m_hWnd;
	dlg.m_psd.hDevNames			= printDlg.hDevNames;
	dlg.m_psd.hDevMode			= printDlg.hDevMode;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	TCHAR szMargins[100];
	DWORD dwSize = sizeof(szMargins);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szMargins, &dwSize) == ERROR_SUCCESS)
		_stscanf_s(szMargins, _T("%ld %ld %ld %ld"), &dlg.m_psd.rtMargin.left, &dlg.m_psd.rtMargin.top, &dlg.m_psd.rtMargin.right, &dlg.m_psd.rtMargin.bottom);
	else
		dlg.m_psd.rtMargin = CRect(2500, 2500, 2500, 2500);

	dlg.DoModal();

	theApp.SelectPrinter(dlg.m_psd.hDevNames, dlg.m_psd.hDevMode);

	_stprintf(szMargins, _T("%ld %ld %ld %ld"), dlg.m_psd.rtMargin.left, dlg.m_psd.rtMargin.top, dlg.m_psd.rtMargin.right, dlg.m_psd.rtMargin.bottom);
	RegSetValueEx(hKey, _T("PrintMargins"), 0, REG_SZ, (LPBYTE)szMargins, (_tcslen(szMargins) + 1) * sizeof(TCHAR));
}

void CNewPrintSheetFrame::OnFilePrintPreview() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->OnFilePrintPreview();
}

void CNewPrintSheetFrame::OnUpdateFilePrintPreview(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bShowPrintMenu);
}

void CNewPrintSheetFrame::OnFilePrint() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->OnFilePrint();
}

void CNewPrintSheetFrame::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bShowPrintMenu);
}
//
//void CNewPrintSheetFrame::SetupViewProducts()
//{
//	CCreateContext context;
//	context.m_pNewViewClass	  = RUNTIME_CLASS(CProductsView);
//	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
//	context.m_pNewDocTemplate = NULL;
//	context.m_pLastView		  = NULL;
//	context.m_pCurrentFrame	  = NULL;
//
//	int nWidth0, nWidth1, nDummy;
//	m_wndSplitter4.GetColumnInfo(0, nWidth0, nDummy);
//	m_wndSplitter4.GetColumnInfo(1, nWidth1, nDummy);
//
//	if (m_browserInfo.m_nShowInfo & ShowProducts)
//	{
//		if ( ! m_wndSplitter4.GetPane(0, 0)->IsKindOf(RUNTIME_CLASS(CProductsView)))
//		{
//			if (m_browserInfo.m_nProductsViewHeight < 10)
//				m_browserInfo.m_nProductsViewHeight = 250;
//
//			m_wndSplitter1.SetRowInfo(0, m_browserInfo.m_nProductsViewHeight, 10);
//			m_wndSplitter4.DeleteView(0, 0);
//			m_wndSplitter4.CreateView(0, 0, RUNTIME_CLASS(CProductsView), CSize(nWidth0, m_browserInfo.m_nProductsViewHeight), &context);
//			((CProductsView*)m_wndSplitter4.GetPane(0, 0))->OnInitialUpdate();
//			m_wndSplitter4.DeleteView(0, 1);
//			m_wndSplitter4.CreateView(0, 1, RUNTIME_CLASS(COrderDataView), CSize(nWidth1, m_browserInfo.m_nProductsViewHeight), &context);
//			((CProductsView*)m_wndSplitter4.GetPane(0, 1))->OnInitialUpdate();
//			m_wndSplitter1.RecalcLayout();
//			m_wndSplitter4.RecalcLayout();
//			RearrangeSplitters();	
//		}
//		else
//		{
//			if (m_browserInfo.m_nProductsViewHeight < 10)
//				m_browserInfo.m_nProductsViewHeight = 250;
//			m_wndSplitter1.SetRowInfo(0, m_browserInfo.m_nProductsViewHeight, 10);
//			m_wndSplitter4.SetRowInfo(0, m_browserInfo.m_nProductsViewHeight, 10);
//			m_wndSplitter1.RecalcLayout();
//			m_wndSplitter4.RecalcLayout();
//			RearrangeSplitters();	
//		}
//	}
//	else
//	{
//		if ( ! m_wndSplitter4.GetPane(0, 0)->IsKindOf(RUNTIME_CLASS(CDummyView)))
//		{
//			m_wndSplitter1.SetRowInfo(0, 0, 10);
//			m_wndSplitter4.DeleteView(0, 0);
//			m_wndSplitter4.CreateView(0, 0, RUNTIME_CLASS(CDummyView), CSize(nWidth0, m_browserInfo.m_nProductsViewHeight), &context);
//			m_wndSplitter4.DeleteView(0, 1);
//			m_wndSplitter4.CreateView(0, 1, RUNTIME_CLASS(CDummyView), CSize(nWidth1, m_browserInfo.m_nProductsViewHeight), &context);
//			m_wndSplitter1.RecalcLayout();
//			m_wndSplitter4.RecalcLayout();
//			RearrangeSplitters();	
//		}
//	}
//
//	if (m_wndSplitter2.GetPane(0, 1)->IsKindOf((RUNTIME_CLASS(CPrintSheetNavigationView))))
//		((CPrintSheetNavigationView*)m_wndSplitter2.GetPane(0, 1))->OnUpdate(NULL, 0, NULL);	// recalc navigationview extents
//}

void CNewPrintSheetFrame::SetupViewPrintSheetList(BOOL bRearrange)
{
	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CPrintSheetComponentsView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	if (m_browserInfo.m_nShowInfo & ShowPrintSheetList)
	{
		if ( ! m_wndSplitter3.GetPane(1, 0)->IsKindOf(RUNTIME_CLASS(CPrintSheetComponentsView)))
		{
			if (m_browserInfo.m_nListViewHeight < 10)
				m_browserInfo.m_nListViewHeight = 150;
			m_wndSplitter3.DeleteView(1, 0);
			m_wndSplitter3.CreateView(1, 0, RUNTIME_CLASS(CPrintSheetComponentsView), CSize(0, m_browserInfo.m_nListViewHeight), &context);
			((CPrintSheetComponentsView*)m_wndSplitter3.GetPane(1, 0))->OnInitialUpdate();
			if (bRearrange)
				RearrangeSplitters();	
			SetActiveView((CView*)m_wndSplitter3.GetPane(1, 0));
		}
		else
		{
			if (m_browserInfo.m_nListViewHeight < 10)
			{
				m_browserInfo.m_nListViewHeight = 150;
				m_wndSplitter3.SetRowInfo(1, m_browserInfo.m_nListViewHeight, 10);
				m_wndSplitter3.RecalcLayout();
			}
			if (bRearrange)
				RearrangeSplitters();	
		}
	}
	else
	{
		if ( ! m_wndSplitter3.GetPane(1, 0)->IsKindOf(RUNTIME_CLASS(CDummyView)))
		{
			m_wndSplitter3.DeleteView(1, 0);
			m_wndSplitter3.CreateView(1, 0, RUNTIME_CLASS(CDummyView), CSize(0, 50), &context);
			if (bRearrange)
				RearrangeSplitters();	
		}
	}
}

BOOL CNewPrintSheetFrame::SetupViewSingleSheet(BOOL /*bResetAnyway*/)
{
	m_nViewSide		 = (m_nViewSide == ALLSIDES) ? FRONTSIDE : m_nViewSide;
	m_bShowExposures = 	m_bShowColorants = FALSE;
	m_bShowGripper	 = TRUE;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CPrintSheetViewStrippingData);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter5.DeleteView(0, 0);
	m_wndSplitter5.CreateView(0, 0, RUNTIME_CLASS(CPrintComponentsView), CSize(m_browserInfo.m_nComponentsViewWidth, 0), &context);
	CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)m_wndSplitter5.GetPane(0, 0);
	pPrintComponentsView->OnInitialUpdate();
	pPrintComponentsView->OnUpdate(NULL, 0, NULL);

	m_wndSplitter5.DeleteView(1, 0);
	m_wndSplitter5.CreateView(1, 0, RUNTIME_CLASS(CPrintComponentsDetailView), CSize(1, 0), &context);
	CPrintComponentsDetailView* pPrintComponentsDetailView = (CPrintComponentsDetailView*)m_wndSplitter5.GetPane(1, 0);
	pPrintComponentsDetailView->OnInitialUpdate();
	pPrintComponentsDetailView->OnUpdate(NULL, 0, NULL);

	pPrintComponentsView->ResizeDetailsView();
	pPrintComponentsView->UpdateWindow();

	m_wndSplitter2.DeleteView(0, 2);
	m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CPrintSheetViewStrippingData), CSize(300, 0), &context);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)m_wndSplitter3.GetPane(0, 0);
	pPrintSheetView->SetViewMode(ID_VIEW_SINGLE_SHEETS);
	pPrintSheetView->m_zoomSteps.RemoveAll();

	m_wndSplitter2.DeleteView(0, 4);
	m_wndSplitter2.CreateView(0, 4, RUNTIME_CLASS(CProdDataPrintToolsView),	CSize(0, 0), &context);

	SetupViewPrintSheetList(FALSE);

	SetActiveView(pPrintSheetView);

	RearrangeSplitters();

	pPrintSheetView->m_DisplayList.Invalidate();
	pPrintSheetView->OnUpdate(NULL, HINT_UPDATE_WINDOW, NULL);
	pPrintSheetView->UpdateWindow();

	CPrintSheetViewStrippingData* pPrintSheetViewStrippingData = (CPrintSheetViewStrippingData*)m_wndSplitter2.GetPane(0, 2);
	pPrintSheetViewStrippingData->OnInitialUpdate();
	pPrintSheetViewStrippingData->Invalidate();
	pPrintSheetViewStrippingData->UpdateWindow();

	CProdDataPrintToolsView* pProdDataPrintToolsView = (CProdDataPrintToolsView*)m_wndSplitter2.GetPane(0, 4);
	pProdDataPrintToolsView->OnInitialUpdate();
	pProdDataPrintToolsView->Invalidate();
	pProdDataPrintToolsView->UpdateWindow();

	return TRUE;
}

BOOL CNewPrintSheetFrame::SetupViewAllSheets(BOOL /*bResetAnyway*/)
{
	m_nViewSide		 = ALLSIDES;
	m_bShowExposures = m_bShowColorants = FALSE;
	m_bShowGripper	 = TRUE;
	m_bShowPrintMenu = FALSE;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CDummyView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter5.DeleteView(0, 0);
	m_wndSplitter5.CreateView(0, 0, RUNTIME_CLASS(CPrintComponentsView),	CSize(m_browserInfo.m_nComponentsViewWidth, 0), &context);
	CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)m_wndSplitter5.GetPane(0, 0);
	pPrintComponentsView->OnInitialUpdate();
	pPrintComponentsView->OnUpdate(NULL, 0, NULL);

	m_wndSplitter5.DeleteView(1, 0);
	m_wndSplitter5.CreateView(1, 0, RUNTIME_CLASS(CPrintComponentsDetailView),	CSize(1, 0), &context);
	CPrintComponentsDetailView* pPrintComponentsDetailView = (CPrintComponentsDetailView*)m_wndSplitter5.GetPane(1, 0);
	pPrintComponentsDetailView->OnInitialUpdate();
	pPrintComponentsDetailView->OnUpdate(NULL, 0, NULL);

	pPrintComponentsView->ResizeDetailsView();
	pPrintComponentsView->UpdateWindow();

	m_wndSplitter2.DeleteView(0, 2);
	m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CPrintSheetViewStrippingData), CSize(300, 0), &context);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)m_wndSplitter3.GetPane(0, 0);
	pPrintSheetView->SetViewMode(ID_VIEW_ALL_SHEETS);

	m_wndSplitter2.DeleteView(0, 4);
	m_wndSplitter2.CreateView(0, 4, RUNTIME_CLASS(CProdDataPrintToolsView),	CSize(0, 0), &context);

	SetupViewPrintSheetList(FALSE);

	SetActiveView(pPrintSheetView);

	RearrangeSplitters();

	pPrintSheetView->m_DisplayList.Invalidate();
	pPrintSheetView->OnUpdate(NULL, 0, NULL);
	pPrintSheetView->Invalidate();
	pPrintSheetView->UpdateWindow();

	CPrintSheetViewStrippingData* pPrintSheetViewStrippingData = (CPrintSheetViewStrippingData*)m_wndSplitter2.GetPane(0, 2);
	pPrintSheetViewStrippingData->OnInitialUpdate();
	pPrintSheetViewStrippingData->Invalidate();
	pPrintSheetViewStrippingData->UpdateWindow();

	CProdDataPrintToolsView* pProdDataPrintToolsView = (CProdDataPrintToolsView*)m_wndSplitter2.GetPane(0, 4);
	pProdDataPrintToolsView->OnInitialUpdate();
	pProdDataPrintToolsView->Invalidate();
	pProdDataPrintToolsView->UpdateWindow();

	return TRUE;
}


BOOL CNewPrintSheetFrame::SetupViewMarksSingleSheet()
{
	m_nViewSide		 = (m_nViewSide == ALLSIDES) ? FRONTSIDE : m_nViewSide;
	m_bShowExposures = 	m_bShowColorants = FALSE;
	m_bShowGripper	 = TRUE;
	m_bShowImpositionAssistent = FALSE;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CProdDataPrintView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter5.DeleteView(0, 0);
	m_wndSplitter5.CreateView(0, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);
	m_wndSplitter5.DeleteView(1, 0);
	m_wndSplitter5.CreateView(1, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	m_wndSplitter2.DeleteView(0, 2);
	m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CProdDataPrintView),	CSize(300, 0), &context);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)m_wndSplitter3.GetPane(0, 0);
	pPrintSheetView->SetViewMode(ID_VIEW_SINGLE_SHEETS);
	pPrintSheetView->m_zoomSteps.RemoveAll();

	if (m_browserInfo.m_nObjectPreviewWidth < 10)
		m_browserInfo.m_nObjectPreviewWidth = 150;
	
	m_wndSplitter2.DeleteView(0, 4);
	m_wndSplitter2.CreateView(0, 4, RUNTIME_CLASS(CPrintSheetObjectPreview), CSize(m_browserInfo.m_nObjectPreviewWidth, 0), &context);

	SetupViewPrintSheetList(FALSE);

	SetActiveView(pPrintSheetView);

	RearrangeSplitters();

	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)m_wndSplitter2.GetPane(0, 2);
	pProdDataPrintView->OnInitialUpdate();
	pProdDataPrintView->Invalidate();
	pProdDataPrintView->UpdateWindow();

	pPrintSheetView->m_DisplayList.Invalidate();
	pPrintSheetView->OnUpdate(NULL, HINT_UPDATE_WINDOW, NULL);
	pPrintSheetView->UpdateWindow();

	CPrintSheetObjectPreview* pPrintSheetObjectPreview = (CPrintSheetObjectPreview*)m_wndSplitter2.GetPane(0, 4);
	pPrintSheetObjectPreview->OnInitialUpdate();
	pPrintSheetObjectPreview->Invalidate();
	pPrintSheetObjectPreview->UpdateWindow();

	return TRUE;
}

BOOL CNewPrintSheetFrame::SetupViewMarksAllSheets()
{
	m_nViewSide		 = ALLSIDES;
	m_bShowExposures = m_bShowColorants = FALSE;
	m_bShowGripper   = TRUE;
	m_bShowImpositionAssistent = FALSE;
	m_bShowPrintMenu = FALSE;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CDummyView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter5.DeleteView(0, 0);
	m_wndSplitter5.CreateView(0, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);
	m_wndSplitter5.DeleteView(1, 0);
	m_wndSplitter5.CreateView(1, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	m_wndSplitter2.DeleteView(0, 2);
	m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CProdDataPrintView),	CSize(0, 0), &context);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)m_wndSplitter3.GetPane(0, 0);
	pPrintSheetView->SetViewMode(ID_VIEW_ALL_SHEETS);

	if (m_browserInfo.m_nObjectPreviewWidth < 10)
		m_browserInfo.m_nObjectPreviewWidth = 150;

	m_wndSplitter2.DeleteView(0, 4);
	m_wndSplitter2.CreateView(0, 4, RUNTIME_CLASS(CPrintSheetObjectPreview), CSize(m_browserInfo.m_nObjectPreviewWidth, 0), &context);

	SetupViewPrintSheetList(FALSE);

	SetActiveView(pPrintSheetView);

	RearrangeSplitters();

	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)m_wndSplitter2.GetPane(0, 2);
	pProdDataPrintView->OnInitialUpdate();
	pProdDataPrintView->Invalidate();
	pProdDataPrintView->UpdateWindow();

	pPrintSheetView->m_DisplayList.Invalidate();
	pPrintSheetView->OnUpdate(NULL, 0, NULL);
	pPrintSheetView->Invalidate();
	pPrintSheetView->UpdateWindow();

	CPrintSheetObjectPreview* pPrintSheetObjectPreview = (CPrintSheetObjectPreview*)m_wndSplitter2.GetPane(0, 4);
	pPrintSheetObjectPreview->OnInitialUpdate();
	pPrintSheetObjectPreview->Invalidate();
	pPrintSheetObjectPreview->UpdateWindow();

	return TRUE;
}

BOOL CNewPrintSheetFrame::SetupViewPrepressSingleSheet()
{
	return TRUE;
}

BOOL CNewPrintSheetFrame::SetupViewPrepressAllSheets()
{
	return TRUE;
}

BOOL CNewPrintSheetFrame::SetupViewOutputSingleSheet()
{
	m_nViewSide	 = (m_nViewSide == ALLSIDES) ? FRONTSIDE : m_nViewSide;
	m_bShowGripper = FALSE;
	m_bShowColorants = FALSE;
	m_bShowFoldSheet = TRUE;
	m_bShowCutBlocks = FALSE;
	m_bShowImpositionAssistent = FALSE;
	m_bShowExposures = m_bShowOutputMaskSettings;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CDummyView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter5.DeleteView(0, 0);
	m_wndSplitter5.CreateView(0, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);
	m_wndSplitter5.DeleteView(1, 0);
	m_wndSplitter5.CreateView(1, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	if (m_bShowOutputColorSettings)
	{
		m_wndSplitter2.DeleteView(0, 2);
		m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CPrintSheetColorSettingsView),	CSize(0, 0), &context);
	}
	else
		if (m_bShowOutputMaskSettings)
		{
			m_wndSplitter2.DeleteView(0, 2);
			m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CPrintSheetMaskSettingsView),	CSize(0, 0), &context);
		}
		else
		{
			if ( ! m_wndSplitter2.GetPane(0, 2)->IsKindOf(RUNTIME_CLASS(CDummyView)))
			{
				m_wndSplitter2.DeleteView(0, 2);
				m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);
			}
		}

	m_wndSplitter2.DeleteView(0, 4);
	m_wndSplitter2.CreateView(0, 4, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	m_wndSplitter3.DeleteView(1, 0);
	m_wndSplitter3.CreateView(1, 0, RUNTIME_CLASS(CPrintSheetTableView), CSize(0, 200), &context);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)m_wndSplitter3.GetPane(0, 0);
	pPrintSheetView->SetViewMode(ID_VIEW_SINGLE_SHEETS);
	pPrintSheetView->m_zoomSteps.RemoveAll();

	SetActiveView(pPrintSheetView);

	CPrintSheetNavigationView* pPrintSheetNavigationView = (CPrintSheetNavigationView*)m_wndSplitter2.GetPane(0, 1);
	pPrintSheetNavigationView->OnUpdate(NULL, 0, NULL);

	if (m_bShowOutputColorSettings)
	{
		CPrintSheetColorSettingsView* pPrintSheetColorSettingsView = (CPrintSheetColorSettingsView*)m_wndSplitter2.GetPane(0, 2);
		pPrintSheetColorSettingsView->OnInitialUpdate();
	}
	else
		if (m_bShowOutputMaskSettings)
		{
			CPrintSheetMaskSettingsView* pPrintSheetMaskSettingsView = (CPrintSheetMaskSettingsView*)m_wndSplitter2.GetPane(0, 2);
			pPrintSheetMaskSettingsView->OnInitialUpdate();
		}

	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)m_wndSplitter3.GetPane(1, 0);
	pPrintSheetTableView->m_DisplayList.Invalidate();
	pPrintSheetTableView->OnInitialUpdate();

	RearrangeSplitters();

	pPrintSheetView->m_DisplayList.Invalidate();
	pPrintSheetView->OnUpdate(NULL, HINT_UPDATE_WINDOW, NULL);
	pPrintSheetView->UpdateWindow();

	return TRUE;
}

BOOL CNewPrintSheetFrame::SetupViewOutputAllSheets()
{
	m_nViewSide	 = ALLSIDES;
	m_bShowGripper = FALSE;
	m_bShowColorants = FALSE;
	m_bShowFoldSheet = TRUE;
	m_bShowCutBlocks = FALSE;
	m_bShowImpositionAssistent = FALSE;
	m_bShowPrintMenu = FALSE;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CDummyView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter5.DeleteView(0, 0);
	m_wndSplitter5.CreateView(0, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);
	m_wndSplitter5.DeleteView(1, 0);
	m_wndSplitter5.CreateView(1, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	if (m_bShowOutputColorSettings)
	{
		m_wndSplitter2.DeleteView(0, 2);
		m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CPrintSheetColorSettingsView),	CSize(0, 0), &context);
	}
	else
		if (m_bShowOutputMaskSettings)
		{
			m_wndSplitter2.DeleteView(0, 2);
			m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CPrintSheetMaskSettingsView),	CSize(0, 0), &context);
		}
		else
		{
			if ( ! m_wndSplitter2.GetPane(0, 2)->IsKindOf(RUNTIME_CLASS(CDummyView)))
			{
				m_wndSplitter2.DeleteView(0, 2);
				m_wndSplitter2.CreateView(0, 2, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);
			}
		}

	m_wndSplitter2.DeleteView(0, 4);
	m_wndSplitter2.CreateView(0, 4, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	m_wndSplitter3.DeleteView(1, 0);
	m_wndSplitter3.CreateView(1, 0, RUNTIME_CLASS(CPrintSheetTableView), CSize(0, 200), &context);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)m_wndSplitter3.GetPane(0, 0);
	pPrintSheetView->SetViewMode(ID_VIEW_ALL_SHEETS);

	SetActiveView(pPrintSheetView);

	CPrintSheetNavigationView* pPrintSheetNavigationView = (CPrintSheetNavigationView*)m_wndSplitter2.GetPane(0, 1);
	pPrintSheetNavigationView->m_DisplayList.Invalidate();	// no sheet should be selected
	pPrintSheetNavigationView->OnUpdate(NULL, 0, NULL);

	if (m_bShowOutputColorSettings)
	{
		CPrintSheetColorSettingsView* pPrintSheetColorSettingsView = (CPrintSheetColorSettingsView*)m_wndSplitter2.GetPane(0, 2);
		pPrintSheetColorSettingsView->OnInitialUpdate();
	}
	else
		if (m_bShowOutputMaskSettings)
		{
			CPrintSheetMaskSettingsView* pPrintSheetMaskSettingsView = (CPrintSheetMaskSettingsView*)m_wndSplitter2.GetPane(0, 2);
			pPrintSheetMaskSettingsView->OnInitialUpdate();
		}


	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)m_wndSplitter3.GetPane(1, 0);
	pPrintSheetTableView->m_DisplayList.Invalidate();
	pPrintSheetTableView->OnInitialUpdate();

	RearrangeSplitters();

	pPrintSheetView->m_DisplayList.Invalidate();
	pPrintSheetView->OnUpdate(NULL, HINT_UPDATE_WINDOW, NULL);
	pPrintSheetView->UpdateWindow();

	return TRUE;
}

void CNewPrintSheetFrame::OnSize(UINT nType, int cx, int cy) 
{
	if (m_wndSplitter2.m_hWnd)
	{
		RearrangeSplitters();
	}

	CMDIChildWnd::OnSize(nType, cx, cy);	
}

void CNewPrintSheetFrame::RearrangeSplitters()
{
	CRect rcFrame, rcPane;
	GetClientRect(rcFrame);

	int nLeftToolViewWidth = 35;
	m_wndSplitter.SetColumnInfo(0, nLeftToolViewWidth, 10);
	m_wndSplitter.SetColumnInfo(1, rcFrame.Width() - nLeftToolViewWidth, 10);
	int nTopViewHeight = 0;
	//if (m_browserInfo.m_nShowInfo & ShowProducts)
	//	nTopViewHeight = m_browserInfo.m_nProductsViewHeight;
		
	int nHeight0 = nTopViewHeight - 4;
	int nHeight1 = 0;//((CFormView*)m_wndSplitter1.GetPane(1, 0))->GetTotalSize().cy;	// toptools view
	int nHeight2 = rcFrame.Height() - nTopViewHeight - 5 - nHeight1 - 5;	
	//m_wndSplitter1.SetRowInfo	(0, (nHeight0 >= 0) ? nHeight0 : 0, 10);
	//m_wndSplitter1.SetRowInfo	(1, (nHeight1 >= 0) ? nHeight1 : 0, 10);
	//m_wndSplitter1.SetRowInfo	(2, (nHeight2 >= 0) ? nHeight2 : 0, 10);

	int nWidth0 = 0, nWidth1 = 0, nWidth2 = 0, nWidth3 = 0, nWidth4 = 0;
	BOOL bHasHorzBar, bHasVertBar;	
	((CFormView*)m_wndSplitter5.GetPane(0, 0))->CheckScrollBars(bHasHorzBar, bHasVertBar);	// components view
	nWidth0 = (m_wndSplitter5.GetPane(0, 0)->IsKindOf(RUNTIME_CLASS(CDummyView))) ? 0 : m_browserInfo.m_nComponentsViewWidth;// + ((bHasVertBar) ? 16 : 0);
	((CFormView*)m_wndSplitter2.GetPane(0, 1))->CheckScrollBars(bHasHorzBar, bHasVertBar);	// navigation view
	nWidth1 = m_browserInfo.m_nNavigationViewWidth + ((bHasVertBar) ? 16 : 0);		
	if ( ! m_wndSplitter2.GetPane(0, 2)->IsKindOf(RUNTIME_CLASS(CDummyView)))
	{
		//((CFormView*)m_wndSplitter2.GetPane(0, 1))->CheckScrollBars(bHasHorzBar, bHasVertBar);
		CFormView* pView = ((CFormView*)m_wndSplitter2.GetPane(0, 2));					
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetViewStrippingData)))
			nWidth2 = 245;
		else
			nWidth2 = ((CFormView*)m_wndSplitter2.GetPane(0, 2))->GetTotalSize().cx;//((bHasVertBar) ? 16 : 0);
	}
	if ( ! m_wndSplitter2.GetPane(0, 4)->IsKindOf(RUNTIME_CLASS(CDummyView)))
	{
		if (m_wndSplitter2.GetPane(0, 4)->IsKindOf(RUNTIME_CLASS(CPrintSheetObjectPreview)))
			nWidth4 = m_browserInfo.m_nObjectPreviewWidth;
		else
			nWidth4 = ((CFormView*)m_wndSplitter2.GetPane(0, 4))->GetTotalSize().cx;
	}
	nWidth3 = rcFrame.Width() - nLeftToolViewWidth - 5 - nWidth4 - nWidth2 - 5 - nWidth1 - 5 - nWidth0 - 20;

	m_wndSplitter2.SetColumnInfo(0, (nWidth0  >= 0) ? nWidth0  : 0, 10);
	m_wndSplitter2.SetColumnInfo(1, (nWidth1  >= 0) ? nWidth1  : 0, 10);
	m_wndSplitter2.SetColumnInfo(2, (nWidth2  >= 0) ? nWidth2  : 0, 10);
	m_wndSplitter2.SetColumnInfo(3, (nWidth3  >= 0) ? nWidth3  : 0, 10);
	m_wndSplitter2.SetColumnInfo(4, (nWidth4  >= 0) ? nWidth4  : 0, 10);

	int nTableViewHeight = 0;
	if (m_browserInfo.m_nShowInfo & ShowPrintSheetList)
		nTableViewHeight = m_browserInfo.m_nListViewHeight;
	else
		if (m_wndSplitter3.GetPane(1, 0)->IsKindOf(RUNTIME_CLASS(CPrintSheetTableView)))
			nTableViewHeight = 200;
	nHeight1 = nTableViewHeight - 5;
	nHeight0 = nHeight2 - 5 - nTableViewHeight - 5;
	m_wndSplitter3.SetRowInfo	(0, (nHeight0 >= 0) ? nHeight0 : 0, 10);
	m_wndSplitter3.SetRowInfo	(1, (nHeight1 >= 0) ? nHeight1 : 0, 10);

	//nWidth1 = 600;
	//nWidth0 = rcFrame.Width() - nLeftToolViewWidth - nWidth1 - 10;
	//m_wndSplitter4.SetColumnInfo(0, (nWidth0  >= 0) ? nWidth0  : 0, 10);
	//m_wndSplitter4.SetColumnInfo(1, (nWidth1  >= 0) ? nWidth1  : 0, 10);

	CWnd* pWnd = m_wndSplitter5.GetPane(1, 0);
	if (pWnd)
		if (pWnd->IsKindOf(RUNTIME_CLASS(CPrintComponentsDetailView)))
			nHeight1 = ((CPrintComponentsDetailView*)m_wndSplitter5.GetPane(1, 0))->m_docSize.cy;
	nHeight0 = nHeight2 - nHeight1; 
	m_wndSplitter5.SetRowInfo(0, (nHeight0 >= 0) ? nHeight0 : 0, 10);
	m_wndSplitter5.SetRowInfo(1, (nHeight1 >= 0) ? nHeight1 : 0, 10);

	m_wndSplitter.RecalcLayout();
//	m_wndSplitter1.RecalcLayout();
	m_wndSplitter2.RecalcLayout();
	m_wndSplitter3.RecalcLayout();
//	m_wndSplitter4.RecalcLayout();
	m_wndSplitter5.RecalcLayout();
	RecalcLayout();

	OnUpdateSectionMenu();
}



// CNewPrintSheetFrame-Meldungshandler


void CNewPrintSheetFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	//CMDIChildWnd::OnUpdateFrameTitle(bAddToTitle);    // do nothing (done in CMainFrame)
	::SetWindowText(this->GetSafeHwnd(), _T(""));

	//CDocument* pDoc = GetActiveDocument();
	//if (pDoc)
	//{
	//	CString title = pDoc->GetTitle();
	//	//title += _T(" - ");
	//	//CString string;
	//	//string.LoadString(IDS_NAV_PRINTSHEETLIST);
	//	//title += string;
	//	::SetWindowText(this->GetSafeHwnd(), title);
	//}
}

void CNewPrintSheetFrame::ActivateFrame(int nCmdShow) 
{
	BOOL bPrevShowPrintSheetList = (m_browserInfo.m_nShowInfo & ShowPrintSheetList) ? TRUE : FALSE;
	BOOL bPrevShowProducts		 = (m_browserInfo.m_nShowInfo & ShowProducts)		? TRUE : FALSE;

	CMainFrame::RestoreFrameStatus(this, m_bStatusRestored, nCmdShow, &m_wndSplitter, &m_wndSplitter1, &m_wndSplitter2, &m_wndSplitter3, &m_wndSplitter4, sizeof(m_browserInfo), (LPBYTE)&m_browserInfo);

	if (m_browserInfo.m_nShowInfo & ShowPrintSheetList)
		if (m_browserInfo.m_nListViewHeight < 10)
			m_browserInfo.m_nListViewHeight = 150;
	if (bPrevShowPrintSheetList != (m_browserInfo.m_nShowInfo & ShowPrintSheetList))
		SetupViewPrintSheetList();

	CFrameWnd::ActivateFrame(nCmdShow);
}

void CNewPrintSheetFrame::OnDestroy() 
{
	CMainFrame::SaveFrameStatus(this, &m_wndSplitter, &m_wndSplitter1, &m_wndSplitter2, &m_wndSplitter3, &m_wndSplitter4, sizeof(struct s_browserInfo), (LPBYTE)&m_browserInfo);		// RestoreFrameStatus() in InitInstance()
	CFrameWnd::OnDestroy();
}

void CNewPrintSheetFrame::OnProcessPrePress()
{
	m_pDlgOutputMedia						= new CDlgPDFOutputMediaSettings;
	m_pDlgOutputMedia->m_bNoSheetSelection	= TRUE;
	m_pDlgOutputMedia->m_nWhatSheets		= 1;	// all of type
	m_pDlgOutputMedia->m_strWhatLayout		= (GetActLayout()) ? GetActLayout()->m_strLayoutName : _T("");
	m_pDlgOutputMedia->Create(IDD_PDFOUTPUT_MEDIA_SETTINGS, this);
}

void CNewPrintSheetFrame::AssignOutputProfile(const CString& strPDFTargetName)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
		return;
	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.GetLayout() == pLayout)
			rPrintSheet.m_strPDFTarget = strPDFTargetName;
	}
}

CPrintSheetView* CNewPrintSheetFrame::GetPrintSheetView()
{
	if ( (m_wndSplitter3.GetRowCount() < 0) || (m_wndSplitter3.GetColumnCount() < 0) )	// pane not existing at this moment
		return NULL;
	CView* pView = (CView*)m_wndSplitter3.GetPane(0, 0);
	if ( ! pView)
		return NULL;
	if ( ! pView->IsKindOf(RUNTIME_CLASS(CPrintSheetView)))
		return NULL;
	return (CPrintSheetView*)m_wndSplitter3.GetPane(0, 0);
}

CPrintComponentsView* CNewPrintSheetFrame::GetPrintComponentsView()
{
	if ( ! m_wndSplitter5.m_hWnd)
		return NULL;
	if ( (m_wndSplitter5.GetRowCount() < 0) || (m_wndSplitter5.GetColumnCount() < 0) )	// pane not existing at this moment
		return NULL;
	CView* pView = (CView*)m_wndSplitter5.GetPane(0, 0);
	if ( ! pView)
		return NULL;
	if ( ! pView->IsKindOf(RUNTIME_CLASS(CPrintComponentsView)))
		return NULL;
	return (CPrintComponentsView*)m_wndSplitter5.GetPane(0, 0);
}

CPrintComponentsDetailView* CNewPrintSheetFrame::GetPrintComponentsDetailView()
{
	if ( (m_wndSplitter5.GetRowCount() < 1) || (m_wndSplitter5.GetColumnCount() < 0) )	// pane not existing at this moment
		return NULL;
	CView* pView = (CView*)m_wndSplitter5.GetPane(1, 0);
	if ( ! pView)
		return NULL;
	if ( ! pView->IsKindOf(RUNTIME_CLASS(CPrintComponentsDetailView)))
		return NULL;
	return (CPrintComponentsDetailView*)m_wndSplitter5.GetPane(1, 0);
}

CPrintSheetNavigationView* CNewPrintSheetFrame::GetPrintSheetNavigationView()
{
	if ( (m_wndSplitter2.GetRowCount() < 0) || (m_wndSplitter2.GetColumnCount() < 1) )	// pane not existing at this moment
		return NULL;
	CView* pView = (CView*)m_wndSplitter2.GetPane(0, 1);
	if ( ! pView)
		return NULL;
	if ( ! pView->IsKindOf(RUNTIME_CLASS(CPrintSheetNavigationView)))
		return NULL;
	return (CPrintSheetNavigationView*)m_wndSplitter2.GetPane(0, 1);
}

CPrintSheetPlanningView* CNewPrintSheetFrame::GetPrintSheetPlanningView()
{
	return NULL;
}

CPrintSheetViewStrippingData* CNewPrintSheetFrame::GetPrintSheetViewStrippingData()
{
	if ( (m_wndSplitter2.GetRowCount() < 0) || (m_wndSplitter2.GetColumnCount() < 2) )	// pane not existing at this moment
		return NULL;
	CView* pView = (CView*)m_wndSplitter2.GetPane(0, 2);
	if ( ! pView)
		return NULL;
	if ( ! pView->IsKindOf(RUNTIME_CLASS(CPrintSheetViewStrippingData)))
		return NULL;
	return (CPrintSheetViewStrippingData*)m_wndSplitter2.GetPane(0, 2);
}

CProdDataPrintView* CNewPrintSheetFrame::GetProdDataPrintView()
{
	if ( (m_wndSplitter2.GetRowCount() < 0) || (m_wndSplitter2.GetColumnCount() < 2) )	// pane not existing at this moment
		return NULL;
	CView* pView = (CView*)m_wndSplitter2.GetPane(0, 2);
	if ( ! pView)
		return NULL;
	if ( ! pView->IsKindOf(RUNTIME_CLASS(CProdDataPrintView)))
		return NULL;
	return (CProdDataPrintView*)m_wndSplitter2.GetPane(0, 2);
}

CProdDataPrintToolsView* CNewPrintSheetFrame::GetProdDataPrintToolsView()
{
	if ( (m_wndSplitter2.GetRowCount() < 0) || (m_wndSplitter2.GetColumnCount() < 4) )	// pane not existing at this moment
		return NULL;
	CView* pView = (CView*)m_wndSplitter2.GetPane(0, 4);
	if ( ! pView)
		return NULL;
	if ( ! pView->IsKindOf(RUNTIME_CLASS(CProdDataPrintToolsView)))
		return NULL;
	return (CProdDataPrintToolsView*)m_wndSplitter2.GetPane(0, 4);
}

CProdDataPrepressToolsView* CNewPrintSheetFrame::GetProdDataPrepressToolsView()
{
	if ( (m_wndSplitter2.GetRowCount() < 0) || (m_wndSplitter2.GetColumnCount() < 4) )	// pane not existing at this moment
		return NULL;
	CView* pView = (CView*)m_wndSplitter2.GetPane(0, 4);
	if ( ! pView)
		return NULL;
	if ( ! pView->IsKindOf(RUNTIME_CLASS(CProdDataPrepressToolsView)))
		return NULL;
	return (CProdDataPrepressToolsView*)m_wndSplitter2.GetPane(0, 4);
}

CProductsView* CNewPrintSheetFrame::GetProductsView()
{
	if (m_wndSplitter4.m_hWnd)
	{
		CView* pView = (CView*)m_wndSplitter4.GetPane(0, 0);
		if ( ! pView)
			return NULL;
		if (pView->IsKindOf(RUNTIME_CLASS(CProductsView)))	
			return (CProductsView*)pView;
	}

	return NULL;
}

CPrintSheetTableView* CNewPrintSheetFrame::GetPrintSheetTableView()
{
	if (m_wndSplitter3.GetRowCount() < 1)	// pane not existing at this moment
		return NULL;
	CView* pView = (CView*)m_wndSplitter3.GetPane(1, 0);
	if ( ! pView)
		return NULL;
	if ( ! pView->IsKindOf(RUNTIME_CLASS(CPrintSheetTableView)))
		return NULL;
	return (CPrintSheetTableView*)m_wndSplitter3.GetPane(1, 0);
}

CPrintSheet* CNewPrintSheetFrame::GetActPrintSheet()
{
	CPrintSheet* pPrintSheet = NULL;
	CPrintSheetMaskSettingsView* pMaskSettingsView = (CPrintSheetMaskSettingsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMaskSettingsView));
	if (pMaskSettingsView)
		pPrintSheet = pMaskSettingsView->GetActPrintSheet();
	if (pPrintSheet)
		return pPrintSheet;

	CPrintSheetNavigationView* pView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pView)
		return pView->GetActPrintSheet();

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));	// this function is static - so use printsheetview to get frame
	if ( ! pPrintSheetView)
		return NULL;
	CNewPrintSheetFrame* pThisFrame = (CNewPrintSheetFrame*)pPrintSheetView->GetParentFrame();	

	CPrintSheetPlanningView* pPrintSheetPlanningView = pThisFrame->GetPrintSheetPlanningView();	// dont't use theApp.GetView() because we do have 2 stripping views (here and in OrderDataFrame)
	if (pPrintSheetPlanningView)
		return pPrintSheetPlanningView->GetPrintSheet();
	else
		return NULL;
	return NULL;
}

CLayout* CNewPrintSheetFrame::GetActLayout()
{
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	return (pPrintSheet) ? (CLayout*)pPrintSheet->GetLayout() : NULL;
}

CPressDevice* CNewPrintSheetFrame::GetActPressDevice()
{
	CLayout* pLayout = GetActLayout();
	return (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
}

int CNewPrintSheetFrame::GetActSight()
{
	return m_nViewSide;
}

CColorantList CNewPrintSheetFrame::GetActColorants()
{
	CPrintSheetViewStrippingData* pView = GetPrintSheetViewStrippingData();
	if ( ! pView)
		return CColorantList();

	return pView->GetActColorants();
}


/////////// update mechanism for gui status of toolbars
BOOL CNewPrintSheetFrame::PreTranslateMessage(MSG* pMsg) 
{
	CView* pView = GetProdDataPrintToolsView();
	if (pView)
		pView->UpdateDialogControls( pView, FALSE );
	pView = GetProdDataPrepressToolsView();
	if (pView)
		pView->UpdateDialogControls( pView, FALSE );

	// VK_DELETE is defined as accelarator key ...
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_DELETE))
		if ( (GetFocus() == &((CPrintSheetView*)GetActiveView())->m_InplaceEditPageNum) ||
		     (GetFocus() == &((CPrintSheetView*)GetActiveView())->m_InplaceEditBleed) ||	
		     (GetFocus() == &((CPrintSheetView*)GetActiveView())->m_InplaceEditGlobalMask) )	
			return FALSE;	// ... so if InplaceEdit has the focus, don't translate the message				
							// but let InplaceEdit receive it.

	return CFrameWnd::PreTranslateMessage(pMsg);
}
///////////


void CNewPrintSheetFrame::OnZoom() 
{
	if (m_nViewMode	!= ID_ZOOM)
	{
		m_nViewMode	  = ID_ZOOM;
		m_bHandActive = FALSE;
	}
	else
		m_nViewMode	  = ID_VIEW_SINGLE_SHEETS;
}

void CNewPrintSheetFrame::OnUpdateZoom(CCmdUI* pCmdUI)
{
	if (m_nViewMode == ID_ZOOM)
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);
}

void CNewPrintSheetFrame::OnZoomDecrease() 
{
	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pPrintSheetView)
	{
		SetCursor(theApp.m_CursorMagnifyDecrease);
		pPrintSheetView->ZoomWindow(CPoint(0,0)); 
	}
}

void CNewPrintSheetFrame::OnZoomFullview() 
{
	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pPrintSheetView)
	{
		SetCursor(theApp.m_CursorMagnifyDecrease);
		pPrintSheetView->m_zoomSteps.RemoveAll();
		pPrintSheetView->ZoomWindow(CPoint(0,0)); 
	}
}

void CNewPrintSheetFrame::OnScrollhand() 
{
	m_bHandActive = ! m_bHandActive;
	m_nViewMode	  = ID_VIEW_SINGLE_SHEETS;
}

void CNewPrintSheetFrame::OnViewAllsides() 
{
	if (m_nViewSide == ALLSIDES)
		return;
	BOOL bChangeFrameLayout = (m_nViewSide != ALLSIDES) ? TRUE : FALSE;
	m_nViewSide = ALLSIDES;
	UpdateFrameLayout(bChangeFrameLayout);
}

void CNewPrintSheetFrame::OnViewFrontside() 
{
	if (m_nViewSide == FRONTSIDE)
		return;
	BOOL bChangeFrameLayout = (m_nViewSide == ALLSIDES) ? TRUE : FALSE;
	m_nViewSide = FRONTSIDE;
	UpdateFrameLayout(bChangeFrameLayout);
}

void CNewPrintSheetFrame::OnViewBackside() 
{
	if (m_nViewSide == BACKSIDE)
		return;
	BOOL bChangeFrameLayout = (m_nViewSide == ALLSIDES) ? TRUE : FALSE;
	m_nViewSide = BACKSIDE;
	UpdateFrameLayout(bChangeFrameLayout);
}

void CNewPrintSheetFrame::OnViewBothsides() 
{
	if (m_nViewSide == BOTHSIDES)
		return;
	BOOL bChangeFrameLayout = (m_nViewSide == ALLSIDES) ? TRUE : FALSE;
	m_nViewSide = BOTHSIDES;
	UpdateFrameLayout(bChangeFrameLayout);
}

void CNewPrintSheetFrame::UpdateFrameLayout(BOOL bChangeFrameLayout)
{
	if (bChangeFrameLayout)
	{
		if (m_nViewSide == BACKSIDE)
		{
			CLayout* pLayout = GetActLayout();
			if (pLayout)
				if (pLayout->GetCurrentWorkStyle() != WORK_AND_BACK)
					m_nViewSide = FRONTSIDE;
		}

		switch(((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection)
		{
		case CMainFrame::NavSelectionPrintSheets:		bChangeFrameLayout = (m_nViewSide == ALLSIDES) ? SetupViewAllSheets(TRUE)	  : SetupViewSingleSheet(TRUE);
														break;
		case CMainFrame::NavSelectionPrintSheetMarks:	bChangeFrameLayout = (m_nViewSide == ALLSIDES) ? SetupViewMarksAllSheets()	  : SetupViewMarksSingleSheet();
														break;
		case CMainFrame::NavSelectionPrepress:			bChangeFrameLayout = (m_nViewSide == ALLSIDES) ? SetupViewPrepressAllSheets() : SetupViewPrepressSingleSheet();
														break;
		case CMainFrame::NavSelectionOutput:			bChangeFrameLayout = (m_nViewSide == ALLSIDES) ? SetupViewOutputAllSheets()   : SetupViewOutputSingleSheet();
		}
	}

	OnUpdateSectionMenu();

	if (bChangeFrameLayout)	// really changed
	{
		CPrintSheetPlanningView* pView = GetPrintSheetPlanningView();
		if (pView)
		{
			pView->Invalidate();
			pView->UpdateWindow();
		}
	}
	else
	{
		CPrintSheetView* pPrintSheetView = GetPrintSheetView();
		if (pPrintSheetView)
			pPrintSheetView->m_DisplayList.Invalidate();
		//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CProdDataPrintView),			NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview),	NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetComponentsView),	NULL, 0, NULL);
	}
}

void CNewPrintSheetFrame::OnFoldsheetCheck() 
{
	m_bShowFoldSheet = ! m_bShowFoldSheet;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CNewPrintSheetFrame::OnUpdateFoldsheetcheck(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bShowFoldSheet);
}

void CNewPrintSheetFrame::OnMeasureCheck() 
{
	m_bShowMeasure = ! m_bShowMeasure;
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CNewPrintSheetFrame::OnUpdateMeasurecheck(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bShowMeasure);
}

void CNewPrintSheetFrame::OnExposuresCheck() 
{
	m_bShowExposures = ! m_bShowExposures;

	if (m_bShowExposures)
	{
		if ( ! m_bShowPlate)
			m_bShowPlate = TRUE;
		if (m_bShowBitmap)
			m_bShowBitmap = FALSE;
	}

	OnUpdateSectionMenu();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CNewPrintSheetFrame::OnUpdateExposurescheck(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bShowExposures);
}

void CNewPrintSheetFrame::OnPlatecheck() 
{
	m_bShowPlate = ! m_bShowPlate;

	if ( ! m_bShowPlate)
	{
		if (m_bShowBitmap)
			m_bShowBitmap = FALSE;

		if (m_bShowExposures) 
			m_bShowExposures = FALSE;
	}

	//CheckTrimToolCtrls();
	OnUpdateSectionMenu();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),					NULL, 0, NULL);

	CPrintSheetViewStrippingData* pPrintSheetViewStrippingData = GetPrintSheetViewStrippingData();
	if (pPrintSheetViewStrippingData)
	{
		pPrintSheetViewStrippingData->m_layoutDataWindow.Invalidate();
		pPrintSheetViewStrippingData->m_layoutDataWindow.UpdateWindow();
	}
}

void CNewPrintSheetFrame::OnUpdatePlatecheck(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bShowPlate);
}

void CNewPrintSheetFrame::OnBitmapCheck() 
{
	m_bShowBitmap = ! m_bShowBitmap;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CNewPrintSheetFrame::OnUpdateBitmapcheck(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bShowBitmap);
}

void CNewPrintSheetFrame::OnShowCutBlocks() 
{
	m_bShowCutBlocks = ! m_bShowCutBlocks;

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CNewPrintSheetFrame::OnUpdateCutBlockscheck(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bShowCutBlocks);
}

void CNewPrintSheetFrame::OnShowBoxshapes() 
{
	m_bShowBoxShapes = ! m_bShowBoxShapes;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_screenLayout.m_bLabelsShowBoxShapes = m_bShowBoxShapes;
		pDoc->SetModifiedFlag();
	}
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CNewPrintSheetFrame::OnDefineTrim() 
{
	if (m_dlgTrimming.m_hWnd)
	{
		m_dlgTrimming.OnCancel();
		::SetCursor(theApp.m_CursorStandard);
	}
	else
	{
		m_dlgTrimming.Create(IDD_TRIMMING, this);
		m_dlgTrimming.m_fTrimWidth = m_dlgTrimming.m_fTrim1 = m_dlgTrimming.m_fTrim2 = 0.0f;

		::SetCursor(theApp.m_CursorTrim);

		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		pDoc->m_TrimOptions.m_bChannelThrough = TRUE;
		pDoc->m_TrimOptions.m_bChannelSingle  = FALSE;
		pDoc->m_TrimOptions.m_bHoriUp		  = TRUE;
		pDoc->m_TrimOptions.m_bHoriLow		  = FALSE;
		pDoc->m_TrimOptions.m_bHoriSym		  = FALSE;
		pDoc->m_TrimOptions.m_bVertLeft		  = FALSE;
		pDoc->m_TrimOptions.m_bVertRight	  = FALSE;
		pDoc->m_TrimOptions.m_bVertSym		  = TRUE;

		CView* pView = theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			((CPrintSheetView *)pView)->m_TrimChannel.OldRect = CRect(0, 0, 0, 0);
	}

	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);

	OnUpdateSectionMenu();
}

void CNewPrintSheetFrame::OnMoveObjects() 
{
	if (m_pDlgMoveObjects->m_hWnd)
		m_pDlgMoveObjects->DestroyWindow();
	else
		m_pDlgMoveObjects->Create(IDD_MOVE_OBJECTS, this);

	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CNewPrintSheetFrame::OnSwapObjects()  
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	if (pDI)
	{
		CPrintSheet* pPrintSheet1	 = (CPrintSheet*)pDI->m_pAuxData;
		int			 nComponentRefIndex1 = (int)pDI->m_pData;
		CFoldSheet*	 pFoldSheet1	 = pPrintSheet1->GetFoldSheet(nComponentRefIndex1);

		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
		if (pDI)
		{
			CPrintSheet* pPrintSheet2	 = (CPrintSheet*)pDI->m_pAuxData;
			int			 nComponentRefIndex2 = (int)pDI->m_pData;
			CFoldSheet*	 pFoldSheet2	 = pPrintSheet2->GetFoldSheet(nComponentRefIndex2);
			if (pFoldSheet1->m_strFoldSheetTypeName != pFoldSheet2->m_strFoldSheetTypeName)
			{
				AfxMessageBox(IDS_FOLDSHEET_EXCHANGE_WARNING, MB_OK);
				return;
			}
			pView->UndoSave();

			SwapFoldSheets(nComponentRefIndex1, pPrintSheet1, nComponentRefIndex2, pPrintSheet2);
		}
	}
	else
	{
		pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		if (pDI)
		{
			CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
			if ( ! pLayoutObject->IsFlatProduct())
				return;

			CPrintSheet*  pPrintSheet  = (CPrintSheet*)pDI->m_pAuxData;
			CFlatProduct& rFlatProduct = pLayoutObject->GetCurrentFlatProduct(pPrintSheet);

			CObArray rObjectPtrList1, rObjectPtrList2;
			while (pDI)
			{
				if (pDI->m_nGroup == 1)
					rObjectPtrList1.Add((CLayoutObject*)pDI->m_pData);
				else
					if (pDI->m_nGroup == 2)
						rObjectPtrList2.Add((CLayoutObject*)pDI->m_pData);
				pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			}
			pView->UndoSave();
			
			SwapFlatProducts(rObjectPtrList1, pPrintSheet, rObjectPtrList2, pPrintSheet);
		}

	}

	CImpManDoc::GetDoc()->CDocument::SetModifiedFlag();	// use base class method to prevent display list from being invalidated
														// in order to keep foldsheet selected
	theApp.OnUpdateView(RUNTIME_CLASS(CProdDataPrintView),			NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL, TRUE);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);

	CImpManDoc::GetDoc()->m_PageTemplateList.FindPrintSheetLocations();
	CImpManDoc::GetDoc()->m_PageTemplateList.ReorganizePrintSheetPlates();
	CImpManDoc::GetDoc()->m_PrintSheetList.ReorganizeColorInfos();
}

void CNewPrintSheetFrame::SwapFoldSheets(int nComponentRefIndex1, CPrintSheet* pPrintSheet1, int nComponentRefIndex2, CPrintSheet* pPrintSheet2)
{
	if (pPrintSheet1 != pPrintSheet2)
	{
		int nTemp = pPrintSheet1->m_FoldSheetLayerRefs[nComponentRefIndex1].m_nFoldSheetIndex;
		pPrintSheet1->m_FoldSheetLayerRefs[nComponentRefIndex1].m_nFoldSheetIndex = pPrintSheet2->m_FoldSheetLayerRefs[nComponentRefIndex2].m_nFoldSheetIndex;
		pPrintSheet2->m_FoldSheetLayerRefs[nComponentRefIndex2].m_nFoldSheetIndex = (short)nTemp;
	}
	else
	{
		CLayout*	 pLayout	 = pPrintSheet1->GetLayout();
		CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
		while (pPrintSheet)
		{
			int nTemp = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex1].m_nFoldSheetIndex;
			pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex1].m_nFoldSheetIndex = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex2].m_nFoldSheetIndex;
			pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex2].m_nFoldSheetIndex = (short)nTemp;
			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}
	}
}

void CNewPrintSheetFrame::SwapFlatProducts(CObArray& rObjectPtrList1, CPrintSheet* pPrintSheet1, CObArray& rObjectPtrList2, CPrintSheet* pPrintSheet2)
{
	if (pPrintSheet1 != pPrintSheet2)
		return;
	CLayout* pLayout = pPrintSheet1->GetLayout();
	if ( ! pLayout)
		return;
	if ( (rObjectPtrList1.GetSize() <= 0) || (rObjectPtrList2.GetSize() <= 0) )
		return;
	CLayoutObject* pLayoutObject = (CLayoutObject*)rObjectPtrList1[0];
	if ( ! pLayoutObject)
		return;
	CLayoutSide* pLayoutSide = pLayoutObject->GetLayoutSide();
	if ( ! pLayoutSide)
		return;

	CObArray counterPartObjectPtrList1, counterPartObjectPtrList2;
	for (int i = 0; i < rObjectPtrList1.GetSize(); i++)
	{
		pLayoutObject = (CLayoutObject*)rObjectPtrList1[i];
		counterPartObjectPtrList1.Add((pLayoutObject) ? pLayoutObject->FindCounterpartObject() : NULL);
	}
	for (int i = 0; i < rObjectPtrList2.GetSize(); i++)
	{
		pLayoutObject = (CLayoutObject*)rObjectPtrList2[i];
		counterPartObjectPtrList2.Add((pLayoutObject) ? pLayoutObject->FindCounterpartObject() : NULL);
	}

	float fLeft1, fBottom1, fRight1, fTop1, fWidth1, fHeight1;
	pLayoutSide->CalcBoundingRectObjects(&fLeft1, &fBottom1, &fRight1, &fTop1, &fWidth1, &fHeight1, FALSE, rObjectPtrList1, FALSE, pPrintSheet1);
	float fLeft2, fBottom2, fRight2, fTop2, fWidth2, fHeight2;
	pLayoutSide->CalcBoundingRectObjects(&fLeft2, &fBottom2, &fRight2, &fTop2, &fWidth2, &fHeight2, FALSE, rObjectPtrList2, FALSE, pPrintSheet2);

	float fDiffX = (fLeft2   + fWidth2  / 2.0f) - (fLeft1   + fWidth1  / 2.0f);
	float fDiffY = (fBottom2 + fHeight2 / 2.0f) - (fBottom1 + fHeight1 / 2.0f);
	for (int i = 0; i < rObjectPtrList1.GetSize(); i++)
	{
		pLayoutObject = (CLayoutObject*)rObjectPtrList1[i];
		if (pLayoutObject)
		{
			pLayoutObject->Offset(fDiffX, fDiffY);
			((CLayoutObject*)counterPartObjectPtrList1[i])->AlignToCounterPart(pLayoutObject);
		}
	}

	fDiffX = (fLeft1   + fWidth1  / 2.0f) - (fLeft2   + fWidth2  / 2.0f);
	fDiffY = (fBottom1 + fHeight1 / 2.0f) - (fBottom2 + fHeight2 / 2.0f);
	for (int i = 0; i < rObjectPtrList2.GetSize(); i++)
	{
		pLayoutObject = (CLayoutObject*)rObjectPtrList2[i];
		if (pLayoutObject)
		{
			pLayoutObject->Offset(fDiffX, fDiffY);
			((CLayoutObject*)counterPartObjectPtrList2[i])->AlignToCounterPart(pLayoutObject);
		}
	}
}

void CNewPrintSheetFrame::OnChangeFormats() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));

	if ( ! pDI)
	{
		AfxMessageBox(IDS_NO_OBJECT_SELECTED, MB_OK|MB_ICONSTOP);
		return;
	}

	CDlgChangeFormats formatsDlg;
	formatsDlg.DoModal();
}

void CNewPrintSheetFrame::OnRenameLayoutObject() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));

	pView->OnActivateLayoutObject(pDI, CPoint(pDI->m_AuxRect.left + 12, pDI->m_AuxRect.bottom - 12), 0);
}

void CNewPrintSheetFrame::OnObjectProperties() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CLayoutObject* pObject = NULL;
	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	if (pDI)
		pObject = (CLayoutObject*)pDI->m_pData;
	else
	{
		pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
		if (pDI)
		{
			CExposure* pExposure = (CExposure*)pDI->m_pData;
			pObject = (pExposure) ? pExposure->GetLayoutObject() : NULL;
		}
	}

	if ( ! m_pDlgLayoutObjectProperties->m_hWnd)
	{
		if (pObject->m_nType == CLayoutObject::Page)
			m_pDlgLayoutObjectProperties->Create(pObject, NULL, (pDI) ? (CPrintSheet*)pDI->m_pAuxData : NULL, this, FALSE, TRUE);
		else
		{
			CLayout* pLayout = pObject->GetLayout();
			if (pLayout)
			{
				CMark* pMark = pLayout->m_markList.FindMark(pObject->m_strFormatName);
				if (pMark)
				{
					if (pMark->IsAutoMark())	
					{
						HKEY hKey;
						CString strKey = theApp.m_strRegistryKey;
						if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)theApp.m_strRegistryKey, &hKey) == ERROR_SUCCESS)
						{
							CString		strAutoMarksFolder;
							const int	nValueLenght = max(_MAX_PATH, MAX_TEXT) + 2;
							TCHAR		szValue[nValueLenght];
							DWORD dwSize = sizeof(szValue);
							if (RegQueryValueEx(hKey, _T("AutoMarkFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
								strAutoMarksFolder = szValue;
							RegCloseKey(hKey);
							m_pDlgMarkSet->m_bOpenAutoMarks    = TRUE;
							m_pDlgMarkSet->m_strCurrentFolder  = strAutoMarksFolder;
							m_pDlgMarkSet->m_strCurrentMarkset = _T("");
							m_pDlgMarkSet->m_strMarkName	   = (pMark) ? pMark->m_strMarkName : _T("");
							m_pDlgMarkSet->Create(IDD_MARKSET, this);
						}
					}
					else
						m_pDlgLayoutObjectProperties->Create(pMark, (CPrintSheet*)pDI->m_pAuxData, this, 0, TRUE);
				}
				else
					m_pDlgLayoutObjectProperties->Create(pObject, NULL, (pDI) ? (CPrintSheet*)pDI->m_pAuxData : NULL, this, FALSE, TRUE);	// marks that are not in mark list must be KIM5 marks 
			}
		}
	}
}

void CNewPrintSheetFrame::OnObjectsrotateClock() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->UndoSave();

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	if (pDI)
	{
		int  nSide		= (pDI->m_AuxRect.Height() < 2) ? BACKSIDE : FRONTSIDE;
		BOOL bInnerForm = (pDI->m_AuxRect.Width()  < 2) ? TRUE	 : FALSE;
		((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->RotateFoldSheet90((int)pDI->m_pData,  TRUE, NULL, nSide, bInnerForm);	
		((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->CheckForProductType();
		((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->AnalyzeMarks();
	}

	pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	if (pDI)
	{
		CLayout* pActLayout = pDoc->GetActLayout();
		if (pActLayout)
		{
			pActLayout->RotateLayoutObjects(TRUE);
			pActLayout->CheckForProductType();
			pActLayout->AnalyzeMarks();
		}
	}

	pDoc->CDocument::SetModifiedFlag();	// use base class method to prevent display list from being invalidated
										// in order to keep foldsheet selected

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
		pNavigationView->UpdatePrintSheets(GetActLayout());
}

void CNewPrintSheetFrame::OnObjectsrotateCounterclock() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->UndoSave();

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	if (pDI)
	{
		int  nSide		= (pDI->m_AuxRect.Height() < 2) ? BACKSIDE : FRONTSIDE;
		BOOL bInnerForm = (pDI->m_AuxRect.Width()  < 2) ? TRUE	 : FALSE;
		((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->RotateFoldSheet90((int)pDI->m_pData, FALSE, NULL, nSide, bInnerForm);
		((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->CheckForProductType();
		((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->AnalyzeMarks();
	}

	pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	if (pDI)
	{
		CLayout* pActLayout = pDoc->GetActLayout();
		if (pActLayout)
		{
			pActLayout->RotateLayoutObjects(FALSE);
			pActLayout->CheckForProductType();
			pActLayout->AnalyzeMarks();
		}
	}

	pDoc->CDocument::SetModifiedFlag();	// use base class method to prevent display list from being invalidated
										// in order to keep foldsheet selected

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
		pNavigationView->UpdatePrintSheets(GetActLayout());
}

void CNewPrintSheetFrame::OnObjectsTurn() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->UndoSave();

	CLayout*	  pLayout = NULL;
	CDisplayItem* pDI	  = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	if (pDI)
	{
		pLayout = ((CPrintSheet*)pDI->m_pAuxData)->GetLayout();
		if (pLayout)
			pLayout->TurnFoldSheet((int)pDI->m_pData);
	}

	pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	if (pDI)
	{
		pLayout = ((CPrintSheet*)pDI->m_pAuxData)->GetLayout();
		if (pLayout)
			pLayout->TurnSelectedObjects();
	}

	if (pLayout)
	{
		pLayout->CheckForProductType();
		pLayout->AnalyzeMarks();
		pLayout->m_FrontSide.Invalidate();
		pLayout->m_BackSide.Invalidate();
	}
	
	pView->GetDocument()->CDocument::SetModifiedFlag();	// use base class method to prevent display list from being invalidated
														// in order to keep foldsheet selected

	theApp.OnUpdateView(RUNTIME_CLASS(CProdDataPrintView),	NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
		pNavigationView->UpdatePrintSheets(GetActLayout());

	CImpManDoc::GetDoc()->m_PageTemplateList.FindPrintSheetLocations();
	CImpManDoc::GetDoc()->m_PageTemplateList.ReorganizePrintSheetPlates();
	CImpManDoc::GetDoc()->m_PrintSheetList.ReorganizeColorInfos();
}

void CNewPrintSheetFrame::OnObjectsTumble() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->UndoSave();

	CLayout*	  pLayout = NULL;
	CDisplayItem* pDI	  = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	if (pDI)
	{
		pLayout = ((CPrintSheet*)pDI->m_pAuxData)->GetLayout();
		if (pLayout)
		{
			pLayout->TurnFoldSheet((int)pDI->m_pData);
			pLayout->RotateFoldSheet90((int)pDI->m_pData);
			pLayout->RotateFoldSheet90((int)pDI->m_pData);
		}
	}

	pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	if (pDI)
	{
		pLayout = ((CPrintSheet*)pDI->m_pAuxData)->GetLayout();
		if (pLayout)
		{
			pLayout->TurnSelectedObjects();
			pLayout->RotateLayoutObjects(FALSE);
			pLayout->RotateLayoutObjects(FALSE);
		}
	}

	if (pLayout)
	{
		pLayout->CheckForProductType();
		pLayout->AnalyzeMarks();
		pLayout->m_FrontSide.Invalidate();
		pLayout->m_BackSide.Invalidate();
	}

	pView->GetDocument()->CDocument::SetModifiedFlag();	// use base class method to prevent display list from being invalidated
														// in order to keep foldsheet selected

	theApp.OnUpdateView(RUNTIME_CLASS(CProdDataPrintView),	NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
		pNavigationView->UpdatePrintSheets(GetActLayout());

	CImpManDoc::GetDoc()->m_PageTemplateList.FindPrintSheetLocations();
	CImpManDoc::GetDoc()->m_PageTemplateList.ReorganizePrintSheetPlates();
	CImpManDoc::GetDoc()->m_PrintSheetList.ReorganizeColorInfos();
}

void CNewPrintSheetFrame::OnDeleteSelection() 
{
	CPrintSheetFrame::DeleteSelection(&m_bLayoutObjectSelected, &m_bFoldSheetSelected);
}

void CNewPrintSheetFrame::OnLockUnlockFoldSheet() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
		while (pDI)
		{
			CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
			if (pPrintSheet)
			{
				CFoldSheetLayer* pFoldSheetLayer = pPrintSheet->GetFoldSheetLayer((int)pDI->m_pData);
				if (pFoldSheetLayer)
				{
					pFoldSheetLayer->m_bPagesLocked = ! pFoldSheetLayer->m_bPagesLocked;
					pView->m_DisplayList.Invalidate();
				}
			}

			pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
		}

		pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		while (pDI)
		{
			CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
			if ( ! pLayoutObject->IsFlatProduct())
			{
				CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
				CFoldSheetLayer* pFoldSheetLayer = pPrintSheet->GetFoldSheetLayer(pLayoutObject->m_nComponentRefIndex);
				if (pFoldSheetLayer)
				{
					pFoldSheetLayer->m_bPagesLocked = ! pFoldSheetLayer->m_bPagesLocked;
					pDI->m_nState = CDisplayItem::Normal;
					pView->m_DisplayList.Invalidate();
				}
			}
			pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		}
	}

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	theApp.UpdateView(RUNTIME_CLASS(CProdDataPrintToolsView));
}

void CNewPrintSheetFrame::OnUndo() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->UndoRestore();

	m_bUndoActive = FALSE;

	//if (m_pDlgLayoutObjectProperties)
	//	if (m_pDlgLayoutObjectProperties->m_hWnd)
	//		m_pDlgLayoutObjectProperties->ReinitDialog((CLayoutObject*)NULL, (CPrintSheet*)NULL);
	//CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	//if (pMarksView)
	//{
	//	pMarksView->InitObjectList();
	//	if (pMarksView->m_dlgLayoutObjectControl)
	//		if (pMarksView->m_dlgLayoutObjectControl.m_hWnd)
	//			pMarksView->m_dlgLayoutObjectControl.ReinitDialog((CLayoutObject*)NULL, (CPrintSheet*)NULL);
	//	if (pMarksView->m_dlgObjectPreviewControl)
	//		if (pMarksView->m_dlgObjectPreviewControl.m_hWnd)
	//			pMarksView->m_dlgObjectPreviewControl.ReinitDialog((CLayoutObject*)NULL, (CPrintSheet*)NULL);
	//}

	CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
		pNavigationView->UpdatePrintSheets(NULL);
	//theApp.UpdateView(RUNTIME_CLASS(CPrintComponentsView));	
}

void CNewPrintSheetFrame::OnReleaseBleed() 
{
	CImpManDoc* pDoc	   = CImpManDoc::GetDoc();
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || ! pDoc)
		return;

	pView->UndoSave();

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	while (pDI)
	{
		pDoc->m_PrintSheetList.UnlockLocalMaskAllExposures((CExposure*)pDI->m_pData);

		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	}
    pDoc->SetModifiedFlag();
	if (pView)
   		pView->UpdateWindow();

	m_bEnableReleaseBleedButton = FALSE;

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetMaskSettingsView));
}

/////////////////////////////////////////////////////////////////////////////

void CNewPrintSheetFrame::OnSelectPrintSheetViewStrippingData()
{
	m_bShowStrippingData = TRUE;
	if (m_nViewSide == ALLSIDES)
		SetupViewAllSheets();
	else
		SetupViewSingleSheet();

	Invalidate();
	UpdateWindow();
}

void CNewPrintSheetFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	if (nState ==  WA_INACTIVE ) // If frame will be deactivated close an open DlgExposureMediaSettings dialog
	{
		if (m_pDlgOutputMedia->m_hWnd)
			m_pDlgOutputMedia->DestroyWindow();
	}

	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
}

void CNewPrintSheetFrame::OnClose()
{
	if (IsOpenMarkSetDlg())
	{
		GetMarkSetDlg()->OnClose();
		if (IsOpenMarkSetDlg())		// close cancelled
			return;
	}
	CMDIChildWnd::OnClose();
}

BOOL CNewPrintSheetFrame::IsOpenMarkSetDlg()
{
	CDlgMarkSet* pDlg = m_pDlgMarkSet;
	if (pDlg)
		if (pDlg->m_hWnd)
			return TRUE;

	return FALSE;
}

BOOL CNewPrintSheetFrame::IsOpenDlgNewMark()
{
	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_hWnd)
			if (m_pDlgLayoutObjectProperties->IsWindowVisible())
				if (m_pDlgLayoutObjectProperties->m_pMark)
					return TRUE;

	return FALSE;
}

BOOL CNewPrintSheetFrame::IsOpenDlgMaskSettings()
{
	if (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection != CMainFrame::NavSelectionOutput)
		return FALSE;

	return m_bShowOutputMaskSettings;
}

BOOL CNewPrintSheetFrame::IsOpenDlgNewMarkGeometry()
{
	if (IsOpenDlgNewMark())
	{
		if (m_pDlgLayoutObjectProperties)
			if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgCustomMarkGeometry.m_hWnd)
				if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgCustomMarkGeometry.IsWindowVisible())
					if (m_pDlgLayoutObjectProperties->IsActiveReflineControl())
						return TRUE;
		if (m_pDlgLayoutObjectProperties)
			if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgTextMarkGeometry.m_hWnd)
				if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgTextMarkGeometry.IsWindowVisible())
					if (m_pDlgLayoutObjectProperties->IsActiveReflineControl())
						return TRUE;
		if (m_pDlgLayoutObjectProperties)
			if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgBarcodeMarkGeometry.m_hWnd)
				if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgBarcodeMarkGeometry.IsWindowVisible())
					if (m_pDlgLayoutObjectProperties->IsActiveReflineControl())
						return TRUE;
		if (m_pDlgLayoutObjectProperties)
			if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgDrawingMarkGeometry.m_hWnd)
				if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgDrawingMarkGeometry.IsWindowVisible())
					if (m_pDlgLayoutObjectProperties->IsActiveReflineControl())
						return TRUE;
	}

	return FALSE;
}

BOOL CNewPrintSheetFrame::IsOpenDlgLayoutObjectGeometry()
{
	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.IsOpenDlgGeometry())
			return TRUE;

	if (m_pDlgMarkSet)
		if (m_pDlgMarkSet->m_dlgLayoutObjectControl.IsOpenDlgGeometry())
			return TRUE;

	return FALSE;
}

BOOL CNewPrintSheetFrame::IsOpenDlgPDFObjectContent()
{
	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.IsOpenDlgPDFObjectContent())
			return TRUE;

	if (m_pDlgMarkSet)
		if (m_pDlgMarkSet->m_dlgLayoutObjectControl.IsOpenDlgPDFObjectContent())
			return TRUE;

	return FALSE;
}

BOOL CNewPrintSheetFrame::IsOpenDlgObjectContent()
{
	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.IsOpenDlgObjectContent())
			return TRUE;

	if (m_pDlgMarkSet)
		if (m_pDlgMarkSet->m_dlgLayoutObjectControl.IsOpenDlgObjectContent())
			return TRUE;

	return FALSE;
}

BOOL CNewPrintSheetFrame::IsOpenDlgFoldSchemeSelector()
{
	if (m_dlgFoldSchemeSelector.m_hWnd)
		return TRUE;
	else
		return FALSE;
}

CDlgSelectFoldSchemePopup* CNewPrintSheetFrame::GetDlgFoldSchemeSelector()
{
	return &m_dlgFoldSchemeSelector;
}

CDlgComponentImposer* CNewPrintSheetFrame::GetDlgComponentImposer()
{
	CPrintSheetViewStrippingData* pView = GetPrintSheetViewStrippingData();
	if ( ! pView)
		return NULL;

	return &pView->m_layoutDataWindow.m_dlgComponentImposer;;
}

BOOL CNewPrintSheetFrame::IsOpenDlgComponentImposer()
{
	CPrintSheetViewStrippingData* pView = GetPrintSheetViewStrippingData();
	if ( ! pView)
		return NULL;

	return (pView->m_layoutDataWindow.m_dlgComponentImposer.m_hWnd) ? TRUE : FALSE;
}

BOOL CNewPrintSheetFrame::IsOpenDlgLayoutObjectProperties()
{
	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_hWnd)
			if (m_pDlgLayoutObjectProperties->IsWindowVisible())
				return TRUE;

	return FALSE;
}

CDlgLayoutObjectProperties* CNewPrintSheetFrame::GetDlgNewMark()
{
	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_hWnd)
			if (m_pDlgLayoutObjectProperties->IsWindowVisible())
				if (m_pDlgLayoutObjectProperties->m_pMark)
					return m_pDlgLayoutObjectProperties;

	return NULL;
}

CDlgMarkSet* CNewPrintSheetFrame::GetMarkSetDlg()
{
	CDlgMarkSet* pDlg = m_pDlgMarkSet;
	if (pDlg)
		if (pDlg->m_hWnd)
			return pDlg;

	return NULL;
}

CDlgLayoutObjectGeometry* CNewPrintSheetFrame::GetDlgLayoutObjectGeometry()
{
	CDlgLayoutObjectGeometry* pDlg = NULL;

	if (m_pDlgLayoutObjectProperties)
		if ( (pDlg = m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.GetDlgGeometry()) != NULL)
			return pDlg;

	if (m_pDlgMarkSet)
		if ( (pDlg = m_pDlgMarkSet->m_dlgLayoutObjectControl.GetDlgGeometry()) != NULL)
			return pDlg;

	return NULL;
}

void CNewPrintSheetFrame::SelectPrintSheet(CPrintSheet* pPrintSheet)
{
	CPrintSheetNavigationView* pView = GetPrintSheetNavigationView();
	if (pView)
	{
		pView->Invalidate();
		pView->UpdateWindow();
		pView->SetActPrintSheet(pPrintSheet);
	}
}

void CNewPrintSheetFrame::InitialUnboundAddNewPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (pDoc->m_PrintSheetList.GetCount() > 0)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetTail();
		CLayout*	 pLayout	 = rPrintSheet.GetLayout();
		if (! pLayout)
			return;
		BOOL bFindLargestPaper = FALSE;
		int	nPressDeviceIndex = pLayout->m_FrontSide.m_nPressDeviceIndex;
		if ( (nPressDeviceIndex == -1) || (nPressDeviceIndex == pDoc->m_nDefaultPressDeviceIndex) )
		{
			CDlgSelectPressDevicePopup	dlgPressDeviceSelector(this, TRUE);	// TRUE = RunModal
			dlgPressDeviceSelector.Create(IDD_SELECT_PRESSDEVICE_POPUP, this);
			dlgPressDeviceSelector.CenterWindow();
			dlgPressDeviceSelector.ShowWindow(SW_SHOW);
			int nPressDeviceIndex = dlgPressDeviceSelector.RunModalLoop();
			if (nPressDeviceIndex == IDCANCEL)
				return;
			nPressDeviceIndex *= -1;	// index returned as negativ value in order to distingued from IDCANCEL
			pDoc->SetPrintSheetPressDevice(nPressDeviceIndex, BOTHSIDES, pDoc->m_PrintSheetList.m_Layouts.FindLayout(pLayout->m_strLayoutName));
			bFindLargestPaper = TRUE;
		}
		CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
		if ( ! pPressDevice)
			return;
		CSheet sheet;
		if ( ( ! pPressDevice->FindPaper(sheet, (pLayout) ? pLayout->m_FrontSide.m_strFormatName : _T(""))) || bFindLargestPaper)
			pPressDevice->FindLargestPaper(sheet);
		pLayout->AssignSheet(sheet);
		pLayout->m_nProductType = WORK_AND_BACK;
		pDoc->m_PrintSheetList.ReNumberPrintSheets();

		if (pDoc->m_flatProducts.HasGraphics())
			m_bShowBoxShapes = TRUE;

		pDoc->SetModifiedFlag();
		pDoc->UpdateAllViews(NULL);
		CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
		if (pNavigationView)
		{
			pNavigationView->m_DisplayList.Invalidate();
			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
			pNavigationView->SetActPrintSheet(&rPrintSheet);
		}

		return;
	}

	OnAddNewPrintSheet();
}

void CNewPrintSheetFrame::OnDoImpose() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintComponentsView* pPrintComponentsView = GetPrintComponentsView();
	if ( ! pPrintComponentsView)
		return;
	CPrintSheetNavigationView* pNavigationView = GetPrintSheetNavigationView();
	if ( ! pNavigationView)
		return;

	CPrintComponent actComponent = pPrintComponentsView->GetActComponent();
	if (actComponent.IsNull())
		return;

	CDisplayItem* pDI = pPrintComponentsView->m_DisplayList.GetFirstSelectedItem();
	CRect rcDI = (pDI) ? pDI->m_BoundingRect : CRect(0,0,0,0);
	pPrintComponentsView->ClientToScreen(rcDI);

	//if (actComponent.m_nType == CPrintComponent::Flat)
	//{
	//	//if (m_dlgAddFlatProducts.m_hWnd)
	//	//	return;

	//	//CRect rcTarget; pNavigationView->GetClientRect(rcTarget); pNavigationView->ClientToScreen(rcTarget);
	//	//m_dlgAddFlatProducts.m_ptInitialPos = CPoint(rcTarget.right, rcTarget.top + 220);
	//	//m_dlgAddFlatProducts.m_nFlatProductIndex = actComponent.m_nFlatProductIndex;
	//	//m_dlgAddFlatProducts.Create(IDD_ADD_FLATPRODUCTS, this);

	//	if (m_dlgComponentImposer.m_hWnd)
	//		return;

	//	CRect rcTarget;	pPrintComponentsView->GetClientRect(rcTarget); pPrintComponentsView->ClientToScreen(rcTarget); 
	//	//CRect rcTarget; pNavigationView->GetClientRect(rcTarget); pNavigationView->ClientToScreen(rcTarget);
	//	m_dlgComponentImposer.m_ptInitialPos = CPoint((pDI) ? rcDI.left + 60 : rcTarget.right, (pDI) ? rcDI.bottom : rcTarget.top + 100);
	//	m_dlgComponentImposer.Create(IDD_COMPONENT_IMPOSER, this);
	//}
	//else
	{
		if (m_dlgFoldSchemeSelector.m_hWnd)
			return;

		CRect rcTarget;	pPrintComponentsView->GetClientRect(rcTarget); pPrintComponentsView->ClientToScreen(rcTarget); 
		//CRect rcTarget; pNavigationView->GetClientRect(rcTarget); pNavigationView->ClientToScreen(rcTarget);
		m_dlgFoldSchemeSelector.m_ptInitialPos = CPoint((pDI) ? rcDI.left + 60 : rcTarget.right, (pDI) ? rcDI.bottom : rcTarget.top + 100);
		m_dlgFoldSchemeSelector.Create(IDD_SELECT_FOLDSCHEME_POPUP, this);

		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	}
}


void CNewPrintSheetFrame::OnDoImposeAuto() 
{
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	if ( ! pPrintSheet)
		return;

	CPrintGroup& rPrintGroup = pPrintSheet->GetPrintGroup();

	CDlgOptimizationMonitor dlg;
	dlg.Create(IDD_OPTIMIZATION_MONITOR);
	dlg.OptimizeGroup(rPrintGroup);
	dlg.DestroyWindow();
}

void CNewPrintSheetFrame::OnAddNewPrintSheet()
{
	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;

	//CPrintSheetNavigationView* pNavigationView = GetPrintSheetNavigationView();
	//if ( ! pNavigationView)
	//	return;

	//CPrintingProfile printingProfile;
	//CPrintGroup*	 pPrintGroup = pNavigationView->GetActPrintGroup();
	//if ( ! pPrintGroup)
	//{
	//	CPrintGroup& rPrintGroup = (pDoc->m_printGroupList.GetSize() > 0) ? pDoc->m_printGroupList[0] : CPrintGroup();
	//}
	//else
	//{
	//	if (pPrintGroup->GetFirstPrintSheet())
	//		printingProfile = pPrintGroup->GetFirstPrintSheet()->GetPrintingProfile();
	//	else
	//	{
	//		if (pPrintGroup->m_strDefaultProfile.IsEmpty())
	//		{
	//			CPrintSheetViewStrippingData* pStrippingView = GetPrintSheetViewStrippingData();
	//			pPrintGroup->m_strDefaultProfile = (pStrippingView) ? pStrippingView->GetActPrintingProfileName() : _T("");
	//		}
	//	}
	//}

	//pDoc->m_PrintSheetList.AddBlankSheet(printingProfile, -1, NULL);//pPrintGroup);
	//if (pDoc->m_PrintSheetList.GetSize() <= 0)
	//	return;

	//CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetTail();
	//CLayout*	 pLayout	 = rPrintSheet.GetLayout();
	//if (! pLayout)
	//	return;

	//pDoc->m_PrintSheetList.ReNumberPrintSheets();

	//pDoc->SetModifiedFlag();

	//pDoc->UpdateAllViews(NULL);
	//
	//pNavigationView->m_DisplayList.Invalidate();
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	//pNavigationView->SetActPrintSheet(&rPrintSheet);
	//pNavigationView->SetActComponent(&rPrintSheet, 0);

	//if (m_dlgFoldSchemeSelector.m_hWnd)
	//{
	//	CPrintGroupComponent* pComponent = pNavigationView->GetActComponent();
	//	if ( ! pComponent)
	//		return;

	//	m_dlgFoldSchemeSelector.InitSchemes();
	//	m_dlgFoldSchemeSelector.InitData();
	//}
}

void CNewPrintSheetFrame::OnRemovePrintSheet()
{
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	if ( ! pPrintSheet)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strMsg;
	AfxFormatString1(strMsg, IDS_DELETE_PRINTSHEET_MSG, pPrintSheet->GetNumber());
	if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
		return;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	pDoc->m_PrintSheetList.RemovePrintSheet(pPrintSheet);

	pDoc->SetModifiedFlag();	

	//CPrintSheetViewStrippingData* pStrippingView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	//if (pStrippingView)
	//	pStrippingView->OnBlankSheet();

	if (IsOpenDlgComponentImposer()) 
		GetDlgComponentImposer()->InitData(FALSE);

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	theApp.UpdateView(RUNTIME_CLASS(CProductsView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),			NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),		NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),					NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetComponentsView),		NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));
}

void CNewPrintSheetFrame::OnModifyShingling() 
{ 
	if (m_dlgShinglingAssistent.m_hWnd)
		return;

	CPrintSheet* pActPrintSheet = GetActPrintSheet();
	CBoundProductPart* pProductPart = (pActPrintSheet) ? pActPrintSheet->GetFirstBoundProductPart() : NULL;
	int nProductPartIndex = (pProductPart) ? pProductPart->GetIndex() : 0;

	m_dlgShinglingAssistent.m_bRunModal = FALSE;
	m_dlgShinglingAssistent.m_bRunEmbedded = FALSE;
	m_dlgShinglingAssistent.m_nGlobalProductPartIndex = nProductPartIndex;
	m_dlgShinglingAssistent.Create(IDD_SHINGLING_ASSISTENT, this);
	m_dlgShinglingAssistent.CenterWindow();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		pView->m_DisplayList.Invalidate();
		pView->Invalidate();
		pView->UpdateWindow();
	}
}

void CNewPrintSheetFrame::OnOutputMaskSettings()
{
	m_bShowOutputMaskSettings  = ! m_bShowOutputMaskSettings;
	m_bShowOutputColorSettings = FALSE;
	
	UpdateFrameLayout(TRUE);
}

void CNewPrintSheetFrame::OnOutputColorSettings()
{
	m_bShowOutputColorSettings = ! m_bShowOutputColorSettings;
	m_bShowOutputMaskSettings  = FALSE;

	UpdateFrameLayout(TRUE);
}

void CNewPrintSheetFrame::OnAlignSection() 
{
	m_bAlignSectionActive = ! m_bAlignSectionActive;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		if (pView->m_nActiveYRefLine <= 0)
			pView->m_nActiveYRefLine = 1;
		pView->m_DisplayList.Invalidate();
		pView->Invalidate();
		pView->UpdateWindow();
	}
	//Invalidate();	// Hide or show the dotted lines for the sections
}

void CNewPrintSheetFrame::OnUpdateAlignSection(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bAlignSectionMenuActive);	
	pCmdUI->SetCheck(m_bAlignSectionActive);
}

void CNewPrintSheetFrame::OnResetSection() 
{
	CImpManDoc* pDoc = ((CImpManDoc*)GetActiveDocument());
	CLayout* pLayout = pDoc->GetActLayout();
	if (pLayout)
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			pView->UndoSave();

		if (theApp.settings.m_bFrontRearParallel)
		{
			pLayout->m_FrontSide.PositionLayoutSide();
			pLayout->m_BackSide.PositionLayoutSide();
		}
		else
		{
			switch (pDoc->GetActSight())
			{
			case BOTHSIDES: pLayout->m_FrontSide.PositionLayoutSide();
							pLayout->m_BackSide.PositionLayoutSide();
							break;
			case FRONTSIDE: pLayout->m_FrontSide.PositionLayoutSide();
							break;
			case BACKSIDE : pLayout->m_BackSide.PositionLayoutSide();
							break;
			}
		}
		pDoc->SetModifiedFlag();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	}
}
void CNewPrintSheetFrame::OnUpdateResetSection(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bAlignSectionMenuActive);	
}

void CNewPrintSheetFrame::OnUpdateSectionMenu() 
{
	m_bAlignSectionMenuActive = FALSE;
	m_bAlignSectionActive	  = FALSE;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	int nActSight = pDoc->GetActSight();
	if (nActSight != ALLSIDES)
//		if (m_bShowPlate && ! m_bShowFoldSheet && ! m_bShowExposures && ! m_bTrimToolActive)
		{
			CPressDevice* pPressDevice = pDoc->GetActPressDevice(nActSight);
			if (pPressDevice) // For BOTHSIDES we get the FRONTSIDE-Device
				if (pPressDevice->m_nPressType == CPressDevice::WebPress)
					m_bAlignSectionMenuActive = TRUE; // Enable Align-Section menu
		}
}

void CNewPrintSheetFrame::OpenImpositionAssistent()
{
	if (m_bShowImpositionAssistent)
		return;

	m_bShowFoldSheet		   = TRUE;
	m_bShowImpositionAssistent = TRUE;

	UpdateFrameLayout(TRUE);
}

void CNewPrintSheetFrame::CloseImpositionAssistent()
{
	if ( ! m_bShowImpositionAssistent)
		return;

	m_bShowImpositionAssistent = FALSE;

	UpdateFrameLayout(TRUE);
}

BOOL CNewPrintSheetFrame::PrintSheetViewIsEmpty()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;
	if (pDoc->m_PrintSheetList.GetCount() <= 0)
		return TRUE;
	if (m_nViewMode != ID_VIEW_ALL_SHEETS)
		if ( ! GetActLayout())
			return TRUE;

	return FALSE;
}

void CNewPrintSheetFrame::OnNotes() 
{
	CDlgNotes notesDlg;
	notesDlg.DoModal();
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));
}

void CNewPrintSheetFrame::OnDefineSheet()
{
	CDlgSheet sheetDlg;
	sheetDlg.DoModal();
}

void CNewPrintSheetFrame::OnTurnTumblePrintSheet() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	CLayout* pActLayout = pDoc->GetActLayout();

	if (!pActLayout)
		return;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	if (pActLayout->KindOfProduction() == WORK_AND_TURN)
	{
		pActLayout->m_BackSide.TurnAll();
		pActLayout->m_BackSide.TumbleAll();
	}
	else
	{
		pActLayout->m_BackSide.TumbleAll();
		pActLayout->m_BackSide.TurnAll();
	}

	pActLayout->CheckForProductType();

	pDoc->SetModifiedFlag();
	Invalidate();

//	UpdateMarkDialogs();
}

void CNewPrintSheetFrame::OnUpdateTurnTumble(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}
