// DlgImpositionScheme.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "DlgImpositionScheme.h"
#include "PrintSheetFrame.h"
#include "PrintSheetView.h"
#include "DlgPressDevice.h"
#include "DlgSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif	 

/////////////////////////////////////////////////////////////////////////////
// CDlgImpositionScheme dialog
 

LPOFNHOOKPROC g_lpfnDefaultHook;


#if WINVER >= 0x0600 
CDlgImpositionScheme::CDlgImpositionScheme(BOOL bOpenFileDialog, 
		LPCTSTR lpszDefExt,	LPCTSTR lpszFileName, DWORD dwFlags,
		LPCTSTR lpszFilter,	CWnd* pParentWnd, CDlgBindingAssistent* pDlg) : CFileDialog(
		bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags,
		lpszFilter,	pParentWnd, 0, FALSE)
#else
CDlgImpositionScheme::CDlgImpositionScheme(BOOL bOpenFileDialog, 
		LPCTSTR lpszDefExt,	LPCTSTR lpszFileName, DWORD dwFlags,
		LPCTSTR lpszFilter,	CWnd* pParentWnd, CDlgBindingAssistent* pDlg) : CFileDialog(
		bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags,
		lpszFilter,	pParentWnd)
#endif
{
	//{{AFX_DATA_INIT(CDlgImpositionScheme)
	m_nLayers = 1;
	m_nCurrentLayer = -1;
	m_nBlockNx = 1;
	m_nBlockNy = 1;
	m_nViewMode = -1;
	m_strNotes = _T("");
	m_nLeftPages = 0;
	m_nRightPages = 0;
	//}}AFX_DATA_INIT

	m_pDlgBindingAssistent = pDlg;

	m_ofn.Flags|= OFN_ENABLETEMPLATE|OFN_ENABLEHOOK;
	m_ofn.lpTemplateName  = MAKEINTRESOURCE(IDD_IMPOSITION_SCHEME);
	m_strInitialDir		  = CString(theApp.settings.m_szSchemeDir);
	m_ofn.lpstrInitialDir = m_strInitialDir.GetBuffer(MAX_PATH); 
//	m_ofn.lpstrTitle = lpsz_title;
	g_lpfnDefaultHook = m_ofn.lpfnHook;
	m_ofn.lpfnHook = &HookProcedure;

	m_bClickedOK  = FALSE;
	m_bStandAlone = FALSE;
	m_foldSheet.Create();

	m_printSheet.Create(-1, 0);
	//m_layout.Create(-1);
	m_pressDevice.Create();
	m_foldSheet.Create(0, 0, "");

	m_smallFont.CreateFont (14, 0, 0, 0, FW_BOLD,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_BkBrush.CreateSolidBrush(RGB(252, 220, 150));
}

CDlgImpositionScheme::~CDlgImpositionScheme()
{
	m_strInitialDir.ReleaseBuffer();
}


 
UINT_PTR APIENTRY CDlgImpositionScheme::HookProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	return g_lpfnDefaultHook(hWnd, message, wParam, lParam);
}

					    
#define MAX_LAYERS 16
#define MAX_BLOCKS 99
#define MAX_PAGES  MAX_LAYERS * (MAX_BLOCKS * MAX_BLOCKS)


void CDlgImpositionScheme::DoDataExchange(CDataExchange* pDX)
{
	CFileDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgImpositionScheme)
	DDX_Control(pDX, IDC_LEFT_PAGES_SPIN, m_LeftPagesSpin);
	DDX_Control(pDX, IDC_RIGHT_PAGES_SPIN, m_RightPagesSpin);
	DDX_Control(pDX, IDC_BLOCK_NY_SPIN, m_BlockNySpin);
	DDX_Control(pDX, IDC_BLOCK_NX_SPIN, m_BlockNxSpin);
	DDX_Control(pDX, IDC_CURRENT_LAYER, m_CurrentLayerCombo);
	DDX_Control(pDX, IDC_NUM_LAYERS_SPIN, m_nLayersSpin);
	DDX_Control(pDX, IDC_SCHEME_WINDOW, m_SchemeWindow);
	DDX_Text(pDX, IDC_NUM_LAYERS, m_nLayers);
	DDV_MinMaxUInt(pDX, m_nLayers, 1, MAX_LAYERS);
	DDX_CBIndex(pDX, IDC_CURRENT_LAYER, m_nCurrentLayer);
	DDX_Text(pDX, IDC_BLOCK_NX, m_nBlockNx);
	DDV_MinMaxUInt(pDX, m_nBlockNx, 1, MAX_BLOCKS);
	DDX_Text(pDX, IDC_BLOCK_NY, m_nBlockNy);
	DDV_MinMaxUInt(pDX, m_nBlockNy, 1, MAX_BLOCKS);
	DDX_Radio(pDX, IDC_OUTER, m_nViewMode);
	DDX_Text(pDX, IDC_SCHEME_NOTES, m_strNotes);
	DDX_Text(pDX, IDC_LEFT_PAGES, m_nLeftPages);
	DDV_MinMaxUInt(pDX, m_nLeftPages, 0, MAX_PAGES);
	DDX_Text(pDX, IDC_RIGHT_PAGES, m_nRightPages);
	DDV_MinMaxUInt(pDX, m_nRightPages, 0, MAX_PAGES);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgImpositionScheme, CFileDialog)
	//{{AFX_MSG_MAP(CDlgImpositionScheme)
	ON_BN_CLICKED(IDC_INNER, OnInner)
	ON_BN_CLICKED(IDC_OUTER, OnOuter)
	ON_BN_CLICKED(IDC_BOTH, OnBoth)
	ON_CBN_SELCHANGE(IDC_CURRENT_LAYER, OnSelchangeCurrentLayer)
	ON_BN_CLICKED(IDC_ADD_SCHEME, OnAddScheme)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	ON_BN_CLICKED(IDC_CALC_REAR, OnCalcRear)
	ON_NOTIFY(UDN_DELTAPOS, IDC_LEFT_PAGES_SPIN, OnDeltaposLeftPagesSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_RIGHT_PAGES_SPIN, OnDeltaposRightPagesSpin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// CDlgImpositionScheme message handlers

BOOL CDlgImpositionScheme::OnInitDialog() 
{
	CFileDialog::OnInitDialog();
	
	CString titleString;
	titleString.LoadString(IDS_FOLDSHEETSCHEME_TITLE);
	GetParent()->SetWindowText(titleString);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! m_bStandAlone)
	{
		if (pDoc)
			if ( ! pDoc->m_Bookblock.m_FoldSheetList.IsEmpty())
				m_foldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetHead(); 
	}
	else
	{
//		CWnd* pWnd = GetDlgItem(IDOK);
//		pWnd->ShowWindow(SW_HIDE);
	}								

	// hide some explorer controls:
	GetParent()->GetDlgItem(cmb1)->ModifyStyle(WS_VISIBLE, 0);		// file type combo
	GetParent()->GetDlgItem(stc2)->ModifyStyle(WS_VISIBLE, 0);		// corresponding label
	GetParent()->GetDlgItem(IDOK)->ModifyStyle(WS_VISIBLE, 0);		// OK button
	GetParent()->GetDlgItem(IDCANCEL)->ModifyStyle(WS_VISIBLE, 0);	// CANCEL button

	m_nLayers = m_foldSheet.m_LayerList.GetCount();
	m_nLayersSpin.SetRange(1, MAX_LAYERS);
	m_CurrentLayerCombo.ResetContent();
	CString strNum;
	for (unsigned i = 0; (i < m_nLayers) && (i < MAX_LAYERS); i++)
	{
		strNum.Format(_T("%d"), i + 1);
		m_CurrentLayerCombo.AddString((LPCTSTR)strNum);
	}
	m_nCurrentLayer = 0;
	m_strNotes = m_foldSheet.m_strNotes;

	CFoldSheetLayer& rLayer = m_foldSheet.m_LayerList.GetHead(); // get first layer
	m_nBlockNx = rLayer.m_nPagesX;
	m_nBlockNy = rLayer.m_nPagesY;
	m_BlockNxSpin.SetRange(1, MAX_BLOCKS);
	m_BlockNySpin.SetRange(1, MAX_BLOCKS);

	m_nLeftPages  = m_foldSheet.m_nPagesLeft;
	m_nRightPages = m_foldSheet.m_nPagesRight;
	m_LeftPagesSpin.SetRange(0, m_nLeftPages + m_nRightPages);
	m_RightPagesSpin.SetRange(0, m_nLeftPages + m_nRightPages);

	m_nViewMode = m_SchemeWindow.FrontSide;	

	UpdateData(FALSE);      

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CDlgImpositionScheme::OnSelchangeCurrentLayer() 
{
	if (!UpdateData(TRUE))	
		return;	// invalid entry

	CFoldSheetLayer& rLayer = GetCurrentLayer();
	m_nBlockNx = rLayer.m_nPagesX;
	m_nBlockNy = rLayer.m_nPagesY;

	UpdateData(FALSE);      

	m_SchemeWindow.Invalidate();	
	m_SchemeWindow.UpdateWindow();	
}


void CDlgImpositionScheme::OnOuter() 
{
	if (m_nViewMode == m_SchemeWindow.FrontSide)	// already FrontSide
		return;

	m_nViewMode = m_SchemeWindow.FrontSide;
	m_SchemeWindow.Invalidate();
	m_SchemeWindow.UpdateWindow();
}


void CDlgImpositionScheme::OnInner() 
{
	if (m_nViewMode == m_SchemeWindow.BackSide)	// already BackSide
		return;

	m_nViewMode = m_SchemeWindow.BackSide;
	m_SchemeWindow.Invalidate();
	m_SchemeWindow.UpdateWindow();
}


void CDlgImpositionScheme::OnBoth() 
{
	if (m_nViewMode == m_SchemeWindow.BothSides)	// already BothSides
		return;

	m_nViewMode = m_SchemeWindow.BothSides;
	m_SchemeWindow.Invalidate();
	m_SchemeWindow.UpdateWindow();
}


void CDlgImpositionScheme::OnAddScheme() 
{
	UpdateData(TRUE);

	CFileException fileException;

	CString strFileExt    = ((CFileDialog*)this)->GetFileExt();
	CString strFileTitle  = ((CFileDialog*)this)->GetFileTitle();
	CString strFolderPath = ((CFileDialog*)this)->GetFolderPath();
	strFileExt.MakeLower();
	if (strFileExt.IsEmpty() || (strFileExt != "isc"))
		strFileExt = "isc";

	CString strFullPath = strFolderPath + _T("\\") + strFileTitle + _T(".") + strFileExt;

	CFile file;
	BOOL  bFileExist = TRUE;
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
	{
		if (fileException.m_cause == CFileException::fileNotFound)
			bFileExist = FALSE;
	}
	else
		file.Close();

	if (bFileExist)
		if (AfxMessageBox(IDS_OVERWRITE_IMP_SCHEME, MB_YESNO) == IDNO)	
			return;

	if (!file.Open((LPCTSTR)strFullPath, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}

	m_foldSheet.m_strNotes	  = m_strNotes;
	m_foldSheet.m_nPagesLeft  = m_nLeftPages;
	m_foldSheet.m_nPagesRight = m_nRightPages;

	CArchive archive(&file, CArchive::store);

	POSITION layerPos = m_foldSheet.m_LayerList.GetHeadPosition();
	while (layerPos)
	{
		CFoldSheetLayer& rLayer = m_foldSheet.m_LayerList.GetNext(layerPos);		
		rLayer.m_FrontSpineArray.RemoveAll();
		rLayer.m_BackSpineArray.RemoveAll();
		for (int i = 0; i < rLayer.m_FrontPageArray.GetSize(); i++)
			if (rLayer.m_FrontPageArray[i] <= (short)m_foldSheet.m_nPagesLeft)
				rLayer.m_FrontSpineArray.Add(LEFT);
			else
				rLayer.m_FrontSpineArray.Add(RIGHT);
		for (int i = 0; i < rLayer.m_BackPageArray.GetSize(); i++)
			if (rLayer.m_BackPageArray[i] <= (short)m_foldSheet.m_nPagesLeft)
				rLayer.m_BackSpineArray.Add(LEFT);
			else
				rLayer.m_BackSpineArray.Add(RIGHT);
	}

	SerializeElements(archive, &m_foldSheet, 1);

	archive.Close();
	file.Close();
}


void CDlgImpositionScheme::OnFileNameChange()
{
	CFileException fileException;

	CString strFileExt = ((CFileDialog*)this)->GetFileExt();
	strFileExt.MakeLower();
	if (strFileExt != "isc")
		return;

	CString strFullPath = ((CFileDialog*)this)->GetPathName();

	CFile file;
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}

	CArchive archive(&file, CArchive::load);

	DestructElements(&m_foldSheet, 1);
	SerializeElements(archive, &m_foldSheet, 1);

	archive.Close();
	file.Close();

	
	m_foldSheet.m_strFoldSheetTypeName = ((CFileDialog*)this)->GetFileTitle();

	m_nLayers = m_foldSheet.m_LayerList.GetCount();
	m_CurrentLayerCombo.ResetContent();
	CString strNum;
	for (unsigned i = 0; (i < m_nLayers) && (i < MAX_LAYERS); i++)
	{
		strNum.Format(_T("%d"), i + 1);
		m_CurrentLayerCombo.AddString((LPCTSTR)strNum);
	}
	m_nCurrentLayer = 0;
	m_strNotes = m_foldSheet.m_strNotes;

	CFoldSheetLayer& rLayer = m_foldSheet.m_LayerList.GetHead(); // get first layer
	m_nBlockNx = rLayer.m_nPagesX;
	m_nBlockNy = rLayer.m_nPagesY;

	m_nLeftPages  = m_foldSheet.m_nPagesLeft;
	m_nRightPages = m_foldSheet.m_nPagesRight;
	m_LeftPagesSpin.SetRange (0, m_nLeftPages + m_nRightPages);
	m_RightPagesSpin.SetRange(0, m_nLeftPages + m_nRightPages);

	UpdateData(FALSE);
	Invalidate();
	UpdateWindow();
}


void CDlgImpositionScheme::OnOK() 
{
//	if (m_bStandAlone)
//		return;

	m_bClickedOK = TRUE;
	GetParent()->SendMessage(WM_COMMAND, IDOK);
}


void CDlgImpositionScheme::OnCancel() 
{
	GetParent()->SendMessage(WM_COMMAND, IDCANCEL);
}


void CDlgImpositionScheme::OnApplyButton() 
{
	UpdateFoldSheet();
}


void CDlgImpositionScheme::OnCalcRear() 
{
	CFoldSheetLayer& rLayer	   = GetCurrentLayer();
	unsigned		 nNumPages = rLayer.m_nPagesX * rLayer.m_nPagesY;

	for (unsigned i = 0; i < nNumPages; i++)
	{
		if (rLayer.m_FrontPageArray[i] % 2)
			rLayer.m_BackPageArray[i] = short(rLayer.m_FrontPageArray[i] + 1);
		else
			rLayer.m_BackPageArray[i] = short(rLayer.m_FrontPageArray[i] - 1);
	}

	UpdateFoldSheet();
}


void CDlgImpositionScheme::UpdateFoldSheet() 
{
	if (!UpdateData(TRUE))	
		return;	// invalid entry

	int nDiff, nSize;

	CFoldSheetLayer  FoldSheetLayerBuf;
	CFoldSheetLayer& rFoldSheetLayerBuf = FoldSheetLayerBuf;
	nDiff = m_nLayers - m_foldSheet.m_LayerList.GetCount();

	for (int i = 0; i < abs(nDiff); i++)
	{
		if (nDiff < 0)
		{	
			nSize = m_foldSheet.m_LayerList.GetCount();
			m_foldSheet.m_LayerList.RemoveTail();
		}
		else
		{
			if (!m_foldSheet.m_LayerList.IsEmpty())
			{
				FoldSheetLayerBuf = m_foldSheet.m_LayerList.GetTail();
				nSize = FoldSheetLayerBuf.m_FrontPageArray.GetSize();
				for (int j = 0; j < nSize; j++)
				{
					FoldSheetLayerBuf.m_FrontPageArray[j] = 0;
					FoldSheetLayerBuf.m_BackPageArray[j] = 0;
				}
			}
			m_foldSheet.m_LayerList.AddTail(rFoldSheetLayerBuf);
		}
	}
	m_CurrentLayerCombo.ResetContent();
	CString strNum;
	for (unsigned j = 0; (j < m_nLayers) && (j < MAX_LAYERS); j++)
	{
		strNum.Format(_T("%d"), j + 1);
		m_CurrentLayerCombo.AddString((LPCTSTR)strNum);
	}

	if ( (m_nCurrentLayer >= m_foldSheet.m_LayerList.GetCount()) || (m_nCurrentLayer < 0) )
		m_nCurrentLayer = m_foldSheet.m_LayerList.GetCount() - 1;
//	m_nCurrentLayer = m_nLayers - 1;
	m_foldSheet.SetDefaultPagesLeftRight();
	m_nLeftPages  = m_foldSheet.m_nPagesLeft;
	m_nRightPages = m_foldSheet.m_nPagesRight;
	m_LeftPagesSpin.SetRange (0, m_nLeftPages + m_nRightPages);
	m_RightPagesSpin.SetRange(0, m_nLeftPages + m_nRightPages);
	UpdateData(FALSE);
	m_SchemeWindow.Invalidate();

	CFoldSheetLayer& rLayer = GetCurrentLayer();	

	nDiff = (m_nBlockNx * m_nBlockNy) - (rLayer.m_nPagesX * rLayer.m_nPagesY);
	for (int i = 0; i < abs(nDiff); i++)
	{
		if (nDiff < 0)
		{	
			nSize = rLayer.m_FrontPageArray.GetSize();
			rLayer.m_FrontPageArray.RemoveAt(nSize - 1, 1);
			rLayer.m_BackPageArray.RemoveAt(nSize - 1, 1);
			rLayer.m_HeadPositionArray.RemoveAt(nSize - 1, 1);
			rLayer.m_FrontSpineArray.RemoveAt(nSize - 1, 1);
			rLayer.m_BackSpineArray.RemoveAt(nSize - 1, 1);
		}
		else
		{
			rLayer.m_FrontPageArray.Add(0);
			rLayer.m_BackPageArray.Add(0);
			rLayer.m_HeadPositionArray.Add(TOP);
			rLayer.m_FrontSpineArray.Add(LEFT);
			rLayer.m_BackSpineArray.Add(LEFT);
		}
	}
	rLayer.m_nPagesX = m_nBlockNx;
	rLayer.m_nPagesY = m_nBlockNy;
	if (nDiff)
	{
		m_foldSheet.SetDefaultPagesLeftRight();
		m_nLeftPages  = m_foldSheet.m_nPagesLeft;
		m_nRightPages = m_foldSheet.m_nPagesRight;
		m_LeftPagesSpin.SetRange (0, m_nLeftPages + m_nRightPages);
		m_RightPagesSpin.SetRange(0, m_nLeftPages + m_nRightPages);
		UpdateData(FALSE);
		m_SchemeWindow.Invalidate();
	}

	UpdateData(FALSE);

	m_SchemeWindow.UpdateWindow();	
}

void CDlgImpositionScheme::OnDeltaposLeftPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int nRightPages = m_RightPagesSpin.GetPos();
	if ((m_nLeftPages > 0 || pNMUpDown->iDelta > 0) && 
		(nRightPages  > 0 || pNMUpDown->iDelta < 0))	// Don't exceed the ranges
	{	//(m_nLeftPages + m_nRightPages) must have always the same value
		m_nLeftPages += pNMUpDown->iDelta;
		UpdateData(FALSE); // Set new value for m_nLeftPages
//		m_RightPagesSpin.SetPos(nRightPages - pNMUpDown->iDelta);
//		UpdateData(TRUE); // Retrieve the new value for m_nRightPages
	}
	*pResult = 0;
}
void CDlgImpositionScheme::OnDeltaposRightPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int nLeftPages = m_LeftPagesSpin.GetPos();
	if ((m_nRightPages > 0 || pNMUpDown->iDelta > 0) && 
		(nLeftPages	   > 0 || pNMUpDown->iDelta < 0))	// Don't exceed the ranges
	{	//(m_nLeftPages + m_nRightPages) must have always the same value
		m_nRightPages += pNMUpDown->iDelta;
		UpdateData(FALSE); // Set new value for m_nRightPages
//		m_LeftPagesSpin.SetPos(nLeftPages - pNMUpDown->iDelta);
//		UpdateData(TRUE); // Retrieve the new value for m_nLeftPages
	}
	*pResult = 0;
}

/////////////////////////////////////////////////////////////////////////////////
// methods regarding the binding assistent


void CDlgImpositionScheme::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CFileDialog::OnShowWindow(bShow, nStatus);
}


BOOL CDlgImpositionScheme::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	NMHDR *hdr = (NMHDR*) lParam;

	if (hdr->code == CDN_FILEOK)
	{
	}
	
	return CFileDialog::OnNotify(wParam, lParam, pResult);
}






/////////////////////////////////////////////////////////////////////////////
// CSchemeWindow

CSchemeWindow::CSchemeWindow()
{
	m_DisplayList.Create(this);
	m_DisplayList.SetHighlightColor(BLUE);

	m_bMouseButtonPressed = FALSE;
	m_FrontSheetRect = CRect(0,0,0,0);
	m_BackSheetRect = CRect(0,0,0,0);
}

CSchemeWindow::~CSchemeWindow()
{
}


BEGIN_MESSAGE_MAP(CSchemeWindow, CStatic)
	//{{AFX_MSG_MAP(CSchemeWindow)
	ON_WM_SETFOCUS()
	ON_WM_LBUTTONDOWN()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_KILLFOCUS()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSchemeWindow message handlers


void CSchemeWindow::PreSubclassWindow() 
{
	CREATE_INPLACE_EDIT_EX(InplaceEdit);

	CStatic::PreSubclassWindow();
}


void CSchemeWindow::OnPaint() 
{
	CPaintDC  dc(this);						
	CFont*	  pOldFont = dc.SelectObject(((CDlgImpositionScheme*)GetParent())->GetFont()); // use same font as the Dialog
	const int nBorder = 30;
	CRect     rect;

	GetClientRect(&rect);
	rect.bottom = dc.GetTextExtent(_T("XXX")).cy; // get height of current font

	GetClientRect(&m_FrontSheetRect);
	m_FrontSheetRect.DeflateRect(nBorder,nBorder);	
	m_BackSheetRect = m_FrontSheetRect;

	m_DisplayList.Invalidate();	// always full item registering - not time critical
								// otherwise invalidation needed when switching between front/back/both
	m_DisplayList.BeginRegisterItems(&dc);

	if (((CDlgImpositionScheme*)GetParent())->m_nViewMode == FrontSide)
		DisplaySheetSide(&dc, FrontSide);
	else
		if (((CDlgImpositionScheme*)GetParent())->m_nViewMode == BackSide)
			DisplaySheetSide(&dc, BackSide);
		else	// BothSides
		{
			m_FrontSheetRect.right/= 2;
			DisplaySheetSide(&dc, FrontSide);

			m_BackSheetRect.right/= 2;
			m_BackSheetRect.OffsetRect(m_BackSheetRect.Width() + nBorder, 0);
			DisplaySheetSide(&dc, BackSide);
		}

	m_DisplayList.EndRegisterItems(&dc);

	if (pOldFont != NULL)
		dc.SelectObject(pOldFont);
} 


void CSchemeWindow::DisplaySheetSide(CDC* pDC, int nForm) 
{
	short			 nPageIndex = 0;
	CRect			 PageRect(0,0,0,0), SheetRect;
	CString			 string;
	CFoldSheetLayer& rLayer = ((CDlgImpositionScheme*)GetParent())->GetCurrentLayer();
	int				 nBorder = 13;
	short			 *pData;

	if (nForm == FrontSide)
	{
		CalcExtends(&m_FrontSheetRect, rLayer.m_nPagesX, rLayer.m_nPagesY, &PageRect);
		SheetRect = m_FrontSheetRect;
		PageRect.OffsetRect(SheetRect.left, SheetRect.bottom - PageRect.Height());// begin at lower left
		string = theApp.settings.m_szFrontName;
	}
	else
	{
		CalcExtends(&m_BackSheetRect, rLayer.m_nPagesX, rLayer.m_nPagesY, &PageRect);
		SheetRect = m_BackSheetRect;
		PageRect.OffsetRect(SheetRect.right - PageRect.Width() - 1, SheetRect.bottom - PageRect.Height()); // begin at lower right
		string = theApp.settings.m_szBackName;
	}

	SheetRect.InflateRect(nBorder, nBorder);

	SheetRect.OffsetRect(3,3);		// draw 3D Sheet
	pDC->FillSolidRect(SheetRect, BLACK); 
	SheetRect.OffsetRect(-3,-3);
	pDC->FillSolidRect(SheetRect, WHITE); 

	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);
	pDC->TextOut(SheetRect.left + nBorder, SheetRect.bottom - pDC->GetTextExtent(string).cy, string);

	for (unsigned i = 0; i < rLayer.m_nPagesY; i++)
	{
		for (unsigned j = 0; j < rLayer.m_nPagesX; j++, nPageIndex++)
		{
			pDC->Rectangle(PageRect);

			if (nForm == FrontSide)
				pData = &rLayer.m_FrontPageArray[nPageIndex];
			else
				pData = &rLayer.m_BackPageArray[nPageIndex];
			
			string.Format(_T("%d"), *pData);
			pDC->TextOut(PageRect.CenterPoint().x - 10, PageRect.CenterPoint().y - 5, string);

			DisplayHeadPosition(pDC, PageRect, rLayer.m_HeadPositionArray[nPageIndex], nForm);

			CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, PageRect, PageRect, (void*)pData, DISPLAY_ITEM_REGISTRY(CSchemeWindow, Page)); // register item into display list
			if (pDI)
			{
				pDI->SetNoStateFeedback();	// make sure CDisplayItem::Invalidate() is not only frame
				if (pDI->m_nState != CDisplayItem::Normal)
				{
					CPen pen(PS_SOLID, 2, LIGHTBLUE);
					CPen*	pOldPen	  = pDC->SelectObject(&pen);
					CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
					CRect rcRect = PageRect;
					rcRect.left++; rcRect.top++;
					pDC->Rectangle(rcRect);
					pDC->SelectObject(pOldPen);
					pDC->SelectObject(pOldBrush);
					pen.DeleteObject();
				}
			}
																							 
			if (nForm == FrontSide)
				PageRect.OffsetRect(PageRect.Width(), 0);  // move one step rigth
			else
				PageRect.OffsetRect(-PageRect.Width(), 0); // move one step left
		}
		if (nForm == FrontSide)
			PageRect.OffsetRect(-((int)rLayer.m_nPagesX * PageRect.Width()), -PageRect.Height());// move to left sheet side and one step up 
		else
			PageRect.OffsetRect(((int)rLayer.m_nPagesX * PageRect.Width()), -PageRect.Height()); // move to rigth sheet side and one step up 
	}

	DisplayRefEdge(pDC, &((CDlgImpositionScheme*)GetParent())->m_foldSheet.m_nFoldSheetRefEdge, nForm, nBorder);
}


void CSchemeWindow::CalcExtends(CRect* pSheetRect, int nx, int ny, CRect* pRect)
{
	if ((nx == 0) || (ny == 0))
		return;

	pRect->right  = pSheetRect->Width()/nx;
	pRect->bottom = pSheetRect->Height()/ny;

	if (pRect->right > pRect->bottom)
		pRect->right = pRect->bottom;
	else
		if (pRect->right < pRect->bottom)
			pRect->bottom = pRect->right;

	pSheetRect->DeflateRect((pSheetRect->Width() - pRect->Width()*nx)/2, (pSheetRect->Height() - pRect->Height()*ny)/2);
}


void CSchemeWindow::DisplayHeadPosition(CDC* pDC, CRect& rect, int nHeadPosition, int nForm)
{
	CPoint p[3];
	int	   dx = rect.Width()/3, dy = rect.Height()/3;

	if (nForm == BackSide)
	{
		switch (nHeadPosition)
		{
		case LEFT:  nHeadPosition = RIGHT; break;
		case RIGHT: nHeadPosition = LEFT;  break;
		default: break;
		}
	}

	switch (nHeadPosition)
	{
	case TOP:
		p[0] = rect.TopLeft(); 
		p[1] = rect.CenterPoint() - CSize(0, dy);
		p[2] = rect.BottomRight() - CSize(0, rect.Height());
		break;
	case RIGHT:
		p[0] = rect.BottomRight() - CSize(0, rect.Height());
		p[1] = rect.CenterPoint() + CSize(dx, 0);
		p[2] = rect.BottomRight();
		break;
	case BOTTOM:
		p[0] = rect.BottomRight();
		p[1] = rect.CenterPoint() + CSize(0, dy);
		p[2] = rect.TopLeft() + CSize(0, rect.Height());
		break;
	case LEFT:
		p[0] = rect.TopLeft() + CSize(0, rect.Height());
		p[1] = rect.CenterPoint() - CSize(dx, 0);
		p[2] = rect.TopLeft();
		break;
	default:
		return;	// draw nothing
	}

	pDC->Polyline(p, 3);
}


void CSchemeWindow::DisplayRefEdge(CDC* pDC, unsigned* pRefEdge, int nForm, int nBorder)
{
	CPoint    p[4];
	unsigned  Edge = *pRefEdge;
	const int nEdgeLen = 12;
	CRect	  rect;

	switch (nForm)
	{
	case FrontSide: 
		rect = m_FrontSheetRect; break;
	case BackSide:
		rect = m_BackSheetRect;
		switch (*pRefEdge)
		{
		case LEFT_TOP:     Edge = RIGHT_TOP;    break;
		case RIGHT_TOP:    Edge = LEFT_TOP;     break;
		case LEFT_BOTTOM:  Edge = RIGHT_BOTTOM; break;
		case RIGHT_BOTTOM: Edge = LEFT_BOTTOM;  break;
		default: return;
		}
		break;
		default: return;
	}

	rect.InflateRect(nBorder,nBorder);	// distance of edge to pages

	switch (Edge)
	{
	case LEFT_TOP: 
		p[1] = rect.TopLeft();
		p[0] = p[1] + CSize(0, nEdgeLen);
		p[2] = p[1] + CSize(nEdgeLen, 0);
		p[3] = p[1];
		break;
	case RIGHT_TOP: 
		p[1] = rect.TopLeft() + CSize(rect.Width(), 0);;
		p[0] = p[1] - CSize(nEdgeLen, 0);
		p[2] = p[1] + CSize(0, nEdgeLen);
		p[3] = p[0];
		break;
	case RIGHT_BOTTOM: 
		p[1] = rect.BottomRight();
		p[0] = p[1] - CSize(0, nEdgeLen);
		p[2] = p[1] - CSize(nEdgeLen, 0);
		p[3] = p[0] - CSize(nEdgeLen, 0);
		break;
	case LEFT_BOTTOM: 
		p[1] = rect.BottomRight() - CSize(rect.Width(), 0);;
		p[0] = p[1] + CSize(nEdgeLen, 0);
		p[2] = p[1] - CSize(0, nEdgeLen);
		p[3] = p[2];
		break;
	default:
		return;
	}

	CPen redPen(PS_SOLID, 2, RGB(200,0,0));
	CPen* pOldPen = pDC->SelectObject(&redPen);
	pDC->Polyline(p, 3);
	pDC->SelectObject(pOldPen);

	rect = CRect(p[3], p[3] + CSize(nEdgeLen, nEdgeLen));
	m_DisplayList.RegisterItem(pDC, rect, rect, (void*)pRefEdge, DISPLAY_ITEM_REGISTRY(CSchemeWindow, RefEdge), (void*)nForm);	// register item into display list
}


BOOL CSchemeWindow::OnEraseBkgnd(CDC* pDC) 
{
	CRect rect;
	GetClientRect(rect);
	CBrush brush(RGB(144, 153, 174));
	pDC->FillRect(&rect, &brush);

	GetParent()->GetDlgItem(IDC_ADD_SCHEME)->UpdateWindow();

	return TRUE;
}


void CSchemeWindow::OnSetFocus(CWnd* pOldWnd) 
{
	CStatic::OnSetFocus(pOldWnd);

//	((CFileDialog*)GetParent()->GetParent())->SetDefID(IDC_APPLY_BUTTON); // just for taking away the default state (black border) from the OK button

	if (m_bMouseButtonPressed)
		return;

	if (pOldWnd == &m_InplaceEdit)	// SchemeWindow gets focus, because PageNumEdit has been hidden (in OnDeactivateItem)
		return;						//		in this case do nothing.
									// otherwise SchemeWindow is reached by TAB key 
									//		in this case activate first page so it can be edited
	if (!m_DisplayList.GetFirstSelectedItem())  // if nothing selected, activate first item 
	{
		CPoint point = m_DisplayList.GetFirstItem()->m_BoundingRect.CenterPoint();
		m_DisplayList.ActivateItem(*m_DisplayList.GetFirstItem(), point);
	}
}


void CSchemeWindow::OnKillFocus(CWnd* pNewWnd) 
{											   
	CStatic::OnKillFocus(pNewWnd);			   

	if (pNewWnd == &m_InplaceEdit)	// SchemeWindow looses focus, because PageNumEdit gets it
		return;						//		in this case do nothing
									// otherwise SchemeWindow was leaved by TAB key w/o active PageNumEdit
									//		in this case disable eventually selected items	
//	((CFileDialog*)GetParent()->GetParent())->SetDefID(IDC_APPLY_BUTTON);	// OK button gets back the default state - RETURN key is no longer catched
	m_DisplayList.DeselectAll();
}


void CSchemeWindow::OnKillFocusInplaceEdit() 					
{
//	if (GetParent()->GetFocus() != this)	// if SchemeWindow has not get the focus it was leaved by TAB key
											//		in this case deactivate current page
											// otherwise PageNumEdit has been hidden (in OnDeactivateItem)
											//		in this case do nothing
	{
//		((CFileDialog*)GetParent()->GetParent())->SetDefID(IDC_APPLY_BUTTON);	// OK button gets back the default state - RETURN key is no longer catched
		CDisplayItem* pDI = m_DisplayList.GetActiveItem();
		if (pDI)
		{
			m_DisplayList.DeactivateItem(*pDI);
			m_DisplayList.InvalidateItem(pDI);
		}
	}
	m_InplaceEdit.Deactivate();
}


void CSchemeWindow::OnCharInplaceEdit(UINT nChar)	// RETURN or TAB key pressed in InplaceEdit
{
	switch (nChar)
	{
	case VK_RETURN:
		{
			CDisplayItem* pDI = m_DisplayList.GetActiveItem();
			if (pDI)
			{
				m_DisplayList.DeactivateItem(*pDI);
				m_DisplayList.InvalidateItem(pDI, FALSE, FALSE);

				CDisplayItem* pNextDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CSchemeWindow, Page));
				if (!pNextDI)
					pNextDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CSchemeWindow, Page));
				if (pNextDI)
					if (!pNextDI->IsEqual(pDI))
					{
						CPoint cp = pNextDI->m_BoundingRect.CenterPoint();
						m_DisplayList.ActivateItem(*pNextDI, cp);
					}
			}
		}
		break;
	case VK_TAB:	
		{
			CDisplayItem* pDI = m_DisplayList.GetActiveItem();
			if (pDI)
			{
				m_DisplayList.DeactivateItem(*pDI);
				m_DisplayList.InvalidateItem(pDI, FALSE, FALSE);
			}
			m_InplaceEdit.Deactivate();
			if (GetKeyState(VK_SHIFT) < 0)	
				((CDlgImpositionScheme*)GetParent())->PrevDlgCtrl();
			else							
				((CDlgImpositionScheme*)GetParent())->NextDlgCtrl();
		}
		break;
	case VK_ESCAPE:	((CDlgImpositionScheme*)GetParent())->GetDlgItem(IDOK)->SetFocus(); break;	
	}
}


void CSchemeWindow::OnLButtonDown(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point); // pass message to display list

	if ((GetFocus() != this) && (GetFocus() != &m_InplaceEdit))	
	{
		m_bMouseButtonPressed = TRUE;	// let SchemeWindow know that it is reached via mouseclick
		SetFocus(); 
		m_bMouseButtonPressed = FALSE;
	}

	CStatic::OnLButtonDown(nFlags, point);
}


void CSchemeWindow::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point); // pass second click to display list

	if ((GetFocus() != this) && (GetFocus() != &m_InplaceEdit))	
	{
		m_bMouseButtonPressed = TRUE;	// let SchemeWindow know that it is reached via mouseclick
		SetFocus(); 
		m_bMouseButtonPressed = FALSE;
	}

	CStatic::OnLButtonDblClk(nFlags, point);
}


/////////////////////////////////////////////////////////////////////////////
// Messages back from display list

BOOL CSchemeWindow::OnActivatePage(CDisplayItem* pDI, CPoint point, LPARAM)	// page has been activated
{
	CFoldSheetLayer& rLayer = ((CDlgImpositionScheme*)GetParent())->GetCurrentLayer();
	int nhitPosition = pDI->HitTestInside(point);

	if (nhitPosition == CDisplayItem::hitMiddle)	///////// edit page number ///////////
	{
		CRect  rect  = pDI->m_BoundingRect;
		CPoint point = rect.CenterPoint();
		rect.left    = point.x - 15; rect.top    = point.y - 10;		  
		rect.right   = point.x + 15; rect.bottom = point.y + 8;
		CString	strPageNum;
		strPageNum.Format(_T("%d"), *((short*)(pDI->m_pData)));	
		m_InplaceEdit.Activate(rect, strPageNum, CInplaceEdit::FixedSize);
	}
	else
	{				   ///////////////// modify head position //////////////////////
		int nHead;
		int nIndex;
		CArray<short, short>& rFrontPageArray = rLayer.m_FrontPageArray;
		CArray<short, short>& rBackPageArray  = rLayer.m_BackPageArray;

		if (GetPageIndex(rBackPageArray, (short*)pDI->m_pData) != -1)	// page on BackSide clicked (ViewMode = BothSides),
		{															    // so eventually reverse left/right hit position 
			switch (nhitPosition)
			{
			case CDisplayItem::hitRight: nhitPosition = CDisplayItem::hitLeft;  break;
			case CDisplayItem::hitLeft:	 nhitPosition = CDisplayItem::hitRight; break;
			default: break;
			}
		}

		nHead = nhitPosition;

		CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CSchemeWindow, Page));
		while (pDI)
		{
			nIndex = GetPageIndex(rBackPageArray, (short*)pDI->m_pData);
			if (nIndex != -1)	// page on BackSide clicked (ViewMode = BackSide/BothSides),
			{					// corresponding head at the front side needs to be repainted as well
				if (((CDlgImpositionScheme*)GetParent())->m_nViewMode == BothSides)
					m_DisplayList.InvalidateItem(m_DisplayList.GetItemFromData((void*)&rLayer.m_FrontPageArray[nIndex]));
			}
			else
			{
				nIndex = GetPageIndex(rFrontPageArray, (short*)pDI->m_pData);
				if (((CDlgImpositionScheme*)GetParent())->m_nViewMode == BothSides)
					m_DisplayList.InvalidateItem(m_DisplayList.GetItemFromData((void*)&rLayer.m_BackPageArray[nIndex]));
			}

			if (nIndex != -1)
				rLayer.m_HeadPositionArray[nIndex] = (short)nHead;

			pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CSchemeWindow, Page));
		};

		m_DisplayList.DeselectAll();	// disabling items forces invalidation and repainting
	}

	return TRUE;
}


int CSchemeWindow::GetPageIndex(CArray<short, short>& rPageArray, short* pData) // get index for page where pData points to
{																				// needed for getting access to corresponding head position
	unsigned nSize = rPageArray.GetSize();	
	for (unsigned i = 0; i < nSize; i++)
		if (&rPageArray[i] == pData)
			return i;

	return -1;
}

 
BOOL CSchemeWindow::OnDeactivatePage(CDisplayItem* pDI, CPoint /*point*/, LPARAM)		// page has been deactivated
{
	if (GetFocus() == &m_InplaceEdit)	// page has been edited
	{
		*((short*)(pDI->m_pData)) = (short)_ttoi((LPCTSTR)m_InplaceEdit.GetString()); // save page number into layer array
		m_InplaceEdit.Deactivate();
	}

	return TRUE;
}


BOOL CSchemeWindow::OnDroppedRefEdge(CDisplayItem* pDI, CPoint point, LPARAM)		// RefEdge has been dropped
{
	unsigned nPosition;
	CPoint   cp;
	CRect	 rect;

	switch (((CDlgImpositionScheme*)GetParent())->m_nViewMode)
	{
	case FrontSide: rect = m_FrontSheetRect; break;
	case BackSide:  rect = m_BackSheetRect;  break;
	case BothSides: if ((int)pDI->m_pAuxData == BackSide) // RefEdge of inner form dropped
						rect = m_BackSheetRect;  
					else
						rect = m_FrontSheetRect;  
					break;
	default: return FALSE;
	}

	rect.InflateRect(30, 30);
	if (!rect.PtInRect(point))
		return FALSE;

	cp = rect.CenterPoint();

	if (point.x < cp.x) 
		if (point.y < cp.y)
			nPosition = LEFT_TOP;
		else
			nPosition = LEFT_BOTTOM;
	else
		if (point.y < cp.y)
			nPosition = RIGHT_TOP;
		else
			nPosition = RIGHT_BOTTOM;

	if ((int)pDI->m_pAuxData == BackSide)	
	{
		switch (nPosition)
		{
		case LEFT_TOP:     nPosition = RIGHT_TOP;    break;
		case RIGHT_TOP:    nPosition = LEFT_TOP;     break;
		case LEFT_BOTTOM:  nPosition = RIGHT_BOTTOM; break;
		case RIGHT_BOTTOM: nPosition = LEFT_BOTTOM;  break;
		default: return FALSE;
		}
	}
	
	*((unsigned*)pDI->m_pData) = nPosition;

	Invalidate();
	UpdateWindow();

	return TRUE;
}


void CSchemeWindow::OnDestroy() // ESCAPE key pressed when in PageNumEdit,
{								// so forward cancel message to FileDialog to close form.
	CStatic::OnDestroy();		// if this is not done, only the scheme window is closed
	
	GetParent()->GetParent()->SendMessage(WM_COMMAND, IDCANCEL); 

	theApp.SetMyCurrentDirectory(theApp.settings.m_szWorkDir, TRUE);
}

