#include "afxwin.h"
#if !defined(AFX_DLGSECTIONMARKCONTENT_H__61BFBB31_BBA9_11D5_88C6_0040F68C1EF7__INCLUDED_)
#define AFX_DLGSECTIONMARKCONTENT_H__61BFBB31_BBA9_11D5_88C6_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSectionMarkContent.h : Header-Datei
//

#include "MyBitmapButton.h"


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSectionMarkContent 

class CDlgSectionMarkContent : public CDialog
{
// Konstruktion
public:
	CDlgSectionMarkContent(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgSectionMarkContent();

// Dialogfelddaten
	//{{AFX_DATA(CDlgSectionMarkContent)
	enum { IDD = IDD_SECTIONMARK_CONTENT };
	CSpinButtonCtrl	m_barWidthSpin;
	CSpinButtonCtrl	m_barHeightSpin;
	CStatic	m_sectionMarkTypeBitmap;
	float	m_fBarWidth;
	float	m_fBarHeight;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl*	m_pParent;
	CDlgSystemGenMarkColorControl	m_printColorControl;
#ifdef PDF
	CDlgSelectSectionMark			m_dlgSelectSectionMark;
#endif
	CBitmap							m_bmSectionMarkType;
	int								m_nIDSectionMarkType;
	CBrush							m_hollowBrush;
	CMyBitmapButton					m_selectSectionMarkButton;

friend class CDlgSelectSectionMark;	// give access to protected members

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgSectionMarkContent)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementierung
public:
	void LoadData();
	void SaveData();
	void Apply();
	void UpdateView();

protected:
	void UpdateSectionMarkType(int nIDSectionMarkType);

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgSectionMarkContent)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelectSectionMark();
	afx_msg void OnDeltaposSectionmarkHeightspin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSectionmarkWidthspin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGSECTIONMARKCONTENT_H__61BFBB31_BBA9_11D5_88C6_0040F68C1EF7__INCLUDED_
