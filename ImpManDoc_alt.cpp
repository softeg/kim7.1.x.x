// ImpManDoc.cpp : implementation of the CImpManDoc class
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "MainFrm.h"
#include "PageListFrame.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "PrintSheetFrame.h"
#include "NewPrintSheetFrame.h"
#include "InplaceEdit.h"	// needed by PrintSheetView.h
#include "InplaceButton.h"
#include "ProductDataView.h"
#include "BookBlockView.h"
#include "BookBlockProdDataView.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PrintSheetTableView.h"
#include "PageListView.h"
#include "PageListTableView.h"
#include "PageSourceListView.h"
#ifdef PDF
#include "PDFReader.h"
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"
#include "spromeps.h"
#endif
#include "PageListDetailView.h"
#include "ThemedTabCtrl.h"
#include "PostScriptParser.h"
#include "DlgEditTempString.h"
#include "DlgPrintColorProps.h"
#include "io.h"	
#include "DlgTrimboxWarning.h"
#include "GraphicComponent.h"
#include "DlgSheet.h"
#include "PrintSheetColorSettingsView.h"
#include "PrintSheetMaskSettingsView.h"
#include "PrintSheetPlanningView.h"
#include "PrintComponentsView.h"
#include "OrderDataView.h"

#include "CntrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImpManDoc


IMPLEMENT_SERIAL(CImpManDoc, COleDocument, VERSIONABLE_SCHEMA | 7)

BEGIN_MESSAGE_MAP(CImpManDoc, COleDocument)
	//{{AFX_MSG_MAP(CImpManDoc)
	ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_FILE_CLOSE, OnFileClose)
	//}}AFX_MSG_MAP
	// Enable default OLE container implementation
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, COleDocument::OnUpdatePasteMenu)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE_LINK, COleDocument::OnUpdatePasteLinkMenu)
	ON_UPDATE_COMMAND_UI(ID_OLE_EDIT_CONVERT, COleDocument::OnUpdateObjectVerbMenu)
	ON_COMMAND(ID_OLE_EDIT_CONVERT, COleDocument::OnEditConvert)
	ON_UPDATE_COMMAND_UI(ID_OLE_EDIT_LINKS, COleDocument::OnUpdateEditLinksMenu)
	ON_COMMAND(ID_OLE_EDIT_LINKS, COleDocument::OnEditLinks)
	ON_UPDATE_COMMAND_UI(ID_OLE_VERB_FIRST, COleDocument::OnUpdateObjectVerbMenu)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImpManDoc construction/destruction

CImpManDoc::CImpManDoc()
{
	// Use OLE compound files
	EnableCompoundFile();

	m_pWndMain = NULL;

	m_nDefaultPressDeviceIndex = -1;
	m_nDongleStatus			   = theApp.m_nDongleStatus;
	// TODO: add one-time construction code here
}

CImpManDoc::~CImpManDoc()
{
}

BOOL CImpManDoc::OnNewDocument()
{
	m_pWndMain = theApp.m_pMainWnd;

	if (!COleDocument::OnNewDocument())
		return FALSE;

	return TRUE;
}

BOOL CImpManDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	m_pWndMain = theApp.m_pMainWnd;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();	// If we have a document at this stage, we are opening via MRU list.
	if (pDoc)									// So for that reason we need this construct here at this position.
	{											// For FileNew and FileOpen see implementation of CImpManApp::OnFileNew()/CImpManApp::OnFileOpen()
		if ( ! pDoc->SaveModified())			// DT 12/20/99
			return FALSE;
		pDoc->OnCloseDocument();	
	}								

	if (!COleDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
		if ( (theApp.m_nDongleStatus == CImpManApp::StandardDongle)		|| (theApp.m_nDongleStatus == CImpManApp::AutoDongle) ||
			 (theApp.m_nDongleStatus == CImpManApp::SmallFormatDongle)	|| (theApp.m_nDongleStatus == CImpManApp::NewsDongle) )
		{
			if (m_nDongleStatus == CImpManApp::NoDongle)
				AfxMessageBox(IDS_DEMO_TEMPLATE_WARNING, MB_OK);
			else
				if (m_nDongleStatus == CImpManApp::ClientDongle)
					AfxMessageBox(IDS_CLIENT_TEMPLATE_WARNING, MB_OK);
		}
		else
			if (theApp.m_nDongleStatus == CImpManApp::ClientDongle)
				if (m_nDongleStatus == CImpManApp::NoDongle)
					AfxMessageBox(IDS_DEMO_TEMPLATE_WARNING, MB_OK);

	UpdateDemoStatusBarPane(this);

	return TRUE;
}

void CImpManDoc::OnCloseDocument() 
{
	CMainFrame* pMainFrm = (CMainFrame*)AfxGetMainWnd();
	if (pMainFrm)
		pMainFrm->m_wndStatusBar.m_pDoc = NULL;
	
	COleDocument::OnCloseDocument();
}

void CImpManDoc::DeleteContents()  
{
	m_PageSourceList.RemoveAll();				// this function needs to be checked before release
	m_MarkSourceList.RemoveAll();
	m_PageTemplateList.RemoveAll();
	m_MarkTemplateList.RemoveAll();
	m_ColorDefinitionTable.RemoveAll();
	m_Bookblock.m_FoldSheetList.RemoveAll();
	m_Bookblock.m_DescriptionList.RemoveAll();
	m_PrintSheetList.RemoveAll();
	m_CutSheetList.RemoveAll();
	m_CutProgram.RemoveAll();
	m_productionData.m_paperList.RemoveAll();
	m_labelList.RemoveAll();
}

void CImpManDoc::SetModifiedFlag( BOOL bModified, CRuntimeClass *pClass)
{
	CDocument::SetModifiedFlag(bModified);

	CView* pView;
	if (pClass)
	{
		pView = theApp.CImpManApp::GetView(pClass);
		if (pView)
		{
			if (pView->IsKindOf(RUNTIME_CLASS(CProductDataView)))
			{
				((CBookBlockView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CBookBlockView)))
			{
				((CBookBlockView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CBookBlockProdDataView)))
			{
				//((CBookBlockProdDataView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPageListView)))
			{
				((CPageListView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPageListDetailView)))
			{
				((CPageListDetailView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPageListTableView)))
			{
				((CPageListTableView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPageSourceListView)))
			{
				((CPageSourceListView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetView)))
			{
				((CPrintSheetView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetTableView)))
			{
				((CPrintSheetTableView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
			{
				((CPrintSheetPlanningView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintComponentsView)))
			{
				((CPrintComponentsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetNavigationView)))
			{
				((CPrintSheetNavigationView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetColorSettingsView)))
			{
				((CPrintSheetColorSettingsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetMaskSettingsView)))
			{
				((CPrintSheetMaskSettingsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
		}
	}
	else	// if no pClass invalidate all views display lists
	{
		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CProductDataView));
		if (pView) ((CProductDataView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CBookBlockView));
		if (pView) ((CBookBlockView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPageListView));
		if (pView) ((CPageListView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPageListDetailView));
		if (pView) ((CPageListDetailView*)	pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPageListTableView));
		if (pView) ((CPageListTableView*)	pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPageSourceListView));
		if (pView) ((CPageSourceListView*)	pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView) ((CPrintSheetView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetTableView));
		if (pView) ((CPrintSheetTableView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
		if (pView) ((CPrintSheetPlanningView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintComponentsView));
		if (pView) ((CPrintComponentsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
		if (pView) ((CPrintSheetNavigationView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetColorSettingsView));
		if (pView) ((CPrintSheetColorSettingsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetMaskSettingsView));
		if (pView) ((CPrintSheetMaskSettingsView*)		pView)->m_DisplayList.Invalidate();
	}

	m_PrintSheetList.DoAutoMarks();
}


/////////////////////////////////////////////////////////////////////////////
// CImpManDoc serialization

void CImpManDoc::Serialize(CArchive& ar)
{
	m_GlobalData.Serialize(ar);

//	theApp.m_pDocWorkaround = this;		// workaround , see: COrderDataView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
	theApp.m_pActiveDoc = this;
	m_FinalProductDescription.Serialize(ar);
	theApp.m_pActiveDoc = NULL;

	theApp.m_nApplication = m_FinalProductDescription.m_nApplicationCategory;

	theApp.m_strPSFileNotFound.Empty();
	theApp.m_strContentFileNotFound.Empty();
	theApp.m_strPreviewFileNotFound.Empty();

	m_PageSourceList.Serialize(ar);
	// Check for missing files in PageSource and give a message to the user
	// Strings will be filled in "m_PageSourceList.Serialize(ar);"
	CImpManDoc::MessageIfFilesAreMissing(CLayoutObject::Page);

	m_MarkSourceList.Serialize(ar);
	// The same procedure for the MarkSource-files
	CImpManDoc::MessageIfFilesAreMissing(CLayoutObject::ControlMark);

	m_PageTemplateList.Serialize(ar);
	m_MarkTemplateList.Serialize(ar);
	m_ColorDefinitionTable.Serialize(ar);
	m_Bookblock.Serialize(ar);
	m_PrintSheetList.Serialize(ar);
	m_CutSheetList.Serialize(ar);
	m_CutProgram.Serialize(ar);

	// Calling the base class COleDocument enables serialization
	//  of the container document's COleClientItem objects.
	COleDocument::Serialize(ar);

	if (ar.IsStoring())
	{
		ar << m_nDefaultPressDeviceIndex;
	}
	else // ar.IsLoading()
	{
		ar >> m_nDefaultPressDeviceIndex;
		if (ar.GetFile() != &theApp.m_undoBuffer)
			AnalysePressDeviceIndices();
	}

	ar.SerializeClass(RUNTIME_CLASS(CImpManDoc));

	if (ar.IsLoading())
	{
		UINT nVersion = ar.GetObjectSchema();

		if (nVersion >= 5)
			m_productionData.Serialize(ar);

		if (nVersion >= 7)
			m_productPartList.Serialize(ar);
		else
			m_productPartList.CreateFromOldSchema(this);

		if (nVersion >= 6)
			m_labelList.Serialize(ar);

		if (nVersion == 1)
			ConvertOldTextmarks();

		if (nVersion < 7)
		{
			theApp.m_pActiveDoc = this;
			POSITION pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
			while (pos)
				m_PrintSheetList.m_Layouts.GetNext(pos).CheckForProductType_Old(this);
			theApp.m_pActiveDoc = NULL;
		}

		if (nVersion >= 3)
			ar >> m_nDongleStatus;

		if (nVersion >= 4)
			m_screenLayout.Serialize(ar);

		m_PageSourceList.InitializePageTemplateRefs(&m_PageTemplateList, &m_ColorDefinitionTable);
	}
	else
	{
		m_productionData.Serialize(ar);
		m_productPartList.Serialize(ar);
		m_labelList.Serialize(ar);
		ar << m_nDongleStatus;
		m_screenLayout.Serialize(ar);
	}

	theApp.m_pActiveDoc = this;
	m_PrintSheetList.m_Layouts.AnalyzeMarks();
	m_PrintSheetList.DoAutoMarks();
	theApp.m_pActiveDoc = NULL;
}

void CImpManDoc::ConvertOldTextmarks()
{
	POSITION layoutPos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (layoutPos)
	{
		CLayout& rLayout = m_PrintSheetList.m_Layouts.GetNext(layoutPos);

		// Frontside
		POSITION objectPos = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = rLayout.m_FrontSide.m_ObjectList.GetNext(objectPos);

			if (rObject.m_nType == CLayoutObject::ControlMark)
				if ( ! rObject.m_strNotes.IsEmpty())
				{
					POSITION templatePos = m_MarkTemplateList.FindIndex(rObject.m_nMarkTemplateIndex);
					if ( ! templatePos)
						continue;

					CPageTemplate& rTemplate = m_MarkTemplateList.GetAt(templatePos);
					POSITION sourcePos = m_MarkSourceList.FindIndex(rTemplate.m_ColorSeparations[0].m_nPageSourceIndex);
					if ( ! sourcePos)
						continue;

					CPageSource& rSource = m_MarkSourceList.GetAt(sourcePos);
					rSource.m_nType = CPageSource::SystemCreated;
					rSource.m_strSystemCreationInfo.Format("$TEXT %s", rObject.m_strNotes);
					rObject.m_strNotes.Empty();
				}
		}

		// Backside
		objectPos = rLayout.m_BackSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = rLayout.m_BackSide.m_ObjectList.GetNext(objectPos);

			if (rObject.m_nType == CLayoutObject::ControlMark)
				if ( ! rObject.m_strNotes.IsEmpty())
				{
					POSITION templatePos = m_MarkTemplateList.FindIndex(rObject.m_nMarkTemplateIndex);
					if ( ! templatePos)
						continue;

					CPageTemplate& rTemplate = m_MarkTemplateList.GetAt(templatePos);
					POSITION sourcePos = m_MarkSourceList.FindIndex(rTemplate.m_ColorSeparations[0].m_nPageSourceIndex);
					if ( ! sourcePos)
						continue;

					CPageSource& rSource = m_MarkSourceList.GetAt(sourcePos);
					rSource.m_nType = CPageSource::SystemCreated;
					rSource.m_strSystemCreationInfo.Format("$TEXT %s", rObject.m_strNotes);
					rObject.m_strNotes.Empty();
				}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CImpManDoc diagnostics

#ifdef _DEBUG
void CImpManDoc::AssertValid() const
{
	COleDocument::AssertValid();
}

void CImpManDoc::Dump(CDumpContext& dc) const
{
	COleDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImpManDoc commands

CImpManDoc* CImpManDoc::GetDoc()
{
	CMDIChildWnd* pChild = NULL;
	CDocument*	  pDoc	 = NULL;
	CWinApp*	  pApp	 = AfxGetApp();
	if (pApp->m_pMainWnd)
		pChild = ((CMDIFrameWnd*)(AfxGetApp()->m_pMainWnd))->MDIGetActive();

	pDoc = (pChild) ? pChild->GetActiveDocument() : NULL;
	if( ! pDoc)
		return theApp.m_pActiveDoc;

	if ( ! pDoc->IsKindOf(RUNTIME_CLASS(CImpManDoc)))
		return theApp.m_pActiveDoc;

	return (CImpManDoc*) pDoc;
}

BOOL CImpManDoc::FoldSheetExisting(CFoldSheet* pFoldSheet)
{
	POSITION pos = m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (&rFoldSheet == pFoldSheet)
			return TRUE;
	}

	return FALSE;
}

void CImpManDoc::ResetPublicFlags()
{
	POSITION pos, pos2;

	pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = m_PrintSheetList.m_Layouts.GetNext(pos);
		pos2 = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
		while (pos2)
			rLayout.m_FrontSide.m_ObjectList.GetNext(pos2).m_nPublicFlag = 0;
		pos2 = rLayout.m_BackSide.m_ObjectList.GetHeadPosition();
		while (pos2)
			rLayout.m_BackSide.m_ObjectList.GetNext(pos2).m_nPublicFlag = 0;
	}
}

void CImpManDoc::MessageIfFilesAreMissing(int ObjectType)
{
	if ( ! theApp.m_strPSFileNotFound.IsEmpty() ||  
		 ! theApp.m_strContentFileNotFound.IsEmpty() ||  
		 ! theApp.m_strPreviewFileNotFound.IsEmpty())
	{
		CString strLabel;
		if (ObjectType == CLayoutObject::Page)
			theApp.m_strTempTitle.LoadString(IDS_PAGEFILES_NOT_FOUND);
		else
			theApp.m_strTempTitle.LoadString(IDS_MARKFILES_NOT_FOUND);
		if ( ! theApp.m_strPSFileNotFound.IsEmpty())
		{
			strLabel.LoadString(IDS_PSFILE_NOT_FOUND);
			theApp.m_strTemp += strLabel;//"\r\nPostScript-Quelldateien :\r\n";
			theApp.m_strTemp += theApp.m_strPSFileNotFound;
			theApp.m_strPSFileNotFound.Empty();
			strLabel.Empty();
		}
		if ( ! theApp.m_strContentFileNotFound.IsEmpty())
		{
			strLabel.LoadString(IDS_EPSFILE_NOT_FOUND);
			theApp.m_strTemp += strLabel;//"\r\nEPS-Zieldateien im Verzeichnis :\r\n";
			theApp.m_strTemp += theApp.m_strContentFileNotFound;
			theApp.m_strContentFileNotFound.Empty();
			strLabel.Empty();
		}
		if ( ! theApp.m_strPreviewFileNotFound.IsEmpty())
		{
			strLabel.LoadString(IDS_PREVIEWFILE_NOT_FOUND);
			theApp.m_strTemp += strLabel;//"\r\nPreView-Dateien im Verzeichnis :\r\n";
			theApp.m_strTemp += theApp.m_strPreviewFileNotFound;
			theApp.m_strPreviewFileNotFound.Empty();
			strLabel.Empty();
		}

		CDlgEditTempString stringDlg;
		stringDlg.DoModal();

		theApp.m_strTemp.Empty();
		theApp.m_strTempTitle.Empty();
	}
}

// Implementation of classes belonging to document:

/////////////////////////////////////////////////////////////////////////////
// CGlobalData

IMPLEMENT_SERIAL(CGlobalData, CObject, VERSIONABLE_SCHEMA | 3)

CGlobalData::CGlobalData()
{
	m_strCreationSoftwareName = theApp.strName;//"Imposition Manager PostScript";
	m_strDocumentVersion.Format("%s (Build: %s)", theApp.strVersion, __DATE__);
	m_nCreationSoftware = 0;
	m_nMeasuringUnit	= 0;
	// CStrings has not to be initialized because they have their own constructor
}


void CGlobalData::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		m_strDocumentVersion.Format("%s (Build: %s)", theApp.strVersion, __DATE__);
		ar << m_strDocumentVersion;
		ar << m_nCreationSoftware;
		ar << m_strCreationSoftwareName;
		ar << m_strJob;
		ar << m_strJobNumber;
		ar << m_strCustomer;
		ar << m_strCustomerNumber;
		ar << m_strCreator;
		ar << m_strPhoneNumber;
		if (m_CreationDate.m_dt == 0.0)
			m_CreationDate = COleDateTime::GetCurrentTime();
		ar << m_CreationDate;
		m_LastModifiedDate = COleDateTime::GetCurrentTime();
		ar << m_LastModifiedDate;
		ar << m_strScheduledToPrint;
		ar << m_strNotes;
		ar << m_nMeasuringUnit;
		ar << m_strJobTicketFile;

		ar.SerializeClass(RUNTIME_CLASS(CGlobalData));
	}
	else
	{ 
		ar >> m_strDocumentVersion;

		UINT	nSchema   = 1;
		CString strVersion = GetVersionNumber();
		if (strVersion >= "6.0.0.0")
			nSchema = 3;  
		else
			if (strVersion > "3.6.0.3")
				nSchema = 2;  

		switch (nSchema)
		{
		case 1: ar >> m_nCreationSoftware;
				ar >> m_strCreationSoftwareName;
				ar >> m_strJob; 
				ar >> m_strCustomer;
				ar >> m_strCustomerNumber;
				ar >> m_strCreator;
				ar >> m_CreationDate;
				ar >> m_LastModifiedDate;
				ar >> m_strNotes;
				ar >> m_nMeasuringUnit;
				ar >> m_strJobTicketFile;
				m_strScheduledToPrint = "";
				m_strJobNumber = L"";
				m_strPhoneNumber = L"";
				break;
		case 2: ar >> m_nCreationSoftware;
				ar >> m_strCreationSoftwareName;
				ar >> m_strJob;
				ar >> m_strCustomer;
				ar >> m_strCustomerNumber;
				ar >> m_strCreator;
				ar >> m_CreationDate;
				ar >> m_LastModifiedDate;
				ar >> m_strScheduledToPrint;
				ar >> m_strNotes;
				ar >> m_nMeasuringUnit;
				ar >> m_strJobTicketFile;
				m_strJobNumber = L"";
				m_strPhoneNumber = L"";
				break;
		case 3: ar >> m_nCreationSoftware;
				ar >> m_strCreationSoftwareName;
				ar >> m_strJob;
				ar >> m_strJobNumber;
				ar >> m_strCustomer;
				ar >> m_strCustomerNumber;
				ar >> m_strCreator;
				ar >> m_strPhoneNumber;
				ar >> m_CreationDate;
				ar >> m_LastModifiedDate;
				ar >> m_strScheduledToPrint;
				ar >> m_strNotes;
				ar >> m_nMeasuringUnit;
				ar >> m_strJobTicketFile;
				break;
		}

		if (strVersion > "3.0.0.1")
			ar.SerializeClass(RUNTIME_CLASS(CGlobalData));
		else
			AfxMessageBox("Diese Datei ist mit einer �lteren Version erstellt\nworden und kann nicht geladen werden!!" ,MB_OK | MB_ICONSTOP);
	}
}

CString CGlobalData::GetVersionNumber()
{
	return m_strDocumentVersion.Left(7);
}

void CGlobalData::Draw(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	CFont	smallFont, boldFont;
	smallFont.CreateFont(Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	boldFont.CreateFont (Pixel2MM(pDC, 14), 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);
	CString	strOut;
	int		nTextHeight = Pixel2MM(pDC, 15);
	int		nRowSpace  = Pixel2MM(pDC, 20);
	CRect	rcTitle, rcTitle1, rcText1, rcTitle2, rcText2, rcTitle3, rcText3, rcTitle4, rcText4, rcTitle5, rcText5;
	int		nXStart	= rcFrame.left + Pixel2MM(pDC, 10);
	int		nYStart	= rcFrame.top;
	COLORREF crValueColor = RGB(50,50,50);

	CPen pen(PS_SOLID, Pixel2MM(pDC, 1), RGB(70,125,245));
	CPen* pOldPen = pDC->SelectObject(&pen);

	rcTitle.left	= nXStart;		 rcTitle.top	= nYStart;
	rcTitle.right   = rcFrame.right; rcTitle.bottom = rcTitle.top + Pixel2MM(pDC, 20);
	
	nYStart += Pixel2MM(pDC, 25);

	rcTitle1.left	= nXStart;  rcTitle1.top = nYStart;
	rcTitle1.right	= rcTitle1.left + Pixel2MM(pDC, 80); rcTitle1.bottom = rcTitle1.top + nTextHeight;
	rcText1 = rcTitle1; rcText1.left = rcTitle1.right + Pixel2MM(pDC, 5); rcText1.right = rcText1.left + Pixel2MM(pDC, 200);

	rcTitle2.left	= rcText1.right + Pixel2MM(pDC, 20);  rcTitle2.top = nYStart;
	rcTitle2.right	= rcTitle2.left + Pixel2MM(pDC, 80); rcTitle2.bottom = rcTitle2.top + nTextHeight;
	rcText2 = rcTitle2; rcText2.left = rcTitle2.right + Pixel2MM(pDC, 5); rcText2.right = rcText2.left + Pixel2MM(pDC, 110);
	
	rcTitle3.left	= rcText2.right + Pixel2MM(pDC, 20);  rcTitle3.top = nYStart;
	rcTitle3.right	= rcTitle3.left + Pixel2MM(pDC, 100); rcTitle3.bottom = rcTitle3.top + nTextHeight;
	rcText3 = rcTitle2; rcText3.left = rcTitle3.right + Pixel2MM(pDC, 5); rcText3.right = rcText3.left + Pixel2MM(pDC, 110);
	
	rcTitle4.left	= rcText3.right + Pixel2MM(pDC, 20);  rcTitle4.top = rcTitle.top + Pixel2MM(pDC, 3);
	rcTitle4.right	= rcTitle4.left + Pixel2MM(pDC, 70); rcTitle4.bottom = rcTitle4.top + nTextHeight;
	rcText4 = rcTitle4; rcText4.left = rcTitle4.right + Pixel2MM(pDC, 5); rcText4.right = rcFrame.right;

	rcTitle5.left	= nXStart;  rcTitle5.top = nYStart;
	rcTitle5.right	= rcTitle1.left + Pixel2MM(pDC, 80); rcTitle5.bottom = rcTitle5.top + nTextHeight;
	rcText5 = rcTitle5; rcText5.left = rcTitle5.right + Pixel2MM(pDC, 5); rcText5.right = rcFrame.right;

	rcTitle.right = rcText3.right;

	if ( ! pDC->IsPrinting())
	{
		pDC->SelectObject(&boldFont);
		//strOut.LoadString(IDS_TITLE_LABEL);
		//pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
		//pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		strOut = pDoc->GetTitle();
		theApp.RemoveExtension(strOut);
		CGraphicComponent gp;
		gp.DrawTitleBar(pDC, CRect(rcFrame.left, rcFrame.top, rcFrame.right, rcTitle.bottom), L"", RGB(231,234,239), RGB(209,215,226), 90, FALSE);
		pDC->DrawText(strOut, rcTitle,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		//pDC->MoveTo(rcTitle.left, rcTitle.bottom); pDC->LineTo(rcFrame.right, rcTitle.bottom);
		nYStart = rcTitle.bottom + Pixel2MM(pDC, 5);
		if (pDisplayList)
		{
			CRect rcDI = rcTitle; rcDI.InflateRect(5, 0); rcDI.bottom -= 3;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)0, DISPLAY_ITEM_REGISTRY(COrderDataView, OrderData), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
				pDI->SetMouseOverFeedback();
		}
	}

	pDC->SelectObject(&smallFont);

	if ( ! pDC->IsPrinting())
	{
		//pDC->MoveTo(rcTitle4.left - Pixel2MM(pDC, 10), rcFrame.top + Pixel2MM(pDC, 5)); pDC->LineTo(rcTitle4.left - Pixel2MM(pDC, 10), rcFrame.bottom - Pixel2MM(pDC, 5));

		if ( ! pDoc->m_GlobalData.m_strJobTicketFile.IsEmpty())
		{
			strOut = L"Job Ticket:";
			pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
			pDC->DrawText(strOut, rcTitle4, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
			pDC->SetTextColor(crValueColor);
			pDC->DrawText(pDoc->m_GlobalData.m_strJobTicketFile, rcText4,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
			//pDC->MoveTo(rcTitle4.left - Pixel2MM(pDC, 5), rcText4.bottom + Pixel2MM(pDC, 2)); pDC->LineTo(rcText4.right, rcText4.bottom + Pixel2MM(pDC, 2));
			// newline
			nRowSpace = Pixel2MM(pDC, 20);
		}
		rcTitle4.OffsetRect(0, nRowSpace); rcText4.OffsetRect(0, nRowSpace);

		strOut.LoadString(IDS_NOTES_TITLE);
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
		pDC->DrawText(strOut, rcTitle4, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
		pDC->SetTextColor(crValueColor);
		rcText4.right = rcFrame.right; rcText4.bottom = rcFrame.bottom - Pixel2MM(pDC, 5);
		pDC->DrawText(pDoc->m_GlobalData.m_strNotes, rcText4,  DT_LEFT | DT_VCENTER);
	}

// newline
	//rcTitle1.OffsetRect(0, nRowSpace); rcText1.OffsetRect(0, nRowSpace); rcTitle2.OffsetRect(0, nRowSpace); rcText2.OffsetRect(0, nRowSpace); rcTitle3.OffsetRect(0, nRowSpace); rcText3.OffsetRect(0, nRowSpace);
	//nRowSpace = Pixel2MM(pDC, 15);

	strOut.LoadString(IDS_JOBNAME_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strJob, rcText1,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	strOut.LoadString(IDS_JOBNUMBER_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle2, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strJobNumber, rcText2,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	strOut.LoadString(IDS_SCHEDULEDTOPRINT_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle3, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strScheduledToPrint, rcText3,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

// newline
	rcTitle1.OffsetRect(0, nRowSpace); rcText1.OffsetRect(0, nRowSpace); rcTitle2.OffsetRect(0, nRowSpace); rcText2.OffsetRect(0, nRowSpace); rcTitle3.OffsetRect(0, nRowSpace); rcText3.OffsetRect(0, nRowSpace);

	strOut.LoadString(IDS_CUSTOMER_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strCustomer, rcText1,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	strOut.LoadString(IDS_CUSTOMERNUMBER_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle2, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strCustomerNumber, rcText2,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	strOut.LoadString(IDS_CREATIONDATE_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle3, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(CPrintSheetView::ConvertToString(pDoc->m_GlobalData.m_CreationDate), rcText3,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

// newline
	rcTitle1.OffsetRect(0, nRowSpace); rcText1.OffsetRect(0, nRowSpace); rcTitle2.OffsetRect(0, nRowSpace); rcText2.OffsetRect(0, nRowSpace); rcTitle3.OffsetRect(0, nRowSpace); rcText3.OffsetRect(0, nRowSpace);

	strOut.LoadString(IDS_CREATOR_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle1, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strCreator, rcText1,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	strOut.LoadString(IDS_PHONENUMBER_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle2, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(pDoc->m_GlobalData.m_strPhoneNumber, rcText2,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	strOut.LoadString(IDS_LASTMODIFIED_LABEL);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
	pDC->DrawText(strOut, rcTitle3, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
	pDC->SetTextColor(crValueColor);
	pDC->DrawText(CPrintSheetView::ConvertToString(pDoc->m_GlobalData.m_LastModifiedDate), rcText3,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	if (pDC->IsPrinting())
	{
		rcTitle5.top = rcTitle1.bottom + Pixel2MM(pDC, 20); rcTitle5.bottom = rcTitle5.top + nTextHeight;
		rcText5.top = rcTitle5.top;  rcText5.right = rcFrame.right; rcText5.bottom = rcText5.top + nTextHeight;
		if ( ! pDoc->m_GlobalData.m_strJobTicketFile.IsEmpty())
		{
			strOut = L"Job Ticket:";
			pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
			pDC->DrawText(strOut, rcTitle5, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	
			pDC->SetTextColor(crValueColor);
			pDC->DrawText(pDoc->m_GlobalData.m_strJobTicketFile, rcText5,  DT_LEFT | DT_VCENTER | DT_SINGLELINE);
			//pDC->MoveTo(rcTitle5.left - Pixel2MM(pDC, 5), rcText5.bottom + Pixel2MM(pDC, 2)); pDC->LineTo(rcText5.right, rcText5.bottom + Pixel2MM(pDC, 2));
			// newline
			nRowSpace = Pixel2MM(pDC, 20);
			rcTitle5.OffsetRect(0, nRowSpace); rcText5.OffsetRect(0, nRowSpace);
		}

		strOut.LoadString(IDS_NOTES_TITLE);
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT); 
		pDC->DrawText(strOut, rcTitle5, DT_LEFT | DT_TOP | DT_SINGLELINE);	
		pDC->SetTextColor(crValueColor);
		rcText5.bottom = rcFrame.bottom - Pixel2MM(pDC, 5);
		pDC->DrawText(pDoc->m_GlobalData.m_strNotes, rcText5,  DT_LEFT | DT_TOP | DT_WORDBREAK);
	}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();
	smallFont.DeleteObject();
	boldFont.DeleteObject();

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);
}



/////////////////////////////////////////////////////////////////////////////
// CFinalProductDescription

IMPLEMENT_SERIAL(CFinalProductDescription, CObject, VERSIONABLE_SCHEMA | 5)

CFinalProductDescription::CFinalProductDescription()
{
	m_nApplicationCategory			 = 0;		// For future purposes
	m_nPages						 = 0;
	m_strPagesComment.Empty();
	m_fTrimSizeWidth				 = 0.0f;
	m_fTrimSizeHeight				 = 0.0f;
	m_strFormatName					 = "";
	m_nPageOrientation				 = 0;
	m_bindingDefs.m_strName			 = theApp.settings.m_szLastBindingStyle;
	m_bindingDefs.m_nBinding		 = 0;
	//m_nBindingMethod				 = 0;
	m_strBindingComment.Empty();
	m_nQuantity						 = 0;
	m_strQuantityComment.Empty();
	m_PageMargins.fWidth			 = 0.0f;
	m_PageMargins.fHeight			 = 0.0f;
	m_PageMargins.fDistanceToBack	 = 0.0f;
	m_PageMargins.fDistanceToHead	 = 0.0f;
}



void CFinalProductDescription::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CFinalProductDescription));

	if (ar.IsStoring())
	{
		ar << m_nApplicationCategory;
		ar << m_nPages;
		ar << m_strPagesComment;
		ar << m_fTrimSizeWidth;
		ar << m_fTrimSizeHeight;
		ar << m_strFormatName;
		ar << m_nPageOrientation;
		ar << m_bindingDefs.m_strName;
		ar << m_bindingDefs.m_nBinding;
		ar << m_strBindingComment;
		ar << m_nQuantity;
		ar << m_strQuantityComment;
		ar << m_PageMargins.fWidth;
		ar << m_PageMargins.fHeight;
		ar << m_PageMargins.fDistanceToBack;
		ar << m_PageMargins.fDistanceToHead;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		unsigned nOldBindingMethod;
		switch (nVersion)
		{
		case 1: 
		case 2:	ar >> m_nApplicationCategory;
				ar >> m_nPages;
				ar >> m_fTrimSizeWidth;
				ar >> m_fTrimSizeHeight;
				ar >> m_strFormatName;
				ar >> m_nPageOrientation;
				ar >> nOldBindingMethod;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fHeadTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fSideTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fFootTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fBackTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_nProductionType;
				ar >> m_PageMargins.fWidth;
				ar >> m_PageMargins.fHeight;
				ar >> m_PageMargins.fDistanceToBack;
				ar >> m_PageMargins.fDistanceToHead;
				if (nVersion == 1)
					switch (nOldBindingMethod)	// binding method changed - paste bound seems not to make sense (in fact the same as perfect bound)
					{							// so from now on, just use perfect bound and saddle stitched
					case 1:  m_bindingDefs.m_nBinding = CFinalProductDescription::PerfectBound;	  
							 m_bindingDefs.m_strName.LoadString(IDS_PERFECTBOUND_TOOLTIP);
							 break;	// old paste bound becomes new perfect bound
					case 2:  m_bindingDefs.m_nBinding = CFinalProductDescription::SaddleStitched; 
							 m_bindingDefs.m_strName.LoadString(IDS_SADDLESTITCHED_TOOLTIP);
							 break;	// old saddle stitched becomes new saddle stitched
					default: break;
					}
				m_nQuantity = 0;
				m_strPagesComment.Empty();
				m_strBindingComment.Empty();
				m_strQuantityComment.Empty();
				break;

		case 3: ar >> m_nApplicationCategory;
				ar >> m_nPages;
				ar >> m_strPagesComment;
				ar >> m_fTrimSizeWidth;
				ar >> m_fTrimSizeHeight;
				ar >> m_strFormatName;
				ar >> m_nPageOrientation;
				ar >> nOldBindingMethod;
				ar >> m_strBindingComment;
				ar >> m_nQuantity;
				ar >> m_strQuantityComment;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fHeadTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fSideTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fFootTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fBackTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_nProductionType;
				ar >> m_PageMargins.fWidth;
				ar >> m_PageMargins.fHeight;
				ar >> m_PageMargins.fDistanceToBack;
				ar >> m_PageMargins.fDistanceToHead;
				switch (nOldBindingMethod)	
				{							
				case CFinalProductDescription::PerfectBound:	m_bindingDefs.m_nBinding = CFinalProductDescription::PerfectBound;	  
																m_bindingDefs.m_strName.LoadString(IDS_PERFECTBOUND_TOOLTIP);
																break;	
				case CFinalProductDescription::SaddleStitched:  m_bindingDefs.m_nBinding = CFinalProductDescription::SaddleStitched; 
																m_bindingDefs.m_strName.LoadString(IDS_SADDLESTITCHED_TOOLTIP);
																break;	
				default: break;
				}
				break;

		case 4: ar >> m_nApplicationCategory;
				ar >> m_nPages;
				ar >> m_strPagesComment;
				ar >> m_fTrimSizeWidth;
				ar >> m_fTrimSizeHeight;
				ar >> m_strFormatName;
				ar >> m_nPageOrientation;
				ar >> nOldBindingMethod;
				ar >> m_strBindingComment;
				ar >> m_nQuantity;
				ar >> m_strQuantityComment;
				ar >> m_PageMargins.fWidth;
				ar >> m_PageMargins.fHeight;
				ar >> m_PageMargins.fDistanceToBack;
				ar >> m_PageMargins.fDistanceToHead;
				switch (nOldBindingMethod)	
				{							
				case CFinalProductDescription::PerfectBound:	m_bindingDefs.m_nBinding = CFinalProductDescription::PerfectBound;	  
																m_bindingDefs.m_strName.LoadString(IDS_PERFECTBOUND_TOOLTIP);
																break;	
				case CFinalProductDescription::SaddleStitched:  m_bindingDefs.m_nBinding = CFinalProductDescription::SaddleStitched; 
																m_bindingDefs.m_strName.LoadString(IDS_SADDLESTITCHED_TOOLTIP);
																break;	
				default: break;
				}
				break;

		case 5: ar >> m_nApplicationCategory;
				ar >> m_nPages;
				ar >> m_strPagesComment;
				ar >> m_fTrimSizeWidth;
				ar >> m_fTrimSizeHeight;
				ar >> m_strFormatName;
				ar >> m_nPageOrientation;
				ar >> m_bindingDefs.m_strName;
				ar >> m_bindingDefs.m_nBinding;
				ar >> m_strBindingComment;
				ar >> m_nQuantity;
				ar >> m_strQuantityComment;
				ar >> m_PageMargins.fWidth;
				ar >> m_PageMargins.fHeight;
				ar >> m_PageMargins.fDistanceToBack;
				ar >> m_PageMargins.fDistanceToHead;
				break;
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CProductionData

IMPLEMENT_SERIAL(CProductionData, CObject, VERSIONABLE_SCHEMA | 1)

CProductionData::CProductionData()
{
	m_fHeadTrim		  = 0.0f;
	m_fSideTrim		  = 0.0f;
	m_fFootTrim		  = 0.0f;
	m_fBackTrim		  = 0.0f;
	m_nProductionType = 0;
}

void CProductionData::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductionData));

	if (ar.IsStoring())
	{
		ar << m_fHeadTrim;
		ar << m_fSideTrim;
		ar << m_fFootTrim;
		ar << m_fBackTrim;
		ar << m_nProductionType;		
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1: ar >> m_fHeadTrim;
				ar >> m_fSideTrim;
				ar >> m_fFootTrim;
				ar >> m_fBackTrim;
				ar >> m_nProductionType;		
				break;
		}
	}

	m_paperList.Serialize(ar);
}


/////////////////////////////////////////////////////////////////////////////
// CPaperList

IMPLEMENT_SERIAL(CPaperList, CObject, VERSIONABLE_SCHEMA | 1)

CPaperList::CPaperList()
{
}



/////////////////////////////////////////////////////////////////////////////
// CPaperListEntry

IMPLEMENT_SERIAL(CPaperListEntry, CObject, VERSIONABLE_SCHEMA | 1)

CPaperListEntry::CPaperListEntry()
{
	m_strName.Empty();
	m_strFormat.Empty();
	m_nNumSheets = 0;
	m_nQuantity  = 0;
	m_nExtra	 = 0;
	m_nTotal	 = 0;
	m_strComment.Empty();
}

const CPaperListEntry& CPaperListEntry::operator=(const CPaperListEntry& rPaperListEntry)
{
	m_strName	 = rPaperListEntry.m_strName;
	m_strFormat  = rPaperListEntry.m_strFormat;
	m_nNumSheets = rPaperListEntry.m_nNumSheets;
	m_nQuantity  = rPaperListEntry.m_nQuantity;
	m_nExtra	 = rPaperListEntry.m_nExtra;
	m_nTotal	 = rPaperListEntry.m_nTotal;
	m_strComment = rPaperListEntry.m_strComment;

	return *this;
}

// helper function for CPaperListEntry elements
template <> void AFXAPI SerializeElements <CPaperListEntry> (CArchive& ar, CPaperListEntry* pPaperListEntry, int nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPaperListEntry));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPaperListEntry->m_strName;
			ar << pPaperListEntry->m_strFormat;
			ar << pPaperListEntry->m_nNumSheets;
			ar << pPaperListEntry->m_nQuantity;
			ar << pPaperListEntry->m_nExtra;
			ar << pPaperListEntry->m_nTotal;
			ar << pPaperListEntry->m_strComment;
		}
		else
		{
			switch (nVersion)
			{
			case 1: ar >> pPaperListEntry->m_strName;
					ar >> pPaperListEntry->m_strFormat;
					ar >> pPaperListEntry->m_nNumSheets;
					ar >> pPaperListEntry->m_nQuantity;
					ar >> pPaperListEntry->m_nExtra;
					ar >> pPaperListEntry->m_nTotal;
					ar >> pPaperListEntry->m_strComment;
					break;
			}
		}
		pPaperListEntry++;
	}
}



/////////////////////////////////////////////////////////////////////////////
// CProductPartList

IMPLEMENT_SERIAL(CProductPartList, CObject, VERSIONABLE_SCHEMA | 1)

CProductPartList::CProductPartList()
{
}

void CProductPartList::CreateFromOldSchema(CImpManDoc* pDoc)
{
	if ( ! pDoc)
		return;

	CProductPart productPart;
	productPart.m_nType				= (pDoc->m_FinalProductDescription.m_nApplicationCategory == APPLICATION_BOOK) ? CProductPart::Bound : CProductPart::Unbound;
	productPart.m_nNumPages			= pDoc->m_FinalProductDescription.m_nPages;
	if (productPart.m_nType == CProductPart::Bound)
	{
		productPart.m_strName.LoadStringA(IDS_UNKNOWN_STR);
		productPart.m_fPageWidth		= pDoc->m_FinalProductDescription.m_fTrimSizeWidth;
		productPart.m_fPageHeight		= pDoc->m_FinalProductDescription.m_fTrimSizeHeight;
		productPart.m_strFormatName		= pDoc->m_FinalProductDescription.m_strFormatName;
		productPart.m_nPageOrientation	= pDoc->m_FinalProductDescription.m_nPageOrientation;
		productPart.m_nAmount			= pDoc->m_FinalProductDescription.m_nQuantity;
		productPart.m_nExtra			= 0;
		productPart.m_strComment.Format(_T("%s\n%s\n%s"), pDoc->m_FinalProductDescription.m_strPagesComment, pDoc->m_FinalProductDescription.m_strBindingComment, pDoc->m_FinalProductDescription.m_strQuantityComment);
		productPart.m_prodDataBound.m_nProductionType		= pDoc->m_productionData.m_nProductionType;
		productPart.m_prodDataBound.m_bindingDefs.m_nBinding= pDoc->m_FinalProductDescription.m_bindingDefs.m_nBinding;
		productPart.m_prodDataBound.m_fHeadTrim			= pDoc->m_productionData.m_fHeadTrim;
		productPart.m_prodDataBound.m_fSideTrim			= pDoc->m_productionData.m_fSideTrim;
		productPart.m_prodDataBound.m_fFootTrim			= pDoc->m_productionData.m_fFootTrim;
		productPart.m_prodDataBound.m_fSpineTrim		= pDoc->m_productionData.m_fBackTrim;
		productPart.m_prodDataBound.m_fMillingEdge		= 0.0f;
		productPart.m_prodDataBound.m_fOverFold			= 0.0f;
		productPart.m_prodDataBound.m_nOverFoldSide		= CProductPart::OverFoldNone;
		CPrintSheet* pPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
		CPlate*		 pPlate		 = (pPrintSheet) ? pPrintSheet->GetTopmostPlate(FRONTSIDE) : NULL;
		productPart.m_prodDataBound.m_fBleed			= (pPlate) ? pPlate->m_fOverbleedOutside : 0.0f;
	}
	else
		productPart.m_strName.LoadStringA(IDS_UNBOUND);

	productPart.m_prodDataUnbound.m_fLeftTrim	= 0.0f;
	productPart.m_prodDataUnbound.m_fBottomTrim	= 0.0f;
	productPart.m_prodDataUnbound.m_fRightTrim	= 0.0f;
	productPart.m_prodDataUnbound.m_fTopTrim	= 0.0f;

	Add(productPart);
}

CProductPart* CProductPartList::JDFAddProductPart(CString strJDFNodeID, CString strJDFProductType, CString strName, int nNumPages, float fPageWidth, float fPageHeight, CString strFormatName, int nBindingMethod)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	///// add product part
	CProductPart productPart;
	productPart.m_strJDFNodeID		= strJDFNodeID;
	productPart.m_strName			= strName;
	productPart.m_nNumPages			= nNumPages;
	productPart.m_fPageWidth		= fPageWidth; 
	productPart.m_fPageHeight		= fPageHeight;
	productPart.m_strFormatName		= strFormatName;
	productPart.m_prodDataBound.m_bindingDefs.m_nBinding = nBindingMethod;
	//productPart.m_nAmount			= pFPD->m_nQuantity;
	//productPart.m_nExtra			= 0;

	CNumbering numbering;
	numbering.m_nRangeFrom			= 1;	
	numbering.m_nRangeTo			= productPart.m_nNumPages;
	numbering.m_nStartNum			= 1;
	numbering.m_strNumberingStyle	= L"$n";
	numbering.m_bReverse			= FALSE;
	productPart.m_numberingList.RemoveAll();
	productPart.m_numberingList.Add(numbering);
	//if ( (nProductPartIndex == 0) && (! pDoc->m_productPartList.GetSize()) )
	//{
	//	productPart.m_colorantList.Add(CColorant(CYAN,		"Cyan"));
	//	productPart.m_colorantList.Add(CColorant(MAGENTA,	"Magenta"));
	//	productPart.m_colorantList.Add(CColorant(YELLOW,	"Yellow"));
	//	productPart.m_colorantList.Add(CColorant(BLACK,		"Black"));
	//}

	productPart.m_nSubType = CProductPart::Body;
	if (strJDFProductType == L"Cover")
	{
		productPart.m_nSubType = CProductPart::Cover;
		productPart.m_strName.LoadStringA(IDS_COVER_NAME);	// needed by ArrangeCover() to identify. Original name is lost. Needs to be changed in future
	}
	else
	if (strJDFProductType == L"Body")
		productPart.m_nSubType = CProductPart::Body;
	else
	if (strJDFProductType == L"Insert")
		productPart.m_nSubType = CProductPart::Insert;
	else
	{
		CString string = strName;
		string.MakeUpper(); 
		if (string.Find(_T("UMSCHLAG")) >= 0)
		{
			productPart.m_nSubType = CProductPart::Cover;
			productPart.m_strName.LoadStringA(IDS_COVER_NAME);	// needed by ArrangeCover() to identify. Original name is lost. Needs to be changed in future
		}
	}

	pDoc->m_productPartList.Add(productPart);
	pDoc->m_PageTemplateList.InsertProductPartPages(productPart.m_nNumPages, pDoc->m_productPartList.GetSize() - 1);

	int nStartNum = 1;
	for (int i = 0; i < pDoc->m_productPartList.GetSize(); i++)
	{
		if ( ! pDoc->m_productPartList[i].m_numberingList.GetSize())
			continue;
		nStartNum = (pDoc->m_productPartList[i].m_numberingList[0].m_nStartNum == 1) ? nStartNum : pDoc->m_productPartList[i].m_numberingList[0].m_nStartNum;
		pDoc->m_PageTemplateList.DoNumber(  TRUE, 
											pDoc->m_productPartList[i].m_numberingList[0].m_nRangeFrom, 
											pDoc->m_productPartList[i].m_numberingList[0].m_nRangeTo, 
											nStartNum, 
											pDoc->m_productPartList[i].m_numberingList[0].m_strNumberingStyle, 
											pDoc->m_productPartList[i].m_numberingList[0].m_bReverse, i); 

		pDoc->m_productPartList[i].m_numberingList[0].m_nStartNum = nStartNum;

		nStartNum += (pDoc->m_productPartList[i].m_strName.CompareNoCase(theApp.m_strCoverName) == 0) ? pDoc->m_productPartList[i].m_nNumPages / 2 : pDoc->m_productPartList[i].m_nNumPages;
	}

	return pDoc->m_productPartList.GetPart(pDoc->m_productPartList.GetSize() - 1);
}

CRect CProductPartList::DrawBoundProductsHeader(CDC* pDC, CRect rcFrame, BOOL bSelected, CDisplayList* pDisplayList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(rcFrame.TopLeft(), CSize(0,0));

	CString strOut, string;
	CFont	boldFont;
	boldFont.CreateFont (Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetTextColor( (bSelected) ? GUICOLOR_DLG_STATIC_TEXT : DARKGRAY);
	pDC->SetBkMode(TRANSPARENT);

	COLORREF crValueColor = RGB(50,50,50);

	int	nTextHeight = Pixel2MM(pDC, 15);

	CRect rcTitle;
	rcTitle.left = rcFrame.left + Pixel2MM(pDC, 10); rcTitle.top = rcFrame.top + Pixel2MM(pDC, 5); rcTitle.right = rcFrame.right; rcTitle.bottom = rcTitle.top + nTextHeight;
	CRect rcBox(rcTitle.TopLeft(), CSize(0,0));

	CGraphicComponent gp;
	CRect rcBackground = rcFrame; rcBackground.bottom = rcTitle.bottom + 5; rcBackground.DeflateRect(5,0);
	if (pDisplayList)
	{
		if ( ! bSelected)
		{
			pDC->FillSolidRect(rcBackground, WHITE);
			rcBackground.top += 2;
			gp.DrawTitleBar(pDC, rcBackground, L"", RGB(241,243,248), RGB(231,234,241), 90, FALSE);

			CRect rcDI = rcBox; rcDI.InflateRect(5, 5);
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)1, DISPLAY_ITEM_REGISTRY(CProductDataView, UnboundProductTab), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
			}
		}
		else
		{
			gp.DrawTitleBar(pDC, rcBackground, L"", RGB(231,234,239), RGB(209,215,226), 90, -1);
			//pDC->FillSolidRect(rcBackground, RGB(209,215,226));
			rcBackground.top += 2;
		}
	}

	strOut.LoadStringA(IDS_BOUND);
	pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcBox.right	 = max(rcBox.right, rcTitle.left + pDC->GetTextExtent(strOut).cx);
	rcBox.bottom = max(rcBox.bottom, rcTitle.bottom);

	pDC->SelectObject(pOldFont);
	boldFont.DeleteObject();

	if (pDisplayList)
		if (bSelected)
		{
			rcBackground.left = rcBox.right + 5;
			gp.DrawTitleBar(pDC, rcBackground, L"", RGB(241,243,248), RGB(231,234,241), 90, FALSE);
			rcBackground.bottom -= 1;

			CRect rcClientRect;
			pDisplayList->m_pParent->GetClientRect(rcClientRect);
			CPen pen(PS_SOLID, 1, MIDDLEGRAY);
			CPen* pOldPen = pDC->SelectObject(&pen);
			pDC->MoveTo(rcClientRect.left, rcClientRect.bottom); 
			pDC->LineTo(rcClientRect.left, rcBackground.bottom); 
			pDC->LineTo(rcBox.left - 5, rcBackground.bottom); 
			pDC->LineTo(rcBox.left - 5, rcClientRect.top); 
			pDC->LineTo(rcBox.right + 5, rcClientRect.top); 
			pDC->LineTo(rcBox.right + 5, rcBackground.bottom); 
			pDC->LineTo(rcClientRect.right, rcBackground.bottom); 
			pDC->SelectObject(pOldPen);
			pen.DeleteObject();
		}
		else
		{
			CRect rcDI = rcBox; rcDI.InflateRect(5, 3);
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)0, DISPLAY_ITEM_REGISTRY(CProductDataView, BoundProductTab), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
			}
		}

	return rcBox;
}

CRect CProductPartList::DrawUnboundProductsHeader(CDC* pDC, CRect rcFrame, BOOL bSelected, CDisplayList* pDisplayList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(rcFrame.TopLeft(), CSize(0,0));

	CString strOut, string;
	CFont	boldFont, font;
	boldFont.CreateFont (Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	font.CreateFont		(Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetTextColor( (bSelected) ? GUICOLOR_DLG_STATIC_TEXT : DARKGRAY);
	pDC->SetBkMode(TRANSPARENT);

	int	nTextHeight = Pixel2MM(pDC, 15);

	CRect rcTitle;
	rcTitle.left = rcFrame.left + Pixel2MM(pDC, 20); rcTitle.top = rcFrame.top + Pixel2MM(pDC, 5); rcTitle.right = rcFrame.right; rcTitle.bottom = rcTitle.top + nTextHeight;
	CRect rcBox(rcTitle.TopLeft(), CSize(0,0));

	CGraphicComponent gp;
	CRect rcBackground = rcFrame; rcBackground.left += 10; rcBackground.bottom = rcTitle.bottom + 5; rcBackground.DeflateRect(5,0);
	if (pDisplayList)
	{
		if ( ! bSelected)
		{
			rcBackground.left -= 29; 
			pDC->FillSolidRect(rcBackground, WHITE);
			rcBackground.left -= 1; rcBackground.top += 2;
			gp.DrawTitleBar(pDC, rcBackground, L"", RGB(241,243,248), RGB(231,234,241), 90, FALSE);
		}
		else
		{
			gp.DrawTitleBar(pDC, rcBackground, L"", RGB(231,234,239), RGB(209,215,226), 90, -1);
			//pDC->FillSolidRect(rcBackground, RGB(209,215,226));
			rcBackground.top += 2;
		}
	}

	strOut.LoadStringA(IDS_UNBOUND);
	pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcBox.right	 = max(rcBox.right, rcTitle.left + pDC->GetTextExtent(strOut).cx);
	rcBox.bottom = max(rcBox.bottom, rcTitle.bottom);

	pDC->SetTextColor(MIDDLEGRAY);
	pDC->SetBkMode(TRANSPARENT);

	rcTitle.left = rcBox.right + Pixel2MM(pDC, 10); 
	string.LoadStringA(IDS_LABELTYPES);
	strOut.Format(_T("%d %s"), pDoc->m_labelList.GetSize(), string);
	pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcBox.right	 = max(rcBox.right, rcTitle.left + pDC->GetTextExtent(strOut).cx);
	rcBox.bottom = max(rcBox.bottom, rcTitle.bottom);

	pDC->SelectObject(pOldFont);
	boldFont.DeleteObject();
	font.DeleteObject();

	if (pDisplayList)
		if (bSelected)
		{
			rcBackground.left = rcBox.right + 5;
			gp.DrawTitleBar(pDC, rcBackground, L"", RGB(241,243,248), RGB(231,234,241), 90, FALSE);
			rcBackground.bottom -= 1;

			CRect rcClientRect;
			pDisplayList->m_pParent->GetClientRect(rcClientRect);
			CPen pen(PS_SOLID, 1, MIDDLEGRAY);
			CPen* pOldPen = pDC->SelectObject(&pen);
			pDC->MoveTo(rcClientRect.left, rcClientRect.bottom); 
			pDC->LineTo(rcClientRect.left, rcBackground.bottom); 
			pDC->LineTo(rcBox.left - 5, rcBackground.bottom); 
			pDC->LineTo(rcBox.left - 5, rcClientRect.top); 
			pDC->LineTo(rcBox.right + 5, rcClientRect.top); 
			pDC->LineTo(rcBox.right + 5, rcBackground.bottom); 
			pDC->LineTo(rcClientRect.right, rcBackground.bottom); 
			pDC->SelectObject(pOldPen);
			pen.DeleteObject();
		}
		else
		{
			CRect rcDI = rcBox; rcDI.InflateRect(5, 3);
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)1, DISPLAY_ITEM_REGISTRY(CProductDataView, UnboundProductTab), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
			}
		}

	return rcBox;
}


CRect CProductPartList::Draw(CDC* pDC, CRect rcFrame, int nProductPartIndex, CDisplayList* pDisplayList)
{
	if (nProductPartIndex == GetLabelsProductPartIndex())
		return DrawUnbound(pDC, rcFrame, nProductPartIndex, pDisplayList);
	else
		return DrawBound(pDC, rcFrame, nProductPartIndex, pDisplayList);
}

CRect CProductPartList::DrawBound(CDC* pDC, CRect rcFrame, int nProductPartIndex, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(0, 0));

	if (GetSize() <= 0)
		return rcBox;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return rcBox;

	int nProductPartNameWidth = rcFrame.Width();
	int nThumbnailWidth		  = rcFrame.Height();
	int nColorInfoWidth		  = Pixel2MM(pDC, 150);
	int nPaperDataWidth		  = Pixel2MM(pDC, 220);
	int nAmountDataWidth	  = Pixel2MM(pDC, 120);
	int nTrimDataWidth		  = Pixel2MM(pDC, 100);
	int nTrimData2Width		  = Pixel2MM(pDC, 120);

	float fRatio = (pDC->IsPrinting()) ? 0.85f : 1.0f;
	nProductPartNameWidth = (int)((float)nProductPartNameWidth * fRatio);
	nThumbnailWidth		  = (int)((float)nThumbnailWidth	   * fRatio);
	nColorInfoWidth		  = (int)((float)nColorInfoWidth	   * fRatio);
	nPaperDataWidth		  = (int)((float)nPaperDataWidth	   * fRatio);
	nAmountDataWidth	  = (int)((float)nAmountDataWidth	   * fRatio);
	nTrimDataWidth		  = (int)((float)nTrimDataWidth		   * fRatio);
	nTrimData2Width		  = (int)((float)nTrimData2Width	   * fRatio);
	int nTab1 = (int)(float)( 60 * fRatio);
	int nTab2 = (int)(float)(100 * fRatio);
	int nTab3 = (int)(float)( 50 * fRatio);

	CProductPart& rProductPart = GetAt(nProductPartIndex);

	COLORREF crValueColor = RGB(50,50,50);

	CFont	boldFont, font;
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	font.CreateFont		((pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&boldFont);
	pDC->SetTextColor(GUICOLOR_CAPTION);
	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(222, 233, 77));

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CRect rcRow = rcFrame; 
	CRect rcText;
	rcText.left = rcFrame.left + Pixel2MM(pDC, 10); rcText.top = rcFrame.top; rcText.bottom = rcText.top + nTextHeight;  rcText.right = rcText.left + nProductPartNameWidth;
	rcBox = CRect(rcText.TopLeft(), CSize(1,1));

	CRect rcDI(rcText.TopLeft(), CSize(0,0));

	////// header (name / pages / page range)
	CRect rcTitle = rcText; rcTitle.left -= Pixel2MM(pDC, 5); rcTitle.right -= Pixel2MM(pDC, 20); 
	rcTitle.InflateRect(0, 1);
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcTitle, L"", RGB(231,234,239), RGB(209,215,226), 90, FALSE);

	CString strOut, strOut2;
	strOut2.LoadStringA(IDS_PAGES);
	strOut.Format(_T(" %s "), rProductPart.m_strName);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	pDC->SelectObject(&font);

	pDC->SetBkMode(TRANSPARENT);
	rcText.left = rcBox.right; rcText.right = rcFrame.right; 
	strOut.Format(_T("  | %d %s"), rProductPart.m_nNumPages, strOut2);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	rcText.left = rcBox.right; rcText.right = rcFrame.right; 
	if (pDoc->m_PageTemplateList.GetCount())
		strOut = _T(" | ") + pDoc->m_PageTemplateList.GetPageRange(nProductPartIndex);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	if (pDisplayList)
	{
		rcDI = rcTitle; rcDI.right -= 2; rcDI.bottom -= 2;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductDataView, ProductPartData), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
			pDI->SetMouseOverFeedback();
	}

	pDC->SetTextColor(crValueColor);

	////// thumbnail
	CRect rcThumbnail = rcFrame; 
	BOOL bShowMeasures = FALSE;
	rcThumbnail.top	  = rcBox.bottom;
	rcThumbnail.top	 += (bShowMeasures) ? nTextHeight : Pixel2MM(pDC, 10); rcThumbnail.bottom -= (bShowMeasures) ? nTextHeight : Pixel2MM(pDC, 5); 
	rcThumbnail.left += Pixel2MM(pDC, 10); rcThumbnail.right = rcThumbnail.left + nThumbnailWidth;
	CRect rcBoxThumbnail = pDoc->m_PageTemplateList.Draw(pDC, rcThumbnail, 0, nProductPartIndex, bShowMeasures, FALSE, TRUE, TRUE);
	rcBox.UnionRect(rcBox, rcBoxThumbnail);
	rcText.left = max(rcThumbnail.left + nThumbnailWidth, rcBox.right + Pixel2MM(pDC, 20)); rcText.top = rcRow.top; rcText.bottom = rcText.top + nTextHeight;  rcText.right = rcText.left + nTrimDataWidth;
	if (pDisplayList)
	{
		CRect rcDI = rcBoxThumbnail; //rcDI.InflateRect(5, 5);
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductDataView, ProductPartPages), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
			pDI->SetMouseOverFeedback();
	}

	////// colors
	rcText.top	= rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
	rcText.left = rcThumbnail.right + Pixel2MM(pDC, 5); rcText.right = rcText.left + nColorInfoWidth;
	//if (rProductPart.m_colorantList.GetSize())
	{
		CRect rcDI = rcText;
		if (rProductPart.m_colorantList.GetSize())
		{
			strOut2.LoadStringA(IDS_COLORED);
			strOut.Format(_T("%d-%s"), rProductPart.m_colorantList.GetSize(), strOut2); 
		}
		else
		{
			strOut.LoadStringA(IDS_NONE); 
			strOut2.LoadStringA(IDS_PLATECOLOR_STRING);
			strOut += _T(" ") + strOut2;
		}
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		int nXPos = rcText.left + pDC->GetTextExtent(strOut).cx + Pixel2MM(pDC, 5);
		int nYPos = rcText.top + Pixel2MM(pDC, 2);
		CRect rcIcon;
		if (pDC->IsPrinting())
		{
			rcIcon.left = nXPos; rcIcon.top = nYPos + Pixel2MM(pDC, 2); rcIcon.top = nYPos; rcIcon.right = rcIcon.left + Pixel2MM(pDC, 7); rcIcon.bottom = nYPos + Pixel2MM(pDC, 7);
		}
		else
		{
			rcIcon.left = nXPos; rcIcon.top = nYPos + Pixel2MM(pDC, 2); rcIcon.right = rcIcon.left + Pixel2MM(pDC, 9); rcIcon.bottom = nYPos + Pixel2MM(pDC, 9);
		}

		CPen pen(PS_SOLID, 1, MIDDLEGRAY);
		CBrush brush;
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		CBrush* pOldBrush = pDC->GetCurrentBrush();
		for (int i = 0; i < rProductPart.m_colorantList.GetSize(); i++)
		{
			brush.CreateSolidBrush(rProductPart.m_colorantList[i].m_crRGB);
			pDC->SelectObject(&brush);
			pDC->Rectangle(rcIcon);
			brush.DeleteObject();

			rcIcon.OffsetRect(rcIcon.Width() - Pixel2MM(pDC, 1), 0);
		}
		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		pen.DeleteObject();
		brush.DeleteObject();

		rcBox.right = max(rcBox.right, rcIcon.right);
		rcText.OffsetRect(0, nTextHeight);
		strOut = rProductPart.m_colorantList.GetProcessColorCode();
		if ( ! strOut.IsEmpty())
		{
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			rcText.OffsetRect(0, nTextHeight);
		}
		for (int i = 0; i < rProductPart.m_colorantList.GetSize(); i++)
		{
			strOut = rProductPart.m_colorantList[i].m_strName;
			if ( ! theApp.m_colorDefTable.IsProcessColor(strOut))
			{
				pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
				rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
				rcBox.bottom = max(rcBox.bottom, rcText.bottom);
				rcText.OffsetRect(0, nTextHeight);
			}
		}
		if (pDisplayList)
		{
			rcDI.right = rcBox.right; rcDI.bottom = rcText.top - 2; rcDI.left -= 5; 
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductDataView, ColorData), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
				pDI->SetMouseOverFeedback();
		}
	}

	////// paper
	rcText.top	= rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
	rcText.left = rcText.left + nColorInfoWidth; rcText.right = rcText.left + nPaperDataWidth;
	//if ( ! rProductPart.m_strPaper.IsEmpty())
	{
		CRect rcDI(rcText.TopLeft(), CSize(0,0));;
		if ( ! rProductPart.m_strPaper.IsEmpty()) 
			strOut = rProductPart.m_strPaper;
		else
			strOut.LoadStringA(IDS_NO_PAPER);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);
		rcText.OffsetRect(0, nTextHeight);
		strOut = ( ! rProductPart.m_strPaper.IsEmpty()) ? rProductPart.InitPaperInfo() : L"";
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);
		if (pDisplayList)
		{
			rcDI.right = rcBox.right; rcDI.bottom = rcText.bottom; rcDI.InflateRect(5, 0); 
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)&rProductPart, DISPLAY_ITEM_REGISTRY(CProductDataView, PaperData), (void*)nProductPartIndex, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
				pDI->SetMouseOverFeedback();
		}
		rcText.OffsetRect(0, Pixel2MM(pDC, 20));
	}
	CRect rcText2 = rcText; rcText2.left = rcText.left + Pixel2MM(pDC, nTab2); 
	//if (fabs(rProductPart.m_prodDataBound.m_fShinglingValue) > 0.0f)
	{
		CRect rcDI(rcText.TopLeft(), CSize(0,0));
		strOut2.LoadStringA(IDS_SHINGLING_PER_SHEET); 
		strOut.Format(_T("%s:"), strOut2);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		if (rProductPart.m_prodDataBound.m_nShinglingMethod == CBookblock::ShinglingMethodBottling)
		{
			CPrintSheetView::TruncateNumber(strOut,  rProductPart.m_prodDataBound.m_fShinglingValue);
			CPrintSheetView::TruncateNumber(strOut2, rProductPart.m_prodDataBound.m_fShinglingValueFoot, TRUE);
			strOut += _T("/") + strOut2;
		}
		else
			CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fShinglingValue, TRUE);
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight);

		strOut2.LoadStringA(IDS_CORRECTION); 
		strOut.Format(_T("%s:"), strOut2);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		if (rProductPart.m_prodDataBound.m_bShinglingActive)
		{
			switch (rProductPart.m_prodDataBound.m_nShinglingMethod)
			{
			case CBookblock::ShinglingMethodMove:		strOut.LoadString(IDS_SHINGLINGMETHOD_MOVE);		break;
			case CBookblock::ShinglingMethodScale:		strOut.LoadString(IDS_SHINGLINGMETHOD_SCALE);		break;
			case CBookblock::ShinglingMethodBottling:	strOut.LoadString(IDS_SHINGLINGMETHOD_BOTTLING);	break; 
			}
		}
		else
			strOut.LoadString(IDS_NONE);
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		if (pDisplayList)
		{
			rcDI.right = rcBox.right; rcDI.bottom = rcText.bottom; rcDI.InflateRect(5, 0); 	
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductDataView, ShinglingData), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
				pDI->SetMouseOverFeedback();
		}
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight);
	}

	//rcText.top	= rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
	//rcText.left = rcText.left + nPaperDataWidth; rcText.right = rcText.left + nAmountDataWidth;
	rcText.OffsetRect(0, nTextHeight/2);
	rcText2 = rcText; rcText2.left = rcText.left + Pixel2MM(pDC, nTab3); rcText2.right = rcFrame.right;
	rcDI = CRect(rcText.TopLeft(), CSize(0,0));
	//if (rProductPart.m_nAmount > 0)
	{
		strOut2.LoadStringA(IDS_AMOUNT); 
		strOut.Format(_T("%s:"), strOut2);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		if (rProductPart.m_nAmount > 0)
			strOut.Format(_T("%ld"), rProductPart.m_nAmount);
		else
			strOut = L"-";
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight);
	}

	//if (rProductPart.m_nExtra > 0)
	{
		strOut2.LoadStringA(IDS_PAPERLIST_EXTRA); 
		strOut.Format(_T("%s:"), strOut2);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		if (rProductPart.m_nExtra > 0)
			strOut.Format(_T("%ld"), rProductPart.m_nExtra);
		else
			strOut = L"-";
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		if (pDisplayList)
		{
			rcDI.right = rcBox.right; rcDI.bottom = rcText.bottom; rcDI.InflateRect(5, 0); 	
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductDataView, VolumeData), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
				pDI->SetMouseOverFeedback();
		}
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight);
	}

	/////// trims
	rcText.top	= rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
	rcText.left	= rcText.left + nPaperDataWidth; rcText.right = rcText.left + nTrimDataWidth;
	rcText2		= rcText; rcText2.left = rcText.left + Pixel2MM(pDC, nTab1); rcText2.right = rcFrame.right;
	rcDI = CRect(rcText.TopLeft(), CSize(1,1));
	int nTrimDataBottom = rcDI.bottom; 

	strOut.LoadStringA(IDS_HEAD); strOut += L":";
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	if (fabs(rProductPart.m_prodDataBound.m_fHeadTrim) > 0.0f)
		CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fHeadTrim, TRUE);
	else
		strOut = L"-";
	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
	rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
	nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
	rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 

	strOut.LoadStringA(IDS_SIDE); strOut += L":";
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	if (fabs(rProductPart.m_prodDataBound.m_fSideTrim) > 0.0f)
		CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fSideTrim, TRUE);
	else
		strOut = L"-";
	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
	rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
	nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
	rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 

	strOut.LoadStringA(IDS_FOOT); strOut += L":";
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	if (fabs(rProductPart.m_prodDataBound.m_fFootTrim) > 0.0f)
		CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fFootTrim, TRUE);
	else
		strOut = L"-";
	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
	rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
	nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
	rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 

	////// spine / milling edge
	//rcText.top	= rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
	//rcText.left = rcText.left + nTrimDataWidth; rcText.right = rcText.left + nTrimData2Width;
	rcText.OffsetRect(0, nTextHeight);
	rcText2 = rcText; rcText2.left = rcText.left + Pixel2MM(pDC, nTab1); rcText2.right = rcFrame.right;
	if (fabs(rProductPart.m_prodDataBound.m_fSpineTrim) > 0.0f)
	{
		float fValue = 0.0f;
		if (rProductPart.m_strName.CompareNoCase(theApp.m_strCoverName) == 0)
		{
			strOut.LoadStringA(IDS_SPINE); strOut += L":";
			fValue = rProductPart.m_prodDataBound.m_fSpineTrim;
		}
		else
		{
			strOut.LoadStringA(IDS_MILLING_EDGE);
			fValue = rProductPart.m_prodDataBound.m_fSpineTrim / 2.0f;
		}
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		CPrintSheetView::TruncateNumber(strOut, fValue, TRUE);
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 
	}

	////// overfold
	if (fabs(rProductPart.m_prodDataBound.m_fOverFold) > 0.0f)
	{
		if (rProductPart.m_prodDataBound.m_nOverFoldSide == CProductPart::OverFoldFront)
			strOut.LoadStringA(IDS_OVERFOLD_FRONT);
		else
			strOut.LoadStringA(IDS_OVERFOLD_BACK); 
		strOut += L":";
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fOverFold, TRUE);
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 
	}

	////// bleed
	//if (fabs(rProductPart.m_prodDataBound.m_fBleed) > 0.0f)
	{
		strOut.LoadStringA(IDS_BLEED); strOut += L":";
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		if (fabs(rProductPart.m_prodDataBound.m_fBleed) > 0.0f)
			CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fBleed, TRUE);
		else
			strOut = L"-";
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 
	}

	if (pDisplayList)
	{
		rcDI.BottomRight() = CPoint(rcBox.right, nTrimDataBottom); rcDI.InflateRect(5, 0);
		rcBox.bottom += 4;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductDataView, TrimData), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
			pDI->SetMouseOverFeedback();
	}

	//rcText.left = rcBoxThumbnail.right + Pixel2MM(pDC, 20); rcText.top = rcBoxThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
	//if (rProductPart.m_strFormatName != pDoc->m_FinalProductDescription.m_strFormatName)
	//{
	//	CString strWidth, strHeight;
	//	CPrintSheetView::TruncateNumber(strWidth,  rProductPart.m_fPageWidth);
	//	CPrintSheetView::TruncateNumber(strHeight, rProductPart.m_fPageHeight);
	//	strOut.Format(_T("%s (%s x %s)"), rProductPart.m_strFormatName, strWidth, strHeight);
	//	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	//	rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
	//	rcBox.bottom = max(rcBox.bottom, rcText.bottom);
	//	rcText.OffsetRect(0, Pixel2MM(pDC, 20));
	//}

	CPen pen(PS_SOLID, 1, MIDDLEGRAY);
	CPen*	pOldPen		= pDC->SelectObject(&pen);
	CBrush* pOldBrush	= (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

	CRect rcBorder = rcTitle; rcBorder.bottom = rcBox.bottom;
	pDC->Rectangle(rcBorder);

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CProductPartList::DrawUnbound(CDC* pDC, CRect rcFrame, int nProductPartIndex, CDisplayList* pDisplayList)
{
	CRect rcTotalBox(rcFrame.TopLeft(), CSize(0, 0));

	if (GetSize() <= 0)
		return rcTotalBox;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcTotalBox;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return rcTotalBox;
	if (pDoc->m_labelList.GetSize() <= 0)
		return rcTotalBox;

	int	  nRowHeight = 70;
	CRect rcRow = rcFrame;
	rcRow.left += 10; rcRow.bottom = rcRow.top + nRowHeight;
	float fMaxHeight  = pDoc->m_labelList.GetMaxHeight();

	int nProductPartNameWidth = rcRow.Width();
	int nThumbnailWidth		  = rcRow.Height();
	int nColorInfoWidth		  = Pixel2MM(pDC, 150);
	int nPaperDataWidth		  = Pixel2MM(pDC, 220);
	int nAmountDataWidth	  = Pixel2MM(pDC, 120);
	int nTrimDataWidth		  = Pixel2MM(pDC, 100);
	int nTrimData2Width		  = Pixel2MM(pDC, 120);

	float fRatio = (pDC->IsPrinting()) ? 0.85f : 1.0f;
	nProductPartNameWidth = (int)((float)nProductPartNameWidth * fRatio);
	nThumbnailWidth		  = (int)((float)nThumbnailWidth	   * fRatio);
	nColorInfoWidth		  = (int)((float)nColorInfoWidth	   * fRatio);
	nPaperDataWidth		  = (int)((float)nPaperDataWidth	   * fRatio);
	nAmountDataWidth	  = (int)((float)nAmountDataWidth	   * fRatio);
	nTrimDataWidth		  = (int)((float)nTrimDataWidth		   * fRatio);
	nTrimData2Width		  = (int)((float)nTrimData2Width	   * fRatio);
	int nTab1 = (int)(float)( 60 * fRatio);
	int nTab2 = (int)(float)(100 * fRatio);
	int nTab3 = (int)(float)( 50 * fRatio);

	CProductPart& rProductPart = GetAt(nProductPartIndex);

	COLORREF crValueColor = RGB(50,50,50);

	CFont	boldFont, font;
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	font.CreateFont		((pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&boldFont);
	pDC->SetTextColor(crValueColor);

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CRect rcText;
	rcText.left = rcRow.left + Pixel2MM(pDC, 10); rcText.top = rcRow.top; rcText.bottom = rcText.top + nTextHeight;  rcText.right = rcText.left + nProductPartNameWidth;
	CRect rcBox = CRect(rcText.TopLeft(), CSize(1,1));

	CRect rcDI(rcText.TopLeft(), CSize(0,0));

	for (int i = 0; i < pDoc->m_labelList.GetSize(); i++)
	{
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(222, 233, 77));
		
		CLabelListEntry& rLabel = pDoc->m_labelList[i];
		////// header (name / comment)
		CRect rcTitle = rcText; rcTitle.left -= Pixel2MM(pDC, 5); rcTitle.right -= Pixel2MM(pDC, 20); 
		rcTitle.InflateRect(0, 1);
		CGraphicComponent gp;
		gp.DrawTitleBar(pDC, rcTitle, L"", RGB(231,234,239), RGB(209,215,226), 90, FALSE);

		CString strOut, strOut2;
		strOut.Format(_T(" %s "), rLabel.m_strName);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText.bottom);

		pDC->SelectObject(&font);

		pDC->SetBkMode(TRANSPARENT);
		if ( ! rLabel.m_strComment.IsEmpty())
		{
			rcText.left = rcBox.right; rcText.right = rcFrame.right; 
			strOut.Format(_T(" | %s"), rLabel.m_strComment);
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
			rcBox.bottom = max(rcBox.bottom, rcText.bottom);
		}

		if (pDisplayList)
		{
			rcDI = rcTitle; rcDI.right -= 2; rcDI.bottom -= 2;
			//rcDI.BottomRight() = rcBox.BottomRight(); rcDI.InflateRect(5, 0);
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)i, DISPLAY_ITEM_REGISTRY(CProductDataView, ProductPartLabel), (void*)nProductPartIndex, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
				pDI->SetMouseOverFeedback();
		}

		////// thumbnail
		CRect rcThumbnail = rcRow; 
		BOOL bShowMeasures = FALSE;
		rcThumbnail.top	  = rcBox.bottom;
		rcThumbnail.top	 += (bShowMeasures) ? nTextHeight : Pixel2MM(pDC, 10); 
		rcThumbnail.left += Pixel2MM(pDC, 10); rcThumbnail.right = rcThumbnail.left + nThumbnailWidth;
		rcThumbnail.bottom = rcThumbnail.top + (int)((float)rcRow.Height() * (pDoc->m_labelList[i].m_fLabelHeight / fMaxHeight));		
		CRect rcBoxThumbnail = pDoc->m_PageTemplateList.DrawSinglePage(pDC, rcThumbnail, i*2, nProductPartIndex);
		rcBox.UnionRect(rcBox, rcBoxThumbnail);
		rcText.left = max(rcThumbnail.left + nThumbnailWidth, rcBox.right + Pixel2MM(pDC, 20)); rcText.top = rcRow.top; rcText.bottom = rcText.top + nTextHeight;  rcText.right = rcText.left + nTrimDataWidth;
		if (pDisplayList)
		{
			CRect rcDI = rcBoxThumbnail; //rcDI.InflateRect(5, 5);
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)i, DISPLAY_ITEM_REGISTRY(CProductDataView, ProductPartLabel), (void*)nProductPartIndex, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
				pDI->SetMouseOverFeedback();
		}

		////// colors
		rcText.top	= rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
		rcText.left = rcThumbnail.right + Pixel2MM(pDC, 5); rcText.right = rcText.left + nColorInfoWidth;
		//if (rProductPart.m_colorantList.GetSize())
		{
			CRect rcDI = rcText; rcDI.right = rcDI.left + 1;
			if (rProductPart.m_colorantList.GetSize())
			{
				strOut2.LoadStringA(IDS_COLORED);
				strOut.Format(_T("%d-%s"), rProductPart.m_colorantList.GetSize(), strOut2); 
			}
			else
			{
				strOut.LoadStringA(IDS_NONE); 
				strOut2.LoadStringA(IDS_PLATECOLOR_STRING);
				strOut += _T(" ") + strOut2;
			}
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
			rcBox.bottom = max(rcBox.bottom, rcText.bottom);
			rcDI.right = max(rcDI.right, rcText.left + pDC->GetTextExtent(strOut).cx);

			int nXPos = rcText.left + pDC->GetTextExtent(strOut).cx + Pixel2MM(pDC, 5);
			int nYPos = rcText.top + Pixel2MM(pDC, 2);
			CRect rcIcon;
			if (pDC->IsPrinting())
			{
				rcIcon.left = nXPos; rcIcon.top = nYPos + Pixel2MM(pDC, 2); rcIcon.top = nYPos; rcIcon.right = rcIcon.left + Pixel2MM(pDC, 7); rcIcon.bottom = nYPos + Pixel2MM(pDC, 7);
			}
			else
			{
				rcIcon.left = nXPos; rcIcon.top = nYPos + Pixel2MM(pDC, 2); rcIcon.right = rcIcon.left + Pixel2MM(pDC, 9); rcIcon.bottom = nYPos + Pixel2MM(pDC, 9);
			}

			CPen pen(PS_SOLID, 1, MIDDLEGRAY);
			CBrush brush;
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->GetCurrentBrush();
			for (int i = 0; i < rProductPart.m_colorantList.GetSize(); i++)
			{
				brush.CreateSolidBrush(rProductPart.m_colorantList[i].m_crRGB);
				pDC->SelectObject(&brush);
				pDC->Rectangle(rcIcon);
				brush.DeleteObject();

				rcIcon.OffsetRect(rcIcon.Width() - Pixel2MM(pDC, 1), 0);
			}
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);
			pen.DeleteObject();
			brush.DeleteObject();

			rcBox.right = max(rcBox.right, rcIcon.right);
			rcText.OffsetRect(0, nTextHeight);
			strOut = rProductPart.m_colorantList.GetProcessColorCode();
			if ( ! strOut.IsEmpty())
			{
				pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
				rcText.OffsetRect(0, nTextHeight);
			}
			for (int i = 0; i < rProductPart.m_colorantList.GetSize(); i++)
			{
				strOut = rProductPart.m_colorantList[i].m_strName;
				if ( ! theApp.m_colorDefTable.IsProcessColor(strOut))
				{
					pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
					rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
					rcBox.bottom = max(rcBox.bottom, rcText.bottom);
					rcDI.right = max(rcDI.right, rcText.left + pDC->GetTextExtent(strOut).cx);
					rcText.OffsetRect(0, nTextHeight);
				}
			}
			if (pDisplayList)
			{
				/*rcDI.right = rcBox.right;*/ rcDI.bottom = rcText.top - 2; rcDI.left -= 5; 
				CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductDataView, ColorData), NULL, CDisplayItem::ForceRegister); // register item into display list
				if (pDI)
					pDI->SetMouseOverFeedback();
			}
		}

		////// paper
		rcText.top	= rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
		rcText.left = rcText.left + nColorInfoWidth; rcText.right = rcText.left + nPaperDataWidth;
		//if ( ! rProductPart.m_strPaper.IsEmpty())
		{
			CRect rcDI(rcText.TopLeft(), CSize(0,0));;
			if ( ! rProductPart.m_strPaper.IsEmpty()) 
				strOut = rProductPart.m_strPaper;
			else
				strOut.LoadStringA(IDS_NO_PAPER);
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
			rcBox.bottom = max(rcBox.bottom, rcText.bottom);
			rcText.OffsetRect(0, nTextHeight);
			strOut = ( ! rProductPart.m_strPaper.IsEmpty()) ? rProductPart.InitPaperInfo() : L"";
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
			rcBox.bottom = max(rcBox.bottom, rcText.bottom);
			if (pDisplayList)
			{
				rcDI.right = rcBox.right; rcDI.bottom = rcText.bottom; rcDI.InflateRect(5, 0); 
				CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)&rProductPart, DISPLAY_ITEM_REGISTRY(CProductDataView, PaperData), (void*)nProductPartIndex, CDisplayItem::ForceRegister); // register item into display list
				if (pDI)
					pDI->SetMouseOverFeedback();
			}
			rcText.OffsetRect(0, Pixel2MM(pDC, 20));
		}

		//rcText.top	= rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
		//rcText.left = rcText.left + nPaperDataWidth; rcText.right = rcText.left + nAmountDataWidth;
		rcText.OffsetRect(0, nTextHeight/2);
		CRect rcText2 = rcText; rcText2.left = rcText.left + Pixel2MM(pDC, nTab3); rcText2.right = rcFrame.right;
		rcDI = CRect(rcText.TopLeft(), CSize(0,0));
		//if (rProductPart.m_nAmount > 0)
		{
			strOut2.LoadStringA(IDS_AMOUNT); 
			strOut.Format(_T("%s:"), strOut2);
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			if (rProductPart.m_nAmount > 0)
				strOut.Format(_T("%ld"), rProductPart.m_nAmount);
			else
				strOut = L"-";
			pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
			rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
			rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight);
		}

		//if (rProductPart.m_nExtra > 0)
		{
			strOut2.LoadStringA(IDS_PAPERLIST_EXTRA); 
			strOut.Format(_T("%s:"), strOut2);
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			if (rProductPart.m_nExtra > 0)
				strOut.Format(_T("%ld"), rProductPart.m_nExtra);
			else
				strOut = L"-";
			pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
			rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
			if (pDisplayList)
			{
				rcDI.right = rcBox.right; rcDI.bottom = rcText.bottom; rcDI.InflateRect(5, 0); 	
				CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductDataView, VolumeData), NULL, CDisplayItem::ForceRegister); // register item into display list
				if (pDI)
					pDI->SetMouseOverFeedback();
			}
			rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight);
		}

		/////// trims
		rcText.top	= rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
		rcText.left	= rcText.left + nPaperDataWidth; rcText.right = rcText.left + nTrimDataWidth;
		rcText2		= rcText; rcText2.left = rcText.left + Pixel2MM(pDC, nTab1); rcText2.right = rcFrame.right;
		rcDI = CRect(rcText.TopLeft(), CSize(1,1));
		int nTrimDataBottom = rcDI.bottom; 

		strOut.LoadStringA(IDS_HEAD); strOut += L":";
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		if (fabs(rProductPart.m_prodDataBound.m_fHeadTrim) > 0.0f)
			CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fHeadTrim, TRUE);
		else
			strOut = L"-";
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 

		strOut.LoadStringA(IDS_SIDE); strOut += L":";
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		if (fabs(rProductPart.m_prodDataBound.m_fSideTrim) > 0.0f)
			CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fSideTrim, TRUE);
		else
			strOut = L"-";
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 

		strOut.LoadStringA(IDS_FOOT); strOut += L":";
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		if (fabs(rProductPart.m_prodDataBound.m_fFootTrim) > 0.0f)
			CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fFootTrim, TRUE);
		else
			strOut = L"-";
		pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
		rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
		nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
		rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 

		////// bleed
		//if (fabs(rProductPart.m_prodDataBound.m_fBleed) > 0.0f)
		{
			strOut.LoadStringA(IDS_BLEED); strOut += L":";
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			if (fabs(rProductPart.m_prodDataBound.m_fBleed) > 0.0f)
				CPrintSheetView::TruncateNumber(strOut, rProductPart.m_prodDataBound.m_fBleed, TRUE);
			else
				strOut = L"-";
			pDC->DrawText(strOut, rcText2, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
			rcBox.right	 = max(rcBox.right, rcText2.left + pDC->GetTextExtent(strOut).cx);
			rcBox.bottom = max(rcBox.bottom, rcText2.bottom);
			nTrimDataBottom = max(nTrimDataBottom, rcText2.bottom);
			rcText.OffsetRect(0, nTextHeight); rcText2.OffsetRect(0, nTextHeight); 
		}

		if (pDisplayList)
		{
			rcDI.BottomRight() = CPoint(rcBox.right, nTrimDataBottom); rcDI.InflateRect(5, 0);
			rcBox.bottom += 4;
			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nProductPartIndex, DISPLAY_ITEM_REGISTRY(CProductDataView, TrimData), NULL, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
				pDI->SetMouseOverFeedback();
		}

		//rcText.left = rcBoxThumbnail.right + Pixel2MM(pDC, 20); rcText.top = rcBoxThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
		//if (rProductPart.m_strFormatName != pDoc->m_FinalProductDescription.m_strFormatName)
		//{
		//	CString strWidth, strHeight;
		//	CPrintSheetView::TruncateNumber(strWidth,  rProductPart.m_fPageWidth);
		//	CPrintSheetView::TruncateNumber(strHeight, rProductPart.m_fPageHeight);
		//	strOut.Format(_T("%s (%s x %s)"), rProductPart.m_strFormatName, strWidth, strHeight);
		//	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		//	rcBox.right	 = max(rcBox.right, rcText.left + pDC->GetTextExtent(strOut).cx);
		//	rcBox.bottom = max(rcBox.bottom, rcText.bottom);
		//	rcText.OffsetRect(0, Pixel2MM(pDC, 20));
		//}

		CPen pen(PS_SOLID, 1, MIDDLEGRAY);
		CPen*	pOldPen		= pDC->SelectObject(&pen);
		CBrush* pOldBrush	= (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

		CRect rcBorder = rcTitle; rcBorder.bottom = rcBox.bottom;
		pDC->Rectangle(rcBorder);

		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		pen.DeleteObject();

		rcRow.top = rcBox.bottom + Pixel2MM(pDC, 10); rcRow.bottom = rcRow.top + nRowHeight;
		rcText.left = rcRow.left + Pixel2MM(pDC, 10); rcText.top = rcRow.top; rcText.bottom = rcText.top + nTextHeight;  rcText.right = rcText.left + nProductPartNameWidth;

		rcTotalBox.UnionRect(rcTotalBox, rcBox);
		rcBox = CRect(rcText.TopLeft(), CSize(1,1));
	}

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcTotalBox;
}
//
//CRect CProductPartList::DrawUnbound(CDC* pDC, CRect rcFrame, int nProductPartIndex, CDisplayList* pDisplayList)
//{
//	CRect rcBox(rcFrame.TopLeft(), CSize(0, 0));
//
//	if (GetSize() <= 0)
//		return rcBox;
//
//	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
//	if ( ! pDoc)
//		return rcBox;
//	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
//		return rcBox;
//	if (pDoc->m_labelList.GetSize() <= 0)
//		return rcBox;
//
//	int nThumbnailWidth	= Pixel2MM(pDC, 120);
//	int nTrimDataWidth	= Pixel2MM(pDC, 120);
//
//	float fRatio	= (pDC->IsPrinting()) ? 0.85f : 1.0f;
//	nThumbnailWidth	= (int)((float)nThumbnailWidth	* fRatio);
//	nTrimDataWidth	= (int)((float)nTrimDataWidth	* fRatio);
//
//	CProductPart& rProductPart = GetAt(nProductPartIndex);
//
//	COLORREF crValueColor = RGB(50,50,50);
//
//	CFont	boldFont, font;
//	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 10) : 11, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
//	font.CreateFont		((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
//	CFont* pOldFont = pDC->SelectObject(&boldFont);
//	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
//	pDC->SetBkMode(TRANSPARENT);
//
//	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;
//
//	CRect rcRow		  = rcFrame; rcRow.left += Pixel2MM(pDC, 10);
//	CRect rcThumbnail = rcRow; 
//	float fMaxHeight  = pDoc->m_labelList.GetMaxHeight();
//
//	CString strOut, strOut2;
//	for (int i = 0; i < pDoc->m_labelList.GetSize(); i++)
//	{
//		rcThumbnail.top	   = rcRow.top;
//		rcThumbnail.bottom = rcThumbnail.top + (int)((float)rcRow.Height() * (pDoc->m_labelList[i].m_fLabelHeight / fMaxHeight));		
//		CRect rcBoxThumbnail = pDoc->m_PageTemplateList.DrawSinglePage(pDC, rcThumbnail, i*2, nProductPartIndex);
//		rcBox.UnionRect(rcBox, rcBoxThumbnail);
//		if (pDisplayList)
//		{
//			CRect rcDI = rcBoxThumbnail; rcDI.InflateRect(5, 5);
//			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)i, DISPLAY_ITEM_REGISTRY(CProductDataView, ProductPartLabel), (void*)nProductPartIndex, CDisplayItem::ForceRegister); // register item into display list
//			if (pDI)
//				pDI->SetMouseOverFeedback();
//		}
//
//		int   nMaxRight = rcBoxThumbnail.right;
//		CRect rcText;
//		rcText.left = rcBoxThumbnail.right + Pixel2MM(pDC, 10); rcText.top = rcRow.top; rcText.bottom = rcText.top + nTextHeight;  rcText.right = rcText.left + nTrimDataWidth;
//		//rcText.left = rcBoxThumbnail.left; rcText.top = rcBoxThumbnail.bottom + Pixel2MM(pDC, 10); rcText.bottom = rcText.top + nTextHeight;  rcText.right = rcText.left + nTrimDataWidth;
//
//		pDC->SelectObject(&boldFont);
//		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
//
//		strOut.Format(_T("%s"), pDoc->m_labelList[i].m_strName);
//		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
//		nMaxRight = max(nMaxRight, rcText.left + pDC->GetTextExtent(strOut).cx);
//
//		pDC->SelectObject(&font);
//		pDC->SetTextColor(crValueColor);
//
//		rcText.OffsetRect(0, nTextHeight);
//		CString strWidth, strHeight;
//		CPrintSheetView::TruncateNumber(strWidth, pDoc->m_labelList[i].m_fLabelWidth); CPrintSheetView::TruncateNumber(strHeight, pDoc->m_labelList[i].m_fLabelHeight, TRUE);
//		strOut.Format(_T("%s x %s"), strWidth, strHeight);
//		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
//		nMaxRight = max(nMaxRight, rcText.left + pDC->GetTextExtent(strOut).cx);
//
//		if ( ! pDoc->m_labelList[i].m_strComment.IsEmpty())
//		{
//			rcText.OffsetRect(0, nTextHeight);
//			strOut = pDoc->m_labelList[i].m_strComment;
//			pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
//			nMaxRight = max(nMaxRight, rcText.left + pDC->GetTextExtent(strOut).cx);
//			rcBox.right	 = max(rcBox.right, nMaxRight);
//			rcBox.bottom = max(rcBox.bottom, rcText.bottom);
//		}
//
//		nMaxRight = min(nMaxRight, rcText.right);
//		if (pDisplayList)
//		{
//			CRect rcDI(CPoint(rcText.left, rcRow.top), CPoint(nMaxRight, rcText.bottom)); rcDI.InflateRect(5, 5);
//			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)i, DISPLAY_ITEM_REGISTRY(CProductDataView, LabelData), (void*)nProductPartIndex, CDisplayItem::ForceRegister); // register item into display list
//			if (pDI)
//				pDI->SetMouseOverFeedback();
//		}
//
//		rcThumbnail.left = nMaxRight + Pixel2MM(pDC, 40);
//
//		if (i < pDoc->m_labelList.GetSize() - 1)
//		{
//			int nNextThumbnailHeight = (int)((float)rcRow.Height() * (pDoc->m_labelList[i + 1].m_fLabelHeight / fMaxHeight));
//			int nNextThumbnailWidth  = (int)((pDoc->m_labelList[i + 1].m_fLabelWidth / pDoc->m_labelList[i + 1].m_fLabelHeight) * (float)nNextThumbnailHeight);
//
//			if ( (rcBoxThumbnail.right + Pixel2MM(pDC, 40) + nNextThumbnailWidth +  + Pixel2MM(pDC, 20) + nTrimDataWidth) >= rcFrame.right)	// next row
//			{
//				rcRow.top = rcBox.bottom + Pixel2MM(pDC, 10); rcRow.bottom = rcRow.top + rcFrame.Height();
//				rcThumbnail.left = rcRow.left;
//			}
//		}
//	}
//
//	pDC->SelectObject(pOldFont);
//
//	font.DeleteObject();
//	boldFont.DeleteObject();
//
//	return rcBox;
//}

int	_nSelComponentIndex	= -1;

CRect CProductPartList::DrawFoldSheetComponentList(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(0,0));

	m_printSheetInfos.RemoveAll();;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;
	if ( ! HasBoundedProductPart())
		return rcBox;

	CString strOut, strOut2;
	CFont	boldFont;
	boldFont.CreateFont (Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD,	 FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(205,220,237));
	pDC->SelectStockObject(HOLLOW_BRUSH);

	CRect rcTitle;
	rcTitle.left = rcFrame.left; rcTitle.top = rcFrame.top; rcTitle.right = rcFrame.right; rcTitle.bottom = rcTitle.top + Pixel2MM(pDC, 15);

	strOut.LoadStringA(IDS_SHEETPLANNING); strOut2.LoadString(IDS_FOLDSHEET_LABEL); strOut += _T(" | ") + strOut2;
	pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcTitle.right = rcTitle.left + pDC->GetTextExtent(strOut).cx;
	rcBox.UnionRect(rcBox, rcTitle);

	boldFont.DeleteObject();

	CPen  foldSheetPen (PS_SOLID, 1, RGB(255,165,0));
	CPen  printSheetPen(PS_SOLID, 2, RGB(200,200,200));//RGB(200,200,240));
	CPen* pOldPen = pDC->SelectObject(&printSheetPen);

	CPrintSheetPlanningView* pPlanningView		  = NULL;
	BOOL					 bNumeratePages		  = FALSE;
	BOOL					 bNumerateFoldSheets  = FALSE;
	BOOL					 bNumeratePrintSheets = FALSE;
	if (pDisplayList)
		if (pDisplayList->m_pParent)
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
			{
				pPlanningView	     = (CPrintSheetPlanningView*)pDisplayList->m_pParent;
				if (pPlanningView->m_nLastSelectedFoldSheetComponentIndex == -1)
					pPlanningView->m_nLastSelectedFoldSheetComponentIndex = pPlanningView->GetSelectedComponentIndex();
				bNumeratePages		 = (pPlanningView->IsOpenPageNumeration())		 ? TRUE : FALSE;
				bNumerateFoldSheets  = (pPlanningView->IsOpenFoldSheetNumeration())  ? TRUE : FALSE;
				bNumeratePrintSheets = (pPlanningView->IsOpenPrintSheetNumeration()) ? TRUE : FALSE;
			}

	int	  nFoldSheetRowHeight  = Pixel2MM(pDC, 25);
	int	  nPrintSheetRowHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 60) : Pixel2MM(pDC, 100);
	CRect rcFoldSheetRow; rcFoldSheetRow.left = rcFrame.left, rcFoldSheetRow.top = rcTitle.bottom + Pixel2MM(pDC, 10); rcFoldSheetRow.right = rcFrame.right; rcFoldSheetRow.bottom = rcFoldSheetRow.top + nFoldSheetRowHeight;
	CRect rcPrintSheetRow(rcFoldSheetRow.TopLeft(), CSize(rcFoldSheetRow.Width(), nPrintSheetRowHeight));

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, -1);
	for (int nProductPartIndex = 0; nProductPartIndex < pDoc->m_productPartList.GetSize(); nProductPartIndex++)
	{
		int nUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(nProductPartIndex);
		if (nUnImposedPages > 0)
		{
			CFoldSheet*	pDummyFoldSheet = new CFoldSheet;	
			pDummyFoldSheet->m_nProductPartIndex = nProductPartIndex;
			sortedFoldSheets.Add(pDummyFoldSheet);
		}
	}

	int	 nOffset			  = 0;
	int	 nPrintSheetRowIndex  = 0;
	int  nComponentIndex	  = 0;
	int	 nPrintSheetIndex	  = 0;
	CLayout* pPrevLayout	  = NULL;
	for (int i = 0; i < sortedFoldSheets.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = sortedFoldSheets[i];
		if ( ! pFoldSheet) 
			continue;

		for (int nLayerIndex = 0; (nLayerIndex < pFoldSheet->m_LayerList.GetSize()) || pFoldSheet->IsEmpty(); nLayerIndex++)
		{
			CPrintSheet*  pPrintSheet	= NULL;
			CLayout*	  pLayout		= NULL;
			CPressDevice* pPressDevice	= NULL;  
			if ( ! pFoldSheet->IsEmpty())		// unassigned pages
			{
				CFoldSheetLayer* pLayer = pFoldSheet->GetLayer(nLayerIndex);
				pPrintSheet				= (pLayer) ? pLayer->GetFirstPrintSheet() : NULL;
				if (pPrintSheet)
					pPrintSheet->InitializeFoldSheetTypeList();
				pLayout					= (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
				pPressDevice			= (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
				if ( ! pLayout || ! pPressDevice)
					continue;
			}

			CPrintSheet* pPrevPrintSheet = (m_printSheetInfos.GetSize()) ? m_printSheetInfos[m_printSheetInfos.GetSize() - 1].m_pPrintSheet : NULL;
			if ( (pPrintSheet != pPrevPrintSheet) || ! pPrintSheet)
			{
				rcFoldSheetRow.top	   = max(rcFoldSheetRow.top, rcPrintSheetRow.top); rcFoldSheetRow.bottom = rcFoldSheetRow.top + nFoldSheetRowHeight;
				rcPrintSheetRow.top	   = max(rcPrintSheetRow.top, rcFoldSheetRow.top);
				rcPrintSheetRow.bottom = rcPrintSheetRow.top + nPrintSheetRowHeight;
				if (nPrintSheetIndex > 0)
				{
					pDC->MoveTo(rcFoldSheetRow.left, rcPrintSheetRow.top - Pixel2MM(pDC, 3)); pDC->LineTo(rcPrintSheetRow.right, rcPrintSheetRow.top - Pixel2MM(pDC, 3));
				}
			}

			CRect rcFoldSheetBox = DrawFoldSheetComponent(pDC, rcFoldSheetRow, pFoldSheet, nLayerIndex, pPrintSheet, nComponentIndex, pDisplayList);
			//pDC->SelectObject(&foldSheetPen);
			//pDC->Rectangle(rcFoldSheetBox);
			//pDC->SelectObject(&printSheetPen);
			rcBox.UnionRect(rcBox, rcFoldSheetBox);

			if (pDisplayList && ( ! bNumeratePages && ! bNumerateFoldSheets && ! bNumeratePrintSheets) )
			{
				CDisplayItem* pDI = NULL;
				CRect rcDI = rcFoldSheetRow; rcDI.bottom = rcFoldSheetBox.bottom; rcDI.left -= 5; rcDI.right = rcFoldSheetBox.right; rcDI.bottom += 5;
				if (pFoldSheet->IsEmpty())
					pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pFoldSheet->m_nProductPartIndex, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, UnassignedPages), (void*)NULL, CDisplayItem::ForceRegister); // register item into display list
				else
					pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pFoldSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, FoldSheetComponent), (void*)nLayerIndex, CDisplayItem::ForceRegister); // register item into display list
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
					pDI->m_nState = (nComponentIndex == pPlanningView->m_nLastSelectedFoldSheetComponentIndex) ? CDisplayItem::Selected : CDisplayItem::Normal;
					if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
					{
						CRect rcFeedback = pDI->m_BoundingRect;
						rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
						Graphics graphics(pDC->m_hDC);
						Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
						COLORREF crHighlightColor = RGB(255,195,15);
						SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
						graphics.FillRectangle(&br, rc);
						graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
					}
				}
			}

			rcFoldSheetRow.top	  = rcFoldSheetBox.bottom + Pixel2MM(pDC, 10);
			rcFoldSheetRow.bottom = rcFoldSheetRow.top + nFoldSheetRowHeight;

			if ( (pPrintSheet != pPrevPrintSheet) || ! pPrintSheet)
			{
				if (PrintSheetNotYetListed(pPrintSheet))
				{
					int nLayoutIndex = 0;
					if (pLayout)
					{
						POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
						while (pos)
						{
							if (pLayout == &pDoc->m_PrintSheetList.m_Layouts.GetNext(pos))
								break;
							nLayoutIndex++;
						}
					}

					int nLeft = Pixel2MM(pDC, 360/*360*/ + 20);//rcFoldSheetBox.right + Pixel2MM(pDC, 20);
					//nLeft += nLayoutIndex * 200;
					CRect rcPrintSheetBox = DrawPrintSheetPart(pDC, CRect(nLeft, rcPrintSheetRow.top, rcPrintSheetRow.right, rcPrintSheetRow.bottom), pPrintSheet, pLayout, 
															   pPressDevice, nPrintSheetIndex, pFoldSheet->m_nProductPartIndex, (pPrevLayout != pLayout) ? TRUE : FALSE, (bNumeratePages || bNumerateFoldSheets) ? NULL : pDisplayList);
					rcBox.UnionRect(rcBox, rcPrintSheetBox);
					rcPrintSheetRow.top	   = rcPrintSheetBox.bottom + Pixel2MM(pDC, 10);
					rcPrintSheetRow.bottom = rcPrintSheetRow.top + nPrintSheetRowHeight;
					if (pPrintSheet)
					{
						nPrintSheetIndex++;
						CComponentPrintSheetInfo psi; psi.m_pPrintSheet = pPrintSheet; psi.m_rcDisplay = rcPrintSheetBox;
						m_printSheetInfos.Add(psi);
					}
					pPrevLayout = pLayout;
				}
				else
				{
					CRect rcDisplay = GetPrintSheetDisplayRect(pPrintSheet);
					if ( ! rcDisplay.IsRectNull())
					{
						Pen pen(Color::Color(255,185,50));
						Graphics graphics(pDC->m_hDC);
						graphics.SetSmoothingMode(SmoothingModeAntiAlias);
						graphics.DrawLine(&pen,	rcFoldSheetBox.right + Pixel2MM(pDC, 40), rcFoldSheetBox.CenterPoint().y, 
												rcFoldSheetBox.right + Pixel2MM(pDC, 50 + nOffset), rcFoldSheetBox.CenterPoint().y);
						graphics.DrawLine(&pen,	rcFoldSheetBox.right + Pixel2MM(pDC, 50 + nOffset), rcFoldSheetBox.CenterPoint().y, 
												rcDisplay.left - Pixel2MM(pDC, 50 - nOffset), rcDisplay.CenterPoint().y); 
						graphics.DrawLine(&pen,	rcDisplay.left - Pixel2MM(pDC, 50 - nOffset), rcDisplay.CenterPoint().y,
												rcDisplay.left - Pixel2MM(pDC, 0), rcDisplay.CenterPoint().y);
						nOffset += 5;
					}
				}
			}
			nComponentIndex++;

			if (pFoldSheet->IsEmpty())
				break;
		}
	}

	pDC->SelectObject(pOldPen);
	foldSheetPen.DeleteObject();
	printSheetPen.DeleteObject();

	return rcBox;
}

CRect CProductPartList::DrawLabelComponentList(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(0,0));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return rcBox;
	if ( ! HasUnboundedProductPart())
		return rcBox;

	CString strOut, strOut2;
	CFont	boldFont, font;
	boldFont.CreateFont (Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD,	 FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	font.CreateFont		(Pixel2MM(pDC, 11), 0, 0, 0, FW_BOLD,	 FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(205,220,237));

	CRect rcTitle;
	rcTitle.left = rcFrame.left; rcTitle.top = rcFrame.top; rcTitle.right = rcFrame.right; rcTitle.bottom = rcTitle.top + Pixel2MM(pDC, 15);

	strOut.LoadStringA(IDS_SHEETPLANNING); strOut2.LoadStringA(IDS_UNBOUND); strOut += _T(" | ") + strOut2;
	pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	rcTitle.right = rcTitle.left + pDC->GetTextExtent(strOut).cx;
	rcBox.UnionRect(rcBox, rcTitle);

	pDC->SetBkMode(TRANSPARENT);

	CPen  foldSheetPen (PS_SOLID, 1, RGB(255,165,0));
	CPen  printSheetPen(PS_SOLID, 1, RGB(200,200,240));
	CPen* pOldPen = pDC->SelectObject(&printSheetPen);

	int						 nSelComponentIndex	  = -1;
	CPrintSheetPlanningView* pPlanningView		  = NULL;
	BOOL					 bNumeratePrintSheets = FALSE;
	if (pDisplayList)
		if (pDisplayList->m_pParent)
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
			{
				pPlanningView	     = (CPrintSheetPlanningView*)pDisplayList->m_pParent;
				nSelComponentIndex   = (pPlanningView) ? pPlanningView->GetSelectedComponentIndex() : -1;
				bNumeratePrintSheets = (pPlanningView) ? ((pPlanningView->IsOpenPrintSheetNumeration()) ? TRUE : FALSE) : FALSE;
			}

	int	  nLabelRowHeight = Pixel2MM(pDC, 35);
	int	  nPrintSheetRowHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 60) : Pixel2MM(pDC, 100);
	CRect rcLabelRow; rcLabelRow.left = rcFrame.left, rcLabelRow.top = rcTitle.bottom + Pixel2MM(pDC, 10); rcLabelRow.right = rcFrame.right; rcLabelRow.bottom = rcLabelRow.top + nLabelRowHeight;
	CRect rcPrintSheetRow(rcLabelRow.TopLeft(), CSize(rcLabelRow.Width(), nPrintSheetRowHeight));

	CArray <CLabelListEntry*, CLabelListEntry*> sortedLabelList;
	for (int i = 0; i < pDoc->m_labelList.GetSize(); i++)
	{
		if (pDoc->m_labelList[i].GetFirstPrintSheet())
			sortedLabelList.Add(&pDoc->m_labelList[i]);
	}
	for (int i = 0; i < pDoc->m_labelList.GetSize(); i++)
	{
		if ( ! pDoc->m_labelList[i].GetFirstPrintSheet())
			sortedLabelList.Add(&pDoc->m_labelList[i]);
	}

	float fMaxHeight  = pDoc->m_labelList.GetMaxHeight();

	int	 nPrintSheetRowIndex  = 0;
	int  nComponentIndex	  = 0;
	int	 nPrintSheetIndex	  = 0;
	int  nLabelBoxMaxBottom	  = 0;
	BOOL bUnassignedMode	  = FALSE;
	for (int i = 0; i < sortedLabelList.GetSize(); i++)
	{
		CLabelListEntry* pLabelListEntry = sortedLabelList[i];
		if ( ! pLabelListEntry) 
			continue;

		CPrintSheet*  pPrintSheet	  = pLabelListEntry->GetFirstPrintSheet();
		CLayout*	  pLayout		  = NULL;
		CLayout*	  pPrevLayout	  = NULL;
		CPressDevice* pPressDevice	  = NULL;
		CPrintSheet*  pPrevPrintSheet = (m_printSheetInfos.GetSize()) ? m_printSheetInfos[m_printSheetInfos.GetSize() - 1].m_pPrintSheet : NULL;;
		if (pPrintSheet)		// assigned label
		{
			pLayout					= (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			pPressDevice			= (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
			if ( ! pLayout || ! pPressDevice)
				continue;

			if (pPrintSheet != pPrevPrintSheet)
			{
				rcLabelRow.left		   = rcFrame.left; rcLabelRow.right = rcFrame.right; 	
				rcLabelRow.top		   = rcLabelRow.top			= rcBox.bottom + Pixel2MM(pDC, 20);/*max(rcLabelRow.top, rcPrintSheetRow.top);*/ rcLabelRow.bottom = rcLabelRow.top + nLabelRowHeight;
				rcPrintSheetRow.top	   = max(rcPrintSheetRow.top, rcLabelRow.top);
				rcPrintSheetRow.bottom = rcPrintSheetRow.top + nPrintSheetRowHeight;
				if (nPrintSheetIndex > 0)
				{
					pDC->MoveTo(rcLabelRow.left, rcPrintSheetRow.top - Pixel2MM(pDC, 3)); pDC->LineTo(rcPrintSheetRow.right, rcPrintSheetRow.top - Pixel2MM(pDC, 3));
				}
			}
		}
		else	// unassigned
		{
			if ( ! bUnassignedMode)
			{
				rcLabelRow.left		= rcFrame.left;		rcLabelRow.top			= rcBox.bottom + Pixel2MM(pDC, 30); rcLabelRow.bottom = rcLabelRow.top + nLabelRowHeight;
				rcPrintSheetRow.top	= rcLabelRow.top;	rcPrintSheetRow.bottom	= rcPrintSheetRow.top + nPrintSheetRowHeight;
				pDC->SelectObject(&font);
				rcTitle = rcLabelRow; rcTitle.top = rcLabelRow.top - Pixel2MM(pDC, 15); rcTitle.bottom = rcLabelRow.top;
				strOut.LoadStringA(IDS_NOT_ASSIGNED);
				pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
				pDC->MoveTo(rcLabelRow.left, rcPrintSheetRow.top - Pixel2MM(pDC, 3)); pDC->LineTo(rcPrintSheetRow.right, rcPrintSheetRow.top - Pixel2MM(pDC, 3));
				rcLabelRow.OffsetRect(0, Pixel2MM(pDC, 10));
			}
			bUnassignedMode	= TRUE;
		}

		CRect rcLabelBox  = DrawLabelComponent(pDC, rcLabelRow, pLabelListEntry, pDisplayList);
		rcBox.UnionRect(rcBox, rcLabelBox);
		nLabelBoxMaxBottom = max(nLabelBoxMaxBottom, rcLabelBox.bottom);

		if (pDisplayList && ! bNumeratePrintSheets)
		{
			CDisplayItem* pDI = NULL;
			CRect rcDI = rcLabelBox; rcDI.InflateRect(5, 5);
			if ( ! pPrintSheet)
				pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pLabelListEntry, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, UnassignedLabel), (void*)NULL, CDisplayItem::ForceRegister); // register item into display list
			else
				pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pLabelListEntry, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, LabelComponent), (void*)pPrintSheet, CDisplayItem::ForceRegister); // register item into display list
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
				{
					CRect rcFeedback = pDI->m_BoundingRect;
					rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
					Graphics graphics(pDC->m_hDC);
					Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
					COLORREF crHighlightColor = RGB(255,195,15);
					SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
					graphics.FillRectangle(&br, rc);
					graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				}
			}
		}

		rcLabelRow.left = rcLabelBox.right + Pixel2MM(pDC, 20);

		if (i < sortedLabelList.GetSize() - 1)
		{
			int nNextThumbnailHeight = (int)((float)rcLabelRow.Height() * (sortedLabelList[i + 1]->m_fLabelHeight / fMaxHeight));
			int nNextThumbnailWidth  = (int)((sortedLabelList[i + 1]->m_fLabelWidth / sortedLabelList[i + 1]->m_fLabelHeight) * (float)nNextThumbnailHeight);

			if ( (rcLabelBox.right + Pixel2MM(pDC, 40) + nNextThumbnailWidth +  + Pixel2MM(pDC, 120)/* + nTrimDataWidth*/) >= rcFrame.right)	// next row
			{
				rcLabelRow.top  = nLabelBoxMaxBottom + Pixel2MM(pDC, 10); rcLabelRow.bottom = rcLabelRow.top + nLabelRowHeight;
				rcLabelRow.left = rcFrame.left;
			}
		}

		if (pPrintSheet != pPrevPrintSheet)
		{
			if (PrintSheetNotYetListed(pPrintSheet))
			{
				CRect rcPrintSheetBox = DrawPrintSheetPart(pDC, CRect(rcFrame.right + Pixel2MM(pDC, 20), rcPrintSheetRow.top, rcPrintSheetRow.right, rcPrintSheetRow.bottom), pPrintSheet, pLayout, 
					pPressDevice, nPrintSheetIndex, pDoc->m_productPartList.GetLabelsProductPartIndex(), (pPrevLayout != pLayout) ? TRUE : FALSE, pDisplayList);
				rcBox.UnionRect(rcBox, rcPrintSheetBox);
				rcPrintSheetRow.top	   = rcPrintSheetBox.bottom + Pixel2MM(pDC, 10);
				rcPrintSheetRow.bottom = rcPrintSheetRow.top + nPrintSheetRowHeight;
				nPrintSheetIndex++;
		
				CComponentPrintSheetInfo psi; psi.m_pPrintSheet = pPrintSheet; psi.m_rcDisplay = rcPrintSheetBox;
				m_printSheetInfos.Add(psi);
				pPrevLayout = pLayout;
			}
			else
			{
				CRect rcDisplay = GetPrintSheetDisplayRect(pPrintSheet);
				if ( ! rcDisplay.IsRectNull())
				{
					Graphics graphics(pDC->m_hDC);
					graphics.SetSmoothingMode(SmoothingModeAntiAlias);
					graphics.DrawLine(&Pen(Color::Color(255,180,100)), rcDisplay.left - Pixel2MM(pDC, 30), rcLabelBox.CenterPoint().y, rcDisplay.left - Pixel2MM(pDC, 20), rcLabelBox.CenterPoint().y);
					graphics.DrawLine(&Pen(Color::Color(255,180,100)), rcDisplay.left - Pixel2MM(pDC, 20), rcLabelBox.CenterPoint().y, rcDisplay.left, rcDisplay.CenterPoint().y);

					//pDC->SelectObject(&foldSheetPen);
					//pDC->MoveTo(rcDisplay.left - Pixel2MM(pDC, 30), rcLabelBox.CenterPoint().y);
					//pDC->LineTo(rcDisplay.left - Pixel2MM(pDC, 20), rcLabelBox.CenterPoint().y); 
					//pDC->LineTo(rcDisplay.left - Pixel2MM(pDC, 10), rcDisplay.CenterPoint().y);
					//pDC->LineTo(rcDisplay.left, rcDisplay.CenterPoint().y);
					//pDC->SelectObject(&printSheetPen);
				}
			}
		}
		//nComponentIndex++;
	}

	pDC->SelectObject(pOldPen);
	foldSheetPen.DeleteObject();
	printSheetPen.DeleteObject();
	boldFont.DeleteObject();
	font.DeleteObject();

	return rcBox;
}

BOOL CProductPartList::PrintSheetNotYetListed(CPrintSheet* pPrintSheet)
{
	for (int i = 0; i < m_printSheetInfos.GetSize(); i++)
	{
		if (pPrintSheet == m_printSheetInfos[i].m_pPrintSheet)
			return FALSE;
	}
	return TRUE;
}

CRect CProductPartList::GetPrintSheetDisplayRect(CPrintSheet* pPrintSheet)
{
	for (int i = 0; i < m_printSheetInfos.GetSize(); i++)
	{
		if (pPrintSheet == m_printSheetInfos[i].m_pPrintSheet)
			return m_printSheetInfos[i].m_rcDisplay;
	}
	return CRect(0,0,0,0);
}

CRect CProductPartList::GetFoldSheetComponentBox(CDC* pDC, CRect rcFrame, CFoldSheet* pFoldSheet, int nLayerIndex, CPrintSheet* pPrintSheet, int nComponentIndex)
{
	CDC dc;
	dc.CreateCompatibleDC(pDC);
	return DrawFoldSheetComponent(&dc, rcFrame, pFoldSheet, nLayerIndex, pPrintSheet, nComponentIndex, NULL, TRUE);
}

CRect CProductPartList::DrawFoldSheetComponent(CDC* pDC, CRect rcFrame, CFoldSheet* pFoldSheet, int nLayerIndex, CPrintSheet* pPrintSheet, int nComponentIndex, CDisplayList* pDisplayList, BOOL bGetBoxOnly)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(rcFrame.TopLeft(), CSize(0, 0));

	BOOL bBlankFoldSheet = (pFoldSheet) ? ((pFoldSheet->IsEmpty()) ? TRUE : FALSE) : FALSE;

	CPrintSheetPlanningView* pPlanningView = NULL;
	BOOL bPageNumeration	  = FALSE; 
	BOOL bFoldSheetNumeration = FALSE;
	if (pDisplayList)
		if (pDisplayList->m_pParent)
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
			{
				CPrintSheetPlanningView* pPlanningView = (CPrintSheetPlanningView*)pDisplayList->m_pParent;
				if ( ! bBlankFoldSheet)
				{
					bPageNumeration		 = (pPlanningView->IsOpenPageNumeration())		? TRUE : FALSE;
					bFoldSheetNumeration = (pPlanningView->IsOpenFoldSheetNumeration()) ? TRUE : FALSE;
				}
			}

	int nProductPartWidth	= Pixel2MM(pDC, 160);
	int nThumbnailWidth		= Pixel2MM(pDC,  60);
	int nFoldSheetWidth		= Pixel2MM(pDC, 200);
	int nGap				= Pixel2MM(pDC,  10);
	int nTextHeight			= Pixel2MM(pDC,  15);

	float fRatio = (pDC->IsPrinting()) ? 0.85f : 1.0f;
	nProductPartWidth	= (int)((float)nProductPartWidth * fRatio);
	nThumbnailWidth		= (int)((float)nThumbnailWidth	 * fRatio);
	nFoldSheetWidth		= (int)((float)nFoldSheetWidth	 * fRatio);
														 
	CRect rcBox(0,0,0,0);
	if ( ! bGetBoxOnly)	// draw 
	{
		rcBox = GetFoldSheetComponentBox(pDC, rcFrame, pFoldSheet, nLayerIndex, pPrintSheet, nComponentIndex);

		CRect rcTitle = rcBox; rcTitle.InflateRect(Pixel2MM(pDC, 5), 0); /*rcTitle.right = rcTitle.left + nThumbnailWidth + nGap + nFoldSheetWidth;*/ rcTitle.bottom = rcTitle.top + nTextHeight;

		CPen	pen(PS_SOLID, 1, RGB(200,200,200));
		CPen*	pOldPen		= pDC->SelectObject(&pen);
		CBrush* pOldBrush	= (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

		CRect rcBorder = rcTitle; rcBorder.bottom = rcBox.bottom + Pixel2MM(pDC, 5);
		pDC->FillSolidRect(rcBorder, RGB(250,250,250));
		rcBox = rcBorder;

		CGraphicComponent gp;
		gp.DrawTitleBar(pDC, rcTitle, L"", WHITE, RGB(240,240,240), 90, -1);

		pDC->Rectangle(rcBorder);

		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		pen.DeleteObject();
	}

	rcBox.SetRectEmpty();

	CString strOut, strOut2, strWidth, strHeight;				 
	CRect	rcText = rcFrame;							 
	CRect	rcText2;									 

	COLORREF crValueColor = RGB(50,50,50);

	CFont boldFont, smallFont;
	boldFont.CreateFont	(Pixel2MM(pDC, 12), 0, 0, 0,										   FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 10) : Pixel2MM(pDC, 12), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	///// first column
	///// product part name
	//rcText.top += Pixel2MM(pDC, 5); rcText.bottom = rcText.top + nTextHeight; rcText.right = rcText.left + nProductPartWidth;
	//CRect rcPages = rcText;
	CProductPart* pProductPart = pDoc->m_productPartList.GetPart(pFoldSheet->m_nProductPartIndex);
	//strOut = (pProductPart) ? pProductPart->m_strName : L"";
	//pDC->SetBkMode(OPAQUE);
	//pDC->SetBkColor(RGB(222, 233, 77));
	//pDC->SetTextColor((bBlankFoldSheet) ? MIDDLEGRAY : RGB(100,100,100)); 
	//pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	//rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx;
	//rcBox.UnionRect(rcBox, rcText);
	pDC->SetBkMode(TRANSPARENT); 
	pDC->SelectObject(&boldFont);

	///// header
	///// foldsheet number
	int nLongest = GetLongestFoldSheetNumString(pDC, (bFoldSheetNumeration) ? nComponentIndex : -1);
	nLongest = max(nLongest, nThumbnailWidth);
	rcText.left += Pixel2MM(pDC, 5); rcText.bottom = rcText.top + nTextHeight; rcText.right = rcText.left + nLongest;
	CRect rcPages = rcText;
	strOut = pFoldSheet->GetNumber();
	pDC->SetTextColor(RGB(255, 150, 50));
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.UnionRect(rcBox, rcText);
	if (bFoldSheetNumeration)
	{
		rcText.left = rcText.left + pDC->GetTextExtent(strOut).cx;
		strOut.Format(_T("  (%d)"), nComponentIndex + 1);
		pDC->SetTextColor(MIDDLEGRAY);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx;
		rcBox.UnionRect(rcBox, rcText);
	}

	pDC->SetTextColor(GUICOLOR_CAPTION); 
	pDC->SelectObject(&smallFont);

	///// pages / page range
	rcText.left  = rcText.left + nLongest + nGap;
	rcText.right = rcText.left + nFoldSheetWidth;
	strOut2.LoadStringA(IDS_PAGES);
	strOut.Format(_T("%-2d %s | %s"), (pFoldSheet) ? pFoldSheet->GetNumPages() : 0, strOut2, pFoldSheet->GetPageRange());
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);
	if (bBlankFoldSheet)
	{
		CString strOut3; strOut3.LoadStringA(IDS_NOT_ASSIGNED);
		int nUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(pFoldSheet->m_nProductPartIndex);
		strOut.Format(_T("%-2d %s | %s"), nUnImposedPages, strOut2, strOut3);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		return CRect(rcFrame.left, rcFrame.top, rcFrame.left + nProductPartWidth + nGap + nFoldSheetWidth, rcText2.bottom);
	}

	rcPages.right  = rcText2.right;
	rcPages.bottom = rcText2.bottom;

	pDC->SetTextColor(crValueColor); 

	///// foldsheet thumbnail
	//CRect rcFoldSheet(CPoint(rcText.left + nProductPartWidth + nGap, rcFrame.top + Pixel2MM(pDC, 8)), CSize(nFoldSheetWidth, rcFrame.Height()));
	//CRect rcThumbnail(CPoint(rcText.left + nProductPartWidth + nGap, rcFrame.top + Pixel2MM(pDC, 8)), CSize(Pixel2MM(pDC, 50), rcFrame.Height()));
	CRect rcFoldSheet(CPoint(rcFrame.left + nGap/2, rcText.bottom + Pixel2MM(pDC, 5)), CSize(nFoldSheetWidth, rcFrame.Height()));
	CRect rcThumbnail(CPoint(rcFrame.left + nGap/2, rcText.bottom + Pixel2MM(pDC, 5)), CSize(Pixel2MM(pDC, 50), rcFrame.Height()));
	rcThumbnail = pFoldSheet->DrawThumbnail(pDC, rcThumbnail, TRUE, FALSE, nLayerIndex);
	rcBox.UnionRect(rcBox, rcThumbnail);

	///// scheme name
	rcText.top = rcThumbnail.top; rcText.bottom = rcText.top + nTextHeight;
	strOut = pFoldSheet->m_strFoldSheetTypeName;
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.UnionRect(rcBox, rcText);


	//rcText2 = rcText;
	//rcText2.right = rcText.left + nFoldSheetWidth; rcText2.OffsetRect(0, nTextHeight);
	//CPrintSheetView::TruncateNumber(strWidth, pProductPart->m_fPageWidth); CPrintSheetView::TruncateNumber(strHeight, pProductPart->m_fPageHeight, TRUE);
	//strOut.Format(_T("%s x %s"), strWidth, strHeight);
	////pDC->DrawText(strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	//rcText2.right = rcText2.left + pDC->GetTextExtent(strOut).cx;
	//rcBox.UnionRect(rcBox, rcText2);

	//int nNUp = pFoldSheet->GetNUpPrintLayout(pPrintSheet, nLayerIndex);
	//if (nNUp > 1)
	//{
	//	rcText2.right = rcText.left + nFoldSheetWidth; rcText2.OffsetRect(0, nTextHeight);
	//	strOut.Format("%d Nutzen", nNUp);
	//	pDC->SetTextColor(crValueColor); 
	//	pDC->DrawText(strOut, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	//	rcText2.right = rcText2.left + pDC->GetTextExtent(strOut).cx;
	//	rcBox.UnionRect(rcBox, rcText2);
	//}

	//if ( ! bBlankFoldSheet && (bPageNumeration || bFoldSheetNumeration) )
	//{
	//	CRect		  rcDI = rcBorder;//(bPageNumeration) ? rcPages : rcFoldSheet; rcDI.left -= 5; rcDI.top -= 5;
	//	CDisplayItem* pDI  = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pFoldSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, FoldSheetComponent), (void*)nLayerIndex, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//		if (pPlanningView->IsInsideNumerationRange(nComponentIndex))
	//		{
	//			pDI->m_nState = CDisplayItem::Selected;
	//			CRect rcFeedback = pDI->m_BoundingRect;
	//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
	//			CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
	//		}
	//	}
	//}

	pDC->SelectObject(pOldFont);
	boldFont.DeleteObject();
	smallFont.DeleteObject();

	return rcBox;
	//return CRect(rcBox.TopLeft(), CSize(nProductPartWidth + nGap + nFoldSheetWidth, rcBox.Height()));
}

int CProductPartList::GetLongestFoldSheetNumString(CDC* pDC, int nComponentIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CString  string;
	int		 nMaxLen = 0;
	POSITION pos	 = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (nComponentIndex >= 0)
			string.Format(_T("%s  (%d)"), rFoldSheet.GetNumber(), nComponentIndex);
		else
			string = rFoldSheet.GetNumber();
		int nLen = pDC->GetTextExtent(string).cx;
		nMaxLen = max(nMaxLen, nLen);
	}

	return nMaxLen;
}

int CProductPartList::GetLongestPrintSheetNumString(CDC* pDC, int nPrintSheetIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CString  string;
	int		 nMaxLen = 0;
	POSITION pos	 = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (nPrintSheetIndex >= 0)
			string.Format(_T("%s  (%d)"), rPrintSheet.GetNumber(), nPrintSheetIndex);
		else
			string = rPrintSheet.GetNumber();
		int nLen = pDC->GetTextExtent(string).cx;
		nMaxLen = max(nMaxLen, nLen);
	}

	return nMaxLen;
}

CRect CProductPartList::DrawLabelComponent(CDC* pDC, CRect rcFrame, CLabelListEntry* pLabelListEntry, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(0, 0));

	if (GetSize() <= 0)
		return rcBox;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nThumbnailWidth	= Pixel2MM(pDC, 120);
	int nTrimDataWidth	= Pixel2MM(pDC, 120);

	float fRatio	= (pDC->IsPrinting()) ? 0.85f : 1.0f;
	nThumbnailWidth	= (int)((float)nThumbnailWidth * fRatio);
	nTrimDataWidth	= (int)((float)nTrimDataWidth  * fRatio);

	int	nLabelIndex		  = pDoc->m_labelList.FindLabelIndex(pLabelListEntry);
	int nProductPartIndex = pDoc->m_productPartList.GetLabelsProductPartIndex();

	COLORREF crValueColor = RGB(50,50,50);

	CFont	boldFont, font;
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 10) : 11, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	font.CreateFont		((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	pDC->SetBkMode(TRANSPARENT);

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CRect rcRow		  = rcFrame; rcRow.left += Pixel2MM(pDC, 10);
	CRect rcThumbnail = rcRow; 
	float fMaxHeight  = pDoc->m_labelList.GetMaxHeight();

	CString strOut, strOut2;
	rcThumbnail.top	   = rcRow.top;
	rcThumbnail.bottom = rcThumbnail.top + (int)((float)rcRow.Height() * (pLabelListEntry->m_fLabelHeight / fMaxHeight));		
	CRect rcBoxThumbnail = pDoc->m_PageTemplateList.DrawSinglePage(pDC, rcThumbnail, nLabelIndex*2, nProductPartIndex);
	rcBox.UnionRect(rcBox, rcBoxThumbnail);

	int   nMaxRight = rcBoxThumbnail.right;
	CRect rcText;
	rcText.left = rcBoxThumbnail.right + Pixel2MM(pDC, 10); rcText.top = rcRow.top - 3; rcText.bottom = rcText.top + nTextHeight;  rcText.right = rcText.left + nTrimDataWidth;

	pDC->SelectObject(&boldFont);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);

	strOut.Format(_T("%s"), pLabelListEntry->m_strName);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	nMaxRight = max(nMaxRight, rcText.left + pDC->GetTextExtent(strOut).cx);

	pDC->SelectObject(&font);
	pDC->SetTextColor(crValueColor);

	rcText.OffsetRect(0, nTextHeight);
	CString strWidth, strHeight;
	CPrintSheetView::TruncateNumber(strWidth, pLabelListEntry->m_fLabelWidth); CPrintSheetView::TruncateNumber(strHeight, pLabelListEntry->m_fLabelHeight, TRUE);
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	nMaxRight = max(nMaxRight, rcText.left + pDC->GetTextExtent(strOut).cx);

	int nNUp = 1;
	POSITION pos = pDoc->m_PageTemplateList.FindIndex(nLabelIndex*2, nProductPartIndex);
	if (pos)
	{
		CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetAt(pos);
		nNUp = rTemplate.m_PrintSheetLocations.GetSize();
	}
	if (nNUp > 1)
	{
		rcText.OffsetRect(0, nTextHeight);
		CString strWidth, strHeight;
		strOut.Format(_T("%d Nutzen"), nNUp);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		nMaxRight = max(nMaxRight, rcText.left + pDC->GetTextExtent(strOut).cx);
	}

	nMaxRight		 = min(nMaxRight, rcText.right);
	rcThumbnail.left = nMaxRight + Pixel2MM(pDC, 40);

	rcBox.right  = max(rcBox.right, nMaxRight);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	pDC->SelectObject(pOldFont);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CProductPartList::GetPrintSheetPartBox(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CLayout* pLayout, CPressDevice* pPressDevice, int nPrintSheetIndex, int nProductPartIndex, BOOL bShowDetails)
{
	CDC dc;
	dc.CreateCompatibleDC(pDC);
	return DrawPrintSheetPart(pDC, rcFrame, pPrintSheet, pLayout, pPressDevice, nPrintSheetIndex, nProductPartIndex, bShowDetails, NULL, TRUE);
}

CRect CProductPartList::DrawPrintSheetPart(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CLayout* pLayout, CPressDevice* pPressDevice, int nPrintSheetIndex, int nProductPartIndex, BOOL bShowDetails, CDisplayList* pDisplayList, BOOL bGetBoxOnly)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(rcFrame.TopLeft(), CSize(0,0));

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);

	CRect rcBox(0,0,0,0);
	if ( ! bGetBoxOnly)	// draw 
	{
		rcBox = GetPrintSheetPartBox(pDC, rcFrame, pPrintSheet, pLayout, pPressDevice, nPrintSheetIndex, nProductPartIndex, bShowDetails);

		CRect rcTitle = rcBox; rcTitle.InflateRect(Pixel2MM(pDC, 5), 0); /*rcTitle.right = rcTitle.left + nThumbnailWidth + nGap + nFoldSheetWidth;*/ rcTitle.bottom = rcTitle.top + nTextHeight;

		CPen	pen(PS_SOLID, 1, RGB(200,200,200));
		CPen*	pOldPen		= pDC->SelectObject(&pen);
		CBrush* pOldBrush	= (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

		CRect rcBorder = rcTitle; rcBorder.left = 5; rcBorder.bottom = rcBox.bottom + Pixel2MM(pDC, 10);
		pDC->FillSolidRect(rcBorder, RGB(250,250,250));
		rcBox = rcBorder;

		if (bShowDetails)
		{
			CGraphicComponent gp;
			gp.DrawTitleBar(pDC, rcTitle, L"", WHITE, RGB(240,240,240), 90, -1);
		}

		pDC->Rectangle(rcBorder);

		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		pen.DeleteObject();
	}

	rcBox.SetRectEmpty();

	COLORREF crValueColor = RGB(50,50,50);

	CFont boldFont, smallFont;
	boldFont.CreateFont	((pDC->IsPrinting()) ? Pixel2MM(pDC, 10) : Pixel2MM(pDC, 12), 0, 0, 0, FW_BOLD,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 10) : Pixel2MM(pDC, 12), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	CPrintSheetPlanningView* pPlanningView = NULL;
	BOOL bPrintSheetNumeration = FALSE;
	if (pDisplayList)
		if (pDisplayList->m_pParent)
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
			{
				CPrintSheetPlanningView* pPlanningView = (CPrintSheetPlanningView*)pDisplayList->m_pParent;
				bPrintSheetNumeration = (pPlanningView->IsOpenPrintSheetNumeration()) ? TRUE : FALSE;
			}

	CProductPart*	pProductPart  = pDoc->m_productPartList.GetPart(nProductPartIndex);
	int     		nProductType  = (pLayout) ? pLayout->m_nProductType : (pProductPart) ? pProductPart->GetDefaultWorkStyle() : WORK_AND_BACK;
	if ( ! pPressDevice)
		pPressDevice = ( ! pPrintSheet) ? theApp.m_PressDeviceList.GetDevice((pProductPart) ? pProductPart->GetDefaultPressDeviceName() : L"") : (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	CSheet sheet;
	if (pLayout)
	{
		sheet.m_fWidth  = pLayout->m_FrontSide.m_fPaperWidth;
		sheet.m_fHeight = pLayout->m_FrontSide.m_fPaperHeight;
	}
	else
	{
		CPressDevice::FindPaper(sheet, (pProductPart) ? pProductPart->GetDefaultPaperName() : L"");
	}

	int		nPressType				= (pPressDevice) ? pPressDevice->m_nPressType : CPressDevice::SheetPress;
	CString	strPressDeviceName		= (pPressDevice) ? pPressDevice->m_strName : (pProductPart) ? pProductPart->GetDefaultPressDeviceName() : L"";
	float	fPlateWidth				= (pPressDevice) ? pPressDevice->m_fPlateWidth  : 0.0f;
	float	fPlateHeight			= (pPressDevice) ? pPressDevice->m_fPlateHeight : 0.0f;
	float	fFactor		 			= (pDC->IsPrinting()) ? 2.8f : 2.2f;
	int     nLayoutWidth 			= (int)(fFactor * (float)rcFrame.Height());
	int		nPrintSheetDetailsWidth = Pixel2MM(pDC, 200);
	CRect   rcLayout	 			= rcFrame; rcLayout.left += Pixel2MM(pDC, 10); rcLayout.right = rcLayout.left + ((nProductType == WORK_AND_BACK) ? nLayoutWidth : nLayoutWidth/2); rcLayout.DeflateRect(Pixel2MM(pDC, 5), Pixel2MM(pDC, 5));
	CRect   rcTitle(rcLayout.TopLeft(), CSize(nPrintSheetDetailsWidth, nTextHeight));
	CString strOut;

	rcBox = CRect(rcLayout.TopLeft(), CSize(1,1));

	CRect rcLayoutString = rcTitle;
	if (pLayout)
	{
		//rcTitle.top = rcBox.bottom; rcTitle.bottom = rcTitle.top + nTextHeight; 
		rcTitle.top = rcFrame.top; rcTitle.bottom = rcTitle.top + nTextHeight; 
		if (bShowDetails)
		{
			pDC->SelectObject(&smallFont);
			pDC->SetTextColor(crValueColor); 
			strOut = (pLayout) ? pLayout->m_strLayoutName : L"";
			pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
			rcTitle.right = rcTitle.left + pDC->GetTextExtent(strOut).cx;
			rcLayoutString = rcTitle;
			rcBox.UnionRect(rcBox, rcTitle);
		}
		rcLayout.top = rcTitle.bottom + Pixel2MM(pDC, 5);
		rcBox = pLayout->Draw(pDC, rcLayout, pPrintSheet, pPressDevice, TRUE, FALSE, FALSE, NULL, TRUE, BOTHSIDES, NULL, NULL, /*( ! IsOpenPrintSheetNumeration()) ? pDisplayList : NULL*/pDisplayList, TRUE, FALSE);
		rcLayout = rcBox;
	}

	pDC->SelectObject(&boldFont);
	int nLongest = GetLongestPrintSheetNumString(pDC, (bPrintSheetNumeration) ? nPrintSheetIndex : -1);
	rcTitle = rcFrame; rcTitle.left = rcFrame.left + nLayoutWidth + Pixel2MM(pDC, 5); rcTitle.right = rcTitle.left + min(nLongest, Pixel2MM(pDC, 80)); rcTitle.bottom = rcTitle.top + nTextHeight; 
	if (pPrintSheet)
	{
		pDC->SetTextColor(GUICOLOR_CAPTION); 
		strOut = (pPrintSheet) ? pPrintSheet->GetNumber() : L"";
		pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcTitle);
	}
	if (bPrintSheetNumeration)
	{
		rcTitle.left = rcTitle.left + pDC->GetTextExtent(strOut).cx;
		strOut.Format(_T("  (%d)"), nPrintSheetIndex + 1);
		pDC->SetTextColor(MIDDLEGRAY);
		pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcTitle);
	}

	if ( ! bShowDetails)
		return rcBox;

	//CPen pen(PS_SOLID, 1, LIGHTGRAY);
	//CPen* pOldPen = pDC->SelectObject(&pen);
	//pDC->MoveTo(rcLayoutString.right, rcLayoutString.CenterPoint().y); 
	//pDC->LineTo(rcFrame.right, rcLayoutString.CenterPoint().y); 
	//pDC->SelectObject(pOldPen);

	rcTitle.left = rcTitle.right + Pixel2MM(pDC, 10); rcTitle.right =  rcTitle.left + nPrintSheetDetailsWidth;;
	strOut = strPressDeviceName;
	pDC->SetTextColor(GUICOLOR_CAPTION); 
	pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcTitle.right = rcTitle.left + pDC->GetTextExtent(strOut).cx;
	rcBox.UnionRect(rcBox, rcTitle);
	CRect rcLayoutData(rcTitle.TopLeft(), CSize(0,0)); 

	if (bPrintSheetNumeration)
	{
		CRect rcDI(rcLayout.left - 5, rcLayout.top - 5, rcTitle.left - 10, rcLayout.bottom + 15);
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcFrame, (void*)pPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, SheetFrame), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetNoStateFeedback();
			if (pPlanningView->IsInsideNumerationRange(nPrintSheetIndex))
			{
				pDI->m_nState = CDisplayItem::Selected;
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	rcTitle.OffsetRect(0, nTextHeight); rcTitle.right = rcTitle.left + nPrintSheetDetailsWidth;
	pDC->SetTextColor(crValueColor); 
	switch (nProductType)
	{
	case WORK_AND_BACK:		strOut.LoadStringA(IDS_WORK_AND_BACK);			break;
	case WORK_SINGLE_SIDE:	strOut.LoadStringA(IDS_WORK_SINGLE_SIDE);		break;
	case WORK_AND_TURN:		strOut = theApp.settings.m_szWorkAndTurnName;   break;
	case WORK_AND_TUMBLE:	strOut = theApp.settings.m_szWorkAndTumbleName; break;
	default:				strOut = L"";									break;
	}
	if (pLayout)
	{
		if (nProductType == WORK_AND_BACK)
		{
			CString strOut2; strOut2.LoadString( (pLayout->KindOfProduction() == WORK_AND_TURN) ? IDS_TURN : IDS_TUMBLE);
			strOut += _T(" | ") + strOut2;
		}
	}

	pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcTitle.right = rcTitle.left + pDC->GetTextExtent(strOut).cx;
	rcBox.UnionRect(rcBox, rcTitle);

	pDC->SelectObject(&smallFont);
	pDC->SetTextColor(crValueColor); 
	CString strWidth, strHeight, strOut2;
	rcTitle.OffsetRect(0, nTextHeight); rcTitle.right = rcTitle.left + Pixel2MM(pDC, 50);
	CRect rcText = rcTitle; rcText.left = rcTitle.right; rcText.right = rcFrame.right;
	strOut2.LoadString(IDS_PLATE_LABEL); strOut2 += L":";
	pDC->DrawText(strOut2, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	CPrintSheetView::TruncateNumber(strWidth, fPlateWidth); CPrintSheetView::TruncateNumber(strHeight, fPlateHeight, TRUE);
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.UnionRect(rcBox, rcTitle);
	rcBox.UnionRect(rcBox, rcText);

	rcTitle.OffsetRect(0, nTextHeight); rcTitle.right = rcTitle.left + Pixel2MM(pDC, 50);
	rcText = rcTitle; rcText.left = rcTitle.right; rcText.right = rcFrame.right;
	strOut2.LoadString( (nPressType == CPressDevice::SheetPress) ? IDS_PAPER_LABEL : IDS_SECTION_LABEL); strOut2 += L":";
	pDC->DrawText(strOut2, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	CPrintSheetView::TruncateNumber(strWidth, sheet.m_fWidth); CPrintSheetView::TruncateNumber(strHeight, sheet.m_fHeight, TRUE);
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx;
	rcBox.UnionRect(rcBox, rcTitle);
	rcBox.UnionRect(rcBox, rcText);

	rcTitle.OffsetRect(0, nTextHeight); rcTitle.right = rcTitle.left + nPrintSheetDetailsWidth;
	if (pProductPart)
		if ( ! pProductPart->m_strPaper.IsEmpty())
		{
			strOut = pProductPart->m_strPaper;
			pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
			rcTitle.right = rcTitle.left + pDC->GetTextExtent(strOut).cx;
			rcBox.UnionRect(rcBox, rcTitle);
			rcTitle.OffsetRect(0, Pixel2MM(pDC, 10));
		}

	if (pPrintSheet)
	{
		rcTitle.right = rcTitle.left + nPrintSheetDetailsWidth;
		CRect rcColorBox = pPrintSheet->DrawColors(pDC, rcTitle, FRONTSIDE);
		rcBox.UnionRect(rcBox, rcColorBox);

		if ( ! pPrintSheet->EqualSeparationColorsFrontBack())
		{
			rcTitle.OffsetRect(rcColorBox.Width() + Pixel2MM(pDC, 20), 0);
			rcColorBox = pPrintSheet->DrawColors(pDC, rcTitle, BACKSIDE);
			rcBox.UnionRect(rcBox, rcColorBox);
		}
	}

	if (pDisplayList && pLayout && pPrintSheet)
	{
		rcLayoutData.BottomRight() = rcBox.BottomRight();
		CRect rcDI = rcLayoutData; rcDI.InflateRect(5, 0);
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, PrintSheetDetails), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
		}

		rcTitle.left = rcLayoutData.left + nPrintSheetDetailsWidth + Pixel2MM(pDC, 40); rcTitle.top = rcLayoutData.top; rcTitle.right = rcFrame.right; rcTitle.bottom = rcTitle.top + nTextHeight;
		strOut.LoadStringA(IDS_NAV_PRINTSHEETMARKS);
		pDC->SetTextColor(GUICOLOR_CAPTION); 
		pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcTitle.right = rcTitle.left + pDC->GetTextExtent(strOut).cx;
		rcBox.UnionRect(rcBox, rcTitle);

		CRect rcControlMarks = rcTitle;

		pDC->SetTextColor(crValueColor); 
		rcTitle.OffsetRect(Pixel2MM(pDC, 10), 0);
		if (pLayout->m_markList.GetCount() <= 0)
		{
			rcTitle.OffsetRect(0, nTextHeight);
			strOut.LoadStringA(IDS_NONE);
			pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
			rcTitle.right = rcTitle.left + pDC->GetTextExtent(strOut).cx;
			rcBox.UnionRect(rcBox, rcTitle);
			rcControlMarks.UnionRect(rcControlMarks, rcTitle);
		}
		for (int i = 0; i < pLayout->m_markList.GetCount(); i++)
		{
			rcTitle.OffsetRect(0, nTextHeight); rcTitle.right = rcFrame.right;
			if (rcTitle.bottom > rcFrame.bottom)
			{
				rcTitle.left = rcBox.right + Pixel2MM(pDC, 20);
				rcTitle.top  = rcLayoutData.top + nTextHeight; rcTitle.bottom = rcTitle.top + nTextHeight;
			}

			strOut = pLayout->m_markList[i]->m_strMarkName;
			pDC->DrawText(strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
			rcTitle.right = rcTitle.left + pDC->GetTextExtent(strOut).cx;
			rcBox.UnionRect(rcBox, rcTitle);
			rcControlMarks.UnionRect(rcControlMarks, rcTitle);
		}

		rcDI = rcControlMarks; rcDI.InflateRect(5, 0);
		pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, ControlMarks), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
		}
	}

	pDC->SelectObject(pOldFont);
	boldFont.DeleteObject();
	smallFont.DeleteObject();

	return rcBox;
}


int	CProductPartList::GetProductPartIndex(CProductPart* pProductPart)
{
	for (int i = 0; i < GetSize(); i++)
		if (&GetAt(i) == pProductPart)
			return i;
	return -1;
}

int	CProductPartList::GetProductPartIndex(CString strJDFNodeID)
{
	for (int i = 0; i < GetSize(); i++)
		if (GetAt(i).m_strJDFNodeID == strJDFNodeID)
			return i;
	return -1;
}

CProductPart* CProductPartList::GetPart(int nProductPartIndex)
{
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= GetSize()) )
		return NULL;
	else
		return &GetAt(nProductPartIndex);
}

void CProductPartList::ReorganizeReferences(int nIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.m_nProductPartIndex > nIndex)
			rFoldSheet.m_nProductPartIndex--;
	}

	pos = pDoc->m_PageTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetNext(pos);
		if (rPageTemplate.m_nProductPartIndex > nIndex)
			rPageTemplate.m_nProductPartIndex--;
	}
}

int	CProductPartList::GetLabelsProductPartIndex()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	int nIndex = -1;
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_nType == CProductPart::Unbound)
		{
			nIndex = i;
			break;
		}
	}

	if ( ! pDoc->m_labelList.GetCount() && (nIndex >= 0) )
		RemoveAt(nIndex);
	else
		if (pDoc->m_labelList.GetCount() && (nIndex < 0) )
		{
			CProductPart productPart;
			productPart.m_nType = CProductPart::Unbound;
			productPart.m_strName.LoadStringA(IDS_UNBOUND);
			Add(productPart);
			nIndex = GetSize() - 1;
		}


	return nIndex;
}

int	CProductPartList::GetCoverProductPartIndex()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_nType == CProductPart::Bound)
			if (GetAt(i).m_strName.CompareNoCase(theApp.m_strCoverName) == 0)
				return i;
	}
	return -1;
}

BOOL CProductPartList::HasBoundedProductPart()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_nType == CProductPart::Bound)
			return TRUE;
	}
	return FALSE;
}

BOOL CProductPartList::HasUnboundedProductPart()
{
	if (GetLabelsProductPartIndex() >= 0)
		return TRUE;
	else
		return FALSE;
}

void CProductPartList::ChangePageFormat(CString strFormatName, float fWidth, float fHeight, int nPageOrientation, int nProductPartIndex)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	for (int i = 0; i < GetSize(); i++)
	{
		if (nProductPartIndex != -1)
			if (nProductPartIndex != i)
				continue;

		CProductPart& rProductPart = GetAt(i);
		rProductPart.m_strFormatName = strFormatName;
		rProductPart.m_fPageWidth	 = fWidth;
		rProductPart.m_fPageHeight	 = fHeight;

		POSITION pos = pDoc->m_PageTemplateList.GetHeadPosition(nProductPartIndex);
		while (pos)
		{
			CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);

			int			   nIndex		 = 0;
			CLayoutObject* pLayoutObject = rTemplate.GetLayoutObject(nIndex);
			while (pLayoutObject)
			{
				pLayoutObject->m_strFormatName	  = strFormatName;
				pLayoutObject->m_nObjectOrientation = (unsigned char)nPageOrientation;
				pLayoutObject->ModifySize(fWidth, fHeight);

				if (pLayoutObject->GetLayoutSide())
					pLayoutObject->GetLayoutSide()->Invalidate();

				pLayoutObject = rTemplate.GetLayoutObject(++nIndex);
			}
		}
	}
}




/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CProductPartProductionBound

IMPLEMENT_SERIAL(CProductPartProductionBound, CProductPartProductionBound, VERSIONABLE_SCHEMA | 2)

CProductPartProductionBound::CProductPartProductionBound()
{
	m_nProductionType		= 0;
	m_bindingDefs.m_nBinding = CBindingDefs::PerfectBound;
	m_fHeadTrim				= 0.0f;
	m_fSideTrim				= 0.0f;
	m_fFootTrim				= 0.0f;
	m_fSpineTrim			= 0.0f; 
	m_fMillingEdge			= 0.0f;
	m_fOverFold				= 0.0f;
	m_nOverFoldSide			= 0;
	m_fBleed				= 0.0f;
	m_bShinglingActive		= FALSE;
	m_nShinglingMethod		= CBookblock::ShinglingMethodMove;
	m_fShinglingValue		= 0.0f;
	m_fShinglingValueFoot	= 0.0f;
}

CProductPartProductionBound::~CProductPartProductionBound()
{
}

const CProductPartProductionBound& CProductPartProductionBound::operator=(const CProductPartProductionBound& rProductPartProductionBound)
{
	Copy(rProductPartProductionBound);

	return *this;
}

void CProductPartProductionBound::Copy(const CProductPartProductionBound& rProductPartProductionBound)
{
	m_nProductionType			= rProductPartProductionBound.m_nProductionType;
	m_bindingDefs.m_strName 	= rProductPartProductionBound.m_bindingDefs.m_strName;
	m_bindingDefs.m_nBinding	= rProductPartProductionBound.m_bindingDefs.m_nBinding;
	m_fHeadTrim					= rProductPartProductionBound.m_fHeadTrim;
	m_fSideTrim					= rProductPartProductionBound.m_fSideTrim;
	m_fFootTrim					= rProductPartProductionBound.m_fFootTrim;
	m_fSpineTrim				= rProductPartProductionBound.m_fSpineTrim; 
	m_fMillingEdge				= rProductPartProductionBound.m_fMillingEdge;
	m_fOverFold					= rProductPartProductionBound.m_fOverFold;
	m_nOverFoldSide				= rProductPartProductionBound.m_nOverFoldSide;
	m_fBleed					= rProductPartProductionBound.m_fBleed;
	m_bShinglingActive			= rProductPartProductionBound.m_bShinglingActive;
	m_nShinglingMethod			= rProductPartProductionBound.m_nShinglingMethod;
	m_fShinglingValue			= rProductPartProductionBound.m_fShinglingValue;
	m_fShinglingValueFoot		= rProductPartProductionBound.m_fShinglingValueFoot;
}

void CProductPartProductionBound::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductPartProductionBound));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		ar << m_nProductionType;
		ar << m_bindingDefs.m_strName;
		ar << m_bindingDefs.m_nBinding;
		ar << m_fHeadTrim;
		ar << m_fSideTrim;
		ar << m_fFootTrim;
		ar << m_fSpineTrim; 
		ar << m_fMillingEdge;
		ar << m_fOverFold;
		ar << m_nOverFoldSide;
		ar << m_fBleed;
		ar << m_bShinglingActive;
		ar << m_nShinglingMethod;
		ar << m_fShinglingValue;
		ar << m_fShinglingValueFoot;
	}
	else
	{
		ar >> m_nProductionType;
		ar >> m_bindingDefs.m_strName;
		ar >> m_bindingDefs.m_nBinding;
		ar >> m_fHeadTrim;
		ar >> m_fSideTrim;
		ar >> m_fFootTrim;
		ar >> m_fSpineTrim; 
		ar >> m_fMillingEdge;
		ar >> m_fOverFold;
		ar >> m_nOverFoldSide;
		ar >> m_fBleed;
		ar >> m_bShinglingActive;
		ar >> m_nShinglingMethod;
		ar >> m_fShinglingValue;
		ar >> m_fShinglingValueFoot;
								
		switch (nVersion)
		{
		case 1:	switch (m_nShinglingMethod)
				{
				case 0:	
				case 1:	m_nShinglingMethod = CBookblock::ShinglingMethodMove;	  break;
				case 2:	m_nShinglingMethod = CBookblock::ShinglingMethodBottling; break;
				}
				break;
		case 2: break;
		}
	}
}

float CProductPartProductionBound::GetOverfold(int nSide)
{
	switch (nSide)
	{
	case LEFT:	if (m_nOverFoldSide == CProductPart::OverFoldFront)
					return m_fOverFold;
				break;
	case RIGHT:	if (m_nOverFoldSide == CProductPart::OverFoldBack)
					return m_fOverFold;
				break;
	}

	return 0.0f;
}


/////////////////////////////////////////////////////////////////////////////
// CProductPartProductionUnbound

IMPLEMENT_SERIAL(CProductPartProductionUnbound, CProductPartProductionUnbound, VERSIONABLE_SCHEMA | 1)

CProductPartProductionUnbound::CProductPartProductionUnbound()
{
	m_fLeftTrim		= 0.0f; 
	m_fBottomTrim	= 0.0f;
	m_fRightTrim	= 0.0f;
	m_fTopTrim		= 0.0f;
}

CProductPartProductionUnbound::~CProductPartProductionUnbound()
{
}

const CProductPartProductionUnbound& CProductPartProductionUnbound::operator=(const CProductPartProductionUnbound& rProductPartProductionUnbound)
{
	Copy(rProductPartProductionUnbound);

	return *this;
}

void CProductPartProductionUnbound::Copy(const CProductPartProductionUnbound& rProductPartProductionUnbound)
{
	m_fLeftTrim		= rProductPartProductionUnbound.m_fLeftTrim; 
	m_fBottomTrim	= rProductPartProductionUnbound.m_fBottomTrim;
	m_fRightTrim	= rProductPartProductionUnbound.m_fRightTrim;
	m_fTopTrim		= rProductPartProductionUnbound.m_fTopTrim;
}

void CProductPartProductionUnbound::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductPartProductionUnbound));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		ar << m_fLeftTrim;
		ar << m_fBottomTrim;
		ar << m_fRightTrim;
		ar << m_fTopTrim;
	}
	else
	{
		ar >> m_fLeftTrim;
		ar >> m_fBottomTrim;
		ar >> m_fRightTrim;
		ar >> m_fTopTrim;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CProductPart

IMPLEMENT_SERIAL(CProductPart, CProductPart, VERSIONABLE_SCHEMA | 1)

CProductPart::CProductPart()
{
	m_nType = Bound;
	m_nSubType = Body;
	m_strName.Empty();
	m_nNumPages	= 0;
	m_colorantList.RemoveAll();
	m_numberingList.RemoveAll();
	m_strPaper.Empty();
	m_nPaperGrammage	= 0;
	m_fPaperVolume		= 0.0f;
	m_fPageWidth		= 0.0f;
	m_fPageHeight		= 0.0f;
	m_strFormatName.Empty();
	m_nPageOrientation	= PORTRAIT;
	m_nAmount			= 0;
	m_nExtra			= 0;
	m_strComment.Empty();

// production data
	m_prodDataBound.m_nProductionType		= CFinalProductDescription::Single;
	m_prodDataBound.m_bindingDefs.m_nBinding= CFinalProductDescription::PerfectBound;
	m_prodDataBound.m_fHeadTrim				= 0.0f;
	m_prodDataBound.m_fSideTrim				= 0.0f;
	m_prodDataBound.m_fFootTrim				= 0.0f;
	m_prodDataBound.m_fSpineTrim			= 0.0f;
	m_prodDataBound.m_fMillingEdge			= 0.0f;
	m_prodDataBound.m_fOverFold				= 0.0f;
	m_prodDataBound.m_nOverFoldSide			= OverFoldNone;
	m_prodDataBound.m_fBleed				= 0.0f;
	m_prodDataBound.m_bShinglingActive		= FALSE;
	m_prodDataBound.m_nShinglingMethod		= CBookblock::ShinglingMethodMove;
	m_prodDataBound.m_fShinglingValue		= 0.0f;
	m_prodDataBound.m_fShinglingValueFoot	= 0.0f;

	m_prodDataUnbound.m_fLeftTrim	= 0.0f;
	m_prodDataUnbound.m_fBottomTrim = 0.0f;
	m_prodDataUnbound.m_fRightTrim	= 0.0f;
	m_prodDataUnbound.m_fTopTrim	= 0.0f;

	m_strDefaultPressDevice.Empty();	
	m_nDefaultWorkStyle = WORK_AND_BACK;
	m_nDefaultTurnTumbleMethod = TURN_LEFTBOTTOMSIDE;
	m_strDefaultPaper.Empty();
	m_strDefaultOutputProfile.Empty();	
	m_strDefaultMarkset.Empty();
}

CProductPart::CProductPart(const CProductPart& rProductPart)
{
	Copy(rProductPart);
}

const CProductPart& CProductPart::operator=(const CProductPart& rProductPart)
{
	Copy(rProductPart);

	return *this;
}

void CProductPart::Copy(const CProductPart& rProductPart)
{
	m_nType				= rProductPart.m_nType;
	m_nSubType			= rProductPart.m_nSubType;
	m_strName			= rProductPart.m_strName;
	m_nNumPages			= rProductPart.m_nNumPages;
	m_colorantList.Append(rProductPart.m_colorantList);
	m_numberingList.Append(rProductPart.m_numberingList);
	m_strPaper			= rProductPart.m_strPaper;
	m_nPaperGrammage	= rProductPart.m_nPaperGrammage;
	m_fPaperVolume		= rProductPart.m_fPaperVolume;
	m_fPageWidth		= rProductPart.m_fPageWidth;
	m_fPageHeight		= rProductPart.m_fPageHeight;
	m_strFormatName		= rProductPart.m_strFormatName;
	m_nPageOrientation	= rProductPart.m_nPageOrientation;
	m_nAmount			= rProductPart.m_nAmount;
	m_nExtra			= rProductPart.m_nExtra;
	m_strComment		= rProductPart.m_strComment;
	m_strJDFNodeID		= rProductPart.m_strJDFNodeID;

	m_prodDataBound		= rProductPart.m_prodDataBound;
	m_prodDataUnbound	= rProductPart.m_prodDataUnbound;

	m_strDefaultPressDevice		= rProductPart.m_strDefaultPressDevice;	
	m_nDefaultWorkStyle			= rProductPart.m_nDefaultWorkStyle;
	m_nDefaultTurnTumbleMethod	= rProductPart.m_nDefaultTurnTumbleMethod;
	m_strDefaultOutputProfile	= rProductPart.m_strDefaultOutputProfile;	
	m_strDefaultPaper			= rProductPart.m_strDefaultPaper;
	m_strDefaultMarkset			= rProductPart.m_strDefaultMarkset;
}

// helper function for CProductPart elements
template <> void AFXAPI SerializeElements <CProductPart> (CArchive& ar, CProductPart* pProductPart, int nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductPart));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pProductPart->m_nType;
			ar << pProductPart->m_nSubType;
			ar << pProductPart->m_strName;
			ar << pProductPart->m_nNumPages;
			ar << pProductPart->m_strPaper;
			ar << pProductPart->m_nPaperGrammage;
			ar << pProductPart->m_fPaperVolume;
			ar << pProductPart->m_fPageWidth;
			ar << pProductPart->m_fPageHeight;
			ar << pProductPart->m_strFormatName;
			ar << pProductPart->m_nPageOrientation;
			ar << pProductPart->m_nAmount;
			ar << pProductPart->m_nExtra;
			ar << pProductPart->m_strComment;
			ar << pProductPart->GetDefaultPressDeviceName();	
			ar << pProductPart->GetDefaultWorkStyle();
			ar << pProductPart->m_nDefaultTurnTumbleMethod;
			ar << pProductPart->GetDefaultPaperName();			
			ar << pProductPart->m_strDefaultMarkset;		
			ar << pProductPart->GetDefaultOutputProfileName();	
			ar << pProductPart->m_strJDFNodeID;
		}	
		else
		{ 
			CString strName;
			int		nWorkStyle;
			ar >> pProductPart->m_nType;
			ar >> pProductPart->m_nSubType;
			ar >> pProductPart->m_strName;
			ar >> pProductPart->m_nNumPages;
			ar >> pProductPart->m_strPaper;
			ar >> pProductPart->m_nPaperGrammage;
			ar >> pProductPart->m_fPaperVolume;
			ar >> pProductPart->m_fPageWidth;
			ar >> pProductPart->m_fPageHeight;
			ar >> pProductPart->m_strFormatName;
			ar >> pProductPart->m_nPageOrientation;
			ar >> pProductPart->m_nAmount; 
			ar >> pProductPart->m_nExtra;
			ar >> pProductPart->m_strComment;
			ar >> strName;		pProductPart->SetDefaultPressDeviceName(strName);	
			ar >> nWorkStyle;	pProductPart->SetDefaultWorkStyle(nWorkStyle);
			ar >> pProductPart->m_nDefaultTurnTumbleMethod;
			ar >> strName;		pProductPart->SetDefaultPaperName(strName);			
			ar >> pProductPart->m_strDefaultMarkset;		
			ar >> strName;		pProductPart->SetDefaultOutputProfileName(strName);	
			ar >> pProductPart->m_strJDFNodeID;
		}

		switch (pProductPart->m_nType)
		{
		case CProductPart::Bound:	pProductPart->m_prodDataBound.Serialize(ar);	break;
		case CProductPart::Unbound:	pProductPart->m_prodDataUnbound.Serialize(ar);	break;
		default:					break;
		}

		pProductPart->m_colorantList.Serialize(ar);
		pProductPart->m_numberingList.Serialize(ar);

		pProductPart++;
	}
}

int	CProductPart::GetProductPartIndex()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	for (int i = 0; i < pDoc->m_productPartList.GetSize(); i++)
		if ( &pDoc->m_productPartList[i] == this)
			return i;

	return -1;
}

CPrintSheet* CProductPart::GetFirstPrintSheet()
{
	return GetNextPrintSheet(NULL);
}

CPrintSheet* CProductPart::GetNextPrintSheet(CPrintSheet* pPrintSheet)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int nProductPartIndex = GetProductPartIndex();

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	if (pPrintSheet)	// find startpos
	{
		while (pos)
		{
			CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
			if (&rPrintSheet == pPrintSheet)
				break;
		}
	}

	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);

		if (m_nType == Bound)
		{
			for (int nLayerRefIndex = 0; nLayerRefIndex < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); nLayerRefIndex++)
			{
				CFoldSheet* pFoldSheet = rPrintSheet.GetFoldSheet(nLayerRefIndex);
				if ( ! pFoldSheet)
					continue;
				if (nProductPartIndex == pFoldSheet->m_nProductPartIndex)
					return &rPrintSheet;
			}
		}
		else	// Unbound
		{
			CLayout* pLayout = rPrintSheet.GetLayout();
			if ( ! pLayout)
				continue;

			POSITION objectPos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
			if ( ! objectPos)	// empty layout per definition is an unbound layout
				return &rPrintSheet;

			while (objectPos)
			{
				CLayoutObject& rObject   = pLayout->m_FrontSide.m_ObjectList.GetNext(objectPos);
				if ( ! rObject.IsLabel())
					continue;
				CPageTemplate* pTemplate = rObject.GetCurrentLabelTemplate();
				if ( ! pTemplate)
					continue;

				if (pTemplate->m_nProductPartIndex == nProductPartIndex)
					return &rPrintSheet;
			}
		}
	}

	return NULL;
}

BOOL CProductPart::HasMarks(int nMarkType)
{
	int nProductPartIndex = GetProductPartIndex();
	if (nProductPartIndex == -1)
		return FALSE;
	int nLayerIndex = 0;
	CArray<CFoldSheet*, CFoldSheet*> foldSheetGroups;
	CBookblock::GetFoldSheetGroups(foldSheetGroups, nProductPartIndex);
	if ( ! foldSheetGroups.GetSize())
		return FALSE;

	for (int i = 0; i < foldSheetGroups.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = foldSheetGroups[i];
		CLayout* pLayout = (pFoldSheet) ? pFoldSheet->GetLayout(nLayerIndex) : NULL;
		if (pLayout)
			if ( ! pLayout->HasMarks(nMarkType))
				return FALSE;
	}
	return TRUE;
}

void CProductPart::SetMarks(CMarkList& rMarkList)
{
	int nProductPartIndex = GetProductPartIndex();
	if (nProductPartIndex == -1)
		return;
	int nLayerIndex = 0;
	CArray<CFoldSheet*, CFoldSheet*> foldSheetGroups;
	CBookblock::GetFoldSheetGroups(foldSheetGroups, nProductPartIndex);
	if ( ! foldSheetGroups.GetSize())
		return;

	for (int i = 0; i < foldSheetGroups.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = foldSheetGroups[i];
		CLayout* pLayout = (pFoldSheet) ? pFoldSheet->GetLayout(nLayerIndex) : NULL;
		if (pLayout)
		{
			for (int j = 0; j < rMarkList.GetSize(); j++)
				CDlgMarkSet::PositionAndCreateMarkAuto(rMarkList[j], &rMarkList, _T(""), pLayout->GetFirstPrintSheet());
		}
	}
}

void CProductPart::DrawProductData(CDC* pDC, CRect rcFrame, int nProductPartIndex)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CProductPart& rProductPart = pDoc->m_productPartList[nProductPartIndex];

	CFont font;
	font.CreateFont (12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);

	int nXPos	 = rcFrame.left + 10;
	int nYPos	 = rcFrame.top  + 1;
	int nXOffset = rcFrame.left + rcFrame.Height()*2/3;

	COLORREF crValueColor = RGB(50,50,50);

	CString strOut, strOut2;
	pDC->SetTextColor(GUICOLOR_CAPTION);
	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(255, 240, 180));
	strOut.Format(_T("%d"), rProductPart.m_nNumPages); 
	pDC->TextOut(nXPos, nYPos, strOut);
	pDC->SetTextColor(GUICOLOR_CAPTION); 
	strOut2.LoadStringA(IDS_PAGES);
	pDC->TextOut(nXPos + pDC->GetTextExtent(strOut).cx + 5, nYPos, strOut2);
	pDC->SetTextColor(crValueColor);
	if (pDoc->m_PageTemplateList.GetCount())
	{
		strOut2 = pDoc->m_PageTemplateList.GetPageRange(nProductPartIndex);
		pDC->TextOut(nXPos + nXOffset - 5, nYPos, strOut2);
	}
	font.DeleteObject();
	font.CreateFont (12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);

	nYPos += 25;
	CRect rcThumbnail = rcFrame;
	rcThumbnail.top = nYPos - 5; rcThumbnail.right = nXOffset;
	pDoc->m_PageTemplateList.Draw(pDC, rcThumbnail, 0, nProductPartIndex, FALSE, FALSE, TRUE, FALSE);

	nXPos	 = rcThumbnail.right + 5;
	nXOffset = 70;

	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	pDC->SetBkMode(TRANSPARENT);
	strOut.LoadStringA(IDS_PAGELISTHEADER_FORMAT); strOut += _T(":");
	pDC->TextOut(nXPos, nYPos, strOut);

	pDC->SetTextColor(crValueColor);
	pDC->TextOut(nXPos + nXOffset, nYPos, m_strFormatName);
	nYPos += 16;

	if ( ! m_strPaper.IsEmpty())
	{
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		pDC->SetBkMode(TRANSPARENT);
		strOut.LoadStringA(IDS_TYPEOF_PAPER); strOut += _T(":");
		pDC->TextOut(nXPos, nYPos, strOut);

		pDC->SetTextColor(crValueColor);
		pDC->TextOut(nXPos + nXOffset, nYPos, m_strPaper);
		nYPos += 16;
	}

	if (m_nPaperGrammage > 0)
	{
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		pDC->SetBkMode(TRANSPARENT);
		pDC->TextOut(nXPos, nYPos, _T("Gewicht"));

		pDC->SetTextColor(crValueColor);
		strOut2.LoadStringA(IDS_PAPER_GRAMMAGE);
		strOut.Format(_T("%d %s"), m_nPaperGrammage, strOut2);
		pDC->TextOut(nXPos + nXOffset, nYPos, strOut);
		nYPos += 16;
	}


	if (m_fPaperVolume > 0.0f)
	{
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		pDC->SetBkMode(TRANSPARENT);
		strOut.LoadStringA(IDS_PAPER_VOLUME); strOut += _T(":");
		pDC->TextOut(nXPos, nYPos, strOut);

		pDC->SetTextColor(crValueColor);
		strOut.Format(_T("%.1f"), m_fPaperVolume);
		pDC->TextOut(nXPos + nXOffset, nYPos, strOut);
		nYPos += 16;
	}


	if (m_nAmount > 0)
	{
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		pDC->SetBkMode(TRANSPARENT);
		strOut.LoadStringA(IDS_AMOUNT); 
		pDC->TextOut(nXPos, nYPos, strOut);

		pDC->SetTextColor(crValueColor);
		strOut.Format(_T("%ld"), m_nAmount);
		pDC->TextOut(nXPos + nXOffset, nYPos, strOut);
		nYPos += 16;

		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		pDC->SetBkMode(TRANSPARENT);
		strOut.LoadStringA(IDS_PAPERLIST_EXTRA);
		pDC->TextOut(nXPos, nYPos, strOut);

		pDC->SetTextColor(crValueColor);
		strOut.Format(_T("%ld"), m_nExtra);
		pDC->TextOut(nXPos + nXOffset, nYPos, strOut);
		nYPos += 16;
	}

	if (m_colorantList.GetSize())
	{
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		strOut.Format(_T("%d-farbig"), m_colorantList.GetSize()); 
		pDC->TextOut(nXPos, nYPos, strOut);
		pDC->SetTextColor(crValueColor);

		nXPos += nXOffset;
		CRect rcIcon;
		rcIcon.left = nXPos; rcIcon.top = nYPos + 4; rcIcon.right = rcIcon.left + 9; rcIcon.bottom = nYPos + 11;
		CPen pen(PS_SOLID, 1, MIDDLEGRAY);
		CBrush brush;
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		CBrush* pOldBrush = pDC->GetCurrentBrush();
		for (int i = 0; i < m_colorantList.GetSize(); i++)
		{
			brush.CreateSolidBrush(m_colorantList[i].m_crRGB);
			pDC->SelectObject(&brush);
			pDC->Rectangle(rcIcon);
			brush.DeleteObject();

			rcIcon.OffsetRect(8, 0);
		}
		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		pen.DeleteObject();
		brush.DeleteObject();
	}

	font.DeleteObject();
}

void CProductPart::DrawProductionData(CDC* pDC, CRect rcFrame, int nProductPartIndex, BOOL bShowDetails)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CProductPart& rProductPart = pDoc->m_productPartList[nProductPartIndex];

	int nXPos = rcFrame.left + 10;
	int nYPos = rcFrame.top  + 1;

	CFont font;
	font.CreateFont (12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
	CString strOut, strOut2;
	pDC->SetTextColor(GUICOLOR_VALUE); 

//// binding 

	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(255, 240, 180));
	font.DeleteObject();
	font.CreateFont (12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
	strOut = rProductPart.m_prodDataBound.m_bindingDefs.m_strName;
	pDC->TextOut(nXPos, nYPos, strOut);
	font.DeleteObject();
	font.CreateFont (12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	nYPos += 20; 
	CRect rcThumbnail = rcFrame;
	rcThumbnail.left = nXPos - 5; rcThumbnail.right = rcFrame.CenterPoint().x - 10; rcThumbnail.top = nYPos; rcThumbnail.bottom = rcThumbnail.top + 50;
	pDoc->m_Bookblock.Draw(pDC, rcThumbnail, nProductPartIndex, L"");

	nYPos = rcThumbnail.bottom + 10;
	if (rProductPart.m_prodDataBound.m_nProductionType == CFinalProductDescription::ComingAndGoing)
	{
		strOut = _T("Kommen und Gehen");
		pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos, nYPos, strOut);
		nYPos += 20; 
	}

	CString strMeasureFormat;
	strMeasureFormat.Format("%s", (LPCTSTR)MeasureFormat("%.1f"));
	if (rProductPart.m_prodDataBound.m_nOverFoldSide != CProductPart::OverFoldNone)
	{
		strOut.Format(strMeasureFormat, rProductPart.m_prodDataBound.m_fOverFold);
		if (rProductPart.m_prodDataBound.m_nOverFoldSide == CProductPart::OverFoldFront)
		{
			strOut2.LoadStringA(IDS_OVERFOLD_FRONT); strOut2 += L":";
		}
		else
		{
			strOut2.LoadStringA(IDS_OVERFOLD_BACK); strOut2 += L":";
		}
		pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, strOut2); pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 60, nYPos, strOut);
		nYPos += 15; 
	}
	if ( (rProductPart.m_prodDataBound.m_fMillingEdge != 0.0f) || (rProductPart.m_prodDataBound.m_fSpineTrim != 0.0f) )
	{
		CString string;
		string.LoadStringA(IDS_COVER_NAME);
		if (rProductPart.m_strName == string)
		{
			strOut2.LoadStringA(IDS_SPINE);
			strOut.Format(strMeasureFormat, MeasureFormat(max(rProductPart.m_prodDataBound.m_fMillingEdge, rProductPart.m_prodDataBound.m_fSpineTrim)));
		}
		else
		{
			strOut2.LoadStringA(IDS_MILLING_EDGE); strOut2 += L":";
			strOut.Format(strMeasureFormat, MeasureFormat(max(rProductPart.m_prodDataBound.m_fMillingEdge, rProductPart.m_prodDataBound.m_fSpineTrim/2.0f)));
		}

		pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, strOut2); pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 60, nYPos, strOut);
		nYPos += 20; 
	}


//// trimming
	nXPos = rcFrame.CenterPoint().x + 20;
	nYPos = rcFrame.top + 1;

	pDC->SetBkMode(TRANSPARENT);
	strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
	strOut.Format(strMeasureFormat, rProductPart.m_fPageWidth, rProductPart.m_fPageHeight); 
	pDC->TextOut(nXPos, nYPos, strOut);

	font.DeleteObject();
	font.CreateFont (12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);

	nYPos += 25;

	strMeasureFormat.Format(_T("%s"), (LPCTSTR)MeasureFormat("%.1f"));
	pDC->SetTextColor(GUICOLOR_VALUE); 
	CString string; string.LoadStringA(IDS_TRIMMINGS); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos, nYPos, string);		
	nYPos += 15;
	strOut.Format(strMeasureFormat, MeasureFormat(rProductPart.m_prodDataBound.m_fHeadTrim));
	string.LoadStringA(IDS_HEAD); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos + 5, nYPos, string);		pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 35, nYPos, strOut);
	
	nYPos += 15;
	strOut.Format(strMeasureFormat, MeasureFormat(rProductPart.m_prodDataBound.m_fSideTrim));
	string.LoadStringA(IDS_SIDE); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos + 5, nYPos, string);		pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 35, nYPos, strOut);
	
	nYPos += 15;
	strOut.Format(strMeasureFormat, MeasureFormat(rProductPart.m_prodDataBound.m_fFootTrim));
	string.LoadStringA(IDS_FOOT); string += L":";
	pDC->SetTextColor(GUICOLOR_CAPTION); pDC->TextOut(nXPos + 5, nYPos, string);		pDC->SetTextColor(GUICOLOR_VALUE); pDC->TextOut(nXPos + 35, nYPos, strOut);


	font.DeleteObject();
}

void CProductPart::LoadSetup(const CString& strSetupName)
{
	CProductionSetup productionSetup;

	productionSetup.Load(strSetupName);

	m_prodDataBound.m_fHeadTrim		= productionSetup.m_prodDataBound.m_fHeadTrim;
	m_prodDataBound.m_fSideTrim		= productionSetup.m_prodDataBound.m_fSideTrim;
	m_prodDataBound.m_fFootTrim		= productionSetup.m_prodDataBound.m_fFootTrim;
	m_prodDataBound.m_fSpineTrim	= productionSetup.m_prodDataBound.m_fSpineTrim;
	m_prodDataBound.m_fMillingEdge	= productionSetup.m_prodDataBound.m_fMillingEdge;
	m_prodDataBound.m_fOverFold		= productionSetup.m_prodDataBound.m_fOverFold;
	m_prodDataBound.m_nOverFoldSide	= productionSetup.m_prodDataBound.m_nOverFoldSide;
	m_prodDataBound.m_fBleed		= productionSetup.m_prodDataBound.m_fBleed;
	m_prodDataUnbound				= productionSetup.m_prodDataUnbound;
	m_strDefaultPressDevice			= productionSetup.m_strDefaultPressDevice;	
	m_nDefaultWorkStyle				= productionSetup.m_nDefaultWorkStyle;
	m_nDefaultTurnTumbleMethod		= productionSetup.m_nDefaultTurnTumbleMethod;
	m_strDefaultPaper				= productionSetup.m_strDefaultPaper;			
	m_strDefaultOutputProfile		= productionSetup.m_strDefaultOutputProfile;	
	m_strDefaultMarkset				= productionSetup.m_strDefaultMarkset;		
}

void CProductPart::SaveSetup(const CString& strSetupName, BOOL bIsDefault)
{
	CProductionSetup productionSetup;

	productionSetup.m_prodDataBound				= m_prodDataBound;
	productionSetup.m_prodDataUnbound			= m_prodDataUnbound;
	productionSetup.m_strDefaultPressDevice		= m_strDefaultPressDevice;
	productionSetup.m_nDefaultWorkStyle			= m_nDefaultWorkStyle;
	productionSetup.m_nDefaultTurnTumbleMethod	= m_nDefaultTurnTumbleMethod;	
	productionSetup.m_strDefaultPaper			= m_strDefaultPaper;	
	productionSetup.m_strDefaultOutputProfile	= m_strDefaultOutputProfile;
	productionSetup.m_strDefaultMarkset			= m_strDefaultMarkset;	
	productionSetup.m_bIsDefault				= bIsDefault;

	productionSetup.Save(strSetupName);
}

CString	CProductPart::GetDefaultPressDeviceName()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return L"";
	if (pDoc->m_PrintSheetList.GetCount() <= 0)
		return m_strDefaultPressDevice;

	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	if ( ! pPrintSheet)
		return m_strDefaultPressDevice;
	
	CLayout* pLayout = pPrintSheet->GetLayout();
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	if ( ! pPressDevice)
		return m_strDefaultPressDevice;

	return m_strDefaultPressDevice = pPressDevice->m_strName;
}

void CProductPart::SetDefaultPressDeviceName(CString strName)
{
	m_strDefaultPressDevice = strName;
}

int	CProductPart::GetDefaultWorkStyle()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return WORK_AND_BACK;
	if (pDoc->m_PrintSheetList.GetCount() <= 0)
		return m_nDefaultWorkStyle;

	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	if ( ! pPrintSheet)
		return m_nDefaultWorkStyle;
	
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return m_nDefaultWorkStyle;

	return m_nDefaultWorkStyle = pLayout->m_nProductType;
}

void CProductPart::SetDefaultWorkStyle(int nWorkStyle)
{
	m_nDefaultWorkStyle = nWorkStyle;
}

CString	CProductPart::GetDefaultPaperName()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return L"";
	if (pDoc->m_PrintSheetList.GetCount() <= 0)
		return m_strDefaultPaper;

	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	if ( ! pPrintSheet)
		return m_strDefaultPaper;
	
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return m_strDefaultPaper;

	CString strFormat = pLayout->m_FrontSide.m_strFormatName;
	if (strFormat.IsEmpty())
	{
		CString strMeasureFormat;
		strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
		strFormat.Format(strMeasureFormat, MeasureFormat(pLayout->m_FrontSide.m_fPaperWidth), MeasureFormat(pLayout->m_FrontSide.m_fPaperHeight));
	}

	return m_strDefaultPaper = strFormat;
}

void CProductPart::SetDefaultPaperName(CString strName)
{
	m_strDefaultPaper = strName;
}

CString	CProductPart::GetDefaultOutputProfileName()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return L"";
	if (pDoc->m_PrintSheetList.GetCount() <= 0)
		return m_strDefaultOutputProfile;

	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	if ( ! pPrintSheet)
		return m_strDefaultOutputProfile;

	return m_strDefaultOutputProfile = pPrintSheet->m_strPDFTarget;
}

void CProductPart::SetDefaultOutputProfileName(CString strName)
{
	m_strDefaultOutputProfile = strName;
}

float CProductPart::GetPaperThickness()
{
	if (m_nPaperGrammage < 1)
		return 0.0f;

	if (fabs(m_fPaperVolume) < 0.1f)
		return (float)m_nPaperGrammage / 1000.0f;
	else
		return ((float)m_nPaperGrammage / 1000.0f) * m_fPaperVolume;
}

CString CProductPart::InitPaperInfo()
{
	if (this == NULL)
		return L"";

	CString strPaperInfo;
	strPaperInfo.LoadStringA(IDS_NO_PAPER);

	CString strVolume;
	CPrintSheetView::TruncateNumber(strVolume, m_fPaperVolume);

	CString strPaperVolume, strPaperWeight;
	strPaperVolume.LoadStringA(IDS_PAPER_VOLUME);
	if ( (m_nPaperGrammage > 0) && (m_fPaperVolume > 0.0f) )
		strPaperInfo.Format(_T("%d g/m� | %s: %s"), m_nPaperGrammage, strPaperVolume, strVolume);
	else
		if (m_nPaperGrammage > 0)
			strPaperInfo.Format(_T("%d g/m�"), m_nPaperGrammage);
		else
			if (m_fPaperVolume > 0.0f)
				strPaperInfo.Format(_T("%s: %s"), strPaperVolume, strVolume);
			else
				if ( ! m_strPaper.IsEmpty())
					strPaperInfo = m_strPaper;

	return strPaperInfo;
}


/////////////////////////////////////////////////////////////////////////////
// CProductionSetup

IMPLEMENT_SERIAL(CProductionSetup, CProductionSetup, VERSIONABLE_SCHEMA | 1)

CProductionSetup::CProductionSetup()
{
	m_nType		= CProductPart::Bound;
	m_nSubType	= CProductPart::Body;
	m_strName.Empty();

	m_strDefaultPressDevice.Empty();	
	m_nDefaultWorkStyle	= WORK_AND_BACK; 
	m_nDefaultTurnTumbleMethod = TURN_LEFTBOTTOMSIDE;
	m_strDefaultPaper.Empty();
	m_strDefaultOutputProfile.Empty();	
	m_strDefaultMarkset.Empty();

	m_bIsDefault = FALSE;
}

CProductionSetup::CProductionSetup(const CProductionSetup& rProductionSetup)
{
	Copy(rProductionSetup);
}

const CProductionSetup& CProductionSetup::operator=(const CProductionSetup& rProductionSetup)
{
	Copy(rProductionSetup);

	return *this;
}

void CProductionSetup::Copy(const CProductionSetup& rProductionSetup)
{
	m_nType				= rProductionSetup.m_nType;
	m_nSubType			= rProductionSetup.m_nSubType;
	m_strName			= rProductionSetup.m_strName;

	m_prodDataBound		= rProductionSetup.m_prodDataBound;
	m_prodDataUnbound	= rProductionSetup.m_prodDataUnbound;

	m_strDefaultPressDevice		= rProductionSetup.m_strDefaultPressDevice;	
	m_nDefaultWorkStyle			= rProductionSetup.m_nDefaultWorkStyle;
	m_nDefaultTurnTumbleMethod	= rProductionSetup.m_nDefaultTurnTumbleMethod;
	m_strDefaultOutputProfile	= rProductionSetup.m_strDefaultOutputProfile;	
	m_strDefaultPaper			= rProductionSetup.m_strDefaultPaper;
	m_strDefaultMarkset			= rProductionSetup.m_strDefaultMarkset;

	m_bIsDefault		= rProductionSetup.m_bIsDefault;
}

void CProductionSetup::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductionSetup));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		ar << m_nType;
		ar << m_nSubType;
		ar << m_strName;
		ar << m_strDefaultPressDevice;	
		ar << m_nDefaultWorkStyle;
		ar << m_nDefaultTurnTumbleMethod;
		ar << m_strDefaultPaper;			
		ar << m_strDefaultMarkset;		
		ar << m_strDefaultOutputProfile;
		ar << m_bIsDefault;
	}	
	else
	{ 
		ar >> m_nType;
		ar >> m_nSubType;
		ar >> m_strName;
		ar >> m_strDefaultPressDevice;	
		ar >> m_nDefaultWorkStyle;
		ar >> m_nDefaultTurnTumbleMethod;
		ar >> m_strDefaultPaper;			
		ar >> m_strDefaultMarkset;		
		ar >> m_strDefaultOutputProfile;	
		ar >> m_bIsDefault;
	}

	switch (m_nType)
	{
	case CProductPart::Bound:	m_prodDataBound.Serialize(ar);		break;
	case CProductPart::Unbound:	m_prodDataUnbound.Serialize(ar);	break;
	default:					break;
	}
}

void CProductionSetup::Load(const CString& strSetupName )
{
	CFileException fileException;
	CString		   strFullPath;
	
	strFullPath.Format(_T("%s\\ProductionSetups\\%s.set"), theApp.settings.m_szDataDir, strSetupName); 
	
	CFile file;
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}
	
	CArchive ar(&file, CArchive::load);
	Serialize(ar);
	ar.Close();
	file.Close();
}

void CProductionSetup::Save(const CString& strSetupName)
{
	CFile		   file;
	CFileException fileException;
	CString		   strFullPath;
	
	strFullPath.Format(_T("%s\\ProductionSetups"), theApp.settings.m_szDataDir); 
	if ( ! theApp.DirectoryExist(strFullPath))
		CreateDirectory(strFullPath, NULL);
	strFullPath.Format(_T("%s\\ProductionSetups\\%s.set"), theApp.settings.m_szDataDir, strSetupName); 

	if (!file.Open((LPCTSTR)strFullPath, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}

	CArchive ar(&file, CArchive::store);
	Serialize(ar);
	ar.Close();
	file.Close();
}

void CProductionSetup::Remove(const CString& strSetupName)
{
	CString strFullPath;
	strFullPath.Format(_T("%s\\ProductionSetups\\%s.set"), theApp.settings.m_szDataDir, strSetupName); 

	DeleteFile(strFullPath);
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CNumberingList

IMPLEMENT_SERIAL(CNumberingList, CObject, VERSIONABLE_SCHEMA | 1)

CNumberingList::CNumberingList()
{
}

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CNumbering

IMPLEMENT_SERIAL(CNumbering, CNumbering, VERSIONABLE_SCHEMA | 1)

CNumbering::CNumbering()
{
	m_nStartNum			= 1;
	m_nRangeTo			= 1;
	m_nRangeFrom		= 1;
	m_strNumberingStyle = _T("$n");
	m_bReverse			= FALSE;
}

CNumbering::CNumbering(const CNumbering& rNumbering)
{
	Copy(rNumbering);
}

const CNumbering& CNumbering::operator=(const CNumbering& rNumbering)
{
	Copy(rNumbering);

	return *this;
}

void CNumbering::Copy(const CNumbering& rNumbering)
{
	m_nStartNum			= rNumbering.m_nStartNum;
	m_nRangeTo			= rNumbering.m_nRangeTo;
	m_nRangeFrom		= rNumbering.m_nRangeFrom;
	m_strNumberingStyle = rNumbering.m_strNumberingStyle;
	m_bReverse			= rNumbering.m_bReverse;
}

// helper function for CNumbering elements
template <> void AFXAPI SerializeElements <CNumbering> (CArchive& ar, CNumbering* pNumbering, int nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CNumbering));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pNumbering->m_nStartNum;
			ar << pNumbering->m_nRangeTo;
			ar << pNumbering->m_nRangeFrom;
			ar << pNumbering->m_strNumberingStyle;	
			ar << pNumbering->m_bReverse;
		}	
		else
		{ 
			switch (nVersion)
			{
			case 1:	ar >> pNumbering->m_nStartNum;
					ar >> pNumbering->m_nRangeTo;
					ar >> pNumbering->m_nRangeFrom;
					ar >> pNumbering->m_strNumberingStyle;
					ar >> pNumbering->m_bReverse;
					break;
			}
		}

		pNumbering++;
	}
}



/////////////////////////////////////////////////////////////////////////////
// CColorantList

IMPLEMENT_SERIAL(CColorantList, CObject, VERSIONABLE_SCHEMA | 1)

CColorantList::CColorantList()
{
}

void CColorantList::AddSeparation(CString strSep)
{
	for (int i = 0; i < GetSize(); i++)
		if (GetAt(i).m_strName.CompareNoCase(strSep) == 0)	// already there
			return;

	CColorant		  colorant;
	CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strSep);
	if (pColorDef)
	{
		colorant.m_crRGB	= pColorDef->m_rgb;
		colorant.m_strName	= pColorDef->m_strColorName;
	}
	else
	{
		colorant.m_crRGB   = WHITE;
		colorant.m_strName = strSep;
	}

	Add(colorant);
}

CString CColorantList::GetProcessColorCode()
{
	BOOL bCyan	  = FALSE;
	BOOL bMagenta = FALSE;
	BOOL bYellow  = FALSE;
	BOOL bKey	  = FALSE;
	for (int i = 0; i < GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsCyan(GetAt(i).m_strName))
			bCyan = TRUE;
		else
			if (theApp.m_colorDefTable.IsMagenta(GetAt(i).m_strName))
				bMagenta = TRUE;
			else
				if (theApp.m_colorDefTable.IsYellow(GetAt(i).m_strName))
					bYellow = TRUE;
				else
					if (theApp.m_colorDefTable.IsKey(GetAt(i).m_strName))
						bKey = TRUE;
	}

	CString strCode;
	if (bCyan)
		strCode += L"C";
	if (bMagenta)
		strCode += L"M";
	if (bYellow)
		strCode += L"Y";
	if (bKey)
		strCode += L"K";

	return strCode;
}


/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CColorant

IMPLEMENT_SERIAL(CColorant, CColorant, VERSIONABLE_SCHEMA | 1)

CColorant::CColorant()
{
	m_crRGB = 0;
	m_strName.Empty();
}

CColorant::CColorant(COLORREF crRGB, const CString& strName)
{
	m_crRGB	  = crRGB;
	m_strName = strName;
}

CColorant::CColorant(const CColorant& rColorant)
{
	Copy(rColorant);
}

const CColorant& CColorant::operator=(const CColorant& rColorant)
{
	Copy(rColorant);

	return *this;
}

void CColorant::Copy(const CColorant& rColorant)
{
	m_crRGB	  = rColorant.m_crRGB;
	m_strName = rColorant.m_strName;
}

// helper function for CProductPart elements
template <> void AFXAPI SerializeElements <CColorant> (CArchive& ar, CColorant* pColorant, int nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CColorant));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pColorant->m_crRGB;
			ar << pColorant->m_strName;
		}
		else
		{ 
			switch (nVersion)
			{
			case 1:	ar >> pColorant->m_crRGB;
					ar >> pColorant->m_strName;
					break;
			}
		}
		pColorant++;
	}
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CPageSourceList

IMPLEMENT_SERIAL(CPageSourceList, CPageSourceList, VERSIONABLE_SCHEMA | 1)

CPageSourceList::CPageSourceList()
{
}


short CPageSourceList::GetPageSourceIndex(CPageSource* pPageSource)
{
	POSITION pos = GetHeadPosition();
	short	 i	 = 0;
	while (pos)
	{
		if (&GetNext(pos) == pPageSource)
			return i;
		i++;
	}
	return -1;
}

short CPageSourceList::GetPageSourceIndex(CString strDocName)
{
	strDocName.MakeUpper();
	POSITION pos = GetHeadPosition();
	short	 i	 = 0;
	while (pos)
	{
		CString strTest = GetNext(pos).m_strDocumentFullpath;
		strTest.MakeUpper();
		if (strTest == strDocName)
			return i;
		i++;
	}

	return -1;
}


void CPageSourceList::ResetFontsCopyStatus()
{
	POSITION pos = GetHeadPosition();
	while (pos)
		GetNext(pos).m_bFontsCopied = FALSE;
}

short CPageSourceList::FindObjectSourceIndex(CString strPath)
{
	if (strPath.IsEmpty())
		return -1;

	short	 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		if (rPageSource.m_strContentFilesPath == strPath)
			return nIndex;

		nIndex++;
	}

	return -1;
}

short CPageSourceList::FindMarkSourceIndex(CMark* pMark)
{
	if (! pMark)
		return -1;

	short	 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);

		switch (pMark->m_nType)
		{
		case CMark::CustomMark:		if (rPageSource.m_strContentFilesPath == pMark->m_MarkSource.m_strContentFilesPath)
										return nIndex;
									break;

		case CMark::TextMark:		if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo)
											return nIndex;
									break;

		case CMark::SectionMark:	if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo)
											return nIndex;
									break;

		case CMark::CropMark:		if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo)
											return nIndex;
									break;

		case CMark::FoldMark:		if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo)
											return nIndex;
									break;

		case CMark::BarcodeMark:	if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo)
											return nIndex;
									break;

		default:					if (rPageSource.m_strContentFilesPath == pMark->m_MarkSource.m_strContentFilesPath)
										return nIndex;
									break;
		}


		nIndex++;
	}

	return -1;
}


short CPageSourceList::FindMarkSourceIndex(CPageSource* pMarkSource, int nMarkType)
{
	if (! pMarkSource)
		return -1;

	short	 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);

		switch (nMarkType)
		{
		case CMark::CustomMark:		if ( ! pMarkSource->m_strContentFilesPath.GetLength())
										return -2;
									else
										if (rPageSource.m_strContentFilesPath == pMarkSource->m_strContentFilesPath)
											return nIndex;
									break;

		case CMark::TextMark:		if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo)
											return nIndex;
									break;

		case CMark::SectionMark:	if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo)
											return nIndex;
									break;

		case CMark::CropMark:		if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo)
											return nIndex;
									break;

		case CMark::FoldMark:		if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo)
											return nIndex;
									break;

		case CMark::BarcodeMark:	if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo)
											return nIndex;
									break;

		default:					if ( ! pMarkSource->m_strContentFilesPath.GetLength())
										return -2;
									else
										if (rPageSource.m_strContentFilesPath == pMarkSource->m_strContentFilesPath)
											return nIndex;
									break;
		}


		nIndex++;
	}

	return -1;
}


void CPageSourceList::RemoveUnusedSources()
{
	int		 nPageSourceIndex = 0;
	POSITION pos			  = GetHeadPosition();
	while (pos)
	{
		POSITION prevPos = pos;
		GetNext(pos);	

		BOOL	 bDelete	 = TRUE;
		POSITION templatePos = CImpManDoc::GetDoc()->m_MarkTemplateList.GetHeadPosition();
		while (templatePos)
		{
			CPageTemplate& rMarkTemplate = CImpManDoc::GetDoc()->m_MarkTemplateList.GetNext(templatePos);

			for (int i = 0; i < rMarkTemplate.m_ColorSeparations.GetSize(); i++)
			{
				SEPARATIONINFO& rSepInfo = rMarkTemplate.m_ColorSeparations[i];
				if (rSepInfo.m_nPageSourceIndex == nPageSourceIndex) 
				{
					bDelete = FALSE;
					break;
				}
			}
			if (!bDelete)
				break;
		}

		if (bDelete)
		{
			RemoveAt(prevPos);
			templatePos = CImpManDoc::GetDoc()->m_MarkTemplateList.GetHeadPosition();
			while (templatePos)
			{
				CPageTemplate& rMarkTemplate = CImpManDoc::GetDoc()->m_MarkTemplateList.GetNext(templatePos);

				for (int i = 0; i < rMarkTemplate.m_ColorSeparations.GetSize(); i++)
				{
					SEPARATIONINFO& rSepInfo = rMarkTemplate.m_ColorSeparations[i];
					if (rSepInfo.m_nPageSourceIndex >= nPageSourceIndex) 
						rSepInfo.m_nPageSourceIndex--;
				}
			}
		}
		else
			nPageSourceIndex++;
	}
}

void CPageSourceList::InitializePageTemplateRefs(CPageTemplateList* pPageTemplateList, CColorDefinitionTable* pColorDefinitionTable)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		for (int i = 0; i < rPageSource.m_PageSourceHeaders.GetSize(); i++)
			rPageSource.m_PageSourceHeaders[i].m_PageTemplateRefs.RemoveAll();
	}

	if (!pPageTemplateList)
		if (CImpManDoc::GetDoc())
			pPageTemplateList = &CImpManDoc::GetDoc()->m_PageTemplateList;

	if (!pPageTemplateList)
		return;

	if (!pColorDefinitionTable)
		if (CImpManDoc::GetDoc())
			pColorDefinitionTable = &CImpManDoc::GetDoc()->m_ColorDefinitionTable;
	if (!pColorDefinitionTable)
		return;

	pos	= pPageTemplateList->GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = pPageTemplateList->GetNext(pos);
		for (int i = 0; i < rTemplate.m_ColorSeparations.GetSize(); i++)
		{
			CPageSourceHeader* pPageSourceHeader = rTemplate.GetObjectSourceHeader(rTemplate.m_ColorSeparations[i], this, pColorDefinitionTable);

			if (pPageSourceHeader)
			{
				BOOL bExist = FALSE;
				for (int i = 0; i < pPageSourceHeader->m_PageTemplateRefs.GetSize(); i++)
					if (pPageSourceHeader->m_PageTemplateRefs[i] == &rTemplate)
					{
						bExist = TRUE;
						break;
					}
				if ( ! bExist)
					pPageSourceHeader->m_PageTemplateRefs.Add(&rTemplate);
			}
		}
	}
}


// Merge two lists together - only not yet existing elements will be appended
//							- links from page template list will be updated
void CPageSourceList::MergeElements(CPageSourceList& rPageSourceList, CPageTemplateList& rPageTemplateList)
{
	rPageTemplateList.ResetFlags();

	short	 nOldPageSourceIndex = 0;
	POSITION pos				 = rPageSourceList.GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = rPageSourceList.GetNext(pos);

		BOOL	 bFound				 = FALSE;
		short 	 nNewPageSourceIndex = 0;
		POSITION pos2				 = GetHeadPosition();
		while (pos2)
		{
			if (rPageSource == GetNext(pos2))
			{
				bFound = TRUE;
				rPageTemplateList.UpdateMarkSourceLinks(nOldPageSourceIndex, nNewPageSourceIndex);
				break;
			}
			nNewPageSourceIndex++;
		}

		if ( ! bFound)
		{
			AddTail(rPageSource);
			rPageTemplateList.UpdateMarkSourceLinks(nOldPageSourceIndex, (short)(GetCount() - 1));
		}

		nOldPageSourceIndex++;
	}

	rPageTemplateList.ResetFlags();
}


int	CPageSourceList::GetTotalNumPages()
{
	int		 nPages = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		nPages += rPageSource.GetNumPages();
	}
	return nPages;
}

// check if PDF sources plausible for Auto Server (docs exists and located in same folder as template itself)
BOOL CPageSourceList::CheckAutoPlausibility(CImpManDoc* pDoc)		
{
	if ( ! pDoc)
		return FALSE;

	char szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFileName[_MAX_FNAME], szExtension[_MAX_EXT];
	_splitpath_s(pDoc->GetPathName(), szDrive, _MAX_DRIVE, szDir, _MAX_DIR, szFileName, _MAX_FNAME, szExtension, _MAX_EXT);
	CString strTemplateFolder = CString(szDrive) + CString(szDir);
	strTemplateFolder.MakeUpper();

	BOOL	 bDeleted		  = FALSE;
	int		 nPageSourceIndex = 0;
	POSITION pos			  = GetHeadPosition();
	while (pos)
	{
		BOOL		 bUnplausible = FALSE;
		POSITION	 prevPos	  = pos;
		CPageSource& rPageSource  = GetNext(pos);
		if ( ! rPageSource.DocumentExists())
			bUnplausible = TRUE;
		else
		{
			_splitpath_s(rPageSource.m_strDocumentFullpath, szDrive, _MAX_DRIVE, szDir, _MAX_DIR, szFileName, _MAX_FNAME, szExtension, _MAX_EXT);
			CString strDocumentFolder = CString(szDrive) + CString(szDir);
			strDocumentFolder.MakeUpper();
			if (strDocumentFolder != strTemplateFolder)
				bUnplausible = TRUE;
		}

		if (bUnplausible)
		{
			RemoveAt(prevPos);
			pDoc->m_PageTemplateList.RemovePageSource(nPageSourceIndex, FALSE);
			bDeleted = TRUE;
		}
		else
			nPageSourceIndex++;
	}

	if (bDeleted)
	{
		InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->SetModifiedFlag();
	}

	return bDeleted; 
} 

BOOL CPageSourceList::SourceDocumentExists(CString strDocName)
{
	strDocName.MakeUpper();
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CString strTest = GetNext(pos).m_strDocumentFullpath;
		strTest.MakeUpper();
		if (strTest == strDocName)
			return TRUE;
	}

	return FALSE; 
} 
    
#ifdef PDF
void CPageSourceList::CheckPDFsModified()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource	= GetNext(pos);
		CString		 strPDFFile		= rPageSource.m_strDocumentFullpath;
		int			 nDot			= strPDFFile.ReverseFind('.');
		CString		 strFileTitle	= (nDot >= 0) ? strPDFFile.Left(nDot) : strPDFFile;
		CString		 strPDFInfoFile = strFileTitle + _T(".pdfinfo");

		CTime		dateNewMethod(2000, 9, 18, 0, 0, 0);	// since release 09/18/2000 we give pdfinfos same timestamp as pdf file
		BOOL		bUpdate = FALSE;						// to avoid problems with system time synchronisation (see ProcessPDFPages())
		CFileStatus statusPDFFile, statusPDFInfoFile;
		if (CFile::GetStatus(strPDFFile, statusPDFFile))		
			if (CFile::GetStatus(strPDFInfoFile, statusPDFInfoFile))		
			{
				if (statusPDFInfoFile.m_mtime < dateNewMethod)
				{
					if (statusPDFInfoFile.m_mtime < statusPDFFile.m_mtime)		// use old method to check if pdf info is out of date
						bUpdate = TRUE;
				}
				else
					if (statusPDFInfoFile.m_mtime != statusPDFFile.m_mtime)		// use new method to check if pdf info is out of date
						bUpdate = TRUE;
			}
			else
				bUpdate = TRUE;	// also update if no pdf info file present

		if (bUpdate)
		{
			theApp.m_pDlgPDFLibStatus->LaunchPDFEngine("", theApp.m_pMainWnd, CDlgPDFLibStatus::UpdatePages);
			theApp.m_pDlgPDFLibStatus->ProcessPDFPages(strPDFFile, &rPageSource);
			theApp.m_pDlgPDFLibStatus->OnCancel();

			BOOL bKeepContentOffset = UNDEFINED;
			BOOL bTrimBoxWarned		= FALSE;
			for (int i = 0; i < rPageSource.m_PageSourceHeaders.GetSize(); i++)
			{
				CPageSourceHeader& rPageSourceHeader = rPageSource.m_PageSourceHeaders[i];
				CPageTemplate*	   pPageTemplate	 = rPageSourceHeader.GetFirstPageTemplate();
				while (pPageTemplate)
				{
					if (bKeepContentOffset == UNDEFINED)	// not yet prompted
						if (pPageTemplate->HasContentOffset())
							if (AfxMessageBox(IDS_KEEP_CONTENTOFFSET_PROMPT, MB_YESNO) == IDYES)
								bKeepContentOffset = TRUE;
							else
								bKeepContentOffset = FALSE;

					if ( ! bKeepContentOffset || (bKeepContentOffset == UNDEFINED) )
					{
						if (rPageSourceHeader.HasTrimBox())
						{
							if ( ! bTrimBoxWarned && theApp.settings.m_bTrimboxPagesizeMismatchWarning)
							{
								CLayoutObject* pObject = pPageTemplate->GetLayoutObject();
								if (pObject)
								{
									if ( (fabs( (rPageSourceHeader.m_fTrimBoxRight - rPageSourceHeader.m_fTrimBoxLeft)   - pObject->GetRealWidth())  >= 0.5) ||
										 (fabs( (rPageSourceHeader.m_fTrimBoxTop   - rPageSourceHeader.m_fTrimBoxBottom) - pObject->GetRealHeight()) >= 0.5) )
									{
										CDlgTrimboxWarning dlg;
										dlg.m_strText = rPageSource.m_strDocumentFullpath;
										dlg.DoModal();
										bTrimBoxWarned = TRUE;
									}
								}
							}
							float fTrimBoxCenterX = rPageSourceHeader.m_fTrimBoxLeft   + (rPageSourceHeader.m_fTrimBoxRight - rPageSourceHeader.m_fTrimBoxLeft)  /2.0f;
							float fTrimBoxCenterY = rPageSourceHeader.m_fTrimBoxBottom + (rPageSourceHeader.m_fTrimBoxTop   - rPageSourceHeader.m_fTrimBoxBottom)/2.0f;
							float fBBoxCenterX	  = rPageSourceHeader.m_fBBoxLeft	   + (rPageSourceHeader.m_fBBoxRight	- rPageSourceHeader.m_fBBoxLeft)	 /2.0f;
							float fBBoxCenterY	  = rPageSourceHeader.m_fBBoxBottom	   + (rPageSourceHeader.m_fBBoxTop	    - rPageSourceHeader.m_fBBoxBottom)	 /2.0f;
							pPageTemplate->SetContentOffsetX(fBBoxCenterX - fTrimBoxCenterX);
							pPageTemplate->SetContentOffsetY(fBBoxCenterY - fTrimBoxCenterY);
							CPageListDetailView* pView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
							if (pView)
								pView->OnUpdate(NULL, 0, NULL);
						}
	//					if (rPageSourceHeader.m_bPageNumberFix)
	//						pPageTemplate->m_strPageID = rPageSourceHeader.m_strPageName;
					}

					pPageTemplate = rPageSourceHeader.GetNextPageTemplate(pPageTemplate);
				}
			}
			CPageSourceListView* pView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
			if (pView)
				pView->OnUpdate(NULL, 0, NULL);
		}
		else
			if (rPageSource.m_pdfInfoDate < statusPDFInfoFile.m_mtime)		// also update (but not re-read content) if page source data is older than pdf info
				rPageSource.UpdatePDFInfo(strPDFFile, strPDFInfoFile);
	}
}

void CPageSourceList::CheckDocuments()
{
	if ( ! theApp.IsViewVisible(RUNTIME_CLASS(CPageSourceListView)) && ! theApp.IsViewVisible(RUNTIME_CLASS(CProductDataView)) )
		return;

	BOOL bDoUpdateViews = FALSE;
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		if ( ! rPageSource.DocumentExists())
		{
			if (rPageSource.m_bDocExist)
			{
				bDoUpdateViews = TRUE;
				rPageSource.m_bDocExist = FALSE;
			}
		}
		else
		{
			if ( ! rPageSource.m_bDocExist)
			{
				bDoUpdateViews = TRUE;
				rPageSource.m_bDocExist = TRUE;
			}
		}
	}

	if (bDoUpdateViews)
	{
		theApp.UpdateView(RUNTIME_CLASS(CPageSourceListView));	// update views to display pathname lowlighted (gray)
		theApp.UpdateView(RUNTIME_CLASS(CProductDataView));
	}
}
#endif


void CPageSourceList::UpdateColorInfos()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rObjectSource = GetNext(pos);
		for (int i = 0; i < rObjectSource.m_PageSourceHeaders.GetSize(); i++)
		{
			rObjectSource.m_PageSourceHeaders[i].m_rgb = theApp.m_colorDefTable.FindColorRef(rObjectSource.m_PageSourceHeaders[i].m_strColorName);
		}
	}
}




/////////////////////////////////////////////////////////////////////////////
// CPageSource

IMPLEMENT_SERIAL(CPageSource, CPageSource, VERSIONABLE_SCHEMA | 5)

CPageSource::CPageSource()
{
	m_nType					 = PDFFile;
	m_nColorant				 = ColorantUnknown;
	m_fGlobalBBoxLeft		 = 0.0f;
	m_fGlobalBBoxBottom		 = 0.0f;
	m_fGlobalBBoxRight		 = 0.0f; 
	m_fGlobalBBoxTop		 = 0.0f;
	m_pdfInfoDate			 = 0;
	m_bBmpFP				 = FALSE;
	m_bPreviewsInHiresFolder = FALSE;

	m_bFontsCopied = FALSE;
	m_bDocExist	   = TRUE;
}

// copy constructor for CPageSource - needed by CList functions:

CPageSource::CPageSource(const CPageSource& rPageSource) 
{
	Copy(rPageSource);
}


const CPageSource& CPageSource::operator=(const CPageSource& rPageSource)
{
	Copy(rPageSource);

	return *this;
}


BOOL CPageSource::operator==(const CPageSource& rPageSource)
{
	if (m_nType						!= rPageSource.m_nType)
		return FALSE;				
	if (m_strDocumentFullpath		!= rPageSource.m_strDocumentFullpath)
		return FALSE;				
	if (m_strContentFilesPath		!= rPageSource.m_strContentFilesPath)
		return FALSE;				
	if (m_strPreviewFilesPath		!= rPageSource.m_strPreviewFilesPath)
		return FALSE;				
	if (m_strDocumentTitle			!= rPageSource.m_strDocumentTitle)
		return FALSE;				
	if (m_strCreator				!= rPageSource.m_strCreator)
		return FALSE;				
	if (m_strCreationDate			!= rPageSource.m_strCreationDate)
		return FALSE;				
	if (m_strSystemCreationInfo		!= rPageSource.m_strSystemCreationInfo)
		return FALSE;				
	if (m_nColorant					!= rPageSource.m_nColorant)
		return FALSE;				
	if (m_fGlobalBBoxLeft			!= rPageSource.m_fGlobalBBoxLeft)
		return FALSE;				
	if (m_fGlobalBBoxBottom			!= rPageSource.m_fGlobalBBoxBottom)
		return FALSE;				
	if (m_fGlobalBBoxRight			!= rPageSource.m_fGlobalBBoxRight)
		return FALSE;				
	if (m_fGlobalBBoxTop			!= rPageSource.m_fGlobalBBoxTop)
		return FALSE;				
	if (m_pdfInfoDate				!= rPageSource.m_pdfInfoDate)
		return FALSE;				
	if (m_bBmpFP					!= rPageSource.m_bBmpFP)
		return FALSE;				
	if (m_bPreviewsInHiresFolder	!= rPageSource.m_bPreviewsInHiresFolder)
		return FALSE;

	if (m_PageSourceHeaders.GetSize() != rPageSource.m_PageSourceHeaders.GetSize())
		return FALSE;

	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		if (m_PageSourceHeaders[i] != rPageSource.m_PageSourceHeaders[i])
			return FALSE;
	}

	return TRUE;
}


BOOL CPageSource::operator!=(const CPageSource& rPageSource)
{
	return !(*this == rPageSource);
}


void CPageSource::Copy(const CPageSource& rPageSource)
{
	m_nType					 = rPageSource.m_nType;
	m_strDocumentFullpath	 = rPageSource.m_strDocumentFullpath;
	m_strContentFilesPath	 = rPageSource.m_strContentFilesPath;
	m_strPreviewFilesPath	 = rPageSource.m_strPreviewFilesPath;
	m_strDocumentTitle		 = rPageSource.m_strDocumentTitle;
	m_strCreator			 = rPageSource.m_strCreator;
	m_strCreationDate		 = rPageSource.m_strCreationDate;
	m_strSystemCreationInfo  = rPageSource.m_strSystemCreationInfo;
	m_nColorant				 = rPageSource.m_nColorant;
	m_fGlobalBBoxLeft		 = rPageSource.m_fGlobalBBoxLeft;
	m_fGlobalBBoxBottom		 = rPageSource.m_fGlobalBBoxBottom;	
	m_fGlobalBBoxRight		 = rPageSource.m_fGlobalBBoxRight;
	m_fGlobalBBoxTop		 = rPageSource.m_fGlobalBBoxTop;		
	m_pdfInfoDate			 = rPageSource.m_pdfInfoDate;
	m_bBmpFP				 = rPageSource.m_bBmpFP;
	m_bPreviewsInHiresFolder = rPageSource.m_bPreviewsInHiresFolder;

	CPageSourceHeader PageSourceHeader;
	m_PageSourceHeaders.RemoveAll();
	for (int i = 0; i < rPageSource.m_PageSourceHeaders.GetSize(); i++)
	{
		PageSourceHeader = rPageSource.m_PageSourceHeaders[i];
		m_PageSourceHeaders.Add(PageSourceHeader);
		//m_PageSourceHeaders.Add((CPageSourceHeader)rPageSource.m_PageSourceHeaders[i]);
	}

	m_bDocExist	= rPageSource.m_bDocExist;
}


// helper function for PageSourceList elements
template <> void AFXAPI SerializeElements <CPageSource> (CArchive& ar, CPageSource* pPageSource, int nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CPageSource));

	if (ar.IsStoring())
	{
		ar << pPageSource->m_nType;
		ar << pPageSource->m_strDocumentFullpath;
		ar << pPageSource->m_strContentFilesPath;
		ar << pPageSource->m_strPreviewFilesPath;
		ar << pPageSource->m_strDocumentTitle;
		ar << pPageSource->m_strCreator;
		ar << pPageSource->m_strCreationDate;
		ar << pPageSource->m_strSystemCreationInfo;
		ar << pPageSource->m_nColorant;
		ar << pPageSource->m_fGlobalBBoxLeft;
		ar << pPageSource->m_fGlobalBBoxBottom;
		ar << pPageSource->m_fGlobalBBoxRight;
		ar << pPageSource->m_fGlobalBBoxTop;
		ar << pPageSource->m_pdfInfoDate;
		ar << pPageSource->m_bBmpFP;
		ar << pPageSource->m_bPreviewsInHiresFolder;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			{
			pPageSource->m_nType = CPageSource::PDFFile;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			pPageSource->m_PageSourceHeaders.RemoveAll();
			pPageSource->m_nColorant = CPageSource::ColorantUnknown;
			}
			break;

		case 2:
			{
			ar >> pPageSource->m_nType;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_strSystemCreationInfo;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			pPageSource->m_PageSourceHeaders.RemoveAll();
			pPageSource->m_nColorant = CPageSource::ColorantUnknown;
			}
			break;

		case 3:
			{
			ar >> pPageSource->m_nType;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_strSystemCreationInfo;
			ar >> pPageSource->m_nColorant;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			pPageSource->m_PageSourceHeaders.RemoveAll();
			}
			break;

		case 4:
			{
			ar >> pPageSource->m_nType;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_strSystemCreationInfo;
			ar >> pPageSource->m_nColorant;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			ar >> pPageSource->m_pdfInfoDate;
			pPageSource->m_PageSourceHeaders.RemoveAll();
			}
			break;

		case 5:
			{
			ar >> pPageSource->m_nType;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_strSystemCreationInfo;
			ar >> pPageSource->m_nColorant;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			ar >> pPageSource->m_pdfInfoDate;
			ar >> pPageSource->m_bBmpFP;
			ar >> pPageSource->m_bPreviewsInHiresFolder;
			pPageSource->m_PageSourceHeaders.RemoveAll();
			}
			break;
		}
	}

	pPageSource->m_PageSourceHeaders.Serialize(ar);
}


void AFXAPI DestructElements(CPageSource* pPageSource, int nCount)
{
	ASSERT(nCount == 1); // in CList context nCount has to be 1

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pPageSource->m_strDocumentFullpath.Empty();
	pPageSource->m_strContentFilesPath.Empty();
	pPageSource->m_strPreviewFilesPath.Empty();
	pPageSource->m_strDocumentTitle.Empty();
	pPageSource->m_strCreator.Empty();
	pPageSource->m_strCreationDate.Empty();
	pPageSource->m_strSystemCreationInfo.Empty();
	pPageSource->m_PageSourceHeaders.RemoveAll();
}


BOOL CPageSource::Create(int nType, const CString& strDocumentFullpath, const CString& strContentFilesPath, const CString& strPreviewFilesPath)
{
	m_nType				  = nType;
	m_strDocumentFullpath = strDocumentFullpath; 
	m_strContentFilesPath = strContentFilesPath;	 
	m_strPreviewFilesPath = strPreviewFilesPath;	 
	m_fGlobalBBoxLeft	  =  FLT_MAX;
	m_fGlobalBBoxBottom	  =  FLT_MAX;
	m_fGlobalBBoxRight	  = -FLT_MAX; 
	m_fGlobalBBoxTop	  = -FLT_MAX;

	CPostScriptParser parser;
	parser.Configure(CPostScriptParser::DocTitleSym, CPostScriptParser::PDICreatorSym, CPostScriptParser::PDICreationDateSym, CPostScriptParser::PDIColorantSym, -1);
	parser.ParsePSFile(strContentFilesPath);	// in PDF version strContentFilesPath holds the pdf info file fullpath

	m_strDocumentTitle	= parser.m_strDocTitle;
	m_strCreator		= parser.m_strCreator;	  
	m_strCreationDate	= parser.m_strCreationDate; 
	m_nColorant			= parser.m_nColorant;

	return ( ! strPreviewFilesPath.IsEmpty()) ? CreateDirectory(m_strPreviewFilesPath, NULL) : TRUE;
}


void CPageSource::RegisterPDFPages(const CString& strFileName, const CString& strInfoFileName, int nStartPage)
{
	CPostScriptParser parser;
	parser.Configure(CPostScriptParser::PDIPageListSym, -1);
	parser.ParsePSFile(strInfoFileName);

	int				  nPageNum	      = (nStartPage != -1) ? nStartPage : 0;
	int				  nPrevPageNum	  = (parser.m_pdfPageList.GetSize()) ? parser.m_pdfPageList[0].m_nPageNum : -1;
	BOOL			  bNotYetPrompted = TRUE;
	CPageSourceHeader pageSourceHeader;
	for (int i = 0; i < parser.m_pdfPageList.GetSize(); i++)
	{
		CPDFPageListEntry entry = parser.m_pdfPageList[i];

		CString strColorant = entry.m_strSeparationColor;
		if ( strColorant.IsEmpty())
			strColorant = _T("Composite");

		if (nStartPage != -1)	// page numbering via naming convention (AUTO only)
		{
			if (nPrevPageNum != entry.m_nPageNum)	// increase absolute page num only if relativ page num changes -> relevant for separated jobs
				nPageNum++;
			nPrevPageNum = entry.m_nPageNum;
			entry.m_nPageNum = nPageNum;
			entry.m_strPageLabel.Empty();
		}

		if ( !entry.CropBoxEmpty())
			pageSourceHeader.Create(entry.m_nPageNum, entry.m_strPageLabel, 
									entry.m_fCropBoxLeft,  entry.m_fCropBoxBottom, entry.m_fCropBoxRight, entry.m_fCropBoxTop, 
									strColorant);
		else
			pageSourceHeader.Create(entry.m_nPageNum, entry.m_strPageLabel, 
									entry.m_fMediaBoxLeft,  entry.m_fMediaBoxBottom, entry.m_fMediaBoxRight, entry.m_fMediaBoxTop, 
									strColorant);

		entry.m_fTrimBoxLeft *= PICA_POINTS; entry.m_fTrimBoxRight *= PICA_POINTS; entry.m_fTrimBoxTop *= PICA_POINTS; entry.m_fTrimBoxBottom *= PICA_POINTS;
		// if trimbox equals crop/mediabox we define trimbox as not existent to avoid final pagesize mismatch warnings while assigning pages
		if ( (fabs(entry.m_fTrimBoxLeft - pageSourceHeader.m_fBBoxLeft) < 0.01) && (fabs(entry.m_fTrimBoxRight  - pageSourceHeader.m_fBBoxRight)  < 0.01) &&
			 (fabs(entry.m_fTrimBoxTop  - pageSourceHeader.m_fBBoxTop)  < 0.01) && (fabs(entry.m_fTrimBoxBottom - pageSourceHeader.m_fBBoxBottom) < 0.01) )
		{
			pageSourceHeader.m_fTrimBoxLeft = pageSourceHeader.m_fTrimBoxRight = pageSourceHeader.m_fTrimBoxTop = pageSourceHeader.m_fTrimBoxBottom = 0.0f;
		}
		else
		{
			pageSourceHeader.m_fTrimBoxLeft  = entry.m_fTrimBoxLeft;  pageSourceHeader.m_fTrimBoxBottom = entry.m_fTrimBoxBottom;
			pageSourceHeader.m_fTrimBoxRight = entry.m_fTrimBoxRight; pageSourceHeader.m_fTrimBoxTop	= entry.m_fTrimBoxTop;
		}

		pageSourceHeader.ExtractSpotColors(entry.m_colorSpaces);

		m_PageSourceHeaders.Add(pageSourceHeader);

		m_fGlobalBBoxLeft	= min(m_fGlobalBBoxLeft,   pageSourceHeader.m_fBBoxLeft);	// Find document's global bbox
		m_fGlobalBBoxBottom = min(m_fGlobalBBoxBottom, pageSourceHeader.m_fBBoxBottom);
		m_fGlobalBBoxRight  = max(m_fGlobalBBoxRight,  pageSourceHeader.m_fBBoxRight);
		m_fGlobalBBoxTop	= max(m_fGlobalBBoxTop,	   pageSourceHeader.m_fBBoxTop);
	}

	CFileStatus statusInfoFile;
	if (CFile::GetStatus(strInfoFileName, statusInfoFile))		
		m_pdfInfoDate = statusInfoFile.m_mtime;
}
 
void CPageSource::UpdatePDFInfo(const CString& strFileName, const CString& strInfoFileName)
{
	CFileStatus status;
	if ( ! CFile::GetStatus(strFileName, status))		
		return;

	m_PageSourceHeaders.RemoveAll();
	RegisterPDFPages(strFileName, strInfoFileName);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_PageTemplateList.CheckPageSourceHeaders();
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->SetModifiedFlag();										
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListView), NULL, 0, NULL);
	}
}

void CPageSource::ReorganizePageNumbers()
{
	int nCurPageNum = (m_PageSourceHeaders.GetSize() > 0) ? m_PageSourceHeaders[0].m_nPageNumber : 0;
	int nPageNum	= 1;
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		if (m_PageSourceHeaders[i].m_nPageNumber != nCurPageNum)
			nPageNum++;
		nCurPageNum = m_PageSourceHeaders[i].m_nPageNumber;
//		if (!m_PageSourceHeaders[i].m_bPageNumberFix)
			m_PageSourceHeaders[i].m_nPageNumber = nPageNum;
	}
}


short CPageSource::GetHeaderIndex(CPageSourceHeader* pPageSourceHeader)
{
	if ( ! pPageSourceHeader)
		return -1;

	for (short i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		if (&m_PageSourceHeaders[i] == pPageSourceHeader)
			return i;
	}
	return -1;
}


void CPageSource::Clear()
{
	m_nType = PSFile;
	m_strDocumentFullpath.Empty();
	m_strContentFilesPath.Empty();
	m_strPreviewFilesPath.Empty();
	m_strDocumentTitle.Empty();
	m_strCreator.Empty();
	m_strCreationDate.Empty();
	m_strSystemCreationInfo.Empty();

	m_nColorant		  = ColorantUnknown;
	m_fGlobalBBoxLeft = m_fGlobalBBoxBottom = m_fGlobalBBoxRight = m_fGlobalBBoxTop = 0.0f;
	m_PageSourceHeaders.RemoveAll();
	m_bFontsCopied	  = FALSE;
	m_bDocExist		  = TRUE;
}


// CTP-Version: Initialize BBox by using preview file 
// If file does not exist, bbox is set to 0,0,0,0
void CPageSource::InitBBox(CPageSourceHeader* pPageSourceHeader, CPrintSheet* pPrintSheet, CPlate* pPlate)
{
#ifdef CTP
	CString	strPreviewFile = GetPreviewFileName(0, pPageSourceHeader, pPrintSheet, pPlate);
	CFileStatus status;
	if ( ! CFile::GetStatus(strPreviewFile, status))
		return;

	extern int nJPEGOutWidth, nJPEGOutHeight; 
	nJPEGOutWidth  = 0; // used by paintlib in CJPEGDecoder::DoDecode() (jpegdec.cpp)
	nJPEGOutHeight = 0;

    CWinBmpEx* pDib = new CWinBmpEx;

    try
    {
		CAnyPicDecoder decoder;
        decoder.MakeBmpFromFile(strPreviewFile, pDib, 0, NULL);
    }
    catch (CTextException e)
    {
        TRACE(e);
		delete pDib;
		return;
    }
 
	if ( ! pDib)
		return;

//	int nWidth  = ( (nHeadPosition == TOP) || (nHeadPosition == BOTTOM) ) ? pDib->GetWidth()  : pDib->GetHeight();
//	int nHeight = ( (nHeadPosition == TOP) || (nHeadPosition == BOTTOM) ) ? pDib->GetHeight() : pDib->GetWidth();
	int nWidth  = pDib->GetWidth();
	int nHeight = pDib->GetHeight();
	if (pPageSourceHeader)
	{
		pPageSourceHeader->m_fBBoxLeft	 = 0.0f;
		pPageSourceHeader->m_fBBoxBottom = 0.0f;
		pPageSourceHeader->m_fBBoxRight	 = (float)((nWidth  / 72.0) * 25.4);
		pPageSourceHeader->m_fBBoxTop	 = (float)((nHeight / 72.0) * 25.4);
		for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
		{
			m_fGlobalBBoxLeft	= min(m_fGlobalBBoxLeft,   m_PageSourceHeaders[i].m_fBBoxLeft);	// Find global bbox
			m_fGlobalBBoxBottom = min(m_fGlobalBBoxBottom, m_PageSourceHeaders[i].m_fBBoxBottom);
			m_fGlobalBBoxRight  = max(m_fGlobalBBoxRight,  m_PageSourceHeaders[i].m_fBBoxRight);
			m_fGlobalBBoxTop	= max(m_fGlobalBBoxTop,	   m_PageSourceHeaders[i].m_fBBoxTop);
		}
	}
	else
	{
		m_fGlobalBBoxLeft	= 0.0f;
		m_fGlobalBBoxBottom = 0.0f;
		m_fGlobalBBoxRight	= (float)((nWidth  / 72.0) * 25.4);
		m_fGlobalBBoxTop	= (float)((nHeight / 72.0) * 25.4);
	}

	delete pDib;

#endif
}


// Get number of different pages (not page separations)
int	CPageSource::GetNumPages()
{
	int	nCurPageNum = 0;
	int nPages		= 0;
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		if (m_PageSourceHeaders[i].m_nPageNumber != nCurPageNum)
			nPages++;

		nCurPageNum = m_PageSourceHeaders[i].m_nPageNumber;
	}
	return nPages;
}

#ifdef CTP
CString	CPageSource::GetHiResBMPFileName(CPageSourceHeader* pObjectSourceHeader)
{
	if (m_strContentFilesPath.GetLength() > 0)
		if (m_strContentFilesPath[m_strContentFilesPath.GetLength() - 1] != '\\')
			m_strContentFilesPath += '\\';

	CString strFileName;
	if (m_bBmpFP)
	{
		if (theApp.settings.m_bExtendedColorHandling)
			strFileName.Format("%04d\\*_%s",   (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_strColorName : "000");
		else
			strFileName.Format("%04d\\*_%03d", (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_nColorIndex : 0);

		WIN32_FIND_DATA findData;
		HANDLE hfileSearch = FindFirstFile(m_strContentFilesPath + strFileName, &findData);
		if (hfileSearch != INVALID_HANDLE_VALUE)
			strFileName.Format("%04d\\%s", (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, findData.cFileName);
	}
	else
	{
		if (theApp.settings.m_bExtendedColorHandling)
			strFileName.Format("%08d.%s",   (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_strColorName : "000");
		else
			strFileName.Format("%08d.%03d", (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_nColorIndex : 0);
	}

	return strFileName;
}
#endif

CString	CPageSource::GetPreviewFileName(int nPageNumInSourceFile, CPageSourceHeader* pObjectSourceHeader, CPrintSheet* pPrintSheet, CPlate* pPlate)
{
	if (m_strPreviewFilesPath.GetLength() > 0)
		if (m_strPreviewFilesPath[m_strPreviewFilesPath.GetLength() - 1] != '\\')
			m_strPreviewFilesPath += '\\';

	CString strFileName;
#ifdef CTP
	nPageNumInSourceFile = nPageNumInSourceFile;
	if (m_nType != SystemCreated)	// in CTP version all marks are system created, so if not, must be a page
	{
		CString strPath;
		if (m_bPreviewsInHiresFolder)
			strPath.Format("%s\\%04d\\Previews\\", m_strContentFilesPath, pObjectSourceHeader->m_nPageNumber);
		else
			strPath = m_strPreviewFilesPath;

		if (theApp.settings.m_bExtendedColorHandling)
			strFileName.Format("%s%08d.%s",	  strPath, (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_strColorName : "000");
		else
			strFileName.Format("%s%08d.%03d", strPath, (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_nColorIndex : 0);

		////// check if preview exists. if not, try again with different path /////
		if (m_bPreviewsInHiresFolder)
			if (_access((LPCTSTR)strFileName, 0) == -1)
			{
				CString strPath2, strFileName2;
				strPath2.Format("%s\\Previews\\", m_strContentFilesPath);
				if (theApp.settings.m_bExtendedColorHandling)
					strFileName2.Format("%s%08d.%s",   strPath2, (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_strColorName : "000");
				else
					strFileName2.Format("%s%08d.%03d", strPath2, (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_nColorIndex : 0);
				if (_access((LPCTSTR)strFileName2, 0) != -1)
					strFileName = strFileName2;
			}
		//////////////////////////////////////////////////////////////////////////
	}
	else	// must be a mark
	{
		if (pPrintSheet)
			strFileName = GetMarkPreviewFileName(pPrintSheet, pPlate);
		else
			strFileName.Format("%s\\%s", theApp.settings.m_szMarksDir, m_strContentFilesPath);
	}
#else
	strFileName.Format("%sKim%05d", m_strPreviewFilesPath, nPageNumInSourceFile - 1);	// numbers beginning with 0
#endif																					// let DrawPreview() look for file type (JPEG or BMP)

	return strFileName;
}

#ifdef CTP
CString	CPageSource::GetMarkPreviewFileName(CPrintSheet* pPrintSheet, CPlate* pPlate)
{
	CString strFileName;
	char	szDest[1024];
	CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)m_strContentFilesPath, pPrintSheet, pPlate, -1);
	strFileName.Format("%s\\%s", theApp.settings.m_szMarksDir, szDest);

	return strFileName;
}
#endif																					// let DrawPreview() look for file type (JPEG or BMP)

BOOL CPageSource::HasLinks(void)
{
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
		if (m_PageSourceHeaders[i].m_PageTemplateRefs.GetSize())
			return TRUE;

	return FALSE;
}

CPrintSheet* CPageSource::GetFirstPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	POSITION	pos  = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.HasPageSource(this))
			return &rPrintSheet;
	}

	return NULL;
}

CPrintSheet* CPageSource::GetNextPrintSheet(CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	POSITION	pos	 = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (&rPrintSheet == pPrintSheet)
			break;
	}

	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.HasPageSource(this))
			return &rPrintSheet;
	}

	return NULL;
}

BOOL CPageSource::DocumentExists(CPrintSheet* pPrintSheet)
{
	CString	strDoc = m_strDocumentFullpath;
	if (pPrintSheet)
	{
		char szDest[1024];
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strDoc, pPrintSheet, NULL, -1);
		strDoc = szDest;
	}

	CFileStatus fileStatus;
	if (CFile::GetStatus(strDoc, fileStatus))	// file exists
		return TRUE;
	else
		return FALSE;
}

int	CPageSource::GetSystemGeneratedMarkType()
{
	if (m_nType != SystemCreated)
		return -1;

	int	nIndex = m_strSystemCreationInfo.Find(" ");
#ifdef PDF
	if (nIndex == -1)	// invalid string
		return -1;
#endif

	CString strContentType = m_strSystemCreationInfo.Left(nIndex);

	if (strContentType == "$TEXT")
		return CMark::TextMark;
	else
		if (strContentType == "$SECTION_BAR")
			return CMark::SectionMark;
		else
			if (strContentType == "$CROP_LINE")
				return CMark::CropMark;
			else
				if (strContentType == "$FOLD_MARK")
					return CMark::FoldMark;
				else
					if (strContentType == "$BARCODE")
						return CMark::BarcodeMark;
					else
						return CMark::CustomMark;
}

float CPageSource::GetBBox(int nSide, CPageSourceHeader* pObjectSourceHeader)
{
	switch (nSide)
	{
	case LEFT:		return (pObjectSourceHeader) ? pObjectSourceHeader->m_fBBoxLeft	  : m_fGlobalBBoxLeft;	 break;
	case RIGHT:		return (pObjectSourceHeader) ? pObjectSourceHeader->m_fBBoxRight  : m_fGlobalBBoxRight;  break;
	case BOTTOM:	return (pObjectSourceHeader) ? pObjectSourceHeader->m_fBBoxBottom : m_fGlobalBBoxBottom; break;
	case TOP:		return (pObjectSourceHeader) ? pObjectSourceHeader->m_fBBoxTop	  : m_fGlobalBBoxTop;	 break;
	}
	return 0.0f;
}

CString	CPageSource::GetSpotColor(int nIndex)
{
	int nNum = 0;
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(m_PageSourceHeaders[i].m_strColorName);
		if (pColorDef)
			if (pColorDef->m_nColorType == CColorDefinition::Spot)
			{
				if (nNum == nIndex)
					return pColorDef->m_strColorName;
				nNum++;
			}
	}
	return "";
}

BOOL CPageSource::IsMemberOfProductPart(int nProductPartIndex)
{
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		CPageTemplate* pTemplate = m_PageSourceHeaders[i].GetFirstPageTemplate();
		while (pTemplate)
		{
			if (pTemplate->m_nProductPartIndex == nProductPartIndex)
				return TRUE;
			pTemplate = m_PageSourceHeaders[i].GetNextPageTemplate(pTemplate);
		}
	}
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CPageSourceHeader

IMPLEMENT_SERIAL(CPageSourceHeader, CPageSourceHeader, VERSIONABLE_SCHEMA | 6)

CPageSourceHeader::CPageSourceHeader()
{
	m_nPageNumber	   = 0;
	m_bPageNumberFix   = FALSE;
	m_rgb			   = WHITE;
	m_nColorIndex      = 0;
	m_fBBoxLeft		   = 0.0f;
	m_fBBoxBottom	   = 0.0f;
	m_fBBoxRight	   = 0.0f; 
	m_fBBoxTop		   = 0.0f;
	m_fTrimBoxLeft	   = 0.0f;
	m_fTrimBoxBottom   = 0.0f;
	m_fTrimBoxRight	   = 0.0f; 
	m_fTrimBoxTop	   = 0.0f;
}

CPageSourceHeader::CPageSourceHeader(const CPageSourceHeader& rPageSourceHeader)
{
	Copy(rPageSourceHeader);
}

const CPageSourceHeader& CPageSourceHeader::operator=(const CPageSourceHeader& rPageSourceHeader)
{
	Copy(rPageSourceHeader);

	return *this;
}

BOOL CPageSourceHeader::operator==(const CPageSourceHeader& rPageSourceHeader)
{
	if (m_strPageName	 != rPageSourceHeader.m_strPageName)
		return FALSE;
	if (m_nPageNumber	 != rPageSourceHeader.m_nPageNumber)
		return FALSE;
	if (m_bPageNumberFix != rPageSourceHeader.m_bPageNumberFix)
		return FALSE;
	if (m_strColorName	 != rPageSourceHeader.m_strColorName)
		return FALSE;
	if (m_rgb			 != rPageSourceHeader.m_rgb)
		return FALSE;
	if (m_fBBoxLeft		 != rPageSourceHeader.m_fBBoxLeft)
		return FALSE;
	if (m_fBBoxBottom	 != rPageSourceHeader.m_fBBoxBottom)
		return FALSE;
	if (m_fBBoxRight	 != rPageSourceHeader.m_fBBoxRight)
		return FALSE;
	if (m_fBBoxTop		 != rPageSourceHeader.m_fBBoxTop)
		return FALSE;

#ifdef PDF
	if (m_fTrimBoxLeft	 != rPageSourceHeader.m_fTrimBoxLeft)
		return FALSE;
	if (m_fTrimBoxBottom != rPageSourceHeader.m_fTrimBoxBottom)
		return FALSE;
	if (m_fTrimBoxRight	 != rPageSourceHeader.m_fTrimBoxRight)
		return FALSE;
	if (m_fTrimBoxTop	 != rPageSourceHeader.m_fTrimBoxTop)
		return FALSE;
#endif

#ifdef CTP
	if (m_nColorIndex	 != rPageSourceHeader.m_nColorIndex)
		return FALSE;
#endif

	return TRUE;
}

BOOL CPageSourceHeader::operator!=(const CPageSourceHeader& rPageSourceHeader)
{
	return !(*this == rPageSourceHeader);
}


void CPageSourceHeader::Copy(const CPageSourceHeader& rPageSourceHeader)
{
	m_strPageName		= rPageSourceHeader.m_strPageName;
	m_nPageNumber		= rPageSourceHeader.m_nPageNumber;
	m_bPageNumberFix	= rPageSourceHeader.m_bPageNumberFix;
	m_strColorName		= rPageSourceHeader.m_strColorName;
	m_rgb				= rPageSourceHeader.m_rgb;
	m_nColorIndex		= rPageSourceHeader.m_nColorIndex;
	m_fBBoxLeft			= rPageSourceHeader.m_fBBoxLeft;
	m_fBBoxBottom		= rPageSourceHeader.m_fBBoxBottom;	
	m_fBBoxRight		= rPageSourceHeader.m_fBBoxRight;
	m_fBBoxTop			= rPageSourceHeader.m_fBBoxTop;		
	m_fTrimBoxLeft		= rPageSourceHeader.m_fTrimBoxLeft;
	m_fTrimBoxBottom	= rPageSourceHeader.m_fTrimBoxBottom;	
	m_fTrimBoxRight		= rPageSourceHeader.m_fTrimBoxRight;
	m_fTrimBoxTop		= rPageSourceHeader.m_fTrimBoxTop;		
	for (int i = 0; i < m_spotColors.GetSize(); i++)
		m_spotColors[i].Empty();
	m_spotColors.RemoveAll();
	m_spotColors.Append(rPageSourceHeader.m_spotColors);
}


// helper function for PageSourceList elements
template <> void AFXAPI SerializeElements <CPageSourceHeader> (CArchive& ar, CPageSourceHeader* pPageSourceHeader, int nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPageSourceHeader));
	
	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();
	
	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPageSourceHeader->m_strPageName;
			ar << pPageSourceHeader->m_nPageNumber;
			ar << pPageSourceHeader->m_bPageNumberFix;
			ar << pPageSourceHeader->m_strColorName;
			ar << pPageSourceHeader->m_rgb;
			ar << pPageSourceHeader->m_nColorIndex;
			ar << pPageSourceHeader->m_fBBoxLeft;
			ar << pPageSourceHeader->m_fBBoxBottom;
			ar << pPageSourceHeader->m_fBBoxRight;
			ar << pPageSourceHeader->m_fBBoxTop;
			ar << pPageSourceHeader->m_fTrimBoxLeft;
			ar << pPageSourceHeader->m_fTrimBoxBottom;
			ar << pPageSourceHeader->m_fTrimBoxRight;
			ar << pPageSourceHeader->m_fTrimBoxTop;
			pPageSourceHeader->m_spotColors.Serialize(ar);
		}
		else
		{
			pPageSourceHeader->m_spotColors.RemoveAll();
			switch (nVersion)
			{
			case 1:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				break;
			case 2:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				break;
			case 3:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				break;
			case 4:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				ar >> pPageSourceHeader->m_fTrimBoxLeft;
				ar >> pPageSourceHeader->m_fTrimBoxBottom;
				ar >> pPageSourceHeader->m_fTrimBoxRight;
				ar >> pPageSourceHeader->m_fTrimBoxTop;
				break;
			case 5:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				ar >> pPageSourceHeader->m_fTrimBoxLeft;
				ar >> pPageSourceHeader->m_fTrimBoxBottom;
				ar >> pPageSourceHeader->m_fTrimBoxRight;
				ar >> pPageSourceHeader->m_fTrimBoxTop;
				BOOL bDummy;
				ar >> bDummy;	// former m_bRemapSpotColors
				pPageSourceHeader->m_spotColors.Serialize(ar);
				break;
			case 6:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				ar >> pPageSourceHeader->m_fTrimBoxLeft;
				ar >> pPageSourceHeader->m_fTrimBoxBottom;
				ar >> pPageSourceHeader->m_fTrimBoxRight;
				ar >> pPageSourceHeader->m_fTrimBoxTop;
				pPageSourceHeader->m_spotColors.Serialize(ar);
				break;
			}
		}
		pPageSourceHeader++;
	}
}


void AFXAPI DestructElements(CPageSourceHeader* pPageSourceHeader, int nCount)
{
	for (; nCount > 0; nCount--)
	{
		pPageSourceHeader->m_strPageName.Empty();
		pPageSourceHeader->m_strColorName.Empty();
		for (int i = 0; i < pPageSourceHeader->m_spotColors.GetSize(); i++)
			pPageSourceHeader->m_spotColors[i].Empty();
		pPageSourceHeader->m_spotColors.RemoveAll();

		pPageSourceHeader++;
	}
}

// Used for registering PDF-Documents in PDF-Version
void CPageSourceHeader::Create(int nPageNum, const CString& strPageName, float fBBoxLeft, float fBBoxBottom, float fBBoxRight, float fBBoxTop, const CString& strColorName, BOOL bDefineColorPrompt)
{
	m_nPageNumber    = nPageNum;
	m_bPageNumberFix = (strPageName.IsEmpty()) ? FALSE : TRUE;
	if (strPageName.IsEmpty())
		m_strPageName.Format("%d", nPageNum);
	else
		m_strPageName = strPageName;
	m_fBBoxLeft		 = fBBoxLeft   * PICA_POINTS;
	m_fBBoxBottom	 = fBBoxBottom * PICA_POINTS;
	m_fBBoxRight	 = fBBoxRight  * PICA_POINTS;
	m_fBBoxTop		 = fBBoxTop	   * PICA_POINTS;

/////////////////////////////////////////////////////////////////////////////
// Look for the right color-name and RGB-value
	CString strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);
 
	if (strColorName.IsEmpty())
	{
		m_strColorName = strUnknown;
		m_rgb		   = WHITE;
	}
	else
	{
		m_strColorName = theApp.m_colorDefTable.FindColorReferenceName(strColorName);
		if (m_strColorName.IsEmpty())
		{
			if (bDefineColorPrompt)
			{
				CString strMessage;
				AfxFormatString1(strMessage, IDS_NEW_COLOR_NAME_FOUND, strColorName);
				if (AfxMessageBox(strMessage, MB_ICONQUESTION | MB_YESNO) == IDYES)
				{
					CDlgPrintColorProps dlg;
					dlg.m_colorDefTable.Append(theApp.m_colorDefTable);
					dlg.m_strPrintColorName	 = strColorName;
					dlg.m_rgbColor			 = WHITE; 
					dlg.m_nIPUIndex			 = 0;
					dlg.m_nProcessCMYK		 = -1;
					dlg.m_nPrintColorType	 = 1;	

					if (dlg.DoModal() == IDCANCEL)
					{
						m_strColorName = strColorName;
						m_rgb		   = WHITE;
					}
					else
					{
						switch (dlg.m_nPrintColorType)
						{
						case 0:	
						case 1: {
									CColorDefinition colorDef(dlg.m_strPrintColorName, dlg.m_rgbColor, 
															 (dlg.m_nPrintColorType == 0) ? dlg.m_nProcessCMYK : CColorDefinition::Spot, dlg.m_nIPUIndex);
									theApp.m_colorDefTable.InsertColorDef(colorDef);
								}
								break;
						case 2: {
									int nBasicColorIndex = theApp.m_colorDefTable.FindColorDefinitionIndex(dlg.m_strColorMappingTarget);
									if (nBasicColorIndex >= 0)
										theApp.m_colorDefTable[nBasicColorIndex].m_AliasNameList.AddTail(dlg.m_strPrintColorName);
								}
								break;
						}
						m_strColorName = strColorName;
						m_rgb		   = theApp.m_colorDefTable.FindColorRef(strColorName); 
						theApp.m_colorDefTable.Save();
					}
				}
				else
				{
					m_strColorName = strColorName;
					m_rgb		   = WHITE;
				}
			}
			else
			{
				m_strColorName = strColorName;
				m_rgb		   = WHITE;
			}
		}
		else
			m_rgb = theApp.m_colorDefTable.FindColorRef(m_strColorName); 
	}
}


// Used for assign manually pages in CTP-Version
void CPageSourceHeader::Create(int nIndex)
{
	m_strPageName.Format("%d", nIndex + 1);
	m_nPageNumber = nIndex + 1;
}


// Used for registering BMP-Files in CTP-Version
void CPageSourceHeader::Create(int nIndex, const CString& strFilePath, int nColorIndex)
{
    CFile		   file;
	CFileException fileException;

    if (!file.Open( (LPCTSTR)strFilePath, CFile::modeRead, &fileException)) // Will be closed in the destructor
	{
		TRACE	("Can't open file %s, error = %u\n",
				strFilePath, fileException.m_cause);
	}
	else
	{
		CString message;
		int nCount, nSize;
		BITMAPFILEHEADER bmfh;
	    LPBITMAPINFOHEADER m_lpBMIH; // buffer containing the BITMAPINFOHEADER
		TRY {
			nCount = file.Read((LPVOID) &bmfh, sizeof(BITMAPFILEHEADER));
			if(nCount != sizeof(BITMAPFILEHEADER))
			{
				message.Format(_T("read error 1: %s"), strFilePath);
				AfxMessageBox(message);
				return;
			}
			if(bmfh.bfType != 0x4d42)
			{
				message.Format(_T("Invalid bitmap file: %s"), strFilePath);
				AfxMessageBox(message);
				return;
			}
			nSize		 = bmfh.bfOffBits - sizeof(BITMAPFILEHEADER);
			m_lpBMIH	 = (LPBITMAPINFOHEADER) new char[nSize];
			//m_nBmihAlloc = m_nImageAlloc = crtAlloc;
			nCount		 = file.Read(m_lpBMIH, nSize); // info hdr & color table
			m_fBBoxLeft		= 0.0f;
			m_fBBoxBottom	= 0.0f;
			m_fBBoxRight	= (float)((m_lpBMIH->biWidth  / 72.0) * 25.4);
			m_fBBoxTop		= (float)((m_lpBMIH->biHeight / 72.0) * 25.4);

			delete m_lpBMIH;
		}
		CATCH (CException, e)
		{
			AfxMessageBox("Read error 1");
			return;
		}
		END_CATCH
	}

	m_strPageName.Format("%d", nIndex + 1);
	m_nPageNumber = nIndex + 1;

	CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(nColorIndex);
	if (pColorDef)
	{
		m_strColorName	= pColorDef->m_strColorName;
		m_rgb			= pColorDef->m_rgb;
		m_nColorIndex	= pColorDef->m_nColorIndex;
	}
	else
	{
		m_strColorName 	= "Undefined";
		m_rgb			= WHITE;
		m_nColorIndex	= 0;
	}
}


CPageTemplate* CPageSourceHeader::GetFirstPageTemplate()
{
	if (m_PageTemplateRefs.GetSize())
		return m_PageTemplateRefs[0];
	else
		return NULL;
}

CPageTemplate* CPageSourceHeader::GetNextPageTemplate(CPageTemplate* pPageTemplate)
{
	int i;
	for (i = 0; i < m_PageTemplateRefs.GetSize(); i++)
		if (m_PageTemplateRefs[i] == pPageTemplate)
			break;

	i++;
	if (i < m_PageTemplateRefs.GetSize())
		return m_PageTemplateRefs[i];			
	else
		return NULL;
}

BOOL CPageSourceHeader::PreviewExists(CPageSource* pPageSource, int nColorSepIndex)
{
#ifdef CTP
	CString strPreviewFile;
	strPreviewFile = pPageSource->GetPreviewFileName(0, this);
	nColorSepIndex = nColorSepIndex;	// not used in CTP version
	return (_access((LPCTSTR)strPreviewFile, 0) == -1) ? FALSE : TRUE;
#else
	CString strPreviewFile;
	CPageTemplate* pTemplate = GetFirstPageTemplate();
	strPreviewFile = pPageSource->GetPreviewFileName(pTemplate->m_ColorSeparations[nColorSepIndex].m_nPageNumInSourceFile, this);
	return (_access((LPCTSTR)strPreviewFile, 0) == -1) ? FALSE : TRUE;
#endif

	return FALSE;
}

void CPageSourceHeader::ExtractSpotColors(const CString& strColorSpaces)
{
	m_spotColors.RemoveAll();

	TCHAR szColorSpaces[1024];
	strncpy(szColorSpaces, strColorSpaces, 1023);

	CString strColorSpace;
	TCHAR*  pszToken;
	pszToken = strtok(szColorSpaces, "\n");
	while (pszToken)
	{
		strColorSpace = pszToken;
		strColorSpace.TrimLeft(); strColorSpace.TrimRight();
		BOOL bIsSpotColor = TRUE;
		if (strColorSpace == _T("DeviceCMYK"))				 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("DeviceRGB"))				 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("DeviceGray"))				 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("CalRGB"))					 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("CalGray"))					 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("Lab"))						 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("ICCBased"))				 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("Separation"))				 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("DeviceN"))					 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("Indexed"))					 bIsSpotColor = FALSE; else
		if (strColorSpace == _T("Pattern"))					 bIsSpotColor = FALSE; else
		if (strColorSpace.CompareNoCase(_T("Black"))   == 0) bIsSpotColor = FALSE; else
		if (strColorSpace.CompareNoCase(_T("Cyan"))	   == 0) bIsSpotColor = FALSE; else
		if (strColorSpace.CompareNoCase(_T("Magenta")) == 0) bIsSpotColor = FALSE; else
		if (strColorSpace.CompareNoCase(_T("Yellow"))  == 0) bIsSpotColor = FALSE; else
		if (strColorSpace.CompareNoCase(_T("All"))	   == 0) bIsSpotColor = FALSE; else
		if (strColorSpace.CompareNoCase(_T("None"))	   == 0) bIsSpotColor = FALSE;

		if (bIsSpotColor)
			m_spotColors.Add(strColorSpace);

		pszToken = strtok(NULL, "\n");
	}
}

int	CPageSourceHeader::GetColorType()
{
	CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(m_strColorName);
	if ( ! pColorDef)
		return CColorDefinition::Spot;
	else
		return pColorDef->m_nColorType;
}

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CLabelList

IMPLEMENT_SERIAL(CLabelList, CObject, VERSIONABLE_SCHEMA | 1)

CLabelList::CLabelList()
{
}

void CLabelList::CheckLayoutObjectFormats()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return;

	for (int i = 0; i < GetSize(); i++)
	{
		CLabelListEntry& rLabelType = ElementAt(i);
		pDoc->m_PrintSheetList.CheckLayoutObjectFormats(i, rLabelType.m_fLabelWidth, rLabelType.m_fLabelHeight, "", -1, TRUE);
	}
}

int CLabelList::Find(const CString& strName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i).m_strName == strName)
			return i;
	}

	return -1;
}

int CLabelList::FindLabelIndex(CLabelListEntry* pLabelListEntry)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (&ElementAt(i) == pLabelListEntry)
			return i;
	}

	return -1;
}

BOOL CLabelList::HasGraphics()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i).m_graphicList.m_elements.GetSize())
			return TRUE;
	}
	return FALSE;
}

float CLabelList::GetMaxHeight()
{
	float fMaxHeight = 0.0f;
	for (int i = 0; i < GetSize(); i++)
	{
		fMaxHeight = max(fMaxHeight, ElementAt(i).m_fLabelHeight);
	}
	return fMaxHeight;
}


/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CLabelListEntry

IMPLEMENT_SERIAL(CLabelListEntry, CObject, VERSIONABLE_SCHEMA | 1)

CLabelListEntry::CLabelListEntry()
{
	m_strName.Empty();
	m_fLabelWidth  = 0.0f;
	m_fLabelHeight = 0.0f;
	m_nQuantity	   = 0L;
	m_strComment.Empty();
}

const CLabelListEntry& CLabelListEntry::operator=(const CLabelListEntry& rLabelListEntry)
{
	m_strName	   = rLabelListEntry.m_strName;
	m_fLabelWidth  = rLabelListEntry.m_fLabelWidth;
	m_fLabelHeight = rLabelListEntry.m_fLabelHeight;
	m_nQuantity	   = rLabelListEntry.m_nQuantity;
	m_strComment   = rLabelListEntry.m_strComment;

	m_graphicList.m_elements.RemoveAll();
	m_graphicList.m_elements.Append(rLabelListEntry.m_graphicList.m_elements);

	return *this;
}

void CLabelListEntry::AddGraphicObject(CGraphicObject* pgObject)
{
	m_graphicList.CopyGraphicObject(pgObject);
}

CPrintSheet* CLabelListEntry::GetFirstPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return NULL;

	int nLabelIndex		  = pDoc->m_labelList.FindLabelIndex(this);
	int nProductPartIndex = pDoc->m_productPartList.GetLabelsProductPartIndex();
	if ( (nLabelIndex < 0) || (nProductPartIndex < 0) )
		return NULL;
	if (nLabelIndex*2 >= pDoc->m_PageTemplateList.GetSize())
		return NULL;
	POSITION pos = pDoc->m_PageTemplateList.FindIndex(nLabelIndex*2, nProductPartIndex);
	if ( ! pos)
		return NULL;
	CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetAt(pos);
	if (rTemplate.m_PrintSheetLocations.GetSize() <= 0)
		return NULL;

	pos = pDoc->m_PrintSheetList.FindIndex(rTemplate.m_PrintSheetLocations[0].m_nPrintSheetIndex);
	if (!pos)
		return NULL;
	CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetAt(pos);
	return &rPrintSheet;
}

CPrintSheet* CLabelListEntry::GetNextPrintSheet(CPrintSheet* pPrintSheet)
{
	return NULL;
}


// helper function for CLabelListEntry elements
template <> void AFXAPI SerializeElements <CLabelListEntry> (CArchive& ar, CLabelListEntry* pLabelListEntry, int nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CLabelListEntry));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pLabelListEntry->m_strName;
			ar << pLabelListEntry->m_fLabelWidth;
			ar << pLabelListEntry->m_fLabelHeight;
			ar << pLabelListEntry->m_nQuantity;
			ar << pLabelListEntry->m_strComment;
			pLabelListEntry->m_graphicList.Serialize(ar);
		}
		else
		{
			switch (nVersion)
			{
			case 1: ar >> pLabelListEntry->m_strName;
					ar >> pLabelListEntry->m_fLabelWidth;
					ar >> pLabelListEntry->m_fLabelHeight;
					ar >> pLabelListEntry->m_nQuantity;
					ar >> pLabelListEntry->m_strComment;
					pLabelListEntry->m_graphicList.Serialize(ar);
					break; 
			}
		}
		pLabelListEntry++;
	}
}

void AFXAPI DestructElements(CLabelListEntry* pLabelListEntry, int nCount)
{
	for (; nCount > 0; nCount--)
	{
		pLabelListEntry->m_strName.Empty();
		pLabelListEntry->m_strComment.Empty();
		pLabelListEntry->m_graphicList.Destruct();

		pLabelListEntry++;
	}
}

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CColorDefinitionTable

IMPLEMENT_SERIAL(CColorDefinitionTable, CColorDefinitionTable, VERSIONABLE_SCHEMA | 1)

int gColorDefSchema = 0;

CColorDefinitionTable::CColorDefinitionTable()
{
}


short CColorDefinitionTable::InsertColorDef(CColorDefinition& rColorDefToInsert)
{
	for (short i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (/*(rColorDef.m_rgb == rColorDefToInsert.m_rgb) && */(rColorDef.m_strColorName == rColorDefToInsert.m_strColorName))
			return i;
	}

	Add(rColorDefToInsert);
	return (short)(GetSize() - 1);
}


void CColorDefinitionTable::Load()
{
	CFile		   file;
	CFileException fileException;
	char		   pszFileName[MAX_PATH];
	strcpy(pszFileName, theApp.settings.m_szDataDir);
	strcat(pszFileName, "\\Colors.def");
	if (!file.Open(pszFileName, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return; 
	}
	
	gColorDefSchema = 0;
	CArchive archive(&file, CArchive::load);
	Serialize(archive);
	archive.Close();
	file.Close();

	if (gColorDefSchema < 4)
	{
		for (int i = 0; i < GetSize(); i++)
			if (ElementAt(i).m_strColorName.CompareNoCase(_T("COMPOSITE")) == 0)
			{
				RemoveAt(i);
				i--;
			}
	}
}

void CColorDefinitionTable::Save()
{
	CFile		   file;
	CFileException fileException;
	char		   pszFileName[MAX_PATH];
	strcpy(pszFileName, theApp.settings.m_szDataDir);
	strcat(pszFileName, "\\Colors.def");
	if (!file.Open(pszFileName, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return; 
	}
	CArchive archive(&file, CArchive::store);
	Serialize(archive);
	archive.Close();
	file.Close();
}

CString	CColorDefinitionTable::GetColorName(int nColorDefinitionIndex)
{
	if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= GetSize()) )
		return "";

	return ElementAt(nColorDefinitionIndex).m_strColorName;
}

CString CColorDefinitionTable::FindColorReferenceName(const CString& strColor)
{
	if (strColor.CompareNoCase(_T("Composite")) == 0)
		return strColor;

	BOOL	bColorFound = FALSE;
	CString strColorName;

	int nSize = GetSize();
	for (int i = 0; i < nSize; i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName == strColor)
		{
			strColorName = rColorDef.m_strColorName;
			bColorFound	   = TRUE;
			break;
		}
		else
		{
			POSITION pos = rColorDef.m_AliasNameList.GetHeadPosition();
			while(pos)
			{
				CString& rAlias = rColorDef.m_AliasNameList.GetNext(pos);
				if (rAlias == strColor)
				{
					strColorName = rColorDef.m_strColorName;
					bColorFound	   = TRUE;
					break;
				}
			}
			if (bColorFound)
				break;
		}
	}
	if (bColorFound)
		return (strColorName);
	else
		return (_T(""));
}

COLORREF CColorDefinitionTable::FindColorRef(const CString& strColor)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName == strColor)
			return rColorDef.m_rgb;
		else
		{
			POSITION pos = rColorDef.m_AliasNameList.GetHeadPosition();
			while(pos)
			{
				CString& rAlias = rColorDef.m_AliasNameList.GetNext(pos);
				if (rAlias == strColor)
					return rColorDef.m_rgb;
			}
		}
	}
	return WHITE;
}

short CColorDefinitionTable::FindColorDefinitionIndex(const CString& strColor)
{
	for (short i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName == strColor)
			return i;
	}
	return -1;
}

CColorDefinition* CColorDefinitionTable::FindColorDefinition(int nColorIndex)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorIndex == nColorIndex)
			if (rColorDef.m_strColorName.CompareNoCase(_T("Composite")) == 0)		// composite should not be defined in color table
				return NULL;
			else
				return &rColorDef;
	}
	return NULL;
}

CColorDefinition* CColorDefinitionTable::FindColorDefinition(const CString& strColor)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName == strColor)
			if (rColorDef.m_strColorName.CompareNoCase(_T("Composite")) == 0)		// composite should not be defined in color table
				return NULL;
			else
				return &rColorDef;
	}
	return NULL;
}

CString	CColorDefinitionTable::GetCyanName()
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorType == CColorDefinition::ProcessC)
			return rColorDef.m_strColorName;
	}
	return _T("Cyan");
}

CString	CColorDefinitionTable::GetMagentaName()
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorType == CColorDefinition::ProcessM)
			return rColorDef.m_strColorName;
	}
	return _T("Magenta");
}

CString	CColorDefinitionTable::GetYellowName()
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorType == CColorDefinition::ProcessY)
			return rColorDef.m_strColorName;
	}
	return _T("Yellow");
}

CString	CColorDefinitionTable::GetKeyName()
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorType == CColorDefinition::ProcessK)
			return rColorDef.m_strColorName;
	}
	return _T("Black");
}

void CColorDefinitionTable::UpdateColorInfos()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (!pDoc)
		return;

	pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results
	for (short i = 0; i < GetSize(); i++)
	{
		if ( ! pDoc->m_PageTemplateList.ColorInUse(pDoc, i))	// remove unused colors
		{
			if ( ! pDoc->m_MarkTemplateList.ColorInUse(pDoc, i))
			{
				RemoveAt(i);				
				pDoc->m_PageTemplateList.DecrementColorDefIndices(i); 
				pDoc->m_MarkTemplateList.DecrementColorDefIndices(i);
				pDoc->m_PrintSheetList.DecrementColorDefIndices(i);
				i--;
			}
		}
		if ( (i >= 0) && (i < GetSize()) )
			ElementAt(i).m_rgb = theApp.m_colorDefTable.FindColorRef(ElementAt(i).m_strColorName);
	}

	POSITION pos = pDoc->m_PageSourceList.GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = pDoc->m_PageSourceList.GetNext(pos);
		for (int i = 0; i < rPageSource.m_PageSourceHeaders.GetSize(); i++)
		{
			CPageSourceHeader& rPageSourceHeader = rPageSource.m_PageSourceHeaders[i];
			if (rPageSourceHeader.GetFirstPageTemplate())	// check if assigned to any page
				if (rPageSourceHeader.m_strColorName.CompareNoCase(_T("Composite")) == 0)
				{
					for (int j = 0; j < rPageSourceHeader.m_spotColors.GetSize(); j++)
					{
						CColorDefinition colorDef(	rPageSourceHeader.m_spotColors[j], 
													theApp.m_colorDefTable.FindColorRef(rPageSourceHeader.m_spotColors[j]),
													CColorDefinition::Spot, 0);
						InsertColorDef(colorDef);					
					}
				}
		}
	}
}

int	CColorDefinitionTable::GetColorIndex(int nColorDefinitionIndex)
{
	if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= GetSize()))
		return 0;

	return ElementAt(nColorDefinitionIndex).m_nColorIndex;
}

int	CColorDefinitionTable::GetCurSpotColorNum(CString& strColor)
{
	if (strColor.CompareNoCase(_T("Composite")) == 0)
		return -1;
	CColorDefinition* pSystemColorDef = theApp.m_colorDefTable.FindColorDefinition(strColor);
	if (pSystemColorDef)
		if (pSystemColorDef->m_nColorType != CColorDefinition::Spot)
			return -1;

	BOOL bColorFound = FALSE;
	int  nCurSpotColorNum = 0;
	for (short i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);

		if (rColorDef.m_strColorName.CompareNoCase(_T("Composite")) != 0)
		{
			pSystemColorDef = theApp.m_colorDefTable.FindColorDefinition(rColorDef.m_strColorName);

			if ( ! pSystemColorDef)
				nCurSpotColorNum++;
			else
				if (pSystemColorDef->m_nColorType == CColorDefinition::Spot)
					nCurSpotColorNum++;

			if (rColorDef.m_strColorName == strColor)
			{
				bColorFound = TRUE;
				break;
			}
		}
	}
	return (bColorFound) ? nCurSpotColorNum : -1;
}

BOOL CColorDefinitionTable::IsProcessColor(CString& strColor)
{
	if (IsCyan(strColor))
		return TRUE;
	else
		if (IsMagenta(strColor))
			return TRUE;
		else
			if (IsYellow(strColor))
				return TRUE;
			else
				if (IsKey(strColor))
					return TRUE;

	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName.CompareNoCase(strColor) == 0)
			if ( (rColorDef.m_nColorType != CColorDefinition::Spot) && (rColorDef.m_nColorType != CColorDefinition::Alternate) )
				return TRUE;
	}
	return FALSE;
}

BOOL CColorDefinitionTable::IsCyan(CString& strColor)
{
	if (strColor.CompareNoCase(_T("CYAN")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(GetCyanName()) == 0)
		return TRUE;
	return FALSE;
}

BOOL CColorDefinitionTable::IsMagenta(CString& strColor)
{
	if (strColor.CompareNoCase(_T("MAGENTA")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(GetMagentaName()) == 0)
		return TRUE;
	return FALSE;
}

BOOL CColorDefinitionTable::IsYellow(CString& strColor)
{
	if (strColor.CompareNoCase(_T("YELLOW")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(_T("GELB")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(GetYellowName()) == 0)
		return TRUE;
	return FALSE;
}

BOOL CColorDefinitionTable::IsKey(CString& strColor)
{
	if (strColor.CompareNoCase(_T("KEY")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(_T("BLACK")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(GetKeyName()) == 0)
		return TRUE;
	return FALSE;
}




/////////////////////////////////////////////////////////////////////////////
// CColorDefinition

IMPLEMENT_SERIAL(CColorDefinition, CColorDefinition, VERSIONABLE_SCHEMA | 5)

CColorDefinition::CColorDefinition()
{
	m_rgb		  = 0;
	m_cmyk		  = 0;
	m_nColorIndex = 0;
}


const CColorDefinition& CColorDefinition::operator=(const CColorDefinition& rColorDefinition)
{
	Copy(rColorDefinition);

	return *this;
}


void CColorDefinition::Copy(const CColorDefinition& rColorDefinition)
{
	m_strColorName = rColorDefinition.m_strColorName;
	m_rgb		   = rColorDefinition.m_rgb;
	m_cmyk		   = rColorDefinition.m_cmyk;
	m_nColorType   = rColorDefinition.m_nColorType;
	m_nColorIndex  = rColorDefinition.m_nColorIndex;

	m_AliasNameList.RemoveAll();  // no append, so target list has to be empty
	unsigned nStrings = rColorDefinition.m_AliasNameList.GetCount(); 
	POSITION pos	  = rColorDefinition.m_AliasNameList.GetHeadPosition();
	CString  StringBuf;
	CString& rStringBuf = StringBuf;

	for (unsigned i = 0; i < nStrings; i++)						
	{
		rStringBuf = rColorDefinition.m_AliasNameList.GetAt(pos);
		m_AliasNameList.AddTail(rStringBuf);
		rColorDefinition.m_AliasNameList.GetNext(pos);
	}	
}


// helper function for CColorDefinitionTable elements
template <> void AFXAPI SerializeElements <CColorDefinition> (CArchive& ar, CColorDefinition* pColorDefinition, int nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CColorDefinition));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();
		
	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pColorDefinition->m_strColorName;
			ar << pColorDefinition->m_rgb;
			ar << pColorDefinition->m_cmyk;
			ar << pColorDefinition->m_nColorType;
			ar << pColorDefinition->m_nColorIndex;
			pColorDefinition->m_AliasNameList.Serialize(ar);
		}
		else
		{
			pColorDefinition->m_AliasNameList.RemoveAll();
			switch (nVersion)
			{
			case 1:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				break;
			case 2:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				ar >> pColorDefinition->m_nColorIndex;
				break;
			case 3:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				ar >> pColorDefinition->m_nColorIndex;
				pColorDefinition->m_cmyk = CColorDefinition::RGB2CMYK(pColorDefinition->m_rgb, pColorDefinition);
				pColorDefinition->m_AliasNameList.Serialize(ar);
				break;
			case 4:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				ar >> pColorDefinition->m_nColorType;
				ar >> pColorDefinition->m_nColorIndex;
				pColorDefinition->m_cmyk = CColorDefinition::RGB2CMYK(pColorDefinition->m_rgb, pColorDefinition);
				pColorDefinition->m_AliasNameList.Serialize(ar);
				break;
			case 5:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				ar >> pColorDefinition->m_cmyk;
				ar >> pColorDefinition->m_nColorType;
				ar >> pColorDefinition->m_nColorIndex;
				pColorDefinition->m_AliasNameList.Serialize(ar);
				break;
			}
			if (nVersion < 4)
			{
				CColorDefinition::ConvertColorsSchemaOlder4(pColorDefinition);
				pColorDefinition->m_cmyk = CColorDefinition::RGB2CMYK(pColorDefinition->m_rgb, pColorDefinition);
			}
			gColorDefSchema = nVersion;
		}
		pColorDefinition++;
	}
}

void CColorDefinition::ConvertColorsSchemaOlder4(CColorDefinition* pColorDefinition)
{
	pColorDefinition->m_nColorType = Spot;
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("BLACK")) == 0)
		pColorDefinition->m_nColorType = ProcessK;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("TIEFE")) == 0)
		pColorDefinition->m_nColorType = ProcessK;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("KEY")) == 0)
		pColorDefinition->m_nColorType = ProcessK;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("SCHWARZ")) == 0)
		pColorDefinition->m_nColorType = ProcessK;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("CYAN")) == 0)
		pColorDefinition->m_nColorType = ProcessC;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("ZYAN")) == 0)
		pColorDefinition->m_nColorType = ProcessC;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("MAGENTA")) == 0)
		pColorDefinition->m_nColorType = ProcessM;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("YELLOW")) == 0)
		pColorDefinition->m_nColorType = ProcessY;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("GELB")) == 0)
		pColorDefinition->m_nColorType = ProcessY;

	switch (pColorDefinition->m_nColorType)
	{
	case CColorDefinition::ProcessC:	pColorDefinition->m_rgb = CYAN;		break;
	case CColorDefinition::ProcessM:	pColorDefinition->m_rgb = MAGENTA;	break;
	case CColorDefinition::ProcessY:	pColorDefinition->m_rgb = YELLOW;	break;
	case CColorDefinition::ProcessK:	pColorDefinition->m_rgb = BLACK;	break;
	default:																break;
	}
}

COLORREF CColorDefinition::RGB2CMYK(COLORREF crRGB, CColorDefinition* pColorDefinition)
{
	float fCyan		= 0.0f;
	float fMagenta	= 0.0f;
	float fYellow	= 0.0f;
	float fKey		= 0.0f;
	if (pColorDefinition)
	{
		switch (pColorDefinition->m_nColorType)
		{
		case ProcessC:	return CMYK(100, 0, 0, 0);	
		case ProcessM:	return CMYK(0, 100, 0, 0);
		case ProcessY:	return CMYK(0, 0, 100, 0);
		case ProcessK:	return CMYK(0, 0, 0, 100);
		default:		break;
		}
	}
	fCyan		= 1.0f - (float)GetRValue(crRGB)/255.0f;	// convert to values between 0 and 1
	fMagenta	= 1.0f - (float)GetGValue(crRGB)/255.0f;	
	fYellow		= 1.0f - (float)GetBValue(crRGB)/255.0f;	
//	fKey		= min(fCyan, min(fMagenta, fYellow));

	float fVarKey = 1.0f;

	if ( fCyan	  < fVarKey )   fVarKey = fCyan;
	if ( fMagenta < fVarKey )   fVarKey = fMagenta;
	if ( fYellow  < fVarKey )   fVarKey = fYellow;
	if ( fVarKey == 1.0f ) 
	{ //Black
	   fCyan	= 0.0f;
	   fMagenta = 0.0f;
	   fYellow	= 0.0f;
	}
	else 
	{
	   fCyan	= ( fCyan	 - fVarKey ) / ( 1.0f - fVarKey );
	   fMagenta = ( fMagenta - fVarKey ) / ( 1.0f - fVarKey );
	   fYellow	= ( fYellow  - fVarKey ) / ( 1.0f - fVarKey );
	}
	fKey = fVarKey;

	return CMYK((WORD)(fCyan * 100.0f), (WORD)(fMagenta * 100.0f), (WORD)(fYellow * 100.0f), (WORD)(fKey * 100.0f));
}

COLORREF CColorDefinition::CMYK2RGB(COLORREF crCMYK)
{
	//CMYK values = 0 � 1

	float fCyan		= (float)( GetCValue(crCMYK) * ( 1 - GetKValue(crCMYK) ) + GetKValue(crCMYK) );
	float fMagenta	= (float)( GetMValue(crCMYK) * ( 1 - GetKValue(crCMYK) ) + GetKValue(crCMYK) );
	float fYellow	= (float)( GetYValue(crCMYK) * ( 1 - GetKValue(crCMYK) ) + GetKValue(crCMYK) );

	//RGB  values = 0 � 255

	float fR = ( 1 - fCyan )	* 255;
	float fG = ( 1 - fMagenta ) * 255;
	float fB = ( 1 - fYellow )	* 255;


	return RGB(fR, fG, fB);
}

void AFXAPI DestructElements(CColorDefinition* pColorDefinition, int nCount)
{
	for (; nCount > 0; nCount--)
	{
		pColorDefinition->m_strColorName.Empty();
		pColorDefinition->m_AliasNameList.RemoveAll();
		pColorDefinition++;
	}
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Implementation of CPlateList

IMPLEMENT_SERIAL(CPlateList, CObject, VERSIONABLE_SCHEMA | 2)

CPlateList::CPlateList()
{
	m_nTopmostPlate				= 0;
	m_bDisplayOpen				= FALSE;
	m_defaultPlate.m_pPlateList = this;
	m_pPrintSheet				= NULL;
}


void CPlateList::Copy(const CPlateList& rPlateList)
{
	m_nTopmostPlate = rPlateList.m_nTopmostPlate;
	m_bDisplayOpen  = rPlateList.m_bDisplayOpen;
	m_nSheetSide	= rPlateList.m_nSheetSide;
	CPlate plate;
 	POSITION pos = rPlateList.GetHeadPosition();
 	while (pos)
	{
		plate = rPlateList.GetNext(pos);
 		AddTail(plate);
	}
	m_defaultPlate = rPlateList.m_defaultPlate;
}


void CPlateList::Serialize(CArchive& ar)
{
	CList <CPlate, CPlate&>::Serialize(ar);

	ar.SerializeClass(RUNTIME_CLASS(CPlateList));

	if (ar.IsStoring())
	{
		ar << m_nTopmostPlate;
		SerializeElements <> (ar, &m_defaultPlate, 1);
	}
	else																		 
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> m_nTopmostPlate;
			break;
		case 2:
			ar >> m_nTopmostPlate;
			SerializeElements(ar, &m_defaultPlate, 1);
			break;
		}
	}
}

POSITION CPlateList::AddTail(CPlate& rPlate)
{
	POSITION pos = CList <CPlate, CPlate&>::AddTail(rPlate);
	CList <CPlate, CPlate&>::GetAt(pos).m_pPlateList = this;
	
	if (m_nTopmostPlate < 0)
		m_nTopmostPlate = 0;

	return pos;
}

void CPlateList::RemoveAt(POSITION pos)
{
	CList <CPlate, CPlate&>::RemoveAt(pos);
	if (m_nTopmostPlate >= GetCount())
		m_nTopmostPlate--;
}

CPlate*	CPlateList::GetTopmostPlate()
{
	CPlate* pPlate = NULL;

	if ( ! GetCount())
	{
		m_defaultPlate.UpdateExposures();
		return &m_defaultPlate;
	}

	if ((m_nTopmostPlate >= 0) && (m_nTopmostPlate < GetCount()))
	{
		POSITION pos = FindIndex(m_nTopmostPlate);
		if (pos)
			pPlate = &GetAt(pos);
	}

	if ( ! pPlate)
		if (GetCount())
			pPlate = &GetHead();

	return pPlate;
}

void CPlateList::SetGlobalMaskAllPlates(float fValue)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	POSITION pos = GetHeadPosition();
	while (pos) 
	{
		BOOL	bChange	= TRUE;
		CPlate& rPlate	= GetNext(pos);
		
		if (pView)
		{
			CDisplayItem* pPlateTabDI = pView->m_DisplayList.GetItemFromData((void*)&rPlate, (void*)rPlate.GetPrintSheet(),
				DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
			if (pPlateTabDI)
				if (pPlateTabDI->m_nState == CDisplayItem::Selected)	// if plate tab is selected with ctrl key 
					bChange = FALSE;									// assume plate is disabled
		}
		if (bChange)
		{
			rPlate.m_fOverbleedOutside = fValue;
			rPlate.Invalidate();
		}
	}

	// same mod�fication on default plate
	m_defaultPlate.m_fOverbleedOutside = fValue;
	m_defaultPlate.Invalidate();
}

CString CPlateList::GetGlobalMaskValueString()
{
	if (GetCount() <= 0)
	{
		m_defaultPlate.UpdateExposures();
		return m_defaultPlate.GetGlobalMaskValueString();
	}

	POSITION pos = GetHeadPosition();
	if ( ! pos)
		return L"";

	float fPrevValue = GetNext(pos).m_fOverbleedOutside;
	while (pos) 
	{
		CPlate& rPlate = GetNext(pos);
		if (rPlate.m_fOverbleedOutside != fPrevValue)
			return L"...";

		fPrevValue = rPlate.m_fOverbleedOutside;
	}

	CString strMeasure;
	SetMeasure(0, fPrevValue, strMeasure);
	return strMeasure;
}

CPlate* CPlateList::GetPlateByColor(short nColorDefinitionIndex)
{
	if (GetCount())
	{
		POSITION pos = GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = GetNext(pos);
			if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
				return &rPlate;
		}
	}
	else
		return &m_defaultPlate;

	return NULL;
}

CPlate* CPlateList::GetPlateByIndex(int nIndex)
{
	POSITION pos = FindIndex(nIndex);
	return ((pos) ? &GetAt(pos) : NULL);
}

int CPlateList::GetPlateIndex(CPlate* pPlate)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (&GetNext(pos) == pPlate)
			return nIndex;
		nIndex++;
	}
	return -1;
}

COleDateTime CPlateList::GetLastOutputTime()
{
	COleDateTime lastOutput;
	POSITION pos = GetHeadPosition();
	if ( ! pos)
		return m_defaultPlate.m_OutputDate;
	while (pos)
	{
		CPlate& rPlate = GetNext(pos);
		lastOutput = max(lastOutput, rPlate.m_OutputDate);
	}

	return lastOutput; 
}

BOOL CPlateList::PlateExists(const CString& strPlateName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPlate& rPlate = GetNext(pos);
		if (rPlate.m_strPlateName == strPlateName)
			return TRUE;
	}
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CPlate

IMPLEMENT_SERIAL(CPlate, CObject, VERSIONABLE_SCHEMA | 4)

CPlate::CPlate()
{
	m_nColorDefinitionIndex		= -1;
	m_bInvalid					= TRUE;
	m_nAutoBleedMethod			= AutoBleedNegativ;
	m_fOverbleedOutside			= 0.0f;	 
	m_fOverbleedInside			= 0.0f;	  
	m_fOverbleedOverlap			= 0.0f;	 
	m_fOverbleedControlMarks	= 0.0f;
	m_nExposingDeviceIndex		= -1; 
//	m_OutputDate				= time_t(0); // error: time_t(0) generates 01.01.70
	m_ExposureSequence.m_pPlate = this;
}

CPrintSheet* CPlate::GetPrintSheet() 
{
	if (this)
		return m_pPlateList->GetPrintSheet(); 
	else
		return NULL;
}

int	CPlate::GetSheetSide()  
{
	return m_pPlateList->GetSheetSide();
}

CLayoutSide* CPlate::GetLayoutSide()
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return NULL;

	switch (GetSheetSide())
	{
	case FRONTSIDE:	return &(pLayout->m_FrontSide);
	case BACKSIDE:	return &(pLayout->m_BackSide);
	default:		return &(pLayout->m_FrontSide);
	}
}

void CPlate::Create(CPlate* pDefaultPlate, CExposure& rExposure, short nColorDefinitionIndex, CPrintSheet* pPrintSheet)
{
	CProductPart* pProductPart = (pPrintSheet) ? pPrintSheet->GetProductPart() : NULL;
	float fDefaultBleed = 0.0f;
	if (pProductPart)
		if (pProductPart->m_nType == CProductPart::Bound)
			fDefaultBleed = pProductPart->m_prodDataBound.m_fBleed;

	if ( (nColorDefinitionIndex >= 0) && (nColorDefinitionIndex < CImpManDoc::GetDoc()->m_ColorDefinitionTable.GetSize()) )
		m_strPlateName		 = CImpManDoc::GetDoc()->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName;
	else
		m_strPlateName		 = _T("");
	m_nColorDefinitionIndex	 = nColorDefinitionIndex;
//	m_OutputDate			 = time_t(0); // error: time_t(0) generates 01.01.70
	m_nAutoBleedMethod		 = AutoBleedNegativ;
	m_fOverbleedOutside		 = fDefaultBleed;
	m_fOverbleedInside		 = 0.0f;	  
	m_fOverbleedOverlap		 = 0.0f;	 
	m_fOverbleedControlMarks = 0.0f;

	if (pDefaultPlate)
	{
		m_nAutoBleedMethod		 = pDefaultPlate->m_nAutoBleedMethod;
		m_fOverbleedOutside		 = pDefaultPlate->m_fOverbleedOutside;	 
		m_fOverbleedInside		 = pDefaultPlate->m_fOverbleedInside;	  
		m_fOverbleedOverlap		 = pDefaultPlate->m_fOverbleedOverlap;	 
		m_fOverbleedControlMarks = pDefaultPlate->m_fOverbleedControlMarks;
	}

	m_nExposingDeviceIndex = -1; // TODO: later: GetActExposingDevice()

	m_ExposureSequence.AddTail(rExposure);
}


// operator overload for CPrintSheet - needed by CList functions:

const CPlate& CPlate::operator=(const CPlate& rPlate)
{
	Copy(rPlate);

	return *this;
}


void CPlate::Copy(const CPlate& rPlate)
{
	m_strPlateName			 = rPlate.m_strPlateName;
	m_nColorDefinitionIndex	 = rPlate.m_nColorDefinitionIndex;
	m_OutputDate			 = rPlate.m_OutputDate;
	m_nAutoBleedMethod		 = rPlate.m_nAutoBleedMethod;
	m_fOverbleedOutside		 = rPlate.m_fOverbleedOutside;	 
	m_fOverbleedInside		 = rPlate.m_fOverbleedInside;	  
	m_fOverbleedOverlap		 = rPlate.m_fOverbleedOverlap;	 
	m_fOverbleedControlMarks = rPlate.m_fOverbleedControlMarks;
	m_nExposingDeviceIndex	 = rPlate.m_nExposingDeviceIndex;
	m_ExposureSequence.RemoveAll();  // no append, so target list has to be empty

	int		 nExposures = rPlate.m_ExposureSequence.GetCount(); 
	POSITION pos		= rPlate.m_ExposureSequence.GetHeadPosition();
	CExposure  ExposureBuf;
	CExposure& rExposureBuf = ExposureBuf;

	for (int i = 0; i < nExposures; i++)						
	{
		rExposureBuf = rPlate.m_ExposureSequence.GetAt(pos);
		m_ExposureSequence.AddTail(rExposureBuf);
		rPlate.m_ExposureSequence.GetNext(pos);
	}	

	for (int i = 0; i < m_processColors.GetSize(); i++)
		m_processColors[i].Empty();
	m_processColors.RemoveAll();
	m_processColors.Append(rPlate.m_processColors);

	for (int i = 0; i < m_spotColors.GetSize(); i++)
		m_spotColors[i].Empty();
	m_spotColors.RemoveAll();
	m_spotColors.Append(rPlate.m_spotColors);
}

// copy constructor for CPlate - needed by CList functions:

CPlate::CPlate(const CPlate& rPlate) 
{
	m_bInvalid = TRUE;
	m_ExposureSequence.m_pPlate = this;

	Copy(rPlate);
}


// helper function for PlateList elements
void AFXAPI SerializeElements (CArchive& ar, CPlate* pPlate, int nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CPlate));

	if (ar.IsStoring())
	{
		ar << pPlate->m_strPlateName;
		ar << pPlate->m_nColorDefinitionIndex;
		ar << pPlate->m_OutputDate;
		ar << pPlate->m_nAutoBleedMethod;
		ar << pPlate->m_fOverbleedOutside;
		ar << pPlate->m_fOverbleedInside;	
		ar << pPlate->m_fOverbleedOverlap;	   
		ar << pPlate->m_fOverbleedControlMarks;
		ar << pPlate->m_nExposingDeviceIndex;	
		pPlate->m_ExposureSequence.Serialize(ar);
		pPlate->m_processColors.Serialize(ar);
		pPlate->m_spotColors.Serialize(ar);
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		pPlate->m_ExposureSequence.RemoveAll();  // no append, so target list has to be empty 
		pPlate->m_spotColors.RemoveAll();
		switch (nVersion)
		{
		case 1:
			ar >> pPlate->m_strPlateName;
			ar >> pPlate->m_nColorDefinitionIndex;
			ar >> pPlate->m_nAutoBleedMethod;
			ar >> pPlate->m_fOverbleedOutside;
			ar >> pPlate->m_fOverbleedInside;	
			ar >> pPlate->m_fOverbleedOverlap;	   
			ar >> pPlate->m_fOverbleedControlMarks;
			ar >> pPlate->m_nExposingDeviceIndex;
			pPlate->m_ExposureSequence.Serialize(ar);
			break;
		case 2:
			ar >> pPlate->m_strPlateName;
			ar >> pPlate->m_nColorDefinitionIndex;
			ar >> pPlate->m_OutputDate;				// new in version 2
			ar >> pPlate->m_nAutoBleedMethod;
			ar >> pPlate->m_fOverbleedOutside;
			ar >> pPlate->m_fOverbleedInside;	
			ar >> pPlate->m_fOverbleedOverlap;	   
			ar >> pPlate->m_fOverbleedControlMarks;
			ar >> pPlate->m_nExposingDeviceIndex;
			pPlate->m_ExposureSequence.Serialize(ar);
			break;
		case 3:
			ar >> pPlate->m_strPlateName;
			ar >> pPlate->m_nColorDefinitionIndex;
			ar >> pPlate->m_OutputDate;				
			ar >> pPlate->m_nAutoBleedMethod;
			ar >> pPlate->m_fOverbleedOutside;
			ar >> pPlate->m_fOverbleedInside;	
			ar >> pPlate->m_fOverbleedOverlap;	   
			ar >> pPlate->m_fOverbleedControlMarks;
			ar >> pPlate->m_nExposingDeviceIndex;
			pPlate->m_ExposureSequence.Serialize(ar);
			pPlate->m_spotColors.Serialize(ar);
			break;
		case 4:
			ar >> pPlate->m_strPlateName;
			ar >> pPlate->m_nColorDefinitionIndex;
			ar >> pPlate->m_OutputDate;			
			ar >> pPlate->m_nAutoBleedMethod;
			ar >> pPlate->m_fOverbleedOutside;
			ar >> pPlate->m_fOverbleedInside;	
			ar >> pPlate->m_fOverbleedOverlap;	   
			ar >> pPlate->m_fOverbleedControlMarks;
			ar >> pPlate->m_nExposingDeviceIndex;
			pPlate->m_ExposureSequence.Serialize(ar);
			pPlate->m_processColors.Serialize(ar);
			pPlate->m_spotColors.Serialize(ar);
			break;
		}
	}
}


void AFXAPI DestructElements(CPlate* pPlate, int nCount)
{
	ASSERT(nCount == 1); // in CList context nCount has to be 1

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pPlate->m_strPlateName.Empty();
	pPlate->m_ExposureSequence.RemoveAll();

	for (int i = 0; i < pPlate->m_processColors.GetSize(); i++)
		pPlate->m_processColors[i].Empty();
	pPlate->m_processColors.RemoveAll();

	for (int i = 0; i < pPlate->m_spotColors.GetSize(); i++)
		pPlate->m_spotColors[i].Empty();
	pPlate->m_spotColors.RemoveAll();
}

// Update plate (usually default plate) regarding exposures to be added.
// This is neccessary only after objects has been added to layout side, so we do not use ReorganizePrintSheetPlates() in that case.
// Removal of objects is handled by RemoveCorrespondingExposures() in CLayoutObjectList::RemoveAt().
void CPlate::UpdateExposures()
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return;
	int nSide = GetSheetSide();
	if ( (nSide != FRONTSIDE) && (nSide != BACKSIDE) )
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;
	CLayoutSide* pLayoutSide = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;

	if ( ! m_ExposureSequence.IsInvalid())
		return;

	m_ExposureSequence.Validate();

	// look for exposures to add/remove
	int		 nLayoutObjectIndex = 0;
	POSITION pos				= pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject			= pLayoutSide->m_ObjectList.GetNext(pos);
		CExposure*	   pExposure		= m_ExposureSequence.GetExposure(nLayoutObjectIndex);
		BOOL		   bIsRightPanorama = (rObject.IsPanoramaPage(*pPrintSheet) && rObject.IsMultipagePart(*pPrintSheet, RIGHT)) ? TRUE : FALSE;
		if ( ! pExposure && ! bIsRightPanorama)
		{
			CExposure exposure;
			exposure.Create(NULL, nLayoutObjectIndex);	// NULL = no defaults
			if ( ! m_ExposureSequence.GetCount())
				Create(NULL, exposure, -1, pPrintSheet);
			else
				m_ExposureSequence.AddTail(exposure);
		}
		else
			if (bIsRightPanorama)	// remove exposure of right panorama page (this is by definition)
			{
				POSITION seqPos = m_ExposureSequence.GetHeadPosition();
				while (seqPos)
				{
					CExposure& rExposure = m_ExposureSequence.GetAt(seqPos);
					if (pExposure == &rExposure)
					{
						m_ExposureSequence.RemoveAt(seqPos);
						break;
					}
					m_ExposureSequence.GetNext(seqPos);
				}
			}

		nLayoutObjectIndex++;
	}
}

CExposure* CPlate::GetDefaultExposure(int nLayoutObjectIndex)
{
	if (m_pPlateList)
	{
		m_pPlateList->m_defaultPlate.UpdateExposures();
		return m_pPlateList->m_defaultPlate.m_ExposureSequence.GetExposure(nLayoutObjectIndex);
	}
	else
		return NULL;
}

void CPlate::CalcAutoBleeds()
{
	POSITION pos = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);

		if (!rExposure.IsBleedLocked(TOP))	  rExposure.CalcAutoBleed(TOP,	  m_fOverbleedOutside);
		if (!rExposure.IsBleedLocked(BOTTOM)) rExposure.CalcAutoBleed(BOTTOM, m_fOverbleedOutside);
		if (!rExposure.IsBleedLocked(LEFT))	  rExposure.CalcAutoBleed(LEFT,	  m_fOverbleedOutside);
		if (!rExposure.IsBleedLocked(RIGHT))  rExposure.CalcAutoBleed(RIGHT,  m_fOverbleedOutside);
	}
}


void CPlate::SetBleedValue(int nSide, float fValue, CLayoutObject* pLayoutObject, BOOL bPanoramaPageClicked, CPrintSheetView* pView)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return;

	BOOL	 bIsPanoramaPage = pLayoutObject->IsPanoramaPage(*pPrintSheet);
	POSITION pos			 = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		
		if (rExposure.GetLayoutObject() == pLayoutObject)
		{
			if (bPanoramaPageClicked)
			{
				if (bIsPanoramaPage)
					rExposure.SetBleedValue(nSide, fValue, TRUE, pView);
			}
			else
			{
				if ( ! bIsPanoramaPage)
					rExposure.SetBleedValue(nSide, fValue, TRUE, pView);
			}
			//rExposure.SetBleedValuePanorama(nSide, fValue, TRUE, pView);
			break;
		}
	}

	// same modification on default plate
	if ( ! m_strPlateName.IsEmpty())	// if not already default
	{
		pos = m_pPlateList->m_defaultPlate.m_ExposureSequence.GetHeadPosition();
		while (pos)
		{
			CExposure& rExposure = m_pPlateList->m_defaultPlate.m_ExposureSequence.GetNext(pos);
			
			if (rExposure.GetLayoutObject() == pLayoutObject)
			{
				if (bPanoramaPageClicked)
				{
					if (bIsPanoramaPage)
						rExposure.SetBleedValue(nSide, fValue, TRUE, pView);
				}
				else
				{
					if ( ! bIsPanoramaPage)
						rExposure.SetBleedValue(nSide, fValue, TRUE, pView);
				}
				//rExposure.SetBleedValuePanorama(nSide, fValue, TRUE, pView);
				break;
			}
		}
	}
}

void CPlate::UnlockLocalMask(CLayoutObject* pLayoutObject, BOOL bPanoramaPageClicked, CPrintSheetView* pView)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return;

	BOOL	 bIsPanoramaPage = pLayoutObject->IsPanoramaPage(*pPrintSheet);
	BOOL	 bUnlocked		 = FALSE;
	POSITION pos			 = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		
		if (rExposure.GetLayoutObject() == pLayoutObject)
		{
			if (bPanoramaPageClicked)
			{
				if (bIsPanoramaPage)
				{
					rExposure.UnlockBleed(LEFT,   pView);
					rExposure.UnlockBleed(RIGHT,  pView);
					rExposure.UnlockBleed(TOP,	  pView);
					rExposure.UnlockBleed(BOTTOM, pView);
					bUnlocked = TRUE;
				}
			}
			else
			{
				if ( ! bIsPanoramaPage)
				{
					rExposure.UnlockBleed(LEFT,   pView);
					rExposure.UnlockBleed(RIGHT,  pView);
					rExposure.UnlockBleed(TOP,	  pView);
					rExposure.UnlockBleed(BOTTOM, pView);
					bUnlocked = TRUE;
				}
			}
			break;
		}
	}
	if (bUnlocked)
		Invalidate();
}

CString CPlate::GetGlobalMaskValueString()
{
	CString strMeasure;
	SetMeasure(0, m_fOverbleedOutside, strMeasure);
	return strMeasure;
}

void CPlate::SetTopmostPlate()
{
	int		 nPlateIndex = 0;
	POSITION pos		 = m_pPlateList->GetHeadPosition();
	while (pos)
	{
		CPlate& rPlate = m_pPlateList->GetNext(pos);
		if (&rPlate == this)
			break;
		nPlateIndex++;
	}
	m_pPlateList->m_nTopmostPlate = nPlateIndex;
}


void CPlate::RemoveExposure(int nLayoutObjectIndex)
{
	POSITION pos = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		POSITION   curPos	 = pos;
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		if (rExposure.m_nLayoutObjectIndex == nLayoutObjectIndex)
			m_ExposureSequence.RemoveAt(curPos);
		else
			if (rExposure.m_nLayoutObjectIndex > nLayoutObjectIndex)
				rExposure.m_nLayoutObjectIndex--;
	}
}

void CPlate::TurnLocalMaskAllExposures()
{
	POSITION pos = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		
		float fLeftValue   = rExposure.GetBleedValue(LEFT);
		BOOL  bLeftLocked  = rExposure.IsBleedLocked(LEFT);
		float fRightValue  = rExposure.GetBleedValue(RIGHT);
		BOOL  bRightLocked = rExposure.IsBleedLocked(RIGHT);
		if (bRightLocked)
			rExposure.SetBleedValue(LEFT, fRightValue, TRUE);
		else
			rExposure.UnlockBleed(LEFT);

		if (bLeftLocked)
			rExposure.SetBleedValue(RIGHT, fLeftValue, TRUE);
		else
			rExposure.UnlockBleed(RIGHT);
	}
}

void CPlate::TumbleLocalMaskAllExposures()
{
	POSITION pos = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		
		float fTopValue		= rExposure.GetBleedValue(TOP);
		BOOL  bTopLocked	= rExposure.IsBleedLocked(TOP);
		float fBottomValue  = rExposure.GetBleedValue(BOTTOM);
		BOOL  bBottomLocked = rExposure.IsBleedLocked(BOTTOM);
		if (bBottomLocked)
			rExposure.SetBleedValue(TOP, fBottomValue, TRUE);
		else
			rExposure.UnlockBleed(TOP);

		if (bTopLocked)
			rExposure.SetBleedValue(BOTTOM, fTopValue, TRUE);
		else
			rExposure.UnlockBleed(BOTTOM);
	}
}


float CPlate::GetObjectTrimBox(int nSide, CLayoutObject& rLayoutObject)
{
	float		 fLeft = 0.0f, fRight = 0.0f, fBottom = 0.0f, fTop = 0.0f;
	CPrintSheet* pPrintSheet = GetPrintSheet();

	if (rLayoutObject.IsPanoramaPage(*pPrintSheet))	// panorama page
	{
		if (rLayoutObject.IsMultipagePart(*pPrintSheet, RIGHT))	// only one half needs to be drawn - per definition we take the left one
			return 0.0f;

		CPageTemplate* pObjectTemplate = (rLayoutObject.m_nType == CLayoutObject::ControlMark) ?
										  rLayoutObject.GetCurrentMarkTemplate() :
										  rLayoutObject.GetCurrentPageTemplate(GetPrintSheet());

		CLayoutObject* pBuddy = rLayoutObject.GetCurrentMultipagePartner(pObjectTemplate);

		if ((nSide == LEFT) || (nSide == RIGHT))
		{
			fLeft	= min(rLayoutObject.GetLeft(),   pBuddy->GetLeft());
			fRight	= max(rLayoutObject.GetRight(),  pBuddy->GetRight());
		}
		else
		{
			fBottom	= min(rLayoutObject.GetBottom(), pBuddy->GetBottom());
			fTop	= max(rLayoutObject.GetTop(),    pBuddy->GetTop());
		}
	}
	else
	{
		if ((nSide == LEFT) || (nSide == RIGHT))
		{
			fLeft	= rLayoutObject.GetLeft();
			fRight	= rLayoutObject.GetRight();
		}
		else
		{
			fBottom = rLayoutObject.GetBottom();
			fTop	= rLayoutObject.GetTop();
		}
	}

	switch (nSide)
	{
	case LEFT:		return fLeft   + rLayoutObject.GetLayoutSide()->GetFanoutOffsetX(m_strPlateName, fLeft + (fRight - fLeft) / 2.0f);

	case RIGHT:		return fRight  + rLayoutObject.GetLayoutSide()->GetFanoutOffsetX(m_strPlateName, fLeft + (fRight - fLeft) / 2.0f);

	case BOTTOM:	return fBottom + rLayoutObject.GetLayoutSide()->GetFanoutOffsetY(m_strPlateName, fBottom + (fTop - fBottom) / 2.0f);

	case TOP:		return fTop	   + rLayoutObject.GetLayoutSide()->GetFanoutOffsetY(m_strPlateName, fBottom + (fTop - fBottom) / 2.0f);

	default:		return 0.0f;
	}
}

int CPlate::GetNumOutputObjects(int nObjectType)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	int			 nNumPages	 = 0;
	POSITION	 pos		 = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure&	   rExposure = m_ExposureSequence.GetNext(pos);
		CLayoutObject* pObject	 = rExposure.GetLayoutObject(pPrintSheet->m_nLayoutIndex, GetSheetSide());
		if (!pObject)
			continue;

		if (nObjectType != -1)
			if (nObjectType != pObject->m_nType)
				continue;

		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPrintSheet);
		if ( ! pObjectTemplate)
			continue;

		int nColorDefinitionIndex = m_nColorDefinitionIndex;
		if ( (m_strPlateName == _T("")) && (nColorDefinitionIndex == -1) )	// if default plate 
			if (pObject->m_nType == CLayoutObject::ControlMark)		// if control mark, take 1st sep of mark
				nColorDefinitionIndex = (pObjectTemplate->m_ColorSeparations.GetSize()) ? pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1;
			else
			{
				nNumPages++;
				continue;
			}

		CPageSource* pPageSource = pObjectTemplate->GetObjectSource(nColorDefinitionIndex, TRUE, pPrintSheet, GetSheetSide());
		if (pPageSource)
			if (pPageSource->m_nType != CPageSource::SystemCreated)
				nNumPages++;
	}		
	return nNumPages;
}

// if not already in list, insert indicated process colors
void CPlate::InsertProcessColors(int nColor1, int nColor2, int nColor3, int nColor4)
{
	int processColors[4] = {nColor1, nColor2, nColor3, nColor4};
	for (int i = 0; (i < 4) && (processColors[i] != -1); i++)
	{
		CString strProcessColor;
		switch (processColors[i])
		{
		case CColorDefinition::ProcessC:	strProcessColor = theApp.m_colorDefTable.GetCyanName();		break;
		case CColorDefinition::ProcessM:	strProcessColor = theApp.m_colorDefTable.GetMagentaName();	break;
		case CColorDefinition::ProcessY:	strProcessColor = theApp.m_colorDefTable.GetYellowName();	break;
		case CColorDefinition::ProcessK:	strProcessColor = theApp.m_colorDefTable.GetKeyName();		break;
		}

		BOOL bOK = FALSE;
		for (int j = 0; j < m_processColors.GetSize(); j++)
		{
			if (strProcessColor == m_processColors[j])	// color already in list
			{
				bOK = TRUE;
				break;
			}
		}
		if ( ! bOK)
			m_processColors.Add(strProcessColor);
	}
}

// if not already in list, insert spot colors in alphabetical order
void CPlate::InsertSpotColors(CStringArray& rSpotColors)
{
	for (int i = 0; i < rSpotColors.GetSize(); i++)
	{
		if ( ! m_spotColors.GetSize())
			m_spotColors.Add(rSpotColors[i]);
		else
		{
			BOOL bOK = FALSE;
			for (int j = 0; j < m_spotColors.GetSize(); j++)
			{
				if (m_spotColors[j] > rSpotColors[i])
				{
					m_spotColors.InsertAt(j, rSpotColors[i]);
					bOK = TRUE;
					break;
				}
				else
					if (rSpotColors[i] == m_spotColors[j])	// color already in list
					{
						bOK = TRUE;
						break;
					}
			}
			if ( ! bOK)
				m_spotColors.Add(rSpotColors[i]);
		}
	}
}

BOOL CPlate::ContainsSpotColor(const CString& strColorName)
{
	if (m_strPlateName.IsEmpty())	// by definition default plate contains all colors 
		return TRUE;

	for (int i = 0; i < m_spotColors.GetSize(); i++)
	{
		if (m_spotColors[i] == strColorName)
			return TRUE;
	}
	return FALSE;
}

#ifdef PDF
void CPlate::CheckSourceLinks(CArray <CPageSource*, CPageSource*>& missingLinks)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return;

	int		 nActLayoutSide = GetSheetSide();
	POSITION pos			= m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure&	   rExposure = m_ExposureSequence.GetNext(pos);
		CLayoutObject* pObject	 = rExposure.GetLayoutObject(pPrintSheet->m_nLayoutIndex, nActLayoutSide);
		if (!pObject)
			continue;

		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPrintSheet);
		if (pObjectTemplate)
		{
			CPageSource* pPageSource = pObjectTemplate->GetObjectSource(m_nColorDefinitionIndex, TRUE, pPrintSheet, GetSheetSide());

			if (pPageSource)
			{
				if (pPageSource->m_nType != CPageSource::PDFFile)
					continue;

				if ( ! pPageSource->DocumentExists(pPrintSheet))
				{
					BOOL bAlreadyInList = FALSE;
					for (int i = 0; i < missingLinks.GetSize(); i++)
						if (missingLinks[i] == pPageSource)
						{
							bAlreadyInList = TRUE;
							break;
						}
					if ( ! bAlreadyInList)
						missingLinks.Add(pPageSource);
				}
			}
		}
	}
} 
#endif

void CPlate::Draw(CDC* pDC, CPressDevice* pPressDevice, BOOL bShowMeasures)
{
	if ( ! pPressDevice)
		return;

	CRect plateRect;
	plateRect.left	 = CPrintSheetView::WorldToLP(0);
	plateRect.top	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateHeight);
	plateRect.right	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth);
	plateRect.bottom = CPrintSheetView::WorldToLP(0);

	// Draw the plate rectangle
	if (pDC->RectVisible(plateRect))
	{
		CPen   whitePen, grayPen, blackPen, *pOldPen;
		CBrush plateBrush, bkgrdBrush, *pOldBrush;

		pOldPen	  = pDC->GetCurrentPen();
		pOldBrush = pDC->GetCurrentBrush();

		whitePen.CreatePen(PS_SOLID, 0, WHITE);
		grayPen.CreatePen (PS_SOLID, 0, LIGHTGRAY);
		blackPen.CreatePen(PS_SOLID, 0, BLACK);
		plateBrush.CreateSolidBrush(PLATE_COLOR); 
		bkgrdBrush.CreateSolidBrush(MIDDLEGRAY);

		pDC->FillRect(plateRect, &plateBrush);

		pDC->SelectObject(&grayPen);
		//pDC->SelectObject(&whitePen);
		pDC->MoveTo(plateRect.left,  plateRect.bottom);
		pDC->LineTo(plateRect.left,  plateRect.top);
		pDC->LineTo(plateRect.right, plateRect.top);

		//pDC->SelectObject(&blackPen);
		pDC->LineTo(plateRect.right, plateRect.bottom);
		pDC->LineTo(plateRect.left,  plateRect.bottom);

		// Draw two holes on the bottom of the plate
		pDC->SelectObject(&bkgrdBrush);
		CRect holeRect;
		holeRect.left	= CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth / 4);
		holeRect.top	= CPrintSheetView::WorldToLP(15);
		holeRect.right	= holeRect.left + CPrintSheetView::WorldToLP(25);
		holeRect.bottom	= CPrintSheetView::WorldToLP(5);
		pDC->RoundRect(&holeRect, CPoint(CPrintSheetView::WorldToLP(10), CPrintSheetView::WorldToLP(10)) );
		holeRect.left	= (holeRect.left*3) - CPrintSheetView::WorldToLP(25);
		holeRect.top	= CPrintSheetView::WorldToLP(15);
		holeRect.right	= holeRect.left + CPrintSheetView::WorldToLP(25);
		holeRect.bottom	= CPrintSheetView::WorldToLP(5);
		pDC->RoundRect(&holeRect, CPoint(CPrintSheetView::WorldToLP(10), CPrintSheetView::WorldToLP(10)) );

		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);

		whitePen.DeleteObject(); 
		grayPen.DeleteObject();
		blackPen.DeleteObject();
		plateBrush.DeleteObject();
		bkgrdBrush.DeleteObject();

		if (bShowMeasures)
		{
			CSize szFont(11,11);
			pDC->DPtoLP(&szFont);
			CString strOut; strOut.Format("%.1f x %.1f", pPressDevice->m_fPlateWidth, pPressDevice->m_fPlateHeight);
			CRect rcText(plateRect.left + (int)(0.2f * (float)szFont.cx), plateRect.top - (int)(0.2f * (float)szFont.cy), plateRect.right, plateRect.top - 2 * szFont.cy);
			CFont font;
			font.CreateFont (szFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
			CFont* pOldFont		= pDC->SelectObject(&font);
			int nOldBkMode		= pDC->SetBkMode(TRANSPARENT);
			COLORREF crOldColor = pDC->SetTextColor(DARKGRAY);
			pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
			pDC->SelectObject(pOldFont);
			font.DeleteObject();
			pDC->SetBkMode(nOldBkMode);
			pDC->SetTextColor(crOldColor);
		}
	}
}

void CPlate::DrawMeasuring(CDC* pDC, CPressDevice* pPressDevice)
{
	if ( ! pPressDevice)
		return;

	CRect plateRect;
	plateRect.left	 = CPrintSheetView::WorldToLP(0);
	plateRect.top	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateHeight);
	plateRect.right	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth);
	plateRect.bottom = CPrintSheetView::WorldToLP(0);
	CRect sheetRect;
	sheetRect.left   = CPrintSheetView::WorldToLP(0);
	sheetRect.top	 = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateHeight);
	sheetRect.right  = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth);
	sheetRect.bottom = CPrintSheetView::WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge - pPressDevice->m_fGripper);

	CSize sizeDistance(10, 10);
	pDC->DPtoLP(&sizeDistance);

	// plate width
	CPoint pt[2]  = {CPoint(plateRect.left, plateRect.bottom - 2 * sizeDistance.cy), CPoint(plateRect.right, plateRect.bottom - 2 * sizeDistance.cy)};
	CPoint ptRef1 = CPoint(plateRect.left,  plateRect.bottom);
	CPoint ptRef2 = CPoint(plateRect.right, plateRect.bottom);
	CString strOut, strMeasure;
	SetMeasure(0, pPressDevice->m_fPlateWidth, strMeasure);
	strOut.Format(IDS_PLATEWIDTH, strMeasure);
	DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

	// plate height
	pt[0]  = CPoint(plateRect.right + 2 * sizeDistance.cx, plateRect.top); pt[1] = CPoint(plateRect.right + 2 * sizeDistance.cx, plateRect.bottom);
	ptRef1 = CPoint(plateRect.right, plateRect.top);
	ptRef2 = CPoint(plateRect.right, plateRect.bottom); 
	SetMeasure(0, pPressDevice->m_fPlateHeight, strMeasure);
	strOut.Format(IDS_PLATEHEIGHT, strMeasure);
	DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

	if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
	{
		// plate center X
		pt[0]  = CPoint(plateRect.left, plateRect.top + sizeDistance.cy); pt[1] = CPoint(plateRect.left + (plateRect.right - plateRect.left)/2, plateRect.top + sizeDistance.cy);
		ptRef1 = CPoint(plateRect.left, plateRect.top);
		ptRef2 = CPoint(plateRect.left + (plateRect.right - plateRect.left)/2, plateRect.bottom - sizeDistance.cy/4);
		SetMeasure(0, pPressDevice->m_fPlateWidth/2.0f, strMeasure);
		strOut.Format(IDS_PLATECENTERX, strMeasure);
		DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

		// gripper
		int nXPos = CPrintSheetView::WorldToLP(0.0f) - sizeDistance.cx;
		pt[0]  = CPoint(nXPos, sheetRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fGripper)); pt[1] = CPoint(nXPos, sheetRect.bottom);
		ptRef1 = CPoint(sheetRect.right + sizeDistance.cx/4, sheetRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fGripper));
		ptRef2 = CPoint(sheetRect.right + sizeDistance.cx/4, sheetRect.bottom);
		SetMeasure(0, pPressDevice->m_fGripper, strMeasure);
		strOut.Format(IDS_GRIPPER, strMeasure);
		DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

		// begin of printing area in Y
		nXPos = CPrintSheetView::WorldToLP(0.0f) - 3 * sizeDistance.cx;
		pt[0]  = CPoint(nXPos,								 CPrintSheetView::WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge)); 
		pt[1]  = CPoint(nXPos,								 CPrintSheetView::WorldToLP(0.0f));
		ptRef1 = CPoint(sheetRect.right + sizeDistance.cx/4, CPrintSheetView::WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge));
		ptRef2 = CPoint(CPrintSheetView::WorldToLP(0.0f),	 CPrintSheetView::WorldToLP(0.0f));
		SetMeasure(0, pPressDevice->m_fPlateEdgeToPrintEdge, strMeasure);
		strOut.Format(IDS_PRINTBEGIN, strMeasure);
		DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
	}
	else
	{
		// web refline X
		pt[0]  = CPoint(plateRect.left, plateRect.top + sizeDistance.cy); pt[1] = CPoint(plateRect.left + CPrintSheetView::WorldToLP(pPressDevice->m_fXRefLine), plateRect.top + sizeDistance.cy);
		ptRef1 = CPoint(plateRect.left, plateRect.top);
		ptRef2 = CPoint(plateRect.left + CPrintSheetView::WorldToLP(pPressDevice->m_fXRefLine), plateRect.bottom - sizeDistance.cy/4);
		SetMeasure(0, pPressDevice->m_fXRefLine, strMeasure);
		strOut.Format(IDS_WEBREFLINEX, strMeasure);
		DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

		// web section center Y
		pt[0]  = CPoint(plateRect.left - sizeDistance.cx, plateRect.bottom); pt[1] = CPoint(plateRect.left - sizeDistance.cx, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fCenterOfSection));
		ptRef1 = CPoint(plateRect.left, plateRect.bottom);
		ptRef2 = CPoint(plateRect.right + sizeDistance.cx/4, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fCenterOfSection));
		SetMeasure(0, pPressDevice->m_fCenterOfSection, strMeasure);
		strOut.Format(IDS_WEBCENTERY, strMeasure);
		DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
	}
}

void CPlate::DrawHMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1, CPoint ptRef2, BOOL bChain)
{
	CSize sizeFont(12,12);
	pDC->DPtoLP(&sizeFont);
	CFont font, *pOldFont;
	font.CreateFont(sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, "Tahoma");	
	//if (pDC->IsPrinting())
		pOldFont = pDC->SelectObject(&font);
	//else
		//pOldFont = (CFont*)pDC->SelectStockObject(ANSI_VAR_FONT);
	int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor	= pDC->SetBkColor(LIGHTGRAY);
	COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

	CString strOut;
	strOut.Format(" %s ", strMeasure);

	pDC->LPtoDP(pt, 2);

	CSize textExtent = pDC->GetTextExtent(strOut);
	pDC->LPtoDP(&textExtent);

	CPen  linePen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&linePen);
	if ( (ptRef1.x != INT_MAX) && (ptRef1.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef1);
		pDC->LPtoDP(&ptRef1);
		CPoint ptTo(pt[0] + CSize(0, (pt[0].y > ptRef1.y) ? 10 : -10));
		pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	if ( (ptRef2.x != INT_MAX) && (ptRef2.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef2);
		pDC->LPtoDP(&ptRef2);
		CPoint ptTo(pt[1] + CSize(0, (pt[1].y > ptRef1.y) ? 10 : -10));
		pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	pDC->SelectObject(pOldPen);
	linePen.DeleteObject();

	BOOL bFit = TRUE;
	int	 nLen = pt[1].x - pt[0].x;
	if (nLen - 10 < textExtent.cx)	// if measurement arrow too short, flip arrows and use fix length
	{
		bFit = FALSE;
		nLen = 15;
		CPrintSheetView::DrawArrow(pDC, LEFT,   pt[1], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
		CPrintSheetView::DrawArrow(pDC, RIGHT,  pt[0], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
	}
	else
	{
		CPrintSheetView::DrawArrow(pDC, LEFT,   pt[0], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
		CPrintSheetView::DrawArrow(pDC, RIGHT,  pt[1], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
	}

	CPoint ptTextCenter = CPoint(( ! bFit) ? pt[1].x + textExtent.cx/2 + ((bChain) ? nLen/2 : 2*nLen/3) : pt[0].x + (pt[1].x - pt[0].x)/2, 
								 ( ! bFit) ? pt[0].y - ((bChain) ? textExtent.cy/2 : 0) : pt[0].y);

	CRect textRect(ptTextCenter, ptTextCenter);
	textRect.InflateRect(textExtent.cx/2 + 5, textExtent.cy/2);
	pDC->DPtoLP(textRect);
	pDC->DrawText(strOut, textRect, DT_CENTER | DT_VCENTER);

	pDC->SelectObject(pOldFont);
	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetTextColor(crOldTextColor);

	font.DeleteObject();
}

void CPlate::DrawVMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1, CPoint ptRef2, BOOL bChain)
{
	CSize sizeFont(12,12);
	pDC->DPtoLP(&sizeFont);
	CFont font270;
	font270.CreateFont(sizeFont.cy, 0, 2700, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, "Tahoma");	
	CFont*	 pOldFont		= pDC->SelectObject(&font270);
	int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor	= pDC->SetBkColor(LIGHTGRAY);
	COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

	CString strOut;
	strOut.Format(" %s ", strMeasure);

	pDC->LPtoDP(pt, 2);

	CSize textExtent = pDC->GetTextExtent(strOut);
	pDC->LPtoDP(&textExtent);

	CPen  linePen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&linePen);
	if ( (ptRef1.x != INT_MAX) && (ptRef1.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef1);
		pDC->LPtoDP(&ptRef1);
		CPoint ptTo(pt[0] + CSize((pt[0].x > ptRef1.x) ? 10 : -10, 0));
		pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	if ( (ptRef2.x != INT_MAX) && (ptRef2.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef2);
		pDC->LPtoDP(&ptRef2);
		CPoint ptTo(pt[1] + CSize((pt[1].x > ptRef1.x) ? 10 : -10, 0));
		pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	pDC->SelectObject(pOldPen);
	linePen.DeleteObject();

	BOOL bFit = TRUE;
	int	 nLen = pt[1].y - pt[0].y;
	if (nLen - 10 < textExtent.cx)	// if measurement arrow too short, flip arrows and use fix length
	{
		bFit = FALSE;
		nLen = 15;
		CPrintSheetView::DrawArrow(pDC, TOP,    pt[1], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
		CPrintSheetView::DrawArrow(pDC, BOTTOM, pt[0], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
	}
	else
	{
		CPrintSheetView::DrawArrow(pDC, TOP,    pt[0], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
		CPrintSheetView::DrawArrow(pDC, BOTTOM, pt[1], nLen - 5, DARKGRAY, LIGHTGRAY, TRUE);
	}

	CPoint ptTextCenter = CPoint(( ! bFit) ? pt[0].x - ((bChain) ? textExtent.cy/2 : 0) : pt[0].x, 
								 ( ! bFit) ? pt[0].y - textExtent.cx/2 - ((bChain) ? nLen/2 : 2*nLen/3) : pt[0].y + (pt[1].y - pt[0].y)/2);

	CRect textRect(ptTextCenter, ptTextCenter);
	textRect.InflateRect(textExtent.cy/2, textExtent.cx/2);
	pDC->DPtoLP(textRect);
//	pDC->DrawText(strMeasure, textRect, DT_SINGLELINE|DT_BOTTOM|DT_LEFT);
	pDC->TextOut(textRect.left, textRect.bottom, strOut);

	pDC->SelectObject(pOldFont);
	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetTextColor(crOldTextColor);

	font270.DeleteObject();
}

void CPlate::AddSeparation(CString strSep)
{
	BOOL bFound = FALSE;

	if (theApp.m_colorDefTable.IsProcessColor(strSep))
	{
		for (int i = 0; i < m_processColors.GetSize(); i++)
			if (m_processColors[i].CompareNoCase(strSep) == 0)
				bFound = TRUE;

		if ( ! bFound)
			m_processColors.Add(strSep);
	}
	else
	{
		for (int i = 0; i < m_spotColors.GetSize(); i++)
			if (m_spotColors[i].CompareNoCase(strSep) == 0)
				bFound = TRUE;

		if ( ! bFound)
			m_spotColors.Add(strSep);
	}
}

BOOL CPlate::SeparationExists(CString strSep)
{
	for (int i = 0; i < m_processColors.GetSize(); i++)
		if (m_processColors[i].CompareNoCase(strSep) == 0)
			return TRUE;

	for (int i = 0; i < m_spotColors.GetSize(); i++)
		if (m_spotColors[i].CompareNoCase(strSep) == 0)
			return TRUE;
	
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Implementation of CExposureSequence

IMPLEMENT_SERIAL(CExposureSequence, CObject, VERSIONABLE_SCHEMA | 1)

CExposureSequence::CExposureSequence()
{
	m_bInvalid = TRUE;
}

POSITION CExposureSequence::AddTail(CExposure& rExposure)
{
	GetPlate()->Invalidate();
	POSITION pos = CList <CExposure, CExposure&>::AddTail(rExposure);
	CList <CExposure, CExposure&>::GetAt(pos).m_pSequence = this;
	return pos;
}


void CExposureSequence::RemoveAll()
{
	CList <CExposure, CExposure&>::RemoveAll();
	GetPlate()->Invalidate();
}


void CExposureSequence::RemoveAt(POSITION pos)
{
	CList <CExposure, CExposure&>::RemoveAt(pos);
	GetPlate()->Invalidate();
}

BOOL CExposureSequence::HasNoPages()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure   = GetNext(pos);
		CLayoutObject* pObject = rExposure.GetLayoutObject();
		if (pObject)
			if (rExposure.GetLayoutObject()->m_nType == CLayoutObject::Page)
				return FALSE;
	}
	return TRUE;
}


CExposure* CExposureSequence::GetExposure(short nLayoutObjectIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = GetNext(pos);
		if (rExposure.m_nLayoutObjectIndex == nLayoutObjectIndex)
			return &rExposure;
	}
	return NULL;
}





/////////////////////////////////////////////////////////////////////////////
// CExposure

IMPLEMENT_SERIAL(CExposure, CObject, VERSIONABLE_SCHEMA | 1)

CExposure::CExposure()
{
	m_fLeftBleed			= 0.0;
	m_fRightBleed			= 0.0;
	m_fUpperBleed			= 0.0;
	m_fLowerBleed			= 0.0;
	m_nLockedBleeds			= 0;
	m_bEnabled				= TRUE;
	m_nAction				= 0;
	m_nDiffusionPercentage	= 0;
	m_PreVacuumTime			= 0;
	m_ExposingClocks		= 0;
	m_nLayoutObjectIndex	= 0;

	m_pSequence				= NULL;
}

// copy constructor for CExposure - needed by CList functions:
CExposure::CExposure(const CExposure& rExposure) 
{
	m_pSequence	= NULL;

	Copy(rExposure);
}

CPlate*	CExposure::GetPlate() 
{
	if (this)
		return m_pSequence->GetPlate(); 
	else
		return NULL;
}

void CExposure::Create(CPlate* pDefaultPlate, short nLayoutObjectIndex)
{
	m_fLeftBleed			= 0.0f;
	m_fRightBleed			= 0.0f;
	m_fUpperBleed			= 0.0f;
	m_fLowerBleed			= 0.0f;
	m_nLockedBleeds			= 0;
	m_bEnabled				= TRUE;
	m_nAction				= 0;
	m_nDiffusionPercentage	= 0;
	m_PreVacuumTime			= 0;
	m_ExposingClocks		= 0;
	m_nLayoutObjectIndex	= nLayoutObjectIndex;

	if (pDefaultPlate)
	{
		CExposure* pDefaultExposure = pDefaultPlate->m_ExposureSequence.GetExposure(nLayoutObjectIndex);
		if (pDefaultExposure)
			*this = *pDefaultExposure;
	}
}

// operator overload for CExposure - needed by CList functions:
const CExposure& CExposure::operator=(const CExposure& rExposure)
{
	Copy(rExposure);

	return *this;
}


void CExposure::Copy(const CExposure& rExposure)
{
	m_fLeftBleed			= rExposure.m_fLeftBleed;
	m_fRightBleed			= rExposure.m_fRightBleed;
	m_fUpperBleed			= rExposure.m_fUpperBleed;
	m_fLowerBleed			= rExposure.m_fLowerBleed;
	m_nLockedBleeds			= rExposure.m_nLockedBleeds;
	m_bEnabled				= rExposure.m_bEnabled;
	m_nAction				= rExposure.m_nAction;
	m_nDiffusionPercentage	= rExposure.m_nDiffusionPercentage;
	m_PreVacuumTime			= rExposure.m_PreVacuumTime;
	m_ExposingClocks		= rExposure.m_ExposingClocks;
	m_nLayoutObjectIndex	= rExposure.m_nLayoutObjectIndex;
}

CLayoutObject* CExposure::GetLayoutObject(int nLayoutIndex, int nLayoutSide)
{
	if (GetPlate()->GetLayoutSide()->m_nLayoutSide != nLayoutSide)
		return NULL;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	POSITION	 pos  = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nLayoutIndex);
	CLayoutSide* pLayoutSide;
	if (nLayoutSide == FRONTSIDE)
		pLayoutSide = &(pDoc->m_PrintSheetList.m_Layouts.GetAt(pos).m_FrontSide);
	else
		pLayoutSide = &(pDoc->m_PrintSheetList.m_Layouts.GetAt(pos).m_BackSide);

	pos = pLayoutSide->m_ObjectList.FindIndex(m_nLayoutObjectIndex);
	if (pos == NULL)
		return NULL;

	return &(pLayoutSide->m_ObjectList.GetAt(pos));
}

CLayoutObject* CExposure::GetLayoutObject()
{
	CPrintSheet* pPrintSheet = GetPlate()->GetPrintSheet();
	CLayoutSide* pLayoutSide = NULL;
	CLayout*	 pLayout	 = pPrintSheet->GetLayout();
	POSITION	 platePos	 = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
	while (platePos)
	{
		CPlate&  rPlate		 = pPrintSheet->m_FrontSidePlates.GetNext(platePos);
		POSITION exposurePos = rPlate.m_ExposureSequence.GetHeadPosition();
		while (exposurePos && !pLayoutSide)
		{
			CExposure& rExposure = rPlate.m_ExposureSequence.GetNext(exposurePos);
			if (this == &rExposure)
			{
				pLayoutSide = &pLayout->m_FrontSide;
				break;
			}
		}
	}
	if (!pLayoutSide)
	{
		platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
		while (platePos)
		{
			CPlate&  rPlate		 = pPrintSheet->m_BackSidePlates.GetNext(platePos);
			POSITION exposurePos = rPlate.m_ExposureSequence.GetHeadPosition();
			while (exposurePos && !pLayoutSide)
			{
				CExposure& rExposure = rPlate.m_ExposureSequence.GetNext(exposurePos);
				if (this == &rExposure)
				{
					pLayoutSide = &pLayout->m_BackSide;
					break;
				}
			}
		}
	}

	if ( ! pLayoutSide)
	{
		// either on frontside plates nor on backside plates found -> inspect default plates
		POSITION exposurePos = pPrintSheet->m_FrontSidePlates.m_defaultPlate.m_ExposureSequence.GetHeadPosition();
		while (exposurePos && !pLayoutSide)
		{
			CExposure& rExposure = pPrintSheet->m_FrontSidePlates.m_defaultPlate.m_ExposureSequence.GetNext(exposurePos);
			if (this == &rExposure)
			{
				pLayoutSide = &pLayout->m_FrontSide;
				break;
			}
		}
		if ( ! pLayoutSide)
		{
			POSITION exposurePos = pPrintSheet->m_BackSidePlates.m_defaultPlate.m_ExposureSequence.GetHeadPosition();
			while (exposurePos && !pLayoutSide)
			{
				CExposure& rExposure = pPrintSheet->m_BackSidePlates.m_defaultPlate.m_ExposureSequence.GetNext(exposurePos);
				if (this == &rExposure)
				{
					pLayoutSide = &pLayout->m_BackSide;
					break;
				}
			}
		}
	}

	if ( ! pLayoutSide)
		return NULL;

	POSITION pos = pLayoutSide->m_ObjectList.FindIndex(m_nLayoutObjectIndex);
	if (pos == NULL)
		return NULL;

	return &(pLayoutSide->m_ObjectList.GetAt(pos));
}

float CExposure::GetBleedValue(int nSide)
{
	if (GetPlate()->IsInvalid())
	{
		GetPlate()->Validate();	// Validate in order to prevent for recursive calls
		GetPlate()->CalcAutoBleeds();
	}

	switch (nSide)
	{
	case TOP:		return m_fUpperBleed; 
	case BOTTOM:	return m_fLowerBleed; 
	case LEFT:		return m_fLeftBleed;  
	case RIGHT:		return m_fRightBleed; 
	default:		return 0.0f; 
	}
}

float CExposure::GetRealBleedValue(int nSide)
{
	if (GetPlate()->IsInvalid())
	{
		GetPlate()->Validate();	// Validate in order to prevent for recursive calls
		GetPlate()->CalcAutoBleeds();
	}

	int nHeadPosition = GetLayoutObject()->m_nHeadPosition;

	switch (nSide)
	{
	case TOP:
		switch (nHeadPosition)
		{
		case TOP:		return m_fUpperBleed; 
		case BOTTOM:	return m_fLowerBleed; 
		case LEFT:		return m_fLeftBleed;  
		case RIGHT:		return m_fRightBleed; 
		}
	case BOTTOM:
		switch (nHeadPosition)
		{
		case TOP:		return m_fLowerBleed; 
		case BOTTOM:	return m_fUpperBleed; 
		case LEFT:		return m_fRightBleed; 
		case RIGHT:		return m_fLeftBleed;  
		}
	case LEFT:
		switch (nHeadPosition)
		{
		case TOP:		return m_fLeftBleed;  
		case BOTTOM:	return m_fRightBleed; 
		case LEFT:		return m_fLowerBleed; 
		case RIGHT:		return m_fUpperBleed; 
		}
	case RIGHT:
		switch (nHeadPosition)
		{
		case TOP:		return m_fRightBleed; 
		case BOTTOM:	return m_fLeftBleed;  
		case LEFT:		return m_fUpperBleed; 
		case RIGHT:		return m_fLowerBleed; 
		}
	}

	return 0.0f;
}

void CExposure::CalcAutoBleed(int nSide, float fValue)
{
	CLayoutObject* pLayoutObject = GetLayoutObject();
	if (!pLayoutObject)
		return;

	if (pLayoutObject->m_nType == CLayoutObject::ControlMark)
	{
		SetBleedValue(nSide, 0.0f);
		return;
	}

	CPrintSheet* pPrintSheet = GetPlate()->GetPrintSheet();
	if ( ! pPrintSheet)
		return;
	CPageTemplate* pObjectTemplate = (pLayoutObject->m_nType == CLayoutObject::ControlMark) ?
									  pLayoutObject->GetCurrentMarkTemplate() :
									  pLayoutObject->GetCurrentPageTemplate(pPrintSheet);
	if ( ! pObjectTemplate)
		return;

	float fBorder = FLT_MAX;
	if (pLayoutObject->IsPanoramaPage(*pPrintSheet))	// panorama page
	{
		CLayoutObject* pBuddy = pLayoutObject->GetCurrentMultipagePartner(pObjectTemplate);

		ASSERT(pBuddy);

		if (pLayoutObject->PanoramaHasNeighbour(pBuddy, nSide))
		{
			switch (nSide)
			{
			case TOP:		fBorder = max(pLayoutObject->GetTop() + pLayoutObject->m_fUpperBorder, pBuddy->GetTop() + pBuddy->m_fUpperBorder) -
									  max(pLayoutObject->GetTop(), pBuddy->GetTop());
							break;
			case BOTTOM:	fBorder = min(pLayoutObject->GetBottom(), pBuddy->GetBottom()) - 
									  min(pLayoutObject->GetBottom() - pLayoutObject->m_fLowerBorder, pBuddy->GetBottom() - pBuddy->m_fLowerBorder);
							break;
			case LEFT:		fBorder = min(pLayoutObject->GetLeft(),	pBuddy->GetLeft()) - 
									  min(pLayoutObject->GetLeft() - pLayoutObject->m_fLeftBorder, pBuddy->GetLeft() - pBuddy->m_fLeftBorder);
							break;
			case RIGHT:		fBorder = max(pLayoutObject->GetRight() + pLayoutObject->m_fRightBorder, pBuddy->GetRight() + pBuddy->m_fRightBorder) -
									  max(pLayoutObject->GetRight(), pBuddy->GetRight());
							break;
			default:		fBorder = FLT_MAX;
							break;
			}			
			fBorder = min(fValue, fBorder);
		}
		else
			fBorder = fValue;
	}
	else
	{
		switch (nSide)
		{
		case TOP:	fBorder = min(fValue, (pLayoutObject->m_UpperNeighbours.GetSize()) ? pLayoutObject->m_fUpperBorder : FLT_MAX); break;
		case RIGHT:	fBorder = min(fValue, (pLayoutObject->m_RightNeighbours.GetSize()) ? pLayoutObject->m_fRightBorder : FLT_MAX); break;
		case BOTTOM:fBorder = min(fValue, (pLayoutObject->m_LowerNeighbours.GetSize()) ? pLayoutObject->m_fLowerBorder : FLT_MAX); break;
		case LEFT:	fBorder = min(fValue, (pLayoutObject->m_LeftNeighbours.GetSize() ) ? pLayoutObject->m_fLeftBorder  : FLT_MAX); break;
		default:	return;
		}

		if ( (pLayoutObject->IsLabel()) && (fBorder < -0.001f) )	// in case of label job and negative bleed (because of overlappings) disable automatizm (set bleed to global value)
			fBorder = fValue;
	}

	SetBleedValue(nSide, fBorder);
}

void CExposure::SetBleedValue(int nSide, float fValue, BOOL bLock, CPrintSheetView* pView)
{
	float fDiff	= fValue - GetBleedValue(nSide);	

	switch (nSide)
	{
	case TOP:	m_fUpperBleed = fValue; break;
	case RIGHT:	m_fRightBleed = fValue; break;
	case BOTTOM:m_fLowerBleed = fValue; break;
	case LEFT:	m_fLeftBleed  = fValue; break; 
	default:	return;
	}

	if (bLock)
		m_nLockedBleeds|= nSide; 
	else
		m_nLockedBleeds&= ~nSide; 


	if (fDiff == 0.0f)
		return;

	if (pView)
	{
		CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData((void*)this);
		if (pDI)
		{
			CRect newRect = pDI->m_BoundingRect;
			switch (nSide)
			{
			case TOP:	newRect.top   += (long)(CPrintSheetView::WorldToLP(fDiff) / pView->m_DisplayList.m_fViewportRatioY + 2); break;
			case RIGHT:	newRect.right += (long)(CPrintSheetView::WorldToLP(fDiff) / pView->m_DisplayList.m_fViewportRatioX + 2); break;
			case BOTTOM:newRect.bottom-= (long)(CPrintSheetView::WorldToLP(fDiff) / pView->m_DisplayList.m_fViewportRatioY + 2); break;
			case LEFT:	newRect.left  -= (long)(CPrintSheetView::WorldToLP(fDiff) / pView->m_DisplayList.m_fViewportRatioX + 2); break;
			default: return;
			}

			newRect.NormalizeRect();
			CRect unionRect;
			unionRect.UnionRect(newRect, pDI->m_AuxRect);
			unionRect.UnionRect(newRect, pDI->m_BoundingRect);
			unionRect.InflateRect(5,5);
			pView->InvalidateRect(unionRect);
		}
	}
}

void CExposure::LockBleed(int nSide)
{
	m_nLockedBleeds|= nSide; 
}

void CExposure::UnlockBleed(int nSide, CPrintSheetView* pView)
{
	if (pView)
	{
		CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData((void*)this);
		if (pDI)
		{
			CRect rect;
			rect.UnionRect(pDI->m_AuxRect, pDI->m_BoundingRect);
			rect.InflateRect(5,5);
			pView->InvalidateRect(rect);
		}
	}
 
	m_nLockedBleeds&= ~nSide; 
}

BOOL CExposure::IsBleedLocked(int nSide)
{
	return (m_nLockedBleeds & nSide) ? TRUE : FALSE;
}


// Get virtual trim box of page contents in sheet coordinates. In case of
//		- normal single pages it is just the layout object box,
//		- separated double pages it is the virtual box, as if the corresponding buddy is moved beside the layout object,
//		- real panorama pages it is the maximum box of layout object and buddy.
float CExposure::GetContentTrimBox(int nSide, CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	if (!pObject)
		return 0.0f;

	float fLeft			 = pObject->GetLeft();		// normal single page
	float fBottom		 = pObject->GetBottom();
	float fRight		 = pObject->GetRight();	
	float fTop			 = pObject->GetTop(); 
	float fFanoutOffsetX = pObject->GetLayoutSide()->GetFanoutOffsetX(GetPlate()->m_strPlateName, fLeft   + (fRight - fLeft)   / 2.0f);
	float fFanoutOffsetY = pObject->GetLayoutSide()->GetFanoutOffsetY(GetPlate()->m_strPlateName, fBottom + (fTop   - fBottom) / 2.0f);

	if (pTemplate->m_nMultipageGroupNum != -1)	// double (multi) page
	{
		CPrintSheet*   pPrintSheet = GetPlate()->GetPrintSheet();
		CLayoutObject* pBuddy	   = pObject->GetCurrentMultipagePartner(pTemplate);
		
		if (pObject->IsPanoramaPage(*pPrintSheet))	// panorama page
		{
			fLeft		   = min(fLeft,   pBuddy->GetLeft() );
			fBottom		   = min(fBottom, pBuddy->GetBottom() );
			fRight		   = max(fRight,  pBuddy->GetRight() );
			fTop		   = max(fTop,    pBuddy->GetTop() );
			// for panorama page fanout corresponds to content box, because we have only one exposure -> so recalc fanout
			fFanoutOffsetX = pObject->GetLayoutSide()->GetFanoutOffsetX(GetPlate()->m_strPlateName, fLeft   + (fRight - fLeft)   / 2.0f);
			fFanoutOffsetY = pObject->GetLayoutSide()->GetFanoutOffsetY(GetPlate()->m_strPlateName, fBottom + (fTop   - fBottom) / 2.0f);

		}
		else										// separated panorama page
		{
			BOOL bIsLeftPage = pObject->IsMultipagePart(*pPrintSheet, LEFT);
			switch (pObject->m_nHeadPosition)
			{
			case TOP:		fLeft   -= (!bIsLeftPage) ? pBuddy->GetWidth()  : 0.0f; 
							fRight  += ( bIsLeftPage) ? pBuddy->GetWidth()  : 0.0f; break;
			case BOTTOM:	fLeft   -= ( bIsLeftPage) ? pBuddy->GetWidth()  : 0.0f; 
							fRight  += (!bIsLeftPage) ? pBuddy->GetWidth()  : 0.0f; break;
			case LEFT:		fBottom	-= (!bIsLeftPage) ? pBuddy->GetHeight() : 0.0f;
							fTop	+= ( bIsLeftPage) ? pBuddy->GetHeight() : 0.0f; break;
			case RIGHT:		fBottom	-= ( bIsLeftPage) ? pBuddy->GetHeight() : 0.0f;
							fTop	+= (!bIsLeftPage) ? pBuddy->GetHeight() : 0.0f; break;
			}
		}
	}

	switch (nSide)
	{
	case LEFT:		return fLeft   + fFanoutOffsetX;
	case BOTTOM:	return fBottom + fFanoutOffsetY;
	case RIGHT:		return fRight  + fFanoutOffsetX;
	case TOP:		return fTop	   + fFanoutOffsetY;
	default:		return 0.0f;											   
	}
}


float CExposure::GetContentTrimBoxCenterX(CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	float fTrimBoxLeft  = GetContentTrimBox(LEFT,  pObject, pTemplate);
	float fTrimBoxRight = GetContentTrimBox(RIGHT, pObject, pTemplate);

	return fTrimBoxLeft + (fTrimBoxRight - fTrimBoxLeft) / 2.0f;
}

float CExposure::GetContentTrimBoxCenterY(CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	float fTrimBoxBottom = GetContentTrimBox(BOTTOM, pObject, pTemplate);
	float fTrimBoxTop	 = GetContentTrimBox(TOP,	pObject, pTemplate);

	return fTrimBoxBottom + (fTrimBoxTop - fTrimBoxBottom) / 2.0f;
}


// Get sheet layout trim box of page in sheet coordinates. In case of
//		- normal single pages or separated double pages it is just the layout object box,
//		- real panorama pages it is the maximum box of layout object and buddy.
float CExposure::GetSheetTrimBox(int nSide, CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	if (!pObject)
		return 0.0f;

	float		 fLeft, fBottom, fRight, fTop, fFanoutOffsetX, fFanoutOffsetY;
	CPrintSheet* pPrintSheet = GetPlate()->GetPrintSheet();
		
	if (pObject->IsPanoramaPage(*pPrintSheet))	// panorama page
	{
		CLayoutObject* pBuddy = pObject->GetCurrentMultipagePartner(pTemplate);

		fLeft		   = min(pObject->GetLeft(),   pBuddy->GetLeft() );
		fBottom		   = min(pObject->GetBottom(), pBuddy->GetBottom() );
		fRight		   = max(pObject->GetRight(),  pBuddy->GetRight() );
		fTop		   = max(pObject->GetTop(),    pBuddy->GetTop() );
		// for panorama page fanout corresponds to content box, because we have only one exposure 
		fFanoutOffsetX = pObject->GetLayoutSide()->GetFanoutOffsetX(GetPlate()->m_strPlateName, fLeft   + (fRight - fLeft)   / 2.0f);
		fFanoutOffsetY = pObject->GetLayoutSide()->GetFanoutOffsetY(GetPlate()->m_strPlateName, fBottom + (fTop   - fBottom) / 2.0f);

	}
	else	// normal single page or separated double page
	{
		fLeft		   = pObject->GetLeft();		
		fBottom		   = pObject->GetBottom();
		fRight		   = pObject->GetRight();	
		fTop		   = pObject->GetTop(); 
		fFanoutOffsetX = pObject->GetLayoutSide()->GetFanoutOffsetX(GetPlate()->m_strPlateName, fLeft   + (fRight - fLeft)   / 2.0f);
		fFanoutOffsetY = pObject->GetLayoutSide()->GetFanoutOffsetY(GetPlate()->m_strPlateName, fBottom + (fTop   - fBottom) / 2.0f);
	}

	switch (nSide)
	{
	case LEFT:		return fLeft   + fFanoutOffsetX;
	case BOTTOM:	return fBottom + fFanoutOffsetY;
	case RIGHT:		return fRight  + fFanoutOffsetX;
	case TOP:		return fTop	   + fFanoutOffsetY;
	default:		return 0.0f;											   
	}
}


// Get the area which will be exposed 
float CExposure::GetSheetBleedBox(int nSide, CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	if (!pObject)
		return 0.0f;

	CPlate* pPlate = GetPlate();
	if (pPlate->IsInvalid())
	{
		pPlate->Validate();	// Validate in order to prevent for recursive calls
		pPlate->CalcAutoBleeds();
	}

	switch (nSide)
	{
	case LEFT:	 return GetSheetTrimBox(nSide, pObject, pTemplate) - m_fLeftBleed;  
	case BOTTOM: return GetSheetTrimBox(nSide, pObject, pTemplate) - m_fLowerBleed; 
	case RIGHT:	 return GetSheetTrimBox(nSide, pObject, pTemplate) + m_fRightBleed; 
	case TOP:	 return GetSheetTrimBox(nSide, pObject, pTemplate) + m_fUpperBleed; 
	default:	 return 0.0f;											   
	}
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////




////////////////////////////// CPressDeviceList /////////////////////////////
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CPressDeviceList, CObject, VERSIONABLE_SCHEMA | 1)


CPressDeviceList::CPressDeviceList()
{
}


// add device to list only if not already in - if so, just return the index
int CPressDeviceList::AddDevice(CPressDevice& rPressDevice, BOOL& bRenamed)
{
	bRenamed = FALSE;

	int		 nIndex	= 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (GetNext(pos) == rPressDevice)	// device already in list - so do not add, but return the index
			return nIndex;				// TODO: comparison of names not enough - comparing of complete parameters would be correct
		nIndex++;
	}

	if (DeviceNameExist(rPressDevice.m_strName))		// list contains device with same name but different parameters - so rename new device
	{
		CString strName, strNum;
		for (int i = 1; i <= GetCount() + 1; i++)
		{
			strNum.Format("%d", i);
			strName = rPressDevice.m_strName + strNum;
			if ( ! DeviceNameExist(strName))
				break;
		}

		rPressDevice.m_strName = strName;
		bRenamed = TRUE;
	}

	AddTail(rPressDevice);

	return GetCount() - 1;
}

BOOL CPressDeviceList::DeviceNameExist(const CString& strName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
		if (GetNext(pos).m_strName.CompareNoCase(strName) == 0)
			return TRUE;

	return FALSE;
}

int CPressDeviceList::GetDeviceIndex(const CString& strName)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (GetNext(pos).m_strName.CompareNoCase(strName) == 0)
			return nIndex;
		nIndex++;
	}

	return -1;
}

int CPressDeviceList::GetDeviceIndexByID(const CString& strID)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (GetNext(pos).m_strDeviceID.CompareNoCase(strID) == 0)
			return nIndex;
		nIndex++;
	}

	return -1;
}

POSITION CPressDeviceList::GetDevicePos(const CString& strName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		if (GetAt(pos).m_strName.CompareNoCase(strName) == 0)
			return pos;
		GetNext(pos);
	}

	return NULL;
}

CPressDevice* CPressDeviceList::GetDevice(const CString& strName)
{
	if (strName.IsEmpty())
		return NULL;

	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (GetAt(pos).m_strName.CompareNoCase(strName) == 0)
			return &GetAt(pos);

		nIndex++;
		GetNext(pos);
	}

	return NULL;
}


/////////////////////////////////////////////////////////////////////////////
// CPressDevice

IMPLEMENT_SERIAL(CPressDevice, CObject, VERSIONABLE_SCHEMA | 2)

CPressDevice::CPressDevice()
{
}

CPressDevice::~CPressDevice()
{
	m_strName.Empty();
	m_strDeviceID.Empty();
	m_strNotes.Empty();
}

// operator overload for CPressDevice - needed by CList functions:

const CPressDevice& CPressDevice::operator=(const CPressDevice& rPressDevice)
{
	m_strName				= rPressDevice.m_strName;
	m_strDeviceID			= rPressDevice.m_strDeviceID;
	m_nPressType			= rPressDevice.m_nPressType;
	m_fPlateWidth			= rPressDevice.m_fPlateWidth;
	m_fPlateHeight			= rPressDevice.m_fPlateHeight; 
	m_fPlateEdgeToPrintEdge = rPressDevice.m_fPlateEdgeToPrintEdge;
	m_fGripper				= rPressDevice.m_fGripper;
	m_fXRefLine				= rPressDevice.m_fXRefLine;
	m_fCenterOfSection		= rPressDevice.m_fCenterOfSection;
	m_strNotes				= rPressDevice.m_strNotes;

	return *this;
}


BOOL CPressDevice::operator==(const CPressDevice& rPressDevice)
{
	if (m_strName		!= rPressDevice.m_strName)
		return FALSE;
	if (m_strDeviceID	!= rPressDevice.m_strDeviceID)
		return FALSE;
	if (m_nPressType	!= rPressDevice.m_nPressType)
		return FALSE;
	if (m_fPlateWidth	!= rPressDevice.m_fPlateWidth)
		return FALSE;
	if (m_fPlateHeight	!= rPressDevice.m_fPlateHeight)
		return FALSE;
		
	switch (m_nPressType)
	{
	case CPressDevice::SheetPress: if (m_fPlateEdgeToPrintEdge	!= rPressDevice.m_fPlateEdgeToPrintEdge)
									   return FALSE;
								   if (m_fGripper				!= rPressDevice.m_fGripper)
									   return FALSE;
								   break;
	case CPressDevice::WebPress:   if (m_fXRefLine				!= rPressDevice.m_fXRefLine) 
									   return FALSE;
								   if (m_fCenterOfSection		!= rPressDevice.m_fCenterOfSection)
									   return FALSE;
								   break;
	}

	return TRUE;
}


float CPressDevice::GetPlateRefline(int nReferenceLine)
{
	float fPlateLeft   = 0.0f;
	float fPlateBottom = 0.0f;
	float fPlateRight  = fPlateLeft   + m_fPlateWidth;
	float fPlateTop	   = fPlateBottom + m_fPlateHeight;

	switch (nReferenceLine)
	{
	case LEFT:		return fPlateLeft;
	case RIGHT:		return fPlateRight;
	case XCENTER:	return fPlateLeft   + (fPlateRight - fPlateLeft) / 2;
	case BOTTOM:	return fPlateBottom;
	case TOP:		return fPlateTop;
	case YCENTER:	return fPlateBottom + (fPlateTop - fPlateBottom) / 2; 
	}

	return 0.0f;
}

void CPressDevice::DrawPlate(CDC* pDC, CRect rcFrame, float fPaperWidth, float fPaperHeight)
{
	float platemid = m_fPlateWidth / 2.0f;
	float fPaperLeft  = platemid - (fPaperWidth / 2.0f);
	float fPaperRight = fPaperLeft +  fPaperWidth;
	float fPaperBottom, fPaperTop;
	if (m_nPressType == SheetPress)
		fPaperBottom = m_fPlateEdgeToPrintEdge - m_fGripper;
	else
		fPaperBottom = m_fCenterOfSection - (fPaperHeight / 2.0f);
	fPaperTop = fPaperBottom + fPaperHeight;

	BOOL bShowMeasuring = ( (rcFrame.Width() > 150) && (rcFrame.Height() > 100) ) ? TRUE : FALSE;

	float fExtentX = max(m_fPlateWidth,  fPaperWidth);
	float fExtentY = max(m_fPlateHeight, fPaperHeight);
	pDC->SaveDC();
	CRect rcClip = rcFrame; 
	pDC->IntersectClipRect(rcClip);
	rcFrame.DeflateRect((bShowMeasuring) ? 45 : 5, (bShowMeasuring) ? 40 : 5);
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(CPrintSheetView::WorldToLP(fExtentX), CPrintSheetView::WorldToLP(fExtentY));
	pDC->SetWindowOrg(CPrintSheetView::WorldToLP(min(fPaperLeft, 0.0f)),	CPrintSheetView::WorldToLP(max(fPaperTop, fExtentY)));
	pDC->SetViewportExt(rcFrame.Width(), -rcFrame.Height());
	pDC->SetViewportOrg(rcFrame.left,	  rcFrame.top);

	CPlate::Draw(pDC, this);

	if ( (fabs(fPaperWidth) > 0.001f) && (fabs(fPaperHeight) > 0.001f) )
	{
		CRect sheetRect;
		sheetRect.left	 = CPrintSheetView::WorldToLP(fPaperLeft);
		sheetRect.top	 = CPrintSheetView::WorldToLP(fPaperBottom);
		sheetRect.right	 = CPrintSheetView::WorldToLP(fPaperRight);
		sheetRect.bottom = CPrintSheetView::WorldToLP(fPaperTop);

		CBrush paperBrush;
		CPen   framePen, shadowPen;
		paperBrush.CreateSolidBrush(WHITE);
		framePen.CreatePen (PS_SOLID, 0, LIGHTGRAY);
		shadowPen.CreatePen(PS_SOLID, 0, DARKGRAY);
		CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&paperBrush);	
		CPen*   pOldPen	  = pDC->SelectObject(&framePen);	
		CPrintSheetView::DrawRectangleExt(pDC, &sheetRect);

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);		
		paperBrush.DeleteObject();
		shadowPen.DeleteObject();
	}

	if (bShowMeasuring)
		CPlate::DrawMeasuring(pDC, this);

	pDC->RestoreDC(-1);
}

BOOL CPressDevice::FindLargestPaper(CSheet& rLargestSheet)
{
	CSheetList sheetList;		
	sheetList.Load();
	
	rLargestSheet.m_fWidth = rLargestSheet.m_fHeight = 0.0f;

	BOOL	 bFound = FALSE;
	POSITION pos	= sheetList.GetHeadPosition();
	while (pos)
	{
		CSheet& rRefSheet = sheetList.GetNext(pos);
		if (SheetFitsToPlate(rRefSheet))
			if ( (rRefSheet.m_fWidth * rRefSheet.m_fHeight) > (rLargestSheet.m_fWidth * rLargestSheet.m_fHeight) )
			{
				rLargestSheet = rRefSheet;
				bFound = TRUE;
			}
	}

	return bFound;
}

BOOL CPressDevice::FindPaper(CSheet& rSheet, CString strPaperName)
{
	CSheetList sheetList;		
	sheetList.Load();
	
	POSITION pos = sheetList.GetHeadPosition();
	while (pos)
	{
		CSheet& rRefSheet = sheetList.GetNext(pos);
		if (rRefSheet.m_strSheetName == strPaperName)
		{
			rSheet = rRefSheet;
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPressDevice::SheetFitsToPlate(CSheet& rSheet)
{
	float platemid = m_fPlateWidth / 2.0f;
	float fPaperLeft  = platemid - (rSheet.m_fWidth / 2.0f);
	float fPaperRight = fPaperLeft +  rSheet.m_fWidth;
	float fPaperBottom, fPaperTop;
	if (m_nPressType == SheetPress)
		fPaperBottom = m_fPlateEdgeToPrintEdge - m_fGripper;
	else
		fPaperBottom = m_fCenterOfSection - (rSheet.m_fHeight / 2.0f);
	fPaperTop = fPaperBottom + rSheet.m_fHeight;

	if ( (fPaperLeft < 0.0f) || (fPaperBottom < 0.0f) || (fPaperRight > m_fPlateWidth) || (fPaperTop > m_fPlateHeight) )		// TODO: check for non printable area
		return FALSE;
	else
		return TRUE;
}


// helper function for LayoutList elements
template <> void AFXAPI SerializeElements <CPressDevice> (CArchive& ar, CPressDevice* pPressDevice, int nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CPressDevice));

	if (ar.IsStoring())
	{
		ar << pPressDevice->m_strName;
		ar << pPressDevice->m_strDeviceID;
		ar << pPressDevice->m_nPressType;
		ar << pPressDevice->m_fPlateWidth;
		ar << pPressDevice->m_fPlateHeight;
		ar << pPressDevice->m_fPlateEdgeToPrintEdge;
		ar << pPressDevice->m_fGripper;
		ar << pPressDevice->m_fXRefLine;
		ar << pPressDevice->m_fCenterOfSection;
		ar << pPressDevice->m_strNotes;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();
		
		switch (nVersion)
		{
		case 1:
			ar >> pPressDevice->m_strName;
			ar >> pPressDevice->m_nPressType;
			ar >> pPressDevice->m_fPlateWidth;
			ar >> pPressDevice->m_fPlateHeight;
			ar >> pPressDevice->m_fPlateEdgeToPrintEdge;
			ar >> pPressDevice->m_fGripper;
			ar >> pPressDevice->m_fXRefLine;
			ar >> pPressDevice->m_fCenterOfSection;
			ar >> pPressDevice->m_strNotes;
			break;
		case 2:
			ar >> pPressDevice->m_strName;
			ar >> pPressDevice->m_strDeviceID;
			ar >> pPressDevice->m_nPressType;
			ar >> pPressDevice->m_fPlateWidth;
			ar >> pPressDevice->m_fPlateHeight;
			ar >> pPressDevice->m_fPlateEdgeToPrintEdge;
			ar >> pPressDevice->m_fGripper;
			ar >> pPressDevice->m_fXRefLine;
			ar >> pPressDevice->m_fCenterOfSection;
			ar >> pPressDevice->m_strNotes;
			break;
		}
	}
}





///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// Document Device Analyse - functions 

// Compares PressDevice entries of the document with the global PressDeviceList 
// If not fully equal take the PressDeviceList from document
void CImpManDoc::AnalysePressDeviceIndices()
{	
	int numberDevices, i;
	int nIndex;
	CPressDevice  PressDeviceBufDoc, PressDeviceBufApp;

	nIndex = -1; // Predefine with "NotFound"

	if ( ! (m_PrintSheetList.m_Layouts.m_PressDevices.IsEmpty())) 
	{
		// Search to Find the default PressDevice of the document in the global theApp.m_PressDeviceList
		POSITION pos = m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(m_nDefaultPressDeviceIndex);
		if (pos)
		{
			PressDeviceBufDoc = m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);

			nIndex = CompareWithAppList(&PressDeviceBufDoc); //Compare default PressDevice

			if (nIndex != -1)
					// Default PressDevice of the document found in the global theApp.m_PressDeviceList
			{		// Now compare the other PressDevices
				numberDevices = m_PrintSheetList.m_Layouts.m_PressDevices.GetCount();
				if (numberDevices > 1) // if more than the DefaultPressDevice only
				{
					pos = m_PrintSheetList.m_Layouts.m_PressDevices.GetHeadPosition();
					for (i = 0; i < numberDevices; i ++)
					{
						PressDeviceBufDoc = m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
						nIndex = CompareWithAppList(&PressDeviceBufDoc);
						if (nIndex == -1)
							break;
						m_PrintSheetList.m_Layouts.m_PressDevices.GetNext(pos);
					}
				}
			}
			//Compare not succesful -> Message Window
			if (nIndex == -1)
			{
				CString string;
				string.Format(IDS_PRESS_NOTFOUND, GetTitle());
				AfxMessageBox(string, MB_OK);
			}
		}
	}
	else
		; // TODO : Errormessage: There must be a default pressdevice
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Compares PressDevice entries with the global PressDeviceList 
int CImpManDoc::CompareWithAppList(CPressDevice* pPressDeviceBufDoc)
{
	CPressDevice PressDeviceBufApp;
	int nIndex = -1;

	int numberDevices = theApp.m_PressDeviceList.GetCount();
	POSITION pos	  = theApp.m_PressDeviceList.GetHeadPosition();

	for (int i = 0; i < numberDevices; i ++)
	{
		PressDeviceBufApp = theApp.m_PressDeviceList.GetAt(pos);

		if (pPressDeviceBufDoc->m_strName == PressDeviceBufApp.m_strName)
		{
			if ((pPressDeviceBufDoc->m_nPressType	== PressDeviceBufApp.m_nPressType) &&
				(pPressDeviceBufDoc->m_fPlateWidth  == PressDeviceBufApp.m_fPlateWidth) &&
				(pPressDeviceBufDoc->m_fPlateHeight == PressDeviceBufApp.m_fPlateHeight))
			{
				if (pPressDeviceBufDoc->m_nPressType == CPressDevice::SheetPress)
				{
					if ((pPressDeviceBufDoc->m_fPlateEdgeToPrintEdge == PressDeviceBufApp.m_fPlateEdgeToPrintEdge) &&
						(pPressDeviceBufDoc->m_fGripper				 == PressDeviceBufApp.m_fGripper))
							nIndex = i;	// PressDevice of Document found in AppList 
				}
				else //WebPress
				{
					if ((pPressDeviceBufDoc->m_fXRefLine		  == PressDeviceBufApp.m_fXRefLine) &&
						(pPressDeviceBufDoc->m_fCenterOfSection	  == PressDeviceBufApp.m_fCenterOfSection))
							nIndex = i;	// PressDevice of Document found in AppList
				}
			}
		}
		theApp.m_PressDeviceList.GetNext(pos);
	}
	return nIndex;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Look for unused entries in document
void CImpManDoc::ReorganizePressDeviceIndices(int nLayoutSide)
{
	CLayout* pLayout;
	POSITION pos;
	BOOL	 PressDeviceFound;

	pos = m_PrintSheetList.m_Layouts.GetHeadPosition(); // if pos == NULL we are creating a new document
	if ( ! pos)											// clicking OK in JobSettingsPage2
		return;

	int numberDevices = m_PrintSheetList.m_Layouts.m_PressDevices.GetCount();
	int	nIndex		  = numberDevices - 1;

	while (numberDevices)
	{
		PressDeviceFound = FALSE;
		pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
		while (pos && ! PressDeviceFound)
		{
			pLayout = &(m_PrintSheetList.m_Layouts.GetNext(pos));
			if (pLayout->m_FrontSide.m_nPressDeviceIndex == nIndex)
				PressDeviceFound = TRUE;
			if (pLayout->m_BackSide.m_nPressDeviceIndex == nIndex)
				PressDeviceFound = TRUE;
		}
		if ( ! PressDeviceFound)
		{
			pos = m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(nIndex);
			m_PrintSheetList.m_Layouts.m_PressDevices.RemoveAt(pos);

			pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
			while (pos)
			{
				pLayout = &(m_PrintSheetList.m_Layouts.GetNext(pos));
				if (pLayout->m_FrontSide.m_nPressDeviceIndex > nIndex)
					pLayout->m_FrontSide.m_nPressDeviceIndex--;
				if (pLayout->m_BackSide.m_nPressDeviceIndex > nIndex)
					pLayout->m_BackSide.m_nPressDeviceIndex--;
			}

			if (nLayoutSide == ALLSIDES)
				if (m_nDefaultPressDeviceIndex > nIndex)
					m_nDefaultPressDeviceIndex--;
		}
	
		nIndex--;
		numberDevices--;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Check if there are more than one PressDevice in Document for selection in TreeView
BOOL CImpManDoc::MoreThanOnePressdevice()
{
	int nSize;
	CLayout* pActLayout;
	CPressDevice* pFrontPressDevice;
	CPressDevice* pBackPressDevice;

	int nSide = GetActSight();
	switch (nSide)
	{
	case ALLSIDES:	nSize = m_PrintSheetList.m_Layouts.m_PressDevices.GetCount();
					if (nSize > 1)
						return TRUE;
					else
						return FALSE;
					break;
	case BOTHSIDES:	pActLayout	= GetActLayout();
					if ( ! pActLayout)
						return FALSE;
					pFrontPressDevice = pActLayout->m_FrontSide.GetPressDevice();
					pBackPressDevice  = pActLayout->m_BackSide.GetPressDevice();
					if (pFrontPressDevice != pBackPressDevice)
						return TRUE;
					else
						return FALSE;
					break;
	default:		return FALSE; // Front- or Backside has only ONE PressDevice
					break;
	}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// Helper - functions 

///////////////////////////////////////////////////////////////////////////////////////////////
// Set the PrintSheet PressDevice in Document from given selection of ListBox
void CImpManDoc::SetPrintSheetPressDevice(int nIndex, int nLayoutSide, CLayout* pActLayout, BOOL bReorganize)
{
	CPressDevice PressDeviceBufDoc, PressDeviceBufApp;
	BOOL		 PressDeviceFoundInDoc = FALSE;

	if ( ! pActLayout)
		pActLayout = GetActLayout();
	if ( ! pActLayout)
		return;
	if ( ! theApp.m_PressDeviceList.GetCount())
		return;

	if (nIndex <= 0)
		nIndex = 0;
	POSITION pos		= theApp.m_PressDeviceList.FindIndex(nIndex);
	if ( ! pos)
		return;
	PressDeviceBufApp	= theApp.m_PressDeviceList.GetAt(pos);
	pos					= m_PrintSheetList.m_Layouts.m_PressDevices.GetHeadPosition();
	int numberDevices	= m_PrintSheetList.m_Layouts.m_PressDevices.GetCount();

	for (int i = 0; i < numberDevices; i ++)
	{
		PressDeviceBufDoc = m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
		if ( (PressDeviceBufDoc.m_strName.CompareNoCase(PressDeviceBufApp.m_strName) == 0) &&
			PressDeviceBufDoc.m_nPressType			  == PressDeviceBufApp.m_nPressType &&
			PressDeviceBufDoc.m_fPlateWidth			  == PressDeviceBufApp.m_fPlateWidth &&
			PressDeviceBufDoc.m_fPlateHeight		  == PressDeviceBufApp.m_fPlateHeight &&
			PressDeviceBufDoc.m_fPlateEdgeToPrintEdge == PressDeviceBufApp.m_fPlateEdgeToPrintEdge &&
			PressDeviceBufDoc.m_fGripper			  == PressDeviceBufApp.m_fGripper &&
			PressDeviceBufDoc.m_fXRefLine			  == PressDeviceBufApp.m_fXRefLine &&
			PressDeviceBufDoc.m_fCenterOfSection	  == PressDeviceBufApp.m_fCenterOfSection )
		{
			switch (nLayoutSide)
			{
			case FRONTSIDE : pActLayout->m_FrontSide.m_nPressDeviceIndex = i;
							 break;
			case BACKSIDE  : pActLayout->m_BackSide.m_nPressDeviceIndex  = i;
							 break;
			case BOTHSIDES : pActLayout->m_FrontSide.m_nPressDeviceIndex = i;
							 pActLayout->m_BackSide.m_nPressDeviceIndex  = i;
							 break;
			case ALLSIDES  : m_nDefaultPressDeviceIndex = i;
							 break;
			}
			PressDeviceFoundInDoc = TRUE;
		}
		m_PrintSheetList.m_Layouts.m_PressDevices.GetNext(pos);
	}
	if ( ! PressDeviceFoundInDoc)
	{
		m_PrintSheetList.m_Layouts.m_PressDevices.AddTail(PressDeviceBufApp);
		
		switch (nLayoutSide)
		{	
		case FRONTSIDE : pActLayout->m_FrontSide.m_nPressDeviceIndex = numberDevices;
			break;
		case BACKSIDE  : pActLayout->m_BackSide.m_nPressDeviceIndex  = numberDevices;
			break;
		case BOTHSIDES : pActLayout->m_FrontSide.m_nPressDeviceIndex = numberDevices;
						 pActLayout->m_BackSide.m_nPressDeviceIndex  = numberDevices;
			break;
		case ALLSIDES  : m_nDefaultPressDeviceIndex = numberDevices;
			break;
		}
	}
	if (nLayoutSide == ALLSIDES)
	{
		int nLayouts = m_PrintSheetList.m_Layouts.GetCount();
		if (nLayouts)
		{
			pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
			for (int i = 0; i < nLayouts; i++)
			{
				CLayout& rActLayout = m_PrintSheetList.m_Layouts.GetAt(pos);
				rActLayout.m_FrontSide.m_nPressDeviceIndex = m_nDefaultPressDeviceIndex;
				rActLayout.m_BackSide.m_nPressDeviceIndex  = m_nDefaultPressDeviceIndex;
				m_PrintSheetList.m_Layouts.GetNext(pos);
			}
		}
	}

	if (bReorganize)
		ReorganizePressDeviceIndices(nLayoutSide); // Look for double or unused entries
}

void CImpManDoc::OnFileSaveAs() 
{
	CDocument::OnFileSaveAs();

#ifdef CTP
	m_PrintSheetList.m_strIPUJobname = GetTitle().SpanExcluding(".");
#endif

	theApp.SetMyCurrentDirectory(theApp.settings.m_szWorkDir, TRUE);
}

void CImpManDoc::OnFileClose() 
{
	CDocument::OnFileClose();	
}

void CImpManDoc::OnFileSave() 
{
	theApp.SetMyCurrentDirectory(theApp.settings.m_szJobsDir, TRUE);

	CDocument::OnFileSave();

	theApp.SetMyCurrentDirectory(theApp.settings.m_szWorkDir, TRUE);
}

BOOL CImpManDoc::DoSave(LPCTSTR lpszPathName, BOOL bReplace)
	// Save the document data to a file
	// lpszPathName = path name where to save document file
	// if lpszPathName is NULL then the user will be prompted (SaveAs)
	// note: lpszPathName can be different than 'm_strPathName'
	// if 'bReplace' is TRUE will change file name if successful (SaveAs)
	// if 'bReplace' is FALSE will not change path name (SaveCopyAs)
{
	CString newName = lpszPathName;
	if (newName.IsEmpty())
	{
		CDocTemplate* pTemplate = GetDocTemplate();
		ASSERT(pTemplate != NULL);

		newName = m_strPathName;
		if (bReplace && newName.IsEmpty())
		{
			newName = m_strTitle;
			// check for dubious filename
			int iBad = newName.FindOneOf(_T("#%;/\\"));
			if (iBad != -1)
				newName.ReleaseBuffer(iBad);

			// append the default suffix if there is one
			CString strExt;
			if (pTemplate->GetDocString(strExt, CDocTemplate::filterExt) &&
			  !strExt.IsEmpty())
			{
				ASSERT(strExt[0] == '.');
				newName += strExt;
			}
		}

		if ( ! theApp.DoPromptFileName(newName, bReplace ? AFX_IDS_SAVEFILE : AFX_IDS_SAVEFILECOPY, OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_NOCHANGEDIR | OFN_OVERWRITEPROMPT | OFN_ENABLESIZING, FALSE, pTemplate))
			return FALSE;       // don't even attempt to save
	}

	CWaitCursor wait;

	if (!OnSaveDocument(newName))
	{
		if (lpszPathName == NULL)
		{
			// be sure to delete the file
			CFile::Remove(newName);
		}
		return FALSE;
	}

	// reset the title and change the document name
	if (bReplace)
		SetPathName(newName);

	return TRUE;        // success
}

void CImpManDoc::OnFileAutoBackup()
{
	char szCurrentDir[_MAX_PATH];
	DWORD nBufferLength = _MAX_PATH;

	if ( ! GetCurrentDirectory(nBufferLength, szCurrentDir))
		return;
	if ( ! theApp.SetMyCurrentDirectory(theApp.settings.m_szTempDir, FALSE))
		return;

	CString strPathName;

	strPathName  = theApp.settings.m_szTempDir;
	strPathName += "\\AutoBackup";

	//CString strBaseName, strExt;
	//CDocTemplate* pDocTemplate = GetDocTemplate();
	//if (!pDocTemplate->GetDocString(strBaseName, CDocTemplate::docName)
	//	|| !pDocTemplate->GetDocString(strExt, CDocTemplate::filterExt))
	//{
	//	AfxThrowUserException(); // These doc template strings will
	//							 // be available if you created the application using AppWizard
	//							 // and specified the file extension as an option for
	//							 // the document class produced by AppWizard.
	//}

	strPathName += _T(".job");//strExt;
/////////////////////////////////////////////////////////////////////////////
//	Look, if an existing autobackup.job-file is opened
	BOOL bStoreOK;
	CFile file;
	CFileException fileException;
	WIN32_FIND_DATA findData;
	HANDLE hfileSearch = FindFirstFile(strPathName, &findData);
	if (hfileSearch != INVALID_HANDLE_VALUE)
	{
	// we found an autobackup.job -> now look if we can open the file for writing.
	//								 If open fails the file is still open in our program
		if (file.Open(strPathName, CFile::modeWrite, &fileException))
		{
			bStoreOK = TRUE;
			file.Close();
		}
		else
			bStoreOK = FALSE;
	}
	else
		bStoreOK = TRUE;

	FindClose(hfileSearch);

	if (bStoreOK)
	{
	//	Now it's safe to store the backup-file
	//	If you work seperate with "OnSaveDocument" the saved file stays open !!!
		BOOL bTemp;
		bTemp = m_bRemember;
		LPSTORAGE lpOrigStg = m_lpRootStg;
		m_lpRootStg = NULL; // ignore embedded storage for now
		TRY
		{
			m_bRemember = FALSE;
			COleDocument::OnSaveDocument(strPathName);
		}
		END_TRY
		m_lpRootStg = lpOrigStg;
		m_bRemember = bTemp;
	}

	theApp.SetMyCurrentDirectory(szCurrentDir, TRUE);
}

BOOL CImpManDoc::SaveModified() 
{
	if (m_strPathName.IsEmpty())	// Only changeDirectory for a new file without path-information
		theApp.SetMyCurrentDirectory(theApp.settings.m_szJobsDir, TRUE);

	BOOL ret;
	CString strFileString;
	ret = COleDocument::SaveModified();

	theApp.SetMyCurrentDirectory(theApp.settings.m_szWorkDir, TRUE);

	return ret;
}

CPrintSheet* CImpManDoc::GetActPrintSheet() 
{
	CFrameWnd* pFrame = ((CMainFrame*)theApp.m_pMainWnd)->GetActiveFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->GetActPrintSheet();

	return (NULL);
}

CLayout* CImpManDoc::GetActLayout() 
{
	CPrintSheet* pActPrintSheet = GetActPrintSheet();

	if (pActPrintSheet)
		return (pActPrintSheet->GetLayout());
	else
		return (NULL);
}

CPressDevice* CImpManDoc::GetActPressDevice(int nSide/*= 0*/)
{
	CLayout* pActLayout = GetActLayout();

	if (pActLayout)
	{
		switch (nSide)
		{
			case FRONTSIDE : return (pActLayout->m_FrontSide.GetPressDevice());
			case BACKSIDE  : return (pActLayout->m_BackSide.GetPressDevice());
			default		   : switch (GetActSight())
							 {
								case BOTHSIDES :
								case FRONTSIDE : return (pActLayout->m_FrontSide.GetPressDevice());
								case BACKSIDE  : return (pActLayout->m_BackSide.GetPressDevice());
								default		   : return (NULL);
							 }
		}
	}
	else
		return (NULL);
}

int CImpManDoc::GetActSight()
{
	//if (theApp.m_nApplication == APPLICATION_LABELS)
	//{
	//	if (m_screenLayout.m_bLabelsShowFrontBack)
	//		return BOTHSIDES;
	//	else
	//		return FRONTSIDE;
	//}

	CFrameWnd* pFrame = GetFrame(RUNTIME_CLASS(CNewPrintSheetFrame));
	if (pFrame)
		return ((CNewPrintSheetFrame*)pFrame)->GetActSight();

	pFrame = ((CMainFrame*)theApp.m_pMainWnd)->GetActiveFrame();
	if (pFrame)
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetActSight();

	return BOTHSIDES;
}

CPlate* CImpManDoc::GetActPlate()
{
	CPrintSheet* pActPrintSheet = GetActPrintSheet();
	if (!pActPrintSheet)
		return NULL;

	int nActSight = GetActSight();
	if (nActSight == -1)
		return NULL;

	CPlateList* pPlateList;
	switch (nActSight)
	{
	case FRONTSIDE: pPlateList = &pActPrintSheet->m_FrontSidePlates; break;
	case BACKSIDE:  pPlateList = &pActPrintSheet->m_BackSidePlates;	 break;
	default:		return NULL;
	}

	if ((m_nActPlate < 0) || (m_nActPlate >= pPlateList->GetCount()))
		return NULL;

	POSITION pos = pPlateList->FindIndex(m_nActPlate);
	if (!pos)
		return NULL;
	else
		return &pPlateList->GetAt(pos);
}

CFrameWnd* CImpManDoc::GetFrame(const CRuntimeClass* pClass)
{
	CWnd *pPrevWnd, *pNextWnd;
	for (pPrevWnd = theApp.m_pMainWnd; (pNextWnd = pPrevWnd->GetWindow(GW_HWNDPREV)) != NULL; pPrevWnd = pNextWnd)
    {
		if (pNextWnd->IsKindOf(pClass))
			return (CFrameWnd*)pNextWnd;
    }

	return NULL;
}


/////////////////////////////////////////////////////////////////////////////
// CZoomState ///////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CZoomState, CObject, VERSIONABLE_SCHEMA | 1)

CZoomState::CZoomState()
{
	m_viewportSize = CSize(0, 0);
	m_scrollPos	   = CPoint(0, 0);
}

CZoomState::CZoomState(const CZoomState& rZoomState)
{
	*this = rZoomState;
}

const CZoomState& CZoomState::operator=(const CZoomState& rZoomState)
{
	m_viewportSize = rZoomState.m_viewportSize;
	m_scrollPos	   = rZoomState.m_scrollPos;

	return *this;
}

void CZoomState::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CZoomState));

	if (ar.IsStoring())
	{
		ar << m_viewportSize;
		ar << m_scrollPos;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1: ar >> m_viewportSize;
				ar >> m_scrollPos;
				break;
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CScreenLayout ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CScreenLayout, CObject, VERSIONABLE_SCHEMA | 3)

CScreenLayout::CScreenLayout()
{
	m_bLabelsShowFrontBack = FALSE;
	m_bLabelsShowBoxShapes = TRUE;
}

void CScreenLayout::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CScreenLayout));

	if (ar.IsStoring())
	{
		m_zoomStateBookBlockView.Serialize(ar);
		ar << m_bLabelsShowFrontBack;
		ar << m_bLabelsShowBoxShapes;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1: m_zoomStateBookBlockView.Serialize(ar);
				m_bLabelsShowFrontBack = FALSE;
				m_bLabelsShowBoxShapes = TRUE;
				break;
		case 2: m_zoomStateBookBlockView.Serialize(ar);
				ar >> m_bLabelsShowFrontBack;
				m_bLabelsShowBoxShapes = TRUE;
				break;
		case 3: m_zoomStateBookBlockView.Serialize(ar);
				ar >> m_bLabelsShowFrontBack;
				ar >> m_bLabelsShowBoxShapes;
				break;
		}
	}
}
