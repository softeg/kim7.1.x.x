// PrintSheetViewMeasure.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CTrimChannel

int CTrimChannel::GetLayoutSide() 
{ 
	if (pObjectList1) 
		if (pObjectList1->GetCount()) 
			if (pObjectList1->GetHead())
				return ((CLayoutObject*)pObjectList1->GetHead())->GetLayoutSideIndex();

	if (pObjectList2) 
		if (pObjectList2->GetCount()) 
			if (pObjectList2->GetHead())
				return ((CLayoutObject*)pObjectList2->GetHead())->GetLayoutSideIndex();

	displayRect.SetRectEmpty();
	displayOldRect.SetRectEmpty();
	return 0;
};

const CTrimChannel& CTrimChannel::operator=(const CTrimChannel& rTrimChannel)
{
	n_Direction		 = rTrimChannel.n_Direction;
	n_MoveDirection	 = rTrimChannel.n_MoveDirection;
	n_Side			 = rTrimChannel.n_Side;
	n_SightToDraw	 = rTrimChannel.n_SightToDraw;
	fTrim1			 = rTrimChannel.fTrim1; 
	fTrim2			 = rTrimChannel.fTrim2;
	Rect			 = rTrimChannel.Rect;
	OldRect			 = rTrimChannel.OldRect;
	pObject			 = rTrimChannel.pObject;				
	pObjectNeighbour = rTrimChannel.pObjectNeighbour;
	displayRect		 = rTrimChannel.displayRect;
	displayOldRect	 = rTrimChannel.displayOldRect;

	return *this;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// operator overload for CChannelPartnerVertical - needed by CList functions:

const CChannelPartnerVertical& CChannelPartnerVertical::operator=(const CChannelPartnerVertical& rPartner)
{
	Copy(rPartner);
	return *this;	   
}

void CChannelPartnerVertical::Copy(const CChannelPartnerVertical& rPartner)
{
	CLayoutObject* pLayoutObject;

	unsigned nCount = rPartner.pObjectListLeft->GetCount();
	POSITION pos   = rPartner.pObjectListLeft->GetHeadPosition();
	for (unsigned i = 0; i < nCount; i++)                        
	{
		pLayoutObject = (CLayoutObject*)rPartner.pObjectListLeft->GetAt(pos);
		pObjectListLeft->AddTail(pLayoutObject);
		rPartner.pObjectListLeft->GetNext(pos);
	}
	nCount = rPartner.pObjectListRight->GetCount();
	pos	   = rPartner.pObjectListRight->GetHeadPosition();
	for (UINT i = 0; i < nCount; i++)                        
	{
		pLayoutObject = (CLayoutObject*)rPartner.pObjectListRight->GetAt(pos);
		pObjectListRight->AddTail(pLayoutObject);
		rPartner.pObjectListRight->GetNext(pos);
	}
}
// copy constructor for CChannelPartnerVertical - needed by CList functions:

CChannelPartnerVertical::CChannelPartnerVertical(const CChannelPartnerVertical& rPartner) 
{
	Copy(rPartner);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// operator overload for CChannelPartnerHorizontal - needed by CList functions:

const CChannelPartnerHorizontal& CChannelPartnerHorizontal::operator=(const CChannelPartnerHorizontal& rPartner)
{
	Copy(rPartner);
	return *this;
}

void CChannelPartnerHorizontal::Copy(const CChannelPartnerHorizontal& rPartner)
{
	CLayoutObject* pLayoutObject;

	unsigned nCount = rPartner.pObjectListTop->GetCount();
	POSITION pos   = rPartner.pObjectListTop->GetHeadPosition();
	for (unsigned i = 0; i < nCount; i++)                        
	{
		pLayoutObject = (CLayoutObject*)rPartner.pObjectListTop->GetAt(pos);
		pObjectListTop->AddTail(pLayoutObject);
		rPartner.pObjectListTop->GetNext(pos);
	}
	nCount = rPartner.pObjectListBottom->GetCount();
	pos	   = rPartner.pObjectListBottom->GetHeadPosition();
	for (UINT i = 0; i < nCount; i++)                        
	{
		pLayoutObject = (CLayoutObject*)rPartner.pObjectListBottom->GetAt(pos);
		pObjectListBottom->AddTail(pLayoutObject);
		rPartner.pObjectListBottom->GetNext(pos);
	}
}
// copy constructor for CChannelPartnerHorizontal - needed by CList functions:

CChannelPartnerHorizontal::CChannelPartnerHorizontal(const CChannelPartnerHorizontal& rPartner) 
{
	Copy(rPartner);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CPrintSheetView::DrawMeasure(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet)
{
	int	nChannelsVertical   = 0;
	int	nChannelsHorizontal = 0;

	CImpManDoc*	pDoc	   = CImpManDoc::GetDoc();
	CLayout*	pActLayout = pActPrintSheet->GetLayout();
	//TODO: Only calculate measurement, if layoutrect is visisble 

//	if ( ! MeasurementsVisibleInView(pDC, pActLayout, nActLayoutSide))
//		return;

	CPen measureArrowPen;
	measureArrowPen.CreatePen(PS_SOLID, 1, RED);
	CPen *pOldPen = (CPen*)pDC->SelectObject(&measureArrowPen);

	CTrimChannel TrimChannel;
	TrimChannel.pObjectList1 = new CObList;
	TrimChannel.pObjectList2 = new CObList;

	CObList* pStringRects = new CObList; 

	ChannelPartnerVerticalList* pChannelObjectsVertical = new ChannelPartnerVerticalList;
	ChannelPartnerHorizontalList *pChannelObjectsHorizontal = new ChannelPartnerHorizontalList;

	switch (nActLayoutSide)
	{
	case FRONTSIDE :
		{
			POSITION pos = pActLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pActLayout->m_FrontSide.m_ObjectList.GetNext(pos);
				MeasureChannel(pDC, &TrimChannel, &rObject, pChannelObjectsVertical, &nChannelsVertical, pChannelObjectsHorizontal, &nChannelsHorizontal, pStringRects);
			}
		}
		break;
	case BACKSIDE :
		{
			POSITION pos = pActLayout->m_BackSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pActLayout->m_BackSide.m_ObjectList.GetNext(pos);
				MeasureChannel(pDC, &TrimChannel, &rObject, pChannelObjectsVertical, &nChannelsVertical, pChannelObjectsHorizontal, &nChannelsHorizontal, pStringRects);
			}
		}
		break;
	}

	pDC->SelectObject(pOldPen);
	measureArrowPen.DeleteObject();

	delete TrimChannel.pObjectList1;
	delete TrimChannel.pObjectList2;

	delete pStringRects;

	pChannelObjectsVertical->RemoveAll();
	delete pChannelObjectsVertical;
	pChannelObjectsHorizontal->RemoveAll();
	delete pChannelObjectsHorizontal;
}

void CPrintSheetView::MeasureChannel(CDC* pDC, CTrimChannel *pTrimChannel, CLayoutObject* pObject, 
									 ChannelPartnerVerticalList *pChannelObjectsVertical, int *nChannelsVertical, 
									 ChannelPartnerHorizontalList *pChannelObjectsHorizontal, int *nChannelsHorizontal, CObList* pStringRects)
{
	CLayoutObject* pObjectNeighbour;
	CImpManDoc*	   pDoc	= CImpManDoc::GetDoc();

	pTrimChannel->n_Direction = VERTICAL;
	pTrimChannel->n_MoveDirection = LEFT | RIGHT;

	if (pObject->m_LeftNeighbours.GetSize() == 0)
	{
		if (!FindInChannelObjectsVertical(NULL, pObject, pChannelObjectsVertical, *nChannelsVertical))
			MeasureChannelVertical(pDC, pTrimChannel, pObject, NULL, pChannelObjectsVertical, LEFT, nChannelsVertical, pStringRects);
	}
	else
		for (int i = 0; i < pObject->m_LeftNeighbours.GetSize(); i++)
		{
			pObjectNeighbour = pObject->m_LeftNeighbours.GetAt(i);
			if (pObjectNeighbour)
			{
				if (!FindInChannelObjectsVertical(pObjectNeighbour, pObject, pChannelObjectsVertical, *nChannelsVertical))
					MeasureChannelVertical(pDC, pTrimChannel, pObject, pObjectNeighbour, pChannelObjectsVertical, LEFT, nChannelsVertical, pStringRects);
			}
		}

	if (pObject->m_RightNeighbours.GetSize() == 0)
	{
		if (!FindInChannelObjectsVertical(pObject, NULL, pChannelObjectsVertical, *nChannelsVertical))
			MeasureChannelVertical(pDC, pTrimChannel, pObject, NULL, pChannelObjectsVertical, RIGHT, nChannelsVertical, pStringRects);
	}
	else
		for (int i = 0; i < pObject->m_RightNeighbours.GetSize(); i++)
		{
			pObjectNeighbour = pObject->m_RightNeighbours.GetAt(i);
			if (pObjectNeighbour)
			{
				if (!FindInChannelObjectsVertical(pObject, pObjectNeighbour, pChannelObjectsVertical, *nChannelsVertical))
					MeasureChannelVertical(pDC, pTrimChannel, pObject, pObjectNeighbour, pChannelObjectsVertical, RIGHT, nChannelsVertical, pStringRects);
			}
		}

	pTrimChannel->n_Direction = HORIZONTAL;
	pTrimChannel->n_MoveDirection = BOTTOM | TOP;

	if (pObject->m_LowerNeighbours.GetSize() == 0)
	{
		if (!FindInChannelObjectsHorizontal(NULL, pObject, pChannelObjectsHorizontal, *nChannelsHorizontal))
			MeasureChannelHorizontal(pDC, pTrimChannel, pObject, NULL, pChannelObjectsHorizontal, BOTTOM, nChannelsHorizontal, pStringRects);
	}
	else
		for (int i = 0; i < pObject->m_LowerNeighbours.GetSize(); i++)
		{
			pObjectNeighbour = pObject->m_LowerNeighbours.GetAt(i);
			if (pObjectNeighbour)
			{
				if (!FindInChannelObjectsHorizontal(pObjectNeighbour, pObject, pChannelObjectsHorizontal, *nChannelsHorizontal))
					MeasureChannelHorizontal(pDC, pTrimChannel, pObject, pObjectNeighbour, pChannelObjectsHorizontal, BOTTOM, nChannelsHorizontal, pStringRects);
			}
		}

	if (pObject->m_UpperNeighbours.GetSize() == 0)
	{
		if (!FindInChannelObjectsHorizontal(pObject, NULL, pChannelObjectsHorizontal, *nChannelsHorizontal))
			MeasureChannelHorizontal(pDC, pTrimChannel, pObject, NULL, pChannelObjectsHorizontal, TOP, nChannelsHorizontal, pStringRects);
	}
	else
		for (int i = 0; i < pObject->m_UpperNeighbours.GetSize(); i++)
		{
			pObjectNeighbour = pObject->m_UpperNeighbours.GetAt(i);
			if (pObjectNeighbour)
			{
				if (!FindInChannelObjectsHorizontal(pObject, pObjectNeighbour, pChannelObjectsHorizontal, *nChannelsHorizontal))
					MeasureChannelHorizontal(pDC, pTrimChannel, pObject, pObjectNeighbour, pChannelObjectsHorizontal, TOP, nChannelsHorizontal, pStringRects);
			}
		}
}

BOOL CPrintSheetView::FindInChannelObjectsVertical (CLayoutObject* pObjectLeft, CLayoutObject* pObjectRight, ChannelPartnerVerticalList *pChannelObjectsVertical, int nChannelnumber)
{
	BOOL ObjectLeftFound, ObjectRightFound;

	CChannelPartnerVertical ChannelVertical;

	ObjectLeftFound  = (pObjectLeft  == NULL) ? TRUE : FALSE; 
	ObjectRightFound = (pObjectRight == NULL) ? TRUE : FALSE;

	POSITION pos = pChannelObjectsVertical->GetHeadPosition();
	for (int i = 0; pos && (!ObjectLeftFound || !ObjectRightFound) && (i < nChannelnumber); i++)
	{
		ChannelVertical = pChannelObjectsVertical->GetNext(pos);
		if (pObjectLeft)
		{
			POSITION pos1 = ChannelVertical.pObjectListLeft->GetHeadPosition();
			while (pos1)
			{
				if (pObjectLeft == (CLayoutObject*)ChannelVertical.pObjectListLeft->GetNext(pos1))
				{
					ObjectLeftFound = TRUE;
					break;
				}
			}
		}
		if (pObjectRight)
		{
			POSITION pos1 = ChannelVertical.pObjectListRight->GetHeadPosition();
			while (pos1)
			{
				if (pObjectRight == (CLayoutObject*)ChannelVertical.pObjectListRight->GetNext(pos1))
				{
					ObjectRightFound = TRUE;
					break;
				}
			}
		}
	}

	if (ObjectLeftFound && ObjectRightFound)
		return(TRUE);
	else
		return(FALSE);
}

BOOL CPrintSheetView::FindInChannelObjectsHorizontal (CLayoutObject* pObjectBottom, CLayoutObject* pObjectTop, ChannelPartnerHorizontalList *pChannelObjectsHorizontal, int nChannelnumber)
{
	BOOL ObjectBottomFound, ObjectTopFound;

	CChannelPartnerHorizontal ChannelHorizontal;

	ObjectBottomFound = (pObjectBottom == NULL) ? TRUE : FALSE;
	ObjectTopFound	  = (pObjectTop	   == NULL) ? TRUE : FALSE;

	POSITION pos = pChannelObjectsHorizontal->GetHeadPosition();
	for (int i = 0; pos && (!ObjectBottomFound || !ObjectTopFound) && (i < nChannelnumber); i++)
	{
		ChannelHorizontal = pChannelObjectsHorizontal->GetNext(pos);

		if (pObjectBottom)
		{
			POSITION pos1 = ChannelHorizontal.pObjectListBottom->GetHeadPosition();
			while (pos1)
			{
				if (pObjectBottom == ChannelHorizontal.pObjectListBottom->GetNext(pos1))
				{
					ObjectBottomFound = TRUE;
					break;
				}
			}
		}
		if (pObjectTop)
		{
			POSITION pos1 = ChannelHorizontal.pObjectListTop->GetHeadPosition();
			while (pos1)
			{
				if (pObjectTop == ChannelHorizontal.pObjectListTop->GetNext(pos1))
				{
					ObjectTopFound = TRUE;
					break;
				}
			}
		}
	}

	if (ObjectBottomFound && ObjectTopFound)
		return(TRUE);
	else
		return(FALSE);
}

void CPrintSheetView::MeasureChannelVertical(CDC* pDC, CTrimChannel *pTrimChannel, CLayoutObject *pObject1, CLayoutObject *pObject2, ChannelPartnerVerticalList *pChannelObjects, int Direction, int *nChannelnumber, CObList* pStringRects)
{
	CTrimOptions tempTrimOptions;

	CChannelPartnerVertical VerticalObject;

	if (pObject1)
		if (pObject1->m_nType == CLayoutObject::ControlMark)
			return;
	if (pObject2)
		if (pObject2->m_nType == CLayoutObject::ControlMark)
			return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	tempTrimOptions = pDoc->m_TrimOptions;

	pDoc->m_TrimOptions.m_bChannelThrough = TRUE;
	pDoc->m_TrimOptions.m_bChannelSingle  = FALSE;

	pTrimChannel->pObjectList1->RemoveAll();	// Target lists has to be empty -> no append
	pTrimChannel->pObjectList2->RemoveAll();

	FindTrimChannel(pTrimChannel, pObject1, pObject2, Direction);
	DrawSingleMeasure(pDC, pTrimChannel, nChannelnumber, pStringRects);

	VerticalObject.pObjectListLeft->AddTail(pTrimChannel->pObjectList1);
	VerticalObject.pObjectListRight->AddTail(pTrimChannel->pObjectList2);
	pChannelObjects->AddTail(VerticalObject);
	(*nChannelnumber)++;

	pDoc->m_TrimOptions = tempTrimOptions;
}

void CPrintSheetView::MeasureChannelHorizontal(CDC* pDC, CTrimChannel *pTrimChannel, CLayoutObject* pObject1, CLayoutObject* pObject2, ChannelPartnerHorizontalList *pChannelObjects, int Direction, int *nChannelnumber, CObList* pStringRects)
{
	CTrimOptions tempTrimOptions;

	CChannelPartnerHorizontal HorizontalObject;

	if (pObject1)
		if (pObject1->m_nType == CLayoutObject::ControlMark)	
			return;	// Don't measure marks, because there would be to much signs on the screen
	if (pObject2)
		if (pObject2->m_nType == CLayoutObject::ControlMark)	
			return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	tempTrimOptions = pDoc->m_TrimOptions;

	pDoc->m_TrimOptions.m_bChannelThrough = TRUE;
	pDoc->m_TrimOptions.m_bChannelSingle  = FALSE;

	pTrimChannel->pObjectList1->RemoveAll();	// Target lists has to be empty -> no append
	pTrimChannel->pObjectList2->RemoveAll();

	FindTrimChannel(pTrimChannel, pObject1, pObject2, Direction);
	DrawSingleMeasure(pDC, pTrimChannel, nChannelnumber, pStringRects);

	HorizontalObject.pObjectListBottom->AddTail(pTrimChannel->pObjectList1);
	HorizontalObject.pObjectListTop->AddTail(pTrimChannel->pObjectList2);
	pChannelObjects->AddTail(HorizontalObject);
	(*nChannelnumber)++;

	pDoc->m_TrimOptions = tempTrimOptions;
}


void CPrintSheetView::DrawSingleMeasure(CDC* pDC, CTrimChannel *pTrimChannel, int *nChannelnumber, CObList* pStringRects)
{
	long	XMid, YMid, XPos, YPos, ObjectWidth, ObjectHeight;
	long	XTextPos, YTextPos;
	int		i, j, overlapped;
	int		rect_pos;
	CString string;
	float	fTrim1, fTrim2;
	int		n_SizeList;
	RECT	TextRect;
	POSITION pos, pos2;
	CLayoutObject* pObject;
	CSize textExtent;

	BOOL bAsymmetric = DetermineChannelWidth(pTrimChannel, &fTrim1, &fTrim2);	//sets also pTrimChannel->fWidth

	switch (theApp.settings.m_iUnit)
	{
	case MILLIMETER: if (fabs(fTrim1) < 0.009)
						 return;
					 break;
	case CENTIMETER: if (fabs(fTrim1) < 0.0009)
						 return;
					 break;
	case INCH	   : if (fabs(fTrim1) < 0.0009)
						 return;
					 break;
	default		   : if (fabs(fTrim1) < 0.0009)
						 return;
					 break;
	}

	if (0)//bAsymmetric)
	{
		CString strTrim1;
		CString strTrim2;
		TruncateNumber(strTrim1, fTrim1);
		TruncateNumber(strTrim2, fTrim2);
		string.Format(_T("%s+%s"), strTrim1, strTrim2);
	}
	else
		TruncateNumber(string, fTrim1 + fTrim2);

	CalculateTrimChannelRect(pTrimChannel);

	overlapped = TRUE;
	j = 0;
	n_SizeList = pTrimChannel->pObjectList1->GetCount();
	pos		   = pTrimChannel->pObjectList1->GetHeadPosition();
	while (overlapped && j < n_SizeList)
	{
		pObject = (CLayoutObject*)pTrimChannel->pObjectList1->GetAt(pos);

		for (rect_pos = 0; (rect_pos < 3) && overlapped; rect_pos++)
		{
			DetermineMeasureTextRect(pTrimChannel, pObject, &TextRect, string, rect_pos);
			overlapped = FALSE;
			for (i = 0; i < *nChannelnumber; i++)
			{
				pos2 = pStringRects->FindIndex(i);
				if (pos2)
					if (RectOverlapped(&TextRect, (RECT*)pStringRects->GetAt(pos2)))
					{
						overlapped = TRUE;
						break;
					}
			}
		}
		pTrimChannel->pObjectList1->GetNext(pos);
		j++;
	}

	j = 0;
	n_SizeList = pTrimChannel->pObjectList2->GetCount();
	pos		   = pTrimChannel->pObjectList2->GetHeadPosition();
	while (overlapped &&  j < n_SizeList)
	{
		pObject = (CLayoutObject*)pTrimChannel->pObjectList2->GetAt(pos);

		for (rect_pos = 0; (rect_pos < 3) && overlapped; rect_pos++)
		{
			DetermineMeasureTextRect(pTrimChannel, pObject, &TextRect, string, rect_pos);
			overlapped = FALSE;
			for (i = 0; i < *nChannelnumber; i++)
			{
				pos2 = pStringRects->FindIndex(i);
				if (pos2)
					if (RectOverlapped(&TextRect, (RECT*)pStringRects->GetAt(pos2)))
					{
						overlapped = TRUE;
						break;
					}
			}
		}
		pTrimChannel->pObjectList2->GetNext(pos);
		j++;
	}

	rect_pos--;
	pStringRects->AddTail((CObject*)&TextRect);

	XMid = pTrimChannel->Rect.left	 + (pTrimChannel->Rect.right - pTrimChannel->Rect.left)	 / 2;
	YMid = pTrimChannel->Rect.bottom + (pTrimChannel->Rect.top	 - pTrimChannel->Rect.bottom)/ 2;

	ObjectWidth  = (long)(pObject->GetRight() - pObject->GetLeft());
	ObjectHeight = (long)(pObject->GetTop() - pObject->GetBottom());
	switch (rect_pos)
	{
		case 1: XPos = WorldToLP(pObject->GetLeft()	  + ObjectWidth/4);
				YPos = WorldToLP(pObject->GetBottom() + ObjectHeight/4);
				break;
		case 0: XPos = WorldToLP(pObject->GetLeft()	  + ObjectWidth/2);
				YPos = WorldToLP(pObject->GetBottom() + ObjectHeight/2);
				break;
		case 2: XPos = WorldToLP(pObject->GetRight() - ObjectWidth/4);
				YPos = WorldToLP(pObject->GetTop()	 - ObjectHeight/4);
				break;
	}

	CSize sizeFont(CLayout::TransformFontSize(pDC, 16), CLayout::TransformFontSize(pDC, 16));
	//pDC->DPtoLP(&sizeFont);
	CFont font, *pOldFont;
	font.CreateFont((int)(sizeFont.cy), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	//if (pDC->IsPrinting())
		pOldFont = pDC->SelectObject(&font);
	//else
	//	pOldFont = (CFont*)pDC->SelectStockObject(ANSI_VAR_FONT);
	CPoint	  hotSpot;
	pDC->LPtoDP(&sizeFont);
	const int nArrowLen = sizeFont.cx;//(pDC->IsPrinting()) ? nFontSize/100 : 10;
	COLORREF  crArrow	= LIGHTBLUE;
	switch (pTrimChannel->n_Direction)
	{
		case HORIZONTAL:if (pTrimChannel->Rect.top - pTrimChannel->Rect.bottom > WorldToLP(30))
						{
							hotSpot = CPoint(XPos, pTrimChannel->Rect.bottom); pDC->LPtoDP(&hotSpot, 1);
							DrawArrow(pDC, BOTTOM, hotSpot, nArrowLen, BLACK, crArrow);
							hotSpot = CPoint(XPos, pTrimChannel->Rect.top);	pDC->LPtoDP(&hotSpot, 1);
							DrawArrow(pDC, TOP,	   hotSpot, nArrowLen, BLACK, crArrow);
						}
						else
						{
							hotSpot = CPoint(XPos, pTrimChannel->Rect.bottom);	pDC->LPtoDP(&hotSpot, 1);
							DrawArrow(pDC, TOP,	   hotSpot, nArrowLen, BLACK, crArrow);
							hotSpot = CPoint(XPos, pTrimChannel->Rect.top);	pDC->LPtoDP(&hotSpot, 1);
							DrawArrow(pDC, BOTTOM, hotSpot, nArrowLen, BLACK, crArrow);
						}
						textExtent = pDC->GetTextExtent(string);
						XTextPos = XPos;
						YTextPos = YMid  - (textExtent.cy/2);
//						if (pTrimChannel->Rect.top - pTrimChannel->Rect.bottom > textExtent.cy)
//							XTextPos = XPos - (textExtent.cx/2);
//						else
//							XTextPos = XPos;
						break;
	  case VERTICAL   : if (pTrimChannel->Rect.right - pTrimChannel->Rect.left > WorldToLP((float)(string.GetLength() * 8 + 22)))
						{
							hotSpot = CPoint(pTrimChannel->Rect.left,  YPos); pDC->LPtoDP(&hotSpot, 1);
							DrawArrow(pDC, LEFT,  hotSpot, nArrowLen, BLACK, crArrow);
							hotSpot = CPoint(pTrimChannel->Rect.right, YPos); pDC->LPtoDP(&hotSpot, 1);
							DrawArrow(pDC, RIGHT, hotSpot, nArrowLen, BLACK, crArrow);
						}
						else
						{
							hotSpot = CPoint(pTrimChannel->Rect.left,  YPos); pDC->LPtoDP(&hotSpot, 1);
							DrawArrow(pDC, RIGHT, hotSpot, nArrowLen, BLACK, crArrow);
							hotSpot = CPoint(pTrimChannel->Rect.right, YPos); pDC->LPtoDP(&hotSpot, 1);
							DrawArrow(pDC, LEFT,  hotSpot, nArrowLen, BLACK, crArrow);
						}
						textExtent = pDC->GetTextExtent(string);
						XTextPos = XMid - (textExtent.cx/2);
						YTextPos = YPos;
//						if (pTrimChannel->Rect.right - pTrimChannel->Rect.left > textExtent.cx)
//							YTextPos = YPos - textExtent.cy/2;
//						else
//							YTextPos = YPos;
						break;
	}

	CSize sizeDistance(sizeFont.cx/2, sizeFont.cy/2);	//(Pixel2MM(pDC, 5), Pixel2MM(pDC, 5));
	pDC->DPtoLP(&sizeDistance);

	RECT  outputRect;
	outputRect.left	  = XTextPos + sizeDistance.cx;
	outputRect.right  = outputRect.left   + textExtent.cx;
	outputRect.bottom = YTextPos + sizeDistance.cy;
	outputRect.top	  = outputRect.bottom + textExtent.cy;

	COLORREF crOldColor   = pDC->SetTextColor(BLACK);
	int		 nOldBkMode	  = pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor = pDC->SetBkColor(WHITE);
	CString strValue;
	strValue.Format(_T(" %s "), string);
	pDC->DrawText(strValue, &outputRect, DT_NOCLIP|DT_SINGLELINE|DT_LEFT|DT_BOTTOM);
	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldColor);
	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);

	font.DeleteObject();
}

void CPrintSheetView::DetermineMeasureTextRect(CTrimChannel *pTrimChannel, CLayoutObject* pObject, RECT *rect, CString string, int pos)
{
	int xm, ym, width, height;
	LPCRECT pChannelRect = (LPCRECT)pTrimChannel->Rect;

	switch (pTrimChannel->n_Direction)
	{
	case HORIZONTAL:width = (int)(pObject->GetRight() - pObject->GetLeft());
					switch (pos)
					{
						case 1: xm = (int)(pObject->GetLeft() + width/6); break;
						case 0: xm = (int)(pObject->GetLeft() + width/2); break;
						case 2: xm = (int)(pObject->GetRight() - width/6); break;
					}
					ym = pChannelRect->bottom + (pChannelRect->top - pChannelRect->bottom)/2;
					rect->left	 = xm - (_tcslen(string)/2) * 8; 
					rect->bottom = pChannelRect->bottom - 11;
					rect->right	 = rect->left + string.GetLength() * 8; 
					rect->top	 = pChannelRect->top + 11;
					break;
	case VERTICAL : xm = pChannelRect->left + (pChannelRect->right - pChannelRect->left)/2;
					height = (int)(pObject->GetTop() - pObject->GetBottom());
					switch (pos)
					{
						case 1: ym = (int)(pObject->GetBottom() + height/6); break;
						case 0: ym = (int)(pObject->GetBottom() + height/2); break;
						case 2: ym = (int)(pObject->GetTop() - height/6); break;
					}
					rect->left	 = pChannelRect->left - 11; 
					rect->bottom = ym - 5;
					rect->right	 = pChannelRect->right + 11; 
					rect->top	 = ym + 5;
					break;
	}
}

BOOL CPrintSheetView::RectOverlapped(RECT *pRect1, RECT *pRect2)
{
	if ((pRect1->right < pRect2->left) || (pRect1->left > pRect2->right))
		return(FALSE);
	else
		if ((pRect1->top < pRect2->bottom) || (pRect1->bottom > pRect2->top))
			return(FALSE);
		else
			return(TRUE);
}

void CPrintSheetView::TruncateNumber(CString& string, float fValue, BOOL bShowUnit)
{
	switch (theApp.settings.m_iUnit)
	{
	case MILLIMETER: string.Format(_T("%.2f"), fValue);
					 break;
	case CENTIMETER: string.Format(_T("%.3f"), fValue/10.0f);
					 break;
	case INCH	   : string.Format(_T("%.3f"), fValue/25.4f);
					 break;
	default		   : string.Format(_T("%.3f"), fValue);
					 break;
	}

	int	   i = string.GetLength() - 1;
	LPTSTR p = string.GetBuffer(string.GetLength() + 1);

	while  (p[i] == '0')	i--;
	if    ((p[i] == '.') || (p[i] == ',') )	i--;
	p[i + 1] = '\0';

	string.ReleaseBuffer();

	switch (theApp.settings.m_iUnit)
	{
	case MILLIMETER: if (bShowUnit) 
						 string += _T(" mm");
					 break;
	case CENTIMETER: if (bShowUnit) 
						 string += _T(" cm");
					 break;
	case INCH	   : if (bShowUnit) 
						 string += _T(" in");
					 break;
	default		   : if (bShowUnit) 
						 string += _T(" mm");
					 break;
	}
}

BOOL CPrintSheetView::MeasurementsVisibleInView(CDC* pDC, CLayout* pActLayout, UINT nActLayoutSide)
{
	CRect		  rect;
	CPressDevice* pPressDevice;

	//if (ShowPlate())
	if (1)
	{
		switch(nActLayoutSide)
		{
		case FRONTSIDE: pPressDevice = pActLayout->m_FrontSide.GetPressDevice();
						break;
		case BACKSIDE : pPressDevice = pActLayout->m_BackSide.GetPressDevice();
						break;
		}

		if (pPressDevice == NULL)
			return FALSE;

		rect.left	= 0;
		rect.top	= WorldToLP(pPressDevice->m_fPlateHeight);
		rect.right	= WorldToLP(pPressDevice->m_fPlateWidth);
		rect.bottom	= 0;
		if (pDC->RectVisible(rect))
			return TRUE;
		else
			return FALSE;
	}
	else
	{
		switch (nActLayoutSide)
		{
		case FRONTSIDE : rect.left	 = WorldToLP(pActLayout->m_FrontSide.m_fPaperLeft);
						 rect.top	 = WorldToLP(pActLayout->m_FrontSide.m_fPaperTop);
						 rect.right	 = WorldToLP(pActLayout->m_FrontSide.m_fPaperRight);
						 rect.bottom = WorldToLP(pActLayout->m_FrontSide.m_fPaperBottom);
						 break;
		case BACKSIDE :	 rect.left	 = WorldToLP(pActLayout->m_BackSide.m_fPaperLeft);
						 rect.top	 = WorldToLP(pActLayout->m_BackSide.m_fPaperTop);
						 rect.right	 = WorldToLP(pActLayout->m_BackSide.m_fPaperRight);
						 rect.bottom = WorldToLP(pActLayout->m_BackSide.m_fPaperBottom);
						 break;
		}
		if (pDC->RectVisible(rect))
			return TRUE;
		else
			return FALSE;
	}
}
