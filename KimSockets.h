#pragma once


#include "afxsock.h"


// CClientSocket

class CClientSocket : public CAsyncSocket
{
public:
	CClientSocket();
	virtual ~CClientSocket();

public:
	BOOL m_bConnectionRefused;

public:
	int CommandToServer(int nCmd, int nParam1 = 0, int nParam2 = 0);
	int	SendMessageToServer(const CString& strMsg);


public:
	virtual void OnReceive(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnConnect(int nErrorCode); 
};





