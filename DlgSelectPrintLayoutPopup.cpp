// DlgSelectPrintLayoutPopup.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgSelectPrintLayoutPopup.h"
#include "GraphicComponent.h"
#include "DlgSelectFoldSchemePopup.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetNavigationView.h"
#include "PrintSheetPlanningView.h"
#include "NewPrintSheetFrame.h"



// CDlgSelectPrintLayoutPopup-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgSelectPrintLayoutPopup, CDialog)

CDlgSelectPrintLayoutPopup::CDlgSelectPrintLayoutPopup(CWnd* pParent /*=NULL*/, BOOL bRunModal /* = FALSE*/)
	: CDialog(CDlgSelectPrintLayoutPopup::IDD, pParent)
{
	m_smallFont.CreateFont (11, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_pPrintLayoutImageList = new CImageList();
	m_pParent				= pParent;
	m_nNup					= 0; 
	m_bLocked				= FALSE;
	m_bRunModal				= bRunModal;
	m_bAllowRotation		= FALSE;
	m_bShowStandards		= FALSE;
	m_ptInitialPos			= CPoint(0,0);
}

CDlgSelectPrintLayoutPopup::~CDlgSelectPrintLayoutPopup()
{
	m_smallFont.DeleteObject();
	delete m_pPrintLayoutImageList;
}

void CDlgSelectPrintLayoutPopup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PD_NUP_COMBO, m_nUpCombo);
	DDX_Control(pDX, IDC_PD_PRINTTYPE_COMBO, m_printTypeCombo);
	DDX_CBIndex(pDX, IDC_PD_PRINTTYPE_COMBO, m_nPrintingType);
	DDX_Control(pDX, IDC_PRINTLAYOUTLIST, m_printLayoutList);
	DDX_Check(pDX, IDC_PD_ALLOW_ROTATION, m_bAllowRotation);
	DDX_Check(pDX, IDC_PD_SHOW_STANDARDS, m_bShowStandards);
}


BEGIN_MESSAGE_MAP(CDlgSelectPrintLayoutPopup, CDialog)
	ON_WM_ACTIVATE()
	ON_CBN_SELCHANGE(IDC_PD_NUP_COMBO, OnCbnSelchangeNUpCombo)
	ON_CBN_SELCHANGE(IDC_PD_PRINTTYPE_COMBO, OnCbnSelchangePrintTypeCombo)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_PRINTLAYOUTLIST, &CDlgSelectPrintLayoutPopup::OnNMCustomdrawPrintLayoutlist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PRINTLAYOUTLIST, &CDlgSelectPrintLayoutPopup::OnLvnItemchangedPrintLayoutlist)
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_PD_ALLOW_ROTATION, &CDlgSelectPrintLayoutPopup::OnBnClickedPdAllowRotation)
	ON_BN_CLICKED(IDC_PD_SHOW_STANDARDS, &CDlgSelectPrintLayoutPopup::OnBnClickedPdShowStandards)
	ON_WM_DESTROY()
END_MESSAGE_MAP()



// CDlgSelectPrintLayoutPopup-Meldungshandler

BOOL CDlgSelectPrintLayoutPopup::OnEraseBkgnd(CDC* pDC)
{
	return CDialog::OnEraseBkgnd(pDC);

	//CRect rcFrame;
	//GetClientRect(rcFrame);

	//rcFrame.right -= 5; rcFrame.bottom -= 5;

	//HTHEME hTheme = NULL;
	//if (theApp.m_nOSVersion == CImpManApp::WinXP)
	//	hTheme = theApp._OpenThemeData(GetParent(), _T("WINDOW"));
	//if (hTheme)
	//	DrawThemeBackgroundEx(hTheme, pDC->GetSafeHdc(), WP_DIALOG, 0, &rcFrame, 0);	
	//theApp._CloseThemeData(hTheme);

	//CPen	pen(PS_SOLID, 1, MIDDLEGRAY);
	//CBrush  brush(::GetSysColor(CTLCOLOR_DLG));
	//CPen*	pOldPen	  = pDC->SelectObject(&pen);
	//CBrush* pOldBrush = (hTheme) ? (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH) : pDC->SelectObject(&brush);
	//pDC->Rectangle(rcFrame);
	//pDC->SelectObject(pOldPen);
	//pDC->SelectObject(pOldBrush);
	//pen.DeleteObject();
	//brush.DeleteObject();

	//rcFrame.right -= 1; rcFrame.bottom -= 1;
	//CGraphicComponent gp;
	//gp.DrawShadowFrame(pDC, rcFrame, 6);

	//return TRUE;
}

BOOL CDlgSelectPrintLayoutPopup::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_ptInitialPos != CPoint(0,0))
		SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	if ( ! m_pPrintLayoutImageList->m_hImageList)
	{
		CBitmap bitmap;
		m_pPrintLayoutImageList->Create(60, 120, ILC_COLOR8, 1, 0);	// List not to grow
		bitmap.LoadBitmap(IDB_BKSCHEMELIST);
		m_pPrintLayoutImageList->Add(&bitmap, BLACK);
		m_printLayoutList.SetImageList(m_pPrintLayoutImageList, LVSIL_SMALL);
		bitmap.DeleteObject();
	}

	InitNUp(TRUE);
	InitPrintLayoutList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgSelectPrintLayoutPopup::InitNUp(BOOL bInitialize)
{
	int nWorkStyle = WORK_AND_BACK;
	int nPrintingType = 0;
	if (m_foldSheet.IsEmpty())
	{
		//CImpManDoc*  pDoc = CImpManDoc::GetDoc();
		//if ( ! pDoc)
		//	return;
		//nWorkStyle = pDoc->m_boundProducts.GetProductPart(GetActProductPartIndex()).GetProductionProfile().m_press.m_nWorkStyle;
	}
	else
	if (GetActLayout())
		nWorkStyle = GetActLayout()->m_nProductType;

	//switch(nWorkStyle)
	//{
	//case WORK_AND_BACK:		nPrintingType = 0;	break;
	//case WORK_AND_TURN:		nPrintingType = 1;	break;
	//case WORK_AND_TUMBLE:		nPrintingType = 2;	break;
	//case WORK_SINGLE_SIDE:	nPrintingType = 3;	break;
	//}
	//m_printTypeCombo.SetCurSel(nPrintingType);

	m_nUpCombo.ResetContent();
	if (m_foldSheet.IsEmpty() || ! GetActPrintSheet())
	{
		m_nNup = 1;
		CString string, strNUp; strNUp.LoadString(IDS_NUP);
		string.Format(_T("1 %s"), strNUp);
		m_nUpCombo.AddString(string);
		return;
	}

	if (bInitialize)
		m_nNup = 1;//m_pFoldSheet->GetNUpPrintLayout(m_pPrintSheet, nLayerIndex);
	
	int	nTotalNUp = GetActLayout()->GetMaxNUp(*GetActPrintSheet(), &m_foldSheet, TRUE);//m_bAllowRotation);

	CString string, strNUp; strNUp.LoadString(IDS_NUP);
	for (int i = 0; i < nTotalNUp; i++)
	{
		string.Format(_T("%d %s"), i + 1, strNUp);
		m_nUpCombo.InsertString(i, string);
	}

	m_nUpCombo.SetCurSel(m_nNup - 1);
}

void CDlgSelectPrintLayoutPopup::InitPrintLayoutList()
{
	UpdateData(TRUE);

	if (m_bShowStandards)
	{
		InitStandardPrintLayoutList();
		return;
	}

	for (int i = 0; i < m_sampleList.GetSize(); i++)
	{
		if (m_sampleList[i].pPrintSheet)
			delete m_sampleList[i].pPrintSheet;
		if (m_sampleList[i].pLayout)
			delete m_sampleList[i].pLayout;
	}
	m_sampleList.RemoveAll();
	m_printLayoutList.DeleteAllItems();

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! GetActPrintSheet() || ! GetActLayout() || m_foldSheet.IsEmpty())
		return;

	m_bLocked = TRUE;

	BeginWaitCursor();

	GetActLayout()->m_nPrintingProfileRef = GetActLayout()->GetIndex();	// needed later on in GetPressDevice()

	CPrintSheet basePrintSheet = *GetActPrintSheet();
	CLayout		baseLayout	   = *GetActLayout();
	baseLayout.m_FrontSide.RemoveMarkObjects(-1, TRUE);
	baseLayout.m_BackSide.RemoveMarkObjects(-1, TRUE);
	baseLayout.m_markList.RemoveAll(FALSE);

	CFoldSheet* pFoldSheet = GetActFoldSheet();
	if (pFoldSheet)
	{
		for (int nComponentRefIndex = 0; nComponentRefIndex < basePrintSheet.m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
		{
			if (basePrintSheet.GetFoldSheet(nComponentRefIndex) == pFoldSheet)
			{
				baseLayout.RemoveFoldSheetLayer(nComponentRefIndex, NULL, FALSE);
				if (basePrintSheet.m_FoldSheetLayerRefs.GetSize())
					basePrintSheet.m_FoldSheetLayerRefs.RemoveAt(nComponentRefIndex);
				break;
			}
		}
	}

	baseLayout.SetObjectsStatusAll(FALSE);

	PRINTLAYOUT pl1  = {new (CPrintSheet), new (CLayout)};
	*pl1.pPrintSheet = basePrintSheet;
	*pl1.pLayout	 = baseLayout;
	if (pl1.pLayout->MakeNUp(FALSE, *pl1.pPrintSheet, &m_foldSheet, 0, m_nNup, FALSE, NULL))//m_bAllowRotation))
	{
		pl1.pLayout->m_markList.Copy(GetActLayout()->m_markList);
		m_sampleList.Add(pl1);
	}

	LV_ITEM item;
	item.mask		= LVIF_PARAM | LVIF_IMAGE | LVIF_STATE;
	item.stateMask	= LVIS_SELECTED;
	item.state		= 0;
	item.cchTextMax = MAX_TEXT;
	item.iItem		= m_printLayoutList.GetItemCount(); 
	item.iSubItem	= 0;
	item.lParam		= 0;//(LPARAM)&pl;
	item.iImage		= 0;
	m_printLayoutList.InsertItem(&item);

	PRINTLAYOUT pl1r  = {new (CPrintSheet), new (CLayout)};
	*pl1r.pPrintSheet = basePrintSheet;
	*pl1r.pLayout	  = baseLayout;
	pl1r.pLayout->m_FrontSide.RemoveMarkObjects(-1, TRUE);
	pl1r.pLayout->m_BackSide.RemoveMarkObjects(-1, TRUE);
	pl1r.pLayout->m_markList.RemoveAll(FALSE);
	if (pl1r.pLayout->MakeNUp(FALSE, *pl1r.pPrintSheet, &m_foldSheet, 0, m_nNup, 2, NULL) > 1)//m_bAllowRotation))
	{
		pl1r.pLayout->m_markList.Copy(GetActLayout()->m_markList);
		m_sampleList.Add(pl1r);
		item.iItem = m_printLayoutList.GetItemCount(); 
		m_printLayoutList.InsertItem(&item);
	}

	INT_PTR	nNumUnImposedPages = 0;
	pDoc->m_PageTemplateList.GetPageRange(GetActFoldSheet(), GetActLayout(), &nNumUnImposedPages);		// get num imposed pages based on act foldsheet and act layout
	nNumUnImposedPages += pDoc->m_PageTemplateList.GetNumUnimposedPages(GetActProductPartIndex());
	int nNumPages		= ( ! m_foldSheet.IsEmpty()) ? m_foldSheet.GetNumPages() : 1;
	int nNumSheets		= (nNumPages > 0) ? (nNumUnImposedPages / nNumPages) : 1;

	PRINTLAYOUT pl2  = {new (CPrintSheet), new (CLayout)};
	*pl2.pPrintSheet = basePrintSheet;
	*pl2.pLayout	 = baseLayout;
	pl2.pLayout->m_FrontSide.RemoveMarkObjects(-1, TRUE);
	pl2.pLayout->m_BackSide.RemoveMarkObjects(-1, TRUE);
	pl2.pLayout->m_markList.RemoveAll(FALSE);
	if (pl2.pLayout->MakeSequential(FALSE, *pl2.pPrintSheet, &m_foldSheet, 0, nNumSheets, m_nNup, FALSE, NULL))//m_bAllowRotation))
	{
		pl2.pLayout->m_markList.Copy(GetActLayout()->m_markList);
		m_sampleList.Add(pl2);
		item.iItem		= m_printLayoutList.GetItemCount(); 
		m_printLayoutList.InsertItem(&item);
	}

	//PRINTLAYOUT pl2r  = {new (CPrintSheet), new (CLayout)};
	//*pl2r.pPrintSheet = basePrintSheet;
	//*pl2r.pLayout	  = baseLayout;
	//pl2r.pLayout->m_FrontSide.RemoveMarkObjects(-1, TRUE);
	//pl2r.pLayout->m_BackSide.RemoveMarkObjects(-1, TRUE);
	//pl2r.pLayout->m_markList.RemoveAll(FALSE);
	//if (pl2r.pLayout->MakeSequential(*pl2r.pPrintSheet, &m_foldSheet, nNumSheets, m_nNup, TRUE, NULL) > 1)//m_bAllowRotation))
	//{
	//	pl2r.pLayout->m_markList.Copy(GetActLayout()->m_markList);
	//	m_sampleList.Add(pl2r);
	//	item.iItem		= m_printLayoutList.GetItemCount(); 
	//	m_printLayoutList.InsertItem(&item);
	//}

	PRINTLAYOUT pl3  = {new (CPrintSheet), new (CLayout)};
	*pl3.pPrintSheet = basePrintSheet;
	*pl3.pLayout	 = baseLayout;
	pl3.pLayout->m_FrontSide.RemoveMarkObjects(-1, TRUE);
	pl3.pLayout->m_BackSide.RemoveMarkObjects(-1, TRUE);
	pl3.pLayout->m_markList.RemoveAll(FALSE);
	if (pl3.pLayout->MakeWorkAndTurn(FALSE, *pl3.pPrintSheet, &m_foldSheet, 0, m_nNup, FALSE, NULL))//m_bAllowRotation))
	{
		pl3.pLayout->m_markList.Copy(GetActLayout()->m_markList);
		m_sampleList.Add(pl3);
		item.iItem		= m_printLayoutList.GetItemCount(); 
		m_printLayoutList.InsertItem(&item);
	}

	PRINTLAYOUT pl3r  = {new (CPrintSheet), new (CLayout)};
	*pl3r.pPrintSheet = basePrintSheet;
	*pl3r.pLayout	  = baseLayout;
	pl3r.pLayout->m_FrontSide.RemoveMarkObjects(-1, TRUE);
	pl3r.pLayout->m_BackSide.RemoveMarkObjects(-1, TRUE);
	pl3r.pLayout->m_markList.RemoveAll(FALSE);
	if (pl3r.pLayout->MakeWorkAndTurn(FALSE, *pl3r.pPrintSheet, &m_foldSheet, 0, m_nNup, TRUE, NULL) > 1)//m_bAllowRotation))
	{
		pl3r.pLayout->m_markList.Copy(GetActLayout()->m_markList);
		m_sampleList.Add(pl3r);
		item.iItem		= m_printLayoutList.GetItemCount(); 
		m_printLayoutList.InsertItem(&item);
	}

	PRINTLAYOUT pl4  = {new (CPrintSheet), new (CLayout)};
	*pl4.pPrintSheet = basePrintSheet;
	*pl4.pLayout	 = baseLayout;
	pl4.pLayout->m_FrontSide.RemoveMarkObjects(-1, TRUE);
	pl4.pLayout->m_BackSide.RemoveMarkObjects(-1, TRUE);
	pl4.pLayout->m_markList.RemoveAll(FALSE);
	if (pl4.pLayout->MakeWorkAndTumble(FALSE, *pl4.pPrintSheet, &m_foldSheet, 0, m_nNup, FALSE, NULL))//m_bAllowRotation))
	{
		pl4.pLayout->m_markList.Copy(GetActLayout()->m_markList);
		m_sampleList.Add(pl4);
		item.iItem		= m_printLayoutList.GetItemCount(); 
		m_printLayoutList.InsertItem(&item);
	}

	PRINTLAYOUT pl4r  = {new (CPrintSheet), new (CLayout)};
	*pl4r.pPrintSheet = basePrintSheet;
	*pl4r.pLayout	  = baseLayout;
	pl4r.pLayout->m_FrontSide.RemoveMarkObjects(-1, TRUE);
	pl4r.pLayout->m_BackSide.RemoveMarkObjects(-1, TRUE);
	pl4r.pLayout->m_markList.RemoveAll(FALSE);
	if (pl4r.pLayout->MakeWorkAndTumble(FALSE, *pl4r.pPrintSheet, &m_foldSheet, 0, m_nNup, TRUE, NULL) > 1)//m_bAllowRotation))
	{
		pl4r.pLayout->m_markList.Copy(GetActLayout()->m_markList);
		m_sampleList.Add(pl4r);
		item.iItem		= m_printLayoutList.GetItemCount(); 
		m_printLayoutList.InsertItem(&item);
	}

	//if (m_printLayoutList.GetItemCount() > 0)
	//{
	//	m_printLayoutList.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
	//	m_printLayoutList.EnsureVisible(0, FALSE);
	//	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	//	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));	
	//}

	EndWaitCursor();

	m_bLocked = FALSE;
}

void CDlgSelectPrintLayoutPopup::InitStandardPrintLayoutList()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! GetActPrintSheet() || ! GetActLayout() || m_foldSheet.IsEmpty())
		return;

	m_bLocked = TRUE;

	for (int i = 0; i < m_sampleList.GetSize(); i++)
	{
		if (m_sampleList[i].pPrintSheet)
			delete m_sampleList[i].pPrintSheet;
		if (m_sampleList[i].pLayout)
			delete m_sampleList[i].pLayout;
	}
	m_sampleList.RemoveAll();
	m_printLayoutList.DeleteAllItems();

	WIN32_FIND_DATA findData;
	BOOL			bFilesExist = FALSE;	

	HANDLE  hfileSearch = FindFirstFile(CString(theApp.settings.m_szStandardsDir) + _T("\\*.lay"), &findData);

	m_bLocked = TRUE;

	int nSel = -1;
	BOOL bFound = FALSE;
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		CString	strBuffer	= findData.cFileName;
		bFilesExist			= TRUE;
		int	i = 0;
		do 
		{
			strBuffer = findData.cFileName;
			if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
			{
				if ( ! (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
				{
					CString strExt = strBuffer.Right(4);
					strExt.MakeLower();
					if (strExt == _T(".lay"))
					{
						//if (SchemeIsRelevant(foldSheet, nNumPagesToImpose))
						{
							PRINTLAYOUT pl1 = {new (CPrintSheet), new (CLayout)};

							CFile file;
							CFileException fileException;
							CString strFullpath = CString(theApp.settings.m_szStandardsDir) + _T("\\") + findData.cFileName;
							if (file.Open(strFullpath, CFile::modeRead, &fileException))
							{
								CArchive archive(&file, CArchive::load);
								CPageTemplateList markTemplateList;
								CPageSourceList	  markSourceList;
								CPressDevice	  pressDeviceFront, pressDeviceBack;

								SerializeElements(archive, pl1.pLayout, 1);
								markTemplateList.Serialize(archive);
								markSourceList.Serialize(archive);

								// load press device(s)
								SerializeElements(archive, &pressDeviceFront, 1);
								SerializeElements(archive, &pressDeviceBack,  1);

								for (int nComponentRefIndex = 0; nComponentRefIndex < pl1.pLayout->m_FoldSheetLayerRotation.GetSize(); nComponentRefIndex++)
								{
									int nLayerIndex = 0;
									CFoldSheetLayerRef layerRef(0, nLayerIndex); 
									pl1.pPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);
								}

								m_sampleList.Add(pl1);

								LV_ITEM item;
								item.mask		= LVIF_PARAM | LVIF_IMAGE | LVIF_STATE;
								item.stateMask	= LVIS_SELECTED;
								item.state		= 0;
								item.cchTextMax = MAX_TEXT;
								item.iItem		= m_printLayoutList.GetItemCount(); 
								item.iSubItem	= 0;
								item.lParam		= 0;//(LPARAM)&pl;
								item.iImage		= 0;
								m_printLayoutList.InsertItem(&item);
							}
							i++;
						}
					}
				}
			}
		}
		while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);

	//m_printLayoutList.EnsureVisible(nSel, FALSE);

	m_bLocked = FALSE;
}

CPrintSheet* CDlgSelectPrintLayoutPopup::GetActPrintSheet()
{
	CDlgSelectFoldSchemePopup* pParent = (CDlgSelectFoldSchemePopup*)GetParent();
	if (pParent)
		return pParent->GetActPrintSheet();

	return NULL;
}

CLayout* CDlgSelectPrintLayoutPopup::GetActLayout()
{
	CDlgSelectFoldSchemePopup* pParent = (CDlgSelectFoldSchemePopup*)GetParent();
	if (pParent)
		return pParent->GetActLayout();

	return NULL;
}

CFoldSheet* CDlgSelectPrintLayoutPopup::GetActFoldSheet()
{
	CDlgSelectFoldSchemePopup* pParent = (CDlgSelectFoldSchemePopup*)GetParent();
	if (pParent)
		return pParent->GetActFoldSheet();

	return NULL;
}

int CDlgSelectPrintLayoutPopup::GetActProductPartIndex()
{
	CDlgSelectFoldSchemePopup* pParent = (CDlgSelectFoldSchemePopup*)GetParent();
	if (pParent)
		return pParent->GetActProductPartIndex();

	return -1;
}

void CDlgSelectPrintLayoutPopup::OnCbnSelchangeNUpCombo()
{
	UpdateData(TRUE);

	int nSel = m_nUpCombo.GetCurSel();
	if (nSel == CB_ERR)
		return; 

	m_nNup = nSel + 1;

	InitPrintLayoutList();
}

void CDlgSelectPrintLayoutPopup::OnCbnSelchangePrintTypeCombo()
{
	UpdateData(TRUE);

	//if ( ! GetActLayout())
	//	return;
	//switch (m_nPrintingType)
	//{
	//case 0: GetActLayout()->SetWorkStyle(WORK_AND_BACK);	
	//		break;

	//case 1: GetActLayout()->SetWorkStyle(WORK_AND_TURN);	
	//		break;
	//case 2: GetActLayout()->SetWorkStyle(WORK_AND_TUMBLE);	
	//		break;
	//case 3: GetActLayout()->SetWorkStyle(WORK_SINGLE_SIDE);	
	//		break;
	//}

	//CPrintSheetView*		 pPrintSheetView		 = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	//CPrintSheetPlanningView* pPrintSheetPlanningView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	//CPageListView*			 pPageListView			 = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	//if (pPrintSheetView)
	//	pPrintSheetView->m_DisplayList.Invalidate();
	//if (pPrintSheetPlanningView)
	//	pPrintSheetPlanningView->m_DisplayList.Invalidate();
	//if (pPageListView)
	//	pPageListView->m_DisplayList.Invalidate();

	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView));
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	//theApp.OnUpdateView(RUNTIME_CLASS(CPageListView));

	InitPrintLayoutList();
}

void CDlgSelectPrintLayoutPopup::OnNMCustomdrawPrintLayoutlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);
			CPrintSheet* pPrintSheet = (lvitem.iItem < m_sampleList.GetSize()) ? m_sampleList[lvitem.iItem].pPrintSheet : NULL;
			CLayout*	 pLayout	 = (lvitem.iItem < m_sampleList.GetSize()) ? m_sampleList[lvitem.iItem].pLayout		: NULL;
			if ( ! pLayout || ! pPrintSheet)
				break;

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			float fPaperWidth  = (GetActLayout()) ? GetActLayout()->m_FrontSide.m_fPaperWidth  : 0.0f;
			float fPaperHeight = (GetActLayout()) ? GetActLayout()->m_FrontSide.m_fPaperHeight : 0.0f;

			CRect rcLayout = rcAll;
			rcLayout.DeflateRect(5, 5); rcLayout.bottom -= 15;
			rcLayout = pLayout->Draw(pDC, rcLayout, pPrintSheet, NULL, TRUE, FALSE, FALSE, &m_foldSheet);		

			CRect rcLabel;
			//ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			//if (rcLabel.IsRectEmpty())
			//	break;

			rcLabel = rcLayout; rcLabel.top = rcLayout.bottom + 5; rcLabel.bottom = rcAll.bottom; 
			COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

			CFont* pOldFont = pDC->SelectObject(&m_smallFont);
			pDC->DrawText(pLayout->m_strLayoutName, rcLabel, DT_VCENTER | DT_CENTER | DT_END_ELLIPSIS);
			rcLabel.OffsetRect(0, 15);

			pDC->SetTextColor(crOldTextColor);
			pDC->SelectObject(pOldFont);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}

void CDlgSelectPrintLayoutPopup::OnLvnItemchangedPrintLayoutlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	if (m_bLocked)
		return;

    HWND hWndListCtrl = pNMHDR->hwndFrom;
	if (pNMLV->iItem < 0)
		return;
	if (pNMLV->uNewState == pNMLV->uOldState)
		return;
	if ( ! (pNMLV->uNewState & LVIS_SELECTED) )
		return;

	LVITEM lvitem;
	lvitem.mask	= LVIF_PARAM;
	lvitem.iItem = pNMLV->iItem;
	lvitem.iSubItem = 0;
	ListView_GetItem(hWndListCtrl, &lvitem);
	CPressDevice* pPressDevice = (CPressDevice*)lvitem.lParam;

	//m_bLocked = TRUE;
	//if (m_hWnd)
	//	if (m_bRunModal)
	//	{
	//		EndDialog(-lvitem.iItem);
	//	}
	//	else
	//		DestroyWindow();

	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;
	//CDlgSelectFoldSchemePopup* pParent = (CDlgSelectFoldSchemePopup*)GetParent();
	//if ( ! pParent)
	//	return;
	//CPrintSheetPlanningView* pPlanningView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));

	//CPrintSheet newPrintSheet	= *m_sampleList[lvitem.iItem].pPrintSheet;
	//CLayout		newLayout	    = *m_sampleList[lvitem.iItem].pLayout;
	//CFoldSheet  newFoldSheet	= *pParent->m_pFoldSheet;

	//CString strSequentially; strSequentially.LoadString(IDS_SEQUENTIALLY);
	//if (newLayout.m_strLayoutName.Find(strSequentially) >= 0)
	//{
	//	int nNumOnLayout   = newLayout.m_FoldSheetLayerRotation.GetSize();
	//	int nNumFoldSheets = (pParent->GetActFoldSheet()) ? pParent->GetActFoldSheet()->GetNumSamePrintLayout() : 0;
	//	if (nNumOnLayout > nNumFoldSheets)
	//		GetActLayout()->AddPrintSheetsBasedOn(nNumOnLayout - nNumFoldSheets);
	//}

	//newLayout.m_strLayoutName = (GetActLayout()) ? GetActLayout()->m_strLayoutName : _T("");
	//if (GetActLayout())
	//{
	//	newLayout.m_markList.RemoveAll();
	//	newLayout.m_markList.Copy(GetActLayout()->m_markList);
	//}

	//CFoldSheet* pLastAddedFoldSheet = pDoc->m_PrintSheetList.ReImposePrintSheetType(pParent->GetActFoldSheet(), &newFoldSheet, GetActPrintSheet(), newPrintSheet, newLayout);
	//if (GetActPrintSheet())
	//	GetActPrintSheet()->CalcTotalQuantity(TRUE);

	//pDoc->SetModifiedFlag();										

	//if (GetActLayout())
	//	GetActLayout()->AnalyzeMarks();

	//pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));	
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	//theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),	NULL, 0, NULL);
	//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	//theApp.UpdateView(RUNTIME_CLASS(CProductsView));	

	//if (pPlanningView)
	//{
	//	//pPlanningView->SelectComponentByFoldSheet(pLastAddedFoldSheet);
	//	pParent->m_pFoldSheet = pLastAddedFoldSheet;//pPlanningView->GetFoldSheet();
	//}

	//pParent->InitData();

	//if (pParent->GetActFoldSheet())
	//{
	//	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)pParent->m_pParent;
	//	if (pFrame)
	//	{
	//		CPrintComponentsView* pView = pFrame->GetPrintComponentsView();
	//		if (pView)
	//		{
	//			int				nFoldSheetIndex		 = pDoc->m_Bookblock.GetFoldSheetIndex(pParent->GetActFoldSheet());
	//			int				nFoldSheetLayerIndex = 0;
	//			CPrintComponent component			 = pDoc->m_printComponentsList.FindFoldSheetLayerComponent(nFoldSheetIndex, nFoldSheetLayerIndex); 
	//			pView->SetActComponent(component);
	//			CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	//			if (pNavigationView)
	//				pNavigationView->SetActPrintSheet(pLastAddedFoldSheet->GetPrintSheet(nFoldSheetLayerIndex));
	//		}
	//	}
	//}

	//pParent->UpdateViews();
}

void CDlgSelectPrintLayoutPopup::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	//if (nState == WA_INACTIVE)
	//{
	//	if ( ! m_bLocked)
	//		if (m_hWnd)
	//			if ( ! m_bRunModal)
	//			{
	//				m_bLocked = TRUE;
	//				DestroyWindow();
	//			}
	//			else
	//				OnCancel();
	//}
	//else
	//	m_bLocked = FALSE;
}

void CDlgSelectPrintLayoutPopup::OnBnClickedPdAllowRotation()
{
	UpdateData(TRUE);

	InitNUp();
	InitPrintLayoutList();
}

void CDlgSelectPrintLayoutPopup::OnBnClickedPdShowStandards()
{
	UpdateData(TRUE);

	int nShow = (m_bShowStandards) ? SW_HIDE : SW_SHOW;
	GetDlgItem(IDC_PD_NUP_COMBO)->ShowWindow(nShow);
	GetDlgItem(IDC_PD_PRINTTYPE_COMBO)->ShowWindow(SW_HIDE);//nShow);
	GetDlgItem(IDC_PD_ALLOW_ROTATION)->ShowWindow(nShow);

	//CString strStatic = (m_bShowStandards) ? _T("Standardlayouts") : _T("Vorschl�ge");
	//GetDlgItem(IDC_PD_LIST_STATIC)->SetWindowText(strStatic);

	InitNUp();
	InitPrintLayoutList();
}

void CDlgSelectPrintLayoutPopup::OnDestroy()
{
	CDialog::OnDestroy();

	m_pPrintLayoutImageList->DeleteImageList();
}

CLayout* CDlgSelectPrintLayoutPopup::GetSelectedLayout()
{
	POSITION pos = m_printLayoutList.GetFirstSelectedItemPosition();
	if ( ! pos)
		return NULL;

	int nSel = m_printLayoutList.GetNextSelectedItem(pos);
	if ( (nSel >= 0) && (nSel < m_sampleList.GetSize()) )
		return m_sampleList[nSel].pLayout;
	else
		return NULL;
}

CPrintSheet* CDlgSelectPrintLayoutPopup::GetSelectedPrintSheet()
{
	POSITION pos = m_printLayoutList.GetFirstSelectedItemPosition();
	if ( ! pos)
		return NULL;

	int nSel = m_printLayoutList.GetNextSelectedItem(pos);
	if ( (nSel >= 0) && (nSel < m_sampleList.GetSize()) )
		return m_sampleList[nSel].pPrintSheet;
	else
		return NULL;
}