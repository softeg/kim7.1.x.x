#if !defined(AFX_DLGSTOPOUTPUT_H__CC05F2F7_E3A7_4426_B3D7_5A2573FABF87__INCLUDED_)
#define AFX_DLGSTOPOUTPUT_H__CC05F2F7_E3A7_4426_B3D7_5A2573FABF87__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgStopOutput.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgStopOutput 

class CDlgStopOutput : public CDialog
{
// Konstruktion
public:
	CDlgStopOutput(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgStopOutput)
	enum { IDD = IDD_STOP_OUTPUT };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgStopOutput)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgStopOutput)
	afx_msg void OnStopOutputAll();
	afx_msg void OnStopOutputSheet();
	afx_msg void OnProceedOutput();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGSTOPOUTPUT_H__CC05F2F7_E3A7_4426_B3D7_5A2573FABF87__INCLUDED_
