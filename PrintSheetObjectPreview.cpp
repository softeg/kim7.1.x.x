// PrintSheetObjectPreview.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PrintSheetObjectPreview.h"
#include "InplaceEdit.h"	// needed by PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"


// CPrintSheetObjectPreview

IMPLEMENT_DYNCREATE(CPrintSheetObjectPreview, CFormView)

CPrintSheetObjectPreview::CPrintSheetObjectPreview()
	: CFormView(CPrintSheetObjectPreview::IDD)
{

}

CPrintSheetObjectPreview::~CPrintSheetObjectPreview()
{
}

void CPrintSheetObjectPreview::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPrintSheetObjectPreview, CFormView)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CPrintSheetObjectPreview-Diagnose

#ifdef _DEBUG
void CPrintSheetObjectPreview::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetObjectPreview::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrintSheetObjectPreview-Meldungshandler

void CPrintSheetObjectPreview::OnInitialUpdate()
{
	m_dlgObjectPreviewWindow.Create(IDD_OBJECT_PREVIEW_WINDOW, this);

	CFormView::OnInitialUpdate();

	CRect rcClient; GetClientRect(rcClient);
	SetScaleToFitSize(rcClient.Size());	//suppress scrollbars

	OnSize(0,0,0);
}

void CPrintSheetObjectPreview::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	if (m_dlgObjectPreviewWindow.m_hWnd)
	{
		m_dlgObjectPreviewWindow.LoadData();
		m_dlgObjectPreviewWindow.OnSize(0, 0, 0);
		//m_dlgObjectPreviewWindow.Invalidate();
		//m_dlgObjectPreviewWindow.UpdateWindow();
	}
}

void CPrintSheetObjectPreview::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if ( ! m_dlgObjectPreviewWindow.m_hWnd)
		return;

	CRect rcClientRect; GetClientRect(rcClientRect);
	CRect rcWindow;
	m_dlgObjectPreviewWindow.GetWindowRect(rcWindow);
	ScreenToClient(rcWindow);
	rcWindow.right  = rcWindow.left + rcClientRect.Width();
	rcWindow.bottom = rcWindow.top + rcClientRect.Height();
	m_dlgObjectPreviewWindow.MoveWindow(rcWindow);
}
