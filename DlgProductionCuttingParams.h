#pragma once


// CDlgProductionCuttingParams-Dialogfeld

class CDlgProductionCuttingParams : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionCuttingParams)

public:
	CDlgProductionCuttingParams(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionCuttingParams();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTION_CUTTINGPARAMS };


public:
	CPoint	m_ptInitialPos;
	BOOL	m_bLocked;


public:
	CProductionProfile& GetProductionProfile();
	void				InitData();
	void				SaveData();


protected:
	int	m_nOptimizeColumns;
	int	m_nOptimizeAlignment;
	CSpinButtonCtrl	m_rightTrimSpin;
	CSpinButtonCtrl	m_leftTrimSpin;
	CSpinButtonCtrl	m_topTrimSpin;
	CSpinButtonCtrl	m_bottomTrimSpin;
	float			m_fTopTrim;
	float			m_fBottomTrim;
	float			m_fRightTrim;
	float			m_fLeftTrim;

public:
	virtual BOOL Create(UINT nIDTemplate, CWnd* pParentWnd = NULL);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedOptimizeColumnsRadio();
	afx_msg void OnBnClickedOptimizeRowsRadio();
	afx_msg void OnBnClickedOptimizeAlignTopRadio();
	afx_msg void OnBnClickedOptimizeAlignBottomRadio();
	afx_msg void OnBnClickedOptimizeAlignLeftRadio();
	afx_msg void OnBnClickedOptimizeAlignRightRadio();
	afx_msg void OnDeltaposBottomtrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposToptrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposRighttrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposLefttrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
};
