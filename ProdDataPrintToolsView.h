#pragma once

#include "TrueColorToolBar.h"
#include "OwnerDrawnButtonList.h"
#include "ToolBarGroup.h"




// CProdDataPrintToolsView-Formularansicht

class CProdDataPrintToolsView : public CFormView
{
	DECLARE_DYNCREATE(CProdDataPrintToolsView)

protected:
	CProdDataPrintToolsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CProdDataPrintToolsView();

public:
	enum { IDD = IDD_PRODDATAPRINTTOOLSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	class CNewPrintSheetFrame*	m_pNewPrintSheetFrame;
	COwnerDrawnButtonList		m_modifyToolBar, m_alignToolBar;
	CToolBarGroup				m_modifyToolBarGroup, m_alignToolBarGroup;


public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* pSender = NULL, LPARAM lHint = 0, CObject* pHint = NULL);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnUpdateDefineTrim(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSwapObjects(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMoveObject(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjectsTurn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjectsTumble(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjectsrotateClock(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjectsrotateCounterclock(CCmdUI* pCmdUI);
	afx_msg void OnUpdateReleaseBleed(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteSelection(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUndo(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignLeft(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignHCenter(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignRight(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignTop(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignVCenter(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignBottom(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLockUnlockFoldSheet(CCmdUI* pCmdUI);
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};


