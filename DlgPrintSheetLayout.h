#if !defined(AFX_DLGPRINTSHEETLAYOUT_H__E76A02FD_8E03_4279_9C90_841D66821C37__INCLUDED_)
#define AFX_DLGPRINTSHEETLAYOUT_H__E76A02FD_8E03_4279_9C90_841D66821C37__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPrintSheetLayout.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPrintSheetLayout 

class CDlgPrintSheetLayout : public CDialog
{
// Konstruktion
public:
	CDlgPrintSheetLayout(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgPrintSheetLayout)
	enum { IDD = IDD_PRINTSHEET_LAYOUT };
	int		m_nPrintingType;
	int		m_nTurningType;
	CString	m_strPaperFormat;
	CString	m_strPlateFormat;
	CString	m_strPrintLayout;
	CString	m_strPrintMachine;
	CString	m_strPrintMarkset;
	//}}AFX_DATA

private:
	CRect				  m_layoutPreviewFrame;
	CFont				  m_smallFont;
	CBrush				  m_BkBrush;

public:
	CPrintSheet			  m_printSheet;
	CLayout				  m_layout;
	CPressDevice		  m_pressDevice;
	CFoldSheet			  m_foldSheet;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPrintSheetLayout)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL


// Implementierung
protected:
	void DrawPrintSheetLayout(CDC* pDC);
	void UpdatePrintSheetControls();



	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPrintSheetLayout)
	afx_msg void OnPpPrintmachineButton();
	afx_msg void OnPpPaperButton();
	afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	afx_msg void OnPrintmarkset();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPRINTSHEETLAYOUT_H__E76A02FD_8E03_4279_9C90_841D66821C37__INCLUDED_
