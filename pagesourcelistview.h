// PageSourceListView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageSourceListView view

class CPageSourceListView : public CScrollView
{
protected:
	CPageSourceListView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPageSourceListView)

// Attributes
public:
	CSize		 m_PageSourceListSize;
	CDisplayList m_DisplayList;
	BOOL		 m_bInitialUpdate;
	DECLARE_DISPLAY_ITEM(CPageSourceListView, PageSource,		OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPageSourceListView, PageSourceHeader, OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)

// Operations
public:
	CRect		Draw(CDC* pDC, BOOL bGetBoxOnly = FALSE);
	CRect		DrawPagePreview(CDC* pDC, CRect rcFrame, CPageSource& rPageSource, CPageSourceHeader& rPageSourceHeader, BOOL bDimmed, BOOL bGetBoxOnly);
	CRect		DrawColorants(CDC* pDC, CRect rcRect, CPageSourceHeader& rPageSourceHeader);
	static int	GetCurProductPartIndex();
	void		CalcPageSourceListSize();
	CImpManDoc* GetDocument();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageSourceListView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPageSourceListView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CPageSourceListView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PageSourceListView.cpp
inline CImpManDoc* CPageSourceListView::GetDocument()
   { return (CImpManDoc*)m_pDocument; }
#endif


/////////////////////////////////////////////////////////////////////////////
