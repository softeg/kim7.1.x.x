#if !defined(AFX_DLGFANOUT_H__803DFB13_02BD_11D3_955C_204C4F4F5020__INCLUDED_)
#define AFX_DLGFANOUT_H__803DFB13_02BD_11D3_955C_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgFanout.h : Header-Datei
//


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgFanout 

class CDlgFanout : public CDialog
{
// Konstruktion
public:
	CDlgFanout(CWnd* pParent = NULL);   // Standardkonstruktor

public:
	CString		m_strPaperName;
	CString		m_strColorName;
	CFanoutList m_FanoutList;


protected:
	CColorListBox m_JobColorListBox;


// Dialogfelddaten
	//{{AFX_DATA(CDlgFanout)
	enum { IDD = IDD_FANOUT };
	CSpinButtonCtrl	m_FanoutSpinY;
	CSpinButtonCtrl	m_FanoutSpinX;
	float	m_fFanoutX;
	float	m_fFanoutY;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgFanout)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgFanout)
	virtual BOOL OnInitDialog();
	afx_msg void OnNewColorButton();
	afx_msg void OnDeltaposFanoutSpinX(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposFanoutSpinY(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelchangeJobColors();
	afx_msg void OnApplyButton();
	afx_msg void OnChangeFanoutEditX();
	afx_msg void OnChangeFanoutEditY();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGFANOUT_H__803DFB13_02BD_11D3_955C_204C4F4F5020__INCLUDED_
