// DlgProdDataBindTrim.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProdDataBindTrim.h"
#include "DlgModifyMark.h"
#include "ProdDataBindTrimView.h"
#include "ProductionDataFrame.h"
#include "ModalFrame.h"
#include "ProdDataWizard.h"
#include "DlgProdDataBindTrimDetails.h"


// CPDCustomPaintFrame

IMPLEMENT_DYNAMIC(CPDCustomPaintFrame, CStatic)

CPDCustomPaintFrame::CPDCustomPaintFrame()
{

}

CPDCustomPaintFrame::~CPDCustomPaintFrame()
{
}


BEGIN_MESSAGE_MAP(CPDCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()




// CPDCustomPaintFrame-Meldungshandler

void CPDCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CDlgProdDataBindTrim* pDlg = (CDlgProdDataBindTrim*)GetParent();
	if ( ! pDlg)
		return;

	CString strText;
	CRect rcFrame;
	GetClientRect(rcFrame);
	switch (GetDlgCtrlID())
	{
	case IDC_PD_MAINFRAME:				{
											CPen pen(PS_SOLID, 1, LIGHTGRAY);
											dc.SelectObject(&pen);
											CBrush brush(GUICOLOR_GROUP);
											dc.SelectObject(&brush);
											dc.RoundRect(rcFrame, CPoint(6,6));
										}
										break;

	case IDC_BT_TRIMMING_GROUP:	
	case IDC_BT_BINDING_GROUP:			{
											CPen pen(PS_SOLID, 1, LIGHTGRAY);
											dc.SelectObject(&pen);
											CBrush brush(GUICOLOR_GROUP);
											dc.SelectObject(&brush);
											dc.RoundRect(rcFrame, CPoint(8, 8));

											//strText.LoadStringA((GetDlgCtrlID() == IDC_BT_TRIMMING_GROUP) ? IDS_PROCESS_TRIMMING : IDS_PROCESS_BINDING);
											//dc.SelectStockObject(NULL_PEN);
											//dc.FillSolidRect(CRect(rcFrame.left + 8, rcFrame.top, rcFrame.left + dc.GetTextExtent(strText).cx, rcFrame.top + 10), GUICOLOR_GROUP);

											//dc.SelectObject(pDlg->m_smallFont);
											//dc.SetBkMode(TRANSPARENT);
											//dc.SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
											//rcFrame.DeflateRect(12, -7);
											//dc.DrawText(strText, rcFrame, DT_LEFT | DT_TOP);
										}
										break;
	}
}




// CDlgProdDataBindTrim-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProdDataBindTrim, CPropertyPage)

//CDlgProdDataBindTrim::CDlgProdDataBindTrim(CWnd* pParent /*=NULL*/)
//	: CDialog(CDlgProdDataBindTrim::IDD, pParent)
CDlgProdDataBindTrim::CDlgProdDataBindTrim() : CPropertyPage(CDlgProdDataBindTrim::IDD)
{
	m_nBindingMethod = -1;
	m_nProductionType = -1;
	m_fFootTrim = 0.0f;
	m_fHeadTrim = 0.0f;
	m_fSideTrim = 0.0f;
	m_bTrimsIdentical = FALSE;
	m_nPagePreviewIndex = 0;

	m_smallFont.CreateFont (13, 0, 0, 0, FW_BOLD,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	m_mediumFont.CreateFont(14, 0, 0, 0, FW_BOLD,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
	m_orangeBrush.CreateSolidBrush(RGB(252, 220, 150));
	m_bkBrush.CreateSolidBrush(GUICOLOR_GROUP);

	m_bCropMarks		= FALSE;
	m_bSectionMark		= FALSE;

	CString strFullPath = theApp.settings.m_szDataDir + CString(_T("\\SysCropMarks.def"));
	if ( ! m_cropMarkList.Load(strFullPath))
	{
		CCropMark* pCropMark = new CCropMark;
		pCropMark->Create(theApp.m_colorDefTable);
		m_cropMarkList.Add(pCropMark);
	}
	strFullPath = theApp.settings.m_szDataDir + CString(_T("\\SysSectionMarks.def"));
	if ( ! m_sectionMarkList.Load(strFullPath))
	{
		CSectionMark* pSectionMark = new CSectionMark;
		pSectionMark->Create(theApp.m_colorDefTable);
		m_sectionMarkList.Add(pSectionMark);
	}

	m_bIsDialog = FALSE;
	m_pButtonOK = NULL;
	m_pButtonCancel = NULL;
}

CDlgProdDataBindTrim::~CDlgProdDataBindTrim()
{
	m_smallFont.DeleteObject();
	m_mediumFont.DeleteObject();
	m_hollowBrush.DeleteObject();
	m_orangeBrush.DeleteObject();
	m_bkBrush.DeleteObject();

	m_markList.RemoveAll();
	m_cropMarkList.RemoveAll();
	m_sectionMarkList.RemoveAll();

	delete m_pButtonOK;
	delete m_pButtonCancel;
}

void CDlgProdDataBindTrim::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);

	DDX_CBIndex(pDX, IDC_PD_BINDING_METHOD, m_nBindingMethod);
	DDX_Control(pDX, IDC_PD_SIDETRIM_SPIN, m_sideTrimSpin);
	DDX_Control(pDX, IDC_PD_HEADTRIM_SPIN, m_headTrimSpin);
	DDX_Control(pDX, IDC_PD_FOOTTRIM_SPIN, m_footTrimSpin);
	DDX_Measure(pDX, IDC_PD_FOOTTRIM_EDIT, m_fFootTrim);
	DDX_Measure(pDX, IDC_PD_HEADTRIM_EDIT, m_fHeadTrim);
	DDX_Measure(pDX, IDC_PD_SIDETRIM_EDIT, m_fSideTrim);
	DDX_Check(pDX, IDC_PD_TRIMS_IDENTICAL, m_bTrimsIdentical);
	DDX_Text(pDX, IDC_PD_PAGEFORMAT, m_strPageFormat);
	DDX_Check(pDX, IDC_BT_CROPMARK_CHECK, m_bCropMarks);
	DDX_Check(pDX, IDC_BT_SECTIONMARK_CHECK, m_bSectionMark);
}


BEGIN_MESSAGE_MAP(CDlgProdDataBindTrim, CPropertyPage)
	ON_WM_CTLCOLOR()
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_FOOTTRIM_SPIN, OnDeltaposFoottrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_HEADTRIM_SPIN, OnDeltaposHeadtrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_SIDETRIM_SPIN, OnDeltaposSidetrimSpin)
	ON_BN_CLICKED(IDC_PD_TRIMS_IDENTICAL, OnTrimsIdentical)
	ON_EN_KILLFOCUS(IDC_PD_FOOTTRIM_EDIT, OnKillfocusFoottrimEdit)
	ON_EN_KILLFOCUS(IDC_PD_HEADTRIM_EDIT, OnKillfocusHeadtrimEdit)
	ON_EN_KILLFOCUS(IDC_PD_SIDETRIM_EDIT, OnKillfocusSidetrimEdit)
	ON_BN_CLICKED(IDC_PD_MODIFY_CROPMARK, &CDlgProdDataBindTrim::OnBnClickedFpModifyCropmark)
	ON_BN_CLICKED(IDC_PD_MODIFY_SECTIONMARK, &CDlgProdDataBindTrim::OnBnClickedFpModifySectionMark)
	ON_CBN_SELCHANGE(IDC_PD_BINDING_METHOD, &CDlgProdDataBindTrim::OnCbnSelchangePdBindingMethod)
	ON_BN_CLICKED(IDC_BT_CROPMARK_CHECK, &CDlgProdDataBindTrim::OnBnClickedBtCropmarkCheck)
	ON_BN_CLICKED(IDC_BT_SECTIONMARK_CHECK, &CDlgProdDataBindTrim::OnBnClickedBtSectionmarkCheck)
END_MESSAGE_MAP()


// CDlgProdDataBindTrim-Meldungshandler

BOOL CDlgProdDataBindTrim::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	CPropertySheet* psheet = (CPropertySheet*) GetParent();  
	if ( ! psheet->IsKindOf(RUNTIME_CLASS(CPropertySheet)))	
	{
		m_bIsDialog = TRUE;
		AddOKCancel();
	}

	m_prodPartFrame.SubclassDlgItem(IDC_BT_PRODPARTINFO_FRAME, this);

	m_productPartFrame.SubclassDlgItem		(IDC_PD_PRODUCTPART_FRAME,		this);
	m_mainFrame.SubclassDlgItem				(IDC_PD_MAINFRAME,				this);
	m_trimmingGroup.SubclassDlgItem			(IDC_BT_TRIMMING_GROUP,			this);
	m_bindingGroup.SubclassDlgItem			(IDC_BT_BINDING_GROUP,			this);

	m_bookBlockPreviewFrame.SubclassDlgItem	(IDC_BT_BOOKBLOCK_FRAME,		this);

	//m_spineTrimSpin.SetRange(0, 1);  
	//m_millingEdgeSpin.SetRange(0, 1);  

	m_bTrimsIdentical = TRUE;
	m_headTrimSpin.SetRange(0, 1);  
	m_sideTrimSpin.SetRange(0, 1);  
	m_footTrimSpin.SetRange(0, 1);  

	//int   nOldProductionType	= dlg.m_nProductionType;
	//int	nOldBindingMethod	= dlg.m_nBindingMethod;
	//float fOldHeadTrim		= dlg.m_fHeadTrim;
	//float fOldSideTrim		= dlg.m_fSideTrim;
	//float fOldFootTrim		= dlg.m_fFootTrim;

// binding method combo ////////////////////////////
	CComboBoxEx* pBox = (CComboBoxEx*)GetDlgItem (IDC_PD_BINDING_METHOD);
	CBitmap bitmap;
	m_bindingImageList.Create(32, 12, ILC_COLOR8, 2, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_PERFECT_BOUND);
	m_bindingImageList.Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	bitmap.LoadBitmap(IDB_SADDLE_STITCHED);
	m_bindingImageList.Add(&bitmap, BLACK);
	pBox->SetImageList(&m_bindingImageList);
	bitmap.DeleteObject();

	COMBOBOXEXITEM item;
	item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;// | CBEIF_LPARAM;
	item.iItem			= 0; 
	item.pszText		= _T("Fadenheftung");
	item.cchTextMax		= MAX_TEXT;
	item.iImage			= 0;
	item.iSelectedImage = 0;
	//item.iIndent		= 0;
	pBox->InsertItem(&item);
	item.iItem			= 1; 
	item.pszText		= _T("Drahtheftung");
	item.iImage			= 1;
	item.iSelectedImage = 1;
	pBox->InsertItem(&item);
////////////////////////////////////////////////////

	//if (m_nProductPartIndex == -1)	// not initialized from caller
	//	m_nProductPartIndex = 0;

	//CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	//if (pDoc)
	//	if ( (m_nProductPartIndex < 0) || (m_nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
	//	{
	//		m_bCropMarks   = pDoc->m_productPartList[m_nProductPartIndex].HasMarks(CMark::CropMark);
	//		m_bSectionMark = pDoc->m_productPartList[m_nProductPartIndex].HasMarks(CMark::SectionMark);
	//	}

	OnSetActive();

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProdDataBindTrim::AddOKCancel()
{
	if (m_bIsDialog)	// we are not inside wizard (stand alone mode)
	{
//		SetWindowText(_T("Produktionsdaten Trimmen"));

//		GetDlgItem(IDC_BT_BINDING_GROUP)->ShowWindow(SW_HIDE);

		CRect rcFrame, rcFrameDlg;
		//GetDlgItem(IDC_PD_PAGES_PREVIEWFRAME)->GetWindowRect(rcFrame);
		GetWindowRect(rcFrameDlg); 
		//rcFrameDlg.bottom = rcFrame.bottom + 45;
		rcFrameDlg.bottom += 40;
		MoveWindow(rcFrameDlg);
		CRect rcBtn(rcFrameDlg.left + 15, rcFrameDlg.bottom - 30, rcFrameDlg.left + 95, rcFrameDlg.bottom - 8);
		ScreenToClient(rcBtn);
		m_pButtonOK = new CButton;
		m_pButtonOK->Create(_T("OK"), WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, rcBtn, this, IDOK);
		m_pButtonOK->SetFont(GetFont());
		rcBtn.OffsetRect(90, 0);
		m_pButtonCancel = new CButton;
		m_pButtonCancel->Create(_T("Abbruch"), WS_VISIBLE | WS_CHILD, rcBtn, this, IDCANCEL);
		m_pButtonCancel->SetFont(GetFont());
	}
}

BOOL CDlgProdDataBindTrim::OnSetActive()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	//if (m_nProductPartIndex == -1)	// not initialized from caller
	//	m_nProductPartIndex = 0;
	//if ( (m_nProductPartIndex < 0) || (m_nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
	//	return FALSE;

	if ( ! pDoc->m_productPartList.GetSize())
		return FALSE;

	//m_nProductionType	= pDoc->m_productionData.m_nProductionType;			//pDoc->m_productPartList[m_nProductPartIndex].m_nProductionType;
	m_nBindingMethod	= pDoc->m_FinalProductDescription.m_nBindingMethod;	//pDoc->m_productPartList[m_nProductPartIndex].m_nBindingMethod;

	//m_strPageFormat.Format(_T("%.1f x %.1f"), pDoc->m_productPartList[m_nProductPartIndex].m_fPageWidth, pDoc->m_productPartList[m_nProductPartIndex].m_fPageHeight);
	m_strPageFormat.Format(_T("%.1f x %.1f"), pDoc->m_FinalProductDescription.m_fTrimSizeWidth, pDoc->m_FinalProductDescription.m_fTrimSizeHeight);
	m_fHeadTrim	= pDoc->m_productionData.m_fHeadTrim;
	m_fFootTrim	= pDoc->m_productionData.m_fFootTrim;
	m_fSideTrim	= pDoc->m_productionData.m_fSideTrim;

	CComboBoxEx* pBox = (CComboBoxEx*)GetDlgItem (IDC_PD_BINDING_METHOD);
	pBox->SetCurSel(m_nBindingMethod);

	//m_bCropMarks   = pDoc->m_productPartList[m_nProductPartIndex].HasMarks(CMark::CropMark);
	//m_bSectionMark = pDoc->m_productPartList[m_nProductPartIndex].HasMarks(CMark::SectionMark);

	CPropertySheet* psheet = (CPropertySheet*) GetParent();
//	psheet->SetWizardButtons((m_nProductPartIndex == 0) ? PSWIZB_NEXT : PSWIZB_BACK | PSWIZB_NEXT);
	psheet->SetWizardButtons(PSWIZB_BACK | PSWIZB_NEXT);

	CRect rcProdPartFrame;
	m_prodPartFrame.GetWindowRect(rcProdPartFrame);		
	ScreenToClient(rcProdPartFrame);
	m_productPartButtons.RemoveAll();
	m_productPartButtons.Create(this, this, &CDlgProdDataBindTrim::DrawItemProductPart);
	for (int nProductPartIndex = 0; nProductPartIndex < pDoc->m_productPartList.GetSize(); nProductPartIndex++)
	{
		// bind/trim data
		m_productPartButtons.AddButton(L"", ID_SELECT_PRODUCTPART, rcProdPartFrame, (ULONG_PTR)nProductPartIndex, FALSE, (pDoc->m_productPartList.GetSize() > 1));
		rcProdPartFrame.OffsetRect(rcProdPartFrame.Width() + 10, 0);
	}

	UpdateData(FALSE);

	return CPropertyPage::OnSetActive();
}

void CDlgProdDataBindTrim::DrawItemProductPart(LPDRAWITEMSTRUCT lpDrawItemStruct, int /*nState*/)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	int nProductPartIndex = lpDrawItemStruct->itemData;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem = lpDrawItemStruct->rcItem;

	CPen pen(PS_SOLID, 0, RGB(200,200,200));
	pDC->SelectObject(&pen);
	pDC->SelectStockObject(HOLLOW_BRUSH);
	pDC->RoundRect(rcItem, CPoint(2, 2));
	pen.DeleteObject();

	if (pDoc->m_productPartList.GetSize() >= 1)
		if ( ! pDoc->m_productPartList[nProductPartIndex].m_strName.IsEmpty())
		{
			CRect rcHeader = rcItem; rcHeader.bottom = rcHeader.top + 15;  
			pen.CreatePen(PS_SOLID, 1, LIGHTGRAY);
			CBrush brush(RGB(170,225,0));
			pDC->SelectObject(&pen);
			pDC->SelectObject(&brush);
			pDC->RoundRect(rcHeader, CPoint(6,6));
			pen.DeleteObject();
			brush.DeleteObject();

			CFont font;
			font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
			pDC->SelectObject(&font);
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(DARKGREEN);
			CRect rcText = rcItem; rcText.left += 10; rcText.top += 0; rcText.bottom = rcText.top + 15;
			pDC->DrawText(pDoc->m_productPartList[nProductPartIndex].m_strName, rcText, DT_LEFT | DT_TOP);
			rcItem.top += 15;
			font.DeleteObject();
		}

	pDoc->m_productPartList[nProductPartIndex].DrawProductionData(pDC, rcItem, nProductPartIndex, TRUE);
}

void CDlgProdDataBindTrim::OnCbnSelchangePdBindingMethod()
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (pDoc->m_productPartList.GetSize() > 1)
		if (AfxMessageBox(_T("Bindeart f�r alle Produktteile �ndern?"), MB_YESNO) == IDNO)
			return;
	for (int i = 0; i < pDoc->m_productPartList.GetSize(); i++)
		pDoc->m_productPartList[i].m_nBindingMethod = m_nBindingMethod;

	pDoc->m_FinalProductDescription.m_nBindingMethod = m_nBindingMethod;

	m_productPartButtons.Redraw();
}

void CDlgProdDataBindTrim::OnDeltaposHeadtrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_headTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusHeadtrimEdit();
	*pResult = 0;
}

void CDlgProdDataBindTrim::OnDeltaposFoottrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_footTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusFoottrimEdit();
	*pResult = 0;
}

void CDlgProdDataBindTrim::OnDeltaposSidetrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_sideTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusSidetrimEdit();
	*pResult = 0;
}

void CDlgProdDataBindTrim::OnTrimsIdentical() 
{
	OnKillfocusHeadtrimEdit();
}

void CDlgProdDataBindTrim::OnKillfocusHeadtrimEdit() 
{
	UpdateData(TRUE);
	if (m_bTrimsIdentical)
	{
		m_fFootTrim = m_fSideTrim = m_fHeadTrim;
		UpdateData(FALSE);
	}
}

void CDlgProdDataBindTrim::OnKillfocusFoottrimEdit() 
{
	UpdateData(TRUE);
	if (m_bTrimsIdentical)
	{
		m_fHeadTrim = m_fSideTrim = m_fFootTrim;
		UpdateData(FALSE);
	}
}

void CDlgProdDataBindTrim::OnKillfocusSidetrimEdit() 
{
	UpdateData(TRUE);
	if (m_bTrimsIdentical)
	{
		m_fHeadTrim = m_fFootTrim = m_fSideTrim;
		UpdateData(FALSE);
	}
}

void CDlgProdDataBindTrim::OnBnClickedFpModifyCropmark()
{
	CDlgModifyMark dlg;
	dlg.m_pPrintSheet = new CPrintSheet;
	dlg.m_pMark		  = (m_cropMarkList.GetSize()) ? m_cropMarkList[0] : NULL;
	dlg.m_strTitle	  = _T("Schneidmarken");
	if (dlg.DoModal() == IDOK)
	{
		CString strFullPath = theApp.settings.m_szDataDir + CString(_T("\\SysCropMarks.def"));
		m_cropMarkList.Save(strFullPath);
		AssembleMarkList();
	}
}

void CDlgProdDataBindTrim::OnBnClickedFpModifySectionMark()
{
	CDlgModifyMark dlg;
	dlg.m_pPrintSheet = new CPrintSheet;
	dlg.m_pMark		  = (m_sectionMarkList.GetSize()) ? m_sectionMarkList[0] : NULL;
	dlg.m_strTitle	  = _T("Flattermarke");
	if (dlg.DoModal() == IDOK)
	{
		CString strFullPath = theApp.settings.m_szDataDir + CString(_T("\\SysSectionMarks.def"));
		m_sectionMarkList.Save(strFullPath);
		AssembleMarkList();
	}
}

void CDlgProdDataBindTrim::AssembleMarkList()
{
	m_markList.RemoveAll();

	if (m_bCropMarks && m_cropMarkList.GetSize())
	{
		CCropMark* pCropMark = new CCropMark;
		*pCropMark = *((CCropMark*)m_cropMarkList[0]);
		m_markList.Add(pCropMark);
	}
		
	if (m_bSectionMark && m_sectionMarkList.GetSize())
	{
		CSectionMark* pSectionMark = new CSectionMark;
		*pSectionMark = *((CSectionMark*)m_sectionMarkList[0]);
		m_markList.Add(pSectionMark);
	}
}


HBRUSH CDlgProdDataBindTrim::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	
    if(nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_PD_PROCESS_STATIC:
				pDC->SelectObject(&m_mediumFont);
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				hbr = (HBRUSH)m_orangeBrush.GetSafeHandle();
				break;
		case IDC_PD_PAGEFORMAT:
				pDC->SelectObject(&m_smallFont);
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				hbr = (HBRUSH)m_orangeBrush.GetSafeHandle();
				break;

		default:pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				hbr = (HBRUSH)m_bkBrush.GetSafeHandle();
				break;
		}
    }

	return hbr;
}

void CDlgProdDataBindTrim::OnBnClickedBtCropmarkCheck()
{
	UpdateData(TRUE);

	GetDlgItem(IDC_PD_MODIFY_CROPMARK)->EnableWindow(m_bCropMarks);

	AssembleMarkList();

	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgProdDataBindTrim::OnBnClickedBtSectionmarkCheck()
{
	UpdateData(TRUE);

	GetDlgItem(IDC_PD_MODIFY_SECTIONMARK)->EnableWindow(m_bSectionMark);

	AssembleMarkList();

	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

BOOL CDlgProdDataBindTrim::PreTranslateMessage(MSG* pMsg)
{
	if (m_bIsDialog)
		return CDialog::PreTranslateMessage(pMsg);
	else
		return CPropertyPage::PreTranslateMessage(pMsg);
}

void CDlgProdDataBindTrim::OnOK()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	//pDoc->m_productPartList[m_nProductPartIndex].SetMarks(m_markList);

	CDialog::OnOK();
}

void CDlgProdDataBindTrim::OnCancel()
{
	CDialog::OnCancel();
}

LRESULT CDlgProdDataBindTrim::OnWizardNext()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CPropertyPage::OnWizardNext();

	//if (m_nProductPartIndex < pDoc->m_productPartList.GetSize() - 1)	// initialize next product part with same trims
	//{
	//	UpdateData(TRUE);
	//	pDoc->m_productPartList[m_nProductPartIndex + 1].m_fHeadTrim = m_fHeadTrim;
	//	pDoc->m_productPartList[m_nProductPartIndex + 1].m_fSideTrim = m_fSideTrim;
	//	pDoc->m_productPartList[m_nProductPartIndex + 1].m_fFootTrim = m_fFootTrim;
	//}

	CProdDataWizard* psheet = (CProdDataWizard*) GetParent();  
	if (psheet)
		psheet->m_foldPrintPage.m_bInitializeNew = TRUE;
	return CPropertyPage::OnWizardNext();
}

LRESULT CDlgProdDataBindTrim::OnWizardBack()
{
	//if (m_nProductPartIndex > 0)
	//{
	//	m_nProductPartIndex--;
	//	CProdDataWizard* psheet = (CProdDataWizard*) GetParent();  
	//	if (psheet)
	//	{
	//		psheet->m_foldPrintPage.m_nProductPartIndex = m_nProductPartIndex;
	//		psheet->m_foldPrintPage.m_bInitializeActual = TRUE;
	//	}
	//	return IDD_PRODDATA_FOLD_PRINT;
	//}

	return CPropertyPage::OnWizardBack();
}

