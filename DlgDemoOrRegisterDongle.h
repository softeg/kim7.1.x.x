#if !defined(AFX_DLGDEMOORREGISTERDONGLE_H__A943A51B_552A_41D8_B23C_18A31D6A726C__INCLUDED_)
#define AFX_DLGDEMOORREGISTERDONGLE_H__A943A51B_552A_41D8_B23C_18A31D6A726C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgDemoOrRegisterDongle.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgDemoOrRegisterDongle 

class CDlgDemoOrRegisterDongle : public CDialog
{
// Konstruktion
public:
	CDlgDemoOrRegisterDongle(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgDemoOrRegisterDongle)
	enum { IDD = IDD_DEMO_OR_REGISTER_DONGLE };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


public:
	CString m_strRemainingDays;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgDemoOrRegisterDongle)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgDemoOrRegisterDongle)
	afx_msg void OnRegisterDongle();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGDEMOORREGISTERDONGLE_H__A943A51B_552A_41D8_B23C_18A31D6A726C__INCLUDED_
