#pragma once

#include "afxext.h"
#include "MyBitmapButton.h"



class CBookBlockProdDataView :	public CScrollView
{
	DECLARE_DYNCREATE(CBookBlockProdDataView)

public:
	CBookBlockProdDataView(void);
	virtual ~CBookBlockProdDataView(void);

public:
	DECLARE_INPLACE_EDIT  (InplaceEditShingling,   CBookBlockProdDataView);
	DECLARE_INPLACE_BUTTON(InplaceButtonShingling, CBookBlockProdDataView);

	DECLARE_DISPLAY_LIST(CBookBlockProdDataView, OnMultiSelect, YES, OnStatusChanged, YES)
	DECLARE_DISPLAY_ITEM(CBookBlockProdDataView, NavigationFrame, OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, FALSE)
	DECLARE_DISPLAY_ITEM(CBookBlockProdDataView, ShinglingValueX, OnSelect, YES, OnDeselect, NO,  OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CBookBlockProdDataView, ShinglingValueY, OnSelect, YES, OnDeselect, NO,  OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, TRUE)


public:
	int				m_nProductIndex;
	int				m_nPagePreviewIndex;
	int				m_nProductPartIndex;
	CMyBitmapButton	m_gotoPrevPageButton, m_gotoNextPageButton;


public:
	void  Draw(CDC* pDC);
	CSize CalcDocSize();
	void  InitData();
	void  GotoFoldSheet(CFoldSheet* pFoldSheet, int nFoldSheetPart);
	void  UpdateShinglingValue();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void OnDraw(CDC* pDC);      
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnGotoPrevPage();
	afx_msg void OnGotoNextPage();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
};
