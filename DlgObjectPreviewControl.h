#if !defined(AFX_DLGOBJECTPREVIEWCONTROL_H__6674AFDC_774B_4942_A2FC_9ECFBC7244FE__INCLUDED_)
#define AFX_DLGOBJECTPREVIEWCONTROL_H__6674AFDC_774B_4942_A2FC_9ECFBC7244FE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgObjectPreviewControl.h : Header-Datei
//

#include "LayoutObjectPreviewFrame.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgObjectPreviewControl 

class CDlgObjectPreviewControl : public CDialog
{
// Konstruktion
public:
	CDlgObjectPreviewControl(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgObjectPreviewControl)
	enum { IDD = IDD_OBJECT_PREVIEW_CONTROL };
	CLayoutObjectPreviewFrame	m_objectPreviewFrame;
	CScrollBar	m_scrollbarV;
	CScrollBar	m_scrollbarH;
	//}}AFX_DATA

public:
	CSize m_sizeViewport;



// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgObjectPreviewControl)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void			Initialize(int nIDFrame, CWnd* pParent);
	void			Move(int nIDFrame, CWnd* pParent);
	void			ReinitDialog();
	void			PrepareUpdatePreview();
	void			Apply();
	CMark*			GetMark();
	CLayoutObject*  GetLayoutObject();


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgObjectPreviewControl)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGOBJECTPREVIEWCONTROL_H__6674AFDC_774B_4942_A2FC_9ECFBC7244FE__INCLUDED_
