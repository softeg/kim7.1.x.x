// Von Klassen-Assistent automatisch erstellte IDispatch-Kapselungsklasse(n). 
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse _PdfEvents 

class _PdfEvents : public COleDispatchDriver
{
public:
	_PdfEvents() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	_PdfEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	_PdfEvents(const _PdfEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	void OnLogMessage(LPCTSTR strMessage);
	void OnJobStart(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF);
	void OnJobDone(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF);
	void OnJobFail(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF);
	void OnPercentDone(long nPercentDone);
	void OnPageNumber(long nPageNumber);
};
/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse IPdfDistiller 

class IPdfDistiller : public COleDispatchDriver
{
public:
	IPdfDistiller() {}		// Ruft den Standardkonstruktor f�r COleDispatchDriver auf
	IPdfDistiller(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	IPdfDistiller(const IPdfDistiller& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attribute
public:

// Operationen
public:
	short FileToPDF(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF, LPCTSTR strJobOptions);
	void Create();
	long GetBSpoolJobs();
	void SetBSpoolJobs(long nNewValue);
	long GetBShowWindow();
	void SetBShowWindow(long nNewValue);
	void CancelJob();
};
