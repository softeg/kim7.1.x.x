// DlgShinglingAssistent.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgShinglingAssistent.h"
#include "BookBlockView.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "BookBlockProdDataView.h"
#include "PageListView.h"
#include "PageListTableView.h"
#include "PrintSheetView.h"
#include "ThemedTabCtrl.h"
#include "DlgPaperDefs.h"
#include "BookBlockDataView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgShinglingAssistent 


CDlgShinglingAssistent::CDlgShinglingAssistent(CWnd* pParent, BOOL bRunModal, int nProductPartIndex)
	: CDialog(CDlgShinglingAssistent::IDD, pParent)
	, m_bShinglingCropMarks(FALSE)
{
	//{{AFX_DATA_INIT(CDlgShinglingAssistent)
	m_bShinglingActive = FALSE;
	m_fShinglingValueXTotal = 0.0f;
	m_fShinglingValueX = 0.0f;
	m_fShinglingValueXFoot = 0.0f;
	m_fShinglingValueXFootTotal = 0.0f;
	m_fShinglingValueYTotal = 0.0f;
	m_fShinglingValueY = 0.0f;
	//}}AFX_DATA_INIT

	m_bRunModal					= bRunModal;
	m_bRunEmbedded				= TRUE;
	m_pParentWnd				= pParent;
	m_nProductIndex				= 0;
	m_nGlobalProductPartIndex	= nProductPartIndex;
	m_nLocalProductPartIndex	= 0;
	m_nPaperGrammage			= 0;
	m_fPaperVolume				= 0.0f;

	m_boldFont.CreateFont(13, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}


CDlgShinglingAssistent::~CDlgShinglingAssistent()
{
	if ( ! m_bRunModal)
		DestroyWindow();
}


void CDlgShinglingAssistent::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgShinglingAssistent)
	DDX_Control(pDX, IDC_SHINGLING_X_FOOT_TOTAL_SPIN, m_ShinglingValueXFootTotalSpin);
	DDX_Control(pDX, IDC_SHINGLING_X_FOOT_SPIN, m_shinglingValueXFootSpin);
	DDX_Control(pDX, IDC_SHINGLING_X_SPIN, m_shinglingValueXSpin);
	DDX_Control(pDX, IDC_SHINGLING_X_TOTAL_SPIN, m_ShinglingValueXTotalSpin);
	DDX_Measure(pDX, IDC_SHINGLING_X_TOTAL_EDIT, m_fShinglingValueXTotal, 4);
	DDX_Measure(pDX, IDC_SHINGLING_X_EDIT, m_fShinglingValueX, 4);
	DDX_Measure(pDX, IDC_SHINGLING_X_FOOT_EDIT, m_fShinglingValueXFoot, 4);
	DDX_Measure(pDX, IDC_SHINGLING_X_FOOT_TOTAL_EDIT, m_fShinglingValueXFootTotal, 4);
	DDX_Control(pDX, IDC_SHINGLING_Y_SPIN, m_shinglingValueYSpin);
	DDX_Control(pDX, IDC_SHINGLING_Y_TOTAL_SPIN, m_ShinglingValueYTotalSpin);
	DDX_Measure(pDX, IDC_SHINGLING_Y_EDIT, m_fShinglingValueY, 4);
	DDX_Measure(pDX, IDC_SHINGLING_Y_TOTAL_EDIT, m_fShinglingValueYTotal, 4);
	DDX_Control(pDX, IDC_SHINGLING_PRODUCTGROUP_COMBO, m_productPartCombo);
	DDX_CBIndex(pDX, IDC_SHINGLING_PRODUCTGROUP_COMBO, m_nLocalProductPartIndex);
	DDX_Control(pDX, IDC_SHINGLING_STYLE_COMBO, m_shinglingStyleCombo);
	DDX_CBIndex(pDX, IDC_SHINGLING_STYLE_COMBO, m_nShinglingStyle);
	DDX_Control(pDX, IDC_SHINGLING_DIRECTION_COMBO, m_shinglingDirectionCombo);
	DDX_CBIndex(pDX, IDC_SHINGLING_DIRECTION_COMBO, m_nShinglingDirection);
	DDX_Text(pDX, IDC_SHINGLING_PAPER_STATIC, m_strPaperInfo);
	DDX_Check(pDX, IDC_SHINGLING_CROPMARKS, m_bShinglingCropMarks);
	//}}AFX_DATA_MAP
	m_paperDefsSelection.DDX_PaperDefsCombo(pDX, IDC_SHINGLING_PAPER_COMBO, m_strPaper);
}


BOOL CDlgShinglingAssistent::Create( UINT nIDTemplate, CWnd* pParentWnd )
{
	m_pParentWnd = pParentWnd;

	return CDialog::Create(nIDTemplate, pParentWnd);
}



BEGIN_MESSAGE_MAP(CDlgShinglingAssistent, CDialog)
	//{{AFX_MSG_MAP(CDlgShinglingAssistent)
	ON_WM_CLOSE()
	ON_NOTIFY(UDN_DELTAPOS, IDC_SHINGLING_X_TOTAL_SPIN, OnDeltaposShinglingXTotalSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SHINGLING_X_SPIN, OnDeltaposShinglingXSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SHINGLING_X_FOOT_SPIN, OnDeltaposShinglingXFootSpin)
	ON_BN_CLICKED(IDC_SHINGLING_METHOD_BOTTLING, OnShinglingMethodBottling)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SHINGLING_X_FOOT_TOTAL_SPIN, OnDeltaposShinglingXFootTotalSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SHINGLING_Y_TOTAL_SPIN, OnDeltaposShinglingYTotalSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SHINGLING_Y_SPIN, OnDeltaposShinglingYSpin)
	ON_EN_KILLFOCUS(IDC_SHINGLING_X_EDIT, OnKillfocusShinglingXEdit)
	ON_EN_KILLFOCUS(IDC_SHINGLING_X_FOOT_EDIT, OnKillfocusShinglingXFootEdit)
	ON_EN_KILLFOCUS(IDC_SHINGLING_X_TOTAL_EDIT, OnKillfocusShinglingXTotalEdit)
	ON_EN_KILLFOCUS(IDC_SHINGLING_X_FOOT_TOTAL_EDIT, OnKillfocusShinglingXFootTotalEdit)
	ON_EN_KILLFOCUS(IDC_SHINGLING_Y_EDIT, OnKillfocusShinglingYEdit)
	ON_EN_KILLFOCUS(IDC_SHINGLING_Y_TOTAL_EDIT, OnKillfocusShinglingYTotalEdit)
	ON_BN_CLICKED(IDC_SHINGLING_PAPER_BUTTON, &CDlgShinglingAssistent::OnBnClickedShinglingPaperButton)
	ON_CBN_SELCHANGE(IDC_SHINGLING_PRODUCTGROUP_COMBO, &CDlgShinglingAssistent::OnCbnSelchangeShinglingProductpartCombo)
	ON_CBN_SELCHANGE(IDC_SHINGLING_STYLE_COMBO, &CDlgShinglingAssistent::OnCbnSelchangeShinglingStyleCombo)
	ON_CBN_SELCHANGE(IDC_SHINGLING_DIRECTION_COMBO, &CDlgShinglingAssistent::OnCbnSelchangeShinglingDirectionCombo)
	ON_CBN_SELCHANGE(IDC_SHINGLING_PAPER_COMBO, &CDlgShinglingAssistent::OnCbnSelchangeShinglingPaperCombo)
	//}}AFX_MSG_MAP
	ON_WM_CREATE()
	ON_BN_CLICKED(IDOK, &CDlgShinglingAssistent::OnBnClickedOk)
	ON_BN_CLICKED(IDC_SHINGLING_CROPMARKS, &CDlgShinglingAssistent::OnBnClickedShinglingCropmarks)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgShinglingAssistent 

int CDlgShinglingAssistent::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	if ( ! m_bRunModal)
	{
		if (m_bRunEmbedded)
		{
			SetParent(m_pParentWnd);
			ModifyStyle(WS_OVERLAPPEDWINDOW | WS_POPUPWINDOW, WS_CHILD);
			ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);
		}
	}

	return 0;
}

BOOL CDlgShinglingAssistent::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			CWnd* pWnd = GetFocus();
			switch (pWnd->GetDlgCtrlID())
			{
			case IDC_SHINGLING_X_EDIT:				OnKillfocusShinglingXEdit();			return TRUE; 
			case IDC_SHINGLING_X_FOOT_EDIT:			OnKillfocusShinglingXFootEdit();		return TRUE;
			case IDC_SHINGLING_X_TOTAL_EDIT:		OnKillfocusShinglingXTotalEdit();		return TRUE;
			case IDC_SHINGLING_X_FOOT_TOTAL_EDIT:	OnKillfocusShinglingXFootTotalEdit();	return TRUE;
			case IDC_SHINGLING_Y_EDIT:				OnKillfocusShinglingYEdit();			return TRUE; 
			case IDC_SHINGLING_Y_TOTAL_EDIT:		OnKillfocusShinglingYTotalEdit();		return TRUE;
			}
			if (m_bRunModal)
				return CDialog::PreTranslateMessage(pMsg);
			else
				return TRUE;
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
				if (m_bRunModal)
					return CDialog::PreTranslateMessage(pMsg);
				else
					return TRUE;

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CDlgShinglingAssistent::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( ! m_bRunModal && m_bRunEmbedded)
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	m_nProductIndex = rProductPart.GetBoundProduct().GetIndex();

	////////// product part combo
	CBitmap bm;
	bm.LoadBitmap(IDB_PRODUCTGROUP);
	CImageList imageList;
	imageList.Create(24, 24, ILC_COLORDDB|ILC_MASK, 1, 0);	// List not to grow
	imageList.Add(&bm, RGB(235,235,235));
	m_productPartCombo.SetImageList(&imageList);
	imageList.Detach();

	m_productPartCombo.SetFont(&m_boldFont);

	InitProductPartCombo();
	////////////////////////////////

	m_strPaperInfo = _T(" ") + rProductPart.InitPaperInfo();

	m_bShinglingActive	   = rProductPart.m_foldingParams.m_bShinglingActive;
	m_nShinglingMethod	   = rProductPart.m_foldingParams.m_nShinglingMethod;
	m_nShinglingDirection  = rProductPart.m_foldingParams.m_nShinglingDirection;
	m_fShinglingValueX 	   = rProductPart.m_foldingParams.GetShinglingValueX(rProductPart);
	m_fShinglingValueXFoot = rProductPart.m_foldingParams.GetShinglingValueXFoot(rProductPart);
	m_fShinglingValueY 	   = rProductPart.m_foldingParams.GetShinglingValueY(rProductPart);
	m_bShinglingCropMarks  = rProductPart.m_foldingParams.m_bShinglingCropMarks;
	m_strPaper			   = rProductPart.m_strPaper;
	m_nPaperGrammage	   = rProductPart.m_nPaperGrammage;
	m_fPaperVolume		   = rProductPart.m_fPaperVolume;

	UpdateData(FALSE);	// save shingling values before OnCbnSelchangeShinglingPaperCombo() because it calls UpdateData(TRUE) 

	m_paperDefsSelection.InitComboBox(this, m_strPaper, &m_nPaperGrammage, &m_fPaperVolume, &m_fShinglingValueX, IDC_SHINGLING_PAPER_COMBO);
	//m_paperDefsSelection.SelchangeFormatList(this, &m_nPaperGrammage, &m_fPaperVolume, m_strPaper, IDC_SHINGLING_PAPER_COMBO);
	if (((CComboBox*)GetDlgItem(IDC_SHINGLING_PAPER_COMBO))->FindString(0, m_strPaper) < 0)
	{
		((CComboBox*)GetDlgItem(IDC_SHINGLING_PAPER_COMBO))->AddString(m_strPaper);
		((CComboBox*)GetDlgItem(IDC_SHINGLING_PAPER_COMBO))->SelectString(0, m_strPaper);
	}

	rProductPart.m_strPaper			= m_strPaper;
	rProductPart.m_fPaperVolume		= m_fPaperVolume;
	rProductPart.m_nPaperGrammage	= m_nPaperGrammage;

	m_ShinglingValueXTotalSpin.SetRange(0, 1);  
	m_ShinglingValueXFootTotalSpin.SetRange(0, 1);  
	m_ShinglingValueYTotalSpin.SetRange(0, 1);  

	GetDlgItem(IDC_SHINGLING_HEAD_STATIC)->GetWindowText(m_strHeadStaticOrg);

	InitBottling();

	pDoc->m_Bookblock.SetShinglingValuesX(m_fShinglingValueX, FLT_MAX, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, m_fShinglingValueXFoot, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesY(m_fShinglingValueY, m_nGlobalProductPartIndex);
	pDoc->m_PageTemplateList.CalcShinglingOffsets(m_fShinglingValueXTotal, m_fShinglingValueXFootTotal, m_fShinglingValueYTotal, m_nGlobalProductPartIndex);

	m_shinglingValueXSpin.SetRange(0,1);
	m_shinglingValueXFootSpin.SetRange(0,1);
	m_shinglingValueYSpin.SetRange(0,1);

	InitShinglingStyle();
	InitShinglingDirection();
	////////////////////////////////

	pDoc->m_Bookblock.m_bShinglingActive = TRUE;

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

int CDlgShinglingAssistent::ProductPartGlobalToLocal(int nGlobalProductPart)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nGlobalProductPart;

	CBoundProductPart& rProductPart = pDoc->GetBoundProductPart(nGlobalProductPart);

	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			if (&rBoundProduct.m_parts[i] == &rProductPart)
				return i;
		}
	}

	return nGlobalProductPart;
}

int CDlgShinglingAssistent::ProductPartLocalToGlobal(int nLocalProductPart)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nLocalProductPart;

	CBoundProductPart& rProductPart = pDoc->GetBoundProduct(m_nProductIndex).GetPart(nLocalProductPart);

	int		 nGlobalProductPart = 0;
	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			if (&rBoundProduct.m_parts[i] == &rProductPart)
				return nGlobalProductPart;
			nGlobalProductPart++;
		}
	}

	return nLocalProductPart;
}

void CDlgShinglingAssistent::InitProductPartCombo()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_productPartCombo.ResetContent();

	CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetProduct(m_nProductIndex);

	COMBOBOXEXITEM item;
	item.mask = CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;

	for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
	{
		item.iItem			= i;
		item.pszText		= rBoundProduct.m_parts[i].m_strName.GetBuffer(MAX_TEXT);
		item.iImage			= 0;
		item.iSelectedImage = 0;
		m_productPartCombo.InsertItem(&item);
	}

	m_nLocalProductPartIndex = ProductPartGlobalToLocal(m_nGlobalProductPartIndex);
	m_productPartCombo.SetCurSel(m_nLocalProductPartIndex);
}

void CDlgShinglingAssistent::InitBottling()
{
	CString strStatic;
	if ( (m_nShinglingMethod == CBookblock::ShinglingMethodBottling) || (m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale) )
	{
		GetDlgItem(IDC_SHINGLING_X_FOOT_EDIT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SHINGLING_X_FOOT_SPIN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SHINGLING_X_FOOT_TOTAL_EDIT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SHINGLING_X_FOOT_TOTAL_SPIN)->ShowWindow(SW_SHOW);
		//GetDlgItem(IDC_SHINGLING_HEAD_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_SHINGLING_FOOT_STATIC)->ShowWindow(SW_SHOW);
		strStatic = m_strHeadStaticOrg;
	}
	else
	{
		GetDlgItem(IDC_SHINGLING_X_FOOT_EDIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SHINGLING_X_FOOT_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SHINGLING_X_FOOT_TOTAL_EDIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SHINGLING_X_FOOT_TOTAL_SPIN)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDC_SHINGLING_HEAD_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SHINGLING_FOOT_STATIC)->ShowWindow(SW_HIDE);
		strStatic = "Bund";//.LoadString(IDS_SPINE);
	}
	GetDlgItem(IDC_SHINGLING_HEAD_STATIC)->SetWindowText(strStatic);
}

void CDlgShinglingAssistent::InitShinglingStyle()
{
	switch (m_nShinglingMethod)
	{
	case CBookblock::ShinglingMethodMove:			((CButton*)GetDlgItem(IDC_SHINGLING_METHOD_BOTTLING))->SetCheck(FALSE); m_nShinglingStyle = 1;	break;
	case CBookblock::ShinglingMethodScale:			((CButton*)GetDlgItem(IDC_SHINGLING_METHOD_BOTTLING))->SetCheck(FALSE); m_nShinglingStyle = 2;	break;
	case CBookblock::ShinglingMethodBottling:		((CButton*)GetDlgItem(IDC_SHINGLING_METHOD_BOTTLING))->SetCheck(TRUE);	m_nShinglingStyle = 1;	break;
	case CBookblock::ShinglingMethodBottlingScale:	((CButton*)GetDlgItem(IDC_SHINGLING_METHOD_BOTTLING))->SetCheck(TRUE);	m_nShinglingStyle = 2;	break;
	}
	if ( ! m_bShinglingActive)
		m_nShinglingStyle = 0;

	UpdateData(FALSE);
}

void CDlgShinglingAssistent::InitShinglingDirection()
{
	CString strStatic;
	int		nShow = SW_SHOW;
	switch (m_nShinglingDirection)
	{
	case CBookblock::ShinglingDirectionSpine:			nShow = SW_HIDE;	
														break;
	case CBookblock::ShinglingDirectionSpineHead:		
	case CBookblock::ShinglingDirectionSpineFoot:		nShow = SW_SHOW;	
														strStatic.LoadString( (m_nShinglingDirection == CBookblock::ShinglingDirectionSpineHead) ? IDS_HEAD : IDS_FOOT);
														break;
	}

	GetDlgItem(IDC_SHINGLING_Y_STATIC)->SetWindowText(strStatic);
	GetDlgItem(IDC_SHINGLING_Y_STATIC)->ShowWindow(nShow); 
	GetDlgItem(IDC_SHINGLING_Y_EDIT)->ShowWindow(nShow);	
	GetDlgItem(IDC_SHINGLING_Y_SPIN)->ShowWindow(nShow);	
	GetDlgItem(IDC_SHINGLING_Y_TOTAL_EDIT)->ShowWindow(nShow);	
	GetDlgItem(IDC_SHINGLING_Y_TOTAL_SPIN)->ShowWindow(nShow);	

	UpdateData(FALSE);
}

CView* CDlgShinglingAssistent::GetBookBlockView() 
{
	CView* pView;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	POSITION pos = pDoc->GetFirstViewPosition();
	while (pos != NULL)
	{
		pView = pDoc->GetNextView(pos);
		if (pView->IsKindOf(RUNTIME_CLASS(CBookBlockView)))
			return pView;
	}
	return NULL;
}

void CDlgShinglingAssistent::UpdateViews()
{
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockProdDataView), TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgShinglingAssistent::OnCbnSelchangeShinglingPaperCombo()
{
	CString strOldPaper = m_strPaper;
	UpdateData(TRUE);
	m_paperDefsSelection.SelchangeFormatList(this, &m_nPaperGrammage, &m_fPaperVolume, &m_fShinglingValueX, m_strPaper, IDC_SHINGLING_PAPER_COMBO);
	if (m_strPaper == strOldPaper)
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);

	rProductPart.m_strPaper			= m_strPaper;
	rProductPart.m_fPaperVolume		= m_fPaperVolume;
	rProductPart.m_nPaperGrammage	= m_nPaperGrammage;

	SetShinglingValueX(0.0f);	// reset
	m_fShinglingValueX = m_fShinglingValueXFoot = m_fShinglingValueY = rProductPart.GetShinglingValueX(TRUE);

	SetShinglingValueX(m_fShinglingValueX);
	SetShinglingValueXFoot(m_fShinglingValueXFoot); 
	SetShinglingValueY(m_fShinglingValueY);
	pDoc->m_Bookblock.SetShinglingValuesX(m_fShinglingValueX, FLT_MAX, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, m_fShinglingValueXFoot, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesY(m_fShinglingValueY, m_nGlobalProductPartIndex);
	pDoc->m_PageTemplateList.CalcShinglingOffsets(m_fShinglingValueXTotal, m_fShinglingValueXFootTotal, m_fShinglingValueYTotal, m_nGlobalProductPartIndex);
	UpdateViews();
	
	m_strPaperInfo = _T(" ") + rProductPart.InitPaperInfo();
	
	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);

	UpdateData(FALSE);
}

void CDlgShinglingAssistent::OnBnClickedShinglingPaperButton()
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgPaperDefs dlg;
	dlg.m_strPaperName = m_strPaper;
	if (dlg.DoModal() == IDCANCEL)
		return;

	m_strPaper			= dlg.m_strPaperName;
	m_fPaperVolume		= dlg.m_fPaperVolume;
	m_nPaperGrammage	= dlg.m_nPaperGrammage;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	rProductPart.m_strPaper			= dlg.m_strPaperName;
	rProductPart.m_fPaperVolume		= dlg.m_fPaperVolume;
	rProductPart.m_nPaperGrammage	= dlg.m_nPaperGrammage;

	m_strPaperInfo = _T(" ") + rProductPart.InitPaperInfo();

	UpdateData(FALSE);

	m_paperDefsSelection.InitComboBox(this, m_strPaper, &m_nPaperGrammage, &m_fPaperVolume, &m_fShinglingValueX, IDC_SHINGLING_PAPER_COMBO);

	SetShinglingValueX(0.0f);	// reset
	m_fShinglingValueX = m_fShinglingValueXFoot = m_fShinglingValueY = rProductPart.GetShinglingValueX(TRUE);

	SetShinglingValueX(m_fShinglingValueX);
	SetShinglingValueXFoot(m_fShinglingValueXFoot); 
	SetShinglingValueY(m_fShinglingValueY);
	pDoc->m_Bookblock.SetShinglingValuesX(m_fShinglingValueX, FLT_MAX, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, m_fShinglingValueXFoot, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesY(m_fShinglingValueY, m_nGlobalProductPartIndex);
	pDoc->m_PageTemplateList.CalcShinglingOffsets(m_fShinglingValueXTotal, m_fShinglingValueXFootTotal, m_fShinglingValueYTotal, m_nGlobalProductPartIndex);
	UpdateViews();
	UpdateData(FALSE);
}

void CDlgShinglingAssistent::OnDeltaposShinglingXSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_shinglingValueXSpin.GetBuddy(), pNMUpDown->iDelta, 0.01f);
	
	InitShinglingValuesXTotal();
	UpdateViews();

	*pResult = 0;
}

void CDlgShinglingAssistent::OnDeltaposShinglingXFootSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_shinglingValueXFootSpin.GetBuddy(), pNMUpDown->iDelta, 0.01f);
	
	InitShinglingValuesXTotal();
	UpdateViews();

	*pResult = 0;
}

void CDlgShinglingAssistent::OnDeltaposShinglingXTotalSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_ShinglingValueXTotalSpin.GetBuddy(), pNMUpDown->iDelta, 0.1f);

	InitShinglingValuesX();
	UpdateViews();

	*pResult = 0;
}

void CDlgShinglingAssistent::OnDeltaposShinglingXFootTotalSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_ShinglingValueXFootTotalSpin.GetBuddy(), pNMUpDown->iDelta, 0.1f);

	InitShinglingValuesX();
	UpdateViews();

	*pResult = 0;
}

void CDlgShinglingAssistent::OnDeltaposShinglingYSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_shinglingValueYSpin.GetBuddy(), pNMUpDown->iDelta, 0.01f);
	
	InitShinglingValuesYTotal();
	UpdateViews();

	*pResult = 0;
}

void CDlgShinglingAssistent::OnDeltaposShinglingYTotalSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_ShinglingValueYTotalSpin.GetBuddy(), pNMUpDown->iDelta, 0.1f);

	InitShinglingValuesY();
	UpdateViews();

	*pResult = 0;
}

void CDlgShinglingAssistent::OnKillfocusShinglingXEdit() 
{
	float fOldValue = m_fShinglingValueX;
	UpdateData(TRUE);
	if (fOldValue == m_fShinglingValueX)
		return;
	InitShinglingValuesXTotal();
	UpdateViews();
}

void CDlgShinglingAssistent::OnKillfocusShinglingXFootEdit() 
{
	float fOldValue = m_fShinglingValueXFoot;
	UpdateData(TRUE);
	if (fOldValue == m_fShinglingValueXFoot)
		return;
	InitShinglingValuesXTotal();
	UpdateViews();
}

void CDlgShinglingAssistent::OnKillfocusShinglingXTotalEdit() 
{
	float fOldValue = m_fShinglingValueXTotal;
	UpdateData(TRUE);
	if (fOldValue == m_fShinglingValueXTotal)
		return;
	InitShinglingValuesX();
	UpdateViews();
}

void CDlgShinglingAssistent::OnKillfocusShinglingXFootTotalEdit() 
{
	float fOldValue = m_fShinglingValueXFootTotal;
	UpdateData(TRUE);
	if (fOldValue == m_fShinglingValueXFootTotal)
		return;
	InitShinglingValuesX();
	UpdateViews();
}

void CDlgShinglingAssistent::OnKillfocusShinglingYEdit() 
{
	float fOldValue = m_fShinglingValueY;
	UpdateData(TRUE);
	if (fOldValue == m_fShinglingValueY)
		return;
	InitShinglingValuesYTotal();
	UpdateViews();
}

void CDlgShinglingAssistent::OnKillfocusShinglingYTotalEdit() 
{
	float fOldValue = m_fShinglingValueYTotal;
	UpdateData(TRUE);
	if (fOldValue == m_fShinglingValueYTotal)
		return;
	InitShinglingValuesY();
	UpdateViews();
}

void CDlgShinglingAssistent::OnShinglingMethodBottling() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (m_nShinglingMethod != CBookblock::ShinglingMethodBottling)
		m_nShinglingMethod = CBookblock::ShinglingMethodBottling;
	else
		m_nShinglingMethod = CBookblock::ShinglingMethodMove;

	SetShinglingMethod(m_nShinglingMethod);

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);

	InitBottling();
	InitShinglingValuesXTotal();
	InitShinglingStyle();
	UpdateViews();
}

void CDlgShinglingAssistent::InitShinglingValuesX() 
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int	nLayerMax = pDoc->m_PageTemplateList.CalcShinglingMaxLayerNum(m_nGlobalProductPartIndex);
	if (nLayerMax >= 2)
	{
		m_fShinglingValueX	   = m_fShinglingValueXTotal	 / (nLayerMax - 1);
		m_fShinglingValueXFoot = m_fShinglingValueXFootTotal / (nLayerMax - 1);
	}
	pDoc->m_Bookblock.SetShinglingValuesX(m_fShinglingValueX, FLT_MAX, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, m_fShinglingValueXFoot, m_nGlobalProductPartIndex);
	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);

	UpdateData(FALSE);
}

void CDlgShinglingAssistent::InitShinglingValuesXTotal() 
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	pDoc->m_Bookblock.SetShinglingValuesX(m_fShinglingValueX, FLT_MAX, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, m_fShinglingValueXFoot, m_nGlobalProductPartIndex);
	pDoc->m_PageTemplateList.CalcShinglingOffsets(m_fShinglingValueXTotal, m_fShinglingValueXFootTotal, m_fShinglingValueYTotal, m_nGlobalProductPartIndex);
	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);

	UpdateData(FALSE);
}

void CDlgShinglingAssistent::InitShinglingValuesY() 
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int	nLayerMax = pDoc->m_PageTemplateList.CalcShinglingMaxLayerNum(m_nGlobalProductPartIndex) / 2;
	if (nLayerMax >= 2)
	{
		m_fShinglingValueY = m_fShinglingValueYTotal / (nLayerMax - 1);
	}
	pDoc->m_Bookblock.SetShinglingValuesY(m_fShinglingValueY, m_nGlobalProductPartIndex);
	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);

	UpdateData(FALSE);
}

void CDlgShinglingAssistent::InitShinglingValuesYTotal() 
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	pDoc->m_Bookblock.SetShinglingValuesY(m_fShinglingValueY, m_nGlobalProductPartIndex);
	pDoc->m_PageTemplateList.CalcShinglingOffsets(m_fShinglingValueXTotal, m_fShinglingValueXFootTotal, m_fShinglingValueYTotal, m_nGlobalProductPartIndex);
	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);

	UpdateData(FALSE);
}

float CDlgShinglingAssistent::ShinglingValue2Display(CFoldSheet& rFoldSheet, float fValue, BOOL bShowPaperThickness)
{
	if (bShowPaperThickness)
		return fValue;
	else
	{
		int nLayers;
		if ( ! rFoldSheet.GetBoundProduct().m_bindingParams.m_bComingAndGoing)	// Normal
			nLayers = max(rFoldSheet.m_nPagesLeft, rFoldSheet.m_nPagesRight) / 2;
		else
			nLayers = max(rFoldSheet.m_nPagesLeft, rFoldSheet.m_nPagesRight) / 4;
		if (nLayers < 2)
			return 0.0f;
		else
			return fValue * (nLayers - 1);
	}
}

void CDlgShinglingAssistent::OnClose() 
{
	if ( ! m_bRunModal)
	{
		DestroyWindow();	
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));	// remove displayed shingling values
	}
	else
		CDialog::OnCancel();
}

void CDlgShinglingAssistent::PostNcDestroy() 
{
	m_pParentWnd = NULL;

	CDialog::PostNcDestroy();
}

void CDlgShinglingAssistent::OnCbnSelchangeShinglingProductpartCombo()
{
	UpdateData(TRUE);
	
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_nGlobalProductPartIndex = ProductPartLocalToGlobal(m_nLocalProductPartIndex);
	ChangeProductPart();

	CBookBlockProdDataView* pView = (CBookBlockProdDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockProdDataView));
	if (pView)
		pView->InitData();

	UpdateViews();

	UpdateData(FALSE);
}

void CDlgShinglingAssistent::ChangeProductPart()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	m_nProductIndex = rProductPart.GetBoundProduct().GetIndex();

	InitProductPartCombo();

	m_fShinglingValueX	   = rProductPart.m_foldingParams.GetShinglingValueX(rProductPart);
	m_fShinglingValueXFoot = rProductPart.m_foldingParams.GetShinglingValueXFoot(rProductPart);
	m_fShinglingValueY	   = rProductPart.m_foldingParams.GetShinglingValueY(rProductPart);

	pDoc->m_Bookblock.SetShinglingValuesX(m_fShinglingValueX, FLT_MAX, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, m_fShinglingValueXFoot, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesY(m_fShinglingValueY, m_nGlobalProductPartIndex);
	pDoc->m_PageTemplateList.CalcShinglingOffsets(m_fShinglingValueXTotal, m_fShinglingValueXFootTotal, m_fShinglingValueYTotal, m_nGlobalProductPartIndex);

	m_strPaperInfo = _T(" ") + rProductPart.InitPaperInfo();

	m_bShinglingActive	   = rProductPart.m_foldingParams.m_bShinglingActive;
	m_nShinglingMethod	   = rProductPart.m_foldingParams.m_nShinglingMethod;
	m_nShinglingDirection  = rProductPart.m_foldingParams.m_nShinglingDirection;
	m_strPaper			   = rProductPart.m_strPaper;
	m_fShinglingValueX	   = rProductPart.m_foldingParams.GetShinglingValueX(rProductPart);
	m_fShinglingValueXFoot = rProductPart.m_foldingParams.GetShinglingValueXFoot(rProductPart); 
	m_fShinglingValueY	   = rProductPart.m_foldingParams.GetShinglingValueY(rProductPart);

	m_paperDefsSelection.InitComboBox(this, m_strPaper, &m_nPaperGrammage, &m_fPaperVolume, &m_fShinglingValueX, IDC_SHINGLING_PAPER_COMBO);
	if (((CComboBox*)GetDlgItem(IDC_SHINGLING_PAPER_COMBO))->FindString(0, m_strPaper) < 0)
	{
		((CComboBox*)GetDlgItem(IDC_SHINGLING_PAPER_COMBO))->AddString(m_strPaper);
		((CComboBox*)GetDlgItem(IDC_SHINGLING_PAPER_COMBO))->SelectString(0, m_strPaper);
	}

	InitBottling();
	InitShinglingStyle();
	InitShinglingDirection();

	pDoc->m_Bookblock.SetShinglingValuesX(m_fShinglingValueX, FLT_MAX, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, m_fShinglingValueXFoot, m_nGlobalProductPartIndex);
	pDoc->m_Bookblock.SetShinglingValuesY(m_fShinglingValueY, m_nGlobalProductPartIndex);
	pDoc->m_PageTemplateList.CalcShinglingOffsets(m_fShinglingValueXTotal, m_fShinglingValueXFootTotal, m_fShinglingValueYTotal, m_nGlobalProductPartIndex);

	m_strPaperInfo = _T(" ") + 	rProductPart.InitPaperInfo();

	UpdateData(FALSE);
}

void CDlgShinglingAssistent::OnCbnSelchangeShinglingStyleCombo()
{
	UpdateData(TRUE);
	
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	BOOL bShinglingActiveOld = m_bShinglingActive;

	switch (m_nShinglingStyle)
	{
	case 0:	m_bShinglingActive = FALSE;	break;
	case 1: m_bShinglingActive = TRUE;	if (m_nShinglingMethod == CBookblock::ShinglingMethodScale) 
											m_nShinglingMethod = CBookblock::ShinglingMethodMove; 
										else
											if (m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale)
												m_nShinglingMethod = CBookblock::ShinglingMethodBottling; 
											else
												m_nShinglingMethod; 
										break;
	case 2: m_bShinglingActive = TRUE;	if (m_nShinglingMethod == CBookblock::ShinglingMethodBottling) 
											m_nShinglingMethod = CBookblock::ShinglingMethodBottlingScale;
										else
											if (m_nShinglingMethod == CBookblock::ShinglingMethodMove)	
												m_nShinglingMethod = CBookblock::ShinglingMethodScale;
											else
												m_nShinglingMethod;	
										break;
	}

	SetShinglingActive(m_bShinglingActive);
	SetShinglingMethod(m_nShinglingMethod);

	if (m_bShinglingActive != bShinglingActiveOld)	// if changed, panorama pages could be separated or not
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);

	InitBottling();
	InitShinglingValuesXTotal();
	InitShinglingValuesYTotal();
	InitShinglingStyle();

	UpdateViews();
	UpdateData(FALSE);
}

void CDlgShinglingAssistent::OnCbnSelchangeShinglingDirectionCombo()
{
	UpdateData(TRUE);
	
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	SetShinglingDirection(m_nShinglingDirection);

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);

	InitBottling();
	InitShinglingValuesXTotal();
	InitShinglingValuesYTotal();
	InitShinglingDirection();

	UpdateViews();
	UpdateData(FALSE);
}

void CDlgShinglingAssistent::OnBnClickedOk()
{
	if ( ! m_bRunModal)
	{
		DestroyWindow();	
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		theApp.OnUpdateView(RUNTIME_CLASS(CBookBlockDataView));
		theApp.UpdateView(RUNTIME_CLASS(CBookBlockDataView));
	}
	else
		OnOK();
}

void CDlgShinglingAssistent::OnBnClickedShinglingCropmarks()
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	SetShinglingCropMarks(m_bShinglingCropMarks);

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE, FALSE);
	
	UpdateViews();
}

void CDlgShinglingAssistent::SetShinglingMethod(int nShinglingMethod)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	rProductPart.m_foldingParams.m_nShinglingMethod	= nShinglingMethod;	
}

void CDlgShinglingAssistent::SetShinglingValueX(float fShinglingValueX)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	rProductPart.m_foldingParams.SetShinglingValueX(fShinglingValueX);	
}

void CDlgShinglingAssistent::SetShinglingValueY(float fShinglingValueY)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	rProductPart.m_foldingParams.SetShinglingValueY(fShinglingValueY);
}

void CDlgShinglingAssistent::SetShinglingValueXFoot(float fShinglingValueXFoot)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	rProductPart.m_foldingParams.SetShinglingValueXFoot(fShinglingValueXFoot);
}

void CDlgShinglingAssistent::SetShinglingActive(BOOL bShinglingActive)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	rProductPart.m_foldingParams.m_bShinglingActive	= bShinglingActive;
}

void CDlgShinglingAssistent::SetShinglingDirection(int nShinglingDirection)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	rProductPart.m_foldingParams.m_nShinglingDirection = nShinglingDirection;
}

void CDlgShinglingAssistent::SetShinglingCropMarks(BOOL bShinglingCropMarks)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(m_nGlobalProductPartIndex);
	rProductPart.m_foldingParams.m_bShinglingCropMarks = bShinglingCropMarks;		
}

