#pragma once



// COutputPlateColorsTable

class COutputPlateColorsTable : public CListCtrl
{
	DECLARE_DYNAMIC(COutputPlateColorsTable)

public:
	COutputPlateColorsTable();
	virtual ~COutputPlateColorsTable();

public:
	enum EHighlight {HIGHLIGHT_NORMAL, HIGHLIGHT_ALLCOLUMNS, HIGHLIGHT_ROW};
	enum Colors		{Cyan = -1, Magenta = -2, Yellow = -3, Key = -4};

	int m_nPrevSel;


public:
	int		m_nHighlight;		// Indicate type of selection highlighting
	CString m_strCyan;
	CString m_strMagenta;
	CString m_strYellow;
	CString m_strKey;



protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};


// CDlgPrintSheetColorSettings-Dialogfeld

class CDlgPrintSheetColorSettings : public CDialog
{
	DECLARE_DYNAMIC(CDlgPrintSheetColorSettings)

public:
	CDlgPrintSheetColorSettings(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgPrintSheetColorSettings();

// Dialogfelddaten
	enum { IDD = IDD_PRINTSHEETCOLORSETTINGS };

public:
	COutputPlateColorsTable m_plateColorsTable;


public:
	void InitPlateColorsTable();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedPdPrintcolorsButton();
};
