// MyRectTracker.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMyRectTracker window

class CMyRectTracker : public CRectTracker
{
// Construction
public:
	CMyRectTracker();
	CMyRectTracker(BOOL bLeftRightMBChanged);

// Attributes
public:


// Operations
public:
	BOOL m_bLeftRightMBChanged;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyRectTracker)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMyRectTracker();
	virtual BOOL TrackRubberBand(CWnd* pWnd, CPoint point, BOOL bAllowInvert);
	virtual BOOL TrackHandle(int nHandle, CWnd* pWnd, CPoint point, CWnd* pWndClipTo);


	// Generated message map functions
protected:
	//{{AFX_MSG(CMyRectTracker)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

};

/////////////////////////////////////////////////////////////////////////////
