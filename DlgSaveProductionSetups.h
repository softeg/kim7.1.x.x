#pragma once
#include "afxwin.h"


// CDlgSaveProductionSetups-Dialogfeld

class CDlgSaveProductionSetups : public CDialog
{
	DECLARE_DYNAMIC(CDlgSaveProductionSetups)

public:
	CDlgSaveProductionSetups(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgSaveProductionSetups();

// Dialogfelddaten
	enum { IDD = IDD_SAVEPRODUCTIONSETUPS };

public:
	CString m_strProductionSetupName;
	BOOL	m_bIsDefault;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
};
