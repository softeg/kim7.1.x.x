// MarkSourceWindow.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetMarksView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMarkSourceWindow

CMarkSourceWindow::CMarkSourceWindow()
{
	m_DisplayList.Create(this);
	m_pMarkSource = NULL;
}

CMarkSourceWindow::~CMarkSourceWindow()
{
}


BEGIN_MESSAGE_MAP(CMarkSourceWindow, CStatic)
	//{{AFX_MSG_MAP(CMarkSourceWindow)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMarkSourceWindow message handlers

void CMarkSourceWindow::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	CFont*	 pOldFont		= dc.SelectObject(((CDialog*)GetParent())->GetFont()); // use same font as the Dialog
	COLORREF crOldTextColor = dc.SetTextColor(BLACK);
	int		 nOldBkMode		= dc.SetBkMode(TRANSPARENT);
	HICON	 hIcon			= AfxGetApp()->LoadIcon(IDI_SOURCEMARK);

	if ( ! m_pMarkSource)
		return;

	m_DisplayList.Invalidate();
	m_DisplayList.BeginRegisterItems(&dc);

	//CPageSource& rMarkSource = pMarkSource;
	CString	strPath	   = m_pMarkSource->m_strDocumentTitle;
//	BOOL	bAllColors = ((CPrintSheetMarksView*)GetParent())->m_bAllColorsCheck;

	CRect clientRect;
	GetClientRect(&clientRect);

	CSize textExt;
	CRect textRect(5, 5, 5, 5);
	textExt					 = dc.GetTextExtent(strPath);
	textRect.right			 = textRect.left + textExt.cx;
	textRect.bottom			 = textRect.top  + textExt.cy;

	dc.TextOut(textRect.left, textRect.top - 2, strPath);

	m_DisplayList.RegisterItem(&dc, textRect, textRect, (void*)m_pMarkSource, 
							   DISPLAY_ITEM_REGISTRY(CMarkSourceWindow, PageSource));
	
	CRect	headerRect(textRect.left, textRect.bottom, textRect.left + 13, textRect.bottom + 15);
	CString string;
	int		nCurPageNum = 0;
	BOOL	bFirstSep	= FALSE;

/*	if (bAllColors)
	{
		dc.DrawIcon(headerRect.TopLeft(), hIcon);
	}
	else
*/
	{
		for (int i = 0; i < m_pMarkSource->m_PageSourceHeaders.GetSize(); i++)
		{
			CPageSourceHeader& rPageSourceHeader = m_pMarkSource->m_PageSourceHeaders[i];

			if (rPageSourceHeader.m_nPageNumber != nCurPageNum)
			{
				if (i != 0)
					headerRect.OffsetRect(6, 0);
				bFirstSep = TRUE;
			}
			else 
				bFirstSep = FALSE;

			nCurPageNum = rPageSourceHeader.m_nPageNumber;

			dc.DrawIcon(headerRect.TopLeft(), hIcon);

			if (rPageSourceHeader.m_rgb != WHITE)
			{
				CBrush colorBrush(rPageSourceHeader.m_rgb);
				CRect  colorRect(headerRect.CenterPoint(), headerRect.CenterPoint());
				colorRect.InflateRect(3, 3);
				colorRect.OffsetRect(1, 3);
				CBrush* pOldBrush = (CBrush*)dc.SelectObject(&colorBrush);
				dc.Rectangle(colorRect);
				dc.SelectObject(pOldBrush);
				colorBrush.DeleteObject();
			}

			m_DisplayList.RegisterItem(&dc, headerRect, headerRect, (void*)&rPageSourceHeader, 
									   DISPLAY_ITEM_REGISTRY(CMarkSourceWindow, PageSourceHeader), (void*)m_pMarkSource);

			headerRect.OffsetRect(10, 0);
			if (headerRect.right >= clientRect.right - 1)
				headerRect+= CSize(-(headerRect.left - clientRect.left - 2), headerRect.Height() + 2);
		}
	}

	m_DisplayList.EndRegisterItems(&dc);

	dc.SetTextColor(crOldTextColor);    
	dc.SetBkMode(nOldBkMode);

	if (pOldFont != NULL)
		dc.SelectObject(pOldFont);
}

BOOL CMarkSourceWindow::OnEraseBkgnd(CDC* pDC) 
{
	CRect rect;
	GetClientRect(rect);
	CBrush brush(LIGHTGRAY);
	pDC->FillRect(&rect, &brush);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////

void CMarkSourceWindow::OnLButtonDown(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point); // pass message to display list

	CStatic::OnLButtonDown(nFlags, point);
}

void CMarkSourceWindow::OnSelectPageSource(CDisplayItem* pDI, CPoint /*point*/) 
{

	CPageSource* pMarkSource = (CPageSource*)pDI->m_pData;

	for (int i = 0; i < pMarkSource->m_PageSourceHeaders.GetSize(); i++)
	{
		CDisplayItem* pDI = m_DisplayList.GetItemFromData(&pMarkSource->m_PageSourceHeaders[i], (void*)pMarkSource,
														  DISPLAY_ITEM_TYPEID(CMarkSourceWindow, PageSourceHeader));
		if (pDI)
			m_DisplayList.SelectItem(*pDI);
	}
/*
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bDeleteSelectionMenuAvtive	  = TRUE;
	pParentFrame->m_bAssigncolorsPsfileMenuAvtive = TRUE;
	pParentFrame->m_bPagesourcePropsMenuAvtive	  = FALSE;
*/
}
void CMarkSourceWindow::OnDeselectPageSource(CDisplayItem* /*pDI*/, CPoint /*point*/) 
{
/*
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bDeleteSelectionMenuAvtive	  = FALSE;
	pParentFrame->m_bAssigncolorsPsfileMenuAvtive = FALSE;
	pParentFrame->m_bPagesourcePropsMenuAvtive	  = FALSE;
*/
}
void CMarkSourceWindow::OnSelectPageSourceHeader(CDisplayItem* /*pDI*/, CPoint /*point*/) 
{
/*
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bAssigncolorsPsfileMenuAvtive = TRUE;
	pParentFrame->m_bPagesourcePropsMenuAvtive	  = TRUE;
*/
}
void CMarkSourceWindow::OnDeselectPageSourceHeader(CDisplayItem* /*pDI*/, CPoint /*point*/) 
{
/*
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bDeleteSelectionMenuAvtive	  = FALSE;
	pParentFrame->m_bAssigncolorsPsfileMenuAvtive = FALSE;
	pParentFrame->m_bPagesourcePropsMenuAvtive	  = FALSE;
*/
}

