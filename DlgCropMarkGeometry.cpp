// DlgCropMarks.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgCropMarkGeometry 


CDlgCropMarkGeometry::CDlgCropMarkGeometry(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgCropMarkGeometry::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgCropMarkGeometry)
	m_fCropMarksDistance = 0.0f;
	m_fCropMarksLineWidth = 0.0f;
	m_fCropMarksLength = 0.0f;
	//}}AFX_DATA_INIT

	m_pParent = NULL;
}

CDlgCropMarkGeometry::~CDlgCropMarkGeometry()
{
}

BOOL CDlgCropMarkGeometry::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgCropMarkGeometry::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgCropMarkGeometry)
	DDX_Control(pDX, IDC_CROPMARKS_LENGTH_SPIN, m_cropMarksLengthSpin);
	DDX_Control(pDX, IDC_CROPMARKS_LINEWIDTH_SPIN, m_cropMarksLineWidthSpin);
	DDX_Control(pDX, IDC_CROPMARKS_DISTANCE_SPIN, m_cropMarksDistanceSpin);
	DDX_Measure(pDX, IDC_CROPMARKS_DISTANCE, m_fCropMarksDistance);
	DDX_Measure(pDX, IDC_CROPMARKS_LINEWIDTH, m_fCropMarksLineWidth);
	DDX_Measure(pDX, IDC_CROPMARKS_LENGTH, m_fCropMarksLength);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgCropMarkGeometry, CDialog)
	//{{AFX_MSG_MAP(CDlgCropMarkGeometry)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CROPMARKS_DISTANCE_SPIN, OnDeltaposCropmarksDistanceSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CROPMARKS_LENGTH_SPIN, OnDeltaposCropmarksLengthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CROPMARKS_LINEWIDTH_SPIN, OnDeltaposCropmarksLinewidthSpin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgCropMarkGeometry 

BOOL CDlgCropMarkGeometry::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_pParent = (CDlgLayoutObjectControl*)GetParent();
	
	m_cropMarksDistanceSpin.SetRange (0, 1);
	m_cropMarksLengthSpin.SetRange	 (0, 1);
	m_cropMarksLineWidthSpin.SetRange(0, 1);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgCropMarkGeometry::OnDeltaposCropmarksDistanceSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_cropMarksDistanceSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgCropMarkGeometry::OnDeltaposCropmarksLengthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_cropMarksLengthSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgCropMarkGeometry::OnDeltaposCropmarksLinewidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_cropMarksLineWidthSpin.GetBuddy(), pNMUpDown->iDelta, 0.1f);
	
	*pResult = 1;
}

void CDlgCropMarkGeometry::LoadData()
{
	if ( ! m_hWnd)
		return;

	CMark* pMark = m_pParent->GetFirstMark();
	while (pMark)
	{
		if (pMark->m_nType == CMark::CropMark)
		{
			CCropMark* pCropMark = (CCropMark*)pMark;
			m_fCropMarksDistance  = pCropMark->m_fPageDistance;
			m_fCropMarksLineWidth = pCropMark->m_fLineWidth;
			m_fCropMarksLength	  = pCropMark->m_fLength;
		}

		pMark = m_pParent->GetNextMark(pMark);
	}

	UpdateData(FALSE);
}

void CDlgCropMarkGeometry::SaveData()
{
	if ( ! m_hWnd)
		return;

	UpdateData(TRUE);

	CMark* pMark = m_pParent->GetFirstMark();
	while (pMark)
	{
		if (pMark->m_nType == CMark::CropMark)
		{
			CCropMark* pCropMark = (CCropMark*)pMark;
			pCropMark->m_fPageDistance = m_fCropMarksDistance;
			pCropMark->m_fLineWidth	   = m_fCropMarksLineWidth;
			pCropMark->m_fLength	   = m_fCropMarksLength;
		}

		pMark = m_pParent->GetNextMark(pMark);
	}
}

void CDlgCropMarkGeometry::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}
