#pragma once

#include "PrintColorList.h"
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"



// CDlgSelectJobColors-Dialogfeld

class CDlgSelectJobColors : public CDialog
{
	DECLARE_DYNAMIC(CDlgSelectJobColors)

public:
	CDlgSelectJobColors(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgSelectJobColors();

// Dialogfelddaten
	enum { IDD = IDD_SELECT_JOBCOLORS };

public:
	CPrintColorList	m_printColorList;
	CColorantList	m_colorantList;
	CPageSource*	m_pPageSource;


public:
	void PDFEngineDataExchange(CPageSource* pNewObjectSource);



protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnOdAddColor();
	afx_msg void OnOdColorSingle();
	afx_msg void OnOdColorCmyk();
	afx_msg void OnOdRemoveColor();
	afx_msg void OnLvnItemchangedSysgenmarkColorlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOdColorImport();
};
