#pragma once

#include "OwnerDrawnButtonList.h"
#include "ProductsView.h"



// COrderDataFrameLeftToolsView-Formularansicht

class COrderDataFrameLeftToolsView : public CFormView
{
	DECLARE_DYNCREATE(COrderDataFrameLeftToolsView)

protected:
	COrderDataFrameLeftToolsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~COrderDataFrameLeftToolsView();

public:
	enum { IDD = IDD_ORDERDATAFRAMELEFTTOOLSVIEW};
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


public:
	COwnerDrawnButtonList	m_toolbarButtons;
	CMenu*					m_pPopupMenu;
	CPageSource*			m_pPageSource;
	CFlatProductList		m_flatProducts;
	int						m_nProductPartIndex;


public:
	CProductsView*	GetProductsView();
	static void		UpdateBackground(CDC* pDC, CRect rcRect, CWnd* /*pWnd*/);
	void			PDFEngineDataExchange(CPageSource* pNewObjectSource);


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnShowProducts();
	afx_msg void OnNewProduct();
	afx_msg void OnNewBound();
	afx_msg void OnNewFlat();
	afx_msg void OnDeleteProduct();
	afx_msg void OnImportProducts();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnImportXML();
	afx_msg void OnImportCSV();
	afx_msg void OnImportPDF();
	afx_msg void OnImportCFF();
	afx_msg void OnUpdateShowProducts(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNext(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNewProduct(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNewBound(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNewFlat(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteProduct(CCmdUI* pCmdUI);
	afx_msg void OnUpdateImportProducts(CCmdUI* pCmdUI);
	afx_msg void OnNext();
};


