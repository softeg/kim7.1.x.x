// DlgEditTempString.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgEditTempString.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgEditTempString dialog


CDlgEditTempString::CDlgEditTempString(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgEditTempString::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgEditTempString)
	m_strString = _T("");
	//}}AFX_DATA_INIT
}


void CDlgEditTempString::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgEditTempString)
	DDX_Text(pDX, IDC_EDIT_TEMP_STRING, m_strString);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgEditTempString, CDialog)
	//{{AFX_MSG_MAP(CDlgEditTempString)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgEditTempString message handlers

BOOL CDlgEditTempString::OnInitDialog() 
{
	if ( ! theApp.m_strTempTitle.IsEmpty())
		SetWindowText(theApp.m_strTempTitle);
	m_strString = theApp.m_strTemp;
	CDialog::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgEditTempString::OnOK() 
{
	CDialog::OnOK();
	theApp.m_strTemp = m_strString;
}
