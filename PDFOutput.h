// PDFOutput.h: Schnittstelle f�r die Klasse CPDFOutput.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PDFOUTPUT_H__CE248371_853A_11D3_8749_204C4F4F5020__INCLUDED_)
#define AFX_PDFOUTPUT_H__CE248371_853A_11D3_8749_204C4F4F5020__INCLUDED_

#pragma once

#include "PDFEngineInterface.h"

#define PDF_CMYK_BLACK		0, 0, 0, 1
#define PDF_CMYK_WHITE		0, 0, 0, 0
#define PDF_COLORSPACE_ALL	PDF_CMYK_BLACK, TRUE


class CSeparationInfoLogicalPage
{
public:
	CSeparationInfoLogicalPage();
	CSeparationInfoLogicalPage(const CSeparationInfoLogicalPage& rLogicalPage);

public:
	CArray <CString, CString&>	m_strColorants;
	CArray <int, int>			m_nSheetIndices;

public:
	const CSeparationInfoLogicalPage& operator=(const CSeparationInfoLogicalPage& rLogicalPage);
	BOOL  IsEmpty();
};

class CSeparationInfo 
{
enum {MaxNumColorants = 256};

public:
	CArray <CSeparationInfoLogicalPage, CSeparationInfoLogicalPage&> m_logicalPages;

	IPL_bool_t			m_callasIsNameArray[MaxNumColorants];
	IPL_utf8_char_t*	m_callasColorantArray[MaxNumColorants];
	IPL_int32_t			m_callasPageArray[MaxNumColorants];
	CSysToUTF8Converter m_sys2utf8;

public:
	void Reset();
	void NewLogicalPage();
	void Add(CString strColorant, int nSheetIndex);
	BOOL IsEmpty();
	void InitCallasArrays(CSeparationInfoLogicalPage& rLogicalPage);
	void Insert(IPL_int32_t hPDFFile);
};



class CSpotColorMapper
{
enum {MaxNumColors = 256};

public:
	CStringArray		m_strOldSpotColors;
	CStringArray		m_strNewSpotColors;
	IPL_utf8_char_t*	m_callasOldColor[MaxNumColors];
	IPL_utf8_char_t*	m_callasNewColor[MaxNumColors];
	CSysToUTF8Converter m_sys2utf8;

public:
	void Initialize(CPageTemplate* pObjectTemplate, CLayoutObject* pObject, CPlate* pPlate, CPageSourceHeader* pPageSourceHeader);
};


typedef 
struct _XYPair
{
	float fX, fY;
}XYPair;

class CClipPath : public CArray <XYPair, XYPair&>
{
public:
	CClipPath() {};
	CClipPath(CClipPath& rClipPath) 
	{ 
		RemoveAll(); 
		for (int i = 0; i < rClipPath.GetSize(); i++)
			Add(rClipPath[i]); 
	};
};

class CAddSysGenSpotColorStreamEntry
{
public:
	CAddSysGenSpotColorStreamEntry() {};

public:
	CString		m_strSpotColorName;
	COLORREF	m_crSpotColorAlternate;
	float		m_fSpotColorTintValue;
	CString		m_strPDFCode;
};

class CAddSysGenSpotColorStreams : public CArray <CAddSysGenSpotColorStreamEntry, CAddSysGenSpotColorStreamEntry&>
{
public:
	CAddSysGenSpotColorStreams() {};

public:

public:
	void AddEntry(const CString& strPDFCode, const CString& strColorName, COLORREF crAlternate, float fTintValue = 100.0f);
};

class CPDFOutput  
{
public:
	CPDFOutput(CPDFTargetProperties& rPDFTarget, CString strDocTitle);
	virtual ~CPDFOutput();


// Attributes
public:
	IPL_int32_t					m_hPDFFile;
	int							m_nError;
	BOOL						m_bCancelJob;
	float						m_fMediaWidth;
	float						m_fMediaHeight;
	CString						m_strTargetName;
	CString						m_strLocation;
	CString						m_strOutputPDFFile;
	CString						m_strFilenameTemplate;
	BOOL						m_bPDFOutputMime;
	BOOL						m_bJDFMarksOutput;
	int							m_nPSLevel;
	BOOL						m_bEmitHalftones;
	CString						m_strTmpOutputPDFFile;
	int							m_nTargetType;
	CString						m_strDocTitle;
	CString						m_strSysGenMarksStream;
	BOOL						m_bSysGenMarksColorSpaceALL;
	CAddSysGenSpotColorStreams	m_addSysGenSpotColorStreams;
	int							m_nCurrentPage;
	CMediaSize					m_media;
	int							m_nPlatesProcessed;
	CSeparationInfo				m_separationInfo;
	CSpotColorMapper			m_spotColorMapper;
	BOOL						m_bCalibration;
	BOOL						m_bOutputFormatExceeded;
	int							m_nDBCurrentPdfID;



// Operations
public:
	BOOL					OpenStream(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bSendIfPrinter, int nSide, CPlate* pPlate = NULL, BOOL bJDFMarksOutput = FALSE);
	BOOL					OpenStream(CBookblock* pBookblock, CFoldSheet* pFoldSheet, BOOL& bOverwriteAll, BOOL bSendIfPrinter, int nSide, BOOL bJDFMarksOutput = FALSE);
	BOOL					OpenStream(CFoldSheet* pFoldSheet, BOOL& bOverwriteAll, BOOL bSendIfPrinter, int nSide, CPlate* pPlate = NULL);
	void					CloseStream(BOOL bSendIfPrinter, BOOL bJDFMarksOutput = FALSE);
	static IPL_bool_t		ProgressCloseStream(IPL_int32_t lowrange, IPL_int32_t highrange, IPL_int32_t counter, void* lparam);
	void					SendToTarget();
	static DWORD CALLBACK	CopyProgressRoutine(LARGE_INTEGER TotalFileSize,	LARGE_INTEGER TotalBytesTransferred, 
												LARGE_INTEGER StreamSize,		LARGE_INTEGER StreamBytesTransferred,
												DWORD dwStreamNumber, DWORD dwCallbackReason, HANDLE hSourceFile, HANDLE hDestinationFile, LPVOID lpData);
	CString				GetSystemDefaultPrinter();
	void 				SetSystemDefaultPrinter(CString strDefaultPrinter);
	BOOL				ProcessPrintSheet		  (CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter = TRUE);
	BOOL				ProcessPrintSheetsJobInOne(CPrintSheetList* pPrintSheetList, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput, CPrintSheet** pPrintSheet, int* nSheetsOut = NULL, int nProductPartIndex = -1);
	BOOL				CheckTargetProps(CPrintSheet& rPrintSheet);
	BOOL				ProcessPrintSheetAllInOne (CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput);
	BOOL				ProcessPrintSheetSidewise (CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput);
	BOOL				ProcessPrintSheetPlatewise(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput);
	BOOL				ProcessBookblockJobInOne(CBookblock* pBookblock, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput, BOOL (*pFnProgressCallback)(int nTotalPages, int nCurrentPage, const CString& strFoldSheetNumber, LPVOID lpData));
	BOOL				ProcessBookblockDoublePage(POSITION pos, POSITION pos2, BOOL bJDFMarksOutput);
	BOOL				ProcessBookblockSinglePage(POSITION pos,				BOOL bJDFMarksOutput);
	BOOL				ProcessFoldSheetsJobInOne(CFoldSheetList* pPrintSheetList, BOOL& bOverwriteAll, BOOL bSendIfPrinter);
	BOOL				ProcessFoldSheetAllInOne (CFoldSheet* pFoldSheet, BOOL& bOverwriteAll, BOOL bSendIfPrinter);
	static float		GetPageEdge(CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet, int nSide);
	void				CheckOutputControlFrames  (CPrintSheet* pPrintSheet, int nSide);
	BOOL				ProcessPlate		(CPlate* pPlate, BOOL bJDFMarksOutput = FALSE);
	BOOL				ProcessTile			(CPlate* pPlate, BOOL bTiling = FALSE, float fXPos = 0.0f, float fYPos = 0.0f, int nRotation = 0, float fCorrectionX = 0.0f, float fCorrectionY = 0.0f, int nTile = 0, BOOL bJDFMarksOutput = FALSE, 
											 IPL_int32_t* pnSheetIndex = NULL, CLayoutObject* pFilterObject1 = NULL, CLayoutObject* pFilterObject2 = NULL);
	BOOL				ProcessKIM5Marks	(IPL_int32_t nSheetIndex, CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile, BOOL bJDFMarksOutput = FALSE);
	BOOL				ProcessControlFrames(CPrintSheet* pPrintSheet, IPL_int32_t nSheetIndex, CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile, 
											 CLayoutObject* pFilterObject1 = NULL, CLayoutObject* pFilterObject2 = NULL);
	BOOL				ProcessExposures	(IPL_int32_t nSheetIndex, CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile, BYTE nObjectType, int nMarkType = -1, int nZOrder = CFoldMark::Background, BOOL bJDFMarksOutput = FALSE, 
											 CString strMarkName = _T(""), CLayoutObject* pFilterObject1 = NULL, CLayoutObject* pFilterObject2 = NULL);
	BOOL				ProcessGraphics(CPrintSheet* pPrintSheet, IPL_int32_t nSheetIndex, CPlate* pPlate, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile);
	static void			CalcBottlingParams(float& fBottlingAngle, float& fBottlingOffsetX, float& fBottlingOffsetY,
										   CLayoutObject* pObject,   CPageTemplate* pObjectTemplate,
										   float fObjectBBoxCenterX, float fObjectBBoxCenterY);
	void				TransformToOutputMedia(int nRotation, float fCorrectionX, float fCorrectionY, float fMediaWidth, float fMediaHeight, 
											   float& fSheetClipBoxLeft, float& fSheetClipBoxBottom, float& fSheetClipBoxRight, float& fSheetClipBoxTop, 
											   float& fSheetExposureCenterX, float& fSheetExposureCenterY, int& nHeadPosition);
	void				TransformToOutputMedia(int nRotation, float fCorrectionX, float fCorrectionY, float fMediaWidth, float fMediaHeight, 
											   float& fSheetClipBoxLeft, float& fSheetClipBoxBottom, float& fSheetClipBoxRight, float& fSheetClipBoxTop);
	BOOL				AddPage(IPL_int32_t nSheetIndex, IPL_TOBJECTID* pObjectID,	CLayoutObject* pObject, CPageTemplate* pObjectTemplate, CPageSource* pPageSource, CPageSourceHeader* pPageSourceHeader,
																					float fSheetExposureCenterX,  float fSheetExposureCenterY, int nHeadPosition,
																					float fSheetClipBoxLeft,	  float fSheetClipBoxBottom, 
																					float fSheetClipBoxRight,	  float fSheetClipBoxTop,
																					float fObjectBBoxCenterX,	  float fObjectBBoxCenterY,
																					float fContentRotation,		  float fContentScaleX,		 float fContentScaleY,
																					float fBottlingAngle,	      float fBottlingOffsetX,	 float fBottlingOffsetY,
																					int   nObjectType,			  BOOL  bSkipIfDemo,		 BOOL bJDFMarksOutput);
	BOOL				PDFSheet_AddPage(IPL_int32_t nSheetIndex, IPL_TOBJECTID* pObjectID, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
																					float fSheetClipBoxLeft,	 float fSheetClipBoxBottom, 
																					float fSheetClipBoxRight,	 float fSheetClipBoxTop,
																					float fObjectBBoxCenterX,	 float fObjectBBoxCenterY,
																					float fContentRotation,		 float fContentScaleX,	float fContentScaleY,
																					float fBottlingAngle,	     float fBottlingOffsetX,	float fBottlingOffsetY,
																					int   nObjectType,			 BOOL  bSkipIfDemo, BOOL bJDFMarksOutput);
	static IPL_bool_t	SpotColorCallback(IPL_TPDFCODECOLOR* color, void* userdata);
	void				CheckCancelJob(CRect cancelRect, class CPrintSheetTableView* pView);
	void				SystemCreatedContent2Stream(int nSheetIndex, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
																     float fSheetClipBoxLeft,	  float fSheetClipBoxBottom, 
																     float fSheetClipBoxRight,	  float fSheetClipBoxTop,
																	 CLayoutObject* pObject, CPageTemplate* pObjectTemplate, CPageSource* pObjectSource,
																	 CPrintSheet* pPrintSheet, CPlate* pPlate, int nTile);
	static void			BuildCompositeColorInfo(CPageTemplate* pObjectTemplate, CPageSource* pObjectSource, CPlate* pPlate,
																	  BOOL& bColorSpaceAll, float& fC, float& fM, float& fY, float& fK, CStringArray& spotColorNames, CArray<COLORREF, COLORREF>& spotColorCMYKs, CArray<float, float>& spotColorTintValues);
	void				Text2Stream(CPlate* pPlate, int nTile, CString strText, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
																	  float fObjectBBoxCenterX,	  float fObjectBBoxCenterY, float fTextHeight, 
																	  BOOL bColorSpaceAll, float fC, float fM, float fY, float fK,
																      CStringArray* pSpotColorNames = NULL, CArray<COLORREF, COLORREF>* pSpotColorCMYKs = NULL, CReflinePositionParams* pReflinePosParams = NULL);
	void				CompositeColorText2Stream(CPlate* pPlate, int nTile, CString strText, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
																	  float fObjectBBoxCenterX,	  float fObjectBBoxCenterY, float fTextHeight, 
																	  float fC, float fM, float fY, float fK,
																      CStringArray* pSpotColorNames = NULL, CArray<COLORREF, COLORREF>* pSpotColorCMYKs = NULL, CReflinePositionParams* pReflinePosParams = NULL);
	void				SectionBar2Stream(float fBarWidth, float fBarHeight, BOOL bNumRotated180, BOOL bHollow,
																	  float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition, int nOrgHeadPosition,
																	  float fObjectBBoxCenterX,	   float fObjectBBoxCenterY,
																	  CLayoutObject* pObject, CPrintSheet* pPrintSheet,
																	  BOOL bColorSpaceAll, float fC, float fM, float fY, float fK,
																      CStringArray* pSpotColorNames = NULL, CArray<COLORREF, COLORREF>* pSpotColorCMYKs = NULL);
	void				CropLine2Stream(float fSheetExposureCenterX, float fSheetExposureCenterY, 
																      float fSheetClipBoxLeft,	  float fSheetClipBoxBottom, 
																      float fSheetClipBoxRight,	  float fSheetClipBoxTop,
																	  BOOL bColorSpaceAll, float fC, float fM, float fY, float fK,
																      CStringArray* pSpotColorNames = NULL, CArray<COLORREF, COLORREF>* pSpotColorCMYKs = NULL);
	void				FoldLine2Stream(BOOL bSolid, BOOL bTransparent, float fSheetExposureCenterX, float fSheetExposureCenterY, 
																      float fSheetClipBoxLeft,	  float fSheetClipBoxBottom, 
																      float fSheetClipBoxRight,	  float fSheetClipBoxTop,
																	  BOOL bColorSpaceAll, float fC, float fM, float fY, float fK,
																      CStringArray* pSpotColorNames = NULL, CArray<COLORREF, COLORREF>* pSpotColorCMYKs = NULL);
	void				CompositeColorBarcode2Stream(CPlate* pPlate, int nTile, CString strBarcodeText, const CString& strBarcodeType, CString strTextOption, float fTextSize, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition, 
																	  float fSheetClipBoxLeft, float fSheetClipBoxBottom, float fSheetClipBoxRight, float fSheetClipBoxTop,
									 								  float fC, float fM, float fY, float fK, CStringArray* pSpotColorNames = NULL, CArray<COLORREF, COLORREF>* pSpotColorCMYKs = NULL, CReflinePositionParams* pReflinePosParams = NULL);
	CSize				Barcode2Stream(CPlate* pPlate, int nTile, const CString& strBarcodeText, const CString& strBarcodeType, CString strTextOption, float fTextSize, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
																	  float fSheetClipBoxLeft,	   float fSheetClipBoxBottom, 
																	  float fSheetClipBoxRight,	   float fSheetClipBoxTop,
																	  BOOL bColorSpaceALL, float fC, float fM, float fY, float fK, CString strColorName = _T(""), 
																      CStringArray* pSpotColorNames = NULL, CArray<COLORREF, COLORREF>* pSpotColorCMYKs = NULL, CReflinePositionParams* pReflinePosParams = NULL);
	CSize				MakeBarcode(CString& strBarcode, IPL_TDMATRIX& textMatrix, const CString strResolvedText, const CString& strBarcodeText, const CString& strBarcodeType, CString strTextOption, float fTextSize, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
																	  float fSheetClipBoxLeft,	   float fSheetClipBoxBottom, 
																	  float fSheetClipBoxRight,	   float fSheetClipBoxTop);
	void				Rectangle2Stream(CPlate* pPlate, int nTile, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
																	  float fSheetClipBoxLeft,	   float fSheetClipBoxBottom, 
																	  float fSheetClipBoxRight,	   float fSheetClipBoxTop,
																	  BOOL bColorSpaceAll, float fC, float fM, float fY, float fK,
																      CStringArray* pSpotColorNames = NULL, CArray<COLORREF, COLORREF>* pSpotColorCMYKs = NULL, CArray<float, float>* pSpotColorTintValues = NULL);
	void				Frame2Stream(int nSheetIndex, float fFrameLeft, float fFrameBottom, float fFrameWidth, float fFrameHeight, CPlate* pPlate, CPageSource& rFrameSource);
	void				Path2Stream(int nSheetIndex, const CString& strStream, CPlate* pPlate, CPageSource& rFrameSource);
	void				AddSysGenStream(int nSheetIndex, const CString& strID, BOOL bOverprint = TRUE);
	void				EmptySysGenStream();
	BOOL				PDFSheet_AddSysGenStream (int nSheetIndex, CString strPDFStream, CString strID, BOOL bOverprint = TRUE);
	BOOL				PDFSheet_AddSysGenStream3(int nSheetIndex, CString strPDFStream, CString strColorName, COLORREF crAlternate, float fTintValue, CString strID);
	BOOL				PDFSheet_AddSysGenStream3(int nSheetIndex, CString strPDFStream, CString strColorName, float fC, float fM, float fY, float fK, float fTintValue, CString strID);
	static void			CalcPDFObjectOrigin (float& fOriginX, float& fOriginY, float fAngle, float fSheetExposureCenterX, float fSheetExposureCenterY, 
																							 float fObjectBBoxCenterX,	  float fObjectBBoxCenterY);
	static IPL_TDMATRIX CalcPDFObjectMatrix  (float fOriginX, float  fOriginY, float fAngle, float fScaleX, float fScaleY);
	static CClipPath	CalcPDFObjectClipPath(float fOriginX, float  fOriginY, float fAngle, float fBottlingAngle, 
										   float fSheetClipBoxLeft,  float fSheetClipBoxBottom, 
										   float fSheetClipBoxRight, float fSheetClipBoxTop);
	static inline BOOL	AngleEquals(double fAngle, double fRefAngle) { return ((fRefAngle - 0.001 < fAngle) && (fAngle < fRefAngle + 0.001)) ? TRUE : FALSE; };
	IPL_TCOLORMAPPER	InitSpotColorMapper(CLayoutObject* pObject, CPlate* pPlate, CPageSourceHeader* pPageSourceHeader);
	void				FreeSpotColorMapper(IPL_TCOLORMAPPER* pSpotColorMapper);
};

#endif // !defined(AFX_PDFOUTPUT_H__CE248371_853A_11D3_8749_204C4F4F5020__INCLUDED_)
