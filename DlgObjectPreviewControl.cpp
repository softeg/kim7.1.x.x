// DlgObjectPreviewControl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgObjectPreviewControl.h"
#include "PageListView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgObjectPreviewWindow.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgObjectPreviewControl 


CDlgObjectPreviewControl::CDlgObjectPreviewControl(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgObjectPreviewControl::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgObjectPreviewControl)
	//}}AFX_DATA_INIT
}

void CDlgObjectPreviewControl::Initialize(int nIDFrame, CWnd* pParent)
{
	CDialog::Create(IDD_OBJECT_PREVIEW_CONTROL, pParent);

	m_objectPreviewFrame.m_pParent = this;

	CRect frameRect, objectPreviewRect, scrollbarHRect, scrollbarVRect;
	m_scrollbarH.GetWindowRect(scrollbarHRect);
	m_scrollbarV.GetWindowRect(scrollbarVRect);

	pParent->GetDlgItem(nIDFrame)->GetWindowRect(frameRect);
	pParent->ScreenToClient(frameRect);
	SetWindowPos					 (NULL, frameRect.left, frameRect.top, frameRect.Width(), frameRect.Height(), SWP_NOZORDER);
	m_objectPreviewFrame.SetWindowPos(NULL, 0, 0, frameRect.Width() - scrollbarVRect.Width(), frameRect.Height() - scrollbarHRect.Height(), SWP_NOZORDER);
	m_objectPreviewFrame.GetWindowRect(objectPreviewRect);
	m_scrollbarH.SetWindowPos		 (NULL, 0, objectPreviewRect.Height(), objectPreviewRect.Width(), scrollbarHRect.Height(), SWP_NOZORDER);
	m_scrollbarV.SetWindowPos		 (NULL, objectPreviewRect.Width(),	0, scrollbarVRect.Width(), objectPreviewRect.Height(), SWP_NOZORDER);

	ReinitDialog();
}

void CDlgObjectPreviewControl::Move(int nIDFrame, CWnd* pParent)
{
	CRect frameRect, objectPreviewRect, scrollbarHRect, scrollbarVRect;
	m_scrollbarH.GetWindowRect(scrollbarHRect);
	m_scrollbarV.GetWindowRect(scrollbarVRect);

	pParent->GetDlgItem(nIDFrame)->GetWindowRect(frameRect);
	pParent->ScreenToClient(frameRect);
	SetWindowPos					 (NULL, frameRect.left, frameRect.top, frameRect.Width(), frameRect.Height(), SWP_NOZORDER);
	m_objectPreviewFrame.SetWindowPos(NULL, 0, 0, frameRect.Width() - scrollbarVRect.Width(), frameRect.Height() - scrollbarHRect.Height(), SWP_NOZORDER);
	m_objectPreviewFrame.GetWindowRect(objectPreviewRect);
	m_scrollbarH.SetWindowPos		 (NULL, 0, objectPreviewRect.Height(), objectPreviewRect.Width(), scrollbarHRect.Height(), SWP_NOZORDER);
	m_scrollbarV.SetWindowPos		 (NULL, objectPreviewRect.Width(),	0, scrollbarVRect.Width(), objectPreviewRect.Height(), SWP_NOZORDER);
}

void CDlgObjectPreviewControl::ReinitDialog()
{
	m_objectPreviewFrame.m_pParent = this;

	if ( ! GetLayoutObject() && ! GetMark())
	{
		m_objectPreviewFrame.ShowWindow(SW_HIDE);
		m_scrollbarV.ShowWindow(SW_HIDE);
		m_scrollbarH.ShowWindow(SW_HIDE);
	}
	else
	{
		m_objectPreviewFrame.ShowWindow(SW_SHOW);
		m_scrollbarV.ShowWindow(SW_SHOW);
		m_scrollbarH.ShowWindow(SW_SHOW);
		PrepareUpdatePreview();
	}
}

void CDlgObjectPreviewControl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgObjectPreviewControl)
	DDX_Control(pDX, IDC_OBJECT_PREVIEWFRAME, m_objectPreviewFrame);
	DDX_Control(pDX, IDC_PREVIEW_VSCROLLBAR, m_scrollbarV);
	DDX_Control(pDX, IDC_PREVIEW_HSCROLLBAR, m_scrollbarH);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgObjectPreviewControl, CDialog)
	//{{AFX_MSG_MAP(CDlgObjectPreviewControl)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgObjectPreviewControl 

void CDlgObjectPreviewControl::PrepareUpdatePreview()
{
	m_objectPreviewFrame.PrepareUpdatePreview();
}

void CDlgObjectPreviewControl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int nOldPos = pScrollBar->GetScrollPos();

	SCROLLINFO scrollInfo;
	pScrollBar->GetScrollInfo(&scrollInfo, SIF_ALL);
	int nNewPos = scrollInfo.nPos;
	switch (nSBCode)
	{
		case SB_LEFT:			break;
		case SB_ENDSCROLL:		break;
		case SB_LINELEFT:		nNewPos -= scrollInfo.nPage/10;	break;
		case SB_LINERIGHT:		nNewPos += scrollInfo.nPage/10;	break;
		case SB_PAGELEFT:		nNewPos -= scrollInfo.nPage;	break;
		case SB_PAGERIGHT:		nNewPos += scrollInfo.nPage;	break;
		case SB_RIGHT:			break;
		case SB_THUMBPOSITION:	
		case SB_THUMBTRACK:		scrollInfo.cbSize = sizeof(SCROLLINFO);
								pScrollBar->GetScrollInfo(&scrollInfo, SIF_TRACKPOS);
						        nNewPos = scrollInfo.nTrackPos;
								break;
	}
	if (nNewPos < 0)
		nNewPos = 0;
	else
		if (nNewPos > pScrollBar->GetScrollLimit())
			nNewPos = pScrollBar->GetScrollLimit();

	pScrollBar->SetScrollPos(nNewPos);

	CRect previewFrameRect;
	m_objectPreviewFrame.GetClientRect(previewFrameRect);
	previewFrameRect.DeflateRect(1, 1);
	m_objectPreviewFrame.ScrollWindow(nOldPos - nNewPos, 0, NULL, previewFrameRect);

	CRect rect;
	m_objectPreviewFrame.GetUpdateRect(rect);
	m_objectPreviewFrame.MapWindowPoints(this, rect);
	InvalidateRect(rect);
	UpdateWindow();
	
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CDlgObjectPreviewControl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int nOldPos = pScrollBar->GetScrollPos();

	SCROLLINFO scrollInfo;
	pScrollBar->GetScrollInfo(&scrollInfo, SIF_ALL);
	int nNewPos = scrollInfo.nPos;
	switch (nSBCode)
	{
		case SB_TOP:			break;
		case SB_ENDSCROLL:		break;
		case SB_LINEUP:			nNewPos -= scrollInfo.nPage/10;	break;
		case SB_LINEDOWN:		nNewPos += scrollInfo.nPage/10;	break;
		case SB_PAGEUP:			nNewPos -= scrollInfo.nPage;	break;
		case SB_PAGEDOWN:		nNewPos += scrollInfo.nPage;	break;
		case SB_BOTTOM:			break;
		case SB_THUMBPOSITION:	
		case SB_THUMBTRACK:		scrollInfo.cbSize = sizeof(SCROLLINFO);
								pScrollBar->GetScrollInfo(&scrollInfo, SIF_TRACKPOS);
						        nNewPos = scrollInfo.nTrackPos;
								break;
	}
	if (nNewPos < 0)
		nNewPos = 0;
	else
		if (nNewPos > pScrollBar->GetScrollLimit())
			nNewPos = pScrollBar->GetScrollLimit();

	pScrollBar->SetScrollPos(nNewPos);

	CRect previewFrameRect;
	m_objectPreviewFrame.GetClientRect(previewFrameRect);
	previewFrameRect.DeflateRect(1, 1);
	m_objectPreviewFrame.ScrollWindow(0, nOldPos - nNewPos, NULL, previewFrameRect);

	CRect rect;
	m_objectPreviewFrame.GetUpdateRect(rect);
	m_objectPreviewFrame.MapWindowPoints(this, rect);
	InvalidateRect(rect);
	UpdateWindow();
	
	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}

CMark* CDlgObjectPreviewControl::GetMark()
{
	CDlgObjectPreviewWindow* pDlg = (CDlgObjectPreviewWindow*)GetParent();
	if ( ! pDlg)
		return NULL;
	return pDlg->GetMark();
}

CLayoutObject* CDlgObjectPreviewControl::GetLayoutObject()
{
	CDlgObjectPreviewWindow* pDlg = (CDlgObjectPreviewWindow*)GetParent();
	if ( ! pDlg)
		return NULL;
	return pDlg->GetLayoutObject(GetMark());
}
