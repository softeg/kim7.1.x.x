#pragma once

#include "iostream"
#include "map"

#define PROJ_JDFTOOLSLIB
#include <jdf/util/PlatformUtils.h>
#include "JDF.h"

using namespace std;
using namespace JDF;



class CJDFOutputMIS
{
public:
	CJDFOutputMIS(void);
	~CJDFOutputMIS(void);


public:
	CString		m_strOutFile;
	CString		m_strLocation;
	int			m_nMarkOrd;
	JDFParser*	parser;
	JDFDoc*		m_pJDFDoc;	


public:
	BOOL Export				(CString strJDFJobTicketFile, CString strOutFile);
	BOOL ProcessProductPart	(JDFNode productNode);
	BOOL MakeSheet			(JDFSheet sheet, CPrintSheet& rPrintSheet);
	BOOL MakeSurface		(JDFSheet sheet, CPrintSheet& rPrintSheet, int nSide, CString strContentsBox);
	BOOL AddPage			(JDFSurface surface, float fSheetExposureCenterX, float fSheetExposureCenterY,	int nHeadPosition,			float fObjectTrimBoxLeft,	float fObjectTrimBoxBottom, 
										 float fObjectTrimBoxRight,	  float fObjectTrimBoxTop,		float fSheetClipBoxLeft,	float fSheetClipBoxBottom, 
										 float fSheetClipBoxRight,	  float fSheetClipBoxTop,		float fObjectBBoxCenterX,	float fObjectBBoxCenterY,
										 float fObjectTrimBoxWidth,	  float fObjectTrimBoxHeight,	float fContentRotation,		float fContentScaleX,		float fContentScaleY, BOOL bSkipIfDemo, int nOrd);
	BOOL AddContentObject	(JDFSurface surface, int nOrd, IPL_TDMATRIX& ctm, IPL_TDMATRIX& trimctm, IPL_TDRECT& sheetClipBox, IPL_TDRECT& objectTrimBox);
	BOOL AddMarksRunList	(JDFRunList runList, CString strName);
};
