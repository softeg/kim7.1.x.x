// DlgPDFEngineSettings.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgPDFEngineSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFEngineSettings 


CDlgPDFEngineSettings::CDlgPDFEngineSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPDFEngineSettings::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPDFEngineSettings)
	m_bAutoAssign = FALSE;
	m_bCreatePreviews = FALSE;
	m_strResolution = _T("");
	m_strDistillerSettings = _T("");
	//}}AFX_DATA_INIT
}


void CDlgPDFEngineSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPDFEngineSettings)
	DDX_Check(pDX, IDC_PDFLIB_STATUS_AUTOASSIGN, m_bAutoAssign);
	DDX_Check(pDX, IDC_PDFLIB_STATUS_CREATE_PREVIEWS, m_bCreatePreviews);
	DDX_CBString(pDX, IDC_PDFLIB_STATUS_RESOLUTION, m_strResolution);
	DDX_CBString(pDX, IDC_DIST_SETTINGS_COMBO, m_strDistillerSettings);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPDFEngineSettings, CDialog)
	//{{AFX_MSG_MAP(CDlgPDFEngineSettings)
	ON_BN_CLICKED(IDC_PDFLIB_STATUS_CREATE_PREVIEWS, OnPdflibStatusCreatePreviews)
	ON_BN_CLICKED(IDC_CHANGE_DIST_SETTINGS, OnChangeDistSettings)
	ON_CBN_SELCHANGE(IDC_DIST_SETTINGS_COMBO, OnSelchangeDistSettingsCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPDFEngineSettings 

BOOL CDlgPDFEngineSettings::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if (m_bCreatePreviews)
		GetDlgItem(IDC_PDFLIB_STATUS_RESOLUTION)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_PDFLIB_STATUS_RESOLUTION)->EnableWindow(FALSE);

	InitDistillerSettingsCombo();

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgPDFEngineSettings::InitDistillerSettingsCombo()
{
	long  lRetCode;
	HKEY  hkey;
	TCHAR szAcroDistPath[255];
	DWORD dwSize = sizeof(szAcroDistPath);
	DWORD dwType;

	lRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,	_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\AcroDist.exe"), 0,	KEY_READ, &hkey);
	if (lRetCode == ERROR_SUCCESS)
	{
		RegQueryValueEx(hkey, _T("Path"), 0, &dwType, (LPBYTE)szAcroDistPath, &dwSize);
		CString strAcroDistJoboptionsPath;
		strAcroDistJoboptionsPath.Format(_T("%s\\Settings\\*.joboptions"), szAcroDistPath);

		((CComboBox*)GetDlgItem(IDC_DIST_SETTINGS_COMBO))->ResetContent();

		CFileFind finder;
		BOOL	  bFound = finder.FindFile(strAcroDistJoboptionsPath);
		while (bFound)
		{
			bFound		 = finder.FindNextFile();
			((CComboBox*)GetDlgItem(IDC_DIST_SETTINGS_COMBO))->AddString(finder.GetFileTitle());
		}
		finder.Close();
	}
}

void CDlgPDFEngineSettings::OnPdflibStatusCreatePreviews() 
{
	UpdateData(TRUE);

	if (m_bCreatePreviews)
		GetDlgItem(IDC_PDFLIB_STATUS_RESOLUTION)->EnableWindow(TRUE);
	else
		GetDlgItem(IDC_PDFLIB_STATUS_RESOLUTION)->EnableWindow(FALSE);
}

void CDlgPDFEngineSettings::OnChangeDistSettings() 
{
	long			lRetCode;
	HKEY			hkey;
	unsigned char	szBuffer[255];
	DWORD			dwSize = sizeof(szBuffer);
	DWORD			dwType;

	lRetCode = RegOpenKeyEx(HKEY_LOCAL_MACHINE,	_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\AcroDist.exe"), 0,	KEY_READ, &hkey);

	if (lRetCode == ERROR_SUCCESS)
	{
		RegQueryValueEx(hkey, _T(""), 0, &dwType, szBuffer, &dwSize);
		CString strAcroDistCommand;
		strAcroDistCommand.Format(_T("%s"), szBuffer);

		dwSize = sizeof(szBuffer);
		RegQueryValueEx(hkey, _T("Path"), 0, &dwType, szBuffer, &dwSize);
		CString strParams;
		strParams.Format(_T("Acrodist.exe /E \"%s\\Settings\\%s.joboptions\""), szBuffer, m_strDistillerSettings);

		STARTUPINFO si = {0};
		PROCESS_INFORMATION pi = {0};		
		
		si.cb = sizeof(STARTUPINFO);
		si.lpReserved = NULL;
		si.lpReserved2 = NULL;
		si.cbReserved2 = 0;
		si.lpDesktop = NULL;
		si.dwFlags = 0;

		CreateProcess(strAcroDistCommand, strParams.GetBuffer(MAX_PATH), NULL, NULL, TRUE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi );

		WaitForSingleObject(pi.hProcess, INFINITE );

		InitDistillerSettingsCombo();	// maybe settings was safed under different filename -> reinit combo

		UpdateData(FALSE);
	}
	else
	{
		AfxMessageBox(_T("Distiller not installed"), MB_OK);
		return;
	}
}

void CDlgPDFEngineSettings::OnSelchangeDistSettingsCombo() 
{
	int nSel = ((CComboBox*)GetDlgItem(IDC_DIST_SETTINGS_COMBO))->GetCurSel();
	if (nSel == CB_ERR)
		return;

	((CComboBox*)GetDlgItem(IDC_DIST_SETTINGS_COMBO))->GetLBText(nSel, m_strDistillerSettings);
}

void CDlgPDFEngineSettings::OnOK() 
{
	UpdateData(TRUE);
	
	CDialog::OnOK();
}
