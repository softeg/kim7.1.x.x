// OrderDataHeaderView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "OrderDataHeaderView.h"
#include "OrderDataView.h"
#include "DlgOrderData.h"
#include "PageListFrame.h"
#include "PageListView.h"


// CODHCustomPaintFrame

IMPLEMENT_DYNAMIC(CODHCustomPaintFrame, CStatic)

CODHCustomPaintFrame::CODHCustomPaintFrame()
{
}

CODHCustomPaintFrame::~CODHCustomPaintFrame()
{
}

BEGIN_MESSAGE_MAP(CODHCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CODHCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	COrderDataHeaderView* pView = (COrderDataHeaderView*)GetParent();
	if ( ! pView)
		return;
	CRect rcFrame;
	GetClientRect(rcFrame);
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	switch (GetDlgCtrlID())
	{
	case IDC_ODH_JOBTITLE_HEADER:			{
												CPen pen(PS_SOLID, 1, MIDDLEGRAY);
												dc.SelectObject(&pen);
												CBrush brush(RGB(193,211,251));
												dc.SelectObject(&brush);
												//dc.RoundRect(rcFrame, CPoint(6,6));
												dc.Rectangle(rcFrame);
												dc.SetTextColor(DARKBLUE);
												dc.SetBkMode(TRANSPARENT);
												dc.SelectObject(&pView->m_bigFont);
												CString strOut;
												rcFrame.OffsetRect(15, 0);
												if (pDoc)
													dc.DrawText(pDoc->GetTitle(), rcFrame, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
											}
											break;

	default:								break;
	}
}


// COrderDataHeaderView

IMPLEMENT_DYNCREATE(COrderDataHeaderView, CFormView)

COrderDataHeaderView::COrderDataHeaderView()
	: CFormView(COrderDataHeaderView::IDD)
{
	m_bigFont.CreateFont(18, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
}

COrderDataHeaderView::~COrderDataHeaderView()
{
	m_bigFont.DeleteObject();
	m_hollowBrush.DeleteObject();
}


BEGIN_MESSAGE_MAP(COrderDataHeaderView, CFormView)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_ODH_MODIFY_ORDERDATA, &COrderDataHeaderView::OnBnClickedOdhModifyOrderdata)
END_MESSAGE_MAP()


// COrderDataHeaderView-Diagnose

#ifdef _DEBUG
void COrderDataHeaderView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void COrderDataHeaderView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG



// COrderDataHeaderView-Meldungshandler



void COrderDataHeaderView::OnInitialUpdate()
{
	m_jobTitleHeader.SubclassDlgItem(IDC_ODH_JOBTITLE_HEADER, this);

	CFormView::OnInitialUpdate();
}

void COrderDataHeaderView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if ( ! m_jobTitleHeader.m_hWnd)
		return;

	CRect rcTitle, rcTarget;
	m_jobTitleHeader.GetClientRect(rcTitle);
	m_jobTitleHeader.ClientToScreen(rcTitle);
	ScreenToClient(rcTitle);
	GetClientRect(rcTarget);
	m_jobTitleHeader.SetWindowPos(NULL, rcTitle.left, rcTitle.top, rcTarget.Width(), rcTitle.Height(), SWP_NOZORDER);
}

void COrderDataHeaderView::OnBnClickedOdhModifyOrderdata()
{
	CDlgOrderData dlg;

	dlg.DoModal();

	theApp.OnUpdateView(RUNTIME_CLASS(COrderDataView));
	theApp.UpdateView(RUNTIME_CLASS(COrderDataView));
}
