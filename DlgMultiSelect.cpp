// DlgMultiSelect.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgMultiSelect.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMultiSelect 


CDlgMultiSelect::CDlgMultiSelect(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgMultiSelect::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgMultiSelect)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void CDlgMultiSelect::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgMultiSelect)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgMultiSelect, CDialog)
	//{{AFX_MSG_MAP(CDlgMultiSelect)
	ON_BN_CLICKED(ID_SELECT_ALL, OnSelectAll)
	ON_BN_CLICKED(ID_SELECT_MARKS, OnSelectMarks)
	ON_BN_CLICKED(ID_SELECT_PAGES, OnSelectPages)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgMultiSelect 

void CDlgMultiSelect::OnSelectAll() 
{
	EndDialog(ID_SELECT_ALL);	
}

void CDlgMultiSelect::OnSelectMarks() 
{
	EndDialog(ID_SELECT_MARKS);	
}

void CDlgMultiSelect::OnSelectPages() 
{
	EndDialog(ID_SELECT_PAGES);	
}
