// DlgPlaceholderSyntaxInfo.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgPlaceholderSyntaxInfo.h"


// CDlgPlaceholderSyntaxInfo-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgPlaceholderSyntaxInfo, CDialog)

CDlgPlaceholderSyntaxInfo::CDlgPlaceholderSyntaxInfo(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPlaceholderSyntaxInfo::IDD, pParent)
{

}

CDlgPlaceholderSyntaxInfo::~CDlgPlaceholderSyntaxInfo()
{
}

void CDlgPlaceholderSyntaxInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgPlaceholderSyntaxInfo, CDialog)
END_MESSAGE_MAP()


// CDlgPlaceholderSyntaxInfo-Meldungshandler
