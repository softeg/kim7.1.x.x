#if !defined(AFX_DISTILLERSINK_H__F23BFEAD_5255_11D4_87A9_0040F68C1EF7__INCLUDED_)
#define AFX_DISTILLERSINK_H__F23BFEAD_5255_11D4_87A9_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DistillerSink.h : Header-Datei
//


// {F093C491-ED00-11D1-B976-00600802DB86}
static const IID IID_IDistillerSink =
{ 0xF093C491, 0xED00, 0x11D1, { 0xB9, 0x76, 0x0, 0x60, 0x08, 0x02, 0xDB, 0x86 } };



/////////////////////////////////////////////////////////////////////////////
// Befehlsziel CDistillerSink 

class CDistillerSink : public CCmdTarget
{
	DECLARE_DYNCREATE(CDistillerSink)

	CDistillerSink();           // Dynamische Erstellung verwendet geschützten Konstruktor

// Attribute
public:

// Operationen
public:

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDistillerSink)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementierung
protected:
	virtual ~CDistillerSink();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDistillerSink)
		// HINWEIS - Der Klassen-Assistent fügt hier Member-Funktionen ein und entfernt diese.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generierte OLE-Dispatch-Zuordnungsfunktionen
	//{{AFX_DISPATCH(CDistillerSink)
	afx_msg void OnJobDone(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF);
	afx_msg void OnJobStart(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF);
	afx_msg void OnLogMessage(LPCTSTR strMessage);
	afx_msg void OnPageNumber(long nPageNumber);
	afx_msg void OnJobFail(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF);
	afx_msg void OnPercentDone(long nPercentDone);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DISTILLERSINK_H__F23BFEAD_5255_11D4_87A9_0040F68C1EF7__INCLUDED_
