// ImpManDocCMark.cpp : implementation of the CImpManDoc subclass CPrintSheetList
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetMarksView.h"
#include "PrintSheetFrame.h"
#include "PageListView.h"
#include "DlgMarkSet.h"
#include "PrintSheetTableView.h"
#include "PDFEngineInterface.h"
#include "PDFOutput.h"
#include "ExpressionParser.h"
#include "GraphicComponent.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define MARKCOLOR				 RGB(245,187,0) //RGB(180,180,0)
#define MARKCOLOR_HIGHLIGHT		 RGB(170,130,0) //RGB(180,180,0)
#define MARKFRAMECOLOR_HIGHLIGHT RGB(50,40,0)
#define MIN_MARKSIZE 3
#define MINSIZE_MARK(markRect)\
{\
	CRect testRect = markRect;\
	pDC->LPtoDP(testRect);\
	if ( (testRect.Height() < MIN_MARKSIZE) || (testRect.Width() < MIN_MARKSIZE) )\
	{\
		testRect.top	-= MIN_MARKSIZE;\
		testRect.bottom += MIN_MARKSIZE;\
		testRect.left   -= MIN_MARKSIZE;\
		testRect.right  += MIN_MARKSIZE;\
	}\
	pDC->DPtoLP(testRect);\
	markRect = testRect;\
}


/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
// CKimRules 

IMPLEMENT_SERIAL(CKimRules, CObject, VERSIONABLE_SCHEMA | 1)


CKimRules::CKimRules()
{
}

CKimRules::~CKimRules()
{
}

const CKimRules& CKimRules::operator=(const CKimRules& rKimRules)
{
	Copy(rKimRules);

	return *this;
}

BOOL CKimRules::operator==(const CKimRules& rKimRules)
{
	if (m_ruleExpressions.GetCount() != rKimRules.m_ruleExpressions.GetCount())
		return FALSE;

	for (int i = 0; i < m_ruleExpressions.GetCount(); i++)
	{
		if (m_ruleExpressions[i] != rKimRules.m_ruleExpressions[i])
			return FALSE;
	}

	return TRUE;
}

BOOL CKimRules::operator!=(const CKimRules& rKimRules)
{
	return !(*this == rKimRules);
}

void CKimRules::Copy(const CKimRules& rKimRules)
{
	m_ruleExpressions.RemoveAll();
	m_ruleExpressions.Append(rKimRules.m_ruleExpressions);
}

void CKimRules::AddRuleExpression(CString strExpression)
{
	m_ruleExpressions.Add(strExpression);
}

void CKimRules::SetRuleExpression(int nIndex, CString strExpression)
{
	if (nIndex < 0)
		return;
	if (nIndex >= m_ruleExpressions.GetCount())
		m_ruleExpressions.Add(strExpression);
	else
		m_ruleExpressions[nIndex] = strExpression;
}

CString	CKimRules::GetRuleExpression(int nIndex)
{
	if ( (nIndex < 0) || (nIndex >= m_ruleExpressions.GetCount()) )
		return CString();
	else
		return m_ruleExpressions[nIndex];
}

void CKimRules::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CKimRules));

	if (ar.IsStoring())
	{
		m_ruleExpressions.Serialize(ar);
	}											
	else																		 
	{
		UINT nVersion;
		nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			m_ruleExpressions.Serialize(ar);
			break;
		}
	}
}

void CKimRules::RemoveAll()
{
	m_ruleExpressions.RemoveAll();
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMark

IMPLEMENT_SERIAL(CMark, CObject, VERSIONABLE_SCHEMA | 6)

CMark::CMark()
{ 
	m_bIsAutoMark	= FALSE;
	m_nType			= CustomMark;		// default type is custom mark
	m_nStatus		= Active;
	m_strMarkName	= "CustomMark";
	m_fMarkWidth	= 0.0f;
	m_fMarkHeight	= 0.0f;
	m_nHeadPosition	= TOP;
	m_bAllColors	= FALSE;
	m_nZOrderIndex	= -1;
	m_ColorSeparations.RemoveAll();
	m_MarkSource.Clear();
	m_markTemplate.m_ColorSeparations.RemoveAll();
	m_markTemplate.m_PrintSheetLocations.RemoveAll();
	m_rules.RemoveAll();
}

CMark::CMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, int nZOrderIndex, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
			 CPageSource& rMarkSource)
{ 
	m_bIsAutoMark	= FALSE;
	m_nType			= CustomMark;
	m_nStatus		= Active;
	m_strMarkName	= strName;
	m_fMarkWidth	= fWidth;
	m_fMarkHeight	= fHeight;
	m_bAllColors	= bAllColors;
	m_nZOrderIndex	= nZOrderIndex;
	m_ColorSeparations.RemoveAll();
	m_ColorSeparations.Copy(rColorSeps);
	m_MarkSource	= rMarkSource;
	m_nHeadPosition	= nHeadPosition;
	m_markTemplate.m_ColorSeparations.RemoveAll();
	m_markTemplate.m_PrintSheetLocations.RemoveAll();
	m_rules.RemoveAll();
}

// operator overload for CLayoutObject - needed by CList functions:
const CMark& CMark::operator=(const CMark& rMark)
{
	Copy(rMark);

	return *this;
}

BOOL CMark::operator==(const CMark& rMark)
{
	if (m_bIsAutoMark		!= rMark.m_bIsAutoMark)			return FALSE;
	if (m_nType				!= rMark.m_nType)				return FALSE;
	if (m_nStatus			!= rMark.m_nStatus)				return FALSE;
	if (m_strMarkName		!= rMark.m_strMarkName)			return FALSE;
	if (m_fMarkWidth		!= rMark.m_fMarkWidth)			return FALSE;
	if (m_fMarkHeight		!= rMark.m_fMarkHeight)			return FALSE;
	if (m_nHeadPosition		!= rMark.m_nHeadPosition)		return FALSE;
	if (m_bAllColors		!= rMark.m_bAllColors)			return FALSE;
	if (m_nZOrderIndex		!= rMark.m_nZOrderIndex)		return FALSE;
	if (m_ColorSeparations	!= rMark.m_ColorSeparations)	return FALSE;
	if (m_MarkSource		!= rMark.m_MarkSource)			return FALSE;
	if (m_markTemplate		!= rMark.m_markTemplate)		return FALSE;
	if (m_rules				!= rMark.m_rules)				return FALSE;

	return TRUE;
}

BOOL CMark::operator!=(const CMark& rMark)
{
	return !(*this == rMark);
}

void CMark::Copy(const CMark& rMark)
{
	m_bIsAutoMark	= rMark.m_bIsAutoMark;
	m_nType			= rMark.m_nType;
	m_nStatus		= rMark.m_nStatus;
	m_strMarkName	= rMark.m_strMarkName;
	m_fMarkWidth	= rMark.m_fMarkWidth;
	m_fMarkHeight	= rMark.m_fMarkHeight;
	m_nHeadPosition	= rMark.m_nHeadPosition;

	m_bAllColors	= rMark.m_bAllColors;
	m_nZOrderIndex	= rMark.m_nZOrderIndex;
	m_ColorSeparations.RemoveAll();
	m_ColorSeparations.Copy(rMark.m_ColorSeparations);
	m_MarkSource	= rMark.m_MarkSource;
	m_markTemplate	= rMark.m_markTemplate;
	m_rules			= rMark.m_rules;
}

void CMark::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CMark));

	if (ar.IsStoring())
	{
		ar << m_bIsAutoMark;
		ar << m_nType;
		ar << m_nStatus;
		ar << m_strMarkName;
		ar << m_fMarkWidth;
		ar << m_fMarkHeight;
		ar << m_nHeadPosition;
		ar << m_bAllColors;
		ar << m_nZOrderIndex;

		m_ColorSeparations.Serialize(ar);
		SerializeElements(ar, &m_MarkSource,   1);
		SerializeElements(ar, &m_markTemplate, 1);
		m_rules.Serialize(ar);
	}											
	else																		 
	{
		UINT nVersion;

		nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> m_nType;
			ar >> m_strMarkName;
			ar >> m_fMarkWidth;
			ar >> m_fMarkHeight;
			ar >> m_nHeadPosition;
			ar >> m_bAllColors;
			m_nStatus = Active;
			m_nZOrderIndex = 10000;		// middle layer (0 = top, INT_MAX = bottom)
			m_ColorSeparations.RemoveAll(); // no append, so target list has to be empty 
			m_ColorSeparations.Serialize(ar);
			SerializeElements(ar, &m_MarkSource,   1);
			m_bIsAutoMark = FALSE;
			break;

		case 2:
			ar >> m_nType;
			ar >> m_strMarkName;
			ar >> m_fMarkWidth;
			ar >> m_fMarkHeight;
			ar >> m_nHeadPosition;
			ar >> m_bAllColors;
			m_nStatus = Active;
			m_nZOrderIndex = 10000;
			m_ColorSeparations.RemoveAll(); // no append, so target list has to be empty 
			m_ColorSeparations.Serialize(ar);
			SerializeElements(ar, &m_MarkSource,   1);
			SerializeElements(ar, &m_markTemplate, 1);
			m_bIsAutoMark = FALSE;
			break;

		case 3:
			ar >> m_nType;
			ar >> m_strMarkName;
			ar >> m_fMarkWidth;
			ar >> m_fMarkHeight;
			ar >> m_nHeadPosition;
			ar >> m_bAllColors;
			m_nStatus = Active;
			m_nZOrderIndex = 10000;
			m_ColorSeparations.RemoveAll(); // no append, so target list has to be empty 
			m_ColorSeparations.Serialize(ar);
			SerializeElements(ar, &m_MarkSource,   1);
			SerializeElements(ar, &m_markTemplate, 1);
			m_rules.Serialize(ar);
			m_bIsAutoMark = (m_rules.GetRuleExpression(0).IsEmpty()) ? FALSE : TRUE;;
			break;

		case 4:
			ar >> m_nType;
			ar >> m_strMarkName;
			ar >> m_fMarkWidth;
			ar >> m_fMarkHeight;
			ar >> m_nHeadPosition;
			ar >> m_bAllColors;
			ar >> m_nZOrderIndex;
			m_nStatus = Active;
			m_ColorSeparations.RemoveAll(); // no append, so target list has to be empty 
			m_ColorSeparations.Serialize(ar);
			SerializeElements(ar, &m_MarkSource,   1);
			SerializeElements(ar, &m_markTemplate, 1);
			m_rules.Serialize(ar);
			m_bIsAutoMark = (m_rules.GetRuleExpression(0).IsEmpty()) ? FALSE : TRUE;
			break;

		case 5:
			ar >> m_nType;
			ar >> m_nStatus;
			ar >> m_strMarkName;
			ar >> m_fMarkWidth;
			ar >> m_fMarkHeight;
			ar >> m_nHeadPosition;
			ar >> m_bAllColors;
			ar >> m_nZOrderIndex;
			m_ColorSeparations.RemoveAll(); // no append, so target list has to be empty 
			m_ColorSeparations.Serialize(ar);
			SerializeElements(ar, &m_MarkSource,   1);
			SerializeElements(ar, &m_markTemplate, 1);
			m_rules.Serialize(ar);
			m_bIsAutoMark = (m_rules.GetRuleExpression(0).IsEmpty()) ? FALSE : TRUE;
			break;

		case 6:
			ar >> m_bIsAutoMark;
			ar >> m_nType;
			ar >> m_nStatus;
			ar >> m_strMarkName;
			ar >> m_fMarkWidth;
			ar >> m_fMarkHeight;
			ar >> m_nHeadPosition;
			ar >> m_bAllColors;
			ar >> m_nZOrderIndex;
			m_ColorSeparations.RemoveAll(); // no append, so target list has to be empty 
			m_ColorSeparations.Serialize(ar);
			SerializeElements(ar, &m_MarkSource,   1);
			SerializeElements(ar, &m_markTemplate, 1);
			m_rules.Serialize(ar);
			break;
		}

		//if (m_MarkSource.m_strDocumentFullpath.Find(_T("C:\\Programme\\KIM6.1\\PDF-Marks\\")) >= 0)
		//	m_MarkSource.m_strDocumentFullpath.Replace(_T("C:\\Programme\\KIM6.1\\PDF-Marks\\"), _T(".\\PDF-Marks\\"));
		//if (m_MarkSource.m_strContentFilesPath.Find(_T("C:\\Programme\\KIM6.1\\PDF-Marks\\")) >= 0)
		//	m_MarkSource.m_strContentFilesPath.Replace(_T("C:\\Programme\\KIM6.1\\PDF-Marks\\"), _T(".\\PDF-Marks\\"));
		//if (m_MarkSource.m_strPreviewFilesPath.Find(_T("C:\\Programme\\KIM6.1\\PDF-Marks\\")) >= 0)
		//	m_MarkSource.m_strPreviewFilesPath.Replace(_T("C:\\Programme\\KIM6.1\\PDF-Marks\\"), _T(".\\PDF-Marks\\"));
		//if (m_MarkSource.m_strDocumentTitle.Find(_T("C:\\\\Programme\\\\KIM6.1\\\\PDF-Marks\\")) >= 0)
		//	m_MarkSource.m_strDocumentTitle.Replace(_T("C:\\\\Programme\\\\KIM6.1\\\\PDF-Marks\\"), _T(".\\PDF-Marks\\"));

		//if (m_MarkSource.m_strDocumentFullpath.Find(_T("C:\\Programme\\KIM5.0\\PDF-Marks\\")) >= 0)
		//	m_MarkSource.m_strDocumentFullpath.Replace(_T("C:\\Programme\\KIM5.0\\PDF-Marks\\"), _T(".\\PDF-Marks\\"));
		//if (m_MarkSource.m_strContentFilesPath.Find(_T("C:\\Programme\\KIM5.0\\PDF-Marks\\")) >= 0)
		//	m_MarkSource.m_strContentFilesPath.Replace(_T("C:\\Programme\\KIM5.0\\PDF-Marks\\"), _T(".\\PDF-Marks\\"));
		//if (m_MarkSource.m_strPreviewFilesPath.Find(_T("C:\\Programme\\KIM5.0\\PDF-Marks\\")) >= 0)
		//	m_MarkSource.m_strPreviewFilesPath.Replace(_T("C:\\Programme\\KIM5.0\\PDF-Marks\\"), _T(".\\PDF-Marks\\"));
		//if (m_MarkSource.m_strDocumentTitle.Find(_T("C:\\\\Programme\\\\KIM5.0\\\\PDF-Marks\\")) >= 0)
		//	m_MarkSource.m_strDocumentTitle.Replace(_T("C:\\\\Programme\\\\KIM5.0\\\\PDF-Marks\\"), _T(".\\PDF-Marks\\"));

		if (m_nType != CustomMark)	// if system generated, marksourceheaders should not have any process colors eventually initialized in SerializeElements<CPageSourceHeader> -> remove
		{
			for (int i = 0; i < m_MarkSource.m_PageSourceHeaders.GetSize(); i++)
				m_MarkSource.m_PageSourceHeaders[i].m_processColors.RemoveAll();
		}
	}
}

BOOL CMark::GlobalCheckRules(CLayout* pLayout, int nMarkListIndex)
{
	if ( ! pLayout)
		return FALSE;

	BOOL		 bApplied = FALSE;
	int			 nState;
	CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		if (IsEnabled(pPrintSheet, FRONTSIDE))
		{
			if (IsEnabled(pPrintSheet, BACKSIDE))
				nState = BOTHSIDES;
			else
				nState = FRONTSIDE;
			bApplied = TRUE;
		}
		else
			if (IsEnabled(pPrintSheet, BACKSIDE))
			{
				nState = BACKSIDE;
				bApplied = TRUE;
			}
			else
				nState = 0;

		pPrintSheet->m_markStates.InsertAt(nMarkListIndex, nState);
		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}

	if ( ! bApplied)
	{
		pPrintSheet = pLayout->GetFirstPrintSheet();
		while (pPrintSheet)
		{
			pPrintSheet->m_markStates.RemoveAt(nMarkListIndex);
			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}
		return FALSE;
	}
	else
		return TRUE;
}

BOOL CMark::InstanceCheckRules(CPrintSheet* pPrintSheet, int nSide, CLayoutObject* pLayoutObject, int nComponentRefIndex)
{
	if ( ! pPrintSheet)
		return FALSE;
	if (pLayoutObject)
		if (pLayoutObject->m_nType != CLayoutObject::Page)
			pLayoutObject = NULL;

	return IsEnabled(pPrintSheet, nSide, pLayoutObject, nComponentRefIndex);
}

BOOL CMark::IsEnabled(CPrintSheet* pPrintSheet, int nSide, CLayoutObject* pLayoutObject, int nComponentRefIndex)
{
	if (m_rules.GetRuleExpression(0).IsEmpty())
		return TRUE;

	CExpressionParser parser;
	CString			  strExpression	   = m_rules.GetRuleExpression(0);
	BOOL			  bExpressionState = TRUE;
	int				  nRelation		   = CKimRules::RelationNone;
	int				  nCurPos		   = 0;
	CString			  strToken		   = strExpression.Tokenize(_T("\n"), nCurPos);
	while (strToken != _T("") && (strToken.Find(_T("[Action]")) < 0) )
	{
		strToken.TrimLeft(_T("(")); strToken.TrimRight(_T(")"));

		if (strToken.CompareNoCase(_T("and")) == 0) 
		{
			nRelation = CKimRules::RelationAnd;
			strToken = strExpression.Tokenize(_T("\n"), nCurPos);
			continue;
		}
		else
			if (strToken.CompareNoCase(_T("or")) == 0) 
			{
				nRelation = CKimRules::RelationOr;
				strToken = strExpression.Tokenize(_T("\n"), nCurPos);
				continue;
			}

		parser.ParseString(strToken);
		if (nRelation == CKimRules::RelationAnd)
			bExpressionState = bExpressionState & RuleExpressionIsTrue(parser.m_strVariable, parser.m_strOperator, parser.m_strValue, pPrintSheet, nSide, pLayoutObject, nComponentRefIndex);
		else
			if (nRelation == CKimRules::RelationOr)
				bExpressionState = bExpressionState | RuleExpressionIsTrue(parser.m_strVariable, parser.m_strOperator, parser.m_strValue, pPrintSheet, nSide, pLayoutObject, nComponentRefIndex);
			else
				bExpressionState = RuleExpressionIsTrue(parser.m_strVariable, parser.m_strOperator, parser.m_strValue, pPrintSheet, nSide, pLayoutObject, nComponentRefIndex);

		strToken = strExpression.Tokenize(_T("\n"), nCurPos);
	}

	parser.ParseString(strToken);
	if (parser.m_strVariable == _T("Action"))
	{
		if (bExpressionState)
		{
			if (parser.m_strValue == _T("DoAddMark")) 
				return TRUE;	
		}
		else
			if ( ! bExpressionState)
				if (parser.m_strValue == _T("DoNotAddMark"))
					return TRUE;
	}

	return FALSE;
}

BOOL CMark::RuleExpressionIsTrue(CString strVariable, CString strOperator, CString strValue, CPrintSheet* pPrintSheet, int nSide, CLayoutObject* pLayoutObject, int nComponentRefIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return FALSE;

	CBoundProductPart& rProductPart	 = CBoundProductPart();
	CBoundProduct&	   rProduct		 = CBoundProduct();
	CFlatProduct&	   rFlatProduct	 = CFlatProduct();
	CPageTemplate*	   pPageTemplate = (pLayoutObject) ? pLayoutObject->GetCurrentPageTemplate(pPrintSheet) : NULL;
	CFoldSheet*		   pFoldSheet	 = (nComponentRefIndex >= 0) ? pPrintSheet->GetFoldSheet(nComponentRefIndex) : ( (pPageTemplate) ? pPageTemplate->GetFoldSheet() : NULL);
	if (pPageTemplate) 
	{
		rProductPart = pPageTemplate->GetProductPart();
		rProduct	 = pPageTemplate->GetBoundProduct();
		rFlatProduct = pPageTemplate->GetFlatProduct();
	}
	else
		if (pFoldSheet) 
		{
			rProductPart = pFoldSheet->GetProductPart();
			rProduct	 = pFoldSheet->GetBoundProduct();
		}

	CString strResult;
	CString strPlaceholder;
	int		nResult = 0;
	float	fResult = 0.0f;
	int		nDataType = CKimRules::TypeNone;

	if ( (m_nType == CMark::CustomMark) || (m_nType == CMark::TextMark) || (m_nType == CMark::BarcodeMark) )
	{
		CReflineMark* pReflineMark = (CReflineMark*)this;
		if (pReflineMark->m_reflinePosParamsAnchor.m_nPlaceOn == CReflinePositionParams::PlaceOnFront)
			nSide = FRONTSIDE;
		else
			if (pReflineMark->m_reflinePosParamsAnchor.m_nPlaceOn == CReflinePositionParams::PlaceOnBack)
				nSide = BACKSIDE;
	}

	if (strVariable == _T("NoRule"))
	{
		return TRUE;
	}
	else
	if (strVariable == _T("PrintingMachine"))
	{
		strPlaceholder = _T("$F");	nDataType = CKimRules::TypeString;
		if ( ! strPlaceholder.IsEmpty())
		{
			CPrintSheetView::ResolvePlaceholders(strResult.GetBuffer(MAX_TEXT), (LPCTSTR)strPlaceholder, pPrintSheet, nSide, NULL, -1, _T(""), -1, -1);
			strResult.ReleaseBuffer();
		}
	}
	else
	if (strVariable == _T("WorkStyle"))
	{
		strResult = (pLayout) ? pLayout->GetPrintingType() : _T(""); nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("BindingStyle"))
	{
		strResult = rProduct.m_bindingParams.m_bindingDefs.m_strName; nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("NumColors"))
	{
		nResult = (pPrintSheet) ? pPrintSheet->GetNumColors(nSide) : 0; nDataType = CKimRules::TypeInteger;
	}
	else
	if (strVariable == _T("NumSpotColors"))
	{
		nResult = (pPrintSheet) ? pPrintSheet->GetNumColors(nSide, TRUE) : 0; nDataType = CKimRules::TypeInteger;
	}
	else
	if (strVariable == _T("SheetFormat"))
	{
		strResult = (pLayout) ? pLayout->m_FrontSide.m_strFormatName : _T(""); nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("SheetSide"))
	{
		nResult = nSide; nDataType = CKimRules::TypeInteger;
	}
	else
	if (strVariable == _T("SheetWidth"))
	{
		fResult = (pLayout) ? pLayout->m_FrontSide.m_fPaperWidth : 0.0f;  nDataType = CKimRules::TypeFloat;
	}
	else
	if (strVariable == _T("SheetHeight"))
	{
		fResult = (pLayout) ? pLayout->m_FrontSide.m_fPaperHeight : 0.0f;  nDataType = CKimRules::TypeFloat;
	}
	else
	if (strVariable == _T("PaperName"))
	{
		if (pPrintSheet)
		{
			CPrintGroup& rPrintGroup = pPrintSheet->GetPrintGroup();
			strResult = rPrintGroup.m_strPaper; nDataType = CKimRules::TypeString;
		}
	}
	else
	if (strVariable == _T("PaperType"))
	{
		if (pPrintSheet)
		{
			CPrintGroup& rPrintGroup = pPrintSheet->GetPrintGroup();
			nResult = rPrintGroup.m_nPaperType; nDataType = CKimRules::TypeInteger;
		}
	}
	else
	if (strVariable == _T("Color"))
	{
		if (nSide == BOTHSIDES)
		{
			strResult	   = (pPrintSheet) ? pPrintSheet->FindSeparation(strValue, FRONTSIDE) : _T(""); 
			int nNumColors = (pPrintSheet) ? pPrintSheet->GetNumColors(FRONTSIDE) : 0; 
			if (nNumColors > 1)		// "Color = xxx" means only if one color
			{
				strResult	   = (pPrintSheet) ? pPrintSheet->FindSeparation(strValue, BACKSIDE) : _T(""); 
				int nNumColors = (pPrintSheet) ? pPrintSheet->GetNumColors(BACKSIDE) : 0; 
				if (nNumColors > 1)		// "Color = xxx" means only if one color
					strResult = _T("");
			}
		}
		else
		{
			strResult	   = (pPrintSheet) ? pPrintSheet->FindSeparation(strValue, nSide) : _T("");
			int nNumColors = (pPrintSheet) ? pPrintSheet->GetNumColors(nSide) : 0; 
			if (nNumColors > 1)		// "Color = xxx" means only if one color
				strResult = _T("");
		}
		nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("DeviceID_PrintingMachine"))
	{
		CPressDevice* pPressDevice = (pLayout) ? ((nSide == BACKSIDE) ? pLayout->m_BackSide.GetPressDevice() : pLayout->m_FrontSide.GetPressDevice()) : NULL;
		strResult = (pPressDevice) ? pPressDevice->m_strDeviceID : _T(""); nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("ProductType"))
	{
		if ( ! pLayoutObject && (nComponentRefIndex == -1) )		// if neither layout object and nor foldsheet means we are checking globally -> skip this rule 
			return TRUE;

		if ( ! rProductPart.IsNull())
		{
			switch (rProduct.m_nProductType)
			{
			case CBoundProduct::Brochure:	strResult = _T("Brochure");	break;
			case CBoundProduct::Flyer:		strResult = _T("Flyer");	break;
			default:						strResult = _T("Bound");	break;
			}
		}
		else
			if (pPageTemplate)
				if (pPageTemplate->IsFlatProduct())
					strResult = _T("Label");
				else
					strResult = _T("Bound");
			else
				strResult = _T("Bound");
		nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("ProductPartName"))
	{
		if ( ! pLayoutObject && (nComponentRefIndex == -1) )		// if neither layout object and nor foldsheet means we are checking globally -> skip this rule 
			return TRUE;
		if (rProductPart.IsNull())	// if no product part means flat product
			return FALSE;

		strResult = rProductPart.m_strName;	nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("PageFormat"))
	{
		if ( ! pLayoutObject && (nComponentRefIndex == -1) )		// if neither layout object and nor foldsheet means we are checking globally -> skip this rule 
			return TRUE;
		if ( ! rProductPart.IsNull())
			strResult = rProductPart.m_strFormatName; 
		else
			strResult = rFlatProduct.m_strFormatName; 
		nDataType = CKimRules::TypeString;
	}
	else
	if (strVariable == _T("PageNum@Product"))
	{
		if ( ! pLayoutObject && (nComponentRefIndex == -1) )		// if neither layout object and nor foldsheet means we are checking globally -> skip this rule 
			return TRUE;
		if (rProduct.IsNull())
			return FALSE;

		if (strValue.CompareNoCase(_T("First")) == 0)
			strValue = _T("1");
		else
			if (strValue.CompareNoCase(_T("Last")) == 0)
				strValue.Format(_T("%d"), rProduct.m_nNumPages);

		nResult = (pPageTemplate) ? (pDoc->m_PageTemplateList.GetIndex(pPageTemplate, -pPageTemplate->m_nProductIndex - 1) + 1) : 1;
		nDataType = CKimRules::TypeInteger;
	}
	else
	if (strVariable == _T("PageNum@ProductPart"))
	{
		if ( ! pLayoutObject && (nComponentRefIndex == -1) )		// if neither layout object and nor foldsheet means we are checking globally -> skip this rule 
			return TRUE;
		if (rProductPart.IsNull())	
			return FALSE;

		if (strValue.CompareNoCase(_T("First")) == 0)
			strValue = _T("1");
		else
			if (strValue.CompareNoCase(_T("Last")) == 0)
				strValue.Format(_T("%d"), rProductPart.m_nNumPages);

		nResult = (pPageTemplate) ? (pDoc->m_PageTemplateList.GetIndex(pPageTemplate, pPageTemplate->m_nProductPartIndex) + 1) : 1;
		nDataType = CKimRules::TypeInteger;
	}
	else
	if (strVariable == _T("PageNum@FoldSheet"))
	{
		if ( ! pLayoutObject && (nComponentRefIndex == -1) )		// if neither layout object and nor foldsheet means we are checking globally -> skip this rule 
			return TRUE;

		if (strValue.CompareNoCase(_T("First")) == 0)
			strValue = _T("1");
		else
			if (strValue.CompareNoCase(_T("Last")) == 0)
				strValue.Format(_T("%d"), (pFoldSheet) ? pFoldSheet->GetNumPages() : 1);

		nResult = (pFoldSheet) ? pFoldSheet->GetPageIndex(pPageTemplate) + 1 : 0;
		nDataType = CKimRules::TypeInteger;
	}
	else
	if (strVariable == _T("FoldSheetSide"))
	{
		if ( ! pLayoutObject && (nComponentRefIndex == -1) )		// if neither layout object and nor foldsheet means we are checking globally -> skip this rule 
			return TRUE;

		nSide			   = (pLayoutObject) ? pLayoutObject->GetLayoutSideIndex() : nSide;
		nComponentRefIndex = (pLayoutObject) ? pLayoutObject->m_nComponentRefIndex : nComponentRefIndex;
		if (nSide == FRONTSIDE)
			if ( ! pLayout->m_FoldSheetLayerTurned[nComponentRefIndex] )
				strResult = _T("Front");
			else
				strResult = _T("Back");
		else 
			if (pLayout->m_FoldSheetLayerTurned[nComponentRefIndex] )
				strResult = _T("Front");
			else
				strResult = _T("Back");

		nDataType = CKimRules::TypeString;
	}

	if (strOperator == _T("="))
	{
		switch (nDataType)
		{
		case CKimRules::TypeString:		strResult.MakeLower(); strValue.MakeLower();
										if (strResult == strValue)
											return TRUE;
										break;
		case CKimRules::TypeInteger:	if (nResult == _ttoi(strValue))
											return TRUE;
										break;
		case CKimRules::TypeFloat:		if (fResult == Ascii2Float(strValue))
											return TRUE;
										break;
		}
	}
	else
	if (strOperator == _T("!="))
	{
		switch (nDataType)
		{
		case CKimRules::TypeString:		strResult.MakeLower(); strValue.MakeLower();
										if (strResult != strValue)
											return TRUE;
										break;
		case CKimRules::TypeInteger:	if (nResult != _ttoi(strValue))
											return TRUE;
										break;
		case CKimRules::TypeFloat:		if (fResult != Ascii2Float(strValue))
											return TRUE;
										break;
		}
	}
	else
	if (strOperator == _T("<"))
	{
		switch (nDataType)
		{
		case CKimRules::TypeString:		if (strResult < strValue)
											return TRUE;
										break;
		case CKimRules::TypeInteger:	if (nResult < _ttoi(strValue))
											return TRUE;
										break;
		case CKimRules::TypeFloat:		if (fResult < Ascii2Float(strValue))
											return TRUE;
										break;
		}
	}
	else
	if (strOperator == _T(">"))
	{
		switch (nDataType)
		{
		case CKimRules::TypeString:		if (strResult > strValue)
											return TRUE;
										break;
		case CKimRules::TypeInteger:	if (nResult > _ttoi(strValue))
											return TRUE;
										break;
		case CKimRules::TypeFloat:		if (fResult > Ascii2Float(strValue))
											return TRUE;
										break;
		}
	}
	else
	if (strOperator.CompareNoCase(_T("INCLUDE")) == 0)
	{
		switch (nDataType)
		{
		case CKimRules::TypeString:		strResult.MakeLower(); strValue.MakeLower();
										if (strResult.Find(strValue) >= 0)
											return TRUE;
										break;
		case CKimRules::TypeInteger:	
		case CKimRules::TypeFloat:		break;
		}
	}

	return FALSE;
}

void CMark::InitializeTemplate(CColorDefinitionTable& rColorDefTable)
{
	m_markTemplate.Create(m_strMarkName, CPageTemplate::CustomMark, 0.0f, 0.0f, -1, -1);

	SEPARATIONINFO sepInfo = {-1, 1, 0};
	if (m_markTemplate.m_ColorSeparations.GetSize())
		sepInfo = m_markTemplate.m_ColorSeparations[0];

	m_markTemplate.m_ColorSeparations.RemoveAll();

	if (m_bAllColors)
	{
		CColorDefinition colorDef(_T("Composite"), WHITE, -1, 0);
		sepInfo.m_nColorDefinitionIndex = rColorDefTable.InsertColorDef(colorDef);
		m_markTemplate.m_ColorSeparations.Add(sepInfo);
		m_markTemplate.m_bMap2AllColors = TRUE;

		if ( (m_nType != CustomMark) && ! m_MarkSource.m_PageSourceHeaders.GetSize() )
		{
			CPageSourceHeader markSourceHeader;
			markSourceHeader.Create(1, _T("1"), 0.0f, 0.0f, 0.0f, 0.0f, colorDef.m_strColorName, FALSE);
			markSourceHeader.m_rgb				= colorDef.m_rgb;
			markSourceHeader.m_bPageNumberFix   = FALSE;
			m_MarkSource.m_PageSourceHeaders.Add(markSourceHeader);
		}
	}
	else
	{	// take ColorSeparationInfo from the assigned colors
		for (short i = 0; i < m_MarkSource.m_PageSourceHeaders.GetSize(); i++)
		{
			CPageSourceHeader& rMarkSourceHeader = m_MarkSource.m_PageSourceHeaders[i];
			CColorDefinition colorDef(rMarkSourceHeader.m_strColorName, rMarkSourceHeader.m_rgb, CColorDefinition::Spot, rMarkSourceHeader.m_nColorIndex);
			SEPARATIONINFO sepInfo;
			sepInfo.m_nPageSourceIndex		= -1;
			sepInfo.m_nPageNumInSourceFile	= (short)(i + 1);
			sepInfo.m_nColorDefinitionIndex = rColorDefTable.InsertColorDef(colorDef);

			m_markTemplate.m_ColorSeparations.Add(sepInfo);
		}

		if (m_nType != CustomMark)
			if (m_MarkSource.m_PageSourceHeaders.GetSize() == 1)	// Compatibility: old marks with only composite are switched to colorspace all
																	// in order to get same output behaviour
				if (m_MarkSource.m_PageSourceHeaders[0].m_strColorName.CompareNoCase(_T("Composite")) == 0)
				{
					m_bAllColors = TRUE;
					m_markTemplate.m_bMap2AllColors = TRUE;
				}
	}
}

CPageTemplate CMark::TransformColorDefs(CColorDefinitionTable& rColorDefTable, short nMarkSourceIndex)
{
	CPageTemplate templateTrans = m_markTemplate;
	CImpManDoc*	  pDoc			= CImpManDoc::GetDoc();
	if ( ! pDoc)
		return templateTrans;

	for (int i = 0; i < templateTrans.m_ColorSeparations.GetSize(); i++)
	{
		SEPARATIONINFO& sepInfo = templateTrans.m_ColorSeparations[i];

		sepInfo.m_nPageSourceIndex = nMarkSourceIndex;

		if ( (sepInfo.m_nColorDefinitionIndex < 0) || (sepInfo.m_nColorDefinitionIndex >= rColorDefTable.GetSize()) )
			continue;

		CColorDefinition& rColorDef	= rColorDefTable[sepInfo.m_nColorDefinitionIndex];

		POSITION pos = pDoc->m_MarkSourceList.FindIndex(nMarkSourceIndex);
		if (pos)
		{
			CPageSource& rMarkSource = pDoc->m_MarkSourceList.GetAt(pos);
			int nIndex = sepInfo.m_nPageNumInSourceFile - 1;
			if ( (nIndex >= 0) && (nIndex < rMarkSource.m_PageSourceHeaders.GetSize()) )
			{
				CPageSourceHeader& rMarkSourceHeader = rMarkSource.m_PageSourceHeaders[nIndex];
				if (rMarkSourceHeader.m_strColorName != rColorDef.m_strColorName)		// if colordef differs from pagesourceheader color -> correct it
				{
					if (rMarkSourceHeader.m_strColorName.CompareNoCase(_T("Composite")) == 0)		
					{
						CColorDefinition colorDef(_T("Composite"), WHITE, -1, 0);
						sepInfo.m_nColorDefinitionIndex = pDoc->m_ColorDefinitionTable.InsertColorDef(colorDef);
						continue;
					}
					else
					{
						CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(rMarkSourceHeader.m_strColorName);
						if (pColorDef)
						{
							sepInfo.m_nColorDefinitionIndex = pDoc->m_ColorDefinitionTable.InsertColorDef(*pColorDef);
							continue;
						}
					}
				}
			}
		}

		sepInfo.m_nColorDefinitionIndex = pDoc->m_ColorDefinitionTable.InsertColorDef(rColorDef);
	}

	m_markTemplate = templateTrans;

	return templateTrans;
}

SEPARATIONINFO CMark::GetSeparationInfo(CPlate* pPlate, CColorDefinitionTable& rColorDefTable, BOOL bDoMapping)
{
	SEPARATIONINFO nullSepInfo = {-1, -1, -1};
	if ( ! pPlate)
		return nullSepInfo;

	if (m_markTemplate.m_bMap2AllColors)
	{
		SEPARATIONINFO sepInfo = {-1, 1, (m_markTemplate.m_ColorSeparations.GetSize()) ? m_markTemplate.m_ColorSeparations[0].m_nColorDefinitionIndex : 
																						 pPlate->m_nColorDefinitionIndex};
		return sepInfo;
	}
	else
	{
		if (bDoMapping)
		{
			for (int nSepIndex = 0; nSepIndex < m_markTemplate.m_ColorSeparations.GetSize(); nSepIndex++)
			{
				if ( (m_markTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex >= 0) && (m_markTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex < rColorDefTable.GetSize()) )
				{
					CString strColor = rColorDefTable[m_markTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex].m_strColorName;
					if ( (theApp.MapPrintColor(strColor, &m_markTemplate, pPlate->GetPrintSheet(), pPlate->GetSheetSide()) == pPlate->m_strPlateName) || pPlate->m_strPlateName.IsEmpty() )
						return m_markTemplate.m_ColorSeparations[nSepIndex];																			 // empty plate name means default plate -> always use 1st sep
				}
			}
		}
		else
		{
			for (int nSepIndex = 0; nSepIndex < m_ColorSeparations.GetSize(); nSepIndex++)
			{
				if ( (m_markTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex >= 0) && (m_markTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex < rColorDefTable.GetSize()) )
				{
					CString strColor = rColorDefTable[m_markTemplate.m_ColorSeparations[nSepIndex].m_nColorDefinitionIndex].m_strColorName;
					if (strColor == pPlate->m_strPlateName)
						return m_markTemplate.m_ColorSeparations[nSepIndex];
				}
			}
		}

		if (m_markTemplate.m_bMap2SpotColors)
		{
			CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(pPlate->m_strPlateName);
			if (pColorDef)
				if (pColorDef->m_nColorType == CColorDefinition::Spot)
				{
					SEPARATIONINFO sepInfo = {-1, 1, pPlate->m_nColorDefinitionIndex};
					return sepInfo;
				}
		}

		return nullSepInfo;
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////
// virtual base class implementations do nothing

void* CMark::GetSheetTrimBox(void*, void*, LPARAM, LPARAM, float&, float&, float&, float&)
{
	return NULL;
}


void CMark::GetBBoxAll(CPrintSheet*, int, float&, float&, float&, float&)
{
}


int CMark::AddToSheet(CPrintSheet*, int, CPageTemplate*, CMarkList*)
{
	return 0;
}


void CMark::DrawSheetPreview(CDC*, CPrintSheet*, int, CMarkList*, BOOL, int, BOOL)
{
}




//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// CReflineMark

IMPLEMENT_SERIAL(CReflineMark, CObject, VERSIONABLE_SCHEMA | 1)

CReflineMark::CReflineMark()
{ 
	m_nType			=  CustomMark;		// default type is custom mark
	m_strMarkName	= "CustomMark";
	m_fMarkWidth	= 0.0f;
	m_fMarkHeight	= 0.0f;
	m_nHeadPosition	= TOP;
	m_bAllColors	= FALSE;
	m_ColorSeparations.RemoveAll();
	m_MarkSource.Clear();
	m_markTemplate.m_ColorSeparations.RemoveAll();
	m_markTemplate.m_PrintSheetLocations.RemoveAll();
	m_rules.RemoveAll();

	m_reflinePosParamsAnchor.m_pParentMark  = this;
	m_reflinePosParamsLL.m_pParentMark		= this;
	m_reflinePosParamsUR.m_pParentMark		= this;
	m_bVariableWidth = m_bVariableHeight	= FALSE;
}

CReflineMark::CReflineMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
			 CPageSource& rMarkSource)
{ 
	m_nType			= CustomMark;
	m_strMarkName	= strName;
	m_fMarkWidth	= fWidth;
	m_fMarkHeight	= fHeight;
	m_bAllColors	= bAllColors;
	m_ColorSeparations.RemoveAll();
	m_ColorSeparations.Copy(rColorSeps);
	m_MarkSource	= rMarkSource;
	m_nHeadPosition	= nHeadPosition;
	m_markTemplate.m_ColorSeparations.RemoveAll();
	m_markTemplate.m_PrintSheetLocations.RemoveAll();
	m_rules.RemoveAll();

	m_reflinePosParamsAnchor.m_pParentMark  = this;
	m_reflinePosParamsLL.m_pParentMark		= this;
	m_reflinePosParamsUR.m_pParentMark		= this;
	m_bVariableWidth = m_bVariableHeight	= FALSE;
}

BOOL CReflineMark::operator==(const CReflineMark& rReflineMark)
{
	if (CMark::operator!=(rReflineMark))									return FALSE;
	if (m_reflinePosParamsAnchor != rReflineMark.m_reflinePosParamsAnchor)	return FALSE;
	if (m_reflinePosParamsLL	 != rReflineMark.m_reflinePosParamsLL)		return FALSE;
	if (m_reflinePosParamsUR	 != rReflineMark.m_reflinePosParamsUR)		return FALSE;
	if (m_bVariableWidth		 != rReflineMark.m_bVariableWidth)			return FALSE;
	if (m_bVariableHeight		 != rReflineMark.m_bVariableHeight)			return FALSE;

	return TRUE;
}

BOOL CReflineMark::operator!=(const CReflineMark& rReflineMark)
{
	return !(*this == rReflineMark);
}

void CReflineMark::Copy(const CReflineMark& rReflineMark)
{
	CMark::Copy(rReflineMark);

	m_reflinePosParamsAnchor = rReflineMark.m_reflinePosParamsAnchor;
	m_reflinePosParamsLL	 = rReflineMark.m_reflinePosParamsLL;
	m_reflinePosParamsUR	 = rReflineMark.m_reflinePosParamsUR;
	m_bVariableWidth		 = rReflineMark.m_bVariableWidth;
	m_bVariableHeight		 = rReflineMark.m_bVariableHeight;
}

void CReflineMark::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CReflineMark));
	UINT nVersion = ar.GetObjectSchema();

	CMark::Serialize(ar);

	if (ar.IsStoring())
	{
		m_reflinePosParamsAnchor.Serialize(ar);
		m_reflinePosParamsLL.Serialize(ar);
		m_reflinePosParamsUR.Serialize(ar);
		ar << m_bVariableWidth;
		ar << m_bVariableHeight;
	}											
	else																		 
	{
		switch (nVersion)
		{
		case 1:	m_reflinePosParamsAnchor.Serialize(ar);	
				m_reflinePosParamsLL.Serialize(ar);
				m_reflinePosParamsUR.Serialize(ar);
				ar >> m_bVariableWidth;
				ar >> m_bVariableHeight;
				break;
		}
	}
}

CReflinePositionParams* CReflineMark::GetReflinePosParams(int nID)
{
	switch (nID)
	{
	case 0: return &m_reflinePosParamsAnchor; 
	case 1: return &m_reflinePosParamsLL; 
	case 2: return &m_reflinePosParamsUR; 
	}
	return 0;
};

void CReflineMark::GetBBoxAll(CPrintSheet* pPrintSheet, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop)
{
	fBBoxLeft = fBBoxBottom = FLT_MAX; fBBoxRight = fBBoxTop = -FLT_MAX;

	if (m_nStatus == Inactive)
		return;
	if ( ! pPrintSheet)
		return;
	if ( (m_reflinePosParamsAnchor.m_nPlaceOn == CReflinePositionParams::PlaceOnFront) && (nSide == BACKSIDE) )
		return;
	if ( (m_reflinePosParamsAnchor.m_nPlaceOn == CReflinePositionParams::PlaceOnBack)  && (nSide == FRONTSIDE) )
		return;

	if (m_reflinePosParamsAnchor.m_nAnchor == REFLINE_ANCHOR)
		GetSheetTrimBox((void*)pPrintSheet, NULL, nSide, 0, fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop);
	else
	if (m_reflinePosParamsAnchor.m_nAnchor == PAGE_ANCHOR)
	{
		CLayout* pLayout = pPrintSheet->GetLayout();
		if ( ! pLayout)
			return;
		float fLeft, fBottom, fRight, fTop;
		CLayoutObjectList* pObjectList = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide.m_ObjectList : &pLayout->m_BackSide.m_ObjectList;
		POSITION pos = pObjectList->GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = pObjectList->GetNext(pos);
			if (rObject.m_nType == CLayoutObject::ControlMark)
				continue;
			GetSheetTrimBox((void*)&rObject, NULL, nSide, 0, fLeft, fBottom, fRight, fTop);
			if ( (fLeft == FLT_MAX) && (fBottom == FLT_MAX) && (fRight == -FLT_MAX) && (fTop == -FLT_MAX) )	// no refpos found for mark -> mark was declared as not existing by setting to +/- FLT_MAX
				continue;
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}
	}
	else
	if (m_reflinePosParamsAnchor.m_nAnchor == FOLDSHEET_ANCHOR)
	{
		for (short nComponentRefIndex = 0; nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
		{
			float fLeft, fBottom, fRight, fTop;
			GetSheetTrimBox((void*)pPrintSheet, (void*)nComponentRefIndex, nSide, 0, fLeft, fBottom, fRight, fTop);

			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}
	}
}

void* CReflineMark::GetSheetTrimBox(void* pParam1, void* pParam2, LPARAM nSide, LPARAM, float& fLeft, float& fBottom, float& fRight, float& fTop)
{
	if (m_reflinePosParamsAnchor.m_nAnchor == REFLINE_ANCHOR)
	{
		CPrintSheet* pPrintSheet = (CPrintSheet*)pParam1;
		if ( ! pPrintSheet)
		{
			fLeft = FLT_MAX; fBottom = FLT_MAX; fRight = -FLT_MAX; fTop = -FLT_MAX;
			return NULL;
		}

		float fLeftLL, fBottomLL, fLeftUR, fBottomUR;
		m_reflinePosParamsAnchor.GetSheetPosition (pPrintSheet, nSide, fLeft,  fBottom);
		m_reflinePosParamsLL.GetSheetPosition(pPrintSheet, nSide, fLeftLL, fBottomLL);
		m_reflinePosParamsUR.GetSheetPosition(pPrintSheet, nSide, fLeftUR, fBottomUR);

		if ( (fLeft == FLT_MAX) || (fBottom == FLT_MAX) || (fLeftLL == FLT_MAX) || (fBottomLL == FLT_MAX) || (fLeftUR == FLT_MAX) || (fBottomUR == FLT_MAX) )	// no refpos found for mark -> declare mark as not existing by setting to +/- FLT_MAX
		{
			fLeft = FLT_MAX; fBottom = FLT_MAX; fRight = -FLT_MAX; fTop = -FLT_MAX;
		}
		else
		{
			if ( ! m_bVariableWidth)
				fRight = fLeft + m_fMarkWidth;
			else
			{
				fLeft		 = fLeftLL;
				fRight		 = fLeftUR + m_fMarkWidth;
				m_fMarkWidth = fRight - fLeft;
			}

			if ( ! m_bVariableHeight)
				fTop = fBottom + m_fMarkHeight;
			else
			{
				fBottom		  = fBottomLL;
				fTop		  = fBottomUR + m_fMarkHeight;
				m_fMarkHeight = fTop - fBottom;
			}
			
			if ( (m_fMarkWidth < -0.001f) || (m_fMarkHeight < 0.001f) )					// width or height less than zero could happen when variable width/height overlaps in negativ direction
			{
				fLeft = FLT_MAX; fBottom = FLT_MAX; fRight = -FLT_MAX; fTop = -FLT_MAX;	// -> declare mark as not existing by setting to +/- FLT_MAX
			}
		}

		return NULL;
	}
	else
	{
		float 					fWidth			 = m_fMarkWidth;
		float 					fHeight			 = m_fMarkHeight;
		CReflinePositionParams	reflinePosParams = m_reflinePosParamsAnchor;
		if (m_reflinePosParamsAnchor.m_nAnchor == PAGE_ANCHOR)
		{
			CLayoutObject* pLayoutObject = (CLayoutObject*)pParam1;
			reflinePosParams = m_reflinePosParamsAnchor.Rotate(pLayoutObject->m_nHeadPosition);
			if ( (pLayoutObject->m_nHeadPosition == LEFT) || (pLayoutObject->m_nHeadPosition == RIGHT) )
			{
				float fTemp = fWidth; fWidth = fHeight; fHeight = fTemp;
			}
			fLeft   = pLayoutObject->GetObjectRefline(reflinePosParams.m_nReferenceLineInX);
			fBottom = pLayoutObject->GetObjectRefline(reflinePosParams.m_nReferenceLineInY);
		}
		else
		if (m_reflinePosParamsAnchor.m_nAnchor == FOLDSHEET_ANCHOR)
		{
			fLeft = FLT_MAX; fBottom = FLT_MAX; fRight = -FLT_MAX; fTop = -FLT_MAX;
			CPrintSheet* pPrintSheet		= (CPrintSheet*)pParam1;
			CLayout*	 pLayout			= (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			int			 nComponentRefIndex = (int)pParam2;
			if ( ! pPrintSheet || ! pLayout)
				return NULL;
			if ( (nComponentRefIndex < 0) || (nComponentRefIndex >= pLayout->m_FoldSheetLayerRotation.GetSize()) )
				return NULL;

			BOOL bFlip	   = ( ((nSide == FRONTSIDE) && pLayout->m_FoldSheetLayerTurned[nComponentRefIndex]) || ((nSide == BACKSIDE) && ! pLayout->m_FoldSheetLayerTurned[nComponentRefIndex]) ) ? TRUE : FALSE;
			int  nRotation = pLayout->m_FoldSheetLayerRotation[nComponentRefIndex];

			if (bFlip && (nRotation == 90))
				nRotation = 270;
			else
				if (bFlip && (nRotation == 270))
					nRotation = 90;

			if ( (nSide == BACKSIDE) && (pLayout->KindOfProduction() == WORK_AND_TUMBLE) )
			{
				if (nRotation == 0)
					nRotation = 180;
				else
					if (nRotation == 90)
						nRotation = 270;
					else
						if (nRotation == 180)
							nRotation = 0;
						else
							if (nRotation == 270)
								nRotation = 90;
			}

			switch (nRotation)
			{
			case 0:																		break;
			case 90:		reflinePosParams = m_reflinePosParamsAnchor.Rotate(LEFT);	break;
			case 180:		reflinePosParams = m_reflinePosParamsAnchor.Rotate(BOTTOM);	break;
			case 270:		reflinePosParams = m_reflinePosParamsAnchor.Rotate(RIGHT);	break;
			}
			if ( (nRotation == 90) || (nRotation == 270) )
			{
				float fTemp = fWidth; fWidth = fHeight; fHeight = fTemp;
			}

			pPrintSheet->GetFoldSheetLayerReflines(nComponentRefIndex, nSide, reflinePosParams.m_nReferenceLineInX, reflinePosParams.m_nReferenceLineInY, fLeft, fBottom);
		}

		float fMarkOffX	= fWidth  /2.0f; 
		float fMarkOffY	= fHeight /2.0f;
		switch (reflinePosParams.m_nObjectReferenceLineInX)
		{
		case LEFT:		fMarkOffX = 0.0f;			break;
		case RIGHT:		fMarkOffX = fWidth;			break;
		case XCENTER:	fMarkOffX = fWidth  / 2.0f;	break;
		}
		switch (reflinePosParams.m_nObjectReferenceLineInY)
		{
		case BOTTOM:	fMarkOffY = 0.0f;			break;
		case TOP:		fMarkOffY = fHeight;		break;
		case YCENTER:	fMarkOffY = fHeight / 2.0f;	break;
		}
		fLeft   += reflinePosParams.m_fDistanceX - fMarkOffX;
		fBottom += reflinePosParams.m_fDistanceY - fMarkOffY;
		fRight	= fLeft	  + fWidth;
		fTop	= fBottom + fHeight;
	}

	return NULL;
}

int CReflineMark::AddToSheet(CPrintSheet* pPrintSheet, int nSide, CPageTemplate* pMarkTemplate, CMarkList* /*pMarkList*/)
{
	if ( ! pPrintSheet)
		return 0;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return 0;
	if ( (m_reflinePosParamsAnchor.m_nPlaceOn == CReflinePositionParams::PlaceOnFront) && (nSide == BACKSIDE) )
		return 0;
	if ( (m_reflinePosParamsAnchor.m_nPlaceOn == CReflinePositionParams::PlaceOnBack)  && (nSide == FRONTSIDE) )
		return 0;

	float fLeft, fBottom, fRight, fTop;

	CLayoutSide& rLayoutSide = (nSide == FRONTSIDE) ? pPrintSheet->GetLayout()->m_FrontSide : pPrintSheet->GetLayout()->m_BackSide;

	if (m_reflinePosParamsAnchor.m_nAnchor == REFLINE_ANCHOR)
	{
		BOOL bDoPlace = TRUE;
		GetSheetTrimBox((void*)pPrintSheet, NULL, nSide, 0, fLeft, fBottom, fRight, fTop);
		if ( (fLeft == FLT_MAX) && (fBottom == FLT_MAX) && (fRight == -FLT_MAX) && (fTop == -FLT_MAX) )	// no refpos found for mark -> mark was declared as not existing by setting to +/- FLT_MAX
			bDoPlace = FALSE;
		else
			if (rLayoutSide.FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))		// identical mark (same position, same template) already
				bDoPlace = FALSE;																// present at layout side -> don't add again
		if ( ! bDoPlace)
		{
			if (pLayout->m_markList.m_strAnalyzeReport.IsEmpty())
				pLayout->m_markList.m_strAnalyzeReport.LoadString(IDS_MARKSNOTASSIGNED);
			CString strMsg; strMsg.Format(_T("%s: '%s'"), rLayoutSide.m_strSideName, m_strMarkName);
			pLayout->m_markList.m_strAnalyzeReport += strMsg + _T("\n");
			return 0;
		}

		// if width/height is variable, values could have been changed in GetSheetTrimBox() -> so update them
		m_fMarkWidth  = fRight - fLeft;
		m_fMarkHeight = fTop   - fBottom;
		if ( ! IsKindOf(RUNTIME_CLASS(CCustomMark)))
		{
			if (m_MarkSource.m_PageSourceHeaders.GetSize() > 0)
			{
				m_MarkSource.m_PageSourceHeaders[0].m_fBBoxRight = m_fMarkWidth;
				m_MarkSource.m_PageSourceHeaders[0].m_fBBoxTop   = m_fMarkHeight;
			}
		}
		m_reflinePosParamsAnchor.m_nAnchorReference = -1;

		CLayoutObject layoutObject;
		layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, m_nHeadPosition, 
								CImpManDoc::GetDoc()->m_MarkTemplateList.GetIndex(pMarkTemplate), &m_reflinePosParamsAnchor);
		rLayoutSide.m_ObjectList.AddTail(layoutObject);
	}
	else
	if (m_reflinePosParamsAnchor.m_nAnchor == PAGE_ANCHOR)
	{
		int nNumAdded = 0;
		CLayoutObjectList* pObjectList = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide.m_ObjectList : &pLayout->m_BackSide.m_ObjectList;
		POSITION pos = pObjectList->GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = pObjectList->GetNext(pos);
			if (rObject.m_nType == CLayoutObject::ControlMark)
				continue;

			BOOL bDoPlace = TRUE;
			GetSheetTrimBox((void*)&rObject, NULL, nSide, 0, fLeft, fBottom, fRight, fTop);
			if ( (fLeft == FLT_MAX) && (fBottom == FLT_MAX) && (fRight == -FLT_MAX) && (fTop == -FLT_MAX) )	// no refpos found for mark -> mark was declared as not existing by setting to +/- FLT_MAX
				bDoPlace = FALSE;
			else
				if (rLayoutSide.FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))		// identical mark (same position, same template) already
					bDoPlace = FALSE;																// present at layout side -> don't add again

			if ( ! InstanceCheckRules(pPrintSheet, nSide, &rObject, -1))
				continue;

			if ( ! bDoPlace)
			{
				if (pLayout->m_markList.m_strAnalyzeReport.IsEmpty())
					pLayout->m_markList.m_strAnalyzeReport.LoadString(IDS_MARKSNOTASSIGNED);
				CString strMsg; strMsg.Format(_T("%s: '%s'"), rLayoutSide.m_strSideName, m_strMarkName);
				pLayout->m_markList.m_strAnalyzeReport += strMsg + _T("\n");
				continue;
			}

			m_reflinePosParamsAnchor.m_nAnchorReference = rObject.m_nIndex;

			CLayoutObject layoutObject;
			layoutObject.m_nHeadPosition = m_nHeadPosition;
			switch (rObject.m_nHeadPosition)
			{
			case TOP:																								break;
			case LEFT:		layoutObject.m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 90);	break;
			case BOTTOM:	layoutObject.m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 180);	break;
			case RIGHT:		layoutObject.m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 270);	break;
			}

			layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, layoutObject.m_nHeadPosition, CImpManDoc::GetDoc()->m_MarkTemplateList.GetIndex(pMarkTemplate), &m_reflinePosParamsAnchor);
			rLayoutSide.m_ObjectList.AddTail(layoutObject);
			nNumAdded++;
		}
		return nNumAdded;
	}
	else
	if (m_reflinePosParamsAnchor.m_nAnchor == FOLDSHEET_ANCHOR)
	{
		int nNumAdded = 0;
		for (short nComponentRefIndex = 0; nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
		{
			BOOL bDoPlace = TRUE;
			GetSheetTrimBox((void*)pPrintSheet, (void*)nComponentRefIndex, nSide, 0, fLeft, fBottom, fRight, fTop);
			if ( (fLeft == FLT_MAX) && (fBottom == FLT_MAX) && (fRight == -FLT_MAX) && (fTop == -FLT_MAX) )	// no refpos found for mark -> mark was declared as not existing by setting to +/- FLT_MAX
				bDoPlace = FALSE;
			else
				if (rLayoutSide.FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))		// identical mark (same position, same template) already
					bDoPlace = FALSE;																// present at layout side -> don't add again

			if ( ! InstanceCheckRules(pPrintSheet, nSide, NULL, nComponentRefIndex))
				continue;

			if ( ! bDoPlace)
			{
				if (pLayout->m_markList.m_strAnalyzeReport.IsEmpty())
					pLayout->m_markList.m_strAnalyzeReport.LoadString(IDS_MARKSNOTASSIGNED);
				CString strMsg; strMsg.Format(_T("%s: '%s'"), rLayoutSide.m_strSideName, m_strMarkName);
				pLayout->m_markList.m_strAnalyzeReport += strMsg + _T("\n");
				continue;
			}

			m_reflinePosParamsAnchor.m_nAnchorReference = nComponentRefIndex;
			CLayoutObject layoutObject;
			layoutObject.m_nHeadPosition = m_nHeadPosition;

			BOOL bFlip	   = ( ((nSide == FRONTSIDE) && pLayout->m_FoldSheetLayerTurned[nComponentRefIndex]) || ((nSide == BACKSIDE) && ! pLayout->m_FoldSheetLayerTurned[nComponentRefIndex]) ) ? TRUE : FALSE;
			int  nRotation = pLayout->m_FoldSheetLayerRotation[nComponentRefIndex];
			if (bFlip && (nRotation == 90))
				nRotation = 270;
			else
				if (bFlip && (nRotation == 270))
					nRotation = 90;

			if ( (nSide == BACKSIDE) && (pLayout->KindOfProduction() == WORK_AND_TUMBLE) )
			{
				if (nRotation == 0)
					nRotation = 180;
				else
					if (nRotation == 90)
						nRotation = 270;
					else
						if (nRotation == 180)
							nRotation = 0;
						else
							if (nRotation == 270)
								nRotation = 90;
			}

			switch (nRotation)
			{
			case 0:																								break;
			case 90:	layoutObject.m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 90);	break;
			case 180:	layoutObject.m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 180);	break;
			case 270:	layoutObject.m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 270);	break;
			}
			layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, layoutObject.m_nHeadPosition, CImpManDoc::GetDoc()->m_MarkTemplateList.GetIndex(pMarkTemplate), &m_reflinePosParamsAnchor);
			rLayoutSide.m_ObjectList.AddTail(layoutObject);
			nNumAdded++;
		}
		return nNumAdded;
	}

	return 1;
}

void CReflineMark::DrawSheetPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, CMarkList* pMarkList, BOOL bShowContent, int nEdge, BOOL bShowHighlighted)
{
	if (m_nStatus == Inactive)
		return;
	if ( ! pPrintSheet)
		return;
	if ( (m_reflinePosParamsAnchor.m_nPlaceOn == CReflinePositionParams::PlaceOnFront) && (nSide == BACKSIDE) )
		return;
	if ( (m_reflinePosParamsAnchor.m_nPlaceOn == CReflinePositionParams::PlaceOnBack)  && (nSide == FRONTSIDE) )
		return;
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	float fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;
	if (m_reflinePosParamsAnchor.m_nAnchor == REFLINE_ANCHOR)
	{
		if ( ! m_rules.GetRuleExpression(0).IsEmpty())
			if ( ! IsEnabled(pPrintSheet))
				return;
		GetSheetTrimBox((void*)pPrintSheet, NULL, nSide, 0, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);
		m_reflinePosParamsAnchor.m_nAnchorReference = -1;
		DrawMarkPreview(pDC, pPrintSheet, nSide, -1, pMarkList, nEdge, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);
	}
	else
	if (m_reflinePosParamsAnchor.m_nAnchor == PAGE_ANCHOR)
	{
		CLayout* pLayout = pPrintSheet->GetLayout();
		if ( ! pLayout)
			return;
		CLayoutObjectList* pObjectList = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide.m_ObjectList : &pLayout->m_BackSide.m_ObjectList;
		POSITION pos = pObjectList->GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = pObjectList->GetNext(pos);
			if (rObject.m_nType == CLayoutObject::ControlMark)
				continue;
			if ( ! InstanceCheckRules(pPrintSheet, nSide, &rObject, -1))
				continue;
			GetSheetTrimBox((void*)&rObject, NULL, nSide, 0, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);
			m_reflinePosParamsAnchor.m_nAnchorReference = rObject.m_nIndex;

			int nTempHead = m_nHeadPosition;
			switch (rObject.m_nHeadPosition)
			{
			case TOP:																					break;
			case LEFT:		m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 90);	break;
			case BOTTOM:	m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 180);	break;
			case RIGHT:		m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 270);	break;
			}
			DrawMarkPreview(pDC, pPrintSheet, nSide, rObject.m_nIndex, pMarkList, nEdge, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);
			m_nHeadPosition = nTempHead;
		}
	}
	else
	if (m_reflinePosParamsAnchor.m_nAnchor == FOLDSHEET_ANCHOR)
	{
		CLayout* pLayout = pPrintSheet->GetLayout();
		if ( ! pLayout)
			return;
		for (short nComponentRefIndex = 0; nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
		{
			GetSheetTrimBox((void*)pPrintSheet, (void*)nComponentRefIndex, nSide, 0, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);
			if ( ! InstanceCheckRules(pPrintSheet, nSide, NULL, nComponentRefIndex))
				continue;
			m_reflinePosParamsAnchor.m_nAnchorReference = nComponentRefIndex;

			int nTempHead = m_nHeadPosition;
			int nRotation = pLayout->m_FoldSheetLayerRotation[nComponentRefIndex];
			switch (nRotation)
			{
			case 0:																					break;
			case 90:	m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 90);	break;
			case 180:	m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 180);	break;
			case 270:	m_nHeadPosition = CLayoutObject::RotateHeadPosition(m_nHeadPosition, 270);	break;
			}
			DrawMarkPreview(pDC, pPrintSheet, nSide, nComponentRefIndex, pMarkList, nEdge, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);
			m_nHeadPosition = nTempHead;
		}
	}
}

void CReflineMark::DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, class CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted)
{
}



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// CCustomMark

IMPLEMENT_SERIAL(CCustomMark, CObject, VERSIONABLE_SCHEMA | 2)

CCustomMark::CCustomMark()
{
	m_nType			=  CustomMark;
	m_strMarkName	= "CustomMark";
	m_bAllColors	= FALSE;	// PDF marks shouldn't have colorspace all 
}

CCustomMark::CCustomMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps, 
						 CPageSource& rMarkSource)
						: CReflineMark(strName, fWidth, fHeight, nHeadPosition, bAllColors, rColorSeps, rMarkSource)
{
	m_nType	= CustomMark;
}

void CCustomMark::Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList)
{
	CString strNewMark;
	strNewMark.LoadString(IDS_NEW_CUSTOMMARK_PDF);
	if (pMarkList)
		strNewMark = pMarkList->FindFreeMarkName(strNewMark);

	m_strMarkName = strNewMark;
	m_fMarkWidth  = m_fMarkHeight = 20.0f;
	InitializeTemplate(rColorDefTable);
}

// operator overload for CLayoutObject - needed by CList functions:
const CCustomMark& CCustomMark::operator=(const CCustomMark& rCustomMark)
{
	Copy(rCustomMark);	

	return *this;
}

BOOL CCustomMark::operator==(const CCustomMark& rCustomMark)
{
	if (CReflineMark::operator!=(rCustomMark))	return FALSE;

	return TRUE;
}

BOOL CCustomMark::operator!=(const CCustomMark& rCustomMark)
{
	return !(*this == rCustomMark);
}

void CCustomMark::Copy(const CCustomMark& rCustomMark)
{
	CReflineMark::Copy(rCustomMark);
}

void CCustomMark::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CCustomMark));
	UINT nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		CReflineMark::Serialize(ar);
	}											
	else																		 
	{
		switch (nVersion)
		{
		case 1:	CMark::Serialize(ar);
				m_reflinePosParamsAnchor.Serialize(ar);	
				m_bVariableWidth  = FALSE;
				m_bVariableHeight = FALSE;
				break;
		case 2:	CReflineMark::Serialize(ar);
				break;
		}

		//// 04/28/2010 - check plausibility
		//if (m_MarkSource.m_nColorant == CPageSource::Composite)	// 09/11/2010 - removed this, because now pdf marks with colorspace all are supported
		//{
		//	if (m_markTemplate.m_bMap2AllColors)
		//		m_markTemplate.m_bMap2AllColors = FALSE;
		//	if (m_MarkSource.m_PageSourceHeaders.GetSize())
		//		if (m_MarkSource.m_PageSourceHeaders[0].m_spotColors.GetSize() <= 0)
		//			m_markTemplate.m_bMap2SpotColors = FALSE;
		//}
	}
}

void CCustomMark::DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted)
{
	if ( (fabs(fTrimBoxLeft) == FLT_MAX) || (fabs(fTrimBoxBottom) == FLT_MAX) || (fabs(fTrimBoxRight) == FLT_MAX) || (fabs(fTrimBoxTop) == FLT_MAX) )
		return;	// mark cannot be placed

	CRect markRect;
	markRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
	markRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
	markRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
	markRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);

	CRect clipRect; 
	m_markTemplate.GetGridClipBox(fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);
	clipRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
	clipRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
	clipRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
	clipRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);

	BOOL			 bUnassigned = TRUE;
   	CPrintSheetView* pView		 = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   	if (pView)
	{
		int nSheetID = (nSide << 16) + pPrintSheet->m_nPrintSheetNumber;
		pView->m_DisplayList.RegisterItem(pDC, markRect, markRect, (void*)this, 
										  SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Mark), (void*)nSheetID, CDisplayItem::ForceRegister); 

		CDlgLayoutObjectProperties* pDlg = pView->GetDlgLayoutObjectProperties();
		if (pDlg)
			if (pDlg->m_bModeModify)
				bUnassigned = FALSE;
	}

	CColorDefinitionTable& rColorDefTable = GetActColorDefTable(pMarkList);

	switch (nEdge)
	{
	case 0:		 m_reflinePosParamsAnchor.DrawSheetReflines(pDC, pPrintSheet, nSide);			break;
	case LEFT: 	 
	case BOTTOM: m_reflinePosParamsLL.DrawSheetReflines	   (pDC, pPrintSheet, nSide, nEdge);	break;
	case RIGHT:  
	case TOP:	 m_reflinePosParamsUR.DrawSheetReflines	   (pDC, pPrintSheet, nSide, nEdge);	break;
	default:	 break;
	}

	CPen	framePen(PS_SOLID, 1, (bShowHighlighted) ? MARKFRAMECOLOR_HIGHLIGHT : MARKCOLOR);	
	CPen*	pOldPen	  = pDC->SelectObject(&framePen);
	CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

	CSize sizeOffset(1,1); pDC->DPtoLP(&sizeOffset);
	markRect.OffsetRect(sizeOffset.cx, 0);
	
	if (bShowHighlighted)
		HighlightMark(pDC, markRect, pPrintSheet);

	CString strPreviewFile;
	if (bShowContent)
	{
		float fOutBBoxWidth, fOutBBoxHeight;
		if ((m_markTemplate.m_fContentRotation == 0.0f) || (m_markTemplate.m_fContentRotation == 180.0f))
		{
			fOutBBoxWidth  = (m_MarkSource.GetBBox(RIGHT, NULL) - m_MarkSource.GetBBox(LEFT,   NULL)) * m_markTemplate.m_fContentScaleX;
			fOutBBoxHeight = (m_MarkSource.GetBBox(TOP,   NULL) - m_MarkSource.GetBBox(BOTTOM, NULL)) * m_markTemplate.m_fContentScaleY;
		}
		else
		{
			fOutBBoxWidth  = (m_MarkSource.GetBBox(TOP,   NULL) - m_MarkSource.GetBBox(BOTTOM, NULL)) * m_markTemplate.m_fContentScaleX;
			fOutBBoxHeight = (m_MarkSource.GetBBox(RIGHT, NULL) - m_MarkSource.GetBBox(LEFT,   NULL)) * m_markTemplate.m_fContentScaleY;
		}

		int nOutBBoxWidth  = CPrintSheetView::WorldToLP(fOutBBoxWidth);
		int nOutBBoxHeight = CPrintSheetView::WorldToLP(fOutBBoxHeight);

		if ((m_nHeadPosition == LEFT) || (m_nHeadPosition == RIGHT))
		{
			int nTemp = nOutBBoxWidth; nOutBBoxWidth = nOutBBoxHeight; nOutBBoxHeight = nTemp;
		}
		CRect outBBox;
		outBBox.left   = markRect.CenterPoint().x - nOutBBoxWidth  / 2;
		outBBox.bottom = markRect.CenterPoint().y - nOutBBoxHeight / 2;
		outBBox.right  = markRect.CenterPoint().x + nOutBBoxWidth  / 2;
		outBBox.top    = markRect.CenterPoint().y + nOutBBoxHeight / 2;

		switch (m_nHeadPosition)
		{
		case TOP:		outBBox	+= CSize(CPrintSheetView::WorldToLP( m_markTemplate.GetContentOffsetX(FALSE, &m_MarkSource, &m_fMarkWidth)), 
									     CPrintSheetView::WorldToLP( m_markTemplate.GetContentOffsetY(FALSE, &m_MarkSource, &m_fMarkHeight))); 
						break;
		case RIGHT:		outBBox	+= CSize(CPrintSheetView::WorldToLP( m_markTemplate.GetContentOffsetY(FALSE, &m_MarkSource, &m_fMarkHeight)), 
									     CPrintSheetView::WorldToLP(-m_markTemplate.GetContentOffsetX(FALSE, &m_MarkSource, &m_fMarkWidth))); 
						break;
		case BOTTOM:	outBBox	+= CSize(CPrintSheetView::WorldToLP(-m_markTemplate.GetContentOffsetX(FALSE, &m_MarkSource, &m_fMarkWidth)), 
									     CPrintSheetView::WorldToLP(-m_markTemplate.GetContentOffsetY(FALSE, &m_MarkSource, &m_fMarkHeight))); 
						break;
		case LEFT:		outBBox	+= CSize(CPrintSheetView::WorldToLP(-m_markTemplate.GetContentOffsetY(FALSE, &m_MarkSource, &m_fMarkHeight)), 
									     CPrintSheetView::WorldToLP( m_markTemplate.GetContentOffsetX(FALSE, &m_MarkSource, &m_fMarkWidth))); 
						break;
		}

		SEPARATIONINFO sepInfo = GetSeparationInfo(pPrintSheet->GetTopmostPlate(nSide), rColorDefTable, TRUE);
		if (sepInfo.m_nColorDefinitionIndex != -1)	// if (custom mark is defined for topmost plate color)
		{
			strPreviewFile = m_MarkSource.GetPreviewFileName(sepInfo.m_nPageNumInSourceFile);

			DrawPreview(pDC, outBBox, clipRect, strPreviewFile);
		}
	}

	if (bShowContent)
		CPrintSheetView::DrawRectangleExt(pDC, markRect);
	else
	{
		if ( ! bShowHighlighted)
		{
			//MINSIZE_MARK(markRect);
			CPrintSheetView::FillTransparent (pDC, markRect, (bShowHighlighted) ? MARKCOLOR_HIGHLIGHT : MARKCOLOR, TRUE, 150);
		}
		CPrintSheetView::DrawRectangleExt(pDC, markRect);
	}

	DrawContentGrid(pDC, markRect);

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	framePen.DeleteObject();
}

void CCustomMark::DrawPreview(CDC* pDC, CRect outBBox, CRect clipRect, const CString& strPreviewFile)
{
	BOOL bDupMark = (m_markTemplate.m_bContentDuplicate && ((m_markTemplate.m_fContentGridX > 0.1f) || (m_markTemplate.m_fContentGridY > 0.1f)) ) ? TRUE : FALSE;
	if (bDupMark)
	{
		int nXGrid = max(CPrintSheetView::WorldToLP(m_markTemplate.m_fContentGridX), 1);
		int nYGrid = max(CPrintSheetView::WorldToLP(m_markTemplate.m_fContentGridY), 1);
		int nBBoxWidth  = outBBox.Width();
		int nBBoxHeight = abs(outBBox.Height());
		int nXPos = 0;
		switch (m_markTemplate.m_nContentPivotX)
		{
		case LEFT:		nXPos = clipRect.left;															break;
		case RIGHT:		nXPos = clipRect.right - (clipRect.Width() / nXGrid) * nXGrid;					break;
		case XCENTER:	if (m_markTemplate.m_bContentDuplicate)
							nXPos = clipRect.CenterPoint().x - ((clipRect.Width()/2) / nXGrid) * nXGrid - nXGrid/2;	
						else
							nXPos = clipRect.CenterPoint().x - ((clipRect.Width()/2) / nXGrid) * nXGrid;	
						break;
		}
		if (nXPos > clipRect.left)
			nXPos -= nXGrid;

		while (nXPos <= clipRect.right)
		{
			int nYPos = 0;
			switch (m_markTemplate.m_nContentPivotY)
			{
			case BOTTOM:	nYPos = clipRect.bottom;															break;
			case TOP:		nYPos = clipRect.top - (abs(clipRect.Height()) / nYGrid) * nYGrid;					break;
			case YCENTER:	if (m_markTemplate.m_bContentDuplicate)
								nYPos = clipRect.CenterPoint().y - ((abs(clipRect.Height())/2) / nYGrid) * nYGrid - nYGrid/2;	
							else
								nYPos = clipRect.CenterPoint().y - ((abs(clipRect.Height())/2) / nYGrid) * nYGrid;	
							break;
			}
			if (nYPos > clipRect.bottom)
				nYPos -= nYGrid;
			while (nYPos <= clipRect.top)
			{
				switch (m_markTemplate.m_nContentPivotX)
				{
				case LEFT:		outBBox.left =  nXPos;									break;
				case RIGHT:		outBBox.left =  nXPos + nXGrid		- nBBoxWidth;		break;
				case XCENTER:	outBBox.left = (nXPos + nXGrid / 2)	- nBBoxWidth / 2;	break;
				}
				switch (m_markTemplate.m_nContentPivotY)
				{
				case BOTTOM:	outBBox.top =  nYPos;									break;
				case TOP:		outBBox.top =  nYPos + nYGrid		- nBBoxHeight;		break;
				case YCENTER:	outBBox.top = (nYPos + nYGrid / 2)	- nBBoxHeight / 2;	break;
				}
				outBBox.right  = outBBox.left + nBBoxWidth;
				outBBox.bottom = outBBox.top  + nBBoxHeight;
				switch (m_nHeadPosition)
				{
				case TOP:		outBBox	+= CSize(CPrintSheetView::WorldToLP( m_markTemplate._GetContentOffsetX()), CPrintSheetView::WorldToLP( m_markTemplate._GetContentOffsetY())); 
								break;
				case RIGHT:		outBBox	+= CSize(CPrintSheetView::WorldToLP( m_markTemplate._GetContentOffsetY()), CPrintSheetView::WorldToLP(-m_markTemplate._GetContentOffsetX())); 
								break;
				case BOTTOM:	outBBox	+= CSize(CPrintSheetView::WorldToLP(-m_markTemplate._GetContentOffsetX()), CPrintSheetView::WorldToLP(-m_markTemplate._GetContentOffsetY())); 
								break;
				case LEFT:		outBBox	+= CSize(CPrintSheetView::WorldToLP(-m_markTemplate._GetContentOffsetY()), CPrintSheetView::WorldToLP( m_markTemplate._GetContentOffsetX())); 
								break;
				}
				CRect rcClipGrid(nXPos, nYPos, nXPos + nXGrid, nYPos + nYGrid);
				CRect rcClip = clipRect; rcClip.NormalizeRect();
				rcClipGrid.IntersectRect(rcClipGrid, rcClip);
				pDC->SaveDC();	// save current clipping region
				pDC->IntersectClipRect(rcClipGrid);
				if ( ! CPageListView::DrawPreview(pDC, outBBox, m_nHeadPosition, 0.0f, strPreviewFile))
				{
					pDC->RestoreDC(-1);
					break;
				}
				pDC->RestoreDC(-1);
				if (m_markTemplate.m_fContentGridY <= 0.1f)
					break;
				if (nYGrid == 0)
					break;
				nYPos += nYGrid;
			}

			if (m_markTemplate.m_fContentGridX <= 0.1f)
				break;
			if (nXGrid == 0)
				break;
			nXPos += nXGrid;
		}
	}
	else
	{
		pDC->SaveDC();	// save current clipping region
		pDC->IntersectClipRect(clipRect);
		CPageListView::DrawPreview(pDC, outBBox, m_nHeadPosition, 0.0f, strPreviewFile);
		pDC->RestoreDC(-1);
	}
}

void CCustomMark::DrawContentGrid(CDC* pDC, CRect markRect)
{
   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView->IsOpenDlgPDFObjectContent())
		return;

	CSize sizeBorder(200, 200);
	CPen pen(PS_DOT, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);

	int nXGrid = CPrintSheetView::WorldToLP(m_markTemplate.m_fContentGridX);
	int nYGrid = CPrintSheetView::WorldToLP(m_markTemplate.m_fContentGridY);
	CSize sizeGrid(nXGrid, nYGrid);
	pDC->LPtoDP(&sizeGrid);

	if ( (sizeGrid.cx > 3) && (m_markTemplate.m_fContentGridX > 0.1f))
	{
		int nXPos = 0;
		switch (m_markTemplate.m_nContentPivotX)
		{
		case LEFT:		nXPos = markRect.left;															break;
		case RIGHT:		nXPos = markRect.right - (markRect.Width() / nXGrid) * nXGrid;					break;
		case XCENTER:	if (m_markTemplate.m_bContentDuplicate)
							nXPos = markRect.CenterPoint().x - ((markRect.Width()/2) / nXGrid) * nXGrid - nXGrid/2;	
						else
							nXPos = markRect.CenterPoint().x - ((markRect.Width()/2) / nXGrid) * nXGrid;	
						break;
		}
		while (nXPos <= markRect.right)
		{
			if (nXPos >= markRect.left)
			{
				pDC->MoveTo(nXPos, markRect.top	   + sizeBorder.cy);
				pDC->LineTo(nXPos, markRect.bottom - sizeBorder.cy);
			}
			nXPos += nXGrid;
		}
	}

	if ( (sizeGrid.cy > 3) && (m_markTemplate.m_fContentGridY > 0.1f))
	{
		int nYPos = 0;
		switch (m_markTemplate.m_nContentPivotY)
		{
		case BOTTOM:	nYPos = markRect.bottom;															break;
		case TOP:		nYPos = markRect.top - (abs(markRect.Height()) / nYGrid) * nYGrid;					break;
		case YCENTER:	if (m_markTemplate.m_bContentDuplicate)
							nYPos = markRect.CenterPoint().y - ((abs(markRect.Height())/2) / nYGrid) * nYGrid - nYGrid/2;	
						else
							nYPos = markRect.CenterPoint().y - ((abs(markRect.Height())/2) / nYGrid) * nYGrid;	
						break;
		}
		while (nYPos <= markRect.top)
		{
			if (nYPos >= markRect.bottom)
			{
				pDC->MoveTo(markRect.left  - sizeBorder.cx, nYPos);
				pDC->LineTo(markRect.right + sizeBorder.cx, nYPos);
			}
			nYPos += nYGrid;
		}
	}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();
}



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// CTextMark

IMPLEMENT_SERIAL(CTextMark, CObject, VERSIONABLE_SCHEMA | 2)

CTextMark::CTextMark()
{ 
	m_nType			=  TextMark;
	m_strMarkName	= "TextMark";
	m_strOutputText	= "TextMark";
}

CTextMark::CTextMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
					 CPageSource& rMarkSource, const CString& strText)
					: CReflineMark(strName, fWidth, fHeight, nHeadPosition, bAllColors, rColorSeps, rMarkSource)
{
	m_nType			= TextMark;
	m_strOutputText	= strText;
}

void CTextMark::Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList)
{
	CString strNewMark;
	strNewMark.LoadString(IDS_NEW_TEXTMARK);
	if (pMarkList)
		strNewMark = pMarkList->FindFreeMarkName(strNewMark);

	m_strMarkName		 = strNewMark;
	m_fMarkWidth		 = 100.0f; m_fMarkHeight = 5.0f;
	m_strOutputText		 = "";
	m_MarkSource.m_nType = CPageSource::SystemCreated;
	m_MarkSource.m_strSystemCreationInfo.Format(_T("$TEXT %s"), m_strOutputText);
	CPageSourceHeader markSourceHeader;
	markSourceHeader.m_bPageNumberFix = FALSE;
	markSourceHeader.m_nPageNumber	  = 1;
	markSourceHeader.m_strColorName	  = _T("Composite");
	markSourceHeader.m_rgb			  = WHITE;
	markSourceHeader.m_nColorIndex	  = 0;
	m_MarkSource.m_PageSourceHeaders.Add(markSourceHeader);
	InitializeTemplate(rColorDefTable);
}

// operator overload for CLayoutObject - needed by CList functions:
const CTextMark& CTextMark::operator=(const CTextMark& rTextMark)
{
	Copy(rTextMark);	

	return *this;
}

BOOL CTextMark::operator==(const CTextMark& rTextMark)
{
	if (CReflineMark::operator!=(rTextMark))				return FALSE;
	if (m_strOutputText		  != rTextMark.m_strOutputText)	return FALSE;

	return TRUE;
}

BOOL CTextMark::operator!=(const CTextMark& rTextMark)
{
	return !(*this == rTextMark);
}

void CTextMark::Copy(const CTextMark& rTextMark)
{
	CReflineMark::Copy(rTextMark);
	m_strOutputText	= rTextMark.m_strOutputText;
}

void CTextMark::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CTextMark));
	UINT nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		CReflineMark::Serialize(ar);
		ar << m_strOutputText;
	}											
	else																		 
	{
		switch (nVersion)
		{
		case 1:
			CMark::Serialize(ar);
			m_reflinePosParamsAnchor.Serialize(ar);	
			ar >> m_strOutputText;
			break;
		case 2:
			CReflineMark::Serialize(ar);
			ar >> m_strOutputText;
			break;
		}
	}
}
	
void CTextMark::DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted)
{
	if ( (fabs(fTrimBoxLeft) == FLT_MAX) || (fabs(fTrimBoxBottom) == FLT_MAX) || (fabs(fTrimBoxRight) == FLT_MAX) || (fabs(fTrimBoxTop) == FLT_MAX) )
		return;	// mark cannot be placed

	CRect markRect;
	markRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
	markRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
	markRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
	markRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   	if (pView)
	{
		int nSheetID = (nSide << 16) + pPrintSheet->m_nPrintSheetNumber;
		pView->m_DisplayList.RegisterItem(pDC, markRect, markRect, (void*)this, 
										  SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Mark), (void*)nSheetID, CDisplayItem::ForceRegister); 
	}

	CColorDefinitionTable& rColorDefTable = GetActColorDefTable(pMarkList);

	switch (nEdge)
	{
	case 0:		 m_reflinePosParamsAnchor.DrawSheetReflines(pDC, pPrintSheet, nSide);			break;
	case LEFT: 	 
	case BOTTOM: m_reflinePosParamsLL.DrawSheetReflines	   (pDC, pPrintSheet, nSide, nEdge);	break;
	case RIGHT:  
	case TOP:	 m_reflinePosParamsUR.DrawSheetReflines	   (pDC, pPrintSheet, nSide, nEdge);	break;
	default:	 break;
	}

	if (pDC->RectVisible(markRect))
	{
		CPen	framePen(PS_SOLID, 1, (bShowHighlighted) ? MARKFRAMECOLOR_HIGHLIGHT : MARKCOLOR);	// Lightred
		CPen*	pOldPen	  = pDC->SelectObject(&framePen);
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

		CSize sizeOffset(1,1); pDC->DPtoLP(&sizeOffset);
		markRect.OffsetRect(sizeOffset.cx, 0);
		
		if (bShowHighlighted)
			HighlightMark(pDC, markRect, pPrintSheet);

		if (bShowContent)
		{
			if (GetSeparationInfo(pPrintSheet->GetTopmostPlate(nSide), rColorDefTable, TRUE).m_nColorDefinitionIndex != -1)	// Textmark is defined for topmost plate color
			{
				CPlate*	pPlate = pPrintSheet->GetTopmostPlate(nSide);
				TCHAR szDest[1024];
				if ( (pPlate->m_strPlateName.CompareNoCase(_T("Composite")) == 0) && (m_strOutputText.Find(_T("$2")) != -1) )	// special handling for color name output in case of composite
				{
					BOOL					   bColorSpaceAll;
					float					   fC = 0.0f, fM = 0.0f, fY = 0.0f, fK = 0.0f;
					CStringArray			   spotColorNames;
					CArray<COLORREF, COLORREF> spotColorRGBs;
					CArray<float,	 float>	   spotColorTints;
					CMark markToCheck;
					markToCheck = *this;
					markToCheck.m_markTemplate = markToCheck.TransformColorDefs(rColorDefTable, -1);
					CPageTemplateList::CheckForExposuresToAdd(pPrintSheet, NULL, &markToCheck, markToCheck.m_markTemplate, -1, FALSE, CImpManDoc::GetDoc(), -1);
					CPDFOutput::BuildCompositeColorInfo(&markToCheck.m_markTemplate, &markToCheck.m_MarkSource, pPlate, bColorSpaceAll, fC, fM, fY, fK, spotColorNames, spotColorRGBs, spotColorTints);
					DrawTextfieldPreview(pDC, m_strOutputText, markRect, m_nHeadPosition, pPrintSheet, pPlate, fC, fM, fY, fK, spotColorNames, spotColorRGBs, &m_reflinePosParamsAnchor);
				}
				else
				{
					BOOL bScaleToFit = (m_MarkSource.m_strSystemCreationInfo.Find(_T("/Font (Barcode")) != -1) ? TRUE : FALSE;
					CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)m_strOutputText, pPrintSheet, pPlate, -1, _T(""), &m_reflinePosParamsAnchor);
					DrawTextfieldPreview(pDC, szDest, markRect, m_nHeadPosition, pPrintSheet, bScaleToFit, BLACK);
				}
			}

			CPrintSheetView::DrawRectangleExt(pDC, markRect);
		}
		else
		{
			if ( ! bShowHighlighted)
			{
				MINSIZE_MARK(markRect);
				CPrintSheetView::FillTransparent (pDC, markRect, (bShowHighlighted) ? MARKCOLOR_HIGHLIGHT : MARKCOLOR, TRUE, 150);
			}
			CPrintSheetView::DrawRectangleExt(pDC, markRect);
//			CPrintSheetView::DrawHeadPosition(pDC, markRect, m_nHeadPosition);
		}
		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		framePen.DeleteObject();
	}
}

void CTextMark::DrawTextfieldPreview(CDC* pDC, const CString& strText, const CRect& rOutBBox, int nHeadPosition, CPrintSheet* pPrintSheet, 
										  CPlate* pPlate, float fC, float fM, float fY, float fK, CStringArray& spotColorNames, CArray<COLORREF, COLORREF>& spotColorRGBs, CReflinePositionParams* pReflinePosParams)
{
	if ( ! pPrintSheet)
		return;

	BOOL  bStacked = (strText.Find(_T("$2_=")) != -1) ? TRUE : FALSE;
	CRect textRect = rOutBBox;

	TCHAR szDest[1024];
	CSize textExtent;
	int	  nXOffset = 0;
	int	  nYOffset = 0;
	if (fC == 1.0f)
	{
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, -1, theApp.m_colorDefTable.GetCyanName(), pReflinePosParams);
		textExtent = DrawTextfieldPreview(pDC, szDest, textRect, nHeadPosition, pPrintSheet, FALSE, CYAN);
		nXOffset = (nHeadPosition == TOP)  ? textExtent.cx : ( (nHeadPosition == BOTTOM) ? -textExtent.cx : 0 );
		nYOffset = (nHeadPosition == LEFT) ? textExtent.cx : ( (nHeadPosition == RIGHT)  ? -textExtent.cx : 0 );
		if ( ! bStacked)
			textRect.OffsetRect(CSize(nXOffset, nYOffset));
	}
	if (fM == 1.0f)
	{
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, -1, theApp.m_colorDefTable.GetMagentaName(), pReflinePosParams);
		textExtent = DrawTextfieldPreview(pDC, szDest, textRect, nHeadPosition, pPrintSheet, FALSE, MAGENTA);
		nXOffset = (nHeadPosition == TOP)  ? textExtent.cx : ( (nHeadPosition == BOTTOM) ? -textExtent.cx : 0 );
		nYOffset = (nHeadPosition == LEFT) ? textExtent.cx : ( (nHeadPosition == RIGHT)  ? -textExtent.cx : 0 );
		if ( ! bStacked)
			textRect.OffsetRect(CSize(nXOffset, nYOffset));
	}
	if (fY == 1.0f)
	{
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, -1, theApp.m_colorDefTable.GetYellowName(), pReflinePosParams);
		textExtent = DrawTextfieldPreview(pDC, szDest, textRect, nHeadPosition, pPrintSheet, FALSE, YELLOW);
		nXOffset = (nHeadPosition == TOP)  ? textExtent.cx : ( (nHeadPosition == BOTTOM) ? -textExtent.cx : 0 );
		nYOffset = (nHeadPosition == LEFT) ? textExtent.cx : ( (nHeadPosition == RIGHT)  ? -textExtent.cx : 0 );
		if ( ! bStacked)
			textRect.OffsetRect(CSize(nXOffset, nYOffset));
	}
	if (fK == 1.0f)
	{
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, -1, theApp.m_colorDefTable.GetKeyName(), pReflinePosParams);
		textExtent = DrawTextfieldPreview(pDC, szDest, textRect, nHeadPosition, pPrintSheet, FALSE, BLACK);
		nXOffset = (nHeadPosition == TOP)  ? textExtent.cx : ( (nHeadPosition == BOTTOM) ? -textExtent.cx : 0 );
		nYOffset = (nHeadPosition == LEFT) ? textExtent.cx : ( (nHeadPosition == RIGHT)  ? -textExtent.cx : 0 );
		if ( ! bStacked)
			textRect.OffsetRect(CSize(nXOffset, nYOffset));
	}

	for (int i = 0; i < spotColorNames.GetSize(); i++)
	{
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, -1, spotColorNames[i], pReflinePosParams);
		DrawTextfieldPreview(pDC, szDest, textRect, nHeadPosition, pPrintSheet, FALSE, spotColorRGBs[i]);
	}
}

CSize CTextMark::DrawTextfieldPreview(CDC* pDC, const CString& strText, const CRect& rOutBBox, int nHeadPosition, CPrintSheet* pPrintSheet, BOOL bScaleToFit, COLORREF crTextColor) 
{
	//if ( ! pPrintSheet)
	//	return CSize(0, 0);

	COLORREF crOldColor = pDC->SetTextColor(BLACK);

	int nOrientation, nTextHeight, nTextLength;
	switch (nHeadPosition)
	{
	case TOP:	 nOrientation = 0;    nTextHeight = rOutBBox.Height(); nTextLength = rOutBBox.Width();  break; 
	case RIGHT:	 nOrientation = 900;  nTextHeight = rOutBBox.Width();  nTextLength = rOutBBox.Height(); break;
	case BOTTOM: nOrientation = 1800; nTextHeight = rOutBBox.Height(); nTextLength = rOutBBox.Width();  break; 
	case LEFT:	 nOrientation = 2700; nTextHeight = rOutBBox.Width();  nTextLength = rOutBBox.Height(); break;
	default:	 nOrientation = 0;    nTextHeight = rOutBBox.Height(); nTextLength = rOutBBox.Width();  break; 
	}
	nTextLength = abs(nTextLength);	// rect is not normalized
	nTextHeight = abs(nTextHeight);	// rect is not normalized

	CFont font;
	if (bScaleToFit)
	{
		CSize sizeFont(CPrintSheetView::WorldToLP(10), CPrintSheetView::WorldToLP(10));
		font.CreateFont(sizeFont.cx, 0, nOrientation, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FIXED_PITCH, _T("Helvetica"));	
		CFont* pOldFont = pDC->SelectObject(&font);
		CSize textExtent = pDC->GetTextExtent(strText);
		nTextHeight = (int)((float)sizeFont.cy * ((float)nTextLength / (float)textExtent.cx));
		pDC->SelectObject(pOldFont);
		font.DeleteObject();
		font.CreateFont(nTextHeight, 0, nOrientation, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FIXED_PITCH, _T("Helvetica"));	
	}
	else
		font.CreateFont(nTextHeight, 0, nOrientation, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FIXED_PITCH, _T("Helvetica"));	

	CFont* pOldFont = pDC->SelectObject(&font);

	int nXPos, nYPos;
	switch (nHeadPosition)
	{
		case LEFT  : nXPos = rOutBBox.left;  nYPos = rOutBBox.bottom;	break;
		case TOP   : nXPos = rOutBBox.left;  nYPos = rOutBBox.top;		break;
		case RIGHT : nXPos = rOutBBox.right; nYPos = rOutBBox.top;		break;
		case BOTTOM: nXPos = rOutBBox.right; nYPos = rOutBBox.bottom;	break;
		default	   : nXPos = rOutBBox.left;  nYPos = rOutBBox.bottom;	break;
	}

	pDC->SaveDC();	// save current clipping region
	pDC->IntersectClipRect(rOutBBox);
	pDC->SetTextColor(crTextColor);
	pDC->SetBkMode(TRANSPARENT);
	//pDC->SetBkColor(WHITE);
	pDC->TextOut(nXPos, nYPos, strText);
	CSize textExtent = pDC->GetTextExtent(strText);

	pDC->RestoreDC(-1);

	pDC->SelectObject(pOldFont);
	font.DeleteObject();

	pDC->SetTextColor(crOldColor);

	return textExtent;
}



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// CSectionMark

IMPLEMENT_SERIAL(CSectionMark, CObject, VERSIONABLE_SCHEMA | 3)

CSectionMark::CSectionMark()
{ 
	m_nType				= SectionMark;
	m_fFootMargin		= 0.0f;
	m_fHeadMargin		= 0.0f;
	m_fSafetyDistance	= 1.0f;
	m_fBarWidth			= 2.0f;
	m_fBarHeight		= 6.0f;
	m_nPosition			= Spine;
}

CSectionMark::CSectionMark(CString strName, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps, CPageSource& rMarkSource) 
						 : CMark(strName, 0.0f, 0.0f, TOP, bAllColors, 0, rColorSeps, rMarkSource)
{ 
	m_nType				=  SectionMark;
	m_strMarkName		= "SectionMark";
	m_fFootMargin		= 0.0f;
	m_fHeadMargin		= 0.0f;
	m_fSafetyDistance	= 1.0f;
	m_fBarWidth			= 2.0f;
	m_fBarHeight		= 6.0f;
	m_nPosition			= Spine;
}

void CSectionMark::Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList)
{
	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if (pDoc)
	//	if (pDoc->GetBoundProduct(g_nProductID).m_bindingParams.m_bindingDefs.m_nBinding == CBindingDefs::SaddleStitched)
	//		m_nPosition = Head_Foot;

	CString strNewMark;
	strNewMark.LoadString(IDS_NEW_SECTIONMARK);
	if (pMarkList)
		strNewMark = pMarkList->FindFreeMarkName(strNewMark);

	m_strMarkName = strNewMark;
	m_MarkSource.m_nType = CPageSource::SystemCreated;
	m_MarkSource.m_strSystemCreationInfo.Format(_T("$SECTION_BAR %.2f %.2f"), m_fBarWidth, m_fBarHeight);
	CPageSourceHeader markSourceHeader;
	markSourceHeader.m_bPageNumberFix = FALSE;
	markSourceHeader.m_nPageNumber	  = 1;
	markSourceHeader.m_strColorName	  = _T("Composite");
	markSourceHeader.m_rgb			  = WHITE;
	markSourceHeader.m_nColorIndex	  = 0;
	m_MarkSource.m_PageSourceHeaders.Add(markSourceHeader);
	InitializeTemplate(rColorDefTable);
}

// operator overload for CLayoutObject - needed by CList functions:
const CSectionMark& CSectionMark::operator=(const CSectionMark& rSectionMark)
{
	Copy(rSectionMark);	

	return *this;
}

BOOL CSectionMark::operator==(const CSectionMark& rSectionMark)
{
	if (CMark::operator!=(rSectionMark))						return FALSE;
	if (m_fFootMargin		!= rSectionMark.m_fFootMargin)		return FALSE;
	if (m_fHeadMargin		!= rSectionMark.m_fHeadMargin)		return FALSE;
	if (m_fSafetyDistance	!= rSectionMark.m_fSafetyDistance)	return FALSE;
	if (m_fBarWidth			!= rSectionMark.m_fBarWidth)		return FALSE;
	if (m_fBarHeight		!= rSectionMark.m_fBarHeight)		return FALSE;
	if (m_nPosition			!= rSectionMark.m_nPosition)		return FALSE;

	return TRUE;
}

BOOL CSectionMark::operator!=(const CSectionMark& rSectionMark)
{
	return !(*this == rSectionMark);
}

void CSectionMark::Copy(const CSectionMark& rSectionMark)
{
	CMark::Copy(rSectionMark);

	m_fFootMargin		= rSectionMark.m_fFootMargin;
	m_fHeadMargin		= rSectionMark.m_fHeadMargin;
	m_fSafetyDistance	= rSectionMark.m_fSafetyDistance;
	m_fBarWidth			= rSectionMark.m_fBarWidth;
	m_fBarHeight		= rSectionMark.m_fBarHeight;
	m_nPosition			= rSectionMark.m_nPosition;
}

void CSectionMark::Serialize(CArchive& ar)
{
	CMark::Serialize(ar);

	ar.SerializeClass(RUNTIME_CLASS(CSectionMark));

	if (ar.IsStoring())
	{
		ar << m_fFootMargin;
		ar << m_fHeadMargin;
		ar << m_fSafetyDistance;
		ar << m_fBarWidth;
		ar << m_fBarHeight;
		ar << m_nPosition;
	}											
	else																		 
	{
		UINT nVersion;

		nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> m_fFootMargin;
			ar >> m_fHeadMargin;
			ar >> m_fBarWidth;
			ar >> m_fBarHeight;
			m_nPosition = Spine;
			break;
		case 2:
			ar >> m_fFootMargin;
			ar >> m_fHeadMargin;
			ar >> m_fBarWidth;
			ar >> m_fBarHeight;
			ar >> m_nPosition;
			break;
		case 3:
			ar >> m_fFootMargin;
			ar >> m_fHeadMargin;
			ar >> m_fSafetyDistance;
			ar >> m_fBarWidth;
			ar >> m_fBarHeight;
			ar >> m_nPosition;
			break;
		}
	}
}


void CSectionMark::GetBBoxAll(CPrintSheet* pPrintSheet, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop)
{
	fBBoxLeft = fBBoxBottom = FLT_MAX; fBBoxRight = fBBoxTop = -FLT_MAX;

	if ( ! pPrintSheet)
		return;

	float fLeft, fBottom, fRight, fTop;
	for (short nComponentRefIndex = 0; nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
	{
		CLayoutObject* pRefPage = (CLayoutObject*)GetSheetTrimBox((void*)pPrintSheet, NULL, nComponentRefIndex, 0, fLeft, fBottom, fRight, fTop);
		if ( ! pRefPage)
			continue;
		if (pRefPage->GetLayoutSideIndex() != nSide)
			continue;

		fBBoxLeft	= min(fBBoxLeft,	fLeft);	
		fBBoxBottom = min(fBBoxBottom,	fBottom);
		fBBoxRight	= max(fBBoxRight,   fRight);
		fBBoxTop	= max(fBBoxTop,		fTop);
	}
}


int CSectionMark::AddToSheet(CPrintSheet* pPrintSheet, int nSide, CPageTemplate* pMarkTemplate, CMarkList* /*pMarkList*/)
{
	if ( ! pPrintSheet)
		return 0;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	float fLeft, fBottom, fRight, fTop;
	int	  nObjectsAdded = 0;
	for (short nComponentRefIndex = 0; nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
	{
		CFoldSheet* pFoldSheet = pPrintSheet->GetFoldSheet(nComponentRefIndex);
		BOOL bComingAndGoing = (pFoldSheet) ? pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing : FALSE;
		if (bComingAndGoing)		
		{
			int nCGIndex = 1;
			do
			{
				nCGIndex--;

				int nMarkHeadPosition;
				CLayoutObject* pRefPage = (CLayoutObject*)GetSheetTrimBox((void*)pPrintSheet, (void*)&nMarkHeadPosition, nComponentRefIndex, nCGIndex, fLeft, fBottom, fRight, fTop);
				if ( ! pRefPage)
					continue;
				if (pRefPage->GetLayoutSideIndex() != nSide)
					continue;
				if (pRefPage->GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))// identical mark (same position, same template) already
					continue;																					// present at layout side -> don't add again

				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, nMarkHeadPosition, pDoc->m_MarkTemplateList.GetIndex(pMarkTemplate));
				layoutObject.m_markBuddyLink = CMarkBuddyLink(pRefPage->GetLayoutSide()->m_ObjectList.GetObjectIndex(pRefPage), m_nPosition, -1);

				layoutObject.m_nComponentRefIndex = nComponentRefIndex;
				if (nCGIndex == -1)
					layoutObject.m_nAttribute |= CLayoutObject::FoldSheetCGMember;

				pRefPage->GetLayoutSide()->m_ObjectList.AddTail(layoutObject);
				nObjectsAdded++;
			}
			while (nCGIndex > -1);		
		}
		else
		{
			int nInstance = 0;
			do
			{
				nInstance++;
				int nMarkHeadPosition;
				CLayoutObject* pRefPage = (CLayoutObject*)GetSheetTrimBox((void*)pPrintSheet, (void*)&nMarkHeadPosition, nComponentRefIndex, nInstance, fLeft, fBottom, fRight, fTop);
				if ( ! pRefPage)
					break;
				if (pRefPage->GetLayoutSideIndex() != nSide)
					continue;
				if (pRefPage->GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))// identical mark (same position, same template) already
					continue;																					// present at layout side -> don't add again

				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, nMarkHeadPosition, pDoc->m_MarkTemplateList.GetIndex(pMarkTemplate));
				layoutObject.m_markBuddyLink = CMarkBuddyLink(pRefPage->GetLayoutSide()->m_ObjectList.GetObjectIndex(pRefPage), m_nPosition, -1);

				layoutObject.m_nComponentRefIndex = nComponentRefIndex;

				pRefPage->GetLayoutSide()->m_ObjectList.AddTail(layoutObject);
				nObjectsAdded++;
			}
			while (1);		
		}
	}

	return nObjectsAdded;
}


void* CSectionMark::GetSheetTrimBox(void* pParam1, void* pParam2, LPARAM nComponentRefIndex, LPARAM lParam2, float& fLeft, float& fBottom, float& fRight, float& fTop)
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pParam1;
	if ( ! pPrintSheet)
	{
		fLeft = fBottom = fRight = fTop = 0.0f;
		return NULL;
	}

	CLayoutObject *pRefPage = NULL;
	float fRefLeft = 0.0f, fRefBottom = 0.0f, fRefRight = 0.0f, fRefTop = 0.0f;																							
	float fWidth = 0.0f, fHeight = 0.0f;
	int nMarkHeadPosition = TOP;
	if (m_nPosition == Spine)
	{																																	  // lParam2 == -1 means ComingAndGoing
		pRefPage = pPrintSheet->GetLayout()->GetSectionMarkSpineArea(pPrintSheet, nComponentRefIndex, fRefLeft, fRefBottom, fRefRight, fRefTop, (lParam2 == -1) ? TRUE : FALSE, lParam2);
		if ( ! pRefPage)
			return NULL;

		for (int i = 0; i < pRefPage->m_markInstanceDefs.GetSize(); i++)
		{
			if ( (pRefPage->m_markInstanceDefs[i].m_nPosition == m_nPosition) && (pRefPage->m_markInstanceDefs[i].m_strParentMark == m_strMarkName) )
				if (pRefPage->m_markInstanceDefs[i].m_nAction == CMarkInstanceDefs::Disable)
					return NULL;
		}

		float fRefCenterX = fRefLeft   + (fRefRight - fRefLeft)   / 2.0f;
		float fRefCenterY = fRefBottom + (fRefTop   - fRefBottom) / 2.0f;
		BOOL bRotated	  = FALSE;
		switch (pRefPage->m_nHeadPosition)			// Calculate rectangle representing section bar
		{
		case TOP:		
		case BOTTOM:	if (fRefLeft == fRefRight)
						{
							if ( ((pRefPage->m_nHeadPosition == TOP) && (pRefPage->m_nIDPosition == RIGHT)) || ((pRefPage->m_nHeadPosition == BOTTOM) && (pRefPage->m_nIDPosition == LEFT)) )
								fLeft = fRefCenterX - max(pRefPage->GetCurrentGrossMargin(pPrintSheet, LEFT),  m_fBarWidth);
							else
								fLeft = fRefCenterX + max(pRefPage->GetCurrentGrossMargin(pPrintSheet, RIGHT), m_fBarWidth) - m_fBarWidth;
						}
						else
							fLeft = fRefCenterX  - m_fBarWidth/2.0f;
						fBottom  = fRefBottom + ((pRefPage->m_nHeadPosition == TOP ) ? m_fFootMargin : m_fHeadMargin);
						break;
		case LEFT:		
		case RIGHT:		fLeft = fRefLeft + ((pRefPage->m_nHeadPosition == LEFT) ? m_fHeadMargin : m_fFootMargin);
						if (fRefBottom == fRefTop)
						{
							if ( ((pRefPage->m_nHeadPosition == LEFT) && (pRefPage->m_nIDPosition == RIGHT)) || ((pRefPage->m_nHeadPosition == RIGHT) && (pRefPage->m_nIDPosition == LEFT)) )
								fBottom = fRefCenterY - max(pRefPage->GetCurrentGrossMargin(pPrintSheet, BOTTOM), m_fBarWidth);
							else
								fBottom = fRefCenterY + max(pRefPage->GetCurrentGrossMargin(pPrintSheet, TOP),	  m_fBarWidth) - m_fBarWidth;
						}
						else
							fBottom = fRefCenterY - m_fBarWidth/2.0f;
						bRotated = TRUE;
						break;
		}

		fWidth  = m_fBarWidth;
		fHeight = pRefPage->GetRealHeight() - m_fFootMargin - m_fHeadMargin;
		if (bRotated)
		{
			float fTemp;
			fTemp = fWidth; fWidth = fHeight; fHeight = fTemp;
		}
		nMarkHeadPosition = pRefPage->m_nHeadPosition;
	}
	else
		if (m_nPosition == Head_Foot)
		{																																		 // lParam2 == -1 means ComingAndGoing
			pRefPage = pPrintSheet->GetLayout()->GetSectionMarkHeadFootArea(pPrintSheet, nComponentRefIndex, fRefLeft, fRefBottom, fRefRight, fRefTop, (lParam2 == -1) ? TRUE : FALSE, lParam2);
			if ( ! pRefPage)
				return NULL;

			for (int i = 0; i < pRefPage->m_markInstanceDefs.GetSize(); i++)
			{
				if ( (pRefPage->m_markInstanceDefs[i].m_nPosition == m_nPosition) && (pRefPage->m_markInstanceDefs[i].m_strParentMark == m_strMarkName) )
					if (pRefPage->m_markInstanceDefs[i].m_nAction == CMarkInstanceDefs::Disable)
						return NULL;
			}

			float fRefCenterX = fRefLeft   + (fRefRight - fRefLeft)   / 2.0f;
			float fRefCenterY = fRefBottom + (fRefTop   - fRefBottom) / 2.0f;
			BOOL bRotated	  = FALSE;
			switch (pRefPage->m_nHeadPosition)			// Calculate rectangle representing section bar
			{
			case TOP:		
			case BOTTOM:	if (pRefPage->m_nIDPosition == RIGHT)	// place headposition of section mark toward the spine
								nMarkHeadPosition = (pRefPage->m_nHeadPosition == TOP) ? LEFT  : RIGHT;	
							else
								nMarkHeadPosition = (pRefPage->m_nHeadPosition == TOP) ? RIGHT : LEFT;
							if (fRefBottom == fRefTop)
							{
								if (pRefPage->m_nHeadPosition == TOP)
									fBottom  = fRefCenterY + m_fSafetyDistance;
								else
									fBottom  = fRefCenterY - m_fBarWidth - m_fSafetyDistance;
							}
							else
								fBottom  = fRefCenterY  - m_fBarWidth/2.0f;
							fLeft	 = fRefLeft		+ ((nMarkHeadPosition == LEFT) ? m_fHeadMargin : m_fFootMargin);
							bRotated = TRUE;
							break;
			case LEFT:		
			case RIGHT:		if (pRefPage->m_nIDPosition == RIGHT)	// place headposition of section mark toward the spine
								nMarkHeadPosition = (pRefPage->m_nHeadPosition == LEFT) ? BOTTOM : TOP;	
							else
								nMarkHeadPosition = (pRefPage->m_nHeadPosition == LEFT) ? TOP	 : BOTTOM;

							if (fRefLeft == fRefRight)
							{
								if (pRefPage->m_nHeadPosition == LEFT)
									fLeft = fRefCenterX - m_fBarWidth - m_fSafetyDistance;
								else
									fLeft = fRefCenterX + m_fSafetyDistance;
							}
							else
								fLeft	 = fRefCenterX  - m_fBarWidth/2.0f;
							fBottom  = fRefBottom   + ((nMarkHeadPosition == TOP) ? m_fFootMargin : m_fHeadMargin);
							break;
			}

			fWidth  = m_fBarWidth;
			fHeight = pRefPage->GetRealWidth() - m_fFootMargin - m_fHeadMargin;
			if (bRotated)
			{
				float fTemp;
				fTemp = fWidth; fWidth = fHeight; fHeight = fTemp;
			}
		}

	fRight = fLeft   + fWidth;
	fTop   = fBottom + fHeight;

	if (pParam2)
		*((int*)pParam2) = nMarkHeadPosition;

	return (void*)pRefPage;
}


void CSectionMark::DrawSheetPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, CMarkList* pMarkList, BOOL bShowContent, int /*nEdge*/, BOOL bShowHighlighted)
{
	if ( ! pPrintSheet)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	//if ( ! m_rules.GetRuleExpression(0).IsEmpty())
	//	if ( ! IsEnabled(pPrintSheet))
	//		return;

	CColorDefinitionTable& rColorDefTable = GetActColorDefTable(pMarkList);

	for (short nComponentRefIndex = 0; nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
	{
		CFoldSheet* pFoldSheet = pPrintSheet->GetFoldSheet(nComponentRefIndex);
		BOOL bComingAndGoing = (pFoldSheet) ? pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing : FALSE;
		if (bComingAndGoing)		
		{
			int nCGIndex = 1;
			do
			{
				nCGIndex--;

				float fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;
				int	  nMarkHeadPosition;
				CLayoutObject* pRefPage = (CLayoutObject*)GetSheetTrimBox((void*)pPrintSheet, (void*)&nMarkHeadPosition, nComponentRefIndex, nCGIndex, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);
				if ( ! pRefPage)
					continue;
				if (pRefPage->GetLayoutSideIndex() != nSide)
					continue;

				CRect markRect;
				markRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
				markRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
				markRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
				markRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);

   				CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   				if (pView)
				{
					int nSheetID = (nSide << 16) + pPrintSheet->m_nPrintSheetNumber;
					pView->m_DisplayList.RegisterItem(pDC, markRect, markRect, (void*)this, 
													  SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Mark), (void*)nSheetID, CDisplayItem::ForceRegister); 
				}

				if (pDC->RectVisible(markRect))
				{
					CPen	framePen(PS_SOLID, 1, (bShowHighlighted) ? MARKFRAMECOLOR_HIGHLIGHT : MARKCOLOR);	// Lightred
					CPen*	pOldPen	  = pDC->SelectObject(&framePen);
					CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

					CSize sizeOffset(1,1); pDC->DPtoLP(&sizeOffset);
					markRect.OffsetRect(sizeOffset.cx, 0);
				
					if (bShowHighlighted)
						HighlightMark(pDC, markRect, pPrintSheet);

					if (bShowContent)
					{
						if (GetSeparationInfo(pPrintSheet->GetTopmostPlate(nSide), rColorDefTable, TRUE).m_nColorDefinitionIndex != -1)	// section mark is defined for topmost plate color
						{
							BOOL bNumRotated180 = (m_MarkSource.m_strSystemCreationInfo.Find(_T("NUMROTATED:180")) == -1) ? FALSE : TRUE;
							BOOL bHollow		= (m_MarkSource.m_strSystemCreationInfo.Find(_T("HOLLOW"))		   == -1) ? FALSE : TRUE;
							DrawSectionBarPreview(pDC, CPrintSheetView::WorldToLP(m_fBarWidth), CPrintSheetView::WorldToLP(m_fBarHeight), 
								bNumRotated180, bHollow, markRect, nComponentRefIndex, nMarkHeadPosition, pPrintSheet, (nCGIndex == -1) ? TRUE : FALSE, m_strMarkName, nSide);
						}

						CPrintSheetView::DrawRectangleExt(pDC, markRect);
					}
					else
					{
						if ( ! bShowHighlighted)
						{
							MINSIZE_MARK(markRect);
							CPrintSheetView::FillTransparent (pDC, markRect, (bShowHighlighted) ? MARKCOLOR_HIGHLIGHT : MARKCOLOR, TRUE, 150);
						}
						CPrintSheetView::DrawRectangleExt(pDC, markRect);
					}

					pDC->SelectObject(pOldBrush);
					pDC->SelectObject(pOldPen);
					framePen.DeleteObject();
				}
			}
			while (nCGIndex > -1);		
		}
		else
		{
			int nInstance = 0;
			do
			{
				nInstance++;
				float fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;
				int	  nMarkHeadPosition;
				CLayoutObject* pRefPage = (CLayoutObject*)GetSheetTrimBox((void*)pPrintSheet, (void*)&nMarkHeadPosition, nComponentRefIndex, nInstance, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);
				if ( ! pRefPage)
					break;
				if (pRefPage->GetLayoutSideIndex() != nSide)
					continue;

				CRect markRect;
				markRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
				markRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
				markRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
				markRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);

   				CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   				if (pView)
				{
					int nSheetID = (nSide << 16) + pPrintSheet->m_nPrintSheetNumber;
					pView->m_DisplayList.RegisterItem(pDC, markRect, markRect, (void*)this, 
													  SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Mark), (void*)nSheetID, CDisplayItem::ForceRegister); 
				}

				if (pDC->RectVisible(markRect))
				{
					CPen	framePen(PS_SOLID, 1, (bShowHighlighted) ? MARKFRAMECOLOR_HIGHLIGHT : MARKCOLOR);	// Lightred
					CPen*	pOldPen	  = pDC->SelectObject(&framePen);
					CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

					CSize sizeOffset(1,1); pDC->DPtoLP(&sizeOffset);
					markRect.OffsetRect(sizeOffset.cx, 0);
				
					if (bShowHighlighted)
						HighlightMark(pDC, markRect, pPrintSheet);

					if (bShowContent)
					{
						if (GetSeparationInfo(pPrintSheet->GetTopmostPlate(nSide), rColorDefTable, TRUE).m_nColorDefinitionIndex != -1)	// section mark is defined for topmost plate color
						{
							BOOL bNumRotated180 = (m_MarkSource.m_strSystemCreationInfo.Find(_T("NUMROTATED:180")) == -1) ? FALSE : TRUE;
							BOOL bHollow		= (m_MarkSource.m_strSystemCreationInfo.Find(_T("HOLLOW"))		   == -1) ? FALSE : TRUE;
							DrawSectionBarPreview(pDC, CPrintSheetView::WorldToLP(m_fBarWidth), CPrintSheetView::WorldToLP(m_fBarHeight), 
								bNumRotated180, bHollow, markRect, nComponentRefIndex, nMarkHeadPosition, pPrintSheet, FALSE, m_strMarkName, nSide);
						}

						CPrintSheetView::DrawRectangleExt(pDC, markRect);
					}
					else
					{
						if ( ! bShowHighlighted)
						{
							MINSIZE_MARK(markRect);
							CPrintSheetView::FillTransparent (pDC, markRect, (bShowHighlighted) ? MARKCOLOR_HIGHLIGHT : MARKCOLOR, TRUE, 150);
						}
						CPrintSheetView::DrawRectangleExt(pDC, markRect);
					}

					pDC->SelectObject(pOldBrush);
					pDC->SelectObject(pOldPen);
					framePen.DeleteObject();
				}
			}
			while (1);		
		}
	}
}

CRect CSectionMark::GetSectionBarRect(long nBarWidth, long nBarHeight, const CRect& rOutBBox, short nComponentRefIndex, int nHeadPosition, CPrintSheet* pPrintSheet)
{
	CRect rectBar(0,0,0,0);

	if ( ! pPrintSheet)
		return rectBar;
	CFoldSheet* pFoldSheet = pPrintSheet->GetFoldSheet(nComponentRefIndex);
	if ( ! pFoldSheet)
		return rectBar;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rectBar;

	if ((nBarWidth == 0) || (nBarHeight == 0))
		return rectBar;

	CPoint ptCenter = rOutBBox.CenterPoint();
	int	   nMax		= ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? rOutBBox.Height() / nBarHeight : rOutBBox.Width() / nBarHeight;
	if (nMax == 0)
		return rectBar;

	int nFoldSheetNumber = pFoldSheet->GetNumberOnly();
	int		nSkip	= (nFoldSheetNumber - 1) % nMax;
	int		nOffset = nSkip * nBarHeight;

	switch (nHeadPosition)
	{
	case BOTTOM:	rectBar.left   = ptCenter.x - nBarWidth/2;   rectBar.right = rectBar.left   + nBarWidth;
					rectBar.bottom = rOutBBox.bottom;			 rectBar.top   = rectBar.bottom + nBarHeight;
					rectBar.OffsetRect(0, nOffset);
					break;

	case RIGHT:		rectBar.left   = rOutBBox.right - nBarHeight;rectBar.right = rectBar.left   + nBarHeight;
					rectBar.bottom = ptCenter.y - nBarWidth/2;	 rectBar.top   = rectBar.bottom + nBarWidth;
					rectBar.OffsetRect(-nOffset, 0);
					break;

	case TOP:		rectBar.left   = ptCenter.x - nBarWidth/2;   rectBar.right = rectBar.left   + nBarWidth;
					rectBar.bottom = rOutBBox.top - nBarHeight;  rectBar.top   = rectBar.bottom + nBarHeight;
					rectBar.OffsetRect(0, -nOffset);
					break;

	case LEFT:		rectBar.left   = rOutBBox.left;				 rectBar.right = rectBar.left   + nBarHeight;
					rectBar.bottom = ptCenter.y - nBarWidth/2;	 rectBar.top   = rectBar.bottom + nBarWidth;
					rectBar.OffsetRect(nOffset, 0);
					break;
	}

	return rectBar;
}

void CSectionMark::DrawSectionBarPreview(CDC* pDC, long nBarWidth, long nBarHeight, BOOL bNumRotated180, BOOL bHollow, const CRect& rOutBBox, short nComponentRefIndex, int nHeadPosition, CPrintSheet* pPrintSheet, BOOL bComingAndGoing, CString strMarkName, int nSide)
{
	if ( ! pPrintSheet)
		return;
	CFoldSheet* pFoldSheet = pPrintSheet->GetFoldSheet(nComponentRefIndex);
	if ( ! pFoldSheet)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ((nBarWidth == 0) || (nBarHeight == 0))
		return;

	int nFoldSheetNumber = pFoldSheet->GetNumberOnly();
	if (bComingAndGoing)
	{
		CFoldSheet* pSimilarFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.CGGetSimilarFoldSheet(*pFoldSheet, INT_MAX);
		nFoldSheetNumber = (pSimilarFoldSheet) ? pSimilarFoldSheet->GetNumberOnly() : pFoldSheet->GetNumberOnly();
	}
	CRect coverRect   = FindCoveringMarks(pPrintSheet, rOutBBox, strMarkName, nSide);
	BOOL bIsCovered	  = (coverRect.IsRectNull()) ? FALSE : TRUE;
	coverRect.left	 -= CPrintSheetView::WorldToLP(3.0f);
	coverRect.bottom -= CPrintSheetView::WorldToLP(3.0f);
	coverRect.right  += CPrintSheetView::WorldToLP(3.0f);
	coverRect.top    += CPrintSheetView::WorldToLP(3.0f);
	int	  nMax1     = 0, nMax2  = 0;
	int   nSkip     = 0;
	int   nOffset   = 0;
	if ( (nHeadPosition == TOP) || (nHeadPosition == BOTTOM) )
	{
		nMax1 = ( ! bIsCovered) ? (abs(rOutBBox.Height()) / nBarHeight) : (rOutBBox.top     - coverRect.top)   / nBarHeight;
		nMax2 = ( ! bIsCovered) ? 0										: (coverRect.bottom - rOutBBox.bottom) / nBarHeight;
		if ( (nMax1 == 0) && (nMax2 == 0) )
			return;
		nSkip = (nFoldSheetNumber - 1) % (nMax1 + nMax2);
		nOffset = nSkip * nBarHeight;
		if (bIsCovered)
		{
			if (nHeadPosition == TOP)
			{
				if ((nSkip + 1) > nMax1)
					nOffset = (rOutBBox.top  - coverRect.bottom) + (nSkip - nMax1) * nBarHeight;
			}
			else
			{
				if ((nSkip + 1) > nMax2)
					nOffset = (coverRect.top - rOutBBox.bottom) + (nSkip - nMax2) * nBarHeight;
			}
		}
	}
	else
	{
		nMax1 = ( ! bIsCovered) ? (rOutBBox.Width() / nBarHeight) : (coverRect.left - rOutBBox.left)   / nBarHeight;
		nMax2 = ( ! bIsCovered) ? 0								  : (rOutBBox.right - coverRect.right) / nBarHeight;
		if ( (nMax1 == 0) && (nMax2 == 0) )
			return;
		nSkip = (nFoldSheetNumber - 1) % (nMax1 + nMax2);
		nOffset = nSkip * nBarHeight;
		if (bIsCovered)
		{
			if (nHeadPosition == LEFT)
			{
				if ((nSkip + 1) > nMax1)
					nOffset = (coverRect.right - rOutBBox.left) + (nSkip - nMax1) * nBarHeight;
			}
			else
			{
				if ((nSkip + 1) > nMax2)
					nOffset = (rOutBBox.right  - coverRect.left) + (nSkip - nMax2) * nBarHeight;
			}
		}
	}

	CString strNum;
	strNum.Format(_T("%d"), nFoldSheetNumber);

	int nTextHead = nHeadPosition;
	if (bNumRotated180)
		switch (nTextHead)
		{
		case TOP:	 nTextHead = BOTTOM; break;
		case RIGHT:  nTextHead = LEFT;   break;
		case BOTTOM: nTextHead = TOP;	 break;
		case LEFT:	 nTextHead = RIGHT;  break;
		}
	if (strNum.GetLength() > 1)
		switch (nTextHead)
		{
		case TOP:	 nTextHead = LEFT;   break;
		case RIGHT:  nTextHead = TOP;    break;
		case BOTTOM: nTextHead = RIGHT;	 break;
		case LEFT:	 nTextHead = BOTTOM; break;
		}


	CFont sectionBarFont;
	CreateSectionBarFont(pDC, strNum, nBarWidth, &sectionBarFont, nTextHead);
	CFont* pOldFont	  = pDC->SelectObject(&sectionBarFont);
	CSize  textExtent = pDC->GetTextExtent(strNum);
	int	   nTextPosX  = 0, nTextPosY = 0;
	CPoint ptCenter   = rOutBBox.CenterPoint();
	CRect  rectBar;
	switch (nHeadPosition)
	{
	case BOTTOM:	rectBar.left   = ptCenter.x - nBarWidth/2;   rectBar.right = rectBar.left   + nBarWidth;
					rectBar.bottom = rOutBBox.bottom;			 rectBar.top   = rectBar.bottom + nBarHeight;
					rectBar.OffsetRect(0, nOffset);
					nTextPosX = rectBar.CenterPoint().x + textExtent.cx/2;
					nTextPosY = rectBar.CenterPoint().y - textExtent.cy/2;
					break;

	case RIGHT:		rectBar.left   = rOutBBox.right - nBarHeight;rectBar.right = rectBar.left   + nBarHeight;
					rectBar.bottom = ptCenter.y - nBarWidth/2;	 rectBar.top   = rectBar.bottom + nBarWidth;
					rectBar.OffsetRect(-nOffset, 0);
					nTextPosX = rectBar.CenterPoint().x + textExtent.cy/2;
					nTextPosY = rectBar.CenterPoint().y + textExtent.cx/2;
					break;

	case TOP:		rectBar.left   = ptCenter.x - nBarWidth/2;   rectBar.right = rectBar.left   + nBarWidth;
					rectBar.bottom = rOutBBox.top - nBarHeight;  rectBar.top   = rectBar.bottom + nBarHeight;
					rectBar.OffsetRect(0, -nOffset);
					nTextPosX = rectBar.CenterPoint().x - textExtent.cx/2;
					nTextPosY = rectBar.CenterPoint().y + textExtent.cy/2;
					break;

	case LEFT:		rectBar.left   = rOutBBox.left;				 rectBar.right = rectBar.left   + nBarHeight;
					rectBar.bottom = ptCenter.y - nBarWidth/2;	 rectBar.top   = rectBar.bottom + nBarWidth;
					rectBar.OffsetRect(nOffset, 0);
					nTextPosX = rectBar.CenterPoint().x - textExtent.cy/2;
					nTextPosY = rectBar.CenterPoint().y - textExtent.cx/2;
					break;
	}

	switch (nTextHead)
	{
	case BOTTOM:	nTextPosX = rectBar.CenterPoint().x + textExtent.cx/2;
					nTextPosY = rectBar.CenterPoint().y - textExtent.cy/2;
					break;

	case RIGHT:		nTextPosX = rectBar.CenterPoint().x + textExtent.cy/2;
					nTextPosY = rectBar.CenterPoint().y + textExtent.cx/2;
					break;

	case TOP:		nTextPosX = rectBar.CenterPoint().x - textExtent.cx/2;
					nTextPosY = rectBar.CenterPoint().y + textExtent.cy/2;
					break;

	case LEFT:		nTextPosX = rectBar.CenterPoint().x - textExtent.cy/2;
					nTextPosY = rectBar.CenterPoint().y - textExtent.cx/2;
					break;
	}

	pDC->FillSolidRect(rOutBBox, WHITE);
	if (bHollow)
	{
		CPen framePen(PS_SOLID, CPrintSheetView::WorldToLP(0.2f), BLACK);
		CPen*	pOldPen	  = pDC->SelectObject(&framePen);
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
		CSize size(1,1);
		pDC->DPtoLP(&size);
		rectBar.NormalizeRect();
		rectBar.DeflateRect(size);
		CPrintSheetView::DrawRectangleExt(pDC, rectBar);
		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		framePen.DeleteObject();
	}
	else
		pDC->FillSolidRect(rectBar,  BLACK);

	pDC->SaveDC();	// save current clipping region
	pDC->IntersectClipRect(rectBar);
	pDC->SetTextColor((bHollow) ? BLACK : WHITE);
	pDC->SetBkMode(TRANSPARENT);
	pDC->TextOut(nTextPosX, nTextPosY, strNum); 
	pDC->RestoreDC(-1);

	pDC->SelectObject(pOldFont);
	sectionBarFont.DeleteObject();
}

CRect CSectionMark::FindCoveringMarks(CPrintSheet* pPrintSheet, CRect rcRefMark, CString strMarkName, int nSide)
{
	if ( ! pPrintSheet || strMarkName.IsEmpty())
		return CRect(0,0,0,0);

	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return CRect(0,0,0,0);
	CLayoutSide* pLayoutSide = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;

	CMark* pThisMark = pLayout->m_markList.FindMark(strMarkName);
	if ( ! pThisMark)
		return CRect(0,0,0,0);

	rcRefMark.NormalizeRect();
	CRect	 rcCurrent, rcIntersect(0,0,0,0);
	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = pLayoutSide->m_ObjectList.GetNext(pos);
		if (rLayoutObject.m_nType == CLayoutObject::Page)
			continue;
		CMark* pMark = pLayout->m_markList.FindMark(rLayoutObject.m_strFormatName);
		if ( ! pMark)
			continue;
		if (pMark->m_strMarkName == strMarkName)
			continue;
		if ( (pMark->m_nType != CMark::CustomMark) && (pMark->m_nType != CMark::TextMark) && (pMark->m_nType != CMark::BarcodeMark) && (pMark->m_nType != CMark::DrawingMark) )
			continue;
		if (pMark->m_nZOrderIndex > pThisMark->m_nZOrderIndex)
			continue;

		rcCurrent.left		= CPrintSheetView::WorldToLP(rLayoutObject.GetLeft());
		rcCurrent.bottom	= CPrintSheetView::WorldToLP(rLayoutObject.GetBottom()); 
		rcCurrent.right		= CPrintSheetView::WorldToLP(rLayoutObject.GetRight());
		rcCurrent.top		= CPrintSheetView::WorldToLP(rLayoutObject.GetTop());
		rcCurrent.NormalizeRect();

		rcIntersect.IntersectRect(rcRefMark, rcCurrent);
		if ( ! rcIntersect.IsRectEmpty())
			break;
	}

	int nTemp = rcIntersect.top; rcIntersect.top = rcIntersect.bottom; rcIntersect.bottom = nTemp;	// rects has been normalized -> undo this
	return rcIntersect;
}


void CSectionMark::CreateSectionBarFont(CDC* pDC, const CString& strNum, long nBarWidth, CFont* pFont, int nTextHead)
{
	pFont->CreateFont(1000, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FIXED_PITCH, _T("Courier New"));	
	CFont* pOldFont = pDC->SelectObject(pFont);
	CSize textExtent = pDC->GetTextExtent(strNum);
	pDC->SelectObject(pOldFont);
	float fFactor    = (float)nBarWidth / (float)((strNum.GetLength() > 1) ? textExtent.cy : textExtent.cx);

	int nOrientation;
	switch (nTextHead)
	{
	case TOP:	 nOrientation = 0;    break;
	case RIGHT:  nOrientation = 900;  break;
	case BOTTOM: nOrientation = 1800; break;
	case LEFT:	 nOrientation = 2700; break;
	default:	 nOrientation = 0;    break;
	}

	pFont->DeleteObject();
	pFont->CreateFont((int)(1000.0f * fFactor), 0, nOrientation, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FIXED_PITCH, _T("Courier New"));	
}


//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// CCropMark

IMPLEMENT_SERIAL(CCropMark, CObject, VERSIONABLE_SCHEMA | 1)

CCropMark::CCropMark()
{ 
	m_nType			= CropMark;
	m_fPageDistance = 2.0f;
	m_fLineWidth	= 0.1f;	
	m_fLength		= 4.0f;
}

CCropMark::CCropMark(CString strName, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps, CPageSource& rMarkSource) 
					 : CMark(strName, 0.0f, 0.0f, TOP, bAllColors, 0, rColorSeps, rMarkSource)
{ 
	m_nType		  =  CropMark;
	m_strMarkName = "CropMark";
	m_fPageDistance = 2.0f;
	m_fLineWidth	= 0.1f;
	m_fLength		= 4.0f;
}

void CCropMark::Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList)
{
	CString strNewMark;
	strNewMark.LoadString(IDS_NEW_CROPMARK);
	if (pMarkList)
		strNewMark = pMarkList->FindFreeMarkName(strNewMark);

	m_strMarkName = strNewMark;
	m_MarkSource.m_nType = CPageSource::SystemCreated;
	m_MarkSource.m_strSystemCreationInfo.Format(_T("$CROP_LINE %.2f %.2f"), m_fLineWidth, m_fLength);
	CPageSourceHeader markSourceHeader;
	markSourceHeader.m_bPageNumberFix = FALSE;
	markSourceHeader.m_nPageNumber	  = 1;
	markSourceHeader.m_strColorName	  = _T("Composite");
	markSourceHeader.m_rgb			  = WHITE;
	markSourceHeader.m_nColorIndex	  = 0;
	m_MarkSource.m_PageSourceHeaders.Add(markSourceHeader);
	InitializeTemplate(rColorDefTable);
}


// operator overload for CLayoutObject - needed by CList functions:
const CCropMark& CCropMark::operator=(const CCropMark& rCropMark)
{
	Copy(rCropMark);	

	return *this;
}

BOOL CCropMark::operator==(const CCropMark& rCropMark)
{
	if (CMark::operator!=(rCropMark))	return FALSE;
	if (m_fPageDistance != rCropMark.m_fPageDistance)	return FALSE;
	if (m_fLineWidth	!= rCropMark.m_fLineWidth)		return FALSE;
	if (m_fLength		!= rCropMark.m_fLength)			return FALSE;

	return TRUE;
}

BOOL CCropMark::operator!=(const CCropMark& rCropMark)
{
	return !(*this == rCropMark);
}

void CCropMark::Copy(const CCropMark& rCropMark)
{
	CMark::Copy(rCropMark);

	m_fPageDistance = rCropMark.m_fPageDistance;
	m_fLineWidth	= rCropMark.m_fLineWidth;
	m_fLength		= rCropMark.m_fLength;
}

void CCropMark::Serialize(CArchive& ar)
{
	CMark::Serialize(ar);

	ar.SerializeClass(RUNTIME_CLASS(CCropMark));

	if (ar.IsStoring())
	{
		ar << m_fPageDistance;
		ar << m_fLineWidth;
		ar << m_fLength;
	}											
	else																		 
	{
		UINT nVersion;

		nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> m_fPageDistance;
			ar >> m_fLineWidth;
			ar >> m_fLength;
			break;
		}
	}
}


void CCropMark::GetBBoxAll(CPrintSheet* pPrintSheet, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop)
{
	fBBoxLeft = fBBoxBottom = FLT_MAX; fBBoxRight = fBBoxTop = -FLT_MAX;

	if ( ! pPrintSheet)
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;

	CLayoutObjectList* pObjectList = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide.m_ObjectList : &pLayout->m_BackSide.m_ObjectList;

	float	 fLeft, fBottom, fRight, fTop;
	POSITION pos = pObjectList->GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pObjectList->GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
			continue;

		BOOL bIsCoverFront = FALSE;
		if (rObject.GetCurrentFoldSheet(pPrintSheet)->IsCover())
		{
			CPageTemplate* pPageTemplate = rObject.GetCurrentPageTemplate(pPrintSheet);
			if (pPageTemplate->IsFirstOrLastPage())
				bIsCoverFront = TRUE;
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		HORIZONTAL, fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}
	
		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		VERTICAL,   fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		HORIZONTAL, fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		VERTICAL,   fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	HORIZONTAL, fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	VERTICAL,   fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,	HORIZONTAL, fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,	VERTICAL,   fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}
	}
}


int CCropMark::AddToSheet(CPrintSheet* pPrintSheet, int nSide, CPageTemplate* pMarkTemplate, CMarkList* pMarkList)
{
	if ( ! pPrintSheet)
		return 0;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return 0;

	CLayoutObjectList* pObjectList = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide.m_ObjectList : &pLayout->m_BackSide.m_ObjectList;

	float fLeft, fBottom, fRight, fTop;

	CFoldMark*	pFoldMark		= (pMarkList) ? (CFoldMark*)pMarkList->GetFirstMark(CMark::FoldMark) : NULL;
	short		nTemplateIndex	= CImpManDoc::GetDoc()->m_MarkTemplateList.GetIndex(pMarkTemplate);
	int			nObjectsAdded	= 0;
	POSITION	pos				= pObjectList->GetHeadPosition();
	while (pos)
	{
		int			   nObjectIndex = pObjectList->GetIndex(pos);
		CLayoutObject& rObject		= pObjectList->GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
			continue;

		BOOL bIsCoverFront = FALSE;
		if (rObject.GetCurrentFoldSheet(pPrintSheet)->IsCover())
		{
			CPageTemplate* pPageTemplate = rObject.GetCurrentPageTemplate(pPrintSheet);
			if (pPageTemplate)
				if (pPageTemplate->IsFirstOrLastPage())
					bIsCoverFront = TRUE;
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		HORIZONTAL, fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, LEFT_TOP, HORIZONTAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}
		
		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		VERTICAL,   fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, LEFT_TOP, VERTICAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		HORIZONTAL, fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, RIGHT_TOP, HORIZONTAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		VERTICAL,   fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, RIGHT_TOP, VERTICAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	HORIZONTAL, fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, RIGHT_BOTTOM, HORIZONTAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	VERTICAL,   fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, RIGHT_BOTTOM, VERTICAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,		HORIZONTAL, fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, LEFT_BOTTOM, HORIZONTAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,		VERTICAL,   fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, LEFT_BOTTOM, VERTICAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}
	}

	return nObjectsAdded;
}


void* CCropMark::GetSheetTrimBox(void* pParam1, void* pParam2, LPARAM lParam1, LPARAM lParam2, float& fLeft, float& fBottom, float& fRight, float& fTop)
{
	CLayoutObject* pObject = (CLayoutObject*)pParam1;
	if ( ! pObject)
	{
		fLeft = fBottom = fRight = fTop = 0.0f;
		return NULL;
	}

	for (int i = 0; i < pObject->m_markInstanceDefs.GetSize(); i++)
	{
		if ( (pObject->m_markInstanceDefs[i].m_nPosition == lParam1) && (pObject->m_markInstanceDefs[i].m_nOrientation == lParam2) && (pObject->m_markInstanceDefs[i].m_strParentMark == m_strMarkName) )
			if (pObject->m_markInstanceDefs[i].m_nAction == CMarkInstanceDefs::Disable)
				return NULL;
	}

	BOOL bIsCoverFront = (pParam2) ? (BOOL)pParam2 : FALSE;
	if (bIsCoverFront)
	{
		switch (pObject->GetSpineSide())
		{
		case LEFT:		if ( (lParam1 == LEFT_TOP)		|| (lParam1 == LEFT_BOTTOM) )
							return NULL;
						break;
		case RIGHT:		if ( (lParam1 == RIGHT_TOP)		|| (lParam1 == RIGHT_BOTTOM) )
							return NULL;
						break;
		case BOTTOM:	if ( (lParam1 == LEFT_BOTTOM)	|| (lParam1 == RIGHT_BOTTOM) )
							return NULL;
						break;
		case TOP:		if ( (lParam1 == LEFT_TOP)		|| (lParam1 == RIGHT_TOP) )
							return NULL;
						break;
		}
	}

	int	  nDirection	= 0;
	void* pRet			= NULL;
	BOOL  bIsCoverSpine = FALSE;
	if (lParam2 == HORIZONTAL)
	{
		nDirection = HORIZONTAL;
		switch (lParam1)
		{
		case LEFT_TOP:		fRight = pObject->GetLeft()   - m_fPageDistance;		fLeft   = fRight - m_fLength;	
							fTop   = pObject->GetTop()    + m_fLineWidth / 2.0f;	fBottom = fTop	 - m_fLineWidth; 
							break;

		case RIGHT_TOP:		fLeft  = pObject->GetRight()  + m_fPageDistance;		fRight  = fLeft  + m_fLength;	
							fTop   = pObject->GetTop()    + m_fLineWidth / 2.0f;	fBottom = fTop	 - m_fLineWidth; 
							break;

		case RIGHT_BOTTOM:	fLeft  = pObject->GetRight()  + m_fPageDistance;		fRight  = fLeft  + m_fLength;	
							fTop   = pObject->GetBottom() + m_fLineWidth / 2.0f;	fBottom = fTop	 - m_fLineWidth; 
							break;

		case LEFT_BOTTOM:	fRight = pObject->GetLeft()	  - m_fPageDistance;		fLeft   = fRight - m_fLength;	
							fTop   = pObject->GetBottom() + m_fLineWidth / 2.0f;	fBottom = fTop	 - m_fLineWidth; 
							break;
		}
	}
	else
	{
		nDirection = VERTICAL;
		switch (lParam1)
		{
		case LEFT_TOP:		fLeft   = pObject->GetLeft()   - m_fLineWidth / 2.0f;	fRight  = fLeft		+ m_fLineWidth;	
							fBottom = pObject->GetTop()    + m_fPageDistance;		fTop	= fBottom	+ m_fLength;	
							break;

		case RIGHT_TOP:		fLeft   = pObject->GetRight()  - m_fLineWidth / 2.0f;	fRight  = fLeft		+ m_fLineWidth;	
							fBottom = pObject->GetTop()    + m_fPageDistance;		fTop	= fBottom	+ m_fLength;	
							break;

		case RIGHT_BOTTOM:	fLeft   = pObject->GetRight()  - m_fLineWidth / 2.0f;	fRight  = fLeft		+ m_fLineWidth;	
							fTop    = pObject->GetBottom() - m_fPageDistance;		fBottom = fTop		- m_fLength;	
							break;

		case LEFT_BOTTOM:	fLeft   = pObject->GetLeft()   - m_fLineWidth / 2.0f;	fRight  = fLeft		+ m_fLineWidth;	
							fTop    = pObject->GetBottom() - m_fPageDistance;		fBottom = fTop		- m_fLength;	
							break;
		}
	}

	CLayoutSide* pLayoutSide = pObject->GetLayoutSide();
	return (void*)pLayoutSide->ClipToObjects(fLeft, fBottom, fRight, fTop, m_fPageDistance, nDirection, TRUE);
}


void CCropMark::DrawSheetPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, CMarkList* pMarkList, BOOL bShowContent, int /*nEdge*/, BOOL bShowHighlighted)
{
	if ( ! pPrintSheet)
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;
	//if ( ! m_rules.GetRuleExpression(0).IsEmpty())
	//	if ( ! IsEnabled(pPrintSheet))
	//		return;

	CColorDefinitionTable& rColorDefTable = GetActColorDefTable(pMarkList);

	CLayoutObjectList* pObjectList = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide.m_ObjectList : &pLayout->m_BackSide.m_ObjectList;

	float fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;

	CFoldMark*	pFoldMark = (pMarkList) ? (CFoldMark*)pMarkList->GetFirstMark(CMark::FoldMark) : NULL;
	POSITION	pos		  = pObjectList->GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pObjectList->GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
			continue;

		BOOL bIsCoverFront = FALSE;
		if (rObject.GetCurrentFoldSheet(pPrintSheet)->IsCover())
		{
			CPageTemplate* pPageTemplate = rObject.GetCurrentPageTemplate(pPrintSheet);
			if (pPageTemplate->IsFirstOrLastPage())
				bIsCoverFront = TRUE;
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		HORIZONTAL,  fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawCropMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		VERTICAL,    fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawCropMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		HORIZONTAL,  fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawCropMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		VERTICAL,	 fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawCropMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	HORIZONTAL,  fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawCropMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	VERTICAL,	 fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawCropMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,		HORIZONTAL,  fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawCropMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,		VERTICAL,	 fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawCropMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);
	}
}

void CCropMark::DrawCropMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, CColorDefinitionTable& rColorDefTable, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop,
									BOOL bShowContent, BOOL bShowHighlighted)
{
	CRect markRect;
	markRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
	markRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
	markRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
	markRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   	if (pView)
	{
		int nSheetID = (nSide << 16) + pPrintSheet->m_nPrintSheetNumber;
		pView->m_DisplayList.RegisterItem(pDC, markRect, markRect, (void*)this, 
										  SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Mark), (void*)nSheetID, CDisplayItem::ForceRegister); 
	}

	if (pDC->RectVisible(markRect))
	{
		CPen	framePen(PS_SOLID, 1, (bShowHighlighted) ? MARKFRAMECOLOR_HIGHLIGHT : MARKCOLOR);	// Lightred
		CPen*	pOldPen	  = pDC->SelectObject(&framePen);
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
	
		CSize sizeOffset(1,1); pDC->DPtoLP(&sizeOffset);
		markRect.OffsetRect(sizeOffset.cx, 0);
			
		if (bShowHighlighted)
			HighlightMark(pDC, markRect, pPrintSheet);

		if (bShowContent)
		{
			if (GetSeparationInfo(pPrintSheet->GetTopmostPlate(nSide), rColorDefTable, TRUE).m_nColorDefinitionIndex != -1)	// section mark is defined for topmost plate color
				DrawCropLinePreview(pDC, markRect);
			CPrintSheetView::DrawRectangleExt(pDC, markRect);
		}
		else
		{
			if ( ! bShowHighlighted)
			{
				MINSIZE_MARK(markRect);
				CPrintSheetView::FillTransparent (pDC, markRect, (bShowHighlighted) ? MARKCOLOR_HIGHLIGHT : MARKCOLOR, TRUE, 150);
			}
			CPrintSheetView::DrawRectangleExt(pDC, markRect);
//				CPrintSheetView::DrawHeadPosition(pDC, markRect, m_nHeadPosition);
		}

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		framePen.DeleteObject();
	}
}


void CCropMark::DrawCropLinePreview(CDC* pDC, CRect outBBox)
{
	pDC->FillSolidRect(outBBox,  BLACK);
}



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// CFoldMark

IMPLEMENT_SERIAL(CFoldMark, CObject, VERSIONABLE_SCHEMA | 3)

CFoldMark::CFoldMark()
{ 
	m_nType				= FoldMark;
	m_fLineDistance		= 3.0f;
	m_fLineWidth		= 0.2f;
	m_fLineLength		= 3.0f;
	m_fCrossWidth		= 3.0f;
	m_fCrossHeight		= 3.0f;
	m_fCrosslineWidth	= 0.2f;
	m_fMinGap			= 2.0f;
	m_bSetMarksBetween	= FALSE;
	m_nZOrder			= Background;
	m_nLinetype			= Solid;
	m_nBkMode			= Transparent;
}

CFoldMark::CFoldMark(CString strName, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps, CPageSource& rMarkSource) 
					 : CMark(strName, 0.0f, 0.0f, TOP, bAllColors, 0, rColorSeps, rMarkSource)
{ 
	m_nType				=  FoldMark;
	m_strMarkName		= "FoldMark";
	m_fLineDistance		= 3.0f;
	m_fLineWidth		= 0.2f;
	m_fLineLength		= 3.0f;
	m_fCrossWidth		= 3.0f;
	m_fCrossHeight		= 3.0f;
	m_fCrosslineWidth	= 0.2f;
	m_fMinGap			= 2.0f;
	m_bSetMarksBetween	= FALSE;
	m_nZOrder			= Background;
	m_nLinetype			= Solid;
	m_nBkMode			= Transparent;
}

void CFoldMark::Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList)
{
	CString strNewMark;
	strNewMark.LoadString(IDS_NEW_FOLDMARK);
	if (pMarkList)
		strNewMark = pMarkList->FindFreeMarkName(strNewMark);

	m_strMarkName		 = strNewMark;
	m_MarkSource.m_nType = CPageSource::SystemCreated;
	m_MarkSource.m_strSystemCreationInfo.Format(_T("$FOLD_MARK %s %s"), "/Solid", "/Transparent");
	CPageSourceHeader markSourceHeader;
	markSourceHeader.m_bPageNumberFix = FALSE;
	markSourceHeader.m_nPageNumber	  = 1;
	markSourceHeader.m_strColorName	  = _T("Composite");
	markSourceHeader.m_rgb			  = WHITE;
	markSourceHeader.m_nColorIndex	  = 0;
	m_MarkSource.m_PageSourceHeaders.Add(markSourceHeader);
	InitializeTemplate(rColorDefTable);
}

// operator overload for CLayoutObject - needed by CList functions:
const CFoldMark& CFoldMark::operator=(const CFoldMark& rFoldMark)
{
	Copy(rFoldMark);	

	return *this;
}

BOOL CFoldMark::operator==(const CFoldMark& rFoldMark)
{
	if (CMark::operator!=(rFoldMark))							return FALSE;
	if (m_fLineDistance		!= rFoldMark.m_fLineDistance)		return FALSE;
	if (m_fLineWidth		!= rFoldMark.m_fLineWidth)			return FALSE;
	if (m_fLineLength		!= rFoldMark.m_fLineLength)			return FALSE;
	if (m_fCrossWidth		!= rFoldMark.m_fCrossWidth)			return FALSE;
	if (m_fCrossHeight		!= rFoldMark.m_fCrossHeight)		return FALSE;
	if (m_fCrosslineWidth	!= rFoldMark.m_fCrosslineWidth)		return FALSE;
	if (m_fMinGap			!= rFoldMark.m_fMinGap)				return FALSE;
	if (m_bSetMarksBetween	!= rFoldMark.m_bSetMarksBetween)	return FALSE;
	if (m_nZOrder			!= rFoldMark.m_nZOrder)				return FALSE;
	if (m_nLinetype			!= rFoldMark.m_nLinetype)			return FALSE;
	if (m_nBkMode			!= rFoldMark.m_nBkMode)				return FALSE;

	return TRUE;
}

BOOL CFoldMark::operator!=(const CFoldMark& rFoldMark)
{
	return !(*this == rFoldMark);
}

void CFoldMark::Copy(const CFoldMark& rFoldMark)
{
	CMark::Copy(rFoldMark);

	m_fLineDistance		= rFoldMark.m_fLineDistance;
	m_fLineWidth		= rFoldMark.m_fLineWidth;
	m_fLineLength		= rFoldMark.m_fLineLength;
	m_fCrossWidth		= rFoldMark.m_fCrossWidth;
	m_fCrossHeight		= rFoldMark.m_fCrossHeight;
	m_fCrosslineWidth	= rFoldMark.m_fCrosslineWidth;
	m_fMinGap			= rFoldMark.m_fMinGap;
	m_bSetMarksBetween	= rFoldMark.m_bSetMarksBetween;
	m_nZOrder			= rFoldMark.m_nZOrder;
	m_nLinetype			= rFoldMark.m_nLinetype;
	m_nBkMode			= rFoldMark.m_nBkMode;
}

void CFoldMark::Serialize(CArchive& ar)
{
	CMark::Serialize(ar);

	ar.SerializeClass(RUNTIME_CLASS(CFoldMark));

	if (ar.IsStoring())
	{
		ar << m_fLineDistance;
		ar << m_fLineWidth;
		ar << m_fLineLength;
		ar << m_fCrossWidth;
		ar << m_fCrossHeight;
		ar << m_fCrosslineWidth;
		ar << m_fMinGap;
		ar << m_bSetMarksBetween;
		ar << m_nZOrder;
		ar << m_nLinetype;
		ar << m_nBkMode;
	}											
	else																		 
	{
		UINT nVersion;

		nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> m_fLineDistance;
			ar >> m_fLineWidth;
			ar >> m_fLineLength;
			ar >> m_fCrossWidth;
			ar >> m_fCrossHeight;
			ar >> m_fCrosslineWidth;
			ar >> m_fMinGap;	m_fMinGap = 2.0f;	// in schema 1 m_fMinGap has not been used -> define as 2mm
			ar >> m_nZOrder;
			ar >> m_nLinetype;
			ar >> m_nBkMode;
			m_bSetMarksBetween = FALSE;
			break;
		case 2:
			ar >> m_fLineDistance;
			ar >> m_fLineWidth;
			ar >> m_fLineLength;
			ar >> m_fCrossWidth;
			ar >> m_fCrossHeight;
			ar >> m_fCrosslineWidth;
			ar >> m_fMinGap;
			ar >> m_nZOrder;
			ar >> m_nLinetype;
			ar >> m_nBkMode;
			m_bSetMarksBetween = FALSE;
			break;
		case 3:
			ar >> m_fLineDistance;
			ar >> m_fLineWidth;
			ar >> m_fLineLength;
			ar >> m_fCrossWidth;
			ar >> m_fCrossHeight;
			ar >> m_fCrosslineWidth;
			ar >> m_fMinGap;
			ar >> m_bSetMarksBetween;
			ar >> m_nZOrder;
			ar >> m_nLinetype;
			ar >> m_nBkMode;
			break;
		}

		if (m_nZOrderIndex == 10000)	// old type (CMark::schema < 4)
			m_nZOrderIndex = (m_nZOrder == CFoldMark::Foreground) ? 0 : INT_MAX;
	}
}


void CFoldMark::GetBBoxAll(CPrintSheet* pPrintSheet, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop)
{
	fBBoxLeft = fBBoxBottom = FLT_MAX; fBBoxRight = fBBoxTop = -FLT_MAX;

	if ( ! pPrintSheet)
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;

	CLayoutObjectList* pObjectList = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide.m_ObjectList : &pLayout->m_BackSide.m_ObjectList;

	float	 fLeft, fBottom, fRight, fTop;
	POSITION pos = pObjectList->GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pObjectList->GetNext(pos);
		if ( (rObject.m_nType == CLayoutObject::ControlMark) || rObject.IsFlatProduct() )
			continue;

		CFoldSheet* pFoldSheet = rObject.GetCurrentFoldSheet(pPrintSheet);
		if ( ! pFoldSheet)
			continue;
		BOOL bIsCoverFront = FALSE;
		if (pFoldSheet->IsCover())
		{
			if (pFoldSheet->GetBoundProduct().m_bindingParams.m_bindingDefs.m_nBinding == CBindingDefs::PerfectBound)
				continue;
			CPageTemplate* pPageTemplate = rObject.GetCurrentPageTemplate(pPrintSheet);
			if (pPageTemplate->IsFirstOrLastPage())
				bIsCoverFront = TRUE;
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		HORIZONTAL, fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}
	
		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		VERTICAL,   fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		HORIZONTAL, fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		VERTICAL,   fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	HORIZONTAL, fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	VERTICAL,   fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,	HORIZONTAL, fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,	VERTICAL,   fLeft, fBottom, fRight, fTop))
		{
			fBBoxLeft	= min(fBBoxLeft,	fLeft);	
			fBBoxBottom = min(fBBoxBottom,	fBottom);
			fBBoxRight	= max(fBBoxRight,   fRight);
			fBBoxTop	= max(fBBoxTop,		fTop);
		}
	}
}


int CFoldMark::AddToSheet(CPrintSheet* pPrintSheet, int nSide, CPageTemplate* pMarkTemplate, CMarkList* /*pMarkList*/)
{
	if ( ! pPrintSheet)
		return 0;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return 0;

	CLayoutObjectList* pObjectList = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide.m_ObjectList : &pLayout->m_BackSide.m_ObjectList;

	float fLeft, fBottom, fRight, fTop;

	short    nTemplateIndex = CImpManDoc::GetDoc()->m_MarkTemplateList.GetIndex(pMarkTemplate);
	int      nObjectsAdded	= 0;
	POSITION pos			= pObjectList->GetHeadPosition();
	while (pos)
	{
		int			   nObjectIndex = pObjectList->GetIndex(pos);
		CLayoutObject& rObject		= pObjectList->GetNext(pos);
		if ( (rObject.m_nType == CLayoutObject::ControlMark) || rObject.IsFlatProduct() )
			continue;

		CFoldSheet* pFoldSheet = rObject.GetCurrentFoldSheet(pPrintSheet);
		if ( ! pFoldSheet)
			continue;
		BOOL bIsCoverFront = FALSE;
		if (pFoldSheet->IsCover())
		{
			if (pFoldSheet->GetBoundProduct().m_bindingParams.m_bindingDefs.m_nBinding == CBindingDefs::PerfectBound)
				continue;
			CPageTemplate* pPageTemplate = rObject.GetCurrentPageTemplate(pPrintSheet);
			if (pPageTemplate->IsFirstOrLastPage())
				bIsCoverFront = TRUE;
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		HORIZONTAL, fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, LEFT_TOP, HORIZONTAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		VERTICAL,   fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, LEFT_TOP, VERTICAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		HORIZONTAL, fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, RIGHT_TOP, HORIZONTAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		VERTICAL,   fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, RIGHT_TOP, VERTICAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	HORIZONTAL, fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, RIGHT_BOTTOM, HORIZONTAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	VERTICAL,   fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, RIGHT_BOTTOM, VERTICAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,	HORIZONTAL, fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, LEFT_BOTTOM, HORIZONTAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,	VERTICAL,   fLeft, fBottom, fRight, fTop))
			if ( ! rObject.GetLayoutSide()->FindIdenticalMark(fLeft, fBottom, fRight, fTop, pMarkTemplate))
			{
				CLayoutObject layoutObject;
				layoutObject.CreateMark(fLeft, fBottom, fRight - fLeft, fTop - fBottom, m_strMarkName, TOP, nTemplateIndex);
				layoutObject.m_markBuddyLink = CMarkBuddyLink(nObjectIndex, LEFT_BOTTOM, VERTICAL);
				pObjectList->AddTail(layoutObject);
				nObjectsAdded++;
			}
	}

	return nObjectsAdded;
}

void* CFoldMark::GetSheetTrimBox(void* pParam1, void* pParam2, LPARAM lParam1, LPARAM lParam2, float& fLeft, float& fBottom, float& fRight, float& fTop)
{
	fLeft = fBottom = fRight = fTop = 0.0f;

	CLayoutObject* pObject = (CLayoutObject*)pParam1;
	if ( ! pObject)
		return NULL;
	BOOL bIsCoverFront = (pParam2) ? (BOOL)pParam2 : FALSE;

	for (int i = 0; i < pObject->m_markInstanceDefs.GetSize(); i++)
	{
		if ( (pObject->m_markInstanceDefs[i].m_nPosition == lParam1) && (pObject->m_markInstanceDefs[i].m_nOrientation == lParam2) && (pObject->m_markInstanceDefs[i].m_strParentMark == m_strMarkName) )
			if (pObject->m_markInstanceDefs[i].m_nAction == CMarkInstanceDefs::Disable)
				return NULL;
	}

	int	 nDirection		= 0;
	BOOL bInside		= FALSE;
	BOOL bIsCoverSpine	= FALSE;
	if (lParam2 == HORIZONTAL)
	{
		if ( (lParam1 == LEFT_TOP) || (lParam1 == RIGHT_TOP) )
		{
			if ( ! pObject->m_UpperNeighbours.GetSize())
				return NULL;
			else
				if ( ! m_bSetMarksBetween)
					if (pObject->m_UpperNeighbours[0]->m_nComponentRefIndex != pObject->m_nComponentRefIndex)	// no foldline between foldsheets
						return NULL;	
			if ((lParam1 == LEFT_TOP) && pObject->m_LeftNeighbours.GetSize()) 
			{
				if (pObject->m_LeftNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex)
					bInside = TRUE;
			}
			else
				if ((lParam1 == RIGHT_TOP) && pObject->m_RightNeighbours.GetSize())
				{
					if (pObject->m_RightNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex)
						bInside = TRUE;
				}

			if (bIsCoverFront && (pObject->GetSpineSide() == TOP) )
				bIsCoverSpine = TRUE;
		}
		else
		{
			if ( ! pObject->m_LowerNeighbours.GetSize())
				return NULL;
			else
				if (! m_bSetMarksBetween)
					if (pObject->m_LowerNeighbours[0]->m_nComponentRefIndex != pObject->m_nComponentRefIndex)	// no foldline between foldsheets
						return NULL;	
			if ((lParam1 == LEFT_BOTTOM) && pObject->m_LeftNeighbours.GetSize())
			{
				if (pObject->m_LeftNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex)
					bInside = TRUE;
			}
			else
				if ((lParam1 == RIGHT_BOTTOM) && pObject->m_RightNeighbours.GetSize())
				{
					if (pObject->m_RightNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex)
						bInside = TRUE;
				}

			if (bIsCoverFront && (pObject->GetSpineSide() == BOTTOM) )
				bIsCoverSpine = TRUE;
		}

		float fDistance  = m_fLineDistance;
		float fWidth	 = m_fLineLength;
		float fLineWidth = m_fLineWidth;
		if (bInside && ((lParam1 == LEFT_TOP) || (lParam1 == LEFT_BOTTOM)) )
		{
			fDistance	= pObject->m_fLeftBorder - m_fCrossWidth / 2.0f;
			fWidth		= m_fCrossWidth;
			fLineWidth	= m_fCrosslineWidth;
		}
		else
			if (bInside && ((lParam1 == RIGHT_TOP) || (lParam1 == RIGHT_BOTTOM)) )
			{
				fDistance	= pObject->m_fRightBorder - m_fCrossWidth / 2.0f;
				fWidth		= m_fCrossWidth;
				fLineWidth	= m_fCrosslineWidth;
			}

		if (bIsCoverSpine && (fDistance < 0.1f) ) // if line is going to reach into spine -> do correct
		{
			fWidth += fDistance;
			fDistance = 0.1f;
		}

		nDirection = HORIZONTAL;
		switch (lParam1)
		{
		case LEFT_TOP:		fRight  = pObject->GetLeft()			- fDistance;			fLeft   = fRight - fWidth;	
							fTop    = pObject->GetTopWithBorder()	+ fLineWidth / 2.0f;	fBottom = fTop	 - fLineWidth; 
							break;

		case RIGHT_TOP:		fLeft	= pObject->GetRight()			+ fDistance;			fRight  = fLeft  + fWidth;	
							fTop	= pObject->GetTopWithBorder()   + fLineWidth / 2.0f;	fBottom = fTop	 - fLineWidth; 
							break;

		case RIGHT_BOTTOM:	fLeft	= pObject->GetRight()			+ fDistance;			fRight  = fLeft  + fWidth;
							fTop   = pObject->GetBottomWithBorder() + fLineWidth / 2.0f;	fBottom = fTop	 - fLineWidth; 
							break;

		case LEFT_BOTTOM:	fRight  = pObject->GetLeft()			- fDistance;			fLeft   = fRight - fWidth;	
							fTop   = pObject->GetBottomWithBorder() + fLineWidth / 2.0f;	fBottom = fTop	 - fLineWidth; 
							break;
		}
	}
	else
	{
		if ( (lParam1 == LEFT_TOP) || (lParam1 == LEFT_BOTTOM) )
		{
			if ( ! pObject->m_LeftNeighbours.GetSize())
				return NULL;
			else
				if ( ! m_bSetMarksBetween)
					if (pObject->m_LeftNeighbours[0]->m_nComponentRefIndex != pObject->m_nComponentRefIndex)	// no foldline between foldsheets
						return NULL;	
			if ((lParam1 == LEFT_TOP) && pObject->m_UpperNeighbours.GetSize()) 
			{
				if (pObject->m_UpperNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex)
					bInside = TRUE;
			}
			else
				if ((lParam1 == LEFT_BOTTOM) && pObject->m_LowerNeighbours.GetSize())
				{
					if (pObject->m_LowerNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex)
						bInside = TRUE;
				}

			if (bIsCoverFront && (pObject->GetSpineSide() == LEFT) )
				bIsCoverSpine = TRUE;
		}
		else
		{
			if ( ! pObject->m_RightNeighbours.GetSize())
				return NULL;
			else
				if ( ! m_bSetMarksBetween)
					if (pObject->m_RightNeighbours[0]->m_nComponentRefIndex != pObject->m_nComponentRefIndex)	// no foldline between foldsheets
						return NULL;	
			if ((lParam1 == RIGHT_TOP) && pObject->m_UpperNeighbours.GetSize()) 
			{
				if (pObject->m_UpperNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex)
					bInside = TRUE;
			}
			else
				if ((lParam1 == RIGHT_BOTTOM) && pObject->m_LowerNeighbours.GetSize())
				{
					if (pObject->m_LowerNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex)
						bInside = TRUE;
				}

			if (bIsCoverFront && (pObject->GetSpineSide() == RIGHT) )
				bIsCoverSpine = TRUE;
		}

		float fDistance  = m_fLineDistance;
		float fHeight	 = m_fLineLength;
		float fLineWidth = m_fLineWidth;
		if (bInside && ((lParam1 == LEFT_TOP) || (lParam1 == RIGHT_TOP)) )
		{
			fDistance	= pObject->m_fUpperBorder - m_fCrossHeight / 2.0f;
			fHeight		= m_fCrossHeight;	
			fLineWidth	= m_fCrosslineWidth;
		}	
		else
			if (bInside && ((lParam1 == LEFT_BOTTOM) || (lParam1 == RIGHT_BOTTOM)) )
			{
				fDistance	= pObject->m_fLowerBorder - m_fCrossHeight / 2.0f;
				fHeight		= m_fCrossHeight;	
				fLineWidth	= m_fCrosslineWidth;
			}	

		if (bIsCoverSpine && (fDistance < 0.1f) ) // if line is going to reach into spine -> do correct
		{
			fHeight += fDistance;
			fDistance = 0.1f;
		}

		nDirection = VERTICAL;
		switch (lParam1)
		{
		case LEFT_TOP:		fLeft   = pObject->GetLeftWithBorder()   - fLineWidth / 2.0f;	fRight  = fLeft	  + fLineWidth;	
							fBottom = pObject->GetTop()				 + fDistance;			fTop	= fBottom + fHeight;	
							break;

		case RIGHT_TOP:		fLeft   = pObject->GetRightWithBorder()  - fLineWidth / 2.0f;	fRight  = fLeft	  + fLineWidth;	
							fBottom = pObject->GetTop()				 + fDistance;			fTop	= fBottom + fHeight;
							break;

		case RIGHT_BOTTOM:	fLeft   = pObject->GetRightWithBorder()  - fLineWidth / 2.0f;	fRight  = fLeft	  + fLineWidth;	
							fTop    = pObject->GetBottom()			 - fDistance;			fBottom = fTop	  - fHeight;	
							break;

		case LEFT_BOTTOM:	fLeft   = pObject->GetLeftWithBorder()   - fLineWidth / 2.0f;	fRight  = fLeft	  + fLineWidth;	
							fTop    = pObject->GetBottom()			 - fDistance;			fBottom = fTop	  - fHeight;
							break;
		}
	}

	CLayoutSide* pLayoutSide = pObject->GetLayoutSide();
	return (void*)pLayoutSide->ClipToObjects(fLeft, fBottom, fRight, fTop, m_fMinGap, nDirection, TRUE);//(m_nZOrderIndex == 0) ? TRUE : FALSE);
}

void CFoldMark::DrawSheetPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, CMarkList* pMarkList, BOOL bShowContent, int /*nEdge*/, BOOL bShowHighlighted)
{
	if ( ! pPrintSheet)
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;
	//if ( ! m_rules.GetRuleExpression(0).IsEmpty())
	//	if ( ! IsEnabled(pPrintSheet))
	//		return;

	CColorDefinitionTable& rColorDefTable = GetActColorDefTable(pMarkList);

	CLayoutObjectList* pObjectList = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide.m_ObjectList : &pLayout->m_BackSide.m_ObjectList;

	float fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;

	POSITION pos = pObjectList->GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pObjectList->GetNext(pos);
		if ( (rObject.m_nType == CLayoutObject::ControlMark) || rObject.IsFlatProduct() )
			continue;

		CFoldSheet* pFoldSheet = rObject.GetCurrentFoldSheet(pPrintSheet);
		if ( ! pFoldSheet)
			continue;
		BOOL bIsCoverFront = FALSE;
		if (pFoldSheet->IsCover())
		{
			if (pFoldSheet->GetBoundProduct().m_bindingParams.m_bindingDefs.m_nBinding == CBindingDefs::PerfectBound)
				continue;
			CPageTemplate* pPageTemplate = rObject.GetCurrentPageTemplate(pPrintSheet);
			if (pPageTemplate->IsFirstOrLastPage())
				bIsCoverFront = TRUE;
		}

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		HORIZONTAL,  fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawFoldMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_TOP,		VERTICAL,    fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawFoldMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		HORIZONTAL,  fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawFoldMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_TOP,		VERTICAL,	 fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawFoldMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	HORIZONTAL,  fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawFoldMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, RIGHT_BOTTOM,	VERTICAL,	 fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawFoldMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,	HORIZONTAL,  fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawFoldMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);

		if (GetSheetTrimBox((void*)&rObject, (void*)bIsCoverFront, LEFT_BOTTOM,	VERTICAL,	 fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop))
			DrawFoldMarkPreview(pDC, pPrintSheet, nSide, rColorDefTable, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop, bShowContent, bShowHighlighted);
	}
}


void CFoldMark::DrawFoldMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, CColorDefinitionTable& rColorDefTable, 
									float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop,
									BOOL bShowContent, BOOL bShowHighlighted)
{
	CRect markRect;
	markRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
	markRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
	markRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
	markRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   	if (pView)
	{
		int nSheetID = (nSide << 16) + pPrintSheet->m_nPrintSheetNumber;
		pView->m_DisplayList.RegisterItem(pDC, markRect, markRect, (void*)this, 
										  SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Mark), (void*)nSheetID, CDisplayItem::ForceRegister); 
	}

	if (pDC->RectVisible(markRect))
	{
		CPen	framePen(PS_SOLID, 1, (bShowHighlighted) ? MARKFRAMECOLOR_HIGHLIGHT : MARKCOLOR);	// Lightred
		CPen*	pOldPen	  = pDC->SelectObject(&framePen);
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

		CSize sizeOffset(1,1); pDC->DPtoLP(&sizeOffset);
		markRect.OffsetRect(sizeOffset.cx, 0);
			
		if (bShowHighlighted)
			HighlightMark(pDC, markRect, pPrintSheet);

		if (bShowContent)
		{
			if (GetSeparationInfo(pPrintSheet->GetTopmostPlate(nSide), rColorDefTable, TRUE).m_nColorDefinitionIndex != -1)	// section mark is defined for topmost plate color
			{
				BOOL bSolid		  = (m_MarkSource.m_strSystemCreationInfo.Find(_T("/Solid"))	   != -1) ? TRUE : FALSE;
				BOOL bTransparent = (m_MarkSource.m_strSystemCreationInfo.Find(_T("/Transparent")) != -1) ? TRUE : FALSE;
				DrawFoldLinePreview(pDC, bSolid, bTransparent, markRect);
			}
			CPrintSheetView::DrawRectangleExt(pDC, markRect);
		}
		else
		{
			if ( ! bShowHighlighted)
			{
				MINSIZE_MARK(markRect);
				CPrintSheetView::FillTransparent (pDC, markRect, (bShowHighlighted) ? MARKCOLOR_HIGHLIGHT : MARKCOLOR, TRUE, 150);
			}
			CPrintSheetView::DrawRectangleExt(pDC, markRect);
//				CPrintSheetView::DrawHeadPosition(pDC, markRect, m_nHeadPosition);
		}

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		framePen.DeleteObject();
	}
}


void CFoldMark::DrawFoldLinePreview(CDC* pDC, BOOL bSolid, BOOL bTransparent, CRect outBBox)
{
	long nLineWidth  = min(outBBox.right - outBBox.left, outBBox.top - outBBox.bottom);
	long nLineLength = max(outBBox.right - outBBox.left, outBBox.top - outBBox.bottom);
	int	 nDirection  = (outBBox.right - outBBox.left < outBBox.top - outBBox.bottom) ? VERTICAL : HORIZONTAL;

	long nLineStartX = (nDirection == HORIZONTAL) ? outBBox.CenterPoint().x - nLineLength / 2 : outBBox.CenterPoint().x;
	long nLineStartY = (nDirection == HORIZONTAL) ? outBBox.CenterPoint().y					  : outBBox.CenterPoint().y - nLineLength / 2;
	long nLineEndX	 = (nDirection == HORIZONTAL) ? outBBox.CenterPoint().x + nLineLength / 2 : outBBox.CenterPoint().x;
	long nLineEndY	 = (nDirection == HORIZONTAL) ? outBBox.CenterPoint().y					  : outBBox.CenterPoint().y + nLineLength / 2;

	CPen  inlinePen (PS_SOLID,	   nLineWidth, BLACK);
	CPen  outlinePen(PS_SOLID, 2 * nLineWidth, WHITE);
	CPen* pOldPen	  = pDC->SelectObject(&outlinePen);
	int	  nOldBkColor = pDC->SetBkColor(WHITE);

	if ( ! bTransparent)	// draw white background
	{
		pDC->MoveTo(nLineStartX, nLineStartY);
		pDC->LineTo(nLineEndX,	 nLineEndY);
	}

	if (bSolid)
	{
		pDC->SelectObject(&inlinePen);
		pDC->MoveTo(nLineStartX, nLineStartY);
		pDC->LineTo(nLineEndX,	 nLineEndY);
	}
	else
	{
		pDC->SelectObject(&inlinePen);
		int nXPos = nLineStartX;
		int nYPos = nLineStartY;
		if (nDirection == HORIZONTAL)
		{
			while (nXPos < nLineEndX)
			{
				pDC->MoveTo(nXPos, nYPos);
				nXPos += CPrintSheetView::WorldToLP(1.0f);
				while (nXPos > nLineEndX)
					nXPos = nLineEndX;
				pDC->LineTo(nXPos, nYPos);
				nXPos += CPrintSheetView::WorldToLP(1.0f);
			}
		}
		else
		{
			while (nYPos < nLineEndY)
			{
				pDC->MoveTo(nXPos, nYPos);
				nYPos += CPrintSheetView::WorldToLP(1.0f);
				while (nYPos > nLineEndY)
					nYPos = nLineEndY;
				pDC->LineTo(nXPos, nYPos);
				nYPos += CPrintSheetView::WorldToLP(1.0f);
			}
		}
	}

	pDC->SetBkColor(nOldBkColor);
	pDC->SelectObject(pOldPen);
	inlinePen.DeleteObject();
	outlinePen.DeleteObject();
}


//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// CBarcodeMark

IMPLEMENT_SERIAL(CBarcodeMark, CObject, VERSIONABLE_SCHEMA | 2)

CBarcodeMark::CBarcodeMark()
{ 
	m_nType							 =  BarcodeMark;
	m_strMarkName					 = "BarcodeMark";
	m_strOutputText					 = "BarcodeMark";
	m_nBarcodeType					 = eBC_Code128C;
}

CBarcodeMark::CBarcodeMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
					 CPageSource& rMarkSource, const CString& strText)
					: CReflineMark(strName, fWidth, fHeight, nHeadPosition, bAllColors, rColorSeps, rMarkSource)
{
	m_nType							 = BarcodeMark;
	m_strOutputText					 = strText;
	m_nBarcodeType					 = eBC_Code128C;
}

void CBarcodeMark::Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList)
{
	CString strNewMark;
	strNewMark.LoadString(IDS_NEW_BARCODEMARK);
	if (pMarkList)
		strNewMark = pMarkList->FindFreeMarkName(strNewMark);

	m_strMarkName		 = strNewMark;
	m_fMarkWidth		 = 100.0f; m_fMarkHeight = 5.0f;
	m_strOutputText		 = "";
	m_MarkSource.m_nType = CPageSource::SystemCreated;
	CString strBarcodeType = ( (m_nBarcodeType >= 0) && (m_nBarcodeType < BCGetBCCount()) ) ? BCGetBCList()[m_nBarcodeType] : _T("");
	m_MarkSource.m_strSystemCreationInfo.Format(_T("$BARCODE | %s | %s"), m_strOutputText, strBarcodeType);
	CPageSourceHeader markSourceHeader;
	markSourceHeader.m_bPageNumberFix = FALSE;
	markSourceHeader.m_nPageNumber	  = 1;
	markSourceHeader.m_strColorName	  = _T("Composite");
	markSourceHeader.m_rgb			  = WHITE;
	markSourceHeader.m_nColorIndex	  = 0;
	m_MarkSource.m_PageSourceHeaders.Add(markSourceHeader);
	InitializeTemplate(rColorDefTable);
}

// operator overload for CLayoutObject - needed by CList functions:
const CBarcodeMark& CBarcodeMark::operator=(const CBarcodeMark& rBarcodeMark)
{
	Copy(rBarcodeMark);	

	return *this;
}

BOOL CBarcodeMark::operator==(const CBarcodeMark& rBarcodeMark)
{
	if (CReflineMark::operator!=(rBarcodeMark))	return FALSE;
	if (m_strOutputText		  != rBarcodeMark.m_strOutputText) return FALSE;
	if (m_nBarcodeType		  != rBarcodeMark.m_nBarcodeType)  return FALSE;

	return TRUE;
}

BOOL CBarcodeMark::operator!=(const CBarcodeMark& rBarcodeMark)
{
	return !(*this == rBarcodeMark);
}

void CBarcodeMark::Copy(const CBarcodeMark& rBarcodeMark)
{
	CReflineMark::Copy(rBarcodeMark);
	m_strOutputText	   = rBarcodeMark.m_strOutputText;
	m_nBarcodeType	   = rBarcodeMark.m_nBarcodeType;
}

void CBarcodeMark::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CBarcodeMark));
	UINT nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		CReflineMark::Serialize(ar);
		ar << m_strOutputText;
		ar << m_nBarcodeType;
	}											
	else																		 
	{
		switch (nVersion)
		{
		case 1:
			CMark::Serialize(ar);
			m_reflinePosParamsAnchor.Serialize(ar);
			ar >> m_strOutputText;
			ar >> m_nBarcodeType;
			break;
		case 2:
			CReflineMark::Serialize(ar);
			ar >> m_strOutputText;
			ar >> m_nBarcodeType;
			break;
		}
	}
}

void CBarcodeMark::DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted)
{
	if ( (fabs(fTrimBoxLeft) == FLT_MAX) || (fabs(fTrimBoxBottom) == FLT_MAX) || (fabs(fTrimBoxRight) == FLT_MAX) || (fabs(fTrimBoxTop) == FLT_MAX) )
		return;	// mark cannot be placed

	CRect markRect;
	markRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
	markRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
	markRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
	markRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   	if (pView)
	{
		int nSheetID = (nSide << 16) + pPrintSheet->m_nPrintSheetNumber;
		pView->m_DisplayList.RegisterItem(pDC, markRect, markRect, (void*)this, 
										  SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Mark), (void*)nSheetID, CDisplayItem::ForceRegister); 
	}

	CColorDefinitionTable& rColorDefTable = GetActColorDefTable(pMarkList);

	switch (nEdge)
	{
	case 0:		 m_reflinePosParamsAnchor.DrawSheetReflines(pDC, pPrintSheet, nSide);			break;
	case LEFT: 	 
	case BOTTOM: m_reflinePosParamsLL.DrawSheetReflines	   (pDC, pPrintSheet, nSide, nEdge);	break;
	case RIGHT:  
	case TOP:	 m_reflinePosParamsUR.DrawSheetReflines	   (pDC, pPrintSheet, nSide, nEdge);	break;
	default:	 break;
	}

	if (pDC->RectVisible(markRect))
	{
		CPen	framePen(PS_SOLID, 1, (bShowHighlighted) ? MARKFRAMECOLOR_HIGHLIGHT : MARKCOLOR);	// Lightred
		CPen*	pOldPen	  = pDC->SelectObject(&framePen);
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

		CSize sizeOffset(1,1); pDC->DPtoLP(&sizeOffset);
		markRect.OffsetRect(sizeOffset.cx, 0);
			
		if (bShowHighlighted)
			HighlightMark(pDC, markRect, pPrintSheet);

		if (bShowContent)
		{
			if (GetSeparationInfo(pPrintSheet->GetTopmostPlate(nSide), rColorDefTable, TRUE).m_nColorDefinitionIndex != -1)		// barcode mark is defined for topmost plate color
			{
				CPlate*	pPlate = pPrintSheet->GetTopmostPlate(nSide);
				CString strBarcodeType	 =			   GetStringToken(m_MarkSource.m_strSystemCreationInfo, 2, _T("|"));
				CString strTextOption	 =			   GetStringToken(m_MarkSource.m_strSystemCreationInfo, 3, _T("|"));
				float   fTextSize		 = Ascii2Float(GetStringToken(m_MarkSource.m_strSystemCreationInfo, 4, _T("|")));
				if (pPlate)
				{
					if ( (pPlate->m_strPlateName.CompareNoCase(_T("Composite")) == 0) && (m_strOutputText.Find(_T("$2")) != -1) )	// special handling for color name output in case of composite
					{
						BOOL					   bColorSpaceAll;
						float					   fC = 0.0f, fM = 0.0f, fY = 0.0f, fK = 0.0f;
						CStringArray			   spotColorNames;
						CArray<COLORREF, COLORREF> spotColorRGBs;
						CArray<float,	 float>	   spotColorTints;
						CPDFOutput::BuildCompositeColorInfo(&m_markTemplate, &m_MarkSource, pPlate, bColorSpaceAll, fC, fM, fY, fK, spotColorNames, spotColorRGBs, spotColorTints);
						DrawBarcodePreview(pDC, m_strOutputText, strBarcodeType, strTextOption, fTextSize, markRect, m_nHeadPosition, pPrintSheet, pPlate, fC, fM, fY, fK, spotColorNames, spotColorRGBs, &m_reflinePosParamsAnchor);
					}
					else
						DrawBarcodePreview(pDC, m_strOutputText, strBarcodeType, strTextOption, fTextSize, markRect, m_nHeadPosition, pPrintSheet, pPlate, _T(""), &m_reflinePosParamsAnchor);
				}
			}

			CPrintSheetView::DrawRectangleExt(pDC, markRect);
		}
		else
		{
			if ( ! bShowHighlighted)
			{
				MINSIZE_MARK(markRect);
				CPrintSheetView::FillTransparent (pDC, markRect, (bShowHighlighted) ? MARKCOLOR_HIGHLIGHT : MARKCOLOR, TRUE, 150);
			}
			CPrintSheetView::DrawRectangleExt(pDC, markRect);
//			CPrintSheetView::DrawHeadPosition(pDC, markRect, m_nHeadPosition);
		}
		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		framePen.DeleteObject();
	}
}

void CBarcodeMark::DrawBarcodePreview(CDC* pDC, const CString& strOutputText, CString strBarcodeType, CString strTextOption, float fTextSize, const CRect& rOutBBox, int nHeadPosition, CPrintSheet* pPrintSheet, CPlate* pPlate, 
									  float fC, float fM, float fY, float fK, CStringArray& spotColorNames, CArray<COLORREF, COLORREF>& spotColorRGBs, CReflinePositionParams* pReflinePosParams)
{
	if ( ! pPrintSheet)
		return;

	BOOL  bStacked = (strOutputText.Find(_T("$2_=")) != -1) ? TRUE : FALSE;
	CRect textRect = rOutBBox;

	TCHAR szDest[1024];
	CSize barcodeExtent;
	int	  nXOffset = 0;
	int	  nYOffset = 0;
	CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strOutputText, pPrintSheet, pPlate, -1, theApp.m_colorDefTable.GetCyanName(), pReflinePosParams);
	if (fC == 1.0f)
	{
		barcodeExtent = DrawBarcodePreview(pDC, strOutputText, strBarcodeType, strTextOption, fTextSize, textRect, nHeadPosition, pPrintSheet, pPlate, theApp.m_colorDefTable.GetCyanName(),	pReflinePosParams, CYAN);
		nXOffset = (nHeadPosition == TOP)  ? barcodeExtent.cx : ( (nHeadPosition == BOTTOM) ? -barcodeExtent.cx : 0 );
		nYOffset = (nHeadPosition == LEFT) ? barcodeExtent.cy : ( (nHeadPosition == RIGHT)  ? -barcodeExtent.cy : 0 );
		if ( ! bStacked)
			textRect.OffsetRect(CSize(nXOffset, nYOffset));
	}
	if (fM == 1.0f)
	{
		barcodeExtent = DrawBarcodePreview(pDC, strOutputText, strBarcodeType, strTextOption, fTextSize, textRect, nHeadPosition, pPrintSheet, pPlate, theApp.m_colorDefTable.GetMagentaName(),	pReflinePosParams, MAGENTA);
		nXOffset = (nHeadPosition == TOP)  ? barcodeExtent.cx : ( (nHeadPosition == BOTTOM) ? -barcodeExtent.cx : 0 );
		nYOffset = (nHeadPosition == LEFT) ? barcodeExtent.cy : ( (nHeadPosition == RIGHT)  ? -barcodeExtent.cy : 0 );
		if ( ! bStacked)
			textRect.OffsetRect(CSize(nXOffset, nYOffset));
	}
	if (fY == 1.0f)
	{
		barcodeExtent = DrawBarcodePreview(pDC, strOutputText, strBarcodeType, strTextOption, fTextSize, textRect, nHeadPosition, pPrintSheet, pPlate, theApp.m_colorDefTable.GetYellowName(),	pReflinePosParams, YELLOW);
		nXOffset = (nHeadPosition == TOP)  ? barcodeExtent.cx : ( (nHeadPosition == BOTTOM) ? -barcodeExtent.cx : 0 );
		nYOffset = (nHeadPosition == LEFT) ? barcodeExtent.cy : ( (nHeadPosition == RIGHT)  ? -barcodeExtent.cy : 0 );
		if ( ! bStacked)
			textRect.OffsetRect(CSize(nXOffset, nYOffset));
	}
	if (fK == 1.0f)
	{
		barcodeExtent = DrawBarcodePreview(pDC, strOutputText, strBarcodeType, strTextOption, fTextSize, textRect, nHeadPosition, pPrintSheet, pPlate, theApp.m_colorDefTable.GetKeyName(),		pReflinePosParams, BLACK);
		nXOffset = (nHeadPosition == TOP)  ? barcodeExtent.cx : ( (nHeadPosition == BOTTOM) ? -barcodeExtent.cx : 0 );
		nYOffset = (nHeadPosition == LEFT) ? barcodeExtent.cy : ( (nHeadPosition == RIGHT)  ? -barcodeExtent.cy : 0 );
		if ( ! bStacked)
			textRect.OffsetRect(CSize(nXOffset, nYOffset));
	}

	for (int i = 0; i < spotColorNames.GetSize(); i++)
	{
		DrawBarcodePreview(pDC, strOutputText, strBarcodeType, strTextOption, fTextSize, textRect, nHeadPosition, pPrintSheet, pPlate, spotColorNames[i], pReflinePosParams, spotColorRGBs[i]);
	}
}

CSize CBarcodeMark::DrawBarcodePreview(CDC* pDC, const CString& strOutputText, CString strBarcodeType, CString strTextOption, float fTextSize, const CRect& rOutBBox, int nHeadPosition, CPrintSheet* pPrintSheet, CPlate* pPlate, CString strColorName, CReflinePositionParams* pReflinePosParams, COLORREF crBarcodeColor)
{
	if ( ! pPrintSheet)
		return CSize(0, 0);

	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strOutputText, pPrintSheet, pPlate, -1, strColorName, pReflinePosParams);

	t_BarCode* pBarCode;
	ERRCODE eCode = BCAlloc(&pBarCode);
	if (eCode != S_OK)
		return CSize(0, 0);
	
	if (theApp.m_nAddOnsLicensed & CImpManApp::Barcode)
		BCLicenseMe(_T("Mem: Tegeler Software-Engineering"), eLicKindDeveloper, 1, _T("9859A097187FB2F2AD47FC15C3ACBFFA"), eLicProd2D);

	int nBarcodeType = GetBarcodeTypeFromString(strBarcodeType);

	BCSetColorBC(pBarCode, crBarcodeColor);

	GenerateBarcode(pBarCode, szDest, nBarcodeType, nHeadPosition);

	CRect rcBarcode = rOutBBox;
	CRect rcText	= rcBarcode;

	int nTextOption = MapTextOption(strTextOption, nHeadPosition);
	switch (nTextOption)
	{
	case TOP:		rcText.bottom	 = rcBarcode.top	- CPrintSheetView::WorldToLP(fTextSize);
					rcBarcode.top	 = rcText.bottom;
					break;
	case BOTTOM:	rcText.top		 = rcBarcode.bottom + CPrintSheetView::WorldToLP(fTextSize);
					rcBarcode.bottom = rcText.top;
					break;
	case LEFT:		rcText.right	 = rcBarcode.left	+ CPrintSheetView::WorldToLP(fTextSize);
					rcBarcode.left	 = rcText.right;
					break;
	case RIGHT:		rcText.left      = rcBarcode.right	- CPrintSheetView::WorldToLP(fTextSize);
					rcBarcode.right  = rcText.left;
					break;
	}

	int nDC = pDC->SaveDC();
	pDC->LPtoDP(&rcBarcode);
	pDC->SetMapMode(MM_TEXT);
	pDC->SetWindowOrg(0,0);
	pDC->SetViewportOrg(0,0);

	BCDraw(pBarCode, pDC->GetSafeHdc(), &rcBarcode);  

	pDC->RestoreDC(nDC);

	CString strText = szDest;
	if (strOutputText.Find(_T("$P")) >= 0)	// includes AsirCode -> show decoded text
	{
		long nCode = _ttoi(szDest);
		strText.Format(_T("%02d"), nCode%100);	// last 2 digits contains sheet number
	}
	if (nTextOption != 0)
	{
		if (MapTextOption(strTextOption) == RIGHT)	// text originally right (w/o head position) -> one char space between text and barcode
			strText.Insert(0, ' ');
		pDC->FillSolidRect(rcText, WHITE);
		CTextMark::DrawTextfieldPreview(pDC, strText, rcText, nHeadPosition, NULL, FALSE, crBarcodeColor); 
	}

	if (pBarCode)
		BCFree(pBarCode);

	return CSize(rcBarcode.Width(), abs(rcBarcode.Height()));
}

int CBarcodeMark::GetBarcodeTypeFromString(CString strBarcodeType)
{
	int nBarcodeType = eBC_None;
	for (int i = 0; i < BCGetBCCount(); i++)
	{
		if (BCGetBCList()[i] == strBarcodeType)
			return i;
	}

	//// search for old V8 names
	for (int i = 0; barcodeTypeNamesV8[i] != _T(""); i++)
	{
		if (strBarcodeType.CompareNoCase(barcodeTypeNamesV8[i]) == 0)
			return i;
	}

	return 0;
}

int CBarcodeMark::MapTextOption(const CString& strTextOption, int nHeadPosition)
{
	int nSide = 0;
	if (strTextOption == _T("TEXT_TOP"))
		nSide = TOP;
	else
	if (strTextOption == _T("TEXT_BOTTOM"))
		nSide = BOTTOM;
	else
	if (strTextOption == _T("TEXT_LEFT"))
		nSide = LEFT;
	else
	if (strTextOption == _T("TEXT_RIGHT"))
		nSide = RIGHT;

	switch (nHeadPosition)
	{
	case TOP:		break;
	case BOTTOM:	switch (nSide)
					{
					case TOP:		nSide = BOTTOM;	break;
					case BOTTOM:	nSide = TOP;	break;
					case LEFT:		nSide = RIGHT;	break;
					case RIGHT:		nSide = LEFT;	break;
					}
					break;
	case LEFT:		switch (nSide)
					{
					case TOP:		nSide = LEFT;	break;
					case BOTTOM:	nSide = RIGHT;	break;
					case LEFT:		nSide = BOTTOM;	break;
					case RIGHT:		nSide = TOP;	break;
					}
					break;
	case RIGHT:		switch (nSide)
					{
					case TOP:		nSide = RIGHT;	break;
					case BOTTOM:	nSide = LEFT;	break;
					case LEFT:		nSide = TOP;	break;
					case RIGHT:		nSide = BOTTOM;	break;
					}
					break;
	}

	return nSide;
}

ERRCODE CBarcodeMark::GenerateBarcode(t_BarCode* pBarCode, const CString strOutputText, int nBarcodeType, int nHeadPosition)
{
	ERRCODE eCode = S_OK;

	BCSetText(pBarCode, strOutputText, strOutputText.GetLength());

	BCSetBCType(pBarCode, (nBarcodeType < 0) ? eBC_None : static_cast<e_BarCType>(nBarcodeType));

	//BCSetCDMethod(pBarCode, eCDStandard);

	//// get the current selected check digit method
	//nIndexCD = m_cbCD.GetCurSel();
	//BCSetCDMethod(m_pbarCode, (nIndexCD < 0) ? eCDStandard : static_cast<e_CDMethod>(nIndexCD));

	//// only used with EAN/UCC Composite Symbologies
	//BCSet2DCompositeComponent(m_pbarCode, static_cast<e_CCType>(m_cbCCType.GetCurSel()));

	//BCSetModWidth(m_pbarCode, m_strModul);      // set the module width (unit is 1/1000 mm); "200" = 0.2 mms
	//BCSetFormat(m_pbarCode, m_strFormat);       // format string (optional)
	//BCSetRatio(m_pbarCode, m_strRatio);         // print ratio   (optional)

	//BCSetBearerBarWidth(m_pbarCode, m_nGuardWidth);         // guard bar width (optional)
	//BCSetBearerBarType(m_pbarCode, eBearerBar_TopAndBottom);

	BCSetPrintText(pBarCode, FALSE, FALSE); // print text yes/no
	//BCSetMirror(m_pbarCode, FALSE);                 // mirror mode yes/no

	switch (nHeadPosition)                 // orientation of the symbol
	{
	case TOP:		BCSetRotation(pBarCode, deg0);   	break;
	case LEFT:		BCSetRotation(pBarCode, deg90);  	break;
	case BOTTOM:	BCSetRotation(pBarCode, deg180); 	break;
	case RIGHT:		BCSetRotation(pBarCode, deg270); 	break;
	default:		BCSetRotation(pBarCode, deg0);  	break;
	}

	eCode = (eCode == S_OK) ? BCCheck(pBarCode)  : eCode;   // check bar code data (optional)
	eCode = (eCode == S_OK) ? BCCreate(pBarCode) : eCode;   // create internal bar code structure (MUST)

	return eCode;
}


ERRCODE CALLBACK CBarcodeMark::CustomDrawBarcode(VOID* customData, HDC drawDC, DOUBLE x, DOUBLE y, DOUBLE width, DOUBLE height)
{
	CDC* pDC = CDC::FromHandle(drawDC);
  
	if (pDC)
		pDC->FillSolidRect((int)x,(int)y,(int)width,(int)height,RGB(255,0,0));
	else
		if (customData)
		{
			DOUBLE fX = x + ((BC_CUSTOMDRAWDATA*)customData)->ptTargetCoords.x / 1000.0f;
			DOUBLE fY =		((BC_CUSTOMDRAWDATA*)customData)->ptTargetCoords.y / 1000.0f - y;	// coord origin is upper left
			DOUBLE fWidth  = width;
			DOUBLE fHeight = height;
			x = fX / fPSPoint;
			y = fY / fPSPoint;
			width  = fWidth  / fPSPoint;
			height = fHeight / fPSPoint;

			CString strPDFCode;
			strPDFCode.Format(_T(" %f %f %f %f re f"), x, y, width, -height);		// -height -> coord origin is upper left

			CString& rstrPDFStream = ((BC_CUSTOMDRAWDATA*)customData)->strPDFBuffer;
			rstrPDFStream += strPDFCode;
		}

	return 0;
}



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// CDrawingMark

IMPLEMENT_SERIAL(CDrawingMark, CObject, VERSIONABLE_SCHEMA | 1)

CDrawingMark::CDrawingMark()
{ 
	m_nType		  =  DrawingMark;
	m_strMarkName = "DrawingMark";
}

CDrawingMark::CDrawingMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
					 CPageSource& rMarkSource, const CString& strText)
					: CReflineMark(strName, fWidth, fHeight, nHeadPosition, bAllColors, rColorSeps, rMarkSource)
{
	m_nType	= DrawingMark;
}

void CDrawingMark::Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList)
{
	CString strNewMark;
	strNewMark.LoadString(IDS_NEW_DRAWINGMARK);
	if (pMarkList)
		strNewMark = pMarkList->FindFreeMarkName(strNewMark);

	m_strMarkName		 = strNewMark;
	m_fMarkWidth		 = 100.0f; m_fMarkHeight = 5.0f;
	m_MarkSource.m_nType = CPageSource::SystemCreated;
	m_MarkSource.m_strSystemCreationInfo = _T("$RECTANGLE | SOLID");
	CPageSourceHeader markSourceHeader;
	markSourceHeader.m_bPageNumberFix = FALSE;
	markSourceHeader.m_nPageNumber	  = 1;
	markSourceHeader.m_strColorName	  = _T("Composite");
	markSourceHeader.m_rgb			  = WHITE;
	markSourceHeader.m_nColorIndex	  = 0;
	m_MarkSource.m_PageSourceHeaders.Add(markSourceHeader);
	InitializeTemplate(rColorDefTable);
}

// operator overload for CLayoutObject - needed by CList functions:
const CDrawingMark& CDrawingMark::operator=(const CDrawingMark& rDrawingMark)
{
	Copy(rDrawingMark);	

	return *this;
}

BOOL CDrawingMark::operator==(const CDrawingMark& rDrawingMark)
{
	if (CReflineMark::operator!=(rDrawingMark))	return FALSE;

	return TRUE;
}

BOOL CDrawingMark::operator!=(const CDrawingMark& rDrawingMark)
{
	return !(*this == rDrawingMark);
}

void CDrawingMark::Copy(const CDrawingMark& rDrawingMark)
{
	CReflineMark::Copy(rDrawingMark);
}

void CDrawingMark::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CDrawingMark));
	UINT nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		CReflineMark::Serialize(ar);
	}											
	else																		 
	{
		switch (nVersion)
		{
		case 1:	CReflineMark::Serialize(ar);
				break;
		}
	}
}

void CDrawingMark::DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted)
{
	if ( (fabs(fTrimBoxLeft) == FLT_MAX) || (fabs(fTrimBoxBottom) == FLT_MAX) || (fabs(fTrimBoxRight) == FLT_MAX) || (fabs(fTrimBoxTop) == FLT_MAX) )
		return;	// mark cannot be placed

	CRect markRect;
	markRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
	markRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
	markRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
	markRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   	if (pView)
	{
		int nSheetID = (nSide << 16) + pPrintSheet->m_nPrintSheetNumber;
		pView->m_DisplayList.RegisterItem(pDC, markRect, markRect, (void*)this, 
										  SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Mark), (void*)nSheetID, CDisplayItem::ForceRegister); 
	}

	CColorDefinitionTable& rColorDefTable = GetActColorDefTable(pMarkList);

	switch (nEdge)
	{
	case 0:		 m_reflinePosParamsAnchor.DrawSheetReflines(pDC, pPrintSheet, nSide);			break;
	case LEFT: 	 
	case BOTTOM: m_reflinePosParamsLL.DrawSheetReflines	   (pDC, pPrintSheet, nSide, nEdge);	break;
	case RIGHT:  
	case TOP:	 m_reflinePosParamsUR.DrawSheetReflines	   (pDC, pPrintSheet, nSide, nEdge);	break;
	default:	 break;
	}

	if (pDC->RectVisible(markRect))
	{
		CPen	framePen(PS_SOLID, 1, (bShowHighlighted) ? MARKFRAMECOLOR_HIGHLIGHT : MARKCOLOR);	// Lightred
		CPen*	pOldPen	  = pDC->SelectObject(&framePen);
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

		CSize sizeOffset(1,1); pDC->DPtoLP(&sizeOffset);
		markRect.OffsetRect(sizeOffset.cx, 0);
			
		if (bShowHighlighted)
			HighlightMark(pDC, markRect, pPrintSheet);

		if (bShowContent)
		{
			if (GetSeparationInfo(pPrintSheet->GetTopmostPlate(nSide), rColorDefTable, TRUE).m_nColorDefinitionIndex != -1)		// drawing mark is defined for topmost plate color
			{
				BOOL					   bColorSpaceAll;
				float					   fC = 0.0f, fM = 0.0f, fY = 0.0f, fK = 0.0f;
				CStringArray			   spotColorNames;
				CArray<COLORREF, COLORREF> spotColorCMYKs;
				CArray<float,	 float>	   spotColorTints;
				CPlate*	pPlate = (pPrintSheet) ? pPrintSheet->GetTopmostPlate(nSide) : NULL;
				BOOL bComposite = ( (pPlate->m_strPlateName.CompareNoCase(_T("Composite")) == 0) || pPlate->m_strPlateName.IsEmpty() ) ? TRUE : FALSE;	// by definition: if no pages assigned (default plate) assume composite
				if (bComposite)
					CPDFOutput::BuildCompositeColorInfo(&m_markTemplate, &m_MarkSource, pPlate, bColorSpaceAll, fC, fM, fY, fK, spotColorNames, spotColorCMYKs, spotColorTints);
				else	
					fK = 1.0f;	// in case of separation -> output in black (gray) / colorspace all is not relevant here, because this has already been considered in ReorganizePrintSheetPlates()
				if ( INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) )
					DrawRectanglePreview(pDC, markRect, CColorDefinition::CMYK2RGB(CMYK((int)(fC*100.0f), (int)(fM*100.0f), (int)(fY*100.0f), (int)(fK*100.0f))));
				for (int i = 0; i < spotColorNames.GetSize(); i++)
				{
					COLORREF crSpot = CColorDefinition::CMYK2RGB(spotColorCMYKs.ElementAt(i));
					crSpot = CGraphicComponent::IncreaseColorBrightness(crSpot, 1.0f + (1.0f - spotColorTints[i])*0.6f);
					DrawRectanglePreview(pDC, markRect, crSpot);
				}
			}

			CPrintSheetView::DrawRectangleExt(pDC, markRect);
		}
		else
		{
			if ( ! bShowHighlighted)
			{
				MINSIZE_MARK(markRect);
				CPrintSheetView::FillTransparent (pDC, markRect, (bShowHighlighted) ? MARKCOLOR_HIGHLIGHT : MARKCOLOR, TRUE, 150);
			}
			CPrintSheetView::DrawRectangleExt(pDC, markRect);
//			CPrintSheetView::DrawHeadPosition(pDC, markRect, m_nHeadPosition);
		}
		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);
		framePen.DeleteObject();
	}
}

void CDrawingMark::DrawRectanglePreview(CDC* pDC, CRect rcRectangle, COLORREF crColor)
{
	if (pDC->RectVisible(rcRectangle))
	{
		pDC->FillSolidRect(rcRectangle, crColor);
	}
}



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
// CReflinePositionParams

IMPLEMENT_SERIAL(CReflinePositionParams, CObject, VERSIONABLE_SCHEMA | 2)

CReflinePositionParams::CReflinePositionParams()
{ 
	m_nAnchor				  = REFLINE_ANCHOR;
	m_nAnchorReference		  = -1;
	m_nReferenceItemInX		  = PAPER_REFLINE;
	m_nReferenceItemInY		  = PAPER_REFLINE;
	m_nReferenceLineInX		  = XCENTER;
	m_nReferenceLineInY		  = YCENTER;
	m_nObjectReferenceLineInX = XCENTER;
	m_nObjectReferenceLineInY = YCENTER;
	m_fDistanceX			  = 0.0f;
	m_fDistanceY			  = 0.0f;
	m_nPlaceOn				  = PlaceOnBothSamePos;
	m_pParentMark			  = NULL;
}

CReflinePositionParams::CReflinePositionParams(CReflinePositionParams& rReflinePositionParams)
{
	Copy(rReflinePositionParams);	
}

const CReflinePositionParams& CReflinePositionParams::operator=(const CReflinePositionParams& rReflinePositionParams)
{
	Copy(rReflinePositionParams);	

	return *this;
}

BOOL CReflinePositionParams::operator==(const CReflinePositionParams& rReflinePositionParams)
{
	if (m_nAnchor					!= rReflinePositionParams.m_nAnchor)				 return FALSE;
	if (m_nAnchorReference			!= rReflinePositionParams.m_nAnchorReference)		 return FALSE;
	if (m_nReferenceItemInX			!= rReflinePositionParams.m_nReferenceItemInX)		 return FALSE;
	if (m_nReferenceItemInY			!= rReflinePositionParams.m_nReferenceItemInY)		 return FALSE;
	if (m_nReferenceLineInX			!= rReflinePositionParams.m_nReferenceLineInX)		 return FALSE;
	if (m_nReferenceLineInY			!= rReflinePositionParams.m_nReferenceLineInY)		 return FALSE;
	if (m_hReferenceObjectInX		!= rReflinePositionParams.m_hReferenceObjectInX)	 return FALSE;
	if (m_hReferenceObjectInY		!= rReflinePositionParams.m_hReferenceObjectInY)	 return FALSE;
	if (m_nObjectReferenceLineInX	!= rReflinePositionParams.m_nObjectReferenceLineInX) return FALSE;
	if (m_nObjectReferenceLineInY	!= rReflinePositionParams.m_nObjectReferenceLineInY) return FALSE;
	if (m_fDistanceX				!= rReflinePositionParams.m_fDistanceX)				 return FALSE;
	if (m_fDistanceY				!= rReflinePositionParams.m_fDistanceY)				 return FALSE;
	if (m_nPlaceOn					!= rReflinePositionParams.m_nPlaceOn)				 return FALSE;
	return TRUE;
}

BOOL CReflinePositionParams::operator!=(const CReflinePositionParams& rReflinePositionParams)
{
	return !(*this == rReflinePositionParams);
}

void CReflinePositionParams::Copy(const CReflinePositionParams& rReflinePositionParams)
{
	m_nAnchor					= rReflinePositionParams.m_nAnchor;
	m_nAnchorReference			= rReflinePositionParams.m_nAnchorReference;
	m_nReferenceItemInX			= rReflinePositionParams.m_nReferenceItemInX;
	m_nReferenceItemInY			= rReflinePositionParams.m_nReferenceItemInY;
	m_nReferenceLineInX			= rReflinePositionParams.m_nReferenceLineInX;
	m_nReferenceLineInY			= rReflinePositionParams.m_nReferenceLineInY;
	m_hReferenceObjectInX		= rReflinePositionParams.m_hReferenceObjectInX;
	m_hReferenceObjectInY		= rReflinePositionParams.m_hReferenceObjectInY;
	m_nObjectReferenceLineInX	= rReflinePositionParams.m_nObjectReferenceLineInX;
	m_nObjectReferenceLineInY	= rReflinePositionParams.m_nObjectReferenceLineInY;
	m_fDistanceX				= rReflinePositionParams.m_fDistanceX;
	m_fDistanceY				= rReflinePositionParams.m_fDistanceY;
	m_nPlaceOn					= rReflinePositionParams.m_nPlaceOn;
}

// class CReflinePositionParams formerly known as CMarkReflinePositionParams
// use old class name for SerializeClass() to keep compatibility to old archives
class CMarkReflinePositionParams : public CReflinePositionParams
{
public:
	DECLARE_SERIAL( CMarkReflinePositionParams )
};
IMPLEMENT_SERIAL(CMarkReflinePositionParams, CObject, VERSIONABLE_SCHEMA | 2)


void CReflinePositionParams::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CMarkReflinePositionParams));

	if (ar.IsStoring())
	{
		ar << m_nAnchor;
		ar << m_nAnchorReference;
		ar << m_nReferenceItemInX;
		ar << m_nReferenceItemInY;
		ar << m_nReferenceLineInX;		
		ar << m_nReferenceLineInY;
		ar << m_hReferenceObjectInX;
		ar << m_hReferenceObjectInY;
		ar << m_nObjectReferenceLineInX;		
		ar << m_nObjectReferenceLineInY;
		ar << m_fDistanceX;			
		ar << m_fDistanceY;			
		ar << m_nPlaceOn;
	}											
	else																		 
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> m_nReferenceItemInX;
			ar >> m_nReferenceItemInY;
			ar >> m_nReferenceLineInX;		
			ar >> m_nReferenceLineInY;
			ar >> m_hReferenceObjectInX;
			ar >> m_hReferenceObjectInY;
			ar >> m_nObjectReferenceLineInX;		
			ar >> m_nObjectReferenceLineInY;
			ar >> m_fDistanceX;			
			ar >> m_fDistanceY;			
			ar >> m_nPlaceOn;
			m_nAnchor = REFLINE_ANCHOR;
			break;
		case 2:
			ar >> m_nAnchor;
			ar >> m_nAnchorReference;
			ar >> m_nReferenceItemInX;
			ar >> m_nReferenceItemInY;
			ar >> m_nReferenceLineInX;		
			ar >> m_nReferenceLineInY;
			ar >> m_hReferenceObjectInX;
			ar >> m_hReferenceObjectInY;
			ar >> m_nObjectReferenceLineInX;		
			ar >> m_nObjectReferenceLineInY;
			ar >> m_fDistanceX;			
			ar >> m_fDistanceY;			
			ar >> m_nPlaceOn;
			break;
		}
	}
}


float CReflinePositionParams::GetParentWidth(float fDefaultWidth)
{
	return ( (m_pParentMark) ? m_pParentMark->m_fMarkWidth : fDefaultWidth);
}

float CReflinePositionParams::GetParentHeight(float fDefaultHeight)
{
	return ( (m_pParentMark) ? m_pParentMark->m_fMarkHeight : fDefaultHeight);
}

BOOL CReflinePositionParams::ParentWidthVariable()
{
	return ( (m_pParentMark) ? ((CReflineMark*)m_pParentMark)->m_bVariableWidth : FALSE);
}

BOOL CReflinePositionParams::ParentHeightVariable()
{
	return ( (m_pParentMark) ? ((CReflineMark*)m_pParentMark)->m_bVariableHeight : FALSE);
}

int CReflinePositionParams::ParentGetReflinePosParamsID()
{
	CReflineMark* pMark = (CReflineMark* )m_pParentMark;
	if (&pMark->m_reflinePosParamsAnchor	== this )	return 0; 
	if (&pMark->m_reflinePosParamsLL		== this )	return 1; 
	if (&pMark->m_reflinePosParamsUR		== this )	return 2; 
	return 0;
}

void CReflinePositionParams::GetSheetPosition(CPrintSheet* pPrintSheet, int nSide, float& rfLeft, float& rfBottom, float fDefaultWidth, float fDefaultHeight, BOOL bDoTransform)
{
	rfLeft = rfBottom = 0.0f;

	if ( ! pPrintSheet)
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;

	int nReflinePosParamsID		= ParentGetReflinePosParamsID();
	int nObjectReferenceLineInX = m_nObjectReferenceLineInX;
	int nObjectReferenceLineInY = m_nObjectReferenceLineInY;
	if (nReflinePosParamsID > 0)	
	{
		if (ParentWidthVariable())
			nObjectReferenceLineInX = (nReflinePosParamsID == 1) ? LEFT   : RIGHT;
		if (ParentHeightVariable())
			nObjectReferenceLineInY = (nReflinePosParamsID == 1) ? BOTTOM : TOP;
	}

	if ( (nSide == BACKSIDE) && bDoTransform) 
		if (m_nPlaceOn == CReflinePositionParams::PlaceOnBothMirror)
		{
			if (pLayout->KindOfProduction() == WORK_AND_TURN)
				switch (nObjectReferenceLineInX)
				{
				case LEFT:	nObjectReferenceLineInX = RIGHT;	break;
				case RIGHT:	nObjectReferenceLineInX = LEFT;		break;
				}
			else
				switch (nObjectReferenceLineInY)
				{
				case BOTTOM:nObjectReferenceLineInY = TOP;		break;
				case TOP:	nObjectReferenceLineInY = BOTTOM;	break;
				}
		}

	float fMarkOffX	= GetParentWidth(fDefaultWidth)  /2.0f;
	float fMarkOffY	= GetParentHeight(fDefaultHeight)/2.0f;
	switch (nObjectReferenceLineInX)
	{
	case LEFT:		fMarkOffX = 0.0f;									break;
	case RIGHT:		fMarkOffX = GetParentWidth(fDefaultWidth);			break;
	case XCENTER:	fMarkOffX = GetParentWidth(fDefaultWidth)  / 2.0f;	break;
	default:		fMarkOffX = GetParentWidth(fDefaultWidth)  / 2.0f;	break;
	}
	switch (nObjectReferenceLineInY)
	{
	case BOTTOM:	fMarkOffY = 0.0f;									break;
	case TOP:		fMarkOffY = GetParentHeight(fDefaultHeight);        break;
	case YCENTER:	fMarkOffY = GetParentHeight(fDefaultHeight) / 2.0f; break;
	default:		fMarkOffY = GetParentHeight(fDefaultHeight) / 2.0f; break;
	}

	int				 nReferenceLineInX	 = m_nReferenceLineInX;
	int				 nReferenceLineInY	 = m_nReferenceLineInY;
	CObjectRefParams hReferenceObjectInX = m_hReferenceObjectInX;
	CObjectRefParams hReferenceObjectInY = m_hReferenceObjectInY;
	float			 fDistanceX			 = m_fDistanceX;
	float			 fDistanceY			 = m_fDistanceY;
	if ( (nSide == BACKSIDE) && bDoTransform) 
		if (m_nPlaceOn == CReflinePositionParams::PlaceOnBothMirror)
		{
			if (pLayout->KindOfProduction() == WORK_AND_TURN)
			{
				switch (nReferenceLineInX)
				{
				case LEFT:	nReferenceLineInX = RIGHT;	break;
				case RIGHT:	nReferenceLineInX = LEFT;	break;
				}

				fDistanceX *= -1.0f;

				hReferenceObjectInX.m_nColIndex = (BYTE)((hReferenceObjectInX.m_nColIndex % 2) ? hReferenceObjectInX.m_nColIndex - 1 : hReferenceObjectInX.m_nColIndex + 1);
			}
			else
			{
				switch (nReferenceLineInY)
				{
				case BOTTOM:nReferenceLineInY = TOP;	break;
				case TOP:	nReferenceLineInY = BOTTOM; break;
				}

				fDistanceY *= -1.0f;

				hReferenceObjectInY.m_nRowIndex = (BYTE)((hReferenceObjectInY.m_nRowIndex % 2) ? hReferenceObjectInY.m_nRowIndex - 1 : hReferenceObjectInY.m_nRowIndex + 1);
			}
		}

	CLayoutSide* pLayoutSide = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;
	rfLeft   = pLayoutSide->GetRefline(m_nReferenceItemInX, nReferenceLineInX, &hReferenceObjectInX) + fDistanceX - fMarkOffX;
	rfBottom = pLayoutSide->GetRefline(m_nReferenceItemInY, nReferenceLineInY, &hReferenceObjectInY) + fDistanceY - fMarkOffY;
}

void CReflinePositionParams::SetSheetReference(BYTE nReferenceItem, BYTE nSide, CLayoutObject* pObject, BOOL bObjectBorder)
{
	BYTE nReferenceObjectRowIndex = 0, nReferenceObjectColIndex = 0;
	if ( (nReferenceItem == OBJECT_REFLINE) && pObject)
	{
		CLayoutSide* pLayoutSide = pObject->GetLayoutSide();
		if (pLayoutSide)
		{
			CLayoutObjectMatrix	objectMatrix(pLayoutSide->m_ObjectList, CLayoutObjectMatrix::TakePages);	
			objectMatrix.GetIndices(pObject, nReferenceObjectRowIndex, nReferenceObjectColIndex);
		}
	}

	switch (nSide)
	{
		case LEFT :		
		case RIGHT:		
		case XCENTER:	m_nReferenceItemInX				  = nReferenceItem;
						m_nReferenceLineInX				  = nSide;
						m_hReferenceObjectInX.m_nRowIndex = nReferenceObjectRowIndex;
						m_hReferenceObjectInX.m_nColIndex = nReferenceObjectColIndex;
						m_hReferenceObjectInX.m_bBorder	  = bObjectBorder;
						break;
		case TOP  :		
		case BOTTOM:	
		case YCENTER:	m_nReferenceItemInY				  = nReferenceItem;
						m_nReferenceLineInY				  = nSide;
						m_hReferenceObjectInY.m_nRowIndex = nReferenceObjectRowIndex;
						m_hReferenceObjectInY.m_nColIndex = nReferenceObjectColIndex;
						m_hReferenceObjectInY.m_bBorder	  = bObjectBorder;
						break;
		default	  :		break;
	}
}


void CReflinePositionParams::SetObjectReference(BYTE nSide)
{
	switch (nSide)
	{
		case LEFT :		
		case RIGHT:		
		case XCENTER:	m_nObjectReferenceLineInX = nSide; 
						break;
		case TOP  :		
		case BOTTOM:	
		case YCENTER:	m_nObjectReferenceLineInY = nSide; 
						break;
		default	  :		break;
	}
}

void CReflinePositionParams::DrawSheetReflines(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nEdge, float fDefaultWidth, float fDefaultHeight, BOOL bDoTransform, int nDirection)
{
	CLayout* pActLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pActLayout)
		return;

	if ( m_nAnchor != REFLINE_ANCHOR)
		return;

	CLayoutSide*  pLayoutSide  = (nSide == FRONTSIDE) ? &pActLayout->m_FrontSide : (nSide == BACKSIDE) ? &pActLayout->m_BackSide : NULL;
	CPressDevice* pPressDevice = (pLayoutSide) ? pLayoutSide->GetPressDevice() : NULL;

	if ( ! pPressDevice)
		return;

	CReflinePositionParams* pReflinePosParamsAnchor = ((CReflineMark*)m_pParentMark)->GetReflinePosParams(0);
	BOOL					bDrawHorizontal			= FALSE;
	BOOL					bDrawVertical			= FALSE;
	switch (nEdge)
	{
	case 0:			bDrawHorizontal = (ParentHeightVariable()) ? FALSE : TRUE;
					bDrawVertical	= (ParentWidthVariable())  ? FALSE : TRUE; 
					break;
	case LEFT:
	case RIGHT:		bDrawVertical	= TRUE; m_nObjectReferenceLineInX = nEdge; 
					if (pReflinePosParamsAnchor && ! ((CReflineMark*)m_pParentMark)->m_bVariableHeight)
					{
						m_hReferenceObjectInY = pReflinePosParamsAnchor->m_hReferenceObjectInY;
						m_nReferenceItemInY	  = pReflinePosParamsAnchor->m_nReferenceItemInY;
						m_nReferenceLineInY   = pReflinePosParamsAnchor->m_nReferenceLineInY;
					}
					break;
	case BOTTOM:
	case TOP:		bDrawHorizontal = TRUE; m_nObjectReferenceLineInY = nEdge; 
					if (pReflinePosParamsAnchor && ! ((CReflineMark*)m_pParentMark)->m_bVariableWidth)
					{
						m_hReferenceObjectInX = pReflinePosParamsAnchor->m_hReferenceObjectInX;
						m_nReferenceItemInX	  = pReflinePosParamsAnchor->m_nReferenceItemInX;
						m_nReferenceLineInX   = pReflinePosParamsAnchor->m_nReferenceLineInX;
					}
					break;
	}
	int nObjectReferenceLineInY = m_nObjectReferenceLineInY;

	float fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;
	if (m_pParentMark)
	{
		m_pParentMark->GetSheetTrimBox((void*)pPrintSheet, NULL, nSide, 0, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);
		if ( (fabs(fTrimBoxLeft) == FLT_MAX) || (fabs(fTrimBoxBottom) == FLT_MAX) || (fabs(fTrimBoxRight) == FLT_MAX) || (fabs(fTrimBoxTop) == FLT_MAX) )
			return;	// mark cannot be placed
	}
	else
	{
		GetSheetPosition(pPrintSheet, nSide, fTrimBoxLeft, fTrimBoxBottom, fDefaultWidth, fDefaultHeight, bDoTransform);
		if ( (fTrimBoxLeft == FLT_MAX) || (fTrimBoxBottom == FLT_MAX) )	// no refpos found for mark 
			return;
		fTrimBoxRight = fTrimBoxLeft   + GetParentWidth (fDefaultWidth);
		fTrimBoxTop	  = fTrimBoxBottom + GetParentHeight(fDefaultHeight);
	}

	CRect markRect;
	markRect.left	= CPrintSheetView::WorldToLP(fTrimBoxLeft);
	markRect.bottom	= CPrintSheetView::WorldToLP(fTrimBoxBottom);
	markRect.right	= CPrintSheetView::WorldToLP(fTrimBoxRight);
	markRect.top	= CPrintSheetView::WorldToLP(fTrimBoxTop);
	markRect.NormalizeRect();

	float fLeft   = fTrimBoxLeft;
	float fBottom = fTrimBoxBottom;

	//float fLeft, fBottom;
	//GetSheetPosition(pPrintSheet, nSide, fLeft, fBottom, fDefaultWidth, fDefaultHeight, bDoTransform);
	//if ( (fLeft == FLT_MAX) || (fBottom == FLT_MAX) )	// no refpos found for mark 
	//	return;

	//CRect markRect;
	//markRect.left   = CPrintSheetView::WorldToLP(fLeft);
	//markRect.bottom = CPrintSheetView::WorldToLP(fBottom);
	//markRect.right  = CPrintSheetView::WorldToLP(fLeft	 + GetParentWidth (fDefaultWidth));
	//markRect.top	= CPrintSheetView::WorldToLP(fBottom + GetParentHeight(fDefaultHeight));
	//markRect.NormalizeRect();

	CRect plateRect(CPrintSheetView::WorldToLP(0.0f),						 CPrintSheetView::WorldToLP(0.0f),
					CPrintSheetView::WorldToLP(pPressDevice->m_fPlateWidth), CPrintSheetView::WorldToLP(pPressDevice->m_fPlateHeight));
	plateRect.NormalizeRect();

	CRect sheetRect;
	sheetRect.left   = CPrintSheetView::WorldToLP(pLayoutSide->m_fPaperLeft  );
	sheetRect.top	 = CPrintSheetView::WorldToLP(pLayoutSide->m_fPaperTop   );
	sheetRect.right  = CPrintSheetView::WorldToLP(pLayoutSide->m_fPaperRight );
	sheetRect.bottom = CPrintSheetView::WorldToLP(pLayoutSide->m_fPaperBottom);
	sheetRect.NormalizeRect();

	CPoint ptMarkRef;
	float  fMarkOffX, fMarkOffY;
	switch (m_nObjectReferenceLineInX)
	{
	case LEFT:		ptMarkRef.x = markRect.left;						fMarkOffX = 0.0f;								  break;
	case RIGHT:		ptMarkRef.x = markRect.right;						fMarkOffX = GetParentWidth(fDefaultWidth);		  break;
	case XCENTER:	ptMarkRef.x = markRect.left + markRect.Width() / 2; fMarkOffX = GetParentWidth(fDefaultWidth) / 2.0f; break;
	default:		ptMarkRef.x = markRect.left + markRect.Width() / 2; fMarkOffX = GetParentWidth(fDefaultWidth) / 2.0f; break;
	}

	if ( (nSide == BACKSIDE) && bDoTransform)
		if (pActLayout->KindOfProduction() == WORK_AND_TUMBLE)
			nObjectReferenceLineInY = (m_nObjectReferenceLineInY == TOP) ? BOTTOM : (m_nObjectReferenceLineInY == BOTTOM) ? TOP : m_nObjectReferenceLineInY;

	switch (nObjectReferenceLineInY)
	{
	case BOTTOM:	ptMarkRef.y = markRect.top;							fMarkOffY = 0.0f;									break;
	case TOP:		ptMarkRef.y = markRect.bottom;						fMarkOffY = GetParentHeight(fDefaultHeight);		break;
	case YCENTER:	ptMarkRef.y = markRect.top + markRect.Height() / 2; fMarkOffY = GetParentHeight(fDefaultHeight) / 2.0f; break;
	default:		ptMarkRef.y = markRect.top + markRect.Height() / 2; fMarkOffY = GetParentHeight(fDefaultHeight) / 2.0f; break;
	}

	if ( (nSide == BACKSIDE) && bDoTransform)
	{
		if (m_nPlaceOn == CReflinePositionParams::PlaceOnBothMirror)
		{
			if (pActLayout->KindOfProduction() == WORK_AND_TURN)
			{
				fLeft   -= -m_fDistanceX - fMarkOffX;
				fBottom -=  m_fDistanceY - fMarkOffY;
			}
			else
			{
				fLeft   -=  m_fDistanceX - fMarkOffX;
				fBottom -= -m_fDistanceY - fMarkOffY;
			}
		}
		else	// same pos
		{
			fLeft   -= m_fDistanceX - fMarkOffX;
			fBottom -= m_fDistanceY - fMarkOffY;
		}
	}
	else
	{
		fLeft   -= m_fDistanceX - fMarkOffX;
		fBottom -= m_fDistanceY - fMarkOffY;
	}

	CPoint ptSheetRef(CPrintSheetView::WorldToLP(fLeft), CPrintSheetView::WorldToLP(fBottom));

	CPoint sheetRefHLine[2] = { CPoint(plateRect.left, ptSheetRef.y),   CPoint(plateRect.right, ptSheetRef.y) };
	CPoint sheetRefVLine[2] = { CPoint(ptSheetRef.x,   plateRect.top), 	CPoint(ptSheetRef.x,    plateRect.bottom) };
	CSize  sizeOff(1000, 1000);
	CPoint markRefHLine[2]  = { CPoint(markRect.left - sizeOff.cx, ptMarkRef.y),   CPoint(markRect.right + sizeOff.cx, ptMarkRef.y) };
	CPoint markRefVLine[2]  = { CPoint(ptMarkRef.x,	markRect.bottom + sizeOff.cy), CPoint(ptMarkRef.x, markRect.top	- sizeOff.cy) };

	// draw sheet reference lines
	CPen reflineHPen(PS_DOT, 1, (m_nReferenceItemInY == PLATE_REFLINE) ? BLACK : (m_nReferenceItemInY == PAPER_REFLINE) ? DARKGRAY : RGB(0,200,0));
	CPen reflineVPen(PS_DOT, 1, (m_nReferenceItemInX == PLATE_REFLINE) ? BLACK : (m_nReferenceItemInX == PAPER_REFLINE) ? DARKGRAY : RGB(0,200,0));
	CPen redPen	    (PS_DOT, 1, LIGHTRED);

	CPen* pOldPen = pDC->SelectObject(&reflineHPen);

	if (nDirection != -1)
	{
		if (nDirection == HORIZONTAL)
		{
			bDrawHorizontal = FALSE;
			bDrawVertical   = TRUE;
		}
		else
		if (nDirection == VERTICAL)
		{
			bDrawHorizontal = TRUE;
			bDrawVertical   = FALSE;
		}
	}

	if (bDrawHorizontal)
	{
		pDC->MoveTo(sheetRefHLine[0]);
		pDC->LineTo(sheetRefHLine[1]);
	}
	pDC->SelectObject(&reflineVPen);
	if (bDrawVertical)
	{
		pDC->MoveTo(sheetRefVLine[0]);
		pDC->LineTo(sheetRefVLine[1]);
	}

	pDC->SelectObject(&redPen);
	if (bDrawHorizontal)
	{
		pDC->MoveTo(markRefHLine[0]);
		pDC->LineTo(markRefHLine[1]);
	}
	if (bDrawVertical)
	{
		pDC->MoveTo(markRefVLine[0]);
		pDC->LineTo(markRefVLine[1]);
	}

	// red solid cross
	CPoint centerPoint(markRefVLine[0].x, markRefHLine[0].y);
	CPen solidRedPen(PS_SOLID, 3, RED);
	pOldPen = pDC->SelectObject(&solidRedPen);
	CSize sizeCross(15, 15);
	pDC->DPtoLP(&sizeCross);
	if (bDrawHorizontal)
	{
		pDC->MoveTo(centerPoint.x - sizeCross.cx, centerPoint.y);
		pDC->LineTo(centerPoint.x + sizeCross.cx, centerPoint.y);
	}
	if (bDrawVertical)
	{
		pDC->MoveTo(centerPoint.x, centerPoint.y - sizeCross.cy);
		pDC->LineTo(centerPoint.x, centerPoint.y + sizeCross.cy);
	}
	pDC->SelectObject(pOldPen);
	reflineHPen.DeleteObject();
	reflineVPen.DeleteObject();
	solidRedPen.DeleteObject();
	redPen.DeleteObject();

	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop; 
	pLayoutSide->CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);
	CRect bboxRect;
	bboxRect.left	= CPrintSheetView::WorldToLP(fBBoxLeft);
	bboxRect.top	= CPrintSheetView::WorldToLP(fBBoxTop);
	bboxRect.right	= CPrintSheetView::WorldToLP(fBBoxRight);
	bboxRect.bottom = CPrintSheetView::WorldToLP(fBBoxBottom);

	DrawCenterHandles(pDC, plateRect, BLACK,		bDrawHorizontal, bDrawVertical, 2);
	DrawCenterHandles(pDC, sheetRect, MIDDLEGRAY,	bDrawHorizontal, bDrawVertical, 2);
	DrawCenterHandles(pDC, bboxRect,  RGB(0,200,0), bDrawHorizontal, bDrawVertical, 2);

	pDC->LPtoDP(&ptMarkRef,  1);
	pDC->LPtoDP(&ptSheetRef, 1);
	pDC->LPtoDP(markRect);

	pDC->LPtoDP(&sizeOff);
	int	   nDistance	   = max(sizeOff.cx / 2, 15);	// distance of measure lines from mark
	CPoint measureHLine[2] = { CPoint(min(ptMarkRef.x, ptSheetRef.x), markRect.top + nDistance), 
							   CPoint(max(ptMarkRef.x, ptSheetRef.x), markRect.top + nDistance) };
	CPoint measureVLine[2] = { CPoint(markRect.right + nDistance,	  min(ptMarkRef.y, ptSheetRef.y)),		 
							   CPoint(markRect.right + nDistance,	  max(ptMarkRef.y, ptSheetRef.y)) };
	BOOL bIntersect = FALSE;
	if (LinesIntersect(measureHLine, measureVLine))
	{
		measureHLine[0].y = markRect.bottom - nDistance;
		measureHLine[1].y = markRect.bottom - nDistance;
		bIntersect = TRUE;
	}
	int	nLenH = measureHLine[1].x - measureHLine[0].x;
	int	nLenV = measureVLine[1].y - measureVLine[0].y;

	// if measurement arrow too short, flip arrows and use fix length
	if (nLenH < 20)
	{
		int nTemp = measureHLine[0].x; measureHLine[0].x = measureHLine[1].x; measureHLine[1].x = nTemp;
		nLenH = 15;
	}
	if (nLenV < 20)
	{
		int nTemp = measureVLine[0].y; measureVLine[0].y = measureVLine[1].y; measureVLine[1].y = nTemp;
		nLenV = 15;
	}

	// draw measurement arrows
	COLORREF crArrowColor = LIGHTGRAY;
	if (bDrawVertical)
	{
		CPrintSheetView::DrawArrow(pDC, LEFT,   measureHLine[0], nLenH - 5, BLACK, crArrowColor, TRUE);
		CPrintSheetView::DrawArrow(pDC, RIGHT,  measureHLine[1], nLenH - 5, BLACK, crArrowColor, TRUE);
	}
	if (bDrawHorizontal)
	{
		CPrintSheetView::DrawArrow(pDC, TOP,	measureVLine[0], nLenV - 5, BLACK, crArrowColor, TRUE);
		CPrintSheetView::DrawArrow(pDC, BOTTOM,	measureVLine[1], nLenV - 5, BLACK, crArrowColor, TRUE);
	}

	// draw measurement text
	CSize sizeFont(13,13);
	pDC->DPtoLP(&sizeFont);
	CFont	 font;
	font.CreateFont(sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	CFont*   pOldFont		= pDC->SelectObject(&font);
	COLORREF crOldTextColor = pDC->SetTextColor(BLACK);
	COLORREF crOldBkColor	= pDC->SetBkColor(WHITE);
	int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);

	float fDistanceX = m_fDistanceX;
	float fDistanceY = m_fDistanceY;
	if ( (nSide == BACKSIDE) && (m_nPlaceOn == CReflinePositionParams::PlaceOnBothMirror) )
	{
		if (pActLayout->KindOfProduction() == WORK_AND_TURN)
			fDistanceX *= -1.0f;
		//else
			//fDistanceY *= -1.0f;
	}

   	CPrintSheetView* pView			= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	BOOL			 bDlgAlignOpen	= (pView) ? ((pView->IsOpenDlgAlignObjects()) ? TRUE : FALSE) : FALSE;
	CString strHValue, strVValue;
	if (bDlgAlignOpen)
	{
		SetMeasure(0, fabs(fDistanceX), strHValue);	// format string
		SetMeasure(0, fabs(fDistanceY), strVValue);	// format string
	}
	else
	{
		SetMeasure(0, fDistanceX, strHValue);	// format string
		SetMeasure(0, fDistanceY, strVValue);	// format string
	}

	CSize textHExtent = pDC->GetTextExtent(strHValue);
	CSize textVExtent = pDC->GetTextExtent(strVValue);
	pDC->LPtoDP(&textHExtent);
	pDC->LPtoDP(&textVExtent);

	CPoint  ptTextHCenter(min(measureHLine[0].x,  measureHLine[1].x) + nLenH/2, (bIntersect) ? measureHLine[0].y - (textHExtent.cy + 2) : measureHLine[0].y + (textHExtent.cy + 2));
	CPoint  ptTextVCenter(measureVLine[0].x + textVExtent.cx/2 + 8, min(measureVLine[0].y, measureVLine[1].y) + nLenV/2 - textVExtent.cy/2 + 2);

	CRect textHRect(ptTextHCenter, ptTextHCenter);
	CRect textVRect(ptTextVCenter, ptTextVCenter);
	textHRect.InflateRect(textHExtent.cx/2 + 5, textHExtent.cy/2 + 1);
	textVRect.InflateRect(textVExtent.cx/2 + 5, textVExtent.cy/2 + 1);
	pDC->DPtoLP(textHRect);
	pDC->DPtoLP(textVRect);

	BOOL bSupressText = FALSE;
	if (pView) 
		if (pView->m_reflineDistanceEdit.m_hWnd)
			if (pView->m_reflineDistanceEdit.IsWindowVisible())
				bSupressText = TRUE;

	if (bDrawVertical)
	{
		if ( ! bSupressText)
			pDC->DrawText(strHValue, textHRect, DT_CENTER | DT_VCENTER);
		pView->m_DisplayList.RegisterItem(pDC, textHRect, textHRect, (void*)&m_fDistanceX, 
										  DISPLAY_ITEM_REGISTRY(CPrintSheetView, ReflineXDistance), NULL, CDisplayItem::DontDeselectType | CDisplayItem::ForceRegister | CDisplayItem::NoFeedback); 
	}
	if (bDrawHorizontal)
	{
		if ( ! bSupressText)
			pDC->DrawText(strVValue, textVRect, DT_CENTER | DT_VCENTER);
		pView->m_DisplayList.RegisterItem(pDC, textVRect, textVRect, (void*)&m_fDistanceY, 
										  DISPLAY_ITEM_REGISTRY(CPrintSheetView, ReflineYDistance), NULL, CDisplayItem::DontDeselectType | CDisplayItem::ForceRegister | CDisplayItem::NoFeedback); 
	}

	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldTextColor);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetBkMode(nOldBkMode);

	CRect rectMeasureHLine(measureHLine[0], measureHLine[1]);
	CRect rectMeasureVLine(measureVLine[0], measureVLine[1]);
	rectMeasureHLine.NormalizeRect();
	rectMeasureVLine.NormalizeRect();
	rectMeasureHLine.InflateRect(15, 15);
	rectMeasureVLine.InflateRect(15, 15);
	markRect.NormalizeRect();
	markRect.InflateRect(nDistance, nDistance);
	pDC->LPtoDP(textHRect);
	pDC->LPtoDP(textVRect);
	textHRect.NormalizeRect();
	textVRect.NormalizeRect();

	if (nSide == FRONTSIDE)
	{
		pView->m_reflineUpdateRectFront.UnionRect(markRect,						   textHRect);
		pView->m_reflineUpdateRectFront.UnionRect(pView->m_reflineUpdateRectFront, textVRect);
		pView->m_reflineUpdateRectFront.UnionRect(pView->m_reflineUpdateRectFront, rectMeasureHLine);
		pView->m_reflineUpdateRectFront.UnionRect(pView->m_reflineUpdateRectFront, rectMeasureVLine);
		pView->m_reflineUpdateRectFront.InflateRect(20, 20);
		pDC->LPtoDP(plateRect);
		plateRect.NormalizeRect();
		if (plateRect.PtInRect(pView->m_reflineUpdateRectFront.TopLeft()) && plateRect.PtInRect(pView->m_reflineUpdateRectFront.BottomRight()))
			pView->m_bReflineUpdateEraseFront = FALSE;
		else
			pView->m_bReflineUpdateEraseFront = TRUE;
	}
	else
	{
		pView->m_reflineUpdateRectBack.UnionRect(markRect,						 textHRect);
		pView->m_reflineUpdateRectBack.UnionRect(pView->m_reflineUpdateRectBack, textVRect);
		pView->m_reflineUpdateRectBack.UnionRect(pView->m_reflineUpdateRectBack, rectMeasureHLine);
		pView->m_reflineUpdateRectBack.UnionRect(pView->m_reflineUpdateRectBack, rectMeasureVLine);
		pView->m_reflineUpdateRectBack.InflateRect(20, 20);
		pDC->LPtoDP(plateRect);
		plateRect.NormalizeRect();
		if (plateRect.PtInRect(pView->m_reflineUpdateRectBack.TopLeft()) && plateRect.PtInRect(pView->m_reflineUpdateRectBack.BottomRight()))
			pView->m_bReflineUpdateEraseBack = FALSE;
		else
			pView->m_bReflineUpdateEraseBack = TRUE;
	}

	font.DeleteObject();
}


void CReflinePositionParams::DrawCenterHandles(CDC* pDC, CRect rect, COLORREF crColor, BOOL bDrawHorizontal, BOOL bDrawVertical, int nThickness)
{
	CSize sizeLen(CPrintSheetView::WorldToLP(2.5f), CPrintSheetView::WorldToLP(2.5f));
	pDC->LPtoDP(&sizeLen);
	sizeLen.cx = max(0, 3);
	sizeLen.cy = max(0, 3);
	pDC->DPtoLP(&sizeLen);
	rect.NormalizeRect();
	CRect outRect = rect, inRect = rect;
	outRect.InflateRect(sizeLen);
	inRect.DeflateRect(sizeLen);

	CPen pen(PS_SOLID, Pixel2ConstMM(pDC, nThickness), crColor);
	CPen* pOldPen = pDC->SelectObject(&pen);

	int nOrientation = 0;
	if (bDrawHorizontal) 
		nOrientation |= HORIZONTAL;
	if (bDrawVertical) 
		nOrientation |= VERTICAL;

	if (nOrientation & VERTICAL)
	{
		pDC->MoveTo(outRect.CenterPoint().x, outRect.top);				// top handle
		pDC->LineTo(outRect.CenterPoint().x,  inRect.top);
		pDC->MoveTo(outRect.CenterPoint().x,  inRect.bottom);			// bottom handle
		pDC->LineTo(outRect.CenterPoint().x, outRect.bottom);
	}
	
	if (nOrientation & HORIZONTAL)
	{
		pDC->MoveTo(outRect.left,			 outRect.CenterPoint().y);	// left handle
		pDC->LineTo( inRect.left,			 outRect.CenterPoint().y);
		pDC->MoveTo( inRect.right,			 outRect.CenterPoint().y);	// right handle
		pDC->LineTo(outRect.right,			 outRect.CenterPoint().y);
	}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();
}


BOOL CReflinePositionParams::LinesIntersect(CPoint lineH[2], CPoint lineV[2])
{
	if (lineH[0].x - 5 > lineV[0].x)
		return FALSE;
	if (lineH[1].x + 5 < lineV[0].x)
		return FALSE;
	if (lineV[0].y - 5 > lineH[0].y)
		return FALSE;
	if (lineV[1].y + 5 < lineH[0].y)
		return FALSE;

	return TRUE;
}


void CReflinePositionParams::SetUndefined()
{
	m_fDistanceX = m_fDistanceY = 9999.0f;
}


BOOL CReflinePositionParams::IsUndefined()
{
	if ( (m_fDistanceX == 9999.0f) || (m_fDistanceY == 9999.0f) )
		return TRUE;
	else
		return FALSE;
}


void CReflinePositionParams::SetDefault(CLayoutObject* pLayoutObject)
{
	if ( ! pLayoutObject)
		return;
	CLayoutSide* pLayoutSide = pLayoutObject->GetLayoutSide();
	if ( ! pLayoutSide)
		return;

	m_fDistanceX			  = pLayoutObject->GetLeft()   + pLayoutObject->GetWidth() /2.0f - pLayoutSide->m_fPaperLeft;
	m_fDistanceY			  = pLayoutObject->GetBottom() + pLayoutObject->GetHeight()/2.0f - pLayoutSide->m_fPaperBottom;
	m_nReferenceItemInX		  = PAPER_REFLINE;
	m_nReferenceItemInY		  = PAPER_REFLINE;
	m_nReferenceLineInX		  = LEFT;
	m_nReferenceLineInY		  = BOTTOM;
	m_nObjectReferenceLineInX = XCENTER;
	m_nObjectReferenceLineInY = YCENTER;
	m_nPlaceOn				  = PlaceOnBothSamePos;
}

float CReflinePositionParams::GetCounterpartOffsetX(CLayoutObject* pLayoutObject)
{
	if ( ! pLayoutObject)
		return 0.0f;

	CLayout*	 pLayout	 = pLayoutObject->GetLayout();
	CLayoutSide* pLayoutSide = pLayoutObject->GetLayoutSide();
	if ( ! pLayout || ! pLayoutSide)
		return 0.0f;
	CLayoutSide* pLayoutSideCounterpart = (pLayoutSide->m_nLayoutSide == FRONTSIDE) ? &pLayout->m_BackSide : &pLayout->m_FrontSide;
	if ( ! pLayoutSideCounterpart)
		return 0.0f;

	float fOffsetX = 0.0f;
	if (m_nReferenceItemInX == OBJECT_REFLINE)
	{
		CLayoutObjectMatrix	objectMatrix(pLayoutSide->m_ObjectList, CLayoutObjectMatrix::TakePages);	
		CLayoutObject* pObject = objectMatrix.GetObject(m_hReferenceObjectInX.m_nRowIndex, m_hReferenceObjectInX.m_nColIndex);

		CLayoutObjectMatrix	objectMatrixCounterpart(pLayoutSideCounterpart->m_ObjectList, CLayoutObjectMatrix::TakePages);	
		CLayoutObject* pObjectCounterpart = objectMatrixCounterpart.GetObject(m_hReferenceObjectInX.m_nRowIndex, m_hReferenceObjectInX.m_nColIndex);
		if (pObject && pObjectCounterpart)
			fOffsetX = pObjectCounterpart->GetLeft() - pObject->GetLeft();
	}
	return fOffsetX;
}

float CReflinePositionParams::GetCounterpartOffsetY(CLayoutObject* pLayoutObject)
{
	if ( ! pLayoutObject)
		return 0.0f;

	CLayout*	 pLayout	 = pLayoutObject->GetLayout();
	CLayoutSide* pLayoutSide = pLayoutObject->GetLayoutSide();
	if ( ! pLayout || ! pLayoutSide)
		return 0.0f;
	CLayoutSide* pLayoutSideCounterpart = (pLayoutSide->m_nLayoutSide == FRONTSIDE) ? &pLayout->m_BackSide : &pLayout->m_FrontSide;
	if ( ! pLayoutSideCounterpart)
		return 0.0f;

	float fOffsetY = 0.0f;
	if (m_nReferenceItemInY == OBJECT_REFLINE)
	{
		CLayoutObjectMatrix	objectMatrix(pLayoutSide->m_ObjectList, CLayoutObjectMatrix::TakePages);	
		CLayoutObject* pObject = objectMatrix.GetObject(m_hReferenceObjectInY.m_nRowIndex, m_hReferenceObjectInY.m_nColIndex);

		CLayoutObjectMatrix	objectMatrixCounterpart(pLayoutSideCounterpart->m_ObjectList, CLayoutObjectMatrix::TakePages);	
		CLayoutObject* pObjectCounterpart = objectMatrixCounterpart.GetObject(m_hReferenceObjectInY.m_nRowIndex, m_hReferenceObjectInY.m_nColIndex);
		if (pObject && pObjectCounterpart)
			fOffsetY = pObjectCounterpart->GetBottom() - pObject->GetBottom();
	}
	return fOffsetY;
}

CReflinePositionParams CReflinePositionParams::Rotate(int nHeadPosition)
{
	CReflinePositionParams reflinePosParams = *this;
	switch (nHeadPosition)
	{
	case TOP:	break;

	case BOTTOM:switch (m_nReferenceLineInX)
				{
				case LEFT:		reflinePosParams.m_nReferenceLineInX = RIGHT;			break;
				case RIGHT:		reflinePosParams.m_nReferenceLineInX = LEFT;			break;
				case XCENTER:	reflinePosParams.m_nReferenceLineInX = XCENTER;			break;
				}
				switch (m_nReferenceLineInY)
				{
				case BOTTOM:	reflinePosParams.m_nReferenceLineInY = TOP;				break;
				case TOP:		reflinePosParams.m_nReferenceLineInY = BOTTOM;			break;
				case YCENTER:	reflinePosParams.m_nReferenceLineInY = YCENTER;			break;
				}
				switch (m_nObjectReferenceLineInX)
				{
				case LEFT:		reflinePosParams.m_nObjectReferenceLineInX = RIGHT;		break;
				case RIGHT:		reflinePosParams.m_nObjectReferenceLineInX = LEFT;		break;
				case XCENTER:	reflinePosParams.m_nObjectReferenceLineInX = XCENTER;	break;
				}
				switch (m_nObjectReferenceLineInY)
				{
				case BOTTOM:	reflinePosParams.m_nObjectReferenceLineInY = TOP;		break;
				case TOP:		reflinePosParams.m_nObjectReferenceLineInY = BOTTOM;	break;
				case YCENTER:	reflinePosParams.m_nObjectReferenceLineInY = YCENTER;	break;
				}
				reflinePosParams.m_fDistanceX = -m_fDistanceX;
				reflinePosParams.m_fDistanceY = -m_fDistanceY;
				break;

	case LEFT:	switch (m_nReferenceLineInX)
				{
				case LEFT:		reflinePosParams.m_nReferenceLineInY = BOTTOM;			break;
				case RIGHT:		reflinePosParams.m_nReferenceLineInY = TOP;				break;
				case XCENTER:	reflinePosParams.m_nReferenceLineInY = YCENTER;			break;
				}
				switch (m_nReferenceLineInY)
				{
				case BOTTOM:	reflinePosParams.m_nReferenceLineInX = RIGHT;			break;
				case TOP:		reflinePosParams.m_nReferenceLineInX = LEFT;			break;
				case YCENTER:	reflinePosParams.m_nReferenceLineInX = XCENTER;			break;
				}
				switch (m_nObjectReferenceLineInX)
				{
				case LEFT:		reflinePosParams.m_nObjectReferenceLineInY = BOTTOM;	break;
				case RIGHT:		reflinePosParams.m_nObjectReferenceLineInY = TOP;		break;
				case XCENTER:	reflinePosParams.m_nObjectReferenceLineInY = YCENTER;	break;
				}									
				switch (m_nObjectReferenceLineInY)	
				{									
				case BOTTOM:	reflinePosParams.m_nObjectReferenceLineInX = RIGHT;		break;
				case TOP:		reflinePosParams.m_nObjectReferenceLineInX = LEFT;		break;
				case YCENTER:	reflinePosParams.m_nObjectReferenceLineInX = XCENTER;	break;
				}
				reflinePosParams.m_fDistanceX = -m_fDistanceY;
				reflinePosParams.m_fDistanceY =  m_fDistanceX;
				break;

	case RIGHT:	switch (m_nReferenceLineInX)
				{
				case LEFT:		reflinePosParams.m_nReferenceLineInY = TOP;				break;
				case RIGHT:		reflinePosParams.m_nReferenceLineInY = BOTTOM;			break;
				case XCENTER:	reflinePosParams.m_nReferenceLineInY = YCENTER;			break;
				}
				switch (m_nReferenceLineInY)
				{
				case BOTTOM:	reflinePosParams.m_nReferenceLineInX = LEFT;			break;
				case TOP:		reflinePosParams.m_nReferenceLineInX = RIGHT;			break;
				case YCENTER:	reflinePosParams.m_nReferenceLineInX = XCENTER;			break;
				}
				switch (m_nObjectReferenceLineInX)
				{
				case LEFT:		reflinePosParams.m_nObjectReferenceLineInY = TOP;		break;
				case RIGHT:		reflinePosParams.m_nObjectReferenceLineInY = BOTTOM;	break;
				case XCENTER:	reflinePosParams.m_nObjectReferenceLineInY = YCENTER;	break;
				}									
				switch (m_nObjectReferenceLineInY)	
				{									
				case BOTTOM:	reflinePosParams.m_nObjectReferenceLineInX = LEFT;		break;
				case TOP:		reflinePosParams.m_nObjectReferenceLineInX = RIGHT;		break;
				case YCENTER:	reflinePosParams.m_nObjectReferenceLineInX = XCENTER;	break;
				}
				reflinePosParams.m_fDistanceX =  m_fDistanceY;
				reflinePosParams.m_fDistanceY = -m_fDistanceX;
				break;
	}

	return reflinePosParams;
}



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Implementation of CMarkListOld (implementation up to 09/11/99)
// needed for compatibility to old mark files

IMPLEMENT_SERIAL(CMarkListOld, CObject, VERSIONABLE_SCHEMA | 1)

CMarkListOld::CMarkListOld()
{
}

void CMarkListOld::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
		ar << m_strNotes;
	else
	{
		ar >> m_strNotes; 
	}

	CList <CMarkOld, CMarkOld&>::Serialize(ar);
}



IMPLEMENT_SERIAL(CMarkOld, CObject, VERSIONABLE_SCHEMA | 1)

CMarkOld::CMarkOld()
{ 
}
// copy constructor for CMarkOld - needed by CList functions:
CMarkOld::CMarkOld(const CMarkOld& rMarkOld) 
{
	Copy(rMarkOld);
}
// operator overload for CLayoutObject - needed by CList functions:
const CMarkOld& CMarkOld::operator=(const CMarkOld& rMarkOld)
{
	Copy(rMarkOld);

	return *this;
}

void CMarkOld::Copy(const CMarkOld& rMarkOld)
{
	m_nType				= rMarkOld.m_nType;
	m_strFormatName		= rMarkOld.m_strFormatName;
	m_bAllColors		= rMarkOld.m_bAllColors;
	m_strReserved		= rMarkOld.m_strReserved;
	m_MarkSource		= rMarkOld.m_MarkSource;
	m_fMarkWidth		= rMarkOld.m_fMarkWidth;
	m_fMarkHeight		= rMarkOld.m_fMarkHeight;
	m_nReferenceItemInX	= rMarkOld.m_nReferenceItemInX;
	m_nReferenceItemInY	= rMarkOld.m_nReferenceItemInY;
	m_nReferenceLineInX	= rMarkOld.m_nReferenceLineInX;
	m_nReferenceLineInY	= rMarkOld.m_nReferenceLineInY;
	m_fDistanceX		= rMarkOld.m_fDistanceX;
	m_fDistanceY		= rMarkOld.m_fDistanceY;
	m_iRepetitionInX	= rMarkOld.m_iRepetitionInX;
	m_iRepetitionInY	= rMarkOld.m_iRepetitionInY;

	m_fLeftBorder		= rMarkOld.m_fLeftBorder;
	m_fRightBorder		= rMarkOld.m_fRightBorder;
	m_fLowerBorder		= rMarkOld.m_fLowerBorder;
	m_fUpperBorder		= rMarkOld.m_fUpperBorder;
	m_nHeadPosition		= rMarkOld.m_nHeadPosition;
	m_strTextmark		= rMarkOld.m_strTextmark;
	m_nDisplayColorIndex= rMarkOld.m_nDisplayColorIndex;

	m_fObjectCenterX	= rMarkOld.m_fObjectCenterX;
	m_fObjectCenterY	= rMarkOld.m_fObjectCenterY;

	m_ColorSeparations.RemoveAll();
	for (int i = 0; i < rMarkOld.m_ColorSeparations.GetSize(); i++)
		m_ColorSeparations.Add(rMarkOld.m_ColorSeparations[i]);
}

// helper function for PlateList elements
template <> void AFXAPI SerializeElements <CMarkOld> (CArchive& ar, CMarkOld* pMarkOld, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CMark));

	if (ar.IsStoring())
	{
		ar << pMarkOld->m_nType;
		ar << pMarkOld->m_strFormatName;
		ar << pMarkOld->m_bAllColors;
		ar << pMarkOld->m_strReserved;
		ar << pMarkOld->m_fMarkWidth;
		ar << pMarkOld->m_fMarkHeight;
		ar << pMarkOld->m_nReferenceItemInX;
		ar << pMarkOld->m_nReferenceItemInY;
		ar << pMarkOld->m_nReferenceLineInX;
		ar << pMarkOld->m_nReferenceLineInY;
		ar << pMarkOld->m_fDistanceX;
		ar << pMarkOld->m_fDistanceY;
		ar << pMarkOld->m_iRepetitionInX;
		ar << pMarkOld->m_iRepetitionInY;
		ar << pMarkOld->m_fLeftBorder;
		ar << pMarkOld->m_fRightBorder;
		ar << pMarkOld->m_fLowerBorder;
		ar << pMarkOld->m_fUpperBorder;
		ar << pMarkOld->m_nHeadPosition;
		ar << pMarkOld->m_strTextmark;
		ar << pMarkOld->m_nDisplayColorIndex;
		ar << pMarkOld->m_fObjectCenterX;
		ar << pMarkOld->m_fObjectCenterY;
	}											
	else																		 
	{
		UINT nVersion;

		nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> pMarkOld->m_nType;
			ar >> pMarkOld->m_strFormatName;
			ar >> pMarkOld->m_bAllColors;
			ar >> pMarkOld->m_strReserved;
			ar >> pMarkOld->m_fMarkWidth;
			ar >> pMarkOld->m_fMarkHeight;
			ar >> pMarkOld->m_nReferenceItemInX;
			ar >> pMarkOld->m_nReferenceItemInY;
			ar >> pMarkOld->m_nReferenceLineInX;
			ar >> pMarkOld->m_nReferenceLineInY;
			ar >> pMarkOld->m_fDistanceX;
			ar >> pMarkOld->m_fDistanceY;
			ar >> pMarkOld->m_iRepetitionInX;
			ar >> pMarkOld->m_iRepetitionInY;
			ar >> pMarkOld->m_fLeftBorder;
			ar >> pMarkOld->m_fRightBorder;
			ar >> pMarkOld->m_fLowerBorder;
			ar >> pMarkOld->m_fUpperBorder;
			ar >> pMarkOld->m_nHeadPosition;
			ar >> pMarkOld->m_strTextmark;
			ar >> pMarkOld->m_nDisplayColorIndex;
			ar >> pMarkOld->m_fObjectCenterX;
			ar >> pMarkOld->m_fObjectCenterY;
			pMarkOld->m_ColorSeparations.RemoveAll(); // no append, so target list has to be empty 
			pMarkOld->m_nHeadPosition = TOP;		  // by mistake, in version 1 head position has not been initialized
			break;
		}
	}

	SerializeElements(ar, &pMarkOld->m_MarkSource, 1);
	pMarkOld->m_ColorSeparations.Serialize(ar);
}


void AFXAPI DestructElements(CMarkOld* pMarkOld, INT_PTR nCount)
{
	ASSERT(nCount == 1); // in CList context nCount has to be 1

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pMarkOld->m_strFormatName.Empty();
	pMarkOld->m_strReserved.Empty();
	pMarkOld->m_strTextmark.Empty();
	pMarkOld->m_ColorSeparations.RemoveAll();
	pMarkOld->m_MarkSource.m_strDocumentFullpath.Empty();
	pMarkOld->m_MarkSource.m_strPreviewFilesPath.Empty();
	pMarkOld->m_MarkSource.m_strContentFilesPath.Empty();
	pMarkOld->m_MarkSource.m_strDocumentTitle.Empty();
	pMarkOld->m_MarkSource.m_strCreator.Empty();
	pMarkOld->m_MarkSource.m_strCreationDate.Empty();
	pMarkOld->m_MarkSource.m_PageSourceHeaders.RemoveAll();
}




/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Implementation of CMarkList

IMPLEMENT_SERIAL(CMarkList, CObject, VERSIONABLE_SCHEMA | 3)

CMarkList::CMarkList()
{
}

BOOL CMarkList::operator==(const CMarkList& rMarkList)
{
	if (GetSize() != rMarkList.GetSize())
		return FALSE;

	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i)->m_nType != rMarkList[i]->m_nType)
			return FALSE;

		switch(GetAt(i)->m_nType)
		{
		case CMark::CustomMark:		if (*(CCustomMark*)GetAt(i)  != *(CCustomMark*)rMarkList[i])	return FALSE;	break;
		case CMark::TextMark:		if (*(CTextMark*)GetAt(i)	 != *(CTextMark*)rMarkList[i])		return FALSE;	break;
		case CMark::SectionMark:	if (*(CSectionMark*)GetAt(i) != *(CSectionMark*)rMarkList[i])	return FALSE;	break;
		case CMark::CropMark:		if (*(CCropMark*)GetAt(i)	 != *(CCropMark*)rMarkList[i])		return FALSE;	break;
		case CMark::FoldMark:		if (*(CFoldMark*)GetAt(i)	 != *(CFoldMark*)rMarkList[i])		return FALSE;	break;
		case CMark::BarcodeMark:	if (*(CBarcodeMark*)GetAt(i) != *(CBarcodeMark*)rMarkList[i])	return FALSE;	break;
		case CMark::DrawingMark:	if (*(CDrawingMark*)GetAt(i) != *(CDrawingMark*)rMarkList[i])	return FALSE;	break;
		}
	}
	return TRUE;
}

void CMarkList::Copy(const CMarkList& rMarkList)
{
	for (int i = 0; i < rMarkList.GetSize(); i++)
	{
		switch(rMarkList[i]->m_nType)
		{
			case CMark::CustomMark:		
				{
					CCustomMark* pMark = new CCustomMark;
					*pMark = *(CCustomMark*)rMarkList[i];
					Add((CMark*)pMark);
				}
				break;
			case CMark::TextMark:		
				{
					CTextMark* pMark = new CTextMark;
					*pMark = *(CTextMark*)rMarkList[i];
					Add((CMark*)pMark);
				}
				break;
			case CMark::SectionMark:	
				{
					CSectionMark* pMark = new CSectionMark;
					*pMark = *(CSectionMark*)rMarkList[i];
					Add((CMark*)pMark);
				}
				break;
			case CMark::CropMark:	
				{
					CCropMark* pMark = new CCropMark;
					*pMark = *(CCropMark*)rMarkList[i];
					Add((CMark*)pMark);
				}
				break;
			case CMark::FoldMark:	
				{
					CFoldMark* pMark = new CFoldMark;
					*pMark = *(CFoldMark*)rMarkList[i];
					Add((CMark*)pMark);
				}
				break;
			case CMark::BarcodeMark:	
				{
					CBarcodeMark* pMark = new CBarcodeMark;
					*pMark = *(CBarcodeMark*)rMarkList[i];
					Add((CMark*)pMark);
				}
				break;
			case CMark::DrawingMark:	
				{
					CDrawingMark* pMark = new CDrawingMark;
					*pMark = *(CDrawingMark*)rMarkList[i];
					Add((CMark*)pMark);
				}
				break;
		}
	}

	m_strNotes = rMarkList.m_strNotes;
	m_colorDefTable.RemoveAll();
	m_colorDefTable.Append(rMarkList.m_colorDefTable);
}

void CMarkList::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CMarkList));

	if (ar.IsStoring())
	{
		CObject::Serialize(ar);
		ar.WriteCount(m_nSize);

		ar << m_strNotes;
		m_colorDefTable.Serialize(ar);

		for (int i = 0; i < GetSize(); i++)
		{
			ar << GetAt(i)->m_nType;
			GetAt(i)->Serialize(ar);
		}
	}
	else
	{
		RemoveAll();
		m_colorDefTable.RemoveAll();

		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:  
			{
				CMarkListOld markListOld; 
				markListOld.Serialize(ar); 
				m_strNotes	 = markListOld.m_strNotes;
				POSITION pos = markListOld.GetHeadPosition();
				while (pos)
				{
					CMarkOld& rMarkOld = markListOld.GetNext(pos);

					if (rMarkOld.m_strTextmark.IsEmpty())
					{
						CCustomMark* pMark = new CCustomMark;
						pMark->m_nType				= CMark::CustomMark;
						pMark->m_strMarkName		= rMarkOld.m_strFormatName;
						pMark->m_fMarkWidth			= rMarkOld.m_fMarkWidth;
						pMark->m_fMarkHeight		= rMarkOld.m_fMarkHeight;
						pMark->m_nHeadPosition		= rMarkOld.m_nHeadPosition;
						pMark->m_bAllColors			= rMarkOld.m_bAllColors;
						pMark->m_ColorSeparations.RemoveAll();
						pMark->m_ColorSeparations.Copy(rMarkOld.m_ColorSeparations);
						pMark->m_MarkSource			= rMarkOld.m_MarkSource;

						pMark->m_reflinePosParamsAnchor.m_nReferenceItemInX			= rMarkOld.m_nReferenceItemInX;
						pMark->m_reflinePosParamsAnchor.m_nReferenceItemInY			= rMarkOld.m_nReferenceItemInY;
						pMark->m_reflinePosParamsAnchor.m_nReferenceLineInX			= rMarkOld.m_nReferenceLineInX;
						pMark->m_reflinePosParamsAnchor.m_nReferenceLineInY			= rMarkOld.m_nReferenceLineInY;
						pMark->m_reflinePosParamsAnchor.m_nObjectReferenceLineInX	= XCENTER;
						pMark->m_reflinePosParamsAnchor.m_nObjectReferenceLineInY	= YCENTER;
						pMark->m_reflinePosParamsAnchor.m_fDistanceX				= (rMarkOld.m_nReferenceLineInX == RIGHT) ? -rMarkOld.m_fDistanceX : rMarkOld.m_fDistanceX;
						pMark->m_reflinePosParamsAnchor.m_fDistanceY				= (rMarkOld.m_nReferenceLineInY == TOP  ) ? -rMarkOld.m_fDistanceY : rMarkOld.m_fDistanceY;
																			// Old definition is: positive distance -> move mark inside selected object.
																			// New definition is: positive distance -> always move to right (in X) and up (in Y).
						if ((pMark->m_reflinePosParamsAnchor.m_nReferenceLineInX != LEFT) && (pMark->m_reflinePosParamsAnchor.m_nReferenceLineInX != RIGHT))
						{
							pMark->m_reflinePosParamsAnchor.m_nReferenceItemInX	= pMark->m_reflinePosParamsAnchor.m_nReferenceItemInY; 
							pMark->m_reflinePosParamsAnchor.m_nReferenceLineInX	= XCENTER;
							pMark->m_reflinePosParamsAnchor.m_fDistanceX = 0.0f;
						}
						if ((pMark->m_reflinePosParamsAnchor.m_nReferenceLineInY != BOTTOM) && (pMark->m_reflinePosParamsAnchor.m_nReferenceLineInY != TOP))
						{
							pMark->m_reflinePosParamsAnchor.m_nReferenceItemInY	= pMark->m_reflinePosParamsAnchor.m_nReferenceItemInX; 
							pMark->m_reflinePosParamsAnchor.m_nReferenceLineInY	= YCENTER;
							pMark->m_reflinePosParamsAnchor.m_fDistanceY = 0.0f;
						}
						Add(pMark);	
					}
					else
					{
						CTextMark* pMark = new CTextMark;
						pMark->m_nType				= CMark::TextMark;
						pMark->m_strMarkName		= rMarkOld.m_strFormatName;
						pMark->m_fMarkWidth			= rMarkOld.m_fMarkWidth;
						pMark->m_fMarkHeight		= rMarkOld.m_fMarkHeight;
						pMark->m_nHeadPosition		= rMarkOld.m_nHeadPosition;
						pMark->m_bAllColors			= rMarkOld.m_bAllColors;
						pMark->m_ColorSeparations.RemoveAll();
						pMark->m_ColorSeparations.Copy(rMarkOld.m_ColorSeparations);
						pMark->m_MarkSource			= rMarkOld.m_MarkSource;

						pMark->m_reflinePosParamsAnchor.m_nReferenceItemInX			= rMarkOld.m_nReferenceItemInX;
						pMark->m_reflinePosParamsAnchor.m_nReferenceItemInY			= rMarkOld.m_nReferenceItemInY;
						pMark->m_reflinePosParamsAnchor.m_nReferenceLineInX			= rMarkOld.m_nReferenceLineInX;
						pMark->m_reflinePosParamsAnchor.m_nReferenceLineInY			= rMarkOld.m_nReferenceLineInY;
						pMark->m_reflinePosParamsAnchor.m_nObjectReferenceLineInX	= XCENTER;
						pMark->m_reflinePosParamsAnchor.m_nObjectReferenceLineInY	= YCENTER;
						pMark->m_reflinePosParamsAnchor.m_fDistanceX				= (rMarkOld.m_nReferenceLineInX == RIGHT) ? -rMarkOld.m_fDistanceX : rMarkOld.m_fDistanceX;
						pMark->m_reflinePosParamsAnchor.m_fDistanceY				= (rMarkOld.m_nReferenceLineInY == TOP  ) ? -rMarkOld.m_fDistanceY : rMarkOld.m_fDistanceY;
						if ((pMark->m_reflinePosParamsAnchor.m_nReferenceLineInX != LEFT) && (pMark->m_reflinePosParamsAnchor.m_nReferenceLineInX != RIGHT))
						{
							pMark->m_reflinePosParamsAnchor.m_nReferenceItemInX	= pMark->m_reflinePosParamsAnchor.m_nReferenceItemInY; 
							pMark->m_reflinePosParamsAnchor.m_nReferenceLineInX	= XCENTER;
							pMark->m_reflinePosParamsAnchor.m_fDistanceX = 0.0f;
						}
						if ((pMark->m_reflinePosParamsAnchor.m_nReferenceLineInY != BOTTOM) && (pMark->m_reflinePosParamsAnchor.m_nReferenceLineInY != TOP))
						{
							pMark->m_reflinePosParamsAnchor.m_nReferenceItemInY	= pMark->m_reflinePosParamsAnchor.m_nReferenceItemInX; 
							pMark->m_reflinePosParamsAnchor.m_nReferenceLineInY	= YCENTER;
							pMark->m_reflinePosParamsAnchor.m_fDistanceY = 0.0f;
						}

						pMark->m_strOutputText = rMarkOld.m_strTextmark;

						pMark->m_MarkSource.m_nType = CPageSource::SystemCreated;
						pMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$TEXT %s"), pMark->m_strOutputText);

						Add(pMark);
					}
				}

				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if (pDoc)
					m_colorDefTable.Append(pDoc->m_ColorDefinitionTable);
			}
			break;

		case 2:	
			{
				CObject::Serialize(ar); 
				DWORD nOldSize = ar.ReadCount();
				SetSize(nOldSize, -1);

				ar >> m_strNotes;

				BYTE nType;
				for (int i = 0; i < GetSize(); i++)
				{
					ar >> nType;

					switch (nType)
					{
					case CMark::CustomMark:		
						{
							CCustomMark* pMark = new CCustomMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					case CMark::TextMark:		
						{
							CTextMark* pMark = new CTextMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					case CMark::SectionMark:	
						{
							CSectionMark* pMark = new CSectionMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					case CMark::CropMark:	
						{
							CCropMark* pMark = new CCropMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					}				
				}

				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if (pDoc)
					m_colorDefTable.Append(pDoc->m_ColorDefinitionTable);
			}
			break;

		case 3:	
			{
				CObject::Serialize(ar); 
				DWORD nOldSize = ar.ReadCount();
				SetSize(nOldSize, -1);

				ar >> m_strNotes;
				m_colorDefTable.Serialize(ar);

				BYTE nType;
				for (int i = 0; i < GetSize(); i++)
				{
					ar >> nType;

					switch (nType)
					{
					case CMark::CustomMark:		
						{
							CCustomMark* pMark = new CCustomMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					case CMark::TextMark:		
						{
							CTextMark* pMark = new CTextMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					case CMark::SectionMark:	
						{
							CSectionMark* pMark = new CSectionMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					case CMark::CropMark:	
						{
							CCropMark* pMark = new CCropMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					case CMark::FoldMark:	
						{
							CFoldMark* pMark = new CFoldMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					case CMark::BarcodeMark:	
						{
							CBarcodeMark* pMark = new CBarcodeMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					case CMark::DrawingMark:	
						{
							CDrawingMark* pMark = new CDrawingMark;
							pMark->Serialize(ar);
							SetAt(i, (CMark*)pMark);
						}
						break;
					}
				}
			}
			break;
		}

		if (nVersion < 3)
		{
			for (int i = 0; i < GetSize(); i++)
				GetAt(i)->InitializeTemplate(m_colorDefTable);
		}
	}
}

void CMarkList::RemoveAt(int nIndex, BOOL bDelete)
{
	if (bDelete)
		delete GetAt(nIndex);

	CArray <CMark*, CMark*>::RemoveAt(nIndex);
}

void CMarkList::RemoveAll(BOOL bFreeMemory)
{
	if ( ! GetSize())
		return;

	m_strNotes.Empty();
	m_colorDefTable.RemoveAll();

	while (GetSize())
	{
		if (bFreeMemory)
			delete GetAt(0);
		CArray <CMark*, CMark*>::RemoveAt(0);
	}
}


int CMarkList::GetIndex(CMark* pMark)
{
	for (int i = 0; i < GetSize(); i++)	
	{
		if (pMark == ElementAt(i))
			return i;
	}
	return -1;
}

int CMarkList::FindMarkIndex(const CString& strName)
{
	for (int i = 0; i < GetSize(); i++)	
	{
		if (strName == ElementAt(i)->m_strMarkName)
			return i;
	}
	return -1;
}

CMark* CMarkList::FindMark(const CString& strName)
{
	for (int i = 0; i < GetSize(); i++)	
	{
		if (strName == ElementAt(i)->m_strMarkName)
			return ElementAt(i);
	}
	return NULL;
}


void CMarkList::DrawSheetPreviews(CDC* pDC, CPrintSheet* pPrintSheet, int nSide)
{
	if ( ! pPrintSheet || ! GetSize())
		return;

   	CPrintSheetView*	pView			= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	BOOL				bShowMeasuring	= FALSE;
	BOOL				bShowContent	= FALSE;
	if (pView)
	{
		bShowMeasuring = ( ! pView->ShowBitmap() && ! pView->ShowExposures() && (pView->ViewMode() != ID_VIEW_ALL_SHEETS) && pView->IsOpenDlgLayoutObjectGeometry()) ? TRUE : FALSE;
		bShowContent   = pView->ShowBitmap();
	}

	if ( ! bShowMeasuring && pView)
	{
		pView->m_reflineXSpin.ShowWindow(SW_HIDE);
		pView->m_reflineYSpin.ShowWindow(SW_HIDE);
		pView->m_reflineDistanceEdit.ShowWindow(SW_HIDE);
	}

	CDlgMarkSet* pDlgMarkSet = NULL;
	if (pView)
		if (pView->IsOpenMarkSetDlg())
			if (pView->GetDlgMarkSet()->m_hWnd)
				pDlgMarkSet = pView->GetDlgMarkSet();

	int  nEdge = -1;
	if (pDlgMarkSet)
	{
		if (bShowMeasuring)
		{
			CDlgLayoutObjectGeometry* pDlg = pDlgMarkSet->m_dlgLayoutObjectControl.GetDlgGeometry();
			nEdge = (pDlg) ? pDlg->GetMarkEdge() : 0;
		}
	}

	int  nSel			 = -1;
	BOOL bShowContentSel = FALSE;
	if (pDlgMarkSet)
	{
		nSel = pDlgMarkSet->m_nCurMarkSel;
		if ( ! pDlgMarkSet->m_dlgLayoutObjectControl.m_hWnd)
			bShowContentSel = TRUE;
		else
			if ( ! pDlgMarkSet->m_dlgLayoutObjectControl.IsWindowVisible())
				bShowContentSel = TRUE;
			else
				bShowContentSel = (pView) ? ( (pView->IsOpenDlgLayoutObjectGeometry()) ? FALSE : TRUE) : FALSE;
	}
	else
		bShowContentSel = (pView) ? ( (pView->IsOpenDlgLayoutObjectGeometry()) ? FALSE : TRUE) : FALSE;

	for (int i = GetSize() - 1; i >= 0; i--)
	{
		if (GetAt(i))
			if (i != nSel)			// don't draw current selected mark here
				GetAt(i)->DrawSheetPreview(pDC, pPrintSheet, nSide, this, bShowContent);
			else
				GetAt(i)->DrawSheetPreview(pDC, pPrintSheet, nSide, this, bShowContentSel, nEdge, TRUE);
	}
}


void CMarkList::GetBBoxAll(CPrintSheet* pPrintSheet, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop)
{
	float fMarkBBoxLeft, fMarkBBoxBottom, fMarkBBoxRight, fMarkBBoxTop;
	fBBoxLeft = FLT_MAX; fBBoxBottom = FLT_MAX; fBBoxRight = -FLT_MAX; fBBoxTop = -FLT_MAX;
	for (int i = 0; i < GetSize(); i++)
	{
		ElementAt(i)->GetBBoxAll(pPrintSheet, nSide, fMarkBBoxLeft, fMarkBBoxBottom, fMarkBBoxRight, fMarkBBoxTop);
		if ( (fabs(fMarkBBoxLeft) == FLT_MAX) || (fabs(fMarkBBoxBottom) == FLT_MAX) || (fabs(fMarkBBoxRight) == FLT_MAX) || (fabs(fMarkBBoxTop) == FLT_MAX) )
			continue;	// mark cannot be placed

		fBBoxLeft	= min(fBBoxLeft,	fMarkBBoxLeft);	
		fBBoxBottom = min(fBBoxBottom,	fMarkBBoxBottom);
		fBBoxRight	= max(fBBoxRight,   fMarkBBoxRight);
		fBBoxTop	= max(fBBoxTop,		fMarkBBoxTop);
	}
}

void CMarkList::Sort()
{
	qsort((void*)GetData(), GetSize(), sizeof(CMark*), CompareMarks);
}

int CMarkList::CompareMarks(const void *p1, const void *p2)
{
	CMark* pMark1 = *(CMark**)p1;
	CMark* pMark2 = *(CMark**)p2;

	if ( ! pMark1 || ! pMark2)
		return 0;

	if ( (pMark1->m_nZOrderIndex >= 0) && (pMark2->m_nZOrderIndex >= 0) )
	{
		if (pMark1->m_nZOrderIndex  < pMark2->m_nZOrderIndex)
			return -1;
		else
			if (pMark1->m_nZOrderIndex  > pMark2->m_nZOrderIndex)
				return 1;
			else
				return 0;
	}
	
	BOOL bSameType = FALSE;
	if (pMark1->m_nType == CMark::CustomMark)
		if (pMark2->m_nType == CMark::CustomMark)
			bSameType = TRUE;
		else
			return -1;

	if ( ! bSameType)
		if (pMark1->m_nType == CMark::TextMark)
			if (pMark2->m_nType == CMark::TextMark)
				bSameType = TRUE;
			else
				if (pMark2->m_nType == CMark::CustomMark)
					return 1;
				else
					return -1;

	if ( ! bSameType)
		if (pMark1->m_nType == CMark::SectionMark)
			if (pMark2->m_nType == CMark::SectionMark)
				bSameType = TRUE;
			else
				if ( (pMark2->m_nType == CMark::CustomMark) || (pMark2->m_nType == CMark::TextMark) )
					return 1;
				else
					return -1;

	if ( ! bSameType)
		if (pMark1->m_nType == CMark::CropMark)
			if (pMark2->m_nType == CMark::CropMark)
				bSameType = TRUE;
			else
				if ( (pMark2->m_nType == CMark::CustomMark) || (pMark2->m_nType == CMark::TextMark) || (pMark2->m_nType == CMark::SectionMark) )
					return 1;
				else
					return -1;

	if ( ! bSameType)
		if (pMark1->m_nType == CMark::FoldMark)
			if (pMark2->m_nType == CMark::FoldMark)
				bSameType = TRUE;
			else
				if ( (pMark2->m_nType == CMark::CustomMark) || (pMark2->m_nType == CMark::TextMark) || (pMark2->m_nType == CMark::SectionMark) || (pMark2->m_nType == CMark::CropMark) )
					return 1;
				else
					return -1;

	if ( ! bSameType)
		if (pMark1->m_nType == CMark::BarcodeMark)
			if (pMark2->m_nType == CMark::BarcodeMark)
				bSameType = TRUE;
			else
				if ( (pMark2->m_nType == CMark::CustomMark) || (pMark2->m_nType == CMark::TextMark) || (pMark2->m_nType == CMark::SectionMark) || (pMark2->m_nType == CMark::CropMark)  || (pMark2->m_nType == CMark::FoldMark) )
					return 1;
				else
					return -1;

	if ( ! bSameType)
		if (pMark1->m_nType == CMark::DrawingMark)
			if (pMark2->m_nType == CMark::DrawingMark)
				bSameType = TRUE;
			else
				if ( (pMark2->m_nType == CMark::CustomMark) || (pMark2->m_nType == CMark::TextMark) || (pMark2->m_nType == CMark::SectionMark) || (pMark2->m_nType == CMark::CropMark)  || (pMark2->m_nType == CMark::FoldMark) || (pMark2->m_nType == CMark::BarcodeMark) )
					return 1;
				else
					return -1;

	// if same type, compare names
	if (pMark1->m_strMarkName.CompareNoCase(pMark2->m_strMarkName) < 0)
		return -1;
	else
		if (pMark1->m_strMarkName.CompareNoCase(pMark2->m_strMarkName) > 0)
			return 1;
		else
			return 0;
}

CMark* CMarkList::GetFirstMark(int nMarkType)
{
	for (int i = 0; i < GetSize(); i++)
		if (GetAt(i)->m_nType == nMarkType)
			return GetAt(i);

	return NULL;
}

CString CMarkList::FindFreeMarkName(CString strNewMark)
{
	strNewMark = CMark::SplitNewMarkName(strNewMark);
	CString strFinal = strNewMark;
	int nCount = 1;
	int i	   = 0;
	while (i < GetSize())
	{
		if (strFinal == GetAt(i)->m_strMarkName)	// mark name exists
		{
			strFinal.Format(_T("%s (%d)"), strNewMark, nCount++);
			i = 0;
		}
		else
			i++;
	}

	return strFinal;
}

BOOL CMarkList::Load(const CString& strFullPath)
{
	CFile			markDefFile;
	CFileException  fileException;

	if (!markDefFile.Open(strFullPath, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return FALSE; 
	}

	CArchive archive(&markDefFile, CArchive::load);

	RemoveAll();		
	Serialize(archive);

	archive.Close();

	markDefFile.Close();

	return TRUE;
}

BOOL CMarkList::Save(const CString& strFullPath)
{
	CFile			markDefFile;
	CFileException  fileException;

	if (!markDefFile.Open(strFullPath, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return FALSE; 
	}

	CArchive archive(&markDefFile, CArchive::store);

	Serialize(archive);

	archive.Close();

	markDefFile.Close();

	return TRUE;
}

BOOL CMarkList::HasMarks(int nMarkType)
{
	for (int i = 0; i < GetSize(); i++)
	{
		switch (nMarkType)
		{
		case CMark::CropMark:		if (GetAt(i)->m_nType == CMark::CropMark)
										return TRUE;
									break;
		case CMark::SectionMark:	if (GetAt(i)->m_nType == CMark::SectionMark)
										return TRUE;
									break;
		case CMark::FoldMark:		if (GetAt(i)->m_nType == CMark::FoldMark)
										return TRUE;
									break;
		case CMark::CustomMark:		if (GetAt(i)->m_nType == CMark::CustomMark)
										return TRUE;
									break;
		case CMark::TextMark:		if (GetAt(i)->m_nType == CMark::TextMark)
										return TRUE;
									break;
		case CMark::BarcodeMark:	if (GetAt(i)->m_nType == CMark::BarcodeMark)
										return TRUE;
									break;
		case CMark::DrawingMark:	if (GetAt(i)->m_nType == CMark::DrawingMark)
										return TRUE;
									break;
		}
	}
	return FALSE;
}

void CMarkList::AddMark(CMark* pMark)
{
	if ( ! pMark)
		return;

	switch (pMark->m_nType)
	{
	case CMark::CustomMark:		
		{
			CCustomMark* pNewMark = new CCustomMark;
			*pNewMark = *(CCustomMark*)pMark;
			Add((CMark*)pNewMark);
		}
		break;
	case CMark::TextMark:		
		{
			CTextMark* pNewMark = new CTextMark;
			*pNewMark = *(CTextMark*)pMark;
			Add((CMark*)pNewMark);
		}
		break;
	case CMark::SectionMark:	
		{
			CSectionMark* pNewMark = new CSectionMark;
			*pNewMark = *(CSectionMark*)pMark;
			Add((CMark*)pNewMark);
		}
		break;
	case CMark::CropMark:	
		{
			CCropMark* pNewMark = new CCropMark;
			*pNewMark = *(CCropMark*)pMark;
			Add((CMark*)pNewMark);
		}
		break;
	case CMark::FoldMark:	
		{
			CFoldMark* pNewMark = new CFoldMark;
			*pNewMark = *(CFoldMark*)pMark;
			Add((CMark*)pNewMark);
		}
		break;
	case CMark::BarcodeMark:		
		{
			CBarcodeMark* pNewMark = new CBarcodeMark;
			*pNewMark = *(CBarcodeMark*)pMark;
			Add((CMark*)pNewMark);
		}
		break;
	case CMark::DrawingMark:		
		{
			CDrawingMark* pNewMark = new CDrawingMark;
			*pNewMark = *(CDrawingMark*)pMark;
			Add((CMark*)pNewMark);
		}
		break;
	}
}




///////////////////////////////////////////////////////////////

CString CMark::SplitNewMarkName(CString strNewMark)
{
	if (strNewMark.IsEmpty())
		return "";
	if (strNewMark.GetLength() < 3)
		return strNewMark;
	if (strNewMark[strNewMark.GetLength() - 1] != ')' )
		return strNewMark;
	if ( ! isdigit(strNewMark[strNewMark.GetLength() - 2]) )
		return strNewMark;
	if ( ! isdigit(strNewMark[strNewMark.GetLength() - 3]) )
		if (strNewMark[strNewMark.GetLength() - 3] != '(' )
			return strNewMark;
		else
			;
	else
		if (strNewMark[strNewMark.GetLength() - 4] != '(' )
			return strNewMark;

	int nIndex = strNewMark.ReverseFind('(');
	if (nIndex == -1)
		return strNewMark;

	CString strLeft = strNewMark.Left(nIndex);
	strLeft.TrimRight();
	return strLeft;
}

void CMark::HighlightMark(CDC* pDC, CRect markRect, CPrintSheet* pPrintSheet)
{
	BOOL bUnassigned = TRUE;
   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)	
	{
		CDlgLayoutObjectProperties* pDlg = pView->GetDlgLayoutObjectProperties();
		if (pDlg)
			if (pDlg->m_bModeModify)
				bUnassigned = FALSE;
	}

	CSize sizeInflate(5, 5); pDC->DPtoLP(&sizeInflate);
	CRect rcHighlight = markRect; rcHighlight.InflateRect(sizeInflate.cx, 0); rcHighlight.top += sizeInflate.cy; rcHighlight.bottom -= sizeInflate.cy;
	if (bUnassigned)
		CPrintSheetView::FillTransparent (pDC, rcHighlight, MARKCOLOR_HIGHLIGHT, TRUE, 80);
	else
		CPrintSheetView::FillTransparent (pDC, rcHighlight, BORDEAUX, TRUE, 50);

	if (pView)
	{
		MARKOBJECTINFO moInfo = {rcHighlight, TRUE, NULL, this, pPrintSheet};
		pView->m_markObjectInfoList.Add(moInfo);
	}

	if ( ! pView->IsOpenDlgLayoutObjectGeometry())
	{
		CPen pen(PS_SOLID, 1, (bUnassigned) ? MARKFRAMECOLOR_HIGHLIGHT : BORDEAUX);	
		CPen* pOldPen = pDC->SelectObject(&pen);
		pDC->FillSolidRect(markRect, WHITE);
		pDC->SelectObject(pOldPen);
		pen.DeleteObject();
	}
}

CColorDefinitionTable& CMark::GetActColorDefTable(CMarkList* pMarkList)
{
	BOOL			 bUnassigned = TRUE;
   	CPrintSheetView* pView		 = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   	if (pView)
	{
		CDlgLayoutObjectProperties* pDlg = pView->GetDlgLayoutObjectProperties();
		if (pDlg)
			if (pDlg->m_bModeModify)
				bUnassigned = FALSE;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	return (pMarkList) ? pMarkList->m_colorDefTable : ((bUnassigned || ! pDoc) ? theApp.m_colorDefTable : pDoc->m_ColorDefinitionTable);
}


////////// CMarkInstanceDefs //////////////////////////////////////////////


IMPLEMENT_SERIAL(CMarkInstanceDefs, CObject, VERSIONABLE_SCHEMA | 1)

CMarkInstanceDefs::CMarkInstanceDefs()
{
}

CMarkInstanceDefs::CMarkInstanceDefs(const CMarkInstanceDefs& rMarkInstanceDefs)
{
	Copy(rMarkInstanceDefs);
}

const CMarkInstanceDefs& CMarkInstanceDefs::operator= (const CMarkInstanceDefs& rMarkInstanceDefs)
{
	Copy(rMarkInstanceDefs);
	return *this;
}

void CMarkInstanceDefs::Copy(const CMarkInstanceDefs& rMarkInstanceDefs)
{
	m_nType			= rMarkInstanceDefs.m_nType;
	m_nPosition		= rMarkInstanceDefs.m_nPosition;
	m_nOrientation	= rMarkInstanceDefs.m_nOrientation;
	m_nAction		= rMarkInstanceDefs.m_nAction;
	m_strParentMark	= rMarkInstanceDefs.m_strParentMark;
}

// helper function for CMarkInstanceDefs elements
template <> void AFXAPI SerializeElements <CMarkInstanceDefs> (CArchive& ar, CMarkInstanceDefs* pMarkInstanceDefs, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CMarkInstanceDefs));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pMarkInstanceDefs->m_nType;		
			ar << pMarkInstanceDefs->m_nPosition;	
			ar << pMarkInstanceDefs->m_nOrientation;
			ar << pMarkInstanceDefs->m_nAction;
			ar << pMarkInstanceDefs->m_strParentMark;
		}											
		else																		 
		{
			switch (nVersion)
			{
			case 1:
				ar >> pMarkInstanceDefs->m_nType;		
				ar >> pMarkInstanceDefs->m_nPosition;	
				ar >> pMarkInstanceDefs->m_nOrientation;
				ar >> pMarkInstanceDefs->m_nAction;
				ar >> pMarkInstanceDefs->m_strParentMark;
				break;
			}
		}
		pMarkInstanceDefs++;
	}
}

/////////////////////////////////////////////////////////////////////////////


////////// CMarkBuddyLink //////////////////////////////////////////////


IMPLEMENT_SERIAL(CMarkBuddyLink, CObject, VERSIONABLE_SCHEMA | 1)

CMarkBuddyLink::CMarkBuddyLink()
{
	m_nLinkedObjectIndex		= -1;
	m_nLinkedObjectPosition		= -1;		
	m_nLinkedObjectOrientation	= -1;		
}

CMarkBuddyLink::CMarkBuddyLink(int nIndex, int nPosition, int nOrientation)
{
	m_nLinkedObjectIndex		= nIndex;
	m_nLinkedObjectPosition		= nPosition;		
	m_nLinkedObjectOrientation	= nOrientation;		
}

const CMarkBuddyLink& CMarkBuddyLink::operator= (const CMarkBuddyLink& rMarkBuddyLink)
{
	Copy(rMarkBuddyLink);
	return *this;
}

void CMarkBuddyLink::Copy(const CMarkBuddyLink& rMarkBuddyLink)
{
	m_nLinkedObjectIndex		= rMarkBuddyLink.m_nLinkedObjectIndex;
	m_nLinkedObjectPosition		= rMarkBuddyLink.m_nLinkedObjectPosition;		
	m_nLinkedObjectOrientation	= rMarkBuddyLink.m_nLinkedObjectOrientation;		
}

void CMarkBuddyLink::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CMarkBuddyLink));

	if (ar.IsStoring())
	{
		ar << m_nLinkedObjectIndex;		
		ar << m_nLinkedObjectPosition;	
		ar << m_nLinkedObjectOrientation;
	}											
	else																		 
	{
		UINT nVersion;
		nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> m_nLinkedObjectIndex;		
			ar >> m_nLinkedObjectPosition;	
			ar >> m_nLinkedObjectOrientation;
			break;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////






