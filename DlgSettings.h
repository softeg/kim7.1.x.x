// DlgSettings.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSettings:
// Global object filled with programsettings from ini-file

class CSettings : public CObject
{
// Attributes
public:
	TCHAR m_szWinDir[_MAX_PATH];	
	TCHAR m_szWorkDir[_MAX_PATH];
	TCHAR m_szDataDir[_MAX_PATH];
	TCHAR m_szJobsDir[_MAX_PATH];
	TCHAR m_szSchemeDir[_MAX_PATH];
	TCHAR m_szPSDir[_MAX_PATH];
	TCHAR m_szEPSDir[_MAX_PATH];
	TCHAR m_szEPSDirDisabled[16];		BOOL m_bEPSDirDisabled;
	TCHAR m_szPDFInputFolder[_MAX_PATH];
	TCHAR m_szPDFPreviewFolder[_MAX_PATH];
	BOOL  m_bPDFPreviewsOrigPlace;
	TCHAR m_szMarksDir[_MAX_PATH];
	TCHAR m_szStandardsDir[_MAX_PATH];
	TCHAR m_szTempDir[_MAX_PATH];
	TCHAR m_szLanguage[16];				int m_iLanguage;
	TCHAR m_szUnit[11];					int m_iUnit;
	TCHAR m_szSheetsPerPage[16];		int m_nSheetsPerPage;
	TCHAR m_szFrontName[16];
	TCHAR m_szBackName[16];
	TCHAR m_szLayoutName[16];
	TCHAR m_szFoldSheetName[16];
	TCHAR m_szCheckForProductType[16];	BOOL m_bCheckForProductType;
	TCHAR m_szWorkAndTurnName[16];
	TCHAR m_szWorkAndTumbleName[16];
	TCHAR m_szSingleSidedName[16];
	TCHAR m_szFrontRearParallel[16];	BOOL m_bFrontRearParallel;
	TCHAR m_szOnStartFileNew[16];		BOOL m_bOnStartFileNew;
	TCHAR m_szOnStartDelTemp[16];		BOOL m_bOnStartDelTemp;
	TCHAR m_szAutoBackup[16];			BOOL m_bAutoBackup;
	TCHAR m_szAutoBackupTime[16];		int  m_iAutoBackupTime;
	TCHAR m_szRipBitmapGenerate[16];	BOOL m_bRipBitmapGenerate;
	TCHAR m_szRipBitmapColorDepth[16];	int	 m_iRipBitmapColorDepth;
	TCHAR m_szRipBitmapResolution[16];	int	 m_iRipBitmapResolution;
	TCHAR m_szRipBBoxDSC[16];			int  m_iRipBBoxDSC;
	TCHAR m_szLastPageFormat[256];
	TCHAR m_szLastBindingStyle[256];
	TCHAR m_szLastProductionStyle[256];
	BOOL  m_bAutoCheckDoublePages;
	BOOL  m_bAutoWatchPageSources;
	BOOL  m_bTrimboxPagesizeMismatchWarning;
	BOOL  m_bExtendedColorHandling;
	float m_fDefaultBleed;
	TCHAR m_szAutoServerNameIP[_MAX_PATH];
	TCHAR m_szSPrintOneTenantID[_MAX_PATH];
	TCHAR m_szSPrintOneUserID[_MAX_PATH];
	TCHAR m_szSPrintOnePassword[_MAX_PATH];

// Implementation
public:	
	BOOL OnLoadingSettings();
	//BOOL OnUpdateSettings(int m_iLanguage, int m_iUnit, char* m_szWorkDir, char* m_szDataDir, char* m_szJobsDir);
};

#ifndef __DLGSETTINGS_H__
#define __DLGSETTINGS_H__

/////////////////////////////////////////////////////////////////////////////
// CSettingsPage1 dialog

class CSettingsPage1 : public CPropertyPage
{
	DECLARE_DYNCREATE(CSettingsPage1)

// Construction
public:
	CSettingsPage1();
	~CSettingsPage1();

// Dialog Data
	//{{AFX_DATA(CSettingsPage1)
	enum { IDD = IDD_SETTINGS_PAGE1 };
	CString	m_strBackName;
	CString	m_strFrontName;
	CString	m_strFoldSheetTypeName;
	CString	m_strLayoutName;
	CString	m_strWorkAndTumbleName;
	CString	m_strWorkAndTurnName;
	CString	m_strSingleSidedName;
	BOOL	m_bCheckForProductType;
	float	m_fDefaultBleed;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSettingsPage1)
	public:
	virtual void OnOK();
	virtual BOOL OnWizardFinish();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CToolTipCtrl m_tooltip;

	// Generated message map functions
	//{{AFX_MSG(CSettingsPage1)
	virtual BOOL OnInitDialog();
	afx_msg void OnCheckProducttype();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};


/////////////////////////////////////////////////////////////////////////////
// CSettingsPage2 dialog

class CSettingsPage2 : public CPropertyPage
{
	DECLARE_DYNCREATE(CSettingsPage2)

// Construction
public:
	CSettingsPage2();
	~CSettingsPage2();

// Dialog Data
	//{{AFX_DATA(CSettingsPage2)
	enum { IDD = IDD_SETTINGS_PAGE2 };
	CString	m_strDataDir;
	CString	m_strJobsDir;
	CString	m_strWorkDir;
	CString	m_strSchemeDir;
	CString	m_strPSDir;
	CString	m_strTempDir;
	CString	m_strStandardsDir;
	CString	m_strMarksDir;
	CString	m_strEPSDir;
	BOOL	m_bEPSDirCheck;
	CString	m_strPDFInputFolder;
	CString	m_strPDFPreviewFolder;
	BOOL	m_bPDFPreviewsOrigPlace;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSettingsPage2)
	public:
	virtual void OnOK();
	virtual BOOL OnWizardFinish();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSettingsPage2)
	virtual BOOL OnInitDialog();
	afx_msg void OnBrowse1();
	afx_msg void OnBrowse2();
	afx_msg void OnBrowse3();
	afx_msg void OnBrowse4();
	afx_msg void OnBrowse5();
	afx_msg void OnBrowse6();
	afx_msg void OnBrowse7();
	afx_msg void OnBrowse8();
	afx_msg void OnEpsdirCheck();
	afx_msg void OnPreviewFolderCheck();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CString OnBrowseDirectories(LPCTSTR lpstrInitialDir);
};


/////////////////////////////////////////////////////////////////////////////
// CSettingsPage3 dialog

class CSettingsPage3 : public CPropertyPage
{
	DECLARE_DYNCREATE(CSettingsPage3)

// Construction
public:
	CSettingsPage3();
	~CSettingsPage3();

// Dialog Data
	//{{AFX_DATA(CSettingsPage3)
	enum { IDD = IDD_SETTINGS_PAGE3 };
	CSpinButtonCtrl	m_PrintSheetPerPageSpin;
	int		m_iLanguage;
	int		m_iUnit;
	CString m_strAutoServerNameIP;
	CString m_strSPrintOneTenantID;
	CString m_strSPrintOneUserID;
	CString m_strSPrintOnePassword;
	int		m_nSheetsPerPage;
	//}}AFX_DATA
#ifdef CTP
	BOOL	m_bExtendedColorHandling;
#endif


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSettingsPage3)
	public:
	virtual void OnOK();
	virtual BOOL OnWizardFinish();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSettingsPage3)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CSettingsPage4 dialog

class CSettingsPage4 : public CPropertyPage
{
	DECLARE_DYNCREATE(CSettingsPage4)

// Construction
public:
	CSettingsPage4();
	~CSettingsPage4();

// Dialog Data
	//{{AFX_DATA(CSettingsPage4)
	enum { IDD = IDD_SETTINGS_PAGE4 };
	CSpinButtonCtrl	m_AutoBackupSpin;
	BOOL	m_bFrontRearParallel;
	BOOL	m_bOnStartFileNew;
	BOOL	m_bOnStartDelTemp;
	BOOL	m_bRipBitmapGenerate;
	int		m_iRipBBoxDSC;
	UINT	m_nAutoBackupTime;
	BOOL	m_bAutoBackup;
	BOOL	m_bAutoCheckDoublePages;
	BOOL	m_bAutoWatchPageSources;
	BOOL	m_bTrimboxPagesizeMismatchWarning;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSettingsPage4)
	public:
	virtual void OnOK();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSettingsPage4)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////
// This class defines custom modal property sheet 
// CSettingsSheet.

class CSettingsSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CSettingsSheet)

// Construction
public:
	CSettingsSheet(CWnd* pParentWnd = NULL);

// Attributes
public:
	CSettingsPage1 m_Page1;
	CSettingsPage2 m_Page2;
	CSettingsPage3 m_Page3;
	CSettingsPage4 m_Page4;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSettingsSheet)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSettingsSheet();
	virtual BOOL OnInitDialog();

// Generated message map functions
protected:
	//{{AFX_MSG(CSettingsSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif // __DLGSETTINGS_H__

