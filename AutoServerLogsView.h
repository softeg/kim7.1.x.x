#pragma once


#include "OwnerDrawnButtonList.h"



class CJobMessageLog : public CObject
{
public:
	CJobMessageLog();
	CJobMessageLog(CJobMessageLog& rJobMessageLog);

public:
	int		m_nMsgType;
	CString	m_strLogDateTime;
	CString	m_strMessage;

public:
	const CJobMessageLog& operator=(const CJobMessageLog& rJobMessageLog);
};


class CJobMessageReport : public CArray <CJobMessageLog, CJobMessageLog&>
{
public:
	void ReadFile(int nJobID);
};


// CAutoServerLogsView-Ansicht

class CAutoServerLogsView : public CScrollView
{
	DECLARE_DYNCREATE(CAutoServerLogsView)


public:
	DECLARE_DISPLAY_LIST(CAutoServerLogsView, OnMultiSelect, NO, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CAutoServerLogsView, MessageLog, OnSelect, NO,  OnDeselect, NO,  OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)


protected:
	CAutoServerLogsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CAutoServerLogsView();

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

enum displayColIDs {ColLogType, ColLogDate, ColLogMessage, ColLogJob};
typedef struct displayColumn
{
	int nID;
	int nWidth;
};
typedef CArray <struct displayColumn, struct displayColumn&> DISPLAY_COLUMN_LIST;

public:
	DISPLAY_COLUMN_LIST	m_displayColumns;


public:
	CSize					m_docSize;
	CHeaderCtrl  			m_HeaderCtrl;
	int			 			m_nHeaderHeight;
	int						m_nHeaderItemsTotalWidth;
	CFont		 			m_headerFont;
	CArray <int, int>		m_headerColumnWidths;
	COwnerDrawnButtonList	m_toolBar;
	int						m_nCurrentView;


public:
	void	SetHeaderColumnWidth(int nIndex, int nValue);
	int		GetHeaderColumnWidth(int nIndex, int nDefault);
	int		GetActJobID();
	CSize	CalcExtents();
	CRect	Draw(CDC* pDC, CDisplayList* pDisplayList);
	CRect	DrawLogsTable(CDC* pDC, CDisplayList* pDisplayList = NULL);
	CRect	DrawLogType		(CDC* pDC, CRect rcFrame, int		  nMsgType,		int nMsgID, CDisplayList* pDisplayList);
	CRect	DrawLogDate		(CDC* pDC, CRect rcFrame, SYSTEMTIME& timeStamp,				CDisplayList* pDisplayList);
	CRect	DrawLogMessage	(CDC* pDC, CRect rcFrame, CString	  strMessage,				CDisplayList* pDisplayList);
	CRect	DrawLogJob		(CDC* pDC, CRect rcFrame, CString	  strJobname,				CDisplayList* pDisplayList);
	void	ResetDisplayColums();
	void	AddDisplayColumn(int nColID, int nWidth);
	void	ClipHeader(CDC* pDC);



	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();     // Erster Aufruf nach Erstellung
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void ScrollToPosition(POINT pt);
	virtual void ScrollToDevicePosition(POINT ptDev);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
protected:
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnHeaderTrack	 (NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


