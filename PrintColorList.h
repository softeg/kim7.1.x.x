#if !defined(AFX_PRINTCOLORLIST_H__6FDA76BA_F4C1_4EDE_B67A_C0796D14B9F7__INCLUDED_)
#define AFX_PRINTCOLORLIST_H__6FDA76BA_F4C1_4EDE_B67A_C0796D14B9F7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintColorList.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// CPrintColorListItem - custom CListCtrl - containing color control informations

class CPrintColorListItem
{
public:
	enum state{ Enabled = 1, Disabled = 2, Remapped = 4};

public:
	CPrintColorListItem() {m_pObjectSourceHeader = NULL; m_pSepInfo = NULL;};
	CPrintColorListItem(int nPageIndex, COLORREF crRGB, CString strColorName, CPageSourceHeader* pObjectSourceHeader, SEPARATIONINFO* pSepInfo, int nState = Enabled, int nMapSpotColorIndex = -1)
	{
		m_nPageIndex = nPageIndex; m_crRGB = crRGB; m_strColorName = strColorName; 
		m_pObjectSourceHeader = pObjectSourceHeader;
		m_pSepInfo			  = pSepInfo;
		m_nState			  = nState;
		m_nMapSpotColorIndex  = nMapSpotColorIndex;
	}


//Attributes:
public:
	int					m_nPageIndex;
	COLORREF			m_crRGB;
	CString				m_strColorName;
	int					m_nState;
	int					m_nMapSpotColorIndex;
	CPageSourceHeader*	m_pObjectSourceHeader;
	SEPARATIONINFO*		m_pSepInfo;
};



/////////////////////////////////////////////////////////////////////////////
// Fenster CPrintColorList 

class CPrintColorList : public CListCtrl
{
// Konstruktion
public:
	CPrintColorList();

// Attribute
public:
	CArray <CPrintColorListItem, 
			CPrintColorListItem&>	m_printColorListItems;
	CImageList						m_imageList;


enum{ ColorItemHeight = 20 };

// Operationen
public:
	void Initialize(CWnd* pParent);
	void AddComposite(CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex);
	void AddBlack	 (CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex);
	void AnalyseData (CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex);
	void LoadData	 (CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex);
	void SaveData	 (CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex);
	void AddColorItem(int nPageNumInDoc, COLORREF color, CString& strName, CPageSourceHeader* pObjectSourceHeader = NULL, SEPARATIONINFO* pSepInfo = NULL, int nState = CPrintColorListItem::Enabled,
					  int nMapSpotColorIndex = -1);
	CPrintColorListItem* GetColorItem(const CString& strName);

//	void DeleteColorItem(int nIndex);

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPrintColorList)
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~CPrintColorList();
//	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMIS);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
	virtual void PostNcDestroy();

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CPrintColorList)
		// HINWEIS - Der Klassen-Assistent fügt hier Member-Funktionen ein und entfernt diese.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


class CPDFObjectPrintColorList : public CPrintColorList
{

// Operations
public:
	virtual void Initialize(CWnd* pParent);
	virtual void LoadData(CPageTemplate* pTemplate, CColorDefinitionTable* pColorDefTable);
	virtual void LoadData(CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable);
	virtual void SaveData(CPageTemplate* pTemplate, CPageSource* pObjectSource);

// Implementation
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_PRINTCOLORLIST_H__6FDA76BA_F4C1_4EDE_B67A_C0796D14B9F7__INCLUDED_

