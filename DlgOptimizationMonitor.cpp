// DlgOptimizationMonitor.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgOptimizationMonitor.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetTableView.h"	
#include "PrintSheetView.h"
#include "PrintSheetPlanningView.h"	
#include "PrintSheetViewStrippingData.h"
#include "ProdDataPrintToolsView.h"	
#include "PrintSheetNavigationView.h"	
#include "ProductsView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "DlgSheet.h"
#include "KimSocketsCommon.h"
#include "KimOptimizer.h"
#include "sPrintOneOptimizer.h"



// CDlgOptimizationMonitor-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgOptimizationMonitor, CDialog)

CDlgOptimizationMonitor::CDlgOptimizationMonitor(CWnd* pParent /*=NULL*/, BOOL bRunModal /* = FALSE*/)
	: CDialog(CDlgOptimizationMonitor::IDD, pParent)
{
	m_strCurrentSheet = _T("");
	m_strCurrentPaper = _T("");
	m_nProgress		  = 0;
	m_bEscapePressed  = FALSE;
	if ( (strlen(theApp.settings.m_szSPrintOneTenantID) <= 0) || (strlen(theApp.settings.m_szSPrintOneUserID) <= 0) || (strlen(theApp.settings.m_szSPrintOnePassword) <= 0) ) 
		m_bLocked	  = FALSE;
	else
		m_bLocked	  = TRUE;
	m_bRunModal		  = bRunModal;
}

CDlgOptimizationMonitor::~CDlgOptimizationMonitor()
{
}

void CDlgOptimizationMonitor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_OPTIMIZATION_PROGRESS, m_progressBar);
	DDX_Text(pDX, IDC_OPTIMIZATION_SHEET_STATIC, m_strCurrentSheet);
	DDX_Text(pDX, IDC_OPTIMIZATION_PAPER_STATIC, m_strCurrentPaper);
	DDX_Control(pDX, IDC_OPTIMIZATION_REPORT_LIST, m_reportList);
}


BEGIN_MESSAGE_MAP(CDlgOptimizationMonitor, CDialog)
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDOK, &CDlgOptimizationMonitor::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgOptimizationMonitor-Meldungshandler

BOOL CDlgOptimizationMonitor::OnInitDialog()
{
	CDialog::OnInitDialog();

	int nNumTotalPages = 0;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if  (pDoc)
	{
		POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
		while (pos)
		{
			CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
			for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
			{
				nNumTotalPages += rBoundProduct.m_parts[i].m_nNumPages;
			}
		}
		nNumTotalPages += pDoc->m_flatProducts.GetSize();
	}

	m_progressBar.SetRange(0, nNumTotalPages);
	m_progressBar.SetPos(0);

	m_reportList.InsertColumn(0, _T("Meldung"), 0, 200);
	m_reportList.InsertColumn(1, _T(""),		0, 300);

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgOptimizationMonitor::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
			if (m_hWnd)
				if ( ! m_bRunModal)
				{
					m_bLocked = TRUE;
					DestroyWindow();
				}
				else
					OnCancel();
	}
	//else
	//	m_bLocked = FALSE;
}

void CDlgOptimizationMonitor::InitData()
{
	m_strCurrentSheet = _T("");
	m_strCurrentPaper = _T("");

	UpdateData(FALSE);
}

void CDlgOptimizationMonitor::SetProgress(int nProgress)
{
	m_progressBar.SetPos(nProgress);
}

void CDlgOptimizationMonitor::OptimizeAll()
{
	if ( (strlen(theApp.settings.m_szSPrintOneTenantID) <= 0) || (strlen(theApp.settings.m_szSPrintOneUserID) <= 0) || (strlen(theApp.settings.m_szSPrintOnePassword) <= 0) ) 
		KimOptimizeAll();
	else
		sPrintOneOptimizeAll();
}

void CDlgOptimizationMonitor::sPrintOneOptimizeAll()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	GetDlgItem(IDOK)->EnableWindow(FALSE);
	GetDlgItem(IDCANCEL)->EnableWindow(TRUE);

	theApp.UndoSave();
 
	UpdateData(TRUE);

	pDoc->m_printGroupList.UnlockGroups();

	CsPrintOneOptimizer sPrintOneOptimizer(this);
	sPrintOneOptimizer.DoOptimizeAll();

	pDoc->SetModifiedFlag(TRUE);

	pDoc->m_Bookblock.Reorganize();

	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	pDoc->m_PrintSheetList.InitializeFlatProductTypeLists();

	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();

	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();	
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	CombineSimilarLayouts();

	pDoc->SetModifiedFlag(TRUE);
//	pDoc->m_printGroupList.Analyze();

	theApp.UpdateView(RUNTIME_CLASS(CProductsView), TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView), TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView), TRUE);

	GetDlgItem(IDOK)->EnableWindow(TRUE);
	GetDlgItem(IDCANCEL)->EnableWindow(FALSE);

	//EndDialog(IDOK);
}

void CDlgOptimizationMonitor::KimOptimizeAll()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	theApp.UndoSave();
 
	UpdateData(TRUE);

	pDoc->m_printGroupList.UnlockGroups();

	CPrintGroup*			pPrintGroup	  = pDoc->m_printGroupList.FindFirstUnassigned();
	CBoundProductPart*		pProductPart  = (pPrintGroup) ? pPrintGroup->GetFirstBoundProductPart() : NULL;
	CFoldingProcess&		foldingParams = (pProductPart) ? pProductPart->m_foldingParams : CFoldingProcess();
	CPrintComponentsGroup	printComponents;
	while (pPrintGroup)
	{
		pPrintGroup->InitPrintComponentsGroup(printComponents);
		pPrintGroup->PreImposeBoundComponents(printComponents);

		AutoImposePrintComponents(printComponents, pPrintGroup->GetPrintingProfile());
		
		if ( ! RunOptimization(*pPrintGroup))
			break;

		pDoc->m_printGroupList.Analyze();
		
		CPrintGroup* pNextPrintGroup = pDoc->m_printGroupList.FindFirstUnassigned();
		if (pNextPrintGroup == pPrintGroup)
			break;

		pPrintGroup = pNextPrintGroup;

		//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), TRUE);

		if (m_bEscapePressed)
			break;
	}

	pDoc->m_Bookblock.Reorganize();

	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	pDoc->m_PrintSheetList.InitializeFlatProductTypeLists();

	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();

	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();	
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->SetModifiedFlag(TRUE);

	CombineSimilarLayouts();

	theApp.UpdateView(RUNTIME_CLASS(CProductsView), TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView), TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView), TRUE);

	EndDialog(IDOK);
}

void CDlgOptimizationMonitor::OptimizeGroup(CPrintGroup& rPrintGroup)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	theApp.UndoSave();

	UpdateData(TRUE);

	pDoc->m_printGroupList.UnlockGroups();

	CPrintingProfile&	rPrintingProfile = rPrintGroup.GetPrintingProfile();
	CBoundProductPart*	pProductPart	 = rPrintGroup.GetFirstBoundProductPart();
	CFoldingProcess&	foldingParams	 = (pProductPart) ? pProductPart->m_foldingParams : CFoldingProcess();

	CPrintComponentsGroup printComponents;
	rPrintGroup.InitPrintComponentsGroup(printComponents);
	rPrintGroup.PreImposeBoundComponents(printComponents);

	RunOptimization(rPrintGroup);

	pDoc->m_Bookblock.Reorganize();
		
	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	pDoc->m_PrintSheetList.InitializeFlatProductTypeLists();

	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();

	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();	
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->SetModifiedFlag(TRUE);

	CombineSimilarLayouts();

	theApp.UpdateView(RUNTIME_CLASS(CProductsView), TRUE); 
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView), TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView), TRUE);

	if ( (strlen(theApp.settings.m_szSPrintOneTenantID) <= 0) || (strlen(theApp.settings.m_szSPrintOneUserID) <= 0) || (strlen(theApp.settings.m_szSPrintOnePassword) <= 0) ) 
		EndDialog(IDOK);
}

BOOL CDlgOptimizationMonitor::RunOptimization(CPrintGroup& rPrintGroup)
{
	if ( (strlen(theApp.settings.m_szSPrintOneTenantID) <= 0) || (strlen(theApp.settings.m_szSPrintOneUserID) <= 0) || (strlen(theApp.settings.m_szSPrintOnePassword) <= 0) ) 
	{
		CKimOptimizer kimOptimizer(this);
		kimOptimizer.DoOptimizePrintGroup(rPrintGroup);
	}
	else
	{
		CsPrintOneOptimizer sPrintOneOptimizer(this);
		sPrintOneOptimizer.DoOptimizePrintGroup(rPrintGroup);
	}

	return TRUE;
}

BOOL CDlgOptimizationMonitor::AutoImposePrintComponents(CPrintComponentsGroup& rPrintComponents, CPrintingProfile& rPrintingProfile)
{
	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return FALSE;

	//if (rPrintComponents.m_printComponents.GetSize() <= 0)
	//	return FALSE;

	//CPrepressParams& rPrepressParams = rPrintingProfile.m_prepress;
	//for (int i = 0; i < rPrepressParams.m_impositionTemplates.GetSize(); i++)
	//{
	//	rPrepressParams.ProcessImpositionTemplate(rPrepressParams.m_impositionTemplates[i], rPrintComponents);
	//}

	return TRUE;
}


//BOOL CDlgOptimizationMonitor::AutoImposePrintComponents(CPrintGroup& rPrintGroup)
//{
//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
//	if ( ! pDoc)
//		return FALSE;
//
//	CPrintingProfile&	rPrintingProfile = rPrintGroup.GetPrintingProfile();
//
//	CPrintSheet* pPrintSheet = rPrintGroup.GetFirstPrintSheet();
//	if ( ! pPrintSheet)
//		return FALSE;
//
//	CLayout* pLayout = pPrintSheet->GetLayout();
//	if ( ! pLayout)
//		return FALSE;
//
//	int nLayoutIndex			= pPrintSheet->m_nLayoutIndex;
//	*pPrintSheet				= rPrintingProfile.m_layoutPool[0].m_printSheet;
//	pPrintSheet->m_nLayoutIndex = nLayoutIndex; 
//	*pLayout					= rPrintingProfile.m_layoutPool[0].m_layout;
//	pPrintSheet->m_strPDFTarget = rPrintingProfile.m_prepress.m_strOutputProfile;
//	if ( ! rPrintingProfile.m_press.m_strMarkset.IsEmpty())
//	{
//		CString strMarksDir		= CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets\\");
//		CString strRelativePath	= rPrintingProfile.m_press.m_strMarkset;	
//		strRelativePath.TrimLeft('.');		// trim .\ and .mks do get equal strings when imported from xml or from ProductionProfile
//		strRelativePath.TrimLeft('\\');
//		CString strMarksetFullPath = strMarksDir + strRelativePath; 
//		if (strRelativePath.Right(4).CompareNoCase(_T(".mks")) != 0)
//			 strMarksetFullPath += _T(".mks");
//		CMarkList markList;
//		markList.Load(strMarksetFullPath);
//		pLayout->m_markList.Append(markList);
//	}
//
//	pLayout->IncreaseNumLayoutName();
//
//	pPrintSheet->m_flatProductRefs.RemoveAll();
//
//	CPrintGroupComponentList componentList = rPrintGroup.PrepareComponentsToOptimize(pLayout->m_FrontSide.m_fPaperWidth, pLayout->m_FrontSide.m_fPaperHeight, rPrintingProfile);
//
//	int nTotalNumUp = pLayout->GetTotalNUp();
//	int nTotalQuantity = 0;
//	for (int i = 0; (i < componentList.GetSize()) && (i < nTotalNumUp); i++)
//	{
//		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.FindByIndex(rPrintGroup.m_componentList[i].m_nFlatProductIndex);
//		rFlatProduct.m_nDesiredQuantity = max(rFlatProduct.m_nDesiredQuantity, 1);
//		nTotalQuantity += rFlatProduct.m_nDesiredQuantity;
//	}
//
//	BOOL bPlaced = FALSE;
//	BOOL bFirst = TRUE;
//	POSITION pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition(); 
//	for (int i = 0; (i < componentList.GetSize()) && (i < nTotalNumUp); i++)
//	{
//		if (componentList[i].m_nProductClass == CPrintGroupComponent::Bound)
//			continue;
//
//		//float fOverPercent	= ((float)nRealQuantity/(float)nDesiredQuantity - 1.0f) * 100.0f;
//		//if ( (fOverPercent < rProductionSetup.m_fMaxOverProduction) || (rProductionSetup.m_fMaxOverProduction == 0.0f) )
//		//{
//
//		int			  nFlatProductIndex	 = componentList[i].m_nFlatProductIndex;
//		CFlatProduct& rFlatProduct		 = pDoc->m_flatProducts.FindByIndex(nFlatProductIndex);
//		int			  nNumUp			 = (int)((float)(((float)rFlatProduct.m_nDesiredQuantity / (float)nTotalQuantity) * (float)nTotalNumUp) + 0.5f);
//		while (pos && (nNumUp > 0))
//		{
//			CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
//			if (rObject.m_nType == CLayoutObject::ControlMark)
//				continue;
//			if ( ! rObject.IsFlatProduct())
//				continue;
//
//			int	nFlatProductRefIndex = pPrintSheet->MakeFlatProductRefIndex(nFlatProductIndex);
//			rObject.m_nComponentRefIndex		= nFlatProductRefIndex;
//
//			int nProductOrientation = (rFlatProduct.m_fTrimSizeWidth < rFlatProduct.m_fTrimSizeHeight) ? PORTRAIT : LANDSCAPE;
//			int nObjectOrientation  = (rObject.GetRealWidth() < rObject.GetRealHeight()) ? PORTRAIT : LANDSCAPE;
//			if (nProductOrientation != nObjectOrientation)
//			{
//				switch (rObject.m_nHeadPosition)
//				{
//				case TOP:		rObject.m_nHeadPosition = RIGHT;	break;
//				case RIGHT:		rObject.m_nHeadPosition = BOTTOM;	break;
//				case BOTTOM:	rObject.m_nHeadPosition = LEFT;		break;
//				case LEFT:		rObject.m_nHeadPosition = TOP;		break;
//				}
//			}
//
//			CLayoutObject* pCounterPart = rObject.FindCounterpartObject();
//			if (pCounterPart)
//			{
//				pCounterPart->m_nComponentRefIndex = nFlatProductRefIndex;
//				switch (pCounterPart->GetLayout()->KindOfProduction())
//				{
//				case WORK_AND_TURN  : pCounterPart->m_nHeadPosition = (rObject.m_nHeadPosition == LEFT) ? RIGHT  : (rObject.m_nHeadPosition == RIGHT)  ? LEFT : rObject.m_nHeadPosition; break;
//        		case WORK_AND_TUMBLE: pCounterPart->m_nHeadPosition = (rObject.m_nHeadPosition == TOP)  ? BOTTOM : (rObject.m_nHeadPosition == BOTTOM) ? TOP  : rObject.m_nHeadPosition; break;
//        		}
//			}
//				
//			nNumUp--;
//			bPlaced = TRUE;
//		}
//	}
//
//	BOOL bObjectsDeleted = FALSE;
//	while (pos)
//	{
//		POSITION prevPos = pos;
//		CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
//		if (rObject.m_nType == CLayoutObject::ControlMark)
//			continue;
//		if ( ! rObject.IsFlatProduct())
//			continue;
//
//		CLayoutObject* pCounterPartObject = rObject.FindCounterpartObject();
//
//		pLayout->m_FrontSide.m_ObjectList.RemoveAt(prevPos);
//
//		if (pCounterPartObject)
//		{
//			CLayoutSide* pCounterPartSide = pCounterPartObject->GetLayoutSide();
//			POSITION pos = pCounterPartSide->m_ObjectList.GetHeadPosition();
//			while (pos)
//			{
//				POSITION	   prevPos = pos;
//				CLayoutObject& rCounterObject = pCounterPartSide->m_ObjectList.GetNext(pos);
//				if (&rCounterObject == pCounterPartObject)
//					if (rCounterObject.m_nType != CLayoutObject::ControlMark)
//						pCounterPartSide->m_ObjectList.RemoveAt(prevPos);
//			}
//		}
//	}
//
//	if ( ! bPlaced)
//	{
//		pPrintSheet->m_flatProductFormatList.RemoveAll();
//		pPrintSheet->m_flatProductRefs.RemoveAll();
//		pLayout->m_FrontSide.m_ObjectList.RemoveAll();
//		pLayout->m_BackSide.m_ObjectList.RemoveAll();
//	}
//
//	pDoc->m_PrintSheetList.ReNumberPrintSheets();
//	pDoc->m_Bookblock.ReNumberFoldSheets(ALL_PARTS);
//
//	pPrintSheet->CalcTotalQuantity(TRUE);
//
//	pDoc->m_PrintSheetList.InitializeFlatProductTypeLists();
//
//	pDoc->m_PageTemplateList.FindPrintSheetLocations();
//	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
//
//	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
//	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();	
//	pDoc->m_PrintSheetList.ReorganizeColorInfos();
//
//	return TRUE;
//}

void CDlgOptimizationMonitor::CombineSimilarLayouts()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);

		int nIndex = pDoc->m_PrintSheetList.m_Layouts.GetSimilarLayout(&rLayout, TRUE);
		if (nIndex >= 0)
		{
			CPrintSheet* pPrintSheet = rLayout.GetFirstPrintSheet();
			while (pPrintSheet)
			{
				pPrintSheet->m_nLayoutIndex = -1;
				pPrintSheet = rLayout.GetNextPrintSheet(pPrintSheet);
			}
			int nIndexToRemove = rLayout.GetIndex();
			POSITION posLayout = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nIndexToRemove);
			if (posLayout)
				pDoc->m_PrintSheetList.m_Layouts.RemoveAt(posLayout);

			POSITION posProfile = pDoc->m_printingProfiles.FindIndex(nIndexToRemove);
			if (posProfile)
				pDoc->m_printingProfiles.RemoveAt(posProfile);
			if ( (nIndexToRemove >= 0) && (nIndexToRemove < pDoc->m_printGroupList.GetSize()) )
				pDoc->m_printGroupList.RemoveAt(nIndexToRemove);

			if (nIndex > nIndexToRemove)
				nIndex--;
			POSITION posPrintSheet = pDoc->m_PrintSheetList.GetHeadPosition();
			while (posPrintSheet)
			{
				CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(posPrintSheet);
				if (rPrintSheet.m_nLayoutIndex < 0)
				{
					rPrintSheet.m_nLayoutIndex = nIndex;
					rPrintSheet.LinkLayout(NULL);
				}
			}
			posLayout = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nIndex);
			if (posLayout)
			{
				CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetAt(posLayout);
				rLayout.m_FrontSide.Invalidate();
				rLayout.m_BackSide.Invalidate();
			}
		}
	}
}

BOOL CDlgOptimizationMonitor::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_ESCAPE)
		{
			m_bEscapePressed = TRUE;
		}

	return CDialog::PreTranslateMessage(pMsg);
}	

void CDlgOptimizationMonitor::ReportWindowOutput(CString strSubject, CString strMsg, BOOL bOverwriteMode)
{
	if (strSubject.IsEmpty() && strMsg.IsEmpty())
		return;

	if (bOverwriteMode)
		if (m_reportList.GetItemCount() <= 0)
			bOverwriteMode = FALSE;

	LVITEM item;
	item.mask	  = LVIF_TEXT;
	item.iItem	  = (bOverwriteMode) ? m_reportList.GetItemCount() - 1 : m_reportList.GetItemCount();
	item.iSubItem = 0;
	item.pszText  = strSubject.GetBuffer();
	if ( ! bOverwriteMode)
		m_reportList.InsertItem(&item);

	if ( ! strMsg.IsEmpty())
	{
		item.iSubItem = 1;
		CString strSubItem = strMsg;
		//if (bOverwriteMode)
		//	strSubItem = m_reportList.GetItemText(item.iItem, item.iSubItem) + _T(" ") + strMsg;
		item.pszText  = strSubItem.GetBuffer();
		m_reportList.SetItem(&item);
	}

	if ( ! bOverwriteMode)
		m_reportList.EnsureVisible(m_reportList.GetItemCount() - 1, FALSE);

	m_reportList.UpdateWindow();
}

void CDlgOptimizationMonitor::OnBnClickedOk()
{
	if ( (strlen(theApp.settings.m_szSPrintOneTenantID) <= 0) || (strlen(theApp.settings.m_szSPrintOneUserID) <= 0) || (strlen(theApp.settings.m_szSPrintOnePassword) <= 0) ) 
		;
	else
	{
		EndModalLoop(IDOK);
		DestroyWindow();
	}
	//OnOK();
}
