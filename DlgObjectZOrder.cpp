// DlgObjectZOrder.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgObjectZOrder 


CDlgObjectZOrder::CDlgObjectZOrder(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgObjectZOrder::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgObjectZOrder)
	m_nZOrder = -1;
	//}}AFX_DATA_INIT

	m_pParent = NULL;
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
}

CDlgObjectZOrder::~CDlgObjectZOrder()
{
	m_hollowBrush.DeleteObject();
}

BOOL CDlgObjectZOrder::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgObjectZOrder::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgObjectZOrder)
	DDX_Radio(pDX, IDC_MARK_FOREGROUND, m_nZOrder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgObjectZOrder, CDialog)
	//{{AFX_MSG_MAP(CDlgObjectZOrder)
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgObjectZOrder 

BOOL CDlgObjectZOrder::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	((CButton*)GetDlgItem(IDC_MARK_FOREGROUND))->SetIcon(theApp.LoadIcon(IDI_MARK_FOREGROUND));
	((CButton*)GetDlgItem(IDC_MARK_BACKGROUND))->SetIcon(theApp.LoadIcon(IDI_MARK_BACKGROUND));
	
	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgObjectZOrder::LoadData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::FoldMark)
		return;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				if (pObjectSource)
				{
					int	nIndex = pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
					if (nIndex != -1)	// invalid string
					{
						CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
						CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
						strParams.TrimLeft(); strParams.TrimRight();
	
						if (strContentType == "$FOLD_MARK")
							m_nZOrder = (strParams.Find(_T("/Foreground")) != -1) ? 0 : 1;
					}
				}
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex != -1)	// invalid string
		{
			CString strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);
			CFoldMark* pFoldMark = (CFoldMark*)pMark;
			if (strContentType == "$FOLD_MARK")
				m_nZOrder = (pFoldMark->m_nZOrder == CFoldMark::Foreground) ? 0 : 1;
		}
	}

	UpdateData(FALSE);
}

void CDlgObjectZOrder::SaveData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::FoldMark)
		return;

	UpdateData(TRUE);

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				int			 nIndex				= pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
				if (nIndex != -1)	// invalid string
				{
					CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
					CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
					strParams.TrimLeft(); strParams.TrimRight();
					
					BOOL bSolid		  = (strParams.Find(_T("/Solid"))		!= -1) ? TRUE : FALSE;
					BOOL bTransparent = (strParams.Find(_T("/Transparent")) != -1) ? TRUE : FALSE;

					if (strContentType == "$FOLD_MARK")
						pObjectSource->m_strSystemCreationInfo.Format(_T("$FOLD_MARK %s %s %s"), (bSolid)	    ? _T("/Solid")	     : _T("/Dashed"),   
																					         (bTransparent) ? _T("/Transparent") : _T("/Opaque"),
																					         (m_nZOrder == 1)  ? _T("/Background")  : _T("/Foreground") );
					pObjectSource->m_nType = CPageSource::SystemCreated;
				}
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex != -1)	// invalid string
		{
			CString strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);

			CFoldMark* pFoldMark = (CFoldMark*)pMark;
			pFoldMark->m_MarkSource.m_nType = CPageSource::SystemCreated;

			pFoldMark->m_nZOrder = (m_nZOrder == 0) ? CFoldMark::Foreground : CFoldMark::Background;

			if (strContentType == "$FOLD_MARK")
				pFoldMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$FOLD_MARK %s %s %s"), (pFoldMark->m_nLinetype == CFoldMark::Solid)		 ? _T("/Solid")		  : _T("/Dashed"),   
																							  (pFoldMark->m_nBkMode   == CFoldMark::Transparent) ? _T("/Transparent") : _T("/Opaque"),
																							  (pFoldMark->m_nZOrder   == CFoldMark::Background)  ? _T("/Background")  : _T("/Foreground") );
		}
	}
}

void CDlgObjectZOrder::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}

HBRUSH CDlgObjectZOrder::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
//	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	HBRUSH hbr = (HBRUSH)m_hollowBrush.GetSafeHandle();

	return hbr;
}
