#pragma once

#include "TrueColorToolBar.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgObjectPreviewWindow.h"
#include "MyBitmapButton.h"



// CProdDataPrintView-Formularansicht

class CProdDataPrintView : public CFormView
{
	DECLARE_DYNCREATE(CProdDataPrintView)

protected:
	CProdDataPrintView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CProdDataPrintView();

public:
	enum { IDD = IDD_PRODDATAPRINTVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


public:
	CTrueColorToolBar			m_toolBar;
	CBrush						m_hollowBrush;
	CFont						m_boldFont;
	CMarkList					m_objectList;
	CString						m_strAutoMarksFolder;
	CLayout*					m_pCurrentLayout;
	CToolTipCtrl*				m_pToolTip;
	CImageList*					m_pMarkImageList;
	CMenu*						m_pNewMarkPopupMenu;
	CDlgObjectPreviewWindow		m_dlgMarkPreviewWindow;
	CMyBitmapButton				m_markUpButton, m_markDownButton;


public:
	void								InitObjectList(CMark* pMark = NULL);
	void								UpdateSheetPreview(BOOL bUpdateMarkOnly = FALSE);
	BOOL								IsSelected(CLayoutObject* pObject);
	class CPrintSheetNavigationView*	GetPrintSheetNavigationView();
	CPrintSheet*						GetActPrintSheet();
	CLayout*							GetActLayout();
	CPressDevice*						GetActPressDevice();
	static void							DrawItemPrintLayout(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	CMark*								GetCurrentMark();
	CLayoutObject*						GetLayoutObject();
	void								AddNewMark(int nMarkType);
	CString								FindFreeMarkName(CString strNewMark);
	CDlgLayoutObjectProperties* 		GetDlgLayoutObjectProperties();
	BOOL								IsOpenMarkEditDlg();
	void								LoadAutoMarksState();
	void								SaveAutoMarksState();
	void								UpdateLayoutMarkObjects(BOOL bUpdateState = TRUE);
	void								ShowPrintSheetViewSelections();

	

public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	CString m_strHeaderText;
	CListCtrl m_objectListCtrl;
	BOOL m_bAutoMarksActive;
	BOOL m_bMarkStatus;

	DECLARE_MESSAGE_MAP()
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnLvnKeydownPdMarkList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnItemchangedObjectList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickObjectList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMDblclkPdMarkList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedPdModifyMark();
	afx_msg void OnBnClickedPdNewMark();
	afx_msg void OnBnClickedPdMarkDuplicate();
	afx_msg void OnBnClickedPdMarkDelete();
	afx_msg void OnBnClickedPdOpenMarklib();
	afx_msg void OnBnClickedPdDeleteAllMarks();
	afx_msg void OnMarkSetStatus();
	afx_msg void OnMarkUp();
	afx_msg void OnMarkDown();
	afx_msg void OnUpdateMarkSetStatus(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMarkUp(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMarkDown(CCmdUI* pCmdUI);
	afx_msg void OnBnClickedPdOpenAutoMarklib();
	afx_msg void OnUpdateModifyMark(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDuplicateMark(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteMark(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteAllMarks(CCmdUI* pCmdUI);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnNewMarkPDF();
	afx_msg void OnNewMarkText();
	afx_msg void OnNewMarkSection();
	afx_msg void OnNewMarkCrop();
	afx_msg void OnNewMarkFold();
	afx_msg void OnNewMarkBarcode();
	afx_msg void OnNewMarkRectangle();
	afx_msg void OnBnClickedPdAutomarksActive();
	afx_msg void OnNMSetfocusPdMarkList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMKillfocusPdMarkList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedPdAnalyzemarksReport();
	afx_msg void OnUpdateAnalyzemarksReport(CCmdUI* pCmdUI);
};

