#if !defined(AFX_DLGMARKSET_H__37AB6A58_75CE_4C46_A2A7_50731918E54D__INCLUDED_)
#define AFX_DLGMARKSET_H__37AB6A58_75CE_4C46_A2A7_50731918E54D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgMarkSet.h : Header-Datei
//



#include "MyBitmapButton.h"



class CDirItem
{
public:
	CDirItem() { m_nAttribute = -1; };
	CDirItem(const CString& strFileName, int nAttribute) { m_strFileName = strFileName; m_nAttribute = nAttribute; };
	~CDirItem() { m_strFileName.Empty(); };

enum {IsMarkset = 1, IsFolder = 2};

public:
	CString m_strFileName;
	int		m_nAttribute;
};


class CDirList : public CArray <CDirItem, CDirItem&>
{
public:
	void inline			Sort() { qsort((void*)GetData(), GetSize(), sizeof(CDirItem), CompareDirItem); }
	static int inline	CompareDirItem(const void *p1, const void *p2)
						{
							CDirItem* pDirItem1 = (CDirItem*)p1;
							CDirItem* pDirItem2 = (CDirItem*)p2;
							BOOL	  bSameType = FALSE;

							if (pDirItem1->m_nAttribute == CDirItem::IsFolder)
								if (pDirItem2->m_nAttribute == CDirItem::IsMarkset)
									return -1;
								else
									bSameType = TRUE;
							else
								if (pDirItem1->m_nAttribute == CDirItem::IsMarkset)
									if (pDirItem2->m_nAttribute == CDirItem::IsFolder)
										return 1;
									else
										bSameType = TRUE;

							if (bSameType)
							{
								if (pDirItem1->m_strFileName.CompareNoCase(pDirItem2->m_strFileName) < 0)
									return -1;
								else
									if (pDirItem1->m_strFileName.CompareNoCase(pDirItem2->m_strFileName) > 0)
										return 1;
									else
										return 0;
							}

							return 0;
						}
	CString				FindMark(CMark* pMark, CString strMarkName, CString strFolder, CPrintSheet* pPrintSheet, int nSide);
};


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMarkSet 

class CDlgMarkSet : public CDialog
{
	DECLARE_DYNCREATE(CDlgMarkSet)

// Konstruktion
public:
	CDlgMarkSet(CWnd* pParent = NULL, BOOL bRunModal = FALSE);   // Standardkonstruktor
	~CDlgMarkSet();

// Dialogfelddaten
	//{{AFX_DATA(CDlgMarkSet)
	enum { IDD = IDD_MARKSET };
	CListCtrl	m_marksetBrowser;
	CListCtrl	m_markListCtrl;
	//}}AFX_DATA


public:
	BOOL					m_bRunModal, m_bBrowseOnly, m_bOpenAutoMarks, m_bIsEditMode;
	CBrush*					m_pBrowserStaticBrush;
	CBrush*					m_pBrowserHeaderBrush;
	CBrush*					m_pMarksetHeaderBrush;
	CDirList				m_dirList;
	CString					m_strCurrentFolder;
	CMarkList				m_markList;
	CString					m_strMarksetFullPath;
	CString					m_strCurrentMarkset;
	int						m_nCurMarkSel;	
	CString					m_strMarkName;
	CToolTipCtrl*			m_pToolTip;
	CImageList*				m_pMarkImageList;
	CImageList*				m_pBrowserImageList;
	CSize					m_sizeViewport;
	CDlgSelectNewMark		m_dlgSelectNewMark;
	CDlgLayoutObjectControl	m_dlgLayoutObjectControl;
	CDlgObjectPreviewControl	m_dlgObjectPreviewControl;
	CPageSource				m_MarkSource;
	CRect					m_markListRectBrowserMode;
	CRect					m_marksetHeaderRectBrowserMode;
	CRect					m_previewRectBrowserMode;
	CPrintSheet*			m_pActPrintSheet;

	CWnd*					m_pWnd;
	CMenu*					m_pNewMarkPopupMenu;
	CMyBitmapButton			m_markUpButton, m_markDownButton;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgMarkSet)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void				SetBrowserMode();
	void				SetEditMode();
	int 				InitMarksetBrowser(CString strMarksetToSelect = _T(""), CMark* pMark = NULL, CString strMarkName = _T(""));
	void				DeleteFolder(const CString& strFolderToRemove);
	CMark*				GetCurrentMark();
	CPrintSheet*		GetPrintSheet();
	CString				GetMarksetInitialFolder();
	CString				GetMarksetFilename(const CString& strName);
	BOOL				MarkListChanged();
	BOOL				LoadMarkSet(CString strTmp = "", CMarkList* pTmpMarkList = NULL);
	BOOL				StoreMarkSet(BOOL bDoPrompt = FALSE);
	static void			UpdateSheetPreview(BOOL bUpdateMarkOnly = FALSE);
	void				InitMarkList(CMark* pMark = NULL, CString strMarkToFind = _T(""));
	void				AddNewMark(int nMarkType);
	void				DuplicateCurrentMark(BOOL bOffsetMark = FALSE);
	static BOOL			CheckTextmarkPlausibility(CMark* pMark, CString& strLayout, CPrintSheet* pActPrintSheet = NULL);
	static BOOL			CheckMarkWidthHeight(CMark* pMark);
	static BOOL			CheckTextmarkLayerRef(LPCTSTR pszTextString, LPCTSTR pszPlaceholder, int nNumFoldSheets);
	static BOOL			PositionAndCreateMarkAuto(CMark *pMark, CMarkList* pMarkList, CString strMarkSetFullPath = "", CPrintSheet* pActPrintSheet = NULL);
	static int			AddObjectsAndExposures   (CMark *pMark, int nSide, CPageTemplate* pMarkTemplate, CPrintSheet* pActPrintSheet, CLayout* pActLayout, CMarkList* pMarkList = NULL);
	static CPrintSheet*	GetActPrintSheet(CPrintSheet* pActPrintSheet);
	static CLayout*		GetActLayout(CPrintSheet* pActPrintSheet);
	static int			GetActSight(CPrintSheet* pActPrintSheet);
	void				UpdateEnableStates();


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgMarkSet)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnMarkNew();
	afx_msg void OnClickMarkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedMarkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndlabeleditMarkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnOpenMarkset();
	afx_msg void OnMarkDuplicate();
	afx_msg void OnMarkUp();
	afx_msg void OnMarkDown();
	afx_msg void OnAssignMarkset();
	afx_msg void OnCloseMarksetDlg();
	afx_msg void OnClose();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnMarkDelete();
	afx_msg void OnApplyButton();
	afx_msg void OnCancel();
	afx_msg void OnAssignSelectedmark();
	afx_msg void OnDblclkMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnFolderUp();
	afx_msg void OnEndlabeleditMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDelete();
	afx_msg void OnFolderNew();
	afx_msg void OnMarksetNew();
	afx_msg void OnItemchangedMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	afx_msg void OnDblclkMarkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeydownMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKeydownMarkList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMarksetDuplicate();
	afx_msg void OnSetfocusMarksetBrowser(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSetfocusMarkList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized );
	afx_msg void OnUpdateSubDialogs();
	afx_msg void OnUpdateMarkname();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnStnClickedMarkpropsFrame();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnNewMarkPDF();
	afx_msg void OnNewMarkText();
	afx_msg void OnNewMarkSection();
	afx_msg void OnNewMarkCrop();
	afx_msg void OnNewMarkFold();
	afx_msg void OnNewMarkBarcode();
	afx_msg void OnNewMarkRectangle();
	afx_msg void OnNMKillfocusMarkList(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	virtual void PostNcDestroy();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGMARKSET_H__37AB6A58_75CE_4C46_A2A7_50731918E54D__INCLUDED_
