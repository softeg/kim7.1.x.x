// DlgBindingDefs.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgBindingDefs.h"


// CDlgBindingDefs-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgBindingDefs, CDialog)

CDlgBindingDefs::CDlgBindingDefs(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgBindingDefs::IDD, pParent)
	, m_nBinding(FALSE)
{

}

CDlgBindingDefs::~CDlgBindingDefs()
{
}

void CDlgBindingDefs::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_BINDING_PERFECT, m_nBinding);
	m_bindingSelection.DDX_BindingDefsCombo(pDX, IDC_BD_BINDINGNAME, m_strBindingName);
}


BEGIN_MESSAGE_MAP(CDlgBindingDefs, CDialog)
	ON_CBN_SELCHANGE(IDC_BD_BINDINGNAME, &CDlgBindingDefs::OnCbnSelchangeBdBindingname)
	ON_BN_CLICKED(IDC_BD_SAVE, &CDlgBindingDefs::OnBnClickedBdSave)
	ON_BN_CLICKED(IDC_BD_DELETE, &CDlgBindingDefs::OnBnClickedBdDelete)
END_MESSAGE_MAP()


// CDlgBindingDefs-Meldungshandler

BOOL CDlgBindingDefs::OnInitDialog()
{
	CDialog::OnInitDialog();

// binding method combo ////////////////////////////
	CComboBoxEx* pBox = (CComboBoxEx*)GetDlgItem (IDC_BD_BINDINGNAME);
	CBitmap bitmap;
	m_bindingImageList.Create(24, 12, ILC_COLOR8, 2, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_PERFECT_BOUND);
	m_bindingImageList.Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	bitmap.LoadBitmap(IDB_SADDLE_STITCHED);
	m_bindingImageList.Add(&bitmap, BLACK);
	pBox->SetImageList(&m_bindingImageList);
	bitmap.DeleteObject();

	m_bindingSelection.InitComboBox(this, m_strBindingName, &m_nBinding, IDC_BD_BINDINGNAME);
	OnCbnSelchangeBdBindingname();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgBindingDefs::OnCbnSelchangeBdBindingname()
{
	UpdateData(TRUE);
	m_bindingSelection.SelchangeFormatList(this, &m_nBinding, m_strBindingName, IDC_BD_BINDINGNAME);
}

void CDlgBindingDefs::OnBnClickedBdSave()
{
	UpdateData(TRUE);
	m_bindingSelection.NewEntry(this, &m_nBinding, m_strBindingName, IDC_BD_BINDINGNAME);
	m_bindingSelection.InitComboBox(this, m_strBindingName, &m_nBinding, IDC_BD_BINDINGNAME);
	OnCbnSelchangeBdBindingname();
}

void CDlgBindingDefs::OnBnClickedBdDelete()
{
	UpdateData(TRUE);
	m_bindingSelection.DeleteEntry(this, &m_nBinding, m_strBindingName, IDC_BD_BINDINGNAME);
	m_bindingSelection.InitComboBox(this, m_strBindingName, &m_nBinding, IDC_BD_BINDINGNAME);
	OnCbnSelchangeBdBindingname();
}
