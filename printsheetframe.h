// PrintSheetFrame.h : header file
//

#pragma once

#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"



class CDlgPDFOutputMediaSettings;
class CDlgPDFTargetProps;
class CDlgBmpExposerMediaSettings;
class CDlgPlateSelection;
class CDlgTable;
class CDlgTableCtp;
class CDlgMarkSet;

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetFrame frame

class CPrintSheetFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CPrintSheetFrame)
protected:
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);

	CPrintSheetFrame();           // protected constructor used by dynamic creation

	float m_fOverbleed;

public:
	enum {SubViewMarks, SubViewPrintSheets, SubViewOutput};

// Attributes
public:
	BOOL m_bUndoActive;
	BOOL m_bMainToolBarActive;
	BOOL m_bTrimToolActive;

	BOOL m_bFoldSheetTurnActive;	// FoldSheet - Turn   - Button 
	BOOL m_bFoldSheetTumbleActive;	// FoldSheet - Tumble - Button 
	BOOL m_bFoldSheetRotateActive;	// FoldSheet - Rotate - Button 

	UINT m_nViewMode;
// Main-Toolbar
	BOOL m_bShowFoldSheet;
	BOOL m_bEnableFoldSheetButton;
	BOOL m_bShowPlate;
	BOOL m_bEnablePlateButton;
	BOOL m_bShowMeasure;
	BOOL m_bEnableMeasureButton;
	BOOL m_bShowExposures;
	BOOL m_bEnableExposuresButton;
	BOOL m_bShowBitmap;
	BOOL m_bShowFrontback;
	BOOL m_bShowBoxShapes;
	BOOL m_bEnableBitmapButton;
	BOOL m_bShowOutputPlates;
	BOOL m_bEnableOutputPlatesButton;
	BOOL m_bShowPrintSheetTable;
	BOOL m_bMaxPrintSheetTable;

	BOOL m_bShowTableCtp;	// CTP-Version

	BOOL m_bReleaseBleed;
	BOOL m_bEnableReleaseBleedButton;
	BOOL m_bShowPlateTable;
	BOOL m_bEnablePlateTableButton;

	BOOL m_bMarksViewActive;
//
	BOOL m_bOverbleedChanged;

	// Main Menu
	BOOL m_bShowPrintMenu;
	// Bearbeiten Menu
	BOOL m_bTrimToolMenuActive;
	BOOL m_bTurnTumbleMenuActive;
	BOOL m_bLayoutObjectSelected;
	BOOL m_bFoldSheetSelected;
	BOOL m_bShowCenterMenu;
	BOOL m_bAlignSectionMenuActive;
	BOOL m_bAlignSectionActive;
	// Einstellungen Menu
	BOOL m_bPressDeviceMenuActive;
	BOOL m_bHandActive;


// Operations
public:
	void Resize();
	BOOL						IsOpenDlgLayoutObjectGeometry();
	CDlgLayoutObjectGeometry*	GetDlgLayoutObjectGeometry();
	BOOL						IsOpenMarkSetDlg();
	CDlgMarkSet*				GetMarkSetDlg();
	BOOL						IsOpenDlgNewMark();
	BOOL						IsOpenDlgNewMarkGeometry();
	CDlgLayoutObjectProperties* GetDlgNewMark();
	void						UpdateMarkDialogs();
	void						OpenDetailedView(CPrintSheet* pPrintSheet, int nSubView);
	static void					DeleteSelection(BOOL* bLayoutObjectSelected, BOOL* bFoldSheetSelected);
	static BOOL					DeleteFoldSheet	 (CDisplayItem* pDI, int& nRet, BOOL& bFoldSheetDeleted, CLayout** ppLayout);
	static BOOL					DeleteFlatProduct(CDisplayItem* pDI, int& nRet, BOOL& bProductDeleted,	 CLayout** ppLayout);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintSheetFrame)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_OVERLAPPEDWINDOW, const RECT& rect = rectDefault, CMDIFrameWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void ActivateFrame(int nCmdShow = -1);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
public:
	void CloseTrimTool();		 // Must have access from PrintSheetTreeView
	void OnUpdateSectionMenu();	 // Must have access from PrintSheetTreeView

	CDlgPDFOutputMediaSettings* m_pDlgOutputMedia;
	CDlgMoveObjects*			m_pDlgMoveObjects;
	CDlgLayoutObjectProperties*	m_pDlgLayoutObjectProperties;
	CDlgMarkSet*				m_pDlgMarkSet;

	// Treeview must have access for redrawing table after selection changes
	CDlgTableCtp*	 m_pDlgTableCtp;
	class CDlgMarks* m_pDlgMarks;

friend class CPrintSheetMarksView;

public:
	CSplitterWnd m_wndSplitter;
	CSplitterWnd m_wndSplitter2;
protected:
	BOOL		 m_bSplittersCreated;
	BOOL		 m_bStatusRestored;

	CToolBar	 m_wndPrintSheetFrameToolBar;

	CToolBar	 m_wndTrimToolBar;
	CToolBar	 m_wndFoldSheetToolBar;
	CToolBar	 m_wndFlatWorkToolBar;

	CToolBar	 m_wndCutToolBar;
	CToolBar	 m_wndCutDeviceToolBar;

	CDlgTable* m_pDlgTable;
	virtual ~CPrintSheetFrame();

	void CreateMainToolBar();
	void CreateTrimToolBar();
	void CreateFoldToolBar();
	void CreateFlatWorkToolBar();

	void CheckTrimToolCtrls();

	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);


public:
	afx_msg void OnOutputPlates();
	afx_msg void OnPlatecheck();
	afx_msg void OnExposuresCheck();

protected:
	// Generated message map functions
	//{{AFX_MSG(CPrintSheetFrame)
	afx_msg void OnDefineSheet();
	afx_msg void OnDefineTrim();
	afx_msg void OnTrim1();
	afx_msg void OnUpdateTrim1(CCmdUI* pCmdUI);
	afx_msg void OnTrim2();
	afx_msg void OnUpdateTrim2(CCmdUI* pCmdUI);
	afx_msg void OnTrim3();
	afx_msg void OnUpdateTrim3(CCmdUI* pCmdUI);
	afx_msg void OnTrim4();
	afx_msg void OnUpdateTrim4(CCmdUI* pCmdUI);
	afx_msg void OnTrim5();
	afx_msg void OnUpdateTrim5(CCmdUI* pCmdUI);
	afx_msg void OnTrim6();
	afx_msg void OnUpdateTrim6(CCmdUI* pCmdUI);
	afx_msg void OnTrim7();
	afx_msg void OnUpdateTrim7(CCmdUI* pCmdUI);
	afx_msg void OnTrim8();
	afx_msg void OnUpdateTrim8(CCmdUI* pCmdUI);
	afx_msg void OnDefineMark();
	afx_msg void OnSettingsPressDevice();
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnChangeFormats();
	afx_msg void OnAddExposurePrintsheet();
	afx_msg void OnUpdateDefineTrim(CCmdUI* pCmdUI);
	afx_msg void OnNotes();
	afx_msg void OnUpdateChangeFormats(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSettingsPressDevice(CCmdUI* pCmdUI);
	afx_msg void OnPagelistView();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTurnTumblePrintSheet();
	afx_msg void OnFoldsheetcheck();
	afx_msg void OnUpdateFoldsheetcheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlatecheck(CCmdUI* pCmdUI);
	afx_msg void OnMeasurecheck();
	afx_msg void OnUpdateMeasurecheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateExposuresCheck(CCmdUI* pCmdUI);
	afx_msg void OnDeleteSelection();
	afx_msg void OnMoveObjects();
	afx_msg void OnUpdateBitmapcheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOutputPlates(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMoveObject(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteSelection(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTurnTumble(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrintPreview(CCmdUI* pCmdUI);
	afx_msg void OnPlatetablecheck();
	afx_msg void OnUpdatePlatetablecheck(CCmdUI* pCmdUI);
	afx_msg void OnReleaseBleed();
	afx_msg void OnUpdateReleaseBleed(CCmdUI* pCmdUI);
	afx_msg void OnViewPrintsheetframeBar();
	afx_msg void OnUpdateViewPrintsheetframeBar(CCmdUI* pCmdUI);
	afx_msg void OnCtpTable();
	afx_msg void OnUpdateCtpTable(CCmdUI* pCmdUI);
	afx_msg void OnWorkOnMark();
	afx_msg void OnSetMark();
	afx_msg void OnUpdateSetMark(CCmdUI* pCmdUI);
	afx_msg void OnUpdateWorkOnMark(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDefineMark(CCmdUI* pCmdUI);
	afx_msg void OnAlignSection();
	afx_msg void OnUpdateAlignSection(CCmdUI* pCmdUI);
	afx_msg void OnResetSection();
	afx_msg void OnUpdateResetSection(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewAllSheets(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewSingleSheets(CCmdUI* pCmdUI);
	afx_msg void OnZoom();
	afx_msg void OnUpdateZoom(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewPrintsheettable(CCmdUI* pCmdUI);
	afx_msg void OnZoomDecrease();
	afx_msg void OnUpdateZoomDecrease(CCmdUI* pCmdUI);
	afx_msg void OnZoomFullview();
	afx_msg void OnUpdateZoomFullview(CCmdUI* pCmdUI);
	afx_msg void OnFilePageSetup();
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	afx_msg void OnClose();
	afx_msg void OnDeleteMarks();
	afx_msg void OnUpdatePdfOutput(CCmdUI* pCmdUI);
	afx_msg void OnViewPrintsheettable();
	afx_msg void OnBmpOutput();
	afx_msg void OnUpdateBmpOutput(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	afx_msg void OnScrollhand();
	afx_msg void OnUpdateScrollhand(CCmdUI* pCmdUI);
	afx_msg void OnShowFrontback();
	afx_msg void OnUpdateShowFrontback(CCmdUI* pCmdUI);
	afx_msg void OnUndo();
	afx_msg void OnUpdateUndo(CCmdUI* pCmdUI);
	afx_msg void OnShowBoxshapes();
	afx_msg void OnUpdateShowBoxshapes(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBitmapcheck();
	afx_msg void OnViewSingleSheets();
	afx_msg void OnViewAllSheets();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPdfOutput();
	afx_msg void OnObjectProperties();
};

/////////////////////////////////////////////////////////////////////////////
