// Von Klassen-Assistent automatisch erstellte IDispatch-Kapselungsklasse(n).

#include "stdafx.h"
#include "acrodist.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// Eigenschaften _PdfEvents 

/////////////////////////////////////////////////////////////////////////////
// Operationen _PdfEvents 

void _PdfEvents::OnLogMessage(LPCTSTR strMessage)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 strMessage);
}

void _PdfEvents::OnJobStart(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 strInputPostScript, strOutputPDF);
}

void _PdfEvents::OnJobDone(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 strInputPostScript, strOutputPDF);
}

void _PdfEvents::OnJobFail(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 strInputPostScript, strOutputPDF);
}

void _PdfEvents::OnPercentDone(long nPercentDone)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 nPercentDone);
}

void _PdfEvents::OnPageNumber(long nPageNumber)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 nPageNumber);
}


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften IPdfDistiller 

/////////////////////////////////////////////////////////////////////////////
// Operationen IPdfDistiller 

short IPdfDistiller::FileToPDF(LPCTSTR strInputPostScript, LPCTSTR strOutputPDF, LPCTSTR strJobOptions)
{
	short result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		strInputPostScript, strOutputPDF, strJobOptions);
	return result;
}

void IPdfDistiller::Create()
{
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long IPdfDistiller::GetBSpoolJobs()
{
	long result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IPdfDistiller::SetBSpoolJobs(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

long IPdfDistiller::GetBShowWindow()
{
	long result;
	InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
	return result;
}

void IPdfDistiller::SetBShowWindow(long nNewValue)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
		 nNewValue);
}

void IPdfDistiller::CancelJob()
{
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}
