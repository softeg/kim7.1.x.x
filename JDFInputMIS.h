#pragma once

#include "iostream"
#include "map"

#define PROJ_JDFTOOLSLIB
#include <jdf/util/PlatformUtils.h>
#include "JDF.h"

using namespace std;
using namespace JDF;



class CJDFInputMIS
{
public:
	CJDFInputMIS(void);
	~CJDFInputMIS(void);


public:
	JDFParser* parser;
	JDFDoc*	m_pJDFDoc;	
	CString m_strProtocol;
	CString m_strAgent;


public:
	BOOL	     Open(CString strJDFFile);
	BOOL	 	 ProcessProductGlobal(JDFNode node);
	CString		 GetPageFormatName(float fWidth, float fHeight);
	void		 ParseCustomerInfo(JDFNode node);
	BOOL		 ProcessProductGroup(JDFNode node);
	BOOL		 ProcessProcessGroup(JDFNode node);
	BOOL		 ProcessStrippingParams(JDFNode node, CBoundProductPart& rProductPart, int nProductPartIndex, CPrintingProfile& rPrintingProfile);
	CPrintSheet* StripSheet(JDFStrippingParams strippingParams, int& nPartMapPos, int nProductPartIndex, CString strSignatureName, float& fPaperWidth, float& fPaperHeight, float& fPlateWidth, float& fPlateHeight, CPrintingProfile& rPrintingProfile);
	void		 AssignJDFStripCellParams(CBoundProductPart& rProductPart, JDFStripCellParams stripCellParams);
	void		 GetJDFStripCellParams(float& fPageWidth, float& fPageHeight, float& fOverFold, int& nOverFoldSide, float& fHeadTrim, float& fFootTrim, float& fSideTrim, float& fSpine, JDFStripCellParams stripCellParams);
	void		 StrippingGetSheetMedia(JDFStrippingParams strippingParams, float& fPaperWidth, float& fPaperHeight, CBoundProductPart& rProductPart);
	void		 StrippingGetPlateMedia(JDFStrippingParams strippingParams, float& fPlateWidth, float& fPlateHeight);
	void		 StrippingGetPressDevice(JDFStrippingParams strippingParams, CString& strPressDeviceName, CString& strPressDeviceID);
	BOOL		 ProcessPrinanceBullShit(JDFNode node, int nProductPartIndex);
	int			 MapOrientation(int nJDFOrientation);
	void		 JDFStripCalcSheetDims(JDFStrippingParams part, CLayout* pLayout, CFoldSheet& rFoldSheet, float& fWidth, float& fHeight);
	BOOL		 JDFStripFoldSheet(CPrintSheet* pPrintSheet, JDFStrippingParams strippingParams, int nPartMapPos, CFoldSheet* pFoldSheet, int nLayerIndex, float fFoldSheetPosX, float fFoldSheetPosY, CBoundProductPart& rProductPart, int nOrientation);
	void		 JDFAddFoldSheetLayer(CLayout* pLayout, CFoldSheet* pFoldSheet, int nLayerIndex, float fFoldSheetPosX, float fFoldSheetPosY, CBoundProductPart& rProductPart, int nOrientation, JDFStrippingParams strippingParams, int nPartMapPos);
	void		 JDFCreateFoldSheet(JDFBinderySignature binderySignature, CFoldSheet& rFoldSheet, int nProductPartIndex);
};
