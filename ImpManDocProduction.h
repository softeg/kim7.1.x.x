// ImpManDocProduction.h : interface of the CImpManDoc class
//
/////////////////////////////////////////////////////////////////////////////


#pragma once

#include "ImpManDocProducts.h"
#include "ImpManDocPrintGroups.h"
#include "PaperDefsSelection.h"


// CPunchingTool ///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CPunchingTool : public CObject
{
public:
    DECLARE_SERIAL( CPunchingTool )
	CPunchingTool();
	CPunchingTool(const CPunchingTool& rPunchingTool); // copy constructor

public:
	CString m_strToolName;
	int		m_nToolID;
	int		m_nPunchesX;
	int		m_nPunchesY;
	int		m_nNXOptions;
	int		m_nNYOptions;
	float	m_fStepX;
	float	m_fStepY;
	float	m_fGutterX;
	float	m_fGutterY;
	float	m_fOffsetRows;
	float	m_fOffsetCols;
	BOOL	m_bRotateRows;
	BOOL	m_bRotateCols;
	int		m_nOffsetRowsOddEven;
	int		m_nOffsetColsOddEven;
	int		m_nRotateRowsOddEven;
	int		m_nRotateColsOddEven;

public:
	const CPunchingTool& operator= (const CPunchingTool& rPunchingTool);
	BOOL				 operator==(const CPunchingTool& rPunchingTool);
	BOOL				 operator!=(const CPunchingTool& rPunchingTool);
	void			     Copy(const CPunchingTool& rPunchingTool);
	void			     Serialize( CArchive& ar );
	static CString		 FindFolder(CString strFolder, CString strPunchToolName);
	static BOOL			 FolderHasPunchTool(const CString& strFolder, const CString& strPunchTool);
	void				 Load(CString strPunchToolName, CString strPath = _T(""));
	void				 Save(CString strPunchToolName, CString strPath = _T(""));
	int					 AssignID();
	int					 IsNull() { return (m_strToolName.IsEmpty()) ? TRUE : FALSE; };
	float				 GetCenterPosX(int nX, int nY);
	float				 GetCenterPosY(int nX, int nY);
	int					 GetHeadPosition(int nX, int nY);
	BOOL				 CheckToAdd(int nX, int nY, int nPunchesX, int nPunchesY);
	int					 CalcMaxNUpX(float fBoxWidth, float fObjectWidth);
	int					 CalcMaxNUpY(float fBoxHeight, float fObjectHeight);
	float				 CalcBoxWidth (float fObjectWidth,  int nNumX = -1);
	float				 CalcBoxHeight(float fObjectHeight, int nNumY = -1);
	CRect				 DrawPreview(CDC* pDC, CRect rcFrame, class CFlatProduct* pFlatProduct = NULL, class CPageTemplate* pPageTemplate = NULL, BOOL bShowBitmap = TRUE);
	CRect				 DrawSinglePunch(CDC* pDC, float fCenterPosX, float fCenterPosY, int nHeadPosition, float fProductWidth, float fProductHeight, class CFlatProduct* pFlatProduct = NULL, class CPageTemplate* pPageTemplate = NULL, BOOL bShowBitmap = TRUE);
};

template <> void AFXAPI SerializeElements <CPunchingTool> (CArchive& ar, CPunchingTool* pPunchingTool, INT_PTR nCount);


// CPunchingProcess ///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CPunchingProcess : public CObject
{
public:
    DECLARE_SERIAL( CPunchingProcess )
	CPunchingProcess();
	CPunchingProcess(const CPunchingProcess& rPunchingProcess); // copy constructor

public:
	CArray<CPunchingTool, CPunchingTool&> m_punchingTools;

public:
	const CPunchingProcess& operator= (const CPunchingProcess& rPunchingProcess);
	BOOL					operator==(const CPunchingProcess& rPunchingProcess);
	BOOL					operator!=(const CPunchingProcess& rPunchingProcess);
	void				    Copy(const CPunchingProcess& rPunchingProcess);
	void					Serialize( CArchive& ar );
	BOOL					ApplyModifications(class CLayout* pLayout, CPunchingProcess& rOldPunchingProcess, BOOL& bDoReImpose);
	CPunchingTool&			GetPunchingTool(int nID);
	CRect					DrawData(CDC* pDC, CRect rcFrame, class CPrintSheet* pPrintSheet, CDisplayList* pDisplayList);
};


// CTrimmingProcess ///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CTrimmingProcess : public CObject
{
public:
    DECLARE_SERIAL( CTrimmingProcess )
	CTrimmingProcess();
	CTrimmingProcess(const CTrimmingProcess& rTrimmingProcess); // copy constructor

protected:
	CString	m_strID;

public:
	float	m_fHeadTrim;
	float	m_fSideTrim;
	float	m_fFootTrim;
	CString m_strMarkset;

public:
	const CTrimmingProcess& operator= (const CTrimmingProcess& rTrimmingProcess);
	BOOL					operator==(const CTrimmingProcess& rTrimmingProcess);
	BOOL					operator!=(const CTrimmingProcess& rTrimmingProcess);
	void					SetID(const CString& strID) { m_strID = strID; };
	CString&				GetID() { return m_strID; };
	void				    Copy(const CTrimmingProcess& rTrimmingProcess);
	void					Serialize( CArchive& ar );
	int					    IsNull() { return (m_strID.IsEmpty()) ? TRUE : FALSE; };
	BOOL					ApplyModifications(class CLayout* pLayout, CTrimmingProcess& rOldTrimmingProcess, BOOL& bDoReImpose);
	CRect					DrawData(CDC* pDC, CRect rcFrame, class CPrintSheet* pPrintSheet, CPrintComponent& rComponent, CDisplayList* pDisplayList);
};

class CBindingDefs;

// CBindingProcess ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CBindingProcess : public CObject
{
public:
    DECLARE_SERIAL( CBindingProcess )
	CBindingProcess();
	CBindingProcess(const CBindingProcess& rBindingProcess); // copy constructor

enum {OverFoldNone, OverFoldFront, OverFoldBack};

protected:
	CString		 m_strID;

public:
	CBindingDefs m_bindingDefs;
	float		 m_fSpineArea;
	BOOL		 m_bSpineAuto;
	float		 m_fMillingDepth;
	float		 m_fGlueLineWidth;
	float		 m_fOverFold;
	int			 m_nOverFoldSide;
	BOOL		 m_bComingAndGoing;
	CString		 m_strMarkset;


public:
	const CBindingProcess& operator= (const CBindingProcess& rBindingProcess);
	BOOL				   operator==(const CBindingProcess& rBindingProcess);
	BOOL				   operator!=(const CBindingProcess& rBindingProcess);
	void				   SetID(const CString& strID) { m_strID = strID; };
	CString&			   GetID() { return m_strID; };
	void				   Copy(const CBindingProcess& rBindingProcess);
	void				   Serialize( CArchive& ar );
	int					   IsNull() { return (m_strID.IsEmpty()) ? TRUE : FALSE; };
	BOOL				   ApplyModifications(CLayout* pLayout, CBindingProcess& rOldBindingProcess, BOOL& bDoReImpose);
	float				   GetSpine(class CBoundProductPart& rProductPart);
	float				   GetOverfold(int nSide);
	CRect				   DrawData(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPrintComponent& rComponent, CDisplayList* pDisplayList);
};

// CFoldingProcess ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CFoldingProcess : public CObject
{
public:
    DECLARE_SERIAL( CFoldingProcess )
	CFoldingProcess();
	CFoldingProcess(const CFoldingProcess& rFoldingProcess); // copy constructor

protected:
	CString			m_strID;

public:
	CFoldSheetList	m_foldSchemeList;
	BOOL			m_bShinglingActive;
	int				m_nShinglingMethod;
	int				m_nShinglingDirection;
	BOOL			m_bShinglingCropMarks;
	CString			m_strMarkset;

protected:
	float			m_fShinglingValueX;
	float			m_fShinglingValueXFoot;
	float			m_fShinglingValueY;

public:
	const CFoldingProcess& operator= (const CFoldingProcess& rFoldingProcess);
	BOOL				   operator==(const CFoldingProcess& rFoldingProcess);
	BOOL				   operator!=(const CFoldingProcess& rFoldingProcess);
	void				   SetID(const CString& strID) { m_strID = strID; };
	CString&			   GetID() { return m_strID; };
	void				   Copy(const CFoldingProcess& rFoldingProcess);
	void				   Serialize( CArchive& ar );
	int					   IsNull() { return (m_strID.IsEmpty()) ? TRUE : FALSE; };
	CFoldSheet&			   FindBestFoldScheme(int nNumPagesToImpose, CPrintSheet* pPrintSheet, class CProductionProfileLayoutPool& rLayoutPool, int& nPoolIndex);
	CFoldSheet&			   FindBestFoldScheme(int nNumPagesToImpose, CSize matrix = CSize(0, 0));
	float				   GetShinglingValueX(CBoundProductPart& rProductPart);
	float				   GetShinglingValueXFoot(CBoundProductPart& rProductPart);
	float				   GetShinglingValueY(CBoundProductPart& rProductPart);
	void				   SetShinglingValueX(float fValue) { m_fShinglingValueX = fValue; };
	void				   SetShinglingValueXFoot(float fValue) { m_fShinglingValueXFoot = fValue; };
	void				   SetShinglingValueY(float fValue) { m_fShinglingValueY = fValue; };
	CRect				   DrawData(CDC* pDC, CRect rcFrame, CFoldSheet* pFoldSheet, CBoundProductPart* pProductPart, CDisplayList* pDisplayList);
};

// CCuttingProcess ////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CCuttingProcess : public CObject
{
public:
    DECLARE_SERIAL( CCuttingProcess )
	CCuttingProcess();
	CCuttingProcess(const CCuttingProcess& rCuttingProcess); // copy constructor


protected:
	CString		m_strID;

public:
	int			m_nBaseCutDirection;
	int			m_nBaseCutAlignment;
	float		m_fLeftBorder;
	float		m_fLowerBorder;
	float		m_fRightBorder;
	float		m_fUpperBorder;
	CString		m_strMarkset;

public:
	const CCuttingProcess& operator= (const CCuttingProcess& rCuttingProcess);
	BOOL				   operator==(const CCuttingProcess& rCuttingProcess);
	BOOL				   operator!=(const CCuttingProcess& rCuttingProcess);
	void				   SetID(const CString& strID) { m_strID = strID; };
	CString&			   GetID() { return m_strID; };
	void				   Copy(const CCuttingProcess& rCuttingProcess);
	void				   Serialize( CArchive& ar );
	int					   IsNull() { return (m_strID.IsEmpty()) ? TRUE : FALSE; };
	BOOL				   ApplyModifications(CLayout* pLayout, CCuttingProcess& rOldCuttingProcess, BOOL& bDoReImpose);
	CRect				   DrawData(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CDisplayList* pDisplayList);
};



// CPrepressParams ////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CPrepressParams : public CObject
{
public:
    DECLARE_SERIAL( CPrepressParams )
	CPrepressParams();
	CPrepressParams(const CPrepressParams& rPrepressParams); // copy constructor

protected:	
	CString m_strID;

public:
	float	m_fBleed;
	BOOL	m_bAdjustContentToMask;
	CString m_strPrintSheetOutputProfile;
	CString m_strFoldSheetOutputProfile;
	CString m_strBookletOutputProfile;
	float	m_fMaxOverProduction;

public:
	const CPrepressParams& operator=  (const CPrepressParams& rPrepressParams);
	BOOL				   operator== (const CPrepressParams& rPrepressParams);
	BOOL				   operator!= (const CPrepressParams& rPrepressParams);
	void				   SetID(const CString& strID) { m_strID = strID; };
	CString&			   GetID() { return m_strID; };
	void				   Copy(const CPrepressParams& rPrepressParams);
	BOOL				   ApplyModifications(CLayout* pLayout, CPrepressParams& rOldPrepressParams, BOOL& bDoReImpose);
	void				   Serialize( CArchive& ar );
	int					   IsNull() { return (m_strID.IsEmpty()) ? TRUE : FALSE; };
};

// CPressParams ////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CPressParams : public CObject
{
public:
    DECLARE_SERIAL( CPressParams )
	CPressParams();
	CPressParams(const CPressParams& rPressParams); // copy constructor

protected:
	CString						m_strID;

public:
	int			 				m_nWorkStyle;
	int			 				m_nTurnStyle;
	int			 				m_nRefEdge;
	int			 				m_nNumColorStationsFront;
	int			 				m_nNumColorStationsBack;
	CPressDevice 				m_pressDevice;
	CPressDevice 				m_backSidePressDevice;
	CSheet		 				m_sheetData;
	CArray <CSheet, CSheet&>	m_availableSheets;
	CString						m_strMarkset;

public:
	const CPressParams& operator=  (const CPressParams& rPressParams);
	BOOL				operator== (const CPressParams& rPressParams);
	BOOL				operator!= (const CPressParams& rPressParams);
	void				SetID(const CString& strID) { m_strID = strID; };
	CString&			GetID() { return m_strID; };
	void				Copy(const CPressParams& rPressParams);
	void				Serialize( CArchive& ar );
	int					IsNull() { return (m_strID.IsEmpty()) ? TRUE : FALSE; };
	BOOL				ApplyModifications(CLayout* pLayout, CPressDevice& rOldPressDevice, BOOL& bDoReImpose);
	CRect				DrawData(CDC* pDC, CRect rcFrame, CLayout* pLayout, CDisplayList* pDisplayList = NULL, BOOL bSmallVersion = FALSE);
};

// CPostpressParams ////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CPostpressParams : public CObject
{
public:
    DECLARE_SERIAL( CPostpressParams )
	CPostpressParams();
	CPostpressParams(const CPostpressParams& CPostpressParams); // copy constructor

public:
	CTrimmingProcess	m_trimming;
	CBindingProcess		m_binding;
	CFoldingProcess		m_folding;
	CCuttingProcess		m_cutting;
	CPunchingProcess	m_punching;


public:
	const CPostpressParams& operator=  (const CPostpressParams& rPostpressParams);
	BOOL					operator== (const CPostpressParams& rPostpressParams);
	BOOL					operator!= (const CPostpressParams& rPostpressParams);
	void					Copy(const CPostpressParams& rPostpressParams);
	void					Serialize( CArchive& ar );
};


// CProductionProfileLayoutTemplate /////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CProductionProfileLayoutTemplate : public CObject
{
public:
    DECLARE_SERIAL( CProductionProfileLayoutTemplate )
	CProductionProfileLayoutTemplate();
	CProductionProfileLayoutTemplate(const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate); // copy constructor

public:
	CLayout		m_layout;
	CPrintSheet m_printSheet;


public:
	const CProductionProfileLayoutTemplate& operator=  (const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate);
	BOOL									operator== (const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate);
	BOOL									operator!= (const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate);
	void									Copy	   (const CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate);
};

// CProductionProfileLayoutPool /////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
class CProductionProfileLayoutPool : public CArray <CProductionProfileLayoutTemplate, CProductionProfileLayoutTemplate&>
{
public:
    DECLARE_SERIAL( CProductionProfileLayoutPool )
	CProductionProfileLayoutPool();
	CProductionProfileLayoutPool(const CProductionProfileLayoutPool& rProductionProfileLayoutPool); // copy constructor



public:
	const CProductionProfileLayoutPool& operator=  (const CProductionProfileLayoutPool& rProductionProfileLayoutPool);
	BOOL								operator== (const CProductionProfileLayoutPool& rProductionProfileLayoutPool);
	BOOL								operator!= (const CProductionProfileLayoutPool& rProductionProfileLayoutPool);
	void								Copy	   (const CProductionProfileLayoutPool& rProductionProfileLayoutPool);
	void								Add(CProductionProfileLayoutTemplate& rProductionProfileLayoutTemplate);
	int									FindSuitableLayout(CFoldSheet& rFoldSheet, int nNumPagesToImpose);
};

template <> void AFXAPI SerializeElements <CProductionProfileLayoutTemplate> (CArchive& ar, CProductionProfileLayoutTemplate* pProductionProfileLayoutTemplate, INT_PTR nCount);





// CProductionProfile ////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

class CProductionProfile : public CObject
{
public:
    DECLARE_SERIAL( CProductionProfile )
	CProductionProfile();
	CProductionProfile(const CProductionProfile& rProductionProfile); // copy constructor

	enum ProductionType {Single, Unknown, ComingAndGoing};
	enum Processes {Prepress = 1, Printing = 2, Cutting = 4, Folding = 8, Binding = 16, Trimming = 32, Punching = 64, Rules = 1024, LayoutPool = 2048};

private:
	struct displayInfo
	{
		CRect		rcDisplayBox;
	}				m_dspi;


public:
	CString						 m_strName;
	CString						 m_strComment;
	int							 m_nProductionType;
	CProductionProfileLayoutPool m_layoutPool;

	CPrepressParams				m_prepress;
	CPressParams				m_press;
	CPostpressParams			m_postpress;
	CArray<int, int>			m_activeProcesses;
	CKimRules					m_rules;

public:
	const CProductionProfile& operator=  (const CProductionProfile& rProductionProfile);
	BOOL					  operator== (const CProductionProfile& rProductionProfile);
	BOOL					  operator!= (const CProductionProfile& rProductionProfile);
	void				Copy(const CProductionProfile& rProductionProfile);
	BOOL				IsNull(){ return (m_press.m_pressDevice.m_strName.IsEmpty()) ? TRUE : FALSE; };
	int					GetIndex();
	CRect				GetDisplayBox() { return m_dspi.rcDisplayBox; };
	void				SetDisplayBox(CRect& rcBox) { m_dspi.rcDisplayBox = rcBox; };
	CLayout*			GetLayout();
	BOOL				ApplyModifications(CProductionProfile& rOldProfile);
	BOOL				ProcessIsActive(int nProcess);
	CRect				DrawInfo			  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground);
	void				DrawPlanningTableLinks(CDC* pDC, BOOL bHighlight, int& nVerticalAxis, int& nMaxSourceRight, CDisplayList* pDisplayList = NULL);
};

template <> void AFXAPI SerializeElements <CProductionProfile> (CArchive& ar, CProductionProfile* pProductionProfile, INT_PTR nCount);




// CProductionProfiles ////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

class CProductionProfiles : public CList <CProductionProfile, CProductionProfile&>
{
public:
    DECLARE_SERIAL( CProductionProfiles )
	CProductionProfiles();
	~CProductionProfiles();

public:
	CProductionProfile m_nullProductionProfile;


public:
	void				Serialize( CArchive& ar );
	CProductionProfile&	GetProductionProfile(int nID);
	CProductionProfile& FindSuitable(CPrintGroup& rPrintGroup);
	CProductionProfile& FindProfile(CString strProfileName);
	void				RemoveProfile(CString strProfileName);
	BOOL				RuleExpressionIsTrue(const CString& strVariable, const CString& strOperator, const CString& strValue, CPrintGroup& rPrintGroup);
};




// CProductionSetup ////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

class CProductionSetup : public CObject
{
public:
    DECLARE_SERIAL( CProductionSetup )
	CProductionSetup();
	CProductionSetup(const CProductionSetup& rProductionSetup); // copy constructor

	enum resourceTypes { ResourcePaper = 1};
	enum processTypes  { ProcessPrepress = 1, ProcessPress, ProcessCutting, ProcessFolding, ProcessBinding, ProcessTrimming };

	typedef struct
{
	int nQuantityMin;
	int nQuantityMax;
}QUANTITY_RANGE;

public:
	CString											m_strName;
	CString											m_strComment;
	CString											m_strProductionProfile;
	CArray <CPaperDefs,		  CPaperDefs&>			m_paperData;
	CArray <CPrepressParams,  CPrepressParams&>		m_prepressData;
	CArray <CPressParams,	  CPressParams&>		m_pressData;
	CArray <CCuttingProcess,  CCuttingProcess&>		m_cuttingData;
	CArray <CFoldingProcess,  CFoldingProcess&>		m_foldingData;
	CArray <CBindingProcess,  CBindingProcess&>		m_bindingData;
	CArray <CTrimmingProcess, CTrimmingProcess&>	m_trimmingData;
	CKimRules										m_rules;
	float											m_fMaxOverProduction;
	BOOL											m_bOneTypePerSheet;
	CArray <QUANTITY_RANGE, QUANTITY_RANGE&>		m_quantityRanges;

public:
	const CProductionSetup& operator=  (const CProductionSetup& rProductionSetup);
	BOOL				    operator== (const CProductionSetup& rProductionSetup);
	BOOL				    operator!= (const CProductionSetup& rProductionSetup);
	void					Copy(const CProductionSetup& rProductionSetup);
	BOOL					IsNull(){ return (m_strName.IsEmpty()) ? TRUE : FALSE; };
	void					Serialize( CArchive& ar );
	void					Load(const CString& strSetupName);
	void					Save(const CString& strSetupName);
	static void				Remove(const CString& strSetupName);
	int						FindResourceIndex(const CString& strID, int nType);
	int						FindProcessIndex(const CString& strID, int nType);
	CPaperDefs				GetPaperResource		(const CString& strID);
	CPrepressParams			GetPrepressProcess		(const CString& strID);
	CPressParams			GetPressProcess			(const CString& strID);
	CCuttingProcess			GetCuttingProcess		(const CString& strID);
	CFoldingProcess			GetFoldingProcess		(const CString& strID);
	CBindingProcess			GetBindingProcess		(const CString& strID);
	CTrimmingProcess		GetTrimmingProcess		(const CString& strID);
	void					AddResource		(CObject& rResource);
	void					AddProcess		(CObject& rProcess);
	void					RemoveResource	(const CString& strID, int nType);
	void					RemoveProcess	(const CString& strID, int nType);
	void					ModifyResource	(const CString& strID, CObject& rResource);
	void					ModifyProcess	(const CString& strID, CObject& rProcess);
};



// CPrintingProfile ////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

class CPrintingProfile : public CObject
{
public:
    DECLARE_SERIAL( CPrintingProfile )
	CPrintingProfile();
	CPrintingProfile(const CPrintingProfile& rPrintingProfile); // copy constructor

	enum ProductionType {Single, Unknown, ComingAndGoing};
	enum Processes {Prepress = 1, Printing = 2, Cutting = 4, Folding = 8, Binding = 16, Trimming = 32, Punching = 64, Rules = 1024, LayoutPool = 2048};

private:
	struct displayInfo
	{
		CRect		rcDisplayBox;
	}				m_dspi;


public:
	CString						 m_strName;
	CString						 m_strComment;
	int							 m_nProductionType;
	CString						 m_strMarkset;
	CProductionProfileLayoutPool m_layoutPool;

	CPrepressParams				m_prepress;
	CPressParams				m_press;
	CCuttingProcess				m_cutting;
	CArray<int, int>			m_activeProcesses;

public:
	const CPrintingProfile&	operator=  (const CPrintingProfile& rPrintingProfile);
	BOOL					operator== (const CPrintingProfile& rPrintingProfile);
	BOOL					operator!= (const CPrintingProfile& rPrintingProfile);
	void				Copy(const CPrintingProfile& rPrintingProfile);
	BOOL				IsNull(){ return (m_press.m_pressDevice.m_strName.IsEmpty()) ? TRUE : FALSE; };
	int					GetIndex();
	CRect				GetDisplayBox() { return m_dspi.rcDisplayBox; };
	void				SetDisplayBox(CRect& rcBox) { m_dspi.rcDisplayBox = rcBox; };
	CLayout*			GetLayout();
	void				AssignProductionProfilePool(CProductionProfile& rProductionProfile);
	BOOL				ApplyModifications(CPrintingProfile& rOldProfile);
	BOOL				ProcessIsActive(int nProcess);
	CRect				DrawInfo			  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground);
	void				DrawPlanningTableLinks(CDC* pDC, BOOL bHighlight, int& nVerticalAxis, int& nMaxSourceRight, CDisplayList* pDisplayList = NULL);
};

template <> void AFXAPI SerializeElements <CPrintingProfile> (CArchive& ar, CPrintingProfile* pPrintingProfile, INT_PTR nCount);




// CPrintingProfiles ////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

class CPrintingProfiles : public CList <CPrintingProfile, CPrintingProfile&>
{
public:
    DECLARE_SERIAL( CPrintingProfiles )
	CPrintingProfiles();
	~CPrintingProfiles();


public:
	void			  Serialize( CArchive& ar );
	CPrintingProfile& GetPrintingProfile(int nID);
	CPrintingProfile& FindProfile(CString strProfileName);
	void			  RemoveProfile(CString strProfileName);
};

