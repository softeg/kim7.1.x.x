// AutoServerFrame.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "AutoServerToolsView.h"
#include "AutoServerJobsView.h"
#include "AutoServerWorkflowsView.h"
#include "AutoServerLogsView.h"
#include "AutoServerDetailsView.h"
#include "AutoServerFrame.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "MainFrm.h"
#include "DummyView.h"




IMPLEMENT_DYNAMIC(CAutoServerSplitter, CSplitterWnd)

BEGIN_MESSAGE_MAP(CAutoServerSplitter, CSplitterWnd)
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()

// CProdDataSplitter message handlers
afx_msg LRESULT CAutoServerSplitter::OnNcHitTest(CPoint point)
{
	//COrderDataFrame* pFrame = (COrderDataFrame*)GetParentFrame();
	//if (pFrame)
	//{
	//	if (this == &pFrame->m_wndSplitter2)
	//	{
	//		CWnd* pPane = pFrame->m_wndSplitter2.GetPane(0, 0);
	//		CRect rcWindow1, rcWindow2;
	//		pPane->GetWindowRect(rcWindow1);
	//		pPane = pFrame->m_wndSplitter2.GetPane(0, 1);
	//		pPane->GetWindowRect(rcWindow2);
	//		if ( (rcWindow1.right <= point.x) && (point.x <= rcWindow2.left) )
	//			return CSplitterWnd::OnNcHitTest(point);
	//	}
	//}

	return CSplitterWnd::OnNcHitTest(point);

	//return HTNOWHERE;
}

void CAutoServerSplitter::OnSize(UINT nType, int cx, int cy)
{
}

void CAutoServerSplitter::OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect)
{
	if (pDC == NULL)
	{
		RedrawWindow(rect, NULL, RDW_INVALIDATE|RDW_NOCHILDREN);
		return;
	}

	CRect rcRect = rect;
	switch (nType)
	{
	case splitBorder:
		{
			CAutoServerFrame* pFrame = (CAutoServerFrame*)GetParentFrame();
			if (pFrame)
			{
				if (this == &pFrame->m_wndSplitter)
				{
					CRect rcBorder = rcRect;
					ClientToScreen(rcBorder);

					CWnd* pPane0 = pFrame->m_wndSplitter.GetPane(0, 0);
					CWnd* pPane1 = pFrame->m_wndSplitter.GetPane(1, 0);
					CRect rcPane0, rcPane1;
					pPane0->GetWindowRect(rcPane0);
					pPane1->GetWindowRect(rcPane1);

					if (rcBorder.PtInRect(rcPane0.CenterPoint()))
					{
						pDC->Draw3dRect(rcRect, MIDDLEGRAY, LIGHTGRAY);
						rcRect.InflateRect(0, -1); 
						pDC->Draw3dRect(rcRect, RGB(211,218,228), RGB(211,218,228));
						rcRect.InflateRect(0, -1); rcRect.left++;
						pDC->Draw3dRect(rcRect, RGB(211,218,228), RGB(211,218,228));
						break;
					}
					if (rcBorder.PtInRect(rcPane1.CenterPoint()))
					{
						CPen pen(PS_SOLID, 1, MIDDLEGRAY);
						pDC->SelectObject(&pen);
						pDC->MoveTo(rcRect.left-4, rcRect.top);			pDC->LineTo(rcRect.right, rcRect.top);
						pen.DeleteObject();
						pen.CreatePen(PS_SOLID, 1, LIGHTGRAY);
						pDC->SelectObject(&pen);
						pDC->MoveTo(rcRect.left-4, rcRect.bottom - 1);	pDC->LineTo(rcRect.right + 4, rcRect.bottom - 1);
						pen.DeleteObject();
						break;
					}
				}
			}
			pDC->Draw3dRect(rcRect, MIDDLEGRAY, LIGHTGRAY);
			rcRect.InflateRect(-1, -1);
			pDC->Draw3dRect(rcRect, RGB(209,215,226), RGB(225,230,235));
		}
		return;

	case splitIntersection:
		break;

	case splitBox:
		break;

	case splitBar:
		{
			COLORREF crColor = RGB(165,190,250);
			CAutoServerFrame* pFrame = (CAutoServerFrame*)GetParentFrame();
			if (pFrame)
			{
				if (this == &pFrame->m_wndSplitter)
				{
					CRect rcBar = rcRect;
					CWnd* pPane0 = pFrame->m_wndSplitter.GetPane(0, 0);
					CWnd* pPane1 = pFrame->m_wndSplitter.GetPane(1, 0);
					CRect rcPane0, rcPane1;
					pPane0->GetWindowRect(rcPane0);
					pPane1->GetWindowRect(rcPane1);
					if ( (rcBar.CenterPoint().x >= rcPane0.right) && (rcBar.CenterPoint().x <= rcPane1.left) )
					{
						rcBar.DeflateRect(0, 1);
						pDC->FillSolidRect(rcBar, RGB(211,218,228));//RGB(240,240,240));//RGB(230,233,238));
						break;
					}
				}

				pDC->FillSolidRect(rcRect, crColor);
			}
		}
		break;

	default:
		ASSERT(FALSE);  // unknown splitter type
	}
}

void CAutoServerSplitter::StopTracking(BOOL bAccept)
{
	if (m_bTracking && bAccept)
	{
		CSplitterWnd::StopTracking(bAccept);
	
		CAutoServerFrame* pFrame = (CAutoServerFrame*)GetParentFrame();
		if (pFrame)
			if (pFrame->IsKindOf(RUNTIME_CLASS(CAutoServerFrame)))
			{
				if (pFrame->m_wndSplitter1.GetPane(0, 0))
				{
					CRect rcView;
					pFrame->m_wndSplitter1.GetPane(0, 0)->GetClientRect(rcView);
					pFrame->m_nJobViewWidth = max(rcView.Width(), 100);
					if (pFrame->GetAutoServerToolsView())
						pFrame->GetAutoServerToolsView()->SendMessage(WM_SIZE);
					if (pFrame->GetAutoServerJobsView())
						pFrame->GetAutoServerJobsView()->OnUpdate(NULL, 0, NULL);
					pFrame->RearrangeSplitters();
				}
			}
	}
	else
		CSplitterWnd::StopTracking(bAccept);
}

void CAutoServerSplitter::TrackColumnSize(int x, int col)
{
	//if (col == 0)
	//{
	//	CFrameWnd* pFrame = GetParentFrame();
	//	if (pFrame)
	//		if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
	//		{
	//			CProductNavigationView* pView = (CProductNavigationView*)((COrderDataFrame*)pFrame)->m_wndSplitter.GetPane(1, 0);
	//			CRect rcButtons = pView->m_productDataSelectorButtons.GetButtonClientRect(pView->m_productDataSelectorButtons.GetSize() - 1);
	//			if (x < rcButtons.right + 5)
	//				x = rcButtons.right + 5;
	//		}
	//}

	CSplitterWnd::TrackColumnSize(x, col);

	//RecalcLayout();
}



// CAutoServerFrame

IMPLEMENT_DYNCREATE(CAutoServerFrame, CMDIChildWnd)

CAutoServerFrame::CAutoServerFrame()
{
	m_bStatusRestored = FALSE;
}

CAutoServerFrame::~CAutoServerFrame()
{
	//theApp.m_kaDB.kA_Disconnect();

	//_tcscpy_s(theApp.GetDataFolder(), _MAX_PATH, theApp.m_strClientOldDataFolder.GetBuffer());
	//theApp.m_bServerConnected = FALSE;

	UpdateDemoStatusBarPane(CImpManDoc::GetDoc());
}


BEGIN_MESSAGE_MAP(CAutoServerFrame, CMDIChildWnd)
	ON_WM_SIZE()
	ON_WM_ACTIVATE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CAutoServerFrame-Meldungshandler

BOOL CAutoServerFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	if ( ! m_wndSplitter.CreateStatic(this, 2, 1))
		return FALSE;

	if ( ! m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CAutoServerToolsView),  CSize(0, 35),  pContext))
		return FALSE;

	if ( ! m_wndSplitter1.CreateStatic(&m_wndSplitter, 1, 2, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter.IdFromRowCol(1, 0)))
		return FALSE;
	if ( ! m_wndSplitter1.CreateView(0, 0, RUNTIME_CLASS(CAutoServerJobsView), CSize(600, 0), pContext))
		return FALSE;
	//if ( ! m_wndSplitter1.CreateView(0, 1, RUNTIME_CLASS(CAutoServerDetailsView), CSize(600, 0), pContext))
	//	return FALSE;

	if ( ! m_wndSplitter2.CreateStatic(&m_wndSplitter1, 2, 1, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter1.IdFromRowCol(0, 1)))
		return FALSE;
	if ( ! m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CAutoServerDetailsView), CSize(600, 0), pContext))
		return FALSE;
	if ( ! m_wndSplitter2.CreateView(1, 0, RUNTIME_CLASS(CDummyView), CSize(600, 0), pContext))
		return FALSE;

	//SetTimer(1, 10000, NULL);

	return TRUE;//CMDIChildWnd::OnCreateClient(lpcs, pContext);
}

void CAutoServerFrame::OnSize(UINT nType, int cx, int cy)
{
	if (m_wndSplitter.m_hWnd)
		RearrangeSplitters();

	CMDIChildWnd::OnSize(nType, cx, cy);
}

BOOL CAutoServerFrame::SetupViewJobs()
{
	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CDummyView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter1.DeleteView(0, 0);
	m_wndSplitter1.CreateView(0, 0, RUNTIME_CLASS(CAutoServerJobsView),	CSize(0, 0), &context);

	m_wndSplitter2.DeleteView(0, 0);
	m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CAutoServerDetailsView),	CSize(600, 0), &context);

	m_wndSplitter2.DeleteView(1, 0);
	m_wndSplitter2.CreateView(1, 0, RUNTIME_CLASS(CDummyView),	CSize(600, 0), &context);

	RearrangeSplitters();

	CAutoServerJobsView* pAutoServerJobsView = (CAutoServerJobsView*)m_wndSplitter1.GetPane(0, 0);
	pAutoServerJobsView->OnInitialUpdate();
	pAutoServerJobsView->Invalidate();
	pAutoServerJobsView->UpdateWindow();

	CAutoServerDetailsView* pAutoServerDetailsView = (CAutoServerDetailsView*)m_wndSplitter2.GetPane(0, 0);
	pAutoServerDetailsView->OnInitialUpdate();
	pAutoServerDetailsView->Invalidate();
	pAutoServerDetailsView->UpdateWindow();

	return TRUE;
}

BOOL CAutoServerFrame::SetupViewWorkflows()
{
	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CDummyView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter1.DeleteView(0, 0);
	m_wndSplitter1.CreateView(0, 0, RUNTIME_CLASS(CAutoServerWorkflowsView), CSize(0, 0), &context);

	m_wndSplitter2.DeleteView(0, 0);
	m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	m_wndSplitter2.DeleteView(1, 0);
	m_wndSplitter2.CreateView(1, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	RearrangeSplitters();

	CAutoServerWorkflowsView* pAutoServerWorkflowsView = (CAutoServerWorkflowsView*)m_wndSplitter1.GetPane(0, 0);
	pAutoServerWorkflowsView->OnInitialUpdate();
	pAutoServerWorkflowsView->Invalidate();
	pAutoServerWorkflowsView->UpdateWindow();

	return TRUE;
}

BOOL CAutoServerFrame::SetupViewLogs()
{
	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CDummyView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter1.DeleteView(0, 0);
	m_wndSplitter1.CreateView(0, 0, RUNTIME_CLASS(CAutoServerLogsView),	CSize(0, 0), &context);

	m_wndSplitter2.DeleteView(0, 0);
	m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	m_wndSplitter2.DeleteView(1, 0);
	m_wndSplitter2.CreateView(1, 0, RUNTIME_CLASS(CDummyView),	CSize(0, 0), &context);

	RearrangeSplitters();

	CAutoServerLogsView* pAutoServerLogsView = (CAutoServerLogsView*)m_wndSplitter1.GetPane(0, 0);
	pAutoServerLogsView->OnInitialUpdate();
	pAutoServerLogsView->Invalidate();
	pAutoServerLogsView->UpdateWindow();

	return TRUE;
}

void CAutoServerFrame::RearrangeSplitters()
{
	if (m_wndSplitter.m_pRowInfo)
	{
		CRect rcFrame;
		GetClientRect(rcFrame);

		int nHeight0 = 35;
		int nHeight1 = rcFrame.Height() - nHeight0;
		m_wndSplitter.SetRowInfo(0, (nHeight0 >= 0) ? nHeight0 : 0, 10);
		m_wndSplitter.SetRowInfo(1, (nHeight1 >= 0) ? nHeight1 : 0, 10);

		if (m_nJobViewWidth < 100)	// seems to be first call - not yet defined
			m_nJobViewWidth = 600;

		int nWidth0 = (m_wndSplitter2.GetPane(0, 0)->IsKindOf(RUNTIME_CLASS(CDummyView))) ? rcFrame.Width() : max(m_nJobViewWidth, 100);
		int nWidth1 = rcFrame.Width() - nWidth0;
		m_wndSplitter1.SetColumnInfo(0, (nWidth0 >= 0) ? nWidth0 : 0, 10);
		m_wndSplitter1.SetColumnInfo(1, (nWidth1 >= 0) ? nWidth1 : 0, 10);

		//nHeight0 = (rcFrame.Height() * 3) / 4;
		nHeight0 = rcFrame.Height();
		nHeight1 = rcFrame.Height() - nHeight0;
		m_wndSplitter2.SetRowInfo(0, (nHeight0 >= 0) ? nHeight0 : 0, 10);
		m_wndSplitter2.SetRowInfo(1, (nHeight1 >= 0) ? nHeight1 : 0, 10);

		m_wndSplitter.RecalcLayout();
		m_wndSplitter1.RecalcLayout();
		m_wndSplitter2.RecalcLayout();
	}
}

void CAutoServerFrame::ActivateFrame(int nCmdShow)
{
	//nCmdShow = SW_SHOWMAXIMIZED;
	CMainFrame::RestoreFrameStatus(this, m_bStatusRestored, nCmdShow, &m_wndSplitter, &m_wndSplitter1, &m_wndSplitter2, NULL, NULL, sizeof(m_nJobViewWidth), (LPBYTE)&m_nJobViewWidth);

	CMDIChildWnd::ActivateFrame(nCmdShow);
}

void CAutoServerFrame::OnDestroy()
{
	CMainFrame::SaveFrameStatus(this, &m_wndSplitter, &m_wndSplitter1, &m_wndSplitter2, NULL, NULL, sizeof(m_nJobViewWidth), (LPBYTE)&m_nJobViewWidth);

	CMDIChildWnd::OnDestroy();
}

int	CAutoServerFrame::GetActJobID()
{
	if (GetAutoServerJobsView())
		return GetAutoServerJobsView()->GetActJobID();

	return -1;
}

CAutoServerJobsView* CAutoServerFrame::GetAutoServerJobsView()
{
	if ( ! m_wndSplitter1.m_hWnd)
		return NULL;

	CView* pView = (CAutoServerJobsView*)m_wndSplitter1.GetPane(0, 0);
	if ( ! pView)
		return NULL;
	if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerJobsView)))
		return (CAutoServerJobsView*)pView;
	return NULL;
}

CAutoServerWorkflowsView* CAutoServerFrame::GetAutoServerWorkflowsView()
{
	if ( ! m_wndSplitter1.m_hWnd)
		return NULL;

	CView* pView = (CAutoServerWorkflowsView*)m_wndSplitter1.GetPane(0, 0);
	if ( ! pView)
		return NULL;
	if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerWorkflowsView)))
		return (CAutoServerWorkflowsView*)pView;
	return NULL;
}

CAutoServerLogsView* CAutoServerFrame::GetAutoServerLogsView()
{
	if ( ! m_wndSplitter1.m_hWnd)
		return NULL;

	CView* pView = (CAutoServerLogsView*)m_wndSplitter1.GetPane(0, 0);
	if ( ! pView)
		return NULL;
	if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerLogsView)))
		return (CAutoServerLogsView*)pView;
	return NULL;
}

CAutoServerDetailsView* CAutoServerFrame::GetAutoServerDetailsView()
{
	if ( ! m_wndSplitter1.m_hWnd)
		return NULL;

	return (CAutoServerDetailsView*)m_wndSplitter2.GetPane(0, 0);
}

CAutoServerToolsView* CAutoServerFrame::GetAutoServerToolsView()
{
	if ( ! m_wndSplitter.m_hWnd)
		return NULL;

	return (CAutoServerToolsView*)m_wndSplitter.GetPane(0, 0);
}

BOOL CAutoServerFrame::PreTranslateMessage(MSG* pMsg)
{
	CAutoServerToolsView* pView = GetAutoServerToolsView();
	if (pView)
		pView->UpdateDialogControls( this, TRUE );

	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_DELETE))
	{
		if (pView)
		{
			pView->SendMessage(WM_COMMAND, ID_AUTOSERVERVIEW_REMOVE);
			return TRUE;
		}
	}

	return CMDIChildWnd::PreTranslateMessage(pMsg);
}

void CAutoServerFrame::UpdateJobViews(int nJobID)
{
	int nActJobID = GetActJobID();	// get act job ID here, because could be changed in UpdateAutoServerView() when job has been removed by server

	theApp.UpdateAutoServerJobsView();

	if (nJobID >= 0)
		if (nJobID == nActJobID)
			theApp.UpdateAutoServerDetailsView();
}

void CAutoServerFrame::OnTimer(UINT_PTR nIDEvent)
{
	CMDIChildWnd::OnTimer(nIDEvent);
}
