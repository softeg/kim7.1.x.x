#pragma once


#include "OwnerDrawnButtonList.h"


typedef struct 
{
	CGIFControl* pGIFControl;
	int			 nID;
	int			 nType;
}STATUS_INDICATOR;


// CAutoServerDetailsView-Ansicht

class CAutoServerDetailsView : public CScrollView
{
	DECLARE_DYNCREATE(CAutoServerDetailsView)


public:
	DECLARE_DISPLAY_LIST(CAutoServerDetailsView, OnMultiSelect, NO, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CAutoServerDetailsView, JobInputFile,			OnSelect, NO,  OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CAutoServerDetailsView, JobInputFilePageRange,	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CAutoServerDetailsView, JobOutputFile,			OnSelect, NO,  OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)


protected:
	CAutoServerDetailsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CAutoServerDetailsView();

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

enum displayColIDs {ColJobProduct, ColJobFormat, ColJobInputFile, ColJobInputFilePageRange, ColJobOutputFile, ColJobOutputDate, ColJobOutputLocation};
typedef struct displayColumn
{
	int nID;
	int nWidth;
};
typedef CArray <struct displayColumn, struct displayColumn&> DISPLAY_COLUMN_LIST;

public:
	DISPLAY_COLUMN_LIST	m_displayColumns;


public:
	CSize											m_docSize;
	CHeaderCtrl  									m_HeaderCtrl;
	int			 									m_nHeaderHeight;
	int												m_nHeaderItemsTotalWidth;
	CFont		 									m_headerFont;
	CArray <int, int>								m_headerColumnWidths;
	int												m_nCurrentView;
	


public:
	void	SetHeaderColumnWidth(int nIndex, int nValue);
	int		GetHeaderColumnWidth(int nIndex, int nDefault);
	int		GetActJobID();
	CSize	CalcExtents();
	void	ClipHeader(CDC* pDC);
	CRect	Draw(CDC* pDC, CDisplayList* pDisplayList);
	CRect	DrawJobDetailsTable			(CDC* pDC, CDisplayList* pDisplayList = NULL);
	CRect	DrawJobProduct				(CDC* pDC, CRect rcFrame, kimAutoDB_product&    rProduct,  BOOL bShowProductName,	CDisplayList* pDisplayList);
	CRect	DrawJobProductFormat		(CDC* pDC, CRect rcFrame, kimAutoDB_product&    rProduct,  							CDisplayList* pDisplayList);
	CRect	DrawJobInputFile			(CDC* pDC, CRect rcFrame, kimAutoDB::_inputPDF& rInputPDF, 							CDisplayList* pDisplayList);
	CRect	DrawJobInputFilePageRange	(CDC* pDC, CRect rcFrame, kimAutoDB::_inputPDF& rInputPDF, 							CDisplayList* pDisplayList);
	CRect	DrawJobOutputFile			(CDC* pDC, CRect rcFrame, kimAutoDB_outputPDF& rOutputPDF, 							CDisplayList* pDisplayList);
	CRect	DrawJobOutputDate			(CDC* pDC, CRect rcFrame, kimAutoDB_outputPDF& rOutputPDF, 							CDisplayList* pDisplayList);
	CRect	DrawJobOutputLocation		(CDC* pDC, CRect rcFrame, kimAutoDB_outputPDF& rOutputPDF, 							CDisplayList* pDisplayList);
	void	ResetDisplayColums();
	void	AddDisplayColumn(int nColID, int nWidth);
	int		GetHeaderColumnPos(int nColID);



	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();     // Erster Aufruf nach Erstellung
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void ScrollToPosition(POINT pt);
	virtual void ScrollToDevicePosition(POINT ptDev);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
protected:
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnHeaderTrack	 (NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


