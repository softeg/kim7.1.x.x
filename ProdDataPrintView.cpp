// ProdDataPrintView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "ProdDataPrintView.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "NewPrintSheetFrame.h"
#include "PrintSheetMarksView.h"
#include "PrintSheetTableView.h"
#include "DlgMarkSet.h"
#include "DlgRemoveControlMarks.h"
#include "PrintSheetObjectPreview.h"
#include "GraphicComponent.h"



// CProdDataPrintView

IMPLEMENT_DYNCREATE(CProdDataPrintView, CFormView)

CProdDataPrintView::CProdDataPrintView()
	: CFormView(CProdDataPrintView::IDD)
{
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
	m_pCurrentLayout = NULL;
	m_pMarkImageList = new CImageList();
	m_pNewMarkPopupMenu = NULL;
	m_pToolTip			= NULL;
	m_bMarkStatus		= FALSE;
	m_boldFont.CreateFont(13, 0, 0, 0, FW_BOLD,	 FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CProdDataPrintView::~CProdDataPrintView()
{
	m_hollowBrush.DeleteObject();
	m_pMarkImageList->DeleteImageList();
	delete m_pMarkImageList; 
	delete m_pToolTip;
	m_boldFont.DeleteObject();
}

void CProdDataPrintView::DoDataExchange(CDataExchange* pDX) 
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PD_MARK_LIST, m_objectListCtrl);
	DDX_Check(pDX, IDC_PD_AUTOMARKS_ACTIVE, m_bAutoMarksActive);
	DDX_Check(pDX, IDC_MARK_SET_STATUS, m_bMarkStatus);
}

BEGIN_MESSAGE_MAP(CProdDataPrintView, CFormView)
	ON_WM_SIZE()
	ON_WM_MOUSEMOVE()
	ON_WM_CTLCOLOR()
	ON_NOTIFY(LVN_KEYDOWN, IDC_PD_MARK_LIST, &CProdDataPrintView::OnLvnKeydownPdMarkList)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PD_MARK_LIST, &CProdDataPrintView::OnItemchangedObjectList)
	ON_NOTIFY(NM_CLICK, IDC_PD_MARK_LIST, &CProdDataPrintView::OnClickObjectList)
	ON_BN_CLICKED(IDC_PD_MODIFY_MARK, &CProdDataPrintView::OnBnClickedPdModifyMark)
	ON_BN_CLICKED(IDC_PD_NEW_MARK, &CProdDataPrintView::OnBnClickedPdNewMark)
	ON_BN_CLICKED(IDC_PD_DUPLICATE_MARK, &CProdDataPrintView::OnBnClickedPdMarkDuplicate) 
	ON_BN_CLICKED(IDC_PD_DELETE_MARK, &CProdDataPrintView::OnBnClickedPdMarkDelete)
	ON_BN_CLICKED(IDC_PD_OPEN_MARKLIB, &CProdDataPrintView::OnBnClickedPdOpenMarklib)
	ON_BN_CLICKED(IDC_PD_DELETE_ALLMARKS, &CProdDataPrintView::OnBnClickedPdDeleteAllMarks)
	ON_BN_CLICKED(IDC_MARK_SET_STATUS, OnMarkSetStatus)
	ON_BN_CLICKED(IDC_MARK_UP, OnMarkUp)
	ON_BN_CLICKED(IDC_MARK_DOWN, OnMarkDown)
	ON_UPDATE_COMMAND_UI(IDC_MARK_SET_STATUS, OnUpdateMarkSetStatus)
	ON_UPDATE_COMMAND_UI(IDC_MARK_UP, OnUpdateMarkUp)
	ON_UPDATE_COMMAND_UI(IDC_MARK_DOWN, OnUpdateMarkDown)
	ON_UPDATE_COMMAND_UI(IDC_PD_MODIFY_MARK, OnUpdateModifyMark)
	ON_UPDATE_COMMAND_UI(IDC_PD_DUPLICATE_MARK, OnUpdateDuplicateMark)
	ON_UPDATE_COMMAND_UI(IDC_PD_DELETE_MARK, OnUpdateDeleteMark)
	ON_UPDATE_COMMAND_UI(IDC_PD_DELETE_ALLMARKS, OnUpdateDeleteAllMarks)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	ON_COMMAND(ID_NEWMARK_PDF, &CProdDataPrintView::OnNewMarkPDF)
	ON_COMMAND(ID_NEWMARK_TEXT, &CProdDataPrintView::OnNewMarkText)
	ON_COMMAND(ID_NEWMARK_SECTION, &CProdDataPrintView::OnNewMarkSection)
	ON_COMMAND(ID_NEWMARK_CROP, &CProdDataPrintView::OnNewMarkCrop)
	ON_COMMAND(ID_NEWMARK_FOLD, &CProdDataPrintView::OnNewMarkFold)
	ON_COMMAND(ID_NEWMARK_BARCODE, &CProdDataPrintView::OnNewMarkBarcode)
	ON_COMMAND(ID_NEWMARK_RECTANGLE, &CProdDataPrintView::OnNewMarkRectangle)
	ON_NOTIFY(NM_DBLCLK, IDC_PD_MARK_LIST, &CProdDataPrintView::OnNMDblclkPdMarkList)
	ON_NOTIFY(NM_SETFOCUS, IDC_PD_MARK_LIST, &CProdDataPrintView::OnNMSetfocusPdMarkList)
	ON_NOTIFY(NM_KILLFOCUS, IDC_PD_MARK_LIST, &CProdDataPrintView::OnNMKillfocusPdMarkList)
	ON_BN_CLICKED(IDC_PD_AUTOMARKS_ACTIVE, &CProdDataPrintView::OnBnClickedPdAutomarksActive)
	ON_BN_CLICKED(IDC_PD_OPEN_AUTOMARKLIB, &CProdDataPrintView::OnBnClickedPdOpenAutoMarklib)
	ON_BN_CLICKED(IDC_PD_ANALYZEMARKS_REPORT, &CProdDataPrintView::OnBnClickedPdAnalyzemarksReport)
	ON_UPDATE_COMMAND_UI(IDC_PD_ANALYZEMARKS_REPORT, &CProdDataPrintView::OnUpdateAnalyzemarksReport)
END_MESSAGE_MAP()


// CProdDataPrintView-Diagnose

#ifdef _DEBUG
void CProdDataPrintView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CProdDataPrintView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CProdDataPrintView::DrawItemPrintLayout(LPDRAWITEMSTRUCT lpDrawItemStruct, int /* nState*/)
{
	CPrintSheet*  pPrintSheet  = (CPrintSheet*)lpDrawItemStruct->itemData;
	CLayout*	  pLayout	   = pPrintSheet->GetLayout();
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem = lpDrawItemStruct->rcItem;
	if (pLayout)
		pLayout->Draw(pDC, rcItem, pPrintSheet, pPressDevice, TRUE, -1, FALSE, NULL, TRUE);
}

// CProdDataPrintView-Meldungshandler

void CProdDataPrintView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	m_markUpButton.SubclassDlgItem	(IDC_MARK_UP, this);
	m_markUpButton.SetBitmap		(IDB_ARROW_UP, IDB_ARROW_UP, IDB_ARROW_UP_DISABLED, XCENTER);
	m_markDownButton.SubclassDlgItem(IDC_MARK_DOWN, this);
	m_markDownButton.SetBitmap		(IDB_ARROW_DOWN, IDB_ARROW_DOWN, IDB_ARROW_DOWN_DISABLED, XCENTER);

// Set up tooltips       
	m_pToolTip = new CToolTipCtrl;
    m_pToolTip->Create(this);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_UP)),	IDS_MARK_UP_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_DOWN)),	IDS_MARK_DOWN_TIP);
	m_pToolTip->Activate(TRUE);

// Set up image lists
	CBitmap bitmap;
	m_pMarkImageList->Create(16, 16, ILC_COLOR8, 28, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_CUSTOM_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TEXT_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_SECTION_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CROP_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_FOLD_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_BARCODE_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_DRAWING_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CUSTOM_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TEXT_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_SECTION_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CROP_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_FOLD_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_BARCODE_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_DRAWING_MARK_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CUSTOM_MARK_AUTO);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TEXT_MARK_AUTO);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_SECTION_MARK_AUTO);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CROP_MARK_AUTO);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_FOLD_MARK_AUTO);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_BARCODE_MARK_AUTO);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_DRAWING_MARK_AUTO);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CUSTOM_MARK_AUTO_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TEXT_MARK_AUTO_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_SECTION_MARK_AUTO_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CROP_MARK_AUTO_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_FOLD_MARK_AUTO_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_BARCODE_MARK_AUTO_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_DRAWING_MARK_AUTO_DISABLED);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	m_objectListCtrl.SetImageList(m_pMarkImageList, LVSIL_SMALL);

	UpdateDialogControls( this, TRUE );

	CRect rcClient; GetClientRect(rcClient);
	SetScaleToFitSize(rcClient.Size());	//suppress scrollbars
}

void CProdDataPrintView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	InitObjectList();
	LoadAutoMarksState();
}

void CProdDataPrintView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);
}

CPrintSheetNavigationView* CProdDataPrintView::GetPrintSheetNavigationView()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetNavigationView();

	return NULL;
}

CPrintSheet* CProdDataPrintView::GetActPrintSheet()
{
	CPrintSheetNavigationView* pView = GetPrintSheetNavigationView();
	return (pView) ? pView->GetActPrintSheet() : NULL;
}

CLayout* CProdDataPrintView::GetActLayout()
{
	CPrintSheetNavigationView* pView = GetPrintSheetNavigationView();
	return (pView) ? pView->GetActLayout() : NULL;
}

CPressDevice* CProdDataPrintView::GetActPressDevice()
{
	CPrintSheetNavigationView* pView = GetPrintSheetNavigationView();
	return (pView) ? pView->GetActPressDevice() : NULL;
}

void CProdDataPrintView::OnMouseMove(UINT nFlags, CPoint point)
{
	CFormView::OnMouseMove(nFlags, point);
}

HBRUSH CProdDataPrintView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

    if(nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_MARKVIEW_STATIC:
					pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
					pDC->SelectObject(&m_boldFont);
					break;

		default:
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				//hbr = (HBRUSH)m_bkBrush.GetSafeHandle();
		//		break;
		}
	}
	return hbr;
}

CMark* CProdDataPrintView::GetCurrentMark()
{
	if (m_objectListCtrl.GetSelectedCount() > 1)
		return NULL;

	POSITION pos = m_objectListCtrl.GetFirstSelectedItemPosition();
	int nSel = m_objectListCtrl.GetNextSelectedItem(pos);
	return ( (nSel >= 0) && (nSel < m_objectList.GetSize()) ? m_objectList[nSel] : NULL);
}

CLayoutObject* CProdDataPrintView::GetLayoutObject()
{
	if (GetCurrentMark())
	{
		CPrintSheet* pPrintSheet = GetActPrintSheet();
		CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		if (pLayout)
			return pLayout->FindMarkObject(GetCurrentMark()->m_strMarkName);
	}
	return NULL;
}

void CProdDataPrintView::InitObjectList(CMark* pMark)
{
	if ( ! m_objectListCtrl.m_hWnd)
		return;

	CPrintSheet* pPrintSheet = GetActPrintSheet();
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pPrintSheet || ! pLayout)
		return;

	int nCurSel = -1;
	if ( ! pMark)
	{
		POSITION pos = m_objectListCtrl.GetFirstSelectedItemPosition();
		nCurSel		 = (pos) ? m_objectListCtrl.GetNextSelectedItem(pos) : -1;
	} 

	m_objectListCtrl.DeleteAllItems();
	m_objectList.RemoveAll(FALSE);
	for (int i = 0; i < pLayout->m_markList.GetCount(); i++)
		m_objectList.Add(pLayout->m_markList[i]);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	int nActSide = (pPrintSheetView) ? pPrintSheetView->GetActSight() : BOTHSIDES;

	for (int i = 0; i < m_objectList.GetSize(); i ++)                        
	{
		LV_ITEM item;
		item.mask		= LVIF_TEXT | LVIF_IMAGE;
		item.cchTextMax = MAX_TEXT;
		item.iItem		= i; 
		item.iSubItem	= 0;
		item.pszText	= m_objectList[i]->m_strMarkName.GetBuffer(MAX_TEXT);
		CMark* pCurMark	= pLayout->m_markList[i];

		BOOL bIsActive	= (pCurMark->m_nStatus == CMark::Active) ? TRUE : FALSE;
		if ( ! pCurMark->IsAutoMark())
		{
			switch (m_objectList[i]->m_nType)
			{
			case CMark::CustomMark:		item.iImage	= (bIsActive) ? 0 :  7; break;
			case CMark::TextMark:		item.iImage	= (bIsActive) ? 1 :  8; break;
			case CMark::SectionMark:	item.iImage	= (bIsActive) ? 2 :  9; break;
			case CMark::CropMark:		item.iImage	= (bIsActive) ? 3 : 10; break;
			case CMark::FoldMark:		item.iImage	= (bIsActive) ? 4 : 11; break;
			case CMark::BarcodeMark:	item.iImage	= (bIsActive) ? 5 : 12; break;
			case CMark::DrawingMark:	item.iImage	= (bIsActive) ? 6 : 13; break;
			}
		}
		else
		{
			switch (m_objectList[i]->m_nType)
			{
			case CMark::CustomMark:		item.iImage	= (bIsActive) ? 14 : 21; break;
			case CMark::TextMark:		item.iImage	= (bIsActive) ? 15 : 22; break;
			case CMark::SectionMark:	item.iImage	= (bIsActive) ? 16 : 23; break;
			case CMark::CropMark:		item.iImage	= (bIsActive) ? 17 : 24; break;
			case CMark::FoldMark:		item.iImage	= (bIsActive) ? 18 : 25; break;
			case CMark::BarcodeMark:	item.iImage	= (bIsActive) ? 19 : 26; break;
			case CMark::DrawingMark:	item.iImage	= (bIsActive) ? 20 : 27; break;
			}
		}
		m_objectListCtrl.InsertItem(&item);

		if (pMark)
			if (pMark->m_strMarkName == m_objectList[i]->m_strMarkName)
				nCurSel = i;
	}

	if ( ! m_objectList.GetSize())
		nCurSel = -1;

	if ( (nCurSel >= 0) && (nCurSel < m_objectList.GetSize()) )
	{
		m_objectListCtrl.SetItemState(nCurSel, LVIS_SELECTED, LVIS_SELECTED);
		UpdateLayoutMarkObjects();
	}

	m_pCurrentLayout = pLayout;
}

BOOL CProdDataPrintView::IsSelected(CLayoutObject* pObject)
{
	if ( ! this)
		return FALSE;
	if ( ! pObject)
		return FALSE;
	if (pObject->m_nType != CLayoutObject::ControlMark)
		return FALSE;

	for (int i = 0; i < m_objectList.GetSize(); i++)
	{
		if (m_objectListCtrl.GetItemState(i, LVIS_SELECTED))
		{
			if (m_objectList[i]->m_strMarkName == pObject->m_strFormatName)
				return TRUE;
		}
	}

	return FALSE;
}

BOOL g_bChanged = FALSE;

void CProdDataPrintView::OnLvnKeydownPdMarkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;

	switch (pLVKeyDow->wVKey)
	{
	case VK_UP:
	case VK_DOWN:
	case VK_PRIOR:
	case VK_NEXT:
	case VK_HOME:
	case VK_END:		g_bChanged = TRUE; 
						break;
	}
}

void CProdDataPrintView::OnItemchangedObjectList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	
	*pResult = 0;

	if (pNMListView->uNewState & LVNI_SELECTED)
		if (g_bChanged)		// only call handler when key has been pressed - clicks will be handled directly
		{
			OnClickObjectList(pNMHDR, pResult);
			g_bChanged = FALSE;
		}
}

void CProdDataPrintView::OnClickObjectList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	*pResult = 0;

	if (m_objectListCtrl.GetSelectedCount() > 1)
		if (GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_hWnd)
			GetDlgLayoutObjectProperties()->DestroyWindow();

	if (GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_hWnd)
		GetDlgLayoutObjectProperties()->DataExchangeSave();

	UpdateLayoutMarkObjects();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CRect clientRect;
	pView->GetClientRect(clientRect);
	BOOL	bScrolled = FALSE;
	CMark*	pMark	  = GetCurrentMark();
    if ( pMark && ((pView->GetTotalSize().cx > clientRect.Width()) || (pView->GetTotalSize().cy > clientRect.Height())) )
	{
		if ( (pMark->m_nType != CMark::CropMark) && (pMark->m_nType != CMark::FoldMark) )
		{
			CLayout*	   pLayout		 = GetActLayout();
			CLayoutObject* pLayoutObject = (pLayout) ? pLayout->m_FrontSide.FindFirstLayoutObject(pMark->m_strMarkName, CLayoutObject::ControlMark) : NULL;
			if (pLayoutObject)
			{
				CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData((void*)pLayoutObject, GetActPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
				if (pDI)
					if (pDI->m_bIsVisible != CDisplayItem::isCompleteVisible) 
					{
						pView->Invalidate();
						CPoint scrollPos = pView->GetScrollPosition();
						int nOffX = (clientRect.Width()  - pDI->m_BoundingRect.Width()) /2;
						int nOffY = (clientRect.Height() - pDI->m_BoundingRect.Height())/2;
						pView->ScrollToPosition(CPoint(scrollPos.x + pDI->m_BoundingRect.left - nOffX, scrollPos.y + pDI->m_BoundingRect.top - nOffY));
						bScrolled = TRUE;
					}
			}
		}
	}

	if (pMark)
		if (GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_hWnd)
			GetDlgLayoutObjectProperties()->ReinitDialog(pMark, GetActPrintSheet());

	UpdateDialogControls( this, TRUE );

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CProdDataPrintView::UpdateLayoutMarkObjects(BOOL bUpdateState)
{
   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	while (pDI)
	{
		CLayoutObject* pLayoutObject = ((CLayoutObject*)pDI->m_pData);
		if (pLayoutObject)
			if (pLayoutObject->m_nType == CLayoutObject::ControlMark)
			{
				CRect rcInvalid		 = pDI->m_BoundingRect; 
				BOOL  bIsSelected	 = IsSelected(pLayoutObject);
				BOOL  bStatusChanged = FALSE;
				if (bIsSelected)
				{
					if (pDI->m_nState != CDisplayItem::Selected)
						bStatusChanged = TRUE;
				}
				else
					if ( ! bIsSelected)
						if (pDI->m_nState == CDisplayItem::Selected)
							bStatusChanged = TRUE;

				if (bStatusChanged)
					pView->m_DisplayList.m_pParent->InvalidateRect(rcInvalid);

				if (bUpdateState)
					pDI->m_nState = (bIsSelected) ? CDisplayItem::Selected : CDisplayItem::Normal;
			}

		pDI = pView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}

	pDI = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, MarkObjectInfo));
	while (pDI)
	{
		CRect		   rcInvalid	  = pDI->m_AuxRect;
		BOOL		   bStatusChanged = FALSE;
		CLayoutObject* pLayoutObject  = ((BOOL)pDI->m_pAuxData) ? (CLayoutObject*)pDI->m_pData : NULL;
		if (pLayoutObject)
		{
			BOOL  bIsSelected	 = IsSelected(pLayoutObject);
			if (bIsSelected)
			{
				if (pDI->m_nState != CDisplayItem::Selected)
					bStatusChanged = TRUE;
			}
			else
				if ( ! bIsSelected)
					if (pDI->m_nState == CDisplayItem::Selected)
						bStatusChanged = TRUE;
		}
		else
			bStatusChanged = TRUE;

		if (bStatusChanged)
			pView->m_DisplayList.m_pParent->InvalidateRect(rcInvalid);

		pDI = pView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, MarkObjectInfo));
	}
}

void CProdDataPrintView::ShowPrintSheetViewSelections()
{
	for (int i = 0; i < m_objectListCtrl.GetItemCount(); i++)
		m_objectListCtrl.SetItemState(i, ~LVIS_SELECTED, LVIS_SELECTED);

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	while (pDI)
	{
		CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
		if (pObject->m_nType == CLayoutObject::ControlMark)
		{
			CMark* pMark = m_objectList.FindMark(pObject->m_strFormatName);
			if (pMark)
			{
				int nIndex = m_objectList.GetIndex(pMark);
				if ( (nIndex >= 0) && (nIndex < m_objectListCtrl.GetItemCount()) )
					m_objectListCtrl.SetItemState(nIndex, LVIS_SELECTED, LVIS_SELECTED);
			}
		}
		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}
	pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark));
	while (pDI)
	{
		CMark* pMark = (CMark*)pDI->m_pData;
		if (pMark)
		{
			int nIndex = m_objectList.GetIndex(pMark);
			if ( (nIndex >= 0) && (nIndex < m_objectListCtrl.GetItemCount()) )
				m_objectListCtrl.SetItemState(nIndex, LVIS_SELECTED, LVIS_SELECTED);
		}
		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}

	POSITION pos = m_objectListCtrl.GetFirstSelectedItemPosition();
	if (pos)
	{
		int nSel = m_objectListCtrl.GetNextSelectedItem(pos);
		m_objectListCtrl.EnsureVisible(nSel, FALSE);
	}

	UpdateLayoutMarkObjects(FALSE);	// FALSE -> leave display item states unchanged (as in selected printsheetview)

	UpdateDialogControls( this, TRUE );

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CProdDataPrintView::OnNMDblclkPdMarkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	OnBnClickedPdModifyMark();
	*pResult = 0;
}

void CProdDataPrintView::OnBnClickedPdModifyMark()
{
	CMark* pMark = GetCurrentMark();
	if ( ! pMark)
		return;

	if ( ! GetDlgLayoutObjectProperties()->m_hWnd)
		GetDlgLayoutObjectProperties()->Create(GetCurrentMark(), GetActPrintSheet(), this, TRUE, TRUE);
	else
		GetDlgLayoutObjectProperties()->ReinitDialog(GetCurrentMark(), GetActPrintSheet());
}

void CProdDataPrintView::OnBnClickedPdNewMark()
{
    CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_NEW_MARK));
	m_pNewMarkPopupMenu = menu.GetSubMenu(0);
	if ( ! m_pNewMarkPopupMenu)
		return;

    MENUITEMINFO mi;
    mi.cbSize = sizeof(MENUITEMINFO);
    mi.fMask = MIIM_TYPE;
    mi.fType = MFT_OWNERDRAW;
//     Making the menu item ownerdrawn:
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_PDF,	   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_TEXT,	   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_SECTION,   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_CROP,	   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_FOLD,	   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_BARCODE,   &mi);
	m_pNewMarkPopupMenu->SetMenuItemInfo(ID_NEWMARK_RECTANGLE, &mi);

	CRect rcClient;
	GetDlgItem(IDC_PD_NEW_MARK)->GetWindowRect(rcClient);

	m_pNewMarkPopupMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rcClient.TopLeft().x, rcClient.TopLeft().y, this);
	m_pNewMarkPopupMenu = NULL;
}

void CProdDataPrintView::AddNewMark(int nMarkType) 
{
	if (GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_hWnd)
		GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.DataExchangeSave();

	CMark*					pMark = NULL;
	CReflinePositionParams* pReflinePosParams = NULL;
	switch (nMarkType)
	{
	case ID_NEWMARK_PDF:
	case ID_NEWMARK_TEXT:
		{
			if (nMarkType == ID_NEWMARK_PDF)
			{
				pMark = (CCustomMark*) new CCustomMark;
				((CCustomMark*)pMark)->Create(theApp.m_colorDefTable);
				pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
				pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
			}
			else
			{
				pMark = (CTextMark*) new CTextMark;
				((CTextMark*)pMark)->Create(theApp.m_colorDefTable);
				pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
				pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
			}
		}
		break;

	case ID_NEWMARK_SECTION:
		pMark = (CSectionMark*) new CSectionMark;
		((CSectionMark*)pMark)->Create(theApp.m_colorDefTable);
		pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
		pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
		break;

	case ID_NEWMARK_CROP:
		pMark = (CCropMark*) new CCropMark;
		((CCropMark*)pMark)->Create(theApp.m_colorDefTable);
		pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
		pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
		break;

	case ID_NEWMARK_FOLD:
		pMark = (CFoldMark*) new CFoldMark;
		((CFoldMark*)pMark)->Create(theApp.m_colorDefTable);
		pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
		pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
		break;

	case ID_NEWMARK_BARCODE:
		pMark = (CBarcodeMark*) new CBarcodeMark;
		((CBarcodeMark*)pMark)->Create(theApp.m_colorDefTable);
		pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
		pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
		break;

	case ID_NEWMARK_RECTANGLE:
		pMark = (CDrawingMark*) new CDrawingMark;
		((CDrawingMark*)pMark)->Create(theApp.m_colorDefTable);
		pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
		pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
		break;
	}

	if ( ! pMark)
		return;

	CFrameWnd* pFrame = GetParentFrame();
	if (pFrame)
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			((CNewPrintSheetFrame*)pFrame)->OnPlatecheck();

	if ( ! GetDlgLayoutObjectProperties()->m_hWnd)
		GetDlgLayoutObjectProperties()->Create(pMark, GetActPrintSheet(), this);
	else
		GetDlgLayoutObjectProperties()->ReinitDialog(pMark, GetActPrintSheet());

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));

	if (nMarkType == ID_NEWMARK_PDF)
		GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgCustomMarkContent.OnRegisterPDF();
}

void CProdDataPrintView::OnBnClickedPdMarkDelete() 
{
	if (m_objectListCtrl.GetSelectedCount() <= 0)
		return;
	CLayout* pLayout	= GetActLayout();
	if ( ! pLayout)
		return;

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	CDlgRemoveControlMarks dlg;
	int		 nFirstSel    = -1;
	int		 nNumSelected = 0;
	POSITION pos		  = m_objectListCtrl.GetFirstSelectedItemPosition();
	while (pos)
	{
		if (nNumSelected > 0)
			dlg.m_strSelectedMarkName += _T(";");
		int nSel = m_objectListCtrl.GetNextSelectedItem(pos);
		dlg.m_strSelectedMarkName += m_objectList[nSel]->m_strMarkName;
		nNumSelected++;
	}
	if (dlg.DoModal() == IDCANCEL)
		return;

	int	nNumDeleted = 0;
	pos				= m_objectListCtrl.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nSel = m_objectListCtrl.GetNextSelectedItem(pos);
		nSel -= nNumDeleted;
		pLayout->m_FrontSide.RemoveMarkObject(pLayout->m_markList[nSel]->m_strMarkName);
		pLayout->m_BackSide.RemoveMarkObject(pLayout->m_markList[nSel]->m_strMarkName);
		pLayout->ResetMarkInstanceDefs(pLayout->m_markList[nSel]->m_strMarkName);
		pLayout->m_markList.RemoveAt(nSel);
		nNumDeleted++;
	}

	pLayout->AnalyzeMarks();

	if (GetDlgLayoutObjectProperties()->m_hWnd)
	{
		GetDlgLayoutObjectProperties()->m_pLayoutObject = NULL;
		GetDlgLayoutObjectProperties()->m_pTemplate	  = NULL;
		GetDlgLayoutObjectProperties()->ReinitDialog(GetCurrentMark(), GetActPrintSheet());
	}

	if ( ! m_objectList.GetSize())
	{
		GetDlgItem(IDC_PD_DUPLICATE_MARK )->EnableWindow(FALSE);
		GetDlgItem(IDC_PD_DELETE_MARK    )->EnableWindow(FALSE);
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		pDoc->SetModifiedFlag();										

	UpdateSheetPreview();

	UpdateLayoutMarkObjects();
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CProdDataPrintView::OnBnClickedPdMarkDuplicate() 
{
	CMark*  pCurrentMark = GetCurrentMark();
	CLayout* pLayout	 = GetActLayout();
	if ( ! pLayout || ! pCurrentMark)
		return;

	CMark* pNewCurMark = NULL;
	BOOL   bOffsetMark = TRUE;
	float fOffset = min(pCurrentMark->m_fMarkWidth, pCurrentMark->m_fMarkHeight) * 1.5f;
	switch (pCurrentMark->m_nType)
	{
	case CMark::CustomMark:  
		{
			CCustomMark* pMark	 = new CCustomMark;
			*pMark			     = *(CCustomMark*)pCurrentMark;
			pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
			pMark->m_markTemplate.m_strPageID	   = pMark->m_strMarkName;
			pMark->m_MarkSource.m_strDocumentTitle = pMark->m_strMarkName;
			if (bOffsetMark)
			{
				pMark->m_reflinePosParamsAnchor.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsAnchor.m_fDistanceY += fOffset;
				pMark->m_reflinePosParamsLL.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsLL.m_fDistanceY += fOffset;
				pMark->m_reflinePosParamsUR.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsUR.m_fDistanceY += fOffset;
			}
			pLayout->m_markList.Add(pMark);	
			pNewCurMark = pMark;
		}
		break;
	case CMark::TextMark:	 
		{
			CTextMark* pMark	 = new CTextMark;
			*pMark				 = *(CTextMark*)pCurrentMark;
			pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
			pMark->m_markTemplate.m_strPageID	   = pMark->m_strMarkName;
			pMark->m_MarkSource.m_strDocumentTitle = pMark->m_strMarkName;
			if (bOffsetMark)
			{
				pMark->m_reflinePosParamsAnchor.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsAnchor.m_fDistanceY += fOffset;
				pMark->m_reflinePosParamsLL.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsLL.m_fDistanceY += fOffset;
				pMark->m_reflinePosParamsUR.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsUR.m_fDistanceY += fOffset;
			}
			pLayout->m_markList.Add(pMark);	
			pNewCurMark = pMark;
		}
		break;
	case CMark::BarcodeMark:	 
		{
			CBarcodeMark* pMark	 = new CBarcodeMark;
			*pMark				 = *(CBarcodeMark*)pCurrentMark;
			pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
			pMark->m_markTemplate.m_strPageID	   = pMark->m_strMarkName;
			pMark->m_MarkSource.m_strDocumentTitle = pMark->m_strMarkName;
			if (bOffsetMark)
			{
				pMark->m_reflinePosParamsAnchor.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsAnchor.m_fDistanceY += fOffset;
				pMark->m_reflinePosParamsLL.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsLL.m_fDistanceY += fOffset;
				pMark->m_reflinePosParamsUR.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsUR.m_fDistanceY += fOffset;
			}
			pLayout->m_markList.Add(pMark);	
			pNewCurMark = pMark;
		}
		break;
	case CMark::DrawingMark:	 
		{
			CDrawingMark* pMark	 = new CDrawingMark;
			*pMark				 = *(CDrawingMark*)pCurrentMark;
			pMark->m_strMarkName = m_objectList.FindFreeMarkName(pMark->m_strMarkName);
			pMark->m_markTemplate.m_strPageID	   = pMark->m_strMarkName;
			pMark->m_MarkSource.m_strDocumentTitle = pMark->m_strMarkName;
			if (bOffsetMark)
			{
				pMark->m_reflinePosParamsAnchor.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsAnchor.m_fDistanceY += fOffset;
				pMark->m_reflinePosParamsLL.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsLL.m_fDistanceY += fOffset;
				pMark->m_reflinePosParamsUR.m_fDistanceX += fOffset;
				pMark->m_reflinePosParamsUR.m_fDistanceY += fOffset;
			}
			pLayout->m_markList.Add(pMark);	
			pNewCurMark = pMark;
		}
		break;
	case CMark::SectionMark:	// Duplicate section mark not allowed (makes no sense). Duplicate button is disabled
	case CMark::CropMark:		// Duplicate crop mark not allowed (makes no sense). Duplicate button is disabled
	case CMark::FoldMark:		// Duplicate fold mark not allowed (makes no sense). Duplicate button is disabled
								break;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->SetModifiedFlag();	// DoAutoMarks is needed, because otherwise new mark will be added at the end of the auto marks list
		pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();	
		//pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		//pDoc->m_PrintSheetList.ReorganizeColorInfos();
	}
	InitObjectList(pNewCurMark);

	UpdateSheetPreview();

	UpdateLayoutMarkObjects();
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CProdDataPrintView::OnBnClickedPdOpenMarklib() 
{
	CFrameWnd* pFrame = GetParentFrame();
	if (pFrame)
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		{
			if ( ! ((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->m_hWnd)
			{
				((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->m_strCurrentFolder = _T("");
				((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->Create(IDD_MARKSET, this);
			}
			//else
			//{
			//	((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->OnClose();
			//	if (((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->m_hWnd)	// close rejected
			//		((CButton*)GetDlgItem(IDC_PD_OPEN_MARKLIB))->SetCheck(1);
			//}
		}
}

void CProdDataPrintView::OnBnClickedPdDeleteAllMarks() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int	nSide	 = pDoc->GetActSight();
	int	nRemoved = 0;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));

	switch (nSide)
	{
	case ALLSIDES:	
		{
			if (AfxMessageBox(IDS_DELETE_MARKS_ALL, MB_YESNO) == IDNO)
				return;

			if (GetDlgLayoutObjectProperties())
				GetDlgLayoutObjectProperties()->DestroyWindow();

			if (pView)
				pView->UndoSave();
			POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
			while (pos)
			{
				CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
				nRemoved += rLayout.RemoveMarkObjects(-1, TRUE);
				rLayout.ResetMarkInstanceDefs(_T(""));
				rLayout.m_markList.RemoveAll();
				rLayout.AnalyzeMarks();
			}
		}
		break;

	case FRONTSIDE:
	case BACKSIDE:
	case BOTHSIDES:
		{
			if (AfxMessageBox(IDS_DELETE_MARKS_LAYOUT, MB_YESNO) == IDNO)
				return;

			if (GetDlgLayoutObjectProperties())
				GetDlgLayoutObjectProperties()->DestroyWindow();

			if (pView)
				pView->UndoSave();
			CLayout* pLayout = pDoc->GetActLayout();
			if (pLayout)
			{
				nRemoved = pLayout->RemoveMarkObjects(-1, TRUE);
				pLayout->ResetMarkInstanceDefs(_T(""));
				pLayout->m_markList.RemoveAll();
				pLayout->AnalyzeMarks();
			}
		}
		break;
	}

	InitObjectList();
	pDoc->SetModifiedFlag();										
	UpdateSheetPreview();

	UpdateLayoutMarkObjects();
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CProdDataPrintView::OnMarkSetStatus() 
{
	UpdateData(TRUE);

	if (m_objectListCtrl.GetSelectedCount() <= 0)
		return;

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	POSITION pos = m_objectListCtrl.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nSel = m_objectListCtrl.GetNextSelectedItem(pos);
		m_objectList[nSel]->m_nStatus = (m_bMarkStatus) ? CMark::Active : CMark::Inactive;
	}

	InitObjectList();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		pDoc->SetModifiedFlag();										

	CLayout* pLayout = pDoc->GetActLayout();
	if (pLayout)
		pLayout->AnalyzeMarks(FALSE);

	UpdateSheetPreview();

	UpdateLayoutMarkObjects();
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CProdDataPrintView::OnMarkUp() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = m_objectListCtrl.GetFirstSelectedItemPosition();
	int nCurSel	 = (pos) ? m_objectListCtrl.GetNextSelectedItem(pos) : -1;
	if ((nCurSel <= 0) || (nCurSel >= m_objectList.GetSize()))
		return;

	CLayout* pLayout = pDoc->GetActLayout();
	if ( ! pLayout)
		return;

	pLayout->m_markList[nCurSel - 1]->m_nZOrderIndex++;
	pLayout->m_markList[nCurSel]->m_nZOrderIndex--;

	pLayout->m_markList.Sort();
	int nZOrderIndex = 0;
	for (int i = 0; i < pLayout->m_markList.GetSize(); i++)                        
		pLayout->m_markList[i]->m_nZOrderIndex = nZOrderIndex++;

	InitObjectList(pLayout->m_markList[nCurSel - 1]);
	pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();	// in order to ClipToObjects() for Crop and FoldMarks regarding to CustomMarks
	pDoc->SetModifiedFlag();										
	UpdateSheetPreview();

	UpdateLayoutMarkObjects();
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CProdDataPrintView::OnMarkDown() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = m_objectListCtrl.GetFirstSelectedItemPosition();
	int nCurSel	 = (pos) ? m_objectListCtrl.GetNextSelectedItem(pos) : -1;
	if ((nCurSel < 0) || (nCurSel >= m_objectList.GetSize() - 1))
		return;

	CLayout* pLayout = pDoc->GetActLayout();
	if ( ! pLayout)
		return;

	pLayout->m_markList[nCurSel]->m_nZOrderIndex++;
	pLayout->m_markList[nCurSel + 1]->m_nZOrderIndex--;

	pLayout->m_markList.Sort();
	int nZOrderIndex = 0;
	for (int i = 0; i < pLayout->m_markList.GetSize(); i++)                        
		pLayout->m_markList[i]->m_nZOrderIndex = nZOrderIndex++;

	InitObjectList(pLayout->m_markList[nCurSel + 1]);
	pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();	// in order to ClipToObjects() for Crop and FoldMarks regarding to CustomMarks
	pDoc->SetModifiedFlag();										
	UpdateSheetPreview();

	UpdateLayoutMarkObjects();
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CProdDataPrintView::OnUpdateMarkSetStatus(CCmdUI* pCmdUI)
{
	POSITION pos	  = m_objectListCtrl.GetFirstSelectedItemPosition();
	int		 nCurSel  = (pos) ? m_objectListCtrl.GetNextSelectedItem(pos) : -1;
	BOOL	 bEnabled = ((nCurSel < 0) || (nCurSel >= m_objectList.GetSize())) ? FALSE : TRUE;

	pCmdUI->Enable(bEnabled);

	if ( (nCurSel >= 0) && (nCurSel < m_objectList.GetSize()) )
	{
		CMark* pMark = m_objectList[nCurSel];
		pCmdUI->SetCheck( (pMark->m_nStatus == CMark::Active) ? 1 : 0);
	}
	else
		pCmdUI->SetCheck(0);
}

void CProdDataPrintView::OnUpdateMarkUp(CCmdUI* pCmdUI)
{
	POSITION pos	  = m_objectListCtrl.GetFirstSelectedItemPosition();
	int		 nCurSel  = (pos) ? m_objectListCtrl.GetNextSelectedItem(pos) : -1;
	BOOL	 bEnabled = ((nCurSel <= 0) || (nCurSel >= m_objectList.GetSize())) ? FALSE : TRUE;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc && bEnabled)
	{
		CLayout* pLayout = pDoc->GetActLayout();
		if (pLayout)
			if (pLayout->m_markList[nCurSel]->IsAutoMark())	
				bEnabled = FALSE;
	}
	pCmdUI->Enable(bEnabled);
}

void CProdDataPrintView::OnUpdateMarkDown(CCmdUI* pCmdUI)
{
	POSITION pos	  = m_objectListCtrl.GetFirstSelectedItemPosition();
	int		 nCurSel  = (pos) ? m_objectListCtrl.GetNextSelectedItem(pos) : -1;
	BOOL	 bEnabled = ((nCurSel < 0) || (nCurSel >= m_objectList.GetSize() - 1)) ? FALSE : TRUE;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc && bEnabled)
	{
		CLayout* pLayout = pDoc->GetActLayout();
		if (pLayout)
			if (pLayout->m_markList[nCurSel]->IsAutoMark())
				bEnabled = FALSE;
			else
			{
				nCurSel++;
				if (nCurSel < pLayout->m_markList.GetSize())
					if (pLayout->m_markList[nCurSel]->IsAutoMark())	// next item is auto mark
						bEnabled = FALSE;
			}
	}

	pCmdUI->Enable(bEnabled);
}

void CProdDataPrintView::OnBnClickedPdOpenAutoMarklib() 
{
	CFrameWnd* pFrame = GetParentFrame();
	if (pFrame)
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		{
			if ( ! ((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->m_hWnd)
			{
				((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->m_bOpenAutoMarks	  = TRUE;
				((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->m_strCurrentFolder = m_strAutoMarksFolder;
				((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->m_strMarkName	  = _T("");
				((CNewPrintSheetFrame*)pFrame)->m_pDlgMarkSet->Create(IDD_MARKSET, this);
			}
		}
}
CString CProdDataPrintView::FindFreeMarkName(CString strNewMark)
{
	CString strFinal = strNewMark;
	int nCount = 1;
	int i	   = 0;
	while (i < m_objectList.GetSize())
	{
		if (strFinal == m_objectList[i]->m_strMarkName)	// mark name exists
		{
			strFinal.Format(_T("%s (%d)"), strNewMark, nCount++);
			i = 0;
		}
		else
			i++;
	}

	return strFinal;
}

void CProdDataPrintView::UpdateSheetPreview(BOOL bUpdateMarkOnly)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CRect clientRect;
	pView->GetClientRect(&clientRect);
    BOOL bZoom = ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) ) ? FALSE : TRUE;

	CSize  oldDocSize = pView->m_docSize;
	CPoint oldDocOrg  = pView->m_docOrg;
	if ( ! bZoom)
	{
		pView->CalcDocExtents();
		pView->CalcViewportExtents();
	}

	if (bUpdateMarkOnly && (oldDocSize == pView->m_docSize) && (oldDocOrg == pView->m_docOrg) )	// updatemarkonly only if docsize hasn't changed
	{
		pView->LockUpdateStatusBars();
		if (pView->m_bReflineUpdateEraseFront)
			pView->RedrawWindow(pView->m_reflineUpdateRectFront);
		else
			pView->RedrawWindow(pView->m_reflineUpdateRectFront, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
		if (pView->m_bReflineUpdateEraseBack)
			pView->RedrawWindow(pView->m_reflineUpdateRectBack);
		else
			pView->RedrawWindow(pView->m_reflineUpdateRectBack,  NULL, RDW_INVALIDATE | RDW_UPDATENOW);
		pView->UnlockUpdateStatusBars();
	}
	else
	{
		if ( ! bZoom)
		{
			pView->CalcScrollSizes();	
			pView->SetScrollSizes(MM_TEXT, pView->m_sizeTotal, pView->m_sizePage, pView->m_sizeLine);
		}
		pView->m_DisplayList.Invalidate();
		pView->OnUpdate(NULL, 0, NULL);
		pView->Invalidate();
		pView->UpdateWindow();
	}
}

void CProdDataPrintView::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType == ODT_MENU)
	{
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		CRect rcItem = lpDrawItemStruct->rcItem;

		pDC->FillSolidRect(rcItem, RGB(252, 252, 249));//CREME);

		CRect rcIcon = rcItem;
		rcIcon.right = rcIcon.left + 25;

		if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			CGraphicComponent gc;
			gc.DrawTransparentFrame(pDC, rcItem, RGB(120,160,220), 2);
		}				

		switch (lpDrawItemStruct->itemID)
		{
		case ID_NEWMARK_PDF:		pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_CUSTOM_MARK_UNASSIGNED));	break;
		case ID_NEWMARK_TEXT:		pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_TEXT_MARK_UNASSIGNED));		break;
		case ID_NEWMARK_SECTION:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_SECTION_MARK_UNASSIGNED));	break;
		case ID_NEWMARK_CROP:		pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_CROP_MARK_UNASSIGNED));		break;
		case ID_NEWMARK_FOLD:		pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_FOLD_MARK_UNASSIGNED));		break;
		case ID_NEWMARK_BARCODE:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_BARCODE_MARK_UNASSIGNED));	break;
		case ID_NEWMARK_RECTANGLE:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_DRAWING_MARK_UNASSIGNED));	break;
		}

		pDC->SelectObject(GetFont());
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(DARKBLUE);
		CRect rcText = rcItem;
		rcText.left = rcIcon.right + 5;
		if (m_pNewMarkPopupMenu)
		{
			CString strText;
			m_pNewMarkPopupMenu->GetMenuString(lpDrawItemStruct->itemID, strText, MF_BYCOMMAND);
			pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		}
	}
	else
		CFormView::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CProdDataPrintView::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct->CtlType == ODT_MENU)
	{
		int	nMaxLen = 0;
		if (m_pNewMarkPopupMenu)
		{
			CString strText;
			m_pNewMarkPopupMenu->GetMenuString(lpMeasureItemStruct->itemID, strText, MF_BYCOMMAND);
			CClientDC dc(this);
			dc.SelectObject(GetFont());
			nMaxLen = dc.GetTextExtent(strText).cx + 40;
		}
		lpMeasureItemStruct->itemWidth  = nMaxLen;
		lpMeasureItemStruct->itemHeight = 22;
	}
	else
		CFormView::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CProdDataPrintView::OnNewMarkPDF()			{	AddNewMark(ID_NEWMARK_PDF);		}
void CProdDataPrintView::OnNewMarkText()		{	AddNewMark(ID_NEWMARK_TEXT);	}
void CProdDataPrintView::OnNewMarkSection()		{	AddNewMark(ID_NEWMARK_SECTION);	}
void CProdDataPrintView::OnNewMarkCrop()		{	AddNewMark(ID_NEWMARK_CROP);	}
void CProdDataPrintView::OnNewMarkFold()		{	AddNewMark(ID_NEWMARK_FOLD);	}
void CProdDataPrintView::OnNewMarkBarcode()		{	AddNewMark(ID_NEWMARK_BARCODE);	}
void CProdDataPrintView::OnNewMarkRectangle()	{	AddNewMark(ID_NEWMARK_RECTANGLE);	}

CDlgLayoutObjectProperties* CProdDataPrintView::GetDlgLayoutObjectProperties()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;

	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->m_pDlgLayoutObjectProperties;

	return NULL;
}

BOOL CProdDataPrintView::IsOpenMarkEditDlg()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;

	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		if ( ((CNewPrintSheetFrame*)pFrame)->IsOpenDlgLayoutObjectProperties() || ((CNewPrintSheetFrame*)pFrame)->IsOpenMarkSetDlg() )
			return TRUE;

	return FALSE;
}

void CProdDataPrintView::OnNMSetfocusPdMarkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	UpdateDialogControls( this, TRUE );
}

void CProdDataPrintView::OnNMKillfocusPdMarkList(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	UpdateDialogControls( this, TRUE );
}

BOOL CProdDataPrintView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (NULL != m_pToolTip)            
		m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

void CProdDataPrintView::OnBnClickedPdAutomarksActive()
{
	if (m_bAutoMarksActive)
		if (AfxMessageBox(IDS_SWITCHOFF_AUTOMARKS_WARNING, MB_OKCANCEL) == IDCANCEL)
		{
			UpdateData(FALSE);
			return;
		}

	UpdateData(TRUE);

	SaveAutoMarksState();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->SetModifiedFlag();	// trigger automarks process
		UpdateSheetPreview();
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
	}
}

void CProdDataPrintView::LoadAutoMarksState()
{
	HKEY	  hKey;
	DWORD	  dwDisposition; 
	const int nValueLenght = max(_MAX_PATH, MAX_TEXT) + 2;
	TCHAR	  szValue[nValueLenght];
	DWORD	  dwSize;
	CString	  strKey = theApp.m_strRegistryKey;
	RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition); 

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("SetAutoMarks"), 0, NULL, (LPBYTE)szValue, &dwSize) != ERROR_SUCCESS)
		_tcsncpy(szValue, _T("FALSE"), 5);
	if (_tcscmp(szValue, _T("TRUE")) == 0)
		m_bAutoMarksActive = TRUE;
	else
		m_bAutoMarksActive = FALSE;

	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("AutoMarkFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		m_strAutoMarksFolder = szValue;
	else
		m_strAutoMarksFolder = _T("");

	UpdateData(FALSE);
}

void CProdDataPrintView::SaveAutoMarksState()
{
	UpdateData(TRUE);

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		if (m_bAutoMarksActive)
			RegSetValueEx(hKey, _T("SetAutoMarks"), 0, REG_SZ, (LPBYTE)_T("TRUE"), 5 * sizeof(TCHAR));
		else
			RegSetValueEx(hKey, _T("SetAutoMarks"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));

		RegSetValueEx(hKey, _T("AutoMarkFolder"), 0, REG_SZ, (LPBYTE)m_strAutoMarksFolder.GetBuffer(_MAX_PATH), _MAX_PATH * sizeof(TCHAR));
		RegCloseKey(hKey);
	}
}

void CProdDataPrintView::OnUpdateModifyMark(CCmdUI* pCmdUI)
{
	GetDlgItem(IDC_PD_MODIFY_MARK)->EnableWindow((m_objectListCtrl.GetSelectedCount() == 1) ? TRUE : FALSE);
}

void CProdDataPrintView::OnUpdateDuplicateMark(CCmdUI* pCmdUI)
{
	BOOL bEnable = FALSE;
	CMark* pMark = GetCurrentMark();
	if (pMark)
	{
		if ( (pMark->m_nType == CMark::CustomMark) || (pMark->m_nType == CMark::TextMark) || (pMark->m_nType == CMark::BarcodeMark) || (pMark->m_nType == CMark::DrawingMark) )
			if ( ! pMark->IsAutoMark())
				bEnable = TRUE;
	}
	GetDlgItem(IDC_PD_DUPLICATE_MARK)->EnableWindow(bEnable);
}

void CProdDataPrintView::OnUpdateDeleteMark(CCmdUI* pCmdUI)
{
	BOOL bEnable = (m_objectListCtrl.GetSelectedCount() > 0) ? TRUE : FALSE;
	CMark* pMark = GetCurrentMark();
	if (pMark)
	{
		if ( ! pMark->m_rules.GetRuleExpression(0).IsEmpty())
			bEnable = FALSE;
	}
	GetDlgItem(IDC_PD_DELETE_MARK)->EnableWindow(bEnable);
}

void CProdDataPrintView::OnUpdateDeleteAllMarks(CCmdUI* pCmdUI)
{
	GetDlgItem(IDC_PD_DELETE_ALLMARKS)->EnableWindow((m_objectList.GetSize()) ? TRUE : FALSE);
}

void CProdDataPrintView::OnBnClickedPdAnalyzemarksReport()
{
	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
		return;

	AfxMessageBox(pLayout->m_markList.m_strAnalyzeReport);
}

void CProdDataPrintView::OnUpdateAnalyzemarksReport(CCmdUI* pCmdUI)
{
	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
		return;

	GetDlgItem(IDC_PD_ANALYZEMARKS_REPORT)->ShowWindow((pLayout->m_markList.m_strAnalyzeReport.IsEmpty()) ? SW_HIDE : SW_SHOW);
}
