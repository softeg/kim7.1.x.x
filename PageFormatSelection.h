// PageFormatSelection.h : header file
//


class CPageFormat
{
// Attributes
public:
	char			m_szName[16];
	float			m_fWidth, 
					m_fHeight;
	// Some types not used for Win95/NT Version 
	// Only compatibility to previous version
		unsigned char	sammel_nx, sammel_ny;
		float			xwbel_off, ywbel_off;		// Offset der Bel.Pos. relativ zum Nutzenzentrum 
		char			kontur_id[9];
		char			id[16];
	unsigned char	m_ucref_object_x, m_ucref_object_y,	// Position rel. to plate or sheet SHEET|PLATE
					m_ucref_line_x, m_ucref_line_y;		// LEFT|RIGHT  BOTTOM|TOP
	char added[1];	//1 Byte added -> struct alignement old: 2 Byte new: 1 Byte
	float			m_fdistance_x, m_fdistance_y;		// Distance from Ref.-lines 
		char			*text;
		char			drz_belicht_auto;			// Druckzeichen automatisch belichten ?
													// TRUE / FALSE
		char			drz_feste_pos;				// Druckzeichen mit umschlagen/umstuelpen ?
													// TRUE / FALSE
		char			dummy[4];
};


class CPageFormatSelection
{
public:
	CList <CPageFormat,CPageFormat&> m_PageFormatList;

public:
	BOOL 			InitComboBox(CWnd* pDlg, CString& rstrFormatName, float* pfWidth, float* pfHeight, int nIDComboBox);
	void 			SelchangeFormatList(CWnd* pDlg, float* pfWidth, float* pfHeight, CString& strFormatName, int* nPageOrientation, int nIDComboBox);
	void 			NewEntry(CWnd* pDlg, float* pfWidth, float* pfHeight, CString& rstrFormatName, int nPageOrientation, int nIDComboBox);
	void 			ChangeEntry(CWnd* pDlg, float* pfWidth, float* pfHeight, CString& rstrFormatName, int nPageOrientation, int nIDComboBox);
	void 			DeleteEntry(CWnd* pDlg, float* pfWidth, float* pfHeight, CString& rstrFormatName, int nPageOrientation, int nIDComboBox);
	BOOL 			LoadData(void);
	BOOL 			SaveData(void);
	CPageFormat*	FindBySize(float fWidth, float fHeight);
	CPageFormat*	FindByName(CString strName);
	void AFXAPI DDX_PageFormatCombo(CDataExchange* pDX, int nIDC, CString& strValue);
};


