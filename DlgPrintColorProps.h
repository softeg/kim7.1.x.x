#if !defined(AFX_DLGPRINTCOLORPROPS_H__F947B6B1_FC64_11D4_87EC_0040F68C1EF7__INCLUDED_)
#define AFX_DLGPRINTCOLORPROPS_H__F947B6B1_FC64_11D4_87EC_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPrintColorProps.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPrintColorProps 

class CDlgPrintColorProps : public CDialog
{
// Konstruktion
public:
	CDlgPrintColorProps(CWnd* pParent = NULL);   // Standardkonstruktor

//Attributes:
public:
	CColorDefinitionTable m_colorDefTable;
	COLORREF			  m_rgbColor;
	CString				  m_strInitialPrintColorName;

//Operations:
public:
	void UpdateControls();


// Dialogfelddaten
	//{{AFX_DATA(CDlgPrintColorProps)
	enum { IDD = IDD_PRINTCOLOR_PROPS };
	CStatic	m_colorPreview;
	CComboBox	m_colorMappingTargetCombo;
	CString	m_strPrintColorName;
	int		m_nPrintColorType;
	CString	m_strColorMappingTarget;
	int		m_nProcessCMYK;
	int		m_nIPUIndex;
	int		m_nCyan;
	int		m_nKey;
	int		m_nMagenta;
	int		m_nYellow;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPrintColorProps)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPrintColorProps)
	virtual BOOL OnInitDialog();
	afx_msg void OnPrintcolorIsProcess();
	afx_msg void OnPrintcolorIsMapped();
	afx_msg void OnPrintcolorIsSpot();
	afx_msg void OnPaint();
	afx_msg void OnDefineSpotcolor();
	afx_msg void OnSelchangeColormappingTarget();
	afx_msg void OnProcessCyan();
	afx_msg void OnProcessKey();
	afx_msg void OnProcessMagenta();
	afx_msg void OnProcessYellow();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPRINTCOLORPROPS_H__F947B6B1_FC64_11D4_87EC_0040F68C1EF7__INCLUDED_
