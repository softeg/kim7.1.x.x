// DlgProductionManager.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionManager.h"
#include "DlgSelectProcesses.h"


CDlgProductionManager* g_pDlgProductionManager;


CPMStatic::CPMStatic()
{
	m_font.CreateFont(13, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CPMStatic::~CPMStatic()
{
}

void CPMStatic::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC*	pDC		= CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect	rcItem	= lpDrawItemStruct->rcItem;

	CPen pen(PS_SOLID, 1, DARKGRAY);

	if (lpDrawItemStruct->CtlID == IDC_PRODUCTION_PARAMS_FRAME)
	{
		pDC->SelectObject(&pen);
		pDC->SelectStockObject(HOLLOW_BRUSH);
		pDC->Rectangle(rcItem);
		pDC->SelectStockObject(WHITE_BRUSH);
		CRect rcTitle = rcItem; rcTitle.bottom = rcTitle.top + 26;
		pDC->Rectangle(rcTitle);

		CString string;
		CDlgProductionManager* pParent = (CDlgProductionManager*)GetParent();
		switch (pParent->m_productionStepButtons.GetSelectedButtonData())
		{
		case CProductionProfile::Prepress:		string.LoadString(IDS_PROCESS_PREPRESS);	break;
		case CProductionProfile::Printing:		string.LoadString(IDS_PROCESS_PRINTING);	break;
		case CProductionProfile::Cutting:		string.LoadString(IDS_PROCESS_CUTTING);		break;
		case CProductionProfile::Folding:		string.LoadString(IDS_PROCESS_FOLDING);		break;
		case CProductionProfile::Binding:		string.LoadString(IDS_PROCESS_BINDING);		break;
		case CProductionProfile::Trimming:		string.LoadString(IDS_PROCESS_TRIMMING);	break;
		case CProductionProfile::Punching:		string.LoadString(IDS_PROCESS_PUNCHING);	break;
		case CProductionProfile::LayoutPool:	string.LoadString(IDS_DEFAULT_LAYOUTS);		break;
		case CProductionProfile::Rules:			string.LoadString(IDS_RULES);				break;
		}

		pDC->SelectObject(&m_font);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(BLACK);
		CRect rcText = rcItem; rcText.left += 10; rcText.bottom = rcText.top + 26;
		pDC->DrawText(string, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	}
	else
	{
		pDC->SelectObject(&pen);
		pDC->SelectStockObject(HOLLOW_BRUSH);//WHITE_BRUSH);
		//pDC->Rectangle(rcItem);
		//pDC->MoveTo(rcItem.left,  rcItem.top + 25);
		//pDC->LineTo(rcItem.right, rcItem.top + 25);
	}

	pen.DeleteObject();
}


// CDlgProductionManager-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionManager, CDialog)

CDlgProductionManager::CDlgProductionManager(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionManager::IDD, pParent)
	, m_strComment(_T(""))
{
	m_strTitle			= _T("");
	m_bEditChanged		= FALSE;
	m_pPrintGroup		= NULL;
	m_nComponentIndex	= -1;
}

CDlgProductionManager::~CDlgProductionManager()
{
}

void CDlgProductionManager::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_CBString(pDX, IDC_PRODUCTIONPROFILE_LIST, m_strProfileName);
	DDX_Text(pDX, IDC_PRODUCTIONPROFILE_COMMENT_EDIT, m_strComment);
}


#define ID_SELECT_PRODUCTIONSTEP 999

BEGIN_MESSAGE_MAP(CDlgProductionManager, CDialog)
	ON_WM_CTLCOLOR()
	ON_LBN_SELCHANGE(IDC_PRODUCTIONPROFILE_LIST, OnSelchangeProductionProfileList)
	ON_CBN_EDITCHANGE(IDC_PRODUCTIONPROFILE_LIST, OnEditchangeProductionProfileList)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_CHANGE, OnChange)
	ON_COMMAND(ID_SELECT_PRODUCTIONSTEP, OnSelectProductionStep)
	ON_BN_CLICKED(IDOK, &CDlgProductionManager::OnBnClickedOk)
	ON_BN_CLICKED(IDC_PRODUCTIONPROFILE_SELECTPROCESSES, &CDlgProductionManager::OnBnClickedProductionprofileSelectProcesses)
END_MESSAGE_MAP()


// CDlgProductionManager-Meldungshandler


BOOL CDlgProductionManager::OnInitDialog()
{
	CDialog::OnInitDialog();

	g_pDlgProductionManager = this;

	if ( ! m_strTitle.IsEmpty())
	{
		CString strCurTitle;
		GetWindowText(strCurTitle);
		SetWindowText(strCurTitle + m_strTitle);
	}

	m_productionStepFrame.SubclassDlgItem(IDC_PRODUCTIONSTEP_FRAME, this);
	m_productionParamsFrame.SubclassDlgItem(IDC_PRODUCTION_PARAMS_FRAME, this);

	m_productionStepButtons.Create(this, this, DrawItemProductionStep, -1, -1, CSize(0, 0));//, DrawBkItemProductionStep);

	m_strProfileName = m_productionProfile.m_strName;

	LoadProfileList();
	CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRODUCTIONPROFILE_LIST);
	POSITION pos = theApp.m_productionProfiles.GetHeadPosition();
	while (pos)                        
	{
		CProductionProfile& rProfile = theApp.m_productionProfiles.GetAt(pos);
		pBox->AddString(rProfile.m_strName);
		theApp.m_productionProfiles.GetNext(pos);
	}
	int nSel = pBox->FindStringExact(-1, m_strProfileName);
	if ( (nSel < 0) && (pBox->GetCount() > 0) )
		nSel = 0;

	pBox->SetCurSel(nSel);
	if (nSel != CB_ERR)
		pBox->GetLBText(nSel, m_strProfileName);

	m_productionProfile = theApp.m_productionProfiles.FindProfile(m_strProfileName);

	m_strProfileName = m_productionProfile.m_strName;
	m_strComment	 = m_productionProfile.m_strComment;	

	InitProcessButtons();

	InitData();

	m_oldProductionProfile = m_productionProfile;

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProductionManager::InitProcessButtons()
{
	GetDlgItem(IDC_PRODUCTIONSTEP_FRAME)->Invalidate(); 
	GetDlgItem(IDC_PRODUCTIONSTEP_FRAME)->UpdateWindow(); 

	m_productionStepButtons.RemoveAll();

	CRect rcButtonFrame;
	m_productionStepFrame.GetWindowRect(rcButtonFrame);
	ScreenToClient(rcButtonFrame);
	rcButtonFrame.top += 40;
	rcButtonFrame.bottom = rcButtonFrame.top + 40; 
	rcButtonFrame.DeflateRect(5, 0);

	for (int i = 0; i < m_productionProfile.m_activeProcesses.GetSize(); i++)
	{
		int nID = m_productionProfile.m_activeProcesses[i];
		m_productionStepButtons.AddButton(_T(""), _T(""), ID_SELECT_PRODUCTIONSTEP, rcButtonFrame, -1, (ULONG_PTR)nID, FALSE);
		rcButtonFrame.OffsetRect(0, rcButtonFrame.Height());
	}

	m_productionStepFrame.GetWindowRect(rcButtonFrame);
	ScreenToClient(rcButtonFrame);
	rcButtonFrame.top = rcButtonFrame.bottom + 10;
	rcButtonFrame.bottom = rcButtonFrame.top + 40; 
	rcButtonFrame.DeflateRect(5, 0);
	m_productionStepButtons.AddButton(_T(""), _T(""), ID_SELECT_PRODUCTIONSTEP, rcButtonFrame, -1, (ULONG_PTR)CProductionProfile::LayoutPool, FALSE);
	rcButtonFrame.OffsetRect(0, rcButtonFrame.Height());
	m_productionStepButtons.AddButton(_T(""), _T(""), ID_SELECT_PRODUCTIONSTEP, rcButtonFrame, -1, (ULONG_PTR)CProductionProfile::Rules, FALSE);

	m_productionStepButtons.SelectButton(0);
}

void CDlgProductionManager::InitData()
{
	GetDlgItem(IDC_PRODUCTION_PARAMS_FRAME)->Invalidate(); 
	GetDlgItem(IDC_PRODUCTION_PARAMS_FRAME)->UpdateWindow(); 

	if (m_dlgProductionPrepressParams.m_hWnd)
	{
		m_dlgProductionPrepressParams.ShowWindow(SW_HIDE);
		m_dlgProductionPrepressParams.InitData();
	}
	if (m_dlgProductionPressParams.m_hWnd)
	{
		m_dlgProductionPressParams.ShowWindow(SW_HIDE);
		m_dlgProductionPressParams.InitData();
	}
	if (m_dlgProductionCuttingParams.m_hWnd)
	{
		m_dlgProductionCuttingParams.ShowWindow(SW_HIDE);
		m_dlgProductionCuttingParams.InitData();
	}
	if (m_dlgProductionFoldingParams.m_hWnd)
	{
		m_dlgProductionFoldingParams.ShowWindow(SW_HIDE);
		m_dlgProductionFoldingParams.InitData();
	}
	if (m_dlgProductionBindingParams.m_hWnd)
	{
		m_dlgProductionBindingParams.ShowWindow(SW_HIDE);
		m_dlgProductionBindingParams.InitData();
	}
	if (m_dlgProductionTrimmingParams.m_hWnd)
	{
		m_dlgProductionTrimmingParams.ShowWindow(SW_HIDE);
		m_dlgProductionTrimmingParams.InitData();
	}
	if (m_dlgProductionPunchingParams.m_hWnd)
	{
		m_dlgProductionPunchingParams.ShowWindow(SW_HIDE);
		m_dlgProductionPunchingParams.InitData();
	}
	if (m_dlgProductionLayoutPool.m_hWnd)
	{
		m_dlgProductionLayoutPool.ShowWindow(SW_HIDE);
		m_dlgProductionLayoutPool.InitData();
	}
	if (m_dlgProductionProfileRules.m_hWnd)
	{
		m_dlgProductionProfileRules.ShowWindow(SW_HIDE);
		m_dlgProductionProfileRules.InitData();
	}

	CRect rcFrame, rcSubDlg;
	GetDlgItem(IDC_PRODUCTION_PARAMS_FRAME)->GetWindowRect(rcFrame); ScreenToClient(rcFrame); 

	int		 nID  = -1;
	CDialog* pDlg = NULL;
	switch (m_productionStepButtons.GetSelectedButtonData())
	{
	case CProductionProfile::Prepress:		pDlg = &m_dlgProductionPrepressParams;		nID = IDD_PRODUCTION_PREPRESSPARAMS;	break;
	case CProductionProfile::Printing:		pDlg = &m_dlgProductionPressParams;			nID = IDD_PRODUCTION_PRESSPARAMS;		break;
	case CProductionProfile::Cutting:		pDlg = &m_dlgProductionCuttingParams;		nID = IDD_PRODUCTION_CUTTINGPARAMS;		break;
	case CProductionProfile::Folding:		pDlg = &m_dlgProductionFoldingParams;		nID = IDD_PRODUCTION_FOLDINGPARAMS;		break;
	case CProductionProfile::Binding:		pDlg = &m_dlgProductionBindingParams;		nID = IDD_PRODUCTION_BINDINGPARAMS;		break;
	case CProductionProfile::Trimming:		pDlg = &m_dlgProductionTrimmingParams;		nID = IDD_PRODUCTION_TRIMMINGPARAMS;	break;
	case CProductionProfile::Punching:		pDlg = &m_dlgProductionPunchingParams;		nID = IDD_PRODUCTION_PUNCHINGPARAMS;	break;
	case CProductionProfile::LayoutPool:	pDlg = &m_dlgProductionLayoutPool;			nID = IDD_PRODUCTION_LAYOUTPOOL;		break;
	case CProductionProfile::Rules:			pDlg = &m_dlgProductionProfileRules;		nID = IDD_PRODUCTIONPROFILE_RULES;		break;
	}
	
	if ( (nID != -1) && (pDlg) )
	{
		if ( ! pDlg->m_hWnd)
		{
			pDlg->Create(nID, this);
			pDlg->GetClientRect(rcSubDlg);
		}

		rcFrame.DeflateRect(1,1); rcFrame.top += 26;
		pDlg->SetWindowPos(NULL, rcFrame.left, rcFrame.top, rcFrame.Width(), rcFrame.Height(), SWP_NOZORDER | SWP_SHOWWINDOW);
	}
}

void CDlgProductionManager::SaveData()
{
	if (m_dlgProductionPrepressParams.m_hWnd)
	{
		//m_dlgProductionPrepressParams.ShowWindow(SW_HIDE);
		m_dlgProductionPrepressParams.SaveData();
	}
	if (m_dlgProductionPressParams.m_hWnd)
	{
		//m_dlgProductionPressParams.ShowWindow(SW_HIDE);
		m_dlgProductionPressParams.SaveData();
	}
	if (m_dlgProductionCuttingParams.m_hWnd)
	{
		//m_dlgProductionCuttingParams.ShowWindow(SW_HIDE);
		m_dlgProductionCuttingParams.SaveData();
	}
	if (m_dlgProductionFoldingParams.m_hWnd)
	{
		//m_dlgProductionFoldingParams.ShowWindow(SW_HIDE);
		m_dlgProductionFoldingParams.SaveData();
	}
	if (m_dlgProductionBindingParams.m_hWnd)
	{
		//m_dlgProductionBindingParams.ShowWindow(SW_HIDE);
		m_dlgProductionBindingParams.SaveData();
	}
	if (m_dlgProductionTrimmingParams.m_hWnd)
	{
		//m_dlgProductionTrimmingParams.ShowWindow(SW_HIDE);
		m_dlgProductionTrimmingParams.SaveData();
	}
	if (m_dlgProductionPunchingParams.m_hWnd)
	{
		//m_dlgProductionPunchingParams.ShowWindow(SW_HIDE);
		m_dlgProductionPunchingParams.SaveData();
	}
	if (m_dlgProductionLayoutPool.m_hWnd)
	{
		//m_dlgProductionProfileRules.ShowWindow(SW_HIDE);
		m_dlgProductionLayoutPool.SaveData();
	}
	if (m_dlgProductionProfileRules.m_hWnd)
	{
		//m_dlgProductionProfileRules.ShowWindow(SW_HIDE);
		m_dlgProductionProfileRules.SaveData();
	}
}

void CDlgProductionManager::CheckModified()
{
	if (m_productionProfile != m_oldProductionProfile)
	{
		BOOL bEntryIsNew = TRUE; 
		POSITION pos = theApp.m_productionProfiles.GetHeadPosition();
		while (pos)                        
		{
			CProductionProfile& rProfile = theApp.m_productionProfiles.GetAt(pos);
			if (m_productionProfile.m_strName == rProfile.m_strName)
				bEntryIsNew = FALSE;
			theApp.m_productionProfiles.GetNext(pos);
		}

		CString strMsg; 
		if (bEntryIsNew)
			OnNew();
		else
			OnChange();
	}
}

void CDlgProductionManager::OnSelchangeProductionProfileList() 
{
	SaveData();

	CheckModified();

	CString strProfileName;
	CComboBox* pBox	= (CComboBox*)GetDlgItem (IDC_PRODUCTIONPROFILE_LIST);
	UINT	   nSel = pBox->GetCurSel();
	if (nSel != CB_ERR)
		pBox->GetLBText(nSel, strProfileName);

	m_productionProfile = theApp.m_productionProfiles.FindProfile(strProfileName);
	m_strProfileName	= m_productionProfile.m_strName;
	m_strComment		= m_productionProfile.m_strComment;	
	
	InitProcessButtons();
	InitData();

	m_oldProductionProfile = m_productionProfile;

	m_bEditChanged = FALSE;
	CButton *pButton = (CButton*)GetDlgItem(IDC_CHANGE);
	pButton->EnableWindow(TRUE);
	
	UpdateData(FALSE);
}

void CDlgProductionManager::OnEditchangeProductionProfileList() 
{
	m_bEditChanged = TRUE;
	CButton *pButton = (CButton*)GetDlgItem(IDC_CHANGE);
	pButton->EnableWindow(FALSE);
}

void CDlgProductionManager::OnNew() 
{
	UpdateData(TRUE);

	SaveData();

	CString strUserDefined; 
	strUserDefined.LoadString(IDS_USERDEFINED);
	int nRet = (m_strProfileName != strUserDefined) ? AfxMessageBox(IDS_NEW_LISTENTRY, MB_YESNO) : IDYES;
	if (nRet == IDYES)	
	{
		m_productionProfile.m_strName	 = m_strProfileName;
		m_productionProfile.m_strComment = m_strComment;

		BOOL bDoubleEntry = FALSE; 
		POSITION pos = theApp.m_productionProfiles.GetHeadPosition();
		while (pos)                        
		{
			CProductionProfile& rProfile = theApp.m_productionProfiles.GetAt(pos);
			if (m_productionProfile.m_strName == rProfile.m_strName)
				bDoubleEntry = TRUE;
			theApp.m_productionProfiles.GetNext(pos);
		}

		if ( ! bDoubleEntry)
		{
			m_oldProductionProfile = m_productionProfile;

			theApp.m_productionProfiles.AddTail(m_productionProfile);
			if ( ! StoreProfileList())
				AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

			CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRODUCTIONPROFILE_LIST);
			pBox->AddString(m_productionProfile.m_strName);
			pBox->SetCurSel(pBox->FindStringExact(-1, m_productionProfile.m_strName));
			OnSelchangeProductionProfileList();	
		}
		else // Name already exists
			if ((m_strProfileName == strUserDefined))
				OnChange();
			else
				AfxMessageBox(IDS_DOUBLEENTRY_NOTOK, MB_OK);
	}
}

void CDlgProductionManager::OnDel() 
{
	if (theApp.m_productionProfiles.IsEmpty()) 
		return;

	if (AfxMessageBox(IDS_DEL_LISTENTRY, MB_YESNO) == IDYES)	
	{
		CString strProfileName;
		CComboBox* pBox = (CComboBox*)GetDlgItem (IDC_PRODUCTIONPROFILE_LIST);
		int		   nSel = pBox->GetCurSel();
		if (nSel != CB_ERR)
			pBox->GetLBText(nSel, strProfileName);
		else
			return;

		theApp.m_productionProfiles.RemoveProfile(strProfileName);

		pBox->DeleteString(nSel);
		if ( ! (theApp.m_productionProfiles.IsEmpty())) 
		{
			pBox->SetCurSel(0);
			OnSelchangeProductionProfileList();	
		}
		else
		{
			m_strProfileName = _T("");
			m_strComment = _T("");
			m_productionProfile = CProductionProfile();
			UpdateData(FALSE);      
		}
		if ( ! StoreProfileList())
		{
			AfxMessageBox(IDS_DEL_NOTOK, MB_OK);
			LoadProfileList();
			UpdateData(FALSE);      
		}
	}
}

void CDlgProductionManager::OnChange() 
{
	UpdateData(TRUE);

	SaveData();

	CString strUserDefined; 
	strUserDefined.LoadString(IDS_USERDEFINED);
	int nRet = (m_strProfileName != strUserDefined) ? AfxMessageBox(IDS_CHANGE_ENTRY, MB_YESNO) : IDYES;
	if (nRet == IDYES)	
	{
		//m_productionProfile.m_strName	 = m_strProfileName;
		m_productionProfile.m_strComment = m_strComment;

		//CComboBox* pBox = (CComboBox*)GetDlgItem (IDC_PRODUCTIONPROFILE_LIST);
		//int		   nSel = pBox->FindStringExact(-1, m_strProfileName);
		//if (nSel != CB_ERR)
		//	pBox->GetLBText(nSel, strProfileName);

		CProductionProfile& rProfile = theApp.m_productionProfiles.FindProfile(m_strProfileName);
		//if (pos)
		//{
			m_oldProductionProfile = m_productionProfile;

			rProfile = m_productionProfile;
			//theApp.m_productionProfiles.SetAt(pos, m_productionProfile);
			if ( ! StoreProfileList())
				AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

			//CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_PRODUCTIONPROFILE_LIST);
			//pBox->SetCurSel(pBox->FindStringExact(-1, m_strProfileName));
			//OnSelchangeProductionProfileList();	
		//}
	}
	//else
	//{
	//	m_strProfileName.LoadString(IDS_USERDEFINED);
	//	UpdateData(FALSE);
	//	OnNew();
	//}
}

extern int gCount;

BOOL CDlgProductionManager::LoadProfileList()
{
	TCHAR			pszFileName[MAX_PATH];
	CFile			file;
	CFileException	fileException;

	_tcscpy_s(pszFileName, MAX_PATH, theApp.GetDataFolder()); // TODO: Remove this construct when old versions (< 3.0.1.0) are updated
	_tcscat_s(pszFileName, MAX_PATH, _T("\\ProductionProfiles.def"));

	if ( ! file.Open(pszFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&file, CArchive::load);
		theApp.m_productionProfiles.RemoveAll();
		theApp.m_productionProfiles.Serialize(archive);

		archive.Close();
		file.Close();

		if (theApp.m_productionProfiles.IsEmpty()) 
			return FALSE; 
		else
			return TRUE;  	
	}
}

BOOL CDlgProductionManager::StoreProfileList()
{
	TCHAR			pszFileName[MAX_PATH];
	CFile			file;
	CFileException	fileException;

	_tcscpy_s(pszFileName, MAX_PATH, theApp.GetDataFolder()); // TODO: Verify the path
	_tcscat_s(pszFileName, MAX_PATH, _T("\\ProductionProfiles.def"));

	if ( ! file.Open(pszFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&file, CArchive::store);
		theApp.m_productionProfiles.Serialize(archive);
		archive.Close();
		file.Close();

		return TRUE;
	}
}

void CDlgProductionManager::DrawItemProductionStep(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState)
{
	CDC*	pDC			= CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect	rcItem		= lpDrawItemStruct->rcItem;

	CFont smallFont;
	smallFont.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);

	pDC->SetBkMode(TRANSPARENT);

	CString string;
	CImage  img;
	int		nNumLayouts = 0;
	switch (lpDrawItemStruct->itemData)
	{
	case CProductionProfile::Prepress:		img.LoadFromResource(theApp.m_hInstance, IDB_PREPRESS);				string.LoadString(IDS_PROCESS_PREPRESS);	break;
	case CProductionProfile::Printing:		img.LoadFromResource(theApp.m_hInstance, IDB_PRINTING_MACHINE);		string.LoadString(IDS_PROCESS_PRINTING);	break;
	case CProductionProfile::Cutting:		img.LoadFromResource(theApp.m_hInstance, IDB_CUTMACHINE);			string.LoadString(IDS_PROCESS_CUTTING);		break;
	case CProductionProfile::Folding:		img.LoadFromResource(theApp.m_hInstance, IDB_FOLD_CUT);				string.LoadString(IDS_PROCESS_FOLDING);		break;
	case CProductionProfile::Binding:		img.LoadFromResource(theApp.m_hInstance, IDB_BINDING);				string.LoadString(IDS_PROCESS_BINDING);		break;
	case CProductionProfile::Trimming:		img.LoadFromResource(theApp.m_hInstance, IDB_TRIMMING);				string.LoadString(IDS_PROCESS_TRIMMING);	break;
	case CProductionProfile::Punching:		img.LoadFromResource(theApp.m_hInstance, IDB_PUNCHING);				string.LoadString(IDS_PROCESS_PUNCHING);	break;
	case CProductionProfile::LayoutPool:	img.LoadFromResource(theApp.m_hInstance, IDB_DEFAULT_LAYOUT);		string.LoadString(IDS_DEFAULT_LAYOUTS);		
											nNumLayouts = g_pDlgProductionManager->m_productionProfile.m_layoutPool.GetSize();								break;
	case CProductionProfile::Rules:			img.LoadFromResource(theApp.m_hInstance, IDB_RULES);				string.LoadString(IDS_RULES);				break;
	}

	CRect rcImg;
	if ( ! img.IsNull())
	{
		rcImg.left = 5; rcImg.top = rcItem.CenterPoint().y - img.GetHeight() / 2; rcImg.right = rcImg.left + img.GetWidth(); rcImg.bottom = rcImg.top + img.GetHeight();
		img.TransparentBlt(lpDrawItemStruct->hDC, rcImg, RGB(192,192,192));
	}

	CRect rcText = lpDrawItemStruct->rcItem; rcText.left += 40;
	pDC->SetTextColor(BLACK);
	pDC->DrawText(string, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	if (nNumLayouts > 0)
	{
		CFont boldFont;
		boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		pDC->SelectObject(&boldFont);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(BLACK);//GUICOLOR_CAPTION);
		string.Format(_T(" %d "), nNumLayouts);
		CRect rcNum = rcImg; rcNum.top = rcImg.CenterPoint().y - 14; rcNum.bottom = rcNum.top + 14; rcNum.right = rcImg.right + 2; rcNum.left = rcNum.right - pDC->GetTextExtent(string).cx;
		rcNum.InflateRect(1, 1);
		CBrush brush(WHITE);
		CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
		CBrush* pOldBrush = pDC->SelectObject(&brush);
		CPen*	pOldPen	  = pDC->SelectObject(&pen);
		pDC->RoundRect(rcNum, CPoint(8, 8));
		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		pDC->DrawText(string, rcNum, DT_CENTER | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS);
		boldFont.DeleteObject();
	}

	//if (lpDrawItemStruct->itemData == CProductionProfile::Rules)
	//{
	//	CPen pen(PS_SOLID, 1, DARKGRAY);
	//	pDC->SelectObject(&pen);
	//	pDC->MoveTo(rcItem.left,  rcItem.top + 1);
	//	pDC->LineTo(rcItem.right, rcItem.top + 1);
	//	pen.DeleteObject();
	//}

	smallFont.DeleteObject();
}

void CDlgProductionManager::DrawBkItemProductionStep(CDC* pDC, CRect rcItem, CWnd* pWnd)
{
	rcItem.InflateRect(1,1);
	pDC->SelectStockObject(NULL_PEN);
	pDC->SelectStockObject(WHITE_BRUSH);
	pDC->Rectangle(rcItem);
}

HBRUSH CDlgProductionManager::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	return hbr;
}

void CDlgProductionManager::OnSelectProductionStep()
{
	SaveData();

	//m_nProductPartIndex = m_productGroupSelectorButtons.GetSelectedButtonData();

	InitData();
}

void CDlgProductionManager::OnBnClickedOk()
{
	UpdateData(TRUE);

	m_productionProfile.m_strName	 = m_strProfileName;
	m_productionProfile.m_strComment = m_strComment;

	if (m_dlgProductionPrepressParams.m_hWnd)
		m_dlgProductionPrepressParams.SaveData();
	if (m_dlgProductionPressParams.m_hWnd)
		m_dlgProductionPressParams.SaveData();
	if (m_dlgProductionCuttingParams.m_hWnd)
		m_dlgProductionCuttingParams.SaveData();
	if (m_dlgProductionFoldingParams.m_hWnd)
		m_dlgProductionFoldingParams.SaveData();
	if (m_dlgProductionBindingParams.m_hWnd)
		m_dlgProductionBindingParams.SaveData();
	if (m_dlgProductionTrimmingParams.m_hWnd)
		m_dlgProductionTrimmingParams.SaveData();
	if (m_dlgProductionPunchingParams.m_hWnd)
		m_dlgProductionPunchingParams.SaveData();
	if (m_dlgProductionLayoutPool.m_hWnd)
		m_dlgProductionLayoutPool.SaveData();
	if (m_dlgProductionProfileRules.m_hWnd)
		m_dlgProductionProfileRules.SaveData();

	CheckModified();

	OnOK();
}

void CDlgProductionManager::OnBnClickedProductionprofileSelectProcesses()
{
	SaveData();

	CDlgSelectProcesses dlg;
	dlg.m_selectedProcesses.Append(m_productionProfile.m_activeProcesses);
	if (dlg.DoModal() == IDCANCEL)
		return;

	m_productionProfile.m_activeProcesses.RemoveAll();
	m_productionProfile.m_activeProcesses.Append(dlg.m_selectedProcesses);

	InitProcessButtons();
	InitData();
}
