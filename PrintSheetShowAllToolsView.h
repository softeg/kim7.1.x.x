#pragma once

#include "OwnerDrawnButtonList.h"
#include "ToolBarGroup.h"


// CPrintSheetShowAllToolsView-Formularansicht

class CPrintSheetShowAllToolsView : public CFormView
{
	DECLARE_DYNCREATE(CPrintSheetShowAllToolsView)

protected:
	CPrintSheetShowAllToolsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetShowAllToolsView();

public:
	enum { IDD = IDD_PRINTSHEETSHOWALLTOOLSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


public:
	class CNewPrintSheetFrame*	m_pNewPrintSheetFrame;
	COwnerDrawnButtonList		m_displayToolBar, m_modifyToolBar;
	CToolBarGroup				m_displayToolBarGroup, m_modifyToolBarGroup;


public:
	virtual void OnInitialUpdate();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnUpdateViewFrontside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBackside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBothsides(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFoldsheetCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlateCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBitmapCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjectsRotate(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjectsTurn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjectsTumble(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjectsrotateClock(CCmdUI* pCmdUI);
	afx_msg void OnUpdateObjectsrotateCounterclock(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteSelection(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUndo(CCmdUI* pCmdUI);
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};


