#pragma once


// CDlgSelectPressDevicePopup-Dialogfeld

class CDlgSelectPressDevicePopup : public CDialog
{
	DECLARE_DYNAMIC(CDlgSelectPressDevicePopup)

public:
	CDlgSelectPressDevicePopup(CWnd* pParent = NULL, BOOL bRunModal = FALSE);   // Standardkonstruktor
	CDlgSelectPressDevicePopup(CPressDevice* pPressDevice, CLayout* pLayout, CWnd* pParent = NULL, BOOL bRunModal = FALSE);   // Standardkonstruktor
	virtual ~CDlgSelectPressDevicePopup();

// Dialogfelddaten
	enum { IDD = IDD_SELECT_PRESSDEVICE_POPUP };


public:
	CWnd*			m_pParent;
	CPressDevice*	m_pPressDevice;
	CLayout*		m_pLayout;
	void			(*m_pFnSelChangeHandler)(CPressDevice* pNewPressDevice, int nNewPressDeviceIndex, CDlgSelectPressDevicePopup* pDlg);

protected:
	CFont			m_smallFont;
	CImageList*		m_pPressDeviceImageList;
	CListCtrl		m_pressDeviceList;
	BOOL			m_bLocked;
	BOOL			m_bRunModal;


public:
	void InitPressDeviceList();

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedPdsDefinePressdeviceButton();
	afx_msg void OnNMCustomdrawPdsPressdevicelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedPdsPressdevicelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
};
