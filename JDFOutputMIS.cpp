#include "stdafx.h"
#include "Imposition Manager.h"
#include "PDFOutput.h"
#include "JDFOutputImposition.h"
#include "JDFOutputMIS.h"
#include "XMLCommon.h"



CJDFOutputMIS::CJDFOutputMIS(void)
{
	setlocale(LC_NUMERIC, "english");	// because when german setted, sscanf() does expect numbers with Comma like "0,138"
}

CJDFOutputMIS::~CJDFOutputMIS(void)
{
	m_nMarkOrd = 0;

	CString strLanguage;
	switch (theApp.settings.m_iLanguage)
	{
		case 0: strLanguage = _T("GER"); setlocale(LC_ALL, "german");	break;
		case 1: strLanguage = _T("ENG"); setlocale(LC_ALL, "english");	break;
		case 2: strLanguage = _T("FRA"); break;
		case 3: strLanguage = _T("ITA"); break;
		case 4: strLanguage = _T("SPA"); break;
		case 5: strLanguage = _T("HOL"); break;	// No holl. MFC4x.dll available
		//Default would already be set at reading ini-file
	}
}

BOOL CJDFOutputMIS::Export(CString strJDFJobTicketFile, CString strOutFile)
{
	m_strOutFile = strOutFile;
	CString strDrive, strDir, strFname, strExt;
	_tsplitpath((LPCTSTR)strOutFile, strDrive.GetBuffer(_MAX_DRIVE), strDir.GetBuffer(_MAX_DIR), strFname.GetBuffer(_MAX_FNAME), strExt.GetBuffer(_MAX_EXT));
	m_strLocation.Format(_T("%s%s"), strDrive, strDir);

	m_pJDFDoc = new JDFDoc(0);
	parser	  = new JDFParser();

	try
	{
		parser->Parse((WString)strJDFJobTicketFile);
	}catch (JDFException e)
	{
		CString strMsg;
		strMsg.Format(_T("JDF API error: %s"), e.getMessage().getBytes());
		KIMOpenLogMessage(strMsg, MB_ICONERROR);
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	*m_pJDFDoc = parser->GetRoot();

	// set up the root process
	JDFNode root=m_pJDFDoc->GetJDFRoot();

	if (root.isNull())
	{
		KIMOpenLogMessage(_T("JDF error! Root is NULL"), MB_ICONERROR);
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	if (root.GetType() == _T("Product"))
	{
		vJDFNode vNode=root.GetvJDFNode();
		for(int i=0;i<vNode.size();i++)
		{
			JDFNode node=vNode[i];
			if (node == root)
				continue;
			if (node.GetType() == _T("Product"))
				ProcessProductPart(node);
			else
			if (node.GetType() == _T("ProcessGroup"))
				ProcessProductPart(node);
		}

		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			// output marks to pdf file
			CString strFilename;	strFilename.Format(_T("%s_Marks.pdf"), strFname);
			CString strLocation;	strLocation.Format(_T("%sMarks"), m_strLocation);
			CMediaSize media(_T(""), 1.0f, 1.0f, 0, 0.0f, 0.0f, FALSE); media.SetAlign(CMediaSize::AlignToPlate);
			CPDFTargetProperties targetProps(CPDFTargetProperties::TypeJDFFolder, _T(""), strLocation, media);
			targetProps.m_strOutputFilename = strFilename;
			CPDFOutput pdfOutput(targetProps, _T(""));
			BOOL bOverwriteAll = -1;
			pdfOutput.ProcessPrintSheetsJobInOne(&pDoc->m_PrintSheetList, bOverwriteAll, FALSE, FALSE, 2, NULL, 0);//, nProductPartIndex);	// not AutoCmd, not SendIfPrinter, marks only
		}
	}

	m_pJDFDoc->Write2File((WString)strOutFile);

	delete parser;
	delete m_pJDFDoc;

	return FALSE;
}

BOOL CJDFOutputMIS::ProcessProductPart(JDFNode productNode)
{
	setlocale(LC_NUMERIC, "english");	// because when german setted, sscanf() does expect numbers with Comma like "0,138"

	JDFResourceLinkPool linkPool = productNode.GetResourceLinkPool();
	//vElement vStrippingParams = linkPool.GetInOutLinks(TRUE, FALSE, _T("StrippingParams"));
	//if ( ! vStrippingParams.size())
	//{
	//	vJDFNode vNode = productNode.GetvJDFNode();
	//	for(int i = 0; i < vNode.size(); i++)
	//	{
	//		JDFNode node = vNode[i];
	//		linkPool = node.GetResourceLinkPool();
	//		vStrippingParams = linkPool.GetInOutLinks(TRUE, FALSE, _T("StrippingParams"));
	//		if (vStrippingParams.size())
	//			break;
	//	}
	//}
	//if (vStrippingParams.size())
	//{
	//	JDFStrippingParams strippingParams = vStrippingParams[0];
	//	strippingParams.SetAvailable(false);
	//}

	CString strID = productNode.GetID().c_str();

	//linkPool = productNode.GetResourceLinkPool();
	JDFNode strippingProcessNode = productNode;
	vElement vLayouts = linkPool.GetInOutLinks(FALSE, FALSE, _T("Layout"));
	if ( ! vLayouts.size())
	{
		vJDFNode vNode = productNode.GetvJDFNode();
		for(int i = 0; i < vNode.size(); i++)
		{
			strippingProcessNode = vNode[i];
			linkPool = strippingProcessNode.GetResourceLinkPool();
			vLayouts = linkPool.GetInOutLinks(FALSE, FALSE, _T("Layout"));
			if (vLayouts.size())
				break;
		}
	}

	if ( ! vLayouts.size())
		return FALSE;

	JDFLayout layout = vLayouts[0];

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	//int nProductPartIndex = pDoc->GetBoundProduct(0).GetProductPartIndex(productNode.GetID().getBytes());
	//if (nProductPartIndex == -1)
	//	return FALSE;

	m_nMarkOrd = 0;

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		//if (rPrintSheet.GetPrintGroupIndex() != nProductPartIndex)
		//	continue;

		int		 nSkip = 0;
		JDFSheet sheet;
		BOOL	 bFound = FALSE;
		while ( ! bFound)
		{
			JDFSignature signature = layout.GetSignature(nSkip++);
			if (signature.isNull())
				break;
			sheet = signature.GetSheet(0);
			if ( ! sheet.isNull())
				if (sheet.GetSheetName().getBytes() == rPrintSheet.GetNumber())
				{
					bFound = TRUE;
					break;
				}
		}

		if ( ! bFound)
		{
			JDFSignature signature = layout.AppendSignature();
			sheet = signature.AppendSheet();
			sheet.SetName((WString)rPrintSheet.GetNumber());
		}

		MakeSheet(sheet, rPrintSheet);
	}

	layout.SetAvailable();

	linkPool = strippingProcessNode.GetResourceLinkPool();
	vElement vRunLists = linkPool.GetInOutLinks(FALSE, FALSE, _T("RunList"));
	if ( ! vRunLists.size())
	{
		vJDFNode vNode = productNode.GetvJDFNode();
		for(int i = 0; i < vNode.size(); i++)
		{
			JDFNode node = vNode[i];
			linkPool = node.GetResourceLinkPool();
			vRunLists = linkPool.GetInOutLinks(FALSE, FALSE, _T("RunList"));
			if (vRunLists.size())
				break;
		}
	}

	if ( ! vRunLists.size())
		return FALSE;

	JDFRunList runList = vRunLists[0];

	if ( ! AddMarksRunList(runList, productNode.GetDescriptiveName().getBytes()))
		return FALSE;

	strippingProcessNode.SetStatus(JDFElement::Status_Completed);

	return TRUE;
}

BOOL CJDFOutputMIS::MakeSheet(JDFSheet sheet, CPrintSheet& rPrintSheet)
{
	CLayout* pLayout = rPrintSheet.GetLayout();
	if ( ! pLayout)
		return FALSE;

	float fMediaWidth  = pLayout->m_FrontSide.m_fPaperWidth;
	float fMediaHeight = pLayout->m_FrontSide.m_fPaperHeight;
	CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
	if (pPressDevice)
	{
		fMediaWidth  = pPressDevice->m_fPlateWidth;
		fMediaHeight = pPressDevice->m_fPlateHeight;
	}	

	CString strContentsBox = _T("0 0 0 0");
	strContentsBox.Format(_T("%.3f %.3f %.3f %.3f"), 0.0f, 0.0f, fMediaWidth / fPSPoint, fMediaHeight / fPSPoint);

	sheet.SetSurfaceContentsBox((WString)strContentsBox);

	BOOL bFrontOnly = TRUE;
	switch (pLayout->m_nProductType)
	{
		case WORK_AND_BACK:		sheet.SetSourceWorkStyle(JDFSheet::SourceWorkStyle_WorkAndBack);	
								bFrontOnly = FALSE;
								break;
		case WORK_AND_TURN:		sheet.SetSourceWorkStyle(JDFSheet::SourceWorkStyle_WorkAndTurn);	break;
		case WORK_AND_TUMBLE:	sheet.SetSourceWorkStyle(JDFSheet::SourceWorkStyle_WorkAndTumble);	break;
		case WORK_SINGLE_SIDE:	sheet.SetSourceWorkStyle(JDFSheet::SourceWorkStyle_Simplex);		break;
		default:				sheet.SetSourceWorkStyle(JDFSheet::SourceWorkStyle_WorkAndBack);		
								bFrontOnly = FALSE;
								break;
	}

	MakeSurface(sheet, rPrintSheet, FRONTSIDE, strContentsBox);

	if ( ! bFrontOnly)
		MakeSurface(sheet, rPrintSheet, BACKSIDE, strContentsBox);

	return TRUE;
}

BOOL CJDFOutputMIS::MakeSurface(JDFSheet sheet, CPrintSheet& rPrintSheet, int nSide, CString strContentsBox)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	JDFSurface surface = (nSide == FRONTSIDE) ? sheet.GetCreateFrontSurface() : sheet.GetCreateBackSurface();
	surface.SetSurfaceContentsBox((WString)strContentsBox);

	// append a markobject 
	JDFMarkObject markObject = surface.AppendMarkObject();
	markObject.SetCTM(JDFMatrix("1 0 0 1 0 0"));
	markObject.SetOrd(m_nMarkOrd++);
	markObject.init();
	markObject.SetClipBox(JDFRectangle((WString)strContentsBox));

	CLayout* pLayout = rPrintSheet.GetLayout();
	if ( ! pLayout)
		return FALSE;

	float fMediaWidth  = pLayout->m_FrontSide.m_fPaperWidth;
	float fMediaHeight = pLayout->m_FrontSide.m_fPaperHeight;
	CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
	if (pPressDevice)
	{
		fMediaWidth  = pPressDevice->m_fPlateWidth;
		fMediaHeight = pPressDevice->m_fPlateHeight;
	}	

	float fSheetPosX = pLayout->m_FrontSide.m_fPaperLeft; 
	float fSheetPosY = pLayout->m_FrontSide.m_fPaperBottom;

	CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;
	CPlate& rPlate = (rPlateList.GetCount()) ? rPlateList.GetHead() : rPlateList.m_defaultPlate;

	srand( (unsigned)time( NULL ) );
	int nRan = rand();
	int nNumPages = rPrintSheet.GetNumPages(nSide);
	int nRandomPageIndex = (nNumPages > 0) ? nRan % nNumPages : 0;

	POSITION pos = rPlate.m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure&	   rExposure = rPlate.m_ExposureSequence.GetNext(pos);
		CLayoutObject* pObject	 = rExposure.GetLayoutObject(rPrintSheet.m_nLayoutIndex, nSide);
		if (!pObject)
			continue;
		if (pObject->m_nType != CLayoutObject::Page)
			continue;

		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(&rPrintSheet);
		if (pObjectTemplate)
		{
			CPageSource*		pPageSource		  = pObjectTemplate->GetObjectSource	  (rPlate.m_nColorDefinitionIndex, TRUE, &rPrintSheet, nSide);
			CPageSourceHeader*	pPageSourceHeader = pObjectTemplate->GetObjectSourceHeader(rPlate.m_nColorDefinitionIndex, TRUE, &rPrintSheet, nSide);
			SEPARATIONINFO*		pSepInfo		  = pObjectTemplate->GetSeparationInfo	  (rPlate.m_nColorDefinitionIndex, TRUE, &rPrintSheet, nSide);

			float fSheetClipBoxLeft	  = rExposure.GetSheetBleedBox(LEFT,   pObject, pObjectTemplate) - fSheetPosX;
			float fSheetClipBoxBottom = rExposure.GetSheetBleedBox(BOTTOM, pObject, pObjectTemplate) - fSheetPosY;
			float fSheetClipBoxRight  = rExposure.GetSheetBleedBox(RIGHT,  pObject, pObjectTemplate) - fSheetPosX;
			float fSheetClipBoxTop	  = rExposure.GetSheetBleedBox(TOP,	   pObject, pObjectTemplate) - fSheetPosY;

			float fSheetExposureCenterX = rExposure.GetContentTrimBoxCenterX(pObject, pObjectTemplate) - fSheetPosX;
			float fSheetExposureCenterY = rExposure.GetContentTrimBoxCenterY(pObject, pObjectTemplate) - fSheetPosY;
			float fObjectWidth			= pObject->GetWidth();
			float fObjectHeight			= pObject->GetHeight();
			float fShinglingOffsetX, fShinglingOffsetY;
			switch (pObject->m_nHeadPosition)
			{
			case TOP:		fShinglingOffsetX =  pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, NULL, &fObjectWidth);	
							fShinglingOffsetY =  pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, NULL, &fObjectHeight);	
							break;
			case LEFT:		fShinglingOffsetX = -pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, NULL, &fObjectHeight);	
							fShinglingOffsetY =  pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, NULL, &fObjectWidth);	
							break;
			case BOTTOM:	fShinglingOffsetX = -pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, NULL, &fObjectWidth);	
							fShinglingOffsetY = -pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, NULL, &fObjectHeight);	
							break;
			case RIGHT:		fShinglingOffsetX =  pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, NULL, &fObjectHeight);	
							fShinglingOffsetY = -pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, NULL, &fObjectWidth);	
							break;
			}

			fSheetExposureCenterX += fShinglingOffsetX;	
			fSheetExposureCenterY += fShinglingOffsetY;	
			float fObjectTrimBoxLeft   = pObject->GetLeft()		+ fShinglingOffsetX;
			float fObjectTrimBoxBottom = pObject->GetBottom()	+ fShinglingOffsetY;
			float fObjectTrimBoxRight  = pObject->GetRight()	+ fShinglingOffsetX;
			float fObjectTrimBoxTop    = pObject->GetTop()		+ fShinglingOffsetY;
			float fObjectTrimBoxWidth  = pObject->GetRealWidth();
			float fObjectTrimBoxHeight = pObject->GetRealHeight();

			float fObjectBBoxCenterX = pObject->GetRealWidth()  / 2.0f;
			float fObjectBBoxCenterY = pObject->GetRealHeight() / 2.0f;
			if (pPageSource && pPageSourceHeader)
			{
				fObjectBBoxCenterX = pPageSource->GetBBox(LEFT,   pPageSourceHeader) + ( pPageSource->GetBBox(RIGHT, pPageSourceHeader) - pPageSource->GetBBox(LEFT,   pPageSourceHeader) ) / 2.0f;
				fObjectBBoxCenterY = pPageSource->GetBBox(BOTTOM, pPageSourceHeader) + ( pPageSource->GetBBox(TOP,   pPageSourceHeader) - pPageSource->GetBBox(BOTTOM, pPageSourceHeader) ) / 2.0f;
			}

			int nOrd = pDoc->m_PageTemplateList.GetIndex(pObjectTemplate);

			AddPage(surface, fSheetExposureCenterX, fSheetExposureCenterY, pObject->m_nHeadPosition, 
						     fObjectTrimBoxLeft, fObjectTrimBoxBottom, fObjectTrimBoxRight, fObjectTrimBoxTop, 
							 fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop,
							 fObjectBBoxCenterX, fObjectBBoxCenterY,
							 fObjectTrimBoxWidth, fObjectTrimBoxHeight,
							 pObjectTemplate->m_fContentRotation, pObjectTemplate->m_fContentScaleX, pObjectTemplate->m_fContentScaleY, (nRandomPageIndex == 0), nOrd);

			nRandomPageIndex--;
		}
	}

	return TRUE;
}


BOOL CJDFOutputMIS::AddPage(JDFSurface surface, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
												float fObjectTrimBoxLeft,	 float fObjectTrimBoxBottom, 
												float fObjectTrimBoxRight,	 float fObjectTrimBoxTop,
												float fSheetClipBoxLeft,	 float fSheetClipBoxBottom, 
												float fSheetClipBoxRight,	 float fSheetClipBoxTop,
												float fObjectBBoxCenterX,	 float fObjectBBoxCenterY,
												float fObjectTrimBoxWidth,	 float fObjectTrimBoxHeight,
												float fContentRotation,		 float fContentScaleX,	float fContentScaleY,
												BOOL bSkipIfDemo, int nOrd)
{
	// add content rotation and convert to angles between 0 and 270
	double fIntPart;
	double fAngle = fContentRotation;
	switch (nHeadPosition)
	{
	case LEFT:	fAngle = (float)modf(( 90.0f + fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case BOTTOM:fAngle = (float)modf((180.0f + fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case RIGHT: fAngle = (float)modf((270.0f + fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	}

	float fScaleX = (CPDFOutput::AngleEquals( fContentRotation, 0.0f ) || CPDFOutput::AngleEquals( fContentRotation, 180.0f )) ? fContentScaleX : fContentScaleY;
	float fScaleY = (CPDFOutput::AngleEquals( fContentRotation, 0.0f ) || CPDFOutput::AngleEquals( fContentRotation, 180.0f )) ? fContentScaleY : fContentScaleX;

	fObjectBBoxCenterX	  *= fScaleX;
	fObjectBBoxCenterY	  *= fScaleY;
	fObjectBBoxCenterX	  /= fPSPoint; fObjectBBoxCenterY	 /= fPSPoint;
	fSheetExposureCenterX /= fPSPoint; fSheetExposureCenterY /= fPSPoint;
	fSheetClipBoxLeft	  /= fPSPoint; fSheetClipBoxBottom	 /= fPSPoint;
	fSheetClipBoxRight	  /= fPSPoint; fSheetClipBoxTop		 /= fPSPoint;
	fObjectTrimBoxLeft	  /= fPSPoint; fObjectTrimBoxRight	 /= fPSPoint;
	fObjectTrimBoxBottom  /= fPSPoint; fObjectTrimBoxTop	 /= fPSPoint;
	fObjectTrimBoxWidth	  /= fPSPoint; fObjectTrimBoxHeight	 /= fPSPoint;

	float fOriginX, fOriginY;
	CPDFOutput::CalcPDFObjectOrigin(fOriginX, fOriginY, (float)fAngle, fSheetExposureCenterX, fSheetExposureCenterY, fObjectBBoxCenterX, fObjectBBoxCenterY);
	IPL_TDMATRIX ctm = CPDFOutput::CalcPDFObjectMatrix (fOriginX, fOriginY, (float)fAngle, fScaleX, fScaleY);

	float fSheetTrimBoxCenterX = ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? fObjectTrimBoxLeft   + fObjectTrimBoxWidth /2.0f : fObjectTrimBoxLeft   + fObjectTrimBoxHeight/2.0f;
	float fSheetTrimBoxCenterY = ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? fObjectTrimBoxBottom + fObjectTrimBoxHeight/2.0f : fObjectTrimBoxBottom + fObjectTrimBoxWidth /2.0f;
	CPDFOutput::CalcPDFObjectOrigin(fOriginX, fOriginY, (float)fAngle, fSheetTrimBoxCenterX, fSheetTrimBoxCenterY, fObjectTrimBoxWidth/2.0f, fObjectTrimBoxHeight/2.0f);
	IPL_TDMATRIX trimctm = CPDFOutput::CalcPDFObjectMatrix (fOriginX, fOriginY, (float)fAngle, fScaleX, fScaleY);

	CheckDonglePDF(FALSE, FALSE);

	CImpManDoc* pDoc			   = CImpManDoc::GetDoc();
	int			nDocsDongleStatus  = (pDoc) ? pDoc->m_nDongleStatus : CImpManApp::NoDongle;
	BOOL		bAddWatermarks     = TRUE;
	switch (theApp.m_nDongleStatus)
	{
	case CImpManApp::NoDongle:			
	case CImpManApp::ClientDongle:		bAddWatermarks = TRUE; break;

	case CImpManApp::SmallFormatDongle:	
	case CImpManApp::StandardDongle:	
	case CImpManApp::NewsDongle:	
	case CImpManApp::DigitalDongle:	
	case CImpManApp::AutoDongle:		if ( (theApp.m_nGUIStyle == CImpManApp::GUIStandard) && (theApp.m_nAddOnsLicensed & CImpManApp::JDFMIS) )
											bAddWatermarks = ( (nDocsDongleStatus == CImpManApp::NoDongle) || 
															   (nDocsDongleStatus == CImpManApp::ClientDongle) ) ? TRUE  : FALSE;
										else
											bAddWatermarks = TRUE;	
										break;

	default:							bAddWatermarks = TRUE; break;
	}

	if (bAddWatermarks)
		if (bSkipIfDemo)
		{
			fObjectTrimBoxWidth  /= 2.0f;
			fObjectTrimBoxHeight /= 2.0f;
			float fWidth  = fSheetClipBoxRight - fSheetClipBoxLeft;
			float fHeight = fSheetClipBoxTop   - fSheetClipBoxBottom;
			fSheetClipBoxLeft   += fWidth  / 4.0f;
			fSheetClipBoxRight  -= fWidth  / 4.0f;
			fSheetClipBoxBottom += fHeight / 4.0f;
			fSheetClipBoxTop	-= fHeight / 4.0f;
		}

	IPL_TDRECT sheetClipBox  = {fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop};
	IPL_TDRECT objectTrimBox = {0.0f, 0.0f, fObjectTrimBoxWidth, fObjectTrimBoxHeight};

	AddContentObject(surface, nOrd, ctm, trimctm, sheetClipBox, objectTrimBox);

	return TRUE;
}

BOOL CJDFOutputMIS::AddContentObject(JDFSurface surface, int nOrd, IPL_TDMATRIX& ctm, IPL_TDMATRIX& trimctm, IPL_TDRECT& sheetClipBox, IPL_TDRECT& objectTrimBox)
{
	JDFContentObject contentObject = surface.AppendContentObject();
	CString strCoords;
	strCoords.Format(_T("%.3f %.3f %.3f %.3f %.3f %.3f"), ctm.a, ctm.b, ctm.c, ctm.d, ctm.h, ctm.v);
	contentObject.SetCTM(JDFMatrix((WString)strCoords));
	contentObject.SetOrd(nOrd);
	contentObject.init();
	strCoords.Format(_T("%.3f %.3f %.3f %.3f"), sheetClipBox.fr.left, sheetClipBox.fr.bottom, sheetClipBox.fr.right, sheetClipBox.fr.top);
	contentObject.SetClipBox(JDFRectangle((WString)strCoords));

	strCoords.Format(_T("%.3f %.3f %.3f %.3f %.3f %.3f"), trimctm.a, trimctm.b, trimctm.c, trimctm.d, trimctm.h, trimctm.v);
	contentObject.SetAttribute(_T("TrimCTM"),  (WString)strCoords);
	contentObject.SetTrimCTM(JDFMatrix((WString)strCoords));
	strCoords.Format(_T("%.3f %.3f"), fabs(objectTrimBox.fr.right - objectTrimBox.fr.left), fabs(objectTrimBox.fr.top - objectTrimBox.fr.bottom));
	contentObject.SetAttribute(_T("TrimSize"), (WString)strCoords);
	contentObject.SetTrimSize(JDFXYPair((WString)strCoords));

	return TRUE;
}

BOOL CJDFOutputMIS::AddMarksRunList(JDFRunList runList, CString strName)
{
	runList.SetStatus(JDF::JDFResource::Status_Available);
	runList.RemoveAttributes((vWString)_T("AgentName AgentVersion"));

	CString strFilename;
	strFilename.Format(_T("Marks\\%s_Marks.pdf"), strName);

	JDFRunList run = runList.AddRun((KString)WindowsPath2URL(strFilename, FALSE), 0, m_nMarkOrd - 1);
	run.SetRun(_T("0"));
	JDFLayoutElement layoutElement = run.GetLayoutElement();
	JDFFileSpec		 fileSpec	   = layoutElement.GetFileSpec();
	fileSpec.SetMimeType(_T("application/pdf"));

	return TRUE;
}