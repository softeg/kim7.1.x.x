// DlgFlatProductData.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgFlatProductData.h"
#include "DlgSheet.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgPageFormats.h"
#include "DlgPaperDefs.h"
#include "DlgSelectJobColors.h"
#include "ProductsView.h"
#include "PrintSheetPlanningView.h"
#include "PageListTopToolsView.h"
#include "DlgProductionPunchingDetails.h"
#include "PrintComponentsView.h"
#include "ProductNavigationView.h"



// CFPDCustomPaintColorsFrame

IMPLEMENT_DYNAMIC(CFPDCustomPaintColorsFrame, CStatic)

CFPDCustomPaintColorsFrame::CFPDCustomPaintColorsFrame()
{
}

CFPDCustomPaintColorsFrame::~CFPDCustomPaintColorsFrame()
{
}


BEGIN_MESSAGE_MAP(CFPDCustomPaintColorsFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CFPDCustomPaintColorsFrame-Meldungshandler

void CFPDCustomPaintColorsFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rcFrame;
	GetClientRect(rcFrame);
	switch (GetDlgCtrlID())
	{
	case IDC_PD_COLORS_PREVIEWFRAME:	DrawColors(&dc, rcFrame); break;
	}
}

void CFPDCustomPaintColorsFrame::DrawColors(CDC* pDC, CRect rcFrame)
{
	CPen	pen(PS_SOLID, 1, MARINE);
	CBrush	brush(RGB(250,250,250));
	CPen*	pOldPen	  = pDC->GetCurrentPen();
	CBrush* pOldBrush = pDC->GetCurrentBrush();

	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	pDC->Rectangle(rcFrame);
	brush.DeleteObject();

	CDlgFlatProductData* pDlg = (CDlgFlatProductData*)GetParent();
	if ( ! pDlg)
		return;

	CFlatProduct& rProduct = pDlg->m_product;
	if ( ! rProduct.m_colorantList.GetSize())
		return;

	CString strText;
	int nXPos = rcFrame.left + 5;
	int nYPos = rcFrame.top	 + 2;
	int nXOffset = 45;

	CFont font;
	font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);

	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	pDC->SetBkMode(TRANSPARENT);
	CString strOut, strOut2;
	strOut2.LoadString(IDS_COLORED);
	strOut.Format(_T("%d-%s"), rProduct.m_colorantList.GetSize(), strOut2); 
	pDC->TextOut(nXPos, nYPos, strOut);
	pDC->SetTextColor(GUICOLOR_VALUE);

	nXPos += nXOffset;
	CRect rcIcon;
	rcIcon.left = nXPos; rcIcon.top = nYPos + 4; rcIcon.right = rcIcon.left + 9; rcIcon.bottom = nYPos + 11;
	for (int i = 0; i < rProduct.m_colorantList.GetSize(); i++)
	{
		brush.CreateSolidBrush(rProduct.m_colorantList[i].m_crRGB);
		pDC->SelectObject(&brush);
		pDC->Rectangle(rcIcon);
		brush.DeleteObject();

		rcIcon.OffsetRect(8, 0);
	}
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();
	font.DeleteObject();
}

// CFPDCustomPaintPagesFrame

IMPLEMENT_DYNAMIC(CFPDCustomPaintPagesFrame, CStatic)

CFPDCustomPaintPagesFrame::CFPDCustomPaintPagesFrame()
{
}

CFPDCustomPaintPagesFrame::~CFPDCustomPaintPagesFrame()
{
}


BEGIN_MESSAGE_MAP(CFPDCustomPaintPagesFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CFPDCustomPaintPagesFrame-Meldungshandler

void CFPDCustomPaintPagesFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CString strText;
	CRect rcFrame;
	GetClientRect(rcFrame);

	switch (GetDlgCtrlID())
	{
	case IDC_PD_PAGES_PREVIEWFRAME:		DrawPages(&dc,	rcFrame);	break;
	default:														break;
	}
}

void CFPDCustomPaintPagesFrame::DrawPages(CDC* pDC, CRect frameRect)
{
	CDlgFlatProductData* pDlg = (CDlgFlatProductData*)GetParent();
	if ( ! pDlg)
		return;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBrush brush(RGB(127,162,207));
	CPen   pen(PS_SOLID, 1, LIGHTGRAY);
	CBrush* pOldBrush = pDC->SelectObject(&brush);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);

	//pDC->SaveDC();	// save current clipping region
	//CRect rcClip;
	//pDlg->GetDlgItem(IDC_PD_PAGESPREVIEW_SPIN)->GetClientRect(rcClip);
	//rcClip.OffsetRect(1, 1);
	//pDC->ExcludeClipRect(rcClip);
	pDC->Rectangle(frameRect);
	//pDC->RestoreDC(-1);;

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	brush.DeleteObject();
	pen.DeleteObject();

	frameRect.DeflateRect(20, 20); frameRect.left += 10; frameRect.top += 10;
	POSITION pos = pDoc->m_PageTemplateList.FindIndex(pDlg->m_nFlatProductIndex * 2, pDoc->GetFlatProductsIndex());
	pDlg->m_product.DrawPreview(pDC, frameRect, (pos) ? &pDoc->m_PageTemplateList.GetAt(pos) : NULL, NULL, TRUE, TRUE);
}




// CDlgFlatProductData-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgFlatProductData, CDialog)

CDlgFlatProductData::CDlgFlatProductData(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgFlatProductData::IDD, pParent)
	, m_strName(_T(""))
	, m_strPaperInfo(_T(""))
	, m_nNumPagesComboIndex(0)
	, m_strComment(_T(""))
	, m_nNumDuplicate(0)
	, m_strPunchTool(_T(""))
{
	m_bInitFinished			= FALSE;
	m_ptInitialPos			= CPoint(0, 0);
	m_pPrintComponentsView	= NULL;

	m_pFlatProductList		= NULL;
	m_nFlatProductIndex		= 0;
	m_nPDPages				= 0;
	m_fPDHeight				= 0.0f;
	m_fPDWidth				= 0.0f;
	m_nPDPageOrientation	= -1;
	m_nPDQuantity			= 0;
	m_fTopTrim				= 0.0f;
	m_fBottomTrim			= 0.0f;
	m_fRightTrim			= 0.0f;
	m_fLeftTrim				= 0.0f;
	m_nPaperGrain			= 2; // GRAIN_EITHER;
	m_nGroup				= 0;
	m_nPriority				= 0;
	m_strPaper				= _T("");
	m_nPaperGrammage		= 0;
	m_fPaperVolume			= 0.0f;
	m_printDeadline			= COleDateTime::GetCurrentTime();
	m_boldFont.CreateFont(13, 0, 0, 0, FW_BOLD,	 FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CDlgFlatProductData::~CDlgFlatProductData()
{
	m_boldFont.DeleteObject();
}

void CDlgFlatProductData::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PD_NAME_EDIT, m_strName);
	DDX_Control(pDX, IDC_PD_FORMAT_COMBO, m_PDFormatCombo);
	DDX_Control(pDX, IDC_PD_WIDTH_SPIN, m_PDWidthSpin);
	DDX_Control(pDX, IDC_PD_HEIGHT_SPIN, m_PDHeightSpin);
	DDX_Measure(pDX, IDC_PD_HEIGHT, m_fPDHeight);
	DDX_Measure(pDX, IDC_PD_WIDTH, m_fPDWidth);
	DDX_Radio(pDX, IDC_PD_PORTRAIT, m_nPDPageOrientation);							   
	DDX_Text(pDX, IDC_PD_QUANTITY, m_nPDQuantity);
	DDX_Control(pDX, IDC_PD_QUANTITY_SPIN, m_PDQuantitySpin);
	DDX_Control(pDX, IDC_PD_QUANTITY1_SPIN, m_PDQuantity1Spin);
	DDX_Control(pDX, IDC_PD_QUANTITY2_SPIN, m_PDQuantity2Spin);
	DDX_Control(pDX, IDC_PD_RIGHTTRIM_SPIN, m_rightTrimSpin);
	DDX_Control(pDX, IDC_PD_LEFTTRIM_SPIN, m_leftTrimSpin);
	DDX_Control(pDX, IDC_PD_TOPTRIM_SPIN, m_topTrimSpin);
	DDX_Control(pDX, IDC_PD_BOTTOMTRIM_SPIN, m_bottomTrimSpin);
	DDX_Control(pDX, IDC_PD_GROUP_SPIN, m_groupSpin);
	DDX_Control(pDX, IDC_PD_PRIORITY_SPIN, m_prioritySpin);
	DDX_Measure(pDX, IDC_PD_BOTTOMTRIM_EDIT, m_fBottomTrim);
	DDX_Measure(pDX, IDC_PD_TOPTRIM_EDIT, m_fTopTrim);
	DDX_Measure(pDX, IDC_PD_RIGHTTRIM_EDIT, m_fRightTrim);
	DDX_Measure(pDX, IDC_PD_LEFTTRIM_EDIT, m_fLeftTrim);
	DDX_Control(pDX, IDC_PD_PAPER_COMBO, m_paperCombo);
	DDX_CBIndex(pDX, IDC_PD_PAPERGRAIN_COMBO, m_nPaperGrain);
	DDX_Text(pDX, IDC_PD_GROUP_EDIT, m_nGroup);
	DDX_Text(pDX, IDC_PD_PRIORITY_EDIT, m_nPriority);
	m_paperDefsSelection.DDX_PaperDefsCombo(pDX, IDC_PD_PAPER_COMBO, m_strPaper);
	DDX_Text(pDX, IDC_SHINGLING_PAPER_STATIC, m_strPaperInfo);
	DDX_CBIndex(pDX, IDC_PD_NUMPAGES_COMBO, m_nNumPagesComboIndex);
	DDX_Text(pDX, IDC_PD_COMMENT_EDIT, m_strComment);
	m_PDPageFormatSelection.DDX_PageFormatCombo(pDX, IDC_PD_FORMAT_COMBO, m_strPDFormatName);
	DDX_DateTimeCtrl(pDX, IDC_OD_DATETIMEPICKER, m_printDeadline);
	DDX_Text(pDX, IDC_PD_NDUPLICATE_EDIT, m_nNumDuplicate);
	DDX_Control(pDX, IDC_PD_NDUPLICATE_SPIN, m_duplicateSpin);
	DDX_Text(pDX, IDC_FLATPRODUCT_PUNCHTOOL_STATIC, m_strPunchTool);
}


BEGIN_MESSAGE_MAP(CDlgFlatProductData, CDialog)
	ON_BN_CLICKED(IDC_PD_MODIFY_FORMAT, OnBnClickedPdModifyFormat)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_HEIGHT_SPIN, OnDeltaposPDHeightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_WIDTH_SPIN, OnDeltaposPDWidthSpin)
	ON_EN_KILLFOCUS(IDC_PD_HEIGHT, OnEnKillfocusPdHeight)
	ON_EN_KILLFOCUS(IDC_PD_WIDTH,  OnEnKillfocusPdWidth)
	ON_BN_CLICKED(IDC_PD_LANDSCAPE, OnPDLandscape)
	ON_BN_CLICKED(IDC_PD_PORTRAIT, OnPDPortrait)
	ON_CBN_SELCHANGE(IDC_PD_FORMAT_COMBO, OnSelchangePDFormat)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_QUANTITY_SPIN, OnDeltaposPDQuantitySpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_QUANTITY1_SPIN, OnDeltaposPDQuantity1Spin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_QUANTITY2_SPIN, OnDeltaposPDQuantity2Spin)
	ON_CBN_SELCHANGE(IDC_PD_PAPER_COMBO, OnCbnSelchangePdPaperCombo)
	ON_BN_CLICKED(IDC_PD_PAPER_BUTTON, OnBnClickedPdPaperButton)
	ON_BN_CLICKED(IDC_PD_COLORS_BUTTON, OnBnClickedPdColorsButton)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_BOTTOMTRIM_SPIN, OnDeltaposBottomtrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_TOPTRIM_SPIN, OnDeltaposToptrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_RIGHTTRIM_SPIN, OnDeltaposRighttrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_LEFTTRIM_SPIN, OnDeltaposLefttrimSpin)
	ON_EN_KILLFOCUS(IDC_PD_BOTTOMTRIM_EDIT, OnKillfocusBottomtrimEdit)
	ON_EN_KILLFOCUS(IDC_PD_TOPTRIM_EDIT, OnKillfocusToptrimEdit)
	ON_EN_KILLFOCUS(IDC_PD_RIGHTTRIM_EDIT, OnKillfocusRighttrimEdit)
	ON_EN_KILLFOCUS(IDC_PD_LEFTTRIM_EDIT, OnKillfocusLefttrimEdit)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_PD_PREVIOUS_BUTTON, OnBnClickedPdPreviousButton)
	ON_BN_CLICKED(IDC_PD_NEXT_BUTTON, OnBnClickedPdNextButton)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_PD_NEWPRODUCT_BUTTON, OnBnClickedPdNewproductButton)
	ON_BN_CLICKED(IDC_PD_DUPLICATE_PRODUCT_BUTTON, &CDlgFlatProductData::OnBnClickedPdDuplicateProductButton)
	ON_EN_CHANGE(IDC_PD_NDUPLICATE_EDIT, &CDlgFlatProductData::OnEnChangePdNduplicateEdit)
	ON_BN_CLICKED(IDC_FLATPRODUCT_SELECT_PUNCHTOOL, &CDlgFlatProductData::OnBnClickedFlatproductSelectPunchtool)
	ON_BN_CLICKED(IDC_FLATPRODUCT_PUNCHTOOL_REMOVE, &CDlgFlatProductData::OnBnClickedFlatproductPunchtoolRemove)
END_MESSAGE_MAP()


// CDlgFlatProductData-Meldungshandler

BOOL CDlgFlatProductData::OnInitDialog()
{
	CDialog::OnInitDialog();

	if ( ! m_pFlatProductList)
	{
		//EndDialog(IDCANCEL);
		//return FALSE;
		GetDlgItem(IDC_PD_NAME_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_PD_NEWPRODUCT_BUTTON)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PD_NDUPLICATE_EDIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PD_NDUPLICATE_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PD_DUPLICATE_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PD_DUPLICATE_PRODUCT_BUTTON)->ShowWindow(SW_HIDE);
	}

	m_colorsPreviewFrame.SubclassDlgItem(IDC_PD_COLORS_PREVIEWFRAME, this);
	m_pagesPreviewFrame.SubclassDlgItem	(IDC_PD_PAGES_PREVIEWFRAME, this);

	m_prevProductButton.SubclassDlgItem	(IDC_PD_PREVIOUS_BUTTON, this);
	m_prevProductButton.SetBitmap		(IDB_ARROW_UP, IDB_ARROW_UP, IDB_ARROW_UP_DISABLED, XCENTER);
	m_nextProductButton.SubclassDlgItem(IDC_PD_NEXT_BUTTON, this);
	m_nextProductButton.SetBitmap		(IDB_ARROW_DOWN, IDB_ARROW_DOWN, IDB_ARROW_DOWN_DISABLED, XCENTER);

	m_PDWidthSpin.SetRange(0, 1);
	m_PDHeightSpin.SetRange(0, 1);
	m_topTrimSpin.SetRange(0, 1);  
	m_rightTrimSpin.SetRange(0, 1);  
	m_leftTrimSpin.SetRange(0, 1);  
	m_bottomTrimSpin.SetRange(0, 1);  
	m_groupSpin.SetRange(0, 9);  
	m_prioritySpin.SetRange(0, 9);  
	m_duplicateSpin.SetRange(0, 100);  
	m_PDQuantitySpin.SetRange(0, 1);  
	m_PDQuantity1Spin.SetRange(0, 1);  
	m_PDQuantity2Spin.SetRange(0, 1);  
	((CSpinButtonCtrl*)GetDlgItem(IDC_PD_PAGESPREVIEW_SPIN))->SetRange(0, 1);

	InitData();

	m_bInitFinished = TRUE;
	OnEnChangePdNduplicateEdit();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgFlatProductData::InitData()
{
	if (m_pFlatProductList)
	{
		if (m_nFlatProductIndex >= m_pFlatProductList->GetSize())
			return;

		if (m_nFlatProductIndex == -1)	// new product to initialize
		{
			//m_product = CFlatProduct();
			if ( ! m_product.m_strProductName.IsEmpty())
				m_strName.Format(_T("%s-%d"), m_product.m_strProductName, m_pFlatProductList->GetSize() + 1);
			else
			{
				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if (pDoc)
					m_strName = pDoc->AssignDefaultProductName();
			}
			m_product.m_strProductName = m_strName; 
			m_product.m_strComment = _T("");
			GetDlgItem(IDC_PD_NAME_EDIT)->SetWindowText(m_strName);
			GetDlgItem(IDC_PD_NAME_EDIT)->SetFocus();
			((CEdit*)GetDlgItem(IDC_PD_NAME_EDIT))->SetSel(0, -1);
		}
		else
			m_product = m_pFlatProductList->FindByIndex(m_nFlatProductIndex);
	}
	else
	{
		if (m_product.IsNull() && m_pPrintComponentsView)
		{
			CPrintComponent component = m_pPrintComponentsView->GetActComponent();
			if (component.m_nType == CPrintComponent::Flat)
				if (component.GetFlatProduct())
				{
					m_product = *component.GetFlatProduct();
					m_nFlatProductIndex = component.m_nFlatProductIndex;
				}
		}
	}

	m_fOldWidth  = m_product.m_fTrimSizeWidth;
	m_fOldHeight = m_product.m_fTrimSizeHeight;

	SetWindowText(m_product.m_strProductName);
	m_strName	 = m_product.m_strProductName;
	m_strComment = m_product.m_strComment;

	m_strPDFormatName	 = m_product.m_strFormatName;
	m_fPDWidth			 = m_product.m_fTrimSizeWidth;
	m_fPDHeight			 = m_product.m_fTrimSizeHeight;
	m_nPDPageOrientation = m_product.m_nPageOrientation;
	m_PDPageFormatSelection.InitComboBox(this, m_strPDFormatName, &m_fPDWidth, &m_fPDHeight, IDC_PD_FORMAT_COMBO);
	if (((CComboBox*)GetDlgItem(IDC_PD_FORMAT_COMBO))->FindString(-1, m_strPDFormatName) < 0)
	{
		m_strPDFormatName.LoadString(IDS_USERDEFINED);
	}

	CString strText;
	if (m_nPDQuantity)
		strText.Format(_T("%d"), m_nPDQuantity);
	GetDlgItem(IDC_PD_QUANTITY)->SetWindowText(strText);

	m_strPaper			= m_product.m_strPaper;
	m_nPaperGrammage	= m_product.m_nPaperGrammage;
	m_fPaperVolume		= m_product.m_fPaperVolume;
	m_paperDefsSelection.InitComboBox(this, m_strPaper, &m_nPaperGrammage, &m_fPaperVolume, NULL, IDC_PD_PAPER_COMBO);
	if (((CComboBox*)GetDlgItem(IDC_PD_PAPER_COMBO))->FindString(0, m_strPaper) < 0)
	{
		((CComboBox*)GetDlgItem(IDC_PD_PAPER_COMBO))->AddString(m_strPaper);
		((CComboBox*)GetDlgItem(IDC_PD_PAPER_COMBO))->SelectString(0, m_strPaper);
	}
	m_strPaperInfo = _T(" ") + InitPaperInfo();

	m_fTopTrim	  = m_product.m_fTopTrim;
	m_fRightTrim  = m_product.m_fRightTrim;
	m_fLeftTrim	  = m_product.m_fLeftTrim;
	m_fBottomTrim = m_product.m_fBottomTrim;
	switch (m_product.m_nPaperGrain)
	{
	case GRAIN_HORIZONTAL:		m_nPaperGrain = 0;	break;
	case GRAIN_VERTICAL:		m_nPaperGrain = 1;	break;
	case GRAIN_EITHER:			m_nPaperGrain = 2;	break;
	}
	m_nGroup		= m_product.m_nGroup;
	m_strPunchTool	= m_product.m_strPunchTool;
	m_nPriority		= m_product.m_nPriority;
	m_nPDQuantity	= m_product.m_nDesiredQuantity;

	m_nNumPagesComboIndex = m_product.m_nNumPages - 1;

	m_printDeadline = m_product.m_printDeadline;

	m_colorsPreviewFrame.Invalidate(); m_colorsPreviewFrame.UpdateWindow(); 
	m_pagesPreviewFrame.Invalidate();  m_pagesPreviewFrame.UpdateWindow();

	UpdateData(FALSE);

	GetDlgItem(IDC_PD_PREVIOUS_BUTTON)->ShowWindow((m_pPrintComponentsView) ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_PD_NEXT_BUTTON	 )->ShowWindow((m_pPrintComponentsView) ? SW_HIDE : SW_SHOW);

	if (m_pFlatProductList)
	{
		CProductsView* pView = (CProductsView*)theApp.GetView(RUNTIME_CLASS(CProductsView));
		if (pView)
		{
			CDisplayItem* pDICur  = pView->m_DisplayList.GetItemFromData((void*)m_nFlatProductIndex, NULL, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
			CDisplayItem* pDIPrev = pView->m_DisplayList.GetPrevItem(pDICur, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
			CDisplayItem* pDINext = pView->m_DisplayList.GetNextItem(pDICur, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
			GetDlgItem(IDC_PD_PREVIOUS_BUTTON)->EnableWindow((pDIPrev) ? TRUE : FALSE);
			GetDlgItem(IDC_PD_NEXT_BUTTON	 )->EnableWindow((pDINext) ? TRUE : FALSE);

			pView->m_DisplayList.DeselectAll();
			CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData((void*)m_nFlatProductIndex, NULL, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
			if (pDI)
			{
				pView->m_DisplayList.SelectItem(*pDI);
				pView->Invalidate();
				pView->UpdateWindow();
			}
		}
	}
}

void CDlgFlatProductData::SaveData()
{
	UpdateData(TRUE);

	if (m_pFlatProductList)
		if (m_strName.IsEmpty())
			return;

	m_product.m_strProductName	 = m_strName;
	m_product.m_strComment		 = m_strComment;

	m_product.m_strFormatName	 = m_strPDFormatName;
	m_product.m_fTrimSizeWidth	 = m_fPDWidth;
	m_product.m_fTrimSizeHeight	 = m_fPDHeight;
	m_product.m_nPageOrientation = m_nPDPageOrientation;

	m_product.m_strPaper		 = m_strPaper;
	m_product.m_nPaperGrammage	 = m_nPaperGrammage;
	m_product.m_fPaperVolume	 = m_fPaperVolume;

	m_product.m_fTopTrim		 = m_fTopTrim;
	m_product.m_fRightTrim		 = m_fRightTrim;
	m_product.m_fLeftTrim		 = m_fLeftTrim;
	m_product.m_fBottomTrim		 = m_fBottomTrim;
	switch (m_nPaperGrain)
	{
	case 0:		m_product.m_nPaperGrain = GRAIN_HORIZONTAL;		break;
	case 1:		m_product.m_nPaperGrain = GRAIN_VERTICAL;		break;
	case 2:		m_product.m_nPaperGrain = GRAIN_EITHER;	break;
	}
	m_product.m_nGroup			 = m_nGroup;
	m_product.m_strPunchTool	 = m_strPunchTool;
	m_product.m_nPriority		 = m_nPriority;
	m_product.m_nDesiredQuantity = m_nPDQuantity;

	m_product.m_nNumPages		 = m_nNumPagesComboIndex + 1;

	m_product.m_printDeadline	 = m_printDeadline;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (m_pFlatProductList)
	{
		if (pDoc)
		{
			BOOL bRedraw = FALSE;
			if (m_nFlatProductIndex == -1)	// new product
			{
				m_pFlatProductList->AddTail(m_product);
				m_nFlatProductIndex = m_pFlatProductList->GetSize() - 1;

				pDoc->m_PageTemplateList.SynchronizeToFlatProductList(*m_pFlatProductList, pDoc->GetFlatProductsIndex());
				pDoc->m_flatProducts.CheckLayoutObjectFormats();
				bRedraw = TRUE;
			}
			else
			{
				if (m_nFlatProductIndex < m_pFlatProductList->GetSize())
				{
					CFlatProduct& rFlatProduct = m_pFlatProductList->FindByIndex(m_nFlatProductIndex);
					BOOL bSingleDoubleSidedChanged = (rFlatProduct.m_nNumPages != m_product.m_nNumPages) ? TRUE : FALSE;
					rFlatProduct = m_product;
					if (bSingleDoubleSidedChanged)
						CheckSingleDoubleSideExposures(m_nFlatProductIndex);
				}

				if ( (m_fOldWidth != m_product.m_fTrimSizeWidth) || (m_fOldHeight != m_product.m_fTrimSizeHeight) )
					pDoc->m_flatProducts.CheckLayoutObjectFormats();
			}

			pDoc->SetModifiedFlag();

			if (bRedraw)
			{
				theApp.OnUpdateView(RUNTIME_CLASS(CProductsView), NULL, 0, NULL, TRUE);
				theApp.OnUpdateView(RUNTIME_CLASS(CProductNavigationView), NULL, 0, NULL, TRUE);
				theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), NULL, 0, NULL, TRUE);
				theApp.OnUpdateView(RUNTIME_CLASS(CPageListTopToolsView), NULL, 0, NULL, TRUE);
			}
		}
	}
	else
	{
		if (m_pPrintComponentsView)
		{
			CPrintComponent component = m_pPrintComponentsView->GetActComponent();
			if (component.m_nType == CPrintComponent::Flat)
			{
				CFlatProduct* pFlatProduct = component.GetFlatProduct();
				if (pFlatProduct)
				{
					BOOL bSingleDoubleSidedChanged = (pFlatProduct->m_nNumPages != m_product.m_nNumPages) ? TRUE : FALSE;
					*pFlatProduct = m_product;
					if (bSingleDoubleSidedChanged)
						CheckSingleDoubleSideExposures(m_nFlatProductIndex);
				}
			}
		}
		if (pDoc)
		{
			if ( (m_fOldWidth != m_product.m_fTrimSizeWidth) || (m_fOldHeight != m_product.m_fTrimSizeHeight) )
				pDoc->m_flatProducts.CheckLayoutObjectFormats();
		}
	}
}

void CDlgFlatProductData::CheckSingleDoubleSideExposures(int nFlatProductIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_PageTemplateList.FindIndex(nFlatProductIndex * 2 + 1, pDoc->GetFlatProductsIndex());	// * 2 + 1: take backside template
	if (pos)
	{
		CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetAt(pos);
		for (int i = 0; i < rTemplate.m_PrintSheetLocations.GetSize(); i++)
		{
			CLayoutObject* pObject	   = rTemplate.m_PrintSheetLocations[i].GetLayoutObject();
			if ( ! pObject)
				continue;
			CPrintSheet*   pPrintSheet = rTemplate.m_PrintSheetLocations[i].GetPrintSheet();
			if ( ! pPrintSheet)
				continue;
			pDoc->m_PageTemplateList.CheckForExposuresToAdd(pPrintSheet, pObject, NULL, rTemplate, i, FALSE, pDoc, -1);
			pDoc->m_PageTemplateList.CheckForExposuresToRemove(rTemplate, i, pPrintSheet, pObject->GetLayoutSideIndex());
		}
	}
}

void CDlgFlatProductData::OnCbnSelchangePdPaperCombo()
{
	CString strOldPaper = m_strPaper;
	UpdateData(TRUE);
	m_paperDefsSelection.SelchangeFormatList(this, &m_nPaperGrammage, &m_fPaperVolume, NULL, m_strPaper, IDC_PD_PAPER_COMBO);
	if (m_strPaper == strOldPaper)
		return;

	m_strPaperInfo = _T(" ") + InitPaperInfo();

	UpdateData(FALSE);
}

CString CDlgFlatProductData::InitPaperInfo()
{
	if (this == NULL)
		return _T("");

	CString strPaperInfo;
	strPaperInfo.LoadString(IDS_NO_PAPER);

	CString strVolume;
	CPrintSheetView::TruncateNumber(strVolume, m_fPaperVolume);

	CString strPaperVolume, strPaperWeight;
	strPaperVolume.LoadString(IDS_PAPER_VOLUME);
	if ( (m_nPaperGrammage > 0) && (m_fPaperVolume > 0.0f) )
		strPaperInfo.Format(_T("%d g/m� | %s: %s"), m_nPaperGrammage, strPaperVolume, strVolume);
	else
		if (m_nPaperGrammage > 0)
			strPaperInfo.Format(_T("%d g/m�"), m_nPaperGrammage);
		else
			if (m_fPaperVolume > 0.0f)
				strPaperInfo.Format(_T("%s: %s"), strPaperVolume, strVolume);
			else
				if ( ! m_strPaper.IsEmpty())
					strPaperInfo = m_strPaper;

	return strPaperInfo;
}

void CDlgFlatProductData::OnBnClickedPdPaperButton()
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgPaperDefs dlg;
	dlg.m_strPaperName = m_strPaper;
	if (dlg.DoModal() == IDCANCEL)
		return;

	m_strPaper			= dlg.m_strPaperName;
	m_fPaperVolume		= dlg.m_fPaperVolume;
	m_nPaperGrammage	= dlg.m_nPaperGrammage;

	m_strPaperInfo = _T(" ") + InitPaperInfo();

	UpdateData(FALSE);

	m_paperDefsSelection.InitComboBox(this, m_strPaper, &m_nPaperGrammage, &m_fPaperVolume, NULL, IDC_PD_PAPER_COMBO);

	UpdateData(FALSE);
}

void CDlgFlatProductData::OnBnClickedPdColorsButton()
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgSelectJobColors dlg;
	dlg.m_colorantList.Append(m_product.m_colorantList);

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_product.m_colorantList.RemoveAll();
	m_product.m_colorantList.Append(dlg.m_colorantList);

	m_colorsPreviewFrame.Invalidate(); m_colorsPreviewFrame.UpdateWindow(); 
}

void CDlgFlatProductData::OnBnClickedPdModifyFormat()
{
	UpdateData(TRUE);

	CDlgPageFormats dlg;
	dlg.m_strFormatName = m_strPDFormatName;
	dlg.m_fWidth		= m_fPDWidth;
	dlg.m_fHeight		= m_fPDHeight;
	dlg.DoModal();

	m_strPDFormatName = dlg.m_strFormatName;
	m_PDPageFormatSelection.InitComboBox(this, m_strPDFormatName, &m_fPDWidth, &m_fPDHeight, IDC_PD_FORMAT_COMBO);
	OnSelchangePDFormat();
}

void CDlgFlatProductData::OnDeltaposPDHeightSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_PDHeightSpin.GetBuddy(), pNMUpDown->iDelta);

	UpdateData(TRUE);
	m_strPDFormatName.LoadString(IDS_USERDEFINED);
	UpdateData(FALSE);
	m_product.m_fTrimSizeHeight = m_fPDHeight;
	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgFlatProductData::OnDeltaposPDWidthSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_PDWidthSpin.GetBuddy(), pNMUpDown->iDelta);

	UpdateData(TRUE);
	m_strPDFormatName.LoadString(IDS_USERDEFINED);
	UpdateData(FALSE);
	m_product.m_fTrimSizeWidth  = m_fPDWidth;
	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgFlatProductData::OnEnKillfocusPdHeight()
{
	float fOldValue = m_fPDHeight;
	UpdateData(TRUE);
	if (fOldValue != m_fPDHeight)
	{
		m_strPDFormatName.LoadString(IDS_USERDEFINED);
		GetDlgItem(IDC_PD_FORMAT_COMBO)->SetWindowText(m_strPDFormatName);
		m_product.m_fTrimSizeHeight = m_fPDHeight;
		m_pagesPreviewFrame.Invalidate();
		m_pagesPreviewFrame.UpdateWindow();
	}
}

void CDlgFlatProductData::OnEnKillfocusPdWidth()
{
	float fOldValue = m_fPDWidth;
	UpdateData(TRUE);
	if (fOldValue != m_fPDWidth)
	{
		m_strPDFormatName.LoadString(IDS_USERDEFINED);
		GetDlgItem(IDC_PD_FORMAT_COMBO)->SetWindowText(m_strPDFormatName);
		m_product.m_fTrimSizeWidth  = m_fPDWidth;
		m_pagesPreviewFrame.Invalidate();
		m_pagesPreviewFrame.UpdateWindow();
	}
}

void CDlgFlatProductData::OnPDPortrait() 
{
	if (m_nPDPageOrientation == PORTRAIT)	// already portrait
		return;

	m_nPDPageOrientation = m_product.m_nPageOrientation = PORTRAIT;		// switch to portrait

	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_PD_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_PD_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);

	UpdateData(TRUE);

	m_product.m_fTrimSizeWidth  = m_fPDWidth;
	m_product.m_fTrimSizeHeight = m_fPDHeight;
	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgFlatProductData::OnPDLandscape() 
{
	if (m_nPDPageOrientation == LANDSCAPE)// already landscape
		return;
	
	m_nPDPageOrientation = m_product.m_nPageOrientation = LANDSCAPE;		// switch to portrait

	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_PD_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_PD_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);

	UpdateData(TRUE);

	m_product.m_fTrimSizeWidth  = m_fPDWidth;
	m_product.m_fTrimSizeHeight = m_fPDHeight;
	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgFlatProductData::OnSelchangePDFormat() 
{
	UpdateData(TRUE);

	m_PDPageFormatSelection.SelchangeFormatList(this, &m_fPDWidth, &m_fPDHeight, m_strPDFormatName, &m_nPDPageOrientation, IDC_PD_FORMAT_COMBO);

	m_product.m_fTrimSizeWidth  = m_fPDWidth;
	m_product.m_fTrimSizeHeight = m_fPDHeight;
	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgFlatProductData::OnDeltaposToptrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_topTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusToptrimEdit();
	*pResult = 0;
}

void CDlgFlatProductData::OnDeltaposBottomtrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_bottomTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusBottomtrimEdit();
	*pResult = 0;
}

void CDlgFlatProductData::OnDeltaposRighttrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_rightTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusRighttrimEdit();
	*pResult = 0;
}

void CDlgFlatProductData::OnDeltaposLefttrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_leftTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusLefttrimEdit();
	*pResult = 0;
}

void CDlgFlatProductData::OnKillfocusToptrimEdit() 
{
	UpdateData(TRUE);
	m_product.m_fTopTrim = m_fTopTrim;
	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgFlatProductData::OnKillfocusBottomtrimEdit() 
{
	UpdateData(TRUE);
	m_product.m_fBottomTrim = m_fBottomTrim;
	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgFlatProductData::OnKillfocusRighttrimEdit() 
{
	UpdateData(TRUE);
	m_product.m_fRightTrim = m_fRightTrim;
	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgFlatProductData::OnKillfocusLefttrimEdit() 
{
	UpdateData(TRUE);
	m_product.m_fLeftTrim = m_fLeftTrim;
	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgFlatProductData::OnDeltaposPDQuantitySpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	m_nPDQuantity += pNMUpDown->iDelta * 10000;
	if (m_nPDQuantity < 0)
		m_nPDQuantity = 0;
	CString strText;
	if (m_nPDQuantity)
		strText.Format(_T("%d"), m_nPDQuantity);
	GetDlgItem(IDC_PD_QUANTITY)->SetWindowText(strText);
}

void CDlgFlatProductData::OnDeltaposPDQuantity1Spin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	m_nPDQuantity += pNMUpDown->iDelta * 1000;
	if (m_nPDQuantity < 0)
		m_nPDQuantity = 0;
	CString strText;
	if (m_nPDQuantity)
		strText.Format(_T("%d"), m_nPDQuantity);
	GetDlgItem(IDC_PD_QUANTITY)->SetWindowText(strText);
}

void CDlgFlatProductData::OnDeltaposPDQuantity2Spin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	m_nPDQuantity += pNMUpDown->iDelta * 100;
	if (m_nPDQuantity < 0)
		m_nPDQuantity = 0;
	CString strText;
	if (m_nPDQuantity)
		strText.Format(_T("%d"), m_nPDQuantity);
	GetDlgItem(IDC_PD_QUANTITY)->SetWindowText(strText);
}

void CDlgFlatProductData::OnBnClickedPdPreviousButton()
{
	CProductsView* pView = (CProductsView*)theApp.GetView(RUNTIME_CLASS(CProductsView));
	if ( ! pView)
		return;
	CDisplayItem* pDICur  = pView->m_DisplayList.GetItemFromData((void*)m_nFlatProductIndex, NULL, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
	CDisplayItem* pDIPrev = pView->m_DisplayList.GetPrevItem(pDICur, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
	if ( ! pDIPrev)
		return;

	SaveData();
	m_nFlatProductIndex = (int)pDIPrev->m_pData;
	InitData();
}

void CDlgFlatProductData::OnBnClickedPdNextButton()
{
	CProductsView* pView = (CProductsView*)theApp.GetView(RUNTIME_CLASS(CProductsView));
	if ( ! pView)
		return;
	CDisplayItem* pDICur  = pView->m_DisplayList.GetItemFromData((void*)m_nFlatProductIndex, NULL, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
	CDisplayItem* pDINext = pView->m_DisplayList.GetNextItem(pDICur, DISPLAY_ITEM_TYPEID(CProductsView, FlatProduct));
	if ( ! pDINext)
		return;

	SaveData();
	m_nFlatProductIndex = (int)pDINext->m_pData;
	InitData();
}

void CDlgFlatProductData::OnBnClickedPdNewproductButton()
{
	SaveData();

	m_nFlatProductIndex = -1;
	InitData();
}

void CDlgFlatProductData::OnEnChangePdNduplicateEdit()
{
	if ( ! m_bInitFinished)
		return;

	UpdateData(TRUE);

	GetDlgItem(IDC_PD_DUPLICATE_PRODUCT_BUTTON)->EnableWindow((m_nNumDuplicate <= 0) ? FALSE : TRUE);
}

void CDlgFlatProductData::OnBnClickedPdDuplicateProductButton()
{
	if ( ! m_pFlatProductList)
		return;
	
	UpdateData(TRUE);

	if (m_nNumDuplicate > 0)
	{
		CString strNumDuplicate; strNumDuplicate.Format(_T("%d"), m_nNumDuplicate);
		CString strOut; strOut.Format(IDS_DUPLICATEPRODUCT_MESSAGE, strNumDuplicate);
		if (AfxMessageBox(strOut, MB_YESNO) == IDNO)
			return;

		SaveData();

		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			CString strName = m_strName;
			for (int i = 0; i < m_nNumDuplicate; i++)
			{
				m_product.m_strProductName.Format(_T("%s-%d"), strName, i + 1);
				m_pFlatProductList->AddTail(m_product);
				m_nFlatProductIndex = m_pFlatProductList->GetSize() - 1;
			}

			pDoc->m_PageTemplateList.SynchronizeToFlatProductList(*m_pFlatProductList, pDoc->GetFlatProductsIndex());
			pDoc->m_flatProducts.CheckLayoutObjectFormats();

			pDoc->SetModifiedFlag();

			theApp.UpdateView(RUNTIME_CLASS(CProductsView), TRUE);
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), TRUE);

			InitData();
		}
	}
}

void CDlgFlatProductData::OnBnClickedOk()
{
	SaveData();

	OnOK();
}

HBRUSH CDlgFlatProductData::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

    if(nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_PD_PRODUCTNAME_STATIC:
					pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
					pDC->SelectObject(&m_boldFont);
					break;

		default:
				//pDC->SetBkMode(TRANSPARENT);
				//pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				break;
		}
    }

	return hbr;
}

BOOL CDlgFlatProductData::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
    {
		if(pMsg->wParam == VK_RETURN)
			if (GetFocus() == GetDlgItem(IDC_PD_NEWPRODUCT_BUTTON))
			{
				UINT nOldStyle = ((CButton*)GetDlgItem(IDC_PD_NEWPRODUCT_BUTTON))->GetButtonStyle();
				((CButton*)GetDlgItem(IDC_PD_NEWPRODUCT_BUTTON))->SetButtonStyle(nOldStyle & ~BS_DEFPUSHBUTTON);
				OnBnClickedPdNewproductButton();
				return TRUE;
			}
			else
				pMsg->wParam = VK_TAB;
    }
	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgFlatProductData::OnBnClickedFlatproductSelectPunchtool()
{
	CDlgProductionPunchingDetails dlg;

	dlg.m_strPunchToolName	= m_strPunchTool;
	dlg.m_flatProduct		= m_product;
	dlg.m_nFlatProductIndex = m_nFlatProductIndex;

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_product.m_strPunchTool = m_strPunchTool = dlg.m_strPunchToolName;
	
	UpdateData(FALSE);
}

void CDlgFlatProductData::OnBnClickedFlatproductPunchtoolRemove()
{
	m_product.m_strPunchTool = m_strPunchTool = _T("");
	UpdateData(FALSE);
}
