#pragma once

#include "ExtendedListCtrl.h"
#include "DlgSelectPressDevicePopup.h"
#include "DlgSelectSheetPopup.h"


// CBoundComponentsListCtrl

class CUnboundComponentsListCtrl : public CExtendedListCtrl
{
	DECLARE_DYNAMIC(CUnboundComponentsListCtrl)

public:
	CUnboundComponentsListCtrl();
	virtual ~CUnboundComponentsListCtrl();


public:
	enum EHighlight {HIGHLIGHT_NORMAL, HIGHLIGHT_ALLCOLUMNS, HIGHLIGHT_ROW};
	enum ColumnType {ProductPart, SheetRange, PrintSheetLayout};

	CBrush			m_bkBrush;
	int				m_nPrevSel;
	BOOL			m_bMagnified;


protected:
	int m_nHighlight;		// Indicate type of selection highlighting

public:
	void Initialize();
	void InitInplaceControl(int iItem, int iSubItem, int nCtrlType, CWnd* pCtrl);
	void DrawPressDeviceInfos	(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight);
	void DrawPrintingTypeInfos	(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight);
	void DrawSheetInfos			(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight);
	void DrawLayoutInfos		(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight);
	void DrawLayoutMagnified	(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight);
	void FunctionRemove();
	static void OnSelchangePressDeviceSelector(CPressDevice* pNewPressDevice, int nPressDeviceIndex, CDlgSelectPressDevicePopup* pDlgSelector);
	static void OnSelchangeSheetSelector(CSheet* pSheet, CDlgSelectSheetPopup* pDlgSelector);


protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
public:
	afx_msg void OnMagnifySheet();
	afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnInplacePressDeviceDropDown();
	afx_msg void OnInplacePrintingTypeSelChange();
	afx_msg void OnInplacePrintSheetDropDown();
	afx_msg void OnInplacePrintLayoutDropDown();
	afx_msg void OnInplaceFunctionsSelChange();
};


// CDlgProductionPlannerUnbound-Dialogfeld

class CDlgProductionPlannerUnbound : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionPlannerUnbound)

public:
	CDlgProductionPlannerUnbound(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionPlannerUnbound();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTIONPLANNERUNBOUND };

public:
	int							m_nProductPartIndex;
	CPrintSheet*				m_pPrintSheet;
	CPressDevice*				m_pPressDevice;
	CLayout*					m_pLayout;
	CUnboundComponentsListCtrl	m_componentsList;
	CDlgSelectPressDevicePopup	m_dlgPressDeviceSelector;
	CDlgSelectSheetPopup		m_dlgSheetSelector;
	BOOL						m_bMagnify;


public:
	void			InitData();
	void			SaveData();
	void			UpdateProductComponentsList();
	CPrintSheet*	GetSelectedPrintSheet();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLvnItemchangedUnboundProductComponentsList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedPpMagnifyButton();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
};
