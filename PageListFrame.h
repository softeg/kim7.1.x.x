// PageListFrame.h : header file
//

#pragma once

#include "TrueColorToolBar.h"
#include "PageListTopToolsView.h"




class CPageListSplitter : public CSplitterWnd
{
    DECLARE_DYNAMIC(CPageListSplitter)
public:
	CPageListSplitter() { }

protected:
	virtual void OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect);

    DECLARE_MESSAGE_MAP()
    afx_msg LRESULT OnNcHitTest(CPoint point);
};





class CDlgPageTemplateProps;

/////////////////////////////////////////////////////////////////////////////
// CPageListFrame frame

class CPageListFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CPageListFrame)
protected:
	CPageListFrame();           // protected constructor used by dynamic creation

// Attributes
public: 
	BOOL m_bStatusRestored;
	BOOL m_bShowBitmap;	
	BOOL m_bZoomMode;
	BOOL m_bDeleteSelectionMenuActive;
	BOOL m_bAssigncolorsPsfileMenuActive;
	BOOL m_bPagesourcePropsMenuActive;
	BOOL m_bRipMessagesMenuActive;
	BOOL m_bShowLinkMenuActive;
	int	 m_nProductPartIndex;

	BOOL m_bShowPagesList;
	BOOL m_bEnablePagesListButton;
	BOOL m_bShowPageSource;
	BOOL m_bEnablePageSourceButton;
	BOOL m_bShowPageDetail;
	BOOL m_bEnablePageDetailButton;
	BOOL m_bShowPropsDlg;
	BOOL m_bEnablePropsDlgButton;

	BOOL m_bLoadPsfileMenuActive;

	BOOL m_bZoomMenuActive;
	BOOL m_bMainToolBarActive;

	BOOL m_bAcrobatOpen;

// Operations
protected:
	void		DeletePDFDocument				 (CDisplayItem* pDI);
	void		DeletePageSource				 (CDisplayItem* pDI, BOOL bBreakLinksOnly = FALSE);
	void		DeleteAssignedPageSourcesFromPage(CDisplayItem* pDI);

	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);

public:
	CPageSource*			 GetSelectedPageSource(int& nPageIndex);
	static BOOL				 LaunchAcrobat(const CString& strPDFFile, int nPageIndex);
	static HDDEDATA CALLBACK DDE_ProcessMessage(UINT uType, UINT uFmt, HCONV hconv, HSZ hsz1, HDDEDATA hdata, DWORD dwData1, DWORD dwData2);
	static BOOL				 InitNumerationDialog(CDlgNumeration* pDlg);
	static void				 ChangeNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex);
	CPageListTopToolsView*	 GetPageListTopToolsView();
	int						 GetActProductPartIndex();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageListFrame)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_OVERLAPPEDWINDOW, const RECT& rect = rectDefault, CMDIFrameWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);
	virtual void ActivateFrame(int nCmdShow = -1);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
public:
	CPageListSplitter m_wndSplitter, m_wndSplitter1;

protected:
	virtual ~CPageListFrame();

public:
	CDlgPageTemplateProps* m_pDlgPageTemplateProps;  // Needed also in PageListView to update DlgPageTemplateProps
	CDlgNumeration*		   m_pDlgFoldSheetNumeration;
////////////////////////////////////////////////////////////////////////////////////////////////
//	Must have acces from DlgPageTemplateProps and PageListView	-> afx_msg void OnPageprops();
	afx_msg void OnPageprops();
	afx_msg void OnRegisterPdfDoc();
protected:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);
	// Generated message map functions
	//{{AFX_MSG(CPageListFrame)
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnPrintsheetView();
	afx_msg void OnLoadPsfile();
	afx_msg void OnBitmapcheck();
	afx_msg void OnUpdateBitmapcheck(CCmdUI* pCmdUI);
	afx_msg void OnDeleteSelection();
	afx_msg void OnAssigncolorsPsfile();
	afx_msg void OnPagesourceProps();
	afx_msg void OnPagesource();
	afx_msg void OnUpdatePagesource(CCmdUI* pCmdUI);
	afx_msg void OnPageslist();
	afx_msg void OnUpdatePageslist(CCmdUI* pCmdUI);
	afx_msg void OnPagedetail();
	afx_msg void OnUpdatePagedetail(CCmdUI* pCmdUI);
	afx_msg void OnZoom();
	afx_msg void OnUpdateZoom(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePageprops(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAssigncolorsPsfile(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteSelection(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLoadPsfile(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePagesourceProps(CCmdUI* pCmdUI);
	afx_msg void OnViewPageListFrameBar();
	afx_msg void OnUpdateViewPageListFrameBar(CCmdUI* pCmdUI);
	afx_msg void OnRipmessages();
	afx_msg void OnUpdateRipmessages(CCmdUI* pCmdUI);
	afx_msg void OnShowLink();
	afx_msg void OnUpdateShowLink(CCmdUI* pCmdUI);
	afx_msg void OnBlockPageSourceHeader();
	afx_msg void OnUpdateBlockPageSourceHeader(CCmdUI* pCmdUI);
	afx_msg void OnZoomDecrease();
	afx_msg void OnUpdateZoomDecrease(CCmdUI* pCmdUI);
	afx_msg void OnFilePageSetup();
	afx_msg void OnOpenPdfDoc();
	afx_msg void OnDestroy();
	afx_msg void OnRefreshPagesources();
public:
	afx_msg void OnPagination();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
