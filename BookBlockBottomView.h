#pragma once



// CBookBlockBottomView-Formularansicht

class CBookBlockBottomView : public CFormView
{
	DECLARE_DYNCREATE(CBookBlockBottomView)

protected:
	CBookBlockBottomView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CBookBlockBottomView();

public:
	enum { IDD = IDD_BOOKBLOCKBOTTOMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};


