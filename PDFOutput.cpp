// PDFOutput.cpp: Implementierung der Klasse CPDFOutput.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "MainFrm.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "DlgStopOutput.h"
#include "PDFEngineInterface.h"
#include "PDFOutput.h"
#include "DlgPDFLibStatus.h"
#include "DlgConfirmPDFSheetOverwrite.h"
#include "acrobat.h"
#include "iac.h"
#include "DlgDemoOrRegisterDongle.h"
#include "spromeps.h"
#include <locale.h>
#include "KimSocketsCommon.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
 



void CAddSysGenSpotColorStreams::AddEntry(const CString& strPDFCode, const CString& strColorName, COLORREF crAlternate, float fTintValue)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (strColorName == GetAt(i).m_strSpotColorName)
		{
			GetAt(i).m_strPDFCode += strPDFCode;
			return;
		}
	}

	CAddSysGenSpotColorStreamEntry entry;
	entry.m_strSpotColorName	 = strColorName;
	entry.m_crSpotColorAlternate = crAlternate;
	entry.m_fSpotColorTintValue	 = fTintValue;
	entry.m_strPDFCode			 = strPDFCode;
	Add(entry);
}



//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CPDFOutput::CPDFOutput(CPDFTargetProperties& rPDFTarget, CString strDocTitle)
{
	m_nTargetType		  		= rPDFTarget.m_nType;
	m_strTargetName		  		= rPDFTarget.m_strTargetName;
	m_strLocation		  		= rPDFTarget.m_strLocation;
	m_strFilenameTemplate 		= rPDFTarget.m_strOutputFilename;
	m_bPDFOutputMime			= rPDFTarget.m_bJDFOutputMime;
	m_nPSLevel			  		= rPDFTarget.m_nPSLevel;
	m_bEmitHalftones	  		= rPDFTarget.m_bEmitHalftones;
	m_strDocTitle		  		= strDocTitle;
	m_fMediaWidth		  		= 0.0f;
	m_fMediaHeight		  		= 0.0f;
	m_nError			  		= 0;
	m_bCancelJob		  		= FALSE;
	m_media				  		= (rPDFTarget.m_mediaList.GetSize()) ? rPDFTarget.m_mediaList[0] : CMediaSize();
	m_nPlatesProcessed	  		= 0;
	m_bCalibration		  		= FALSE;
	m_bOutputFormatExceeded 	= FALSE;
	m_bJDFMarksOutput			= FALSE;
	m_bSysGenMarksColorSpaceALL = FALSE;

	m_hPDFFile					= -1;
	m_nDBCurrentPdfID			= -1;

	setlocale(LC_NUMERIC, "english");	// because when german setted, Format() does produce numbers with Comma like "0,138"
}

CPDFOutput::~CPDFOutput()
{
	CString strLanguage;
	switch (theApp.settings.m_iLanguage)
	{
		case 0: strLanguage = _T("GER"); setlocale(LC_ALL, "german");	break;
		case 1: strLanguage = _T("ENG"); setlocale(LC_ALL, "english");	break;
		case 2: strLanguage = _T("FRA"); break;
		case 3: strLanguage = _T("ITA"); break;
		case 4: strLanguage = _T("SPA"); break;
		case 5: strLanguage = _T("HOL"); break;	// No holl. MFC4x.dll available
		//Default would already be set at reading ini-file
	}
}

BOOL CPDFOutput::OpenStream(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bSendIfPrinter, int nSide, CPlate* pPlate, BOOL bJDFMarksOutput)
{
	m_bJDFMarksOutput = bJDFMarksOutput;
 
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE; 

	m_bCalibration = FALSE;
	if (m_strDocTitle.CompareNoCase(_T("KIM_Output_Calibration")) == 0)
		m_bCalibration = TRUE;

	int nPages = 0;
	if (m_bCalibration)
	{
		CLayoutSide* pLayoutSide = (pPlate) ? pPlate->GetLayoutSide() : NULL;
		nPages = (pLayoutSide) ? pLayoutSide->m_ObjectList.GetCount() : 0;
	}
	else
	{
		if (nSide == ALLSIDES)
			nPages = pDoc->m_PrintSheetList.GetNumOutputObjects((bJDFMarksOutput) ? CLayoutObject::ControlMark : -1);	// -1 = pages and marks
		else
			nPages = pPrintSheet->GetNumOutputObjects(nSide, pPlate, (bJDFMarksOutput) ? CLayoutObject::ControlMark : -1);

		if ( ! bJDFMarksOutput)
		{
			if (nPages == 0)	// don't create empty file 
				return FALSE;
			if (pPrintSheet->GetNumPagesOccupied(nSide) <= 0)
				return FALSE;
		}

		m_strTmpOutputPDFFile = pPrintSheet->GetOutFileTmpFullPath((nSide == ALLSIDES) ? BOTHSIDES : nSide, pPlate);
		m_strOutputPDFFile	  = pPrintSheet->GetOutFileFullPath	  ((nSide == ALLSIDES) ? BOTHSIDES : nSide, pPlate, bJDFMarksOutput, TRUE);

		if (bJDFMarksOutput)
		{
			theApp.RemoveExtension(m_strTmpOutputPDFFile);
			m_strTmpOutputPDFFile += _T("_Marks.pdf");
			if (m_strOutputPDFFile.IsEmpty())
				m_strOutputPDFFile.Format(_T("%s\\%s"), m_strLocation, m_strFilenameTemplate);
			else
			{
				theApp.RemoveExtension(m_strOutputPDFFile);
				m_strOutputPDFFile += _T("_Marks.pdf");
			}
			if ( ! theApp.DirectoryExist(m_strLocation))
				CreateDirectory(m_strLocation, NULL);
		}

	}

	CPrintSheetTableView*  pPrintSheetTableView  = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (bJDFMarksOutput != 2)
		if (pPrintSheetTableView)
			pPrintSheetTableView->HighlightPDFOutFile(pPrintSheet, (nSide == ALLSIDES) ? BOTHSIDES : nSide, pPlate);

	CFileStatus	fileStatus;
	if ( (bOverwriteAll == -1) || ! bOverwriteAll)
	{
		BOOL bFileExists = FALSE;
		if (CFile::GetStatus(m_strOutputPDFFile, fileStatus))	// target file exists
			bFileExists = TRUE;

		if (bFileExists)
		{
			int nRet = IDC_USE_EXISTING;
			if (bOverwriteAll == -1)
			{
				CDlgConfirmPDFSheetOverwrite dlg;
				dlg.m_strPDFFilename = m_strOutputPDFFile;
				nRet = dlg.DoModal();
			}
			switch (nRet)
			{
			case IDC_NEW:			bOverwriteAll = TRUE;
									break;
			case IDC_USE_EXISTING:	if ( (m_nTargetType == CPDFTargetProperties::TypePDFPrinter) && bSendIfPrinter)
										SendToTarget();
									bOverwriteAll = FALSE;
									if (pPrintSheetTableView)
										pPrintSheetTableView->StopPDFOutFeedback();
									if ( (m_nTargetType == CPDFTargetProperties::TypePDFFolder) || (m_nTargetType == CPDFTargetProperties::TypeJDFFolder) )	// if target is folder for PDFs we can break 
										m_bCancelJob = TRUE;
									return FALSE;
			case IDCANCEL:			m_bCancelJob = TRUE;
									if (pPrintSheetTableView)
										pPrintSheetTableView->StopPDFOutFeedback();
									return FALSE;
			}
		}
	}

	if (CFile::GetStatus(m_strTmpOutputPDFFile, fileStatus))	// make sure file doesn't exist, to prevent objects from being appended
		DeleteFile(m_strTmpOutputPDFFile);

	if (bJDFMarksOutput != 2)
		if (pPrintSheetTableView)
			pPrintSheetTableView->StartPDFOutFeedback(nPages);

	m_nCurrentPage = 1;

	if (IPL_GetPDFImpositionDLLVersion() != 0x00020005) 
	{
		pPrintSheetTableView->StopPDFOutFeedback();

		KIMOpenLogMessage(_T("pdfEngine library version incorrect, must be 0x00020005!"), MB_ICONERROR);

		m_nError = CImpManApp::PDFOutputError;
		m_bCancelJob = TRUE;
		return FALSE;
	}

	char szTmpDir[MAX_PATH];
	strcpy(szTmpDir, theApp.settings.m_szTempDir); strcat(szTmpDir, "\\");

	CSysToUTF8Converter s2utf8;
	IPL_int32_t nResult = IPL_PDFOutputStream_Open(s2utf8.Convert(m_strTmpOutputPDFFile), &m_hPDFFile, s2utf8.Convert(szTmpDir));		
	if (nResult)
	{
		m_nError = CImpManApp::PDFOutputError;
		if (bJDFMarksOutput != 2)
			pPrintSheetTableView->StopPDFOutFeedback();

		CString			strMsg, strMsg2;
		PTB_uint32_t	nBufSize = 1024;
		IPL_utf8_char_t szMsgUTF8[1024];
		IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
		PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();
		if ( ! CFile::GetStatus(theApp.settings.m_szTempDir, fileStatus))	// tmp folder does not exist
		{
			strMsg2.Format(IDS_TEMPFOLDER_NOT_EXISTING, theApp.settings.m_szTempDir);
			if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
				strMsg += _T("\n\n") + strMsg2;
			else
				strMsg += _T("\r\n   ") + strMsg2;
		}

		KIMOpenLogMessage(strMsg, MB_ICONERROR);

		return FALSE;
	}

	CString strOutFileName = pPrintSheet->GetOutFilename((nSide == ALLSIDES) ? BOTHSIDES : nSide, pPlate);

#ifdef KIM_ENGINE
	COleDateTime curTime(COleDateTime::GetCurrentTime());
	CString		 strMsg, strText;
	strText.LoadString(IDS_OUTPUT_PROFILE_SETTINGS);
	strMsg.Format(_T("\r\n%s\r\n   Template %s\r\n   %s - %s"), curTime.Format(_T("%c")), m_strDocTitle, strText, strOutFileName);
	theApp.LogEntry(strMsg); 
#endif

	// remove extension .pdf
	CString string = strOutFileName; string.MakeLower();
	int nIndex = string.Find(_T(".pdf"));	
	strOutFileName = (nIndex != -1) ? strOutFileName.Left(nIndex) : strOutFileName;

	CString strProducer;
	strProducer.Format(_T("KIM PDF %s (Build: %s)"), theApp.strVersion, CString(__DATE__));

	IPL_PDFIMPOSITIONINFO infoStruct;
	infoStruct.size			= sizeof(IPL_PDFIMPOSITIONINFO);
	infoStruct.lpAuthor		= s2utf8.Convert(pDoc->m_GlobalData.m_strCreator); 
	infoStruct.lpProducer	= s2utf8.Convert(strProducer);
	infoStruct.lpTitle		= s2utf8.Convert(strOutFileName);
	infoStruct.lpSubject	= s2utf8.Convert(pDoc->m_GlobalData.m_strJob);
	infoStruct.lpKeywords	= s2utf8.Convert(pDoc->m_GlobalData.m_strNotes);
	infoStruct.lpDummy1		= NULL;
	infoStruct.lpDummy2		= NULL;

	nResult = IPL_PDFOutputStream_AddInfo(m_hPDFFile, &infoStruct);	
	if (nResult)
	{
		CString			strMsg;
		PTB_uint32_t	nBufSize = 1024;
		IPL_utf8_char_t szMsgUTF8[1024];
		IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
		PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();

		KIMOpenLogMessage(strMsg, MB_ICONERROR);

		CloseStream(bSendIfPrinter);
		return FALSE;
	}

	m_nDBCurrentPdfID = -1;
	kimAutoDB_outputPDF outputPDF;
	theApp.m_kaDB.ka_GetOutputPDF(&outputPDF, pPrintSheet->GetNumber().GetBuffer(), theApp.m_nDBCurrentJobID, 1);
	if (pPrintSheet->GetNumber().CompareNoCase(outputPDF.name.c_str()) == 0)
	{
		m_nDBCurrentPdfID = outputPDF.id;
		CTime firstDate = CTime::GetCurrentTime();
		CTime lastDate  = CTime::GetCurrentTime();

		theApp.m_kaDB.ka_SetOutputPDF(m_nDBCurrentPdfID, outputPDF.index, outputPDF.name, outputPDF.pageNr, outputPDF.pageOccupied, kimAutoDB::outPDF_writing, theApp.m_nDBCurrentJobID, outputPDF.thumbnailBitmap, outputPDF.outputPath, 
										(std::string)firstDate.Format("%Y-%m-%d %H:%M:%S"), (std::string)lastDate.Format("%Y-%m-%d %H:%M:%S"), outputPDF.inputOutputKey);
		KimEngineNotifyServer(MSG_OUTPUTPDF_STATUS_CHANGED, theApp.m_nDBCurrentJobID);
	}

	//theApp.m_pDlgPDFLibStatus->m_strFilename = theApp.m_pDlgPDFLibStatus->m_strFullFilename = m_strOutputPDFFile;
	//theApp.m_pDlgPDFLibStatus->UpdateStatus(CDlgPDFLibStatus::OpenDocument);

	m_separationInfo.Reset();

	return TRUE;
}

BOOL CPDFOutput::OpenStream(CBookblock* pBookblock, CFoldSheet* pFoldSheet, BOOL& bOverwriteAll, BOOL bSendIfPrinter, int nSide, BOOL bJDFMarksOutput)
{
	m_bJDFMarksOutput = bJDFMarksOutput;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE; 
	
	m_strTmpOutputPDFFile = pBookblock->GetOutFileTmpFullPath((nSide == ALLSIDES) ? BOTHSIDES : nSide, pFoldSheet);
	m_strOutputPDFFile	  = pBookblock->GetOutFileFullPath	 ((nSide == ALLSIDES) ? BOTHSIDES : nSide, pFoldSheet, bJDFMarksOutput, TRUE);

	if (bJDFMarksOutput)
	{
		theApp.RemoveExtension(m_strTmpOutputPDFFile);
		m_strTmpOutputPDFFile += _T("_Marks.pdf");
		if (m_strOutputPDFFile.IsEmpty())
			m_strOutputPDFFile.Format(_T("%s\\%s"), m_strLocation, m_strFilenameTemplate);
		else
		{
			theApp.RemoveExtension(m_strOutputPDFFile);
			m_strOutputPDFFile += _T("_Marks.pdf");
		}
		if ( ! theApp.DirectoryExist(m_strLocation))
			CreateDirectory(m_strLocation, NULL);
	}

	CFileStatus	fileStatus;
	if ( (bOverwriteAll == -1) || ! bOverwriteAll)
	{
		BOOL bFileExists = FALSE;
		if (CFile::GetStatus(m_strOutputPDFFile, fileStatus))	// target file exists
			bFileExists = TRUE;

		if (bFileExists)
		{
			int nRet = IDC_USE_EXISTING;
			if (bOverwriteAll == -1)
			{
				CDlgConfirmPDFSheetOverwrite dlg;
				dlg.m_strPDFFilename = m_strOutputPDFFile;
				nRet = dlg.DoModal();
			}
			switch (nRet)
			{
			case IDC_NEW:			bOverwriteAll = TRUE;
									break;
			case IDC_USE_EXISTING:	if ( (m_nTargetType == CPDFTargetProperties::TypePDFPrinter) && bSendIfPrinter)
										SendToTarget();
									bOverwriteAll = FALSE;
									if ( (m_nTargetType == CPDFTargetProperties::TypePDFFolder) || (m_nTargetType == CPDFTargetProperties::TypeJDFFolder) )	// if target is folder for PDFs we can break 
										m_bCancelJob = TRUE;
									return FALSE;
			case IDCANCEL:			m_bCancelJob = TRUE;
									return FALSE;
			}
		}
	}

	if (CFile::GetStatus(m_strTmpOutputPDFFile, fileStatus))	// make sure file doesn't exist, to prevent objects from being appended
		DeleteFile(m_strTmpOutputPDFFile);

	m_nCurrentPage = 1;

	if (IPL_GetPDFImpositionDLLVersion() != 0x00020005) 
	{
		KIMOpenLogMessage(_T("pdfEngine library version incorrect, must be 0x00020005!"), MB_ICONERROR);

		m_nError = CImpManApp::PDFOutputError;
		m_bCancelJob = TRUE;
		return FALSE;
	}

	char szTmpDir[MAX_PATH];
	strcpy(szTmpDir, theApp.settings.m_szTempDir); strcat(szTmpDir, "\\");

	CSysToUTF8Converter s2utf8;
	IPL_int32_t nResult = IPL_PDFOutputStream_Open(s2utf8.Convert(m_strTmpOutputPDFFile), &m_hPDFFile, s2utf8.Convert(szTmpDir));		
	if (nResult)
	{
		m_nError = CImpManApp::PDFOutputError;

		CString			strMsg, strMsg2;
		PTB_uint32_t	nBufSize = 1024;
		IPL_utf8_char_t szMsgUTF8[1024];
		IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
		PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();
		if ( ! CFile::GetStatus(theApp.settings.m_szTempDir, fileStatus))	// tmp folder does not exist
		{
			strMsg2.Format(IDS_TEMPFOLDER_NOT_EXISTING, theApp.settings.m_szTempDir);
			if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
				strMsg += _T("\n\n") + strMsg2;
			else
				strMsg += _T("\r\n   ") + strMsg2;
		}

		KIMOpenLogMessage(strMsg, MB_ICONERROR);

		return FALSE;
	}

	CString strOutFileName = pBookblock->GetOutFilename((nSide == ALLSIDES) ? BOTHSIDES : nSide, pFoldSheet);

#ifdef KIM_ENGINE
	COleDateTime curTime(COleDateTime::GetCurrentTime());
	CString		 strMsg;
	strMsg.Format(_T("\r\n%s\r\n   Template %s\r\n   Booklet Proof - %s"), curTime.Format(_T("%c")), m_strDocTitle, strOutFileName);
	theApp.LogEntry(strMsg); 
#endif

	// remove extension .pdf
	CString string = strOutFileName; string.MakeLower();
	int nIndex = string.Find(_T(".pdf"));	
	strOutFileName = (nIndex != -1) ? strOutFileName.Left(nIndex) : strOutFileName;

	CString strProducer;
	strProducer.Format(_T("KIM PDF %s (Build: %s)"), theApp.strVersion, CString(__DATE__));

	IPL_PDFIMPOSITIONINFO infoStruct;
	infoStruct.size			= sizeof(IPL_PDFIMPOSITIONINFO);
	infoStruct.lpAuthor		= s2utf8.Convert(pDoc->m_GlobalData.m_strCreator); 
	infoStruct.lpProducer	= s2utf8.Convert(strProducer);
	infoStruct.lpTitle		= s2utf8.Convert(strOutFileName);
	infoStruct.lpSubject	= s2utf8.Convert(pDoc->m_GlobalData.m_strJob);
	infoStruct.lpKeywords	= s2utf8.Convert(pDoc->m_GlobalData.m_strNotes);
	infoStruct.lpDummy1		= NULL;
	infoStruct.lpDummy2		= NULL;

	nResult = IPL_PDFOutputStream_AddInfo(m_hPDFFile, &infoStruct);	
	if (nResult)
	{
		CString			strMsg;
		PTB_uint32_t	nBufSize = 1024;
		IPL_utf8_char_t szMsgUTF8[1024];
		IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
		PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();

		KIMOpenLogMessage(strMsg, MB_ICONERROR);

		CloseStream(bSendIfPrinter);
		return FALSE;
	}

	m_separationInfo.Reset();

	return TRUE;
}

BOOL CPDFOutput::OpenStream(CFoldSheet* pFoldSheet, BOOL& bOverwriteAll, BOOL bSendIfPrinter, int nSide, CPlate* pPlate)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE; 

	m_strTmpOutputPDFFile = pFoldSheet->GetOutFileTmpFullPath((nSide == ALLSIDES) ? BOTHSIDES : nSide, pFoldSheet, pPlate);
	m_strOutputPDFFile	  = pFoldSheet->GetOutFileFullPath	 ((nSide == ALLSIDES) ? BOTHSIDES : nSide, pFoldSheet, pPlate, TRUE);

	CFileStatus	fileStatus;
	if ( (bOverwriteAll == -1) || ! bOverwriteAll)
	{
		BOOL bFileExists = FALSE;
		if (CFile::GetStatus(m_strOutputPDFFile, fileStatus))	// target file exists
			bFileExists = TRUE;

		if (bFileExists)
		{
			int nRet = IDC_USE_EXISTING;
			if (bOverwriteAll == -1)
			{
				CDlgConfirmPDFSheetOverwrite dlg;
				dlg.m_strPDFFilename = m_strOutputPDFFile;
				nRet = dlg.DoModal();
			}
			switch (nRet)
			{
			case IDC_NEW:			bOverwriteAll = TRUE;
									break;
			case IDC_USE_EXISTING:	if ( (m_nTargetType == CPDFTargetProperties::TypePDFPrinter) && bSendIfPrinter)
										SendToTarget();
									bOverwriteAll = FALSE;
									if ( (m_nTargetType == CPDFTargetProperties::TypePDFFolder) || (m_nTargetType == CPDFTargetProperties::TypeJDFFolder) )	// if target is folder for PDFs we can break 
										m_bCancelJob = TRUE;
									return FALSE;
			case IDCANCEL:			m_bCancelJob = TRUE;
									return FALSE;
			}
		}
	}

	if (CFile::GetStatus(m_strTmpOutputPDFFile, fileStatus))	// make sure file doesn't exist, to prevent objects from being appended
		DeleteFile(m_strTmpOutputPDFFile);

	m_nCurrentPage = 1;

	if (IPL_GetPDFImpositionDLLVersion() != 0x00020005) 
	{
		KIMOpenLogMessage(_T("pdfEngine library version incorrect, must be 0x00020005!"), MB_ICONERROR);
		m_nError = CImpManApp::PDFOutputError;
		m_bCancelJob = TRUE;
		return FALSE;
	}

	char szTmpDir[MAX_PATH];
	strcpy(szTmpDir, theApp.settings.m_szTempDir); strcat(szTmpDir, "\\");

	CSysToUTF8Converter s2utf8;
	IPL_int32_t nResult = IPL_PDFOutputStream_Open(s2utf8.Convert(m_strTmpOutputPDFFile), &m_hPDFFile, s2utf8.Convert(szTmpDir));		
	if (nResult)
	{
		m_nError = CImpManApp::PDFOutputError;
		CString			strMsg, strMsg2;
		PTB_uint32_t	nBufSize = 1024;
		IPL_utf8_char_t szMsgUTF8[1024];
		IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
		PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();
		if ( ! CFile::GetStatus(theApp.settings.m_szTempDir, fileStatus))	// tmp folder does not exist
		{
			strMsg2.Format(IDS_TEMPFOLDER_NOT_EXISTING, theApp.settings.m_szTempDir);
			if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
				strMsg += _T("\n\n") + strMsg2;
			else
				strMsg += _T("\r\n   ") + strMsg2;
		}

		KIMOpenLogMessage(strMsg, MB_ICONERROR);

		return FALSE;
	}

	CString strOutFileName = pFoldSheet->GetOutFilename((nSide == ALLSIDES) ? BOTHSIDES : nSide, pFoldSheet, pPlate);

	// remove extension .pdf
	CString string = strOutFileName; string.MakeLower();
	int nIndex = string.Find(_T(".pdf"));	
	strOutFileName = (nIndex != -1) ? strOutFileName.Left(nIndex) : strOutFileName;

	CString strProducer;
	strProducer.Format(_T("KIM PDF %s (Build: %s)"), theApp.strVersion, CString(__DATE__));

	IPL_PDFIMPOSITIONINFO infoStruct;
	infoStruct.size			= sizeof(IPL_PDFIMPOSITIONINFO);
	infoStruct.lpAuthor		= s2utf8.Convert(pDoc->m_GlobalData.m_strCreator); 
	infoStruct.lpProducer	= s2utf8.Convert(strProducer);
	infoStruct.lpTitle		= s2utf8.Convert(strOutFileName);
	infoStruct.lpSubject	= s2utf8.Convert(pDoc->m_GlobalData.m_strJob);
	infoStruct.lpKeywords	= s2utf8.Convert(pDoc->m_GlobalData.m_strNotes);
	infoStruct.lpDummy1		= NULL;
	infoStruct.lpDummy2		= NULL;

	nResult = IPL_PDFOutputStream_AddInfo(m_hPDFFile, &infoStruct);	
	if (nResult)
	{
		CString			strMsg;
		PTB_uint32_t	nBufSize = 1024;
		IPL_utf8_char_t szMsgUTF8[1024];
		IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
		PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();
		KIMOpenLogMessage(strMsg, MB_ICONERROR);
		CloseStream(bSendIfPrinter);
		return FALSE;
	}

	m_separationInfo.Reset();

	return TRUE;
}


void CPDFOutput::CloseStream(BOOL bSendIfPrinter, BOOL bJDFMarksOutput)
{
	m_separationInfo.Insert(m_hPDFFile);

	CPrintSheetTableView*  pPrintSheetTableView  = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));

	IPL_SetProgressFunction( NULL, (long)NULL);
	if (bJDFMarksOutput != 2)
	{
		if (pPrintSheetTableView)
		{
			pPrintSheetTableView->StopPDFOutFeedback();
			pPrintSheetTableView->m_strPDFOutputMessage.LoadString(IDS_CLOSE_PDF_OUTPUT);
			pPrintSheetTableView->UpdateData(FALSE);
		}

		if (pPrintSheetTableView)
		{
			pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_SHOW);
			IPL_SetProgressFunction( ProgressCloseStream, (void*)pPrintSheetTableView);
		}
	}

	IPL_int32_t nError = IPL_PDFOutputStream_Close(m_hPDFFile, TRUE);//FALSE);
	m_hPDFFile = -1;

	if (bJDFMarksOutput != 2)
	{
		if (pPrintSheetTableView)
			pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_HIDE);
	}

	if ( (m_nTargetType == CPDFTargetProperties::TypePDFPrinter) && ! bSendIfPrinter)
		;
	else
		SendToTarget();

	if (bJDFMarksOutput != 2)
	{
		if (pPrintSheetTableView)
			pPrintSheetTableView->LowlightPDFOutFile();
	}
}


IPL_bool_t CPDFOutput::ProgressCloseStream( IPL_int32_t lowrange, IPL_int32_t highrange, IPL_int32_t counter, void* lparam)
{
	CPrintSheetTableView* pView = (CPrintSheetTableView*)lparam;
	if (pView)
	{
		pView->m_pdfOutputProgress.SetRange((short)lowrange, (short)highrange);
		pView->m_pdfOutputProgress.SetPos(counter);
	}

	return TRUE;
}


void CPDFOutput::SendToTarget()
{
	CPrintSheetTableView* pPrintSheetTableView  = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if ( (m_nTargetType == CPDFTargetProperties::TypePDFFolder) || (m_nTargetType == CPDFTargetProperties::TypeJDFFolder) )
	{
		if ( ! MoveFileEx(m_strTmpOutputPDFFile, m_strOutputPDFFile, MOVEFILE_REPLACE_EXISTING))
		{
			if (pPrintSheetTableView)
			{
				pPrintSheetTableView->m_strPDFOutputMessage.Format(IDS_COPY_PDF_TO, m_strOutputPDFFile);
				pPrintSheetTableView->UpdateData(FALSE);
				pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_SHOW);
			}
			BOOL bCancel = FALSE;
			if ( ! CopyFileEx(m_strTmpOutputPDFFile, m_strOutputPDFFile, CopyProgressRoutine, (LPVOID)pPrintSheetTableView, &bCancel, 0))	// 0 = overwrite if dest. existing
			{
				LPVOID lpMsgBuf;
				FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |	FORMAT_MESSAGE_IGNORE_INSERTS, NULL, GetLastError(),
							  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0,	NULL);
				CString strMsg;
				strMsg.Format(IDS_ERROR_COPY_PDF, m_strOutputPDFFile);
				strMsg += _T("\r\n") + CString("   ") + CString((LPCTSTR)lpMsgBuf);
				KIMOpenLogMessage(strMsg, MB_ICONERROR);
				// Free the buffer.
				LocalFree( lpMsgBuf );
				m_nError = CImpManApp::PDFOutputError;
 			}
			DeleteFile(m_strTmpOutputPDFFile);
			if (pPrintSheetTableView)
				pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_HIDE);
		}
	}
	else
	{
		if (m_bCancelJob != 2)	// not stop all
		{
			if (pPrintSheetTableView)
			{
				pPrintSheetTableView->Invalidate();
				pPrintSheetTableView->UpdateWindow();
				//pPrintSheetTableView->m_wndAnimate.Stop(); 

				pPrintSheetTableView->m_strPDFOutputMessage.Format(IDS_SEND_PDF_TO, m_strTargetName);
				pPrintSheetTableView->UpdateData(FALSE);
			}
			
			HKEY hKey;
			CString strKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\Acrobat.exe");
			if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
			{
				DWORD dwSize = 512;
				TCHAR szPath[512];
				RegQueryValueEx(hKey, _T("Path"), 0, NULL, (LPBYTE)szPath, &dwSize);
				RegCloseKey(hKey);

				CString strOldDefaultPrinter = GetSystemDefaultPrinter(); 
				SetSystemDefaultPrinter(m_strLocation);

				if (theApp.m_pMessageFilter != NULL) 
				{
					theApp.m_pMessageFilter->EnableBusyDialog(TRUE);
					theApp.m_pMessageFilter->EnableNotRespondingDialog(TRUE);
					theApp.m_pMessageFilter->SetRetryReply(10000);
					theApp.m_pMessageFilter->SetMessagePendingDelay(20000);
				}

				CAcroApp* pMainAcroApp = new CAcroApp; 
				COleException e;
				if (!pMainAcroApp->CreateDispatch(_T("AcroExch.App"),&e)) 
				{
					KIMOpenLogMessage(_T("Failed to create object :<"), MB_ICONERROR);
					m_nError = CImpManApp::PDFOutputError;
				}

				BOOL bOldShrinkToFit = pMainAcroApp->GetPreference(avpShrinkToFit);		// Workaround: shrinkToFit only works by using preferences
				pMainAcroApp->SetPreference(avpShrinkToFit,	m_media.m_bShrinkToFit);	// Setting in PrintPages...() has no effect :-( ???

				// Create new CAcroAVDoc  
				CAcroAVDoc* pAcroAVDoc = new CAcroAVDoc;
				if (!pAcroAVDoc->CreateDispatch(_T("AcroExch.AVDoc"),&e)) 
				{
					KIMOpenLogMessage(_T("Failed to create AVDoc :<"), MB_ICONERROR);
					m_nError = CImpManApp::PDFOutputError;
				}
				else
				{
					pAcroAVDoc->Open(m_strTmpOutputPDFFile, NULL);
					CAcroPDDoc acroPDDoc; 
					acroPDDoc.AttachDispatch(pAcroAVDoc->GetPDDoc(), TRUE);
					pAcroAVDoc->PrintPagesSilentEx(0, acroPDDoc.GetNumPages() - 1, m_nPSLevel, FALSE, FALSE, FALSE, FALSE, m_bEmitHalftones, PDAllPages);
					pAcroAVDoc->Close(TRUE);
				}

				pMainAcroApp->SetPreference(avpShrinkToFit,	bOldShrinkToFit);

				delete pAcroAVDoc;
				delete pMainAcroApp;

				SetSystemDefaultPrinter(strOldDefaultPrinter);
			}

		}
	}

	if (pPrintSheetTableView)
	{
		pPrintSheetTableView->m_strPDFOutputMessage  = "";
		pPrintSheetTableView->UpdateData(FALSE);
	}

#ifdef KIM_ENGINE
	theApp.m_kaDB.ka_SetOutputPDFStatus(m_nDBCurrentPdfID, kimAutoDB::outPDF_idle);
	KimEngineNotifyServer(MSG_OUTPUTPDF_STATUS_CHANGED, theApp.m_nDBCurrentJobID);
	if ( ! m_nError && ! m_bCancelJob)
	{
		TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
		_tsplitpath(m_strOutputPDFFile, szDrive, szDir, szFname, szExt);
		CString strFilename = CString(szFname) + CString(szExt);
		CString strMsg, strOutputtedTo;
		strOutputtedTo.LoadString(IDS_OUTPUTTED_TO);
		strMsg.Format(_T("%s - %s %s\\%s"), strFilename, strOutputtedTo, szDrive, szDir);
		KIMLogMessage(strMsg, KIM_MSGTYPE_INFO);

		if (theApp.m_bKimEngineAbort)
			m_bCancelJob = TRUE;
	}
#endif
}

DWORD CALLBACK CPDFOutput::CopyProgressRoutine(	LARGE_INTEGER TotalFileSize,	LARGE_INTEGER TotalBytesTransferred, 
												LARGE_INTEGER StreamSize,		LARGE_INTEGER StreamBytesTransferred,
												DWORD dwStreamNumber, DWORD dwCallbackReason, HANDLE hSourceFile, HANDLE hDestinationFile, LPVOID lpData)
{
	CPrintSheetTableView* pView = (CPrintSheetTableView*)lpData;
	if (pView)
	{
		pView->m_pdfOutputProgress.SetRange((short)0, (short)(TotalFileSize.QuadPart/1000000));	// show progress in megabytes
		pView->m_pdfOutputProgress.SetPos((short)(TotalBytesTransferred.QuadPart/1000000));
	}

	return PROGRESS_CONTINUE;
}

CString CPDFOutput::GetSystemDefaultPrinter()
{
	TCHAR szDefaultPrinter[512];
	DWORD dwSize = 512;
	HKEY  hKey;
	CString strKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Windows");
	if (RegOpenKey(HKEY_CURRENT_USER, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		dwSize = 512;
		RegQueryValueEx(hKey, _T("Device"), 0, NULL, (LPBYTE)szDefaultPrinter, &dwSize);
		RegCloseKey(hKey);
	}
	CString strDefaultPrinter = szDefaultPrinter;
	int nIndex = strDefaultPrinter.Find(_T(","));
	if (nIndex > 0)
		return strDefaultPrinter.Left(nIndex);
	else
		return "";
}


void CPDFOutput::SetSystemDefaultPrinter(CString strDefaultPrinter)
{
	if (strDefaultPrinter.IsEmpty())
		return;

	TCHAR szPrinterData[512];
	DWORD dwSize = 512;
	HKEY  hKey;
	CString strKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Devices");
	if (RegOpenKey(HKEY_CURRENT_USER, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		RegQueryValueEx(hKey, strDefaultPrinter, 0, NULL, (LPBYTE)szPrinterData, &dwSize);
		RegCloseKey(hKey);

		CString strKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Windows");
		if (RegOpenKey(HKEY_CURRENT_USER, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
		{
			dwSize = 512;
			strDefaultPrinter += _T(",") + CString(szPrinterData);
			RegSetValueEx  (hKey, _T("Device"),	0, REG_SZ, (LPBYTE)strDefaultPrinter.GetBuffer(MAX_TEXT), strDefaultPrinter.GetLength() + 1);
			RegCloseKey(hKey);
			//SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0L, 0L, SMTO_NORMAL, 1000, NULL);
		}
	}
}


BOOL CPDFOutput::ProcessPrintSheet(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter) 
{
	if (!pPrintSheet)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return TRUE;

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(pPrintSheet->m_strPDFTarget);
	if ( ! pTargetProps)
		return TRUE;

	if (theApp.m_nDongleStatus == CImpManApp::NewsDongle)
	{
		for (int i = 0; i < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); i++)
		{
			CFoldSheetLayer* pLayer = pPrintSheet->GetFoldSheetLayer(i);
			if (pLayer->m_nPagesX * pLayer->m_nPagesY > 4)
			{
				KIMOpenLogMessage(IDS_NEWS_EDITION_NUMPAGES_HINT, MB_ICONWARNING);
				return TRUE;
			}
		}
	}

	int nRet	  = TRUE;
	switch (pPrintSheet->GetOutfileRelation(pTargetProps->m_strOutputFilename))
	{
	case CPrintSheet::SheetPerFile: nRet = ProcessPrintSheetAllInOne (pPrintSheet, bOverwriteAll, bAutoCmd, bSendIfPrinter, FALSE);	break;
	case CPrintSheet::SidePerFile:	nRet = ProcessPrintSheetSidewise (pPrintSheet, bOverwriteAll, bAutoCmd, bSendIfPrinter, FALSE);	break;
	case CPrintSheet::PlatePerFile:	nRet = ProcessPrintSheetPlatewise(pPrintSheet, bOverwriteAll, bAutoCmd, bSendIfPrinter, FALSE);	break;
	}

	return nRet;
}


BOOL CPDFOutput::ProcessPrintSheetsJobInOne(CPrintSheetList* pPrintSheetList, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput, CPrintSheet** pCurPrintSheet, int* pnSheetsOut, int nProductPartIndex) 
{
	if ( ! pPrintSheetList)
		return m_bCancelJob;
	//if ( ! pPrintSheetList->ReadyForOutput(BOTHSIDES, bAutoCmd))
	//	return m_bCancelJob;

	CPrintSheet* pPrintSheet = &pPrintSheetList->GetHead();
	if (pCurPrintSheet)
		pPrintSheet = (*pCurPrintSheet) ? *pCurPrintSheet : &pPrintSheetList->GetHead();
	if ( ! pPrintSheet)
		return m_bCancelJob;

	CPlate* pPlate = NULL;
	if (bJDFMarksOutput)
		pPlate = &pPrintSheet->m_FrontSidePlates.m_defaultPlate; 

	pPrintSheetList->ResetLastOutputDates();

	if ( ! OpenStream(pPrintSheet, bOverwriteAll, bSendIfPrinter, ALLSIDES, pPlate, bJDFMarksOutput))
		return m_bCancelJob;

	if (pnSheetsOut)
		*pnSheetsOut = 0;

	POSITION pos = pPrintSheetList->GetHeadPosition();
	if (pCurPrintSheet)
		pos = (*pCurPrintSheet) ? pPrintSheetList->GetPos(**pCurPrintSheet) : pPrintSheetList->GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pPrintSheetList->GetNext(pos);
		if (nProductPartIndex >= 0)
		//if (nProductPartIndex != -1)
			if (rPrintSheet.GetPrintGroupIndex() != nProductPartIndex)
				continue;

		if (theApp.m_nDongleStatus == CImpManApp::NewsDongle)
		{
			for (int i = 0; i < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
			{
				CFoldSheetLayer* pLayer = rPrintSheet.GetFoldSheetLayer(i);
				if (pLayer->m_nPagesX * pLayer->m_nPagesY > 4)
				{
					KIMOpenLogMessage(IDS_NEWS_EDITION_NUMPAGES_HINT, MB_ICONWARNING);
					pPrintSheetList->ResetLastOutputDates();
					return m_bCancelJob;
				}
			}
		}

		if (pCurPrintSheet)
			*pCurPrintSheet = &rPrintSheet;

		m_nPlatesProcessed = 0;

		if ( ! bJDFMarksOutput)	
		{
			if (CheckTargetProps(rPrintSheet))
			{
				m_bCancelJob = 1;	// if type , filename or location of target is different, abort current sheet
				pos = pPrintSheetList->GetPos(rPrintSheet);
				pPrintSheetList->GetPrev(pos);			// go back to previous in order to let calling function resume with this sheet
				if (pos)
					if (pCurPrintSheet)
						*pCurPrintSheet = &pPrintSheetList->GetAt(pos);	
				break;
			}
		}

		BOOL bDoOutput = ( ! bAutoCmd || bJDFMarksOutput) ? TRUE : FALSE;
		if ( ! bJDFMarksOutput)
			if (bAutoCmd)
				bDoOutput = (rPrintSheet.GetNumPages(BOTHSIDES) == rPrintSheet.GetNumPagesOccupied(BOTHSIDES)) ? TRUE : FALSE;

		//if (bDoOutput)
		{
			POSITION platePos = rPrintSheet.m_FrontSidePlates.GetHeadPosition();
			while ( (platePos || bJDFMarksOutput) && ! m_bCancelJob)
			{
				CPlate& rPlate = (platePos) ? rPrintSheet.m_FrontSidePlates.GetNext(platePos) : rPrintSheet.m_FrontSidePlates.m_defaultPlate;
				if ( ! rPlate.m_bOutputDisabled || (bJDFMarksOutput == 2) )
				{
					rPlate.m_OutputDate.m_dt = 0.0f;
					if ( ! (m_bCancelJob = ProcessPlate(&rPlate, bJDFMarksOutput)) )
						rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
				}
				if ( ! platePos)
					break;
			}

			CheckOutputControlFrames(&rPrintSheet, FRONTSIDE);

			if (rPrintSheet.GetLayout()->m_nProductType == WORK_AND_BACK)	// work and turn / work and tumble / flat work is single sided
			{
				m_separationInfo.NewLogicalPage();

				platePos = rPrintSheet.m_BackSidePlates.GetHeadPosition();
				while ( (platePos || bJDFMarksOutput) && ! m_bCancelJob)
				{
					CPlate& rPlate = (platePos) ? rPrintSheet.m_BackSidePlates.GetNext(platePos) : rPrintSheet.m_BackSidePlates.m_defaultPlate;
					if ( ! rPlate.m_bOutputDisabled || (bJDFMarksOutput == 2) )
					{
						rPlate.m_OutputDate.m_dt = 0.0f;
						if ( ! (m_bCancelJob = ProcessPlate(&rPlate, bJDFMarksOutput)) )
							rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
					}
					if ( ! platePos)
						break;
				}

				CheckOutputControlFrames(&rPrintSheet, BACKSIDE);
			}
		}

		if (m_bCancelJob == 2)
			break;
		else
			if (m_bCancelJob == TRUE)
				m_bCancelJob = 0;

		if (pnSheetsOut)
			if (m_nPlatesProcessed)
				(*pnSheetsOut)++;
	}

	CloseStream(bSendIfPrinter, bJDFMarksOutput);

	if ( (m_nError != 0) || m_bCancelJob)
		pPrintSheetList->ResetLastOutputDates();

	return m_bCancelJob;
}

BOOL CPDFOutput::CheckTargetProps(CPrintSheet& rPrintSheet)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
	if ( ! pTargetProps)
		return TRUE;

	if (m_nTargetType		  != pTargetProps->m_nType)
		return TRUE;
	if (m_strFilenameTemplate != pTargetProps->m_strOutputFilename)
		return TRUE;
	if ( ! m_bPDFOutputMime)	// when mime, location is always temp folder
		if (m_strLocation	  != pTargetProps->m_strLocation)
			return TRUE;
	
	m_media	= pTargetProps->m_mediaList[0];

	return FALSE;
}

BOOL CPDFOutput::ProcessPrintSheetAllInOne(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput) 
{
	pPrintSheet->ResetLastOutputDates();

	if (pPrintSheet->ReadyForOutput(BOTHSIDES, bAutoCmd) || bJDFMarksOutput)
	{
		if (OpenStream(pPrintSheet, bOverwriteAll, bSendIfPrinter, BOTHSIDES, NULL, bJDFMarksOutput))
		{
			POSITION platePos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
			while ( (platePos || bJDFMarksOutput) && ! m_bCancelJob)
			{
				CPlate& rPlate = (platePos) ? pPrintSheet->m_FrontSidePlates.GetNext(platePos) : pPrintSheet->m_FrontSidePlates.m_defaultPlate;
				if ( ! rPlate.m_bOutputDisabled)
				{
					rPlate.m_OutputDate.m_dt = 0.0f;
					if ( ! (m_bCancelJob = ProcessPlate(&rPlate, bJDFMarksOutput)) )
						rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
				}
				if ( ! platePos)
					break;
			}

			CheckOutputControlFrames(pPrintSheet, FRONTSIDE);

			if (pPrintSheet->GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
			{
				CloseStream(bSendIfPrinter);
				if (m_nError != 0)
					pPrintSheet->ResetLastOutputDates();
				return m_bCancelJob;
			}

			m_separationInfo.NewLogicalPage();

			platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
			while ( (platePos || bJDFMarksOutput) && ! m_bCancelJob)
			{
				CPlate& rPlate = (platePos) ? pPrintSheet->m_BackSidePlates.GetNext(platePos) : pPrintSheet->m_BackSidePlates.m_defaultPlate;
				if ( ! rPlate.m_bOutputDisabled)
				{
					rPlate.m_OutputDate.m_dt = 0.0f;
					if ( ! (m_bCancelJob = ProcessPlate(&rPlate, bJDFMarksOutput)) )
						rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
				}
				if ( ! platePos)
					break;
			}

			CheckOutputControlFrames(pPrintSheet, BACKSIDE);

			CloseStream(bSendIfPrinter);
			if (m_nError != 0)
				pPrintSheet->ResetLastOutputDates();
		}
	}
	return m_bCancelJob;
}

BOOL CPDFOutput::ProcessPrintSheetSidewise(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput) 
{
	pPrintSheet->ResetLastOutputDates(FRONTSIDE);

	if (pPrintSheet->ReadyForOutput(FRONTSIDE, bAutoCmd) || bJDFMarksOutput)
		if (pPrintSheet->m_FrontSidePlates.GetCount() || bJDFMarksOutput)
		{
			if (OpenStream(pPrintSheet, bOverwriteAll, bSendIfPrinter, FRONTSIDE, (pPrintSheet->m_FrontSidePlates.GetCount()) ? &pPrintSheet->m_FrontSidePlates.GetHead() : &pPrintSheet->m_FrontSidePlates.m_defaultPlate, bJDFMarksOutput))
			{
				POSITION platePos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
				while ( (platePos || bJDFMarksOutput) && ! m_bCancelJob)
				{
					CPlate& rPlate = (platePos) ? pPrintSheet->m_FrontSidePlates.GetNext(platePos) : pPrintSheet->m_FrontSidePlates.m_defaultPlate;
					if ( ! rPlate.m_bOutputDisabled)
					{
						rPlate.m_OutputDate.m_dt = 0.0f;
						if ( ! (m_bCancelJob = ProcessPlate(&rPlate, bJDFMarksOutput)) )
							rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
					}
					if ( ! platePos)
						break;
				}
				CheckOutputControlFrames(pPrintSheet, FRONTSIDE);

				CloseStream(bSendIfPrinter);
			}

			if (m_nError != 0)
				pPrintSheet->ResetLastOutputDates(FRONTSIDE);

			if (pPrintSheet->GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
				return m_bCancelJob;
		}


	pPrintSheet->ResetLastOutputDates(BACKSIDE);

	if (pPrintSheet->ReadyForOutput(BACKSIDE, bAutoCmd) || bJDFMarksOutput)
		if ( (pPrintSheet->m_BackSidePlates.GetCount()  || bJDFMarksOutput) && ! m_bCancelJob)
		{
			if (OpenStream(pPrintSheet, bOverwriteAll, bSendIfPrinter, BACKSIDE, (pPrintSheet->m_BackSidePlates.GetCount()) ? &pPrintSheet->m_BackSidePlates.GetHead() : &pPrintSheet->m_BackSidePlates.m_defaultPlate, bJDFMarksOutput))
			{
				POSITION platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
				while ( (platePos || bJDFMarksOutput) && ! m_bCancelJob)
				{
					CPlate& rPlate = (platePos) ? pPrintSheet->m_BackSidePlates.GetNext(platePos) : pPrintSheet->m_BackSidePlates.m_defaultPlate;
					if ( ! rPlate.m_bOutputDisabled)
					{
						rPlate.m_OutputDate.m_dt = 0.0f;
						if ( ! (m_bCancelJob = ProcessPlate(&rPlate, bJDFMarksOutput)) )
							rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
					}
					if ( ! platePos)
						break;
				}
				CheckOutputControlFrames(pPrintSheet, BACKSIDE);
	
				CloseStream(bSendIfPrinter);
				if (m_nError != 0)
					pPrintSheet->ResetLastOutputDates(BACKSIDE);
			}
		}

	return m_bCancelJob;
}


BOOL CPDFOutput::ProcessPrintSheetPlatewise(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput) 
{
	pPrintSheet->ResetLastOutputDates(FRONTSIDE);

	if (pPrintSheet->ReadyForOutput(FRONTSIDE, bAutoCmd) || bJDFMarksOutput)
	{
		POSITION platePos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
		while ( (platePos || bJDFMarksOutput) && ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? pPrintSheet->m_FrontSidePlates.GetNext(platePos) : pPrintSheet->m_FrontSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				if (OpenStream(pPrintSheet, bOverwriteAll, bSendIfPrinter, -1, &rPlate, bJDFMarksOutput))
				{
					rPlate.m_OutputDate.m_dt = 0.0f;
					if ( ! (m_bCancelJob = ProcessPlate(&rPlate, bJDFMarksOutput)) )
						rPlate.m_OutputDate = COleDateTime::GetCurrentTime();

					CloseStream(bSendIfPrinter);
				}
			}
			if ( ! platePos)
				break;
		}
		if (m_nError != 0)
			pPrintSheet->ResetLastOutputDates(FRONTSIDE);
	}

	if (pPrintSheet->GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
		return m_bCancelJob;

	pPrintSheet->ResetLastOutputDates(BACKSIDE);

	if (pPrintSheet->ReadyForOutput(BACKSIDE, bAutoCmd) || bJDFMarksOutput)
	{
		POSITION platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
		while ( (platePos || bJDFMarksOutput) && ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? pPrintSheet->m_BackSidePlates.GetNext(platePos) : pPrintSheet->m_BackSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				if (OpenStream(pPrintSheet, bOverwriteAll, bSendIfPrinter, -1, &rPlate, bJDFMarksOutput))
				{
					rPlate.m_OutputDate.m_dt = 0.0f;
					if ( ! (m_bCancelJob = ProcessPlate(&rPlate, bJDFMarksOutput)) )
						rPlate.m_OutputDate = COleDateTime::GetCurrentTime();

					CloseStream(bSendIfPrinter);
				}
			}
			if ( ! platePos)
				break;
		}
		if (m_nError != 0)
			pPrintSheet->ResetLastOutputDates(BACKSIDE);
	}

	return m_bCancelJob;
}

BOOL CPDFOutput::ProcessBookblockJobInOne(CBookblock* pBookblock, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL bSendIfPrinter, BOOL bJDFMarksOutput, BOOL (*pFnProgressCallback)(int nTotalPages, int nCurrentPage, const CString& strFoldSheetNumber, LPVOID lpData)) 
{
	pBookblock->m_OutputDate.m_dt = 0;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return m_bCancelJob;
	if ( ! pDoc->m_PrintSheetList.GetCount())
		return m_bCancelJob;

	int nOutfileRelation = pBookblock->GetOutfileRelation(pBookblock->m_outputTargetProps.m_strOutputFilename);

	if (nOutfileRelation == CBookblock::JobPerFile)
		if ( ! OpenStream(pBookblock, NULL, bOverwriteAll, bSendIfPrinter, ALLSIDES, bJDFMarksOutput))
			return m_bCancelJob;

	int nTotalPages  = pBookblock->GetNumPages(-1, TRUE);
	int nCurrentPage = 0;
	CArray <short*, short*> pageSequence;
	POSITION pos = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem		 = pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		CFoldSheet&		  rFoldSheet = rItem.GetFoldSheet(&(pDoc->m_Bookblock));
		if (rFoldSheet.m_bOutputDisabled)
			continue;
		
		if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
			continue;

		if (nOutfileRelation == CBookblock::FoldSheetPerFile)
			if ( ! OpenStream(pBookblock, &rFoldSheet, bOverwriteAll, bSendIfPrinter, BOTHSIDES, bJDFMarksOutput))
				return m_bCancelJob;

		int nCurrentPageIndex = -1;
		rFoldSheet.GetPageSequence(pageSequence,  -1);
		for (int nPageSeqIndex = 0; nPageSeqIndex < pageSequence.GetSize(); nPageSeqIndex++)
		{
			if (nCurrentPageIndex == *pageSequence[nPageSeqIndex])		// check if pages are nUP -> output only once
				continue;
			nCurrentPageIndex = *pageSequence[nPageSeqIndex];

			if (m_media.m_nAutoAlignTo == CMediaSize::AlignToBookblockDoublePage)
				if (nPageSeqIndex >= pageSequence.GetSize()/2)
					break;

			int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(rFoldSheet.m_nProductPartIndex);
			if (m_media.m_nAutoAlignTo == CMediaSize::AlignToBookblockDoublePage)
			{
				POSITION pos  = pDoc->m_PageTemplateList.FindIndex(*pageSequence[nPageSeqIndex] - 1, -nProductIndex - 1);
				POSITION pos2 = pDoc->m_PageTemplateList.FindIndex(*pageSequence[pageSequence.GetSize() - 1 - nPageSeqIndex] - 1, -nProductIndex - 1);
				ProcessBookblockDoublePage(pos, pos2, bJDFMarksOutput);
				nCurrentPage += 2;
				if (pFnProgressCallback)
					m_bCancelJob = pFnProgressCallback(nTotalPages, nCurrentPage, rFoldSheet.GetNumber(), NULL);
			}
			else
			{
				POSITION pos  = pDoc->m_PageTemplateList.FindIndex(*pageSequence[nPageSeqIndex] - 1, -nProductIndex - 1);
				ProcessBookblockSinglePage(pos, bJDFMarksOutput);
				nCurrentPage++;
				if (pFnProgressCallback)
					m_bCancelJob = pFnProgressCallback(nTotalPages, nCurrentPage, rFoldSheet.GetNumber(), NULL);
			}

			if (m_bCancelJob)
				break;
		}

		if (nOutfileRelation == CBookblock::FoldSheetPerFile)
			CloseStream(bSendIfPrinter, bJDFMarksOutput);

		if (m_bCancelJob)
			break;
	}

	if (nOutfileRelation == CBookblock::JobPerFile)
		CloseStream(bSendIfPrinter, bJDFMarksOutput);

	if ( ! m_bCancelJob)
		pBookblock->m_OutputDate = COleDateTime::GetCurrentTime();
	if (m_nError != 0)
		pBookblock->m_OutputDate.m_dt = 0;
	
	return m_bCancelJob;
}


BOOL CPDFOutput::ProcessBookblockDoublePage(POSITION pos, POSITION pos2, BOOL bJDFMarksOutput)
{
	if ( ! pos)
		return m_bCancelJob;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return m_bCancelJob;

	CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
	CLayoutObject* pLayoutObject = rPageTemplate.GetLayoutObject();
	CPrintSheet*   pPrintSheet   = rPageTemplate.GetPrintSheet();
	if ( ! pLayoutObject || ! pPrintSheet)
		return m_bCancelJob;
	CPlateList&	plateList = (pLayoutObject->GetLayoutSideIndex() == FRONTSIDE) ? pPrintSheet->m_FrontSidePlates : pPrintSheet->m_BackSidePlates;
	CPlate*		pPlate	  = ( ! plateList.GetCount()) ? &plateList.m_defaultPlate : &plateList.GetHead(); 

	m_fMediaWidth  = ( (pLayoutObject->m_nHeadPosition == TOP) || (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaWidth(NULL)  : m_media.GetMediaHeight(NULL);
	m_fMediaHeight = ( (pLayoutObject->m_nHeadPosition == TOP) || (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaHeight(NULL) : m_media.GetMediaWidth(NULL);

	int   nRotation = 0;
	float fXPos, fYPos, fXCenter, fYCenter;
	switch (pLayoutObject->m_nHeadPosition)
	{
	case TOP:		nRotation = 0;
					fXCenter  = (pLayoutObject->m_nIDPosition == RIGHT) ? GetPageEdge(pLayoutObject, pPrintSheet, LEFT)   : GetPageEdge(pLayoutObject, pPrintSheet, RIGHT);
					fYCenter  = pLayoutObject->GetBottom() + pLayoutObject->GetHeight() / 2.0f;
					break;
	case BOTTOM:	nRotation = 180;	
					fXCenter  = (pLayoutObject->m_nIDPosition == LEFT)  ? GetPageEdge(pLayoutObject, pPrintSheet, LEFT)   : GetPageEdge(pLayoutObject, pPrintSheet, RIGHT);
					fYCenter  = pLayoutObject->GetBottom() + pLayoutObject->GetHeight() / 2.0f;
					break;
	case LEFT:		nRotation =  90;	
					fXCenter  = pLayoutObject->GetLeft() + pLayoutObject->GetWidth() / 2.0f;
					fYCenter  = (pLayoutObject->m_nIDPosition == RIGHT) ? GetPageEdge(pLayoutObject, pPrintSheet, BOTTOM) : GetPageEdge(pLayoutObject, pPrintSheet, TOP);
					break;
	case RIGHT:		nRotation = 270;	
					fXCenter  = pLayoutObject->GetLeft() + pLayoutObject->GetWidth() / 2.0f;
					fYCenter  = (pLayoutObject->m_nIDPosition == LEFT)  ? GetPageEdge(pLayoutObject, pPrintSheet, BOTTOM) : GetPageEdge(pLayoutObject, pPrintSheet, TOP);
					break;
	}

	fXPos = fXCenter - m_fMediaWidth  / 2.0f;
	fYPos = fYCenter - m_fMediaHeight / 2.0f;
	IPL_int32_t nSheetIndex = -1;

	if (pos2)
	{
		CPageTemplate& rPageTemplate2 = pDoc->m_PageTemplateList.GetAt(pos2);
		CLayoutObject* pLayoutObject2 = rPageTemplate2.GetLayoutObject();
		CPrintSheet*   pPrintSheet2   = rPageTemplate2.GetPrintSheet();
		if ( ! pLayoutObject2 || ! pPrintSheet2)
			return m_bCancelJob;

		CLayoutObject* pNeighbour = NULL;
		switch (pLayoutObject->m_nHeadPosition)
		{
		case TOP:		pNeighbour = (pLayoutObject->m_nIDPosition == RIGHT) ? ((pLayoutObject->m_LeftNeighbours.GetSize())  ? pLayoutObject->m_LeftNeighbours[0]  : NULL) : ((pLayoutObject->m_RightNeighbours.GetSize()) ? pLayoutObject->m_RightNeighbours[0] : NULL);
						break;
		case BOTTOM:	pNeighbour = (pLayoutObject->m_nIDPosition == LEFT)  ? ((pLayoutObject->m_LeftNeighbours.GetSize())  ? pLayoutObject->m_LeftNeighbours[0]  : NULL) : ((pLayoutObject->m_RightNeighbours.GetSize()) ? pLayoutObject->m_RightNeighbours[0] : NULL);
						break;
		case LEFT:		pNeighbour = (pLayoutObject->m_nIDPosition == RIGHT) ? ((pLayoutObject->m_LowerNeighbours.GetSize()) ? pLayoutObject->m_LowerNeighbours[0] : NULL) : ((pLayoutObject->m_UpperNeighbours.GetSize()) ? pLayoutObject->m_UpperNeighbours[0] : NULL);
						break;
		case RIGHT:		pNeighbour = (pLayoutObject->m_nIDPosition == LEFT)  ? ((pLayoutObject->m_LowerNeighbours.GetSize()) ? pLayoutObject->m_LowerNeighbours[0] : NULL) : ((pLayoutObject->m_UpperNeighbours.GetSize()) ? pLayoutObject->m_UpperNeighbours[0] : NULL);
						break;
		}

		if (pNeighbour)
			if (pNeighbour == pLayoutObject2)
				return ProcessTile(pPlate, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f,1, bJDFMarksOutput, &nSheetIndex, pLayoutObject, pLayoutObject2);

		m_bCancelJob = ProcessTile(pPlate, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f,1, bJDFMarksOutput, &nSheetIndex, pLayoutObject, NULL);
		CPlateList&	plateList2 = (pLayoutObject2->GetLayoutSideIndex() == FRONTSIDE) ? pPrintSheet2->m_FrontSidePlates : pPrintSheet2->m_BackSidePlates;
		CPlate*		pPlate2	   = ( ! plateList2.GetCount()) ? &plateList2.m_defaultPlate : &plateList2.GetHead(); 

		m_fMediaWidth  = ( (pLayoutObject2->m_nHeadPosition == TOP) || (pLayoutObject2->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaWidth(NULL)  : m_media.GetMediaHeight(NULL);
		m_fMediaHeight = ( (pLayoutObject2->m_nHeadPosition == TOP) || (pLayoutObject2->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaHeight(NULL) : m_media.GetMediaWidth(NULL);

		float fXPos, fYPos, fXCenter, fYCenter;
		switch (pLayoutObject2->m_nHeadPosition)
		{
		case TOP:		nRotation = 0;
						fXCenter  = (pLayoutObject2->m_nIDPosition == RIGHT) ? GetPageEdge(pLayoutObject2, pPrintSheet2, LEFT)   : GetPageEdge(pLayoutObject2, pPrintSheet2, RIGHT);
						fYCenter  = pLayoutObject2->GetBottom() + pLayoutObject2->GetHeight() / 2.0f;
						break;
		case BOTTOM:	nRotation = 180;
						fXCenter  = (pLayoutObject2->m_nIDPosition == LEFT)  ? GetPageEdge(pLayoutObject2, pPrintSheet2, LEFT)   : GetPageEdge(pLayoutObject2, pPrintSheet2, RIGHT);
						fYCenter  = pLayoutObject2->GetBottom() + pLayoutObject2->GetHeight() / 2.0f;
						break;
		case LEFT:		nRotation = 90;
						fXCenter  = pLayoutObject2->GetLeft() + pLayoutObject2->GetWidth() / 2.0f;
						fYCenter  = (pLayoutObject2->m_nIDPosition == RIGHT) ? GetPageEdge(pLayoutObject2, pPrintSheet2, BOTTOM) : GetPageEdge(pLayoutObject2, pPrintSheet2, TOP);
						break;
		case RIGHT:		nRotation = 270;
						fXCenter  = pLayoutObject2->GetLeft() + pLayoutObject2->GetWidth() / 2.0f;
						fYCenter  = (pLayoutObject2->m_nIDPosition == LEFT)  ? GetPageEdge(pLayoutObject2, pPrintSheet2, BOTTOM) : GetPageEdge(pLayoutObject2, pPrintSheet2, TOP);
						break;
		}

		fXPos = fXCenter - m_fMediaWidth  / 2.0f;
		fYPos = fYCenter - m_fMediaHeight / 2.0f;
		return ProcessTile(pPlate2, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f,1, bJDFMarksOutput, &nSheetIndex, pLayoutObject2, NULL);
	}

	return ProcessTile(pPlate, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f,1, bJDFMarksOutput, &nSheetIndex, pLayoutObject, NULL);
}

BOOL CPDFOutput::ProcessBookblockSinglePage(POSITION pos, BOOL bJDFMarksOutput)
{
	if ( ! pos)
		return m_bCancelJob;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return m_bCancelJob;

	CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
	CLayoutObject* pLayoutObject = rPageTemplate.GetLayoutObject();
	CPrintSheet*   pPrintSheet   = rPageTemplate.GetPrintSheet();
	if ( ! pLayoutObject || ! pPrintSheet)
		return m_bCancelJob;
	CPlateList&	plateList = (pLayoutObject->GetLayoutSideIndex() == FRONTSIDE) ? pPrintSheet->m_FrontSidePlates : pPrintSheet->m_BackSidePlates;
	CPlate*		pPlate	  = ( ! plateList.GetCount()) ? &plateList.m_defaultPlate : &plateList.GetHead(); 

	m_fMediaWidth  = ( (pLayoutObject->m_nHeadPosition == TOP) || (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaWidth(NULL)  : m_media.GetMediaHeight(NULL);
	m_fMediaHeight = ( (pLayoutObject->m_nHeadPosition == TOP) || (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaHeight(NULL) : m_media.GetMediaWidth(NULL);

	int   nRotation = 0;
	float fXPos, fYPos;
	switch (pLayoutObject->m_nHeadPosition)
	{
	case TOP:		nRotation = 0;
					fXPos	  = (pLayoutObject->m_nIDPosition == RIGHT) ? GetPageEdge(pLayoutObject, pPrintSheet, LEFT)   : (GetPageEdge(pLayoutObject, pPrintSheet, RIGHT)    - m_fMediaWidth);
					fYPos	  = pLayoutObject->GetBottom() + pLayoutObject->GetHeight() / 2.0f - m_fMediaHeight / 2.0f;
					break;
	case BOTTOM:	nRotation = 180;	
					fXPos	  = (pLayoutObject->m_nIDPosition == LEFT) ? GetPageEdge(pLayoutObject, pPrintSheet, LEFT)    : (GetPageEdge(pLayoutObject, pPrintSheet, RIGHT)	   - m_fMediaWidth);
					fYPos	  = pLayoutObject->GetBottom() + pLayoutObject->GetHeight() / 2.0f - m_fMediaHeight / 2.0f;
					break;
	case LEFT:		nRotation =  90;	
					fXPos	  = pLayoutObject->GetLeft()   + pLayoutObject->GetWidth()  / 2.0f - m_fMediaWidth  / 2.0f;
					fYPos	  = (pLayoutObject->m_nIDPosition == RIGHT) ? GetPageEdge(pLayoutObject, pPrintSheet, BOTTOM) : (GetPageEdge(pLayoutObject, pPrintSheet, TOP)	   - m_fMediaHeight);
					break;
	case RIGHT:		nRotation = 270;	
					fXPos	  = pLayoutObject->GetLeft()   + pLayoutObject->GetWidth()  / 2.0f - m_fMediaWidth  / 2.0f;;
					fYPos	  = (pLayoutObject->m_nIDPosition == LEFT)  ? GetPageEdge(pLayoutObject, pPrintSheet, BOTTOM) : (GetPageEdge(pLayoutObject, pPrintSheet, TOP) 	   - m_fMediaHeight);
					break;
	}

	m_bCancelJob = ProcessTile(pPlate, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f,1, bJDFMarksOutput, NULL, pLayoutObject);

	return m_bCancelJob;
}

BOOL CPDFOutput::ProcessFoldSheetsJobInOne(CFoldSheetList* pFoldSheetList, BOOL& bOverwriteAll, BOOL bSendIfPrinter) 
{
	if ( ! pFoldSheetList)
		return m_bCancelJob;
	if (pFoldSheetList->GetSize() <= 0)
		return m_bCancelJob;

	if ( ! OpenStream(&pFoldSheetList->GetHead(), bOverwriteAll, bSendIfPrinter, ALLSIDES, NULL))
		return m_bCancelJob;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return m_bCancelJob;

	POSITION pos = pFoldSheetList->GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pFoldSheetList->GetNext(pos);

		CFoldingProcess& rFoldingProcess = rFoldSheet.GetProductPart().m_foldingParams;

		int   nLayerIndex = 0;
		float fWidth, fHeight; 
		rFoldSheet.GetCutSize(nLayerIndex, fWidth, fHeight);

		CPrintingProfile printingProfile; 
		printingProfile.m_press.m_sheetData.m_fWidth		 = fWidth;
		printingProfile.m_press.m_sheetData.m_fHeight		 = fHeight;
		printingProfile.m_press.m_pressDevice.m_fPlateWidth  = fWidth;
		printingProfile.m_press.m_pressDevice.m_fPlateHeight = fHeight;
		printingProfile.m_press.m_strMarkset				 = rFoldingProcess.m_strMarkset;
		CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.AddBlankSheet(printingProfile);
		CLayout*	 pLayout	 = pPrintSheet->GetLayout();
		
		pPrintSheet->m_nPrintSheetNumber = rFoldSheet.GetIndex() + 1;
		pLayout->MakeNUp(FALSE, *pPrintSheet, &rFoldSheet, nLayerIndex, 1, TOP, NULL, FALSE);
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();

		pPrintSheet->ResetLastOutputDates();
		pPrintSheet->EnableOutput();

		m_media.SetMediaWidth (pLayout->m_FrontSide.m_fPaperWidth);
		m_media.SetMediaHeight(pLayout->m_FrontSide.m_fPaperHeight);

		POSITION platePos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
		while (platePos && ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? pPrintSheet->m_FrontSidePlates.GetNext(platePos) : pPrintSheet->m_FrontSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
			}
			if ( ! platePos)
				break;
		}

		CheckOutputControlFrames(pPrintSheet, FRONTSIDE);

		m_separationInfo.NewLogicalPage();

		platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
		while (platePos && ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? pPrintSheet->m_BackSidePlates.GetNext(platePos) : pPrintSheet->m_BackSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
			}
			if ( ! platePos)
				break;
		}

		CheckOutputControlFrames(pPrintSheet, BACKSIDE);

		if (m_bCancelJob == 2)
			break;
		else
			if (m_bCancelJob == TRUE)
				m_bCancelJob = 0;
	}

	CloseStream(bSendIfPrinter);

	return m_bCancelJob;
}

BOOL CPDFOutput::ProcessFoldSheetAllInOne(CFoldSheet* pFoldSheet, BOOL& bOverwriteAll, BOOL bSendIfPrinter) 
{
	if ( ! pFoldSheet)
		return 0;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CFoldingProcess& rFoldingProcess = pFoldSheet->GetProductPart().m_foldingParams;

	int   nLayerIndex = 0;
	float fWidth, fHeight; 
	pFoldSheet->GetCutSize(nLayerIndex, fWidth, fHeight);

	CPrintingProfile printingProfile; 
	printingProfile.m_press.m_sheetData.m_fWidth		 = fWidth;
	printingProfile.m_press.m_sheetData.m_fHeight		 = fHeight;
	printingProfile.m_press.m_pressDevice.m_fPlateWidth  = fWidth;
	printingProfile.m_press.m_pressDevice.m_fPlateHeight = fHeight;
	printingProfile.m_press.m_strMarkset				 = rFoldingProcess.m_strMarkset;
	CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.AddBlankSheet(printingProfile);
	CLayout*	 pLayout	 = pPrintSheet->GetLayout();
	
	pPrintSheet->m_nPrintSheetNumber = pFoldSheet->GetIndex() + 1;
	pLayout->MakeNUp(FALSE, *pPrintSheet, pFoldSheet, nLayerIndex, 1, TOP, NULL, FALSE);
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();

	pPrintSheet->ResetLastOutputDates();
	pPrintSheet->EnableOutput();

	m_media.SetMediaWidth (pLayout->m_FrontSide.m_fPaperWidth);
	m_media.SetMediaHeight(pLayout->m_FrontSide.m_fPaperHeight);

	if (OpenStream(pFoldSheet, bOverwriteAll, bSendIfPrinter, BOTHSIDES, NULL))
	{
		POSITION platePos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
		while (platePos && ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? pPrintSheet->m_FrontSidePlates.GetNext(platePos) : pPrintSheet->m_FrontSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
			}
			if ( ! platePos)
				break;
		}

		CheckOutputControlFrames(pPrintSheet, FRONTSIDE);

		m_separationInfo.NewLogicalPage();

		platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
		while (platePos && ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? pPrintSheet->m_BackSidePlates.GetNext(platePos) : pPrintSheet->m_BackSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
			}
			if ( ! platePos)
				break;
		}

		CheckOutputControlFrames(pPrintSheet, BACKSIDE);

		CloseStream(bSendIfPrinter);
		if (m_nError != 0)
			pPrintSheet->ResetLastOutputDates();
	}

	return m_bCancelJob;
}

float CPDFOutput::GetPageEdge(CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet, int nSide)
{
	CLayoutObject* pNeighbour = NULL;
	switch (nSide)
	{
	case LEFT:		pNeighbour = (pLayoutObject->m_LeftNeighbours.GetSize()) ? pLayoutObject->m_LeftNeighbours[0] : NULL;
					if (pNeighbour)
						if (pNeighbour->m_nComponentRefIndex != pLayoutObject->m_nComponentRefIndex)
							pNeighbour = NULL;
					return (pNeighbour) ? pLayoutObject->GetLeftWithBorder() : pLayoutObject->GetLeft() - pLayoutObject->GetCurrentGrossMargin(pPrintSheet, LEFT);

	case RIGHT:		pNeighbour = (pLayoutObject->m_RightNeighbours.GetSize()) ? pLayoutObject->m_RightNeighbours[0] : NULL;
					if (pNeighbour)
						if (pNeighbour->m_nComponentRefIndex != pLayoutObject->m_nComponentRefIndex)
							pNeighbour = NULL;
					return (pNeighbour) ? pLayoutObject->GetRightWithBorder() : pLayoutObject->GetRight() + pLayoutObject->GetCurrentGrossMargin(pPrintSheet, RIGHT);

	case BOTTOM:	pNeighbour = (pLayoutObject->m_LowerNeighbours.GetSize()) ? pLayoutObject->m_LowerNeighbours[0] : NULL;
					if (pNeighbour)
						if (pNeighbour->m_nComponentRefIndex != pLayoutObject->m_nComponentRefIndex)
							pNeighbour = NULL;
					return (pNeighbour) ? pLayoutObject->GetBottomWithBorder() : pLayoutObject->GetBottom() - pLayoutObject->GetCurrentGrossMargin(pPrintSheet, BOTTOM);

	case TOP:		pNeighbour = (pLayoutObject->m_UpperNeighbours.GetSize()) ? pLayoutObject->m_UpperNeighbours[0] : NULL;
					if (pNeighbour)
						if (pNeighbour->m_nComponentRefIndex != pLayoutObject->m_nComponentRefIndex)
							pNeighbour = NULL;
					return (pNeighbour) ? pLayoutObject->GetTopWithBorder() : pLayoutObject->GetTop() + pLayoutObject->GetCurrentGrossMargin(pPrintSheet, TOP);
	}

	return 0.0f;
}

void CPDFOutput::CheckOutputControlFrames(CPrintSheet* pPrintSheet, int nSide)
{
	if ( ! m_media.m_bPageFrame && ! m_media.m_bMarkFrame && ! m_media.m_bSheetFrame && ! m_media.m_bPlateFrame )
		return;
	if ( ! pPrintSheet->GetNumOutputObjects(nSide))
		return;

	CPlateList& rPlateList = (nSide == FRONTSIDE) ? pPrintSheet->m_FrontSidePlates : pPrintSheet->m_BackSidePlates;
	BOOL bDidOutputSeparated = FALSE;
	POSITION pos = rPlateList.GetHeadPosition();
	while (pos)
	{
		CPlate& rPlate = rPlateList.GetNext(pos);
		if (rPlate.m_strPlateName.CompareNoCase(_T("Composite")) != 0)
			if ( ! rPlate.m_bOutputDisabled)
				bDidOutputSeparated = TRUE;
	}
	if ( ! bDidOutputSeparated)
		return;

	for (int i = 0; i < m_media.m_frameSource.m_PageSourceHeaders.GetSize(); i++)
	{
		if (m_media.m_frameSource.m_PageSourceHeaders[i].GetColorType() == CColorDefinition::Spot)
			if ( ! rPlateList.PlateExists(m_media.m_frameSource.m_PageSourceHeaders[i].m_strColorName))
			{
				CPlate plate;
				plate.m_strPlateName = m_media.m_frameSource.m_PageSourceHeaders[i].m_strColorName;
				rPlateList.AddTail(plate);

				m_fMediaWidth  = m_media.GetMediaWidth(pPrintSheet);
				m_fMediaHeight = m_media.GetMediaHeight(pPrintSheet);

				for (int nTileIndex = 0; (nTileIndex < m_media.m_posInfos.GetSize()) && ! m_bCancelJob; nTileIndex++)
				{
					float fXPos, fYPos;
					m_media.m_posInfos[nTileIndex].m_reflinePosParams.GetSheetPosition(pPrintSheet, nSide, fXPos, fYPos, m_fMediaWidth, m_fMediaHeight);
					m_bCancelJob = ProcessControlFrames(pPrintSheet, -1, &rPlateList.GetTail(), FALSE, fXPos, fYPos, 
														m_media.m_posInfos[nTileIndex].m_nRotation, m_media.m_posInfos[nTileIndex].m_fCorrectionX, m_media.m_posInfos[nTileIndex].m_fCorrectionY, nTileIndex + 1);
				}
				rPlateList.RemoveAt(rPlateList.GetTailPosition());
			}
	}
}

BOOL CPDFOutput::ProcessPlate(CPlate* pPlate, BOOL bJDFMarksOutput) 
{
	if (!pPlate)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}

	CPrintSheet* pPrintSheet = pPlate->GetPrintSheet();
	if (!pPrintSheet)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}

//	theApp.m_pDlgPDFLibStatus->UpdateStatus(CDlgPDFLibStatus::OutputPlate,  0, pPlate->GetLayoutSide()->m_strSideName, pPlate->m_strPlateName);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pPrintSheetView && ! bJDFMarksOutput)
		if (pPlate->GetPlateList())
			if (pPlate->GetPlateList()->GetCount() > 1)	// switch plate only if preseparated job (more than 1 plate)
			{
				pPlate->SetTopmostPlate();
				pPrintSheetView->Invalidate();
				pPrintSheetView->UpdateWindow();
			}

	m_fMediaWidth  = m_media.GetMediaWidth(pPrintSheet);
	m_fMediaHeight = m_media.GetMediaHeight(pPrintSheet);

	for (int nTileIndex = 0; (nTileIndex < m_media.m_posInfos.GetSize()) && ! m_bCancelJob; nTileIndex++)
	{
		float fXPos, fYPos;
		m_media.m_posInfos[nTileIndex].m_reflinePosParams.GetSheetPosition(pPrintSheet, pPlate->GetSheetSide(), fXPos, fYPos, m_fMediaWidth, m_fMediaHeight);
		m_bCancelJob = ProcessTile(pPlate, (m_media.m_posInfos.GetSize() > 1) ? TRUE : FALSE, fXPos, fYPos, 
								   m_media.m_posInfos[nTileIndex].m_nRotation, 
								   m_media.m_posInfos[nTileIndex].m_fCorrectionX, 
								   m_media.m_posInfos[nTileIndex].m_fCorrectionY, 
								   nTileIndex + 1, bJDFMarksOutput);
	}

	if ( ! m_bCancelJob)
		m_nPlatesProcessed++;

	return m_bCancelJob;
}
//                        0123456789012345678901234
const CString DEMOSTRING("yoxxIOezQmbDzzKbbMxyzIIJ ");
#define GETDEMOSTRING() (CString(CString(DEMOSTRING[14]) + CString(DEMOSTRING[21]) + CString(DEMOSTRING[17]) + CString(DEMOSTRING[24]) + \
								 CString(DEMOSTRING[11]) + CString(DEMOSTRING[6])  + CString(DEMOSTRING[9])  + CString(DEMOSTRING[1])))


BOOL CPDFOutput::ProcessTile(CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile, BOOL bJDFMarksOutput, IPL_int32_t* pnSheetIndex, CLayoutObject* pFilterObject1, CLayoutObject* pFilterObject2)
{
	if (!pPlate)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}
	CPrintSheet* pPrintSheet = pPlate->GetPrintSheet();
	CLayout*	 pLayout	 = pPrintSheet->GetLayout();
	if ( ! pPrintSheet || ! pLayout)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}

	IPL_int32_t nSheetIndex = (pnSheetIndex) ? *pnSheetIndex : -1;
	if (nSheetIndex == -1)
	{
		float fWidth  = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaWidth  : m_fMediaHeight;
		float fHeight = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaHeight : m_fMediaWidth;
		IPL_TDRECT  mediaBox = { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };
		IPL_TDRECT  cropBox	 = { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };

		IPL_int32_t nResult = IPL_PDFOutputStream_AddSheet(m_hPDFFile, &nSheetIndex, &mediaBox, &cropBox, &mediaBox, &mediaBox, &mediaBox);
		if (pnSheetIndex)
			*pnSheetIndex = nSheetIndex;
		if (nResult)
		{
			CString			strMsg;
			PTB_uint32_t	nBufSize = 1024;
			IPL_utf8_char_t szMsgUTF8[1024];
			IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
			PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();

			KIMOpenLogMessage(strMsg, MB_ICONERROR);

			m_nError = CImpManApp::PDFOutputError;
			return TRUE;
		}
	}

	if ( ! m_bCalibration)
	{
		m_separationInfo.Add(pPlate->m_strPlateName, nSheetIndex);

		EmptySysGenStream();

		m_bCancelJob = FALSE;
		if ( ! bJDFMarksOutput)
			m_bCancelJob = ProcessExposures(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::Page, -1, -1, FALSE, _T(""), pFilterObject1, pFilterObject2);

		for (int i = pLayout->m_markList.GetSize() - 1; (i >= 0) && ! m_bCancelJob; i--)
		{
			EmptySysGenStream();
			CString strMarkType = _T("");
			BOOL	bOverprint  = TRUE;
			switch (pLayout->m_markList[i]->m_nType)
			{
			case CMark::CustomMark:																			break;
			case CMark::TextMark:		strMarkType = _T("KIM_GeneratedTextMark");							break;
			case CMark::SectionMark:	strMarkType = _T("KIM_GeneratedSectionMark");	bOverprint = FALSE;	break;
			case CMark::CropMark:		strMarkType = _T("KIM_GeneratedCropMarks");							break;	
			case CMark::FoldMark:		strMarkType = _T("KIM_GeneratedFoldMarks");							break;
			case CMark::BarcodeMark:	strMarkType = _T("KIM_GeneratedBarcodeMark");						break;
			case CMark::DrawingMark:	strMarkType = _T("KIM_GeneratedDrawingMark");						break;
			}
			m_bCancelJob = ProcessExposures(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::ControlMark, pLayout->m_markList[i]->m_nType, -1, bJDFMarksOutput, pLayout->m_markList[i]->m_strMarkName);
			if ( ! strMarkType.IsEmpty())
			{
				AddSysGenStream(nSheetIndex, strMarkType, bOverprint);
				EmptySysGenStream();
			}
		}
		if ( ! m_bCancelJob)
			m_bCancelJob = ProcessKIM5Marks(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, bJDFMarksOutput);
	}

	if ( ! m_bCancelJob)
		m_bCancelJob = ProcessControlFrames(pPrintSheet, nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, pFilterObject1, pFilterObject2);

	if ( ! m_bCancelJob)
		m_bCancelJob = ProcessGraphics(pPrintSheet, nSheetIndex, pPlate, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile);

	if ( ! m_bCancelJob)
	{
		if (bJDFMarksOutput)
		{
			if ( ! (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp))
			{
				m_strSysGenMarksStream.Empty();
				int   nStrLen = GETDEMOSTRING().GetLength();
				float fTextBoxWidth		  = m_media.GetMediaWidth(pPrintSheet) / 2.0f;
				float fTextBoxHeight	  = fTextBoxWidth/nStrLen;
				float fSheetClipBoxLeft	  = fXPos + m_media.GetMediaWidth(pPrintSheet) /2.0f - fTextBoxWidth /2.0f;
				float fSheetClipBoxBottom = fYPos + m_media.GetMediaHeight(pPrintSheet)/2.0f - fTextBoxHeight/2.0f;
				float fSheetClipBoxRight  = fSheetClipBoxLeft   + fTextBoxWidth;
				float fSheetClipBoxTop	  = fSheetClipBoxBottom + fTextBoxHeight;

				float fSheetExposureCenterX = fSheetClipBoxLeft   + fTextBoxWidth /2.0f;
				float fSheetExposureCenterY = fSheetClipBoxBottom + fTextBoxHeight/2.0f;
				int nHeadPosition = TOP;
				TransformToOutputMedia(nRotation, fCorrectionX, fCorrectionY, m_fMediaWidth, m_fMediaHeight, 
									   fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop, 
									   fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition);
				Text2Stream(pPlate, nTile, GETDEMOSTRING(), fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition, 
																fTextBoxWidth / 2.0f, fTextBoxHeight / 2.0f, fTextBoxHeight, 
																FALSE, 0.1f, 0.1f, 0.1f, 0.1f);
				PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, "KIM_GeneratedTextMarks");
				m_strSysGenMarksStream.Empty();
			}
		}
	}

	return m_bCancelJob;
}

BOOL CPDFOutput::ProcessKIM5Marks(IPL_int32_t nSheetIndex, CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile, BOOL bJDFMarksOutput)
{
	m_strSysGenMarksStream.Empty();

	m_bCancelJob = FALSE;
	if ( ! m_bCancelJob)	// output foldmarks first if they are in background
	{
		m_bCancelJob = ProcessExposures(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::ControlMark, CMark::FoldMark,	  CFoldMark::Background, bJDFMarksOutput);
		if ( ! m_strSysGenMarksStream.IsEmpty())
		{
			PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, "KIM_GeneratedFoldMarks");
			m_strSysGenMarksStream.Empty();
		}
	}
	if ( ! m_bCancelJob)
	{
		m_bCancelJob = ProcessExposures(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::ControlMark, CMark::CropMark,	  CFoldMark::Background, bJDFMarksOutput);
		if ( ! m_strSysGenMarksStream.IsEmpty())
		{
			PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, "KIM_GeneratedCropMarks");
			m_strSysGenMarksStream.Empty();
		}
	}
	if ( ! m_bCancelJob)
	{
		m_bCancelJob = ProcessExposures(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::ControlMark, CMark::TextMark,	  CFoldMark::Background, bJDFMarksOutput);
		if ( ! m_strSysGenMarksStream.IsEmpty())
		{
			PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, "KIM_GeneratedTextMarks");
			m_strSysGenMarksStream.Empty();
		}
	}
	if ( ! m_bCancelJob)
	{
		m_bCancelJob = ProcessExposures(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::ControlMark, CMark::BarcodeMark, CFoldMark::Background, bJDFMarksOutput);
		if ( ! m_strSysGenMarksStream.IsEmpty())
		{
			PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, "KIM_GeneratedBarcodeMark");
			m_strSysGenMarksStream.Empty();
		}
	}
	if ( ! m_bCancelJob)
	{
		m_bCancelJob = ProcessExposures(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::ControlMark, CMark::SectionMark, CFoldMark::Background, bJDFMarksOutput);
		if ( ! m_strSysGenMarksStream.IsEmpty())
		{
			PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, "KIM_GeneratedSectionMarks", FALSE);
			m_strSysGenMarksStream.Empty();
		}
	}
	if ( ! m_bCancelJob)
		m_bCancelJob = ProcessExposures(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::ControlMark, CMark::CustomMark,  CFoldMark::Background, bJDFMarksOutput);
	if ( ! m_bCancelJob)	// output foldmarks last if they are in foreground
	{
		m_strSysGenMarksStream.Empty();
		m_bCancelJob = ProcessExposures(nSheetIndex, pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::ControlMark, CMark::FoldMark,	  CFoldMark::Foreground, bJDFMarksOutput);
		if ( ! m_strSysGenMarksStream.IsEmpty())
		{
			PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, "KIM_GeneratedFoldMarks");
			m_strSysGenMarksStream.Empty();
		}
	}

	return m_bCancelJob;
}

void CPDFOutput::AddSysGenStream(int nSheetIndex, const CString& strID, BOOL bOverprint)
{
	if ( ! m_strSysGenMarksStream.IsEmpty())
		PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, strID, bOverprint);

	for (int i = 0; i < m_addSysGenSpotColorStreams.GetSize(); i++)
		if (m_addSysGenSpotColorStreams[i].m_strSpotColorName != _T("PROCESSWHITE"))
			if ( ! m_addSysGenSpotColorStreams[i].m_strPDFCode.IsEmpty())
				PDFSheet_AddSysGenStream3(nSheetIndex, m_addSysGenSpotColorStreams[i].m_strPDFCode, m_addSysGenSpotColorStreams[i].m_strSpotColorName, m_addSysGenSpotColorStreams[i].m_crSpotColorAlternate, m_addSysGenSpotColorStreams[i].m_fSpotColorTintValue, strID);	

	for (int i = 0; i < m_addSysGenSpotColorStreams.GetSize(); i++)
		if (m_addSysGenSpotColorStreams[i].m_strSpotColorName == _T("PROCESSWHITE"))	// workaround for section marks where we need white text over bars which has been rendered in spotcolors before
			if ( ! m_addSysGenSpotColorStreams[i].m_strPDFCode.IsEmpty())
			{
				CString strPDFCode1;
				strPDFCode1.Format(_T("q n %f %f %f %f k %f %f %f %f K\n"), 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
				PDFSheet_AddSysGenStream(nSheetIndex, strPDFCode1 + m_addSysGenSpotColorStreams[i].m_strPDFCode, strID, bOverprint);
			}
}

void CPDFOutput::EmptySysGenStream()
{
	m_strSysGenMarksStream.Empty();

	m_bSysGenMarksColorSpaceALL = FALSE;

	for (int i = 0; i < m_addSysGenSpotColorStreams.GetSize(); i++)
		m_addSysGenSpotColorStreams[i].m_strPDFCode.Empty();
	m_addSysGenSpotColorStreams.RemoveAll();
}

BOOL CPDFOutput::ProcessExposures(IPL_int32_t nSheetIndex, CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile, BYTE nObjectType, int nMarkType, int nZOrder, BOOL bJDFMarksOutput, 
								  CString strMarkName, CLayoutObject* pFilterObject1, CLayoutObject* pFilterObject2)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;
	if ( ! pPlate)
		return TRUE;

	CPrintSheet* pPrintSheet = pPlate->GetPrintSheet();
	if ( ! pPrintSheet)
		return TRUE;

	int			 nActLayoutSide = pPlate->GetSheetSide();
	POSITION	 pos			= pPlate->m_ExposureSequence.GetHeadPosition();
	CString		 strColorName	= pPlate->m_strPlateName;
	CString		 strSideName	= (pPlate->GetLayoutSide()) ? pPlate->GetLayoutSide()->m_strSideName : "";

	srand( (unsigned)time( NULL ) );
	int nRan = rand();
	int nNumPages = pPrintSheet->GetNumPages(nActLayoutSide);
	int nRandomPageIndex = (nNumPages > 0) ? nRan % nNumPages : 0;

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CDC* pDCPrintSheetView = (pPrintSheetView) ? pPrintSheetView->GetDC() : NULL;

	CLayout* pLayout = pPrintSheet->GetLayout();

	int nIndex = 0;
	while (pos)
	{
		CExposure&	   rExposure = pPlate->m_ExposureSequence.GetNext(pos);
		//CLayoutObject* pObject	 = rExposure.GetLayoutObject(pPrintSheet->m_nLayoutIndex, nActLayoutSide);
		CLayoutObject* pObject	 = rExposure.GetLayoutObject(pLayout, nActLayoutSide);
		if (!pObject)
			continue;
		if (pObject->m_nType != nObjectType)
			continue;
		if (pObject->IsAutoMarkDisabled(pPrintSheet, nActLayoutSide))
			continue;
		if (nObjectType == CLayoutObject::ControlMark)
			if ( ! strMarkName.IsEmpty())
			{
				if (pObject->m_strFormatName != strMarkName)
					continue;
			}
			else
				if (pPrintSheet->GetLayout()->m_markList.FindMark(pObject->m_strFormatName))	// for compatibilty to KIM5 marks: if mark is not in mark list it is a KIM5 mark -> so draw it
					continue;
		if (pFilterObject1 && pFilterObject2)
			if ( (pObject != pFilterObject1) && (pObject != pFilterObject2) )
				continue;
		if (pFilterObject1 && ! pFilterObject2)
			if (pObject != pFilterObject1)
				continue;
		if ( ! pFilterObject1 && pFilterObject2)
			if (pObject != pFilterObject2)
				continue;

		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPrintSheet);
		if (pObjectTemplate)
		{
			nIndex++;

			int nColorDefinitionIndex = pPlate->m_nColorDefinitionIndex;
			if (bJDFMarksOutput)
				if ( (nColorDefinitionIndex == -1) && (pObject->m_nType == CLayoutObject::ControlMark) )	// if no plates, output 1st sep of marks
					nColorDefinitionIndex = (pObjectTemplate->m_ColorSeparations.GetSize()) ? pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1;

			CPageSource*		pPageSource		  = pObjectTemplate->GetObjectSource	  (nColorDefinitionIndex, TRUE, pPrintSheet, nActLayoutSide);
			CPageSourceHeader*	pPageSourceHeader = pObjectTemplate->GetObjectSourceHeader(nColorDefinitionIndex, TRUE, pPrintSheet, nActLayoutSide);
			SEPARATIONINFO*		pSepInfo		  = pObjectTemplate->GetSeparationInfo	  (nColorDefinitionIndex, TRUE, pPrintSheet, nActLayoutSide);

			if (pPageSource && pSepInfo)
			{
				float fSheetClipBoxLeft	  = rExposure.GetSheetBleedBox(LEFT,   pObject, pObjectTemplate) - fXPos;
				float fSheetClipBoxBottom = rExposure.GetSheetBleedBox(BOTTOM, pObject, pObjectTemplate) - fYPos;
				float fSheetClipBoxRight  = rExposure.GetSheetBleedBox(RIGHT,  pObject, pObjectTemplate) - fXPos;
				float fSheetClipBoxTop	  = rExposure.GetSheetBleedBox(TOP,	   pObject, pObjectTemplate) - fYPos;

				pObjectTemplate->GetGridClipBox(fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop);

				float fSheetExposureCenterX = rExposure.GetContentTrimBoxCenterX(pObject, pObjectTemplate) - fXPos;
				float fSheetExposureCenterY = rExposure.GetContentTrimBoxCenterY(pObject, pObjectTemplate) - fYPos;
				switch (pObject->m_nHeadPosition)
				{
				case TOP:		fSheetExposureCenterX += pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, pObject, pPrintSheet);	
								fSheetExposureCenterY += pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, pObject, pPrintSheet);	
								break;
				case LEFT:		fSheetExposureCenterX -= pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, pObject, pPrintSheet);	
								fSheetExposureCenterY += pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, pObject, pPrintSheet);	
								break;
				case BOTTOM:	fSheetExposureCenterX -= pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, pObject, pPrintSheet);	
								fSheetExposureCenterY -= pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, pObject, pPrintSheet);	
								break;
				case RIGHT:		fSheetExposureCenterX += pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, pObject, pPrintSheet);	
								fSheetExposureCenterY -= pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, pObject, pPrintSheet);	
								break;
				}

				int nHeadPosition = pObject->m_nHeadPosition;
				TransformToOutputMedia(nRotation, fCorrectionX, fCorrectionY, m_fMediaWidth, m_fMediaHeight, 
									   fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop, 
									   fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition);

				m_bOutputFormatExceeded = CDlgPDFOutputMediaSettings::OutputFormatExceeded(m_fMediaWidth, m_fMediaHeight);

				if (bTiling && (pObject->m_nType == CLayoutObject::Page) )
				{
					float fSheetTrimBoxLeft	  = rExposure.GetSheetTrimBox(LEFT,   pObject, pObjectTemplate) - fXPos;
					float fSheetTrimBoxBottom = rExposure.GetSheetTrimBox(BOTTOM, pObject, pObjectTemplate) - fYPos;
					float fSheetTrimBoxRight  = rExposure.GetSheetTrimBox(RIGHT,  pObject, pObjectTemplate) - fXPos;
					float fSheetTrimBoxTop	  = rExposure.GetSheetTrimBox(TOP,	  pObject, pObjectTemplate) - fYPos;
					TransformToOutputMedia(nRotation, fCorrectionX, fCorrectionY, m_fMediaWidth, m_fMediaHeight, fSheetTrimBoxLeft, fSheetTrimBoxBottom, fSheetTrimBoxRight, fSheetTrimBoxTop);
					float fWidth  = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaWidth  : m_fMediaHeight;
					float fHeight = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaHeight : m_fMediaWidth ;
					if ((fSheetTrimBoxLeft < -0.1f) || (fSheetTrimBoxBottom < -0.1f))
						continue;
					if ((fSheetTrimBoxRight > fWidth  + 0.1f) || (fSheetTrimBoxTop > fHeight + 0.1f))
						continue;
				}


				if (pPageSource->m_nType == CPageSource::SystemCreated)
					SystemCreatedContent2Stream(nSheetIndex, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition,
														     fSheetClipBoxLeft,     fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop,
											 				 pObject, pObjectTemplate, pPageSource, pPrintSheet, pPlate, nTile);

				else
				{	
					CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
					if (pPageSource->DocumentExists(pPrintSheet, (pObject->m_nType == CLayoutObject::Page) ? TRUE : FALSE))
					{
						if ( (bJDFMarksOutput != 2) && (m_media.m_nAutoAlignTo != CMediaSize::AlignToBookblockSinglePage) && (m_media.m_nAutoAlignTo != CMediaSize::AlignToBookblockDoublePage) )
						{
							// feedback to CPrintSheetView
							if (pDCPrintSheetView)
							{
								CDisplayItem* pDI = pPrintSheetView->m_DisplayList.GetItemFromData(&rExposure, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
								if ( ! pDI)
									pDI = pPrintSheetView->m_DisplayList.GetItemFromData(pObject, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
								if (pDI) 
									CPrintSheetView::FillTransparent(pDCPrintSheetView, pDI->m_BoundingRect, LIGHTGREEN, TRUE);
							}
						}

						float fObjectBBoxCenterX = pPageSource->GetBBox(LEFT,   pPageSourceHeader) + ( pPageSource->GetBBox(RIGHT, pPageSourceHeader) - pPageSource->GetBBox(LEFT,   pPageSourceHeader) ) / 2.0f;
						float fObjectBBoxCenterY = pPageSource->GetBBox(BOTTOM, pPageSourceHeader) + ( pPageSource->GetBBox(TOP,   pPageSourceHeader) - pPageSource->GetBBox(BOTTOM, pPageSourceHeader) ) / 2.0f;

						IPL_TOBJECTID	objectID   = {sizeof(IPL_TOBJECTID), "", pSepInfo->m_nPageNumInSourceFile, "", ""};
						CString			strPDFFile = pPageSource->m_strDocumentFullpath;
						TCHAR			szDest[1024];
						CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strPDFFile, pPrintSheet, NULL, -1);
						CSysToUTF8Converter s2utf8;
						_tcscpy_s((TCHAR*)objectID.pdffile, IPL_MAX_LENGTH, (TCHAR*)s2utf8.Convert(szDest));

						if (bJDFMarksOutput != 2)
						{
							// feedback to CPrintSheetTableView
							if (pPrintSheetTableView)
							{
								if ( (pPlate->m_strPlateName == "Composite") || (pPlate->m_strPlateName == _T("")) )
									pPrintSheetTableView->m_strCurrentOutputPage = pObjectTemplate->m_strPageID;
								else
									pPrintSheetTableView->m_strCurrentOutputPage.Format(_T("%s (%s)"), pObjectTemplate->m_strPageID, strColorName);
									pPrintSheetTableView->UpdateData(FALSE);
							}
						}

	//					theApp.m_pDlgPDFLibStatus->UpdateStatus(CDlgPDFLibStatus::OutputPage, (pos) ? 0 : 1, pObjectTemplate->m_strPageID);

						m_spotColorMapper.Initialize(pObjectTemplate, pObject, pPlate, pPageSourceHeader);

						float fBottlingAngle   = 0.0f, fBottlingOffsetX = 0.0f, fBottlingOffsetY = 0.0f;
						CalcBottlingParams(fBottlingAngle, fBottlingOffsetX, fBottlingOffsetY, pObject, pObjectTemplate, fObjectBBoxCenterX, fObjectBBoxCenterY);
						float fContentScaleX = pObjectTemplate->m_fContentScaleX * pObjectTemplate->CalcShinglingScaleX();
						float fContentScaleY = pObjectTemplate->m_fContentScaleY * pObjectTemplate->CalcShinglingScaleY();

						if (AddPage(nSheetIndex, &objectID, pObject, pObjectTemplate, pPageSource, pPageSourceHeader,	fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition, 
																														fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop,
																														fObjectBBoxCenterX, fObjectBBoxCenterY,
																														pObjectTemplate->m_fContentRotation, fContentScaleX, fContentScaleY,
																														fBottlingAngle, fBottlingOffsetX, fBottlingOffsetY, pObject->m_nType, (nRandomPageIndex == 0), bJDFMarksOutput ) )
							return m_bCancelJob; // break

						nRandomPageIndex--;
					}

					if (bJDFMarksOutput != 2)
						if (pPrintSheetTableView)
							pPrintSheetTableView->m_pdfOutputProgress.SetPos(m_nCurrentPage++);	// no progress feedback for system created marks, because this is too fast, so makes no sense
				}		
			}
		}
	}

	return FALSE;
}

BOOL CPDFOutput::ProcessControlFrames(CPrintSheet* pPrintSheet, IPL_int32_t nSheetIndex, CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile,
									  CLayoutObject* pFilterObject1, CLayoutObject* pFilterObject2)
{
	if (nSheetIndex == -1)	// called from inside CheckOutputControlFrames()
	{
		float fWidth  = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaWidth  : m_fMediaHeight;
		float fHeight = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaHeight : m_fMediaWidth;
		IPL_TDRECT mediaBox	= { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };
		IPL_TDRECT cropBox	= { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };

		IPL_int32_t nResult = IPL_PDFOutputStream_AddSheet(m_hPDFFile, &nSheetIndex, &mediaBox, &cropBox, &mediaBox, &mediaBox, &mediaBox);
		if (nResult)
		{
			CString			strMsg;
			PTB_uint32_t	nBufSize = 1024;
			IPL_utf8_char_t szMsgUTF8[1024];
			IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
			PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();

			KIMOpenLogMessage(strMsg, MB_ICONERROR);

			m_nError = CImpManApp::PDFOutputError;
			return TRUE;
		}

		m_separationInfo.Add(pPlate->m_strPlateName, nSheetIndex);
	}

	CLayoutSide* pLayoutSide = pPlate->GetLayoutSide();
	if (pLayoutSide)
	{
		// Output page/mark frames for testing purposes
		CLayoutObjectList* pObjectList = &pLayoutSide->m_ObjectList;
		POSITION pos = pObjectList->GetHeadPosition();	
		while (pos)
		{
			CLayoutObject& rObject = pObjectList->GetNext(pos);

			if (rObject.IsAutoMarkDisabled(pPrintSheet, pLayoutSide->m_nLayoutSide))
				continue;

			if (pFilterObject1 && pFilterObject2)
				if ( (&rObject != pFilterObject1) && (&rObject != pFilterObject2) )
					continue;
			if (pFilterObject1 && ! pFilterObject2)
				if (&rObject != pFilterObject1)
					continue;
			if ( ! pFilterObject1 && pFilterObject2)
				if (&rObject != pFilterObject2)
					continue;

			if ( ((rObject.m_nType == CLayoutObject::Page)		  && m_media.m_bPageFrame) ||
				 ((rObject.m_nType == CLayoutObject::ControlMark) && m_media.m_bMarkFrame) || m_bCalibration)
			{
				float fFrameLeft	= rObject.GetLeft() - fXPos, fFrameBottom = rObject.GetBottom() - fYPos;
				float fFrameRight	= fFrameLeft + rObject.GetWidth(), fFrameTop = fFrameBottom + rObject.GetHeight();
				float fFrameCenterX = fFrameLeft   + (fFrameRight - fFrameLeft)/2.0f;
				float fFrameCenterY = fFrameBottom + (fFrameTop   - fFrameBottom)/2.0f;
				int   nHeadPosition = rObject.m_nHeadPosition;
				TransformToOutputMedia(nRotation, fCorrectionX, fCorrectionY, m_fMediaWidth, m_fMediaHeight, fFrameLeft, fFrameBottom, fFrameRight, fFrameTop, fFrameCenterX, fFrameCenterY, nHeadPosition); 

				if (bTiling && (rObject.m_nType == CLayoutObject::Page) )
				{
					float fWidth  = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaWidth  : m_fMediaHeight;
					float fHeight = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaHeight : m_fMediaWidth ;
					if ((fFrameLeft < -0.1f) || (fFrameBottom < -0.1f))
						continue;
					if ((fFrameRight > fWidth  + 0.1f) || (fFrameTop > fHeight + 0.1f))
						continue;
				}

				Frame2Stream(nSheetIndex, fFrameLeft, fFrameBottom, fFrameRight - fFrameLeft, fFrameTop - fFrameBottom, pPlate, m_media.m_frameSource);
				if (m_bCalibration && (rObject.m_nType == CLayoutObject::Page) )
				{
					CPageTemplate* pObjectTemplate = (rObject.m_nType == CLayoutObject::ControlMark) ?
													  rObject.GetCurrentMarkTemplate() :
													  rObject.GetCurrentPageTemplate(pPrintSheet);
					if (pObjectTemplate)
						Text2Stream(pPlate, nTile, pObjectTemplate->m_strPageID, fFrameCenterX, fFrameCenterY, nHeadPosition, 
												 10.0f, 5.0f, 10.0f, FALSE, 1.0f, 1.0f, 1.0f, 1.0f);
				}
			}
		}

		// Output plate frame for testing purposes
		if (m_media.m_bPlateFrame || m_bCalibration)	
		{
			CPressDevice* pPressDevice = pLayoutSide->GetPressDevice();
			if (pPressDevice)
			{
				float fFrameLeft	= -fXPos, fFrameBottom = -fYPos;
				float fFrameRight	= fFrameLeft + pPressDevice->m_fPlateWidth, fFrameTop = fFrameBottom + pPressDevice->m_fPlateHeight;
				TransformToOutputMedia(nRotation, fCorrectionX, fCorrectionY, m_fMediaWidth, m_fMediaHeight, fFrameLeft, fFrameBottom, fFrameRight, fFrameTop); 
				Frame2Stream(nSheetIndex, fFrameLeft, fFrameBottom, fFrameRight - fFrameLeft, fFrameTop - fFrameBottom, pPlate, m_media.m_frameSource);
			}
		}
		
		// Output sheet frame for testing purposes
		if (m_media.m_bSheetFrame || m_bCalibration)	
		{
			float fFrameLeft	= pLayoutSide->m_fPaperLeft - fXPos, fFrameBottom = pLayoutSide->m_fPaperBottom - fYPos;
			float fFrameRight	= fFrameLeft + pLayoutSide->m_fPaperWidth, fFrameTop = fFrameBottom + pLayoutSide->m_fPaperHeight;
			TransformToOutputMedia(nRotation, fCorrectionX, fCorrectionY, m_fMediaWidth, m_fMediaHeight, fFrameLeft, fFrameBottom, fFrameRight, fFrameTop); 
			Frame2Stream(nSheetIndex, fFrameLeft, fFrameBottom, fFrameRight - fFrameLeft, fFrameTop - fFrameBottom, pPlate, m_media.m_frameSource);
		}

		if (m_bCalibration)
		{
			CString strGripper;
			strGripper.Format(IDS_GRIPPER, "");
			float fFrameLeft	= 0.0f, fFrameBottom = max(pLayoutSide->m_fPaperBottom - fYPos, 10.0f);
			float fFrameRight	= m_fMediaWidth, fFrameTop = m_fMediaHeight;
			float fFrameCenterX = fFrameLeft   + (fFrameRight - fFrameLeft)/2.0f;
			float fFrameCenterY = fFrameBottom + 7.5f;
			int   nHeadPosition = TOP;
			TransformToOutputMedia(nRotation, fCorrectionX, fCorrectionY, m_fMediaWidth, m_fMediaHeight, fFrameLeft, fFrameBottom, fFrameRight, fFrameTop, fFrameCenterX, fFrameCenterY, nHeadPosition); 
			Text2Stream(pPlate, nTile, strGripper, fFrameCenterX, fFrameCenterY, nHeadPosition, 50.0f, 7.5f, 15.0f, FALSE, 1.0f, 1.0f, 1.0f, 1.0f);
		}

		if ( ! m_strSysGenMarksStream.IsEmpty())
		{
			PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, "KIM_GeneratedControlFrames");
			m_strSysGenMarksStream.Empty();
		}
	}

	return FALSE;
}

BOOL CPDFOutput::ProcessGraphics(CPrintSheet* pPrintSheet, IPL_int32_t nSheetIndex, CPlate* pPlate, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile)
{
	if ( ! m_media.m_bBoxShape)	
		return FALSE;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;
	if ( ! pPrintSheet)
		return TRUE;
	if ( ! pPlate)
		return TRUE;

	if (nSheetIndex == -1)	// called from inside CheckOutputControlFrames()
	{
		float fWidth  = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaWidth  : m_fMediaHeight;
		float fHeight = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaHeight : m_fMediaWidth;
		IPL_TDRECT mediaBox	= { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };
		IPL_TDRECT cropBox	= { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };

		IPL_int32_t nResult = IPL_PDFOutputStream_AddSheet(m_hPDFFile, &nSheetIndex, &mediaBox, &cropBox, &mediaBox, &mediaBox, &mediaBox);
		if (nResult)
		{
			CString			strMsg;
			PTB_uint32_t	nBufSize = 1024;
			IPL_utf8_char_t szMsgUTF8[1024];
			IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
			PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();

			KIMOpenLogMessage(strMsg, MB_ICONERROR);

			m_nError = CImpManApp::PDFOutputError;
			return TRUE;
		}

		m_separationInfo.Add(pPlate->m_strPlateName, nSheetIndex);
	}

	CLayout*	 pLayout	 = pPrintSheet->GetLayout();
	CLayoutSide* pLayoutSide = pPlate->GetLayoutSide();
	if (pLayout && pLayoutSide)
	{
		// Output page/mark frames for testing purposes
		int	nActLayoutSide = pPlate->GetSheetSide();
		int	nMirrorAxis	   = -1;
		if (nActLayoutSide == BACKSIDE)
		{
			switch (pLayout->KindOfProduction())
			{
			case WORK_AND_TURN	: nMirrorAxis = VERTICAL;	break;
			case WORK_AND_TUMBLE: nMirrorAxis = HORIZONTAL;	break;
			default:			  break;
			}
		}

		CLayoutObjectList*	pObjectList = &pLayoutSide->m_ObjectList;
		POSITION			pos			= pObjectList->GetHeadPosition();	
		while (pos)
		{
			CLayoutObject& rObject = pObjectList->GetNext(pos);

			if (rObject.m_nType != CLayoutObject::Page) 
				continue;									   
			if ( (rObject.m_nComponentRefIndex < 0) || (rObject.m_nComponentRefIndex >= pDoc->m_flatProducts.GetSize()) )
				continue;

			CString strObjectName	 = pDoc->m_flatProducts.FindByIndex(rObject.m_nComponentRefIndex).m_strProductName;
			CGraphicObject* pgObject = pDoc->m_flatProducts.FindByIndex(rObject.m_nComponentRefIndex).m_graphicList.GetObject(strObjectName);
			if ( ! pgObject)
				continue;

			int nHeadPosition = (nActLayoutSide == BACKSIDE) ? rObject.GetMirrorHeadPosition(nMirrorAxis) : rObject.m_nHeadPosition;
			CString strPDFStream = pgObject->MakePDFStream(fCorrectionX, fCorrectionY, m_fMediaWidth, m_fMediaHeight, nRotation,
														   rObject.GetLeft() - fXPos, rObject.GetBottom() - fYPos, rObject.GetWidth(), rObject.GetHeight(), nHeadPosition, nMirrorAxis);

			Path2Stream(nSheetIndex, strPDFStream, pPlate, m_media.m_frameSource);
		}

		if ( ! m_strSysGenMarksStream.IsEmpty())
		{
			PDFSheet_AddSysGenStream(nSheetIndex, m_strSysGenMarksStream, "KIM_GeneratedGraphicPath");
			m_strSysGenMarksStream.Empty();
		}
	}

	return FALSE;
}

void CPDFOutput::CalcBottlingParams(float& fBottlingAngle, float& fBottlingOffsetX, float& fBottlingOffsetY, CLayoutObject* pObject, CPageTemplate* pObjectTemplate, float fObjectBBoxCenterX, float fObjectBBoxCenterY)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(pObjectTemplate->m_nProductPartIndex);
	if ( ! rProductPart.m_foldingParams.m_bShinglingActive)
		return;
	if ( (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodBottling) && (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodBottlingScale) )
		return;
	if (pObject->m_nType != CLayoutObject::Page)
		return;

	fBottlingAngle = -asin( ( fabs(pObjectTemplate->m_fShinglingOffsetX - pObjectTemplate->m_fShinglingOffsetXFoot) ) / pObject->GetRealHeight() );
	if (AngleEquals( fBottlingAngle, 0.0f ))
		return;

	int nRefPointPosition = 0;
	if ( fabs(pObjectTemplate->m_fShinglingOffsetX) < fabs(pObjectTemplate->m_fShinglingOffsetXFoot) )
		if (pObjectTemplate->m_fShinglingOffsetX > pObjectTemplate->m_fShinglingOffsetXFoot)
		{
			nRefPointPosition = LEFT_TOP;
			fBottlingAngle = -fabs(fBottlingAngle);
		}
		else
		{
			nRefPointPosition = RIGHT_TOP;
			fBottlingAngle = fabs(fBottlingAngle);
		}
	else
		if (pObjectTemplate->m_fShinglingOffsetX < pObjectTemplate->m_fShinglingOffsetXFoot)
		{
			nRefPointPosition = LEFT_BOTTOM;
			fBottlingAngle = fabs(fBottlingAngle);
		}
		else
		{
			nRefPointPosition = RIGHT_BOTTOM;
			fBottlingAngle = -fabs(fBottlingAngle);
		}

	switch (pObject->m_nHeadPosition)
	{
	case TOP:	break;
	case LEFT:	switch (nRefPointPosition)
				{
				case LEFT_TOP:		nRefPointPosition = LEFT_BOTTOM;	break;
				case RIGHT_TOP:		nRefPointPosition = LEFT_TOP;		break;
				case LEFT_BOTTOM:	nRefPointPosition = RIGHT_BOTTOM;	break;
				case RIGHT_BOTTOM:	nRefPointPosition = RIGHT_TOP;		break;
				}
				break;
	case BOTTOM:switch (nRefPointPosition)
				{
				case LEFT_TOP:		nRefPointPosition = RIGHT_BOTTOM;	break;
				case RIGHT_TOP:		nRefPointPosition = LEFT_BOTTOM;	break;
				case LEFT_BOTTOM:	nRefPointPosition = RIGHT_TOP;		break;
				case RIGHT_BOTTOM:	nRefPointPosition = LEFT_TOP;		break;
				}
				break;
	case RIGHT:	switch (nRefPointPosition)
				{
				case LEFT_TOP:		nRefPointPosition = RIGHT_TOP;		break;
				case RIGHT_TOP:		nRefPointPosition = RIGHT_BOTTOM;	break;
				case LEFT_BOTTOM:	nRefPointPosition = LEFT_TOP;		break;
				case RIGHT_BOTTOM:	nRefPointPosition = LEFT_BOTTOM;	break;
				}
				break;
	}

	float fObjectWidth  = pObject->GetWidth();
	float fObjectHeight = pObject->GetHeight();

	// add content rotation and convert to angles between 0 and 270
	double fIntPart;
	double fAngle = pObjectTemplate->m_fContentRotation;
	switch (pObject->m_nHeadPosition)
	{
	case LEFT:	fAngle = (float)modf(( 90.0f + pObjectTemplate->m_fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case BOTTOM:fAngle = (float)modf((180.0f + pObjectTemplate->m_fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case RIGHT: fAngle = (float)modf((270.0f + pObjectTemplate->m_fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	}

	if (AngleEquals(fAngle, 90.0f) )
	{
		float fTemp = fObjectWidth; fObjectWidth = fObjectHeight; fObjectHeight = fTemp;
		switch (nRefPointPosition)
		{
		case LEFT_TOP:		nRefPointPosition = RIGHT_TOP;		break;
		case RIGHT_TOP:		nRefPointPosition = RIGHT_BOTTOM;	break;
		case RIGHT_BOTTOM:	nRefPointPosition = LEFT_BOTTOM;	break;
		case LEFT_BOTTOM:	nRefPointPosition = LEFT_TOP;		break;
		}
	}
	else
		if (AngleEquals(fAngle, 180.0f) )
		{
			switch (nRefPointPosition)
			{
			case LEFT_TOP:		nRefPointPosition = RIGHT_BOTTOM;	break;
			case RIGHT_TOP:		nRefPointPosition = LEFT_BOTTOM;	break;
			case RIGHT_BOTTOM:	nRefPointPosition = LEFT_TOP;		break;
			case LEFT_BOTTOM:	nRefPointPosition = RIGHT_TOP;		break;
			}
		}
		else
			if (AngleEquals(fAngle, 270.0f) )
			{
				float fTemp = fObjectWidth; fObjectWidth = fObjectHeight; fObjectHeight = fTemp;
				switch (nRefPointPosition)
				{
				case LEFT_TOP:		nRefPointPosition = LEFT_BOTTOM;	break;
				case RIGHT_TOP:		nRefPointPosition = LEFT_TOP;		break;
				case RIGHT_BOTTOM:	nRefPointPosition = RIGHT_TOP;		break;
				case LEFT_BOTTOM:	nRefPointPosition = RIGHT_BOTTOM;	break;
				}
			}

	float fRefPointX = 0.0f;
	float fRefPointY = 0.0f;
	switch (nRefPointPosition)
	{
	case LEFT_TOP:		fRefPointX = fObjectBBoxCenterX - fObjectWidth /2.0f;
						fRefPointY = fObjectBBoxCenterY + fObjectHeight/2.0f;
						break;
	case RIGHT_TOP:		fRefPointX = fObjectBBoxCenterX + fObjectWidth /2.0f;
						fRefPointY = fObjectBBoxCenterY + fObjectHeight/2.0f;
						break;
	case RIGHT_BOTTOM:	fRefPointX = fObjectBBoxCenterX + fObjectWidth /2.0f;
						fRefPointY = fObjectBBoxCenterY - fObjectHeight/2.0f;
						break;
	case LEFT_BOTTOM:	fRefPointX = fObjectBBoxCenterX - fObjectWidth /2.0f;
						fRefPointY = fObjectBBoxCenterY - fObjectHeight/2.0f;
						break;
	}

	float fA, fB, fC, fD;
	fA = cos(fBottlingAngle); fB = sin(fBottlingAngle); fC = -fB; fD =  fA;
	float fX_ = fA * fRefPointX + fC * fRefPointY;
	float fY_ = fB * fRefPointX + fD * fRefPointY;
	float fBottlingOffsetX_ = fX_ - fRefPointX;
	float fBottlingOffsetY_ = fY_ - fRefPointY;

	if (AngleEquals(fAngle, 0.0f))
	{
		fBottlingOffsetX = -fBottlingOffsetX_;
		fBottlingOffsetY = -fBottlingOffsetY_;
	}
	else
		if (AngleEquals(fAngle, 90.0f))
		{
			switch (nRefPointPosition)
			{
			case LEFT_TOP:		fBottlingOffsetX = -fBottlingOffsetY_;
								fBottlingOffsetY = -fBottlingOffsetX_;
								break;
			case RIGHT_TOP:		fBottlingOffsetX =  fBottlingOffsetY_;
								fBottlingOffsetY = -fBottlingOffsetX_;
								break;
			case RIGHT_BOTTOM:	fBottlingOffsetX =  fBottlingOffsetY_;
								fBottlingOffsetY = -fBottlingOffsetX_;
								break;
			case LEFT_BOTTOM:	fBottlingOffsetX = -fBottlingOffsetY_;
								fBottlingOffsetY = -fBottlingOffsetX_;
								break;
			}
		}
		else
			if (AngleEquals(fAngle, 180.0f))
			{
				fBottlingOffsetX = fBottlingOffsetX_;
				fBottlingOffsetY = fBottlingOffsetY_;
			}
				else
					if (AngleEquals(fAngle, 270.0f))
					{
						switch (nRefPointPosition)
						{
						case LEFT_TOP:		fBottlingOffsetX =  fBottlingOffsetY_;
											fBottlingOffsetY =  fBottlingOffsetX_;
											break;
						case RIGHT_TOP:		fBottlingOffsetX = -fBottlingOffsetY_;
											fBottlingOffsetY =  fBottlingOffsetX_;
											break;
						case RIGHT_BOTTOM:	fBottlingOffsetX = -fBottlingOffsetY_;
											fBottlingOffsetY =  fBottlingOffsetX_;
											break;
						case LEFT_BOTTOM:	fBottlingOffsetX =  fBottlingOffsetY_;
											fBottlingOffsetY =  fBottlingOffsetX_;
											break;
						}
					}

	fBottlingAngle *= 180.0f / 3.141592654f;
}

void CPDFOutput::TransformToOutputMedia(int nRotation, float fCorrectionX, float fCorrectionY, float fMediaWidth, float fMediaHeight, 
									   float& fSheetClipBoxLeft, float& fSheetClipBoxBottom, float& fSheetClipBoxRight, float& fSheetClipBoxTop, 
									   float& fSheetExposureCenterX, float& fSheetExposureCenterY, int& nHeadPosition)
{
	float fSheetClipBoxLeftTrans, fSheetClipBoxBottomTrans, fSheetClipBoxRightTrans, fSheetClipBoxTopTrans;
	float fSheetExposureCenterXTrans, fSheetExposureCenterYTrans;
	switch (nRotation)
	{
	case 0:		
		fSheetClipBoxLeftTrans	   = fSheetClipBoxLeft;
		fSheetClipBoxBottomTrans   = fSheetClipBoxBottom;
		fSheetClipBoxRightTrans	   = fSheetClipBoxRight;
		fSheetClipBoxTopTrans	   = fSheetClipBoxTop;
		fSheetExposureCenterXTrans = fSheetExposureCenterX;
		fSheetExposureCenterYTrans = fSheetExposureCenterY;
		break;
	case 90:	
		fSheetClipBoxLeftTrans	   = fSheetClipBoxBottom;
		fSheetClipBoxBottomTrans   = fMediaWidth - fSheetClipBoxRight;
		fSheetClipBoxRightTrans	   = fSheetClipBoxTop;
		fSheetClipBoxTopTrans	   = fMediaWidth - fSheetClipBoxLeft;
		fSheetExposureCenterXTrans = fSheetExposureCenterY;
		fSheetExposureCenterYTrans = fMediaWidth - fSheetExposureCenterX;
		break;
	case 180:	
		fSheetClipBoxLeftTrans	   = fMediaWidth  - fSheetClipBoxRight;
		fSheetClipBoxBottomTrans   = fMediaHeight - fSheetClipBoxTop;
		fSheetClipBoxRightTrans	   = fMediaWidth  - fSheetClipBoxLeft;
		fSheetClipBoxTopTrans	   = fMediaHeight - fSheetClipBoxBottom;
		fSheetExposureCenterXTrans = fMediaWidth  - fSheetExposureCenterX;
		fSheetExposureCenterYTrans = fMediaHeight - fSheetExposureCenterY;
		break;
	case 270:	
		fSheetClipBoxLeftTrans	   = fMediaHeight - fSheetClipBoxTop;
		fSheetClipBoxBottomTrans   = fSheetClipBoxLeft;
		fSheetClipBoxRightTrans	   = fMediaHeight - fSheetClipBoxBottom;
		fSheetClipBoxTopTrans	   = fSheetClipBoxRight;
		fSheetExposureCenterXTrans = fMediaHeight - fSheetExposureCenterY;
		fSheetExposureCenterYTrans = fSheetExposureCenterX;
		break;
	default:		
		fSheetClipBoxLeftTrans	   = fSheetClipBoxLeft;
		fSheetClipBoxBottomTrans   = fSheetClipBoxBottom;
		fSheetClipBoxRightTrans	   = fSheetClipBoxRight;
		fSheetClipBoxTopTrans	   = fSheetClipBoxTop;
		fSheetExposureCenterXTrans = fSheetExposureCenterX;
		fSheetExposureCenterYTrans = fSheetExposureCenterY;
		break;
	}
	
	fSheetClipBoxLeft	  = fSheetClipBoxLeftTrans		+ fCorrectionX;
	fSheetClipBoxBottom	  = fSheetClipBoxBottomTrans	+ fCorrectionY;
	fSheetClipBoxRight	  = fSheetClipBoxRightTrans		+ fCorrectionX;
	fSheetClipBoxTop	  = fSheetClipBoxTopTrans		+ fCorrectionY;
	fSheetExposureCenterX = fSheetExposureCenterXTrans	+ fCorrectionX;
	fSheetExposureCenterY = fSheetExposureCenterYTrans	+ fCorrectionY;

	switch (nRotation)
	{
	case 0:	  
		switch (nHeadPosition)
		{
		case TOP:		nHeadPosition = TOP;	break;
		case RIGHT:		nHeadPosition = RIGHT;	break;
		case BOTTOM:	nHeadPosition = BOTTOM; break;
		case LEFT:		nHeadPosition = LEFT;	break;
		}
		break;
	case 90:  
		switch (nHeadPosition)
		{
		case TOP:		nHeadPosition = RIGHT;	break;
		case RIGHT:		nHeadPosition = BOTTOM;	break;
		case BOTTOM:	nHeadPosition = LEFT;	break;
		case LEFT:		nHeadPosition = TOP;	break;
		}
		break;
	case 180: 
		switch (nHeadPosition)
		{
		case TOP:		nHeadPosition = BOTTOM;	break;
		case RIGHT:		nHeadPosition = LEFT;	break;
		case BOTTOM:	nHeadPosition = TOP;	break;
		case LEFT:		nHeadPosition = RIGHT;	break;
		}
		break;
	case 270: 
		switch (nHeadPosition)
		{
		case TOP:		nHeadPosition = LEFT;	break;
		case RIGHT:		nHeadPosition = TOP;	break;
		case BOTTOM:	nHeadPosition = RIGHT;	break;
		case LEFT:		nHeadPosition = BOTTOM;	break;
		}
		break;
	}
}

void CPDFOutput::TransformToOutputMedia(int nRotation, float fCorrectionX, float fCorrectionY, float fMediaWidth, float fMediaHeight, 
									   float& fSheetClipBoxLeft, float& fSheetClipBoxBottom, float& fSheetClipBoxRight, float& fSheetClipBoxTop)
{
	float fSheetClipBoxLeftTrans, fSheetClipBoxBottomTrans, fSheetClipBoxRightTrans, fSheetClipBoxTopTrans;
	switch (nRotation)
	{
	case 0:		
		fSheetClipBoxLeftTrans	   = fSheetClipBoxLeft;
		fSheetClipBoxBottomTrans   = fSheetClipBoxBottom;
		fSheetClipBoxRightTrans	   = fSheetClipBoxRight;
		fSheetClipBoxTopTrans	   = fSheetClipBoxTop;
		break;
	case 90:	
		fSheetClipBoxLeftTrans	   = fSheetClipBoxBottom;
		fSheetClipBoxBottomTrans   = fMediaWidth - fSheetClipBoxRight;
		fSheetClipBoxRightTrans	   = fSheetClipBoxTop;
		fSheetClipBoxTopTrans	   = fMediaWidth - fSheetClipBoxLeft;
		break;
	case 180:	
		fSheetClipBoxLeftTrans	   = fMediaWidth  - fSheetClipBoxRight;
		fSheetClipBoxBottomTrans   = fMediaHeight - fSheetClipBoxTop;
		fSheetClipBoxRightTrans	   = fMediaWidth  - fSheetClipBoxLeft;
		fSheetClipBoxTopTrans	   = fMediaHeight - fSheetClipBoxBottom;
		break;
	case 270:	
		fSheetClipBoxLeftTrans	   = fMediaHeight - fSheetClipBoxTop;
		fSheetClipBoxBottomTrans   = fSheetClipBoxLeft;
		fSheetClipBoxRightTrans	   = fMediaHeight - fSheetClipBoxBottom;
		fSheetClipBoxTopTrans	   = fSheetClipBoxRight;
		break;
	default:		
		fSheetClipBoxLeftTrans	   = fSheetClipBoxLeft;
		fSheetClipBoxBottomTrans   = fSheetClipBoxBottom;
		fSheetClipBoxRightTrans	   = fSheetClipBoxRight;
		fSheetClipBoxTopTrans	   = fSheetClipBoxTop;
		break;
	}
	
	fSheetClipBoxLeft	= fSheetClipBoxLeftTrans   + fCorrectionX;
	fSheetClipBoxBottom	= fSheetClipBoxBottomTrans + fCorrectionY;
	fSheetClipBoxRight	= fSheetClipBoxRightTrans  + fCorrectionX;
	fSheetClipBoxTop	= fSheetClipBoxTopTrans	   + fCorrectionY;
}

BOOL CPDFOutput::AddPage(IPL_int32_t nSheetIndex, IPL_TOBJECTID* pObjectID,	CLayoutObject* pObject, CPageTemplate* pObjectTemplate, CPageSource* pPageSource, CPageSourceHeader* pPageSourceHeader,
																			float fSheetExposureCenterX,  float fSheetExposureCenterY, int nHeadPosition,
																			float fSheetClipBoxLeft,	  float fSheetClipBoxBottom, 
																			float fSheetClipBoxRight,	  float fSheetClipBoxTop,
																			float fObjectBBoxCenterX,	  float fObjectBBoxCenterY,
																			float fContentRotation,		  float fContentScaleX,		 float fContentScaleY,
																			float fBottlingAngle,	      float fBottlingOffsetX,	 float fBottlingOffsetY,
																			int   nObjectType,			  BOOL  bSkipIfDemo,		 BOOL bJDFMarksOutput)
{
	BOOL bDupMark = (pObjectTemplate->m_bContentDuplicate && ((pObjectTemplate->m_fContentGridX > 0.1f) || (pObjectTemplate->m_fContentGridY > 0.1f)) ) ? TRUE : FALSE;
	if (bDupMark)
	{
		float fXGrid = pObjectTemplate->m_fContentGridX;
		float fYGrid = pObjectTemplate->m_fContentGridY;
		float fSheetClipBoxWidth  = fSheetClipBoxRight - fSheetClipBoxLeft;
		float fSheetClipBoxHeight = fSheetClipBoxTop   - fSheetClipBoxBottom;
		float fObjectBBoxWidth	  = pPageSource->GetBBox(RIGHT, pPageSourceHeader) - pPageSource->GetBBox(LEFT,   pPageSourceHeader);
		float fObjectBBoxHeight	  = pPageSource->GetBBox(TOP,   pPageSourceHeader) - pPageSource->GetBBox(BOTTOM, pPageSourceHeader);
		float fXPos = 0.0f;
		switch (pObjectTemplate->m_nContentPivotX)
		{
		case LEFT:		fXPos = fSheetClipBoxLeft;																				break;
		case RIGHT:		fXPos = fSheetClipBoxRight - ((int)(fSheetClipBoxWidth / fXGrid)) * fXGrid;								break;
		case XCENTER:	if (pObjectTemplate->m_bContentDuplicate)
							fXPos = fSheetClipBoxLeft + fSheetClipBoxWidth / 2.0f - ((int)(fSheetClipBoxWidth / fXGrid)) * fXGrid - fXGrid/2.0f;	
						else
							fXPos = fSheetClipBoxLeft + fSheetClipBoxWidth / 2.0f - ((int)(fSheetClipBoxWidth / fXGrid)) * fXGrid;	
						break;
		}
		if (fXPos > fSheetClipBoxLeft)
			fXPos -= fXGrid;

		while (fXPos <= fSheetClipBoxRight)
		{
			float fYPos = 0.0f;
			switch (pObjectTemplate->m_nContentPivotY)
			{
			case BOTTOM:	fYPos = fSheetClipBoxBottom;																				break;
			case TOP:		fYPos = fSheetClipBoxTop - ((int)(fSheetClipBoxHeight / fYGrid)) * fYGrid;									break;
			case YCENTER:	if (pObjectTemplate->m_bContentDuplicate)
								fYPos = fSheetClipBoxBottom + fSheetClipBoxHeight / 2.0f - ((int)(fSheetClipBoxHeight / fYGrid)) * fYGrid - fYGrid/2.0f;	
							else
								fYPos = fSheetClipBoxBottom + fSheetClipBoxHeight / 2.0f - ((int)(fSheetClipBoxHeight / fYGrid)) * fYGrid;	
							break;
			}
			if (fYPos > fSheetClipBoxBottom)
				fYPos -= fYGrid;
			
			while (fYPos <= fSheetClipBoxTop)
			{
				switch (pObjectTemplate->m_nContentPivotX)
				{
				case LEFT:		fSheetExposureCenterX =  fXPos + fObjectBBoxWidth / 2.0f;			break;
				case RIGHT:		fSheetExposureCenterX =  fXPos + fXGrid - fObjectBBoxWidth / 2.0f;	break;
				case XCENTER:	fSheetExposureCenterX = (fXPos + fXGrid / 2.0f);					break;
				}
				switch (pObjectTemplate->m_nContentPivotY)
				{
				case BOTTOM:	fSheetExposureCenterY =  fYPos + fObjectBBoxHeight / 2.0f;			break;
				case TOP:		fSheetExposureCenterY =  fYPos + fYGrid - fObjectBBoxHeight / 2.0f;	break;
				case YCENTER:	fSheetExposureCenterY = (fYPos + fYGrid / 2.0f);					break;
				}
				switch (pObject->m_nHeadPosition)
				{
				case TOP:		fSheetExposureCenterX += pObjectTemplate->_GetContentOffsetX();	
								fSheetExposureCenterY += pObjectTemplate->_GetContentOffsetY();	
								break;
				case LEFT:		fSheetExposureCenterX -= pObjectTemplate->_GetContentOffsetY();	
								fSheetExposureCenterY += pObjectTemplate->_GetContentOffsetX();	
								break;
				case BOTTOM:	fSheetExposureCenterX -= pObjectTemplate->_GetContentOffsetX();
								fSheetExposureCenterY -= pObjectTemplate->_GetContentOffsetY();
								break;
				case RIGHT:		fSheetExposureCenterX += pObjectTemplate->_GetContentOffsetY();
								fSheetExposureCenterY -= pObjectTemplate->_GetContentOffsetX();
								break;
				}
				PDFSheet_AddPage(nSheetIndex, pObjectID,	fSheetExposureCenterX,	fSheetExposureCenterY, nHeadPosition,
															fSheetClipBoxLeft,		fSheetClipBoxBottom, 
															fSheetClipBoxRight,		fSheetClipBoxTop,
															fObjectBBoxCenterX,		fObjectBBoxCenterY,
															fContentRotation,		fContentScaleX,		fContentScaleY,
															fBottlingAngle,			fBottlingOffsetX,	fBottlingOffsetY,
															nObjectType,			bSkipIfDemo,		bJDFMarksOutput);
				if (pObjectTemplate->m_fContentGridY <= 0.1f)
					break;
				fYPos += fYGrid;
			}

			if (pObjectTemplate->m_fContentGridX <= 0.1f)
				break;
			fXPos += fXGrid;
		}
	}
	else
		return PDFSheet_AddPage(nSheetIndex, pObjectID,	fSheetExposureCenterX,	fSheetExposureCenterY, nHeadPosition,
														fSheetClipBoxLeft,		fSheetClipBoxBottom, 
														fSheetClipBoxRight,		fSheetClipBoxTop,
														fObjectBBoxCenterX,		fObjectBBoxCenterY,
														fContentRotation,		fContentScaleX,		fContentScaleY,
														fBottlingAngle,			fBottlingOffsetX,	fBottlingOffsetY,
														nObjectType,			bSkipIfDemo,		bJDFMarksOutput);

	return m_bCancelJob;
}

BOOL CPDFOutput::PDFSheet_AddPage(IPL_int32_t nSheetIndex, IPL_TOBJECTID* pObjectID, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
																					 float fSheetClipBoxLeft,	  float fSheetClipBoxBottom, 
																					 float fSheetClipBoxRight,	  float fSheetClipBoxTop,
																					 float fObjectBBoxCenterX,	  float fObjectBBoxCenterY,
																					 float fContentRotation,	  float fContentScaleX,	 float fContentScaleY,
																					 float fBottlingAngle,	      float fBottlingOffsetX, float fBottlingOffsetY,
																					 int   nObjectType,			  BOOL  bSkipIfDemo,		 BOOL bJDFMarksOutput)
{
	if (m_nError != 0)
		return TRUE;

	// add content rotation and convert to angles between 0 and 270
	double fIntPart;
	double fAngle = fContentRotation;
	switch (nHeadPosition)
	{
	case LEFT:	fAngle = (float)modf(( 90.0f + fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case BOTTOM:fAngle = (float)modf((180.0f + fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case RIGHT: fAngle = (float)modf((270.0f + fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	}

	float fScaleX = (AngleEquals( fContentRotation, 0.0f ) || AngleEquals( fContentRotation, 180.0f )) ? fContentScaleX : fContentScaleY;
	float fScaleY = (AngleEquals( fContentRotation, 0.0f ) || AngleEquals( fContentRotation, 180.0f )) ? fContentScaleY : fContentScaleX;

	fObjectBBoxCenterX	  *= fScaleX;
	fObjectBBoxCenterY	  *= fScaleY;
	fObjectBBoxCenterX	  /= fPSPoint; fObjectBBoxCenterY	 /= fPSPoint;
	fSheetExposureCenterX /= fPSPoint; fSheetExposureCenterY /= fPSPoint;
	fSheetClipBoxLeft	  /= fPSPoint; fSheetClipBoxBottom	 /= fPSPoint;
	fSheetClipBoxRight	  /= fPSPoint; fSheetClipBoxTop		 /= fPSPoint;
	fBottlingOffsetX	  /= fPSPoint; fBottlingOffsetY		 /= fPSPoint;

	float fOriginX, fOriginY;
	CalcPDFObjectOrigin(fOriginX, fOriginY, (float)fAngle, fSheetExposureCenterX, fSheetExposureCenterY, fObjectBBoxCenterX, fObjectBBoxCenterY);
	IPL_TDMATRIX matrix		= CalcPDFObjectMatrix  (fOriginX + fBottlingOffsetX, fOriginY + fBottlingOffsetY, (float)fAngle + fBottlingAngle, fScaleX, fScaleY);
	CClipPath	 clipPath	= CalcPDFObjectClipPath(fOriginX + fBottlingOffsetX, fOriginY + fBottlingOffsetY, (float)fAngle, fBottlingAngle, fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop);

	clipPath[0].fX /= fScaleX; clipPath[0].fY /= fScaleY;
	clipPath[1].fX /= fScaleX; clipPath[1].fY /= fScaleY;
	clipPath[2].fX /= fScaleX; clipPath[2].fY /= fScaleY;
	clipPath[3].fX /= fScaleX; clipPath[3].fY /= fScaleY;

	CheckDonglePDF(FALSE, FALSE);

	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	//if ( ! pPrintSheetTableView)
	//	return m_bCancelJob;

	CImpManDoc* pDoc			   = CImpManDoc::GetDoc();
	int			nDocsDongleStatus  = (pDoc) ? pDoc->m_nDongleStatus : CImpManApp::NoDongle;
	BOOL		bAddWatermarks     = TRUE;
	switch (theApp.m_nDongleStatus)
	{
	case CImpManApp::NoDongle:			bAddWatermarks = TRUE; break;
	case CImpManApp::ClientDongle:		bAddWatermarks = TRUE; break;

	case CImpManApp::StandardDongle:	
	case CImpManApp::SmallFormatDongle:	if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
											bAddWatermarks = ( (nDocsDongleStatus == CImpManApp::NoDongle) || 
															   (nDocsDongleStatus == CImpManApp::ClientDongle) ) ? TRUE  : FALSE;
										else
											bAddWatermarks = TRUE;	
										if ( (theApp.m_nDongleStatus == CImpManApp::SmallFormatDongle) && ! bAddWatermarks)
											if (m_bOutputFormatExceeded)
												bAddWatermarks = TRUE;
										break;
	case CImpManApp::AutoDongle:
	case CImpManApp::NewsDongle:		
	case CImpManApp::DigitalDongle:		if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
											bAddWatermarks = ( (nDocsDongleStatus == CImpManApp::NoDongle) ||
															   (nDocsDongleStatus == CImpManApp::ClientDongle) ) ? TRUE  : FALSE;	
										else
											bAddWatermarks = (nDocsDongleStatus == CImpManApp::NoDongle) ? TRUE  : FALSE;	// if server, client and standard templates allowed

										if ( (theApp.m_nDongleStatus == CImpManApp::NewsDongle) && ! bAddWatermarks)
											if (m_bOutputFormatExceeded)
												bAddWatermarks = TRUE;
										if ( (theApp.m_nDongleStatus == CImpManApp::DigitalDongle) && ! bAddWatermarks)
											if (m_bOutputFormatExceeded)
												bAddWatermarks = TRUE;
										break;
	}

	if (bJDFMarksOutput)
		if ( ! bAddWatermarks)
			if ( ! (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp))
				bAddWatermarks = TRUE;

	if (bAddWatermarks)
		if (bSkipIfDemo)
		{
			CPageSource frameSource;
			Frame2Stream(nSheetIndex, fSheetClipBoxLeft*fPSPoint, fSheetClipBoxBottom*fPSPoint, (fSheetClipBoxRight - fSheetClipBoxLeft)*fPSPoint, (fSheetClipBoxTop - fSheetClipBoxBottom)*fPSPoint, 
						 NULL, frameSource);
			GETDEMOSTRING().LoadString(IDS_DEMOVERSION);
			int   nStrLen = GETDEMOSTRING().GetLength();
			float fTextHeight = ((fSheetClipBoxRight - fSheetClipBoxLeft)*fPSPoint)/2/nStrLen;
			Text2Stream(NULL, 0, GETDEMOSTRING(), fSheetExposureCenterX*fPSPoint, fSheetExposureCenterY*fPSPoint, nHeadPosition, fTextHeight*nStrLen/2, fTextHeight/2, fTextHeight, FALSE, 1.0f, 1.0f, 1.0f, 1.0f);
			CString strOut;
			strOut.LoadString(IDS_PAGE_SUPPRESSED);
			nStrLen = strOut.GetLength();
			fTextHeight = ((fSheetClipBoxRight - fSheetClipBoxLeft)*fPSPoint)/2/nStrLen;
			float fOffset = 3*fTextHeight;
			float fXPos = fSheetExposureCenterX*fPSPoint, fYPos = fSheetExposureCenterY*fPSPoint - fOffset;
			switch (nHeadPosition)
			{
			case RIGHT:  fXPos = fSheetExposureCenterX*fPSPoint - fOffset;	fYPos = fSheetExposureCenterY*fPSPoint;				break;
			case BOTTOM: fXPos = fSheetExposureCenterX*fPSPoint;			fYPos = fSheetExposureCenterY*fPSPoint + fOffset;	break;
			case LEFT:	 fXPos = fSheetExposureCenterX*fPSPoint + fOffset;	fYPos = fSheetExposureCenterY*fPSPoint;				break;
			}
			Text2Stream(NULL, 0, strOut, fXPos, fYPos, nHeadPosition, fTextHeight*nStrLen/2, fTextHeight/2, fTextHeight, FALSE, 1.0f, 1.0f, 1.0f, 1.0f);
			return m_bCancelJob;
		}

	CRect cancelRect;
	if (pPrintSheetTableView)
	{
		int nButtonIndex = pPrintSheetTableView->m_outputListToolBarCtrl.CommandToIndex(ID_CANCEL_OUTPUT);
		pPrintSheetTableView->m_outputListToolBarCtrl.GetItemRect(nButtonIndex, cancelRect);
		pPrintSheetTableView->m_outputListToolBarCtrl.ClientToScreen(cancelRect);
	}

	CString strClipPath;
	strClipPath.Format(_T("%f %f m %f %f l %f %f l %f %f l h W n"),  clipPath[0].fX, clipPath[0].fY, clipPath[1].fX, clipPath[1].fY,
																	 clipPath[2].fX, clipPath[2].fY, clipPath[3].fX, clipPath[3].fY);

	IPL_utf8_char_t		szDemo[] = "";
	IPL_TCOLORMAPPER	colorMapper;
	IPL_TPDFOBJECTCODE	objectCode;
	colorMapper.size			= sizeof(IPL_TCOLORMAPPER);
	colorMapper.colors			= m_spotColorMapper.m_strOldSpotColors.GetSize();
	colorMapper.oldcolor		= m_spotColorMapper.m_callasOldColor;
	colorMapper.newcolor		= m_spotColorMapper.m_callasNewColor;
	colorMapper.altpdf[0]		= '\0';
	colorMapper.altpagenum		= 0;
	colorMapper.alternatecb		= SpotColorCallback;
	colorMapper.alternatecbdata	= (void*)&m_spotColorMapper;
	objectCode.size				= sizeof( IPL_TPDFOBJECTCODE);
	objectCode.preobjcode		= strClipPath.GetBuffer(strClipPath.GetLength() + 1);
	objectCode.preobjcodelen	= strlen(objectCode.preobjcode);
	objectCode.postobjcode		= NULL;
	objectCode.postobjcodelen	= 0; 

	IPL_int32_t nResult = 0;
	if (colorMapper.colors)
		nResult = IPL_PDFSheet_AddObject2(m_hPDFFile, nSheetIndex, pObjectID, &matrix, NULL, (bAddWatermarks) ? szDemo : NULL, &colorMapper, &objectCode, NULL);
	else
		nResult = IPL_PDFSheet_AddObject2(m_hPDFFile, nSheetIndex, pObjectID, &matrix, NULL, (bAddWatermarks) ? szDemo : NULL, NULL, &objectCode, NULL);

	CheckCancelJob(cancelRect, pPrintSheetTableView);

	if (nResult)
	{
		CString strError, strMsg, strCancel;
		strCancel.LoadString(ID_CANCEL_OUTPUT);
		PTB_uint32_t	nBufSize = 1024;
		IPL_utf8_char_t szMsgUTF8[1024];
		IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
		PTB_ConvertUTF8ToSys(szMsgUTF8, strError.GetBuffer(1024), &nBufSize);	strError.ReleaseBuffer();

#ifdef KIM_ENGINE
		strMsg.Format(_T("%s\r\n   File: %s\r\n   Index: %d\r\n"), strError, pObjectID->pdffile, pObjectID->pageidx);
		theApp.LogEntry(_T("   ") + strMsg, TRUE);
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
#else
		CString strFilename = pObjectID->pdffile;
		PathCompactPathEx(strFilename.GetBuffer(MAX_TEXT), (LPCSTR)pObjectID->pdffile, 50, 0); strFilename.ReleaseBuffer();
		strMsg.Format(_T("%s\n\nFile:\t%s\nIndex:\t%d\n\n%s"), strError, strFilename, pObjectID->pageidx, strCancel);
		if (KIMOpenLogMessage(strMsg, MB_YESNO | MB_ICONSTOP) == IDYES)	//break
		{
			m_nError = CImpManApp::PDFOutputError;
			return TRUE;
		}
#endif
	}

	return m_bCancelJob;
}

IPL_bool_t CPDFOutput::SpotColorCallback(IPL_TPDFCODECOLOR* color, void* userdata)
{
	CString			  strMappedColor;
	CSpotColorMapper* pSpotColorMapper = (CSpotColorMapper*)userdata;
	for (int i = 0; i < pSpotColorMapper->m_strOldSpotColors.GetSize(); i++)
	{
		if (color->colorname == pSpotColorMapper->m_strOldSpotColors[i])
		{
			strMappedColor = (i < pSpotColorMapper->m_strNewSpotColors.GetSize()) ? pSpotColorMapper->m_strNewSpotColors[i] : _T("");
			break;
		}
	}

	CColorDefinition* pColorDef = NULL;
	CImpManDoc*		  pDoc		= CImpManDoc::GetDoc();
	if (pDoc)
		pColorDef = pDoc->m_ColorDefinitionTable.FindColorDefinition(strMappedColor);

	color->size	     = sizeof(IPL_TPDFCODECOLOR);
	color->colorcomp = 4;
	if ((pColorDef))
	{
		color->colorv[0] = (float)GetCValue(pColorDef->m_cmyk)/100.0f;
		color->colorv[1] = (float)GetMValue(pColorDef->m_cmyk)/100.0f;
		color->colorv[2] = (float)GetYValue(pColorDef->m_cmyk)/100.0f;
		color->colorv[3] = (float)GetKValue(pColorDef->m_cmyk)/100.0f;
	}
	else
	{
		color->colorv[0] = 0.2f;
		color->colorv[1] = 0.2f;
		color->colorv[2] = 0.2f;
		color->colorv[3] = 0.2f;
	}

	return 1;
}

void CPDFOutput::CheckCancelJob(CRect cancelRect, CPrintSheetTableView* pView)
{
	CString strCancelPDFOutput;
	strCancelPDFOutput.LoadString(ID_CANCEL_OUTPUT);

	MSG msg;
	if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		if (msg.message == WM_LBUTTONDOWN)
		{
			CPoint ptMousePos;
			GetCursorPos(&ptMousePos);
			if (cancelRect.PtInRect(ptMousePos))
			{
				//pView->m_wndAnimate.Stop(); 
				CDlgStopOutput dlg;
				switch (dlg.DoModal())
				{
				case IDC_STOP_OUTPUT_ALL:	m_bCancelJob = 2;	  break;
				case IDC_STOP_OUTPUT_SHEET: m_bCancelJob = TRUE;  break;
				case IDC_PROCEED_OUTPUT:	m_bCancelJob = FALSE; break;
				}
				//pView->m_wndAnimate.Play(0, (unsigned)-1, (unsigned)-1);
			}
		}
		if (msg.message == WM_MOUSEMOVE)
		{
			CPoint ptMousePos;
			GetCursorPos(&ptMousePos);
			if (cancelRect.PtInRect(ptMousePos))
			{
				if (pView)
					pView->m_strPDFOutputMessage = strCancelPDFOutput;
			}
			else
				if (pView)
					pView->m_strPDFOutputMessage = "";
			if (pView)
				pView->UpdateData(FALSE);
		}
		if (msg.message == WM_PAINT)
			if (!AfxGetApp()->PreTranslateMessage(&msg))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
	}
}

void CPDFOutput::SystemCreatedContent2Stream(int nSheetIndex, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
															  float fSheetClipBoxLeft,	   float fSheetClipBoxBottom, 
															  float fSheetClipBoxRight,	   float fSheetClipBoxTop,
															  CLayoutObject* pObject, CPageTemplate* pObjectTemplate, CPageSource* pObjectSource, 
															  CPrintSheet* pPrintSheet, CPlate* pPlate, int nTile)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strSystemCreationInfo = pObjectSource->m_strSystemCreationInfo;

	int	nIndex = strSystemCreationInfo.Find(_T(" "));
	if (nIndex == -1)	// invalid string
		return;

	float					   fC = 0.0f, fM = 0.0f, fY = 0.0f, fK = 0.0f;
	CStringArray			   spotColorNames;
	CArray<COLORREF, COLORREF> spotColorCMYKs;
	CArray<float,	 float>	   spotColorTintValues;

	m_bSysGenMarksColorSpaceALL = FALSE;

	BOOL bColorSpaceALL = FALSE;
	BOOL bComposite		= ( (pPlate->m_strPlateName.CompareNoCase(_T("Composite")) == 0) || pPlate->m_strPlateName.IsEmpty() ) ? TRUE : FALSE;	// by definition: if no pages assigned (default plate) assume composite
	if (bComposite)
		BuildCompositeColorInfo(pObjectTemplate, pObjectSource, pPlate, bColorSpaceALL, fC, fM, fY, fK, spotColorNames, spotColorCMYKs, spotColorTintValues);
	else	
		fK = 1.0f;	// in case of separation -> output in black (gray) / colorspace all is not relevant here, because this has already been considered in ReorganizePrintSheetPlates()

	CString strContentType = strSystemCreationInfo.Left(nIndex);
	CString strParams	   = strSystemCreationInfo.Right(strSystemCreationInfo.GetLength() - nIndex);
	strParams.TrimLeft(); strParams.TrimRight();

	if (strContentType == "$TEXT")
	{
		CString strText;
		nIndex = strParams.Find(_T("/EOT"));
		if (nIndex == -1)	
		{
			strText	  = strParams;
			strParams = "";
		}
		else
		{
			strText	  = strParams.Left(nIndex);
			nIndex	  = strParams.GetLength() - nIndex - 4;	// 4 = length of $EOT
			strParams = (nIndex >= 0) ? strParams.Right(nIndex) : strParams;	
			strParams.TrimLeft(); strParams.TrimRight();
		}
		float fTextBoxHeight = pObject->GetRealHeight() * 0.55f;
		float fTextBoxWidth  = pObject->GetRealWidth();
		if ( bComposite && (strText.Find(_T("$2")) != -1) )	// special handling for color name output in case of composite
			CompositeColorText2Stream(pPlate, nTile, strText, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition, 
															fTextBoxWidth / 2.0f, fTextBoxHeight / 2.0f, fTextBoxHeight, 
															fC, fM, fY, fK, &spotColorNames, &spotColorCMYKs);
		else
			Text2Stream(pPlate, nTile, strText, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition, 
															fTextBoxWidth / 2.0f, fTextBoxHeight / 2.0f, fTextBoxHeight, 
															bColorSpaceALL, fC, fM, fY, fK, &spotColorNames, &spotColorCMYKs, (pObject) ? &pObject->m_reflinePosParams : NULL);
	}
	else
	if (strContentType == "$SECTION_BAR")
	{
		float fBarWidth, fBarHeight;
		Ascii2Float(strParams, _T("%f %f"), &fBarWidth, &fBarHeight);

		BOOL bNumRotated180 = (strParams.Find(_T("NUMROTATED:180")) == -1) ? FALSE : TRUE;
		BOOL bHollow		= (strParams.Find(_T("HOLLOW"))			== -1) ? FALSE : TRUE;
		float fObjectBBoxCenterX = ( (nHeadPosition == TOP) || (nHeadPosition == BOTTOM) ) ? (min(pObject->GetWidth(), pObject->GetHeight()) / 2.0f) : 
																							 (max(pObject->GetWidth(), pObject->GetHeight()) / 2.0f);
		float fObjectBBoxCenterY = ( (nHeadPosition == TOP) || (nHeadPosition == BOTTOM) ) ? (max(pObject->GetWidth(), pObject->GetHeight()) / 2.0f) : 
																							 (min(pObject->GetWidth(), pObject->GetHeight()) / 2.0f);
		SectionBar2Stream(fBarWidth, fBarHeight, bNumRotated180, bHollow, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition, pObject->m_nHeadPosition, // if media is rotated, nHeadPosition is rotated - but we need orig. head position as well
						  fObjectBBoxCenterX, fObjectBBoxCenterY, pObject, pPrintSheet,
						  bColorSpaceALL, fC, fM, fY, fK, &spotColorNames, &spotColorCMYKs);
	}
	else
	if (strContentType == "$CROP_LINE")
	{
		CropLine2Stream(fSheetExposureCenterX, fSheetExposureCenterY, 
						fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop,
						bColorSpaceALL, fC, fM, fY, fK, &spotColorNames, &spotColorCMYKs);
	}
	else
	if (strContentType == "$FOLD_MARK")
	{
		BOOL bSolid		  = (strParams.Find(_T("/Solid"))		!= -1) ? TRUE : FALSE;
		BOOL bTransparent = (strParams.Find(_T("/Transparent")) != -1) ? TRUE : FALSE;
		FoldLine2Stream(bSolid, bTransparent, fSheetExposureCenterX, fSheetExposureCenterY, 
						fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop,
						bColorSpaceALL, fC, fM, fY, fK, &spotColorNames, &spotColorCMYKs);
	}
	else
	if (strContentType == "$BARCODE")
	{
		CString strBarcodeText =			 GetStringToken(pObjectSource->m_strSystemCreationInfo, 1, _T("|"));
		CString strBarcodeType =			 GetStringToken(pObjectSource->m_strSystemCreationInfo, 2, _T("|"));
		CString strTextOption  =			 GetStringToken(pObjectSource->m_strSystemCreationInfo, 3, _T("|"));
		float   fTextSize	   = Ascii2Float(GetStringToken(pObjectSource->m_strSystemCreationInfo, 4, _T("|")));
		if ( bComposite && (strBarcodeText.Find(_T("$2")) != -1) )	// special handling for color name output in case of composite
			CompositeColorBarcode2Stream(pPlate, nTile, strBarcodeText, strBarcodeType, strTextOption, fTextSize, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition, 
										fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop,
										fC, fM, fY, fK, &spotColorNames, &spotColorCMYKs, (pObject) ? &pObject->m_reflinePosParams : NULL);
		else
			Barcode2Stream(pPlate, nTile, strBarcodeText, strBarcodeType, strTextOption, fTextSize, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition,
										fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop,
										bColorSpaceALL, fC, fM, fY, fK, _T(""), &spotColorNames, &spotColorCMYKs, (pObject) ? &pObject->m_reflinePosParams : NULL);
	}
	else
	if (strContentType == "$RECTANGLE")
	{
		//CString strRectangleStyle = GetStringToken(pObjectSource->m_strSystemCreationInfo, 1, _T("|"));
		Rectangle2Stream(pPlate, nTile, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition,
						fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop,
						bColorSpaceALL, fC, fM, fY, fK, &spotColorNames, &spotColorCMYKs, &spotColorTintValues);
	}
}

void CPDFOutput::BuildCompositeColorInfo(CPageTemplate* pObjectTemplate, CPageSource* pObjectSource, CPlate* pPlate,
										 BOOL& bColorSpaceALL, float& fC, float& fM, float& fY, float& fK, CStringArray& spotColorNames, CArray<COLORREF, COLORREF>& spotColorCMYKs, CArray<float, float>& spotColorTintValues)
{
	bColorSpaceALL = FALSE;
	if (pObjectTemplate->m_bMap2AllColors || pObjectTemplate->m_bMap2SpotColors)
	{
		fC = 0.0f; fM = 0.0f; fY = 0.0f; fK = 0.0f;
		if (pObjectTemplate->m_bMap2AllColors)
		{
			for (int i = 0; i < pPlate->m_processColors.GetSize(); i++)
			{
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(pPlate->m_processColors[i]);
				if (pColorDef)
				{
					switch (pColorDef->m_nColorType)
					{
					case CColorDefinition::ProcessC: fC	= 1.0f; break;
					case CColorDefinition::ProcessM: fM	= 1.0f; break;
					case CColorDefinition::ProcessY: fY	= 1.0f; break;
					case CColorDefinition::ProcessK: fK = 1.0f; break;
					}
				}
			}
			bColorSpaceALL = TRUE;
		}
		float fColorIntensity = 100.0f;
		for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
			if (pObjectSource->m_PageSourceHeaders[i].m_strColorName.CompareNoCase(_T("Composite")) == 0)
				fColorIntensity = pObjectSource->m_PageSourceHeaders[i].m_fColorIntensity;
		for (int i = 0; i < pPlate->m_spotColors.GetSize(); i++)
		{
			CString		strColorName = pPlate->m_spotColors[i];
			COLORREF	crCMYK		 = CMYK(20,20,20,20);
			CImpManDoc* pDoc		 = CImpManDoc::GetDoc();
			if (pDoc)
			{
				CColorDefinition* pColorDef = pDoc->m_ColorDefinitionTable.FindColorDefinition(strColorName);
				if (pColorDef)
					crCMYK = pColorDef->m_cmyk;
			}
			if (crCMYK == CMYK(20,20,20,20))
			{
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
				if (pColorDef)
					crCMYK = pColorDef->m_cmyk;
			}
			spotColorNames.Add(strColorName);
			spotColorCMYKs.Add(crCMYK);
			spotColorTintValues.Add(fColorIntensity);
		}

		if (pObjectTemplate->m_bMap2AllColors)
			return;
	}
	
	BOOL bIncludeProcess = FALSE; 
	for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
	{
		CString strColorName	= pObjectSource->m_PageSourceHeaders[i].m_strColorName;
		float	fColorIntensity = pObjectSource->m_PageSourceHeaders[i].m_fColorIntensity;

		if (strColorName.CompareNoCase(_T("Composite")) != 0)	// process- or spotcolor
		{
			CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
			if (pColorDef)
			{
				if (pPlate->SeparationExists(pColorDef->m_strColorName))
				{
					switch (pColorDef->m_nColorType)
					{
					case CColorDefinition::ProcessC:	fC	  = fColorIntensity; bIncludeProcess = TRUE; break;
					case CColorDefinition::ProcessM:	fM	  = fColorIntensity; bIncludeProcess = TRUE; break;
					case CColorDefinition::ProcessY:	fY	  = fColorIntensity; bIncludeProcess = TRUE; break;
					case CColorDefinition::ProcessK:	fK	  = fColorIntensity; bIncludeProcess = TRUE; break;
					case CColorDefinition::Spot:		spotColorNames.Add(pColorDef->m_strColorName);
														spotColorCMYKs.Add(pColorDef->m_cmyk);
														spotColorTintValues.Add(fColorIntensity);
														break;
					}
				}
			}
			else
				if (pPlate->SeparationExists(strColorName))
				{
					spotColorNames.Add(strColorName);
					spotColorCMYKs.Add(CMYK(20,20,20,20));
					spotColorTintValues.Add(fColorIntensity);
				}
		}
		else	
			if (pObjectSource->m_PageSourceHeaders.GetSize() == 1)	// per definition: when only composite is defined w/o additional colors -> take all process colors
			{
				fC = 1.0f; fM = 1.0f; fY = 1.0f; fK = 1.0f;
			}
	} 
	if ( ! bIncludeProcess)
		SET_NOPROCESSCOLOR(fC, fM, fY, fK);
}


void CPDFOutput::Text2Stream(CPlate* pPlate, int nTile, CString strText, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition, float fObjectBBoxCenterX,	float fObjectBBoxCenterY, float fTextHeight, 
																 BOOL bColorSpaceALL, float fC, float fM, float fY, float fK,
															     CStringArray* pSpotColorNames, CArray<COLORREF, COLORREF>* pSpotColorCMYKs, CReflinePositionParams* pReflinePosParams)
{
	if (m_nError != 0)
		return;

	// add content rotation and convert to angles between 0 and 270
	float fAngle;
	switch (nHeadPosition)
	{
	case TOP:	fAngle =   0.0f; break;
	case LEFT:	fAngle =  90.0f; break;
	case BOTTOM:fAngle = 180.0f; break;
	case RIGHT: fAngle = 270.0f; break;
	default:	fAngle =   0.0f; break;
	}

	fObjectBBoxCenterX	  /= fPSPoint;	 fObjectBBoxCenterY	   /= fPSPoint;
	fSheetExposureCenterX /= fPSPoint;	 fSheetExposureCenterY /= fPSPoint;

	float fTextBaseHeight = 0.637f * fPSPoint;	// this value determined by trial and error (fontsize defined as 1 in IPL_PDFSheet_InsertPDFCode()
	float fTextScale	  = fTextHeight / fTextBaseHeight;
	float fOriginX, fOriginY;
	CalcPDFObjectOrigin (fOriginX, fOriginY, fAngle, fSheetExposureCenterX, fSheetExposureCenterY, fObjectBBoxCenterX, fObjectBBoxCenterY);
	IPL_TDMATRIX matrix	= CalcPDFObjectMatrix(fOriginX, fOriginY, fAngle, fTextScale, fTextScale);

	CPrintSheet* pPrintSheet = (pPlate) ? pPlate->GetPrintSheet() : NULL;
	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, nTile, _T(""), pReflinePosParams);

	CString strPDFCode;		
	if (bColorSpaceALL && m_bJDFMarksOutput)	// 2 strategies: when JDF output, colormanagement is done by prepress w/f system, so CS ALL is suitable
												//               when generic PDF sheet output, KIM knows all used colors, so they will be outputted all separately in order to prevent 
												//				 that uneeded process color plates will be generated (i.e. when only black pages, cmy plates will be generated also, even if CS ALL is setted)
	{
		strPDFCode.Format(_T("q BT %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), matrix.a, matrix.b, matrix.c, matrix.d, matrix.h, matrix.v, szDest);
		m_strSysGenMarksStream += strPDFCode;	
		m_bSysGenMarksColorSpaceALL = TRUE;
	}
	else
	{
		if ( INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) )
		{
			// save and restore graphics state, because color will be changed
			strPDFCode.Format(_T("q BT %f %f %f %f k %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), fC, fM, fY, fK, matrix.a, matrix.b, matrix.c, matrix.d, matrix.h, matrix.v, szDest);
			m_strSysGenMarksStream += strPDFCode;	
		}

		if (pSpotColorNames && pSpotColorCMYKs)
		{
			strPDFCode.Format(_T("q BT %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), matrix.a, matrix.b, matrix.c, matrix.d, matrix.h, matrix.v, szDest);
			for (int i = 0; i < pSpotColorNames->GetSize(); i++)
				m_addSysGenSpotColorStreams.AddEntry(strPDFCode, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i));	
		}
	}
}

void CPDFOutput::CompositeColorText2Stream(CPlate* pPlate, int nTile, CString strText, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition, float fObjectBBoxCenterX, float fObjectBBoxCenterY, float fTextHeight, 
																 float fC, float fM, float fY, float fK, CStringArray* pSpotColorNames, CArray<COLORREF, COLORREF>* pSpotColorCMYKs, CReflinePositionParams* pReflinePosParams)
{
	if (m_nError != 0)
		return;

	// add content rotation and convert to angles between 0 and 270
	float fAngle;
	switch (nHeadPosition)
	{
	case TOP:	fAngle =   0.0f; break;
	case LEFT:	fAngle =  90.0f; break;
	case BOTTOM:fAngle = 180.0f; break;
	case RIGHT: fAngle = 270.0f; break;
	default:	fAngle =   0.0f; break;
	}

	fObjectBBoxCenterX	  /= fPSPoint;	 fObjectBBoxCenterY	   /= fPSPoint;
	fSheetExposureCenterX /= fPSPoint;	 fSheetExposureCenterY /= fPSPoint;

	float fTextBaseHeight = 0.637f * fPSPoint;	// this value determined by trial and error (fontsize defined as 1 in IPL_PDFSheet_InsertPDFCode()
	float fTextScale	  = fTextHeight / fTextBaseHeight;
	float fOriginX, fOriginY;
	CalcPDFObjectOrigin (fOriginX, fOriginY, fAngle, fSheetExposureCenterX, fSheetExposureCenterY, fObjectBBoxCenterX, fObjectBBoxCenterY);
	IPL_TDMATRIX matrix	= CalcPDFObjectMatrix(fOriginX, fOriginY, fAngle, fTextScale, fTextScale);

	CPrintSheet* pPrintSheet = (pPlate) ? pPlate->GetPrintSheet() : NULL;
	TCHAR szDest[1024];

	BOOL	bStacked = (strText.Find(_T("$2_=")) != -1) ? TRUE : FALSE;
	CString strPDFCode;		
	if (fC == 1.0f)
	{
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, nTile, theApp.m_colorDefTable.GetCyanName(), pReflinePosParams);
		strPDFCode.Format(_T("q BT %f %f %f %f k %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), 1.0f, 0.0f, 0.0f, 0.0f, matrix.a, matrix.b, matrix.c, matrix.d, matrix.h, matrix.v, szDest);
		m_strSysGenMarksStream += strPDFCode;	
		float fOffset = _tcslen(szDest) * (fTextHeight / fPSPoint);
		fOriginX += (nHeadPosition == TOP)  ? fOffset : ( (nHeadPosition == BOTTOM) ? -fOffset : 0.0f );
		fOriginY += (nHeadPosition == LEFT) ? fOffset : ( (nHeadPosition == RIGHT)  ? -fOffset : 0.0f );
		if ( ! bStacked)
			matrix	= CalcPDFObjectMatrix(fOriginX, fOriginY, fAngle, fTextScale, fTextScale);
	}

	if (fM == 1.0f)
	{
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, nTile, theApp.m_colorDefTable.GetMagentaName(), pReflinePosParams);
		strPDFCode.Format(_T("q BT %f %f %f %f k %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), 0.0f, 1.0f, 0.0f, 0.0f, matrix.a, matrix.b, matrix.c, matrix.d, matrix.h, matrix.v, szDest);
		m_strSysGenMarksStream += strPDFCode;
		float fOffset = _tcslen(szDest) * (fTextHeight / fPSPoint);
		fOriginX += (nHeadPosition == TOP)  ? fOffset : ( (nHeadPosition == BOTTOM) ? -fOffset : 0.0f );
		fOriginY += (nHeadPosition == LEFT) ? fOffset : ( (nHeadPosition == RIGHT)  ? -fOffset : 0.0f );
		if ( ! bStacked)
			matrix	= CalcPDFObjectMatrix(fOriginX, fOriginY, fAngle, fTextScale, fTextScale);
	}

	if (fY == 1.0f)
	{
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, nTile, theApp.m_colorDefTable.GetYellowName(), pReflinePosParams);
		strPDFCode.Format(_T("q BT %f %f %f %f k %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), 0.0f, 0.0f, 1.0f, 0.0f, matrix.a, matrix.b, matrix.c, matrix.d, matrix.h, matrix.v, szDest);
		m_strSysGenMarksStream += strPDFCode;	
		float fOffset = _tcslen(szDest) * (fTextHeight / fPSPoint);
		fOriginX += (nHeadPosition == TOP)  ? fOffset : ( (nHeadPosition == BOTTOM) ? -fOffset : 0.0f );
		fOriginY += (nHeadPosition == LEFT) ? fOffset : ( (nHeadPosition == RIGHT)  ? -fOffset : 0.0f );
		if ( ! bStacked)
			matrix	= CalcPDFObjectMatrix(fOriginX, fOriginY, fAngle, fTextScale, fTextScale);
	}

	if (fK == 1.0f)
	{
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, nTile, theApp.m_colorDefTable.GetKeyName(), pReflinePosParams);
		strPDFCode.Format(_T("q BT %f %f %f %f k %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), 0.0f, 0.0f, 0.0f, 1.0f, matrix.a, matrix.b, matrix.c, matrix.d, matrix.h, matrix.v, szDest);
		m_strSysGenMarksStream += strPDFCode;	
		float fOffset = _tcslen(szDest) * (fTextHeight / fPSPoint);
		fOriginX += (nHeadPosition == TOP)  ? fOffset : ( (nHeadPosition == BOTTOM) ? -fOffset : 0.0f );
		fOriginY += (nHeadPosition == LEFT) ? fOffset : ( (nHeadPosition == RIGHT)  ? -fOffset : 0.0f );
		if ( ! bStacked)
			matrix	= CalcPDFObjectMatrix(fOriginX, fOriginY, fAngle, fTextScale, fTextScale);
	}

	if (pSpotColorNames && pSpotColorCMYKs)
	{
		for (int i = 0; i < pSpotColorNames->GetSize(); i++)
		{
			CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, nTile, pSpotColorNames->ElementAt(i), pReflinePosParams);
			strPDFCode.Format(_T("q BT %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), matrix.a, matrix.b, matrix.c, matrix.d, matrix.h, matrix.v, szDest);
			m_addSysGenSpotColorStreams.AddEntry(strPDFCode, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i));	
		}
	}
}

void CPDFOutput::SectionBar2Stream(float fBarWidth, float fBarHeight, BOOL bNumRotated180, BOOL bHollow, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition, int nOrgHeadPosition,
											 										   float fObjectBBoxCenterX,	float fObjectBBoxCenterY, CLayoutObject* pObject, CPrintSheet* pPrintSheet,
																					   BOOL bColorSpaceALL, float fC, float fM, float fY, float fK, CStringArray* pSpotColorNames, CArray<COLORREF, COLORREF>* pSpotColorCMYKs)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	float fObjectLeft		= fSheetExposureCenterX - fObjectBBoxCenterX;
	float fObjectBottom 	= fSheetExposureCenterY - fObjectBBoxCenterY;
	float fObjectRight  	= fObjectLeft   + fObjectBBoxCenterX * 2.0f;
	float fObjectTop    	= fObjectBottom + fObjectBBoxCenterY * 2.0f;
	CFoldSheet* pFoldSheet	= pPrintSheet->GetFoldSheet(pObject->m_nComponentRefIndex);
	int	  nBarNum			= pFoldSheet->GetNumberOnly();
	if (pObject->m_nAttribute & CLayoutObject::FoldSheetCGMember)
	{
		CFoldSheet* pSimilarFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.CGGetSimilarFoldSheet(*pFoldSheet, INT_MAX);
		nBarNum = (pSimilarFoldSheet) ? pSimilarFoldSheet->GetNumberOnly() : pFoldSheet->GetNumberOnly();
	}
	CRect outBBox;
	outBBox.left	 = CPrintSheetView::WorldToLP(pObject->GetLeft());
	outBBox.bottom	 = CPrintSheetView::WorldToLP(pObject->GetBottom()); 
	outBBox.right	 = CPrintSheetView::WorldToLP(pObject->GetRight());
	outBBox.top		 = CPrintSheetView::WorldToLP(pObject->GetTop());
	CRect coverRect  = CSectionMark::FindCoveringMarks(pPrintSheet, outBBox, pObject->m_strFormatName, pObject->GetLayoutSideIndex());
	BOOL bIsCovered	 = (coverRect.IsRectNull()) ? FALSE : TRUE;
	coverRect.left	 -= CPrintSheetView::WorldToLP(3.0f);
	coverRect.bottom -= CPrintSheetView::WorldToLP(3.0f);
	coverRect.right  += CPrintSheetView::WorldToLP(3.0f);
	coverRect.top    += CPrintSheetView::WorldToLP(3.0f);
	int   nBarHeight = (int)(fBarHeight * 100.0f);
	int	  nMax1      = 0, nMax2  = 0;
	int   nSkip      = 0;
	int   nOffset    = 0;
	if ( (nOrgHeadPosition == TOP) || (nOrgHeadPosition == BOTTOM) )
	{
		nMax1 = ( ! bIsCovered) ? (abs(outBBox.Height()) / nBarHeight) : (outBBox.top      - coverRect.top)  / nBarHeight;
		nMax2 = ( ! bIsCovered) ? 0									   : (coverRect.bottom - outBBox.bottom) / nBarHeight;
		if ( (nMax1 == 0) && (nMax2 == 0) )
			return;
		nSkip = (nBarNum - 1) % (nMax1 + nMax2);
		nOffset = nSkip * nBarHeight;
		if (bIsCovered)
		{
			if (nOrgHeadPosition == TOP)
			{
				if ((nSkip + 1) > nMax1)
					nOffset = (outBBox.top   - coverRect.bottom) + (nSkip - nMax1) * nBarHeight;
			}
			else
			{
				if ((nSkip + 1) > nMax2)
					nOffset = (coverRect.top - outBBox.bottom)   + (nSkip - nMax2) * nBarHeight;
			}
		}
	}
	else
	{
		nMax1 = ( ! bIsCovered) ? (outBBox.Width() / nBarHeight) : (coverRect.left - outBBox.left)    / nBarHeight;
		nMax2 = ( ! bIsCovered) ? 0								 : (outBBox.right  - coverRect.right) / nBarHeight;
		if ( (nMax1 == 0) && (nMax2 == 0) )
			return;
		nSkip = (nBarNum - 1) % (nMax1 + nMax2);
		nOffset = nSkip * nBarHeight;
		if (bIsCovered)
		{
			if (nOrgHeadPosition == LEFT)
			{
				if ((nSkip + 1) > nMax1)
					nOffset = (coverRect.right - outBBox.left)   + (nSkip - nMax1) * nBarHeight;
			}
			else
			{
				if ((nSkip + 1) > nMax2)
					nOffset = (outBBox.right   - coverRect.left) + (nSkip - nMax2) * nBarHeight;
			}
		}
	}

	float fOffset  = (float)nOffset / 100.0f;
	float fBarLeft = 0.0f, fBarBottom = 0.0f, fBarRight = 0.0f, fBarTop = 0.0f;

	switch (nHeadPosition)
	{ 
	case BOTTOM:	fBarLeft   = fSheetExposureCenterX - fBarWidth/2.0f;	fBarRight	= fBarLeft   + fBarWidth;
					fBarBottom = fObjectBottom;								fBarTop		= fBarBottom + fBarHeight;
					fBarBottom+= fOffset; fBarTop += fOffset;
					break;

	case RIGHT:		fBarLeft   = fObjectRight - fBarHeight;					fBarRight	= fBarLeft   + fBarHeight;
					fBarBottom = fSheetExposureCenterY - fBarWidth/2.0f;	fBarTop		= fBarBottom + fBarWidth;
					fBarLeft  -= fOffset; fBarRight -= fOffset;
					break;

	case TOP:		fBarLeft   = fSheetExposureCenterX - fBarWidth/2.0f;    fBarRight	= fBarLeft   + fBarWidth;
					fBarBottom = fObjectTop - fBarHeight;					fBarTop		= fBarBottom + fBarHeight;
					fBarBottom-= fOffset; fBarTop -= fOffset;
					break;

	case LEFT:		fBarLeft   = fObjectLeft;								fBarRight	= fBarLeft   + fBarHeight;
					fBarBottom = fSheetExposureCenterY - fBarWidth/2.0f;	fBarTop		= fBarBottom + fBarWidth;
					fBarLeft  += fOffset; fBarRight += fOffset;
					break;
	}

	CString strPDFCode1, strPDFCode2;

	strPDFCode2.Format(_T(" %f %f m %f %f l %f %f l %f %f l %f %f l b Q\n"),	fBarLeft  /fPSPoint, fBarTop     /fPSPoint,	// b = close, fill and stroke
																				fBarRight /fPSPoint, fBarTop     /fPSPoint,
													 							fBarRight /fPSPoint, fBarBottom  /fPSPoint,
																				fBarLeft  /fPSPoint, fBarBottom  /fPSPoint,
																				fBarLeft  /fPSPoint, fBarTop     /fPSPoint);
	BOOL bSentOut = FALSE;
	if (bColorSpaceALL && m_bJDFMarksOutput)	// 2 strategies: when JDF output, colormanagement is done by prepress w/f system, so CS ALL is suitable
												//               when generic PDF sheet output, KIM knows all used colors, so they will be outputted all separately in order to prevent 
												//				 that uneeded process color plates will be generated (i.e. when only black pages, cmy plates will be generated also, even if CS ALL is setted)
	{
		if (bHollow)
			strPDFCode1 = _T("q n 0.5 w 0 0 0 0 k");	// k  = fillcolor; strokecolor is ColorSpaceAll
		else
			strPDFCode1 = _T("q n 0.5 w");
		m_strSysGenMarksStream += strPDFCode1 + strPDFCode2;	
		m_bSysGenMarksColorSpaceALL = TRUE;
		bSentOut = TRUE;
	}
	else
	{
		if ( INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) )
		{
			if (bHollow)
				strPDFCode1.Format(_T("q n 0.5 w 0 0 0 0 k %f %f %f %f K"), fC, fM, fY, fK);	// k  = fillcolor; K  = strokecolor
			else
				strPDFCode1.Format(_T("q n 0.5 w %f %f %f %f k %f %f %f %f K"), fC, fM, fY, fK, fC, fM, fY, fK);

			m_strSysGenMarksStream += strPDFCode1 + strPDFCode2;
			bSentOut = TRUE;
		}

		if (pSpotColorNames && pSpotColorCMYKs)
		{
			for (int i = 0; i < pSpotColorNames->GetSize(); i++)
			{
				if (bHollow)
					strPDFCode1.Format(_T("q n 0.5 w 0 0 0 0 k"));
				else
					strPDFCode1.Format(_T("q n"));

				m_addSysGenSpotColorStreams.AddEntry(strPDFCode1 + strPDFCode2, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i));	
				bSentOut = TRUE;
			}
		}
	}

	if ( ! bSentOut)
		return;

	CString strNum;
	strNum.Format(_T("%d"), nBarNum);

	float fTextBoxWidth, fTextBoxHeight;
	if (strNum.GetLength() > 1)
	{
		fTextBoxHeight = (bHollow) ? (fBarWidth - 0.4f) : fBarWidth;	// if bHollow leave space to avoid black text touching the black frame
		fTextBoxWidth  = fTextBoxHeight * strNum.GetLength();			// assume height/width ratio is 1
	}
	else
	{
		fTextBoxWidth  = fBarWidth;
		fTextBoxHeight = fTextBoxWidth;		// assume height/width ratio is 1
	}

	int nTextHead = nHeadPosition;
	if (bNumRotated180)
		switch (nTextHead)
		{
		case TOP:	 nTextHead = BOTTOM; break;
		case RIGHT:  nTextHead = LEFT;   break;
		case BOTTOM: nTextHead = TOP;	 break;
		case LEFT:	 nTextHead = RIGHT;  break;
		}

	if (strNum.GetLength() > 1)
		switch (nTextHead)
		{
		case TOP:	 nTextHead = LEFT;   break;
		case RIGHT:  nTextHead = TOP;    break;
		case BOTTOM: nTextHead = RIGHT;	 break;
		case LEFT:	 nTextHead = BOTTOM; break;
		}

	if (bHollow)
		Text2Stream(NULL, 0, strNum, fBarLeft + (fBarRight - fBarLeft) / 2.0f, fBarBottom + (fBarTop - fBarBottom) / 2.0f, nTextHead, 
					fTextBoxWidth / 2.0f, fTextBoxHeight / 2.0f, fTextBoxHeight, bColorSpaceALL, fC, fM, fY, fK, pSpotColorNames, pSpotColorCMYKs);
	else
	{
		CStringArray				spotColorNames; 
		CArray<COLORREF, COLORREF>	spotColorCMYKs; 
		if (bColorSpaceALL && m_bJDFMarksOutput)
			fC = fM = fY = fK = 0.0f;
		else
		{
			SET_NOPROCESSCOLOR(fC, fM, fY, fK);
			spotColorNames.Add(_T("PROCESSWHITE"));	// workaround for section marks where we need white text over bars which has been rendered in spotcolors before
			spotColorCMYKs.Add(WHITE);
		}
		Text2Stream(NULL, 0, strNum, fBarLeft + (fBarRight - fBarLeft) / 2.0f, fBarBottom + (fBarTop - fBarBottom) / 2.0f, nTextHead, 
					fTextBoxWidth / 2.0f, fTextBoxHeight / 2.0f, fTextBoxHeight, FALSE, fC, fM, fY, fK, &spotColorNames, &spotColorCMYKs);
	}
}

void CPDFOutput::CropLine2Stream(float fSheetExposureCenterX, float fSheetExposureCenterY, float fSheetClipBoxLeft,	float fSheetClipBoxBottom, float fSheetClipBoxRight, float fSheetClipBoxTop,
												  BOOL bColorSpaceALL, float fC, float fM, float fY, float fK, CStringArray* pSpotColorNames, CArray<COLORREF, COLORREF>* pSpotColorCMYKs)
{
	float fLineWidth  = min(fSheetClipBoxRight - fSheetClipBoxLeft, fSheetClipBoxTop - fSheetClipBoxBottom);
	float fLineLength = max(fSheetClipBoxRight - fSheetClipBoxLeft, fSheetClipBoxTop - fSheetClipBoxBottom);
	int	  nDirection  = (fSheetClipBoxRight - fSheetClipBoxLeft < fSheetClipBoxTop - fSheetClipBoxBottom) ? VERTICAL : HORIZONTAL;

	float fLineStartX = (nDirection == HORIZONTAL) ? (fSheetExposureCenterX - fLineLength / 2.0f) / fPSPoint : (fSheetExposureCenterX					  ) / fPSPoint;
	float fLineStartY = (nDirection == HORIZONTAL) ? (fSheetExposureCenterY						) / fPSPoint : (fSheetExposureCenterY - fLineLength / 2.0f) / fPSPoint;
	float fLineEndX	  = (nDirection == HORIZONTAL) ? (fSheetExposureCenterX + fLineLength / 2.0f) / fPSPoint : (fSheetExposureCenterX					  ) / fPSPoint;
	float fLineEndY	  = (nDirection == HORIZONTAL) ? (fSheetExposureCenterY						) / fPSPoint : (fSheetExposureCenterY + fLineLength / 2.0f) / fPSPoint;
	fLineWidth  /= fPSPoint;
	fLineLength /= fPSPoint;

	CString strPDFCode;
	if (bColorSpaceALL && m_bJDFMarksOutput)	// 2 strategies: when JDF output, colormanagement is done by prepress w/f system, so CS ALL is suitable
												//               when generic PDF sheet output, KIM knows all used colors, so they will be outputted all separately in order to prevent 
												//				 that uneeded process color plates will be generated (i.e. when only black pages, cmy plates will be generated also, even if CS ALL is setted)
	{
		strPDFCode.Format(_T("q n %f w %f %f m %f %f l s Q\n"), fLineWidth, fLineStartX, fLineStartY, fLineEndX, fLineEndY);
		m_strSysGenMarksStream += strPDFCode;	
		m_bSysGenMarksColorSpaceALL = TRUE;
	}
	else
	{
		if ( INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) )
		{
			// save and restore graphics state, because color will be changed
			strPDFCode.Format(_T("q n %f w %f %f %f %f K %f %f m %f %f l s Q\n"), fLineWidth, fC, fM, fY, fK, fLineStartX, fLineStartY, fLineEndX, fLineEndY);
			m_strSysGenMarksStream += strPDFCode;	
		}

		if (pSpotColorNames && pSpotColorCMYKs)
		{
			strPDFCode.Format(_T("n %f w %f %f m %f %f l s\n"), fLineWidth, fLineStartX, fLineStartY, fLineEndX, fLineEndY);
			for (int i = 0; i < pSpotColorNames->GetSize(); i++)
				m_addSysGenSpotColorStreams.AddEntry(strPDFCode, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i));	
		}
	}
}

void CPDFOutput::FoldLine2Stream(BOOL bSolid, BOOL bTransparent, float fSheetExposureCenterX, float fSheetExposureCenterY, 
												  float fSheetClipBoxLeft,	   float fSheetClipBoxBottom, 
												  float fSheetClipBoxRight,	   float fSheetClipBoxTop,
												  BOOL bColorSpaceALL, float fC, float fM, float fY, float fK,
												  CStringArray* pSpotColorNames, CArray<COLORREF, COLORREF>* pSpotColorCMYKs)
{
	float fLineWidth  = min(fSheetClipBoxRight - fSheetClipBoxLeft, fSheetClipBoxTop - fSheetClipBoxBottom);
	float fLineLength = max(fSheetClipBoxRight - fSheetClipBoxLeft, fSheetClipBoxTop - fSheetClipBoxBottom);
	int	  nDirection  = (fSheetClipBoxRight - fSheetClipBoxLeft < fSheetClipBoxTop - fSheetClipBoxBottom) ? VERTICAL : HORIZONTAL;

	float fLineStartX = (nDirection == HORIZONTAL) ? (fSheetExposureCenterX - fLineLength / 2.0f) / fPSPoint : (fSheetExposureCenterX					  ) / fPSPoint;
	float fLineStartY = (nDirection == HORIZONTAL) ? (fSheetExposureCenterY						) / fPSPoint : (fSheetExposureCenterY - fLineLength / 2.0f) / fPSPoint;
	float fLineEndX	  = (nDirection == HORIZONTAL) ? (fSheetExposureCenterX + fLineLength / 2.0f) / fPSPoint : (fSheetExposureCenterX					  ) / fPSPoint;
	float fLineEndY	  = (nDirection == HORIZONTAL) ? (fSheetExposureCenterY						) / fPSPoint : (fSheetExposureCenterY + fLineLength / 2.0f) / fPSPoint;

	float fOutlineStartX = (nDirection == HORIZONTAL) ? fLineStartX - fLineWidth / 2.0f : fLineStartX;
	float fOutlineStartY = (nDirection == VERTICAL	) ? fLineStartY - fLineWidth / 2.0f : fLineStartY;
	float fOutlineEndX	 = (nDirection == HORIZONTAL) ? fLineEndX   + fLineWidth / 2.0f : fLineEndX;
	float fOutlineEndY	 = (nDirection == VERTICAL	) ? fLineEndY   + fLineWidth / 2.0f : fLineEndY;

	CString strPDFCode = "", strDashCode = (bSolid) ? _T("[]0 d") : _T("[3 5]0 d");

	fLineWidth  /= fPSPoint;
	fLineLength /= fPSPoint;

	if ( ! bTransparent)
	{
		strPDFCode.Format(_T("q n 1 J %s %f w %f %f %f %f K %f %f m %f %f l s Q\n"), strDashCode, 2.0f * fLineWidth, 0.0f, 0.0f, 0.0f, 0.0f, fOutlineStartX, fOutlineStartY, fOutlineEndX, fOutlineEndY);
		m_strSysGenMarksStream += strPDFCode;	
	}

	if (bColorSpaceALL && m_bJDFMarksOutput)	// 2 strategies: when JDF output, colormanagement is done by prepress w/f system, so CS ALL is suitable
												//               when generic PDF sheet output, KIM knows all used colors, so they will be outputted all separately in order to prevent 
												//				 that uneeded process color plates will be generated (i.e. when only black pages, cmy plates will be generated also, even if CS ALL is setted)
	{
		strPDFCode.Format(_T("q n 1 J %s %f w %f %f m %f %f l s Q\n"), strDashCode, fLineWidth, fLineStartX, fLineStartY, fLineEndX, fLineEndY);
		m_strSysGenMarksStream += strPDFCode;	
		m_bSysGenMarksColorSpaceALL = TRUE;
	}
	else
	{
		if ( INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) )
		{
			// save and restore graphics state, because color will be changed
			strPDFCode.Format(_T("q n 1 J %s %f w %f %f %f %f K %f %f m %f %f l s Q\n"), strDashCode, fLineWidth, fC, fM, fY, fK, fLineStartX, fLineStartY, fLineEndX, fLineEndY);
			m_strSysGenMarksStream += strPDFCode;	
		}

		if (pSpotColorNames && pSpotColorCMYKs)
		{
			strPDFCode.Format(_T("n 1 J %s %f w %f %f m %f %f l s\n"), strDashCode, fLineWidth, fLineStartX, fLineStartY, fLineEndX, fLineEndY);
			for (int i = 0; i < pSpotColorNames->GetSize(); i++)
				m_addSysGenSpotColorStreams.AddEntry(strPDFCode, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i));	
		}
	}
}

void CPDFOutput::CompositeColorBarcode2Stream(CPlate* pPlate, int nTile, CString strBarcodeText, const CString& strBarcodeType, CString strTextOption, float fTextSize, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition, 
												float fSheetClipBoxLeft, float fSheetClipBoxBottom, float fSheetClipBoxRight, float fSheetClipBoxTop,
									 			float fC, float fM, float fY, float fK, CStringArray* pSpotColorNames, CArray<COLORREF, COLORREF>* pSpotColorCMYKs, CReflinePositionParams* pReflinePosParams)
{
	if (m_nError != 0)
		return;

	CPrintSheet* pPrintSheet = (pPlate) ? pPlate->GetPrintSheet() : NULL;
	CSize barcodeExtent;
	float fOffsetX = 0.0f, fOffsetY = 0.0f;

	BOOL	bStacked = (strBarcodeText.Find(_T("$2_=")) != -1) ? TRUE : FALSE;
	CString strPDFCode;		
	if (fC == 1.0f)
	{
		barcodeExtent = Barcode2Stream(pPlate, nTile, strBarcodeText, strBarcodeType, strTextOption, fTextSize, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition,
										fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop, FALSE, 1.0f, 0.0f, 0.0f, 0.0f, theApp.m_colorDefTable.GetCyanName(), NULL, NULL, pReflinePosParams);
		if ( ! bStacked)
		{
			fOffsetX = (float)( (nHeadPosition == TOP)  ? barcodeExtent.cx : ( (nHeadPosition == BOTTOM) ? -barcodeExtent.cx : 0.0f ) ) * fPSPoint;
			fOffsetY = (float)( (nHeadPosition == LEFT) ? barcodeExtent.cy : ( (nHeadPosition == RIGHT)  ? -barcodeExtent.cy : 0.0f ) ) * fPSPoint;
			fSheetExposureCenterX += fOffsetX; fSheetExposureCenterY += fOffsetY;
			fSheetClipBoxLeft += fOffsetX; fSheetClipBoxBottom += fOffsetY; fSheetClipBoxRight += fOffsetX; fSheetClipBoxTop += fOffsetY;
		}
	}

	if (fM == 1.0f)
	{
		barcodeExtent = Barcode2Stream(pPlate, nTile, strBarcodeText, strBarcodeType, strTextOption, fTextSize, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition,
										fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop, FALSE, 0.0f, 1.0f, 0.0f, 0.0f, theApp.m_colorDefTable.GetMagentaName(), NULL, NULL, pReflinePosParams);
		if ( ! bStacked)
		{
			fOffsetX = (float)( (nHeadPosition == TOP)  ? barcodeExtent.cx : ( (nHeadPosition == BOTTOM) ? -barcodeExtent.cx : 0.0f ) ) * fPSPoint;
			fOffsetY = (float)( (nHeadPosition == LEFT) ? barcodeExtent.cy : ( (nHeadPosition == RIGHT)  ? -barcodeExtent.cy : 0.0f ) ) * fPSPoint;
			fSheetExposureCenterX += fOffsetX; fSheetExposureCenterY += fOffsetY;
			fSheetClipBoxLeft += fOffsetX; fSheetClipBoxBottom += fOffsetY; fSheetClipBoxRight += fOffsetX; fSheetClipBoxTop += fOffsetY;
		}
	}

	if (fY == 1.0f)
	{
		barcodeExtent = Barcode2Stream(pPlate, nTile, strBarcodeText, strBarcodeType, strTextOption, fTextSize, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition,
										fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop, FALSE, 0.0f, 0.0f, 1.0f, 0.0f, theApp.m_colorDefTable.GetYellowName(), NULL, NULL, pReflinePosParams);
		if ( ! bStacked)
		{
			fOffsetX = (float)( (nHeadPosition == TOP)  ? barcodeExtent.cx : ( (nHeadPosition == BOTTOM) ? -barcodeExtent.cx : 0.0f ) ) * fPSPoint;
			fOffsetY = (float)( (nHeadPosition == LEFT) ? barcodeExtent.cy : ( (nHeadPosition == RIGHT)  ? -barcodeExtent.cy : 0.0f ) ) * fPSPoint;
			fSheetExposureCenterX += fOffsetX; fSheetExposureCenterY += fOffsetY; 
			fSheetClipBoxLeft += fOffsetX; fSheetClipBoxBottom += fOffsetY; fSheetClipBoxRight += fOffsetX; fSheetClipBoxTop += fOffsetY;
		}
	}

	if (fK == 1.0f)
	{
		barcodeExtent = Barcode2Stream(pPlate, nTile, strBarcodeText, strBarcodeType, strTextOption, fTextSize, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition,
										fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop, FALSE, 0.0f, 0.0f, 0.0f, 1.0f, theApp.m_colorDefTable.GetKeyName(), NULL, NULL, pReflinePosParams);
		if ( ! bStacked)
		{
			fOffsetX = (float)( (nHeadPosition == TOP)  ? barcodeExtent.cx : ( (nHeadPosition == BOTTOM) ? -barcodeExtent.cx : 0.0f ) ) * fPSPoint;
			fOffsetY = (float)( (nHeadPosition == LEFT) ? barcodeExtent.cy : ( (nHeadPosition == RIGHT)  ? -barcodeExtent.cy : 0.0f ) ) * fPSPoint;
			fSheetExposureCenterX += fOffsetX; fSheetExposureCenterY += fOffsetY; 
			fSheetClipBoxLeft += fOffsetX; fSheetClipBoxBottom += fOffsetY; fSheetClipBoxRight += fOffsetX; fSheetClipBoxTop += fOffsetY;
		}
	}

	if (pSpotColorNames && pSpotColorCMYKs)
	{
		TCHAR		 szDest[1024];
		CString		 strPDFCode, strBarcode;
		IPL_TDMATRIX textMatrix = {0,0,0,0,0,0};
		for (int i = 0; i < pSpotColorNames->GetSize(); i++)
		{
			CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strBarcodeText, pPrintSheet, pPlate, nTile, pSpotColorNames->ElementAt(i), pReflinePosParams);
			MakeBarcode(strBarcode, textMatrix, szDest, strBarcodeText, strBarcodeType, strTextOption, fTextSize, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition, fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop);

			strPDFCode.Format(_T("n %s s\n"), strBarcode);
			m_addSysGenSpotColorStreams.AddEntry(strPDFCode, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i));	

			strPDFCode.Format(_T("q BT %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), textMatrix.a, textMatrix.b, textMatrix.c, textMatrix.d, textMatrix.h, textMatrix.v, szDest);
			m_addSysGenSpotColorStreams.AddEntry(strPDFCode, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i));	
		}
	}
}

CSize CPDFOutput::Barcode2Stream(CPlate* pPlate, int nTile, const CString& strBarcodeText, const CString& strBarcodeType, CString strTextOption, float fTextSize, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
								float fSheetClipBoxLeft, float fSheetClipBoxBottom, float fSheetClipBoxRight, float fSheetClipBoxTop,
								BOOL bColorSpaceALL, float fC, float fM, float fY, float fK, CString strColorName, CStringArray* pSpotColorNames, CArray<COLORREF, COLORREF>* pSpotColorCMYKs, CReflinePositionParams* pReflinePosParams)
{
	if (m_nError != 0)
		return CSize(0, 0);

	CPrintSheet* pPrintSheet = (pPlate) ? pPlate->GetPrintSheet() : NULL;
	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strBarcodeText, pPrintSheet, pPlate, nTile, strColorName, pReflinePosParams);

	CString		 strPDFCode, strBarcode;
	IPL_TDMATRIX textMatrix = {0,0,0,0,0,0};
	CSize sizeBarcode = MakeBarcode(strBarcode, textMatrix, szDest, strBarcodeText, strBarcodeType, strTextOption, fTextSize, fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition, fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop);

	if (bColorSpaceALL && m_bJDFMarksOutput)	// 2 strategies: when JDF output, colormanagement is done by prepress w/f system, so CS ALL is suitable
												//               when generic PDF sheet output, KIM knows all used colors, so they will be outputted all separately in order to prevent 
												//				 that uneeded process color plates will be generated (i.e. when only black pages, cmy plates will be generated also, even if CS ALL is setted)
	{
		strPDFCode.Format(_T("q n %s s Q\n"), strBarcode);
		m_strSysGenMarksStream += strPDFCode;	
		m_bSysGenMarksColorSpaceALL = TRUE;
	}
	else
	{
		if ( INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) )
		{
			// save and restore graphics state, because color will be changed
			strPDFCode.Format(_T("q n %f %f %f %f k %s s Q\n"), fC, fM, fY, fK, strBarcode);
			m_strSysGenMarksStream += strPDFCode;	
		}

		if (pSpotColorNames && pSpotColorCMYKs)
		{
			strPDFCode.Format(_T("n %s s\n"), strBarcode);
			for (int i = 0; i < pSpotColorNames->GetSize(); i++)
				m_addSysGenSpotColorStreams.AddEntry(strPDFCode, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i));	
		}
	}

	if ( ! strTextOption.IsEmpty())
	{
		if (bColorSpaceALL && m_bJDFMarksOutput)	// 2 strategies: when JDF output, colormanagement is done by prepress w/f system, so CS ALL is suitable
													//               when generic PDF sheet output, KIM knows all used colors, so they will be outputted all separately in order to prevent 
													//				 that uneeded process color plates will be generated (i.e. when only black pages, cmy plates will be generated also, even if CS ALL is setted)
		{
			strPDFCode.Format(_T("q BT %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), textMatrix.a, textMatrix.b, textMatrix.c, textMatrix.d, textMatrix.h, textMatrix.v, szDest);
			m_strSysGenMarksStream += strPDFCode;	
			m_bSysGenMarksColorSpaceALL = TRUE;
		}
		else
		{
			if ( INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) )
			{
				// save and restore graphics state, because color will be changed
				strPDFCode.Format(_T("q BT %f %f %f %f k %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), fC, fM, fY, fK, textMatrix.a, textMatrix.b, textMatrix.c, textMatrix.d, textMatrix.h, textMatrix.v, szDest);
				m_strSysGenMarksStream += strPDFCode;	
			}

			if (pSpotColorNames && pSpotColorCMYKs)
			{
				strPDFCode.Format(_T("q BT %f %f %f %f %f %f Tm (%s) Tj ET Q\n"), textMatrix.a, textMatrix.b, textMatrix.c, textMatrix.d, textMatrix.h, textMatrix.v, szDest);
				for (int i = 0; i < pSpotColorNames->GetSize(); i++)
					m_addSysGenSpotColorStreams.AddEntry(strPDFCode, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i));	
			}
		}
	}

	return sizeBarcode;
}

CSize CPDFOutput::MakeBarcode(CString& strBarcode, IPL_TDMATRIX& textMatrix, const CString strResolvedText, const CString& strBarcodeText, const CString& strBarcodeType, CString strTextOption, float fTextSize, float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
								float fSheetClipBoxLeft, float fSheetClipBoxBottom, float fSheetClipBoxRight, float fSheetClipBoxTop)
{
	if (m_nError != 0)
		return CSize(0, 0);

	ERRCODE eCode = S_OK;
	t_BarCode* pBarCode;
	eCode = BCAlloc(&pBarCode);
	if (eCode != S_OK)
		return CSize(0, 0);

	BCLicenseMe(_T("Mem: Tegeler Software-Engineering"), eLicKindDeveloper, 1, _T("9859A097187FB2F2AD47FC15C3ACBFFA"), eLicProd2D);

	int nBarcodeType = CBarcodeMark::GetBarcodeTypeFromString(strBarcodeType);

	eCode = CBarcodeMark::GenerateBarcode(pBarCode, strResolvedText, nBarcodeType, nHeadPosition); 

	float fBCLeft   = fSheetClipBoxLeft, fBCTop = fSheetClipBoxTop, fBCRight = fSheetClipBoxRight, fBCBottom = fSheetClipBoxBottom;
	float fTextLeft = fSheetClipBoxLeft, fTextTop = fSheetClipBoxTop, fTextRight = fSheetClipBoxRight, fTextBottom = fSheetClipBoxBottom;

	int nTextOption = CBarcodeMark::MapTextOption(strTextOption, nHeadPosition);
	switch (nTextOption)
	{
	case TOP:		fTextBottom	= fBCTop	- fTextSize;
					fBCTop		= fTextBottom;
					break;
	case BOTTOM:	fTextTop	= fBCBottom + fTextSize;
					fBCBottom	= fTextTop;
					break;
	case LEFT:		fTextRight	= fBCLeft	+ fTextSize;
					fBCLeft		= fTextRight;
					break;
	case RIGHT:		fTextLeft   = fBCRight	- fTextSize;
					fBCRight	= fTextLeft;
					break;
	}

	fTextLeft   = fTextLeft/fPSPoint;
	fTextTop    = fTextTop/fPSPoint;
	fTextRight  = fTextRight/fPSPoint;
	fTextBottom = fTextBottom/fPSPoint;

	CString strText = strResolvedText;
	if (strBarcodeText.Find(_T("$P")) >= 0)	// includes AsirCode -> show decoded text
	{
		long nCode = _ttoi(strResolvedText);
		strText.Format(_T("%02d"), nCode%100);	// last 2 digits contains sheet number
	}
	if (CBarcodeMark::MapTextOption(strTextOption) == RIGHT)	// text originally right (w/o head position) -> one char space between text and barcode
		strText.Insert(0, ' ');

	float fTextBoxHeight = ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? fabs(fTextTop - fTextBottom) : ((fTextRight - fTextLeft));
	float fTextBoxWidth  = ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? (fTextRight - fTextLeft)		: fabs(fTextTop - fTextBottom);
	fTextBoxHeight *= 0.55f;

	BC_CUSTOMDRAWDATA customData;
	customData.ptTargetCoords.x = (long)(fBCLeft * 1000.0f + 0.5f);
	customData.ptTargetCoords.y = (long)(fBCTop  * 1000.0f + 0.5f);		// coord origin is upper left

	if (theApp.m_nAddOnsLicensed & CImpManApp::Barcode)
	{
		if (eCode == S_OK)
		{
			m_strSysGenMarksStream.Empty();
			if (nTextOption != 0)
				Rectangle2Stream(NULL, 0, (fTextLeft + fTextBoxWidth)*fPSPoint, (fTextBottom + fTextBoxHeight)*fPSPoint, TOP, fTextLeft*fPSPoint, fTextBottom*fPSPoint, fTextRight*fPSPoint, fTextTop*fPSPoint, FALSE, 0.01f, 0.01f, 0.01f, 0.01f); 
			Rectangle2Stream(NULL, 0, fBCLeft + (fBCRight - fBCLeft) / 2.0f, fBCBottom + (fBCTop - fBCBottom) / 2.0f, TOP, fBCLeft, fBCBottom, fBCRight, fBCTop, FALSE, 0.01f, 0.01f, 0.01f, 0.01f); 
			CString& rstrPDFStream = ((BC_CUSTOMDRAWDATA*)&customData)->strPDFBuffer;
			rstrPDFStream += m_strSysGenMarksStream;
			m_strSysGenMarksStream.Empty();

			BCSetFuncDrawBar(pBarCode, &CBarcodeMark::CustomDrawBarcode);
			BCSetCBData(pBarCode, &customData);
			CRect rcBarcode((long)(fBCLeft * 1000.0f + 0.5f), (long)(fBCTop * 1000.0f + 0.5f), (long)(fBCRight * 1000.0f + 0.5f), (long)(fBCBottom * 1000.0f + 0.5f));
			BCDrawCB(pBarCode, 0, &rcBarcode);
		}
	}
	
	if (pBarCode)
		BCFree(pBarCode);

	strBarcode = customData.strPDFBuffer;

	if (nTextOption != 0)
	{
		float fObjectBBoxCenterX = fTextBoxWidth  / 2.0f;
		float fObjectBBoxCenterY = fTextBoxHeight / 2.0f;
		float fTextHeight		 = fTextBoxHeight;
		// add content rotation and convert to angles between 0 and 270
		float fAngle;
		switch (nHeadPosition)
		{
		case TOP:	fAngle =   0.0f; break;
		case LEFT:	fAngle =  90.0f; break;
		case BOTTOM:fAngle = 180.0f; break;
		case RIGHT: fAngle = 270.0f; break;
		default:	fAngle =   0.0f; break;
		}
		float fTextBaseHeight = 0.637f;	// this value determined by trial and error (fontsize defined as 1 in IPL_PDFSheet_InsertPDFCode()
		float fTextScale	  = fTextHeight / fTextBaseHeight;
		float fOriginX, fOriginY;
		CalcPDFObjectOrigin (fOriginX, fOriginY, fAngle, fTextLeft + (fTextRight - fTextLeft)/2.0f, fTextBottom + (fTextTop - fTextBottom)/2.0f, fObjectBBoxCenterX, fObjectBBoxCenterY);
		textMatrix = CalcPDFObjectMatrix(fOriginX, fOriginY, fAngle, fTextScale, fTextScale);
	}

	return CSize((long)((fBCRight - fBCLeft) / fPSPoint + 0.5f), (long)(abs((fBCTop - fBCBottom) / fPSPoint + 0.5f)));
}

void CPDFOutput::Rectangle2Stream(CPlate* pPlate, int nTile,  float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
								  float fSheetClipBoxLeft, float fSheetClipBoxBottom, float fSheetClipBoxRight,	float fSheetClipBoxTop,
								  BOOL bColorSpaceALL, float fC, float fM, float fY, float fK, CStringArray* pSpotColorNames, CArray<COLORREF, COLORREF>* pSpotColorCMYKs, CArray<float, float>* pSpotColorTintValues)
{
	if (m_nError != 0)
		return;
	float fFrameLeft	= fSheetClipBoxLeft;
	float fFrameBottom	= fSheetClipBoxBottom;
	float fFrameRight   = fSheetClipBoxRight;
	float fFrameTop     = fSheetClipBoxTop;

	CString strFrameCode;
	strFrameCode.Format(_T(" %f %f m %f %f l %f %f l %f %f l %f %f l b\n"),	fFrameLeft  /fPSPoint, fFrameTop     /fPSPoint,	// b = close, fill and stroke
																			fFrameRight /fPSPoint, fFrameTop     /fPSPoint,
													 						fFrameRight /fPSPoint, fFrameBottom  /fPSPoint,
																			fFrameLeft  /fPSPoint, fFrameBottom  /fPSPoint,
																			fFrameLeft  /fPSPoint, fFrameTop     /fPSPoint);

	float	fLineWidth = 0.4f;
	CString strPDFCode;
	if (bColorSpaceALL && m_bJDFMarksOutput)	// 2 strategies: when JDF output, colormanagement is done by prepress w/f system, so CS ALL is suitable
												//               when generic PDF sheet output, KIM knows all used colors, so they will be outputted all separately in order to prevent 
												//				 that uneeded process color plates will be generated (i.e. when only black pages, cmy plates will be generated also, even if CS ALL is setted)
	{
		strPDFCode.Format(_T("q n %f w %s Q\n"), fLineWidth, strFrameCode);	// fillcolor = strokecolor = ColorSpaceALL
		m_strSysGenMarksStream += strPDFCode;	
		m_bSysGenMarksColorSpaceALL = TRUE;
	}
	else
	{
		if ( INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) )
		{
			// save and restore graphics state, because color will be changed
			strPDFCode.Format(_T("q n %f w %f %f %f %f k %f %f %f %f K %s Q\n"), fLineWidth, fC, fM, fY, fK, fC, fM, fY, fK, strFrameCode);	// k  = fillcolor; K  = strokecolor
			m_strSysGenMarksStream += strPDFCode;
		}

		if (pSpotColorNames && pSpotColorCMYKs)
		{
			strPDFCode.Format(_T("n %f w %s\n"), fLineWidth, strFrameCode);
			for (int i = 0; i < pSpotColorNames->GetSize(); i++)
				m_addSysGenSpotColorStreams.AddEntry(strPDFCode, pSpotColorNames->ElementAt(i), pSpotColorCMYKs->ElementAt(i), pSpotColorTintValues->ElementAt(i));	
		}
	}
}

void CPDFOutput::Frame2Stream(int nSheetIndex, float fFrameLeft, float fFrameBottom, float fFrameWidth, float fFrameHeight, CPlate* pPlate, CPageSource& rFrameSource)
{
	float fFrameRight   = fFrameLeft   + fFrameWidth;
	float fFrameTop     = fFrameBottom + fFrameHeight;
	float fFrameLLeft   = fFrameLeft   - 3.0f;
	float fFrameRRight  = fFrameRight  + 3.0f;
	float fFrameTTop    = fFrameTop    + 3.0f;
	float fFrameBBottom = fFrameBottom - 3.0f;

	CString strFrameCode;
	strFrameCode.Format(_T("%f %f m %f %f l s %f %f m %f %f l s %f %f m %f %f l s %f %f m %f %f l s\n"),fFrameLLeft  /fPSPoint, fFrameTop     /fPSPoint,
																									fFrameRRight /fPSPoint, fFrameTop     /fPSPoint,
																									fFrameRight  /fPSPoint, fFrameTTop    /fPSPoint,
																									fFrameRight  /fPSPoint, fFrameBBottom /fPSPoint,
													 												fFrameRRight /fPSPoint, fFrameBottom  /fPSPoint,
																									fFrameLLeft  /fPSPoint, fFrameBottom  /fPSPoint,
																									fFrameLeft   /fPSPoint, fFrameBBottom /fPSPoint,
																									fFrameLeft   /fPSPoint, fFrameTTop    /fPSPoint);
	CString strPDFCode, strPDFCode2;
	if ( ! rFrameSource.m_PageSourceHeaders.GetSize() || m_bCalibration)	// if no colors defined or if calibrating -> output in all colorspaces
		strPDFCode.Format(_T("n 0.4 w %s"), strFrameCode);
	else
	{
		if ( (pPlate->m_strPlateName.CompareNoCase(_T("Composite")) == 0) || ( pPlate->m_strPlateName.IsEmpty() && (m_nTargetType == CPDFTargetProperties::TypeJDFFolder) ) )
		{
			int nC = 0, nM = 0, nY = 0, nK = 0;
			for (int i = 0; i < rFrameSource.m_PageSourceHeaders.GetSize(); i++)
			{
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(rFrameSource.m_PageSourceHeaders[i].m_strColorName);
				if ( ! pColorDef)
					continue;

				switch (pColorDef->m_nColorType)
				{
				case CColorDefinition::ProcessC:	nC = 1; break;
				case CColorDefinition::ProcessM:	nM = 1; break;
				case CColorDefinition::ProcessY:	nY = 1; break;
				case CColorDefinition::ProcessK:	nK = 1; break;
				case CColorDefinition::Spot:		strPDFCode2.Format(_T("n 0.4 w %s"), strFrameCode);
													PDFSheet_AddSysGenStream3(nSheetIndex, strPDFCode2, pColorDef->m_strColorName, pColorDef->m_cmyk, 1.0f, "KIM_GeneratedControlFrame");	
													break;
				}
			}  

			if ( (nC == 1) || (nM == 1) || (nY == 1) || (nK == 1) )
				strPDFCode.Format(_T("n 0.4 w %d %d %d %d K %s"), nC, nM, nY, nK, strFrameCode);
		}
		else
		{
			BOOL bFound = FALSE;
			for (int i = 0; i < rFrameSource.m_PageSourceHeaders.GetSize(); i++)
			{
				if (rFrameSource.m_PageSourceHeaders[i].m_strColorName == pPlate->m_strPlateName)
				{
					bFound = TRUE;
					break;
				}
			}
			if (bFound)
				strPDFCode.Format(_T("n 0.4 w %s"), strFrameCode);
		}
	}

	m_strSysGenMarksStream += strPDFCode;
}

void CPDFOutput::Path2Stream(int nSheetIndex, const CString& strStream, CPlate* pPlate, CPageSource& rFrameSource)
{
	CString strPDFCode, strPDFCode2;
	if ( ! rFrameSource.m_PageSourceHeaders.GetSize())	// if no colors defined -> output in all colorspaces
		strPDFCode.Format(_T("n 0.4 w %s"), strStream);
	else
	{
		if (pPlate->m_strPlateName.CompareNoCase(_T("Composite")) == 0)
		{
			int nC = 0, nM = 0, nY = 0, nK = 0;
			for (int i = 0; i < rFrameSource.m_PageSourceHeaders.GetSize(); i++)
			{
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(rFrameSource.m_PageSourceHeaders[i].m_strColorName);
				if ( ! pColorDef)
					continue;

				switch (pColorDef->m_nColorType)
				{
				case CColorDefinition::ProcessC:	nC = 1; break;
				case CColorDefinition::ProcessM:	nM = 1; break;
				case CColorDefinition::ProcessY:	nY = 1; break;
				case CColorDefinition::ProcessK:	nK = 1; break;
				case CColorDefinition::Spot:		strPDFCode2.Format(_T("n 0.4 w %s"), strStream);
													PDFSheet_AddSysGenStream3(nSheetIndex, strPDFCode2, pColorDef->m_strColorName, pColorDef->m_cmyk, 1.0f, "KIM_GeneratedGraphicPath");	
													break;
				}
			}  

			if ( (nC == 1) || (nM == 1) || (nY == 1) || (nK == 1) )
				strPDFCode.Format(_T("n 0.4 w %d %d %d %d K %s"), nC, nM, nY, nK, strStream);
		}
		else
		{
			BOOL bFound = FALSE;
			for (int i = 0; i < rFrameSource.m_PageSourceHeaders.GetSize(); i++)
			{
				if (rFrameSource.m_PageSourceHeaders[i].m_strColorName == pPlate->m_strPlateName)
				{
					bFound = TRUE;
					break;
				}
			}
			if (bFound)
				strPDFCode.Format(_T("n 0.4 w %s"), strStream);
		}
	}

	m_strSysGenMarksStream += strPDFCode;
}


BOOL CPDFOutput::PDFSheet_AddSysGenStream(int nSheetIndex, CString strPDFStream, CString strID, BOOL bOverprint)
{
	IPL_TPDFCODEID objid;
	objid.size			= sizeof( IPL_TPDFCODEID);
	objid.pdfcode		= strPDFStream.GetBuffer(strPDFStream.GetLength() + 1);
	objid.pdfcodelen	= strlen( objid.pdfcode);
	objid.allcolorspace	= (m_bSysGenMarksColorSpaceALL) ? TRUE : FALSE;
	objid.callasID[0]	= '\0';
	_tcscpy_s( (TCHAR*)objid.userID, MAX_PATH, strID);

	IPL_TEXTGRSTATE extgrstate;
    extgrstate.size			= sizeof(IPL_TEXTGRSTATE);
    extgrstate.mask			= IPL_eStrokeOPrint | IPL_eFillOPrint | IPL_eOprintMode;
	extgrstate.strokeoprint = (bOverprint) ? true : false;
    extgrstate.filloprint	= (bOverprint) ? true : false;
    extgrstate.oprintmode	= 1;

	IPL_TPDFCODEFONT pdfcodefont;
	pdfcodefont.size	 = sizeof(IPL_TPDFCODEFONT);
	pdfcodefont.name	 = IPL_eHelvetica;
	pdfcodefont.encoding = IPL_eWinAnsi;
	pdfcodefont.fontsize = 1;
	pdfcodefont.cspacing = 0.0f;
	pdfcodefont.wspacing = 0.0f;

	IPL_int32_t nResult = IPL_PDFSheet_InsertPDFCode(m_hPDFFile, nSheetIndex, &objid, NULL, &extgrstate, &pdfcodefont);
	if (nResult)
	{
		CString			strMsg;
		PTB_uint32_t	nBufSize = 1024;
		IPL_utf8_char_t szMsgUTF8[1024];
		IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
		PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();

		KIMOpenLogMessage(strMsg, MB_ICONERROR);

		m_nError = CImpManApp::PDFOutputError;
		return FALSE;
	}
	return TRUE;
}

BOOL CPDFOutput::PDFSheet_AddSysGenStream3(int nSheetIndex, CString strPDFStream, CString strColorName, COLORREF crAlternate, float fTintValue, CString strID)
{
	//return PDFSheet_AddSysGenStream3(nSheetIndex, strPDFStream, strColorName, fCyan, fMagenta, fYellow, fBlack, strID);
	return PDFSheet_AddSysGenStream3(nSheetIndex, strPDFStream, strColorName, GetCValue(crAlternate)/100.0f, GetMValue(crAlternate)/100.0f, GetYValue(crAlternate)/100.0f, GetKValue(crAlternate)/100.0f, fTintValue, strID);
}

BOOL CPDFOutput::PDFSheet_AddSysGenStream3(int nSheetIndex, CString strPDFStream, CString strColorName, float fC, float fM, float fY, float fK, float fTintValue, CString strID)
{
	IPL_TPDFCODEID objid;
	objid.size			= sizeof( IPL_TPDFCODEID);
	objid.pdfcode		= strPDFStream.GetBuffer(strPDFStream.GetLength() + 1);
	objid.pdfcodelen	= strlen( objid.pdfcode); 
	objid.allcolorspace	= (m_bSysGenMarksColorSpaceALL) ? TRUE : FALSE;
	objid.callasID[0]	= '\0';
	_tcscpy_s( (TCHAR*)objid.userID, MAX_PATH, strID);

	IPL_TPDFCODECOLOR_2 pdfcodecolor;
	pdfcodecolor.size	   = sizeof(IPL_TPDFCODECOLOR_2);
	pdfcodecolor.colorcomp = 4;
	pdfcodecolor.colorv[0] = fC;
	pdfcodecolor.colorv[1] = fM;
	pdfcodecolor.colorv[2] = fY;
	pdfcodecolor.colorv[3] = fK;
	pdfcodecolor.tintvalue = fTintValue;
	CSysToUTF8Converter s2utf8;
	_tcscpy_s((TCHAR*)pdfcodecolor.colorname, MAX_PATH, (TCHAR*)s2utf8.Convert(strColorName));

	IPL_TEXTGRSTATE extgrstate;
    extgrstate.size			= sizeof(IPL_TEXTGRSTATE);
    extgrstate.mask			= IPL_eStrokeOPrint | IPL_eFillOPrint | IPL_eOprintMode;
    extgrstate.strokeoprint = TRUE;
    extgrstate.filloprint	= TRUE;
    extgrstate.oprintmode	= 1;

	IPL_TPDFCODEFONT pdfcodefont;
	pdfcodefont.size	 = sizeof(IPL_TPDFCODEFONT);
	pdfcodefont.name	 = IPL_eHelvetica;
	pdfcodefont.encoding = IPL_eWinAnsi;
	pdfcodefont.fontsize = 1;
	pdfcodefont.cspacing = 0.0f;
	pdfcodefont.wspacing = 0.0f;

	IPL_int32_t nResult = IPL_PDFSheet_InsertPDFCode(m_hPDFFile, nSheetIndex, &objid, &pdfcodecolor, &extgrstate, &pdfcodefont);
	if (nResult)
	{
		CString			strMsg;
		PTB_uint32_t	nBufSize = 1024;
		IPL_utf8_char_t szMsgUTF8[1024];
		IPL_GetErrorStringFromErrorCode( nResult, szMsgUTF8, nBufSize, CDlgPDFLibStatus::MapLanguageID()); 
		PTB_ConvertUTF8ToSys(szMsgUTF8, strMsg.GetBuffer(1024), &nBufSize); strMsg.ReleaseBuffer();

		KIMOpenLogMessage(strMsg, MB_ICONERROR);

		m_nError = CImpManApp::PDFOutputError;
		return FALSE;
	}
	return TRUE;
}


void CPDFOutput::CalcPDFObjectOrigin(float& fOriginX, float& fOriginY, float fAngle, float fSheetExposureCenterX, float fSheetExposureCenterY, 
																					 float fObjectBBoxCenterX,	  float fObjectBBoxCenterY)
{
	fOriginX = fSheetExposureCenterX - fObjectBBoxCenterX;
	fOriginY = fSheetExposureCenterY - fObjectBBoxCenterY;
	if (AngleEquals( fAngle, 90.0f ))
	{
		fOriginX = fSheetExposureCenterX + fObjectBBoxCenterY;
		fOriginY = fSheetExposureCenterY - fObjectBBoxCenterX;
	}
	else
		if (AngleEquals( fAngle, 180.0f ))
		{
			fOriginX = fSheetExposureCenterX + fObjectBBoxCenterX;
			fOriginY = fSheetExposureCenterY + fObjectBBoxCenterY;
		}
		else
			if (AngleEquals( fAngle, 270.0f ))
			{
				fOriginX = fSheetExposureCenterX - fObjectBBoxCenterY;
				fOriginY = fSheetExposureCenterY + fObjectBBoxCenterX;
			}
}


IPL_TDMATRIX CPDFOutput::CalcPDFObjectMatrix(float fOriginX, float fOriginY, float fAngle, float fScaleX, float fScaleY)
{
	IPL_TDMATRIX matrix = { fScaleX, 0, 0, fScaleY, fOriginX, fOriginY };

	if (AngleEquals( fAngle, 0.0f ))
		return matrix;
	else
		if (AngleEquals( fAngle, 90.0f ))
		{
			matrix.a = 0.0f; matrix.b = 1.0f * fScaleX; matrix.c = -1.0f * fScaleY; matrix.d = 0.0f; matrix.h = fOriginX; matrix.v = fOriginY;
		}
		else
			if (AngleEquals( fAngle, 180.0f ))
			{
				matrix.a = -1.0f * fScaleX;  matrix.b = 0.0f; matrix.c = 0.0f; matrix.d = -1.0f * fScaleY; matrix.h = fOriginX; matrix.v = fOriginY;
			}
			else
				if (AngleEquals( fAngle, 270.0f ))
				{
					matrix.a = 0.0f;  matrix.b = -1.0f * fScaleX; matrix.c = 1.0f * fScaleY; matrix.d = 0.0f; matrix.h = fOriginX; matrix.v = fOriginY;
				}
				else
				{
					double fAngleRad = fAngle * (3.141592654f / 180.0f);
					matrix.a = (float)cos(fAngleRad) * fScaleX;  matrix.b = (float)sin(fAngleRad); matrix.c = (float)(-sin(fAngleRad)); matrix.d = (float)cos(fAngleRad) * fScaleY; matrix.h = fOriginX; matrix.v = fOriginY;
				}

	return matrix;
}


CClipPath CPDFOutput::CalcPDFObjectClipPath(float fOriginX, float fOriginY, float fAngle, float fBottlingAngle, 
										    float fSheetClipBoxLeft,	 float fSheetClipBoxBottom, 
										    float fSheetClipBoxRight, float fSheetClipBoxTop)
{
	IPL_TDRECT objectClipBox;

	objectClipBox.fr.left	= fSheetClipBoxLeft   - fOriginX;
	objectClipBox.fr.bottom = fSheetClipBoxBottom - fOriginY;
	objectClipBox.fr.right	= fSheetClipBoxRight  - fOriginX;
	objectClipBox.fr.top	= fSheetClipBoxTop	  - fOriginY;

	if (AngleEquals( fAngle, 90.0f ))
	{
		objectClipBox.fr.left	= fSheetClipBoxBottom - fOriginY;
		objectClipBox.fr.bottom = fOriginX - fSheetClipBoxRight;
		objectClipBox.fr.right	= objectClipBox.fr.left   + (fSheetClipBoxTop	- fSheetClipBoxBottom);
		objectClipBox.fr.top	= objectClipBox.fr.bottom + (fSheetClipBoxRight - fSheetClipBoxLeft);
	}
	else
		if (AngleEquals( fAngle, 180.0f ))
		{
			objectClipBox.fr.left	= fOriginX - fSheetClipBoxRight;
			objectClipBox.fr.bottom = fOriginY - fSheetClipBoxTop;
			objectClipBox.fr.right	= objectClipBox.fr.left   + (fSheetClipBoxRight - fSheetClipBoxLeft);
			objectClipBox.fr.top	= objectClipBox.fr.bottom + (fSheetClipBoxTop	- fSheetClipBoxBottom);
		}
		else
			if (AngleEquals( fAngle, 270.0f ))
			{
				objectClipBox.fr.left	= fOriginY - fSheetClipBoxTop;
				objectClipBox.fr.bottom = fSheetClipBoxLeft - fOriginX;
				objectClipBox.fr.right	= objectClipBox.fr.left   + (fSheetClipBoxTop	- fSheetClipBoxBottom);
				objectClipBox.fr.top	= objectClipBox.fr.bottom + (fSheetClipBoxRight - fSheetClipBoxLeft);
			}

	CClipPath objectClipPath;
	objectClipPath.SetSize(4);

	if ( ! AngleEquals( fBottlingAngle, 0.0f ))
	{
		float fA, fB, fC, fD;
		float fAngleRad = -fBottlingAngle * (3.141592654f / 180.0f);
		fA = cos(fAngleRad); fB = sin(fAngleRad); fC = -fB; fD =  fA;
		objectClipPath[0].fX = fA * objectClipBox.fr.left  + fC * objectClipBox.fr.bottom;
		objectClipPath[0].fY = fB * objectClipBox.fr.left  + fD * objectClipBox.fr.bottom;
		objectClipPath[1].fX = fA * objectClipBox.fr.left  + fC * objectClipBox.fr.top;
		objectClipPath[1].fY = fB * objectClipBox.fr.left  + fD * objectClipBox.fr.top;
		objectClipPath[2].fX = fA * objectClipBox.fr.right + fC * objectClipBox.fr.top;
		objectClipPath[2].fY = fB * objectClipBox.fr.right + fD * objectClipBox.fr.top;
		objectClipPath[3].fX = fA * objectClipBox.fr.right + fC * objectClipBox.fr.bottom;
		objectClipPath[3].fY = fB * objectClipBox.fr.right + fD * objectClipBox.fr.bottom;
	}
	else
	{
		objectClipPath[0].fX = objectClipBox.fr.left;
		objectClipPath[0].fY = objectClipBox.fr.bottom;
		objectClipPath[1].fX = objectClipBox.fr.left;
		objectClipPath[1].fY = objectClipBox.fr.top;
		objectClipPath[2].fX = objectClipBox.fr.right;
		objectClipPath[2].fY = objectClipBox.fr.top;
		objectClipPath[3].fX = objectClipBox.fr.right;
		objectClipPath[3].fY = objectClipBox.fr.bottom;
	}

	return objectClipPath;
}

//////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////
// CSeparationInfo

CSeparationInfoLogicalPage::CSeparationInfoLogicalPage()
{
}

CSeparationInfoLogicalPage::CSeparationInfoLogicalPage(const CSeparationInfoLogicalPage& rLogicalPage)
{
	for (int i = 0; i < rLogicalPage.m_strColorants.GetSize(); i++)
	{
		CString strColorant = rLogicalPage.m_strColorants[i];
		m_strColorants.Add(strColorant);
	}
	for (int i = 0; i < rLogicalPage.m_nSheetIndices.GetSize(); i++)
		m_nSheetIndices.Add(rLogicalPage.m_nSheetIndices[i]);
}

const CSeparationInfoLogicalPage& CSeparationInfoLogicalPage::operator=(const CSeparationInfoLogicalPage& rLogicalPage)
{
	for (int i = 0; i < rLogicalPage.m_strColorants.GetSize(); i++)
	{
		CString strColorant = rLogicalPage.m_strColorants[i];
		m_strColorants.Add(strColorant);
	}
	for (int i = 0; i < rLogicalPage.m_nSheetIndices.GetSize(); i++)
		m_nSheetIndices.Add(rLogicalPage.m_nSheetIndices[i]);

	return *this;
}

BOOL CSeparationInfoLogicalPage::IsEmpty()
{
	for (int i = 0; i < m_strColorants.GetSize(); i++)
	{
		if ( ! m_strColorants[i].IsEmpty())
			return FALSE;
	}

	return TRUE;
}

void CSeparationInfo::Reset()
{
	for (int i = 0; i < m_logicalPages.GetSize(); i++)
	{
		CSeparationInfoLogicalPage& rLogicalPage = m_logicalPages[i];
		for (int j = 0; j < rLogicalPage.m_strColorants.GetSize(); j++)
			rLogicalPage.m_strColorants[j].Empty();
		rLogicalPage.m_strColorants.RemoveAll();
		rLogicalPage.m_nSheetIndices.RemoveAll();
	}
	m_logicalPages.RemoveAll();

	NewLogicalPage();
}

void CSeparationInfo::NewLogicalPage()
{
	CSeparationInfoLogicalPage logicalPage;
	m_logicalPages.Add(logicalPage);
}

void CSeparationInfo::Add(CString strColorant, int nSheetIndex)
{
	if ( ! m_logicalPages.GetSize())
		return;

	CSeparationInfoLogicalPage& rLogicalPage = m_logicalPages[m_logicalPages.GetSize() - 1];

	if (strColorant == _T("Composite"))
		strColorant = "";
	else
	{
		CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorant);
		if (pColorDef)
		{
			if (pColorDef->m_nColorType == CColorDefinition::ProcessC)					// if process color, always output standard names
				strColorant = _T("Cyan");												// regardless of strColorant
			else
				if (pColorDef->m_nColorType == CColorDefinition::ProcessM)
					strColorant = _T("Magenta");
				else
					if (pColorDef->m_nColorType == CColorDefinition::ProcessY)
						strColorant = _T("Yellow");
					else
						if (pColorDef->m_nColorType == CColorDefinition::ProcessK)
							strColorant = _T("Black");
		}
	}
	rLogicalPage.m_strColorants.Add(strColorant);
	rLogicalPage.m_nSheetIndices.Add(nSheetIndex);
}

BOOL CSeparationInfo::IsEmpty()
{
	BOOL bIsEmpty = TRUE;
	for (int i = 0; i < m_logicalPages.GetSize(); i++)
	{
		if (m_logicalPages[i].m_strColorants.GetSize())
		{
			bIsEmpty = FALSE;
			break;
		}
	}
	return bIsEmpty;
}

void CSeparationInfo::InitCallasArrays(CSeparationInfoLogicalPage& rLogicalPage)
{
	for (int i = 0; (i < rLogicalPage.m_nSheetIndices.GetSize()) && (i < MaxNumColorants); i++)
	{
		m_callasIsNameArray[i]	 = FALSE;
		m_callasColorantArray[i] = m_sys2utf8.Convert(rLogicalPage.m_strColorants[i]);
		m_callasPageArray[i]	 = rLogicalPage.m_nSheetIndices[i];
	}
}

void CSeparationInfo::Insert(IPL_int32_t hPDFFile)
{
	for (int i = 0; i < m_logicalPages.GetSize(); i++)
	{
		CSeparationInfoLogicalPage& rLogicalPage = m_logicalPages[i];
		if ( ! rLogicalPage.IsEmpty())
		{
			InitCallasArrays(rLogicalPage);
			IPL_TSEPARATIONINFO sepinfo = {sizeof( IPL_TSEPARATIONINFO), rLogicalPage.m_nSheetIndices.GetSize(), m_callasIsNameArray, m_callasColorantArray, m_callasPageArray, i + 1};
			IPL_PDFSheet_InsertSepInfo(hPDFFile, &sepinfo);
		}
	}
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// CSpotColorMapper

void CSpotColorMapper::Initialize(CPageTemplate* pObjectTemplate, CLayoutObject* pObject, CPlate* pPlate, CPageSourceHeader* pPageSourceHeader)
{
	for (int i = 0; i < m_strOldSpotColors.GetSize(); i++)
		m_strOldSpotColors[i].Empty();
	for (int i = 0; i < m_strNewSpotColors.GetSize(); i++)
		m_strNewSpotColors[i].Empty();
	m_strOldSpotColors.RemoveAll();
	m_strNewSpotColors.RemoveAll();
	for (int i = 0; i < MaxNumColors; i++)
	{
		m_callasOldColor[i] = NULL;
		m_callasNewColor[i] = NULL;
	}

	if ( ! pObjectTemplate || ! pObject || ! pPlate || ! pPageSourceHeader)
		return;

	if (pObject->m_nType == CLayoutObject::ControlMark)
	{
		if (pObjectTemplate->m_bMap2SpotColors && pPageSourceHeader->m_spotColors.GetSize())
		{
			CString strNone = _T("");
			for (int i = 0; (i < pPageSourceHeader->m_spotColors.GetSize()) && (i < MaxNumColors); i++)
			{
				BOOL bSwitchOffSpot	   = FALSE;
				int  nMap2SpotColorNum = pObjectTemplate->m_spotColorMappingTable.GetEntry(i);
				if (nMap2SpotColorNum > 0)
				{
					if (nMap2SpotColorNum - 1 < pPlate->m_spotColors.GetSize())
					{
						m_strOldSpotColors.Add(pPageSourceHeader->m_spotColors[i]);
						m_strNewSpotColors.Add(pPlate->m_spotColors[nMap2SpotColorNum - 1]);
						m_callasOldColor[i] = m_sys2utf8.Convert(m_strOldSpotColors[i]);
						m_callasNewColor[i] = m_sys2utf8.Convert(m_strNewSpotColors[i]);
					}
					else
						bSwitchOffSpot = TRUE;
				}
				else
					bSwitchOffSpot = TRUE;
				if (bSwitchOffSpot)
				{
					m_strOldSpotColors.Add(pPageSourceHeader->m_spotColors[i]);
					m_strNewSpotColors.Add(strNone);
					m_callasOldColor[i] = m_sys2utf8.Convert(m_strOldSpotColors[i]);
					m_callasNewColor[i] = NULL;
				}
			}
		}
	}
	else
	{
		if (pPageSourceHeader->m_spotColors.GetSize())
		{
			int nColorIndex = 0;
			for (int i = 0; (i < pPageSourceHeader->m_spotColors.GetSize()) && (i < MaxNumColors); i++)
			{
				CString strNewColorName = theApp.m_colorDefTable.FindColorReferenceName(pPageSourceHeader->m_spotColors[i]);
				if ( ! strNewColorName.IsEmpty())	// if empty, spot color not defined -> don't map
					if (strNewColorName != pPageSourceHeader->m_spotColors[i])
					{
						m_strOldSpotColors.Add(pPageSourceHeader->m_spotColors[i]);
						m_strNewSpotColors.Add(strNewColorName);
						m_callasOldColor[nColorIndex] = m_sys2utf8.Convert(m_strOldSpotColors[nColorIndex]);
						m_callasNewColor[nColorIndex] = m_sys2utf8.Convert(m_strNewSpotColors[nColorIndex]);
						nColorIndex++;
					}
			}
		} 
	}
}

