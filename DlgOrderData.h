
#pragma once

#include "afxwin.h"

#include "DlgNumeration.h"
#include "OwnerDrawnButtonList.h"



// CDlgOrderData-Dialogfeld

class CDlgOrderData : public CDialog
{
	DECLARE_DYNAMIC(CDlgOrderData)

public:
	CDlgOrderData(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgOrderData();

// Dialogfelddaten
public:
	//{{AFX_DATA(COrderDataView)
	enum { IDD = IDD_ORDERDATA };
	CListCtrl	m_ODPrintColorTable;
	CString	m_strODCreator;
	CString	m_strODCustomer;
	CString	m_strODCustomerNumber;
	CString	m_strODJob;
	CString	m_strODJobNumber;
	CString	m_strODPhone;
	CString	m_strODScheduledToPrint;
	CString	m_strODTitle;
	CString	m_strODNotes;
	COleDateTime m_scheduledToPrint;
	COleDateTime m_CreationDate;
	COleDateTime m_LastModifiedDate;
	CString m_strODNumPagesComment;
	CString m_strODQuantityComment;
	//}}AFX_DATA

// Attribute
public:
	CFont						m_smallFont;
	BOOL						m_bIsDialog;
	CButton*					m_pButtonOK;
	CButton*					m_pButtonCancel;
	BOOL						m_bInitialized;
	BOOL						m_bModified;


// Operationen
public:
	void	InitData();
	void	SaveData();
	void	AddOKCancel();


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(COrderDataView)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgOrderData)
	afx_msg void OnKillfocusOdTitle();
	afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnDtnDatetimechangeOdDatetimepicker(NMHDR *pNMHDR, LRESULT *pResult);
	//}}AFX_MSG
};
