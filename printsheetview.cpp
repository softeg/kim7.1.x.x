// PrintSheetView.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PrintSheetTableView.h"
#include "PageListTableView.h"
#include "PrintSheetMarksView.h"
#include "PrintSheetFrame.h"
#include "NewPrintSheetFrame.h"
#include "DlgTrimming.h"
#include "DlgSheet.h"
#include "PageListView.h"
#include "DlgMultiSelect.h"
#include "MyRectTracker.h"
#include "DlgMarkSet.h"
#include "Afxpriv.h"
#include "GraphicComponent.h"
#include "MainFrm.h"
#include "PrintSheetMaskSettingsView.h"
#include "PrintSheetPlanningView.h"
#include "PrintSheetObjectPreview.h"
#include "PrintComponentsView.h"
#include "PrintSheetComponentsView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



#define PDFTARGETPROPS_OPEN	( (GetDlgOutputMedia() ?  (BOOL)(GetDlgOutputMedia()->m_hWnd) : FALSE ) )
#define SHOW_MEASURING ( PDFTARGETPROPS_OPEN || ShowMeasure() )


/////////////////////////////////////////////////////////////////////////////
// CPrintSheetView

IMPLEMENT_DYNCREATE(CPrintSheetView, CScrollView)

CPrintSheetView::CPrintSheetView() : m_docSize(1,1), m_viewportCellWidth(INITIAL_CELLWIDTH_SCREEN)
{
	m_nToolBarHeight		= 34;

	m_nViewportCellRows		= 1;
	m_nViewportCellCols		= 1;
	m_bDrawTrimChannel		= FALSE;
    m_bPaintStatusChanged   = FALSE;

	m_nFrameColorIndex		= 0;
	m_bEnableColorMenues	= FALSE;
	m_bCheckMarineFrame		= TRUE;
	m_bCheckCyanFrame		= FALSE;
	m_bCheckRedFrame		= FALSE;
	m_bCheckYellowFrame		= FALSE;

	m_bLockUpdateStatusBars		= FALSE;
	m_bReflineUpdateEraseFront	= TRUE;
	m_bReflineUpdateEraseBack	= TRUE;

	m_fScreenPrinterRatioX	= 1.0f;
	m_fScreenPrinterRatioY	= 1.0f;
	m_viewportSize			= CSize(m_viewportCellWidth, 1);
	m_docBorderTopLeft	    = CSize(VIEWPORT_BORDER(SHOW_MEASURING, HORIZONTAL, TRUE), VIEWPORT_BORDER(SHOW_MEASURING, VERTICAL, TRUE));
	m_docBorderBottomRight  = CSize(VIEWPORT_BORDER(SHOW_MEASURING, HORIZONTAL, TRUE), VIEWPORT_BORDER(SHOW_MEASURING, VERTICAL, TRUE));

    m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CPrintSheetView), &WorldToLP); // Ole Drag/Drop
	m_DisplayList.SetClassName(_T("CPrintSheetView"));

	m_pBkBitmap = NULL;
	m_TrimChannel.pObjectList1 = NULL;
	m_TrimChannel.pObjectList2 = NULL;

	m_nActiveYRefLine = 0;
}

CPrintSheetView::~CPrintSheetView()
{
	if (m_pBkBitmap)
	{
		m_pBkBitmap->DeleteObject();
		delete m_pBkBitmap;
		m_pBkBitmap = NULL;
	}
}

BEGIN_MESSAGE_MAP(CPrintSheetView, CScrollView)
    //{{AFX_MSG_MAP(CPrintSheetView)
    ON_WM_LBUTTONDOWN()
    ON_WM_CONTEXTMENU()
    ON_WM_SIZE()
    ON_WM_SETCURSOR()
    ON_WM_CREATE()
    ON_WM_KEYDOWN()
    ON_WM_LBUTTONDBLCLK()
    ON_WM_ERASEBKGND()
	ON_COMMAND(ID_MARINE_FRAME, OnMarineFrame)
	ON_UPDATE_COMMAND_UI(ID_MARINE_FRAME, OnUpdateMarineFrame)
	ON_COMMAND(ID_CYAN_FRAME, OnCyanFrame)
	ON_UPDATE_COMMAND_UI(ID_CYAN_FRAME, OnUpdateCyanFrame)
	ON_COMMAND(ID_RED_FRAME, OnRedFrame)
	ON_UPDATE_COMMAND_UI(ID_RED_FRAME, OnUpdateRedFrame)
	ON_COMMAND(ID_YELLOW_FRAME, OnYellowFrame)
	ON_UPDATE_COMMAND_UI(ID_YELLOW_FRAME, OnUpdateYellowFrame)
	ON_WM_MOUSEWHEEL()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_KEYUP()
	ON_WM_RBUTTONUP()
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	//}}AFX_MSG_MAP
    // Standard printing commands
    ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
    ON_COMMAND(ID_FILE_PRINT_DIRECT, OnFilePrint)
    ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)

	ON_NOTIFY(UDN_DELTAPOS, 1, OnDeltaposReflineXSpin)
	ON_NOTIFY(UDN_DELTAPOS, 2, OnDeltaposReflineYSpin)
	ON_EN_KILLFOCUS(		3, OnKillfocusReflineDistanceEdit)
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	ON_UPDATE_COMMAND_UI(ID_SELECT_PRINTSHEETVIEW_STRIPPINGDATA, OnUpdatePrintSheetViewStrippingData)
	ON_UPDATE_COMMAND_UI(ID_ADDNEW_PRINTSHEET, OnUpdateAddNewPrintSheet)
	ON_UPDATE_COMMAND_UI(ID_REMOVE_PRINTSHEET, OnUpdateRemovePrintSheet)
	ON_UPDATE_COMMAND_UI(ID_MODIFY_SHINGLING, OnUpdateModifyShingling)
	ON_UPDATE_COMMAND_UI(ID_OUTPUT_MASK_SETTINGS, OnUpdateOutputMaskSettings)
	ON_UPDATE_COMMAND_UI(ID_OUTPUT_COLOR_SETTINGS, OnUpdateOutputColorSettings)
	ON_UPDATE_COMMAND_UI(ID_UNDO, OnUpdateUndo)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ALLSIDES, OnUpdateViewAllsides)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FRONTSIDE, OnUpdateViewFrontside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BACKSIDE, OnUpdateViewBackside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BOTHSIDES, OnUpdateViewBothsides)
	ON_UPDATE_COMMAND_UI(IDC_FOLDSHEETCHECK, OnUpdateFoldsheetCheck)
	ON_UPDATE_COMMAND_UI(IDC_MEASURECHECK, OnUpdateMeasureCheck)
	ON_UPDATE_COMMAND_UI(IDC_PLATECHECK, OnUpdatePlateCheck)
	ON_UPDATE_COMMAND_UI(IDC_BITMAPCHECK, OnUpdateBitmapCheck)
	ON_UPDATE_COMMAND_UI(IDC_EXPOSURECHECK, OnUpdateExposureCheck)
	ON_UPDATE_COMMAND_UI(ID_SHOW_CUTBLOCKS, OnUpdateShowCutBlocks)
	ON_UPDATE_COMMAND_UI(ID_SHOW_BOXSHAPES, OnUpdateShowBoxshapes)
	ON_UPDATE_COMMAND_UI(ID_ZOOM, OnUpdateZoom)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_DECREASE, OnUpdateZoomDecrease)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_FULLVIEW, OnUpdateZoomFullview)
	ON_UPDATE_COMMAND_UI(ID_SCROLLHAND, OnUpdateScrollHand)
END_MESSAGE_MAP()




CPrintSheet* CPrintSheetView::GetActPrintSheet() 
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;

	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->GetActPrintSheet();

	if (CImpManDoc::GetDoc())
		return CImpManDoc::GetDoc()->GetActPrintSheet();

	return NULL;
}

CLayout* CPrintSheetView::GetActLayout() 
{
	CPrintSheet* pActPrintSheet = GetActPrintSheet();

	if (pActPrintSheet)
		return (pActPrintSheet->GetLayout());
	else
		return (NULL);
}

CPressDevice* CPrintSheetView::GetActPressDevice(int nSide/*= 0*/)
{
	CLayout* pActLayout = GetActLayout();

	if (pActLayout)
	{
		switch (nSide)
		{
			case FRONTSIDE : return (pActLayout->m_FrontSide.GetPressDevice());
			case BACKSIDE  : return (pActLayout->m_BackSide.GetPressDevice());
			default		   : switch (GetActSight())
							 {
								case BOTHSIDES :
								case FRONTSIDE : return (pActLayout->m_FrontSide.GetPressDevice());
								case BACKSIDE  : return (pActLayout->m_BackSide.GetPressDevice());
								default		   : return (NULL);
							 }
		}
	}
	else
		return (NULL);
}

int CPrintSheetView::GetActSight()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	CFrameWnd* pFrame = GetParentFrame();
	if (pFrame)
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetActSight();

	return pDoc->GetActSight();
}

CPlate* CPrintSheetView::GetActPlate()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		return pDoc->GetActPlate();
	else
		return NULL;
}



/////////////////////////////////////////////////////////////////////////////
// CPrintSheetView drawing

void CPrintSheetView::OnDraw(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(rcClient);

	CDC	memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap bitmap;	bitmap.CreateCompatibleBitmap(pDC, rcClient.Width(), rcClient.Height());
	memDC.SelectObject(&bitmap);

	CRect rcBackGround = rcClient; rcBackGround.top += m_nToolBarHeight;
	memDC.FillSolidRect(rcBackGround, RGB(127,162,207));

	memDC.SaveDC();
	OnPrepareDC(&memDC);

	Draw(&memDC);
	//Draw(pDC);
	
	memDC.RestoreDC(-1);

	pDC->SaveDC();
	pDC->SetMapMode(MM_TEXT);
	pDC->SetWindowOrg(0, 0);
	pDC->SetWindowExt(rcClient.Width(), rcClient.Height());
	pDC->SetViewportOrg(0, 0);
	pDC->SetViewportExt(rcClient.Width(), rcClient.Height());

	pDC->BitBlt(0, m_nToolBarHeight, rcClient.Width(), rcClient.Height(), &memDC, 0, m_nToolBarHeight, SRCCOPY);
	pDC->RestoreDC(-1);
	bitmap.DeleteObject();
	memDC.DeleteDC();
}

void CPrintSheetView::Draw(CDC* pDC)
{
	InitStatusBars(pDC);

	m_ColumnBars.m_rectList.RemoveAll();

	if (m_pBkBitmap)	
	{
		RestoreBackground(pDC, m_rectBk);	// first restore background (if any) for mouse move feedback in marks view mode
		m_rectBk = CRect(0,0,0,0);
	}

    m_DisplayList.BeginRegisterItems(pDC);

	BOOL bReturn = FALSE;
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		bReturn = TRUE;
	else
		if (pDoc->m_PrintSheetList.IsEmpty())
			bReturn = TRUE;
	if (bReturn)
	{
		m_DisplayList.EndRegisterItems(pDC);
		EndWaitCursor();
		return;
	}
	
	// Make the ToolTip-List empty
	if (m_tooltip.m_hWnd)
	{
		int nCount = m_tooltip.GetToolCount();
		for (int n = 0; n < nCount; n++)
			m_tooltip.DelTool(this, 1);	// ID is > 0    // (this, n + 1)
	}

    CLayout* pActLayout = GetActLayout();
          
    CPrintSheet* pActPrintSheet = NULL;
    if (pActLayout) 
    {
        pActPrintSheet = GetActPrintSheet();
    }

	if (MarksViewActive())
		m_markObjectInfoList.RemoveAll();

	CSize  frontExtent;
	CPoint backOrg;
	CPoint ptVpOrg( (( ! pDC->IsPrinting()) ? -GetScrollPos(SB_HORZ) : m_rcPrintMargins.left) + STATUSBAR_THICKNESS(HORIZONTAL), 
					(( ! pDC->IsPrinting()) ? -GetScrollPos(SB_VERT) : m_rcPrintMargins.top)  + STATUSBAR_THICKNESS(VERTICAL));
    switch (GetActSight())
    {
        case FRONTSIDE : pDC->SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 	 pDC->SetViewportOrg(ptVpOrg);
						 DrawPrintSheet(pDC, FRONTSIDE, pActLayout, pActPrintSheet, GetParentFrame());
        				 break;
        case BACKSIDE  : pDC->SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 	 pDC->SetViewportOrg(ptVpOrg);
						 DrawPrintSheet(pDC, BACKSIDE,  pActLayout, pActPrintSheet, GetParentFrame());
        				 break;
        case BOTHSIDES : pDC->SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 	 pDC->SetViewportOrg(ptVpOrg);
        				 DrawPrintSheet(pDC, FRONTSIDE, pActLayout, pActPrintSheet, GetParentFrame());

						 if (pActLayout)
						 {
							 frontExtent = pActLayout->GetExtents(pActPrintSheet, FRONTSIDE, SHOW_MEASURING, NULL);
							 pActLayout->GetExtents(pActPrintSheet, BACKSIDE, SHOW_MEASURING, &backOrg);
							 pDC->SetWindowOrg(backOrg.x - FRONT_BACK_GAP(SHOW_MEASURING) - frontExtent.cx - m_docBorderTopLeft.cx, m_docSize.cy + m_docOrg.y);	
        					 DrawPrintSheet(pDC, BACKSIDE, pActLayout, pActPrintSheet, GetParentFrame(), FALSE);
						 }
        				 break;
        case ALLSIDES :  DrawPrintSheetAllSidesToScreen(pDC);
        				 break;
        default		:	//TODO: Errormessage !
        				 break;
    }

    m_DisplayList.EndRegisterItems(pDC);

	CRect clientRect;
	GetClientRect(clientRect);
	CPoint ptCenter = clientRect.CenterPoint();
	pDC->DPtoLP(&ptCenter, 1);
	m_fScreenCenterX = (float)(ptCenter.x / WorldToLP(1.0f));
	m_fScreenCenterY = (float)(ptCenter.y / WorldToLP(1.0f));
}

///i "C:\Programme\KIM3.0\Auto-Input\Demo\Demo.job" "C:\Programme\KIM3.0\Auto-Input\Demo\Demo_1.pdf"


void CPrintSheetView::DrawPrintSheetAllSidesToScreen(CDC* pDC)
{
    CImpManDoc*		  pDoc = CImpManDoc::GetDoc();
	CRect			  clientRect;
	GetClientRect(clientRect);

    //We begin in the upper left corner of the view
	pDC->SetViewportOrg(-GetScrollPos(SB_HORZ) + STATUSBAR_THICKNESS(HORIZONTAL), -GetScrollPos(SB_VERT) + STATUSBAR_THICKNESS(VERTICAL));
	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CSize  frontExtent;
		CPoint frontOrg, backOrg;
		for (int j = 0; (j < m_nViewportCellRows) && pos; j++)                        
		{
        	CPrintSheet* pActPrintSheet = &(pDoc->m_PrintSheetList.GetNext(pos));
        	CLayout*	 pActLayout		= pActPrintSheet->GetLayout();
        	if (pActLayout == NULL)
        		continue;

			frontExtent = pActLayout->GetExtents(pActPrintSheet, FRONTSIDE, SHOW_MEASURING, &frontOrg);
			pDC->SetWindowOrg(frontOrg.x - m_docBorderTopLeft.cx, m_docSize.cy + m_docOrg.y);
	       	DrawPrintSheet(pDC, FRONTSIDE, pActLayout, pActPrintSheet, GetParentFrame());

			////// toolbar
			CRect rcToolbar(WorldToLP(pActLayout->m_FrontSide.m_fPaperLeft),   WorldToLP(pActLayout->m_FrontSide.m_fPaperTop), 
							WorldToLP(pActLayout->m_FrontSide.m_fPaperRight ), WorldToLP(pActLayout->m_FrontSide.m_fPaperBottom));
			CSize size(20,10);
			pDC->DPtoLP(&size);
			rcToolbar.top += size.cy; rcToolbar.bottom = rcToolbar.top - size.cy * 2;
			m_DisplayList.RegisterItem(pDC, rcToolbar, rcToolbar, (void*)pActPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetView, Toolbar), NULL, CDisplayItem::ForceRegister); 
			//////////////

			pActLayout->GetExtents(pActPrintSheet, BACKSIDE, SHOW_MEASURING, &backOrg);
			pDC->SetWindowOrg(backOrg.x - FRONT_BACK_GAP(SHOW_MEASURING) - frontExtent.cx - m_docBorderTopLeft.cx, m_docSize.cy + m_docOrg.y);	
	    	DrawPrintSheet(pDC, BACKSIDE, pActLayout, pActPrintSheet, GetParentFrame(), FALSE);

			////// toolbar
			rcToolbar = CRect(WorldToLP(pActLayout->m_BackSide.m_fPaperLeft),   WorldToLP(pActLayout->m_BackSide.m_fPaperTop), 
							  WorldToLP(pActLayout->m_BackSide.m_fPaperRight ), WorldToLP(pActLayout->m_BackSide.m_fPaperBottom));
			rcToolbar.top += size.cy; rcToolbar.bottom = rcToolbar.top - size.cy * 2;
			m_DisplayList.RegisterItem(pDC, rcToolbar, rcToolbar, (void*)pActPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetView, Toolbar), NULL, CDisplayItem::ForceRegister); 
			//////////////

			// next row (cell)
        	pDC->OffsetViewportOrg(0, m_viewportSize.cy);
		}

       	pDC->OffsetViewportOrg(m_viewportSize.cx, -(m_nViewportCellRows * m_viewportSize.cy));
		
		ReInitLeftStatusBar(pDC);
		m_bUpperStatusBarUpdated = FALSE;

       	pDC->OffsetViewportOrg(STATUSBAR_THICKNESS(HORIZONTAL), 0);
	}
}


BOOL CPrintSheetView::OnMouseMoveToolbar(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;

				if ( ! m_gotoDetailsButton.m_hWnd)
				{
					CRect rcButton = pDI->m_BoundingRect;
					rcButton.right  = rcButton.left + 20;
					rcButton.bottom = rcButton.top  + 20;
					m_gotoDetailsButton.Create(_T(""), WS_CHILD | BS_OWNERDRAW, rcButton, this, ID_GOTO_DETAILS);
					m_gotoDetailsButton.SetBitmap(IDB_ARROW_RIGHT_HOT, IDB_ARROW_RIGHT_COLD, -1, XCENTER);
					m_gotoDetailsButton.SetTransparent(TRUE);
					m_gotoDetailsButton.m_lParam = (LPARAM)pDI->m_pData;
				}
				m_gotoDetailsButton.ShowWindow(SW_SHOW);
			}
			break;

	case 2:	// mouse leave
			if (m_gotoDetailsButton.m_hWnd)
				m_gotoDetailsButton.DestroyWindow();
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

void CPrintSheetView::OnGotoDetails()
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_gotoDetailsButton.m_lParam;
	if ( ! pPrintSheet)
		return;

	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETLIST);
	pFrame->SelectPrintSheet(pPrintSheet);

	if (m_gotoDetailsButton.m_hWnd)
		m_gotoDetailsButton.DestroyWindow();
}

void CPrintSheetView::DrawPrintSheet(CDC* pDC, UINT nActLayoutSide, CLayout* pActLayout, CPrintSheet* pActPrintSheet, CFrameWnd* pParentFrame, BOOL bDrawTitle)
{
	if ( ! pActLayout || ! pActPrintSheet)
		return;

	if (nActLayoutSide == BACKSIDE)
		if (pActLayout->m_nProductType)
			return;

	if (IsOpenDlgFoldSchemeSelector())
		pActLayout->SetObjectsStatusAll(FALSE);

	m_DisplayList.InitializeItemGrouping((IsOpenDlgAlignObjects()) ? CDisplayItem::GroupCenterHandles : CDisplayItem::GroupTracker);

    DrawPlate				(pDC, nActLayoutSide, pActPrintSheet, pActLayout, bDrawTitle);
    DrawSheet				(pDC, nActLayoutSide, pActPrintSheet, bDrawTitle);
    DrawExposures			(pDC, nActLayoutSide, pActPrintSheet);
    DrawObjects				(pDC, nActLayoutSide, pActPrintSheet, CLayoutObject::Page);						
    DrawObjects				(pDC, nActLayoutSide, pActPrintSheet, CLayoutObject::Page, TRUE);	pDC->SelectClipRgn(NULL);	ClipStatusBars(pDC);	// release cliprects for page id protection
	for (int i = pActLayout->m_markList.GetSize() - 1; i >= 0; i--)
		DrawObjects(pDC, nActLayoutSide, pActPrintSheet, CLayoutObject::ControlMark, FALSE, pActLayout->m_markList[i]->m_strMarkName);	
	DrawObjects(pDC, nActLayoutSide, pActPrintSheet, CLayoutObject::ControlMark, FALSE, _T(""));		// draw marks that are not in mark list - needed for compatibility to KIM5 marks
    DrawPlatePreviews		(pDC, nActLayoutSide, pActPrintSheet, CLayoutObject::Page);			
	for (int i = pActLayout->m_markList.GetSize() - 1; i >= 0; i--)
	    DrawPlatePreviews(pDC, nActLayoutSide, pActPrintSheet, CLayoutObject::ControlMark, FALSE, pActLayout->m_markList[i]->m_strMarkName);
    DrawPlatePreviews(pDC, nActLayoutSide, pActPrintSheet, CLayoutObject::ControlMark, FALSE, _T(""));	// draw marks that are not in mark list - needed for compatibility to KIM5 marks
	DrawColorants			(pDC, nActLayoutSide, pActPrintSheet);
	DrawGraphics			(pDC, nActLayoutSide, pActPrintSheet);
	DrawShinglingOffsets	(pDC, nActLayoutSide, pActPrintSheet);
	DrawFanoutOffsets		(pDC, nActLayoutSide, pActPrintSheet);
    DrawSheetFrame			(pDC, nActLayoutSide, pActPrintSheet, bDrawTitle);
    DrawRefEdge				(pDC, nActLayoutSide, pActPrintSheet, pActLayout);
    DrawFoldSheet			(pDC, nActLayoutSide, pActPrintSheet);
	DrawGripper				(pDC, nActLayoutSide, pActPrintSheet);
	DrawCutBlocks			(pDC, nActLayoutSide, pActPrintSheet);
	if ( (ViewMode() != ID_VIEW_ALL_SHEETS) && ShowMeasure() )
	{
		BeginWaitCursor();	// in case of complex layouts, measure painting could take a while
	    DrawMeasure			(pDC, nActLayoutSide, pActPrintSheet);
		EndWaitCursor();	
	}
	DrawTrimChannelRect		(pDC, nActLayoutSide);
    DrawMedia				(pDC, nActLayoutSide, pActPrintSheet);
	if ( (ViewMode() != ID_VIEW_ALL_SHEETS) && ShowMeasure() )
		DrawSheetMeasuring	(pDC, nActLayoutSide, pActLayout, ShowPlate());
	if ( (ViewMode() != ID_VIEW_ALL_SHEETS) && ShowMeasure() && ShowPlate())
		DrawPlateMeasuring	(pDC, nActLayoutSide, pActLayout);
	DrawMoveObjectReflines	(pDC, nActLayoutSide, pActPrintSheet);
	if (IsOpenDlgLayoutObjectGeometry())
		DrawBoundingBoxFrame(pDC, nActLayoutSide, pActPrintSheet);

	if (IsOpenDlgComponentImposer())
	{
		//if (GetDlgComponentImposer()->m_nNup > 0)
		{
			float fWidth = 0.0f, fHeight = 0.0f; 
			GetDlgComponentImposer()->GetMinSize(fWidth, fHeight);
			pActLayout->m_FrontSide.FindFreeAreas(pActPrintSheet, fWidth, fHeight);
			for (int i = 0; i < pActLayout->m_FrontSide.m_freeAreas.GetSize(); i++)
			{
				CRect rcOut(pActLayout->m_FrontSide.m_freeAreas[i].left/10,  pActLayout->m_FrontSide.m_freeAreas[i].top/10,
							pActLayout->m_FrontSide.m_freeAreas[i].right/10, pActLayout->m_FrontSide.m_freeAreas[i].bottom/10);

				//CPen pen(PS_SOLID, 1, LIGHTGRAY);
				//CPen* pOldPen = pDC->SelectObject(&pen);
				//pDC->SelectStockObject(HOLLOW_BRUSH);
				//pDC->Rectangle(rcOut);
				//pDC->SelectObject(pOldPen);
				//pen.DeleteObject();
				CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, rcOut, rcOut, (void*)i, DISPLAY_ITEM_REGISTRY(CPrintSheetView, FreeArea), (void*)pActPrintSheet, CDisplayItem::ForceRegister | CDisplayItem::NotClickable, NULL); // register item into display list
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
				}
			}

			GetDlgComponentImposer()->DrawPreviewFrame(pDC);
		}
	}
	else
	{
		if ( ! IsOpenDlgAlignObjects() && ! IsOpenDlgLayoutObjectProperties())
		{
//			m_DisplayList.Invalidate();	// TODO: this is fatal here inside Regsiter phase. Question: why did I do this
			if (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)) == 1)
			{
				CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
				if (pDI)
				{
					CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
					if (pLayoutObject->m_nType != CLayoutObject::ControlMark)
					{
						pDI->m_bMouseOver = -1;
						DrawResizeTracker(pDC, pDI->m_BoundingRect, RED, pDI); 
					}
				}
			}
		}
	}

	if (IsOpenMarkSetDlg())
	{
		GetDlgMarkSet()->m_markList.DrawSheetPreviews(pDC, pActPrintSheet, nActLayoutSide);
		GetDlgMarkSet()->UpdateEnableStates();
	}

	if (IsOpenDlgNewMark())
	{
		BOOL bShowMeasuring = ( IsOpenDlgNewMarkGeometry() && ! ShowBitmap() && ! ShowExposures() && ViewMode() != ID_VIEW_ALL_SHEETS) ? TRUE : FALSE;
		int  nEdge = -1;
		if (bShowMeasuring)
		{
			CDlgLayoutObjectGeometry* pDlg = GetDlgLayoutObjectGeometry();
			nEdge = (pDlg) ? pDlg->GetMarkEdge() : 0;
		}
		if (GetDlgNewMark()->m_pMark)
		{
			BOOL bShowContent = (IsOpenDlgLayoutObjectGeometry()) ? FALSE : TRUE; 
			GetDlgNewMark()->m_pMark->DrawSheetPreview(pDC, pActPrintSheet, nActLayoutSide, NULL, bShowContent, nEdge, TRUE);
		}
	}
	if (MarksViewActive())
		DrawMarkInfoFields(pDC, pActPrintSheet);

	if (AlignSectionActive())
		DrawSectionLinesForWebPress(pDC, nActLayoutSide, pActLayout);

    if (m_bPaintStatusChanged || ! ShowExposures())	// if no exposures show status could be changed
    {
    	CPrintSheetTreeView* pView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
    	if (pView)
    		pView->SynchronizePlateTreeItems(m_DisplayList);

    	m_bPaintStatusChanged = FALSE;
    }
}

BOOL CPrintSheetView::OnMouseMoveFreeArea(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 

				CPen pen(PS_SOLID, 1, LIGHTRED);
				CPen* pOldPen = pDC->SelectObject(&pen);
				CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
				pDC->Rectangle(pDI->m_BoundingRect);
				pDC->SelectObject(pOldPen);
				pDC->SelectObject(pOldBrush);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetView::OnSelectLayoutSideName(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CLayoutSide* pLayoutSide = (CLayoutSide*)pDI->m_pData;
	CLayout*	 pLayout	 = (pLayoutSide) ? pLayoutSide->GetLayout() : NULL;
	if ( ! pLayout)
		return FALSE;
	if (pLayout->m_nProductType != WORK_AND_BACK)
		return FALSE;

	pDI->m_nState = CDisplayItem::Normal;

	CRect rcText = pDI->m_BoundingRect; rcText.right += pDI->m_BoundingRect.Width(); rcText.InflateRect(0, 2);

	m_InplaceEditSideName.Activate(rcText, pLayoutSide->m_strSideName, CInplaceEdit::GrowToRight, 0, *pDI);

	return TRUE;
}

BOOL CPrintSheetView::OnDeselectLayoutSideName(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been deselected
{
	CLayoutSide* pLayoutSide = (CLayoutSide*)pDI->m_pData;
	if ( ! pLayoutSide)
		return FALSE;

	pLayoutSide->m_strSideName = m_InplaceEditSideName.GetString();

	m_InplaceEditSideName.Deactivate();

   	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);

	return TRUE;
}

BOOL CPrintSheetView::OnSelectWebYRefLine1(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

BOOL CPrintSheetView::OnDeselectWebYRefLine1(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	return FALSE;
}

BOOL CPrintSheetView::OnSelectWebYRefLine2(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

BOOL CPrintSheetView::OnDeselectWebYRefLine2(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	return FALSE;
}

void CPrintSheetView::OnKillFocusInplaceEditSideName() 		// Message from InplaceEdit
{
   	OnDeselectLayoutSideName(&m_InplaceEditSideName.m_di, CPoint(0, 0), 0);
}

void CPrintSheetView::OnCharInplaceEditSideName(UINT nChar) // Message from InplaceEdit
{
    switch (nChar)
    {
    case VK_TAB:	
    case VK_RETURN:	// Deactivate item and InplaceEdit
        {
			SetFocus();
        	//if (!m_InplaceEditSideName.m_di.IsNull())
        	//{
        	//	m_DisplayList.DeactivateItem(m_InplaceEditSideName.m_di);
        	//	m_DisplayList.InvalidateItem(&m_InplaceEditSideName.m_di);
        	//}
        }
        break;
    default: 
        break;
    }
}

STATUSBAR_DC_STATE CPrintSheetView::PrepareStatusBarDC(CDC* pDC)
{
	CRect clientRect;
	if (pDC->IsPrinting())
        clientRect = CRect(CPoint(0, 0), CSize(pDC->GetDeviceCaps(HORZRES), pDC->GetDeviceCaps(VERTRES)));
	else
		GetClientRect(clientRect);

	STATUSBAR_DC_STATE dcState;
	if (pDC->IsPrinting())
		if (pDC->IsKindOf(RUNTIME_CLASS(CPreviewDC)))	// for preview dc only SaveDC/RestoreDC is not enough,
		{												// we have to restore the window and viewport separately in order to get correct
			dcState.ptWinOrg   = pDC->GetWindowOrg();	// results regarding mirroring of printer and preview dc (look file ...mfc\src\dcprev.cpp)
			dcState.sizeWinExt = pDC->GetWindowExt();
			dcState.ptVpOrg	   = pDC->GetViewportOrg();
			dcState.sizeVpExt  = pDC->GetViewportExt();
		}

	pDC->SaveDC();
	pDC->SetMapMode(MM_TEXT);
	pDC->SetWindowOrg(0, 0);
	pDC->SetWindowExt(clientRect.Width(), clientRect.Height());
	pDC->SetViewportOrg(0, 0);
	pDC->SetViewportExt(clientRect.Width(), clientRect.Height());
	pDC->SelectClipRgn(NULL);

	return dcState;
}

void CPrintSheetView::RestoreStatusBarDC(CDC* pDC, STATUSBAR_DC_STATE& rOldDCState)
{
	pDC->RestoreDC(-1);
	if (pDC->IsPrinting())
		if (pDC->IsKindOf(RUNTIME_CLASS(CPreviewDC)))
		{
			pDC->SetWindowOrg  (rOldDCState.ptWinOrg);
			pDC->SetWindowExt  (rOldDCState.sizeWinExt);
			pDC->SetViewportOrg(rOldDCState.ptVpOrg);
			pDC->SetViewportExt(rOldDCState.sizeVpExt);
		}
}

void CPrintSheetView::ClipStatusBars(CDC* pDC)
{
    if (ViewMode() == ID_VIEW_ALL_SHEETS)
		pDC->SelectClipRgn(NULL);
	else
	{
		CRect clipRect;
		clipRect.left = m_LeftStatusBar.right;
		clipRect.top  = m_UpperStatusBar.bottom + 1;
		if (pDC->IsPrinting())
		{
			clipRect.right  = pDC->GetDeviceCaps(HORZRES);
			clipRect.bottom = pDC->GetDeviceCaps(VERTRES);
		}
		else
		{
			clipRect.right  = m_sizeTotal.cx;
			clipRect.bottom = m_sizeTotal.cy;
		}
		pDC->DPtoLP(clipRect);
		pDC->BeginPath();
			pDC->Rectangle(clipRect);	
		pDC->EndPath();
		pDC->SelectClipPath(RGN_COPY);

		pDC->LPtoDP(clipRect);
		m_DisplayList.m_clipRect = clipRect;
	}
}

COLORREF g_crStatusBar = RGB(240,240,255);

void CPrintSheetView::InitStatusBars(CDC* pDC)
{
	if (m_bLockUpdateStatusBars)
	{
		ClipStatusBars(pDC);
		return;
	}

	CRect clientRect;
	if (pDC->IsPrinting())
        clientRect = CRect(CPoint(m_rcPrintMargins.left, m_rcPrintMargins.top), 
						   CSize(pDC->GetDeviceCaps(HORZRES) - (m_rcPrintMargins.left + m_rcPrintMargins.right), 
								 pDC->GetDeviceCaps(VERTRES) - (m_rcPrintMargins.top  + m_rcPrintMargins.bottom)));
	else
		GetClientRect(clientRect);
	m_LeftStatusBar	 = clientRect;
	m_UpperStatusBar = clientRect;
	//m_UpperStatusBar.bottom = m_UpperStatusBar.top + 33;
	m_UpperStatusBar.bottom = m_UpperStatusBar.top + STATUSBAR_THICKNESS(VERTICAL) - 1;

	CRect rcBar = m_UpperStatusBar; rcBar.top = 34; rcBar.bottom -= RULER_THICKNESS();

    if (ViewMode() == ID_VIEW_ALL_SHEETS)  
	{
		CPoint pt(0, 0);
		pDC->LPtoDP(&pt, 1);
		m_LeftStatusBar.top	  = rcBar.top;
		m_LeftStatusBar.left  = pt.x;
		m_LeftStatusBar.right = pt.x + STATUSBAR_THICKNESS(HORIZONTAL);
	}
	else
	{
		m_LeftStatusBar.top	  = m_UpperStatusBar.bottom;
		m_LeftStatusBar.right = m_LeftStatusBar.left + STATUSBAR_THICKNESS(HORIZONTAL);
	}

	STATUSBAR_DC_STATE oldDCState = PrepareStatusBarDC(pDC);

	HTHEME hTheme = NULL;
	if (theApp.m_nOSVersion == CImpManApp::WinXP)	// theming only under XP necessary - Vista runs with "pDC->FillSolidRect(rcBar, ::GetSysColor(CTLCOLOR_DLG))"
#ifdef UNICODE
		hTheme = theApp._OpenThemeData(GetParent(), _T("WINDOW"));
#else
		hTheme = theApp._OpenThemeData(GetParent(), CA2W("WINDOW"));
#endif
	if (hTheme)
		DrawThemeBackgroundEx(hTheme, pDC->GetSafeHdc(), WP_DIALOG, 0, &rcBar, 0);	
	else
		pDC->FillSolidRect(rcBar, ::GetSysColor(CTLCOLOR_DLG));
	theApp._CloseThemeData(hTheme);

	CGraphicComponent gp;
	CRect rcRulerBar = m_UpperStatusBar; rcRulerBar.top = rcRulerBar.bottom - RULER_THICKNESS(); rcRulerBar.bottom += 1;
	if ( ! pDC->IsPrinting())
		gp.DrawRuler(pDC, TOP, rcRulerBar);	

	if (ViewMode() == ID_VIEW_ALL_SHEETS)
		gp.DrawTitleBar(pDC, m_LeftStatusBar,  _T(""), RGB(240,240,230), RGB(238,234,215), 0);
	else
		if ( ! pDC->IsPrinting())
			gp.DrawRuler(pDC, LEFT, m_LeftStatusBar);	

	RestoreStatusBarDC(pDC, oldDCState);

	ClipStatusBars(pDC);

	m_bUpperStatusBarUpdated = FALSE;
}

void CPrintSheetView::ReInitLeftStatusBar(CDC* pDC)
{
	if (ViewMode() == ID_VIEW_SINGLE_SHEETS)
		return;
	if (m_bLockUpdateStatusBars)
		return;

	CPoint pt			  = pDC->GetViewportOrg();
	m_LeftStatusBar.left  = pt.x;
	m_LeftStatusBar.right = pt.x + STATUSBAR_THICKNESS(HORIZONTAL);

	STATUSBAR_DC_STATE oldDCState = PrepareStatusBarDC(pDC);

	if ( ! pDC->IsPrinting())
	{
		CGraphicComponent gp;
		if (ViewMode() == ID_VIEW_ALL_SHEETS)
			gp.DrawTitleBar(pDC, m_LeftStatusBar,  _T(""), RGB(240,240,230), RGB(238,234,215), 0);
		else
			gp.DrawRuler(pDC, LEFT, m_LeftStatusBar);	
	}

	RestoreStatusBarDC(pDC, oldDCState);

	m_ColumnBars.m_rectList.Add(m_LeftStatusBar);

	CRect clipRect = m_LeftStatusBar;
	pDC->DPtoLP(clipRect);
	pDC->ExcludeClipRect(clipRect);
}

void CPrintSheetView::UpdateLeftStatusBar(CDC* pDC, CRect sheetRect, CPrintSheet* pActPrintSheet, CLayoutSide* pLayoutSide)
{
	if (pDC->IsPrinting())
		return;
	if (m_bLockUpdateStatusBars)
		return;

	int nSide = GetActSight();
	if ((nSide == BOTHSIDES) || (nSide == ALLSIDES))
		if (pLayoutSide->m_nLayoutSide == BACKSIDE)
			return;

	CString string;
	HWND hWnd = (GetDlgOutputMedia()) ? GetDlgOutputMedia()->m_hWnd : NULL;
	if (hWnd)
	{
		CPrintSheetTableView* pView	= (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
		if (pView)
		{
			if (pView->m_bStartOutputChecked)
			{
				if (GetDlgOutputMedia())
					if (GetDlgOutputMedia()->PrintSheetIsSelected(pActPrintSheet))
					{
						CPDFTargetProperties* pTargetProps = CImpManDoc::GetDoc()->m_PrintSheetList.m_pdfTargets.Find(pActPrintSheet->m_strPDFTarget);
						if (pTargetProps)
							string = pTargetProps->m_strTargetName;
					}
			}
			else
				string = GetDlgOutputMedia()->GetCurPDFTargetName(pActPrintSheet);

			if (string.IsEmpty())
				string = "   ";	// because textrect needs to be unequal zero, otherwise print sheet number will not be outputted
		}
	}
	else
		if (ShowPlate())
		{
			if (pLayoutSide->GetPressDevice())
				string = pLayoutSide->GetPressDevice()->m_strName;
		}
		else
		{
			if (pLayoutSide->GetLayout())
				string = pLayoutSide->GetLayout()->m_strLayoutName;
		}

	float fRulerFrom = (float)sheetRect.bottom  / (float)WorldToLP(1.0f);
	float fRulerTo   = (float)sheetRect.top		/ (float)WorldToLP(1.0f);
	fRulerTo  -= fRulerFrom;	// transform to 0
	fRulerFrom = 0.0f;

	pDC->LPtoDP(sheetRect);

	CRect rcRuler = sheetRect;

	sheetRect-= CSize(sheetRect.left - m_LeftStatusBar.left, 0);	// move sheet rect to left side

	CRect clientRect;
	if (pDC->IsPrinting())
        clientRect = CRect(CPoint(m_rcPrintMargins.left, m_rcPrintMargins.top), 
						   CSize(pDC->GetDeviceCaps(HORZRES) - (m_rcPrintMargins.left + m_rcPrintMargins.right), 
								 pDC->GetDeviceCaps(VERTRES) - (m_rcPrintMargins.top  + m_rcPrintMargins.bottom)));
	else
		GetClientRect(clientRect);

	if (!sheetRect.IntersectRect(sheetRect, clientRect))
		return;

	STATUSBAR_DC_STATE oldDCState = PrepareStatusBarDC(pDC);

	if (ViewMode() == ID_VIEW_ALL_SHEETS)
	{
		CFont font0, font90;
		font0.CreateFont ((int)(13 * m_fScreenPrinterRatioX), 0,   0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
		font90.CreateFont((int)(13 * m_fScreenPrinterRatioX), 0, 900, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
		CFont*	 pOldFont		= pDC->SelectObject(&font90);
		COLORREF crOldTextColor = pDC->SetTextColor(GUICOLOR_VALUE);
		int		 nOldBkMode		= pDC->SetBkMode(TRANSPARENT);

		CSize textExtent = pDC->GetTextExtent(string);
		CRect textRect;
		textRect.left	= m_LeftStatusBar.CenterPoint().x - textExtent.cy/2 - 1;
		textRect.top	= max(sheetRect.top, m_UpperStatusBar.bottom);
		textRect.right	= textRect.left + textExtent.cy;
		textRect.bottom = textRect.top  + textExtent.cx + textExtent.cy + (long)(30 * m_fScreenPrinterRatioY); // 30 pixels for printsheet number

		textRect.IntersectRect(textRect, sheetRect);

		BOOL bNameDisplayed = FALSE;
		if (textRect.Height() - (40 * m_fScreenPrinterRatioX) > textExtent.cx)	// if layout name doesn't fit completely - don't draw
		{
			pDC->DrawText(string, textRect, DT_SINGLELINE|DT_BOTTOM|DT_LEFT);
			bNameDisplayed = TRUE;
		}

		string.Format(_T("%s"), pActPrintSheet->GetNumber());
		pDC->SetTextColor(GUICOLOR_CAPTION);
		if (pDC->GetTextExtent(string).cx > m_LeftStatusBar.Width())
			pDC->TextOut(textRect.left, ((bNameDisplayed) ? textRect.bottom : textRect.top) + pDC->GetTextExtent(string).cx, string);
		else
		{
			pDC->SelectObject(&font0);
			pDC->DrawText(string, textRect, DT_SINGLELINE|DT_TOP|DT_CENTER);
		}

		pDC->SelectObject(pOldFont);
		pDC->SetTextColor(crOldTextColor);
		pDC->SetBkMode(nOldBkMode);

		font0.DeleteObject();
		font90.DeleteObject();
	}

	if ( (ViewMode() == ID_VIEW_SINGLE_SHEETS) || (ViewMode() == ID_ZOOM) )
	{
		CRect rcRulerBar = m_LeftStatusBar; rcRulerBar.top = rcRulerBar.bottom - RULER_THICKNESS();
		CRect rcClip;
		GetClientRect(rcClip); rcClip.top = STATUSBAR_THICKNESS(VERTICAL); rcClip.right = STATUSBAR_THICKNESS(HORIZONTAL);
		CGraphicComponent gp;
		if ( ! pDC->IsPrinting())
			gp.DrawRuler(pDC, LEFT, rcRulerBar, rcRuler, fRulerFrom, fRulerTo, rcClip);	
	}

	RestoreStatusBarDC(pDC, oldDCState);
}

void CPrintSheetView::UpdateUpperStatusBar(CDC* pDC, CRect sheetRect, CPrintSheet* pActPrintSheet, CLayoutSide* pLayoutSide, BOOL bDrawTitle)
{
	if (m_bLockUpdateStatusBars)
		return;

	if (m_bUpperStatusBarUpdated)
		return;

	int nSight = GetActSight();
	if (nSight == ALLSIDES)
		if (pLayoutSide->GetLayout()->m_nProductType)	// work and turn, work and tumble or flat work -> don't update in ALLSIDES view
			if (m_nViewportCellRows > 1)				// if only one row -> we can display the side name
				return;

	float fRulerFrom = (float)sheetRect.left  / (float)WorldToLP(1.0f);
	float fRulerTo   = (float)sheetRect.right / (float)WorldToLP(1.0f);
	fRulerTo  -= fRulerFrom;	// transform to 0
	fRulerFrom = 0.0f;

	pDC->LPtoDP(sheetRect);

	CRect rcRuler = sheetRect;

	sheetRect-= CSize(0, sheetRect.top);	// move sheet rect to upper side

	CRect clientRect;
	if (pDC->IsPrinting())
        clientRect = CRect(CPoint(m_rcPrintMargins.left, m_rcPrintMargins.top), 
						   CSize(pDC->GetDeviceCaps(HORZRES) - (m_rcPrintMargins.left + m_rcPrintMargins.right), 
								 pDC->GetDeviceCaps(VERTRES) - (m_rcPrintMargins.top  + m_rcPrintMargins.bottom)));
	else
		GetClientRect(clientRect);

	if (!sheetRect.IntersectRect(sheetRect, clientRect))
		return;

	STATUSBAR_DC_STATE oldDCState = PrepareStatusBarDC(pDC);

	int nOffset =  (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionOutput) ? 0 : -80;
	m_printViewToolbar1Buttons.MoveButtons(CPoint(		nOffset, 2));
	m_printViewToolbar2Buttons.MoveButtons(CPoint(180 + nOffset, 2));
	m_printViewToolbar3Buttons.MoveButtons(CPoint(400 + nOffset, 2));
	m_zoomToolBar.MoveButtons(CPoint(590 + nOffset, 2));

	CFont font, smallFont;
	font.CreateFont		((int)(13 * m_fScreenPrinterRatioY), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	smallFont.CreateFont((int)(13 * m_fScreenPrinterRatioY), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	CFont*	 pOldFont		= pDC->SelectObject(&font);
	COLORREF crOldTextColor = pDC->SetTextColor(GUICOLOR_CAPTION);
	int		 nOldBkMode		= pDC->SetBkMode(TRANSPARENT);

	CString string;
	string.Format(_T("%s - %s"), pActPrintSheet->GetNumber(), pLayoutSide->GetLayout()->m_strLayoutName);
	CSize textExtent = pDC->GetTextExtent(string);
	CRect textRect;
	textRect.left	= max(sheetRect.left, m_LeftStatusBar.right);
	textRect.top	= m_UpperStatusBar.bottom - RULER_THICKNESS() - textExtent.cy - textExtent.cy/4 - 2;
	textRect.right	= textRect.left + textExtent.cx;
	textRect.bottom = textRect.top  + textExtent.cy; 

	textRect.IntersectRect(textRect, sheetRect);

	int nLeft = textRect.left;
	CPageListTableView::FitStringToWidth(pDC, string, textRect.Width());
	if ( (ViewMode() == ID_VIEW_SINGLE_SHEETS) || (ViewMode() == ID_ZOOM) )
	{
		int nXPos = textRect.left;
		if (bDrawTitle)
		{
			CString string1; string1.LoadString(IDS_NEW); 
			CString string2; string2.LoadString(IDS_SHEET_TEXT); string2 += _T(":");
			pDC->SetTextColor(DARKGRAY);
			string.Format(_T("%s"), ( ! pActPrintSheet->IsEmpty()) ? pActPrintSheet->GetNumber() : string1);
			pDC->SelectObject(&smallFont);
			pDC->TextOut(nXPos, textRect.top - 1, string2);
			nLeft += pDC->GetTextExtent(string2).cx + 5;
			nXPos += pDC->GetTextExtent(string2).cx + 5;
			pDC->SetTextColor(RGB(255,100,15));
			pDC->SelectObject(&font);
			pDC->TextOut(nXPos, textRect.top - 1, string);
			nLeft += pDC->GetTextExtent(string).cx + 20;
		}
		nXPos += pDC->GetTextExtent(string).cx + (int)(120 * m_fScreenPrinterRatioX);
		switch(((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection)
		{
		case CMainFrame::NavSelectionPrintSheets:	string = _T(""); break; //string.Format(_T("%s"), pLayoutSide->GetLayout()->m_strLayoutName);	break;	// layoutname is displayed in stripping view
		case CMainFrame::NavSelectionOutput:		string.Format(_T("%s"), pActPrintSheet->m_strPDFTarget);			break;	
		default:									string.Format(_T("%s"), pLayoutSide->GetLayout()->m_strLayoutName);	break;
		}
		if (bDrawTitle)
		{
			pDC->SetTextColor(LIGHTBLUE);//GUICOLOR_CAPTION);
			pDC->TextOut(nXPos, textRect.top - 1, string);
		}

		if ( ! pDC->IsPrinting())
		{
			CRect rcFrame(nXPos, textRect.top - 2, nXPos + 200, textRect.top + 30); 
			pActPrintSheet->DrawColors(pDC, rcFrame, pLayoutSide->m_nLayoutSide);
		}
	}

	switch((pLayoutSide->GetLayout())->m_nProductType)
	{
	case WORK_AND_TURN	:	string.Format(_T("%s"), theApp.settings.m_szWorkAndTurnName);	  break;
	case WORK_AND_TUMBLE:	string.Format(_T("%s"), theApp.settings.m_szWorkAndTumbleName);	  break;
	case WORK_SINGLE_SIDE:	string.Format(_T("%s"), theApp.settings.m_szSingleSidedName);	  break;
	default:				string.Format(_T("%s"), pLayoutSide->m_strSideName);			  break;
	}

	pDC->SelectObject(&font);

	textExtent = pDC->GetTextExtent(string);
	textRect.left = nLeft;
	textRect.right	= textRect.left + textExtent.cx;
	textRect.bottom = textRect.top  + textExtent.cy; 

	textRect.IntersectRect(textRect, sheetRect);

	CPageListTableView::FitStringToWidth(pDC, string, textRect.Width());
	pDC->SetTextColor(RGB(170,140,90));
	pDC->TextOut(textRect.left, textRect.top - 1, string);

	if (pLayoutSide->GetLayout()->m_nProductType == WORK_AND_BACK)
		m_DisplayList.RegisterItem(pDC, textRect, textRect, (void*)pLayoutSide, DISPLAY_ITEM_REGISTRY(CPrintSheetView, LayoutSideName));

	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldTextColor);
	pDC->SetBkMode(nOldBkMode);
	font.DeleteObject();
	smallFont.DeleteObject();

	if ( (ViewMode() == ID_VIEW_SINGLE_SHEETS) || (ViewMode() == ID_ZOOM) )
	{
		CRect rcRulerBar = m_UpperStatusBar; rcRulerBar.left = m_LeftStatusBar.right; rcRulerBar.top = rcRulerBar.bottom - RULER_THICKNESS();
		CGraphicComponent gp;
		if ( ! pDC->IsPrinting())
			gp.DrawRuler(pDC, TOP, rcRulerBar, rcRuler, fRulerFrom, fRulerTo);	
	}

	RestoreStatusBarDC(pDC, oldDCState);

	if ((nSight == BOTHSIDES) || (nSight == ALLSIDES))
	{
		if (pLayoutSide->m_nLayoutSide == BACKSIDE)
			m_bUpperStatusBarUpdated = TRUE;
	}
	else
		m_bUpperStatusBarUpdated = TRUE;
}


// This function is overridden in order to:
//		- control catchrect and drag/drop feedback
//		- protect status bars from being scrolled 
BOOL CPrintSheetView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
    CClientDC dc(this);

    // remove drag/drop feedback before scrolling
    if (bDoScroll)
        m_DisplayList.RemoveDragFeedback();


//// following taken from MFC /////
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
	{
		// This part is changed - rest is taken from MFC
		if (GetKeyState(VK_LBUTTON) >= 0)
		{
			InvalidateRect(m_LeftStatusBar);	// update text positions in status bars
			InvalidateRect(m_UpperStatusBar);
		}
		////////////////////////////////////////////////
		return FALSE;
	}

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
	    if (ViewMode() == ID_VIEW_ALL_SHEETS)  
			ScrollWindow(-(x-xOrig), -(y-yOrig), NULL, NULL);
		else
		{
			CRect clipRect, scrollRect;
			GetClientRect(clipRect); scrollRect = clipRect;
			//clipRect.SubtractRect(clipRect, m_LeftStatusBar);	
			clipRect.left = m_LeftStatusBar.right;
			//clipRect.SubtractRect(clipRect, m_UpperStatusBar);
			clipRect.top = m_UpperStatusBar.bottom + 1;
			ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, clipRect);
		}
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CPrintSheetView diagnostics

#ifdef _DEBUG
void CPrintSheetView::AssertValid() const
{
    CScrollView::AssertValid();
}

void CPrintSheetView::Dump(CDumpContext& dc) const
{
    CScrollView::Dump(dc);
}

CImpManDoc* CPrintSheetView::GetDocument() // non-debug version is inline
{
    ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CImpManDoc)));
    return (CImpManDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetView::CalcDocExtents()
{
    CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (pDoc->m_PrintSheetList.IsEmpty())
		return;

	m_docSize = CSize (1, 1);
	m_docOrg  = CPoint(0, 0);
	
	CLayout* pLayout;
    CSize	 tmpDocSize;
	CPoint	 tmpDocOrg;
	CRect	 clientRect;
	GetClientRect(clientRect); 
    switch (GetActSight())
    {
        case FRONTSIDE : 
		case BACKSIDE:		
		case BOTHSIDES:		pLayout  = GetActLayout();
        					if (pLayout)
								m_docSize = pLayout->GetExtents(GetActPrintSheet(), GetActSight(), SHOW_MEASURING, &m_docOrg);
							break;

        case ALLSIDES  :	m_docOrg	   = CPoint(INT_MAX, INT_MAX);
							int		 nMinY = INT_MAX;
							int		 nMaxY = INT_MIN;
							POSITION pos   = pDoc->m_PrintSheetList.GetHeadPosition();
		    				while (pos)                        
							{
        						CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
        						pLayout = rPrintSheet.GetLayout();
        						if ( ! pLayout)
        							continue;

								tmpDocSize	 = pLayout->GetExtents(&rPrintSheet, BOTHSIDES, SHOW_MEASURING, &tmpDocOrg);
								nMinY		 = min(nMinY, tmpDocOrg.y);
								nMaxY		 = max(nMaxY, tmpDocOrg.y + tmpDocSize.cy);
        						m_docSize.cx = max(m_docSize.cx, tmpDocSize.cx);
								m_docOrg.x	 = min(m_docOrg.x,	 tmpDocOrg.x);
								m_docOrg.y	 = min(m_docOrg.y,	 tmpDocOrg.y);
							}
       						m_docSize.cy = nMaxY - nMinY;
							break;
    }
}

void CPrintSheetView::CalcViewportExtents(BOOL bReset)
{
    CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (pDoc->m_PrintSheetList.IsEmpty())
		return;

	m_nViewportCellCols = m_nViewportCellRows = 1;

	CRect clientRect;
	GetClientRect(&clientRect);
    switch (GetActSight())
    {
        case FRONTSIDE : 
		case BACKSIDE:		
		case BOTHSIDES:		
			{
				if ( ! bReset)
					if ( ! m_zoomSteps.GetCount())
				    //if (ViewMode() != ID_ZOOM)
					    //if ( (GetTotalSize().cx <= clientRect.Width()) && (GetTotalSize().cy <= clientRect.Height()) )
							bReset = TRUE;

				m_docBorderTopLeft	   = CSize(VIEWPORT_BORDER(SHOW_MEASURING, HORIZONTAL, TRUE), VIEWPORT_BORDER(SHOW_MEASURING, VERTICAL, TRUE));
				m_docBorderBottomRight = CSize(VIEWPORT_BORDER(SHOW_MEASURING, HORIZONTAL, TRUE), VIEWPORT_BORDER(SHOW_MEASURING, VERTICAL, TRUE));

				if (bReset)
				{
					CSize maxExtents = CSize(clientRect.Width()  - STATUSBAR_THICKNESS(HORIZONTAL, TRUE) - 2*VIEWPORT_BORDER(SHOW_MEASURING, HORIZONTAL, TRUE), 
											 clientRect.Height() - STATUSBAR_THICKNESS(VERTICAL,   TRUE) - 2*VIEWPORT_BORDER(SHOW_MEASURING, VERTICAL,   TRUE));    
					m_viewportSize.cx = maxExtents.cx;
					if (m_docSize.cx != 0)
						m_viewportSize.cy = (int)((float)m_docSize.cy * ((float)m_viewportSize.cx / (float)m_docSize.cx));
					else
						m_viewportSize.cy = 100;

					if (m_viewportSize.cy > maxExtents.cy)
					{
						m_viewportSize.cy = maxExtents.cy;
						if (m_docSize.cy != 0)
							m_viewportSize.cx = (int)((float)m_docSize.cx * ((float)m_viewportSize.cy / (float)m_docSize.cy));
						else
							m_viewportSize.cx = 100;
					}
				}
				else
				{
					m_viewportSize -= m_docBorderTopLeft + m_docBorderBottomRight;
				}
			}
			break;

        case ALLSIDES  :	
			{
				int nPrintSheets	  = CImpManDoc::GetDoc()->m_PrintSheetList.GetCount();
				m_viewportSize.cx = m_viewportCellWidth - 3 * VIEWPORT_CELLBORDER(HORIZONTAL, TRUE);	// 1*border left, 2*border right
				m_viewportSize.cy = (int)((float)m_docSize.cy * ((float)m_viewportSize.cx / (float)m_docSize.cx));
				m_nViewportCellRows	  = (clientRect.Height() - STATUSBAR_THICKNESS(VERTICAL, TRUE)) / (m_viewportSize.cy + 2 * VIEWPORT_CELLBORDER(VERTICAL, TRUE));
				if (m_nViewportCellRows == 0)
					m_nViewportCellRows = 1;
				m_nViewportCellCols	  = nPrintSheets / m_nViewportCellRows + ((nPrintSheets % m_nViewportCellRows) ? 1 : 0);

				m_docBorderTopLeft	   = CSize(	   VIEWPORT_CELLBORDER(HORIZONTAL, TRUE), VIEWPORT_CELLBORDER(VERTICAL, TRUE));
				m_docBorderBottomRight = CSize(2 * VIEWPORT_CELLBORDER(HORIZONTAL, TRUE), VIEWPORT_CELLBORDER(VERTICAL, TRUE));
			}
			break;

		default: 
			return;
    }

	float fXRatio = (float)m_docSize.cx / (float)m_viewportSize.cx;
	float fYRatio = (float)m_docSize.cy / (float)m_viewportSize.cy;
	m_viewportSize += m_docBorderTopLeft + m_docBorderBottomRight;
	m_docBorderTopLeft.cx	  = (long)((float)m_docBorderTopLeft.cx		* fXRatio);
	m_docBorderTopLeft.cy	  = (long)((float)m_docBorderTopLeft.cy		* fYRatio);
	m_docBorderBottomRight.cx = (long)((float)m_docBorderBottomRight.cx * fXRatio);
	m_docBorderBottomRight.cy = (long)((float)m_docBorderBottomRight.cy * fYRatio);

	m_docSize  += m_docBorderTopLeft + m_docBorderBottomRight;
	m_docOrg.x -= m_docBorderTopLeft.cx;
	m_docOrg.y -= m_docBorderBottomRight.cy;
}


void CPrintSheetView::UpdateToolbarBackground(CDC* pDC, CRect rcRect, CWnd* /*pWnd*/)
{
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcRect, _T(""), WHITE, RGB(240,240,240), 90, -1);
}

void CPrintSheetView::OnInitialUpdate() 
{
    CScrollView::OnInitialUpdate();
    
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;

	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		SetViewMode(ID_VIEW_ALL_SHEETS);
	else 
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			SetViewMode(ID_VIEW_SINGLE_SHEETS);
		else 
			SetViewMode(ID_VIEW_ALL_SHEETS);


	CString string;
	CRect rcButton; 
	m_printViewToolbar1Buttons.Create(this, GetParentFrame(), NULL, IDB_PRINTVIEW_TOOLBAR, IDB_PRINTVIEW_TOOLBAR_COLD, CSize(32, 25), &UpdateToolbarBackground);
	rcButton = CRect(CPoint(0,2), CSize(32, 28));
	m_printViewToolbar1Buttons.AddButton(-1, IDS_OUTPUT_MASK_SETTINGS,	ID_OUTPUT_MASK_SETTINGS,	rcButton, 4);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printViewToolbar1Buttons.AddButton(-1, IDS_OUTPUT_COLOR_SETTINGS,	ID_OUTPUT_COLOR_SETTINGS,	rcButton, 5);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printViewToolbar1Buttons.AddButton(-1, IDS_SHINGLING,				ID_MODIFY_SHINGLING,		rcButton, 7);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printViewToolbar1Buttons.AddButton(-1, IDS_UNDO_TIP,				ID_UNDO,					rcButton, 6);	rcButton.OffsetRect(rcButton.Width(), 0);

	rcButton = CRect(CPoint(180,2), CSize(32, 28));
	m_printViewToolbar2Buttons.Create(this, GetParentFrame(), NULL, IDB_PRINTVIEW_TOOLBAR2, IDB_PRINTVIEW_TOOLBAR2_COLD, CSize(40, 20), &UpdateToolbarBackground);
	CString strFront, strBack;
	AfxFormatString1(strFront, IDS_FRONT, theApp.settings.m_szFrontName);
	AfxFormatString1(strBack,  IDS_BACK,  theApp.settings.m_szBackName);
	m_printViewToolbar2Buttons.AddButton(-1,		IDS_ALL,			ID_VIEW_ALLSIDES,			rcButton, 0);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printViewToolbar2Buttons.AddButton(_T(""),	strFront,			ID_VIEW_FRONTSIDE,			rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printViewToolbar2Buttons.AddButton(_T(""),	strBack,			ID_VIEW_BACKSIDE,			rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printViewToolbar2Buttons.AddButton(-1,		IDS_BOTH,			ID_VIEW_BOTHSIDES,			rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);

	rcButton = CRect(CPoint(400,2), CSize(32, 28));
	m_printViewToolbar3Buttons.Create(this, GetParentFrame(), NULL, IDB_PRINTVIEW_TOOLBAR3, IDB_PRINTVIEW_TOOLBAR3_COLD, CSize(30, 20), &UpdateToolbarBackground);
	m_printViewToolbar3Buttons.AddButton(-1, 	IDS_SHOW_MEASURING,			IDC_MEASURECHECK,			rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printViewToolbar3Buttons.AddButton(-1, 	IDS_SHOW_PLATE,				IDC_PLATECHECK,				rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printViewToolbar3Buttons.AddButton(-1, 	IDS_SHOW_PREVIEWS,			IDC_BITMAPCHECK,			rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_printViewToolbar3Buttons.AddButton(-1, 	IDS_SHOW_CUTBLOCKS,			ID_SHOW_CUTBLOCKS,			rcButton, 5);	rcButton.OffsetRect(rcButton.Width(), 0);	
	m_printViewToolbar3Buttons.AddButton(-1, 	IDS_SHOW_BOXSHAPES,			ID_SHOW_BOXSHAPES,			rcButton, 6);	rcButton.OffsetRect(rcButton.Width(), 0);

	rcButton = CRect(CPoint(600,2), CSize(32, 28));
	m_zoomToolBar.Create(this, GetParentFrame(), NULL, IDB_ZOOM_TOOLBAR, IDB_ZOOM_TOOLBAR_COLD, CSize(25, 20), &UpdateToolbarBackground);
	m_zoomToolBar.AddButton(-1, IDS_ZOOMIN_TOOLTIP,		ID_ZOOM,			rcButton, 0); rcButton.OffsetRect(rcButton.Width(), 0);
	m_zoomToolBar.AddButton(-1,	IDS_ZOOMOUT_TOOLTIP,	ID_ZOOM_DECREASE,	rcButton, 1); rcButton.OffsetRect(rcButton.Width(), 0);
	m_zoomToolBar.AddButton(-1,	IDS_ZOOMALL_TOOLTIP,	ID_ZOOM_FULLVIEW,	rcButton, 2); rcButton.OffsetRect(rcButton.Width(), 0);
	m_zoomToolBar.AddButton(-1,	IDS_ZOOMMOVE_TOOLTIP,	ID_SCROLLHAND,		rcButton, 3);


	m_tooltip.Create(this);	// Create the ToolTip control.
	m_tooltip.Activate(TRUE);
}

void CPrintSheetView::OnUpdate(CView* /*pSender*/, LPARAM lHint, CObject* pHint) 
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nShow = (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionPrintSheets) ? SW_SHOW : SW_HIDE;
	m_printViewToolbar1Buttons.ShowButton(ID_ADDNEW_PRINTSHEET,		 nShow);
	m_printViewToolbar1Buttons.ShowButton(ID_REMOVE_PRINTSHEET,		 nShow);
	nShow = (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionOutput) ? SW_SHOW : SW_HIDE;
	m_printViewToolbar1Buttons.ShowButton(ID_OUTPUT_MASK_SETTINGS,   nShow);
	m_printViewToolbar1Buttons.ShowButton(ID_OUTPUT_COLOR_SETTINGS,  nShow);

	if (pFrame->PrintSheetViewIsEmpty())
	{
		m_printViewToolbar2Buttons.Hide();
		m_printViewToolbar3Buttons.Hide();
		m_zoomToolBar.Hide();
	}
	else
	{
		m_printViewToolbar2Buttons.Show();
		m_printViewToolbar3Buttons.Show();
		if (pFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
			m_zoomToolBar.Hide();
		else
			m_zoomToolBar.Show();
	}

	if ( ! pFrame->PrintSheetViewIsEmpty())
	{
		nShow = (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionOutput) ? SW_SHOW : SW_HIDE;
		m_printViewToolbar3Buttons.ShowButton(IDC_EXPOSURECHECK, nShow);
		nShow = ( (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionPrintSheets) ||
				  (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionPrintSheetMarks) ) ? SW_SHOW : SW_HIDE;
		m_printViewToolbar3Buttons.ShowButton(ID_SHOW_CUTBLOCKS, nShow);
		m_printViewToolbar3Buttons.ShowButton(ID_SHOW_BOXSHAPES, (pDoc->m_flatProducts.HasGraphics()) ? SW_SHOW : SW_HIDE);
		//nShow = (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionOutput) ? SW_HIDE : SW_SHOW;
		//m_printViewToolbar3Buttons.ShowButton(ID_OUTPUT_COLOR_SETTINGS,  nShow);
	}

	if (lHint != HINT_UPDATE_NULL)
	{
		CalcDocExtents();
		CalcViewportExtents((lHint == 2) ? TRUE : FALSE);
		CalcScrollSizes();	
		SetScrollSizes(MM_TEXT, m_sizeTotal, m_sizePage, m_sizeLine);

		CRect rcUpdate = (CRect*)pHint;
		switch(lHint)
		{	
			case HINT_UPDATE_WINDOW : 	Invalidate();
        								break;
			case HINT_UPDATE_RECT   :	InvalidateRect(rcUpdate);
        								break;
		}
	}
}

void CPrintSheetView::OnSize(UINT nType, int cx, int cy) 
{
    CScrollView::OnSize(nType, cx, cy);
    
	//m_DisplayList.Invalidate();

	CalcDocExtents();					
	CalcViewportExtents();
	CalcScrollSizes();
	SetScrollSizes(MM_TEXT, m_sizeTotal, m_sizePage, m_sizeLine);

	Invalidate();

/*	ShowScrollBar(SB_BOTH);
	CRect clientRect;
	GetClientRect(&clientRect);
	EnableScrollBar(SB_HORZ, (m_sizeTotal.cx > clientRect.Width() ) ? ESB_ENABLE_BOTH : ESB_DISABLE_BOTH);
	EnableScrollBar(SB_VERT, (m_sizeTotal.cy > clientRect.Height()) ? ESB_ENABLE_BOTH : ESB_DISABLE_BOTH);*/
}

void CPrintSheetView::CalcScrollSizes()
{
	if ((m_docSize.cx == 0) || (m_docSize.cy == 0))
	{
		m_sizeTotal = m_sizePage = m_sizeLine = CSize(0, 0);
		return;
	}

	CRect clientRect;
	GetClientRect(&clientRect);
    switch (ViewMode())
	{
	case ID_VIEW_ALL_SHEETS:
		m_sizeTotal = CSize(m_nViewportCellCols * (STATUSBAR_THICKNESS(HORIZONTAL, TRUE) + m_viewportSize.cx), clientRect.Height());
		m_sizePage  = CSize(m_sizeTotal.cx / m_nViewportCellCols, m_sizeTotal.cy);
		m_sizeLine	= CSize(m_sizePage.cx / 4, m_sizeTotal.cy);
		break;
	case ID_VIEW_SINGLE_SHEETS:
	case ID_ZOOM:
		m_sizeTotal = m_viewportSize + CSize(STATUSBAR_THICKNESS(HORIZONTAL, TRUE), STATUSBAR_THICKNESS(VERTICAL, TRUE));
		m_sizePage  = CSize(clientRect.Width()/2,  clientRect.Height()/2);
		m_sizeLine  = CSize(clientRect.Width()/10, clientRect.Height()/10);
		break;
	}

	if ((m_sizeTotal.cx < 0) || (m_sizeTotal.cy < 0))
	{
		m_sizeTotal = m_sizePage = m_sizeLine = CSize(0, 0);
	}
}

void CPrintSheetView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
    CScrollView::OnPrepareDC(pDC, pInfo);

	if ( ! CImpManDoc::GetDoc())
		return;
    if (CImpManDoc::GetDoc()->m_PrintSheetList.IsEmpty() || (m_docSize.cx == 0) || (m_docSize.cy == 0) )
        return;

    pDC->SetMapMode(MM_ISOTROPIC); 

    pDC->SetWindowExt(m_docSize);

	if (pInfo)
	{
		GetPrinterMargins(pDC);

		CClientDC dcScreen(this);
	    m_fScreenPrinterRatioX = (float)pDC->GetDeviceCaps(LOGPIXELSX) / (float)dcScreen.GetDeviceCaps(LOGPIXELSX);
	    m_fScreenPrinterRatioY = (float)pDC->GetDeviceCaps(LOGPIXELSY) / (float)dcScreen.GetDeviceCaps(LOGPIXELSY);
		m_nPrinterNumColors    = pDC->GetDeviceCaps(NUMCOLORS);

		long lDeviceExtX = pDC->GetDeviceCaps(HORZRES) - (m_rcPrintMargins.left + m_rcPrintMargins.right);
		long lDeviceExtY = pDC->GetDeviceCaps(VERTRES) - (m_rcPrintMargins.top  + m_rcPrintMargins.bottom);
		CSize sizeDeviceVp(lDeviceExtX, (long)((float)m_viewportSize.cy * ((float)lDeviceExtX / (float)m_viewportSize.cx)));
		if (sizeDeviceVp.cy > lDeviceExtY)
			sizeDeviceVp = CSize((long)((float)m_viewportSize.cx * ((float)lDeviceExtY / (float)m_viewportSize.cy)), lDeviceExtY);

		pDC->SetViewportExt(sizeDeviceVp.cx, -sizeDeviceVp.cy);
	}
	else
	{
		m_fScreenPrinterRatioX = m_fScreenPrinterRatioY = 1.0f;
		m_nPrinterNumColors	   = 256;

		switch (ViewMode())
		{
		case ID_ZOOM:				
		case ID_VIEW_SINGLE_SHEETS:	pDC->SetViewportExt(m_viewportSize.cx, -m_viewportSize.cy);	break;   
		case ID_VIEW_ALL_SHEETS:	pDC->SetViewportExt(m_viewportSize.cx, -m_viewportSize.cy); break;
		}
	}
}

void CPrintSheetView::GetPrinterMargins(CDC* pDC)
{
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	long ret = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	TCHAR szMargins[100];
	DWORD dwSize = sizeof(szMargins);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szMargins, &dwSize) == ERROR_SUCCESS)
		_stscanf(szMargins, _T("%ld %ld %ld %ld"), &m_rcPrintMargins.left, &m_rcPrintMargins.top, &m_rcPrintMargins.right, &m_rcPrintMargins.bottom);
	else
		m_rcPrintMargins = CRect(2500, 2500, 2500, 2500);

	m_rcPrintMargins.left   = (long)(((float)(m_rcPrintMargins.left  )/2540.0f) * pDC->GetDeviceCaps(LOGPIXELSX));
	m_rcPrintMargins.top    = (long)(((float)(m_rcPrintMargins.top   )/2540.0f) * pDC->GetDeviceCaps(LOGPIXELSY));
	m_rcPrintMargins.right  = (long)(((float)(m_rcPrintMargins.right )/2540.0f) * pDC->GetDeviceCaps(LOGPIXELSX));
	m_rcPrintMargins.bottom = (long)(((float)(m_rcPrintMargins.bottom)/2540.0f) * pDC->GetDeviceCaps(LOGPIXELSY));
}

void CPrintSheetView::DoTrimmings(CPoint point) 
{
	CPoint		  ptOrig = point;
    CDisplayItem  itemFound;
    CLayoutObject *pCounterpartObject = NULL, *pCounterpartObjectNeighbour = NULL;
    int			  n_Side;
//    CDlgTrimming  TrimDlg;
    float		  fOldTrim1, fOldTrim2;

    CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	int nSight = GetActSight();

	CRect updateRect(0,0,0,0), updateOldRect(0,0,0,0);

    if (nSight != ALLSIDES)// && nSight != BOTHSIDES)
    {
		UndoSave();

        itemFound = m_DisplayList.GetItemNearby(point, &n_Side, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), TRUE);

        if ( ! itemFound.IsNull())
        {
        	if (itemFound.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
        	{	
				if (m_TrimChannel.pObjectList1)
	        		delete m_TrimChannel.pObjectList1;
				if (m_TrimChannel.pObjectList2)
		    		delete m_TrimChannel.pObjectList2;

        		m_TrimChannel.pObjectList1 = new CObList;
        		m_TrimChannel.pObjectList2 = new CObList;

        		m_TrimChannel.n_Side = n_Side;

        		CLayoutObject* pObject	= (CLayoutObject*)itemFound.m_pData;
				CalculateAndShowTrimChannel(&m_TrimChannel, pObject, n_Side, point, (CPrintSheet*)itemFound.m_pAuxData);
        		fOldTrim1 = m_TrimChannel.fTrim1;
        		fOldTrim2 = m_TrimChannel.fTrim2;

        		m_bDrawTrimChannel = TRUE;

				CRect updateTempRect = m_TrimChannel.displayRect;

				m_TrimChannel.n_SightToDraw = (pObject->GetLayoutSide())->m_nLayoutSide;
				if (m_TrimChannel.n_SightToDraw == BACKSIDE && nSight == BOTHSIDES)
				// clicked to the right of BOTHSIDES
					m_TrimChannel.displayRect.OffsetRect((m_docSize.cx / 2), 0);
				
				OnUpdate(this, HINT_UPDATE_RECT, (CObject*)&m_TrimChannel.displayRect);
				if ( ! m_TrimChannel.displayOldRect.IsRectEmpty())
					OnUpdate(this, HINT_UPDATE_RECT, (CObject*)&m_TrimChannel.displayOldRect);
				m_TrimChannel.displayOldRect = updateTempRect;

				UpdateWindow();

				CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
				if (pFrame)
					if (pFrame->m_dlgTrimming.m_hWnd)
					{
						pFrame->m_dlgTrimming.InitData();
						pFrame->m_dlgTrimming.m_point = ptOrig;
						pFrame->m_dlgTrimming.SetFocus();
					}
        	}
        }
    }
}

void CPrintSheetView::DoAlignSection(CPoint point) 
{
    CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nSight = GetActSight();
    if (nSight == ALLSIDES)
		return;

    int nSide;
	CDisplayItem itemFound = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), 2);	// first try with border ( 2 = auxauxrect)
	if (itemFound.IsNull())
		itemFound = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), TRUE);			// now try w/o border

	if (itemFound.IsNull() || ! InsideAnyFoldSheet(point))
	{
		CDisplayItem itemFound = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, WebYRefLine1), TRUE);
		if ( ! itemFound.IsNull())
		{
			m_nActiveYRefLine = 1;
			Invalidate();
			return;
		}
		itemFound = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, WebYRefLine2), TRUE);
		if ( ! itemFound.IsNull())
		{
			m_nActiveYRefLine = 2;
			Invalidate();
			return;
		}
	}

    if ( ! itemFound.IsNull())
    {
    	if (itemFound.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
    	{
			CLayoutObject* pObject = (CLayoutObject*)itemFound.m_pData;
			if (pObject->m_nType == CLayoutObject::Page)
			{
				CRect rcRect = itemFound.m_AuxRect;
				switch (nSide)
				{
				case LEFT:
				case RIGHT:		nSide = (abs(point.y - rcRect.CenterPoint().y) <= 5) ? YCENTER : nSide; break;
				case BOTTOM:
				case TOP:		nSide = (abs(point.x - rcRect.CenterPoint().x) <= 5) ? XCENTER : nSide; break;
				}

				UndoSave();
				if ( (nSide == LEFT) || (nSide == RIGHT) || (nSide == XCENTER) )
					AlignSectionComplete(pObject, nSide);
				else
				{
					CLayoutSide*   pLayoutSide	= pObject->GetLayoutSide();
					CPressDevice*  pPressDevice	= pLayoutSide->GetPressDevice();
					if (pPressDevice)
					{
						switch (m_nActiveYRefLine)
						{
						case 1:	if ( ! pPressDevice->Has2YRefLines())
									AlignSectionComplete(pObject, nSide);		
								else
									AlignSectionYRefLine(pObject, nSide, 1);	break;
						case 2:	AlignSectionYRefLine(pObject, nSide, 2);		break;
						}
					}
				}
				pDoc->SetModifiedFlag();
				Invalidate();
			}
		}
	}
}

void CPrintSheetView::AlignSectionComplete(CLayoutObject* pObject, int nSide)
{
	CLayoutSide*   pLayoutSideCounterPart = NULL;
	CLayout*	   pLayout		= pObject->GetLayout();
	CLayoutSide*   pLayoutSide	= pObject->GetLayoutSide();
	CPressDevice*  pPressDevice	= pLayoutSide->GetPressDevice();
	float fYW = pPressDevice->GetYCenterOfSection();
	float fXW;
	if (pLayout->KindOfProduction() == WORK_AND_TURN && 
		pLayoutSide->m_nLayoutSide == BACKSIDE)
		fXW = pPressDevice->m_fPlateWidth - pPressDevice->m_fXRefLine;
	else
		fXW = pPressDevice->m_fXRefLine;

	float fRef, fXDiff, fYDiff;
	switch (nSide)
	{
	case LEFT:	 	if (pObject->m_LeftNeighbours.GetSize() > 0)
						fRef = pObject->GetLeft() - pObject->m_fLeftBorder;
				 	else
						fRef = pObject->GetLeft();
				 	break;
	case RIGHT:	 	if (pObject->m_RightNeighbours.GetSize() > 0)
						fRef = pObject->GetRight() + pObject->m_fRightBorder;
				 	else
						fRef = pObject->GetRight();
				 	break;
	case BOTTOM: 	if (pObject->m_LowerNeighbours.GetSize() > 0)
						fRef = pObject->GetBottom() - pObject->m_fLowerBorder;
				 	else
						fRef = pObject->GetBottom();
				 	break;
	case TOP:	 	if (pObject->m_UpperNeighbours.GetSize() > 0)
						fRef = pObject->GetTop() + pObject->m_fUpperBorder;
				 	else
						fRef = pObject->GetTop();
				 	break;
	case XCENTER:	fRef = pObject->GetLeft()	+ (pObject->GetRight() - pObject->GetLeft())   / 2.0f;
				 	break;
	case YCENTER:	fRef = pObject->GetBottom() + (pObject->GetTop()   - pObject->GetBottom()) / 2.0f;
				 	break;
	default:	 	fRef = 0.0f;
				 	break;
	}

	if (nSide == LEFT || nSide == RIGHT || nSide == XCENTER)
	{
		fXDiff = fXW - fRef;
		fYDiff = 0.0f;
	}
	else
	{
		fXDiff = 0.0f;
		fYDiff = fYW - fRef;
	}

	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);

		rObject.SetLeft(rObject.GetLeft()	  + fXDiff, FALSE);
		rObject.SetRight(rObject.GetRight()   + fXDiff, FALSE);
		rObject.SetBottom(rObject.GetBottom() + fYDiff, FALSE);
		rObject.SetTop(rObject.GetTop()		  + fYDiff, FALSE);
	}

	pLayoutSide->m_fPaperLeft	+= fXDiff;
	pLayoutSide->m_fPaperRight	+= fXDiff;
	pLayoutSide->m_fPaperBottom += fYDiff;
	pLayoutSide->m_fPaperTop	+= fYDiff;

	if (theApp.settings.m_bFrontRearParallel)
	{
		switch (pLayoutSide->m_nLayoutSide)
		{
		case FRONTSIDE: if (pLayoutSide->m_nPressDeviceIndex == pLayout->m_BackSide.m_nPressDeviceIndex)
							pLayoutSideCounterPart = &pLayout->m_BackSide;
						break;
		case BACKSIDE : if (pLayoutSide->m_nPressDeviceIndex == pLayout->m_FrontSide.m_nPressDeviceIndex)
							pLayoutSideCounterPart = &pLayout->m_FrontSide;
						break;
		}
		if (pLayoutSideCounterPart)
		{
			if (pLayout->KindOfProduction() == WORK_AND_TURN)
				fXDiff *= -1.0f; 
			else
				fYDiff *= -1.0f;

			pos = pLayoutSideCounterPart->m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pLayoutSideCounterPart->m_ObjectList.GetNext(pos);

				rObject.SetLeft(rObject.GetLeft()	  + fXDiff, FALSE);
				rObject.SetRight(rObject.GetRight()   + fXDiff, FALSE);
				rObject.SetBottom(rObject.GetBottom() + fYDiff, FALSE);
				rObject.SetTop(rObject.GetTop()		  + fYDiff, FALSE);
			}

			pLayoutSideCounterPart->m_fPaperLeft   += fXDiff;
			pLayoutSideCounterPart->m_fPaperRight  += fXDiff;
			pLayoutSideCounterPart->m_fPaperBottom += fYDiff;
			pLayoutSideCounterPart->m_fPaperTop	   += fYDiff;
		}
	}
	pLayout->m_FrontSide.Invalidate();
	pLayout->m_BackSide.Invalidate();
}

void CPrintSheetView::AlignSectionYRefLine(CLayoutObject* pObject, int nSide, int nWhatRefLine)
{
	CLayoutSide*   pLayoutSideCounterPart = NULL;
	CLayout*	   pLayout		= pObject->GetLayout();
	CLayoutSide*   pLayoutSide	= pObject->GetLayoutSide();
	CPressDevice*  pPressDevice	= pLayoutSide->GetPressDevice();
	float fYW = (nWhatRefLine == 1) ? pPressDevice->m_fYRefLine1 : pPressDevice->m_fYRefLine2;

	float fRef, fYDiff;
	switch (nSide)
	{
	case BOTTOM:	if (pObject->m_LowerNeighbours.GetSize() > 0)
						fRef = pObject->GetBottom() - ((pObject->m_LowerNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex) ? pObject->m_fLowerBorder : 0.0f);
					else
						fRef = pObject->GetBottom();
					break;
	case TOP:		if (pObject->m_UpperNeighbours.GetSize() > 0)
						fRef = pObject->GetTop()	+ ((pObject->m_UpperNeighbours[0]->m_nComponentRefIndex == pObject->m_nComponentRefIndex) ? pObject->m_fUpperBorder : 0.0f);
					else
						fRef = pObject->GetTop();
					break;
	case YCENTER:	fRef = pObject->GetBottom() + (pObject->GetTop() - pObject->GetBottom()) / 2.0f;
				 	break;
	default:		fRef = 0.0f;
					break;
	}

	fYDiff = fYW - fRef;

	CLayoutObjectMatrix	objectMatrix(pLayoutSide->m_ObjectList, CLayoutObjectMatrix::TakePages);	
	BYTE nRowIndex, nColIndex;
	int  nNumRows = objectMatrix.GetSize();

	int nActiveRefLine = (fYW == min(pPressDevice->m_fYRefLine1, pPressDevice->m_fYRefLine2)) ? BOTTOM : TOP;

	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
		objectMatrix.GetMatrixIndices(&rObject, nRowIndex, nColIndex);
		if (nActiveRefLine == BOTTOM)
		{
			if (nRowIndex >= nNumRows / 2)
				continue;
		}
		else
			if (nRowIndex < nNumRows / 2)
				continue;

		rObject.SetBottom(rObject.GetBottom() + fYDiff, FALSE);
		rObject.SetTop(rObject.GetTop()		  + fYDiff, FALSE);
	}

	if (theApp.settings.m_bFrontRearParallel)
	{
		switch (pLayoutSide->m_nLayoutSide)
		{
		case FRONTSIDE: if (pLayoutSide->m_nPressDeviceIndex == pLayout->m_BackSide.m_nPressDeviceIndex)
							pLayoutSideCounterPart = &pLayout->m_BackSide;
						break;
		case BACKSIDE : if (pLayoutSide->m_nPressDeviceIndex == pLayout->m_FrontSide.m_nPressDeviceIndex)
							pLayoutSideCounterPart = &pLayout->m_FrontSide;
						break;
		}
		if (pLayoutSideCounterPart)
		{
			if (pLayout->KindOfProduction() != WORK_AND_TURN)
				fYDiff *= -1.0f;
		
			CLayoutObjectMatrix	objectMatrixCounterPart(pLayoutSideCounterPart->m_ObjectList, CLayoutObjectMatrix::TakePages);	
			nNumRows = objectMatrixCounterPart.GetSize();

			pos = pLayoutSideCounterPart->m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pLayoutSideCounterPart->m_ObjectList.GetNext(pos);
				objectMatrixCounterPart.GetMatrixIndices(&rObject, nRowIndex, nColIndex);
				if (nActiveRefLine == BOTTOM)
				{
					if (pLayout->KindOfProduction() != WORK_AND_TURN)
					{
						if (nRowIndex < nNumRows / 2)
							continue;
					}
					else
						if (nRowIndex >= nNumRows / 2)
							continue;
				}
				else
					if (pLayout->KindOfProduction() != WORK_AND_TURN)
					{
						if (nRowIndex >= nNumRows / 2)
							continue;
					}
					else
						if (nRowIndex < nNumRows / 2)
							continue;

				rObject.SetBottom(rObject.GetBottom() + fYDiff, FALSE);
				rObject.SetTop(rObject.GetTop()		  + fYDiff, FALSE);
			}
		}

		pLayout->m_FrontSide.RecenterPaperY();
		pLayout->m_BackSide.RecenterPaperY();
	}
	else
	{
	    CImpManDoc* pDoc = CImpManDoc::GetDoc();
		switch (pDoc->GetActSight())
		{
		case BOTHSIDES: pLayout->m_FrontSide.RecenterPaperY();
						pLayout->m_BackSide.RecenterPaperY();
						break;
		case FRONTSIDE: pLayout->m_FrontSide.RecenterPaperY();
						break;
		case BACKSIDE : pLayout->m_BackSide.RecenterPaperY();
						break;
		}
	}

	pLayout->m_FrontSide.Invalidate();
	pLayout->m_BackSide.Invalidate();
}


void CPrintSheetView::DoPositionMarks(CPoint point)
{
	CMark*		 pMark		 = NULL;
	CDlgMarkSet* pDlgMarkSet = NULL;
	if (GetDlgMarkSet())
		if (GetDlgMarkSet()->m_hWnd)
		{
			pDlgMarkSet = GetDlgMarkSet();
			if ( (pDlgMarkSet->m_nCurMarkSel >= 0) && (pDlgMarkSet->m_nCurMarkSel < pDlgMarkSet->m_markList.GetSize()) )
				pMark = pDlgMarkSet->m_markList[pDlgMarkSet->m_nCurMarkSel];
		}

	CDlgLayoutObjectProperties* pDlgNewMark = NULL;
	if ( ! pMark)
	{
		pDlgNewMark = GetDlgNewMark();
		if (pDlgNewMark)
			pMark = pDlgNewMark->m_pMark;
	}
	if (pMark)
		if ( (pMark->m_nType == CMark::SectionMark) || (pMark->m_nType == CMark::CropMark) || (pMark->m_nType == CMark::FoldMark) )
			return;

	int			 nSide;
	BOOL		 bBorder;
	CDisplayItem di = GetReflineDI(point, nSide, bBorder, pDlgMarkSet, TRUE, pMark);

	CDlgLayoutObjectGeometry* pDlgLayoutObjectGeometry = GetDlgLayoutObjectGeometry();
	int nEdge = (pDlgLayoutObjectGeometry) ? pDlgLayoutObjectGeometry->GetMarkEdge() : 0;

	BOOL bDrawHorizontal = FALSE;
	BOOL bDrawVertical	 = FALSE;
	switch (nEdge)
	{
	case 0:			if (pDlgLayoutObjectGeometry)
					{
						bDrawHorizontal = (pDlgLayoutObjectGeometry->m_bVariableHeight) ? FALSE : TRUE;
						bDrawVertical	= (pDlgLayoutObjectGeometry->m_bVariableWidth)  ? FALSE : TRUE; 
					}
					break;
	case LEFT:
	case RIGHT:		bDrawVertical	= TRUE; break;
	case BOTTOM:
	case TOP:		bDrawHorizontal = TRUE; break;
	}

	switch (nSide)
	{
	case LEFT:		
	case RIGHT:		
	case XCENTER:	if ( ! bDrawVertical)	return;	
					break;
	case BOTTOM:	
	case TOP:		
	case YCENTER:	if ( ! bDrawHorizontal)	return;	
					break;
	}

	if ( ! di.IsNull())
	{
   		if (pDlgMarkSet || pDlgNewMark)
		{
			if (pDlgMarkSet)
			{
				if ( (pDlgMarkSet->m_nCurMarkSel >= 0) && (pDlgMarkSet->m_nCurMarkSel < pDlgMarkSet->m_markList.GetSize()) )
					if (GetKeyState(VK_CONTROL) < 0) // Ctrl key pressed
					{
						pDlgMarkSet->DuplicateCurrentMark();
						pMark = pDlgMarkSet->GetCurrentMark();
					}
			}

			if ( ! pMark)
				return;


			CDlgLayoutObjectGeometry* pDlgObjectGeometry = GetDlgLayoutObjectGeometry();
			int nReflinePosParamsID = (pDlgObjectGeometry) ? pDlgObjectGeometry->GetReflinePosParamsID() : 1;

			CReflinePositionParams* pReflinePosParams = ((CReflineMark*)pMark)->GetReflinePosParams(nReflinePosParamsID);
			if (pReflinePosParams)
			{
				CReflinePositionParams newReflinePosParams; newReflinePosParams = *pReflinePosParams;
				if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
					newReflinePosParams.SetSheetReference(PLATE_REFLINE, (BYTE)nSide);
				else
					if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
						newReflinePosParams.SetSheetReference(PAPER_REFLINE, (BYTE)nSide);
					else
						if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, BoundingBoxFrame)))
							newReflinePosParams.SetSheetReference(BBOXFRAME_REFLINE, (BYTE)nSide);
						else
							if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
								newReflinePosParams.SetSheetReference(OBJECT_REFLINE, (BYTE)nSide, (CLayoutObject*)di.m_pData, bBorder);

				if ( (nSide == LEFT) || (nSide == RIGHT) || (nSide == XCENTER) )
					newReflinePosParams.m_fDistanceX = 0.0f;
				else
					if ( (nSide == TOP) || (nSide == BOTTOM) || (nSide == YCENTER) )
						newReflinePosParams.m_fDistanceY = 0.0f;

				if (pDlgObjectGeometry)
				{
					pDlgObjectGeometry->CheckSwapReflinePosParams(newReflinePosParams);
					pDlgObjectGeometry->LoadData();
				}
				else
					*pReflinePosParams = newReflinePosParams;

				CPrintSheetMarksView* pView	= (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
				if (pView)
					pView->UpdateSheetPreview();
				else
				{
					Invalidate();
					UpdateWindow();
				}
			}
		}
	}
	else
		m_DisplayList.OnLButtonDown(point); // pass message to display list
}


void CPrintSheetView::DoPositionOutputMedia(CPoint point)
{
	CDlgPDFTargetProps* pDlg = GetDlgOutputMedia()->GetDlgPDFTargetProps();
	int					nSide;
	BOOL				bBorder;
	CDisplayItem		di = GetReflineDI(point, nSide, bBorder, pDlg, TRUE);

	if ( ! di.IsNull())
	{
   		if (pDlg)
		{
			if ( (pDlg->m_nCurTileIndex < 0) || (pDlg->m_nCurTileIndex >= pDlg->m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize()) )
				return;

			CReflinePositionParams* pReflinePosParams = &pDlg->m_pdfTargetProps.m_mediaList[0].m_posInfos[pDlg->m_nCurTileIndex].m_reflinePosParams;

			if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
				pReflinePosParams->SetSheetReference(PLATE_REFLINE, (BYTE)nSide);
			else
				if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
					pReflinePosParams->SetSheetReference(PAPER_REFLINE, (BYTE)nSide);
				else
					if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
						pReflinePosParams->SetSheetReference(OBJECT_REFLINE, (BYTE)nSide, (CLayoutObject*)di.m_pData, bBorder);

			Invalidate();
			UpdateWindow();
		}
	}
	else
		m_DisplayList.OnLButtonDown(point); // pass message to display list
}

void CPrintSheetView::SetAlignLayoutObjects(CPoint point)
{
	int	 nSide;
	BOOL bBorder;
	CDisplayItem di = GetReflineDI(point, nSide, bBorder, GetDlgAlignObjects(), TRUE);
									  
	if ( ! di.IsNull())
	{
		if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, GroupTracker)))
		{
			if (GetDlgAlignObjects())
			{
				if ( (nSide != XCENTER) && (nSide != YCENTER) )
				{
					CLayoutObject* pClosestNeighbour = GetDlgAlignObjects()->FindSelectedGroupClosestNeighbour(nSide, di.m_nGroup, point);
					if ( ! pClosestNeighbour)
						GetDlgAlignObjects()->m_reflinePosParams.SetSheetReference(PAPER_REFLINE, (BYTE)nSide);
					else
					{
						int nOppositeSide = nSide;
						switch (nSide)
						{
						case LEFT:		nOppositeSide = RIGHT;	break;
						case RIGHT:		nOppositeSide = LEFT;	break;
						case BOTTOM:	nOppositeSide = TOP;	break;
						case TOP:		nOppositeSide = BOTTOM;	break;
						}
						GetDlgAlignObjects()->m_reflinePosParams.SetSheetReference(OBJECT_REFLINE, (BYTE)nOppositeSide, pClosestNeighbour, FALSE);
					}
				}

				GetDlgAlignObjects()->m_reflinePosParams.m_nObjectReferenceLineInX = 255;
				GetDlgAlignObjects()->m_reflinePosParams.m_nObjectReferenceLineInY = 255;
				switch (nSide)
				{
				case LEFT:		GetDlgAlignObjects()->m_reflinePosParams.m_nObjectReferenceLineInX = LEFT;		break;
				case XCENTER:	GetDlgAlignObjects()->m_reflinePosParams.m_nObjectReferenceLineInX = XCENTER;	break;
				case RIGHT:		GetDlgAlignObjects()->m_reflinePosParams.m_nObjectReferenceLineInX = RIGHT;		break;
				case BOTTOM:	GetDlgAlignObjects()->m_reflinePosParams.m_nObjectReferenceLineInY = BOTTOM;	break;
				case YCENTER:	GetDlgAlignObjects()->m_reflinePosParams.m_nObjectReferenceLineInY = YCENTER;	break;
				case TOP:		GetDlgAlignObjects()->m_reflinePosParams.m_nObjectReferenceLineInY = TOP;		break;
				}
		
				m_DisplayList.Invalidate();		// reflines will be displayed
				GetDlgAlignObjects()->InitReflinePosParams(di.m_nGroup);
			}
		}
		else
		{
			CLayoutObject* pLayoutObject = NULL;
			CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			pLayoutObject = (pDI) ? (CLayoutObject*)pDI->m_pData : NULL;
			if ( ! pLayoutObject)
				return;

			GetDlgAlignObjects()->m_reflinePosParams.m_nReferenceLineInX = 255;
			GetDlgAlignObjects()->m_reflinePosParams.m_nReferenceLineInY = 255;

			if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
				GetDlgAlignObjects()->m_reflinePosParams.SetSheetReference(PLATE_REFLINE, (BYTE)nSide);
			else
				if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
					GetDlgAlignObjects()->m_reflinePosParams.SetSheetReference(PAPER_REFLINE, (BYTE)nSide);
				else
					if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
						GetDlgAlignObjects()->m_reflinePosParams.SetSheetReference(OBJECT_REFLINE, (BYTE)nSide, (CLayoutObject*)di.m_pData, bBorder);
	
			m_DisplayList.Invalidate();		// reflines will be displayed
			GetDlgAlignObjects()->InitReflinePosParams(-1, TRUE);
		}
	}
	else
	{
		CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineXDistance));
		if (pDI)
		{
			if ( ! pDI->m_BoundingRect.PtInRect(point))
				m_DisplayList.Invalidate();			// reflines and edit will be removed
		}
		else
		{
			pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineYDistance));
			if (pDI)
			{
				if ( ! pDI->m_BoundingRect.PtInRect(point))
					m_DisplayList.Invalidate();		// reflines and edit will be removed
			}
		}
		m_DisplayList.OnLButtonDown(point); // pass message to display list
	}
}

void CPrintSheetView::DoPositionLayoutObjects(CPoint point)
{
	int	 nSide;
	BOOL bBorder;
	HWND hWnd = (GetDlgMoveObjects()) ? GetDlgMoveObjects()->m_hWnd : NULL;
	CDisplayItem di = (hWnd) ? GetReflineDI(point, nSide, bBorder, GetDlgAlignObjects(), TRUE) : GetReflineDI(point, nSide, bBorder, GetDlgLayoutObjectGeometry(), TRUE);
									  

	if ( ! di.IsNull())
	{
		CLayoutObject* pLayoutObject = NULL;
		if (IsOpenDlgLayoutObjectGeometry())
			pLayoutObject = (GetDlgLayoutObjectGeometry()->m_pParent) ? GetDlgLayoutObjectGeometry()->m_pParent->m_pLayoutObject : NULL;
		else
		{
			CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			pLayoutObject = (pDI) ? (CLayoutObject*)pDI->m_pData : NULL;
		}
		if ( ! pLayoutObject)
			return;

		BOOL bDoUpdate = TRUE;
		if (hWnd)
		{
			if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
				GetDlgMoveObjects()->m_reflinePosParams.SetSheetReference(PLATE_REFLINE, (BYTE)nSide);
			else
				if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
					GetDlgMoveObjects()->m_reflinePosParams.SetSheetReference(PAPER_REFLINE, (BYTE)nSide);
				else
					if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
						GetDlgMoveObjects()->m_reflinePosParams.SetSheetReference(OBJECT_REFLINE, (BYTE)nSide, (CLayoutObject*)di.m_pData, bBorder);

			if (m_DisplayList.GetNumSelectedItems() == 1)
			{
				pLayoutObject->m_reflinePosParams = GetDlgMoveObjects()->m_reflinePosParams;
				pLayoutObject->UpdateReflineDistances();
				GetDlgMoveObjects()->m_fDistanceX = pLayoutObject->m_reflinePosParams.m_fDistanceX;
				GetDlgMoveObjects()->m_fDistanceY = pLayoutObject->m_reflinePosParams.m_fDistanceY;
				GetDlgMoveObjects()->m_reflinePosParams.m_fDistanceX = GetDlgMoveObjects()->m_fDistanceX;
				GetDlgMoveObjects()->m_reflinePosParams.m_fDistanceY = GetDlgMoveObjects()->m_fDistanceY;
				GetDlgMoveObjects()->UpdateData(FALSE);
			}
			else
			{
				GetDlgMoveObjects()->InitReflinePosParams();
				bDoUpdate = FALSE;
			}
		}
		else
			if (IsOpenDlgLayoutObjectGeometry())
			{
				if (pLayoutObject)
				{
					if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
						pLayoutObject->m_reflinePosParams.SetSheetReference(PLATE_REFLINE, (BYTE)nSide);
					else
						if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
							pLayoutObject->m_reflinePosParams.SetSheetReference(PAPER_REFLINE, (BYTE)nSide);
						else
							if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
								pLayoutObject->m_reflinePosParams.SetSheetReference(OBJECT_REFLINE, (BYTE)nSide, (CLayoutObject*)di.m_pData, bBorder);

					GetDlgLayoutObjectGeometry()->OnLayoutReflineChanged();
				}
			}

		if (bDoUpdate)
		{
			Invalidate();
			UpdateWindow();
		}
	}
	else
		m_DisplayList.OnLButtonDown(point); // pass message to display list
}

BOOL CPrintSheetView::OnStatusChanged()		// display list status changed
{
	theApp.OnUpdateView(RUNTIME_CLASS(CProdDataPrintToolsView));

	//if (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)) == 1)
	//{
	//	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	//	CRect rcInvalidate = pDI->m_BoundingRect;
	//	rcInvalidate.InflateRect(8,8);
	//	InvalidateRect(rcInvalidate);
	//}

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));

	HWND hWndDlgLayoutObjectProperties  = (GetDlgLayoutObjectProperties()) ? GetDlgLayoutObjectProperties()->m_hWnd : NULL;
	if ( ! IsOpenDlgAlignObjects() && ! hWndDlgLayoutObjectProperties )
		return FALSE;

	if (IsOpenDlgAlignObjects() && ! m_DisplayList.m_bIsInsideMultiSelect)
		if (GetFocus() != &m_reflineDistanceEdit)
			GetDlgAlignObjects()->InitData((m_DisplayList.m_bHasBeenDragged) ? FALSE : TRUE);

	if (IsOpenDlgNewMark())
		return FALSE;
	else
		if (hWndDlgLayoutObjectProperties)
		{
			CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			if (pDI)
				GetDlgLayoutObjectProperties()->ReinitDialog((CLayoutObject*)pDI->m_pData, (CPrintSheet*)pDI->m_pAuxData);
			else
			{
				pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
				if (pDI)
				{
					CExposure* pExposure = (CExposure*)pDI->m_pData;
					CLayoutObject* pLayoutObject = (pExposure) ? pExposure->GetLayoutObject() : NULL;
					GetDlgLayoutObjectProperties()->ReinitDialog(pLayoutObject, (CPrintSheet*)pDI->m_pAuxData);
				}
				else
					GetDlgLayoutObjectProperties()->ReinitDialog((CLayoutObject*)NULL, (CPrintSheet*)NULL);
			}
		}

	return TRUE;
}

BOOL CPrintSheetView::GetReflineDIFilterMarks(CDisplayItem& rDI)
{
	CLayoutObject* pObject = (CLayoutObject*)rDI.m_pData;
	if (pObject)
		if (pObject->m_nType == CLayoutObject::ControlMark)
			return FALSE;	// don't care about

	return TRUE;	// take into account
}

CDisplayItem CPrintSheetView::GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder, CDlgMarkSet* pDlg, BOOL bClicked, CMark* pMark)
{
	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
		return CDisplayItem();

	if ( ! bClicked && pMark)
		if ( (pMark->m_nType == CMark::SectionMark) || (pMark->m_nType == CMark::CropMark) || (pMark->m_nType == CMark::FoldMark) )
			return CDisplayItem();	// section, crop and fold marks not be positioned by reflines -> don't show refline feedback

	CDisplayItem di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineXDistance));
	if (di.IsNull())
		di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineYDistance));
	if ( ! di.IsNull())			// Clicking inside refline distance has highest priority (for drag/drop). 
		return CDisplayItem();	// So leave and message will be passed to display list 

	bBorder	= FALSE;
	di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark));
	if ( ! di.IsNull() && bClicked)
	{
		if (pDlg)
		{
			int i = pDlg->m_markList.GetIndex((CMark*)di.m_pData);
			if (i != pDlg->m_nCurMarkSel)
			{
				pDlg->m_markListCtrl.SetItemState(pDlg->m_nCurMarkSel, 0, LVNI_SELECTED);
				pDlg->m_markListCtrl.SetItemState(i, LVNI_SELECTED, LVNI_SELECTED);// Clicking inside inactive mark activates this mark
			}
		}
		return CDisplayItem();
	}

	BOOL bIsLayoutObject = FALSE;
	if (di.IsNull())
	{
		di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
		if (di.IsNull())
		{
			di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
			if (di.IsNull())
			{
				di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, BoundingBoxFrame));

				if (pLayout->IsUniform())
				{
					// check LayoutObjects anyway (even if BoundingBoxFrame has been touched), because LayoutObjects center handle could be touched which has priority
					int nSideLayoutObject = -1;
					CDisplayItem diLayoutObject = m_DisplayList.GetItemNearby(point, &nSideLayoutObject, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), 2, 0, &GetReflineDIFilterMarks);		// first try with border ( 2 = auxauxrect)
					if (diLayoutObject.IsNull())
						diLayoutObject = m_DisplayList.GetItemNearby(point, &nSideLayoutObject, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), TRUE, 0, &GetReflineDIFilterMarks);	// now try w/o border
					else
						bBorder = TRUE;
					if ( ! diLayoutObject.IsNull())
					{
						CheckCenterHandle(diLayoutObject.m_AuxRect, nSideLayoutObject, point);
						if ((nSideLayoutObject == XCENTER) || (nSideLayoutObject == YCENTER))	// center handles touched
						{
							bBorder = FALSE;
							nSide = nSideLayoutObject;
							return diLayoutObject;
						}
						else
							if (di.IsNull())	// LayoutObject touched, but not BoundingBoxFrame
							{
								bBorder = FALSE;
								nSide = nSideLayoutObject;
								return diLayoutObject;
							}
					}
				}
				// here: BoundingBoxFrame touched
			}
			// here: Sheet touched
		}
		// here: Plate touched

		if (di.IsNull())
			return CDisplayItem();
	}

	CRect rcRect = (bIsLayoutObject) ? di.m_AuxRect : di.m_BoundingRect;
	CheckCenterHandle(di.m_BoundingRect, nSide, point);

	if ((nSide == XCENTER) || (nSide == YCENTER))
		bBorder = FALSE;
	
	return di;
}

void CPrintSheetView::CheckCenterHandle(CRect rcRect, int& nSide, CPoint point)
{
	switch (nSide)
	{
	case LEFT:
	case RIGHT:		nSide = (abs(point.y - rcRect.CenterPoint().y) <= 5) ? YCENTER : nSide; break;
	case BOTTOM:
	case TOP:		nSide = (abs(point.x - rcRect.CenterPoint().x) <= 5) ? XCENTER : nSide; break;
	}
}

CDisplayItem CPrintSheetView::GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder, CDlgPDFTargetProps* pDlg, BOOL bClicked)
{
	CDisplayItem di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineXDistance));
	if (di.IsNull())
		di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineYDistance));
	if ( ! di.IsNull())			// Clicking inside refline distance has highest priority (for drag/drop). 
		return CDisplayItem();	// So leave and message will be passed to display list 

	BOOL bIsLayoutObject = FALSE;
	bBorder	= FALSE;
	di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
	if (di.IsNull())
	{
		di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
		if (di.IsNull())
		{
			bIsLayoutObject = TRUE;
			di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), 2);			// first try with border ( 2 = auxauxrect)
			if (di.IsNull())
				di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), TRUE);	// now try w/o border
			else
				bBorder = TRUE;
		}
	}

	if (di.IsNull())
	{
		di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia));
		if ( ! di.IsNull() && bClicked)
			return CDisplayItem();
	}

	if (di.IsNull())
		return CDisplayItem();

	CRect rcRect = (bIsLayoutObject) ? di.m_AuxRect : di.m_BoundingRect;
	switch (nSide)
	{
	case LEFT:
	case RIGHT:		nSide = (abs(point.y - rcRect.CenterPoint().y) <= 5) ? YCENTER : nSide; break;
	case BOTTOM:
	case TOP:		nSide = (abs(point.x - rcRect.CenterPoint().x) <= 5) ? XCENTER : nSide; break;
	}

	if ((nSide == XCENTER) || (nSide == YCENTER))
		bBorder = FALSE;
	
	return di;
}

CDisplayItem CPrintSheetView::GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder, CDlgAlignObjects* pDlg, BOOL bClicked)
{
	if ( ( ! m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject))) && ( ! m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet))) )
		return CDisplayItem();

 	CDisplayItem di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineXDistance));
	if (di.IsNull())
		di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineYDistance));
	if ( ! di.IsNull())			// Clicking inside refline distance has highest priority (for drag/drop). 
		return CDisplayItem();	// So leave and message will be passed to display list 

	BOOL bIsLayoutObject = FALSE;
	bBorder	= FALSE;
	di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, GroupTracker), TRUE);		
	if (di.IsNull())
	{
		di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
		if (di.IsNull())
		{
			di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
			if (di.IsNull())
			{
				bIsLayoutObject = TRUE;
				di = m_DisplayList.GetItemNearbyNonSelected(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), 2);				// first try with border
				if (di.IsNull())
					di = m_DisplayList.GetItemNearbyNonSelected(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), TRUE);		// now try w/o border
				else
					bBorder = TRUE;

				if ( ! di.IsNull())
				{
					CLayoutObject* pObject = (CLayoutObject*)di.m_pData;
					if (pObject)
						if (pObject->m_nType == CLayoutObject::ControlMark)
							return CDisplayItem();
				}
			}
		}
	}

	if (di.IsNull())
		return CDisplayItem();

	CRect rcRect = (bIsLayoutObject) ? di.m_AuxRect : di.m_BoundingRect;
	switch (nSide)
	{
	case LEFT:
	case RIGHT:		nSide = (abs(point.y - rcRect.CenterPoint().y) <= 5) ? YCENTER : nSide; break;
	case BOTTOM:
	case TOP:		nSide = (abs(point.x - rcRect.CenterPoint().x) <= 5) ? XCENTER : nSide; break;
	}

	if ((nSide == XCENTER) || (nSide == YCENTER))
		bBorder = FALSE;
	
	return di;
}

CDisplayItem CPrintSheetView::GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder, CDlgLayoutObjectGeometry* pDlg, BOOL bClicked)
{
	if ( ! MarksViewActive())
		if ( ! m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
			return CDisplayItem();

	CDisplayItem di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineXDistance));
	if (di.IsNull())
		di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineYDistance));
	if ( ! di.IsNull())			// Clicking inside refline distance has highest priority (for drag/drop). 
		return CDisplayItem();	// So leave and message will be passed to display list 

	if (di.IsNull())
	{
		di = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		if ( ! di.IsNull())
			if (di.m_nState != CDisplayItem::Normal)
				return CDisplayItem();
	}

	BOOL bIsLayoutObject = FALSE;
	bBorder	= FALSE;
	di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
	if (di.IsNull())
	{
		di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
		if (di.IsNull())
		{
			bIsLayoutObject = TRUE;
			di = m_DisplayList.GetItemNearbyNonSelected(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), 2);	// first try with border
			if (di.IsNull())
				di = m_DisplayList.GetItemNearbyNonSelected(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), TRUE);		// now try w/o border
			else
				bBorder = TRUE;

			if ( ! di.IsNull())
			{
				CLayoutObject* pObject = (CLayoutObject*)di.m_pData;
				if (pObject)
					if (pObject->m_nType == CLayoutObject::ControlMark)
						return CDisplayItem();
			}
		}
	}

	if (di.IsNull())
		return CDisplayItem();

	CRect rcRect = (bIsLayoutObject) ? di.m_AuxRect : di.m_BoundingRect;
	switch (nSide)
	{
	case LEFT:
	case RIGHT:		nSide = (abs(point.y - rcRect.CenterPoint().y) <= 5) ? YCENTER : nSide; break;
	case BOTTOM:
	case TOP:		nSide = (abs(point.x - rcRect.CenterPoint().x) <= 5) ? XCENTER : nSide; break;
	}

	if ((nSide == XCENTER) || (nSide == YCENTER))
		bBorder = FALSE;
	
	return di;
}

CDisplayItem CPrintSheetView::GetReflineDI(CPoint& point, int& nSide, BOOL& bBorder, BOOL bClicked)
{
	bBorder	= FALSE;

	CDisplayItem di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), 2);	// first try with border ( 2 = auxauxrect)
	if (di.IsNull())
		di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject), TRUE);			// now try w/o border
	else
		bBorder = TRUE;

	if (di.IsNull() || ! InsideAnyFoldSheet(point))
	{
		di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, WebYRefLine1));
		if ( ! di.IsNull())	
		{
			nSide = YCENTER;
			return di;	
		}
		di = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetView, WebYRefLine2));
		if ( ! di.IsNull())	
		{
			nSide = YCENTER;
			return di;	
		}
	}

	if (di.IsNull())
		return CDisplayItem();
	
	CRect rcRect = di.m_AuxRect;
	switch (nSide)
	{
	case LEFT:
	case RIGHT:		nSide = (abs(point.y - rcRect.CenterPoint().y) <= 5) ? YCENTER : nSide; break;
	case BOTTOM:
	case TOP:		nSide = (abs(point.x - rcRect.CenterPoint().x) <= 5) ? XCENTER : nSide; break;
	}
	if ((nSide == XCENTER) || (nSide == YCENTER))
		bBorder = FALSE;

	return di;
}

BOOL CPrintSheetView::InsideAnyFoldSheet(CPoint& point)
{
	CLayout* pLayout = GetActLayout();
	if ( ! pLayout)
		return FALSE;

	for (int nComponentRefIndex = 0; nComponentRefIndex < pLayout->m_FoldSheetLayerRotation.GetSize(); nComponentRefIndex++)
	{
		CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		CRect		  rcFoldSheet;
		BOOL		  bFirst = TRUE;
		while (pDI)
		{
			CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
			if (pObject->m_nComponentRefIndex == nComponentRefIndex)
			{
				if (bFirst)
					rcFoldSheet = pDI->m_BoundingRect;
				else
					rcFoldSheet.UnionRect(rcFoldSheet, pDI->m_BoundingRect);
				bFirst = FALSE;
			}
			pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		}
		if (rcFoldSheet.PtInRect(point))
			return TRUE;
	}
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// enable OLE DragDrop functionality - needed in conjunction with display list for foldsheet manipulation

int CPrintSheetView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CREATE_INPLACE_EDIT(InplaceEditPageNum);
	CREATE_INPLACE_EDIT(InplaceEditBleed);
	CREATE_INPLACE_EDIT(InplaceEditGlobalMask);
	CREATE_INPLACE_EDIT(InplaceEditShingling);
	CREATE_INPLACE_EDIT(InplaceEditSideName);

	CREATE_INPLACE_BUTTON(InplaceButtonShingling, IDB_RELEASE_LOCK, IDS_RELEASE_SHINGLING_LOCK);

	m_reflineXSpin.Create(WS_CHILD | UDS_HORZ, CRect(0,0,0,0), this, 1);
	m_reflineYSpin.Create(WS_CHILD,			   CRect(0,0,0,0), this, 2);
	m_reflineXSpin.SetRange(0, 1);
	m_reflineYSpin.SetRange(0, 1);

	m_reflineDistanceEdit.Create(WS_CHILD|WS_BORDER|ES_AUTOHSCROLL,	CRect(0,0,0,0), this, 3);
	//m_reflineDistanceFont.CreatePointFont(14, _T("MS Sans Serif"));
	m_reflineDistanceFont.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	m_reflineDistanceEdit.SetFont(&m_reflineDistanceFont, FALSE);

	if( m_dropTarget.Register( this ) )
		return 0;
	else
		return -1;
}

void CPrintSheetView::PostNcDestroy() 
{
	m_reflineDistanceFont.DeleteObject();

	CScrollView::PostNcDestroy();
}

void CPrintSheetView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
    switch (nChar)
    {
    case VK_HOME: 
		OnVScroll(SB_TOP, 0, NULL);
        OnHScroll(SB_LEFT, 0, NULL);
        break;
    case VK_END: 
		OnVScroll(SB_BOTTOM, 0, NULL);
        OnHScroll(SB_RIGHT, 0, NULL);
        break;
    case VK_UP: 
		OnVScroll(SB_LINEUP, 0, NULL);
        break;
    case VK_DOWN: 
		OnVScroll(SB_LINEDOWN, 0, NULL);
        break;
    case VK_PRIOR: 
		OnVScroll(SB_PAGEUP, 0, NULL);
        break;
    case VK_NEXT: 
		OnVScroll(SB_PAGEDOWN, 0, NULL);
        break;
    case VK_LEFT: 
		OnHScroll(SB_LINELEFT, 0, NULL);
        break;
    case VK_RIGHT: 
		OnHScroll(SB_LINERIGHT, 0, NULL);
        break;
	case VK_CONTROL:
		{
			if (HandActive())
			{
				CFrameWnd* pFrame = GetParentFrame();
				if (pFrame)
					if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
						((CNewPrintSheetFrame*)pFrame)->m_bHandActive = FALSE;
			}
			if (ViewMode() == ID_ZOOM)
			{
				if (GetCursor() == theApp.m_CursorMagnifyIncrease)
					SetCursor(theApp.m_CursorMagnifyDecrease);
			}
			else
				if ( MarksViewActive() && m_pBkBitmap && ! HandActive())
					SetCursor(theApp.m_CursorCopy);
		}
		break;
	case VK_SHIFT:
		{
			if (GetKeyState(VK_CONTROL) & 0x8000)
				break;

			CRect clientRect;
			GetClientRect(&clientRect);
			if ( (GetTotalSize().cx > clientRect.Width()) || (GetTotalSize().cy > clientRect.Height()) )
			{
				SetCursor(theApp.m_CursorHand);
				CFrameWnd* pFrame = GetParentFrame();
				if (pFrame)
					if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
						((CNewPrintSheetFrame*)pFrame)->m_bHandActive = TRUE;
			}
		}
		break;
	default: 
		break;
    }

//	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CPrintSheetView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if (nChar == VK_CONTROL)
	{
		if (ViewMode() == ID_ZOOM)
			if (GetCursor() == theApp.m_CursorMagnifyDecrease)
				SetCursor(theApp.m_CursorMagnifyIncrease);

		if ( ((CPrintSheetFrame*)GetParentFrame())->m_bMarksViewActive)
			SetCursor(theApp.m_CursorStandard);
	}
	else
	if (nChar == VK_SHIFT)
	{
		if (ViewMode() == ID_ZOOM)
			SetCursor(theApp.m_CursorMagnifyIncrease);
		else
		{
			SetCursor(theApp.m_CursorStandard);
			CFrameWnd* pFrame = GetParentFrame();
			if (pFrame)
				if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
					((CNewPrintSheetFrame*)pFrame)->m_bHandActive = FALSE;
		}
	}

	CScrollView::OnKeyUp(nChar, nRepCnt, nFlags);
}

BOOL CPrintSheetView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// The default call of OnMouseWheel causes an assertion
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion
}

/////////////////////////////////////////////////////////////////////////////
// Messages back from display list

BOOL CPrintSheetView::OnMultiSelect()	// multiple items has been selected
{
    BOOL		  bPage		   = FALSE;
    BOOL		  bControlMark = FALSE;
    BOOL		  bPlateTab	   = FALSE;
    BOOL		  bFrontSide   = FALSE;
    BOOL		  bBackSide    = FALSE;
    CDisplayItem* pDI		   = m_DisplayList.GetFirstSelectedItem();
    while (pDI)
    {
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
        	m_DisplayList.DeselectItem(*pDI);
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, RefEdgePrintSheet)))
        	m_DisplayList.DeselectItem(*pDI);
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
        	m_DisplayList.DeselectItem(*pDI);

        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
        {
        	if (((CLayoutObject*)pDI->m_pData)->m_nType == CLayoutObject::Page)
        		bPage = TRUE;
        	else
        		bControlMark = TRUE;

        	if (((CLayoutObject*)pDI->m_pData)->GetLayoutSide()->m_nLayoutSide == FRONTSIDE)
        		bFrontSide = TRUE;
        	else
        		bBackSide  = TRUE;
        }
        if (ShowPlate())
        	if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab)))
        		bPlateTab = TRUE;

        pDI = m_DisplayList.GetNextSelectedItem(pDI);
    }

    if (ShowPlate())
    {
        if (!bPlateTab)
        {
        	pDI	= m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
        	while (pDI)
        	{
        		if (pDI->m_nLastPaintedState != pDI->m_nState)
        		{
        			bPlateTab = TRUE;
        			break;
        		}
        		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
        	}
        }
        if (bPlateTab)
        {
        	pDI	= m_DisplayList.GetFirstSelectedItem();
        	while (pDI)
        	{
        		if (!pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab)))
        			m_DisplayList.DeselectItem(*pDI);
        		pDI = m_DisplayList.GetNextSelectedItem(pDI);
        	}
        	return FALSE;
        }
    }

    if (bFrontSide && bBackSide)
    {
        pDI	= m_DisplayList.GetFirstSelectedItem();
        while (pDI)
        {
        	if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
        		if (((CLayoutObject*)pDI->m_pData)->GetLayoutSide()->m_nLayoutSide == BACKSIDE)
        				m_DisplayList.DeselectItem(*pDI);
        	pDI = m_DisplayList.GetNextSelectedItem(pDI);
        }
    }

    if (bPage && bControlMark)
    {
		CDlgMultiSelect dlg;
		int nID = dlg.DoModal();
        pDI		= m_DisplayList.GetFirstSelectedItem();
        while (pDI)
        {
        	if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
        		if (((CLayoutObject*)pDI->m_pData)->m_nType == CLayoutObject::Page)
        			if ((nID == ID_SELECT_MARKS) || (nID == IDCANCEL))
        				m_DisplayList.DeselectItem(*pDI);
        	if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
        		if (((CLayoutObject*)pDI->m_pData)->m_nType == CLayoutObject::ControlMark)
        			if ((nID == ID_SELECT_PAGES) || (nID == IDCANCEL))
        				m_DisplayList.DeselectItem(*pDI);

        	pDI = m_DisplayList.GetNextSelectedItem(pDI);
        }
        if (nID != IDCANCEL)
        	SetLayoutObjectSelected(TRUE);
    }

	return TRUE;
}


BOOL CPrintSheetView::OnActivateLayoutObject(CDisplayItem* pDI, CPoint point, LPARAM)	// page has been activated
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if (IsOpenDlgAlignObjects())
		return FALSE;

    CLayoutObject* pObject		 = (CLayoutObject*)pDI->m_pData;
    CPageTemplate* pPageTemplate = NULL;
    int			   nIDCorner	 = LEFT_TOP;
    CRect		   rectID;
    CRect		   editRect;

    if (pObject->m_nType == CLayoutObject::Page)
    {
        pPageTemplate   = pObject->GetCurrentPageTemplate((CPrintSheet*)pDI->m_pAuxData);
        if (pPageTemplate)
        	rectID = CalcPageIDRect(pDI->m_AuxRect, pPageTemplate->m_strPageID,
        							pObject->m_nHeadPosition, pObject->m_nIDPosition, &nIDCorner, &editRect);
        else
        	rectID = CRect(0,0,0,0);

		if (rectID.PtInRect(point))	// PageID clicked -> edit PageID
		{							
			if (pPageTemplate)
			{
				if (pPageTemplate->IsFlatProduct())
				{
					if (m_DisplayList.GetNumSelectedItems() > 1)
					{
						CDisplayItem* pTestDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
						while (pTestDI)
						{
							CLayoutObject* pTestObject = (CLayoutObject*)pTestDI->m_pData;
							if ( (pObject->GetWidth() != pTestObject->GetWidth()) || (pObject->GetHeight() != pTestObject->GetHeight()) )
							{
								AfxMessageBox(IDS_DIFFERENT_LABELSIZE);
								return FALSE;
							}
							pTestDI = m_DisplayList.GetNextSelectedItem(pTestDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
						}
					}
				}

        		int nGrowTo = ((nIDCorner == LEFT_TOP) || (nIDCorner == LEFT_BOTTOM)) ? CInplaceEdit::GrowToRight : CInplaceEdit::GrowToLeft;
       			m_InplaceEditPageNum.Activate(editRect, pPageTemplate->m_strPageID, nGrowTo);
				m_DisplayList.m_bDeselectAll = FALSE;
				pDI->m_nState = CDisplayItem::Active;
			}
		}
		else
		{				   
			if (GetDlgLayoutObjectProperties())
				if ( ! GetDlgLayoutObjectProperties()->m_hWnd)
					GetDlgLayoutObjectProperties()->Create(pObject, NULL, (CPrintSheet*)pDI->m_pAuxData, this, FALSE, TRUE);
		}

		if (pDoc->IsModified())
		{
			pDoc->m_PageSourceList.InitializePageTemplateRefs();
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();	// because panorama pages could be arised or destroyed
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
		}
	}
	else
	{
		if (GetDlgLayoutObjectProperties())
			if ( ! GetDlgLayoutObjectProperties()->m_hWnd)
			{
				CLayout* pLayout = pObject->GetLayout();
				if (pLayout)
				{
					CMark* pMark = pLayout->m_markList.FindMark(pObject->m_strFormatName);
					if (pMark)
						if (pMark->IsAutoMark())
						{
							HKEY hKey;
							CString strKey = theApp.m_strRegistryKey;
							if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)theApp.m_strRegistryKey, &hKey) == ERROR_SUCCESS)
							{
								CString		strAutoMarksFolder;
								const int	nValueLenght = max(_MAX_PATH, MAX_TEXT) + 2;
								TCHAR		szValue[nValueLenght];
								DWORD dwSize = sizeof(szValue);
								if (RegQueryValueEx(hKey, _T("AutoMarkFolder"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
									strAutoMarksFolder = szValue;
								RegCloseKey(hKey);
								CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
								pFrame->m_pDlgMarkSet->m_bOpenAutoMarks    = TRUE;
								pFrame->m_pDlgMarkSet->m_strCurrentFolder  = strAutoMarksFolder;
								pFrame->m_pDlgMarkSet->m_strCurrentMarkset = _T("");
								pFrame->m_pDlgMarkSet->m_strMarkName	   = (pMark) ? pMark->m_strMarkName : _T("");
								pFrame->m_pDlgMarkSet->Create(IDD_MARKSET, this);
							}
						}
						else
						{
							//GetDlgLayoutObjectProperties()->Create(pMark, (CPrintSheet*)pDI->m_pAuxData, this, 0, TRUE);
							CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
							if (pView)
								pView->PostMessage(WM_COMMAND, IDC_PD_MODIFY_MARK);
						}
				}
			}
	}

	return TRUE;
}

void CPrintSheetView::InvalidateItemsOnOtherSheets(CLayoutObject* pObject, CPrintSheet* pSheet)
{
    CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
    while (pDI)
    {
        if (pDI->m_pAuxData != pSheet)
        	if (pDI->m_pData == pObject)
        		InvalidateRect(pDI->m_BoundingRect);
        pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
    }
}

BOOL CPrintSheetView::OnDeactivateLayoutObject(CDisplayItem* pDI, CPoint /*point*/, LPARAM)		// page has been deactivated
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL bUpdate = FALSE;
    if (GetFocus() == &m_InplaceEditPageNum)	// page has been edited
    {
	    CLayoutObject* pObject = (CLayoutObject*)pDI->m_pData;
		if ( ! pObject)
			return FALSE;

		//if ( ! pObject->IsFlatProduct())
	        UpdatePageID(pDI, m_InplaceEditPageNum.GetString());
		//else
		//{
		//	CFlatProduct&	 rFlatProduct		= pDoc->m_flatProducts.FindByIndex(pObject->m_nComponentRefIndex);
		//	int				 nFlatProductsIndex = pDoc->GetFlatProductsIndex();
		//	CLayoutObject*	 pCounterPartObject = pObject->FindCounterpartObject();
		//	CString		     strName			= m_InplaceEditPageNum.GetString();
		//	POSITION	     pos				= pDoc->m_PageTemplateList.GetHeadPosition(nFlatProductsIndex);
		//	int			     nIndex			    = 0;
		//	BOOL		     bExisting		    = FALSE;
		//	while (pos)
		//	{
		//		CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetNext(pos, nFlatProductsIndex);
		//		if (rTemplate.m_strPageID.CompareNoCase(strName) == 0)
		//		{
		//			bExisting = TRUE;
		//			if ( (pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fTrimSizeWidth != pObject->GetWidth()) || (pDoc->m_flatProducts.FindByIndex(nIndex/2).m_fTrimSizeHeight != pObject->GetHeight()) )
		//			{
		//				CString str;
		//				AfxFormatString1(str, IDS_LABELTYPE_EXISTING, strName);
		//				AfxMessageBox(str);
		//			}
		//			else
		//			{
		//				pObject->m_nComponentRefIndex = nIndex/2;
		//				if (pCounterPartObject)
		//					pCounterPartObject->m_nComponentRefIndex = nIndex/2;
		//				bUpdate = TRUE;
		//			}
		//			break;
		//		}
		//		nIndex++;
		//	}
		//	if ( ! bExisting)
		//	{
		//		rFlatProduct.m_strProductName = strName;
		//		pDoc->m_flatProducts.AddTail(rFlatProduct);
		//		pObject->m_nComponentRefIndex = pDoc->m_flatProducts.GetSize() - 1;
		//		if (pCounterPartObject)
		//			pCounterPartObject->m_nComponentRefIndex = pObject->m_nComponentRefIndex;
		//		bUpdate = TRUE;
		//	}
		//	if (bUpdate)
		//	{
		//		pDI = m_DisplayList.GetFirstSelectedItem();		// assign all other selected objects
		//		while (pDI)
		//		{
		//			CLayoutObject* pRefObject = (CLayoutObject*)pDI->m_pData;
		//			CLayoutObject* pRefCounterPartObject = pRefObject->FindCounterpartObject();
		//			pRefObject->m_nComponentRefIndex = pObject->m_nComponentRefIndex;
		//			if (pRefCounterPartObject)
		//				pRefCounterPartObject->m_nComponentRefIndex = pRefObject->m_nComponentRefIndex;
		//			pDI = m_DisplayList.GetNextSelectedItem(pDI);
		//		}

		//		pDoc->m_PageTemplateList.SynchronizeToFlatProductList(pDoc->m_flatProducts, nFlatProductsIndex);
		//		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		//		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();

		//		CLayout*	 pLayout	 = pObject->GetLayout();
		//		CLayoutSide* pLayoutSide = (pObject) ? pObject->GetLayoutSide() : NULL;
		//		if (pLayoutSide)
		//			pLayoutSide->AnalyseObjectGroups();
		//	}
		//	m_DisplayList.DeselectAll();
		//}
        m_InplaceEditPageNum.Deactivate();
    }
 
    SetLayoutObjectSelected(FALSE);

	if (bUpdate)
	{
       	pDoc->SetModifiedFlag();
       	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
       	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),	NULL, 0, NULL);
       	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),	NULL, 0, NULL);
	}

	return TRUE;
}

void CPrintSheetView::OnKillFocusInplaceEditPageNum() 		// Message from InplaceEdit
{
    CDisplayItem* pDI = m_DisplayList.GetActiveItem();
	if (pDI)
	{
		m_DisplayList.DeactivateItem(*pDI);
		m_DisplayList.InvalidateItem(pDI);
		UpdatePageID(pDI, m_InplaceEditPageNum.GetString());
	}
}

void CPrintSheetView::OnCharInplaceEditPageNum(UINT nChar) // Message from InplaceEdit
{
    switch (nChar)
    {
    case VK_TAB:	// Go to next page and activate item and InplaceEdit
        {
        	CDisplayItem* pDI = m_DisplayList.GetActiveItem();
        	if (pDI)
        	{
        		m_DisplayList.DeactivateItem(*pDI);
        		m_DisplayList.InvalidateItem(pDI);
        		CDisplayItem* pNextDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
        		if (!pNextDI)
        			pNextDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
        		while (pNextDI)
				{
        			if (!pNextDI->IsEqual(pDI))
        			{
        				CLayoutObject* pObject		 = (CLayoutObject*)pNextDI->m_pData;
        				CPageTemplate* pPageTemplate = pObject->GetCurrentPageTemplate((CPrintSheet*)pNextDI->m_pAuxData);
						if (pPageTemplate)
						{
        					CPoint pt = CalcPageIDRect(pNextDI->m_BoundingRect, pPageTemplate->m_strPageID,
        											   pObject->m_nHeadPosition, pObject->m_nIDPosition).CenterPoint();
        					m_DisplayList.ActivateItem(*pNextDI, pt);
							pNextDI = NULL;
						}
						else	// goto next object which has a page template
						{
			        		pNextDI = m_DisplayList.GetNextItem(pNextDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			        		if (!pNextDI)
        						pNextDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
						}
        			}
				}
        	}
        }
        break;
    case VK_RETURN:	// Deactivate item and InplaceEdit
        {
        	CDisplayItem* pDI = m_DisplayList.GetActiveItem();
        	if (pDI)
        	{
        		m_DisplayList.DeactivateItem(*pDI);
        		m_DisplayList.InvalidateItem(pDI);
        	}
        }
        break;
    default: 
        break;
    }
}


void CPrintSheetView::UpdatePageID(CDisplayItem* pDI, CString strPageNum)
{
    if (pDI->IsNull())
        return;
    if (!pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
        return;

    CLayoutObject* pObject		 = (CLayoutObject*)pDI->m_pData;
    CPageTemplate* pPageTemplate = pObject->GetCurrentPageTemplate((CPrintSheet*)pDI->m_pAuxData);
    if (pPageTemplate)
    {
        if (_tcscmp(pPageTemplate->m_strPageID, strPageNum) != 0)
        {
			UndoSave();

        	pPageTemplate->m_strPageID = strPageNum;
			if (CImpManDoc::GetDoc())
			{
	        	CImpManDoc::GetDoc()->m_PageTemplateList.FindPrintSheetLocations();
		    	CImpManDoc::GetDoc()->SetModifiedFlag();
			}

        	theApp.UpdateView(RUNTIME_CLASS(CPageListView));
        	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));
        }
    }
}

BOOL g_bResizeHandleTracked = FALSE;

BOOL CPrintSheetView::OnSelectTrackerResizeHandle(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	pDI->m_nState = CDisplayItem::Normal;

	if (m_DisplayList.m_bIsInsideMultiSelect)
		return FALSE;

	CMyRectTracker tracker(FALSE);
	tracker.m_rect = pDI->m_AuxRect;

	int nHit = CRectTracker::hitBottomRight;
	switch ((int)pDI->m_pData)
	{
	case LEFT_MIDDLE:	nHit = CRectTracker::hitLeft;			break;
	case MIDDLE_BOTTOM:	nHit = CRectTracker::hitBottom;			break;
	case RIGHT_MIDDLE:	nHit = CRectTracker::hitRight;			break;
	case MIDDLE_TOP:	nHit = CRectTracker::hitTop;			break;
	case RIGHT_BOTTOM:	nHit = CRectTracker::hitBottomRight;	break;
	case LEFT_BOTTOM:	nHit = CRectTracker::hitBottomLeft;		break;
	case RIGHT_TOP:		nHit = CRectTracker::hitTopRight;		break;
	case LEFT_TOP:		nHit = CRectTracker::hitTopLeft;		break;
	}

	if (tracker.TrackHandle(nHit, this, point, NULL))
	{
		CClientDC cdc(this);
		CDC dc;
		dc.CreateCompatibleDC(&cdc);
		OnPrepareDC(&dc);
		CPoint ptVpOrg( -GetScrollPos(SB_HORZ) + STATUSBAR_THICKNESS(HORIZONTAL), -GetScrollPos(SB_VERT) + STATUSBAR_THICKNESS(VERTICAL));
		switch (GetActSight())
		{
			case FRONTSIDE : dc.SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
				 			 dc.SetViewportOrg(ptVpOrg);
    						 break;
			case BACKSIDE  : dc.SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
				 			 dc.SetViewportOrg(ptVpOrg);
    						 break;
			case BOTHSIDES : dc.SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
				 			 dc.SetViewportOrg(ptVpOrg);
    						 break;
			default		:	break;
		}

		dc.DPtoLP(&tracker.m_rect);
		tracker.m_rect.NormalizeRect();

		if (IsOpenDlgComponentImposer())
		{
			tracker.m_rect.left	  *= 10;
			tracker.m_rect.bottom *= 10;
			tracker.m_rect.right  *= 10;
			tracker.m_rect.top	  *= 10;
			g_bResizeHandleTracked = TRUE;
			GetDlgComponentImposer()->SelectPlaceArea(tracker.m_rect);
		}
		else
		{
			CPrintSheet*	pPrintSheet	  = GetActPrintSheet();
			CLayout*		pLayout		  = GetActLayout();
			CLayoutObject*	pLayoutObject = (CLayoutObject*)pDI->m_pAuxData;
			if ( ! pLayout || ! pLayoutObject)
				return FALSE;
			CFlatProduct&	rFlatProduct  = pLayoutObject->GetCurrentFlatProduct(pPrintSheet);
			if (rFlatProduct.IsNull())
				return FALSE;

			UndoSave();

			if (pLayout->GetNumPrintSheetsBasedOn() > 1)
			{
				pLayout = pPrintSheet->SplitLayout();
				if ( ! pLayout)
					return FALSE;
			}

			float fFrameLeft	= (float)tracker.m_rect.left	/ (float)WorldToLP(1);
			float fFrameBottom	= (float)tracker.m_rect.top		/ (float)WorldToLP(1);
			float fFrameRight	= (float)tracker.m_rect.right	/ (float)WorldToLP(1);
			float fFrameTop		= (float)tracker.m_rect.bottom	/ (float)WorldToLP(1);

			int	nFlatProductIndex = rFlatProduct.GetIndex();

			float fLeftBorder = 0.0f, fBottomBorder = 0.0f, fRightBorder = 0.0f, fTopBorder = 0.0f; 
			switch (pLayoutObject->m_nHeadPosition)
			{
			case TOP:		fLeftBorder	  = rFlatProduct.m_fLeftTrim;
							fBottomBorder = rFlatProduct.m_fBottomTrim;
							fRightBorder  = rFlatProduct.m_fRightTrim;
							fTopBorder	  = rFlatProduct.m_fTopTrim;
							break;			
			case RIGHT:		fLeftBorder	  = rFlatProduct.m_fBottomTrim;
							fBottomBorder = rFlatProduct.m_fRightTrim;
							fRightBorder  = rFlatProduct.m_fTopTrim;
							fTopBorder	  = rFlatProduct.m_fLeftTrim;
							break;			
			case BOTTOM:	fLeftBorder	  = rFlatProduct.m_fRightTrim;
							fBottomBorder = rFlatProduct.m_fTopTrim;
							fRightBorder  = rFlatProduct.m_fLeftTrim;
							fTopBorder	  = rFlatProduct.m_fBottomTrim;
							break;			
			case LEFT:		fLeftBorder	  = rFlatProduct.m_fTopTrim;
							fBottomBorder = rFlatProduct.m_fLeftTrim;
							fRightBorder  = rFlatProduct.m_fBottomTrim;
							fTopBorder	  = rFlatProduct.m_fRightTrim;
							break;
			}
			float fWidth  = ( (pLayoutObject->m_nHeadPosition == TOP) ||  (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? rFlatProduct.m_fTrimSizeWidth  : rFlatProduct.m_fTrimSizeHeight;
			float fHeight = ( (pLayoutObject->m_nHeadPosition == TOP) ||  (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? rFlatProduct.m_fTrimSizeHeight : rFlatProduct.m_fTrimSizeWidth;

			float fNetWidth    = fWidth;
			float fNetHeight   = fHeight;

			float fGrossWidth, fGrossHeight;
			float fBoxLeft, fBoxBottom, fBoxRight, fBoxTop;
			pLayoutObject->GetCurrentGrossBox(pPrintSheet, fBoxLeft, fBoxBottom, fBoxRight, fBoxTop);
			fGrossWidth  = fBoxRight - fBoxLeft;
			fGrossHeight = fBoxTop   - fBoxBottom;
			CRect rcGrossObject;
			rcGrossObject.left	 = CPrintSheetView::WorldToLP(fBoxLeft);
			rcGrossObject.bottom = CPrintSheetView::WorldToLP(fBoxBottom);
			rcGrossObject.right  = CPrintSheetView::WorldToLP(fBoxRight);
			rcGrossObject.top	 = CPrintSheetView::WorldToLP(fBoxTop);

			dc.LPtoDP(rcGrossObject);	
			InvalidateRect(rcGrossObject);

			int nGrowX = RIGHT;
			int nGrowY = TOP;
			switch (nHit)
			{
			case CRectTracker::hitTopLeft:		
			case CRectTracker::hitLeft:				nGrowX = LEFT;							break;
			case CRectTracker::hitBottomLeft:		nGrowX = LEFT;		nGrowY = BOTTOM;	break;
			case CRectTracker::hitTop:							
			case CRectTracker::hitTopRight:	
			case CRectTracker::hitRight:													break;			
			case CRectTracker::hitBottomRight:		
			case CRectTracker::hitBottom:			nGrowY = BOTTOM;						break;
			}

			float fXStart = pLayoutObject->GetLeft();
			float fYStart = pLayoutObject->GetBottom();
			int nNumAdded = 0;
			int i = 0, j = 0;
			while (1)
			{
				float fObjectPosY = (nGrowY == TOP) ? (fYStart + i * fGrossHeight) : (fYStart - i * fGrossHeight);
				if (nGrowY == TOP)
				{
					if ( (fObjectPosY + fNetHeight) > fFrameTop)
						break;
				}
				else
					if (fObjectPosY < fFrameBottom)
						break;

				j = 0;
				while (1)
				{
					float fObjectPosX = (nGrowX == RIGHT) ? (fXStart + j * fGrossWidth) : (fXStart - j * fGrossWidth);
					if (nGrowX == RIGHT)
					{
						if ( (fObjectPosX + fNetWidth) > fFrameRight)
							break;
					}
					else
						if (fObjectPosX < fFrameLeft)
							break;

					CLayoutObject layoutObject;
					layoutObject.CreateFlatProduct(fObjectPosX, fObjectPosY, fNetWidth, fNetHeight, pLayoutObject->m_nHeadPosition, -1);
					if ( ! pLayout->m_FrontSide.FindObjectSamePosition(layoutObject))
					{
						int nComponentRefIndex = pPrintSheet->MakeFlatProductRefIndex(nFlatProductIndex);
						layoutObject.m_nComponentRefIndex = nComponentRefIndex;

						pLayout->m_FrontSide.m_ObjectList.AddTail(layoutObject);
						CLayoutObject* pFrontLayoutObject = &pLayout->m_FrontSide.m_ObjectList.GetTail();

						layoutObject.CreateFlatProduct(fObjectPosX, fObjectPosY, fNetWidth, fNetHeight, pLayoutObject->m_nHeadPosition, nComponentRefIndex);
						pLayout->m_BackSide.m_ObjectList.AddTail(layoutObject);
						CLayoutObject* pBackLayoutObject = &pLayout->m_BackSide.m_ObjectList.GetTail();

						pBackLayoutObject->AlignToCounterPart(pFrontLayoutObject);

						float fGrossLeft = 0.0f, fGrossBottom = 0.0f, fGrossRight = 0.0f, fGrossTop = 0.0f; 
						pFrontLayoutObject->GetCurrentGrossBox(pPrintSheet, fGrossLeft, fGrossBottom, fGrossRight, fGrossTop);

						rcGrossObject.left	 = CPrintSheetView::WorldToLP(fGrossLeft);
						rcGrossObject.bottom = CPrintSheetView::WorldToLP(fGrossBottom);
						rcGrossObject.right  = CPrintSheetView::WorldToLP(fGrossRight);
						rcGrossObject.top	 = CPrintSheetView::WorldToLP(fGrossTop);

						dc.LPtoDP(rcGrossObject);	 
						InvalidateRect(rcGrossObject);
						nNumAdded++;
					}
					j++;
				}
				i++;
			}

			if (nNumAdded > 0)
			{
				pPrintSheet->CalcTotalQuantity(TRUE);

				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if (pDoc)
				{
					pDoc->m_PageTemplateList.FindPrintSheetLocations();
					pDoc->m_MarkTemplateList.FindPrintSheetLocations();

					pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
					pDoc->m_PrintSheetList.ReorganizeColorInfos();

					pDoc->SetModifiedFlag();
				}
				m_DisplayList.Invalidate();
				UpdateWindow();

				theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
				theApp.UpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));
			}
		}

		dc.DeleteDC();
	}

	return TRUE;
}

BOOL CPrintSheetView::OnMouseMoveTrackerResizeHandle(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetView::OnSelectLayoutObject(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if ( ! pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return FALSE;

	if ( ! MarksViewActive())
		SetLayoutObjectSelected(TRUE);

	BOOL bShiftDown	  = (GetKeyState(VK_SHIFT)   & 0x8000) ? TRUE : FALSE;
	BOOL bControlDown = (GetKeyState(VK_CONTROL) & 0x8000) ? TRUE : FALSE;
	if ( bControlDown && bShiftDown )	// control AND shift -> select block
	{
		CLayoutObject* pInitialObject = ((CLayoutObject*)pDI->m_pData);
		if (pInitialObject->IsFlatProduct())
		{
			CStepAndRepeatMatrix stepAndRepeatMatrix; stepAndRepeatMatrix.Create(pInitialObject, pDI->m_BoundingRect.CenterPoint(), GetActPrintSheet(), CStepAndRepeatMatrix::TakePages, FALSE);
			for (int nRow = 0; nRow < stepAndRepeatMatrix.GetSize(); nRow++)
			{
				CStepAndRepeatRow row = stepAndRepeatMatrix[nRow];
				for (int nCol = 0; nCol < row.GetSize(); nCol++)
				{
					CLayoutObject* pObject = (CLayoutObject*)row[nCol];
					pDI	= m_DisplayList.GetItemFromData((void*)pObject, (void*)GetActPrintSheet(), DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
					if (pDI)
					{
						pDI->m_nState = CDisplayItem::Selected;
						m_DisplayList.InvalidateItem(pDI, TRUE);
					}
				}
			}
		}
	}

	if ( ! m_DisplayList.m_bIsInsideMultiSelect)
	{
		if ( ! IsOpenDlgComponentImposer())
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			if ( ! pDoc)
				return FALSE;
			CPrintComponentsView* pView = ((CNewPrintSheetFrame*)pFrame)->GetPrintComponentsView();
			if ( ! pView)
				return FALSE;

			if (pDI->m_pData)
			{
				CLayoutObject* pLayoutObject = ((CLayoutObject*)pDI->m_pData);
				if (pLayoutObject->IsFlatProduct())
				{
					CFlatProduct&	rFlatProduct	  = pLayoutObject->GetCurrentFlatProduct(GetActPrintSheet());
					int				nFlatProductIndex = rFlatProduct.GetIndex();
					CPrintComponent component		  = pDoc->m_printComponentsList.FindFlatComponent(nFlatProductIndex);
					if (component != pView->GetActComponent())
					{
						pView->SetActComponent(component);
						theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
						theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
					}
				}
				else
				{
					CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
					if (pFrame)
						if ( ! pFrame->m_bShowFoldSheet)
							pFrame->OnFoldsheetCheck();

					CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
					if (pPrintSheet)
					{
						int nSide = pLayoutObject->GetLayoutSideIndex();
						if ( (pLayoutObject->m_nComponentRefIndex >= 0) && (pLayoutObject->m_nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize()) )
						{
							CFoldSheetLayerRef& rLayerRef = pPrintSheet->m_FoldSheetLayerRefs[pLayoutObject->m_nComponentRefIndex];
							CPrintComponent		component = pDoc->m_printComponentsList.FindFoldSheetLayerComponent(rLayerRef.m_nFoldSheetIndex, rLayerRef.m_nFoldSheetLayerIndex);
							if (component != pView->GetActComponent())
							{
								pView->SetActComponent(component);
								theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
								theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
							}

							CFoldSheetLayer* pFoldSheetLayer = pPrintSheet->GetFoldSheetLayer(pLayoutObject->m_nComponentRefIndex);
							if (pFoldSheetLayer)
							{
								if (pFoldSheetLayer->m_bPagesLocked)
								{
									pDI->m_nState = CDisplayItem::Normal;
									CDisplayItem* pDIFoldSheetLayer = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
									while (pDIFoldSheetLayer)
									{
										BOOL bFound = FALSE;
										if ((int)pDIFoldSheetLayer->m_pData == pLayoutObject->m_nComponentRefIndex)
											if (pDIFoldSheetLayer->m_pAuxData == pPrintSheet)
												if ( (nSide == FRONTSIDE) && (pDIFoldSheetLayer->m_AuxRect.Height() > 2) )	// Height() > 2 means frontside
													bFound = TRUE;
												else
													if ( (nSide == BACKSIDE) && (pDIFoldSheetLayer->m_AuxRect.Height() <= 2) )	// Height() <= 2 means backside
														bFound = TRUE;

										if (bFound)
										{
											pDIFoldSheetLayer->m_nState = CDisplayItem::Selected;
											SetFoldSheetSelected(TRUE);
										}
										pDIFoldSheetLayer = m_DisplayList.GetNextItem(pDIFoldSheetLayer, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CPrintSheetView::OnDeselectLayoutObject(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been deselected
{
	int nWidth = 2;
	CRect rcInvalid = pDI->m_AuxRect; rcInvalid.bottom = rcInvalid.top + nWidth;
	InvalidateRect(rcInvalid, FALSE);
	rcInvalid = pDI->m_AuxRect; rcInvalid.left = rcInvalid.right - nWidth;
	InvalidateRect(rcInvalid, FALSE);
	rcInvalid = pDI->m_AuxRect; rcInvalid.top = rcInvalid.bottom - nWidth;
	InvalidateRect(rcInvalid, FALSE);
	rcInvalid = pDI->m_AuxRect; rcInvalid.right = rcInvalid.left + nWidth;
	InvalidateRect(rcInvalid, FALSE);

	return TRUE;
}
//
//BOOL CPrintSheetView::OnDrawStateLayoutObject(CDisplayItem* pDI, LPARAM lParam, LPARAM lParam2)
//{
//	CDC* pDC = (CDC*)lParam2;//GetDC(); 
//	ClipStatusBars(pDC);
//
//	CRect rcFrame = pDI->m_BoundingRect;
//
//	switch (lParam)
//	{
//	case 0:		// draw selected state
//		{
//			COLORREF crHighlight = pDI->m_crHighlightColor;//OCKER;//LIGHTRED;
//			FillTransparent(pDC, rcFrame, crHighlight, TRUE, 80, TRUE);
//
//			//CPen pen, pen2;//, pen3;
//			//if (crHighlight == RGB(255,0,0))
//			//{
//			//	pen2.CreatePen(PS_SOLID, 1, RGB(200, 100, 100));
//			//	pen.CreatePen (PS_SOLID, 1, RGB(200, 100, 100));
//			//	//pen3.CreatePen(PS_SOLID, 1, RGB(200, 100, 100));
//			//}
//			//else
//			//if (crHighlight == RGB(0,0,255))
//			//{
//			//	pen.CreatePen (PS_SOLID, 1, RGB(100,190,250));
//			//	pen2.CreatePen(PS_SOLID, 1, RGB(100,100,200));
//			//	//pen3.CreatePen(PS_SOLID, 1, RGB(200,200,255));
//			//}
//			//else
//			//{
//			//	pen.CreatePen (PS_SOLID, 1, crHighlight);
//			//	pen2.CreatePen(PS_SOLID, 1, crHighlight);
//			//	//pen3.CreatePen(PS_SOLID, 1, crHighlight);
//			//}
//			//CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
//			//CPen* pOldPen = pDC->SelectObject(&pen);
//			////rect.right += 2; rect.bottom--;
//			//pDC->Rectangle(rcFrame);
//			//pDC->SelectObject(&pen2);
//			//rcFrame.DeflateRect(1,1);
//			//pDC->Rectangle(rcFrame);
//			////pDC->SelectObject(&pen3);
//			////rect.DeflateRect(1,1);
//			////pDC->Rectangle(rect);
//			//pDC->SelectObject(pOldPen);
//			//pDC->SelectObject(pOldBrush);
//			//pen.DeleteObject();
//			//pen2.DeleteObject();
//		}
//		break;
//	case 1:	// prepare for dragging
//		{
//			CDC* pDC = (CDC*)lParam2;
//			rcFrame.OffsetRect(-rcFrame.TopLeft());		// draw to memory context -> begin at 0,0
//			pDC->FillSolidRect(rcFrame, GREEN);
//		}
//		break;
//	}
//
//	return TRUE;
//}

BOOL CPrintSheetView::OnDrawStateGroupTracker(CDisplayItem* pDI, LPARAM lParam, LPARAM lParam2)
{
	CDC* pDC = GetDC(); 
	ClipStatusBars(pDC);

	CRect rcFrame = pDI->m_BoundingRect;

	switch (lParam)
	{
	case 0:		// draw selected state
		{
			COLORREF crHighlight = OCKER;//LIGHTRED;
			FillTransparent(pDC, rcFrame, crHighlight, TRUE, 80, TRUE);
		}
		break;
	case 1:	// prepare for dragging
		{
			CDC* pDC = (CDC*)lParam2;
			pDC->SaveDC();

			OnPrepareDC(pDC);
			CPoint ptVpOrg( -GetScrollPos(SB_HORZ) + STATUSBAR_THICKNESS(HORIZONTAL), -GetScrollPos(SB_VERT) + STATUSBAR_THICKNESS(VERTICAL));
			ptVpOrg = ptVpOrg - rcFrame.TopLeft();
			switch (GetActSight())
			{
				case FRONTSIDE : pDC->SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 			 pDC->SetViewportOrg(ptVpOrg);
        						 break;
				case BACKSIDE  : pDC->SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 			 pDC->SetViewportOrg(ptVpOrg);
        						 break;
				case BOTHSIDES : pDC->SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 			 pDC->SetViewportOrg(ptVpOrg);
        						 break;
				default		:	break;
			}

			rcFrame.OffsetRect(-rcFrame.TopLeft());
			pDC->DPtoLP(rcFrame);
			pDC->FillSolidRect(rcFrame, WHITE);

			CPrintSheet* pPrintSheet = GetActPrintSheet();
			CLayout*	 pLayout	 = GetActLayout();

			CDisplayItem* pDIGroupMember = m_DisplayList.GetFirstGroupMember(pDI->m_nGroup);
			while (pDIGroupMember)
			{
				if (pDIGroupMember->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
				{
					CLayoutObject* pLayoutObject = (CLayoutObject*)pDIGroupMember->m_pData;
					CRect objectRect;
					objectRect.left		 = WorldToLP(pLayoutObject->GetLeft());
					objectRect.top		 = WorldToLP(pLayoutObject->GetTop());
					objectRect.right	 = WorldToLP(pLayoutObject->GetRight());
					objectRect.bottom	 = WorldToLP(pLayoutObject->GetBottom());
					DrawPage(pDC, objectRect, *pLayoutObject, NULL, pPrintSheet);
					//pLayoutObject->Draw(pDC);
				}
				else
				if (pDIGroupMember->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
				{
					int			 nComponentRefIndex = (int)pDIGroupMember->m_pData;
					CFoldSheet*  pFoldSheet			= (pPrintSheet) ? pPrintSheet->GetFoldSheet(nComponentRefIndex) : NULL;
					CLayoutSide* pLayoutSide		= (pLayout) ? ((pDI->m_AuxRect.Height() > 1) ? &pLayout->m_FrontSide : &pLayout->m_BackSide) : NULL;
					if (pLayoutSide)
						DrawFoldSheetRect(pDC, pLayoutSide->m_nLayoutSide, pPrintSheet, nComponentRefIndex, pFoldSheet);
						//pLayoutSide->DrawFoldSheet(pDC, nComponentRefIndex, _T(""), pFoldSheet, pLayout, pPrintSheet);
				}
				pDIGroupMember = m_DisplayList.GetNextGroupMember(pDIGroupMember, pDI->m_nGroup);
			}

			pDC->RestoreDC(-1);
		}
		break;
	}

	return TRUE;
}

BOOL CPrintSheetView::OnSelectSheet(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

BOOL CPrintSheetView::OnSelectBoundingBoxFrame(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

BOOL CPrintSheetView::OnSelectPlate(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// plate has been selected
{
	pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

BOOL CPrintSheetView::OnSelectFoldSheet(CDisplayItem* pDI, CPoint point, LPARAM)	// foldsheet has been selected
{
	SetFoldSheetRotateActive(TRUE);
    SetFoldSheetTurnActive(TRUE);
    SetFoldSheetTumbleActive(TRUE);
    SetFoldSheetSelected(TRUE);

	if (MarksViewActive())
		pDI->m_nState = CDisplayItem::Normal;	// in marks view foldsheets needs not to be selectable

	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if ( ! pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return FALSE;
	if (((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView())
	{
		CFoldSheet* pFoldSheet = ((CPrintSheet*)pDI->m_pAuxData)->GetFoldSheet((int)pDI->m_pData);
		((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView()->SelectComponentByFoldSheet(pFoldSheet);
		((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView()->m_DisplayList.InvalidateItem();
		((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView()->UpdateWindow();
	}

	CPrintComponentsView* pView = ((CNewPrintSheetFrame*)pFrame)->GetPrintComponentsView();
	if ( ! pView)
		return FALSE;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if ( ! m_DisplayList.m_bIsInsideMultiSelect)
	{
		if ( ! IsOpenDlgComponentImposer())
		{
			int			 nComponentRefIndex = (int)pDI->m_pData;
			CPrintSheet* pPrintSheet		= (CPrintSheet*)pDI->m_pAuxData;
			if (pPrintSheet)
			{
				int nFoldSheetLayer		 = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;
				int nFoldSheetLayerIndex = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex;
				CPrintComponent component = pDoc->m_printComponentsList.FindFoldSheetLayerComponent(nFoldSheetLayer, nFoldSheetLayerIndex);
				if (component != pView->GetActComponent())
				{
					pView->SetActComponent(component);
					theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
					theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
				}
			}
		}
	}

	return TRUE;
}

BOOL CPrintSheetView::OnDeselectFoldSheet(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)		// foldsheet has been deactivated
{
    SetFoldSheetRotateActive(FALSE);
    SetFoldSheetTurnActive(FALSE);
    SetFoldSheetTumbleActive(FALSE);
    SetFoldSheetSelected(FALSE);

	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if ( ! pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return FALSE;
	if (((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView())
	{
		((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView()->m_nLastSelectedComponentIndex = -1;
		((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView()->m_DisplayList.DeselectAll();
		((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView()->m_DisplayList.InvalidateItem();
		((CNewPrintSheetFrame*)pFrame)->GetPrintSheetPlanningView()->UpdateWindow();
	}
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));

	return TRUE;
}

BOOL CPrintSheetView::OnMouseMoveFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 

				//CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
				//if (pPrintSheet) 
				//{
				//	CFoldSheetLayer* pFoldSheetLayer = pPrintSheet->GetFoldSheetLayer((int)pDI->m_pData);
				//	if (pFoldSheetLayer)
				//	{
				//		CImage img;
				//		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE((pFoldSheetLayer->m_bPagesLocked) ? IDB_LOCK_OPEN : IDB_LOCK_CLOSED));
				//		CRect rcImg = pDI->m_BoundingRect; rcImg.left = rcImg.right - 30; rcImg.top += 5; rcImg.bottom = rcImg.top + 30;
				//		img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
				//	}
				//}

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetView::OnActivateFoldSheet(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	// foldsheet has been activated
{
	return FALSE;
}

BOOL CPrintSheetView::OnDeactivateFoldSheet(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)		// foldsheet has been deactivated
{
    SetFoldSheetRotateActive(FALSE);
    SetFoldSheetTurnActive(FALSE);
    SetFoldSheetTumbleActive(FALSE);

	return TRUE;
}

BOOL CPrintSheetView::OnSelectPlateTab(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// plate tab has been selected
{
    CPrintSheetTreeView* pView		   = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
    CPlate*				 pTopmostPlate = ((CPrintSheet*)pDI->m_pAuxData)->GetTopmostPlate(((CPlate*)pDI->m_pData)->GetSheetSide()); 
    if (pTopmostPlate != (CPlate*)pDI->m_pData)
    {
        if (GetKeyState(VK_CONTROL) < 0) // Ctrl key pressed
    	{
        	if (pView)
        		pView->EnablePlateTreeItem((CPrintSheet*)pDI->m_pAuxData, (CPlate*)pDI->m_pData, FALSE);

        	return FALSE;
    	}

        pDI->m_nState = CDisplayItem::Normal;

        ChangeTopmostPlate(pDI);

        // If in BOTHSIDES or ALLSIDES view don't select tree item (don't switch to single plate)
		if (CImpManDoc::GetDoc())
			if ((GetActSight() == FRONTSIDE) || (GetActSight() == BACKSIDE))
			{
        		if (pView)
        			pView->SelectPlateTreeItem((CPrintSheet*)pDI->m_pAuxData, (CPlate*)pDI->m_pData);
			}
    }
    else
    {	// If topmost plate is selected - switch to this single plate
        pDI->m_nState = CDisplayItem::Normal;

        if (pView)
        	pView->SelectPlateTreeItem((CPrintSheet*)pDI->m_pAuxData, pTopmostPlate);
    }

	return TRUE;
}

BOOL CPrintSheetView::OnDeselectPlateTab(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// plate tab has been deselected
{
    CPrintSheetTreeView* pView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
    if (GetKeyState(VK_CONTROL) < 0) // Ctrl key pressed
    {
       	if (pView)
       		pView->EnablePlateTreeItem((CPrintSheet*)pDI->m_pAuxData, (CPlate*)pDI->m_pData);

		return FALSE;
    }

	return TRUE;
}

BOOL CPrintSheetView::OnActivatePlateTab(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// plate tab has been activated
{
    CPrintSheetTreeView* pView		= (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
    CDisplayItem*		 pPrevTabDI = ChangeTopmostPlate(pDI);

    pDI->m_nState = CDisplayItem::Normal;
    if (pView)
       	pView->EnablePlateTreeItem((CPrintSheet*)pDI->m_pAuxData, (CPlate*)pDI->m_pData);

    if (pPrevTabDI)
    {
        pPrevTabDI->m_nState = CDisplayItem::Selected;
       	if (pView)
       		pView->EnablePlateTreeItem((CPrintSheet*)pPrevTabDI->m_pAuxData, (CPlate*)pPrevTabDI->m_pData, FALSE);
    }

	return TRUE;
}

CDisplayItem* CPrintSheetView::ChangeTopmostPlate(CDisplayItem* pDI, BOOL /*bSelectTreeItem*/)
{
    int	nSheetSide = ((CPlate*)pDI->m_pData)->GetSheetSide();

    // Get previous topmost plate
    CPlate*		  pPrevTopmostPlate = ((CPrintSheet*)pDI->m_pAuxData)->GetTopmostPlate(nSheetSide); 
    CDisplayItem* pPrevTopmostTabDI	= NULL;
    if (pPrevTopmostPlate != (CPlate*)pDI->m_pData)
    {
        if (pPrevTopmostTabDI = m_DisplayList.GetItemFromData((void*)pPrevTopmostPlate, (void*)pDI->m_pAuxData,
        													  DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab)))
        	InvalidateRect(pPrevTopmostTabDI->m_BoundingRect);
    }

    ((CPlate*)pDI->m_pData)->SetTopmostPlate();

    CLayoutSide*  pLayoutSide = ((CPlate*)pDI->m_pData)->GetLayoutSide();
    CDisplayItem* pPlateDI	  = m_DisplayList.GetItemFromData((void*)pLayoutSide, (void*)pDI->m_pAuxData,
        			 										  DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
    CDisplayItem* pTagDI	  = m_DisplayList.GetItemFromData((void*)pPrevTopmostPlate, (void*)pDI->m_pAuxData,
        			 										  DISPLAY_ITEM_TYPEID(CPrintSheetView, GlobalMaskTag));
    if (pTagDI)
    	InvalidateRect(pTagDI->m_AuxRect);			// Invalidate global mask tag
    if (pPlateDI)
	{
		CRect rect = pPlateDI->m_BoundingRect;
		rect.bottom += 20;
        InvalidateRect(rect);	// Invalidate plate bounding rect and spot color tabs 
	}

	CRect rect;
	rect.UnionRect(pPlateDI->m_BoundingRect, pDI->m_BoundingRect);
    InvalidateRect(rect);			// Invalidate plate tabs

    return pPrevTopmostTabDI;
}

BOOL CPrintSheetView::OnSelectExposure(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
    BOOL bFound = FALSE;
    pDI			= m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));

    while (pDI && !bFound)	// Look for a selected exposure with locked bleed(s).
    {						// If so, enable release button
        if (((CExposure*)(pDI->m_pData))->IsBleedLocked(LEFT))
        	bFound = TRUE;
        else
        	if (((CExposure*)(pDI->m_pData))->IsBleedLocked(TOP))
        		bFound = TRUE;
        	else
        		if (((CExposure*)(pDI->m_pData))->IsBleedLocked(RIGHT))
        			bFound = TRUE;
        		else
        			if (((CExposure*)(pDI->m_pData))->IsBleedLocked(BOTTOM))
        				bFound = TRUE;

        pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
    }

    if (bFound)
        SetEnableReleaseBleedButton(TRUE);

	return TRUE;
}

BOOL CPrintSheetView::OnDeselectExposure(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	
{
    if (!m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure)))
        SetEnableReleaseBleedButton(FALSE);

	return TRUE;
}

BOOL CPrintSheetView::OnActivateExposure(CDisplayItem* pDI, CPoint point, LPARAM)	// overbleed has been activated
{
    CString bleedValueString;
    int		nSide;
    CRect	bleedValueRect = CalcBleedValueRect((CExposure*)pDI->m_pData, pDI->m_AuxRect, point, nSide, bleedValueString);

	bleedValueRect.InflateRect(5, 1);
    m_InplaceEditBleed.Activate(bleedValueRect, bleedValueString, CInplaceEdit::GrowToRight, (LPARAM)nSide, *pDI);

	m_DisplayList.m_bDeselectAll = FALSE;

	return TRUE;
}

BOOL CPrintSheetView::OnDeactivateExposure(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	// overbleed has been deactivated
{
	if (!m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure)))
		SetEnableReleaseBleedButton(FALSE);

	if (GetFocus() == &m_InplaceEditBleed)	// overbleed has been edited
		m_InplaceEditBleed.Deactivate();

	return TRUE;
}

void CPrintSheetView::OnKillFocusInplaceEditBleed() 		// Message from InplaceEdit
{
	m_DisplayList.DeactivateItem(m_InplaceEditBleed.m_di);
}

void CPrintSheetView::OnCharInplaceEditBleed(UINT nChar) // Message from InplaceEdit
{
    switch (nChar)
    {
    case VK_UP:	
    case VK_DOWN:
        {
        	CDisplayItem di = m_InplaceEditBleed.m_di;
        	if (!di.IsNull())
        	{
				SetFocus();
				UpdateBleed();
        		int	nSide;
        		switch ((int)m_InplaceEditBleed.m_lParam)
        		{
        		case TOP:	 nSide = (nChar == VK_DOWN) ? RIGHT  : LEFT;	break;
        		case RIGHT:	 nSide = (nChar == VK_DOWN) ? BOTTOM : TOP;		break;
        		case BOTTOM: nSide = (nChar == VK_DOWN) ? LEFT   : RIGHT;	break;
        		case LEFT:	 nSide = (nChar == VK_DOWN) ? TOP	 : BOTTOM;	break;
				default:	 nSide = LEFT;									break;
        		}

        		CString bleedValueString;
        		CRect	bleedValueRect = CalcBleedValueRect((CExposure*)di.m_pData, di.m_AuxRect, 
															nSide, bleedValueString, &m_DisplayList);
				bleedValueRect.InflateRect(5, 1);
        		m_InplaceEditBleed.Move(bleedValueRect, bleedValueString, (LPARAM)nSide);

				CSize s(di.m_BoundingRect.Width()/4, di.m_BoundingRect.Height()/4);
        		switch (nSide)
        		{
        		case TOP:	 m_DisplayList.ActivateItem(di, di.m_BoundingRect.CenterPoint() + CSize(    0, -s.cy));	break;
        		case RIGHT:	 m_DisplayList.ActivateItem(di, di.m_BoundingRect.CenterPoint() + CSize( s.cx,     0));	break;
        		case BOTTOM: m_DisplayList.ActivateItem(di, di.m_BoundingRect.CenterPoint() + CSize(    0,  s.cy));	break;
        		case LEFT:	 m_DisplayList.ActivateItem(di, di.m_BoundingRect.CenterPoint() + CSize(-s.cx,     0));	break;
        		}
        	}
        }
        break;
    case VK_TAB:
        {
        	CDisplayItem di = m_InplaceEditBleed.m_di;
        	if (!di.IsNull())
        	{
				SetFocus();
				UpdateBleed();
        		CDisplayItem* pNextDI = m_DisplayList.GetNextItem(&di, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
        		if (!pNextDI)
        			pNextDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
        		if (pNextDI)
        			if (!pNextDI->IsEqual(&di))
        				m_DisplayList.ActivateItem(*pNextDI, pNextDI->m_BoundingRect.CenterPoint() + 
        													 CSize(0, -pNextDI->m_BoundingRect.Height()/4)); 
        	}
        }
        break;
    case VK_RETURN:	// Deactivate item and InplaceEdit
        {
        	CDisplayItem di = m_InplaceEditBleed.m_di;
        	if (!di.IsNull())
        	{
				SetFocus();
				UpdateBleed();
        	}
        }
        break;
    default: 
        break;
    }
}


void CPrintSheetView::UpdateBleed()
{
   	CDisplayItem di = m_InplaceEditBleed.m_di;
    if (!di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure)))
        return;

	UndoSave();

	float fValue	   = m_InplaceEditBleed.GetFloat();
	int   nHitPosition = (int)m_InplaceEditBleed.m_lParam;

	if (CImpManDoc::GetDoc())
		CImpManDoc::GetDoc()->m_PrintSheetList.SetLocalMaskAllExposures((CExposure*)di.m_pData, nHitPosition, fValue);

	m_DisplayList.InvalidateItem(&di);
	m_DisplayList.DeactivateItem(di);
	InvalidateRect(di.m_BoundingRect);

    CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	while (pDI)
	{
		if (CImpManDoc::GetDoc())
			CImpManDoc::GetDoc()->m_PrintSheetList.SetLocalMaskAllExposures((CExposure*)pDI->m_pData, nHitPosition, fValue);

		m_DisplayList.DeactivateItem(*pDI);
		InvalidateRect(pDI->m_BoundingRect);

       	pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	}

	if (CImpManDoc::GetDoc())
		CImpManDoc::GetDoc()->SetModifiedFlag();
	UpdateWindow();

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetMaskSettingsView));
}


BOOL CPrintSheetView::OnSelectGlobalMaskTag(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// global mask tag has been selected
{
	pDI->m_nState = CDisplayItem::Normal;
    CPlate* pPlate = (CPlate*)pDI->m_pData;
    if (!pPlate)
        return FALSE;

    CString globalMaskString;
    TruncateNumber(globalMaskString, pPlate->m_fOverbleedOutside);
    m_InplaceEditGlobalMask.Activate(pDI->m_AuxRect, globalMaskString, CInplaceEdit::GrowToRight, 0, *pDI);

	return TRUE;
}

void CPrintSheetView::OnKillFocusInplaceEditGlobalMask() 		// Message from InplaceEdit
{
}

void CPrintSheetView::UpdateGlobalMask()
{
	CPlate* pPlate = NULL;
	if (!m_InplaceEditGlobalMask.m_di.IsNull())
		pPlate = (CPlate*)m_InplaceEditGlobalMask.m_di.m_pData;
    if (!pPlate)
        return;

	UndoSave();

    float fOldValue = pPlate->m_fOverbleedOutside;
	if (CImpManDoc::GetDoc())
		CImpManDoc::GetDoc()->SetModifiedFlag();

	if (CImpManDoc::GetDoc())
		CImpManDoc::GetDoc()->m_PrintSheetList.SetGlobalMaskAllPlates(m_InplaceEditGlobalMask.GetFloat());

    if (fOldValue == pPlate->m_fOverbleedOutside)	// value not changed -> don't update view
    	return;

    Invalidate();
    UpdateWindow();
}

void CPrintSheetView::OnCharInplaceEditGlobalMask(UINT nChar) // Message from InplaceEdit
{
    if (nChar == VK_RETURN)	// Deactivate item and InplaceEdit
    {
		SetFocus();
		UpdateGlobalMask();
    }
}

BOOL CPrintSheetView::OnSelectShinglingValueX(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// shingling value has been selected
{
	pDI->m_nState					= CDisplayItem::Normal;
    CPageTemplate* pPageTemplate	= (CPageTemplate*)pDI->m_pData;
	float*		   pfShinglingValue = (float*)pDI->m_pAuxData;
    if ( ! pPageTemplate || ! pfShinglingValue)
        return FALSE;

	if (pPageTemplate->m_bShinglingOffsetLocked)
	{
		CRect rectButton(pDI->m_BoundingRect.left - 25, pDI->m_BoundingRect.top - 2, pDI->m_BoundingRect.left - 5, pDI->m_BoundingRect.bottom + 2);
		m_InplaceButtonShingling.Activate(rectButton, "", 0, *pDI);
	}

    CString strShinglingValue;
    TruncateNumber(strShinglingValue, *pfShinglingValue);
   	CRect rcEdit = pDI->m_BoundingRect;
	rcEdit.InflateRect(4, 2);
	m_InplaceEditShingling.Activate(rcEdit, strShinglingValue, CInplaceEdit::GrowToRight, 0, *pDI);

	return TRUE;
}

BOOL CPrintSheetView::OnSelectShinglingValueY(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// shingling value has been selected
{
	pDI->m_nState					= CDisplayItem::Normal;
    CPageTemplate* pPageTemplate	= (CPageTemplate*)pDI->m_pData;
	float*		   pfShinglingValue = (float*)pDI->m_pAuxData;
    if ( ! pPageTemplate || ! pfShinglingValue)
        return FALSE;

	if (pPageTemplate->m_bShinglingOffsetLocked)
	{
		CRect rectButton(pDI->m_BoundingRect.left - 25, pDI->m_BoundingRect.top - 2, pDI->m_BoundingRect.left - 5, pDI->m_BoundingRect.bottom + 2);
		m_InplaceButtonShingling.Activate(rectButton, "", 0, *pDI);
	}

    CString strShinglingValue;
    TruncateNumber(strShinglingValue, *pfShinglingValue);
   	CRect rcEdit = pDI->m_BoundingRect;
	rcEdit.InflateRect(4, 2);
	m_InplaceEditShingling.Activate(rcEdit, strShinglingValue, CInplaceEdit::GrowToRight, 0, *pDI);

	return TRUE;
}

void CPrintSheetView::UpdateShinglingValue()
{
	CPageTemplate* pPageTemplate	= NULL;
	float*		   pfShinglingValue = (float*)m_InplaceEditShingling.m_di.m_pAuxData;

	if (!m_InplaceEditShingling.m_di.IsNull())
		pPageTemplate = (CPageTemplate*)m_InplaceEditShingling.m_di.m_pData;
    if ( ! pPageTemplate || ! pfShinglingValue)
        return;

	UndoSave();

    float fOldValue = *pfShinglingValue;
    CImpManDoc::GetDoc()->SetModifiedFlag();

    *pfShinglingValue = m_InplaceEditShingling.GetFloat();
	pPageTemplate->m_bShinglingOffsetLocked = TRUE;

    if (fOldValue == *pfShinglingValue)	// value not changed -> don't update view
    	return;

    InvalidateRect(m_InplaceEditShingling.m_di.m_AuxRect);
	m_DisplayList.Invalidate();
    UpdateWindow();
}

void CPrintSheetView::OnCharInplaceEditShingling(UINT nChar)	// Message from InplaceEdit
{
    if (nChar == VK_RETURN)	
    {
		SetFocus();
		UpdateShinglingValue();
	}
}

void CPrintSheetView::OnKillFocusInplaceEditShingling() 		// Message from InplaceEdit
{
	CPoint pt;
	CRect  rect;
    GetCursorPos(&pt);  
	m_InplaceButtonShingling.GetWindowRect(rect);
	if (!rect.PtInRect(pt))
		m_InplaceButtonShingling.Deactivate();
}

void CPrintSheetView::OnClickedInplaceButtonShingling() 		// Message from InplaceButton
{
	CPageTemplate* pPageTemplate = NULL;
	if (!m_InplaceButtonShingling.m_di.IsNull())
		pPageTemplate = (CPageTemplate*)m_InplaceButtonShingling.m_di.m_pData;
    if (!pPageTemplate)
        return;

	pPageTemplate->m_bShinglingOffsetLocked = FALSE;

	float fDummy;
	CImpManDoc::GetDoc()->m_PageTemplateList.CalcShinglingOffsets(fDummy, fDummy, fDummy, pPageTemplate->m_nProductPartIndex);

	m_InplaceButtonShingling.Deactivate();

    InvalidateRect(m_InplaceEditShingling.m_di.m_AuxRect);
    UpdateWindow();
}

BOOL CPrintSheetView::OnSelectMark(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	return FALSE;
}

BOOL CPrintSheetView::OnSelectReflineXDistance(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	if (m_DisplayList.m_bIsInsideMultiSelect)
		return FALSE;

	CRect spinRect(pDI->m_BoundingRect.right + 1,  pDI->m_BoundingRect.CenterPoint().y - 8, 
				   pDI->m_BoundingRect.right + 24, pDI->m_BoundingRect.CenterPoint().y + 9);
	CRect editRect = pDI->m_BoundingRect;
	editRect.InflateRect(1,1);

	m_reflineXSpin.MoveWindow(spinRect);
	m_reflineXSpin.ShowWindow(SW_SHOW);

	m_pReflineXValue = (float*)pDI->m_pData;

	CString strValue;
	if (IsOpenDlgAlignObjects())
		GetDlgAlignObjects()->m_fDistance = abs(*m_pReflineXValue);

	SetMeasure(0, abs(*m_pReflineXValue), strValue);	// format string

	m_reflineDistanceEdit.SetWindowText(strValue);
	m_reflineDistanceEdit.MoveWindow(editRect);
	m_reflineDistanceEdit.ShowWindow(SW_SHOW);
	m_reflineDistanceEdit.SetFocus();
	m_reflineDistanceEdit.SetSel(0, -1);

	return TRUE;
}

BOOL CPrintSheetView::OnDeselectReflineXDistance(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	if (IsOpenDlgAlignObjects())
		GetMeasure(m_reflineDistanceEdit.m_hWnd, GetDlgAlignObjects()->m_fDistance);
	else
		GetMeasure(m_reflineDistanceEdit.m_hWnd, *((float*)pDI->m_pData));
	
	m_reflineXSpin.ShowWindow(SW_HIDE);
	m_reflineDistanceEdit.ShowWindow(SW_HIDE);

	if (GetDlgOutputMedia()->GetDlgPDFTargetProps())
	{
		GetDlgOutputMedia()->GetDlgPDFTargetProps()->UpdateControls();
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	}
	else
	{
		if (IsOpenDlgAlignObjects())
			GetDlgAlignObjects()->ChangeDistance();
		else
			if (IsOpenDlgLayoutObjectGeometry() )
				GetDlgLayoutObjectGeometry()->OnLayoutDistanceChanged();
	}

	return TRUE;
}

BOOL CPrintSheetView::OnSelectReflineYDistance(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	if (m_DisplayList.m_bIsInsideMultiSelect)
		return FALSE;

	CRect spinRect(pDI->m_BoundingRect.right + 1,  pDI->m_BoundingRect.CenterPoint().y - 10, 
				   pDI->m_BoundingRect.right + 15, pDI->m_BoundingRect.CenterPoint().y + 10);
	CRect editRect = pDI->m_BoundingRect;
	editRect.InflateRect(1,1);

	m_reflineYSpin.MoveWindow(spinRect);
	m_reflineYSpin.ShowWindow(SW_SHOW);

	m_pReflineYValue = (float*)pDI->m_pData;

	CString strValue;
	if (IsOpenDlgAlignObjects())
		GetDlgAlignObjects()->m_fDistance = abs(*m_pReflineYValue);

	SetMeasure(0, abs(*m_pReflineYValue), strValue);	// format string

	m_reflineDistanceEdit.SetWindowText(strValue);
	m_reflineDistanceEdit.MoveWindow(editRect);
	m_reflineDistanceEdit.ShowWindow(SW_SHOW);
	m_reflineDistanceEdit.SetFocus();
	m_reflineDistanceEdit.SetSel(0, -1);

	return TRUE;
}

BOOL CPrintSheetView::OnDeselectReflineYDistance(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	if (IsOpenDlgAlignObjects())
		GetMeasure(m_reflineDistanceEdit.m_hWnd, GetDlgAlignObjects()->m_fDistance);
	else
		GetMeasure(m_reflineDistanceEdit.m_hWnd, *((float*)pDI->m_pData));

	m_reflineYSpin.ShowWindow(SW_HIDE);
	m_reflineDistanceEdit.ShowWindow(SW_HIDE);

	if (GetDlgOutputMedia()->GetDlgPDFTargetProps())
	{
		GetDlgOutputMedia()->GetDlgPDFTargetProps()->UpdateControls();
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	}
	else
	{
		if (IsOpenDlgAlignObjects())
			GetDlgAlignObjects()->ChangeDistance();
		else
			if (IsOpenDlgLayoutObjectGeometry())
				GetDlgLayoutObjectGeometry()->OnLayoutDistanceChanged();
	}

	return TRUE;
}

void CPrintSheetView::OnDeltaposReflineXSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	if (IsOpenDlgAlignObjects())
	{
		if (pNMUpDown->iDelta < 0)
			GetDlgAlignObjects()->m_fDistance += (GetDlgAlignObjects()->m_reflinePosParams.m_fDistanceX < 0.0f) ?  0.5f : -0.5f;
		else
			GetDlgAlignObjects()->m_fDistance += (GetDlgAlignObjects()->m_reflinePosParams.m_fDistanceX < 0.0f) ? -0.5f :  0.5f;
		GetDlgAlignObjects()->ChangeDistance();
		CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineXDistance));
		if (pDI)
		{
			m_pReflineXValue = (float*)pDI->m_pData;
			CString strValue;
			SetMeasure(0, abs(*m_pReflineXValue), strValue);	// format string
			m_reflineDistanceEdit.SetWindowText(strValue);
		}
	}
	else
	{
		SpinMeasure(&m_reflineDistanceEdit, pNMUpDown->iDelta, 0.5f);
		GetMeasure(m_reflineDistanceEdit.m_hWnd, *m_pReflineXValue);
	}

	CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pView)
	{
		pView->UpdateSheetPreview(TRUE);
//		pView->LoadMarkIntoDialog(pView->m_nCurMarkSel);
	}
	else
	{
		if (GetDlgOutputMedia()->GetDlgPDFTargetProps())
		{
			GetDlgOutputMedia()->GetDlgPDFTargetProps()->UpdateControls();
        	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		}
	}
	*pResult = 0;
}

void CPrintSheetView::OnDeltaposReflineYSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	
	if (IsOpenDlgAlignObjects())
	{
		if (pNMUpDown->iDelta < 0)
			GetDlgAlignObjects()->m_fDistance += (GetDlgAlignObjects()->m_reflinePosParams.m_fDistanceY < 0.0f) ?  0.5f : -0.5f;
		else
			GetDlgAlignObjects()->m_fDistance += (GetDlgAlignObjects()->m_reflinePosParams.m_fDistanceY < 0.0f) ? -0.5f :  0.5f;
		GetDlgAlignObjects()->ChangeDistance();
		CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineYDistance));
		if (pDI)
		{
			m_pReflineYValue = (float*)pDI->m_pData;
			CString strValue;
			SetMeasure(0, abs(*m_pReflineYValue), strValue);	// format string
			m_reflineDistanceEdit.SetWindowText(strValue);
		}
	}
	else
	{
		SpinMeasure(&m_reflineDistanceEdit, pNMUpDown->iDelta, 0.5f);
		GetMeasure(m_reflineDistanceEdit.m_hWnd, *m_pReflineYValue);
	}

	CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pView)
	{
		pView->UpdateSheetPreview(TRUE);
//		pView->LoadMarkIntoDialog(pView->m_nCurMarkSel);
	}
	else
	{
		if (GetDlgOutputMedia())
			if (GetDlgOutputMedia()->GetDlgPDFTargetProps())
			{
				GetDlgOutputMedia()->GetDlgPDFTargetProps()->UpdateControls();
        		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
			}
	}
	*pResult = 0;
}

void CPrintSheetView::OnKillfocusReflineDistanceEdit()
{
	if (IsOpenDlgAlignObjects() || IsOpenDlgLayoutObjectGeometry())
	{
		CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineXDistance));
		if ( ! pDI)
			pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, ReflineYDistance));
		if (pDI)
			m_DisplayList.DeselectItem(*pDI);
	}
	else
		m_DisplayList.DeselectAll();


	m_reflineXSpin.ShowWindow(SW_HIDE);
	m_reflineYSpin.ShowWindow(SW_HIDE);
	m_reflineDistanceEdit.ShowWindow(SW_HIDE);
}




/////////////////////////////////////////////////////////////////////////////
// Snap callbacks from display list

// Filter called when layout object dragging is started
BOOL CPrintSheetView::OnFilterSnapGridLayoutObject(CDisplayItem* pDI)
{
    if ( ! pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
		return FALSE;

	CDisplayItem* pSelectedDI = m_DisplayList.GetFirstSelectedItem();
    if (!pSelectedDI)
        return FALSE;

    if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
    {
       	if (((CLayoutObject*)pSelectedDI->m_pData)->GetLayoutSide() == ((CLayoutObject*)pDI->m_pData)->GetLayoutSide())
       		if (pSelectedDI->m_pAuxData == pDI->m_pAuxData)	// same layoutside and same printsheet
       			return TRUE;
    }
    else
        if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		{
       		CLayoutSide* pLayoutSide = ((CLayoutObject*)pDI->m_pData)->GetLayoutSide();
			int nSide = (pSelectedDI->m_AuxRect.Height() > 1) ? FRONTSIDE : BACKSIDE;
			if (nSide == pLayoutSide->m_nLayoutSide)
	       		if (pSelectedDI->m_pAuxData == pDI->m_pAuxData)	// same layoutside and same printsheet
			    	return TRUE;
		}
		else
			if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet)))
       			return TRUE;
			else
				if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FlatProduct)))
        			return TRUE;
				else
					if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia)))
        				return TRUE;
					else
						if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark)))
							if (CImpManDoc::GetDoc()->m_PrintSheetList.GetPrintSheetFromNumber((int)pSelectedDI->m_pAuxData & 0x0000ffff) == pDI->m_pAuxData)	// same printsheet
								if (((CLayoutObject*)pDI->m_pData)->m_nType == CLayoutObject::Page)
	        						return TRUE;

    return FALSE;
}

void CPrintSheetView::OnGetSnapCoordsLayoutObject(CDisplayItem* pDI, unsigned nSnapPosition, float* x, float* y)
{
    if ( ! pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
		return;

	CLayoutObject* pObject	   = (CLayoutObject*)pDI->m_pData;
	CPrintSheet*   pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
	if ( ! pObject || ! pPrintSheet)
		return;

    if (nSnapPosition & CDisplayItem::snapOutside)	// bounding rect and aux rect have changed meaning for layout objects (bounding rect = outer rect / aux rect = inner rect)
    {												// so snapOutside means snapInside here
        switch (nSnapPosition & ~CDisplayItem::snapOutside)
        {
        case CDisplayItem::snapTopLeft:		*x = pObject->GetLeft();	*y = pObject->GetTop();		break;
        case CDisplayItem::snapTopRight:	*x = pObject->GetRight();	*y = pObject->GetTop();		break;
        case CDisplayItem::snapBottomRight: *x = pObject->GetRight();	*y = pObject->GetBottom();	break;
        case CDisplayItem::snapBottomLeft:	*x = pObject->GetLeft();	*y = pObject->GetBottom();	break;
        case CDisplayItem::snapTop:			*x = pObject->GetLeft() + 
        										 pObject->GetWidth()/2;	*y = pObject->GetTop();		break;
        case CDisplayItem::snapRight:		*x = pObject->GetRight();	*y = pObject->GetBottom() +
        																	 pObject->GetHeight()/2;break;
        case CDisplayItem::snapBottom:		*x = pObject->GetLeft() + 
        										 pObject->GetWidth()/2;	*y = pObject->GetBottom();	break;
        case CDisplayItem::snapLeft:		*x = pObject->GetLeft();	*y = pObject->GetBottom() +
        																	 pObject->GetHeight()/2;break;
        case CDisplayItem::snapMiddle:		*x = pObject->GetLeft() + 
        										 pObject->GetWidth()/2;	*y = pObject->GetBottom() +
        																	 pObject->GetHeight()/2;break;
        default:							*x = pObject->GetLeft();	*y = pObject->GetTop();		break;
        }
    }
    else
    {
		float fLeft	  = pObject->GetLeft()   - pObject->GetCurrentGrossMargin(pPrintSheet, LEFT);
		float fBottom = pObject->GetBottom() - pObject->GetCurrentGrossMargin(pPrintSheet, BOTTOM);
		float fRight  = pObject->GetRight()  + pObject->GetCurrentGrossMargin(pPrintSheet, RIGHT);
		float fTop	  = pObject->GetTop()	 + pObject->GetCurrentGrossMargin(pPrintSheet, TOP);
		float fWidth  = fRight - fLeft;
		float fHeight = fTop   - fBottom;
        switch (nSnapPosition & ~CDisplayItem::snapOutside)
        {
        case CDisplayItem::snapTopLeft:		*x = fLeft;				*y = fTop;						break;
        case CDisplayItem::snapTopRight:	*x = fRight;			*y = fTop;						break;
        case CDisplayItem::snapBottomRight: *x = fRight;			*y = fBottom;					break;
        case CDisplayItem::snapBottomLeft:	*x = fLeft;				*y = fBottom;					break;
        case CDisplayItem::snapTop:			*x = fLeft + fWidth/2;	*y = fTop;						break;
        case CDisplayItem::snapRight:		*x = fRight;			*y = fBottom + fHeight/2;		break;
        case CDisplayItem::snapBottom:		*x = fLeft + fWidth/2;	*y = fBottom;					break;
        case CDisplayItem::snapLeft:		*x = fLeft;				*y = fBottom + fHeight/2;		break;
        case CDisplayItem::snapMiddle:		*x = fLeft + fWidth/2;	*y = fBottom + fHeight/2;		break;
        default:							*x = fLeft;				*y = fTop;						break;
        }
    }
}

// Filter called when foldsheet dragging is started
BOOL CPrintSheetView::OnFilterSnapGridFoldSheet(CDisplayItem* pDI)
{
    CDisplayItem* pSelectedDI = m_DisplayList.GetFirstSelectedItem();
    if (!pSelectedDI)
        return FALSE;

    return FALSE;
}

void CPrintSheetView::OnGetSnapCoordsFoldSheet(CDisplayItem* pDI, unsigned nSnapPosition, float* x, float* y)
{
    if ( ! pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		return;

	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
	if ( ! pPrintSheet)
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;
	CLayoutSide* pLayoutSide = (pDI->m_AuxRect.Height() > 1) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;

	float fLeft, fBottom, fRight, fTop, fWidth, fHeight;
	pLayoutSide->CalcBoundingRectObjects(&fLeft, &fBottom, &fRight, &fTop, &fWidth, &fHeight, FALSE, (int)pDI->m_pData, FALSE, pPrintSheet);		

    switch (nSnapPosition & ~CDisplayItem::snapOutside)
    {
    case CDisplayItem::snapTopLeft:		*x = fLeft;				*y = fTop;						break;
    case CDisplayItem::snapTopRight:	*x = fRight;			*y = fTop;						break;
    case CDisplayItem::snapBottomRight: *x = fRight;			*y = fBottom;					break;
    case CDisplayItem::snapBottomLeft:	*x = fLeft;				*y = fBottom;					break;
    case CDisplayItem::snapTop:			*x = fLeft + fWidth/2;	*y = fTop;						break;
    case CDisplayItem::snapRight:		*x = fRight;			*y = fBottom + fHeight/2;		break;
    case CDisplayItem::snapBottom:		*x = fLeft + fWidth/2;	*y = fBottom;					break;
    case CDisplayItem::snapLeft:		*x = fLeft;				*y = fBottom + fHeight/2;		break;
    case CDisplayItem::snapMiddle:		*x = fLeft + fWidth/2;	*y = fBottom + fHeight/2;		break;
    default:							*x = fLeft;				*y = fTop;						break;
    }
}

BOOL CPrintSheetView::OnFilterSnapGridSheet(CDisplayItem* pDI)
{
    CDisplayItem* pSelectedDI = m_DisplayList.GetFirstSelectedItem();
    if (!pSelectedDI)
        return FALSE;

    if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
    {
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
        {
        	CLayoutSide* pLayoutSide = (CLayoutSide*)pDI->m_pData;
        	if (((CLayoutObject*)pSelectedDI->m_pData)->GetLayoutSide() == pLayoutSide)	// same layoutside 
        		if (pSelectedDI->m_pAuxData == pDI->m_pAuxData)	// same printsheet
        			return TRUE;
        }
    }
    else
		if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		{
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
			{
        		CLayoutSide* pLayoutSide = (CLayoutSide*)pDI->m_pData;
				int nSide = (pSelectedDI->m_AuxRect.Height() > 1) ? FRONTSIDE : BACKSIDE;
				if (nSide == pLayoutSide->m_nLayoutSide)			// same layoutside 
        			if (pSelectedDI->m_pAuxData == pDI->m_pAuxData)	// same printsheet
        				return TRUE;
			}
		}
		else
			if ( (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet))) || (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FlatProduct))) )
				return TRUE;
			else
			{
				if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia)))
        			return TRUE;
				else
					if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark)))
						if (CImpManDoc::GetDoc()->m_PrintSheetList.GetPrintSheetFromNumber((int)pSelectedDI->m_pAuxData & 0x0000ffff) == pDI->m_pAuxData)	// same printsheet
	        				return TRUE;
			}

    return FALSE;
}

void CPrintSheetView::OnGetSnapCoordsSheet(CDisplayItem* pDI, unsigned nSnapPosition, float* x, float* y)
{
    CLayoutSide* pLayoutSide;
    if ((int)pDI->m_pData == FRONTSIDE)
        pLayoutSide = &((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->m_FrontSide;
    else
        pLayoutSide = &((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->m_BackSide;

    switch (nSnapPosition & ~CDisplayItem::snapOutside)
    {
    case CDisplayItem::snapTopLeft:		*x = pLayoutSide->m_fPaperLeft;		*y = pLayoutSide->m_fPaperTop;		break;
    case CDisplayItem::snapTopRight:	*x = pLayoutSide->m_fPaperRight;	*y = pLayoutSide->m_fPaperTop;		break;
    case CDisplayItem::snapBottomRight: *x = pLayoutSide->m_fPaperRight;	*y = pLayoutSide->m_fPaperBottom;	break;
    case CDisplayItem::snapBottomLeft:	*x = pLayoutSide->m_fPaperLeft;		*y = pLayoutSide->m_fPaperBottom;	break;
    case CDisplayItem::snapTop:			*x = pLayoutSide->m_fPaperLeft + 
        									 pLayoutSide->m_fPaperWidth/2;	*y = pLayoutSide->m_fPaperTop;		break;
    case CDisplayItem::snapRight:		*x = pLayoutSide->m_fPaperRight;	*y = pLayoutSide->m_fPaperBottom +
        																		 pLayoutSide->m_fPaperHeight/2;	break;
    case CDisplayItem::snapBottom:		*x = pLayoutSide->m_fPaperLeft + 
        									 pLayoutSide->m_fPaperWidth/2;	*y = pLayoutSide->m_fPaperBottom;	break;
    case CDisplayItem::snapLeft:		*x = pLayoutSide->m_fPaperLeft;		*y = pLayoutSide->m_fPaperBottom +
        																		 pLayoutSide->m_fPaperHeight/2;	break;
    case CDisplayItem::snapMiddle:		*x = pLayoutSide->m_fPaperLeft + 
        									 pLayoutSide->m_fPaperWidth/2;	*y = pLayoutSide->m_fPaperBottom +
        																		 pLayoutSide->m_fPaperHeight/2;	break;
    default:							*x = pLayoutSide->m_fPaperLeft;		*y = pLayoutSide->m_fPaperTop;		break;
    }
}

BOOL CPrintSheetView::OnFilterSnapGridBoundingBoxFrame(CDisplayItem* pDI)
{
    CDisplayItem* pSelectedDI = m_DisplayList.GetFirstSelectedItem();
    if (!pSelectedDI)
        return FALSE;

    if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
    {
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, BoundingBoxFrame)))
        {
        	CLayoutSide* pLayoutSide = (CLayoutSide*)pDI->m_pData;
        	if (((CLayoutObject*)pSelectedDI->m_pData)->GetLayoutSide() == pLayoutSide)	// same layoutside 
        		if (pSelectedDI->m_pAuxData == pDI->m_pAuxData)	// same printsheet
        			return TRUE;
        }
    }
    else
		if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		{
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, BoundingBoxFrame)))
			{
        		CLayoutSide* pLayoutSide = (CLayoutSide*)pDI->m_pData;
				int nSide = (pSelectedDI->m_AuxRect.Height() > 1) ? FRONTSIDE : BACKSIDE;
				if (nSide == pLayoutSide->m_nLayoutSide)			// same layoutside 
        			if (pSelectedDI->m_pAuxData == pDI->m_pAuxData)	// same printsheet
        				return TRUE;
			}
		}
		else
			if ( (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet))) || (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FlatProduct))) )
				return TRUE;
			else
			{
				if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia)))
        			return TRUE;
				else
					if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark)))
						if (CImpManDoc::GetDoc()->m_PrintSheetList.GetPrintSheetFromNumber((int)pSelectedDI->m_pAuxData & 0x0000ffff) == pDI->m_pAuxData)	// same printsheet
	        				return TRUE;
			}

    return FALSE;
}

void CPrintSheetView::OnGetSnapCoordsBoundingBoxFrame(CDisplayItem* pDI, unsigned nSnapPosition, float* x, float* y)
{
    CLayoutSide* pLayoutSide;
    if ((int)pDI->m_pData == FRONTSIDE)
        pLayoutSide = &((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->m_FrontSide;
    else
        pLayoutSide = &((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->m_BackSide;

	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop, fBBoxWidth, fBBoxHeight; 
	pLayoutSide->CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);
	fBBoxWidth  = fBBoxRight - fBBoxLeft;
	fBBoxHeight = fBBoxTop	 - fBBoxBottom;

	switch (nSnapPosition & ~CDisplayItem::snapOutside)
    {
    case CDisplayItem::snapTopLeft:		*x = fBBoxLeft;					*y = fBBoxTop;						break;
    case CDisplayItem::snapTopRight:	*x = fBBoxRight;				*y = fBBoxTop;						break;
    case CDisplayItem::snapBottomRight: *x = fBBoxRight;				*y = fBBoxBottom;					break;
    case CDisplayItem::snapBottomLeft:	*x = fBBoxLeft;					*y = fBBoxBottom;					break;
    case CDisplayItem::snapTop:			*x = fBBoxLeft + fBBoxWidth/2;	*y = fBBoxTop;						break;
    case CDisplayItem::snapRight:		*x = fBBoxRight;				*y = fBBoxBottom + fBBoxHeight/2;	break;
    case CDisplayItem::snapBottom:		*x = fBBoxLeft + fBBoxWidth/2;	*y = fBBoxBottom;					break;
    case CDisplayItem::snapLeft:		*x = fBBoxLeft;					*y = fBBoxBottom + fBBoxHeight/2;	break;
    case CDisplayItem::snapMiddle:		*x = fBBoxLeft + fBBoxWidth/2;	*y = fBBoxBottom + fBBoxHeight/2;	break;
    default:							*x = fBBoxLeft;					*y = fBBoxTop;						break;
    }
}

BOOL CPrintSheetView::OnFilterSnapGridPlate(CDisplayItem* pDI)
{
    CDisplayItem* pSelectedDI = m_DisplayList.GetFirstSelectedItem();
    if (!pSelectedDI)
        return FALSE;

    if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
    {
        if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
        {
        	CLayoutSide* pLayoutSide = (CLayoutSide*)pDI->m_pData;
        	if (((CLayoutObject*)pSelectedDI->m_pData)->GetLayoutSide() == pLayoutSide)	// same layoutside 
        		if (pSelectedDI->m_pAuxData == pDI->m_pAuxData)	// same printsheet
        			return TRUE;
        }
    }
    else
		if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		{
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
			{
        		CLayoutSide* pLayoutSide = (CLayoutSide*)pDI->m_pData;
				int nSide = (pSelectedDI->m_AuxRect.Height() > 1) ? FRONTSIDE : BACKSIDE;
				if (nSide == pLayoutSide->m_nLayoutSide)			// same layoutside 
        			if (pSelectedDI->m_pAuxData == pDI->m_pAuxData)	// same printsheet
        				return TRUE;
			}
		}
		else
			if ( (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet))) || (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FlatProduct))) )
				return TRUE;
			else
			{
				if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia)))
        			return TRUE;
				else
					if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark)))
						if (CImpManDoc::GetDoc()->m_PrintSheetList.GetPrintSheetFromNumber((int)pSelectedDI->m_pAuxData & 0x0000ffff) == pDI->m_pAuxData)	// same printsheet
	        				return TRUE;
			}

    return FALSE;
}

void CPrintSheetView::OnGetSnapCoordsPlate(CDisplayItem* pDI, unsigned nSnapPosition, float* x, float* y)
{
    CPressDevice* pPressDevice = ((CLayoutSide*)pDI->m_pData)->GetPressDevice();
    float fWidth  = pPressDevice->m_fPlateWidth;
    float fHeight = pPressDevice->m_fPlateHeight;

    switch (nSnapPosition & ~CDisplayItem::snapOutside)
    {
    case CDisplayItem::snapTopLeft:		*x = 0.0f;		*y = fHeight;	break;
    case CDisplayItem::snapTopRight:	*x = fWidth;	*y = fHeight;	break;
    case CDisplayItem::snapBottomRight: *x = fWidth;	*y = 0.0f;		break;
    case CDisplayItem::snapBottomLeft:	*x = 0.0f;		*y = 0.0f;		break;
    case CDisplayItem::snapTop:			*x = fWidth/2;	*y = fHeight;	break;
    case CDisplayItem::snapRight:		*x = fWidth;	*y = fHeight/2;	break;
    case CDisplayItem::snapBottom:		*x = fWidth/2;	*y = 0.0f;		break;
    case CDisplayItem::snapLeft:		*x = 0.0f;		*y = fHeight/2;	break;
    case CDisplayItem::snapMiddle:		*x = fWidth/2;	*y = fHeight/2;	break;
    default:							*x = 0.0f;		*y = fHeight;	break;
    }
}


BOOL CPrintSheetView::OnFilterSnapGridOutputMedia(CDisplayItem* /*pDI*/)
{
    CDisplayItem* pSelectedDI = m_DisplayList.GetFirstSelectedItem();
    if (!pSelectedDI)
        return FALSE;

    if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia)))
        return FALSE;

    return TRUE;
}

void CPrintSheetView::OnGetSnapCoordsOutputMedia(CDisplayItem* pDI, unsigned nSnapPosition, float* x, float* y)
{
    CDlgPDFOutputMediaSettings* pDlg  = GetDlgOutputMedia();

    float fLeft, fBottom, fRight, fTop;
    float fWidth, fHeight;

	pDlg->GetTileBBox((CPrintSheet*)pDI->m_pData, (int)pDI->m_pAuxData, fLeft, fBottom, fRight, fTop);
	fWidth  = fRight - fLeft;
	fHeight = fTop   - fBottom;

    switch (nSnapPosition & ~CDisplayItem::snapOutside)
    {
    case CDisplayItem::snapTopLeft:		*x = fLeft;				*y = fTop;					break;
    case CDisplayItem::snapTopRight:	*x = fRight;			*y = fTop;					break;
    case CDisplayItem::snapBottomRight: *x = fRight;			*y = fBottom;				break;
    case CDisplayItem::snapBottomLeft:	*x = fLeft;				*y = fBottom;				break;
    case CDisplayItem::snapTop:			*x = fLeft + fWidth/2;	*y = fTop;					break;
    case CDisplayItem::snapRight:		*x = fRight;			*y = fBottom + fHeight/2;	break;
    case CDisplayItem::snapBottom:		*x = fLeft + fWidth/2;	*y = fBottom;				break;
    case CDisplayItem::snapLeft:		*x = fLeft;				*y = fBottom + fHeight/2;	break;
    case CDisplayItem::snapMiddle:		*x = fLeft + fWidth/2;	*y = fBottom + fHeight/2;	break;
    default:							*x = fLeft;				*y = fBottom;				break;
    }
}


void CPrintSheetView::OnGetSnapCoordsMark(CDisplayItem* pDI, unsigned nSnapPosition, float* x, float* y)
{
    CMark* pMark			 = (CMark*)pDI->m_pData;
	int	   nPrintSheetNumber = ( (int)pDI->m_pAuxData) & 0x0000ffff;
	int	   nSide			 = (((int)pDI->m_pAuxData) & 0xffff0000) >> 16;
	CPrintSheet* pPrintSheet = CImpManDoc::GetDoc()->m_PrintSheetList.GetPrintSheetFromNumber(nPrintSheetNumber);

	float fLeft, fBottom, fRight, fTop, fWidth, fHeight;
	pMark->GetSheetTrimBox(pPrintSheet, NULL, nSide, 0, fLeft, fBottom, fRight, fTop);
	fWidth  = fRight - fLeft;
	fHeight = fTop   - fBottom;

    switch (nSnapPosition & ~CDisplayItem::snapOutside)
    {
    case CDisplayItem::snapTopLeft:		*x = fLeft;				*y = fTop;					break;
    case CDisplayItem::snapTopRight:	*x = fRight;			*y = fTop;					break;
    case CDisplayItem::snapBottomRight: *x = fRight;			*y = fBottom;				break;
    case CDisplayItem::snapBottomLeft:	*x = fLeft;				*y = fBottom;				break;
    case CDisplayItem::snapTop:			*x = fLeft + fWidth/2;	*y = fTop;					break;
    case CDisplayItem::snapRight:		*x = fRight;			*y = fBottom + fHeight/2;	break;
    case CDisplayItem::snapBottom:		*x = fLeft + fWidth/2;	*y = fBottom;				break;
    case CDisplayItem::snapLeft:		*x = fLeft;				*y = fBottom + fHeight/2;	break;
    case CDisplayItem::snapMiddle:		*x = fLeft + fWidth/2;	*y = fBottom + fHeight/2;	break;
    default:							*x = fLeft;				*y = fBottom;				break;
    }
}

BOOL CPrintSheetView::OnFilterSnapGridMark(CDisplayItem* /*pDI*/)
{
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetView ContextMenu

void CPrintSheetView::OnContextMenu(CWnd*, CPoint point)
{
	CLayoutObject* pObject = NULL;
	int			   nMenuID = IDR_PRINTSHEETVIEW_POPUP;
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	if (pDI)
	{
		if (pDI->m_pData)
		{
			nMenuID = IDR_PRINTSHEETVIEW_OBJECT_POPUP;
			pObject = (CLayoutObject*)pDI->m_pData;
		}
	}
	else
	{
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
		if (pDI)
			if (pDI->m_pData)
				nMenuID = IDR_PRINTSHEETVIEW_OBJECT_POPUP;
	}

    CMenu menu;
	VERIFY(menu.LoadMenu(nMenuID));

	CMenu* pPopup = menu.GetSubMenu(0);
	if ( ! pPopup)
		return;

	if (pObject)
		if (pObject->m_nType == CLayoutObject::ControlMark)
		{
			pPopup->DeleteMenu(ID_MOVE_OBJECT,		MF_BYCOMMAND);
			pPopup->DeleteMenu(ID_CHANGE_FORMATS,   MF_BYCOMMAND);
		}

	CWnd* pWndPopupOwner = this;
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	if (nMenuID == IDR_PRINTSHEETVIEW_POPUP)
	{
	///////////////////////////////////////////////////////////////////
	//  Add bitmaps to menu-items corresponding to frame colors
		CBitmap MarineBmp, CyanBmp, RedBmp, YellowBmp, MarineChkBmp, CyanChkBmp, RedChkBmp, YellowChkBmp;
		MarineBmp.LoadBitmap(IDB_MARINE);
		CyanBmp.LoadBitmap(IDB_CYAN);
		RedBmp.LoadBitmap(IDB_RED);
		YellowBmp.LoadBitmap(IDB_YELLOW);
		MarineChkBmp.LoadBitmap(IDB_MARINE_CHECKED);
		CyanChkBmp.LoadBitmap(IDB_CYAN_CHECKED);
		RedChkBmp.LoadBitmap(IDB_RED_CHECKED);
		YellowChkBmp.LoadBitmap(IDB_YELLOW_CHECKED);

		if (ShowBitmap())
		{
			pPopup->SetMenuItemBitmaps(ID_MARINE_FRAME, MF_BYCOMMAND, &MarineBmp, &MarineChkBmp);
			pPopup->SetMenuItemBitmaps(ID_CYAN_FRAME,	MF_BYCOMMAND, &CyanBmp,	  &CyanChkBmp);
			pPopup->SetMenuItemBitmaps(ID_RED_FRAME,	MF_BYCOMMAND, &RedBmp,	  &RedChkBmp);
			pPopup->SetMenuItemBitmaps(ID_YELLOW_FRAME, MF_BYCOMMAND, &YellowBmp, &YellowChkBmp);
			m_bEnableColorMenues = TRUE;
		}
		else
			m_bEnableColorMenues = FALSE;
	}
///////////////////////////////////////////////////////////////////
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
}

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetView Translate Message-Queue -> looking for key-strokes etc.

BOOL CPrintSheetView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	// Shift+F10: show pop-up menu.
	if ((((pMsg->message == WM_KEYDOWN || pMsg->message == WM_SYSKEYDOWN) && // If we hit a key and
		(pMsg->wParam == VK_F10) && (GetKeyState(VK_SHIFT) & ~1)) != 0) ||	// it's Shift+F10 OR
		(pMsg->message == WM_CONTEXTMENU))									// Natural keyboard key
	{
		CRect rect;
		GetClientRect(rect);
		ClientToScreen(rect);
		
		CPoint point = rect.TopLeft();
		point.Offset(5, 5);
		OnContextMenu(NULL, point);
		
		return TRUE;
	}

	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
	{
		if (m_reflineDistanceEdit.IsWindowVisible())
			OnKillfocusReflineDistanceEdit();
		if (m_InplaceEditPageNum.IsWindowVisible())
			OnKillFocusInplaceEditPageNum();
	}

	// Let the ToolTip process this message.
	if (m_tooltip.m_hWnd)
		m_tooltip.RelayEvent(pMsg);

	if (m_printViewToolbar1Buttons.m_pToolTip)            
		m_printViewToolbar1Buttons.m_pToolTip->RelayEvent(pMsg);	

	if (m_printViewToolbar2Buttons.m_pToolTip)            
		m_printViewToolbar2Buttons.m_pToolTip->RelayEvent(pMsg);	

	if (m_printViewToolbar3Buttons.m_pToolTip)            
		m_printViewToolbar3Buttons.m_pToolTip->RelayEvent(pMsg);	

	if (m_zoomToolBar.m_pToolTip)            
		m_zoomToolBar.m_pToolTip->RelayEvent(pMsg);	

	return CScrollView::PreTranslateMessage(pMsg);
}

BOOL CPrintSheetView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	CBrush* pBkgrdBrush = new CBrush;
	pBkgrdBrush->CreateSolidBrush(RGB(127,162,207));

	//lpszClassName = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)*pBkgrdBrush, 0);

    BOOL ret = CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	pBkgrdBrush->Detach();
	delete pBkgrdBrush;

    return ret;
}

BOOL CPrintSheetView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	//if (GetAsyncKeyState(VK_SHIFT))
	//    return CScrollView::OnSetCursor(pWnd, nHitTest, message);
	if (MarksViewActive() && GetAsyncKeyState(VK_CONTROL))
	{
		if (m_pBkBitmap && ! HandActive())
			SetCursor(theApp.m_CursorCopy);
		else
			SetCursor(theApp.m_CursorStandard);

	    return TRUE;//CScrollView::OnSetCursor(pWnd, nHitTest, message);
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	int nSight;

	if (ViewMode() == ID_ZOOM)	
	{
		if (GetKeyState(VK_CONTROL) < 0)
			SetCursor(theApp.m_CursorMagnifyDecrease);
		else
			if (GetAsyncKeyState(VK_SHIFT))
				SetCursor(theApp.m_CursorHand);
			else
				SetCursor(theApp.m_CursorMagnifyIncrease);
	}
	else
	{
		if (HandActive())
			SetCursor(theApp.m_CursorHand);
		else
			if (TrimToolActive())
			{
    			nSight = GetActSight();
    			if (nSight == FRONTSIDE || nSight == BACKSIDE || nSight == BOTHSIDES)
    				SetCursor(theApp.m_CursorTrim);
			}
			else 
			{
				if (AlignSectionActive())
					SetCursor(theApp.m_CursorAlignSection);
				else
					if (m_ColumnBars.m_bMouseIsOver)
						SetCursor(theApp.m_CursorSizeWE);
					else
						SetCursor(theApp.m_CursorStandard);
			}
	}

	return TRUE;// CScrollView::OnSetCursor(pWnd, nHitTest, message);
}

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetView Helper functions

unsigned int CPrintSheetView::WhichQuadrant(CRect rect, CPoint point)
{
    unsigned int nPosition;

    int width  = rect.Width() / 2;
    int height = rect.Height()/ 2;

    if (rect.PtInRect(point))
    {
        if (point.x < (rect.left + width))
        	if (point.y < (rect.bottom - height))
        		nPosition = LEFT_TOP;
        	else
        		nPosition = LEFT_BOTTOM;
        else
        	if (point.y < (rect.bottom - height))
        		nPosition = RIGHT_TOP;
        	else
        		nPosition = RIGHT_BOTTOM;

        return nPosition;
    }
    else
        return NULL;
}

void CPrintSheetView::OnFilePrint() 
{
	CView::OnFilePrint();	
}

void CPrintSheetView::OnFilePrintPreview() 
{
	CView::OnFilePrintPreview();	
}

BOOL CPrintSheetView::OnPreparePrinting(CPrintInfo* pInfo) 
{	
	BOOL bRet;
    CImpManDoc* pDoc = CImpManDoc::GetDoc();
    if (GetActSight() == ALLSIDES)
    {
        m_nSheetsPerPage = theApp.settings.m_nSheetsPerPage;
        int nPrintSheets = pDoc->m_PrintSheetList.GetCount();
        int nPages = (nPrintSheets / m_nSheetsPerPage);
        if ((nPrintSheets % m_nSheetsPerPage) != 0)
        	nPages += 1;
        pInfo->SetMaxPage(nPages);
        bRet = DoPreparePrinting(pInfo);// default preparation
        if (nPages > 1)
        	pInfo->m_nNumPreviewPages = 2;	// Preview 2 pages at a time
        else								// Set this value after calling DoPreparePrinting to override
        	pInfo->m_nNumPreviewPages = 1;	// value read from .INI file
    }
    else
    {
        pInfo->SetMaxPage(1);
		bRet = DoPreparePrinting(pInfo);
        pInfo->m_nNumPreviewPages = 1;
    }
    return bRet;
}

void CPrintSheetView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
    // TODO: Add your specialized code here and/or call the base class
    
    CScrollView::OnBeginPrinting(pDC, pInfo);
}

/*
void CPadView::CalculateMargins(CDC* pDC)   
{   POINT pt;
   // Start by getting the dimensions of the unprintable part of the
   // page (in device units). GETPRINTINGOFFSET will tell us the left
   // and upper unprintable area.
   pDC->Escape(GETPRINTINGOFFSET, 0, NULL, &pt);   
   m_rectMargins.left = pt.x;
   m_rectMargins.top  = pt.y;
   // To get the right and lower unprintable area, we need to take
   // the entire width and height of the paper (GETPHYSPAGESIZE) and
   // subtract everything else.   pDC->Escape(GETPHYSPAGESIZE, 0, NULL, &pt);
   m_rectMargins.right  = pt.x                     // total paper width
                          - pDC->GetDeviceCaps(HORZRES) // printable width
                          - m_rectMargins.left;   // left unprtable margin
   m_rectMargins.bottom = pt.y                     // total paper height
                          - pDC->GetDeviceCaps(VERTRES) // printable ht
                          - m_rectMargins.top;    // rt unprtable margin
   // At this point, m_rectMargins contains the widths of the
   // unprintable regions on all four sides of the page in device units.
   // Convert the Hi-Metric margin values from the Page Setup dialog
   // to device units and subtract the unprintable part we just
   // calculated. Save the results back in m_rectMargins.
   // (2540 == # of Hi-Metric units in an inch)
   pt.x = pDC->GetDeviceCaps(LOGPIXELSX);    // dpi in X direction
   pt.y = pDC->GetDeviceCaps(LOGPIXELSY);    // dpi in Y direction
   m_rectMargins.left   = MulDiv(dlgPageSetup.m_rMargin.left, pt.x, 2540)
                         - m_rectMargins.left;
   m_rectMargins.top    = MulDiv(dlgPageSetup.m_rMargin.top, pt.y, 2540)
                         - m_rectMargins.top;
   m_rectMargins.right  = MulDiv(dlgPageSetup.m_rMargin.right, pt.x, 2540)
                         - m_rectMargins.right;
   m_rectMargins.bottom = MulDiv(dlgPageSetup.m_rMargin.bottom, pt.y,2540)
                         - m_rectMargins.bottom;
   // m_rectMargins now contains the values used to shrink the printable
   // area of the page. Could check m_rectMargins here for negative values
   // to prevent setting margins outside the printable area of the page.
   // Convert to logical units and we're done!   pDC->DPtoLP(m_rectMargins);   
}
*/

void CPrintSheetView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
    // TODO: Add your specialized code here and/or call the base class
    
    CScrollView::OnEndPrinting(pDC, pInfo);
}

void CPrintSheetView::DrawPrintSheetAllSidesToPrinter(CDC* pDC, UINT nCurPage)
{
    CString		 string;
    CPrintSheet* pActPrintSheet;
    CLayout*	 pActLayout;

    CImpManDoc*		  pDoc			 = CImpManDoc::GetDoc();
    UINT			  nPrintSheets	 = pDoc->m_PrintSheetList.GetCount(); 

    int nStartPrintSheet = (nCurPage - 1) * m_nSheetsPerPage;
    POSITION pos = pDoc->m_PrintSheetList.FindIndex(nStartPrintSheet);

    //We begin with drawing in the upper left corner of the view
    pDC->OffsetWindowOrg(0, -m_docSize.cy); 

    for (int i = 0; i < m_nSheetsPerPage && pos != NULL; i ++)                        
    {
        pActPrintSheet = &(pDoc->m_PrintSheetList.GetAt(pos));
        pActLayout = pActPrintSheet->GetLayout();
        if (pActLayout == NULL)
        	continue;
    // Frontside
        pDC->OffsetWindowOrg(0, m_docSize.cy/nPrintSheets);
		string.Format(_T("( %s ) -> %s : %s"), pActPrintSheet->GetNumber(), 
       									   pActLayout->m_strLayoutName, 
       									   pActLayout->m_FrontSide.m_strSideName);
		pDC->TextOut(0, WorldToLP(-20), string);

        DrawPrintSheet(pDC, FRONTSIDE, pActLayout, pActPrintSheet, GetParentFrame());

    // Backside
        pDC->OffsetWindowOrg(-((m_docSize.cx)/2), 0);
        pDC->TextOut(0, WorldToLP(-20), pActLayout->m_BackSide.m_strSideName);

        DrawPrintSheet(pDC, BACKSIDE, pActLayout, pActPrintSheet, GetParentFrame());

    // Go back to left side
        pDC->OffsetWindowOrg((m_docSize.cx)/2, 0);

        pDoc->m_PrintSheetList.GetNext(pos);
    }
}

BOOL CPrintSheetView::OnEraseBkgnd(CDC* pDC) 
{
	m_printViewToolbar1Buttons.ClipButtons(pDC);
	m_printViewToolbar2Buttons.ClipButtons(pDC);
	m_printViewToolbar3Buttons.ClipButtons(pDC);
	m_zoomToolBar.ClipButtons(pDC);

	CRect clientRect;
    GetClientRect(clientRect);

	CRect rcHeader = clientRect; rcHeader.bottom = m_nToolBarHeight;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);

	rcHeader.top = rcHeader.bottom - 3;
	pDC->FillSolidRect(rcHeader, RGB(215,220,225));

	//COLORREF crBackGround = RGB(127,162,207);
	//CBrush brush(crBackGround);
	//clientRect.top  = STATUSBAR_THICKNESS(VERTICAL);
	//clientRect.left = STATUSBAR_THICKNESS(HORIZONTAL);
    //pDC->FillRect(&clientRect, &brush);

	return TRUE;	//return CScrollView::OnEraseBkgnd(pDC);
}
///////////////////////////////////////////////////////////////////
//  message-handlers for setting frame-colors
void CPrintSheetView::OnMarineFrame() 
{
	m_nFrameColorIndex  = 0;
	m_bCheckMarineFrame	= TRUE;
	m_bCheckCyanFrame	= FALSE;
	m_bCheckRedFrame	= FALSE;
	m_bCheckYellowFrame = FALSE;
	
	Invalidate();
	UpdateWindow(); 
}
void CPrintSheetView::OnUpdateMarineFrame(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bCheckMarineFrame);
	pCmdUI->Enable(m_bEnableColorMenues);
}
void CPrintSheetView::OnCyanFrame() 
{
	m_nFrameColorIndex  = 1;
	m_bCheckMarineFrame	= FALSE;
	m_bCheckCyanFrame	= TRUE;
	m_bCheckRedFrame	= FALSE;
	m_bCheckYellowFrame = FALSE;
	
	Invalidate();
	UpdateWindow(); 
}
void CPrintSheetView::OnUpdateCyanFrame(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bCheckCyanFrame);
	pCmdUI->Enable(m_bEnableColorMenues);
}
void CPrintSheetView::OnRedFrame() 
{
	m_nFrameColorIndex  = 2;
	m_bCheckMarineFrame	= FALSE;
	m_bCheckCyanFrame	= FALSE;
	m_bCheckRedFrame	= TRUE;
	m_bCheckYellowFrame = FALSE;
	
	Invalidate();
	UpdateWindow(); 
}
void CPrintSheetView::OnUpdateRedFrame(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bCheckRedFrame);
	pCmdUI->Enable(m_bEnableColorMenues);
}
void CPrintSheetView::OnYellowFrame() 
{
	m_nFrameColorIndex  = 3;
	m_bCheckMarineFrame	= FALSE;
	m_bCheckCyanFrame	= FALSE;
	m_bCheckRedFrame	= FALSE;
	m_bCheckYellowFrame = TRUE;
	
	Invalidate();
	UpdateWindow(); 
}
void CPrintSheetView::OnUpdateYellowFrame(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bCheckYellowFrame);
	pCmdUI->Enable(m_bEnableColorMenues);
}
///////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// bitmap save/restore
void CPrintSheetView::SaveBitmap(CDC* pDC, CBitmap* pBitmap, const CRect& rect)
{
	if ( ! pBitmap)
		return;

	pBitmap->DeleteObject();

 	CDC		  memDC;
	memDC.CreateCompatibleDC(pDC);
	pBitmap->CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(pBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void CPrintSheetView::RestoreBitmap(CDC* pDC, CBitmap* pBitmap, const CRect& rect, int nAlpha)
{
	if ( ! pBitmap)
		return;
	if ( ! pBitmap->GetSafeHandle())
		return;

	CImage img;
	img.Attach((HBITMAP)*pBitmap);
	if (nAlpha > 0)
		img.AlphaBlend(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(), 0, 0, img.GetWidth(), img.GetHeight(), 255);//nAlpha);
	else
	{
		img.AlphaBlend(pDC->GetSafeHdc(), rect.left, rect.top, 5, rect.Height(), rect.left, rect.top, 5, rect.Height(), 255);//nAlpha);
		img.AlphaBlend(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), 5, rect.left, rect.top, rect.Width(), 5, 255);//nAlpha);
		img.AlphaBlend(pDC->GetSafeHdc(), rect.left + rect.Width()-5, rect.top, 5, rect.Height(), rect.left + rect.Width()-5, rect.top, 5, rect.Height(), 255);//nAlpha);
		img.AlphaBlend(pDC->GetSafeHdc(), rect.left, rect.top + rect.Height() - 5, rect.Width(), 5, rect.left, rect.top + rect.Height() - 5, rect.Width(), 5, 255);//nAlpha);
	}
	//if (nAlpha > 0)
	//	img.TransparentBlt(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(), 0, 0, rect.Width(), rect.Height(), 0);
	//else
	//	img.TransparentBlt(pDC->GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(), rect.left, rect.top, rect.Width(), rect.Height(), 0);
	img.Detach();
}

void CPrintSheetView::MoveBitmap(CDC* pDC, CBitmap* pbmBackground, void (*pfnDrawFeedback)(CDC*, CRect), CRect rcBackground, CRect rcForeground)
{
	CRect rcUnion;
	rcUnion.UnionRect(rcBackground, rcForeground);

	///// to avoid flicker effects do 'cut and paste' of bitmaps in memory
	CImage imgUnion;
	imgUnion.Create(rcUnion.Width(), rcUnion.Height(), 32);
	CDC* pDCUnion = CDC::FromHandle(imgUnion.GetDC());
	pDCUnion->BitBlt(0, 0, rcUnion.Width(), rcUnion.Height(), pDC, rcUnion.left, rcUnion.top, SRCCOPY);
	
	CImage imgBackground;
	imgBackground.Attach((HBITMAP)*pbmBackground);
	imgBackground.BitBlt(pDCUnion->GetSafeHdc(), rcBackground.left - rcUnion.left, rcBackground.top - rcUnion.top, rcBackground.Width(), rcBackground.Height(), 0, 0);
	imgBackground.Detach();

	SaveBitmap(pDCUnion, pbmBackground, CRect(CPoint(rcForeground.left - rcUnion.left, rcForeground.top - rcUnion.top), CSize(rcForeground.Width(), rcForeground.Height())) );

	if (pfnDrawFeedback)
		pfnDrawFeedback(pDCUnion, rcForeground - rcUnion.TopLeft());

	imgUnion.BitBlt(pDC->GetSafeHdc(), rcUnion.left, rcUnion.top, rcUnion.Width(), rcUnion.Height(), 0, 0);

	imgUnion.ReleaseDC();
	imgUnion.Destroy();
}

void CPrintSheetView::MoveBitmap(CDC* pDC, CBitmap* pbmBackground, CBitmap* pbmForeground, CRect rcBackground, CRect rcForeground, int nAlpha, void (*pfnDrawFeedback)(CDC*, CRect), CRect rcFeedbackArea)
{
	rcForeground.right += 1; rcForeground.bottom += 1;	// for shadow

	CRect rcUnion;
	rcUnion.UnionRect(rcBackground, rcForeground);

	rcForeground.right -= 1; rcForeground.bottom -= 1;	// for shadow

	///// to avoid flicker effects do 'cut and paste' of bitmaps in memory
	CImage imgUnion;
	imgUnion.Create(rcUnion.Width(), rcUnion.Height(), 32);
	CDC* pDCUnion = CDC::FromHandle(imgUnion.GetDC());
	pDCUnion->BitBlt(0, 0, rcUnion.Width(), rcUnion.Height(), pDC, rcUnion.left, rcUnion.top, SRCCOPY);
	
	CImage imgBackground;
	imgBackground.Attach((HBITMAP)*pbmBackground);
	imgBackground.BitBlt(pDCUnion->GetSafeHdc(), rcBackground.left - rcUnion.left, rcBackground.top - rcUnion.top, rcBackground.Width(), rcBackground.Height(), 0, 0);
	imgBackground.Detach();

	SaveBitmap(pDCUnion, pbmBackground, CRect(CPoint(rcForeground.left - rcUnion.left, rcForeground.top - rcUnion.top), CSize(rcForeground.Width(), rcForeground.Height())) );

	if (pfnDrawFeedback)
		pfnDrawFeedback(pDCUnion, rcFeedbackArea - rcUnion.TopLeft());

	CImage imgForeground;
	imgForeground.Attach((HBITMAP)*pbmForeground);
	imgForeground.AlphaBlend(pDCUnion->GetSafeHdc(), rcForeground.left - rcUnion.left, rcForeground.top - rcUnion.top, rcForeground.Width(), rcForeground.Height(), 
												     0, 0, imgForeground.GetWidth(), imgForeground.GetHeight(), nAlpha);
	imgForeground.Detach();

	//// shadow
	CPen pen(PS_SOLID, 1, DARKGRAY);
	pDCUnion->SelectObject(&pen);
	CRect rcShadow = rcForeground - rcUnion.TopLeft();
	pDCUnion->MoveTo(rcShadow.right + 1, rcShadow.top);
	pDCUnion->LineTo(rcShadow.right + 1, rcShadow.bottom + 1);
	pDCUnion->LineTo(rcShadow.left,		 rcShadow.bottom + 1);
	pen.DeleteObject();
	////

	imgUnion.BitBlt(pDC->GetSafeHdc(), rcUnion.left, rcUnion.top, rcUnion.Width(), rcUnion.Height(), 0, 0);

	imgUnion.ReleaseDC();
	imgUnion.Destroy();
}
///////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////
// background save/restore
void CPrintSheetView::SaveBackground(CDC* pDC, const CRect& rect)
{
	if (m_pBkBitmap)
	{
		m_pBkBitmap->DeleteObject();
		delete m_pBkBitmap;
	}
	m_pBkBitmap = new CBitmap;

 	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	m_pBkBitmap->CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBkBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void CPrintSheetView::RestoreBackground(CDC* pDC, const CRect& rect)
{
	if (!m_pBkBitmap)
		return;

	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBkBitmap);
	pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();

	m_pBkBitmap->DeleteObject();
	delete m_pBkBitmap;
	m_pBkBitmap = NULL;

	ValidateRect(rect);
}


////////// resize tracker /////////////////////////////////////////////////////////////////////////////////////

void CPrintSheetView::DrawResizeTracker(CDC* pDC, CRect rcFrame, COLORREF crColor, CDisplayItem* pActDI)
{
	int nDC = pDC->SaveDC();
	pDC->SetMapMode(MM_TEXT);
	pDC->SetWindowOrg(0,0);
	pDC->SetViewportOrg(0,0);

	COLORREF cr = crColor;
	CPen pen(PS_SOLID, 1, cr);
	pDC->SelectObject(&pen);
	pDC->SelectStockObject(HOLLOW_BRUSH);
	pDC->Rectangle(rcFrame);

	CLayoutObject* pLayoutObject = (pActDI) ? (CLayoutObject*)pActDI->m_pData : NULL;
	BOOL bSmall = TRUE;//(pActDI) ? TRUE : FALSE;

	CRect rcDI2 = rcFrame;
	rcDI2.InflateRect(2,2);

	CRect  rcImg, rcDI;
	CSize  szImg(4, 4);
	CImage img; 
	if ( ! bSmall)
	{
		img.LoadFromResource(theApp.m_hInstance, IDB_RESIZE_HANDLE1);		
		szImg = CSize(img.GetWidth(), img.GetHeight());
		if (img.IsNull())
			return;
	}
	else
	{
		rcFrame.left += 2; rcFrame.right -= 3; rcFrame.top += 2; rcFrame.bottom -= 3; 
	}

	CPen	pen2(PS_SOLID, 1, GUICOLOR_CAPTION);
	CBrush	brush(WHITE);
	pDC->SelectObject(&pen2);
	pDC->SelectObject(&brush);

	rcImg = CRect(CPoint(rcFrame.CenterPoint().x, rcFrame.top),		CSize(1,1)); rcImg.InflateRect(szImg.cx/2, szImg.cy/2);
	if (bSmall) pDC->Rectangle(rcImg); else img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	rcDI = CRect(rcImg.CenterPoint(), rcImg.CenterPoint()); rcDI.InflateRect(10,10);
	CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI2, (void*)MIDDLE_TOP, DISPLAY_ITEM_REGISTRY(CPrintSheetView, TrackerResizeHandle), pLayoutObject, CDisplayItem::ForceRegister); 
	pDI->SetMouseOverCursor(CDisplayItem::cursorSizeNS);

	rcImg = CRect(CPoint(rcFrame.CenterPoint().x, rcFrame.bottom),	CSize(1,1)); rcImg.InflateRect(szImg.cx/2, szImg.cy/2);
	if (bSmall) pDC->Rectangle(rcImg); else img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	rcDI = CRect(rcImg.CenterPoint(), rcImg.CenterPoint()); rcDI.InflateRect(10,10);
	pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI2, (void*)MIDDLE_BOTTOM, DISPLAY_ITEM_REGISTRY(CPrintSheetView, TrackerResizeHandle), pLayoutObject, CDisplayItem::ForceRegister); 
	pDI->SetMouseOverCursor(CDisplayItem::cursorSizeNS);

	rcImg = CRect(CPoint(rcFrame.right, rcFrame.CenterPoint().y),	CSize(1,1)); rcImg.InflateRect(szImg.cx/2, szImg.cy/2);
	if (bSmall) pDC->Rectangle(rcImg); else img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	rcDI = CRect(rcImg.CenterPoint(), rcImg.CenterPoint()); rcDI.InflateRect(10,10);
	pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI2, (void*)RIGHT_MIDDLE, DISPLAY_ITEM_REGISTRY(CPrintSheetView, TrackerResizeHandle), pLayoutObject, CDisplayItem::ForceRegister); 
	pDI->SetMouseOverCursor(CDisplayItem::cursorSizeWE);

	rcImg = CRect(CPoint(rcFrame.left,  rcFrame.CenterPoint().y),	CSize(1,1)); rcImg.InflateRect(szImg.cx/2, szImg.cy/2);
	if (bSmall) pDC->Rectangle(rcImg); else img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	rcDI = CRect(rcImg.CenterPoint(), rcImg.CenterPoint()); rcDI.InflateRect(10,10);
	pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI2, (void*)LEFT_MIDDLE, DISPLAY_ITEM_REGISTRY(CPrintSheetView, TrackerResizeHandle), pLayoutObject, CDisplayItem::ForceRegister); 
	pDI->SetMouseOverCursor(CDisplayItem::cursorSizeWE);

	if ( ! bSmall)
	{
		img.Destroy();
		img.LoadFromResource(theApp.m_hInstance, IDB_RESIZE_HANDLE2);		
		if (img.IsNull())
			return;
	}

	rcImg = CRect(CPoint(rcFrame.left, rcFrame.top),	 CSize(1,1)); rcImg.InflateRect(szImg.cx/2, szImg.cy/2); 
	if (bSmall) pDC->Rectangle(rcImg); else img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	rcDI = CRect(rcImg.CenterPoint(), rcImg.CenterPoint()); rcDI.InflateRect(10,10);
	pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI2, (void*)LEFT_TOP, DISPLAY_ITEM_REGISTRY(CPrintSheetView, TrackerResizeHandle), pLayoutObject, CDisplayItem::ForceRegister); 
	pDI->SetMouseOverCursor(CDisplayItem::cursorSizeNWSE);

	rcImg = CRect(CPoint(rcFrame.right, rcFrame.top),	 CSize(1,1)); rcImg.InflateRect(szImg.cx/2, szImg.cy/2);
	if (bSmall) pDC->Rectangle(rcImg); else img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	rcDI = CRect(rcImg.CenterPoint(), rcImg.CenterPoint()); rcDI.InflateRect(10,10);
	pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI2, (void*)RIGHT_TOP, DISPLAY_ITEM_REGISTRY(CPrintSheetView, TrackerResizeHandle), pLayoutObject, CDisplayItem::ForceRegister); 
	pDI->SetMouseOverCursor(CDisplayItem::cursorSizeNESW);

	rcImg = CRect(CPoint(rcFrame.right, rcFrame.bottom), CSize(1,1)); rcImg.InflateRect(szImg.cx/2, szImg.cy/2);
	if (bSmall) pDC->Rectangle(rcImg); else img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	rcDI = CRect(rcImg.CenterPoint(), rcImg.CenterPoint()); rcDI.InflateRect(10,10);
	pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI2, (void*)RIGHT_BOTTOM, DISPLAY_ITEM_REGISTRY(CPrintSheetView, TrackerResizeHandle), pLayoutObject, CDisplayItem::ForceRegister); 
	pDI->SetMouseOverCursor(CDisplayItem::cursorSizeNWSE);

	rcImg = CRect(CPoint(rcFrame.left, rcFrame.bottom),	 CSize(1,1)); rcImg.InflateRect(szImg.cx/2, szImg.cy/2);
	if (bSmall) pDC->Rectangle(rcImg); else img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	rcDI = CRect(rcImg.CenterPoint(), rcImg.CenterPoint()); rcDI.InflateRect(10,10);
	pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI2, (void*)LEFT_BOTTOM, DISPLAY_ITEM_REGISTRY(CPrintSheetView, TrackerResizeHandle), pLayoutObject, CDisplayItem::ForceRegister); 
	pDI->SetMouseOverCursor(CDisplayItem::cursorSizeNESW);

	pDC->RestoreDC(nDC);

	pen.DeleteObject();
	pen2.DeleteObject();
	brush.DeleteObject();
}
//////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////
// mouse events

void CPrintSheetView::OnLButtonDown(UINT nFlags, CPoint point) 
{
    SetFocus();	// In order to deactivate eventually active InplaceEdit

	if ( ( (GetKeyState(VK_SHIFT) & 0x8000) && ! ((GetKeyState(VK_CONTROL) & 0x8000)) ) || HandActive())
	{
		m_lastHandPos = point;
		return;
	}

    if (ViewMode() == ID_ZOOM)	
		ZoomWindow(point);
	else
		if (m_ColumnBars.m_bMouseIsOver)
			m_ColumnBars.BeginDrag(point, this);
		else
		{
			if (TrimToolActive()) 
        		DoTrimmings(point);
			else
			{
				if (AlignSectionActive())
					DoAlignSection(point);
				else
				{
					if (IsOpenMarkSetDlg() || IsOpenDlgNewMark())
						DoPositionMarks(point);
					else
					{
						if (IsOpenDlgComponentImposer())
						{
							g_bResizeHandleTracked = FALSE;
							m_DisplayList.OnLButtonDown(point);
							if ( ! g_bResizeHandleTracked)
								if (m_DisplayList.GetNumSelectedItems() < 1)
									GetDlgComponentImposer()->SelectPlaceArea(point);
						}
						else
						{
							CDlgPDFTargetProps* pDlg = (GetDlgOutputMedia()) ? GetDlgOutputMedia()->GetDlgPDFTargetProps() : NULL;
							if (pDlg)
								DoPositionOutputMedia(point);
							else
							{
								if (IsOpenDlgAlignObjects())
									SetAlignLayoutObjects(point);
								else
									//if (IsOpenDlgLayoutObjectGeometry())
									//	DoPositionLayoutObjects(point);
									//else
									{
										m_DisplayList.OnLButtonDown(point); // pass message to display list
										if (MarksViewActive())
										{
											CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
											if (pView)
												pView->ShowPrintSheetViewSelections();
										}
									}
							}
						}
					}
				}
			}
		}

    CScrollView::OnLButtonDown(nFlags, point);
}

void CPrintSheetView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (m_ColumnBars.m_bDragging)
		m_ColumnBars.OnDrop(point, this);

	if (GetKeyState(VK_SHIFT) < 0)
	{
		InvalidateRect(m_LeftStatusBar);	// update text positions in status bars
		InvalidateRect(m_UpperStatusBar);
		UpdateWindow();
	}

	CScrollView::OnLButtonUp(nFlags, point);
}

void CPrintSheetView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if (nFlags & MK_CONTROL)
	{
		SetCursor(theApp.m_CursorMagnifyDecrease);
		ZoomWindow(point); 
		return;
	}

	if (ViewMode() != ID_VIEW_ALL_SHEETS)
		if (m_DisplayList.BeginDrag(10))
		{
			SetViewMode(ID_ZOOM);
			SetCursor(theApp.m_CursorMagnifyIncrease);
			ZoomWindow(point, TRUE);
			SetCursor(theApp.m_CursorStandard);
			CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
			if (pFrame)
				pFrame->m_nViewMode = ID_VIEW_SINGLE_SHEETS;
			return;
		}

	m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list
	CPoint pt = point;
	ClientToScreen(&pt);
	OnContextMenu(NULL, pt);
}

void CPrintSheetView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	if (nFlags & MK_CONTROL)
		if (GetCursor() == theApp.m_CursorMagnifyDecrease)
		{
			ZoomWindow(point);
			SetCursor(theApp.m_CursorStandard);
			SetViewMode(ID_VIEW_SINGLE_SHEETS);
		}
}

void CPrintSheetView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
    OnLButtonDown(nFlags, point);	// Handle second click 
    
    CScrollView::OnLButtonDblClk(nFlags, point);
}

void CPrintSheetView::ZoomWindow(const CPoint& rPoint, BOOL bRButton)
{
	if (GetCursor() == theApp.m_CursorMagnifyDecrease)
	{
		if ( ! m_zoomSteps.GetCount())
		{
			SetViewMode(ID_VIEW_SINGLE_SHEETS);
			Invalidate();
			CalcDocExtents();
			CalcViewportExtents(TRUE);	// TRUE = reset
			CalcScrollSizes();	
			SetScrollSizes(MM_TEXT, m_sizeTotal, m_sizePage, m_sizeLine);
		}
		else
		{
			m_viewportSize   = m_zoomSteps.GetTail().m_viewportSize;
			CPoint scrollPos = m_zoomSteps.GetTail().m_scrollPos;
			m_zoomSteps.RemoveTail();
			CalcViewportExtents();
			CalcScrollSizes();
			SetScrollSizes(MM_TEXT, m_sizeTotal, m_sizePage, m_sizeLine);
			Invalidate();
       		ScrollToPosition(scrollPos);
		}
	}
	else
	{
		BOOL		   bTracked = FALSE;
	    CRectTracker   tracker;
	    CMyRectTracker rbTracker;
		bTracked = (bRButton) ? rbTracker.TrackRubberBand(this, rPoint, TRUE) : tracker.TrackRubberBand(this, rPoint, TRUE);

		if (bTracked)
		{
			CRect trackRect = (bRButton) ? rbTracker.m_rect : tracker.m_rect;
			trackRect.NormalizeRect(); 

			CRect clientRect;
			GetClientRect(&clientRect);

			float fRatioX	= (float)((clientRect.Width()  - STATUSBAR_THICKNESS(HORIZONTAL, TRUE)) / (float)trackRect.Width()); 
			float fRatioY	= (float)((clientRect.Height() - STATUSBAR_THICKNESS(VERTICAL,   TRUE)) / (float)trackRect.Height());
			float fMinRatio = min(fRatioX, fRatioY);

			CSize scrollSize = GetTotalSize();
			scrollSize.cx = (long)((float)scrollSize.cx * fMinRatio);
			scrollSize.cy = (long)((float)scrollSize.cy * fMinRatio);
			int bSizeValid;
			switch (theApp.m_dwPlatformId)
			{
			case VER_PLATFORM_WIN32_NT:		 bSizeValid = (scrollSize.cx < LONG_MAX && scrollSize.cy < LONG_MAX) ? TRUE : FALSE; break;
			case VER_PLATFORM_WIN32_WINDOWS: bSizeValid = (scrollSize.cx < SHRT_MAX && scrollSize.cy < SHRT_MAX) ? TRUE : FALSE; break;
			default:						 bSizeValid = (scrollSize.cx < SHRT_MAX && scrollSize.cy < SHRT_MAX) ? TRUE : FALSE; break;
			}
			if (bSizeValid)
			{
				CPoint scrollPos = GetScrollPosition();

				CZoomState zoomState(m_viewportSize, scrollPos);	// save old status
				m_zoomSteps.AddTail(zoomState);

				scrollPos += CSize(trackRect.left - STATUSBAR_THICKNESS(HORIZONTAL, TRUE), trackRect.top - STATUSBAR_THICKNESS(VERTICAL, TRUE));

				CalcDocExtents();
				m_viewportSize -= CSize((int)((float)(2 * VIEWPORT_BORDER(SHOW_MEASURING, HORIZONTAL, TRUE))), (int)((float)(2 * VIEWPORT_BORDER(SHOW_MEASURING, VERTICAL, TRUE))) );
				if (fRatioX < fRatioY)
				{
					m_viewportSize.cx = (int)((float)m_viewportSize.cx * fRatioX);
					m_viewportSize.cy = (int)((float)m_docSize.cy * ((float)m_viewportSize.cx / (float)m_docSize.cx));
				}
				else
				{
					m_viewportSize.cy = (int)((float)m_viewportSize.cy * fRatioY);
					m_viewportSize.cx = (int)((float)m_docSize.cx * ((float)m_viewportSize.cy / (float)m_docSize.cy));
				}
				m_viewportSize.cx += STATUSBAR_THICKNESS(HORIZONTAL, TRUE);
				CalcViewportExtents();
				CalcScrollSizes();
				SetScrollSizes(MM_TEXT, m_sizeTotal, m_sizePage, m_sizeLine);

				Invalidate();

				trackRect.left	 = (long)((float)trackRect.left   * fMinRatio);
				trackRect.right  = (long)((float)trackRect.right  * fMinRatio);
				trackRect.top	 = (long)((float)trackRect.top	  * fMinRatio);
				trackRect.bottom = (long)((float)trackRect.bottom * fMinRatio);

        		scrollPos.x = (long)((float)scrollPos.x * fMinRatio - ((clientRect.Width()  - STATUSBAR_THICKNESS(HORIZONTAL, TRUE) - trackRect.Width() )/2) - STATUSBAR_THICKNESS(HORIZONTAL, TRUE) * fMinRatio);
        		scrollPos.y = (long)((float)scrollPos.y * fMinRatio - ((clientRect.Height() - STATUSBAR_THICKNESS(VERTICAL,   TRUE) - trackRect.Height())/2) - STATUSBAR_THICKNESS(VERTICAL,   TRUE) * fMinRatio);
        		ScrollToPosition(scrollPos);
			}
		}
	}
}

void CPrintSheetView::OnMouseMove(UINT nFlags, CPoint point) 
{
	//if (GetFocus() != this)
	//	SetFocus();

	m_DisplayList.OnMouseMove(point);

	m_ColumnBars.OnMouseMove(nFlags&MK_LBUTTON, point, this);

	if ( (GetKeyState(VK_SHIFT) < 0) || HandActive()) 
		if (GetKeyState(VK_LBUTTON) < 0)
		{
			CSize szScroll = CSize(m_lastHandPos - point);
			szScroll.cx *= 4; szScroll.cy *= 4;
			OnScrollBy(szScroll);
			m_lastHandPos = point;
			return;
		}

	if ( ! MarksViewActive() && ! GetDlgOutputMedia()->GetDlgPDFTargetProps() && 
		 ! IsOpenDlgAlignObjects() && ! IsOpenDlgLayoutObjectGeometry() && ! AlignSectionActive() )
		return;

	//if (ShowBitmap() || ShowExposures() || (ViewMode() == ID_VIEW_ALL_SHEETS) )
	if (ViewMode() == ID_VIEW_ALL_SHEETS)
		return;

	//if (GetParentFrame())
	//	if (GetParentFrame()->GetActiveView() != this)
	//		GetParentFrame()->SetActiveView(this);

	if (GetDlgOutputMedia()->GetDlgPDFTargetProps())
		OnMouseMoveOutputMedia(point);
	else
		if (IsOpenDlgAlignObjects())
			OnMouseMoveLayoutObject(point);
		else
			if (IsOpenDlgLayoutObjectGeometry())
			{
				if (IsOpenMarkSetDlg() || IsOpenDlgNewMark())
					OnMouseMoveControlMarks(point);
				//else
				//	OnMouseMoveLayoutObject(point);
			}
			else
				if (AlignSectionActive())
					OnMouseMoveAlignSection(point);

	CScrollView::OnMouseMove(nFlags, point);
}

void CPrintSheetView::OnMouseMoveLayoutObject(CPoint point)
{
	CClientDC dc(this);
	ClipStatusBars(&dc);

	int			 nSide = -1;
	BOOL		 bBorder;
	CDisplayItem di = (IsOpenDlgAlignObjects()) ? GetReflineDI(point, nSide, bBorder, GetDlgAlignObjects()) : GetReflineDI(point, nSide, bBorder, GetDlgLayoutObjectGeometry());

	if ( ! di.IsNull() && (nSide != -1) )
	{
		CRect clientRect;
		GetClientRect(clientRect);
		int	  nLeft = clientRect.left, nTop = clientRect.top, nRight = clientRect.right, nBottom = clientRect.bottom;

		CRect  rect = (bBorder) ? di.m_AuxAuxRect : di.m_AuxRect;
//		if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject))
		if (0)
		{
			nLeft = rect.left - 15; nTop = rect.top - 15; nRight = rect.right + 15; nBottom = rect.bottom + 15;
		}

		CPoint pt[2];
		switch (nSide)
		{
		case LEFT:		pt[0] = CPoint(rect.left,			 nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;	
		case RIGHT:		pt[0] = CPoint(rect.right - 1,		 nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;
		case XCENTER:	pt[0] = CPoint(rect.CenterPoint().x, nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;
		case BOTTOM:	pt[0] = CPoint(nLeft, rect.bottom - 2);			pt[1] = CPoint(nRight,  pt[0].y);	break;
		case TOP:		pt[0] = CPoint(nLeft, rect.top    - 1);			pt[1] = CPoint(nRight,  pt[0].y);	break;
		case YCENTER:	pt[0] = CPoint(nLeft, rect.CenterPoint().y);	pt[1] = CPoint(nRight,  pt[0].y);	break;
		}

		rect = CRect(pt[0], pt[1]);
		rect.NormalizeRect();
		rect.InflateRect(5, 5);

		if (rect != m_rectBk)
		{
			if (m_pBkBitmap)	
				RestoreBackground(&dc, m_rectBk);

			SaveBackground(&dc, m_rectBk = rect);

			COLORREF crPenColor = BLACK;
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate))
				crPenColor = DARKGRAY;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet))
				crPenColor = LIGHTGRAY;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, GroupTracker))
				crPenColor = RED;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject))
				crPenColor = LIGHTBLUE;
	
			CPen pen(PS_DASH, 1, crPenColor);
			dc.SelectObject(&pen);
			dc.MoveTo(pt[0]);
			dc.LineTo(pt[1]);
			pen.DeleteObject();
		}
	}
	else
		if (m_pBkBitmap)	
		{
			RestoreBackground(&dc, m_rectBk);
			m_rectBk = CRect(0,0,0,0);
		}
}


void CPrintSheetView::OnMouseMoveControlMarks(CPoint point)
{
	CClientDC dc(this);
	ClipStatusBars(&dc);

	CDlgMarkSet* pDlg = NULL;
	if (GetDlgMarkSet())
		if (GetDlgMarkSet()->m_hWnd)
			pDlg = GetDlgMarkSet();

	int					  nSide = -1;
	BOOL				  bBorder;
	CDisplayItem di = GetReflineDI(point, nSide, bBorder, pDlg);

	CDlgLayoutObjectGeometry* pDlgLayoutObjectGeometry = GetDlgLayoutObjectGeometry();
	if (pDlgLayoutObjectGeometry)
		if (pDlgLayoutObjectGeometry->m_nMarkAnchor != REFLINE_ANCHOR)
			return;

	int nEdge = (pDlgLayoutObjectGeometry) ? pDlgLayoutObjectGeometry->GetMarkEdge() : 0;

	BOOL bDrawHorizontal = FALSE;
	BOOL bDrawVertical	 = FALSE;
	switch (nEdge)
	{
	case 0:			if (pDlgLayoutObjectGeometry)
					{
						bDrawHorizontal = (pDlgLayoutObjectGeometry->m_bVariableHeight) ? FALSE : TRUE;
						bDrawVertical	= (pDlgLayoutObjectGeometry->m_bVariableWidth)  ? FALSE : TRUE; 
					}
					break;
	case LEFT:
	case RIGHT:		bDrawVertical	= TRUE; break;
	case BOTTOM:
	case TOP:		bDrawHorizontal = TRUE; break;
	}

	switch (nSide)
	{
	case LEFT:		
	case RIGHT:		
	case XCENTER:	if ( ! bDrawVertical)	return;	
					break;
	case BOTTOM:	
	case TOP:		
	case YCENTER:	if ( ! bDrawHorizontal)	return;	
					break;
	}

	if ( ! di.IsNull() && (nSide != -1) )
	{
		CRect clientRect;
		GetClientRect(clientRect);
		int	  nLeft = clientRect.left, nTop = clientRect.top, nRight = clientRect.right, nBottom = clientRect.bottom;

		CRect  rect = (bBorder) ? di.m_AuxAuxRect : di.m_AuxRect;
		if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark))
		{
			nLeft = rect.left - 15; nTop = rect.top - 15; nRight = rect.right + 15; nBottom = rect.bottom + 15;
		}

		CPoint pt[2];
		switch (nSide)
		{
		case LEFT:		pt[0] = CPoint(rect.left,			 nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;	
		case RIGHT:		pt[0] = CPoint(rect.right - 1,		 nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;
		case XCENTER:	pt[0] = CPoint(rect.CenterPoint().x, nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;
		case BOTTOM:	pt[0] = CPoint(nLeft, rect.bottom - 2);			pt[1] = CPoint(nRight,  pt[0].y);	break;
		case TOP:		pt[0] = CPoint(nLeft, rect.top    - 1);			pt[1] = CPoint(nRight,  pt[0].y);	break;
		case YCENTER:	pt[0] = CPoint(nLeft, rect.CenterPoint().y);	pt[1] = CPoint(nRight,  pt[0].y);	break;
		}

		rect = CRect(pt[0], pt[1]);
		rect.NormalizeRect();
		rect.InflateRect(5, 5);

		if (rect != m_rectBk)
		{
			if (m_pBkBitmap)	
				RestoreBackground(&dc, m_rectBk);

			SaveBackground(&dc, m_rectBk = rect);

			COLORREF crPenColor = BLACK;
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate))
				crPenColor = DARKGRAY;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet))
				crPenColor = LIGHTGRAY;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, BoundingBoxFrame))
				crPenColor = RGB(0,200,0);
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject))
			{
				if ( ((CLayoutObject*)di.m_pData)->m_nType == CLayoutObject::Page)
					crPenColor = LIGHTBLUE;
				else
					crPenColor = LIGHTRED;
			}
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark))
				crPenColor = LIGHTRED;

			CPen pen(PS_DASH, 1, crPenColor);
			dc.SelectObject(&pen);
			dc.MoveTo(pt[0]);
			dc.LineTo(pt[1]);
			pen.DeleteObject();
		}
	}
	else
		if (m_pBkBitmap)	
		{
			RestoreBackground(&dc, m_rectBk);
			m_rectBk = CRect(0,0,0,0);
		}
}


void CPrintSheetView::OnMouseMoveOutputMedia(CPoint point)
{
	CClientDC dc(this);
	ClipStatusBars(&dc);

	CDlgPDFTargetProps* pDlg = GetDlgOutputMedia()->GetDlgPDFTargetProps();
	int					nSide = -1;
	BOOL				bBorder;
	CDisplayItem		di = GetReflineDI(point, nSide, bBorder, pDlg);

	if ( ! di.IsNull() && (nSide != -1) )
	{
		CRect clientRect;
		GetClientRect(clientRect);
		int	  nLeft = clientRect.left, nTop = clientRect.top, nRight = clientRect.right, nBottom = clientRect.bottom;

		CRect  rect = (bBorder) ? di.m_AuxAuxRect : di.m_AuxRect;
		if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia))
		{
			nLeft = rect.left - 15; nTop = rect.top - 15; nRight = rect.right + 15; nBottom = rect.bottom + 15;
		}

		CPoint pt[2];
		switch (nSide)
		{
		case LEFT:		pt[0] = CPoint(rect.left,			 nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;	
		case RIGHT:		pt[0] = CPoint(rect.right - 1,		 nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;
		case XCENTER:	pt[0] = CPoint(rect.CenterPoint().x, nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;
		case BOTTOM:	pt[0] = CPoint(nLeft, rect.bottom - 2);			pt[1] = CPoint(nRight,  pt[0].y);	break;
		case TOP:		pt[0] = CPoint(nLeft, rect.top    - 1);			pt[1] = CPoint(nRight,  pt[0].y);	break;
		case YCENTER:	pt[0] = CPoint(nLeft, rect.CenterPoint().y);	pt[1] = CPoint(nRight,  pt[0].y);	break;
		}

		rect = CRect(pt[0], pt[1]);
		rect.NormalizeRect();
		rect.InflateRect(5, 5);

		if (rect != m_rectBk)
		{
			if (m_pBkBitmap)	
				RestoreBackground(&dc, m_rectBk);

			SaveBackground(&dc, m_rectBk = rect);

			COLORREF crPenColor = BLACK;
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate))
				crPenColor = DARKGRAY;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet))
				crPenColor = LIGHTGRAY;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject))
				crPenColor = LIGHTBLUE;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark))
				crPenColor = LIGHTRED;

			CPen pen(PS_DASH, 1, crPenColor);
			dc.SelectObject(&pen);
			dc.MoveTo(pt[0]);
			dc.LineTo(pt[1]);
			pen.DeleteObject();
		}
	}
	else
		if (m_pBkBitmap)	
		{
			RestoreBackground(&dc, m_rectBk);
			m_rectBk = CRect(0,0,0,0);
		}
}

void CPrintSheetView::OnMouseMoveAlignSection(CPoint point)
{
	CClientDC dc(this);
	ClipStatusBars(&dc);

	int				nSide = -1;
	BOOL			bBorder;
	CDisplayItem	di = GetReflineDI(point, nSide, bBorder, FALSE);

	if ( ! di.IsNull() && (nSide != -1) )
	{
		CRect clientRect;
		GetClientRect(clientRect);
		int	  nLeft = clientRect.left, nTop = clientRect.top, nRight = clientRect.right, nBottom = clientRect.bottom;
		CRect rect  = (bBorder) ? di.m_AuxAuxRect : di.m_AuxRect;

		CPoint pt[2];
		switch (nSide)
		{
		case LEFT:		pt[0] = CPoint(rect.left,			 nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;	
		case RIGHT:		pt[0] = CPoint(rect.right - 1,		 nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;
		case XCENTER:	pt[0] = CPoint(rect.CenterPoint().x, nTop);		pt[1] = CPoint(pt[0].x,	nBottom);	break;
		case BOTTOM:	pt[0] = CPoint(nLeft, rect.bottom - 2);			pt[1] = CPoint(nRight,  pt[0].y);	break;
		case TOP:		pt[0] = CPoint(nLeft, rect.top    - 1);			pt[1] = CPoint(nRight,  pt[0].y);	break;
		case YCENTER:	pt[0] = CPoint(nLeft, rect.CenterPoint().y);	pt[1] = CPoint(nRight,  pt[0].y);	break;
		}

		rect = CRect(pt[0], pt[1]);
		rect.NormalizeRect();
		rect.InflateRect(5, 5);

		if (rect != m_rectBk)
		{
			if (m_pBkBitmap)	
				RestoreBackground(&dc, m_rectBk);

			SaveBackground(&dc, m_rectBk = rect);

			COLORREF crPenColor = BLACK;
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, WebYRefLine1))
				crPenColor = DARKGRAY;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, WebYRefLine2))
				crPenColor = DARKGRAY;
			else
			if (di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject))
				crPenColor = LIGHTBLUE;

			CPen pen(PS_DASH, 1, crPenColor);
			dc.SelectObject(&pen);
			dc.MoveTo(pt[0]);
			dc.LineTo(pt[1]);
			pen.DeleteObject();
		}
	}
	else
		if (m_pBkBitmap)	
		{
			RestoreBackground(&dc, m_rectBk);
			m_rectBk = CRect(0,0,0,0);
		}
}


CColumnBars::CColumnBars()
{
	m_bMouseIsOver = FALSE;
	m_bDragging	   = FALSE;
}

void CColumnBars::OnMouseMove(BOOL bLButton, CPoint& rPoint, class CPrintSheetView* pView)
{
	if (bLButton && m_bDragging)
	{
		OnDrag(rPoint, pView);
		return;
	}

	if (m_bDragging)
		return;

	m_bMouseIsOver = FALSE;
	m_nDragIndex   = -1;
	for (int i = 0; i < m_rectList.GetSize(); i++)
		if (m_rectList[i].PtInRect(rPoint))
		{
			m_nDragIndex = i;
			break;
		}
	if (m_nDragIndex < 0)	// first separator is not allowed to be dragged
		return;
	m_bMouseIsOver = TRUE;
}

void CColumnBars::BeginDrag(CPoint& /*rPoint*/, CPrintSheetView* pView)
{
	m_bDragging	= TRUE;

	m_rectListOld.RemoveAll();
	CClientDC dc(pView);

	for (int i = 0; i < m_rectList.GetSize(); i++)
	{
		CRect dragRect = m_rectList[i];
		int	  nX	   = dragRect.CenterPoint().x;
		dragRect.left  = nX - 2;
		dragRect.right = nX + 2;
		dc.DrawDragRect(&dragRect, CSize(2,2), NULL, CSize(0,0));

		m_rectListOld.Add(m_rectList[i]);
	}
}

void CColumnBars::OnDrag(CPoint& rPoint, CPrintSheetView* pView)
{
	int nDiff = (rPoint.x - pView->STATUSBAR_THICKNESS(HORIZONTAL, TRUE)/2) / (m_nDragIndex + 1);
	if (nDiff < 50)
		return;
	for (int i = 0; i < m_rectList.GetSize(); i++)
	{
		m_rectList[i].left  = nDiff * (i + 1);
		m_rectList[i].right = nDiff * (i + 1) + pView->STATUSBAR_THICKNESS(HORIZONTAL, TRUE);
		if (m_rectList[i] != m_rectListOld[i])
		{
			CClientDC dc(pView);
			CRect dragRect	  = m_rectList[i];
			int	  nX		  = dragRect.CenterPoint().x;
			dragRect.left	  = nX - 2;
			dragRect.right	  = nX + 2;
			CRect dragRectOld = m_rectListOld[i];
			nX				  = dragRectOld.CenterPoint().x;
			dragRectOld.left  = nX - 2;
			dragRectOld.right = nX + 2;
			dc.DrawDragRect(&dragRect, CSize(2,2), &dragRectOld, CSize(2,2));
			m_rectListOld[i] = m_rectList[i];
		}
	}
}

void CColumnBars::OnDrop(CPoint& rPoint, CPrintSheetView* pView)
{
	m_bMouseIsOver = FALSE;
	m_bDragging	   = FALSE;

	int nDiff = (rPoint.x - pView->STATUSBAR_THICKNESS(HORIZONTAL, TRUE)/2) / (m_nDragIndex + 1);
	if (nDiff < 50)
		nDiff = 50;
	nDiff	 -= 3 * pView->VIEWPORT_CELLBORDER(HORIZONTAL, TRUE);
	pView->m_viewportCellWidth = nDiff;

	m_rectListOld.RemoveAll();
	m_rectList.RemoveAll();

	pView->m_DisplayList.Invalidate();

	pView->Invalidate();
	pView->OnUpdate(NULL, -1, NULL);
	pView->UpdateWindow();
	
	OnMouseMove(FALSE, rPoint, pView);
}

void CPrintSheetView::OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{
	CView::OnNcCalcSize(bCalcValidRects, lpncsp);

	//if (bCalcValidRects)
	//{
	//	if ( (ViewMode() != ID_VIEW_ALL_SHEETS) && ( ((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionPrintSheets) )
	//		lpncsp->rgrc[0].bottom -= 18;		
	//}
}

void CPrintSheetView::OnNcPaint()
{
	CView::OnNcPaint();

	//if ( (ViewMode() != ID_VIEW_ALL_SHEETS) && ( ((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionPrintSheets) )
	//{
	//	CDC* pDC = GetWindowDC();

	//	CRect rcFrame;
	//	GetClientRect(rcFrame); 
	//	rcFrame.top = rcFrame.bottom; rcFrame.bottom += 18;

	//	Graphics graphics(pDC->m_hDC);
	//	Rect rc(rcFrame.left, rcFrame.top, rcFrame.Width(), rcFrame.Height());
	//	LinearGradientBrush linearBrush(rc, Color::Color(255,251,240), Color::Color(240,235,225), 0);
	//	graphics.FillRectangle(&linearBrush, rc);

	//	Font font(_T("Tahoma"), 8);
	//	SolidBrush brush(Color::Gray);
	//	rc = Rect(rcFrame.left + 30, rcFrame.top + 4, 18, 8);
	//	graphics.FillRectangle(&SolidBrush(Color::Color(227,245,173)), rc);	graphics.DrawRectangle(&Pen(Color::LightGray), rc);
	//	graphics.DrawString(_T("Vor-/Nachfalz"), -1, &font, PointF((float)(rc.GetRight() + 5), (float)(rc.GetTop() - 3)), &brush );

	//	rc.Offset(120, 0);
	//	graphics.FillRectangle(&SolidBrush(Color::Color(200,230,255)), rc);	graphics.DrawRectangle(&Pen(Color::LightGray), rc);
	//	graphics.DrawString(_T("Beschnitt"), -1, &font, PointF((float)(rc.GetRight() + 5), (float)(rc.GetTop() - 3)), &brush );

	//	rc.Offset(100, 0);
	//	graphics.FillRectangle(&SolidBrush(Color::Color(255,255,160)), rc);	graphics.DrawRectangle(&Pen(Color::LightGray), rc);
	//	graphics.DrawString(_T("Fr�srand/Bundsteg"), -1, &font, PointF((float)(rc.GetRight() + 5), (float)(rc.GetTop() - 3)), &brush );

	//	ReleaseDC(pDC);
	//}
}

void CPrintSheetView::UndoSave()
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_undoBuffer.SeekToBegin();
	CArchive archive(&m_undoBuffer, CArchive::store);
	m_undoBuffer.SetFilePath(_T("UndoRestore"));

	pDoc->Serialize(archive);

	archive.Close();

	SetUndoActive(TRUE);
}

void CPrintSheetView::UndoRestore()
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (m_undoBuffer.GetLength() == 0)
		return;

	theApp.ViewsResetDisplayList(RUNTIME_CLASS(CPrintSheetPlanningView));

	m_undoBuffer.SeekToBegin();
	CArchive archive(&m_undoBuffer, CArchive::load);
	m_undoBuffer.SetFilePath(_T("UndoRestore"));

	pDoc->DeleteContents();
	pDoc->Serialize(archive);

	archive.Close();
	m_undoBuffer.SetLength(0);

	SetUndoActive(FALSE);

	m_DisplayList.Invalidate();
	m_DisplayList.Clear();

	pDoc->SetModifiedFlag(FALSE);

	pDoc->m_PageTemplateList.InvalidateProductRefs();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));	// reorganize pointers to foldsheets in table
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData), NULL, 0, NULL, TRUE);	// reorganize pointers to foldsheets in table
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		 NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));	
	theApp.UpdateView(RUNTIME_CLASS(CProductsView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CProdDataPrintToolsView),		 NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CProdDataPrintToolsView));	

	if (IsOpenDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);

	CPrintComponentsView*		pComponentsView	= (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
	CPrintSheetNavigationView*	pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));	
	if (pComponentsView && pNavigationView)
	{
		CPrintComponent component = pComponentsView->GetActComponent();
		if ( ! component.IsNull())
		{
			CPrintSheet* pPrintSheet = component.GetFirstPrintSheet();
			pNavigationView->SetActPrintSheet(pPrintSheet, TRUE);
		}
		else
		{
			pNavigationView->Invalidate();
			pNavigationView->UpdateWindow();
			CPrintSheet* pPrintSheet = GetActPrintSheet();
			component = (pPrintSheet) ? pPrintSheet->GetFirstPrintComponent() : CPrintComponent();
			pComponentsView->SetActComponent(component);
		}
	}

	OnUpdate(NULL, 0, NULL);	
	Invalidate();
	UpdateWindow();
}



CDlgPDFOutputMediaSettings* CPrintSheetView::GetDlgOutputMedia()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->m_pDlgOutputMedia;
	else
		return NULL;
}

CDlgMoveObjects* CPrintSheetView::GetDlgMoveObjects()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_pDlgMoveObjects;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_pDlgMoveObjects;
		else
			return NULL;
}

CDlgLayoutObjectProperties* CPrintSheetView::GetDlgNewMark()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->GetDlgNewMark();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetDlgNewMark();
		else
			return NULL;
}

CDlgMarkSet* CPrintSheetView::GetDlgMarkSet()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->GetMarkSetDlg();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetMarkSetDlg();
		else
			return NULL;
}

CDlgLayoutObjectGeometry* CPrintSheetView::GetDlgLayoutObjectGeometry()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->GetDlgLayoutObjectGeometry();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetDlgLayoutObjectGeometry();
		else
			return NULL;
}

CDlgLayoutObjectProperties* CPrintSheetView::GetDlgLayoutObjectProperties()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_pDlgLayoutObjectProperties;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_pDlgLayoutObjectProperties;
		else
			return NULL;
}

CDlgSelectFoldSchemePopup* CPrintSheetView::GetDlgFoldSchemeSelector()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return &((CNewPrintSheetFrame*)pFrame)->m_dlgFoldSchemeSelector;
	else
		return NULL;
}

CDlgComponentImposer* CPrintSheetView::GetDlgComponentImposer()
{
	CPrintSheetViewStrippingData* pView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	if (pView)
		return &pView->m_layoutDataWindow.m_dlgComponentImposer;

	return NULL;
}

CDlgAlignObjects* CPrintSheetView::GetDlgAlignObjects()
{
	CPrintSheetViewStrippingData* pView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	if (pView)
		return &pView->m_layoutDataWindow.m_dlgAlignObjects;

	return NULL;
}

BOOL CPrintSheetView::IsOpenDlgLayoutObjectGeometry()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->IsOpenDlgLayoutObjectGeometry();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->IsOpenDlgLayoutObjectGeometry();
		else
			return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgPDFObjectContent()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->IsOpenDlgPDFObjectContent();
	else
		return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgObjectContent()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->IsOpenDlgObjectContent();
	else
		return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgLayoutObjectProperties()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return FALSE;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->IsOpenDlgLayoutObjectProperties();
		else
			return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgNewMarkGeometry()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->IsOpenDlgNewMarkGeometry();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->IsOpenDlgNewMarkGeometry();
		else
			return FALSE;
}

BOOL CPrintSheetView::IsOpenMarkSetDlg()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->IsOpenMarkSetDlg();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->IsOpenMarkSetDlg();
		else
			return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgNewMark()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->IsOpenDlgNewMark();
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->IsOpenDlgNewMark();
		else
			return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgMaskSettings()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->IsOpenDlgMaskSettings();
	else
		return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgShinglingAssistent()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return (((CNewPrintSheetFrame*)pFrame)->m_dlgShinglingAssistent.m_hWnd) ? TRUE : FALSE;
	else
		return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgFoldSchemeSelector()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
	{
		if (((CNewPrintSheetFrame*)pFrame)->m_dlgFoldSchemeSelector.m_hWnd)
			//if (((CNewPrintSheetFrame*)pFrame)->m_dlgAddFlatProducts.IsWindowVisible())
				return TRUE;
	}
	return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgComponentImposer()
{
	CPrintSheetViewStrippingData* pView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	if (pView)
	{
		if (pView->m_layoutDataWindow.m_dlgComponentImposer.m_hWnd)
			return TRUE;
	}
	return FALSE;
}

BOOL CPrintSheetView::IsOpenDlgAlignObjects()
{
	CPrintSheetViewStrippingData* pView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	if (pView)
	{
		if (pView->m_layoutDataWindow.m_dlgAlignObjects.m_hWnd)
			return TRUE;
	}
	return FALSE;
}

BOOL CPrintSheetView::ShowMeasure()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_bShowMeasure;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_bShowMeasure;
		else
			return FALSE;
}

BOOL CPrintSheetView::ShowPlate()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_bShowPlate;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_bShowPlate;
		else
			return FALSE;
}

BOOL CPrintSheetView::ShowBitmap()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_bShowBitmap;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_bShowBitmap;
		else
			return FALSE;
}

BOOL CPrintSheetView::ShowExposures()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_bShowExposures;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_bShowExposures;
		else
			return FALSE;
}

BOOL CPrintSheetView::ShowColorants()
{
	return (GetActColorants().GetSize() > 0) ? TRUE : FALSE;
	//CFrameWnd* pFrame = GetParentFrame();
	//if ( ! pFrame)
	//	return FALSE;
	//if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
	//	return FALSE;
	//else
	//	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
	//		return ((CNewPrintSheetFrame*)pFrame)->m_bShowOutputColorSettings;
	//	else
	//		return FALSE;
}

BOOL CPrintSheetView::ShowFoldSheet()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_bShowFoldSheet;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_bShowFoldSheet;
		else
			return FALSE;
}

BOOL CPrintSheetView::ShowCutBlocks()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->m_bShowCutBlocks;
	else
		return FALSE;
}

BOOL CPrintSheetView::ShowBoxShapes()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_bShowBoxShapes;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_bShowBoxShapes;
		else
			return FALSE;
}

BOOL CPrintSheetView::ShowGripper()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return FALSE;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_bShowGripper;
		else
			return FALSE;
}

BOOL CPrintSheetView::AlignSectionActive()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->m_bAlignSectionActive;
	else
		return FALSE;
}

BOOL CPrintSheetView::MarksViewActive()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_bMarksViewActive;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			if (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionPrintSheetMarks)
				return TRUE;

	return FALSE;
}

BOOL CPrintSheetView::OutputViewActive()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return FALSE;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			if (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionOutput)
				return TRUE;

	return FALSE;
}

BOOL CPrintSheetView::HandActive()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_bHandActive;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_bHandActive;
		else
			return FALSE;
}

BOOL CPrintSheetView::TrimToolActive()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_bTrimToolActive;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return (((CNewPrintSheetFrame*)pFrame)->m_dlgTrimming.m_hWnd) ? TRUE : FALSE;
		else
			return FALSE;
}

int CPrintSheetView::ViewMode()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		return ((CPrintSheetFrame*)pFrame)->m_nViewMode;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->m_nViewMode;
		else
			return ID_VIEW_SINGLE_SHEETS;
}

void CPrintSheetView::SetViewMode(int nViewMode)
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		((CPrintSheetFrame*)pFrame)->m_nViewMode = nViewMode;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		{
			int nPrevViewMode = ((CNewPrintSheetFrame*)pFrame)->m_nViewMode;
			((CNewPrintSheetFrame*)pFrame)->m_nViewMode = nViewMode;

			if (nPrevViewMode != nViewMode)
			{
				switch (nViewMode)
				{
				case ID_VIEW_SINGLE_SHEETS: ((CNewPrintSheetFrame*)pFrame)->m_nViewSide = FRONTSIDE;	break;
				case ID_VIEW_ALL_SHEETS:	((CNewPrintSheetFrame*)pFrame)->m_nViewSide = ALLSIDES;		break;
				}
			}
		}
}

void CPrintSheetView::SetLayoutObjectSelected(BOOL bSelected)
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		((CPrintSheetFrame*)pFrame)->m_bLayoutObjectSelected = bSelected;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			((CNewPrintSheetFrame*)pFrame)->m_bLayoutObjectSelected = bSelected;
}

void CPrintSheetView::SetFoldSheetRotateActive(BOOL bActive)
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		((CPrintSheetFrame*)pFrame)->m_bFoldSheetRotateActive = bActive;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			((CNewPrintSheetFrame*)pFrame)->m_bFoldSheetRotateActive = bActive;
}

void CPrintSheetView::SetFoldSheetTurnActive(BOOL bActive)
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		((CPrintSheetFrame*)pFrame)->m_bFoldSheetTurnActive = bActive;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			((CNewPrintSheetFrame*)pFrame)->m_bFoldSheetTurnActive = bActive;
}

void CPrintSheetView::SetFoldSheetTumbleActive(BOOL bActive)
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		((CPrintSheetFrame*)pFrame)->m_bFoldSheetTumbleActive = bActive;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			((CNewPrintSheetFrame*)pFrame)->m_bFoldSheetTumbleActive = bActive;
}

void CPrintSheetView::SetFoldSheetSelected(BOOL bSelected)
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		((CPrintSheetFrame*)pFrame)->m_bFoldSheetSelected = bSelected;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			((CNewPrintSheetFrame*)pFrame)->m_bFoldSheetSelected = bSelected;
}

void CPrintSheetView::SetEnableReleaseBleedButton(BOOL bEnable)
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		((CPrintSheetFrame*)pFrame)->m_bEnableReleaseBleedButton = bEnable;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		{
			((CNewPrintSheetFrame*)pFrame)->m_bEnableReleaseBleedButton = bEnable;
			CPrintSheetMaskSettingsView* pView = (CPrintSheetMaskSettingsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMaskSettingsView));
			if (pView)
				pView->UpdateDialogControls( this, TRUE );
		}
}

void CPrintSheetView::SetUndoActive(BOOL bActive)
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		((CPrintSheetFrame*)pFrame)->m_bUndoActive = bActive;
	else
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			((CNewPrintSheetFrame*)pFrame)->m_bUndoActive = bActive;
}

CColorantList CPrintSheetView::GetActColorants()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return CColorantList();
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->GetActColorants();
	else
		return CColorantList();
}

void CPrintSheetView::OnUpdatePrintSheetViewStrippingData(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, (pFrame->m_bShowStrippingData) ? TRUE : FALSE);
}

void CPrintSheetView::OnUpdatePrintSheetViewMarks(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, (pFrame->m_bShowStrippingData) ? FALSE : TRUE);
}

void CPrintSheetView::OnUpdateViewAllsides(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_nViewSide == ALLSIDES);
}

void CPrintSheetView::OnUpdateViewFrontside(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_nViewSide == FRONTSIDE);
}

void CPrintSheetView::OnUpdateViewBackside(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_nViewSide == BACKSIDE);
	if (pFrame->m_nViewSide == ALLSIDES)
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	else
	{
		CLayout* pLayout = pFrame->GetActLayout();
		if ( ! pLayout)
			COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		else
			COwnerDrawnButtonList::Enable(pCmdUI, ((pLayout->GetCurrentWorkStyle() == WORK_AND_TURN) || (pLayout->GetCurrentWorkStyle() == WORK_AND_TUMBLE) || (pLayout->GetCurrentWorkStyle() == WORK_SINGLE_SIDE)) ? FALSE : TRUE);

	}
}

void CPrintSheetView::OnUpdateViewBothsides(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_nViewSide == BOTHSIDES);
	if (pFrame->m_nViewSide == ALLSIDES)
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	else
	{
		CLayout* pLayout = pFrame->GetActLayout();
		if ( ! pLayout)
			COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		else
			COwnerDrawnButtonList::Enable(pCmdUI, ((pLayout->GetCurrentWorkStyle() == WORK_AND_TURN) || (pLayout->GetCurrentWorkStyle() == WORK_AND_TUMBLE || (pLayout->GetCurrentWorkStyle() == WORK_SINGLE_SIDE))) ? FALSE : TRUE);

	}
}

void CPrintSheetView::OnUpdateFoldsheetCheck(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowFoldSheet);
}

void CPrintSheetView::OnUpdateMeasureCheck(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowMeasure);
}

void CPrintSheetView::OnUpdatePlateCheck(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowPlate);
}

void CPrintSheetView::OnUpdateBitmapCheck(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowBitmap);
}

void CPrintSheetView::OnUpdateExposureCheck(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowExposures);
}

void CPrintSheetView::OnUpdateShowCutBlocks(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;

	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowCutBlocks);
}

void CPrintSheetView::OnUpdateShowBoxshapes(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;

	COwnerDrawnButtonList::Enable(pCmdUI, pDoc->m_flatProducts.HasGraphics());
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowBoxShapes);
}

void CPrintSheetView::OnUpdateAddNewPrintSheet(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetView::OnUpdateRemovePrintSheet(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	BOOL bEnable = (pFrame->GetActLayout() && (pFrame->m_nViewSide != ALLSIDES)) ? TRUE : FALSE;
	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
}

void CPrintSheetView::OnUpdateModifyShingling(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	COwnerDrawnButtonList::Show(pCmdUI, (pDoc->m_boundProducts.GetSize()) ? SW_SHOW : SW_HIDE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, (pFrame->m_dlgShinglingAssistent.m_hWnd) ? TRUE : FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetView::OnUpdateOutputMaskSettings(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, (pFrame->m_bShowOutputMaskSettings) ? TRUE : FALSE);
}

void CPrintSheetView::OnUpdateOutputColorSettings(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, (pFrame->m_bShowOutputColorSettings) ? TRUE : FALSE);
}

void CPrintSheetView::OnUpdateUndo(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::Enable(pCmdUI, pFrame->m_bUndoActive);
}

void CPrintSheetView::OnUpdateZoom(CCmdUI* pCmdUI) 
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->m_nViewMode == ID_ZOOM)
		COwnerDrawnButtonList::SetCheck(pCmdUI, TRUE);
	else
		COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetView::OnUpdateZoomDecrease(CCmdUI* pCmdUI) 
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (pFrame->m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetView::OnUpdateZoomFullview(CCmdUI* pCmdUI) 
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (pFrame->m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		return;
	}

	//CRect clientRect;
	//pView->GetClientRect(&clientRect);
 //   if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
	//	COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
	//else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetView::OnUpdateScrollHand(CCmdUI* pCmdUI) 
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;

	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bHandActive);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (pFrame->m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		pFrame->m_bHandActive = FALSE;
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		pFrame->m_bHandActive = FALSE;
	}
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}
