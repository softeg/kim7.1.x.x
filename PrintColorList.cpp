// PrintColorList.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "PrintColorList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintColorList

BEGIN_MESSAGE_MAP(CPrintColorList, CListCtrl)
	//{{AFX_MSG_MAP(CPrintColorList)
		// HINWEIS - Der Klassen-Assistent f�gt hier Zuordnungsmakros ein und entfernt diese.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CPrintColorList 


/////////////////////////////////////////////////////////////////////////////
// CPrintColorList - custom CListCtrl - containing color control informations

CPrintColorList::CPrintColorList()
{
}

CPrintColorList::~CPrintColorList()
{
	m_imageList.DeleteImageList();
}

void CPrintColorList::PostNcDestroy() 
{
	m_imageList.DeleteImageList();
	
	CListCtrl::PostNcDestroy();
}

void CPrintColorList::Initialize(CWnd* pParent)
{
	if ( ! m_hWnd)
	{
		VERIFY(SubclassDlgItem(IDC_SYSGENMARK_COLORLIST, pParent));

		CString string;
		string.LoadString(IDS_COLOR);
		InsertColumn(0, string, LVCFMT_LEFT, 180, 0);
		InsertColumn(1, _T(""), LVCFMT_LEFT,  80, 0);

		m_imageList.Create(16, 16, TRUE, 4, 4);
		m_imageList.Add(theApp.LoadIcon(IDI_SHEET_PAGE));
		SetImageList(&m_imageList, LVSIL_SMALL);
	}

	DeleteAllItems();
	m_printColorListItems.RemoveAll();
}

void CPrintColorList::AddComposite(CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex) 
{
	if ( ! pTemplate || ! pObjectSource || ! pColorDefTable)
		return;

	for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
		if (pObjectSource->m_PageSourceHeaders[i].m_strColorName.CompareNoCase(_T("Composite")) == 0)
			return;

	CColorDefinition  colorDef(_T("Composite"), WHITE, CColorDefinition::Spot, 0);
	short			  nPageNumInSourceFile = pObjectSource->m_PageSourceHeaders.GetSize() + 1;
	CPageSourceHeader objectSourceHeader;
	objectSourceHeader.Create(1, _T("1"), 0.0f, 0.0f, 0.0f, 0.0f, colorDef.m_strColorName, FALSE);
	objectSourceHeader.m_rgb			  = colorDef.m_rgb;
	objectSourceHeader.m_bPageNumberFix   = FALSE;
	pObjectSource->m_PageSourceHeaders.Add(objectSourceHeader);

	SEPARATIONINFO sepInfo = {(short)nObjectSourceIndex, nPageNumInSourceFile, (short)pColorDefTable->FindColorDefinitionIndex(colorDef.m_strColorName)};
	if (sepInfo.m_nColorDefinitionIndex < 0)
		sepInfo.m_nColorDefinitionIndex = pColorDefTable->InsertColorDef(colorDef);
	pTemplate->InsertSepInfo(&sepInfo);
}

void CPrintColorList::AddBlack(CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex) 
{
	if ( ! pTemplate || ! pObjectSource || ! pColorDefTable)
		return;

	for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
		if (pObjectSource->m_PageSourceHeaders[i].m_strColorName.CompareNoCase(_T("Composite")) == 0)
			return;

	CColorDefinition  colorDef(theApp.m_colorDefTable.GetKeyName(), BLACK, CColorDefinition::ProcessK, 0);
	short			  nPageNumInSourceFile = pObjectSource->m_PageSourceHeaders.GetSize() + 1;
	CPageSourceHeader objectSourceHeader;
	objectSourceHeader.Create(1, _T("1"), 0.0f, 0.0f, 0.0f, 0.0f, colorDef.m_strColorName, FALSE);
	objectSourceHeader.m_rgb			  = colorDef.m_rgb;
	objectSourceHeader.m_bPageNumberFix   = FALSE;
	pObjectSource->m_PageSourceHeaders.Add(objectSourceHeader);

	SEPARATIONINFO sepInfo = {(short)nObjectSourceIndex, nPageNumInSourceFile, (short)pColorDefTable->FindColorDefinitionIndex(colorDef.m_strColorName)};
	if (sepInfo.m_nColorDefinitionIndex < 0)
		sepInfo.m_nColorDefinitionIndex = pColorDefTable->InsertColorDef(colorDef);
	pTemplate->InsertSepInfo(&sepInfo);
}

void CPrintColorList::AnalyseData(CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex)
{
	if ( ! pObjectSource)
		return;
	if ( ! pTemplate)
		return;
	if ( ! pColorDefTable)
		return;
	if (pTemplate->m_bMap2AllColors)
		return;

	BOOL bCompositeFound = FALSE;
	BOOL bUnknownFound	 = FALSE;
	CString strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);
	for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
	{
		CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[i];
		if (rObjectSourceHeader.m_strColorName.CompareNoCase(_T("Composite")) == 0)
			bCompositeFound = TRUE;
		else
			if (rObjectSourceHeader.m_strColorName.CompareNoCase(strUnknown) == 0)
				bUnknownFound = TRUE;
	}

	if ( ! bCompositeFound)
	{
		if (pObjectSource->m_PageSourceHeaders.GetSize())	// any color(s) defined -> add composite to get correct behaviour for composite output (in PDFOutput.cpp)
		{
			AddComposite(pTemplate, pObjectSource, pColorDefTable, nObjectSourceIndex);
			bCompositeFound = TRUE;
		}
	}
	
	if (bCompositeFound || bUnknownFound)	
	{
		if ( ( (pObjectSource->m_PageSourceHeaders.GetSize() == 1) && bCompositeFound) ||					// only composite defined -> remove 
		     ( (pObjectSource->m_PageSourceHeaders.GetSize() == 2) && bCompositeFound && bUnknownFound)	)	// only composite and unknown defined -> remove 
		{
			pObjectSource->m_PageSourceHeaders.RemoveAll();
			pTemplate->m_ColorSeparations.RemoveAll();
		}
	}

	// look for unknown together with other colors -> if present remove unknown
	if (pObjectSource->m_PageSourceHeaders.GetSize() > 1)
		for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
		{
			CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[i];
			if (rObjectSourceHeader.m_strColorName.CompareNoCase(strUnknown) == 0)
			{
				int	nPageNumInSourceFile = pTemplate->m_ColorSeparations[i].m_nPageNumInSourceFile;
				pObjectSource->m_PageSourceHeaders.RemoveAt(i);
				for (int j = 0; j < pTemplate->m_ColorSeparations.GetSize(); j++)
					if (pTemplate->m_ColorSeparations[j].m_nPageNumInSourceFile >= nPageNumInSourceFile)
						pTemplate->m_ColorSeparations[j].m_nPageNumInSourceFile--;

				pTemplate->m_ColorSeparations.RemoveAt(i);
			}
		}

	if ( ! pObjectSource->m_PageSourceHeaders.GetSize())	
	{
		if (pTemplate->m_bMap2SpotColors)
			AddComposite(pTemplate, pObjectSource, pColorDefTable, nObjectSourceIndex);
		else
			AddBlack(pTemplate, pObjectSource, pColorDefTable, nObjectSourceIndex);
	}

	// check if color separations corresponds to page aource headers
	for (int i = 0; i < pTemplate->m_ColorSeparations.GetSize(); i++)
	{
		int	nPageNumInSourceFile = pTemplate->m_ColorSeparations[i].m_nPageNumInSourceFile;
		if ( (nPageNumInSourceFile < 1) || (nPageNumInSourceFile > pObjectSource->m_PageSourceHeaders.GetSize()) )
		{
			pTemplate->m_ColorSeparations.RemoveAt(i);
			continue;
		}

		CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[nPageNumInSourceFile - 1];
		int nColorDefinitionIndex = pColorDefTable->FindColorDefinitionIndex(rObjectSourceHeader.m_strColorName);
		if (nColorDefinitionIndex >= 0)
			pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex = nColorDefinitionIndex;
	}
}

void CPrintColorList::LoadData(CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex)
{
	int nItemSel = -1;
	POSITION pos = GetFirstSelectedItemPosition();
	if (pos)
		nItemSel = GetNextSelectedItem(pos);

	DeleteAllItems();
	m_printColorListItems.RemoveAll();

	if ( ! pObjectSource)
		return;
	if ( ! pTemplate)
		return;
	if ( ! pColorDefTable)
		return;

	AnalyseData(pTemplate, pObjectSource, pColorDefTable, nObjectSourceIndex);

	if ( (pTemplate->m_bMap2AllColors) && pTemplate->m_ColorSeparations.GetSize())
	{
		CString string;
		string.LoadString(IDS_ALL_COLOR);
		SEPARATIONINFO* pSepInfo = &pTemplate->m_ColorSeparations[0];
		CPageSourceHeader* pObjectSourceHeader = (pObjectSource->m_PageSourceHeaders.GetSize()) ? &pObjectSource->m_PageSourceHeaders[0] : NULL;
		AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, WHITE, string, pObjectSourceHeader, pSepInfo, CPrintColorListItem::Enabled | CPrintColorListItem::Remapped); 
	}
	else
		for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
		{
			CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[i];
			if (rObjectSourceHeader.m_strColorName.CompareNoCase(_T("Composite")) == 0)
				continue;

			BOOL			bIsSpot	  = FALSE;
			BOOL			bSepExist = FALSE;
			SEPARATIONINFO* pSepInfo  = pTemplate->GetFirstSepInfo(i + 1);
			while (pSepInfo)
			{
				CString strColorName;
				if ( (pSepInfo->m_nColorDefinitionIndex >= 0) && (pSepInfo->m_nColorDefinitionIndex < pColorDefTable->GetSize()) )
					strColorName = pColorDefTable->ElementAt(pSepInfo->m_nColorDefinitionIndex).m_strColorName;
				else
					strColorName = rObjectSourceHeader.m_strColorName;

				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
				int nColorType = (pColorDef) ? pColorDef->m_nColorType : ((strColorName.CompareNoCase(_T("Composite")) != 0) ? CColorDefinition::Spot : -1);
				int nState	   = ((nColorType == CColorDefinition::Spot) && (pTemplate->m_bMap2SpotColors)) ? 
																	CPrintColorListItem::Enabled | CPrintColorListItem::Remapped : 
																	CPrintColorListItem::Enabled;

				if (pTemplate->m_bMap2SpotColors)		// template could have been exploded by ReorganizePrintSheetPlates()
					if (nColorType == CColorDefinition::Spot)
					{
						bIsSpot = TRUE;
						break;								
					}

				if (pTemplate->m_bMap2AllColors)
				{
					CString string;
					string.LoadString(IDS_ALL_COLOR);
					AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, WHITE, string, &rObjectSourceHeader, pSepInfo, CPrintColorListItem::Enabled | CPrintColorListItem::Remapped); 
				}
				else
					if (pColorDef)
						AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, pColorDef->m_rgb, pColorDef->m_strColorName, &rObjectSourceHeader, pSepInfo, nState); 
					else
						AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, WHITE, strColorName, &rObjectSourceHeader, pSepInfo, nState); 

				bSepExist = TRUE;

				if (pTemplate->m_bMap2AllColors)		// template could have been exploded by ReorganizePrintSheetPlates()
					break;								// so show always only 1st separation

				pSepInfo = pTemplate->GetNextSepInfo(pSepInfo, i + 1);
			}

			if (bIsSpot)
				continue;

			if ( ! bSepExist)
				AddColorItem(i, rObjectSourceHeader.m_rgb, rObjectSourceHeader.m_strColorName, &rObjectSourceHeader, NULL, CPrintColorListItem::Disabled); 
		}

	if (pTemplate->m_bMap2SpotColors)
	{
		CString string;
		string.LoadString(IDS_SPOT_COLORS);
		CPageSourceHeader* pObjectSourceHeader = NULL;
		for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
			if (pObjectSource->m_PageSourceHeaders[i].m_strColorName.CompareNoCase(_T("Composite")) == 0)
				pObjectSourceHeader = &pObjectSource->m_PageSourceHeaders[i];
		AddColorItem(m_printColorListItems.GetSize(), WHITE, string, pObjectSourceHeader, NULL, CPrintColorListItem::Enabled | CPrintColorListItem::Remapped); 
	}

	if (nItemSel >= 0)
		SetItemState(nItemSel, LVIS_SELECTED, LVIS_SELECTED);
}

void CPrintColorList::SaveData(CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable, int nObjectSourceIndex)
{
	AnalyseData(pTemplate, pObjectSource, pColorDefTable, nObjectSourceIndex);
}

void CPrintColorList::AddColorItem(int nPageNumInDoc, COLORREF color, CString& strName, CPageSourceHeader* pObjectSourceHeader, SEPARATIONINFO* pSepInfo, int nState, int nMapSpotColorIndex)
{
	CPrintColorListItem clItem(nPageNumInDoc, color, strName, pObjectSourceHeader, pSepInfo, nState, nMapSpotColorIndex);
	m_printColorListItems.Add(clItem);

	LVITEM lvitem;
	lvitem.iItem	= GetItemCount();
	lvitem.iSubItem = 0;
	lvitem.mask	    = LVIF_TEXT;
	lvitem.pszText  = NULL;		// text will be taken from m_printColorListItems by DrawItem()
	InsertItem(&lvitem); 

	lvitem.iSubItem = 1;
	lvitem.pszText  = NULL;
	SetItem(&lvitem);
}

CPrintColorListItem* CPrintColorList::GetColorItem(const CString& strName)
{
	for (int i = 0; i < m_printColorListItems.GetSize(); i++)
	{
		if (m_printColorListItems[i].m_strColorName == strName)
			return &m_printColorListItems[i];
	}
	return NULL;
}

void CPrintColorList::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC	= CDC::FromHandle(lpDIS->hDC);
	int nIndex	= (int)lpDIS->itemID; // Index in item data
	if ((nIndex < 0) || (nIndex >= m_printColorListItems.GetSize()))
		return;

	// first of all draw hilite/lowlite background for entire row
	pDC->SetBkMode(TRANSPARENT);
	if ( lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE) )
	{
		CRect rect = lpDIS->rcItem;
		CBrush fillBrush((lpDIS->itemState & ODS_SELECTED) ? ::GetSysColor(COLOR_HIGHLIGHT) : pDC->GetBkColor());
		pDC->FillRect(rect, &fillBrush);
		fillBrush.DeleteObject();
	}
	//////////////////////

	int nOff			 = 4;
	int nColorRectWidth  = 11;
	int nColorRectHeight = 11;
	int nPosLeft		 = 0;
	int nPosRight		 = GetColumnWidth(0);

	/////////// 1st column
	// draw color rect
	if ( lpDIS->itemAction & ODA_DRAWENTIRE )
	{
		CRect rect;
		rect.left	= nPosLeft + nOff;
		rect.right	= rect.left + nColorRectWidth;
		rect.top	= lpDIS->rcItem.top;
		rect.bottom = rect.top + nColorRectHeight;
		rect.OffsetRect(0, 1);
		if (m_printColorListItems[nIndex].m_nState & CPrintColorListItem::Remapped)
			pDC->DrawIcon(rect.left - 2, rect.top - 1, theApp.LoadIcon(IDI_LINK));
		else
		{
			CBrush colorBrush((m_printColorListItems[nIndex].m_nState & CPrintColorListItem::Disabled) ? LIGHTGRAY : m_printColorListItems[nIndex].m_crRGB);
			CPen   framePen(PS_SOLID, 1, DARKGRAY);
			CBrush* pOldBrush = pDC->SelectObject(&colorBrush);
			CPen*	pOldPen	  = pDC->SelectObject(&framePen);
			pDC->Rectangle(rect);
			pDC->SelectObject(pOldBrush);
			pDC->SelectObject(pOldPen);
			colorBrush.DeleteObject();
			framePen.DeleteObject();
		}
	}

	CString strSpotColorName;
	// draw text
	if ( lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE) )
	{
		CRect rect = lpDIS->rcItem;
		rect.left  = nPosLeft + nOff + nColorRectWidth + nOff;
		rect.right = nPosRight;
		if (m_printColorListItems[nIndex].m_nState & CPrintColorListItem::Disabled)
			pDC->SetTextColor(DARKGRAY);
		else
			pDC->SetTextColor((lpDIS->itemState & ODS_SELECTED) ? ::GetSysColor(COLOR_HIGHLIGHTTEXT) : ::GetSysColor(COLOR_MENUTEXT));
		if (m_printColorListItems[nIndex].m_nMapSpotColorIndex >= 0) 
			strSpotColorName.Format(_T("%s = Spot %d"), m_printColorListItems[nIndex].m_strColorName, m_printColorListItems[nIndex].m_nMapSpotColorIndex);
		else
			strSpotColorName = m_printColorListItems[nIndex].m_strColorName;
		pDC->DrawText(strSpotColorName, rect, DT_LEFT | DT_VCENTER);
	}
	/////////// end of 1st column

	nPosLeft  = nPosRight;
	nPosRight+= GetColumnWidth(1);

	if ( lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE) )
	{
		CRect rect = lpDIS->rcItem;
		rect.left  = nPosLeft + nOff + nColorRectWidth + nOff;
		rect.right = nPosRight;
		if (m_printColorListItems[nIndex].m_nState & CPrintColorListItem::Disabled)
			pDC->SetTextColor(DARKGRAY);
		else
			pDC->SetTextColor((lpDIS->itemState & ODS_SELECTED) ? ::GetSysColor(COLOR_HIGHLIGHTTEXT) : ::GetSysColor(COLOR_MENUTEXT));
		CPageSourceHeader* pObjectSourceHeader = m_printColorListItems[nIndex].m_pObjectSourceHeader;
		if (pObjectSourceHeader)
		{
			CString strIntensity;
			strIntensity.Format(_T("%.0f %%"), pObjectSourceHeader->m_fColorIntensity * 100.0f);
			pDC->DrawText(strIntensity, rect, DT_LEFT | DT_VCENTER);
		}
	}
	/////////// end of 2nd column
}



/////////////////////////////////////////////////////////////////////////////
// CPrintColorList - custom CListCtrl - containing color control informations

void CPDFObjectPrintColorList::Initialize(CWnd* pParent)
{
	if ( ! m_hWnd)
	{
		VERIFY(SubclassDlgItem(IDC_PDFOBJECT_COLORCONTROL, pParent));

		CString string;
		//string.LoadString(IDS_PDF_SEPARATION);
		//InsertColumn(0, string,	LVCFMT_LEFT, 50, 0);
		string.LoadString(IDS_PLATECOLOR_STRING);
		InsertColumn(0, string, LVCFMT_LEFT, 180, 0);

		m_imageList.Create(16, 16, TRUE, 4, 4);
		m_imageList.Add(theApp.LoadIcon(IDI_SHEET_PAGE));
		SetImageList(&m_imageList, LVSIL_SMALL);
	}

	DeleteAllItems();
	m_printColorListItems.RemoveAll();
}

void CPDFObjectPrintColorList::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC	= CDC::FromHandle(lpDIS->hDC);
	int nIndex	= (int)lpDIS->itemID; // Index in item data
	if ((nIndex < 0) || (nIndex >= m_printColorListItems.GetSize()))
		return;

	// first of all draw hilite/lowlite background for entire row
	pDC->SetBkMode(TRANSPARENT);
	if ( lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE) )
	{
		CRect rect = lpDIS->rcItem;
		CBrush fillBrush((lpDIS->itemState & ODS_SELECTED) ? ::GetSysColor(COLOR_HIGHLIGHT) : pDC->GetBkColor());
		pDC->FillRect(rect, &fillBrush);
		fillBrush.DeleteObject();
	}
	//////////////////////

	int nOff			 = 4;
	int nColorRectWidth  = 11;
	int nColorRectHeight = 11;
	int nPosLeft		 = 0;
	int nPosRight		 = GetColumnWidth(0);

	/////////// 1st column
	// draw page symbol
	CString string;
	//if ( (lpDIS->itemAction & ODA_DRAWENTIRE))
	//{
	//	CRect rect = lpDIS->rcItem;
	//	rect.left  = nPosLeft + 5;
	//	rect.right = nPosRight;
	//	BOOL bDrawIcon = TRUE;
	//	if (nIndex > 0)
	//		if (m_printColorListItems[nIndex - 1].m_nPageIndex == m_printColorListItems[nIndex].m_nPageIndex)
	//			bDrawIcon = FALSE;
	//	if (bDrawIcon)
	//	{
	//		pDC->DrawIcon(rect.left + 1, rect.top + 1, theApp.LoadIcon(IDI_SHEET_PAGE));
	//		rect.left += 20;
	//		pDC->SetTextColor((lpDIS->itemState & ODS_SELECTED) ? ::GetSysColor(COLOR_HIGHLIGHTTEXT) : ::GetSysColor(COLOR_MENUTEXT));
	//		string.Format(_T("%d"), m_printColorListItems[nIndex].m_nPageIndex + 1);
	//		pDC->DrawText(string, rect, DT_LEFT | DT_VCENTER);
	//	}
	//}
	/////////// end of 1st column

	//nPosLeft  = nPosRight;
	//nPosRight+= GetColumnWidth(1);

	/////////// 2nd column
	// draw color rect
	if ( lpDIS->itemAction & ODA_DRAWENTIRE )
	{
		CRect rect;
		rect.left	= nPosLeft + nOff;
		rect.right	= rect.left + nColorRectWidth;
		rect.top	= lpDIS->rcItem.top;
		rect.bottom = rect.top + nColorRectHeight;
		rect.OffsetRect(0, 1);
		if (m_printColorListItems[nIndex].m_nMapSpotColorIndex > 0)
			pDC->DrawIcon(rect.left - 2, rect.top - 1, theApp.LoadIcon(IDI_LINK));
		else
		{
			CBrush colorBrush((m_printColorListItems[nIndex].m_nState & CPrintColorListItem::Disabled) ? LIGHTGRAY : m_printColorListItems[nIndex].m_crRGB);
			CPen   framePen(PS_SOLID, 1, DARKGRAY);
			CBrush* pOldBrush = pDC->SelectObject(&colorBrush);
			CPen*	pOldPen	  = pDC->SelectObject(&framePen);
			if (m_printColorListItems[nIndex].m_strColorName.CompareNoCase(_T("Composite")) == 0)
				pDC->DrawIcon(rect.left - 1, rect.top, theApp.LoadIcon((m_printColorListItems[nIndex].m_nState & CPrintColorListItem::Disabled) ? IDI_CMYK_DIMMED : IDI_CMYK));
			else
				pDC->Rectangle(rect);
			pDC->SelectObject(pOldBrush);
			pDC->SelectObject(pOldPen);
			colorBrush.DeleteObject();
			framePen.DeleteObject();
		}
	}
	// draw text
	if ( lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE) )
	{
		CRect rect = lpDIS->rcItem;
		rect.left  = nPosLeft + nOff + nColorRectWidth + nOff;
		rect.right = nPosRight;
		if (m_printColorListItems[nIndex].m_nState & CPrintColorListItem::Disabled)
			pDC->SetTextColor(DARKGRAY);
		else
			pDC->SetTextColor((lpDIS->itemState & ODS_SELECTED) ? ::GetSysColor(COLOR_HIGHLIGHTTEXT) : ::GetSysColor(COLOR_MENUTEXT));

		CString strColorName = m_printColorListItems[nIndex].m_strColorName;
		if (strColorName.CompareNoCase(_T("Composite")) == 0)
			strColorName = _T("Prozess");
		else
			if (m_printColorListItems[nIndex].m_nMapSpotColorIndex > 0) 
				strColorName.Format(_T("%s = Spot %d"), m_printColorListItems[nIndex].m_strColorName, m_printColorListItems[nIndex].m_nMapSpotColorIndex);

		pDC->DrawText(strColorName, rect, DT_LEFT | DT_VCENTER);
	}
	/////////// end of 2nd column

	nPosLeft  = nPosRight;
	nPosRight+= GetColumnWidth(2);
}

void CPDFObjectPrintColorList::LoadData(CPageTemplate* pTemplate, CColorDefinitionTable* pColorDefTable)
{
	int nItemSel = -1;
	POSITION pos = GetFirstSelectedItemPosition();
	if (pos)
		nItemSel = GetNextSelectedItem(pos);

	DeleteAllItems();
	m_printColorListItems.RemoveAll();

	if ( ! pColorDefTable)
		return;

	for (int i = 0; i < pTemplate->m_ColorSeparations.GetSize(); i++)
	{
		CPageSourceHeader* pObjectSourceHeader = NULL;
		if (pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex >= 0)
			pObjectSourceHeader = pTemplate->GetObjectSourceHeader(pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex);
		else
			pObjectSourceHeader = pTemplate->GetObjectSourceHeader(pTemplate->m_ColorSeparations[i]);

		if ( ! pObjectSourceHeader)
			continue;

		SEPARATIONINFO* pSepInfo = &pTemplate->m_ColorSeparations[i];
		if (pSepInfo)
		{
			CString strColorName;
			if ( (pSepInfo->m_nColorDefinitionIndex >= 0) && (pSepInfo->m_nColorDefinitionIndex < pColorDefTable->GetSize()) )
				strColorName = pColorDefTable->ElementAt(pSepInfo->m_nColorDefinitionIndex).m_strColorName;
			else
				strColorName = pObjectSourceHeader->m_strColorName;

			CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
			int nColorType = (pColorDef) ? pColorDef->m_nColorType : ((strColorName.CompareNoCase(_T("Composite")) != 0) ? CColorDefinition::Spot : -1);
			int nState	   = ((nColorType == CColorDefinition::Spot) && (pTemplate->m_bMap2SpotColors)) ? 
																CPrintColorListItem::Enabled | CPrintColorListItem::Remapped : 
																CPrintColorListItem::Enabled;
			if (pSepInfo->m_nColorDefinitionIndex == -1)
				nState = nState | CPrintColorListItem::Disabled;

			if (pTemplate->m_bMap2AllColors)
			{
				CString string;
				string.LoadString(IDS_ALL_COLOR);
				AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, WHITE, string, pObjectSourceHeader, pSepInfo, CPrintColorListItem::Enabled | CPrintColorListItem::Remapped); 
			}
			else
			{
				//if (pColorDef)
				//	AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, pColorDef->m_rgb, pColorDef->m_strColorName, pObjectSourceHeader, pSepInfo, nState); 
				//else
				//	AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, WHITE, strColorName, pObjectSourceHeader, pSepInfo, nState); 

				if (strColorName.CompareNoCase(_T("Composite")) == 0)
				{
					for (int j = 0; j < pObjectSourceHeader->m_processColors.GetSize(); j++)
						AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, theApp.m_colorDefTable.FindColorRef(pObjectSourceHeader->m_processColors[j]), pObjectSourceHeader->m_processColors[j], pObjectSourceHeader, pSepInfo, nState); 

					for (int j = 0; j < pObjectSourceHeader->m_spotColors.GetSize(); j++)
					{
						if (pTemplate->m_spotColorMappingTable.GetEntry(j) > 0)
							nState = nState | CPrintColorListItem::Remapped;
						CPageSource* pObjectSource = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex);
						COLORREF crRGB = (pObjectSource) ? pObjectSource->m_spotcolorDefList.GetAlternateRGB(pObjectSourceHeader->m_spotColors[j]) : WHITE;
						AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, crRGB, pObjectSourceHeader->m_spotColors[j], pObjectSourceHeader, pSepInfo, 
									 nState, pTemplate->m_spotColorMappingTable.GetEntry(j)); 
					}
				}
			}
		}
		else
			AddColorItem(i, pObjectSourceHeader->m_rgb, pObjectSourceHeader->m_strColorName, pObjectSourceHeader, NULL, CPrintColorListItem::Disabled); 
	}

	if (nItemSel >= 0)
		SetItemState(nItemSel, LVIS_SELECTED, LVIS_SELECTED);
}

void CPDFObjectPrintColorList::LoadData(CPageTemplate* pTemplate, CPageSource* pObjectSource, CColorDefinitionTable* pColorDefTable)
{
	int nItemSel = -1;
	POSITION pos = GetFirstSelectedItemPosition();
	if (pos)
		nItemSel = GetNextSelectedItem(pos);

	if ( ! pObjectSource)
	{
		LoadData(pTemplate, pColorDefTable);
		return;
	}

	DeleteAllItems();
	m_printColorListItems.RemoveAll();

	if ( ! pTemplate)
		return;
	if ( ! pColorDefTable)
		return;

	for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
	{
		CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[i];

		BOOL			bSepExist = FALSE;
		INT_PTR			nSepIndex;
		SEPARATIONINFO* pSepInfo = pTemplate->GetFirstSepInfo(i + 1, &nSepIndex);
		while (pSepInfo)
		{
			CString strColorName;
			if ( (pSepInfo->m_nColorDefinitionIndex >= 0) && (pSepInfo->m_nColorDefinitionIndex < pColorDefTable->GetSize()) )
				strColorName = pColorDefTable->ElementAt(pSepInfo->m_nColorDefinitionIndex).m_strColorName;
			else
				strColorName = rObjectSourceHeader.m_strColorName;

			CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strColorName);
			int nColorType = (pColorDef) ? pColorDef->m_nColorType : ((strColorName.CompareNoCase(_T("Composite")) != 0) ? CColorDefinition::Spot : -1);
			int nState	   = ((nColorType == CColorDefinition::Spot) && (pTemplate->m_spotColorMappingTable.GetEntry(pTemplate->GetCurSpotColorNum(pSepInfo->m_nColorDefinitionIndex, "", NULL) - 1) > 0)) ? 
																CPrintColorListItem::Enabled | CPrintColorListItem::Remapped : 
																CPrintColorListItem::Enabled;
			if (pSepInfo->m_nColorDefinitionIndex == -1)
				nState = nState | CPrintColorListItem::Disabled;

			if (pTemplate->m_bMap2AllColors)
			{
				CString string;
				string.LoadString(IDS_ALL_COLOR);
				AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, WHITE, string, &rObjectSourceHeader, pSepInfo, CPrintColorListItem::Enabled | CPrintColorListItem::Remapped); 
			}
			else
			{
				//if (pColorDef)
				//	AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, pColorDef->m_rgb, pColorDef->m_strColorName, &rObjectSourceHeader, pSepInfo, nState); 
				//else
				//	AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, WHITE, strColorName, &rObjectSourceHeader, pSepInfo, nState); 

				if (strColorName.CompareNoCase(_T("Composite")) == 0)
				{
					for (int j = 0; j < rObjectSourceHeader.m_processColors.GetSize(); j++)
						AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, theApp.m_colorDefTable.FindColorRef(rObjectSourceHeader.m_processColors[j]), rObjectSourceHeader.m_processColors[j], &rObjectSourceHeader, pSepInfo, nState); 

					for (int j = 0; j < rObjectSourceHeader.m_spotColors.GetSize(); j++)
					{
						if (pTemplate->m_spotColorMappingTable.GetEntry(j) > 0)
							nState = nState | CPrintColorListItem::Remapped;
						COLORREF crRGB = (pObjectSource) ? pObjectSource->m_spotcolorDefList.GetAlternateRGB(rObjectSourceHeader.m_spotColors[j]) : WHITE;
						AddColorItem(pSepInfo->m_nPageNumInSourceFile - 1, crRGB, rObjectSourceHeader.m_spotColors[j], &rObjectSourceHeader, pSepInfo, 
									 nState, pTemplate->m_spotColorMappingTable.GetEntry(j)); 
					}
				}
			}

			bSepExist = TRUE;

			if (pTemplate->m_bMap2AllColors)		// template could have been exploded by ReorganizePrintSheetPlates()
				break;								// so show always only 1st separation

			pSepInfo = pTemplate->GetNextSepInfo(pSepInfo, i + 1, &nSepIndex);
		}

		if ( ! bSepExist)
			AddColorItem(i, rObjectSourceHeader.m_rgb, rObjectSourceHeader.m_strColorName, &rObjectSourceHeader, NULL, CPrintColorListItem::Disabled); 
	}

	if (nItemSel >= 0)
		SetItemState(nItemSel, LVIS_SELECTED, LVIS_SELECTED);
}

void CPDFObjectPrintColorList::SaveData(CPageTemplate* pTemplate, CPageSource* pObjectSource)
{
}

