// DlgSelectFoldSchemePopup.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgSelectFoldSchemePopup.h"
#include "DlgComponentImposer.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "DlgPressDevice.h"
#include "DlgJobData.h"
#include "GraphicComponent.h"
#include "PrintSheetNavigationView.h"
#include "PrintSheetViewStrippingData.h"
#include "PrintSheetPlanningView.h"
#include "PrintSheetView.h"
#include "PageListView.h"
#include "NewPrintSheetFrame.h"
#include "DlgSelectSheetPopup.h"
#include "DlgMarkSet.h"
#include "PrintSheetComponentsView.h"
#include "PrintComponentsView.h"


// CAddBoundProductCustomPaintFrame

IMPLEMENT_DYNAMIC(CAddBoundProductCustomPaintFrame, CStatic)

CAddBoundProductCustomPaintFrame::CAddBoundProductCustomPaintFrame()
{
}

CAddBoundProductCustomPaintFrame::~CAddBoundProductCustomPaintFrame()
{
}

BEGIN_MESSAGE_MAP(CAddBoundProductCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CPPCCustomPaintFrame-Meldungshandler

void CAddBoundProductCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rcFrame;
	GetClientRect(rcFrame);

	CRect rcBackGround = rcFrame; rcBackGround.InflateRect(1, 1);
	dc.FillSolidRect(rcBackGround, ::GetSysColor(COLOR_3DFACE));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgSelectFoldSchemePopup* pParent = (CDlgSelectFoldSchemePopup*)GetParent();
	if ( ! pParent)
		return;

	pDoc->m_PageTemplateList.Draw(&dc, rcFrame, 0, pParent->GetActProductPartIndex(), FALSE, FALSE, TRUE, FALSE, NULL, FALSE, FALSE);
}


// CFoldSchemeDirList

CString	CFoldSchemeDirList::FindScheme(CString strFoldSchemeName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_nAttribute == CFoldSchemeDirItem::IsFoldScheme)
		{
			if (strFoldSchemeName == GetAt(i).m_strFileName)
				return GetAt(i).m_strFileName;
		}
	}

	return _T("");
}




// CDlgSelectFoldSchemePopup-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgSelectFoldSchemePopup, CDialog)

CDlgSelectFoldSchemePopup::CDlgSelectFoldSchemePopup(int nXPos, int nYPos, int nWidth, int nHeight, CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSelectFoldSchemePopup::IDD, pParent)
{
	m_bVisible			= TRUE;
	m_bStatic			= TRUE;
	m_bLocked			= FALSE;
	m_ptInitialPos		= CPoint(0, 0);
	m_nDockingSide		= RIGHT;

	m_pSchemeImageList  = new CImageList();
	m_boldFont.CreateFont (13, 0, 0, 0, FW_BOLD,	 FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_pBkBrush			  = new CBrush(RGB(225,234,253));
	m_pBrowserHeaderBrush = new CBrush(RGB(220,220,230));	

	m_nNumSheets = 0;
	m_strNumRestPages = _T("");
	m_nNumMaxSheets = 0;
}

CDlgSelectFoldSchemePopup::~CDlgSelectFoldSchemePopup()
{
	delete m_pSchemeImageList;
	delete m_pBkBrush;
	delete m_pBrowserHeaderBrush;
	m_boldFont.DeleteObject();
}

void CDlgSelectFoldSchemePopup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSelectFoldSchemePopup)
	DDX_Control(pDX, IDC_PD_SCHEMELIST, m_schemeList);
	DDX_Text(pDX, IDC_PD_NUM_SHEETS, m_nNumSheets);
	DDX_Text(pDX, IDC_PD_NUM_RESTPAGES, m_strNumRestPages);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgSelectFoldSchemePopup, CDialog)
	//{{AFX_MSG_MAP(CDlgSelectFoldSchemePopup)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_PD_SCHEMELIST, OnNMCustomdrawProddataSchemelist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PD_SCHEMELIST, OnLvnItemchangedPdSchemelist)
	ON_NOTIFY(NM_DBLCLK, IDC_PD_SCHEMELIST, OnNMDblclkPdSchemelist)
	ON_BN_CLICKED(IDC_PD_FOLDER_UP, OnBnClickedPdFolderUp)
	ON_BN_CLICKED(IDC_PD_DEFINE_SCHEMES, OnBnClickedDefineSchemes)
	ON_WM_ACTIVATE()
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_NUM_SHEETS_SPIN, OnDeltaposPdNumSheetsSpin)
	ON_WM_CREATE()
	ON_EN_KILLFOCUS(IDC_PD_NUM_SHEETS,OnEnKillfocusPdNumSheets)
	ON_BN_CLICKED(IDC_PD_MAX_NUM_SHEETS, OnBnClickedPdMaxNumSheets)
	ON_BN_CLICKED(IDC_PD_MIN_NUM_SHEETS, OnBnClickedPdMinNumSheets)
	ON_WM_WINDOWPOSCHANGING()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, OnBnClickedCancel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// CDlgSelectFoldSchemePopup-Meldungshandler

BOOL CDlgSelectFoldSchemePopup::OnEraseBkgnd(CDC* pDC)
{
	CRect rcFrame;
	GetClientRect(rcFrame);
	CPen pen(PS_SOLID, 1, BROWN);
	CBrush brush(RGB(225,234,253));
	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	pDC->Rectangle(rcFrame);
	pen.DeleteObject();
	brush.DeleteObject();

	//CGraphicComponent gc;
	//gc.DrawPopupWindowBackground(pDC, rcFrame, RGB(190,160,130), FALSE);

	return TRUE;
	//return CDialog::OnEraseBkgnd(pDC);
}

HBRUSH CDlgSelectFoldSchemePopup::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	switch (nCtlColor)
	{
	case CTLCOLOR_STATIC:	
		{
			int nID = pWnd->GetDlgCtrlID(); 
			switch (nID)
			{
			case IDC_FS_WINTITLE:			pDC->SelectObject(&m_boldFont);
											pDC->SetTextColor(GUICOLOR_CAPTION);
											pDC->SetBkMode(TRANSPARENT); 
											return (HBRUSH)(m_pBkBrush->GetSafeHandle());

			case IDC_PD_BROWSER_HEADER:		pDC->SetBkMode(TRANSPARENT);
											return (HBRUSH)(m_pBrowserHeaderBrush->GetSafeHandle());

			default:						pDC->SetBkMode(TRANSPARENT); 
											return (HBRUSH)(m_pBkBrush->GetSafeHandle());

			}
		}
		break;

	default:				
		break;
	}
	return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
}

BOOL CDlgSelectFoldSchemePopup::OnInitDialog()
{
	CDialog::OnInitDialog();

	CRect rcDlg; GetClientRect(rcDlg);
	CRect rcButton; GetDlgItem(IDOK)->GetWindowRect(rcButton); ScreenToClient(rcButton);
	if (m_ptInitialPos != CPoint(0,0))
	{
		if (GetDlgComponentImposer())
			rcDlg.bottom = rcButton.top - 10;
		CRect rcTarget = (m_nDockingSide == RIGHT) ? CRect(CPoint(m_ptInitialPos.x, m_ptInitialPos.y), rcDlg.Size()) : CRect(CPoint(m_ptInitialPos.x - rcDlg.Width(), m_ptInitialPos.y), rcDlg.Size());
		ClientToScreen(rcTarget);
		MoveWindow(rcTarget);
	}
	if (GetDlgComponentImposer())
	{
		rcDlg.bottom = rcButton.top - 10;
		MoveWindow(rcDlg);
	}

	m_nNumSheets = 0;
	m_strNumRestPages = _T("");
	m_nNumMaxSheets = 0;

	m_productThumbnail.SubclassDlgItem(IDC_ADDBOUND_THUMBNAIL, this);

	CBitmap bitmap;
	if ( ! m_pSchemeImageList->m_hImageList)
	{
		m_pSchemeImageList->Create(32, 32, ILC_COLOR8, 1, 0);	// List not to grow
		bitmap.LoadBitmap(IDB_BKSCHEMELIST);
		m_pSchemeImageList->Add(&bitmap, BLACK);
		m_schemeList.SetImageList(m_pSchemeImageList, LVSIL_SMALL);bitmap.DeleteObject();
	}

	((CButton*)GetDlgItem(IDC_PD_FOLDER_UP))->SetIcon(theApp.LoadIcon(IDI_FOLDER_UP));
	GetDlgItem(IDC_PD_FOLDER_UP)->EnableWindow(FALSE);

	bitmap.DeleteObject();
	bitmap.LoadBitmap(IDB_CLOSE_WINDOW);
	((CButton*)GetDlgItem(IDCANCEL))->SetBitmap(bitmap);

	CString strShortCurrentFolder, strShortInitialFolder;
	GetShortPathName(m_strCurrentSchemeFolder, strShortCurrentFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortCurrentFolder.ReleaseBuffer();
	GetShortPathName(theApp.settings.m_szSchemeDir, strShortInitialFolder.GetBuffer(MAX_TEXT), MAX_TEXT); strShortInitialFolder.ReleaseBuffer();
	strShortCurrentFolder.MakeUpper();
	strShortInitialFolder.MakeUpper();
	if (strShortCurrentFolder == strShortInitialFolder)
		GetDlgItem(IDC_PD_FOLDER_UP)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PD_FOLDER_UP)->EnableWindow(TRUE);

	((CSpinButtonCtrl*)GetDlgItem(IDC_PD_NUM_SHEETS_SPIN))->SetRange(0, 1);

	m_undoButton.SubclassDlgItem (IDC_UNDO_BUTTON, this);
	m_undoButton.SetBitmap(IDB_UNDO, IDB_UNDO, IDB_UNDO);

	m_selectedFoldSheet.DeleteContents();

	m_bLocked = TRUE;
	InitData();
	m_bLocked = FALSE;

	InitSchemes();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgSelectFoldSchemePopup::InitData()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintComponent component = GetActComponent();
	if (component.m_nType == CPrintComponent::Flat)
		return;

	BOOL bEnable = TRUE;
	int  nMaxNumSheets = 0;
	//if (component.m_nType == CPrintComponent::FoldSheet)
	//{
	//	CFoldSheet* pFoldSheet = component.GetFoldSheet();
	//	if (pFoldSheet)
	//		m_nNumSheets = pDoc->m_Bookblock.GetNumFoldSheets(component.m_nProductPartIndex, pFoldSheet->m_strFoldSheetTypeName);
	//	else
	//		m_nNumSheets = 0;

	//	m_nNumMaxSheets = 10;//(GetActLayout()) ? GetActLayout()->GetMaxNumPrintSheetsBasedOn() : 1;
	//}
	//else
	{
		CFoldSheet* pSelectedFoldSheet = GetSelectedFoldSheet();
		int nNumFoldSheetPages = (pSelectedFoldSheet) ? pSelectedFoldSheet->GetNumPages() : INT_MAX;
		m_nNumMaxSheets = component.m_nNumPages / nNumFoldSheetPages;
		int nNumRestPages = component.m_nNumPages - m_nNumSheets * nNumFoldSheetPages;
		if (nNumRestPages > 0)
			m_strNumRestPages.Format(_T("%d"), nNumRestPages);
		else
			m_strNumRestPages = _T("");
		bEnable = (m_nNumMaxSheets <= 0) ? FALSE : TRUE;
	}

	GetDlgItem(IDC_PD_NUM_SHEETS_STATIC)->EnableWindow(bEnable);
	GetDlgItem(IDC_PD_NUM_SHEETS	   )->EnableWindow(bEnable);
	GetDlgItem(IDC_PD_NUM_SHEETS_SPIN  )->EnableWindow(bEnable);
	GetDlgItem(IDC_PD_MIN_NUM_SHEETS   )->EnableWindow(bEnable);
	GetDlgItem(IDC_PD_MAX_NUM_SHEETS   )->EnableWindow(bEnable);
	CString strNum, strMinNum, strMaxNum;	
	if (m_nNumMaxSheets > 0)
	{
		strMinNum = (m_nNumMaxSheets == 0) ? _T("") : _T("1");
		strMaxNum.Format(_T("%d"), m_nNumMaxSheets);
	}
	else
	{
		strMinNum = _T(""); 
		strMaxNum = _T(""); 
	}

	GetDlgItem(IDC_PD_MIN_NUM_SHEETS)->SetWindowText(strMinNum);
	GetDlgItem(IDC_PD_MAX_NUM_SHEETS)->SetWindowText(strMaxNum);

	UpdateData(FALSE);

	if (m_nNumMaxSheets <= 0)
		GetDlgItem(IDC_PD_NUM_SHEETS)->SetWindowText(_T(""));
}

void CDlgSelectFoldSchemePopup::SaveData()
{
}

void CDlgSelectFoldSchemePopup::InitSchemes()
{
	CString strCurrentFolder = theApp.settings.m_szSchemeDir;
	if ( ! GetActFoldSheet())
		m_strCurrentSchemeFolder = strCurrentFolder;
	else
		if (GetActFoldSheet()->IsEmpty())
			m_strCurrentSchemeFolder = strCurrentFolder;
		else
			if ( ! FindSchemeFolder(strCurrentFolder, GetActFoldSheet()->m_strFoldSheetTypeName))
				m_strCurrentSchemeFolder = strCurrentFolder;
	
	InitSchemeList();
}

BOOL CDlgSelectFoldSchemePopup::FindSchemeFolder(CString strFolder, CString strScheme)
{
	if (FolderHasScheme(strFolder, strScheme))
	{
		m_strCurrentSchemeFolder = strFolder;
		return TRUE;
	}

	WIN32_FIND_DATA findData;
	CString			strFilename = strFolder + _T("\\*.*");
	HANDLE			hfileSearch = FindFirstFile(strFilename, &findData);
	if (hfileSearch == INVALID_HANDLE_VALUE)
	{
		FindClose(hfileSearch);
		return FALSE;
	}

	BOOL	bFound	  = FALSE;
	CString	strBuffer = findData.cFileName;
	do 
	{
		strBuffer = findData.cFileName;
		if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
		{
			if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (FindSchemeFolder(strFolder + _T("\\") + strBuffer, strScheme))
				{
					bFound = TRUE;
					break;
				}
			}
		}
	}
	while ( FindNextFile( hfileSearch, &findData ));
	FindClose(hfileSearch);

	return bFound;
}

BOOL CDlgSelectFoldSchemePopup::FolderHasScheme(const CString& strFolder, const CString& strScheme)
{
	WIN32_FIND_DATA findData;
	CString			strFilename = strFolder + _T("\\*.*");
	HANDLE			hfileSearch = FindFirstFile(strFilename, &findData);
	if (hfileSearch == INVALID_HANDLE_VALUE)
	{
		FindClose(hfileSearch);
		return FALSE;
	}

	BOOL	bFound	  = FALSE;
	CString	strBuffer = findData.cFileName;
	do 
	{
		strBuffer = findData.cFileName;
		if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
		{
			if ( ! (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
			{
				CString strExt = strBuffer.Right(4);
				strExt.MakeLower();
				if (strExt == _T(".isc"))
				{
					strBuffer = strBuffer.SpanExcluding(_T("."));
					if (strBuffer == strScheme)
					{
						bFound = TRUE;
						break;
					}
				}
			}
		}
	}
	while ( FindNextFile( hfileSearch, &findData ));
	FindClose(hfileSearch);

	return bFound;
}

void CDlgSelectFoldSchemePopup::InitSchemeList()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintComponent component = GetActComponent();

	int	nNumUnPlacedPages = component.m_nNumPages;

	m_bLocked = TRUE;

	WIN32_FIND_DATA findData;
	CString			strFilename = m_strCurrentSchemeFolder + _T("\\*.*");
	HANDLE			hfileSearch = FindFirstFile(strFilename, &findData);

	m_schemeDirList.RemoveAll();
	if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		CString	strBuffer = findData.cFileName;
		do 
		{
			strBuffer = findData.cFileName;
			if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
			{
				if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					CFoldSchemeDirItem dirItem(strBuffer, CFoldSchemeDirItem::IsFolder);
					m_schemeDirList.Add(dirItem);
				}
				else 
				{
					CFoldSheet foldSheet;
					foldSheet.CreateFromISC(m_strCurrentSchemeFolder + _T("\\") + strBuffer, _T(""), GetActProductPartIndex());
					if (SchemeIsRelevant(foldSheet, nNumUnPlacedPages))
					{
						CString strExt = strBuffer.Right(4);
						strExt.MakeLower();
						if (strExt == _T(".isc"))
						{
							strBuffer = strBuffer.SpanExcluding(_T("."));
							CFoldSchemeDirItem dirItem(strBuffer, CFoldSchemeDirItem::IsFoldScheme);
							m_schemeDirList.Add(dirItem);
						}
					}
				}
			}
		}
		while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);

	m_schemeDirList.Sort();
	m_schemeList.DeleteAllItems();

	int  nSel	= -1;
	BOOL bFound = FALSE;
	for (int i = 0; i < m_schemeDirList.GetSize(); i++)
	{
		LV_ITEM item;
		item.mask		= LVIF_TEXT | LVIF_IMAGE | LVIF_STATE | LVIF_PARAM;
		item.cchTextMax = MAX_TEXT;
		item.iItem		= i; 
		item.iSubItem	= 0;
		item.pszText	= m_schemeDirList[i].m_strFileName.GetBuffer(MAX_TEXT);
		item.iImage		= 0;
		item.state		= 0;
		item.stateMask	= LVIS_SELECTED;
		item.lParam		= (m_schemeDirList[i].m_nAttribute == CDirItem::IsFolder) ? 0 : 1;
		if (GetActFoldSheet())
			if (GetActFoldSheet()->m_strFoldSheetTypeName == m_schemeDirList[i].m_strFileName)
			{
				item.state = LVIS_SELECTED;
				bFound = TRUE;
				nSel = item.iItem;
			}
		m_schemeList.InsertItem(&item);
	}

	m_schemeList.EnsureVisible(nSel, FALSE);

	m_selectedFoldSheet.DeleteContents();
	if (nSel >= 0)
		m_selectedFoldSheet.CreateFromISC(m_strCurrentSchemeFolder + _T("\\") + m_schemeList.GetItemText(nSel, 0) + _T(".isc"), _T(""), GetActProductPartIndex());
	m_selectedFoldSheet.m_nProductPartIndex = (GetActBoundProductPart()) ? GetActBoundProductPart()->GetIndex() : -1;
	m_selectedFoldSheet.m_nFoldSheetNumber  = -1;

	CRect headerRect;
	GetDlgItem(IDC_PD_BROWSER_HEADER)->GetWindowRect(headerRect);
	CString strRelativePath;
	PathRelativePathTo(strRelativePath.GetBuffer(MAX_PATH), (LPCTSTR)theApp.settings.m_szSchemeDir, FILE_ATTRIBUTE_DIRECTORY, (LPCTSTR)m_strCurrentSchemeFolder, FILE_ATTRIBUTE_DIRECTORY);
	strRelativePath.ReleaseBuffer();
	CString strOut;
	strOut.Format(_T("  %s"), strRelativePath);
	CClientDC clientDC(this);
	PathCompactPath(clientDC.m_hDC, strOut.GetBuffer(MAX_PATH), headerRect.Width());

	GetDlgItem(IDC_PD_BROWSER_HEADER)->SetWindowText(strOut);

	m_bLocked = FALSE;
}

BOOL CDlgSelectFoldSchemePopup::SchemeIsRelevant(CFoldSheet& rFoldSheet, int nNumPagesToImpose)
{
	if (rFoldSheet.GetNumPages() > nNumPagesToImpose)
		return FALSE;

	CPrintComponent component = GetActComponent();

	if (component.m_nType == CPrintComponent::PageSection)
	{
		CPrintSheet* pPrintSheet = component.GetFirstPrintSheet();
		if ( ! pPrintSheet)
			return TRUE;

		int nLayerIndex = 0;
		float fXPos, fYPos;
		int nNumFoldSheetsX, nNumFoldSheetsY;
		pPrintSheet->FindFreePosition(&rFoldSheet, nLayerIndex, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY, NULL, FALSE);
		if ( (nNumFoldSheetsX * nNumFoldSheetsY) <= 0)
			return FALSE;
		else
			return TRUE;
	}
	else
	{
		if ( ! GetActPrintSheet() || ! GetActLayout() || ! GetActFoldSheet())
			return TRUE;

		int				 nLayerIndex	= 0;
		CFoldSheetLayer* pOldLayer		= GetActFoldSheet()->GetLayer(nLayerIndex);
		CLayout			 layout			= *GetActPrintSheet()->GetLayout();
		CPrintSheet		 printSheet		= *GetActPrintSheet(); printSheet.LinkLayout(&layout);
		int				 nComponentRefIndex = printSheet.FoldSheetLayerToLayerRef(pOldLayer);
		while (nComponentRefIndex >= 0)
		{
			if ((nComponentRefIndex >= 0) && (nComponentRefIndex < printSheet.m_FoldSheetLayerRefs.GetSize()))
			{
				layout.RemoveFoldSheetLayer(nComponentRefIndex);
				printSheet.m_FoldSheetLayerRefs.RemoveAt(nComponentRefIndex);
			}
			nComponentRefIndex = printSheet.FoldSheetLayerToLayerRef(pOldLayer);
		}
		float fXPos, fYPos;
		int nNumFoldSheetsX, nNumFoldSheetsY;
		printSheet.FindFreePosition(&rFoldSheet, nLayerIndex, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY, NULL, FALSE);
		if ( (nNumFoldSheetsX * nNumFoldSheetsY) <= 0)
			return FALSE;
		else
			return TRUE;
	}

	return FALSE;
}

CPrintSheet* CDlgSelectFoldSchemePopup::GetActPrintSheet()
{
	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	return (pPrintSheetView) ? pPrintSheetView->GetActPrintSheet() : NULL;
}

CLayout* CDlgSelectFoldSchemePopup::GetActLayout()
{
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	if ( ! pPrintSheet)
		return NULL;
	return pPrintSheet->GetLayout();
}

CPrintComponent CDlgSelectFoldSchemePopup::GetActComponent()
{
	CPrintSheetView*	 pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CNewPrintSheetFrame* pFrame			 = (pPrintSheetView) ? (CNewPrintSheetFrame*)pPrintSheetView->GetParentFrame() : NULL;
	if ( ! pFrame)
		return CPrintComponent();

	CPrintComponentsView* pView = pFrame->GetPrintComponentsView();
	if ( ! pView)
		return CPrintComponent();

	return pView->GetActComponent();
}

CFoldSheet* CDlgSelectFoldSchemePopup::GetActFoldSheet()
{
	CPrintSheetView*	 pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CNewPrintSheetFrame* pFrame			 = (pPrintSheetView) ? (CNewPrintSheetFrame*)pPrintSheetView->GetParentFrame() : NULL;
	if ( ! pFrame)
		return NULL;

	CPrintComponentsView* pView = pFrame->GetPrintComponentsView();
	if ( ! pView)
		return NULL;

	CPrintComponent component = pView->GetActComponent();
	return component.GetFoldSheet();
}

CBoundProductPart* CDlgSelectFoldSchemePopup::GetActBoundProductPart()
{
	CPrintSheetView*	 pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CNewPrintSheetFrame* pFrame			 = (pPrintSheetView) ? (CNewPrintSheetFrame*)pPrintSheetView->GetParentFrame() : NULL;
	if ( ! pFrame)
		return NULL;

	CPrintComponentsView* pView = pFrame->GetPrintComponentsView();
	if ( ! pView)
		return NULL;

	CPrintComponent component = pView->GetActComponent();
	return component.GetBoundProductPart();
}

BOOL CDlgSelectFoldSchemePopup::GetActProductPartIndex()
{
	return GetActComponent().m_nProductPartIndex;
}

CDlgComponentImposer* CDlgSelectFoldSchemePopup::GetDlgComponentImposer()
{
	CPrintSheetViewStrippingData* pView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	if ( ! pView)
		return NULL;

	if (pView->m_layoutDataWindow.m_dlgComponentImposer.m_hWnd)
		return &pView->m_layoutDataWindow.m_dlgComponentImposer;
	else
		return NULL;
}

void CDlgSelectFoldSchemePopup::OnLvnItemchangedPdSchemelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	if (m_bLocked)
		return;

	if (pNMLV->iItem < 0)
		return;
	if (pNMLV->uNewState == pNMLV->uOldState) 
		return;
	if ( ! (pNMLV->uNewState & LVIS_SELECTED) )
		return;
	if (pNMLV->lParam == 0)		// folder
		return;

	m_selectedFoldSheet.DeleteContents();
	m_selectedFoldSheet.CreateFromISC(m_strCurrentSchemeFolder + _T("\\") + m_schemeList.GetItemText(pNMLV->iItem, 0) + _T(".isc"), _T(""), GetActProductPartIndex());
	m_selectedFoldSheet.m_nProductPartIndex = (GetActBoundProductPart()) ? GetActBoundProductPart()->GetIndex() : -1;
	m_selectedFoldSheet.m_nFoldSheetNumber  = -1;

	InitData();

	if (GetDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);

	//OnBnClickedPdMaxNumSheets();			// 05/19/2015: changed because it seems to be more practical when initially only 1 sheet
	OnBnClickedPdMinNumSheets();
}

void CDlgSelectFoldSchemePopup::OnNMDblclkPdSchemelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	if ( (pNMItemActivate->iItem >= 0) && (pNMItemActivate->iItem < m_schemeDirList.GetSize()) )
	{
		CFoldSchemeDirItem dirItem = m_schemeDirList[pNMItemActivate->iItem];
		if (dirItem.m_nAttribute == CFoldSchemeDirItem::IsFolder)
		{
			m_strCurrentSchemeFolder += _T("\\") + dirItem.m_strFileName;
			InitSchemeList();

			GetDlgItem(IDC_PD_FOLDER_UP)->EnableWindow(TRUE);
			m_selectedFoldSheet.DeleteContents();
			InitData();
			if (GetDlgComponentImposer())
				GetDlgComponentImposer()->InitData(TRUE);
		}
		else
			OnBnClickedOk();
	}

	*pResult = 0;
}

void CDlgSelectFoldSchemePopup::OnBnClickedPdFolderUp()
{
	CString strShortCurrentFolder, strShortInitialFolder;
	GetShortPathName(m_strCurrentSchemeFolder,		strShortCurrentFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortCurrentFolder.ReleaseBuffer();
	GetShortPathName(theApp.settings.m_szSchemeDir, strShortInitialFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortInitialFolder.ReleaseBuffer();
	strShortCurrentFolder.MakeUpper();
	strShortInitialFolder.MakeUpper();
	if (strShortCurrentFolder == strShortInitialFolder)
		return;

	int nIndex = m_strCurrentSchemeFolder.ReverseFind(_T('\\'));	
	if (nIndex == -1)
		return;

	m_strCurrentSchemeFolder = m_strCurrentSchemeFolder.Left(nIndex);

	GetShortPathName(m_strCurrentSchemeFolder, strShortCurrentFolder.GetBuffer(MAX_TEXT), MAX_TEXT);	strShortCurrentFolder.ReleaseBuffer();
	strShortCurrentFolder.MakeUpper();
	if (strShortCurrentFolder == strShortInitialFolder)
		GetDlgItem(IDC_PD_FOLDER_UP)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_PD_FOLDER_UP)->EnableWindow(TRUE);

	InitSchemeList();

	m_selectedFoldSheet.DeleteContents();
	InitData();
	if (GetDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);
}

void CDlgSelectFoldSchemePopup::OnBnClickedDefineSchemes()
{
	m_bLocked = TRUE;
	theApp.OnSettingsImpose();
	m_bLocked = FALSE;
	InitSchemeList();

	m_selectedFoldSheet.DeleteContents();
	InitData();
	if (GetDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);
}

void CDlgSelectFoldSchemePopup::OnNMCustomdrawProddataSchemelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW	 pNMCD	 = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			CString strItem;
			ListView_GetItemText(hWndListCtrl, pNMCD->dwItemSpec, 0, strItem.GetBuffer(MAX_TEXT), MAX_TEXT);
			strItem.ReleaseBuffer();

			CRect rcLabel;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			if (rcLabel.IsRectEmpty())
				break;

			if (lvitem.lParam == CFoldSchemeDirItem::IsFoldScheme)
			{
				DrawSchemeThumbnail(pDC, rcItem, strItem);
				rcLabel.left += 2;
			}
			else
			{
				CImage img;
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_FILE_OPEN));				
				if ( ! img.IsNull())
					img.TransparentBlt(pDC->m_hDC, rcItem.left + 2, rcItem.top + 5, img.GetWidth(), img.GetHeight(), WHITE);
				rcLabel.left += 5;
			}

			COLORREF crOldTextColor = pDC->SetTextColor(BLACK);//DARKBLUE);

			pDC->DrawText(strItem, rcLabel, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);

			pDC->SetTextColor(crOldTextColor);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}

void CDlgSelectFoldSchemePopup::DrawSchemeThumbnail(CDC* pDC, CRect rcFrame, const CString& strScheme)
{
	CFileException fileException;
	CFile		   file;
	CString strFullPath = m_strCurrentSchemeFolder + _T("\\") + strScheme + _T(".isc");
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}
	CArchive archive(&file, CArchive::load);

	CFoldSheet foldSheet;
	DestructElements(&foldSheet, 1);
	SerializeElements(archive, &foldSheet, 1);
	archive.Close();
	file.Close();

	foldSheet.DrawThumbnail(pDC, rcFrame);
}

void CDlgSelectFoldSchemePopup::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if ( ! m_bStatic)
	{
		if (nState == WA_INACTIVE)
		{
			if ( ! m_bLocked)
			{
				if (m_hWnd)
				{
					m_bLocked = TRUE;
					EndModalLoop(IDCANCEL);
					DestroyWindow();
				}
			}
		}
		else
			m_bLocked = FALSE;
	}
}

void CDlgSelectFoldSchemePopup::OnDestroy()
{
	CDialog::OnDestroy();

	m_pSchemeImageList->DeleteImageList();
}

void CDlgSelectFoldSchemePopup::OnEnKillfocusPdNumSheets()
{
	int nNumSheetsOld = m_nNumSheets;

	UpdateData(TRUE);

	if (m_nNumSheets == nNumSheetsOld)
		return;

	InitData();
}

void CDlgSelectFoldSchemePopup::OnDeltaposPdNumSheetsSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	int nOldNum = m_nNumSheets ;

	int nNum = m_nNumSheets + pNMUpDown->iDelta;
	if ((nNum <= 0) || (nNum > 999))
	{
		m_nNumSheets = nOldNum;
		return;
	}
	m_nNumSheets = nNum;
	InitData();
}

int CDlgSelectFoldSchemePopup::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

BOOL CDlgSelectFoldSchemePopup::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			OnEnKillfocusPdNumSheets();
			return TRUE;
		}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgSelectFoldSchemePopup::OnBnClickedPdMinNumSheets()
{
	m_nNumSheets = 1;
	UpdateData(FALSE);
	InitData();
}

void CDlgSelectFoldSchemePopup::OnBnClickedPdMaxNumSheets()
{
	m_nNumSheets = m_nNumMaxSheets;
	UpdateData(FALSE);
	InitData();
}

CFoldSheet* CDlgSelectFoldSchemePopup::GetSelectedFoldSheet()
{
	//POSITION pos = m_schemeList.GetFirstSelectedItemPosition();
	//if ( ! pos)
	//	return NULL;

	//int nSel = m_schemeList.GetNextSelectedItem(pos);
	//if (nSel < 0)
	//	return NULL;

	if (m_selectedFoldSheet.IsEmpty())
		return NULL;
	else
		return &m_selectedFoldSheet;
}

void CDlgSelectFoldSchemePopup::UndoSave()
{
	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pPrintSheetView)
		pPrintSheetView->UndoSave();
}

void CDlgSelectFoldSchemePopup::OnWindowPosChanging(WINDOWPOS FAR* lpwndpos) 
{
	if( ! m_bVisible)
	{
		lpwndpos->flags &= ~SWP_SHOWWINDOW;
	}

	CDialog::OnWindowPosChanging(lpwndpos);
}

void CDlgSelectFoldSchemePopup::OnBnClickedOk()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintComponent component = GetActComponent();
	if (component.IsNull())
		return;

	if (component.m_nType == CPrintComponent::PageSection)
	{
		m_bLocked = TRUE;
		CFoldSheet* pNewFoldSheet = GetSelectedFoldSheet();
		if (pNewFoldSheet)
		{
			UndoSave();
			int nNumPagesImposed = pDoc->m_Bookblock.ImposePageSection(GetActBoundProductPart(), *pNewFoldSheet, component.m_nNumPages, m_nNumSheets);
			pDoc->m_printComponentsList.Analyze();
			if (GetDlgComponentImposer())
				GetDlgComponentImposer()->InitData(TRUE);
		}
		EndModalLoop(IDOK);
		DestroyWindow();
	}
	//else
	//	ReImposeFoldSheets();
}

void CDlgSelectFoldSchemePopup::OnBnClickedCancel()
{
	EndModalLoop(IDCANCEL);
	DestroyWindow();
}

void CDlgSelectFoldSchemePopup::PostNcDestroy()
{
	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if (pDoc)
	//{
	//	pDoc->m_printGroupList.Analyze();
	//}

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),					NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),		NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),			NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	theApp.UpdateView(RUNTIME_CLASS(CProductsView));
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));

	CDialog::PostNcDestroy();
}
