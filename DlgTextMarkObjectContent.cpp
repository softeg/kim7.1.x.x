// DlgTextMarkObjectContent.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetFrame.h"
#include "DlgJobColorDefinitions.h"
#include "DlgTextMarkObjectContent.h"
#include "DlgPlaceholderSyntaxInfo.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgTextMarkObjectContent 


CDlgTextMarkObjectContent::CDlgTextMarkObjectContent(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgTextMarkObjectContent::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgTextMarkObjectContent)
	m_strOutputText = _T("");
	//}}AFX_DATA_INIT

	m_pParent = NULL;
}

CDlgTextMarkObjectContent::~CDlgTextMarkObjectContent()
{
}

BOOL CDlgTextMarkObjectContent::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}	

void CDlgTextMarkObjectContent::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgTextMarkObjectContent)
	DDX_Text(pDX, IDC_TEXTMARKSTRING, m_strOutputText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgTextMarkObjectContent, CDialog)
	//{{AFX_MSG_MAP(CDlgTextMarkObjectContent)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_PLACEHOLDER,	OnButtonSelectPlaceholder)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSHEETNUM,			OnPlaceholderFoldsheetNum)
	ON_COMMAND(ID_PLACEHOLDER_COLORNAME,			OnPlaceholderColorName)
	ON_COMMAND(ID_PLACEHOLDER_SHEETSIDE,			OnPlaceholderSheetside)
	ON_COMMAND(ID_PLACEHOLDER_WEBNUM,				OnPlaceholderWebNum)
	ON_COMMAND(ID_PLACEHOLDER_COLORNUM,				OnPlaceholderColorNum)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSHEETNUM_CG,		OnPlaceholderFoldsheetNumCG)
	ON_COMMAND(ID_PLACEHOLDER_JOBTITLE,				OnPlaceholderJobTitle)
	ON_COMMAND(ID_PLACEHOLDER_TILENUM,				OnPlaceholderTileNum)
	ON_COMMAND(ID_PLACEHOLDER_SHEETNUM,				OnPlaceholderSheetNum)
	ON_COMMAND(ID_PLACEHOLDER_FILE,					OnPlaceholderFile)
	ON_COMMAND(ID_PLACEHOLDER_LOWPAGE_SIDE,			OnPlaceholderLowpageSide)
	ON_COMMAND(ID_PLACEHOLDER_LOWPAGE_SHEET,		OnPlaceholderLowpageSheet)
	ON_COMMAND(ID_PLACEHOLDER_HIGHPAGE_SIDE,		OnPlaceholderHighpageSide)
	ON_COMMAND(ID_PLACEHOLDER_HIGHPAGE_SHEET,		OnPlaceholderHighpageSheet)
	ON_COMMAND(ID_PLACEHOLDER_PRESSDEVICE,			OnPlaceholderPressDevice)
	ON_COMMAND(ID_PLACEHOLDER_JOB,					OnPlaceholderJob)
	ON_COMMAND(ID_PLACEHOLDER_JOBNUM,				OnPlaceholderJobNum)
	ON_COMMAND(ID_PLACEHOLDER_CUSTOMER,				OnPlaceholderCustomer)
	ON_COMMAND(ID_PLACEHOLDER_CUSTOMERNUM,			OnPlaceholderCustomerNum)
	ON_COMMAND(ID_PLACEHOLDER_CREATOR,				OnPlaceholderCreator)
	ON_COMMAND(ID_PLACEHOLDER_CREATIONDATE,			OnPlaceholderCreationDate)
	ON_COMMAND(ID_PLACEHOLDER_LASTMODIFIED,			OnPlaceholderLastModified)
	ON_COMMAND(ID_PLACEHOLDER_SCHEDULEDTOPRINT,		OnPlaceholderScheduledToPrint)
	ON_COMMAND(ID_PLACEHOLDER_OUTPUTDATE,			OnPlaceholderOutputDate)
	ON_COMMAND(ID_PLACEHOLDER_NOTES,				OnPlaceholderNotes)
	ON_COMMAND(ID_PLACEHOLDER_ASIR,					OnPlaceholderAsir)
	ON_COMMAND(ID_PLACEHOLDER_PAGENUMLIST,			OnPlaceholderPageNumList)
	ON_COMMAND(ID_PLACEHOLDER_PAGENUM,				OnPlaceholderPageNum)
	ON_COMMAND(ID_PLACEHOLDER_PRODUCTNAME,			OnPlaceholderProductName)
	ON_COMMAND(ID_PLACEHOLDER_PRODUCTPARTNAME,		OnPlaceholderProductPartName)
	ON_COMMAND(ID_PLACEHOLDER_PAPERGRAIN,			OnPlaceholderPaperGrain)
	ON_COMMAND(ID_PLACEHOLDER_PAPERTYPE,			OnPlaceholderPaperType)
	ON_COMMAND(ID_PLACEHOLDER_PAPERGRAMMAGE,		OnPlaceholderPaperGrammage)
	ON_COMMAND(ID_PLACEHOLDER_WORKSTYLE,			OnPlaceholderWorkStyle)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSCHEME,			OnPlaceholderFoldScheme)
	ON_COMMAND(ID_PLACEHOLDER_NUMFOLDSHEETS,		OnPlaceholderNumFoldsheets)
	ON_COMMAND(ID_PLACEHOLDER_BLANK,				OnPlaceholderBlank)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNeedText )
	ON_BN_CLICKED(IDC_BUTTON_PLACEHOLDER_INFO, &CDlgTextMarkObjectContent::OnBnClickedButtonPlaceholderInfo)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgTextMarkObjectContent 

BOOL CDlgTextMarkObjectContent::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	if ( ! m_placeholderMenu.m_hMenu)
		VERIFY(m_placeholderMenu.LoadMenu(IDR_PLACEHOLDER_POPUP));

	VERIFY(m_placeholderEdit.SubclassDlgItem(IDC_TEXTMARKSTRING, this));
	m_placeholderEdit.m_pParent			 = this;
	m_placeholderEdit.m_pPlaceholderMenu = &m_placeholderMenu;
	m_placeholderEdit.m_nIDButton		 = IDC_BUTTON_SELECT_PLACEHOLDER;

	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	m_printColorControl.Create(this, m_pParent);

	CRect controlRect, frameRect;
	m_printColorControl.GetWindowRect(controlRect);
	GetDlgItem(IDC_COLORCONTROL_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	m_printColorControl.SetWindowPos(NULL, frameRect.left, frameRect.top, controlRect.Width(), controlRect.Height(), SWP_NOZORDER);
	
	m_bInitialShowFoldSheet = TRUE;

	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_PLACEHOLDER))->SetIcon(theApp.LoadIcon(IDI_DROPDOWN_ARROW));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgTextMarkObjectContent::CheckTextmarkForFoldsheetView() 
{
/*
	CPrintSheetView*  pView	 = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheetFrame* pFrame = (CPrintSheetFrame*)pView->GetParentFrame();
	CString strTextmark;
	GetDlgItem(IDC_TEXTMARKSTRING)->GetWindowText(strTextmark);
	if ( (strTextmark.Find("$1_") != -1) || (strTextmark.Find("$4_") != -1) || (strTextmark.Find("$6_") != -1) )
	{
		if ( ! pFrame->m_bShowFoldSheet || m_bInitialShowFoldSheet)
		{
			pFrame->m_bShowFoldSheet = TRUE;
			pView->m_DisplayList.Invalidate();
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
			m_bInitialShowFoldSheet  = FALSE;
		}
	}
	else
	{
		if (pFrame->m_bShowFoldSheet)
		{
			pFrame->m_bShowFoldSheet = FALSE;
			pView->m_DisplayList.Invalidate();
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		}
	}
*/
}

void CDlgTextMarkObjectContent::LoadData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::TextMark)
		return;

	CString strContentType, strParams;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		//CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		CPageTemplate* pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->GetPrintSheet()) : pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				if (pObjectSource)
				{
					int	nIndex = pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
					if (nIndex != -1)	// invalid string
					{
						strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
						strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
						strParams.TrimLeft(); strParams.TrimRight();
					}
					m_printColorControl.LoadData(pTemplate, pObjectSource, nObjectSourceIndex);
				}
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex != -1)	// invalid string
		{
			strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);
			strParams	   = pMark->m_MarkSource.m_strSystemCreationInfo.Right(pMark->m_MarkSource.m_strSystemCreationInfo.GetLength() - nIndex);
			strParams.TrimLeft(); strParams.TrimRight();
		}

		m_printColorControl.LoadData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
	}

	if (strContentType == "$TEXT")
		m_strOutputText = strParams;

	UpdateData(FALSE);

	CheckTextmarkForFoldsheetView();
}

void CDlgTextMarkObjectContent::SaveData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::TextMark)
		return;

	UpdateData(TRUE);

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				int	nIndex = (pObjectSource) ? pObjectSource->m_strSystemCreationInfo.Find(_T(" ")) : -1;
				if (nIndex != -1)	// invalid string
				{
					CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
					CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
					strParams.TrimLeft(); strParams.TrimRight();

					if (strContentType == "$TEXT")
						pObjectSource->m_strSystemCreationInfo.Format(_T("$TEXT %s"), m_strOutputText);

					pObjectSource->m_nType = CPageSource::SystemCreated;
				}

				m_printColorControl.SaveData(pTemplate, pObjectSource, nObjectSourceIndex);
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex != -1)	// invalid string
		{
			CString strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);
			CString strParams	   = pMark->m_MarkSource.m_strSystemCreationInfo.Right(pMark->m_MarkSource.m_strSystemCreationInfo.GetLength() - nIndex);
			strParams.TrimLeft(); strParams.TrimRight();

			if (strContentType == "$TEXT")
				pMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$TEXT %s"), m_strOutputText);

			CTextMark* pTextMark = (CTextMark*)pMark;
			pTextMark->m_MarkSource.m_nType = CPageSource::SystemCreated;
			pTextMark->m_strOutputText = m_strOutputText;
		}

		m_printColorControl.SaveData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);

		//CPrintSheet* pPrintSheet	 = m_pParent->GetPrintSheet();
		//CLayout*	 pLayout		 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		//if (pLayout)
		//{
		//	CLayoutObject* pLayoutObject = pLayout->FindMarkObject(pMark->m_strMarkName);
		//	if (pLayoutObject)
		//	{
		//		CPageTemplate* pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->GetPrintSheet()) : pLayoutObject->GetCurrentMarkTemplate();
		//		if (pTemplate)
		//		{
		//			CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
		//			int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
		//			int	nIndex = (pObjectSource) ? pObjectSource->m_strSystemCreationInfo.Find(_T(" ")) : -1;
		//			if (nIndex != -1)	// invalid string
		//			{
		//				CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
		//				CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
		//				strParams.TrimLeft(); strParams.TrimRight();

		//				if (strContentType == "$TEXT")
		//					pObjectSource->m_strSystemCreationInfo.Format(_T("$TEXT %s"), m_strOutputText);

		//				pObjectSource->m_nType = CPageSource::SystemCreated;
		//			}

		//			m_printColorControl.SaveData(pTemplate, pObjectSource, nObjectSourceIndex);

		//			//pTemplate->m_ColorSeparations.RemoveAll();
		//			//pTemplate->m_ColorSeparations.Append(pMark->m_markTemplate.m_ColorSeparations);

		//			//CImpManDoc* pDoc			 = CImpManDoc::GetDoc();
		//			//short		nMarkSourceIndex = (pDoc) ? pDoc->m_MarkSourceList.FindMarkSourceIndex(pMark) : -1;
		//			//for (int i = 0; i < pTemplate->m_ColorSeparations.GetSize(); i++)
		//			//	pTemplate->m_ColorSeparations[i].m_nPageSourceIndex = nMarkSourceIndex;
		//			//if (pTemplate->m_ColorSeparations.GetSize())
		//			//{
		//			//	CPageSource* pObjectSource = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
		//			//	if (pObjectSource)
		//			//		*pObjectSource = pMark->m_MarkSource;
		//			//}
		//		}
		//	}
		//}
	}
}

void CDlgTextMarkObjectContent::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
		{
			CheckTextmarkForFoldsheetView();
			SaveData();
		}
}

void CDlgTextMarkObjectContent::OnButtonSelectPlaceholder() 
{
	m_placeholderEdit.SetFocus();
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup)
	{
		CRect buttonRect;
		GetDlgItem(IDC_BUTTON_SELECT_PLACEHOLDER)->GetWindowRect(buttonRect);
		pPopup->TrackPopupMenu(TPM_RIGHTALIGN | TPM_RIGHTBUTTON, buttonRect.right, buttonRect.bottom, this);
	}

	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_PLACEHOLDER))->SetCheck(0);
}

void CDlgTextMarkObjectContent::OnSelectPlaceholder(int nID)
{
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if ( ! pPopup)
		return;

	CString strPlaceholder;
	pPopup->GetMenuString(nID, strPlaceholder, MF_BYCOMMAND);
	strPlaceholder = strPlaceholder.Left(3);
	strPlaceholder.Delete(1, 1);	// remove &

	UpdateData(TRUE);

	int nStartChar, nEndChar;
	((CEdit*)GetDlgItem(IDC_TEXTMARKSTRING))->GetSel(nStartChar, nEndChar);

	if (nStartChar == nEndChar)
		if (nStartChar > 0)
			if (m_strOutputText[nStartChar - 1] == '$')		// assume user already typed $ -> remove $ to prevent from being inserted twice
				nStartChar--;

	m_strOutputText.Delete(nStartChar, nEndChar - nStartChar);
	m_strOutputText.Insert(nStartChar, strPlaceholder);

	UpdateData(FALSE);

	((CEdit*)GetDlgItem(IDC_TEXTMARKSTRING))->SetSel(nStartChar + 2, nStartChar + 2);

	Apply();
	m_pParent->UpdateView();
}

void CDlgTextMarkObjectContent::OnPlaceholderFoldsheetNum()		{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSHEETNUM);		}
void CDlgTextMarkObjectContent::OnPlaceholderSheetside()		{ OnSelectPlaceholder(ID_PLACEHOLDER_SHEETSIDE);		}		
void CDlgTextMarkObjectContent::OnPlaceholderColorName()		{ OnSelectPlaceholder(ID_PLACEHOLDER_COLORNAME);		}
void CDlgTextMarkObjectContent::OnPlaceholderWebNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_WEBNUM);			}
void CDlgTextMarkObjectContent::OnPlaceholderColorNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_COLORNUM);			}	
void CDlgTextMarkObjectContent::OnPlaceholderFoldsheetNumCG()	{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSHEETNUM_CG);	}
void CDlgTextMarkObjectContent::OnPlaceholderJobTitle()			{ OnSelectPlaceholder(ID_PLACEHOLDER_JOBTITLE);			}
void CDlgTextMarkObjectContent::OnPlaceholderTileNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_TILENUM);			}
void CDlgTextMarkObjectContent::OnPlaceholderSheetNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_SHEETNUM);			}
void CDlgTextMarkObjectContent::OnPlaceholderFile()				{ OnSelectPlaceholder(ID_PLACEHOLDER_FILE);				}
void CDlgTextMarkObjectContent::OnPlaceholderLowpageSide()		{ OnSelectPlaceholder(ID_PLACEHOLDER_LOWPAGE_SIDE);		}
void CDlgTextMarkObjectContent::OnPlaceholderLowpageSheet()		{ OnSelectPlaceholder(ID_PLACEHOLDER_LOWPAGE_SHEET);	}
void CDlgTextMarkObjectContent::OnPlaceholderHighpageSide()		{ OnSelectPlaceholder(ID_PLACEHOLDER_HIGHPAGE_SIDE);	}
void CDlgTextMarkObjectContent::OnPlaceholderHighpageSheet()	{ OnSelectPlaceholder(ID_PLACEHOLDER_HIGHPAGE_SHEET);	}
void CDlgTextMarkObjectContent::OnPlaceholderPressDevice()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PRESSDEVICE);		}
void CDlgTextMarkObjectContent::OnPlaceholderJob()				{ OnSelectPlaceholder(ID_PLACEHOLDER_JOB);				}
void CDlgTextMarkObjectContent::OnPlaceholderJobNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_JOBNUM);			}
void CDlgTextMarkObjectContent::OnPlaceholderCustomer()			{ OnSelectPlaceholder(ID_PLACEHOLDER_CUSTOMER);			}
void CDlgTextMarkObjectContent::OnPlaceholderCustomerNum()		{ OnSelectPlaceholder(ID_PLACEHOLDER_CUSTOMERNUM);		}
void CDlgTextMarkObjectContent::OnPlaceholderCreator()			{ OnSelectPlaceholder(ID_PLACEHOLDER_CREATOR);			}
void CDlgTextMarkObjectContent::OnPlaceholderCreationDate()		{ OnSelectPlaceholder(ID_PLACEHOLDER_CREATIONDATE);		}
void CDlgTextMarkObjectContent::OnPlaceholderLastModified()		{ OnSelectPlaceholder(ID_PLACEHOLDER_LASTMODIFIED);		}
void CDlgTextMarkObjectContent::OnPlaceholderScheduledToPrint()	{ OnSelectPlaceholder(ID_PLACEHOLDER_SCHEDULEDTOPRINT);	}
void CDlgTextMarkObjectContent::OnPlaceholderOutputDate()		{ OnSelectPlaceholder(ID_PLACEHOLDER_OUTPUTDATE);		}
void CDlgTextMarkObjectContent::OnPlaceholderNotes()			{ OnSelectPlaceholder(ID_PLACEHOLDER_NOTES);			}
void CDlgTextMarkObjectContent::OnPlaceholderAsir()				{ OnSelectPlaceholder(ID_PLACEHOLDER_ASIR);				}
void CDlgTextMarkObjectContent::OnPlaceholderPageNumList()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAGENUMLIST);		}
void CDlgTextMarkObjectContent::OnPlaceholderPageNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_PAGENUM);			}
void CDlgTextMarkObjectContent::OnPlaceholderProductName()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PRODUCTNAME);		}
void CDlgTextMarkObjectContent::OnPlaceholderProductPartName()	{ OnSelectPlaceholder(ID_PLACEHOLDER_PRODUCTPARTNAME);	}
void CDlgTextMarkObjectContent::OnPlaceholderPaperGrain()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERGRAIN);		}
void CDlgTextMarkObjectContent::OnPlaceholderPaperType()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERTYPE);		}
void CDlgTextMarkObjectContent::OnPlaceholderPaperGrammage()	{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERGRAMMAGE);	}
void CDlgTextMarkObjectContent::OnPlaceholderWorkStyle()		{ OnSelectPlaceholder(ID_PLACEHOLDER_WORKSTYLE);		}
void CDlgTextMarkObjectContent::OnPlaceholderFoldScheme()		{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSCHEME);		}
void CDlgTextMarkObjectContent::OnPlaceholderNumFoldsheets()	{ OnSelectPlaceholder(ID_PLACEHOLDER_NUMFOLDSHEETS);	}
void CDlgTextMarkObjectContent::OnPlaceholderBlank()			{ OnSelectPlaceholder(ID_PLACEHOLDER_BLANK);			}


BOOL CDlgTextMarkObjectContent::OnToolTipNeedText(UINT /*id*/, NMHDR * pNMHDR, LRESULT * /*pResult*/)
{
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if ( ! pPopup)
		return FALSE;

	CString strPlaceholder = m_placeholderEdit.GetPlaceholderHit();

	if (strPlaceholder.IsEmpty())
		return FALSE;

	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	pTTT->szText[0] = 0;

	strPlaceholder.Insert(1, '&');
	for (UINT i = 0; i < pPopup->GetMenuItemCount(); i++)
	{
		CString strMenu;
		pPopup->GetMenuString(i, strMenu, MF_BYPOSITION);
		if (strPlaceholder == strMenu.Left(3))
		{
			strMenu = strMenu.Right(strMenu.GetLength() - 4);
			_tcscpy(pTTT->szText, strMenu);
			return TRUE;
		}
	}

	return FALSE;
}

void CDlgTextMarkObjectContent::OnPaint() 
{
	CDialog::OnPaint();

	m_placeholderEdit.Invalidate();
	m_placeholderEdit.UpdateWindow();

	m_printColorControl.Invalidate();
	m_printColorControl.UpdateWindow();
}

void CDlgTextMarkObjectContent::OnBnClickedButtonPlaceholderInfo()
{
	CDlgPlaceholderSyntaxInfo dlg;
	dlg.DoModal();
}
