// ProdDataProcessOverView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "ProdDataProcessOverView.h"
#include "GraphicComponent.h"
#include "BookBlockView.h"
#include "BookBlockFrame.h"
#include "ModalFrame.h"
#include "NewPrintSheetFrame.h"
#include "MainFrm.h"
#include "DlgJobData.h"



// CPPPCustomPaintFrame

IMPLEMENT_DYNAMIC(CPPPCustomPaintFrame, CStatic)

CPPPCustomPaintFrame::CPPPCustomPaintFrame()
{
}

CPPPCustomPaintFrame::~CPPPCustomPaintFrame()
{
}

BEGIN_MESSAGE_MAP(CPPPCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CPPPCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CProdDataProcessOverView* pView = (CProdDataProcessOverView*)GetParent();
	if ( ! pView)
		return;
	CRect rcFrame;
	GetClientRect(rcFrame);
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nIDClip = -1, nIDClip2 = -1;
	switch (GetDlgCtrlID())
	{
	case IDC_PD_PRODUCT_FRAME:				nIDClip  = IDC_PD_GOTO_PRODUCTDATA;	
											nIDClip2 = IDC_PD_GOTO_PRODUCTIONDATA;	break;

	case IDC_PD_BOOKBLOCK_FRAME:			nIDClip  = IDC_PD_GOTO_BOOKBLOCK;		break;

	default:								break;
	}

	CPen pen(PS_SOLID, 1, MIDDLEGRAY);
	dc.SelectObject(&pen);
	CBrush brush(RGB(237,240,241));//RGB(250,250,245));//WHITE);//RGB(250, 250, 245));
	dc.SelectObject(&brush);

	if (nIDClip != -1)
	{
		dc.SaveDC();	// save current clipping region
		CRect rcClip;
		pView->GetDlgItem(nIDClip)->GetWindowRect(rcClip);
		ScreenToClient(rcClip);
		dc.ExcludeClipRect(rcClip);
		if (nIDClip2 != -1)
		{
			pView->GetDlgItem(nIDClip2)->GetWindowRect(rcClip);
			ScreenToClient(rcClip);
			dc.ExcludeClipRect(rcClip);
		}
		dc.RoundRect(rcFrame, CPoint(4,4));
		dc.RestoreDC(-1);
	}
	else
		dc.RoundRect(rcFrame, CPoint(4,4));

	switch (GetDlgCtrlID())
	{
	case IDC_PD_PRODUCT_FRAME:				pDoc->m_FinalProductDescription.Draw(&dc, rcFrame);
											break;

	case IDC_PD_BOOKBLOCK_FRAME:			DrawBookblockPreview(&dc, rcFrame);	
											break;

	default:								break;
	}
}

void CPPPCustomPaintFrame::DrawBookblockPreview(CDC* pDC, CRect rcFrame)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CRect   rcText;
	CString strOut, string;
	CFont	font;
	font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	pDC->SetBkMode(TRANSPARENT);

	strOut = pDoc->m_FinalProductDescription.m_bindingDefs.m_strName;

	rcText = rcFrame; rcText.DeflateRect(10,5); rcText.left += 15; rcText.bottom = rcText.top + 15; 
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER);

	CRect rcThumbnail = rcFrame;
	//rcThumbnail.left -= 10; rcThumbnail.top += 10; 
	pDoc->m_Bookblock.Draw(pDC, rcThumbnail, -1, L"");

	font.DeleteObject();
}



// CPDOFoldSheetListCtrl

IMPLEMENT_DYNAMIC(CPDOFoldSheetListCtrl, CListCtrl)

CPDOFoldSheetListCtrl::CPDOFoldSheetListCtrl()
{
	m_nHighlight = HIGHLIGHT_ROW;	
}

CPDOFoldSheetListCtrl::~CPDOFoldSheetListCtrl()
{
}


BEGIN_MESSAGE_MAP(CPDOFoldSheetListCtrl, CListCtrl)
	ON_WM_MEASUREITEM_REFLECT()
	ON_NOTIFY_REFLECT(NM_CLICK, &CPDOFoldSheetListCtrl::OnNMClick)
END_MESSAGE_MAP()



// CPDOFoldSheetListCtrl-Meldungshandler

void CPDOFoldSheetListCtrl::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	lpMeasureItemStruct->itemHeight = 25;
}

void CPDOFoldSheetListCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	int nItem = lpDrawItemStruct->itemID;

	// Save dc state
	int nSavedDC = pDC->SaveDC();

	CFont font;
	font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	pDC->SelectObject(&font);

	// Get item image and state info
	LV_ITEM lvi;
	lvi.mask = LVIF_STATE | LVIF_PARAM;
	lvi.iItem = nItem;
	lvi.iSubItem = 0;
	lvi.stateMask = 0xFFFF;		// get all state flags
	GetItem(&lvi);

	// Should the item be highlighted
	BOOL bHighlight = ((lvi.state & LVIS_DROPHILITED) || ( (lvi.state & LVIS_SELECTED) && ((GetFocus() == this))) );
//	BOOL bHighlight = ( (lvi.state & LVIS_SELECTED) && (GetFocus() == this) );

	if (GetStyle() & LVS_SHOWSELALWAYS)
	{
		POSITION pos = GetFirstSelectedItemPosition();
		if (GetNextSelectedItem(pos) == lvi.iItem)
			bHighlight = TRUE;
	}

	// Get rectangles for drawing
	CRect rcBounds, rcLabel, rcIcon;
	GetItemRect(nItem, rcBounds, LVIR_BOUNDS);
	GetItemRect(nItem, rcLabel, LVIR_LABEL);
	GetItemRect(nItem, rcIcon, LVIR_ICON);
	CRect rcCol( rcBounds );

	CString sLabel = GetItemText( nItem, 0 );

	// Labels are offset by a certain amount  
	// This offset is related to the width of a space character
	int offset = pDC->GetTextExtent(_T(" "), 1 ).cx*2;

	CRect rcHighlight;
	CRect rcWnd;
	GetClientRect(&rcWnd);
	int nExt;
	switch( m_nHighlight )
	{
	case 0:
		nExt = pDC->GetOutputTextExtent(sLabel).cx + offset;
		rcHighlight = rcLabel;
		if( rcLabel.left + nExt < rcLabel.right )
			rcHighlight.right = rcLabel.left + nExt;
		break;
	case 1:
		rcHighlight = rcBounds;
		rcHighlight.left = rcLabel.left;
		break;
	case 2:
		rcHighlight = rcBounds;
//		rcHighlight.left = rcLabel.left;
//		rcHighlight.right = rcWnd.right;
		break;
	default:
		rcHighlight = rcLabel;
	}

	CFoldSheet* pFoldSheet = (CFoldSheet*)lvi.lParam;
	CLayout*	pLayout	   = (pFoldSheet) ? pFoldSheet->GetLayout(0) : NULL;

	// Draw the background color
	if( bHighlight )
	{
		pDC->SetTextColor(DARKBLUE);//::GetSysColor(COLOR_HIGHLIGHTTEXT));
		pDC->SetBkColor(RGB(193,211,251));//::GetSysColor(COLOR_HIGHLIGHT));

		pDC->FillRect(rcHighlight, &CBrush(RGB(193,211,251)));//::GetSysColor(COLOR_HIGHLIGHT)));
	}
	else
	{
		pDC->FillRect(rcHighlight, &CBrush(::GetSysColor(COLOR_WINDOW)));
	}

	// Set clip region
	rcCol.right = rcCol.left + GetColumnWidth(0);
	CRgn rgn;
	rgn.CreateRectRgnIndirect(&rcCol);
	pDC->SelectClipRgn(&rgn);
	rgn.DeleteObject();

	// Draw item label - Column 0
	rcLabel.left  += offset/2; 
	rcLabel.right -= offset;

	rcIcon.right = rcIcon.left + rcIcon.Height();
	rcIcon.DeflateRect(0, 2);
	//rcLabel.left = rcIcon.right + 5;

	CGraphicComponent gp;
	gp.DrawTransparentFrame(pDC, rcBounds, RGB(120,180,0));
	pDC->DrawText(sLabel,-1,rcLabel,DT_LEFT | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER | DT_END_ELLIPSIS);


	// Draw labels for remaining columns
	LV_COLUMN lvc;
	lvc.mask = LVCF_FMT | LVCF_WIDTH;

	if ( m_nHighlight == HIGHLIGHT_NORMAL )		// Highlight only first column
	{
		pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));
		pDC->SetBkColor(::GetSysColor(COLOR_WINDOW));
	}

	rcBounds.right = rcHighlight.right > rcBounds.right ? rcHighlight.right :
							rcBounds.right;
	rgn.CreateRectRgnIndirect(&rcBounds);
	pDC->SelectClipRgn(&rgn);

	for(int nColumn = 1; GetColumn(nColumn, &lvc); nColumn++)
	{
		rcCol.left = rcCol.right;
		rcCol.right += lvc.cx;

		// Draw the background if needed
		if ( m_nHighlight == HIGHLIGHT_NORMAL )
			pDC->FillRect(rcCol, &CBrush(::GetSysColor(COLOR_WINDOW)));

		sLabel = GetItemText(nItem, nColumn);
		if (sLabel.GetLength() == 0)
			continue;

		// Get the text justification
		UINT nJustify = DT_LEFT;
		switch(lvc.fmt & LVCFMT_JUSTIFYMASK)
		{
		case LVCFMT_RIGHT:
			nJustify = DT_RIGHT;
			break;
		case LVCFMT_CENTER:
			nJustify = DT_CENTER;
			break;
		default:
			break;
		}

		rcLabel = rcCol;
		rcLabel.left += offset;
		rcLabel.right -= offset;

		if (nColumn == 2)
		{
			rcIcon = rcLabel;
			rcIcon.right = rcIcon.left + 30;
			if (pFoldSheet)
				if ( ! pFoldSheet->IsEmpty())
					pFoldSheet->DrawThumbnail(pDC, rcIcon);

			pDC->DrawText(sLabel, -1, rcLabel + CSize(35,0), nJustify | DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS);
		}
		else
			pDC->DrawText(sLabel, -1, rcLabel, nJustify | DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS);
	}

	// Draw focus rectangle if item has focus
	//if (lvi.state & LVIS_FOCUSED && (GetFocus() == this))
	//	pDC->DrawFocusRect(rcHighlight);

	font.DeleteObject();
	// Restore dc
	pDC->RestoreDC( nSavedDC );
}

void CPDOFoldSheetListCtrl::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMITEMACTIVATE* nm = (NMITEMACTIVATE*)pNMHDR;
	*pResult = 0;

	if (nm->iItem < 0)
		return;

	// Get item image and state info
	LV_ITEM lvi;
	lvi.mask = LVIF_STATE | LVIF_PARAM;
	lvi.iItem = nm->iItem;
	lvi.iSubItem = 0;
	lvi.stateMask = 0xFFFF;		// get all state flags
	GetItem(&lvi);

	CFoldSheet* pFoldSheet = (CFoldSheet*)lvi.lParam;
	if ( ! pFoldSheet)
		return;
	CPrintSheetPlanningView* pPrintSheetPlanningView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	if ( ! pPrintSheetPlanningView)
		return;
	pPrintSheetPlanningView->m_DisplayList.DeselectAll();
	pPrintSheetPlanningView->m_DisplayList.InvalidateItem();

	CDisplayItem*	pDIScrollTo = NULL;
	int				nLayerIndex	= 0;
	CPrintSheet*	pPrintSheet	= pFoldSheet->GetPrintSheet(nLayerIndex);
	while (pPrintSheet)
	{
		for (int nLayerRefIndex = 0; nLayerRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nLayerRefIndex++)
		{
			if (pPrintSheet->GetFoldSheet(nLayerRefIndex) == pFoldSheet)
			{
				CDisplayItem* pDISelect = pPrintSheetPlanningView->m_DisplayList.GetItemFromData((void*)nLayerRefIndex, (void*)pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
				if (pDISelect)
				{
					pDISelect->m_nState = CDisplayItem::Selected;
					pPrintSheetPlanningView->m_DisplayList.InvalidateItem(pDISelect);
					if ( ! pDIScrollTo)
						pDIScrollTo = pDISelect;
				}
			}
		}
		pPrintSheet	= pFoldSheet->GetNextPrintSheet(pPrintSheet, nLayerIndex);
	}
	if (pDIScrollTo)	
		if ( ! pDIScrollTo->m_bIsVisible)	
		{
			pPrintSheetPlanningView->Invalidate();
			pPrintSheetPlanningView->OnHScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.left - 20, NULL);
			pPrintSheetPlanningView->OnVScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.top  - 20, NULL);
		}
	pPrintSheetPlanningView->UpdateWindow();
}



// CProdDataProcessOverView

IMPLEMENT_DYNCREATE(CProdDataProcessOverView, CFormView)

CProdDataProcessOverView::CProdDataProcessOverView()
	: CFormView(CProdDataProcessOverView::IDD)
{
	m_bigFont.CreateFont(18, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
}

CProdDataProcessOverView::~CProdDataProcessOverView()
{
	m_bigFont.DeleteObject();
}

void CProdDataProcessOverView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CProdDataProcessOverView, CFormView)
	ON_NOTIFY(NM_KILLFOCUS, IDC_PD_OVERVIEW_FOLDSHEETLIST, &CProdDataProcessOverView::OnNMKillfocusPdOverviewFoldsheetlist)
	ON_NOTIFY(NM_SETFOCUS, IDC_PD_OVERVIEW_FOLDSHEETLIST, &CProdDataProcessOverView::OnNMSetfocusPdOverviewFoldsheetlist)
	ON_NOTIFY(NM_DBLCLK, IDC_PD_OVERVIEW_FOLDSHEETLIST, &CProdDataProcessOverView::OnNMDblclkPdOverviewFoldsheetlist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PD_OVERVIEW_FOLDSHEETLIST, &CProdDataProcessOverView::OnLvnItemchangedPdOverviewFoldsheetlist)
	ON_BN_CLICKED(IDC_PD_GOTO_PRODUCTDATA, OnBnClickedGotoProductdata)
	ON_BN_CLICKED(IDC_PD_GOTO_PRODUCTIONDATA, OnBnClickedGotoProductiondata)
	ON_BN_CLICKED(IDC_PD_GOTO_BOOKBLOCK, OnBnClickedGotoBookblock)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CProdDataProcessOverView-Diagnose

#ifdef _DEBUG
void CProdDataProcessOverView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CProdDataProcessOverView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CProdDataProcessOverView-Meldungshandler

void CProdDataProcessOverView::OnInitialUpdate()
{
	m_productPreview.SubclassDlgItem	 (IDC_PD_PRODUCT_FRAME,				this);
	m_bookBlockPreview.SubclassDlgItem	 (IDC_PD_BOOKBLOCK_FRAME,			this);
	m_printComponentsList.SubclassDlgItem(IDC_PD_OVERVIEW_FOLDSHEETLIST,	this);

	m_gotoProductDataButton.SubclassDlgItem		(IDC_PD_GOTO_PRODUCTDATA,		this);
	m_gotoProductDataButton.SetBitmap			(IDB_ARROW_RIGHT_HOT, IDB_ARROW_RIGHT_COLD, -1, XCENTER);
	m_gotoProductDataButton.SetTransparent		(TRUE);
	m_gotoProductDataButton.AlignToBitmap();

	m_gotoProductionDataButton.SubclassDlgItem	(IDC_PD_GOTO_PRODUCTIONDATA,	this);
	m_gotoProductionDataButton.SetBitmap		(IDB_ARROW_RIGHT_HOT, IDB_ARROW_RIGHT_COLD, -1, XCENTER);
	m_gotoProductionDataButton.SetTransparent	(TRUE);
	m_gotoProductionDataButton.AlignToBitmap();

	m_gotoBookblockButton.SubclassDlgItem		(IDC_PD_GOTO_BOOKBLOCK,			this);
	m_gotoBookblockButton.SetBitmap				(IDB_ARROW_RIGHT_HOT, IDB_ARROW_RIGHT_COLD, -1, XCENTER);
	m_gotoBookblockButton.SetTransparent		(TRUE);
	m_gotoBookblockButton.AlignToBitmap();

///// foldsheet list /////////////////////////////////////////////////////////////////////////////////////////////

	ListView_SetExtendedListViewStyle(m_printComponentsList.m_hWnd, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	CString string;
	int nIndex = 0;
	string.LoadString(IDS_PRODUCTPART);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_PAGELISTHEADER_PAGES);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,  120, 0);
	string.LoadString(IDS_FOLDSHEET_LABEL);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_SCHEME);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,  150, 0);
	string.LoadString(IDS_NUP);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,   50, 0);
	string.LoadString(IDS_PRINTSHEET_TITLE);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,   80, 0);
	string.LoadString(IDS_PRINTSHEETLAYOUT);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_MACHINE);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,   90, 0);
	string.LoadString(IDS_PLATESIZE);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_PAPERSIZE);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_TYPEOFPRINTING);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_TYPEOFTURNING);
	m_printComponentsList.InsertColumn(nIndex++,			string, LVCFMT_LEFT,  100, 0);

	CFormView::OnInitialUpdate();

	UpdateData(FALSE);
}

void CProdDataProcessOverView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_bookBlockPreview.Invalidate();
	m_bookBlockPreview.UpdateWindow();
	m_productPreview.Invalidate();
	m_productPreview.UpdateWindow();
	InitializeFoldSheetList();
}

void CProdDataProcessOverView::InitializeFoldSheetList()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = m_printComponentsList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_printComponentsList.GetNextSelectedItem(pos) : 0;

	LVITEM   lvitem;
	int		 nSubItem = 0;
	CString  string, strMeasureFormat;

	lvitem.iItem = 0;

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, -1);

	for (int i = 0; i < sortedFoldSheets.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = sortedFoldSheets[i];
		if ( ! pFoldSheet)
			continue;
		CFoldSheetLayer* pLayer = pFoldSheet->GetLayer(0);
		CPrintSheet* pPrintSheet = (pLayer) ? pLayer->GetFirstPrintSheet() : NULL;
		if ( ! pPrintSheet)
			continue;
		pPrintSheet->InitializeFoldSheetTypeList();
		CLayout*	  pLayout	   = pPrintSheet->GetLayout();
		CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
		if ( ! pLayout || ! pPressDevice)
			continue;

		nSubItem = 0;

		CProductPart* pProductPart = pDoc->m_productPartList.GetPart(pFoldSheet->m_nProductPartIndex);
		string = (pProductPart) ? pProductPart->m_strName : L"";
		lvitem.iSubItem = nSubItem++;
		lvitem.mask	    = LVIF_TEXT | LVIF_PARAM;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		lvitem.lParam	= (LPARAM)pFoldSheet;
		if (lvitem.iItem < m_printComponentsList.GetItemCount())
			m_printComponentsList.SetItem(&lvitem); 
		else
			m_printComponentsList.InsertItem(&lvitem); 

		lvitem.mask	    = LVIF_TEXT;
		string = pFoldSheet->GetPageRange();
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		string.Format("%s", pFoldSheet->GetNumber());
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		string = pFoldSheet->m_strFoldSheetTypeName;
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		string.Format("%d", pFoldSheet->GetNUpPrintLayout(pPrintSheet, 0));
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		string.Format("%s", (pPrintSheet) ? pPrintSheet->GetNumber() : "");
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		string.Format("%s", (pLayout) ? pLayout->m_strLayoutName : "");
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		string = pPressDevice->m_strName;
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		strMeasureFormat.Format("%s x %s", (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
		string.Format((LPCTSTR)strMeasureFormat, MeasureFormat((float)pPressDevice->m_fPlateWidth), MeasureFormat((float)pPressDevice->m_fPlateHeight));
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		string.Format(strMeasureFormat, MeasureFormat((float)pLayout->m_FrontSide.m_fPaperWidth), MeasureFormat((float)pLayout->m_FrontSide.m_fPaperHeight));
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		string = pLayout->GetPrintingType();
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		string = pLayout->GetTurningType();
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_printComponentsList.SetItem(&lvitem); 

		lvitem.iItem++;
	}

	for (int nProductPartIndex = 0; nProductPartIndex < pDoc->m_productPartList.GetSize(); nProductPartIndex++)
	{
		int nUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(nProductPartIndex);
		nSubItem = 0;
		if (nUnImposedPages > 0)
		{
			CFoldSheet*	  pDummyFoldSheet = new CFoldSheet;	
			CProductPart* pProductPart	  = pDoc->m_productPartList.GetPart(nProductPartIndex);
			CPressDevice* pPressDevice	  = theApp.m_PressDeviceList.GetDevice(pProductPart->GetDefaultPressDeviceName());
			CSheet		  sheet;
			CPressDevice::FindPaper(sheet, pProductPart->GetDefaultPaperName());
			pDummyFoldSheet->m_nProductPartIndex = nProductPartIndex;

			string = pDoc->m_productPartList[nProductPartIndex].m_strName;
			lvitem.iSubItem = nSubItem++;
			lvitem.mask	    = LVIF_TEXT | LVIF_PARAM;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);;
			lvitem.lParam	= (LPARAM)pDummyFoldSheet;
			if (lvitem.iItem < m_printComponentsList.GetItemCount())
				m_printComponentsList.SetItem(&lvitem); 
			else
				m_printComponentsList.InsertItem(&lvitem); 

			CString string2;
			string2.LoadStringA(IDS_PAGES_HEADER);
			string.Format(_T("%d %s"), nUnImposedPages, string2);
			lvitem.mask	    = LVIF_TEXT;
			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_printComponentsList.SetItem(&lvitem); 

			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = _T("");
			m_printComponentsList.SetItem(&lvitem); 

			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = _T("");
			m_printComponentsList.SetItem(&lvitem); 

			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = _T("");
			m_printComponentsList.SetItem(&lvitem); 

			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = _T("");
			m_printComponentsList.SetItem(&lvitem); 

			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = _T("");
			m_printComponentsList.SetItem(&lvitem); 

			string = (pPressDevice) ? pPressDevice->m_strName : L"";
			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_printComponentsList.SetItem(&lvitem); 

			if (pPressDevice)
			{
				strMeasureFormat.Format("%s x %s", (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
				string.Format((LPCTSTR)strMeasureFormat, MeasureFormat((float)pPressDevice->m_fPlateWidth), MeasureFormat((float)pPressDevice->m_fPlateHeight));
			}
			else
				string = L"";
			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_printComponentsList.SetItem(&lvitem); 

			if ((sheet.m_fWidth != 0.0f) || (sheet.m_fHeight != 0.0f))
				string.Format(strMeasureFormat, MeasureFormat((float)sheet.m_fWidth), MeasureFormat((float)sheet.m_fHeight));
			else
				string = L"";
			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			m_printComponentsList.SetItem(&lvitem); 

			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = _T("");
			m_printComponentsList.SetItem(&lvitem); 

			lvitem.iSubItem = nSubItem++;
			lvitem.pszText  = _T("");
			m_printComponentsList.SetItem(&lvitem); 

			lvitem.iItem++;
		}
	}

	for (int i = lvitem.iItem + 1; i < m_printComponentsList.GetItemCount(); i++)
		m_printComponentsList.DeleteItem(i);

	if ( ! m_printComponentsList.GetFirstSelectedItemPosition())
		m_printComponentsList.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

 // Cause WM_MEASUREITEM to be sent by pretending to move CListCtrls window
	CRect rc;
	m_printComponentsList.GetWindowRect( &rc );
	WINDOWPOS wp;
	wp.hwnd = m_hWnd;
	wp.cx = rc.Width();
	wp.cy = rc.Height();
	wp.flags = SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER |
	SWP_NOZORDER;
	m_printComponentsList.SendMessage(WM_WINDOWPOSCHANGED, 0, (LPARAM)&wp);
}

void CProdDataProcessOverView::OnNMKillfocusPdOverviewFoldsheetlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;
}

void CProdDataProcessOverView::OnNMSetfocusPdOverviewFoldsheetlist(NMHDR* /*pNMHDR*/, LRESULT* /*pResult*/)
{
//	*pResult = 0;
}

void CProdDataProcessOverView::OnLvnItemchangedPdOverviewFoldsheetlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	OnNMSetfocusPdOverviewFoldsheetlist(NULL, NULL);

	*pResult = 0;
}

void CProdDataProcessOverView::OnNMDblclkPdOverviewFoldsheetlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;
}

CFoldSheet* CProdDataProcessOverView::GetSelectedFoldSheet()
{
	if ( ! m_printComponentsList.m_hWnd)
		return NULL;

	POSITION pos = m_printComponentsList.GetFirstSelectedItemPosition();
	int nSel = m_printComponentsList.GetNextSelectedItem(pos);
	if ( nSel >= 0)
	{
		return (CFoldSheet*)m_printComponentsList.GetItemData(nSel);
	}
	return NULL;
}

CPrintSheet* CProdDataProcessOverView::GetSelectedPrintSheet()
{
	if ( ! m_printComponentsList.m_hWnd)
		return NULL;

	POSITION pos = m_printComponentsList.GetFirstSelectedItemPosition();
	int nSel = m_printComponentsList.GetNextSelectedItem(pos);
	if ( nSel >= 0)
	{
		int nLayerIndex = 0;
		CFoldSheet* pFoldSheet = (CFoldSheet*)m_printComponentsList.GetItemData(nSel);
		if (pFoldSheet)
			return pFoldSheet->GetPrintSheet(nLayerIndex);
	}
	return NULL;
}

void CProdDataProcessOverView::OnBnClickedGotoProductdata()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgJobData dlgJobData(IDD_PRODUCTDATA, (ULONG_PTR)0);//nProductPartIndex);
	if (dlgJobData.DoModal() == IDCANCEL)
		return;
	else
		pDoc->SetModifiedFlag();

	pDoc->UpdateAllViews(NULL);
}

void CProdDataProcessOverView::OnBnClickedGotoProductiondata()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgJobData dlgJobData(IDD_PRODUCTIONPLANNER, (ULONG_PTR)0);//nProductPartIndex);
	if (dlgJobData.DoModal() == IDCANCEL)
		return;
	else
		pDoc->SetModifiedFlag();

	pDoc->UpdateAllViews(NULL);
}

void CProdDataProcessOverView::OnBnClickedGotoBookblock()
{
	CImpManDoc*	 pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CCreateContext context;
	context.m_pCurrentDoc = pDoc;
	context.m_pNewViewClass = RUNTIME_CLASS(CBookBlockView);

	CMemFile memFile;
	theApp.UndoSave(&memFile);
	if ( ! CModalFrame::Run(*RUNTIME_CLASS(CBookblockFrame), FALSE, IDR_BOOKBLOCKFRAME, WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, this, &context))
		theApp.UndoRestore(&memFile);
	else
	{
		pDoc->SetModifiedFlag();
		pDoc->UpdateAllViews(NULL);
	}
}

void CProdDataProcessOverView::OnSize(UINT nType, int cx, int cy)
{
	CRect rcClient, rcFrame;
	GetClientRect(rcClient);
	if (m_printComponentsList.m_hWnd)
	{
		CRect rcProductPreview;
		m_productPreview.GetWindowRect(rcProductPreview); ScreenToClient(rcProductPreview); 
		m_printComponentsList.GetWindowRect(rcFrame); ScreenToClient(rcFrame); rcFrame.right = rcClient.Width() - rcProductPreview.left;
		m_printComponentsList.MoveWindow(rcFrame);
	}

	CFormView::OnSize(nType, cx, cy);
}
