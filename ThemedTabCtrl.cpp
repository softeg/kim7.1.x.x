// ThemedTabCtrl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "ThemedTabCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CThemedTabCtrl

CThemedTabCtrl::CThemedTabCtrl()
{
}

CThemedTabCtrl::~CThemedTabCtrl()
{
}


BEGIN_MESSAGE_MAP(CThemedTabCtrl, CTabCtrl)
	//{{AFX_MSG_MAP(CThemedTabCtrl)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CThemedTabCtrl 



void CThemedTabCtrl::GetThemedTabCtrlBackground()
{
    // Only do this if the theme is active
    //if (m_bThemeActive)
	if (1)
    {
        RECT rc;

        // Get tab control dimensions
        GetWindowRect(&rc);

        // Get the tab control DC
		CDC* pDC = GetDC();
        HDC hDC = pDC->m_hDC;

        // Create a compatible DC
        HDC hDCMem = ::CreateCompatibleDC(hDC);
        HBITMAP hBmp = ::CreateCompatibleBitmap(hDC, 
               rc.right - rc.left, rc.bottom - rc.top);
        HBITMAP hBmpOld = (HBITMAP)(::SelectObject(hDCMem, hBmp));

        // Tell the tab control to paint in our DC
        SendMessage(WM_PRINTCLIENT, (WPARAM)(hDCMem), 
           (LPARAM)(PRF_ERASEBKGND | PRF_CLIENT | PRF_NONCLIENT));

        // Create a pattern brush from the bitmap selected in our DC
        m_hBrush = ::CreatePatternBrush(hBmp);

        // Restore the bitmap
        ::SelectObject(hDCMem, hBmpOld);

        // Cleanup
        ::DeleteObject(hBmp);
        ::DeleteDC(hDCMem);
        ReleaseDC(pDC);
    }
}

void CThemedTabCtrl::OnSize(UINT nType, int cx, int cy) 
{
	CTabCtrl::OnSize(nType, cx, cy);
	
	GetThemedTabCtrlBackground();
}
