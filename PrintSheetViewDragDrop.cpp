// PrintSheetViewDragDrop.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgLayoutObjectControl.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PrintSheetTableView.h"
#include "PrintSheetMarksView.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "DlgMarkSet.h"
#include "ProdDataPrintView.h"
#include "PrintSheetNavigationView.h"
#include "PrintSheetPlanningView.h"
#include "PrintComponentsView.h"
#include "PrintSheetComponentsView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



DROPEFFECT CPrintSheetView::OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return DROPEFFECT_NONE;

	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	DROPEFFECT de = ((dwKeyState & MK_CONTROL) == MK_CONTROL) ? DROPEFFECT_COPY : DROPEFFECT_MOVE;

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, GroupTracker)))
	{
		CDisplayItem* pSelectedDI = m_DisplayList.GetFirstSelectedItem();
		while (pSelectedDI)
		{
			if ( (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject))) || (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet))) )
			{
				pSelectedDI->m_bIsDragging	= TRUE;
				pSelectedDI->m_dragPoint	= pSelectedDI->m_BoundingRect.TopLeft();
				pSelectedDI->m_dragSize		= CSize(pSelectedDI->m_BoundingRect.Width(), pSelectedDI->m_BoundingRect.Height());
				pSelectedDI->m_dragDropObject.DragInitialize(NULL, CRect(pSelectedDI->m_dragPoint, pSelectedDI->m_dragSize), 180);

				CClientDC cdc(this);
				CDC memDC;
				memDC.CreateCompatibleDC(&cdc);
				pSelectedDI->m_dragDropObject.m_bmObject.CreateCompatibleBitmap(&cdc, pSelectedDI->m_dragSize.cx, pSelectedDI->m_dragSize.cy);
				memDC.SelectObject(&pSelectedDI->m_dragDropObject.m_bmObject);

				if (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
				{
					CLayoutObject* pLayoutObject = (CLayoutObject*)pSelectedDI->m_pData;
					CPrintSheet*   pPrintSheet	 = (CPrintSheet*)pSelectedDI->m_pAuxData;
					if (pLayoutObject)
					{
						if (pLayoutObject->IsFlatProduct())
						{
							CFlatProduct&  rFlatProduct  = pLayoutObject->GetCurrentFlatProduct(pPrintSheet);
							rFlatProduct.DrawSheetPreview(&memDC, CRect(CPoint(0, 0), pSelectedDI->m_dragSize), pLayoutObject, ShowBitmap());
							pSelectedDI->m_dragDropObject.DragEnter(&cdc);
						}
						else
						{
							CPageTemplate* pPageTemplate = pLayoutObject->GetCurrentPageTemplate(pPrintSheet);
							if (pPageTemplate)
							{
								pPageTemplate->DrawSheetPreview(&memDC, CRect(CPoint(0, 0), pSelectedDI->m_dragSize), pLayoutObject, pPrintSheet, ShowBitmap());
								pSelectedDI->m_dragDropObject.DragEnter(&cdc);
							}
						}
					}
				}
				else
				{
					int			 nComponentRefIndex = (int)pSelectedDI->m_pData;
					CPrintSheet* pPrintSheet		= (CPrintSheet*)pSelectedDI->m_pAuxData;
					CLayout*	 pLayout			= (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
					int			 nRotation			= (pLayout) ? pLayout->m_FoldSheetLayerRotation[nComponentRefIndex] : 0;
					int			 nTurned			= (pLayout) ? pLayout->m_FoldSheetLayerTurned[nComponentRefIndex] : 0;
					CFoldSheet*  pFoldSheet			= (pPrintSheet) ? pPrintSheet->GetFoldSheet(nComponentRefIndex) : NULL;
					int			 nLayerIndex		= (pPrintSheet) ? ( (nComponentRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize()) ? pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex : 0) : 0;
					if (pFoldSheet)
					{
						pFoldSheet->DrawSheetPreview(&memDC, CRect(CPoint(0, 0), pSelectedDI->m_dragSize), nLayerIndex, nRotation, nTurned, ShowBitmap());
						pSelectedDI->m_dragDropObject.DragEnter(&cdc);
					}
				}

				//OnDrawStateGroupTracker(pSelectedDI, 1, (LPARAM)&memDC);
	
				memDC.DeleteDC();
			}
			
			pSelectedDI	= m_DisplayList.GetNextSelectedItem(pSelectedDI);
		}
	}
	else
	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet)))
	{
		CPrintComponent component = pDoc->m_printComponentsList.GetComponent((int)sourceDI.m_pData);
		if (component.IsNull())
			return DROPEFFECT_NONE;

		CFoldSheet* pFoldSheet = component.GetFoldSheet();
		if ( ! pFoldSheet)
			return DROPEFFECT_NONE;

		m_DisplayList.DeselectAll(FALSE);
		m_DisplayList.m_List.Add(sourceDI);
		m_DisplayList.InitializeSnapGrid();

		CDisplayItem& rDragDI = m_DisplayList.m_List[m_DisplayList.m_List.GetSize() - 1];

		int nLayerIndex = component.m_nFoldSheetLayerIndex;
		float fWidth, fHeight; 
		pFoldSheet->GetCutSize(nLayerIndex, fWidth, fHeight);
		CSize size(CPrintSheetView::WorldToLP(fWidth), CPrintSheetView::WorldToLP(fHeight));

		CClientDC cdc(this);
		CDC dc, memDC;
		dc.CreateCompatibleDC(&cdc);	// use dc to calculate the frame size in device coords
		OnPrepareDC(&dc);
		dc.LPtoDP(&size);
		dc.DeleteDC();
		CRect rcFrame = CRect(CPoint(point.x - size.cx/2, point.y - size.cy/2), size);

		m_DisplayList.m_dragSize   = rDragDI.m_dragSize  = CSize(rcFrame.Width(), rcFrame.Height());
		m_DisplayList.m_dragPoint  = rDragDI.m_dragPoint = rcFrame.TopLeft();
		m_DisplayList.m_dragOffset = CSize(point.x - rcFrame.left, point.y - rcFrame.top);

		CDC dc2;
		dc2.CreateCompatibleDC(&cdc);
		OnPrepareDC(&dc2);
		CPoint ptVpOrg( -GetScrollPos(SB_HORZ) + STATUSBAR_THICKNESS(HORIZONTAL), -GetScrollPos(SB_VERT) + STATUSBAR_THICKNESS(VERTICAL));
		switch (GetActSight())
		{
			case FRONTSIDE : dc2.SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 		 dc2.SetViewportOrg(ptVpOrg);
        					 break;
			case BACKSIDE  : dc2.SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 		 dc2.SetViewportOrg(ptVpOrg);
        					 break;
			case BOTHSIDES : dc2.SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 		 dc2.SetViewportOrg(ptVpOrg);
        					 break;
			default		:	break;
		}
		CPoint ptOrg(0,0);
		dc2.LPtoDP(&ptOrg); ptOrg.y -= rDragDI.m_dragSize.cy;
		rDragDI.m_BoundingRect = CRect(ptOrg, rDragDI.m_dragSize);
		dc2.DeleteDC();

		rDragDI.m_dragDropObject.DragInitialize(NULL, CRect(rDragDI.m_dragPoint, rDragDI.m_dragSize), 180);

		memDC.CreateCompatibleDC(&cdc);
		rDragDI.m_dragDropObject.m_bmObject.CreateCompatibleBitmap(&cdc, rcFrame.Width(), rcFrame.Height());
		memDC.SelectObject(&rDragDI.m_dragDropObject.m_bmObject);

		rcFrame.OffsetRect(-rcFrame.TopLeft());
		pFoldSheet->DrawSheetPreview(&memDC, rcFrame, nLayerIndex, 0, 0, TRUE);

		memDC.DeleteDC();

		rDragDI.m_dragDropObject.DragEnter(&cdc);
	}
	else
	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FlatProduct)))
	{
		CPrintComponent component = pDoc->m_printComponentsList.GetComponent((int)sourceDI.m_pData);
		if (component.IsNull())
			return DROPEFFECT_NONE;

		CFlatProduct* pFlatProduct = component.GetFlatProduct();
		if ( ! pFlatProduct)
			return DROPEFFECT_NONE;

		m_DisplayList.DeselectAll(FALSE);
		m_DisplayList.m_List.Add(sourceDI);
		m_DisplayList.InitializeSnapGrid();

		CDisplayItem& rDragDI = m_DisplayList.m_List[m_DisplayList.m_List.GetSize() - 1];

		float fWidth, fHeight; 
		pFlatProduct->GetCutSize(fWidth, fHeight);
		CSize size(CPrintSheetView::WorldToLP(fWidth), CPrintSheetView::WorldToLP(fHeight));

		CClientDC cdc(this);
		CDC dc, memDC;
		dc.CreateCompatibleDC(&cdc);	// use dc to calculate the frame size in device coords
		OnPrepareDC(&dc);
		dc.LPtoDP(&size);
		CRect rcFrame = CRect(CPoint(point.x - size.cx/2, point.y - size.cy/2), size);

		m_DisplayList.m_dragSize   = rDragDI.m_dragSize  = CSize(rcFrame.Width(), rcFrame.Height());
		m_DisplayList.m_dragPoint  = rDragDI.m_dragPoint = rcFrame.TopLeft();
		m_DisplayList.m_dragOffset = CSize(point.x - rcFrame.left, point.y - rcFrame.top);

		CDC dc2;
		dc2.CreateCompatibleDC(&cdc);
		OnPrepareDC(&dc2);
		CPoint ptVpOrg( -GetScrollPos(SB_HORZ) + STATUSBAR_THICKNESS(HORIZONTAL), -GetScrollPos(SB_VERT) + STATUSBAR_THICKNESS(VERTICAL));
		switch (GetActSight())
		{
			case FRONTSIDE : dc2.SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 		 dc2.SetViewportOrg(ptVpOrg);
        					 break;
			case BACKSIDE  : dc2.SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 		 dc2.SetViewportOrg(ptVpOrg);
        					 break;
			case BOTHSIDES : dc2.SetWindowOrg(m_docOrg.x, m_docSize.cy + m_docOrg.y);
					 		 dc2.SetViewportOrg(ptVpOrg);
        					 break;
			default		:	break;
		}
		CPoint ptOrg(0,0);
		dc2.LPtoDP(&ptOrg); ptOrg.y -= rDragDI.m_dragSize.cy;
		rDragDI.m_BoundingRect = CRect(ptOrg, rDragDI.m_dragSize);
		dc2.DeleteDC();

		rDragDI.m_dragDropObject.DragInitialize(NULL, CRect(rDragDI.m_dragPoint, rDragDI.m_dragSize), 180);

		memDC.CreateCompatibleDC(&cdc);
		rDragDI.m_dragDropObject.m_bmObject.CreateCompatibleBitmap(&cdc, rcFrame.Width(), rcFrame.Height());
		memDC.SelectObject(&rDragDI.m_dragDropObject.m_bmObject);

		rcFrame.OffsetRect(-rcFrame.TopLeft());
		pFlatProduct->DrawSheetPreview(&memDC, rcFrame, NULL, TRUE);

		memDC.DeleteDC();
		dc.DeleteDC();

		rDragDI.m_dragDropObject.DragEnter(&cdc);
	}
	else
	{
		if ( ! GetDlgLayoutObjectGeometry())
			return DROPEFFECT_NONE;

		CDisplayItem* pDragDI = m_DisplayList.FindItem(sourceDI);
		if (pDragDI)
		{
			CClientDC cdc(this);
			pDragDI->m_dragPoint = m_DisplayList.m_dragPoint;
			pDragDI->m_dragSize  = m_DisplayList.m_dragSize;
			pDragDI->m_dragDropObject.DragInitialize(&cdc, CRect(pDragDI->m_dragPoint, pDragDI->m_dragSize), 180);
			pDragDI->m_dragDropObject.DragEnter(&cdc);
		}
	}

	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		return DROPEFFECT_COPY;
	else 
		return DROPEFFECT_MOVE;
}


DROPEFFECT CPrintSheetView::OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, GroupTracker)))
		return OnDragOverGroup(sourceDI, dwKeyState, point);

	if ( (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet))) || (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FlatProduct))) )
		return OnDragOverPrintComponent(sourceDI, dwKeyState, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia)))
		return OnDragOverOutputMedia(sourceDI, dwKeyState, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark)))
		return OnDragOverMark(sourceDI, dwKeyState, point);

	return DROPEFFECT_NONE;
}

DROPEFFECT CPrintSheetView::OnDragOverGroup(CDisplayItem& /*di*/, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT de;
	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		de = DROPEFFECT_MOVE;

	m_DisplayList.GiveDragFeedback(de, point);

	return de;
}

DROPEFFECT CPrintSheetView::OnDragOverOutputMedia(CDisplayItem& /*di*/, DWORD /*dwKeyState*/, CPoint point)
{
	DROPEFFECT de = DROPEFFECT_MOVE;

	m_DisplayList.GiveDragFeedback(de, point);

	return de;
}

DROPEFFECT CPrintSheetView::OnDragOverMark(CDisplayItem& /*di*/, DWORD dwKeyState, CPoint point)
{
	if ( ! GetDlgLayoutObjectGeometry())
		return DROPEFFECT_NONE;

	DROPEFFECT de = DROPEFFECT_MOVE;

	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		de = DROPEFFECT_MOVE;

	m_DisplayList.GiveDragFeedback(de, point);

	return de;
}

DROPEFFECT CPrintSheetView::OnDragOverPrintComponent(CDisplayItem& di, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT de;
	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		de = DROPEFFECT_MOVE;

	m_DisplayList.GiveDragFeedback(de, point);

	return de;
}

void CPrintSheetView::OnDragLeave() 
{
	CImpManDoc*  pDoc = GetDocument();
	if (pDoc)
		pDoc->m_PrintSheetList.m_Layouts.SetAllFoldSheetLayerVisible();

	m_DisplayList.RemoveDragFeedback();
}

BOOL CPrintSheetView::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point) 
{
	if (dropEffect == DROPEFFECT_NONE)
		return FALSE;

	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return FALSE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, GroupTracker)))
		return OnDropGroup(sourceDI, dropEffect, point);

	if ( (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet))) || (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FlatProduct))) )
		return OnDropPrintComponent(sourceDI, dropEffect, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, OutputMedia)))
		return OnDropOutputMedia(sourceDI, dropEffect, point);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Mark)))
		return OnDropMark(sourceDI, dropEffect, point);

	m_DisplayList.RemoveDragFeedback();

	return FALSE;
}

BOOL CPrintSheetView::OnDropGroup(CDisplayItem& /*di*/, DROPEFFECT dropEffect, CPoint /*point*/)
{
	UndoSave();

	CPrintSheet* pPrintSheet = GetActPrintSheet();
	CLayout*	 pLayout	 = GetActLayout();
	if ( ! pLayout || ! pPrintSheet)
		return FALSE;

	if (pLayout->GetNumPrintSheetsBasedOn() > 1)
	{
		int ret = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNOCANCEL);
		switch (ret)
		{
		case IDYES	  : break;
		case IDNO	  :	pLayout = pPrintSheet->SplitLayout();
						if ( ! pLayout)
							return FALSE;
						break;
		case IDCANCEL : return FALSE;
		}
	}

	float fXOffset, fYOffset;
	m_DisplayList.GetDistanceDragged(&fXOffset, &fYOffset);

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem();
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
			pLayout = MoveCopyFlatLayoutObject(*pDI, dropEffect, fXOffset, fYOffset);
		else
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
			pLayout = MoveCopyFoldSheet(*pDI, dropEffect, fXOffset, fYOffset);

		pDI = m_DisplayList.GetNextSelectedItem(pDI);
	}

	if ( ! pLayout)
		return FALSE;

	pLayout->AnalyzeMarks();

	if (pPrintSheet)
		pPrintSheet->CalcTotalQuantity(TRUE);

	CImpManDoc* pDoc = GetDocument();
	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);	// auto marks don't need analyzation, because just moving objects cannot change anything for auto marks

	if (IsOpenDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);

	if (dropEffect != DROPEFFECT_MOVE)
	{
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView), NULL, 0, NULL);	
	}

	OnUpdate(NULL, 0, NULL);

	CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
		pNavigationView->UpdatePrintSheets(pLayout);

	theApp.OnUpdateView(RUNTIME_CLASS(CProductsView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));	

	return TRUE;
}

BOOL CPrintSheetView::OnDropPrintComponent(CDisplayItem& di, DROPEFFECT dropEffect, CPoint point)
{
	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return FALSE;

	UndoSave();

	CPrintSheet* pPrintSheet = GetActPrintSheet();
	CLayout*	 pLayout	 = GetActLayout();
	if ( ! pLayout || ! pPrintSheet)
		return FALSE;

	CPrintComponent component = pDoc->m_printComponentsList.GetComponent((int)di.m_pData);
	if (component.IsNull())
		return FALSE;

	BOOL bRedrawNavigationView = (pPrintSheet->IsEmpty()) ? TRUE : FALSE;

	if (pLayout->GetNumPrintSheetsBasedOn() > 1)
	{
		int ret = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNOCANCEL);
		switch (ret)
		{
		case IDYES	  : break;
		case IDNO	  :	pLayout = pPrintSheet->SplitLayout();
						if ( ! pLayout)
							return FALSE;
						bRedrawNavigationView = TRUE;
						break;
		case IDCANCEL : return FALSE;
		}
	}

	float fXOffset, fYOffset;
	m_DisplayList.GetDistanceDragged(&fXOffset, &fYOffset);

	if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet)))
	{
		CFoldSheet* pFoldSheet = component.GetFoldSheet();
		if ( ! pFoldSheet)
			return DROPEFFECT_NONE;
		pLayout = AddNewFoldSheet(pFoldSheet, component.m_nFoldSheetLayerIndex, fXOffset, fYOffset);
	}
	else
		if (di.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FlatProduct))) 
		{
			CFlatProduct* pFlatProduct = component.GetFlatProduct();
			if ( ! pFlatProduct)
				return DROPEFFECT_NONE;
			pLayout = AddNewFlatProduct(pFlatProduct, fXOffset, fYOffset);
		}

	if ( ! pLayout)
		return FALSE;

	pLayout->AnalyzeMarks();

	pPrintSheet->CalcTotalQuantity(TRUE);

	if (IsOpenDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);

	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();

	pDoc->SetModifiedFlag(TRUE, NULL, FALSE);	// auto marks don't need analyzation, because just moving objects cannot change anything for auto marks

	if (IsOpenDlgComponentImposer())
		GetDlgComponentImposer()->InitData(TRUE);

	OnUpdate(NULL, 0, NULL);

	CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if (pNavigationView)
	{
		pNavigationView->m_DisplayList.Invalidate();
		pNavigationView->SetActPrintSheet(pPrintSheet);		// set act printsheet again because printsheet number could have been changed
	}

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CProductsView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView), NULL, 0, NULL);	

	return TRUE;
}

BOOL CPrintSheetView::OnDropOutputMedia(CDisplayItem& /*di*/, DROPEFFECT /*dropEffect*/, CPoint /*point*/)
{
	float fXOffset, fYOffset;

	m_DisplayList.GetDistanceDragged(&fXOffset, &fYOffset);

	CPrintSheetFrame* pParentFrame = (CPrintSheetFrame*)GetParentFrame();

	if ( !pParentFrame)
		return FALSE;
	CDlgPDFTargetProps*		pDlg			  = GetDlgOutputMedia()->GetDlgPDFTargetProps();
	CReflinePositionParams* pReflinePosParams = (pDlg) ? &pDlg->m_pdfTargetProps.m_mediaList[0].m_posInfos[pDlg->m_nCurTileIndex].m_reflinePosParams : NULL;
	if ( ! pReflinePosParams)
		return FALSE;

	unsigned	nMarkSnapPosition;
	CSnapPoint* pSnapPoint = m_DisplayList.GetDistanceDragged(&fXOffset, &fYOffset, &nMarkSnapPosition);

	if ( ! pSnapPoint)
	{
		pReflinePosParams->m_fDistanceX += fXOffset;
		pReflinePosParams->m_fDistanceY += fYOffset;
	}
	else
	{
		pReflinePosParams->m_fDistanceX = 0.0f;
		pReflinePosParams->m_fDistanceY = 0.0f;
		CDisplayItem snapDI = *pSnapPoint->m_pItem;
		int			 nSideX = LEFT;
		int			 nSideY = TOP;
        switch (pSnapPoint->m_nSnapPosition & ~CDisplayItem::snapOutside)
		{
		case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
		case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
		case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
		case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
		case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
		}

		if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
		{
			pReflinePosParams->SetSheetReference(PLATE_REFLINE, (BYTE)nSideX);
			pReflinePosParams->SetSheetReference(PLATE_REFLINE, (BYTE)nSideY);
		}
		else
			if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
			{
				pReflinePosParams->SetSheetReference(PAPER_REFLINE, (BYTE)nSideX);
				pReflinePosParams->SetSheetReference(PAPER_REFLINE, (BYTE)nSideY);
			}
			else
				if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
				{
					BOOL bObjectBorder = (pSnapPoint->m_nSnapPosition & CDisplayItem::snapOutside) ? TRUE : FALSE;
					pReflinePosParams->SetSheetReference(OBJECT_REFLINE, (BYTE)nSideX, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
					pReflinePosParams->SetSheetReference(OBJECT_REFLINE, (BYTE)nSideY, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
				}

        switch (nMarkSnapPosition & ~CDisplayItem::snapOutside)
		{
		case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
		case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
		case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
		case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
		case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
		}
		pReflinePosParams->SetObjectReference((BYTE)nSideX);
		pReflinePosParams->SetObjectReference((BYTE)nSideY);
	}

	if (pDlg)
		pDlg->UpdateControls();

	OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CPrintSheetView::OnDropMark(CDisplayItem& /*di*/, DROPEFFECT /*dropEffect*/, CPoint /*point*/)
{
   	CPrintSheetFrame* pParentFrame = (CPrintSheetFrame*)GetParentFrame();
	if ( ! pParentFrame)
		return FALSE;

	CMark*		 pMark		 = NULL;
	CDlgMarkSet* pDlgMarkSet = GetDlgMarkSet();
	if (pDlgMarkSet)
		if ( (pDlgMarkSet->m_nCurMarkSel >= 0) && (pDlgMarkSet->m_nCurMarkSel < pDlgMarkSet->m_markList.GetSize()) )
		{
		    if (GetKeyState(VK_CONTROL) < 0) // Ctrl key pressed
				pDlgMarkSet->DuplicateCurrentMark();
			pMark = pDlgMarkSet->m_markList[pDlgMarkSet->m_nCurMarkSel];
		}

	CDlgLayoutObjectProperties* pDlgNewMark = NULL;
	if ( ! pMark)
	{
		pDlgNewMark = GetDlgNewMark();
		if (pDlgNewMark)
			pMark = pDlgNewMark->m_pMark;
	}

	if ( ! pMark)
		return FALSE;

	CDlgLayoutObjectGeometry* pDlgObjectGeometry = GetDlgLayoutObjectGeometry();
	int nReflinePosParamsID = (pDlgObjectGeometry) ? pDlgObjectGeometry->GetReflinePosParamsID() : 1;

	CReflinePositionParams* pReflinePosParams = ((CReflineMark*)pMark)->GetReflinePosParams(nReflinePosParamsID);
	if ( ! pReflinePosParams)
		return FALSE;

	float		fXOffset, fYOffset;
	unsigned	nMarkSnapPosition;
	CSnapPoint* pSnapPoint = m_DisplayList.GetDistanceDragged(&fXOffset, &fYOffset, &nMarkSnapPosition);

	if ( ! pSnapPoint)
	{
		pReflinePosParams->m_fDistanceX += fXOffset;
		pReflinePosParams->m_fDistanceY += fYOffset;
	}
	else
	{
		pReflinePosParams->m_fDistanceX = 0.0f;
		pReflinePosParams->m_fDistanceY = 0.0f;
		CDisplayItem snapDI = *pSnapPoint->m_pItem;
		int			 nSideX = LEFT;
		int			 nSideY = TOP;
        switch (pSnapPoint->m_nSnapPosition & ~CDisplayItem::snapOutside)
		{
		case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
		case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
		case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
		case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
		case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
		}

		if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate)))
		{
			pReflinePosParams->SetSheetReference(PLATE_REFLINE, (BYTE)nSideX);
			pReflinePosParams->SetSheetReference(PLATE_REFLINE, (BYTE)nSideY);
		}
		else
			if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet)))
			{
				pReflinePosParams->SetSheetReference(PAPER_REFLINE, (BYTE)nSideX);
				pReflinePosParams->SetSheetReference(PAPER_REFLINE, (BYTE)nSideY);
			}
			else
				if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, BoundingBoxFrame)))
				{
					pReflinePosParams->SetSheetReference(BBOXFRAME_REFLINE, (BYTE)nSideX);
					pReflinePosParams->SetSheetReference(BBOXFRAME_REFLINE, (BYTE)nSideY);
				}
				else
					if (snapDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
					{
						BOOL bObjectBorder = (pSnapPoint->m_nSnapPosition & CDisplayItem::snapOutside) ? TRUE : FALSE;
						pReflinePosParams->SetSheetReference(OBJECT_REFLINE, (BYTE)nSideX, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
						pReflinePosParams->SetSheetReference(OBJECT_REFLINE, (BYTE)nSideY, (CLayoutObject*)snapDI.m_pData, bObjectBorder);
					}

        switch (nMarkSnapPosition & ~CDisplayItem::snapOutside)
		{
		case CDisplayItem::snapTopLeft:		nSideX = LEFT;		nSideY = TOP;		break;
		case CDisplayItem::snapTopRight:	nSideX = RIGHT;		nSideY = TOP;		break;
		case CDisplayItem::snapBottomRight: nSideX = RIGHT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapBottomLeft:  nSideX = LEFT;		nSideY = BOTTOM;	break;
		case CDisplayItem::snapTop:			nSideX = XCENTER;	nSideY = TOP;		break;
		case CDisplayItem::snapRight:		nSideX = RIGHT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapBottom:		nSideX = XCENTER;	nSideY = BOTTOM;	break;
		case CDisplayItem::snapLeft:		nSideX = LEFT;		nSideY = YCENTER;	break;
		case CDisplayItem::snapMiddle:		nSideX = XCENTER;	nSideY = YCENTER;	break;
		}
		pReflinePosParams->SetObjectReference((BYTE)nSideX);
		pReflinePosParams->SetObjectReference((BYTE)nSideY);
	}

	if (pDlgMarkSet)
		pDlgMarkSet->m_dlgLayoutObjectControl.DataExchangeLoad();
	if (pDlgNewMark)
		pDlgNewMark->m_dlgLayoutObjectControl.DataExchangeLoad();

	OnUpdate(NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	

	return TRUE;
}

CLayout* CPrintSheetView::MoveCopyFlatLayoutObject(CDisplayItem& di, DROPEFFECT dropEffect, float fXOffset, float fYOffset)
{
	CLayoutObject* pLayoutObject	  = (CLayoutObject*)di.m_pData;
	CLayoutObject* pCounterPartObject = (theApp.settings.m_bFrontRearParallel) ? pLayoutObject->FindCounterpartObject() : NULL;
	CLayout*	   pLayout			  = pLayoutObject->GetLayout();

	if (dropEffect == DROPEFFECT_MOVE)
	{
		pLayoutObject->Offset(fXOffset, fYOffset);
		if (pCounterPartObject)
			pCounterPartObject->AlignToCounterPart(pLayoutObject);
	}
	else
	{
		if (pLayoutObject->IsFlatProduct())	// here only copy flat pages - foldsheet pages can only be copied as complete foldsheet
		{
			CPrintSheet*   pPrintSheet		  = pLayout->GetFirstPrintSheet();
			CLayoutSide*   pLayoutSide		  = pLayoutObject->GetLayoutSide();
			CLayoutSide*   pCounterPartSide	  = (pCounterPartObject) ? pCounterPartObject->GetLayoutSide() : NULL;
			int			   nComponentRefIndex = pPrintSheet->m_flatProductRefs.GetSize();

			// Insert before modify, because Offset function needs pointer to layout side for invalidation
			POSITION pos = pLayoutSide->m_ObjectList.AddTail(*pLayoutObject);	 
			CLayoutObject& rNewLayoutObject = pLayoutSide->m_ObjectList.GetAt(pos); 
			rNewLayoutObject.Offset(fXOffset, fYOffset);
			rNewLayoutObject.m_nComponentRefIndex = nComponentRefIndex;
			if (pCounterPartSide)
			{
				pos = pCounterPartSide->m_ObjectList.AddTail(*pCounterPartObject);	 
				CLayoutObject& rNewCounterPartObject = pCounterPartSide->m_ObjectList.GetAt(pos); 
				rNewCounterPartObject.AlignToCounterPart(&rNewLayoutObject);
				rNewCounterPartObject.m_nComponentRefIndex = nComponentRefIndex;
			}

			while (pPrintSheet)
			{
				CFlatProduct& rFlatProduct = pLayoutObject->GetCurrentFlatProduct(pPrintSheet);
				pPrintSheet->MakeFlatProductRefIndex(rFlatProduct.GetIndex());
				pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
			}
		}
	}

	return pLayoutObject->GetLayout();
}


CLayout* CPrintSheetView::MoveCopyFoldSheet(CDisplayItem& di, DROPEFFECT dropEffect, float fXOffset, float fYOffset)
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)di.m_pAuxData;
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return NULL;

	CLayoutSide* pLayoutSide		 = (di.m_AuxRect.Height() <= 2) ? &pLayout->m_BackSide  : &pLayout->m_FrontSide;
	CLayoutSide* pOppositeLayoutSide = (di.m_AuxRect.Height() <= 2) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;
	CObArray	 objectPtrList;
	pLayoutSide->GetFoldSheetObjects((int)di.m_pData, &objectPtrList);

	if (dropEffect == DROPEFFECT_MOVE)
	{
		for (int i = 0; i < objectPtrList.GetSize(); i++)
		{
			CLayoutObject* pLayoutObject	  = (CLayoutObject*)objectPtrList[i];
			CLayoutObject* pCounterPartObject = (theApp.settings.m_bFrontRearParallel) ? pLayoutObject->FindCounterpartObject() : NULL;
			pLayoutObject->Offset(fXOffset, fYOffset);
			if (pCounterPartObject)
				pCounterPartObject->AlignToCounterPart(pLayoutObject);
		}
	}
	else
	{
		BeginWaitCursor();	// copy and paste could take a while, in case of many printsheets

		pPrintSheet									= pLayout->GetFirstPrintSheet();
		int					nSrcComponentRefIndex	= (int)di.m_pData;
		CFoldSheetLayer*	pLayer					= pPrintSheet->GetFoldSheetLayer(nSrcComponentRefIndex);
		int					nSrcLayerRotation		= pLayout->m_FoldSheetLayerRotation[nSrcComponentRefIndex];
		int					nSrcLayerTurned			= pLayout->m_FoldSheetLayerTurned[nSrcComponentRefIndex];

		pLayout->m_FoldSheetLayerRotation.Add(nSrcLayerRotation);
		pLayout->m_FoldSheetLayerTurned.Add(nSrcLayerTurned);

		int nComponentRefIndex = pLayout->m_FoldSheetLayerRotation.GetSize() - 1;

		for (int i = 0; i < objectPtrList.GetSize(); i++)
		{
			CLayoutObject* pSrcLayoutObject = (CLayoutObject*)objectPtrList[i];

			CLayoutObject layoutObject = *pSrcLayoutObject;
			layoutObject.m_nComponentRefIndex = nComponentRefIndex;
			layoutObject.Offset(fXOffset, fYOffset);

			pLayoutSide->m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pNewLayoutObject = &pLayoutSide->m_ObjectList.GetTail();

			pOppositeLayoutSide->m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pNewOppositeLayoutObject = &pOppositeLayoutSide->m_ObjectList.GetTail();

			pNewOppositeLayoutObject->AlignToCounterPart(pNewLayoutObject);
		}

		while (pPrintSheet)
		{
			CFoldSheetLayerRef srcLayerRef = pPrintSheet->m_FoldSheetLayerRefs[nSrcComponentRefIndex];
			pPrintSheet->m_FoldSheetLayerRefs.Add(srcLayerRef);
			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}

		EndWaitCursor();
	}

	return pLayout;
}

CLayout* CPrintSheetView::AddNewFoldSheet(CFoldSheet* pFoldSheet, int nLayerIndex, float fXOffset, float fYOffset)
{
	CPrintSheet* pActPrintSheet = GetActPrintSheet();
	CLayout*	 pActLayout		= (pActPrintSheet) ? pActPrintSheet->GetLayout() : NULL;
	if ( ! pActLayout || ! pActPrintSheet)
		return NULL;

	CPrintSheet printSheet;
	CLayout		layout;
	printSheet.Create(0, -1);
	layout.Create(CPrintingProfile());
	printSheet.LinkLayout(&layout);

	float fWidth, fHeight; 
	pFoldSheet->GetCutSize(nLayerIndex, fWidth, fHeight);
	CRect rcPlaceArea = CRect(0, 0, (long)(fWidth * 1000.0f), (long)(fHeight * 1000.0f));

	layout.m_FrontSide.m_fPaperLeft = 0.0f; layout.m_FrontSide.m_fPaperBottom = 0.0f; layout.m_FrontSide.m_fPaperRight = fWidth; layout.m_FrontSide.m_fPaperTop = fHeight; layout.m_FrontSide.m_fPaperWidth = fWidth; layout.m_FrontSide.m_fPaperHeight = fHeight; 
	layout.m_BackSide.m_fPaperLeft  = 0.0f; layout.m_BackSide.m_fPaperBottom  = 0.0f; layout.m_BackSide.m_fPaperRight  = fWidth; layout.m_BackSide.m_fPaperTop  = fHeight; layout.m_BackSide.m_fPaperWidth  = fWidth; layout.m_BackSide.m_fPaperHeight  = fHeight; 
	layout.MakeNUp(FALSE, printSheet, pFoldSheet, nLayerIndex, 1, TOP, NULL, FALSE, rcPlaceArea);

	for (int nSrcComponentRefIndex = 0; nSrcComponentRefIndex < printSheet.m_FoldSheetLayerRefs.GetSize(); nSrcComponentRefIndex++)
	{
		pActPrintSheet->m_FoldSheetLayerRefs.Add(printSheet.m_FoldSheetLayerRefs[nSrcComponentRefIndex]);
		pActLayout->m_FoldSheetLayerRotation.Add(layout.m_FoldSheetLayerRotation[nSrcComponentRefIndex]);
		pActLayout->m_FoldSheetLayerTurned.Add(layout.m_FoldSheetLayerTurned[nSrcComponentRefIndex]);

		int nComponentRefIndex = pActPrintSheet->m_FoldSheetLayerRefs.GetSize() - 1;

		POSITION pos = layout.m_FrontSide.m_ObjectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = layout.m_FrontSide.m_ObjectList.GetNext(pos);

			CLayoutObject layoutObject = rObject;
			layoutObject.m_nComponentRefIndex = nComponentRefIndex;
			layoutObject.Offset(fXOffset, fYOffset);

			pActLayout->m_FrontSide.m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pFrontLayoutObject = &pActLayout->m_FrontSide.m_ObjectList.GetTail();

			pActLayout->m_BackSide.m_ObjectList.AddTail(layoutObject);
			CLayoutObject* pBackLayoutObject = &pActLayout->m_BackSide.m_ObjectList.GetTail();

			pBackLayoutObject->AlignToCounterPart(pFrontLayoutObject);
		}
	}
	return pActLayout;
}

CLayout* CPrintSheetView::AddNewFlatProduct(CFlatProduct* pFlatProduct, float fXOffset, float fYOffset)
{
	CPrintSheet* pActPrintSheet = GetActPrintSheet();
	CLayout*	 pActLayout		= (pActPrintSheet) ? pActPrintSheet->GetLayout() : NULL;
	if ( ! pActLayout || ! pActPrintSheet)
		return NULL;

	CPrintSheet printSheet;
	CLayout		layout;
	printSheet.Create(0, -1);
	layout.Create(CPrintingProfile());
	printSheet.LinkLayout(&layout);

	float fWidth, fHeight; 
	pFlatProduct->GetCutSize(fWidth, fHeight);
	CRect rcPlaceArea = CRect(0, 0, (long)(fWidth * 1000.0f), (long)(fHeight * 1000.0f));

	layout.m_FrontSide.m_fPaperLeft = 0.0f; layout.m_FrontSide.m_fPaperBottom = 0.0f; layout.m_FrontSide.m_fPaperRight = fWidth; layout.m_FrontSide.m_fPaperTop = fHeight; layout.m_FrontSide.m_fPaperWidth = fWidth; layout.m_FrontSide.m_fPaperHeight = fHeight; 
	layout.m_BackSide.m_fPaperLeft  = 0.0f; layout.m_BackSide.m_fPaperBottom  = 0.0f; layout.m_BackSide.m_fPaperRight  = fWidth; layout.m_BackSide.m_fPaperTop  = fHeight; layout.m_BackSide.m_fPaperWidth  = fWidth; layout.m_BackSide.m_fPaperHeight  = fHeight; 
	layout.MakeNUp(FALSE, printSheet, pFlatProduct, 1, TOP, NULL, FALSE, rcPlaceArea);

	int	nFlatProductIndex = pFlatProduct->GetIndex();

	POSITION pos = layout.m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = layout.m_FrontSide.m_ObjectList.GetNext(pos);

		int nComponentRefIndex = pActPrintSheet->MakeFlatProductRefIndex(nFlatProductIndex);
		CLayoutObject layoutObject;
		layoutObject.CreateFlatProduct(fXOffset + rObject.GetLeft(), fYOffset + rObject.GetBottom(), rObject.GetWidth(), rObject.GetHeight(), rObject.m_nHeadPosition, nComponentRefIndex);
		pActLayout->m_FrontSide.m_ObjectList.AddTail(layoutObject);
		CLayoutObject* pFrontLayoutObject = &pActLayout->m_FrontSide.m_ObjectList.GetTail();

		layoutObject.CreateFlatProduct(fXOffset + rObject.GetLeft(), fYOffset + rObject.GetBottom(), rObject.GetWidth(), rObject.GetHeight(), rObject.m_nHeadPosition, nComponentRefIndex);
		pActLayout->m_BackSide.m_ObjectList.AddTail(layoutObject);
		CLayoutObject* pBackLayoutObject = &pActLayout->m_BackSide.m_ObjectList.GetTail();

		pBackLayoutObject->AlignToCounterPart(pFrontLayoutObject);
	}

	return pActLayout;
}
