// DlgEditTempString.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgEditTempString dialog

class CDlgEditTempString : public CDialog
{
// Construction
public:
	CDlgEditTempString(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgEditTempString)
	enum { IDD = IDD_EDIT_TEMP_STRING };
	CString	m_strString;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgEditTempString)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgEditTempString)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
