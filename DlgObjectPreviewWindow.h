#if !defined(AFX_DLGOBJECTPREVIEWWINDOW_H__7F3C63A0_1979_45B2_9865_FB71EE1AB6F9__INCLUDED_)
#define AFX_DLGOBJECTPREVIEWWINDOW_H__7F3C63A0_1979_45B2_9865_FB71EE1AB6F9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgObjectPreviewWindow.h : Header-Datei
//


#include "DlgObjectPreviewControl.h"
#include "PrintColorList.h"



class CJobColorListItem
{
public:
	enum state{ Enabled = 1, Disabled = 2, Remapped = 4};

public:
	CJobColorListItem() { m_crRGB = WHITE; m_bToPrint = FALSE; };
	CJobColorListItem(COLORREF crRGB, CString strColorName, BOOL bToPrint = FALSE, CString strSepStatus = "", CString strRemapped = "")
	{
		m_crRGB		   = crRGB; 
		m_strColorName = strColorName; 
		m_bToPrint	   = bToPrint;
		m_strSepStatus = strSepStatus;
		m_strRemapped  = strRemapped;
	}


//Attributes:
public:
	COLORREF	m_crRGB;
	CString		m_strColorName;
	BOOL		m_bToPrint;
	CString		m_strSepStatus;
	CString		m_strRemapped;
};


class CJobColorList : public CArray <CJobColorListItem, CJobColorListItem&>
{
// Operations
public:
	void		 AddColorItem(COLORREF crRGB, CString strColorName, BOOL bToPrint = FALSE, CString strSepStatus = "", CString strRemapped = "");

// Implementation
	void		 LoadData(CPageTemplate* pObjectTemplate, CLayoutObject* pLayoutObject, CMark* pMark, CPrintSheet* pPrintSheet, int nSide, CColorDefinitionTable* pColorDef);
	void		 AddProcessColors(CStringArray& sheetSideProcessColors, CPageTemplate* pObjectTemplate, CMark *pMark, CColorDefinitionTable* pColorDefTable);
	int			 GetMaxColorNameLength(CDC* pDC);
};


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgObjectPreviewWindow 

class CDlgObjectPreviewWindow : public CDialog
{
// Konstruktion
public:
	CDlgObjectPreviewWindow(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgObjectPreviewWindow)
	enum { IDD = IDD_OBJECT_PREVIEW_WINDOW };
	CStatic	m_previewFrame;
	//}}AFX_DATA

public:
	CJobColorList			 m_jobColorListFront, m_jobColorListBack;
	CDlgObjectPreviewControl m_dlgObjectPreviewControl;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgObjectPreviewWindow)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL


// Implementierung
public:
	CRect						DrawColorData(CDC* pDC, CJobColorList& rColorList, CRect previewFrameRect, CLayoutObject* pLayoutObject, CMark* pMark, CPrintSheet* pPrintSheet, int nSide, int nColumn);
	int							GetMarkType(CLayoutObject* pLayoutObject, CMark* pMark);
	void						RepositionControls();
	CDlgLayoutObjectControl*	GetActiveDlgLayoutObjectControl();
	CLayoutObject*				GetLayoutObject(CMark* pMark);
	CPageTemplate*				GetObjectTemplate();
	CPageSource*				GetObjectSource();
	CPageSourceHeader*			GetObjectSourceHeader();
	CMark*						GetMark();
	CPrintSheet*				GetPrintSheet();
	CColorDefinitionTable*		GetColorDefTable();
	void						LoadData();

public:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgObjectPreviewWindow)
	virtual BOOL OnInitDialog();
	virtual void OnPaint();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	afx_msg void OnNcLButtonUp( UINT nHitTest, CPoint point );
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGOBJECTPREVIEWWINDOW_H__7F3C63A0_1979_45B2_9865_FB71EE1AB6F9__INCLUDED_
