#pragma once

#include "DlgOrderData.h"
#include "OwnerDrawnButtonList.h"
#include "afxwin.h"



// CDlgJobData-Dialogfeld

class CDlgJobData : public CDialog
{
	DECLARE_DYNAMIC(CDlgJobData)

public:
	CDlgJobData(int nInitialDlgID, int nProductType, ULONG_PTR initialData, CWnd* pParent = NULL, BOOL bWizardMode = FALSE);   // Standardkonstruktor
	virtual ~CDlgJobData();

// Dialogfelddaten
	enum { IDD = IDD_JOBDATA };


public:
	CMemFile					m_memFile;
	CFont						m_font;
	CStatic						m_subDlgSelectorFrame;
	COwnerDrawnButtonList		m_subDlgSelectorButtons;
	int							m_nDlgID;
	ULONG_PTR					m_initialData;
	CDlgOrderData				m_orderDataPage;
	BOOL						m_bWizardMode;
	BOOL						m_bFinishEnabled;



public:
	static void DrawItemSubDlgSelector(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static BOOL QueryJobShowDetails();
	void		InitSubDlgSelectorButtons();
	void		UpdateToolBarStatus();
	CDialog*	GetSubDialog(int nID);
	void		OpenSubDialog();
	void		CloseSubDialog();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnSelectSubDialog();
	afx_msg void OnUpdateSelectSubDialog(CCmdUI* pCmdUI);
	afx_msg void OnBnClickedPrevButton();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedJdProgramSettings();
	afx_msg void OnUpdateOK(CCmdUI* pCmdUI);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
};
