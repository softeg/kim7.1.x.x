#if !defined(AFX_DLGFOLDMARKGEOMETRY_H__6ABF47F8_2749_44A0_A20E_9023443D6395__INCLUDED_)
#define AFX_DLGFOLDMARKGEOMETRY_H__6ABF47F8_2749_44A0_A20E_9023443D6395__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgFoldMarkGeometry.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgFoldMarkGeometry 

class CDlgFoldMarkGeometry : public CDialog
{
// Konstruktion
public:
	CDlgFoldMarkGeometry(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgFoldMarkGeometry();   


// Dialogfelddaten
	//{{AFX_DATA(CDlgFoldMarkGeometry)
	enum { IDD = IDD_FOLDMARK_GEOMETRY };
	CSpinButtonCtrl	m_crosslineWidthSpin;
	CSpinButtonCtrl	m_crossWidthSpin;
	CSpinButtonCtrl	m_crossHeightSpin;
	CSpinButtonCtrl	m_lineWidthSpin;
	CSpinButtonCtrl	m_lineLengthSpin;
	CSpinButtonCtrl	m_lineDistanceSpin;
	CSpinButtonCtrl	m_minDistanceSpin;
	float	m_fLineDistance;
	float	m_fLineLength;
	float	m_fLineWidth;
	float	m_fCrossHeight;
	float	m_fCrossWidth;
	float	m_fCrosslineWidth;
	float	m_fMinDistance;
	BOOL m_bSetMarksBetween;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl* m_pParent;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgFoldMarkGeometry)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void LoadData();
	void SaveData();
	void Apply();

protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgFoldMarkGeometry)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposFoldmarksDistanceSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposFoldmarksLinelengthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposFoldmarksLinewidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposFoldmarksCrossheightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposFoldmarksCrosswidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposFoldmarksCrosslinewidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposFoldmarksMinDistanceSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnFoldmarksBetween();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGFOLDMARKGEOMETRY_H__6ABF47F8_2749_44A0_A20E_9023443D6395__INCLUDED_
