#pragma once


#include "DlgSelectFoldSchemePopup.h"
#include "DlgSelectPrintLayoutPopup.h"
#include "MyBitmapButton.h"



typedef struct
{
	CPrintSheet* pPrintSheet;
	CLayout*	 pLayout;
}CI_PRINTLAYOUT;


// CDlgComponentImposer-Dialogfeld

class CDlgComponentImposer : public CDialog
{
	DECLARE_DYNAMIC(CDlgComponentImposer)

public:
	CDlgComponentImposer(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgComponentImposer();


public:
	CBrush									m_bkBrush;
	CPoint									m_ptInitialPos;
	BOOL									m_bLockDrawPreviews;
	CArray <CRect, CRect>					m_oldRectList, m_newRectList;
	CRect									m_rcPlaceArea, m_rcOldPlaceArea;
	int										m_nNup;
	CPrintSheet								m_printSheetTemplate;
	CLayout									m_layoutTemplate;
	BOOL									m_bSheetGrainOn;
	CMyBitmapButton							m_nextProductButton;


public:
	void								InitData(BOOL bUpdatePrintSheetViewAll);
	void 								InitUps(BOOL bReset = FALSE, BOOL bUpdatePrintSheetViewAll = TRUE, int nOptNup = -1);
	int									CalcOptimumNup(CPrintComponent& rComponent, CPrintSheet* pPrintSheet, int nDefaultNup = 1);
	void 								InitPrintLayoutTemplate();
	void 								InitFoldSheetImposingSamples();
	void 								InitFlatProductImposingSamples();
	void								SelectPlaceArea(CPoint point);
	void								SelectPlaceArea(CRect rcPlaceArea);
	CPrintSheetView*					GetPrintSheetView();
	class CPrintSheetViewStrippingData*	GetPrintSheetViewStrippingData();
	class CPrintComponentsView*			GetPrintComponentsView();
	class CNewPrintSheetFrame*			GetNewPrintSheetFrame();
	BOOL								IsOpenDlgFoldSchemeSelector();
	CDlgSelectFoldSchemePopup*			GetDlgFoldSchemeSelector();
	CPrintSheet*						GetActPrintSheet();
	CLayout*							GetActLayout();
	CPrintingProfile*					GetActPrintingProfile();
	CPrintComponent						GetActComponent();
	CFoldSheet*							GetActFoldSheet();
	int									GetActLayerIndex();
	CBoundProductPart*					GetActBoundProductPart();
	BOOL								GetActProductPartIndex();
	CFlatProduct*						GetActFlatProduct();
	void								DrawPreviewFrame(CDC* pDC);
	void								DrawPreviewFrameFlat	 (CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CDisplayList* pDisplayList = NULL);
	void								DrawPreviewFrameFoldSheet(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CDisplayList* pDisplayList = NULL);
	void								GetMinSize(float& fMinWidth, float& fMinHeight);
	void								UpdateCalculation(CPoint point);
	void								InitComponentRotation() ;
	void								AddProduct(BOOL bGotoNext);
	void								AddFlatProduct();
	CFoldSheet*							AddFoldSheet();
	void								UndoSave();
	void								EnableAll();
	BOOL static CALLBACK				EnumChildProc(HWND hwndChild, LPARAM lParam);
	void								ChangeNumSheets(int nNumSheetsOld);
	void								UpdatePrintSheetView(BOOL bInvalidateAll = FALSE);
	void								SwitchPrintSheetViewSheetGrain(BOOL bSheetGrainOn);


// Dialogfelddaten
	//{{AFX_DATA(CDlgComponentImposer)
	enum { IDD = IDD_COMPONENT_IMPOSER };
	int			m_nPlaceMethod;
	CComboBox	m_NUpCombo;
	UINT		m_nRotation;
	//}}AFX_DATA


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void PostNcDestroy();

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnAddProduct();
	afx_msg void OnNextProduct();
	afx_msg void OnClose();
public:
	afx_msg void OnCbnSelchangePlaceMethod();
	afx_msg void OnCbnSelchangeNUpCombo();
	afx_msg void OnBnClickedComponentsFillup();
	afx_msg void OnBnClickedComponentsOptimumFillup();
	afx_msg void OnSelchangeComponentRotationCombo();
	afx_msg void OnDeltaposPdNumSheetsSpin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedPrintLayoutlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDestroy();
};
