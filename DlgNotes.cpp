// DlgNotes.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgNotes.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgNotes dialog


CDlgNotes::CDlgNotes(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgNotes::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgNotes)
	m_strNotes = _T("");
	//}}AFX_DATA_INIT
	m_NotesChanged = FALSE;
	m_pFoldSheet   = NULL;
	m_pLayout	   = NULL;
}


void CDlgNotes::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgNotes)
	DDX_Text(pDX, IDC_NOTES_EDIT, m_strNotes);
	DDV_MaxChars(pDX, m_strNotes, 250);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgNotes, CDialog)
	//{{AFX_MSG_MAP(CDlgNotes)
	ON_EN_CHANGE(IDC_NOTES_EDIT, OnChangeNotesEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgNotes message handlers

BOOL CDlgNotes::OnInitDialog() 
{
	CGlobalData* pGD;
	CView* pView;
	CDisplayItem* pDIfoldsheet;
	CFoldSheetLayerRef LayerRefBuf;
	CPrintSheet* pPrintSheet;

	CString title;
	title.LoadString(IDS_NOTES_TITLE);
	title += _T(" -> ");

	CDialog::OnInitDialog();

	if ( ! m_pLayout)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			pView = pDoc->GetNextView(pos);
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetView)))
			{
				pDIfoldsheet = ((CPrintSheetView*)pView)->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
				if (pDIfoldsheet)
				{
					pPrintSheet = (CPrintSheet*)pDIfoldsheet->m_pAuxData;
					LayerRefBuf = pPrintSheet->m_FoldSheetLayerRefs.GetAt((int)pDIfoldsheet->m_pData);
					if (pDoc->m_Bookblock.m_FoldSheetList.GetCount() > (LayerRefBuf.m_nFoldSheetIndex)) //Index starts with 0
					{
						pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(LayerRefBuf.m_nFoldSheetIndex);
						m_pFoldSheet = &(pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos));
					}
				}
				break;
			}
		}

		switch (pDoc->GetActSight())
		{
			case FRONTSIDE	:   //actObject ???	TODO
			case BACKSIDE	:   //actObject ???
			case BOTHSIDES	: if (m_pFoldSheet)
							  {
								  m_strNotes = m_pFoldSheet->m_strNotes;
								  SetWindowText(title += m_pFoldSheet->m_strFoldSheetTypeName);
							  }
							  else
							  {
								  CLayout* pActLayout = pDoc->GetActLayout();

								  m_strNotes = pActLayout->m_strNotes;
								  SetWindowText(title += pActLayout->m_strLayoutName);
							  }
							  break;
			case ALLSIDES	: pGD = &pDoc->m_GlobalData;
							  m_strNotes = pGD->m_strNotes;
							  SetWindowText(title += pDoc->GetTitle());
							  break;
			default			: //TODO: Errormessage !
							 break;
		}	
	}
	else
	{
		m_strNotes = m_pLayout->m_strNotes;
		SetWindowText(title += m_pLayout->m_strLayoutName);
	}

	UpdateData(FALSE);      
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgNotes::OnOK() 
{
	UpdateData(TRUE);      
	if(m_NotesChanged)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();

		if (m_pLayout)
			m_pLayout->m_strNotes = m_strNotes;
		else
		{
			CGlobalData* pGD;
			CLayout* pActLayout;

			switch (pDoc->GetActSight())
			{
				case FRONTSIDE	:   //actObject ???	TODO
				case BACKSIDE	:   //actObject ???
				case BOTHSIDES	: if (m_pFoldSheet)
									  m_pFoldSheet->m_strNotes = m_strNotes;
								  else
								  {
									  pActLayout = pDoc->GetActLayout();
									  pActLayout->m_strNotes = m_strNotes;
								  }
								  break;
				case ALLSIDES	: pGD = &pDoc->m_GlobalData;
								  pGD->m_strNotes = m_strNotes;
								  break;
				default			: //TODO: Errormessage !
								 break;
			}	
		}
		pDoc->SetModifiedFlag();
	}
	CDialog::OnOK();
}

void CDlgNotes::OnChangeNotesEdit() 
{
	m_NotesChanged = TRUE;
}
