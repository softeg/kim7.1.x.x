#pragma once

#include "MyBitmapButton.h"


// CPPPCustomPaintFrame

class CPPPCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CPPPCustomPaintFrame)

public:
	CPPPCustomPaintFrame();virtual ~CPPPCustomPaintFrame();

public:
	DWORD m_itemData;

public:
	void Create(LPCTSTR lpszText, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, DWORD itemData) 
	{
		CStatic::Create(lpszText, dwStyle, rect, pParentWnd, nID); 
		m_itemData = itemData; 
	};
	void DrawBookblockPreview(CDC* pDC, CRect rcFrame);

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};


// CPPFoldSheetListCtrl

class CPDOFoldSheetListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CPDOFoldSheetListCtrl)

public:
	CPDOFoldSheetListCtrl();
	virtual ~CPDOFoldSheetListCtrl();

public:
	enum EHighlight {HIGHLIGHT_NORMAL, HIGHLIGHT_ALLCOLUMNS, HIGHLIGHT_ROW};

	int m_nPrevSel;

protected:
	int m_nHighlight;		// Indicate type of selection highlighting


protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
public:
	afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
public:
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
};




// CProdDataProcessOverView-Formularansicht

class CProdDataProcessOverView : public CFormView
{
	DECLARE_DYNCREATE(CProdDataProcessOverView)

public:
	CProdDataProcessOverView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CProdDataProcessOverView();

public:
	enum { IDD = IDD_PRODDATAPROCESSOVERVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	CPPPCustomPaintFrame	m_productPreview, m_bookBlockPreview;
	CPDOFoldSheetListCtrl	m_printComponentsList;
	CFont					m_bigFont;
	CMyBitmapButton			m_gotoProductDataButton, m_gotoProductionDataButton, m_gotoBookblockButton;

protected:
	void		 InitializeFoldSheetList();
	CFoldSheet*  GetSelectedFoldSheet();
	CPrintSheet* GetSelectedPrintSheet();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
protected:
	afx_msg void OnNMKillfocusPdOverviewFoldsheetlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMSetfocusPdOverviewFoldsheetlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkPdOverviewFoldsheetlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedPdOverviewFoldsheetlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedGotoProductdata();
	afx_msg void OnBnClickedGotoProductiondata();
	afx_msg void OnBnClickedGotoBookblock();
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


