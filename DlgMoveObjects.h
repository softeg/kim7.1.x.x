// DlgMoveObjects.h : header file
//
#pragma once

/////////////////////////////////////////////////////////////////////////////
// CDlgMoveObjects dialog

class CDlgMoveObjects : public CDialog
{
// Construction
public:
	CDlgMoveObjects(CWnd* pParent = NULL);   // standard constructor
	~CDlgMoveObjects();

// Dialog Data
	//{{AFX_DATA(CDlgMoveObjects)
	enum { IDD = IDD_MOVE_OBJECTS };
	CSpinButtonCtrl	m_distanceXSpin;
	CSpinButtonCtrl	m_distanceYSpin;
	float	m_fDistanceX;
	float	m_fDistanceY;
	int m_nMoveXY;
	//}}AFX_DATA

public:
	CObjectPivotControl	   m_objectPivotCtrl;
	CReflinePositionParams m_reflinePosParams;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgMoveObjects)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

public:
	void InitReflinePosParams();
	void MoveObjects(int nDirection);

protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgMoveObjects)
	afx_msg void OnDeltaposDistanceXSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposDistanceYSpin(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnChangedPivot();
	//}}AFX_MSG
public:
	afx_msg void OnApplyButton();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedMovex();
	afx_msg void OnBnClickedMovey();
};
