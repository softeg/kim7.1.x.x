// ImpManDocDeprecated.cpp : implementation of the CImpManDoc class
//

#include "stdafx.h"
#include "Imposition Manager.h"




void CImpManDoc::SerializeDeprecated(CArchive& ar)		// old doc before KIM7
{
	if ( ! ar.IsLoading())
		return;

	CBoundProduct newBoundProduct;
	m_boundProducts.AddTail(newBoundProduct);
	int nProductID = m_boundProducts.GetCount() - 1;

	theApp.m_pActiveDoc = this;
	
	CFinalProductDescription finalProductDescriptionDeprecated;
	finalProductDescriptionDeprecated.Serialize(ar);
	CBoundProduct& rBoundProduct = m_boundProducts.GetProduct(nProductID);
	rBoundProduct.m_strProductName.LoadString(IDS_BOUND);
	rBoundProduct.m_strComment			= finalProductDescriptionDeprecated.m_strPagesComment;
	rBoundProduct.m_nNumPages			= finalProductDescriptionDeprecated.GetNumPages();
	rBoundProduct.m_fTrimSizeWidth		= finalProductDescriptionDeprecated.m_fTrimSizeWidth;
	rBoundProduct.m_fTrimSizeHeight		= finalProductDescriptionDeprecated.m_fTrimSizeHeight;
	rBoundProduct.m_strFormatName		= finalProductDescriptionDeprecated.m_strFormatName;
	rBoundProduct.m_nPageOrientation	= finalProductDescriptionDeprecated.m_nPageOrientation;
	rBoundProduct.m_strBindingComment	= finalProductDescriptionDeprecated.m_strBindingComment;
	rBoundProduct.SetDesiredQuantity(finalProductDescriptionDeprecated.m_nQuantity);
	
	theApp.m_pActiveDoc = NULL;

	theApp.m_strPSFileNotFound.Empty();
	theApp.m_strContentFileNotFound.Empty();
	theApp.m_strPreviewFileNotFound.Empty();

	m_PageSourceList.Serialize(ar);

	CFile* fp = ar.GetFile();
	if (fp->GetFilePath() != _T("UndoRestore"))
		// Check for missing files in PageSource and give a message to the user
		// Strings will be filled in "m_PageSourceList.Serialize(ar);"
		CImpManDoc::MessageIfFilesAreMissing(CLayoutObject::Page);

	m_MarkSourceList.Serialize(ar);
	// The same procedure for the MarkSource-files
	CImpManDoc::MessageIfFilesAreMissing(CLayoutObject::ControlMark);

	m_PageTemplateList.Serialize(ar);
	m_MarkTemplateList.Serialize(ar);
	m_ColorDefinitionTable.Serialize(ar);

	POSITION pos = m_PageTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = m_PageTemplateList.GetNext(pos, ALL_PARTS);
		if (rTemplate.m_nProductIndex < 0)
			rTemplate.m_nProductIndex = nProductID;
	}

	m_Bookblock.Serialize(ar);
	
	CPressDeviceList pressDevices;
	SerializeOldPrintSheetList(ar, m_PrintSheetList, pressDevices);

	CPrintSheetList	cutSheetListDeprecated;
	CCipCutProgram	cutProgramDeprecated;
	CPressDeviceList dummyPressDevices;
	SerializeOldPrintSheetList(ar, cutSheetListDeprecated, dummyPressDevices);
	cutProgramDeprecated.Serialize(ar);

	// Calling the base class COleDocument enables serialization
	//  of the container document's COleClientItem objects.
	COleDocument::Serialize(ar);

	ar >> m_nDefaultPressDeviceIndex;
	//if (ar.GetFile() != &theApp.m_undoBuffer)
	//	AnalysePressDeviceIndices();

	ar.SerializeClass(RUNTIME_CLASS(CImpManDoc));
	UINT nVersion = ar.GetObjectSchema();

	if (nVersion >= 5)
		m_productionData.Serialize(ar);

	CProductPartList productPartListDeprecated;
	if (nVersion >= 7)
		productPartListDeprecated.Serialize(ar);
	else
		productPartListDeprecated.CreateFromOldSchema(this, finalProductDescriptionDeprecated);

	CLabelList labelList;
	if (nVersion >= 6)
		labelList.Serialize(ar);

	ConvertOldProductPartList(productPartListDeprecated, rBoundProduct, labelList, pressDevices);

	if (nVersion == 1)
		ConvertOldTextmarks();

	if (nVersion < 7)
	{
		theApp.m_pActiveDoc = this;
		POSITION pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
		while (pos)
			m_PrintSheetList.m_Layouts.GetNext(pos).CheckForProductType_Old(this);
		theApp.m_pActiveDoc = NULL;
	}

	if (nVersion >= 3)
	{
		ar >> m_nDongleStatus;
			m_nDongleStatus = CImpManApp::StandardDongle;
	}

	if (nVersion >= 4)
		m_screenLayout.Serialize(ar);

	m_PageSourceList.InitializePageTemplateRefs(&m_PageTemplateList, &m_ColorDefinitionTable);

	theApp.m_pActiveDoc = this;
	m_Bookblock.CheckFoldSheetQuantities();
	m_Bookblock.m_FoldSheetList.UpdatePaperNames();
	m_PrintSheetList.UpdateQuantities();
	m_productionData.m_paperList.UpdateEntries();
	m_ColorDefinitionTable.UpdateColorInfos();
	pos = m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = m_PrintSheetList.GetNext(pos);
		rPrintSheet.m_flatProductRefs.RemoveAll();
		for (int i = 0; i < rPrintSheet.m_flatProductTypeList.GetSize(); i++)
			rPrintSheet.m_flatProductRefs.Add(CFlatProductRef(rPrintSheet.m_flatProductTypeList[i].m_nFlatProductIndex, 0));
	}

	ConvertOldFlatProductRefIndices();
	theApp.m_pActiveDoc = NULL;

	if (fp->GetFilePath() != _T("UndoRestore"))
	{
		theApp.m_pActiveDoc = this;
		m_PrintSheetList.m_Layouts.AnalyzeMarks();
		m_PrintSheetList.DoAutoMarks();
		//m_PageTemplateList.ReorganizeColorInfos(TRUE);
		theApp.m_pActiveDoc = NULL;
	}
}

void CImpManDoc::ConvertOldFlatProductRefIndices()
{
	POSITION pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout&	 rLayout	 = m_PrintSheetList.m_Layouts.GetNext(pos);
		CPrintSheet* pPrintSheet = rLayout.GetFirstPrintSheet();
		if (pPrintSheet)
		{
			pPrintSheet->m_flatProductRefs.RemoveAll();

			POSITION pos = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = rLayout.m_FrontSide.m_ObjectList.GetNext(pos);
				if (rObject.m_nType == CLayoutObject::ControlMark)
					continue;
				if ( ! rObject.IsFlatProduct())
					continue;

				int nFlatProductIndex = ( (rObject.m_nComponentRefIndex >= 0) && (rObject.m_nComponentRefIndex < m_flatProducts.GetSize()) ) ? rObject.m_nComponentRefIndex : 0;
				rObject.m_nComponentRefIndex = pPrintSheet->MakeFlatProductRefIndex(nFlatProductIndex);
				CLayoutObject* pCounterPartObject = rObject.FindOldCounterpartObject();
				if (pCounterPartObject)
					pCounterPartObject->m_nComponentRefIndex = rObject.m_nComponentRefIndex;
			}
		}
	}

	m_PrintSheetList.InitializeFlatProductTypeLists();
}

void CImpManDoc::ConvertOldProductPartList(CProductPartList& productPartListDeprecated, CBoundProduct& rBoundProduct, CLabelList& rLabelList, CPressDeviceList& rPressDevices)
{
	for (int i = 0; i < productPartListDeprecated.GetSize(); i++)
	{
		CProductPart& rProductPart = productPartListDeprecated[i];

		if (rProductPart.m_nType == CProductPart::Bound)
		{
			CBoundProductPart boundProductPart;
			boundProductPart.m_strName								= rProductPart.m_strName;
			boundProductPart.m_nSubType								= rProductPart.m_nSubType;
			boundProductPart.m_nNumPages							= rProductPart.m_nNumPages;
			boundProductPart.m_numberingList.Copy(rProductPart.m_numberingList);
			boundProductPart.m_fPageWidth							= rProductPart.m_fPageWidth;
			boundProductPart.m_fPageHeight							= rProductPart.m_fPageHeight;
			boundProductPart.m_strFormatName						= rProductPart.m_strFormatName;
			boundProductPart.m_nPageOrientation						= rProductPart.m_nPageOrientation;
			boundProductPart.m_strPaper								= rProductPart.m_strPaper;
			boundProductPart.m_nPaperGrammage						= rProductPart.m_nPaperGrammage;
			boundProductPart.m_fPaperVolume							= rProductPart.m_fPaperVolume;
			boundProductPart.m_nDesiredQuantity						= rProductPart.m_nAmount;
			boundProductPart.m_nExtra								= rProductPart.m_nExtra;

			boundProductPart.m_foldingParams.m_bShinglingActive		= rProductPart.m_prodDataBound.m_bShinglingActive;
			boundProductPart.m_foldingParams.m_nShinglingMethod		= rProductPart.m_prodDataBound.m_nShinglingMethod;
			boundProductPart.m_foldingParams.SetShinglingValueX(rProductPart.m_prodDataBound.m_fShinglingValue);
			boundProductPart.m_foldingParams.SetShinglingValueXFoot(rProductPart.m_prodDataBound.m_fShinglingValueFoot);
			boundProductPart.m_foldingParams.m_bShinglingCropMarks	= rProductPart.m_prodDataBound.m_bShinglingCropMarks;

			boundProductPart.m_trimmingParams.m_fHeadTrim			= rProductPart.m_prodDataBound.m_fHeadTrim;
			boundProductPart.m_trimmingParams.m_fSideTrim			= rProductPart.m_prodDataBound.m_fSideTrim;
			boundProductPart.m_trimmingParams.m_fFootTrim			= rProductPart.m_prodDataBound.m_fFootTrim;

			rBoundProduct.m_bindingParams.m_bindingDefs				= rProductPart.m_prodDataBound.m_bindingDefs;
			rBoundProduct.m_bindingParams.m_fSpineArea				= rProductPart.m_prodDataBound.m_fSpineTrim;
			rBoundProduct.m_bindingParams.m_fMillingDepth			= rProductPart.m_prodDataBound.m_fMillingEdge;
			rBoundProduct.m_bindingParams.m_fGlueLineWidth			= rProductPart.m_prodDataBound.m_fGlueLine;
			rBoundProduct.m_bindingParams.m_fOverFold				= rProductPart.m_prodDataBound.m_fOverFold;
			rBoundProduct.m_bindingParams.m_nOverFoldSide			= rProductPart.m_prodDataBound.m_nOverFoldSide;

			rBoundProduct.m_parts.Add(boundProductPart);
			CBoundProductPart& rNewBoundProductPart = rBoundProduct.m_parts.GetAt(rBoundProduct.m_parts.GetSize() - 1);
		}
		else
		{
			for (int i = 0; i < rLabelList.GetSize(); i++)
			{
				CLabelListEntry& rEntry = rLabelList[i];

				CFlatProduct flatProduct;
				flatProduct.m_strProductName	= rEntry.m_strName;
				flatProduct.m_strComment		= rEntry.m_strComment;
				flatProduct.m_nNumPages			= 2;
				//flatProduct.m_colorantList;
				flatProduct.m_strPaper			= rProductPart.m_strPaper;
				flatProduct.m_nPaperGrammage	= rProductPart.m_nPaperGrammage;
				flatProduct.m_fPaperVolume		= rProductPart.m_fPaperVolume;
				flatProduct.m_fTrimSizeWidth	= rEntry.m_fLabelWidth;
				flatProduct.m_fTrimSizeHeight	= rEntry.m_fLabelHeight;
				flatProduct.m_strFormatName		= _T("");
				flatProduct.m_nPageOrientation	= PORTRAIT;
				flatProduct.m_nDesiredQuantity	= rEntry.m_nQuantity;
				flatProduct.m_nRealQuantity		= rEntry.m_nQuantityActual;
				flatProduct.m_fLeftTrim			= rEntry.m_fLeftTrim; 
				flatProduct.m_fBottomTrim		= rEntry.m_fBottomTrim;
				flatProduct.m_fRightTrim		= rEntry.m_fRightTrim;
				flatProduct.m_fTopTrim			= rEntry.m_fTopTrim;
				flatProduct.m_nPaperGrain		= (rEntry.m_bAllowRotation) ? GRAIN_EITHER : GRAIN_VERTICAL;	
				flatProduct.m_nPriority			= rEntry.m_nPriority;
				flatProduct.m_graphicList.m_elements.Copy(rEntry.m_graphicList.m_elements);

				POSITION pos = m_flatProducts.AddTail(flatProduct);
				CFlatProduct& rNewFlatProduct = m_flatProducts.GetAt(pos);
			}
		}

		//CPrintingProfile printingProfile;
		//printingProfile.m_nProductionType						= rProductPart.m_prodDataBound.m_nProductionType;	// Single = 0, Double = 1, ComingAndGoing = 2
		//printingProfile.m_strMarkset							= rProductPart.m_strDefaultMarkset;

		//printingProfile.m_prepress.m_fBleed						= (rProductPart.m_nType == CProductPart::Bound) ? rProductPart.m_prodDataBound.m_fBleed : rProductPart.m_prodDataUnbound.m_fBleed;;
		//printingProfile.m_prepress.m_bAdjustContentToMask		= rProductPart.m_prodDataBound.m_bAdjustContentToMask;
		//printingProfile.m_prepress.m_strOutputProfile			= rProductPart.m_strDefaultOutputProfile;	

		//printingProfile.m_press.m_nWorkStyle					= rProductPart.m_nDefaultWorkStyle;
		//printingProfile.m_press.m_nTurnStyle					= rProductPart.m_nDefaultTurnTumbleMethod;
		//printingProfile.m_press.m_sheetData.m_strSheetName		= rProductPart.m_strDefaultPaper;
		//
		//m_printingProfiles.AddTail(printingProfile);
	}


	theApp.m_pActiveDoc = this;
	POSITION pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout&	 rLayout	 = m_PrintSheetList.m_Layouts.GetNext(pos);
		CPrintSheet* pPrintSheet = rLayout.GetFirstPrintSheet();
		if ( ! pPrintSheet)
			continue;

		pPrintSheet->InitializeFoldSheetTypeList();
		pPrintSheet->InitializeFlatProductTypeList();

		CPrintingProfile printingProfile;
		for (int i = 0; i < pPrintSheet->m_FoldSheetTypeList.GetSize(); i++)
		{
			CFoldSheet* pFoldSheet = pPrintSheet->m_FoldSheetTypeList[i].m_pFoldSheet;
			if ( ! pFoldSheet)
				continue;
			if ( (pFoldSheet->m_nProductPartIndex < 0) || (pFoldSheet->m_nProductPartIndex >= productPartListDeprecated.GetSize()) )
				continue;
			CProductPart& rOldProductPart = productPartListDeprecated[pFoldSheet->m_nProductPartIndex];

			printingProfile.m_nProductionType								= rOldProductPart.m_prodDataBound.m_nProductionType;	// Single = 0, Double = 1, ComingAndGoing = 2
			printingProfile.m_strMarkset									= rOldProductPart.m_strDefaultMarkset;

			printingProfile.m_prepress.m_fBleed								= (rOldProductPart.m_nType == CProductPart::Bound) ? rOldProductPart.m_prodDataBound.m_fBleed : rOldProductPart.m_prodDataUnbound.m_fBleed;;
			printingProfile.m_prepress.m_bAdjustContentToMask				= rOldProductPart.m_prodDataBound.m_bAdjustContentToMask;
			printingProfile.m_prepress.m_strPrintSheetOutputProfile			= rOldProductPart.m_strDefaultOutputProfile;	

			//productionProfile.m_postpress.m_binding.m_bindingDefs			= rOldProductPart.m_prodDataBound.m_bindingDefs;
			//productionProfile.m_postpress.m_binding.m_fSpineArea			= rOldProductPart.m_prodDataBound.m_fSpineTrim;
			//productionProfile.m_postpress.m_binding.m_fMillingDepth			= rOldProductPart.m_prodDataBound.m_fMillingEdge;
			//productionProfile.m_postpress.m_binding.m_fGlueLineWidth		= rOldProductPart.m_prodDataBound.m_fGlueLine;
			//productionProfile.m_postpress.m_binding.m_fOverFold				= rOldProductPart.m_prodDataBound.m_fOverFold;
			//productionProfile.m_postpress.m_binding.m_nOverFoldSide			= rOldProductPart.m_prodDataBound.m_nOverFoldSide;
			//productionProfile.m_postpress.m_folding.m_bShinglingActive		= rOldProductPart.m_prodDataBound.m_bShinglingActive;
			//productionProfile.m_postpress.m_folding.m_nShinglingMethod		= rOldProductPart.m_prodDataBound.m_nShinglingMethod;
			//productionProfile.m_postpress.m_folding.SetShinglingValueX(rOldProductPart.m_prodDataBound.m_fShinglingValue);
			//productionProfile.m_postpress.m_folding.SetShinglingValueXFoot(rOldProductPart.m_prodDataBound.m_fShinglingValueFoot);
			//productionProfile.m_postpress.m_folding.m_bShinglingCropMarks	= rOldProductPart.m_prodDataBound.m_bShinglingCropMarks;

			//productionProfile.m_postpress.m_trimming.m_fHeadTrim			= rOldProductPart.m_prodDataBound.m_fHeadTrim;
			//productionProfile.m_postpress.m_trimming.m_fSideTrim			= rOldProductPart.m_prodDataBound.m_fSideTrim;
			//productionProfile.m_postpress.m_trimming.m_fFootTrim			= rOldProductPart.m_prodDataBound.m_fFootTrim;
		}

		POSITION pos	 = rPressDevices.FindIndex(rLayout.m_FrontSide.m_nPressDeviceIndex);
		POSITION posBack = rPressDevices.FindIndex(rLayout.m_BackSide.m_nPressDeviceIndex);
		if (pos && posBack)
		{
			CPressDevice& rPressDevice						= rPressDevices.GetAt(pos);
			CPressDevice& rBackSidePressDevice				= rPressDevices.GetAt(posBack);
			printingProfile.m_press.m_pressDevice			= rPressDevice;
			printingProfile.m_press.m_backSidePressDevice	= rBackSidePressDevice;
			rLayout.m_nPrintingProfileRef					= pPrintSheet->GetPrintGroupIndex();
			printingProfile.m_strName						= rPressDevice.m_strName;
			//rProfile.m_strComment.Format(_T("%.1f x %.1f"), rPressDevice.m_fPlateWidth, rPressDevice.m_fPlateHeight);
		}

		printingProfile.m_press.m_nWorkStyle				 = rLayout.GetCurrentWorkStyle();
		printingProfile.m_press.m_nTurnStyle				 = (rLayout.KindOfProduction() == WORK_AND_TURN) ? 0 : 1;
		printingProfile.m_press.m_nRefEdge					 = rLayout.m_FrontSide.m_nPaperRefEdge;
		printingProfile.m_press.m_sheetData.m_strSheetName	 = rLayout.m_FrontSide.m_strFormatName;
		printingProfile.m_press.m_sheetData.m_fWidth		 = rLayout.m_FrontSide.m_fPaperWidth;
		printingProfile.m_press.m_sheetData.m_fHeight		 = rLayout.m_FrontSide.m_fPaperHeight;
		printingProfile.m_press.m_sheetData.m_nPaperGrain	 = rLayout.m_FrontSide.m_nPaperGrain;

		m_printingProfiles.AddTail(printingProfile);
		rLayout.m_nPrintingProfileRef = m_printingProfiles.GetSize() - 1;
	}
	theApp.m_pActiveDoc = NULL;

	theApp.m_pActiveDoc = this;
	m_printGroupList.Analyze();
	theApp.m_pActiveDoc = NULL;
}

void CImpManDoc::ConvertOldTextmarks()
{
	POSITION layoutPos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (layoutPos)
	{
		CLayout& rLayout = m_PrintSheetList.m_Layouts.GetNext(layoutPos);

		// Frontside
		POSITION objectPos = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = rLayout.m_FrontSide.m_ObjectList.GetNext(objectPos);

			if (rObject.m_nType == CLayoutObject::ControlMark)
				if ( ! rObject.m_strNotes.IsEmpty())
				{
					POSITION templatePos = m_MarkTemplateList.FindIndex(rObject.m_nMarkTemplateIndex);
					if ( ! templatePos)
						continue;

					CPageTemplate& rTemplate = m_MarkTemplateList.GetAt(templatePos);
					POSITION sourcePos = m_MarkSourceList.FindIndex(rTemplate.m_ColorSeparations[0].m_nPageSourceIndex);
					if ( ! sourcePos)
						continue;

					CPageSource& rSource = m_MarkSourceList.GetAt(sourcePos);
					rSource.m_nType = CPageSource::SystemCreated;
					rSource.m_strSystemCreationInfo.Format(_T("$TEXT %s"), rObject.m_strNotes);
					rObject.m_strNotes.Empty();
				}
		}

		// Backside
		objectPos = rLayout.m_BackSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = rLayout.m_BackSide.m_ObjectList.GetNext(objectPos);

			if (rObject.m_nType == CLayoutObject::ControlMark)
				if ( ! rObject.m_strNotes.IsEmpty())
				{
					POSITION templatePos = m_MarkTemplateList.FindIndex(rObject.m_nMarkTemplateIndex);
					if ( ! templatePos)
						continue;

					CPageTemplate& rTemplate = m_MarkTemplateList.GetAt(templatePos);
					POSITION sourcePos = m_MarkSourceList.FindIndex(rTemplate.m_ColorSeparations[0].m_nPageSourceIndex);
					if ( ! sourcePos)
						continue;

					CPageSource& rSource = m_MarkSourceList.GetAt(sourcePos);
					rSource.m_nType = CPageSource::SystemCreated;
					rSource.m_strSystemCreationInfo.Format(_T("$TEXT %s"), rObject.m_strNotes);
					rObject.m_strNotes.Empty();
				}
		}
	}
}

void CImpManDoc::ConvertBoundProductsPostpressParams(CProductionProfiles& rProductionProfiles)
{
	POSITION pos = m_boundProducts.GetHeadPosition();
	while (pos)
	{
		BOOL bInitialized = FALSE;

		CBoundProduct& rProduct = m_boundProducts.GetNext(pos);
		for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
		{
			CBoundProductPart& rProductPart = rProduct.m_parts[i];

			CFoldSheet*			pFoldSheet = rProductPart.GetFirstFoldSheet();
			CLayout*			pLayout	   = (pFoldSheet) ? pFoldSheet->GetLayout(0) : NULL;
			if ( ! pLayout)
				continue;

			int nLayoutIndex = pLayout->GetIndex();
			if (nLayoutIndex < 0)
				nLayoutIndex = pLayout->m_nPrintingProfileRef;
			CProductionProfile& rProfile = rProductionProfiles.GetProductionProfile(nLayoutIndex);
			if (rProfile.IsNull())
				continue;

			rProductPart.m_foldingParams = rProfile.m_postpress.m_folding;

			if ( ! bInitialized)
			{
				CBindingDefs bindingDefs	  = rProduct.m_bindingParams.m_bindingDefs;
				rProduct.m_bindingParams	  = rProfile.m_postpress.m_binding;
				rProductPart.m_trimmingParams = rProfile.m_postpress.m_trimming;
				rProduct.m_bindingParams.m_bindingDefs = bindingDefs;	// dont take binding defs from profile -> instead use products defs - so restore
				bInitialized = TRUE;
			}
		}
	}
}

void CImpManDoc::ConvertProductionToPrintingProfiles(CProductionProfiles& rProductionProfiles)
{
	POSITION pos = rProductionProfiles.GetHeadPosition();
	while (pos)
	{
		CProductionProfile& rProductionProfile = rProductionProfiles.GetNext(pos);

		CPrintingProfile printingProfile;
		printingProfile.m_strName				= rProductionProfile.m_strName;
		printingProfile.m_strComment			= rProductionProfile.m_strComment;
		printingProfile.m_nProductionType		= rProductionProfile.m_nProductionType;
		printingProfile.m_layoutPool			= rProductionProfile.m_layoutPool;
		printingProfile.m_prepress				= rProductionProfile.m_prepress;
		printingProfile.m_press					= rProductionProfile.m_press;
		printingProfile.m_cutting				= rProductionProfile.m_postpress.m_cutting;

		m_printingProfiles.AddTail(printingProfile);
	}
}

// unfortunately from version 7.0.1.5 we have new AND old style flat product refs -> find out what style and convert if necessary
void CImpManDoc::ConvertFlatProductRefs()
{
	POSITION layoutPos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (layoutPos)
	{
		CLayout& rLayout = m_PrintSheetList.m_Layouts.GetNext(layoutPos);
		if ( ! rLayout.HasFlatProducts())
			continue;
		if (rLayout.GetNumPrintSheetsBasedOn() > 1)		// must already be new style - old style has never worked with more than one sheet 
			continue;
		CPrintSheet* pPrintSheet = rLayout.GetFirstPrintSheet();
		if ( ! pPrintSheet)
			continue;

		int nNumFlatObjects = 0;
		POSITION pos = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rLayoutObject = rLayout.m_FrontSide.m_ObjectList.GetNext(pos);
			if (rLayoutObject.IsFlatProduct())
				nNumFlatObjects++;
		}

		if (nNumFlatObjects > pPrintSheet->m_flatProductRefs.GetSize()) // must be old style. New style has one flat product ref per flat object
		{
			CArray <CFlatProductRef, CFlatProductRef&> oldFlatProductRefs;
			oldFlatProductRefs.Append(pPrintSheet->m_flatProductRefs);
			pPrintSheet->m_flatProductRefs.RemoveAll();

			POSITION pos = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = rLayout.m_FrontSide.m_ObjectList.GetNext(pos);
				if (rObject.m_nType == CLayoutObject::ControlMark)
					continue;
				if ( ! rObject.IsFlatProduct())
					continue;

				int nFlatProductIndex = ( (rObject.m_nComponentRefIndex >= 0) && (rObject.m_nComponentRefIndex < oldFlatProductRefs.GetSize()) ) ? oldFlatProductRefs[rObject.m_nComponentRefIndex].m_nFlatProductIndex : 0;
				rObject.m_nComponentRefIndex = pPrintSheet->MakeFlatProductRefIndex(nFlatProductIndex);
				CLayoutObject* pCounterPartObject = rObject.FindCounterpartObject();
				if (pCounterPartObject)
					pCounterPartObject->m_nComponentRefIndex = rObject.m_nComponentRefIndex;
			}
		}
	}
}

void CImpManDoc::SerializeOldPrintSheetList(CArchive& ar, CPrintSheetList& rOldPrintSheetList, CPressDeviceList& rPressDevices)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrintSheetList));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	//CList <CPrintSheet, CPrintSheet&>::Serialize(ar);
	if (ar.IsStoring())		// storing not needed
	{
	}
	else
	{
		DWORD_PTR nNewCount = ar.ReadCount();
		while (nNewCount--)
		{
			CPrintSheet newData[1];
			SerializeElements(ar, newData, 1);
			rOldPrintSheetList.AddTail(newData[0]);
		}
	}

	rOldPrintSheetList.m_Layouts.Serialize(ar);
	rPressDevices.Serialize(ar);

	if (ar.IsLoading())
	{
		rOldPrintSheetList.m_pdfTargets.RemoveAll();

		switch (nVersion)
		{
		case 1: break;
		case 2: rOldPrintSheetList.m_pdfTargets.Serialize(ar); 
				break;
		case 3: rOldPrintSheetList.m_pdfTargets.Serialize(ar); 
				ar >> rOldPrintSheetList.m_strIPUJobname;
				ar >> rOldPrintSheetList.m_bIPUAllcolors;
				ar >> rOldPrintSheetList.m_strIPUColorString;
				break;
		}
	}
	else
	{
		rOldPrintSheetList.m_pdfTargets.Serialize(ar);
		ar << rOldPrintSheetList.m_strIPUJobname;
		ar << rOldPrintSheetList.m_bIPUAllcolors;
		ar << rOldPrintSheetList.m_strIPUColorString;
	}
}

////////////////////////////// CCipCutProgram /////////////////////////////
/////////////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CCipCutProgram, CObject, VERSIONABLE_SCHEMA | 1)

void CCipCutProgram::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CCipCutProgram));

	if (ar.IsStoring())
		ar << m_nPressureTime;
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> m_nPressureTime;
			break;
		}
	}

	CList <CCipCutSingleCut, CCipCutSingleCut&>::Serialize(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CCipCutSingleCut
IMPLEMENT_SERIAL(CCipCutSingleCut, CObject, VERSIONABLE_SCHEMA | 1)


// helper function for CutList elements
template <> void AFXAPI SerializeElements <CCipCutSingleCut> (CArchive& ar, CCipCutSingleCut* pSingleCut, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CCipCutSingleCut));

	if (ar.IsStoring())
	{
		ar << pSingleCut->m_nIndex;
		ar << pSingleCut->m_fCutPosition;
		ar << pSingleCut->m_fCutOffset;			
		ar << pSingleCut->m_nSteps;			
		ar << pSingleCut->m_fObjectCutWidth;
		ar << pSingleCut->m_bTrimCut;	
		ar << pSingleCut->m_fTrimCutWidth;				
		ar << pSingleCut->m_nRotation;
		ar << pSingleCut->m_bAutomatic;			
		ar << pSingleCut->m_bAirdesk;			
		ar << pSingleCut->m_bSaddleSlow;			
		ar << pSingleCut->m_bChipOutput;			
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> pSingleCut->m_nIndex;
			ar >> pSingleCut->m_fCutPosition;
			ar >> pSingleCut->m_fCutOffset;			
			ar >> pSingleCut->m_nSteps;			
			ar >> pSingleCut->m_fObjectCutWidth;
			ar >> pSingleCut->m_bTrimCut;	
			ar >> pSingleCut->m_fTrimCutWidth;				
			ar >> pSingleCut->m_nRotation;
			ar >> pSingleCut->m_bAutomatic;			
			ar >> pSingleCut->m_bAirdesk;			
			ar >> pSingleCut->m_bSaddleSlow;			
			ar >> pSingleCut->m_bChipOutput;			
			break;
		}
	}
}



/////////////////////////////////////////////////////////////////////////////
// CFinalProductDescription

IMPLEMENT_SERIAL(CFinalProductDescription, CObject, VERSIONABLE_SCHEMA | 5)

CFinalProductDescription::CFinalProductDescription()
{
	m_nApplicationCategory			 = 0;		// For future purposes
	m_nPages						 = 0;
	m_strPagesComment.Empty();
	m_fTrimSizeWidth				 = 0.0f;
	m_fTrimSizeHeight				 = 0.0f;
	m_strFormatName					 = _T("");
	m_nPageOrientation				 = 0;
	m_bindingDefs.m_strName			 = theApp.settings.m_szLastBindingStyle;
	m_bindingDefs.m_nBinding		 = 0;
	//m_nBindingMethod				 = 0;
	m_strBindingComment.Empty();
	m_nQuantity						 = 0;
	m_strQuantityComment.Empty();
	m_PageMargins.fWidth			 = 0.0f;
	m_PageMargins.fHeight			 = 0.0f;
	m_PageMargins.fDistanceToBack	 = 0.0f;
	m_PageMargins.fDistanceToHead	 = 0.0f;
}



void CFinalProductDescription::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CFinalProductDescription));

	if (ar.IsStoring())
	{
		ar << m_nApplicationCategory;
		ar << m_nPages;
		ar << m_strPagesComment;
		ar << m_fTrimSizeWidth;
		ar << m_fTrimSizeHeight;
		ar << m_strFormatName;
		ar << m_nPageOrientation;
		ar << m_bindingDefs.m_strName;
		ar << m_bindingDefs.m_nBinding;
		ar << m_strBindingComment;
		ar << m_nQuantity;
		ar << m_strQuantityComment;
		ar << m_PageMargins.fWidth;
		ar << m_PageMargins.fHeight;
		ar << m_PageMargins.fDistanceToBack;
		ar << m_PageMargins.fDistanceToHead;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		unsigned nOldBindingMethod;
		switch (nVersion)
		{
		case 1: 
		case 2:	ar >> m_nApplicationCategory;
				ar >> m_nPages;
				ar >> m_fTrimSizeWidth;
				ar >> m_fTrimSizeHeight;
				ar >> m_strFormatName;
				ar >> m_nPageOrientation;
				ar >> nOldBindingMethod;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fHeadTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fSideTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fFootTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fBackTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_nProductionType;
				ar >> m_PageMargins.fWidth;
				ar >> m_PageMargins.fHeight;
				ar >> m_PageMargins.fDistanceToBack;
				ar >> m_PageMargins.fDistanceToHead;
				if (nVersion == 1)
					switch (nOldBindingMethod)	// binding method changed - paste bound seems not to make sense (in fact the same as perfect bound)
					{							// so from now on, just use perfect bound and saddle stitched
					case 1:  m_bindingDefs.m_nBinding = CFinalProductDescription::PerfectBound;	  
							 m_bindingDefs.m_strName.LoadString(IDS_PERFECTBOUND_TOOLTIP);
							 break;	// old paste bound becomes new perfect bound
					case 2:  m_bindingDefs.m_nBinding = CFinalProductDescription::SaddleStitched; 
							 m_bindingDefs.m_strName.LoadString(IDS_SADDLESTITCHED_TOOLTIP);
							 break;	// old saddle stitched becomes new saddle stitched
					default: break;
					}
				m_nQuantity = 0;
				m_strPagesComment.Empty();
				m_strBindingComment.Empty();
				m_strQuantityComment.Empty();
				break;

		case 3: ar >> m_nApplicationCategory;
				ar >> m_nPages;
				ar >> m_strPagesComment;
				ar >> m_fTrimSizeWidth;
				ar >> m_fTrimSizeHeight;
				ar >> m_strFormatName;
				ar >> m_nPageOrientation;
				ar >> nOldBindingMethod;
				ar >> m_strBindingComment;
				ar >> m_nQuantity;
				ar >> m_strQuantityComment;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fHeadTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fSideTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fFootTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_fBackTrim;
				ar >> theApp.m_pActiveDoc->m_productionData.m_nProductionType;
				ar >> m_PageMargins.fWidth;
				ar >> m_PageMargins.fHeight;
				ar >> m_PageMargins.fDistanceToBack;
				ar >> m_PageMargins.fDistanceToHead;
				switch (nOldBindingMethod)	
				{							
				case CFinalProductDescription::PerfectBound:	m_bindingDefs.m_nBinding = CFinalProductDescription::PerfectBound;	  
																m_bindingDefs.m_strName.LoadString(IDS_PERFECTBOUND_TOOLTIP);
																break;	
				case CFinalProductDescription::SaddleStitched:  m_bindingDefs.m_nBinding = CFinalProductDescription::SaddleStitched; 
																m_bindingDefs.m_strName.LoadString(IDS_SADDLESTITCHED_TOOLTIP);
																break;	
				default: break;
				}
				break;

		case 4: ar >> m_nApplicationCategory;
				ar >> m_nPages;
				ar >> m_strPagesComment;
				ar >> m_fTrimSizeWidth;
				ar >> m_fTrimSizeHeight;
				ar >> m_strFormatName;
				ar >> m_nPageOrientation;
				ar >> nOldBindingMethod;
				ar >> m_strBindingComment;
				ar >> m_nQuantity;
				ar >> m_strQuantityComment;
				ar >> m_PageMargins.fWidth;
				ar >> m_PageMargins.fHeight;
				ar >> m_PageMargins.fDistanceToBack;
				ar >> m_PageMargins.fDistanceToHead;
				switch (nOldBindingMethod)	
				{							
				case CFinalProductDescription::PerfectBound:	m_bindingDefs.m_nBinding = CFinalProductDescription::PerfectBound;	  
																m_bindingDefs.m_strName.LoadString(IDS_PERFECTBOUND_TOOLTIP);
																break;	
				case CFinalProductDescription::SaddleStitched:  m_bindingDefs.m_nBinding = CFinalProductDescription::SaddleStitched; 
																m_bindingDefs.m_strName.LoadString(IDS_SADDLESTITCHED_TOOLTIP);
																break;	
				default: break;
				}
				break;

		case 5: ar >> m_nApplicationCategory;
				ar >> m_nPages;
				ar >> m_strPagesComment;
				ar >> m_fTrimSizeWidth;
				ar >> m_fTrimSizeHeight;
				ar >> m_strFormatName;
				ar >> m_nPageOrientation;
				ar >> m_bindingDefs.m_strName;
				ar >> m_bindingDefs.m_nBinding;
				ar >> m_strBindingComment;
				ar >> m_nQuantity;
				ar >> m_strQuantityComment;
				ar >> m_PageMargins.fWidth;
				ar >> m_PageMargins.fHeight;
				ar >> m_PageMargins.fDistanceToBack;
				ar >> m_PageMargins.fDistanceToHead;
				break;
		}
	}
}

int	CFinalProductDescription::GetNumPages()
{
	return m_nPages;
}


///////////////////////////////////////////////////////////////////////////
// CProductPart

IMPLEMENT_SERIAL(CProductPart, CProductPart, VERSIONABLE_SCHEMA | 1)

CProductPart::CProductPart()
{
	m_nType = Bound;
	m_nSubType = Body;
	m_strName.Empty();
	m_nNumPages	= 0;
	m_colorantList.RemoveAll();
	m_numberingList.RemoveAll();
	m_strPaper.Empty();
	m_nPaperGrammage	= 0;
	m_fPaperVolume		= 0.0f;
	m_fPageWidth		= 0.0f;
	m_fPageHeight		= 0.0f;
	m_strFormatName.Empty();
	m_nPageOrientation	= PORTRAIT;
	m_nAmount			= 0;
	m_nExtra			= 0;
	m_strComment.Empty();

// production data
	m_prodDataBound.m_nProductionType		= CFinalProductDescription::Single;
	m_prodDataBound.m_bindingDefs.m_nBinding= CFinalProductDescription::PerfectBound;
	m_prodDataBound.m_fHeadTrim				= 0.0f;
	m_prodDataBound.m_fSideTrim				= 0.0f;
	m_prodDataBound.m_fFootTrim				= 0.0f;
	m_prodDataBound.m_fSpineTrim			= 0.0f;
	m_prodDataBound.m_fMillingEdge			= 0.0f;
	m_prodDataBound.m_fGlueLine				= 0.0f;
	m_prodDataBound.m_fOverFold				= 0.0f;
	m_prodDataBound.m_nOverFoldSide			= OverFoldNone;
	m_prodDataBound.m_fBleed				= theApp.settings.m_fDefaultBleed;
	m_prodDataBound.m_bShinglingActive		= FALSE;
	m_prodDataBound.m_nShinglingMethod		= CBookblock::ShinglingMethodMove;
	m_prodDataBound.m_fShinglingValue		= 0.0f;
	m_prodDataBound.m_fShinglingValueFoot	= 0.0f;

	m_prodDataUnbound.m_fLeftTrim	= 0.0f;
	m_prodDataUnbound.m_fBottomTrim = 0.0f;
	m_prodDataUnbound.m_fRightTrim	= 0.0f;
	m_prodDataUnbound.m_fTopTrim	= 0.0f;
	m_prodDataUnbound.m_fBleed		= theApp.settings.m_fDefaultBleed;

	m_strDefaultPressDevice.Empty();	
	m_nDefaultWorkStyle = WORK_AND_BACK;
	m_nDefaultTurnTumbleMethod = TURN_LEFTBOTTOMSIDE;
	m_strDefaultPaper.Empty();
	m_strDefaultOutputProfile.Empty();	
	m_strDefaultMarkset.Empty();
}

CProductPart::CProductPart(const CProductPart& rProductPart)
{
	Copy(rProductPart);
}

const CProductPart& CProductPart::operator=(const CProductPart& rProductPart)
{
	Copy(rProductPart);

	return *this;
}

void CProductPart::Copy(const CProductPart& rProductPart)
{
	m_nType				= rProductPart.m_nType;
	m_nSubType			= rProductPart.m_nSubType;
	m_strName			= rProductPart.m_strName;
	m_nNumPages			= rProductPart.m_nNumPages;
	m_colorantList.Append(rProductPart.m_colorantList);
	m_numberingList.Append(rProductPart.m_numberingList);
	m_strPaper			= rProductPart.m_strPaper;
	m_nPaperGrammage	= rProductPart.m_nPaperGrammage;
	m_fPaperVolume		= rProductPart.m_fPaperVolume;
	m_fPageWidth		= rProductPart.m_fPageWidth;
	m_fPageHeight		= rProductPart.m_fPageHeight;
	m_strFormatName		= rProductPart.m_strFormatName;
	m_nPageOrientation	= rProductPart.m_nPageOrientation;
	m_nAmount			= rProductPart.m_nAmount;
	m_nExtra			= rProductPart.m_nExtra;
	m_strComment		= rProductPart.m_strComment;
	m_strJDFNodeID		= rProductPart.m_strJDFNodeID;

	m_prodDataBound		= rProductPart.m_prodDataBound;
	m_prodDataUnbound	= rProductPart.m_prodDataUnbound;

	m_strDefaultPressDevice		= rProductPart.m_strDefaultPressDevice;	
	m_nDefaultWorkStyle			= rProductPart.m_nDefaultWorkStyle;
	m_nDefaultTurnTumbleMethod	= rProductPart.m_nDefaultTurnTumbleMethod;
	m_strDefaultOutputProfile	= rProductPart.m_strDefaultOutputProfile;	
	m_strDefaultPaper			= rProductPart.m_strDefaultPaper;
	m_strDefaultMarkset			= rProductPart.m_strDefaultMarkset;
}

// helper function for CProductPart elements
template <> void AFXAPI SerializeElements <CProductPart> (CArchive& ar, CProductPart* pProductPart, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductPart));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
		}	
		else
		{ 
			ar >> pProductPart->m_nType;
			ar >> pProductPart->m_nSubType;
			ar >> pProductPart->m_strName;
			ar >> pProductPart->m_nNumPages;
			ar >> pProductPart->m_strPaper;
			ar >> pProductPart->m_nPaperGrammage;
			ar >> pProductPart->m_fPaperVolume;
			ar >> pProductPart->m_fPageWidth;
			ar >> pProductPart->m_fPageHeight;
			ar >> pProductPart->m_strFormatName;
			ar >> pProductPart->m_nPageOrientation;
			ar >> pProductPart->m_nAmount; 
			ar >> pProductPart->m_nExtra;
			ar >> pProductPart->m_strComment;
			ar >> pProductPart->m_strDefaultPressDevice;		
			ar >> pProductPart->m_nDefaultWorkStyle;	
			ar >> pProductPart->m_nDefaultTurnTumbleMethod;
			ar >> pProductPart->m_strDefaultPaper;			
			ar >> pProductPart->m_strDefaultMarkset;		
			ar >> pProductPart->m_strDefaultOutputProfile;		
			ar >> pProductPart->m_strJDFNodeID;
		}

		switch (pProductPart->m_nType)
		{
		case CProductPart::Bound:	pProductPart->m_prodDataBound.Serialize(ar);	break;
		case CProductPart::Unbound:	pProductPart->m_prodDataUnbound.Serialize(ar);	break;
		default:					break;
		}

		pProductPart->m_colorantList.Serialize(ar);
		pProductPart->m_numberingList.Serialize(ar);

		pProductPart++;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CProductPartProductionBound

IMPLEMENT_SERIAL(CProductPartProductionBound, CProductPartProductionBound, VERSIONABLE_SCHEMA | 5)

CProductPartProductionBound::CProductPartProductionBound()
{
	m_nProductionType		= 0;
	m_bindingDefs.m_nBinding = CBindingDefs::PerfectBound;
	m_fHeadTrim				= 0.0f;
	m_fSideTrim				= 0.0f;
	m_fFootTrim				= 0.0f;
	m_fSpineTrim			= 0.0f; 
	m_fMillingEdge			= 0.0f;
	m_fGlueLine				= 0.0f;
	m_fOverFold				= 0.0f;
	m_nOverFoldSide			= 0;
	m_fBleed				= 0.0f;
	m_bShinglingActive		= FALSE;
	m_nShinglingMethod		= CBookblock::ShinglingMethodMove;
	m_fShinglingValue		= 0.0f;
	m_fShinglingValueFoot	= 0.0f;
	m_bShinglingCropMarks	= FALSE;
	m_bAdjustContentToMask	= FALSE;
}

CProductPartProductionBound::~CProductPartProductionBound()
{
}

const CProductPartProductionBound& CProductPartProductionBound::operator=(const CProductPartProductionBound& rProductPartProductionBound)
{
	Copy(rProductPartProductionBound);

	return *this;
}

void CProductPartProductionBound::Copy(const CProductPartProductionBound& rProductPartProductionBound)
{
	m_nProductionType			= rProductPartProductionBound.m_nProductionType;
	m_bindingDefs.m_strName 	= rProductPartProductionBound.m_bindingDefs.m_strName;
	m_bindingDefs.m_nBinding	= rProductPartProductionBound.m_bindingDefs.m_nBinding;
	m_fHeadTrim					= rProductPartProductionBound.m_fHeadTrim;
	m_fSideTrim					= rProductPartProductionBound.m_fSideTrim;
	m_fFootTrim					= rProductPartProductionBound.m_fFootTrim;
	m_fSpineTrim				= rProductPartProductionBound.m_fSpineTrim; 
	m_fMillingEdge				= rProductPartProductionBound.m_fMillingEdge;
	m_fGlueLine					= rProductPartProductionBound.m_fGlueLine;
	m_fOverFold					= rProductPartProductionBound.m_fOverFold;
	m_nOverFoldSide				= rProductPartProductionBound.m_nOverFoldSide;
	m_fBleed					= rProductPartProductionBound.m_fBleed;
	m_bShinglingActive			= rProductPartProductionBound.m_bShinglingActive;
	m_nShinglingMethod			= rProductPartProductionBound.m_nShinglingMethod;
	m_fShinglingValue			= rProductPartProductionBound.m_fShinglingValue;
	m_fShinglingValueFoot		= rProductPartProductionBound.m_fShinglingValueFoot;
	m_bShinglingCropMarks		= rProductPartProductionBound.m_bShinglingCropMarks;
	m_bAdjustContentToMask		= rProductPartProductionBound.m_bAdjustContentToMask;
}

void CProductPartProductionBound::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductPartProductionBound));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		ar << m_nProductionType;
		ar << m_bindingDefs.m_strName;
		ar << m_bindingDefs.m_nBinding;
		ar << m_fHeadTrim;
		ar << m_fSideTrim;
		ar << m_fFootTrim;
		ar << m_fSpineTrim; 
		ar << m_fMillingEdge;
		ar << m_fGlueLine;
		ar << m_fOverFold;
		ar << m_nOverFoldSide;
		ar << m_fBleed;
		ar << m_bShinglingActive;
		ar << m_nShinglingMethod;
		ar << m_fShinglingValue;
		ar << m_fShinglingValueFoot;
		ar << m_bShinglingCropMarks;
		ar << m_bAdjustContentToMask;
	}
	else
	{
		switch (nVersion)
		{
		case 1:	ar >> m_nProductionType;
				ar >> m_bindingDefs.m_strName;
				ar >> m_bindingDefs.m_nBinding;
				ar >> m_fHeadTrim;
				ar >> m_fSideTrim;
				ar >> m_fFootTrim;
				ar >> m_fSpineTrim; 
				ar >> m_fMillingEdge;
				ar >> m_fOverFold;
				ar >> m_nOverFoldSide;
				ar >> m_fBleed;
				ar >> m_bShinglingActive;
				ar >> m_nShinglingMethod;
				ar >> m_fShinglingValue;
				ar >> m_fShinglingValueFoot;
				switch (m_nShinglingMethod)
				{
				case 0:	
				case 1:	m_nShinglingMethod = CBookblock::ShinglingMethodMove;	  break;
				case 2:	m_nShinglingMethod = CBookblock::ShinglingMethodBottling; break;
				}
				m_bShinglingCropMarks = FALSE;	
				m_bAdjustContentToMask = FALSE;
				break;
		case 2:	ar >> m_nProductionType;
				ar >> m_bindingDefs.m_strName;
				ar >> m_bindingDefs.m_nBinding;
				ar >> m_fHeadTrim;
				ar >> m_fSideTrim;
				ar >> m_fFootTrim;
				ar >> m_fSpineTrim; 
				ar >> m_fMillingEdge;
				ar >> m_fOverFold;
				ar >> m_nOverFoldSide;
				ar >> m_fBleed;
				ar >> m_bShinglingActive;
				ar >> m_nShinglingMethod;
				ar >> m_fShinglingValue;
				ar >> m_fShinglingValueFoot;
				m_bShinglingCropMarks = FALSE;	
				m_bAdjustContentToMask = FALSE;
				break;
		case 3:	ar >> m_nProductionType;
				ar >> m_bindingDefs.m_strName;
				ar >> m_bindingDefs.m_nBinding;
				ar >> m_fHeadTrim;
				ar >> m_fSideTrim;
				ar >> m_fFootTrim;
				ar >> m_fSpineTrim; 
				ar >> m_fMillingEdge;
				ar >> m_fOverFold;
				ar >> m_nOverFoldSide;
				ar >> m_fBleed;
				ar >> m_bShinglingActive;
				ar >> m_nShinglingMethod;
				ar >> m_fShinglingValue;
				ar >> m_fShinglingValueFoot;
				ar >> m_bShinglingCropMarks;	
				m_bAdjustContentToMask = FALSE;
				break;
		case 4:	ar >> m_nProductionType;
				ar >> m_bindingDefs.m_strName;
				ar >> m_bindingDefs.m_nBinding;
				ar >> m_fHeadTrim;
				ar >> m_fSideTrim;
				ar >> m_fFootTrim;
				ar >> m_fSpineTrim; 
				ar >> m_fMillingEdge;
				ar >> m_fGlueLine;
				ar >> m_fOverFold;
				ar >> m_nOverFoldSide;
				ar >> m_fBleed;
				ar >> m_bShinglingActive;
				ar >> m_nShinglingMethod;
				ar >> m_fShinglingValue;
				ar >> m_fShinglingValueFoot;
				ar >> m_bShinglingCropMarks;	
				m_bAdjustContentToMask = FALSE;
				break;
		case 5:	ar >> m_nProductionType;
				ar >> m_bindingDefs.m_strName;
				ar >> m_bindingDefs.m_nBinding;
				ar >> m_fHeadTrim;
				ar >> m_fSideTrim;
				ar >> m_fFootTrim;
				ar >> m_fSpineTrim; 
				ar >> m_fMillingEdge;
				ar >> m_fGlueLine;
				ar >> m_fOverFold;
				ar >> m_nOverFoldSide;
				ar >> m_fBleed;
				ar >> m_bShinglingActive;
				ar >> m_nShinglingMethod;
				ar >> m_fShinglingValue;
				ar >> m_fShinglingValueFoot;
				ar >> m_bShinglingCropMarks;	
				ar >> m_bAdjustContentToMask;
				break;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CProductPartProductionUnbound

IMPLEMENT_SERIAL(CProductPartProductionUnbound, CProductPartProductionUnbound, VERSIONABLE_SCHEMA | 2)

CProductPartProductionUnbound::CProductPartProductionUnbound()
{
	m_fLeftTrim		= 0.0f; 
	m_fBottomTrim	= 0.0f;
	m_fRightTrim	= 0.0f;
	m_fTopTrim		= 0.0f;
	m_fBleed		= 0.0f;
}

CProductPartProductionUnbound::~CProductPartProductionUnbound()
{
}

const CProductPartProductionUnbound& CProductPartProductionUnbound::operator=(const CProductPartProductionUnbound& rProductPartProductionUnbound)
{
	Copy(rProductPartProductionUnbound);

	return *this;
}

void CProductPartProductionUnbound::Copy(const CProductPartProductionUnbound& rProductPartProductionUnbound)
{
	m_fLeftTrim		= rProductPartProductionUnbound.m_fLeftTrim; 
	m_fBottomTrim	= rProductPartProductionUnbound.m_fBottomTrim;
	m_fRightTrim	= rProductPartProductionUnbound.m_fRightTrim;
	m_fTopTrim		= rProductPartProductionUnbound.m_fTopTrim;
	m_fBleed		= rProductPartProductionUnbound.m_fBleed;
}

void CProductPartProductionUnbound::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductPartProductionUnbound));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	if (ar.IsStoring())
	{
		ar << m_fLeftTrim;
		ar << m_fBottomTrim;
		ar << m_fRightTrim;
		ar << m_fTopTrim;
		ar << m_fBleed;
	}
	else
	{
		switch (nVersion)
		{
		case 1:	ar >> m_fLeftTrim;
				ar >> m_fBottomTrim;
				ar >> m_fRightTrim;
				ar >> m_fTopTrim;
				break;
		case 2:	ar >> m_fLeftTrim;
				ar >> m_fBottomTrim;
				ar >> m_fRightTrim;
				ar >> m_fTopTrim;
				ar >> m_fBleed;
				break;
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CProductPartList

IMPLEMENT_SERIAL(CProductPartList, CObject, VERSIONABLE_SCHEMA | 1)

CProductPartList::CProductPartList()
{
}

void CProductPartList::CreateFromOldSchema(CImpManDoc* pDoc, CFinalProductDescription& finalProductDescriptionDeprecated)
{
	if ( ! pDoc)
		return;

	CProductPart productPart;
	productPart.m_nType				= (finalProductDescriptionDeprecated.m_nApplicationCategory == 0) ? CProductPart::Bound : CProductPart::Unbound;	// former APPLICATION_BOOK == 0
	productPart.m_nNumPages			= finalProductDescriptionDeprecated.GetNumPages();
	if (productPart.m_nType == CProductPart::Bound)
	{
		productPart.m_strName.LoadString(IDS_UNKNOWN_STR);
		productPart.m_fPageWidth		= finalProductDescriptionDeprecated.m_fTrimSizeWidth;
		productPart.m_fPageHeight		= finalProductDescriptionDeprecated.m_fTrimSizeHeight;
		productPart.m_strFormatName		= finalProductDescriptionDeprecated.m_strFormatName;
		productPart.m_nPageOrientation	= finalProductDescriptionDeprecated.m_nPageOrientation;
		productPart.m_nAmount			= finalProductDescriptionDeprecated.m_nQuantity;
		productPart.m_nExtra			= 0;
		productPart.m_strComment.Format(_T("%s\n%s\n%s"), finalProductDescriptionDeprecated.m_strPagesComment, finalProductDescriptionDeprecated.m_strBindingComment, finalProductDescriptionDeprecated.m_strQuantityComment);
		productPart.m_prodDataBound.m_nProductionType		= pDoc->m_productionData.m_nProductionType;
		productPart.m_prodDataBound.m_bindingDefs.m_nBinding= finalProductDescriptionDeprecated.m_bindingDefs.m_nBinding;
		productPart.m_prodDataBound.m_fHeadTrim			= pDoc->m_productionData.m_fHeadTrim;
		productPart.m_prodDataBound.m_fSideTrim			= pDoc->m_productionData.m_fSideTrim;
		productPart.m_prodDataBound.m_fFootTrim			= pDoc->m_productionData.m_fFootTrim;
		productPart.m_prodDataBound.m_fSpineTrim		= pDoc->m_productionData.m_fBackTrim;
		productPart.m_prodDataBound.m_fMillingEdge		= 0.0f;
		productPart.m_prodDataBound.m_fGlueLine			= 0.0f;
		productPart.m_prodDataBound.m_fOverFold			= 0.0f;
		productPart.m_prodDataBound.m_nOverFoldSide		= CProductPart::OverFoldNone;
		CPrintSheet* pPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
		CPlate*		 pPlate		 = (pPrintSheet) ? pPrintSheet->GetTopmostPlate(FRONTSIDE) : NULL;
		productPart.m_prodDataBound.m_fBleed			= (pPlate) ? pPlate->m_fOverbleedOutside : 0.0f;
	}
	else
		productPart.m_strName.LoadString(IDS_UNBOUND);

	productPart.m_prodDataUnbound.m_fLeftTrim	= 0.0f;
	productPart.m_prodDataUnbound.m_fBottomTrim	= 0.0f;
	productPart.m_prodDataUnbound.m_fRightTrim	= 0.0f;
	productPart.m_prodDataUnbound.m_fTopTrim	= 0.0f;
	productPart.m_prodDataUnbound.m_fBleed		= 0.0f;

	Add(productPart);
}


/////////////////////////////////////////////////////////////////////////////
// CLabelList

IMPLEMENT_SERIAL(CLabelList, CObject, VERSIONABLE_SCHEMA | 1)

CLabelList::CLabelList()
{
}

BOOL CLabelList::operator==(const CLabelList& rLabelList)
{
	if (GetSize() != rLabelList.GetSize())
		return FALSE;

	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i).m_strName			!= rLabelList[i].m_strName)
			return FALSE;
		if (ElementAt(i).m_fLabelWidth		!= rLabelList[i].m_fLabelWidth)
			return FALSE;
		if (ElementAt(i).m_fLabelHeight		!= rLabelList[i].m_fLabelHeight)
			return FALSE;
		if (ElementAt(i).m_fLeftTrim		!= rLabelList[i].m_fLeftTrim)
			return FALSE;
		if (ElementAt(i).m_fBottomTrim		!= rLabelList[i].m_fBottomTrim)
			return FALSE;
		if (ElementAt(i).m_fRightTrim		!= rLabelList[i].m_fRightTrim)
			return FALSE;
		if (ElementAt(i).m_fTopTrim			!= rLabelList[i].m_fTopTrim)
			return FALSE;
		if (ElementAt(i).m_bAllowRotation	!= rLabelList[i].m_bAllowRotation)
			return FALSE;
		if (ElementAt(i).m_nPriority		!= rLabelList[i].m_nPriority)
			return FALSE;
		if (ElementAt(i).m_nQuantity		!= rLabelList[i].m_nQuantity)
			return FALSE;
		if (ElementAt(i).m_nQuantityActual	!= rLabelList[i].m_nQuantityActual)
			return FALSE;
		if (ElementAt(i).m_strComment		!= rLabelList[i].m_strComment)
			return FALSE;
	}

	return TRUE;
}

BOOL CLabelList::operator!=(const CLabelList& rLabelList)
{
	return !(*this == rLabelList);
}

void CLabelList::CheckLayoutObjectFormats()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return;

	for (int i = 0; i < GetSize(); i++)
	{
		CLabelListEntry& rLabelType = ElementAt(i);
		pDoc->m_PrintSheetList.CheckLayoutObjectFormats(i, rLabelType.m_fLabelWidth, rLabelType.m_fLabelHeight, "", -1, TRUE);
	}
}

int CLabelList::Find(const CString& strName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i).m_strName == strName)
			return i;
	}

	return -1;
}

int CLabelList::FindLabelIndex(CLabelListEntry* pLabelListEntry)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (&ElementAt(i) == pLabelListEntry)
			return i;
	}

	return -1;
}

BOOL CLabelList::HasGraphics()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i).m_graphicList.m_elements.GetSize())
			return TRUE;
	}
	return FALSE;
}

float CLabelList::GetMaxHeight(int nNum, int nProductPartIndex)
{
	if (nNum == -1)
		nNum = GetSize();
	else
		nNum = min(nNum, GetSize());

	float fMaxHeight = 0.0f;
	for (int i = 0; i < nNum; i++)
	{
		//if (GetAt(i).m_nProductPartIndex != nProductPartIndex)
		//	continue;
		fMaxHeight = max(fMaxHeight, ElementAt(i).m_fLabelHeight);
	}
	return fMaxHeight;
}

int	CLabelList::MapIndexLocal2Global(int nLocalIndex, int nProductPartIndex)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_nProductPartIndex == nProductPartIndex)
			nLocalIndex--;
		if (nLocalIndex < 0)
			return i;
	}
	return -1;
}


/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CLabelListEntry

IMPLEMENT_SERIAL(CLabelListEntry, CObject, VERSIONABLE_SCHEMA | 5)

CLabelListEntry::CLabelListEntry()
{
	m_strName.Empty();
	m_nProductPartIndex = -1;
	m_fLabelWidth		= 0.0f;
	m_fLabelHeight		= 0.0f;
	m_fLeftTrim			= 0.0f; 
	m_fBottomTrim		= 0.0f;
	m_fRightTrim		= 0.0f;
	m_fTopTrim			= 0.0f;
	m_bAllowRotation	= TRUE;
	m_nPriority			= 0;

	m_nQuantity			= 0L;
	m_nQuantityActual	= 0L;
	m_strComment.Empty();
}

const CLabelListEntry& CLabelListEntry::operator=(const CLabelListEntry& rLabelListEntry)
{
	m_strName			= rLabelListEntry.m_strName;
	m_nProductPartIndex = rLabelListEntry.m_nProductPartIndex;
	m_fLabelWidth		= rLabelListEntry.m_fLabelWidth;
	m_fLabelHeight		= rLabelListEntry.m_fLabelHeight;
	m_fLeftTrim			= rLabelListEntry.m_fLeftTrim; 
	m_fBottomTrim		= rLabelListEntry.m_fBottomTrim;
	m_fRightTrim		= rLabelListEntry.m_fRightTrim;
	m_fTopTrim			= rLabelListEntry.m_fTopTrim;
	m_bAllowRotation	= rLabelListEntry.m_bAllowRotation;
	m_nPriority			= rLabelListEntry.m_nPriority;
	m_nQuantity			= rLabelListEntry.m_nQuantity;
	m_nQuantityActual	= rLabelListEntry.m_nQuantityActual;
	m_strComment		= rLabelListEntry.m_strComment;

	m_graphicList.m_elements.RemoveAll();
	m_graphicList.m_elements.Append(rLabelListEntry.m_graphicList.m_elements);

	return *this;
}


// helper function for CLabelListEntry elements
template <> void AFXAPI SerializeElements <CLabelListEntry> (CArchive& ar, CLabelListEntry* pLabelListEntry, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CLabelListEntry));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pLabelListEntry->m_strName;
			ar << pLabelListEntry->m_nProductPartIndex;
			ar << pLabelListEntry->m_fLabelWidth;
			ar << pLabelListEntry->m_fLabelHeight;
			ar << pLabelListEntry->m_fLeftTrim;
			ar << pLabelListEntry->m_fRightTrim;
			ar << pLabelListEntry->m_fBottomTrim;
			ar << pLabelListEntry->m_fTopTrim;
			ar << pLabelListEntry->m_bAllowRotation;
			ar << pLabelListEntry->m_nPriority;
			ar << pLabelListEntry->m_nQuantity;
			ar << pLabelListEntry->m_nQuantityActual;
			ar << pLabelListEntry->m_strComment;
			pLabelListEntry->m_graphicList.Serialize(ar);
		}
		else
		{
			switch (nVersion)
			{
			case 1: ar >> pLabelListEntry->m_strName;
					ar >> pLabelListEntry->m_fLabelWidth;
					ar >> pLabelListEntry->m_fLabelHeight;
					ar >> pLabelListEntry->m_nQuantity;
					ar >> pLabelListEntry->m_strComment;
					pLabelListEntry->m_graphicList.Serialize(ar);
					pLabelListEntry->m_nQuantityActual	= 0;
					pLabelListEntry->m_fLeftTrim		= 0.0f;
					pLabelListEntry->m_fRightTrim		= 0.0f;
					pLabelListEntry->m_fBottomTrim		= 0.0f;
					pLabelListEntry->m_fTopTrim			= 0.0f;
					pLabelListEntry->m_bAllowRotation	= TRUE;
					pLabelListEntry->m_nPriority		= 0;
					break; 
			case 2: ar >> pLabelListEntry->m_strName;
					ar >> pLabelListEntry->m_fLabelWidth;
					ar >> pLabelListEntry->m_fLabelHeight;
					ar >> pLabelListEntry->m_nQuantity;
					ar >> pLabelListEntry->m_nQuantityActual;
					ar >> pLabelListEntry->m_strComment;
					pLabelListEntry->m_graphicList.Serialize(ar);
					pLabelListEntry->m_fLeftTrim		= 0.0f;
					pLabelListEntry->m_fRightTrim		= 0.0f;
					pLabelListEntry->m_fBottomTrim		= 0.0f;
					pLabelListEntry->m_fTopTrim			= 0.0f;
					pLabelListEntry->m_bAllowRotation	= TRUE;
					pLabelListEntry->m_nPriority		= 0;
					break; 
			case 3: ar >> pLabelListEntry->m_strName;
					ar >> pLabelListEntry->m_fLabelWidth;
					ar >> pLabelListEntry->m_fLabelHeight;
					ar >> pLabelListEntry->m_fLeftTrim;
					ar >> pLabelListEntry->m_fRightTrim;
					ar >> pLabelListEntry->m_fBottomTrim;
					ar >> pLabelListEntry->m_fTopTrim;
					ar >> pLabelListEntry->m_nQuantity;
					ar >> pLabelListEntry->m_nQuantityActual;
					ar >> pLabelListEntry->m_strComment;
					pLabelListEntry->m_graphicList.Serialize(ar);
					pLabelListEntry->m_bAllowRotation = TRUE;
					pLabelListEntry->m_nPriority	  = 0;
					break; 
			case 4: ar >> pLabelListEntry->m_strName;
					ar >> pLabelListEntry->m_fLabelWidth;
					ar >> pLabelListEntry->m_fLabelHeight;
					ar >> pLabelListEntry->m_fLeftTrim;
					ar >> pLabelListEntry->m_fRightTrim;
					ar >> pLabelListEntry->m_fBottomTrim;
					ar >> pLabelListEntry->m_fTopTrim;
					ar >> pLabelListEntry->m_bAllowRotation;
					ar >> pLabelListEntry->m_nPriority;
					ar >> pLabelListEntry->m_nQuantity;
					ar >> pLabelListEntry->m_nQuantityActual;
					ar >> pLabelListEntry->m_strComment;
					pLabelListEntry->m_graphicList.Serialize(ar);
					break; 
			case 5: ar >> pLabelListEntry->m_strName;
					ar >> pLabelListEntry->m_nProductPartIndex;
					ar >> pLabelListEntry->m_fLabelWidth;
					ar >> pLabelListEntry->m_fLabelHeight;
					ar >> pLabelListEntry->m_fLeftTrim;
					ar >> pLabelListEntry->m_fRightTrim;
					ar >> pLabelListEntry->m_fBottomTrim;
					ar >> pLabelListEntry->m_fTopTrim;
					ar >> pLabelListEntry->m_bAllowRotation;
					ar >> pLabelListEntry->m_nPriority;
					ar >> pLabelListEntry->m_nQuantity;
					ar >> pLabelListEntry->m_nQuantityActual;
					ar >> pLabelListEntry->m_strComment;
					pLabelListEntry->m_graphicList.Serialize(ar);
					break; 
			}
		}
		pLabelListEntry++;
	}
}

void AFXAPI DestructElements(CLabelListEntry* pLabelListEntry, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pLabelListEntry->m_strName.Empty();
		pLabelListEntry->m_strComment.Empty();
		pLabelListEntry->m_graphicList.Destruct();

		pLabelListEntry++;
	}
}

/////////////////////////////////////////////////////////////////////////////


CLayoutObject* CLayoutObject::FindOldCounterpartObject()
{
	if (this == NULL)
		return NULL;

	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return NULL;

	CLayoutSide* pLayoutSide	= GetLayoutSide();
	CLayoutSide* pOppLayoutSide = (pLayoutSide->m_nLayoutSide == FRONTSIDE) ? &pLayout->m_BackSide : &pLayout->m_FrontSide;

	if ( ! pLayoutSide || ! pOppLayoutSide)
		return NULL;

	if (m_nType == Page)
	{
		if ( ! IsFlatProduct())
		{
			POSITION pos = pOppLayoutSide->m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pOppLayoutSide->m_ObjectList.GetNext(pos);
				if ( ! rObject.IsFlatProduct() && (rObject.m_nType == Page) )
					if ( (rObject.m_nComponentRefIndex == m_nComponentRefIndex) && (rObject.m_nLayerPageIndex == m_nLayerPageIndex) )
						return &rObject;
			}
		}
		else
		{
			float fCounterpartLeft = 0.0f, fCounterpartRight = 0.0f, fCounterpartTop = 0.0f, fCounterpartBottom = 0.0f;
			switch (pLayout->KindOfProduction())
			{
			case WORK_AND_TURN  : fCounterpartLeft	 = pLayoutSide->m_fPaperRight - GetLeft() + pLayoutSide->m_fPaperLeft - GetWidth();
								  fCounterpartRight	 = fCounterpartLeft + GetWidth();
								  fCounterpartTop	 = GetTop();
								  fCounterpartBottom = GetBottom();
								  break;
			// TODO : Calculation is not yet OK ????
			case WORK_AND_TUMBLE: fCounterpartBottom = pLayoutSide->m_fPaperTop - GetBottom() + pLayoutSide->m_fPaperBottom - GetHeight();
								  fCounterpartTop	 = fCounterpartBottom + GetHeight();
								  fCounterpartLeft	 = GetLeft();
								  fCounterpartRight	 = GetRight();
								  break;
			}
			return FindCounterPart(pLayoutSide->m_nLayoutSide, fCounterpartLeft, fCounterpartBottom, fCounterpartRight, fCounterpartTop, m_strFormatName);
		}
	}
	else
		return FindCounterpartControlMark();

	return NULL;
}
