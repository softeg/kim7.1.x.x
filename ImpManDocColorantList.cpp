#include "stdafx.h"

#include "Imposition Manager.h"



/////////////////////////////////////////////////////////////////////////////
// CColorantList

IMPLEMENT_SERIAL(CColorantList, CObject, VERSIONABLE_SCHEMA | 1)

CColorantList::CColorantList()
{
}

CColorantList::CColorantList(CColorantList& rColorantList)
{
	*this = rColorantList;
}

const CColorantList& CColorantList::operator= (const CColorantList& rColorantList)
{
	RemoveAll();
	for (int i = 0; i < rColorantList.GetSize(); i++)
	{
		CColorant colorant = rColorantList[i];
		Add(colorant);
	}

	return *this;
}

BOOL CColorantList::operator==(const CColorantList& rColorantList)
{
	if (GetSize() != rColorantList.GetSize())
		return FALSE;

	for (int i = 0; i < rColorantList.GetSize(); i++)
	{
		if (GetAt(i) != rColorantList[i])
			return FALSE;
	}
	return TRUE;
}

CColorant* CColorantList::FindColorant(CString& strName)
{
	for (int i = 0; i < GetSize(); i++)
		if (GetAt(i).m_strName == strName)
			return &GetAt(i);

	return NULL;
}

void CColorantList::AddSeparation(CString strSep, CSpotcolorDefList* pSpotcolorDefList)
{
	for (int i = 0; i < GetSize(); i++)
		if (GetAt(i).m_strName.CompareNoCase(strSep) == 0)	// already there
			return;

	CColorant		  colorant;
	CImpManDoc*		  pDoc = CImpManDoc::GetDoc();
	CColorDefinition* pColorDef = (pDoc) ? pDoc->m_ColorDefinitionTable.FindColorDefinition(strSep) : NULL;
	if ( ! pColorDef)
		pColorDef = theApp.m_colorDefTable.FindColorDefinition(strSep);
	if (pColorDef)
	{
		colorant.m_crRGB	= pColorDef->m_rgb;
		colorant.m_strName	= pColorDef->m_strColorName;
	}
	else
	{
		if (pSpotcolorDefList)
			colorant.m_crRGB = pSpotcolorDefList->GetAlternateRGB(strSep);
		else
			colorant.m_crRGB   = WHITE;
		colorant.m_strName = strSep;
	}

	if (theApp.m_colorDefTable.IsProcessColor(strSep))
	{
		int nLastProcessColorPos = -1;
		for (int i = 0; i < GetSize(); i++)
			if (theApp.m_colorDefTable.IsProcessColor(GetAt(i).m_strName))
				nLastProcessColorPos = i;

		nLastProcessColorPos++;
		InsertAt(nLastProcessColorPos, colorant);
	}
	else
		Add(colorant);
}

void CColorantList::AddSeparations(CStringArray& strSeps)
{
	for (int i = 0; i < strSeps.GetSize(); i++)
		AddSeparation(strSeps[i]);
}

void CColorantList::MixColors(CColorantList& rColorantList)
{
	for (int i = 0; i < rColorantList.GetSize(); i++)
	{
		CColorant rColorant = rColorantList[i];
		if ( ! FindColorant(rColorant.m_strName))
			Add(rColorant);
	}
}

CString CColorantList::GetProcessColorCode()
{
	BOOL bCyan	  = FALSE;
	BOOL bMagenta = FALSE;
	BOOL bYellow  = FALSE;
	BOOL bKey	  = FALSE;
	for (int i = 0; i < GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsCyan(GetAt(i).m_strName))
			bCyan = TRUE;
		else
			if (theApp.m_colorDefTable.IsMagenta(GetAt(i).m_strName))
				bMagenta = TRUE;
			else
				if (theApp.m_colorDefTable.IsYellow(GetAt(i).m_strName))
					bYellow = TRUE;
				else
					if (theApp.m_colorDefTable.IsKey(GetAt(i).m_strName))
						bKey = TRUE;
	}

	CString strCode;
	if (bCyan)
		strCode += _T("C");
	if (bMagenta)
		strCode += _T("M");
	if (bYellow)
		strCode += _T("Y");
	if (bKey)
		strCode += _T("K");

	return strCode;
}

void CColorantList::GetSpotColors(CStringArray& spotColors)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if ( ! theApp.m_colorDefTable.IsProcessColor(GetAt(i).m_strName))
			spotColors.Add(GetAt(i).m_strName);
	}
}

BOOL CColorantList::HasCyan()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsCyan(GetAt(i).m_strName))
			return TRUE;
	}
	return FALSE;
}

BOOL CColorantList::HasMagenta()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsMagenta(GetAt(i).m_strName))
			return TRUE;
	}
	return FALSE;
}

BOOL CColorantList::HasYellow()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsYellow(GetAt(i).m_strName))
			return TRUE;
	}
	return FALSE;
}

BOOL CColorantList::HasKey()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsKey(GetAt(i).m_strName))
			return TRUE;
	}
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CColorant

IMPLEMENT_SERIAL(CColorant, CColorant, VERSIONABLE_SCHEMA | 1)

CColorant::CColorant()
{
	m_crRGB = 0;
	m_strName.Empty();
}

CColorant::CColorant(COLORREF crRGB, const CString& strName)
{
	m_crRGB	  = crRGB;
	m_strName = strName;
}

CColorant::CColorant(const CColorant& rColorant)
{
	Copy(rColorant);
}

const CColorant& CColorant::operator=(const CColorant& rColorant)
{
	Copy(rColorant);

	return *this;
}

BOOL CColorant::operator==(const CColorant& rColorant)
{
	if (m_crRGB		!= rColorant.m_crRGB)		 return FALSE;
	if (m_strName	!= rColorant.m_strName)		 return FALSE;
	return TRUE;
}

BOOL CColorant::operator!=(const CColorant& rColorant)
{
	return !(*this == rColorant);
}

void CColorant::Copy(const CColorant& rColorant)
{
	m_crRGB	  = rColorant.m_crRGB;
	m_strName = rColorant.m_strName;
}

// helper function for CPrintGroup elements
template <> void AFXAPI SerializeElements <CColorant> (CArchive& ar, CColorant* pColorant, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CColorant));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pColorant->m_crRGB;
			ar << pColorant->m_strName;
		}
		else
		{ 
			switch (nVersion)
			{
			case 1:	ar >> pColorant->m_crRGB;
					ar >> pColorant->m_strName;
					break;
			}
		}
		pColorant++;
	}
}
/////////////////////////////////////////////////////////////////////////////

