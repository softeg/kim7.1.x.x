#if !defined(AFX_DLGMARKPOSITIONING_H__F8684AC1_B240_11D5_88B9_204C4F4F5020__INCLUDED_)
#define AFX_DLGMARKPOSITIONING_H__F8684AC1_B240_11D5_88B9_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgMarkPositioning.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMarkPositioning 

class CDlgMarkPositioning : public CDialog
{
// Konstruktion
public:
	CDlgMarkPositioning(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgMarkPositioning)
	enum { IDD = IDD_MARK_POSITIONING };
	CSpinButtonCtrl	m_distanceYSpin;
	CSpinButtonCtrl	m_distanceXSpin;
	float	m_fDistanceX;
	float	m_fDistanceY;
	//}}AFX_DATA

public:
	CObjectPivotControl		   m_markObjectPivotCtrl;
	CMarkReflinePositionParams m_reflinePosParams;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgMarkPositioning)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgMarkPositioning)
	virtual BOOL OnInitDialog();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnDeltaposDistancexSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposDistanceySpin(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGMARKPOSITIONING_H__F8684AC1_B240_11D5_88B9_204C4F4F5020__INCLUDED_
