#pragma once





// CPunchToolDirList

class CPunchToolDirItem
{
public:
	CPunchToolDirItem() { m_nAttribute = -1; };
	CPunchToolDirItem(const CString& strFileName, int nAttribute) { m_strFileName = strFileName; m_nAttribute = nAttribute; };
	~CPunchToolDirItem() { m_strFileName.Empty(); };

enum {IsPunchTool = 1, IsFolder = 2};

public:
	CString m_strFileName;
	int		m_nAttribute;
};


class CPunchToolDirList : public CArray <CPunchToolDirItem, CPunchToolDirItem&>
{
public:
	void inline			Sort() { qsort((void*)GetData(), GetSize(), sizeof(CPunchToolDirItem), ComparePunchToolDirItem); }
	static int inline	ComparePunchToolDirItem(const void *p1, const void *p2)
						{
							CPunchToolDirItem* pDirItem1 = (CPunchToolDirItem*)p1;
							CPunchToolDirItem* pDirItem2 = (CPunchToolDirItem*)p2;
							BOOL	  bSameType = FALSE;

							if (pDirItem1->m_nAttribute == CPunchToolDirItem::IsFolder)
								if (pDirItem2->m_nAttribute == CPunchToolDirItem::IsPunchTool)
									return -1;
								else
									bSameType = TRUE;
							else
								if (pDirItem1->m_nAttribute == CPunchToolDirItem::IsPunchTool)
									if (pDirItem2->m_nAttribute == CPunchToolDirItem::IsFolder)
										return 1;
									else
										bSameType = TRUE;

							if (bSameType)
							{
								if (pDirItem1->m_strFileName.CompareNoCase(pDirItem2->m_strFileName) < 0)
									return -1;
								else
									if (pDirItem1->m_strFileName.CompareNoCase(pDirItem2->m_strFileName) > 0)
										return 1;
									else
										return 0;
							}

							return 0;
						}
	CString				FindTool(CString strPunchToolName);
};



// CDlgProductionPunchingDetails-Dialogfeld

class CDlgProductionPunchingDetails : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionPunchingDetails)

public:
	CDlgProductionPunchingDetails(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionPunchingDetails();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTION_PUNCHINGDETAILS };

public:
	CPunchingTool		m_punchingTool;
	CImageList*			m_pPunchToolImageList;
	CPunchToolDirList	m_punchToolDirList;
	CString				m_strCurrentPunchToolFolder;
	CFont				m_boldFont;
	CFlatProduct		m_flatProduct;
	int					m_nFlatProductIndex;


public:
	void InitData();
	void SaveData();
	void InitPunchTools();
	BOOL FindPunchToolFolder(CString strFolder, CString strPunchTool);
	BOOL FolderHasPunchTool(const CString& strFolder, const CString& strPunchTool);
	int  InitPunchToolBrowser(CString strSelectPunchTool = _T(""));
	void DeleteFolder(const CString& strFolderToRemove);

public:
	CString m_strPunchToolName;
protected:
	CListCtrl m_punchToolBrowser;
	CSpinButtonCtrl m_punchToolNXSpin;
	CSpinButtonCtrl m_punchToolNYSpin;
	UINT m_nPunchToolNX;
	UINT m_nPunchToolNY;
	int m_nPunchToolNXOptions;
	int m_nPunchToolNYOptions;
	float m_fPunchToolStepX;
	float m_fPunchToolStepY;
	float m_fPunchToolGutterX;
	float m_fPunchToolGutterY;
	float m_fPunchToolOffsetRows;
	float m_fPunchToolOffsetCols;
	BOOL m_bPunchToolRotateRows;
	BOOL m_bPunchToolRotateCols;
	int m_nPunchToolOffsetRowsOddEven;
	int m_nPunchToolOffsetColsOddEven;
	int m_nPunchToolRotateRowsOddEven;
	int m_nPunchToolRotateColsOddEven;
	BOOL m_bShowPreviews;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnNMCustomdrawPtBrowserlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedPtBrowserlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkPtBrowserlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEndlabeleditPunchtoolBrowser(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedPtFolderUp();
	afx_msg void OnUpdatePtFolderUp(CCmdUI* pCmdUI);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnDeltaposPunchtoolNxspin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPunchtoolNyspin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangePunchtoolNxOptions();
	afx_msg void OnCbnSelchangePunchtoolNyOptions();
	afx_msg void OnEnKillfocusPunchtoolNameEdit();
	afx_msg void OnEnKillfocusPunchtoolStepx();
	afx_msg void OnEnKillfocusPunchtoolStepy();
	afx_msg void OnEnKillfocusPunchtoolNx();
	afx_msg void OnEnKillfocusPunchtoolNy();
	afx_msg void OnBnClickedPunchtoolRotateRows();
	afx_msg void OnBnClickedPunchtoolRotateCols();
	afx_msg void OnBnClickedPunchtoolOffsetRowsOdd();
	afx_msg void OnBnClickedPunchtoolOffsetRowsEven();
	afx_msg void OnBnClickedPunchtoolOffsetColsOdd();
	afx_msg void OnBnClickedPunchtoolOffsetColsEven();
	afx_msg void OnBnClickedPunchtoolRotateRowsOdd();
	afx_msg void OnBnClickedPunchtoolRotateRowsEven();
	afx_msg void OnBnClickedPunchtoolRotateColsOdd();
	afx_msg void OnBnClickedPunchtoolRotateColsEven();
	afx_msg void OnEnKillfocusPunchtoolOffsetRows();
	afx_msg void OnEnKillfocusPunchtoolOffsetCols();
	afx_msg void OnBnClickedPunchtoolShowpreviews();
	afx_msg void OnBnClickedPunchtoolAdd();
	afx_msg void OnBnClickedPunchtoolRemove();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedPunchtoolNewfolder();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
