// PrintSheetViewDraw.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "PageListView.h"
#include "PageListTableView.h"
#include "PrintSheetMarksView.h"
#include "ProdDataPrintView.h"
#include "MainFrm.h"
#include "PDFEngineInterface.h"
#include "PDFOutput.h"
#include "GraphicComponent.h"
#include "BookBlockProdDataView.h"
#include "PrintComponentsView.h"

 
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// CPrintSheetView

void CPrintSheetView::DrawRectangleExt(CDC* pDC, CRect rect)
{
	CSize size(1, 1);
	pDC->DPtoLP(&size);
	rect.NormalizeRect();
	rect.right += size.cx;
//	rect.bottom+= size.cy;
	pDC->Rectangle(rect);

	//Graphics graphics(pDC->m_hDC);
	//Point points[4] = {Point(rect.left, rect.top), Point(rect.right, rect.top), Point(rect.right, rect.bottom), Point(rect.left, rect.bottom)};
	//Color crColor(GetRValue(LIGHTBLUE), GetGValue(LIGHTBLUE), GetBValue(LIGHTBLUE));
	//graphics.DrawPolygon(&Pen(crColor, 0), points, 4);
}

void CPrintSheetView::FillRectangleExt(CDC* pDC, CRect rect, COLORREF crFill)
{
	CSize size(1, 1);
	pDC->DPtoLP(&size);
	rect.NormalizeRect();
	rect.right += size.cx;
//	rect.bottom+= size.cy;
	CPen   pen(PS_SOLID, 1, crFill);
	CBrush brush(crFill);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CBrush* pOldBrush = pDC->SelectObject(&brush);
	pDC->Rectangle(rect);
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();
}

void CPrintSheetView::DrawPlatePreviews(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, BYTE nType, CLayoutObject* pLayoutObject, CString strMarkName) 
{
	if ( ! pLayoutObject)
		if ( ! ShowBitmap())
			return;

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString	 strPreviewFile;
	CRect	 exposureRect, objectRect;
	CPlate*  pPlate	= pActPrintSheet->GetTopmostPlate(nActLayoutSide);

	if (pPlate)
	{
		int nColorDefinitionIndex = (pPlate->m_nColorDefinitionIndex == -1) ? 0 : pPlate->m_nColorDefinitionIndex;	// -1 = default plate -> show 1st colorant

		POSITION pos = pPlate->m_ExposureSequence.GetHeadPosition();
		while (pos)
		{
			CExposure&	   rExposure = pPlate->m_ExposureSequence.GetNext(pos);
			CLayoutObject* pObject	 = rExposure.GetLayoutObject(pActPrintSheet->m_nLayoutIndex, nActLayoutSide);
			if (!pObject)
				continue;
			if (pObject->m_bHidden) 
				continue;									   
			if (pObject->m_nType != nType) 
				continue;
			if (pObject->IsAutoMarkDisabled(pActPrintSheet, nActLayoutSide))
				continue;
			if (pLayoutObject)
				if (pLayoutObject != pObject)
					continue;

			if (nType == CLayoutObject::ControlMark)
				if ( ! strMarkName.IsEmpty())
				{
					if (pObject->m_strFormatName != strMarkName)
						continue;
				}
				else
					if (pActPrintSheet->GetLayout()->m_markList.FindMark(pObject->m_strFormatName))	// for compatibilty to KIM5 marks: if mark is not in mark list it is a KIM5 mark -> so draw it
						continue;

			CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
											  pObject->GetCurrentMarkTemplate() :
											  pObject->GetCurrentPageTemplate(pActPrintSheet);

			if (pObjectTemplate)
			{
				float fSheetClipBoxLeft	  = rExposure.GetSheetBleedBox(LEFT,   pObject, pObjectTemplate);
				float fSheetClipBoxBottom = rExposure.GetSheetBleedBox(BOTTOM, pObject, pObjectTemplate);
				float fSheetClipBoxRight  = rExposure.GetSheetBleedBox(RIGHT,  pObject, pObjectTemplate);
				float fSheetClipBoxTop	  = rExposure.GetSheetBleedBox(TOP,	   pObject, pObjectTemplate);

				pObjectTemplate->GetGridClipBox(fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop);

				exposureRect.left	= WorldToLP(fSheetClipBoxLeft);
				exposureRect.bottom = WorldToLP(fSheetClipBoxBottom);
				exposureRect.right	= WorldToLP(fSheetClipBoxRight);
				exposureRect.top	= WorldToLP(fSheetClipBoxTop);
				exposureRect.NormalizeRect();
			}

			if (pDC->RectVisible(exposureRect))
			{
				if (pObjectTemplate)
				{
					if ( (pPlate->m_nColorDefinitionIndex == -1) && (pObject->m_nType == CLayoutObject::ControlMark) )	// if no plates, show 1st sep of marks
						nColorDefinitionIndex = (pObjectTemplate->m_ColorSeparations.GetSize()) ? pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1;

					CPageSource*	   pObjectSource		= pObjectTemplate->GetObjectSource		(nColorDefinitionIndex, TRUE, pActPrintSheet, nActLayoutSide);
					CPageSourceHeader* pObjectSourceHeader	= pObjectTemplate->GetObjectSourceHeader(nColorDefinitionIndex, TRUE, pActPrintSheet, nActLayoutSide);
					SEPARATIONINFO*	   pSepInfo				= pObjectTemplate->GetSeparationInfo	(nColorDefinitionIndex, TRUE, pActPrintSheet, nActLayoutSide);

					if (pObjectSource && pSepInfo)
					{
						CRect outBBox, objectBBox;
						objectBBox.left	   = WorldToLP(rExposure.GetContentTrimBox(LEFT,   pObject, pObjectTemplate));
						objectBBox.top	   = WorldToLP(rExposure.GetContentTrimBox(TOP,	   pObject, pObjectTemplate));
						objectBBox.right   = WorldToLP(rExposure.GetContentTrimBox(RIGHT,  pObject, pObjectTemplate));
						objectBBox.bottom  = WorldToLP(rExposure.GetContentTrimBox(BOTTOM, pObject, pObjectTemplate));

						float fContentScaleX = pObjectTemplate->m_fContentScaleX * pObjectTemplate->CalcShinglingScaleX();
						float fContentScaleY = pObjectTemplate->m_fContentScaleY * pObjectTemplate->CalcShinglingScaleY();

						int	nOutBBoxWidth, nOutBBoxHeight;
						if ((pObjectTemplate->m_fContentRotation == 0.0f) || (pObjectTemplate->m_fContentRotation == 180.0f))
						{
							nOutBBoxWidth  = WorldToLP((pObjectSource->GetBBox(RIGHT, pObjectSourceHeader) - pObjectSource->GetBBox(LEFT,   pObjectSourceHeader)) * fContentScaleX);
							nOutBBoxHeight = WorldToLP((pObjectSource->GetBBox(TOP,   pObjectSourceHeader) - pObjectSource->GetBBox(BOTTOM, pObjectSourceHeader)) * fContentScaleY);
						}
						else
						{
							nOutBBoxWidth  = WorldToLP((pObjectSource->GetBBox(TOP,   pObjectSourceHeader) - pObjectSource->GetBBox(BOTTOM, pObjectSourceHeader)) * fContentScaleX);
							nOutBBoxHeight = WorldToLP((pObjectSource->GetBBox(RIGHT, pObjectSourceHeader) - pObjectSource->GetBBox(LEFT,   pObjectSourceHeader)) * fContentScaleY);
						}

						if ((pObject->m_nHeadPosition == LEFT) || (pObject->m_nHeadPosition == RIGHT))
						{
							int nTemp = nOutBBoxWidth; nOutBBoxWidth = nOutBBoxHeight; nOutBBoxHeight = nTemp;
						}

						outBBox.left   = (pObjectSource->m_nType == CPageSource::SystemCreated) ? objectBBox.left	: (objectBBox.CenterPoint().x - nOutBBoxWidth  / 2);
						outBBox.bottom = (pObjectSource->m_nType == CPageSource::SystemCreated) ? objectBBox.bottom : (objectBBox.CenterPoint().y - nOutBBoxHeight / 2);
						outBBox.right  = (pObjectSource->m_nType == CPageSource::SystemCreated) ? objectBBox.right	: (objectBBox.CenterPoint().x + nOutBBoxWidth  / 2);
						outBBox.top    = (pObjectSource->m_nType == CPageSource::SystemCreated) ? objectBBox.top	: (objectBBox.CenterPoint().y + nOutBBoxHeight / 2);

						switch (pObject->m_nHeadPosition)
						{
						case TOP:		outBBox	+= CSize(WorldToLP( pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, pObject, pActPrintSheet)), 
														 WorldToLP( pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, pObject, pActPrintSheet))); 
										break;
						case RIGHT:		outBBox	+= CSize(WorldToLP( pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, pObject, pActPrintSheet)), 
													     WorldToLP(-pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, pObject, pActPrintSheet))); 
										break;
						case BOTTOM:	outBBox	+= CSize(WorldToLP(-pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, pObject, pActPrintSheet)), 
													     WorldToLP(-pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, pObject, pActPrintSheet))); 
										break;
						case LEFT:		outBBox	+= CSize(WorldToLP(-pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, pObject, pActPrintSheet)), 
													     WorldToLP( pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, pObject, pActPrintSheet))); 
										break;
						}

						if (pObjectSource->m_nType == CPageSource::SystemCreated)
							DrawSystemCreatedContentPreview(pDC, pObjectTemplate, pObjectSource, outBBox, pObject, pObject->m_nHeadPosition, pObjectSource->m_strSystemCreationInfo, pActPrintSheet);
						else
						{
							strPreviewFile = pObjectSource->GetPreviewFileName(pSepInfo->m_nPageNumInSourceFile);
							int nDC = pDC->SaveDC();	// save current clipping region
							pDC->IntersectClipRect(exposureRect);

							if ( ! DrawPreview(pDC, outBBox, exposureRect, pObject, pObjectTemplate, strPreviewFile))
								DrawPreviewNotFoundMessage(pDC, objectBBox, strPreviewFile);

							if (pObject->m_nType == CLayoutObject::Page)
								CLayoutObject::DrawPageID(pDC, objectBBox, *pObject, pActPrintSheet, *pObjectTemplate, this, RGB(242,242,255), ShowMeasure());

							pDC->RestoreDC(nDC);
						}
					}
				}
			}

			if(((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionOutput)
				m_DisplayList.RegisterItem(pDC, exposureRect, exposureRect, (void*)&rExposure, 
										   DISPLAY_ITEM_REGISTRY(CPrintSheetView, Exposure), (void*)pActPrintSheet, CDisplayItem::ForceRegister); 
		}
	}

	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActPrintSheet->GetLayout()->m_FrontSide : 
															   &pActPrintSheet->GetLayout()->m_BackSide;

//	Create the pen color for the LayoutObject-frame from choosen ColorComboBox-entry
	COLORREF crPen = MARINE;
	switch(m_nFrameColorIndex)
	{
	case 0: crPen = RGB(190,200,190); break;//MARINE; break;
	case 1: crPen = CYAN;	break;
	case 2: crPen = RED;	break;
	case 3: crPen = YELLOW; break;
	}
//
	
	CPen	pen(PS_SOLID, 1, crPen);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= pLayoutSide->m_ObjectList.GetNext(pos);
		if (rObject.m_nType != nType)
			continue;
		if (rObject.m_bHidden)
			continue;
		if (rObject.IsAutoMarkDisabled(pActPrintSheet, nActLayoutSide))
			continue;
		if (pLayoutObject)
			if (pLayoutObject != &rObject)
				continue;

		objectRect.left	  = WorldToLP(rObject.GetLeft());
		objectRect.top	  = WorldToLP(rObject.GetTop());
		objectRect.right  = WorldToLP(rObject.GetRight());
		objectRect.bottom = WorldToLP(rObject.GetBottom());
		if (pDC->RectVisible(objectRect))
		{
			objectRect.NormalizeRect();
			pDC->Rectangle(objectRect);
		}
	}
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
}

BOOL CPrintSheetView::DrawPreview(CDC* pDC, CRect outBBox, CRect clipRect, CLayoutObject* pLayoutObject, CPageTemplate* pTemplate, const CString& strPreviewFile)
{
	BOOL bDupMark = (pTemplate->m_bContentDuplicate && ((pTemplate->m_fContentGridX > 0.1f) || (pTemplate->m_fContentGridY > 0.1f)) ) ? TRUE : FALSE;
	if (bDupMark)
	{
		int nXGrid = CPrintSheetView::WorldToLP(pTemplate->m_fContentGridX);
		int nYGrid = CPrintSheetView::WorldToLP(pTemplate->m_fContentGridY);
		int nBBoxWidth  = outBBox.Width();
		int nBBoxHeight = abs(outBBox.Height());
		int nXPos = 0;
		switch (pTemplate->m_nContentPivotX)
		{
		case LEFT:		nXPos = clipRect.left;															break;
		case RIGHT:		nXPos = clipRect.right - (clipRect.Width() / nXGrid) * nXGrid;					break;
		case XCENTER:	if (pTemplate->m_bContentDuplicate)
							nXPos = clipRect.CenterPoint().x - ((clipRect.Width()/2) / nXGrid) * nXGrid - nXGrid/2;	
						else
							nXPos = clipRect.CenterPoint().x - ((clipRect.Width()/2) / nXGrid) * nXGrid;	
						break;
		}
		if (nXPos > clipRect.left)
			nXPos -= nXGrid;

		while (nXPos <= clipRect.right)
		{
			int nYPos = 0;
			switch (pTemplate->m_nContentPivotY)
			{
			case BOTTOM:	nYPos = clipRect.top;																break;
			case TOP:		nYPos = clipRect.bottom - (abs(clipRect.Height()) / nYGrid) * nYGrid;				break;
			case YCENTER:	if (pTemplate->m_bContentDuplicate)
								nYPos = clipRect.CenterPoint().y - ((abs(clipRect.Height())/2) / nYGrid) * nYGrid - nYGrid/2;	
							else
								nYPos = clipRect.CenterPoint().y - ((abs(clipRect.Height())/2) / nYGrid) * nYGrid;	
							break;
			}
			if (nYPos > clipRect.top)
				nYPos -= nYGrid;
			while (nYPos <= clipRect.bottom)
			{
				switch (pTemplate->m_nContentPivotX)
				{
				case LEFT:		outBBox.left =  nXPos;									break;
				case RIGHT:		outBBox.left =  nXPos + nXGrid		- nBBoxWidth;		break;
				case XCENTER:	outBBox.left = (nXPos + nXGrid / 2)	- nBBoxWidth / 2;	break;
				}
				switch (pTemplate->m_nContentPivotY)
				{
				case BOTTOM:	outBBox.top =  nYPos;									break;
				case TOP:		outBBox.top =  nYPos + nYGrid		- nBBoxHeight;		break;
				case YCENTER:	outBBox.top = (nYPos + nYGrid / 2)	- nBBoxHeight / 2;	break;
				}
				outBBox.right  = outBBox.left + nBBoxWidth;
				outBBox.bottom = outBBox.top  + nBBoxHeight;
				switch (pLayoutObject->m_nHeadPosition)
				{
				case TOP:		outBBox	+= CSize(WorldToLP( pTemplate->_GetContentOffsetX()), WorldToLP( pTemplate->_GetContentOffsetY())); 
								break;
				case RIGHT:		outBBox	+= CSize(WorldToLP( pTemplate->_GetContentOffsetY()), WorldToLP(-pTemplate->_GetContentOffsetX())); 
								break;
				case BOTTOM:	outBBox	+= CSize(WorldToLP(-pTemplate->_GetContentOffsetX()), WorldToLP(-pTemplate->_GetContentOffsetY())); 
								break;
				case LEFT:		outBBox	+= CSize(WorldToLP(-pTemplate->_GetContentOffsetY()), WorldToLP( pTemplate->_GetContentOffsetX())); 
								break;
				}

				CRect rcClipGrid(nXPos, nYPos, nXPos + nXGrid, nYPos + nYGrid);
				CRect rcClip = clipRect; 
				rcClipGrid.IntersectRect(rcClipGrid, rcClip);
				pDC->SaveDC();	
				pDC->IntersectClipRect(rcClipGrid);
				if ( ! CPageListView::DrawPreview(pDC, outBBox, pLayoutObject->m_nHeadPosition, 0.0f, strPreviewFile))
				{
					pDC->RestoreDC(-1);
					break;
				}
				pDC->RestoreDC(-1);

				if (pTemplate->m_fContentGridY <= 0.1f)
					break;
				if (nYGrid == 0)
					break;
				nYPos += nYGrid;
			}

			if (pTemplate->m_fContentGridX <= 0.1f)
				break;
			if (nXGrid == 0)
				break;
			nXPos += nXGrid;
		}
	}
	else
		return CPageListView::DrawPreview(pDC, outBBox, pLayoutObject->m_nHeadPosition, pTemplate->m_fContentRotation, strPreviewFile);

	return TRUE;
}

void CPrintSheetView::DrawPreviewNotFoundMessage(CDC* pDC, CRect outRect, const CString& strPreviewFile)
{
	if (pDC->IsPrinting())
		return;

	CSize fontSize = CSize(0, 12);
	pDC->DPtoLP(&fontSize);

	CFont	 font;
	font.CreateFont(fontSize.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));	
	CFont*	 pOldFont		= pDC->SelectObject(&font);
	CBrush*	 pOldBrush		= (CBrush*)pDC->SelectStockObject(WHITE_BRUSH);
	CPen*	 pOldPen		= (CPen*)pDC->SelectStockObject(WHITE_PEN);
	COLORREF crOldTextColor = pDC->SetTextColor(BLACK);
	int		 nOldBkMode		= pDC->SetBkMode(TRANSPARENT);

	CSize size(1, 1);
	pDC->DPtoLP(&size);
	CRect bkRect = outRect;
	bkRect.NormalizeRect();
	bkRect.DeflateRect(size);
	DrawRectangleExt(pDC, bkRect);

	CString strMsg;
	strMsg.Format(_T("Can't open preview file %s"), strPreviewFile);
	outRect.left += size.cx;
	pDC->DrawText(strMsg, outRect, DT_LEFT | DT_END_ELLIPSIS | DT_WORDBREAK);

	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	pDC->SetTextColor(crOldTextColor);
	pDC->SetBkMode(nOldBkMode);

	font.DeleteObject();
}


void CPrintSheetView::DrawSystemCreatedContentPreview(CDC* pDC, CPageTemplate* pObjectTemplate, CPageSource* pObjectSource, CRect outBBox, CLayoutObject* pObject, int nHeadPosition, const CString& strSystemCreationInfo, 
													  CPrintSheet* pPrintSheet)
{
	int	nIndex = strSystemCreationInfo.Find(_T(" "));
	if (nIndex == -1)	// invalid string
		return;

	CString strContentType = strSystemCreationInfo.Left(nIndex);
	CString strParams	   = strSystemCreationInfo.Right(strSystemCreationInfo.GetLength() - nIndex);
	strParams.TrimLeft(); strParams.TrimRight();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	int nSide = (pObject) ? pObject->GetLayoutSideIndex() : (pDoc) ? pDoc->GetActSight() : FRONTSIDE;
	CPlate*	pPlate = (pPrintSheet) ? pPrintSheet->GetTopmostPlate(nSide) : NULL;

	if (strContentType == "$TEXT")
	{
		BOOL	bScaleToFit = FALSE;
		CString strText;
		nIndex = strParams.Find(_T("/EOT"));
		if (nIndex == -1)	
		{
			strText	  = strParams;
			strParams = "";
		}
		else
		{
			strText	  = strParams.Left(nIndex);
			nIndex	  = strParams.GetLength() - nIndex - 4;	// 4 = length of $EOT
			strParams = (nIndex >= 0) ? strParams.Right(nIndex) : strParams;	
			strParams.TrimLeft(); strParams.TrimRight();
			bScaleToFit = (strParams.Find(_T("/Font (Barcode")) != -1) ? TRUE : FALSE;
		}

		if (pPlate)
		{
			TCHAR szDest[1024];
			if ( (pPlate->m_strPlateName.CompareNoCase(_T("Composite")) == 0) && (strText.Find(_T("$2")) != -1) )	// special handling for color name output in case of composite
			{
				BOOL					   bColorSpaceAll;
				float					   fC = 0.0f, fM = 0.0f, fY = 0.0f, fK = 0.0f;
				CStringArray			   spotColorNames;
				CArray<COLORREF, COLORREF> spotColorRGBs;
				CArray<float,	 float>	   spotColorTints;
				CPDFOutput::BuildCompositeColorInfo(pObjectTemplate, pObjectSource, pPlate, bColorSpaceAll, fC, fM, fY, fK, spotColorNames, spotColorRGBs, spotColorTints);
				CTextMark::DrawTextfieldPreview(pDC, strText, outBBox, nHeadPosition, pPrintSheet, pPlate, fC, fM, fY, fK, spotColorNames, spotColorRGBs, (pObject) ? &pObject->m_reflinePosParams : NULL);
			}
			else
			{
				CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strText, pPrintSheet, pPlate, -1, _T(""), (pObject) ? &pObject->m_reflinePosParams : NULL);
				CTextMark::DrawTextfieldPreview(pDC, szDest, outBBox, nHeadPosition, pPrintSheet, bScaleToFit, BLACK);
			}
		}
	}
	else
	if (strContentType == "$SECTION_BAR")
	{
		float fBarWidth, fBarHeight;
		Ascii2Float((LPCTSTR)strParams, _T("%f %f"), &fBarWidth, &fBarHeight);
		BOOL bNumRotated180 = (strParams.Find(_T("NUMROTATED:180")) == -1) ? FALSE : TRUE;
		BOOL bHollow		= (strParams.Find(_T("HOLLOW"))			== -1) ? FALSE : TRUE;
		CSectionMark::DrawSectionBarPreview(pDC, WorldToLP(fBarWidth), WorldToLP(fBarHeight), bNumRotated180, bHollow, outBBox, 
											(pObject) ? pObject->m_nComponentRefIndex : 0, nHeadPosition, pPrintSheet, 
											(pObject) ? ((pObject->m_nAttribute & CLayoutObject::FoldSheetCGMember) ? TRUE : FALSE) : FALSE, pObjectTemplate->m_strPageID);
	}
	else
	if (strContentType == "$CROP_LINE")
	{
		CCropMark::DrawCropLinePreview(pDC, outBBox);
	}
	else
	if (strContentType == "$FOLD_MARK")
	{
		BOOL bSolid		  = (strParams.Find(_T("/Solid"))		 != -1) ? TRUE : FALSE;
		BOOL bTransparent = (strParams.Find(_T("/Transparent")) != -1) ? TRUE : FALSE;
		CFoldMark::DrawFoldLinePreview(pDC, bSolid, bTransparent, outBBox);
	}
	else
	if (strContentType == "$BARCODE")
	{
		CString strOutputText  =			 GetStringToken(strParams, 0, _T("|"));
		CString strBarcodeType =			 GetStringToken(strParams, 1, _T("|"));
		CString strTextOption  =			 GetStringToken(strParams, 2, _T("|"));
		float   fTextHeight	   = Ascii2Float(GetStringToken(strParams, 3, _T("|")));

		if (pPlate)
		{
			if ( (pPlate->m_strPlateName.CompareNoCase(_T("Composite")) == 0) && (strOutputText.Find(_T("$2")) != -1) )	// special handling for color name output in case of composite
			{
				BOOL					   bColorSpaceAll;
				float					   fC = 0.0f, fM = 0.0f, fY = 0.0f, fK = 0.0f;
				CStringArray			   spotColorNames;
				CArray<COLORREF, COLORREF> spotColorRGBs;
				CArray<float,	 float>	   spotColorTints;
				CPDFOutput::BuildCompositeColorInfo(pObjectTemplate, pObjectSource, pPlate, bColorSpaceAll, fC, fM, fY, fK, spotColorNames, spotColorRGBs, spotColorTints);
				CBarcodeMark::DrawBarcodePreview(pDC, strOutputText, strBarcodeType, strTextOption, fTextHeight, outBBox, nHeadPosition, pPrintSheet, pPlate, fC, fM, fY, fK, spotColorNames, spotColorRGBs, (pObject) ? &pObject->m_reflinePosParams : NULL);
			}
			else
				CBarcodeMark::DrawBarcodePreview(pDC, strOutputText, strBarcodeType, strTextOption, fTextHeight, outBBox, nHeadPosition, pPrintSheet, pPlate, _T(""), (pObject) ? &pObject->m_reflinePosParams : NULL);
		}
	}
	else
	if (strContentType == "$RECTANGLE")
	{
		BOOL					   bColorSpaceAll;
		float					   fC = 0.0f, fM = 0.0f, fY = 0.0f, fK = 0.0f;
		CStringArray			   spotColorNames;
		CArray<COLORREF, COLORREF> spotColorCMYKs;
		CArray<float,	 float>	   spotColorTints;

		BOOL bComposite = ( (pPlate->m_strPlateName.CompareNoCase(_T("Composite")) == 0) || pPlate->m_strPlateName.IsEmpty() ) ? TRUE : FALSE;	// by definition: if no pages assigned (default plate) assume composite
		if (bComposite)
			CPDFOutput::BuildCompositeColorInfo(pObjectTemplate, pObjectSource, pPlate, bColorSpaceAll, fC, fM, fY, fK, spotColorNames, spotColorCMYKs, spotColorTints);
		else	
			fK = 1.0f;	// in case of separation -> output in black (gray) / colorspace all is not relevant here, because this has already been considered in ReorganizePrintSheetPlates()
		if ( INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) )
			CDrawingMark::DrawRectanglePreview(pDC, outBBox, CColorDefinition::CMYK2RGB(CMYK((int)(fC*100.0f), (int)(fM*100.0f), (int)(fY*100.0f), (int)(fK*100.0f))));
		for (int i = 0; i < spotColorNames.GetSize(); i++)
		{
			COLORREF crSpot = CColorDefinition::CMYK2RGB(spotColorCMYKs.ElementAt(i));
			crSpot = CGraphicComponent::IncreaseColorBrightness(crSpot, 1.0f + (1.0f - spotColorTints[i])*0.6f);
			CDrawingMark::DrawRectanglePreview(pDC, outBBox, crSpot);
		}
	}
}


void CPrintSheetView::DrawPlate(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, CLayout* pActLayout, BOOL bDrawTitle) 
{
	if (! ShowPlate())
		return;
	
	CLayoutSide* pLayoutSide = NULL;
	CPlateList*  pPlateList  = NULL;

	switch(nActLayoutSide)
	{
	case FRONTSIDE: pLayoutSide = &pActLayout->m_FrontSide;
					pPlateList = &pActPrintSheet->m_FrontSidePlates;
					break;
	case BACKSIDE : pLayoutSide = &pActLayout->m_BackSide;
					pPlateList = &pActPrintSheet->m_BackSidePlates;
					break;
	}

	if ( !pLayoutSide || !pPlateList)
		return;
 
	CPressDevice* pPressDevice = pLayoutSide->GetPressDevice();

	if (pPressDevice == NULL)
		return;

	if ((pPlateList->GetCount() > 0) && (pPlateList->m_nTopmostPlate < 0))
		pPlateList->m_nTopmostPlate = 0;	

	CRect plateRect;
	plateRect.left	 = WorldToLP(0);
	plateRect.top	 = WorldToLP(pPressDevice->m_fPlateHeight);
	plateRect.right	 = WorldToLP(pPressDevice->m_fPlateWidth);
	plateRect.bottom = WorldToLP(0);

	//if ( (ShowExposures() || ShowBitmap()) && ! ShowColorants())
	{
		CSize tabSize	(WorldToLP(200), WorldToLP(40));
		CSize tabOverlap(WorldToLP(20), 0);
		CSize testSize = tabSize;
		pDC->LPtoDP(&testSize);
		if (testSize.cy > 16)
		{
			tabSize = CSize(80, 16); tabOverlap = CSize(8, 0);
			pDC->DPtoLP(&tabSize);
			pDC->DPtoLP(&tabOverlap);
		}

		tabSize.cx	  *= m_fScreenPrinterRatioX; tabSize.cy	   *= m_fScreenPrinterRatioY;
		tabOverlap.cx *= m_fScreenPrinterRatioX; tabOverlap.cy *= m_fScreenPrinterRatioY;

		CRect clipRect	 = plateRect;
		clipRect.top    += 2 * tabSize.cy;
		clipRect.bottom -= 2 * tabSize.cy;
		pDC->IntersectClipRect(clipRect);

		// In order to get the visual effect of topmost plate's tab being on top:
		//	- first draw normal and selected state tabs
		CPoint  tagPos(plateRect.left, plateRect.top);
		CPlate* pTopmostPlate = pPlateList->GetTopmostPlate();
		CRect   tabRect		  = CRect(plateRect.left, plateRect.top + tabSize.cy, plateRect.left + tabSize.cx, plateRect.top);
		if (pPlateList->GetCount() > 1)
		{
			POSITION pos = pPlateList->GetHeadPosition();
			while (pos)
			{
				CPlate& rPlate = pPlateList->GetNext(pos);
				if (&rPlate != pTopmostPlate)
					DrawPlateTab(pDC, pActPrintSheet, rPlate, pTopmostPlate, tabRect, FALSE);
				tagPos.x = max(tagPos.x, tabRect.right);
				tabRect  = CRect(tabRect.right - tabOverlap.cx, plateRect.top + tabSize.cy, 
					 			 tabRect.right + tabSize.cx - tabOverlap.cx, plateRect.top);
			}
			//	- than draw tab of topmost plate
			tabRect		= CRect(plateRect.left, plateRect.top + tabSize.cy, plateRect.left + tabSize.cx, plateRect.top);
			pos			= pPlateList->GetHeadPosition();
			while (pos)
			{
				CPlate&	rPlate = pPlateList->GetNext(pos);
				if (&rPlate == pTopmostPlate)
					DrawPlateTab(pDC, pActPrintSheet, rPlate, pTopmostPlate, tabRect, TRUE);
				tagPos.x = max(tagPos.x, tabRect.right);
				tabRect = CRect(tabRect.right - tabOverlap.cx, plateRect.top + tabSize.cy, 
								tabRect.right + tabSize.cx - tabOverlap.cx, plateRect.top);
			}
		}

		if ( ! ShowBitmap() && ! MarksViewActive() && IsOpenDlgMaskSettings())
			DrawGlobalMaskTag(pDC, pTopmostPlate, tagPos + CSize(tabOverlap.cx, 0), pActPrintSheet);

		tabSize.cx *= 2;
		tabRect		= CRect(plateRect.left, plateRect.bottom, plateRect.left + tabSize.cx, plateRect.bottom - tabSize.cy);
		//for (int i = 0; i < pTopmostPlate->m_spotColors.GetSize(); i++)
		//{
		//	CString strTargetColorName = theApp.MapPrintColor(pTopmostPlate->m_spotColors[i]);
		//	if ( (strTargetColorName == pTopmostPlate->m_spotColors[i]) || strTargetColorName.IsEmpty()) // if empty, spot color not defined (will not be mapped) -> show it
		//	{
		//		DrawSpotColorTab(pDC, pActPrintSheet, pTopmostPlate->m_spotColors[i], tabRect);
		//		tagPos.x = max(tagPos.x, tabRect.right);
		//		tabRect = CRect(tabRect.right - tabOverlap.cx, plateRect.bottom, 
		//						tabRect.right + tabSize.cx - tabOverlap.cx, plateRect.bottom - tabSize.cy);
		//	}
		//}

		// Only reinit ClipRgn -> because of previously pDC->SelectClipRgn(NULL)
		ClipStatusBars(pDC);
	}

	// Draw the plate rectangle
	if (pDC->RectVisible(plateRect))
	{
		CPen   grayPen, blackPen, *pOldPen;
		CBrush plateBrush, bkgrdBrush, *pOldBrush;

		pOldPen	  = pDC->GetCurrentPen();
		pOldBrush = pDC->GetCurrentBrush();

		grayPen.CreatePen (PS_SOLID, Pixel2ConstMM(pDC, 1), LIGHTGRAY);
		blackPen.CreatePen(PS_SOLID, Pixel2ConstMM(pDC, 1), MIDDLEGRAY);
		plateBrush.CreateSolidBrush(RGB(245,245,245));//PLATE_COLOR); 
		bkgrdBrush.CreateSolidBrush(MIDDLEGRAY);

		pDC->FillRect(plateRect, &plateBrush);

		pDC->SelectObject(&grayPen);
		pDC->MoveTo(plateRect.left,  plateRect.bottom);
		pDC->LineTo(plateRect.left,  plateRect.top);
		pDC->LineTo(plateRect.right, plateRect.top);

		//pDC->SelectObject(&blackPen);
		pDC->LineTo(plateRect.right, plateRect.bottom);
		pDC->LineTo(plateRect.left,  plateRect.bottom);

		// Draw two holes on the bottom of the plate
		pDC->SelectObject(&bkgrdBrush);
		CRect holeRect;
		holeRect.left	= WorldToLP(pPressDevice->m_fPlateWidth / 4);
		holeRect.top	= WorldToLP(15);
		holeRect.right	= holeRect.left + WorldToLP(25);
		holeRect.bottom	= WorldToLP(5);
		pDC->RoundRect(&holeRect, CPoint(WorldToLP(10), WorldToLP(10)) );
		holeRect.left	= (holeRect.left*3) - WorldToLP(25);
		holeRect.top	= WorldToLP(15);
		holeRect.right	= holeRect.left + WorldToLP(25);
		holeRect.bottom	= WorldToLP(5);
		pDC->RoundRect(&holeRect, CPoint(WorldToLP(10), WorldToLP(10)) );

		pDC->SelectObject(pOldPen);
		pDC->SelectObject(pOldBrush);

		grayPen.DeleteObject();
		blackPen.DeleteObject();
		plateBrush.DeleteObject();
		bkgrdBrush.DeleteObject();
	}

	CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, plateRect, plateRect, (void*)pLayoutSide, SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Plate), (void*)pActPrintSheet, CDisplayItem::ForceRegister | CDisplayItem::NotClickable); 
	if (pDI)
		pDI->SetHighlightColor(RED);

	UpdateLeftStatusBar (pDC, plateRect, pActPrintSheet, pLayoutSide);
	UpdateUpperStatusBar(pDC, plateRect, pActPrintSheet, pLayoutSide, bDrawTitle);
}

void CPrintSheetView::DrawPlateTab(CDC* pDC, CPrintSheet* pActPrintSheet, CPlate& rPlate, CPlate* pTopmostPlate, CRect& rTabRect, BOOL bTopmost)
{
	CImpManDoc* pDoc  = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	COLORREF	crRGB = (rPlate.m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) ? 
						 pDoc->m_ColorDefinitionTable[rPlate.m_nColorDefinitionIndex].m_rgb : WHITE;
	CPen		tabPen(PS_SOLID, 0, DARKGRAY);
	CBrush		tabBrush;
	if (!bTopmost)
		tabBrush.CreateSolidBrush(MIDDLEGRAY); 
	else
		tabBrush.CreateSolidBrush(crRGB);	

	CPoint p[5] = { 
					CPoint(rTabRect.left,							rTabRect.bottom),
					CPoint(rTabRect.left  + rTabRect.Width()/10,	rTabRect.top),
					CPoint(rTabRect.right - rTabRect.Width()/10,	rTabRect.top),
					CPoint(rTabRect.right,							rTabRect.bottom),
					CPoint(rTabRect.left,							rTabRect.bottom),
				  };

	CPen*	pOldPen   = pDC->SelectObject(&tabPen);
	CBrush*	pOldBrush = pDC->SelectObject(&tabBrush);
	pDC->BeginPath();
	pDC->Polyline(p, 5);
	pDC->EndPath();
	pDC->StrokeAndFillPath();

	CRect  textRect;
	int    nGap  = abs(rTabRect.Height()) / 10;
	int	   nSize = abs(rTabRect.Height()) - 2 * nGap;
	if (!bTopmost)
	{
		CRect colorRect(p[1].x + nGap, p[1].y - nGap, p[1].x + nGap + nSize, p[0].y + nGap);
		CBrush colorBrush(crRGB);
		pDC->SelectObject(&colorBrush);

		pDC->Rectangle(colorRect);

		colorBrush.DeleteObject();
		textRect.left = colorRect.right + nGap;
	}
	else
		textRect.left = p[1].x; 

	textRect.right	= p[2].x; 
	textRect.top	= rTabRect.top	  - nGap; 
	textRect.bottom = rTabRect.bottom + nGap;

	CString string;
	if (rPlate.m_fOverbleedOutside != pTopmostPlate->m_fOverbleedOutside)
		string.Format(_T("! %s"), rPlate.m_strPlateName);
	else
		string = rPlate.m_strPlateName;

	CSize fontSize = CSize(0, abs(rTabRect.Height()));
	pDC->LPtoDP(&fontSize);
	if (fontSize.cy > 13)
		fontSize.cy = 13;
	pDC->DPtoLP(&fontSize);
	fontSize.cx	*= m_fScreenPrinterRatioX; fontSize.cy *= m_fScreenPrinterRatioY;

	CFont	 font;
	font.CreateFont(fontSize.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));	
	CFont*	 pOldFont		= pDC->SelectObject(&font);
	int		 nOldBkMode		= pDC->SetBkMode(TRANSPARENT);
	int		 nBrightness	= (bTopmost) ? (GetRValue(crRGB) + GetGValue(crRGB) + GetBValue(crRGB)) / 3 : INT_MAX;
	COLORREF crOldTextColor = pDC->SetTextColor((nBrightness < 100) ? WHITE : BLACK);
	
	CPageListTableView::FitStringToWidth(pDC, string, textRect.Width());
	pDC->DrawText(string, textRect, DT_SINGLELINE|DT_LEFT|DT_VCENTER);

	pDC->SetTextColor(crOldTextColor);
	pDC->SetBkMode	 (nOldBkMode);
	pDC->SelectObject(pOldFont);

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	tabPen.DeleteObject();
	tabBrush.DeleteObject();
	font.DeleteObject();

	pDC->LPtoDP(p, 5);
	CRgn* pClipRgn = new CRgn;	// CRgn object will be deleted by DisplayList
	pClipRgn->CreatePolygonRgn(p, 5, WINDING);	// Create clipping region
	m_DisplayList.RegisterItem(pDC, rTabRect, rTabRect, (void*)&rPlate, 
							   DISPLAY_ITEM_REGISTRY(CPrintSheetView, PlateTab), (void*)pActPrintSheet, 
							   CDisplayItem::CtrlDeselectType | CDisplayItem::ForceRegister, pClipRgn); 
}

void CPrintSheetView::DrawSpotColorTab(CDC* pDC, CPrintSheet* pActPrintSheet, CString strSpotColor, CRect& rTabRect)
{
	CImpManDoc* pDoc  = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPen	tabPen(PS_SOLID, 0, DARKGRAY);
	CBrush	tabBrush;
	tabBrush.CreateSolidBrush(RGB(251,223,162));//RGB(210,210,210));	

	CSize fontSize = CSize(0, abs(rTabRect.Height()));
	pDC->LPtoDP(&fontSize);
	if (fontSize.cy > 11)
		fontSize.cy = 11;
	pDC->DPtoLP(&fontSize);
	fontSize.cx	*= m_fScreenPrinterRatioX; fontSize.cy *= m_fScreenPrinterRatioY;

	CFont	 font;
	font.CreateFont(fontSize.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));	
	CFont*	 pOldFont		= pDC->SelectObject(&font);
	int		 nOldBkMode		= pDC->SetBkMode(TRANSPARENT);
	COLORREF crOldTextColor = pDC->SetTextColor(RGB(64,64,64));
	
	CSize sizeDiff(20, 20);
	pDC->DPtoLP(&sizeDiff);
	int     nGap	 = abs(rTabRect.Height()) / 5;
	int		nDiff	 = abs(rTabRect.Height()) / 2;
	CString string   = strSpotColor;
	CSize textExtent = pDC->GetTextExtent(string);
	textExtent.cx = max(textExtent.cx, 3 * nDiff);
	if (textExtent.cx < rTabRect.Width())
		rTabRect.right = rTabRect.left + textExtent.cx + 2 * (nGap + nDiff) + sizeDiff.cx;

	CPen*	pOldPen   = pDC->SelectObject(&tabPen);
	CBrush*	pOldBrush = pDC->SelectObject(&tabBrush);

	CPoint p[5] = { 
					CPoint(rTabRect.left,			rTabRect.top),
					CPoint(rTabRect.left  + nDiff,	rTabRect.bottom),
					CPoint(rTabRect.right - nDiff,	rTabRect.bottom),
					CPoint(rTabRect.right,			rTabRect.top),
					CPoint(rTabRect.left,			rTabRect.top),
				  };

	pDC->BeginPath();
	pDC->Polyline(p, 5);
	pDC->EndPath();
	pDC->StrokeAndFillPath();

	int	  nSize = abs(rTabRect.Height()) - 2 * nGap;
	CRect colorRect(p[1].x + nGap, p[0].y - nGap, p[1].x + nGap + nSize, p[1].y + nGap);
	COLORREF crColor = pDoc->m_ColorDefinitionTable.FindColorRef(strSpotColor);
	if (crColor == WHITE)
		crColor = theApp.m_colorDefTable.FindColorRef(strSpotColor);
	CBrush colorBrush(crColor);
	pDC->SelectObject(&colorBrush);
	pDC->Rectangle(colorRect);
	colorBrush.DeleteObject();

	CRect  textRect;
	textRect.left = colorRect.right + nGap;
	textRect.right	= p[2].x - nGap; 
	textRect.top	= rTabRect.top	  - nGap; 
	textRect.bottom = rTabRect.bottom + nGap;

	//CPageListTableView::FitStringToWidth(pDC, string, textRect.Width());
	pDC->DrawText(string, textRect, DT_SINGLELINE|DT_LEFT|DT_VCENTER|DT_END_ELLIPSIS);

	pDC->SetTextColor(crOldTextColor);
	pDC->SetBkMode	 (nOldBkMode);
	pDC->SelectObject(pOldFont);

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	tabPen.DeleteObject();
	tabBrush.DeleteObject();
	font.DeleteObject();
}

void CPrintSheetView::DrawGlobalMaskTag(CDC* pDC, CPlate* pPlate, CPoint tagPos, CPrintSheet* pActPrintSheet)
{
    if (ViewMode() == ID_VIEW_ALL_SHEETS)  
		return;

	if (!pPlate)
		return;

	CSize size(15, 15);
	pDC->DPtoLP(&size);
	size.cx *= m_fScreenPrinterRatioX; size.cy *= m_fScreenPrinterRatioY;
	CRect iconRect(tagPos.x, tagPos.y + size.cy, tagPos.x + size.cx, tagPos.y);
	CRect textRect(iconRect.right + 1, iconRect.top, iconRect.right + 1, iconRect.bottom);

	CBitmap bitmap;	bitmap.LoadBitmap(IDB_GLOBALMASK);
	BITMAP bm;   	bitmap.GetBitmap(&bm);
	CDC memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = memDC.SelectObject(&bitmap);
	pDC->StretchBlt(iconRect.left, iconRect.top, iconRect.Width(), iconRect.Height(), &memDC, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);
	memDC.SelectObject(pOldBitmap);

	CFont*	 pOldFont = (CFont*)pDC->SelectStockObject(ANSI_VAR_FONT);
	COLORREF crOld    = pDC->SetTextColor(BLACK);
	int		 bkOld    = pDC->SetBkMode(TRANSPARENT);

	CString string;
	TruncateNumber(string, pPlate->m_fOverbleedOutside);
	textRect.right+= pDC->GetTextExtent(string).cx + pDC->GetTextExtent("X").cx;

	CPen	pen(PS_SOLID, 0, DARKGRAY);
	CPen*	pOldPen	  = (CPen*)  pDC->SelectObject(&pen);
	CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(WHITE_BRUSH);
	pDC->Rectangle(textRect);
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();

	pDC->DrawText(string, textRect, DT_SINGLELINE|DT_VCENTER|DT_CENTER);

	iconRect.NormalizeRect();
	textRect.NormalizeRect();
	CRect uRect;
	uRect.UnionRect(iconRect, textRect);
	m_DisplayList.RegisterItem(pDC, uRect, textRect, (void*)pPlate, 
							   DISPLAY_ITEM_REGISTRY(CPrintSheetView, GlobalMaskTag), (void*)pActPrintSheet, CDisplayItem::ForceRegister); 

	// Add tooltip to GlobalMask - button/editfield
	pDC->LPtoDP(uRect);
	long temp	 = uRect.top;	// Un-Normalize the rect
	uRect.top	 = uRect.bottom;// only for registering the tooltip
	uRect.bottom = temp;
	m_tooltip.AddTool(this, IDS_AUTOMATIC_BLEED, uRect, 1); // ID must be > 0

	pDC->SetTextColor(crOld);
	pDC->SetBkMode(bkOld);
	pDC->SelectObject(pOldFont);
}

void CPrintSheetView::DrawExposures(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	if ( ! ShowExposures() || ShowBitmap())
		return;
	if (ShowColorants() && ! ShowExposures())
		return;

	CArray   <CRect, CRect&> rectList;
	CPlate*	pPlate = pActPrintSheet->GetTopmostPlate(nActLayoutSide);
	if ( ! pPlate)
		return;

	CSize size(13, 13);
	pDC->DPtoLP(&size);
	CFont font;
	font.CreateFont((int)(size.cy * m_fScreenPrinterRatioY), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
    CFont* pOldFont	= pDC->SelectObject(&font);

	CRect	 exposureRect, pageRect, upperBleedRect, lowerBleedRect, leftBleedRect, rightBleedRect, gluelineRect;
	CPen	 exposurePen(PS_SOLID, Pixel2ConstMM(pDC, 1), BLACK);
	CPen	 pagePen	(PS_SOLID, Pixel2ConstMM(pDC, 1), DARKGRAY);
	CPen*    pOldPen   = pDC->GetCurrentPen();
	CBrush*	 pOldBrush = pDC->GetCurrentBrush();
	CBrush   defaultPageBrush(MIDDLEGRAY);		
	CBrush   pageBrush(MARINE);		

	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActPrintSheet->GetLayout()->m_FrontSide : 
															   &pActPrintSheet->GetLayout()->m_BackSide;
	pDC->SelectObject(&pagePen);
	pDC->SelectStockObject(HOLLOW_BRUSH);
	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	= pLayoutSide->m_ObjectList.GetNext(pos);
		pageRect.left			= WorldToLP(pPlate->GetObjectTrimBox(LEFT,   rObject));
		pageRect.top			= WorldToLP(pPlate->GetObjectTrimBox(TOP,	 rObject));
		pageRect.right			= WorldToLP(pPlate->GetObjectTrimBox(RIGHT,  rObject));
		pageRect.bottom			= WorldToLP(pPlate->GetObjectTrimBox(BOTTOM, rObject));
		if (pDC->RectVisible(pageRect))
			DrawRectangleExt(pDC, pageRect);
	}

	int	 nLayoutObjectIndex = 0;
	pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		BOOL bIsDefault = (nActLayoutSide == FRONTSIDE) ? ( (pActPrintSheet->m_FrontSidePlates.GetCount()) ? FALSE : TRUE ) : ( (pActPrintSheet->m_BackSidePlates.GetCount()) ? FALSE : TRUE );
		CLayoutObject* pObject = &pLayoutSide->m_ObjectList.GetNext(pos);
		if ( ! pObject)
			continue;
		if (pObject->m_bHidden)
			continue;
		if (pObject->IsAutoMarkDisabled(pActPrintSheet, nActLayoutSide))
			continue;

		CExposure* pExposure = pPlate->m_ExposureSequence.GetExposure(nLayoutObjectIndex);
		if ( ! pExposure)
		{
			pExposure  = pPlate->GetDefaultExposure(nLayoutObjectIndex);
			bIsDefault = TRUE;
		}
		nLayoutObjectIndex++;
		if ( ! pExposure)
			continue;

		COLORREF	   crExposure	   = (bIsDefault) ? MIDDLEGRAY : MARINE;
		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPlate->GetPrintSheet());
		if (pObjectTemplate)
		{
			exposureRect.left	= WorldToLP(pExposure->GetSheetBleedBox(LEFT,   pObject, pObjectTemplate));
			exposureRect.top	= WorldToLP(pExposure->GetSheetBleedBox(TOP,    pObject, pObjectTemplate));
			exposureRect.right	= WorldToLP(pExposure->GetSheetBleedBox(RIGHT,  pObject, pObjectTemplate));
			exposureRect.bottom	= WorldToLP(pExposure->GetSheetBleedBox(BOTTOM, pObject, pObjectTemplate));
			pageRect.left		= WorldToLP(pExposure->GetSheetTrimBox (LEFT,   pObject, pObjectTemplate));
			pageRect.top		= WorldToLP(pExposure->GetSheetTrimBox (TOP,    pObject, pObjectTemplate));
			pageRect.right		= WorldToLP(pExposure->GetSheetTrimBox (RIGHT,  pObject, pObjectTemplate));
			pageRect.bottom		= WorldToLP(pExposure->GetSheetTrimBox (BOTTOM, pObject, pObjectTemplate));
			upperBleedRect		= CRect(exposureRect.left, exposureRect.top, exposureRect.right, pageRect.top);
			leftBleedRect		= CRect(exposureRect.left, exposureRect.top, pageRect.left,      exposureRect.bottom);
			lowerBleedRect		= CRect(exposureRect.left, pageRect.bottom,  exposureRect.right, exposureRect.bottom);
			rightBleedRect		= CRect(pageRect.right,    exposureRect.top, exposureRect.right, exposureRect.bottom);

			exposureRect.NormalizeRect();
			pageRect.NormalizeRect();
			if (pDC->RectVisible(pageRect))
			{
				pageRect.IntersectRect(exposureRect, pageRect);
				pDC->SelectStockObject(NULL_PEN);
				pDC->SelectObject((bIsDefault) ? &defaultPageBrush : &pageBrush);
				DrawRectangleExt(pDC, pageRect);
			}
			
			if (pDC->RectVisible(exposureRect))
			{
				if (pExposure->GetBleedValue(TOP)	 > 0.0f)
					FillTransparent(pDC, upperBleedRect, crExposure, TRUE);
				
				if (pExposure->GetBleedValue(LEFT)   > 0.0f)
					FillTransparent(pDC, leftBleedRect,  crExposure, TRUE);
				
				if (pExposure->GetBleedValue(BOTTOM) > 0.0f)
					FillTransparent(pDC, lowerBleedRect, crExposure, TRUE);
				
				if (pExposure->GetBleedValue(RIGHT)	 > 0.0f)
					FillTransparent(pDC, rightBleedRect, crExposure, TRUE);

				if ( (pObject->m_nType == CLayoutObject::Page) && ! pObject->IsFlatProduct())
				{
					BOOL bHasGlueline = FALSE;
					CBoundProduct& rProduct = pObjectTemplate->GetProductPart().GetBoundProduct();
					if ( (fabs(rProduct.m_bindingParams.m_fMillingDepth + rProduct.m_bindingParams.m_fGlueLineWidth) > 0.01) && (rProduct.m_bindingParams.m_bindingDefs.m_nBinding == CFinalProductDescription::PerfectBound))
					{
						if ( ! pObjectTemplate->GetProductPart().IsCover())
							bHasGlueline = TRUE;
					}
					else
						if ( (fabs(rProduct.m_bindingParams.m_fGlueLineWidth) > 0.01) && (rProduct.m_bindingParams.m_bindingDefs.m_nBinding == CFinalProductDescription::SaddleStitched))
						{
							if ( ! pObjectTemplate->IsFirstLastOrInnerPage())
							{
								if (fabs(rProduct.m_bindingParams.m_fGlueLineWidth) > 0.01)
									bHasGlueline = TRUE;
							}
						}

					if (bHasGlueline)
					{
						CString strValue; CPrintSheetView::TruncateNumber(strValue, rProduct.m_bindingParams.m_fGlueLineWidth);
						CSize   szText = pDC->GetTextExtent(strValue);
						CRect objectRect;
						objectRect.left		 = WorldToLP(pObject->GetLeft());
						objectRect.top		 = WorldToLP(pObject->GetTop());
						objectRect.right	 = WorldToLP(pObject->GetRight());
						objectRect.bottom	 = WorldToLP(pObject->GetBottom());
						int   nGlueline = WorldToLP(rProduct.m_bindingParams.m_fGlueLineWidth);
						CSize szOffset(2, 2); pDC->DPtoLP(&szOffset);
						int   nOffset	= szOffset.cx + szText.cx/2 + nGlueline/2;
						switch (pObject->GetSpineSide())
						{
						case LEFT:		gluelineRect = CRect(objectRect.left,			   objectRect.top,					objectRect.left + nGlueline,	objectRect.bottom);				szOffset = CSize( nOffset, 0);	break;
						case RIGHT:		gluelineRect = CRect(objectRect.right - nGlueline, objectRect.top,					objectRect.right,				objectRect.bottom);				szOffset = CSize(-nOffset, 0);	break;
						case BOTTOM:	gluelineRect = CRect(objectRect.left,			   objectRect.bottom + nGlueline,	objectRect.right,				objectRect.bottom);				szOffset = CSize(0,  nOffset);	break;
						case TOP:		gluelineRect = CRect(objectRect.left,			   objectRect.top,					objectRect.right,				objectRect.top - nGlueline);	szOffset = CSize(0, -nOffset);	break;
						}
						CRect rcText(gluelineRect.CenterPoint(), gluelineRect.CenterPoint()); rcText.InflateRect(szText.cx/2, szText.cy/2);
						rcText.OffsetRect(szOffset);
						FillTransparent(pDC, gluelineRect, OCKER, TRUE, 180, FALSE);
						pDC->SetBkMode(OPAQUE); pDC->SetBkColor(RGB(255,200,80)); pDC->SetTextColor(BLACK);
						pDC->DrawText(strValue, rcText, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
					}
				}

				pDC->SelectObject(&exposurePen);
				pDC->SelectStockObject(HOLLOW_BRUSH);
				DrawRectangleExt(pDC, exposureRect);
				rectList.Add(exposureRect);
			}
		}
	}

	for (int i = 0; i < rectList.GetSize(); i++)
	{
		CRect& rRect1 = rectList[i];
		for (int j = i + 1; j < rectList.GetSize(); j++)
		{
 			CRect intersectRect;
			if (intersectRect.IntersectRect(rRect1, rectList[j]))
				FillTransparent(pDC, intersectRect, RED);
		}
	}

	COLORREF crOldTextColor = pDC->SetTextColor(YELLOW);
	COLORREF crOldBkColor	= pDC->GetBkColor  ();
	int		 nOldBkMode		= pDC->SetBkMode   (OPAQUE);

	nLayoutObjectIndex = 0;
	pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		BOOL		   bIsDefault = (pPlate->m_strPlateName.IsEmpty()) ? TRUE : FALSE;
		CLayoutObject* pObject	  = &pLayoutSide->m_ObjectList.GetNext(pos);
		if ( ! pObject)
			continue;
		CExposure* pExposure = pPlate->m_ExposureSequence.GetExposure(nLayoutObjectIndex);
		if ( ! pExposure)
		{
			pExposure  = pPlate->GetDefaultExposure(nLayoutObjectIndex);
			bIsDefault = TRUE;
		}
		nLayoutObjectIndex++;
		if ( ! pExposure)
			continue;

		pDC->SetBkColor((bIsDefault) ? MIDDLEGRAY : MARINE);
		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPlate->GetPrintSheet());
		if (pObjectTemplate)
		{
			exposureRect.left	= WorldToLP(pExposure->GetSheetBleedBox(LEFT,   pObject, pObjectTemplate));
			exposureRect.top	= WorldToLP(pExposure->GetSheetBleedBox(TOP,	pObject, pObjectTemplate));
			exposureRect.right	= WorldToLP(pExposure->GetSheetBleedBox(RIGHT,  pObject, pObjectTemplate));
			exposureRect.bottom	= WorldToLP(pExposure->GetSheetBleedBox(BOTTOM, pObject, pObjectTemplate));
			pageRect.left		= WorldToLP(pExposure->GetSheetTrimBox (LEFT,   pObject, pObjectTemplate));
			pageRect.top		= WorldToLP(pExposure->GetSheetTrimBox (TOP,    pObject, pObjectTemplate));
			pageRect.right		= WorldToLP(pExposure->GetSheetTrimBox (RIGHT,  pObject, pObjectTemplate));
			pageRect.bottom		= WorldToLP(pExposure->GetSheetTrimBox (BOTTOM, pObject, pObjectTemplate));

			if(((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionOutput)
			{
				CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, exposureRect, pageRect, (void*)pExposure, DISPLAY_ITEM_REGISTRY(CPrintSheetView, Exposure), (void*)pActPrintSheet, CDisplayItem::ForceRegister); 
				if (pDI)
				{
					pDI->SetHighlightColor(LIGHTRED);
					if (pDI->m_nState != CDisplayItem::Normal)
					{
						FillTransparent(pDC, exposureRect, LIGHTRED, TRUE, 150);
					}
				}
			}
			pDC->LPtoDP(exposureRect);	// CalcBleedValueRect() needs device coordinates
			pDC->LPtoDP(pageRect);		
			CString bleedValueString;
			CRect	bleedValueRect;
			if (pExposure->IsBleedLocked(TOP))
			{
				bleedValueRect = CalcBleedValueRect(pExposure, pageRect, TOP,    bleedValueString, &m_DisplayList);
				pDC->DPtoLP(bleedValueRect);	
				DrawMaskArrow(pDC, exposureRect, pageRect, TOP);
				pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
			}
			if (pExposure->IsBleedLocked(RIGHT))
			{
				bleedValueRect = CalcBleedValueRect(pExposure, pageRect, RIGHT,  bleedValueString, &m_DisplayList);
				pDC->DPtoLP(bleedValueRect);	
				DrawMaskArrow(pDC, exposureRect, pageRect, RIGHT);
				pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
			}
			if (pExposure->IsBleedLocked(BOTTOM))
			{
				bleedValueRect = CalcBleedValueRect(pExposure, pageRect, BOTTOM, bleedValueString, &m_DisplayList);
				pDC->DPtoLP(bleedValueRect);	
				DrawMaskArrow(pDC, exposureRect, pageRect, BOTTOM);
				pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
			}
			if (pExposure->IsBleedLocked(LEFT))
			{
				bleedValueRect = CalcBleedValueRect(pExposure, pageRect, LEFT,   bleedValueString, &m_DisplayList);
				pDC->DPtoLP(bleedValueRect);	
				DrawMaskArrow(pDC, exposureRect, pageRect, LEFT);
				pDC->DrawText(bleedValueString, bleedValueRect, DT_NOCLIP|DT_SINGLELINE|DT_VCENTER|DT_CENTER);
			}
		}
	}
	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldTextColor);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetBkMode(nOldBkMode);

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	exposurePen.DeleteObject();
	pagePen.DeleteObject();
	defaultPageBrush.DeleteObject();
	pageBrush.DeleteObject();
	font.DeleteObject();
}

void CPrintSheetView::DrawColorants(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	if ( ! ( ShowColorants()))// && ShowBitmap()) )
		return;

	CPlate*	pPlate = pActPrintSheet->GetTopmostPlate(nActLayoutSide);
	if ( ! pPlate)
		return;

	CRect	 exposureRect, pageRect, upperBleedRect, lowerBleedRect, leftBleedRect, rightBleedRect;
	CPen	 exposurePen(PS_SOLID, 0, BLACK);
	CBrush*	 pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
	CPen*    pOldPen   = pDC->SelectObject(&exposurePen);

	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActPrintSheet->GetLayout()->m_FrontSide : 
															   &pActPrintSheet->GetLayout()->m_BackSide;

	CColorantList colorantList = GetActColorants();

	int	 nLayoutObjectIndex = 0;
	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		BOOL bIsDefault = (nActLayoutSide == FRONTSIDE) ? ( (pActPrintSheet->m_FrontSidePlates.GetCount()) ? FALSE : TRUE ) : ( (pActPrintSheet->m_BackSidePlates.GetCount()) ? FALSE : TRUE );
		CLayoutObject* pObject = &pLayoutSide->m_ObjectList.GetNext(pos);
		if ( ! pObject)
			continue;
		CExposure* pExposure = pPlate->m_ExposureSequence.GetExposure(nLayoutObjectIndex);
		if ( ! pExposure)
		{
			pExposure  = pPlate->GetDefaultExposure(nLayoutObjectIndex);
			bIsDefault = TRUE;
		}
		nLayoutObjectIndex++;
		if ( ! pExposure)
			continue;

		COLORREF	   crExposure	   = (bIsDefault) ? MIDDLEGRAY : MARINE;
		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPlate->GetPrintSheet());
		if (pObjectTemplate)
		{
			exposureRect.left	= WorldToLP(pExposure->GetSheetBleedBox(LEFT,   pObject, pObjectTemplate));
			exposureRect.top	= WorldToLP(pExposure->GetSheetBleedBox(TOP,    pObject, pObjectTemplate));
			exposureRect.right	= WorldToLP(pExposure->GetSheetBleedBox(RIGHT,  pObject, pObjectTemplate));
			exposureRect.bottom	= WorldToLP(pExposure->GetSheetBleedBox(BOTTOM, pObject, pObjectTemplate));
			
			exposureRect.NormalizeRect();
			if (pDC->RectVisible(exposureRect))
			{
				BOOL bFirst = TRUE;
				CBrush* pOldBrush = pDC->GetCurrentBrush();
				for (int i = 0; i < colorantList.GetSize(); i++)
				{
					CColorant rColorant = colorantList[i];
					if (pObjectTemplate->HasColorant(rColorant.m_strName))
					{
						if (bFirst)
						{
							CBrush brush(2, rColorant.m_crRGB);
							pDC->SelectObject(&brush);
							DrawRectangleExt(pDC, exposureRect);
							brush.DeleteObject();
							bFirst = FALSE;
						}
						FillTransparent(pDC, exposureRect, rColorant.m_crRGB, TRUE);
					}
				}
				pDC->SelectObject(pOldBrush);
			}
		}
	}

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	exposurePen.DeleteObject();
}

CRect CPrintSheetView::CalcBleedValueRect(CExposure* pExposure, const CRect& rRect, const CPoint& clickPoint, int& nSide, CString& valueString)
{
	CString dummyString;
	CRect leftRect	 = CalcBleedValueRect(pExposure, rRect, LEFT,	valueString, &m_DisplayList);
	if (leftRect.PtInRect(clickPoint))
	{
		nSide = LEFT;
		return leftRect;
	}

	CRect rightRect	 = CalcBleedValueRect(pExposure, rRect, RIGHT,	valueString, &m_DisplayList);
	if (rightRect.PtInRect(clickPoint))
	{
		nSide = RIGHT;
		return rightRect;
	}

	CRect topRect	 = CalcBleedValueRect(pExposure, rRect, TOP,	valueString, &m_DisplayList);
	if (topRect.PtInRect(clickPoint))
	{
		nSide = TOP;
		return topRect;
	}

	CRect bottomRect = CalcBleedValueRect(pExposure, rRect, BOTTOM, valueString, &m_DisplayList);
	if (bottomRect.PtInRect(clickPoint))
	{
		nSide = BOTTOM;
		return bottomRect;
	}

	CDisplayItem di;
	di.m_BoundingRect = rRect;
	nSide = di.HitTestInside(clickPoint);
	return CalcBleedValueRect(pExposure, rRect, nSide, valueString, &m_DisplayList);
}

CRect CPrintSheetView::CalcBleedValueRect(CExposure* pExposure, const CRect& rRect, int nSide, CString& valueString, CDisplayList* pDisplayList, CDC* pDC)
{
	float fValue = pExposure->GetBleedValue(nSide);
	return CPrintSheetView::CalcBleedValueRect(fValue, rRect, nSide, valueString, pDisplayList);
}

CRect CPrintSheetView::CalcBleedValueRect(float fValue, const CRect& rRect, int nSide, CString& valueString, CDisplayList* pDisplayList, CDC* pDC)
{
    TruncateNumber(valueString, fValue);

	float fViewportRatioX = (pDisplayList) ? pDisplayList->m_fViewportRatioX : 1.0f;
	if (pDC)
	{
		CSize sizeWin   = pDC->GetWindowExt();
		CSize sizeView  = pDC->GetViewportExt();
		fViewportRatioX = sizeWin.cx / sizeView.cx;
	}

	int	  nOff = 0;
	if (fValue < 0.0f)
		nOff = (int)(-WorldToLP(fValue) / fViewportRatioX);

	CWnd*  pWnd		  = (pDisplayList) ? pDisplayList->m_pParent : NULL;
	CClientDC dc(pWnd);
	CSize size(13, 13);
	dc.DPtoLP(&size);
	CFont font;
	font.CreateFont(size.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
    CFont* pOldFont	  = dc.SelectObject(&font);
    CSize  textExtent = dc.GetTextExtent(valueString);	
    dc.SelectObject(pOldFont);
	font.DeleteObject();

    CPoint	  pt;
    int		  nDX = textExtent.cx/2 + 2, nDY = textExtent.cy/2 + 2;
    switch (nSide)
    {
    case LEFT:		pt.x = rRect.left  + nDX + 2 + nOff;	  pt.y = rRect.top	  + rRect.Height()/2 + 6;	break;
    case RIGHT:		pt.x = rRect.right - nDX - 4 - nOff;	  pt.y = rRect.top	  + rRect.Height()/2 - 6;	break;
    case TOP:		pt.x = rRect.left  + rRect.Width()/2 - 6; pt.y = rRect.top	  + nDY + 2 + nOff;			break;
    case BOTTOM:	pt.x = rRect.left  + rRect.Width()/2 + 6; pt.y = rRect.bottom - nDY - 2 - nOff;			break;
    default:		pt.x = rRect.left  + rRect.Width()/2 - 6; pt.y = rRect.top	  + nDY - 4;				break;
    }

    CRect valueRect(pt, pt);
    valueRect.InflateRect(nDX, nDY);

    return valueRect;
}

void CPrintSheetView::DrawMaskArrow(CDC* pDC, const CRect& exposureRect, const CRect& pageRect, int nDirection)
{
	const int nEdgeLen = 4;
	int		  nLen	   = 0;
	CPoint	  hotSpot;
	switch (nDirection)
	{
	case LEFT:	hotSpot    = CPoint(exposureRect.left,  pageRect.CenterPoint().y + 6); 
				nLen	   = pageRect.left - exposureRect.left; 
				nDirection = (nLen < 0) ? RIGHT : nDirection;
				nLen       = max(nEdgeLen, abs(nLen));
				break;
	case RIGHT:	hotSpot    = CPoint(exposureRect.right, pageRect.CenterPoint().y - 6); 
				nLen	   = exposureRect.right - pageRect.right; 
				nDirection = (nLen < 0) ? LEFT : nDirection;
				nLen       = max(nEdgeLen, abs(nLen));
				break;
	case TOP:	hotSpot	   = CPoint(pageRect.CenterPoint().x - 6, exposureRect.top);
				nLen	   = pageRect.top - exposureRect.top; 
				nDirection = (nLen < 0) ? BOTTOM : nDirection;
				nLen       = max(nEdgeLen, abs(nLen));
				break;
	case BOTTOM:hotSpot	   = CPoint(pageRect.CenterPoint().x + 6, exposureRect.bottom);
				nLen	   = exposureRect.bottom - pageRect.bottom; 
				nDirection = (nLen < 0) ? TOP : nDirection;
				nLen       = max(nEdgeLen, abs(nLen));
				break;
	}
	DrawArrow(pDC, nDirection, hotSpot, nLen, BLACK, YELLOW);
}

void CPrintSheetView::DrawArrow(CDC* pDC, int nDirection, CPoint hotSpot, int nLen, COLORREF crOutline, COLORREF crFill, BOOL bSlim, CPoint ptVPOrg, int nEdgeLen)
{
	if ( ! pDC->IsPrinting())
	{
		pDC->SaveDC();
		pDC->SetMapMode(MM_TEXT);
		pDC->SetViewportOrg(ptVPOrg);
		pDC->SetWindowOrg(0, 0);
	}
	else
	{
		pDC->DPtoLP(&hotSpot);
		nEdgeLen *= 5;
		CSize sizeLen(nLen, nLen);
		pDC->DPtoLP(&sizeLen);
		nLen = sizeLen.cx;
		if (nDirection == TOP)
			nDirection = BOTTOM;
		else
			if (nDirection == BOTTOM)
				nDirection = TOP;
	}

	CPen   pen(PS_SOLID, 1, crOutline);
	CBrush brush(crFill);
	CPen*   pOldPen   = pDC->SelectObject(&pen);
	CBrush* pOldBrush = pDC->SelectObject(&brush);

	int nEdgeLenX = Pixel2MM(pDC, nEdgeLen);
	int nEdgeLenY = Pixel2MM(pDC, nEdgeLen);
	int n1X		  = Pixel2MM(pDC, nEdgeLen);
	int n1Y		  = Pixel2MM(pDC, nEdgeLen);
	int n2X		  = 0;//Pixel2MM(pDC, 2);
	int n2Y		  = 0;//Pixel2MM(pDC, 2);
	int nLenX	  = nLen;
	int nLenY	  = nLen;
	CPoint p[8];
	switch (nDirection)
	{
	case LEFT:	hotSpot.x++;
				p[0] = hotSpot;							p[1] = hotSpot + CSize( nEdgeLenX,  nEdgeLenY); 
				p[2] = CPoint(p[1].x, p[1].y - n1Y);	p[3] = CPoint(hotSpot.x + nLenX, p[2].y);
				p[4] = CPoint(p[3].x, p[3].y - n2Y);	p[5] = CPoint(p[2].x, p[4].y);
				p[6] = CPoint(p[5].x, p[5].y - n1Y);	p[7] = CPoint(hotSpot.x, hotSpot.y); 
				break;
	case RIGHT:	hotSpot.x--;
				p[0] = hotSpot;							p[1] = hotSpot + CSize(-nEdgeLenX, -nEdgeLenY); 
				p[2] = CPoint(p[1].x, p[1].y + n1Y);	p[3] = CPoint(hotSpot.x - nLenX, p[2].y);
				p[4] = CPoint(p[3].x, p[3].y + n2Y);	p[5] = CPoint(p[2].x, p[4].y);
				p[6] = CPoint(p[5].x, p[5].y + n1Y);	p[7] = hotSpot; 
				break;
	case TOP:	hotSpot.y++;
				p[0] = hotSpot;							p[1] = hotSpot + CSize(-nEdgeLenX,  nEdgeLenY); 
				p[2] = CPoint(p[1].x + n1X, p[1].y);	p[3] = CPoint(p[2].x, hotSpot.y + nLenY);
				p[4] = CPoint(p[3].x + n2X, p[3].y);	p[5] = CPoint(p[4].x, p[2].y);
				p[6] = CPoint(p[5].x + n1X, p[5].y);	p[7] = hotSpot; 
				break;
	case BOTTOM:hotSpot.y--;
				p[0] = hotSpot;							p[1] = hotSpot + CSize( nEdgeLenX, -nEdgeLenY); 
				p[2] = CPoint(p[1].x - n1X, p[1].y);	p[3] = CPoint(p[2].x, hotSpot.y - nLenY);
				p[4] = CPoint(p[3].x - n2X, p[3].y);	p[5] = CPoint(p[4].x, p[2].y);
				p[6] = CPoint(p[5].x - n1X, p[5].y);	p[7] = hotSpot; 
				break;
	}

	pDC->BeginPath();
	pDC->Polyline(p, 8);
	pDC->EndPath();
	pDC->StrokeAndFillPath();

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();

	if ( ! pDC->IsPrinting())
		pDC->RestoreDC(-1);
}

//#include <d2d1.h>
//#include <d2d1helper.h>
//#include <dwrite.h>
//#include <wincodec.h>
//
//#pragma comment (lib, "d2d1.lib")
//
//extern ID2D1Factory* pDirect2dFactory;
//extern ID2D1DCRenderTarget* pRenderTarget;


void CPrintSheetView::FillTransparent(CDC* pDC, const CRect& rRect, COLORREF cr, BOOL bTrueTransparency, int nAlpha, BOOL bFrame) 
{
	if (bTrueTransparency && ! pDC->IsPrinting())
	{
		CRect rect = rRect;
		pDC->LPtoDP(rect);
		rect.NormalizeRect();

		int nDC = pDC->SaveDC();
		pDC->SetMapMode(MM_TEXT);
		pDC->SetViewportOrg(0, 0);
		pDC->SetWindowOrg(0, 0);

		CRect clipBox;
		pDC->GetClipBox(clipBox);	
		rect.IntersectRect(rect, clipBox);
		//rect.left   -= (rect.left%2)	? 1 : 0;
		//rect.top    -= (rect.top%2)		? 1 : 0;
		//rect.right  -= (rect.right%2)	? 1 : 0;
		//rect.bottom  -= (rect.bottom%2)  ? 1 : 0;

		if (1)	// xp and older
		{
			Graphics graphics(pDC->m_hDC);
			graphics.SetPageUnit(UnitPixel);
		
			Rect rc(rect.left, rect.top, rect.Width(), rect.Height());
			SolidBrush br(Color::Color(nAlpha, GetRValue(cr), GetGValue(cr), GetBValue(cr)));
			graphics.FillRectangle(&br, rc);

			if (bFrame)
			{
				SolidBrush br(Color::Color(min(255, nAlpha + 20), GetRValue(cr), GetGValue(cr), GetBValue(cr)));
				Pen pen(&br);
				//rc.Inflate(-1,-1);
				graphics.DrawRectangle(&pen, rc);
			}
		}
		//else
		//{
		//	//ID2D1Factory* pDirect2dFactory = NULL;
		//	//ID2D1DCRenderTarget* pRenderTarget = NULL;

		//	HDC hdc = pDC->GetSafeHdc();

		//	//D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &pDirect2dFactory);
		//	
		//	//D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(	D2D1_RENDER_TARGET_TYPE_DEFAULT, D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE),	0, 0, D2D1_RENDER_TARGET_USAGE_NONE, D2D1_FEATURE_LEVEL_DEFAULT);

		//	//pDirect2dFactory->CreateDCRenderTarget(&props, &pRenderTarget);

		//	CRect rcBox(0,0,1000,1000);
		//	pRenderTarget->BindDC(hdc, &rcBox);

		//	D2D1_COLOR_F cc;
		//	cc.a = nAlpha;//GetAValue(c)/255.0f;
		//	if (cc.a == 0)
		//		cc.a = 1.0f; 
		//	cc.r = GetRValue(cr)/255.0f;
		//	cc.g = GetGValue(cr)/255.0f;
		//	cc.b = GetBValue(cr)/255.0f;
		//	ID2D1SolidColorBrush* pBlackBrush = NULL;
		//	pRenderTarget->CreateSolidColorBrush(cc, &pBlackBrush); 

		//	pRenderTarget->BeginDraw();
		//	pRenderTarget->FillRectangle(D2D1::RectF(rect.left, rect.top, rect.right, rect.bottom), pBlackBrush);
		//	pRenderTarget->EndDraw();

		//	//pRenderTarget->Release();
		//	pBlackBrush->Release();
		//	//pDirect2dFactory->Release();
		//}

		pDC->RestoreDC(nDC);
	}
	else
	{
		if (bTrueTransparency)
			cr = CGraphicComponent::IncreaseColorBrightness(cr, 1.6f);

		CRect rcFrame = rRect;
		CSize size(1, 1);
		pDC->DPtoLP(&size);
		rcFrame.NormalizeRect();
		rcFrame.right += size.cx;
		rcFrame.bottom+= size.cy;

		pDC->FillSolidRect(rcFrame, cr);

		if (bFrame)
		{
			CPen pen(PS_SOLID, 1, CGraphicComponent::IncreaseColorBrightness(cr, 0.98f));//RGB(colorR, colorG, colorB)); 
			CPen* pOldPen = pDC->SelectObject(&pen); 
			CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH); 
			pDC->Rectangle(rRect); 
			pDC->SelectObject(pOldPen); 
			pDC->SelectObject(pOldBrush);
		}
	}
}

void CPrintSheetView::DrawMedia(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	if ( ! GetDlgOutputMedia())
		return;
	if ( ! GetDlgOutputMedia()->m_hWnd)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CMediaSize*	pMediaSize;
	//CPrintSheetTableView* pView	= (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	//if ( ! pView)
	//	return;

	if (GetDlgOutputMedia()->m_hWnd)
		pMediaSize = GetDlgOutputMedia()->GetCurMedia(pActPrintSheet);
	else
	{
		if ( ! GetDlgOutputMedia()->PrintSheetIsSelected(pActPrintSheet, nActLayoutSide))
			return;
		CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(pActPrintSheet->m_strPDFTarget);
		if ( ! pTargetProps)
			return;
		pMediaSize = &pTargetProps->m_mediaList[0];
	}

	if ( !pMediaSize)
		return;

	if (GetDlgOutputMedia()->GetDlgPDFTargetProps())	// if props dialog open, show current tile only
	{
		//BOOL bShowMeasuring = ( ! ShowBitmap() && ! ShowExposures() && 
		//						  ViewMode() != ID_VIEW_ALL_SHEETS) ? TRUE : FALSE;
		BOOL bShowMeasuring = (ViewMode() != ID_VIEW_ALL_SHEETS) ? TRUE : FALSE;
		pMediaSize->DrawTile(pDC, pActPrintSheet, nActLayoutSide, GetDlgOutputMedia()->GetCurTileIndex(), bShowMeasuring, TRUE);
	}
	else
		for (int i = 0; i < pMediaSize->m_posInfos.GetSize(); i++)	// if props dialog not open, show all tiles
			pMediaSize->DrawTile(pDC, pActPrintSheet, nActLayoutSide, i, FALSE, FALSE);
}

void CPrintSheetView::DrawGripper(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	if ( ! ShowGripper())
		return;

	CLayout* pActLayout = (pActPrintSheet) ? pActPrintSheet->GetLayout() : NULL;
	if ( ! pActLayout)
		return;
	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActLayout->m_FrontSide : &pActLayout->m_BackSide;
	if ( ! pLayoutSide)
		return;

	pLayoutSide->DrawGripper(pDC, pActPrintSheet);
}

void CPrintSheetView::DrawCutBlocks(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	if ( ! ShowCutBlocks())
		return;

	CLayout* pActLayout = (pActPrintSheet) ? pActPrintSheet->GetLayout() : NULL;
	if ( ! pActLayout)
		return;
	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActLayout->m_FrontSide : &pActLayout->m_BackSide;

	CSize size(13, 13);
	pDC->DPtoLP(&size);

	CFont font;
	font.CreateFont((int)(size.cy * m_fScreenPrinterRatioY), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	CFont*	pOldFont = pDC->SelectObject(&font);
	
	size = CSize(1,1);
	pDC->DPtoLP(&size);

	CPen	pen(PS_SOLID, size.cx, RED);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

	CString	 string;
	CRect	 rcCutBlock;
	POSITION pos	   = pLayoutSide->m_cutBlocks.GetHeadPosition();
	int		 nBlockNum = 1;
	while (pos)
	{
		CLayoutObject& rCutBlockObject = pLayoutSide->m_cutBlocks.GetNext(pos);
		float fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight; 
		rCutBlockObject.GetCurrentGrossCutBlock(pActPrintSheet, fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight);

		rcCutBlock.left		= WorldToLP(pLayoutSide->m_fPaperLeft   + fCropBoxX);
		rcCutBlock.bottom	= WorldToLP(pLayoutSide->m_fPaperBottom + fCropBoxY);
		rcCutBlock.right	= WorldToLP(pLayoutSide->m_fPaperLeft   + fCropBoxX + fCropBoxWidth);
		rcCutBlock.top		= WorldToLP(pLayoutSide->m_fPaperBottom + fCropBoxY + fCropBoxHeight);

		pDC->Rectangle(rcCutBlock);

		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(WHITE);
		pDC->SetTextColor(RED);
		string.Format(_T("B %d"), nBlockNum);
		pDC->DrawText(string, rcCutBlock, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		nBlockNum++;
	}

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldFont);
	pen.DeleteObject();
	font.DeleteObject();
}

CSize CPrintSheetView::GetDrawingOffset(UINT nActLayoutSide, CLayout* pActLayout)	//Offset between Plate 0,0 and Sheet 0,0
{
	CPressDevice* pActPressDevice;

	CSize DrawingOffset = CSize(0,0);

	return DrawingOffset;

	if (ShowPlate())
		return DrawingOffset;

#ifdef CIPCUT
	CLayoutObject *pObject;
	POSITION pos = pActLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		pObject = &(pActLayout->m_FrontSide.m_ObjectList.GetNext(pos));	// For the case when the objects
		DrawingOffset.cy = min(DrawingOffset.cy, WorldToLP(pObject->GetBottom()));	// move outside the paper
		DrawingOffset.cx = min(DrawingOffset.cx, WorldToLP(pObject->GetLeft()));	// after cutting or turning
	}
	return DrawingOffset;
#endif

	switch(nActLayoutSide) //Find the right PressDevice
	{
		case BOTHSIDES :
		case FRONTSIDE : pActPressDevice = pActLayout->m_FrontSide.GetPressDevice();
						 break;
		case BACKSIDE  : pActPressDevice = pActLayout->m_BackSide.GetPressDevice();
						 break;
	}
	// Get the offsets for the actual PressDevice
	if (pActPressDevice->m_nPressType == CPressDevice::SheetPress)
	{
		switch (nActLayoutSide)
		{
			case BOTHSIDES :
			case FRONTSIDE : DrawingOffset.cx = (LONG)((pActPressDevice->m_fPlateWidth - pActLayout->m_FrontSide.m_fPaperWidth) / 2); 
							 DrawingOffset.cy = (LONG) (pActPressDevice->m_fPlateEdgeToPrintEdge - pActPressDevice->m_fGripper);
							 break;
			case BACKSIDE :	 DrawingOffset.cx = (LONG)((pActPressDevice->m_fPlateWidth - pActLayout->m_BackSide.m_fPaperWidth) / 2); 
							 DrawingOffset.cy = (LONG) (pActPressDevice->m_fPlateEdgeToPrintEdge - pActPressDevice->m_fGripper);
							 break;
		}
	}
	else //CPressDevice::WebPress
	{
		switch (nActLayoutSide)
		{
			case BOTHSIDES :
			case FRONTSIDE : //DrawingOffset.cx = (LONG)(pActPressDevice->m_fXRefLine - (pActLayout->m_FrontSide.m_fPaperWidth / 2)); 
							 //DrawingOffset.cy = (LONG)(pActPressDevice->m_fCenterOfSection - (pActLayout->m_FrontSide.m_fPaperHeight / 2));
							 //DrawingOffset.cx = (LONG)((pActPressDevice->m_fPlateWidth - pActLayout->m_FrontSide.m_fPaperWidth) / 2); 
							 //DrawingOffset.cy = (LONG)(pActPressDevice->m_fPlateHeight - (2 * pActPressDevice->m_fCenterOfSection));
							 DrawingOffset.cx = (LONG)(pActLayout->m_FrontSide.m_fPaperLeft); 
							 DrawingOffset.cy = (LONG)(pActLayout->m_FrontSide.m_fPaperBottom);
							 break;
			case BACKSIDE :	 //DrawingOffset.cx = (LONG)(pActPressDevice->m_fXRefLine - (pActLayout->m_BackSide.m_fPaperWidth / 2)); 
							 //DrawingOffset.cy = (LONG)(pActPressDevice->m_fCenterOfSection - (pActLayout->m_BackSide.m_fPaperHeight / 2));
							 //DrawingOffset.cx = (LONG)((pActPressDevice->m_fPlateWidth - pActLayout->m_BackSide.m_fPaperWidth) / 2); 
							 //DrawingOffset.cy = (LONG)(pActPressDevice->m_fPlateHeight - (2 * pActPressDevice->m_fCenterOfSection));
							 DrawingOffset.cx = (LONG)(pActLayout->m_BackSide.m_fPaperLeft); 
							 DrawingOffset.cy = (LONG)(pActLayout->m_BackSide.m_fPaperBottom);
							 break;
		}
	}

	DrawingOffset.cx = WorldToLP((float)DrawingOffset.cx);
	DrawingOffset.cy = WorldToLP((float)DrawingOffset.cy);

	return DrawingOffset;
}

void CPrintSheetView::DrawSheet(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, BOOL bDrawTitle) 
{
	if (ShowExposures())// || ShowColorants())	// in exposures or colorants view draw sheet frame only
		return;

	CLayout*	 pActLayout  = pActPrintSheet->GetLayout();
	CLayoutSide* pLayoutSide = NULL;

	CRect sheetRect;
	switch (nActLayoutSide)
	{
	case FRONTSIDE : sheetRect.left   = WorldToLP(pActLayout->m_FrontSide.m_fPaperLeft  );
					 sheetRect.top	  = WorldToLP(pActLayout->m_FrontSide.m_fPaperTop   );
					 sheetRect.right  = WorldToLP(pActLayout->m_FrontSide.m_fPaperRight );
					 sheetRect.bottom = WorldToLP(pActLayout->m_FrontSide.m_fPaperBottom);
					 pLayoutSide = &pActPrintSheet->GetLayout()->m_FrontSide;
					 break;
	case BACKSIDE :	 sheetRect.left   = WorldToLP(pActLayout->m_BackSide.m_fPaperLeft  );
					 sheetRect.top	  = WorldToLP(pActLayout->m_BackSide.m_fPaperTop   );
					 sheetRect.right  = WorldToLP(pActLayout->m_BackSide.m_fPaperRight );
					 sheetRect.bottom = WorldToLP(pActLayout->m_BackSide.m_fPaperBottom);
					 pLayoutSide = &pActPrintSheet->GetLayout()->m_BackSide;
					 break;
	}

	if (!pActLayout || !pLayoutSide)
		return;

	if (pDC->RectVisible(sheetRect))
	{
		CBrush paperBrush;
		CPen   framePen;
		paperBrush.CreateSolidBrush(WHITE);
		framePen.CreatePen (PS_SOLID, Pixel2ConstMM(pDC, 1), (ShowPlate()) ? RGB(210,210,210) : MIDDLEGRAY);
		CBrush* pOldBrush = pDC->GetCurrentBrush();
		pDC->SelectObject(&paperBrush);	
		CPen* pOldPen = pDC->SelectObject(&framePen);	
		CRect rcSheetFrame = sheetRect;	rcSheetFrame.NormalizeRect();
		CSize sizeInflate(1,1);
		pDC->DPtoLP(&sizeInflate);
		rcSheetFrame.InflateRect(sizeInflate);
		DrawRectangleExt(pDC, &rcSheetFrame);
		if (IsOpenDlgComponentImposer())
		{
			if (GetDlgComponentImposer()->m_bSheetGrainOn)
			{
				int nHatchStyle = (pLayoutSide->m_nPaperGrain == GRAIN_VERTICAL) ? HS_VERTICAL : HS_HORIZONTAL;
				CBrush  brush(nHatchStyle, RGB(230,230,230,));
				CBrush* pOldBrush  = pDC->SelectObject(&brush);
				CPen*	pOldPen	   = (CPen*)pDC->SelectStockObject(NULL_PEN);
				int		nOldBkMode = pDC->SetBkMode(TRANSPARENT);
				DrawRectangleExt(pDC, rcSheetFrame);
				pDC->SelectObject(pOldBrush);
				pDC->SelectObject(pOldPen);
				pDC->SetBkMode(nOldBkMode);
				brush.DeleteObject();
			}
		}

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);		
		paperBrush.DeleteObject();
	}

	CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, sheetRect, sheetRect, (void*)pLayoutSide, SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, Sheet), (void*)pActPrintSheet, CDisplayItem::ForceRegister | CDisplayItem::NotClickable); 
	if (pDI)
		pDI->SetHighlightColor(RED);

	if (! ShowPlate())
	{
		UpdateLeftStatusBar (pDC, sheetRect, pActPrintSheet, pLayoutSide);
		UpdateUpperStatusBar(pDC, sheetRect, pActPrintSheet, pLayoutSide, bDrawTitle);
	}
}

void CPrintSheetView::DrawSheetFrame(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, BOOL bDrawTitle) 
{
	//if ( ! ShowColorants())	// if in colorants view frame will be drawn
		if ( ! ShowExposures())	// if not in exposures view frame will not be drawn
			return;

	CRect		 rect;
	CLayout*	 pActLayout  = pActPrintSheet->GetLayout();
	CLayoutSide* pLayoutSide = NULL;

	switch (nActLayoutSide)
	{
	case FRONTSIDE : rect.left	 = WorldToLP(pActLayout->m_FrontSide.m_fPaperLeft  );
					 rect.top	 = WorldToLP(pActLayout->m_FrontSide.m_fPaperTop   );
					 rect.right	 = WorldToLP(pActLayout->m_FrontSide.m_fPaperRight );
					 rect.bottom = WorldToLP(pActLayout->m_FrontSide.m_fPaperBottom);
					 pLayoutSide = &pActPrintSheet->GetLayout()->m_FrontSide;
					 break;
	case BACKSIDE :	 rect.left	 = WorldToLP(pActLayout->m_BackSide.m_fPaperLeft  );
					 rect.top	 = WorldToLP(pActLayout->m_BackSide.m_fPaperTop   );
					 rect.right	 = WorldToLP(pActLayout->m_BackSide.m_fPaperRight );
					 rect.bottom = WorldToLP(pActLayout->m_BackSide.m_fPaperBottom);
					 pLayoutSide = &pActPrintSheet->GetLayout()->m_BackSide;
					 break;
	}

	if (!pActLayout || !pLayoutSide)
		return;

	if (pDC->RectVisible(rect))
	{
		CPen pen;
		pen.CreatePen(PS_SOLID, Pixel2ConstMM(pDC, 1), RGB(210,210,210));
		CPen*   pOldPen	  = pDC->GetCurrentPen();
		CBrush* pOldBrush = pDC->GetCurrentBrush();

		pDC->SelectStockObject(HOLLOW_BRUSH);	
		pDC->SelectObject(&pen);		
		pDC->Rectangle(&rect);

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);		
		pen.DeleteObject();
	}

	if ( ! ShowPlate())
	{
		UpdateLeftStatusBar	(pDC, rect, pActPrintSheet, pLayoutSide);
		UpdateUpperStatusBar(pDC, rect, pActPrintSheet, pLayoutSide, bDrawTitle);
	}
}

void CPrintSheetView::DrawRefEdge(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, CLayout* pActLayout) 
{
	if ( ShowExposures() || ShowBitmap() || MarksViewActive() ) 
		return;

	CRect rect;

	switch (nActLayoutSide)
	{
	case FRONTSIDE : rect.left	 = WorldToLP(pActLayout->m_FrontSide.m_fPaperLeft  );
					 rect.top	 = WorldToLP(pActLayout->m_FrontSide.m_fPaperTop   );
					 rect.right	 = WorldToLP(pActLayout->m_FrontSide.m_fPaperRight );
					 rect.bottom = WorldToLP(pActLayout->m_FrontSide.m_fPaperBottom);
					 break;
	case BACKSIDE :	 rect.left	 = WorldToLP(pActLayout->m_BackSide.m_fPaperLeft   );
					 rect.top	 = WorldToLP(pActLayout->m_BackSide.m_fPaperTop    );
					 rect.right	 = WorldToLP(pActLayout->m_BackSide.m_fPaperRight  );
					 rect.bottom = WorldToLP(pActLayout->m_BackSide.m_fPaperBottom );
					 break;
	}

	CPen refEdgePenPaper;
	CPen* pOldPen;

	pOldPen = pDC->GetCurrentPen();

	CSize szEdge(3,3);
	pDC->DPtoLP(&szEdge);
	if (m_nPrinterNumColors > 2)
		refEdgePenPaper.CreatePen(PS_SOLID, szEdge.cx, OCKER);
	else
		refEdgePenPaper.CreatePen(PS_SOLID, szEdge.cx, BLACK);

	pDC->SelectObject(&refEdgePenPaper);
	switch (nActLayoutSide)
	{
		case FRONTSIDE : DrawEdge(pDC, &rect, pActLayout->m_FrontSide.m_nPaperRefEdge, pActLayout, pActPrintSheet, 0, PRINTSHEET, FRONTSIDE, &m_DisplayList, ShowFoldSheet());
						 break;
		case BACKSIDE  : DrawEdge(pDC, &rect, pActLayout->m_BackSide.m_nPaperRefEdge, pActLayout, pActPrintSheet, 0, PRINTSHEET, BACKSIDE, &m_DisplayList, ShowFoldSheet());
						 break;
		default : break; //TODO Errormessage
	}
	pDC->SelectObject(pOldPen);
	refEdgePenPaper.DeleteObject();
}

void CPrintSheetView::DrawBoundingBoxFrame(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	CLayout* pActLayout = pActPrintSheet->GetLayout();
	if ( ! pActLayout)
		return;

	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActLayout->m_FrontSide : &pActPrintSheet->GetLayout()->m_BackSide;
	float fBBoxLeft, fBBoxBottom, fBBoxRight, fBBoxTop; 
	pLayoutSide->CalcBoundingBoxAll(&fBBoxLeft, &fBBoxBottom, &fBBoxRight, &fBBoxTop, TRUE);

	CRect rect;
	rect.left	= WorldToLP(fBBoxLeft);
	rect.top	= WorldToLP(fBBoxTop);
	rect.right	= WorldToLP(fBBoxRight);
	rect.bottom = WorldToLP(fBBoxBottom);

	if (pDC->RectVisible(rect))
	{
		CPen   whitePen; 
		whitePen.CreatePen(PS_SOLID, Pixel2ConstMM(pDC, 2), RGB(0, 200, 0));
		CPen*   pOldPen	  = pDC->GetCurrentPen();
		CBrush* pOldBrush = pDC->GetCurrentBrush();

		pDC->SelectStockObject(HOLLOW_BRUSH);	
		pDC->SelectObject(&whitePen);		
		pDC->SetBkColor(WHITE);
		DrawRectangleExt(pDC, &rect);

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);		
		whitePen.DeleteObject();

		m_DisplayList.RegisterItem(pDC, rect, rect, (void*)pLayoutSide, SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, BoundingBoxFrame), (void*)pActPrintSheet, CDisplayItem::ForceRegister | CDisplayItem::NotClickable); 
	}
}

void CPrintSheetView::DrawObjects(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, BYTE nType, BOOL bDrawSelectedOnly, CString strMarkName)
{
	CRect			  objectRect, objectOutRect;
	CLayoutSide*	  pLayoutSide;
	if (nActLayoutSide == FRONTSIDE)
		pLayoutSide = &pActPrintSheet->GetLayout()->m_FrontSide;
	else
		pLayoutSide = &pActPrintSheet->GetLayout()->m_BackSide;

	BOOL bUnplausibleObjectFound = FALSE;

	pLayoutSide->ResetPublicFlags();

	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);

		if (rObject.m_nType != nType) 
			continue;									   
		if (rObject.m_bHidden) 
			continue;									   
		if (rObject.IsAutoMarkDisabled(pActPrintSheet, nActLayoutSide))
			continue;

		if (!rObject.IsPlausible())						   
		{
			bUnplausibleObjectFound = TRUE;
			continue;
		}
		if (nType == CLayoutObject::ControlMark)
			if ( ! strMarkName.IsEmpty())
			{
				if (rObject.m_strFormatName != strMarkName)
					continue;
			}
			else
				if (pActPrintSheet->GetLayout()->m_markList.FindMark(rObject.m_strFormatName))	// for compatibilty to KIM5 marks: if mark is not in mark list it is a KIM5 mark -> so draw it
					continue;

		CPageTemplate* pObjectTemplate = (rObject.m_nType == CLayoutObject::ControlMark) ?
										  rObject.GetCurrentMarkTemplate() :
										  rObject.GetCurrentPageTemplate(pActPrintSheet);

		CPlate*  pPlate	= pActPrintSheet->GetTopmostPlate(nActLayoutSide);
		CPageSource*   pObjectSource   = (pObjectTemplate) ? pObjectTemplate->GetObjectSource((pPlate) ? pPlate->m_nColorDefinitionIndex : -1, TRUE, pActPrintSheet, nActLayoutSide) : NULL;

		objectRect.left		 = WorldToLP(rObject.GetLeft());
		objectRect.top		 = WorldToLP(rObject.GetTop());
		objectRect.right	 = WorldToLP(rObject.GetRight());
		objectRect.bottom	 = WorldToLP(rObject.GetBottom());
		objectOutRect.left	 = WorldToLP(rObject.GetLeftWithBorder());
		objectOutRect.top	 = WorldToLP(rObject.GetTopWithBorder());
		objectOutRect.right	 = WorldToLP(rObject.GetRightWithBorder());
		objectOutRect.bottom = WorldToLP(rObject.GetBottomWithBorder());

		COLORREF crBackground	   = RGB(242,242,255);
		BOOL	 bMarkPreviewDrawn = FALSE;
		BOOL	 bDrawn			   = (bDrawSelectedOnly) ? FALSE : TRUE;
		if (rObject.m_nType == CLayoutObject::Page) 
		{
			CRect dragRect = objectRect;
			if ( ! bDrawSelectedOnly) 
			{
				if (rObject.IsPanoramaPage(*pActPrintSheet) && pObjectTemplate)
					DrawPanoramaPage(pDC, objectRect, rObject, *rObject.GetCurrentMultipagePartner(pObjectTemplate, TRUE), *pActPrintSheet);
				else
				{
					if ( ! ShowExposures() && ! ShowBitmap() && ! ShowColorants() && ! (rObject.m_nStatus & CLayoutObject::Disabled) )	// show borders to indicate asymetric trimmings
					{
						CPen grayPen(PS_SOLID, Pixel2ConstMM(pDC, 1), RGB(240,240,240));	
						CPen bluePen(PS_SOLID, Pixel2ConstMM(pDC, 1), RGB(220,220,255));	
						CPen*	pOldPen		 = pDC->SelectObject(&grayPen);
						CBrush* pOldBrush	 = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
						if (pObjectTemplate)
							//if ( ! pObjectTemplate->IsFlatProduct())
							{
								CRect rcGrossFrame = rObject.CalcGrossFrame(pActPrintSheet);
								CRect rcOverFoldFrame = rObject.CalcOverFoldFrame(pActPrintSheet);
								CRect rcOutFrame, rcObject = objectRect; rcObject.NormalizeRect();
								rcOutFrame.UnionRect(rcGrossFrame, rcOverFoldFrame); 
								int nTemp = rcOutFrame.top; rcOutFrame.top = rcOutFrame.bottom; rcOutFrame.bottom = nTemp;
								if ( ((rcOutFrame.Width() == 0) && (abs(rcOutFrame.top - rcOutFrame.bottom) == 0)) || (rcOutFrame == rcObject))
								{
									//if ( ! rObject.IsFlatProduct() && ! rObject.HasLabelNeighbour())
									//	DrawRectangleExt(pDC, objectOutRect);
								}
								else
								{
									pDC->SelectObject(&bluePen);
									DrawRectangleExt(pDC, rcOutFrame);
									dragRect	  = rcOutFrame;
								}
							}

						grayPen.DeleteObject();
						bluePen.DeleteObject();
						pDC->SelectObject(pOldPen);
						pDC->SelectObject(pOldBrush);
					}
				}

				DrawPage(pDC, objectRect, rObject, pObjectTemplate, pActPrintSheet, bDrawSelectedOnly, &crBackground);
				bDrawn = TRUE;
			}

			if (MarksViewActive())
				dragRect = objectRect;

			if (MarksViewActive() && ! GetDlgLayoutObjectGeometry())
				;
			else
				if (bDrawn && ! ShowExposures() && ! (rObject.m_nStatus & CLayoutObject::Disabled))
				{
					int nItemType = CDisplayItem::ForceRegister;
					if ( ! rObject.IsFlatProduct())
					{
						CFoldSheetLayer* pLayer = pActPrintSheet->GetFoldSheetLayer(rObject.m_nComponentRefIndex);
						if (pLayer)
							nItemType = (pLayer->m_bPagesLocked) ? CDisplayItem::ForceRegister | CDisplayItem::NotClickable : nItemType;
					}
					CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, dragRect, objectRect, (void*)&rObject, SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, LayoutObject), (void*)pActPrintSheet, nItemType | CDisplayItem::GroupMember, NULL, objectOutRect); 
					if (pDI)
					{
						pDI->SetHighlightColor(RGB(190,112,85));
						if (MarksViewActive())
							pDI->SetNoStateFeedback();
						if (IsOpenDlgAlignObjects())
						{
							if (pDI->m_nState == CDisplayItem::Normal)
								CReflinePositionParams::DrawCenterHandles(pDC, objectRect, LIGHTBLUE);
						}
						if (pDI->m_nState != CDisplayItem::Normal)
						{
							FillTransparent(pDC, dragRect, RGB(230,218,205), TRUE, 80);
							int nHatchStyle = HS_FDIAGONAL;
							CBrush  brush(nHatchStyle, BROWN);//RGB(220,190,65));
							CBrush* pOldBrush  = pDC->SelectObject(&brush);
							CPen*	pOldPen	   = (CPen*)pDC->SelectStockObject(NULL_PEN);
							int		nOldBkMode = pDC->SetBkMode(TRANSPARENT);
							DrawRectangleExt(pDC, dragRect);
							pDC->SelectObject(pOldBrush);
							pDC->SelectObject(pOldPen);
							pDC->SetBkMode(nOldBkMode);
							brush.DeleteObject();
						}
					}
				}
		}
		else
		{
			if ( ! ShowBitmap() && ! ShowExposures() && ! (rObject.m_nStatus & CLayoutObject::Disabled))
			{
				CDisplayItem* pDI = NULL;
				if (MarksViewActive())
				{
					CRect rcDI = objectRect; CSize sizeInflate(4,4); pDC->DPtoLP(&sizeInflate); rcDI.NormalizeRect(); rcDI.InflateRect(sizeInflate);
					pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI, (void*)&rObject, SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, LayoutObject), (void*)pActPrintSheet, CDisplayItem::ForceRegister | CDisplayItem::NoFeedback, NULL, rcDI);
					if (pDI)
						pDI->SetNoStateFeedback();
				}
				BOOL bSelected = (IsOpenDlgNewMark()) ? FALSE : ( (pDI) ? ( (pDI->m_nState == CDisplayItem::Selected) ? TRUE : FALSE ) : FALSE );
				bMarkPreviewDrawn = DrawMark(pDC, objectRect, rObject, pActPrintSheet, bSelected);
			}
		}

		if (rObject.m_nType == CLayoutObject::Page) 
		{
			if ( (pObjectTemplate != NULL) && ! bDrawSelectedOnly)
				CLayoutObject::DrawPageID(pDC, objectRect, rObject, pActPrintSheet, *pObjectTemplate, this, crBackground, ShowMeasure());
		}
		else
			if ( ! bMarkPreviewDrawn)
			{
				CPageTemplate *pMarkTemplate = rObject.GetCurrentMarkTemplate();
				if (pMarkTemplate != NULL)
					if (MarksViewActive() && ! ShowBitmap())
					{
						CProdDataPrintView*	pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
						if (pProdDataPrintView)
							if ( ! pProdDataPrintView->IsSelected(&rObject))
								if ( ! IsOpenDlgLayoutObjectProperties() && ! IsOpenMarkSetDlg())
									CLayoutObject::DrawObjectID(pDC, objectRect, rObject, pMarkTemplate->m_strPageID, "", 0, this); 
					}
			}
	}

	if (nType == CLayoutObject::Page) 
		if (!(ShowBitmap() || ShowExposures() || ShowColorants()))
		{
			pos = pLayoutSide->m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
				if ( ! (rObject.m_nStatus & CLayoutObject::Disabled))
					rObject.DrawOverFold(pDC, pActPrintSheet);
			}
		}

	if (bUnplausibleObjectFound)
	{
		//CString strMessage;
		//AfxFormatString1(strMessage, IDS_INVALID_OBJECT_MESSAGE, pLayoutSide->GetLayout()->m_strLayoutName);
		//AfxMessageBox(strMessage, MB_OK);

		pLayoutSide->m_ObjectList.RemoveUnplausibleObjects();
	}
}

COLORREF CPrintSheetView::DrawPage(CDC* pDC, CRect& rPageRect, CLayoutObject& rObject, CPageTemplate* pObjectTemplate, CPrintSheet* pActPrintSheet, BOOL bDrawSelectedOnly, COLORREF* pcrBackground)
{
	COLORREF crPageColor = LIGHTGRAY;
	CLayout* pLayout = pActPrintSheet->GetLayout();
	if ( ! pLayout)
		return crPageColor;

	int nProductPartIndex = (pObjectTemplate) ? pObjectTemplate->m_nProductPartIndex : 0;

	if (pDC->RectVisible(rPageRect))
	{
		CImpManDoc*  pDoc = CImpManDoc::GetDoc();
		BOOL bIsFlatProduct = rObject.IsFlatProduct();
		BOOL bShowBoxShape = FALSE;
		if ( bIsFlatProduct && bShowBoxShape)
		{
			if (pDoc)
				if ( (rObject.m_nComponentRefIndex >= 0) && (rObject.m_nComponentRefIndex < pDoc->m_flatProducts.GetSize()) )
				{
					CString strObjectName	 = pDoc->m_flatProducts.FindByIndex(rObject.m_nComponentRefIndex).m_strProductName;
					CGraphicObject* pgObject = pDoc->m_flatProducts.FindByIndex(rObject.m_nComponentRefIndex).m_graphicList.GetObject(strObjectName);
					if (pgObject)
						bShowBoxShape = TRUE;
				}
		}

		//COLORREF crFrameColor = (ShowFoldSheet() || MarksViewActive() || bShowBoxShape) ? RGB(180,180,255) : BLUE;
		COLORREF crFrameColor = RGB(100,100,255);
		if (bDrawSelectedOnly)
			return FALSE;

		if (rObject.m_nStatus & CLayoutObject::Highlighted)
			crFrameColor = CGraphicComponent::IncreaseColorBrightness(crFrameColor, 1.3f);

		CPen	pen(PS_SOLID, Pixel2ConstMM(pDC, 1), (rObject.m_nStatus & CLayoutObject::Disabled) ? RGB(220,220,220) : crFrameColor);	
		CPen*	pOldPen = pDC->GetCurrentPen();

		pDC->SelectObject(&pen);
		pDC->SelectStockObject(HOLLOW_BRUSH);	// Predefined brush

		if ( ! ( ShowBitmap() || ShowExposures()) )
		{
			if (pDoc)
			{
				if ( ! ShowColorants() && ! (rObject.m_nStatus & CLayoutObject::Disabled))
				{
					pDC->SaveDC();
					pDC->BeginPath();
						pDC->Rectangle(rPageRect);	
					pDC->EndPath();
					pDC->SelectClipPath(RGN_DIFF);
					CRect rcGrossFrame = rObject.CalcGrossFrame(pActPrintSheet);
					FillTransparent(pDC, rcGrossFrame, RGB(100,190,255), TRUE, 40, TRUE);
					pDC->RestoreDC(-1);
				}
			}

			int nColorIndex = 0;
			if (bIsFlatProduct)
				nColorIndex = ( (rObject.m_nComponentRefIndex < 0) || (rObject.m_nComponentRefIndex >= pActPrintSheet->m_flatProductRefs.GetSize()) ) ? rObject.m_nComponentRefIndex : pActPrintSheet->m_flatProductRefs[rObject.m_nComponentRefIndex].m_nFlatProductIndex;
			else
				nColorIndex = pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex);
			nColorIndex = nColorIndex % MAX_OBJECT_COLORS;
			crPageColor = theApp.m_objectColors[nColorIndex];
			float	 fDimmFactor = (ShowFoldSheet() || MarksViewActive()) ? 1.2f : 1.2f;
			CBrush*  pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);		// draw pages transparent in order to make visible overlappings
																					// disadvantage: slower display procedure
			crPageColor  = CGraphicComponent::IncreaseColorBrightness(crPageColor, fDimmFactor);
			if (pcrBackground)
				*pcrBackground = crPageColor;
			if ( ! (rObject.m_nStatus & CLayoutObject::Disabled))
			{
				FillTransparent(pDC, rPageRect, crPageColor, TRUE, 200);
				if ( ! bIsFlatProduct)
				{
					CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
					if ( (rProductPart.m_fPageWidth != rObject.GetRealWidth()) || (rProductPart.m_fPageHeight != rObject.GetRealHeight()) )		// page format modifed by user (i.e. cover inlet) -> display darker
						FillTransparent(pDC, rPageRect, LIGHTGRAY, TRUE, 100);
				}
			}

			DrawRectangleExt(pDC, rPageRect);
			DrawHeadPosition(pDC, rPageRect, rObject.m_nHeadPosition, (rObject.m_nStatus & CLayoutObject::Disabled) ? RGB(240,240,240) : crFrameColor);

			if ( (GetDlgOutputMedia()->GetDlgPDFTargetProps()) || (IsOpenDlgLayoutObjectGeometry()) || AlignSectionActive())
			{
				BOOL bDrawHorizontal	= TRUE;
				BOOL bDrawVertical		= TRUE;
				BOOL bDrawCenterHandles = TRUE;
				CDlgLayoutObjectGeometry* pDlg = GetDlgLayoutObjectGeometry();
				if (pDlg)
				{
					int nEdge = pDlg->GetMarkEdge();
					bDrawHorizontal	= bDrawVertical	= FALSE;
					bDrawCenterHandles = (pDlg->m_nMarkAnchor == REFLINE_ANCHOR) ? TRUE : FALSE;
					switch (nEdge)
					{
					case 0:			bDrawHorizontal = (pDlg->m_bVariableHeight) ? FALSE : TRUE;
									bDrawVertical	= (pDlg->m_bVariableWidth)  ? FALSE : TRUE; 
									break;
					case LEFT:
					case RIGHT:		bDrawVertical	= TRUE; break;
					case BOTTOM:
					case TOP:		bDrawHorizontal = TRUE; break;
					}
				}
				if (bDrawCenterHandles && pLayout->IsUniform())
					CReflinePositionParams::DrawCenterHandles(pDC, rPageRect, LIGHTBLUE, bDrawHorizontal, bDrawVertical);
			}

			int nFontSize = CLayout::TransformFontSize(pDC, 18);	// 18 mm when plotted 1:1
			CFont font, boldFont, *pOldFont;
			font.CreateFont	   (nFontSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
			boldFont.CreateFont(nFontSize, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
			pOldFont = pDC->SelectObject(&font);
			COLORREF crOldTextColor = pDC->SetTextColor(BLUE);
 
			if (bIsFlatProduct)
			{
				CFlatProduct& rFlatProduct = rObject.GetCurrentFlatProduct(pActPrintSheet);
				if ( ! rFlatProduct.IsNull())
				{
					int nFlatProductIndex = rFlatProduct.GetIndex();
					CString strOut; 
					if (pObjectTemplate)
						strOut.Format((pObjectTemplate->m_bLabelBackSide) ? _T(" %d* ") : _T(" %d "), nFlatProductIndex + 1);
					else
						strOut.Format(_T(" %d "), nFlatProductIndex + 1);
					CRect rcText = rPageRect;
					pDC->SetBkMode(TRANSPARENT);
					pDC->SetTextColor(RGB(200,100,0));
					pDC->SelectObject(( (rcText.Width() < nFontSize * 3) || (abs(rcText.Height()) < nFontSize * 3)) ? &font : &boldFont);
					pDC->DrawText(strOut, rcText, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
				}
			}
			else
			{	// show format here only when not flat. Formats of flat products will be handled in DrawObjectIDFlatProduct() because of special spacing when objects are small and grouped
				if (ViewMode() != ID_VIEW_ALL_SHEETS)
					if (ShowMeasure() && (rObject.GetCurrentAttribute(pActPrintSheet) & CLayoutObject::GroupLowerLeft) )
					{
						CString strWidth, strHeight, strOut;
						TruncateNumber(strWidth,  rObject.GetWidth());
						TruncateNumber(strHeight, rObject.GetHeight());
						strOut.Format(_T("%s x %s"), strWidth, strHeight);
						CRect rcText = rPageRect; rcText.top -= nFontSize/2;
						pDC->SetBkMode(OPAQUE);
						pDC->SetBkColor(WHITE);
						pDC->SelectObject(&font);
						pDC->SetTextColor(BLACK);
						pDC->DrawText(strOut, rcText, DT_CENTER | DT_TOP | DT_SINGLELINE);
					}
			}

			pDC->SelectObject(pOldFont);
			pDC->SetTextColor(crOldTextColor);
			font.DeleteObject();
			boldFont.DeleteObject();

			pDC->SelectObject(pOldBrush);
		}

		pDC->SelectObject(pOldPen);
		pen.DeleteObject();
	}

	return crPageColor;
}

void CPrintSheetView::DrawPanoramaPage(CDC* pDC, CRect& rPageRect, CLayoutObject& rObject, CLayoutObject& rBuddy, CPrintSheet& rPrintSheet)
{
	if (rBuddy.m_nPublicFlag)	// if other panorama half already drawn -> return to avoid page id from being overlayed
		return;

	rObject.m_nPublicFlag = 1;

	CRect buddyRect;
	buddyRect.left	 = WorldToLP(rBuddy.GetLeft());
	buddyRect.top	 = WorldToLP(rBuddy.GetTop());
	buddyRect.right	 = WorldToLP(rBuddy.GetRight());
	buddyRect.bottom = WorldToLP(rBuddy.GetBottom());

	CRect panoramaRect(min(buddyRect.left,  rPageRect.left),  max(buddyRect.top,	rPageRect.top), 
					   max(buddyRect.right, rPageRect.right), min(buddyRect.bottom, rPageRect.bottom));

	if (pDC->RectVisible(panoramaRect))
	{
		CPen	linePen		(PS_DOT,   Pixel2ConstMM(pDC, 1), RGB(180,180,255));
		CPen	lightBluePen(PS_SOLID, Pixel2ConstMM(pDC, 1), RGB(180,180,255));	//LIGHTBLUE);
		CPen	bluePen		(PS_SOLID, Pixel2ConstMM(pDC, 1), BLUE);	
		CPen	lightRedPen (PS_SOLID, Pixel2ConstMM(pDC, 1), LIGHTRED);	
		CPen*	pOldPen = pDC->GetCurrentPen();

		COLORREF crFrame = BLUE;
		if (ShowFoldSheet() || MarksViewActive())
		{
			pDC->SelectObject((rObject.m_nType == CLayoutObject::Page) ? &lightBluePen : &lightRedPen);
			crFrame = (rObject.m_nType == CLayoutObject::Page) ? RGB(180,180,255) : LIGHTRED;
		}
		else
		{
			pDC->SelectObject((rObject.m_nType == CLayoutObject::Page) ? &bluePen : &lightRedPen);
			crFrame = (rObject.m_nType == CLayoutObject::Page) ? BLUE : LIGHTRED;
		}

		pDC->SelectStockObject(HOLLOW_BRUSH);	// Predefined brush

		if ( ! (ShowBitmap() || ShowExposures()))
		{
			int		 nIndex		 = (ShowMeasure()) ? (rObject.GetDisplayColorIndex(&rPrintSheet) % MAX_OBJECT_COLORS) : 0;
			COLORREF crPageColor = theApp.m_objectColors[nIndex];
			float	 fDimmFactor = (ShowFoldSheet() || MarksViewActive()) ? 1.2f : 1.0f;
			CBrush   pageBrush(CGraphicComponent::IncreaseColorBrightness(crPageColor, fDimmFactor));
			CBrush*  pOldBrush = pDC->SelectObject(&pageBrush);

			DrawRectangleExt(pDC, panoramaRect);
			DrawHeadPosition(pDC, buddyRect, rObject.m_nHeadPosition, ( ! (rObject.m_nStatus & CLayoutObject::Disabled)) ? crFrame : RGB(240,240,240));
			DrawHeadPosition(pDC, rPageRect, rObject.m_nHeadPosition, ( ! (rObject.m_nStatus & CLayoutObject::Disabled)) ? crFrame : RGB(240,240,240));
			pDC->SelectObject(&linePen);

			if (rObject.IsMultipagePart(rPrintSheet, LEFT))
			{
				switch (rObject.m_nHeadPosition)
				{
				case TOP:
					pDC->MoveTo(rPageRect.right, rPageRect.top);
					pDC->LineTo(rPageRect.right, rPageRect.bottom);
					break;
				case RIGHT:
					pDC->MoveTo(rPageRect.left,  rPageRect.bottom);
					pDC->LineTo(rPageRect.right, rPageRect.bottom);
					break;
				case BOTTOM:
					pDC->MoveTo(rPageRect.left,  rPageRect.top);
					pDC->LineTo(rPageRect.left,  rPageRect.bottom);
					break;
				case LEFT:
					pDC->MoveTo(rPageRect.left,  rPageRect.top);
					pDC->LineTo(rPageRect.right, rPageRect.top);
					break;
				}
			}
			else
			{
				switch (rObject.m_nHeadPosition)
				{
				case TOP:
					pDC->MoveTo(rPageRect.left,  rPageRect.top);
					pDC->LineTo(rPageRect.left,  rPageRect.bottom);
					break;
				case RIGHT:
					pDC->MoveTo(rPageRect.left,  rPageRect.top);
					pDC->LineTo(rPageRect.right, rPageRect.top);
					break;
				case BOTTOM:
					pDC->MoveTo(rPageRect.right, rPageRect.top);
					pDC->LineTo(rPageRect.right, rPageRect.bottom);
					break;
				case LEFT:
					pDC->MoveTo(rPageRect.left,  rPageRect.bottom);
					pDC->LineTo(rPageRect.right, rPageRect.bottom);
					break;
				}
			}

			pDC->SelectObject(pOldBrush);
			pageBrush.DeleteObject();
		}

		pDC->SelectObject(pOldPen);
		linePen.DeleteObject();
		lightBluePen.DeleteObject();
		bluePen.DeleteObject();
		lightRedPen.DeleteObject();
	}
}


#define MIN_MARKSIZE 3
#define MINSIZE_MARK(markRect)\
{\
	CRect testRect = rMarkRect;\
	pDC->LPtoDP(testRect);\
	if ( (testRect.Height() < MIN_MARKSIZE) || (testRect.Width() < MIN_MARKSIZE) )\
	{\
		testRect.top	-= MIN_MARKSIZE;\
		testRect.bottom += MIN_MARKSIZE;\
		testRect.left   -= MIN_MARKSIZE;\
		testRect.right  += MIN_MARKSIZE;\
	}\
	pDC->DPtoLP(testRect);\
	rMarkRect = testRect;\
}

BOOL CPrintSheetView::DrawMark(CDC* pDC, CRect& rMarkRect, CLayoutObject& rObject, CPrintSheet* pActPrintSheet, BOOL bDoHighlightMark)
{
	BOOL bPreviewDrawn	  = FALSE;
	BOOL bIsOpenDlgModify = (IsOpenMarkSetDlg() || IsOpenDlgNewMark()) ? TRUE : FALSE;
	if (MarksViewActive() && ! bIsOpenDlgModify)
	{
		CSize sizeInflate(4, 4); pDC->DPtoLP(&sizeInflate);
		CRect rcHighlight = rMarkRect; rcHighlight.InflateRect(sizeInflate.cx, 0); rcHighlight.top += sizeInflate.cy; rcHighlight.bottom -= sizeInflate.cy;
		if (bDoHighlightMark)
		{
			CPrintSheetView::FillTransparent (pDC, rcHighlight, BORDEAUX, TRUE, 100);

			CPen pen(PS_SOLID, 1, BORDEAUX);	
			CPen* pOldPen = pDC->SelectObject(&pen);
			pDC->FillSolidRect(rMarkRect, WHITE);
			pDC->SelectObject(pOldPen);
			pen.DeleteObject();

			DrawPlatePreviews(pDC, rObject.GetLayoutSideIndex(), pActPrintSheet, CLayoutObject::ControlMark, &rObject, rObject.m_strFormatName);
			bPreviewDrawn = TRUE;
		}
		MARKOBJECTINFO moInfo = {rcHighlight, bDoHighlightMark, &rObject, NULL, pActPrintSheet};
		m_markObjectInfoList.Add(moInfo);
	}

	if ( ! bDoHighlightMark && pDC->RectVisible(rMarkRect))
	{
		CPen	markPen (PS_SOLID, Pixel2ConstMM(pDC, 1), RGB(255,150,150));
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
		CPen*	pOldPen	  = pDC->SelectObject(&markPen);

		if ( MarksViewActive() && ! bIsOpenDlgModify)
			FillTransparent(pDC, rMarkRect, RGB(255,180,180), TRUE);
		else
			FillTransparent(pDC, rMarkRect, RGB(255,160,160), TRUE);
	
		if (MarksViewActive() && ! bIsOpenDlgModify)
			DrawRectangleExt(pDC, rMarkRect);
		//DrawHeadPosition(pDC, rMarkRect, rObject.m_nHeadPosition, /*(bDoHighLightMark) ? BORDEAUX : */LIGHTRED);
		//HWND hWnd = (GetDlgMoveObjects()) ? GetDlgMoveObjects()->m_hWnd : NULL;
		//if ( GetDlgOutputMedia()->GetDlgPDFTargetProps() || IsOpenDlgLayoutObjectGeometry() || hWnd )
		//	CReflinePositionParams::DrawCenterHandles(pDC, rMarkRect, LIGHTRED);

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		markPen.DeleteObject();
	}

	return bPreviewDrawn;
}

void CPrintSheetView::DrawMarkInfoFields(CDC* pDC, CPrintSheet* pActPrintSheet)
{
	//BOOL bModifyMode = FALSE;
	//if (IsOpenDlgLayoutObjectProperties())
	//{
	//	CDlgLayoutObjectProperties* pDlg = GetDlgLayoutObjectProperties();
	//	if (pDlg)
	//		if (pDlg->m_bModeModify)
	//			bModifyMode = TRUE;
	//}

	//for (int i = 0; i < m_markObjectInfoList.GetSize(); i++)
	//{
	//	MARKOBJECTINFO moInfo = m_markObjectInfoList[i];
	//	if (moInfo.m_pPrintSheet != pActPrintSheet)
	//		continue;
	//	if ( ! moInfo.m_pLayoutObject && ! moInfo.m_pMark)
	//		continue;

	//	CString strName = (moInfo.m_pLayoutObject) ? moInfo.m_pLayoutObject->m_strFormatName : moInfo.m_pMark->m_strMarkName;

	//	pDC->LPtoDP(moInfo.m_rcMark);
	//	CDC*  pWinDC	 = GetDC();
	//	CFont font;
	//	font.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));	
	//	CFont* pOldFont = pWinDC->SelectObject(&font);
	//	CSize sizeExtent = pWinDC->GetTextExtent(strName);
	//	CRect rcInfo(CPoint(moInfo.m_rcMark.CenterPoint().x - sizeExtent.cx/2 - 10, moInfo.m_rcMark.top - sizeExtent.cy - 10), CSize(sizeExtent.cx + 20, 20)); 

	//	if (moInfo.m_bShow)
	//	{
	//		CGraphicComponent gc;
	//		gc.DrawShadowFrame(pWinDC, rcInfo, 2);
	//		if (moInfo.m_pLayoutObject || bModifyMode)
	//			gc.DrawTitleBar(pWinDC, rcInfo, _T(""), WHITE, RGB(255,220,220), 90, FALSE);
	//		else
	//			gc.DrawTitleBar(pWinDC, rcInfo, _T(""), WHITE, RGB(255,235,200), 90, FALSE);

	//		pWinDC->SetTextColor(BLACK);
	//		pWinDC->SetBkMode(TRANSPARENT);
	//		pWinDC->DrawText(strName, rcInfo, DT_CENTER | DT_SINGLELINE | DT_VCENTER);
	//	}
	//	font.DeleteObject();
	//	ReleaseDC(pWinDC);

	//	rcInfo.bottom += 2; rcInfo.right += 2;
	//	pDC->DPtoLP(rcInfo);

	//	// register info field anyway (if shown or not) - otherwise we get problems with display list invalidation
	//	BOOL  bIsLayoutObject = (moInfo.m_pLayoutObject) ? TRUE : FALSE;
	//	void* pObject		  = (bIsLayoutObject) ? (void*)moInfo.m_pLayoutObject : (void*)moInfo.m_pMark;
	//	CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, (moInfo.m_bShow) ? rcInfo : CRect(0,0,0,0), rcInfo, pObject, DISPLAY_ITEM_REGISTRY(CPrintSheetView, MarkObjectInfo), (void*)bIsLayoutObject, CDisplayItem::ForceRegister); 
	//	if (pDI)
	//	{
	//		pDI->SetNoStateFeedback();
	//		pDI->m_nState = (moInfo.m_bShow) ? CDisplayItem::Selected : CDisplayItem::Normal;
	//	}
	//}
}

void CPrintSheetView::DrawHeadPosition(CDC* pDC, CRect& rPageRect, BYTE HeadPosition, COLORREF crColor)
{
	CRect rect = rPageRect;
	CSize size(1, 1);
	pDC->DPtoLP(&size);
	rect.bottom += size.cx;
	rect.left   += size.cx;

	CPoint p[3];
	int	   dx = rect.Width()/3, dy = rect.Height()/3;

	if (pDC->RectVisible(rect))
	{
		int nWidth  = abs(rect.right - rect.left);
		int nHeight = abs(rect.top   - rect.bottom);
		CSize szLimit(5, 5);
		pDC->DPtoLP(&szLimit);
		switch (HeadPosition)
		{
		case LEFT	: if (nHeight < szLimit.cy)
						  return;
					  p[0] = rect.TopLeft()	 - CSize(0, nHeight);
					  p[1] = rect.CenterPoint() - CSize(dx, 0);
					  p[2] = rect.TopLeft();
			break;
		case RIGHT	: if (nHeight < szLimit.cy)
						  return;
					  p[0] = rect.BottomRight() + CSize(0, nHeight);
					  p[1] = rect.CenterPoint() + CSize(dx, 0);
					  p[2] = rect.BottomRight();
			break;
		case BOTTOM	: if (nWidth < szLimit.cx)
						  return;
					  p[0] = rect.BottomRight();
					  p[1] = rect.CenterPoint() + CSize(0, dy);
					  p[2] = rect.BottomRight() - CSize(nWidth, 0);
			break;
		case TOP	: if (nWidth < szLimit.cx)
						  return;
					  p[0] = rect.TopLeft(); 
					  p[1] = rect.CenterPoint() - CSize(0, dy);
					  p[2] = rect.TopLeft()	 + CSize(nWidth, 0);
			break;
		default		: if (nWidth < szLimit.cx)
						  return;
					  p[0] = rect.TopLeft(); 
					  p[1] = rect.CenterPoint() - CSize(0, dy);
					  p[2] = rect.TopLeft()	 + CSize(nWidth, 0);
			break;
		}

		CSize sizeObject(abs(rect.Width()), abs(rect.Height()));
		pDC->LPtoDP(&sizeObject);
		sizeObject.cx = DP2MM(pDC, sizeObject.cx); sizeObject.cy = DP2MM(pDC, sizeObject.cy);
		int nPenWidth = ( (sizeObject.cx < 2000) || (sizeObject.cy < 2000) ) ?  Pixel2ConstMM(pDC, 1)/2 : Pixel2ConstMM(pDC, 1);	// smaller than 20 mm -> take half line width

		CPen pen(PS_SOLID, nPenWidth, crColor);
		CPen* pOldPen = pDC->SelectObject(&pen);
		pDC->Polyline(p, 3);
		pDC->SelectObject(pOldPen);
	}
}

void CPrintSheetView::DrawGraphics(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	if ( ! ShowBoxShapes())
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CLayout* pLayout = pActPrintSheet->GetLayout();
	if ( ! pLayout)
		return;

	int	nMirrorAxis = -1;
	if (nActLayoutSide == BACKSIDE)
	{
		switch (pLayout->KindOfProduction())
		{
		case WORK_AND_TURN	: nMirrorAxis = VERTICAL;	break;
		case WORK_AND_TUMBLE: nMirrorAxis = HORIZONTAL;	break;
		default:			  break;
		}
	}

	CPen* pOldPen = pDC->GetCurrentPen();
	CPen  pen(PS_SOLID, 0, DARKBLUE);	
	pDC->SelectObject(&pen);

	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;
	POSITION	 pos		 = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
		if (rObject.m_nType != CLayoutObject::Page) 
			continue;									   
		if ( (rObject.m_nComponentRefIndex < 0) || (rObject.m_nComponentRefIndex >= pDoc->m_flatProducts.GetSize()) )
			continue;

		CString strObjectName	 = pDoc->m_flatProducts.FindByIndex(rObject.m_nComponentRefIndex).m_strProductName;
		CGraphicObject* pgObject = pDoc->m_flatProducts.FindByIndex(rObject.m_nComponentRefIndex).m_graphicList.GetObject(strObjectName);
		if (pgObject)
		{
			g_bIsContentView = FALSE;
			int nHeadPosition = (nActLayoutSide == BACKSIDE) ? rObject.GetMirrorHeadPosition(nMirrorAxis) : rObject.m_nHeadPosition;
			pgObject->Draw(pDC, rObject.GetLeft(), rObject.GetBottom(), rObject.GetWidth(), rObject.GetHeight(), nHeadPosition, nMirrorAxis);
		}
	}

	pDC->SelectObject(pOldPen);
}

void CPrintSheetView::DrawShinglingOffsets(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	if ( ! IsOpenDlgShinglingAssistent())
		return;
	if (ShowBitmap() || MarksViewActive() || ShowExposures())
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActPrintSheet->GetLayout()->m_FrontSide : &pActPrintSheet->GetLayout()->m_BackSide;
	CFont*	 pOldFont		 = (CFont*)pDC->SelectStockObject(ANSI_VAR_FONT);
	COLORREF crOldTextColor  = pDC->SetTextColor(BLACK);
	COLORREF crOldBkColor	 = pDC->SetBkColor  (WHITE);
	int		 nOldBkMode		 = pDC->SetBkMode   (OPAQUE);

	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
		if (rObject.m_nType != CLayoutObject::Page) 
			continue;									   

		CPageTemplate *pPageTemplate = rObject.GetCurrentPageTemplate(pActPrintSheet);
		if (pPageTemplate != NULL)
		{
			CRect pageRect;
			pageRect.left	= WorldToLP(rObject.GetLeft());
			pageRect.top	= WorldToLP(rObject.GetTop());
			pageRect.right	= WorldToLP(rObject.GetRight());
			pageRect.bottom	= WorldToLP(rObject.GetBottom());

			CBoundProductPart& rProductPart = pPageTemplate->GetProductPart();
			if (rProductPart.m_foldingParams.m_bShinglingActive)
			{
				if (fabs(pPageTemplate->m_fShinglingOffsetX) > 0.0001f)
				{
					CSize size(2, 2);
					pDC->DPtoLP(&size);
					CPen pen(PS_SOLID, size.cx, RGB(252, 220, 150));
					pDC->SelectObject(&pen);
					CRect rcShingling = pageRect; 
					float fShinglingOffset = 0.0f;
					int	  nIDPos		   = LEFT;
					switch (rObject.m_nHeadPosition)
					{
					case TOP:		
					case BOTTOM:	nIDPos			 = (rObject.m_nHeadPosition == TOP) ? LEFT : RIGHT;
									fShinglingOffset = (rObject.m_nHeadPosition == TOP) ? CPrintSheetView::WorldToLP(pPageTemplate->m_fShinglingOffsetX) : -CPrintSheetView::WorldToLP(pPageTemplate->m_fShinglingOffsetX);
									if (rObject.m_nIDPosition == nIDPos)
									{
										rcShingling.left  += fShinglingOffset;
										if ( (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodScale) && (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodBottlingScale) )
											rcShingling.right  += fShinglingOffset;
									}
									else
									{
										rcShingling.right += fShinglingOffset;
										if ( (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodScale) && (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodBottlingScale) )
											rcShingling.left  += fShinglingOffset;
									}
									break;
					case LEFT:		
					case RIGHT:		nIDPos			 = (rObject.m_nHeadPosition == LEFT) ? LEFT : RIGHT;
									fShinglingOffset = (rObject.m_nHeadPosition == LEFT) ? CPrintSheetView::WorldToLP(pPageTemplate->m_fShinglingOffsetX) : -CPrintSheetView::WorldToLP(pPageTemplate->m_fShinglingOffsetX);
									if (rObject.m_nIDPosition == nIDPos)
									{
										rcShingling.bottom  += fShinglingOffset;
										if ( (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodScale) && (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodBottlingScale) )
											rcShingling.top  += fShinglingOffset;
									}
									else
									{
										rcShingling.top += fShinglingOffset;
										if ( (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodScale) && (rProductPart.m_foldingParams.m_nShinglingMethod != CBookblock::ShinglingMethodBottlingScale) )
											rcShingling.bottom  += fShinglingOffset;
									}
									break;
					}

					DrawRectangleExt(pDC, rcShingling);
					pen.DeleteObject();
				}

				if ( (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottling) || (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale) )
				{
					DrawPageShingling(pDC, pPageTemplate, pageRect, rObject.m_nHeadPosition, TOP,	  &m_DisplayList);
					DrawPageShingling(pDC, pPageTemplate, pageRect, rObject.m_nHeadPosition, BOTTOM,  &m_DisplayList);
				}
				else
					DrawPageShingling(pDC, pPageTemplate, pageRect, rObject.m_nHeadPosition, YCENTER, &m_DisplayList);
			}
		}
	}

	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldTextColor);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetBkMode(nOldBkMode);
}

void CPrintSheetView::DrawPageShingling(CDC* pDC, CPageTemplate* pPageTemplate, CRect pageRect, int nHeadPosition, int nPosition, CDisplayList* pDisplayList)
{
	DrawPageShinglingX(pDC, pPageTemplate, pageRect, nHeadPosition, nPosition, pDisplayList);
	DrawPageShinglingY(pDC, pPageTemplate, pageRect, nHeadPosition, pDisplayList);
}

void CPrintSheetView::DrawPageShinglingX(CDC* pDC, CPageTemplate* pPageTemplate, CRect pageRect, int nHeadPosition, int nPosition, CDisplayList* pDisplayList)
{
	if ( ! pPageTemplate)
		return;
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(pPageTemplate->m_nProductPartIndex);

	float fShinglingOffset = (nPosition == BOTTOM) ? pPageTemplate->m_fShinglingOffsetXFoot : pPageTemplate->m_fShinglingOffsetX;
	CString strShinglingOffset, strString;
	if (fabs(fShinglingOffset) > 0.009f)
		TruncateNumber(strString, (float)fabs(fShinglingOffset));
	if (pPageTemplate->m_bShinglingOffsetLocked) 
		if (strString.IsEmpty())
			strShinglingOffset = _T(" * ");
		else
			strShinglingOffset.Format(_T(" %s * "), strString);
	else
		strShinglingOffset.Format(_T(" %s "), strString);

	if (fabs(fShinglingOffset) > 0.000001f)
		if ( (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodScale) || (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale) ) 
		{
			float fScale = pPageTemplate->CalcShinglingScaleX();
			if (fabs(fScale) > 0.000001)
			{
				CString strNum, string;
				TruncateNumber(strNum, fScale * 100.0f);
				string.Format(_T(" | %s %%"), strNum);
				strShinglingOffset += string;
			}
		}

	if ( ! strShinglingOffset.IsEmpty())
	{
		CRect rcRect = pageRect; pDC->LPtoDP(rcRect);
		if ( (rcRect.Width() < 50) || (rcRect.Height() < 50) )	// don't draw if page is too small
			return;

		CSize fontSize(0, 13);
		pDC->DPtoLP(&fontSize);
		CFont font;
		font.CreateFont(fontSize.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));	
		CFont* pOldFont = pDC->SelectObject(&font);

		pageRect.NormalizeRect();

		CSize textExtent = pDC->GetTextExtent(strShinglingOffset);
		int nXPos = pageRect.CenterPoint().x - textExtent.cx/2;				  
		int nYPos = pageRect.CenterPoint().y + textExtent.cy/2; 
		CSize szOffset(0,0);
		switch (nPosition)
		{
		case TOP   : szOffset.cx =  2 * textExtent.cx; szOffset.cy =  pageRect.Height() / 4; break;
		case BOTTOM: szOffset.cx = -2 * textExtent.cx; szOffset.cy = -pageRect.Height() / 4; break;
		}
		switch (nHeadPosition)
		{
		case TOP:		nYPos += szOffset.cy; break;
		case RIGHT:		nXPos += szOffset.cx; break;
		case BOTTOM:	nYPos -= szOffset.cy; break;
		case LEFT:		nXPos -= szOffset.cx; break;
		}

		COLORREF crColor = RGB(220,220,255);
		CRect textRect(nXPos, nYPos, nXPos + textExtent.cx, nYPos - textExtent.cy);

		int	   nDirection = TOP;
		switch (nHeadPosition)
		{
		case TOP:	nDirection = (fShinglingOffset < 0.0f) ? LEFT   : RIGHT;	break;
		case BOTTOM:nDirection = (fShinglingOffset < 0.0f) ? RIGHT  : LEFT;		break;
		case LEFT:	nDirection = (fShinglingOffset < 0.0f) ? BOTTOM : TOP;		break;
		case RIGHT: nDirection = (fShinglingOffset < 0.0f) ? TOP	: BOTTOM;	break;
		}

		CRect rcArrow(pageRect.CenterPoint(), pageRect.CenterPoint());
		rcArrow.NormalizeRect();
		textRect.NormalizeRect();
		CSize szSize(pageRect.Height()/30, pageRect.Height()/30);
		pDC->LPtoDP(&szSize); szSize = CSize(min(5, szSize.cx), min(5, szSize.cy));
		pDC->DPtoLP(&szSize);
		rcArrow.InflateRect(szSize);
		if ( (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodScale) || (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale) )
		{
			switch (nHeadPosition)
			{
			case TOP:		
			case BOTTOM:	rcArrow.OffsetRect(	  (nDirection == LEFT)   ? pageRect.right  - rcArrow.right  : -(rcArrow.left - pageRect.left), 0);	break;
			case LEFT:		
			case RIGHT:		rcArrow.OffsetRect(0, (nDirection == BOTTOM) ? pageRect.bottom - rcArrow.bottom : -(rcArrow.top  - pageRect.top));		break;
			}
		}

		CPoint hotSpot = rcArrow.CenterPoint();
		if (fabs(fShinglingOffset) > 0.009f)
		{
			int nWidth  = textRect.Width(); 
			int nHeight = textRect.Height();
			switch (nDirection)
			{
			case LEFT:		hotSpot.x = rcArrow.left;				hotSpot.y = textRect.CenterPoint().y;	textRect.right  = hotSpot.x - szSize.cx/2; textRect.left   = textRect.right  - nWidth;	break;
			case RIGHT:		hotSpot.x = rcArrow.right;				hotSpot.y = textRect.CenterPoint().y;	textRect.left   = hotSpot.x + szSize.cx/2; textRect.right  = textRect.left   + nWidth;	break;
			case BOTTOM:	hotSpot.x = textRect.CenterPoint().x;	hotSpot.y = rcArrow.top;				textRect.bottom = hotSpot.y - szSize.cy/2; textRect.top    = textRect.bottom - nHeight;	break;
			case TOP:		hotSpot.x = textRect.CenterPoint().x;	hotSpot.y = rcArrow.bottom;				textRect.top    = hotSpot.y + szSize.cy/2; textRect.bottom = textRect.top    + nHeight;	break;
			}
		}

		crColor = RGB(220,150,0);
		if (pDisplayList)
		{
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetView)))
			{
				float* pAuxData = (nPosition == BOTTOM) ? &pPageTemplate->m_fShinglingOffsetXFoot : &pPageTemplate->m_fShinglingOffsetX;
				pDisplayList->RegisterItem(pDC, textRect, pageRect, (void*)pPageTemplate, DISPLAY_ITEM_REGISTRY(CPrintSheetView, ShinglingValueX), pAuxData, CDisplayItem::ForceRegister); 
			}
			else
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CBookBlockProdDataView)))
			{
				float* pAuxData = (nPosition == BOTTOM) ? &pPageTemplate->m_fShinglingOffsetXFoot : &pPageTemplate->m_fShinglingOffsetX;
				pDisplayList->RegisterItem(pDC, textRect, pageRect, (void*)pPageTemplate, DISPLAY_ITEM_REGISTRY(CBookBlockProdDataView, ShinglingValueX), pAuxData, CDisplayItem::ForceRegister); 
			}
		}
			
		if (fabs(fShinglingOffset) > 0.009f)
		{
			pDC->LPtoDP(&hotSpot);
			pDC->LPtoDP(&szSize);
			DrawArrow(pDC, nDirection, hotSpot, 2 * szSize.cx, BLACK, crColor, TRUE, CPoint(0,0), szSize.cx);
		}

		pDC->TextOut(textRect.left, textRect.bottom, strShinglingOffset);

		pDC->SelectObject(pOldFont);
		font.DeleteObject();
	}
}

void CPrintSheetView::DrawPageShinglingY(CDC* pDC, CPageTemplate* pPageTemplate, CRect pageRect, int nHeadPosition, CDisplayList* pDisplayList)
{
	if ( ! pPageTemplate)
		return;
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(pPageTemplate->m_nProductPartIndex);
	if (rProductPart.m_foldingParams.m_nShinglingDirection == CBookblock::ShinglingDirectionSpine)
		return;

	float fShinglingOffset = pPageTemplate->m_fShinglingOffsetY;
	CString strShinglingOffset, strString;
	if (fabs(fShinglingOffset) > 0.009f)
		TruncateNumber(strString, (float)fabs(fShinglingOffset));
	if (pPageTemplate->m_bShinglingOffsetLocked) 
		if (strString.IsEmpty())
			strShinglingOffset = _T(" * ");
		else
			strShinglingOffset.Format(_T(" %s * "), strString);
	else
		strShinglingOffset.Format(_T(" %s "), strString);

	if (fabs(fShinglingOffset) > 0.000001f)
		if ( (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodScale) || (rProductPart.m_foldingParams.m_nShinglingMethod == CBookblock::ShinglingMethodBottlingScale) ) 
		{
			float fScale = pPageTemplate->CalcShinglingScaleY();
			if (fabs(fScale) > 0.000001)
			{
				CString strNum, string;
				TruncateNumber(strNum, fScale * 100.0f);
				string.Format(_T(" | %s %%"), strNum);
				strShinglingOffset += string;
			}
		}

	if ( ! strShinglingOffset.IsEmpty())
	{
		CRect rcRect = pageRect; pDC->LPtoDP(rcRect);
		if ( (rcRect.Width() < 50) || (rcRect.Height() < 50) )	// don't draw if page is too small
			return;

		CSize fontSize(0, 13);
		pDC->DPtoLP(&fontSize);
		CFont font;
		font.CreateFont(fontSize.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));	
		CFont* pOldFont = pDC->SelectObject(&font);

		pageRect.NormalizeRect();

		CSize textExtent = pDC->GetTextExtent(strShinglingOffset);
		int nXPos = pageRect.CenterPoint().x - textExtent.cx/2;				  
		int nYPos = pageRect.CenterPoint().y + textExtent.cy/2; 

		COLORREF crColor = RGB(220,220,255);
		CRect textRect(nXPos, nYPos, nXPos + textExtent.cx, nYPos - textExtent.cy);

		int	  nShinglingMethod = rProductPart.m_foldingParams.m_nShinglingMethod;
		int	  nDirection = TOP;
		CSize szOffset;
		int	  nQ = ( (nShinglingMethod == CBookblock::ShinglingMethodBottling) || (nShinglingMethod == CBookblock::ShinglingMethodBottlingScale) ) ? 3 : 4;
		switch (nHeadPosition)
		{
		case RIGHT:	 nDirection = (fShinglingOffset < 0.0f) ? LEFT   : RIGHT;	(fShinglingOffset < 0.0f) ? szOffset = CSize(-pageRect.Width()/nQ, 0)  : szOffset = CSize( pageRect.Width()/nQ, 0);  break;
		case LEFT:	 nDirection = (fShinglingOffset < 0.0f) ? RIGHT  : LEFT;	(fShinglingOffset < 0.0f) ? szOffset = CSize( pageRect.Width()/nQ, 0)  : szOffset = CSize(-pageRect.Width()/nQ, 0);  break;
		case BOTTOM: nDirection = (fShinglingOffset < 0.0f) ? TOP    : BOTTOM;	(fShinglingOffset < 0.0f) ? szOffset = CSize(0,  pageRect.Height()/nQ) : szOffset = CSize(0, -pageRect.Height()/nQ); break;
		case TOP:	 nDirection = (fShinglingOffset < 0.0f) ? BOTTOM : TOP;		(fShinglingOffset < 0.0f) ? szOffset = CSize(0, -pageRect.Height()/nQ) : szOffset = CSize(0,  pageRect.Height()/nQ); break;
		}

		CRect rcArrow(pageRect.CenterPoint() + szOffset, pageRect.CenterPoint() + szOffset);
		rcArrow.NormalizeRect();
		textRect.NormalizeRect();

		CSize szSize(pageRect.Height()/30, pageRect.Height()/30);
		pDC->LPtoDP(&szSize); szSize = CSize(min(5, szSize.cx), min(5, szSize.cy));
		pDC->DPtoLP(&szSize);
		rcArrow.InflateRect(szSize);
		if ( (nShinglingMethod == CBookblock::ShinglingMethodScale) || (nShinglingMethod == CBookblock::ShinglingMethodBottlingScale) )
		{
			switch (nHeadPosition)
			{
			case TOP:		
			case BOTTOM:	rcArrow.OffsetRect(0, (nDirection == BOTTOM) ? pageRect.bottom - rcArrow.bottom : -(rcArrow.top  - pageRect.top));		break;
			case LEFT:		
			case RIGHT:		rcArrow.OffsetRect(	  (nDirection == LEFT)	 ? pageRect.right  - rcArrow.right  : -(rcArrow.left - pageRect.left), 0);	break;
			}
		}

		CPoint hotSpot = rcArrow.CenterPoint();
		//if (fabs(fShinglingOffset) > 0.009f)
		{
			int nWidth  = textRect.Width(); 
			int nHeight = textRect.Height();
			switch (nDirection)
			{
			case LEFT:		hotSpot.x = rcArrow.left;				hotSpot.y = textRect.CenterPoint().y;	textRect.right  = hotSpot.x - szSize.cx/2; textRect.left   = textRect.right  - nWidth;	break;
			case RIGHT:		hotSpot.x = rcArrow.right;				hotSpot.y = textRect.CenterPoint().y;	textRect.left   = hotSpot.x + szSize.cx/2; textRect.right  = textRect.left   + nWidth;	break;
			case BOTTOM:	hotSpot.x = textRect.CenterPoint().x;	hotSpot.y = rcArrow.top;				textRect.bottom = hotSpot.y - szSize.cy/2; textRect.top    = textRect.bottom - nHeight;	break;
			case TOP:		hotSpot.x = textRect.CenterPoint().x;	hotSpot.y = rcArrow.bottom;				textRect.top    = hotSpot.y + szSize.cy/2; textRect.bottom = textRect.top    + nHeight;	break;
			}
		}

		crColor = RGB(220,150,0);
		if (pDisplayList)
		{
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CPrintSheetView)))
			{
				float* pAuxData = &pPageTemplate->m_fShinglingOffsetY;
				pDisplayList->RegisterItem(pDC, textRect, pageRect, (void*)pPageTemplate, DISPLAY_ITEM_REGISTRY(CPrintSheetView, ShinglingValueY), pAuxData, CDisplayItem::ForceRegister); 
			}
			else
			if (pDisplayList->m_pParent->IsKindOf(RUNTIME_CLASS(CBookBlockProdDataView)))
			{
				float* pAuxData = &pPageTemplate->m_fShinglingOffsetY;
				pDisplayList->RegisterItem(pDC, textRect, pageRect, (void*)pPageTemplate, DISPLAY_ITEM_REGISTRY(CBookBlockProdDataView, ShinglingValueY), pAuxData, CDisplayItem::ForceRegister); 
			}
		}
			
		if (fabs(fShinglingOffset) > 0.009f)
		{
			pDC->LPtoDP(&hotSpot);
			pDC->LPtoDP(&szSize);
			DrawArrow(pDC, nDirection, hotSpot, 2 * szSize.cx, BLACK, crColor, TRUE, CPoint(0,0), szSize.cx);
		}

		pDC->TextOut(textRect.left, textRect.bottom, strShinglingOffset);

		pDC->SelectObject(pOldFont);
		font.DeleteObject();
	}
}

void CPrintSheetView::DrawFanoutOffsets(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	if ( ! ShowExposures() || ShowBitmap() || MarksViewActive())
		return;

	CPlate*		 pPlate		 = pActPrintSheet->GetTopmostPlate(nActLayoutSide);
	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActPrintSheet->GetLayout()->m_FrontSide : &pActPrintSheet->GetLayout()->m_BackSide;
	if (!pLayoutSide->m_bAdjustFanout)
		return;
	if (!pPlate)
		return;
	if ( (pLayoutSide->m_FanoutList.GetFanoutX(pPlate->m_strPlateName) == 0.0f) && (pLayoutSide->m_FanoutList.GetFanoutY(pPlate->m_strPlateName) == 0.0f) )
		return;

	CFont*	 pOldFont		= (CFont*)pDC->SelectStockObject(ANSI_VAR_FONT);
	COLORREF crOldTextColor = pDC->SetTextColor(BLACK);
	COLORREF crOldBkColor	= pDC->SetBkColor  (WHITE);
	int		 nOldBkMode		= pDC->SetBkMode   (OPAQUE);
	CRect	 exposureRect;

	POSITION pos = pPlate->m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure&	   rExposure = pPlate->m_ExposureSequence.GetNext(pos);
		CLayoutObject* pObject	 = rExposure.GetLayoutObject(pActPrintSheet->m_nLayoutIndex, nActLayoutSide);
		if ( ! pObject)
			continue;

		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPlate->GetPrintSheet());
		if (pObjectTemplate)
		{
			float fLeft			 = rExposure.GetSheetBleedBox(LEFT,   pObject, pObjectTemplate);
			float fTop			 = rExposure.GetSheetBleedBox(TOP,    pObject, pObjectTemplate);
			float fRight		 = rExposure.GetSheetBleedBox(RIGHT,  pObject, pObjectTemplate);
			float fBottom		 = rExposure.GetSheetBleedBox(BOTTOM, pObject, pObjectTemplate);
			float fFanoutOffsetX = pLayoutSide->GetFanoutOffsetX(pPlate->m_strPlateName, fLeft   + (fRight - fLeft)   / 2.0f);
			float fFanoutOffsetY = pLayoutSide->GetFanoutOffsetY(pPlate->m_strPlateName, fBottom + (fTop   - fBottom) / 2.0f);

			if ( (fFanoutOffsetX == 0.0f) && (fFanoutOffsetY == 0.0f) )
				continue;

			exposureRect.left	= WorldToLP(fLeft)  ;
			exposureRect.top	= WorldToLP(fTop)	;
			exposureRect.right	= WorldToLP(fRight)	;
			exposureRect.bottom	= WorldToLP(fBottom);

			if (pDC->RectVisible(exposureRect))
			{
				CString strString;

				if (fFanoutOffsetX != 0.0f)
				{
					TruncateNumber(strString, (float)fabs(fFanoutOffsetX));

					CSize textExtent = pDC->GetTextExtent(strString);
					int nXPos = exposureRect.CenterPoint().x - textExtent.cx/2;				  
					int nYPos = exposureRect.CenterPoint().y + textExtent.cy/2; 
					pDC->TextOut(nXPos, nYPos, strString);
					CRect textRect(nXPos, nYPos, nXPos + textExtent.cx + pDC->GetTextExtent(_T("X")).cx, nYPos - textExtent.cy);

					int	   nDirection = TOP;
					CPoint hotSpot	  = exposureRect.CenterPoint();
					pDC->LPtoDP(&hotSpot, 1);	
					nDirection		  = (fFanoutOffsetX < 0.0f) ? LEFT   : RIGHT;  
					hotSpot			 += CPoint((nDirection == LEFT) ? -4 : 4, 12);	
					DrawArrow(pDC, nDirection, hotSpot, 8, BLACK, LIGHTRED);
				}

				if (fFanoutOffsetY != 0.0f)
				{
					TruncateNumber(strString, (float)fabs(fFanoutOffsetY));

					CSize textExtent = pDC->GetTextExtent(strString);
					int nXPos = exposureRect.CenterPoint().x - textExtent.cx/2;				  
					int nYPos = exposureRect.CenterPoint().y + 5 * textExtent.cy/2; 
					pDC->TextOut(nXPos, nYPos, strString);
					CRect textRect(nXPos, nYPos, nXPos + textExtent.cx + pDC->GetTextExtent(_T("X")).cx, nYPos - textExtent.cy);

					int	   nDirection = TOP;
					CPoint hotSpot	  = exposureRect.CenterPoint();
					pDC->LPtoDP(&hotSpot, 1);	
					nDirection = (fFanoutOffsetY < 0.0f) ? BOTTOM : TOP;		
					hotSpot	  -= CPoint(2, (nDirection == TOP) ? 20 : 12);	
					DrawArrow(pDC, nDirection, hotSpot, 8, BLACK, LIGHTRED);
				}
			}
		}
	}

	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldTextColor);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetBkMode(nOldBkMode);
}

void CPrintSheetView::ResolvePlaceholders(TCHAR *pszDest, const TCHAR *pszSrc, CPrintSheet* pActPrintSheet, int nSide, CPlate* pPlate, int nTile, CString strFoldSheetNum, int nFoldSheetNumCAG, int nLayerNum, CFoldSheet* pFoldSheet)
{
	TCHAR *pszEnd, *pszStart, szSheettext[128];

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
	{
		_tcscpy(pszDest, pszSrc);
		return;
	}

	CLayout* pLayout = (pActPrintSheet) ? pActPrintSheet->GetLayout() : NULL;

	CString		 strPlateName	= (pPlate)				? pPlate->m_strPlateName	  : "";
	CLayoutSide* pLayoutSide	= NULL;
	if (pLayout)
		pLayoutSide = (nSide == FRONTSIDE)	? &pLayout->m_FrontSide : ((nSide == BACKSIDE)	? &pLayout->m_BackSide : NULL);
	else
		pLayoutSide = NULL;
	CString		  strSheetNumber = (pActPrintSheet)		? pActPrintSheet->GetNumber() : "";
	int			  nColorIndex	 = GetColorNumber(pPlate);
	CString		  strJobName	 = pDoc->m_GlobalData.m_strJob;//theApp.RemoveFileExtension(pDoc->GetTitle(), _T(".job") ); 
	CString		  strSideName	 = "";
	CString		  strWorkStyle	 = "";
	int			  nComponentRefIndex = 0;
	CPressDevice* pPressDevice   = NULL;
	int			  nSpotColorNum  = 0;
	if (pLayout)
	{
		switch (pLayout->m_nProductType)
		{
		case WORK_AND_TURN	 :	strSideName = theApp.settings.m_szWorkAndTurnName;				break;
		case WORK_AND_TUMBLE :	strSideName = theApp.settings.m_szWorkAndTumbleName;			break;
		case WORK_SINGLE_SIDE:	strSideName = theApp.settings.m_szSingleSidedName;				break;
		default				 :	strSideName = (pLayoutSide) ? pLayoutSide->m_strSideName : "";	break;
		}
		switch (pLayout->m_nProductType)
		{
		case WORK_AND_BACK	 :	strWorkStyle.LoadString(IDS_WORK_AND_BACK);						break;
		case WORK_AND_TURN	 :	strWorkStyle = theApp.settings.m_szWorkAndTurnName;				break;
		case WORK_AND_TUMBLE :	strWorkStyle = theApp.settings.m_szWorkAndTumbleName;			break;
		case WORK_SINGLE_SIDE:	strWorkStyle = theApp.settings.m_szSingleSidedName;				break;
		}
		pPressDevice  = (pLayoutSide) ? pLayoutSide->GetPressDevice() : pLayout->m_FrontSide.GetPressDevice();
		nSpotColorNum = pDoc->m_ColorDefinitionTable.GetCurSpotColorNum(strPlateName);
	}

	CString	strPaperGrain	 = _T("");
	CString strPaperType	 = _T("");
	CString strPaperGrammage = _T("");
	if (pLayout)
	{
		if (pLayoutSide)
			strPaperGrain.LoadString((pLayoutSide->m_nPaperGrain == GRAIN_VERTICAL) ? IDS_PAPERGRAIN_LONG : IDS_PAPERGRAIN_SHORT);
		else
			strPaperGrain.LoadString((pLayout->m_FrontSide.m_nPaperGrain == GRAIN_VERTICAL) ? IDS_PAPERGRAIN_LONG : IDS_PAPERGRAIN_SHORT);
		CPrintGroup& rPrintGroup = pLayout->GetPrintGroup();
		if (rPrintGroup.m_nPaperType > 0)
			strPaperType.Format(_T("%d"), rPrintGroup.m_nPaperType);
		if (rPrintGroup.m_nPaperGrammage > 0)
			strPaperGrammage.Format(_T("%d"), rPrintGroup.m_nPaperGrammage);
	}

	_tcscpy(pszDest, _T(""));

#ifdef CTP
	strForIPU = pszDest;
#endif

	pszStart = (TCHAR*)pszSrc;

	while ((pszEnd = _tcschr(pszStart, '$')) != NULL)
	{
		*pszEnd = '\0';
		_tcscat(pszDest, pszStart);
		switch (*(pszEnd + 1))
		{
			case '1': if ((*(pszEnd + 2) == '_') && isdigit(*(pszEnd + 3)))
					  {
						  pFoldSheet = GetFoldSheetFromPlaceholder(&pszStart, &pszEnd, pActPrintSheet, &nComponentRefIndex);
						  BOOL bComingAndGoing = (pFoldSheet) ? pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing : FALSE;
						  if (bComingAndGoing)
							_stprintf(pszDest, _T("%s%d"), pszDest, (pFoldSheet) ? pFoldSheet->GetNumberOnly() : 0);
						  else
							_stprintf(pszDest, _T("%s%s"), pszDest, (pFoldSheet) ? pFoldSheet->GetNumber() : "");
					  }
					  else
					  {
						  pFoldSheet = ( ! pFoldSheet && pActPrintSheet) ? pActPrintSheet->GetFoldSheet(nComponentRefIndex) : pFoldSheet;
						  BOOL bComingAndGoing = (pFoldSheet) ? pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing : FALSE;
						  if (bComingAndGoing)
						  {
							  pFoldSheet = ( ! pFoldSheet) ? GetFoldSheetFromPlaceholder(&pszStart, &pszEnd, pActPrintSheet, &nComponentRefIndex) : pFoldSheet;
							  _stprintf(pszDest, _T("%s%d"), pszDest, (pFoldSheet) ? pFoldSheet->GetNumberOnly() : 0);
						  }
						  else
							  _stprintf(pszDest, _T("%s%s"), pszDest, (pFoldSheet) ? pFoldSheet->GetNumber() : strFoldSheetNum);
						  pszStart = pszEnd + 2;
					  }
					  break;
			case '2': if ( (*(pszEnd + 2) == '_') && (*(pszEnd + 3) == 'S') )
					  { 
						 if (nSpotColorNum >= 0)
							_stprintf(pszDest, _T("%sS%d"), pszDest, nSpotColorNum);	
						 else
							_stprintf(pszDest, _T("%s%s"),  pszDest, strPlateName);	
						 pszStart = pszEnd + 4; 
					  }
					  else
						  if ((*(pszEnd + 2) == '_') && (*(pszEnd + 3) == '='))		// process colors to be stacked instead of side by side, makes no difference here
						  {
							 _stprintf(pszDest, _T("%s%s"), pszDest, strPlateName);
							 pszStart = pszEnd + 4; 
						  }
						  else
							 _stprintf(pszDest, _T("%s%s"), pszDest, TruncatePlaceholder(&pszStart, &pszEnd, strPlateName));
					  break;
			case '3': _stprintf(pszDest, _T("%s%s"), pszDest, TruncatePlaceholder(&pszStart, &pszEnd, strSideName));
					  break;
			case '4': if ((*(pszEnd + 2) == '_') && isdigit(*(pszEnd + 3)))
					  {
						  GetFoldSheetFromPlaceholder(&pszStart, &pszEnd, pActPrintSheet, &nComponentRefIndex);
						  if (pActPrintSheet)
							  if ( (nComponentRefIndex >= 0) && (nComponentRefIndex < pActPrintSheet->m_FoldSheetLayerRefs.GetSize()) )
								  _stprintf(pszDest, "%s%d", pszDest, pActPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex + 1);
							  else
								_stprintf(pszDest, "%s%d", pszDest, 0);
						  else
							_stprintf(pszDest, "%s%d", pszDest, 0);
					  }
					  else
					  {
						  _stprintf(pszDest, "%s%d", pszDest, nLayerNum);
						  pszStart = pszEnd + 2;
					  }
					  break;
			case '5': _stprintf(pszDest, "%s%d", pszDest, nColorIndex);
					  pszStart = pszEnd + 2; break;
			case '6': if ((*(pszEnd + 2) == '_') && isdigit(*(pszEnd + 3)))
					  {
						  pFoldSheet = GetFoldSheetFromPlaceholder(&pszStart, &pszEnd, pActPrintSheet, &nComponentRefIndex);
						  int		  nNum		 = pDoc->m_Bookblock.m_FoldSheetList.GetCount() - ((pFoldSheet) ? (pFoldSheet->GetNumberOnly() + 1) : 0);
						  _stprintf(pszDest, "%s%d", pszDest, nNum);
					  }
					  else
					  {
						  _stprintf(pszDest, "%s%d", pszDest, nFoldSheetNumCAG);
						  pszStart = pszEnd + 2;
					  }
					  break;
			case '7': _stprintf(pszDest, "%s%s", pszDest, TruncatePlaceholder(&pszStart, &pszEnd, strJobName));
					  break;
			case '8': if (nTile == -1)
						  _stprintf(pszDest, "%s%s", pszDest, _T(""));
					  else
						  _stprintf(pszDest, "%s%d", pszDest, nTile);	
					  pszStart = pszEnd + 2; break;
			case '9': _stprintf(pszDest, "%s%s", pszDest, strSheetNumber);
					  pszStart = pszEnd + 2; break;
			case 'A': ReadSheettextFromFile((pActPrintSheet) ? pActPrintSheet->m_nPrintSheetNumber : 0, szSheettext);
					  _stprintf(pszDest, "%s%s", pszDest, szSheettext);
					  pszStart = pszEnd + 2; break;
			case 'B': _stprintf(pszDest, "%s%03d", pszDest, (pActPrintSheet) ? pActPrintSheet->GetMinPagenum(nSide) : 0);
					  pszStart = pszEnd + 2; break;
			case 'C': _stprintf(pszDest, "%s%03d", pszDest, (pActPrintSheet) ? pActPrintSheet->GetMinPagenum(BOTHSIDES) : 0);
					  pszStart = pszEnd + 2; break;
			case 'D': _stprintf(pszDest, "%s%03d", pszDest, (pActPrintSheet) ? pActPrintSheet->GetMaxPagenum(nSide) : 0);
					  pszStart = pszEnd + 2; break;
			case 'E': _stprintf(pszDest, "%s%03d", pszDest, (pActPrintSheet) ? pActPrintSheet->GetMaxPagenum(BOTHSIDES) : 0);
					  pszStart = pszEnd + 2; break;
			case 'F': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, (pPressDevice) ? pPressDevice->m_strName : _T("")));
					  break;
			case 'G': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strJob));
					  break;
			case 'H': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strCustomer));
					  break;
			case 'I': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strCustomerNumber));
					  break;
			case 'J': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strCreator));
					  break;
			case 'K': _stprintf(pszDest, "%s%s", pszDest,	  ConvertToString(pDoc->m_GlobalData.m_CreationDate));
					  pszStart = pszEnd + 2; break;
			case 'L': _stprintf(pszDest, "%s%s", pszDest,	  ConvertToString(pDoc->m_GlobalData.m_LastModifiedDate));
					  pszStart = pszEnd + 2; break;
			case 'M': _stprintf(pszDest, "%s%s", pszDest,	  pDoc->m_GlobalData.m_strScheduledToPrint);			// print date
					  pszStart = pszEnd + 2; break;
			case 'N': _stprintf(pszDest, "%s%s", pszDest,	  ConvertToString(COleDateTime::GetCurrentTime()));		// output date
					  pszStart = pszEnd + 2; break;
			case 'O': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strNotes));	
					  break;
			case 'P': _stprintf(pszDest, "%s$",  pszDest);	// AsirCode only supported by ResolvePlaceholdersExt()
					  pszStart = pszEnd + 1; break;
			case 'Q': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strJobNumber));
					  break;
			case 'R': sprintf(pszDest, "%s%s", pszDest, (pActPrintSheet) ? pActPrintSheet->GetAllPagenums(nSide) : _T(""));
					  pszStart = pszEnd + 2; break;
			case 'V': sprintf(pszDest, "%s%s", pszDest, strPaperGrain);
					  pszStart = pszEnd + 2; break;
			case 'W': sprintf(pszDest, "%s%s", pszDest, strPaperType);
					  pszStart = pszEnd + 2; break;
			case 'X': sprintf(pszDest, "%s%s", pszDest, strPaperGrammage);
					  pszStart = pszEnd + 2; break;
			case 'Y': sprintf(pszDest, "%s%s", pszDest, strWorkStyle);
					  pszStart = pszEnd + 2; break;
			case '$': _stprintf(pszDest, "%s$",  pszDest);
					  pszStart = pszEnd + 2; break;
			default : _stprintf(pszDest, "%s$",  pszDest);
					  pszStart = pszEnd + 1; break;
		}

		*pszEnd = '$';

		pszDest+= _tcslen(pszDest);
	}

	_tcscat(pszDest, pszStart);

#ifdef CTP
	strForIPU[40] = '\0'; // For the IPU all text strings need to be truncated to 40 chars.
#endif

}

void CPrintSheetView::ResolvePlaceholdersExt(TCHAR *pszDest, const TCHAR *pszSrc, CPrintSheet* pActPrintSheet, CPlate* pPlate, int nTile, CString strColorName, CReflinePositionParams* pReflinePosParams)
{
	TCHAR *pszEnd, *pszStart, szSheettext[128];

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
	{
		_tcscpy(pszDest, pszSrc);
		return;
	}
	if (pActPrintSheet)
		if ( ! pActPrintSheet->GetLayout())
		{
			_tcscpy(pszDest, pszSrc);
			return;
		}

	int				nComponentRefIndex = 0;
	int				nLayoutObjectIndex = 0;
	int				nFoldSheetNumCAG, nLayerNum;
	CLayout*		pLayout			= (pActPrintSheet) ? pActPrintSheet->GetLayout() : NULL;
	CLayoutSide*	pLayoutSide		= (pPlate)		   ? pPlate->GetLayoutSide() : ((pLayout) ? &pLayout->m_FrontSide : NULL);
	CLayoutObject*	pLayoutObject	= (pLayoutSide)	   ? pLayoutSide->GetLayoutObjectByIndex(nLayoutObjectIndex) : NULL;
	CPageTemplate*	pPageTemplate	= (pLayoutObject)  ? pLayoutObject->GetCurrentPageTemplate(pActPrintSheet) : NULL;
	CFoldSheet*		pFoldSheet		= CPrintSheetView::GetMarkFoldSheetAndLayerNum(pActPrintSheet, nComponentRefIndex, &nFoldSheetNumCAG, &nLayerNum);

	CBoundProduct*		pBoundProduct = (pFoldSheet) ? &pFoldSheet->GetProductPart().GetBoundProduct() : NULL;
	CBoundProductPart*	pProductPart  = (pFoldSheet) ? &pFoldSheet->GetProductPart() : NULL;
	CFlatProduct*		pFlatProduct  = NULL;
	if (pReflinePosParams)
	{
		if (pReflinePosParams->m_nAnchor == PAGE_ANCHOR)
		{
			nLayoutObjectIndex = pReflinePosParams->m_nAnchorReference;
			pLayoutObject	   = (pLayoutSide)	 ? pLayoutSide->GetLayoutObjectByIndex(nLayoutObjectIndex) : NULL;
			pPageTemplate	   = (pLayoutObject) ? pLayoutObject->GetCurrentPageTemplate(pActPrintSheet) : NULL;
			if (pPageTemplate)
			{
				if (pPageTemplate->IsFlatProduct())
					pFlatProduct   = &pPageTemplate->GetFlatProduct();
				else
				{
					pFoldSheet	   = pPageTemplate->GetFoldSheet();
					pBoundProduct  = &pPageTemplate->GetBoundProduct(); 
					pProductPart   = &pPageTemplate->GetProductPart();
				}
			}
			else
			{
				pBoundProduct = NULL; pProductPart = NULL; pFoldSheet = NULL;
			}
		}
		else
		if (pReflinePosParams->m_nAnchor == FOLDSHEET_ANCHOR)
		{
			nComponentRefIndex = pReflinePosParams->m_nAnchorReference; 
			pFoldSheet		   = pActPrintSheet->GetFoldSheet(nComponentRefIndex);
			pBoundProduct	   = (pFoldSheet) ? &pFoldSheet->GetProductPart().GetBoundProduct() : NULL;
			pProductPart	   = (pFoldSheet) ? &pFoldSheet->GetProductPart() : NULL;
		}
	}

	CString			strPlateName	= (strColorName.IsEmpty()) ? ((pPlate) ? pPlate->m_strPlateName	: "") : strColorName;
	int				nSide			= (pLayoutSide)		? pLayoutSide->m_nLayoutSide  : 0;
	CString			strSheetNumber	= (pActPrintSheet)	? pActPrintSheet->GetNumber() : "";
	int				nColorIndex		= GetColorNumber(pPlate);
	CString			strJobName		= pDoc->m_GlobalData.m_strJob;//theApp.RemoveFileExtension(pDoc->GetTitle(), _T(".job") ); 
	CString			strFoldSheetNum	= (pFoldSheet) ? pFoldSheet->GetNumber() : "";
	CString			strFoldScheme	= (pFoldSheet) ? pFoldSheet->m_strFoldSheetTypeName : "";
	CString			strSideName		= "";
	CString			strWorkStyle	= "";
	int				nProductType	= (pActPrintSheet) ? pActPrintSheet->GetLayout()->m_nProductType : WORK_AND_BACK;
	switch (nProductType)
	{
	case WORK_AND_TURN	 : strSideName = theApp.settings.m_szWorkAndTurnName;			  break;
	case WORK_AND_TUMBLE : strSideName = theApp.settings.m_szWorkAndTumbleName;			  break;
	case WORK_SINGLE_SIDE: strSideName = theApp.settings.m_szSingleSidedName;			  break;
	default				 : strSideName = (pLayoutSide) ? pLayoutSide->m_strSideName : ""; break;
	}
	switch (nProductType)
	{
	case WORK_AND_BACK	 :	strWorkStyle.LoadString(IDS_WORK_AND_BACK);						break;
	case WORK_AND_TURN	 :	strWorkStyle = theApp.settings.m_szWorkAndTurnName;				break;
	case WORK_AND_TUMBLE :	strWorkStyle = theApp.settings.m_szWorkAndTumbleName;			break;
	case WORK_SINGLE_SIDE:	strWorkStyle = theApp.settings.m_szSingleSidedName;				break;
	}

	CPressDevice* pPressDevice	   = (pLayoutSide) ? pLayoutSide->GetPressDevice()  : NULL;
	int			  nSpotColorNum	   = pDoc->m_ColorDefinitionTable.GetCurSpotColorNum(strPlateName);

	CString	strPaperGrain	 = _T("");
	CString strPaperType	 = _T("");
	CString strPaperGrammage = _T("");
	if (pLayout)
	{
		if (pLayoutSide)
			strPaperGrain.LoadString((pLayoutSide->m_nPaperGrain == GRAIN_VERTICAL) ? IDS_PAPERGRAIN_LONG : IDS_PAPERGRAIN_SHORT);
		else
			strPaperGrain.LoadString((pLayout->m_FrontSide.m_nPaperGrain == GRAIN_VERTICAL) ? IDS_PAPERGRAIN_LONG : IDS_PAPERGRAIN_SHORT);
		CPrintGroup& rPrintGroup = pLayout->GetPrintGroup();
		if (rPrintGroup.m_nPaperType > 0)
			strPaperType.Format(_T("%d"), rPrintGroup.m_nPaperType);
		if (rPrintGroup.m_nPaperGrammage > 0)
			strPaperGrammage.Format(_T("%d"), rPrintGroup.m_nPaperGrammage);
	}

	_tcscpy(pszDest, _T(""));

#ifdef CTP
	strForIPU = pszDest;
#endif

	pszStart = (TCHAR*)pszSrc;

	while ((pszEnd = _tcschr(pszStart, '$')) != NULL)
	{
		*pszEnd = '\0';
		_tcscat(pszDest, pszStart);
		switch (*(pszEnd + 1))
		{
		case '1': if ((*(pszEnd + 2) == '_') && isdigit(*(pszEnd + 3)))
				  {
					  pFoldSheet = GetFoldSheetFromPlaceholder(&pszStart, &pszEnd, pActPrintSheet, &nComponentRefIndex);
					  BOOL bComingAndGoing = (pFoldSheet) ? pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing : FALSE;
					  if (bComingAndGoing)
						_stprintf(pszDest, "%s%d", pszDest, (pFoldSheet) ? pFoldSheet->GetNumberOnly() : 0);
					  else
						_stprintf(pszDest, "%s%s", pszDest, (pFoldSheet) ? pFoldSheet->GetNumber() : "");
				  }
				  else
				  {
					  BOOL bComingAndGoing = (pFoldSheet) ? pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing : FALSE;
					  if (bComingAndGoing)
					  {
						_stprintf(pszDest, "%s%d", pszDest, (pFoldSheet) ? pFoldSheet->GetNumberOnly() : 0);
						pszStart = pszEnd + 2;
					  }
					  else
					  {
						if ((*(pszEnd + 2) == '%') && isdigit(*(pszEnd + 3)))
							_stprintf(pszDest, "%s%s", pszDest, FieldWidthPlaceholder(&pszStart, &pszEnd, (pFoldSheet) ? pFoldSheet->GetNumberOnly() : 0));
						else
						{
							_stprintf(pszDest, "%s%s", pszDest, strFoldSheetNum);
							pszStart = pszEnd + 2;
						}
					  }
				  }
				  break;
		case '2': if ((*(pszEnd + 2) == '_') && (*(pszEnd + 3) == 'S'))
				  { 
					 if (nSpotColorNum >= 0)
						_stprintf(pszDest, "%sS%d", pszDest, nSpotColorNum);	
					 else
						_stprintf(pszDest, "%s%s",  pszDest, strPlateName);	
					pszStart = pszEnd + 4; 
				  }
				  else
					  if ((*(pszEnd + 2) == '_') && (*(pszEnd + 3) == '='))		// process colors to be stacked instead of side by side, makes no difference here
					  {
						_stprintf(pszDest, "%s%s", pszDest, strPlateName);
						pszStart = pszEnd + 4; 
					  }
					  else
						_stprintf(pszDest, "%s%s", pszDest, TruncatePlaceholder(&pszStart, &pszEnd, strPlateName));
				  break;
		case '3': _stprintf(pszDest, "%s%s", pszDest, TruncatePlaceholder(&pszStart, &pszEnd, strSideName));
				  break;
		case '4': if ((*(pszEnd + 2) == '_') && isdigit(*(pszEnd + 3)))
				  {
					  GetFoldSheetFromPlaceholder(&pszStart, &pszEnd, pActPrintSheet, &nComponentRefIndex);
					  if (pActPrintSheet)
						  if ( (nComponentRefIndex >= 0) && (nComponentRefIndex < pActPrintSheet->m_FoldSheetLayerRefs.GetSize()) )
							  _stprintf(pszDest, "%s%d", pszDest, pActPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex + 1);
						  else
							  _stprintf(pszDest, "%s%d", pszDest, 0);
					  else
						  _stprintf(pszDest, "%s%d", pszDest, 0);
				  }
				  else
				  {
					  _stprintf(pszDest, "%s%d", pszDest, nLayerNum);
					  pszStart = pszEnd + 2;
				  }
				  break;
		case '5': _stprintf(pszDest, "%s%d", pszDest, nColorIndex);
				  pszStart = pszEnd + 2; break;
		case '6': if ((*(pszEnd + 2) == '_') && isdigit(*(pszEnd + 3)))
				  {
					  CFoldSheet* pFoldSheet = GetFoldSheetFromPlaceholder(&pszStart, &pszEnd, pActPrintSheet, &nComponentRefIndex);
					  int		  nNum		 = pDoc->m_Bookblock.m_FoldSheetList.GetCount() - ((pFoldSheet) ? (pFoldSheet->GetNumberOnly() + 1) : 0);
					  _stprintf(pszDest, "%s%d", pszDest, nNum);
				  }
				  else
				  {
				      _stprintf(pszDest, "%s%d", pszDest, nFoldSheetNumCAG);
					  pszStart = pszEnd + 2;
				  }
				  break;
		case '7': _stprintf(pszDest, "%s%s", pszDest, TruncatePlaceholder(&pszStart, &pszEnd, strJobName));
				  break;
		case '8': if (nTile == -1)
					  _stprintf(pszDest, "%s%s", pszDest, _T(""));
				  else
					  _stprintf(pszDest, "%s%d", pszDest, nTile);	
					  pszStart = pszEnd + 2; break;
		case '9': _stprintf(pszDest, "%s%s", pszDest, strSheetNumber);
				  pszStart = pszEnd + 2; break;
		case 'A': ReadSheettextFromFile(pActPrintSheet->m_nPrintSheetNumber, szSheettext);
				  _stprintf(pszDest, "%s%s", pszDest, szSheettext);
				  pszStart = pszEnd + 2; break;
		case 'B': _stprintf(pszDest, "%s%03d", pszDest, (pActPrintSheet) ? pActPrintSheet->GetMinPagenum(nSide) : 0);
				  pszStart = pszEnd + 2; break;
		case 'C': _stprintf(pszDest, "%s%03d", pszDest, (pActPrintSheet) ? pActPrintSheet->GetMinPagenum(BOTHSIDES) : 0);
				  pszStart = pszEnd + 2; break;
		case 'D': _stprintf(pszDest, "%s%03d", pszDest, (pActPrintSheet) ? pActPrintSheet->GetMaxPagenum(nSide) : 0);
				  pszStart = pszEnd + 2; break;
		case 'E': _stprintf(pszDest, "%s%03d", pszDest, (pActPrintSheet) ? pActPrintSheet->GetMaxPagenum(BOTHSIDES) : 0);
				  pszStart = pszEnd + 2; break;
		case 'F': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, (pPressDevice) ? pPressDevice->m_strName : _T("")));
				  break;
		case 'G': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strJob));
				  break;
		case 'H': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strCustomer));
				  break;
		case 'I': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strCustomerNumber));
				  break;
		case 'J': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strCreator));
				  break;
		case 'K': _stprintf(pszDest, "%s%s", pszDest,	  ConvertToString(pDoc->m_GlobalData.m_CreationDate));
				  pszStart = pszEnd + 2; break;
		case 'L': _stprintf(pszDest, "%s%s", pszDest,	  ConvertToString(pDoc->m_GlobalData.m_LastModifiedDate));
				  pszStart = pszEnd + 2; break;
		case 'M': _stprintf(pszDest, "%s%s", pszDest,	  pDoc->m_GlobalData.m_strScheduledToPrint);			// print date
				  pszStart = pszEnd + 2; break;
		case 'N': _stprintf(pszDest, "%s%s", pszDest,	  ConvertToString(COleDateTime::GetCurrentTime()));		// output date
				  pszStart = pszEnd + 2; break;
		case 'O': _stprintf(pszDest, "%s%s", pszDest,	  TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strNotes));	
				  break;
		case 'P': if ((*(pszEnd + 2) == '_') && isdigit(*(pszEnd + 3)))
				  {
					  CFoldSheet* pFoldSheet = GetFoldSheetFromPlaceholder(&pszStart, &pszEnd, pActPrintSheet, &nComponentRefIndex);
					  _stprintf(pszDest, "%s%s", pszDest,	CreateAsirCode(pFoldSheet) );	
				  }
				  else
				      if ((*(pszEnd + 2) == '_') && (*(pszEnd + 3) == 'N'))
					  {
						  _stprintf(pszDest, "%s%s", pszDest,	CreateAsirCode(pFoldSheet, TRUE) );	
						  pszStart = pszEnd + 4; 
					  }
					  else
					  {
						  _stprintf(pszDest, "%s%s", pszDest,	CreateAsirCode(pFoldSheet) );	
						  pszStart = pszEnd + 2; 
					  }
				  break;
		case 'Q': if ((*(pszEnd + 2) == '%') && isdigit(*(pszEnd + 3)))
					_stprintf(pszDest, "%s%s", pszDest, FieldWidthPlaceholder(&pszStart, &pszEnd, atoi(pDoc->m_GlobalData.m_strJobNumber)));
				  else
					_stprintf(pszDest, "%s%s", pszDest,	TruncatePlaceholder(&pszStart, &pszEnd, pDoc->m_GlobalData.m_strJobNumber));
				  break;
		case 'R': sprintf(pszDest, "%s%s", pszDest, (pActPrintSheet) ? pActPrintSheet->GetAllPagenums(nSide) : _T(""));
				  pszStart = pszEnd + 2; break;
		case 'S': sprintf(pszDest, "%s%s", pszDest, (pPageTemplate)  ? pPageTemplate->m_strPageID : _T(""));
				  pszStart = pszEnd + 2; break;
		case 'T': sprintf(pszDest, "%s%s", pszDest, (pBoundProduct)  ? pBoundProduct->m_strProductName : ( (pFlatProduct) ? pFlatProduct->m_strProductName : _T("")) );
				  pszStart = pszEnd + 2; break;
		case 'U': sprintf(pszDest, "%s%s", pszDest, (pProductPart)   ? pProductPart->m_strName : _T(""));
				  pszStart = pszEnd + 2; break;
		case 'V': sprintf(pszDest, "%s%s", pszDest, strPaperGrain);
				  pszStart = pszEnd + 2; break;
		case 'W': sprintf(pszDest, "%s%s", pszDest, strPaperType);
				  pszStart = pszEnd + 2; break;
		case 'X': sprintf(pszDest, "%s%s", pszDest, strPaperGrammage);
				  pszStart = pszEnd + 2; break;
		case 'Y': sprintf(pszDest, "%s%s", pszDest, strWorkStyle);
				  pszStart = pszEnd + 2; break;
		case 'Z': sprintf(pszDest, "%s%s", pszDest, strFoldScheme);
				  pszStart = pszEnd + 2; break;
		case 'a': sprintf(pszDest, "%s%s", pszDest, FieldWidthPlaceholder(&pszStart, &pszEnd, (pFoldSheet) ? pDoc->m_Bookblock.GetNumFoldSheets(pFoldSheet->m_nProductPartIndex) : 0));
				  break;
		case '$': _stprintf(pszDest, "%s$",  pszDest);
				  pszStart = pszEnd + 2; break;
		default : _stprintf(pszDest, "%s$",  pszDest);
				  pszStart = pszEnd + 1; break;
		}

		*pszEnd = '$';

		pszDest+= _tcslen(pszDest);
	}

	_tcscat(pszDest, pszStart);
}

CString CPrintSheetView::CreateAsirCode(CFoldSheet* pFoldSheet, BOOL bOrigNums)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pFoldSheet)
		return _T("");

	CBoundProductPart& rProductPart = pFoldSheet->GetProductPart();
	CBoundProduct&	   rProduct		= pFoldSheet->GetBoundProduct();
	
	int nProdnum = (IsNumericString(rProduct.m_strProductName)) ? _ttoi(rProduct.m_strProductName) : _ttoi(pDoc->m_GlobalData.m_strJobNumber);
	int nb		 = pFoldSheet->m_nFoldSheetNumber;//_ttoi(pFoldSheet->GetNumber());

	if ( ! bOrigNums)	// not use original KIM numeration
	{ 
		if (rProductPart.IsCover())
			nb = 0;
		else
			if (rProduct.HasCover())
				nb--;
	}

	int nMaxb = (pDoc->m_Bookblock.GetNumFoldSheets(-rProduct.GetIndex() - 1) == pFoldSheet->m_nFoldSheetNumber) ? 1 : 0;	// is last fold sheet?
	
	int		nProdnum2	  = nProdnum * 2;
	int		nBarcodeValue = ( (nProdnum2 / 100 + nb) % 100) * 10000 + (nProdnum2 % 100 + nMaxb) * 100 + nb;
	CString strCode; strCode.Format(_T("%06d"), nBarcodeValue);

	return strCode;
}

int CPrintSheetView::GetColorNumber(CPlate* pPlate)
{
	if (! pPlate)
		return 0;

#ifdef CTP
	if (theApp.settings.m_bExtendedColorHandling)
#else
	if (1)
#endif
	{
		if (pPlate->GetPlateList())
			return pPlate->GetPlateList()->GetPlateIndex(pPlate);
		else
			return 0;
	}
	else
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return 0;
		else
			return pDoc->m_ColorDefinitionTable.GetColorIndex(pPlate->m_nColorDefinitionIndex);
	}
}

CString CPrintSheetView::ConvertToString(COleDateTime& rDate, BOOL bLongVersion, BOOL bYearWithCentury)
{
	if (rDate == 0)
		return _T("");

	CString strOut;

	if (theApp.settings.m_iLanguage == 0)
	{
		if (bYearWithCentury)
			strOut = (bLongVersion) ? rDate.Format(_T("%d.%m.%Y %H:%M")) : rDate.Format(_T("%d.%m.%Y"));
		else
			strOut = (bLongVersion) ? rDate.Format(_T("%d.%m.%y %H:%M")) : rDate.Format(_T("%d.%m.%y"));
	}
	else
	{
		if (bYearWithCentury)
			strOut = (bLongVersion) ? rDate.Format(_T("%m/%d/%Y %H:%M")) : rDate.Format(_T("%m/%d/%Y"));
		else
			strOut = (bLongVersion) ? rDate.Format(_T("%m/%d/%y %H:%M")) : rDate.Format(_T("%m/%d/%y"));
	}

	return strOut;
}

CFoldSheet* CPrintSheetView::GetMarkFoldSheetAndLayerNum(CPrintSheet* pPrintSheet, int nComponentRefIndex, int *nFoldSheetNumCAG, int *nLayerNum)
{
	if ( ! pPrintSheet)
		return NULL;
	if ( ! pPrintSheet->m_FoldSheetLayerRefs.GetSize())
		return NULL;
	if ( (nComponentRefIndex < 0) || (nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize()) )
		nComponentRefIndex = 0;  

	CImpManDoc*	pDoc		= CImpManDoc::GetDoc();
	int			nIndex		= pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;
	POSITION	pos			= pDoc->m_Bookblock.m_FoldSheetList.FindIndex(nIndex);
	CFoldSheet&	rFoldSheet	= pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);
	*nFoldSheetNumCAG		= pDoc->m_Bookblock.m_FoldSheetList.GetCount() - rFoldSheet.GetNumberOnly() + 1;
	*nLayerNum				= pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex + 1;

	return &rFoldSheet;
}


CFoldSheet* CPrintSheetView::GetFoldSheetFromPlaceholder(TCHAR **ppStart, TCHAR **ppEnd, CPrintSheet* pPrintSheet, int *nComponentRefIndex)
{
	TCHAR str[5];
	
	
	str[0] = *(*ppEnd + 3); str[1] = *(*ppEnd + 4); str[2] = '\0';
	if (!isdigit(*(*ppEnd + 4)))
	{
		str[1] = '\0';
		*ppStart = *ppEnd + 4;
	}
	else
		*ppStart = *ppEnd + 5;

	if ( ! pPrintSheet)
		return NULL;
	
	*nComponentRefIndex = _ttoi(str) - 1;
	if ((*nComponentRefIndex < 0) || (*nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize()))
		*nComponentRefIndex = 0;
	
	CImpManDoc*	pDoc		= CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if  ( (*nComponentRefIndex < 0) || (*nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize()) )
		return NULL;
	int			nIndex		= pPrintSheet->m_FoldSheetLayerRefs[*nComponentRefIndex].m_nFoldSheetIndex;
	POSITION	pos			= pDoc->m_Bookblock.m_FoldSheetList.FindIndex(nIndex);
	if ( ! pos)
		return NULL;
	CFoldSheet&	rFoldSheet	= pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);

	return &rFoldSheet;
}

CString CPrintSheetView::TruncatePlaceholder(TCHAR **ppStart, TCHAR **ppEnd, CString strValue)
{
	if ((*(*ppEnd + 2) == ':') && isdigit(*(*ppEnd + 3)))
	{
		TCHAR str[6];
			
		str[0] = *(*ppEnd + 3); str[1] = *(*ppEnd + 4); str[2] = *(*ppEnd + 5); str[3] = '\0';
		if (!isdigit(*(*ppEnd + 4)))
		{
			str[1] = '\0';
			*ppStart = *ppEnd + 4;
		}
		else
		if (!isdigit(*(*ppEnd + 5)))
		{
			str[2] = '\0';
			*ppStart = *ppEnd + 5;
		}
		else
			*ppStart = *ppEnd + 6;

		int nMaxLen = _ttoi(str);
		return strValue.Left(nMaxLen);
	}
	else
	{
		*ppStart = *ppEnd + 2; 
		return strValue;
	}	
}

CString CPrintSheetView::FieldWidthPlaceholder(TCHAR **ppStart, TCHAR **ppEnd, int nValue)
{
	CString strValue;
	if ((*(*ppEnd + 2) == '%') && isdigit(*(*ppEnd + 3)))
	{
		TCHAR str[6];
			
		str[0] = *(*ppEnd + 3); str[1] = *(*ppEnd + 4); str[2] = *(*ppEnd + 5); str[3] = '\0';
		if (!isdigit(*(*ppEnd + 4)))
		{
			str[1] = '\0';
			*ppStart = *ppEnd + 4;
		}
		else
		if (!isdigit(*(*ppEnd + 5)))
		{
			str[2] = '\0';
			*ppStart = *ppEnd + 5;
		}
		else
			*ppStart = *ppEnd + 6;

		CString strCtrl = CString("%") + str +  CString("d");
		strValue.Format(strCtrl, nValue);
		return strValue;
	}
	else
	{
		*ppStart = *ppEnd + 2; 
		strValue.Format(_T("%d"), nValue);
		return strValue;
	}	
}

void CPrintSheetView::ReadSheettextFromFile(int nPrintSheetNumber, TCHAR* szText)
{
	FILE	*fp;
	TCHAR	szString[128] = _T(""), *poi;
	CString	strJobName	= theApp.RemoveFileExtension(CImpManDoc::GetDoc()->GetTitle(), _T(".job") ); 
	CString strPath		= CString(theApp.settings.m_szJobsDir) + CString("\\") + strJobName + CString(".IMP");
	
	if ( (fp = _tfopen((LPCTSTR)strPath, _T("rt"))) == NULL)
		return;
	
	_tcscpy(szText, _T(""));
	
	while( 1 )
	{
		if( _fgetts(szString, 127, fp) == NULL )
		{
			if( feof(fp) )
				break;
		}
		
		if ( (poi = _tcschr(szString, ':')) != NULL )
		{
			*poi = '\0';
			if (_ttoi(szString) == nPrintSheetNumber)
			{
				_tcscpy(szText, poi + 1);
				if ( (poi = _tcsrchr(szText, '\n')) != NULL )	// remove newline 
					*poi = '\0';
				
				break;
			}
		}
	}
	
	fclose(fp);
}

// Calculates the rectangle where the object's pageID is located (in device units)
// Used for click into test
// If requested calculates also the required rectangle for a InplaceEdit 
CRect CPrintSheetView::CalcPageIDRect(CRect pageRect, CString strPageID, int nHeadPosition, int nIDPosition, int* pnIDCorner, CRect* pEditRect)
{
	// Get text extent of strPageID
	CDC*   pDC = GetDC();
	CFont  pageIDFont;
	CLayoutObject::CreatePageIDFont(pDC, &pageIDFont, nHeadPosition, FALSE, 13);
	CFont* pOldFont  = pDC->SelectObject(&pageIDFont);
	CSize  textExtent = pDC->GetTextExtent(strPageID + _T("  "));	
	pDC->SelectObject(pOldFont);
	pageIDFont.DeleteObject();
	ReleaseDC(pDC);
	if ((nHeadPosition == LEFT) || (nHeadPosition == RIGHT))	// Flip textExtent
	{
		int temp = textExtent.cx; textExtent.cx = textExtent.cy; textExtent.cy = temp;
	}

	textExtent.cx = max(textExtent.cx, 14);
	textExtent.cy = max(textExtent.cy, 14);
	int nIDCorner = LEFT_BOTTOM;
	switch (nHeadPosition)
	{
	case TOP:	nIDCorner = (nIDPosition == LEFT) ? LEFT_BOTTOM : RIGHT_BOTTOM; break;
	case BOTTOM:nIDCorner = (nIDPosition == LEFT) ? RIGHT_TOP   : LEFT_TOP;		break;
	case LEFT:	nIDCorner = (nIDPosition == LEFT) ? RIGHT_BOTTOM: RIGHT_TOP;	break;
	case RIGHT:	nIDCorner = (nIDPosition == LEFT) ? LEFT_TOP    : LEFT_BOTTOM;  break;	
	}

	pageRect.DeflateRect(3, 3);	// IDs has space between text and border
	CRect rectID;
	switch (nIDCorner)
	{
	case LEFT_BOTTOM:	rectID.left	 = pageRect.left;				 rectID.bottom	= pageRect.bottom;
						rectID.right = rectID.left  + textExtent.cx; rectID.top		= rectID.bottom - textExtent.cy;
						break;
	case LEFT_TOP:		rectID.left	 = pageRect.left;				 rectID.top		= pageRect.top;
						rectID.right = rectID.left  + textExtent.cx; rectID.bottom	= rectID.top	+ textExtent.cy;
						break;
	case RIGHT_TOP	:	rectID.right = pageRect.right;				 rectID.top		= pageRect.top;
						rectID.left  = rectID.right - textExtent.cx; rectID.bottom  = rectID.top	+ textExtent.cy;
						break;
	case RIGHT_BOTTOM:	rectID.right = pageRect.right;				 rectID.bottom	= pageRect.bottom;
						rectID.left  = rectID.right - textExtent.cx; rectID.top		= rectID.bottom - textExtent.cy;
						break;
	}

	if (pnIDCorner)
		*pnIDCorner = nIDCorner;

	if (pEditRect)
	{
		*pEditRect = rectID;
		pEditRect->InflateRect(2, 2);
		int nHeight = pEditRect->Height(), nWidth = pEditRect->Width();
		if (nHeadPosition == LEFT)
		{
			pEditRect->left = pEditRect->right - nHeight; 
			if (nIDPosition == LEFT)
				pEditRect->top = pEditRect->bottom - nWidth; 
			else
				pEditRect->bottom = pEditRect->top + nWidth; 
		}

		if (nHeadPosition == RIGHT)
		{
			pEditRect->right  = pEditRect->left + nHeight;
			if (nIDPosition == RIGHT)
				pEditRect->top = pEditRect->bottom - nWidth; 
			else
				pEditRect->bottom = pEditRect->top + nWidth; 
		}
	}


	return rectID;
}

void CPrintSheetView::DrawFoldSheet(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet) 
{
	if ( ! ShowFoldSheet() ) // || MarksViewActive() )
		return;

	CFoldSheetLayerRef LayerRefBuf;
	POSITION		   pos;
	int				   numberFoldSheets, j;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	switch (nActLayoutSide)
	{
	case FRONTSIDE: numberFoldSheets = pActPrintSheet->m_FoldSheetLayerRefs.GetSize();
					for (j = 0; j < numberFoldSheets; j++)
					{
						LayerRefBuf = pActPrintSheet->m_FoldSheetLayerRefs.GetAt(j);
						if (pDoc->m_Bookblock.m_FoldSheetList.GetCount() > (LayerRefBuf.m_nFoldSheetIndex)) //Index starts with 0
						{
							pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(LayerRefBuf.m_nFoldSheetIndex);
							if (pos)
								DrawFoldSheetRect(pDC, FRONTSIDE, pActPrintSheet, j, &(pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos)));
						}
					}
					break;
	case BACKSIDE :	numberFoldSheets = pActPrintSheet->m_FoldSheetLayerRefs.GetSize();
					for (j = 0; j < numberFoldSheets; j++)
					{
						LayerRefBuf = pActPrintSheet->m_FoldSheetLayerRefs.GetAt(j);
						if (pDoc->m_Bookblock.m_FoldSheetList.GetCount() > (LayerRefBuf.m_nFoldSheetIndex)) //Index starts with 0
						{
							pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(LayerRefBuf.m_nFoldSheetIndex);
							if (pos)
								DrawFoldSheetRect(pDC, BACKSIDE, pActPrintSheet, j, &(pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos)));
						}
					}
					break;
	}
}

void CPrintSheetView::DrawFoldSheetRect(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, int nActFoldSheetRefIndex, CFoldSheet* pFoldSheet) 
{
	if ( ! pFoldSheet)
		return;

	CRect objectRect, foldsheetRect, deflateRect;
	BOOL  bDrawFoldsheet	 = FALSE;
	BOOL  bFirstLoop		 = TRUE;
	CImpManDoc*  pDoc		 = CImpManDoc::GetDoc();
	CLayout*	 pActLayout  = pActPrintSheet->GetLayout();
	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActLayout->m_FrontSide : &pActLayout->m_BackSide;

	BOOL bDisabled = FALSE;
	POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
			continue;
		if ( ! rObject.IsFlatProduct() && (rObject.m_nComponentRefIndex == nActFoldSheetRefIndex) )
		{
			if (rObject.m_bHidden)
				return;

			if (bFirstLoop)
			{
				foldsheetRect.left	 = WorldToLP(rObject.GetLeft());
				foldsheetRect.top	 = WorldToLP(rObject.GetTop());
				foldsheetRect.right  = WorldToLP(rObject.GetRight());
				foldsheetRect.bottom = WorldToLP(rObject.GetBottom());
				deflateRect = foldsheetRect;
				bFirstLoop = FALSE;
			}
			bDrawFoldsheet = TRUE;
			foldsheetRect.left	 = min(WorldToLP(rObject.GetLeft()),   foldsheetRect.left);
			foldsheetRect.bottom = min(WorldToLP(rObject.GetBottom()), foldsheetRect.bottom);
			foldsheetRect.right	 = max(WorldToLP(rObject.GetRight()),  foldsheetRect.right);
			foldsheetRect.top	 = max(WorldToLP(rObject.GetTop()),	   foldsheetRect.top);
			if (rObject.m_nStatus & CLayoutObject::Disabled)
				bDisabled = TRUE;
		}
	}

	if (bDrawFoldsheet)
	{
		BOOL  bMarksViewActive = MarksViewActive();
		CPen* pOldPen;
		CPen  refEdgePenOuterFoldSheet;
		CPen  refEdgePenInnerFoldSheet;
		CPen  foldsheetPen;
		BOOL  bInnerForm;
		COLORREF crPenColor;
		int		 nPenStyle;

		pDC->SelectStockObject(HOLLOW_BRUSH);	// Predefined brush with no filling of rectangle
		pOldPen = pDC->GetCurrentPen();
// TODO: How shall we draw the foldsheet-parts
		if (((nActLayoutSide == FRONTSIDE) && (pActLayout->m_FoldSheetLayerTurned[nActFoldSheetRefIndex])) ||
			((nActLayoutSide == BACKSIDE) && (!pActLayout->m_FoldSheetLayerTurned[nActFoldSheetRefIndex])))
		{
			nPenStyle = PS_DOT;
			pDC->SetBkColor(WHITE);
			if (m_nPrinterNumColors > 2)
				crPenColor = RGB(255, 180, 80);//LIGHTRED);
			else																										
				crPenColor = BLACK;

			bInnerForm = TRUE;
		}
		else
		{
			nPenStyle = PS_SOLID;
			if (m_nPrinterNumColors > 2)
				crPenColor = (bMarksViewActive) ? RGB(255, 200, 120) : RGB(255, 180, 80);//LIGHTRED : RED);	// if mark dlg open -> showing foldsheet indices is the main task.
			else																			// the rest will be displayed dimmed
				crPenColor = BLACK;						

			bInnerForm = FALSE;
		}
		BOOL bEnabled = TRUE;

		foldsheetPen.CreatePen(nPenStyle, Pixel2ConstMM(pDC, 1), (bDisabled) ? RGB(220,220,220) : crPenColor);

		pDC->SelectObject(&foldsheetPen);	
		
		foldsheetRect.left	 = foldsheetRect.left	+ ((deflateRect.right - deflateRect.left)  / 4);
		foldsheetRect.top	 = foldsheetRect.top	- ((deflateRect.top	  - deflateRect.bottom)/ 4);
		foldsheetRect.right	 = foldsheetRect.right	- ((deflateRect.right - deflateRect.left)  / 4);
		foldsheetRect.bottom = foldsheetRect.bottom + ((deflateRect.top	  - deflateRect.bottom)/ 4);

		if (pDC->RectVisible(foldsheetRect))
		{
			pDC->Rectangle(foldsheetRect);

			int			nIndex	   = pActPrintSheet->m_FoldSheetLayerRefs[nActFoldSheetRefIndex].m_nFoldSheetIndex;
			//POSITION	pos		   = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(nIndex);
			//CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);
			nIndex				   = pActPrintSheet->m_FoldSheetLayerRefs[nActFoldSheetRefIndex].m_nFoldSheetLayerIndex;

			int nFontSize = CLayout::TransformFontSize(pDC, 18);	// 18 mm when plotted 1:1
			CFont font, boldFont;
			font.CreateFont	   (nFontSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
			boldFont.CreateFont(nFontSize, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
			CFont* pOldFont = pDC->SelectObject(&boldFont);
			COLORREF oldColor;
			if (m_nPrinterNumColors > 2)
				oldColor = pDC->SetTextColor(RGB(200,100,0));//OCKER);	
			else
				oldColor = pDC->SetTextColor(BLACK);
			if ( ! bEnabled)
				pDC->SetTextColor(LIGHTGRAY);

			if (bDisabled)
				pDC->SetTextColor(RGB(220,220,220));

			CString strOut;
			if (pFoldSheet->m_LayerList.GetCount() > 1)
				strOut.Format(_T("%s (%d) "), pFoldSheet->GetNumber(), nIndex + 1); 
			else
				strOut.Format(_T("%s "), pFoldSheet->GetNumber()); 
			
			CRect textRect = foldsheetRect;
			textRect.DeflateRect(WorldToLP(10), WorldToLP(-10));

			pDC->SetBkMode(TRANSPARENT);
			pDC->DrawText(strOut, textRect, DT_SINGLELINE|DT_LEFT|DT_TOP);

			pDC->SetBkMode(OPAQUE);
			pDC->SetBkColor(RGB(255,240,225));
			pDC->SelectObject(&font);
			strOut.Format(_T("%s "), pFoldSheet->m_strFoldSheetTypeName); 
			pDC->SetTextColor(GUICOLOR_CAPTION);
			pDC->DrawText(strOut, textRect, DT_SINGLELINE|DT_LEFT|DT_BOTTOM);
			if (bMarksViewActive)
			{
				if (m_nPrinterNumColors > 2)
					pDC->SetTextColor(RED);	
				if (bDisabled)
					pDC->SetTextColor(RGB(220,220,220));
				strOut.Format(_T(" $*_%d "), nActFoldSheetRefIndex + 1);
				pDC->DrawText(strOut, textRect, DT_SINGLELINE|DT_RIGHT|DT_TOP);	// Index for Textmarks ($1_....)
			}
			
			CBoundProductPart& rProductPart = pFoldSheet->GetProductPart();
			pDC->SetTextColor(GUICOLOR_CAPTION);

			if (bDisabled)
				pDC->SetTextColor(RGB(220,220,220));

			textRect.bottom += pDC->GetTextExtent(_T("XXX")).cy;
			pDC->SelectObject(&boldFont);
			if (pDoc->m_boundProducts.GetSize() > 1)
			{
				strOut = rProductPart.GetBoundProduct().m_strProductName;
				pDC->DrawText(strOut, textRect, DT_LEFT | DT_SINGLELINE | DT_BOTTOM | DT_END_ELLIPSIS);
				textRect.left += pDC->GetTextExtent(_T("   ") + strOut).cx;
			}
	
			pDC->SetTextColor(RGB(40,120,30));	// green
	
			if (bDisabled)
				pDC->SetTextColor(RGB(220,220,220));

			strOut.Format(_T("%s"), rProductPart.m_strName);
			pDC->DrawText(strOut, textRect, DT_LEFT | DT_SINGLELINE | DT_BOTTOM | DT_END_ELLIPSIS);

			pDC->SelectObject(pOldFont);
			pDC->SetTextColor(oldColor);
			font.DeleteObject();
			boldFont.DeleteObject();
		}
		pDC->SelectObject(pOldPen);
		foldsheetPen.DeleteObject();
// TODO: How shall we draw the foldsheet-parts
		CSize szEdge(3,3);
		pDC->DPtoLP(&szEdge);
		if (((nActLayoutSide == FRONTSIDE) && (pActLayout->m_FoldSheetLayerTurned[nActFoldSheetRefIndex])) ||
			((nActLayoutSide == BACKSIDE) && (!pActLayout->m_FoldSheetLayerTurned[nActFoldSheetRefIndex])))
		{
			if ( ! bEnabled)
				refEdgePenInnerFoldSheet.CreatePen(PS_DOT, szEdge.cx, (bDisabled) ? RGB(220,220,220) : LIGHTGRAY);
			else
				if (m_nPrinterNumColors > 2)
					refEdgePenInnerFoldSheet.CreatePen(PS_DOT, szEdge.cx, (bDisabled) ? RGB(220,220,220) : ((bMarksViewActive || bInnerForm) ? RGB(255, 200, 120) : RGB(255, 180, 80)));
				else
					refEdgePenInnerFoldSheet.CreatePen(PS_DOT, szEdge.cx, (bDisabled) ? RGB(220,220,220) : BLACK);
			pDC->SelectObject(&refEdgePenInnerFoldSheet);
		}
		else
		{
			if ( ! bEnabled)
				refEdgePenOuterFoldSheet.CreatePen(PS_SOLID, szEdge.cx, (bDisabled) ? RGB(220,220,220) : LIGHTGRAY);
			else
				if (m_nPrinterNumColors > 2)
					refEdgePenOuterFoldSheet.CreatePen(PS_SOLID, szEdge.cx, (bDisabled) ? RGB(220,220,220) : ((bMarksViewActive || bInnerForm) ? RGB(255, 200, 120) : RGB(255, 180, 80)));
				else
					refEdgePenOuterFoldSheet.CreatePen(PS_SOLID, szEdge.cx, (bDisabled) ? RGB(220,220,220) : BLACK);
			pDC->SelectObject(&refEdgePenOuterFoldSheet);
		}

		DrawEdge(pDC, &foldsheetRect, pFoldSheet->m_nFoldSheetRefEdge, pActLayout, pActPrintSheet, nActFoldSheetRefIndex, FOLDSHEET, nActLayoutSide, &m_DisplayList, ShowFoldSheet());
		//DrawFoldSheetSectionBar(pDC, pActPrintSheet, nActFoldSheetRefIndex, nActLayoutSide);

		CRect auxRect = foldsheetRect;
		if (bInnerForm) 
			auxRect.left = auxRect.right = 0;	// trick: use display items auxrect width to indicate if it is the inner form
		if ((nActLayoutSide == BACKSIDE))		// - used by CopyAndPasteFoldSheet()
			auxRect.bottom = auxRect.top = 0;	// trick: use display items auxrect height to indicate if foldsheet is on layout backside 
												// - used by CopyAndPasteFoldSheet()
		CLayoutObject* pCutBlock = pLayoutSide->GetFoldSheetCutBlock(nActFoldSheetRefIndex);
		CRect auxAuxRect;
		if (pCutBlock)
		{
			float fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight; 
			pCutBlock->GetCurrentGrossCutBlock(pActPrintSheet, fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight, FALSE);
			auxAuxRect.left	  = WorldToLP(fCropBoxX);
			auxAuxRect.top	  = WorldToLP(fCropBoxY);
			auxAuxRect.right  = WorldToLP(fCropBoxX + fCropBoxWidth);
			auxAuxRect.bottom = WorldToLP(fCropBoxY + fCropBoxHeight);
		}
		CFoldSheetLayer* pLayer = pActPrintSheet->GetFoldSheetLayer(nActFoldSheetRefIndex);
		BOOL bLocked = (pLayer) ? pLayer->m_bPagesLocked : FALSE;
		if ( ! MarksViewActive() && ! OutputViewActive() && bLocked)
		{
			int nItemType = CDisplayItem::ForceRegister | CDisplayItem::GroupMember;
			CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, (bLocked) ? auxAuxRect : foldsheetRect, auxRect, (void*)nActFoldSheetRefIndex, SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, FoldSheet), (void*)pActPrintSheet, nItemType, NULL, auxAuxRect); // register item into display list
			if (pDI)
			{
				pDI->SetHighlightColor(RGB(190,112,85));
				pDI->SetMouseOverFeedback();
				if (pDI->m_nState != CDisplayItem::Normal)
				{
					FillTransparent(pDC, auxAuxRect, RGB(230,218,205), TRUE, 80);
					int nHatchStyle = HS_FDIAGONAL;
					CBrush  brush(nHatchStyle, RGB(220,190,65));
					CBrush* pOldBrush  = pDC->SelectObject(&brush);
					CPen*	pOldPen	   = (CPen*)pDC->SelectStockObject(NULL_PEN);
					int		nOldBkMode = pDC->SetBkMode(TRANSPARENT);
					DrawRectangleExt(pDC, auxAuxRect);
					pDC->SelectObject(pOldBrush);
					pDC->SelectObject(pOldPen);
					pDC->SetBkMode(nOldBkMode);
					brush.DeleteObject();
				}
			}
		}

		pDC->SelectObject(pOldPen);
		refEdgePenOuterFoldSheet.DeleteObject();
		refEdgePenInnerFoldSheet.DeleteObject();
	}
}

void CPrintSheetView::DrawEdge(CDC* pDC, CRect* rect, int ntype, CLayout* pActLayout, CPrintSheet* pActPrintSheet, int nActFoldSheetRefIndex, int nTypeOfEdge, int nSide, CDisplayList* pDisplayList, BOOL bShowFoldSheet) // PaperRefEdge and FoldSheetRefEdge
{
	CPoint	p[4];
	CRect	edgeRect;
	UINT	edgeLength;

	CSize sizeMinLength(Pixel2ConstMM(pDC, 4), Pixel2ConstMM(pDC, 4)), sizeMaxLength(Pixel2ConstMM(pDC, 12), Pixel2ConstMM(pDC, 12));
	edgeLength = max((rect->top - rect->bottom)/ 8, sizeMinLength.cx);
	edgeLength = min(edgeLength, sizeMaxLength.cx);

	if ( (edgeLength > rect->Width()) || (edgeLength > abs(rect->Height())) )
		return;

	if(nTypeOfEdge == FOLDSHEET)
	{
		if ( (nActFoldSheetRefIndex < 0) || (nActFoldSheetRefIndex >= pActLayout->m_FoldSheetLayerRotation.GetSize()) )
			return;

		int	nRotation = pActLayout->m_FoldSheetLayerRotation[nActFoldSheetRefIndex];

		switch (ntype)
		{
		case LEFT_TOP	 : switch (nRotation)
						   {
						   case 90:		ntype = LEFT_BOTTOM;	break; 
						   case 180:	ntype = RIGHT_BOTTOM;	break; 
						   case 270:	ntype = RIGHT_TOP;		break; 
						   default:		break;
						   } 
						   break;
		case LEFT_BOTTOM : switch (nRotation)
						   {
						   case 90:		ntype = RIGHT_BOTTOM;	break; 
						   case 180:	ntype = RIGHT_TOP;		break; 
						   case 270:	ntype = LEFT_TOP;		break; 
						   default:		break;
						   } 
						   break;
		case RIGHT_TOP	 : switch (nRotation)
						   {
						   case 90:		ntype = LEFT_TOP;		break; 
						   case 180:	ntype = LEFT_BOTTOM;	break; 
						   case 270:	ntype = RIGHT_BOTTOM;	break; 
						   default:		break;
						   } 
						   break;
		case RIGHT_BOTTOM: switch (nRotation)
						   {
						   case 90:		ntype = RIGHT_TOP;		break; 
						   case 180:	ntype = LEFT_TOP;		break; 
						   case 270:	ntype = LEFT_BOTTOM;	break; 
						   default:		break;
						   } 
						   break;
		default :		   break; 
		}

		if (pActLayout->m_FoldSheetLayerTurned[nActFoldSheetRefIndex])
		{
			switch (ntype)	// turn edge
			{
			case LEFT_TOP:		ntype = RIGHT_TOP;		break;
			case RIGHT_TOP:		ntype = LEFT_TOP;		break;
			case LEFT_BOTTOM:	ntype = RIGHT_BOTTOM;	break;
			case RIGHT_BOTTOM:	ntype = LEFT_BOTTOM;	break;
			}
		}

		if (nSide == BACKSIDE) 
			if (pActLayout->KindOfProduction() == WORK_AND_TURN)
			{
				switch (ntype)	// turn edge
				{
				case LEFT_TOP:		ntype = RIGHT_TOP;		break;
				case RIGHT_TOP:		ntype = LEFT_TOP;		break;
				case LEFT_BOTTOM:	ntype = RIGHT_BOTTOM;	break;
				case RIGHT_BOTTOM:	ntype = LEFT_BOTTOM;	break;
				}
			}
			else	// WORK_AND_TUMBLE
			{
				switch (ntype)	// tumble edge
				{
				case LEFT_TOP:		ntype = LEFT_BOTTOM;	break;
				case LEFT_BOTTOM:	ntype = LEFT_TOP;		break;
				case RIGHT_TOP:		ntype = RIGHT_BOTTOM;	break;
				case RIGHT_BOTTOM:	ntype = RIGHT_TOP;		break;
				}
			}
	}

	switch (ntype)
	{
	case LEFT_TOP	 : p[1] = rect->TopLeft();
					   p[2] = p[1] - CSize(0, edgeLength);
					   p[0] = p[1] + CSize(edgeLength, 0);
					   p[3] = p[1];
					   break;
	case LEFT_BOTTOM : p[1] = rect->BottomRight() - CSize(rect->Width(), 0);
					   p[0] = p[1] + CSize(edgeLength, 0);
					   p[2] = p[1] + CSize(0, edgeLength);
					   p[3] = p[2];
					   break;
	case RIGHT_TOP	 : p[1] = rect->TopLeft() + CSize(rect->Width(), 0);
					   p[0] = p[1] - CSize(edgeLength, 0);
					   p[2] = p[1] - CSize(0, edgeLength);
					   p[3] = p[0];
					   break;
	case RIGHT_BOTTOM: p[1] = rect->BottomRight();
					   p[2] = p[1] + CSize(0, edgeLength);
					   p[0] = p[1] - CSize(edgeLength, 0);
					   p[3] = p[0] + CSize(0, edgeLength);
					   break;
	default : return; break; //TODO Errormessage
	}

	pDC->Polyline(p, 3);

	//Graphics graphics(pDC->m_hDC);
	//PointF points[3] = {PointF(p[0].x, p[0].y), PointF(p[1].x, p[1].y), PointF(p[2].x, p[2].y)};
	//Color crColor(85, GetRValue(RED), GetGValue(RED), GetBValue(RED));
	//graphics.DrawLines(&Pen(crColor, WorldToLP(5)), points, 3);

	edgeRect.top	= p[3].y;
	edgeRect.bottom	= p[3].y - edgeLength;
	edgeRect.left	= p[3].x;
	edgeRect.right  = p[3].x + edgeLength;

	//if (pDisplayList)
	//{
	//	if( ! bShowFoldSheet)
	//		if (nSide == FRONTSIDE)
	//			pDisplayList->RegisterItem(pDC, edgeRect, edgeRect, (void*)&(pActLayout->m_FrontSide), 
	//									   DISPLAY_ITEM_REGISTRY(CPrintSheetView, RefEdgePrintSheet),
	//									   (void*)pActPrintSheet, CDisplayItem::ForceRegister); 
	//		else
	//			pDisplayList->RegisterItem(pDC, edgeRect, edgeRect, (void*)&(pActLayout->m_BackSide),  
	//									   DISPLAY_ITEM_REGISTRY(CPrintSheetView, RefEdgePrintSheet),
	//									   (void*)pActPrintSheet, CDisplayItem::ForceRegister); 
	//}
}


void CPrintSheetView::DrawFoldSheetSectionBar(CDC* pDC, CPrintSheet* pActPrintSheet, int nActFoldSheetRefIndex, int nLayoutSide)
{
	if (MarksViewActive())
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CLayout* pActLayout = pActPrintSheet->GetLayout();

	if (0) //(pDoc->m_productionData.m_nProductionType == 1)		// Coming and going
	{
		int nCGIndex = 1;
		do
		{
			nCGIndex--;

			float fSpineLeft, fSpineBottom, fSpineRight, fSpineTop;
			CLayoutObject* pSpinePage = pActLayout->GetSectionMarkSpineArea(pActPrintSheet, nActFoldSheetRefIndex, fSpineLeft, fSpineBottom, fSpineRight, fSpineTop, (nCGIndex == -1) ? TRUE : FALSE, 1);
			if ( ! pSpinePage)
				return;
			if (nLayoutSide != pSpinePage->GetLayoutSideIndex())
				return;

			CRect		   sectionBar;
			BOOL		   bRotated = FALSE;
			long		   nWidth = WorldToLP(12), nHeight = WorldToLP(40), nLenght = 0;
			switch (pSpinePage->m_nHeadPosition)			// Calculate rectangle representing section bar
			{
			case TOP:		if (pSpinePage->m_nIDPosition == RIGHT)
								sectionBar.left = WorldToLP(pSpinePage->GetLeft())  - nWidth/2;
							else
								sectionBar.left = WorldToLP(pSpinePage->GetRight()) - nWidth/2;
							sectionBar.top = WorldToLP(pSpinePage->GetTop());
							nLenght = WorldToLP(pSpinePage->GetTop() - pSpinePage->GetBottom());
							break;

			case BOTTOM:	if (pSpinePage->m_nIDPosition == RIGHT)
								sectionBar.left = WorldToLP(pSpinePage->GetRight()) - nWidth/2;
							else
								sectionBar.left = WorldToLP(pSpinePage->GetLeft())  - nWidth/2;
							sectionBar.top = WorldToLP(pSpinePage->GetTop());
							nLenght = WorldToLP(pSpinePage->GetTop() - pSpinePage->GetBottom());
							break;

			case LEFT:		sectionBar.left = WorldToLP(pSpinePage->GetLeft());
							if (pSpinePage->m_nIDPosition == RIGHT)
								sectionBar.top = WorldToLP(pSpinePage->GetBottom()) + nWidth/2;
							else
								sectionBar.top = WorldToLP(pSpinePage->GetTop())    + nWidth/2;
							nLenght = WorldToLP(pSpinePage->GetRight() - pSpinePage->GetLeft());
							bRotated = TRUE;
							break;

			case RIGHT:		sectionBar.left = WorldToLP(pSpinePage->GetLeft());
							if (pSpinePage->m_nIDPosition == RIGHT)
								sectionBar.top = WorldToLP(pSpinePage->GetTop())    + nWidth/2;
							else
								sectionBar.top = WorldToLP(pSpinePage->GetBottom()) + nWidth/2;
							nLenght = WorldToLP(pSpinePage->GetRight() - pSpinePage->GetLeft());
							bRotated = TRUE;
							break;
			}

			int  nFoldSheetIndex = pActPrintSheet->m_FoldSheetLayerRefs[nActFoldSheetRefIndex].m_nFoldSheetIndex;
			if (nCGIndex == -1)
				nFoldSheetIndex = pDoc->m_Bookblock.m_FoldSheetList.GetCount() - nFoldSheetIndex - 1;
			long nOffset = 0;
			if (pDoc->m_Bookblock.m_FoldSheetList.GetCount() > 1)
				nOffset = ((nLenght - nHeight)/(pDoc->m_Bookblock.m_FoldSheetList.GetCount() - 1)) * nFoldSheetIndex;
			if (bRotated)
			{
				sectionBar.left  += (pSpinePage->m_nHeadPosition == LEFT) ? nOffset : nLenght - nOffset - nHeight;
				sectionBar.right  = sectionBar.left + nHeight;
				sectionBar.bottom = sectionBar.top  - nWidth;
			}
			else
			{
				sectionBar.top   -= (pSpinePage->m_nHeadPosition == TOP) ? nOffset : nLenght - nOffset - nHeight;
				sectionBar.right  = sectionBar.left + nWidth;
				sectionBar.bottom = sectionBar.top  - nHeight;
			}
			if (pDC->RectVisible(sectionBar))
				pDC->FillSolidRect(sectionBar, RED);
		}
		while (nCGIndex > -1);		
	}
	else
	{
		int nInstance = 1;
		do
		{
			float fSpineLeft, fSpineBottom, fSpineRight, fSpineTop;
			CLayoutObject* pSpinePage = pActLayout->GetSectionMarkSpineArea(pActPrintSheet, nActFoldSheetRefIndex, fSpineLeft, fSpineBottom, fSpineRight, fSpineTop, 0, nInstance);
			if ( ! pSpinePage)
				return;
			if (nLayoutSide != pSpinePage->GetLayoutSideIndex())
				return;

			CRect		   sectionBar;
			BOOL		   bRotated = FALSE;
			long		   nWidth = WorldToLP(12), nHeight = WorldToLP(40), nLenght = 0;
			switch (pSpinePage->m_nHeadPosition)			// Calculate rectangle representing section bar
			{
			case TOP:		if (pSpinePage->m_nIDPosition == RIGHT)
								sectionBar.left = WorldToLP(pSpinePage->GetLeft())  - nWidth/2;
							else
								sectionBar.left = WorldToLP(pSpinePage->GetRight()) - nWidth/2;
							sectionBar.top = WorldToLP(pSpinePage->GetTop());
							nLenght = WorldToLP(pSpinePage->GetTop() - pSpinePage->GetBottom());
							break;

			case BOTTOM:	if (pSpinePage->m_nIDPosition == RIGHT)
								sectionBar.left = WorldToLP(pSpinePage->GetRight()) - nWidth/2;
							else
								sectionBar.left = WorldToLP(pSpinePage->GetLeft())  - nWidth/2;
							sectionBar.top = WorldToLP(pSpinePage->GetTop());
							nLenght = WorldToLP(pSpinePage->GetTop() - pSpinePage->GetBottom());
							break;

			case LEFT:		sectionBar.left = WorldToLP(pSpinePage->GetLeft());
							if (pSpinePage->m_nIDPosition == RIGHT)
								sectionBar.top = WorldToLP(pSpinePage->GetBottom()) + nWidth/2;
							else
								sectionBar.top = WorldToLP(pSpinePage->GetTop())    + nWidth/2;
							nLenght = WorldToLP(pSpinePage->GetRight() - pSpinePage->GetLeft());
							bRotated = TRUE;
							break;

			case RIGHT:		sectionBar.left = WorldToLP(pSpinePage->GetLeft());
							if (pSpinePage->m_nIDPosition == RIGHT)
								sectionBar.top = WorldToLP(pSpinePage->GetTop())    + nWidth/2;
							else
								sectionBar.top = WorldToLP(pSpinePage->GetBottom()) + nWidth/2;
							nLenght = WorldToLP(pSpinePage->GetRight() - pSpinePage->GetLeft());
							bRotated = TRUE;
							break;
			}

			int  nFoldSheetIndex = pActPrintSheet->m_FoldSheetLayerRefs[nActFoldSheetRefIndex].m_nFoldSheetIndex;
			long nOffset = 0;
			if (pDoc->m_Bookblock.m_FoldSheetList.GetCount() > 1)
				nOffset = ((nLenght - nHeight)/(pDoc->m_Bookblock.m_FoldSheetList.GetCount() - 1)) * nFoldSheetIndex;
			if (bRotated)
			{
				sectionBar.left  += (pSpinePage->m_nHeadPosition == LEFT) ? nOffset : nLenght - nOffset - nHeight;
				sectionBar.right  = sectionBar.left + nHeight;
				sectionBar.bottom = sectionBar.top  - nWidth;
			}
			else
			{
				sectionBar.top   -= (pSpinePage->m_nHeadPosition == TOP) ? nOffset : nLenght - nOffset - nHeight;
				sectionBar.right  = sectionBar.left + nWidth;
				sectionBar.bottom = sectionBar.top  - nHeight;
			}
			if (pDC->RectVisible(sectionBar))
				pDC->FillSolidRect(sectionBar, RED);

			nInstance++;
		}
		while (1);		
	}
}

void CPrintSheetView::DrawTrimChannelRect(CDC* pDC, UINT nActLayoutSide) 
{
	if (m_bDrawTrimChannel && (nActLayoutSide == (UINT)m_TrimChannel.n_SightToDraw))
	{
		CRect TrimRectToDraw;

		pDC->SelectStockObject(HOLLOW_BRUSH);	// Predefined brush with no filling of rectangle

		CPen trimChannelPen;
		trimChannelPen.CreatePen(PS_SOLID, 0, RED);
		CPen* pOldPen = (CPen*)pDC->SelectObject(&trimChannelPen);

		TrimRectToDraw.left	  = m_TrimChannel.Rect.left;
		TrimRectToDraw.top	  = m_TrimChannel.Rect.top;
		TrimRectToDraw.right  = m_TrimChannel.Rect.right;
		TrimRectToDraw.bottom = m_TrimChannel.Rect.bottom;

		CRect rcTest = TrimRectToDraw;
		pDC->LPtoDP(rcTest);

		CPoint height, BottomLeft, TopRight; 

		height.y = TrimRectToDraw.top - TrimRectToDraw.bottom;
		height.x = 0;
		BottomLeft = CPoint(TrimRectToDraw.TopLeft()     - height);
		TopRight   = CPoint(TrimRectToDraw.BottomRight() + height);

		switch (m_TrimChannel.n_Direction)
		{
			case VERTICAL  : pDC->MoveTo(TrimRectToDraw.TopLeft());
							 pDC->LineTo(BottomLeft);
							 pDC->MoveTo(TrimRectToDraw.BottomRight());
							 pDC->LineTo(TopRight);
							 break;
			case HORIZONTAL: pDC->MoveTo(TrimRectToDraw.TopLeft());
							 pDC->LineTo(TopRight);
							 pDC->MoveTo(TrimRectToDraw.BottomRight());
							 pDC->LineTo(BottomLeft);
							 break;
		}
		pDC->SelectObject(pOldPen);
		trimChannelPen.DeleteObject();
	}
}

void CPrintSheetView::DrawSectionLinesForWebPress(CDC* pDC, UINT nActLayoutSide, CLayout* pActLayout)
{
	CPressDevice* pPressDevice = NULL;
	long lXRefLine;
	long lYRefLine1;
	long lYRefLine2;
	long lWidth;
	long lHeight;

	switch (nActLayoutSide)
	{
		case FRONTSIDE: pPressDevice = pActLayout->m_FrontSide.GetPressDevice(); break;
		case BACKSIDE : pPressDevice = pActLayout->m_BackSide.GetPressDevice(); break;
	}

	if ( ! pPressDevice)
		return;

	if (pActLayout->KindOfProduction() == WORK_AND_TURN)
	{
		if (nActLayoutSide == FRONTSIDE)
			lXRefLine = WorldToLP(pPressDevice->m_fXRefLine);
		else
			lXRefLine = WorldToLP(pPressDevice->m_fPlateWidth - pPressDevice->m_fXRefLine);
	}
	else
		lXRefLine = WorldToLP(pPressDevice->m_fXRefLine);

	lYRefLine1 = WorldToLP(pPressDevice->m_fYRefLine1);
	lYRefLine2 = WorldToLP(pPressDevice->m_fYRefLine2);

	lWidth  = WorldToLP(pPressDevice->m_fPlateWidth);
	lHeight = WorldToLP(pPressDevice->m_fPlateHeight);

	CPen activeLinePen, inActiveLinePen;
	activeLinePen.CreatePen(PS_DOT, 0, BLACK);
	inActiveLinePen.CreatePen(PS_SOLID, 0, MIDDLEGRAY);
	CPen* pOldPen = pDC->SelectObject(&activeLinePen);

	pDC->MoveTo(lXRefLine, 0);
	pDC->LineTo(lXRefLine, lHeight);

	if (m_nActiveYRefLine == 1)
		pDC->SelectObject(&activeLinePen);
	else
		pDC->SelectObject(&inActiveLinePen);
	pDC->MoveTo(0, lYRefLine1);
	pDC->LineTo(lWidth, lYRefLine1);
	CRect rcDI(CPoint(0, lYRefLine1), CPoint(lWidth, lYRefLine1)); rcDI.InflateRect(0, 5);
	m_DisplayList.RegisterItem(pDC, rcDI, rcDI, (void*)NULL, DISPLAY_ITEM_REGISTRY(CPrintSheetView, WebYRefLine1), (void*)NULL, CDisplayItem::ForceRegister); 

	if (pPressDevice->Has2YRefLines())
	{
		if (m_nActiveYRefLine == 2)
			pDC->SelectObject(&activeLinePen);
		else
			pDC->SelectObject(&inActiveLinePen);
		pDC->MoveTo(0, lYRefLine2);
		pDC->LineTo(lWidth, lYRefLine2);
		CRect rcDI(CPoint(0, lYRefLine2), CPoint(lWidth, lYRefLine2)); rcDI.InflateRect(0, 5);
		m_DisplayList.RegisterItem(pDC, rcDI, rcDI, (void*)NULL, DISPLAY_ITEM_REGISTRY(CPrintSheetView, WebYRefLine2), (void*)NULL, CDisplayItem::ForceRegister); 
	}

	pDC->SelectObject(pOldPen);
	activeLinePen.DeleteObject();
	inActiveLinePen.DeleteObject();
}

void CPrintSheetView::DrawSheetMeasuring(CDC* pDC, UINT nActLayoutSide, CLayout* pActLayout, BOOL bShowPlate)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));

	//if (pView->ViewMode() == ID_VIEW_ALL_SHEETS)
	//	return;
	//if ( ! pView->ShowMeasure())
	//	return;
	if ( ! pActLayout)
		return;
	CLayoutSide* pLayoutSide = (nActLayoutSide == FRONTSIDE) ? &pActLayout->m_FrontSide : &pActLayout->m_BackSide;
	if ( ! pLayoutSide)
		return;
	CRect sheetRect;
	sheetRect.left   = WorldToLP(pLayoutSide->m_fPaperLeft  );
	sheetRect.top	 = WorldToLP(pLayoutSide->m_fPaperTop   );
	sheetRect.right  = WorldToLP(pLayoutSide->m_fPaperRight );
	sheetRect.bottom = WorldToLP(pLayoutSide->m_fPaperBottom);

	CSize sizeDistance(WorldToLP(20), WorldToLP(20));//(Pixel2MM(pDC, 50), Pixel2MM(pDC, 50));
	//pDC->DPtoLP(&sizeDistance);

	int nXPos = sheetRect.right  + sizeDistance.cx;
	int nYPos = sheetRect.bottom - sizeDistance.cy;
	CPressDevice* pPressDevice = pLayoutSide->GetPressDevice();
	if ( ! pPressDevice)
		bShowPlate = FALSE;
	if (bShowPlate)
	{
		nXPos = WorldToLP(pPressDevice->m_fPlateWidth) + sizeDistance.cx;
		nYPos = WorldToLP(0.0f) - sizeDistance.cy;
	}

	CString strMeasure, strOut;
	if (bShowPlate)
	{
		// paper origin X
		CPoint pt[2]  = {CPoint(WorldToLP(0.0f), nYPos), CPoint(sheetRect.left, nYPos)};
		CPoint ptRef1 =  CPoint(WorldToLP(0.0f), WorldToLP(0.0f));
		CPoint ptRef2 =  CPoint(sheetRect.left,	 sheetRect.bottom);
		SetMeasure(0, pLayoutSide->m_fPaperLeft, strMeasure);
		DrawHMeasuring(pDC, pt, strMeasure, ptRef1, ptRef2, TRUE);

		// paper origin Y
		pt[0]  = CPoint(nXPos, sheetRect.bottom); pt[1] = CPoint(nXPos, WorldToLP(0.0f));
		ptRef1 = CPoint(sheetRect.left,	 sheetRect.bottom);
		ptRef2 = CPoint(WorldToLP(0.0f), WorldToLP(0.0f));
		SetMeasure(0, pLayoutSide->m_fPaperBottom, strMeasure);
		DrawVMeasuring(pDC, pt, strMeasure, ptRef1, ptRef2, TRUE);

		if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
		{
			// gripper
			int nXPos = WorldToLP(0.0f) - sizeDistance.cx;
			pt[0]  = CPoint(nXPos, sheetRect.bottom + WorldToLP(pPressDevice->m_fGripper)); pt[1] = CPoint(nXPos, sheetRect.bottom);
			ptRef1 = CPoint(sheetRect.right + sizeDistance.cx/4, sheetRect.bottom + WorldToLP(pPressDevice->m_fGripper));
			ptRef2 = CPoint(sheetRect.left, sheetRect.bottom);
			SetMeasure(0, pPressDevice->m_fGripper, strMeasure);
			strOut.Format(IDS_GRIPPER, strMeasure);
			DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

			// begin of printing area in Y
			nXPos = WorldToLP(0.0f) - 2 * sizeDistance.cx;
			pt[0]  = CPoint(nXPos,								 WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge)); 
			pt[1]  = CPoint(nXPos,								 WorldToLP(0.0f));
			ptRef1 = CPoint(sheetRect.right + sizeDistance.cx/4, WorldToLP(pPressDevice->m_fPlateEdgeToPrintEdge));
			ptRef2 = CPoint(WorldToLP(0.0f),					 WorldToLP(0.0f));
			SetMeasure(0, pPressDevice->m_fPlateEdgeToPrintEdge, strMeasure);
			strOut.Format(IDS_PRINTBEGIN, strMeasure);
			DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
		}

	}

	// paper width
	CPoint pt[2]  = {CPoint(sheetRect.left, nYPos), pt[1] = CPoint(sheetRect.right, nYPos)};
	CPoint ptRef1 = CPoint(sheetRect.left,  sheetRect.bottom);
	CPoint ptRef2 = CPoint(sheetRect.right, sheetRect.bottom);
	SetMeasure(0, pLayoutSide->m_fPaperWidth, strMeasure);
	strOut.Format((pPressDevice->m_nPressType == CPressDevice::SheetPress) ? IDS_SHEETWIDTH : IDS_WEBWIDTH, strMeasure);
	DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

	// paper height
	pt[0]  = CPoint(nXPos, sheetRect.top); pt[1] = CPoint(nXPos, sheetRect.bottom);
	ptRef1 = CPoint(sheetRect.right, sheetRect.top);
	ptRef2 = CPoint(sheetRect.right, sheetRect.bottom);
	SetMeasure(0, pLayoutSide->m_fPaperHeight, strMeasure);
	strOut.Format((pPressDevice->m_nPressType == CPressDevice::SheetPress) ? IDS_SHEETHEIGHT : IDS_WEBLENGTH, strMeasure);
	DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
}

void CPrintSheetView::DrawPlateMeasuring(CDC* pDC, UINT nActLayoutSide, CLayout* pActLayout)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));

	//if (pView->ViewMode() == ID_VIEW_ALL_SHEETS)
	//	return;
	//if ( ! pView->ShowMeasure())
	//	return;
	//if ( ! pView->ShowPlate())
	//	return;

	if ( ! pActLayout)
		return;
	CPressDevice* pPressDevice = NULL;
	switch(nActLayoutSide)
	{
	case FRONTSIDE: pPressDevice = pActLayout->m_FrontSide.GetPressDevice();
					break;
	case BACKSIDE : pPressDevice = pActLayout->m_BackSide.GetPressDevice();
					break;
	}
	if ( ! pPressDevice)
		return;

	CRect plateRect;
	plateRect.left	 = WorldToLP(0);
	plateRect.top	 = WorldToLP(pPressDevice->m_fPlateHeight);
	plateRect.right	 = WorldToLP(pPressDevice->m_fPlateWidth);
	plateRect.bottom = WorldToLP(0);

	CSize sizeDistance(WorldToLP(20), WorldToLP(20));
	//pDC->DPtoLP(&sizeDistance);

	// plate width
	CPoint pt[2]  = {CPoint(plateRect.left, plateRect.bottom - 2 * sizeDistance.cy), CPoint(plateRect.right, plateRect.bottom - 2 * sizeDistance.cy)};
	CPoint ptRef1 = CPoint(plateRect.left,  plateRect.bottom);
	CPoint ptRef2 = CPoint(plateRect.right, plateRect.bottom);
	CString strOut, strMeasure;
	SetMeasure(0, pPressDevice->m_fPlateWidth, strMeasure);
	strOut.Format(IDS_PLATEWIDTH, strMeasure);
	DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

	// plate height
	pt[0]  = CPoint(plateRect.right + 2 * sizeDistance.cx, plateRect.top); pt[1] = CPoint(plateRect.right + 2 * sizeDistance.cx, plateRect.bottom);
	ptRef1 = CPoint(plateRect.right, plateRect.top);
	ptRef2 = CPoint(plateRect.right, plateRect.bottom); 
	SetMeasure(0, pPressDevice->m_fPlateHeight, strMeasure);
	strOut.Format(IDS_PLATEHEIGHT, strMeasure);
	DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

	if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
	{
		// plate center X
		pt[0]  = CPoint(plateRect.left, plateRect.top + sizeDistance.cy); pt[1] = CPoint(plateRect.left + (plateRect.right - plateRect.left)/2, plateRect.top + sizeDistance.cy);
		ptRef1 = CPoint(plateRect.left, plateRect.top);
		ptRef2 = CPoint(plateRect.left + (plateRect.right - plateRect.left)/2, plateRect.bottom - sizeDistance.cy/4);
		SetMeasure(0, pPressDevice->m_fPlateWidth/2.0f, strMeasure);
		strOut.Format(IDS_PLATECENTERX, strMeasure);
		DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
	}
	else
	{
		// web refline X
		pt[0]  = CPoint(plateRect.left, plateRect.top + sizeDistance.cy); pt[1] = CPoint(plateRect.left + WorldToLP(pPressDevice->m_fXRefLine), plateRect.top + sizeDistance.cy);
		ptRef1 = CPoint(plateRect.left, plateRect.top);
		ptRef2 = CPoint(plateRect.left + WorldToLP(pPressDevice->m_fXRefLine), plateRect.bottom - sizeDistance.cy/4);
		SetMeasure(0, pPressDevice->m_fXRefLine, strMeasure);
		strOut.Format(IDS_WEBREFLINEX, strMeasure);
		DrawHMeasuring(pDC, pt, strOut, ptRef1, ptRef2);

		int nLeft = plateRect.left - sizeDistance.cx;
		// web Y refline 1
		pt[0] = CPoint(nLeft, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fYRefLine1));
		pt[1]  = CPoint(nLeft, plateRect.bottom); 
		ptRef1 = CPoint(plateRect.right + sizeDistance.cx/4, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fYRefLine1));
		ptRef2 = CPoint(plateRect.left, plateRect.bottom);
		SetMeasure(0, pPressDevice->m_fYRefLine1, strMeasure);
		strOut.Format(IDS_WEBREFLINEY, strMeasure);
		DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
		nLeft -= 2 * sizeDistance.cx;

		if (pPressDevice->Has2YRefLines())
		{
			// web Y refline 2
			pt[0] = CPoint(nLeft, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fYRefLine2));
			pt[1]  = CPoint(nLeft, plateRect.bottom); 
			ptRef1 = CPoint(plateRect.right + sizeDistance.cx/4, plateRect.bottom + CPrintSheetView::WorldToLP(pPressDevice->m_fYRefLine2));
			ptRef2 = CPoint(plateRect.left, plateRect.bottom);
			SetMeasure(0, pPressDevice->m_fYRefLine2, strMeasure);
			strOut.Format(IDS_WEBREFLINEY, strMeasure);
			DrawVMeasuring(pDC, pt, strOut, ptRef1, ptRef2);
		}
	}
}

void CPrintSheetView::DrawHMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1, CPoint ptRef2, BOOL bChain)
{
	int nFontSize = CLayout::TransformFontSize(pDC, 18);
	CFont font;
	font.CreateFont(nFontSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
	CFont* pOldFont = pDC->SelectObject(&font);

	int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor	= pDC->SetBkColor((pDC->IsPrinting()) ? WHITE : RGB(220,225,240));
	COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

	CString strOut;
	strOut.Format(_T(" %s "), strMeasure);

	//pDC->LPtoDP(pt, 2);

	CSize textExtent = pDC->GetTextExtent(strOut);
	//pDC->LPtoDP(&textExtent);

	CPen  linePen(PS_SOLID, 1, BLACK);
	CPen* pOldPen = pDC->SelectObject(&linePen);
	if ( (ptRef1.x != INT_MAX) && (ptRef1.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef1);
		//pDC->LPtoDP(&ptRef1);
		CPoint ptTo(pt[0] + CSize(0, (pt[0].y > ptRef1.y) ? Pixel2MM(pDC, 10) : -Pixel2MM(pDC, 10)));
		//pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	if ( (ptRef2.x != INT_MAX) && (ptRef2.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef2);
		//pDC->LPtoDP(&ptRef2);
		CPoint ptTo(pt[1] + CSize(0, (pt[1].y > ptRef1.y) ? Pixel2MM(pDC, 10) : -Pixel2MM(pDC, 10)));
		//pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	pDC->SelectObject(pOldPen);
	linePen.DeleteObject();

	BOOL bFit = TRUE;
	CSize sizeLen(pt[1].x - pt[0].x, 0);
	if (sizeLen.cx - Pixel2MM(pDC, 10) < textExtent.cx)	// if measurement arrow too short, flip arrows and use fix length
	{
		pDC->LPtoDP(pt, 2);
		bFit = FALSE;
		sizeLen = CSize(15, 0);
		DrawArrow(pDC, LEFT,   pt[1], sizeLen.cx - 5, BLACK, LIGHTGRAY, TRUE);
		DrawArrow(pDC, RIGHT,  pt[0], sizeLen.cx - 5, BLACK, LIGHTGRAY, TRUE);
	}
	else
	{
		pDC->LPtoDP(pt, 2);
		pDC->LPtoDP(&sizeLen);
		DrawArrow(pDC, LEFT,   pt[0], sizeLen.cx - 5, BLACK, LIGHTGRAY, TRUE);
		DrawArrow(pDC, RIGHT,  pt[1], sizeLen.cx - 5, BLACK, LIGHTGRAY, TRUE);
	}

	pDC->DPtoLP(pt, 2);
	pDC->DPtoLP(&sizeLen);
	CPoint ptTextCenter = CPoint(( ! bFit) ? pt[1].x + textExtent.cx/2 + ((bChain) ? sizeLen.cx/2 : 2*sizeLen.cx/3) : pt[0].x + (pt[1].x - pt[0].x)/2, 
								 ( ! bFit) ? pt[0].y - ((bChain) ? textExtent.cy/2 : 0) : pt[0].y);

	int nX = (textExtent.cx/2 + Pixel2MM(pDC, 5));
	int nY = textExtent.cy/2;
	CRect textRect(ptTextCenter.x - nX, ptTextCenter.y + nY, ptTextCenter.x + nX, ptTextCenter.y - nY);
	//textRect.InflateRect(textExtent.cx/2 + Pixel2MM(pDC, 5), textExtent.cy/2);
	//pDC->DPtoLP(textRect);
	pDC->DrawText(strOut, textRect, DT_CENTER | DT_VCENTER);

	pDC->SelectObject(pOldFont);
	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetTextColor(crOldTextColor);

	font.DeleteObject();
}

void CPrintSheetView::DrawVMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1, CPoint ptRef2, BOOL bChain)
{
	int nFontSize = CLayout::TransformFontSize(pDC, 18);
	CFont font270;
	font270.CreateFont(nFontSize, 0, 2700, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	

	CFont*	 pOldFont		= pDC->SelectObject(&font270);
	int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor	= pDC->SetBkColor((pDC->IsPrinting()) ? WHITE : RGB(220,225,240));
	COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

	CString strOut;
	strOut.Format(_T(" %s "), strMeasure);

	//pDC->LPtoDP(pt, 2);

	CSize textExtent = pDC->GetTextExtent(strOut);
	//pDC->LPtoDP(&textExtent);

	CPen  linePen(PS_SOLID, 1, BLACK);
	CPen* pOldPen = pDC->SelectObject(&linePen);
	if ( (ptRef1.x != INT_MAX) && (ptRef1.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef1);
		//pDC->LPtoDP(&ptRef1);
		CPoint ptTo(pt[0] + CSize((pt[0].x > ptRef1.x) ? Pixel2MM(pDC, 10) : -Pixel2MM(pDC, 10), 0));
		//pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	if ( (ptRef2.x != INT_MAX) && (ptRef2.y != INT_MAX) )
	{
		pDC->MoveTo(ptRef2);
		//pDC->LPtoDP(&ptRef2);
		CPoint ptTo(pt[1] + CSize((pt[1].x > ptRef1.x) ? Pixel2MM(pDC, 10) : -Pixel2MM(pDC, 10), 0));
		//pDC->DPtoLP(&ptTo);
		pDC->LineTo(ptTo);
	}
	pDC->SelectObject(pOldPen);
	linePen.DeleteObject();

	BOOL bFit = TRUE;
	CSize sizeLen(0, pt[1].y - pt[0].y);
	if (abs(sizeLen.cy) - Pixel2MM(pDC, 10) < textExtent.cx)	// if measurement arrow too short, flip arrows and use fix length
	{
		bFit = FALSE;
		sizeLen = CSize(15, 15);
		pDC->LPtoDP(pt, 2);
		DrawArrow(pDC, BOTTOM, pt[0], sizeLen.cy - 5, BLACK, LIGHTGRAY, TRUE);
		DrawArrow(pDC, TOP,	   pt[1], sizeLen.cy - 5, BLACK, LIGHTGRAY, TRUE);
	}
	else
	{
		pDC->LPtoDP(pt, 2);
		pDC->LPtoDP(&sizeLen); 
		DrawArrow(pDC, TOP,    pt[0], abs(sizeLen.cy) - 5, BLACK, LIGHTGRAY, TRUE);
		DrawArrow(pDC, BOTTOM, pt[1], abs(sizeLen.cy) - 5, BLACK, LIGHTGRAY, TRUE);
	}

	pDC->DPtoLP(pt, 2);
	pDC->DPtoLP(&sizeLen);
	CPoint ptTextCenter = CPoint(( ! bFit) ? pt[0].x - ((bChain) ? textExtent.cy/2 : 0) : pt[0].x, 
								 ( ! bFit) ? pt[0].y + textExtent.cx/2 + ((bChain) ? sizeLen.cx/2 : 2*sizeLen.cx/3) : pt[0].y + (pt[1].y - pt[0].y)/2);

	int nX = textExtent.cx/2;
	int nY = textExtent.cy/2;
	CRect textRect(ptTextCenter.x - nY, ptTextCenter.y + nX, ptTextCenter.x + nY, ptTextCenter.y - nX);
	pDC->TextOut(textRect.left, textRect.bottom, strOut);

	pDC->SelectObject(pOldFont);
	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetTextColor(crOldTextColor);

	font270.DeleteObject();
}


void CPrintSheetView::DrawMoveObjectReflines(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet)
{
	if ( ShowBitmap() || ShowExposures() || (ViewMode() == ID_VIEW_ALL_SHEETS) )
		return;

	//if (IsOpenDlgLayoutObjectGeometry())
	//{
	//	CDlgLayoutObjectGeometry* pDlg			= GetDlgLayoutObjectGeometry();
	//	CLayoutObject*			  pLayoutObject = (pDlg) ? pDlg->m_pParent->GetFirstLayoutObject() : NULL;
	//	if (pLayoutObject)
	//	{
	//		BOOL bDraw = TRUE;
	//		int nSide = pLayoutObject->GetLayoutSideIndex();
	//		if ( ! pLayoutObject->FindCounterpartObject())
	//			if (nActLayoutSide != nSide) 
	//				bDraw = FALSE;
	//		if (bDraw)
	//			pLayoutObject->m_reflinePosParams.DrawSheetReflines(pDC, pActPrintSheet, nActLayoutSide, 0, 1.0f, 1.0f, (nSide == BACKSIDE) ? FALSE : TRUE);
	//	}
	//	return;
	//}

	if (IsOpenDlgAlignObjects())
	{
		if (nActLayoutSide == BACKSIDE)		// actually only draw on front side
			return;

		CDisplayItem* pDI = m_DisplayList.GetFirstGroupMember(1);
		if (pDI)
		{
			BOOL bDraw = TRUE;
			int  nSide = FRONTSIDE; 
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
			{
				CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
				if (pLayoutObject)
				{
					nSide = pLayoutObject->GetLayoutSideIndex();
					if ( ! pLayoutObject->FindCounterpartObject())
						if (nActLayoutSide != nSide) 
							bDraw = FALSE;
				}
			}
			else
				if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))		
				{
					CLayout*	 pLayout			= (pActPrintSheet) ? pActPrintSheet->GetLayout() : NULL;
					int			 nComponentRefIndex = (int)pDI->m_pData;
					CLayoutSide* pLayoutSide		= (pLayout) ? ((pDI->m_AuxRect.Height() > 1) ? &pLayout->m_FrontSide : &pLayout->m_BackSide) : NULL;
					if (pLayoutSide) 
						nSide = pLayoutSide->m_nLayoutSide;
					else
						bDraw = FALSE;
				}

			if (bDraw)
			{
				int nDirection = -1;	// means both defined
				if ( (GetDlgAlignObjects()->GetXReference() == 255) && (GetDlgAlignObjects()->GetYReference() == 255) )
					nDirection = -1;
				else
					if (GetDlgAlignObjects()->GetXReference() != 255) 
						nDirection = HORIZONTAL;
					else
						if (GetDlgAlignObjects()->GetYReference() != 255)
							nDirection = VERTICAL;

				if (nDirection != -1)
					GetDlgAlignObjects()->m_reflinePosParams.DrawSheetReflines(pDC, pActPrintSheet, nActLayoutSide, 0, 1.0f, 1.0f, (nSide == BACKSIDE) ? FALSE : TRUE, nDirection);
			}
		}
	}
}
