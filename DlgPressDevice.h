// DlgPressDevice.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPressDevice CPressDeviceList 
//
/*
class CPressDevice
{
// Attributes
public:
	char	m_szName[16];			// Name
	USHORT	m_usType;				// Sheet or Web - machine
	float	m_fPlateEdge,			// Plateedge -> printedge
			m_fGripper,				// Gripper
			m_fRefline,				// Referenceline in X
			m_fSectionCenter,		// Center of section in Y
			m_fWidth,				// Plateformat:	width
			m_fHeight;				//		""	  :	height
	char	m_szDummy[20];
};

typedef CList <CPressDevice,CPressDevice&> CPressDeviceList;
*/



// CPDPreviewFrame

class CPDPreviewFrame : public CStatic
{
	DECLARE_DYNAMIC(CPDPreviewFrame)

public:
	CPDPreviewFrame();
	virtual ~CPDPreviewFrame();



protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};



/////////////////////////////////////////////////////////////////////////////
// CDlgPressDevice dialog

class CDlgPressDevice : public CDialog
{
// Construction
public:
	CDlgPressDevice(int bModifyDirect = TRUE, int nActSight = -1, CPressDevice* pActPressDevice = NULL, CLayout* pActLayout = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPressDevice)
	enum { IDD = IDD_PRESSDEVICE };
	float	m_fGripper;
	float	m_fHeight;
	float	m_fWidth;
	float	m_fPlateEdge;
	CString	m_strNotes;
	CString	m_strName;
	float	m_fSheetEdge;
	float   m_fYRefLine1;
	float   m_fYRefLine2;
	BOOL	m_bShow2YRefLines;
	//}}AFX_DATA
	CPressDevice m_pressDevice;
	int			 m_nPressDeviceIndex;
	CString		 m_strDeviceID;
protected:
	BOOL				m_bModifyDirect;
	int					m_nActSight;
	CPressDevice*		m_pActPressDevice;
	CLayout*			m_pActLayout;
	BOOL				m_bModified;
	BOOL				m_bEditChanged;
	CPDPreviewFrame		m_pressDevicePreviewFrame;


public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPressDevice)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	static BOOL		OnLoadingData();
	static float	CheckGripper(CPressDevice* pOldPressDevice, CPressDevice* pNewPressDevice, CLayoutSide* pLayoutSide, BOOL bDoWarn = TRUE);
	void			SetPressDevice(int nSide, int nIndex);
	int				GetActSight();
	CPressDevice*	GetActPressDevice();
	CLayout*		GetActLayout();

protected:
	BOOL OnUpdateData();

	// Generated message map functions
	//{{AFX_MSG(CDlgPressDevice)
	virtual BOOL OnInitDialog();
	afx_msg void OnDel();
	afx_msg void OnNew();
	virtual void OnOK();
	afx_msg void OnSheet();
	afx_msg void OnWeb();
	afx_msg void OnSelchangePressDeviceList();
	afx_msg void OnEditchangePressDeviceList();
	afx_msg void OnChange();
	afx_msg void OnChangeSheetedge();
	afx_msg void OnChangePlateedge(); 
	afx_msg void OnChangeGripper();
	afx_msg void OnChangePlateHeight(); 
	afx_msg void OnChangePlateWidth();
	afx_msg void OnKillfocusSheetedge();
	afx_msg void OnKillfocusPlateedge();
	afx_msg void OnKillfocusGripper();
	afx_msg void OnKillfocusPlateHeight();
	afx_msg void OnKillfocusPlateWidth();
	afx_msg void OnKillfocusYRefLine2();
	afx_msg void OnBnClickedYReflines();
	DECLARE_MESSAGE_MAP()
	//}}AFX_MSG
};
