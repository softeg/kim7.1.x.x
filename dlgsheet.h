// DlgSheet.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CDlgSheet dialog

class CDlgSheet : public CDialog
{
// Construction

public:
	CDlgSheet(BOOL bModifyDirect = TRUE, CPrintSheet* pActPrintSheet = NULL, CWnd* pParent = NULL);   // standard constructor
	CSheetList m_sheetList;

// Dialog Data
	//{{AFX_DATA(CDlgSheet)
	enum { IDD = IDD_SHEET };
	CSpinButtonCtrl	m_WidthSpin;
	CSpinButtonCtrl	m_HeightSpin;
	int		m_nPageOrientation;
	float	m_fHeight;
	float	m_fWidth;
	CString	m_strName;
	int		m_nPaperGrain;
	BOOL	m_bFanoutCheck;
	//}}AFX_DATA
//	CString				 m_strName;
//	CPageFormatSelection m_PageFormatSelection;

protected:
	BOOL			m_bModifyDirect;
	BOOL			m_bModified;
	int				m_nActSight;
	CPrintSheet*	m_pActPrintSheet;
	CLayout*		m_pActLayout;
	CPressDevice*	m_pActPressDevice;

public:
	CFanoutList m_FanoutList;


friend class CLayoutSide; // TODO: provisional - needed by CLayoutSide::FindSuitablePaper()

public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgSheet)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL LoadSheets();
protected:
	BOOL LoadOldStyleSheets();
	BOOL SaveSheets();
	void UpdateSheetSide(CLayoutSide* pLayoutSide);

	// Generated message map functions
	//{{AFX_MSG(CDlgSheet)
	virtual BOOL OnInitDialog();
	afx_msg void OnDel();
	afx_msg void OnNew();
	afx_msg void OnSelchangeSheetList();
	virtual void OnOK();
	afx_msg void OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPortrait();
	afx_msg void OnLandscape();
	afx_msg void OnChangeSheetHeight();
	afx_msg void OnChangeSheetWidth();
	afx_msg void OnChange();
	afx_msg void OnEditchangeSheetList();
	afx_msg void OnDefineFanout();
	afx_msg void OnCheckFanout();
	afx_msg void OnKillfocusSheetHeight();
	afx_msg void OnKillfocusSheetWidth();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
