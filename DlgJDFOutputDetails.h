#pragma once

#include "PlaceholderEdit.h"
#include "MyBitmapButton.h"


// CDlgJDFOutputDetails-Dialogfeld

class CDlgJDFOutputDetails : public CDialog
{
	DECLARE_DYNAMIC(CDlgJDFOutputDetails)

public:
	CDlgJDFOutputDetails(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgJDFOutputDetails();

// Dialogfelddaten
	enum { IDD = IDD_JDF_OUPUT_DETAILS};

public:
	CMenu					m_placeholderMenu;
	CPlaceholderEdit		m_placeholderJobIDEdit;
	CPlaceholderEdit		m_placeholderJobPartIDEdit;
	CPlaceholderEdit		m_placeholderDescriptiveNameEdit;
	CMyBitmapButton			m_buttonSelectJobIDPlaceholder;
	CMyBitmapButton			m_buttonSelectJobPartIDPlaceholder;
	CMyBitmapButton			m_buttonSelectDescriptiveNamePlaceholder;
	int						m_nCurPlaceholderEditID;

CString m_strJDFJobIDMap;
CString m_strJDFJobPartIDMap;
CString m_strJDFDescriptiveNameMap;
CString m_strApogeeTrappingSet;
CString m_strApogeeSeparationSet;
CString m_strApogeeRenderingSet;
CString m_strApogeeScreeningSet;
CString m_strApogeePrintSet;
CString m_strApogeeOutputSet;
CString m_strApogeeImageSet;

public:
	void OnSelectPlaceholder(int nID);


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnButtonSelectJobIDPlaceholder();
	afx_msg void OnButtonSelectJobPartIDPlaceholder();
	afx_msg void OnButtonSelectDescriptiveNamePlaceholder();
	afx_msg void OnPlaceholderFoldsheetNum();
	afx_msg void OnPlaceholderSheetside();
	afx_msg void OnPlaceholderWebNum();
	afx_msg void OnPlaceholderColorNum();
	afx_msg void OnPlaceholderFoldsheetNumCG();
	afx_msg void OnPlaceholderJobTitle();
	afx_msg void OnPlaceholderTileNum();
	afx_msg void OnPlaceholderSheetNum();
	afx_msg void OnPlaceholderFile();
	afx_msg void OnPlaceholderLowpageSide();
	afx_msg void OnPlaceholderLowpageSheet();
	afx_msg void OnPlaceholderHighpageSide();
	afx_msg void OnPlaceholderHighpageSheet();
	afx_msg void OnPlaceholderPressDevice();
	afx_msg void OnPlaceholderJob();
	afx_msg void OnPlaceholderJobNum();
	afx_msg void OnPlaceholderCustomer();
	afx_msg void OnPlaceholderCustomerNum();
	afx_msg void OnPlaceholderCreator();
	afx_msg void OnPlaceholderCreationDate();
	afx_msg void OnPlaceholderLastModified();
	afx_msg void OnPlaceholderScheduledToPrint();
	afx_msg void OnPlaceholderOutputDate();
	afx_msg void OnPlaceholderNotes();
	afx_msg void OnPlaceholderColorName();
	afx_msg void OnPlaceholderPageNumList();
	afx_msg void OnPlaceholderPageNum();
	afx_msg void OnPlaceholderProductName();
	afx_msg void OnPlaceholderProductPartName();
	afx_msg void OnPlaceholderPaperGrain();
	afx_msg void OnPlaceholderPaperType();
	afx_msg void OnPlaceholderPaperGrammage();
	afx_msg void OnPlaceholderWorkStyle();
	afx_msg void OnPlaceholderFoldScheme();
	afx_msg BOOL OnToolTipNeedText(UINT id, NMHDR * pNMHDR, LRESULT * pResult);
};
