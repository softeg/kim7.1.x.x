// InplaceButton.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInplaceButton

CInplaceButton::CInplaceButton()
{
	m_lParam	 = 0;
	m_pBitmap	 = NULL;
	m_di		 = CDisplayItem();
	m_nTooltipID = -1;
}

CInplaceButton::~CInplaceButton()
{
	if (m_pBitmap)
	{
		m_pBitmap->DeleteObject();
		delete m_pBitmap;
		m_pBitmap = NULL;
	}

	if ((HBITMAP)m_bmSurface)
		m_bmSurface.DeleteObject();

	if (m_nTooltipID != -1)
	{
		if ( ! m_tooltip.m_hWnd)
			return;

		int nCount = m_tooltip.GetToolCount();
		for (int n = 0; n < nCount; n++)
			m_tooltip.DelTool(this, 1);	
	}
}

BOOL CInplaceButton::Create(LPCTSTR lpszCaption, int nBitmapID, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, int nTooltipID )
{
	//dwStyle|= BS_FLAT;

	if (CButton::Create(lpszCaption, (nBitmapID != -1) ? dwStyle | BS_BITMAP : dwStyle, rect, pParentWnd, nID) == FALSE)
		return FALSE;

	if (nBitmapID != -1)
		if (m_bmSurface.LoadBitmap(nBitmapID))
			SetBitmap((HBITMAP)m_bmSurface);
	
	m_nTooltipID = nTooltipID;

	if (m_nTooltipID != -1)
	{
		m_tooltip.Create(this);
		// Add tooltip 
		m_tooltip.AddTool(this, m_nTooltipID, CRect(0,0, 20, 20), 1);	// set 20,20 as default, because rect is 0,0,0,0
	}

	return TRUE;
}

void CInplaceButton::Activate(CRect rect, const CString& rString, LPARAM lParam, CDisplayItem di) 
{
	m_lParam = lParam;
	m_di	 = di;

	if (!IsWindowVisible())
	{
		SaveBackground(rect);
		GetParent()->SetFocus();
		ShowWindow(SW_HIDE);
		MoveWindow(rect, FALSE);	
		SetWindowText(rString);
		ShowWindow(SW_SHOW);	
		UpdateWindow();
	}

	if (m_nTooltipID != -1)
	{
		m_tooltip.SetToolRect(this, m_nTooltipID, CRect(0,0, rect.Width(), rect.Height()));
		m_tooltip.Activate(TRUE);
	}

	SetFocus();
	GetParent()->ValidateRect(rect);
}

void CInplaceButton::Move(CRect newRect, const CString& rString, LPARAM lParam) 
{
	m_lParam = lParam;

	if (!IsWindowVisible())
		return;

	CRect oldRect;
	GetWindowRect(oldRect);
	GetParent()->ScreenToClient(oldRect);

	if (m_nTooltipID != -1)
		m_tooltip.SetToolRect(this, m_nTooltipID, CRect(0,0, newRect.Width(), newRect.Height()));

	GetParent()->SetFocus();
	ShowWindow(SW_HIDE);
	RestoreBackground(oldRect);
	SaveBackground(newRect);
	MoveWindow(newRect, FALSE);	
	SetWindowText(rString);
	ShowWindow(SW_SHOW);
	UpdateWindow();
	SetFocus();
	GetParent()->ValidateRect(newRect);
}

void CInplaceButton::Deactivate() 
{
	if (!IsWindowVisible())
		return;

//	GetParent()->SetFocus();
	ShowWindow(SW_HIDE); // make invisible	

	if (m_pBitmap)
	{
		CRect oldRect;
		GetWindowRect(oldRect);
		GetParent()->ScreenToClient(oldRect);
		RestoreBackground(oldRect);
	}

	if (m_nTooltipID != -1)
		m_tooltip.Activate(FALSE);
}

void CInplaceButton::SaveBackground(const CRect& rect)
{
	if (m_pBitmap)
	{
		m_pBitmap->DeleteObject();
		delete m_pBitmap;
	}
	m_pBitmap = new CBitmap;

 	CDC* pDC = GetParent()->GetDC(); 
	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	m_pBitmap->CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	GetParent()->ReleaseDC(pDC);
}

void CInplaceButton::RestoreBackground(const CRect& rect)
{
	if (!m_pBitmap)
		return;

	CDC* pDC = GetParent()->GetDC(); 
	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBitmap);
	pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	GetParent()->ReleaseDC(pDC);

	m_pBitmap->DeleteObject();
	delete m_pBitmap;
	m_pBitmap = NULL;

	GetParent()->ValidateRect(rect);
}


BEGIN_MESSAGE_MAP(CInplaceButton, CButton)
	//{{AFX_MSG_MAP(CInplaceButton)
	ON_CONTROL_REFLECT(BN_CLICKED, OnClicked)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CInplaceButton 

void CInplaceButton::OnClicked() 
{
	if (m_pFnClickedHandler)
		m_pFnClickedHandler(GetParent());
}


BOOL CInplaceButton::PreTranslateMessage(MSG* pMsg) 
{
 	if (m_nTooltipID != -1)
		// Let the ToolTip process this message.
		m_tooltip.RelayEvent(pMsg);

	return CButton::PreTranslateMessage(pMsg);
}
