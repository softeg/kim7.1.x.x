#pragma once



class CJobDocumentationSplitter : public CSplitterWnd
{
    DECLARE_DYNAMIC(CJobDocumentationSplitter)
public:
	CJobDocumentationSplitter() { }

protected:
	virtual void OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect);

    DECLARE_MESSAGE_MAP()
    afx_msg LRESULT OnNcHitTest(CPoint point);
};



// CJobDocumentationFrame-Rahmen

class CJobDocumentationFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CJobDocumentationFrame)
protected:
	CJobDocumentationFrame();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CJobDocumentationFrame();

protected:
	CJobDocumentationSplitter m_wndSplitter;

protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnFilePageSetup();
};


