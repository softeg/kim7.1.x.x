#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "BookBlockDataView.h"
#include "BookBlockProdDataView.h"
#include "BookblockFrame.h"
#include "BookBlockView.h"
#include "PrintSheetView.h"
#include "GraphicComponent.h"
#include "ProductsView.h"



#define ID_GOTO_PREV_PAGE 4321
#define ID_GOTO_NEXT_PAGE ID_GOTO_PREV_PAGE - 1


// CBookBlockProdDataView

IMPLEMENT_DYNCREATE(CBookBlockProdDataView, CScrollView)

CBookBlockProdDataView::CBookBlockProdDataView()
{
	m_nProductIndex		= 0;
	m_nProductPartIndex = 0;

    m_DisplayList.Create(this, TRUE);
}

CBookBlockProdDataView::~CBookBlockProdDataView(void)
{
}


void CBookBlockProdDataView::DoDataExchange(CDataExchange* pDX)
{
	CScrollView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CBookBlockProdDataView, CScrollView)
    ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_GOTO_PREV_PAGE, OnGotoPrevPage)
	ON_COMMAND(ID_GOTO_NEXT_PAGE, OnGotoNextPage)
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_WM_SETCURSOR()
END_MESSAGE_MAP()


BOOL CBookBlockProdDataView::OnEraseBkgnd(CDC* pDC) 
{
	return TRUE;
}


// CBookBlockProdDataView-Meldungshandler

int CBookBlockProdDataView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CREATE_INPLACE_EDIT(InplaceEditShingling);
	CREATE_INPLACE_BUTTON(InplaceButtonShingling, IDB_RELEASE_LOCK, IDS_RELEASE_SHINGLING_LOCK);

	return 0;
}

BOOL CBookBlockProdDataView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	CBrush* pBkgrdBrush = new CBrush;
	pBkgrdBrush->CreateSolidBrush(RGB(127,162,207));

	lpszClassName = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)*pBkgrdBrush, 0);

	BOOL ret = CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	pBkgrdBrush->Detach();
	delete pBkgrdBrush;

	return ret;
}

void CBookBlockProdDataView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
	CScrollView::OnPrepareDC(pDC, pInfo);

	pDC->SetMapMode(MM_TEXT);
	pDC->SetWindowOrg(0, 0);
}

void CBookBlockProdDataView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	InitData();
}

void CBookBlockProdDataView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	SetScrollSizes(MM_TEXT, CalcDocSize());

	Invalidate();

	UpdateDialogControls( this, TRUE );
}

CSize CBookBlockProdDataView::CalcDocSize()
{
	CImpManDoc* pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return CSize(0, 0);

	return CSize(400, 400);
}

void CBookBlockProdDataView::InitData()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBookblockFrame* pFrame	= (CBookblockFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	CBookBlockDataView* pView = (CBookBlockDataView*)pFrame->m_wndSplitter.GetPane(0, 0);
	if ( ! pView)
		return;
	
	CBoundProduct&		rBoundProduct	  = pView->GetActiveBoundProduct();
	CBoundProductPart&	rBoundProductPart = pView->GetActiveBoundProductPart();
	m_nProductIndex						  = rBoundProduct.GetIndex();
	m_nProductPartIndex					  = rBoundProductPart.GetIndex();
	
	CPageTemplate& rNewPageTemplate = pDoc->m_PageTemplateList.GetHead(m_nProductPartIndex);
	POSITION	   pos				= pDoc->m_PageTemplateList.FindIndex(m_nPagePreviewIndex, m_nProductPartIndex);
	if (pos)
	{
		CPageTemplate& rCurPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
		if (rNewPageTemplate.m_nProductPartIndex != rCurPageTemplate.m_nProductPartIndex)
			m_nPagePreviewIndex = pDoc->m_PageTemplateList.GetIndex(&rNewPageTemplate, m_nProductPartIndex);
	}
	else
		m_nPagePreviewIndex = pDoc->m_PageTemplateList.GetIndex(&rNewPageTemplate, m_nProductPartIndex);

	if (m_nPagePreviewIndex > 0)
		if ( ! (m_nPagePreviewIndex % 2))
			m_nPagePreviewIndex--;
}

void CBookBlockProdDataView::GotoFoldSheet(CFoldSheet* pFoldSheet, int nFoldSheetPart)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! pFoldSheet)
		return;

	int nProductPartIndex = -1;
	int nPagePreviewIndex = -1;
	POSITION pos = pDoc->m_PageTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.CList::GetNext(pos);
		if (rPageTemplate.GetFoldSheet() == pFoldSheet)
		{
			CLayoutObject*	 pLayoutObject	 = rPageTemplate.GetLayoutObject();
			CLayout*		 pLayout		 = (pLayoutObject) ? pLayoutObject->GetLayout() : NULL; 
			CPrintSheet*	 pPrintSheet	 = (pLayout)	   ? pLayout->GetFirstPrintSheet() : NULL;
			CFoldSheetLayer* pLayer			 = (pPrintSheet)   ? pPrintSheet->GetFoldSheetLayer(pLayoutObject->m_nComponentRefIndex) : NULL;
			if (pLayer)
				if (nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
				{
					if (pLayer->m_FrontSpineArray[pLayoutObject->m_nLayerPageIndex] == RIGHT)
						continue;
				}
				else
					if (pLayer->m_FrontSpineArray[pLayoutObject->m_nLayerPageIndex] == LEFT)
						continue;

			nProductPartIndex = pFoldSheet->m_nProductPartIndex;
			int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(nProductPartIndex);
			nPagePreviewIndex = pDoc->m_PageTemplateList.GetIndex(&rPageTemplate, -nProductIndex - 1);
			break;
		}
	}

	if (nPagePreviewIndex > 0)
		if ( ! (nPagePreviewIndex % 2))
			nPagePreviewIndex--;

	if ( (nProductPartIndex == m_nProductPartIndex) && (nPagePreviewIndex == m_nPagePreviewIndex) )
		return;

	m_nProductPartIndex = nProductPartIndex;
	m_nPagePreviewIndex = nPagePreviewIndex; 

	CBookBlockDataView* pBookBlockDataView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
	if (pBookBlockDataView)
	{
		pBookBlockDataView->m_dlgShinglingAssistent.m_nGlobalProductPartIndex = m_nProductPartIndex;
		pBookBlockDataView->m_dlgShinglingAssistent.ChangeProductPart();
	}

	m_DisplayList.Invalidate();
	Invalidate();
	UpdateWindow();
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
}

void CBookBlockProdDataView::OnDraw(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(rcClient);

	CDC	memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap bitmap;	bitmap.CreateCompatibleBitmap(pDC, rcClient.Width(), rcClient.Height());
	memDC.SelectObject(&bitmap);

	CRect rcBackGround = rcClient; 
	memDC.FillSolidRect(rcBackGround, RGB(127,162,207));

	memDC.SaveDC();
	OnPrepareDC(&memDC);

	Draw(&memDC);
	//Draw(pDC);
	
	memDC.RestoreDC(-1);

	pDC->SaveDC();
	pDC->SetMapMode(MM_TEXT);
	pDC->SetWindowOrg(0, 0);
	pDC->SetWindowExt(rcClient.Width(), rcClient.Height());
	pDC->SetViewportOrg(0, 0);
	pDC->SetViewportExt(rcClient.Width(), rcClient.Height());

	pDC->BitBlt(0, 0, rcClient.Width(), rcClient.Height(), &memDC, 0, 0, SRCCOPY);
	pDC->RestoreDC(-1);
	bitmap.DeleteObject();
	memDC.DeleteDC();
}

void CBookBlockProdDataView::Draw(CDC* pDC)
{
	CImpManDoc* pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return;

	m_DisplayList.BeginRegisterItems(pDC);

	CRect rcFrameRect;
	GetClientRect(rcFrameRect);

	rcFrameRect.left += 20; rcFrameRect.top += 50; rcFrameRect.right -= 20; rcFrameRect.bottom -= 20;
	pDoc->m_PageTemplateList.Draw(pDC, rcFrameRect, m_nPagePreviewIndex, -m_nProductIndex - 1, FALSE, FALSE, TRUE, TRUE, &m_DisplayList, TRUE);

	m_DisplayList.EndRegisterItems(pDC);
}

void CBookBlockProdDataView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

BOOL CBookBlockProdDataView::OnMouseMoveNavigationFrame(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CRect rcButton(20, 20, 45, 45);
				if ( ! m_gotoPrevPageButton.m_hWnd)
				{
					m_gotoPrevPageButton.Create(_T(""), WS_CHILD | BS_OWNERDRAW, rcButton, this, ID_GOTO_PREV_PAGE);
					m_gotoPrevPageButton.SetBitmap(IDB_GOTO_PREV_HOT, IDB_GOTO_PREV, -1, XCENTER);
					m_gotoPrevPageButton.SetTransparent(TRUE);
					m_gotoPrevPageButton.m_lParam = NULL;
				}
				rcButton.OffsetRect(25, 0);
				if ( ! m_gotoNextPageButton.m_hWnd)
				{
					m_gotoNextPageButton.Create(_T(""), WS_CHILD | BS_OWNERDRAW, rcButton, this, ID_GOTO_NEXT_PAGE);
					m_gotoNextPageButton.SetBitmap(IDB_GOTO_NEXT_HOT, IDB_GOTO_NEXT, -1, XCENTER);
					m_gotoNextPageButton.SetTransparent(TRUE);
					m_gotoNextPageButton.m_lParam = NULL;
				}
				m_gotoPrevPageButton.ShowWindow(SW_SHOW);
				m_gotoNextPageButton.ShowWindow(SW_SHOW);
			}
			break;

	case 2:	// mouse leave
			if (m_gotoPrevPageButton.m_hWnd)
				m_gotoPrevPageButton.DestroyWindow();
			if (m_gotoNextPageButton.m_hWnd)
				m_gotoNextPageButton.DestroyWindow();
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CBookBlockProdDataView::OnSelectNavigationFrame(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	return TRUE;
}

BOOL CBookBlockProdDataView::OnDeselectNavigationFrame(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	return TRUE;
}

void CBookBlockProdDataView::OnGotoPrevPage()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_nPagePreviewIndex -= 2;

	if (m_nPagePreviewIndex < 0)
		m_nPagePreviewIndex = 0;

	if (m_nPagePreviewIndex > 0)
		if ( ! (m_nPagePreviewIndex%2) )	// even
			m_nPagePreviewIndex--;			// always set to odd index -> left page

	POSITION pos = pDoc->m_PageTemplateList.FindIndex(m_nPagePreviewIndex, -m_nProductIndex - 1);
	if (pos)
	{
		CPageTemplate& rCurPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
		CBookBlockDataView* pBookBlockDataView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
		if (pBookBlockDataView)
		{
			if (pBookBlockDataView->m_dlgShinglingAssistent.m_nGlobalProductPartIndex != rCurPageTemplate.m_nProductPartIndex)
			{
				pBookBlockDataView->m_dlgShinglingAssistent.m_nGlobalProductPartIndex = rCurPageTemplate.m_nProductPartIndex;
				pBookBlockDataView->m_dlgShinglingAssistent.ChangeProductPart();
			}
		}
	}

	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));

	m_DisplayList.Invalidate();

	OnUpdate(NULL, 0, NULL);
	Invalidate();
	UpdateWindow();
}

void CBookBlockProdDataView::OnGotoNextPage()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CBookblockFrame* pFrame = (CBookblockFrame*)GetParentFrame();

	m_nPagePreviewIndex += 2;

	int nProductPartIndex = -1;
	if (m_nPagePreviewIndex >= pDoc->m_PageTemplateList.GetCount(-m_nProductIndex - 1))
		m_nPagePreviewIndex = pDoc->m_PageTemplateList.GetCount(-m_nProductIndex - 1) - 1;

	if (m_nPagePreviewIndex > 0)
		if ( ! (m_nPagePreviewIndex%2) )	// even
			m_nPagePreviewIndex--;			// always set to odd index -> left page

	POSITION pos = pDoc->m_PageTemplateList.FindIndex(m_nPagePreviewIndex, -m_nProductIndex - 1);
	if (pos)
	{
		CPageTemplate& rCurPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
		CBookBlockDataView* pBookBlockDataView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
		if (pBookBlockDataView)
		{
			if (pBookBlockDataView->m_dlgShinglingAssistent.m_nGlobalProductPartIndex != rCurPageTemplate.m_nProductPartIndex)
			{
				pBookBlockDataView->m_dlgShinglingAssistent.m_nGlobalProductPartIndex = rCurPageTemplate.m_nProductPartIndex;
				pBookBlockDataView->m_dlgShinglingAssistent.ChangeProductPart();
			}
		}
	}

	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));

	m_DisplayList.Invalidate();

	OnUpdate(NULL, 0, NULL);
	Invalidate();
	UpdateWindow();
}

void CBookBlockProdDataView::OnLButtonDown(UINT nFlags, CPoint point)
{
    SetFocus();	// In order to deactivate eventually active InplaceEdit

	m_DisplayList.OnLButtonDown(point); // pass message to display list
	
	CScrollView::OnLButtonDown(nFlags, point);
}

BOOL CBookBlockProdDataView::OnSelectShinglingValueX(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// shingling value has been selected
{
	pDI->m_nState					= CDisplayItem::Normal;
    CPageTemplate* pPageTemplate	= (CPageTemplate*)pDI->m_pData;
	float*		   pfShinglingValue = (float*)pDI->m_pAuxData;
    if ( ! pPageTemplate || ! pfShinglingValue)
        return FALSE;

	if (pPageTemplate->m_bShinglingOffsetLocked)
	{
		CRect rectButton(pDI->m_BoundingRect.left - 25, pDI->m_BoundingRect.top - 2, pDI->m_BoundingRect.left - 5, pDI->m_BoundingRect.bottom + 2);
		m_InplaceButtonShingling.Activate(rectButton, "", 0, *pDI);
	}

    CString strShinglingValue;
	CPrintSheetView::TruncateNumber(strShinglingValue, *pfShinglingValue);
	CRect rcEdit = pDI->m_BoundingRect;
	rcEdit.InflateRect(4, 2);
    m_InplaceEditShingling.Activate(rcEdit, strShinglingValue, CInplaceEdit::GrowToRight, 0, *pDI);

	return TRUE;
}

BOOL CBookBlockProdDataView::OnSelectShinglingValueY(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// shingling value has been selected
{
	pDI->m_nState					= CDisplayItem::Normal;
    CPageTemplate* pPageTemplate	= (CPageTemplate*)pDI->m_pData;
	float*		   pfShinglingValue = (float*)pDI->m_pAuxData;
    if ( ! pPageTemplate || ! pfShinglingValue)
        return FALSE;

	if (pPageTemplate->m_bShinglingOffsetLocked)
	{
		CRect rectButton(pDI->m_BoundingRect.left - 25, pDI->m_BoundingRect.top - 2, pDI->m_BoundingRect.left - 5, pDI->m_BoundingRect.bottom + 2);
		m_InplaceButtonShingling.Activate(rectButton, "", 0, *pDI);
	}

    CString strShinglingValue;
	CPrintSheetView::TruncateNumber(strShinglingValue, *pfShinglingValue);
	CRect rcEdit = pDI->m_BoundingRect;
	rcEdit.InflateRect(4, 2);
    m_InplaceEditShingling.Activate(rcEdit, strShinglingValue, CInplaceEdit::GrowToRight, 0, *pDI);

	return TRUE;
}

void CBookBlockProdDataView::UpdateShinglingValue()
{
	CPageTemplate* pPageTemplate	= NULL;
	float*		   pfShinglingValue = (float*)m_InplaceEditShingling.m_di.m_pAuxData;

	if (!m_InplaceEditShingling.m_di.IsNull())
		pPageTemplate = (CPageTemplate*)m_InplaceEditShingling.m_di.m_pData;
    if ( ! pPageTemplate || ! pfShinglingValue)
        return;

    float fOldValue = *pfShinglingValue;
    CImpManDoc::GetDoc()->SetModifiedFlag();

    *pfShinglingValue = m_InplaceEditShingling.GetFloat();
	pPageTemplate->m_bShinglingOffsetLocked = TRUE;

    if (fOldValue == *pfShinglingValue)	// value not changed -> don't update view
    	return;

    InvalidateRect(m_InplaceEditShingling.m_di.m_AuxRect);
	m_DisplayList.Invalidate();
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockProdDataView));
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
}

void CBookBlockProdDataView::OnCharInplaceEditShingling(UINT nChar)	// Message from InplaceEdit
{
    if (nChar == VK_RETURN)	
    {
		SetFocus();
		UpdateShinglingValue();
	}
}

void CBookBlockProdDataView::OnKillFocusInplaceEditShingling() 		// Message from InplaceEdit
{
	CPoint pt;
	CRect  rect;
    GetCursorPos(&pt);  
	m_InplaceButtonShingling.GetWindowRect(rect);
	if (!rect.PtInRect(pt))
		m_InplaceButtonShingling.Deactivate();
}

void CBookBlockProdDataView::OnClickedInplaceButtonShingling() 		// Message from InplaceButton
{
	CPageTemplate* pPageTemplate = NULL;
	if (!m_InplaceButtonShingling.m_di.IsNull())
		pPageTemplate = (CPageTemplate*)m_InplaceButtonShingling.m_di.m_pData;
    if (!pPageTemplate)
        return;

	pPageTemplate->m_bShinglingOffsetLocked = FALSE;

	float fDummy;
	CImpManDoc::GetDoc()->m_PageTemplateList.CalcShinglingOffsets(fDummy, fDummy, fDummy, pPageTemplate->m_nProductPartIndex);

	m_InplaceButtonShingling.Deactivate();

    InvalidateRect(m_InplaceEditShingling.m_di.m_AuxRect);
	m_DisplayList.Invalidate();
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockProdDataView));
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
}

BOOL CBookBlockProdDataView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	SetCursor(theApp.m_CursorStandard);

	return CScrollView::OnSetCursor(pWnd, nHitTest, message);
}
