// DlgSelectPressDevicePopup.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgSelectPressDevicePopup.h"
#include "DlgPressDevice.h"


// CDlgSelectPressDevicePopup-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgSelectPressDevicePopup, CDialog)

CDlgSelectPressDevicePopup::CDlgSelectPressDevicePopup(CWnd* pParent /*=NULL*/, BOOL bRunModal /* = FALSE*/)
	: CDialog(CDlgSelectPressDevicePopup::IDD, pParent)
{
	m_smallFont.CreateFont (12, 0, 0, 0, FW_NORMAL,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_pPressDeviceImageList = new CImageList();
	m_pParent				= pParent;
	m_pPressDevice			= NULL;
	m_pLayout				= NULL;
	m_pFnSelChangeHandler	= NULL;
	m_bLocked				= FALSE;
	m_bRunModal				= bRunModal;
}

CDlgSelectPressDevicePopup::CDlgSelectPressDevicePopup(CPressDevice* pPressDevice, CLayout* pLayout, CWnd* pParent /*=NULL*/, BOOL bRunModal /* = FALSE*/)
	: CDialog(CDlgSelectPressDevicePopup::IDD, pParent)
{
	m_smallFont.CreateFont (12, 0, 0, 0, FW_NORMAL,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_pPressDeviceImageList = new CImageList();
	m_pParent				= pParent;
	m_pPressDevice			= pPressDevice;
	m_pLayout				= pLayout;
	m_pFnSelChangeHandler	= NULL;
	m_bLocked				= FALSE;
	m_bRunModal				= bRunModal;
}

CDlgSelectPressDevicePopup::~CDlgSelectPressDevicePopup()
{
	m_smallFont.DeleteObject();
	delete m_pPressDeviceImageList;
}

void CDlgSelectPressDevicePopup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PDS_PRESSDEVICELIST, m_pressDeviceList);
}


BEGIN_MESSAGE_MAP(CDlgSelectPressDevicePopup, CDialog)
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDC_PDS_DEFINE_PRESSDEVICE_BUTTON, &CDlgSelectPressDevicePopup::OnBnClickedPdsDefinePressdeviceButton)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_PDS_PRESSDEVICELIST, &CDlgSelectPressDevicePopup::OnNMCustomdrawPdsPressdevicelist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PDS_PRESSDEVICELIST, &CDlgSelectPressDevicePopup::OnLvnItemchangedPdsPressdevicelist)
	ON_WM_ERASEBKGND()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CDlgSelectPressDevicePopup-Meldungshandler

BOOL CDlgSelectPressDevicePopup::OnEraseBkgnd(CDC* pDC)
{
	CRect rcFrame;
	GetClientRect(rcFrame);

	COLORREF crStartColor = RGB(243,245,247);
	COLORREF crEndColor = RGB(236,233,212);
	Graphics graphics(pDC->m_hDC);
	Rect rc(rcFrame.left, rcFrame.top, rcFrame.Width(), rcFrame.Height());
	LinearGradientBrush br(rc, Color::Color(GetRValue(crStartColor), GetGValue(crStartColor), GetBValue(crStartColor)), 
							   Color::Color(GetRValue(crEndColor),	 GetGValue(crEndColor),	  GetBValue(crEndColor)), 
							   0);

	graphics.FillRectangle(&br, rc);

	return TRUE;
	//return CDialog::OnEraseBkgnd(pDC);
}

BOOL CDlgSelectPressDevicePopup::OnInitDialog()
{
	CDialog::OnInitDialog();

	if ( ! m_pPressDeviceImageList->m_hImageList)
	{
		CBitmap bitmap;
		m_pPressDeviceImageList->Create(48, 48, ILC_COLOR8, 1, 0);	// List not to grow
		bitmap.LoadBitmap(IDB_BKSCHEMELIST);
		m_pPressDeviceImageList->Add(&bitmap, BLACK);
		m_pressDeviceList.SetImageList(m_pPressDeviceImageList, LVSIL_SMALL);
		bitmap.DeleteObject();
	}
	InitPressDeviceList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgSelectPressDevicePopup::InitPressDeviceList()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_bLocked = TRUE;

	m_pressDeviceList.DeleteAllItems();

	int nSel = -1;
	POSITION pos = theApp.m_PressDeviceList.GetHeadPosition();
	while (pos) 
	{
		CPressDevice& rPressDevice = theApp.m_PressDeviceList.GetNext(pos);
		LV_ITEM item;
		item.mask		= LVIF_PARAM | LVIF_IMAGE | LVIF_STATE;
		item.stateMask	= LVIS_SELECTED;
		item.state		= 0;
		item.cchTextMax = MAX_TEXT;
		item.iItem		= m_pressDeviceList.GetItemCount(); 
		item.iSubItem	= 0;
		item.lParam		= (LPARAM)&rPressDevice;
		item.iImage		= 0;
		if (m_pPressDevice)
			if (m_pPressDevice->m_strName == rPressDevice.m_strName)
			{
				item.state = LVIS_SELECTED;
				nSel = item.iItem;
			}
		m_pressDeviceList.InsertItem(&item);
	}

	m_pressDeviceList.EnsureVisible(nSel, FALSE);

	m_bLocked = FALSE;
}

void CDlgSelectPressDevicePopup::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
			if (m_hWnd)
				if ( ! m_bRunModal)
				{
					m_bLocked = TRUE;
					DestroyWindow();
				}
				else
					OnCancel();
	}
	else
		m_bLocked = FALSE;
}

void CDlgSelectPressDevicePopup::OnNMCustomdrawPdsPressdevicelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);
			CPressDevice* pPressDevice = (CPressDevice*)lvitem.lParam;
			if ( ! pPressDevice)
				break;

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			float fPaperWidth  = (m_pLayout) ? m_pLayout->m_FrontSide.m_fPaperWidth  : 0.0f;
			float fPaperHeight = (m_pLayout) ? m_pLayout->m_FrontSide.m_fPaperHeight : 0.0f;

			pPressDevice->DrawPlate(pDC, rcItem, fPaperWidth, fPaperHeight);			

			CRect rcLabel;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			if (rcLabel.IsRectEmpty())
				break;

			COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

			CRect rcText = rcLabel; rcText.left += 5; rcText.top = rcText.CenterPoint().y - 20; rcText.bottom = rcText.top + 15;
			pDC->DrawText(pPressDevice->m_strName, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);
			rcText.OffsetRect(0, 15);

			CFont* pOldFont = pDC->SelectObject(&m_smallFont);
			pDC->SetTextColor(DARKGRAY);
			CString string;
			string.Format(_T("%.1f x %.1f"), pPressDevice->m_fPlateWidth, pPressDevice->m_fPlateHeight);
			pDC->DrawText(string, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);

			pDC->SetTextColor(crOldTextColor);
			pDC->SelectObject(pOldFont);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}

void CDlgSelectPressDevicePopup::OnLvnItemchangedPdsPressdevicelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	if (m_bLocked)
		return;

    HWND hWndListCtrl = pNMHDR->hwndFrom;
	if (pNMLV->iItem < 0)
		return;
	if (pNMLV->uNewState == pNMLV->uOldState)
		return;
	if ( ! (pNMLV->uNewState & LVIS_SELECTED) )
		return;

	LVITEM lvitem;
	lvitem.mask	= LVIF_PARAM;
	lvitem.iItem = pNMLV->iItem;
	lvitem.iSubItem = 0;
	ListView_GetItem(hWndListCtrl, &lvitem);
	CPressDevice* pPressDevice = (CPressDevice*)lvitem.lParam;

	if (m_pFnSelChangeHandler)
		m_pFnSelChangeHandler(pPressDevice, pNMLV->iItem, this);

	m_bLocked = TRUE;
	if (m_hWnd)
		if (m_bRunModal)
			EndDialog(-lvitem.iItem);
		else
			DestroyWindow();
}

void CDlgSelectPressDevicePopup::OnBnClickedPdsDefinePressdeviceButton()
{
	CDlgPressDevice dlg(FALSE, FRONTSIDE, m_pPressDevice, m_pLayout);  // don't modify direct inside dialog

	if (m_pPressDevice)
		dlg.m_pressDevice = *m_pPressDevice;

	m_bLocked = TRUE;
	if (dlg.DoModal() == IDOK)
		if ( (m_pPressDevice) && (m_pLayout) )
		{
			CPressDevice oldPressDevice;
			oldPressDevice  = *m_pPressDevice;
			*m_pPressDevice = dlg.m_pressDevice;
			float fGripper  = CDlgPressDevice::CheckGripper(&oldPressDevice, m_pPressDevice, &m_pLayout->m_FrontSide);
			m_pLayout->m_FrontSide.PositionLayoutSide(fGripper, m_pPressDevice);
			m_pLayout->m_BackSide.PositionLayoutSide (fGripper, m_pPressDevice);
		}
	m_bLocked = FALSE;

	InitPressDeviceList();
}

void CDlgSelectPressDevicePopup::OnDestroy()
{
	CDialog::OnDestroy();

	m_pPressDeviceImageList->DeleteImageList();
}
