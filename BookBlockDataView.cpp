// BookBlockDataView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "BookBlockDataView.h"
#include "MainFrm.h"
#include "BookBlockView.h"
#include "PrintSheetView.h"
#include "GraphicComponent.h"
#include "BookBlockProdDataView.h"
#include "PrintSheetPlanningView.h"


// CBVFoldSheetListCtrl

IMPLEMENT_DYNAMIC(CBVFoldSheetListCtrl, CListCtrl)

CBVFoldSheetListCtrl::CBVFoldSheetListCtrl()
{
	m_nHighlight = HIGHLIGHT_ROW;	
	m_nPrevSel	 = -1;
}

CBVFoldSheetListCtrl::~CBVFoldSheetListCtrl()
{
}


BEGIN_MESSAGE_MAP(CBVFoldSheetListCtrl, CListCtrl)
	ON_WM_MEASUREITEM_REFLECT()
	ON_NOTIFY_REFLECT(NM_CLICK, OnNMClick)
	ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, OnLvnItemchanged)
END_MESSAGE_MAP()



// CBVFoldSheetListCtrl-Meldungshandler


void CBVFoldSheetListCtrl::SelectAll()
{
	for (int i = 0; i < GetItemCount(); i++)
	{
		SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
	}
}

void CBVFoldSheetListCtrl::SelectRange(int nFrom, int nTo)
{
	for (int i = 0; i < GetItemCount(); i++)
	{
		if ((i + 1 >= nFrom) && (i + 1 <= nTo))
			SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
		else
			SetItemState(i, ~LVIS_SELECTED, LVIS_SELECTED);
	}
}

void CBVFoldSheetListCtrl::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	lpMeasureItemStruct->itemHeight = 25;
}

void CBVFoldSheetListCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	int nItem = lpDrawItemStruct->itemID;

	// Save dc state
	int nSavedDC = pDC->SaveDC();

	CFont font;
	font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);

	// Get item image and state info
	LV_ITEM lvi;
	lvi.mask = LVIF_STATE | LVIF_PARAM;
	lvi.iItem = nItem;
	lvi.iSubItem = 0;
	lvi.stateMask = 0xFFFF;		// get all state flags
	GetItem(&lvi);

	// Should the item be highlighted
	BOOL bHighlight = ((lvi.state & LVIS_DROPHILITED) || ( (lvi.state & LVIS_SELECTED) && ((GetFocus() == this))) );
//	BOOL bHighlight = ( (lvi.state & LVIS_SELECTED) && (GetFocus() == this) );

	if (GetStyle() & LVS_SHOWSELALWAYS)
	{
		POSITION pos = GetFirstSelectedItemPosition();
		while (pos)
		{
			if (GetNextSelectedItem(pos) == lvi.iItem)
			{
				bHighlight = TRUE;
				break;
			}
		}
	}

	// Get rectangles for drawing
	CRect rcBounds, rcLabel, rcIcon;
	GetItemRect(nItem, rcBounds, LVIR_BOUNDS);
	GetItemRect(nItem, rcLabel, LVIR_LABEL);
	GetItemRect(nItem, rcIcon, LVIR_ICON);
	CRect rcCol( rcBounds );

	CString sLabel = GetItemText( nItem, 0 );

	// Labels are offset by a certain amount  
	// This offset is related to the width of a space character
	int offset = pDC->GetTextExtent(_T(" "), 1 ).cx*2;

	CRect rcHighlight;
	CRect rcWnd;
	int nExt;
	switch( m_nHighlight )
	{
	case 0:
		nExt = pDC->GetOutputTextExtent(sLabel).cx + offset;
		rcHighlight = rcLabel;
		if( rcLabel.left + nExt < rcLabel.right )
			rcHighlight.right = rcLabel.left + nExt;
		break;
	case 1:
		rcHighlight = rcBounds;
		rcHighlight.left = rcLabel.left;
		break;
	case 2:
		GetClientRect(&rcWnd);
		rcHighlight = rcBounds;
//		rcHighlight.left = rcLabel.left;
//		rcHighlight.right = rcWnd.right;
		break;
	default:
		rcHighlight = rcLabel;
	}

	CFoldSheet* pFoldSheet = (CFoldSheet*)lvi.lParam;
	CLayout*	pLayout	   = (pFoldSheet) ? pFoldSheet->GetLayout(0) : NULL;

	COLORREF crTextOld = pDC->GetTextColor();

	// Draw the background color
	if( bHighlight )
	{
		pDC->SetTextColor(DARKBLUE);//::GetSysColor(COLOR_HIGHLIGHTTEXT));
		pDC->SetBkColor(RGB(193,211,251));//::GetSysColor(COLOR_HIGHLIGHT));

		pDC->FillRect(rcHighlight, &CBrush(RGB(193,211,251)));//::GetSysColor(COLOR_HIGHLIGHT)));
	}
	else
	{
		pDC->FillRect(rcHighlight, &CBrush(::GetSysColor(COLOR_WINDOW)));
	}

	// Set clip region
	rcCol.right = rcCol.left + GetColumnWidth(0);
	CRgn rgn;
	rgn.CreateRectRgnIndirect(&rcCol);
	pDC->SelectClipRgn(&rgn);
	rgn.DeleteObject();

	// Draw item label - Column 0
	rcLabel.left  += offset/2;
	rcLabel.right -= offset;

	rcIcon.right = rcIcon.left + rcIcon.Height();
	rcIcon.DeflateRect(0, 2);
	rcLabel.left = rcIcon.right + 5;

	if (pFoldSheet)
		pFoldSheet->DrawThumbnail(pDC, rcIcon);
	else
		pDC->DrawIcon(rcIcon.TopLeft() + CSize(5,5), theApp.LoadIcon(IDI_UNIMPOSED_PAGE));
	pDC->DrawText(sLabel,-1,rcLabel,DT_LEFT | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER | DT_END_ELLIPSIS);


	// Draw labels for remaining columns
	LV_COLUMN lvc;
	lvc.mask = LVCF_FMT | LVCF_WIDTH;

	if ( m_nHighlight == HIGHLIGHT_NORMAL )		// Highlight only first column
	{
		pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));
		pDC->SetBkColor(::GetSysColor(COLOR_WINDOW));
	}

	rcBounds.right = rcHighlight.right > rcBounds.right ? rcHighlight.right :
							rcBounds.right;
	rgn.CreateRectRgnIndirect(&rcBounds);
	pDC->SelectClipRgn(&rgn);

	BOOL bInsideNumeration = FALSE;
	CBookBlockDataView* pView = (CBookBlockDataView*)GetParent();
	if (pView)
		if (pView->m_pDlgNumeration->m_hWnd)
		{
			bInsideNumeration = TRUE;
			CString strIndex; strIndex.Format(_T(" (%d)"), nItem + 1);
			pDC->SetTextColor(LIGHTRED);
			pDC->DrawText(strIndex, -1, rcLabel, DT_SINGLELINE | DT_VCENTER | DT_RIGHT);
			pDC->SetTextColor(crTextOld);
		}

	CGraphicComponent gp;
	if (bInsideNumeration)
		if (pView->IsInsideNumerationRange(nItem))
			gp.DrawTransparentFrame(pDC, rcCol, RED);

	for(int nColumn = 1; GetColumn(nColumn, &lvc); nColumn++)
	{
		rcCol.left = rcCol.right;
		rcCol.right += lvc.cx;

		// Draw the background if needed
		if ( m_nHighlight == HIGHLIGHT_NORMAL )
			pDC->FillRect(rcCol, &CBrush(::GetSysColor(COLOR_WINDOW)));

		sLabel = GetItemText(nItem, nColumn);
		if (sLabel.GetLength() == 0)
			continue;

		// Get the text justification
		UINT nJustify = DT_LEFT;
		switch(lvc.fmt & LVCFMT_JUSTIFYMASK)
		{
		case LVCFMT_RIGHT:
			nJustify = DT_RIGHT;
			break;
		case LVCFMT_CENTER:
			nJustify = DT_CENTER;
			break;
		default:
			break;
		}

		rcLabel = rcCol;
		rcLabel.left += offset;
		rcLabel.right -= offset;

		if (bInsideNumeration)
			pDC->SetTextColor(MIDDLEGRAY);
				
		pDC->DrawText(sLabel, -1, rcLabel, nJustify | DT_SINGLELINE |
					DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS);
	}

	// Draw focus rectangle if item has focus
	//if (lvi.state & LVIS_FOCUSED && (GetFocus() == this))
	//	pDC->DrawFocusRect(rcHighlight);

	font.DeleteObject();
	// Restore dc
	pDC->RestoreDC( nSavedDC );
}

void CBVFoldSheetListCtrl::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMITEMACTIVATE* nm = (NMITEMACTIVATE*)pNMHDR;
	*pResult = 0;

	CBookBlockDataView* pView = (CBookBlockDataView*)GetParent();
	if ( ! pView)
		return;

	if (pView->m_pDlgNumeration->m_hWnd)
	{
		pView->m_pDlgNumeration->m_nSelection = CDlgNumeration::SelectionMarked;
		pView->m_pDlgNumeration->m_pfnInitCallback(pView->m_pDlgNumeration);
	}

	Invalidate();

	CBookblockFrame* pParentFrame = (CBookblockFrame*)pView->GetParentFrame();
	if (pParentFrame)
		if (pParentFrame->m_dlgOutput.m_hWnd)
		{
			pParentFrame->m_dlgOutput.m_nWhatSheets = 2;		// selected sheets
			pParentFrame->m_dlgOutput.UpdateData(FALSE);
			pParentFrame->m_dlgOutput.SelectPDFTargetCombo();
		}
}

void CBVFoldSheetListCtrl::OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	*pResult = 0;

	if ( ! (pNMLV->uNewState & LVNI_SELECTED))
		return;

//	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	CBookBlockDataView* pView = (CBookBlockDataView*)GetParent();
	if ( ! pView)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CFoldSheet* pFirstSelFoldSheet = NULL;
	POSITION	pos				   = pView->m_foldSheetList.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nCurSel = pView->m_foldSheetList.GetNextSelectedItem(pos);
		if (nCurSel >= 0)
		{
			CFoldSheet* pFoldSheet = (CFoldSheet*)pView->m_foldSheetList.GetItemData(nCurSel);
			if (pFoldSheet)
			{
				if ( ! pFirstSelFoldSheet)
					pFirstSelFoldSheet = pFoldSheet;

				//CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
				//pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, pFoldSheet->m_nProductPartIndex, pFoldSheet);

				//int nPages = 0;
				//if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
				//	nPages = pFoldSheet->m_nPagesLeft;
				//else
				//	nPages = pFoldSheet->m_nPagesLeft + pFoldSheet->m_nPagesRight;
				//int nNumUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(pFoldSheet->m_nProductPartIndex);
				//pView->GetDlgItem(IDC_BV_ADD_FOLDSHEET)->EnableWindow( (nPages > nNumUnImposedPages) ? FALSE : TRUE);
			
				if (pView->GetActiveBoundProduct().HasCover())
					pView->GetDlgItem(IDC_BV_STEP_ABOVE)->EnableWindow((nCurSel <= 1) ? FALSE : TRUE);
				else
					pView->GetDlgItem(IDC_BV_STEP_ABOVE)->EnableWindow((nCurSel <= 0) ? FALSE : TRUE);
				pView->GetDlgItem(IDC_BV_STEP_BELOW)->EnableWindow((nCurSel >= pView->m_foldSheetList.GetItemCount() - 1) ? FALSE : TRUE);

				//CBookBlockView* pBookBlockView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));
				//if (pBookBlockView)
				//{
				//	pBookBlockView->m_DisplayList.DeselectAll();
				//	pBookBlockView->m_bFoldSheetSelected = TRUE;
				//	pDoc->m_Bookblock.SelectFoldSheet(pDoc->m_Bookblock.GetFoldSheetIndex(pFoldSheet), pBookBlockView->m_DisplayList);
				//	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
				//}
			}
		}
	}

	if (pFirstSelFoldSheet)
	{
		CBookBlockProdDataView* pView = (CBookBlockProdDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockProdDataView));
		if (pView)
			pView->GotoFoldSheet(pFirstSelFoldSheet, CDescriptionItem::FoldSheetLeftPart);
	}
	else
	{
		pView->GetDlgItem(IDC_BV_STEP_ABOVE)->EnableWindow(FALSE);
		pView->GetDlgItem(IDC_BV_STEP_BELOW)->EnableWindow(FALSE);
	}
}


// CBookBlockDataView

IMPLEMENT_DYNCREATE(CBookBlockDataView, CFormView)

CBookBlockDataView::CBookBlockDataView()
	: CFormView(CBookBlockDataView::IDD)
{
	m_nBindingMethod = -1;
	m_nProductionType = -1;
	m_bkBrush.CreateSolidBrush(RGB(252, 220, 150));
	m_bkGrayBrush.CreateSolidBrush(RGB(250, 250, 250));
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
	m_smallFont.CreateFont (14, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_mediumFont.CreateFont(18, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_bigFont.CreateFont   (23, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));

	m_pDlgNumeration = new CDlgNumeration(this);//, TRUE);
}

CBookBlockDataView::~CBookBlockDataView()
{
	m_bkBrush.DeleteObject();
	m_bkGrayBrush.DeleteObject();
	m_hollowBrush.DeleteObject();
	m_smallFont.DeleteObject();
	m_mediumFont.DeleteObject();
	m_bigFont.DeleteObject();

	delete m_pDlgNumeration;
}

void CBookBlockDataView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_CBIndex(pDX, IDC_BV_BINDING_METHOD, m_nBindingMethod);
	DDX_CBIndex(pDX, IDC_BV_PRODUCTION_TYPE, m_nProductionType);
	m_bindingDefsSelection.DDX_BindingDefsCombo(pDX, IDC_BV_BINDING_METHOD, m_strBindingName);
	DDX_Control(pDX, IDC_BV_PRODUCT_COMBO, m_productCombo);
}

BEGIN_MESSAGE_MAP(CBookBlockDataView, CFormView)
	ON_WM_CTLCOLOR()
	ON_CBN_SELCHANGE(IDC_BV_BINDING_METHOD, &CBookBlockDataView::OnCbnSelchangeBvBindingMethod)
	ON_CBN_SELCHANGE(IDC_BV_PRODUCTION_TYPE, &CBookBlockDataView::OnCbnSelchangeBvProductionType)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BV_ADD_FOLDSHEET, &CBookBlockDataView::OnBnClickedBvAddFoldsheet)
	ON_BN_CLICKED(IDC_BV_REMOVE_FOLDSHEET, &CBookBlockDataView::OnBnClickedBvRemoveFoldsheet)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PP_FOLDSHEETLIST, &CBookBlockDataView::OnLvnItemchangedPpFoldsheetlist)
	ON_BN_CLICKED(IDC_BV_DONUMBER, &CBookBlockDataView::OnBnClickedBvDonumber)
	ON_BN_CLICKED(IDC_BV_STEP_ABOVE, &CBookBlockDataView::OnBnClickedBvStepAbove)
	ON_BN_CLICKED(IDC_BV_STEP_BELOW, &CBookBlockDataView::OnBnClickedBvStepBelow)
	ON_CBN_SELCHANGE(IDC_BV_PRODUCT_COMBO, &CBookBlockDataView::OnCbnSelchangeBvProductCombo)
END_MESSAGE_MAP()


// CBookBlockDataView-Diagnose

#ifdef _DEBUG
void CBookBlockDataView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CBookBlockDataView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

int CBookBlockDataView::GetPrintGroupIndex()
{
	return 0;
}

// CBookBlockDataView-Meldungshandler

void CBookBlockDataView::OnInitialUpdate()
{
	m_foldSheetList.SubclassDlgItem(IDC_PP_FOLDSHEETLIST,  this);

	CComboBoxEx* pBox = (CComboBoxEx*)GetDlgItem (IDC_BV_PRODUCT_COMBO);
	m_productImageList.Create(16, 16, ILC_COLOR24 | ILC_MASK, 2, 0);	// List not to grow
	m_productImageList.Add(theApp.LoadIcon(IDI_BOUND_PRODUCT));
	pBox->SetImageList(&m_productImageList);

// binding method combo ////////////////////////////
	pBox = (CComboBoxEx*)GetDlgItem (IDC_BV_BINDING_METHOD);
	CBitmap bitmap;
	m_bindingImageList.Create(24, 12, ILC_COLOR8, 2, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_PERFECT_BOUND);
	m_bindingImageList.Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	bitmap.LoadBitmap(IDB_SADDLE_STITCHED);
	m_bindingImageList.Add(&bitmap, BLACK);
	pBox->SetImageList(&m_bindingImageList);
	bitmap.DeleteObject();

// binding method combo
	m_bindingDefsSelection.InitComboBox(this, m_strBindingName, &m_nBindingMethod, IDC_BV_BINDING_METHOD);
	pBox = (CComboBoxEx*)GetDlgItem (IDC_BV_BINDING_METHOD);
	pBox->SetCurSel(m_nBindingMethod);
/////////////////////////////////////////////////


///// foldsheet list /////////////////////////////////////////////////////////////////////////////////////////////

	CString string;
	ListView_SetExtendedListViewStyle(m_foldSheetList.m_hWnd, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	int nIndex = 0;
	string.LoadString(IDS_SIGNATURE);
	m_foldSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,  100, 0);
	string.LoadString(IDS_PAGELISTHEADER_PAGES);
	m_foldSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,  140, 0);
	string.LoadString(IDS_SCHEME);
	m_foldSheetList.InsertColumn(nIndex++,  string, LVCFMT_LEFT,  120, 0);

	m_dlgShinglingAssistent.Create(IDD_SHINGLING_ASSISTENT, this);

	CFormView::OnInitialUpdate();

	CRect rcClient; GetClientRect(rcClient);
	SetScaleToFitSize(rcClient.Size());	//suppress scrollbars
}

void CBookBlockDataView::OnCbnSelchangeBvProductCombo()
{
	OnUpdate(NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
}

CBoundProduct& CBookBlockDataView::GetActiveBoundProduct()
{
	static private CBoundProduct nullBoundProduct;

	int nSel = m_productCombo.GetCurSel();
	if (nSel == CB_ERR)
		return nullBoundProduct;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullBoundProduct;

	int nProductIndex = m_productCombo.GetItemData(nSel);
	nProductIndex = abs(nProductIndex + 1);
	return pDoc->m_boundProducts.GetProduct(nProductIndex);
}

CBoundProductPart& CBookBlockDataView::GetActiveBoundProductPart()
{
	static private CBoundProductPart nullBoundProductPart;

	CBoundProduct& rBoundProduct = GetActiveBoundProduct();
	return rBoundProduct.GetPart(0);
}

void CBookBlockDataView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nSel = m_productCombo.GetCurSel();

	m_productCombo.ResetContent();
	COMBOBOXEXITEM item;
	item.mask = CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE | CBEIF_INDENT | CBEIF_LPARAM;
	for (int i = 0; i < pDoc->m_boundProducts.GetSize(); i++)
	{
		CBoundProduct& rBoundProduct = pDoc->GetBoundProduct(i);

		item.iItem			= m_productCombo.GetCount(); 
		item.pszText		= rBoundProduct.m_strProductName.GetBuffer();
		item.cchTextMax		= MAX_TEXT;
		item.lParam			= -i - 1;
		item.iImage			= 0;
		item.iSelectedImage = 0;
		item.iIndent		= 0;
		m_productCombo.InsertItem(&item);
	}
	m_productCombo.SetCurSel((nSel != CB_ERR) ? nSel : 0);

	CBoundProduct&		rBoundProduct	  = GetActiveBoundProduct();
	CBoundProductPart&	rBoundProductPart = GetActiveBoundProductPart();

	m_nProductionType	= pDoc->m_productionData.m_nProductionType;
	m_nBindingMethod	= m_bindingDefsSelection.GetBindingDefsIndex(rBoundProduct.m_bindingParams.m_bindingDefs);

	nSel = m_bindingDefsSelection.GetBindingDefsIndex(rBoundProduct.m_bindingParams.m_bindingDefs, TRUE);
	if (nSel < 0)
	{
		m_bindingDefsSelection.m_bindingDefsList.AddTail(rBoundProduct.m_bindingParams.m_bindingDefs);
		m_bindingDefsSelection.SaveData();
		m_bindingDefsSelection.InitComboBox(this, rBoundProduct.m_bindingParams.m_bindingDefs.m_strName, &m_nBindingMethod, IDC_BV_BINDING_METHOD);
	}

	InitializeFoldSheetList();

	m_dlgShinglingAssistent.m_nGlobalProductPartIndex = rBoundProductPart.GetIndex();
	m_dlgShinglingAssistent.ChangeProductPart();
	CBookBlockProdDataView* pView = (CBookBlockProdDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockProdDataView));
	if (pView)
	{
		pView->InitData();
		pView->Invalidate();
		pView->UpdateWindow();
	}

	UpdateData(FALSE);
}

void CBookBlockDataView::InitializeFoldSheetList()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBoundProduct& rBoundProduct = GetActiveBoundProduct();
	int			   nProductIndex = rBoundProduct.GetIndex();

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, -nProductIndex - 1);

	LVITEM   lvitem;
	lvitem.iItem = 0;
	int		 nSubItem = 0;
	CString  string, strMeasureFormat;
    for (int i = 0; i < sortedFoldSheets.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = sortedFoldSheets[i];
		if ( ! pFoldSheet)
			continue;
		if ( ! pFoldSheet->IsPartOfBoundProduct(-nProductIndex - 1))
			continue;
		//if (nProductPartIndex != -1)
		//	if (pFoldSheet->m_nProductPartIndex != nProductPartIndex)
		//		continue;
		//CFoldSheetLayer* pLayer = pFoldSheet->GetLayer(0);
		//CPrintSheet* pPrintSheet = (pLayer) ? pLayer->GetFirstPrintSheet() : NULL;
		//if ( ! pPrintSheet)
		//	continue;
		//pPrintSheet->InitializeFoldSheetTypeList();
		//CLayout*	  pLayout	   = pPrintSheet->GetLayout();
		//CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
		//if ( ! pLayout || ! pPressDevice)
		//	continue;

		nSubItem = 0;

//		if (pFoldSheet->m_LayerList.GetCount() > 1)
//				string.Format(_T("%s (%d)"), pFoldSheet->GetNumber(), pPrintSheet->m_FoldSheetTypeList[j].m_nFoldSheetLayerIndex + 1);
//			else
		string.Format(_T("%s"), pFoldSheet->GetNumber());
		lvitem.iSubItem = nSubItem++;
		lvitem.mask	    = LVIF_TEXT | LVIF_STATE | LVIF_PARAM;
		lvitem.state	 = 0;
		lvitem.stateMask = 0;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		lvitem.lParam	= (LPARAM)pFoldSheet;
		if (lvitem.iItem < m_foldSheetList.GetItemCount())
			m_foldSheetList.SetItem(&lvitem); 
		else
			m_foldSheetList.InsertItem(&lvitem); 

		lvitem.mask	    = LVIF_TEXT;
		string = pFoldSheet->GetPageRange();
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_foldSheetList.SetItem(&lvitem); 

		string = pFoldSheet->m_strFoldSheetTypeName;
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_foldSheetList.SetItem(&lvitem); 

		lvitem.iItem++;
	}

	int nUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(-nProductIndex - 1);
	nSubItem = 0;
	if (nUnImposedPages > 0)
	{
		lvitem.iSubItem = nSubItem++;
		lvitem.mask	    = LVIF_TEXT | LVIF_STATE | LVIF_PARAM;
		lvitem.state	 = 0;
		lvitem.stateMask = 0;
		lvitem.pszText  = _T("");
		lvitem.lParam	= NULL;
		if (lvitem.iItem < m_foldSheetList.GetItemCount())
			m_foldSheetList.SetItem(&lvitem); 
		else
			m_foldSheetList.InsertItem(&lvitem); 

		CString string2; string2.LoadString(IDS_PAGELISTHEADER_PAGES);
		string.Format(_T("%d %s"), nUnImposedPages, string2);
		lvitem.mask	    = LVIF_TEXT;
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_foldSheetList.SetItem(&lvitem); 

		string.LoadString(IDS_NOT_ASSIGNED);
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_foldSheetList.SetItem(&lvitem); 

		lvitem.iItem++;
	}

	while (lvitem.iItem < m_foldSheetList.GetItemCount())
		m_foldSheetList.DeleteItem(lvitem.iItem);

	if ( ! m_foldSheetList.GetFirstSelectedItemPosition())
		m_foldSheetList.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

 // Cause WM_MEASUREITEM to be sent by pretending to move CListCtrls window
	CRect rc;
	m_foldSheetList.GetWindowRect( &rc );
	WINDOWPOS wp;
	wp.hwnd = m_hWnd;
	wp.cx = rc.Width();
	wp.cy = rc.Height();
	wp.flags = SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER |
	SWP_NOZORDER;
	m_foldSheetList.SendMessage(WM_WINDOWPOSCHANGED, 0, (LPARAM)&wp);
}

void CBookBlockDataView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if ( ! m_foldSheetList.m_hWnd)
		return;
	if ( ! m_dlgShinglingAssistent.m_hWnd)
		return;

	CRect rcFrame, rcClient;
	GetClientRect(rcClient);

	m_dlgShinglingAssistent.GetWindowRect(rcFrame);
	int nShinglingDialogHeight = rcFrame.Height();

	CRect rcFoldSheetList;
	m_foldSheetList.GetWindowRect(rcFoldSheetList);	ScreenToClient(rcFoldSheetList);
	m_foldSheetList.SetWindowPos(NULL, rcFoldSheetList.left, rcFoldSheetList.top, rcFoldSheetList.Width(), rcClient.Height() - nShinglingDialogHeight/*rcFoldSheetList.top*/ - 100, SWP_NOZORDER);
	m_foldSheetList.GetWindowRect(rcFoldSheetList);	ScreenToClient(rcFoldSheetList);

	GetDlgItem(IDC_BV_DONUMBER)->GetWindowRect(rcFrame);	ScreenToClient(rcFrame);
	GetDlgItem(IDC_BV_DONUMBER)->SetWindowPos(NULL,			rcFrame.left, rcFoldSheetList.bottom + 5, rcFrame.Width(), rcFrame.Height(), SWP_NOZORDER);

	GetDlgItem(IDC_BV_STEP_ABOVE)->GetWindowRect(rcFrame);	ScreenToClient(rcFrame);
	GetDlgItem(IDC_BV_STEP_ABOVE)->SetWindowPos(NULL,		rcFrame.left, rcFoldSheetList.bottom + 5, rcFrame.Width(), rcFrame.Height(), SWP_NOZORDER);

	GetDlgItem(IDC_BV_STEP_BELOW)->GetWindowRect(rcFrame);	ScreenToClient(rcFrame);
	GetDlgItem(IDC_BV_STEP_BELOW)->SetWindowPos(NULL,		rcFrame.left, rcFoldSheetList.bottom + 5, rcFrame.Width(), rcFrame.Height(), SWP_NOZORDER);

	GetDlgItem(IDC_BV_ADD_FOLDSHEET)->GetWindowRect(rcFrame);		ScreenToClient(rcFrame);
	GetDlgItem(IDC_BV_ADD_FOLDSHEET)->SetWindowPos(NULL,			rcFrame.left, rcFoldSheetList.bottom + 5, rcFrame.Width(), rcFrame.Height(), SWP_NOZORDER);

	GetDlgItem(IDC_BV_REMOVE_FOLDSHEET)->GetWindowRect(rcFrame);	ScreenToClient(rcFrame);
	GetDlgItem(IDC_BV_REMOVE_FOLDSHEET)->SetWindowPos(NULL,			rcFrame.left, rcFoldSheetList.bottom + 5, rcFrame.Width(), rcFrame.Height(), SWP_NOZORDER);

	m_dlgShinglingAssistent.GetWindowRect(rcFrame);	ScreenToClient(rcFrame);
	m_dlgShinglingAssistent.SetWindowPos(NULL, rcFoldSheetList.left, rcFoldSheetList.bottom + 50, rcFrame.Width(), rcFrame.Height(), SWP_NOZORDER);

	OnUpdate(NULL, 0, NULL);
}

void CBookBlockDataView::OnCbnSelchangeBvProductionType()
{
	//UpdateData(TRUE);

	//CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;
	//int nProductPartIndex = GetPrintGroupIndex();
	//if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_printGroupList.GetSize()) )
	//	return;
	//pDoc->m_printGroupList[nProductPartIndex].GetProductionProfile().m_nProductionType = m_nProductionType;

	//Invalidate();
	//UpdateWindow();
	//theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
}

void CBookBlockDataView::OnCbnSelchangeBvBindingMethod()
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nTemp = m_nBindingMethod;	// workaround - GetActiveBoundProduct() chnges back m_nBindingMethod
	CBoundProduct& rBoundProduct = GetActiveBoundProduct();
	m_nBindingMethod = nTemp;

	int nProductIndex = rBoundProduct.GetIndex();

	int nNumDifferent = (rBoundProduct.HasCover()) ? 3 : 2;
	if (GetActiveBoundProduct().m_parts.GetSize() >= nNumDifferent)
		if (AfxMessageBox(IDS_CHANGE_PRODUCTPART_BINDING, MB_YESNO) == IDNO)
			return;

	rBoundProduct.m_bindingParams.m_bindingDefs = m_bindingDefsSelection.GetBindingDefs(m_nBindingMethod);
	switch(m_bindingDefsSelection.GetBindingDefs(m_nBindingMethod).m_nBinding)
	{
	case 0:	 pDoc->m_Bookblock.ModifyBindingMethod(CFinalProductDescription::PerfectBound,		-nProductIndex - 1); break;
	case 1:	 pDoc->m_Bookblock.ModifyBindingMethod(CFinalProductDescription::SaddleStitched,	-nProductIndex - 1); break;
	default: return;
	}
	pDoc->m_Bookblock.Reorganize(NULL, NULL);
	pDoc->m_Bookblock.ReNumberFoldSheets(-nProductIndex - 1);
	pDoc->m_PageTemplateList.SortPages();

	OnUpdate(NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockProdDataView));
}

HBRUSH CBookBlockDataView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

    if(nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_STATIC:
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(LIGHTBLUE);
				hbr = (HBRUSH)m_hollowBrush.GetSafeHandle();
				break;

		default:
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(DARKBLUE);
				hbr = (HBRUSH)m_bkGrayBrush.GetSafeHandle();
				break;
		}
    }

	return hbr;
}

void CBookBlockDataView::AddRemoveFoldsheet(BOOL bAdd)
{
	POSITION pos = m_foldSheetList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_foldSheetList.GetNextSelectedItem(pos) : -1;
	if (nCurSel == -1)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CFoldSheet* pFoldSheet = (CFoldSheet*)m_foldSheetList.GetItemData(nCurSel);
	if ( ! pFoldSheet)
		return;

	int nLayerIndex = 0;
	CPrintSheet* pPrintSheet = pFoldSheet->GetPrintSheet(nLayerIndex);
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;

	int nPages = 0;
	if (pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing)	
		nPages = pFoldSheet->m_nPagesLeft;
	else
		nPages = pFoldSheet->m_nPagesLeft + pFoldSheet->m_nPagesRight;

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, pFoldSheet->m_nProductPartIndex, pFoldSheet);

	if (bAdd)
	{
		int nNumUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(pFoldSheet->m_nProductPartIndex);
		if (nPages > nNumUnImposedPages)	// too many sheets
			return;
	}
	else
		if (sortedFoldSheets.GetSize() <= 1)
		{
			CString strMsg;
			AfxFormatString1(strMsg, IDS_REMOVE_SCHEME_MSG, pFoldSheet->m_strFoldSheetTypeName);
			if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
				return;
		}

	if (bAdd)
		pDoc->m_Bookblock.AddFoldSheets(pFoldSheet, 1, pFoldSheet->m_nProductPartIndex);
	else
		pDoc->m_Bookblock.RemoveFoldSheets(pFoldSheet, 1, pFoldSheet->m_nProductPartIndex);

	pDoc->m_Bookblock.Reorganize(pPrintSheet, pLayout);//, pFoldSheet->m_nProductPartIndex);

	pDoc->SetModifiedFlag();

	OnUpdate(NULL, 0, NULL);

	pDoc->UpdateAllViews(NULL);	
}

void CBookBlockDataView::OnBnClickedBvAddFoldsheet()
{
	AddRemoveFoldsheet(TRUE);
}

void CBookBlockDataView::OnBnClickedBvRemoveFoldsheet()
{
	AddRemoveFoldsheet(FALSE);
}

void CBookBlockDataView::OnLvnItemchangedPpFoldsheetlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	POSITION pos = m_foldSheetList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_foldSheetList.GetNextSelectedItem(pos) : -1;
	if (nCurSel >= 0)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			CFoldSheet* pFoldSheet = (CFoldSheet*)m_foldSheetList.GetItemData(nCurSel);
			if (pFoldSheet)
			{
				CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
				pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, pFoldSheet->m_nProductPartIndex, pFoldSheet);

				int nPages = 0;
				if (pFoldSheet->GetBoundProduct().m_bindingParams.m_bComingAndGoing)	
					nPages = pFoldSheet->m_nPagesLeft;
				else
					nPages = pFoldSheet->m_nPagesLeft + pFoldSheet->m_nPagesRight;
				int nNumUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(pFoldSheet->m_nProductPartIndex);
				GetDlgItem(IDC_BV_ADD_FOLDSHEET)->EnableWindow( (nPages > nNumUnImposedPages) ? FALSE : TRUE);
			
				GetDlgItem(IDC_BV_STEP_ABOVE)->EnableWindow((pFoldSheet == pDoc->m_Bookblock.GetFirstFoldSheet(pFoldSheet->m_nProductPartIndex)) ? FALSE : TRUE);
				GetDlgItem(IDC_BV_STEP_BELOW)->EnableWindow((nCurSel >= m_foldSheetList.GetItemCount() - 1) ? FALSE : TRUE);

				CBookBlockView* pBookBlockView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));
				if (pBookBlockView)
				{
					pBookBlockView->m_DisplayList.DeselectAll();
					pBookBlockView->m_bFoldSheetSelected = TRUE;
					pDoc->m_Bookblock.SelectFoldSheet(pDoc->m_Bookblock.GetFoldSheetIndex(pFoldSheet), pBookBlockView->m_DisplayList);
					theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
				}
			}
		}
	}
	else
	{
		GetDlgItem(IDC_BV_STEP_ABOVE)->EnableWindow(FALSE);
		GetDlgItem(IDC_BV_STEP_BELOW)->EnableWindow(FALSE);
	}

	*pResult = 0;
}

CBookBlockDataView* g_pThisBookBlockDataView = NULL;

void CBookBlockDataView::OnBnClickedBvDonumber()
{
	g_pThisBookBlockDataView = this;

	if (InitFoldSheetNumerationDialog(m_pDlgNumeration))
		if ( ! m_pDlgNumeration->m_hWnd)
		{
			m_pDlgNumeration->m_nSelection = CDlgNumeration::SelectionMarked;
			if ( ! GetFirstSelectedFoldSheet())
				m_pDlgNumeration->m_nSelection = CDlgNumeration::SelectionAll;
			m_pDlgNumeration->Create(IDD_NUMERATION_DIALOG, this);
			m_pDlgNumeration->m_nRangeFrom = 1;
			m_pDlgNumeration->m_nRangeTo   = m_foldSheetList.GetItemCount();
		}
}

BOOL CBookBlockDataView::InitFoldSheetNumerationDialog(CDlgNumeration* pDlg)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	int nLayerIndex = 0;
	CFoldSheet*  pFoldSheetFirst  = g_pThisBookBlockDataView->GetFirstSelectedFoldSheet();
	CFoldSheet*  pFoldSheetLast   = g_pThisBookBlockDataView->GetLastSelectedFoldSheet();

	pDlg->m_strTitle.LoadString(IDS_FOLDSHEETNUMERATION_TITLE);
	pDlg->m_pfnApplyCallback = ChangeFoldSheetNumeration;
	pDlg->m_pfnInitCallback  = InitFoldSheetNumerationDialog;
	//pDlg->m_nSelection = (pFoldSheetFirst) ? CDlgNumeration::SelectionMarked : CDlgNumeration::SelectionAll;

	if (pFoldSheetFirst && pFoldSheetLast)
	{
		pDlg->m_nRangeFrom = pFoldSheetFirst->m_nFoldSheetNumber;
		pDlg->m_nRangeTo   = pFoldSheetLast->m_nFoldSheetNumber;
	}
	else
	{
		pDlg->m_nRangeFrom = 1;
		pDlg->m_nRangeTo   = pDoc->m_Bookblock.m_FoldSheetList.GetCount();
	}

	pDlg->m_nStartNum = (pDlg->m_bReverse) ? pDlg->m_nRangeTo : pDlg->m_nRangeFrom;

	pDlg->m_nProductPartIndex = (pFoldSheetFirst) ? pFoldSheetFirst->m_nProductPartIndex : 0;

	pDlg->m_pViewToUpdate1 = g_pThisBookBlockDataView;

	if (pDlg->m_hWnd)
		pDlg->UpdateData(FALSE);

	return TRUE;
}

void CBookBlockDataView::ChangeFoldSheetNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nCurNum = (nStartNum - 1) * CDlgNumeration::GetSeqLen(strStyle) + 1;
	switch (nSelection)
	{
	case CDlgNumeration::SelectionAll:
	case CDlgNumeration::SelectionRange:	
		{
			for (int i = 0; i < g_pThisBookBlockDataView->m_foldSheetList.GetItemCount(); i++)
			{
				CFoldSheet* pFoldSheet = g_pThisBookBlockDataView->GetFoldSheet(i);
				if ( ! pFoldSheet)
					continue;

				if ( ( nSelection == CDlgNumeration::SelectionAll) ||
					 ((nSelection == CDlgNumeration::SelectionRange) && (i + 1 >= nRangeFrom) && (i + 1 <= nRangeTo)) )
				{
					if (strStyle == _T("reset"))
					{
						pFoldSheet->m_strNumerationStyle = _T("$n");
						pFoldSheet->m_nNumerationOffset = 0;
					}
					else
					{
						pFoldSheet->m_strNumerationStyle = strStyle;
						pFoldSheet->m_nNumerationOffset  = nCurNum - pFoldSheet->m_nFoldSheetNumber;
					}
					nCurNum += (bReverse) ? -1 : 1;
				}
			}
		}
		break;

	case CDlgNumeration::SelectionMarked:	
		{
			POSITION pos = g_pThisBookBlockDataView->m_foldSheetList.GetFirstSelectedItemPosition();
			while (pos)
			{
				int nItem = g_pThisBookBlockDataView->m_foldSheetList.GetNextSelectedItem(pos);
				if (nItem < 0)
					break;
				CFoldSheet* pFoldSheet = g_pThisBookBlockDataView->GetFoldSheet(nItem);
				
				if (pFoldSheet)	
				{
					if (strStyle == _T("reset"))
					{
						pFoldSheet->m_strNumerationStyle = _T("$n");
						pFoldSheet->m_nNumerationOffset = 0;
					}
					else
					{
						pFoldSheet->m_strNumerationStyle = strStyle;
						pFoldSheet->m_nNumerationOffset  = nCurNum - pFoldSheet->m_nFoldSheetNumber;
					}
				}
				nCurNum += (bReverse) ? -1 : 1;
			}
		}
		break;
	}

	pDoc->SetModifiedFlag(TRUE);

	theApp.OnUpdateView(RUNTIME_CLASS(CBookBlockView), NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView), NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), NULL, 0, NULL);
	g_pThisBookBlockDataView->InitializeFoldSheetList();
	g_pThisBookBlockDataView->m_foldSheetList.Invalidate();
	g_pThisBookBlockDataView->m_foldSheetList.UpdateWindow();
}

CFoldSheet* CBookBlockDataView::GetFirstSelectedFoldSheet(int* pnSel)
{
	POSITION pos = m_foldSheetList.GetFirstSelectedItemPosition();
	if (pos)
	{
		int nSel = m_foldSheetList.GetNextSelectedItem(pos);
		if (pnSel)
			*pnSel = nSel;
		if (nSel < 0)
			return NULL;

		return (CFoldSheet*)GetFoldSheet(nSel);
	}
	if (pnSel)
		*pnSel = -1;
	return NULL;
}

CFoldSheet* CBookBlockDataView::GetLastSelectedFoldSheet(int* pnSel)
{
	CFoldSheet* pFoldSheet = NULL;
	POSITION pos = m_foldSheetList.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nSel = m_foldSheetList.GetNextSelectedItem(pos);
		if (pnSel)
			*pnSel = nSel;
		if (nSel < 0)
			continue;

		pFoldSheet = GetFoldSheet(nSel);
	}
	return pFoldSheet;
}

CFoldSheet* CBookBlockDataView::GetFoldSheet (int nSel)
{
	if ( (nSel < 0) || (nSel >= m_foldSheetList.GetItemCount()) )
		return NULL;

	return (CFoldSheet*)m_foldSheetList.GetItemData(nSel);
}

BOOL CBookBlockDataView::IsInsideNumerationRange(int nItemIndex)
{
	switch (m_pDlgNumeration->m_nSelection)
	{
	case CDlgNumeration::SelectionAll:		return TRUE; 

	case CDlgNumeration::SelectionRange:	if ((nItemIndex + 1 >= (int)m_pDlgNumeration->m_nRangeFrom) && (nItemIndex + 1 <= (int)m_pDlgNumeration->m_nRangeTo))
												return TRUE;
											break;

	case CDlgNumeration::SelectionMarked:	if (m_foldSheetList.GetItemState(nItemIndex, LVIS_SELECTED))
												return TRUE;
											break;
	}

	return FALSE;
}

void CBookBlockDataView::OnBnClickedBvStepAbove()
{
	POSITION pos = m_foldSheetList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_foldSheetList.GetNextSelectedItem(pos) : -1;
	if (nCurSel < 0)
		return;

	MoveFoldSheet((CFoldSheet*)m_foldSheetList.GetItemData(nCurSel), TRUE);

	m_foldSheetList.SetItemState(nCurSel, ~LVIS_SELECTED, LVIS_SELECTED);
	if (nCurSel > 0)
		m_foldSheetList.SetItemState(nCurSel - 1, LVIS_SELECTED, LVIS_SELECTED);

	OnUpdate(NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
}

void CBookBlockDataView::OnBnClickedBvStepBelow()
{
	POSITION pos = m_foldSheetList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_foldSheetList.GetNextSelectedItem(pos) : -1;
	if (nCurSel < 0)
		return;

	MoveFoldSheet((CFoldSheet*)m_foldSheetList.GetItemData(nCurSel), FALSE);

	m_foldSheetList.SetItemState(nCurSel, ~LVIS_SELECTED, LVIS_SELECTED);
	if (nCurSel < m_foldSheetList.GetItemCount() - 1)
		m_foldSheetList.SetItemState(nCurSel + 1, LVIS_SELECTED, LVIS_SELECTED);

	OnUpdate(NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
}

void CBookBlockDataView::MoveFoldSheet(CFoldSheet* pFoldSheet, BOOL bMoveUp)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = pFoldSheet->m_nProductPartIndex;
	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);

	int	nFoldSheetIndex = pDoc->m_Bookblock.GetFoldSheetIndex(pFoldSheet);
	int nFoldSheetPart  = (bMoveUp) ? CDescriptionItem::FoldSheetLeftPart : CDescriptionItem::FoldSheetRightPart;
	if (rProductPart.GetBoundProduct().m_bindingParams.m_bindingDefs.m_nBinding == CFinalProductDescription::SaddleStitched)
		nFoldSheetPart = CDescriptionItem::FoldSheetLeftPart;

	POSITION pos = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem	= pDoc->m_Bookblock.m_DescriptionList.GetAt(pos);
		if (nFoldSheetIndex == rItem.m_nFoldSheetIndex)
			if (rItem.m_nFoldSheetPart == nFoldSheetPart)
				break;

		pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
	}
	if (pos)
	{
		if (bMoveUp)
			pDoc->m_Bookblock.m_DescriptionList.GetPrev(pos);
		else
			pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		if ( ! pos)
			return;

		CDescriptionItem& rItem	= pDoc->m_Bookblock.m_DescriptionList.GetAt(pos);
		int nNewFoldSheetIndex = rItem.m_nFoldSheetIndex;
		if (nNewFoldSheetIndex == nFoldSheetIndex)
			if (bMoveUp && (nNewFoldSheetIndex > 0) )
				nNewFoldSheetIndex--;
			else
				if ( ! bMoveUp && (nNewFoldSheetIndex < pDoc->m_Bookblock.m_DescriptionList.GetCount() - 1) )
					nNewFoldSheetIndex++;
		//if (pDoc->m_Bookblock.GetFoldSheetByIndex(nNewFoldSheetIndex)) 
		//	if (pDoc->m_Bookblock.GetFoldSheetByIndex(nNewFoldSheetIndex)->m_nProductPartIndex != nProductPartIndex) 
		//		return;

		pos = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
		while (pos)
		{
			CDescriptionItem& rItem	= pDoc->m_Bookblock.m_DescriptionList.GetAt(pos);
			if (rItem.m_nFoldSheetIndex == nFoldSheetIndex)
				rItem.m_nFoldSheetIndex = nNewFoldSheetIndex;
			else
				if (rItem.m_nFoldSheetIndex == nNewFoldSheetIndex)
					rItem.m_nFoldSheetIndex = nFoldSheetIndex;

			pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		}
	}

	pDoc->m_Bookblock.Reorganize(NULL, NULL);
}

void CBookBlockDataView::SelectFoldSheet(CFoldSheet* pFoldSheetToSelect, int nFoldSheetPart)
{
	POSITION pos = m_foldSheetList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_foldSheetList.GetNextSelectedItem(pos) : -1;
	if (nCurSel >= 0)
		m_foldSheetList.SetItemState(nCurSel, ~LVIS_SELECTED, LVIS_SELECTED);
	
	for (int i = 0; i < m_foldSheetList.GetItemCount(); i++)
	{
		if ( (CFoldSheet*)m_foldSheetList.GetItemData(i) == pFoldSheetToSelect)
		{
			m_foldSheetList.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);

			CBookBlockProdDataView* pBookBlockProdDataView = (CBookBlockProdDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockProdDataView));
			if (pBookBlockProdDataView)
				pBookBlockProdDataView->GotoFoldSheet(pFoldSheetToSelect, nFoldSheetPart);

			return;
		}
	}
}
