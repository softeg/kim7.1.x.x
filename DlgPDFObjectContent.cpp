// DlgPDFObjectContent.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "MainFrm.h"
#ifdef PDF
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"
#endif
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PageListView.h"
#include "DlgJobColorDefinitions.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PageListFrame.h"
#include "PageSourceListView.h"
#include "PageListTableView.h"
#ifdef PDF
#include "PDFReader.h"
#endif
#include "PageListDetailView.h"
#include "Shlwapi.h"
#include "DlgPDFObjectContent.h"
#include "ObjectPivotControl.h"
#include "PrintSheetObjectPreview.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFObjectContent 


IMPLEMENT_DYNCREATE(CDlgPDFObjectContent, CDialog)


CDlgPDFObjectContent::CDlgPDFObjectContent(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPDFObjectContent::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPDFObjectContent)
	m_strDocFullpath = _T("");
	m_fContentOffsetX = 0.0f;
	m_fContentOffsetY = 0.0f;
	m_fContentScaleX = 0.0f;
	m_fContentScaleY = 0.0f;
	m_bContentDuplicate = FALSE;
	m_bContentTrim = FALSE;
	m_fContentGridX = 0.0f;
	m_fContentGridY = 0.0f;
	m_bMap2AllColors = FALSE;
	m_bMap2SpotColors = FALSE;
	m_nMap2SpotColorIndex = 0;
	//}}AFX_DATA_INIT

	m_pParent = NULL;
}

CDlgPDFObjectContent::~CDlgPDFObjectContent()
{
}


BOOL CDlgPDFObjectContent::PreTranslateMessage(MSG* pMsg) 
{
	UpdateDialogControls( this, TRUE );

	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}	
 
void CDlgPDFObjectContent::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPDFObjectContent)
	DDX_Control(pDX, IDC_CONTENTSCALESPIN_X, m_contentScaleSpinX);
	DDX_Control(pDX, IDC_CONTENTSCALESPIN_Y, m_contentScaleSpinY);
	DDX_Control(pDX, IDC_CONTENTOFFSETSPIN_X, m_contentOffsetSpinX);
	DDX_Control(pDX, IDC_CONTENTOFFSETSPIN_Y, m_contentOffsetSpinY);
	DDX_Control(pDX, IDC_CONTENTGRIDSPIN_X, m_contentGridSpinX);
	DDX_Control(pDX, IDC_CONTENTGRIDSPIN_Y, m_contentGridSpinY);
	DDX_Text(pDX, IDC_PDFMARK_FILE, m_strDocFullpath); 
	DDX_Measure(pDX, IDC_CONTENTOFFSET_X, m_fContentOffsetX);
	DDX_Measure(pDX, IDC_CONTENTOFFSET_Y, m_fContentOffsetY);
	DDX_Text(pDX, IDC_CONTENTSCALE_X, m_fContentScaleX);
	DDX_Text(pDX, IDC_CONTENTSCALE_Y, m_fContentScaleY);
	DDX_Measure(pDX, IDC_CONTENTGRID_X, m_fContentGridX);
	DDX_Measure(pDX, IDC_CONTENTGRID_Y, m_fContentGridY);
	DDX_Check(pDX, IDC_ASSIGN_ALL_PRINTCOLORS, m_bMap2AllColors);
	DDX_Check(pDX, IDC_MAP_SPOTCOLORS, m_bMap2SpotColors);
	DDX_CBIndex(pDX, IDC_MAP_SPOTCOLOR_INDEX_COMBO, m_nMap2SpotColorIndex);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CONTENT_DUPLICATE, m_bContentDuplicate);
	DDX_Check(pDX, IDC_CONTENT_TRIM, m_bContentTrim);
}


BEGIN_MESSAGE_MAP(CDlgPDFObjectContent, CDialog)
	//{{AFX_MSG_MAP(CDlgPDFObjectContent)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTOFFSETSPIN_X, OnDeltaposContentoffsetspinX)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTOFFSETSPIN_Y, OnDeltaposContentoffsetspinY)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTSCALESPIN_X, OnDeltaposContentscalespinX)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTSCALESPIN_Y, OnDeltaposContentscalespinY)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTGRIDSPIN_X, OnDeltaposContentgridspinX)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTGRIDSPIN_Y, OnDeltaposContentgridspinY)
	ON_BN_CLICKED(IDC_MODIFY_PRINTCOLOR, OnModifyPrintcolor)
	ON_BN_CLICKED(IDC_INSERT_PRINTCOLOR, OnInsertPrintcolor)
	ON_BN_CLICKED(IDC_ASSIGN_ALL_PRINTCOLORS, OnAssignAllPrintcolors)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_PDFOBJECT_COLORCONTROL, OnItemchangedObjectcolorControl)
	ON_BN_CLICKED(IDC_MAP_SPOTCOLORS, OnMapSpotcolors)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_LAUNCH_ACROBAT, OnLaunchAcrobat)
	ON_BN_CLICKED(IDC_REMOVE_PRINTCOLOR, OnRemovePrintcolor)
	ON_BN_CLICKED(IDC_REMOVE_PDFFILE, OnRemovePdffile)
	ON_BN_CLICKED(IDC_REGISTER_MARK, OnRegisterPDF)
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_CHANGED_PIVOT, OnChangedPivot)
	ON_CBN_SELCHANGE(IDC_MAP_SPOTCOLOR_INDEX_COMBO, &CDlgPDFObjectContent::OnCbnSelchangeMapSpotcolorIndexCombo)
	ON_UPDATE_COMMAND_UI(IDC_MAP_SPOTCOLOR_INDEX_COMBO, OnUpdateMapSpotcolorIndexCombo)
	ON_BN_CLICKED(IDC_CONTENT_DUPLICATE, OnContentDuplicate)
	ON_BN_CLICKED(IDC_CONTENT_TRIM, OnContentTrim)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPDFObjectContent 

BOOL CDlgPDFObjectContent::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_printColorList.Initialize(this);

	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	((CButton*)GetDlgItem(IDC_REMOVE_PDFFILE))->SetIcon(theApp.LoadIcon(IDI_DELETE));
	((CButton*)GetDlgItem(IDC_LAUNCH_ACROBAT))->SetIcon(theApp.LoadIcon(IDI_ACROBAT));

	GetDlgItem(IDC_PDFMARK_FILE)->ModifyStyle(0, SS_PATHELLIPSIS);

	m_contentPivotCtrl.SubclassDlgItem(IDC_CONTENT_PIVOT, this);
	m_contentPivotCtrl.m_pParent = this;
	m_contentPivotCtrl.m_nActiveCell = -1;
	m_contentPivotCtrl.Reinit(IDD_PDFOBJECT_CONTENT);

	m_contentOffsetSpinX.SetRange(0, 1);
	m_contentOffsetSpinY.SetRange(0, 1);
	m_contentScaleSpinX.SetRange(1, 9999);
	m_contentScaleSpinY.SetRange(1, 9999);
	m_contentGridSpinX.SetRange(0, 1);
	m_contentGridSpinY.SetRange(0, 1);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgPDFObjectContent::OnDeltaposContentoffsetspinX(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_contentOffsetSpinX.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgPDFObjectContent::OnDeltaposContentoffsetspinY(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_contentOffsetSpinY.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgPDFObjectContent::OnDeltaposContentscalespinX(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	if (pNMUpDown->iDelta > 0)
		pNMUpDown->iPos++;
	else
		pNMUpDown->iPos--;
	
	*pResult = 0;
}

void CDlgPDFObjectContent::OnDeltaposContentscalespinY(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	if (pNMUpDown->iDelta > 0)
		pNMUpDown->iPos++;
	else
		pNMUpDown->iPos--;
	
	*pResult = 0;
}

void CDlgPDFObjectContent::OnDeltaposContentgridspinX(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	SpinMeasure(m_contentGridSpinX.GetBuddy(), pNMUpDown->iDelta, 1.0f);

	*pResult = 1;
}

void CDlgPDFObjectContent::OnDeltaposContentgridspinY(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	SpinMeasure(m_contentGridSpinY.GetBuddy(), pNMUpDown->iDelta, 1.0f);

	*pResult = 1;
}

void CDlgPDFObjectContent::OnModifyPrintcolor() 
{
	CDlgJobColorDefinitions dlg(TRUE);	// TRUE = Overwrite mode
	CPageTemplate*			pTemplate		   = NULL;
	CPageSource*			pObjectSource	   = NULL;
	int						nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pLayoutObject->m_nType == CLayoutObject::ControlMark)
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				pObjectSource	   = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
			}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate)
		return;
	if (pTemplate->m_bMap2AllColors) 
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if ( ! pos)
	{
		for (int i = 0; i < m_printColorList.GetItemCount(); i++)
			m_printColorList.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
		pos = m_printColorList.GetFirstSelectedItemPosition();
	}

	while (pos)
	{
		int				   nItem			   = m_printColorList.GetNextSelectedItem(pos);
		CPageSourceHeader* pObjectSourceHeader = m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
		SEPARATIONINFO*	   pSepInfo			   = m_printColorList.m_printColorListItems[nItem].m_pSepInfo;
		BOOL			   bInserted		   = FALSE;
		if (pObjectSourceHeader)
		{
			if (pSepInfo)
			{
				CColorDefinition* pColorDef = m_pParent->ColorDefTable_GetColorDef(pSepInfo->m_nColorDefinitionIndex);
				if (pColorDef)
				{
					dlg.m_PSColorDefTable.InsertColorDef(*pColorDef);
					bInserted = TRUE;
				}
			}
			if ( ! bInserted)
			{
				CColorDefinition colorDef;
				colorDef.m_strColorName = pObjectSourceHeader->m_strColorName;
				colorDef.m_rgb			= pObjectSourceHeader->m_rgb;
				colorDef.m_nColorIndex	= pObjectSourceHeader->m_nColorIndex;
				dlg.m_PSColorDefTable.InsertColorDef(colorDef);
			}
		}
	}

	if (dlg.DoModal() == IDCANCEL)
		return;

	int i = 0;
	pos = m_printColorList.GetFirstSelectedItemPosition();
	while (pos && (i < dlg.m_PSColorDefTable.GetSize()) )
	{
		int				   nItem			   = m_printColorList.GetNextSelectedItem(pos);
		CPageSourceHeader* pObjectSourceHeader = m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
		SEPARATIONINFO*	   pSepInfo			   = m_printColorList.m_printColorListItems[nItem].m_pSepInfo;
		if (pObjectSourceHeader)
		{
			BOOL bIsFirst = FALSE;
			if (pSepInfo)
			{
				pSepInfo->m_nColorDefinitionIndex = m_pParent->ColorDefTable_FindColorDefinitionIndex(dlg.m_PSColorDefTable[i].m_strColorName);
				if (pSepInfo->m_nColorDefinitionIndex < 0)
					pSepInfo->m_nColorDefinitionIndex = m_pParent->ColorDefTable_InsertColorDef(dlg.m_PSColorDefTable[i]);

				bIsFirst = (pSepInfo == pTemplate->GetFirstSepInfo(pSepInfo->m_nPageNumInSourceFile)) ? TRUE : FALSE;
			}

			if ( ! pSepInfo || bIsFirst )
			{
				pObjectSourceHeader->m_strColorName	= dlg.m_PSColorDefTable[i].m_strColorName;
				pObjectSourceHeader->m_rgb			= dlg.m_PSColorDefTable[i].m_rgb;
				pObjectSourceHeader->m_nColorIndex	= dlg.m_PSColorDefTable[i].m_nColorIndex;
			}
			i++;
		}
	}

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable());

	theApp.UpdateView(RUNTIME_CLASS(CPageListView));
	theApp.UpdateView(RUNTIME_CLASS(CPageListDetailView));
	theApp.UpdateView(RUNTIME_CLASS(CPageListTableView));
	theApp.UpdateView(RUNTIME_CLASS(CPageSourceListView));

	OnItemchangedObjectcolorControl(NULL, NULL);
}

void CDlgPDFObjectContent::OnInsertPrintcolor() 
{
	CDlgJobColorDefinitions dlg;	
	CColorDefinition		colorDef;
	CPageTemplate*			pTemplate		   = NULL;
	CPageSource*			pObjectSource	   = NULL;
	int						nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pLayoutObject->m_nType == CLayoutObject::ControlMark)
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				pObjectSource = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
			}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate)
		return;
	if (pTemplate->m_bMap2AllColors) 
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if ( ! pos)
	{
		if (m_printColorList.GetItemCount())
			m_printColorList.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
		pos = m_printColorList.GetFirstSelectedItemPosition();
	}

	pos	= m_printColorList.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nItem = m_printColorList.GetNextSelectedItem(pos);
		if (nItem < m_printColorList.m_printColorListItems.GetSize())
		{
			CPageSourceHeader* pObjectSourceHeader  = m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
			int				   nPageNumInSourceFile = m_printColorList.m_printColorListItems[nItem].m_nPageIndex + 1;
			SEPARATIONINFO*	   pSepInfo				= m_printColorList.m_printColorListItems[nItem].m_pSepInfo;
			if (pObjectSourceHeader)
			{
				BOOL bEnableOnly = FALSE;
				if (pSepInfo)
					if (pSepInfo->m_nColorDefinitionIndex == -1)
						bEnableOnly = TRUE;

				if (bEnableOnly)
				{
					pSepInfo->m_nColorDefinitionIndex = m_pParent->GetActColorDefTable()->FindColorDefinitionIndex(pObjectSourceHeader->m_strColorName);
					if (pSepInfo->m_nColorDefinitionIndex == -1)		// color not in table
					{
						CColorDefinition colorDef(pObjectSourceHeader->m_strColorName, pObjectSourceHeader->m_rgb, CColorDefinition::Spot, pObjectSourceHeader->m_nColorIndex);
						pSepInfo->m_nColorDefinitionIndex = m_pParent->GetActColorDefTable()->InsertColorDef(colorDef);
					}
				}
				else
				{
					if (m_printColorList.GetSelectedCount() > 1)	// multiple selected - only enable defined
						continue;
					if (dlg.DoModal() == IDCANCEL)
						return;
					for (int i = 0; i < dlg.m_PSColorDefTable.GetSize(); i++)
					{
						int nObjectSourceIndex = (pSepInfo) ? pSepInfo->m_nPageSourceIndex : -1;
						SEPARATIONINFO sepInfo = {(short)nObjectSourceIndex, (short)nPageNumInSourceFile, (short)m_pParent->ColorDefTable_FindColorDefinitionIndex(dlg.m_PSColorDefTable[i].m_strColorName)};
						if (sepInfo.m_nColorDefinitionIndex < 0)
							sepInfo.m_nColorDefinitionIndex = m_pParent->ColorDefTable_InsertColorDef(dlg.m_PSColorDefTable[i]);
						pTemplate->InsertSepInfo(&sepInfo);
					}
				}
			}
		}
	}

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable());

	theApp.UpdateView(RUNTIME_CLASS(CPageListView));
	theApp.UpdateView(RUNTIME_CLASS(CPageListDetailView));
	theApp.UpdateView(RUNTIME_CLASS(CPageListTableView));
	theApp.UpdateView(RUNTIME_CLASS(CPageSourceListView));

	OnItemchangedObjectcolorControl(NULL, NULL);
}

void CDlgPDFObjectContent::OnRemovePrintcolor() 
{
	UpdateData(TRUE);

	CPageTemplate* pTemplate		  = NULL;
	CPageSource*   pObjectSource	  = NULL;
	int			   nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pLayoutObject->m_nType == CLayoutObject::ControlMark)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				pObjectSource	   = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate)
		return;
	if (pTemplate->m_bMap2AllColors) 
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	while (pos)
	{
		int				   nItem				= m_printColorList.GetNextSelectedItem(pos);
		CPageSourceHeader* pObjectSourceHeader	= m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
		SEPARATIONINFO*	   pSepInfo				= m_printColorList.m_printColorListItems[nItem].m_pSepInfo;
		int				   nPageNumInSourceFile = m_printColorList.m_printColorListItems[nItem].m_nPageIndex + 1;
		if (pObjectSourceHeader)
		{
			if (pSepInfo)
				if (pSepInfo->m_nColorDefinitionIndex >= 0)
					pTemplate->RemoveSepInfo(pSepInfo, TRUE);	// keep one entry of each separation
		}
	}

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable());

	theApp.UpdateView(RUNTIME_CLASS(CPageListView));
	theApp.UpdateView(RUNTIME_CLASS(CPageListDetailView));
	theApp.UpdateView(RUNTIME_CLASS(CPageListTableView));
	theApp.UpdateView(RUNTIME_CLASS(CPageSourceListView));

	OnItemchangedObjectcolorControl(NULL, NULL);
}

void CDlgPDFObjectContent::OnAssignAllPrintcolors() 
{
	UpdateData(TRUE);
	
	CPageTemplate* pTemplate		  = NULL;
	CPageSource*   pObjectSource	  = NULL;
	int			   nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pLayoutObject->m_nType == CLayoutObject::ControlMark)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				pObjectSource	   = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
			}
		}
		else
			return;	// assign all only defined for marks
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate)
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if ( ! pos)
	{
		m_printColorList.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
		pos = m_printColorList.GetFirstSelectedItemPosition();
	}

	if (pos)
	{
		if (m_bMap2AllColors)
		{
			int				   nItem				= m_printColorList.GetNextSelectedItem(pos);
			CPageSourceHeader* pObjectSourceHeader	= m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
			SEPARATIONINFO*	   pSepInfo				= m_printColorList.m_printColorListItems[nItem].m_pSepInfo;
			int				   nPageNumInSourceFile = m_printColorList.m_printColorListItems[nItem].m_nPageIndex + 1;
			if (pObjectSourceHeader)
			{
				if (pSepInfo)
				{
					SEPARATIONINFO sepInfo = *pSepInfo;
					pTemplate->m_ColorSeparations.RemoveAll();
					pTemplate->m_ColorSeparations.Add(sepInfo);
				}
				else
				{
					SEPARATIONINFO sepInfo = {(short)nObjectSourceIndex, (short)nPageNumInSourceFile, (short)m_pParent->ColorDefTable_FindColorDefinitionIndex(pObjectSourceHeader->m_strColorName)};
					pTemplate->m_ColorSeparations.RemoveAll();
					pTemplate->m_ColorSeparations.Add(sepInfo);
				}
				pTemplate->m_bMap2AllColors = TRUE;
			}
		}
		else
		{
			pTemplate->m_bMap2AllColors = FALSE;
			if (pObjectSource)
			{
				if (pTemplate->m_ColorSeparations.GetSize())
				{
					SEPARATIONINFO sepInfo = pTemplate->m_ColorSeparations[0];
					pTemplate->m_ColorSeparations.RemoveAll();
					pTemplate->m_ColorSeparations.Add(sepInfo);
				}
				for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
				{
					CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[i];
					if ( ! pTemplate->GetFirstSepInfo(i + 1))
					{
						SEPARATIONINFO sepInfo = {(short)nObjectSourceIndex, (short)(i + 1), (short)m_pParent->ColorDefTable_FindColorDefinitionIndex(rObjectSourceHeader.m_strColorName)};
						if (sepInfo.m_nColorDefinitionIndex < 0)
						{
							CColorDefinition colorDef(rObjectSourceHeader.m_strColorName, rObjectSourceHeader.m_rgb, CColorDefinition::Spot, rObjectSourceHeader.m_nColorIndex);
							sepInfo.m_nColorDefinitionIndex = m_pParent->ColorDefTable_InsertColorDef(colorDef);
						}
						pTemplate->InsertSepInfo(&sepInfo);
					}
				}
			}
		}
	}

	if (m_bMap2AllColors && m_bMap2SpotColors)
	{
		m_bMap2SpotColors = FALSE;
		m_spotColorMappingTable.RemoveAll();
		UpdateData(FALSE);
		OnMapSpotcolors();
	}
	pTemplate->m_spotColorMappingTable = m_spotColorMappingTable;

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable());

	OnItemchangedObjectcolorControl(NULL, NULL);
}

void CDlgPDFObjectContent::OnMapSpotcolors() 
{
	UpdateData(TRUE);

	CPageTemplate* pTemplate		  = NULL;
	CPageSource*   pObjectSource	  = NULL;
	CLayoutObject* pLayoutObject	  = m_pParent->GetFirstLayoutObject();
	int			   nObjectSourceIndex = -1;

	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pLayoutObject->m_nType == CLayoutObject::Page)
			return;	// mapping only defined for marks

		if (pTemplate->m_ColorSeparations.GetSize())
		{
			pObjectSource = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
			nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pObjectSource)
		return;

	for (int nItem = 0; nItem < m_printColorList.m_printColorListItems.GetSize(); nItem++)
	{
		if (m_printColorList.m_printColorListItems[nItem].m_strColorName.CompareNoCase(_T("Composite")) == 0)
			continue;

		if (m_bMap2SpotColors)
		{
			int nSpotColorNum = pTemplate->GetCurSpotColorNum(m_printColorList.m_printColorListItems[nItem].m_pSepInfo->m_nColorDefinitionIndex,
															  m_printColorList.m_printColorListItems[nItem].m_strColorName, m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader);
			m_spotColorMappingTable.SetEntry(nSpotColorNum - 1, nSpotColorNum);
			m_printColorList.m_printColorListItems[nItem].m_nMapSpotColorIndex = nSpotColorNum;
			m_printColorList.m_printColorListItems[nItem].m_nState |= CPrintColorListItem::Remapped;
		}
		else
		{
			m_printColorList.m_printColorListItems[nItem].m_nMapSpotColorIndex = -1;
			m_printColorList.m_printColorListItems[nItem].m_nState &= ~CPrintColorListItem::Remapped;
		}
	}

	if (m_bMap2SpotColors && m_bMap2AllColors)
	{
		m_bMap2AllColors		    = FALSE;
		pTemplate->m_bMap2AllColors = FALSE;
		UpdateData(FALSE);
		OnAssignAllPrintcolors();
	}
	if( ! m_bMap2SpotColors)
		m_spotColorMappingTable.RemoveAll();

	pTemplate->m_spotColorMappingTable = m_spotColorMappingTable;

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable());

	OnItemchangedObjectcolorControl(NULL, NULL);
}

void CDlgPDFObjectContent::OnItemchangedObjectcolorControl(NMHDR* /*pNMHDR*/, LRESULT* /*pResult*/) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if ( ! m_pParent)
		return;
	
	CPageTemplate* pTemplate	 = NULL;
	CPageSource*   pObjectSource = NULL;
	CPageSourceHeader*   pObjectSourceHeader = NULL;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	BOOL		   bIsMarkObject = TRUE;
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate)
			if (pTemplate->m_ColorSeparations.GetSize())
				pObjectSource = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);

		if (pLayoutObject->m_nType == CLayoutObject::Page)
			bIsMarkObject = FALSE;
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if (pTemplate)
	{
		POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
		if (pos)
		{
			BOOL bNonPrint = FALSE;
			int  nItem	   = m_printColorList.GetNextSelectedItem(pos);
			if ( ! m_printColorList.m_printColorListItems[nItem].m_pSepInfo)
				bNonPrint = TRUE;
			else
				if (m_printColorList.m_printColorListItems[nItem].m_pSepInfo->m_nColorDefinitionIndex < 0) 
					bNonPrint = TRUE;

			GetDlgItem(IDC_INSERT_PRINTCOLOR)->EnableWindow((pTemplate->m_bMap2AllColors			 ) ? FALSE : TRUE);
			GetDlgItem(IDC_REMOVE_PRINTCOLOR)->EnableWindow((pTemplate->m_bMap2AllColors || bNonPrint) ? FALSE : TRUE);
			int nSpotColorNum = pTemplate->GetCurSpotColorNum(m_printColorList.m_printColorListItems[nItem].m_pSepInfo->m_nColorDefinitionIndex,
															  m_printColorList.m_printColorListItems[nItem].m_strColorName, m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader);
			m_nMap2SpotColorIndex = m_spotColorMappingTable.GetEntry(nSpotColorNum - 1);
		}
		else
		{
			GetDlgItem(IDC_INSERT_PRINTCOLOR)->EnableWindow(FALSE);
			GetDlgItem(IDC_REMOVE_PRINTCOLOR)->EnableWindow(FALSE);
			m_nMap2SpotColorIndex = 0;
		}

		GetDlgItem(IDC_MODIFY_PRINTCOLOR)->EnableWindow((pTemplate->m_bMap2AllColors)	  																	 ? FALSE : TRUE);
		GetDlgItem(IDC_MAP_SPOTCOLORS	)->EnableWindow((pTemplate->m_bMap2AllColors || ! pTemplate->HasSpotColors(pMark, m_pParent->GetActColorDefTable())) ? FALSE : TRUE);
	}

	UpdateData(FALSE);
}

void CDlgPDFObjectContent::OnRegisterPDF() 
{
	UpdateData(TRUE);

	CMainFrame* pMainFrame = (CMainFrame*)theApp.m_pMainWnd;;
	if (pMainFrame)
	{
		CLayoutObject* pObject = m_pParent->GetFirstLayoutObject();
		BOOL bIsMark = (m_pParent->GetFirstMark()) ? TRUE : ( (pObject) ? ( (pObject->m_nType == CLayoutObject::ControlMark) ? TRUE : FALSE) : FALSE);
		theApp.m_pDlgPDFLibStatus->LaunchPDFEngine((bIsMark) ? theApp.settings.m_szMarksDir : theApp.settings.m_szPDFInputFolder, this, CDlgPDFLibStatus::RegisterMarks);
	}
}

int CDlgPDFObjectContent::AssignObjectSource(int nStartIndex, CPageTemplate* pTemplate, CPageSource* pNewObjectSource, short nObjectSourceIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;
	if (nStartIndex >= pNewObjectSource->m_PageSourceHeaders.GetSize())
		return -1;

	int nCurPageNum = pNewObjectSource->m_PageSourceHeaders[nStartIndex].m_nPageNumber;
	int i;
	for (i = nStartIndex; i < pNewObjectSource->m_PageSourceHeaders.GetSize(); i++)
	{
		CPageSourceHeader& rObjectSourceHeader = pNewObjectSource->m_PageSourceHeaders[i];

		if (nCurPageNum != rObjectSourceHeader.m_nPageNumber)
			break;

		nCurPageNum = rObjectSourceHeader.m_nPageNumber;

		CColorDefinition colorDef(rObjectSourceHeader.m_strColorName, rObjectSourceHeader.m_rgb, CColorDefinition::Spot, rObjectSourceHeader.m_nColorIndex);
		SEPARATIONINFO	 sepInfo;
		sepInfo.m_nPageSourceIndex		= nObjectSourceIndex;
		sepInfo.m_nPageNumInSourceFile	= (short)(i + 1);
		sepInfo.m_nColorDefinitionIndex = m_pParent->ColorDefTable_InsertColorDef(colorDef);

		pTemplate->m_ColorSeparations.Add(sepInfo);
		if ( ! ((rObjectSourceHeader.m_fTrimBoxLeft == 0.0f) && (rObjectSourceHeader.m_fTrimBoxBottom == 0.0f) && (rObjectSourceHeader.m_fTrimBoxRight == 0.0f) && (rObjectSourceHeader.m_fTrimBoxTop == 0.0f)) )
		{
			float fTrimBoxCenterX = rObjectSourceHeader.m_fTrimBoxLeft   + (rObjectSourceHeader.m_fTrimBoxRight - rObjectSourceHeader.m_fTrimBoxLeft)	/ 2.0f;
			float fTrimBoxCenterY = rObjectSourceHeader.m_fTrimBoxBottom + (rObjectSourceHeader.m_fTrimBoxTop   - rObjectSourceHeader.m_fTrimBoxBottom)	/ 2.0f;
			float fBBoxCenterX	  = rObjectSourceHeader.m_fBBoxLeft		 + (rObjectSourceHeader.m_fBBoxRight	- rObjectSourceHeader.m_fBBoxLeft)		/ 2.0f;
			float fBBoxCenterY	  = rObjectSourceHeader.m_fBBoxBottom	 + (rObjectSourceHeader.m_fBBoxTop		- rObjectSourceHeader.m_fBBoxBottom)	/ 2.0f;
			pTemplate->SetContentOffsetX(fBBoxCenterX - fTrimBoxCenterX);
			pTemplate->SetContentOffsetY(fBBoxCenterY - fTrimBoxCenterY);
		}
		CPageTemplate* p2ndTemplate = NULL;
		if (pTemplate->CheckMultipage(&rObjectSourceHeader))
		{
			p2ndTemplate = pTemplate->GetFirstMultipagePartner();
			p2ndTemplate->m_ColorSeparations.Add(sepInfo);
			if ( ! ((rObjectSourceHeader.m_fTrimBoxLeft == 0.0f) && (rObjectSourceHeader.m_fTrimBoxBottom == 0.0f) && (rObjectSourceHeader.m_fTrimBoxRight == 0.0f) && (rObjectSourceHeader.m_fTrimBoxTop == 0.0f)) )
			{
				float fTrimBoxCenterX = rObjectSourceHeader.m_fTrimBoxLeft   + (rObjectSourceHeader.m_fTrimBoxRight - rObjectSourceHeader.m_fTrimBoxLeft)   / 2.0f;
				float fTrimBoxCenterY = rObjectSourceHeader.m_fTrimBoxBottom + (rObjectSourceHeader.m_fTrimBoxTop   - rObjectSourceHeader.m_fTrimBoxBottom) / 2.0f;
				float fBBoxCenterX	  = rObjectSourceHeader.m_fBBoxLeft		 + (rObjectSourceHeader.m_fBBoxRight	- rObjectSourceHeader.m_fBBoxLeft)		/ 2.0f;
				float fBBoxCenterY	  = rObjectSourceHeader.m_fBBoxBottom	 + (rObjectSourceHeader.m_fBBoxTop		- rObjectSourceHeader.m_fBBoxBottom)	/ 2.0f;
				p2ndTemplate->SetContentOffsetX(fBBoxCenterX - fTrimBoxCenterX);
				p2ndTemplate->SetContentOffsetY(fBBoxCenterY - fTrimBoxCenterY);
			}
		}
	}

	return i;
}

#ifdef PDF
void CDlgPDFObjectContent::PDFEngineDataExchange(CPageSource* pNewObjectSource)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! m_pParent)
		return;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	CMark*		   pMark		 = m_pParent->GetFirstMark();
	CPageTemplate* pTemplate	 = m_pParent->m_pObjectTemplate;

	CString strNewMarkName;
	strNewMarkName.LoadString(IDS_NEW_CUSTOMMARK_PDF);

	if (pTemplate)
		if (pTemplate->m_strPageID.Find(strNewMarkName) != -1)	// if default mark name not changed by user, assume user wants to get PDF filename automatically
		{
			pTemplate->m_strPageID = CString(theApp.m_pDlgPDFLibStatus->m_strFilename);
			PathRemoveExtension(pTemplate->m_strPageID.GetBuffer(MAX_PATH));
			CWnd* pGrandParent = (m_pParent) ? m_pParent->GetParent() : NULL;
			if (pGrandParent)
				pGrandParent->SendMessage(WM_COMMAND, ID_UPDATE_MARKNAME);
		}

	if (pLayoutObject)
	{
		// remove existing source
		if (pTemplate)
		{
			pTemplate->m_ColorSeparations.RemoveAll();
			if (pTemplate->m_nMultipageGroupNum >= 0)
			{
				CPageTemplate* p2ndPageTemplate = pTemplate->GetFirstMultipagePartner();
				if (p2ndPageTemplate)
					p2ndPageTemplate->m_ColorSeparations.RemoveAll();

				if (theApp.settings.m_bAutoCheckDoublePages)
					CPageListView::SetDoublePage(pTemplate, (pTemplate->GetMultipageSide() == LEFT) ? RIGHT : LEFT, FALSE);
			}
		}

		// assign new source
		if (pLayoutObject->m_nType == CLayoutObject::Page)
		{
			pDoc->m_PageSourceList.AddTail(*pNewObjectSource);

			AssignObjectSource(0, pTemplate, pNewObjectSource, (short)(pDoc->m_PageSourceList.GetCount() - 1));

			pDoc->m_PageSourceList.InitializePageTemplateRefs();
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_PrintSheetList.ReorganizeColorInfos();

			CPageSourceListView* pView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
			if (pView)
				pView->m_DisplayList.Invalidate();		// new display item

		}
		else
		{
			short nMarkSourceIndex = pDoc->m_MarkSourceList.FindMarkSourceIndex(pNewObjectSource, pTemplate->GetMarkType());
			if (nMarkSourceIndex == -2)		// invalid mark source
				return;
			if (nMarkSourceIndex == -1)
			{
				pDoc->m_MarkSourceList.AddTail(*pNewObjectSource);
				nMarkSourceIndex = (short)(pDoc->m_MarkSourceList.GetCount() - 1);
			}

			AssignObjectSource(0, pTemplate, pNewObjectSource, nMarkSourceIndex);

			pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
			pDoc->m_MarkSourceList.RemoveUnusedSources();
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
		}
	}

	if (pMark)
	{
		// remove existing source
		if (pTemplate)
		{
			pTemplate->m_ColorSeparations.RemoveAll();
			AssignObjectSource(0, pTemplate, pNewObjectSource, -1);
			pMark->m_MarkSource = *pNewObjectSource;

			for (int i = 0; i < pMark->m_MarkSource.m_PageSourceHeaders.GetSize(); i++)
			{
				if (pMark->m_MarkSource.m_PageSourceHeaders[i].m_strColorName.CompareNoCase(_T("Composite")) == 0)
					if ( ! pMark->m_MarkSource.m_PageSourceHeaders[i].m_processColors.GetSize() && ! pMark->m_MarkSource.m_PageSourceHeaders[i].m_spotColors.GetSize())	// if we have a pdf mark w/o any colors, we define this as
					{	pMark->m_bAllColors = TRUE; pMark->m_markTemplate.m_bMap2AllColors = TRUE; }																	// colorspace all, since the xml report is blank for
			}																																							// this type of marks (i.e. InDesign color register)
		}																																								// If this mark is not colorspace all, it should be blank
																																										// which doesn't generate any output, so is OK also
		((CDlgLayoutObjectControl*)m_pParent)->m_dlgCustomMarkGeometry.m_fObjectWidth  = pNewObjectSource->m_fGlobalBBoxRight - pNewObjectSource->m_fGlobalBBoxLeft;
		((CDlgLayoutObjectControl*)m_pParent)->m_dlgCustomMarkGeometry.m_fObjectHeight = pNewObjectSource->m_fGlobalBBoxTop   - pNewObjectSource->m_fGlobalBBoxBottom;
		((CDlgLayoutObjectControl*)m_pParent)->m_dlgCustomMarkGeometry.m_nHeadPosition = TOP;
		((CDlgLayoutObjectControl*)m_pParent)->m_dlgCustomMarkGeometry.UpdateData(FALSE);
		((CDlgLayoutObjectControl*)m_pParent)->m_dlgCustomMarkGeometry.SaveData();
	}

	LoadData();

	if (m_pParent->GetParent())
		m_pParent->GetParent()->SendMessage(WM_COMMAND, ID_UPDATE_SUBDIALOGS);

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}
#endif


// Calculate the best fitting resolution. Because of the small size of marks we need higher res to see anything and it does not take too much memory
long CDlgPDFObjectContent::GetOptPreviewResolution(CPageSource& rObjectSource)
{
	float fPlaneInch = ( (rObjectSource.m_fGlobalBBoxRight - rObjectSource.m_fGlobalBBoxLeft) * (rObjectSource.m_fGlobalBBoxTop - rObjectSource.m_fGlobalBBoxBottom) ) / 25.4f;

	long lResolution = (long)sqrt(8000000.0f / fPlaneInch);

	lResolution = max(lResolution, 144);		// force resolution to be between 144 and 3000 dpi
	lResolution = min(lResolution, 3000);

	return lResolution;
}

void CDlgPDFObjectContent::OnRemovePdffile() 
{
	UpdateData(TRUE);
	
	CPageTemplate* pTemplate		  = NULL;
	CPageSource*   pObjectSource	  = NULL;
	int			   nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate->m_ColorSeparations.GetSize())
		{
			pObjectSource	   = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
			nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate || ! pObjectSource)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (AfxMessageBox(IDS_REMOVE_LINK_WARNING, MB_YESNO) == IDNO)
		return;

	pTemplate->m_ColorSeparations.RemoveAll();

	if (pLayoutObject)
	{
		if (pTemplate->m_nMultipageGroupNum >= 0)
		{
			CPageTemplate* p2ndPageTemplate = pTemplate->GetFirstMultipagePartner();
			if (p2ndPageTemplate)
				p2ndPageTemplate->m_ColorSeparations.RemoveAll();
		}

		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
		
		if ( ! pObjectSource->HasLinks())
		{
			POSITION pos = pDoc->m_PageSourceList.FindIndex(nObjectSourceIndex);
			if (pos)
			{
				pDoc->m_PageSourceList.RemoveAt(pos);
				pDoc->m_PageTemplateList.RemovePageSource(nObjectSourceIndex, FALSE);
			}
		}

		theApp.UpdateView(RUNTIME_CLASS(CPageListView));
		theApp.UpdateView(RUNTIME_CLASS(CPageListDetailView));
		theApp.UpdateView(RUNTIME_CLASS(CPageListTableView));
		theApp.UpdateView(RUNTIME_CLASS(CPageSourceListView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTreeView));
	}

	if (pMark)
		pObjectSource->Clear();

	LoadData();

	if (m_pParent->GetParent())
		m_pParent->GetParent()->SendMessage(WM_COMMAND, ID_UPDATE_SUBDIALOGS);
}

void CDlgPDFObjectContent::OnLaunchAcrobat() 
{
	if (m_strDocFullpath.IsEmpty())
		return;

	int nPageIndex = -1;
	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if (pos)
	{
		int	nItem  = m_printColorList.GetNextSelectedItem(pos);
		nPageIndex = m_printColorList.m_printColorListItems[nItem].m_nPageIndex;
	}
	else
		if (m_pParent) 
			if (m_pParent->m_pObjectTemplate) 
				if (m_pParent->m_pObjectTemplate->m_ColorSeparations.GetSize()) 
					nPageIndex = m_pParent->m_pObjectTemplate->m_ColorSeparations[0].m_nPageNumInSourceFile - 1;
				else
					nPageIndex = -1;

	BeginWaitCursor();     
	CPageListFrame::LaunchAcrobat(m_strDocFullpath, nPageIndex);
	EndWaitCursor();     
}

void CDlgPDFObjectContent::LoadData()
{
	if ( ! m_pParent)
		return;
	int nObjectType = m_pParent->GetObjectType();
	if ( (nObjectType != CDlgLayoutObjectControl::PageObject) && (nObjectType != CMark::CustomMark) )
		return;

	m_strDocFullpath.Empty();
	m_fContentOffsetX = 0.0f;
	m_fContentOffsetY = 0.0f;
	m_fContentScaleX  = 100.0f;
	m_fContentScaleY  = 100.0f;
	m_fContentGridX = 0.0f;
	m_fContentGridY = 0.0f;

	CPageTemplate* pTemplate	 = NULL;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			pTemplate->m_spotColorMappingTable.CleanUp();
			m_bMap2AllColors		= pTemplate->m_bMap2AllColors;
			m_bMap2SpotColors		= (pTemplate->m_spotColorMappingTable.GetSize()) ? TRUE : FALSE;
			m_spotColorMappingTable = pTemplate->m_spotColorMappingTable;
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				m_strDocFullpath = (pObjectSource) ? pObjectSource->m_strDocumentFullpath : "";	
				if (pLayoutObject->m_nType == CLayoutObject::Page)
					m_printColorList.LoadData(pTemplate, m_pParent->GetActColorDefTable());
				else
					m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable());

				m_fContentOffsetX	= pTemplate->GetContentOffsetX();
				m_fContentOffsetY	= pTemplate->GetContentOffsetY();
				m_fContentScaleX	= pTemplate->m_fContentScaleX * 100.0f;
				m_fContentScaleY	= pTemplate->m_fContentScaleY * 100.0f;
				m_bContentDuplicate = pTemplate->m_bContentDuplicate;
				m_bContentTrim		= pTemplate->m_bContentTrim;
				m_fContentGridX		= pTemplate->m_fContentGridX;
				m_fContentGridY		= pTemplate->m_fContentGridY;
			}
			else
				m_printColorList.LoadData(pTemplate, NULL);

			m_contentPivotCtrl.LoadData(pTemplate->m_nContentPivotX, pTemplate->m_nContentPivotY);
		}
	}

	BOOL   bMarkIsSeparated = FALSE;
	CMark* pMark			= m_pParent->GetFirstMark();
	if (pMark)
	{
		pMark->m_markTemplate.m_spotColorMappingTable.CleanUp();
		m_strDocFullpath		= pMark->m_MarkSource.m_strDocumentFullpath;	
		m_bMap2AllColors		= pMark->m_markTemplate.m_bMap2AllColors;
		m_bMap2SpotColors		= (pMark->m_markTemplate.m_spotColorMappingTable.GetSize()) ? TRUE : FALSE;
		m_spotColorMappingTable = pMark->m_markTemplate.m_spotColorMappingTable;
		bMarkIsSeparated		= (pMark->m_MarkSource.m_nColorant == CPageSource::Separated) ? TRUE : FALSE;

		pTemplate = &pMark->m_markTemplate;
		if (pTemplate)
		{
			m_printColorList.LoadData(pTemplate, &pMark->m_MarkSource, m_pParent->GetActColorDefTable());

			m_fContentOffsetX	= pTemplate->GetContentOffsetX();
			m_fContentOffsetY	= pTemplate->GetContentOffsetY();
			m_fContentScaleX	= pTemplate->m_fContentScaleX * 100.0f;
			m_fContentScaleY	= pTemplate->m_fContentScaleY * 100.0f;
			m_bContentDuplicate = pTemplate->m_bContentDuplicate;
			m_bContentTrim		= pTemplate->m_bContentTrim;
			m_fContentGridX		= pTemplate->m_fContentGridX;
			m_fContentGridY		= pTemplate->m_fContentGridY;
			m_contentPivotCtrl.LoadData(pTemplate->m_nContentPivotX, pTemplate->m_nContentPivotY);
		}
		else
			m_printColorList.LoadData(pTemplate, NULL);
	}



	UpdateData(FALSE);

	OnItemchangedObjectcolorControl(NULL, NULL);

	BOOL bIsMarkObject = (nObjectType == CDlgLayoutObjectControl::PageObject) ? FALSE : TRUE;
	GetDlgItem(IDC_CONTENT_PIVOT_STATIC		)->ShowWindow((bIsMarkObject) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_CONTENT_PIVOT			)->ShowWindow((bIsMarkObject) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_CONTENT_TRIMDUP_STATIC	)->ShowWindow((bIsMarkObject) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_CONTENT_TRIM				)->ShowWindow((bIsMarkObject) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_CONTENT_DUPLICATE		)->ShowWindow((bIsMarkObject) ? SW_SHOW : SW_HIDE);

	int nShow = ( bIsMarkObject && (m_bContentDuplicate || m_bContentTrim) ) ? SW_SHOW : SW_HIDE;
	GetDlgItem(IDC_CONTENTGRID_STATIC		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_X_STATIC		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_X			)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRIDSPIN_X		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_Y_STATIC		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_Y			)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRIDSPIN_Y		)->ShowWindow(nShow);

	GetDlgItem(IDC_REMOVE_PDFFILE)->EnableWindow((m_strDocFullpath.IsEmpty()) ? FALSE : TRUE);
	GetDlgItem(IDC_LAUNCH_ACROBAT)->EnableWindow((m_strDocFullpath.IsEmpty()) ? FALSE : TRUE);

	BOOL		 bHasSpotColors		= (pTemplate) ? ( (pTemplate->HasSpotColors(pMark, m_pParent->GetActColorDefTable())) ? TRUE : FALSE) : FALSE;
	BOOL		 bContainsSeparated = FALSE;
	CPrintSheet* pPrintSheet		= m_pParent->GetPrintSheet();
	CImpManDoc*	 pDoc				= CImpManDoc::GetDoc();
	if (pDoc)
	{
		for (int i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
		{
			int nSepStatus = pPrintSheet->ContainsPrintColor(i, BOTHSIDES);
			if (nSepStatus == CColorDefinition::Separated)
			{
				bContainsSeparated = TRUE;
				break;
			}
		}
	}
	if (pMark)
		if ( ! bContainsSeparated)	// sheet is composite
			if (pMark->m_MarkSource.m_PageSourceHeaders.GetSize())
			{
				pMark->m_MarkSource.m_PageSourceHeaders[0].m_strColorName = _T("Composite");
				pMark->m_markTemplate.m_ColorSeparations[0].m_nColorDefinitionIndex = m_pParent->ColorDefTable_FindColorDefinitionIndex(_T("Composite"));
				if ( ! pMark->m_MarkSource.m_PageSourceHeaders[0].m_processColors.GetSize() && ! pMark->m_MarkSource.m_PageSourceHeaders[0].m_spotColors.GetSize())	// if we have a pdf mark w/o any colors, we define this as colorspace all
				{
					pMark->m_bAllColors = TRUE;
					pMark->m_markTemplate.m_bMap2AllColors = TRUE;
				}
				else
				{
					pMark->m_bAllColors = FALSE;
					pMark->m_markTemplate.m_bMap2AllColors = FALSE;
				}
				if ( ! bHasSpotColors)
					pMark->m_markTemplate.m_bMap2SpotColors = FALSE;
			}

	GetDlgItem(IDC_ASSIGN_ALL_PRINTCOLORS)->ShowWindow((bIsMarkObject && bMarkIsSeparated && bContainsSeparated) ? SW_SHOW : SW_HIDE);	// we need this button only if sheet contains separated colors
																																		// for unseparated composite, 'map to all' makes no sense (does not work)
	GetDlgItem(IDC_MODIFY_PRINTCOLOR	 )->ShowWindow((bIsMarkObject && bMarkIsSeparated && bContainsSeparated) ? SW_SHOW : SW_HIDE);	// same does apply to color modification
	GetDlgItem(IDC_INSERT_PRINTCOLOR	 )->ShowWindow((bIsMarkObject && bMarkIsSeparated && bContainsSeparated) ? SW_SHOW : SW_HIDE);	
	GetDlgItem(IDC_REMOVE_PRINTCOLOR	 )->ShowWindow((bIsMarkObject && bMarkIsSeparated && bContainsSeparated) ? SW_SHOW : SW_HIDE);	

	GetDlgItem(IDC_MAP_SPOTCOLORS			)->ShowWindow((bIsMarkObject && bHasSpotColors) ? SW_SHOW : SW_HIDE);	
	GetDlgItem(IDC_MAP_SPOTCOLOR_INDEX_COMBO)->ShowWindow((bIsMarkObject && bHasSpotColors) ? SW_SHOW : SW_HIDE);

	CRect rcColors, rcButton;
	GetDlgItem(IDC_PDFOBJECT_COLORCONTROL)->GetWindowRect(rcColors); ScreenToClient(rcColors);
	if (bIsMarkObject && bMarkIsSeparated && bContainsSeparated)
	{
		GetDlgItem(IDC_MODIFY_PRINTCOLOR)->GetWindowRect(rcButton);	 ScreenToClient(rcButton);
		rcColors.bottom = rcButton.top - 5;
		GetDlgItem(IDC_PDFOBJECT_COLORCONTROL)->MoveWindow(rcColors);
	}
	else
	{
		GetDlgItem(IDC_MAP_SPOTCOLORS)->GetWindowRect(rcButton);	 ScreenToClient(rcButton);
		rcColors.bottom = rcButton.top - 5;
		GetDlgItem(IDC_PDFOBJECT_COLORCONTROL)->MoveWindow(rcColors);
	}
}


void CDlgPDFObjectContent::SaveData()
{
	if ( ! m_hWnd)
		return;
	int nObjectType = m_pParent->GetObjectType();
	if ( (nObjectType != CDlgLayoutObjectControl::PageObject) && (nObjectType != CMark::CustomMark) )
		return;

	UpdateData(TRUE);

	m_spotColorMappingTable.CleanUp();

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	while (pLayoutObject)
	{
		int nObjectSourceIndex = -1;
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if ( ! pTemplate)
			break;
		if ( ! pTemplate->m_ColorSeparations.GetSize())
			break;

		CPageSource* pObjectSource = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
		if ( ! pObjectSource)
			break;

		nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;

		pObjectSource->m_strDocumentFullpath = m_strDocFullpath;	
		pTemplate->m_spotColorMappingTable	 = m_spotColorMappingTable;
		pTemplate->m_bMap2SpotColors		 = (m_spotColorMappingTable.GetSize()) ? TRUE : FALSE;

		m_printColorList.SaveData(pTemplate, pObjectSource);

		pTemplate->SetContentOffsetX(m_fContentOffsetX);
		pTemplate->SetContentOffsetY(m_fContentOffsetY);
		pTemplate->m_fContentScaleX		= m_fContentScaleX / 100.0f; 
		pTemplate->m_fContentScaleY		= m_fContentScaleY / 100.0f;
		pTemplate->m_bContentDuplicate	= m_bContentDuplicate;
		pTemplate->m_bContentTrim		= m_bContentTrim;
		pTemplate->m_fContentGridX		= m_fContentGridX;
		pTemplate->m_fContentGridY		= m_fContentGridY;
		m_contentPivotCtrl.SaveData(pTemplate->m_nContentPivotX, pTemplate->m_nContentPivotY);

		pLayoutObject = m_pParent->GetNextLayoutObject(pLayoutObject);
	}

	CMark* pMark = m_pParent->GetFirstMark();
	while (pMark)
	{
		pMark->m_MarkSource.m_strDocumentFullpath	  = m_strDocFullpath;	
		pMark->m_markTemplate.m_bMap2AllColors		  = m_bMap2AllColors;
		pMark->m_markTemplate.m_spotColorMappingTable = m_spotColorMappingTable;
		pMark->m_markTemplate.m_bMap2SpotColors		  = (m_spotColorMappingTable.GetSize()) ? TRUE : FALSE;

		CPageTemplate* pTemplate = &pMark->m_markTemplate;
		if (pTemplate)
		{
			m_printColorList.SaveData(pTemplate, &pMark->m_MarkSource);

			pTemplate->SetContentOffsetX(m_fContentOffsetX);
			pTemplate->SetContentOffsetY(m_fContentOffsetY);
			pTemplate->m_fContentScaleX		= m_fContentScaleX / 100.0f; 
			pTemplate->m_fContentScaleY		= m_fContentScaleY / 100.0f;
			pTemplate->m_bContentDuplicate	= m_bContentDuplicate;
			pTemplate->m_bContentTrim		= m_bContentTrim;
			pTemplate->m_fContentGridX		= m_fContentGridX;
			pTemplate->m_fContentGridY		= m_fContentGridY;
			m_contentPivotCtrl.SaveData(pTemplate->m_nContentPivotX, pTemplate->m_nContentPivotY);
		}
		
		pMark = m_pParent->GetNextMark(pMark);
	}
}

void CDlgPDFObjectContent::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}

void CDlgPDFObjectContent::OnPaint() 
{
	CDialog::OnPaint();

	m_printColorList.Invalidate();
	m_printColorList.UpdateWindow();
}

void CDlgPDFObjectContent::OnMouseMove(UINT nFlags, CPoint point) 
{
	m_contentPivotCtrl.OnMouseMove(nFlags, point);

	CDialog::OnMouseMove(nFlags, point);
}

void CDlgPDFObjectContent::OnChangedPivot()
{
	if (m_pParent->GetFirstLayoutObject())
	{
		CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
		while (pLayoutObject)
		{
			CPageTemplate* pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																						 pLayoutObject->GetCurrentMarkTemplate();
			if (pTemplate)
				m_contentPivotCtrl.SaveData(pTemplate->m_nContentPivotX, pTemplate->m_nContentPivotY);

			pLayoutObject = m_pParent->GetNextLayoutObject(pLayoutObject);
		}
	}

	if (m_pParent->GetFirstMark())
	{
		CMark* pMark = m_pParent->GetFirstMark();
		while (pMark)
		{
			m_contentPivotCtrl.SaveData(pMark->m_markTemplate.m_nContentPivotX, pMark->m_markTemplate.m_nContentPivotY);
			pMark = m_pParent->GetNextMark(pMark);
		}
	}

	UpdateData(FALSE);

	m_pParent->UpdateView();
}

void CDlgPDFObjectContent::OnCbnSelchangeMapSpotcolorIndexCombo()
{
	UpdateData(TRUE);

	CPageTemplate* pTemplate	 = NULL;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																	  pLayoutObject->GetCurrentMarkTemplate();
	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
		pTemplate = &pMark->m_markTemplate;

	if ( ! pTemplate)
		return;

	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if (pos)
	{
		int nItem = m_printColorList.GetNextSelectedItem(pos);

		int nSpotColorNum = pTemplate->GetCurSpotColorNum(m_printColorList.m_printColorListItems[nItem].m_pSepInfo->m_nColorDefinitionIndex,
														  m_printColorList.m_printColorListItems[nItem].m_strColorName, m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader);
		m_spotColorMappingTable.SetEntry(nSpotColorNum - 1, m_nMap2SpotColorIndex);
		m_printColorList.m_printColorListItems[nItem].m_nMapSpotColorIndex = m_nMap2SpotColorIndex;
		if (m_nMap2SpotColorIndex > 0)
			m_printColorList.m_printColorListItems[nItem].m_nState |= CPrintColorListItem::Remapped;
		else
			m_printColorList.m_printColorListItems[nItem].m_nState &= ~CPrintColorListItem::Remapped;

		m_spotColorMappingTable.CleanUp();
		m_bMap2SpotColors = (m_spotColorMappingTable.GetSize()) ? TRUE : FALSE;

		m_printColorList.Invalidate();
		m_printColorList.UpdateWindow();

		UpdateData(FALSE);
	}
}

void CDlgPDFObjectContent::OnUpdateMapSpotcolorIndexCombo(CCmdUI* pCmdUI)
{
	BOOL bEnable = FALSE;
	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if (pos)
	{
		int	nItem = m_printColorList.GetNextSelectedItem(pos);
		if (m_printColorList.m_printColorListItems[nItem].m_strColorName.CompareNoCase(_T("Composite")) != 0)
			if ( ! theApp.m_colorDefTable.IsProcessColor(m_printColorList.m_printColorListItems[nItem].m_strColorName) && m_bMap2SpotColors)
				bEnable = TRUE;
	}

	pCmdUI->Enable(bEnable);
}

void CDlgPDFObjectContent::OnContentDuplicate()
{
	UpdateData(TRUE);

	if (m_bContentDuplicate)
	{
		if (m_bContentTrim)
			m_bContentTrim = FALSE;

		CMark* pMark = m_pParent->GetFirstMark();
		if (pMark)
		{
			float fOutBBoxWidth, fOutBBoxHeight;
			if ((pMark->m_markTemplate.m_fContentRotation == 0.0f) || (pMark->m_markTemplate.m_fContentRotation == 180.0f))
			{
				fOutBBoxWidth  = (pMark->m_MarkSource.GetBBox(RIGHT, NULL) - pMark->m_MarkSource.GetBBox(LEFT,   NULL)) * pMark->m_markTemplate.m_fContentScaleX;
				fOutBBoxHeight = (pMark->m_MarkSource.GetBBox(TOP,   NULL) - pMark->m_MarkSource.GetBBox(BOTTOM, NULL)) * pMark->m_markTemplate.m_fContentScaleY;
			}
			else
			{
				fOutBBoxWidth  = (pMark->m_MarkSource.GetBBox(TOP,   NULL) - pMark->m_MarkSource.GetBBox(BOTTOM, NULL)) * pMark->m_markTemplate.m_fContentScaleX;
				fOutBBoxHeight = (pMark->m_MarkSource.GetBBox(RIGHT, NULL) - pMark->m_MarkSource.GetBBox(LEFT,   NULL)) * pMark->m_markTemplate.m_fContentScaleY;
			}

			if ((pMark->m_nHeadPosition == LEFT) || (pMark->m_nHeadPosition == RIGHT))
			{
				float fTemp = fOutBBoxWidth; fOutBBoxWidth = fOutBBoxHeight; fOutBBoxHeight = fTemp;
			}
			m_fContentGridX = fOutBBoxWidth;
			m_fContentGridY = fOutBBoxHeight;
		}
	}
	else
	{
		m_fContentGridX = 0.0f;
		m_fContentGridY = 0.0f;
	}

	int nShow = (m_bContentDuplicate || m_bContentTrim) ? SW_SHOW : SW_HIDE;
	GetDlgItem(IDC_CONTENTGRID_STATIC		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_X_STATIC		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_X			)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRIDSPIN_X		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_Y_STATIC		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_Y			)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRIDSPIN_Y		)->ShowWindow(nShow);

	UpdateData(FALSE);

	if (m_pParent->GetParent())
		m_pParent->GetParent()->SendMessage(WM_COMMAND, IDC_APPLY_BUTTON);
}

void CDlgPDFObjectContent::OnContentTrim()
{
	UpdateData(TRUE);

	if (m_bContentTrim)
		if (m_bContentDuplicate)
			m_bContentDuplicate = FALSE;

	int nShow = (m_bContentDuplicate || m_bContentTrim) ? SW_SHOW : SW_HIDE;
	GetDlgItem(IDC_CONTENTGRID_STATIC		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_X_STATIC		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_X			)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRIDSPIN_X		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_Y_STATIC		)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRID_Y			)->ShowWindow(nShow);
	GetDlgItem(IDC_CONTENTGRIDSPIN_Y		)->ShowWindow(nShow);

	UpdateData(FALSE);

	if (m_pParent->GetParent())
		m_pParent->GetParent()->SendMessage(WM_COMMAND, IDC_APPLY_BUTTON);
}