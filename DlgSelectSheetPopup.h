#pragma once


#include "DlgSheet.h"


// CDlgSelectSheetPopup-Dialogfeld

class CDlgSelectSheetPopup : public CDialog
{
	DECLARE_DYNAMIC(CDlgSelectSheetPopup)

public:
	CDlgSelectSheetPopup(CWnd* pParent = NULL, BOOL bRunModal = FALSE);   // Standardkonstruktor
	virtual ~CDlgSelectSheetPopup();

// Dialogfelddaten
	enum { IDD = IDD_SELECT_SHEET_POPUP };


public:
	CWnd*			m_pParent;
	CPrintSheet*	m_pPrintSheet;
	CLayout*		m_pLayout;
	CPressDevice*	m_pPressDevice;
	void			(*m_pFnSelChangeHandler)(CSheet* pSheet, CDlgSelectSheetPopup* pDlg);
	CDlgSheet		m_dlgSheet;
protected:
	CFont			m_smallFont;
	CImageList*		m_pSheetImageList;
	CListCtrl		m_sheetList;
	BOOL			m_bLocked;
	BOOL			m_bRunModal;


public:
	void InitSheetList();

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedSsDefineSheetButton();
	afx_msg void OnNMCustomdrawSsSheetlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedSsSheetlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
};
