#pragma once

class CBindingDefs : public CObject
{
    DECLARE_SERIAL( CBindingDefs )

public:
	CBindingDefs();
	CBindingDefs(const CBindingDefs& rBindingDefs);
	~CBindingDefs();

enum Binding {PerfectBound, SaddleStitched}; 

public:
	CString m_strName;
	int		m_nBinding;

public:
	const CBindingDefs& operator=(const CBindingDefs& rBindingDefs);
};

template <> void AFXAPI SerializeElements <CBindingDefs> (CArchive& ar, CBindingDefs* pBindingDefs, INT_PTR nCount);



class CBindingDefsSelection : public CObject
{
public:
	CBindingDefsSelection(void);
	~CBindingDefsSelection(void);

public:
	CList <CBindingDefs, CBindingDefs&> m_bindingDefsList;
	CBindingDefs						m_nullBindingDefs;

public:
	BOOL 			InitComboBox(CWnd* pDlg, CString& rstrBindingName, int* pnBinding, int nIDComboBox);
	void 			SelchangeFormatList(CWnd* pDlg, int* pnBinding, CString& rstrBindingName, int nIDComboBox);
	void 			NewEntry	(CWnd* pDlg, int* pnBinding, CString& rstrBindingName, int nIDComboBox);
	void 			ChangeEntry(CWnd* pDlg, int* pnBinding, CString& rstrBindingName, int nIDComboBox);
	void 			DeleteEntry(CWnd* pDlg, int* pnBinding, CString& rstrBindingName, int nIDComboBox);
	BOOL 			LoadData(void);
	BOOL 			SaveData(void);
	int  			GetBindingDefsIndex(CBindingDefs& rBindingDefs, BOOL bBoth = FALSE);
	CBindingDefs&	GetBindingDefs(int nIndex);
	void AFXAPI DDX_BindingDefsCombo(CDataExchange* pDX, int nIDC, CString& strValue);
};
