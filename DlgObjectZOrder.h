#if !defined(AFX_DLGOBJECTZORDER_H__15BCD248_A88D_46F1_BD3B_93603E0D7821__INCLUDED_)
#define AFX_DLGOBJECTZORDER_H__15BCD248_A88D_46F1_BD3B_93603E0D7821__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgObjectZOrder.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgObjectZOrder 

class CDlgObjectZOrder : public CDialog
{
// Konstruktion
public:
	CDlgObjectZOrder(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgObjectZOrder();

// Dialogfelddaten
	//{{AFX_DATA(CDlgObjectZOrder)
	enum { IDD = IDD_OBJECT_ZORDER };
	int		m_nZOrder;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl* m_pParent;
	CBrush						   m_hollowBrush;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgObjectZOrder)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void LoadData();
	void SaveData();
	void Apply();

protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgObjectZOrder)
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGOBJECTZORDER_H__15BCD248_A88D_46F1_BD3B_93603E0D7821__INCLUDED_
