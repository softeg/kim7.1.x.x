// OrderDataView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "OrderDataView.h"
#include "GraphicComponent.h"
#include "DlgJobData.h"


// CODCustomPaintFrame

IMPLEMENT_DYNAMIC(CODVCustomPaintFrame, CStatic)

CODVCustomPaintFrame::CODVCustomPaintFrame()
{
	m_itemData = -1;
}

CODVCustomPaintFrame::~CODVCustomPaintFrame()
{
}

BEGIN_MESSAGE_MAP(CODVCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CODVCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	COrderDataView* pView = (COrderDataView*)GetParent();
	if ( ! pView)
		return;
	CRect rcFrame;
	GetClientRect(rcFrame);
    dc.FillSolidRect(rcFrame, RGB(245,245,245));
	//CGraphicComponent gp;
	//gp.DrawTitleBar(&dc, rcFrame, _T(""), RGB(234,237,242), RGB(211,218,228), 90, -1);

	int nProductPartIndex = (int)m_itemData;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	switch (GetDlgCtrlID())
	{
	case IDC_PDH_JOBTITLE_HEADER:			{
												//CGraphicComponent gp;
												//gp.DrawTitleBar(&dc, rcFrame, _T(""), RGB(0,120,200), RGB(0,107,181), 0);

												//dc.SetTextColor(WHITE);//RGB(210,220,240));
												//dc.SetBkMode(TRANSPARENT);
												//dc.SelectObject(&pView->m_bigFont);
												//CString strOut;
												//rcFrame.OffsetRect(15, 0);
												//if (pDoc)
												//{
												//	CString strOut = pDoc->GetTitle(); 
												//	theApp.RemoveExtension(strOut);
												//	dc.DrawText(strOut, rcFrame, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
												//}
											}
											break;

	case IDC_JC_ORDERDATA_FRAME:			{
												//CPen pen(PS_SOLID, 1, MIDDLEGRAY);
												//dc.SelectObject(&pen);
												//CBrush brush(RGB(237,240,241));//225,230,235));//RGB(250, 250, 245));
												//dc.SelectObject(&brush);

												//dc.SaveDC();	// save current clipping region
												//CRect rcClip;
												//pView->GetDlgItem(IDC_JD_MODIFY_JOBDATA)->GetWindowRect(rcClip);
												//ScreenToClient(rcClip);
												//dc.ExcludeClipRect(rcClip);
												//dc.RoundRect(rcFrame, CPoint(4,4));
												//dc.RestoreDC(-1);

												//rcFrame.left += 15;
												if (pDoc)
													pDoc->m_GlobalData.Draw(&dc, rcFrame, &pView->m_DisplayList, TRUE);				
											}
											break;

	default:								break;
	}
}


// CODCustomPaintFrameList

IMPLEMENT_DYNAMIC(CODVCustomPaintFrameList, CStatic)

CODVCustomPaintFrameList::CODVCustomPaintFrameList()
{
}

CODVCustomPaintFrameList::~CODVCustomPaintFrameList()
{
	for (int i = 0; i < GetSize(); i++)
		delete ElementAt(i);
}

void CODVCustomPaintFrameList::RemoveAll()
{
	for (int i = 0; i < GetSize(); i++)
		delete ElementAt(i);

	CArray::RemoveAll();
}



// COrderDataView

IMPLEMENT_DYNCREATE(COrderDataView, CFormView)

COrderDataView::COrderDataView()
	: CFormView(COrderDataView::IDD)
{
	m_DisplayList.Create(this);	
	m_bigFont.CreateFont   (18, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

COrderDataView::~COrderDataView()
{
	m_bigFont.DeleteObject();
}

void COrderDataView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(COrderDataView, CFormView)
	ON_BN_CLICKED(IDC_JD_MODIFY_JOBDATA, &COrderDataView::OnBnClickedJdModifyJobdata)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()


// COrderDataView-Diagnose

#ifdef _DEBUG
void COrderDataView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void COrderDataView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG





// COrderDataView-Meldungshandler

void COrderDataView::OnInitialUpdate()
{
	m_jobTitleHeader.SubclassDlgItem(IDC_PDH_JOBTITLE_HEADER,	this);
	m_orderDataFrame.SubclassDlgItem(IDC_JC_ORDERDATA_FRAME,	this);

	m_gotoOrderDataButton.SubclassDlgItem(IDC_JD_MODIFY_JOBDATA, this);
	m_gotoOrderDataButton.SetBitmap(IDB_ARROW_RIGHT_HOT, IDB_ARROW_RIGHT_COLD, -1, XCENTER);
	m_gotoOrderDataButton.SetTransparent(TRUE);
	m_gotoOrderDataButton.AlignToBitmap();

	CFormView::OnInitialUpdate();

	//CRect rcClient; GetClientRect(rcClient);
	SetScaleToFitSize(CSize(10,10));//rcClient.Size());	//suppress scrollbars
}

void COrderDataView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_orderDataFrame.Invalidate();
	m_orderDataFrame.UpdateWindow();
}

void COrderDataView::OnBnClickedJdModifyJobdata()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgJobData dlgJobData(IDD_ORDERDATA, 0, (ULONG_PTR)0);
	if (dlgJobData.DoModal() == IDCANCEL)
		return;

	pDoc->SetModifiedFlag();

	Invalidate();
	UpdateWindow();
}

void COrderDataView::OnSize(UINT nType, int cx, int cy)
{
	CRect rcClient, rcFrame;
	GetClientRect(rcClient);
	if (m_jobTitleHeader.m_hWnd)
	{
		m_jobTitleHeader.GetWindowRect(rcFrame); ScreenToClient(rcFrame); rcFrame.right = rcClient.Width() - rcFrame.left;
		m_jobTitleHeader.MoveWindow(rcFrame);
	}
	if (m_orderDataFrame.m_hWnd)
	{
		m_orderDataFrame.GetWindowRect(rcFrame); ScreenToClient(rcFrame); rcFrame.right = rcClient.Width() - rcFrame.left;
		m_orderDataFrame.MoveWindow(rcFrame);
	}

	CFormView::OnSize(nType, cx, cy);
}

BOOL COrderDataView::OnEraseBkgnd(CDC* pDC)
{
    CRect rect;
    GetClientRect(rect);
    CBrush brush(RGB(245,245,245));
    pDC->FillRect(&rect, &brush);
	//CGraphicComponent gp;
	//gp.DrawTitleBar(pDC, rect, _T(""), WHITE, RGB(245,245,245), 90, -1);

    brush.DeleteObject();

	return TRUE;

	//	return CFormView::OnEraseBkgnd(pDC);
}

void COrderDataView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_DisplayList.OnLButtonDown(point);
	
	CFormView::OnLButtonDown(nFlags, point);
}

void COrderDataView::OnMouseMove(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

LRESULT COrderDataView::OnMouseLeave(WPARAM wParam, LPARAM lParam) 
{
	m_DisplayList.OnMouseLeave();
	return TRUE;
}


BOOL COrderDataView::OnSelectOrderData(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	if ( ! pDI)
		return FALSE;
	pDI->m_nState = CDisplayItem::Normal;

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CDlgJobData dlgJobData(IDD_ORDERDATA, 0, (ULONG_PTR)0);
	if (dlgJobData.DoModal() == IDCANCEL)
		return FALSE;
	else
		pDoc->SetModifiedFlag();

	pDoc->UpdateAllViews(NULL);

	return TRUE;
}

BOOL COrderDataView::OnMouseMoveOrderData(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}
