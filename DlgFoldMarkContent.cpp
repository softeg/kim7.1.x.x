// DlgFoldMarkContent.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"
#include "DlgFoldMarkContent.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgFoldMarkContent 


CDlgFoldMarkContent::CDlgFoldMarkContent(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgFoldMarkContent::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgFoldMarkContent)
	//}}AFX_DATA_INIT

	m_pParent = NULL;
}

CDlgFoldMarkContent::~CDlgFoldMarkContent()
{
}

BOOL CDlgFoldMarkContent::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}	

void CDlgFoldMarkContent::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgFoldMarkContent)
	DDX_Control(pDX, IDC_LINETYPE_COMBO, m_linetypeCombo);
	DDX_Control(pDX, IDC_BACKGROUND_COMBO, m_backgroundCombo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgFoldMarkContent, CDialog)
	//{{AFX_MSG_MAP(CDlgFoldMarkContent)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_MARK_FOREGROUND, OnMarkForeground)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgFoldMarkContent 

BOOL CDlgFoldMarkContent::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	m_colorControl.Create(this, m_pParent);
	
	CRect controlRect, frameRect;
	m_colorControl.GetWindowRect(controlRect);
	GetDlgItem(IDC_COLORCONTROL_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	m_colorControl.SetWindowPos(NULL, frameRect.left, frameRect.top, controlRect.Width(), controlRect.Height(), SWP_NOZORDER);

	m_linetypeCombo.SetCurSel(0);
	m_backgroundCombo.SetCurSel(0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgFoldMarkContent::LoadData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::FoldMark)
		return;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				if (pObjectSource)
				{
					int	nIndex = pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
					if (nIndex != -1)	// invalid string
					{
						CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
						CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
						strParams.TrimLeft(); strParams.TrimRight();
	
						if (strContentType == "$FOLD_MARK")
						{
							m_linetypeCombo.SetCurSel  ( (strParams.Find(_T("/Dashed")) != -1) ? 1 : 0);		// default is Solid
							m_backgroundCombo.SetCurSel( (strParams.Find(_T("/Opaque")) != -1) ? 1 : 0);		// default is Transparent
						}
					}
					m_colorControl.LoadData(pTemplate, pObjectSource, nObjectSourceIndex);
				}
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex != -1)	// invalid string
		{
			CString strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);
			CFoldMark* pFoldMark = (CFoldMark*)pMark;
			if (strContentType == "$FOLD_MARK")
			{
				m_linetypeCombo.SetCurSel  ( (pFoldMark->m_nLinetype == CFoldMark::Solid)	    ? 0 : 1);
				m_backgroundCombo.SetCurSel( (pFoldMark->m_nBkMode	 == CFoldMark::Transparent) ? 0 : 1);
			}
		}

		m_colorControl.LoadData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
	}

	UpdateData(FALSE);
}

void CDlgFoldMarkContent::SaveData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::FoldMark)
		return;

	UpdateData(TRUE);

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				int			 nIndex				= pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
				if (nIndex != -1)	// invalid string
				{
					CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
					CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
					strParams.TrimLeft(); strParams.TrimRight();
					
					BOOL bSolid		  = (m_linetypeCombo.GetCurSel()   == 0) ? TRUE : FALSE;
					BOOL bTransparent = (m_backgroundCombo.GetCurSel() == 0) ? TRUE : FALSE;
					BOOL bBackground  = (strParams.Find(_T("/Background"))  != -1) ? TRUE : FALSE;

					if (strContentType == "$FOLD_MARK")
						pObjectSource->m_strSystemCreationInfo.Format(_T("$FOLD_MARK %s %s %s"), (bSolid)	    ? _T("/Solid")	     : _T("/Dashed"),   
																					         (bTransparent) ? _T("/Transparent") : _T("/Opaque"),
																					         (bBackground)  ? _T("/Background")  : _T("/Foreground") );
					pObjectSource->m_nType = CPageSource::SystemCreated;

					m_colorControl.SaveData(pTemplate, pObjectSource, nObjectSourceIndex);
				}
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex != -1)	// invalid string
		{
			CString strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);

			CFoldMark* pFoldMark = (CFoldMark*)pMark;
			pFoldMark->m_MarkSource.m_nType = CPageSource::SystemCreated;

			pFoldMark->m_nLinetype = (m_linetypeCombo.GetCurSel()	== 0) ? CFoldMark::Solid	   : CFoldMark::Dashed;
			pFoldMark->m_nBkMode   = (m_backgroundCombo.GetCurSel() == 0) ? CFoldMark::Transparent : CFoldMark::Opaque;

			if (strContentType == "$FOLD_MARK")
				pFoldMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$FOLD_MARK %s %s %s"), (pFoldMark->m_nLinetype == CFoldMark::Solid)		 ? _T("/Solid")		  : _T("/Dashed"),   
																							  (pFoldMark->m_nBkMode   == CFoldMark::Transparent) ? _T("/Transparent") : _T("/Opaque"),
																							  (pFoldMark->m_nZOrder   == CFoldMark::Background)  ? _T("/Background")  : _T("/Foreground") );
		}

		m_colorControl.SaveData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
	}
}

void CDlgFoldMarkContent::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}

void CDlgFoldMarkContent::OnPaint() 
{
	CDialog::OnPaint();

	m_colorControl.Invalidate();
	m_colorControl.UpdateWindow();
}

void CDlgFoldMarkContent::OnMarkForeground() 
{
	
}
