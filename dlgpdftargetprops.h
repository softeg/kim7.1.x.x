#if !defined(AFX_DLGPDFTARGETPROPS_H__C13FA691_F8BE_11D3_8795_204C4F4F5020__INCLUDED_)
#define AFX_DLGPDFTARGETPROPS_H__C13FA691_F8BE_11D3_8795_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPDFTargetProps.h : Header-Datei
//


#include "DlgPDFTargetProps.h"
#include "PlaceholderEdit.h"
#include "MediaPivotControl.h"
#include "MyBitmapButton.h"


extern int SFW[];
extern int SFH[];
extern int NEWSW[];
extern int NEWSH[];
extern int DIGITALW[];
extern int DIGITALH[];

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFTargetProps 

class CDlgPDFTargetProps : public CDialog
{
// Konstruktion
public:
	CDlgPDFTargetProps(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgPDFTargetProps();


//Attributes:
public:
	CMediaPivotControl		m_mediaPivotCtrl;
	CString					m_strDlgTitle;
	CString					m_strTileNumber;
	CPDFTargetProperties	m_pdfTargetProps;
	COutputFormatList		m_defaultFormats;
	CWnd*					m_pCallWnd;
	CWnd*					m_pWndMsgTarget;
	void					(*m_pfnCallback)(CDlgPDFTargetProps* pDlg, CWnd* pCallWnd, int nIDReturn);
	int						m_nCurTileIndex;
	int						m_nMediaRotation;
	CBitmap					m_bmRotation;
	CToolTipCtrl*			m_pToolTip;
	CMenu					m_placeholderMenu;
	CMenu					m_cuttingDataPlaceholderMenu;
	CPlaceholderEdit		m_pathPlaceholderEdit;
	CPlaceholderEdit		m_fileNamePlaceholderEdit;
	CPlaceholderEdit		m_cuttingDataPlaceholderEdit;
	CMyBitmapButton			m_buttonSelectPathPlaceholder, m_buttonSelectFileNamePlaceholder;
	CMyBitmapButton			m_buttonSelectCuttingDataFilenamePlaceholder;
	int						m_nAutoAlignTo;
	int						m_nPlaceholderMessageSource;
	int						m_nOutputTarget;


//Operations:
public:
	static void	CheckTargetPathSideButtons(CDialog* pDlg);
	void	InitExposerCombo();
	void	InitTileSpin();
	void	InitRotationBitmap();
	void	UpdatePrintSheetView();
	CString GetCurrentForm(CString strPrinterName, short dmPaperSize, float* pfPaperWidth, float* pfPaperHeight, PDEVMODE pDevMode);
	CString AddFilenamePlaceHolder(int nSel, CString strFilename);
	void	UpdateControls();
	void	GetCurrentSelection(CPrintSheet** pPrintSheet, CPlate** pPlate, int& nSide);
	void	CalibrateOutput(BOOL bOpenInAcrobat);
	void	OnSelectPlaceholder(int nID);
	void		 CheckOutputFormat();
	static float _SFW() {return (SFW[0] + SFW[1]*10.0f + SFW[2] * 100.0f); };
	static float _SFH() {return (SFH[0] + SFH[1]*10.0f + SFH[2] * 100.0f); };
	static float _NEWSW() {return (NEWSW[0] + NEWSW[1]*10.0f + NEWSW[2] * 100.0f); };
	static float _NEWSH() {return (NEWSH[0] + NEWSH[1]*10.0f + NEWSH[2] * 100.0f); };
	static float _DIGITALW() {return (DIGITALW[0] + DIGITALW[1]*10.0f + DIGITALW[2] * 100.0f); };
	static float _DIGITALH() {return (DIGITALH[0] + DIGITALH[1]*10.0f + DIGITALH[2] * 100.0f); };


// Dialogfelddaten
	//{{AFX_DATA(CDlgPDFTargetProps)
	enum { IDD = IDD_PDFTARGET_PROPS };
	CComboBox	m_mediaNameCombo;
	CSpinButtonCtrl	m_correctionYSpin;
	CSpinButtonCtrl	m_correctionXSpin;
	CSpinButtonCtrl	m_distanceYSpin;
	CSpinButtonCtrl	m_distanceXSpin;
	CComboBoxEx	m_exposerCombo;
	CSpinButtonCtrl	m_widthSpin;
	CSpinButtonCtrl	m_heightSpin;
	float	m_fMediaHeight;
	float	m_fMediaWidth;
	CString	m_strPDFTargetName;
	CString	m_strFolder;
	CString	m_strExposer;
	BOOL	m_bMarkFrameCheck;
	BOOL	m_bPageFrameCheck;
	BOOL	m_bPlateFrameCheck;
	BOOL	m_bSheetFrameCheck;
	BOOL	m_bShrinkToFit;
	BOOL	m_bUsePrinterHalftones;
	float	m_fMediaDistanceX;
	float	m_fMediaDistanceY;
	float	m_fMediaCorrectionX;
	float	m_fMediaCorrectionY;
	CString	m_strMediaName;
	CString	m_strOutputFilename;
	int		m_nJDFCompatibility;
	BOOL	m_bBoxShapeCheck;
	int		m_nTargetPathSide;
	BOOL	m_bJDFMarksInFolder;
	CString m_strJDFMarksFolder;
	BOOL	m_bJDFOutputMime;
	int		m_nBookblockDoubleSingle;
	BOOL	m_bOutputCuttingData;
	CString m_strCuttingDataFolder;
	CString	m_strCuttingDataFilename;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPDFTargetProps)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPDFTargetProps)
	virtual BOOL OnInitDialog();
	afx_msg void OnBrowseFolder();
	afx_msg void OnBrowseMarkfileFolder();
	afx_msg void OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnClose();
	afx_msg void OnSelchangeExposerCombo();
	afx_msg void OnAddTile();
	afx_msg void OnDeleteTile();
	afx_msg void OnApplyButton();
	afx_msg void OnChangeMediaHeight();
	afx_msg void OnChangeMediaWidth();
	afx_msg void OnChangePdftargetName();
	afx_msg void OnDeltaposTileSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeOutputFilename();
	afx_msg void OnDeltaposMediaOrientationSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnDeltaposDistancexSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposDistanceySpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeMediaDistancex();
	afx_msg void OnChangeMediaDistancey();
	afx_msg void OnChangeMediaCorrectx();
	afx_msg void OnChangeMediaCorrecty();
	afx_msg void OnDeltaposMediaCorrectxSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposMediaCorrectySpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTestOutput();
	afx_msg void OnLaunchAcrobat();
	afx_msg void OnShrinktoFit();
	afx_msg void OnOutputframeColorants();
	afx_msg void OnRemoveFormat();
	afx_msg void OnAddFormat();
	afx_msg void OnSelchangeMedianameCombo();
	afx_msg void OnButtonSelectPathPlaceholder();
	afx_msg void OnButtonSelectFilenamePlaceholder();
	afx_msg void OnPlaceholderFoldsheetNum();
	afx_msg void OnPlaceholderSheetside();
	afx_msg void OnPlaceholderWebNum();
	afx_msg void OnPlaceholderColorNum();
	afx_msg void OnPlaceholderFoldsheetNumCG();
	afx_msg void OnPlaceholderJobTitle();
	afx_msg void OnPlaceholderTileNum();
	afx_msg void OnPlaceholderSheetNum();
	afx_msg void OnPlaceholderFile();
	afx_msg void OnPlaceholderLowpageSide();
	afx_msg void OnPlaceholderLowpageSheet();
	afx_msg void OnPlaceholderHighpageSide();
	afx_msg void OnPlaceholderHighpageSheet();
	afx_msg void OnPlaceholderPressDevice();
	afx_msg void OnPlaceholderJob();
	afx_msg void OnPlaceholderJobNum();
	afx_msg void OnPlaceholderCustomer();
	afx_msg void OnPlaceholderCustomerNum();
	afx_msg void OnPlaceholderCreator();
	afx_msg void OnPlaceholderCreationDate();
	afx_msg void OnPlaceholderLastModified();
	afx_msg void OnPlaceholderScheduledToPrint();
	afx_msg void OnPlaceholderOutputDate();
	afx_msg void OnPlaceholderNotes();
	afx_msg void OnPlaceholderColorName();
	afx_msg void OnPlaceholderPageNumList();
	afx_msg void OnPlaceholderPageNum();
	afx_msg void OnPlaceholderProductName();
	afx_msg void OnPlaceholderProductPartName();
	afx_msg void OnPlaceholderPaperGrain();
	afx_msg void OnPlaceholderPaperType();
	afx_msg void OnPlaceholderPaperGrammage();
	afx_msg void OnPlaceholderWorkStyle();
	afx_msg void OnPlaceholderFoldScheme();
	afx_msg void OnKillfocusMediaHeight();
	afx_msg void OnKillfocusMediaWidth();
	afx_msg void OnAlign2Plate();
	afx_msg void OnAlign2Sheet();
	afx_msg void OnTargetpathFront();
	afx_msg void OnTargetpathBack();
	afx_msg void OnBnClickedButtonFilenamePlaceholderInfo();
	afx_msg void OnMaskInFolderCheck();
	afx_msg void OnOutputMimeCheck();
	afx_msg void OnJDFOutputDetails();
	afx_msg void OnBookblockDoublePage();
	afx_msg void OnBookblockSinglePage();
	afx_msg void OnCuttingDataBrowseFolder();
	afx_msg void OnButtonSelectCuttingDataFilenamePlaceholder();
	afx_msg void OnBnClickedButtonCuttingDataFilenamePlaceholderInfo();

	//}}AFX_MSG
#ifdef CTP
	afx_msg void OnTagExport();
#endif
	afx_msg BOOL OnToolTipNeedText(UINT id, NMHDR * pNMHDR, LRESULT * pResult);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPDFTARGETPROPS_H__C13FA691_F8BE_11D3_8795_204C4F4F5020__INCLUDED_
