// JDFOutputDetails.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgJDFOutputDetails.h"


// CDlgJDFOutputDetails-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgJDFOutputDetails, CDialog)

CDlgJDFOutputDetails::CDlgJDFOutputDetails(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgJDFOutputDetails::IDD, pParent)
{
	m_nCurPlaceholderEditID = -1;
}

CDlgJDFOutputDetails::~CDlgJDFOutputDetails()
{
}

void CDlgJDFOutputDetails::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_JDF_JOBID_MAP, m_strJDFJobIDMap);
	DDX_Text(pDX, IDC_JDF_JOBPARTID_MAP, m_strJDFJobPartIDMap);
	DDX_Text(pDX, IDC_JDF_DESCRIPTIVENAME_MAP, m_strJDFDescriptiveNameMap);
	DDX_Text(pDX, IDC_APOGEE_TRAPPING_SET, m_strApogeeTrappingSet);
	DDX_Text(pDX, IDC_APOGEE_SEPARATION_SET, m_strApogeeSeparationSet);
	DDX_Text(pDX, IDC_APOGEE_RENDERING_SET, m_strApogeeRenderingSet);
	DDX_Text(pDX, IDC_APOGEE_SCREENING_SET, m_strApogeeScreeningSet);
	DDX_Text(pDX, IDC_APOGEE_PRINT_SET, m_strApogeePrintSet);
	DDX_Text(pDX, IDC_APOGEE_OUTPUT_SET, m_strApogeeOutputSet);
	DDX_Text(pDX, IDC_APOGEE_IMAGE_SET, m_strApogeeImageSet);
}


BEGIN_MESSAGE_MAP(CDlgJDFOutputDetails, CDialog)
	ON_BN_CLICKED(IDC_JOBID_PLACEHOLDER_BUTTON, OnButtonSelectJobIDPlaceholder)
	ON_BN_CLICKED(IDC_JOBPARTID_PLACEHOLDER_BUTTON, OnButtonSelectJobPartIDPlaceholder)
	ON_BN_CLICKED(IDC_DESCRIPTIVENAME_PLACEHOLDER_BUTTON, OnButtonSelectDescriptiveNamePlaceholder)
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNeedText )
	ON_COMMAND(ID_PLACEHOLDER_FOLDSHEETNUM,			OnPlaceholderFoldsheetNum)
	ON_COMMAND(ID_PLACEHOLDER_SHEETSIDE,			OnPlaceholderSheetside)
	ON_COMMAND(ID_PLACEHOLDER_WEBNUM,				OnPlaceholderWebNum)
	ON_COMMAND(ID_PLACEHOLDER_COLORNUM,				OnPlaceholderColorNum)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSHEETNUM_CG,		OnPlaceholderFoldsheetNumCG)
	ON_COMMAND(ID_PLACEHOLDER_JOBTITLE,				OnPlaceholderJobTitle)
	ON_COMMAND(ID_PLACEHOLDER_TILENUM,				OnPlaceholderTileNum)
	ON_COMMAND(ID_PLACEHOLDER_SHEETNUM,				OnPlaceholderSheetNum)
	ON_COMMAND(ID_PLACEHOLDER_FILE,					OnPlaceholderFile)
	ON_COMMAND(ID_PLACEHOLDER_LOWPAGE_SIDE,			OnPlaceholderLowpageSide)
	ON_COMMAND(ID_PLACEHOLDER_LOWPAGE_SHEET,		OnPlaceholderLowpageSheet)
	ON_COMMAND(ID_PLACEHOLDER_HIGHPAGE_SIDE,		OnPlaceholderHighpageSide)
	ON_COMMAND(ID_PLACEHOLDER_HIGHPAGE_SHEET,		OnPlaceholderHighpageSheet)
	ON_COMMAND(ID_PLACEHOLDER_PRESSDEVICE,			OnPlaceholderPressDevice)
	ON_COMMAND(ID_PLACEHOLDER_JOB,					OnPlaceholderJob)
	ON_COMMAND(ID_PLACEHOLDER_JOBNUM,				OnPlaceholderJobNum)
	ON_COMMAND(ID_PLACEHOLDER_CUSTOMER,				OnPlaceholderCustomer)
	ON_COMMAND(ID_PLACEHOLDER_CUSTOMERNUM,			OnPlaceholderCustomerNum)
	ON_COMMAND(ID_PLACEHOLDER_CREATOR,				OnPlaceholderCreator)
	ON_COMMAND(ID_PLACEHOLDER_CREATIONDATE,			OnPlaceholderCreationDate)
	ON_COMMAND(ID_PLACEHOLDER_LASTMODIFIED,			OnPlaceholderLastModified)
	ON_COMMAND(ID_PLACEHOLDER_SCHEDULEDTOPRINT,		OnPlaceholderScheduledToPrint)
	ON_COMMAND(ID_PLACEHOLDER_OUTPUTDATE,			OnPlaceholderOutputDate)
	ON_COMMAND(ID_PLACEHOLDER_NOTES,				OnPlaceholderNotes)
	ON_COMMAND(ID_PLACEHOLDER_COLORNAME,			OnPlaceholderColorName)
	ON_COMMAND(ID_PLACEHOLDER_PAGENUMLIST,			OnPlaceholderPageNumList)
	ON_COMMAND(ID_PLACEHOLDER_PAGENUM,				OnPlaceholderPageNum)
	ON_COMMAND(ID_PLACEHOLDER_PRODUCTNAME,			OnPlaceholderProductName)
	ON_COMMAND(ID_PLACEHOLDER_PRODUCTPARTNAME,		OnPlaceholderProductPartName)
	ON_COMMAND(ID_PLACEHOLDER_PAPERGRAIN,			OnPlaceholderPaperGrain)
	ON_COMMAND(ID_PLACEHOLDER_PAPERTYPE,			OnPlaceholderPaperType)
	ON_COMMAND(ID_PLACEHOLDER_PAPERGRAMMAGE,		OnPlaceholderPaperGrammage)
	ON_COMMAND(ID_PLACEHOLDER_WORKSTYLE,			OnPlaceholderWorkStyle)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSCHEME,			OnPlaceholderFoldScheme)
END_MESSAGE_MAP()


// CDlgJDFOutputDetails-Meldungshandler

BOOL CDlgJDFOutputDetails::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_buttonSelectJobIDPlaceholder.SubclassDlgItem(IDC_JOBID_PLACEHOLDER_BUTTON, this);
	m_buttonSelectJobIDPlaceholder.SetTheme(_T("COMBOBOX"), CP_DROPDOWNBUTTON, XCENTER);
	m_buttonSelectJobIDPlaceholder.SetBitmap(IDB_DROPDOWN, IDB_DROPDOWN, -1, XCENTER);

	m_buttonSelectJobPartIDPlaceholder.SubclassDlgItem(IDC_JOBPARTID_PLACEHOLDER_BUTTON, this);
	m_buttonSelectJobPartIDPlaceholder.SetTheme(_T("COMBOBOX"), CP_DROPDOWNBUTTON, XCENTER);
	m_buttonSelectJobPartIDPlaceholder.SetBitmap(IDB_DROPDOWN, IDB_DROPDOWN, -1, XCENTER);

	m_buttonSelectDescriptiveNamePlaceholder.SubclassDlgItem(IDC_DESCRIPTIVENAME_PLACEHOLDER_BUTTON, this);
	m_buttonSelectDescriptiveNamePlaceholder.SetTheme(_T("COMBOBOX"), CP_DROPDOWNBUTTON, XCENTER);
	m_buttonSelectDescriptiveNamePlaceholder.SetBitmap(IDB_DROPDOWN, IDB_DROPDOWN, -1, XCENTER);

	if ( ! m_placeholderMenu.m_hMenu)
		VERIFY(m_placeholderMenu.LoadMenu(IDR_PLACEHOLDER_POPUP));

	VERIFY(m_placeholderJobIDEdit.SubclassDlgItem(IDC_JDF_JOBID_MAP, this));
	m_placeholderJobIDEdit.m_pParent			= this;
	m_placeholderJobIDEdit.m_pPlaceholderMenu	= &m_placeholderMenu;
	m_placeholderJobIDEdit.m_nIDButton			= IDC_JOBID_PLACEHOLDER_BUTTON;

	VERIFY(m_placeholderJobPartIDEdit.SubclassDlgItem(IDC_JDF_JOBPARTID_MAP, this));
	m_placeholderJobPartIDEdit.m_pParent			= this;
	m_placeholderJobPartIDEdit.m_pPlaceholderMenu	= &m_placeholderMenu;
	m_placeholderJobPartIDEdit.m_nIDButton			= IDC_JOBPARTID_PLACEHOLDER_BUTTON;

	VERIFY(m_placeholderDescriptiveNameEdit.SubclassDlgItem(IDC_JDF_DESCRIPTIVENAME_MAP, this));
	m_placeholderDescriptiveNameEdit.m_pParent			= this;
	m_placeholderDescriptiveNameEdit.m_pPlaceholderMenu	= &m_placeholderMenu;
	m_placeholderDescriptiveNameEdit.m_nIDButton		= IDC_DESCRIPTIVENAME_PLACEHOLDER_BUTTON;


	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgJDFOutputDetails::OnButtonSelectJobIDPlaceholder() 
{
	m_nCurPlaceholderEditID = IDC_JDF_JOBID_MAP;
	m_placeholderJobIDEdit.SetFocus();
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup)
	{
		CRect buttonRect;
		GetDlgItem(IDC_JOBID_PLACEHOLDER_BUTTON)->GetWindowRect(buttonRect);
		pPopup->TrackPopupMenu(TPM_RIGHTALIGN | TPM_RIGHTBUTTON, buttonRect.right, buttonRect.bottom, this);
	}

	((CButton*)GetDlgItem(IDC_JOBID_PLACEHOLDER_BUTTON))->SetCheck(0);
}

void CDlgJDFOutputDetails::OnButtonSelectJobPartIDPlaceholder() 
{
	m_nCurPlaceholderEditID = IDC_JDF_JOBPARTID_MAP;
	m_placeholderJobPartIDEdit.SetFocus();
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup)
	{
		CRect buttonRect;
		GetDlgItem(IDC_JOBPARTID_PLACEHOLDER_BUTTON)->GetWindowRect(buttonRect);
		pPopup->TrackPopupMenu(TPM_RIGHTALIGN | TPM_RIGHTBUTTON, buttonRect.right, buttonRect.bottom, this);
	}

	((CButton*)GetDlgItem(IDC_JOBPARTID_PLACEHOLDER_BUTTON))->SetCheck(0);
}

void CDlgJDFOutputDetails::OnButtonSelectDescriptiveNamePlaceholder() 
{
	m_nCurPlaceholderEditID = IDC_JDF_DESCRIPTIVENAME_MAP;
	m_placeholderDescriptiveNameEdit.SetFocus();
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup)
	{
		CRect buttonRect;
		GetDlgItem(IDC_DESCRIPTIVENAME_PLACEHOLDER_BUTTON)->GetWindowRect(buttonRect);
		pPopup->TrackPopupMenu(TPM_RIGHTALIGN | TPM_RIGHTBUTTON, buttonRect.right, buttonRect.bottom, this);
	}

	((CButton*)GetDlgItem(IDC_DESCRIPTIVENAME_PLACEHOLDER_BUTTON))->SetCheck(0);
}

void CDlgJDFOutputDetails::OnSelectPlaceholder(int nID)
{
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if ( ! pPopup)
		return;

	CString strPlaceholder;
	pPopup->GetMenuString(nID, strPlaceholder, MF_BYCOMMAND);
	strPlaceholder = strPlaceholder.Left(3);
	strPlaceholder.Delete(1, 1);	// remove &

	UpdateData(TRUE);

	int nStartChar, nEndChar;
	((CEdit*)GetDlgItem(m_nCurPlaceholderEditID))->GetSel(nStartChar, nEndChar);

	CString* pstrMap = NULL;
	switch (m_nCurPlaceholderEditID)
	{
	case IDC_JDF_JOBID_MAP:				pstrMap = &m_strJDFJobIDMap;			break;
	case IDC_JDF_JOBPARTID_MAP:			pstrMap = &m_strJDFJobPartIDMap;		break;
	case IDC_JDF_DESCRIPTIVENAME_MAP:	pstrMap = &m_strJDFDescriptiveNameMap;	break;
	}

	if (nStartChar == nEndChar)
		if (nStartChar > 0)
			if (pstrMap->GetAt(nStartChar - 1) == '$')		// assume user already typed $ -> remove $ to prevent from being inserted twice
				nStartChar--;

	pstrMap->Delete(nStartChar, nEndChar - nStartChar);
	pstrMap->Insert(nStartChar, strPlaceholder);

	UpdateData(FALSE);

	((CEdit*)GetDlgItem(m_nCurPlaceholderEditID))->SetSel(nStartChar + 2, nStartChar + 2);
}

void CDlgJDFOutputDetails::OnPlaceholderFoldsheetNum()		{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSHEETNUM);		}
void CDlgJDFOutputDetails::OnPlaceholderSheetside()			{ OnSelectPlaceholder(ID_PLACEHOLDER_SHEETSIDE);		}		
void CDlgJDFOutputDetails::OnPlaceholderWebNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_WEBNUM);			}
void CDlgJDFOutputDetails::OnPlaceholderColorNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_COLORNUM);			}	
void CDlgJDFOutputDetails::OnPlaceholderFoldsheetNumCG()	{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSHEETNUM_CG);	}
void CDlgJDFOutputDetails::OnPlaceholderJobTitle()			{ OnSelectPlaceholder(ID_PLACEHOLDER_JOBTITLE);			}
void CDlgJDFOutputDetails::OnPlaceholderTileNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_TILENUM);			}
void CDlgJDFOutputDetails::OnPlaceholderSheetNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_SHEETNUM);			}
void CDlgJDFOutputDetails::OnPlaceholderFile()				{ OnSelectPlaceholder(ID_PLACEHOLDER_FILE);				}
void CDlgJDFOutputDetails::OnPlaceholderLowpageSide()		{ OnSelectPlaceholder(ID_PLACEHOLDER_LOWPAGE_SIDE);		}
void CDlgJDFOutputDetails::OnPlaceholderLowpageSheet()		{ OnSelectPlaceholder(ID_PLACEHOLDER_LOWPAGE_SHEET);	}
void CDlgJDFOutputDetails::OnPlaceholderHighpageSide()		{ OnSelectPlaceholder(ID_PLACEHOLDER_HIGHPAGE_SIDE);	}
void CDlgJDFOutputDetails::OnPlaceholderHighpageSheet()		{ OnSelectPlaceholder(ID_PLACEHOLDER_HIGHPAGE_SHEET);	}
void CDlgJDFOutputDetails::OnPlaceholderPressDevice()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PRESSDEVICE);		}
void CDlgJDFOutputDetails::OnPlaceholderJob()				{ OnSelectPlaceholder(ID_PLACEHOLDER_JOB);				}
void CDlgJDFOutputDetails::OnPlaceholderJobNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_JOBNUM);			}
void CDlgJDFOutputDetails::OnPlaceholderCustomer()			{ OnSelectPlaceholder(ID_PLACEHOLDER_CUSTOMER);			}
void CDlgJDFOutputDetails::OnPlaceholderCustomerNum()		{ OnSelectPlaceholder(ID_PLACEHOLDER_CUSTOMERNUM);		}
void CDlgJDFOutputDetails::OnPlaceholderCreator()			{ OnSelectPlaceholder(ID_PLACEHOLDER_CREATOR);			}
void CDlgJDFOutputDetails::OnPlaceholderCreationDate()		{ OnSelectPlaceholder(ID_PLACEHOLDER_CREATIONDATE);		}
void CDlgJDFOutputDetails::OnPlaceholderLastModified()		{ OnSelectPlaceholder(ID_PLACEHOLDER_LASTMODIFIED);		}
void CDlgJDFOutputDetails::OnPlaceholderScheduledToPrint()	{ OnSelectPlaceholder(ID_PLACEHOLDER_SCHEDULEDTOPRINT);	}
void CDlgJDFOutputDetails::OnPlaceholderOutputDate()		{ OnSelectPlaceholder(ID_PLACEHOLDER_OUTPUTDATE);		}
void CDlgJDFOutputDetails::OnPlaceholderNotes()				{ OnSelectPlaceholder(ID_PLACEHOLDER_NOTES);			}
void CDlgJDFOutputDetails::OnPlaceholderColorName()			{ OnSelectPlaceholder(ID_PLACEHOLDER_COLORNAME);		}
void CDlgJDFOutputDetails::OnPlaceholderPageNumList()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAGENUMLIST);		}
void CDlgJDFOutputDetails::OnPlaceholderPageNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_PAGENUM);			}
void CDlgJDFOutputDetails::OnPlaceholderProductName()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PRODUCTNAME);		}
void CDlgJDFOutputDetails::OnPlaceholderProductPartName()	{ OnSelectPlaceholder(ID_PLACEHOLDER_PRODUCTPARTNAME);	}
void CDlgJDFOutputDetails::OnPlaceholderPaperGrain()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERGRAIN);		}
void CDlgJDFOutputDetails::OnPlaceholderPaperType()			{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERTYPE);		}
void CDlgJDFOutputDetails::OnPlaceholderPaperGrammage()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERGRAMMAGE);	}
void CDlgJDFOutputDetails::OnPlaceholderWorkStyle()			{ OnSelectPlaceholder(ID_PLACEHOLDER_WORKSTYLE);		}
void CDlgJDFOutputDetails::OnPlaceholderFoldScheme()		{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSCHEME);		}


BOOL CDlgJDFOutputDetails::OnToolTipNeedText(UINT id, NMHDR * pNMHDR, LRESULT * /*pResult*/)
{
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if ( ! pPopup)
		return FALSE;

	CString strPlaceholder = m_placeholderJobIDEdit.GetPlaceholderHit();
	if (strPlaceholder.IsEmpty())
		strPlaceholder = m_placeholderJobPartIDEdit.GetPlaceholderHit();
	if (strPlaceholder.IsEmpty())
		strPlaceholder = m_placeholderDescriptiveNameEdit.GetPlaceholderHit();
	if (strPlaceholder.IsEmpty())
		return FALSE;

	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	pTTT->szText[0] = 0;

	strPlaceholder.Insert(1, '&');
	for (UINT i = 0; i < pPopup->GetMenuItemCount(); i++)
	{
		CString strMenu;
		pPopup->GetMenuString(i, strMenu, MF_BYPOSITION);
		if (strPlaceholder == strMenu.Left(3))
		{
			strMenu = strMenu.Right(strMenu.GetLength() - 4);
			_tcscpy(pTTT->szText, strMenu);
			return TRUE;
		}
	}

	return FALSE;
}
