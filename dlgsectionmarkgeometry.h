#if !defined(AFX_DLGSECTIONMARKGEOMETRY_H__2EEA8B13_63D7_11D3_8712_204C4F4F5020__INCLUDED_)
#define AFX_DLGSECTIONMARKGEOMETRY_H__2EEA8B13_63D7_11D3_8712_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSectionMarkGeometry.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSectionMarkGeometry 

class CDlgSectionMarkGeometry : public CDialog
{
// Konstruktion
public:
	CDlgSectionMarkGeometry(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgSectionMarkGeometry();


// Dialogfelddaten
	//{{AFX_DATA(CDlgSectionMarkGeometry)
	enum { IDD = IDD_SECTIONMARK_GEOMETRY };
	CSpinButtonCtrl	m_headMarginSpin;
	CSpinButtonCtrl	m_footMarginSpin;
	CSpinButtonCtrl	m_safetyDistanceSpin;
	float	m_fFootMargin;
	float	m_fHeadMargin;
	float	m_fSafetyDistance;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl* m_pParent;
	CBrush						   m_hollowBrush;
	CBitmap						   m_bmSectionMarkPreview;
	int							   m_nPosition;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgSectionMarkGeometry)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void LoadData();
	void SaveData();
	void Apply();


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgSectionMarkGeometry)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSectionmarkFootmarginSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSectionmarkHeadmarginSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSectionmarkSafetyDistanceSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSectionMarkSpine();
	afx_msg void OnSectionMarkHeadFoot();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGSECTIONMARKGEOMETRY_H__2EEA8B13_63D7_11D3_8712_204C4F4F5020__INCLUDED_
