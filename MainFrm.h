// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once


#include "TrueColorToolBar.h"
#include "PrintSheetFrame.h"
#include "NewPrintSheetFrame.h"
#include "AutoServerFrame.h"			
#include "OrderDataFrame.h"			
#include "ProdDataFrame.h"			
#include "JobDocumentationFrame.h"	
#include "PageListFrame.h"	
#include "BookblockFrame.h"			


class CMyStatusBar : public CStatusBar
{
public:
	CMyStatusBar() { m_pDoc = NULL; };

public:
	CImpManDoc* m_pDoc;

protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
};



class CDlgPDFLibStatus;

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	int m_nDatePaneNo;
	void OnProperties();
	CMainFrame();

enum { AutoBackupTimerID = 8888, DongleQueryTimerID = 7777, WatchPageSourceTimerID = 6666, CheckRemainingDaysTimerID = 5555, CheckServerConnection = 4444 };
enum { NavSelectionAutoServer = 1, NavSelectionOrderData = 2, NavSelectionPrintSheets = 3, NavSelectionPrintSheetMarks = 4, NavSelectionBookBlock = 5, NavSelectionPageList = 6, NavSelectionPrepress = 7, NavSelectionOutput = 8, NavSelectionJobDocumentation = 9};

// Attributes
public:
	CPageSource*		m_pPageSource;
	CMyStatusBar		m_wndStatusBar;
	CToolBar			m_wndToolBar;
	CTrueColorToolBar	m_wndNavigationBar, m_productGroupsBar, m_showTableBar, m_wndKIMLogoBar;
	int					m_nNavSelection;
	CReBar				m_wndReBar;

	CWnd				m_wndMDIClient;			// used to find frames (i.e. CAutoServerFrame) not connected to document
	CWnd*				m_pWndCurrentChild;		// 
	CMDIChildWnd*		GetNextMDIChildWnd();	//

// Operations
public:
	void							InitNavigationToolbar();
	static CAutoServerFrame*		CreateOrActivateAutoServerFrame(CDocTemplate* pTemplate, CDocument* pDoc);
	static COrderDataFrame*			CreateOrActivateOrderDataFrame(CDocTemplate* pTemplate, CDocument* pDoc);
	static CProdDataFrame*			CreateOrActivateProdDataFrame(CDocTemplate* pTemplate, CDocument* pDoc);
	static CJobDocumentationFrame*	CreateOrActivateJobDocumentationFrame(CDocTemplate* pTemplate, CDocument* pDoc);
	static CBookblockFrame*			CreateOrActivateBookblockFrame(CDocTemplate* pTemplate, CDocument* pDoc);
	static CPrintSheetFrame*		CreateOrActivatePrintSheetFrame(CDocTemplate* pTemplate, CDocument* pDoc);
	static CNewPrintSheetFrame*		CreateOrActivateNewPrintSheetFrame(CDocTemplate* pTemplate, CDocument* pDoc);
	static CPageListFrame*			CreateOrActivatePageListFrame(CDocTemplate* pTemplate, CDocument* pDoc);
	static void						SaveFrameStatus   (CWnd* pWnd, CSplitterWnd* pWndSplitter1 = NULL, CSplitterWnd* pWndSplitter2 = NULL, CSplitterWnd* pWndSplitter3 = NULL,
													   CSplitterWnd* pWndSplitter4 = NULL, CSplitterWnd* pWndSplitter5 = NULL, int nSizePrivate = 0, LPBYTE lpPrivate = NULL, CString str2ndName = _T(""));
	static void						RestoreFrameStatus(CWnd* pWnd, BOOL& rbStatusRestored, int& rnCmdShow, CSplitterWnd* pWndSplitter1 = NULL, CSplitterWnd* pWndSplitter2 = NULL, CSplitterWnd* pWndSplitter3 = NULL, 
													   CSplitterWnd* pWndSplitter4 = NULL, CSplitterWnd* pWndSplitter5 = NULL, int nSizePrivate = 0, LPBYTE lpPrivate = NULL, CString str2ndName = _T(""));
	virtual void					OnUpdateFrameTitle(BOOL bAddToTitle);


	virtual BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);
	void	ShowToolBarCtrl(int nID, int nShow);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	afx_msg void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnFileSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDate(CCmdUI* pCmdUI);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnFilePageSetup();
	afx_msg void OnLicenseSetup();
	afx_msg void OnKrauseHomepage();
	afx_msg void OnKimUpdates();
	afx_msg void OnDestroy();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnClose();
	afx_msg void OnHelp();
	afx_msg void OnHelpFinder();
	afx_msg void OnKimHomepage();
	afx_msg void OnNavAutoServer();
	afx_msg void OnNavAutoServerClose();
	afx_msg void OnNavOrderdata();
	afx_msg void OnNavPrintsheetlist();
	afx_msg void OnNavPrintsheetMarks();
	afx_msg void OnNavBookblock();
	afx_msg void OnNavPagelist();
	afx_msg void OnNavPrepress();
	afx_msg void OnNavOutput();
	afx_msg void OnNavJobDocumentation();
	afx_msg void OnNavResources();
	afx_msg void OnNavJobMap();
	afx_msg void OnUpdateNavAutoServer(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavAutoServerClose(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavOrderdata(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavPrintsheetlist(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavPrintsheetMarks(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavBookblock(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavPagelist(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavPrepress(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavOutput(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavJobDocumentation(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNavResources(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	UINT  m_nStatusPane1ID;
	UINT  m_nStatusPane1Style;
	INT   m_nStatusPane1Width;
	BOOL  m_bMenuSelect;
	BOOL  InitStatusBar(UINT *pIndicators, int nSize, int nSeconds);
	CSize GetStatusBarStringWidth(CString string);
public:
	afx_msg void OnImportJdf();
	afx_msg void OnExportJdf();
	afx_msg void OnUpdateImportJdf(CCmdUI* pCmdUI);
	afx_msg void OnUpdateExportJdf(CCmdUI* pCmdUI);
	afx_msg void OnModifyOrderData();
	afx_msg void OnModifyProduct();
	afx_msg void OnModifyStripping();
	//afx_msg void OnModifyBookblock();
	afx_msg void OnResourceSheetformats();
	afx_msg void OnResourcePageformats();
	afx_msg void OnResourcePapers();
	afx_msg void OnResourceProductionprofiles();
	afx_msg void OnResourceControlMarks();
	afx_msg void OnResourcePrintSheetOutputProfiles();
	afx_msg void OnResourceFoldSheetOutputProfiles();
	afx_msg void OnResourceBookletOutputProfiles();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};

/////////////////////////////////////////////////////////////////////////////
