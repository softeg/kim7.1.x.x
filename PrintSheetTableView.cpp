// PrintSheetTableView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "PageListFrame.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PrintSheetTableView.h"
#include "PrintSheetColorSettingsView.h"
#include "PageListTableView.h"
#include "DlgNotes.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "DlgNumeration.h"
#include "BookBlockView.h"
#include "DlgPrintColorTable.h"
#ifdef PDF
#include "PDFEngineInterface.h"
#include "PDFOutput.h"
#endif
#include "MainFrm.h"
#include "NewPrintSheetFrame.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTableHeaderCtrl

CPrintSheetTableHeaderCtrl::CPrintSheetTableHeaderCtrl()
{
}


void CPrintSheetTableHeaderCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (m_pParent->m_nWhatToShow != CPrintSheetTable::ShowPrintColorInfo)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (lpDrawItemStruct->itemID < 1)	// first column holds sheets
		return;
	if ((int)lpDrawItemStruct->itemID > pDoc->m_ColorDefinitionTable.GetSize())
		return;

	// Attach to a CDC object
	CDC dc;
	dc.Attach(lpDrawItemStruct->hDC);

	CString strColor;
	CRect	rect(CPoint(lpDrawItemStruct->rcItem.left + 5, lpDrawItemStruct->rcItem.bottom - 16), CSize(10,10));
	int		nIndex = m_pParent->m_ColumnRefList[lpDrawItemStruct->itemID].m_pColumn->m_dwData;
	if ( (nIndex >= 0) && (nIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
		strColor = pDoc->m_ColorDefinitionTable[nIndex].m_strColorName;
	if (strColor == _T("Composite"))
	{
		strColor = _T("CMYK");
		dc.DrawIcon(rect.left, lpDrawItemStruct->rcItem.bottom - 18, theApp.LoadIcon(IDI_CMYK));
	}
	else
	{
		if ( (nIndex >= 0) && (nIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
		{
			dc.FillSolidRect(rect, pDoc->m_ColorDefinitionTable[nIndex].m_rgb);
			dc.SelectStockObject(BLACK_PEN);
			dc.SelectStockObject(HOLLOW_BRUSH);
			dc.Rectangle(rect);
		}
	}

	CFont font;
	font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	dc.SelectObject(&font);
	dc.SetBkMode(TRANSPARENT);
	dc.SetTextColor(BLACK);
	
	//CRect rcText(rect.right + 5, lpDrawItemStruct->rcItem.top + 1, lpDrawItemStruct->rcItem.right, lpDrawItemStruct->rcItem.bottom);
	CRect rcText(rect.right + 5, lpDrawItemStruct->rcItem.top + 1, lpDrawItemStruct->rcItem.right, lpDrawItemStruct->rcItem.top + 15);
	rcText.OffsetRect(0, 20*nIndex);
	//CPageListTableView::FitStringToWidth(&dc, strColor, rcText.Width());
	//dc.DrawText(strColor, rcText, DT_LEFT | DT_TOP);
	//dc.TextOutA(rcText.left, rcText.bottom - 15, strColor);
	dc.TextOut(rcText.left, rcText.top, strColor);

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	dc.SelectObject(&pen);
	//dc.MoveTo(rcText.left, lpDrawItemStruct->rcItem.bottom);
	//dc.LineTo(rcText.left, rcText.bottom);
	//dc.LineTo(rcText.left + lpDrawItemStruct->rcItem.right, rcText.bottom);
	dc.MoveTo(rect.CenterPoint().x, rect.top - 1);
	dc.LineTo(rect.CenterPoint().x, rcText.CenterPoint().y);
	dc.LineTo(rcText.left, rcText.CenterPoint().y);

	pen.DeleteObject();
	font.DeleteObject();

	// Detach from the CDC object, otherwise the hDC will be
	// destroyed when the CDC object goes out of scope
	dc.Detach();
}



/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTableColumn

CPrintSheetTableColumn::CPrintSheetTableColumn()
{
	m_nColumnLeft = m_nColumnWidth = 0;
	m_strColumnLabel.Empty();
	m_pParent		 = NULL;
	m_pDisplayList	 = NULL;
	m_dwData		 = 0;
}

CPrintSheetTableColumn::CPrintSheetTableColumn(int nColumnWidth, CString strColumnLabel, class CPrintSheetTable* pParent, DWORD dwData)
{
	m_nColumnWidth	 = nColumnWidth;
	m_strColumnLabel = strColumnLabel;
	m_pParent		 = pParent;
	m_dwData		 = dwData;
}

void CPrintSheetTableColumn::OnDraw(CDC* pDC)
{
	pDC->TextOut(m_nColumnLeft, 20, "Default output");
}

int CPrintSheetTableColumn::GetCellLines(CPrintSheet& rPrintSheet)
{
	return m_pParent->GetCellLines(rPrintSheet);
}


/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTable itself

CPrintSheetTable::CPrintSheetTable()
{
	m_HeaderCtrl.m_pParent = this;

	m_HeaderFont.CreatePointFont(80, _T("Tahoma"));

	m_nHeaderWidth = m_nHeaderHeight = 0;
	m_nWhatToShow  = UnInitialized;

	m_hIcon = theApp.LoadIcon(IDI_INSERT_ARROW);
	m_pBkBitmap = NULL;
	m_redPen.CreatePen(PS_SOLID, 1, RED);
}

CPrintSheetTable::~CPrintSheetTable()
{
	for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
		delete m_ColumnRefList[i].m_pColumn;

	m_HeaderFont.DeleteObject();

	if (m_pBkBitmap)
	{
		m_pBkBitmap->DeleteObject();
		delete m_pBkBitmap;
		m_pBkBitmap = NULL;
	}

	m_redPen.DeleteObject();
}

void CPrintSheetTable::Create(CPrintSheetTableView* pParent)
{
	m_pParent = pParent;

	m_pParent->GetDlgItem(IDC_PRINTSHEET_TABLE_HEADER)->GetWindowRect(m_headerFrameRect);
	m_pParent->ScreenToClient(m_headerFrameRect);
	m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ|HDS_DRAGDROP, m_headerFrameRect, pParent, IDC_PRINTSHEETTABLEHEADER);	
	m_HeaderCtrl.SetFont(&m_HeaderFont, FALSE);

	((CPrintSheetTableView*)m_pParent)->CreatePrintSheetListToolbar();
	((CPrintSheetTableView*)m_pParent)->CreatePrintColorInfoToolbar();
	((CPrintSheetTableView*)m_pParent)->CreateOutputListToolbar();

	m_nWhatToShow = UnInitialized;
}

void CPrintSheetTable::Initialize(int nWhatToShow)
{
	if (m_nWhatToShow == nWhatToShow)	// already initialized
		return;

	m_nWhatToShow = nWhatToShow;

	for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
		delete m_ColumnRefList[i].m_pColumn;

	m_ColumnRefList.RemoveAll();

	CArray<CPrintSheetTableColumn*, CPrintSheetTableColumn*> pColumns;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	switch (nWhatToShow)
	{

	case ShowPrintSheetList:
//		m_HeaderCtrl.ModifyStyle(HDS_BUTTONS, 0);
		m_HeaderCtrl.ModifyStyle(0, HDS_DRAGDROP);
		if (theApp.settings.m_iLanguage == 0)
		{
			pColumns.Add(new CColumnPrintSheetNumber	(100, "Bogen",					this));
			pColumns.Add(new CColumnPrintSheetLayout	(180, "Typ",					this));
			pColumns.Add(new CColumnLayoutNotes			(30,  "",						this));
			pColumns.Add(new CColumnFoldSheetPages		(150, "Seiten",					this));
			pColumns.Add(new CColumnFoldSheetNumPages	(40,  "Anz.",					this));
			pColumns.Add(new CColumnFoldSheet			(200, "Falzbogen [ Schema ]",	this));
			pColumns.Add(new CColumnFoldSheetsNUp		(40,  "Ntz.",					this));
			pColumns.Add(new CColumnPressDevice			(200, "Druckmaschine",			this));
		}
		else
		{
			pColumns.Add(new CColumnPrintSheetNumber	(100, "Sheet",					this));
			pColumns.Add(new CColumnPrintSheetLayout	(180, "Type",					this));
			pColumns.Add(new CColumnLayoutNotes			(30,  "",						this));
			pColumns.Add(new CColumnFoldSheetPages		(150, "Pages",					this));
			pColumns.Add(new CColumnFoldSheetNumPages	(40,  "Num.",					this));
			pColumns.Add(new CColumnFoldSheet			(200, "Foldsheet [ Schema ]",	this));
			pColumns.Add(new CColumnFoldSheetsNUp		(40,  "nUp",					this));
			pColumns.Add(new CColumnPressDevice			(200, "Press",					this));
		}
		((CPrintSheetTableView*)m_pParent)->m_printSheetListToolBarCtrl.SetState(ID_PRINTSHEET_LIST, TBSTATE_ENABLED | TBSTATE_CHECKED);
		((CPrintSheetTableView*)m_pParent)->m_printSheetListToolBarCtrl.SetState(ID_PRINTCOLOR_INFO, TBSTATE_ENABLED);
		((CPrintSheetTableView*)m_pParent)->m_printSheetListToolBarCtrl.SetState(ID_PDF_OUTPUT,		TBSTATE_ENABLED);
		break;

	case ShowPrintColorInfo:
//		m_HeaderCtrl.ModifyStyle(0, HDS_BUTTONS);
		m_HeaderCtrl.ModifyStyle(HDS_DRAGDROP, 0);
		if (theApp.settings.m_iLanguage == 0)
			pColumns.Add(new CColumnPDFSheet			(150, "Bogen",					this));
		else
			pColumns.Add(new CColumnPDFSheet			(150, "Sheet",					this));

		if (pDoc)
		{
			pDoc->m_ColorDefinitionTable.UpdateColorInfos();
			for (int i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
			{
				CString strMappedColor = theApp.MapPrintColor(pDoc->m_ColorDefinitionTable[i].m_strColorName);
				if (strMappedColor == pDoc->m_ColorDefinitionTable[i].m_strColorName)	// Basic colors first
				{
					if (pDoc->m_PageTemplateList.ColorInUse(pDoc, pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strMappedColor)) )
					{
						int nWidth = min(pDoc->m_ColorDefinitionTable[i].m_strColorName.GetLength() * 8, 250);
						nWidth	   = max(nWidth, 80);
						pColumns.Add(new CColumnPrintColorInfo	(nWidth, pDoc->m_ColorDefinitionTable[i].m_strColorName, this, i));
					}
				}
			}
			pColumns.Add(new CColumnPrintColorInfo	(250, "", this, -2));
		}

		((CPrintSheetTableView*)m_pParent)->m_printColorInfoToolBarCtrl.SetState(ID_PRINTSHEET_LIST, TBSTATE_ENABLED);
		((CPrintSheetTableView*)m_pParent)->m_printColorInfoToolBarCtrl.SetState(ID_PRINTCOLOR_INFO, TBSTATE_ENABLED | TBSTATE_CHECKED);
		((CPrintSheetTableView*)m_pParent)->m_printColorInfoToolBarCtrl.SetState(ID_PDF_OUTPUT,		TBSTATE_ENABLED);
		break;

	case ShowOutputList:
		m_HeaderCtrl.ModifyStyle(0, HDS_DRAGDROP);
		if (theApp.settings.m_iLanguage == 0)
		{
			pColumns.Add(new CColumnPDFSheet			(150, "Bogen",					this));
			pColumns.Add(new CColumnPDFFilename			(180, "Datei",					this));
			pColumns.Add(new CColumnOutputStatus		(100, "Letzte Ausgabe",			this));
			pColumns.Add(new CColumnPDFTarget			(200, "Ausgabeziel",			this));
			pColumns.Add(new CColumnPDFLocation			(300, "Ort",					this));
		}
		else
		{
			pColumns.Add(new CColumnPDFSheet			(150, "Sheet",					this));
			pColumns.Add(new CColumnPDFFilename			(180, "File",					this));
			pColumns.Add(new CColumnOutputStatus		(100, "Last Output",			this));
			pColumns.Add(new CColumnPDFTarget			(200, "Target",					this));
			pColumns.Add(new CColumnPDFLocation			(300, "Location",				this));
		}
		((CPrintSheetTableView*)m_pParent)->m_outputListToolBarCtrl.SetState(ID_PRINTSHEET_LIST, TBSTATE_ENABLED);
		((CPrintSheetTableView*)m_pParent)->m_outputListToolBarCtrl.SetState(ID_PRINTCOLOR_INFO, TBSTATE_ENABLED);
		((CPrintSheetTableView*)m_pParent)->m_outputListToolBarCtrl.SetState(ID_PDF_OUTPUT,		TBSTATE_ENABLED | TBSTATE_CHECKED);
		break;
	}

	for (int i = 0; i < pColumns.GetSize(); i++)
	{
		CColumnRef column(i, pColumns[i]);
		m_ColumnRefList.Add(column);
	}

	HD_ITEM hdi;
	hdi.mask = HDI_TEXT | HDI_FORMAT | HDI_WIDTH;

	while (m_HeaderCtrl.GetItemCount() > 0)
		m_HeaderCtrl.DeleteItem(0);

	m_nHeaderWidth = 0;
	for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
	{
		hdi.pszText		= m_ColumnRefList[i].m_pColumn->m_strColumnLabel.GetBuffer(0);
		hdi.cxy			= m_ColumnRefList[i].m_pColumn->m_nColumnWidth;
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		hdi.fmt			= ( (nWhatToShow == ShowPrintColorInfo) && (i != 0) ) ? HDF_LEFT | HDF_STRING | HDF_OWNERDRAW : HDF_LEFT | HDF_STRING;
		m_HeaderCtrl.InsertItem(i, &hdi);

		m_ColumnRefList[i].m_pColumn->m_nColumnLeft = m_nHeaderWidth;
		m_nHeaderWidth += m_ColumnRefList[i].m_pColumn->m_nColumnWidth;

		m_ColumnRefList[i].m_pColumn->m_pDisplayList = &((CPrintSheetTableView*)m_pParent)->m_DisplayList;
	}

	CClientDC dc(m_pParent);
	TEXTMETRIC tm;
	dc.GetTextMetrics(&tm); 
	m_nLineHeight = tm.tmHeight + 4;//CPrintSheetTableColumn::columnMargin;

	if (pDoc)
		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();

	switch (nWhatToShow)
	{
	case ShowPrintSheetList:
		((CPrintSheetTableView*)m_pParent)->m_printSheetListToolBarCtrl.ShowWindow(SW_SHOW);
		((CPrintSheetTableView*)m_pParent)->m_printColorInfoToolBarCtrl.ShowWindow(SW_HIDE);
		((CPrintSheetTableView*)m_pParent)->m_outputListToolBarCtrl.ShowWindow(SW_HIDE);
		break;
	case ShowPrintColorInfo:
		((CPrintSheetTableView*)m_pParent)->m_printSheetListToolBarCtrl.ShowWindow(SW_HIDE);
		((CPrintSheetTableView*)m_pParent)->m_printColorInfoToolBarCtrl.ShowWindow(SW_HIDE);//SW_SHOW);
		((CPrintSheetTableView*)m_pParent)->m_outputListToolBarCtrl.ShowWindow(SW_HIDE);
		break;
	case ShowOutputList:
		((CPrintSheetTableView*)m_pParent)->m_printSheetListToolBarCtrl.ShowWindow(SW_HIDE);
		((CPrintSheetTableView*)m_pParent)->m_printColorInfoToolBarCtrl.ShowWindow(SW_HIDE);
		((CPrintSheetTableView*)m_pParent)->m_outputListToolBarCtrl.ShowWindow(SW_SHOW);
		((CPrintSheetTableView*)m_pParent)->m_bStartOutputChecked   = FALSE;
		((CPrintSheetTableView*)m_pParent)->m_bOutputProfileChecked = FALSE;
		break;
	}

	((CPrintSheetTableView*)m_pParent)->OnUpdate(NULL, 0, 0);
	((CPrintSheetTableView*)m_pParent)->UpdateWindow();
}

int CPrintSheetTable::GetCellLines(CPrintSheet& rPrintSheet)
{
	if ( ! rPrintSheet.GetLayout())
		return 0;

	switch (m_nWhatToShow)
	{
	case CPrintSheetTable::ShowPrintSheetList:	if ( ! rPrintSheet.m_FoldSheetTypeList.GetSize())
													return 1;
												else
													return rPrintSheet.m_FoldSheetTypeList.GetSize();		

	case CPrintSheetTable::ShowPrintColorInfo: 
	case CPrintSheetTable::ShowOutputList:  
		{
			int nLines = 1;
			if (rPrintSheet.m_bDisplayOpen)
			{
				nLines++;

				if (m_nWhatToShow == CPrintSheetTable::ShowOutputList)
				{
					if (rPrintSheet.m_FrontSidePlates.GetCount())
					{
						if ( (rPrintSheet.m_FrontSidePlates.GetHead().m_strPlateName != "Composite") || (rPrintSheet.m_FrontSidePlates.GetCount() > 1) )
							if (rPrintSheet.m_FrontSidePlates.m_bDisplayOpen)
								nLines += rPrintSheet.m_FrontSidePlates.GetCount();	// front colors 
					}

					if ( ! rPrintSheet.GetLayout()->m_nProductType)	// if product type, we have only one side
					{
						nLines++;
						if (rPrintSheet.m_BackSidePlates.GetCount())
						{
							if ( (rPrintSheet.m_BackSidePlates.GetHead().m_strPlateName  != "Composite") || (rPrintSheet.m_BackSidePlates.GetCount() > 1) )
								if (rPrintSheet.m_BackSidePlates.m_bDisplayOpen)
									nLines += rPrintSheet.m_BackSidePlates.GetCount();	// back colors 
						}
					}
				}
				else
				{
					if ( (rPrintSheet.GetLayout()->m_FrontSide.m_ObjectList.m_bDisplayOpen))
						nLines += rPrintSheet.GetLayout()->m_FrontSide.m_ObjectList.GetSheetObjectRefList(&rPrintSheet);
					if ( ! rPrintSheet.GetLayout()->m_nProductType)	// if product type, we have only one side
					{
						nLines++;
						if (rPrintSheet.GetLayout()->m_BackSide.m_ObjectList.m_bDisplayOpen)
							nLines += rPrintSheet.GetLayout()->m_BackSide.m_ObjectList.GetSheetObjectRefList(&rPrintSheet);
					}
				}
			}
			return nLines;
		}
		break;
	}

	return 0;
}

void CPrintSheetTable::CalcTableLenght()
{
	m_nTableLenght = 0;

	CImpManDoc* pDoc  = CImpManDoc::GetDoc();
	if (!pDoc)
		return;

	int		 nLines = 0;
	POSITION pos    = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		nLines += GetCellLines(rPrintSheet);
	}

	m_nTableLenght = nLines * m_nLineHeight;
}

void CPrintSheetTable::SaveBackground(CDC* pDC, const CRect& rect)
{
	if (m_pBkBitmap)
	{
		m_pBkBitmap->DeleteObject();
		delete m_pBkBitmap;
	}
	m_pBkBitmap = new CBitmap;

 	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	m_pBkBitmap->CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBkBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void CPrintSheetTable::RestoreBackground(CDC* pDC, const CRect& rect)
{
	if (!m_pBkBitmap)
		return;

	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBkBitmap);
	pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();

	m_pBkBitmap->DeleteObject();
	delete m_pBkBitmap;
	m_pBkBitmap = NULL;

	m_pParent->ValidateRect(rect);
}

void CPrintSheetTable::ResizeColumns(int nItem, int nNewWidth)
{
	int i;
	for (i = 0; i < m_ColumnRefList.GetSize(); i++)
		if (m_ColumnRefList[i].m_iItem == nItem)
			break;

	if (i == m_ColumnRefList.GetSize()) // item not found
		return;

	m_ColumnRefList[i].m_pColumn->m_nColumnWidth = nNewWidth;

	m_nHeaderWidth = 0;
	for (i = 0; i < m_ColumnRefList.GetSize(); i++)
	{
		m_ColumnRefList[i].m_pColumn->m_nColumnLeft = m_nHeaderWidth;
		m_nHeaderWidth += m_ColumnRefList[i].m_pColumn->m_nColumnWidth;
	}
}

BOOL CPrintSheetTable::ReorderColumns(int nItem, int nNewOrder)
{
	int nOldOrder;
	for (nOldOrder = 0; nOldOrder < m_ColumnRefList.GetSize(); nOldOrder++)
		if (m_ColumnRefList[nOldOrder].m_iItem == nItem)
			break;

	if (nOldOrder == m_ColumnRefList.GetSize()) // item not found
		return FALSE;

	if (nOldOrder != nNewOrder)
	{
		CColumnRef colRef = m_ColumnRefList[nOldOrder];	// keep old order info

		m_ColumnRefList.RemoveAt(nOldOrder);			// remove item at old position
		m_ColumnRefList.InsertAt(nNewOrder, colRef);	// insert item at new position

		m_nHeaderWidth = 0;
		for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
		{
			m_ColumnRefList[i].m_pColumn->m_nColumnLeft = m_nHeaderWidth;
			m_nHeaderWidth += m_ColumnRefList[i].m_pColumn->m_nColumnWidth;
		}

		return TRUE;
	}
	else
		return FALSE;
}

void CPrintSheetTable::OnDraw(CDC* pDC)
{
	CPen   grayPen(PS_SOLID, 1, TABLECOLOR_SEPARATION_LINE);
	CFont* pOldFont	  = pDC->SelectObject(&m_HeaderFont);
	int	   nOldBkMode = pDC->SetBkMode(TRANSPARENT);
	CPen*  pOldPen	  = pDC->SelectObject(&grayPen);

	CRect	clientRect;
	CSize	totalSize = ((CScrollView*)m_pParent)->GetTotalSize();
	CPoint	scrollPos(m_pParent->GetScrollPos(SB_HORZ), m_pParent->GetScrollPos(SB_VERT));
	m_pParent->GetClientRect(clientRect);

	CPoint ptViewPortOrg = CSize(CPoint(0, m_headerFrameRect.bottom) - scrollPos);
	pDC->SetViewportOrg(ptViewPortOrg);

CRgn clipRgn;
clipRgn.CreateRectRgnIndirect(CRect(clientRect.left, clientRect.top + m_headerFrameRect.bottom /*+ scrollPos.y*/, clientRect.right, clientRect.bottom));
pDC->SelectClipRgn(&clipRgn);
clipRgn.DeleteObject();

	CRect bkRect(0, 0, clientRect.Width() - ptViewPortOrg.x, max(totalSize.cy, clientRect.Height()));
	pDC->FillSolidRect(bkRect, TABLECOLOR_BACKGROUND);	// Background

	CImpManDoc* pDoc  = CImpManDoc::GetDoc();
	if (!pDoc)
		return;

	int		 nLine = 0, nIndex = 0, nCellLines = 0;
	POSITION pos   = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);

		if (rPrintSheet.IsEmpty())
			continue;

		rPrintSheet.InitializeFoldSheetTypeList();

		nCellLines = GetCellLines(rPrintSheet);
		if (nIndex % 2)
		{
			bkRect = CRect(CPoint(0, nLine * m_nLineHeight), CSize(clientRect.Width() - ptViewPortOrg.x, nCellLines * m_nLineHeight));
			pDC->FillSolidRect(bkRect, TABLECOLOR_BACKGROUND_DARK);
			pDC->MoveTo(bkRect.left,  bkRect.top);
			pDC->LineTo(bkRect.right, bkRect.top);
			pDC->MoveTo(bkRect.left,  bkRect.bottom-1);
			pDC->LineTo(bkRect.right, bkRect.bottom-1);
		}

		nLine += nCellLines;
		nIndex++;
	}
	// fill up with empty lines
	while (nLine * m_nLineHeight < max(totalSize.cy, clientRect.Height()))
	{
		if (nIndex % 2)
		{ 
			bkRect = CRect(CPoint(0, nLine * m_nLineHeight), CSize(clientRect.Width() - ptViewPortOrg.x, m_nLineHeight));
			pDC->FillSolidRect(bkRect, TABLECOLOR_BACKGROUND_DARK);
			pDC->MoveTo(bkRect.left,  bkRect.top);
			pDC->LineTo(bkRect.right, bkRect.top);
			pDC->MoveTo(bkRect.left,  bkRect.bottom-1);
			pDC->LineTo(bkRect.right, bkRect.bottom-1);
		}
		nLine++;
		nIndex++;
	}

	// draw column separation lines
	for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
	{
		pDC->MoveTo(m_ColumnRefList[i].m_pColumn->m_nColumnLeft + m_ColumnRefList[i].m_pColumn->m_nColumnWidth - 2, 0);
		pDC->LineTo(m_ColumnRefList[i].m_pColumn->m_nColumnLeft + m_ColumnRefList[i].m_pColumn->m_nColumnWidth - 2, max(totalSize.cy, clientRect.Height()));
	}

	for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
	{
		ptViewPortOrg = CPoint(m_ColumnRefList[i].m_pColumn->m_nColumnLeft + CPrintSheetTableColumn::columnMargin, 
							   m_headerFrameRect.bottom) - scrollPos;

		m_ColumnRefList[i].m_pColumn->m_columnClipRect = CRect(ptViewPortOrg, 
															   CSize(m_ColumnRefList[i].m_pColumn->m_nColumnWidth - 2 * CPrintSheetTableColumn::columnMargin, 
															   totalSize.cy - ptViewPortOrg.y));
		pDC->SetViewportOrg(ptViewPortOrg);

		CRgn clipRgn;
		CRect clipRect = m_ColumnRefList[i].m_pColumn->m_columnClipRect;
		clipRect.top += m_pParent->GetScrollPos(SB_VERT);
		clipRgn.CreateRectRgnIndirect(clipRect);
		pDC->SelectClipRgn(&clipRgn);
		clipRgn.DeleteObject();

		m_ColumnRefList[i].m_pColumn->OnDraw(pDC);
	}

	pDC->SetViewportOrg(CPoint(0, m_headerFrameRect.bottom));

	pDC->SelectClipRgn(NULL);
	pDC->SetBkMode(nOldBkMode);


	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pOldPen);

	grayPen.DeleteObject();
}



/////////////////////////////////////////////////////////////////////////////
// Drawing routines for the specific columns

void CColumnPrintSheetNumber::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	HICON	 hIcon	= theApp.LoadIcon(IDI_PRINTSHEET);
	int		 nLine  = 0;
	POSITION pos    = pDoc->m_PrintSheetList.GetHeadPosition();
	CString	 strOut;
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		pDC->DrawIcon(CPoint(0, nLine * m_pParent->m_nLineHeight + 2), hIcon);
		for (int i = 0; i < GetCellLines(rPrintSheet); i++)
		{
			if (i == 0)
			{
				strOut.Format(_T("%s"), rPrintSheet.GetNumber());
				CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 25);
				pDC->TextOut(25, GETTEXTPOS_VERT(nLine), strOut);
				CPrintSheetTableView* pView = (CPrintSheetTableView*)m_pParent->m_pParent;
				if (pView)
					if ( pView->m_pDlgFoldSheetNumeration)
						if ( pView->m_pDlgFoldSheetNumeration->m_hWnd)
						{
							int nXPos = 25 + pDC->GetTextExtent(strOut).cx;
							strOut.Format(_T("  (%d)"), rPrintSheet.m_nPrintSheetNumber);
							COLORREF crText = pDC->SetTextColor(MIDDLEGRAY);
							pDC->TextOut(nXPos, GETTEXTPOS_VERT(nLine), strOut);
							pDC->SetTextColor(crText);
						}

				CRect regRect(CPoint(0, nLine * m_pParent->m_nLineHeight), 
							  CSize(m_columnClipRect.Width(), m_pParent->m_nLineHeight));
				CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet,
								   			 DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, SheetNumber), (void*)&rPrintSheet, CDisplayItem::ForceRegister);
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
					if (pDI->m_nState == CDisplayItem::Selected)
					{
						pDC->SaveDC();
						pDC->SelectClipRgn(NULL);
						CRect rcFeedback = pDI->m_BoundingRect;
						rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
						Graphics graphics(pDC->m_hDC);
						Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
						rc.Inflate(-1, -1);
						COLORREF crHighlightColor = RGB(165,175,200);
						SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
						graphics.FillRectangle(&br, rc);
						graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
						pDC->RestoreDC(-1);
					}
				}
			}

			nLine++;
		}
	}
}

void CColumnPrintSheetLayout::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	CString  strOut;
	int		 nLine = 0;
	POSITION pos   = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		for (int i = 0; i < GetCellLines(rPrintSheet); i++)
		{
			if (i == 0)
			{
				strOut = rPrintSheet.GetLayout()->m_strLayoutName;
				CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
				pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);

				if (m_pParent->m_nWhatToShow == CPrintSheetTable::ShowPrintSheetList)
				{
					CRect regRect(CPoint(-CPrintSheetTableColumn::columnMargin, nLine * m_pParent->m_nLineHeight), 
										  CSize(m_columnClipRect.Width() + CPrintSheetTableColumn::columnMargin/2, m_pParent->m_nLineHeight));
					CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)rPrintSheet.GetLayout(),
									   			 DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, Layout), (void*)&rPrintSheet, CDisplayItem::ForceRegister);
					if (pDI)
					{
						pDI->SetMouseOverFeedback();
						pDI->SetNoStateFeedback();
						if (pDI->m_nState == CDisplayItem::Selected)
						{
							pDC->SaveDC();
							pDC->SelectClipRgn(NULL);
							CRect rcFeedback = pDI->m_BoundingRect;
							rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
							Graphics graphics(pDC->m_hDC);
							Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
							rc.Inflate(-1, -1);
							COLORREF crHighlightColor = RGB(165,175,200);
							SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
							graphics.FillRectangle(&br, rc);
							graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
							pDC->RestoreDC(-1);
						}
					}
				}
			}

			nLine++;
		}
	}
}

void CColumnLayoutNotes::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	int		 nLine = 0;
	POSITION pos   = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		for (int i = 0; i < GetCellLines(rPrintSheet); i++)
		{
			if (i == 0)
			{
				CRect textRect(CPoint(0, nLine * m_pParent->m_nLineHeight), CSize(16, 16));

				HICON hIcon;
				if ( ! rPrintSheet.GetLayout()->m_strNotes.IsEmpty())
					hIcon = theApp.LoadIcon(IDI_NOTES);
				else
					hIcon = theApp.LoadIcon(IDI_NO_NOTES);

				pDC->DrawIcon(textRect.TopLeft() + CSize(0, -2), hIcon);

				CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, textRect, textRect, (void*)rPrintSheet.GetLayout(), DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, LayoutNotes), (void*)&rPrintSheet, CDisplayItem::ForceRegister);
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
					if (pDI->m_nState == CDisplayItem::Selected)
					{
						pDC->SaveDC();
						pDC->SelectClipRgn(NULL);
						CRect rcFeedback = pDI->m_BoundingRect;
						rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
						Graphics graphics(pDC->m_hDC);
						Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
						rc.Inflate(-1, -1);
						COLORREF crHighlightColor = RGB(165,175,200);
						SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
						graphics.FillRectangle(&br, rc);
						graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
						pDC->RestoreDC(-1);
					}
				}
			}

			nLine++;
		}
	}
}

void CColumnFoldSheetPages::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	CPoint ptViewPortOrg = pDC->GetViewportOrg();
	CRect  clientRect;
	m_pParent->m_pParent->GetClientRect(clientRect);

	int		 nLine  = 0;
	POSITION pos    = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		CRect rectPrintSheet(-ptViewPortOrg.x, nLine * m_pParent->m_nLineHeight, 
							 -ptViewPortOrg.x + clientRect.Width(), (nLine + rPrintSheet.m_FoldSheetTypeList.GetSize()) * m_pParent->m_nLineHeight);
		int i;
		for (i = 0; i < rPrintSheet.m_FoldSheetTypeList.GetSize(); i++)
		{
			CFoldSheet* pFoldSheet = rPrintSheet.m_FoldSheetTypeList[i].m_pFoldSheet;
			if (pFoldSheet)
				pFoldSheet->DrawPageNumRange(pDC, 0, GETTEXTPOS_VERT(nLine), GETTEXTPOS_VERT(nLine + 1), m_columnClipRect.Width());
			nLine++;
		}

		nLine += GetCellLines(rPrintSheet) - i;
	}
}

void CColumnFoldSheetNumPages::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	CString  strOut;
	int		 nLine = 0;
	POSITION pos   = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		for (int i = 0; i < rPrintSheet.m_FoldSheetTypeList.GetSize(); i++)
		{
			CFoldSheetLayer* pFoldSheetLayer = rPrintSheet.GetFoldSheetLayer(rPrintSheet.LayerTypeToLayerRef(i));

			if (pFoldSheetLayer)
			{
				strOut.Format(_T("%d"), 2 * pFoldSheetLayer->m_nPagesX * pFoldSheetLayer->m_nPagesY);
				CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
				pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
			}

			nLine++;
		}
	}
}

void CColumnFoldSheet::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPoint ptViewPortOrg = pDC->GetViewportOrg();
	CRect  clientRect;
	m_pParent->m_pParent->GetClientRect(clientRect);

	HICON	 hIcon	= theApp.LoadIcon(IDI_FOLDSHEET);
	CString  strOut;
	int		 nWidth;
	int		 nLine = 0;
	int		 nMaxFoldSheetNumLen = min(100, CPrintSheetTableView::GetMaxFoldSheetNumberLen(pDC, TRUE));
	POSITION pos   = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		CRect rectPrintSheet(-ptViewPortOrg.x, nLine * m_pParent->m_nLineHeight, 
							 -ptViewPortOrg.x + clientRect.Width(), (nLine + rPrintSheet.m_FoldSheetTypeList.GetSize()) * m_pParent->m_nLineHeight);
		for (int i = 0; i < rPrintSheet.m_FoldSheetTypeList.GetSize(); i++)
		{
			CFoldSheet* pFoldSheet = rPrintSheet.m_FoldSheetTypeList[i].m_pFoldSheet;
			if (pFoldSheet)
			{
				pDC->DrawIcon(CPoint(0, nLine * m_pParent->m_nLineHeight + 2), hIcon);
				if (pFoldSheet->m_LayerList.GetCount() > 1)
					strOut.Format(_T("%s (%d)"), pFoldSheet->GetNumber(), rPrintSheet.m_FoldSheetTypeList[i].m_nFoldSheetLayerIndex + 1);
				else
					strOut.Format(_T("%s"), pFoldSheet->GetNumber());
				nWidth = min(nMaxFoldSheetNumLen, m_columnClipRect.Width());

				CPageListTableView::FitStringToWidth(pDC, strOut, nWidth);
				pDC->DrawText(strOut, CRect(18, GETTEXTPOS_VERT(nLine), 18 + nWidth, GETTEXTPOS_VERT(nLine + 1)), DT_VCENTER|DT_LEFT);

				int nTextPosHorz = 18 + nWidth;
				nWidth = m_columnClipRect.Width() - nTextPosHorz;
				strOut.Format(_T("[%s]"), pFoldSheet->m_strFoldSheetTypeName);
				CPageListTableView::FitStringToWidth(pDC, strOut, nWidth);
				pDC->TextOut(nTextPosHorz, GETTEXTPOS_VERT(nLine), strOut);

				CRect rectFoldSheet(CPoint(-CPrintSheetTableColumn::columnMargin, nLine * m_pParent->m_nLineHeight), 
											CSize(m_columnClipRect.Width() + CPrintSheetTableColumn::columnMargin/2, m_pParent->m_nLineHeight));
				CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, rectFoldSheet, rectPrintSheet, (void*)(int)i, 
							   									 DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, FoldSheetType), (void*)&rPrintSheet, CDisplayItem::ForceRegister);
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
					if (pDI->m_nState == CDisplayItem::Selected)
					{
						pDC->SaveDC();
						pDC->SelectClipRgn(NULL);
						CRect rcFeedback = pDI->m_BoundingRect;
						rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
						Graphics graphics(pDC->m_hDC);
						Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
						rc.Inflate(-1, -1);
						COLORREF crHighlightColor = RGB(165,175,200);
						SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
						graphics.FillRectangle(&br, rc);
						graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
						pDC->RestoreDC(-1);
					}
				}
			}
			nLine++;
		}
	}
}

void CColumnFoldSheetsNUp::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	CString  strOut;
	int		 nLine = 0;
	POSITION pos   = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		for (int i = 0; i < rPrintSheet.m_FoldSheetTypeList.GetSize(); i++)
		{
			strOut.Format(_T("%d"), rPrintSheet.m_FoldSheetTypeList[i].m_nNumUp);
			CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
			pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);

			nLine++;
		}
	}
}

void CColumnPressDevice::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	CString  strOut;
	int		 nLine = 0;
	POSITION pos   = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet&  rPrintSheet  = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		CLayout*	  pLayout	   = rPrintSheet.GetLayout();
		if (!pLayout)
			continue;
		CPressDevice* pFrontPressDevice = pLayout->m_FrontSide.GetPressDevice();
		CPressDevice* pBackPressDevice  = pLayout->m_BackSide.GetPressDevice();

		if (pFrontPressDevice)
			strOut = pFrontPressDevice->m_strName;
		if (pBackPressDevice)
			if (pFrontPressDevice != pBackPressDevice)
				strOut += _T(" / ") + pBackPressDevice->m_strName;
		CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
		pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);

		nLine+= rPrintSheet.m_FoldSheetTypeList.GetSize();
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////
// columns for output table view

void CColumnPDFSheet::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	HICON	 hIconPDF		  = theApp.LoadIcon(IDI_OUT_SHEET);
	HICON	 hIconFront		  = theApp.LoadIcon(IDI_OUT_SHEET_FRONT);
	HICON	 hIconBack		  = theApp.LoadIcon(IDI_OUT_SHEET_BACK);
	HICON	 hIconPage		  = theApp.LoadIcon(IDI_SHEET_PAGE);
	HICON	 hIconCustomMark  = theApp.LoadIcon(IDI_CUSTOM_MARK);
	HICON	 hIconCropMark	  = theApp.LoadIcon(IDI_CROP_MARK);
	HICON	 hIconFoldMark	  = theApp.LoadIcon(IDI_FOLD_MARK);
	HICON	 hIconBarcodeMark = theApp.LoadIcon(IDI_BARCODE_MARK);
	HICON	 hIconDrawingMark = theApp.LoadIcon(IDI_DRAWING_MARK);
	HICON	 hIconSectionMark = theApp.LoadIcon(IDI_SECTION_MARK);
	HICON	 hIconTextMark	  = theApp.LoadIcon(IDI_TEXT_MARK);
	HICON	 hIconPlus		  = theApp.LoadIcon(IDI_PLUS);
	HICON	 hIconMinus		  = theApp.LoadIcon(IDI_MINUS);
	int		 nLine			  = 0;
	POSITION pos			  = pDoc->m_PrintSheetList.GetHeadPosition();
	CString	 strOut;
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		CLayout*	 pLayout	 = rPrintSheet.GetLayout();
		if ( ! pLayout)
			continue;

		pDC->DrawIcon(CPoint(0, nLine * m_pParent->m_nLineHeight + 5), (rPrintSheet.m_bDisplayOpen) ? hIconMinus : hIconPlus);

		pDC->DrawIcon(CPoint(12, nLine * m_pParent->m_nLineHeight + 2), hIconPDF);

		strOut.Format(_T("%s (%s)"), rPrintSheet.GetNumber(), pLayout->m_strLayoutName);
		CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 40);
		pDC->TextOut(40, GETTEXTPOS_VERT(nLine), strOut);

		CSize textExtent = pDC->GetTextExtent(strOut);
		CRect regRect(CPoint(0, nLine * m_pParent->m_nLineHeight), CSize(40 + textExtent.cx, 20));
		CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, PDFSheet), NULL, CDisplayItem::ForceRegister);
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				pDC->SaveDC();
				pDC->SelectClipRgn(NULL);
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				Graphics graphics(pDC->m_hDC);
				Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
				rc.Inflate(-1, -1);
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
				pDC->RestoreDC(-1);
			}
		}

		int	nCount = 0;
		if (rPrintSheet.m_bDisplayOpen)
		{
			int nSide = -1;
			do
			{
				nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;

				nLine++;

				pDC->DrawIcon(CPoint(30, nLine * m_pParent->m_nLineHeight + 2), (nSide == FRONTSIDE) ? hIconFront : hIconBack);

				switch(pLayout->m_nProductType)
				{
				case WORK_AND_BACK:		strOut = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_strSideName : pLayout->m_BackSide.m_strSideName; break;
				case WORK_AND_TURN  :	strOut = theApp.settings.m_szWorkAndTurnName;		break;
				case WORK_AND_TUMBLE:	strOut = theApp.settings.m_szWorkAndTumbleName;		break;
				case WORK_SINGLE_SIDE:	strOut = theApp.settings.m_szSingleSidedName;		break;
				default:				strOut = "";										break;
				}

				CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 50);
				pDC->TextOut(50, GETTEXTPOS_VERT(nLine), strOut);

				textExtent = pDC->GetTextExtent(strOut);
				CRect regRect(CPoint(17, nLine * m_pParent->m_nLineHeight), CSize(17 + 20 + textExtent.cx, 15));
				CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, PDFSheetSide), (void*)nSide, CDisplayItem::ForceRegister);
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
					if (pDI->m_nState == CDisplayItem::Selected)
					{
						pDC->SaveDC();
						pDC->SelectClipRgn(NULL);
						CRect rcFeedback = pDI->m_BoundingRect;
						rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
						Graphics graphics(pDC->m_hDC);
						Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
						rc.Inflate(-1, -1);
						COLORREF crHighlightColor = RGB(165,175,200);
						SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
						graphics.FillRectangle(&br, rc);
						graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
						pDC->RestoreDC(-1);
					}
				}

				nCount++;
				if (m_pParent->m_nWhatToShow == CPrintSheetTable::ShowOutputList)
				{
					CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;
					if (rPlateList.GetCount())
					{
						if ( (rPlateList.GetHead().m_strPlateName != "Composite") || (rPlateList.GetCount() > 1) )
						{
							pDC->DrawIcon(CPoint(17, nLine * m_pParent->m_nLineHeight + 5), (rPlateList.m_bDisplayOpen) ? hIconMinus : hIconPlus);

							if (rPlateList.m_bDisplayOpen)
							{
								POSITION pos = rPlateList.GetHeadPosition();
								while (pos)
								{
									CPlate& rPlate = rPlateList.GetNext(pos);

									nLine++;
								
									CRect colorRect(CPoint(30, nLine * m_pParent->m_nLineHeight + 5), CSize(10, 10));
									if (pDC->RectVisible(colorRect))
									{
										CColorDefinition& rColorDef	= pDoc->m_ColorDefinitionTable[rPlate.m_nColorDefinitionIndex];
										CBrush  colorBrush(rColorDef.m_rgb);
										CPen	grayPen(PS_SOLID, 1, DARKGRAY);
										CBrush* pOldBrush = pDC->SelectObject(&colorBrush);
										CPen*	pOldPen	  = pDC->SelectObject(&grayPen);
										pDC->Rectangle(colorRect);
										pDC->SelectObject(pOldBrush);
										pDC->SelectObject(pOldPen);
										colorBrush.DeleteObject();
									}

									strOut = rPlate.m_strPlateName;
									CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 43);
									pDC->TextOut(43, GETTEXTPOS_VERT(nLine), strOut);

									textExtent = pDC->GetTextExtent(strOut);
									CRect regRect(CPoint(30, nLine * m_pParent->m_nLineHeight), CSize(18 + textExtent.cx, 15));
									CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet,
								   													 DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, PDFSheetPlate), (void*)&rPlate, CDisplayItem::ForceRegister);
									if (pDI)
									{
										pDI->SetMouseOverFeedback();
										pDI->SetNoStateFeedback();
										if (pDI->m_nState == CDisplayItem::Selected)
										{
											pDC->SaveDC();
											pDC->SelectClipRgn(NULL);
											CRect rcFeedback = pDI->m_BoundingRect;
											rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
											Graphics graphics(pDC->m_hDC);
											Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
											rc.Inflate(-1, -1);
											COLORREF crHighlightColor = RGB(165,175,200);
											SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
											graphics.FillRectangle(&br, rc);
											graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
											pDC->RestoreDC(-1);
										}
									}

									nCount++;
								}
							}
						}
					}
				}
				else
				{
					CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;
					CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
					rObjectList.GetSheetObjectRefList(&rPrintSheet, &objectRefList);

					if (objectRefList.GetSize())
					{
						pDC->DrawIcon(CPoint(17, nLine * m_pParent->m_nLineHeight + 5), (rObjectList.m_bDisplayOpen) ? hIconMinus : hIconPlus);

						if ( (rObjectList.m_bDisplayOpen))
						{
							for (int i = 0; i < objectRefList.GetSize(); i++)
							{
								CLayoutObject* pObject		   = objectRefList[i].m_pLayoutObject;
								CPageTemplate* pObjectTemplate = objectRefList[i].m_pObjectTemplate;
								if ( ! pObject || ! pObjectTemplate)
									continue;

								nLine++;
							
								if (objectRefList[i].m_nObjectType == CLayoutObject::Page)
									pDC->DrawIcon(CPoint(40, nLine * m_pParent->m_nLineHeight + 3), hIconPage);
								else
								{
									HICON hIcon = hIconCustomMark;
									switch (objectRefList[i].m_nMarkType)
									{
									case CMark::CustomMark:		hIcon = hIconCustomMark;	break;
									case CMark::TextMark:		hIcon = hIconTextMark;		break;
									case CMark::SectionMark:	hIcon = hIconSectionMark;	break;
									case CMark::CropMark:		hIcon = hIconCropMark;		break;
									case CMark::FoldMark:		hIcon = hIconFoldMark;		break;
									case CMark::BarcodeMark:	hIcon = hIconBarcodeMark;	break;
									case CMark::DrawingMark:	hIcon = hIconDrawingMark;	break;
									}
									pDC->DrawIcon(CPoint(38, nLine * m_pParent->m_nLineHeight + 2), hIcon);
								}

								strOut = pObjectTemplate->m_strPageID;
								CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 43);
								pDC->TextOut(55, GETTEXTPOS_VERT(nLine), strOut);

								textExtent = pDC->GetTextExtent(strOut);
								CRect regRect(CPoint(35, nLine * m_pParent->m_nLineHeight), CSize(25 + textExtent.cx, 15));
								regRect.right = min(regRect.right, m_columnClipRect.Width());
								CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)pObject,
								 												 DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, PDFSheetObject), (void*)&rPrintSheet, CDisplayItem::ForceRegister);
								if (pDI)
								{
									pDI->SetMouseOverFeedback();
									pDI->SetNoStateFeedback();
									if (pDI->m_nState == CDisplayItem::Selected)
									{
										pDC->SaveDC();
										pDC->SelectClipRgn(NULL);
										CRect rcFeedback = pDI->m_BoundingRect;
										rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
										Graphics graphics(pDC->m_hDC);
										Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
										rc.Inflate(-1, -1);
										COLORREF crHighlightColor = RGB(165,175,200);
										SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
										graphics.FillRectangle(&br, rc);
										graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
										pDC->RestoreDC(-1);
									}
								}

								nCount++;
							}
						}
					}
				}

				if (pLayout->m_nProductType)
					nSide = BACKSIDE;	// skip backside
			}
			while (nSide != BACKSIDE);
		}
	
		nLine += GetCellLines(rPrintSheet) - nCount;
	}
}


void CColumnPDFFilename::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

#ifdef PDF
	HICON	 hIconPDFOutFile		= theApp.LoadIcon(IDI_PDF_FILE);
	HICON	 hIconPDFOutFileDimmed	= theApp.LoadIcon(IDI_PDF_FILE_DIMMED);
	HICON	 hIconJDFOutFile		= theApp.LoadIcon(IDI_JDF);
	HICON	 hIconJDFOutFileDimmed	= theApp.LoadIcon(IDI_JDF_DIMMED);
	HICON	 hIconOutFile			= hIconPDFOutFile;
	HICON	 hIconOutFileDimmed		= hIconPDFOutFileDimmed;
#else
	HICON	 hIconOutFile			= theApp.LoadIcon(IDI_IPU_FILE);
	HICON	 hIconOutFileDimmed		= theApp.LoadIcon(IDI_IPU_FILE_DIMMED);
#endif
	int		 nLine				= 0;
	POSITION pos				= pDoc->m_PrintSheetList.GetHeadPosition();
	CString	 strOut;
	COLORREF crOldTextColor		= pDC->GetTextColor();
	while (pos)
	{
		CPrintSheet&  rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		if (rPrintSheet.m_strPDFTarget.IsEmpty())
		{
			nLine += GetCellLines(rPrintSheet);
			continue;
		}

#ifdef PDF
		CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
		if (pTargetProps)
			if (pTargetProps->m_nType == CPDFTargetProperties::TypePDFFolder)
			{
				hIconOutFile		= hIconPDFOutFile;
				hIconOutFileDimmed	= hIconPDFOutFileDimmed;
			}
			else
			{
				hIconOutFile		= hIconJDFOutFile;
				hIconOutFileDimmed	= hIconJDFOutFileDimmed;
			}
#endif

		strOut = rPrintSheet.GetOutFilename(BOTHSIDES);
		BOOL bFilePerSheet = (strOut.IsEmpty()) ? FALSE : TRUE;
		if ( ! strOut.IsEmpty() || ! rPrintSheet.m_bDisplayOpen)
		{
			BOOL bFileExists = rPrintSheet.OutFileExists(FALSE, BOTHSIDES);
			pDC->DrawIcon(CPoint(0, nLine * m_pParent->m_nLineHeight + 2), (bFileExists) ? hIconOutFile : hIconOutFileDimmed);
			strOut = (strOut.IsEmpty()) ? "..." : strOut;
			CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 20);
			pDC->SetTextColor((bFileExists) ? BLACK : DARKGRAY);
			pDC->TextOut(20, GETTEXTPOS_VERT(nLine), strOut);

			CSize textExtent = pDC->GetTextExtent(strOut);
			CRect regRect(CPoint(0, nLine * m_pParent->m_nLineHeight), CSize(20 + textExtent.cx, 17));
			CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet,
						   									 DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, PDFSheetFile), NULL, CDisplayItem::ForceRegister);
			if (pDI)
			{
				pDI->SetMouseOverFeedback();
				pDI->SetNoStateFeedback();
				if (pDI->m_nState == CDisplayItem::Selected)
				{
					pDC->SaveDC();
					pDC->SelectClipRgn(NULL);
					CRect rcFeedback = pDI->m_BoundingRect;
					rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
					Graphics graphics(pDC->m_hDC);
					Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
					rc.Inflate(-1, -1);
					COLORREF crHighlightColor = RGB(165,175,200);
					SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
					graphics.FillRectangle(&br, rc);
					graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
					pDC->RestoreDC(-1);
				}
			}
		}

		int	nCount = 0;
		if (rPrintSheet.m_bDisplayOpen)
		{
			CLayout* pLayout = rPrintSheet.GetLayout();
			if ( ! pLayout)
				continue;

			int nSide = -1;
			do
			{
				nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;
				CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;

				nLine++;

				if ( ! bFilePerSheet)
				{
					strOut = rPrintSheet.GetOutFilename(nSide);
					if ( ! strOut.IsEmpty() || ! rPlateList.m_bDisplayOpen)
					{
						BOOL bFileExists = rPrintSheet.OutFileExists(FALSE, nSide);
						pDC->DrawIcon(CPoint(0, nLine * m_pParent->m_nLineHeight + 2), (bFileExists) ? hIconOutFile : hIconOutFileDimmed);
						strOut = (strOut.IsEmpty()) ? "..." : strOut;
						CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 20);	
						pDC->SetTextColor((bFileExists) ? BLACK : DARKGRAY);
						pDC->TextOut(20, GETTEXTPOS_VERT(nLine), strOut);

						CSize textExtent = pDC->GetTextExtent(strOut);
						CRect regRect(CPoint(0, nLine * m_pParent->m_nLineHeight), CSize(20 + textExtent.cx, 17));
						CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet,
						   												 DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, PDFSheetSideFile), (void*)nSide, CDisplayItem::ForceRegister);
						if (pDI)
						{
							pDI->SetMouseOverFeedback();
							pDI->SetNoStateFeedback();
							if (pDI->m_nState == CDisplayItem::Selected)
							{
								pDC->SaveDC();
								pDC->SelectClipRgn(NULL);
								CRect rcFeedback = pDI->m_BoundingRect;
								rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
								Graphics graphics(pDC->m_hDC);
								Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
								rc.Inflate(-1, -1);
								COLORREF crHighlightColor = RGB(165,175,200);
								SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
								graphics.FillRectangle(&br, rc);
								graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
								pDC->RestoreDC(-1);
							}
						}
					}
				}

				nCount++;
				if (rPlateList.GetCount())
				{
					if ( (rPlateList.GetHead().m_strPlateName != "Composite") || (rPlateList.GetCount() > 1) )
					{
						if (rPlateList.m_bDisplayOpen)
						{
							POSITION pos = rPlateList.GetHeadPosition();
							while (pos)
							{
								CPlate& rPlate = rPlateList.GetNext(pos);

								nLine++;
							
								strOut = rPrintSheet.GetOutFilename(-1, &rPlate);
								if ( ! strOut.IsEmpty())
								{
									BOOL bFileExists = rPrintSheet.OutFileExists(FALSE, -1, &rPlate);
									pDC->DrawIcon(CPoint(0, nLine * m_pParent->m_nLineHeight + 2), (bFileExists) ? hIconOutFile : hIconOutFileDimmed);
									strOut = (strOut.IsEmpty()) ? "..." : strOut;
									CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 20);
									pDC->SetTextColor((bFileExists) ? BLACK : DARKGRAY);
									pDC->TextOut(20, GETTEXTPOS_VERT(nLine), strOut);

									CSize textExtent = pDC->GetTextExtent(strOut);
									CRect regRect(CPoint(0, nLine * m_pParent->m_nLineHeight), CSize(20 + textExtent.cx, 17));
									CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet,
						   															 DISPLAY_ITEM_REGISTRY(CPrintSheetTableView, PDFSheetPlateFile), (void*)&rPlate, CDisplayItem::ForceRegister);
									if (pDI)
									{
										pDI->SetMouseOverFeedback();
										pDI->SetNoStateFeedback();
										if (pDI->m_nState == CDisplayItem::Selected)
										{
											pDC->SaveDC();
											pDC->SelectClipRgn(NULL);
											CRect rcFeedback = pDI->m_BoundingRect;
											rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
											Graphics graphics(pDC->m_hDC);
											Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
											rc.Inflate(-1, -1);
											COLORREF crHighlightColor = RGB(165,175,200);
											SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
											graphics.FillRectangle(&br, rc);
											graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
											pDC->RestoreDC(-1);
										}
									}
								}

								nCount++;
							}
						}
					}
				}
				if (pLayout->m_nProductType)
					nSide = BACKSIDE;	// skip backside
			}
			while (nSide != BACKSIDE);
		}
		
		nLine += GetCellLines(rPrintSheet) - nCount;
	}
	pDC->SetTextColor(crOldTextColor);
}

void CColumnOutputStatus::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int		 nLine			= 0;
	POSITION pos			= pDoc->m_PrintSheetList.GetHeadPosition();
	CString	 strOut;
	COLORREF crOldTextColor	= pDC->GetTextColor();
	while (pos)
	{
		CPrintSheet&  rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;

		if (rPrintSheet.m_strPDFTarget.IsEmpty())
		{
			nLine += GetCellLines(rPrintSheet);
			continue;
		}

		strOut = rPrintSheet.GetOutFilename(BOTHSIDES);
		BOOL bFilePerSheet = (strOut.IsEmpty()) ? FALSE : TRUE;
		if ( ! strOut.IsEmpty() || ! rPrintSheet.m_bDisplayOpen)
		{
			strOut = rPrintSheet.GetLastTimestamp(BOTHSIDES);
			CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
			pDC->SetTextColor((rPrintSheet.OutFileExists(FALSE, BOTHSIDES)) ? BLACK : DARKGRAY);
			pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
		}

		int	nCount = 0;
		if (rPrintSheet.m_bDisplayOpen)
		{
			CLayout* pLayout = rPrintSheet.GetLayout();
			if ( ! pLayout)
				continue;

			int nSide = -1;
			do
			{
				nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;
				CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;

				nLine++;

				if ( ! bFilePerSheet)
				{
					strOut = rPrintSheet.GetOutFilename(nSide);
					if ( ! strOut.IsEmpty() || ! rPlateList.m_bDisplayOpen)
					{
						strOut = rPrintSheet.GetLastTimestamp(nSide);
						CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());	
						pDC->SetTextColor((rPrintSheet.OutFileExists(FALSE, nSide)) ? BLACK : DARKGRAY);
						pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
					}
				}

				nCount++;
				if (rPlateList.GetCount())
				{
					if ( (rPlateList.GetHead().m_strPlateName != "Composite") || (rPlateList.GetCount() > 1) )
					{
						if (rPlateList.m_bDisplayOpen)
						{
							POSITION pos = rPlateList.GetHeadPosition();
							while (pos)
							{
								CPlate& rPlate = rPlateList.GetNext(pos);

								nLine++;
							
								strOut = rPrintSheet.GetOutFilename(-1, &rPlate);
								if ( ! strOut.IsEmpty())
								{
									strOut = rPrintSheet.GetLastTimestamp(-1, &rPlate);
									CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
									pDC->SetTextColor((rPrintSheet.OutFileExists(FALSE, -1, &rPlate)) ? BLACK : DARKGRAY);
									pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
								}

								nCount++;
							}
						}
					}
				}
				if (pLayout->m_nProductType)
					nSide = BACKSIDE;	// skip backside
			}
			while (nSide != BACKSIDE);
		}
	
		nLine += GetCellLines(rPrintSheet) - nCount;
	}
	pDC->SetTextColor(crOldTextColor);
}

void CColumnPDFTarget::OnDraw(CDC* pDC)
{
	CImpManDoc*	pDoc	= CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int			nLine	= 0;
	POSITION	pos		= pDoc->m_PrintSheetList.GetHeadPosition();
	CString		strOut, strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);
	while (pos)
	{
		CPrintSheet&  rPrintSheet  = pDoc->m_PrintSheetList.GetNext(pos);
		CLayout*	  pLayout	   = rPrintSheet.GetLayout();
		if (!pLayout)
			continue;
		if (rPrintSheet.IsEmpty())
			continue;

		CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
		if ( ! pTargetProps)
			strOut.Format(_T("%s: %s"), rPrintSheet.m_strPDFTarget, strUnknown);
		else
		{
			HICON hIcon = NULL;
			switch (pTargetProps->m_nType)
			{
			case CPDFTargetProperties::TypePDFFolder:	
			case CPDFTargetProperties::TypeJDFFolder:	hIcon = theApp.LoadIcon(IDI_FOLDER);	break;
			case CPDFTargetProperties::TypePDFPrinter:	hIcon = theApp.LoadIcon(IDI_PRINTER);	break;
			}
			pDC->DrawIcon(2, GETTEXTPOS_VERT(nLine) - 2, hIcon);
			strOut = rPrintSheet.m_strPDFTarget;
		}
		CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
		pDC->TextOut(20, GETTEXTPOS_VERT(nLine), strOut);

		nLine += GetCellLines(rPrintSheet);
	}
}

void CColumnPDFLocation::OnDraw(CDC* pDC)
{
	CImpManDoc*	pDoc	= CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int			nLine	= 0;
	POSITION	pos		= pDoc->m_PrintSheetList.GetHeadPosition();
	CString		strOut, strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);
	while (pos)
	{
		CPrintSheet&  rPrintSheet  = pDoc->m_PrintSheetList.GetNext(pos);
		CLayout*	  pLayout	   = rPrintSheet.GetLayout();
		if (!pLayout)
			continue;
		if (rPrintSheet.IsEmpty())
			continue;

		int nCount = 0;
		CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
		if ( ! pTargetProps)
		{
			strOut.Format(_T("%s"), strUnknown);
			CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
			pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
		}
		else
		{
			pTargetProps->m_strLocationBack.TrimLeft();
			pTargetProps->m_strLocationBack.TrimRight();
			BOOL bShowBack = FALSE;
			TCHAR szDest[1024];
			CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)pTargetProps->m_strLocation, &rPrintSheet, BOTHSIDES, NULL, -1, "", -1, -1);	
			strOut = szDest;
			if ( ! pTargetProps->m_strLocationBack.IsEmpty())
			{
				int nRelation = rPrintSheet.GetOutfileRelation(pTargetProps->m_strOutputFilename);
				if ( (nRelation == CPrintSheet::PlatePerFile) || (nRelation == CPrintSheet::SidePerFile) )
				{
					if (rPrintSheet.m_bDisplayOpen)
					{
						nLine++;
						bShowBack = TRUE;
						nCount++;
					}
					else
						strOut = _T("...");
				}
			}

			CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
			pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);

			if (bShowBack)
			{
				nLine++;
				strOut = pTargetProps->m_strLocationBack;
				CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
				pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
				nCount++;
			}
		}

		nLine += GetCellLines(rPrintSheet) - nCount;
	}
}

void CColumnPrintColorInfo::OnDraw(CDC* pDC)
{
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTableView

IMPLEMENT_DYNCREATE(CPrintSheetTableView, CFormView)

CPrintSheetTableView::CPrintSheetTableView()
	: CFormView(CPrintSheetTableView::IDD)
{
	m_ptDragIcon = CPoint(0, 0);

	m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CPrintSheetTableView));
	m_DisplayList.SetHighlightColor(RED);

	m_bOutputProfileChecked = FALSE;

	m_pDlgFoldSheetNumeration = new CDlgNumeration;

	//{{AFX_DATA_INIT(CPrintSheetTableView)
	//}}AFX_DATA_INIT
}

CPrintSheetTableView::~CPrintSheetTableView()
{
	delete m_pDlgFoldSheetNumeration;
}

void CPrintSheetTableView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintSheetTableView)
	DDX_Control(pDX, IDC_PDFOUTPUT_PROGRESS, m_pdfOutputProgress);
	DDX_Control(pDX, IDC_PRINTSHEET_TABLE_HEADER, m_headerFrame);
	DDX_Text(pDX, IDC_PDFOUTPUT_MESSAGE, m_strPDFOutputMessage);
	DDX_Text(pDX, IDC_PDFOUTPUT_CURPAGE, m_strCurrentOutputPage);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPrintSheetTableView, CFormView)
	//{{AFX_MSG_MAP(CPrintSheetTableView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_PRINTSHEET_DEL_FOLDSHEET, OnPrintsheetDelFoldsheet)
	ON_COMMAND(ID_PRINTSHEET_DUPLICATE_FOLDSHEET, OnPrintsheetDuplicateFoldsheet)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_BN_CLICKED(IDC_CLOSE_BUTTON, OnCloseButton)
	ON_COMMAND(ID_PRINTSHEET_NUMERATION, OnPrintsheetNumeration)
	ON_COMMAND(ID_EDIT_FOLDSHEETNUMBER, OnEditFoldSheetNumber)
	ON_BN_CLICKED(IDC_MAXMIN_BUTTON, OnMaxminButton)
	//}}AFX_MSG_MAP
	ON_NOTIFY(HDN_TRACK,	 IDC_PRINTSHEETTABLEHEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK,	 IDC_PRINTSHEETTABLEHEADER, OnHeaderEndTrack)
	ON_NOTIFY(HDN_ENDDRAG,	 IDC_PRINTSHEETTABLEHEADER, OnHeaderEndDrag) 
	ON_NOTIFY(HDN_ITEMCLICK, IDC_PRINTSHEETTABLEHEADER, OnHeaderItemClick) 
	ON_COMMAND(ID_PRINTSHEET_LIST,		OnPrintsheetList)
	ON_COMMAND(ID_PRINTCOLOR_INFO,		OnPrintcolorInfo)
	ON_COMMAND(ID_PDF_OUTPUT,			OnPdfOutput)
	ON_COMMAND(ID_BMP_OUTPUT,			OnBmpOutput)
	ON_COMMAND(ID_STANDARDLAYOUT_LOAD,  OnLoadStandardLayout)
	ON_COMMAND(ID_STANDARDLAYOUT_STORE, OnSaveStandardLayout)
	ON_COMMAND(ID_LAYOUT_RENAME,	    OnRenameLayout)
	ON_COMMAND(ID_START_OUTPUT,			OnStartOutput)
	ON_COMMAND(ID_CANCEL_OUTPUT,		OnCancelOutput)
	ON_COMMAND(ID_OUTPUT_PROFILE,		OnOutputProfile)
	ON_COMMAND(ID_OPEN_OUTPUT_FILE,		OnOpenOutputFile)
	ON_COMMAND(ID_PRINTCOLOR_TABLE,		OnPrintColorTable)
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Diagnose CPrintSheetTableView

#ifdef _DEBUG
void CPrintSheetTableView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPrintSheetTableView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CPrintSheetTableView 

BEGIN_EVENTSINK_MAP(CPrintSheetTableView, CFormView)
    //{{AFX_EVENTSINK_MAP(CPrintSheetTableView)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

int CPrintSheetTableView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CREATE_INPLACE_EDIT_EX(InplaceEdit);

	if( m_dropTarget.Register( this ) )
		return 0;   
	else      
		return -1;

	return 0;
}

void CPrintSheetTableView::OnContextMenu(CWnd* /*pWnd*/, CPoint point) 
{
	CMenu menu;
	if (m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout)))
		VERIFY(menu.LoadMenu(IDR_PRINTSHEETTABLEVIEW_LAYOUT_POPUP));
	else
		if (m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber)))
			VERIFY(menu.LoadMenu(IDR_PRINTSHEETTABLEVIEW_SHEETNUMBER_POPUP));
		else
			if (m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType)))
				VERIFY(menu.LoadMenu(IDR_PRINTSHEETTABLEVIEW_FOLDSHEET_POPUP));
			else
				return;

	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);

	CWnd* pWndPopupOwner = this;
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
}

void CPrintSheetTableView::OnInitialUpdate() 
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pParentFrame)
		return;

	m_PrintSheetTable.Create(this);
	switch(((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection)
	{
	case CMainFrame::NavSelectionPrintSheets:		
	case CMainFrame::NavSelectionPrintSheetMarks:	m_PrintSheetTable.Initialize(CPrintSheetTable::ShowPrintSheetList);	break;
	case CMainFrame::NavSelectionOutput:			m_PrintSheetTable.Initialize(CPrintSheetTable::ShowOutputList);		
													GetDlgItem(IDC_PRINTSHEET_TABLE_HEADER)->MoveWindow(0, 0, 300, 150);
													break;
	default:										m_PrintSheetTable.Initialize(CPrintSheetTable::ShowPrintSheetList);	break;
	}

	((CButton*)GetDlgItem(IDC_CLOSE_BUTTON ))->SetIcon(theApp.LoadIcon(IDI_CLOSE_WINDOW));
	((CButton*)GetDlgItem(IDC_MAXMIN_BUTTON))->SetIcon(theApp.LoadIcon(IDI_MAXIMIZE_WINDOW));

	CRect rect;
	m_outputListToolBarCtrl.GetWindowRect(rect);
	ScreenToClient(rect);
	//CRect aviWinRect = CRect(CPoint(rect.right + 10, rect.top), CSize(60, 25)); 
	//m_wndAnimate.Create(WS_CHILD | WS_CLIPSIBLINGS, aviWinRect, this, 0);
	//m_wndAnimate.ModifyStyleEx(0, WS_EX_STATICEDGE);

	ModifyStyle(0, WS_CLIPSIBLINGS);	// Clip header control 

	//if (((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection == CMainFrame::NavSelectionOutput)
	//{
	//	GetDlgItem(IDC_SEPARATOR_STATIC)->ShowWindow(SW_HIDE);
	//	int nHeight = m_PrintSheetTable.m_headerFrameRect.Height();
	//	m_PrintSheetTable.m_headerFrameRect.top = 0; m_PrintSheetTable.m_headerFrameRect.bottom = nHeight;
	//}

	OnSize(0, 0, 0);
	
	CFormView::OnInitialUpdate();
}



TBBUTTON tbprintSheetListButtons[] = { 
//    {0,   ID_PRINTSHEET_LIST,				TBSTATE_ENABLED |
//											TBSTATE_CHECKED, TBSTYLE_CHECKGROUP,  0, 0},
//    {1,   ID_PRINTCOLOR_INFO,				TBSTATE_ENABLED, TBSTYLE_CHECKGROUP,  0, 0},
//#ifdef PDF
//    {2,   ID_PDF_OUTPUT,					TBSTATE_ENABLED, TBSTYLE_CHECKGROUP,  0, 0},
//#else
//    {2,   ID_BMP_OUTPUT,					TBSTATE_ENABLED, TBSTYLE_CHECKGROUP,  0, 0},
//#endif
//    {0,   0,								TBSTATE_ENABLED, TBSTYLE_SEP,    0, 0},
//    {0,   0,								TBSTATE_ENABLED, TBSTYLE_SEP,    0, 0},
    {3,   ID_STANDARDLAYOUT_LOAD,			TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
    {4,   ID_STANDARDLAYOUT_STORE,			TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
    {5,   ID_LAYOUT_RENAME,					TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
    {0,   0,								TBSTATE_ENABLED, TBSTYLE_SEP,    0, 0},
    {6,   ID_PRINTSHEET_DUPLICATE_FOLDSHEET,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
    {7,   ID_PRINTSHEET_DEL_FOLDSHEET,		TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
    {8,   ID_PRINTSHEET_NUMERATION,			TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
};

void CPrintSheetTableView::CreatePrintSheetListToolbar()
{
	const int nBitmaps = 9;
	CSize	  sizeBitmap(30,20);
	CRect frameRect;
	GetDlgItem(IDC_PRINTSHEETTOOLBAR_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	m_printSheetListToolBarCtrl.Create(WS_CHILD | CCS_NORESIZE | CCS_NOPARENTALIGN | CCS_NODIVIDER | TBSTYLE_FLAT | TBSTYLE_TOOLTIPS, 
									   frameRect, this, 0);

	m_printSheetListToolBarCtrl.SetBitmapSize(sizeBitmap);

	tbprintSheetListButtons[ 5].fsState = TBSTATE_ENABLED;
	tbprintSheetListButtons[ 6].fsState = TBSTATE_ENABLED;
	tbprintSheetListButtons[ 9].fsState = TBSTATE_ENABLED;
	tbprintSheetListButtons[10].fsState = TBSTATE_ENABLED;
	tbprintSheetListButtons[11].fsState = TBSTATE_ENABLED;

	VERIFY(m_printSheetListToolBarCtrl.AddBitmap(nBitmaps, IDB_PRINTSHEETLIST_TOOLBAR) != -1);
	VERIFY(m_printSheetListToolBarCtrl.AddButtons(sizeof(tbprintSheetListButtons)/sizeof(TBBUTTON), tbprintSheetListButtons));

	CRect buttonRect;
    m_printSheetListToolTipCtrl.Create(this);
	m_printSheetListToolBarCtrl.GetItemRect(0,  buttonRect);
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_PRINTSHEET_LIST,				buttonRect, ID_PRINTSHEET_LIST);
	m_printSheetListToolBarCtrl.GetItemRect(1,  buttonRect);
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_PRINTCOLOR_INFO,				buttonRect, ID_PRINTCOLOR_INFO);
	m_printSheetListToolBarCtrl.GetItemRect(2,  buttonRect);
#ifdef PDF
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_PDF_OUTPUT,					buttonRect, ID_PDF_OUTPUT);
#else
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_BMP_OUTPUT,					buttonRect, ID_BMP_OUTPUT);
#endif
	m_printSheetListToolBarCtrl.GetItemRect(5,  buttonRect);
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_STANDARDLAYOUT_LOAD,			buttonRect, ID_STANDARDLAYOUT_LOAD);
	m_printSheetListToolBarCtrl.GetItemRect(6,  buttonRect);
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_STANDARDLAYOUT_STORE,			buttonRect, ID_STANDARDLAYOUT_STORE);
	m_printSheetListToolBarCtrl.GetItemRect(7,  buttonRect);
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_LAYOUT_RENAME,					buttonRect, ID_LAYOUT_RENAME);
	m_printSheetListToolBarCtrl.GetItemRect(9,  buttonRect);
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_PRINTSHEET_DUPLICATE_FOLDSHEET,buttonRect, ID_PRINTSHEET_DUPLICATE_FOLDSHEET);
	m_printSheetListToolBarCtrl.GetItemRect(10, buttonRect);
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_PRINTSHEET_DEL_FOLDSHEET,		buttonRect, ID_PRINTSHEET_DEL_FOLDSHEET);
	m_printSheetListToolBarCtrl.GetItemRect(11, buttonRect);
	m_printSheetListToolTipCtrl.AddTool(&m_printSheetListToolBarCtrl, ID_PRINTSHEET_NUMERATION,			buttonRect, ID_PRINTSHEET_NUMERATION);
	m_printSheetListToolTipCtrl.Activate(TRUE);

	m_printSheetListToolBarCtrl.SetToolTips(&m_printSheetListToolTipCtrl);

	m_printSheetListToolBarCtrl.AutoSize();
}



TBBUTTON tbprintColorInfoButtons[] = { 
//    {0,   ID_PRINTSHEET_LIST,				TBSTATE_ENABLED |
//											TBSTATE_CHECKED, TBSTYLE_CHECKGROUP,  0, 0},
//    {1,   ID_PRINTCOLOR_INFO,				TBSTATE_ENABLED, TBSTYLE_CHECKGROUP,  0, 0},
//#ifdef PDF
//    {2,   ID_PDF_OUTPUT,					TBSTATE_ENABLED, TBSTYLE_CHECKGROUP,  0, 0},
//#else
//    {2,   ID_BMP_OUTPUT,					TBSTATE_ENABLED, TBSTYLE_CHECKGROUP,  0, 0},
//#endif
//    {0,   0,								TBSTATE_ENABLED, TBSTYLE_SEP,    0, 0},
//    {0,   0,								TBSTATE_ENABLED, TBSTYLE_SEP,    0, 0},
    {3,   ID_PRINTCOLOR_TABLE,				TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
};

void CPrintSheetTableView::CreatePrintColorInfoToolbar()
{
	const int nBitmaps = 4;
	CSize	  sizeBitmap(30,20);
	CRect frameRect;
	GetDlgItem(IDC_PRINTSHEETTOOLBAR_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	m_printColorInfoToolBarCtrl.Create(WS_CHILD | CCS_NORESIZE | CCS_NOPARENTALIGN | CCS_NODIVIDER | TBSTYLE_FLAT | TBSTYLE_TOOLTIPS, 
									   frameRect, this, 0);

	m_printColorInfoToolBarCtrl.SetBitmapSize(sizeBitmap);

	VERIFY(m_printColorInfoToolBarCtrl.AddBitmap(nBitmaps, IDB_PRINTCOLORINFO_TOOLBAR) != -1);
	VERIFY(m_printColorInfoToolBarCtrl.AddButtons(sizeof(tbprintColorInfoButtons)/sizeof(TBBUTTON), tbprintColorInfoButtons));

	CRect buttonRect;
    m_printColorInfoToolTipCtrl.Create(this);
	m_printColorInfoToolBarCtrl.GetItemRect(0,  buttonRect);
	m_printColorInfoToolTipCtrl.AddTool(&m_printColorInfoToolBarCtrl, ID_PRINTSHEET_LIST,	buttonRect, ID_PRINTSHEET_LIST);
	m_printColorInfoToolBarCtrl.GetItemRect(1,  buttonRect);
	m_printColorInfoToolTipCtrl.AddTool(&m_printColorInfoToolBarCtrl, ID_PRINTCOLOR_INFO,	buttonRect, ID_PRINTCOLOR_INFO);
	m_printColorInfoToolBarCtrl.GetItemRect(2,  buttonRect);
#ifdef PDF
	m_printColorInfoToolTipCtrl.AddTool(&m_printColorInfoToolBarCtrl, ID_PDF_OUTPUT,		buttonRect, ID_PDF_OUTPUT);
#else
	m_printColorInfoToolTipCtrl.AddTool(&m_printColorInfoToolBarCtrl, ID_BMP_OUTPUT,		buttonRect, ID_BMP_OUTPUT);
#endif
	m_printColorInfoToolBarCtrl.GetItemRect(5,  buttonRect);
	m_printColorInfoToolTipCtrl.AddTool(&m_printColorInfoToolBarCtrl, ID_PRINTCOLOR_TABLE,	buttonRect, ID_PRINTCOLOR_TABLE);
	m_printColorInfoToolTipCtrl.Activate(TRUE);

	m_printColorInfoToolBarCtrl.SetToolTips(&m_printColorInfoToolTipCtrl);

	m_printColorInfoToolBarCtrl.AutoSize();
}


TBBUTTON tbOutputListButtons[] = { 
//    {0,   ID_PRINTSHEET_LIST,		TBSTATE_ENABLED |
//									TBSTATE_CHECKED, TBSTYLE_CHECKGROUP,  0, 0},
//    {1,   ID_PRINTCOLOR_INFO,		TBSTATE_ENABLED, TBSTYLE_CHECKGROUP,  0, 0},
//#ifdef PDF
//    {2,   ID_PDF_OUTPUT,			TBSTATE_ENABLED, TBSTYLE_CHECKGROUP,  0, 0},
//#else
//    {2,   ID_BMP_OUTPUT,			TBSTATE_ENABLED, TBSTYLE_CHECKGROUP,  0, 0},
//#endif
//    {0,   0,						TBSTATE_ENABLED, TBSTYLE_SEP,    0, 0},
//    {0,   0,						TBSTATE_ENABLED, TBSTYLE_SEP,    0, 0},
    {3,   ID_START_OUTPUT,			TBSTATE_ENABLED, TBSTYLE_CHECK,  0, 0},
    {4,   ID_CANCEL_OUTPUT,			TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0},
    {5,   ID_OUTPUT_PROFILE,		TBSTATE_ENABLED, TBSTYLE_CHECK,  0, 0},
    {0,   0,						TBSTATE_ENABLED, TBSTYLE_SEP,    0, 0},
    {6,   ID_OPEN_OUTPUT_FILE,		TBSTATE_ENABLED, TBSTYLE_CHECK,  0, 0},
};

void CPrintSheetTableView::CreateOutputListToolbar()
{
	const int nBitmaps = 7;
	CSize	  sizeBitmap(30,20);

	CRect frameRect;
	GetDlgItem(IDC_OUTPUTTOOLBAR_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	m_outputListToolBarCtrl.Create(WS_CHILD | CCS_NORESIZE | CCS_NOPARENTALIGN | CCS_NODIVIDER | TBSTYLE_FLAT | TBSTYLE_TOOLTIPS, frameRect, this, 0);

	m_outputListToolBarCtrl.SetBitmapSize(sizeBitmap);

#ifdef PDF
	VERIFY(m_outputListToolBarCtrl.AddBitmap(nBitmaps, IDB_PDF_OUTPUT_TOOLBAR) != -1);
#else
	VERIFY(m_outputListToolBarCtrl.AddBitmap(nBitmaps, IDB_BMP_OUTPUT_TOOLBAR) != -1);
#endif
	VERIFY(m_outputListToolBarCtrl.AddButtons(sizeof(tbOutputListButtons)/sizeof(TBBUTTON), tbOutputListButtons));

	CRect buttonRect;
    m_outputListToolTipCtrl.Create(this);
	m_outputListToolBarCtrl.GetItemRect(0, buttonRect);
	m_outputListToolTipCtrl.AddTool(&m_outputListToolBarCtrl, ID_PRINTSHEET_LIST,	buttonRect, ID_PRINTSHEET_LIST);
	m_outputListToolBarCtrl.GetItemRect(1, buttonRect);
	m_outputListToolTipCtrl.AddTool(&m_outputListToolBarCtrl, ID_PRINTCOLOR_INFO,	buttonRect, ID_PRINTCOLOR_INFO);
	m_outputListToolBarCtrl.GetItemRect(2, buttonRect);
#ifdef PDF
	m_outputListToolTipCtrl.AddTool(&m_outputListToolBarCtrl, ID_PDF_OUTPUT,		buttonRect, ID_PDF_OUTPUT);
#else
	m_outputListToolTipCtrl.AddTool(&m_outputListToolBarCtrl, ID_BMP_OUTPUT,		buttonRect, ID_BMP_OUTPUT);
#endif
	m_outputListToolBarCtrl.GetItemRect(5, buttonRect);
	m_outputListToolTipCtrl.AddTool(&m_outputListToolBarCtrl, ID_START_OUTPUT,		buttonRect, ID_START_OUTPUT);
	m_outputListToolBarCtrl.GetItemRect(6, buttonRect);
	m_outputListToolTipCtrl.AddTool(&m_outputListToolBarCtrl, ID_CANCEL_OUTPUT,		buttonRect, ID_CANCEL_OUTPUT);
	m_outputListToolBarCtrl.GetItemRect(7, buttonRect);
	m_outputListToolTipCtrl.AddTool(&m_outputListToolBarCtrl, ID_OUTPUT_PROFILE,	buttonRect, ID_OUTPUT_PROFILE);
	m_outputListToolBarCtrl.GetItemRect(9, buttonRect);
	m_outputListToolTipCtrl.AddTool(&m_outputListToolBarCtrl, ID_OPEN_OUTPUT_FILE,	buttonRect, ID_OPEN_OUTPUT_FILE);
	m_outputListToolTipCtrl.Activate(TRUE);

	m_outputListToolBarCtrl.SetToolTips(&m_outputListToolTipCtrl);

	m_outputListToolBarCtrl.AutoSize();

	m_bStartOutputChecked   = FALSE;
	m_bOutputProfileChecked = FALSE;

	if ( (theApp.m_nDongleStatus == CImpManApp::ClientDongle) && (theApp.m_nGUIStyle == CImpManApp::GUIStandard) )
	{
		m_outputListToolBarCtrl.EnableButton(ID_START_OUTPUT,  FALSE);
		m_outputListToolBarCtrl.EnableButton(ID_CANCEL_OUTPUT, FALSE);
	}
}

// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
BOOL CPrintSheetTableView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect;
		GetClientRect(scrollRect);
		scrollRect.top+= m_PrintSheetTable.m_headerFrameRect.bottom;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, scrollRect);

		if (sizeScroll.cx != 0)				// Scroll header control horizontally
			m_PrintSheetTable.m_HeaderCtrl.MoveWindow(-x, m_PrintSheetTable.m_headerFrameRect.top, scrollRect.Width() + x, m_PrintSheetTable.m_nHeaderHeight);
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

#include "Afxpriv.h"

// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
void CPrintSheetTableView::UpdateBars()
{
	// UpdateBars may cause window to be resized - ignore those resizings
	if (m_bInsideUpdate)
		return;         // Do not allow recursive calls

	// Lock out recursion
	m_bInsideUpdate = TRUE;

	// update the horizontal to reflect reality
	// NOTE: turning on/off the scrollbars will cause 'OnSize' callbacks
	ASSERT(m_totalDev.cx >= 0 && m_totalDev.cy >= 0);

	CRect rectClient;
	BOOL bCalcClient = TRUE;

	// allow parent to do inside-out layout first
	CWnd* pParentWnd = GetParent();
	if (pParentWnd != NULL)
	{
		// if parent window responds to this message, use just
		//  client area for scroll bar calc -- not "true" client area
		if ((BOOL)pParentWnd->SendMessage(WM_RECALCPARENT, 0,
			(LPARAM)(LPCRECT)&rectClient) != 0)
		{
			// use rectClient instead of GetTrueClientSize for
			//  client size calculation.
			bCalcClient = FALSE;
		}
	}

	CSize sizeClient;
	CSize sizeSb;

	if (bCalcClient)
	{
		// get client rect
		if (!GetTrueClientSize(sizeClient, sizeSb))
		{
			// no room for scroll bars (common for zero sized elements)
			CRect rect;
			GetClientRect(&rect);
			if (rect.right > 0 && rect.bottom > 0)
			{
				// if entire client area is not invisible, assume we have
				//  control over our scrollbars
				EnableScrollBarCtrl(SB_BOTH, FALSE);
			}
			m_bInsideUpdate = FALSE;
			return;
		}
	}
	else
	{
		// let parent window determine the "client" rect
		GetScrollBarSizes(sizeSb);
		sizeClient.cx = rectClient.right - rectClient.left;
		sizeClient.cy = rectClient.bottom - rectClient.top;
	}

	// enough room to add scrollbars
	CSize sizeRange;
	CPoint ptMove;
	CSize needSb;

	// get the current scroll bar state given the true client area
	GetScrollBarState(sizeClient, needSb, sizeRange, ptMove, bCalcClient);
	if (needSb.cx)
		sizeClient.cy -= sizeSb.cy;
	if (needSb.cy)
		sizeClient.cx -= sizeSb.cx;

	// first scroll the window as needed
	ScrollToDevicePosition(ptMove); // will set the scroll bar positions too

	// this structure needed to update the scrollbar page range
	SCROLLINFO info;
	info.fMask = SIF_PAGE|SIF_RANGE;
	info.nMin = 0;

	// now update the bars as appropriate
	EnableScrollBarCtrl(SB_HORZ, needSb.cx);
	if (needSb.cx)
	{
		info.nPage = sizeClient.cx;
		info.nMax = m_totalDev.cx-1;
		if (!SetScrollInfo(SB_HORZ, &info, TRUE))
			SetScrollRange(SB_HORZ, 0, sizeRange.cx, TRUE);
	}
	EnableScrollBarCtrl(SB_VERT, needSb.cy);
	if (needSb.cy)
	{
		info.nPage = sizeClient.cy;
		info.nMax = m_totalDev.cy-1;
		if (!SetScrollInfo(SB_VERT, &info, TRUE))
			SetScrollRange(SB_VERT, 0, sizeRange.cy, TRUE);
	}

	// remove recursion lockout
	m_bInsideUpdate = FALSE;
}

// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
void CPrintSheetTableView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);

	// This part is changed - rest is taken from MFC
	CRect scrollRect;
	GetClientRect(scrollRect);
	scrollRect.top+= m_PrintSheetTable.m_headerFrameRect.bottom;	// Protect header control from being vertically scrolled
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, scrollRect);
//	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y);	// original implementation
}

void CPrintSheetTableView::SetScrollSizes(int nMapMode, SIZE sizeTotal,
	const SIZE& sizePage, const SIZE& sizeLine)
{
	ASSERT(sizeTotal.cx >= 0 && sizeTotal.cy >= 0);
	ASSERT(nMapMode > 0);
	ASSERT(nMapMode != MM_ISOTROPIC && nMapMode != MM_ANISOTROPIC);

	int nOldMapMode = m_nMapMode;
	m_nMapMode = nMapMode;
	m_totalLog = sizeTotal;

	//BLOCK: convert logical coordinate space to device coordinates
	{
		CWindowDC dc(NULL);
		ASSERT(m_nMapMode > 0);
		dc.SetMapMode(m_nMapMode);

		// total size
		m_totalDev = m_totalLog;
		dc.LPtoDP((LPPOINT)&m_totalDev);
		m_pageDev = sizePage;
		dc.LPtoDP((LPPOINT)&m_pageDev);
		m_lineDev = sizeLine;
		dc.LPtoDP((LPPOINT)&m_lineDev);
		if (m_totalDev.cy < 0)
			m_totalDev.cy = -m_totalDev.cy;
		if (m_pageDev.cy < 0)
			m_pageDev.cy = -m_pageDev.cy;
		if (m_lineDev.cy < 0)
			m_lineDev.cy = -m_lineDev.cy;
	} // release DC here

	// now adjust device specific sizes
	ASSERT(m_totalDev.cx >= 0 && m_totalDev.cy >= 0);
	if (m_pageDev.cx == 0)
		m_pageDev.cx = m_totalDev.cx / 10;
	if (m_pageDev.cy == 0)
		m_pageDev.cy = m_totalDev.cy / 10;
	if (m_lineDev.cx == 0)
		m_lineDev.cx = m_pageDev.cx / 10;
	if (m_lineDev.cy == 0)
		m_lineDev.cy = m_pageDev.cy / 10;

	if (m_hWnd != NULL)
	{
		// window has been created, invalidate now
		UpdateBars();
		if (nOldMapMode != m_nMapMode)
			Invalidate(TRUE);
	}
}

void CPrintSheetTableView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*pResult*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 3 * CPrintSheetTableColumn::columnMargin)
		hdn->pitem->cxy = 3 * CPrintSheetTableColumn::columnMargin;
}

void CPrintSheetTableView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*pResult*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	m_PrintSheetTable.ResizeColumns(hdn->iItem, hdn->pitem->cxy);

	OnUpdate(NULL, 0, NULL);

	CRect clientRect;
	GetClientRect(clientRect);
	InvalidateRect(clientRect);
	UpdateWindow(); 
}

void CPrintSheetTableView::OnHeaderEndDrag(NMHDR* pNotifyStruct, LRESULT* pResult) 
{
	*pResult = FALSE;	// allow the control to automatically place and reorder the item

	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (m_PrintSheetTable.ReorderColumns(hdn->iItem, hdn->pitem->iOrder))
	{
		OnUpdate(NULL, 0, NULL);

		CRect clientRect;
		GetClientRect(clientRect);
		InvalidateRect(clientRect);
		UpdateWindow(); 
	}
}

void CPrintSheetTableView::OnHeaderItemClick(NMHDR* pNotifyStruct, LRESULT* pResult) 
{
	*pResult = FALSE;	

	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pParentFrame)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int	nColorDefinitionIndex = m_PrintSheetTable.m_ColumnRefList[hdn->iItem].m_pColumn->m_dwData;
	if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= pDoc->m_ColorDefinitionTable.GetSize()) )
		return;

	CString strColor = pDoc->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName;

	pView->m_DisplayList.DeselectAll();
	pView->Invalidate();
	pView->UpdateWindow();

	CDisplayItem* pDI = (pParentFrame->m_bShowExposures) ? pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure)) : 
														   pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	while (pDI)
	{
		CLayoutObject* pObject = NULL;
		if (pParentFrame->m_bShowExposures)
		{
			CExposure* pExposure = (CExposure*)pDI->m_pData;
			pObject = (pExposure) ? pExposure->GetLayoutObject() : NULL;
		}
		else
			pObject = (CLayoutObject*)pDI->m_pData;

		CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
		if (pObject && pPrintSheet)
		{
			CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
											  pObject->GetCurrentMarkTemplate() :
											  pObject->GetCurrentPageTemplate(pPrintSheet);

			if (pObjectTemplate)
			{
				BOOL bFound = FALSE;
				for (int i = 0; i < pObjectTemplate->m_ColorSeparations.GetSize(); i++)
				{
					if (pObjectTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex == nColorDefinitionIndex)
					{
						pView->m_DisplayList.SelectItem(*pDI);
						pView->m_DisplayList.InvalidateItem(pDI);
						bFound = TRUE;
						break;
					}
				}
				if ( ! bFound)
					if (pObjectTemplate->ContainsUnseparatedColor(nColorDefinitionIndex))
					{
						pView->m_DisplayList.SelectItem(*pDI);
						pView->m_DisplayList.InvalidateItem(pDI);
					}
			}
		}

		pDI = (pParentFrame->m_bShowExposures) ? pView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure)) : 
											     pView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}
	pView->UpdateWindow();
}

void CPrintSheetTableView::OnSize(UINT /*nType*/, int /*cx*/, int /*cy*/) 
{
//	CFormView::OnSize(nType, cx, cy);

	UpdateBars();	//	protect header control from being scrolled vertically 
	
	if (m_PrintSheetTable.m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdlHeader;
		WINDOWPOS wp;
		hdlHeader.prc	= &clientRect;
		hdlHeader.pwpos	= &wp;
		m_PrintSheetTable.m_HeaderCtrl.Layout(&hdlHeader);	
		m_PrintSheetTable.m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y+m_PrintSheetTable.m_headerFrameRect.top, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_PrintSheetTable.m_nHeaderHeight = wp.cy;

		CRect rect;
		GetDlgItem(IDC_SEPARATOR_STATIC)->GetWindowRect(rect);
		ScreenToClient(rect);
		GetDlgItem(IDC_SEPARATOR_STATIC)->MoveWindow(0, rect.top, clientRect.Width(), rect.Height());
	} 
}

void CPrintSheetTableView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/) 
{
	m_DisplayList.Invalidate();	// for now it's easier always to invalidate, because no. items changes when items are expanded

	m_PrintSheetTable.CalcTableLenght();

	CRect clientRect;
	GetClientRect(clientRect);

	CSize scrollSize(m_PrintSheetTable.m_nHeaderWidth, m_PrintSheetTable.m_nTableLenght + m_PrintSheetTable.m_headerFrameRect.bottom);
	CSize pageSize  (m_PrintSheetTable.m_nHeaderWidth, clientRect.Height() - m_PrintSheetTable.m_nHeaderHeight);
	CSize lineSize  (m_PrintSheetTable.m_nHeaderWidth, m_PrintSheetTable.m_nLineHeight);
	SetScrollSizes(MM_TEXT, scrollSize, pageSize, lineSize);

	Invalidate();
}

void CPrintSheetTableView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	CFormView::OnPrepareDC(pDC, pInfo);
}

void CPrintSheetTableView::OnDraw(CDC* pDC) 
{
	if ( ! m_PrintSheetTable.m_ColumnRefList.GetSize())
		return;

	m_DisplayList.BeginRegisterItems(pDC);

	m_PrintSheetTable.OnDraw(pDC);

	m_DisplayList.EndRegisterItems(pDC);
}

void CPrintSheetTableView::OnLButtonDown(UINT /*nFlags*/, CPoint point) 
{
	if (((CPrintSheetFrame*)GetParentFrame())->m_nViewMode == ID_ZOOM)
		((CNewPrintSheetFrame*)GetParentFrame())->m_nViewMode = ID_VIEW_SINGLE_SHEETS;

    SetFocus();	// In order to deactivate eventually active InplaceEdit

	m_DisplayList.OnLButtonDown(point); // pass message to display list

	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if (pParentFrame)
		if (pParentFrame->m_pDlgOutputMedia->m_hWnd)
			pParentFrame->m_pDlgOutputMedia->UpdatePrintSheetView();	// update, because selected sheets could be changed
																		// TODO: later enhance display list by msg, when any status changed
}

void CPrintSheetTableView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
    OnLButtonDown(nFlags, point);	// Handle second click 
    
	CFormView::OnLButtonDblClk(nFlags, point);
}

void CPrintSheetTableView::OnRButtonDown(UINT /*nFlags*/, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list
}

void CPrintSheetTableView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

DROPEFFECT CPrintSheetTableView::OnDragEnter(COleDataObject* pDataObject, DWORD /*dwKeyState*/, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return FALSE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	DROPEFFECT	 de;
	CClientDC    dc(this);
	CDisplayItem targetDI = m_DisplayList.GetItemPtInside(point, TRUE);

	if (targetDI.IsNull())
	{	
		m_dragPoint = CPoint(0,0);
		de = DROPEFFECT_NONE;
	}
	else
	{
		m_dragPoint = CPoint(targetDI.m_AuxRect.CenterPoint().x, targetDI.m_AuxRect.CenterPoint().y - 1);

        if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout)))
			de = DROPEFFECT_NONE;	// don't drag to printsheet based on same layout
		else
			de = DROPEFFECT_MOVE;
	}										 

	if (de != DROPEFFECT_NONE)
	{
		CRect clientRect;
		GetClientRect(clientRect);
		m_PrintSheetTable.m_rectBk = CRect(clientRect.left, m_dragPoint.y - 16, clientRect.right, m_dragPoint.y + 16);
		m_PrintSheetTable.SaveBackground(&dc, m_PrintSheetTable.m_rectBk);
		dc.DrawIcon(m_PrintSheetTable.m_rectBk.TopLeft(), m_PrintSheetTable.m_hIcon);
	}

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType)))
	{
		m_hDragIcon	= theApp.LoadIcon(IDI_DRAG_FOLDSHEET);

		int nRefIndex			 = (int)sourceDI.m_pData;
		CPrintSheet* pPrintSheet = (CPrintSheet*)sourceDI.m_pAuxData;
		CFoldSheet*  pFoldSheet  = pPrintSheet->GetFoldSheet(nRefIndex);
		m_strDragIcon.Format(_T(" %s  [%s] "), pFoldSheet->GetNumber(), pFoldSheet->m_strFoldSheetTypeName);
	}
	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout)))
	{
		m_hDragIcon	= theApp.LoadIcon(IDI_DRAG_LAYOUT);

		CLayout*	 pLayout	 = (CLayout*)sourceDI.m_pData;
		CPrintSheet* pPrintSheet = (CPrintSheet*)sourceDI.m_pAuxData;
		if (pLayout && pPrintSheet)
			m_strDragIcon.Format(_T(" %s  [%s] "), pPrintSheet->GetNumber(), pLayout->m_strLayoutName);
	}
	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber)))
	{
		m_hDragIcon	= theApp.LoadIcon(IDI_DRAG_LAYOUT);

		CPrintSheet* pPrintSheet = (CPrintSheet*)sourceDI.m_pData;
		if (pPrintSheet)
		{
			CLayout* pLayout = pPrintSheet->GetLayout();
			if (pLayout)
				m_strDragIcon.Format(_T(" %s  [%s] "), pPrintSheet->GetNumber(), pLayout->m_strLayoutName);
		}
	}

	dc.SelectStockObject(ANSI_VAR_FONT);
	dc.SetTextColor(WHITE);
	dc.SetBkColor(BLACK);
	CSize textExtent = dc.GetTextExtent(m_strDragIcon);
	m_rectDragIcon.left   = min(point.x - 40, point.x - 24 - textExtent.cx/2 - 1);
	m_rectDragIcon.right  = max(point.x,	  point.x - 24 + textExtent.cx/2 + 1); 
	m_rectDragIcon.top	  = point.y - 16;
	m_rectDragIcon.bottom = point.y + 16 + 2 * textExtent.cy;

	m_ptDragIcon = CPoint(0,0);
	SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
	dc.DrawIcon(point - CSize(40,16), m_hDragIcon);
	dc.TextOut(point.x - 24 - textExtent.cx/2, point.y + 5 + textExtent.cy, m_strDragIcon);
	m_ptDragIcon = point;

	return de;
}

DROPEFFECT CPrintSheetTableView::OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return FALSE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if ( ! (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType)) ||
	        sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber  )) ||
	        sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout)) ) )
		return DROPEFFECT_NONE;

	int			 nSide	  = -1;
	CDisplayItem targetDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType), TRUE, 5);
	if (targetDI.IsNull())
	{
        if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber)))
			return DROPEFFECT_NONE;
		targetDI = m_DisplayList.GetItemPtInside(point, TRUE);
	}

 	CClientDC dc(this);

	BOOL bBitmapRestored = FALSE;
	if (point != m_ptDragIcon)
		RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	DROPEFFECT   de;
	if (targetDI.IsNull())
	{
		if (m_PrintSheetTable.m_pBkBitmap)	// insertion arrow was shown - so hide now by restoring background
			m_PrintSheetTable.RestoreBackground(&dc, m_PrintSheetTable.m_rectBk);

		de = DROPEFFECT_NONE;
	}
	else
	{
        if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout)))
		{
			if ((nSide <= 0) && ((CPrintSheet*)sourceDI.m_pAuxData)->m_nLayoutIndex == ((CPrintSheet*)targetDI.m_pAuxData)->m_nLayoutIndex)
				de = DROPEFFECT_NONE;	// don't drag to printsheet based on same layout
			else
				de = DROPEFFECT_MOVE;
		}
		else
			if ((sourceDI.m_AuxRect == targetDI.m_AuxRect) && (nSide <= 0))	// drag and drop on same printsheet -> only COPY makes sense
				de = DROPEFFECT_COPY;
			else
				if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
					de = DROPEFFECT_COPY;
				else 
					if ((dwKeyState & MK_ALT) == MK_ALT)
						de = DROPEFFECT_MOVE;
					else
						de = DROPEFFECT_MOVE;
	}

	if (de != DROPEFFECT_NONE)
	{
		CPoint newDragPoint;
		if (nSide > 0)	// we are closely between two sheets
		{
			if (nSide == TOP)
				newDragPoint = CPoint(targetDI.m_AuxRect.CenterPoint().x, targetDI.m_AuxRect.top);
			else
				newDragPoint = CPoint(targetDI.m_AuxRect.CenterPoint().x, targetDI.m_AuxRect.bottom - 1);
		}
		else
			newDragPoint = CPoint(targetDI.m_AuxRect.CenterPoint().x, targetDI.m_AuxRect.CenterPoint().y - 1);

		if ((m_dragPoint != newDragPoint))
		{
			if (point == m_ptDragIcon)	// bitmap needs to be restored even if point has not changed
			{
				RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);
				bBitmapRestored = TRUE;
			}

			if (m_PrintSheetTable.m_pBkBitmap)	// insertion arrow was shown - so hide now by restoring background
				m_PrintSheetTable.RestoreBackground(&dc, m_PrintSheetTable.m_rectBk);

			CRect clientRect;
			GetClientRect(clientRect);
			m_PrintSheetTable.m_rectBk = CRect(clientRect.left, newDragPoint.y - 16, clientRect.right, newDragPoint.y + 16);
			m_PrintSheetTable.SaveBackground(&dc, m_PrintSheetTable.m_rectBk);
			dc.DrawIcon(m_PrintSheetTable.m_rectBk.TopLeft(), m_PrintSheetTable.m_hIcon);
			if (nSide > 0)	// show insertion line
			{
				dc.SelectObject(&m_PrintSheetTable.m_redPen);
				dc.MoveTo(m_PrintSheetTable.m_rectBk.left,  m_PrintSheetTable.m_rectBk.CenterPoint().y);
				dc.LineTo(m_PrintSheetTable.m_rectBk.right, m_PrintSheetTable.m_rectBk.CenterPoint().y);
			}

			m_dragPoint = newDragPoint;
		}										 
	}
	else
	{
		if (m_PrintSheetTable.m_pBkBitmap)	// insertion arrow was shown - so hide now by restoring background
			m_PrintSheetTable.RestoreBackground(&dc, m_PrintSheetTable.m_rectBk);

		m_dragPoint = CPoint(0,0);
	}

	if ((point != m_ptDragIcon) || bBitmapRestored)
	{
		m_rectDragIcon += point - m_ptDragIcon;
		SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
		dc.DrawIcon(point - CSize(40,16), m_hDragIcon);
		dc.SelectStockObject(ANSI_VAR_FONT);
		dc.SetTextColor(WHITE);
		dc.SetBkColor(BLACK);
		CSize textExtent = dc.GetTextExtent(m_strDragIcon);
		dc.TextOut(point.x - 24 - textExtent.cx/2, point.y + 5 + textExtent.cy, m_strDragIcon);
		m_ptDragIcon = point;
	}
 
	return de;
}

void CPrintSheetTableView::OnDragLeave() 
{
	RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	if (m_PrintSheetTable.m_pBkBitmap)	// separation arrow was shown - so hide now by restoring background
	{
		CClientDC dc(this);
		m_PrintSheetTable.RestoreBackground(&dc, m_PrintSheetTable.m_rectBk);
	}
}

BOOL CPrintSheetTableView::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point) 
{
	RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	if (m_PrintSheetTable.m_pBkBitmap)	// separation arrow was shown - so hide now by restoring background
	{
		CClientDC dc(this);
		m_PrintSheetTable.RestoreBackground(&dc, m_PrintSheetTable.m_rectBk);
	}

	if (dropEffect == DROPEFFECT_NONE)
		return FALSE;

	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return FALSE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if ( ! (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType)) ||
	        sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber  )) ||
	        sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout)) ) )
		return FALSE;

	int			 nSide	  = -1;
	CDisplayItem targetDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType), TRUE, 5);
	if (targetDI.IsNull())
	{
        if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber)))
			return FALSE;
		targetDI = m_DisplayList.GetItemPtInside(point, TRUE);
	}

	if (targetDI.IsNull())
		return FALSE;

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType)))
		return MoveCopyFoldSheet(sourceDI, targetDI, nSide, dropEffect);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber)))
		return MovePrintSheet(sourceDI, targetDI, nSide, dropEffect);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout)))
		return MoveCopyLayout(sourceDI, targetDI, nSide, dropEffect);

	return FALSE;
}

void CPrintSheetTableView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if (nChar == VK_CONTROL)
		if (((CNewPrintSheetFrame*)GetParentFrame())->m_nViewMode == ID_ZOOM)
			if (GetCursor() == theApp.m_CursorMagnifyIncrease)
				SetCursor(theApp.m_CursorMagnifyDecrease);
	
	CFormView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CPrintSheetTableView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if (nChar == VK_CONTROL)
		if (((CNewPrintSheetFrame*)GetParentFrame())->m_nViewMode == ID_ZOOM)
			if (GetCursor() == theApp.m_CursorMagnifyDecrease)
				SetCursor(theApp.m_CursorMagnifyIncrease);
	
	CFormView::OnKeyUp(nChar, nRepCnt, nFlags);
}
///////////////////////////////////////////////////////////////////
// bitmap save/restore
void CPrintSheetTableView::SaveBitmap(CBitmap* pBitmap, const CRect& rect)
{
	if (!pBitmap)
		return;

	pBitmap->DeleteObject();

	CClientDC dc(this);
 	CDC		  memDC;
	memDC.CreateCompatibleDC(&dc);
	pBitmap->CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(pBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), &dc, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void CPrintSheetTableView::RestoreBitmap(CBitmap* pBitmap, const CRect& rect)
{
	if (!pBitmap)
		return;

	CClientDC dc(this);
	CDC		  memDC;
	memDC.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = memDC.SelectObject(pBitmap);
	dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}
///////////////////////////////////////////////////////////////////

BOOL CPrintSheetTableView::OnSelectFoldSheetType(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
    if (pParentFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
	{
		CPrintSheet*	 pPrintSheet	   = (CPrintSheet*)pDI->m_pAuxData;
		int				 nComponentRefIndex	   = pPrintSheet->LayerTypeToLayerRef((int)pDI->m_pData);
		CPrintSheetView* pView			   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));

		if ( ! pParentFrame->m_bShowFoldSheet)
		{
			pParentFrame->m_bShowFoldSheet = TRUE;
			pView->m_DisplayList.Invalidate();
			pView->Invalidate();
			pView->UpdateWindow();
		}

		CDisplayItem*	 pDIFoldSheetLayer = NULL;
		if ((nComponentRefIndex >= 0) && pView)
		{
			pDIFoldSheetLayer = pView->m_DisplayList.GetItemFromData((void*)nComponentRefIndex, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
			if (pDIFoldSheetLayer)
			{
				pView->m_DisplayList.DeselectAll();
				pView->m_DisplayList.SelectItem(*pDIFoldSheetLayer);
				pView->m_DisplayList.InvalidateItem(NULL);
			}
		}

		if (pDIFoldSheetLayer)
		{
			if ( ! pDIFoldSheetLayer->m_bIsVisible)	
			{
				pView->Invalidate();
				pView->m_DisplayList.Invalidate();
				CPoint ScrollPos = pView->GetScrollPosition();
				pView->ScrollToPosition(CPoint(ScrollPos.x + pDIFoldSheetLayer->m_BoundingRect.left - 20, 0));
			}
		}
	}
	else
	{
		CPrintSheetTreeView* pView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
		if (pView)
			pView->SelectPrintSheetTreeItem(((CPrintSheet*)pDI->m_pAuxData)->m_nPrintSheetNumber);
	}

	return TRUE;
}

BOOL CPrintSheetTableView::OnActivateFoldSheetType(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect(di.m_BoundingRect.TopLeft() + CSize(25, CPrintSheetTableColumn::columnMargin/2),
			   CSize(di.m_BoundingRect.Width() - 25, m_PrintSheetTable.m_nLineHeight - CPrintSheetTableColumn::columnMargin/2));

	CPrintSheet* pPrintSheet = (CPrintSheet*)di.m_pAuxData;
	if ( ! pPrintSheet)
		return FALSE;
	if ((int)di.m_pData >= pPrintSheet->m_FoldSheetTypeList.GetSize())
		return FALSE;
	CFoldSheet* pFoldSheet = pPrintSheet->m_FoldSheetTypeList[(int)di.m_pData].m_pFoldSheet;
	if ( ! pFoldSheet)
		return FALSE;
	m_InplaceEdit.Activate(rect, pFoldSheet->GetNumber(), CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

BOOL CPrintSheetTableView::OnSelectSheetNumber(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
    if (pParentFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
	{
		CPrintSheetView* pView		   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		CPrintSheet*	 pPrintSheet   = (CPrintSheet*)pDI->m_pAuxData;
		CLayout*		 pLayout	   = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		CLayoutSide*	 pLayoutSide   = (pLayout) ? &pLayout->m_FrontSide : NULL;
		CDisplayItem*	 pDILayoutSide = NULL;
		if (pLayoutSide && pView)
		{
			if (pParentFrame->m_bShowPlate)	
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
			else
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
			if (pDILayoutSide)
			{
				pView->m_DisplayList.DeselectAll();
				pView->m_DisplayList.SelectItem(*pDILayoutSide);
				pView->m_DisplayList.InvalidateItem(NULL);
			}
		}

		if (pDILayoutSide)
		{
			if ( ! pDILayoutSide->m_bIsVisible)	
			{
				pView->Invalidate();
				pView->m_DisplayList.Invalidate();
				CPoint ScrollPos = pView->GetScrollPosition();
				pView->ScrollToPosition(CPoint(ScrollPos.x + pDILayoutSide->m_BoundingRect.left - 20, 0));
			}
		}
	}
	else
	{
		CPrintSheetTreeView* pView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
		if (pView)
			pView->SelectPrintSheetTreeItem(((CPrintSheet*)pDI->m_pAuxData)->m_nPrintSheetNumber);
	}

	return TRUE;
}

BOOL CPrintSheetTableView::OnActivateSheetNumber(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect(di.m_BoundingRect.TopLeft() + CSize(25, CPrintSheetTableColumn::columnMargin/2),
			   CSize(di.m_BoundingRect.Width() - 25, m_PrintSheetTable.m_nLineHeight - CPrintSheetTableColumn::columnMargin/2));

	m_InplaceEdit.Activate(rect, ((CPrintSheet*)di.m_pData)->GetNumber(), CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

BOOL CPrintSheetTableView::OnSelectLayout(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
    if (pParentFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
	{
		CPrintSheetView* pView		   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		CLayoutSide*	 pLayoutSide   = &((CLayout*)pDI->m_pData)->m_FrontSide;
		CPrintSheet*	 pPrintSheet   = (CPrintSheet*)pDI->m_pAuxData;
		CDisplayItem*	 pDILayoutSide = NULL;
		if (pLayoutSide && pView)
		{
			if (pParentFrame->m_bShowPlate)	
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
			else
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
			if (pDILayoutSide)
			{
				pView->m_DisplayList.DeselectAll();
				pView->m_DisplayList.SelectItem(*pDILayoutSide);
				pView->m_DisplayList.InvalidateItem(NULL);
			}
		}

		if (pDILayoutSide)
		{
			if ( ! pDILayoutSide->m_bIsVisible)	
			{
				pView->Invalidate();
				pView->m_DisplayList.Invalidate();
				CPoint ScrollPos = pView->GetScrollPosition();
				pView->ScrollToPosition(CPoint(ScrollPos.x + pDILayoutSide->m_BoundingRect.left - 20, 0));
			}
		}
	}
	else
	{
		CPrintSheetTreeView* pView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
		if (pView)
			pView->SelectPrintSheetTreeItem(((CPrintSheet*)pDI->m_pAuxData)->m_nPrintSheetNumber);
	}

	return TRUE;
}

BOOL CPrintSheetTableView::OnActivateLayout(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect(di.m_BoundingRect.TopLeft() + CSize(CPrintSheetTableColumn::columnMargin/2, CPrintSheetTableColumn::columnMargin/2),
			   CSize(di.m_BoundingRect.Width() - CPrintSheetTableColumn::columnMargin, m_PrintSheetTable.m_nLineHeight - CPrintSheetTableColumn::columnMargin/2));

	m_InplaceEdit.Activate(rect, ((CLayout*)di.m_pData)->m_strLayoutName, CInplaceEdit::FixedSize, 0, di);

	return TRUE;
}

void CPrintSheetTableView::OnKillFocusInplaceEdit() 		// Message from InplaceEdit
{
}

void CPrintSheetTableView::OnCharInplaceEdit(UINT nChar) // Message from InplaceEdit
{
	if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout))
	{
		if (nChar == VK_RETURN)	
 			UpdateLayoutName();
		return;
	}

	if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber))
	{
		if (nChar == VK_RETURN)	
		{
 			UpdatePrintSheetNumber();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
				OnActivateSheetNumber(pDI, CPoint(0,0), 0);
		}
		return;
	}

	if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType))
	{
		if (nChar == VK_RETURN)	
		{
 			UpdateFoldSheetNumber();
			CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			if (pDI)
				OnActivateFoldSheetType(pDI, CPoint(0,0), 0);
		}
		return;
	}
}

void CPrintSheetTableView::UpdatePrintSheetNumber() 		
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_InplaceEdit.m_di.m_pData;

	if (pPrintSheet->m_strNumerationStyle == m_InplaceEdit.GetString())
	{
		m_InplaceEdit.Deactivate();
		return;
	}

	pPrintSheet->m_strNumerationStyle = m_InplaceEdit.GetString();

	m_InplaceEdit.Deactivate();

	CImpManDoc::GetDoc()->SetModifiedFlag();										
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),			NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	
}


void CPrintSheetTableView::UpdateLayoutName() 		
{
	CLayout* pLayout = (CLayout*)m_InplaceEdit.m_di.m_pData;

	if (pLayout->m_strLayoutName == m_InplaceEdit.GetString())
	{
		m_InplaceEdit.Deactivate();
		return;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	POSITION	pos	 = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if (&rLayout != pLayout)
			if (rLayout.m_strLayoutName == m_InplaceEdit.GetString())
			{
				CString strMsg;
				AfxFormatString1(strMsg, IDS_RENAME_LAYOUT_NAME_EXISTING, m_InplaceEdit.GetString()); 
				AfxMessageBox(strMsg);
				return;
			}
	}

	m_InplaceEdit.LockKillFocusHandler();	// to prevent inplace edit from being closed

	int nRet;
	if (pLayout->GetNumPrintSheetsBasedOn() > 1)
		nRet = AfxMessageBox(IDS_RENAME_LAYOUT_PROMPT, MB_YESNOCANCEL);
	else
		nRet = IDYES;

	m_InplaceEdit.UnlockKillFocusHandler();

	switch (nRet)
	{
	case IDYES: 
		pLayout->m_strLayoutName = m_InplaceEdit.GetString();
		break;
	case IDNO:
		{
			CPrintSheet* pPrintSheet = (CPrintSheet*)m_InplaceEdit.m_di.m_pAuxData;
			CLayout		 layout;
			layout = *(pPrintSheet->GetLayout());
			layout.m_strLayoutName = m_InplaceEdit.GetString();
			CImpManDoc::GetDoc()->m_PrintSheetList.m_Layouts.AddTail(layout);
			pPrintSheet->m_nLayoutIndex = CImpManDoc::GetDoc()->m_PrintSheetList.m_Layouts.GetCount() - 1;
			CImpManDoc::GetDoc()->m_PrintSheetList.ReNumberPrintSheets();
		}
		break;
	case IDCANCEL: 
		m_InplaceEdit.Deactivate();
		return;
	}

	m_InplaceEdit.Deactivate();

	CImpManDoc::GetDoc()->SetModifiedFlag();										
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),			NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	
}

void CPrintSheetTableView::UpdateFoldSheetNumber() 		
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_InplaceEdit.m_di.m_pAuxData;
	if ( ! pPrintSheet)
		return;
	if ((int)m_InplaceEdit.m_di.m_pData >= pPrintSheet->m_FoldSheetTypeList.GetSize())
		return;
	CFoldSheet* pFoldSheet = pPrintSheet->m_FoldSheetTypeList[(int)m_InplaceEdit.m_di.m_pData].m_pFoldSheet;
	if ( ! pFoldSheet)
		return;
	if (pFoldSheet->m_strNumerationStyle == m_InplaceEdit.GetString())
	{
		m_InplaceEdit.Deactivate();
		return;
	}

	pFoldSheet->m_strNumerationStyle = m_InplaceEdit.GetString();

	m_InplaceEdit.Deactivate();

	CImpManDoc::GetDoc()->SetModifiedFlag();										
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));	
	theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));	
}

BOOL CPrintSheetTableView::OnActivateLayoutNotes(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CDlgNotes notesDlg;

	notesDlg.m_pLayout = (CLayout*)pDI->m_pData;
	notesDlg.DoModal();

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);

	Invalidate();
	UpdateWindow();

	return TRUE;
}

BOOL CPrintSheetTableView::OnActivatePDFSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been activated
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
	pPrintSheet->m_bDisplayOpen = ! pPrintSheet->m_bDisplayOpen;

	pDI->m_nState = CDisplayItem::Normal;

	OnUpdate(NULL, 0, 0);

	return TRUE;
}

BOOL CPrintSheetTableView::OnSelectPDFSheet(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;
	CRect rect(pDI->m_BoundingRect.TopLeft(), CSize(12, pDI->m_BoundingRect.Height()));
	if (rect.PtInRect(point))
		OnActivatePDFSheet(pDI, point, 0);

	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
    if (pParentFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
	{
		CPrintSheetView* pView		   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		CPrintSheet*	 pPrintSheet   = (CPrintSheet*)pDI->m_pData;
		CLayout*		 pLayout	   = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		CLayoutSide*	 pLayoutSide   = (pLayout) ? &pLayout->m_FrontSide : NULL;
		CDisplayItem*	 pDILayoutSide = NULL;
		if (pLayoutSide && pView)
		{
			if (pParentFrame->m_bShowPlate)	
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
			else
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
			if (pDILayoutSide)
			{
				pView->m_DisplayList.DeselectAll();
				pView->m_DisplayList.SelectItem(*pDILayoutSide);
				pView->m_DisplayList.InvalidateItem(NULL);
			}
		}

		if (pDILayoutSide)
		{
			if ( ! pDILayoutSide->m_bIsVisible)	
			{
				pView->Invalidate();
				pView->m_DisplayList.Invalidate();
				CPoint ScrollPos = pView->GetScrollPosition();
				pView->ScrollToPosition(CPoint(ScrollPos.x + pDILayoutSide->m_BoundingRect.left - 20, 0));
			}
		}
	}

	return TRUE;
}

BOOL CPrintSheetTableView::OnActivatePDFSheetSide(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been activated
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
	if (m_PrintSheetTable.m_nWhatToShow == CPrintSheetTable::ShowOutputList)
	{
		if ((int)pDI->m_pAuxData == FRONTSIDE)
			pPrintSheet->m_FrontSidePlates.m_bDisplayOpen = ! pPrintSheet->m_FrontSidePlates.m_bDisplayOpen;
		else
			pPrintSheet->m_BackSidePlates.m_bDisplayOpen  = ! pPrintSheet->m_BackSidePlates.m_bDisplayOpen;
	}
	else
		if (m_PrintSheetTable.m_nWhatToShow == CPrintSheetTable::ShowPrintColorInfo)
		{
			CLayout* pLayout = pPrintSheet->GetLayout();
			if (pLayout)
				if ((int)pDI->m_pAuxData == FRONTSIDE)
					pLayout->m_FrontSide.m_ObjectList.m_bDisplayOpen = ! pLayout->m_FrontSide.m_ObjectList.m_bDisplayOpen;
				else
					pLayout->m_BackSide.m_ObjectList.m_bDisplayOpen  = ! pLayout->m_BackSide.m_ObjectList.m_bDisplayOpen;
		}

	pDI->m_nState = CDisplayItem::Normal;

	OnUpdate(NULL, 0, 0);

	return TRUE;
}

BOOL CPrintSheetTableView::OnSelectPDFSheetSide(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;
	CRect rect(pDI->m_BoundingRect.TopLeft(), CSize(12, pDI->m_BoundingRect.Height()));
	if (rect.PtInRect(point))
		OnActivatePDFSheetSide(pDI, point, 0);

	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
    if (pParentFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
	{
		CPrintSheetView* pView		   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		CPrintSheet*	 pPrintSheet   = (CPrintSheet*)pDI->m_pData;
		CLayout*		 pLayout	   = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		CLayoutSide*	 pLayoutSide   = (pLayout) ? &pLayout->m_FrontSide : NULL;
		CDisplayItem*	 pDILayoutSide = NULL;
		if (pLayoutSide && pView)
		{
			if (pParentFrame->m_bShowPlate)	
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
			else
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
			if (pDILayoutSide)
			{
				pView->m_DisplayList.DeselectAll();
				pView->m_DisplayList.SelectItem(*pDILayoutSide);
				pView->m_DisplayList.InvalidateItem(NULL);
			}
		}

		if (pDILayoutSide)
		{
			if ( ! pDILayoutSide->m_bIsVisible)	
			{
				pView->Invalidate();
				pView->m_DisplayList.Invalidate();
				CPoint ScrollPos = pView->GetScrollPosition();
				pView->ScrollToPosition(CPoint(ScrollPos.x + pDILayoutSide->m_BoundingRect.left - 20, 0));
			}
		}
	}

	return TRUE;
}

BOOL CPrintSheetTableView::OnSelectPDFSheetFile(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
    if (pParentFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
	{
		CPrintSheetView* pView		   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		CPrintSheet*	 pPrintSheet   = (CPrintSheet*)pDI->m_pData;
		CLayout*		 pLayout	   = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		CLayoutSide*	 pLayoutSide   = (pLayout) ? &pLayout->m_FrontSide : NULL;
		CDisplayItem*	 pDILayoutSide = NULL;
		if (pLayoutSide && pView)
		{
			if (pParentFrame->m_bShowPlate)	
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
			else
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
			if (pDILayoutSide)
			{
				pView->m_DisplayList.DeselectAll();
				pView->m_DisplayList.SelectItem(*pDILayoutSide);
				pView->m_DisplayList.InvalidateItem(NULL);
			}
		}

		if (pDILayoutSide)
		{
			if ( ! pDILayoutSide->m_bIsVisible)	
			{
				pView->Invalidate();
				pView->m_DisplayList.Invalidate();
				CPoint ScrollPos = pView->GetScrollPosition();
				pView->ScrollToPosition(CPoint(ScrollPos.x + pDILayoutSide->m_BoundingRect.left - 20, 0));
			}
		}
	}

	return TRUE;
}

BOOL CPrintSheetTableView::OnSelectPDFSheetSideFile(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
    if (pParentFrame->m_nViewMode == ID_VIEW_ALL_SHEETS)
	{
		CPrintSheetView* pView		   = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		CPrintSheet*	 pPrintSheet   = (CPrintSheet*)pDI->m_pData;
		CLayout*		 pLayout	   = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		CLayoutSide*	 pLayoutSide   = (pLayout) ? &pLayout->m_FrontSide : NULL;
		CDisplayItem*	 pDILayoutSide = NULL;
		if (pLayoutSide && pView)
		{
			if (pParentFrame->m_bShowPlate)	
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Plate));
			else
				pDILayoutSide = pView->m_DisplayList.GetItemFromData(pLayoutSide, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Sheet));
			if (pDILayoutSide)
			{
				pView->m_DisplayList.DeselectAll();
				pView->m_DisplayList.SelectItem(*pDILayoutSide);
				pView->m_DisplayList.InvalidateItem(NULL);
			}
		}

		if (pDILayoutSide)
		{
			if ( ! pDILayoutSide->m_bIsVisible)	
			{
				pView->Invalidate();
				pView->m_DisplayList.Invalidate();
				CPoint ScrollPos = pView->GetScrollPosition();
				pView->ScrollToPosition(CPoint(ScrollPos.x + pDILayoutSide->m_BoundingRect.left - 20, 0));
			}
		}
	}

	return TRUE;
}

BOOL CPrintSheetTableView::OnActivatePDFSheetFile(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	// object(s) has been activated
{
	OnOpenOutputFile();

	return TRUE;
}

BOOL CPrintSheetTableView::OnActivatePDFSheetSideFile(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	// object(s) has been activated
{
	OnOpenOutputFile();

	return TRUE;
}

BOOL CPrintSheetTableView::OnActivatePDFSheetPlateFile(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	OnOpenOutputFile();

	return TRUE;
}

BOOL CPrintSheetTableView::OnActivatePDFSheetObject(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
    CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if (pParentFrame)
		if ( ! pParentFrame->m_pDlgLayoutObjectProperties->m_hWnd)
			pParentFrame->m_pDlgLayoutObjectProperties->Create((CLayoutObject*)pDI->m_pData, NULL, (CPrintSheet*)pDI->m_pAuxData, this, 1, TRUE);

	return TRUE;
}

BOOL CPrintSheetTableView::OnStatusChanged()	// status has changed (any item selected / deselected)
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if (pParentFrame)
		if (pParentFrame->m_pDlgOutputMedia->m_hWnd)
		{
			pParentFrame->m_pDlgOutputMedia->m_nWhatSheets = 2;		// selected sheets
			pParentFrame->m_pDlgOutputMedia->UpdateData(FALSE);
//			pParentFrame->m_pDlgOutputMedia->OnSheetsSelected();
			pParentFrame->m_pDlgOutputMedia->SelectPDFTargetCombo();
		}

	InitNumerationDialog(m_pDlgFoldSheetNumeration);

	if ( ! pParentFrame->m_pDlgLayoutObjectProperties->m_hWnd )
		return FALSE;

	if (pParentFrame->m_pDlgLayoutObjectProperties->m_hWnd)
	{
		CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetObject));
		if (pDI)
			pParentFrame->m_pDlgLayoutObjectProperties->ReinitDialog((CLayoutObject*)pDI->m_pData, (CPrintSheet*)pDI->m_pAuxData);
		else
			pParentFrame->m_pDlgLayoutObjectProperties->ReinitDialog((CLayoutObject*)NULL, (CPrintSheet*)NULL);
	}

	return TRUE;
}


////////////////////////////////////////////////////////////////////////////////////////
// context menu actions
void CPrintSheetTableView::OnPrintsheetList()	
{
	OnScrollBy(CSize(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)));
	m_DisplayList.Invalidate();
	Invalidate();
	m_PrintSheetTable.Initialize(CPrintSheetTable::ShowPrintSheetList);
}

void CPrintSheetTableView::OnPrintcolorInfo()	
{
	OnScrollBy(CSize(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)));
	m_DisplayList.Invalidate();
	Invalidate();
	m_PrintSheetTable.Initialize(CPrintSheetTable::ShowPrintColorInfo);
}

void CPrintSheetTableView::OnPdfOutput()	
{
	OnScrollBy(CSize(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)));
	m_DisplayList.Invalidate();
	Invalidate();
	m_PrintSheetTable.Initialize(CPrintSheetTable::ShowOutputList);
}

void CPrintSheetTableView::OnBmpOutput()	
{
	m_DisplayList.Invalidate();
	Invalidate();
	m_PrintSheetTable.Initialize(CPrintSheetTable::ShowOutputList);
}

void CPrintSheetTableView::OnLoadStandardLayout()	
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout));
	if (pDI)
		LoadStandardLayout((CLayout*)pDI->m_pData, (CPrintSheet*)pDI->m_pAuxData);
	else
	{
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber));
		if (pDI)
			LoadStandardLayout( ((CPrintSheet*)pDI->m_pData)->GetLayout(), (CPrintSheet*)pDI->m_pData);
	}
}

void CPrintSheetTableView::OnSaveStandardLayout()	
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout));
	if (pDI)
		SaveStandardLayout((CLayout*)pDI->m_pData, (CPrintSheet*)pDI->m_pAuxData);
	else
	{
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber));
		if (pDI)
			SaveStandardLayout( ((CPrintSheet*)pDI->m_pData)->GetLayout(), (CPrintSheet*)pDI->m_pData);
	}
}

void CPrintSheetTableView::OnRenameLayout()	
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout));
	if (pDI)
		m_DisplayList.ActivateItem(*pDI, CPoint(0,0));
}

void CPrintSheetTableView::OnPrintsheetDuplicateFoldsheet() 
{
	CDisplayItem* pSourceDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType));
	if (!pSourceDI)
		return;

	MoveCopyFoldSheet(*pSourceDI, *pSourceDI, -1, DROPEFFECT_COPY);
}

void CPrintSheetTableView::OnPrintsheetDelFoldsheet()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType));
	if (!pDI)
		return;

	DeleteFoldSheet((CPrintSheet*)pDI->m_pAuxData, (int)pDI->m_pData);
}

void CPrintSheetTableView::OnPrintsheetNumeration() 
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if (pParentFrame)
	{
		if (InitNumerationDialog(m_pDlgFoldSheetNumeration))
			if ( ! m_pDlgFoldSheetNumeration->m_hWnd)
				m_pDlgFoldSheetNumeration->Create(IDD_NUMERATION_DIALOG, this);
	}
}

BOOL CPrintSheetTableView::InitNumerationDialog(CDlgNumeration* pDlg)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if ( ! pPrintSheetTableView)
		return FALSE;
	CDisplayItem*		  pDIFirst			   = NULL;
	CDisplayItem*		  pDILast			   = NULL;
	CPrintSheet*		  pPrintSheetFirst	   = NULL;
	CPrintSheet*		  pPrintSheetLast	   = NULL;
	pDIFirst = pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout));
	if ( ! pDIFirst)
	{
		pDIFirst = pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber));
		pDILast  = pPrintSheetTableView->m_DisplayList.GetLastSelectedItem (DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber));
		pPrintSheetFirst = (pDIFirst) ? (CPrintSheet*)pDIFirst->m_pData : NULL;
		pPrintSheetLast  = (pDILast ) ? (CPrintSheet*)pDILast->m_pData  : NULL;
	}
	else
	{
		pDILast  = pPrintSheetTableView->m_DisplayList.GetLastSelectedItem (DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout));
		pPrintSheetFirst = (pDIFirst) ? (CPrintSheet*)pDIFirst->m_pAuxData : NULL;
		pPrintSheetLast  = (pDILast ) ? (CPrintSheet*)pDILast->m_pAuxData  : NULL;
	}

	pDlg->m_strTitle.LoadString(IDS_PRINTSHEETNUMERATION_TITLE);
	pDlg->m_pfnApplyCallback = ChangeNumeration;
	pDlg->m_pfnInitCallback  = InitNumerationDialog;
	pDlg->m_nSelection = (pPrintSheetFirst && pPrintSheetLast) ? CDlgNumeration::SelectionMarked : CDlgNumeration::SelectionAll;
	if (pPrintSheetFirst && pPrintSheetLast)
	{
		pDlg->m_nRangeFrom = pPrintSheetFirst->m_nPrintSheetNumber;
		pDlg->m_nRangeTo   = pPrintSheetLast->m_nPrintSheetNumber;
	}
	else
	{
		pDlg->m_nRangeFrom = 1;
		pDlg->m_nRangeTo   = pDoc->m_PrintSheetList.GetCount();
	}

	pDlg->m_nStartNum = (pDlg->m_bReverse) ? pDlg->m_nRangeTo : pDlg->m_nRangeFrom;

	pDlg->m_pViewToUpdate1 = pPrintSheetTableView;

	if (pDlg->m_hWnd)
		pDlg->UpdateData(FALSE);

	return TRUE;
}

void CPrintSheetTableView::ChangeNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int /*nProductPartIndex*/)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nCurNum = (nStartNum - 1) * CDlgNumeration::GetSeqLen(strStyle) + 1;
	switch (nSelection)
	{
	case CDlgNumeration::SelectionAll:
	case CDlgNumeration::SelectionRange:	
		{
			int		 i	 = 0;
			POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
			while (pos)
			{
				CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
				if ( ( nSelection == CDlgNumeration::SelectionAll) ||
					 ((nSelection == CDlgNumeration::SelectionRange) && (i + 1 >= nRangeFrom) && (i + 1 <= nRangeTo)) )
				{
					if (strStyle == _T("reset"))
					{
						rPrintSheet.m_strNumerationStyle = _T("$n");
						rPrintSheet.m_nNumerationOffset  = 0;
					}
					else
					{
						rPrintSheet.m_strNumerationStyle = strStyle;
						rPrintSheet.m_nNumerationOffset  = nCurNum - rPrintSheet.m_nPrintSheetNumber;
						nCurNum += (bReverse) ? -1 : 1;
					}
				}
				i++;
			}
		}
		break;

	case CDlgNumeration::SelectionMarked:	
		{
			CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
			if ( ! pPrintSheetTableView)
				return;
			CDisplayItem* pDI = pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout));
			if (pDI)
			{
				while (pDI)
				{
					if (pDI->m_pAuxData)	
					{
						CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
						if (strStyle == _T("reset"))
						{
							pPrintSheet->m_strNumerationStyle = _T("$n");
							pPrintSheet->m_nNumerationOffset  = 0;
						}
						else
						{
							pPrintSheet->m_strNumerationStyle = strStyle;
							pPrintSheet->m_nNumerationOffset  = nCurNum - pPrintSheet->m_nPrintSheetNumber;
						}
					}
					pDI = pPrintSheetTableView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetTableView, Layout));
					nCurNum += (bReverse) ? -1 : 1;
				}
			}
			else
			{
				CDisplayItem* pDI = pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber));
				while (pDI)
				{
					CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
					if (strStyle == _T("reset"))
					{
						pPrintSheet->m_strNumerationStyle = _T("$n");
						pPrintSheet->m_nNumerationOffset  = 0;
					}
					else
					{
						pPrintSheet->m_strNumerationStyle = strStyle;
						pPrintSheet->m_nNumerationOffset  = nCurNum - pPrintSheet->m_nPrintSheetNumber;
					}
					pDI = pPrintSheetTableView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetTableView, SheetNumber));
					nCurNum += (bReverse) ? -1 : 1;
				}
			}
		}
		break;
	}


	pDoc->SetModifiedFlag(TRUE);

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView), NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));
}

BOOL CPrintSheetTableView::MoveCopyFoldSheet(CDisplayItem& sourceDI, CDisplayItem& targetDI, int nSide, DROPEFFECT dropEffect)
{
	CImpManDoc*  pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return FALSE;

	BOOL bFurtherQuestions = TRUE;

	if (nSide <= 0)		// dropped on top of a sheet - not between two ones
	{
		CPrintSheet* pSourcePrintSheet	  = (CPrintSheet*)sourceDI.m_pAuxData;
		CPrintSheet* pTargetPrintSheet	  = (CPrintSheet*)targetDI.m_pAuxData;
		int			 nSourceLayerRefIndex = pSourcePrintSheet->LayerTypeToLayerRef((int)sourceDI.m_pData);
		int			 nTargetLayerRefIndex = pTargetPrintSheet->LayerTypeToLayerRef((int)targetDI.m_pData);
		BOOL		 bAutoAssign		  = FALSE;
		CFoldSheetDropTargetInfo dropTargetInfo(-1, NULL, NULL, NULL, nTargetLayerRefIndex, 0.0f, 0.0f, 0.0f, 0.0f, -1, -1, FALSE);

		if (pSourcePrintSheet->GetLayout() == pTargetPrintSheet->GetLayout())
		{
			if (pSourcePrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
			{
				// If source and target layout are equal -> ask user if he wants
				// this operation be performed on all printsheets of this type   
				int ret = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNOCANCEL);
				switch (ret)
				{
				case IDYES	  :	bAutoAssign = TRUE;		
								if (pSourcePrintSheet != pTargetPrintSheet)	// If different printsheets and AutoAssign 
									dropEffect = DROPEFFECT_MOVE;				// -> only MOVE makes sense
								break;
				case IDNO	  : bFurtherQuestions = FALSE;
								break;
				case IDCANCEL : return FALSE;
				}
			}
		}

		if (dropEffect == DROPEFFECT_COPY)
		{
			if (bFurtherQuestions)
				if (AfxMessageBox(IDS_FOLDSHEET_REALLY_COPY, MB_YESNO) == IDNO)
					return FALSE;

			BeginWaitCursor();	// copy/cut and paste could take a while, in case of many printsheets
			pTargetPrintSheet->CopyAndPasteFoldSheet(pSourcePrintSheet, nSourceLayerRefIndex, FALSE, dropTargetInfo, FALSE, bAutoAssign, NULL);
			if (bAutoAssign)
				pDoc->m_PrintSheetList.CopyAndPasteFoldSheet(pTargetPrintSheet, nSourceLayerRefIndex);
			EndWaitCursor();	
		}
		else
		{
			if (bFurtherQuestions)
				if (AfxMessageBox(IDS_FOLDSHEET_REALLY_MOVE, MB_YESNO) == IDNO)
					return FALSE;

			BOOL bReverse = (pSourcePrintSheet->m_nPrintSheetNumber <				// Determine direction of foldsheet move
							 pTargetPrintSheet->m_nPrintSheetNumber) ? TRUE : FALSE;
			BeginWaitCursor();	
			pTargetPrintSheet->CutAndPasteFoldSheet (pSourcePrintSheet, nSourceLayerRefIndex, FALSE, dropTargetInfo, FALSE, bAutoAssign, NULL);
			if (bAutoAssign)
				pDoc->m_PrintSheetList.CutAndPasteFoldSheet(pTargetPrintSheet, bReverse);
			EndWaitCursor();	
		}
										   
		pDoc->SetModifiedFlag();										
	}
	else	// dropped between two sheets
	{
		if (!targetDI.IsNull() && (nSide != LEFT) && (nSide != RIGHT))
		{
			CPrintSheet* pSourcePrintSheet = (CPrintSheet*)sourceDI.m_pAuxData;
			CPrintSheet* pTargetPrintSheet = (CPrintSheet*)targetDI.m_pAuxData;
			CPrintSheet  newPrintSheet;
			newPrintSheet.Create(CFoldSheetLayerRef(-1, -1), -1, 0);	
			CFoldSheetDropTargetInfo dropTargetInfo(-1, NULL, NULL, NULL, -1, 0.0f, 0.0f, 0.0f, 0.0f, -1, -1, FALSE);

			if (nSide == TOP)
				pTargetPrintSheet = pDoc->m_PrintSheetList.InsertBefore(pTargetPrintSheet, newPrintSheet);
			else
				pTargetPrintSheet = pDoc->m_PrintSheetList.InsertAfter (pTargetPrintSheet, newPrintSheet);

			int	nSourceLayerRefIndex = pSourcePrintSheet->LayerTypeToLayerRef((int)sourceDI.m_pData);
			if (dropEffect == DROPEFFECT_COPY)
			{
				if (bFurtherQuestions)
					if (AfxMessageBox(IDS_FOLDSHEET_REALLY_COPY, MB_YESNO) == IDNO)
						return FALSE;

				BeginWaitCursor();	
				pTargetPrintSheet->CopyAndPasteFoldSheet(pSourcePrintSheet, nSourceLayerRefIndex, FALSE, dropTargetInfo, FALSE, FALSE, NULL);
				EndWaitCursor();	
			}
			else
			{
				if (bFurtherQuestions)
					if (AfxMessageBox(IDS_FOLDSHEET_REALLY_MOVE, MB_YESNO) == IDNO)
						return FALSE;

				BeginWaitCursor();	
				pTargetPrintSheet->CutAndPasteFoldSheet (pSourcePrintSheet, nSourceLayerRefIndex, FALSE, dropTargetInfo, FALSE, FALSE, NULL);
				EndWaitCursor();	
			}

			pDoc->SetModifiedFlag();										
		}
	}

	if (pDoc->IsModified())
	{
		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	// because while drawing printsheets GetAct...() calls will be made
	}

	return TRUE;
}

BOOL CPrintSheetTableView::MovePrintSheet(CDisplayItem& sourceDI, CDisplayItem& targetDI, int nSide, DROPEFFECT /*dropEffect*/)
{
	CImpManDoc*  pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return FALSE;

	if (nSide > 0)	// dropped between two sheets
	{
		if (!targetDI.IsNull() && (nSide != LEFT) && (nSide != RIGHT))
		{
			CPrintSheet* pSourcePrintSheet = (CPrintSheet*)sourceDI.m_pData;
			CPrintSheet* pTargetPrintSheet = (CPrintSheet*)targetDI.m_pAuxData;
			if (nSide == TOP)
				pDoc->m_PrintSheetList.InsertBefore(pTargetPrintSheet, *pSourcePrintSheet);
			else
				pDoc->m_PrintSheetList.InsertAfter (pTargetPrintSheet, *pSourcePrintSheet);

			pDoc->m_PrintSheetList.RemoveAt(pSourcePrintSheet);

			pDoc->SetModifiedFlag();										
		}
	}

	if (pDoc->IsModified())
	{
		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	// because while drawing printsheets GetAct...() calls will be made
	}

	return TRUE;
}


BOOL CPrintSheetTableView::MoveCopyLayout(CDisplayItem& sourceDI, CDisplayItem& targetDI, int nSide, DROPEFFECT /*dropEffect*/)
{
	CImpManDoc*  pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return FALSE;

	if (nSide <= 0)		// dropped on top of a sheet - not between two ones
	{
		CPrintSheet* pSourcePrintSheet  = (CPrintSheet*)sourceDI.m_pAuxData;
		CPrintSheet* pTargetPrintSheet  = (CPrintSheet*)targetDI.m_pAuxData;

		CString strMsg;
		if ( ! pTargetPrintSheet->IsSuitableLayout(pSourcePrintSheet->GetLayout()))
		{
			AfxFormatString2(strMsg, IDS_LAYOUT_NOT_SUITABLE, pTargetPrintSheet->GetLayout()->m_strLayoutName, 
															  pSourcePrintSheet->GetLayout()->m_strLayoutName); 
			AfxMessageBox(strMsg);
			return FALSE;
		}

		int nRet;
		if (pTargetPrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
		{
			AfxFormatString2(strMsg, IDS_REPLACE_LAYOUT_ALL_PROMPT, pTargetPrintSheet->GetLayout()->m_strLayoutName, 
																	pSourcePrintSheet->GetLayout()->m_strLayoutName); 
			nRet = AfxMessageBox(strMsg, MB_YESNOCANCEL);

			if (nRet == IDCANCEL)
				return FALSE;
		}
		else
		{
			AfxFormatString2(strMsg, IDS_REPLACE_LAYOUT_PROMPT, pTargetPrintSheet->GetLayout()->m_strLayoutName, 
																pSourcePrintSheet->GetLayout()->m_strLayoutName); 
			nRet = AfxMessageBox(strMsg, MB_YESNO);
			if (nRet == IDNO)
				return FALSE;
		}

		switch (nRet)
		{
		case IDYES: // replace all -> old target layout needs to be removed
			{
				int		 nTargetLayoutIndex = pTargetPrintSheet->m_nLayoutIndex;
				POSITION pos			    = pDoc->m_PrintSheetList.GetHeadPosition();
				while (pos)
				{
					CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
					if (rPrintSheet.m_nLayoutIndex == nTargetLayoutIndex)
						rPrintSheet.m_nLayoutIndex = pSourcePrintSheet->m_nLayoutIndex;
				}

				// remove old target layout
				pos = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nTargetLayoutIndex);
				pDoc->m_PrintSheetList.m_Layouts.RemoveAt(pos);	// overloaded version recalcs indices automatically

				pos = pDoc->m_printingProfiles.FindIndex(nTargetLayoutIndex);
				pDoc->m_printingProfiles.RemoveAt(pos);
				pDoc->m_printGroupList.RemoveAt(nTargetLayoutIndex);
			}
			break;
		case IDNO:
			pTargetPrintSheet->m_nLayoutIndex = pSourcePrintSheet->m_nLayoutIndex;
			break;
		}

		pDoc->SetModifiedFlag();										
	}
	else	// dropped between two sheets
	{
		if (!targetDI.IsNull() && (nSide != LEFT) && (nSide != RIGHT))
		{
			CPrintSheet* pSourcePrintSheet = (CPrintSheet*)sourceDI.m_pAuxData;
			CPrintSheet* pTargetPrintSheet = (CPrintSheet*)targetDI.m_pAuxData;
			if (nSide == TOP)
				pDoc->m_PrintSheetList.InsertBefore(pTargetPrintSheet, *pSourcePrintSheet);
			else
				pDoc->m_PrintSheetList.InsertAfter (pTargetPrintSheet, *pSourcePrintSheet);

			pDoc->m_PrintSheetList.RemoveAt(pSourcePrintSheet);

			pDoc->SetModifiedFlag();										
		}
	}

	if (pDoc->IsModified())
	{
		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),			NULL, 0, NULL);	// important: update tree view before printsheetview
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	// because while drawing printsheets GetAct...() calls will be made
	}

	return TRUE;
}

void CPrintSheetTableView::DeleteFoldSheet(CPrintSheet* pPrintSheet, int nLayerTypeIndex)		
{
	CImpManDoc* pDoc	 = (CImpManDoc*)GetDocument();
	BOOL		bDeleted = FALSE;
	if (pPrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
	{
		int nRet = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNO);
		if (nRet == IDYES)
		{
			int nLayoutIndex = pPrintSheet->m_nLayoutIndex;
			POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
			while (pos)
			{
				CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
				if (rPrintSheet.m_nLayoutIndex == nLayoutIndex)
					if (!rPrintSheet.RemoveFoldSheetLayerNUp(nLayerTypeIndex))
						break;
					else
						bDeleted = TRUE;
			}
		}
		else
			bDeleted = pPrintSheet->RemoveFoldSheetLayerNUp(nLayerTypeIndex);
	}
	else
		bDeleted = pPrintSheet->RemoveFoldSheetLayerNUp(nLayerTypeIndex);

	if (bDeleted)	
	{
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();

		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

		pDoc->m_MarkSourceList.RemoveUnusedSources();
		pDoc->SetModifiedFlag();										

		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	// because while drawing printsheets GetAct...() calls will be made
	}
}

void CPrintSheetTableView::OnEditFoldSheetNumber() 
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, FoldSheetType));
	if ( ! pDI)
		return;
	OnActivateFoldSheetType(pDI, CPoint(0,0), 0);
}

////////////////////////////////////////////////////////////////////////////////////////


void CPrintSheetTableView::LoadStandardLayout(CLayout* pLayout, CPrintSheet* pPrintSheet)
{
	CString strFilter, strTitle;
	CFile file;
	CFileException fileException;

	strFilter.LoadString(IDS_LOAD_STANDARD_FILTER);
	CFileDialog dlg(TRUE, _T("lay"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING,
					strFilter);

	if ( ! pLayout)
	{
		AfxMessageBox(IDS_NOLAYOUT_CHOOSED_FORLOAD, MB_OK);
		return;
	}

	//strTitle.Format(IDS_LOADSTANDARD_TITLE, pLayout->m_strLayoutName);
	strTitle.LoadString(IDS_LOADSTANDARD_TITLE);
	dlg.m_ofn.lpstrTitle = strTitle;
	dlg.m_ofn.lpstrInitialDir = theApp.settings.m_szStandardsDir;
	if (dlg.DoModal() == IDCANCEL)
		return;

	if (!file.Open(dlg.m_ofn.lpstrFile, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", dlg.m_ofn.lpstrFile, fileException.m_cause);
		return; 
	}
	
	// check for layout with same name as desired standard
	// if there is any, this is not permitted
	CImpManDoc* pDoc		  = ((CImpManDoc*)CImpManDoc::GetDoc());
	CString		strLayoutName = ExtractStandardName(dlg.m_ofn.lpstrFileTitle);
	POSITION	pos		      = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if (&rLayout != pLayout)
			if (rLayout.m_strLayoutName.CompareNoCase(strLayoutName) == 0)
			{
				CString strMsg;
				AfxFormatString1(strMsg, IDS_REPLACE_LAYOUT_NAME_EXISTING, strLayoutName); 
				AfxMessageBox(strMsg);
				return;
			}
	}

	CString strMsg;
	int		nRet;
	if (pPrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 1)	// if more than one sheet based on that layout exists, 
	{																// ask if all of them to be replaced or only the selected
		AfxFormatString2(strMsg, IDS_REPLACE_LAYOUT_ALL_PROMPT, pLayout->m_strLayoutName, 
																dlg.m_ofn.lpstrFile); 
		nRet = AfxMessageBox(strMsg, MB_YESNOCANCEL);

		if (nRet == IDCANCEL)
			return;
	}
	else
	{
		AfxFormatString2(strMsg, IDS_REPLACE_LAYOUT_PROMPT, pLayout->m_strLayoutName,	// if there is only one sheet based on that layout,
															dlg.m_ofn.lpstrFile);		// ask if really want to replace.
		nRet = AfxMessageBox(strMsg, MB_YESNO);
		if (nRet == IDNO)
			return;
	}

	CArchive archive(&file, CArchive::load);
	CLayout			  layout;
	CPageTemplateList markTemplateList;
	CPageSourceList	  markSourceList;
	CPressDevice	  pressDeviceFront, pressDeviceBack;

	SerializeElements(archive, &layout, 1);
	markTemplateList.Serialize(archive);
	markSourceList.Serialize(archive);

	// load press device(s)
	SerializeElements(archive, &pressDeviceFront, 1);
	SerializeElements(archive, &pressDeviceBack,  1);

	if (pPrintSheet->IsSuitableLayout(&layout))
	{
		pDoc->m_MarkSourceList.MergeElements  (markSourceList,	 markTemplateList);	
 		pDoc->m_MarkTemplateList.MergeElements(markTemplateList, layout);

		BOOL bFrontRenamed = FALSE, bBackRenamed = FALSE;
		//if (pressDeviceFront == pressDeviceBack)
		//{
		//	layout.m_FrontSide.m_nPressDeviceIndex = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.AddDevice(pressDeviceFront, bFrontRenamed);
		//	layout.m_BackSide.m_nPressDeviceIndex  = layout.m_FrontSide.m_nPressDeviceIndex;
		//}
		//else
		//{
		//	layout.m_FrontSide.m_nPressDeviceIndex = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.AddDevice(pressDeviceFront, bFrontRenamed);
		//	layout.m_BackSide.m_nPressDeviceIndex  = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.AddDevice(pressDeviceBack,  bBackRenamed);
		//}
		layout.GetPrintingProfile().m_press.m_pressDevice		  = pressDeviceFront;
		layout.GetPrintingProfile().m_press.m_backSidePressDevice = pressDeviceBack;

		if (bFrontRenamed || bBackRenamed)
			AfxMessageBox(IDS_PRESSDEVICE_RENAMED);

		switch (nRet)
		{
		case IDYES: 
			pLayout->Copy(layout); break;	// replace
		case IDNO:
			{														// keep old layout for rest of sheets, based on that layout
				pDoc->m_PrintSheetList.m_Layouts.AddTail(layout);	// add new layout for the selected sheet
				pPrintSheet->m_nLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;
				pDoc->m_PrintSheetList.ReNumberPrintSheets();
			}
			break;
		}

		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results

		pDoc->ReorganizePressDeviceIndices(BOTHSIDES); // Look for double or unused entries

		if (pPrintSheet->GetLayout())
			pPrintSheet->GetLayout()->SetPrintSheetDefaultPlates();	// set all printsheet default plates to layout default plates

		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates(TRUE);	// TRUE = use plate defaults
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results
		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results

		pDoc->SetModifiedFlag();										
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),			NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	
	}
	else
	{
		CString strMsg;
		AfxFormatString2(strMsg, IDS_LAYOUT_NOT_SUITABLE, pLayout->m_strLayoutName, 
														  layout.m_strLayoutName); 
		AfxMessageBox(strMsg);
	}

	archive.Close();
	file.Close();
}

void CPrintSheetTableView::SaveStandardLayout(CLayout* pLayout, CPrintSheet* pPrintSheet)
{
	CString strFilter, strTitle;
	CFile file;
	CFileException fileException;

	strFilter.LoadString(IDS_LOAD_STANDARD_FILTER);
	CFileDialog dlg(FALSE, _T("lay"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING,
					strFilter);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pLayout)
	{
		AfxMessageBox(IDS_NOLAYOUT_CHOOSED_FORSTORE, MB_OK);
		return;
	}

	//strTitle.Format(IDS_STORESTANDARD_TITLE, pLayout->m_strLayoutName);
	strTitle.LoadString(IDS_STORESTANDARD_TITLE);
	dlg.m_ofn.lpstrTitle = strTitle.GetBuffer();
	dlg.m_ofn.lpstrInitialDir = theApp.settings.m_szStandardsDir;
	if (dlg.DoModal() == IDCANCEL)
		return;

	if (!file.Open(dlg.m_ofn.lpstrFile, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", dlg.m_ofn.lpstrFile, fileException.m_cause);
		return; 
	}

	CArchive archive(&file, CArchive::store);
	CLayout  layout;
	layout.Copy(*pLayout);
	// set indices to unassigned
	layout.m_FrontSide.m_nPressDeviceIndex = -1;
	layout.m_BackSide.m_nPressDeviceIndex  = -1;
	// update layoutname

	layout.m_strLayoutName	 = ExtractStandardName(dlg.m_ofn.lpstrFileTitle);	
	pLayout->m_strLayoutName = layout.m_strLayoutName;

	// save actual printsheet default plates 
	layout.m_FrontSide.m_DefaultPlates.RemoveAll();
	layout.m_BackSide.m_DefaultPlates.RemoveAll();
	layout.m_FrontSide.m_DefaultPlates.AddTail(pPrintSheet->m_FrontSidePlates.m_defaultPlate);
	layout.m_BackSide.m_DefaultPlates.AddTail (pPrintSheet->m_BackSidePlates.m_defaultPlate);

	CPageTemplateList markTemplateList;
	CPageSourceList	  markSourceList;

	layout.ExtractMarkTemplates		   (markTemplateList, pDoc->m_MarkTemplateList);
	markTemplateList.ExtractMarkSources(markSourceList,   pDoc->m_MarkSourceList);

	SerializeElements(archive, &layout, 1);
	markTemplateList.Serialize(archive);
	markSourceList.Serialize(archive);

	// serialize press device(s)
	SerializeElements(archive, pLayout->m_FrontSide.GetPressDevice(), 1);
	SerializeElements(archive, pLayout->m_BackSide.GetPressDevice(),  1);

	archive.Close();
	file.Close();

	pDoc->SetModifiedFlag();										
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	// update views, because layout name has changed
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),			NULL, 0, NULL);	
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	
}	


// remove extension from filename
CString CPrintSheetTableView::ExtractStandardName(CString strFileTitle)
{
	CString string = strFileTitle; string.MakeLower();
	int nIndex = string.Find(_T(".lay"));	
	return (nIndex != -1) ? strFileTitle.Left(nIndex) : strFileTitle;
}

void CPrintSheetTableView::HighlightPDFOutFile(CPrintSheet* pPrintSheet, int nSide, CPlate* pPlate)
{
	CClientDC dc(this);
	CDisplayItem* pDI = FindDisplayItem(pPrintSheet, nSide, pPlate);
	if (pDI)
		dc.InvertRect(CRect(0, pDI->m_BoundingRect.top, 10000, pDI->m_BoundingRect.bottom));
}

void CPrintSheetTableView::LowlightPDFOutFile()
{
	Invalidate();
	UpdateWindow();
}

void CPrintSheetTableView::StartPDFOutFeedback(int nPages)
{
	m_pdfOutputProgress.ShowWindow(SW_SHOW);
	m_pdfOutputProgress.SetRange(0, (short)nPages);
	m_pdfOutputProgress.SetPos(0);
	//if (m_wndAnimate.m_hWnd)
	//{
	//	m_wndAnimate.ShowWindow(SW_SHOW);
	//	m_wndAnimate.Open(IDR_PDF_TO_SHEET_AVI); 
	//	m_wndAnimate.Play(0, (unsigned)-1, (unsigned)-1);
	//}
}

void CPrintSheetTableView::StopPDFOutFeedback()
{
	//if (m_wndAnimate.m_hWnd)
	//{
	//	m_wndAnimate.Stop(); 
	//	m_wndAnimate.Close(); 
	//	m_wndAnimate.ShowWindow(SW_HIDE);
	//}
	m_pdfOutputProgress.ShowWindow(SW_HIDE);
	m_strCurrentOutputPage = "";
	UpdateData(FALSE);
}

CDisplayItem* CPrintSheetTableView::FindDisplayItem(CPrintSheet* pPrintSheet, int nSide, CPlate* pPlate)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return NULL;
	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(pPrintSheet->m_strPDFTarget);
	if ( ! pTargetProps)
		return NULL;

	int			  i		  = 0;
	CDisplayItem* pDI	  = NULL;
	int  nOutfileRelation = pPrintSheet->GetOutfileRelation(pTargetProps->m_strOutputFilename);
	switch (nOutfileRelation)
	{
	case CPrintSheet::JobPerFile:
	case CPrintSheet::SheetPerFile:
		for (i = 0; i < m_DisplayList.m_List.GetSize(); i++)
		{
			CDisplayItem& rDI = m_DisplayList.m_List[i];
			if (rDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetFile)))
				if (rDI.m_pData == pPrintSheet) 
				{
					pDI = &rDI;
					break;
				}
		}
		break;
	case CPrintSheet::SidePerFile:
		if ( ! pPrintSheet->m_bDisplayOpen)
		{
			pPrintSheet->m_bDisplayOpen = TRUE;
			OnUpdate(NULL, 0, NULL);
			UpdateWindow();
		}
		for (i = 0; i < m_DisplayList.m_List.GetSize(); i++)
		{
			CDisplayItem& rDI = m_DisplayList.m_List[i];
			if (rDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetSideFile)))
				if ( (rDI.m_pData == pPrintSheet) && ((int)rDI.m_pAuxData == nSide) )
				{
					pDI = &rDI;
					break;
				}
		}
		break;
	case CPrintSheet::PlatePerFile:
		if (pPlate)
		{
			CPlateList* pPlateList = (pPlate->GetSheetSide() == FRONTSIDE) ? &pPrintSheet->m_FrontSidePlates : &pPrintSheet->m_BackSidePlates;
			if ( ! pPlateList->m_bDisplayOpen)
			{
				pPrintSheet->m_bDisplayOpen = TRUE;
				pPlateList->m_bDisplayOpen  = TRUE;
				OnUpdate(NULL, 0, NULL);
				UpdateWindow();
			}
			for (i = 0; i < m_DisplayList.m_List.GetSize(); i++)
			{
				CDisplayItem& rDI = m_DisplayList.m_List[i];
				if (rDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetPlateFile)))
					if ( (rDI.m_pData == pPrintSheet) && (rDI.m_pAuxData == pPlate) )
					{
						pDI = &rDI;
						break;
					}
			}
		}
		break;
	}

	if (pDI)
		if ( ! pDI->m_bIsVisible)	
			OnVScroll(SB_THUMBTRACK, pDI->m_BoundingRect.top - 20, NULL);

	return pDI;
}

void CPrintSheetTableView::OnCloseButton() 
{
	//CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	//if (pParentFrame)
	//{
	//	pParentFrame->m_bShowPrintSheetTable = FALSE;
	//	pParentFrame->Resize();
	//	((CButton*)GetDlgItem(IDC_CLOSE_BUTTON))->SetButtonStyle(BS_PUSHBUTTON);
	//	SetFocus();
	//}
}

void CPrintSheetTableView::OnMaxminButton() 
{
	//CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	//if (pParentFrame)
	//{
	//	pParentFrame->m_bMaxPrintSheetTable = ! pParentFrame->m_bMaxPrintSheetTable;
	//	pParentFrame->Resize();
	//	((CButton*)GetDlgItem(IDC_MAXMIN_BUTTON))->SetIcon(theApp.LoadIcon((pParentFrame->m_bMaxPrintSheetTable) ? IDI_MINIMIZE_WINDOW : IDI_MAXIMIZE_WINDOW));
	//	((CButton*)GetDlgItem(IDC_MAXMIN_BUTTON))->SetButtonStyle(BS_PUSHBUTTON);
	//	SetFocus();
	//}
}

void CPrintSheetTableView::OnStartOutput()
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pParentFrame)
		return;

	m_bStartOutputChecked = ! m_bStartOutputChecked;

	if (pParentFrame->m_pDlgOutputMedia)
	{
		if (pParentFrame->m_pDlgOutputMedia->m_hWnd && ! m_bStartOutputChecked)
		{
			pParentFrame->m_pDlgOutputMedia->DestroyWindow();

			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		}
		if ( ! pParentFrame->m_pDlgOutputMedia->m_hWnd && m_bStartOutputChecked)
		{
			if ( ! pParentFrame->m_bShowPlate)
				pParentFrame->OnExposuresCheck();
			
			//pParentFrame->m_bEnablePlateButton		= FALSE;
			//pParentFrame->m_bEnablePlateTableButton = FALSE;

			pParentFrame->m_pDlgOutputMedia->m_nOutputTarget = CPDFTargetList::PrintSheetTarget;
			pParentFrame->m_pDlgOutputMedia->Create(IDD_PDFOUTPUT_MEDIA_SETTINGS, this);
		}
	}
}

void CPrintSheetTableView::OnCancelOutput()
{
}

void CPrintSheetTableView::OnOutputProfile()
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pParentFrame)
		return;

	m_bOutputProfileChecked = ! m_bOutputProfileChecked;

	if (pParentFrame->m_pDlgOutputMedia)
	{
		if (pParentFrame->m_pDlgOutputMedia->m_hWnd && ! m_bOutputProfileChecked)
		{
			pParentFrame->m_pDlgOutputMedia->DestroyWindow();

			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		}
		if ( ! pParentFrame->m_pDlgOutputMedia->m_hWnd && m_bOutputProfileChecked)
		{
			if ( ! pParentFrame->m_bShowPlate)
				pParentFrame->OnPlatecheck();
			
			//pParentFrame->m_bEnablePlateButton		= FALSE;
			//pParentFrame->m_bEnablePlateTableButton = FALSE;

			pParentFrame->m_pDlgOutputMedia->m_nOutputTarget = CPDFTargetList::PrintSheetTarget;
			pParentFrame->m_pDlgOutputMedia->Create(IDD_PDFOUTPUT_MEDIA_SETTINGS, this);
		}
	}
}

void CPrintSheetTableView::OnOpenOutputFile()
{
	CImpManDoc* pDoc = ((CImpManDoc*)GetDocument());
	if ( ! pDoc)
	{
		m_outputListToolBarCtrl.CheckButton(ID_OPEN_OUTPUT_FILE, FALSE);
		return;
	}

	pDoc->m_PrintSheetList.EnableOutput(FALSE);

	CPrintSheet*  pPrintSheet = NULL;
	CPlate*		  pPlate	  = NULL;
	int			  nSide		  = -1;
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheet));
	if ( ! pDI)   
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetFile));
	if (pDI)
	{
		pPrintSheet = (CPrintSheet*)pDI->m_pData;
		pPlate		= NULL;
		nSide		= BOTHSIDES;
		if (pPrintSheet)
			pPrintSheet->EnableOutput();
	}
	else
	{
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetSide));
		if ( ! pDI)   
			pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetSideFile));
		if (pDI)
		{
			pPrintSheet = (CPrintSheet*)pDI->m_pData;
			nSide		= (int)pDI->m_pAuxData;
			CPlateList* pPlateList = (nSide == FRONTSIDE) ? &pPrintSheet->m_FrontSidePlates : &pPrintSheet->m_BackSidePlates;
			if (pPlateList->GetCount())
				pPlate = &pPlateList->GetHead();
			if (pPrintSheet)
				pPrintSheet->EnableOutput(TRUE, nSide);
		}
		else
		{
			pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetPlate));
			if ( ! pDI)   
				pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetPlateFile));
			if (pDI)
			{
				pPrintSheet = (CPrintSheet*)pDI->m_pData;
				pPlate		= (CPlate*)pDI->m_pAuxData;
				nSide		= -1;
				if (pPlate)
					pPlate->m_bOutputDisabled = FALSE;
			}
		}
	}

	if ( ! pDI)
	{
		m_outputListToolBarCtrl.CheckButton(ID_OPEN_OUTPUT_FILE, FALSE);
		return;
	}
	if ( ! pPrintSheet)
	{
		m_outputListToolBarCtrl.CheckButton(ID_OPEN_OUTPUT_FILE, FALSE);
		return;
	}
	if (pPrintSheet->GetOutFilename(nSide, pPlate).IsEmpty())
	{
		m_outputListToolBarCtrl.CheckButton(ID_OPEN_OUTPUT_FILE, FALSE);
		return;
	}

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(pPrintSheet->m_strPDFTarget);
	if ( ! pTargetProps)
	{
		m_outputListToolBarCtrl.CheckButton(ID_OPEN_OUTPUT_FILE, FALSE);
		return;
	}

#ifdef PDF
	if ( ! pPrintSheet->OutFileExists(TRUE, nSide, pPlate))
	{
		if (AfxMessageBox(IDS_PDFFILE_NOT_EXISTING, MB_YESNO) == IDYES)
		{
			CPrintSheetView* pView	= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
			if (pView)
			{
				pView->m_DisplayList.DeselectAll(FALSE);	
				pView->Invalidate();
				pView->UpdateWindow();
			}

			BOOL	   bOverwrite = FALSE;
			CPDFOutput pdfOutput(*pTargetProps, pDoc->GetTitle());
			pdfOutput.ProcessPrintSheet(pPrintSheet, bOverwrite, FALSE, FALSE);	// 1st FALSE = not AutoCmd / 2nd FALSE = don't send job to target

			pDoc->m_PrintSheetList.NotifyClients(CPrintSheetList::NOTIFY_UPDATE_VIEW);
		}
		else
		{
			m_outputListToolBarCtrl.CheckButton(ID_OPEN_OUTPUT_FILE, FALSE);
			return;
		}
	}

	if (pPrintSheet->OutFileExists(TRUE, nSide, pPlate))
	{
		BeginWaitCursor();     
		CString strFilename  = pPrintSheet->GetOutFileFullPath(nSide, pPlate);
		CString strExtension = strFilename.Right(4);
		strExtension.MakeLower();
		if (strExtension == _T(".pdf"))
			CPageListFrame::LaunchAcrobat(strFilename, -1);
		else
			if (strExtension == _T(".jdf"))
				ShellExecute(NULL, _T("open"), strFilename, NULL, NULL, SW_SHOWNORMAL);	
		EndWaitCursor();     
	}
#else
	if (pPrintSheet->OutFileExists(TRUE, nSide, pPlate))
	{
		BeginWaitCursor();     
		LaunchNotepad(pPrintSheet->GetOutFileFullPath(BOTHSIDES));
		EndWaitCursor();     
	}
#endif

	m_outputListToolBarCtrl.CheckButton(ID_OPEN_OUTPUT_FILE, FALSE);
}


void CPrintSheetTableView::LaunchNotepad(const CString& strOutFile)
{
	CString strPath;
	strPath.Format(_T("NOTEPAD.EXE \"%s\""), strOutFile);

	STARTUPINFO			si = {0};
	PROCESS_INFORMATION pi = {0};
	si.cb			= sizeof(STARTUPINFO);
	si.lpReserved	= NULL;
	si.lpReserved2	= NULL;
	si.cbReserved2	= 0;
	si.lpDesktop	= NULL;
	si.dwFlags		= 0;

	CreateProcess( NULL, strPath.GetBuffer(255), NULL, NULL, TRUE, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi );
	WaitForInputIdle( pi.hProcess, INFINITE );
}


void CPrintSheetTableView::OnPrintColorTable()
{
	CDlgPrintColorTable dlg;

	if (dlg.DoModal() == IDCANCEL)
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PageSourceList.UpdateColorInfos();
	pDoc->m_MarkSourceList.UpdateColorInfos();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();
	pDoc->SetModifiedFlag();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),		NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),	NULL, 0, NULL);
}

void CPrintSheetTableView::SelectPrintSheet(CPrintSheet* pPrintSheet) 
{
	m_DisplayList.DeselectAll(FALSE);	// don't notify
	for (int i = 0; i < m_DisplayList.m_List.GetSize(); i++)
	{
		CDisplayItem& rDI = m_DisplayList.m_List[i];
		if (rDI.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheet))
		{
			CPrintSheet* pRefPrintSheet = (CPrintSheet*)rDI.m_pData;
			if (pRefPrintSheet == pPrintSheet)
			{
				rDI.m_nState = CDisplayItem::Selected;
				m_DisplayList.InvalidateItem(&rDI);
				break;
			}
		}
	}
	Invalidate();
	UpdateWindow();
}

void CPrintSheetTableView::OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{
	CView::OnNcCalcSize(bCalcValidRects, lpncsp);
//	lpncsp->rgrc[0].top += 40;		
}

void CPrintSheetTableView::OnNcPaint()
{
	CView::OnNcPaint();
}

int CPrintSheetTableView::GetMaxFoldSheetNumberLen(CDC* pDC, BOOL bIncludeLayerNum)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nTextLen = 0;
	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		int nLayerNumLen = ( bIncludeLayerNum && (rFoldSheet.m_LayerList.GetCount() > 1) ) ? pDC->GetTextExtent(" (X)").cx : 0;
		nTextLen = max(nTextLen, pDC->GetTextExtent(_T(" ") + rFoldSheet.GetNumber() + _T("  ")).cx + nLayerNumLen);
	}

	return nTextLen;	
}

