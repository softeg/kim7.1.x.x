// ImpManDocPrintGroups.h : interface of the CImpManDoc class
//
/////////////////////////////////////////////////////////////////////////////


#pragma once

//#include "ImpManDocProduction.h"


/////////////////////////// CPrintGroupComponent ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CPrintGroupComponent : public CObject
{
public:
    DECLARE_SERIAL( CPrintGroupComponent )
	CPrintGroupComponent();
	CPrintGroupComponent(const CPrintGroupComponent& rPrintGroupComponent); // copy constructor

private:
	int m_nPrintGroupIndex;

public:
	enum {Bound, Flat};

public:
	int m_nProductClass;
	int m_nProductClassIndex;
	int m_nNumBoundPages;
	int m_nFlatProductIndex;

public:
	const				CPrintGroupComponent& operator= (const CPrintGroupComponent& rPrintGroupComponent);
	void				Copy(const CPrintGroupComponent& rPrintGroupComponent);
	BOOL				operator==(const CPrintGroupComponent& rPrintGroupComponent);
	BOOL				operator!=(const CPrintGroupComponent& rPrintGroupComponent);
	BOOL				IsNull() { return (m_nProductClass == -1) ? TRUE : FALSE;};
	int					GetIndex();
	void				SetPrintGroupIndex(int nIndex) { m_nPrintGroupIndex = nIndex; }; 
	int					GetPrintGroupIndex()		   { return m_nPrintGroupIndex; }; 
	class CPrintGroup&	GetPrintGroup();
	static int			CompareDesiredQuantity(const void *p1, const void *p2);
	CRect				DrawInfo	(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect				DrawQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
};

template <> void AFXAPI SerializeElements <CPrintGroupComponent> (CArchive& ar, CPrintGroupComponent* pPrintGroupComponent, INT_PTR nCount);


class CPrintGroupComponentList : public CArray <CPrintGroupComponent, CPrintGroupComponent&> 
{
public:
	CPrintGroupComponentList();
	CPrintGroupComponentList(const CPrintGroupComponentList& rPrintGroupComponentList); // copy constructor

public:
	const	CPrintGroupComponentList& operator=(const CPrintGroupComponentList& rPrintGroupComponentList);
	void	Copy(const CPrintGroupComponentList& rPrintGroupComponentList);
	int		GetComponentIndex(CPrintGroupComponent* pComponent);
};


/////////////////////////// CPrintGroup ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CPrintGroup : public CObject
{
public:
    DECLARE_SERIAL( CPrintGroup )
	CPrintGroup();
	CPrintGroup(const CPrintGroup& rPrintGroup); // copy constructor


private:
	struct displayInfo
	{
		CRect		rcDisplayBox;
		BOOL		bIsDisplayed;
		BOOL		bShowExpanded;
	}				m_dspi;
	BOOL			m_bLocked;
	CLayout*		m_pLayout;


//Attributes:
public:
	CString						m_strName;
	CColorantList				m_colorantList;
	CString						m_strPaper;
	int							m_nPaperGrammage;
	float						m_fPaperVolume;
	int							m_nPaperType;
	int							m_nMinQuantity, m_nMaxQuantity;
	COleDateTime				m_dueDate;
	CString						m_strComment;
	CPrintGroupComponentList	m_componentList;
	CString						m_strDefaultProfile;


//Operations:
public:
	const						CPrintGroup& operator= (const CPrintGroup& rPrintGroup);
	BOOL						operator==(const CPrintGroup& rPrintGroup);
	void						SetLayout(CLayout* pLayout) { m_pLayout = pLayout; };
	BOOL						IsSimilar(CPrintGroup& rPrintGroup);
	BOOL						IsNull()   { return (m_componentList.GetSize() <= 0) ? TRUE : FALSE; };
	BOOL						IsLocked() { return m_bLocked; };
	void						SetLock(BOOL bLock) { m_bLocked = bLock; };
	void						Copy(const CPrintGroup& rPrintGroup);
	int							GetIndex();
	CPrintGroupComponent&		GetComponent(int nComponentIndex);
	CRect						GetDisplayBox() { return m_dspi.rcDisplayBox; };
	void						SetDisplayBox(CRect& rcBox) { m_dspi.rcDisplayBox = rcBox; };
	BOOL						IsDisplayed() { return m_dspi.bIsDisplayed; };
	void						SetDisplayed(BOOL bIsDisplayed) { m_dspi.bIsDisplayed = bIsDisplayed; };
	void						AddBoundProductPartComponent(CBoundProductPart& rProductPart, int nNumPages);
	void						AddFlatProductComponent(CFlatProduct& rFlatProduct);
	void						InsertComponent(CPrintGroupComponent component);
	CPrintGroupComponent*		GetFirstBoundComponent();	
	CPrintGroupComponent*		GetNextBoundComponent(CPrintGroupComponent* pComponent);	
	CBoundProductPart*			GetFirstBoundProductPart();
	CBoundProductPart*			GetNextBoundProductPart(CBoundProductPart* pProductPart);
	CFlatProduct*				GetFirstFlatProduct();
	CFlatProduct*				GetNextFlatProduct(CFlatProduct* pProduct);
	class CPrintingProfile&		GetPrintingProfile();
	CPrintSheet*				GetFirstPrintSheet();
	CPrintSheet*				GetNextPrintSheet(CPrintSheet* pPrintSheet);
	CLayout*					GetLayout();
	BOOL						HasBoundProducts();
	BOOL						HasFlatProducts();
	BOOL						DoShowProductsExpanded() { return m_dspi.bShowExpanded; };
	void						SetShowProductsExpanded(BOOL bShowExpanded) { m_dspi.bShowExpanded = bShowExpanded; };
	CRect						DrawHeader				(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect						DrawInfo				(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground);
	CRect						DrawFlatComponentStack	(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect						DrawColorInfo	  		(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect						DrawPaperInfo	  		(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect						DrawPostpressInfo 		(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	void						DrawPlanningTableLinks(CDC* pDC, BOOL bHighlight, CDisplayList* pDisplayList);
	void						SetQuantityRange(int nQuantity);
	BOOL						CheckInsideQuantityRange(CPrintGroup& rPrintGroup, class CProductionSetup& rProductionSetup);
	BOOL						CheckInsideColorRange(CPrintGroup& rPrintGroup, class CProductionSetup& rProductionSetup);
	CPrintGroupComponentList	PrepareComponentsToOptimize(float fSheetWidth, float fSheetHeight, CPrintingProfile& rPrintingProfile);
	void						InitPrintComponentsGroup(CPrintComponentsGroup& rPrintComponents);
	void						PreImposeBoundComponents(CPrintComponentsGroup& rPrintComponents);
};

template <> void AFXAPI SerializeElements <CPrintGroup> (CArchive& ar, CPrintGroup* pPrintGroup, INT_PTR nCount);


///////////////////////////// CPrintGroupList //////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CComponentDisplayInfo
{
public:
	void*		 m_pComponent;
	CPrintSheet* m_pPrintSheet;
	CRect		 m_rcDisplay;
};

class CPrintSheetDisplayInfo
{
public:
	CPrintSheet* m_pPrintSheet;
	CRect		 m_rcDisplay;
};

//class CPrintGroupComponent
//{
//public:
//	CPrintGroupComponent() 
//		{
//		m_pPrintGroup		= NULL;
//		m_pFoldSheet		= NULL;
//		m_pLayer			= NULL;
//		m_nLayerIndex		= 0;
//		m_pFlatProduct		= NULL;
//		m_pPrintSheet		= NULL;
//		m_pLayout			= NULL;
//		m_pPressDevice		= NULL;
//	}
//
//public:
//	CPrintGroup*		m_pPrintGroup;
//	CFoldSheet*			m_pFoldSheet;
//	CFoldSheetLayer*	m_pLayer;
//	int					m_nLayerIndex;
//	CFlatProduct*		m_pFlatProduct;
//	CPrintSheet*		m_pPrintSheet;
//	CLayout*			m_pLayout;
//	CPressDevice*		m_pPressDevice;
//};
//
//class CPrintGroupComponentList : public CArray <CPrintGroupComponent, CPrintGroupComponent&>
//{
//public:
//	CPrintGroup*		GetProductGroup(int nIndex)		{ return ( (nIndex < 0) || (nIndex >= GetSize()) ) ? NULL : GetAt(nIndex).m_pPrintGroup;	 };
//	CFoldSheet*			GetFoldSheet(int nIndex)		{ return ( (nIndex < 0) || (nIndex >= GetSize()) ) ? NULL : GetAt(nIndex).m_pFoldSheet;		 };
//	CFoldSheetLayer*	GetLayer(int nIndex)			{ return ( (nIndex < 0) || (nIndex >= GetSize()) ) ? NULL : GetAt(nIndex).m_pLayer;			 };
//	int					GetLayerIndex(int nIndex)		{ return ( (nIndex < 0) || (nIndex >= GetSize()) ) ? NULL : GetAt(nIndex).m_nLayerIndex;	 };
//	CFlatProduct*		GetFlatProduct(int nIndex)		{ return ( (nIndex < 0) || (nIndex >= GetSize()) ) ? NULL : GetAt(nIndex).m_pFlatProduct;	 };
//	CPrintSheet*		GetPrintSheet(int nIndex)		{ return ( (nIndex < 0) || (nIndex >= GetSize()) ) ? NULL : GetAt(nIndex).m_pPrintSheet;	 };
//	CLayout*			GetLayout(int nIndex)			{ return ( (nIndex < 0) || (nIndex >= GetSize()) ) ? NULL : GetAt(nIndex).m_pLayout;		 };
//	CPressDevice*		GetPressDevice(int nIndex)		{ return ( (nIndex < 0) || (nIndex >= GetSize()) ) ? NULL : GetAt(nIndex).m_pPressDevice;	 };
//	CPrintGroup*		GetPrevProductGroup(int nIndex)	{ return ( (nIndex <= 0) || (nIndex > GetSize()) ) ? NULL : GetAt(nIndex - 1).m_pPrintGroup;};
//	CFoldSheet*			GetPrevFoldSheet(int nIndex)	{ return ( (nIndex <= 0) || (nIndex > GetSize()) ) ? NULL : GetAt(nIndex - 1).m_pFoldSheet;	 };
//	CPrintSheet*		GetPrevPrintSheet(int nIndex)	{ return ( (nIndex <= 0) || (nIndex > GetSize()) ) ? NULL : GetAt(nIndex - 1).m_pPrintSheet; };
//	CLayout*			GetPrevLayout(int nIndex)		{ return ( (nIndex <= 0) || (nIndex > GetSize()) ) ? NULL : GetAt(nIndex - 1).m_pLayout;	 };
//	BOOL				HasFoldSheets();
//	BOOL				HasFlatProducts();
//};
//



class CPrintGroupList : public CArray <CPrintGroup, CPrintGroup&> 
{
public:
    DECLARE_SERIAL( CPrintGroupList )
	CPrintGroupList();


//Attributes:

public:
	//CArray <CComponentDisplayInfo,  CComponentDisplayInfo&>  m_componentDisplayInfos;
	//CArray <CPrintSheetDisplayInfo, CPrintSheetDisplayInfo&> m_printSheetDisplayInfos;
	//CPrintGroupComponentList								 m_componentList;
	//int														 m_nPrintSheetRowHeight;

//Operations:
public:
	CPrintGroup&	GetPrintGroup(int nGroupIndex);
	CPrintGroup*	FindFirstUnassigned();
	void			UnlockGroups();
	int				GetPrintGroupIndex(CPrintGroup* pPrintGroup);
	void			Analyze();
	void			CreatePrintGroupFromLayout(CLayout& rLayout, CPrintGroup& rPrintGroup);
	void			UnassignedProductsToPrintGroups();
	void			RemoveDoubleUnassignedComponents();
	void			ReorderEmptyPrintSheets();
	static int		ComparePrintSheetPos(const void *p1, const void *p2);
	int				GetPrintSheetInsertionPos(CPrintGroup& rPrintGroup);
	void			AddFindPrintGroup(CPrintGroup& rPrintGroup);
	CPrintGroup&	FindSameProductClass(CPrintGroup& rPrintGroup, int nExceptThisIndex);
	int				GetLongestFoldSheetNumString(CDC* pDC, CPrintSheet* pPrintSheet = NULL, int nComponentIndex = -1);
	int				GetLongestFoldSheetPageRange(CDC* pDC);
	int				GetLongestLabelNameString(CDC* pDC, BOOL bAssignedOnly = TRUE);
	int				GetLongestPrintSheetNumString(CDC* pDC, int nPrintSheetIndex = -1);
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

