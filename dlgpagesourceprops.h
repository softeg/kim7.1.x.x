// DlgPageSourceProps.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPageSourceProps dialog

class CDlgPageSourceProps : public CDialog
{
// Construction
public:
	CDlgPageSourceProps(CWnd* pParent = NULL);   // standard constructor
	~CDlgPageSourceProps();

// Attributes
public:
	CColorInfoListBox	m_ColorInfoListBox;
	CPageSourceHeader*	m_pPageSourceHeader;
	CPageSource*		m_pPageSource;
	int					m_nHeaderIndex;
	CImageList*			m_pImageList;

// Operations
public:


// Dialog Data
	//{{AFX_DATA(CDlgPageSourceProps)
	enum { IDD = IDD_PAGESOURCEHEADER_PROPS };
	CStatic	m_PagePreviewFrame;
	CString	m_strBoundingBox;
	CString	m_strDocumentPath;
	CString	m_strPageName;
	CString	m_strCreator;
	CString	m_strCreationDate;
	CString	m_strDocumentTitle;
	CString	m_strTrimBox;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPageSourceProps)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPageSourceProps)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnSelchangeSpotcolorlist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
