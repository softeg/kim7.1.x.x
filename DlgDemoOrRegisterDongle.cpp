// DlgDemoOrRegisterDongle.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "DlgDemoOrRegisterDongle.h"
#include "MainFrm.h"
#include "spromeps.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgDemoOrRegisterDongle 


CDlgDemoOrRegisterDongle::CDlgDemoOrRegisterDongle(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgDemoOrRegisterDongle::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgDemoOrRegisterDongle)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void CDlgDemoOrRegisterDongle::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgDemoOrRegisterDongle)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	DDX_Text(pDX, IDC_DEMO_REMAININGDAYS_STATIC, m_strRemainingDays);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgDemoOrRegisterDongle, CDialog)
	//{{AFX_MSG_MAP(CDlgDemoOrRegisterDongle)
	ON_BN_CLICKED(IDC_REGISTER_DONGLE, OnRegisterDongle)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgDemoOrRegisterDongle 

BOOL CDlgDemoOrRegisterDongle::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	MessageBeep(MB_ICONQUESTION);

	if (theApp.m_nRemainingDays > 0)
	{
		GetDlgItem(IDC_DEMOPHASE_EXPIRED_STATIC)->ShowWindow(SW_HIDE);
		CString strDays; strDays.LoadString((theApp.m_nRemainingDays > 1) ? IDS_DAYS : IDS_DAY);
		m_strRemainingDays.Format(_T("%d %s"), theApp.m_nRemainingDays, strDays);
	}
	else
	{
		GetDlgItem(IDC_DEMOMESSAGE1_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_DEMOMESSAGE2_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_DEMO_REMAININGDAYS_STATIC)->ShowWindow(SW_HIDE);
	}

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgDemoOrRegisterDongle::OnRegisterDongle() 
{
	CString strPath, strArg;
	strPath.Format(_T("%s\\Sentinel_Setup\\Setupx86.exe"), theApp.settings.m_szWorkDir);
	strArg.Format(_T("/q /USB /p%s\\Sentinel_Setup\\I386"), theApp.settings.m_szWorkDir);

	_tspawnl(_P_WAIT, strPath, _T("setupx86.exe"), strArg, NULL);	

	HKEY	hKey1;
	CString	strKey1 = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey1, &hKey1);
	RegSetValueEx(hKey1, _T("Demo"),  0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));
	RegCloseKey(hKey1);

	CheckDonglePDF(FALSE, FALSE);

	if ( ! theApp.m_bDemoMode)
	{
		AfxMessageBox(IDS_DONGLE_DRIVER_INSTALLED, MB_OK);
		EndDialog(TRUE);
	}
	else
		AfxMessageBox(IDS_DONGLE_DRIVER_NOT_INSTALLED, MB_OK);
}

