// DlgDirPicker.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgDirPicker.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgDirPicker

IMPLEMENT_DYNAMIC(CDlgDirPicker, CFileDialog)

#if WINVER >= 0x0600 
CDlgDirPicker::CDlgDirPicker(BOOL bOpenFileDialog, LPCTSTR lpszDefExt, LPCTSTR lpszFileName,
		DWORD dwFlags, LPCTSTR lpszFilter, CWnd* pParentWnd) :
		CFileDialog(bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags, lpszFilter, pParentWnd, 0, FALSE)
#else
CDlgDirPicker::CDlgDirPicker(BOOL bOpenFileDialog, LPCTSTR lpszDefExt, LPCTSTR lpszFileName,
		DWORD dwFlags, LPCTSTR lpszFilter, CWnd* pParentWnd) :
		CFileDialog(bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags, lpszFilter, pParentWnd)
#endif
{
	m_ofn.Flags|= OFN_ENABLETEMPLATE | OFN_EXPLORER;
	m_ofn.lpTemplateName = MAKEINTRESOURCE(IDD_DIRECTORY_PICKER);
}


BEGIN_MESSAGE_MAP(CDlgDirPicker, CFileDialog)
	//{{AFX_MSG_MAP(CDlgDirPicker)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CDlgDirPicker::OnInitDialog() 
{
	CFileDialog::OnInitDialog();
	
	GetParent()->GetDlgItem(cmb1)->ModifyStyle(WS_VISIBLE, 0);	// file type combo make invisible
	GetParent()->GetDlgItem(stc2)->ModifyStyle(WS_VISIBLE, 0);	// corresponding label make invisible
	CString strTitle;
	strTitle.LoadString(IDS_DIRECTORY_STRING);
	GetParent()->GetDlgItem(stc3)->SetWindowText(strTitle);		// Set Window-title
	GetParent()->GetDlgItem(IDOK)->ModifyStyle(WS_VISIBLE, 0);	// OK button make invisible
	GetParent()->GetDlgItem(cmb13)->ModifyStyle(0, WS_DISABLED);	// edit-field make readonly

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgDirPicker::OnOK() 
{
	char szPath[MAX_PATH];
	CommDlg_OpenSave_GetFolderPath(GetParent()->m_hWnd, szPath, MAX_PATH);
	m_strPath = szPath;

	((CFileDialog*)GetParent())->EndDialog(IDOK);
}

//WM_NOTIFY	CDN_FOLDERCHANGE
void CDlgDirPicker::OnFolderChange()
{
	TCHAR szPath[MAX_PATH];
	CommDlg_OpenSave_GetFolderPath(GetParent()->m_hWnd, szPath, MAX_PATH);

	if (theApp.DirectoryExist(szPath))
	{
		if ( ! m_strDirToAdd.IsEmpty())
		{
			CString strCompletePath;
			strCompletePath = szPath;
			strCompletePath += "\\";
			strCompletePath += m_strDirToAdd;
			GetParent()->GetDlgItem(cmb13)->SetWindowText(strCompletePath);
		}
		else
			GetParent()->GetDlgItem(cmb13)->SetWindowText(szPath);
	}
	else
		GetParent()->GetDlgItem(cmb13)->SetWindowText(_T(""));
}

void CDlgDirPicker::OnSize(UINT nType, int cx, int cy) 
{
	CFileDialog::OnSize(nType, cx, cy);
	
	CRect rectOK, rectCancel;
	GetDlgItem(IDOK)->GetWindowRect(rectOK); ScreenToClient(rectOK);
	GetParent()->GetDlgItem(IDCANCEL)->GetWindowRect(rectCancel); ScreenToClient(rectCancel);
	rectOK.left  = rectCancel.left;
	rectOK.right = rectCancel.right;
	GetDlgItem(IDOK)->MoveWindow(rectOK);
}
