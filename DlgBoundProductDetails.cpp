// DlgBoundProductDetails.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgBoundProductDetails.h"


// CDlgBoundProductDetails-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgBoundProductDetails, CDialog)

CDlgBoundProductDetails::CDlgBoundProductDetails(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgBoundProductDetails::IDD, pParent)
{
	m_nBoundProductPartIndex = 0;
	m_nBoundProductPartPageOrientation = -1;
	m_fBoundProductPartHeight = 0.0f;
	m_fBoundProductPartWidth = 0.0f;
	m_nBoundProductPartPaperGrammage = 0;
	m_fBoundProductPartPaperVolume = 0.0f;
	m_nBoundProductPartAmount = 0;
	m_nBoundProductPartExtra = 0;
	m_pBindingImageList = NULL;
	m_strComment = _T("");
}

CDlgBoundProductDetails::~CDlgBoundProductDetails()
{
	if (m_pBindingImageList)
		delete m_pBindingImageList;
}

void CDlgBoundProductDetails::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_CBIndex(pDX, IDC_PD_BINDING_METHOD, m_nBindingMethod);
	DDX_Control(pDX, IDC_PRODUCTPART_WIDTH_SPIN, m_boundProductPartWidthSpin);
	DDX_Control(pDX, IDC_PRODUCTPART_HEIGHT_SPIN, m_boundProductPartHeightSpin);
	DDX_Radio(pDX, IDC_PRODUCTPART_PORTRAIT, m_nBoundProductPartPageOrientation);
	DDX_Measure(pDX, IDC_PRODUCTPART_HEIGHT, m_fBoundProductPartHeight);
	DDX_Measure(pDX, IDC_PRODUCTPART_WIDTH, m_fBoundProductPartWidth);
	DDX_Text(pDX, IDC_PRODUCTPART_PAPERGRAMMAGE, m_nBoundProductPartPaperGrammage);
	DDX_Text(pDX, IDC_PRODUCTPART_PAPERVOLUME, m_fBoundProductPartPaperVolume);
	DDX_Text(pDX, IDC_PRODUCTPART_AMOUNT, m_nBoundProductPartAmount);
	DDX_Text(pDX, IDC_PRODUCTPART_EXTRA, m_nBoundProductPartExtra);
	DDX_Text(pDX, IDC_PD_COMMENT_EDIT, m_strComment);
	m_boundProductPartPaperDefsSelection.DDX_PaperDefsCombo(pDX, IDC_PRODUCTPART_PAPER, m_strBoundProductPartPaper);
	m_boundProductPartFormatSelection.DDX_PageFormatCombo(pDX, IDC_PRODUCTPART_FORMAT, m_strBoundProductPartFormatName);
	m_bindingDefsSelection.DDX_BindingDefsCombo(pDX, IDC_PD_BINDING_METHOD, m_strBindingName);
}


BEGIN_MESSAGE_MAP(CDlgBoundProductDetails, CDialog)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART_FORMAT, OnSelchangeProductpartFormat)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART_HEIGHT_SPIN, OnDeltaposProductpartHeightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART_WIDTH_SPIN, OnDeltaposProductpartWidthSpin)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART_HEIGHT, OnEnKillfocusBoundProductPartHeight)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART_WIDTH,  OnEnKillfocusBoundProductPartWidth)
	ON_BN_CLICKED(IDC_PRODUCTPART_PORTRAIT, OnProductpartPortrait)
	ON_BN_CLICKED(IDC_PRODUCTPART_LANDSCAPE, OnProductpartLandscape)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART_PAPER, OnSelchangeProductpartPaper)
	ON_BN_CLICKED(IDOK, &CDlgBoundProductDetails::OnBnClickedOk)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART_AMOUNT_SPIN, OnDeltaposODQuantitySpin)
	ON_BN_CLICKED(IDC_MODIFY_PRODUCTGROUP_FORMAT, OnBnClickedModifyProductpartFormat)
	ON_BN_CLICKED(IDC_MODIFY_PRODUCTGROUP_PAPER, OnBnClickedModifyProductpartPaper)
END_MESSAGE_MAP()


// CDlgBoundProductDetails-Meldungshandler

BOOL CDlgBoundProductDetails::OnInitDialog()
{
	CDialog::OnInitDialog();

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	((CSpinButtonCtrl*)GetDlgItem(IDC_PRODUCTPART_AMOUNT_SPIN))->SetRange(0, 1);

// binding method combo ////////////////////////////
	CComboBoxEx* pBox = (CComboBoxEx*)GetDlgItem (IDC_PD_BINDING_METHOD);
	CBitmap bitmap;
	m_pBindingImageList = new CImageList;
	m_pBindingImageList->Create(24, 12, ILC_COLOR8, 2, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_PERFECT_BOUND);
	m_pBindingImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	bitmap.LoadBitmap(IDB_SADDLE_STITCHED);
	m_pBindingImageList->Add(&bitmap, BLACK);
	pBox->SetImageList(m_pBindingImageList);
	bitmap.DeleteObject();
	m_bindingDefsSelection.InitComboBox(this, m_strBindingName, &m_nBindingMethod, IDC_PD_BINDING_METHOD);
	pBox = (CComboBoxEx*)GetDlgItem (IDC_PD_BINDING_METHOD);
	pBox->SetCurSel(m_nBindingMethod);
/////////////////////////////////////////////////
////////////////////////////////////////////////////

	m_boundProductPartFormatSelection.InitComboBox(this, m_strBoundProductPartFormatName, &m_fBoundProductPartWidth, &m_fBoundProductPartHeight, IDC_PRODUCTPART_FORMAT);
	OnSelchangeProductpartFormat();

	m_boundProductPartPaperDefsSelection.InitComboBox(this, m_strBoundProductPartPaper, &m_nBoundProductPartPaperGrammage, &m_fBoundProductPartPaperVolume, NULL, IDC_PRODUCTPART_PAPER);
	OnSelchangeProductpartPaper();

	m_strBoundProductPartPaper			= m_boundProductPart.m_strPaper;
	m_nBoundProductPartPaperGrammage 	= m_boundProductPart.m_nPaperGrammage;
	m_fBoundProductPartPaperVolume		= m_boundProductPart.m_fPaperVolume;
	m_strBoundProductPartFormatName		= m_boundProductPart.m_strFormatName;
	m_fBoundProductPartWidth			= m_boundProductPart.m_fPageWidth;
	m_fBoundProductPartHeight			= m_boundProductPart.m_fPageHeight;
	m_nBoundProductPartPageOrientation	= m_boundProductPart.m_nPageOrientation;
	m_nBoundProductPartAmount			= m_boundProductPart.m_nDesiredQuantity;
	m_nBoundProductPartExtra			= m_boundProductPart.m_nExtra;
	m_strComment						= m_boundProductPart.m_strComment;
	//m_nBindingMethod					= m_bindingDefsSelection.GetBindingDefsIndex(m_boundProductPart.m_bindingDefs);

	UpdateData(FALSE);

	KeepCurrentPageFormat();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}


void CDlgBoundProductDetails::KeepCurrentPageFormat()
{
	m_strOldFormatName    = m_strBoundProductPartFormatName;
	m_fOldWidth			  = m_fBoundProductPartWidth;
	m_fOldHeight		  = m_fBoundProductPartHeight;
	m_nOldPageOrientation = m_nBoundProductPartPageOrientation;
}

BOOL CDlgBoundProductDetails::PageFormatChanged()
{
	if ( (m_strOldFormatName != m_strBoundProductPartFormatName) || (fabs(m_fOldWidth - m_fBoundProductPartWidth) > 0.001) || (fabs(m_fOldHeight - m_fBoundProductPartHeight) > 0.001) || (m_nOldPageOrientation != m_nBoundProductPartPageOrientation) )
		return TRUE;
	else
		return FALSE;
}

void CDlgBoundProductDetails::OnBnClickedOk()
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	BOOL bReject = FALSE;
	if (PageFormatChanged())
		if (AfxMessageBox(IDS_MODIFY_PRODUCTPART_FORMAT, MB_YESNO) == IDNO)
			bReject = TRUE;

	if (bReject)
	{
		m_boundProductPart.m_strFormatName		= m_strOldFormatName;
		m_boundProductPart.m_fPageWidth			= m_fOldWidth;	
		m_boundProductPart.m_fPageHeight		= m_fOldHeight;					
		m_boundProductPart.m_nPageOrientation	= m_nOldPageOrientation;
	}
	else
	{
		if (PageFormatChanged())
			m_boundProductPart.GetBoundProduct(m_nBoundProductPartIndex).ChangePageFormat(m_strBoundProductPartFormatName, m_fBoundProductPartWidth, m_fBoundProductPartHeight, m_nBoundProductPartPageOrientation, m_nBoundProductPartIndex);
		m_boundProductPart.m_strFormatName		= m_strBoundProductPartFormatName;
		m_boundProductPart.m_fPageWidth			= m_fBoundProductPartWidth;
		m_boundProductPart.m_fPageHeight		= m_fBoundProductPartHeight;
		m_boundProductPart.m_nPageOrientation	= m_nBoundProductPartPageOrientation;
	}

	m_boundProductPart.m_strPaper				= m_strBoundProductPartPaper;
	m_boundProductPart.m_nPaperGrammage			= m_nBoundProductPartPaperGrammage;
	m_boundProductPart.m_fPaperVolume			= m_fBoundProductPartPaperVolume;
	m_boundProductPart.m_nDesiredQuantity				= m_nBoundProductPartAmount;
	m_boundProductPart.m_nExtra					= m_nBoundProductPartExtra;
	m_boundProductPart.m_strComment				= m_strComment;
	//m_boundProductPart.m_bindingDefs			= m_bindingDefsSelection.GetBindingDefs(m_nBindingMethod);
	
	OnOK();
}

void CDlgBoundProductDetails::OnSelchangeProductpartFormat() 
{
	UpdateData(TRUE);
	m_boundProductPartFormatSelection.SelchangeFormatList(this, &m_fBoundProductPartWidth, &m_fBoundProductPartHeight, m_strBoundProductPartFormatName, &m_nBoundProductPartPageOrientation, IDC_PRODUCTPART_FORMAT);
}

void CDlgBoundProductDetails::OnDeltaposProductpartHeightSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_boundProductPartHeightSpin.GetBuddy(), pNMUpDown->iDelta);
	
	UpdateData(TRUE);
	m_strBoundProductPartFormatName.LoadString(IDS_USERDEFINED);
	UpdateData(FALSE);

	*pResult = 0;
}

void CDlgBoundProductDetails::OnDeltaposProductpartWidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_boundProductPartWidthSpin.GetBuddy(), pNMUpDown->iDelta);
	
	UpdateData(TRUE);
	m_strBoundProductPartFormatName.LoadString(IDS_USERDEFINED);
	UpdateData(FALSE);

	*pResult = 0;
}

void CDlgBoundProductDetails::OnEnKillfocusBoundProductPartHeight()
{
	float fOldValue = m_fBoundProductPartHeight;
	UpdateData(TRUE);
	if (fOldValue != m_fBoundProductPartHeight)
	{
		m_strBoundProductPartFormatName.LoadString(IDS_USERDEFINED);
		UpdateData(FALSE);
	}
}

void CDlgBoundProductDetails::OnEnKillfocusBoundProductPartWidth()
{
	float fOldValue = m_fBoundProductPartWidth;
	UpdateData(TRUE);
	if (fOldValue != m_fBoundProductPartWidth)
	{
		m_strBoundProductPartFormatName.LoadString(IDS_USERDEFINED);
		UpdateData(FALSE);
	}
}

void CDlgBoundProductDetails::OnProductpartPortrait() 
{
	if (m_nBoundProductPartPageOrientation == PORTRAIT)	// already portrait
		return;
	m_nBoundProductPartPageOrientation = PORTRAIT;		// switch to portrait
	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_PRODUCTPART_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_PRODUCTPART_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);
}

void CDlgBoundProductDetails::OnBnClickedModifyProductpartFormat()
{
	UpdateData(TRUE);

	CDlgPageFormats dlg;
	dlg.m_strFormatName = m_strBoundProductPartFormatName;
	dlg.DoModal();

	m_strBoundProductPartFormatName = dlg.m_strFormatName;
	m_boundProductPartFormatSelection.InitComboBox(this, m_strBoundProductPartFormatName, &m_fBoundProductPartWidth, &m_fBoundProductPartHeight, IDC_PRODUCTPART_FORMAT);
	OnSelchangeProductpartFormat();
}

void CDlgBoundProductDetails::OnBnClickedModifyProductpartPaper()
{
	UpdateData(TRUE);

	CDlgPaperDefs dlg;
	dlg.m_strPaperName = m_strBoundProductPartPaper;
	dlg.DoModal();

	m_strBoundProductPartPaper = dlg.m_strPaperName;
	m_boundProductPartPaperDefsSelection.InitComboBox(this, m_strBoundProductPartPaper, &m_nBoundProductPartPaperGrammage, &m_fBoundProductPartPaperVolume, NULL, IDC_PRODUCTPART_PAPER);
	OnSelchangeProductpartPaper();
}

void CDlgBoundProductDetails::OnProductpartLandscape() 
{
	if (m_nBoundProductPartPageOrientation == LANDSCAPE)	// already portrait
		return;
	m_nBoundProductPartPageOrientation = LANDSCAPE;		// switch to portrait
	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_PRODUCTPART_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_PRODUCTPART_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);
}

void CDlgBoundProductDetails::OnSelchangeProductpartPaper() 
{
	UpdateData(TRUE);
	m_boundProductPartPaperDefsSelection.SelchangeFormatList(this, &m_nBoundProductPartPaperGrammage, &m_fBoundProductPartPaperVolume, NULL, m_strBoundProductPartPaper, IDC_PRODUCTPART_PAPER);
}

void CDlgBoundProductDetails::OnDeltaposODQuantitySpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	m_nBoundProductPartAmount += pNMUpDown->iDelta * 1000;
	if (m_nBoundProductPartAmount < 0)
		m_nBoundProductPartAmount = 0;
	CString strText = _T("0");
	if (m_nBoundProductPartAmount)
		strText.Format(_T("%d"), m_nBoundProductPartAmount);
	GetDlgItem(IDC_PRODUCTPART_AMOUNT)->SetWindowText(strText);
}
