#pragma once


// CMyBitmapButton

class CMyBitmapButton : public CButton
{
	DECLARE_DYNAMIC(CMyBitmapButton)

public:
	CMyBitmapButton();
	virtual ~CMyBitmapButton();

public:
	CFont	m_font;
	CBrush	m_bkBrush;
	BOOL	m_bMouseOverButton;
	LPARAM	m_lParam;
	CString m_strTheme;
	int		m_nPartID;
	int		m_nImgIDHot;
	int		m_nImgIDCold;
	int		m_nImgIDDisabled;
	int		m_nBitmapPosition;
	BOOL	m_bTransparent;

public:
	void		SetTheme(CString strTheme, int nPartID, int nPosition = RIGHT);
	void		SetBitmap(int nIDBitmapHot, int nIDBitmapCold = -1, int nIDBitmapDisabled = -1, int nPosition = RIGHT);
	void		SetTransparent(BOOL bTransparent);
	void		AlignToBitmap();
	static void ThemeButton(CDC* pDC, CRect rcItem, BOOL bMouseOverButton, int nState, CWnd* pParentWnd);
	static void ThemeBitmap(CDC* pDC, CRect rcItem, BOOL bMouseOverButton, int nState, CWnd* pParentWnd, CString strTheme, int nPartID, int nImgIDHot, int nImgIDCold = -1, int nImgIDDisabled = -1, int nBitmapPosition = RIGHT);


protected:
	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg HBRUSH CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/);
};


