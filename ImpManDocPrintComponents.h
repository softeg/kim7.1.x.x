// ImpManDocPrintComponentsList.h : interface of the CImpManDoc class
//
/////////////////////////////////////////////////////////////////////////////


#pragma once


struct PrintComponentSheetRef
{
	int m_nPrintSheetIndex;
	int m_nQuantity;
};

/////////////////////////// CPrintComponent ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class CPrintComponent : public CObject
{
public:
    DECLARE_SERIAL( CPrintComponent )
	CPrintComponent();
	CPrintComponent(const CPrintComponent& rPrintComponent); // copy constructor


public:
	enum Type {PageSection, FoldSheet, Flat};


private:
	int	m_nIndex;

public:
	int	m_nType;
	union
	{
		int m_nProductPartIndex;
		int m_nFlatProductIndex;
	};
	int m_nNumPages;
	int m_nFoldSheetIndex;
	int	m_nFoldSheetLayerIndex;

	int m_nQuantityDesired;
	int m_nQuantityPrinted;
	CArray <PrintComponentSheetRef, PrintComponentSheetRef&> m_sheetRefs;

public:
	const					CPrintComponent& operator= (const CPrintComponent& rPrintComponent);
	void					Copy(const CPrintComponent& rPrintComponent);
	BOOL					operator==(const CPrintComponent& rPrintComponent);
	BOOL					operator!=(const CPrintComponent& rPrintComponent);
	BOOL					IsEquivalent(const CPrintComponent& rPrintComponent);
	BOOL					IsNull() { return (m_nType == -1) ? TRUE : FALSE;};
	BOOL					IsPlaced();
	void					SetIndex(int nIndex) { m_nIndex = nIndex; };
	int						GetIndex() { return m_nIndex; };
	CPrintComponentsGroup&	GetPrintComponentsGroup();
	CPrintingProfile&		GetFirstPrintingProfile();
	BOOL					IsSuitablePrintGroup(CPrintGroup& rPrintGroup);
	CPrintSheet*			GetSuitablePrintSheet();
	CPrintSheet*			GetSuitableEmptyPrintSheet();
	CFoldSheet*				GetFoldSheet();
	CFoldSheetLayer*		GetFoldSheetLayer();
	CFlatProduct*			GetFlatProduct();
	CBoundProduct*			GetBoundProduct();
	CBoundProductPart*		GetBoundProductPart();
	CPrintSheet*			GetFirstPrintSheet();
	CPrintSheet*			GetNextPrintSheet(CPrintSheet* pPrintSheet);
	CPrintSheet*			GetLastPrintSheet();
	void					GetCutSize(float& fWidth, float& fHeight);
	int						GetQuantityDiff();

	CRect					DrawInfo		 (CDC* pDC, CRect rcFrame, int nSheetRefIndex, BOOL bDrawThumbnail,	BOOL bDrawProductHeader, BOOL bShowUnprinted, BOOL bShowPrintedOnly, CDisplayList* pDisplayList);
	CRect					DrawPrintSheetLoc(CDC* pDC, CRect rcFrame, int nSheetRefIndex, BOOL bDrawThumbnail,	BOOL bDrawProductHeader, BOOL bShowUnprinted, BOOL bShowPrintedOnly, CDisplayList* pDisplayList);
	CRect					DrawQuantity	 (CDC* pDC, CRect rcFrame, int nSheetRefIndex, BOOL bDrawThumbnail,	BOOL bDrawProductHeader, BOOL bShowUnprinted, BOOL bShowPrintedOnly, CDisplayList* pDisplayList);
	CRect					DrawQuantityDiff (CDC* pDC, CRect rcFrame, int nSheetRefIndex, BOOL bDrawThumbnail,	BOOL bDrawProductHeader, BOOL bShowUnprinted, BOOL bShowPrintedOnly, CDisplayList* pDisplayList);
};

template <> void AFXAPI SerializeElements <CPrintComponent> (CArchive& ar, CPrintComponent* pPrintComponent, INT_PTR nCount);


/////////////////////////// CPrintComponentsGroup ///////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CPrintComponentsGroup : public CObject
{
public:
    DECLARE_SERIAL( CPrintComponentsGroup )
	CPrintComponentsGroup();
	CPrintComponentsGroup(const CPrintComponentsGroup& rPrintComponentsGroup); // copy constructor


private:
	CRect										m_rcDisplayBoxLong, m_rcDisplayBox;

public:
	CString										m_strPaper;
	CColorantList								m_colorantList;
	int											m_nMinQuantity, m_nMaxQuantity;
	COleDateTime								m_dueDate;
	CString										m_strPressSetup;
	CArray <CPrintComponent, CPrintComponent&>	m_printComponents;

public:
	const	CPrintComponentsGroup& operator= (const CPrintComponentsGroup& rPrintComponentsGroup);
	void	Copy(const CPrintComponentsGroup& rPrintComponentsGroup);
	BOOL	IsNull() { return (m_printComponents.GetSize() <= 0) ? TRUE : FALSE; };
	int		GetIndex();
	CRect	GetDisplayBoxLong()				{ return m_rcDisplayBoxLong;  };
	CRect	GetDisplayBox()					{ return m_rcDisplayBox;	  };
	void	SetDisplayBoxLong(CRect& rcBox) { m_rcDisplayBoxLong = rcBox; };
	void	SetDisplayBox	 (CRect& rcBox) { m_rcDisplayBox	 = rcBox; };
	void	SetQuantityRange(int nQuantity);
	BOOL	CheckProductionProfile(CPrintComponentsGroup& rPrintComponentsGroup, CProductionSetup& rProductionSetup);
	BOOL	CheckInsideQuantityRange(CPrintComponentsGroup& rPrintComponentsGroup, CProductionSetup& rProductionSetup);
	BOOL	CheckInsideColorRange(CPrintComponentsGroup& rPrintComponentsGroup, CProductionSetup& rProductionSetup);
	void	AddFoldSheetComponent(CBoundProductPart& rProductPart, CFoldSheet* pFoldSheet, int nFoldSheetLayerIndex);
	void	AddPageSectionComponent(CBoundProductPart& rProductPart, int nNumPages);
	void	AddFlatProductComponent(CFlatProduct& rFlatProduct);
	BOOL	HasPrinted();
	BOOL	HasUnprinted();
	CRect	DrawHeader(CDC* pDC, CRect rcFrame, BOOL bEnabled, BOOL bLongVersion, BOOL bShowBackground, CDisplayList* pDisplayList);
	CRect	DrawColorInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	int		GetNumFoldSheets(CFoldSheet* pRefFoldSheet);
};

template <> void AFXAPI SerializeElements <CPrintComponentsGroup> (CArchive& ar, CPrintComponentsGroup* pPrintComponentsGroup, INT_PTR nCount);



/////////////////////////// CPrintComponentsList ///////////////////////
/////////////////////////////////////////////////////////////////////////////

class CPrintComponentsList : public CArray <CPrintComponentsGroup, CPrintComponentsGroup&> 
{
public:
	CPrintComponentsList();
	CPrintComponentsList(const CPrintComponentsList& rPrintComponentsGroupList); // copy constructor


public:
	const					CPrintComponentsList& operator=(const CPrintComponentsList& rPrintComponentsGroupList);
	void					Copy(const CPrintComponentsList& rPrintComponentsGroupList);
	void					Analyze();
	void					AddFindComponentsGroup(CPrintComponentsGroup& rComponentsGroup);
	CPrintComponent 		GetComponent(int nComponentIndex);
	CPrintComponent 		FindFoldSheetLayerComponent(int nFoldSheetIndex, int nFoldSheetLayerIndex);
	CPrintComponent 		FindPageSectionComponent(int nProductPartIndex);
	CPrintComponent 		FindFlatComponent(int nFlatProductIndex);
	CPrintComponent 		FindPrintSheet(CPrintSheet* pPrintSheet);
	CPrintComponentsGroup&	FindGroup(CPrintComponent& rComponent);
	int						GetNumComponentsAll();
};
