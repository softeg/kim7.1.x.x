// DlgLicenseManager.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgLicenseManager.h"


// CDlgLicenseManager-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgLicenseManager, CDialog)

CDlgLicenseManager::CDlgLicenseManager(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgLicenseManager::IDD, pParent)
	, m_strProgrammVersion(_T(""))
{
	m_boldFont.CreateFont (13, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_bkBrushGray.CreateSolidBrush(RGB(250,250,250));
	m_bkBrushRed.CreateSolidBrush(RGB(255,165,165));
	m_bkBrushYellow.CreateSolidBrush(RGB(255,245,175));
	m_bkBrushGreen.CreateSolidBrush(RGB(210,245,85));
}

CDlgLicenseManager::~CDlgLicenseManager()
{
	m_boldFont.DeleteObject();
	m_bkBrushGray.DeleteObject();
	m_bkBrushRed.DeleteObject();
	m_bkBrushYellow.DeleteObject();
	m_bkBrushGreen.DeleteObject();
}

void CDlgLicenseManager::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_LM_VERSION_STATIC, m_strProgrammVersion);
	DDX_Text(pDX, IDC_LM_PDFSHEETOUT_STATIC, m_strPDFSheetOutStatus);
	DDX_Text(pDX, IDC_LM_JDFMIS_STATIC, m_strJDFMisAddOnStatus);
	DDX_Text(pDX, IDC_LM_JDFIMPO_STATIC, m_strJDFImpoAddOnStatus);
	DDX_Text(pDX, IDC_LM_JDFCUT_STATIC, m_strJDFCutAddOnStatus);
	DDX_Text(pDX, IDC_LM_JDFFOLD_STATIC, m_strJDFFoldAddOnStatus);
	DDX_Text(pDX, IDC_LM_BARCODE_STATIC, m_strBarcodeAddOnStatus);
	DDX_Text(pDX, IDC_LM_OPTIMIZE_STATIC, m_strOptimizeAddOnStatus);
}


BEGIN_MESSAGE_MAP(CDlgLicenseManager, CDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_LM_GETLICENSE_BUTTON, &CDlgLicenseManager::OnBnClickedLmGetlicenseButton)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CDlgLicenseManager-Meldungshandler

BOOL CDlgLicenseManager::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitData();

	SetTimer(1, 2000, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgLicenseManager::InitData()
{
	CString strStatusDemo, strStatusAvailable, strStatusNotAvailable;
	strStatusDemo.LoadString(IDS_LICENSE_STATUS_DEMO);
	strStatusAvailable.LoadString(IDS_LICENSE_STATUS_ENABLED);
	strStatusNotAvailable.LoadString(IDS_LICENSE_STATUS_DISABLED);
	m_strProgrammVersion	= theApp.BuildKIMTitle(_T(""));
	if (theApp.m_nDongleStatus == CImpManApp::ClientDongle)
	{
		m_strPDFSheetOutStatus	= _T(" ") + strStatusNotAvailable;
		m_strJDFImpoAddOnStatus = _T(" ") + strStatusNotAvailable;
		m_strJDFCutAddOnStatus	= _T(" ") + strStatusNotAvailable;
		m_strJDFFoldAddOnStatus	= _T(" ") + strStatusNotAvailable;
	}
	else
	{
		m_strPDFSheetOutStatus	= _T(" ") + ( ( (theApp.m_nDongleStatus == CImpManApp::NoDongle) || (theApp.m_nDongleStatus == CImpManApp::ClientDongle) ) ? strStatusDemo : strStatusAvailable );
		m_strJDFImpoAddOnStatus = _T(" ") + ( (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp)		? strStatusAvailable : strStatusDemo );
		m_strJDFCutAddOnStatus	= _T(" ") + ( (theApp.m_nAddOnsLicensed & CImpManApp::JDFCutting)		? strStatusAvailable : strStatusDemo );
		m_strJDFFoldAddOnStatus	= _T(" ") + ( (theApp.m_nAddOnsLicensed & CImpManApp::JDFFolding)		? strStatusAvailable : strStatusNotAvailable );
	}
	m_strJDFMisAddOnStatus	 = _T(" ") + ( (theApp.m_nAddOnsLicensed & CImpManApp::JDFMIS)   ? strStatusAvailable : strStatusDemo );
	m_strBarcodeAddOnStatus	 = _T(" ") + ( (theApp.m_nAddOnsLicensed & CImpManApp::Barcode)  ? strStatusAvailable : strStatusDemo );
	m_strOptimizeAddOnStatus = _T(" ") + ( (theApp.m_nAddOnsLicensed & CImpManApp::Optimize) ? strStatusAvailable : strStatusDemo );

	UpdateData(FALSE);
}

void CDlgLicenseManager::OnTimer(UINT_PTR nIDEvent)
{
	InitData();

	CDialog::OnTimer(nIDEvent);
}

void CDlgLicenseManager::OnBnClickedLmGetlicenseButton()
{
	CString strPath;
	strPath = theApp.GetDefaultInstallationFolder();
	strPath += _T("\\Sentinel_Setup\\FieldExUtil.exe");;
	_tspawnl(_P_NOWAIT, strPath, _T("FieldExUtil.exe"), _T(""), NULL);	
}

HBRUSH CDlgLicenseManager::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

    if(nCtlColor == CTLCOLOR_STATIC)
    {
		pDC->SetBkMode(TRANSPARENT);
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_LM_VERSION_STATIC:	
					pDC->SelectObject(&m_boldFont);
					hbr = (HBRUSH)m_bkBrushGray.GetSafeHandle();	
					break;
		case IDC_LM_PDFSHEETOUT_STATIC:	
					if (theApp.m_nDongleStatus == CImpManApp::NoDongle)
						hbr = (HBRUSH)m_bkBrushYellow.GetSafeHandle();
					else
						if (theApp.m_nDongleStatus == CImpManApp::ClientDongle)
							hbr = (HBRUSH)m_bkBrushRed.GetSafeHandle();	
						else
							hbr = (HBRUSH)m_bkBrushGreen.GetSafeHandle();	
					break;
		case IDC_LM_JDFMIS_STATIC:
					if (theApp.m_nAddOnsLicensed & CImpManApp::JDFMIS)
						hbr = (HBRUSH)m_bkBrushGreen.GetSafeHandle();	
					else
						hbr = (HBRUSH)m_bkBrushYellow.GetSafeHandle();	
					break;
		case IDC_LM_JDFIMPO_STATIC:
					if (theApp.m_nDongleStatus == CImpManApp::ClientDongle)
						hbr = (HBRUSH)m_bkBrushRed.GetSafeHandle();	
					else
						if (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp)
							hbr = (HBRUSH)m_bkBrushGreen.GetSafeHandle();	
						else
							hbr = (HBRUSH)m_bkBrushYellow.GetSafeHandle();	
					break;
		case IDC_LM_JDFCUT_STATIC:
					if (theApp.m_nDongleStatus == CImpManApp::ClientDongle)
						hbr = (HBRUSH)m_bkBrushRed.GetSafeHandle();	
					else
						if (theApp.m_nAddOnsLicensed & CImpManApp::JDFCutting)
							hbr = (HBRUSH)m_bkBrushGreen.GetSafeHandle();	
						else
							hbr = (HBRUSH)m_bkBrushYellow.GetSafeHandle();	
					break;
		case IDC_LM_JDFFOLD_STATIC:
					if (theApp.m_nDongleStatus == CImpManApp::ClientDongle)
						hbr = (HBRUSH)m_bkBrushRed.GetSafeHandle();	
					else
						if (theApp.m_nAddOnsLicensed & CImpManApp::JDFFolding)
							hbr = (HBRUSH)m_bkBrushGreen.GetSafeHandle();	
						else
							hbr = (HBRUSH)m_bkBrushRed.GetSafeHandle();	
					break;
		case IDC_LM_BARCODE_STATIC:
					if (theApp.m_nAddOnsLicensed & CImpManApp::Barcode)
						hbr = (HBRUSH)m_bkBrushGreen.GetSafeHandle();	
					else
						hbr = (HBRUSH)m_bkBrushYellow.GetSafeHandle();	
					break;
		case IDC_LM_OPTIMIZE_STATIC:
					if (theApp.m_nAddOnsLicensed & CImpManApp::Optimize)
						hbr = (HBRUSH)m_bkBrushGreen.GetSafeHandle();	
					else
						hbr = (HBRUSH)m_bkBrushYellow.GetSafeHandle();	
					break;
		default:	break;
		}
    }

	return hbr;
}
