//{{AFX_INCLUDES()
//}}AFX_INCLUDES
#if !defined(AFX_DLGSECTIONMARK_H__2EEA8B13_63D7_11D3_8712_204C4F4F5020__INCLUDED_)
#define AFX_DLGSECTIONMARK_H__2EEA8B13_63D7_11D3_8712_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSectionMark.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSectionMark 

class CDlgSectionMark : public CDialog
{
// Konstruktion
public:
	CDlgSectionMark(CWnd* pParent = NULL);   // Standardkonstruktor

#ifdef PDF
	CDlgSelectSectionMark m_dlgSelectSectionMark;
#endif
	CBitmap				  m_bmSectionMarkType;
	int					  m_nIDSectionMarkType;


// Dialogfelddaten
	//{{AFX_DATA(CDlgSectionMark)
	enum { IDD = IDD_SECTION_MARK };
	CStatic	m_sectionMarkTypeBitmap;
	CSpinButtonCtrl	m_barWidthSpin;
	CSpinButtonCtrl	m_barHeightSpin;
	CSpinButtonCtrl	m_headMarginSpin;
	CSpinButtonCtrl	m_footMarginSpin;
	float	m_fFootMargin;
	float	m_fHeadMargin;
	float	m_fSafetyDistance;
	float	m_fBarHeight;
	float	m_fBarWidth;
	//}}AFX_DATA

friend class CPrintSheetMarksView;	// give access to protected members
friend class CDlgSelectSectionMark;	// give access to protected members

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgSectionMark)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementierung
protected:
	void ResetDialog();
	void LoadMarkIntoDialog(CSectionMark* pMark);
	void SaveDialogToMark(CSectionMark* pMark);
	void UpdateSectionMarkType(int nIDSectionMarkType);

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgSectionMark)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSectionmarkFootmarginSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSectionmarkHeadmarginSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSectionmarkHeightspin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSectionmarkWidthspin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusSectionmarkFootmargin();
	afx_msg void OnKillfocusSectionmarkHeadmargin();
	afx_msg void OnKillfocusSectionmarkHeight();
	afx_msg void OnKillfocusSectionmarkWidth();
	afx_msg void OnSelectSectionMark();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGSECTIONMARK_H__2EEA8B13_63D7_11D3_8712_204C4F4F5020__INCLUDED_
