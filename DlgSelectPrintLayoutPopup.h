#pragma once


typedef struct
{
	CPrintSheet* pPrintSheet;
	CLayout*	 pLayout;
}PRINTLAYOUT;


// CDlgSelectPrintLayoutPopup-Dialogfeld

class CDlgSelectPrintLayoutPopup : public CDialog
{
	DECLARE_DYNAMIC(CDlgSelectPrintLayoutPopup)

public:
	CDlgSelectPrintLayoutPopup(CWnd* pParent = NULL, BOOL bRunModal = FALSE);   // Standardkonstruktor
	virtual ~CDlgSelectPrintLayoutPopup();

// Dialogfelddaten
	enum { IDD = IDD_SELECTPRINTLAYOUTPOPUP};

public:
	CFont								m_smallFont;
	CWnd*								m_pParent;
	CFoldSheet							m_foldSheet;
	CArray<PRINTLAYOUT, PRINTLAYOUT>	m_sampleList;
	int									m_nNup;
	CPoint								m_ptInitialPos;

protected:
	CComboBox		m_nUpCombo, m_printTypeCombo;
	int				m_nPrintingType;
	CImageList*		m_pPrintLayoutImageList;
	CListCtrl		m_printLayoutList;
	BOOL			m_bLocked;
	BOOL			m_bRunModal;
	BOOL			m_bAllowRotation;
	BOOL			m_bShowStandards;


public:
	void 			InitNUp(BOOL bInitialize = FALSE);
	void 			InitPrintLayoutList();
	void 			InitStandardPrintLayoutList();
	CPrintSheet*	GetActPrintSheet();
	CLayout*		GetActLayout();
	CFoldSheet*		GetActFoldSheet();
	BOOL			GetActProductPartIndex();
	CLayout*		GetSelectedLayout();
	CPrintSheet*	GetSelectedPrintSheet();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnCbnSelchangeNUpCombo();
	afx_msg void OnCbnSelchangePrintTypeCombo();
	afx_msg void OnNMCustomdrawPrintLayoutlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedPrintLayoutlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedPdAllowRotation();
	afx_msg void OnBnClickedPdShowStandards();
	afx_msg void OnDestroy();
};
