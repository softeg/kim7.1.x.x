#if !defined(AFX_DLGPDFENGINESETTINGS_H__14BEC6A1_563E_11D4_87A9_0040F68C1EF7__INCLUDED_)
#define AFX_DLGPDFENGINESETTINGS_H__14BEC6A1_563E_11D4_87A9_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPDFEngineSettings.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFEngineSettings 

class CDlgPDFEngineSettings : public CDialog
{
// Konstruktion
public:
	CDlgPDFEngineSettings(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgPDFEngineSettings)
	enum { IDD = IDD_PDF_ENGINE_SETTINGS };
	BOOL	m_bAutoAssign;
	BOOL	m_bCreatePreviews;
	CString	m_strResolution;
	CString	m_strDistillerSettings;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPDFEngineSettings)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	void InitDistillerSettingsCombo();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPDFEngineSettings)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnPdflibStatusCreatePreviews();
	afx_msg void OnChangeDistSettings();
	afx_msg void OnSelchangeDistSettingsCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPDFENGINESETTINGS_H__14BEC6A1_563E_11D4_87A9_0040F68C1EF7__INCLUDED_
