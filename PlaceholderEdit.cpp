// PlaceholderEdit.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h" 
#include "PlaceholderEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPlaceholderEdit

CPlaceholderEdit::CPlaceholderEdit()
{
	m_pPlaceholderMenu	 = NULL;
	m_pParent			 = NULL;
	m_pToolTip			 = NULL;
	m_nIDButton			 = -1;
	m_pfnOnChangeHandler = NULL;
}

CPlaceholderEdit::~CPlaceholderEdit()
{
	delete m_pToolTip;
}


BEGIN_MESSAGE_MAP(CPlaceholderEdit, CEdit)
	//{{AFX_MSG_MAP(CPlaceholderEdit)
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_CONTROL_REFLECT(EN_CHANGE, OnChange)
	ON_WM_LBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_CHAR()
	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CPlaceholderEdit::IsValidPlaceholder(CString strToCheck)
{
	if ( ! m_pPlaceholderMenu)
		return FALSE;

	CMenu* pPopup = m_pPlaceholderMenu->GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if ( ! pPopup)
		return FALSE;

	CString strPlaceholder;
	strToCheck.Insert(1, '&');
	for (UINT i = 0; i < pPopup->GetMenuItemCount(); i++)
	{
		pPopup->GetMenuString(i, strPlaceholder, MF_BYPOSITION);
		if (strToCheck == strPlaceholder.Left(3))
			return TRUE;
	}

	return FALSE;
}

CString CPlaceholderEdit::GetPlaceholderHit()
{ 
	CString strText;     
	GetWindowText(strText);     

	CPoint cursorPos;
    ::GetCursorPos(&cursorPos);
    ScreenToClient(&cursorPos);
	int nPos = LOWORD(CharFromPos(cursorPos));
	if ( (nPos < 0) || (nPos >= strText.GetLength()) )
		return "";
	
	CString strPlaceholder = strText.Mid(nPos-2, 4);
	int nIndex = strPlaceholder.Find('$');
	if (nIndex != -1)
		strPlaceholder = strPlaceholder.Mid(nIndex, 2);
	else
		strPlaceholder.Empty();

	if (strPlaceholder.GetLength() == 2)
		return strPlaceholder;
	else
		return "";
}


/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CPlaceholderEdit 

void CPlaceholderEdit::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

// Set the clipping region for the drawing // 
	RECT rect; 
	GetClientRect(&rect); 
	CRgn rgn; 
	rgn.CreateRectRgnIndirect(&rect);  
	dc.SelectClipRgn(&rgn);  
// Erase the drawing area // 
	dc.FillSolidRect(&rect, WHITE);  
// Redisplay the text //     
	CString strText;     
	GetWindowText(strText);    

	dc.SelectObject(GetFont());

	rect.top++;     
	rect.left++; 

	CString strPlaceholder;
	TCHAR *pszEnd, *pszStart;
	TCHAR buffer[256];
	_tcsncpy(buffer, (LPCTSTR)strText, 255);
	pszStart = buffer;
	while ((pszEnd = _tcschr(pszStart, '$')) != NULL)
	{
		*pszEnd = '\0';
		strPlaceholder.Format(_T("$%c"), *(pszEnd + 1));
		if (IsValidPlaceholder(strPlaceholder))
		{
			if (IsExtSyntax(*(pszEnd + 2), *(pszEnd + 3), strPlaceholder))
				strPlaceholder += CString(*(pszEnd + 2)) + GotoNext(&pszStart, &pszEnd);
			else
				pszStart = pszEnd + 2;

			CSize textExtentPlaceholder = dc.GetTextExtent(strPlaceholder);
			CPoint ptLeft = PosFromChar(pszEnd - buffer);
			dc.FillSolidRect(CRect(ptLeft.x-1, ptLeft.y+1, ptLeft.x + textExtentPlaceholder.cx+1, ptLeft.y + textExtentPlaceholder.cy+1), RGB(255,200,200));
		}
		else
			pszStart = pszEnd + 2;
		*pszEnd = '$';
	}
	dc.SetBkMode(TRANSPARENT);
	dc.SetBkColor(WHITE);
	dc.DrawText(strText, CRect(4,1,rect.right, rect.bottom), DT_TOP | DT_LEFT | DT_SINGLELINE);  

	int nStartChar, nEndChar;
	GetSel(nStartChar, nEndChar);
	if (nEndChar > nStartChar)
		if (this == GetFocus())
		{
			CSize textExtent = dc.GetTextExtent(strText);
			CPoint ptLeft  = PosFromChar(nStartChar);
			CPoint ptRight = PosFromChar(nEndChar);
			if (ptRight.x <= 0) 
				ptRight.x = textExtent.cx;
			CRect selectionRect(ptLeft.x-1, ptLeft.y+1, ptRight.x+4, ptLeft.y + textExtent.cy+1);
			selectionRect.right++;
			dc.InvertRect(selectionRect);  
		}

// Kein Aufruf von CEdit::OnPaint() f�r Zeichnungsnachrichten
}

BOOL CPlaceholderEdit::IsExtSyntax(TCHAR ch1, TCHAR ch2, CString strPlaceholder)
{
	if ( (ch1 == '_') && isdigit(ch2) )
	{
		if ( (strPlaceholder == _T("$1")) || (strPlaceholder == _T("$4")) || (strPlaceholder == _T("$6"))  || (strPlaceholder == _T("$P")) )
			return TRUE;
	}

	if ( (ch1 == ':') && isdigit(ch2) )
	{
		if ( (strPlaceholder == _T("$2")) || (strPlaceholder == _T("$3")) || (strPlaceholder == _T("$7")) || (strPlaceholder == _T("$F")) || (strPlaceholder == _T("$G")) || (strPlaceholder == _T("$H")) ||(strPlaceholder == _T("$I")) || 
			 (strPlaceholder == _T("$J")) || (strPlaceholder == _T("$O")) || (strPlaceholder == _T("$Q")) )
			return TRUE;
	}

	if ( (ch1 == '%') && isdigit(ch2) )
	{
		if ( (strPlaceholder == _T("$1")) || (strPlaceholder == _T("$Q")) || (strPlaceholder == _T("$a")) )
			return TRUE;
	}

	if ( (ch1 == '_') && (ch2 == 'S') )
	{
		if (strPlaceholder == _T("$2"))
			return TRUE;
	}

	if ( (ch1 == '_') && (ch2 == '=') )
	{
		if (strPlaceholder == _T("$2"))
			return TRUE;
	}

	if ( (ch1 == '_') && (ch2 == 'N') )
	{
		if (strPlaceholder == _T("$P"))
			return TRUE;
	}

	return FALSE;
}

CString CPlaceholderEdit::GotoNext(TCHAR **ppStart, TCHAR **ppEnd)
{
	TCHAR str[6];
		
	str[0] = *(*ppEnd + 3); str[1] = *(*ppEnd + 4); str[2] = *(*ppEnd + 5); str[3] = '\0';
	if (!isdigit(*(*ppEnd + 4)))
	{
		str[1] = '\0';
		*ppStart = *ppEnd + 4;
	}
	else
	if (!isdigit(*(*ppEnd + 5)))
	{
		str[2] = '\0';
		*ppStart = *ppEnd + 5;
	}
	else
		*ppStart = *ppEnd + 6;

	return CString(str);
}

void CPlaceholderEdit::OnSetFocus(CWnd* pOldWnd) 
{
	CEdit::OnSetFocus(pOldWnd);
	RedrawWindow();
}

void CPlaceholderEdit::OnKillFocus(CWnd* pNewWnd) 
{
	CEdit::OnKillFocus(pNewWnd);
	RedrawWindow();
}

void CPlaceholderEdit::OnChange() 
{
	RedrawWindow();

	if (m_pfnOnChangeHandler)
		m_pfnOnChangeHandler((CDialog*)m_pParent);
}

void CPlaceholderEdit::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CEdit::OnLButtonDown(nFlags, point);
	RedrawWindow();
}

void CPlaceholderEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
	RedrawWindow();
}

void CPlaceholderEdit::PreSubclassWindow() 
{
	m_pToolTip = new CToolTipCtrl;
    m_pToolTip->Create(this, TTS_ALWAYSTIP);
	m_pToolTip->AddTool(this, LPSTR_TEXTCALLBACK);
	m_pToolTip->SetDelayTime(TTDT_INITIAL, 0);
	m_pToolTip->SetDelayTime(TTDT_RESHOW, 0);
	m_pToolTip->Activate(TRUE);
	
	CEdit::PreSubclassWindow();
}

BOOL CPlaceholderEdit::PreTranslateMessage(MSG* pMsg) 
{
	if (NULL != m_pToolTip)            
	{
        switch(pMsg->message)
        {
        case WM_LBUTTONDOWN:    
        case WM_MOUSEMOVE:
        case WM_LBUTTONUP:    
        case WM_RBUTTONDOWN:
        case WM_MBUTTONDOWN:    
        case WM_RBUTTONUP:
        case WM_MBUTTONUP:   m_pToolTip->RelayEvent(pMsg);	break;
        }
	}
	
	return CEdit::PreTranslateMessage(pMsg);
}

void CPlaceholderEdit::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (NULL != m_pToolTip)            
		if (::IsWindow(m_pToolTip->m_hWnd))
		{
			CString strPlaceholderHit = GetPlaceholderHit();

			if (strPlaceholderHit.IsEmpty() || strPlaceholderHit != m_strPlaceholderHit)
			{
				// Use Activate() to hide the tooltip.
				m_pToolTip->Activate(FALSE);        
			}

			if ( ! strPlaceholderHit.IsEmpty())
			{
				m_pToolTip->Activate(TRUE);
				m_strPlaceholderHit = strPlaceholderHit;
			}
		}
    	

	CEdit::OnMouseMove(nFlags, point);
}

void CPlaceholderEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CEdit::OnChar(nChar, nRepCnt, nFlags);

	if (nChar == '$')
		m_pParent->SendMessage(WM_COMMAND, m_nIDButton);
}
