// DlgSystemGenMarkColorControl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgJobColorDefinitions.h"
#include "DlgLayoutObjectControl.h"
#include "DlgSystemGenMarkColorControl.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSystemGenMarkColorControl 


CDlgSystemGenMarkColorControl::CDlgSystemGenMarkColorControl(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSystemGenMarkColorControl::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSystemGenMarkColorControl)
	m_bMap2AllColors = FALSE;
	m_bMap2SpotColors = FALSE;
	//}}AFX_DATA_INIT

	m_pParent = NULL;
}

CDlgSystemGenMarkColorControl::~CDlgSystemGenMarkColorControl()
{
}

BOOL CDlgSystemGenMarkColorControl::Create(CWnd* pParent, CDlgLayoutObjectControl* pGrandParent)
{
	m_pParent = pGrandParent;

	return CDialog::Create(IDD_SYSTEMGENMARK_COLORCONTROL, pParent);
}


void CDlgSystemGenMarkColorControl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSystemGenMarkColorControl)
	DDX_Check(pDX, IDC_ASSIGN_ALL_PRINTCOLORS, m_bMap2AllColors);
	DDX_Check(pDX, IDC_MAP_SPOTCOLORS, m_bMap2SpotColors);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgSystemGenMarkColorControl, CDialog)
	//{{AFX_MSG_MAP(CDlgSystemGenMarkColorControl)
	ON_BN_CLICKED(IDC_INSERT_PRINTCOLOR, OnInsertPrintcolor)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SYSGENMARK_COLORLIST, OnItemchangedSysGenMarkColorList)
	ON_BN_CLICKED(IDC_MODIFY_PRINTCOLOR, OnModifyPrintcolor)
	ON_BN_CLICKED(IDC_ASSIGN_ALL_PRINTCOLORS, OnAssignAllPrintcolors)
	ON_BN_CLICKED(IDC_REMOVE_PRINTCOLOR, OnRemovePrintcolor)
	ON_BN_CLICKED(IDC_MAP_SPOTCOLORS, OnMapSpotcolors)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgSystemGenMarkColorControl 

BOOL CDlgSystemGenMarkColorControl::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);
	
	m_printColorList.Initialize(this);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgSystemGenMarkColorControl::OnModifyPrintcolor() 
{
	CDlgJobColorDefinitions dlg(TRUE, TRUE);	// Overwrite mode, hide composite button
	CPageTemplate*			pTemplate		   = NULL;
	CPageSource*			pObjectSource	   = NULL;
	int						nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->GetPrintSheet()) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate->m_ColorSeparations.GetSize())
		{
			pObjectSource	   = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
			nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate)
		return;

	if (pTemplate->m_bMap2AllColors  && pTemplate->m_ColorSeparations.GetSize())
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if ( ! pos)
	{
		for (int i = 0; i < m_printColorList.GetItemCount(); i++)
			m_printColorList.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);
		pos = m_printColorList.GetFirstSelectedItemPosition();
	}

	while (pos)
	{
		int				   nItem			   = m_printColorList.GetNextSelectedItem(pos);
		CPageSourceHeader* pObjectSourceHeader = m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
		SEPARATIONINFO*	   pSepInfo			   = m_printColorList.m_printColorListItems[nItem].m_pSepInfo;
		BOOL			   bInserted		   = FALSE;
		if (pObjectSourceHeader)
		{
			if (pSepInfo)
			{
				CColorDefinition* pColorDef = m_pParent->ColorDefTable_GetColorDef(pSepInfo->m_nColorDefinitionIndex);
				if (pColorDef)
				{
					dlg.m_PSColorDefTable.InsertColorDef(*pColorDef);
					bInserted = TRUE;
				}
			}
			if ( ! bInserted)
			{
				CColorDefinition colorDef;
				colorDef.m_strColorName = pObjectSourceHeader->m_strColorName;
				colorDef.m_rgb			= pObjectSourceHeader->m_rgb;
				colorDef.m_nColorIndex	= pObjectSourceHeader->m_nColorIndex;
				dlg.m_PSColorDefTable.InsertColorDef(colorDef);
			}
		}
	}

	if (dlg.DoModal() == IDCANCEL)
		return;

	//pTemplate->m_ColorSeparations.RemoveAll();
	int i = 0;
	pos = m_printColorList.GetFirstSelectedItemPosition();
	while (pos)
	{
		int				   nItem			   = m_printColorList.GetNextSelectedItem(pos);
		CPageSourceHeader* pObjectSourceHeader = m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
		SEPARATIONINFO*	   pSepInfo			   = m_printColorList.m_printColorListItems[nItem].m_pSepInfo;
		if (pObjectSourceHeader)
		{
			BOOL bIsFirst = FALSE;
			if (pSepInfo)
			{
				pSepInfo->m_nColorDefinitionIndex = m_pParent->ColorDefTable_FindColorDefinitionIndex(dlg.m_PSColorDefTable[i].m_strColorName);
				if (pSepInfo->m_nColorDefinitionIndex < 0)
					pSepInfo->m_nColorDefinitionIndex = m_pParent->ColorDefTable_InsertColorDef(dlg.m_PSColorDefTable[i]);

				bIsFirst = (pSepInfo == pTemplate->GetFirstSepInfo(pSepInfo->m_nPageNumInSourceFile)) ? TRUE : FALSE;
			}

			if ( ! pSepInfo || bIsFirst )
			{
				pObjectSourceHeader->m_strColorName	= dlg.m_PSColorDefTable[i].m_strColorName;
				pObjectSourceHeader->m_rgb			= dlg.m_PSColorDefTable[i].m_rgb;
				pObjectSourceHeader->m_nColorIndex	= dlg.m_PSColorDefTable[i].m_nColorIndex;
			}
			i++;
		}
		//pTemplate->InsertSepInfo(pSepInfo);
	}

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable(), nObjectSourceIndex);

	OnItemchangedSysGenMarkColorList(NULL, NULL);
}

void CDlgSystemGenMarkColorControl::OnInsertPrintcolor() 
{
	CDlgJobColorDefinitions dlg(FALSE, TRUE);	// no modify mode, hide composite button
	CColorDefinition		colorDef;
	CPageTemplate*			pTemplate		   = NULL;
	CPageSource*			pObjectSource	   = NULL;
	int						nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->GetPrintSheet()) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate->m_ColorSeparations.GetSize())
		{
			pObjectSource	   = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
			nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate || ! pObjectSource)
		return;

	if (pTemplate->m_bMap2AllColors && pTemplate->m_ColorSeparations.GetSize())
		if (pObjectSource)
			if (pObjectSource->m_PageSourceHeaders.GetSize())
				return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (dlg.DoModal() == IDCANCEL)
		return;

	int nInsertIndex = pObjectSource->m_PageSourceHeaders.GetSize();
	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if (pos)
	{
		int nItem = m_printColorList.GetNextSelectedItem(pos);
		if (nItem < m_printColorList.m_printColorListItems.GetSize())
		{
			CPageSourceHeader* pObjectSourceHeader  = m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
			for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
			{
				if (&pObjectSource->m_PageSourceHeaders[i] == pObjectSourceHeader)
				{
					nInsertIndex = i + 1;
					break;
				}
			}
		}
	}

	for (int i = 0; i < dlg.m_PSColorDefTable.GetSize(); i++)
	{
		CPageSourceHeader objectSourceHeader;
		objectSourceHeader.Create(1, _T("1"), 0.0f, 0.0f, 0.0f, 0.0f, dlg.m_PSColorDefTable[i].m_strColorName, FALSE);
		objectSourceHeader.m_rgb			  = dlg.m_PSColorDefTable[i].m_rgb;
		objectSourceHeader.m_bPageNumberFix   = FALSE;
		pObjectSource->m_PageSourceHeaders.InsertAt(nInsertIndex + i, objectSourceHeader);

		short nPageNumInSourceFile = nInsertIndex + i + 1;
		if (pObjectSource->m_PageSourceHeaders.GetSize() > pTemplate->m_ColorSeparations.GetSize())
//		if ( ! pTemplate->GetFirstSepInfo(nPageNumInSourceFile))
		{
			for (int j = 0; j < pTemplate->m_ColorSeparations.GetSize(); j++)
				if (pTemplate->m_ColorSeparations[j].m_nPageNumInSourceFile >= nPageNumInSourceFile)
					pTemplate->m_ColorSeparations[j].m_nPageNumInSourceFile++;

			SEPARATIONINFO sepInfo = {(short)nObjectSourceIndex, nPageNumInSourceFile, (short)m_pParent->ColorDefTable_FindColorDefinitionIndex(objectSourceHeader.m_strColorName)};
			if (sepInfo.m_nColorDefinitionIndex < 0)
				sepInfo.m_nColorDefinitionIndex = m_pParent->ColorDefTable_InsertColorDef(dlg.m_PSColorDefTable[i]);
			pTemplate->InsertSepInfo(&sepInfo);
		}
	}

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable(), nObjectSourceIndex);

	OnItemchangedSysGenMarkColorList(NULL, NULL);
}

void CDlgSystemGenMarkColorControl::OnRemovePrintcolor() 
{
	UpdateData(TRUE);

	CPageTemplate*			pTemplate		   = NULL;
	CPageSource*			pObjectSource	   = NULL;
	int						nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->GetPrintSheet()) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate->m_ColorSeparations.GetSize())
		{
			pObjectSource	   = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
			nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate)
		return;
	if (pTemplate->m_bMap2AllColors && pTemplate->m_ColorSeparations.GetSize())
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if (pos)
	{
		int				   nItem				= m_printColorList.GetNextSelectedItem(pos);
		CPageSourceHeader* pObjectSourceHeader	= m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
		SEPARATIONINFO*	   pSepInfo				= m_printColorList.m_printColorListItems[nItem].m_pSepInfo;
		if (pObjectSourceHeader)
		{
			if (pSepInfo)
			{
				for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
				{
					if (&pObjectSource->m_PageSourceHeaders[i] == pObjectSourceHeader)
					{
						int	nPageNumInSourceFile = pTemplate->m_ColorSeparations[i].m_nPageNumInSourceFile;
						pObjectSource->m_PageSourceHeaders.RemoveAt(i);
						for (int j = 0; j < pTemplate->m_ColorSeparations.GetSize(); j++)
							if (pTemplate->m_ColorSeparations[j].m_nPageNumInSourceFile >= nPageNumInSourceFile)
								pTemplate->m_ColorSeparations[j].m_nPageNumInSourceFile--;
						break;
					}
				}

				pTemplate->RemoveSepInfo(pSepInfo);
			}
		}
	}

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable(), nObjectSourceIndex);

	OnItemchangedSysGenMarkColorList(NULL, NULL);
}

void CDlgSystemGenMarkColorControl::OnAssignAllPrintcolors() 
{
	UpdateData(TRUE);
	
	CPageTemplate* pTemplate		  = NULL;
	CPageSource*   pObjectSource	  = NULL;
	int			   nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->GetPrintSheet()) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate->m_ColorSeparations.GetSize())
		{
			pObjectSource	   = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
			nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate)
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (m_bMap2AllColors)
	{
		m_printColorList.AddComposite(pTemplate, pObjectSource, m_pParent->GetActColorDefTable(), nObjectSourceIndex);
		if (pObjectSource)
			for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
			{
				if (pObjectSource->m_PageSourceHeaders[i].m_strColorName.CompareNoCase(_T("Composite")) != 0)
					pTemplate->RemoveSepInfo(pTemplate->GetFirstSepInfo(i + 1));
			}
		pTemplate->m_bMap2AllColors = TRUE;
	}
	else
	{
		pTemplate->m_bMap2AllColors = FALSE;
		if (pObjectSource)
		{
			pTemplate->m_ColorSeparations.RemoveAll();
			for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
			{
				CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[i];
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(rObjectSourceHeader.m_strColorName);
				if (pColorDef)
				{
					SEPARATIONINFO sepInfo = {(short)nObjectSourceIndex, (short)(i + 1), (short)m_pParent->ColorDefTable_InsertColorDef(*pColorDef)};
					pTemplate->InsertSepInfo(&sepInfo);
				}
				else
				{
					CColorDefinition colorDef(rObjectSourceHeader.m_strColorName, WHITE, CColorDefinition::Spot, 0);
					SEPARATIONINFO sepInfo = {(short)nObjectSourceIndex, (short)(i + 1), (short)m_pParent->ColorDefTable_InsertColorDef(colorDef)};
					pTemplate->InsertSepInfo(&sepInfo);
				}
			}
		}
	}

	if (m_bMap2AllColors && m_bMap2SpotColors)
	{
		m_bMap2SpotColors			 = FALSE;
		pTemplate->m_bMap2SpotColors = FALSE;
		UpdateData(FALSE);
		OnMapSpotcolors();
	}

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable(), nObjectSourceIndex);

	OnItemchangedSysGenMarkColorList(NULL, NULL);
}


void CDlgSystemGenMarkColorControl::OnMapSpotcolors() 
{
	UpdateData(TRUE);
	
	CPageTemplate* pTemplate		  = NULL;
	CPageSource*   pObjectSource	  = NULL;
	int			   nObjectSourceIndex = -1;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->GetPrintSheet()) :
																	  pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate->m_ColorSeparations.GetSize())
		{
			pObjectSource	   = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
			nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		pTemplate	  = &pMark->m_markTemplate;
		pObjectSource = &pMark->m_MarkSource;
	}

	if ( ! pTemplate)
		return;

	pTemplate->m_bMap2SpotColors = m_bMap2SpotColors;

	if ( ! m_bMap2SpotColors)
		pTemplate->RemoveSpotSeps();

	if (m_bMap2SpotColors && m_bMap2AllColors)
	{
		m_bMap2AllColors		    = FALSE;
		pTemplate->m_bMap2AllColors = FALSE;
		UpdateData(FALSE);
		OnAssignAllPrintcolors();
	}

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable(), nObjectSourceIndex);

	OnItemchangedSysGenMarkColorList(NULL, NULL);
}

void CDlgSystemGenMarkColorControl::OnItemchangedSysGenMarkColorList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

	UpdateData(TRUE);

	CPageTemplate* pTemplate	 = NULL;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
		pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->GetPrintSheet()) :
																	  pLayoutObject->GetCurrentMarkTemplate();
	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
		pTemplate = &pMark->m_markTemplate;

	POSITION pos = m_printColorList.GetFirstSelectedItemPosition();
	if (pTemplate)
	{
		GetDlgItem(IDC_MODIFY_PRINTCOLOR)->EnableWindow((pTemplate->m_bMap2AllColors && pTemplate->m_ColorSeparations.GetSize()) ? FALSE : TRUE);
		GetDlgItem(IDC_INSERT_PRINTCOLOR)->EnableWindow((pTemplate->m_bMap2AllColors && pTemplate->m_ColorSeparations.GetSize()) ? FALSE : TRUE);
		BOOL bEnable = FALSE;
		if (pos)
		{
			if (pTemplate->m_bMap2AllColors && pTemplate->m_ColorSeparations.GetSize())
				;
			else
			{
				int				   nItem				= m_printColorList.GetNextSelectedItem(pos);
				CPageSourceHeader* pObjectSourceHeader	= m_printColorList.m_printColorListItems[nItem].m_pObjectSourceHeader;
				if (pObjectSourceHeader)
				{
					CString strUnknown;
					strUnknown.LoadString(IDS_UNKNOWN_STR);
					if (pObjectSourceHeader->m_strColorName.CompareNoCase(strUnknown) != 0)
						bEnable = TRUE;
				}
				else
					GetDlgItem(IDC_MODIFY_PRINTCOLOR)->EnableWindow(FALSE);
			}
		}
		GetDlgItem(IDC_REMOVE_PRINTCOLOR)->EnableWindow(bEnable);
	}

	if (pNMListView)
		if (pNMListView->uNewState & LVNI_SELECTED)
			if (GetParent())
				GetParent()->SendMessage(WM_COMMAND, ID_COLORLISTITEM_CHANGED);

	UpdateData(FALSE);
}

void CDlgSystemGenMarkColorControl::LoadData(CPageTemplate* pTemplate, CPageSource* pObjectSource, int nObjectSourceIndex)
{
	m_bMap2AllColors  = pTemplate->m_bMap2AllColors;
	m_bMap2SpotColors = pTemplate->m_bMap2SpotColors;
	UpdateData(FALSE);

	m_printColorList.LoadData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable(), nObjectSourceIndex);

	OnItemchangedSysGenMarkColorList(NULL, NULL);
}

void CDlgSystemGenMarkColorControl::SaveData(CPageTemplate* pTemplate, CPageSource* pObjectSource, int nObjectSourceIndex)
{
	m_printColorList.SaveData(pTemplate, pObjectSource, m_pParent->GetActColorDefTable(), nObjectSourceIndex);
}

