#pragma once


// CDlgPageFormats-Dialogfeld

class CDlgPageFormats : public CDialog
{
	DECLARE_DYNAMIC(CDlgPageFormats)

public:
	CDlgPageFormats(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgPageFormats();

// Dialogfelddaten
	//{{AFX_DATA(CDlgProductDetails)
	enum { IDD = IDD_PAGE_FORMATS };
	CSpinButtonCtrl	m_widthSpin;
	CSpinButtonCtrl	m_heightSpin;
	CComboBox	m_formatCombo;
	int		m_nPageOrientation;
	float	m_fHeight;
	float	m_fWidth;
	//}}AFX_DATA

	CPageFormatSelection m_formatSelection;
	CString				 m_strFormatName;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPortrait();
	afx_msg void OnLandscape(); 
	afx_msg void OnSelchangeFormat();
	afx_msg void OnBnClickedPfSave();
	afx_msg void OnBnClickedPfDelete();
};
