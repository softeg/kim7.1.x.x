#pragma once

#include "OwnerDrawnButtonList.h"


// CDlgAlignObjects-Dialogfeld

class CDlgAlignObjects : public CDialog
{
	DECLARE_DYNAMIC(CDlgAlignObjects)

public:
	CDlgAlignObjects(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgAlignObjects();

// Dialog Data
	//{{AFX_DATA(CDlgAlignObjects)
	enum { IDD = IDD_ALIGN_OBJECTS };
	float			m_fDistance;
	CSpinButtonCtrl	m_distanceSpin;
	//}}AFX_DATA


public:
	CBrush					m_bkBrush;
	CPoint					m_ptInitialPos;
	COwnerDrawnButtonList	m_toolBar;
	CReflinePositionParams  m_reflinePosParams;


public:
	static void					DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd);
	void						InitData(BOOL bReset = TRUE);
	BOOL						ObjectReferenceIsUndefined();
	int							ObjectReferenceDirection();
	int							ReferenceDirection();
	void						InitReflinePosParams(int nGroupNum = -1, BOOL bInit = FALSE);
	void						ChangeDistance();
	void						MoveObjects(int nDirection, float fNewDistance);
	CLayoutObject*				FindSelectedGroupClosestNeighbour(int nSide, int nGroupNum, CPoint point);;
	void						UpdatePrintSheetView(BOOL bUpdateAll = FALSE);
	int							GetXReference();
	int							GetYReference();
	void						DoAlign(int nDirection);
	void						DoAlignLayout(int nDirection);
	void						DoAlignObjects(int nGroupNum, int nDirection);
	void						DoAlignGroups(int nDirection);
	void						GetGroupBox(int nGroupNum, float& fBoxLeft, float& fBoxBottom, float& fBoxRight, float& fBoxTop, float& fBoxWidth, float& fBoxHeight, CPrintSheet* pPrintSheet);
	void						GetAllBox(float& fBoxLeft, float& fBoxBottom, float& fBoxRight, float& fBoxTop, float& fBoxWidth, float& fBoxHeight, CPrintSheet* pPrintSheet);
	BOOL						IsOpenDlgComponentImposer();
	class CDlgComponentImposer*	GetDlgComponentImposer();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnAlignLeft();
	afx_msg void OnAlignHCenter();
	afx_msg void OnAlignRight();
	afx_msg void OnAlignTop();
	afx_msg void OnAlignVCenter();
	afx_msg void OnAlignBottom();
	afx_msg void OnUpdateAlignLeft(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignHCenter(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignRight(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignTop(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignVCenter(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignBottom(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignDistanceStatic(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignDistance(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlignDistanceSpin(CCmdUI* pCmdUI);
	afx_msg void OnDeltaposDistanceSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDestroy();
};
