// ImpManDocDeprecated.h : interface of the CImpManDoc class
//
/////////////////////////////////////////////////////////////////////////////


#pragma once

//#include "ImpManDoc.h"


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CFinalProductDescription

class CFinalProductDescription : public CObject
{
public:
    DECLARE_SERIAL( CFinalProductDescription )
    // empty constructor is necessary
    CFinalProductDescription();

enum BindingMethod {PerfectBound, SaddleStitched}; 
enum ProductionType {Single, Double, ComingAndGoing};

//Attributes:
public:
	unsigned 	 m_nApplicationCategory;	// APPLICATION_BOOK or APPLICATION_LABEL
	CString	 	 m_strPagesComment;
	float	 	 m_fTrimSizeWidth, m_fTrimSizeHeight;
	CString  	 m_strFormatName;
	unsigned 	 m_nPageOrientation;
	CBindingDefs m_bindingDefs;
	//unsigned m_nBindingMethod;
	CString		 m_strBindingComment;
	long		 m_nQuantity;
	CString		 m_strQuantityComment;
	struct
	{
		float fWidth, fHeight;
		float fDistanceToBack, fDistanceToHead;
	}m_PageMargins;
private:
	unsigned 	 m_nPages;


public:
//Operations:
    void  Serialize( CArchive& archive );
	int	  GetNumPages();
	void  SetNumPages(int nPages) { m_nPages = nPages; };
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CCipCutSingleCut : public CObject
{
public:
    DECLARE_SERIAL( CCipCutSingleCut )
	CCipCutSingleCut() {};

//enum PressType {SheetPress, WebPress};

//Attributes:
public:
	unsigned m_nIndex;
	float	 m_fCutPosition, m_fCutOffset; 

	int		 m_nSteps;
	float	 m_fObjectCutWidth;
	BOOL	 m_bTrimCut;
	float	 m_fTrimCutWidth;

	int		 m_nRotation;
	BOOL	 m_bAutomatic;
	BOOL	 m_bAirdesk;
	BOOL	 m_bSaddleSlow;
	BOOL	 m_bChipOutput;

//Operations:
public:
	const CCipCutSingleCut& operator=(const CCipCutSingleCut& /*rSingleCut*/) { return *this; };
};

template <> void AFXAPI SerializeElements <CCipCutSingleCut> (CArchive& ar, CCipCutSingleCut* pSingleCut, INT_PTR nCount);

/////////////////////////////////////////////////////////////////////////////
// CCipCutProgram will be subclassed in CImpManDoc
class CCipCutProgram : public CList <CCipCutSingleCut, CCipCutSingleCut&>	// CList is derived from CObject
{																			// - so serialization will run
public:
    DECLARE_SERIAL( CCipCutProgram )
	CCipCutProgram() {};

//Attributes:
	int	m_nPressureTime;

//Operations:
public:
	void Serialize(CArchive& ar);
	int  GetNumberOfCutLines() { return 0; };
};
/////////////////////////////////////////////////////////////////////////////



// CProductPartProductionBound //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CProductPartProductionBound : public CObject
{
public:
    DECLARE_SERIAL( CProductPartProductionBound )
	CProductPartProductionBound();
	CProductPartProductionBound(const CProductPartProductionBound& rProductPartProductionBound); // copy constructor
	~CProductPartProductionBound();

public:
	int					m_nProductionType;
	CBindingDefs		m_bindingDefs;
	float				m_fHeadTrim;
	float				m_fSideTrim;
	float				m_fFootTrim;
	float				m_fSpineTrim; 
	float				m_fMillingEdge;
	float				m_fGlueLine;
	float				m_fOverFold;
	int					m_nOverFoldSide;
	float				m_fBleed;
	BOOL				m_bShinglingActive;
	int					m_nShinglingMethod;
	float				m_fShinglingValue;
	float				m_fShinglingValueFoot;
	BOOL				m_bShinglingCropMarks;
	BOOL				m_bAdjustContentToMask;

public:
	const	CProductPartProductionBound& operator= (const CProductPartProductionBound& rProductPartProductionBound);
	void	Copy(const CProductPartProductionBound& rProductPartProductionBound);
	void	Serialize( CArchive& ar );
};

class CProductPartProductionUnbound : public CObject
{
public:
    DECLARE_SERIAL( CProductPartProductionUnbound )
	CProductPartProductionUnbound();
	CProductPartProductionUnbound(const CProductPartProductionUnbound& rProductPartProductionUnbound); // copy constructor
	~CProductPartProductionUnbound();

public:
	float				m_fLeftTrim; 
	float				m_fBottomTrim;
	float				m_fRightTrim;
	float				m_fTopTrim;
	float				m_fBleed;

public:
	const	CProductPartProductionUnbound& operator= (const CProductPartProductionUnbound& rProductPartProductionUnbound);
	void	Copy(const CProductPartProductionUnbound& rProductPartProductionUnbound);
	void	Serialize( CArchive& ar );
};


class CProductPart : public CObject
{
public:
    DECLARE_SERIAL( CProductPart )
	CProductPart();
	CProductPart(const CProductPart& rProductPart); // copy constructor

enum {Bound, Unbound};
enum {Cover, Body, Insert};
enum {OverFoldNone, OverFoldFront, OverFoldBack};

//Attributes:
public:
	int								m_nType;
	int								m_nSubType;
	CString							m_strName;
	int								m_nNumPages;
	CColorantList					m_colorantList;
	CNumberingList					m_numberingList;
	CString							m_strPaper;
	int								m_nPaperGrammage;
	float							m_fPaperVolume;
	float							m_fPageWidth;
	float							m_fPageHeight;
	CString							m_strFormatName;
	int								m_nPageOrientation;
	long							m_nAmount;
	long							m_nExtra;
	CString							m_strComment;
	CString							m_strJDFNodeID;

	int								m_nDefaultTurnTumbleMethod;
	CString							m_strDefaultMarkset;		
// production data
	CProductPartProductionBound		m_prodDataBound;
	CProductPartProductionUnbound	m_prodDataUnbound;
//private:
	CString							m_strDefaultPressDevice;	
	int								m_nDefaultWorkStyle;
	CString							m_strDefaultPaper;			
	CString							m_strDefaultOutputProfile;	


//Operations:
public:
	const CProductPart& operator= (const CProductPart& rProductPart);
	void  Copy(const CProductPart& rProductPart);
	BOOL  IsBound()	{ return (m_nType == Bound)	  ? TRUE : FALSE; };
	BOOL  IsUnbound() { return (m_nType == Unbound) ? TRUE : FALSE; };
};

template <> void AFXAPI SerializeElements <CProductPart> (CArchive& ar, CProductPart* pProductPart, INT_PTR nCount);



// CProductPartList //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CProductPartList : public CArray <CProductPart, CProductPart&> 
{
public:
    DECLARE_SERIAL( CProductPartList )
	CProductPartList();

//Attributes:
public:
	//CArray <CComponentDisplayInfo,  CComponentDisplayInfo&>  m_componentDisplayInfos;
	//CArray <CPrintSheetDisplayInfo, CPrintSheetDisplayInfo&> m_printSheetDisplayInfos;
	//CProductPartComponentList								 m_componentList;
	//int														 m_nPrintSheetRowHeight;

//Operations:
public:
	void CreateFromOldSchema(CImpManDoc* pDoc, CFinalProductDescription& finalProductDescriptionDeprecated);

//    void Serialize( CArchive& archive );
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////



class CProductionSetupDeprecated : public CObject
{
public:
    DECLARE_SERIAL( CProductionSetupDeprecated )
	CProductionSetupDeprecated();
	CProductionSetupDeprecated(const CProductionSetupDeprecated& rProductionSetup); // copy constructor

//Attributes:
public:
	int					m_nType;
	int					m_nSubType;
	CString				m_strName;

// production data
	CProductPartProductionBound		m_prodDataBound;
	CProductPartProductionUnbound	m_prodDataUnbound;

	CString				m_strDefaultPressDevice;
	int					m_nDefaultWorkStyle;
	int					m_nDefaultTurnTumbleMethod;
	CString				m_strDefaultPaper;			
	CString				m_strDefaultOutputProfile;	
	CString				m_strDefaultMarkset;

	BOOL				m_bIsDefault;


//Operations:
public:
	const		CProductionSetupDeprecated& operator= (const CProductionSetupDeprecated& rProductionSetup);
	void		Copy(const CProductionSetupDeprecated& rProductionSetup);
	void		Serialize(CArchive& ar);
	void		Load(const CString& strSetupName);
	void		Save(const CString& strSetupName);
	static void	Remove(const CString& strSetupName);
};



/////////////////////////// CLabelListEntry /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CLabelListEntry : public CObject
{
public:
    DECLARE_SERIAL( CLabelListEntry )
	CLabelListEntry();
	CLabelListEntry(const CString& strName, float fWidth, float fHeight, long nQuantity, const CString& strComment) 
					: m_strName(strName), m_nProductPartIndex(-1), m_fLabelWidth(fWidth), m_fLabelHeight(fHeight), m_nQuantity(nQuantity), m_nQuantityActual(0), 
					  m_fLeftTrim(0.0f), m_fBottomTrim(0.0f), m_fRightTrim(0.0f), m_fTopTrim(0.0f), m_bAllowRotation(TRUE), m_nPriority(0), m_strComment(strComment) {};
	CLabelListEntry(const CLabelListEntry& rLabelListEntry); // copy constructor

//Attributes:
public:
	CString		 m_strName;
	int			 m_nProductPartIndex;
	float		 m_fLabelWidth;
	float		 m_fLabelHeight;
	float		 m_fLeftTrim; 
	float		 m_fBottomTrim;
	float		 m_fRightTrim;
	float		 m_fTopTrim;
	BOOL		 m_bAllowRotation;
	int			 m_nPriority;
	long		 m_nQuantity;
	long		 m_nQuantityActual;
	CString		 m_strComment;
	CGraphicList m_graphicList;

//Operations:
public:
	const CLabelListEntry&	operator= (const CLabelListEntry& rLabelListEntry);
};

template <> void AFXAPI SerializeElements <CLabelListEntry> (CArchive& ar, CLabelListEntry* pLabelListEntry, INT_PTR nCount);
void AFXAPI DestructElements(CLabelListEntry* pLabelListEntry, INT_PTR nCount);

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


///////////////////////////// CLabelList ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CLabelList : public CArray <CLabelListEntry, CLabelListEntry&> 
{
public:
    DECLARE_SERIAL( CLabelList )
	CLabelList();

//Attributes:
public:

//Operations:
public:
	BOOL	operator==(const CLabelList& rLabelList);
	BOOL	operator!=(const CLabelList& rLabelList);
	void	CheckLayoutObjectFormats();
	int		Find(const CString& strName);
	int		FindLabelIndex(CLabelListEntry* pLabelListEntry);
	BOOL	HasGraphics();
	float	GetMaxHeight(int nNum = -1, int nProductPartIndex = -1);
	int		MapIndexLocal2Global(int nLocalIndex, int nProductPartIndex);
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
