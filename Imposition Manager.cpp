// Imposition Manager.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "MainFrm.h"
#include "ThemedTabCtrl.h"
#include "BookblockFrame.h"
#include "BookBlockView.h"
#include "BookBlockProdDataView.h"
#include "ObjectPivotControl.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h" 
#include "DlgMarkSet.h"
#include "PrintSheetFrame.h"
#include "NewPrintSheetFrame.h"
#include "PrintSheetTableView.h"
#include "PrintSheetColorSettingsView.h"
#include "PrintSheetView.h"  
#include "PrintSheetTreeView.h"
#include "PrintSheetMarksView.h"
#include "PageListFrame.h"
#include "PageListView.h"
#include "PageListTableView.h"
#include "PageSourceListView.h"   
#ifdef PDF 
#include "PDFReader.h" 
#endif
#include "PageListDetailView.h" 
#include "DlgPressDevice.h"
#include "DlgShinglingAssistent.h"
#include "DlgImpositionScheme.h"
#include "DlgEditTempString.h"
#include "DlgPrintColorTable.h"
#include "mmsystem.h"
#include "Shlwapi.h"
#include <locale.h>
#include "JobDocumentationFrame.h"
#include "JobDocumentationView.h"
#include "JobDocumentationSettingsView.h"
#include "AutoServerFrame.h"
#include "OrderDataFrame.h"
#include "OrderDataView.h"
#include "ProdDataFrame.h"
#include "ProdDataHeaderView.h"
#include "DlgJobData.h"
#include "ProdDataProcessOverView.h"
#include "PrintSheetPlanningView.h"
#include "PrintSheetMaskSettingsView.h"
#include "ProductNavigationView.h"
#include "PrintSheetObjectPreview.h"
#include "ProductsView.h"
#include "DlgProductionManager.h"
#include "PrintSheetComponentsView.h"
#include "PrintComponentsView.h"
#include "DlgConnectToServer.h"



#ifdef PDF
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"
#endif

#include "DlgDemoOrRegisterDongle.h"
#include "spromeps.h" 


#include <dos.h>					 		 
#include <direct.h>
#include "Splash.h"  
#include "PrintSheetTableView.h"
#include "JDFOutputImposition.h"

#include "PDFEngineXMLParser.h"
#include "KimSocketsCommon.h"
#include "XMLCommon.h"
#include "DlgDirPicker.h"



TCHAR barcodeTypeNamesV8[][40] = 
{
  _T("None"),
  _T("Code 11"),
  _T("Code 2OF5 Standard"),
  _T("Code 2OF5 Interleaved"),
  _T("Code 2OF5 IATA"),
  _T("Code 2OF5 Matrix"),
  _T("Code 2OF5 DataLogic"),
  _T("Code 2OF5 Industry"),
  _T("Code 39"),
  _T("Code 39 Full ASCII"),
  _T("EAN8"),
  _T("EAN8P2"),
  _T("EAN8P5"),
  _T("EAN13"),
  _T("EAN13P2"),
  _T("EAN13P5"),
  _T("EAN/UCC-128"),
  _T("UPC12"),
  _T("CodaBar 2 Widths"),
  _T("CodaBar 18 Widths"),
  _T("Code128"),
  _T("DP Leitcode"),
  _T("DP Identcode"),
  _T("Code 16K"),
  _T("Code 49"),
  _T("Code 93"),
  _T("UPC25"),
  _T("UPCD1"),
  _T("Flattermarken"),
  _T("GS1 DataBar (RSS-14)"),
  _T("GS1 DataBar Limited (RSS)"),
  _T("GS1 DataBar Expanded (RSS)"),
  _T("Telepen Alpha"),
  _T("UCC128"),
  _T("UPCA"),
  _T("UPCAP2"),
  _T("UPCAP5"),
  _T("UPCE"),
  _T("UPCEP2"),
  _T("UPCEP5"),
  _T("USPS PostNet5 (ZIP)"),
  _T("USPS PostNet6 (ZIP+cd)"),
  _T("USPS PostNet9 (ZIP+4)"),
  _T("USPS PostNet10 (ZIP+4+cd)"),
  _T("USPS PostNet11 (ZIP+4+2)"),
  _T("USPS PostNet12 (ZIP+4+2+cd)"),
  _T("Plessey"),
  _T("MSI"),
  _T("SSCC-18"),
  _T("FIM"),
  _T("LOGMARS"),
  _T("Pharmacode One-Track"),
  _T("PZN"),
  _T("Pharmacode Two-Track"),
  _T("General Parcel"),
  _T("PDF417"),
  _T("PDF417 Truncated"),
  _T("MaxiCode"),
  _T("QR-Code"),
  _T("Code128A"),
  _T("Code128B"),
  _T("Code128C"),
  _T("Code 93 Full ASCII"),
  _T("Australian Post Custom"),
  _T("Australian Post Custom2"),
  _T("Australian Post Custom3"),
  _T("Australian Post Reply Paid"),
  _T("Australian Post Routing"),
  _T("Australian Post Redirect"),
  _T("ISBN"),
  _T("Royal Mail 4 State (RM4SCC)"),
  _T("Data Matrix"),
  _T("EAN14"),
  _T("Codablock-E"),
  _T("Codablock-F"),
  _T("NVE-18"),
  _T("Japanese Postal"),
  _T("Korean Postal Authority"),
  _T("GS1 DataBar Truncated (RSS)"),
  _T("GS1 DataBar Stacked (RSS)"),
  _T("GS1 DataBar Stacked Omnidir (RSS)"),
  _T("GS1 DataBar Expanded Stacked (RSS)"),
  _T("PLANET 12 digit"),
  _T("PLANET 14 digit"),
  _T("MicroPDF417"),
  _T("USPS OneCode (4-CB)"),
  _T("Plessey Bidirectional"),
  _T("Telepen"),
  _T("GS1-128 (EAN/UCC-128)"),
  _T("ITF-14"),
  _T("KIX"),
  _T("BC412"),
  _T("Aztec Code"),
  _T("DAFT Code"),
  _T("Italian Postal 2 of 5"),
  _T("Italian Postal 3 of 9"),
  _T("DPD"),
  _T("")
};



#include "aclapi.h"
void CreateFileBySecurity(const CString& strFile);


CSysToUTF8Converter::~CSysToUTF8Converter()
{
	for (int i = 0; i < m_BufArray.GetSize(); i++)
		delete m_BufArray[i];

	m_BufArray.RemoveAll();
}

IPL_Path_t* CSysToUTF8Converter::Convert(const CString& strSys)
{
	PTB_uint32_t nBufSize = 1024;
	m_BufArray.Add(new IPL_Path_t[1024]);
	if (PTB_ConvertSysToUTF8(strSys, m_BufArray[m_BufArray.GetSize() - 1], &nBufSize) == PTB_eerrStrBufferTooSmall)
	{
		delete m_BufArray[m_BufArray.GetSize() - 1];
		m_BufArray[m_BufArray.GetSize() - 1] = new IPL_Path_t[nBufSize];
		PTB_ConvertSysToUTF8(strSys, m_BufArray[m_BufArray.GetSize() - 1], &nBufSize);
	}

	return m_BufArray[m_BufArray.GetSize() - 1];
}

int StrippingParamsMapOrientation(int nJDFOrientation)
{
	switch (nJDFOrientation)
	{
	case JDFElement::Orientation_Rotate0:	return ROTATE0;
	case JDFElement::Orientation_Rotate90:	return ROTATE90;
	case JDFElement::Orientation_Rotate180:	return ROTATE180;
	case JDFElement::Orientation_Rotate270:	return ROTATE270;
	case JDFElement::Orientation_Flip0:		return FLIP0;
	case JDFElement::Orientation_Flip90:	return FLIP90;
	case JDFElement::Orientation_Flip180:	return FLIP180;
	case JDFElement::Orientation_Flip270:	return FLIP270;
	}

	return ROTATE0;
}

// transform screen pixels to mm (in hundreds) / needed for printing
int Pixel2MM(CDC* pDC, int nValue)
{
	if ( ! pDC)
		return nValue;

	if (pDC->IsPrinting())
		return nValue * (int)(2540.0f / (float)theApp.m_nScreenDPI + 0.5f);
	else
		return nValue;
}

// transform mm (in hundreds) to device coordinates (dots)
int MM2DP(CDC* pDC, int nMM)
{
	if ( ! pDC)
		return nMM;

	int nTargetDots = nMM;
	if (pDC->IsPrinting())
		nTargetDots = (int)((float)nMM * (float)pDC->GetDeviceCaps(LOGPIXELSX) / 2540.0f + 0.5f);
	else
		nTargetDots = (int)((float)nMM * (float)theApp.m_nScreenDPI / 2540.0f + 0.5f);

	nTargetDots = max(nTargetDots, 1);

	return nTargetDots;
}

// transform device coordinates (dots) to mm (in hundreds)
int DP2MM(CDC* pDC, int nDP)
{
	if ( ! pDC)
		return nDP;

	int nTargetMM = nDP;
	if (pDC->IsPrinting())
		nTargetMM = (int)( ((float)nDP / (float)pDC->GetDeviceCaps(LOGPIXELSX)) * 2540.0f + 0.5f);
	else
		nTargetMM = (int)( ((float)nDP / (float)theApp.m_nScreenDPI) * 2540.0f + 0.5f);

	nTargetMM = max(nTargetMM, 1);

	return nTargetMM;
}

// transform screen pixels to constant mm value (in hundreds) independent of world/viewport ratio / needed for printing
int Pixel2ConstMM(CDC* pDC, int nValue)
{
	if ( ! pDC)
		return nValue;

	CSize sizeValue(1, 1);
	if (pDC->IsPrinting())
	{
		int nMM			 = nValue * (int)(2540.0f / (float)theApp.m_nScreenDPI + 0.5f);	// desired value in hundreds of millimeter
		int nPrinterDots = (int)((float)nMM * (float)pDC->GetDeviceCaps(LOGPIXELSX) / 2540.0f + 0.5f);
		sizeValue = CSize(nPrinterDots, nPrinterDots);
	}
	else
		sizeValue = CSize(nValue, nValue);

	pDC->DPtoLP(&sizeValue);
	return sizeValue.cx;
}

void Ascii2Float(CString strBuffer, CString strControl, float* pValue1, float* pValue2)
{
	strBuffer.Replace(',', '.');

	_locale_t locale = _create_locale(LC_NUMERIC, "english");

	if (pValue1 && pValue2)
		_stscanf_s_l((LPCTSTR)strBuffer, strControl, locale, pValue1, pValue2);
	else
		if (pValue1)
			_stscanf_s_l((LPCTSTR)strBuffer, strControl, locale, pValue1);
		else
			if (pValue2)
				_stscanf_s_l((LPCTSTR)strBuffer, strControl, locale, pValue2);
	
	_free_locale(locale);
}

float Ascii2Float(CString strBuffer)
{
	if (strBuffer.IsEmpty())
		return 0.0f;

	strBuffer.Replace(',', '.');

	_locale_t locale = _create_locale(LC_NUMERIC, "english");

	float fValue;
	_stscanf_s_l((LPCTSTR)strBuffer, _T("%f"), locale, &fValue);
	
	_free_locale(locale);

	return fValue;
}

CString Float2Ascii(float fValue, int nDigits)
{
	int nDecimal, nSign;
	char buffer[_CVTBUFSIZE];
	_fcvt_s(buffer, _CVTBUFSIZE, fValue, nDigits, &nDecimal, &nSign);
	CString string = buffer;
	string.Insert((nDecimal < 0) ? 0 : nDecimal, '.');
	if (nSign != 0)
		string.Insert(0, '-');
	return string;
}

CString Int2Ascii(int nValue)
{
	CString string;

	string.Format(_T("%d"), nValue);	// in case of integer we can use Format() here, because there is no problem with locale, since we have no comma

	return string;
}

CString GetStringToken(CString string, int nToken, CString strDelimiters)
{
	CString strToken;
	int nCurToken	  = 0;
	int nCurStringPos = 0;

	strToken = string.Tokenize(strDelimiters, nCurStringPos);
	while ( (strToken != "") && (nCurToken < nToken) )
	{
	   strToken = string.Tokenize(strDelimiters, nCurStringPos);
	   nCurToken++;
	}

	strToken.TrimLeft(); strToken.TrimRight();

	return strToken;
}

CString GetKeyValue(CArray <CString, CString&>& keyValueList, const CString& strSearchKey)
{
	for (int i = 0; i < keyValueList.GetSize(); i++)
	{
		CString strKey = GetStringToken(keyValueList[i], 0, _T(" "));
		if (strSearchKey == strKey)
			return GetStringToken(keyValueList[i], 1, _T(" "));
	}
	return _T("");
}

int GetNumTextLines(CDC* pDC, const CString& rString, int nMaxWidth)
{
	int nCount = 1;

	if (nMaxWidth == 0)
	{
		int nIndex = rString.Find(_T("\n"), 0);
		while (nIndex != -1)
		{
			nCount++;
			nIndex = rString.Find(_T("\n"), nIndex + 1);
		}
		return nCount;
	}
	else	// take care about automatic wordbreak
	{
		CSize	extent, extentSep;
		int		nXPos	= 0;
		TCHAR	string[32768];
		_tcsncpy(string, rString, 32768);
		string[32767] = '\0';
		TCHAR*	poi	  = string;
		TCHAR*   token = poi;
		TCHAR	sep;
		while( *poi != '\0' )
		{
			if ( (*poi == ' ') || (*poi == '\t') || (*poi == '\n') || (*poi == '\r') )
			{
				extentSep = pDC->GetTextExtent(*poi);
				sep  = *poi;
				*poi = '\0';
				extent = pDC->GetTextExtent(token);
				if ((nXPos + extent.cx > nMaxWidth) )
				{
					if  (nXPos > 0)
					{
						nCount++;
						nXPos = 0;
						*poi  = sep;
						poi   = token;
					}
					else
						nCount++;
					poi++;
					token = poi;
				}
				else
				{
					nXPos += extent.cx + extentSep.cx;
					*poi  = sep;
					if ( (*poi == '\n') || (*poi == '\r') )
					{
						if ( ( (*poi == '\n') && (*(poi-1) == '\r') ) || 
							 ( (*poi == '\r') && (*(poi-1) == '\n') ) )
							 ;
						else
						{
							nCount++;
							nXPos = 0;
						}
					}

					poi++;
					token = poi;
				}
			}
			else
				poi++;
		}
		return nCount;
	}
}

void DrawTextMeasured(CDC* pDC, const CString& strText, CRect rcText, int nFlags, CRect& rcBox)
{
	CRect rcTextMeasure = rcText;
	if (strText.IsEmpty())
	{
		rcTextMeasure.right  = rcTextMeasure.left + 1;
		rcTextMeasure.bottom = rcTextMeasure.top  + 1;
		rcBox.UnionRect(rcBox, rcTextMeasure); 
		return;
	}

	pDC->DrawText(strText, rcText, nFlags);
	pDC->DrawText(strText, rcTextMeasure, nFlags | DT_CALCRECT);
	rcBox.UnionRect(rcBox, rcTextMeasure); 
}

CString	FormatQuantityString(long nValue)
{
	struct lconv* pLocaleInfo = localeconv();

	BOOL bNegative = (nValue < 0) ? TRUE : FALSE;
	nValue = abs(nValue);

	CString string;
	string.Format(_T("%ld"), nValue);

	TCHAR cDecimalPoint = *pLocaleInfo->decimal_point;
	TCHAR cThousandsSep = *pLocaleInfo->mon_thousands_sep;

	CString text(string); 
    int left = text.Find('.'); 
    if(left == -1) 
        left = text.GetLength(); 
    else 
		text.SetAt(left, cDecimalPoint); 

    while(left > 3) 
		text.Insert(left -= 3, cThousandsSep);

	if (bNegative)
		return CString(_T("-")) + text;
	else
		return text;
}

BOOL IsNumericString(const CString& string)
{
	for (int i = 0; i < string.GetLength(); i++)
	{
		if ( ! isdigit(string[i]))
			return FALSE;
	}
	return TRUE;
}

//// if engine log message 

void KIMLogMessage(int nIDPrompt, int nType)
{
#ifdef KIM_ENGINE
	int nMsgType = KIM_MSGTYPE_INFO;
	switch (nType & MB_ICONMASK)
	{
	case MB_ICONINFORMATION:	nMsgType = KIM_MSGTYPE_INFO;		break;
	case MB_ICONWARNING:		nMsgType = KIM_MSGTYPE_WARNING;		break;
	case MB_ICONERROR:			nMsgType = KIM_MSGTYPE_ERROR;		break;
	case MB_ICONQUESTION:		nMsgType = KIM_MSGTYPE_QUESTION;	break;
	}
	switch (nType & MB_TYPEMASK)
	{
	case MB_YESNO:				nMsgType = KIM_MSGTYPE_QUESTION;	break;
	}

	CString strText; strText.LoadString(nIDPrompt);
	KimEngineAddMessageLog(nMsgType, strText);
	KimEngineNotifyServer(MSG_MESSAGELOGS_CHANGED);
#endif
}

void KIMLogMessage(LPCTSTR lpszText, int nType)
{
#ifdef KIM_ENGINE
	int nMsgType = KIM_MSGTYPE_INFO;
	switch (nType & MB_ICONMASK)
	{
	case MB_ICONINFORMATION:	nMsgType = KIM_MSGTYPE_INFO;		break;
	case MB_ICONWARNING:		nMsgType = KIM_MSGTYPE_WARNING;		break;
	case MB_ICONERROR:			nMsgType = KIM_MSGTYPE_ERROR;		break;
	case MB_ICONQUESTION:		nMsgType = KIM_MSGTYPE_QUESTION;	break;
	}
	switch (nType & MB_TYPEMASK)
	{
	case MB_YESNO:				nMsgType = KIM_MSGTYPE_QUESTION;	break;
	}

	KimEngineAddMessageLog(nMsgType, lpszText);
	KimEngineNotifyServer(MSG_MESSAGELOGS_CHANGED);
#endif
}

//// if engine log message, if app open message
int KIMOpenLogMessage(int nIDPrompt, int nType, int nDefaultReturn)
{
#ifdef KIM_ENGINE
	CString strPrompt; strPrompt.LoadString(nIDPrompt);
	KIMLogMessage(strPrompt, nType);
	return nDefaultReturn;
#else
	return AfxMessageBox(nIDPrompt, nType);
#endif
}

//// if engine log message, if app open message
int KIMOpenLogMessage(LPCTSTR lpszText, int nType, int nDefaultReturn)
{
#ifdef KIM_ENGINE
	KIMLogMessage(lpszText, nType);
	return nDefaultReturn;
#else
	return AfxMessageBox(lpszText, nType);
#endif
}
//
//void KimEngineAddMessageLog(int nMsgType, CString strMessage, BOOL bDoLog)
//{
//	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
//	_tsplitpath(theApp.m_strDBCurrentJobFullpath, szDrive, szDir, szFname, szExt);
//	CString strTemplatePath = CString(szDrive) + CString(szDir);
//
//	CString		 strInfoFile;
//	CStringArray strErrors;
//	strInfoFile.Format(_T("%sJobMessages.xml"), strTemplatePath);
//
//	XMLDoc*		pXMLDoc = NULL;
//	CFileStatus	fileStatus;
//	if (CFile::GetStatus(strInfoFile, fileStatus))	
//	{
//		pXMLDoc = new XMLDoc();
//		try
//		{
//			pXMLDoc->Parse((WString)strInfoFile);
//		}
//		catch (JDFException e)
//		{
//			return;
//		}
//		if (pXMLDoc->isNull())
//		{
//			return;
//		}
//	}
//	else
//		pXMLDoc = new XMLDoc(_T("KIM-JobMessages"));
//
//	KElement root = pXMLDoc->GetRoot();
//	CString strNodeName = root.GetDocRootName().getBytes();
//	if (strNodeName.CompareNoCase(_T("KIM-JobMessages")) != 0)
//		return;
//
//	COleDateTime curTime(COleDateTime::GetCurrentTime());
//	strMessage.TrimLeft(); strMessage.TrimRight(); strMessage.Remove('\r'); strMessage.Remove('\n');
//
//	KElement firstChildNode = root.GetFirstChildElement();
//	KElement logElement = (firstChildNode.isNull()) ? root.AppendElement((WString)_T("MessageLog")) : root.InsertBefore((WString)_T("MessageLog"), firstChildNode);
//	CString strMsgType;
//	switch (nMsgType)
//	{
//	case KIM_MSGTYPE_INFO:		strMsgType = _T("Information");	break;
//	case KIM_MSGTYPE_WARNING:	strMsgType = _T("Warning");		break;
//	case KIM_MSGTYPE_ERROR:		strMsgType = _T("Error");		break;
//	}
//	logElement.AppendAttribute((WString)_T("Type"), (WString)strMsgType);
//	logElement.AppendAttribute((WString)_T("DateTime"), (WString)curTime.Format(LOCALE_NOUSEROVERRIDE, LANG_USER_DEFAULT));
//	KElement messageElement = logElement.AppendElement((WString)_T("Message"));
//	messageElement.AppendText((WString)strMessage);
//
//	try
//	{
//		pXMLDoc->Write2URL((WString)WindowsPath2URL(strInfoFile));
//	}
//	catch (JDF::JDFException e)
//	{
//	}
//
//	delete pXMLDoc;
//}

void KimEngineAddMessageLog(int nMsgType, CString strMessage)
{
	CImpManDoc*	 pDoc		= CImpManDoc::GetDoc();
	CString		 strJobname = (pDoc) ? pDoc->GetTitle() : _T("");
	CString		 strExt		= strJobname.Right(4).MakeLower();
	if (strExt == _T(".job"))
		PathRemoveExtension(strJobname.GetBuffer(MAX_PATH));
	
	COleDateTime timeStamp(COleDateTime::GetCurrentTime());

	KimEngineNotifyServer(MSG_MESSAGELOGS_ADD, nMsgType, theApp.m_nDBCurrentJobID, timeStamp.Format(_T("%Y-%m-%d %H:%M:%S")), strMessage, strJobname);
}

int KimEngineNotifyServer(int nMsg, int nParam1, int nParam2, CString strParam3, CString strParam4, CString strParam5)
{
#ifndef KIM_ENGINE
	return -1;
#endif

	CWnd* pWnd = CWnd::FindWindow("KIMPDFServerApp", NULL);
	if ( ! pWnd)
		return -1;

	struct 
	{
		int		nMsg;
		int		nParam1;
		int		nParam2;
		TCHAR	szParam3[1024];
		TCHAR	szParam4[1024];
		TCHAR	szParam5[1024];
	}data;
	memset(&data, 0, sizeof(data));

	data.nMsg	  = nMsg;
	data.nParam1  = nParam1;
	data.nParam2  = nParam2;
	strncpy(data.szParam3, strParam3, strParam3.GetLength() + 1);
	strncpy(data.szParam4, strParam4, strParam4.GetLength() + 1);
	strncpy(data.szParam5, strParam5, strParam5.GetLength() + 1);

	COPYDATASTRUCT cds;
	cds.dwData = 1;
	cds.cbData = sizeof(data);
	cds.lpData = &data;

	return pWnd->SendMessage(WM_COPYDATA, (WPARAM)0, (LPARAM)&cds);
}

int KimEngineDBJobAbort(int nJobID)
{
#ifndef KIM_ENGINE
	return FALSE;
#endif
	if (theApp.m_bKimEngineAbort)
		return TRUE;

	//if ( ! theApp.m_bServerConnected)
	//{
	//	if (theApp.m_kaDB.kA_Connect(kimAutoDB, "", theApp.settings.m_szAutoServerNameIP, "SYSDBA", "masterkey") != 1)
	//		return -1;
	//	theApp.m_bServerConnected = TRUE;
	//}

	//kimAutoDB::_jobs job;
	//theApp.m_kaDB.ka_GetJob(&job, nJobID);
	//if (job.status & kimAutoDB::jobToAbort)
	//{
	//	theApp.m_bKimEngineAbort = TRUE;
	//	return TRUE;
	//}
	//else
		return FALSE;
}

int KimEngineDBSetProduct(int nJobID, int nProductID, int nType, CString strName, CString strPartName, float fWidth, float fHeight, int nNumPages)
{
#ifndef KIM_ENGINE
	return -1;
#endif
	if ( ! theApp.m_bServerConnected)
	{
		if (theApp.m_kaDB.kA_Connect("kimAutoDB", "", theApp.settings.m_szAutoServerNameIP, "SYSDBA", "masterkey") != 1)
			return -1;
		theApp.m_bServerConnected = TRUE;
	}

	return theApp.m_kaDB.ka_SetProduct(nProductID, nType, (std::string)strName, (std::string)strPartName, fWidth, fHeight, nNumPages, nJobID);
}

int KimEngineDBGetInputPDFID(int nJobID, CString strFullpath)
{
#ifndef KIM_ENGINE
	return -1;
#endif
	if ( ! theApp.m_bServerConnected)
	{
		if (theApp.m_kaDB.kA_Connect("kimAutoDB", "", theApp.settings.m_szAutoServerNameIP, "SYSDBA", "masterkey") != 1)
			return -1;
		theApp.m_bServerConnected = TRUE;
	}

	CString strDrive, strDir, strFname, strExt;
	_splitpath((LPCTSTR)strFullpath, strDrive.GetBuffer(_MAX_DRIVE), strDir.GetBuffer(_MAX_DIR), strFname.GetBuffer(_MAX_FNAME), strExt.GetBuffer(_MAX_EXT));
	strDrive.ReleaseBuffer(); strDir.ReleaseBuffer(); strFname.ReleaseBuffer(); strExt.ReleaseBuffer();
	CString strFileName = strFname + strExt;
	CString strPath		= strDrive + strDir;

	kimAutoDB::_inputPDF inputPDF;
	theApp.m_kaDB.ka_GetInputPDF(&inputPDF, strFileName.GetBuffer(), theApp.m_nDBCurrentJobID, 1);

	return inputPDF.id;
}

int KimEngineDBSetInputPDF(int nJobID, int nPdfID, CString strFullpath, int nNumPages, int nStatus)
{
#ifndef KIM_ENGINE
	return -1;
#endif
	if ( ! theApp.m_bServerConnected)
	{
		if (theApp.m_kaDB.kA_Connect("kimAutoDB", "", theApp.settings.m_szAutoServerNameIP, "SYSDBA", "masterkey") != 1)
			return -1;
		theApp.m_bServerConnected = TRUE;
	}

	CTime	creationDate = CTime::GetCurrentTime();
	CFileStatus fileStatus;
	if (CFile::GetStatus(strFullpath, fileStatus))	
		creationDate = fileStatus.m_mtime;
	if (nStatus == -1)
		nStatus = kimAutoDB::inPDF_idle;

	CString strDrive, strDir, strFname, strExt;
	_splitpath((LPCTSTR)strFullpath, strDrive.GetBuffer(_MAX_DRIVE), strDir.GetBuffer(_MAX_DIR), strFname.GetBuffer(_MAX_FNAME), strExt.GetBuffer(_MAX_EXT));
	strDrive.ReleaseBuffer(); strDir.ReleaseBuffer(); strFname.ReleaseBuffer(); strExt.ReleaseBuffer();
	CString strFileName = strFname + strExt;
	CString strPath		= strDrive + strDir;
	return theApp.m_kaDB.ka_SetInputPDF(nPdfID, (std::string)strFileName, (std::string)strPath, 1, nStatus, nJobID, (std::string)creationDate.Format(_T("%Y-%m-%d %H:%M:%S")), (std::string)creationDate.Format(_T("%Y-%m-%d %H:%M:%S")), nNumPages, 2);
}

int KimEngineDBSetOutputSheet(int nJobID, int nPdfID, CPrintSheet& rPrintSheet, int nNumPages, int nStatus)
{
#ifndef KIM_ENGINE
	return -1;
#endif
	if ( ! theApp.m_bServerConnected)
	{
		if (theApp.m_kaDB.kA_Connect("kimAutoDB", "", theApp.settings.m_szAutoServerNameIP, "SYSDBA", "masterkey") != 1)
			return -1;
		theApp.m_bServerConnected = TRUE;
	}

	kimAutoDB_outputPDF outputPDF;
	theApp.m_kaDB.ka_GetOutputPDF(&outputPDF, rPrintSheet.GetNumber().GetBuffer(), nJobID, 1);
	if (rPrintSheet.GetNumber().CompareNoCase(outputPDF.name.c_str()) == 0)
		nPdfID = outputPDF.id;

	if (nStatus == -1)
		nStatus = kimAutoDB::outPDF_idle;

	CThumbnailBitmap thumbnailBitmap;
	CLayout* pLayout = rPrintSheet.GetLayout();
	if (pLayout)
	{
		pLayout->CreateThumbnail(thumbnailBitmap, rPrintSheet, CSize(100,100));

		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		CPDFTargetProperties* pTargetProps = (pDoc) ? pDoc->m_PrintSheetList.m_pdfTargets.Find(rPrintSheet.m_strPDFTarget) : NULL;

		CString strOutputLocation = (pTargetProps) ? pTargetProps->m_strLocation : "";
		CString strOutputFilename = rPrintSheet.GetOutFilename(BOTHSIDES);
		if (strOutputFilename.IsEmpty())	// try again with frontside only
			strOutputFilename	  = rPrintSheet.GetOutFilename(FRONTSIDE);
		CString strOutputFullpath = strOutputLocation + _T("\\") + strOutputFilename;

		return theApp.m_kaDB.ka_SetOutputPDF(nPdfID, rPrintSheet.GetNumberOnly(), (std::string)rPrintSheet.GetNumber(), rPrintSheet.GetNumPages(), rPrintSheet.GetNumPagesOccupied(), nStatus, nJobID, thumbnailBitmap, (std::string)strOutputFullpath, (std::string)_T(""), (std::string)_T(""), 2);
	}
	else
		return -1;
}


/////////// dongle
BOOL CheckDonglePDF(BOOL bInitialCheck, BOOL bUpdateStatusBar)
{
	const int nValueLenght = MAX_TEXT + 2;
	TCHAR	  szValue[nValueLenght];
	HKEY hKey;
	BOOL bDemoKey = FALSE;
	DWORD dwSize = nValueLenght;
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)theApp.m_strRegistryKey, &hKey) == ERROR_SUCCESS)
	{
		if (RegQueryValueEx(hKey, _T("Demo"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		{
			bDemoKey = (_tcscmp(szValue, _T("TRUE")) == 0) ? TRUE : FALSE;
		}
	}
#ifdef KIM_ENGINE
	BOOL bMessage = FALSE;
#else
	BOOL bMessage = (bDemoKey) ? FALSE : TRUE;
#endif
	theApp.m_nAddOnsLicensed = 0;
	RB_SPRO_APIPACKET ApiPacket;
	SP_STATUS         spStatus;
	CString			  strMsg;
	spStatus = RNBOsproFormatPacket( ApiPacket, sizeof(ApiPacket) );
	if (spStatus != 0)
	{
		if ( ! theApp.m_bDemoMode)
		{
			strMsg.Format(IDS_SENTINEL_INVALID_PACKET, spStatus);
			if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
				if (bMessage)
					KIMOpenLogMessage(strMsg, MB_OK);
			theApp.m_bDemoMode = TRUE;
			if (bUpdateStatusBar) UpdateDemoStatusBarPane(CImpManDoc::GetDoc());
		}
	}
	else
	{
		spStatus = RNBOsproInitialize( ApiPacket );
		if (spStatus != 0)
		{
			if ( ! theApp.m_bDemoMode)
			{
				strMsg.Format(IDS_SENTINEL_DRIVER_ERROR, spStatus);
				if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
					if (bMessage)
						KIMOpenLogMessage(strMsg, MB_OK);
				theApp.m_bDemoMode = TRUE;
				if (bUpdateStatusBar) UpdateDemoStatusBarPane(CImpManDoc::GetDoc());
			}
		}
		else
		{
			for (int i = 0; i < 5; i++)
				if ( (spStatus = RNBOsproFindFirstUnit( ApiPacket, 0xd75c )) == 0)
					break;
			BOOL bPrevMode = theApp.m_bDemoMode;
			if (spStatus != 0)
			{
				theApp.m_nDongleStatus = CImpManApp::NoDongle;
				theApp.m_bDemoMode	   = TRUE;
			}
			else
			{
				unsigned short nData;
				RNBOsproRead( ApiPacket, 0x0015, &nData);
				if (nData >= 0x7100)
				{
					RNBOsproRead( ApiPacket, 0x000C, &nData);
					switch (nData)
					{
					case 0xf75c: theApp.m_nDongleStatus = CImpManApp::DigitalDongle;		break;
					case 0xe75c: theApp.m_nDongleStatus = CImpManApp::NewsDongle;			break;
					case 0xd75c: theApp.m_nDongleStatus = CImpManApp::StandardDongle;		break;
                    // 0xd75c = formerly dongle status JDF
					case 0xc75c: theApp.m_nDongleStatus = CImpManApp::ClientDongle;			break;
					case 0xb75c: theApp.m_nDongleStatus = CImpManApp::SmallFormatDongle;	break;
					case 0xa75c: theApp.m_nDongleStatus = CImpManApp::AutoDongle;			break;
					case 0x075c: theApp.m_nDongleStatus = CImpManApp::AutoSmallFormatDongle;break;
					default:	 theApp.m_nDongleStatus = CImpManApp::StandardDongle;		break;
					}
					theApp.m_bDemoMode = FALSE;
				}
				else
				{
					theApp.m_nDongleStatus = CImpManApp::NoDongle; theApp.m_bDemoMode = TRUE;
				}
				if ( (theApp.m_nGUIStyle != CImpManApp::GUIStandard) && (theApp.m_nDongleStatus != CImpManApp::AutoDongle) && (theApp.m_nDongleStatus != CImpManApp::NewsDongle) )
					theApp.m_bDemoMode = TRUE;
				if (bUpdateStatusBar) if (bPrevMode != theApp.m_bDemoMode) UpdateDemoStatusBarPane(CImpManDoc::GetDoc());
				if ( ! theApp.m_bDemoMode)
				{
					RNBOsproRead( ApiPacket, 0x0010, &nData); if (nData != 0) theApp.m_nAddOnsLicensed = theApp.m_nAddOnsLicensed | CImpManApp::JDFMIS;
					RNBOsproRead( ApiPacket, 0x0011, &nData); if (nData != 0) theApp.m_nAddOnsLicensed = theApp.m_nAddOnsLicensed | CImpManApp::JDFLayCrImp;
					RNBOsproRead( ApiPacket, 0x0012, &nData); if (nData != 0) theApp.m_nAddOnsLicensed = theApp.m_nAddOnsLicensed | CImpManApp::JDFCutting;
					RNBOsproRead( ApiPacket, 0x0013, &nData); if (nData != 0) theApp.m_nAddOnsLicensed = theApp.m_nAddOnsLicensed | CImpManApp::JDFFolding;
					RNBOsproRead( ApiPacket, 0x0014, &nData); if (nData != 0) theApp.m_nAddOnsLicensed = theApp.m_nAddOnsLicensed | CImpManApp::Barcode;
					RNBOsproRead( ApiPacket, 0x0016, &nData); if (nData != 0) theApp.m_nAddOnsLicensed = theApp.m_nAddOnsLicensed | CImpManApp::Optimize;
				}
			}
			if (theApp.m_nDongleStatus == CImpManApp::NoDongle)
			{
				/*if ( (bInitialCheck && ! bDemoKey) || (bPrevMode != theApp.m_bDemoMode) )*/
				if ( ! bInitialCheck && (bPrevMode != theApp.m_bDemoMode) )
				{
					strMsg.Format(IDS_SENTINEL_KEY_NOT_FOUND, spStatus);
					if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
						if (bMessage)
							KIMOpenLogMessage(strMsg, MB_OK);
				}
				if (bUpdateStatusBar) UpdateDemoStatusBarPane(CImpManDoc::GetDoc());
			}
		}
	}

	if ( ! theApp.m_bDemoMode && bDemoKey)
		RegSetValueEx(hKey, _T("Demo"),  0, REG_SZ, (LPBYTE)_T("FALSE"), 6);
	else
		if (theApp.m_bDemoMode && ! bDemoKey)
			RegSetValueEx(hKey, _T("Demo"),  0, REG_SZ, (LPBYTE)_T("TRUE"), 6);

	theApp.m_nRemainingDays = theApp.CheckRemainingDays();
	if (theApp.m_bDemoMode && theApp.m_bServerConnected)
	{
		theApp.m_bDemoMode = FALSE;
		theApp.m_nDongleStatus = CImpManApp::ClientDongle;
		if (bUpdateStatusBar) UpdateDemoStatusBarPane(CImpManDoc::GetDoc());
	}

#ifndef KIM_ENGINE
	if ( (theApp.m_nGUIStyle == CImpManApp::GUIStandard) && bInitialCheck)
		if (theApp.m_bDemoMode)// && bDemoKey)
		{
			CDlgDemoOrRegisterDongle dlg;
			dlg.DoModal();
		}
#endif
	
	RegCloseKey(hKey);
	if (theApp.m_bDemoMode && (theApp.m_nRemainingDays <= 0) )
		return TRUE;
	else
		return FALSE;
}




#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImpManApp

BEGIN_MESSAGE_MAP(CImpManApp, CWinApp)
	//{{AFX_MSG_MAP(CImpManApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_SETTINGS_PROGRAM, OnSettingsProgram)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_COMMAND(ID_SETTINGS_PRESSDEVICE, OnSettingsPressdevice)
	ON_COMMAND(ID_IMPOSE, OnSettingsImpose)
	ON_COMMAND(ID_REFERENCE_COLOR, OnReferenceColor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
 
/////////////////////////////////////////////////////////////////////////////
// CImpManApp construction

CImpManApp::CImpManApp()
{
	m_nGUIStyle	     = GUIStandard;
	m_pActiveDoc     = NULL;
	m_nPreviousFrame = -1;
	m_nActualFrame	 = -1;
	m_bInsideNewJobLoop = FALSE;

	//color defines for layout object types
	m_objectColors[0] = RGB(237,237,242);
	m_objectColors[1] = RGB(235,235,150);
	m_objectColors[2] = RGB(213,203,138);

	m_objectColors[3] = RGB(180,180,201);
	m_objectColors[4] = RGB(171,171, 88);
	m_objectColors[5] = RGB(206,186, 47);

	m_objectColors[6] = RGB(126,126,163);
	m_objectColors[7] = RGB(101,101, 50);
	m_objectColors[8] = RGB(152,137, 35);
	// MAX_OBJECT_COLORS = 9

	m_bServerConnected = FALSE;
	m_bSocketsInitialized = FALSE;
	m_nDBCurrentJobID = -1;
	m_bKimEngineAbort = FALSE;
	m_strDBCurrentJobFullpath = _T("");
	m_bPDFEngineInitialized = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CImpManApp object

CImpManApp theApp; 

/////////////////////////////////////////////////////////////////////////////
// CImpManApp initialization




#include <windows.h> 
#include <dbghelp.h>  

typedef BOOL (__stdcall *tMDWD)(IN HANDLE hProcess, IN DWORD ProcessId, IN HANDLE hFile, IN MINIDUMP_TYPE DumpType, IN CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam, OPTIONAL IN CONST PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam, 
								OPTIONAL IN CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam OPTIONAL); 

static tMDWD s_pMDWD; 
static HMODULE s_hDbgHelpMod = NULL; 
static MINIDUMP_TYPE s_dumpTyp = (MINIDUMP_TYPE)(MiniDumpWithFullMemory | MiniDumpWithDataSegs); 

LONG WriteMiniDump(EXCEPTION_POINTERS* pEx)
{
#ifndef _DEBUG
	return 0;
#endif

	COleDateTime timeStamp(COleDateTime::GetCurrentTime());
	CString		 strFile; 
	CString		 strFolder = CString(theApp.settings.m_szDataDir) + _T("\\Dumps");
	CreateDirectory(strFolder, NULL);
#ifdef KIM_ENGINE
	strFile.Format(_T("%s\\KIMEngine_Dump_%s.dmp"), strFolder, timeStamp.Format(_T("%Y-%m-%d_%H-%M-%S")));
#else
	strFile.Format(_T("%s\\KIMClient_Dump_%s.dmp"), strFolder, timeStamp.Format(_T("%Y-%m-%d_%H-%M-%S")));
#endif 

	bool bFailed = true; 
	HANDLE hFile; 
	hFile = CreateFile(strFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL); 
	if (hFile != INVALID_HANDLE_VALUE) 
	{ 
		if (pEx)
		{
			MINIDUMP_EXCEPTION_INFORMATION stMDEI; 
			stMDEI.ThreadId = GetCurrentThreadId(); 
			stMDEI.ExceptionPointers = pEx; 
			stMDEI.ClientPointers = TRUE; 
			// try to create an miniDump: 
			if (s_pMDWD(GetCurrentProcess(), GetCurrentProcessId(), hFile, s_dumpTyp, &stMDEI, NULL, NULL)) 
				bFailed = false;  // suceeded 
		}
		else
		{
			if (s_pMDWD(GetCurrentProcess(), GetCurrentProcessId(), hFile, s_dumpTyp, NULL, NULL, NULL)) 
				bFailed = false;  // suceeded 
		}

		CloseHandle(hFile); 
	} 

	if (bFailed) 
	{ 
		//FatalAppExit(-1, strFile); 
		return EXCEPTION_CONTINUE_SEARCH; 
	} 

	// Optional display an error message 
	//FatalAppExit(-1, _T("Application failed!")); 

	// or return one of the following: 
	// - EXCEPTION_CONTINUE_SEARCH 
	// - EXCEPTION_CONTINUE_EXECUTION 
	// - EXCEPTION_EXECUTE_HANDLER 
	return EXCEPTION_CONTINUE_SEARCH;  // this will trigger the "normal" OS error-dialog 
} 

static LONG __stdcall MyCrashHandlerExceptionFilter(EXCEPTION_POINTERS* pEx) 
{   
#ifdef _M_IX86 
	if (pEx)
	{
		if (pEx->ExceptionRecord->ExceptionCode == EXCEPTION_STACK_OVERFLOW)   
		{ 
			// be sure that we have enought space... 
			static char MyStack[1024*128];   
			// it assumes that DS and SS are the same!!! (this is the case for Win32) 
			// change the stack only if the selectors are the same (this is the case for Win32) 
			//__asm push offset MyStack[1024*128]; 
			//__asm pop esp; 
			__asm mov eax,offset MyStack[1024*128]; 
			__asm mov esp,eax; 
		}
	}
#endif 

	return WriteMiniDump(pEx);
}

void InitMiniDumpWriter() 
{  
	if (s_hDbgHelpMod != NULL) 
		return; 

	// Initialize the member, so we do not load the dll after the exception has occured 
	// which might be not possible anymore... 
	CString strDLL; strDLL.Format(_T("%s\\dbghelp.dll"), theApp.settings.m_szWorkDir);
	s_hDbgHelpMod = LoadLibrary(strDLL); 
	if (s_hDbgHelpMod != NULL) 
		s_pMDWD = (tMDWD) GetProcAddress(s_hDbgHelpMod, "MiniDumpWriteDump"); 

	// Register Unhandled Exception-Filter: 
	SetUnhandledExceptionFilter(MyCrashHandlerExceptionFilter); 

	// Additional call "PreventSetUnhandledExceptionFilter"... 
	// See also: "SetUnhandledExceptionFilter" and VC8 (and later) 
	// http://blog.kalmbachnet.de/?postid=75 
} 


static BOOL bClassRegistered = FALSE;
ULONG_PTR g_gdiplusToken;


BOOL CImpManApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	 CWinApp::InitInstance();

   // Initialize GDI+.
	GdiplusStartupInput gdiplusStartupInput;
	GdiplusStartup(&g_gdiplusToken, &gdiplusStartupInput, NULL);	

	//D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &pDirect2dFactory);
	//D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(	D2D1_RENDER_TARGET_TYPE_HARDWARE, D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED),	0, 0, D2D1_RENDER_TARGET_USAGE_NONE, D2D1_FEATURE_LEVEL_DEFAULT);
	//pDirect2dFactory->CreateDCRenderTarget(&props, &pRenderTarget);

	m_strRegistryKey = _T("SOFTWARE\\KRAUSE\\KIM7.1 Settings");

	try	
	{ 
		JDF::PlatformUtils::Initialize();
	} 
	catch (const JDF::Exception&)	
	{
		return FALSE;
	}

	CKIMCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Register our unique class name that we wish to use
	WNDCLASS wndcls;
	memset(&wndcls, 0, sizeof(WNDCLASS));   // start with NULL defaults
	wndcls.style = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
	wndcls.lpfnWndProc = ::DefWindowProc;
	wndcls.hInstance = AfxGetInstanceHandle(); 
	wndcls.hIcon = LoadIcon(IDR_MAINFRAME); // or load a different icon
	wndcls.hCursor = LoadCursor( IDC_ARROW );
	wndcls.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
	wndcls.lpszMenuName = NULL;
	// Specify our own class name for using FindWindow later

#ifdef KIM_ENGINE
	wndcls.lpszClassName = theApp.m_strClassName = _T("KIMEngineApp");
	if (CWnd::FindWindow(theApp.m_strClassName, NULL) )	// if an instance is already running -> close
		return FALSE;
#else
	wndcls.lpszClassName = theApp.m_strClassName = _T("KIMClientApp");
#endif

	// Register new class and exit if it fails
	 if(!AfxRegisterClass(&wndcls))
	{
		TRACE("Class Registration Failed\n");
		return FALSE;
	}
	bClassRegistered = TRUE;
	////////////////////////////////////////////////

	m_bInsideInitInstance = TRUE;

	// Fills theApp.strName and theApp.strVersion for about-box and document-informat ion
	GetVersionInfo();
 
	settings.OnLoadingSettings();

	CString strModule("KimRes");
	CString strLanguage;
	switch (settings.m_iLanguage)
	{
		case 0: strLanguage = _T("GER"); setlocale(LC_ALL, "german");	break;
		case 1: strLanguage = _T("ENG"); setlocale(LC_ALL, "english");	break;
		case 2: strLanguage = _T("FRA"); break;
		case 3: strLanguage = _T("ITA"); break;
		case 4: strLanguage = _T("SPA"); break;
		case 5: strLanguage = _T("HOL"); break;	// No holl. MFC4x.dll available
		//Default would already be set at reading ini-file
	}

	CString strDLLName = strModule + strLanguage + CString(".DLL");
	hResourceInst = LoadLibrary((LPCTSTR)strDLLName);

	if (hResourceInst != NULL)
		AfxSetResourceHandle(hResourceInst);
	else
		; // TODO: Errormessage ? -> No ressource-DLL for this language present
		  // For default the linked resources will be load

#ifdef KIM_ENGINE
	CheckDonglePDF(TRUE, FALSE);
	if (theApp.m_kaDB.kA_Connect("kimAutoDB", "", theApp.settings.m_szAutoServerNameIP, "SYSDBA", "masterkey") != -1)
		theApp.m_bServerConnected = TRUE;
#else
	theApp.m_bServerConnected = FALSE;
	m_bDemoMode = FALSE;
	if (strlen(theApp.settings.m_szAutoServerNameIP) > 0)
	{
		BeginWaitCursor();
		theApp.AutoServerConnect();
		
		CDlgConnectToServer dlg;
		dlg.DoModal();
		
		EndWaitCursor();
		CheckDonglePDF(TRUE, FALSE);
	}
	else	// when trying to connect, dongle check will be done in CClientSocket::OnReceive()
	{
		if (CheckDonglePDF(TRUE, FALSE))		// TRUE = initial check  FALSE = update status bar
			return FALSE;
	}
#endif

	TCHAR szCurrentDir[_MAX_PATH];
	_tcscpy_s(theApp.settings.m_szWorkDir, _MAX_PATH, theApp.GetDefaultInstallationFolder());
	if ( ! PathFileExists(theApp.settings.m_szWorkDir))
		if (GetCurrentDirectory(_MAX_PATH, szCurrentDir))
			_tcscpy_s(theApp.settings.m_szWorkDir, _MAX_PATH, szCurrentDir);

	if (_tcslen(theApp.settings.m_szWorkDir))
		if (theApp.settings.m_szWorkDir[_tcslen(theApp.settings.m_szWorkDir) - 1] != '\\')
			_tcscat(theApp.settings.m_szWorkDir, _T("\\"));

	theApp.SetMyCurrentDirectory(theApp.settings.m_szWorkDir, TRUE);
	CString strDataFolder = GetDataFolder();	// check if local data folder is not the network folder for some reason
	if ( ! DirectoryExist(strDataFolder))
	{
		CString strMsg; 
		AfxFormatString1(strMsg, IDS_DATAFOLDER_INVALID, strDataFolder);
		KIMOpenLogMessage(strMsg, MB_ICONERROR);
	}

	InitMiniDumpWriter();  

	///////////////////////// initialize pdfEngine 
	if( IPL_GetPDFImpositionDLLVersion() != 0x00020005) 
	{
		KIMOpenLogMessage(_T("pdfEngine library version incorrect, must be 0x00020005!"), MB_ICONERROR);
		return FALSE;
	}

	CSysToUTF8Converter s2utf8;
	PTB_EError nError = PTB_LibInit(callas_keycode, NULL, s2utf8.Convert(theApp.settings.m_szWorkDir));		// PTB_LibRelease() in ExitInstance()
	if (nError)
	{
		CString	strMsg;	strMsg.Format(_T("Error initializing pdfEngine! Code: %d."), nError);	// when init fails, we cannot use IPL_GetErrorStringFromErrorCode() -> causes crash
		KIMOpenLogMessage(strMsg, MB_ICONERROR);
		m_bPDFEngineInitialized = FALSE;
	}
	else
		m_bPDFEngineInitialized = TRUE;
	///////////////////////////////////////////////

	m_colorDefTable.Load();

	if (theApp.settings.m_bOnStartDelTemp)
	{
		CString FileToDelete;
		WIN32_FIND_DATA findData;
		TCHAR searchbuffer[MAX_PATH];
		_tcscpy_s(searchbuffer, MAX_PATH, theApp.settings.m_szTempDir); // TODO: Verify the path
		_tcscat_s(searchbuffer, MAX_PATH, _T("\\*.tmp"));
		HANDLE hfileSearch = FindFirstFile(searchbuffer, &findData);
		if (hfileSearch != INVALID_HANDLE_VALUE)
		{ 
			do {
				FileToDelete = theApp.settings.m_szTempDir; // TODO: Verify the path
				FileToDelete += findData.cFileName;
				DeleteFile(LPCTSTR(FileToDelete));
			}while ( FindNextFile( hfileSearch, &findData ));
		}
		FindClose(hfileSearch);
	}

	m_strCoverName.LoadString(IDS_COVER_NAME);

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		KIMOpenLogMessage(IDP_OLE_INIT_FAILED, MB_ICONERROR);
		return FALSE;
	}

	AfxEnableControlContainer();

	ResetLogFile();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	MyReadRecentFileList();

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	m_pAutoServerTemplate = new CMultiDocTemplate(
		IDR_AUTOSERVERFRAME,
		RUNTIME_CLASS(CImpManDoc),
		RUNTIME_CLASS(CAutoServerFrame), // custom MDI child frame
		RUNTIME_CLASS(CAutoServerJobsView));
	m_pOrderDataTemplate = new CMultiDocTemplate(
		IDR_ORDERDATAFRAME,
		RUNTIME_CLASS(CImpManDoc), 
		RUNTIME_CLASS(COrderDataFrame), // custom MDI child frame
		RUNTIME_CLASS(COrderDataView)); 
	m_pJobDocumentationTemplate = new CMultiDocTemplate(
		IDR_JOBDOCUMENTATIONFRAME,
		RUNTIME_CLASS(CImpManDoc),
		RUNTIME_CLASS(CJobDocumentationFrame), // custom MDI child frame
		RUNTIME_CLASS(CJobDocumentationView));
	m_pBookBlockTemplate = new CMultiDocTemplate(
		IDR_BOOKBLOCKFRAME,
		RUNTIME_CLASS(CImpManDoc),
		RUNTIME_CLASS(CBookblockFrame), // custom MDI child frame
		RUNTIME_CLASS(CBookBlockView));
	m_pNewPrintSheetTemplate = new CMultiDocTemplate(
		IDR_NEWPRINTSHEETFRAME,
		RUNTIME_CLASS(CImpManDoc),
		RUNTIME_CLASS(CNewPrintSheetFrame), // custom MDI child frame
		RUNTIME_CLASS(CPrintSheetView));
	m_pPageListTemplate = new CMultiDocTemplate(
		IDR_PAGELISTFRAME,
		RUNTIME_CLASS(CImpManDoc),
		RUNTIME_CLASS(CPageListFrame), // custom MDI child frame
		RUNTIME_CLASS(CPageListView));

//	m_pBookBlockTemplate->SetContainerInfo(IDR_IMPMANTYPE_CNTR_IP);
//	m_pOrderDataTemplate->SetContainerInfo(IDR_IMPMANTYPE_CNTR_IP);
	//AddDocTemplate(m_pAutoServerTemplate);
	AddDocTemplate(m_pOrderDataTemplate);
	//AddDocTemplate(m_pJobDocumentationTemplate);
	//AddDocTemplate(m_pBookBlockTemplate);
	//AddDocTemplate(m_pNewPrintSheetTemplate);
	//AddDocTemplate(m_pPageListTemplate);

#ifndef KIM_ENGINE
//	PlaySound(MAKEINTRESOURCE(IDR_WAVE_INTRO), theApp.m_hInstance, SND_RESOURCE | SND_ASYNC);
	CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);
#endif

  	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame; 
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME)) 
		return FALSE;
	m_pMainWnd = pMainFrame;

	m_nScreenDPI = 96;
	if (pMainFrame)
	{
		CClientDC dc(pMainFrame);
		m_nScreenDPI = dc.GetDeviceCaps(LOGPIXELSX); 
	}

	// Enable drag/drop open 
	m_pMainWnd->DragAcceptFiles();

	// Enable DDE Execute open 
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);
	
	// Load global theApp.m_PressDeviceList
	if ( ! (CDlgPressDevice::OnLoadingData()))
		KIMOpenLogMessage(IDS_PRESSDEVICE_NOT_LOAD, MB_ICONWARNING);

	theApp.m_paperDefsList.LoadData();
	CDlgProductionManager::LoadProfileList();

	//Load Cursors
	m_CursorCopy			= LoadCursor(IDC_POINTER_COPY);
	m_CursorNoDrop			= LoadCursor(IDC_NODROP);
	m_CursorTrim			= LoadCursor(IDC_TRIM);
	m_CursorAlignSection	= LoadCursor(IDC_ALIGN_SECTION);
	m_CursorMagnifyIncrease = LoadCursor(IDC_MAGNIFY_INCREASE);
	m_CursorMagnifyDecrease = LoadCursor(IDC_MAGNIFY_DECREASE);
	m_CursorNoMagnify		= LoadCursor(IDC_NOMAGNIFY);
	m_CursorHand			= LoadCursor(IDC_GRABBING_HAND);
	m_CursorStandard		= LoadStandardCursor(IDC_ARROW);
	m_CursorSepCut			= LoadCursor(IDC_SEP_CUT);
	m_CursorSizeWE			= LoadStandardCursor(IDC_SIZEWE);

	m_pDlgMarkSet			 = new CDlgMarkSet;
	m_pDlgPDFLibStatus		 = new CDlgPDFLibStatus;

	// Register the ownerdrawn ColorComboBox
	CColorComboBox::RegisterControlClass();		

	// if 256 color device create logical palette for specific colors
	CreateKimPalette();

	//First free the string allocated by MFC at CWinApp startup.
	//The string is allocated before InitInstance is called.
	free((void*)m_pszHelpFilePath);
	//Change the name of the .HLP file.
	//The CWinApp destructor will free the memory.
	CString strHelpfile;
	strHelpfile.Format(_T("%s\\Imposition Manager.hlp"), theApp.settings.m_szWorkDir);
	m_pszHelpFilePath=_tcsdup(strHelpfile);

#ifdef KIM_ENGINE
	pMainFrame->ShowWindow(SW_HIDE);
	pMainFrame->UpdateWindow(); 
	if (cmdInfo.m_nKIMCommand != -1)		
		if (ProcessCommand(cmdInfo, theApp.m_nAppError) == 0)	// nAppError used in ExitInstance()
			return FALSE;	// exit program
#else									// startup in command mode
	if (cmdInfo.m_nKIMCommand == -1)		// normal startup (not in command mode)
	{
		if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileNothing)
			OpenIfAutoBackupFile(cmdInfo);

		if (!ProcessShellCommand(cmdInfo))
			return FALSE;
		if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileNew && theApp.settings.m_bOnStartFileNew)
			if ( ! OnWizard())
				CImpManDoc::GetDoc()->OnCloseDocument();

		int  nCmdShow		 = SW_SHOWMAXIMIZED;
		BOOL bStatusRestored = FALSE;
		CMainFrame::RestoreFrameStatus(pMainFrame, bStatusRestored, nCmdShow);
		pMainFrame->ShowWindow(nCmdShow);
		pMainFrame->UpdateWindow(); 
		pMainFrame->OnUpdateFrameTitle(TRUE);
	}
	else									// startup in command mode
	{
		//cmdInfo.m_nKIMCommand = CImpManApp::CmdDoOutput;
		//cmdInfo.m_strJobTemplate = "C:\\Program Files (x86)\\KIM7.1\\KIM Auto Jobs\\Harms\\Pool_6492_XL106_01_09_14_19_46\\Pool_6492_XL106_01_09_14_19_46.job";
		if (cmdInfo.m_nKIMCommand != CImpManApp::CmdRunEngine)		
			if (ProcessCommand(cmdInfo, theApp.m_nAppError) == 0)	// nAppError used in ExitInstance()
				return FALSE;	// exit program
	}
#endif

	m_bInsideInitInstance = FALSE;

	return TRUE;
}

//BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle,
//	CWnd* pParentWnd, CCreateContext* pContext)
//{
//	if (!CFrameWnd::LoadFrame(nIDResource, dwDefaultStyle,
//	  pParentWnd, pContext))
//		return FALSE;
//
//	// save menu to use when no active MDI child window is present
//	ASSERT(m_hWnd != NULL);
//	m_hMenuDefault = ::GetMenu(m_hWnd);
//	if (m_hMenuDefault == NULL)
//		TRACE(traceAppMsg, 0, "Warning: CMDIFrameWnd without a default menu.\n");
//	return TRUE;
//}

void AFXAPI AfxHookWindowCreate(CWnd* pWnd);
BOOL AFXAPI AfxUnhookWindowCreate();

BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle,
	CWnd* pParentWnd, CCreateContext* pContext)
{
	// only do this once
	//ASSERT_VALID_IDR(nIDResource);
	//ASSERT(m_nIDHelp == 0 || m_nIDHelp == nIDResource);

	m_nIDHelp = nIDResource;    // ID for help context (+HID_BASE_RESOURCE)

	CString strFullString;
	if (strFullString.LoadString(nIDResource))
		AfxExtractSubString(m_strTitle, strFullString, 0);    // first sub-string

	//VERIFY(AfxDeferRegisterClass(AFX_WNDFRAMEORVIEW_REG));

	// attempt to create the window
	//LPCTSTR lpszClass = GetIconWndClass(dwDefaultStyle, nIDResource);
	CString strTitle = m_strTitle;
	//if (!Create(/*lpszClass*/NULL, strTitle, dwDefaultStyle, rectDefault,
	//  pParentWnd, ATL_MAKEINTRESOURCE(nIDResource), 0L, pContext))
	//{
	//	return FALSE;   // will self destruct on failure normally
	//}


	LPCTSTR lpszMenuName = ATL_MAKEINTRESOURCE(nIDResource);
	HMENU hMenu = NULL;
	if (lpszMenuName != NULL)
	{
		// load in a menu that will get destroyed when window gets destroyed
		HINSTANCE hInst = AfxFindResourceHandle(lpszMenuName, ATL_RT_MENU);
		if ((hMenu = ::LoadMenu(hInst, lpszMenuName)) == NULL)
		{
			TRACE(traceAppMsg, 0, "Warning: failed to load menu for CFrameWnd.\n");
			PostNcDestroy();            // perhaps delete the C++ object
			return FALSE;
		}
	}

	m_strTitle = strTitle;    // save title for later

	//if (!CreateEx(0L, NULL/*lpszClassName*/, strTitle, dwDefaultStyle,
	//	rectDefault.left, rectDefault.top, rectDefault.right - rectDefault.left, rectDefault.bottom - rectDefault.top,
	//	pParentWnd->GetSafeHwnd(), hMenu, (LPVOID)pContext))
	//{
	//	TRACE(traceAppMsg, 0, "Warning: failed to create CFrameWnd.\n");
	//	if (hMenu != NULL)
	//		DestroyMenu(hMenu);
	//	return FALSE;
	//}


	// allow modification of several common create parameters
	CREATESTRUCT cs;
	cs.dwExStyle = 0L;
	cs.lpszClass = NULL;
	cs.lpszName = strTitle;
	cs.style = dwDefaultStyle;
	cs.x = rectDefault.left;
	cs.y = rectDefault.top;
	cs.cx = rectDefault.right - rectDefault.left;
	cs.cy = rectDefault.bottom - rectDefault.top;
	cs.hwndParent = pParentWnd->GetSafeHwnd();
	cs.hMenu = hMenu;
	cs.hInstance = AfxGetInstanceHandle();
	cs.lpCreateParams = pContext;

	if (!PreCreateWindow(cs))
	{
		PostNcDestroy();
		return FALSE;
	}

	AfxHookWindowCreate(this);

	HWND hWnd = ::CreateWindowEx(cs.dwExStyle, cs.lpszClass, cs.lpszName, cs.style, cs.x, cs.y, cs.cx, cs.cy, cs.hwndParent, cs.hMenu, cs.hInstance, cs.lpCreateParams);

#ifdef _DEBUG
	if (hWnd == NULL)
	{
		TRACE(traceAppMsg, 0, "Warning: Window creation failed: GetLastError returns 0x%8.8X\n",
			GetLastError());
	}
#endif

	if (!AfxUnhookWindowCreate())
		PostNcDestroy();        // cleanup if CreateWindowEx fails too soon

	if (hWnd == NULL)
		return FALSE;
	ASSERT(hWnd == m_hWnd); // should have been set in send msg hook


	// save the default menu handle
	ASSERT(m_hWnd != NULL);
	m_hMenuDefault = ::GetMenu(m_hWnd);

	// load accelerator resource
	LoadAccelTable(ATL_MAKEINTRESOURCE(nIDResource));

	if (pContext == NULL)   // send initial update
		SendMessageToDescendants(/*WM_INITIALUPDATE*/0x0364, 0, 0, TRUE, TRUE);

	return TRUE;
}

BOOL CImpManApp::FirstInstance(CKIMCommandLineInfo& rCmdInfo)
{
	CWnd *pWndPrev, *pWndChild;
	
	// Determine if another window with our class name exists...
	if (pWndPrev = CWnd::FindWindow(theApp.m_strClassName,NULL))
	{
		// if so, does it have any popups?
		pWndChild = pWndPrev->GetLastActivePopup();
		
		// If iconic, restore the main window
		if (pWndPrev->IsIconic())
			pWndPrev->ShowWindow(SW_RESTORE);
		
		// Bring the main window or its popup to
		// the foreground
		pWndChild->SetForegroundWindow();

		if (pWndPrev == pWndChild)	// main window is in foreground -> we can open the new document
			if (rCmdInfo.m_strFileName.GetLength())
			{
				COPYDATASTRUCT cds;
				cds.dwData = CImpManApp::CmdOpen;
				cds.cbData = rCmdInfo.m_strFileName.GetLength() + sizeof(TCHAR);
				cds.lpData = rCmdInfo.m_strFileName.GetBuffer(512);
				pWndChild->SendMessage(WM_COPYDATA, (WPARAM)0, (LPARAM)&cds);
			}
		
		// and we are done activating the previous one.
		return FALSE;
	}
	// First instance. Proceed as normal.
	else
		return TRUE;
}


void CKIMCommandLineInfo::ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast)
{
	m_nJobID = -1;

	CString strParam(lpszParam);

	if (bFlag)	// command
	{
		strParam.MakeUpper();
		if (strParam == "N")	// new template
			m_nKIMCommand = CImpManApp::CmdNew;

		if (strParam == "O")	// open template
			m_nKIMCommand = CImpManApp::CmdOpen;

		if (strParam == "OL")	// open template's output list
			m_nKIMCommand = CImpManApp::CmdOpenOutputList;

		if (strParam == "OI")	// get last output information (PDFSheets)
			m_nKIMCommand = CImpManApp::CmdGetOutputInfo;

		if (strParam == "CR")	// check registered pdf docs
			m_nKIMCommand = CImpManApp::CmdCheckRegisteredPDFs;

		if (strParam == "IR")	// register pdf doc
			m_nKIMCommand = CImpManApp::CmdIsRegistered;

		if (strParam == "R")	// register pdf doc
			m_nKIMCommand = CImpManApp::CmdRegister;

		if (strParam == "UR")	// register pdf doc
			m_nKIMCommand = CImpManApp::CmdUnregister;

		if (strParam == "I")	// impose
			m_nKIMCommand = CImpManApp::CmdImpose;

		if (strParam == "IQ")	// impose quiet
			m_nKIMCommand = CImpManApp::CmdImposeQuiet;

		if (strParam == "IC")	// impose csv file
			m_nKIMCommand = CImpManApp::CmdImposeCSV;

		if (strParam == "IX")	// impose xml file
			m_nKIMCommand = CImpManApp::CmdImposeXML;

		if (strParam == "IJ")	// impose jdf file
			m_nKIMCommand = CImpManApp::CmdImposeJDF;

		if (strParam == "DO")	// do output
			m_nKIMCommand = CImpManApp::CmdDoOutput;

		if (strParam == "RD")	// reset page source (pdf doc)
			m_nKIMCommand = CImpManApp::CmdResetPageSource;

		if (strParam == "XD")	// reset page source (pdf doc) w/o resetting page numbers to originals, because doc is about to be exchanged (new version)
			m_nKIMCommand = CImpManApp::CmdResetPageSourceX;

		if (strParam == "RP")	// reset pagelist
			m_nKIMCommand = CImpManApp::CmdResetPageList;

		if (strParam == "P")	// show pagelist
			m_nKIMCommand = CImpManApp::CmdPageList;

		if (strParam == "KE")	// start as KimEngine
			m_nKIMCommand = CImpManApp::CmdRunEngine;

		if (strParam == "Q")	// quiet mode - don't show user interface
			m_bQuiet = TRUE;

		m_nParam = 1;
	}
	else		// parameter
	{
		switch (m_nParam)
		{
		case 0: m_strFileName		= strParam;	break;
		case 1: m_strJobTemplate	= strParam;	break;
		case 2: m_strPDFDoc			= strParam;	break;
		case 3: m_strStartPage		= strParam;	break;
		case 4: m_strPDFInfoFile	= strParam;	break;
		case 5: m_strPreviewsFolder = strParam;	break;
		default:								break;
		}

		m_nParam++;
	}

	if (bLast)
	{
		if (m_bQuiet)
			theApp.m_nGUIStyle = CImpManApp::GUIQuiet;
		else
		{
			switch (m_nKIMCommand)
			{
			case CImpManApp::CmdNew:				theApp.m_nGUIStyle = CImpManApp::GUINewTemplate;	break;
			case CImpManApp::CmdOpen:				theApp.m_nGUIStyle = CImpManApp::GUIOpenTemplate;	break;
			case CImpManApp::CmdOpenOutputList:		theApp.m_nGUIStyle = CImpManApp::GUIOpenOutputList;	break;
			case CImpManApp::CmdGetOutputInfo:		theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdCheckRegisteredPDFs:theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdIsRegistered:		theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdRegister:			theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdUnregister:			theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdImpose:				theApp.m_nGUIStyle = CImpManApp::GUIAutoImpose;		break;
			case CImpManApp::CmdImposeCSV:			
			case CImpManApp::CmdImposeXML:			
			case CImpManApp::CmdImposeJDF:			theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdDoOutput:			theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdImposeQuiet:		theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdResetPageList:		theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdResetPageSource:	theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdResetPageSourceX:	theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			case CImpManApp::CmdPageList:			theApp.m_nGUIStyle = CImpManApp::GUIPageList;		break;
			case CImpManApp::CmdRunEngine:			theApp.m_nGUIStyle = CImpManApp::GUIQuiet;			break;
			default:								theApp.m_nGUIStyle = CImpManApp::GUIStandard;		break;
			}
		}

		if (m_strFileName.IsEmpty())
			m_nShellCommand = CCommandLineInfo::FileNothing;
		else
			m_nShellCommand = CCommandLineInfo::FileOpen;
	}
}

int	CImpManApp::ProcessCommand(CKIMCommandLineInfo& rCmdInfo, int& nError)
{
	if (rCmdInfo.m_nKIMCommand == CImpManApp::CmdNew)
		OnFileNew();
	else
	{
		if ( (rCmdInfo.m_nKIMCommand == CImpManApp::CmdImposeCSV) || (rCmdInfo.m_nKIMCommand == CImpManApp::CmdImposeXML)  || (rCmdInfo.m_nKIMCommand == CImpManApp::CmdImposeJDF) )
		{
			OnFileNew();
			CImpManDoc* pDoc = CImpManDoc::GetDoc(); 
			if (pDoc) 
			{
				CString strDrive, strDir, strFname, strExt;
				_splitpath((LPCTSTR)rCmdInfo.m_strJobTemplate, strDrive.GetBuffer(_MAX_DRIVE), strDir.GetBuffer(_MAX_DIR), strFname.GetBuffer(_MAX_FNAME), strExt.GetBuffer(_MAX_EXT));
				pDoc->SetTitle(strFname);
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);
				//rCmdInfo.m_strJobTemplate = pDoc->GetPathName();	// because path name could have been revised (i.e. double backslashes removed etc.)
				pDoc->SetPathName(rCmdInfo.m_strJobTemplate, FALSE);
			}
		}
		else
			OpenDocumentFile(rCmdInfo.m_strJobTemplate);
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
	{
		CString strOut; strOut.Format(_T("Cmd %d >Error open doc %s\r\n"), rCmdInfo.m_nKIMCommand, rCmdInfo.m_strJobTemplate);
		theApp.LogEntry(strOut);
		return -1;	
	}

	if ( (rCmdInfo.m_nKIMCommand != CImpManApp::CmdNew) && (rCmdInfo.m_nKIMCommand != CImpManApp::CmdImposeCSV) && (rCmdInfo.m_nKIMCommand != CImpManApp::CmdImposeXML) && (rCmdInfo.m_nKIMCommand != CImpManApp::CmdImposeJDF) )
		rCmdInfo.m_strJobTemplate = pDoc->GetPathName();	// because path name could have been revised (i.e. double backslashes removed etc.)

	if (theApp.CalledFromKPM())		// when called from KPM, following naming template and location is needed by definition
	{
		for (int i = 0; i < pDoc->m_PrintSheetList.m_pdfTargets.GetSize(); i++)
		{
			CString strLocation	 = CString(theApp.settings.m_szWorkDir) + _T("\\PDF-Output");
			strLocation.Replace(_T("\\\\"), _T("\\"));

			CPDFTargetProperties& rTargetProps	= pDoc->m_PrintSheetList.m_pdfTargets[i];
			rTargetProps.m_strOutputFilename	= _T("KIM_$7_$9.pdf");
			rTargetProps.m_strLocation			= rTargetProps.m_strLocationBack = strLocation;
		}
	}

	switch (rCmdInfo.m_nKIMCommand)
	{
	case CImpManApp::CmdNew:
		{
			pDoc->SetTitle(rCmdInfo.m_strJobTemplate);

			if ( ! rCmdInfo.m_strPDFDoc.IsEmpty())
			{
				// same as page list command
				nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc);

				CMainFrame frame;
				frame.CreateOrActivatePageListFrame(theApp.m_pPageListTemplate, pDoc);
				CPageListView* pView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
				if (pView)
				{
					CPageListFrame* pPageListFrame = (CPageListFrame*)pView->GetParentFrame();
					if (pPageListFrame)
					{
						pPageListFrame->ShowWindow(SW_SHOWMAXIMIZED);
						m_pMainWnd->SetMenu(NULL);
						m_pMainWnd->ShowWindow(SW_SHOWNORMAL);
						m_pMainWnd->UpdateWindow();
					}
				}
				pDoc->SetModifiedFlag();
			}
			else
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);
			return 1;	// don't exit program
		}
		break;

	case CImpManApp::CmdOpen:
		{
			CMainFrame frame;
			frame.CreateOrActivatePageListFrame(theApp.m_pPageListTemplate, pDoc);
			CPageListView* pView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
			if (pView)
			{
				CPageListFrame* pPageListFrame = (CPageListFrame*)pView->GetParentFrame();
				if (pPageListFrame)
				{
					pPageListFrame->ShowWindow(SW_SHOWMAXIMIZED);
					m_pMainWnd->ShowWindow(SW_SHOWNORMAL);
					m_pMainWnd->UpdateWindow();
				}
			}
			return 1;	// don't exit program
		}
		break;

	case CImpManApp::CmdOpenOutputList:
		{
			CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, pDoc);
			if (pFrame)
				pFrame->SetupViewOutputAllSheets();
			((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection = CMainFrame::NavSelectionOutput;
			CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
			if (pView)
			{
				CNewPrintSheetFrame* pNewPrintSheetFrame = (CNewPrintSheetFrame*)pView->GetParentFrame();
				if (pNewPrintSheetFrame)
				{
					pNewPrintSheetFrame->ShowWindow(SW_SHOWMAXIMIZED);

					pView->m_DisplayList.Invalidate();
					pView->Invalidate();
					pNewPrintSheetFrame->m_bShowPlate		  = TRUE;
					pNewPrintSheetFrame->m_bShowExposures	  = TRUE;
					pView->m_PrintSheetTable.Initialize(CPrintSheetTable::ShowOutputList);
					//pView->OnMaxminButton();
					m_pMainWnd->ShowWindow(SW_SHOWNORMAL);
					m_pMainWnd->UpdateWindow();
				}
			}
			return 1;	// don't exit program
		}
		break;

	case CImpManApp::CmdGetOutputInfo:		
		{
			theApp.LogEntry(_T("/oi "), rCmdInfo.m_strJobTemplate);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc);
			theApp.LogEntry(nError);
			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdCheckRegisteredPDFs:		
		{
			theApp.LogEntry(_T("/cr "), rCmdInfo.m_strJobTemplate);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand);
			theApp.LogEntry(nError);
			if (nError == 1)	// PDF(s) has been removed
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);
			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdIsRegistered:		
		{
			theApp.LogEntry(_T("/ir "), rCmdInfo.m_strJobTemplate, rCmdInfo.m_strPDFDoc);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc);
			theApp.LogEntry(nError);
			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdRegister:		
		{
			theApp.LogEntry(_T("/r "), rCmdInfo.m_strJobTemplate, rCmdInfo.m_strPDFDoc, rCmdInfo.m_strStartPage);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc, rCmdInfo.m_strStartPage);
			theApp.LogEntry(nError);
			if (nError == 1)	// 1 = registered ; 2 = is already registered -> don't save
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);

			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdUnregister:		
		{
			theApp.LogEntry(_T("/ur "), rCmdInfo.m_strJobTemplate, rCmdInfo.m_strPDFDoc);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc);
			theApp.LogEntry(nError);
			if (nError == 1)	// 1 = unregistered ; 2 = is already unregistered -> don't save
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);

			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdImpose:		
	case CImpManApp::CmdImposeQuiet:		
		{
			theApp.LogEntry((rCmdInfo.m_nKIMCommand == CImpManApp::CmdImpose) ? _T("/i ") : _T("/iq "), rCmdInfo.m_strJobTemplate, rCmdInfo.m_strPDFDoc);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc);
			theApp.LogEntry(nError);
			if (nError != 0)	// < 0 = error ; 0 = nothing happened ; > 0 = no. sheets outputted
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);

			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdImposeCSV:		
		{
			theApp.LogEntry(_T("/ic "), rCmdInfo.m_strJobTemplate, rCmdInfo.m_strPDFDoc);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc, rCmdInfo.m_strStartPage, rCmdInfo.m_strPDFInfoFile, rCmdInfo.m_strPreviewsFolder);
			theApp.LogEntry(nError);
			if (nError != 0)	// < 0 = error ; 0 = nothing happened ; > 0 = no. sheets outputted
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);

			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdImposeXML:		
		{
			theApp.LogEntry(_T("/ix "), rCmdInfo.m_strJobTemplate, rCmdInfo.m_strPDFDoc);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc, rCmdInfo.m_strStartPage, rCmdInfo.m_strPDFInfoFile, rCmdInfo.m_strPreviewsFolder);
			theApp.LogEntry(nError);
			if (nError != 0)	// < 0 = error ; 0 = nothing happened ; > 0 = no. sheets outputted
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);

			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdImposeJDF:		
		{
			theApp.LogEntry(_T("/ij "), rCmdInfo.m_strJobTemplate, rCmdInfo.m_strPDFDoc);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc, rCmdInfo.m_strStartPage, rCmdInfo.m_strPDFInfoFile, rCmdInfo.m_strPreviewsFolder);
			theApp.LogEntry(nError);
			if (nError != 0)	// < 0 = error ; 0 = nothing happened ; > 0 = no. sheets outputted
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);

			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdDoOutput:		
		{
			theApp.LogEntry(_T("/do "), rCmdInfo.m_strJobTemplate);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc);
			theApp.LogEntry(nError);
			if (nError != 0)	// < 0 = error ; 0 = nothing happened ; > 0 = no. sheets outputted
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);

			return nError;	// exit program
		}
		break;

	case CImpManApp::CmdResetPageSource:		
	case CImpManApp::CmdResetPageSourceX:
		{
			if (CImpManApp::CmdResetPageSource)
				theApp.LogEntry(_T("/rd "), rCmdInfo.m_strJobTemplate, rCmdInfo.m_strPDFDoc);
			else
				theApp.LogEntry(_T("/xd "), rCmdInfo.m_strJobTemplate, rCmdInfo.m_strPDFDoc);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc);
			theApp.LogEntry(nError);
			if (nError == 1)	// 0 = not resetted -> don't save  ; 1 = resetted ; 
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);

			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdResetPageList:		
		{
			theApp.LogEntry(_T("/rp "), rCmdInfo.m_strJobTemplate);
			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc);
			theApp.LogEntry(nError);
			if (nError != 1)	// 1 = already in reset state (i.e. no page sources)
				pDoc->OnSaveDocument(rCmdInfo.m_strJobTemplate);

			return 0;	// exit program
		}
		break;

	case CImpManApp::CmdPageList:	
		{
//			nError = pDoc->ProcessCommand(rCmdInfo.m_nKIMCommand, rCmdInfo.m_strPDFDoc, rCmdInfo.m_strPDFInfoFile, rCmdInfo.m_strPreviewsFolder);

			BOOL bExitProgram = TRUE;
			CMainFrame frame;
			frame.CreateOrActivatePageListFrame(theApp.m_pPageListTemplate, pDoc);
			CPageListView* pView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
			if (pView)
			{
				short nPageSourceIndex = pDoc->m_PageSourceList.GetPageSourceIndex(rCmdInfo.m_strPDFDoc);
				if ((nPageSourceIndex >= 0) && (nPageSourceIndex < pDoc->m_PageSourceList.GetCount()))
				{
					POSITION pos = pDoc->m_PageSourceList.FindIndex(nPageSourceIndex);
					if (pos)
					{
						CPageSource& rPageSource = pDoc->m_PageSourceList.GetAt(pos); 
						if (rPageSource.m_PageSourceHeaders.GetSize())
						{
							CPageListFrame* pPageListFrame = (CPageListFrame*)pView->GetParentFrame();
							if (pPageListFrame)
							{
								pPageListFrame->ShowWindow(SW_SHOWMAXIMIZED);
								m_pMainWnd->SetMenu(NULL);
								m_pMainWnd->ShowWindow(SW_SHOWNORMAL);
								m_pMainWnd->UpdateWindow();
								bExitProgram = FALSE;
							}
							CPageSourceHeader& rPageSourceHeader = rPageSource.m_PageSourceHeaders[0];
							CPageSourceListView* pView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
							CDisplayItem* pDI = (pView) ? pView->m_DisplayList.GetItemFromData(&rPageSourceHeader) : NULL;
							if (pDI)
								pView->OnSelectPageSourceHeader(pDI, pDI->m_AuxRect.BottomRight() - CSize(2, 2), 0);	// emulate click into link symbol
						}
					}
				}
			}
			return (bExitProgram) ? 0 : 1;
		}
		break;

	default:
		return 0;		// exit program
	}

	return 0;
}

BOOL CImpManApp::CalledFromKPM()
{
	const int nValueLenght = 7;
	HKEY	  hKey;
	CString   strKey = "SOFTWARE\\Krause\\KIM7.1 Settings";
	DWORD	  dwSize = nValueLenght;
	long	  ret	 = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	char	  szGUIStyle[nValueLenght];
	if ( ! RegQueryValueEx(hKey, "ServerGUIStyle", 0, NULL, (LPBYTE)szGUIStyle, &dwSize) == ERROR_SUCCESS)
		strcpy_s(szGUIStyle, "Full");
	
	//if (CString(szGUIStyle).CompareNoCase(_T("Full")) == 0)
	//	return GUIFull;
	//else
	//	if (CString(szGUIStyle).CompareNoCase(_T("Simple")) == 0)
	//		return GUISimple;
	//	else
	if (CString(szGUIStyle).CompareNoCase(_T("Hidden")) == 0)
		return TRUE;
	else
		return FALSE;
}

void CImpManApp::ResetLogFile()
{
	CString strLogFullPath = theApp.settings.m_szWorkDir + CString("\\KimKernel.log");
	DeleteFile(strLogFullPath);
}

void CImpManApp::LogEntry(CString strOut1, CString strOut2, CString strOut3, CString strOut4)
{
	FILE *fp;
	CString strLogFullPath = theApp.settings.m_szWorkDir + CString("\\KimKernel.log");
	if ( (fp = _tfopen((LPCTSTR)strLogFullPath, _T("a+t"))) == NULL)
		return;

	_fputts(strOut1 + strOut2 + strOut3 + strOut4 + _T("\r\n"), fp);
	fclose(fp);
}

void CImpManApp::LogEntry(int nValue)
{
	FILE *fp;
	CString strLogFullPath = theApp.settings.m_szWorkDir + CString("\\KimKernel.log");
	if ( (fp = _tfopen((LPCTSTR)strLogFullPath, _T("a+t"))) == NULL)
		return;

	CString strOut;
	strOut.Format(_T("/%d\r\n"), nValue);

	_fputts(strOut, fp);
	fclose(fp);
}

void CImpManApp::LogEntry(CString strOut, BOOL bIsError)
{
	LogEntry(strOut);
	//LogJobInfoError(strOut);
}

void CImpManApp::LogJobInfoError(CString strOut)
{
	//CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;

	//TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	//_tsplitpath(pDoc->GetPathName(), szDrive, szDir, szFname, szExt);
	//CString strTemplatePath = CString(szDrive) + CString(szDir);

	//CString		 strInfoFile;
	//CStringArray strErrors;
	//strInfoFile.Format(_T("%sJobInfo.xml"), strTemplatePath);
	//strErrors.Add(strOut);
	//pDoc->SetJobInfoErrors(strInfoFile, strErrors);
}

int	CImpManApp::MapPrintColor(int nColorDefinitionIndex, CPageTemplate* pObjectTemplate, CPrintSheet* pPrintSheet, int nSide)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nColorDefinitionIndex;

	if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= pDoc->m_ColorDefinitionTable.GetSize()) )
		return nColorDefinitionIndex;

	if (pObjectTemplate && pPrintSheet)
	{
		CColorDefinition& rColorDef = pDoc->m_ColorDefinitionTable[nColorDefinitionIndex];
		if (pObjectTemplate->m_bMap2SpotColors)
			if (rColorDef.m_strColorName.CompareNoCase(_T("Composite")) != 0)	// if composite -> mapping happens in PDFOutput.cpp
				if (rColorDef.m_nColorType == CColorDefinition::Spot)
				{
					//int nMap2SpotColorIndex	 = pObjectTemplate->m_spotColorMappingTable.GetEntry(pObjectTemplate->GetCurSpotColorNum(nColorDefinitionIndex, "", NULL));
					//int nCurSpotColorNum	 = (nMap2SpotColorIndex <= 0) ? pObjectTemplate->GetCurSpotColorNum(nColorDefinitionIndex, "", NULL) : nMap2SpotColorIndex;
					//int nMappedColorDefIndex = pPrintSheet->GetSpotColor(nSide, nCurSpotColorNum);
					//return (nMappedColorDefIndex == -1) ? nColorDefinitionIndex : nMappedColorDefIndex;
				}
	}

	CColorDefinition& rColorDef = pDoc->m_ColorDefinitionTable[nColorDefinitionIndex];
	CString strTargetColorName = m_colorDefTable.FindColorReferenceName(rColorDef.m_strColorName);
	if (strTargetColorName.IsEmpty())
		return nColorDefinitionIndex;

	nColorDefinitionIndex = pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strTargetColorName);
	if (nColorDefinitionIndex == -1)
	{
		CColorDefinition colorDef(strTargetColorName, rColorDef.m_rgb, rColorDef.m_nColorType, rColorDef.m_nColorIndex);
		pDoc->m_ColorDefinitionTable.InsertColorDef(colorDef);
		nColorDefinitionIndex = pDoc->m_ColorDefinitionTable.GetSize() - 1;
	}
	return nColorDefinitionIndex;
}

CString	CImpManApp::MapPrintColor(const CString& rstrColorName, CPageTemplate* pObjectTemplate, CPrintSheet* pPrintSheet, int nSide)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return "";

	if (pObjectTemplate && pPrintSheet)
	{
		int nColorDefinitionIndex = pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(rstrColorName);
		CPageSourceHeader* pPageSourceHeader = pObjectTemplate->GetObjectSourceHeader(nColorDefinitionIndex);
		if (pPageSourceHeader)
			if (pObjectTemplate->m_bMap2SpotColors)
				if (pPageSourceHeader->m_strColorName.CompareNoCase(_T("Composite")) != 0)	// if composite -> mapping happens in PDFOutput.cpp
					if (pPageSourceHeader->GetColorType() == CColorDefinition::Spot)
					{
						int nMap2SpotColorIndex	 = pObjectTemplate->m_spotColorMappingTable.GetEntry(pObjectTemplate->GetCurSpotColorNum(nColorDefinitionIndex, pPageSourceHeader->m_strColorName, pPageSourceHeader));
						int nCurSpotColorNum	 = (nMap2SpotColorIndex <= 0) ? pObjectTemplate->GetCurSpotColorNum(nColorDefinitionIndex, pPageSourceHeader->m_strColorName, pPageSourceHeader) : nMap2SpotColorIndex;
						int nMappedColorDefIndex = pPrintSheet->GetSpotColor(nSide, nCurSpotColorNum);
						if (nMappedColorDefIndex != -1)
							pDoc->m_ColorDefinitionTable[nMappedColorDefIndex].m_strColorName;
						return rstrColorName;
					}
	}

	CString strMappedColor = m_colorDefTable.FindColorReferenceName(rstrColorName);
	return (strMappedColor.GetLength()) ? strMappedColor : rstrColorName;
}


void CImpManApp::OpenIfAutoBackupFile(CCommandLineInfo& rCmdInfo)
{
/////////////////////////////////////////////////////////////////////////////
// Open the autobackup-file if exist -> means the program exited not correct
	WIN32_FIND_DATA findData;
	TCHAR searchbuffer[MAX_PATH];
	_tcscpy_s(searchbuffer, MAX_PATH, theApp.settings.m_szTempDir); // TODO: Verify the path
	_tcscat_s(searchbuffer, MAX_PATH, _T("\\autobackup.job"));
	HANDLE hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		//if (AfxMessageBox(IDS_LOAD_AUTOBACKUP, MB_YESNO) == IDYES)
		{
			rCmdInfo.m_nShellCommand = CCommandLineInfo::FileOpen;
			rCmdInfo.m_strFileName   = searchbuffer;
		}
		//else
		//	if ( ! theApp.settings.m_bOnStartFileNew)
		//		rCmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;
	}
	else
	{
		// DON'T display a new MDI child window during startup!!!
		if ( ! theApp.settings.m_bOnStartFileNew)
			rCmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;
	}
	FindClose(hfileSearch);
}

void CImpManApp::CreateKimPalette()
{
	LPLOGPALETTE pLogPal = (LPLOGPALETTE) new char[2 * sizeof(WORD) + KIM_PALETTE_NUMCOLORS * sizeof(PALETTEENTRY)];
	pLogPal->palVersion    = 0x300;
	pLogPal->palNumEntries = KIM_PALETTE_NUMCOLORS;

	pLogPal->palPalEntry[0].peRed   = 200;
	pLogPal->palPalEntry[0].peGreen = 200;
	pLogPal->palPalEntry[0].peBlue  = 183;
	pLogPal->palPalEntry[0].peFlags = 0;

	pLogPal->palPalEntry[1].peRed   = 255;
	pLogPal->palPalEntry[1].peGreen = 128;
	pLogPal->palPalEntry[1].peBlue  = 64;
	pLogPal->palPalEntry[1].peFlags = 0;

	m_kimPalette.CreatePalette(pLogPal);
	delete pLogPal;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnButtonKimhomepage();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON_KIMHOMEPAGE, OnButtonKimhomepage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CImpManApp::OnAppAbout()
{
#ifdef _DEBUG
	theApp.CountProjectLines();
	CDlgEditTempString stringDlg;
	stringDlg.DoModal();
	theApp.m_strTemp.Empty();
#endif
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if (! theApp.strName.IsEmpty())
	{
		CString strTitle = theApp.BuildKIMTitle(_T(""));
		SetWindowText(strTitle);
	}

	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CImpManApp::PreTranslateMessage(MSG* pMsg)
{
	// CG: The following lines were added by the Splash Screen component.
	if (CSplashWnd::PreTranslateAppMessage(pMsg))
		return TRUE;
	return CWinApp::PreTranslateMessage(pMsg);
}

void CImpManApp::OnSettingsProgram() 
{
	CSettingsSheet propSheet;
	if (propSheet.DoModal() != IDCANCEL)
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			pView->m_DisplayList.Invalidate();
		CImpManDoc* pDoc = CImpManDoc::GetDoc();	
		if (pDoc)
			pDoc->UpdateAllViews(NULL);
	}
}

void CImpManApp::OnSettingsPressdevice() 
{
	CDlgPressDevice pDDlg;
	pDDlg.DoModal();
}

void CImpManApp::OnSettingsImpose() 
{
	CString strFile;
	strFile.LoadString(IDS_DEFAULTNAME_TEXT);

	CDlgImpositionScheme dlg(TRUE, _T("isc"), (LPCTSTR)strFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
							 _T(" |*.isc|"), NULL, NULL);	// 'this' pointer gives DlgImpositionScheme access to BindingAssistent
														// NULL if Stand-Alone
	dlg.m_bStandAlone = TRUE;
	dlg.DoModal();
	dlg.m_bStandAlone = FALSE;
}

void CImpManApp::OnReferenceColor() 
{
	CDlgPrintColorTable dlg;
	dlg.DoModal();
}

void CImpManApp::OnFileNew() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();	
	if (pDoc)
	{
		if ( ! pDoc->SaveModified())
			return;
		pDoc->OnCloseDocument();	// In debug version after OnCloseDocument() we get an assertion later on when next dialog is opened
	}								// Didn't find out any reason. Seems not to be critical, so ignore this bug so far.

	CWinApp::OnFileNew();

	if ( ! m_bInsideInitInstance)
	{
		CAutoServerFrame* pAutoServerFrame = GetAutoServerFrame();
		if (pAutoServerFrame)
		{
			pAutoServerFrame->ShowWindow(SW_HIDE);
		}
		COrderDataView* pView  = (COrderDataView*)GetView(RUNTIME_CLASS(COrderDataView));
		COrderDataFrame* pFrame = (COrderDataFrame*)((pView) ? pView->GetParentFrame() : NULL);
		if (pFrame)
			pFrame->ShowWindow(SW_HIDE);
		if ( ! OnWizard())
			if (CImpManDoc::GetDoc())
			{
				CImpManDoc::GetDoc()->OnCloseDocument();
				return;
			}

		if (pView)
		{
			pView->OnUpdate(NULL, 0, NULL);
			if (pFrame)
				pFrame->RearrangeSplitters();
		}
		m_bInsideNewJobLoop = TRUE;
		if (m_pMainWnd)
			m_pMainWnd->PostMessage(WM_COMMAND, ID_NAV_ORDERDATA);
	}
}

void CImpManApp::OnFileOpen() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();	
	if (pDoc)
	{
		if ( ! pDoc->SaveModified())
			return;
		pDoc->OnCloseDocument();	// In debug version after OnCloseDocument() we get an assertion later on when next dialog is opened
	}								// Didn't find out any reason. Seems not to be critical, so ignore this bug so far.

	CString newName;
	if ( ! DoPromptFileName(newName, AFX_IDS_OPENFILE, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR | OFN_ENABLESIZING, TRUE, NULL))
		return; // open cancelled

	AfxGetApp()->OpenDocumentFile(newName);
}

CDocument* CImpManApp::OpenDocumentFile(LPCTSTR lpszFileName) 
{
	((CMainFrame*)theApp.m_pMainWnd)->m_nNavSelection = 0;
	
	CDocument* pDoc = CWinApp::OpenDocumentFile(lpszFileName);
	if (pDoc)
	{
		((CImpManDoc*)pDoc)->m_PageTemplateList.FindPrintSheetLocations();
		((CImpManDoc*)pDoc)->m_MarkTemplateList.FindPrintSheetLocations();

		pDoc->UpdateAllViews(NULL, 0, NULL);
	}
	return pDoc;
}


// prompt for file name - used for open and save as
BOOL CImpManApp::DoPromptFileName(CString& fileName, UINT nIDSTitle, DWORD lFlags, BOOL bOpenFileDialog, CDocTemplate* pTemplate)
		// if pTemplate==NULL => all document templates
{
	ASSERT(m_pDocManager != NULL);

	CString strFilter;
	VERIFY(strFilter.LoadString(IDS_KIMDOCFILE_FILTER));

	CKIMFileDialog dlgFile(bOpenFileDialog, _T(".job"), NULL, lFlags, strFilter);

	CString title;
	VERIFY(title.LoadString(nIDSTitle));

	dlgFile.m_ofn.lpstrInitialDir = settings.m_szJobsDir;
	dlgFile.m_ofn.lpstrTitle = title;
	dlgFile.m_ofn.lpstrFile = fileName.GetBuffer(_MAX_PATH);

	int nResult = dlgFile.DoModal();
	fileName.ReleaseBuffer();

	if (nResult == IDOK)
	{
		TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFileName[_MAX_FNAME], szExtension[_MAX_EXT];
		_tsplitpath_s(dlgFile.GetPathName(), szDrive, _MAX_DRIVE, szDir, _MAX_DIR, szFileName, _MAX_FNAME, szExtension, _MAX_EXT);
		_tcscpy(settings.m_szJobsDir, CString(szDrive) + CString(szDir));
		HKEY hKey;
		if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)theApp.m_strRegistryKey, &hKey) == ERROR_SUCCESS)
		{
			RegSetValueEx(hKey, _T("JobFolder"), 0, REG_SZ, (LPBYTE)settings.m_szJobsDir, _MAX_PATH);
			RegCloseKey(hKey);
		}
	}

	return nResult == IDOK;
}

void CImpManApp::SaveWindowPlacement(CWnd* pWnd, const CString& strWindowName)
{
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\") + strWindowName;
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		WINDOWPLACEMENT wp;
		pWnd->GetWindowPlacement(&wp);

		RegSetValueEx(hKey, _T("Placement"), 0, REG_BINARY, (LPBYTE)&wp, wp.length);

		RegCloseKey(hKey);
	}
}

BOOL CImpManApp::RestoreWindowPlacement(CWnd* pWnd, const CString& strWindowName)
{
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\") + strWindowName;
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		DWORD dwSize;
		WINDOWPLACEMENT wp;
		wp.length = sizeof(WINDOWPLACEMENT);
		dwSize	  = sizeof(WINDOWPLACEMENT);
		if (RegQueryValueEx(hKey, _T("Placement"), 0, NULL, (LPBYTE)&wp, &dwSize) == ERROR_SUCCESS)
			pWnd->SetWindowPlacement(&wp);

		RegCloseKey(hKey);

		return TRUE;
	}
	else
		return FALSE;
}

CString CImpManApp::GetDefaultInstallationFolder()
{
	CString	  strDir;
	const int nValueLenght = _MAX_PATH + 2;
	TCHAR	  szValue[nValueLenght];
	DWORD	  dwSize = nValueLenght;
	HKEY	  hKey;
	CString   strKey = m_strRegistryKey;
	DWORD	  dwDisposition; 
	long ret = RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition); 
	if (RegQueryValueEx(hKey, _T("InstallDir"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		strDir = szValue;
	else
	{
		strDir = _T("C:\\Programme\\KIM7");
		if ( ! DirectoryExist(strDir))
		{
			strDir = _T("C:\\Program files\\KIM7");
			if ( ! DirectoryExist(strDir))
			{
				strDir = _T("C:\\Programme (x86)\\KIM7");
				if ( ! DirectoryExist(strDir))
				{
					strDir = _T("C:\\Program files (x86)\\KIM7");
					if ( ! DirectoryExist(strDir))
						strDir = _T("");
				}
			}
		}
	}

	RegCloseKey(hKey);

	return strDir;
}

CString CImpManApp::GetMarksetInitialFolder(BOOL bAutoMarks)
{
	if (bAutoMarks)
		return (CString(settings.m_szDataDir) + _T("\\Auto-Marksets"));
	else
		return (CString(settings.m_szDataDir) + _T("\\PDF-Marksets"));
}

BOOL CImpManApp::OnWizard()
{
	BOOL bWizardFinishedOK = TRUE;

	CDlgJobData dlgJobData(IDD_ORDERDATA, 0, 0);//, NULL, TRUE);
	if (dlgJobData.DoModal() == IDCANCEL)
		bWizardFinishedOK = FALSE;

	return bWizardFinishedOK;
}

int CImpManApp::ExitInstance() 
{
	// if you don't execute "AddDocTemplate(m_p<name>Template);" in InitInstance
	//		you have to delete them when you "ExitInstance"

	m_PressDeviceList.RemoveAll();

#ifdef KIM_ENGINE
	if (theApp.m_bServerConnected) 
		theApp.m_kaDB.kA_Disconnect();
#else
	AutoServerDisconnect();
#endif

	if (m_pDlgMarkSet)
		delete m_pDlgMarkSet;
	if (m_pDlgPDFLibStatus)
		delete m_pDlgPDFLibStatus;
/////////////////////////////////////////////////////////////////////////////
//	Delete autobackup-file
	WIN32_FIND_DATA findData;
	TCHAR searchbuffer[MAX_PATH];
	_tcscpy_s(searchbuffer, MAX_PATH, theApp.settings.m_szTempDir); // TODO: Verify the path
	_tcscat_s(searchbuffer, MAX_PATH, _T("\\autobackup.job"));
	HANDLE hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
		DeleteFile(LPCTSTR(searchbuffer));
	FindClose(hfileSearch);

	if(bClassRegistered)
		::UnregisterClass(theApp.m_strClassName,AfxGetInstanceHandle());

	// clean up
	JDF::PlatformUtils::Terminate();

	GdiplusShutdown(g_gdiplusToken);

	if (m_bPDFEngineInitialized)
		PTB_LibRelease();

	MyWriteRecentFileList();
	
	delete m_pAutoServerTemplate;	
	delete m_pProdDataTemplate;
	delete m_pJobDocumentationTemplate;
	delete m_pBookBlockTemplate;	
	delete m_pNewPrintSheetTemplate;
	delete m_pPageListTemplate;	
	//m_pOrderDataTemplate has been added by AddDocTemplate(m_pOrderDataTemplate) -> will be delete automatically

#ifdef KIM_ENGINE
	CWinApp::ExitInstance();
	return theApp.m_nAppError;
#else
    if ( (m_nGUIStyle == GUIQuiet) || (m_nGUIStyle == GUIAutoImpose) )
	{
		CWinApp::ExitInstance();
		return theApp.m_nAppError;
	}
	else
		return CWinApp::ExitInstance();
#endif
}


/////////////////////////////////////////////////////////////////////////////
// CImpManApp global helper functions

int CImpManApp::GetResolution(void)
{
	int nVertExt = GetSystemMetrics(SM_CYSCREEN);
	int nHorExt  = GetSystemMetrics(SM_CXSCREEN);

	if (nHorExt == 640 && nVertExt == 480)
		return (R640X480);
	else
		if (nHorExt == 800 && nVertExt == 600)
			return (R800X600);
		else
			if (nHorExt == 1024 && nVertExt == 768)
				return (R1024X768);
			else
				if (nHorExt == 1152 && nVertExt == 864)
					return (R1152X864);
				else
					if (nHorExt == 1280 && nVertExt == 1024)
						return (R1280X1024);
					else
						return (R800X600);
}

CView* CImpManApp::GetActiveView()
{
	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)m_pMainWnd;
// Get the active MDI child window.
	CMDIChildWnd *pChild = (CMDIChildWnd *) pFrame->GetActiveFrame();

// Get the active view attached to the active MDI child
// window.
	return pChild->GetActiveView();
}

CView* CImpManApp::GetView(CRuntimeClass *pClass)
{
	CView*	 pView = NULL;
	POSITION pos;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = m_pActiveDoc;
	if (pDoc)
		pos = pDoc->GetFirstViewPosition();
	else
		pos = NULL;
	while (pos != NULL)
	{
		pView = pDoc->GetNextView(pos);	
		if (pView->IsKindOf(pClass))
			return pView;
	}

	return NULL;
}

void CImpManApp::UpdateView(CRuntimeClass *pClass, BOOL bInvalidateDisplayList)
{
	CView*	 pView = NULL;
	POSITION pos;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = m_pActiveDoc;
	if (pDoc)
		pos = pDoc->GetFirstViewPosition();
	else
		pos = NULL;
	while (pos != NULL)
	{
		pView = pDoc->GetNextView(pos);	
		if ( ! pView->IsKindOf(pClass))
			continue;

	//CView* pView = CImpManApp::GetView(pClass);
	//if (pView)
	//{
		OnUpdateView(pClass, NULL, 0, NULL, bInvalidateDisplayList);
		pView->Invalidate();
		pView->UpdateWindow(); 
	}
}

void CImpManApp::OnUpdateView(CRuntimeClass *pClass, CView* pSender, LPARAM lHint, CObject* pHint, BOOL bInvalidateDisplayList)
{
	CView*	 pView = NULL;
	POSITION pos;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = m_pActiveDoc;
	if (pDoc)
		pos = pDoc->GetFirstViewPosition();
	else
		pos = NULL;
	while (pos != NULL)
	{
		pView = pDoc->GetNextView(pos);	
		if ( ! pView->IsKindOf(pClass))
			continue;

		if (pView->IsKindOf(RUNTIME_CLASS(COrderDataView)))
			((COrderDataView*)pView)->OnUpdate(pSender, lHint, pHint);

		if (pView->IsKindOf(RUNTIME_CLASS(CProductNavigationView)))
			((CProductNavigationView*)pView)->OnUpdate(pSender, lHint, pHint);

		if (pView->IsKindOf(RUNTIME_CLASS(CProductsView)))
		{
			if (bInvalidateDisplayList)
				((CProductsView*)pView)->m_DisplayList.Invalidate();
			((CProductsView*)pView)->OnUpdate(pSender, lHint, pHint);
		}

		if (pView->IsKindOf(RUNTIME_CLASS(CBookBlockView)))
		{
			if (bInvalidateDisplayList)
				((CBookBlockView*)pView)->m_DisplayList.Invalidate();
			((CBookBlockView*)pView)->OnUpdate(pSender, lHint, pHint);
		}

		if (pView->IsKindOf(RUNTIME_CLASS(CBookBlockProdDataView)))
		{
			if (bInvalidateDisplayList)
				((CBookBlockProdDataView*)pView)->m_DisplayList.Invalidate();
			((CBookBlockProdDataView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPageListTopToolsView)))
			((CPageListTopToolsView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPageListView)))
		{
			if (bInvalidateDisplayList)
				((CPageListView*)pView)->m_DisplayList.Invalidate();
			((CPageListView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPageListDetailView)))
		{
			if (bInvalidateDisplayList)
				((CPageListDetailView*)pView)->m_DisplayList.Invalidate();
			((CPageListDetailView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPageListTableView)))
		{
			if (bInvalidateDisplayList)
				((CPageListTableView*)pView)->m_DisplayList.Invalidate();
			((CPageListTableView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPageSourceListView)))
		{
			if (bInvalidateDisplayList)
				((CPageSourceListView*)pView)->m_DisplayList.Invalidate();
			((CPageSourceListView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetTreeView)))
			((CPrintSheetTreeView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetTableView)))
		{
			if (bInvalidateDisplayList)
				((CPrintSheetTableView*)pView)->m_DisplayList.Invalidate();
			((CPrintSheetTableView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetMarksView)))
			((CPrintSheetMarksView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetView)))
		{
			if (bInvalidateDisplayList)
				((CPrintSheetView*)pView)->m_DisplayList.Invalidate();
			((CPrintSheetView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CProdDataPrintView)))
			((CProdDataPrintView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CProdDataPrintToolsView)))
			((CProdDataPrintToolsView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetNavigationView)))
		{
			if (bInvalidateDisplayList)
				((CPrintSheetNavigationView*)pView)->m_DisplayList.Invalidate();
			((CPrintSheetNavigationView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CJobDocumentationView)))
			((CJobDocumentationView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CJobDocumentationSettingsView)))
			((CJobDocumentationSettingsView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
		{
			if (bInvalidateDisplayList)
				((CPrintSheetPlanningView*)pView)->m_DisplayList.Invalidate();
			((CPrintSheetPlanningView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetViewStrippingData)))
			((CPrintSheetViewStrippingData*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetColorSettingsView)))
			((CPrintSheetColorSettingsView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetMaskSettingsView)))
			((CPrintSheetMaskSettingsView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetObjectPreview)))
			((CPrintSheetObjectPreview*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetComponentsView)))
			((CPrintSheetComponentsView*)pView)->OnUpdate(pSender, lHint, pHint);
		
		if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerJobsView)))
		{
			if (bInvalidateDisplayList)
				((CAutoServerJobsView*)pView)->m_DisplayList.Invalidate();
			((CAutoServerJobsView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerWorkflowsView)))
		{
			if (bInvalidateDisplayList)
				((CAutoServerWorkflowsView*)pView)->m_DisplayList.Invalidate();
			((CAutoServerWorkflowsView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerLogsView)))
		{
			if (bInvalidateDisplayList)
				((CAutoServerLogsView*)pView)->m_DisplayList.Invalidate();
			((CAutoServerLogsView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerDetailsView)))
		{
			if (bInvalidateDisplayList)
				((CAutoServerDetailsView*)pView)->m_DisplayList.Invalidate();
			((CAutoServerDetailsView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintComponentsView)))
		{
			if (bInvalidateDisplayList)
				((CPrintComponentsView*)pView)->m_DisplayList.Invalidate();
			((CPrintComponentsView*)pView)->OnUpdate(pSender, lHint, pHint);
		}
	}
}

void CImpManApp::ViewsResetDisplayList(CRuntimeClass *pClass)
{
	CView*	 pView = NULL;
	POSITION pos;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = m_pActiveDoc;
	if (pDoc)
		pos = pDoc->GetFirstViewPosition();
	else
		pos = NULL;
	while (pos != NULL)
	{
		pView = pDoc->GetNextView(pos);	
		if ( ! pView->IsKindOf(pClass))
			continue;

		if (pView->IsKindOf(RUNTIME_CLASS(CProductsView)))
		{
			((CProductsView*)pView)->m_DisplayList.Invalidate();
			((CProductsView*)pView)->m_DisplayList.Clear();
		}

		if (pView->IsKindOf(RUNTIME_CLASS(CBookBlockView)))
		{
			((CBookBlockView*)pView)->m_DisplayList.Invalidate();
			((CBookBlockView*)pView)->m_DisplayList.Clear();
		}

		if (pView->IsKindOf(RUNTIME_CLASS(CBookBlockProdDataView)))
		{
			((CBookBlockProdDataView*)pView)->m_DisplayList.Invalidate();
			((CBookBlockProdDataView*)pView)->m_DisplayList.Clear();
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPageListView)))
		{
			((CPageListView*)pView)->m_DisplayList.Invalidate();
			((CPageListView*)pView)->m_DisplayList.Clear();
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPageListDetailView)))
		{
			((CPageListDetailView*)pView)->m_DisplayList.Invalidate();
			((CPageListDetailView*)pView)->m_DisplayList.Clear();
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPageListTableView)))
		{
			((CPageListTableView*)pView)->m_DisplayList.Invalidate();
			((CPageListTableView*)pView)->m_DisplayList.Clear();
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPageSourceListView)))
		{
			((CPageSourceListView*)pView)->m_DisplayList.Invalidate();
			((CPageSourceListView*)pView)->m_DisplayList.Clear();
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetTableView)))
		{
			((CPrintSheetTableView*)pView)->m_DisplayList.Invalidate();
			((CPrintSheetTableView*)pView)->m_DisplayList.Clear();
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetView)))
		{
			((CPrintSheetView*)pView)->m_DisplayList.Invalidate();
			((CPrintSheetView*)pView)->m_DisplayList.Clear();
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetNavigationView)))
		{
			((CPrintSheetNavigationView*)pView)->m_DisplayList.Invalidate();
			((CPrintSheetNavigationView*)pView)->m_DisplayList.Clear();
		}
		
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
		{
			((CPrintSheetPlanningView*)pView)->m_DisplayList.Invalidate();
			((CPrintSheetPlanningView*)pView)->m_DisplayList.Clear();
		}
	}
}

BOOL CImpManApp::IsViewVisible(CRuntimeClass *pClass)
{
	if ( ! pClass)
		return FALSE;
	CView* pView = GetView(pClass);
	if ( ! pView)
		return FALSE;
	CFrameWnd* pFrame = pView->GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if ( ! pFrame->IsWindowVisible())
		return FALSE;

	CRect viewRect, otherRect, destRect;
	pFrame->GetWindowRect(viewRect);    
	CWnd *pPrevWnd, *pNextWnd;
	for (pPrevWnd = pFrame; (pNextWnd = pPrevWnd->GetWindow(GW_HWNDPREV)) != NULL; pPrevWnd = pNextWnd)
    {
		pNextWnd->GetWindowRect(otherRect);
		if (pNextWnd->IsWindowVisible() && otherRect.PtInRect(viewRect.TopLeft()) && otherRect.PtInRect(viewRect.BottomRight()) )
			return FALSE;
    }

	return TRUE;
}

void CImpManApp::GetVersionInfo(void)
{
	DWORD dwError;

	TCHAR szFullPath[_MAX_PATH];
	DWORD dwVerInfoSize;
	DWORD dwVerHnd = 0;
	::GetModuleFileName(AfxGetInstanceHandle(), szFullPath, sizeof(szFullPath));
	dwVerInfoSize = ::GetFileVersionInfoSize(szFullPath, &dwVerHnd);
	if (dwVerInfoSize) 
	{
		LPVOID  lpstrVffInfo;
		HANDLE  hMem;
		UINT    uNameLen;
		LPTSTR  lpName;
		UINT    uVersionLen;
		LPTSTR  lpVersion;
		UINT    uCopyrightLen; 
		LPTSTR  lpCopyright;
		UINT    uOriginalFilenameLen;
		LPTSTR  lpOriginalFilename;

		hMem = GlobalAlloc (GMEM_MOVEABLE, dwVerInfoSize);
		lpstrVffInfo = GlobalLock(hMem);
		::GetFileVersionInfo(szFullPath, dwVerHnd, dwVerInfoSize, lpstrVffInfo);
		::VerQueryValue((LPVOID)lpstrVffInfo, TEXT("\\StringFileInfo\\040704b0\\ProductName"),		(LPVOID*)&lpName,				(UINT *)&uNameLen); 
		::VerQueryValue((LPVOID)lpstrVffInfo, TEXT("\\StringFileInfo\\040704b0\\ProductVersion"),	(LPVOID*)&lpVersion,			(UINT *)&uVersionLen); 
		::VerQueryValue((LPVOID)lpstrVffInfo, TEXT("\\StringFileInfo\\040704b0\\LegalCopyright"),	(LPVOID*)&lpCopyright,			(UINT *)&uCopyrightLen); 
		::VerQueryValue((LPVOID)lpstrVffInfo, TEXT("\\StringFileInfo\\040704b0\\OriginalFilename"), (LPVOID*)&lpOriginalFilename,	(UINT *)&uOriginalFilenameLen); 

		theApp.strName				= lpName;
		theApp.strVersion			= lpVersion;
		theApp.strCopyright			= lpCopyright;
		theApp.strOriginalFilename	= lpOriginalFilename;

		GlobalUnlock(hMem);
		GlobalFree(hMem);
	}
	else
		dwError = GetLastError();

	OSVERSIONINFO infoStruct;
	infoStruct.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&infoStruct);
	theApp.m_dwPlatformId = infoStruct.dwPlatformId;
	theApp.m_nOSVersion = CImpManApp::Unknown;
	if ( infoStruct.dwMajorVersion == 6)
	{
		switch (infoStruct.dwMinorVersion)
		{
		case 2: theApp.m_nOSVersion = CImpManApp::Win8;		break;
		case 1: theApp.m_nOSVersion = CImpManApp::Win7;		break;
		case 0: theApp.m_nOSVersion = CImpManApp::WinVista;	break;
		}
	}
	else
		if ( infoStruct.dwMajorVersion == 5)
		{
			if ( infoStruct.dwMinorVersion == 0 )
				theApp.m_nOSVersion = CImpManApp::Win2000;
			else
				theApp.m_nOSVersion = CImpManApp::WinXP;
		}
}	

BOOL CImpManApp::SetMyCurrentDirectory(LPCTSTR lpPathName, BOOL bWithMessage)
{
	if ( ! SetCurrentDirectory(lpPathName)) // TODO: Make a function with the following instructions
	{
		if (bWithMessage)
		{
			CString string;
			AfxFormatString1(string, IDS_PATHNOTFOUND_TEXT, lpPathName);
			KIMOpenLogMessage(string, MB_ICONERROR);
			return FALSE;
		}
		else
			return FALSE;
	}
	else
		return TRUE;
}

BOOL CImpManApp::DirectoryExist(LPCTSTR lpPathName)
{
	CFileStatus status;
	if ( ! CFile::GetStatus(lpPathName, status))	
		return FALSE;
	else
		return TRUE;
}

void CImpManApp::RemoveExtension(CString& strPath)
{
	PathRemoveExtension(strPath.GetBuffer(0));
	strPath.ReleaseBuffer();
}

BOOL CImpManApp::FindFileFolder(CString strStartFolder, CString strFile, CString& strTargetFolder)
{
	if (FolderHasFile(strStartFolder, strFile))
	{
		strTargetFolder = strStartFolder;
		return TRUE;
	}

	WIN32_FIND_DATA findData;
	CString			strFilename = strStartFolder + _T("*.*");
	HANDLE			hfileSearch = FindFirstFile(strFilename, &findData);
	if (hfileSearch == INVALID_HANDLE_VALUE)
	{
		FindClose(hfileSearch);
		return FALSE;
	}

	BOOL	bFound	  = FALSE;
	CString	strBuffer = findData.cFileName;
	do 
	{
		strBuffer = findData.cFileName;
		if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
		{
			if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (FindFileFolder(strStartFolder + strBuffer + CString("\\"), strFile, strTargetFolder))
				{
					bFound = TRUE;
					break;
				}
			}
		}
	}
	while ( FindNextFile( hfileSearch, &findData ));
	FindClose(hfileSearch);

	return bFound;
}

BOOL CImpManApp::FolderHasFile(const CString& strFolder, const CString& strFile)
{
	WIN32_FIND_DATA findData;
	CString			strFilename = strFolder + _T("\\*.*");
	HANDLE			hfileSearch = FindFirstFile(strFilename, &findData);
	if (hfileSearch == INVALID_HANDLE_VALUE)
	{
		FindClose(hfileSearch);
		return FALSE;
	}

	BOOL	bFound	  = FALSE;
	CString	strBuffer = findData.cFileName;
	do 
	{
		strBuffer = findData.cFileName;
		if ( (strBuffer != _T(".")) && (strBuffer != _T("..")) )
		{
			if ( ! (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
			{
				//CString strExt = strBuffer.Right(4);
				//strExt.MakeLower();
				//if (strExt == _T(".isc"))
				//{
				//	strBuffer = strBuffer.SpanExcluding(_T("."));
					if (strBuffer == strFile)
					{
						bFound = TRUE;
						break;
					}
				//}
			}
		}
	}
	while ( FindNextFile( hfileSearch, &findData ));
	FindClose(hfileSearch);

	return bFound;
}


/////////////////////////////////////////////////////////////////////////////
// Cookies


void CImpManApp::CountProjectLines(void)
{
	CFile SearchFile;
	CFileException fileException;
	WIN32_FIND_DATA findData;
	TCHAR searchbuffer[MAX_PATH];
	TCHAR buffer[50];
	CString strSearchFile;
	long i;
	long nBytesRead;
	long nBytesReadCPP = 0;
	long nBytesReadH = 0;
	long nBytesReadHelper = 0;
	long nBytesReadBinaries = 0;
	long lines;
	long linesCompleteCPP = 0;
	long linesCompleteH = 0;
	long linesCompleteHelper = 0;
	long files = 0;
	TCHAR filebuffer[262145];

	theApp.m_strTemp.Empty();

/////////////////////////////////////////////////////////////////////////////
//	Get information about the source-files
	_tcscpy_s(searchbuffer, MAX_PATH, _T("S:\\KIM-Projekte\\KIM Client\\KIM 7\\Sources"));//theApp.settings.m_szWorkDir);
	theApp.SetMyCurrentDirectory(searchbuffer, TRUE);
	_tcscat_s(searchbuffer, MAX_PATH, _T("\\*.cpp"));
	HANDLE hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			strSearchFile = findData.cFileName;
			if (SearchFile.Open(strSearchFile, CFile::modeRead, &fileException))
			{
				SearchFile.SeekToBegin();
				nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
				nBytesReadCPP += nBytesRead;
				lines = 0;
				for (i = 0; i < nBytesRead; i++)
					if (filebuffer[i] == '\n')
						lines ++;
				SearchFile.Close();
				linesCompleteCPP += lines;
			}
			files++;
		}while ( FindNextFile( hfileSearch, &findData ));

		theApp.m_strTemp += "Gesamt *.cpp : \r\n";
		theApp.m_strTemp += "Zeilen       : ";
		_ltot(linesCompleteCPP, buffer, 10);
		theApp.m_strTemp += buffer;
		theApp.m_strTemp += "\r\nBytes        : ";
		_ltot(nBytesReadCPP, buffer, 10);
		theApp.m_strTemp += buffer;
		theApp.m_strTemp += "\r\nDateien    : ";
		_ltot(files, buffer, 10);
		theApp.m_strTemp += buffer;
		theApp.m_strTemp += "\r\n\r\n";
	}
	FindClose(hfileSearch);
/////////////////////////////////////////////////////////////////////////////
//	Get information about the header-files
	files = 0;
	_tcscpy_s(searchbuffer, MAX_PATH, theApp.settings.m_szWorkDir);
	_tcscat_s(searchbuffer, MAX_PATH, _T("\\*.h"));
	hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			strSearchFile = findData.cFileName;
			if (SearchFile.Open(strSearchFile, CFile::modeRead, &fileException))
			{
				SearchFile.SeekToBegin();
				nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
				nBytesReadH += nBytesRead;
				lines = 0;
				for (i = 0; i < nBytesRead; i++)
					if (filebuffer[i] == '\n')
						lines ++;
				SearchFile.Close();
				linesCompleteH += lines;
			}
			files++;
		}while ( FindNextFile( hfileSearch, &findData ));

		theApp.m_strTemp += "Gesamt *.h  : \r\n";
		theApp.m_strTemp += "Zeilen      : ";
		_ltot(linesCompleteH, buffer, 10);
		theApp.m_strTemp += buffer;
		theApp.m_strTemp += "\r\nBytes       : ";
		_ltot(nBytesReadH, buffer, 10);
		theApp.m_strTemp += buffer;
		theApp.m_strTemp += "\r\nDateien   : ";
		_ltot(files, buffer, 10);
		theApp.m_strTemp += buffer;
		theApp.m_strTemp += "\r\n\r\n";
	}
	FindClose(hfileSearch);
/////////////////////////////////////////////////////////////////////////////
//	Get information about the helper-files
	files = 0;
	_tcscpy_s(searchbuffer, MAX_PATH, theApp.settings.m_szWorkDir);
	_tcscat_s(searchbuffer, MAX_PATH, _T("\\resource.hm"));
	if (SearchFile.Open(searchbuffer, CFile::modeRead, &fileException))
	{
		SearchFile.SeekToBegin();
		nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
		nBytesReadHelper += nBytesRead;
		lines = 0;
		for (i = 0; i < nBytesRead; i++)
			if (filebuffer[i] == '\n')
				lines ++;
		SearchFile.Close();
		linesCompleteHelper += lines;
		files++;
	}
	_tcscpy (searchbuffer, theApp.settings.m_szWorkDir);
	_tcscat (searchbuffer, _T("\\Imposition Manager.rc"));
	if (SearchFile.Open(searchbuffer, CFile::modeRead, &fileException))
	{
		SearchFile.SeekToBegin();
		nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
		nBytesReadHelper += nBytesRead;
		lines = 0;
		for (i = 0; i < nBytesRead; i++)
			if (filebuffer[i] == '\n')
				lines ++;
		SearchFile.Close();
		linesCompleteHelper += lines;
		files++;
	}
	_tcscpy (searchbuffer, theApp.settings.m_szWorkDir);
	_tcscat (searchbuffer, _T("\\Imposition Manager.clw"));
	if (SearchFile.Open(searchbuffer, CFile::modeRead, &fileException))
	{
		SearchFile.SeekToBegin();
		nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
		nBytesReadHelper += nBytesRead;
		lines = 0;
		for (i = 0; i < nBytesRead; i++)
			if (filebuffer[i] == '\n')
				lines ++;
		SearchFile.Close();
		linesCompleteHelper += lines;
		files++;
	}
	theApp.m_strTemp += "Gesamt Hilfsdateien : \r\n";
	theApp.m_strTemp += "Zeilen      : ";
	_ltot(linesCompleteHelper, buffer, 10);
	theApp.m_strTemp += buffer;
	theApp.m_strTemp += "\r\nBytes       : ";
	_ltot(nBytesReadHelper, buffer, 10);
	theApp.m_strTemp += buffer;
	theApp.m_strTemp += "\r\nDateien   : ";
	_ltot(files, buffer, 10);
	theApp.m_strTemp += buffer;
	theApp.m_strTemp += "\r\n\r\n";
/////////////////////////////////////////////////////////////////////////////
//	Add all values
	theApp.m_strTemp += "Gesamt-Zeilen : ";
	lines	   = linesCompleteCPP + linesCompleteH + linesCompleteHelper;
	_ltot(lines, buffer, 10);
	theApp.m_strTemp += buffer;
	theApp.m_strTemp += "\r\nGesamt-Bytes  : ";
	nBytesRead = nBytesReadCPP + nBytesReadH + nBytesReadHelper;
	_ltot(nBytesRead, buffer, 10);
	theApp.m_strTemp += buffer;
	theApp.m_strTemp += "\r\n\r\n";
/////////////////////////////////////////////////////////////////////////////
//	Binaries -> Bitmaps, Icons .....
	files = 0;
	_tcscpy (searchbuffer, theApp.settings.m_szWorkDir);
	_tcscat (searchbuffer, _T("\\res"));
	theApp.SetMyCurrentDirectory(searchbuffer, TRUE);
	_tcscat (searchbuffer, _T("\\*.*"));
	hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			strSearchFile = findData.cFileName;
			if (strSearchFile != "." && strSearchFile != "..")
			{
				if (SearchFile.Open(strSearchFile, CFile::modeRead, &fileException))
				{
					SearchFile.SeekToBegin();
					nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
					nBytesReadBinaries += nBytesRead;
					SearchFile.Close();
				}
			}
			files++;
		}while ( FindNextFile( hfileSearch, &findData ));

		theApp.m_strTemp += "Gesamt BinaryResources : ";
		_ltot(nBytesReadBinaries, buffer, 10);
		theApp.m_strTemp += buffer;
		theApp.m_strTemp += " Bytes";
		theApp.m_strTemp += "\r\nDateien                            : ";
		_ltot(files, buffer, 10);
		theApp.m_strTemp += buffer;
		theApp.m_strTemp += "\r\n\r\n";
	}
	FindClose(hfileSearch);
/////////////////////////////////////////////////////////////////////////////
//	Get information about DOS-KIM
	long nBytes = 0; 
	long nLines = 0; 
	lines = 0;
	files = 0;
	_tcscpy (searchbuffer, _T("c:\\kim"));
	theApp.SetMyCurrentDirectory(searchbuffer, TRUE);
	_tcscat (searchbuffer, _T("\\ks*.c"));
	hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			strSearchFile = findData.cFileName;
			if (SearchFile.Open(strSearchFile, CFile::modeRead, &fileException))
			{
				SearchFile.SeekToBegin();
				nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
				nBytes += nBytesRead;
				lines = 0;
				for (i = 0; i < nBytesRead; i++)
					if (filebuffer[i] == '\n')
						lines ++;
				SearchFile.Close();
				nLines += lines;
			}
			files++;
		}while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);
	_tcscpy (searchbuffer, _T("c:\\kim"));
	_tcscat (searchbuffer, _T("\\ks*.h"));
	hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			strSearchFile = findData.cFileName;
			if (SearchFile.Open(strSearchFile, CFile::modeRead, &fileException))
			{
				SearchFile.SeekToBegin();
				nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
				nBytes += nBytesRead;
				lines = 0;
				for (i = 0; i < nBytesRead; i++)
					if (filebuffer[i] == '\n')
						lines ++;
				SearchFile.Close();
				nLines += lines;
			}
			files++;
		}while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);
	_tcscpy (searchbuffer, _T("c:\\kim"));
	_tcscat (searchbuffer, _T("\\av*.c"));
	hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			strSearchFile = findData.cFileName;
			if (SearchFile.Open(strSearchFile, CFile::modeRead, &fileException))
			{
				SearchFile.SeekToBegin();
				nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
				nBytes += nBytesRead;
				lines = 0;
				for (i = 0; i < nBytesRead; i++)
					if (filebuffer[i] == '\n')
						lines ++;
				SearchFile.Close();
				nLines += lines;
			}
			files++;
		}while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);
	_tcscpy (searchbuffer, _T("c:\\kim"));
	_tcscat (searchbuffer, _T("\\av*.h"));
	hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			strSearchFile = findData.cFileName;
			if (SearchFile.Open(strSearchFile, CFile::modeRead, &fileException))
			{
				SearchFile.SeekToBegin();
				nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
				nBytes += nBytesRead;
				lines = 0;
				for (i = 0; i < nBytesRead; i++)
					if (filebuffer[i] == '\n')
						lines ++;
				SearchFile.Close();
				nLines += lines;
			}
			files++;
		}while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);
	_tcscpy (searchbuffer, _T("c:\\kim"));
	_tcscat (searchbuffer, _T("\\ks*.txt"));
	hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			strSearchFile = findData.cFileName;
			if (SearchFile.Open(strSearchFile, CFile::modeRead, &fileException))
			{
				SearchFile.SeekToBegin();
				nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
				nBytes += nBytesRead;
				lines = 0;
				for (i = 0; i < nBytesRead; i++)
					if (filebuffer[i] == '\n')
						lines ++;
				SearchFile.Close();
				nLines += lines;
			}
			files++;
		}while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);
	_tcscpy (searchbuffer, _T("c:\\kim"));
	_tcscat (searchbuffer, _T("\\av*.txt"));
	hfileSearch = FindFirstFile(searchbuffer, &findData);
    if (hfileSearch != INVALID_HANDLE_VALUE)
	{
		do 
		{
			strSearchFile = findData.cFileName;
			if (SearchFile.Open(strSearchFile, CFile::modeRead, &fileException))
			{
				SearchFile.SeekToBegin();
				nBytesRead = SearchFile.Read(filebuffer, sizeof(filebuffer));
				nBytes += nBytesRead;
				lines = 0;
				for (i = 0; i < nBytesRead; i++)
					if (filebuffer[i] == '\n')
						lines ++;
				SearchFile.Close();
				nLines += lines;
			}
			files++;
		}while ( FindNextFile( hfileSearch, &findData ));
	}
	FindClose(hfileSearch);
/////////////////////////////////////////////////////////////////////////////
	theApp.m_strTemp += "DOS-KIM Zeilen : ";
	_ltot(nLines, buffer, 10);
	theApp.m_strTemp += buffer;
	theApp.m_strTemp += "\r\nDOS-KIM Bytes  : ";
	_ltot(nBytes, buffer, 10);
	theApp.m_strTemp += buffer;
	theApp.m_strTemp += "\r\nDateien              : ";
	_ltot(files, buffer, 10);
	theApp.m_strTemp += buffer;
	theApp.m_strTemp += "\r\n\r\n";
/////////////////////////////////////////////////////////////////////////////
	theApp.SetMyCurrentDirectory(theApp.settings.m_szWorkDir, TRUE);
}


BOOL CImpManApp::SaveAllModified() 
{
	if (m_nGUIStyle == GUINewTemplate)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();	 
		if (pDoc)
			pDoc->OnSaveDocument(pDoc->GetTitle());
		return 1;
	}
	else
		return CWinApp::SaveAllModified();
}

BOOL CImpManApp::CheckDongleCtP() 
{
	theApp.m_bDemoMode = FALSE;

#ifdef _DEBUG
	return TRUE;
#endif

	RB_SPRO_APIPACKET ApiPacket;
	SP_STATUS         spStatus;
	CString			  strMsg;
	spStatus = RNBOsproFormatPacket( ApiPacket, sizeof(ApiPacket) );
	if (spStatus != 0)
	{
		strMsg.Format(IDS_SENTINEL_INVALID_PACKET, spStatus);
		AfxMessageBox(strMsg, MB_OK);
		return FALSE;
	}
	else
	{
		spStatus = RNBOsproInitialize( ApiPacket );
		if (spStatus != 0)
		{
			strMsg.Format(IDS_SENTINEL_DRIVER_ERROR, spStatus);
			AfxMessageBox(strMsg, MB_OK);
			return FALSE;
		}
		else
		{
			for (int i = 0; i < 5; i++)
				if ( (spStatus = RNBOsproFindFirstUnit( ApiPacket, 0xd75c )) == 0)
					break;
			if (spStatus != 0)
			{
				theApp.m_nDongleStatus = CImpManApp::NoDongle;
			}
			else
			{
				unsigned short nData;
				RNBOsproRead( ApiPacket, 0x000F, &nData);
				if (nData == 0x4000)
				{
					RNBOsproRead( ApiPacket, 0x000E, &nData);
					switch (nData)
					{
					case 0xCCCC: theApp.m_nDongleStatus = CImpManApp::StandardDongle;	break;
					default:	 theApp.m_nDongleStatus = CImpManApp::NoDongle;			break;
					}
				}
				else
				{
					theApp.m_nDongleStatus = CImpManApp::NoDongle;
				}
			}
		}
	}
	if (theApp.m_nDongleStatus == CImpManApp::NoDongle)
	{
		strMsg.Format(IDS_SENTINEL_KEY_NOT_FOUND_CTP, spStatus);
		AfxMessageBox(strMsg, MB_OK);
		return FALSE;
	}
	else
		return TRUE;
}

CString CImpManApp::RemoveFileExtension(const CString& strFilename, CString strExtension)
{
	CString strTemp = strFilename;
	int		nIndex	= strTemp.ReverseFind('.');
	if (nIndex == -1)
		return strTemp;

	CString strExt = strTemp.Right(strTemp.GetLength() - nIndex);
	strExt.MakeLower();
	strExtension.MakeLower();
	if (strExt != strExtension)
		return strTemp;

	return strTemp.Left(nIndex);
}

CString CImpManApp::GetRegistryKey(const CString& strKey, const CString strDefault)
{
	const int nValueLenght = _MAX_PATH + 2;
	TCHAR	  szValue[nValueLenght];
	DWORD	  dwSize = nValueLenght;
	DWORD	  dwDisposition; 
	HKEY	  hKey;
	long ret = RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)m_strRegistryKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition); 
	if (RegQueryValueEx(hKey, strKey, 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		return CString(szValue);
	else
		return strDefault;
	ret = RegCloseKey(hKey); 
}

void CImpManApp::SetRegistryKey(const CString& strKey, CString strValue)
{
	int  nLength = strValue.GetLength();
	HKEY hKey;
	long ret = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)theApp.m_strRegistryKey, &hKey);
	RegSetValueEx(hKey, strKey, 0, REG_SZ, (LPBYTE)strValue.GetBuffer(nLength), nLength);
	ret = RegCloseKey(hKey);
}

CString CImpManApp::BuildKIMTitle(const CString& strPrefix)
{
	CString strTitle = strPrefix;
	switch (theApp.m_nDongleStatus)
	{
	case CImpManApp::NoDongle:				strTitle += _T(" Demo");				break;
	case CImpManApp::StandardDongle:		strTitle += _T(" Standard");			break;
	case CImpManApp::ClientDongle:			strTitle += _T(" Client");				break;
	case CImpManApp::AutoDongle:			strTitle += _T(" Auto");				break;
	case CImpManApp::SmallFormatDongle:		strTitle += _T(" Small Format");		break;
	case CImpManApp::AutoSmallFormatDongle:	strTitle += _T(" Auto Small Format");	break;
	case CImpManApp::NewsDongle:			strTitle += _T(" News");				break;
	case CImpManApp::DigitalDongle:			strTitle += _T(" Digital");				break;
	default:								strTitle += _T(" Version");				break;
	}
	return strTitle;
}

HTHEME CImpManApp::_OpenThemeData(CWnd* pWnd, LPCWSTR pszClassList)
{
	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		if (IsAppThemed())
			return OpenThemeData(pWnd->GetSafeHwnd(), pszClassList);
		else
			return NULL;
	else
		return NULL;
}

void CImpManApp::_CloseThemeData(HTHEME hTheme)
{
	if ( ! hTheme)
		return;
	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		CloseThemeData(hTheme);
}

void CImpManApp::UndoSave(CMemFile* pMemFile)
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! pMemFile)
		pMemFile = &theApp.m_undoBuffer;
	pMemFile->SeekToBegin();
	CArchive archive(pMemFile, CArchive::store);
	pMemFile->SetFilePath(_T("UndoRestore"));

	pDoc->Serialize(archive);

	archive.Close();
}

void CImpManApp::UndoRestore(CMemFile* pMemFile)
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if ( ! pMemFile)
		pMemFile = &theApp.m_undoBuffer;
	pMemFile->SeekToBegin();
	CArchive archive(pMemFile, CArchive::load);
	pMemFile->SetFilePath(_T("UndoRestore"));

	pDoc->DeleteContents();
	pDoc->Serialize(archive);

	archive.Close();
	pMemFile->SetLength(0);
 
	pDoc->SetModifiedFlag(FALSE);

	pDoc->m_PageTemplateList.InvalidateProductRefs();

	OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), NULL, 0, NULL, TRUE);	// reorganize pointers to foldsheets 
	OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	// reorganize pointers to foldsheets in table

	pDoc->UpdateAllViews(NULL);
}

CString CImpManApp::GetDataFolder()
{
	CString strUNC;
	if (theApp.m_bServerConnected)
	{
		if (CString(theApp.settings.m_szAutoServerNameIP).CompareNoCase(_T("localhost")) == 0)
			strUNC = theApp.settings.m_szDataDir;
		else
			strUNC.Format(_T("\\\\%s\\KimAutoData"), theApp.settings.m_szAutoServerNameIP);
		return strUNC;
	}
	else
		return theApp.settings.m_szDataDir;
}


char g_szCode[] = "CFFDDDF3-C72E-4034-A93A-A7BB67E961CE";	// upgrade code from install shield
int EVALDAYS[] = {5,4};

int CImpManApp::CheckRemainingDays()
{
	int nEvaluationDays = EVALDAYS[0] + 10 * EVALDAYS[1];
#ifdef KIM_ENGINE
	return nEvaluationDays;
#endif

	float g_codeArray[64];
	for (int i = 0; i < 64; i++)
		g_codeArray[i] = 0.0f;
	for (int i = 0; (i < strlen(g_szCode)) && (i < 64); i++)
		g_codeArray[i] = g_szCode[i];

	CString strFile, strFile1;
	GetSystemDirectory(strFile.GetBuffer(1024), 1024); strFile.ReleaseBuffer();
	strFile1 = strFile;
	strFile  += _T("\\sysmik.dll");
	strFile1 += _T("\\domejo.dll");

	COleDateTime curDateTime = COleDateTime::GetCurrentTime();
	COleDateTime firstDateTime, lastDateTime;

	int					nExpiredDays = 0;
	CFileStatus			fileStatus;
	CFileException		fileException;
	CFile				file;
	if ( ! file.Open((LPCTSTR)strFile, CFile::modeRead, &fileException))
	{
		if (CFile::GetStatus(strFile, fileStatus))	// file exists -> maybe is opened by another kim instance -> assume everthing OK
			return nEvaluationDays;

		if ( ! theApp.m_bDemoMode)
		{
			CreateFileBySecurity(strFile);

			if ( ! file.Open((LPCTSTR)strFile, CFile::modeWrite, &fileException))
				return 0;
			else
			{
				CArchive archiveStore(&file, CArchive::store);
				for (int i = 0; i < 64; i++)
					archiveStore << g_codeArray[i];
				archiveStore << curDateTime;
				archiveStore << curDateTime;
				archiveStore.Close();
				file.Close();

				CreateFileBySecurity(strFile1);

				if ( ! file.Open((LPCTSTR)strFile1, CFile::modeWrite, &fileException))
					return 0;

				CArchive archiveStore1(&file, CArchive::store);
				for (int i = 0; i < 64; i++)
					archiveStore1 << g_codeArray[i];
				archiveStore1 << curDateTime;
				archiveStore1 << curDateTime;
				archiveStore1.Close();
				file.Close();

				CFile::GetStatus(strFile, fileStatus);	
				fileStatus.m_ctime = CTime(2007, 3, 21, 9, 21, 43, 0);		// assign any time stamp
				fileStatus.m_mtime = CTime(2007, 3, 21, 9, 21, 43, 0);		
				fileStatus.m_atime = CTime(2007, 3, 21, 9, 21, 43, 0);		
				CFile::SetStatus(strFile, fileStatus);	

				CFile::GetStatus(strFile1, fileStatus);	
				fileStatus.m_ctime = CTime(2006, 7, 15, 16, 28, 10, 0);		
				fileStatus.m_mtime = CTime(2006, 7, 15, 16, 28, 10, 0);		
				fileStatus.m_atime = CTime(2006, 7, 15, 16, 28, 10, 0);		
				CFile::SetStatus(strFile1, fileStatus);	

				return nEvaluationDays;
			}
		}
		return 0;
	}

	if ( ! theApp.m_bDemoMode)
		firstDateTime  = curDateTime;
	else
	{
		CArchive archiveLoad(&file, CArchive::load);
		float codeArray[64];
		for (int i = 0; i < 64; i++)
			archiveLoad >> codeArray[i];
		archiveLoad >> firstDateTime; 
		archiveLoad >> lastDateTime;
		archiveLoad.Close();
		for (int i = 0; i < 64; i++)
			if (codeArray[i] != g_codeArray[i])
				return 0;

		if (curDateTime < lastDateTime)
		{
			COleDateTimeSpan spanOneDay(1, 0, 0, 0);
			firstDateTime = lastDateTime - curDateTime - spanOneDay;
		}
	}

	COleDateTimeSpan timeSpan = curDateTime - firstDateTime;
	nExpiredDays = (int)timeSpan.GetTotalDays();

	file.Close();

	if (file.Open((LPCTSTR)strFile, CFile::modeWrite | CFile::typeBinary, &fileException))
	{
		CArchive archiveStore(&file, CArchive::store);
		for (int i = 0; i < 64; i++)
			archiveStore << g_codeArray[i];
		archiveStore << firstDateTime;
		archiveStore << curDateTime;
		archiveStore.Close();
		file.Close();
	}

	CFile::GetStatus(strFile, fileStatus);	
	fileStatus.m_ctime = CTime(2007, 3, 21, 9, 21, 43, 0);		// assign any time stamp
	fileStatus.m_mtime = CTime(2007, 3, 21, 9, 21, 43, 0);		
	fileStatus.m_atime = CTime(2007, 3, 21, 9, 21, 43, 0);		
	CFile::SetStatus(strFile, fileStatus);	

	return (nEvaluationDays - nExpiredDays);
}

void CreateFileBySecurity(const CString& strFile)
{
	PSID pEveryoneSID = NULL, pAdminSID = NULL;
    PACL pACL = NULL;
    PSECURITY_DESCRIPTOR pSD = NULL;
    EXPLICIT_ACCESS ea[2];
    SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;
	SID_IDENTIFIER_AUTHORITY SIDAuthNT = SECURITY_NT_AUTHORITY;
	SECURITY_ATTRIBUTES sa;

	// Create a well-known SID for the Everyone group.
	AllocateAndInitializeSid(&SIDAuthWorld, 1, SECURITY_WORLD_RID, 0, 0, 0, 0, 0, 0, 0, &pEveryoneSID);

	// Initialize an EXPLICIT_ACCESS structure for an ACE.
    // The ACE will allow Everyone read access to the key.
    ZeroMemory(&ea, 2 * sizeof(EXPLICIT_ACCESS));
    ea[0].grfAccessPermissions = FILE_ALL_ACCESS;
    ea[0].grfAccessMode = SET_ACCESS;
    ea[0].grfInheritance= NO_INHERITANCE;
    ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
    ea[0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
    ea[0].Trustee.ptstrName  = (LPTSTR) pEveryoneSID;

	// Create a SID for the BUILTIN\Administrators group.
	AllocateAndInitializeSid(&SIDAuthNT, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &pAdminSID);

    // Initialize an EXPLICIT_ACCESS structure for an ACE.
    // The ACE will allow the Administrators group full access to
    // the key.
    ea[1].grfAccessPermissions = FILE_ALL_ACCESS;
    ea[1].grfAccessMode = SET_ACCESS;
    ea[1].grfInheritance= NO_INHERITANCE;
    ea[1].Trustee.TrusteeForm = TRUSTEE_IS_SID;
    ea[1].Trustee.TrusteeType = TRUSTEE_IS_GROUP;
    ea[1].Trustee.ptstrName  = (LPTSTR) pAdminSID;

	DWORD dwRes = SetEntriesInAcl(2, ea, NULL, &pACL);

    pSD = (PSECURITY_DESCRIPTOR) LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH); 
	InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION);
	SetSecurityDescriptorDacl(pSD, TRUE, pACL, FALSE);   

	sa.nLength = sizeof (SECURITY_ATTRIBUTES);
    sa.lpSecurityDescriptor = pSD;
    sa.bInheritHandle = FALSE;

	HANDLE hFile = CreateFile(strFile, WRITE_OWNER|WRITE_DAC, FILE_SHARE_WRITE, &sa, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	CString strMsg;
    if (hFile == INVALID_HANDLE_VALUE) 
    { 
		strMsg.Format("Could not open file (error %d)\n", GetLastError());
    }

    if (pEveryoneSID) 
        FreeSid(pEveryoneSID);
    if (pAdminSID) 
        FreeSid(pAdminSID);
    if (pACL) 
        LocalFree(pACL);
    if (pSD) 
        LocalFree(pSD);
}

void CAboutDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	CDC dcMemory;
	dcMemory.CreateCompatibleDC(&dc);

	CBitmap oBmp;
	if ( ! oBmp.LoadBitmap(IDB_KIM_STARTUP))
		return;

	CBitmap* pOldBitmap = dcMemory.SelectObject(&oBmp);
	BITMAP sBmp;
	oBmp.GetBitmap(&sBmp);
	
	CPoint ptSrcSize;
	ptSrcSize.x = sBmp.bmWidth;
	ptSrcSize.y = sBmp.bmHeight;
	dcMemory.DPtoLP(&ptSrcSize);

	dc.BitBlt(0, 0, ptSrcSize.x, ptSrcSize.y, &dcMemory,
		0, 0, SRCCOPY);
	dcMemory.SelectObject(pOldBitmap);

	CFont standardFont, smallFont;//, bigFont;
	standardFont.CreateFont (14, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	smallFont.CreateFont	(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));

	dc.SetTextColor(BLACK);
	dc.SetBkMode(TRANSPARENT);
	dc.SelectObject(&standardFont);
	CString strOut;
	strOut.Format(_T("Version %s  %s"), theApp.strVersion, CString(__DATE__));
	CRect textRect(CPoint(310, 270), CSize(300, 50));
	dc.DrawText(strOut, textRect, DT_LEFT | DT_TOP);

	dc.SelectObject(&smallFont);

	textRect += CSize(0, 30);
	dc.DrawText(theApp.strCopyright, textRect, DT_LEFT | DT_WORDBREAK);

	dc.SelectClipRgn(NULL);  

	CString strCallasLib = theApp.settings.m_szWorkDir + CString("\\pdfEngine.dll");
	DWORD   dwVerInfoSize;        
	LPSTR   lpProduct, lpVersion;            
	DWORD   dwVerHnd=0;  // An 'ignored' parameter, always '0'
	UINT    uVersionLen;
	BOOL    bRetCode;

	dwVerInfoSize = GetFileVersionInfoSize(strCallasLib.GetBuffer(MAX_TEXT), &dwVerHnd);
	if (dwVerInfoSize) 
	{
		BYTE* lpBuff = new BYTE[dwVerInfoSize];
		GetFileVersionInfo(strCallasLib.GetBuffer(MAX_TEXT), dwVerHnd, dwVerInfoSize, lpBuff);

		// Product Version
		bRetCode = VerQueryValue((LPVOID)lpBuff, TEXT("\\StringFileInfo\\040704b0\\ProductName"), (LPVOID *)&lpProduct, &uVersionLen);

		CString strPDFEngineInfo;
		if(bRetCode)
		{
			bRetCode = VerQueryValue((LPVOID)lpBuff, TEXT("\\StringFileInfo\\040704b0\\ProductVersion"), (LPVOID *)&lpVersion, &uVersionLen);
			if(bRetCode)
				strPDFEngineInfo.Format(_T("pdfEngine Library\nVersion %s\n(c) 1999-2012 callas software gmbh"), lpVersion);
		}

		GetDlgItem(IDC_PDF_ENGINE_INFO)->GetWindowRect(textRect);
		ScreenToClient(textRect);

		dc.SelectObject(&smallFont);
		dc.SetTextColor(DARKGRAY);
		dc.DrawText(strPDFEngineInfo, textRect, DT_LEFT | DT_WORDBREAK);

		delete lpBuff;
	}
	standardFont.DeleteObject();
	smallFont.DeleteObject();
//	bigFont.DeleteObject();
}

void CAboutDlg::OnButtonKimhomepage() 
{
	ShellExecute(NULL, _T("open"), _T("http://www.krause-imposition-manager.de"), NULL, NULL, SW_SHOWNORMAL);	
}

void CImpManApp::MyReadRecentFileList()
{
	//if (theApp.m_bServerConnected)
	//	return;

	if (m_pRecentFileList)
		delete m_pRecentFileList;

	m_pRecentFileList = new CRecentFileList(0, _T("Recent File List"), _T("File %d"), 8);//nMaxMRU);

	const int nValueLenght = _MAX_PATH + 2;
	TCHAR	  szValue[nValueLenght];
	DWORD	  dwSize = nValueLenght;
	HKEY	  hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\Recent File List\\");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		CString strEntry;
		for (int iMRU = 0; iMRU < m_pRecentFileList->m_nSize; iMRU++)
		{
			dwSize = nValueLenght;
			strEntry.Format(_T("File %d"), iMRU + 1);
			if (RegQueryValueEx(hKey, strEntry, 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
				m_pRecentFileList->m_arrNames[iMRU] = szValue;
			else
				m_pRecentFileList->m_arrNames[iMRU].Empty();
		}
		RegCloseKey(hKey);
	}
}

void CImpManApp::MyWriteRecentFileList()
{
	//if (theApp.m_bServerConnected)
	//	return;

	if ( ! m_pRecentFileList)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\Recent File List\\");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		CString strEntry;
		for (int iMRU = 0; iMRU < m_pRecentFileList->m_nSize; iMRU++)
		{
			strEntry.Format(_T("File %d"), iMRU + 1);
			if ( ! m_pRecentFileList->m_arrNames[iMRU].IsEmpty())
				RegSetValueEx(hKey, strEntry, 0, REG_SZ, (LPBYTE)m_pRecentFileList->m_arrNames[iMRU].GetBuffer(), m_pRecentFileList->m_arrNames[iMRU].GetLength() + 1);
			else
				RegDeleteKey(hKey, strEntry);
		}
		RegCloseKey(hKey);
	}
}

CString CImpManApp::GetAutoServerDatabasePath()
{
	CString	  strDir;
	const int nValueLenght = _MAX_PATH + 2;
	TCHAR	  szValue[nValueLenght];
	DWORD	  dwSize = nValueLenght; 
	HKEY	  hKey;
	CString   strKey = "SOFTWARE\\Krause\\KIM7.1 Settings";
	DWORD	  dwDisposition; 
	long ret = RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition); 
	if (RegQueryValueEx(hKey, _T("AutoDatabasePath"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		strDir = szValue;
	else
		strDir = CString(theApp.settings.m_szWorkDir) + _T("C:\\Program Files\\Firebird\\Firebird_2_5\\examples\\kimAuto\\KIMAUTO.FDB");

	RegCloseKey(hKey);

	return strDir;
}

CString CImpManApp::GetAutoServerHotFolderRoot()
{
	CString	  strDir;
	const int nValueLenght = _MAX_PATH + 2;
	TCHAR	  szValue[nValueLenght];
	DWORD	  dwSize = nValueLenght;
	HKEY	  hKey;
	CString   strKey = "SOFTWARE\\Krause\\KIM7.1 Settings";
	DWORD	  dwDisposition; 
	long ret = RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition); 
	if (RegQueryValueEx(hKey, _T("AutoHotFolderRoot"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		strDir = szValue;
	else
	{
		strDir = CString(theApp.settings.m_szWorkDir) + _T("\\AutoHotFolders");
		if ( ! DirectoryExist(strDir))
		{
			CString strMsg;
			strMsg.Format(_T("Cannot access network folder %s!"), strDir);
			KIMOpenLogMessage(strMsg, MB_ICONERROR);
		}
	}

	RegCloseKey(hKey);

	return strDir;
}

CString CImpManApp::GetAutoServerJobFolderRoot()
{
	CString strUNC;

	if (CString(theApp.settings.m_szAutoServerNameIP).CompareNoCase(_T("localhost")) == 0)
	{
		CString	  strDir;
		const int nValueLenght = _MAX_PATH + 2;
		TCHAR	  szValue[nValueLenght];
		DWORD	  dwSize = nValueLenght;
		HKEY	  hKey;
		CString   strKey = "SOFTWARE\\Krause\\KIM7.1 Settings";
		DWORD	  dwDisposition; 
		long ret = RegCreateKeyEx(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_QUERY_VALUE, NULL, &hKey, &dwDisposition); 
		if (RegQueryValueEx(hKey, _T("AutoJobFolderRoot"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
			strUNC = szValue;
		else
			strUNC = CString(theApp.settings.m_szWorkDir) + _T("\\AutoJobs");

		RegCloseKey(hKey);
	}
	else
	{
		strUNC.Format(_T("\\\\%s\\KimAutoJobs"), theApp.settings.m_szAutoServerNameIP);
		if ( ! DirectoryExist(strUNC))
		{
			CString strMsg;
			strMsg.Format(_T("Cannot access network folder %s!"), strUNC);
			KIMOpenLogMessage(strMsg, MB_ICONERROR);
		}
	}
	return strUNC;
}

CAutoServerFrame* CImpManApp::GetAutoServerFrame()
{
	if ( ! theApp.m_pMainWnd)
		return NULL;

	CMainFrame* pMainFrame = (CMainFrame*)theApp.m_pMainWnd;
	pMainFrame->m_pWndCurrentChild = NULL;

	CWnd* pWnd = pMainFrame->GetNextMDIChildWnd();
	while (pWnd)
	{
		if (pWnd->IsKindOf(RUNTIME_CLASS(CAutoServerFrame)))
			return (CAutoServerFrame*)pWnd;

		pWnd = pMainFrame->GetNextMDIChildWnd();
	}
	return NULL;
}

CAutoServerToolsView* CImpManApp::GetAutoServerToolsView()
{
	CAutoServerFrame* pFrame = GetAutoServerFrame();
	if ( ! pFrame)
		return NULL;
	else
		return pFrame->GetAutoServerToolsView();
}

CAutoServerJobsView* CImpManApp::GetAutoServerJobsView()
{
	CAutoServerFrame* pFrame = GetAutoServerFrame();
	if ( ! pFrame)
		return NULL;
	else
		return pFrame->GetAutoServerJobsView();
}

CAutoServerWorkflowsView* CImpManApp::GetAutoServerWorkflowsView()
{
	CAutoServerFrame* pFrame = GetAutoServerFrame();
	if ( ! pFrame)
		return NULL;
	else
		return pFrame->GetAutoServerWorkflowsView();
}

CAutoServerLogsView* CImpManApp::GetAutoServerLogsView()
{
	CAutoServerFrame* pFrame = GetAutoServerFrame();
	if ( ! pFrame)
		return NULL;
	else
		return pFrame->GetAutoServerLogsView();
}

CAutoServerDetailsView* CImpManApp::GetAutoServerDetailsView()
{
	CAutoServerFrame* pFrame = GetAutoServerFrame();
	if ( ! pFrame)
		return NULL;
	else
		return pFrame->GetAutoServerDetailsView();
}

void CImpManApp::UpdateAutoServerJobsView(BOOL bInvalidate, BOOL bInvalidateDisplayList)
{
	CAutoServerJobsView* pView = GetAutoServerJobsView();
	if ( ! pView)
		return;

	if (pView->m_DisplayList.m_bIsRegistering)		// update is just running
		return;

	if (bInvalidate)
		pView->Invalidate();
	if (bInvalidateDisplayList)
		pView->m_DisplayList.Invalidate();

	pView->OnUpdate(NULL, 0, NULL);
	pView->UpdateWindow();
}

void CImpManApp::UpdateAutoServerWorkflowsView(BOOL bInvalidate, BOOL bInvalidateDisplayList)
{
	CAutoServerWorkflowsView* pView = GetAutoServerWorkflowsView();
	if ( ! pView)
		return;

	if (pView->m_DisplayList.m_bIsRegistering)		// update is just running
		return;

	if (bInvalidate)
		pView->Invalidate();
	if (bInvalidateDisplayList)
		pView->m_DisplayList.Invalidate();

	pView->OnUpdate(NULL, 0, NULL);
	pView->UpdateWindow();
}

void CImpManApp::UpdateAutoServerLogsView(BOOL bInvalidate, BOOL bInvalidateDisplayList)
{
	CAutoServerLogsView* pView = GetAutoServerLogsView();
	if ( ! pView)
		return;

	if (pView->m_DisplayList.m_bIsRegistering)		// update is just running
		return;

	if (bInvalidate)
		pView->Invalidate();
	if (bInvalidateDisplayList)
		pView->m_DisplayList.Invalidate();

	pView->OnUpdate(NULL, 0, NULL);
	pView->UpdateWindow();
}

void CImpManApp::UpdateAutoServerDetailsView(BOOL bInvalidate, BOOL bInvalidateDisplayList)
{
	CAutoServerDetailsView* pView = GetAutoServerDetailsView();
	if ( ! pView)
		return;

	if (pView->m_DisplayList.m_bIsRegistering)		// update is just running
		return;

	if (bInvalidate)
		pView->Invalidate();
	if (bInvalidateDisplayList)
		pView->m_DisplayList.Invalidate();

	pView->OnUpdate(NULL, 0, NULL);
	pView->UpdateWindow();
}

int	CImpManApp::AutoServerConnect()
{
	if (theApp.m_bServerConnected)
		return 1;
	if (strlen(theApp.settings.m_szAutoServerNameIP) <= 0)
		return -1;

	m_bSocketsInitialized = FALSE;
	if( AfxSocketInit() == FALSE)
	{
		AfxMessageBox("Failed to Initialize Sockets", MB_ICONERROR); 
		return -1;
	}
	m_bSocketsInitialized = TRUE;

	if(m_clientSocket.Create() == FALSE)
	{
		AfxMessageBox("Failed to Create Socket", MB_ICONERROR);
		return -1;
	}

	m_clientSocket.Connect(settings.m_szAutoServerNameIP, 1001);

	return 0;
}

int	CImpManApp::AutoServerDisconnect()
{
	if ( ! theApp.m_bServerConnected)
		return 1;

	theApp.m_kaDB.kA_Disconnect();

	theApp.m_bServerConnected = FALSE;

	m_clientSocket.ShutDown();

	return 0;
}

BOOL CImpManApp::OnIdle(LONG lCount)
{
	BOOL bMore = CWinApp::OnIdle(lCount);

#ifndef _DEBUG
#ifdef KIM_ENGINE
	if (lCount >= 1000000L)
	{
		bMore = FALSE;
		if ( ! CWnd::FindWindow(_T("KIMPDFServerApp"), NULL))	// if server down, close engine
			OnAppExit();
	}
	else
		bMore = TRUE;
#endif
#endif

	return bMore;
}

int CImpManApp::DoMessageBox(LPCTSTR lpszPrompt, UINT nType, UINT nIDPrompt)
{
#ifdef KIM_ENGINE
	KIMLogMessage(lpszPrompt, nType);
	switch (nType & MB_ICONMASK)
	{
	case MB_ICONINFORMATION:	return IDOK;
	case MB_ICONWARNING:		return IDOK;
	case MB_ICONERROR:			return IDOK;
	case MB_ICONQUESTION:		return IDYES;
	}
	switch (nType & MB_TYPEMASK)
	{
	case MB_YESNO:				return IDYES;
	}
	return IDOK;
#else
	CSplashWnd::HideSplashScreen();	// if splash window is still open (after program startup) -> close it before open message box

	return CWinApp::DoMessageBox(lpszPrompt, nType, nIDPrompt);
#endif

}
