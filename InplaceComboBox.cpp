// InplaceComboBox.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceComboBox.h"


// CInplaceComboBox

IMPLEMENT_DYNAMIC(CInplaceComboBox, CComboBox)

CInplaceComboBox::CInplaceComboBox()
{
	m_lParam	 = 0;
	m_pBitmap	 = NULL;
	m_di		 = CDisplayItem();
	m_nTooltipID = -1;
}

CInplaceComboBox::~CInplaceComboBox()
{
	m_Font.DeleteObject();
	if (m_pBitmap)
	{
		m_pBitmap->DeleteObject();
		delete m_pBitmap;
		m_pBitmap = NULL;
	}

	if ((HBITMAP)m_bmSurface)
		m_bmSurface.DeleteObject();

	//if (m_nTooltipID != -1)
	//{
	//	if ( ! m_tooltip.m_hWnd)
	//		return;

	//	int nCount = m_tooltip.GetToolCount();
	//	for (int n = 0; n < nCount; n++)
	//		m_tooltip.DelTool(this, 1);	
	//}
}

BOOL CInplaceComboBox::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, int nTooltipID )
{
	//dwStyle|= BS_FLAT;

	if (CComboBox::Create(dwStyle | CBS_DROPDOWNLIST, rect, pParentWnd, nID) == FALSE)
		return FALSE;
	if ( ! m_Font.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma")))
		return FALSE;
	SetFont(&m_Font, FALSE);
	
	m_nTooltipID = nTooltipID;

	//if (m_nTooltipID != -1)
	//{
	//	m_tooltip.Create(this);
	//	// Add tooltip 
	//	m_tooltip.AddTool(this, m_nTooltipID, CRect(0,0, 20, 20), 1);	// set 20,20 as default, because rect is 0,0,0,0
	//}

	return TRUE;
}

void CInplaceComboBox::Activate(CRect rect, void (*pFnInitializeCallback)(CComboBox* pBox), const CString& rString, LPARAM lParam, CDisplayItem di) 
{
	m_lParam = lParam;
	m_di	 = di;

	if (!IsWindowVisible())
	{
		SaveBackground(rect);
		GetParent()->SetFocus();
		ShowWindow(SW_HIDE);
	
		MoveWindow(rect, FALSE);	
		SetWindowText(rString);
		ShowWindow(SW_SHOW);	
		UpdateWindow();
	}

	//if (m_nTooltipID != -1)
	//{
	//	m_tooltip.SetToolRect(this, m_nTooltipID, CRect(0,0, rect.Width(), rect.Height()));
	//	m_tooltip.Activate(TRUE);
	//}

	ResetContent();
	if (pFnInitializeCallback)
		pFnInitializeCallback(this);
	SelectString(-1, rString);
	ShowDropDown();

	SetFocus();
	GetParent()->ValidateRect(rect);
}

void CInplaceComboBox::Move(CRect newRect, const CString& rString, LPARAM lParam) 
{
	m_lParam = lParam;

	if (!IsWindowVisible())
		return;

	CRect oldRect;
	GetWindowRect(oldRect);
	GetParent()->ScreenToClient(oldRect);

	//if (m_nTooltipID != -1)
	//	m_tooltip.SetToolRect(this, m_nTooltipID, CRect(0,0, newRect.Width(), newRect.Height()));

	GetParent()->SetFocus();
	ShowWindow(SW_HIDE);
	RestoreBackground(oldRect);
	SaveBackground(newRect);
	MoveWindow(newRect, FALSE);	
	SetWindowText(rString);
	ShowWindow(SW_SHOW);
	UpdateWindow();
	SetFocus();
	GetParent()->ValidateRect(newRect);
}

void CInplaceComboBox::Deactivate() 
{
	if (!IsWindowVisible())
		return;

//	GetParent()->SetFocus();
	ShowWindow(SW_HIDE); // make invisible	

	if (m_pBitmap)
	{
		CRect oldRect;
		GetWindowRect(oldRect);
		GetParent()->ScreenToClient(oldRect);
		RestoreBackground(oldRect);
	}

	//if (m_nTooltipID != -1)
	//	m_tooltip.Activate(FALSE);
}

void CInplaceComboBox::SaveBackground(const CRect& rect)
{
	if (m_pBitmap)
	{
		m_pBitmap->DeleteObject();
		delete m_pBitmap;
	}
	m_pBitmap = new CBitmap;

 	CDC* pDC = GetParent()->GetDC(); 
	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	m_pBitmap->CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	GetParent()->ReleaseDC(pDC);
}

void CInplaceComboBox::RestoreBackground(const CRect& rect)
{
	if (!m_pBitmap)
		return;

	CDC* pDC = GetParent()->GetDC(); 
	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBitmap);
	pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	GetParent()->ReleaseDC(pDC);

	m_pBitmap->DeleteObject();
	delete m_pBitmap;
	m_pBitmap = NULL;

	GetParent()->ValidateRect(rect);
}

CString CInplaceComboBox::GetSelString() 
{
	int nSel = GetCurSel();
	if (nSel == CB_ERR)
		return _T("");

	CString string;
	GetLBText(nSel, string);
	return string;
}


BEGIN_MESSAGE_MAP(CInplaceComboBox, CComboBox)
	ON_CONTROL_REFLECT(CBN_SELCHANGE, OnSelChange)
	ON_WM_KILLFOCUS()
	ON_CONTROL_REFLECT(CBN_CLOSEUP, &CInplaceComboBox::OnCbnCloseup)
END_MESSAGE_MAP()



// CInplaceComboBox-Meldungshandler

void CInplaceComboBox::OnSelChange() 
{
	if (m_pFnSelChangeHandler)
		m_pFnSelChangeHandler(GetParent());
}

void CInplaceComboBox::OnKillFocus(CWnd* pNewWnd) 
{
	if (!IsWindowVisible())
		return;

	//if (!m_bKillFocusHandlerLocked)
	//{
		Deactivate();

	//	if (m_pFnKillFocusHandler)
	//		m_pFnKillFocusHandler(GetParent());
	//}

	CComboBox::OnKillFocus(pNewWnd);
}

BOOL CInplaceComboBox::PreTranslateMessage(MSG* pMsg) 
{
 	//if (m_nTooltipID != -1)
		//// Let the ToolTip process this message.
		//m_tooltip.RelayEvent(pMsg);

	return CComboBox::PreTranslateMessage(pMsg);
}

void CInplaceComboBox::OnCbnCloseup()
{
	if (!IsWindowVisible())
		return;

	Deactivate();
}
