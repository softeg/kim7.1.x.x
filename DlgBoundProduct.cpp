// DlgBoundProduct.cpp: Implementierungsdatei
//


#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgJobData.h"
#include "DlgBoundProduct.h"
#include "DlgPageFormats.h"
#include "DlgBindingDefs.h"
#include "DlgSelectJobColors.h"
#include "DlgBoundProductDetails.h"


// CPPCCustomPaintFrame

IMPLEMENT_DYNAMIC(CPPCCustomPaintFrame, CStatic)

CPPCCustomPaintFrame::CPPCCustomPaintFrame()
{
}

CPPCCustomPaintFrame::~CPPCCustomPaintFrame()
{
}


BEGIN_MESSAGE_MAP(CPPCCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CPPCCustomPaintFrame-Meldungshandler

void CPPCCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rcFrame;
	GetClientRect(rcFrame);
	switch (GetDlgCtrlID())
	{
	case IDC_PRODUCTPART1_COLORS:	DrawBoundProductPartColors (&dc, rcFrame, 0); break;
	case IDC_PRODUCTPART2_COLORS:	DrawBoundProductPartColors (&dc, rcFrame, 1); break;
	case IDC_PRODUCTPART3_COLORS:	DrawBoundProductPartColors (&dc, rcFrame, 2); break;
	case IDC_PRODUCTPART4_COLORS:	DrawBoundProductPartColors (&dc, rcFrame, 3); break;
	case IDC_PRODUCTPART5_COLORS:	DrawBoundProductPartColors (&dc, rcFrame, 4); break;
	case IDC_PRODUCTPART6_COLORS:	DrawBoundProductPartColors (&dc, rcFrame, 5); break;
	case IDC_PRODUCTPART7_COLORS:	DrawBoundProductPartColors (&dc, rcFrame, 6); break;
	case IDC_PRODUCTPART8_COLORS:	DrawBoundProductPartColors (&dc, rcFrame, 7); break;
	case IDC_PRODUCTPART1_DETAILS:	DrawBoundProductPartDetails(&dc, rcFrame, 0); break;
	case IDC_PRODUCTPART2_DETAILS:	DrawBoundProductPartDetails(&dc, rcFrame, 1); break;
	case IDC_PRODUCTPART3_DETAILS:	DrawBoundProductPartDetails(&dc, rcFrame, 2); break;
	case IDC_PRODUCTPART4_DETAILS:	DrawBoundProductPartDetails(&dc, rcFrame, 3); break;
	case IDC_PRODUCTPART5_DETAILS:	DrawBoundProductPartDetails(&dc, rcFrame, 4); break;
	case IDC_PRODUCTPART6_DETAILS:	DrawBoundProductPartDetails(&dc, rcFrame, 5); break;
	case IDC_PRODUCTPART7_DETAILS:	DrawBoundProductPartDetails(&dc, rcFrame, 6); break;
	case IDC_PRODUCTPART8_DETAILS:	DrawBoundProductPartDetails(&dc, rcFrame, 7); break;
	}
}

void CPPCCustomPaintFrame::DrawBoundProductPartColors(CDC* pDC, CRect rcFrame, int nBoundProductPartIndex)
{
	CPen	pen(PS_SOLID, 1, MARINE);
	CBrush	brush(RGB(250,250,250));
	CPen*	pOldPen	  = pDC->GetCurrentPen();
	CBrush* pOldBrush = pDC->GetCurrentBrush();

	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	pDC->Rectangle(rcFrame);
	brush.DeleteObject();

	CDlgBoundProduct* pDlgBoundProduct = (CDlgBoundProduct*)GetParent();
	if ( ! pDlgBoundProduct)
		return;
	CBoundProduct& product = pDlgBoundProduct->m_boundProduct;
	if ( (nBoundProductPartIndex < 0) || (nBoundProductPartIndex >= product.m_parts.GetSize()) )
		return;
	CBoundProductPart& productPart = product.m_parts[nBoundProductPartIndex];
	if ( ! productPart.m_colorantList.GetSize())
		return;

	CString strText;
	int nXPos = rcFrame.left + 5;
	int nYPos = rcFrame.top	 + 2;
	int nXOffset = 45;

	CFont font;
	font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);

	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	pDC->SetBkMode(TRANSPARENT);
	CString strOut, strOut2;
	strOut2.LoadString(IDS_COLORED);
	strOut.Format(_T("%d-%s"), productPart.m_colorantList.GetSize(), strOut2); 
	pDC->TextOut(nXPos, nYPos, strOut);
	pDC->SetTextColor(GUICOLOR_VALUE);

	nXPos += nXOffset;
	CRect rcIcon;
	rcIcon.left = nXPos; rcIcon.top = nYPos + 4; rcIcon.right = rcIcon.left + 9; rcIcon.bottom = nYPos + 11;
	for (int i = 0; i < productPart.m_colorantList.GetSize(); i++)
	{
		brush.CreateSolidBrush(productPart.m_colorantList[i].m_crRGB);
		pDC->SelectObject(&brush);
		pDC->Rectangle(rcIcon);
		brush.DeleteObject();

		rcIcon.OffsetRect(8, 0);
	}
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();
	font.DeleteObject();
}


void CPPCCustomPaintFrame::DrawBoundProductPartDetails(CDC* pDC, CRect rcFrame, int nBoundProductPartIndex)
{
	CPen	pen(PS_SOLID, 1, MARINE);
	CBrush	brush(RGB(250,250,250));
	CPen*	pOldPen	  = pDC->GetCurrentPen();
	CBrush* pOldBrush = pDC->GetCurrentBrush();

	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	pDC->Rectangle(rcFrame);
	brush.DeleteObject();

	CDlgBoundProduct* pDlgBoundProduct = (CDlgBoundProduct*)GetParent();
	if ( ! pDlgBoundProduct)
		return;
	CBoundProduct& product = pDlgBoundProduct->m_boundProduct;
	if ( (nBoundProductPartIndex < 0) || (nBoundProductPartIndex >= product.m_parts.GetSize()) )
		return;
	CBoundProductPart& productPart = product.m_parts[nBoundProductPartIndex];

	CFont font;
	font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);

	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	pDC->SetBkMode(TRANSPARENT);

	rcFrame.DeflateRect(2,2);

	CString strOut, strOut2;
	if ( ! productPart.m_strPaper.IsEmpty())
		strOut += productPart.m_strPaper + _T("/");
	if ( productPart.m_strFormatName != product.m_strFormatName)
		strOut += productPart.m_strFormatName + _T("/");
	if ( productPart.m_nDesiredQuantity != 0)
	{
		strOut2.Format(_T("%d (%d)"), productPart.m_nDesiredQuantity, productPart.m_nExtra);
		strOut += strOut2 + _T("/");
	}
	//if ( productPart.m_bindingDefs.m_nBinding != product.m_bindingDefs.m_nBinding)
	//{
	//	strOut2 = productPart.m_bindingDefs.m_strName;
	//	strOut += strOut2;
	//}

	strOut.TrimRight('/');
	if ( ! strOut.IsEmpty())
		pDC->DrawText(strOut, rcFrame, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);

	font.DeleteObject();
}



// CDlgBoundProduct-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgBoundProduct, CDialog)

CDlgBoundProduct::CDlgBoundProduct(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgBoundProduct::IDD, pParent)
	, m_strODProductName(_T(""))
{
	m_nODPages = 0;
	m_nODBindingMethod = -1;
	m_fODHeight = 0.0f;
	m_fODWidth = 0.0f;
	m_nODPageOrientation = -1;
	m_nODQuantity = 0;
	m_strNumbering1 = _T("");
	m_strNumbering2 = _T("");
	m_strNumbering3 = _T("");
	m_strNumbering4 = _T("");
	m_strNumbering5 = _T("");
	m_strNumbering6 = _T("");
	m_strNumbering7 = _T("");
	m_strNumbering8 = _T("");
	m_strODBindingComment = _T("");

	m_pDlgNumeration = new CDlgNumeration(this, TRUE);
	m_bkBrush.CreateSolidBrush(GUICOLOR_GROUP);
}

CDlgBoundProduct::~CDlgBoundProduct()
{
	delete m_pDlgNumeration;
	m_bkBrush.DeleteObject();
}

void CDlgBoundProduct::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgBoundProduct)
	DDX_Control(pDX, IDC_OD_QUANTITY_SPIN, m_ODQuantitySpin);
	DDX_Control(pDX, IDC_OD_FORMAT, m_ODFormatCombo);
	DDX_Control(pDX, IDC_OD_WIDTH_SPIN, m_ODWidthSpin);
	DDX_Control(pDX, IDC_OD_HEIGHT_SPIN, m_ODHeightSpin);
	DDX_Text(pDX, IDC_OD_PRODUCTNAME, m_strODProductName);
	DDX_Text(pDX, IDC_OD_NUMPAGES, m_nODPages);
	DDV_MinMaxUInt(pDX, m_nODPages, 0, 9999);
	DDX_CBIndex(pDX, IDC_OD_BINDING, m_nODBindingMethod);
	DDX_Measure(pDX, IDC_OD_HEIGHT, m_fODHeight);
	DDX_Measure(pDX, IDC_OD_WIDTH, m_fODWidth);
	DDX_Radio(pDX, IDC_OD_PORTRAIT, m_nODPageOrientation);							   
	DDX_Text(pDX, IDC_OD_QUANTITY, m_nODQuantity);
	DDX_Text(pDX, IDC_OD_BINDING_COMMENT, m_strODBindingComment);
	DDX_CBString(pDX, IDC_PRODUCTPART1_COMBO, m_strProductPartName1);
	DDX_CBString(pDX, IDC_PRODUCTPART2_COMBO, m_strProductPartName2);
	DDX_CBString(pDX, IDC_PRODUCTPART3_COMBO, m_strProductPartName3);
	DDX_CBString(pDX, IDC_PRODUCTPART4_COMBO, m_strProductPartName4);
	DDX_CBString(pDX, IDC_PRODUCTPART5_COMBO, m_strProductPartName5);
	DDX_CBString(pDX, IDC_PRODUCTPART6_COMBO, m_strProductPartName6);
	DDX_CBString(pDX, IDC_PRODUCTPART7_COMBO, m_strProductPartName7);
	DDX_CBString(pDX, IDC_PRODUCTPART8_COMBO, m_strProductPartName8);
	DDX_Text(pDX, IDC_PRODUCTPART1_NUMPAGES, m_nProductPartNumPages1);
	DDX_Text(pDX, IDC_PRODUCTPART2_NUMPAGES, m_nProductPartNumPages2);
	DDX_Text(pDX, IDC_PRODUCTPART3_NUMPAGES, m_nProductPartNumPages3);
	DDX_Text(pDX, IDC_PRODUCTPART4_NUMPAGES, m_nProductPartNumPages4);
	DDX_Text(pDX, IDC_PRODUCTPART5_NUMPAGES, m_nProductPartNumPages5);
	DDX_Text(pDX, IDC_PRODUCTPART6_NUMPAGES, m_nProductPartNumPages6);
	DDX_Text(pDX, IDC_PRODUCTPART7_NUMPAGES, m_nProductPartNumPages7);
	DDX_Text(pDX, IDC_PRODUCTPART8_NUMPAGES, m_nProductPartNumPages8);
	DDX_Text(pDX, IDC_PRODUCTPART1_NUM_STATIC, m_strNumbering1);
	DDX_Text(pDX, IDC_PRODUCTPART2_NUM_STATIC, m_strNumbering2);
	DDX_Text(pDX, IDC_PRODUCTPART3_NUM_STATIC, m_strNumbering3);
	DDX_Text(pDX, IDC_PRODUCTPART4_NUM_STATIC, m_strNumbering4);
	//DDX_Text(pDX, IDC_PRODUCTPART5_NUM_STATIC, m_strNumbering5);
	//DDX_Text(pDX, IDC_PRODUCTPART6_NUM_STATIC, m_strNumbering6);
	//DDX_Text(pDX, IDC_PRODUCTPART7_NUM_STATIC, m_strNumbering7);
	//DDX_Text(pDX, IDC_PRODUCTPART8_NUM_STATIC, m_strNumbering8);
	m_ODPageFormatSelection.DDX_PageFormatCombo(pDX, IDC_OD_FORMAT, m_strODFormatName);
	//m_bindingDefsSelection.DDX_BindingDefsCombo(pDX, IDC_OD_BINDING, m_strBindingName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgBoundProduct, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_NOTIFY(UDN_DELTAPOS, IDC_OD_HEIGHT_SPIN, OnDeltaposODHeightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_OD_WIDTH_SPIN, OnDeltaposODWidthSpin)
	ON_EN_KILLFOCUS(IDC_OD_HEIGHT, &CDlgBoundProduct::OnEnKillfocusOdHeight)
	ON_EN_KILLFOCUS(IDC_OD_WIDTH,  &CDlgBoundProduct::OnEnKillfocusOdWidth)
	ON_BN_CLICKED(IDC_OD_LANDSCAPE, OnODLandscape)
	ON_BN_CLICKED(IDC_OD_PORTRAIT, OnODPortrait)
	ON_CBN_SELCHANGE(IDC_OD_FORMAT, OnSelchangeODFormat)
	ON_CBN_SELCHANGE(IDC_OD_BINDING, OnSelchangeODBinding)
	ON_NOTIFY(UDN_DELTAPOS, IDC_OD_QUANTITY_SPIN, OnDeltaposODQuantitySpin)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART1_COMBO, &CDlgBoundProduct::OnCbnSelchangeProductpart1Combo)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART2_COMBO, &CDlgBoundProduct::OnCbnSelchangeProductpart2Combo)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART3_COMBO, &CDlgBoundProduct::OnCbnSelchangeProductpart3Combo)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART4_COMBO, &CDlgBoundProduct::OnCbnSelchangeProductpart4Combo)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART5_COMBO, &CDlgBoundProduct::OnCbnSelchangeProductpart5Combo)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART6_COMBO, &CDlgBoundProduct::OnCbnSelchangeProductpart6Combo)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART7_COMBO, &CDlgBoundProduct::OnCbnSelchangeProductpart7Combo)
	ON_CBN_SELCHANGE(IDC_PRODUCTPART8_COMBO, &CDlgBoundProduct::OnCbnSelchangeProductpart8Combo)
	ON_CBN_KILLFOCUS(IDC_PRODUCTPART1_COMBO, OnKillfocusProductpart1Combo)
	ON_CBN_KILLFOCUS(IDC_PRODUCTPART2_COMBO, OnKillfocusProductpart2Combo)
	ON_CBN_KILLFOCUS(IDC_PRODUCTPART3_COMBO, OnKillfocusProductpart3Combo)
	ON_CBN_KILLFOCUS(IDC_PRODUCTPART4_COMBO, OnKillfocusProductpart4Combo)
	ON_CBN_KILLFOCUS(IDC_PRODUCTPART5_COMBO, OnKillfocusProductpart5Combo)
	ON_CBN_KILLFOCUS(IDC_PRODUCTPART6_COMBO, OnKillfocusProductpart6Combo)
	ON_CBN_KILLFOCUS(IDC_PRODUCTPART7_COMBO, OnKillfocusProductpart7Combo)
	ON_CBN_KILLFOCUS(IDC_PRODUCTPART8_COMBO, OnKillfocusProductpart8Combo)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART1_NUMPAGES_SPIN, OnDeltaposProductpart1NumPagesSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART2_NUMPAGES_SPIN, OnDeltaposProductpart2NumPagesSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART3_NUMPAGES_SPIN, OnDeltaposProductpart3NumPagesSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART4_NUMPAGES_SPIN, OnDeltaposProductpart4NumPagesSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART5_NUMPAGES_SPIN, OnDeltaposProductpart5NumPagesSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART6_NUMPAGES_SPIN, OnDeltaposProductpart6NumPagesSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART7_NUMPAGES_SPIN, OnDeltaposProductpart7NumPagesSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PRODUCTPART8_NUMPAGES_SPIN, OnDeltaposProductpart8NumPagesSpin)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART1_NUMPAGES, OnKillfocusNumPages1)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART2_NUMPAGES, OnKillfocusNumPages2)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART3_NUMPAGES, OnKillfocusNumPages3)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART4_NUMPAGES, OnKillfocusNumPages4)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART5_NUMPAGES, OnKillfocusNumPages5)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART6_NUMPAGES, OnKillfocusNumPages6)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART7_NUMPAGES, OnKillfocusNumPages7)
	ON_EN_KILLFOCUS(IDC_PRODUCTPART8_NUMPAGES, OnKillfocusNumPages8)
	ON_EN_CHANGE(IDC_PRODUCTPART1_NUMPAGES, &CDlgBoundProduct::OnEnChangeProductpart1Numpages)
	ON_EN_CHANGE(IDC_PRODUCTPART2_NUMPAGES, &CDlgBoundProduct::OnEnChangeProductpart2Numpages)
	ON_EN_CHANGE(IDC_PRODUCTPART3_NUMPAGES, &CDlgBoundProduct::OnEnChangeProductpart3Numpages)
	ON_EN_CHANGE(IDC_PRODUCTPART4_NUMPAGES, &CDlgBoundProduct::OnEnChangeProductpart4Numpages)
	ON_EN_CHANGE(IDC_PRODUCTPART5_NUMPAGES, &CDlgBoundProduct::OnEnChangeProductpart5Numpages)
	ON_EN_CHANGE(IDC_PRODUCTPART6_NUMPAGES, &CDlgBoundProduct::OnEnChangeProductpart6Numpages)
	ON_EN_CHANGE(IDC_PRODUCTPART7_NUMPAGES, &CDlgBoundProduct::OnEnChangeProductpart7Numpages)
	ON_EN_CHANGE(IDC_PRODUCTPART8_NUMPAGES, &CDlgBoundProduct::OnEnChangeProductpart8Numpages)
	ON_BN_CLICKED(IDC_PRODUCTPART1_NUM_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart1NumButton)
	ON_BN_CLICKED(IDC_PRODUCTPART2_NUM_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart2NumButton)
	ON_BN_CLICKED(IDC_PRODUCTPART3_NUM_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart3NumButton)
	ON_BN_CLICKED(IDC_PRODUCTPART4_NUM_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart4NumButton)
	ON_BN_CLICKED(IDC_PRODUCTPART5_NUM_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart5NumButton)
	ON_BN_CLICKED(IDC_PRODUCTPART6_NUM_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart6NumButton)
	ON_BN_CLICKED(IDC_PRODUCTPART7_NUM_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart7NumButton)
	ON_BN_CLICKED(IDC_PRODUCTPART8_NUM_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart8NumButton)
	ON_BN_CLICKED(IDC_OD_MODIFY_FORMAT, &CDlgBoundProduct::OnBnClickedOdModifyFormat)
	ON_BN_CLICKED(IDC_OD_DEFINE_BINDING, &CDlgBoundProduct::OnBnClickedOdDefineBinding)
	ON_BN_CLICKED(IDC_PRODUCTPART1_DETAILS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart1DetailsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART2_DETAILS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart2DetailsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART3_DETAILS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart3DetailsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART4_DETAILS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart4DetailsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART5_DETAILS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart5DetailsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART6_DETAILS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart6DetailsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART7_DETAILS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart7DetailsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART8_DETAILS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart8DetailsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART1_REMOVE, &CDlgBoundProduct::OnBnClickedProductpart1Remove)
	ON_BN_CLICKED(IDC_PRODUCTPART2_REMOVE, &CDlgBoundProduct::OnBnClickedProductpart2Remove)
	ON_BN_CLICKED(IDC_PRODUCTPART3_REMOVE, &CDlgBoundProduct::OnBnClickedProductpart3Remove)
	ON_BN_CLICKED(IDC_PRODUCTPART4_REMOVE, &CDlgBoundProduct::OnBnClickedProductpart4Remove)
	ON_BN_CLICKED(IDC_PRODUCTPART5_REMOVE, &CDlgBoundProduct::OnBnClickedProductpart5Remove)
	ON_BN_CLICKED(IDC_PRODUCTPART6_REMOVE, &CDlgBoundProduct::OnBnClickedProductpart6Remove)
	ON_BN_CLICKED(IDC_PRODUCTPART7_REMOVE, &CDlgBoundProduct::OnBnClickedProductpart7Remove)
	ON_BN_CLICKED(IDC_PRODUCTPART8_REMOVE, &CDlgBoundProduct::OnBnClickedProductpart8Remove)
	ON_BN_CLICKED(IDC_PRODUCTPART1_COLORS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart1ColorsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART2_COLORS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart2ColorsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART3_COLORS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart3ColorsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART4_COLORS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart4ColorsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART5_COLORS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart5ColorsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART6_COLORS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart6ColorsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART7_COLORS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart7ColorsButton)
	ON_BN_CLICKED(IDC_PRODUCTPART8_COLORS_BUTTON, &CDlgBoundProduct::OnBnClickedProductpart8ColorsButton)
	ON_BN_CLICKED(IDOK, &CDlgBoundProduct::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgBoundProduct-Meldungshandler

BOOL CDlgBoundProduct::OnEraseBkgnd(CDC* pDC)
{
	return CDialog::OnEraseBkgnd(pDC);
//	return TRUE;
}

BOOL CDlgBoundProduct::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_productPart1Colors.SubclassDlgItem(IDC_PRODUCTPART1_COLORS, this);
	m_productPart2Colors.SubclassDlgItem(IDC_PRODUCTPART2_COLORS, this);
	m_productPart3Colors.SubclassDlgItem(IDC_PRODUCTPART3_COLORS, this);
	m_productPart4Colors.SubclassDlgItem(IDC_PRODUCTPART4_COLORS, this);
	m_productPart5Colors.SubclassDlgItem(IDC_PRODUCTPART5_COLORS, this);
	m_productPart6Colors.SubclassDlgItem(IDC_PRODUCTPART6_COLORS, this);
	m_productPart7Colors.SubclassDlgItem(IDC_PRODUCTPART7_COLORS, this);
	m_productPart8Colors.SubclassDlgItem(IDC_PRODUCTPART8_COLORS, this);
	m_productPart1Details.SubclassDlgItem(IDC_PRODUCTPART1_DETAILS, this);
	m_productPart2Details.SubclassDlgItem(IDC_PRODUCTPART2_DETAILS, this);
	m_productPart3Details.SubclassDlgItem(IDC_PRODUCTPART3_DETAILS, this);
	m_productPart4Details.SubclassDlgItem(IDC_PRODUCTPART4_DETAILS, this);
	m_productPart5Details.SubclassDlgItem(IDC_PRODUCTPART5_DETAILS, this);
	m_productPart6Details.SubclassDlgItem(IDC_PRODUCTPART6_DETAILS, this);
	m_productPart7Details.SubclassDlgItem(IDC_PRODUCTPART7_DETAILS, this);
	m_productPart8Details.SubclassDlgItem(IDC_PRODUCTPART8_DETAILS, this);

	m_ODWidthSpin.SetRange(0, 1);  
	m_ODHeightSpin.SetRange(0, 1);  
	m_ODQuantitySpin.SetRange(0, 9999);	

// binding method combo ////////////////////////////
	CComboBoxEx* pBox = (CComboBoxEx*)GetDlgItem (IDC_OD_BINDING);
	CBitmap bitmap;
	m_bindingImageList.Create(24, 12, ILC_COLOR8, 2, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_PERFECT_BOUND);
	m_bindingImageList.Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	bitmap.LoadBitmap(IDB_SADDLE_STITCHED);
	m_bindingImageList.Add(&bitmap, BLACK);
	pBox->SetImageList(&m_bindingImageList);
	bitmap.DeleteObject();

	((CSpinButtonCtrl*)GetDlgItem(IDC_PRODUCTPART1_NUMPAGES_SPIN))->SetRange(2, 9999);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PRODUCTPART2_NUMPAGES_SPIN))->SetRange(2, 9999);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PRODUCTPART3_NUMPAGES_SPIN))->SetRange(2, 9999);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PRODUCTPART4_NUMPAGES_SPIN))->SetRange(2, 9999);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PRODUCTPART5_NUMPAGES_SPIN))->SetRange(2, 9999);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PRODUCTPART6_NUMPAGES_SPIN))->SetRange(2, 9999);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PRODUCTPART7_NUMPAGES_SPIN))->SetRange(2, 9999);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PRODUCTPART8_NUMPAGES_SPIN))->SetRange(2, 9999);

	m_strProductPartName1	= m_strProductPartName2		= m_strProductPartName3		= m_strProductPartName4		= _T("");
	m_strProductPartName5	= m_strProductPartName6		= m_strProductPartName7		= m_strProductPartName8		= _T("");
	m_nProductPartNumPages1	= m_nProductPartNumPages2	= m_nProductPartNumPages3	= m_nProductPartNumPages4	= 0;
	m_nProductPartNumPages5	= m_nProductPartNumPages6	= m_nProductPartNumPages7	= m_nProductPartNumPages8	= 0;

	InitData();

	KeepCurrentPageFormat();

	GetDlgItem(IDC_OD_PRODUCTNAME)->SetFocus();
	((CEdit*)GetDlgItem(IDC_OD_PRODUCTNAME))->SetSel(0, -1);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}
void CDlgBoundProduct::InitData()
{
	m_strODProductName		= m_boundProduct.m_strProductName;
	m_nODPages			 	= m_boundProduct.GetNumPages();		
	m_fODWidth			 	= m_boundProduct.m_fTrimSizeWidth;
	m_fODHeight			 	= m_boundProduct.m_fTrimSizeHeight;				
	m_strODFormatName	 	= m_boundProduct.m_strFormatName;
	m_nODPageOrientation 	= m_boundProduct.m_nPageOrientation;
	m_nODBindingMethod	 	= m_bindingDefsSelection.GetBindingDefsIndex(m_boundProduct.m_bindingParams.m_bindingDefs);
	m_nODQuantity		 	= m_boundProduct.GetDesiredQuantity();
	m_strODBindingComment	= m_boundProduct.m_strBindingComment;

// binding method combo
	m_bindingDefsSelection.InitComboBox(this, m_strBindingName, &m_nODBindingMethod, IDC_OD_BINDING);
	CComboBoxEx* pBox = (CComboBoxEx*)GetDlgItem (IDC_OD_BINDING);
	pBox->SetCurSel(m_nODBindingMethod);
/////////////////////////////////////////////////

	UpdateData(FALSE);

	m_ODPageFormatSelection.InitComboBox(this, m_strODFormatName, &m_fODWidth, &m_fODHeight, IDC_OD_FORMAT);
	if (m_strODFormatName.IsEmpty())
	{
		CPageFormat* pFormat = m_ODPageFormatSelection.FindBySize(m_fODWidth, m_fODHeight);
		if (pFormat)
		{
			m_strODFormatName =  pFormat->m_szName;
			if ( (fabs(pFormat->m_fWidth - m_fODHeight) < 0.01) && (fabs(pFormat->m_fHeight - m_fODWidth) < 0.01) )		// rotated
				m_nODPageOrientation = (m_nODPageOrientation == PORTRAIT) ? LANDSCAPE : PORTRAIT;
			m_ODPageFormatSelection.InitComboBox(this, m_strODFormatName, &m_fODWidth, &m_fODHeight, IDC_OD_FORMAT);
		}
	}
	if (m_strODFormatName.IsEmpty())
	{
		if (m_ODFormatCombo.GetCount())
		{
			m_ODFormatCombo.SetCurSel(0);
			UpdateData(TRUE);
			m_ODPageFormatSelection.SelchangeFormatList(this, &m_fODWidth, &m_fODHeight, m_strODFormatName, &m_nODPageOrientation, IDC_OD_FORMAT);
		}
	}
	else
		if ( (m_fODWidth == 0.0f) || (m_fODHeight == 0.0f) )
			m_ODPageFormatSelection.SelchangeFormatList(this, &m_fODWidth, &m_fODHeight, m_strODFormatName, &m_nODPageOrientation, IDC_OD_FORMAT);

	CString strText;
	if (m_nODQuantity)
		strText.Format(_T("%d"), m_nODQuantity);
	GetDlgItem(IDC_OD_QUANTITY)->SetWindowText(strText);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (m_nODPages <= 0)	
	{
		m_nODPages = 0;
	}

	for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
	{
		switch (i)
		{
		case 0:	m_strProductPartName1	= m_boundProduct.m_parts[i].m_strName;
				m_nProductPartNumPages1	= m_boundProduct.m_parts[i].m_nNumPages;
				break;
		case 1:	m_strProductPartName2	= m_boundProduct.m_parts[i].m_strName;
				m_nProductPartNumPages2	= m_boundProduct.m_parts[i].m_nNumPages;
				break;
		case 2:	m_strProductPartName3	= m_boundProduct.m_parts[i].m_strName;
				m_nProductPartNumPages3	= m_boundProduct.m_parts[i].m_nNumPages;
				break;
		case 3:	m_strProductPartName4	= m_boundProduct.m_parts[i].m_strName;
				m_nProductPartNumPages4	= m_boundProduct.m_parts[i].m_nNumPages;
				break;
		case 4:	m_strProductPartName5	= m_boundProduct.m_parts[i].m_strName;
				m_nProductPartNumPages5	= m_boundProduct.m_parts[i].m_nNumPages;
				break;
		case 5:	m_strProductPartName6	= m_boundProduct.m_parts[i].m_strName;
				m_nProductPartNumPages6	= m_boundProduct.m_parts[i].m_nNumPages;
				break;
		case 6:	m_strProductPartName7	= m_boundProduct.m_parts[i].m_strName;
				m_nProductPartNumPages7	= m_boundProduct.m_parts[i].m_nNumPages;
				break;
		case 7:	m_strProductPartName8	= m_boundProduct.m_parts[i].m_strName;
				m_nProductPartNumPages8	= m_boundProduct.m_parts[i].m_nNumPages;
				break;
		}
		InitNumberingString(i);
	}

	UpdateData(FALSE);
}

void CDlgBoundProduct::SaveData()
{
	if ( ! m_hWnd)
		return;

	UpdateData(TRUE);

	m_boundProduct.m_strProductName = m_strODProductName;

	m_boundProduct.m_strFormatName		= m_strODFormatName;
	m_boundProduct.m_fTrimSizeWidth		= m_fODWidth;	
	m_boundProduct.m_fTrimSizeHeight	= m_fODHeight;					
	m_boundProduct.m_nPageOrientation	= m_nODPageOrientation;

	m_boundProduct.SetNumPages(m_nODPages);
	m_boundProduct.m_bindingParams.m_bindingDefs = m_bindingDefsSelection.GetBindingDefs(m_nODBindingMethod);
	m_boundProduct.SetDesiredQuantity(m_nODQuantity);
	m_boundProduct.m_strBindingComment = m_strODBindingComment;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	long ret = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	_tcsncpy(theApp.settings.m_szLastPageFormat, m_boundProduct.m_strFormatName.GetBuffer(MAX_TEXT), MAX_TEXT);
	RegSetValueEx(hKey, _T("LastPageFormat"), 0, REG_SZ, (LPBYTE)theApp.settings.m_szLastPageFormat, (_tcslen(theApp.settings.m_szLastPageFormat) + 1) * sizeof(TCHAR));

	_tcsncpy(theApp.settings.m_szLastBindingStyle, m_boundProduct.m_bindingParams.m_bindingDefs.m_strName.GetBuffer(MAX_TEXT), MAX_TEXT);
	RegSetValueEx(hKey, _T("LastBindingStyle"), 0, REG_SZ, (LPBYTE)theApp.settings.m_szLastBindingStyle, (_tcslen(theApp.settings.m_szLastBindingStyle) + 1) * sizeof(TCHAR));
	ret = RegCloseKey(hKey);

	CBoundProductPart productPart;
	productPart.m_strName			= _T("");
	productPart.m_nNumPages			= m_nODPages;
	productPart.m_strFormatName		= m_strODFormatName;
	productPart.m_fPageWidth		= m_fODWidth;
	productPart.m_fPageHeight		= m_fODHeight;
	//productPart.m_bindingDefs		= m_bindingDefsSelection.GetBindingDefs(m_nODBindingMethod);
	productPart.SetDesiredQuantity(m_boundProduct.GetDesiredQuantity());
	productPart.m_nExtra			= 0;

	int nProductPartIndex = -1;
	for (int i = 0; i < 8; i++)
	{
		switch (i)
		{
		case 0: productPart.m_strName = m_strProductPartName1; productPart.m_nNumPages = m_nProductPartNumPages1;	break;
		case 1: productPart.m_strName = m_strProductPartName2; productPart.m_nNumPages = m_nProductPartNumPages2;	break;
		case 2: productPart.m_strName = m_strProductPartName3; productPart.m_nNumPages = m_nProductPartNumPages3;	break;
		case 3: productPart.m_strName = m_strProductPartName4; productPart.m_nNumPages = m_nProductPartNumPages4;	break;
		case 4: productPart.m_strName = m_strProductPartName5; productPart.m_nNumPages = m_nProductPartNumPages5;	break;
		case 5: productPart.m_strName = m_strProductPartName6; productPart.m_nNumPages = m_nProductPartNumPages6;	break;
		case 6: productPart.m_strName = m_strProductPartName7; productPart.m_nNumPages = m_nProductPartNumPages7;	break;
		case 7: productPart.m_strName = m_strProductPartName8; productPart.m_nNumPages = m_nProductPartNumPages8;	break;
		}
		if (productPart.m_nNumPages <= 0)
			continue;

		if (productPart.m_strName.IsEmpty())
			productPart.m_strName.LoadStringA(IDS_PARTPRODUCT);

		nProductPartIndex++;

		BOOL bOverWrite = (nProductPartIndex < m_boundProduct.m_parts.GetSize()) ? TRUE : FALSE;
		if (bOverWrite)
		{
			m_boundProduct.m_parts[nProductPartIndex].m_strName	  = productPart.m_strName;
			m_boundProduct.m_parts[nProductPartIndex].m_nNumPages = productPart.m_nNumPages;
		}
		else
		{
			CNumbering numbering;
			numbering.m_nRangeFrom			= 1;	
			numbering.m_nRangeTo			= productPart.m_nNumPages;
			numbering.m_nStartNum			= 1;
			numbering.m_strNumberingStyle	= _T("$n");
			numbering.m_bReverse			= FALSE;
			productPart.m_numberingList.RemoveAll();
			productPart.m_numberingList.Add(numbering);
			if (nProductPartIndex > 0)
				if ( ! productPart.m_colorantList.GetSize())
					productPart.m_colorantList.Append(m_boundProduct.m_parts[nProductPartIndex - 1].m_colorantList);

			m_boundProduct.m_parts.InsertAt(nProductPartIndex, productPart);

			UpdateColors(nProductPartIndex);
			UpdateDetails(nProductPartIndex);
		}
	}
}

BOOL CDlgBoundProduct::CheckData()
{
	if (m_boundProduct.m_strProductName.IsEmpty())	// todo:
	{
	}

	for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
	{
		if ( (fabs(m_boundProduct.m_parts[i].m_fPageWidth) < 0.1f) || (fabs(m_boundProduct.m_parts[i].m_fPageHeight) < 0.1f) )
			if (AfxMessageBox(IDS_INVALID_PAGEFORMAT, MB_YESNO) == IDNO)
				return IDCANCEL;
	}

	return IDOK;
}

void CDlgBoundProduct::KeepCurrentPageFormat()
{
	m_strOldFormatName	  = m_strODFormatName;
	m_fOldWidth			  = m_fODWidth;
	m_fOldHeight		  = m_fODHeight;
	m_nOldPageOrientation = m_nODPageOrientation;
}

BOOL CDlgBoundProduct::PageFormatChanged()
{
	if ( (m_strOldFormatName != m_strODFormatName) || (fabs(m_fOldWidth - m_fODWidth) > 0.001) || (fabs(m_fOldHeight - m_fODHeight) > 0.001) || (m_nOldPageOrientation != m_nODPageOrientation) )
		return TRUE;
	else
		return FALSE;
}

void CDlgBoundProduct::OnSelchangeODFormat() 
{
	UpdateData(TRUE);

	m_ODPageFormatSelection.SelchangeFormatList(this, &m_fODWidth, &m_fODHeight, m_strODFormatName, &m_nODPageOrientation, IDC_OD_FORMAT);

	for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
	{
		m_boundProduct.m_parts[i].m_strFormatName	 = m_strODFormatName;
		m_boundProduct.m_parts[i].m_fPageWidth		 = m_fODWidth;
		m_boundProduct.m_parts[i].m_fPageHeight		 = m_fODHeight;
		m_boundProduct.m_parts[i].m_nPageOrientation = m_nODPageOrientation;
	}
	UpdateDetails(-1);
}

void CDlgBoundProduct::OnDeltaposODHeightSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_ODHeightSpin.GetBuddy(), pNMUpDown->iDelta);

	UpdateData(TRUE);
	m_strODFormatName.LoadString(IDS_USERDEFINED);
	m_ODFormatCombo.SelectString(-1, m_strODFormatName);

	for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
	{
		m_boundProduct.m_parts[i].m_strFormatName	= m_strODFormatName;
		m_boundProduct.m_parts[i].m_fPageHeight		= m_fODHeight;
	}
	UpdateDetails(-1);
}

void CDlgBoundProduct::OnDeltaposODWidthSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_ODWidthSpin.GetBuddy(), pNMUpDown->iDelta);

	UpdateData(TRUE);
	m_strODFormatName.LoadString(IDS_USERDEFINED);
	m_ODFormatCombo.SelectString(-1, m_strODFormatName);

	for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
	{
		m_boundProduct.m_parts[i].m_strFormatName	= m_strODFormatName;
		m_boundProduct.m_parts[i].m_fPageWidth		= m_fODWidth;
	}
	UpdateDetails(-1);
}

void CDlgBoundProduct::OnEnKillfocusOdHeight()
{
	float fOldValue = m_fODHeight;
	UpdateData(TRUE);
	if (fOldValue != m_fODHeight)
	{
		m_strODFormatName.LoadString(IDS_USERDEFINED);
		m_ODFormatCombo.SelectString(-1, m_strODFormatName);

		for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
		{
			m_boundProduct.m_parts[i].m_strFormatName	= m_strODFormatName;
			m_boundProduct.m_parts[i].m_fPageHeight		= m_fODHeight;
		}
		UpdateDetails(-1);
	}
}

void CDlgBoundProduct::OnEnKillfocusOdWidth()
{
	float fOldValue = m_fODWidth;
	UpdateData(TRUE);
	if (fOldValue != m_fODWidth)
	{
		m_strODFormatName.LoadString(IDS_USERDEFINED);
		m_ODFormatCombo.SelectString(-1, m_strODFormatName);

		for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
		{
			m_boundProduct.m_parts[i].m_strFormatName	= m_strODFormatName;
			m_boundProduct.m_parts[i].m_fPageWidth		= m_fODWidth;
		}
		UpdateDetails(-1);
	}
}

void CDlgBoundProduct::OnODPortrait() 
{
	if (m_nODPageOrientation == PORTRAIT)	// already portrait
		return;

	m_nODPageOrientation = PORTRAIT;		// switch to portrait

	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_OD_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_OD_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);

	UpdateData(TRUE);

	for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
	{
		m_boundProduct.m_parts[i].m_nPageOrientation	= m_nODPageOrientation;
		m_boundProduct.m_parts[i].m_fPageWidth			= m_fODWidth;
		m_boundProduct.m_parts[i].m_fPageHeight			= m_fODHeight;
	}
	UpdateDetails(-1);
}

void CDlgBoundProduct::OnODLandscape() 
{
	if (m_nODPageOrientation == LANDSCAPE)// already landscape
		return;
	
	m_nODPageOrientation = LANDSCAPE;		// switch to portrait

	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_OD_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_OD_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);

	UpdateData(TRUE);

	for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
	{
		m_boundProduct.m_parts[i].m_nPageOrientation	= m_nODPageOrientation;
		m_boundProduct.m_parts[i].m_fPageWidth			= m_fODWidth;
		m_boundProduct.m_parts[i].m_fPageHeight			= m_fODHeight;
	}
	UpdateDetails(-1);
}

void CDlgBoundProduct::OnSelchangeODBinding() 
{
	UpdateData(TRUE);

	if (m_boundProduct.m_parts.GetSize() > 1)
		if (AfxMessageBox(IDS_CHANGE_PRODUCTPART_BINDING, MB_YESNO) == IDNO)
			return;

	m_boundProduct.m_bindingParams.m_bindingDefs = m_bindingDefsSelection.GetBindingDefs(m_nODBindingMethod);

	//for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
	//{
	//	m_boundProduct.m_parts[i].m_bindingDefs = m_bindingDefsSelection.GetBindingDefs(m_nODBindingMethod);
	//}

	m_strBindingName = m_bindingDefsSelection.GetBindingDefs(m_nODBindingMethod).m_strName;
}

void CDlgBoundProduct::OnDeltaposODQuantitySpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	m_nODQuantity += pNMUpDown->iDelta * 1000;
	if (m_nODQuantity < 0)
		m_nODQuantity = 0;
	CString strText;
	if (m_nODQuantity)
		strText.Format(_T("%d"), m_nODQuantity);
	GetDlgItem(IDC_OD_QUANTITY)->SetWindowText(strText);
}

void CDlgBoundProduct::OnBnClickedOdModifyFormat()
{
	UpdateData(TRUE);

	CDlgPageFormats dlg;
	dlg.m_strFormatName = m_strODFormatName;
	dlg.m_fWidth		= m_fODWidth;
	dlg.m_fHeight		= m_fODHeight;
	dlg.DoModal();

	m_strODFormatName = dlg.m_strFormatName;
	m_ODPageFormatSelection.InitComboBox(this, m_strODFormatName, &m_fODWidth, &m_fODHeight, IDC_OD_FORMAT);
	OnSelchangeODFormat();
}

void CDlgBoundProduct::OnBnClickedOdDefineBinding()
{
	UpdateData(TRUE);

	CDlgBindingDefs dlg;
	dlg.m_strBindingName = m_strBindingName;
	dlg.DoModal();
	m_strBindingName = dlg.m_strBindingName;

	m_bindingDefsSelection.InitComboBox(this, m_strBindingName, &m_nODBindingMethod, IDC_OD_BINDING);
	CComboBoxEx* pBox = (CComboBoxEx*)GetDlgItem (IDC_OD_BINDING);
	pBox->FindStringExact(0, m_strBindingName);
	m_nODBindingMethod = pBox->GetCurSel();
	OnSelchangeODBinding();
}

void CDlgBoundProduct::OnCbnSelchangeProductpart1Combo(){ ChangeProductPartName(0); }
void CDlgBoundProduct::OnCbnSelchangeProductpart2Combo(){ ChangeProductPartName(1); }
void CDlgBoundProduct::OnCbnSelchangeProductpart3Combo(){ ChangeProductPartName(2); }
void CDlgBoundProduct::OnCbnSelchangeProductpart4Combo(){ ChangeProductPartName(3); }
void CDlgBoundProduct::OnCbnSelchangeProductpart5Combo(){ ChangeProductPartName(4); }
void CDlgBoundProduct::OnCbnSelchangeProductpart6Combo(){ ChangeProductPartName(5); }
void CDlgBoundProduct::OnCbnSelchangeProductpart7Combo(){ ChangeProductPartName(6); }
void CDlgBoundProduct::OnCbnSelchangeProductpart8Combo(){ ChangeProductPartName(7); }

void CDlgBoundProduct::ChangeProductPartName(int nIndex)
{
	UpdateData(TRUE);

	CString* pstrName	= &m_strProductPartName1;
	int*	 pnNumPages	= &m_nProductPartNumPages1;
	int		 nID		= IDC_PRODUCTPART1_COMBO;
	switch (nIndex)
	{
	case 0: nID = IDC_PRODUCTPART1_COMBO; pstrName = &m_strProductPartName1; pnNumPages = &m_nProductPartNumPages1;	break;
	case 1: nID = IDC_PRODUCTPART2_COMBO; pstrName = &m_strProductPartName2; pnNumPages = &m_nProductPartNumPages2;	break;
	case 2: nID = IDC_PRODUCTPART3_COMBO; pstrName = &m_strProductPartName3; pnNumPages = &m_nProductPartNumPages3;	break;
	case 3: nID = IDC_PRODUCTPART4_COMBO; pstrName = &m_strProductPartName4; pnNumPages = &m_nProductPartNumPages4;	break;
	case 4: nID = IDC_PRODUCTPART5_COMBO; pstrName = &m_strProductPartName5; pnNumPages = &m_nProductPartNumPages5;	break;
	case 5: nID = IDC_PRODUCTPART6_COMBO; pstrName = &m_strProductPartName6; pnNumPages = &m_nProductPartNumPages6;	break;
	case 6: nID = IDC_PRODUCTPART7_COMBO; pstrName = &m_strProductPartName7; pnNumPages = &m_nProductPartNumPages7;	break;
	case 7: nID = IDC_PRODUCTPART8_COMBO; pstrName = &m_strProductPartName8; pnNumPages = &m_nProductPartNumPages8;	break;
	}

	int nSel = ((CComboBox*)GetDlgItem(nID))->GetCurSel();
	if (nSel == CB_ERR)
		return;

	((CComboBox*)GetDlgItem(nID))->GetLBText(nSel, *pstrName);

	if (pstrName->CompareNoCase(theApp.m_strCoverName) == 0)
	{
		*pnNumPages = 4;
		UpdateData(FALSE);
		ChangeProductPartNumPages(0, 0);
		SaveData(); InitData();
	}

	if ( (nIndex >= 0) && (nIndex < m_boundProduct.m_parts.GetSize()) )
		m_boundProduct.m_parts[nIndex].m_strName = *pstrName;
}

void CDlgBoundProduct::ChangeProductPartNumPages(int nIndex, int nDelta)
{
	int		nID			= IDC_PRODUCTPART1_NUMPAGES_SPIN;
	int*	pnNumPages	= &m_nProductPartNumPages1;
	switch (nIndex)
	{
	case 0: nID = IDC_PRODUCTPART1_NUMPAGES_SPIN; pnNumPages = &m_nProductPartNumPages1;	break;
	case 1: nID = IDC_PRODUCTPART2_NUMPAGES_SPIN; pnNumPages = &m_nProductPartNumPages2;	break;
	case 2: nID = IDC_PRODUCTPART3_NUMPAGES_SPIN; pnNumPages = &m_nProductPartNumPages3;	break;
	case 3: nID = IDC_PRODUCTPART4_NUMPAGES_SPIN; pnNumPages = &m_nProductPartNumPages4;	break;
	case 4: nID = IDC_PRODUCTPART5_NUMPAGES_SPIN; pnNumPages = &m_nProductPartNumPages5;	break;
	case 5: nID = IDC_PRODUCTPART6_NUMPAGES_SPIN; pnNumPages = &m_nProductPartNumPages6;	break;
	case 6: nID = IDC_PRODUCTPART7_NUMPAGES_SPIN; pnNumPages = &m_nProductPartNumPages7;	break;
	case 7: nID = IDC_PRODUCTPART8_NUMPAGES_SPIN; pnNumPages = &m_nProductPartNumPages8;	break;
	}

	if (nDelta != 0)
	{
		if (nDelta < 0)
		{
			if ( (*pnNumPages) > 2)
				(*pnNumPages)--;
		}
		else
			(*pnNumPages)++;
	}

	m_nODPages = m_nProductPartNumPages1 + m_nProductPartNumPages2 + m_nProductPartNumPages3 + m_nProductPartNumPages4 + m_nProductPartNumPages5 + m_nProductPartNumPages6 + m_nProductPartNumPages7 + m_nProductPartNumPages8;

	UpdateData(FALSE);
}

void CDlgBoundProduct::OnDeltaposProductpart1NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) { ChangeProductPartNumPages(0, ((NM_UPDOWN*)pNMHDR)->iDelta); }
void CDlgBoundProduct::OnDeltaposProductpart2NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) { ChangeProductPartNumPages(1, ((NM_UPDOWN*)pNMHDR)->iDelta); }
void CDlgBoundProduct::OnDeltaposProductpart3NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) { ChangeProductPartNumPages(2, ((NM_UPDOWN*)pNMHDR)->iDelta); }
void CDlgBoundProduct::OnDeltaposProductpart4NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) { ChangeProductPartNumPages(3, ((NM_UPDOWN*)pNMHDR)->iDelta); }
void CDlgBoundProduct::OnDeltaposProductpart5NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) { ChangeProductPartNumPages(4, ((NM_UPDOWN*)pNMHDR)->iDelta); }
void CDlgBoundProduct::OnDeltaposProductpart6NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) { ChangeProductPartNumPages(5, ((NM_UPDOWN*)pNMHDR)->iDelta); }
void CDlgBoundProduct::OnDeltaposProductpart7NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) { ChangeProductPartNumPages(6, ((NM_UPDOWN*)pNMHDR)->iDelta); }
void CDlgBoundProduct::OnDeltaposProductpart8NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult) { ChangeProductPartNumPages(7, ((NM_UPDOWN*)pNMHDR)->iDelta); }

void CDlgBoundProduct::OnKillfocusProductpart1Combo() { UpdateData(TRUE); ChangeProductPartName(0); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusProductpart2Combo() { UpdateData(TRUE); ChangeProductPartName(1); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusProductpart3Combo() { UpdateData(TRUE); ChangeProductPartName(2); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusProductpart4Combo() { UpdateData(TRUE); ChangeProductPartName(3); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusProductpart5Combo() { UpdateData(TRUE); ChangeProductPartName(4); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusProductpart6Combo() { UpdateData(TRUE); ChangeProductPartName(5); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusProductpart7Combo() { UpdateData(TRUE); ChangeProductPartName(6); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusProductpart8Combo() { UpdateData(TRUE); ChangeProductPartName(7); SaveData(); InitData(); }

void CDlgBoundProduct::OnKillfocusNumPages1() { UpdateData(TRUE); ChangeProductPartNumPages(0, 0); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusNumPages2() { UpdateData(TRUE); ChangeProductPartNumPages(1, 0); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusNumPages3() { UpdateData(TRUE); ChangeProductPartNumPages(2, 0); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusNumPages4() { UpdateData(TRUE); ChangeProductPartNumPages(3, 0); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusNumPages5() { UpdateData(TRUE); ChangeProductPartNumPages(4, 0); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusNumPages6() { UpdateData(TRUE); ChangeProductPartNumPages(5, 0); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusNumPages7() { UpdateData(TRUE); ChangeProductPartNumPages(6, 0); SaveData(); InitData(); }
void CDlgBoundProduct::OnKillfocusNumPages8() { UpdateData(TRUE); ChangeProductPartNumPages(7, 0); SaveData(); InitData(); }

void CDlgBoundProduct::OnEnChangeProductpart1Numpages(){ CString strWin; GetDlgItem(IDC_PRODUCTPART1_NUMPAGES)->GetWindowText(strWin); if (strWin.GetLength() == 0)	return;
															 UpdateData(TRUE); ChangeProductPartNumPages(0, 0); SaveData(); }
void CDlgBoundProduct::OnEnChangeProductpart2Numpages(){ CString strWin; GetDlgItem(IDC_PRODUCTPART2_NUMPAGES)->GetWindowText(strWin); if (strWin.GetLength() == 0)	return;
															 UpdateData(TRUE); ChangeProductPartNumPages(0, 0); SaveData(); }
void CDlgBoundProduct::OnEnChangeProductpart3Numpages(){ CString strWin; GetDlgItem(IDC_PRODUCTPART3_NUMPAGES)->GetWindowText(strWin); if (strWin.GetLength() == 0)	return;
															 UpdateData(TRUE); ChangeProductPartNumPages(0, 0); SaveData(); }
void CDlgBoundProduct::OnEnChangeProductpart4Numpages(){ CString strWin; GetDlgItem(IDC_PRODUCTPART4_NUMPAGES)->GetWindowText(strWin); if (strWin.GetLength() == 0)	return;
															 UpdateData(TRUE); ChangeProductPartNumPages(0, 0); SaveData(); }
void CDlgBoundProduct::OnEnChangeProductpart5Numpages(){ CString strWin; GetDlgItem(IDC_PRODUCTPART5_NUMPAGES)->GetWindowText(strWin); if (strWin.GetLength() == 0)	return;
															 UpdateData(TRUE); ChangeProductPartNumPages(0, 0); SaveData(); }
void CDlgBoundProduct::OnEnChangeProductpart6Numpages(){ CString strWin; GetDlgItem(IDC_PRODUCTPART6_NUMPAGES)->GetWindowText(strWin); if (strWin.GetLength() == 0)	return;
															 UpdateData(TRUE); ChangeProductPartNumPages(0, 0); SaveData(); }
void CDlgBoundProduct::OnEnChangeProductpart7Numpages(){ CString strWin; GetDlgItem(IDC_PRODUCTPART7_NUMPAGES)->GetWindowText(strWin); if (strWin.GetLength() == 0)	return;
															 UpdateData(TRUE); ChangeProductPartNumPages(0, 0); SaveData(); }
void CDlgBoundProduct::OnEnChangeProductpart8Numpages(){ CString strWin; GetDlgItem(IDC_PRODUCTPART8_NUMPAGES)->GetWindowText(strWin); if (strWin.GetLength() == 0)	return;
															 UpdateData(TRUE); ChangeProductPartNumPages(0, 0); SaveData(); }

void CDlgBoundProduct::ChangeNumeration(int nIndex)
{
	UpdateData(TRUE);

	int		nStartNum			= m_pDlgNumeration->m_nStartNum;
	int		nRangeTo			= m_pDlgNumeration->m_nRangeTo;
	int		nRangeFrom			= m_pDlgNumeration->m_nRangeFrom;
	int		nSelection			= m_pDlgNumeration->m_nSelection;
	CString strNumerationStyle	= m_pDlgNumeration->m_strNumerationStyle;
	BOOL	bReverse			= m_pDlgNumeration->m_bReverse;

	if (m_pDlgNumeration->DoModal() == IDCANCEL)
	{
		m_pDlgNumeration->m_nStartNum			= nStartNum;
		m_pDlgNumeration->m_nRangeTo			= nRangeTo;
		m_pDlgNumeration->m_nRangeFrom			= nRangeFrom;
		m_pDlgNumeration->m_nSelection			= nSelection;
		m_pDlgNumeration->m_strNumerationStyle	= strNumerationStyle;
		m_pDlgNumeration->m_bReverse			= bReverse;
	}
	else
	{
		InitNumberingString(nIndex);
		SaveData();
	}
}

void CDlgBoundProduct::OnBnClickedProductpart1NumButton() { ChangeNumeration(0); }
void CDlgBoundProduct::OnBnClickedProductpart2NumButton() { ChangeNumeration(1); }
void CDlgBoundProduct::OnBnClickedProductpart3NumButton() { ChangeNumeration(2); }
void CDlgBoundProduct::OnBnClickedProductpart4NumButton() { ChangeNumeration(3); }
void CDlgBoundProduct::OnBnClickedProductpart5NumButton() { ChangeNumeration(4); }
void CDlgBoundProduct::OnBnClickedProductpart6NumButton() { ChangeNumeration(5); }
void CDlgBoundProduct::OnBnClickedProductpart7NumButton() { ChangeNumeration(6); }
void CDlgBoundProduct::OnBnClickedProductpart8NumButton() { ChangeNumeration(7); }

void CDlgBoundProduct::InitNumberingString(int nIndex)
{
//	CString* pstrNumbering = &m_strNumbering1;
//	switch (nIndex)
//	{
//	case 0: pstrNumbering = &m_strNumbering1; break;
//	case 1: pstrNumbering = &m_strNumbering2; break;
//	case 2: pstrNumbering = &m_strNumbering3; break;
//	case 3: pstrNumbering = &m_strNumbering4; break;
//	case 4: pstrNumbering = &m_strNumbering5; break;
//	case 5: pstrNumbering = &m_strNumbering6; break;
//	case 6: pstrNumbering = &m_strNumbering7; break;
//	case 7: pstrNumbering = &m_strNumbering8; break;
//	}
//
////	CString strOut;
////	if (pProductGroup->m_strName.CompareNoCase(theApp.m_strCoverName) == 0)
//	{
//		*pstrNumbering = pDoc->m_PageTemplateList.GetPageRange(nIndex);
//		return;
//	}
		
	//int		nProductGroupNumPages = m_boundProduct.m_parts[nIndex].m_nNumPages;
	//CString strPageNum;
	//strPageNum.Format(_T("%d"), nProductGroupNumPages);

	//int nCurNum = (m_pDlgNumeration->m_nStartNum - 1) * CDlgNumeration::GetSeqLen(m_pDlgNumeration->m_strNumerationStyle) + 1;
	//CString strPageIDFrom = (m_pDlgNumeration->m_nRangeFrom == 1) ? CDlgNumeration::GenerateNumber(0, m_pDlgNumeration->m_strNumerationStyle, nCurNum) : _T("1");
	//nCurNum += (m_pDlgNumeration->m_bReverse) ? -(nProductGroupNumPages - 1) : (nProductGroupNumPages - 1);
	//int nPageIndex = nProductGroupNumPages - 1;
	//CString strPageIDTo   = (m_pDlgNumeration->m_nRangeTo == nProductGroupNumPages) ? CDlgNumeration::GenerateNumber(nPageIndex, m_pDlgNumeration->m_strNumerationStyle, nCurNum - nPageIndex) : strPageNum;

	//pstrNumbering->Format(_T(" %s - %s"), strPageIDFrom, strPageIDTo);
}

HBRUSH CDlgBoundProduct::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

  //  if(nCtlColor == CTLCOLOR_STATIC)
  //  {
		////UINT nID = pWnd->GetDlgCtrlID();
		////switch (nID)
		////{
		////default:
		//if (theApp.m_nOSVersion == CImpManApp::WinXP)
		//{
		//		//pDC->SetBkMode(TRANSPARENT);
		//		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
		//		//hbr = (HBRUSH)m_bkBrush.GetSafeHandle();
		//}
		////		break;
		////}
  //  }

	return hbr;
}

void CDlgBoundProduct::OnBnClickedProductpart1Remove()	{ RemoveProductPart(0); }
void CDlgBoundProduct::OnBnClickedProductpart2Remove()	{ RemoveProductPart(1); }
void CDlgBoundProduct::OnBnClickedProductpart3Remove()	{ RemoveProductPart(2); }
void CDlgBoundProduct::OnBnClickedProductpart4Remove()	{ RemoveProductPart(3); }
void CDlgBoundProduct::OnBnClickedProductpart5Remove()	{ RemoveProductPart(4); }
void CDlgBoundProduct::OnBnClickedProductpart6Remove()	{ RemoveProductPart(5); }
void CDlgBoundProduct::OnBnClickedProductpart7Remove()	{ RemoveProductPart(6); }
void CDlgBoundProduct::OnBnClickedProductpart8Remove()	{ RemoveProductPart(7); }

void CDlgBoundProduct::RemoveProductPart(int nProductPartIndex)
{
	//CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;

	//if (nProductPartIndex < m_boundProduct.m_parts.GetSize())
	//{
	//	CString strMsg;
	//	AfxFormatString1(strMsg, IDS_DELETE_PRODUCTPART_MSG, m_boundProduct.m_parts[nProductPartIndex].m_strName);
	//	if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
	//		return;

	//	pDoc->m_Bookblock.RemoveFoldSheets(nProductPartIndex);
	//	pDoc->m_Bookblock.Reorganize();
	//	pDoc->m_PageTemplateList.RemoveProductGroupTemplates(nProductPartIndex);
	//	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	//	pDoc->m_PageTemplateList.FindPrintSheetLocations(nProductPartIndex);
	//	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	//	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	//	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	//	pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
	//	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

	//	pDoc->m_MarkSourceList.RemoveUnusedSources();

	//	m_boundProduct.m_parts.RemoveAt(nProductPartIndex);
	//	m_boundProduct.ReorganizeReferences(nProductPartIndex);

	//	///// reinitialize
	//	m_strProductPartName1	= m_strProductPartName2		= m_strProductPartName3		= m_strProductPartName4		= _T("");
	//	m_nProductPartNumPages1	= m_nProductPartNumPages2	= m_nProductPartNumPages3	= m_nProductPartNumPages4	= 0;
	//	for (int i = 0; i < m_boundProduct.m_parts.GetSize(); i++)
	//	{
	//		switch (i)
	//		{
	//		case 0:	m_strProductPartName1	= m_boundProduct.m_parts[i].m_strName;
	//				m_nProductPartNumPages1	= m_boundProduct.m_parts[i].m_nNumPages;
	//				break;
	//		case 1:	m_strProductPartName2	= m_boundProduct.m_parts[i].m_strName;
	//				m_nProductPartNumPages2	= m_boundProduct.m_parts[i].m_nNumPages;
	//				break;
	//		case 2:	m_strProductPartName3	= m_boundProduct.m_parts[i].m_strName;
	//				m_nProductPartNumPages3	= m_boundProduct.m_parts[i].m_nNumPages;
	//				break;
	//		case 3:	m_strProductPartName4	= m_boundProduct.m_parts[i].m_strName;
	//				m_nProductPartNumPages4	= m_boundProduct.m_parts[i].m_nNumPages;
	//				break;
	//		case 4:	m_strProductPartName5	= m_boundProduct.m_parts[i].m_strName;
	//				m_nProductPartNumPages5	= m_boundProduct.m_parts[i].m_nNumPages;
	//				break;
	//		case 5:	m_strProductPartName6	= m_boundProduct.m_parts[i].m_strName;
	//				m_nProductPartNumPages6	= m_boundProduct.m_parts[i].m_nNumPages;
	//				break;
	//		case 6:	m_strProductPartName7	= m_boundProduct.m_parts[i].m_strName;
	//				m_nProductPartNumPages7	= m_boundProduct.m_parts[i].m_nNumPages;
	//				break;
	//		case 7:	m_strProductPartName8	= m_boundProduct.m_parts[i].m_strName;
	//				m_nProductPartNumPages8	= m_boundProduct.m_parts[i].m_nNumPages;
	//				break;
	//		}
	//		InitNumberingString(i);
	//	}

	//	ChangeProductPartNumPages(nProductPartIndex, 0);
	//	for (int i = 0; i < 4; i++)
	//	{
	//		UpdateColors(i);
	//		UpdateDetails(i);
	//	}
	//	SaveData();
	//	InitData();
	//	SetFocus();
	//}
}

void CDlgBoundProduct::OnBnClickedProductpart1ColorsButton()	{ InitProductPartColor(0); }
void CDlgBoundProduct::OnBnClickedProductpart2ColorsButton()	{ InitProductPartColor(1); }
void CDlgBoundProduct::OnBnClickedProductpart3ColorsButton()	{ InitProductPartColor(2); }
void CDlgBoundProduct::OnBnClickedProductpart4ColorsButton()	{ InitProductPartColor(3); }
void CDlgBoundProduct::OnBnClickedProductpart5ColorsButton()	{ InitProductPartColor(4); }
void CDlgBoundProduct::OnBnClickedProductpart6ColorsButton()	{ InitProductPartColor(5); }
void CDlgBoundProduct::OnBnClickedProductpart7ColorsButton()	{ InitProductPartColor(6); }
void CDlgBoundProduct::OnBnClickedProductpart8ColorsButton()	{ InitProductPartColor(7); }

void CDlgBoundProduct::InitProductPartColor(int nProductPartIndex)
{
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= m_boundProduct.m_parts.GetSize()) )
		return;

	CDlgSelectJobColors dlg;
	dlg.m_colorantList.Append(m_boundProduct.m_parts[nProductPartIndex].m_colorantList);
	if (dlg.DoModal() == IDCANCEL)
		return;

	m_boundProduct.m_parts[nProductPartIndex].m_colorantList.RemoveAll();
	m_boundProduct.m_parts[nProductPartIndex].m_colorantList.Append(dlg.m_colorantList);

	//pDoc->m_PrintSheetList.ReorganizeColorInfos();

	UpdateColors(nProductPartIndex);
}

void CDlgBoundProduct::UpdateColors(int nProductPartIndex)
{
	if (nProductPartIndex < 0)
	{
		m_productPart1Colors.Invalidate(); m_productPart1Colors.UpdateWindow(); 
		m_productPart2Colors.Invalidate(); m_productPart2Colors.UpdateWindow(); 
		m_productPart3Colors.Invalidate(); m_productPart3Colors.UpdateWindow(); 
		m_productPart4Colors.Invalidate(); m_productPart4Colors.UpdateWindow(); 
		m_productPart5Colors.Invalidate(); m_productPart5Colors.UpdateWindow(); 
		m_productPart6Colors.Invalidate(); m_productPart6Colors.UpdateWindow(); 
		m_productPart7Colors.Invalidate(); m_productPart7Colors.UpdateWindow(); 
		m_productPart8Colors.Invalidate(); m_productPart8Colors.UpdateWindow(); 
	}
	else
	{
		switch (nProductPartIndex)
		{
		case 0:	m_productPart1Colors.Invalidate(); m_productPart1Colors.UpdateWindow(); break;
		case 1:	m_productPart2Colors.Invalidate(); m_productPart2Colors.UpdateWindow(); break;
		case 2:	m_productPart3Colors.Invalidate(); m_productPart3Colors.UpdateWindow(); break;
		case 3:	m_productPart4Colors.Invalidate(); m_productPart4Colors.UpdateWindow(); break;
		case 4:	m_productPart5Colors.Invalidate(); m_productPart5Colors.UpdateWindow(); break;
		case 5:	m_productPart6Colors.Invalidate(); m_productPart6Colors.UpdateWindow(); break;
		case 6:	m_productPart7Colors.Invalidate(); m_productPart7Colors.UpdateWindow(); break;
		case 7:	m_productPart8Colors.Invalidate(); m_productPart8Colors.UpdateWindow(); break;
		}
	}
}

void CDlgBoundProduct::OnBnClickedProductpart1DetailsButton() { OpenDetails(0); }
void CDlgBoundProduct::OnBnClickedProductpart2DetailsButton() { OpenDetails(1); }
void CDlgBoundProduct::OnBnClickedProductpart3DetailsButton() { OpenDetails(2); }
void CDlgBoundProduct::OnBnClickedProductpart4DetailsButton() { OpenDetails(3); }
void CDlgBoundProduct::OnBnClickedProductpart5DetailsButton() { OpenDetails(4); }
void CDlgBoundProduct::OnBnClickedProductpart6DetailsButton() { OpenDetails(5); }
void CDlgBoundProduct::OnBnClickedProductpart7DetailsButton() { OpenDetails(6); }
void CDlgBoundProduct::OnBnClickedProductpart8DetailsButton() { OpenDetails(7); }

void CDlgBoundProduct::OpenDetails(int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= m_boundProduct.m_parts.GetSize()) )
		return;

	CDlgBoundProductDetails dlg;
	dlg.m_boundProductPart = m_boundProduct.m_parts[nProductPartIndex];
	dlg.m_nBoundProductPartIndex = nProductPartIndex;
	if (dlg.DoModal() == IDCANCEL)
		return;

	m_boundProduct.m_parts[nProductPartIndex] = dlg.m_boundProductPart;

	pDoc->m_Bookblock.m_FoldSheetList.TakeOverProductPartQuantityPlanned(nProductPartIndex, dlg.m_boundProductPart.m_nDesiredQuantity);
	pDoc->m_Bookblock.m_FoldSheetList.TakeOverProductPartQuantityExtra  (nProductPartIndex, dlg.m_boundProductPart.m_nExtra);
	pDoc->m_Bookblock.m_FoldSheetList.TakeOverProductPartPaperName		(nProductPartIndex, dlg.m_boundProductPart.m_strPaper);

	//m_boundProduct.m_fTrimSizeWidth		= dlg.m_boundProductPart.m_fPageWidth;
	//m_boundProduct.m_fTrimSizeHeight	= dlg.m_boundProductPart.m_fPageHeight;				
	//m_boundProduct.m_strFormatName	 	= dlg.m_boundProductPart.m_strFormatName;
	//m_boundProduct.m_nPageOrientation 	= dlg.m_boundProductPart.m_nPageOrientation;
	//m_boundProduct.m_bindingDefs		= dlg.m_boundProductPart.m_bindingDefs;
	//m_boundProduct.m_nDesiredQuantity	= dlg.m_boundProductPart.m_nDesiredQuantity;

	UpdateDetails(nProductPartIndex);
	InitData();
}

void CDlgBoundProduct::UpdateDetails(int nProductPartIndex)
{
	if (nProductPartIndex < 0)
	{
		m_productPart1Details.Invalidate(); m_productPart1Details.UpdateWindow();
		m_productPart2Details.Invalidate(); m_productPart2Details.UpdateWindow();
		m_productPart3Details.Invalidate(); m_productPart3Details.UpdateWindow();
		m_productPart4Details.Invalidate(); m_productPart4Details.UpdateWindow();
		m_productPart5Details.Invalidate(); m_productPart5Details.UpdateWindow();
		m_productPart6Details.Invalidate(); m_productPart6Details.UpdateWindow();
		m_productPart7Details.Invalidate(); m_productPart7Details.UpdateWindow();
		m_productPart8Details.Invalidate(); m_productPart8Details.UpdateWindow();
	}
	else
	{
		switch (nProductPartIndex)
		{
		case 0:	m_productPart1Details.Invalidate(); m_productPart1Details.UpdateWindow(); break;
		case 1:	m_productPart2Details.Invalidate(); m_productPart2Details.UpdateWindow(); break;
		case 2:	m_productPart3Details.Invalidate(); m_productPart3Details.UpdateWindow(); break;
		case 3:	m_productPart4Details.Invalidate(); m_productPart4Details.UpdateWindow(); break;
		case 4:	m_productPart1Details.Invalidate(); m_productPart5Details.UpdateWindow(); break;
		case 5:	m_productPart2Details.Invalidate(); m_productPart6Details.UpdateWindow(); break;
		case 6:	m_productPart3Details.Invalidate(); m_productPart7Details.UpdateWindow(); break;
		case 7:	m_productPart4Details.Invalidate(); m_productPart8Details.UpdateWindow(); break;
		}
	}
}

void CDlgBoundProduct::OnBnClickedOk()
{
	SaveData();

	if (CheckData() == IDCANCEL)
		return;

	OnOK();
}
