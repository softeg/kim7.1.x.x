// ImpManDocServerCommands.cpp 
//


#include "stdafx.h"
#include "Imposition Manager.h"
#include "JDFOutputImposition.h"
#include "PrintJobDocumentation.h"
#include "CSVInput.h"
#include "XMLInput.h"
#include "JDFInputMIS.h"
#include "DlgOptimizationMonitor.h"
#include "XMLCommon.h"
#include "XMLOutput.h"
#include "DlgProductionManager.h"



int	CImpManDoc::ProcessCommand(int nCommand, const CString& strPDFDoc, CString strStartPage, CString strPDFInfoFile, CString strPreviewsFolder)
{
	CDlgProductionManager::LoadProfileList();	// profiles could be changed while kimEngine is running -> so load before each ProcessCommand

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath(strPDFDoc, szDrive, szDir, szFname, szExt);

	CString strPDFDocPath  = CString(szDrive) + CString(szDir);
	CString strPDFDocTitle = CString(szFname);

	if (strStartPage.IsEmpty())
		strStartPage = _T("-1"); 
	if (strPDFInfoFile.IsEmpty())
	{
		if (theApp.settings.m_bPDFPreviewsOrigPlace)
			strPDFInfoFile.Format(_T("%s%s.pdfinfo"), strPDFDocPath, strPDFDocTitle);
		else
			strPDFInfoFile = theApp.settings.m_szPDFPreviewFolder + CString("\\") + strPDFDocTitle + _T(".pdfinfo");
	}
	if (strPreviewsFolder.IsEmpty())
		strPreviewsFolder.Format(_T("%s%s.preview"), strPDFDocPath, strPDFDocTitle);

	_tsplitpath(GetPathName(), szDrive, szDir, szFname, szExt);
	CString strTemplatePath = CString(szDrive) + CString(szDir);

	switch (nCommand)
	{ 
	case CImpManApp::CmdGetOutputInfo:	
			return WriteJobInfoExchangeFile(strTemplatePath);

	case CImpManApp::CmdCheckRegisteredPDFs:	  
		{
			if (m_PageSourceList.CheckAutoPlausibility(this))
				return 1;
			else
				return 0;
		}
		break;

	case CImpManApp::CmdIsRegistered:	 
		{
			if (m_PageSourceList.SourceDocumentExists(strPDFDoc))
				return 1;
			else
				return 0;
		}
		break;
 
	case CImpManApp::CmdRegister:	
		{
			int nRet = 0;
			if ( ! m_PageSourceList.SourceDocumentExists(strPDFDoc))
			{
				CPageSource pageSource;
				pageSource.Create(CPageSource::PDFFile, strPDFDoc, strPDFInfoFile, strPreviewsFolder);
				pageSource.RegisterPDFPages(strPDFDoc, strPDFInfoFile, _ttoi((LPCTSTR)strStartPage));
				m_PageSourceList.AddTail(pageSource);
				nRet = m_PageTemplateList.AutoAssignPageSource(&m_PageSourceList.GetTail(), -1);
			}
			else
				nRet = 2;

			HDC hDC = CreateDC(NULL, "Adobe PDF", NULL, NULL);
			CPrintJobDocumentation printJob;
			printJob.Print(hDC);

			return nRet;
		}
		break;

	case CImpManApp::CmdUnregister:	
		{
			if ( ! m_PageSourceList.SourceDocumentExists(strPDFDoc))	// already unregistered
				return 2;
			else
			{
				BOOL bDeleted = FALSE;
				short nPageSourceIndex = m_PageSourceList.GetPageSourceIndex(strPDFDoc);
				if ((nPageSourceIndex >= 0) && (nPageSourceIndex < m_PageSourceList.GetCount()))
				{
					POSITION pos = m_PageSourceList.FindIndex(nPageSourceIndex);
					if (pos)
					{
						m_PageSourceList.RemoveAt(pos);
						m_PageTemplateList.RemovePageSource(nPageSourceIndex, FALSE, TRUE);
						m_Bookblock.m_OutputDate.m_dt = 0;
						bDeleted = TRUE;
					}
				}
				if (bDeleted)
				{
					m_PageSourceList.InitializePageTemplateRefs();
					m_PageTemplateList.ReorganizePrintSheetPlates();
					m_MarkTemplateList.ReorganizePrintSheetPlates();
					m_PrintSheetList.ReorganizeColorInfos();
					SetModifiedFlag();

					HDC hDC = CreateDC(NULL, "Adobe PDF", NULL, NULL);
					CPrintJobDocumentation printJob;
					printJob.Print(hDC);

					return 1;
				}
				return 0;
			}
		}
		break;

	case CImpManApp::CmdImpose:	
	case CImpManApp::CmdImposeQuiet:	
		{
			if ( ! m_PageSourceList.SourceDocumentExists(strPDFDoc))
			{
				CPageSource pageSource;
				pageSource.Create(CPageSource::PDFFile, strPDFDoc, strPDFInfoFile, strPreviewsFolder);
				pageSource.RegisterPDFPages(strPDFDoc, strPDFInfoFile);
				m_PageSourceList.AddTail(pageSource);
				m_PageTemplateList.AutoAssignPageSource(&m_PageSourceList.GetTail(), -1);
			}
			if (m_Bookblock.ReadyForOutput())
				m_Bookblock.OutputProofAuto(GetPathName());
			return m_PrintSheetList.OutputSheetsAuto(GetPathName());
		}
		break;
 
	case CImpManApp::CmdImposeCSV:
		{
// /ic "C:\Program Files (x86)\KIM7.1\KIM Auto Jobs\Tucanna\36 pages\36 pages.job" "C:\Program Files (x86)\KIM7.1\KIM Auto Jobs\Tucanna\36 pages\36 pages.csv" "Brochures" "Pause"
			CCSVInput csvInput(GetFlatProductsIndex());
			if ( ! csvInput.DoImport(strPDFDoc))	// pdf doc means csv file
				return 0;
			else
			{
				if (KimEngineDBJobAbort(theApp.m_nDBCurrentJobID))
				{
					OnSaveDocument(GetPathName());
					WriteJobInfoExchangeFile(strTemplatePath);
					return 0;
				}

				CString strPDFTarget;
				m_productionSetup.Load(strStartPage);
				CProductionProfile productionProfile = theApp.m_productionProfiles.FindProfile(m_productionSetup.m_strProductionProfile);
				POSITION pos = m_printingProfiles.GetHeadPosition();
				while (pos)
				{
					CPrintingProfile& rProfile		= m_printingProfiles.GetNext(pos);
					CPrintingProfile oldProfile 	= rProfile;

					rProfile.AssignProductionProfilePool(productionProfile);
					rProfile.ApplyModifications(oldProfile);
				}

				pos = m_boundProducts.GetHeadPosition();
				while (pos)
				{
					CBoundProduct& rBoundProduct = m_boundProducts.GetNext(pos);
					rBoundProduct.AssignProductionProfilePool(productionProfile);
					for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
					{
						CBoundProductPart& rProductPart = rBoundProduct.m_parts[i];
						rProductPart.AssignProductionProfilePool(productionProfile);
					}
				}
				m_printGroupList.Analyze();

				CDlgOptimizationMonitor dlg;
				dlg.Create(IDD_OPTIMIZATION_MONITOR);
				dlg.OptimizeAll();
				dlg.DestroyWindow();

				WriteJobInfoExchangeFile(strTemplatePath);

				if (strPDFInfoFile.CompareNoCase(_T("Pause")) != 0)		// pdfinfo file used to transport attribute "Pause"
				{
					OnSaveDocument(GetPathName());
					return m_PrintSheetList.OutputSheetsAuto(GetPathName());
				}
				else
					return 1;
			}
		}
		break;

	case CImpManApp::CmdImposeXML:
		{
			int		  nAttribute = atoi(strPDFInfoFile);
			CXMLInput xmlInput; 
			if ( ! xmlInput.ParseXML(strPDFDoc, (nAttribute & kimAutoDB::Action_ImposeFoldSheetsOnly) ? TRUE : FALSE, theApp.m_productionProfiles.FindProfile(strStartPage)))
				return 0;
			else
			{ 
				if (KimEngineDBJobAbort(theApp.m_nDBCurrentJobID))
				{
					OnSaveDocument(GetPathName());
					WriteJobInfoExchangeFile(strTemplatePath);
					return 0;
				}

				if (nAttribute & kimAutoDB::Action_ImposeByProfile)
				{
					//m_productionSetup.Load(strStartPage);
					m_productionSetup						 = CProductionSetup();
					m_productionSetup.m_strName				 = _("AutoSetup");
					m_productionSetup.m_strProductionProfile = strStartPage;
					m_printGroupList.Analyze();

					CDlgOptimizationMonitor dlg;
					dlg.Create(IDD_OPTIMIZATION_MONITOR);
					dlg.OptimizeAll(); 
					dlg.DestroyWindow();
				}

				WriteJobInfoExchangeFile(strTemplatePath);

				if (nAttribute & kimAutoDB::DataOutput_ResultsBackToSourceXML)
				{
					CXMLOutput xmlOutput;
					xmlOutput.UpdateXML(strPDFDoc);
				}

				if (nAttribute & kimAutoDB::DataOutput_Booklets)
					m_Bookblock.OutputProofAuto(GetPathName());

				if (nAttribute & kimAutoDB::DataOutput_FoldSheets)
					m_Bookblock.m_FoldSheetList.OutputSheetsAuto();

				if (nAttribute & kimAutoDB::DataOutput_PrintSheets)
				{
					OnSaveDocument(GetPathName());
					return m_PrintSheetList.OutputSheetsAuto(GetPathName());
				}
				else
					return 1;
			}
		}
		break;

	case CImpManApp::CmdImposeJDF:
		{
			CJDFInputMIS jdfInput; 
			if ( ! jdfInput.Open(strPDFDoc))
				return 0;
			else
			{ 
				if (KimEngineDBJobAbort(theApp.m_nDBCurrentJobID))
				{
					OnSaveDocument(GetPathName());
					WriteJobInfoExchangeFile(strTemplatePath);
					return 0;
				}

				if ( ! strStartPage.IsEmpty() && (strStartPage != _T("-1")) )	// if there is a profile, we use it to auto impose. Otherwise we assume, that imposition info is included in JDF.
				{
					//m_productionSetup.Load(strStartPage);
					//m_printGroupList.Analyze();

					//CDlgOptimizationMonitor dlg;
					//dlg.Create(IDD_OPTIMIZATION_MONITOR); 
					//dlg.OptimizeAll();
					//dlg.DestroyWindow();
					m_productionSetup						 = CProductionSetup();
					m_productionSetup.m_strName				 = _T("AutoSetup");
					m_productionSetup.m_strProductionProfile = strStartPage;
					CProductionProfile& rProductionProfile = theApp.m_productionProfiles.FindProfile(m_productionSetup.m_strProductionProfile);
					CPrepressParams&	rPrepressParams	   = (rProductionProfile.ProcessIsActive(CProductionProfile::Prepress)) ? rProductionProfile.m_prepress : CPrepressParams();
					if ( ! rPrepressParams.m_strPrintSheetOutputProfile.IsEmpty())
					{
						POSITION pos = m_PrintSheetList.GetHeadPosition();
						while (pos)
						{
							CPrintSheet& rPrintSheet  = m_PrintSheetList.GetNext(pos);
							if (rPrintSheet.m_strPDFTarget.IsEmpty())
								rPrintSheet.m_strPDFTarget = rPrepressParams.m_strPrintSheetOutputProfile;
						}
					}
				}

				WriteJobInfoExchangeFile(strTemplatePath);

				if (strPDFInfoFile.CompareNoCase(_T("Pause")) != 0)		// pdfinfo file used to transport attribute "Pause"
				{
					OnSaveDocument(GetPathName());
					return m_PrintSheetList.OutputSheetsAuto(GetPathName());
				}
				else
					return 1;
			}
		}
		break;

	case CImpManApp::CmdDoOutput:
		{
// /do "C:\Program Files (x86)\KIM7.1\KIM Auto Jobs\Harms\Pool_6492_XL106_01_09_14_19_46\Pool_6492_XL106_01_09_14_19_46.job"
			return m_PrintSheetList.OutputSheetsAuto(GetPathName());
		}
		break;

	case CImpManApp::CmdResetPageSource:	// remove and reset corresponding plate timestamps
	case CImpManApp::CmdResetPageSourceX:	// remove w/o resetting page numbers (exchange of document) and reset corresponding plate timestamps
		{
			BOOL bDeleted = FALSE;
			short nPageSourceIndex = m_PageSourceList.GetPageSourceIndex(strPDFDoc);
			if ((nPageSourceIndex >= 0) && (nPageSourceIndex < m_PageSourceList.GetCount()))
			{
				POSITION pos = m_PageSourceList.FindIndex(nPageSourceIndex);
				if (pos)
				{
					m_PageSourceList.RemoveAt(pos);
					m_PageTemplateList.RemovePageSource(nPageSourceIndex, FALSE, TRUE, (nCommand == CImpManApp::CmdResetPageSource) ? TRUE : FALSE);		
					if (nCommand == CImpManApp::CmdResetPageSource)
						m_Bookblock.m_OutputDate.m_dt = 0;
					bDeleted = TRUE;
				}
			}
			if (bDeleted)
			{
				m_PageSourceList.InitializePageTemplateRefs();
				m_PageTemplateList.ReorganizePrintSheetPlates();
				m_MarkTemplateList.ReorganizePrintSheetPlates();
				m_PrintSheetList.ReorganizeColorInfos();
				SetModifiedFlag();
				return 1;
			}
			return 0;
		}
		break;

	case CImpManApp::CmdResetPageList:
		{
			if ( ! m_PageSourceList.GetCount())
				return 1;
			m_PageTemplateList.RemovePageSourcesAll();
			m_PageSourceList.RemoveAll();
			m_PageSourceList.InitializePageTemplateRefs();
			m_PageTemplateList.ReorganizePrintSheetPlates();
			m_MarkTemplateList.ReorganizePrintSheetPlates();
			m_PrintSheetList.ReorganizeColorInfos();
			SetModifiedFlag();	
		}
		break;

	case CImpManApp::CmdNew:
	case CImpManApp::CmdPageList:
		{
			CPageSource pageSource;
			pageSource.Create(CPageSource::PDFFile, strPDFDoc, strPDFInfoFile, strPreviewsFolder);
			pageSource.RegisterPDFPages(strPDFDoc, strPDFInfoFile);

			m_PageTemplateList.RemovePageSourcesAll();
			m_PageSourceList.RemoveAll();
			m_PageSourceList.AddTail(pageSource);

			m_PageTemplateList.AutoAssignPageSource(&m_PageSourceList.GetTail(), -1);

			SetModifiedFlag(FALSE);	// avoid save doc prompt
		}
		break;

	default:	
		break;
	}

	return 0;
}

int CImpManDoc::WriteJobInfoExchangeFile(const CString& strPath)
{
	CFile			file;
	CFileException	fileException;
	CString			strInfoFile;
	strInfoFile.Format(_T("%sImpMan.info"), strPath);

	if (!file.Open(strInfoFile, CFile::modeWrite|CFile::modeCreate|CFile::modeNoTruncate|CFile::shareDenyRead, &fileException))
	{
		TCHAR szMsgError[255];
		fileException.GetErrorMessage(szMsgError, 255);
		CString strMsg;
		strMsg.Format(_T("Can't open file %s, error = %s\n"), strInfoFile, szMsgError);
		if (theApp.m_nGUIStyle != CImpManApp::GUIStandard)
			theApp.LogEntry(strMsg);
	}
	else
	{
		CJobInfoExchange jobInfoExchange;
		m_boundProducts.GetProductInfo(jobInfoExchange);
		m_flatProducts.GetProductInfo(jobInfoExchange);
		m_PageSourceList.GetPDFInputInfo(jobInfoExchange);
		m_PrintSheetList.GetOutputSheetInfo(jobInfoExchange);

		CArchive archive(&file, CArchive::store);

		jobInfoExchange.m_productInfoList.Serialize(archive);
		jobInfoExchange.m_pdfInputInfoList.Serialize(archive);
		jobInfoExchange.m_outputSheetInfoList.Serialize(archive);

		archive.Close();
		file.Close();
	}

	return 0;
}
