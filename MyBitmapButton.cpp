// MyBitmapButton.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "MyBitmapButton.h"


// CBitmapButton

IMPLEMENT_DYNAMIC(CMyBitmapButton, CButton)

CMyBitmapButton::CMyBitmapButton()
{
	m_bMouseOverButton = FALSE;
	m_font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_bkBrush.CreateStockObject(HOLLOW_BRUSH);
	m_strTheme = _T("");
	m_nPartID = -1;
	m_nImgIDHot = -1;
	m_nImgIDCold = -1;
	m_nImgIDDisabled = -1;
	m_bTransparent = FALSE;
}

CMyBitmapButton::~CMyBitmapButton()
{
	m_font.DeleteObject();
	m_bkBrush.DeleteObject();
}


BEGIN_MESSAGE_MAP(CMyBitmapButton, CButton)
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_WM_CREATE()
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()



void CMyBitmapButton::SetTheme(CString strTheme, int nPartID, int nPosition)
{
	m_strTheme		  = strTheme;
	m_nPartID		  = nPartID;
	m_nBitmapPosition = nPosition;
}

void CMyBitmapButton::SetBitmap(int nIDBitmapHot, int nIDBitmapCold, int nIDBitmapDisabled, int nPosition)
{
	m_nImgIDHot		  = nIDBitmapHot;
	m_nImgIDCold	  = nIDBitmapCold;
	m_nImgIDDisabled  = nIDBitmapDisabled;
	m_nBitmapPosition = nPosition;
}

void CMyBitmapButton::SetTransparent(BOOL bTransparent)
{
	m_bTransparent = bTransparent;
}

void CMyBitmapButton::AlignToBitmap()
{
	CImage img;
	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(m_nImgIDHot));
	if (! img.IsNull())
	{
		CRect rcButton;
		GetWindowRect(rcButton);
		rcButton.right  = rcButton.left + img.GetWidth();
		rcButton.bottom = rcButton.top  + img.GetHeight();
		GetParent()->ScreenToClient(rcButton);
		MoveWindow(rcButton, FALSE);
	}
}


// CMyBitmapButton-Meldungshandler


int CMyBitmapButton::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CButton::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_font);

	return 0;
}

HBRUSH CMyBitmapButton::CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/)
{
	return (HBRUSH)m_bkBrush.GetSafeHandle();
}

void CMyBitmapButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect rcItem = lpDrawItemStruct->rcItem;
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

	rcItem.left -= 1; rcItem.right += 1; rcItem.top -= 1;
	if ( ! m_bTransparent)
		ThemeButton(pDC, rcItem, m_bMouseOverButton, lpDrawItemStruct->itemState, GetParent());

	pDC->SelectObject(GetParent()->GetFont());
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor((lpDrawItemStruct->itemState & ODS_DISABLED) ? MIDDLEGRAY : BLACK);

	CString strText;
	GetWindowText(strText);
	if ( ! strText.IsEmpty())
	{
		rcItem.left += 8;
		CRect rcText = rcItem; rcText.right -= 20;
		pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
		rcItem.left = rcText.right; rcItem.right -= 5;
	}

	ThemeBitmap(pDC, lpDrawItemStruct->rcItem, m_bMouseOverButton, lpDrawItemStruct->itemState, GetParent(), m_strTheme, m_nPartID, m_nImgIDHot, m_nImgIDCold, m_nImgIDDisabled, m_nBitmapPosition);
}

void CMyBitmapButton::ThemeButton(CDC* pDC, CRect rcItem, BOOL bMouseOverButton, int nState, CWnd* pParentWnd)
{
#ifdef UNICODE
	HTHEME hThemeButton = theApp._OpenThemeData(pParentWnd, _T("BUTTON"));
#else
	HTHEME hThemeButton = theApp._OpenThemeData(pParentWnd, CA2W("BUTTON"));
#endif

	BOOL bSelected = (nState & ODS_SELECTED) ? TRUE : FALSE;
	BOOL bDisabled = (nState & ODS_DISABLED) ? TRUE : FALSE;
	
	if (hThemeButton)
	{
		if (bDisabled)
			DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), BP_PUSHBUTTON, PBS_DISABLED,	&rcItem, 0);	
		else
		{
			if (bSelected)
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), BP_PUSHBUTTON, PBS_PRESSED, &rcItem, 0);	
			else
			{
				if (bMouseOverButton)
					DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), BP_PUSHBUTTON, PBS_HOT,		&rcItem, 0);	
				else
					DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), BP_PUSHBUTTON, PBS_NORMAL,	&rcItem, 0);	
			}
		}
	}
	else
	{
		if (bSelected)
			DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);	
		else
			DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_TRANSPARENT);	
	}

	theApp._CloseThemeData(hThemeButton);
}

void CMyBitmapButton::ThemeBitmap(CDC* pDC, CRect rcItem, BOOL bMouseOverButton, int nState, CWnd* pParentWnd, CString strTheme, int nPartID, int nImgIDHot, int nImgIDCold, int nImgIDDisabled, int nBitmapPosition)
{
#ifdef UNICODE
	HTHEME hThemeButton = theApp._OpenThemeData(pParentWnd, strTheme );
#else
	HTHEME hThemeButton = theApp._OpenThemeData(pParentWnd, CA2W( strTheme ));
#endif

	BOOL bSelected = (nState & ODS_SELECTED) ? TRUE : FALSE;
	BOOL bDisabled = (nState & ODS_DISABLED) ? TRUE : FALSE;

	//rcItem.DeflateRect(0, 2);
	//rcItem.top += 1; rcItem.bottom -= 2;

	if (hThemeButton)
	{
		int nXCenter = rcItem.CenterPoint().x;
		int nYCenter = rcItem.CenterPoint().y;
		int nWidth	 = min(16, rcItem.Width());
		int nHeight	 = min(16, rcItem.Height());
		rcItem.top	  = nYCenter - nHeight/2; 
		rcItem.bottom = nYCenter + nHeight/2; 
		switch (nBitmapPosition)
		{
		case LEFT:		rcItem.left  += 5; rcItem.right = rcItem.left  + nWidth;				break;
		case RIGHT:		rcItem.right -= 5; rcItem.left  = rcItem.right - nWidth;				break;
		case XCENTER:	rcItem.left = nXCenter - nWidth/2; rcItem.right = rcItem.left + nWidth;	break;
		}

		if (bDisabled)
			DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), nPartID, PBS_DISABLED,	&rcItem, 0);	
		else
		{
			if (bSelected)
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), nPartID, CBXS_PRESSED,		&rcItem, 0);	
			else
			{
				if (bMouseOverButton)
					DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), nPartID, CBXS_HOT,		&rcItem, 0);	
				else
					DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), nPartID, CBXS_NORMAL,	&rcItem, 0);	
			}
		}
	}
	else
		if (nImgIDHot != -1)
		{
			CImage img;
			if (bDisabled)
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE((nImgIDDisabled != -1) ? nImgIDDisabled : nImgIDHot));
			else
				if (bSelected)
					img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(nImgIDHot));
				else
					if (bMouseOverButton)
						img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(nImgIDHot));
					else
						img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE((nImgIDCold != -1) ? nImgIDCold : nImgIDHot));

			int nXCenter = rcItem.CenterPoint().x;
			int nYCenter = rcItem.CenterPoint().y;
			int nWidth	 = img.GetWidth();
			int nHeight	 = img.GetHeight();
			switch (nBitmapPosition)
			{
			case LEFT:		rcItem.left  += 5; rcItem.right = rcItem.left  + nWidth;				break;
			case RIGHT:		rcItem.right -= 5; rcItem.left  = rcItem.right - nWidth;				break;
			case XCENTER:	rcItem.left = nXCenter - nWidth/2; rcItem.right = rcItem.left + nWidth;	break;
			}

			img.TransparentBlt(pDC->m_hDC, rcItem.left, rcItem.CenterPoint().y - img.GetHeight()/2, img.GetWidth(), img.GetHeight(), WHITE);
		}

	theApp._CloseThemeData(hThemeButton);
}

void CMyBitmapButton::OnMouseMove(UINT nFlags, CPoint point)
{
	if ( ! m_bMouseOverButton)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = this->m_hWnd;
		if (::_TrackMouseEvent(&tme))
			m_bMouseOverButton = TRUE;
		InvalidateRect(NULL);
	}

	CButton::OnMouseMove(nFlags, point);
}

LRESULT CMyBitmapButton::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	m_bMouseOverButton = FALSE;
	InvalidateRect(NULL);
	UpdateWindow();
	return TRUE;
}


