// DlgProductionPunchParams.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionPunchingParams.h"
#include "DlgProductionPunchingDetails.h"
#include "DlgProductionManager.h"


// CDlgProductionPunchingParams-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionPunchingParams, CDialog)

CDlgProductionPunchingParams::CDlgProductionPunchingParams(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionPunchingParams::IDD, pParent)
	, m_strPunchToolName(_T(""))
	, m_nPunchToolID(0)
	, m_nPunchToolNX(1)
	, m_nPunchToolNY(1)
	, m_fPunchToolStepX(0)
	, m_fPunchToolStepY(0)
	, m_fPunchToolGutterX(0)
	, m_fPunchToolGutterY(0)
{
	m_nCurSel = -1;
	m_bLocked = FALSE;
	m_ptInitialPos = CPoint(0,0);
}

CDlgProductionPunchingParams::~CDlgProductionPunchingParams()
{
}

int CDlgProductionPunchingParams::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		SetParent(m_pParentWnd);
		ModifyStyle(WS_OVERLAPPEDWINDOW | WS_POPUPWINDOW, WS_CHILD);
		ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);
	}

	return 0;
}

BOOL CDlgProductionPunchingParams::Create(UINT nIDTemplate, CWnd* pParentWnd)
{
	m_pParentWnd = pParentWnd;
	return CDialog::Create(nIDTemplate, pParentWnd);
}

void CDlgProductionPunchingParams::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
		{
			if (m_hWnd)
			{
				if ( ! m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
				{
					m_bLocked = TRUE;
					EndModalLoop(IDCANCEL);
					DestroyWindow();
				}
			}
		}
	}
	else
		m_bLocked = FALSE;
}

void CDlgProductionPunchingParams::OnBnClickedOk()
{
	SaveData();
	EndModalLoop(IDOK);
	m_bLocked = TRUE;
	DestroyWindow();
}

CProductionProfile& CDlgProductionPunchingParams::GetProductionProfile()
{
	static CProductionProfile nullProductionProfile;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile;
	}
	//else
	//	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CProfileSummaryWindow)))
	//	{
	//		CProfileSummaryWindow*	pParent	= (CProfileSummaryWindow*)m_pParentWnd;
	//		return pParent->GetProductionProfile();
	//	}

	return nullProductionProfile;
}

void CDlgProductionPunchingParams::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PUNCHTOOL_LIST, m_punchToolList);
	DDX_Control(pDX, IDC_PUNCHTOOL_NXSPIN, m_punchToolNXSpin);
	DDX_Control(pDX, IDC_PUNCHTOOL_NYSPIN, m_punchToolNYSpin);
	DDX_Text(pDX, IDC_PUNCHTOOL_NAME, m_strPunchToolName);
	DDX_Text(pDX, IDC_PUNCHTOOL_ID, m_nPunchToolID);
	DDX_Text(pDX, IDC_PUNCHTOOL_NX, m_nPunchToolNX);
	DDV_MinMaxUInt(pDX, m_nPunchToolNX, 1, 100);
	DDX_Text(pDX, IDC_PUNCHTOOL_NY, m_nPunchToolNY);
	DDV_MinMaxUInt(pDX, m_nPunchToolNY, 1, 100);
	DDX_Measure(pDX, IDC_PUNCHTOOL_STEPX, m_fPunchToolStepX);
	DDX_Measure(pDX, IDC_PUNCHTOOL_STEPY, m_fPunchToolStepY);
	DDX_Measure(pDX, IDC_PUNCHTOOL_GUTTERX, m_fPunchToolGutterX);
	DDX_Measure(pDX, IDC_PUNCHTOOL_GUTTERY, m_fPunchToolGutterY);
}


BEGIN_MESSAGE_MAP(CDlgProductionPunchingParams, CDialog)
	ON_WM_CREATE()
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDC_PUNCHTOOL_SAVE, &CDlgProductionPunchingParams::OnBnClickedPunchtoolSave)
	ON_UPDATE_COMMAND_UI(IDC_PUNCHTOOL_SAVE,  OnUpdatePunchtoolSave)
	ON_LBN_SELCHANGE(IDC_PUNCHTOOL_LIST, &CDlgProductionPunchingParams::OnLbnSelchangePunchtoolList)
	ON_BN_CLICKED(IDC_PUNCHTOOL_DELETE, &CDlgProductionPunchingParams::OnBnClickedPunchtoolDelete)
	ON_EN_CHANGE(IDC_PUNCHTOOL_NAME, &CDlgProductionPunchingParams::OnEnChangePunchtoolName)
	ON_BN_CLICKED(IDC_PUNCHTOOL_DETAILS, &CDlgProductionPunchingParams::OnBnClickedPunchtoolDetails)
END_MESSAGE_MAP()


// CDlgProductionPunchingParams-Meldungshandler

BOOL CDlgProductionPunchingParams::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_ptInitialPos != CPoint(0,0))
		SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);

	m_punchToolNXSpin.SetRange(0, 100);
	m_punchToolNYSpin.SetRange(0, 100);

	CPunchingProcess& rPunchingProcess = GetProductionProfile().m_postpress.m_punching;

	for (int i = 0; i < rPunchingProcess.m_punchingTools.GetSize(); i++)
	{
		m_punchToolList.AddString(rPunchingProcess.m_punchingTools[i].m_strToolName);
	}

	m_punchToolList.SetCurSel(m_nCurSel = 0);

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProductionPunchingParams::InitData()
{
	CPunchingProcess& rPunchingProcess = GetProductionProfile().m_postpress.m_punching;

	if ( (m_nCurSel >= 0) && (m_nCurSel < rPunchingProcess.m_punchingTools.GetSize()) )
	{
		m_strPunchToolName	= rPunchingProcess.m_punchingTools[m_nCurSel].m_strToolName;
		m_nPunchToolID		= rPunchingProcess.m_punchingTools[m_nCurSel].m_nToolID;
		m_nPunchToolNX		= rPunchingProcess.m_punchingTools[m_nCurSel].m_nPunchesX;
		m_nPunchToolNY		= rPunchingProcess.m_punchingTools[m_nCurSel].m_nPunchesY;
		m_fPunchToolStepX	= rPunchingProcess.m_punchingTools[m_nCurSel].m_fStepX;
		m_fPunchToolStepY	= rPunchingProcess.m_punchingTools[m_nCurSel].m_fStepY;
		m_fPunchToolGutterX	= rPunchingProcess.m_punchingTools[m_nCurSel].m_fGutterX;
		m_fPunchToolGutterY	= rPunchingProcess.m_punchingTools[m_nCurSel].m_fGutterY;
	}
	else
	{
		m_strPunchToolName	= _T("");
		m_nPunchToolID		= 0;
		m_nPunchToolNX		= 1;
		m_nPunchToolNY		= 1;
		m_fPunchToolStepX	= 0.0f;
		m_fPunchToolStepY	= 0.0f;
		m_fPunchToolGutterX	= 0.0f;
		m_fPunchToolGutterY	= 0.0f;
	}

	UpdateData(FALSE);
}

void CDlgProductionPunchingParams::SaveData()
{
	CPunchingProcess& rPunchingProcess = GetProductionProfile().m_postpress.m_punching;

	if ( (m_nCurSel >= 0) && (m_nCurSel < rPunchingProcess.m_punchingTools.GetSize()) )
	{
		UpdateData(TRUE);

		rPunchingProcess.m_punchingTools[m_nCurSel].m_strToolName	= m_strPunchToolName;
		rPunchingProcess.m_punchingTools[m_nCurSel].m_nToolID		= m_nPunchToolID;
		rPunchingProcess.m_punchingTools[m_nCurSel].m_nPunchesX		= m_nPunchToolNX;		
		rPunchingProcess.m_punchingTools[m_nCurSel].m_nPunchesY		= m_nPunchToolNY;		
		rPunchingProcess.m_punchingTools[m_nCurSel].m_fStepX		= m_fPunchToolStepX;
		rPunchingProcess.m_punchingTools[m_nCurSel].m_fStepY		= m_fPunchToolStepY;
		rPunchingProcess.m_punchingTools[m_nCurSel].m_fGutterX		= m_fPunchToolGutterX;
		rPunchingProcess.m_punchingTools[m_nCurSel].m_fGutterY		= m_fPunchToolGutterY;
	}
}

void CDlgProductionPunchingParams::OnLbnSelchangePunchtoolList()
{
	SaveData();

	m_nCurSel = m_punchToolList.GetCurSel();

	InitData();
}

void CDlgProductionPunchingParams::OnBnClickedPunchtoolSave()
{
	UpdateData(TRUE);

	if (m_strPunchToolName.IsEmpty())
		return;

	CPunchingProcess& rPunchingProcess = GetProductionProfile().m_postpress.m_punching;

	rPunchingProcess.m_punchingTools.Add(CPunchingTool());

	m_punchToolList.AddString(m_strPunchToolName);
	m_punchToolList.SetCurSel(m_nCurSel = m_punchToolList.GetCount() - 1);

	SaveData();
}

void CDlgProductionPunchingParams::OnUpdatePunchtoolSave(CCmdUI* pCmdUI)
{
	BOOL bEnable = (m_punchToolList.FindStringExact(-1, m_strPunchToolName) >= 0) ? FALSE : TRUE;

	pCmdUI->Enable(bEnable);
}

void CDlgProductionPunchingParams::OnBnClickedPunchtoolDelete()
{
	UpdateData(TRUE);

	CPunchingProcess& rPunchingProcess = GetProductionProfile().m_postpress.m_punching;

	rPunchingProcess.m_punchingTools.RemoveAt(m_nCurSel);
	m_punchToolList.DeleteString(m_nCurSel);

	if (m_punchToolList.GetCount() > 0)
	{
		if (m_nCurSel >= m_punchToolList.GetCount())
		{
			m_nCurSel--;
			m_punchToolList.SetCurSel(m_nCurSel);
		}
	}
	else
		m_nCurSel = -1;

	InitData();
}

BOOL CDlgProductionPunchingParams::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgProductionPunchingParams::OnEnChangePunchtoolName()
{
	UpdateData(TRUE);

	UpdateDialogControls( this, TRUE );
}

void CDlgProductionPunchingParams::OnBnClickedPunchtoolDetails()
{
	UpdateData(TRUE);

	SaveData();

	CDlgProductionPunchingDetails dlg;
	dlg.m_strPunchToolName	= m_strPunchToolName;

	CPunchingProcess& rPunchingProcess = GetProductionProfile().m_postpress.m_punching;
	if ( (m_nCurSel >= 0) && (m_nCurSel < rPunchingProcess.m_punchingTools.GetSize()) )
		dlg.m_punchingTool = rPunchingProcess.m_punchingTools[m_nCurSel];
	else
		dlg.m_punchingTool = CPunchingTool();

	m_bLocked = TRUE;

	int nRet = dlg.DoModal();
		
	m_bLocked = FALSE;

	if (nRet == IDCANCEL)
		return;

	if ( (m_nCurSel >= 0) && (m_nCurSel < rPunchingProcess.m_punchingTools.GetSize()) )
		rPunchingProcess.m_punchingTools[m_nCurSel] = dlg.m_punchingTool;
	else
	{
		rPunchingProcess.m_punchingTools.Add(dlg.m_punchingTool);

		m_punchToolList.AddString(dlg.m_punchingTool.m_strToolName);
		m_punchToolList.SetCurSel(m_nCurSel = m_punchToolList.GetCount() - 1);
	}

	InitData();
}
