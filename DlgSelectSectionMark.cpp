// DlgSelectSectionMark.cpp: Implementierungsdatei
//
// (C) Tegeler-Software-Engineering 02/05/2000


#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSelectSectionMark 


CDlgSelectSectionMark::CDlgSelectSectionMark(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSelectSectionMark::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSelectSectionMark)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void CDlgSelectSectionMark::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSelectSectionMark)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgSelectSectionMark, CDialog)
	//{{AFX_MSG_MAP(CDlgSelectSectionMark)
	ON_COMMAND(ID_SECTIONMARK_TYPE1, OnSectionmarkType1)
	ON_COMMAND(ID_SECTIONMARK_TYPE2, OnSectionmarkType2)
	ON_COMMAND(ID_SECTIONMARK_TYPE3, OnSectionmarkType3)
	ON_COMMAND(ID_SECTIONMARK_TYPE4, OnSectionmarkType4)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgSelectSectionMark 

BOOL CDlgSelectSectionMark::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_toolBar.Create(WS_VISIBLE | WS_CHILD | CCS_TOP | CCS_NODIVIDER | TBSTYLE_FLAT | TBSTYLE_WRAPABLE, 
					 CRect(0,0,200,200), this, IDR_SECTIONMARK_TYPES);
	
	m_toolBar.SetBitmapSize(CSize(20,20));

	VERIFY(m_toolBar.AddBitmap(4, IDR_SECTIONMARK_TYPES) != -1);

	m_pTBButtons = new TBBUTTON[4];

	for (int nIndex = 0; nIndex < 4; nIndex++)
	{
		m_pTBButtons[nIndex].fsState = TBSTATE_ENABLED;
		m_pTBButtons[nIndex].fsStyle = TBSTYLE_BUTTON;		
		m_pTBButtons[nIndex].dwData  = 0;
		m_pTBButtons[nIndex].iBitmap = nIndex;
		m_pTBButtons[nIndex].iString = NULL;
	}

	m_pTBButtons[0].idCommand = ID_SECTIONMARK_TYPE1;
	m_pTBButtons[1].idCommand = ID_SECTIONMARK_TYPE2;
	m_pTBButtons[2].idCommand = ID_SECTIONMARK_TYPE3;
	m_pTBButtons[3].idCommand = ID_SECTIONMARK_TYPE4;

	VERIFY(m_toolBar.AddButtons(4, m_pTBButtons));

	//m_toolBar.SetRows(2, TRUE, NULL);

	m_toolBar.AutoSize();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
} 

void CDlgSelectSectionMark::OnSectionmarkType1() 
{
	((CDlgSectionMarkContent*)m_pParent)->UpdateSectionMarkType(ID_SECTIONMARK_TYPE1);
	ShowWindow(SW_HIDE);
	((CDlgSectionMarkContent*)m_pParent)->Apply();
	((CDlgSectionMarkContent*)m_pParent)->UpdateView();
}

void CDlgSelectSectionMark::OnSectionmarkType2() 
{
	((CDlgSectionMarkContent*)m_pParent)->UpdateSectionMarkType(ID_SECTIONMARK_TYPE2);
	ShowWindow(SW_HIDE);
	((CDlgSectionMarkContent*)m_pParent)->Apply();
	((CDlgSectionMarkContent*)m_pParent)->UpdateView();
}

void CDlgSelectSectionMark::OnSectionmarkType3() 
{
	((CDlgSectionMarkContent*)m_pParent)->UpdateSectionMarkType(ID_SECTIONMARK_TYPE3);
	ShowWindow(SW_HIDE);
	((CDlgSectionMarkContent*)m_pParent)->Apply();
	((CDlgSectionMarkContent*)m_pParent)->UpdateView();
}

void CDlgSelectSectionMark::OnSectionmarkType4() 
{
	((CDlgSectionMarkContent*)m_pParent)->UpdateSectionMarkType(ID_SECTIONMARK_TYPE4);
	ShowWindow(SW_HIDE);
	((CDlgSectionMarkContent*)m_pParent)->Apply();
	((CDlgSectionMarkContent*)m_pParent)->UpdateView();
}

BOOL CDlgSelectSectionMark::PreTranslateMessage(MSG* pMsg) 
{
	// If we get a keyboard or mouse message, hide the toolbar.
	if (pMsg->message == WM_LBUTTONDOWN || pMsg->message == WM_RBUTTONDOWN || pMsg->message == WM_MBUTTONDOWN ||
	    pMsg->message == WM_NCLBUTTONDOWN || pMsg->message == WM_NCRBUTTONDOWN || pMsg->message == WM_NCMBUTTONDOWN)
	{
		CPoint ptMouse;
		GetCursorPos(&ptMouse);
		CRect rect;
		GetWindowRect(rect);
		if ( ! rect.PtInRect(ptMouse))
		{
			ShowWindow(SW_HIDE);
			return TRUE;	// message handled here
		}
	}
	else
		if (pMsg->message == WM_KEYDOWN || pMsg->message == WM_SYSKEYDOWN)
		{
			ShowWindow(SW_HIDE);
			return TRUE;	// message handled here
		}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgSelectSectionMark::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	if (bShow)
	{
		m_toolBar.SetCapture();

		CSize sizeMax;
		CRect rect;
		m_toolBar.GetMaxSize(&sizeMax);
		GetWindowRect(rect);
		MoveWindow(rect.left, rect.top, sizeMax.cx + 6, sizeMax.cy + 6, FALSE);
	}
	else
	{
		ReleaseCapture();
		((CButton*)m_pParent->GetDlgItem(IDC_SELECT_SECTION_MARK))->SetCheck(0);
		m_pParent->SetFocus();
	}

	CDialog::OnShowWindow(bShow, nStatus);
}
