// InplaceEdit.cpp : implementation file
//

#include "stdafx.h"
#include "DisplayList.h"
#include "InplaceEdit.h"
#include "MeasureControl.h"		// special data exchange for measurement edit controls


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// CInplaceEdit

CInplaceEdit::CInplaceEdit()
{
	m_nGrowTo				  = FixedSize;
	m_lParam				  = 0;
	m_pBitmap				  = NULL;
	m_bKillFocusHandlerLocked = FALSE;
	m_di					  = CDisplayItem();
}

CInplaceEdit::~CInplaceEdit()
{
	m_Font.DeleteObject();
	if (m_pBitmap)
	{
		m_pBitmap->DeleteObject();
		delete m_pBitmap;
		m_pBitmap = NULL;
	}
}

BOOL CInplaceEdit::Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID )
{
	if (CEdit::Create(dwStyle, rect, pParentWnd, nID) == FALSE)
		return FALSE;
	if ( ! m_Font.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma")))
		return FALSE;
	SetFont(&m_Font, FALSE);

	return TRUE;
}

void CInplaceEdit::Activate(CRect rect, const CString& rString, int nGrowTo, LPARAM lParam, CDisplayItem di) 
{
	m_nGrowTo = nGrowTo;
	m_lParam  = lParam;
	m_di	  = di;

	if (!IsWindowVisible())
	{
		SaveBackground(rect);
		GetParent()->SetFocus();
		ShowWindow(SW_HIDE);
		MoveWindow(rect, FALSE);	
		SetWindowText(rString);
		ShowWindow(SW_SHOW);	
		UpdateWindow();
	}
	SetSel(0, -1, FALSE);
	SetFocus();
	GetParent()->ValidateRect(rect);
}

void CInplaceEdit::Move(CRect newRect, const CString& rString, LPARAM lParam) 
{
	m_lParam = lParam;

	if (!IsWindowVisible())
		return;

	CRect oldRect;
	GetWindowRect(oldRect);
	GetParent()->ScreenToClient(oldRect);

	LockKillFocusHandler();
	GetParent()->SetFocus();
	ShowWindow(SW_HIDE);
	RestoreBackground(oldRect);
	SaveBackground(newRect);
	MoveWindow(newRect, FALSE);	
	SetWindowText(rString);
	ShowWindow(SW_SHOW);
	UpdateWindow();
	SetFocus();
	SetSel(0, -1, FALSE);
	GetParent()->ValidateRect(newRect);
	UnlockKillFocusHandler();
}

void CInplaceEdit::Deactivate() 
{
	if (!IsWindowVisible())
		return;

	LockKillFocusHandler();
//	GetParent()->SetFocus();
	ShowWindow(SW_HIDE); // make invisible	

	if (m_pBitmap)
	{
		CRect oldRect;
		GetWindowRect(oldRect);
		GetParent()->ScreenToClient(oldRect);
		RestoreBackground(oldRect);
	}

	UnlockKillFocusHandler();
}

void CInplaceEdit::SaveBackground(const CRect& rect)
{
	if (m_pBitmap)
	{
		m_pBitmap->DeleteObject();
		delete m_pBitmap;
	}
	m_pBitmap = new CBitmap;

 	CDC* pDC = GetParent()->GetDC(); 
	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	m_pBitmap->CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	GetParent()->ReleaseDC(pDC);
}

void CInplaceEdit::RestoreBackground(const CRect& rect)
{
	if (!m_pBitmap)
		return;

	CDC* pDC = GetParent()->GetDC(); 
	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBitmap);
	pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	GetParent()->ReleaseDC(pDC);

	m_pBitmap->DeleteObject();
	delete m_pBitmap;
	m_pBitmap = NULL;

	GetParent()->ValidateRect(rect);
}

CString CInplaceEdit::GetString() 
{
	CString string;
	GetWindowText(string);
	return string;
}

float CInplaceEdit::GetFloat() 
{
	CString string;
	GetWindowText(string);

	float fValue;
	GetMeasure(0, fValue, string);

	return fValue;
}


BEGIN_MESSAGE_MAP(CInplaceEdit, CEdit)
	//{{AFX_MSG_MAP(CInplaceEdit)
	ON_WM_CHAR()
	ON_CONTROL_REFLECT(EN_UPDATE, OnUpdate)
	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInplaceEdit message handlers


// If not FixedSize modify length of control
void CInplaceEdit::OnUpdate() 
{
	if (m_nGrowTo == FixedSize)
		return;

	if (!IsWindowVisible())
		return;

// Get required text extent for new string
	CDC*   pDC		 = GetDC();
	CFont* pOldFont  = pDC->SelectObject(&m_Font);
	CSize textExtent = pDC->GetTextExtent(GetString());	
	pDC->SelectObject(pOldFont);
	ReleaseDC(pDC);

	textExtent.cx = max(textExtent.cx, 30);

	CRect oldRect;
	GetWindowRect(oldRect);
	GetParent()->ScreenToClient(&oldRect);
	CRect newRect = oldRect;
	if (m_nGrowTo == GrowToRight)
		newRect.right = newRect.left  + (textExtent.cx + 10);
	else
		newRect.left  = newRect.right - (textExtent.cx + 10);

	LockKillFocusHandler();
	GetParent()->SetFocus();
	ShowWindow(SW_HIDE);
	MoveWindow(newRect, FALSE);	
	RestoreBackground(oldRect);
	SaveBackground(newRect);
	ShowWindow(SW_SHOW);
	UpdateWindow();
	SetFocus();
	GetParent()->ValidateRect(newRect);
	UnlockKillFocusHandler();
}

void CInplaceEdit::OnKillFocus(CWnd* pNewWnd) 
{
	if (!IsWindowVisible())
		return;

	if (!m_bKillFocusHandlerLocked)
	{
		Deactivate();

		if (m_pFnKillFocusHandler)
			m_pFnKillFocusHandler(GetParent());
	}

	CEdit::OnKillFocus(pNewWnd);
}

void CInplaceEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
	{
	case VK_TAB:	
	case VK_RETURN:	
	case VK_UP:		
	case VK_DOWN:	if (m_pFnOnCharHandler) m_pFnOnCharHandler(GetParent(), nChar);	break;
	case VK_ESCAPE:	Undo();	GetParent()->SetFocus();		
					if (m_pFnOnCharHandler) m_pFnOnCharHandler(GetParent(), nChar);	break;	// Undo and leave 
	default:		CEdit::OnChar(nChar, nRepCnt, nFlags);	break;
	}
}

BOOL CInplaceEdit::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if ((pMsg->wParam == VK_ESCAPE) || (pMsg->wParam == VK_UP) || (pMsg->wParam == VK_DOWN))
		{
			OnChar(pMsg->wParam, 1, 0);
			return TRUE;
		}
	
	return CEdit::PreTranslateMessage(pMsg);
}

