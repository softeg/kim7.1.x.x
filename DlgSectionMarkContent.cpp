// DlgSectionMarkContent.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgSelectSectionMark.h"
#include "DlgLayoutObjectControl.h"
#include "DlgSectionMarkContent.h"
#include "GraphicComponent.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSectionMarkContent 


CDlgSectionMarkContent::CDlgSectionMarkContent(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSectionMarkContent::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSectionMarkContent)
	m_fBarWidth = 0.0f;
	m_fBarHeight = 0.0f;
	//}}AFX_DATA_INIT

	m_pParent = NULL;
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
}

CDlgSectionMarkContent::~CDlgSectionMarkContent()
{
	m_hollowBrush.DeleteObject();
}

BOOL CDlgSectionMarkContent::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}	

void CDlgSectionMarkContent::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSectionMarkContent)
	DDX_Control(pDX, IDC_SECTIONMARK_WIDTHSPIN, m_barWidthSpin);
	DDX_Control(pDX, IDC_SECTIONMARK_HEIGHTSPIN, m_barHeightSpin);
	DDX_Control(pDX, IDC_SECTIONMARK_TYPE, m_sectionMarkTypeBitmap);
	DDX_Measure(pDX, IDC_SECTIONMARK_HEIGHT, m_fBarWidth);
	DDX_Measure(pDX, IDC_SECTIONMARK_WIDTH, m_fBarHeight);
	//}}AFX_DATA_MAP
//	DDX_Control(pDX, IDC_SELECT_SECTION_MARK, m_selectSectionMarkButton);
}


BEGIN_MESSAGE_MAP(CDlgSectionMarkContent, CDialog)
	//{{AFX_MSG_MAP(CDlgSectionMarkContent)
	ON_BN_CLICKED(IDC_SELECT_SECTION_MARK, OnSelectSectionMark)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SECTIONMARK_HEIGHTSPIN, OnDeltaposSectionmarkHeightspin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SECTIONMARK_WIDTHSPIN, OnDeltaposSectionmarkWidthspin)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
	ON_WM_MEASUREITEM()
	ON_WM_DRAWITEM()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgSectionMarkContent 

BOOL CDlgSectionMarkContent::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_selectSectionMarkButton.SubclassDlgItem(IDC_SELECT_SECTION_MARK, this);
	m_selectSectionMarkButton.SetTheme(_T("COMBOBOX"), CP_DROPDOWNBUTTON, XCENTER);
	m_selectSectionMarkButton.SetBitmap(IDB_DROPDOWN, IDB_DROPDOWN, -1, XCENTER);
	
	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	m_printColorControl.Create(this, m_pParent);
	
	CRect controlRect, frameRect;
	m_printColorControl.GetWindowRect(controlRect);
	GetDlgItem(IDC_COLORCONTROL_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	m_printColorControl.SetWindowPos(NULL, frameRect.left, frameRect.top, controlRect.Width(), controlRect.Height(), SWP_NOZORDER);


	m_barWidthSpin.SetRange(0, 1);
	m_barHeightSpin.SetRange(0, 1);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgSectionMarkContent::OnSelectSectionMark() 
{
	CImage img1, img2, img3, img4;
	img1.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_SECTIONMARK_TYPE1));	
	img2.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_SECTIONMARK_TYPE2));	
	img3.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_SECTIONMARK_TYPE3));	
	img4.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_SECTIONMARK_TYPE4));	

	CMenu menu, menuPopup;
	menu.CreateMenu();
	menuPopup.CreatePopupMenu();
	menuPopup.AppendMenu(MF_STRING, ID_SECTIONMARK_TYPE1, _T(""));		
	menuPopup.AppendMenu(MF_STRING, ID_SECTIONMARK_TYPE2, _T(""));
	menuPopup.AppendMenu(MF_STRING, ID_SECTIONMARK_TYPE3, _T(""));
	menuPopup.AppendMenu(MF_STRING, ID_SECTIONMARK_TYPE4, _T(""));
	menu.AppendMenu(MF_POPUP, (UINT)menuPopup.m_hMenu, _T(""));

	MENUITEMINFO mi;
	mi.cbSize = sizeof(MENUITEMINFO);
	mi.fMask = MIIM_TYPE | MIIM_DATA;
	mi.fType = MFT_OWNERDRAW;
	mi.dwItemData = (ULONG_PTR)menu.m_hMenu;
	//     Making the menu item ownerdrawn:
	menuPopup.SetMenuItemInfo(ID_SECTIONMARK_TYPE1,	&mi);
	menuPopup.SetMenuItemInfo(ID_SECTIONMARK_TYPE2,	&mi);
	menuPopup.SetMenuItemInfo(ID_SECTIONMARK_TYPE3,	&mi);
	menuPopup.SetMenuItemInfo(ID_SECTIONMARK_TYPE4,	&mi);

	CRect rcButton;
	GetDlgItem(IDC_SELECT_SECTION_MARK)->GetWindowRect(rcButton);

	int nRet = menuPopup.TrackPopupMenuEx(TPM_RETURNCMD | TPM_NONOTIFY | TPM_LEFTALIGN | TPM_TOPALIGN | TPM_RIGHTBUTTON, rcButton.left, rcButton.bottom, this, NULL);
	menu.DestroyMenu();
	if (nRet <= 0)
		return;

	UpdateSectionMarkType(nRet);
}

void CDlgSectionMarkContent::UpdateSectionMarkType(int nIDSectionMarkType) 
{
	Invalidate();

	int nIDBitmap = -1;
	switch (m_nIDSectionMarkType = nIDSectionMarkType)
	{
	case IDCANCEL:												  return;
	case ID_SECTIONMARK_TYPE1: nIDBitmap = IDB_SECTIONMARK_TYPE1; break;
	case ID_SECTIONMARK_TYPE2: nIDBitmap = IDB_SECTIONMARK_TYPE2; break;
	case ID_SECTIONMARK_TYPE3: nIDBitmap = IDB_SECTIONMARK_TYPE3; break;
	case ID_SECTIONMARK_TYPE4: nIDBitmap = IDB_SECTIONMARK_TYPE4; break;
	}

	if ((HBITMAP)m_bmSectionMarkType)
		m_bmSectionMarkType.DeleteObject();
	m_bmSectionMarkType.LoadBitmap(nIDBitmap); 
	m_sectionMarkTypeBitmap.SetBitmap((HBITMAP)m_bmSectionMarkType);
}

void CDlgSectionMarkContent::OnDeltaposSectionmarkWidthspin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_barWidthSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgSectionMarkContent::OnDeltaposSectionmarkHeightspin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_barHeightSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgSectionMarkContent::PostNcDestroy() 
{
	m_bmSectionMarkType.DeleteObject();
	
	CDialog::PostNcDestroy();
}

void CDlgSectionMarkContent::LoadData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::SectionMark)
		return;

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				if (pObjectSource)
				{
					int	nIndex = pObjectSource->m_strSystemCreationInfo.Find(_T(" "));
					if (nIndex != -1)	// invalid string
					{
						CString strContentType = pObjectSource->m_strSystemCreationInfo.Left(nIndex);
						CString strParams	   = pObjectSource->m_strSystemCreationInfo.Right(pObjectSource->m_strSystemCreationInfo.GetLength() - nIndex);
						strParams.TrimLeft(); strParams.TrimRight();

						if (strContentType == _T("$SECTION_BAR"))
						{
							BOOL bNumRotated180 = (strParams.Find(_T("NUMROTATED:180")) == -1) ? FALSE : TRUE;
							BOOL bHollow		= (strParams.Find(_T("HOLLOW"))			== -1) ? FALSE : TRUE;

							if (! bNumRotated180 && ! bHollow) m_nIDSectionMarkType = ID_SECTIONMARK_TYPE1;
							else
							if (  bNumRotated180 && ! bHollow) m_nIDSectionMarkType = ID_SECTIONMARK_TYPE2;
							else
							if (! bNumRotated180 &&   bHollow) m_nIDSectionMarkType = ID_SECTIONMARK_TYPE3;
							else
							if (  bNumRotated180 &&   bHollow) m_nIDSectionMarkType = ID_SECTIONMARK_TYPE4;
							else
								m_nIDSectionMarkType = ID_SECTIONMARK_TYPE1;

							UpdateSectionMarkType(m_nIDSectionMarkType);

							Ascii2Float((LPCTSTR)strParams, _T("%f %f"), &m_fBarWidth, &m_fBarHeight);

							m_printColorControl.LoadData(pTemplate, pObjectSource, nObjectSourceIndex);
						}
					}
				}
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
		if (nIndex != -1)	// invalid string
		{
			CString strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);
			CString strParams	   = pMark->m_MarkSource.m_strSystemCreationInfo.Right(pMark->m_MarkSource.m_strSystemCreationInfo.GetLength() - nIndex);
			strParams.TrimLeft(); strParams.TrimRight();

			if (strContentType == _T("$SECTION_BAR"))
			{
				BOOL bNumRotated180 = (strParams.Find(_T("NUMROTATED:180")) == -1) ? FALSE : TRUE;
				BOOL bHollow		= (strParams.Find(_T("HOLLOW"))		 == -1) ? FALSE : TRUE;

				if (! bNumRotated180 && ! bHollow) m_nIDSectionMarkType = ID_SECTIONMARK_TYPE1;
				else
				if (  bNumRotated180 && ! bHollow) m_nIDSectionMarkType = ID_SECTIONMARK_TYPE2;
				else
				if (! bNumRotated180 &&   bHollow) m_nIDSectionMarkType = ID_SECTIONMARK_TYPE3;
				else
				if (  bNumRotated180 &&   bHollow) m_nIDSectionMarkType = ID_SECTIONMARK_TYPE4;
				else
					m_nIDSectionMarkType = ID_SECTIONMARK_TYPE1;

				UpdateSectionMarkType(m_nIDSectionMarkType);

				Ascii2Float((LPCTSTR)strParams, _T("%f %f"), &m_fBarWidth, &m_fBarHeight);

				m_printColorControl.LoadData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
			}
		}
	}

	UpdateData(FALSE);
}

void CDlgSectionMarkContent::SaveData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::SectionMark)
		return;

	UpdateData(TRUE);

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;

				pObjectSource->m_nType = CPageSource::SystemCreated;
				switch(m_nIDSectionMarkType)
				{
				case ID_SECTIONMARK_TYPE1:	pObjectSource->m_strSystemCreationInfo.Format(_T("$SECTION_BAR %.2f %.2f"),						
																							m_fBarWidth, m_fBarHeight); 
																							break;
				case ID_SECTIONMARK_TYPE2:	pObjectSource->m_strSystemCreationInfo.Format(_T("$SECTION_BAR %.2f %.2f NUMROTATED:180"),			
																							m_fBarWidth, m_fBarHeight); 
																							break;
				case ID_SECTIONMARK_TYPE3:	pObjectSource->m_strSystemCreationInfo.Format(_T("$SECTION_BAR %.2f %.2f HOLLOW"),					
																							m_fBarWidth, m_fBarHeight); 
																							break;
				case ID_SECTIONMARK_TYPE4:	pObjectSource->m_strSystemCreationInfo.Format(_T("$SECTION_BAR %.2f %.2f NUMROTATED:180 HOLLOW"),	
																							m_fBarWidth, m_fBarHeight); 
																							break;
				}
				if (m_pParent->m_dlgLayoutObjectGeometry.m_hWnd)
				{
					if ( (pLayoutObject->m_nHeadPosition == LEFT) || (pLayoutObject->m_nHeadPosition == RIGHT) )
						m_pParent->m_dlgLayoutObjectGeometry.m_fObjectHeight = m_fBarWidth;
					else
						m_pParent->m_dlgLayoutObjectGeometry.m_fObjectWidth  = m_fBarWidth;
					m_pParent->m_dlgLayoutObjectGeometry.UpdateData(FALSE);
					m_pParent->m_dlgLayoutObjectGeometry.SaveData();
				}

				m_printColorControl.SaveData(pTemplate, pObjectSource, nObjectSourceIndex);
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		if (pMark->m_nType == CMark::SectionMark)
		{
			CSectionMark* pSectionMark = (CSectionMark*)pMark;
			pSectionMark->m_MarkSource.m_nType = CPageSource::SystemCreated;
			switch(m_nIDSectionMarkType)
			{
			case ID_SECTIONMARK_TYPE1:	pSectionMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$SECTION_BAR %.2f %.2f"),						
																							m_fBarWidth, m_fBarHeight); 
																							break;
			case ID_SECTIONMARK_TYPE2:	pSectionMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$SECTION_BAR %.2f %.2f NUMROTATED:180"),			
																							m_fBarWidth, m_fBarHeight); 
																							break;
			case ID_SECTIONMARK_TYPE3:	pSectionMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$SECTION_BAR %.2f %.2f HOLLOW"),					
																							m_fBarWidth, m_fBarHeight); 
																							break;
			case ID_SECTIONMARK_TYPE4:	pSectionMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$SECTION_BAR %.2f %.2f NUMROTATED:180 HOLLOW"),	
																							m_fBarWidth, m_fBarHeight); 
																							break;
			}
			pSectionMark->m_fBarWidth   = m_fBarWidth;
			pSectionMark->m_fBarHeight  = m_fBarHeight;
			pSectionMark->m_fMarkWidth  = m_fBarWidth;
			pSectionMark->m_fMarkHeight = m_fBarHeight;

			m_printColorControl.SaveData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
		}
	}
}

void CDlgSectionMarkContent::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}

void CDlgSectionMarkContent::UpdateView()
{
	m_pParent->UpdateView();
}

void CDlgSectionMarkContent::OnPaint() 
{
	CDialog::OnPaint();

	m_printColorControl.Invalidate();
	m_printColorControl.UpdateWindow();
}

void CDlgSectionMarkContent::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct->CtlType == ODT_MENU)
	{
		//int	nMaxLen = 0;
		//if(IsMenu((HMENU)lpMeasureItemStruct->itemData))
		//{
		//	CMenu* pMenu = CMenu::FromHandle((HMENU)lpMeasureItemStruct->itemData);
		//	if (pMenu)
		//	{
		//		CString strText;
		//		pMenu->GetMenuStringA(lpMeasureItemStruct->itemID, strText, MF_BYCOMMAND);
		//		CClientDC dc(this);
		//		dc.SelectObject(GetFont());
		//		nMaxLen = dc.GetTextExtent(strText).cx + 40;
		//	}
		//}
		lpMeasureItemStruct->itemWidth  = 25;//nMaxLen;
		lpMeasureItemStruct->itemHeight = 30;
	}
	else
		CDialog::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CDlgSectionMarkContent::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType == ODT_MENU)
	{
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		CRect rcItem = lpDrawItemStruct->rcItem;

		pDC->FillSolidRect(rcItem, RGB(252, 252, 249));//CREME);

		CRect rcIcon = rcItem;
		rcIcon.right = rcIcon.left + 45;

		if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			CGraphicComponent gc;
			gc.DrawTransparentFrame(pDC, rcItem, RGB(120,160,220), 2);
		}				

		if ( ! (lpDrawItemStruct->itemState & MF_DISABLED) )
		{
			int nID = -1;
			switch (lpDrawItemStruct->itemID)
			{
			case ID_SECTIONMARK_TYPE1:	nID = IDB_SECTIONMARK_TYPE1; break;
			case ID_SECTIONMARK_TYPE2:	nID = IDB_SECTIONMARK_TYPE2; break;
			case ID_SECTIONMARK_TYPE3:	nID = IDB_SECTIONMARK_TYPE3; break;
			case ID_SECTIONMARK_TYPE4:	nID = IDB_SECTIONMARK_TYPE4; break;
			}
			CImage img;
			img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(nID));	
			CRect rcDst(CPoint(0, 0), CSize(img.GetWidth(), img.GetHeight()));	
			img.TransparentBlt(pDC->m_hDC, rcIcon.left + 15, rcIcon.top + 5, img.GetWidth()/3*2, img.GetHeight()/3*2, rcDst.left, rcDst.top, rcDst.Width(), rcDst.Height(), RGB(244,244,238));
		}

		pDC->SelectObject(GetFont());
		pDC->SetBkMode(TRANSPARENT);
		CRect rcText = rcItem;
		rcText.left = rcIcon.right + 5;
		if(IsMenu((HMENU)lpDrawItemStruct->hwndItem))
		{
			CMenu* pMenu = CMenu::FromHandle((HMENU)lpDrawItemStruct->hwndItem);
			if (pMenu)
			{
				CString strText;
				pMenu->GetMenuString(lpDrawItemStruct->itemID, strText, MF_BYCOMMAND);
				if (lpDrawItemStruct->itemState & MF_DISABLED)
					pDC->SetTextColor(LIGHTGRAY);
				else
					pDC->SetTextColor(DARKBLUE);
				pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
			}
		}
	}
	else
		CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
