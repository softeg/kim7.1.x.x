// PageListTableView.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "PageListFrame.h"
#include "PageListTableView.h"
#include "PageListView.h"
#include "BookBlockView.h"
#include "PageListTopToolsView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// CPageListTableView

IMPLEMENT_DYNCREATE(CPageListTableView, CScrollView)

CPageListTableView::CPageListTableView()
{
	m_DisplayList.Create(this, FALSE, DISPLAY_LIST_TYPE(CPageListTableView));
	m_nViewBorder	= 0;
	m_nStartIndex	= 0;
	m_nCurPrintPage = 0;
}

CPageListTableView::~CPageListTableView()
{
	m_HeaderFont.DeleteObject();
}


BEGIN_MESSAGE_MAP(CPageListTableView, CScrollView)
	//{{AFX_MSG_MAP(CPageListTableView)
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_WM_SETCURSOR()
	ON_WM_CONTEXTMENU()
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
	ON_NOTIFY(HDN_TRACK,	IDC_PAGELISTHEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK, IDC_PAGELISTHEADER, OnHeaderEndTrack)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageListTableView drawing

void CPageListTableView::OnInitialUpdate()
{
	CPaintDC dc(this);

	TEXTMETRIC	textmetrics;
	dc.GetTextMetrics(&textmetrics);
	m_nCharHeight = textmetrics.tmHeight;
	m_nCharWidth  = textmetrics.tmMaxCharWidth;

	m_nViewBorder = 10;

	m_HeaderFont.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));

	m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, IDC_PAGELISTHEADER);	
	m_HeaderCtrl.SetFont(&m_HeaderFont, FALSE);

	CString string; 

	HD_ITEM hdi;
	hdi.mask		= HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
	hdi.fmt			= HDF_LEFT | HDF_STRING;

	string.LoadString(IDS_PAGELISTHEADER_INDEX);
	hdi.pszText		= string.GetBuffer(0);
	hdi.cxy			= m_nHeaderItemWidthIndex = 50;
	hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
	m_HeaderCtrl.InsertItem(0, &hdi);

	string.LoadString(IDS_PAGELISTHEADER_PAGENAME);
	hdi.pszText		= string.GetBuffer(0);
	hdi.cxy			= m_nHeaderItemWidthPageName = 50;
	hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
	m_HeaderCtrl.InsertItem(1, &hdi);

	string.LoadString(IDS_PAGELISTHEADER_PSPATH);
	hdi.pszText		= string.GetBuffer(0);
	hdi.cxy			= m_nHeaderItemWidthPSpath = 300;
	hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
	m_HeaderCtrl.InsertItem(2, &hdi);

	string.LoadString(IDS_PAGELISTHEADER_FORMAT);
	hdi.pszText		= string.GetBuffer(0);
	hdi.cxy			= m_nHeaderItemWidthFormat = 100;
	hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
	m_HeaderCtrl.InsertItem(3, &hdi);

	string.LoadString(IDS_PAGELISTHEADER_OFFSET);
	hdi.pszText		= string.GetBuffer(0);
	hdi.cxy			= m_nHeaderItemWidthOffset = 100;
	hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
	m_HeaderCtrl.InsertItem(4, &hdi);

	string.LoadString(IDS_PAGELISTHEADER_SCALE);
	hdi.pszText		= string.GetBuffer(0);
	hdi.cxy			= m_nHeaderItemWidthScale  = 100;
	hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
	m_HeaderCtrl.InsertItem(5, &hdi);

	m_nHeaderItemsTotalWidth = m_nHeaderItemWidthIndex +  m_nHeaderItemWidthPageName + m_nHeaderItemWidthPSpath + 
							   m_nHeaderItemWidthFormat + m_nHeaderItemWidthOffset + m_nHeaderItemWidthScale;

	OnSize(0, 0, 0);
	
	ModifyStyle(0, WS_CLIPCHILDREN);	// Clip header control 

	m_bInitialUpdate = TRUE;

	CScrollView::OnInitialUpdate();
}

void CPageListTableView::OnSize(UINT nType, int cx, int cy) 
{
	CScrollView::OnSize(nType, cx, cy);

	if (m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdl;
		WINDOWPOS wp;
		hdl.prc = &clientRect;
		hdl.pwpos = &wp;
		m_HeaderCtrl.Layout(&hdl);	
		m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_nHeaderHeight = wp.cy;
	} 
}

// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
BOOL CPageListTableView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect;
		GetClientRect(scrollRect);
		scrollRect.top+= m_nHeaderHeight;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect);

		if (sizeScroll.cx != 0)				// Scroll header control horizontally
			m_HeaderCtrl.MoveWindow(-x, 0, scrollRect.Width() + x, m_nHeaderHeight);
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

void CPageListTableView::CalcTemplateListSize()
{
	m_TemplateListSize = CSize(m_nHeaderItemsTotalWidth, 0);

	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return;
	if (pDoc->m_PageTemplateList.IsEmpty())
		return;

	POSITION pos = pDoc->m_PageTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = pDoc->m_PageTemplateList.CList::GetNext(pos);
		if ( ! pos && rTemplate.m_nMultipageGroupNum >= 0)	// double page at end -> don't draw
			;
		else
		{
			int	nNumColorants = rTemplate.GetTotalNumColorants();
			m_TemplateListSize.cy+= (nNumColorants == 0) ? 2 * m_nCharHeight : (nNumColorants + 1) * m_nCharHeight;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPageListTableView printing
BOOL CPageListTableView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return DoPreparePrinting(pInfo);
}

void CPageListTableView::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
	if (pInfo->m_nCurPage == 1)
		m_nStartIndex = 0;

	m_nCurPrintPage = pInfo->m_nCurPage;

	OnDraw(pDC);

	CImpManDoc* pDoc = GetDocument();
	int nCount = (pDoc) ? pDoc->m_PageTemplateList.GetCount() : 0;
	if (m_nStartIndex > nCount)
		pInfo->m_bContinuePrinting = FALSE;
}

void CPageListTableView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CPageListTableView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}
/////////////////////////////////////////////////////////////////////////////


void CPageListTableView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	//if (pInfo != NULL)
	//{
	//	m_rcPrintMargins = CBookBlockView::GetPrinterMargins(pDC);

	//	CClientDC dcScreen(this);
	//	m_fScreenPrinterRatioX = (float)pDC->GetDeviceCaps(LOGPIXELSX) / (float)dcScreen.GetDeviceCaps(LOGPIXELSX);
	//	m_fScreenPrinterRatioY = (float)pDC->GetDeviceCaps(LOGPIXELSY) / (float)dcScreen.GetDeviceCaps(LOGPIXELSY);

	//	pDC->SetWindowExt(m_TemplateListSize - CSize((m_nViewBorder*3)/2, (m_nViewBorder*3)/2));
	//	pDC->SetWindowOrg(m_nViewBorder, m_nViewBorder);

	//	pDC->SetViewportExt( pDC->GetDeviceCaps(HORZRES) - m_rcPrintMargins.left   - m_rcPrintMargins.right, 
	//						 pDC->GetDeviceCaps(VERTRES) - m_rcPrintMargins.bottom - m_rcPrintMargins.top);         
	//	pDC->SetViewportOrg(m_rcPrintMargins.left, m_rcPrintMargins.top);
	//}
	//else
	{
		CScrollView::OnPrepareDC(pDC, pInfo);

		m_fScreenPrinterRatioX = 1;
		m_fScreenPrinterRatioY = 1;
	}
}

void CPageListTableView::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = GetDocument();
	if (pDoc->m_PageTemplateList.IsEmpty())
		return;

	// Calculate columns
#ifdef PDF
	m_p[0]  = CPoint(0, 0);
	m_p[1]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() - (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[2]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[3]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() - (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[4]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[5]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() - (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[6]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[7]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + GetHeaderItemWidthFormat() - (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[8]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + GetHeaderItemWidthFormat() + (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[9]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + GetHeaderItemWidthFormat() + GetHeaderItemWidthOffset() - (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[10] = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + GetHeaderItemWidthFormat() + GetHeaderItemWidthOffset() + (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[11] = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + GetHeaderItemWidthFormat() + GetHeaderItemWidthOffset() + GetHeaderItemWidthScale() - (int)(5 * m_fScreenPrinterRatioX), 0);
#else
	m_p[0]  = CPoint(0, 0);
	m_p[1]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() - (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[2]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[3]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() - (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[4]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[5]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() - (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[6]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[7]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + GetHeaderItemWidthFormat() - (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[8]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + GetHeaderItemWidthFormat() + (int)(5 * m_fScreenPrinterRatioX), 0);
	m_p[9]  = CPoint(m_p[0].x + GetHeaderItemWidthIndex() + GetHeaderItemWidthPageName() + GetHeaderItemWidthPSpath() + GetHeaderItemWidthFormat() + GetHeaderItemWidthOffset() - (int)(5 * m_fScreenPrinterRatioX), 0);
#endif

	if (pDC->IsPrinting())
	{
		CFont  bigFont;
		CFont* pOldFont;
		bigFont.CreateFont	 (26*(int)m_fScreenPrinterRatioX, 0, 0, 0, FW_BOLD,	 FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		pOldFont = pDC->SelectObject(&bigFont);

		int	nOldBkMode = pDC->SetBkMode(TRANSPARENT);

		int nMaxRight = pDC->GetDeviceCaps(HORZRES) - m_rcPrintMargins.left - m_rcPrintMargins.right;
		pDC->FillSolidRect(0, 0, nMaxRight, 40 * (int)m_fScreenPrinterRatioY, DARKBLUE);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor((pDC->IsPrinting()) ? WHITE : RGB(170,170,255));

		CString strOut;
		int	nIndex = pDoc->GetTitle().Find(_T(".job"));
		if (nIndex != -1)
			strOut = pDoc->GetTitle().Left(nIndex);
		else
			strOut = pDoc->GetTitle();
		pDC->TextOut(10 * (int)m_fScreenPrinterRatioX, 5 * (int)m_fScreenPrinterRatioY, strOut);

		if (m_nCurPrintPage > 1)
		{
			CFont smallFont;
			smallFont.CreateFont (12*(int)m_fScreenPrinterRatioX, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
			pDC->SelectObject(&smallFont);
			strOut.Format(IDS_PAGENUM, m_nCurPrintPage);
			CSize sizeExtent = pDC->GetTextExtent(strOut);
			pDC->TextOut(nMaxRight - sizeExtent.cx - 10 * (int)m_fScreenPrinterRatioX, 20 * (int)m_fScreenPrinterRatioY - sizeExtent.cy/2, strOut);
			pDC->SelectObject(pOldFont);
			smallFont.DeleteObject();
		}

		pDC->SetBkMode(nOldBkMode);
		pDC->SelectObject(pOldFont);
		bigFont.DeleteObject();
	}

	CFont* pOldFont = pDC->GetCurrentFont();
	CFont textFont;
	textFont.CreatePointFont(80, _T("MS Sans Serif"), pDC); //Arial	 MS Sans Serif	 Times New Roman
	pDC->SelectObject(&textFont);

	if (pDC->IsPrinting())
	{
		int nMaxRight = pDC->GetDeviceCaps(HORZRES) - m_rcPrintMargins.left - m_rcPrintMargins.right;
		pDC->FillSolidRect(0, 40 * (int)m_fScreenPrinterRatioY, nMaxRight, 20 * (int)m_fScreenPrinterRatioY, LIGHTGRAY);
		int		 nOldBkMode		= pDC->SetBkMode(TRANSPARENT);
		COLORREF crOldTextColor = pDC->SetTextColor(BLACK);
		CString string;
		string.LoadString(IDS_PAGELISTHEADER_INDEX);
		pDC->TextOut(m_p[0].x + 5 * (int)m_fScreenPrinterRatioX, 43 * (int)m_fScreenPrinterRatioY, string);
		string.LoadString(IDS_PAGELISTHEADER_PAGENAME);
		pDC->TextOut(m_p[2].x, 43 * (int)m_fScreenPrinterRatioY, string);
#ifdef PDF
		string.LoadString(IDS_PAGELISTHEADER_PSPATH);
#else
		string.LoadString(IDS_PAGELISTHEADER_BITMAPPATH);
#endif
		pDC->TextOut(m_p[4].x, 43 * (int)m_fScreenPrinterRatioY, string);
		string.LoadString(IDS_PAGELISTHEADER_FORMAT);
		pDC->TextOut(m_p[6].x, 43 * (int)m_fScreenPrinterRatioY, string);
		string.LoadString(IDS_PAGELISTHEADER_OFFSET);
		pDC->TextOut(m_p[8].x, 43 * (int)m_fScreenPrinterRatioY, string);
#ifdef PDF
		string.LoadString(IDS_PAGELISTHEADER_SCALE);
		pDC->TextOut(m_p[10].x, 43 * (int)m_fScreenPrinterRatioY, string);
#endif

		pDC->SetBkMode(nOldBkMode);
		pDC->SetTextColor(crOldTextColor);
	}

	COLORREF crOldTextColor = pDC->SetTextColor(BLACK);
	int		 nOldBkMode	    = pDC->SetBkMode(TRANSPARENT);

	m_DisplayList.BeginRegisterItems(pDC);

	CRect currentOutRect;
	if (pDC->IsPrinting())
	{
		int widthForPrint = pDC->GetDeviceCaps(HORZRES) - m_rcPrintMargins.left   - m_rcPrintMargins.right;
		currentOutRect = CRect(GetScrollPos(SB_HORZ), 60 * (int)m_fScreenPrinterRatioY + m_nHeaderHeight, widthForPrint + GetScrollPos(SB_HORZ), 60 * (int)m_fScreenPrinterRatioY + m_nHeaderHeight);
	}
	else
	{
		CRect clientRect;
		GetClientRect(clientRect);
		currentOutRect = CRect(GetScrollPos(SB_HORZ), m_nHeaderHeight, clientRect.Width() + GetScrollPos(SB_HORZ), m_nHeaderHeight);
	}
	
	BeginWaitCursor();
	int		 nIndex		 = (pDC->IsPrinting()) ? m_nStartIndex : 0;
	BOOL	 bBackGround = FALSE;
	POSITION pos = NULL;
	//if (pDC->IsPrinting())
	//	pos = pDoc->m_PageTemplateList.FindIndex(m_nStartIndex, theApp.GetCurProductPartIndex());
	//else
		pos	= pDoc->m_PageTemplateList.GetHeadPosition();
	int nYMax = pDC->GetDeviceCaps(VERTRES) - m_rcPrintMargins.bottom;
	while (pos)
	{
		CPageTemplate& rTemplate = pDoc->m_PageTemplateList.CList::GetNext(pos);
		int			   nLines	 = GetNumLines(&rTemplate);
		currentOutRect.bottom	 = (nLines == 0) ? currentOutRect.top + 2 * GetCharHeight() : currentOutRect.top + (nLines + 1) * GetCharHeight();

		if (pDC->IsPrinting())
			if (currentOutRect.bottom > nYMax) 
				break;

		DrawTemplateInfos(pDC, &rTemplate, pos, currentOutRect, nIndex, bBackGround);

		currentOutRect.top = currentOutRect.bottom;
		m_nStartIndex++;
	}
	EndWaitCursor();

	m_DisplayList.EndRegisterItems(pDC);

///////////////////////////////////////////////////////////////////////////////////////////////
//	draw dotted gray column-lines for headers
	int nTop = (pDC->IsPrinting()) ? (40 * (int)m_fScreenPrinterRatioY + m_nHeaderHeight) : m_nHeaderHeight;
	m_p[0] = CPoint(		   GetHeaderItemWidthIndex(),	    	nTop);
	m_p[1] = CPoint(m_p[0].x + GetHeaderItemWidthPageName() - 1,	nTop);
	m_p[2] = CPoint(m_p[1].x + GetHeaderItemWidthPSpath()   - 1,	nTop);
	m_p[3] = CPoint(m_p[2].x + GetHeaderItemWidthFormat()   - 1,	nTop);
	m_p[4] = CPoint(m_p[3].x + GetHeaderItemWidthOffset()   - 1,	nTop);

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	CPen* pOldPen = (CPen*)pDC->SelectObject(&pen);
	pDC->MoveTo(m_p[0]); 
	pDC->LineTo(m_p[0].x, currentOutRect.bottom * (int)m_fScreenPrinterRatioY);
	pDC->MoveTo(m_p[1]); 
	pDC->LineTo(m_p[1].x, currentOutRect.bottom * (int)m_fScreenPrinterRatioY);
	pDC->MoveTo(m_p[2]); 
	pDC->LineTo(m_p[2].x, currentOutRect.bottom * (int)m_fScreenPrinterRatioY);
	pDC->MoveTo(m_p[3]); 
	pDC->LineTo(m_p[3].x, currentOutRect.bottom * (int)m_fScreenPrinterRatioY);
	pDC->MoveTo(m_p[4]); 
	pDC->LineTo(m_p[4].x, currentOutRect.bottom * (int)m_fScreenPrinterRatioY);
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();


	pDC->SelectObject(pOldFont);
	textFont.DeleteObject();
	pDC->SetTextColor(crOldTextColor);    
	pDC->SetBkMode(nOldBkMode);


	if (m_bInitialUpdate)	// if first OnDraw, find and select links to eventually selected page templates
	{
		CPageListView*  pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
		if (!pPageListView)
			return;

		CDisplayItem* pDIScrollTo = NULL;
		CDisplayItem* pDISelect	  = pPageListView->m_DisplayList.GetFirstSelectedItem();
		while (pDISelect)
		{
			CPageTemplate* pTemplate = (CPageTemplate*)pDISelect->m_pData;
			for (int i = 0; i < pTemplate->m_ColorSeparations.GetSize(); i++)
			{
				pDISelect = m_DisplayList.GetItemFromData((void*)pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex,
								 						  (void*)pTemplate, DISPLAY_ITEM_TYPEID(CPageListTableView, PDFDocument));
				if (pDISelect)
				{
					m_DisplayList.SelectItem(*pDISelect);
					m_DisplayList.InvalidateItem(pDISelect);

					if (!pDIScrollTo)
						pDIScrollTo = pDISelect;
				}
			}
			pDISelect = pPageListView->m_DisplayList.GetNextSelectedItem(pDISelect);
		}

		if (pDIScrollTo)
		{
			if ( ! pDIScrollTo->m_bIsVisible)	
			{
				Invalidate();
				OnVScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.top - 20, NULL);
			}
		}

		m_bInitialUpdate = FALSE;
	}
}

void CPageListTableView::DrawTemplateInfos(CDC* pDC, CPageTemplate* pTemplate, POSITION& rPos, CRect& rOutRect, int& rIndex, BOOL& rBackGround)
{
	if ( ! pTemplate)
		return;
	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc )
		return;

	int			   nIndex2		= -1;
	CPageTemplate* p2ndTemplate = NULL;
	if (pTemplate->m_nMultipageGroupNum >= 0)
	{
		if (pTemplate == &pDoc->m_PageTemplateList.GetHead()) 
		{
			p2ndTemplate = &pDoc->m_PageTemplateList.GetTail();
			nIndex2 = pDoc->m_PageTemplateList.GetCount() - 1;
		}
		else
			if (pTemplate == &pDoc->m_PageTemplateList.GetTail()) 
			{
				p2ndTemplate = &pDoc->m_PageTemplateList.GetHead();
				nIndex2 = 0;
				return;
			}
			else
			{
				p2ndTemplate = &pDoc->m_PageTemplateList.CList::GetNext(rPos);
				nIndex2 = rIndex + 1;
			}
	}

	int nTextY = rOutRect.top + GetCharHeight()/2;

	if (rBackGround)
	{
		if (pDC->RectVisible(rOutRect))
		{
  			pDC->FillSolidRect(rOutRect, TABLECOLOR_BACKGROUND_DARK);

			CPen  grayPen(PS_SOLID, 1, TABLECOLOR_SEPARATION_LINE);
			CPen* pOldPen = pDC->SelectObject(&grayPen);
			pDC->MoveTo(rOutRect.left,  rOutRect.top);
			pDC->LineTo(rOutRect.right, rOutRect.top);
			pDC->MoveTo(rOutRect.left,  rOutRect.bottom);
			pDC->LineTo(rOutRect.right, rOutRect.bottom);
			pDC->SelectObject(pOldPen);
		}
	}
	rBackGround = !rBackGround;

	CString	string;
	CString strMeasureFormat;

	// Output page index
	if (pDC->RectVisible(CRect(m_p[0].x, nTextY, m_p[1].x, nTextY + GetCharHeight())))
	{
		if (p2ndTemplate)
			string.Format(_T("%4d/%-4d"), rIndex + 1, nIndex2 + 1);
		else
			string.Format(_T("%4d"), rIndex + 1);
		FitStringToWidth(pDC, string, m_p[1].x - m_p[0].x);
		pDC->TextOut(m_p[0].x, nTextY, string);
	}

	// Output page name
	if (pDC->RectVisible(CRect(m_p[2].x, nTextY, m_p[3].x, nTextY + GetCharHeight())))
	{
		if (p2ndTemplate)
			string.Format(_T("%s/%s"), pTemplate->m_strPageID, p2ndTemplate->m_strPageID);
		else
			string.Format(_T("%s"), pTemplate->m_strPageID);
		FitStringToWidth(pDC, string, m_p[3].x - m_p[2].x);
		pDC->TextOut(m_p[2].x, nTextY, string);
	}

	// Output PDF fullpath			  
	CString strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);
	for (int i = 0; i < pTemplate->m_ColorSeparations.GetSize(); i++)
	{
		CPageSourceHeader* pPageSourceHeader = NULL;
		COLORREF		   crColor			 = LIGHTGRAY;
		CString			   strColorName;
		if ( (pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex >= 0) && (pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
		{
			crColor		 = pDoc->m_ColorDefinitionTable[pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex].m_rgb;
			strColorName = pDoc->m_ColorDefinitionTable[pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex].m_strColorName;
			pPageSourceHeader = pTemplate->GetObjectSourceHeader(pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex);
		}
		else
		{
			pPageSourceHeader = pTemplate->GetObjectSourceHeader(-1);
			strColorName = (pPageSourceHeader) ? pPageSourceHeader->m_strColorName : "";
		}

		CRect regRect(0,0,0,0);
		if (m_p[5].x - m_p[4].x > GetCharWidth())
		{
			CRect colorRect;
			colorRect.left   = m_p[4].x; 
			colorRect.top	 = nTextY + (i * GetCharHeight()) + GetCharHeight()/4 - 1;
			colorRect.right  = colorRect.left + GetCharHeight()/2;
			colorRect.bottom = colorRect.top  + GetCharHeight()/2;
			regRect			 = colorRect;
			if (pDC->RectVisible(colorRect))
			{
				CBrush  colorBrush(crColor);
				CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&colorBrush);
				CPen	pen(PS_SOLID, 1, (crColor == LIGHTGRAY) ? LIGHTGRAY : DARKGRAY);
				CPen*	pOldPen	= pDC->SelectObject(&pen);
				if (strColorName.CompareNoCase(_T("Composite")) == 0)
				{
					if ( ! pDC->IsPrinting())
						pDC->DrawIcon(colorRect.left - 2, colorRect.top - 3, theApp.LoadIcon((crColor == LIGHTGRAY) ? IDI_CMYK_DIMMED : IDI_CMYK));
				}
				else
					pDC->Rectangle(colorRect);
				pDC->SelectObject(pOldBrush);
				pDC->SelectObject(pOldPen);
				colorBrush.DeleteObject();
				pen.DeleteObject();
			}
		}

		CRect textRect;
		textRect.left	= m_p[4].x + GetCharWidth();
		textRect.top	= nTextY + (i * GetCharHeight());
		textRect.right  = m_p[5].x; //textRect.left + pDC->GetTextExtent(string).cx;
		textRect.bottom = textRect.top  + GetCharHeight();

		if (pDC->RectVisible(textRect))
		{
			int				   nColorDefinitionIndex = pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex;
			CPageSource*	   pPageSource			 = pTemplate->GetObjectSource	   (nColorDefinitionIndex);
			CPageSourceHeader* pPageSourceHeader	 = pTemplate->GetObjectSourceHeader(nColorDefinitionIndex);
			if (pPageSource && pPageSourceHeader )
			{
				int nHeaderIndex = pPageSource->GetHeaderIndex(pPageSourceHeader);
#ifdef PDF
				string.Format(_T("%s [%d]"), (LPCTSTR)((pPageSource->m_strDocumentFullpath.IsEmpty()) ? strUnknown : pPageSource->m_strDocumentFullpath), nHeaderIndex + 1);
#else
				string.Format(_T("%s [%d]"), (LPCTSTR)((pPageSource->m_strContentFilesPath.IsEmpty()) ? strUnknown : pPageSource->m_strContentFilesPath), nHeaderIndex + 1);
#endif
			}
			else
				string = "";
			FitStringToWidth(pDC, string, m_p[5].x - (m_p[4].x + GetCharWidth()));
			pDC->TextOut(textRect.left, textRect.top, string);
		}

		regRect.UnionRect(regRect, textRect);

		m_DisplayList.RegisterItem(pDC, regRect, regRect, (void*)(int)pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex, 
								   DISPLAY_ITEM_REGISTRY(CPageListTableView, PDFDocument), (void*)pTemplate, CDisplayItem::ForceRegister);

		if ( (strColorName.CompareNoCase(_T("Composite")) == 0) && pPageSourceHeader)
			for (int j = 0; j < pPageSourceHeader->m_spotColors.GetSize(); j++)
			{
				COLORREF crSpotColor	  = (crColor == LIGHTGRAY) ? LIGHTGRAY : theApp.m_colorDefTable.FindColorRef(pPageSourceHeader->m_spotColors[j]);
				CString	 strSpotColorName = pPageSourceHeader->m_spotColors[j];
				CRect regRect(0,0,0,0);
				if (m_p[5].x - m_p[4].x > GetCharWidth())
				{
					CRect colorRect;
					colorRect.left   = m_p[4].x; 
					colorRect.top	 = nTextY + ( (i + 1 + j) * GetCharHeight()) + GetCharHeight()/4 - 1;
					colorRect.right  = colorRect.left + GetCharHeight()/2;
					colorRect.bottom = colorRect.top  + GetCharHeight()/2;
					regRect			 = colorRect;
					if (pDC->RectVisible(colorRect))
					{
						CBrush  colorBrush(crSpotColor);
						CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&colorBrush);
						CPen	pen(PS_SOLID, 1, (crColor == LIGHTGRAY) ? LIGHTGRAY : DARKGRAY);
						CPen*	pOldPen	= pDC->SelectObject(&pen);
						pDC->Rectangle(colorRect);
						pDC->SelectObject(pOldBrush);
						pDC->SelectObject(pOldPen);
						colorBrush.DeleteObject();
						pen.DeleteObject();
					}
					FitStringToWidth(pDC, strSpotColorName, m_p[5].x - (m_p[4].x + GetCharWidth()));
					pDC->TextOut(textRect.left, colorRect.top - 2, strSpotColorName);
				}
			}
	} 

	// Output page format
	if (pDC->RectVisible(CRect(m_p[6].x, nTextY, m_p[7].x, nTextY + GetCharHeight())))
	{
		float		   fWidth, fHeight;
		CLayoutObject* pObject = pTemplate->GetLayoutObject();
		if (pObject)
		{
			fWidth  = pObject->GetRealWidth();
			fHeight = pObject->GetRealHeight();
		}
		else
		{
			if ( ! pTemplate->IsFlatProduct())
			{
				fWidth  = pDoc->m_boundProducts.GetProductPart(pTemplate->m_nProductPartIndex).m_fPageWidth;
				fHeight = pDoc->m_boundProducts.GetProductPart(pTemplate->m_nProductPartIndex).m_fPageHeight;
			}
			else
			{
				int nIndex = pDoc->m_PageTemplateList.GetIndex(pTemplate, pTemplate->m_nProductPartIndex)/2;
				if (nIndex < pDoc->m_flatProducts.GetSize())
				{
					fWidth  = pDoc->m_flatProducts.FindByIndex(nIndex).m_fTrimSizeWidth;
					fHeight = pDoc->m_flatProducts.FindByIndex(nIndex).m_fTrimSizeHeight;
				}
			}
		}
		if (p2ndTemplate)
		{
			pObject = p2ndTemplate->GetLayoutObject();
			if (pObject)
				fWidth+= pObject->GetRealWidth();
			else
				fWidth+= p2ndTemplate->GetProductPart().m_fPageWidth;
		}

		// Sets the FormatString dependent on unit
		strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.2f"), (LPCTSTR)MeasureFormat("%.2f"));
		string.Format((LPCTSTR)strMeasureFormat, MeasureFormat(fWidth), MeasureFormat(fHeight));
		//string.Format(_T("%.2f x %.2f"), fWidth, fHeight);
		FitStringToWidth(pDC, string, m_p[7].x - m_p[6].x);
		pDC->TextOut(m_p[6].x, nTextY, string);
	}

	// Output center offset
	if (pDC->RectVisible(CRect(m_p[8].x, nTextY, m_p[9].x, nTextY + GetCharHeight())))
	{
		// Sets the FormatString dependent on unit
		strMeasureFormat.Format(_T("%s;%s"), (LPCTSTR)MeasureFormat("%.2f"), (LPCTSTR)MeasureFormat("%.2f"));
		string.Format((LPCTSTR)strMeasureFormat, MeasureFormat(pTemplate->GetContentOffsetX()), MeasureFormat(pTemplate->GetContentOffsetY()));
		//string.Format(_T("%.2f;%.2f"), pTemplate->m_fContentOffsetX,  pTemplate->m_fContentOffsetY);
		FitStringToWidth(pDC, string, m_p[9].x - m_p[8].x);
		pDC->SetTextColor( (pTemplate->HasContentOffset()) ? RED : BLACK);
		pDC->TextOut(m_p[8].x, nTextY, string);
		pDC->SetTextColor(BLACK);
	}
#ifdef PDF
	// Output page scale
	if (pDC->RectVisible(CRect(m_p[10].x, nTextY, m_p[11].x, nTextY + GetCharHeight())))
	{
		// Sets the FormatString dependent on unit
		strMeasureFormat.Format(_T("%s;%s"), (LPCTSTR)MeasureFormat("%.2f"), (LPCTSTR)MeasureFormat("%.2f"));
		string.Format((LPCTSTR)strMeasureFormat, MeasureFormat(pTemplate->m_fContentScaleX * 100.0f), MeasureFormat(pTemplate->m_fContentScaleY * 100.0f));
		//string.Format(_T("%.2f;%.2f"), pTemplate->m_fScaleX * 100.0f,  pTemplate->m_fScaleY * 100.0f);
		FitStringToWidth(pDC, string, m_p[11].x - m_p[10].x);
		pDC->SetTextColor( (pTemplate->IsScaled()) ? RED : BLACK);
		pDC->TextOut(m_p[10].x, nTextY, string);
		pDC->SetTextColor(BLACK);
	}
#endif 
	rIndex+= (p2ndTemplate && (nIndex2 == rIndex + 1) ) ? 2 : 1;
}

int CPageListTableView::GetNumLines(CPageTemplate* pTemplate)
{
	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc )
		return 0;

	int nLines = 0;
	for (int i = 0; i < pTemplate->m_ColorSeparations.GetSize(); i++)
	{
		CString			   strColorName;
		CPageSourceHeader* pPageSourceHeader = NULL;
		if ( (pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex >= 0) && (pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex < pDoc->m_ColorDefinitionTable.GetSize()) )
		{
			pPageSourceHeader = pTemplate->GetObjectSourceHeader(pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex);
			strColorName = pDoc->m_ColorDefinitionTable[pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex].m_strColorName;
		}
		else
		{
			pPageSourceHeader = pTemplate->GetObjectSourceHeader(-1);
			strColorName = (pPageSourceHeader) ? pPageSourceHeader->m_strColorName : "";
		}
		nLines++;
		if ( (strColorName.CompareNoCase(_T("Composite")) == 0) && pPageSourceHeader)
			nLines += pPageSourceHeader->m_spotColors.GetSize();
	}
	return nLines;
}

void CPageListTableView::FitStringToWidth(CDC* pDC, CString& rString, int nWidth)
{
	CString strTmp;
	CSize	size;
	int		nFit;

	if (nWidth < 0)
	{
		rString.Empty();
		return;
	}

	GetTextExtentExPoint(pDC->m_hDC, (LPCTSTR)rString, rString.GetLength(), nWidth, &nFit, NULL, &size);
	if ((nFit <= 3) && (rString.GetLength() > 3))
	{
		strTmp  = rString.Left(1);
		strTmp += (nFit == 3) ? ".." : ".";
		rString = strTmp;
	}
	else
		if (nFit < rString.GetLength())
		{
			strTmp  = rString.Left(nFit - 2);
			strTmp += "...";
			rString = strTmp;
		}
}

/////////////////////////////////////////////////////////////////////////////
// CPageListTableView diagnostics

#ifdef _DEBUG
void CPageListTableView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CPageListTableView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CImpManDoc* CPageListTableView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CImpManDoc)));
	return (CImpManDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPageListTableView message handlers

BOOL CPageListTableView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// 2nd parameter must be 0 (means no standard-cursor will be registered), 
	// if we want draw out own cursors
	CBrush* pBkgrdBrush = new CBrush;
	pBkgrdBrush->CreateSolidBrush(TABLECOLOR_BACKGROUND);

	lpszClassName = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(*pBkgrdBrush), 0);

	BOOL ret = CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	pBkgrdBrush->Detach();
	delete pBkgrdBrush;

	return ret;
}

void CPageListTableView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/) 
{
	CPageListView* pView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));// Workaround:
	CRect clientRect;																	// Use PageListView to get clientRect height.
	pView->GetClientRect(clientRect);													// because for some reason this view's clientRect is 0 yet (don't know why)
	
	CalcTemplateListSize();
	CSize sizeTotal = CSize(m_nHeaderItemsTotalWidth, m_TemplateListSize.cy + m_nHeaderHeight);
	int	  nNumPages = m_TemplateListSize.cy / clientRect.Height();
	CSize sizePage  = CSize(sizeTotal.cx, (nNumPages) ? sizeTotal.cy / nNumPages : sizeTotal.cy);
	CSize sizeLine  = CSize(sizeTotal.cx, m_nCharHeight);

	SetScrollSizes(MM_TEXT, sizeTotal, sizePage, sizeLine);

	Invalidate();	
}

void CPageListTableView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 20)
		hdn->pitem->cxy = 20;
}

void CPageListTableView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	switch (hdn->iItem)
	{
	case 0:		m_nHeaderItemWidthIndex	   = hdn->pitem->cxy; 
				break;
	case 1:		m_nHeaderItemWidthPageName = hdn->pitem->cxy; 
				break;
	case 2:		m_nHeaderItemWidthPSpath   = hdn->pitem->cxy; 
				break;
	case 3:		m_nHeaderItemWidthFormat   = hdn->pitem->cxy; 
				break;
	case 4:		m_nHeaderItemWidthOffset   = hdn->pitem->cxy; 
				break;
	case 5:		m_nHeaderItemWidthScale    = hdn->pitem->cxy; 
				break;
	default:	break;
	}

	if ((m_TemplateListSize.cx > 0) && (m_TemplateListSize.cy > 0))
	{
		m_nHeaderItemsTotalWidth = m_nHeaderItemWidthIndex  + m_nHeaderItemWidthPageName + m_nHeaderItemWidthPSpath + 
								   m_nHeaderItemWidthFormat + m_nHeaderItemWidthOffset + 
								   m_nHeaderItemWidthScale;
		CRect clientRect;
		GetClientRect(&clientRect);
		CSize sizeTotal = CSize(m_nHeaderItemsTotalWidth, m_TemplateListSize.cy + m_nHeaderHeight);
		int	  nNumPages = m_TemplateListSize.cy / clientRect.Height();
		CSize sizePage  = CSize(sizeTotal.cx, (nNumPages) ? sizeTotal.cy / nNumPages : sizeTotal.cy);
		CSize sizeLine  = CSize(sizeTotal.cx, m_nCharHeight);

		SetScrollSizes(MM_TEXT, sizeTotal, sizePage, sizeLine);

		InvalidateRect(clientRect);
		UpdateWindow(); 
	}
}

BOOL CPageListTableView::OnMultiSelect()	// multiple items has been selected
{
	CDisplayItem* pFirstDI = m_DisplayList.GetFirstSelectedItem();
	if (!pFirstDI)
		return FALSE;

	CDisplayItem* pDI = m_DisplayList.GetNextSelectedItem(pFirstDI);
	while (pDI)
	{
		if (pDI->m_pFnMsgHandler != pFirstDI->m_pFnMsgHandler)
			m_DisplayList.DeselectItem(*pDI);

		pDI = m_DisplayList.GetNextSelectedItem(pDI);
	}

	return TRUE;
}

BOOL CPageListTableView::OnSelectPDFDocument(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	// object(s) has been selected
{
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bDeleteSelectionMenuActive = TRUE;

//TODO: doesn't work correct yet
/*
	CPageListView* pView	 = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	CPageTemplate* pTemplate = (CPageTemplate*)pDI->m_pAuxData;
	if (pTemplate && pView)
	{
		CDisplayItem* pDITemplate = pView->m_DisplayList.GetItemFromData(pTemplate);
		if (pDITemplate)
		{
			pView->m_DisplayList.SelectItem(*pDITemplate);
			pView->m_DisplayList.InvalidateItem(pDITemplate);
			if ( ! pDITemplate->m_bIsVisible)	
			{
				pView->Invalidate();
				CPoint ScrollPos = pView->GetScrollPosition();
				pView->ScrollToPosition(CPoint(0, ScrollPos.y + pDITemplate->m_BoundingRect.top - 20));
			}
		}
	}
*/

	return TRUE;
}

BOOL CPageListTableView::OnDeselectPDFDocument(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	// object(s) has been deselected
{
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bDeleteSelectionMenuActive = FALSE;

	return TRUE;
}

void CPageListTableView::OnLButtonDown(UINT /*nFlags*/, CPoint point) 
{
	// First of all deselect items in counterpart pane
	CPageListView* pView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pView)
	{
		pView->m_DisplayList.DeselectAll();			
		pView->m_DisplayList.InvalidateItem(NULL);
		pView->UpdateWindow();
	}
	//////////////////////////////////////////////////

	m_DisplayList.OnLButtonDown(point); // pass message to display list

	CPageListTopToolsView* pTopToolsView = (CPageListTopToolsView*)theApp.GetView(RUNTIME_CLASS(CPageListTopToolsView));
	if (pTopToolsView)
		pTopToolsView->UpdateDialogControls(pTopToolsView, TRUE);
}

void CPageListTableView::OnContextMenu(CWnd* /*pWnd*/, CPoint point) 
{
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_PAGELIST_RIGHTVIEWS_POPUP));

	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);

	CWnd* pWndPopupOwner = this;
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
}

void CPageListTableView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
	{
	case VK_HOME	: OnVScroll(SB_TOP, 0, NULL);
					  OnHScroll(SB_LEFT, 0, NULL);
					  break;
	case VK_END		: OnVScroll(SB_BOTTOM, 0, NULL);
					  OnHScroll(SB_RIGHT, 0, NULL);
					  break;
	case VK_UP		: OnVScroll(SB_LINEUP, 0, NULL);
					  break;
	case VK_DOWN	: OnVScroll(SB_LINEDOWN, 0, NULL);
					  break;
	case VK_PRIOR	: OnVScroll(SB_PAGEUP, 0, NULL);
					  break;
	case VK_NEXT	: OnVScroll(SB_PAGEDOWN, 0, NULL);
					  break;
	case VK_LEFT	: OnHScroll(SB_LINELEFT, 0, NULL);
					  break;
	case VK_RIGHT	: OnHScroll(SB_LINERIGHT, 0, NULL);
					  break;
	default			: break;
	}
	
	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CPageListTableView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// The default call of OnMouseWheel causes an assertion or doesn't work
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion
}

void CPageListTableView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

BOOL CPageListTableView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	SetCursor(theApp.m_CursorStandard);
	
	return CScrollView::OnSetCursor(pWnd, nHitTest, message);
}

void CPageListTableView::PostNcDestroy() 
{
	// Free the C++ class.
	delete this;
}

