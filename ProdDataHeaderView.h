#pragma once



// CPDHCustomPaintFrame

class CPDHCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CPDHCustomPaintFrame)

public:
	CPDHCustomPaintFrame();
	virtual ~CPDHCustomPaintFrame();

public:

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};



// CProdDataHeaderView-Formularansicht

class CProdDataHeaderView : public CFormView
{
	DECLARE_DYNCREATE(CProdDataHeaderView)

protected:
	CProdDataHeaderView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CProdDataHeaderView();

public:
	enum { IDD = IDD_PRODDATAHEADERVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	CPDHCustomPaintFrame m_jobTitleHeader, m_orderDataHeader, m_orderDataFrame, m_productDataHeader, m_productDataFrame;
	CFont				 m_bigFont;
	CBrush				 m_hollowBrush;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};


