// DlgProductionBoundDetails.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionBoundDetails.h"
#include "DlgShinglingAssistent.h"




// CPBDCustomPaintFrame

IMPLEMENT_DYNAMIC(CPBDCustomPaintFrame, CStatic)

CPBDCustomPaintFrame::CPBDCustomPaintFrame()
{
}

CPBDCustomPaintFrame::~CPBDCustomPaintFrame()
{
}


BEGIN_MESSAGE_MAP(CPBDCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CPBDCustomPaintFrame-Meldungshandler

void CPBDCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CString strText;
	CRect rcFrame;
	GetClientRect(rcFrame);

	switch (GetDlgCtrlID())
	{
	case IDC_MASKING_PREVIEW_FRAME:		DrawPages	  (&dc,	rcFrame);	break;
	default:															break;
	}
}


void CPBDCustomPaintFrame::DrawPages(CDC* pDC, CRect frameRect)
{
	CDlgProductionBoundDetails* pDlg = (CDlgProductionBoundDetails*)GetParent();
	if ( ! pDlg)
		return;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBrush brush(RGB(127,162,207));
	CPen   pen(PS_SOLID, 1, LIGHTGRAY);
	CBrush* pOldBrush = pDC->SelectObject(&brush);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);

	pDC->Rectangle(frameRect);

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	brush.DeleteObject();
	pen.DeleteObject();

	if (pDlg->m_bShinglingActive)
	{
		CRect rcText = frameRect; 
		rcText.left += 2; rcText.bottom -= 2;
		CFont font;
		font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		CFont* pOldFont = pDC->SelectObject(&font);
		pDC->SetTextColor(BROWN);
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(255,230,180));
		CString string; string.LoadString(IDS_SHINGLING_ACTIVE);
		pDC->DrawText(string, rcText, DT_LEFT | DT_BOTTOM | DT_SINGLELINE);
		pDC->SelectObject(pOldFont);
		font.DeleteObject();
	}

	frameRect.DeflateRect(15, 15);
	frameRect.DeflateRect(5,5); frameRect.left += 10; frameRect.top += 10;
	
	pDoc->m_PageTemplateList.Draw(pDC, frameRect, 1, pDlg->m_nProductPartIndex, TRUE, FALSE, TRUE, TRUE, NULL, FALSE, TRUE, pDlg->m_fBleed, pDlg->m_fGlueLine, pDlg->m_fSpineArea);
}


// CDlgProductionBoundDetails-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionBoundDetails, CDialog)

CDlgProductionBoundDetails::CDlgProductionBoundDetails(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionBoundDetails::IDD, pParent)
{
	m_pDlgMarkSet = NULL;
	m_strPressMarkset = _T("");
	m_nProductPartIndex = 0;
	m_strProductPartComment = _T(""); 
	m_fBleed = 0.0f;
	m_fGlueLine = 0.0f;
	m_bAdjustContentToMask = FALSE;
	m_bShinglingActive = FALSE;
	m_fSpineArea = 0.0f;
}

CDlgProductionBoundDetails::~CDlgProductionBoundDetails()
{
}

void CDlgProductionBoundDetails::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PD_PRESSMARKSET, m_strPressMarkset);
	DDX_CBIndex(pDX, IDC_PD_PRODUCTION_TYPE, m_nProductionType);
	DDX_Text(pDX, IDC_PRODUCTPART_COMMENT, m_strProductPartComment);
	DDX_Control(pDX, IDC_PD_BLEED_SPIN, m_bleedSpin);
	DDX_Measure(pDX, IDC_PD_BLEED_EDIT, m_fBleed);
	DDX_Control(pDX, IDC_PD_GLUELINE_SPIN, m_glueLineSpin);
	DDX_Measure(pDX, IDC_PD_GLUELINE_EDIT, m_fGlueLine);
	DDX_Radio(pDX, IDC_CUT_CONTENT, m_bAdjustContentToMask);
}


BEGIN_MESSAGE_MAP(CDlgProductionBoundDetails, CDialog)
	ON_BN_CLICKED(IDC_FP_BROWSE_MARKSET, OnBnClickedFpBrowseMarkset)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_BLEED_SPIN, OnDeltaposBleedSpin)
	ON_EN_KILLFOCUS(IDC_PD_BLEED_EDIT, OnKillfocusBleedEdit)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_GLUELINE_SPIN, OnDeltaposGluelineSpin)
	ON_EN_KILLFOCUS(IDC_PD_GLUELINE_EDIT, OnKillfocusGluelineEdit)
	ON_BN_CLICKED(IDC_CUT_CONTENT, &CDlgProductionBoundDetails::OnBnClickedCutContent)
	ON_BN_CLICKED(IDC_SCALE_CONTENT, &CDlgProductionBoundDetails::OnBnClickedScaleContent)
END_MESSAGE_MAP()


// CDlgProductionBoundDetails-Meldungshandler

BOOL CDlgProductionBoundDetails::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_pagesPreviewFrame.SubclassDlgItem	(IDC_MASKING_PREVIEW_FRAME,	this);

	m_bleedSpin.SetRange(0, 1);  
	m_glueLineSpin.SetRange(0, 1);  

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		//CString strTitle;
		//GetWindowText(strTitle);
		//strTitle += _T(" ") + pDoc->m_printGroupList[m_nProductPartIndex].m_strName;
		//SetWindowText(strTitle);
		int nShow = SW_SHOW;
		//if (pDoc->m_printGroupList[m_nProductPartIndex].m_strName.CompareNoCase(theApp.m_strCoverName) == 0)
		//	nShow = SW_HIDE;
		GetDlgItem(IDC_PD_GLUELINE_STATIC)->ShowWindow(nShow);
		GetDlgItem(IDC_PD_GLUELINE_ICONSTATIC)->ShowWindow(nShow);
		GetDlgItem(IDC_PD_GLUELINE_EDIT)->ShowWindow(nShow);
		GetDlgItem(IDC_PD_GLUELINE_SPIN)->ShowWindow(nShow);
		GetDlgItem(IDC_CUT_CONTENT)->ShowWindow(nShow);
		GetDlgItem(IDC_SCALE_CONTENT)->ShowWindow(nShow);
	}

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProductionBoundDetails::OnBnClickedFpBrowseMarkset()
{
	UpdateData(TRUE);

	m_pDlgMarkSet = new CDlgMarkSet(this, TRUE);
	m_pDlgMarkSet->m_bBrowseOnly	   = TRUE;
	m_pDlgMarkSet->m_strCurrentMarkset = m_strPressMarkset;

	if (m_pDlgMarkSet->DoModal() == IDOK)
	{
		m_strPressMarkset = m_pDlgMarkSet->m_strMarksetFullPath;
		UpdateData(FALSE);
	}

	delete m_pDlgMarkSet;
}

void CDlgProductionBoundDetails::OnBnClickedOk()
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	UpdateCutScaleContent();

	OnOK();
}

void CDlgProductionBoundDetails::OnDeltaposBleedSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_bleedSpin.GetBuddy(), pNMUpDown->iDelta, 0.5f);
	*pResult = 0;

	OnKillfocusBleedEdit();
}

void CDlgProductionBoundDetails::OnKillfocusBleedEdit() 
{
	UpdateData(TRUE);
	UpdateMaskingPreview();
}

void CDlgProductionBoundDetails::OnDeltaposGluelineSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_glueLineSpin.GetBuddy(), pNMUpDown->iDelta, 0.5f);
	OnKillfocusGluelineEdit();
	*pResult = 0;
}

void CDlgProductionBoundDetails::OnKillfocusGluelineEdit() 
{
	UpdateData(TRUE);
	UpdateCutScaleContent();
}

void CDlgProductionBoundDetails::UpdateMaskingPreview()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgProductionBoundDetails::OnBnClickedCutContent()
{
	UpdateData(TRUE);
	UpdateCutScaleContent();
}

void CDlgProductionBoundDetails::OnBnClickedScaleContent()
{
	UpdateData(TRUE);
	UpdateCutScaleContent();
}

void CDlgProductionBoundDetails::UpdateCutScaleContent()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}
