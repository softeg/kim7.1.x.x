#pragma once



// CColorSettingsTableHeader

class CColorSettingsTableHeader : public CStatic
{
	DECLARE_DYNAMIC(CColorSettingsTableHeader)

public:
	CColorSettingsTableHeader();
	virtual ~CColorSettingsTableHeader();

public:
	CFont m_font;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};



DECLARE_TABLECOLUMN(CColumnSheetColorSettingsPDFSheet);
DECLARE_TABLECOLUMN(CColumnSheetColorSettingsInfo);


class CPrintSheetColorSettingsTable : public CPrintSheetTable
{
public:
	void Create(class CPrintSheetColorSettingsView* pParent);
	void Initialize(int nWhatToShow);
	int	 GetCellLines(CPrintSheet& rPrintSheet);
};


// CPrintSheetColorSettingsView-Formularansicht

class CPrintSheetColorSettingsView : public CFormView
{
	DECLARE_DYNCREATE(CPrintSheetColorSettingsView)

protected:
	CPrintSheetColorSettingsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetColorSettingsView();

public:
	enum { IDD = IDD_PRINTSHEETCOLORSETTINGSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


	DECLARE_DISPLAY_LIST(CPrintSheetColorSettingsView, OnMultiSelect, NO, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CPrintSheetColorSettingsView, PDFSheet,		  OnSelect, YES,  OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetColorSettingsView, PDFSheetSide,	  OnSelect, YES,  OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetColorSettingsView, PDFSheetPlate,	  OnSelect, NO,   OnDeselect, NO, OnActivate, NO,  OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetColorSettingsView, PDFSheetObject,	  OnSelect, NO,   OnDeselect, NO, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)

public:
	CColorSettingsTableHeader		m_colorSettingsTableTitle, m_colorSettingsTableHeader;
	CPrintSheetColorSettingsTable	m_colorSettingsTable;


public:
	void UpdateBars();
	void ScrollToDevicePosition(POINT ptDev); // explicit scrolling no checking
	void SetScrollSizes(int nMapMode, SIZE sizeTotal, const SIZE& sizePage, const SIZE& sizeLine);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
protected:
	virtual void OnDraw(CDC* /*pDC*/);
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedColorsettingsPrintcolorsButton();
};


