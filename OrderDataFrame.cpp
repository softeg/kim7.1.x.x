// OrderDataFrame.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "MainFrm.h"
#include "PrintSheetView.h"
#include "PrintSheetPlanningView.h"
#include "PrintSheetTableView.h"
#include "JobDocumentationView.h"
#include "JobDocumentationSettingsView.h"
#include "OrderDataView.h"
#include "BookBlockView.h"
#include "OrderDataFrame.h"
#include "DlgJobData.h"
#include "BookBlockFrame.h"
#include "ModalFrame.h"
#include "ProductNavigationView.h"
#include "DummyView.h"
#include "ProductsView.h"
#include "PrintComponentsView.h"
#include "DlgBoundProduct.h"
#include "OrderDataFrameLeftToolsView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNAMIC(COrderDataSplitter, CSplitterWnd)

BEGIN_MESSAGE_MAP(COrderDataSplitter, CSplitterWnd)
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()

// CProdDataSplitter message handlers
afx_msg LRESULT COrderDataSplitter::OnNcHitTest(CPoint point)
{
	//COrderDataFrame* pFrame = (COrderDataFrame*)GetParentFrame();
	//if (pFrame)
	//{
	//	if (this == &pFrame->m_wndSplitter2)
	//	{
	//		CWnd* pPane = pFrame->m_wndSplitter2.GetPane(0, 0);
	//		CRect rcWindow1, rcWindow2;
	//		pPane->GetWindowRect(rcWindow1);
	//		pPane = pFrame->m_wndSplitter2.GetPane(0, 1);
	//		pPane->GetWindowRect(rcWindow2);
	//		if ( (rcWindow1.right <= point.x) && (point.x <= rcWindow2.left) )
	//			return CSplitterWnd::OnNcHitTest(point);
	//	}
	//}

	return CSplitterWnd::OnNcHitTest(point);

	//return HTNOWHERE;
}

void COrderDataSplitter::OnSize(UINT nType, int cx, int cy)
{
}

void COrderDataSplitter::OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect)
{
	if (pDC == NULL)
	{
		RedrawWindow(rect, NULL, RDW_INVALIDATE|RDW_NOCHILDREN);
		return;
	}

	CRect rcRect = rect;
	switch (nType)
	{
	case splitBorder:
		{
			COrderDataFrame* pFrame = (COrderDataFrame*)GetParentFrame();
			if (pFrame)
			{
				if (this == &pFrame->m_wndSplitter)
				{
					CRect rcBorder = rcRect;
					ClientToScreen(rcBorder);

					CWnd* pPane0 = pFrame->m_wndSplitter.GetPane(0, 0);
					CWnd* pPane1 = pFrame->m_wndSplitter.GetPane(0, 1);
					CRect rcPane0, rcPane1;
					pPane0->GetWindowRect(rcPane0);
					pPane1->GetWindowRect(rcPane1);

					if (rcBorder.PtInRect(rcPane0.CenterPoint()))
					{
						pDC->Draw3dRect(rcRect, MIDDLEGRAY, LIGHTGRAY);
						rcRect.InflateRect(0, -1); 
						pDC->Draw3dRect(rcRect, RGB(211,218,228), RGB(211,218,228));
						rcRect.InflateRect(0, -1); rcRect.left++;
						pDC->Draw3dRect(rcRect, RGB(211,218,228), RGB(211,218,228));
						break;
					}
					if (rcBorder.PtInRect(rcPane1.CenterPoint()))
					{
						CPen pen(PS_SOLID, 1, MIDDLEGRAY);
						pDC->SelectObject(&pen);
						pDC->MoveTo(rcRect.left-4, rcRect.top);			pDC->LineTo(rcRect.right, rcRect.top);
						pen.DeleteObject();
						pen.CreatePen(PS_SOLID, 1, LIGHTGRAY);
						pDC->SelectObject(&pen);
						pDC->MoveTo(rcRect.left-4, rcRect.bottom - 1);	pDC->LineTo(rcRect.right + 4, rcRect.bottom - 1);
						pen.DeleteObject();
						break;
					}
				}
				else
				if (this == &pFrame->m_wndSplitter1)
				{
					CRect rcBorder = rcRect;
					ClientToScreen(rcBorder);

					CWnd* pPane0 = pFrame->m_wndSplitter1.GetPane(1, 0);
					CWnd* pPane1 = pFrame->m_wndSplitter1.GetPane(2, 0);
					CRect rcPane0, rcPane1;
					pPane0->GetWindowRect(rcPane0);
					pPane1->GetWindowRect(rcPane1);

					Pen pen1(Color::Color(160,160,164));	
					Pen pen2(Color::Color(192,192,192));	
					Pen pen3(Color::Color(209,215,226));	
					Pen pen4(Color::Color(225,230,235));	
					Graphics graphics(pDC->m_hDC);
					rcRect.OffsetRect(-1, 0);
					//if (rcBorder.PtInRect(rcPane0.CenterPoint()))
					//{
						//graphics.DrawLine(&pen1, rcRect.left,  rcRect.bottom,	rcRect.left,	rcRect.top);
						graphics.DrawLine(&pen4, rcRect.left+1,  rcRect.top-5,		rcRect.right,	rcRect.top-5);
						graphics.DrawLine(&pen3, rcRect.left+1,  rcRect.top-4,		rcRect.right,	rcRect.top-4);
						graphics.DrawLine(&pen3, rcRect.left+1,  rcRect.top-3,		rcRect.right,	rcRect.top-3);
						//graphics.DrawLine(&pen2, rcRect.right, rcRect.top,		rcRect.right,	rcRect.bottom);
						//rcRect.InflateRect(-1, -1);
						//graphics.DrawLine(&pen3, rcRect.left,  rcRect.bottom,	rcRect.left,	rcRect.top);
						//graphics.DrawLine(&pen3, rcRect.left,  rcRect.top,		rcRect.right,	rcRect.top);
						//graphics.DrawLine(&pen4, rcRect.right, rcRect.top,		rcRect.right,	rcRect.bottom);
						break;
					//}
				}
			}
			pDC->Draw3dRect(rcRect, MIDDLEGRAY, LIGHTGRAY);
			rcRect.InflateRect(-1, -1);
			pDC->Draw3dRect(rcRect, RGB(209,215,226), RGB(225,230,235));
		}
		return;

	case splitIntersection:
		break;

	case splitBox:
		break;

	case splitBar:
		{
			COLORREF crColor = RGB(165,190,250);
			COrderDataFrame* pFrame = (COrderDataFrame*)GetParentFrame();
			if (pFrame)
			{
				if (this == &pFrame->m_wndSplitter)
				{
					CRect rcBar = rcRect;
					CWnd* pPane0 = pFrame->m_wndSplitter.GetPane(0, 0);
					CWnd* pPane1 = pFrame->m_wndSplitter.GetPane(0, 1);
					CRect rcPane0, rcPane1;
					pPane0->GetWindowRect(rcPane0);
					pPane1->GetWindowRect(rcPane1);
					if ( (rcBar.CenterPoint().x >= rcPane0.right) && (rcBar.CenterPoint().x <= rcPane1.left) )
					{
						rcBar.DeflateRect(0, 1);
						pDC->FillSolidRect(rcBar, RGB(211,218,228));//RGB(240,240,240));//RGB(230,233,238));
						break;
					}
				}
				else
				if (this == &pFrame->m_wndSplitter1)
				{
					CRect rcBar = rcRect;
					ClientToScreen(rcBar);
					CWnd* pPane0 = pFrame->m_wndSplitter1.GetPane(1, 0);
					CWnd* pPane1 = pFrame->m_wndSplitter1.GetPane(2, 0);
					CRect rcPane0, rcPane1;
					pPane0->GetWindowRect(rcPane0);
					pPane1->GetWindowRect(rcPane1);
					if ( (rcBar.CenterPoint().y >= rcPane0.bottom) && (rcBar.CenterPoint().y <= rcPane1.top) )
					{
						CWnd* pPane = pFrame->m_wndSplitter1.GetPane(2, 0);
						CRect rcPane;
						pPane->GetWindowRect(rcPane);
						ScreenToClient(rcPane);
						CRect rect = rcRect; rect.right = rcPane.left;
						rect.left = rect.right; rect.right = rcRect.right-2;
						pDC->FillSolidRect(&rect, RGB(252,252,252));
						break;
					}
				}
				else
					//if (this == &pFrame->m_wndSplitter2)
						crColor = RGB(209,215,226);

				pDC->FillSolidRect(rcRect, crColor);
			}
			else
				pDC->FillSolidRect(rcRect, crColor);
		}
		break;

	default:
		ASSERT(FALSE);  // unknown splitter type
	}
}

void COrderDataSplitter::StopTracking(BOOL bAccept)
{
	if (m_bTracking && bAccept)
	{
		CSplitterWnd::StopTracking(bAccept);
	
		COrderDataFrame* pFrame = (COrderDataFrame*)GetParentFrame();
		if (pFrame)
			if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
			{
				CRect rcView;
				pFrame->m_wndSplitter1.GetPane(0, 0)->GetClientRect(rcView);
				pFrame->m_browserInfo.m_nProductsViewHeight = rcView.Height();
				if (rcView.Height() < 20)
				{
					if (pFrame->m_browserInfo.m_nShowInfo & COrderDataFrame::ShowProducts)
					{
						pFrame->m_browserInfo.m_nShowInfo = pFrame->m_browserInfo.m_nShowInfo & ~COrderDataFrame::ShowProducts;
						pFrame->SetupViewProducts();
						CView* pView = pFrame->GetOrderDataFrameLeftToolsView();
						if (pView)
							pView->UpdateDialogControls(pView, TRUE);
					}
				}
				else
				{
					if ( ! (pFrame->m_browserInfo.m_nShowInfo & COrderDataFrame::ShowProducts))
					{
						pFrame->m_browserInfo.m_nShowInfo = pFrame->m_browserInfo.m_nShowInfo |  COrderDataFrame::ShowProducts;
						pFrame->SetupViewProducts();
						CView* pView = pFrame->GetOrderDataFrameLeftToolsView();
						if (pView)
							pView->UpdateDialogControls(pView, TRUE);
					}
				}

				pFrame->RearrangeSplitters();
			}
	}
	else
		CSplitterWnd::StopTracking(bAccept);
}

void COrderDataSplitter::TrackColumnSize(int x, int col)
{
	//if (col == 0)
	//{
	//	CFrameWnd* pFrame = GetParentFrame();
	//	if (pFrame)
	//		if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
	//		{
	//			CProductNavigationView* pView = (CProductNavigationView*)((COrderDataFrame*)pFrame)->m_wndSplitter.GetPane(1, 0);
	//			CRect rcButtons = pView->m_productDataSelectorButtons.GetButtonClientRect(pView->m_productDataSelectorButtons.GetSize() - 1);
	//			if (x < rcButtons.right + 5)
	//				x = rcButtons.right + 5;
	//		}
	//}

	CSplitterWnd::TrackColumnSize(x, col);

	RecalcLayout();
	theApp.OnUpdateView(RUNTIME_CLASS(CProductsView), NULL, 0, NULL);
}





/////////////////////////////////////////////////////////////////////////////
// COrderDataFrame

IMPLEMENT_DYNCREATE(COrderDataFrame, CMDIChildWnd)

COrderDataFrame::COrderDataFrame()
{
	m_bStatusRestored					= FALSE;
	m_bUndoActive						= FALSE;
	m_browserInfo.m_nShowInfo			= ShowProducts;
	m_browserInfo.m_nProductsViewHeight = 150;
}

COrderDataFrame::~COrderDataFrame()
{
}

BEGIN_MESSAGE_MAP(COrderDataFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(COrderDataFrame)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten COrderDataFrame 

BOOL COrderDataFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	if ( ! m_wndSplitter.CreateStatic(this, 1, 2))
		return FALSE;
	if ( ! m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(COrderDataFrameLeftToolsView),  CSize(30, 0),  pContext))
		return FALSE;

	if ( ! m_wndSplitter1.CreateStatic(&m_wndSplitter, 3, 1, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter.IdFromRowCol(0, 1)))
		return FALSE;
	if ( ! m_wndSplitter1.CreateView(1, 0, RUNTIME_CLASS(CProductNavigationView),  CSize(0, 35),  pContext))
		return FALSE;
	//if ( ! m_wndSplitter1.CreateView(2, 0, RUNTIME_CLASS(CPrintSheetPlanningView), CSize(0, 400), pContext))
	//	return FALSE;

	if ( ! m_wndSplitter2.CreateStatic(&m_wndSplitter1, 1, 2, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter1.IdFromRowCol(0, 0)))
		return FALSE;
	if ( ! m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CProductsView), CSize(500, 100), pContext))
		return FALSE;
	if ( ! m_wndSplitter2.CreateView(0, 1, pContext->m_pNewViewClass, CSize(500, 100), pContext))
		return FALSE;

	if ( ! m_wndSplitter3.CreateStatic(&m_wndSplitter1, 1, 2, WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter1.IdFromRowCol(2, 0)))
		return FALSE;
	//if ( ! m_wndSplitter3.CreateView(0, 0, RUNTIME_CLASS(CPrintComponentsView), CSize(400, 100), pContext))
	//	return FALSE;
	if ( ! m_wndSplitter3.CreateView(0, 0, RUNTIME_CLASS(CDummyView), CSize(0, 0), pContext))
		return FALSE;
	if ( ! m_wndSplitter3.CreateView(0, 1, RUNTIME_CLASS(CPrintSheetPlanningView), CSize(0, 400), pContext))
		return FALSE;

	return TRUE; 
}

void COrderDataFrame::OnSize(UINT nType, int cx, int cy)
{
	if (m_wndSplitter2.m_hWnd)
		RearrangeSplitters();

	CMDIChildWnd::OnSize(nType, cx, cy);
}

void COrderDataFrame::RearrangeSplitters()
{
	CRect rcFrame;
	GetClientRect(rcFrame);

	int nLeftToolViewWidth = 35;
	m_wndSplitter.SetColumnInfo(0, nLeftToolViewWidth, 10);
	m_wndSplitter.SetColumnInfo(1, rcFrame.Width() - nLeftToolViewWidth, 10);

	int nTopViewHeight = 0;
	if (m_browserInfo.m_nShowInfo & ShowProducts)
		nTopViewHeight = ( (m_browserInfo.m_nProductsViewHeight <= 0) || (m_browserInfo.m_nProductsViewHeight > 2000) ) ? 200 : m_browserInfo.m_nProductsViewHeight;
		
	int nHeight0 = nTopViewHeight;
	int nHeight1 = ((CFormView*)m_wndSplitter1.GetPane(1, 0))->GetTotalSize().cy - 10;	
	int nHeight2 = rcFrame.Height() - nHeight1 - nTopViewHeight;
	m_wndSplitter1.SetRowInfo	(0, (nHeight0 >= 0) ? nHeight0 : 0, 10);
	m_wndSplitter1.SetRowInfo	(1, (nHeight1 >= 0) ? nHeight1 : 0, 10);
	m_wndSplitter1.SetRowInfo	(2, (nHeight2 >= 0) ? nHeight2 : 0, 10);

	//int nWidth1 = 600;//((CFormView*)m_wndSplitter2.GetPane(0, 1))->GetTotalSize().cx;	
	//int nWidth0 = rcFrame.Width() - nLeftToolViewWidth - nWidth1 - 10;
	//m_wndSplitter2.SetColumnInfo(0, (nWidth0 >= 0) ? nWidth0 : 0, 10);
	//m_wndSplitter2.SetColumnInfo(1, (nWidth1 >= 0) ? nWidth1 : 0, 10);

	m_wndSplitter.RecalcLayout();
	m_wndSplitter1.RecalcLayout();
	m_wndSplitter2.RecalcLayout();
}

void COrderDataFrame::ActivateFrame(int nCmdShow)
{
	CMainFrame::RestoreFrameStatus(this, m_bStatusRestored, nCmdShow, &m_wndSplitter, &m_wndSplitter1, &m_wndSplitter2, NULL, NULL, sizeof(m_browserInfo), (LPBYTE)&m_browserInfo);

	CMDIChildWnd::ActivateFrame(nCmdShow);
}

void COrderDataFrame::OnDestroy()
{
	CMainFrame::SaveFrameStatus(this, &m_wndSplitter, &m_wndSplitter1, &m_wndSplitter2, NULL, NULL, sizeof(m_browserInfo), (LPBYTE)&m_browserInfo);

	CMDIChildWnd::OnDestroy();
}

void COrderDataFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	//CMDIChildWnd::OnUpdateFrameTitle(bAddToTitle);	// do nothing (done in CMainFrame)
	::SetWindowText(this->GetSafeHwnd(), _T(""));

	//CDocument* pDoc = GetActiveDocument();
	//if (pDoc)
	//{
	//	CString title = pDoc->GetTitle();
	//	//title += _T(" - ");
	//	//CString string;
	//	//string.LoadString(IDS_NAV_ORDERDATA);
	//	//title += string;
	//	::SetWindowText(this->GetSafeHwnd(), title);

		SetIcon(NULL, FALSE);
	//}
}

void COrderDataFrame::SetupViewProducts()
{
	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CProductsView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	int nWidth0, nWidth1, nDummy;
	m_wndSplitter2.GetColumnInfo(0, nWidth0, nDummy);
	m_wndSplitter2.GetColumnInfo(1, nWidth1, nDummy);

	if (m_browserInfo.m_nShowInfo & ShowProducts)
	{
		if ( ! m_wndSplitter2.GetPane(0, 0)->IsKindOf(RUNTIME_CLASS(CProductsView)))
		{
			if (m_browserInfo.m_nProductsViewHeight < 10)
				m_browserInfo.m_nProductsViewHeight = 250;

			m_wndSplitter1.SetRowInfo(0, m_browserInfo.m_nProductsViewHeight, 10);
			m_wndSplitter2.DeleteView(0, 0);
			m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CProductsView), CSize(nWidth0, m_browserInfo.m_nProductsViewHeight), &context);
			((CProductsView*)m_wndSplitter2.GetPane(0, 0))->OnInitialUpdate();
			m_wndSplitter2.DeleteView(0, 1);
			m_wndSplitter2.CreateView(0, 1, RUNTIME_CLASS(COrderDataView), CSize(nWidth1, m_browserInfo.m_nProductsViewHeight), &context);
			((CProductsView*)m_wndSplitter2.GetPane(0, 1))->OnInitialUpdate();
			m_wndSplitter1.RecalcLayout();
			m_wndSplitter2.RecalcLayout();
			RearrangeSplitters();	
		}
		else
		{
			if (m_browserInfo.m_nProductsViewHeight < 10)
				m_browserInfo.m_nProductsViewHeight = 250;
			m_wndSplitter1.SetRowInfo(0, m_browserInfo.m_nProductsViewHeight, 10);
			m_wndSplitter2.SetRowInfo(0, m_browserInfo.m_nProductsViewHeight, 10);
			m_wndSplitter1.RecalcLayout();
			m_wndSplitter2.RecalcLayout();
			RearrangeSplitters();	
		}
	}
	else
	{
		if ( ! m_wndSplitter2.GetPane(0, 0)->IsKindOf(RUNTIME_CLASS(CDummyView)))
		{
			m_wndSplitter1.SetRowInfo(0, 0, 10);
			m_wndSplitter2.DeleteView(0, 0);
			m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CDummyView), CSize(nWidth0, m_browserInfo.m_nProductsViewHeight), &context);
			m_wndSplitter2.DeleteView(0, 1);
			m_wndSplitter2.CreateView(0, 1, RUNTIME_CLASS(CDummyView), CSize(nWidth1, m_browserInfo.m_nProductsViewHeight), &context);
			m_wndSplitter1.RecalcLayout();
			m_wndSplitter2.RecalcLayout();
			RearrangeSplitters();	
		}
	}
}

CPrintComponentsView* COrderDataFrame::GetPrintComponentsView()
{
	if (m_wndSplitter3.m_hWnd)
	{
		CView* pView = (CView*)m_wndSplitter3.GetPane(0, 0);
		if (pView)
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintComponentsView)))	
				return (CPrintComponentsView*)pView;
	}

	return NULL;
}

CPrintSheetPlanningView* COrderDataFrame::GetPrintSheetPlanningView()
{
	if (m_wndSplitter1.m_hWnd)
	{
		CView* pView = (CView*)m_wndSplitter1.GetPane(2, 0);
		if (pView)
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))	
				return (CPrintSheetPlanningView*)pView;
	}

	return NULL;
}

CProductsView* COrderDataFrame::GetProductsView()
{
	if (m_wndSplitter2.m_hWnd)
	{
		CView* pView = (CView*)m_wndSplitter2.GetPane(0, 0);
		if (pView)
			if (pView->IsKindOf(RUNTIME_CLASS(CProductsView)))	
				return (CProductsView*)pView;
	}

	return NULL;
}

COrderDataView* COrderDataFrame::GetOrderDataView()
{
	if (m_wndSplitter2.m_hWnd)
	{
		CView* pView = (CView*)m_wndSplitter2.GetPane(0, 1);
		if (pView)
			if (pView->IsKindOf(RUNTIME_CLASS(COrderDataView)))	
				return (COrderDataView*)pView;
	}

	return NULL;
}

COrderDataFrameLeftToolsView* COrderDataFrame::GetOrderDataFrameLeftToolsView()
{
	if (m_wndSplitter.m_hWnd)
	{
		CView* pView = (CView*)m_wndSplitter.GetPane(0, 0);
		if (pView)
			if (pView->IsKindOf(RUNTIME_CLASS(COrderDataFrameLeftToolsView)))	
				return (COrderDataFrameLeftToolsView*)pView;
	}

	return NULL;
}
