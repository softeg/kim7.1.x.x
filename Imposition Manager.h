// Imposition Manager.h : main header file for the IMPOSITION MANAGER application
//


#pragma warning(disable : 4995)


#define PDF


#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <windowsx.h>
#include <winver.h>
#include <winspool.h>
#include <winbase.h>
//#include <ctl3d.h>

#include <conio.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <process.h>
#include <stdlib.h>

#include "resource.h"	// main symbols
#include "KIMResource.h"

#include "tbarcode.h"

#include "atlimage.h"
#include <gdiplus.h>
using namespace Gdiplus;

#include "DisplayList.h"
#include "GraphicList.h"
#include "JobInfoExchange.h" 
#include "BindingDefsSelection.h"

#include "ImpManDoc.h"
//#include "ImpManDocProduction.h"
//#include "ImpManDocProducts.h"
//#include "PDFTargetList.h"
//#include "ImpManDocPrintGroups.h"
//#include "ImpManDocDeprecated.h"

#include "DlgNumeration.h"		

#include "MeasureControl.h"		// special data exchange for measurement edit controls
#include "PageFormatSelection.h"
#include "DlgSettings.h"		// Definition of CSettings

#include "ImpManCtrls.h"		// Custom controls and other .......

#include "KIMFileDialog.h"

#include "PdfImpositionLib.h"

#include "kimAutoDB.h"
#include "KimSockets.h"


extern TCHAR barcodeTypeNamesV8[][40]; 


/////////////////////////////////////////////////////////////////////////////
// dummy-defines for resources without data
#define IDC_PAGELISTHEADER	3000

/////////////////////////////////////////////////////////////////////////////
// extended RGB defines for colors (SVGA-256 Colors)
#define BLACK		RGB(  0,  0,  0)	//0
#define BLUE		RGB(  0,  0,255)	//1
#define	GREEN		RGB(  0,255,  0)	//2
#define	RED			RGB(255,  0,  0)	//3
#define	CYAN		RGB( 0, 153,255)	//4
#define	PINK		RGB(255,  0,255)	//5
#define	YELLOW		RGB(255,255,  0)	//6
#define	WHITE		RGB(255,255,255)	//7
#define	DARKBLUE	RGB(  0,  0,128)	//8
#define	DARKGREEN	RGB(  0,128,  0)	//9
#define	MARINE		RGB(  0,128,128)	//10
#define	BORDEAUX	RGB(128,  0,  0)	//11
#define	PURPLE		RGB(128,  0,128)	//12
#define	OLIVE		RGB(128,128,  0)	//13
#define	DARKGRAY	RGB(128,128,128)	//14
#define	LIGHTGRAY	RGB(192,192,192)	//15
#define	PALEGREEN	RGB(192,220,192)	//16
#define	HEAVENBLUE	RGB(166,202,240)	//17
#define	CREME		RGB(255,251,240)	//18
#define	MIDDLEGRAY	RGB(160,160,164)	//19
// extended RGB defines for colors (SVGA-more than 256 Colors)
#define LIGHTRED	 RGB(255,128,128)	//20
#define LIGHTYELLOW	 RGB(255,255,128)	//21
#define	LIGHTGREEN	 RGB(128,255,  0)	//22
#define	LIGHTCYAN	 RGB(128,255,255)	//23
#define	LIGHTBLUE	 RGB(128,128,255)	//24
#define	LIGHTPINK	 RGB(255,128,255)	//25
#define	DARKPURPLE	 RGB( 64,  0, 64)	//26
#define	DARKBORDEAUX RGB( 64,  0,  0)	//27
#define	BROWN		 RGB(128, 64,  0)	//28
#define	OCKER		 RGB(255,128,  0)	//29
#define	DARKMARINE	 RGB(  0, 64,128)	//30
#define	MAGENTA		 RGB(255, 0, 128)

#define GUICOLOR_DLG_STATIC_TEXT	RGB(0, 87, 220)
#define GUICOLOR_VALUE				DARKGRAY//RGB(120,50,0) 
#define GUICOLOR_CAPTION			GUICOLOR_DLG_STATIC_TEXT
#define GUICOLOR_GROUP				RGB(244,244,238)
#define GUICOLOR_ITEM_HOT			RGB(252, 220, 150)

#define UNDEFINED -1


#define KIM_DARKBLUE RGB( 28, 58,115)

#define	PLATE_COLOR	 RGB(235,235,235)

#define KIM_PALETTE_NUMCOLORS	 2

#define PRINTSHEETVIEW_BKGRD	 PALETTEINDEX(0)
#define PRINTSHEETTREEVIEW_BKGRD PALETTEINDEX(1)

// colors for table views
#define TABLECOLOR_SEPARATION_LINE	RGB(245,230,175)
#define TABLECOLOR_BACKGROUND		RGB(254,246,196)
#define TABLECOLOR_BACKGROUND_DARK	RGB(252,240,184)


#define UpdateDemoStatusBarPane(pDoc)\
	{\
		if ((CMainFrame*)theApp.m_pMainWnd)\
		{\
			if (((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.m_hWnd)\
			{\
				int	nDocsDongleStatus  = (pDoc) ? pDoc->m_nDongleStatus : CImpManApp::StandardDongle;\
				if ( theApp.m_bDemoMode || (theApp.m_nDongleStatus == CImpManApp::ClientDongle) ||\
					 (nDocsDongleStatus == CImpManApp::NoDongle) || (nDocsDongleStatus == CImpManApp::ClientDongle) )\
					((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.SetPaneStyle(2, SBPS_NORMAL | SBT_OWNERDRAW);\
				else\
					((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.SetPaneStyle(2, SBPS_NOBORDERS | SBT_OWNERDRAW);\
				((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.m_pDoc = pDoc;\
				((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.Invalidate();((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.UpdateWindow();\
			}\
		}\
	}


class CSysToUTF8Converter
{
public:
	CSysToUTF8Converter() {};
	~CSysToUTF8Converter();

	IPL_Path_t* Convert(const CString& strSys);

protected:
	CArray <IPL_Path_t*, IPL_Path_t*> m_BufArray;
};



extern		  void		SerializeBitmap(CBitmap& rBitmap, CArchive& ar);
extern		  int		StrippingParamsMapOrientation(int nJDFOrientation);
extern inline int		Pixel2MM(CDC* pDC, int nValue);
extern inline int		MM2DP(CDC* pDC, int nMM);
extern inline int		DP2MM(CDC* pDC, int nDP);
extern inline int		Pixel2ConstMM(CDC* pDC, int nValue);
extern inline void		Ascii2Float(CString strBuffer, CString strControl, float* pValue1, float* pValue2 = NULL);
extern inline float		Ascii2Float(CString strBuffer);
extern inline CString	Float2Ascii(float fValue, int nDigits = 1);
extern inline CString	Int2Ascii(int nValue);
extern inline CString	GetStringToken(CString string, int nToken, CString strDelimiters);
extern inline CString	GetKeyValue(CArray <CString, CString&>& keyValueList, const CString& strSearchKey);
extern		  int		GetNumTextLines(CDC* pDC, const CString& rString, int nMaxWidth = 0);
extern		  void		DrawTextMeasured(CDC* pDC, const CString& strText, CRect rcText, int nFlags, CRect& rcBox);
extern		  CString	FormatQuantityString(long nValue);
extern		  BOOL		IsNumericString(const CString& string);
extern		  void 		KIMLogMessage	 (int	  nIDPrompt, int nType = MB_OK);
extern		  void 		KIMLogMessage	 (LPCTSTR lpszText,  int nType = MB_OK);
extern		  int		KIMOpenLogMessage(int	  nIDPrompt, int nType = MB_OK, int nDefaultReturn = IDOK);
extern		  int		KIMOpenLogMessage(LPCTSTR lpszText,  int nType = MB_OK, int nDefaultReturn = IDOK);
extern		  void		KimEngineAddMessageLog(int nMsgType, CString strMessage);
extern		  int		KimEngineNotifyServer(int nMsg, int nParam1 = -1, int nParam2 = -1, CString strParam3 = _T(""), CString strParam4 = _T(""), CString strParam5 = _T(""));
extern		  BOOL		KimEngineDBJobAbort(int nJobID);
extern		  int		KimEngineDBSetProduct(int nJobID, int nProductID, int nType, CString strName, CString strPartName, float fWidth, float fHeight, int nNumPages);
extern		  int		KimEngineDBGetInputPDFID(int nJobID, CString strFullpath);
extern		  int		KimEngineDBSetInputPDF(int nJobID, int nPdfID, CString strFullpath, int nNumPages = 0, int nStatus = -1);
extern		  int		KimEngineDBSetOutputSheet(int nJobID, int nPdfID, CPrintSheet& rPrintSheet, int nNumPages = 0, int nStatus = -1);
extern inline BOOL		CheckDonglePDF(BOOL bInitialCheck, BOOL bUpdateStatusBar);


static TCHAR callas_keycode[] = _T("YB3BJ37HU6JPDPEHY5CK9RJ5");


inline void SwapValues(float& fValue1, float& fValue2)
{
	float fTemp = fValue1; fValue1 = fValue2; fValue2 = fTemp;
};


const float fPSPoint = 0.3527777f;



/////////////////////////////////////////////////////////////////////////////
// KIM Command line informations 

class CKIMCommandLineInfo : public CCommandLineInfo
{
public:
	CKIMCommandLineInfo() { m_nShellCommand = CCommandLineInfo::FileNothing; 
							m_nParam = 0; m_nKIMCommand = -1; m_bQuiet = FALSE; m_strPDFDoc = ""; m_strJobTemplate = ""; }


public:
	int		m_nParam;
	int		m_nKIMCommand;
	BOOL	m_bQuiet;
	int		m_nJobID;
	CString m_strPDFDoc;
	CString m_strJobTemplate;
	CString m_strPDFInfoFile;
	CString m_strPreviewsFolder;
	CString m_strStartPage;


public:
	virtual void ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast);
};







/////////////////////////////////////////////////////////////////////////////


class CDlgShinglingAssistent;
class CDlgPDFLibStatus;
class CDlgMarkSet;


// Application
#define APPLICATION_BOOK   0
#define APPLICATION_LABELS 1

#define MAX_OBJECT_COLORS 9



class CImpManApp : public CWinApp
{
public:
	CImpManApp();

enum AppReturns	 {ProcessSuccessful = 0, ProcessCancelled = -1, PDFOutputError = -2};
enum Cmd		 {CmdNew, CmdOpen, CmdOpenOutputList, CmdGetOutputInfo, CmdCheckRegisteredPDFs, CmdIsRegistered, CmdRegister, CmdUnregister, CmdImpose, CmdImposeQuiet, 
				  CmdResetPageSource, CmdResetPageSourceX, CmdResetPageList, CmdPageList, CmdImposeCSV, CmdImposeXML, CmdImposeJDF, CmdDoOutput, CmdRunEngine};
enum GUIStyle	 {GUIQuiet, GUIStandard, GUINewTemplate, GUIOpenTemplate, GUIOpenOutputList, GUIAutoImpose, GUIPageList};
enum DongleStatus{NoDongle, StandardDongle, ClientDongle, AutoDongle, SmallFormatDongle, NewsDongle, DigitalDongle, AutoSmallFormatDongle};
enum AddOns		 {JDFMIS = 1, JDFLayCrImp = 2, JDFCutting = 4, JDFFolding = 8, Barcode = 16, Optimize = 32};
enum OSVersion	 {Unknown, Win2000, WinXP, WinVista, Win7, Win8};


// Global variables for use in application
	DWORD	m_dwPlatformId;
	int		m_nOSVersion;

	CMultiDocTemplate*	m_pAutoServerTemplate;	
	CMultiDocTemplate*	m_pOrderDataTemplate;	
	CMultiDocTemplate*	m_pProdDataTemplate;
	CMultiDocTemplate*	m_pJobDocumentationTemplate;
	CMultiDocTemplate*	m_pBookBlockTemplate;	
	CMultiDocTemplate*	m_pNewPrintSheetTemplate;
	CMultiDocTemplate*	m_pPageListTemplate;	
	CSettings			settings;				// Global settings from registry
	CPressDeviceList	m_PressDeviceList;		// Global list of PressDevices read from settings-file
	CPaperDefsList		m_paperDefsList;		// Global list of PaperDefs read from settings-file
	CProductionProfiles	m_productionProfiles;	// Global list of production profiles read from settings-file
	CDlgMarkSet*		m_pDlgMarkSet;
	CDlgPDFLibStatus*	m_pDlgPDFLibStatus;
	HCURSOR	m_CursorCopy;		// Cursor for DragAndDrop Copy
	HCURSOR	m_CursorNoDrop;		// Cursor for not allowed regions during DragAndDrop
	HCURSOR	m_CursorTrim;		// Cursor to show active Trim-function
	HCURSOR	m_CursorAlignSection; // Cursor to show the AlignSection-function
	HCURSOR	m_CursorMagnifyIncrease;	// Cursor to show Zoomin-Mode 
	HCURSOR	m_CursorMagnifyDecrease;	// Cursor to show Zoomout-Mode
	HCURSOR	m_CursorNoMagnify;	// Cursor for 1:1 Zoom-Mode
	HCURSOR	m_CursorStandard;	// Cursor for reset to standard outfit
	HCURSOR	m_CursorSepCut;		// Cursor to show active SeparationCut-function
	HCURSOR m_CursorSizeWE;
	HCURSOR m_CursorHand;

	BOOL	m_bInsideInitInstance;	// Indicates if we are during startup of application
	BOOL	m_bInsideNewJobLoop;	// we are just creating a new job - used to automatically start impositioning in printsheet view
	BOOL	m_bPDFEngineInitialized;

	HINSTANCE hResourceInst;

	CString m_strClassName;			// register class name
		
	CString strName;				// Application-Name		needed for About-Box and document information
	CString strVersion;				// Application-Version		""
	CString strCopyright;			// Copyright - entry	needed for About-Box
	CString strOriginalFilename;	// Application's original filename

	CString m_strRegistryKey;	// path for settings in registry

	BOOL	m_bDemoMode;
	int		m_nDongleStatus;
	int		m_nAddOnsLicensed;

	CString m_strPSFileNotFound;  // If a PS-document isn't found during loading-procedure
	CString m_strContentFileNotFound; // the user will informed about the missing files
	CString m_strPreviewFileNotFound; //	"""

	CString m_strTemp;		// Need for storing temporary string -> can be load and edited in dialogs
	CString m_strTempTitle;	// Used for set the windowtitle of the dialog for editing the m_strTemp

//	Used only for CipCut - filled with CommandLine
	float	m_fSheetXCipCut;	// Given SheetExtension in X
	float	m_fSheetYCipCut;	// Given SheetExtension in Y
	CString	m_strFileNameCipCut;// Given Filename for opening or creation

	CPalette m_kimPalette;
	int		 m_nGUIStyle;
	int		 m_nRemainingDays;
	int		 m_nAppError;

	CColorDefinitionTable		m_colorDefTable;
	CImpManDoc*					m_pActiveDoc;
//	class CSummaryFrame*		m_pSummaryFrame;
	int							m_nPreviousFrame, m_nActualFrame;

	CMemFile					m_undoBuffer;

	COLORREF	m_objectColors[MAX_OBJECT_COLORS];

	CString		m_strCoverName;
	int			m_nScreenDPI;

	CProductionProfile m_defaultProductionProfile;
	CBoundProductPart  m_defaultBoundProductPart;

	BOOL			m_bServerConnected;
	BOOL			m_bSocketsInitialized;
	kimAutoDB		m_kaDB;
	int				m_nDBCurrentJobID;
	CString			m_strDBCurrentJobFullpath;
	BOOL			m_bKimEngineAbort;
	CClientSocket	m_clientSocket;




public:
	BOOL	OnWizard();
	void	OnProperties();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
//	Global helper functions
	int		GetResolution(void);
	CView*	GetActiveView();
	CView*	GetView(CRuntimeClass *pClass);
	void	UpdateView(CRuntimeClass *pClass, BOOL bInvalidateDisplayList = FALSE);
	void	OnUpdateView(CRuntimeClass *pClass, CView* pSender = NULL, LPARAM lHint = 0, CObject* pHint = NULL, BOOL bInvalidateDisplayList = FALSE);
	void	ViewsResetDisplayList(CRuntimeClass *pClass);
	BOOL	IsViewVisible(CRuntimeClass *pClass);
	void	GetVersionInfo(void);	
	BOOL	SetMyCurrentDirectory(LPCTSTR lpPathName, BOOL bWithMessage);
	BOOL	DirectoryExist(LPCTSTR lpPathName);
	void	RemoveExtension(CString& strPath);
	BOOL	FindFileFolder(CString strFolder, CString strFile, CString& strTargetFolder);
	BOOL	FolderHasFile(const CString& strFolder, const CString& strFile);
	void	OpenIfAutoBackupFile(CCommandLineInfo& rCmdInfo);
	void	CreateKimPalette();
	int		ProcessCommand(CKIMCommandLineInfo& rCmdInfo, int &nError);	
	BOOL	CalledFromKPM();
	void	ResetLogFile();
	void	LogEntry(CString strOut1, CString strOut2 = _T(""), CString strOut3 = _T(""), CString strOut4 = _T(""));
	void	LogEntry(int nValue);
	void	LogEntry(CString strOut, BOOL bIsError);
	void	LogJobInfoError(CString strOut);
	int		MapPrintColor(int nColorDefinitionIndex,	CPageTemplate* pObjectTemplate = NULL, CPrintSheet* pPrintSheet = NULL, int nSide = 0);
	CString	MapPrintColor(const CString& rstrColorName, CPageTemplate* pObjectTemplate = NULL, CPrintSheet* pPrintSheet = NULL, int nSide = 0);
	BOOL	FirstInstance(CKIMCommandLineInfo& rCmdInfo);
	BOOL	DoPromptFileName(CString& fileName, UINT nIDSTitle, DWORD lFlags, BOOL bOpenFileDialog, CDocTemplate* pTemplate);
	void	SaveWindowPlacement	  (CWnd* pWnd, const CString& strWindowName);
	BOOL	RestoreWindowPlacement(CWnd* pWnd, const CString& strWindowName);
	CString GetDefaultInstallationFolder();
	CString GetMarksetInitialFolder(BOOL bAutoMarks = FALSE);
	BOOL	CImpManApp::CheckDongleCtP();
	CString RemoveFileExtension(const CString& strFilename, CString strExtension);
	CString GetRegistryKey(const CString& strKey, const CString strDefault);
	void	SetRegistryKey(const CString& strKey, CString strValue);
	CString BuildKIMTitle(const CString& strPrefix);
	HTHEME	_OpenThemeData(CWnd* pWnd, LPCWSTR pszClassList);
	void	_CloseThemeData(HTHEME hTheme);
	void	UndoSave(CMemFile* pMemFile = NULL);
	void	UndoRestore(CMemFile* pMemFile = NULL);
	int		CheckRemainingDays();
	CString GetDataFolder();


// Read the project files, count and add the lines
	void	CountProjectLines(void);

	void							MyReadRecentFileList();
	void							MyWriteRecentFileList();
	CString							GetAutoServerDatabasePath();
	CString							GetAutoServerHotFolderRoot();
	CString							GetAutoServerJobFolderRoot();
	class CAutoServerFrame*			GetAutoServerFrame();
	class CAutoServerToolsView*		GetAutoServerToolsView();
	class CAutoServerJobsView*		GetAutoServerJobsView();
	class CAutoServerWorkflowsView*	GetAutoServerWorkflowsView();
	class CAutoServerLogsView*		GetAutoServerLogsView();
	class CAutoServerDetailsView*	GetAutoServerDetailsView();
	void							UpdateAutoServerJobsView(BOOL bInvalidate = TRUE, BOOL bInvalidateDisplayList = TRUE);
	void							UpdateAutoServerWorkflowsView(BOOL bInvalidate = TRUE, BOOL bInvalidateDisplayList = TRUE);
	void							UpdateAutoServerLogsView(BOOL bInvalidate = TRUE, BOOL bInvalidateDisplayList = TRUE);
	void							UpdateAutoServerDetailsView(BOOL bInvalidate = TRUE, BOOL bInvalidateDisplayList = TRUE);
	int								AutoServerConnect();
	int								AutoServerDisconnect();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImpManApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL SaveAllModified();
	virtual CDocument* OpenDocumentFile(LPCTSTR lpszFileName);
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CImpManApp)
	afx_msg void OnAppAbout();
	afx_msg void OnSettingsProgram();
	afx_msg void OnFileOpen();
	afx_msg void OnFileNew();
	afx_msg void OnSettingsPressdevice();
	afx_msg void OnSettingsImpose();
	afx_msg void OnReferenceColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnIdle(LONG lCount);
	virtual int DoMessageBox(LPCTSTR lpszPrompt, UINT nType, UINT nIDPrompt);
};


/////////////////////////////////////////////////////////////////////////////

extern CImpManApp theApp;

const int MAX_TEXT = 256;	// For the "GetBuffer" - functions etc.

// CReflinePositionParams
const BYTE REFLINE_ANCHOR	 = 0;
const BYTE PAGE_ANCHOR		 = 1;
const BYTE FOLDSHEET_ANCHOR  = 2;
const BYTE PLATE_REFLINE	 = 0;
const BYTE PAPER_REFLINE	 = 1;
const BYTE OBJECT_REFLINE	 = 2;
const BYTE BBOXFRAME_REFLINE = 4;
// For CipCut -> Kind of Cut
const BYTE OBJECT = 2;
const BYTE TRIM	  = 4;
const BYTE SEARCH = 8;
// Kind of attribute of LayoutObject
const BYTE BLOCKHANDLE		 = 1;
const BYTE BLOCK_UPPER_RIGHT = 2;
const BYTE DUMMYOBJECT		 = 4;
const BYTE FIRST_CUT		 = 8;	// Only CIPCUT
const BYTE SECOND_CUT		 = 16;	// Only CIPCUT
// Which kind of view in PrintSheetView -> was set in PrintSheetTabView
const BYTE FRONTSIDE = 1;  // Shows only PrintSheet frontside 
const BYTE BACKSIDE  = 2;  // Shows only PrintSheet backside 
const BYTE BOTHSIDES = 4;  // Shows PrintSheet front- and backside
const BYTE ALLSIDES  = 8;  // Shows all PrintSheetsides of the actual job
// KindOfProduction types
const BYTE WORK_AND_BACK    = 0;	
const BYTE WORK_AND_TURN    = 1;	
const BYTE WORK_AND_TUMBLE  = 2;
const BYTE WORK_SINGLE_SIDE = 8;

const BYTE TURN_LEFTBOTTOMSIDE = 1;
const BYTE TURN_RIGHTTOPSIDE   = 2;
//
const BYTE PRINTSHEET = 0;
const BYTE FOLDSHEET  = 1;

// For use in DlgTableCtp
const BYTE TITLE_PRINTSHEET	= 1;
const BYTE TITLE_FORMAT		= 2;
const BYTE TITLE_LAYOUT		= 3;
const BYTE TITLE_SIDE		= 4;
const BYTE TITLE_PLATE		= 5;
const BYTE TITLE_TILE		= 6;
const BYTE TITLE_PAGEPATH	= 7;

//
const BYTE LEFT		= 1;
const BYTE RIGHT	= 2;
const BYTE BOTTOM	= 4;
const BYTE TOP		= 8;
const BYTE XCENTER  = 16;
const BYTE YCENTER  = 32;

const BYTE ROTATE0	 = 1;
const BYTE ROTATE90	 = 2;
const BYTE ROTATE180 = 4;
const BYTE ROTATE270 = 8;
const BYTE FLIP0	 = 16;
const BYTE FLIP90	 = 32;
const BYTE FLIP180   = 64;
const BYTE FLIP270   = 128;
//
const BYTE VERTICAL	  = 1;
const BYTE HORIZONTAL = 2;
const BYTE GRAIN_VERTICAL	= 1;
const BYTE GRAIN_HORIZONTAL	= 2;
const BYTE GRAIN_EITHER		= 4;
//
const int LEFT_TOP      =   1;
const int LEFT_MIDDLE   =   2;
const int LEFT_BOTTOM	=   4;
const int MIDDLE_TOP	=   8;
const int MIDDLE_BOTTOM =  16;
const int RIGHT_TOP     =  32;
const int RIGHT_MIDDLE  =  64;
const int RIGHT_BOTTOM  = 128;
const int MIDDLE_MIDDLE = 256;
//
const float PICA_POINTS = 0.352777f;
#define SET_NOPROCESSCOLOR(fC, fM, fY, fK) {fC = FLT_MAX; fM = FLT_MAX; fY = FLT_MAX; fK = FLT_MAX;}
#define INCLUDE_PROCESSCOLOR(fC, fM, fY, fK) ( (fC != FLT_MAX) && (fM != FLT_MAX) && (fY != FLT_MAX) && (fK != FLT_MAX) )

