#if !defined(AFX_DLGPAPERLIST_H__DE4310DB_E334_4FD2_9903_3B359A46057B__INCLUDED_)
#define AFX_DLGPAPERLIST_H__DE4310DB_E334_4FD2_9903_3B359A46057B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaperList.h : Header-Datei
//

#include "DlgPaperListEntry.h"



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPaperList 

class CDlgPaperList : public CDialog
{
	DECLARE_DYNCREATE(CDlgPaperList)

// Konstruktion
public:
	CDlgPaperList(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgPaperList)
	enum { IDD = IDD_PAPERLIST };
	CListCtrl	m_paperListTable;
	//}}AFX_DATA


public:
	CPaperList m_paperList;

protected:
	CDlgPaperListEntry m_dlgPaperListEntry;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPaperList)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPaperList)
	virtual BOOL OnInitDialog();
	afx_msg void OnClickPaperlistTable(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkPaperlistTable(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnAddPaperentry();
	afx_msg void OnRemovePaperentry();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPAPERLIST_H__DE4310DB_E334_4FD2_9903_3B359A46057B__INCLUDED_
