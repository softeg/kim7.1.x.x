// PrintSheetViewStrippingData.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetViewStrippingData.h"
#include "NewPrintSheetFrame.h"
#include "DlgPressDevice.h"
#include "DlgSheet.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "GraphicComponent.h"
#include "PrintSheetPlanningView.h"
#include "PageListView.h"
#include "DlgProductionManager.h"
#include "DlgFlatProductData.h"
#include "PrintComponentsView.h"
#include "PrintSheetComponentsView.h"



// CPrintSheetViewStrippingData-Formularansicht

IMPLEMENT_DYNCREATE(CPrintSheetViewStrippingData, CFormView)



CPrintSheetViewStrippingData::CPrintSheetViewStrippingData(CWnd* pParent /*=NULL*/)
	: CFormView(CPrintSheetViewStrippingData::IDD)
{
	m_boldFont.CreateFont(13, 0, 0, 0, FW_BOLD,	 FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_nToolBarHeight = 34;
}

CPrintSheetViewStrippingData::~CPrintSheetViewStrippingData()
{
	m_boldFont.DeleteObject();
}

void CPrintSheetViewStrippingData::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PD_LAYOUT_DATA_STATIC, m_layoutDataWindow);
}

#define ID_REMOVE_LAYOUT_PRINTSHEETS	10001
#define	ID_ADD_BLANK_SHEET				10002


BEGIN_MESSAGE_MAP(CPrintSheetViewStrippingData, CFormView)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_DRAWITEM()
	ON_COMMAND(ID_ADD_BLANK_SHEET, OnBlankSheet)
	ON_COMMAND(ID_REMOVE_PRINTSHEET, OnRemovePrintSheet)
	ON_COMMAND(ID_REMOVE_LAYOUT_PRINTSHEETS, OnRemoveLayoutPrintSheets)
	ON_UPDATE_COMMAND_UI(ID_REMOVE_LAYOUT_PRINTSHEETS, OnUpdateRemoveLayoutPrintSheets)
	ON_UPDATE_COMMAND_UI(ID_ADD_BLANK_SHEET, OnUpdateBlankSheet)
	ON_UPDATE_COMMAND_UI(ID_REMOVE_PRINTSHEET, OnUpdateRemovePrintSheet)
END_MESSAGE_MAP()


// CPrintSheetViewStrippingData-Meldungshandler


BOOL CPrintSheetViewStrippingData::OnEraseBkgnd(CDC* pDC)
{
	m_toolBar.ClipButtons(pDC);

	CRect rect;
    GetClientRect(rect);
	rect.top = m_nToolBarHeight;

	CRect rcHeader = rect;
	rcHeader.top = 0; rcHeader.bottom = m_nToolBarHeight;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);

	rcHeader.top = rcHeader.bottom - 3;
	pDC->FillSolidRect(rcHeader, RGB(215,220,225));

	return TRUE; //CScrollView::OnEraseBkgnd(pDC);
}

void CPrintSheetViewStrippingData::DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd)
{
	CRect rcHeader = rcFrame;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);
}

void CPrintSheetViewStrippingData::OnInitialUpdate()
{
	CRect rcButton; 
	m_toolBar.Create(this, GetParentFrame(), NULL, IDB_LAYOUTDATA_TOOLBAR, IDB_LAYOUTDATA_TOOLBAR_COLD, CSize(30, 25), &DrawToolsBackground);

	GetDlgItem(IDC_PD_LAYOUTDATA_TOOLFRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton);	rcButton.bottom -= 2;
	rcButton.right = rcButton.left + 30;
	m_toolBar.AddButton(-1, IDS_ADD,				ID_ADD_BLANK_SHEET,				rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);	
	m_toolBar.AddButton(-1, IDS_REMOVE_SHEET,		ID_REMOVE_PRINTSHEET,			rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);	
	m_toolBar.AddButton(-1, IDS_REMOVE_ALL_SHEETS,	ID_REMOVE_LAYOUT_PRINTSHEETS,	rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);	

	CFormView::OnInitialUpdate();

	CRect rcWin; 
	GetWindowRect(rcWin); ScreenToClient(rcWin); 
	rcWin.top = rcButton.bottom + 3;
	m_layoutDataWindow.MoveWindow(rcWin);

	CRect rcClient; GetClientRect(rcClient);
	SetScaleToFitSize(rcClient.Size());	//suppress scrollbars
}

void CPrintSheetViewStrippingData::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	UpdateDialogControls( this, TRUE );
}

void CPrintSheetViewStrippingData::OnBlankSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheetNavigationView*	pNavigationView	= (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	CPrintComponent				component		= GetActComponent();
	CPrintSheet*				pPrintSheet		= component.GetSuitableEmptyPrintSheet();
	if ( ! pPrintSheet)
	{
		pPrintSheet = component.GetFirstPrintSheet();
		if ( ! pPrintSheet)
			pPrintSheet = GetActPrintSheet();
		CPrintingProfile printingProfile = (pPrintSheet) ? pPrintSheet->GetPrintingProfile() : CPrintingProfile();
		pPrintSheet	= pDoc->m_PrintSheetList.AddBlankSheet(printingProfile, -1, &CPrintGroup());
	}

	if (pNavigationView)
	{
		pNavigationView->SetActPrintSheet(pPrintSheet, TRUE);
		pNavigationView->m_DisplayList.Invalidate();
		pNavigationView->Invalidate();
		pNavigationView->UpdateWindow();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),	NULL, 0, NULL, TRUE);
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));

		BOOL bUpdate = TRUE;
		if (m_layoutDataWindow.m_dlgComponentImposer.m_hWnd)
		{
			m_layoutDataWindow.m_dlgComponentImposer.InitData(TRUE);
			bUpdate = FALSE;
		}
		if (bUpdate)
		{
			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),			NULL, 0, NULL, TRUE);
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
		}
	}

	Invalidate();
	UpdateWindow();
}

void CPrintSheetViewStrippingData::OnRemovePrintSheet() 
{ 
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	pFrame->OnRemovePrintSheet();
}

void CPrintSheetViewStrippingData::OnRemoveLayoutPrintSheets()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strMsg;
	strMsg.LoadString(IDS_REMOVE_ALL_SHEETS);
	if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
		return;

	CPrintSheet* pPrintSheet = GetActPrintSheet();
	CLayout*	 pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return;

	CPrintComponent& rComponent = GetActComponent();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	pLayout->RemovePrintSheetInstance(INT_MAX);

	pDoc->m_Bookblock.Reorganize(NULL, NULL);
	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->SetModifiedFlag();										
	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	pDoc->m_PrintSheetList.InitializeFlatProductTypeLists();

	CPrintComponentsView* pComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));

	OnBlankSheet();	// switch to components empty sheet, because printgrouplist analyzation changes position of empty sheets in printsheet list

	if (pComponentsView)
		pComponentsView->SetActComponent(rComponent);

	if (m_layoutDataWindow.m_dlgComponentImposer.m_hWnd)
		m_layoutDataWindow.m_dlgComponentImposer.InitData(FALSE);

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),		NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	theApp.OnUpdateView(RUNTIME_CLASS(CProductsView),				NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetComponentsView),		NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));
}

void CPrintSheetViewStrippingData::OnUpdateRemoveLayoutPrintSheets(CCmdUI* pCmdUI)
{
	CLayout* pLayout = GetActLayout();
	BOOL bEnable = (pLayout) ? ((pLayout->GetNumPrintSheetsBasedOn() > 1) ? TRUE : FALSE) : FALSE;
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
}

void CPrintSheetViewStrippingData::OnUpdateBlankSheet(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetViewStrippingData::OnUpdateRemovePrintSheet(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	BOOL bEnable = (pFrame->GetActLayout() && (pFrame->m_nViewSide != ALLSIDES)) ? TRUE : FALSE;
	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
}

CPrintSheetNavigationView* CPrintSheetViewStrippingData::GetPrintSheetNavigationView()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->GetPrintSheetNavigationView();

	return NULL;
}

CPrintComponentsView* CPrintSheetViewStrippingData::GetPrintComponentsView()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return NULL;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->GetPrintComponentsView();

	return NULL;
}

CPrintSheet* CPrintSheetViewStrippingData::GetActPrintSheet()
{
	return CNewPrintSheetFrame::GetActPrintSheet();
}

CLayout* CPrintSheetViewStrippingData::GetActLayout()
{
	return CNewPrintSheetFrame::GetActLayout();
}

CPressDevice* CPrintSheetViewStrippingData::GetActPressDevice()
{
	return CNewPrintSheetFrame::GetActPressDevice();
}

int CPrintSheetViewStrippingData::GetActFlatProductIndex()
{
	CPrintComponent component = GetActComponent();
	if (component.IsNull())
		return -1;
	if (component.m_nType != CPrintComponent::Flat)
		return -1;

	return component.m_nFlatProductIndex;
}

CString CPrintSheetViewStrippingData::GetActPrintingProfileName()
{
	CPrintGroup& rPrintGroup = GetActPrintGroup();
	return rPrintGroup.GetPrintingProfile().m_strName;
}

CPrintComponent CPrintSheetViewStrippingData::GetActComponent()
{
	CPrintComponentsView* pView = GetPrintComponentsView();
	if ( ! pView)
		return CPrintComponent();

	return pView->GetActComponent();
}

CColorantList CPrintSheetViewStrippingData::GetActColorants()
{
	CColorantList colorantList;
	CDisplayItem* pDI = m_layoutDataWindow.m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CLayoutDataWindow, PrintColor));
	while (pDI)
	{
		int nIndex = (int)pDI->m_pData;
		if ( (nIndex >= 0) && (nIndex < m_layoutDataWindow.m_colorantList.GetSize()) )
			colorantList.Add(m_layoutDataWindow.m_colorantList[nIndex]);

		pDI = m_layoutDataWindow.m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CLayoutDataWindow, PrintColor));
	}

	return colorantList;
}

CPrintGroup& CPrintSheetViewStrippingData::GetActPrintGroup()
{
	static CPrintGroup nullPrintGroup;

	CPrintSheetNavigationView* pView = GetPrintSheetNavigationView();
	if ( ! pView)
		return nullPrintGroup;

	CPrintSheet* pPrintSheet = GetActPrintSheet();
	if (pPrintSheet)
		return pPrintSheet->GetPrintGroup();

	return nullPrintGroup;
}

BOOL CPrintSheetViewStrippingData::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_toolBar.m_pToolTip)            
		m_toolBar.m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

HBRUSH CPrintSheetViewStrippingData::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	UINT nID = pWnd->GetDlgCtrlID();
    if(nCtlColor == CTLCOLOR_STATIC)
    {
		pDC->SetBkMode(TRANSPARENT);

		switch (nID)
		{
		case IDC_PD_PROFILE_STATIC:
					pDC->SelectObject(&m_boldFont);
					break;

		default:	//pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
					break;
		}
    }

	return hbr;
}
