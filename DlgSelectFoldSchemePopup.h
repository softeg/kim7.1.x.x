#pragma once


#include "MyBitmapButton.h"
#include "DlgSelectPressDevicePopup.h"
#include "DlgSelectPrintLayoutPopup.h"
#include "ExtendedListCtrl.h"




// CAddBoundProductListCtrl

class CAddBoundProductListCtrl : public CExtendedListCtrl
{
	DECLARE_DYNAMIC(CAddBoundProductListCtrl)

public:
	CAddBoundProductListCtrl();
	virtual ~CAddBoundProductListCtrl();

public:
	enum EHighlight {HIGHLIGHT_NORMAL, HIGHLIGHT_ALLCOLUMNS, HIGHLIGHT_ROW};

	int m_nPrevSel;

protected:
	int m_nHighlight;		// Indicate type of selection highlighting


protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
};




// CAddBoundProductCustomPaintFrame

class CAddBoundProductCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CAddBoundProductCustomPaintFrame)

public:
	CAddBoundProductCustomPaintFrame();
	virtual ~CAddBoundProductCustomPaintFrame();

public:

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};




// CFoldSchemeDirList

class CFoldSchemeDirItem
{
public:
	CFoldSchemeDirItem() { m_nAttribute = -1; };
	CFoldSchemeDirItem(const CString& strFileName, int nAttribute) { m_strFileName = strFileName; m_nAttribute = nAttribute; };
	~CFoldSchemeDirItem() { m_strFileName.Empty(); };

enum {IsFoldScheme = 1, IsFolder = 2};

public:
	CString m_strFileName;
	int		m_nAttribute;
};


class CFoldSchemeDirList : public CArray <CFoldSchemeDirItem, CFoldSchemeDirItem&>
{
public:
	void inline			Sort() { qsort((void*)GetData(), GetSize(), sizeof(CFoldSchemeDirItem), CompareFoldSchemeDirItem); }
	static int inline	CompareFoldSchemeDirItem(const void *p1, const void *p2)
						{
							CFoldSchemeDirItem* pDirItem1 = (CFoldSchemeDirItem*)p1;
							CFoldSchemeDirItem* pDirItem2 = (CFoldSchemeDirItem*)p2;
							BOOL	  bSameType = FALSE;

							if (pDirItem1->m_nAttribute == CFoldSchemeDirItem::IsFolder)
								if (pDirItem2->m_nAttribute == CFoldSchemeDirItem::IsFoldScheme)
									return -1;
								else
									bSameType = TRUE;
							else
								if (pDirItem1->m_nAttribute == CFoldSchemeDirItem::IsFoldScheme)
									if (pDirItem2->m_nAttribute == CFoldSchemeDirItem::IsFolder)
										return 1;
									else
										bSameType = TRUE;

							if (bSameType)
							{
								if (pDirItem1->m_strFileName.CompareNoCase(pDirItem2->m_strFileName) < 0)
									return -1;
								else
									if (pDirItem1->m_strFileName.CompareNoCase(pDirItem2->m_strFileName) > 0)
										return 1;
									else
										return 0;
							}

							return 0;
						}
	CString				FindScheme(CString strFoldSchemeName);
};



// CDlgSelectFoldSchemePopup-Dialogfeld

class CDlgSelectFoldSchemePopup : public CDialog
{
	DECLARE_DYNAMIC(CDlgSelectFoldSchemePopup)

public:
	CDlgSelectFoldSchemePopup(int nXPos = -1, int nYPos = -1, int nWidth = -1, int nHeight = -1, CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgSelectFoldSchemePopup();

// Dialogfelddaten
	enum { IDD = IDD_SELECT_FOLDSCHEME_POPUP };


public:
	BOOL								m_bVisible;
	BOOL								m_bStatic;	// will not be closed when clicked outside
	CAddBoundProductCustomPaintFrame	m_productThumbnail;
	CPoint								m_ptInitialPos;
	int									m_nDockingSide;
	BOOL								m_bLocked;
	CFont								m_boldFont;
	void								(*m_pFnSelChangeHandler)(const CString& strFolder, const CString& strScheme, int nNUp, CDlgSelectFoldSchemePopup* pDlg);
	int									m_nXPos, m_nYPos, m_nWidth, m_nHeight;
	CImageList*							m_pSchemeImageList;
	CString								m_strCurrentSchemeFolder;
	CFoldSchemeDirList					m_schemeDirList;
	CMyBitmapButton						m_undoButton;
	CBrush*								m_pBrowserHeaderBrush;
	CBrush*								m_pBkBrush;
	int									m_nNumMaxSheets;
	CFoldSheet							m_selectedFoldSheet;


public:
	void						InitData();
	void						SaveData();
	void						InitSchemes();
	BOOL						FindSchemeFolder(CString strFolder, CString strScheme);
	BOOL						FolderHasScheme(const CString& strFolder, const CString& strScheme);
	void						InitSchemeList();
	CPrintSheet*				GetActPrintSheet();
	CLayout*					GetActLayout();
	CPrintComponent				GetActComponent();
	CFoldSheet*					GetActFoldSheet();
	CBoundProductPart*			GetActBoundProductPart();
	BOOL						GetActProductPartIndex();
	class CDlgComponentImposer*	GetDlgComponentImposer();
	CFoldSheet*					GetSelectedFoldSheet();
	void						UndoSave();

protected:
	BOOL						SchemeIsRelevant(CFoldSheet& rFoldSheet, int nNumPagesToImpose);
	void						DrawSchemeThumbnail(CDC* pDC, CRect rcFrame, const CString& strScheme);


public:
	int			m_nNumSheets;
protected:
	CListCtrl	m_schemeList;
	CString		m_strNumRestPages;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();
	virtual void PostNcDestroy();
	afx_msg void OnLvnItemchangedPdSchemelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedDefineSchemes();
	afx_msg void OnNMCustomdrawProddataSchemelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
	afx_msg void OnDeltaposPdNumSheetsSpin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnEnKillfocusPdNumSheets();
	afx_msg void OnBnClickedPdMaxNumSheets();
	afx_msg void OnBnClickedPdMinNumSheets();
	afx_msg void OnNMDblclkPdSchemelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedPdFolderUp();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
#pragma once



