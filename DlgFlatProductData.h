#pragma once
#include "afxwin.h"
#include "PaperDefsSelection.h"
#include "MyBitmapButton.h"
#include "afxcmn.h"
#include "PrintComponentsView.h"


// CFPDCustomPaintColorsFrame

class CFPDCustomPaintColorsFrame : public CStatic
{
	DECLARE_DYNAMIC(CFPDCustomPaintColorsFrame)

public:
	CFPDCustomPaintColorsFrame();
	virtual ~CFPDCustomPaintColorsFrame();

public:
	void DrawColors(CDC* pDC, CRect frameRect);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};

// CFPDCustomPaintFrame

class CFPDCustomPaintPagesFrame : public CStatic
{
	DECLARE_DYNAMIC(CFPDCustomPaintPagesFrame)

public:
	CFPDCustomPaintPagesFrame();
	virtual ~CFPDCustomPaintPagesFrame();

public:
	void DrawPages(CDC* pDC, CRect frameRect);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};



// CDlgFlatProductData-Dialogfeld

class CDlgFlatProductData : public CDialog
{
	DECLARE_DYNAMIC(CDlgFlatProductData)

public:
	CDlgFlatProductData(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgFlatProductData();

// Dialogfelddaten
	enum { IDD = IDD_FLAT_PRODUCT_DATA };


public:
	CPoint						m_ptInitialPos;
	CPrintComponentsView*		m_pPrintComponentsView;
	BOOL						m_bInitFinished;
	CFlatProductList*			m_pFlatProductList;
	CFlatProduct				m_product;
	float						m_fOldWidth;
	float						m_fOldHeight;
	int							m_nFlatProductIndex;
	CFPDCustomPaintColorsFrame	m_colorsPreviewFrame;
	CFPDCustomPaintPagesFrame	m_pagesPreviewFrame;
	CPaperDefsSelection			m_paperDefsSelection;
	CPageFormatSelection		m_PDPageFormatSelection;
	CString						m_strPDFormatName;
	CString						m_strPaper;
	int							m_nPaperGrammage;
	float						m_fPaperVolume;
	CFont		 				m_boldFont;
	CMyBitmapButton				m_prevProductButton, m_nextProductButton;


public:
	void	InitData();
	void	SaveData();
	void	CheckSingleDoubleSideExposures(int nFlatProductIndex);
	CString InitPaperInfo();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnCbnSelchangePdPaperCombo();

public:
	CSpinButtonCtrl	m_PDQuantitySpin;
	CSpinButtonCtrl	m_PDQuantity1Spin;
	CSpinButtonCtrl	m_PDQuantity2Spin;
	CSpinButtonCtrl	m_PDPagesSpin;
	CComboBox	m_PDFormatCombo;
	CSpinButtonCtrl	m_PDWidthSpin;
	CSpinButtonCtrl	m_PDHeightSpin;
	CSpinButtonCtrl	m_rightTrimSpin;
	CSpinButtonCtrl	m_leftTrimSpin;
	CSpinButtonCtrl	m_topTrimSpin;
	CSpinButtonCtrl	m_bottomTrimSpin;
	CSpinButtonCtrl	m_groupSpin;
	CSpinButtonCtrl	m_prioritySpin;
	CSpinButtonCtrl m_duplicateSpin;
	CString m_strName;
	CString m_strComment;
	int		m_nPDPages;
	float	m_fPDHeight;
	float	m_fPDWidth;
	int		m_nPDPageOrientation;
	int		m_nPDQuantity;
	int		m_nNumPagesComboIndex;
	float	m_fTopTrim;
	float	m_fBottomTrim;
	float	m_fRightTrim;
	float   m_fLeftTrim;
	CComboBox m_paperCombo;
	int m_nPaperGrain;
	CString m_strPunchTool;
	int m_nGroup;
	int m_nPriority;
	CString m_strPaperInfo;
	int m_nNumDuplicate;
	afx_msg void OnBnClickedPdModifyFormat();
	afx_msg void OnDeltaposPDHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposPDWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEnKillfocusPdHeight();
	afx_msg void OnEnKillfocusPdWidth();
	afx_msg void OnPDLandscape();
	afx_msg void OnPDPortrait();
	afx_msg void OnSelchangePDFormat();
	afx_msg void OnSelchangePDBinding();
	afx_msg void OnDeltaposPDQuantitySpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposPDQuantity1Spin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposPDQuantity2Spin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedPdPaperButton();
	afx_msg void OnBnClickedPdColorsButton();
	afx_msg void OnDeltaposBottomtrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposToptrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposRighttrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposLefttrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusBottomtrimEdit();
	afx_msg void OnKillfocusToptrimEdit();
	afx_msg void OnKillfocusRighttrimEdit();
	afx_msg void OnKillfocusLefttrimEdit();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedPdPreviousButton();
	afx_msg void OnBnClickedPdNextButton();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedPdNewproductButton();
	COleDateTime m_printDeadline;
	afx_msg void OnBnClickedPdDuplicateProductButton();
	afx_msg void OnEnChangePdNduplicateEdit();
	afx_msg void OnBnClickedFlatproductSelectPunchtool();
	afx_msg void OnBnClickedFlatproductPunchtoolRemove();
};
