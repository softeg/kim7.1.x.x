#if !defined(AFX_PLACEHOLDEREDIT_H__7F8B4190_E29A_42F7_8FB1_52F285FF706F__INCLUDED_)
#define AFX_PLACEHOLDEREDIT_H__7F8B4190_E29A_42F7_8FB1_52F285FF706F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PlaceholderEdit.h : Header-Datei
//

#define ID_MSG_UPDATE_PLACEHOLDER_COMMENT 9999

/////////////////////////////////////////////////////////////////////////////
// Fenster CPlaceholderEdit 

class CPlaceholderEdit : public CEdit
{
// Konstruktion
public:
	CPlaceholderEdit();
	~CPlaceholderEdit();

// Attribute
public:
	CWnd*		  m_pParent;
	CMenu*		  m_pPlaceholderMenu;
	CToolTipCtrl* m_pToolTip;
	CString		  m_strPlaceholderHit;
	int			  m_nIDButton;
	void		 (*m_pfnOnChangeHandler)(CDialog* pDlg);

// Operationen
public:

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPlaceholderEdit)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementierung
public:
	BOOL		IsValidPlaceholder(CString strToCheck);
	CString		GetPlaceholderHit();
	static BOOL	IsExtSyntax(TCHAR ch1, TCHAR ch2, CString strPlaceholder);
	CString		GotoNext(TCHAR **ppStart, TCHAR **ppEnd);

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CPlaceholderEdit)
	afx_msg void OnPaint();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnChange();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_PLACEHOLDEREDIT_H__7F8B4190_E29A_42F7_8FB1_52F285FF706F__INCLUDED_
