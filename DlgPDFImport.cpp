// DlgPDFImport.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgPDFImport.h"
#include "PageListFrame.h"


// CDlgPDFImport-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgPDFImport, CDialog)

CDlgPDFImport::CDlgPDFImport(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPDFImport::IDD, pParent)
{
	m_pImageList = new CImageList();
}

CDlgPDFImport::~CDlgPDFImport()
{
	if (m_pImageList)
		delete m_pImageList;
}

void CDlgPDFImport::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PDFIMPORT_PAGESOURCES, m_pageSourcesCtrl);
}


BEGIN_MESSAGE_MAP(CDlgPDFImport, CDialog)
	ON_BN_CLICKED(IDOK, &CDlgPDFImport::OnBnClickedOk)
	ON_BN_CLICKED(IDC_PDFIMPORT_OPEN, &CDlgPDFImport::OnBnClickedPdfimportOpen)
	ON_BN_CLICKED(IDC_PDFIMPORT_ACROBAT, &CDlgPDFImport::OnBnClickedPdfimportAcrobat)
	ON_BN_CLICKED(IDC_PDFIMPORT_BOUNDPRODUCT, &CDlgPDFImport::OnBnClickedPdfimportBoundproduct)
	ON_BN_CLICKED(IDC_PDFIMPORT_FLATPRODUCT, &CDlgPDFImport::OnBnClickedPdfimportFlatproduct)
	ON_BN_CLICKED(IDC_PDFIMPORT_SINGLESIDED, &CDlgPDFImport::OnBnClickedPdfimportSinglesided)
	ON_BN_CLICKED(IDC_PDFIMPORT_DOUBLESIDED, &CDlgPDFImport::OnBnClickedPdfimportDoublesided)
	ON_BN_CLICKED(IDC_PDFIMPORT_SETTINGS, &CDlgPDFImport::OnBnClickedPdfimportSettings)
END_MESSAGE_MAP()


// CDlgPDFImport-Meldungshandler

BOOL CDlgPDFImport::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString string; 
	string.LoadString(IDS_FILE_LABEL);
	m_pageSourcesCtrl.InsertColumn(0, string,	LVCFMT_LEFT, 300, 0);
	
	string.LoadString(IDS_PAGES_HEADER);
	m_pageSourcesCtrl.InsertColumn(1, string,	LVCFMT_LEFT,  80, 1);

	string.LoadString(IDS_PRODUCT);
	m_pageSourcesCtrl.InsertColumn(2, string,	LVCFMT_LEFT,  80, 2);

	string.LoadString(IDS_SIDED);
	m_pageSourcesCtrl.InsertColumn(3, string,	LVCFMT_LEFT, 100, 3);

	CBitmap bitmap;
	m_pImageList->Create(16, 16, ILC_COLOR8, 3, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_PDF_FILE);
	m_pImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	m_pageSourcesCtrl.SetImageList(m_pImageList, LVSIL_SMALL);

	m_dlgPDFImportSettings.LoadSettings();

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgPDFImport::OnBnClickedPdfimportOpen()
{
	CString strInitialFolder;
	if (CImpManDoc::GetDoc()->m_PageSourceList.IsEmpty())	// Take path from settings
		strInitialFolder = theApp.settings.m_szPDFInputFolder;
	else
	{														// Take path from last page source
		CPageSource& rPageSource  = CImpManDoc::GetDoc()->m_PageSourceList.GetTail();
		TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
		_tsplitpath((LPCTSTR)rPageSource.m_strDocumentFullpath, szDrive, szDir, szFname, szExt);
		strInitialFolder  = szDrive;
		strInitialFolder += szDir;
		CFileStatus status;
		if ( ! CFile::GetStatus(strInitialFolder + _T("."), status))	// if folder not present, take from settings
			strInitialFolder = theApp.settings.m_szPDFInputFolder;
	}

	CPageSource* pPageSource = new CPageSource;

	theApp.m_pDlgPDFLibStatus->m_pPageSource = pPageSource;
	theApp.m_pDlgPDFLibStatus->LaunchPDFEngine(strInitialFolder, this, CDlgPDFLibStatus::RegisterPages, _T(""), TRUE);
	theApp.m_pDlgPDFLibStatus->m_pPageSource = NULL;

	pPageSource->m_PageSourceHeaders.RemoveAll();
	delete pPageSource;
}

void CDlgPDFImport::PDFEngineDataExchange(CPageSource* pNewObjectSource)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! pNewObjectSource)
		return;
	if (pNewObjectSource->m_PageSourceHeaders.GetSize() <= 0)
		return;

	int	nNumPDFPages = pNewObjectSource->m_PageSourceHeaders.GetSize();
	int nSides		 = (nNumPDFPages == 1) ? 1 : 2;
	int nProductType = (nNumPDFPages <= 2) ? 1 : 0;

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath((LPCTSTR)pNewObjectSource->m_strDocumentFullpath, szDrive, szDir, szFname, szExt);

	PAGESOURCE_PROPS props = {*pNewObjectSource, nNumPDFPages, nProductType, nSides};
	m_pageSourceList.Add(props);

	CString strBound;	strBound.LoadString(IDS_BOUND);
	CString strUnbound; strUnbound.LoadString(IDS_UNBOUND);
	CString string;

	int nItem	 = m_pageSourcesCtrl.GetItemCount();
	int nSubItem = 1;
	m_pageSourcesCtrl.InsertItem(nItem, szFname, 0);

	string.Format(_T("%d"), props.m_nNumPages);
	m_pageSourcesCtrl.SetItemText(nItem, nSubItem++, (LPTSTR)(LPCTSTR)string);

	string = (props.m_nProductType == 0) ? strBound : strUnbound;
	m_pageSourcesCtrl.SetItemText(nItem, nSubItem++, string);

	string.Format(_T("%d"), props.m_nSides);
	m_pageSourcesCtrl.SetItemText(nItem, nSubItem++, string);

	m_pageSourcesCtrl.SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);

	UpdateData(FALSE);

	pNewObjectSource->m_PageSourceHeaders.RemoveAll();
	*pNewObjectSource = CPageSource();
}

void CDlgPDFImport::OnBnClickedPdfimportAcrobat()
{
	POSITION pos = m_pageSourcesCtrl.GetFirstSelectedItemPosition();
	if ( ! pos)
		return;

	PAGESOURCE_PROPS& rProps = m_pageSourceList.GetAt(m_pageSourcesCtrl.GetNextSelectedItem(pos));

	CPageListFrame::LaunchAcrobat(rProps.m_pageSource.m_strDocumentFullpath, -1);
}

void CDlgPDFImport::OnBnClickedPdfimportBoundproduct()
{
	CString strBound; strBound.LoadString(IDS_BOUND);
	POSITION pos = m_pageSourcesCtrl.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nItem = m_pageSourcesCtrl.GetNextSelectedItem(pos);
		PAGESOURCE_PROPS& rProps = m_pageSourceList.GetAt(nItem);
		rProps.m_nProductType = 0;

		m_pageSourcesCtrl.SetItemText(nItem, 2, strBound);
	}
	UpdateData(FALSE);
}

void CDlgPDFImport::OnBnClickedPdfimportFlatproduct()
{
	CString strUnbound; strUnbound.LoadString(IDS_UNBOUND);
	POSITION pos = m_pageSourcesCtrl.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nItem = m_pageSourcesCtrl.GetNextSelectedItem(pos);
		PAGESOURCE_PROPS& rProps = m_pageSourceList.GetAt(nItem);
		rProps.m_nProductType = 1;

		m_pageSourcesCtrl.SetItemText(nItem, 2, strUnbound);
	}
	UpdateData(FALSE);
}

void CDlgPDFImport::OnBnClickedPdfimportSinglesided()
{
	POSITION pos = m_pageSourcesCtrl.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nItem = m_pageSourcesCtrl.GetNextSelectedItem(pos);
		PAGESOURCE_PROPS& rProps = m_pageSourceList.GetAt(nItem);
		rProps.m_nSides = 1;

		m_pageSourcesCtrl.SetItemText(nItem, 3, _T("1"));
	}
	UpdateData(FALSE);
}

void CDlgPDFImport::OnBnClickedPdfimportDoublesided()
{
	POSITION pos = m_pageSourcesCtrl.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nItem = m_pageSourcesCtrl.GetNextSelectedItem(pos);
		PAGESOURCE_PROPS& rProps = m_pageSourceList.GetAt(nItem);
		rProps.m_nSides = 2;

		m_pageSourcesCtrl.SetItemText(nItem, 3, _T("2"));
	}
	UpdateData(FALSE);
}

void CDlgPDFImport::OnBnClickedOk()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
	{
		OnOK();
		return;
	}

	for (int i = 0; i < m_pageSourceList.GetSize(); i++)
	{
		PAGESOURCE_PROPS& rProps = m_pageSourceList.GetAt(i);

		int	nNumPDFPages = rProps.m_nNumPages;
		int nSides		 = rProps.m_nSides;

		int nProductPartIndex = ALL_PARTS;
		if (rProps.m_nProductType == 0)
			nProductPartIndex = ImportPDFBound(&rProps.m_pageSource);
		else
			nProductPartIndex = ImportPDFFlats(&rProps.m_pageSource, nNumPDFPages, nSides);

		pDoc->m_PageSourceList.AddTail(rProps.m_pageSource);
		int nNumPageSourceHeaders = pDoc->m_PageSourceList.GetTail().m_PageSourceHeaders.GetSize();
		int nCount				  = (int)ceil(float(nNumPDFPages * nSides) / (float)nNumPageSourceHeaders);
		while (nCount)
		{
			pDoc->m_PageTemplateList.AutoAssignPageSource(&pDoc->m_PageSourceList.GetTail(), nProductPartIndex);
			nCount--;
		}
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
	}

	pDoc->SetModifiedFlag();
	pDoc->UpdateAllViews(NULL);

	OnOK();
}

int CDlgPDFImport::ImportPDFFlats(CPageSource* pNewObjectSource, int nNumPDFPages, int nSides)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return ALL_PARTS;

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath((LPCTSTR)pNewObjectSource->m_strDocumentFullpath, szDrive, szDir, szFname, szExt);

	CFlatProductList flatProducts;

	int nNumProducts = nNumPDFPages / nSides;
	int nStep		 = (nSides == 1) ? 1 : 2;
	int nNameIndex	 = 1;
	for (int i = 0; i < pNewObjectSource->m_PageSourceHeaders.GetSize(); i = i + nStep, nNameIndex++)
	{
		float fTrimSizeWidth, fTrimSizeHeight;
		if (pNewObjectSource->m_PageSourceHeaders[i].HasTrimBox())
		{
			fTrimSizeWidth  = pNewObjectSource->m_PageSourceHeaders[i].m_fTrimBoxRight  - pNewObjectSource->m_PageSourceHeaders[i].m_fTrimBoxLeft;
			fTrimSizeHeight = pNewObjectSource->m_PageSourceHeaders[i].m_fTrimBoxTop	- pNewObjectSource->m_PageSourceHeaders[i].m_fTrimBoxBottom;
		}
		else
		{
			fTrimSizeWidth  = pNewObjectSource->m_PageSourceHeaders[i].m_fBBoxRight	- pNewObjectSource->m_PageSourceHeaders[i].m_fBBoxLeft;
			fTrimSizeHeight = pNewObjectSource->m_PageSourceHeaders[i].m_fBBoxTop	- pNewObjectSource->m_PageSourceHeaders[i].m_fBBoxBottom;
		}
		switch (theApp.settings.m_iUnit)
		{
		case MILLIMETER:	fTrimSizeWidth  = (long)(fTrimSizeWidth  * 10.0f + 0.5f) / 10.0f;	// round to 1 digit after decimal
							fTrimSizeHeight = (long)(fTrimSizeHeight * 10.0f + 0.5f) / 10.0f;
							break;
		case CENTIMETER:	
		case INCH:			fTrimSizeWidth  = (long)(fTrimSizeWidth  * 100.0f + 0.5f) / 100.0f;	// round to 2 digits after decimal
							fTrimSizeHeight = (long)(fTrimSizeHeight * 100.0f + 0.5f) / 100.0f;
							break;
		}

		CString strPostFix;
		if (nNumProducts > 1)
			strPostFix.Format(_T("-%d"), nNameIndex);
		else
			strPostFix = _T("");

		CFlatProduct flatProduct;
		flatProduct.m_strProductName	= m_dlgPDFImportSettings.FindAndReplace(szFname + strPostFix);
		flatProduct.m_nNumPages			= nSides;
		flatProduct.m_nDesiredQuantity	= 1;
		flatProduct.m_fTrimSizeWidth	= fTrimSizeWidth;	
		flatProduct.m_fTrimSizeHeight	= fTrimSizeHeight;

		flatProduct.m_colorantList.RemoveAll();
		for (int i = 0; i < pNewObjectSource->m_PageSourceHeaders[0].m_processColors.GetSize(); i++)
		{
			CColorant colorant;
			colorant.m_strName = pNewObjectSource->m_PageSourceHeaders[0].m_processColors[i];
			colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
			flatProduct.m_colorantList.Add(colorant);
		}
		for (int i = 0; i < pNewObjectSource->m_PageSourceHeaders[0].m_spotColors.GetSize(); i++)
		{
			CColorant colorant;
			colorant.m_strName = pNewObjectSource->m_PageSourceHeaders[0].m_spotColors[i];
			colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
			flatProduct.m_colorantList.Add(colorant);
		}

		flatProducts.AddTail(flatProduct);
	}

	POSITION pos = flatProducts.GetHeadPosition();
	while (pos)
	{
		pDoc->m_flatProducts.AddTail(flatProducts.GetNext(pos));
	}

	int nProductPartIndex = pDoc->GetFlatProductsIndex();
	pDoc->m_PageTemplateList.SynchronizeToFlatProductList(pDoc->m_flatProducts, nProductPartIndex);
	pDoc->m_PageTemplateList.FindPrintSheetLocations();

	return nProductPartIndex;
}

int CDlgPDFImport::ImportPDFBound(CPageSource* pNewObjectSource)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return ALL_PARTS;

	float fTrimSizeWidth, fTrimSizeHeight;
	if (pNewObjectSource->m_PageSourceHeaders[0].HasTrimBox())
	{
		fTrimSizeWidth  = pNewObjectSource->m_PageSourceHeaders[0].m_fTrimBoxRight  - pNewObjectSource->m_PageSourceHeaders[0].m_fTrimBoxLeft;
		fTrimSizeHeight = pNewObjectSource->m_PageSourceHeaders[0].m_fTrimBoxTop	- pNewObjectSource->m_PageSourceHeaders[0].m_fTrimBoxBottom;
	}
	else
	{
		fTrimSizeWidth  = pNewObjectSource->m_PageSourceHeaders[0].m_fBBoxRight	- pNewObjectSource->m_PageSourceHeaders[0].m_fBBoxLeft;
		fTrimSizeHeight = pNewObjectSource->m_PageSourceHeaders[0].m_fBBoxTop	- pNewObjectSource->m_PageSourceHeaders[0].m_fBBoxBottom;
	}
	switch (theApp.settings.m_iUnit)
	{
	case MILLIMETER:	fTrimSizeWidth  = (long)(fTrimSizeWidth  * 10.0f + 0.5f) / 10.0f;	// round to 1 digit after decimal
						fTrimSizeHeight = (long)(fTrimSizeHeight * 10.0f + 0.5f) / 10.0f;
						break;
	case CENTIMETER:	
	case INCH:			fTrimSizeWidth  = (long)(fTrimSizeWidth  * 100.0f + 0.5f) / 100.0f;	// round to 2 digits after decimal
						fTrimSizeHeight = (long)(fTrimSizeHeight * 100.0f + 0.5f) / 100.0f;
						break;
	}

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	_tsplitpath((LPCTSTR)pNewObjectSource->m_strDocumentFullpath, szDrive, szDir, szFname, szExt);

	CString strProductName		= szFname;
	CString strSubProductName;	strSubProductName.LoadString(IDS_CONTENT);
	CString strProductType		= "BoundContent";
	CString strBindingOrder		= "Gathering";
	int		nNumPages			= pNewObjectSource->m_PageSourceHeaders.GetSize();

	int nBoundProductIndex = -1;
	if ( (strProductType.CompareNoCase(_T("BoundCover")) == 0) || (strProductType.CompareNoCase(_T("BoundContent")) == 0) )
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.FindProduct(strProductName);
		if (rBoundProduct.IsNull())
		{
			CBoundProduct newBoundProduct;
			newBoundProduct.m_strProductName  = strProductName;
			newBoundProduct.m_fTrimSizeWidth  = fTrimSizeWidth;
			newBoundProduct.m_fTrimSizeHeight = fTrimSizeHeight;
			if (strBindingOrder.CompareNoCase(_T("Gathering")) == 0)
			{
				newBoundProduct.m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::PerfectBound;
				newBoundProduct.m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_PERFECTBOUND_TOOLTIP);
			}
			else
			{
				newBoundProduct.m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::SaddleStitched;
				newBoundProduct.m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_SADDLESTITCHED_TOOLTIP);
			}
			pDoc->m_boundProducts.AddTail(newBoundProduct);
			nBoundProductIndex = pDoc->m_boundProducts.GetSize() - 1;
		}
		else
			nBoundProductIndex = rBoundProduct.GetIndex();

		CBoundProductPart product;
		product.m_strName			= strSubProductName;
		product.m_strComment		= _T("");
		product.m_fPageWidth		= fTrimSizeWidth;	
		product.m_fPageHeight		= fTrimSizeHeight;
		product.m_nDesiredQuantity	= 1;//nDesiredQuantity;
		product.m_strPaper			= _T("");//strPaper;		
		//product.m_nPaperGrain
		product.m_nNumPages			= nNumPages;
		//product.m_colorantList		= m_colorantList;
		//product.m_printDeadline
		if ( (product.m_colorantList.GetSize() <= 0) && (pNewObjectSource->m_PageSourceHeaders.GetSize() > 0) )
		{
			for (int i = 0; i < pNewObjectSource->m_PageSourceHeaders[0].m_processColors.GetSize(); i++)
			{
				CColorant colorant;
				colorant.m_strName = pNewObjectSource->m_PageSourceHeaders[0].m_processColors[i];
				colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
				product.m_colorantList.Add(colorant);
			}
			for (int i = 0; i < pNewObjectSource->m_PageSourceHeaders[0].m_spotColors.GetSize(); i++)
			{
				CColorant colorant;
				colorant.m_strName = pNewObjectSource->m_PageSourceHeaders[0].m_spotColors[i];
				colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
				product.m_colorantList.Add(colorant);
			}
			if (pNewObjectSource->m_PageSourceHeaders[0].HasTrimBox())
			{
				product.m_fPageWidth  = pNewObjectSource->m_PageSourceHeaders[0].m_fTrimBoxRight  - pNewObjectSource->m_PageSourceHeaders[0].m_fTrimBoxLeft;
				product.m_fPageHeight = pNewObjectSource->m_PageSourceHeaders[0].m_fTrimBoxTop	  - pNewObjectSource->m_PageSourceHeaders[0].m_fTrimBoxBottom;
			}
			else
			{
				product.m_fPageWidth  = pNewObjectSource->m_PageSourceHeaders[0].m_fBBoxRight - pNewObjectSource->m_PageSourceHeaders[0].m_fBBoxLeft;
				product.m_fPageHeight = pNewObjectSource->m_PageSourceHeaders[0].m_fBBoxTop	  - pNewObjectSource->m_PageSourceHeaders[0].m_fBBoxBottom;
			}
			switch (theApp.settings.m_iUnit)
			{
			case MILLIMETER:	product.m_fPageWidth  = (long)(product.m_fPageWidth  * 10.0f + 0.5f) / 10.0f;	// round to 1 digit after decimal
								product.m_fPageHeight = (long)(product.m_fPageHeight * 10.0f + 0.5f) / 10.0f;
								break;
			case CENTIMETER:	
			case INCH:			product.m_fPageWidth  = (long)(product.m_fPageWidth  * 100.0f + 0.5f) / 100.0f;	// round to 2 digits after decimal
								product.m_fPageHeight = (long)(product.m_fPageHeight * 100.0f + 0.5f) / 100.0f;
								break;
			}
		}
		pDoc->m_boundProducts.GetProduct(nBoundProductIndex).m_parts.Add(product);
		int nProductPartIndex = pDoc->m_boundProducts.GetProduct(nBoundProductIndex).m_parts[pDoc->GetBoundProduct(nBoundProductIndex).m_parts.GetSize() - 1].GetIndex();

		pDoc->m_PageTemplateList.ShiftProductPartIndices(nProductPartIndex);
		pDoc->m_Bookblock.ShiftProductPartIndices(nProductPartIndex);
		pDoc->m_PageTemplateList.InsertProductPartPages(product.m_nNumPages, nBoundProductIndex, nProductPartIndex);

		return nProductPartIndex;
	}

	return ALL_PARTS;
}

void CDlgPDFImport::OnBnClickedPdfimportSettings()
{
	m_dlgPDFImportSettings.DoModal();
}
