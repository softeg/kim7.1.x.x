#pragma once


// CDlgEditMarkRule-Dialogfeld

class CDlgEditMarkRule : public CDialog
{
	DECLARE_DYNAMIC(CDlgEditMarkRule)

public:
	CDlgEditMarkRule(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgEditMarkRule();

// Dialogfelddaten
	enum { IDD = IDD_EDIT_MARKRULE};

	enum{MarkRules, ProductionProfileRules};


public:
	int		m_nRuleSet;
	CString m_strVariable;
	CString m_strOperator;
	CString m_strValue;


public:
	void InitVariableCombo();
	void InitVariableComboMarkRules();
	void InitVariableComboProductionProfileRules();
	void InitValueCombo();
	void InitValueComboMarkRules();
	void InitValueComboProductionProfileRules();


protected:
	CComboBox m_leftOperandCombo;
	CComboBox m_operatorCombo;
	CComboBox m_rightOperandCombo;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeRuleLeftoperandCombo();
};
