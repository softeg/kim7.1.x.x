#pragma once


#include "OwnerDrawnButtonList.h"
#include "afxwin.h"




// CPageListTopToolsView-Formularansicht

class CPageListTopToolsView : public CFormView
{
	DECLARE_DYNCREATE(CPageListTopToolsView)

protected:
	CPageListTopToolsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPageListTopToolsView();

public:
	enum { IDD = IDD_PAGELISTTOPTOOLSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


public:
	COwnerDrawnButtonList	m_pageListToolbar;
	CImageList				m_productImageList;


public:
	static void	UpdateBackground(CDC* pDC, CRect rcRect, CWnd* pWnd);


public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnUpdatePageslist(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePagesource(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePagedetail(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePageprops(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePagination(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteSelection(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRegisterPDFDoc(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOpenPDFDoc(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRefreshPagesources(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoom(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomDecrease(CCmdUI* pCmdUI);
public:
	CComboBoxEx m_productCombo;
	afx_msg void OnCbnSelchangePagelistToolbarCombo();
};


