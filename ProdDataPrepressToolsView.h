#pragma once


#include "OwnerDrawnButtonList.h"
#include "ToolBarGroup.h"



// CProdDataPrepressToolsView-Formularansicht

class CProdDataPrepressToolsView : public CFormView
{
	DECLARE_DYNCREATE(CProdDataPrepressToolsView)

protected:
	CProdDataPrepressToolsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CProdDataPrepressToolsView();

public:
	enum { IDD = IDD_PRODDATAPREPRESSTOOLSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	COwnerDrawnButtonList	m_zoomToolBar, m_displayToolBar;
	CToolBarGroup			m_zoomToolBarGroup, m_displayToolBarGroup;
	CNewPrintSheetFrame*	m_pNewPrintSheetFrame;

public:
	virtual void OnInitialUpdate();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnUpdateZoom(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomDecrease(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomFullview(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScrollHand(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewFrontside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBackside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBothsides(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMeasureCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateExposuresCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBitmapCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateReleaseBleed(CCmdUI* pCmdUI);
};


