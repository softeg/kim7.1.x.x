// DlgImpositionScheme.h : header file
//




/////////////////////////////////////////////////////////////////////////////
// CSchemeWindow window

  

class CSchemeWindow : public CStatic
{
// Construction
public:
	CSchemeWindow();

// View modes:
	enum ViewMode {FrontSide, BackSide, BothSides};

// Attributes
public:
	BOOL	     m_bMouseButtonPressed;
	CRect		 m_FrontSheetRect, m_BackSheetRect;
	CDisplayList m_DisplayList;
	DECLARE_DISPLAY_ITEM(CSchemeWindow, Page,	 OnSelect, NO, OnDeselect, NO, OnActivate, YES, OnDeactivate, YES, OnDropped, NO,  OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CSchemeWindow, RefEdge, OnSelect, NO, OnDeselect, NO, OnActivate, NO,  OnDeactivate, NO,  OnDropped, YES, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)

	DECLARE_INPLACE_EDIT(InplaceEdit, CSchemeWindow);


// Operations
public:
	static void OnCharEdit(CWnd* pWnd, UINT nChar);
	void DisplaySheetSide(CDC* pDC, int nViewMode);
	void CalcExtends(CRect* pSheetRect, int nx, int ny, CRect* pRect);
	void DisplayHeadPosition(CDC* pDC, CRect& rect, int nHeadPosition, int nForm);
	void DisplayRefEdge(CDC* pDC, unsigned* pRefEdge, int nForm, int nBorder);
	int  GetPageIndex(CArray<short, short>& rPageArray, short* pData);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSchemeWindow)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSchemeWindow();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSchemeWindow)
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CDlgImpositionScheme dialog


class CDlgBindingAssistent;


class CDlgImpositionScheme : public CFileDialog
{
// Construction
public:
	CDlgImpositionScheme(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL,
		CDlgBindingAssistent* pDlg = NULL);

	~CDlgImpositionScheme();


// Dialog Data
	//{{AFX_DATA(CDlgImpositionScheme)
	enum { IDD = IDD_IMPOSITION_SCHEME };
	CSpinButtonCtrl	m_LeftPagesSpin;
	CSpinButtonCtrl	m_RightPagesSpin;
	CSpinButtonCtrl	m_BlockNySpin;
	CSpinButtonCtrl	m_BlockNxSpin;
	CComboBox	m_CurrentLayerCombo;
	CSpinButtonCtrl	m_nLayersSpin;
	CSchemeWindow	m_SchemeWindow;
	UINT	m_nLayers;
	int		m_nCurrentLayer;
	UINT	m_nBlockNx;
	UINT	m_nBlockNy;
	int		m_nViewMode;
	CString	m_strNotes;
	UINT	m_nLeftPages;
	UINT	m_nRightPages;
	//}}AFX_DATA


private:
	CRect				  m_layoutPreviewFrame;
	CFont				  m_smallFont;
	CBrush				  m_BkBrush;

public:
	CPrintSheet			  m_printSheet;
	CLayout				  m_layout;
	CPressDevice		  m_pressDevice;
	CFoldSheet			  m_foldSheet;
	CDlgBindingAssistent* m_pDlgBindingAssistent;
	BOOL				  m_bClickedOK;
	BOOL				  m_bStandAlone;
	CString				  m_strInitialDir;

// Operations
public:
	void				 UpdateFoldSheet();
	CFoldSheetLayer&	 GetCurrentLayer() {return m_foldSheet.m_LayerList.GetAt(m_foldSheet.m_LayerList.FindIndex(m_nCurrentLayer));};
	static UINT_PTR APIENTRY HookProcedure(HWND hdlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgImpositionScheme)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL
	virtual void OnFileNameChange();

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDlgImpositionScheme)
	virtual BOOL OnInitDialog();
	afx_msg void OnInner();
	afx_msg void OnOuter();
	afx_msg void OnBoth();
	afx_msg void OnSelchangeCurrentLayer();
	virtual void OnCancel();
	afx_msg void OnAddScheme();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual void OnOK();
	afx_msg void OnApplyButton();
	afx_msg void OnCalcRear();
	afx_msg void OnDeltaposLeftPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposRightPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


