// DlgConnectToServer.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgConnectToServer.h"


// CDlgConnectToServer-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgConnectToServer, CDialog)

CDlgConnectToServer::CDlgConnectToServer(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgConnectToServer::IDD, pParent)
{
	m_nProgressCount = 0;
}

CDlgConnectToServer::~CDlgConnectToServer()
{
}

void CDlgConnectToServer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CONNECTION_PROGRESS, m_progressBar);
}


BEGIN_MESSAGE_MAP(CDlgConnectToServer, CDialog)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CDlgConnectToServer-Meldungshandler

BOOL CDlgConnectToServer::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString strText, strStatic;
	GetDlgItem(IDC_CONNECT_TO_STATIC)->GetWindowText(strStatic);
	strText.Format(strStatic, theApp.settings.m_szAutoServerNameIP);
	GetDlgItem(IDC_CONNECT_TO_STATIC)->SetWindowText(strText);

	m_progressBar.SetRange(0, 60);
	m_progressBar.SetPos(m_nProgressCount = 0);
 
	SetTimer(0, 500, NULL);  

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgConnectToServer::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 0)
	{
		if (m_nProgressCount > 4)	// wait 2 seconds to let user read message
		{
			if (theApp.m_bServerConnected)
				EndDialog(IDOK);
			else
				if (m_nProgressCount > 60)
				{
					KillTimer(nIDEvent);
					CString strMsg;
					AfxFormatString1(strMsg, IDS_SERVER_NOT_FOUND, theApp.settings.m_szAutoServerNameIP);
					AfxMessageBox(strMsg);
					EndDialog(IDCANCEL);
				}
		}

		m_progressBar.SetPos(m_nProgressCount++);
	}

	CDialog::OnTimer(nIDEvent);
}
