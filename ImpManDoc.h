// ImpManDoc.h : interface of the CImpManDoc class
//
/////////////////////////////////////////////////////////////////////////////


#pragma once

//#include "ImpManDocProducts.h"
//#include "ImpManDocProduction.h"

//#include "BindingDefsSelection.h"
//#include "tbarcode.h"
#include "PDFEngineInterface.h"


#include "atlimage.h"
#include <gdiplus.h>
using namespace Gdiplus;


// constants belonging to document
enum PageOrientation {PORTRAIT, LANDSCAPE};



// Declaration of classes belonging to document:



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CGlobalData

class CGlobalData : public CObject
{
public:
    DECLARE_SERIAL( CGlobalData )
    CGlobalData();

//Attributes:
public:
	CString		 m_strDocumentVersion;
	unsigned	 m_nCreationSoftware;
	CString		 m_strCreationSoftwareName;
	CString		 m_strJob;
	CString		 m_strJobNumber;
	CString		 m_strCustomer;
	CString		 m_strCustomerNumber;
	CString		 m_strCreator;
	CString		 m_strPhoneNumber;
	COleDateTime m_CreationDate;
	COleDateTime m_LastModifiedDate;
	CString		 m_strScheduledToPrint;
	CString		 m_strNotes;
	unsigned	 m_nMeasuringUnit;
	CString		 m_strJobTicketFile;


//Operations:
    void	Serialize( CArchive& archive );
	CString GetVersionNumber();
	CRect	Draw(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL, BOOL bShowExtended = FALSE);
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////



/////////////////////////// CPaperListEntry /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CPaperListEntry : public CObject
{
public:
    DECLARE_SERIAL( CPaperListEntry )
	CPaperListEntry();
	CPaperListEntry(const CPaperListEntry& rPaperListEntry); // copy constructor

//Attributes:
public:
	CString m_strPaperName;
	CString m_strPressName;
	CString m_strLayoutName;
	CString m_strFormatName;
	int		m_nNumSheets;
	long	m_nQuantityTotal;
	CString m_strComment;

	class CPrintSheet* m_pPrintSheet;		// temporary - not serialized

//Operations:
public:
	const CPaperListEntry& operator= (const CPaperListEntry& rPaperListEntry);
	void				   Copy(const CPaperListEntry& rPaperListEntry);
};

template <> void AFXAPI SerializeElements <CPaperListEntry> (CArchive& ar, CPaperListEntry* pPaperListEntry, INT_PTR nCount);


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CFanoutList

class CFanoutDefinition : public CObject
{
public:
    DECLARE_SERIAL( CFanoutDefinition )

	CFanoutDefinition();
	CFanoutDefinition(CString& strColorName, float fFanoutX, float fFanoutY);


//Attributes:
public:
	CString m_strColorName;
	float	m_fFanoutX;
	float	m_fFanoutY;

//Operations:
	const CFanoutDefinition& operator=(const CFanoutDefinition& rFanoutDefinition);
};

template <> void AFXAPI SerializeElements <CFanoutDefinition> (CArchive& ar, CFanoutDefinition* pFanoutDefinition, INT_PTR nCount);
void AFXAPI DestructElements(CFanoutDefinition* pFanoutDefinition, INT_PTR nCount);



class CFanoutList : public CArray <CFanoutDefinition, CFanoutDefinition&>  
{
//Operations:
public:
	const CFanoutList&	operator= (const CFanoutList& rFanoutList);
	BOOL				operator==(const CFanoutList& rFanoutList);
	BOOL				operator!=(const CFanoutList& rFanoutList);
	int   FindColor (const CString& strColorName);
	float GetFanoutX(const CString& strColorName);
	float GetFanoutY(const CString& strColorName);
};

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CSheet CSheetList 
//

class CSheetOldStyle
{
// Attributes
public:
	char	m_szName[16];
	float	m_fWidth;
	float	m_fHeight;
};

class CSheet : public CObject
{
public:
    DECLARE_SERIAL( CSheet )
	CSheet();
	CSheet(CSheet& rSheet);

// Attributes
public:
	CString		m_strSheetName;
	float		m_fWidth;
	float		m_fHeight;
	int			m_nPaperGrain;
	CFanoutList m_FanoutList;

//Operations:
	const CSheet& operator= (const CSheet& rSheet);
	BOOL		  operator==(const CSheet& rSheet);
	BOOL		  operator!=(const CSheet& rSheet);
	void Copy(const CSheet& rSheet);
};

template <> void AFXAPI SerializeElements <CSheet> (CArchive& ar, CSheet* pSheet, INT_PTR nCount);
void AFXAPI DestructElements(CSheet* pSheet, INT_PTR nCount);


class CSheetList : public CList <CSheet,CSheet&>
{
public:
	POSITION FindByName(const CString& strName);
	CSheet*  FindBySize(float fWidth, float fHeight);
	int		 FindIndexByName(const CString& strName);
	BOOL	 Load();
};

/////////////////////////////////////////////////////////////////////////////




///////////////////////////// CPaperList ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CPaperList : public CArray <CPaperListEntry, CPaperListEntry&> 
{
public:
    DECLARE_SERIAL( CPaperList )
	CPaperList();

//Attributes:
public:

//Operations:
public:
	void			 UpdateEntries();
	CPaperListEntry* FindEntry(const CString& strPaperName, const CString& strPressName, const CString& strLayoutName, const CString& strFormatName);
	CRect			 Draw(CDC* pDC, CRect rcFrame, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList = NULL);
	CRect			 PrintHeader(CDC* pDC, CRect rcFrame);
	CRect			 DrawPaperListEntryRow(CDC* pDC, CRect rcFrame, CPaperListEntry& rEntry, CArray <int, int>& rColumnWidths, int nPaperTypeTotal, CDisplayList* pDisplayList = NULL);
//    void Serialize( CArchive& archive );
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


////////////////////////// CProductionData //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CProductionData : public CObject
{
public:
    DECLARE_SERIAL( CProductionData )
	CProductionData();

//Attributes:
public:
	float		m_fHeadTrim, m_fSideTrim, m_fFootTrim, m_fBackTrim;
	unsigned	m_nProductionType;		// Normal or ComingAndGoing
	CPaperList	m_paperList;

//Operations:
public:
    void Serialize( CArchive& archive );
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


////////////////////////// CColorDefinitionTable ////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CColorDefinition : public CObject
{																	
public:
    DECLARE_SERIAL( CColorDefinition )
	CColorDefinition();
	CColorDefinition(CString strColorName, COLORREF rgb, int nColorType, int nColorIndex) 
	{
		m_strColorName = strColorName; m_rgb = rgb; m_nColorType = nColorType; m_nColorIndex = nColorIndex;
	};
	CColorDefinition(CString strColorName, COLORREF rgb, COLORREF cmyk, int nColorType) 
	{
		m_strColorName = strColorName; m_rgb = rgb; m_cmyk = cmyk; m_nColorType = nColorType; m_nColorIndex = -1;
	};


enum colorTypes{ProcessC = 0, ProcessM = 1, ProcessY = 2, ProcessK = 3, Spot = 4, Alternate = 5}; 
enum sepStatus {Separated = 1, Unseparated = 2, Mixed = 4};
 

//Attributes:
public:
	CString  m_strColorName;
	COLORREF m_rgb;			// display color
	COLORREF m_cmyk;
	int		 m_nColorType;
	int		 m_nColorIndex;
	CList	 <CString, CString&> m_AliasNameList;

//Operations:
	const			CColorDefinition& operator=(const CColorDefinition& rColorDefinition);
	void			Copy(const CColorDefinition& rColorDefinition);
	static void		CheckProcessColorPlausibility(CColorDefinition* pColorDefinition);
	static COLORREF RGB2CMYK(COLORREF crRGB, CColorDefinition* pColorDefinition = NULL);
	static COLORREF CMYK2RGB(COLORREF crCMYK);
	static COLORREF	LAB2RGB (float fL, float fA, float fB);
	static COLORREF	LAB2CMYK(float fL, float fA, float fB);
	BOOL			IsProcessColor() { if ( (m_nColorType == ProcessC) || (m_nColorType == ProcessM) || (m_nColorType == ProcessY) || (m_nColorType == ProcessK) ) return TRUE; else return FALSE; };
};

template <> void AFXAPI SerializeElements <CColorDefinition> (CArchive& ar, CColorDefinition* pColorDefinition, INT_PTR nCount);
void AFXAPI DestructElements(CColorDefinition* pColorDefinition, INT_PTR nCount);



class CColorDefinitionTable : public CArray <CColorDefinition, CColorDefinition&>	// CArray is derived from CObject
{																					// - so serialization will run
public:
    DECLARE_SERIAL( CColorDefinitionTable )
	CColorDefinitionTable();


//Attributes:

//Operations:
	short			  InsertColorDef(CColorDefinition& rColorDefToInsert);
	void			  Load();
	void			  Save();
	CString			  GetColorName			  (int nColorDefinitionIndex);
	CString			  FindColorReferenceName  (const CString& strColor);
	COLORREF		  FindColorRef			  (const CString& strColor);
	short			  FindColorDefinitionIndex(const CString& strColor);
	CColorDefinition* FindColorDefinition	  (int nColorIndex);	// attention: nColorIndex does not mean nColorDefinitionIndex
	CColorDefinition* FindColorDefinition	  (const CString& strColor);
	CString			  GetCyanName();
	CString			  GetMagentaName();
	CString			  GetYellowName();
	CString			  GetKeyName();
	void			  UpdateColorInfos();
	int				  GetColorIndex(int nColorDefinitionIndex);
	int				  GetCurSpotColorNum(CString& strColor);
	BOOL			  IsProcessColor(CString& strColor);
	BOOL			  IsCyan(CString& strColor);
	BOOL			  IsMagenta(CString& strColor);
	BOOL			  IsYellow(CString& strColor);
	BOOL			  IsKey(CString& strColor);
};



class CSpotcolorDefs : public CObject
{
public:
    DECLARE_SERIAL( CSpotcolorDefs )
	CSpotcolorDefs();
	CSpotcolorDefs(const CSpotcolorDefs& rSpotcolorDefs); // copy constructor
	~CSpotcolorDefs();

public:
	CString m_strSpotColorName;
	struct alternateValues
	{
		float fC, fM, fY, fK;
	}m_alternateValues;

public:
	const CSpotcolorDefs&	operator= (const CSpotcolorDefs& rSpotcolorDefs);
	void					Copy(const CSpotcolorDefs& rSpotcolorDefs);
};

class CSpotcolorDefList : public CArray <CSpotcolorDefs, CSpotcolorDefs&>
{
public:
	void	 AddEntry(CSpotcolorDefs& entry);
	void	 SetEntry(CSpotcolorDefs& entry);
	void	 InsertSpotcolorDefs(CSpotcolorDefs &spotcolorDefs);
	BOOL	 IsSpotcolorDefined(const CString& strSpotColorName);
	COLORREF GetAlternateCMYK(const CString& strSpotColorName);
	COLORREF GetAlternateRGB(const CString& strSpotColorName);
};

template <> void AFXAPI SerializeElements <CSpotcolorDefs> (CArchive& ar, CSpotcolorDefs* pSpotcolorDefs, INT_PTR nCount);
void AFXAPI DestructElements(CSpotcolorDefs* pSpotcolorDefs, INT_PTR nCount);

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////





////////////////////////// CPageSourceList ///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CPageSourceHeader : public CObject
{																	
public:
    DECLARE_SERIAL( CPageSourceHeader )
	CPageSourceHeader();
	CPageSourceHeader(const CPageSourceHeader& rPageSourceHeader); // copy constructor
	~CPageSourceHeader();

//Attributes:
public:
	CString			m_strPageName;
	int				m_nPageNumber;
	BOOL			m_bPageNumberFix;
	CString			m_strColorName;
	float			m_fColorIntensity;	// used for system created marks
	COLORREF		m_rgb;		// display color
	int				m_nColorIndex;
	float			m_fBBoxLeft, m_fBBoxBottom, m_fBBoxRight, m_fBBoxTop;
	float			m_fTrimBoxLeft, m_fTrimBoxBottom, m_fTrimBoxRight, m_fTrimBoxTop;
	BOOL			m_bOutputBlocked;
	CStringArray	m_processColors;
	CStringArray	m_spotColors;
	Image*			m_pThumbnailImage;

	CArray <class CPageTemplate*, class CPageTemplate*> m_PageTemplateRefs;	// used only at runtime - so not to be serialized


//Operations:
public:
	const CPageSourceHeader& operator= (const CPageSourceHeader& rPageSourceHeader);
	BOOL					 operator==(const CPageSourceHeader& rPageSourceHeader);
	BOOL					 operator!=(const CPageSourceHeader& rPageSourceHeader);
	void					 Copy(const CPageSourceHeader& rPageSourceHeader);
	void					 Create(int nPageNum, const CString& strPageName, float fBBoxLeft, float fBBoxBottom, float fBBoxRight, float fBBoxTop, 
									const CString& strColorName, BOOL bDefineColorPrompt = TRUE); // registering PDF-file
	void					 Create(int nIndex, const CString& strFilePath, int nColorIndex); // registering BMP-file
	void					 Create(int nIndex);	// Assign manually
	class CPageTemplate*	 GetFirstPageTemplate();
	class CPageTemplate*	 GetNextPageTemplate(CPageTemplate* pPageTemplate);
	BOOL					 PreviewExists(class CPageSource* pPageSource, int nColorSepIndex = -1);	
	void					 GetProcessColors(CStringArray& plateNames);
	void					 GetSpotColors(CStringArray& plateNames);
	int						 GetColorType();
	BOOL					 HasCyan();
	BOOL					 HasMagenta();
	BOOL					 HasYellow();
	BOOL					 HasKey();
	BOOL					 HasTrimBox() { return ( (m_fTrimBoxLeft != 0.0f) || (m_fTrimBoxBottom != 0.0f) || (m_fTrimBoxRight != 0.0f) || (m_fTrimBoxTop != 0.0f) ) ? TRUE : FALSE; };
};

template <> void AFXAPI SerializeElements <CPageSourceHeader> (CArchive& ar, CPageSourceHeader* pPageSourceHeader, INT_PTR nCount);
void AFXAPI DestructElements(CPageSourceHeader* pPageSourceHeader, INT_PTR nCount);



class CPageSource : public CObject
{																	
public:
    DECLARE_SERIAL( CPageSource )
	CPageSource();
	CPageSource(const CPageSource& rPageSource); // copy constructor


enum SourceType{PSFile, PDFFile, Bitmap, SystemCreated}; 
enum Colorant{ColorantUnknown = 0, Separated = 1, Composite = 2};


//Attributes:
public:
	int		m_nType;
	CString	m_strDocumentFullpath;
	CString	m_strContentFilesPath;
	CString	m_strPreviewFilesPath;
	CString	m_strDocumentTitle;
	CString	m_strCreator;
	CString	m_strCreationDate;
	CString m_strSystemCreationInfo;
	int 	m_nColorant;
	float	m_fGlobalBBoxLeft, m_fGlobalBBoxBottom, m_fGlobalBBoxRight, m_fGlobalBBoxTop;
	CTime	m_pdfInfoDate;
	CArray <CPageSourceHeader, CPageSourceHeader&>	
			m_PageSourceHeaders;
	BOOL	m_bBmpFP;
	BOOL	m_bPreviewsInHiresFolder;

	BOOL	m_bFontsCopied;	// temporary only, not be serialized
	BOOL	m_bDocExist;

	CSpotcolorDefList m_spotcolorDefList;

//Operations:
	const CPageSource& operator= (const CPageSource& rPageSource);
	BOOL			   operator==(const CPageSource& rPageSource);
	BOOL			   operator!=(const CPageSource& rPageSource);
	void			   Copy(const CPageSource& rPageSource);
	BOOL			   Create(int nType, const CString& strDocumentFullpath, const CString& strContentFilesPath, const CString& strPreviewFilesPath);
	static IPL_bool_t  EnumPagePropertyCallBack( IPL_TENUMPAGEPROPERTY* pageprop, void* data );
	void			   RegisterPDFPages(const CString& strFileName, const CString& strInfoFileName, int nStartPage = -1);
	void			   UpdatePDFInfo(const CString& strFileName, const CString& strInfoFileName);
	void			   ReorganizePageNumbers();
	short			   GetHeaderIndex(CPageSourceHeader* pPageSourceHeader);
	void			   Clear();
	void			   InitBBox(CPageSourceHeader* pPageSourceHeader, class CPrintSheet* pPrintSheet = NULL, class CPlate* pPlate = NULL);
	int				   GetNumPages();
	CString			   GetPreviewFileName(int nPageNumInSourceFile);
	BOOL			   HasLinks(void);
	class CPrintSheet* GetFirstPrintSheet();
	class CPrintSheet* GetNextPrintSheet(class CPrintSheet* pPrintSheet);
	BOOL			   DocumentExists(CPrintSheet* pPrintSheet = NULL, BOOL bLogMessage = FALSE);
	int				   GetSystemGeneratedMarkType();
	float			   GetBBox(int nSide, CPageSourceHeader* pObjectSourceHeader);
	CString			   GetSpotColor(int nIndex);
	BOOL			   IsMemberOfProductGroup(int nProductPartIndex);
	static BOOL		   IsPdfEngineReport(const CString& strInfoFileName);
	BOOL			   IsEqualPageSourceHeaders(CArray <CPageSourceHeader, CPageSourceHeader&>& rPageSourceHeaders);
};

template <> void AFXAPI SerializeElements <CPageSource> (CArchive& ar, CPageSource* pPageSource, INT_PTR nCount);
void AFXAPI DestructElements(CPageSource* pPageSource, INT_PTR nCount);



class CPageSourceList : public CList <CPageSource, CPageSource&>	// CList is derived from CObject
{																	// - so serialization will run
public:
    DECLARE_SERIAL( CPageSourceList )
	CPageSourceList();


//Attributes:

//Operations:
	short 			GetPageSourceIndex(CPageSource* pPageSource);
	short 			GetPageSourceIndex(CString strDocName);
	void  			ResetFontsCopyStatus();
	short 			FindObjectSourceIndex(CString strPath);
	short 			FindMarkSourceIndex(class CMark* pMark);
	short 			FindMarkSourceIndex(CPageSource* pMarkSource, int nMarkType);
	CPageSource*	FindByDocumentFullPath(const CString& strPath);
	CPageSource*	FindBySystemCreationInfo(const CString& strSystemCreationInfo);
	void  			RemoveUnusedSources();
	void  			InitializePageTemplateRefs(class CPageTemplateList* pPageTemplateList = NULL, CColorDefinitionTable* pColorDefinitionTable = NULL);
	void  			MergeElements(CPageSourceList& rPageSourceList, CPageTemplateList& rPageTemplateList);
	int	  			GetTotalNumPages();
	BOOL  			CheckAutoPlausibility(class CImpManDoc* pDoc);
	BOOL  			SourceDocumentExists(CString strDocName);
	void  			CheckPDFsModified();
	void  			CheckDocuments();
	void  			UpdateColorInfos();
	BOOL			IsSpotcolorDefined(const CString& strSpotColorName);
	COLORREF		GetAlternateCMYK(const CString& strSpotColorName);
	COLORREF		GetAlternateRGB(const CString& strSpotColorName);
	void			GetPDFInputInfo(CJobInfoExchange& rJobInfoExchange);
};
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////




//////////////////////////// Exposing Templates /////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Declaration of CPageTemplateList and CMarkTemplateList and all included subclasses
// Implementation in "ImpManDocExpTemplates.cpp"

class CPrintSheetLocation : public CObject
{
public:
    DECLARE_SERIAL( CPrintSheetLocation )
	CPrintSheetLocation();
	CPrintSheetLocation(const CPrintSheetLocation& rPrintSheetLocation); // copy constructor

//Attributes:
public:
	short m_nPrintSheetIndex;		// Description on what printsheet and where 
	BYTE  m_nLayoutSide;			// on that printsheet this page is located.           
	short m_nLayoutObjectIndex;

//Operations:
	const CPrintSheetLocation&	operator=(const CPrintSheetLocation& rPrintSheetLocation);
	void						Copy(const CPrintSheetLocation& rPrintSheetLocation);
	class CPrintSheet*			GetPrintSheet();
	class CLayoutSide*			GetLayoutSide();
	class CLayoutObject*		GetLayoutObject();
	class CFoldSheet*			GetFoldSheet();
	class CPlateList*			GetPlateList();
	class CExposure*			GetExposure(short nColorDefinitionIndex);
	void						AddExposure(short nColorDefinitionIndex);
	void						RemoveExposure(short nColorDefinitionIndex);
};
template <> void AFXAPI SerializeElements <CPrintSheetLocation> (CArchive& ar, CPrintSheetLocation* pPrintSheetLocation, INT_PTR nCount);


typedef
struct
{
	short	m_nPageSourceIndex;			// Index of associated PageSource (postscript filename) in PageSourceList
	short	m_nPageNumInSourceFile;		// Number of associated page in source file
	short	m_nColorDefinitionIndex;	// Index for ColorDefinitionTable where color is described (name, RGB value, ...)
}SEPARATIONINFO;						// if m_bMap2AllColors, this separation has to be assigned to all colorants (plates)
										// if m_bMap2SpotColors, this separation has to be assigned to all spotcolor plates

class CColorSeparations : public CArray <SEPARATIONINFO, SEPARATIONINFO>
{
public:
	inline BOOL operator==(const CColorSeparations& rColorSeparations)
	{
		if (GetSize() != rColorSeparations.GetSize())	return FALSE;
		for (int i = 0; i < GetSize(); i++)
		{
			if (GetAt(i).m_nPageSourceIndex		 != rColorSeparations[i].m_nPageSourceIndex)		return FALSE;
			if (GetAt(i).m_nPageNumInSourceFile  != rColorSeparations[i].m_nPageNumInSourceFile)	return FALSE;
			if (GetAt(i).m_nColorDefinitionIndex != rColorSeparations[i].m_nColorDefinitionIndex)	return FALSE;
		}
		return TRUE;
	}
	inline BOOL operator!=(const CColorSeparations& rColorSeparations) { return !(*this == rColorSeparations); };
};


class CSpotColorMappingTable : public CArray <int, int>
{
public:
	const CSpotColorMappingTable& operator= (const CSpotColorMappingTable& rSpotColorMappingTable)
	{
		RemoveAll();
		Append(rSpotColorMappingTable);
		return *this;
	}

	int	GetEntry(int nIndex)
	{
		if ( (nIndex >= 0) && (nIndex < GetSize()) )
			return GetAt(nIndex);
		else
			return -1;
	}

	void SetEntry(int nIndex, int nMapToSpotColorNum)
	{
		for (int i = 0; i <= nIndex; i++)
		{
			if (i >= GetSize())
				Add(0);
			if (i == nIndex)
				SetAt(i, nMapToSpotColorNum);
		}
	}

	void CleanUp()
	{
		for (int i = 0; i < GetSize(); i++)
		{
			if (GetAt(i) > 0)
				return;
		}
		RemoveAll();	// if no entry with colornum > 0, we don't need this list
	}
};


class CPageTemplate : public CObject
{
public:
    DECLARE_SERIAL( CPageTemplate )
	CPageTemplate();
	CPageTemplate(const CPageTemplate& rPageTemplate); // copy constructor

enum ObjectType {SinglePage, MultipageMember, ColorStrip, RegisterMark, CutMark, FoldMark, SectionMark, CustomMark};
enum Attribute  {ScaleOffsetModified = 1};
enum Shingling  {AddShinglingOffset = TRUE};

friend class CPageTemplateList;

//Attributes:	// we use short and BYTE types where possible, because a document could hold a lot of pages (maybe some thousands)
public:
	CString					m_strPageID;				// PageID is the real name of the page, i.e. "-1-", "VIII", "Blank", ...
	BOOL					m_bPageNumLocked;
	BYTE					m_nPageType;
	short					m_nMultipageGroupNum;
private:
	float					m_fContentOffsetX,			// access only via GetContentOffset(), because shingling offset has to be added if required
							m_fContentOffsetY;
public:
	float					m_fContentScaleX, 
							m_fContentScaleY;
	BOOL					m_bContentDuplicate,		// for pdf marks only
							m_bContentTrim;				// for pdf marks only
	float					m_fContentGridX,
							m_fContentGridY,
							m_fContentRotation,
							m_fShinglingOffsetX,
							m_fShinglingOffsetXFoot,
							m_fShinglingOffsetY;
	BOOL					m_bShinglingOffsetLocked;
	BYTE					m_nContentPivotX;
	BYTE					m_nContentPivotY;
	float					m_fLeftPageMargin,
							m_fRightPageMargin,
							m_fLowerPageMargin,
							m_fUpperPageMargin;
	BYTE					m_nAttribute;
	BOOL					m_bMap2AllColors;
	BOOL					m_bMap2SpotColors;
	CSpotColorMappingTable	m_spotColorMappingTable;
	BOOL					m_bLabelBackSide;
	BOOL					m_bAdjustContentToMask;
	BOOL					m_bFlag;
	CArray <SEPARATIONINFO, SEPARATIONINFO>	
							m_ColorSeparations;			// Represents the color separations this page is composed of	
	CArray <CPrintSheetLocation, CPrintSheetLocation&>	// Locations where page appears in printsheet list.
							m_PrintSheetLocations;		// A page appears only once in the bookblock, 
														// but can appear on multiple locations in the printsheet list.  
	int						m_nProductIndex;
	int						m_nProductPartIndex;

	CFoldSheet*				m_pFoldSheet;


//Operations:														
public:
	void				 		Serialize(CArchive& ar);
	const CPageTemplate& 		operator= (const CPageTemplate& rPageTemplate);
	BOOL				 		operator==(const CPageTemplate& rPageTemplate);
	BOOL				 		operator!=(const CPageTemplate& rPageTemplate);
	void				 		Copy(const CPageTemplate& rPageTemplate);
	class CLayoutObject* 		GetLayoutObject(int nIndex = 0);
	class CPrintSheet*	 		GetPrintSheet(int nIndex = 0);
	inline BOOL					IsPartOfBoundProduct(int nProductPartIndex, int nIndex);
	void				 		Create(CString& strID, BYTE nType, float fCenterOffsetX, float fCenterOffsetY, int nProductIndex, int nProductPartIndex);
	SEPARATIONINFO*		 		GetSeparationInfo		 (int nColorDefinitionIndex, BOOL bDoMapping = FALSE, CPrintSheet* pPrintSheet = NULL, int nSide = 4);	// 4 == BOTHSIDES
	SEPARATIONINFO*		 		GetSeparationInfo		 (CPageSource* pPageSource, int nPageNumInSourceFile);	
	BOOL				 		ContainsUnseparatedColor(int nColorDefinitionIndex, BOOL bDoMapping = FALSE, CPrintSheet* pPrintSheet = NULL, CLayoutObject* pObject = NULL, CMark* pMark = NULL, CString* pstrMappedColor = NULL, CColorDefinitionTable* pColorDefTable = NULL);
	CPageSource*		 		GetObjectSource		 (int nColorDefinitionIndex, BOOL bDoMapping = FALSE, CPrintSheet* pPrintSheet = NULL, int nSide = 4);
	CPageSourceHeader*	 		GetObjectSourceHeader	 (SEPARATIONINFO& rSepInfo, class CPageSourceList* pObjectSourceList = NULL, CColorDefinitionTable* pColorDefinitionTable = NULL, CImpManDoc* pDoc = NULL);
	CPageSourceHeader*	 		GetObjectSourceHeader	 (int nColorDefinitionIndex, BOOL bDoMapping = FALSE, CPrintSheet* pPrintSheet = NULL, int nSide = 4, CImpManDoc* pDoc = NULL);
	CPageSource*		 		GetVarnishInfo();
	BOOL				 		IsMemberOf(class CPageTemplateList* pTemplateList);
	CPageTemplate*		 		GetFirstMultipagePartner();
	int					 		GetMultipageSide();
	float				 		_GetContentOffsetX() { return m_fContentOffsetX; };
	float				 		_GetContentOffsetY() { return m_fContentOffsetY; };
	float				 		GetContentOffsetX(BOOL bAddShinglingOffset, CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet);
	float				 		GetContentOffsetY(BOOL bAddShinglingOffset, CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet);
	float				 		GetContentOffsetX(BOOL bAddShinglingOffset = FALSE, CPageSource* pObjectSource = NULL, float* pfObjectWidth  = NULL, BOOL bOutputJDF = FALSE);
	float				 		GetContentOffsetY(BOOL bAddShinglingOffset = FALSE, CPageSource* pObjectSource = NULL, float* pfObjectHeight = NULL, BOOL bOutputJDF = FALSE);
	inline void 		 		SetContentOffsetX(float fContentOffsetX) { m_fContentOffsetX = (fabs(fContentOffsetX) < 0.001f) ? 0.0f : fContentOffsetX; };
	inline void			 		SetContentOffsetY(float fContentOffsetY) { m_fContentOffsetY = (fabs(fContentOffsetY) < 0.001f) ? 0.0f : fContentOffsetY; };
	void				 		GetGridClipBox(float& fClipBoxLeft, float& fClipBoxBottom, float& fClipBoxRight, float& fClipBoxTop);
	BOOL				 		HasContentOffset() { return ( (m_fContentOffsetX != 0.0f) || (m_fContentOffsetY != 0.0f) ) ? TRUE : FALSE; };
	BOOL				 		IsScaled()			{ return ( (m_fContentScaleX  != 1.0f) || (m_fContentScaleY  != 1.0f) ) ? TRUE : FALSE; };
	BOOL				 		IsSystemCreated(CMark* pMark = NULL);
	int					 		GetMarkType();
	CString				 		GetMarkText();
	BOOL				 		SetMarkText(CString strOutputText);
	int					 		GetCurSpotColorNum(int nColorDefinitionIndex, const CString strColorName, CPageSourceHeader* pObjectSourceHeader);
	CString				 		GetNonePrintedColors(CPrintSheet* pPrintSheet, CLayoutObject* pObject);
	CString				 		GetMappedColorsComposite();
	BOOL				 		IsColorPrinted(int nColorDefinitionIndex, CPrintSheet* pPrintSheet, CLayoutObject* pObject, CString& strRemappedColor);
	BOOL				 		IsColorPrinted(int nColorDefinitionIndex, CPrintSheet* pPrintSheet, CMark*			pMark,	 CString& strRemappedColor, CColorDefinitionTable* pColorDefTable);
	BOOL				 		CheckMultipage(CPageSourceHeader* pPageSourceHeader);
	SEPARATIONINFO*		 		GetFirstSepInfo(						  INT_PTR nPageNumInSourceFile, INT_PTR* pSepIndex = NULL);
	SEPARATIONINFO*		 		GetNextSepInfo (SEPARATIONINFO* pSepInfo, INT_PTR nPageNumInSourceFile, INT_PTR* pSepIndex = NULL);
	void				 		InsertSepInfo  (SEPARATIONINFO* pSepInfo);
	void				 		RemoveSepInfo  (SEPARATIONINFO* pSepInfo, BOOL bKeepLast = FALSE);
	void				 		RemoveSpotSeps();
	BOOL				 		HasProcessColors(CMark* pMark = NULL, CColorDefinitionTable* pColorDefTable = NULL);
	BOOL				 		HasProcessColor(CMark* pMark = NULL, int nColorType = -1, CColorDefinitionTable* pColorDefTable = NULL);
	BOOL				 		HasSpotColors(CMark* pMark = NULL, CColorDefinitionTable* pColorDefTable = NULL);
	int					 		GetTotalNumColorants();
	BOOL						HasColorant(const CString& strColorName);
	CFoldSheet*					GetFoldSheet();
	class CBoundProduct&		GetBoundProduct();
	class CBoundProductPart&	GetProductPart();
	class CFlatProduct&			GetFlatProduct();
	CRect				 		Draw(CDC* pDC, int* nTop, int* nMinBottom, int nSide, int nIndex, long lTemplateListCenterX, BOOL bShowTrims = FALSE, BOOL bShowColors = FALSE, BOOL bShowPageNums = TRUE, BOOL bShowShingling = FALSE, 
						 		COLORREF crPage = RGB(255,255,255), CDisplayList* pDisplayList = NULL, BOOL bShowTitle = FALSE, BOOL bShowMasking = FALSE, 
								float fBleed = 0.0f, float fGluelineWidth = 0.0f, float fSpineArea = 0.0f, float fHeadTrim = 0.0f, float fFootTrim = 0.0f, float fSideTrim = 0.0f, BOOL bThumbnail = FALSE);
	void				 		DrawMeasuresBound(CDC* pDC, int nSide, CRect multipageRect, CRect overfoldRect, CRect spineRect, CRect trimRect, int nPageIndex);
	void				 		DrawMeasuresUnbound(CDC* pDC, CRect pageRect, CRect trimRect, int nPageIndex);
	static void			 		DrawSingleMeasure(CDC* pDC, CString string, CPoint hotSpot, int nDirection, int nTextPosition = -1, BOOL bBackground = FALSE);
	CRect				 		DrawColorants(CDC* pDC, CRect rcRect);
	void				 		DrawCropMarks(CDC* pDC, class CCropMark* pMark, CRect rcPage);
	void*				 		GetCropMarkTrimBox(CRect rcPage, CCropMark* pMark, LPARAM lParam1, LPARAM lParam2, long& lLeft, long& lBottom, long& lRight, long& lTop);
	void				 		DrawCropMarkPreview(CDC* pDC, long lTrimBoxLeft, long lTrimBoxBottom, long lTrimBoxRight, long lTrimBoxTop);
	BOOL				 		IsPage();
	BOOL				 		IsFlatProduct();
	BOOL						IsFlatSingleSidedBackSide();
	BOOL				 		FlatProductHasBackSide();
	BOOL				 		PageNumHasAlpha();
	int					 		GetPageNum();
	float				 		CalcShinglingScaleX();
	float				 		CalcShinglingScaleY();
	BOOL				 		ColorInUse(CImpManDoc* pDoc, short nColorDefinitionIndex, BOOL bSeparationOnly, BOOL bCompositeAlso);
	BOOL						IsFirstLastOrInnerPage();
	BOOL						IsFirstOrLastPage();
	BOOL						IsInnerPage();
	CRect						DrawSheetPreview(CDC* pDC, CRect rcFrame, CLayoutObject* pLayoutObject = NULL, CPrintSheet* pPrintSheet = NULL, BOOL bShowContent = FALSE);
};

template <> void AFXAPI SerializeElements <CPageTemplate> (CArchive& ar, CPageTemplate* pPageTemplate, INT_PTR nCount);	
void AFXAPI DestructElements(CPageTemplate* pPageTemplate, INT_PTR nCount);
	


/////////////////////////////////////////////////////////////////////////////
// CPageTemplateList itself

#define ALL_PARTS -INT_MAX

class CPageTemplatePosList : public CArray <POSITION, POSITION> 
{
public:
	CPageTemplatePosList();
	CPageTemplatePosList(CPageTemplatePosList& rPosList);

public:
	const CPageTemplatePosList& operator=(const CPageTemplatePosList& rPosList);
};

class CPageTemplateProductRefs : public CObject
{
public:
	CPageTemplateProductRefs() { m_bInvalid = TRUE; };

private:
	BOOL													m_bInvalid;
	CArray <CPageTemplatePosList, CPageTemplatePosList&>	m_products;
	CArray <CPageTemplatePosList, CPageTemplatePosList&>	m_productParts;

public:
	void Invalidate() { m_bInvalid = TRUE; };
	void Validate()	  { m_bInvalid = FALSE; };
	BOOL IsInvalid()  { return m_bInvalid; };
	void Initialize(CPageTemplateList* pPageTemplateList = NULL);
	POSITION FindIndex			 (int nIndex, int nProductPartIndex);
	POSITION FindIndexProduct	 (int nIndex, int nProductIndex);
	POSITION FindIndexProductPart(int nIndex, int nProductPartIndex);
};


class CPageTemplateList : public CList <CPageTemplate, CPageTemplate&>
{
public:
    DECLARE_SERIAL( CPageTemplateList )
	CPageTemplateList();

//Attributes:
public:
	CSize m_TemplateListSize;
	long  m_lTemplateListCenterX;

private:
	CPageTemplateProductRefs m_productRefs;


//Operations:
public:
	void		    InvalidateProductRefs() { m_productRefs.Invalidate(); };
	POSITION		FindIndex(INT_PTR nIndex, int nProductPartIndex = ALL_PARTS);
	POSITION		FindLabelPos(int nLabelIndex, int nSide = 1);
	CPageTemplate*	FindFlatProductTemplate(int nLabelIndex, int nSide = 1);	// 1 == FRONTSIDE
	POSITION		GetHeadPosition(int nProductPartIndex = ALL_PARTS);
	POSITION		GetTailPosition(int nProductPartIndex = ALL_PARTS);
	CPageTemplate&	GetHead(int nProductPartIndex = ALL_PARTS);
	CPageTemplate&	GetTail(int nProductPartIndex = ALL_PARTS);
	CPageTemplate&	GetNext(POSITION& rPosition, int nProductPartIndex);// = ALL_PARTS);
	CPageTemplate&	GetPrev(POSITION& rPosition, int nProductPartIndex);// = ALL_PARTS);
	short			GetIndex(CPageTemplate* pPageTemplate, int nProductPartIndex = ALL_PARTS);
	INT_PTR			GetCount(int nProductPartIndex = ALL_PARTS);
	POSITION		AddHead(CPageTemplate& rPageTemplate, int nProductPartIndex = ALL_PARTS);
	POSITION		AddTail(CPageTemplate& rPageTemplate, int nProductPartIndex = ALL_PARTS);
	POSITION		InsertBefore(POSITION position, CPageTemplate& rPageTemplate);
	POSITION		InsertAfter(POSITION position, CPageTemplate& rPageTemplate);
	void			RemoveTail(int nProductPartIndex = ALL_PARTS);

	void			FindPrintSheetLocations();
	void			RemoveUnusedTemplates();
	void			RemoveProductGroupTemplates(int nProductPartIndex);
	void			AdjustNumPages(int nPages, int nProductIndex, int nProductPartIndex);
	void			InsertProductPartPages(int nPages, int nProductIndex, int nProductPartIndex);
	void			SynchronizeToFlatProductList(class CFlatProductList& rFlatProductList, int nProductPartIndex);
	int				GetPageIndex(CPageTemplate* pPageTemplate);
	short			GetNextFreeGroupNum();
	void			ReorganizeColorInfos(BOOL bUndefinedOnly = FALSE);
	void			ReorganizePrintSheetPlates	   (BOOL bUseDefaults = FALSE);
	void			ReorganizePrintSheetPlatesMarks(BOOL bUseDefaults = FALSE);
	static BOOL		CheckForExposuresToAdd	 (CPrintSheet* pPrintSheet, CLayoutObject* pObject, CMark* pMark, CPageTemplate& rTemplate, int nLocIndex, BOOL bUseDefaults, CImpManDoc* pDoc, int nColorDefinitionIndexToCheck = -1);
	void			CheckForExposuresToRemove(CPageTemplate& rTemplate, int nLocIndex, CPrintSheet* pPrintSheet, int nSide);
	void			ReorganizeGluelineScaling();
	void			ShiftProductPartIndices(int nNewIndex, int nNumNewParts = 1);
	void			RemovePageSource(int nPageSourceIndex, BOOL bBreakLinksOnly, BOOL bResetOutputStamps = FALSE, BOOL bResetPageNums = TRUE);
	void			RemovePageSourcesAll();
	CPageTemplate*	FindTemplate(CString strID);
	POSITION		FindTemplatePos(CString strID, POSITION startPos = NULL, int nProductPartIndex = ALL_PARTS);
	BOOL			ColorInUse(CImpManDoc* pDoc, short nColorDefinitionIndex, BOOL bSeparationOnly = FALSE, BOOL bCompositeAlso = FALSE);
	void			DecrementColorDefIndices(short nColorDefinitionIndex);
	void			ResetFlags();
	void			CalcShinglingOffsets(float& fOffsetX, float& fOffsetXFoot, float& fOffsetY, int nProductPartIndex);
	float			CalcShinglingOffsetsX(int nProductPartIndex, BOOL bFoot);
	float			CalcShinglingOffsetsY(int nProductPartIndex);
	float			CalcShinglingOffsetsComingAndGoing(BOOL bFoot, int nProductPartIndex);
	int				CalcShinglingMaxLayerNum(int nProductPartIndex);
	void			UpdateMarkSourceLinks(short nOldMarkSourceIndex, short nNewMarkSourceIndex);
	void			MergeElements(CPageTemplateList& rMarkTemplateList, class CLayout& rLayout);
	void			ExtractMarkSources(CPageSourceList& rTargetList, CPageSourceList& rSourceList);
	BOOL			MarkSourceExists(short nMarkSourceIndex);
	BOOL			AutoAssignPageSource(CPageSource* pPageSource, int nProductPartIndex, int nLabelPageIndex = -1, BOOL bReorganize = TRUE);
	void			CheckPageSourceHeaders();
	int				GetNumUnassignedPages();
	int				GetNumUnimposedPages(int nProductPartIndex);
	int				GetNumUnplacedPages(int nProductPartIndex);
	void			CalcPagePreviewSize(int nPageIndex, int nProductPartIndex);
	void			CalcSinglePagePreviewSize(int nPageIndex, int nProductPartIndex);
	void			CalcLabelPreviewSize(int nLabelIndex, BOOL bShowTrims);
	CRect			Draw(CDC* pDC, CRect rcFrame, int nPageIndex = 0, int nProductPartIndex = ALL_PARTS, BOOL bShowTrims = FALSE, BOOL bShowColors = FALSE, BOOL bShowPageNums = TRUE, BOOL bShowShingling = TRUE, 
						 CDisplayList* pDisplayList = NULL, BOOL bShowTitle = FALSE, BOOL bShowMasking = FALSE, 
						 float fBleed = 0.0f, float fGluelineWidth = 0.0f, float fSpineArea = 0.0f, float fHeadTrim = 0.0f, float fFootTrim = 0.0f, float fSideTrim = 0.0f, BOOL bThumbnail = FALSE);
	CRect			DrawSinglePage(CDC* pDC, CRect rcFrame, int nPageIndex = 0, int nProductPartIndex = ALL_PARTS, BOOL bShowTrims = FALSE, BOOL bShowColors = FALSE, BOOL bShowPageNums = TRUE, BOOL bShowBitmap = TRUE, BOOL bThumbnail = FALSE);
	CRect			DrawPagesList(CDC* pDC, CRect rcFrame, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList = NULL);
	CRect			DrawPagesRow(CDC* pDC, CRect rcFrame, CPageTemplate& rTemplate, int nPageIndex, BOOL bShowProductPart, BOOL bShowPageFormat, BOOL bShowFoldSheet, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList = NULL);
	CRect			DrawFlatProduct(CDC* pDC, CRect rcFrame, int nLabelIndex = 0, BOOL bShowTrims = FALSE, BOOL bShowColors = FALSE, BOOL bShowPageNums = TRUE, BOOL bShowBitmap = TRUE, BOOL bThumbnail = FALSE);
	void			DoNumber(BOOL bRangeAll, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex);
	CString			GetPageRange(int nProductPartIndex);
	CString			GetPageRange(CFoldSheet* pFoldSheet, CLayout* pLayout = NULL, INT_PTR* pNumPages = NULL);
	CString			FindFreeMarkName(CString strNewMark);
	void			SortPages();
	int				GetLongestPageNumString(CDC* pDC);
	BOOL			IsUnpopulated();
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// ControlMarkTemplates

class CMarkTemplate : public CObject
{
public:
    DECLARE_SERIAL( CMarkTemplate )
	CMarkTemplate();
	CMarkTemplate(const CMarkTemplate& rMarkTemplate); // copy constructor

enum MarkType {ColorStrip, RegisterMark, CutMark, FoldMark, SectionMark, CustomMark};

//Attributes:		
public:
	CString		m_strMarkID;				// Real name of control mark
//	CString		m_strContentFile;				// path + name of the eps-file
	BYTE		m_nMarkType;
	float		m_fCenterOffsetX,
				m_fCenterOffsetY;
	float		m_fScaleX, m_fScaleY;
	CArray <SEPARATIONINFO, SEPARATIONINFO>	
				m_ColorSeparations;			// Represents the color separations this mark is composed of	
	CArray <CPrintSheetLocation, CPrintSheetLocation&>	
				m_PrintSheetLocations;		// Locations where mark appears in printsheet list

//Operations:
public:
	const CMarkTemplate& operator=(const CMarkTemplate& rMarkTemplate);
	void  Copy(const CMarkTemplate& rMarkTemplate);
	class CLayoutObject* GetLayoutObject(int nLayoutIndex, int nLayoutSide);  
	void  Create(CString& strID, unsigned char nMarkType, int nObjectIndex, CString& strContentFile);
};

template <> void AFXAPI SerializeElements <CMarkTemplate> (CArchive& ar, CMarkTemplate* pMarkTemplate, INT_PTR nCount);
void AFXAPI DestructElements(CMarkTemplate* pMarkTemplate, INT_PTR nCount);



/////////////////////////////////////////////////////////////////////////////
// CMarkTemplateList itself - will be subclassed in CLayoutSide

class CMarkTemplateList : public CList <CMarkTemplate, CMarkTemplate&>
{
public:
    DECLARE_SERIAL( CMarkTemplateList )
	CMarkTemplateList();


//Attributes:

//Operations:
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////




////////////////////////////// CBookblock ///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Declaration of CBookblock and all included subclasses
// Implementation in "ImpManDocCBookblock.cpp"

//
//class CPrintSheetRefList : public CArray <int, int> 
//{
//public:
//	CPrintSheetRefList();
//	CPrintSheetRefList(CPrintSheetRefList& rRefList);
//
//private:
//	BOOL m_bInvalid;
//
//public:
//	const CPrintSheetRefList& operator=(const CPrintSheetRefList& rRefList);
//	void  Invalidate() { m_bInvalid = TRUE; };
//	void  Validate()   { m_bInvalid = FALSE; };
//	BOOL  IsInvalid()  { return m_bInvalid; };
//};


class CFoldSheetLayer : public CObject
{
public:
    DECLARE_SERIAL( CFoldSheetLayer )
	CFoldSheetLayer();
	CFoldSheetLayer(const CFoldSheetLayer& rFoldSheetLayer); // copy constructor


//Attributes:
public:
	unsigned				m_nPagesX, m_nPagesY;
	CArray <short, short>	m_FrontPageArray;			// Page numbers
	CArray <short, short>	m_BackPageArray;			// Page numbers
	CArray <short, short>	m_HeadPositionArray;		// Head positions
	CArray <char,  char>	m_FrontSpineArray;			// Front page is left or right of spine
	CArray <char,  char>	m_BackSpineArray;			// Back page is left or right of spine
	BOOL					m_bPagesLocked;
	//CPrintSheetRefList		m_printSheetRefs;

//Operations:
public:
	const CFoldSheetLayer& operator=(const CFoldSheetLayer& rFoldSheetLayer);
	void				Copy(const CFoldSheetLayer& rFoldSheetLayer);
	CPrintSheet*		GetFirstPrintSheet(int nProductPartIndex);
	CPrintSheet*		GetNextPrintSheet(CPrintSheet* pPrintSheet, int nProductPartIndex);
	CString				GetWebName(class CFoldSheet* pFoldSheet);
	int					GetRealNumPages();
};

template <> void AFXAPI SerializeElements <CFoldSheetLayer> (CArchive& ar, CFoldSheetLayer* pFoldSheetLayer, INT_PTR nCount);
void AFXAPI DestructElements(CFoldSheetLayer* pFoldSheetLayer, INT_PTR nCount);


extern CString strDefLowRange, strDefHighRange;

class CFoldSheet : public CObject
{
public:
    DECLARE_SERIAL( CFoldSheet )
	CFoldSheet();
	CFoldSheet(const CFoldSheet& rFoldSheet);
	~CFoldSheet();


protected:
	CString	 m_strID;


//Attributes:
public:
	int		 m_nFoldSheetNumber;
	BOOL	 m_bFoldSheetNumLocked;
	CString  m_strNumerationStyle;
	int		 m_nNumerationOffset;
	CString	 m_strFoldSheetTypeName;
	unsigned m_nPagesLeft, m_nPagesRight; // no. Pages in left/right part of folded sheet
	unsigned m_nFoldSheetRefEdge;
	CString	 m_strNotes;
	float	 m_fShinglingValueX;
	float	 m_fShinglingValueXFoot;
	float	 m_fShinglingValueY;
	BOOL	 m_bShinglingValueLocked;	// not in use yet - reserved for future purpose (05/04/99)
	int		 m_nProductIndex;
	int		 m_nProductPartIndex;
	CList <CFoldSheetLayer, CFoldSheetLayer&> m_LayerList;
	CString  m_strPaperName;
	BOOL	 m_bOutputDisabled;
protected:
	int		 m_nQuantityPlanned;
	int		 m_nQuantityExtra;
	int		 m_nQuantityActual;


//Operations:
public:
	const							CFoldSheet& operator=(const CFoldSheet& rFoldSheet);
	void							SetID(const CString& strID) { m_strID = strID; };
	CString&						GetID() { return m_strID; };
	void							Copy(const CFoldSheet& rFoldSheet);
	void							Create(int countX = 1, int countY = 1, CString FoldSheetName = "", int nProductPartIndex = 0);
	BOOL							CreateFromISC(const CString& strFullPath, CString strName = _T(""), int nProductPartIndex = 0, BOOL bSearchSubFolders = FALSE);
	void							DeleteContents();
	BOOL							IsEmpty();
	int								GetIndex();
	void							GetPageSequence(CArray <short*, short*>& rPageSequence, int nPart);
	void							ReducePageSequence(CArray <short*, short*>& rPageSequence);
	static int						ComparePageSeqIndex(const void *p1, const void *p2);
	void							SetDefaultPagesLeftRight();		// set default values for m_nPagesLeft/m_nPagesRight
	CString							GetPageRange(int nSide = 4, CString& strLowRange = strDefLowRange, CString& strHighRange = strDefHighRange);	// 4 == BOTHSIDES
	int								GetMinPageNum();
	int								GetPageIndex(CPageTemplate* pPageTemplate);
	CString							GetNumber();
	int								GetNumberOnly() { return m_nFoldSheetNumber + m_nNumerationOffset; };
	void							DrawPageNumRange(CDC* pDC, int nLeft, int nTop, int nBottom, int nClipWidth, float fScreenPrinterRatioX = 1.0f);
	int								GetLayerIndex(CFoldSheetLayer* pLayer);
	CFoldSheetLayer*				GetLayer(int nIndex);
	CLayout*						GetLayout(int nLayerIndex);
	class CPrintGroup&				GetFirstPrintGroup();
	CPrintSheet*					GetPrintSheet(int nLayerIndex);
	CPrintSheet*					GetNextPrintSheet(CPrintSheet* pPrintSheet, int nLayerIndex);
	CSize							GetMatrix();
	int								GetNumPages();
	CSize							GetExtents(CPoint* pPtOrg, int nComponentRefIndex = -1, CPrintSheet* pPrintSheet = NULL, CLayout* pLayout = NULL);
	CRect							DrawSheetPreview(CDC* pDC, CRect rcFrame, int nLayerIndex, int nRotation = 0, int nTurned = 0, BOOL bShowContent = FALSE);
	CRect							DrawThumbnail(CDC* pDC, CRect rcFrame, BOOL bHighlight = TRUE, BOOL bDrawCentered = TRUE, int nLayerHighlightIndex = -1, int nHighlightSpineSide = -1);
	void							DrawThumbnailHeadPosition(CDC* pDC, CRect rect, int nHeadPosition, int nForm);
	void							DrawThumbnailRefEdge(CDC* pDC, int nForm, CRect sheetRect);
	CRect							DrawSection(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	CRect							DrawSummary(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet = NULL);
	CRect							DrawSize			  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0, CPrintSheet* pPrintSheet = NULL);
	CRect							DrawTargetQuantity	  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0);
	CRect							DrawActualQuantity	  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0);
	CRect							DrawDifferenceQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0);
	CRect							DrawCoverage		  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth = 0, CPrintSheet* pPrintSheet = NULL);
	int								GetNumSamePrintLayout();
	int								GetNUpPrintLayout(CPrintSheet* pPrintSheet, int nLayerIndex);
	int								GetNUpAll(int nLayerIndex);
	int								GetFoldSheetIndex();
	class CBoundProduct&			GetBoundProduct();
	class CBoundProductPart&		GetProductPart();
	BOOL							IsPartOfBoundProduct(int nProductPartIndex);
	void							GetCutSize(int nLayerIndex, float& fCutWidth, float& fCutHeight);
	float							GetCutMargin(int nLayerIndex, int nSide);
	void							GetPageCutBox(CFoldSheetLayer* pLayer, int nPageIndex, CBoundProductPart& rBoundProductPart, float& fCutBoxWidth, float& fCutBoxHeight, 
												  float& fPageLeft, float& fPageBottom, float& fPageRight, float& fPageTop, BOOL bRotate = FALSE);
	void							GetPageCutBox(CFoldSheetLayer* pLayer, int nPageIndex, int nPageOrientation, float fPageWidth, float fPageHeight, float fOverFold, int nOverFoldSide, float fHeadTrim, float fFootTrim, float fSideTrim, float fSpine,
												  float& fCutBoxWidth, float& fCutBoxHeight, float& fPageLeft, float& fPageBottom, float& fPageRight, float& fPageTop, BOOL bRotate = FALSE);
	int								GetQuantityPlanned() { return m_nQuantityPlanned; };
	int								GetQuantityExtra()	 { return m_nQuantityExtra;   };
	int								GetQuantityActual()	 { return m_nQuantityActual;  };
	void							SetQuantityPlanned(int nQuantity, BOOL bUpdatePrintSheets = TRUE);
	void							SetQuantityExtra  (int nQuantity, BOOL bUpdatePrintSheets = TRUE);
	void							SetQuantityActual (int nQuantity) { m_nQuantityActual = nQuantity; };
	void							CalcQuantityActual();
	float							CalcCoverage(CPrintSheet* pPrintSheet);
	CString							GetPaperName();
	BOOL							IsLeftFromSpine(int nPageIndex);
	BOOL							HasFoldSheetInside();
	int								GetBestRotation90();
	BOOL							IsFoldSchemeClosedOnHead();
	BOOL							IsCover();
	class CPrintComponentsGroup&	GetPrintComponentsGroup();
	int								GetSpineOrientation();
	int								GetPaperGrain();
	int								SpreadLayer(int nLayerIndex, int nOrientation, int nSpreadAxis, float& fFoldSheetLeftBottomPart, float& fFoldSheetRightTopPart);
	CString							GetOutFilename		 (int nSide, CFoldSheet* pFoldSheet = NULL, CPlate* pPlate = NULL);
	CString							GetOutFileFullPath	 (int nSide, CFoldSheet* pFoldSheet = NULL, CPlate* pPlate = NULL, BOOL bCreateMissingPath = FALSE);
	CString							GetOutFileTmpFullPath(int nSide, CFoldSheet* pFoldSheet = NULL, CPlate* pPlate = NULL);
};

template <> void AFXAPI SerializeElements <CFoldSheet> (CArchive& ar, CFoldSheet* pFoldSheet, INT_PTR nCount);
void AFXAPI DestructElements(CFoldSheet* pFoldSheet, INT_PTR nCount);



class COldDescriptionItem : public CObject
{
public:
    DECLARE_SERIAL( COldDescriptionItem )
	COldDescriptionItem();
	COldDescriptionItem(const COldDescriptionItem& rDescriptionItem); // copy constructor

enum FoldSheetPart {FoldSheetLeftPart, FoldSheetRightPart};

//Attributes:
public:
	int		 m_nFoldSheetIndex;
	unsigned m_nFoldSheetPart;	// left or right part of folded sheet

//Operations:
public:
	const		COldDescriptionItem& operator=(const COldDescriptionItem& rDescriptionItem);
	void		Copy(const COldDescriptionItem& rDescriptionItem);
	CFoldSheet& GetFoldSheet(class CBookblock* pBookblock);
	CFoldSheet* GetFoldSheetPointer(class CBookblock* pBookblock);
};


class CDescriptionItem : public CObject
{
public:
    DECLARE_SERIAL( CDescriptionItem )
	CDescriptionItem();
	CDescriptionItem(const CDescriptionItem& rDescriptionItem); // copy constructor

enum FoldSheetPart {FoldSheetLeftPart, FoldSheetRightPart};

//Attributes:
public:
	int		 m_nFoldSheetIndex;
	unsigned m_nFoldSheetPart;	// left or right part of folded sheet
	int		 m_nPart;
	int		 m_nSection;

//Operations:
public:
	const		CDescriptionItem& operator=(const CDescriptionItem& rDescriptionItem);
	void		Copy(const CDescriptionItem& rDescriptionItem);
	CFoldSheet& GetFoldSheet(class CBookblock* pBookblock);
	CFoldSheet* GetFoldSheetPointer(class CBookblock* pBookblock);
};
template <> void AFXAPI SerializeElements <CDescriptionItem> (CArchive& ar, CDescriptionItem* pDescriptionItem, INT_PTR nCount);


#include "PDFTargetList.h"

class CFoldSheetList : public CList <CFoldSheet, CFoldSheet&>
{
public:
	CFoldSheetList();

public:
	enum OutfileRelation{FoldSheetPerFile, JobPerFile};

public:
	CPDFTargetProperties m_outputTargetProps;

public:
	void		LoadFromDatabase();
	CFoldSheet* CGGetSimilarFoldSheet(CFoldSheet& rFoldSheet, int nLimitIndex);
	BOOL	    CGFoldSheetsSimilar(CFoldSheet& rFoldSheetRef, CFoldSheet& rFoldSheet);
	void		UpdatePaperNames();
	void		TakeOverProductPartQuantityPlanned(int nProductPartIndex, int nProductPartAmount);
	void		TakeOverProductPartQuantityExtra  (int nProductPartIndex, int nProductPartExtra);
	void		TakeOverProductPartPaperName	  (int nProductPartIndex, CString strPaperName);
	int			GetOutfileRelation(const CString& strOutputFilename);
	BOOL		OutputSheetsAuto();
};


/////////////////////////////////////////////////////////////////////////////
// CBookblock itself

class CBookblock : public CObject
{
public:
    DECLARE_SERIAL( CBookblock )
	CBookblock();

enum {PaperThickness = 8, FoldSheetGap = 8};	// assume paper is 0.2 mm thick
												// assume gap between foldsheets in bookblock is 0.2 mm

//enum ShinglingMethod {ShinglingMethodPaper, ShinglingMethodSheet, ShinglingMethodBottling};	// old work style
enum ShinglingMethod{ShinglingMethodMove = 0, ShinglingMethodScale = 1, ShinglingMethodBottling = 2, ShinglingMethodBottlingScale = 3}; 
enum ShinglingDirection{ShinglingDirectionSpine = 0, ShinglingDirectionSpineHead = 1, ShinglingDirectionSpineFoot = 2};

enum OutfileRelation{FoldSheetPerFile, JobPerFile};


//Attributes:
public:
	CList <CDescriptionItem, CDescriptionItem&> m_DescriptionList;
	CFoldSheetList							    m_FoldSheetList;
	BOOL										m_bShinglingActive;
	int											m_nShinglingMethod;
	CPDFTargetProperties						m_outputTargetProps;
	COleDateTime								m_OutputDate;		

//Operations:
	void					Copy(const CBookblock& rBookblock);
    void					Serialize( CArchive& archive );
	POSITION				GetHeadPosition_DescriptionList(int nBoundProductIndex);
	POSITION				GetTailPosition_DescriptionList(int nBoundProductIndex);
	CDescriptionItem&		GetPrev_DescriptionList(int nBoundProductIndex, POSITION& pos);
	CDescriptionItem&		GetNext_DescriptionList(int nBoundProductIndex, POSITION& pos);
	void					AddHead_DescriptionList(int nBoundProductIndex, CDescriptionItem& rItem);
	void					AddTail_DescriptionList(int nBoundProductIndex, CDescriptionItem& rItem);
	void					CheckFoldSheetQuantities();
	BOOL					IsEmpty() {return m_FoldSheetList.IsEmpty();};
	int						ImposePageSection(CBoundProductPart* pProductPart, CFoldSheet newFoldSheet, int nNumPages, int nNumSheets, CArray <int, int>* pNewFoldSheetIndices = NULL);
	int						AddFoldSheets(CFoldSheet* pFoldSheet, int nCount, int nProductPartIndex = ALL_PARTS, CArray <int, int>* pNewFoldSheets = NULL);
	int						AddFoldSheetsSimple(CFoldSheet* pFoldSheet, int nCount, CArray <int, int>* pNewFoldSheets = NULL, int nPart = -1, int nSection = -1, CFoldSheet* pFoldSheetInsertAfter = NULL);
	int						AddFoldSheetsSimpleCG(CFoldSheet* pFoldSheet, int nCount, CArray <int, int>* pNewFoldSheets = NULL, int nPart = -1, int nSection = -1);
	void					InsertFoldSheetAfter(CFoldSheet* pInsertAfterFoldSheet, CFoldSheet* pNewFoldSheet);
	void					ChangeFoldSheet(int nFoldSheetIndex, CFoldSheet* pNewFoldSheet);
	void					ChangeFoldSheets(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, int nProductPartIndex = ALL_PARTS, CLayout* pLayout = NULL, CArray <int, int>* pNewFoldSheets = NULL);
	void					RemoveUnplacedFoldSheets(int nProductPartIndex);
	void					RemoveFoldSheet(int nIndex);
	void					RemoveFoldSheets(CFoldSheet* pFoldSheet, int nCount, int nProductPartIndex = ALL_PARTS);
	void					RemoveFoldSheets(int nProductPartIndex);
	void					RemoveSelectedFoldSheets(CDisplayList& rDisplayList);
	void					ReNumberFoldSheets(int nProductPartIndex);
	void					RemoveUnusedFoldSheets(BOOL bDontRemoveCGGoing = FALSE, BOOL bNeverRemovePrintSheet = FALSE);
	int						GetNextUnusedFoldSheet(int nIndex);
	void					ReIndexFoldSheets(int nIndex);
	void					SelectFoldSheets(CString& strName, CDisplayList& rDisplayList);
	void					SelectFoldSheet(int nIndex, CDisplayList& rDisplayList);
	void					DeselectFoldSheet(int nIndex, CDisplayList& rDisplayList);
	void					ModifyBindingMethod(int nBindingMethod, CDisplayList& rDisplayList);
	void					ModifyBindingMethod(int nBindingMethod, int nProductPartIndex);
	void					BindingMethodReorganize(int nBindingMethod, int nInsertIndex, CList <CFoldSheet, CFoldSheet&>& tempFList, int nProductPartIndex, CArray <int, int>* pFoldSheetIndices = NULL);
	void					CutAndPaste(int nIndexDI, BOOL bInsertAfter, CDisplayList& rDisplayList);
	void					CopyAndPaste(int nIndexDI, BOOL bInsertAfter, CDisplayList& rDisplayList, int nProductIndex);
	void					Reorganize(CPrintSheet* pPrintSheet = NULL, CLayout* pLayout = NULL, int nProductPartIndex = ALL_PARTS, BOOL bDontRemoveCGGoing = FALSE);
	void					ShiftProductPartIndices(int nNewIndex, int nNumNewParts = 1);
	void					ArrangeCover();
	CFoldSheet* 			GetFoldSheetByNum(int nFoldSheetNumber, int nProductPartIndex);
	void					SetShinglingValuesX(float fValue, float fValueFoot, int nProductPartIndex);
	void					SetShinglingValuesY(float fValue, int nProductPartIndex);
	CSize					GetMaxFoldSheetNumberExtent(CDC* pDC);
	CSize					GetMaxFoldSheetNameExtent(CDC* pDC);
	CSize					GetMaxFoldSheetPageRangeExtent(CDC* pDC, int nProductPartIndex);
	int						GetFoldSheetIndex(CFoldSheet* pFoldSheet);
	void					InitSortedFoldSheets(CArray <CFoldSheet*, CFoldSheet*>&	sortedFoldSheets, int nProductPartIndex = ALL_PARTS, CFoldSheet* pFoldSheet = NULL, CLayout* pLayout = NULL);
	void					GetFoldSheetTypes(CArray <CFoldSheet*, CFoldSheet*>& foldSheetTypes, CArray <int, int>& foldSheetTypeNums, int nProductPartIndex);
	static int				CompareFoldSheetNum(const void *p1, const void *p2);
	CRect					DrawTitle(CDC* pDC, CRect rcFrame, BOOL bShowIcon = TRUE);
	CRect					Draw(CDC* pDC, CRect rcFrame, int nProductPartIndex, CString strSelectedFoldSheet, COLORREF crBkColor = 0);
	void					DrawFoldSheet(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex);
	int						GetFoldSheetPart(POSITION& pos);
	static int				TransformFontSize(CDC* pDC, float fWorld, int nScreenMin);
	CPoint					DrawFoldSheetLeftPart(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex);
	CPoint					DrawFoldSheetRightPart(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex);
	void					DrawConnectionLeftRight(CDC* pDC, CDescriptionItem& Item, CPoint endPointLeft, CPoint endPointRight, int nProductPartIndex);
	void					RenderFoldSheet(CDC* pDC, CDescriptionItem& Item, int nSheetThickness, CPoint& cp, int nRadius, int nProductPartIndex);
	int						GetFoldSheetRadiusLeftPart(POSITION pos, int nProductPartIndex);
	int						GetFoldSheetRadiusRightPart(POSITION pos, int nProductPartIndex);
	CRect					DrawFoldSheetList(CDC* pDC, CRect rcFrame, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList = NULL);
	CRect					DrawFoldSheetRow(CDC* pDC, CRect rcFrame, CFoldSheet& rFoldSheet, int nFoldSheetIndex, BOOL bShowProductGroup, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList = NULL);
	CSize					GetExtend(int nProductPartIndex);
	CFoldSheet* 			FindFoldSheet(int nProductPartIndex, CString strName);
	int						GetNumFoldSheets(int nProductPartIndex, CString strName = _T(""));
	void					DoNumber(BOOL bRangeAll, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex);
	int						GetDescriptionItemIndex(CFoldSheet* pFoldSheet, int nFoldSheetPart);
	CFoldSheet* 			GetFoldSheetByID(CString& strID);
	CFoldSheet* 			GetFoldSheetByIndex(int nFoldSheetIndex);
	static void				GetFoldSheetGroups(CArray<CFoldSheet*, CFoldSheet*>& foldSheetGroups, int nProductPartIndex);
	CString					GetFoldSheetRange(CFoldSheet* pFoldSheet, CLayout* pLayout = NULL, int* pNumFoldSheets = NULL);
	BOOL					FoldSheetExist(CFoldSheet* pFoldSheet);
	CFoldSheet* 			GetFirstFoldSheet(int nProductPartIndex);
	CFoldSheet* 			GetNextFoldSheet	 (CFoldSheet* pFoldSheet, int nProductPartIndex, CString strFoldScheme = _T(""));
	CFoldSheet* 			GetNextFoldSheetLayer(CFoldSheet* pFoldSheet, int& nLayerIndex, int nProductPartIndex, CString strFoldScheme = _T(""));
	CFoldSheet* 			GetLastFoldSheet(int nProductPartIndex);
	CFoldSheet* 			GetFirstUnplacedFoldSheet	  (CFoldSheet* pFoldSheet, int nStartAt = -1, BOOL bAddSchemeIfRequired = FALSE);
	CFoldSheet* 			GetFirstUnplacedFoldSheetLayer(CFoldSheet* pFoldSheet, int& nLayerIndex, BOOL bAddSchemeIfRequired = FALSE);
	CFoldSheet* 			GetFirstUnassignedFoldSheet();
	CFoldSheet* 			GetNextUnassignedFoldSheet(CFoldSheet* pFoldSheet);
	int						GetNumUnplacedFoldSheets(CFoldSheet* pStartFoldSheet, int nProductPartIndex, CString strSchemeName = CString(_T("")), BOOL bIncludeUnfoldedPages = FALSE);
	class CBindingProcess&	GetBindingProcess(int nProductPartIndex);
	void					ArrangeAssemblySections() ;
	CDescriptionItem*		FindDescriptionItem(int nFoldSheetIndex, int nFoldSheetPart);
	void					CheckFoldSheetsPlausibility();
	int						GetNumPages(int nProductPartIndex, BOOL bEnabledOnly = FALSE);
	BOOL					OutputProof(const CString strDocTitle);
	BOOL					OutputProofAuto(const CString strDocTitle);
	static BOOL				OutputProofProgessCallback(int nTotalPages, int nCurrentPage, const CString& strFoldSheetNumber, LPVOID lpData);
	void					EnableProofOutput();
	int						GetOutfileRelation	 (const CString& strOutputFilename);
	CString					GetOutFilename		 (int nSide, CFoldSheet* pFoldSheet);
	CString					GetOutFileFullPath	 (int nSide, CFoldSheet* pFoldSheet, BOOL bJDFMarksOutput = FALSE, BOOL bCreateMissingPath = FALSE);
	CString					GetOutFileTmpFullPath(int nSide, CFoldSheet* pFoldSheet);
	BOOL					OutFileExists		 (int nSide, CFoldSheet* pFoldSheet);
	BOOL					NotYetOutputted();
	BOOL					ReadyForOutput();
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////






//////////////////////////////// CLayoutList ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// declaration of CLayoutList and all included subclasses


/////////////////////////////////////////////////////////////////////////////
// CExposure

class CExposure : public CObject
{
public:
    DECLARE_SERIAL( CExposure )
	CExposure();
	CExposure(const CExposure& rExposure); // copy constructor

//Attributes:
private:
	float m_fLeftBleed, m_fRightBleed, m_fUpperBleed, m_fLowerBleed;
	BYTE  m_nLockedBleeds;
	BOOL  m_bEnabled;
public:
	BYTE  m_nAction;				// Stepper only
	short m_nDiffusionPercentage;	// Stepper only
	short m_PreVacuumTime;			// Stepper only
	short m_ExposingClocks;			// Stepper only
	short m_nLayoutObjectIndex;		// Access to corresponding layout object 

private:
	class CExposureSequence* m_pSequence;	// Pointer to sequence which contains this exposure - will not be serialized.
friend class CExposureSequence;				// Will be initialized when a exposure is inserted into the plates exposure sequence.
											// (so define as friend to give access)

//Operations:
public:
	class CPlate*		 GetPlate();
	void				 Create(CPlate* pDefaultPlate, short nLayoutObjectIndex);
	const CExposure&	 operator=(const CExposure& rExposure);
	void				 Copy(const CExposure& rExposure);
	class CLayoutObject* GetLayoutObject(int nLayoutIndex, int nLayoutSide);
	class CLayoutObject* GetLayoutObject(CLayout* pLayout, int nLayoutSide);
	class CLayoutObject* GetLayoutObject();
	float				 GetBleedValue    (int nSide);
	float				 GetRealBleedValue(int nSide);
	void				 CalcAutoBleed    (int nSide, float fValue);
	void				 SetBleedValue    (int nSide, float fValue, BOOL bLock = FALSE, class CPrintSheetView* pView = NULL);
	void				 LockBleed		  (int nSide);
	void				 UnlockBleed	  (int nSide, class CPrintSheetView* pView = NULL);
	BOOL				 IsBleedLocked	  (int nSide);
	float				 GetContentTrimBox(int nSide, CLayoutObject* pObject, CPageTemplate* pTemplate);
	float				 GetContentTrimBoxCenterX	 (CLayoutObject* pObject, CPageTemplate* pTemplate);
	float				 GetContentTrimBoxCenterY	 (CLayoutObject* pObject, CPageTemplate* pTemplate);
	float				 GetSheetTrimBox  (int nSide, CLayoutObject* pObject, CPageTemplate* pTemplate);
	float				 GetSheetBleedBox (int nSide, CLayoutObject* pObject, CPageTemplate* pTemplate);
};



class CExposureSequence : public CList <CExposure, CExposure&>	// CList is derived from CObject
{																// - so serialization will run
public:
    DECLARE_SERIAL( CExposureSequence )
	CExposureSequence();


//Attributes:
private:
	class CPlate* m_pPlate;	// Pointer to plate which contains this list - will not be serialized.
	BOOL  m_bInvalid;
friend class CPlate;		// Will be initialized in constructor of CPlate (so define as friend to give access)


//Operations:
public:
	inline class  CPlate* GetPlate() { return m_pPlate; };
	POSITION	  AddTail(CExposure& rExposure);
	void		  RemoveAll();
	void		  RemoveAt(POSITION pos);			
	BOOL		  HasNoPages();
	CExposure*	  GetExposure(short nLayoutObjectIndex);
	BOOL		  IsInvalid()  { return m_bInvalid; };
	inline void	  Invalidate() { m_bInvalid = TRUE; };
	inline void	  Validate()   { m_bInvalid = FALSE; };

protected:											// Inserting objects is only possible via AddTail()		
	POSITION	  AddHead(CExposure& rExposure);	// because we have to initialize the plate pointer of the exposure to be inserted
	void		  SetAt(POSITION pos, CExposure& rExposure);
	POSITION	  InsertBefore(POSITION pos, CExposure& rExposure);
	POSITION	  InsertAfter(POSITION pos, CExposure& rExposure);
	CLayoutObject RemoveHead();
	CLayoutObject RemoveTail();
};



/////////////////////////////////////////////////////////////////////////////
// CPlate

class CPlate : public CObject
{
public:
    DECLARE_SERIAL( CPlate )
	CPlate();
	CPlate(const CPlate& rPlate); // copy constructor

enum AutoBleedMethod {AutoBleedPositiv, AutoBleedNegativ};

//Attributes:
public:
	CString				m_strPlateName;	
	short				m_nColorDefinitionIndex;	// Index for ColorDefinitionTable where color is described
	BYTE				m_nAutoBleedMethod;			// (name, RGB value, ...) - corresponds to template separation
	float				m_fOverbleedOutside;
	float				m_fOverbleedInside;
	float				m_fOverbleedOverlap;
	float				m_fOverbleedControlMarks;
	short				m_nExposingDeviceIndex;		
	CExposureSequence	m_ExposureSequence;
	CStringArray		m_processColors;
	CStringArray		m_spotColors;

	COleDateTime		m_OutputDate;		
	BOOL				m_bOutputDisabled;

private:
	BOOL				m_bInvalid;
	class CPlateList*	m_pPlateList;	// Pointer to platelist which contains this plate - will not be serialized      
friend class CPlateList;				// Will be initialized in AddTail() of CPlateList (so define as friend to give access)

//Operations:
public:
	class CPrintSheet*	GetPrintSheet();
	int					GetSheetSide();
	CLayoutSide*		GetLayoutSide();
	class CPlateList*	GetPlateList() { return m_pPlateList; };
	void				Create(CPlate* pDefaultPlate, CExposure& rExposure, short nColorDefinitionIndex, CPrintSheet* pPrintSheet);
	const CPlate&		operator=(const CPlate& rPlate);
	void				Copy(const CPlate& rPlate);
	BOOL				IsInvalid()  { return m_bInvalid; };
	inline void			Invalidate() { m_bInvalid = TRUE; };
	inline void			Validate()	 { m_bInvalid = FALSE; };
	void				UpdateExposures();
	CExposure*			GetDefaultExposure(int nLayoutObjectIndex);
	void				CalcAutoBleeds();
	void				SetBleedValue(int nSide, float fValue, CLayoutObject* pLayoutObject, BOOL bPanoramaPageClicked, class CPrintSheetView* pView = NULL);
	CString				GetGlobalMaskValueString();
	void				UnlockLocalMask(CLayoutObject* pLayoutObject, BOOL bPanoramaPageClicked, class CPrintSheetView* pView = NULL);
	void				AdjustContent2LocalMask(CLayoutObject* pLayoutObject, BOOL bPanoramaPageClicked, class CPrintSheetView* pView = NULL);
	void				SetTopmostPlate();
	void				RemoveExposure(int nLayoutObjectIndex);
	void				RotateLocalMaskAllExposures(int nComponentRefIndex = -1, BOOL bClockwise = TRUE);
	void				TurnLocalMaskAllExposures(int nComponentRefIndex = -1);
	void				TumbleLocalMaskAllExposures(int nComponentRefIndex = -1);
	float				GetObjectTrimBox(int nSide,	CLayoutObject& rLayoutObject);
	int					GetNumOutputObjects(int nObjectType);
	void				InsertProcessColors(int nColor1 = -1, int nColor2 = -1, int nColor3 = -1, int nColor4 = -1);
	void				InsertSpotColors(CStringArray& rSpotColors);
	void				CheckSourceLinks(CArray <CString, CString>& missingLinks, BOOL bJDFMarksOutput = FALSE);
	void				CheckOverlappingExposures(CArray <CString, CString&>& overlappingExposures);
	static void			Draw(CDC* pDC, class CPressDevice* pPressDevice, BOOL bShowMeasures = FALSE);
	static void			DrawMeasuring(CDC* pDC, class CPressDevice* pPressDevice);
	static void			DrawHMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1 = CPoint(INT_MAX, INT_MAX), CPoint ptRef2 = CPoint(INT_MAX, INT_MAX), BOOL bChain = FALSE);
	static void			DrawVMeasuring(CDC* pDC, CPoint pt[2], const CString& strMeasure, CPoint ptRef1 = CPoint(INT_MAX, INT_MAX), CPoint ptRef2 = CPoint(INT_MAX, INT_MAX), BOOL bChain = FALSE);
	void				AddSeparation(CString strSep);
	BOOL				SeparationExists(CString strSep);
	void				ReorganizeColorInfos();
	int					GetNumColors(BOOL bSpotsOnly);
};

template <> void AFXAPI SerializeElements <CPlate> (CArchive& ar, CPlate* pPlate, INT_PTR nCount);
void AFXAPI DestructElements(CPlate* pPlate, INT_PTR nCount);


/////////////////////////////////////////////////////////////////////////////
// CPlateList

class CPlateList : public CList <CPlate, CPlate&>	// CList is derived from CObject
{													// - so serialization will run
public:
    DECLARE_SERIAL( CPlateList )
	CPlateList();

//Attributes:
public:
	int	   m_nTopmostPlate;	// Overbleeds of these plate are displayed and can be edited	
	CPlate m_defaultPlate;

	BOOL m_bDisplayOpen;	// Not to be serialized

private:
	int				   m_nSheetSide;	// Side of sheet this list belongs to (FRONTSIDE/BACKSIDE)
	class CPrintSheet* m_pPrintSheet;	// Pointer to printsheet which contains this list - will not be serialized.
friend class CPrintSheet;				// Will be initialized in constructor of CPrintSheet (so define as friend to give access)


//Operations:
public:
	void		  Copy(const CPlateList& rPlateList);
	inline class  CPrintSheet* GetPrintSheet() { return m_pPrintSheet; };
	inline int				   GetSheetSide()  { return m_nSheetSide; };
	void		  Serialize(CArchive& ar);
	POSITION	  AddTail(CPlate& rPlate);
	void		  RemoveAt(POSITION pos);
	CPlate*		  GetTopmostPlate();
	void		  InvalidateAllPlates();
	void		  SetGlobalMaskAllPlates(float fValue);
	CString		  GetGlobalMaskValueString();
	CPlate*		  GetPlateByColor(short nColorDefinitionIndex);
	CPlate*		  GetPlateByColorName(const CString strColorName);
	CPlate*		  GetPlateByIndex(int nIndex);
	int			  GetPlateIndex(CPlate* pPlate);
	COleDateTime  GetLastOutputTime();
	BOOL		  PlateExists(const CString& strPlateName);

protected:									// Inserting objects is only possible via AddTail()		
	POSITION	  AddHead(CPlate& rPlate);	// because we have to initialize the printsheet pointer of the plate to be inserted
	void		  SetAt(POSITION pos, CPlate& rPlate);
	POSITION	  InsertBefore(POSITION pos, CPlate& rPlate);
	POSITION	  InsertAfter(POSITION pos, CPlate& rPlate);
	CPlate		  RemoveHead();
	CPlate		  RemoveTail();
};



class CPressDevice : public CObject
{
public:
    DECLARE_SERIAL( CPressDevice )
	CPressDevice();
	CPressDevice(CPressDevice& rPressDevice);
	~CPressDevice();

enum PressType {SheetPress, WebPress};

//Attributes:
public:
	CString	 m_strName;
	CString	 m_strDeviceID;
	unsigned m_nPressType;
	float	 m_fPlateWidth, m_fPlateHeight; 
	float	 m_fPlateEdgeToPrintEdge;
	float	 m_fGripper;
	float	 m_fXRefLine;
	float	 m_fYRefLine1;
	float    m_fYRefLine2;
	CString  m_strNotes;


//Operations:
public:
	const CPressDevice& operator= (const CPressDevice& rPressDevice);
	BOOL			    operator==(const CPressDevice& rPressDevice);
	BOOL			    operator!=(const CPressDevice& rPressDevice);
	void				Create() {	m_nPressType = SheetPress; 
									m_fPlateWidth = m_fPlateHeight = 0.0f;
									m_fPlateEdgeToPrintEdge = 0.0f;
									m_fGripper = 0.0f;
									m_fXRefLine = 0.0f;
									m_fYRefLine1 = 0.0f;
									m_fYRefLine2 = 0.0f; }
	void				DeleteContents() {};
	float				GetPlateRefline	(int nReferenceLine);
	void				DrawPlate(CDC* pDC, CRect rcFrame, float fPaperWidth = 0.0f, float fPaperHeight = 0.0f);
	BOOL				FindLargestPaper(CSheet& rLargestSheet);
	static BOOL			FindPaper(CSheet& rSheet, CString strPaperName);
	BOOL				SheetFitsToPlate(CSheet& rSheet);
	BOOL				Has2YRefLines() { return (fabs(m_fYRefLine2) >= 0.01f) ? TRUE : FALSE; };
	float				GetYCenterOfSection() { return (Has2YRefLines()) ? (m_fPlateHeight / 2.0f) : m_fYRefLine1; };
};

template <> void AFXAPI SerializeElements <CPressDevice> (CArchive& ar, CPressDevice* pPressDevice, INT_PTR nCount);



/////////////////////////////////////////////////////////////////////////////
// PressDeviceList will be subclassed in CLayoutList
class CPressDeviceList : public CList <CPressDevice, CPressDevice&>	// CList is derived from CObject
{																	// - so serialization will run
public:
    DECLARE_SERIAL( CPressDeviceList )
	CPressDeviceList();


//Attributes:

//Operations:
	int				AddDevice(CPressDevice& rPressDevice, BOOL& bRenamed);
	BOOL			DeviceNameExist(const CString& strName);
	int				GetDeviceIndex(const CString& strName);
	int				GetDeviceIndexByID(const CString& strID);
	POSITION		GetDevicePos(const CString& strName);
	CPressDevice*	GetDevice(const CString& strName);
};
/////////////////////////////////////////////////////////////////////////////




//////// CKIMRules //////////////////////////////////////////////////////////

class CKimRules : public CObject
{
public:
	DECLARE_SERIAL( CKimRules )
	CKimRules();
	~CKimRules();


enum DataType{TypeNone = 0, TypeString, TypeInteger, TypeFloat};
enum Relation{RelationNone = 0, RelationAnd, RelationOr};


public:
	CArray <CString, CString&> m_ruleExpressions;


public:
	const CKimRules& operator= (const CKimRules& rKimRules);
	virtual BOOL	 operator==(const CKimRules& rKimRules);
	virtual BOOL	 operator!=(const CKimRules& rKimRules);
	void			 Copy(const CKimRules& rKimRules);
	void			 AddRuleExpression(CString strExpression);
	void			 SetRuleExpression(int nIndex, CString strExpression);
	CString			 GetRuleExpression(int nIndex);
	void Serialize(CArchive& ar);
	void RemoveAll();
};


/////////////////////////////// CMarkList /////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Declaration of CMarkList and all included subclasses
// Implementation in "ImpManDocCMark.cpp"

/////////////////////////////////////////////////////////////////////////////
//  CMark
//


class CMark : public CObject
{
public:
	DECLARE_SERIAL( CMark )
	CMark();
	CMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, int nZOrderIndex, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
		  CPageSource& rMarkSource);

	enum MarkType {CustomMark, TextMark, SectionMark, CropMark, CutMark, FoldMark, RegisterMark, ColorStrip, BarcodeMark, DrawingMark};
	enum Status {Inactive = 0, Active = 1};


//Attributes:
protected:
	BOOL					m_bIsAutoMark;

public:
	BYTE					m_nType;
	int						m_nStatus;
	CString					m_strMarkName;
	float					m_fMarkWidth, 
							m_fMarkHeight;
	BYTE					m_nHeadPosition;				// LEFT | RIGHT | TOP | BOTTOM
	BOOL					m_bAllColors;
	int						m_nZOrderIndex;
	CColorSeparations		m_ColorSeparations;				// Represents the color separations this mark is composed of	
	CPageSource				m_MarkSource;
	CPageTemplate			m_markTemplate;
	CKimRules				m_rules;


//Operations:
public:
	const CMark&			operator= (const CMark& rMark);
	virtual BOOL			operator==(const CMark& rMark);
	virtual BOOL			operator!=(const CMark& rMark);
	void					Copy(const CMark& rMark);
	void					Serialize(CArchive& ar);
	void					SetAutoMark(BOOL bEnable) { m_bIsAutoMark = bEnable; };
	BOOL					IsAutoMark() { return m_bIsAutoMark; };
	BOOL					GlobalCheckRules(CLayout* pLayout, int nMarkListIndex);
	BOOL					InstanceCheckRules(CPrintSheet* pPrintSheet, int nSide, CLayoutObject* pLayoutObject, int nComponentRefIndex);
	BOOL					IsEnabled(CPrintSheet* pPrintSheet = NULL, int nSide = 4, CLayoutObject* pLayoutObject = NULL, int nComponentRefIndex = -1);	// 4 = BOTHSIDES
	BOOL					RuleExpressionIsTrue(CString strVariable, CString strOperator, CString strValue, CPrintSheet* pPrintSheet, int nSide, CLayoutObject* pLayoutObject = NULL, int nComponentRefIndex = -1);
	void					InitializeTemplate(CColorDefinitionTable& rColorDefTable);
	CPageTemplate			TransformColorDefs(CColorDefinitionTable& rColorDefTable, short nMarkSourceIndex);
	SEPARATIONINFO			GetSeparationInfo(CPlate* pPlate, CColorDefinitionTable& rColorDefTable, BOOL bDoMapping = FALSE);
	virtual void*			GetSheetTrimBox(void* pParam1, void* pParam2, LPARAM lParam1, LPARAM lParam2, float& fLeft, float& fBottom, float& fRight, float& fTop);
	virtual void			GetBBoxAll(CPrintSheet*, int, float&, float&, float&, float&);
	virtual int				AddToSheet(CPrintSheet* pPrintSheet, int nSide, CPageTemplate* pMarkTemplate, class CMarkList* pMarkList = NULL);
	virtual void			DrawSheetPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, class CMarkList* pMarkList, BOOL bShowContent, int nEdge = -1, BOOL bShowHighlighted = FALSE);
	static CString			SplitNewMarkName(CString strNewMark);
	void					HighlightMark(CDC* pDC, CRect markRect, CPrintSheet* pPrintSheet);
	CColorDefinitionTable&	GetActColorDefTable(CMarkList* pMarkList);
};



class CReflineMark : public CMark
{
public:
	DECLARE_SERIAL( CReflineMark )
	CReflineMark();
	CReflineMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
		  CPageSource& rMarkSource);

//Attributes:
public:
	CReflinePositionParams m_reflinePosParamsAnchor;
	CReflinePositionParams m_reflinePosParamsLL;
	CReflinePositionParams m_reflinePosParamsUR;
	BOOL				   m_bVariableWidth, m_bVariableHeight;


//Operations:
public:
	const CReflineMark&  	operator=(const  CReflineMark& rReflineMark);
	BOOL					operator==(const CReflineMark& rReflineMark);
	BOOL					operator!=(const CReflineMark& rReflineMark);
	void					Copy(const CReflineMark& rReflineMark);
	void					Serialize(CArchive& ar);
	CReflinePositionParams* GetReflinePosParams(int nID);
	void					GetBBoxAll(CPrintSheet*, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop);
	void*					GetSheetTrimBox(void* pParam1, void* /*pParam2*/, LPARAM nSide, LPARAM, float& fLeft, float& fBottom, float& fRight, float& fTop);
	int						AddToSheet(CPrintSheet* pPrintSheet, int nSide, CPageTemplate* pMarkTemplate, class CMarkList* pMarkList = NULL);
	void					DrawSheetPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, class CMarkList* pMarkList, BOOL bShowContent, int nEdge = -1, BOOL bShowHighlighted = FALSE);
	virtual void			DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, class CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted);
};



class CCustomMark : public CReflineMark
{
public:
	DECLARE_SERIAL( CCustomMark )
	CCustomMark();
	CCustomMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
				CPageSource& rMarkSource);

//Attributes:
public:


//Operations:
public:
	const CCustomMark&  operator=(const CCustomMark& rCustomMark);
	BOOL				operator==(const CCustomMark& rCustomMark);
	BOOL				operator!=(const CCustomMark& rCustomMark);
	void				Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList = NULL);
	void				Copy(const CCustomMark& rCustomMark);
	void				Serialize(CArchive& ar);
	void				DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, class CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted);
	void				DrawPreview(CDC* pDC, CRect outBBox, CRect clipRect, const CString& strPreviewFile);
	void				DrawContentGrid(CDC* pDC, CRect markRect);
};


class CTextMark : public CReflineMark
{
public:
	DECLARE_SERIAL( CTextMark )
	CTextMark();
	CTextMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
			  CPageSource& rMarkSource, const CString& strText);

//Attributes:
public:
	CString	m_strOutputText;


//Operations:
public:
	const CTextMark& operator= (const CTextMark& rTextMark);
	BOOL			 operator==(const CTextMark& rTextMark);
	BOOL			 operator!=(const CTextMark& rTextMark);
	void			 Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList = NULL);
	void			 Copy  (const CTextMark& rTextMark);
	void			 Serialize(CArchive& ar);
	void			 DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, class CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted);
	static void		 DrawTextfieldPreview(CDC* pDC, const CString& strText, const CRect& rOutBBox, int nHeadPosition, CPrintSheet* pPrintSheet, 
										  CPlate* pPlate, float fC, float fM, float fY, float fK, CStringArray& spotColorNames, CArray<COLORREF, COLORREF>& spotColorRGBs, CReflinePositionParams* pReflinePosParams = NULL);
	static CSize	 DrawTextfieldPreview(CDC* pDC, const CString& strText, const CRect& rOutBBox, int nHeadPosition, CPrintSheet* pPrintSheet, BOOL bScaleToFit, COLORREF crTextColor);
};


class CSectionMark : public CMark
{
public:
	DECLARE_SERIAL( CSectionMark )
	CSectionMark();
	CSectionMark(CString strName, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps, CPageSource& rMarkSource);

	enum Position {Spine = 0, Head_Foot = 1};

//Attributes:
public:
	float m_fFootMargin;
	float m_fHeadMargin;
	float m_fSafetyDistance;
	float m_fBarWidth;
	float m_fBarHeight;
	int	  m_nPosition;


//Operations:
public:
	const CSectionMark& operator= (const CSectionMark& rSectionMark);
	BOOL				operator==(const CSectionMark& rSectionMark);
	BOOL				operator!=(const CSectionMark& rSectionMark);
	void				Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList = NULL);
	void				Copy(const CSectionMark& rSectionMark);
	void				Serialize(CArchive& ar);
	void				GetBBoxAll(CPrintSheet*, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop);
	int					AddToSheet(CPrintSheet* pPrintSheet, int nSide, CPageTemplate* pMarkTemplate, class CMarkList* pMarkList = NULL);
	void*				GetSheetTrimBox(void* pParam1, void* pParam2, LPARAM nComponentRefIndex, LPARAM lParam2, float& fLeft, float& fBottom, float& fRight, float& fTop);
	void				DrawSheetPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, class CMarkList* pMarkList, BOOL bShowContent, int nEdge = -1, BOOL bShowHighlighted = FALSE);
	static CRect		GetSectionBarRect(long nBarWidth, long nBarHeight, const CRect& rOutBBox, short nComponentRefIndex, int nHeadPosition, CPrintSheet* pPrintSheet);
	static void			DrawSectionBarPreview(CDC* pDC, long nBarWidth, long nBarHeight, BOOL bNumRotated180, BOOL bHollow, const CRect& rOutBBox, short nComponentRefIndex, int nHeadPosition, 
											  CPrintSheet* pPrintSheet, BOOL bComingAndGoing = FALSE, CString strMarkName = _T(""), int nSide = 1);
	static CRect		FindCoveringMarks(CPrintSheet* pPrintSheet, CRect outBBox, CString strMarkName, int nSide);
	static void			CreateSectionBarFont(CDC* pDC, const CString& strNum, long nBarWidth, CFont* pFont, int nTextHead);
};


class CCropMark : public CMark
{
public:
	DECLARE_SERIAL( CCropMark )
	CCropMark();
	CCropMark(CString strName, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps, CPageSource& rMarkSource);

//Attributes:
public:
	float m_fPageDistance;
	float m_fLineWidth;
	float m_fLength;


//Operations:
public:
	const CCropMark&	operator= (const CCropMark& rCropMark);
	BOOL				operator==(const CCropMark& rCropMark);
	BOOL				operator!=(const CCropMark& rCropMark);
	void				Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList = NULL);
	void				Copy(const CCropMark& rCropMark);
	void				Serialize(CArchive& ar);
	void				GetBBoxAll(CPrintSheet*, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop);
	int					AddToSheet(CPrintSheet* pPrintSheet, int nSide, CPageTemplate* pMarkTemplate, class CMarkList* pMarkList = NULL);
	void*				GetSheetTrimBox(void* pParam1, void* pParam2, LPARAM nPosition, LPARAM lParam2, float& fLeft, float& fBottom, float& fRight, float& fTop);
	void				DrawSheetPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, class CMarkList* pMarkList, BOOL bShowContent, int nEdge = -1, BOOL bShowHighlighted = FALSE);
	void				DrawCropMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, CColorDefinitionTable& rColorDefTable, 
											float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop,
											BOOL bShowContent, BOOL bShowHighlighted);
	static void			DrawCropLinePreview(CDC* pDC, CRect outBBox);
};


class CFoldMark : public CMark
{
public:
	DECLARE_SERIAL( CFoldMark )
	CFoldMark();
	CFoldMark(CString strName, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps, CPageSource& rMarkSource);

enum ZOrder	 {Foreground = 1, Background = 2};
enum Linetype{Solid = 1, Dashed = 2};
enum BkMode	 {Transparent = 1, Opaque = 2}; 

//Attributes:
public:
	float m_fLineDistance;
	float m_fLineWidth;
	float m_fLineLength;
	float m_fCrossWidth;
	float m_fCrossHeight;
	float m_fCrosslineWidth;
	float m_fMinGap;
	BOOL  m_bSetMarksBetween;
	int	  m_nZOrder;
	int	  m_nLinetype;
	int	  m_nBkMode;


//Operations:
public:
	const CFoldMark&	operator= (const CFoldMark& rFoldMark);
	BOOL				operator==(const CFoldMark& rFoldMark);
	BOOL				operator!=(const CFoldMark& rFoldMark);
	void				Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList = NULL);
	void				Copy(const CFoldMark& rFoldMark);
	void				Serialize(CArchive& ar);
	void				GetBBoxAll(CPrintSheet*, int nSide, float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop);
	int					AddToSheet(CPrintSheet* pPrintSheet, int nSide, CPageTemplate* pMarkTemplate, class CMarkList* pMarkList = NULL);
	void*				GetSheetTrimBox(void* pParam1, void* pParam2, LPARAM nPosition, LPARAM lParam2, float& fLeft, float& fBottom, float& fRight, float& fTop);
	void				DrawSheetPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, class CMarkList* pMarkList, BOOL bShowContent, int nEdge = -1, BOOL bShowHighlighted = FALSE);
	void				DrawFoldMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, CColorDefinitionTable& rColorDefTable, 
											float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop,
											BOOL bShowContent, BOOL bShowHighlighted);
	static void			DrawFoldLinePreview(CDC* pDC, BOOL bSolid, BOOL bTransparent, CRect outBBox);
};






typedef struct
{
	CString strPDFBuffer;
	CPoint	ptTargetCoords;
}BC_CUSTOMDRAWDATA;



class CBarcodeMark : public CReflineMark
{
public:
	DECLARE_SERIAL( CBarcodeMark )
	CBarcodeMark();
	CBarcodeMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
				 CPageSource& rMarkSource, const CString& strText);

//Attributes:
public:
	CString	m_strOutputText;
	int		m_nBarcodeType;


//Operations:
public:
	const CBarcodeMark& operator= (const CBarcodeMark& rBarcodeMark);
	BOOL			 		operator==(const CBarcodeMark& rBarcodeMark);
	BOOL			 		operator!=(const CBarcodeMark& rBarcodeMark);
	void			 		Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList = NULL);
	void			 		Copy  (const CBarcodeMark& rBarcodeMark);
	void			 		Serialize(CArchive& ar);
	void					DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, class CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted);
	static void		 		DrawBarcodePreview(CDC* pDC, const CString& strOutputText, CString strBarcodeType, CString strTextOption, float fTextSize, const CRect& rOutBBox, int nHeadPosition, CPrintSheet* pPrintSheet, CPlate* pPlate, 
							float fC, float fM, float fY, float fK, CStringArray& spotColorNames, CArray<COLORREF, COLORREF>& spotColorRGBs, CReflinePositionParams* pReflinePosParams = NULL);
	static CSize	 		DrawBarcodePreview(CDC* pDC, const CString& strOutputText, CString strBarcodeType, CString strTextOption, float fTextSize, const CRect& rOutBBox, int nHeadPosition, CPrintSheet* pPrintSheet, CPlate* pPlate, CString strColorName, CReflinePositionParams* pReflinePosParams = NULL, COLORREF crBarcodeColor = RGB(0,0,0));
	static int				GetBarcodeTypeFromString(CString strBarcodeType);
	static int				MapTextOption(const CString& strTextOption, int nHeadPosition = 8);		// 8 = TOP
	static ERRCODE	 		GenerateBarcode(t_BarCode* pBarCode, const CString strOutputText, int nBarcodeType, int nHeadPosition);
	static ERRCODE CALLBACK CustomDrawBarcode(VOID* customData, HDC drawDC, DOUBLE x, DOUBLE y, DOUBLE width, DOUBLE height);
};



class CDrawingMark : public CReflineMark
{
public:
	DECLARE_SERIAL( CDrawingMark )
	CDrawingMark();
	CDrawingMark(CString strName, float fWidth, float fHeight, BYTE nHeadPosition, BOOL bAllColors, CArray <SEPARATIONINFO, SEPARATIONINFO>& rColorSeps,
				 CPageSource& rMarkSource, const CString& strText);

//Attributes:
public:



//Operations:
public:
	const CDrawingMark& operator= (const CDrawingMark& rDrawingMark);
	BOOL			 		operator==(const CDrawingMark& rDrawingMark);
	BOOL			 		operator!=(const CDrawingMark& rDrawingMark);
	void			 		Create(CColorDefinitionTable& rColorDefTable, CMarkList* pMarkList = NULL);
	void			 		Copy  (const CDrawingMark& rDrawingMark);
	void			 		Serialize(CArchive& ar);
	void					DrawMarkPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nAnchorReference, class CMarkList* pMarkList, int nEdge, float fTrimBoxLeft, float fTrimBoxBottom, float fTrimBoxRight, float fTrimBoxTop, BOOL bShowContent, BOOL bShowHighlighted);
	static void				DrawRectanglePreview(CDC* pDC, CRect rOutBBox, COLORREF crColor);
};



/////////////////////////////////////////////////////////////////////////////
//  CMarkListOld
//
class CMarkOld : public CObject
{
public:
	DECLARE_SERIAL( CMarkOld )
	CMarkOld();
	CMarkOld(const CMarkOld& rMarkOld); // copy constructor

enum MarkType {ColorStrip, RegisterMark, CutMark, FoldMark, SectionMark, CustomMark};

//Attributes:
public:
	BYTE				m_nType;			
	CString				m_strFormatName;
	BOOL				m_bAllColors;
	CString				m_strReserved;			
	CPageSource			m_MarkSource;
	CArray <SEPARATIONINFO, SEPARATIONINFO>	
						m_ColorSeparations;			// Represents the color separations this mark is composed of	
	float				m_fMarkWidth, 
						m_fMarkHeight;
	BYTE				m_nReferenceItemInX,		// Position rel. to plate or sheet SHEET|PLATE
						m_nReferenceItemInY,	
						m_nReferenceLineInX,		// LEFT|RIGHT|MID 
						m_nReferenceLineInY;		// BOTTOM|TOP|MID
	float				m_fDistanceX,				// Distance from Ref.-lines 
						m_fDistanceY;
	int					m_iRepetitionInX;
	int					m_iRepetitionInY;

	float				m_fLeftBorder, 
						m_fRightBorder,
						m_fLowerBorder,
						m_fUpperBorder;
	BYTE				m_nHeadPosition;		// LEFT | RIGHT | TOP | BOTTOM
	CString				m_strTextmark;
	short				m_nDisplayColorIndex;

public:
	float				m_fObjectCenterX,
						m_fObjectCenterY;

//Operations:
public:
	const CMarkOld& operator=(const CMarkOld& rMarkOld);
	void		 Copy(const CMarkOld& rMarkOld);
};
template <> void AFXAPI SerializeElements <CMarkOld> (CArchive& ar, CMarkOld* pMarkOld, INT_PTR nCount);
void AFXAPI DestructElements(CMarkOld* pMarkOld, INT_PTR nCount);


class CMarkListOld : public CList <CMarkOld, CMarkOld&>
{
public:
    DECLARE_SERIAL( CMarkListOld )
	CMarkListOld();

	CString	m_strNotes;
//Attributes:
private:

//Operations:
public:
	void Serialize(CArchive& ar);
};




class CMarkList : public CArray <CMark*, CMark*>
{
public:
    DECLARE_SERIAL( CMarkList )
	CMarkList();

public:
	CString				  m_strNotes;
	CColorDefinitionTable m_colorDefTable;
	CString				  m_strAnalyzeReport;

//Attributes:
private:

//Operations:	
public:
	BOOL operator==(const CMarkList& rMarkList);
	void		Copy(const CMarkList& rMarkList);
	void		Serialize(CArchive& ar);
	void		RemoveAt(int nIndex, BOOL bDelete = TRUE);
	void		RemoveAll(BOOL bFreeMemory = TRUE);
	int			GetIndex(CMark* pMark);
	int			FindMarkIndex(const CString& strName);
	CMark*		FindMark(const CString& strName);
	void		DrawSheetPreviews(CDC* pDC, CPrintSheet* pPrintSheet, int nSide);
	void		GetBBoxAll(CPrintSheet* pPrintSheet, int nSide, float& fMarksBBoxLeft, float& fMarksBBoxBottom, float& fMarksBBoxRight, float& fMarksBBoxTop);
	void		Sort();
	static int  CompareMarks(const void *p1, const void *p2);
	CMark*		GetFirstMark(int nMarkType);
	CString		FindFreeMarkName(CString strNewMark);
	BOOL		Load(const CString& strFullPath);
	BOOL		Save(const CString& strFullPath);
	BOOL		HasMarks(int nMarkType);
	void		AddMark(CMark* pMark);
};



class CMarkInstanceDefs : public CObject
{
public:
	DECLARE_SERIAL( CMarkInstanceDefs )
	CMarkInstanceDefs();
	CMarkInstanceDefs(const CMarkInstanceDefs& rMarkInstanceDefs); // copy constructor

	enum action{Disable = 1, UseSpecialDefs = 2};

	int		m_nType;		 // foldmark, cropmark or sectionmark - other marks are single / has no instances
	CString	m_strParentMark; // name of parent mark object
	int		m_nPosition;	 // LEFT_TOP, etc.
	int		m_nOrientation;	 // HORIZONTAL, VERTICAL
	int		m_nAction;

	const CMarkInstanceDefs& operator= (const CMarkInstanceDefs& rMarkInstanceDefs);
	void  Copy(const CMarkInstanceDefs& rMarkInstanceDefs);
};

template <> void AFXAPI SerializeElements <CMarkInstanceDefs> (CArchive& ar, CMarkInstanceDefs* pMarkInstanceDefs, INT_PTR nCount);	// Give SerializeElements() access to 


class CMarkBuddyLink : public CObject
{
public:
	DECLARE_SERIAL( CMarkBuddyLink )
	CMarkBuddyLink();
	CMarkBuddyLink(int nIndex, int nPosition, int nOrientation);

public:
	int	m_nLinkedObjectIndex;
	int	m_nLinkedObjectPosition;		// LEFT_TOP, etc.
	int	m_nLinkedObjectOrientation;		// HORIZONTAL, VERTICAL

public:
	const CMarkBuddyLink& operator= (const CMarkBuddyLink& rMarkBuddyLink);
	void  Copy(const CMarkBuddyLink& rMarkBuddyLink);
	void  Serialize(CArchive& ar);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////

class CObjectNeighbours : protected CArray <CLayoutObject*, CLayoutObject*>	
{
	CObjectNeighbours();

//Attributes:
private:
	class CLayoutObject* m_pLayoutObject;
friend class CLayoutObject;
friend class CLayoutObjectList;

//Operations:
public:
	int					   GetSize(BOOL bDontAnalyze = FALSE) const;
	inline CLayoutObject*  operator[](int nIndex) const { return GetAt(nIndex); };
	inline CLayoutObject*& operator[](int nIndex)		{ return ElementAt(nIndex); };
	CLayoutObject*		   GetAt(int nIndex) const;	
	CLayoutObject*&		   ElementAt(int nIndex);
};



class CLayoutObject : public CObject
{
public:
	DECLARE_SERIAL( CLayoutObject )
	CLayoutObject();
	CLayoutObject(const CLayoutObject& rLayoutObject); // copy constructor

enum ObjectType {Page, ControlMark, CutBlock};
enum Attributes {GroupLowerLeft = 1, FoldSheetCGMember = 2};	// FoldSheetCGMember = object belongs to 'coming part' of foldsheet (ComingAndGoing only)
enum Status		{Normal = 0, Disabled = 2, Highlighted = 4};


//Attributes:
public:
	BYTE					m_nType;			
private:					
	float					m_fObjectLeft,				// These members can only be modified by member functions SetLeft(), SetRight(), ...
							m_fObjectBottom, 			// because we want to have control if anything changes. If so, we invalidate 
							m_fObjectRight,				// the layout side to enable the neighbours itself to start a new analysis,
							m_fObjectTop;				// when they are accessed.
	float					m_fObjectWidth, 
							m_fObjectHeight;
public:						
	short					m_nIndex;					// Temporary - will not be serialized
	CString					m_strObjectName;
	CString					m_strFormatName;
	BYTE					m_nObjectOrientation;		// Portrait/Landscape
	float					m_fLeftBorder,				// Trim-Borders and so on 
							m_fRightBorder,
							m_fLowerBorder,
							m_fUpperBorder;
	BYTE					m_nLockBorder;				// LEFT | RIGHT | TOP | BOTTOM border(s) locked in case of asymmetric trim(s)
	BYTE					m_nIDPosition;				// LEFT | RIGHT
	BYTE					m_nHeadPosition;			// LEFT | RIGHT | TOP | BOTTOM
	short					m_nBlockNumber;
	BYTE					m_nAttribute;				// GroupLowerLeft / FoldSheetCGMember
	CString					m_strNotes;
	short					m_nDisplayColorIndex;
	short					m_nComponentRefIndex;			// Reference to foldsheet or to flat product  (printsheet component)
	short					m_nLayerPageIndex;			// Index of corresponding page in foldsheet layer page array
	short					m_nMarkTemplateIndex;		// Used only if object type is ControlMark
	BOOL					m_bObjectTurned;			// layout object is turned (front/back flipped) - used only for unbound objects
	float					m_fDefaultContentOffsetX;	// Content offset defined in page template is saved as default when storing as standard layout
	float					m_fDefaultContentOffsetY;
	CReflinePositionParams	m_reflinePosParams;

	short					m_nPublicFlag;				// Temporary flag used for miscellaneous purposes - not serialized
	CObjectNeighbours		m_LeftNeighbours, 
							m_RightNeighbours, 
							m_LowerNeighbours, 
							m_UpperNeighbours;
	BOOL					m_bHidden;
	int						m_nStatus;
	int						m_nGroupIndex;
	CArray <CMarkInstanceDefs, CMarkInstanceDefs&>	
							m_markInstanceDefs;			// layout object is page or label; special defs for instances of foldmarks, cropmarks or sectionmarks, belonging to this object 
	CMarkBuddyLink			m_markBuddyLink;			// used by layout objects which are marks; link defines reference to buddy page/label

private:					
	short					m_nAnalyseFlag;				// These members are needed temporary when analysing neighbourhoods
	float					m_fObjectCenterX,			// They will not be serialized
							m_fObjectCenterY;
friend class CLayoutSide;							// CLayoutSide needs access to previous private members
friend class CLayoutObjectMatrix;	// CLayoutObjectMatrix needs access to previous private members
friend class CStepAndRepeatMatrix;	// CStepAndRepeatMatrix needs access to previous private members

class CLayoutObjectList* m_pObjectList;			// Pointer to layout object list which contains this object - will not be serialized.
friend class CLayoutObjectList;					// Will be initialized when a layout object is inserted into the layout object list.
												// (so define as friend to give access)



//Operations:
public:
	void				 Serialize(CArchive& ar);
	int					 GetIndex();
	class CLayout*		 GetLayout();
	class CLayoutSide*   GetLayoutSide();
	BYTE				 GetLayoutSideIndex();
	void				 SetLeft  (float fLeft,   BOOL bInvalidate = TRUE);
	void				 SetRight (float fRight,  BOOL bInvalidate = TRUE);
	void				 SetBottom(float fBottom, BOOL bInvalidate = TRUE);
	void				 SetTop   (float fTop,    BOOL bInvalidate = TRUE);
	void				 SetWidth (float fWidth,  BOOL bInvalidate = TRUE);
	void				 SetHeight(float fHeight, BOOL bInvalidate = TRUE);
	inline float		 GetLeft()		{ return m_fObjectLeft;   };
	inline float		 GetRight()		{ return m_fObjectRight;  };
	inline float		 GetBottom ()	{ return m_fObjectBottom; };
	inline float		 GetTop()		{ return m_fObjectTop;	  };
	float				 GetLeftWithBorder();
	float				 GetRightWithBorder();
	float				 GetBottomWithBorder();
	float				 GetTopWithBorder();
	inline float		 GetWidth()	 { return m_fObjectWidth; };
	inline float		 GetHeight() { return m_fObjectHeight; };
	float				 GetRealWidth();		// GetReal...() functions returns the page's real width, height and borders 
	float				 GetRealHeight();		// as if the page has normal orientation (head position on top).
	float				 GetRealBorder(BYTE nBorder);	
	float				 GetObjectCenterX() { return m_fObjectCenterX; };
	float				 GetObjectCenterY() { return m_fObjectCenterY; };
	void				 Offset(float fOffsetX, float fOffsetY);
	void				 ModifySize(float fWidth, float fHeight, CPrintSheet* pPrintSheet, BOOL bRotate = FALSE);
	void				 RecalcObject(CPrintSheet* pPrintSheet, int nDirection);
	void				 CalcObjectPos(CLayoutObject *pObject, CPrintSheet* pPrintSheet, int nDirection);
	const CLayoutObject& operator=(const CLayoutObject& rLayoutObject);
	BOOL				 IsSimilar(const CLayoutObject& rLayoutObject);
	BOOL				 IsIdentical(CLayoutObject& rLayoutObject);
	void				 Copy(const CLayoutObject& rLayoutObject);
	void				 CreatePage(float fLeft, float fBottom, float fWidth, float fHeight, CString strObjectName, CString& strFormatName, BYTE nOrientation, 
						 		    CFoldSheetLayer& rLayer, BYTE nPageIndex, BYTE nComponentRefIndex, BYTE nLayerSide);
	void				 CreateFlatProduct(float fLeft, float fBottom, float fWidth, float fHeight, int nHeadPosition, int nFlatProductRefIndex);
	void				 CreateMark(float fLeft, float fBottom, float fWidth, float fHeight, const CString& strName, BYTE nHeadPosition, short nCountMarkTemplate);
	void				 CreateMark(float fLeft, float fBottom, float fWidth, float fHeight, const CString& strName, BYTE nHeadPosition, short nCountMarkTemplate, CReflinePositionParams* pReflinePosParams);
	void				 CreateCutBlockFoldSheet(float fLeft, float fBottom, float fWidth, float fHeight, int nComponentRefIndex, BYTE nHeadPosition);
	void				 CreateCutBlockLabel(float fLeft, float fBottom, float fWidth, float fHeight, int nComponentRefIndex, BYTE nHeadPosition, int nBlockNumber);
	CPageTemplate*		 GetCurrentObjectTemplate(CPrintSheet* pPrintSheet);
	CPageTemplate*		 GetCurrentPageTemplate(class CPrintSheet* pPrintSheet);
	CPageTemplate*		 GetCurrentPageTemplate(int nPrintSheetIndex);
	CPageTemplate*		 GetCurrentFlatProductTemplate(CPrintSheet* pPrintSheet);
	CPageTemplate*		 GetCurrentMarkTemplate();
	BOOL				 IsFlatProduct() { return ( (m_nType == Page) && (m_nLayerPageIndex < 0) ) ? TRUE : FALSE; };	// flat product is of type page
	void				 FindLeftNeighbours ();
	void				 FindRightNeighbours();
	void				 FindLowerNeighbours();
	void				 FindUpperNeighbours();
	BOOL				 HasLabelNeighbour();
	BOOL				 IsPotentialLeftNeighbour (CLayoutObject* pObject);
	BOOL				 IsPotentialRightNeighbour(CLayoutObject* pObject);
	BOOL				 IsPotentialLowerNeighbour(CLayoutObject* pObject);
	BOOL				 IsPotentialUpperNeighbour(CLayoutObject* pObject);
	BOOL				 OverlappingInX(CLayoutObject* pObject);
	BOOL				 OverlappingInY(CLayoutObject* pObject);
	static int			 CompareLeft  (const void *p1, const void *p2);
	static int			 CompareRight (const void *p1, const void *p2);
	static int			 CompareBottom(const void *p1, const void *p2);
	static int			 CompareTop   (const void *p1, const void *p2);
	void				 CalculateBorder(int nDirection);
	CLayoutObject*		 FindCounterpartObject();
	CLayoutObject*		 FindCounterpartControlMark();
	CLayoutObject*		 FindCounterPart(int nSide, float fCounterpartLeft, float fCounterpartBottom, float fCounterpartRight, float fCounterpartTop, CString strFormatName = "");
	int					 FindCounterpartClickSide(int n_Side);
	void				 AlignToCounterPart(CLayoutObject* pObject);
	void				 RemoveCorrespondingExposures(int nLayoutObjectIndex);
	void				 SetLocalMaskAllExposures(int nSide, float fValue, BOOL bPanoramaPageClicked);
	void				 UnlockLocalMaskAllExposures(BOOL bPanoramaPageClicked);
	void				 AdjustContent2LocalMaskAllExposures(BOOL bPanoramaPageClicked);
	BOOL				 IsPlausible();
	CExposure*			 GetExposure(class CPlate* pPlate, CLayout* pLayout = NULL, int nSide = -1);
	CExposure*			 GetExposure(CPrintSheet& rPrintSheet, int nColorDefinitionIndex);
	CExposure*			 GetTopmostExposure(CPrintSheet* pPrintSheet);
	int					 RealSide2LayoutSide(int nRealSide);
	int					 LayoutSide2RealSide(int nLayoutSide);
	BOOL				 IsMultipagePart(CPrintSheet& rPrintSheet, int nSide = -1);
	BOOL				 IsMultipagePart(CPageTemplate& rTemplate, int nSide = -1);
	BOOL				 IsPanoramaPage(CPrintSheet& rPrintSheet);
	BOOL				 PanoramaHasNeighbour(CLayoutObject* pBuddy, int nSide);
	CLayoutObject*		 GetCurrentMultipagePartner(CPageTemplate* pPageTemplate, BOOL bIsPanoramaPage);
	float				 GetObjectRefline(int nReferenceLine, BOOL bBorder = FALSE);
	void				 UpdateReflineDistances(BYTE nDefaultReferenceLineInX = 255, BYTE nDefaultReferenceLineInY = 255);
	float				 GetLayoutObjectRefline(BYTE nReferenceLine, BYTE nDefaultReferenceLine = 255);
	int					 GetMirrorHeadPosition(int nMirrorAxis);
	void				 Draw(CDC* pDC, BOOL bHighlight = TRUE, BOOL bThumbnail = FALSE, BOOL bShowMark = TRUE, CPrintSheet* pPrintSheet = NULL, int nProductPartIndex = ALL_PARTS, BOOL bShowShingling = TRUE, BOOL bShowMeasures = FALSE, CDisplayList* pDisplayList = NULL, BOOL bShowNoTemplate = FALSE);
	static void			 DrawPageID				(CDC* pDC, CRect& rPageRect,   CLayoutObject& rObject, CPrintSheet* pActPrintSheet, CPageTemplate& rPageTemplate, CPrintSheetView* pView = NULL, COLORREF crBackground = RGB(242,242,255), BOOL bShowMeasures = FALSE, BOOL bShowNoPageTemplate = FALSE);
	static void			 DrawObjectID			(CDC* pDC, CRect& rObjectRect, CLayoutObject& rObject, const CString& strID, const CString& strID2 = CString(""), int nDimmedText = 0, CPrintSheetView* pView = NULL, BOOL bHighlight = TRUE, int nStatus = CLayoutObject::Normal);
	static void			 DrawObjectIDFlatProduct(CDC* pDC, CRect& rObjectRect, CLayoutObject& rObject, const CString& strID, CPrintSheet* pPrintSheet, CPrintSheetView* pView, COLORREF crBackground = RGB(242,242,255), BOOL bShowMeasures = FALSE, BOOL bShowNoPageTemplate = FALSE); 
	static void			 CreatePageIDFont(CDC* pDC, CFont* pFont, int nHeadPosition, BOOL bThumbnail = FALSE, int nDefaultFontSize = 0);
	static BOOL			 TextFits(CDC* pDC, int nXPos, int nYPos, CRect rcObject, int nHeadPosition, const CString& string);
	void				 DrawPreview(CDC* pDC, CPrintSheet* pPrintSheet, int nZOrder) ;
	void				 DrawMark(CDC* pDC);
	void				 DrawMarkProperties(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet);
	CRect				 CalcGrossFrame(CPrintSheet* pPrintSheet);
	CRect				 CalcOverFoldFrame(CPrintSheet* pPrintSheet);
	void				 DrawOverFold(CDC* pDC, CPrintSheet* pPrintSheet, BOOL bThumbnail = FALSE);
	void				 DrawPageShingling(CDC* pDC, CPageTemplate* pPageTemplate, int nPosition);
	static int			 RotateHeadPosition(int nHeadPosition, int nAngle);
	void				 GetCurrentGrossCutBlock(CPrintSheet* pPrintSheet, float& fX, float& fY, float& fWidth, float& fHeight, BOOL bRelativToPaper = TRUE);
	void				 GetCurrentPageCutSize(CPrintSheet* pPrintSheet, float& fWidth, float& fHeight);
	void				 GetCurrentGrossBox	(CPrintSheet* pPrintSheet, float& fLeft, float& fBottom, float& fRight, float& fTop);
	float				 GetCurrentGrossMargin(CPrintSheet* pPrintSheet, int nDirection);
	CFoldSheet*			 GetCurrentFoldSheet(CPrintSheet* pPrintSheet);
	CBoundProduct&		 GetCurrentBoundProduct(CPrintSheet* pPrintSheet);
	CFlatProduct&		 GetCurrentFlatProduct(CPrintSheet* pPrintSheet);
	void				 GetCutblockMatrix(int& nNX, int& nNY);
	BOOL				 IsDynamicMark();
	BOOL				 IsAutoMarkDisabled(CPrintSheet* pPrintSheet, int nSide);
	void				 RemoveMarkInstance();
	int					 GetSpineSide();
	int					 GetDisplayColorIndex(CPrintSheet* pPrintSheet);
	int					 GetCurrentAttribute(CPrintSheet* pPrintSheet);
	void				 FindLayoutObjectCluster(CObArray& rObjectPtrList, CPrintSheet* pPrintSheet, BOOL bNormalize = TRUE);
private:
	void				 FindClusterNeighbours(CLayoutObject *pObject, int nDirection, float& fStepX, float& fStepY, CObArray& rObjectPtrList, CPrintSheet* pPrintSheet, BOOL bNormalize);

//deprecated:
public:
	CLayoutObject*		 FindOldCounterpartObject();
};

template <> void AFXAPI SerializeElements <CLayoutObject> (CArchive& ar, CLayoutObject* pLayoutObject, INT_PTR nCount);	// Give SerializeElements() access to 


class CSheetObjectRefs
{
public:
	CSheetObjectRefs() 
	{
		m_nObjectType		= -1;
		m_nMarkType			= -1;
		m_pLayoutObject		= NULL;
		m_pObjectTemplate	= NULL;
	};
	CSheetObjectRefs(int nObjectType, int nMarkType, CLayoutObject* pLayoutObject, CPageTemplate* pObjectTemplate)
	{
		m_nObjectType		= nObjectType;
		m_nMarkType			= nMarkType;
		m_pLayoutObject		= pLayoutObject;
		m_pObjectTemplate	= pObjectTemplate;
	};

//Attributes:
public:
	int				m_nObjectType;
	int				m_nMarkType;
	CLayoutObject*	m_pLayoutObject;
	CPageTemplate*	m_pObjectTemplate;

//Operations:
public:
	static int Compare(const void *p1, const void *p2);
};


class CLayoutObjectList : public CList <CLayoutObject, CLayoutObject&>	// CList is derived from CObject
{																		// - so serialization will run
public:
    DECLARE_SERIAL( CLayoutObjectList )
	CLayoutObjectList();
	CLayoutObjectList(const CLayoutObjectList& rLayoutObjectList);


//Attributes:
private:
	class CLayoutSide* m_pLayoutSide;	// Pointer to layout side which contains this list - will not be serialized.
	BOOL			   m_bInvalid;		// Invalid after layout objects has been added or removed
public:
	BOOL			   m_bDisplayOpen;	// Not to be serialized

friend class CLayoutSide;				// Will be initialized in constructor of CLayoutSide (so define as friend to give access)


//Operations:
public:
	inline class CLayoutSide* GetLayoutSide() { if (this == NULL) return NULL; else return m_pLayoutSide; };
	const CLayoutObjectList&  operator=(const CLayoutObjectList& rLayoutObjectList);
	void					  Copy(const CLayoutObjectList& rLayoutObjectList);
	void					  Serialize(CArchive& ar, CLayoutSide* pLayoutSide);
	POSITION				  AddTail(CLayoutObject& rLayoutObject);
	void					  RemoveAll();
	void					  RemoveAt(POSITION pos);
	void					  RemoveObject(CLayoutObject* pObject);
	int						  GetIndex(POSITION posRef);
	void					  RemoveUnplausibleObjects();
	inline void				  ResetPublicFlags() { POSITION pos = GetHeadPosition(); while (pos) GetNext(pos).m_nPublicFlag = 0; };
	BOOL					  IsInvalid() { return m_bInvalid; };
	void					  Invalidate();
	inline void				  Validate()  { m_bInvalid = FALSE; };
	int						  GetSheetObjectRefList(CPrintSheet* pPrintSheet, CArray <CSheetObjectRefs, CSheetObjectRefs&>* pObjectRefList = NULL, int nType = -1, BOOL bLabel = FALSE);
	BOOL					  FindMarkGroup(const CArray <CString, CString&>& markGroups, const CString& strName);
	int						  GetObjectIndex(CLayoutObject* pObject);

protected:													// Inserting objects is only possible via AddTail()
	POSITION	  AddHead(CLayoutObject& rLayoutObject);	// because we have to initialize the list pointer of the object to be inserted
	void		  SetAt(POSITION pos, CLayoutObject& rLayoutObject);
	POSITION	  InsertBefore(POSITION pos, CLayoutObject& rLayoutObject);
	POSITION	  InsertAfter(POSITION pos, CLayoutObject& rLayoutObject);
	CLayoutObject RemoveHead();
	CLayoutObject RemoveTail();
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////
////// CLayoutObjectMatrix

class CLayoutObjectMatrixElem
{
public:
	CLayoutObjectMatrixElem();
	CLayoutObjectMatrixElem(CLayoutObject* pObject, int nFlag) { m_pObject = pObject; m_nFlag = nFlag; };

friend class CLayoutObjectMatrix;

private:
	CLayoutObject* m_pObject;
	int			   m_nFlag;
};


class CLayoutObjectMatrixRow : public CArray <CLayoutObject*, CLayoutObject*>
{
public:
	CLayoutObjectMatrixRow();
	CLayoutObjectMatrixRow(const CLayoutObjectMatrixRow& rLayoutObjectMatrixRow); // copy constructor


friend class CLayoutSide;

//Attributes:
private:

//Operations:
public:
	const CLayoutObjectMatrixRow& operator=(const CLayoutObjectMatrixRow& rLayoutObjectMatrixRow);
};


class CLayoutObjectMatrix : public CArray <CLayoutObjectMatrixRow, CLayoutObjectMatrixRow&>
{
public:
	CLayoutObjectMatrix(CLayoutObjectList& objectList, int nTakeWhat);

friend class CLayoutSide;

enum {TakePages = 1, TakeFoldSheet = 2, TakeByFlags = 4};

//Attributes:
private:
	CArray <CLayoutObjectMatrixElem, CLayoutObjectMatrixElem&> m_elems;


//Operations:
public:
	CLayoutObject* GetObject(int nRowIndex, int nColIndex);
	void		   GetIndices	   (CLayoutObject* pObject, BYTE& nRowIndex, BYTE& nColIndex);
	void		   GetMatrixIndices(CLayoutObject* pObject, BYTE& nRowIndex, BYTE& nColIndex);
private:
	CLayoutObject* FindLowerLeft();
	CLayoutObject* FindNextOnRight(CLayoutObject* pRefObject);
};



////////////////////////////////////////////////////////////////////////////////////////////////
//////// CStepAndRepeatMatrix = homogeneous block of objects - that means: each row has same size, same step in x and same step in y

class CStepAndRepeatElem
{
public:
	CStepAndRepeatElem();
	CStepAndRepeatElem(CLayoutObject* pObject, int nFlag) { m_pObject = pObject; m_nFlag = nFlag; };

friend class CStepAndRepeatMatrix;

private:
	CLayoutObject* m_pObject;
	int			   m_nFlag;
};


class CStepAndRepeatRow : public CArray <CLayoutObject*, CLayoutObject*>
{
public:
	CStepAndRepeatRow();
	CStepAndRepeatRow(const CStepAndRepeatRow& rStepAndRepeatRow); // copy constructor


friend class CLayoutSide;

//Attributes:
private:

//Operations:
public:
	const CStepAndRepeatRow& operator=(const CStepAndRepeatRow& rStepAndRepeatRow);
};


class CStepAndRepeatMatrix : public CArray <CStepAndRepeatRow, CStepAndRepeatRow&>
{
public:
	CStepAndRepeatMatrix();

friend class CLayoutSide;

enum {TakePages = 1, TakeFoldSheet = 2, TakeByFlags = 4, TakeGrossBox = 8};

//Attributes:
private:
	CArray <CStepAndRepeatElem, CStepAndRepeatElem&> m_elems;
	BOOL											 m_bNormalize;
	int												 m_nNx;
	int												 m_nNy;
	float											 m_fXPos;
	float											 m_fYPos;
	float											 m_fStepX;
	float											 m_fStepY;
	int												 m_nHeadPosition;
	CPoint											 m_ptInitial;
	float											 m_fBoxLeft, m_fBoxBottom, m_fBoxRight, m_fBoxTop;



//Operations:
public:
	void		   Reset();
	void		   Create(CLayoutObject* pInitialObject, CPoint ptInitial, CPrintSheet* pPrintSheet, int nTakeWhat, BOOL bNormalize);
	CLayoutObject* GetObject(int nRowIndex, int nColIndex);
	void		   GetMatrixIndices(CLayoutObject* pObject, BYTE& nRowIndex, BYTE& nColIndex);
	int			   GetNx();
	int			   GetNy();
	void		   SetNx(int nNx) { m_nNx = nNx; };
	void		   SetNy(int nNy) { m_nNy = nNy; };
	float		   GetXPos();
	float		   GetYPos();
	float		   GetStepX();
	float		   GetStepY();
	int			   GetHeadPosition(int nRow, int nCol);
	CPoint		   GetPtInitial() { return m_ptInitial; };
	void		   SetBoxLeft	(float fBoxLeft)   { m_fBoxLeft	  = fBoxLeft; };
	void		   SetBoxBottom	(float fBoxBottom) { m_fBoxBottom = fBoxBottom; };
	void		   SetBoxRight	(float fBoxRight)  { m_fBoxRight  = fBoxRight; };
	void		   SetBoxTop	(float fBoxTop)	   { m_fBoxTop	  = fBoxTop; };
	float		   GetBoxLeft()  { return m_fBoxLeft; };
	float		   GetBoxBottom(){ return m_fBoxBottom; };
	float		   GetBoxRight() { return m_fBoxRight; };
	float		   GetBoxTop()	 { return m_fBoxTop; };
private:
	CLayoutObject* FindLowerLeft();
	CLayoutObject* FindNextOnRight(CLayoutObject* pRefObject);
};


////////////////////////////////////////////////////////////////////////////////////////

typedef CArray<CRect, CRect&> CFreeAreas;	// list of free spaces on sheet side


class CLayoutSide : public CObject
{
public:
    DECLARE_SERIAL( CLayoutSide )
	CLayoutSide();

enum Level {POS_ALL, POS_SHEET};


//Attributes:
public:
	CString			  m_strSideName;
	BYTE			  m_nLayoutSide;
	float			  m_fPaperLeft, m_fPaperBottom, m_fPaperRight, m_fPaperTop;
	float			  m_fPaperWidth, m_fPaperHeight;
	CString			  m_strFormatName;
	BYTE			  m_nPaperOrientation;
	int				  m_nPaperGrain;
	BYTE			  m_nPaperRefEdge;	
	int				  m_nPressDeviceIndex;
	BOOL			  m_bAdjustFanout;
	CFanoutList		  m_FanoutList;
	CLayoutObjectList m_ObjectList;
	CPlateList		  m_DefaultPlates;		// Default exposing data which is saved with standard layouts.
											// When new plates/exposures are created, these values will be taken, 
											// if suitable plate (correct color) does exist.
	CLayoutObjectList m_cutBlocks;
	CFreeAreas		  m_freeAreas;

private:
	BOOL			  m_bInvalid;
	class CLayout*	  m_pLayout;			// Pointer to layout which contains this side - will not be serialized      
	CFont			  m_smallFont;

friend class CLayout;						// Will be initialized in constructor of CLayout (so define as friend to give access)
	

//Operations:
public:
	const CLayoutSide&		operator=(const CLayoutSide& rLayoutSide);
	void					ReinitLinks(CLayout* pLayout);
	void					Copy(const CLayoutSide& rLayoutSide);
    void					Serialize(CArchive& archive);
	inline class CLayout*	GetLayout() { if (this == NULL) return NULL; else return m_pLayout; };
	BOOL					IsInvalid() { return m_bInvalid; };
	void					Invalidate();
	inline void				Validate()	 { m_bInvalid = FALSE; };
	BOOL					IsSimilar(CLayoutSide& rLayoutSide);
	BOOL					IsIdentical(CLayoutSide& rLayoutSide);
	class CPressDevice*		GetPressDevice(CImpManDoc* pAuxDoc = NULL);
	void					GetFoldSheetObjects(int nComponentRefIndex, CObArray* pObjectPtrList, 
												float* fBBoxLeft  = NULL, float* fBBoxBottom = NULL, 
												float* fBBoxRight = NULL, float* fBBoxTop    = NULL, BOOL bPagesOnly = TRUE, CPrintSheet* pPrintSheet = NULL);
	CLayoutObject*			GetFirstFoldSheetLayerNeighbour  (int nComponentRefIndex, int nSide);
	CLayoutObject*			GetLeftmostFoldSheetLayerObject	 (int nComponentRefIndex); 
	CLayoutObject*			GetRightmostFoldSheetLayerObject (int nComponentRefIndex); 
	CLayoutObject*			GetBottommostFoldSheetLayerObject(int nComponentRefIndex); 
	CLayoutObject*			GetTopmostFoldSheetLayerObject	 (int nComponentRefIndex); 
	void					SpreadSheet(CLayoutObject* pObject, int nSide, float fSpace);
	void					CalcBoundingBoxAll(float* fBBoxLeft, float* fBBoxBottom, float* fBBoxRight, float* fBBoxTop, BOOL bExcludeMarks = FALSE, CPrintSheet* pPrintSheet = NULL, int nSide = 0);
	BOOL					CheckPaper();
	BOOL					FindSuitablePaper(BOOL bDontChangeIfFit = FALSE, float fFoldSheetWidth = 0.0f, float fFoldSheetHeight = 0.0f, CPressDevice* pPressDevice = NULL);
	void					TurnAll();
	void					TumbleAll();
	void					TurnLocalMaskAllExposures();
	void					TumbleLocalMaskAllExposures();
	void					PositionLayoutSide(float fGripper = FLT_MAX, CPressDevice* pPressDevice = NULL);	// Calculate the position of the LayoutObjects after changing Sheet,PressDevice...
	void					RecenterPaperY();
	void					CalcBoundingRectObjects(float* left, float* bottom, float* right, float* top, float* width, float* height, BOOL bWithMarks, int nComponentRefIndex, BOOL bWithBorders, CPrintSheet* pPrintSheet);		// Calculates the outer rectangle of the LayoutObjects
	BOOL					CalcBoundingRectObjects(float* left, float* bottom, float* right, float* top, float* width, float* height, BOOL bWithMarks, BOOL bWithBorders, CPrintSheet* pPrintSheet);
	static BOOL				CalcBoundingRectObjects(float* left, float* bottom, float* right, float* top, float* width, float* height, BOOL bWithMarks, CObArray& rObjectPtrList, BOOL bWithBorders, CPrintSheet* pPrintSheet = NULL);
	void					OffsetFoldSheetLayer(int nComponentRefIndex, float fOffsetX, float fOffsetY);
	void					Analyse(BOOL bAnalyseBorders = TRUE);
	void					FindCutBlocks();
	void					AnalyseCutblocks();
	void					FindFreeAreas(CPrintSheet* pPrintSheet, float fMinWidth = 0.0f, float fMinHeight = 0.0f);
	void					UnifyFreeAraes(CFreeAreas& rFreeAreas);
	void					FindLargestFreeArea(CPrintSheet* pPrintSheet, float& fLeft, float& fBottom, float& fRight, float& fTop);
	CRect					FindLargestFreeArea(CPrintSheet* pPrintSheet, float fMinWidth, float fMinHeight, int* pnIndex = NULL);
	BOOL					IsInsideFreeArea(CPrintSheet* pPrintSheet, float fLeft, float fBottom, float fRight, float fTop);
	void					AnalyseObjectGroups(CPrintSheet* pPrintSheet);
	CLayoutObject*			FindGroupLowerLeft(int nGroupIndex);
	CLayoutSide*			GetOppositeLayoutSide();
	void					ResetObjectGroupAttributes(CPrintSheet* pPrintSheet);
	void					ResetPublicFlags();
	float					GetFanoutOffsetX(const CString& strColorName, float fXPos);
	float					GetFanoutOffsetY(const CString& strColorName, float fYPos);
	float					GetRefline(int nReferenceItem, int nReferenceLine, class CObjectRefParams* pObjectRefParams);
	int 					RemoveMarkObjects(int nMarkType = -1, BOOL bRemoveAll = FALSE);
	void					RemoveMarkObject(const CString strName);
	BOOL					FindIdenticalMark(float fLeft, float fBottom, float fRight, float fTop, CPageTemplate* pMarkTemplate);
	int						GetSelectedObjectRefList(CDisplayList& rDisplayList, CArray <CSheetObjectRefs, CSheetObjectRefs&>* pObjectRefList = NULL);
	CLayoutObject*			FindOverlappingPage(float fLeft, float fBottom, float fRight, float fTop);
	BOOL					ClipToObjects(float& fLeft, float& fBottom, float& fRight, float& fTop, float fBorder, int nDirect, BOOL bPagesOnly = FALSE);
	void					Draw(CDC* pDC, BOOL bShowMarks = TRUE, BOOL bShowShingling = TRUE, BOOL bShowCutlines = FALSE, BOOL bShowMeasures = FALSE, BOOL bThumbnail = FALSE, CPrintSheet* pPrintSheet = NULL, 
								 int nProductPartIndex = ALL_PARTS, CFoldSheet* pFoldSheet = NULL, BOOL bHighlight = TRUE, BOOL bShowPreviews = FALSE, CDisplayList* pDisplayList = NULL, BOOL bShowOccupied = FALSE, BOOL bShowNoTemplate = FALSE);
	void					DrawOccupied(CDC* pDC, CPrintSheet* pPrintSheet, BOOL bOutputted = FALSE);
	void					DrawGripper(CDC* pDC, CPrintSheet* pPrintSheet);
	void					DrawPreviews(CDC* pDC, CPrintSheet* pPrintSheet, BYTE nType, int nZOrder = -1);
	void					DrawRefEdge(CDC* pDC, CPrintSheet* pPrintSheet);
	void					DrawCutLines(CDC* pDC, CPrintSheet* pPrintSheet, int nProductPartIndex, BOOL bThumbnail = FALSE);
	//void					DrawFoldSheet(CDC* pDC, int nComponentRefIndex, CString strFoldSheetNumber, CFoldSheet* pActFoldSheet, CLayout* pActLayout, CPrintSheet* pActPrintSheet, BOOL bThumbnail = FALSE, CDisplayList* pDisplayList = NULL, BOOL bDimmed = FALSE);
	void					DrawFoldSheet(CDC* pDC, UINT nActLayoutSide, CPrintSheet* pActPrintSheet, int nActFoldSheetRefIndex, CFoldSheet* pFoldSheet, BOOL bThumbnail = FALSE, CDisplayList* pDisplayList = NULL);
	void					DrawCutBlock(CDC* pDC, CRect rcFrame, CLayoutObject& rCutBlock, CLayout* pActLayout, BOOL bThumbnail = FALSE, CDisplayList* pDisplayList = NULL, BOOL bDimmed = FALSE);
	CLayoutObject*			GetFoldSheetCutBlock(int nComponentRefIndex);
	CLayoutObject*			GetLabelCutBlock(int nComponentRefIndex, int nBlockNumber);
	void					SetAllVisible(CPrintSheetView* pView = NULL);
	void					SetFoldSheetLayerVisible(int nComponentRefIndex, BOOL bShow);
	void					SetFlatProductVisible(int nComponentRefIndex, BOOL bShow);
	void					SetMarkVisible(CString strMarkName, BOOL bShow);
	CLayoutObject*			FindFirstLayoutObject(CString strName, int nType);
	CLayoutObject*			FindNextLayoutObject(CLayoutObject* pLayoutObject, CString strName, int nType);
	int						GetTotalNUp();
	CLayoutObject*			GetLayoutObjectByIndex(int nLayoutObjectIndex);
	CLayoutObject*			FindObjectSamePosition(CLayoutObject& rLayoutObject);

private:
	void					ResetAnalyseFlags(BOOL bCutBlocks = FALSE);
};


class CFoldSheetDropTargetInfo
{
public:
	CFoldSheetDropTargetInfo()	
	{	
		m_nTargetType			= -1; 
		m_pTargetPrintSheet		= NULL; 
		m_pTargetLayoutSide		= NULL; 
		m_pTargetLayoutObject	= NULL; 
		m_nTargetLayerRefIndex	= -1; 
		m_fTargetLeft			= 0.0f;
		m_fTargetBottom			= 0.0f;
		m_fTargetRight			= 0.0f;
		m_fTargetTop			= 0.0f;
		m_nDockingDirection		= -1; 
		m_nDockingAlignment		= -1;
		m_bDockingRotate		= FALSE;
		m_dropEffect			= DROPEFFECT_NONE;
		m_point					= CPoint(0, 0);
		m_pDisplayList			= NULL;
	};

	CFoldSheetDropTargetInfo(int nTargetType, CPrintSheet* pTargetPrintSheet, CLayoutSide* pTargetLayoutSide, CLayoutObject* pTargetLayoutObject, int nTargetLayerRefIndex,
							 float fTargetLeft, float fTargetBottom, float fTargetRight, float fTargetTop, int nDockingDirection, int nDockingAlignment, BOOL bDockingRotate)
	{
		m_nTargetType			= nTargetType; 
		m_pTargetPrintSheet		= pTargetPrintSheet; 
		m_pTargetLayoutSide		= pTargetLayoutSide; 
		m_pTargetLayoutObject	= pTargetLayoutObject; 
		m_nTargetLayerRefIndex	= nTargetLayerRefIndex; 
		m_fTargetLeft			= fTargetLeft;
		m_fTargetBottom			= fTargetBottom;
		m_fTargetRight			= fTargetRight;
		m_fTargetTop			= fTargetTop;
		m_nDockingDirection		= nDockingDirection; 
		m_nDockingAlignment		= nDockingAlignment;
		m_bDockingRotate		= bDockingRotate;
		m_dropEffect			= DROPEFFECT_NONE;
		m_point					= CPoint(0, 0);
		m_pDisplayList			= NULL;
	}
		
	~CFoldSheetDropTargetInfo() {};

	enum dropTargetType {DropTargetSheet, DropTargetFoldSheet, DropTargetLayoutObject};

public:
	int			   m_nTargetType;		
	CPrintSheet*   m_pTargetPrintSheet;
	CLayoutSide*   m_pTargetLayoutSide;
	CLayoutObject* m_pTargetLayoutObject;
	int			   m_nTargetLayerRefIndex;
	float		   m_fTargetLeft, m_fTargetBottom, m_fTargetRight, m_fTargetTop;
	int			   m_nDockingDirection;
	int			   m_nDockingAlignment;
	BOOL		   m_bDockingRotate;
	DROPEFFECT	   m_dropEffect;
	CPoint		   m_point;
	CDisplayList*  m_pDisplayList;
};


class CProductionProfile;
class CPrintGroup;


/////////////////////////////////////////////////////////////////////////////
// CLayoutVariantObject

class CLayoutVariantObject : public CObject
{
public:
    DECLARE_SERIAL( CLayoutVariantObject )
	CLayoutVariantObject();
	CLayoutVariantObject(const CLayoutVariantObject& rLayoutVariantObject); // copy constructor

enum Type{ FlatProduct = 1, FoldSheet = 2};

public:
	int		m_nObjectType;
	int		m_nObjectID;
	float	m_fPositionX;
	float	m_fPositionY;
	int		m_nRotation;

public:
	const CLayoutVariantObject& operator=(const CLayoutVariantObject& pLayoutVariantObject);
};

template <> void AFXAPI SerializeElements <CLayoutVariantObject> (CArchive& ar, CLayoutVariantObject* pLayoutVariantObject, INT_PTR nCount);


/////////////////////////////////////////////////////////////////////////////
// CLayoutVariant 

class CLayoutVariant : public CObject
{
public:
    DECLARE_SERIAL( CLayoutVariant )
	CLayoutVariant();
	CLayoutVariant(const CLayoutVariant& rLayoutVariant); // copy constructor

public:
	CString													m_strFormatName;
	float													m_fWidth;
	float													m_fHeight;
	int														m_nPaperGrain;
	int														m_nNumSheets;
	CArray <CLayoutVariantObject, CLayoutVariantObject&>	m_objects;
	CFoldSheetList											m_foldSheetList;

public:
	const CLayoutVariant& operator=(const CLayoutVariant& rLayoutVariant);
};

template <> void AFXAPI SerializeElements <CLayoutVariant> (CArchive& ar, CLayoutVariant* pLayoutVariant, INT_PTR nCount);



/////////////////////////////////////////////////////////////////////////////
// CLayout 

class CLayout : public CObject
{
public:
    DECLARE_SERIAL( CLayout )
	CLayout();
	CLayout(const CLayout& rLayout); // copy constructor
	~CLayout();


//Attributes:
public:
	CString										m_strLayoutName;
	CString										m_strNotes;
	CString										m_strLastAssignedMarkset;
	int											m_nPrintingProfileRef;
	CArray <int, int>							m_FoldSheetLayerRotation;	// Corresponds to foldsheets on layout and indicates the rotation (0/90/180/270)
	CArray <int, int>							m_FoldSheetLayerTurned;		// Corresponds to foldsheets on layout and indicates if turned or not
	CLayoutSide									m_FrontSide, m_BackSide;
	int											m_nProductType;				// WORK_AND_BACK ; WORK_AND_TURN ; WORK_AND_TUMBLE ; 
																			// Analysis for drawing backside and name
	BOOL										m_bAskForChangeToWorkAndBack;
	BOOL										m_bAskForChangeToWorkAndTurnTumble;
	CMarkList									m_markList;
	CArray <CLayoutVariant, CLayoutVariant&>	m_variants;

	int											m_nFlag;	// not to be serialized

//Operations:
public:
	const CLayout&		operator=(const CLayout& rLayout);
	void				ReinitLinks();
	void				Copy(const CLayout& rLayout);
	void				Create(float fPaperLeft, float fPaperBottom, float fPaperRight, float fPaperTop, int nPrintingProfileIndex, CString strMarkset = _T(""));
	void				Create(CFoldSheet& rFoldSheet, int nLayerIndex, int nProductPartIndex);
	void				Create(CLayout* pSourceLayout, int nSourceLayerRefIndex, int nProductPartIndex);
//	void				Create(int nProductPartIndex, int nPressDeviceIndex = -1);
	void				Create(class CPrintingProfile& rPrintingProfile);
	int					GetIndex();
	class CPrintingProfile&	GetPrintingProfile();
	CPrintGroup&		GetPrintGroup();
	BOOL				AssignAutoMarks();
	void				DeleteContents();
	BOOL				IsEmpty();
	BOOL				IsSimilar(CLayout& rLayout);
	CPrintSheet*		GetFirstPrintSheet();
	CPrintSheet*		GetLastPrintSheet(int nLayerIndex = -1, BOOL bIgnoreEmpty = FALSE);
	CPrintSheet*		GetLastEmptyPrintSheet();
	CPrintSheet*		GetNextPrintSheet(CPrintSheet* pPrintSheet);
	CPrintSheet*		GetPrevPrintSheet(CPrintSheet* pPrintSheet);
	void				AssignDefaultName();
	int					GetNumDefaultName();
	void				IncreaseNumLayoutName();
	int					GetNumLayoutName();
	int					KindOfProduction(); // WORK_AND_TURN <-> WORK_AND_TUMBLE
	void				RotateFoldSheet90(int nComponentRefIndex, BOOL bClockwise = FALSE, CPrintSheet* pDefaultPrintSheet = NULL, int nSide = -1, BOOL bInnerForm = FALSE);
	void				TurnFoldSheet(int nComponentRefIndex, CPrintSheet* pDefaultPrintSheet = NULL);
	void				TurnSelectedObjects();
	void				RemoveFoldSheetLayer(int nComponentRefIndex, CPressDevice* pPressDevice = NULL, BOOL bRearrange = FALSE);
	void				RemoveFoldSheetLayerAll();
	void				CopyAndPasteFoldSheet(CPrintSheet* pSourcePrintSheet, CPrintSheet* pTargetPrintSheet, int nSourceLayerRefIndex, BOOL bSourceInnerForm, 
													  CFoldSheetDropTargetInfo dropTargetInfo, BOOL bTargetBackSide, CLayoutObject* pSpreadObject, BOOL bRearrange = TRUE);
	void				CutAndPasteFoldSheet(CPrintSheet* pSourcePrintSheet, CPrintSheet* pTargetPrintSheet, int nSourceLayerRefIndex, BOOL bSourceInnerForm, 
													  CFoldSheetDropTargetInfo dropTargetInfo, BOOL bTargetBackSide, CLayoutObject* pSpreadObject, BOOL bRearrange = TRUE);
	void				RearrangeToPasteLayer(int nDockingLayerRefIndex, int nDockingSide, float fSourceBBoxLeft,  float fSourceBBoxBottom, 
																						   float fSourceBBoxRight, float fSourceBBoxTop, CPrintSheet* pTargetPrintSheet = NULL);
	BOOL				SetWorkStyle(int nWorkStyle);
	void				SetTurnStyle(int nTurnStyle);
	int					GetCurrentWorkStyle();
	BOOL				CheckForProductType(CImpManDoc* pDoc = NULL);// Analysis for drawing backside and name
	BOOL				CheckForProductType_Old(CImpManDoc* pDoc = NULL);
	int					GetNumPrintSheetsBasedOn();
	int					GetNumEmptyPrintSheetsBasedOn();
	int					GetMaxNumPrintSheetsBasedOn();
	int					AddPrintSheetInstance(int nNum);
	int					RemovePrintSheetInstance(int nNum, BOOL bRemoveFromBookblock = FALSE);
	void				AlignLayoutObjects(int nDirection, CPrintSheet* pPrintSheet = NULL);
	void				MoveLayoutObjects(float fXDiff, float fYDiff);
	void				RearrangePaper(BOOL bDontChangeIfFit = FALSE, BOOL bPromptUser = FALSE);
	void				RearrangeGeometry(CBoundProductPart& rProductPart, CPrintSheet* pPrintSheet);
	CLayoutObject*		GetSectionMarkSpineArea(CPrintSheet* pPrintSheet, int nComponentRefIndex, float& fSpineLeft, float& fSpineBottom, float& fSpineRight, float& fSpineTop, BOOL bComingAndGoing, int nInstance);
	CLayoutObject*		GetSectionMarkHeadFootArea(CPrintSheet* pPrintSheet, int nComponentRefIndex, float& fRefLeft, float& fRefBottom, float& fRefRight, float& fRefTop, BOOL bComingAndGoing, int nInstance);
	INT_PTR				GetNumFoldSheets() {return m_FoldSheetLayerRotation.GetSize();};
	int					GetNumFlatProducts();
	CSize				GetExtents(CPrintSheet* pPrintSheet, int nSide, BOOL bShowMeasuring, CPoint* pPtOrg = NULL, BOOL bInsidePrintSheetView = TRUE, CPressDevice* pPressDevice = NULL);
	void				UpdateMarkTemplateLinks(short nOldMarkTemplateIndex, short nNewMarkTemplateIndex);
	void				ExtractMarkTemplates(CPageTemplateList& rTargetList, CPageTemplateList& rSourceList);
	BOOL				MarkTemplateExists(short nMarkTemplateIndex);
	CLayoutObject*  	FindSimilarObject(CLayoutObject* pLayoutObject);
	int 				RemoveMarkObjects(int nMarkType = -1, BOOL bRemoveAll = FALSE);
	void				ResetMarkInstanceDefs(const CString strMarkName);
	void				RemoveFlatProductType(int nRefIndex);
	void				CheckLayoutObjectFormats(int nRefIndex, float fWidth, float fHeight, const CString strFormatName, int nOrientation, BOOL bTypeLabel = FALSE);
	void				SetPrintSheetDefaultPlates();
	void				RotateLayoutObjects(BOOL bClockwise);
	CSize				FindFoldSheetMatrix(float fFoldSheetWidth, float fFoldSheetHeight, CPressDevice* pPressDevice, BOOL& bRotate);
	void				AddFoldSheetLayer(CFoldSheet* pFoldSheet, int nLayerIndex, float fFoldSheetPosX, float fFoldSheetPosY, CBoundProductPart& rProductPart, int nOrientation, CPrintSheet* pDefaultPrintSheet = NULL);
	CString				GetPrintingType();
	CString				GetTurningType();
	CRect				GetDrawingBox(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice = NULL, int nSide = 1, BOOL bShowMeasures = FALSE, BOOL bInsidePrintSheetView = TRUE);
	CRect				GetDrawingBox(CWnd* pWnd, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice = NULL, int nSide = 1, BOOL bShowMeasures = FALSE, BOOL bInsidePrintSheetView = TRUE);
	void				DrawHeader(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice, BOOL bShort = TRUE);
	CRect				DrawData(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, class CPrintComponent* pComponent, CDisplayList* pDisplayList);
	CRect				DrawColorList(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, int nSide, CDisplayList* pDisplayList);
	CRect				Draw(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice, BOOL bShowFoldSheets = TRUE, BOOL bShowCutlines = FALSE, BOOL bShowMeasures = FALSE, CFoldSheet* pFoldSheet = NULL, 
							 BOOL bThumbnail = FALSE, int nSide = 1, class CMarkList* pMarkList = NULL, class CMediaSize* pMediaSize = NULL, CDisplayList* pDisplayList = NULL, BOOL bHighlight = TRUE, BOOL bShowPreviews = FALSE, BOOL bShowSelected = FALSE, BOOL bShowOccupied = FALSE, BOOL bShowNoTemplate = FALSE);
	void				DrawSide(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CPressDevice* pPressDevice, BOOL bShowFoldSheets = TRUE, BOOL bShowCutlines = FALSE, BOOL bShowMeasures = FALSE, CFoldSheet* pFoldSheet = NULL, 
							 BOOL bThumbnail = FALSE, int nSide = 1, class CMarkList* pMarkList = NULL, class CMediaSize* pMediaSize = NULL, CDisplayList* pDisplayList = NULL, BOOL bHighlight = TRUE, BOOL bShowPreviews = FALSE, BOOL bShowOccupied = FALSE, BOOL bShowNoTemplate = FALSE);
	void				CreateThumbnail(CThumbnailBitmap& rThumbnailBitmap, CPrintSheet& rPrintSheet, CSize size);
	static int			TransformFontSize(CDC* pDC, float fWorld, int nScreenMin = 0, int nWorldMin = 250);
	BOOL				HasMarks(int nMarkType = -1);
	BOOL				HasPages();
	BOOL				HasFlatProducts();
	CLayoutObject*		GetFirstFlatObject();
	void				AssignSheet(CSheet& rSheet);
	void				AnalyzeMarks(BOOL bUpdateViews = TRUE, CMark* pMark = NULL, CPrintSheet* pPrintSheet = NULL);
	void				DecrementMarkListColorDefIndices(int nColorDefinitionIndex);
	BOOL				ColorInUseMarkList(CImpManDoc* pDoc, int nColorDefinitionIndex, BOOL bSeparationOnly, BOOL bCompositeOnly);
	int					GetMaxNUp(CPrintSheet& rPrintSheet, CFoldSheet* pNewFoldSheet,  BOOL bDoRotate, int* pnNumFoldSheetsX = NULL, int* pnNumFoldSheetsY = NULL, CRect rcPlaceArea = CRect(0,0,0,0));
	int					GetMaxNUp(CPrintSheet& rPrintSheet, CFlatProduct& pFlatProduct, BOOL bDoRotate, int* pnNumProductsX = NULL,	  int* pnNumProductsY = NULL,   CRect rcPlaceArea = CRect(0,0,0,0));
	int					MakeNUp			 (BOOL bSample, CPrintSheet& rPrintSheet, CFoldSheet*	  pFoldSheet,	int nLayerIndex, int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition = TRUE, CRect rcPlaceArea = CRect(0,0,0,0), int* pnNumFoldSheetsX = NULL, int* pnNumFoldSheetsY = NULL);
	int					MakeNUp			 (BOOL bSample, CPrintSheet& rPrintSheet, CFlatProduct* pFlatProduct,					 int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition = TRUE, CRect rcPlaceArea = CRect(0,0,0,0), int* pnNumProductsX	 = NULL, int* pnNumProductsY   = NULL);
	int					MakeSequential	 (BOOL bSample, CPrintSheet& rPrintSheet, CFoldSheet*   pFoldSheet,		int nLayerIndex, int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition = TRUE, CRect rcPlaceArea = CRect(0,0,0,0));
	int					MakeWorkAndTurn	 (BOOL bSample, CPrintSheet& rPrintSheet, CFoldSheet*   pFoldSheet,		int nLayerIndex, int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition = TRUE, CRect rcPlaceArea = CRect(0,0,0,0));
	int					MakeWorkAndTumble(BOOL bSample, CPrintSheet& rPrintSheet, CFoldSheet*   pFoldSheet,		int nLayerIndex, int nNUp, int nHeadPosition, CPrintingProfile* pPrintingProfile, BOOL bReposition = TRUE, CRect rcPlaceArea = CRect(0,0,0,0));
	CLayoutObject*  	FindMarkObject(CString strMarkName);
	float				CalcUtilization(CPrintSheet* pPrintSheet = NULL, CFlatProduct* pFlatProduct = NULL);
	float				CalcUtilization(CPrintSheet* pPrintSheet, CFoldSheet* pFoldSheet);
	int					GetTotalNUp();
	void				SelectLayoutVariant(CPrintSheet& rPrintSheet, int nIndex);
	int					GetSuitableFoldSheetObjects(int nNX, int nNY);
	CSize				GetFoldSheetLayerMatrix(int nComponentRefIndex);
	void				UpdatePressParams(CPressDevice& rPressDeviceFront, CPressDevice& rPressDeviceBack);
	void				SetObjectsStatusAll(int nStatus);
	void				SetObjectsStatusFoldSheetLayer(int nComponentRefIndex, int nStatus);
	void				ReorganizeColorInfos();
	BOOL				IsUniform();
};

template <> void AFXAPI SerializeElements <CLayout> (CArchive& ar, CLayout* pLayout, INT_PTR nCount);



/////////////////////////////////////////////////////////////////////////////
// CLayoutList itself - will be subclassed in PrintSheetList

class CLayoutList : public CList <CLayout, CLayout&>	// CList is derived from CObject
{														// - so serialization will run
public:
    DECLARE_SERIAL( CLayoutList )
	CLayoutList();


//Attributes:
	//CPressDeviceList m_PressDevices;


//Operations:
public:
	void 		Serialize(CArchive& ar);
	void 		RemoveAt(POSITION pos);
	int	 		GetSimilarLayout(CLayout* pLayout, BOOL bSearchBefore = FALSE);
	void 		RearrangeMarkTemplateIndices(int nIndex);
	CLayout*	FindLayout(const CString& strLayoutName);
	void		AnalyzeMarks();
	void		DecrementMarkListColorDefIndices(int nColorDefinitionIndex);
	BOOL		ColorInUseMarkList(CImpManDoc* pDoc, int nColorDefinitionIndex, BOOL bSeparationOnly = FALSE, BOOL bCompositeOnly = FALSE);
	void		SetAllFoldSheetLayerVisible();
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


					   

/////////////////////////////// CPrintSheetList /////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Declaration of CPrintSheetList and all included subclasses
// Implementation in "ImpManDocCPrintSheetList.cpp"


class CFoldSheetLayerRef
{
public:
	CFoldSheetLayerRef() :	m_nFoldSheetIndex(-1), 
							m_nFoldSheetLayerIndex(-1) {};

	CFoldSheetLayerRef(short nFoldSheetIndex, short nLayerIndex) :
							m_nFoldSheetIndex(nFoldSheetIndex), 
							m_nFoldSheetLayerIndex(nLayerIndex) {};
//Attributes:
public:
	short m_nFoldSheetIndex;
	short m_nFoldSheetLayerIndex;

//Operations:
public:
	BOOL operator==(const CFoldSheetLayerRef& rFoldSheetLayerRef);
	BOOL operator!=(const CFoldSheetLayerRef& rFoldSheetLayerRef);

	static int CFoldSheetLayerRef::CompareFoldSheetIndex(const void *p1, const void *p2);
};


class CFoldSheetType
{
public:
	CFoldSheetType() {m_pFoldSheet = NULL; m_nFoldSheetLayerIndex = 0; m_nNumUp = 0;};
	CFoldSheetType(CFoldSheet* pFoldSheet, short nFoldSheetLayerIndex, int nNumUp) {m_pFoldSheet = pFoldSheet; 
																					m_nFoldSheetLayerIndex = nFoldSheetLayerIndex; 
																					m_nNumUp = nNumUp;};

//Attributes:
public:
	CFoldSheet* m_pFoldSheet;
	short		m_nFoldSheetLayerIndex;
	int			m_nNumUp;		// no. foldsheets of this type (i.e. 2up, 4up, etc.) placed on this printsheet

//Operations:
public:
	static int Compare(const void *p1, const void *p2);
};


class CFlatProductRef
{
public:
	CFlatProductRef() :	m_nFlatProductIndex(-1), m_nAttribute(-1) {};

	CFlatProductRef(short nFlatProductIndex, short nAttribute) : m_nFlatProductIndex(nFlatProductIndex), m_nAttribute(nAttribute) {};
//Attributes:
public:
	short m_nFlatProductIndex;
	short m_nAttribute;

//Operations:
public:
	BOOL operator==(const CFlatProductRef& rFlatProductRef);
	BOOL operator!=(const CFlatProductRef& rFlatProductRef);

	static int CFlatProductRef::CompareFlatProductIndex(const void *p1, const void *p2);
};

class CFlatProductType
{
public:
	CFlatProductType() {m_pFlatProduct = NULL; m_nFlatProductIndex = 0; m_nNumUp = 0;};
	CFlatProductType(class CFlatProduct* pFlatProduct, short nFlatProductIndex, int nNumUp) {	m_pFlatProduct = pFlatProduct; m_nFlatProductIndex = nFlatProductIndex; 
																								m_nNumUp = nNumUp;};

//Attributes:
public:
	CFlatProduct*	m_pFlatProduct;
	short			m_nFlatProductIndex;
	int				m_nNumUp;		// no. flat products of this type placed on this printsheet

//Operations:
public:
	static int Compare(const void *p1, const void *p2);
};


class CPrintSheetComponentRef
{
public:
	enum {FoldSheetLayer, FlatProduct};

public:
	int				   m_nType;
	CFoldSheetLayerRef m_layerRef;
	int				   m_nFlatProductIndex;
	int				   m_nComponentRefIndex;


public:
	static int Compare(const void *p1, const void *p2);
};



static BOOL	   g_bDummy;
static CString g_strDummy;

class CPrintSheet : public CObject
{
public:
    DECLARE_SERIAL( CPrintSheet )
	CPrintSheet();
	CPrintSheet(const CPrintSheet& rPrintSheet); // copy constructor

enum PrintSheetType {TopBottom, WorkAndTurn, WorkAndTumble};
enum OutfileRelation{PlatePerFile, SidePerFile, SheetPerFile, JobPerFile};


protected:
	CString	 m_strID;

//Attributes:
private:
	CLayout*					 m_pLayout;
	int							 m_nProductPartIndex;
	struct displayInfo
	{
		CRect rcDisplayBoxSheet;
		CRect rcDisplayBoxComponents;
	}							 m_dspi;

public:
	int							 m_nPrintSheetNumber;
	CString						 m_strNumerationStyle;
	int							 m_nNumerationOffset;
	int							 m_nPrintSheetParentIndex; 
	unsigned					 m_nPrintSheetType;
	CArray <CFoldSheetLayerRef, 
		    CFoldSheetLayerRef&> m_FoldSheetLayerRefs;
	CArray <CFlatProductRef, 
		    CFlatProductRef&>	 m_flatProductRefs;
	float  	    				 m_fShinglingOffset, m_fShinglingStep;
	int							 m_nLayoutIndex;
	CPlateList					 m_FrontSidePlates;
	CPlateList					 m_BackSidePlates;
	CString						 m_strPDFTarget;

	BOOL						 m_bDisplayOpen;			// Not to be serialized
	CArray<CFoldSheetType,									// Holds the different foldsheets and how many of them are placed on this printsheet.
		   CFoldSheetType&>		 m_FoldSheetTypeList;		// This list is used temporary only - so will not be serialized
	CArray<int, int>			 m_boundProductPartRefList;	// Holds the different bound productparts indices.
															// This list is used temporary only - so will not be serialized
	CArray<CFlatProductType,								// Holds the different flat products and how many of them are placed on this printsheet.
		   CFlatProductType&>	 m_flatProductTypeList;		// This list is used temporary only - so will not be serialized
	CArray<CFlatProductType,								// Holds the different flat product formats and how many of them are placed on this printsheet. Needed by GetFirstFlatFormat()/GetNextFlatFormat()
		   CFlatProductType&>	 m_flatProductFormatList;	// This list is used temporary only - so will not be serialized

	int							 m_nQuantityExtra;
	int							 m_nQuantityTotal;

	CArray <int, int>			 m_markStates;


//Operations:
public:
	const						CPrintSheet& operator=(const CPrintSheet& rPrintSheet);
	void						SetID(const CString& strID) { m_strID = strID; };
	CString&					GetID() { return m_strID; };
	void						ReinitLinks();
	void						Copy(const CPrintSheet& rPrintSheet);
	void						Create(CFoldSheetLayerRef layerRef, int nLayoutIndex, int nProductPartIndex, CLayout* pLayout = NULL);
	void						Create(int nLayoutIndex, int nProductPartIndex, CLayout* pLayout = NULL);
	void						DeleteContents();
	CRect						GetDisplayBoxSheet() { return m_dspi.rcDisplayBoxSheet; };
	void						SetDisplayBoxSheet(CRect& rcBox) { m_dspi.rcDisplayBoxSheet = rcBox; };
	CRect						GetDisplayBoxComponents() { return m_dspi.rcDisplayBoxComponents; };
	void						SetDisplayBoxComponents(CRect& rcBox) { m_dspi.rcDisplayBoxComponents = rcBox; };
	void						LinkLayout(CLayout* pLayout) {m_pLayout = pLayout;};
	int							GetIndex();
	class CLayout*				GetLayout(CImpManDoc* pAuxDoc = NULL);
	class CPrintingProfile&		GetPrintingProfile() { return GetLayout()->GetPrintingProfile(); };
	class CPrintGroup&			GetPrintGroup();
	int							GetPrintGroupIndex() { return m_nProductPartIndex; };
	void						SetPrintGroupIndex(int nProductPartIndex) { m_nProductPartIndex = nProductPartIndex; };
	CString						GetPrintGroupName();
	CPrintComponent				GetFirstPrintComponent();
	CPrintComponentsGroup&		GetFirstPrintComponentsGroup();
	class CBoundProductPart*	GetFirstBoundProductPart();
	class CBoundProductPart*	GetNextBoundProductPart(CBoundProductPart* pProductPart);
	CFlatProductType*			GetFirstFlatFormatType();
	CFlatProductType*			GetNextFlatFormatType(CFlatProductType* pFlatProductType);
	CFoldSheet*					GetFirstFoldSheetProductPart(int nProductPartIndex);
	CFoldSheet*					GetFoldSheet		(int nComponentRefIndex);
	CFoldSheetLayer*			GetFoldSheetLayer	(int nComponentRefIndex);
	CFlatProduct&				GetFlatProduct		(int nComponentRefIndex);
	int							GetFlatProductIndex	(int nComponentRefIndex);
	BOOL						HasCoverProduct();
	BOOL						HasCoverProductsOnly();
	BOOL						HasPerfectBoundProduct();
	BOOL						HasBoundProductPart(int nProductPartIndex);
	int							GetMaxProductQuantity();
	int							MakeFlatProductRefIndex(int nFlatProductIndex);
	int							GetNUpsFoldSheetLayerRefIndex(const CString& strFoldSheetTypeName, int nLayerIndex, int nUp);
	int							GetNumUpFoldSheetType(const CString& strFoldSheetTypeName, int nLayerIndex);
	int							LayerTypeToLayerRef(int nLayerTypeIndex);
	int							FoldSheetLayerToLayerRef(CFoldSheetLayer* pLayer);
	int							GetNextLayerRefIndex(CFoldSheet* pFoldSheet, int nComponentRefIndex);
	int							GetNextLayerRefIndex(CString strFoldSheetTypeName, int nProductPartIndex, int nComponentRefIndex);
	void						CopyAndPasteFoldSheet(CPrintSheet* pSourcePrintSheet,	int nSourceLayerRefIndex, BOOL bSourceInnerForm,
													 CFoldSheetDropTargetInfo dropTargetInfo, BOOL bTargetBackSide, BOOL bAutoAssign, CLayoutObject* pSpreadObject, BOOL bRearrange = TRUE);
	void						CutAndPasteFoldSheet (CPrintSheet* pSourcePrintSheet,	int nSourceLayerRefIndex, BOOL bSourceInnerForm,
													 CFoldSheetDropTargetInfo dropTargetInfo, BOOL bTargetBackSide, BOOL bAutoAssign, CLayoutObject* pSpreadObject, BOOL bRearrange = TRUE);
	void						CopyFoldSheetLayerData(CPrintSheet* pPrintSheet, int nComponentRefIndex);
	BOOL						RemoveFoldSheetLayerNUp(int nLayerTypeIndex);
	BOOL						RemoveFoldSheetLayer (int nComponentRefIndex, BOOL bAutoAssign = FALSE, BOOL bRemoveFromLayout = FALSE, BOOL bIgnoreLayout = FALSE, BOOL bDontAsk = FALSE, CPressDevice* pPressDevice = NULL, BOOL& bPrintSheetRemoved = g_bDummy, BOOL bRemoveFromBookblock = FALSE);
	BOOL						RemoveFlatProductType(int nComponentRefIndex, BOOL bAutoAssign = FALSE, BOOL bRemoveFromLayout = FALSE, BOOL bIgnoreLayout = FALSE, BOOL bDontAsk = FALSE, CPressDevice* pPressDevice = NULL, BOOL& bPrintSheetRemoved = g_bDummy);
	void						RemoveFoldSheetLayerAll();
	int							GetPlatesSheetSide(CPlate* pPlate);
	CPlate*						GetTopmostPlate(int nLayoutSide);
	int							GetTopmostPlateIndex(int nLayoutSide);
	void						InitializeFoldSheetTypeList();
	void						InitializeFlatProductTypeList();
	void						InitializeFlatProductRefsByFormats();
	BOOL						IsSuitableLayout(CLayout* pLayout);
	int							GetNumOutputObjects(int nSide = 4, CPlate* pPlate = NULL, int nObjectType = -1);	// 4 == BOTHSIDES
	BOOL						IsOutputDisabled();
	void						EnableOutput(BOOL bEnable = TRUE, int nSide = 4);			// 4 == BOTHSIDES
	CString						GetOutFilename		 (int nSide, CPlate* pPlate = NULL);
	CString						GetOutFileFullPath	 (int nSide, CPlate* pPlate = NULL, BOOL bJDFMarksOutput = FALSE, BOOL bCreateMissingPath = FALSE);
	CString						GetOutFileTmpFullPath(int nSide, CPlate* pPlate = NULL);
	CString						GetCuttingDataOutFilename	 ();
	CString						GetCuttingDataOutFileFullPath();
	CString						GetCuttingDataOutFileTmpFullPath();
	BOOL						OutFileExists(BOOL bExact, int nSide, CPlate* pPlate = NULL);
	static int					GetOutfileRelation(const CString& strOutputFilename);
	CString						GetLastTimestamp (int nSide, CPlate* pPlate = NULL);
	int							GetNumPages		   (int nSide = 4);
	int							GetNumPagesOccupied(int nSide = 4, CString& rstrMissingPages = g_strDummy);
	BOOL						NotYetOutputted	   (int nSide = 4);
	BOOL						ReadyForOutput(int nSide, BOOL bAutoCmd);
	void						ResetLastOutputDates(int nSide = 4);	// 4 = BOTHSIDES
	BOOL						HasPageSource(CPageSource* pPageSource);
	COleDateTime				GetLastOutputDate();
	CString						GetOutputFormat();
	int							GetMinPagenum(int nSide, int nProductPartIndex = ALL_PARTS, BOOL bTakeIndex = FALSE);
	int							GetMaxPagenum(int nSide, int nProductPartIndex = ALL_PARTS, BOOL bTakeIndex = FALSE);
	CString						GetAllPagenums(int nSide);
	static int					ComparePageNums(const void *p1, const void *p2);
	CString						GetNumber();
	int							GetNumberOnly() { return m_nPrintSheetNumber; };// + m_nNumerationOffset; };
	BOOL						ContainsPrintColor(int nColorDefinitionIndex, int nSide = 4, CString strSeparation = _T(""));	// 4 == BOTHSIDES
	int							GetSpotColor(int nSide, int nSpotColorNum);
	int							GetNumColors(int nSide, BOOL bSpotsOnly = FALSE);
	void						GetProcessColors(int nSide, CStringArray& plateNames);
	void						SubstituteFoldSheetIndex(CFoldSheet& rNewFoldSheet, int nNewFoldSheetIndex, int nLayerIndex);
	BOOL						FindFreePosition(float fObjectWidth, float fObjectHeight, float& fLeft, float& fBottom, int* pnNumObjectsX = NULL, int* pnNumObjectsY = NULL, int* pnTotalNUp = NULL, class CPunchingTool* pPunchTool = NULL, float fPercentTolerance = 0.0f);
	BOOL						FindFreePos		(float& fLeft, float& fBottom, float fObjectWidth, float fObjectHeight, int* pnNumX = NULL, int* pnNumY = NULL, int* pnTotalNUp = NULL, class CPunchingTool* pPunchTool = NULL);
	BOOL						FindFreePosition(CFoldSheet* pFoldSheet, int nLayerIndex, float& fLeft, float& fBottom, int* pnNumFoldSheetsX = NULL, int* pnNumFoldSheetsY = NULL, int* pnTotalNUp = NULL, BOOL bDontRotate = FALSE);
	BOOL						FindFreePos		(float& fLeft, float& fBottom, float fMinBottom, float fObjectWidth, float fObjectHeight, int* pnNumX = NULL, int* pnNumY = NULL, int* pnTotalNUp = NULL, BOOL bDontRotate = FALSE, CPoint ptClicked = CPoint(0,0));
	CLayoutObject*				FindClosestLayoutObject(int nLayoutSide, float fLeft, float fBottom, float fRight, float fTop, int nDirection);
	void						RearrangeLayout(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, CPrintSheet& newPrintSheet, CLayout& newLayout, int nNUp = INT_MAX);
	BOOL						HasDifferentFoldSheets();
	BOOL						HasFoldSheetsOnly();
	BOOL						HasFoldSheets();
	BOOL						HasFlatProductsOnly();
	BOOL						HasFlatProducts();
	BOOL						IsEmpty();
	int							GetNumPagesProductPart(int nProductPartIndex);
	CString						GetGlobalMaskValueString(int nSide);
	BOOL						HasLocalMaskValues(int nSide);
	BOOL						HasAdjustContentToMask(int nSide);
	CString						FindSeparation(CString strSep, int nSide);
	BOOL						EqualSeparationColorsFrontBack();
	CRect						DrawColors(CDC* pDC, CRect rcFrame, int nSide);
	CRect						GetPlanningViewBox(CDC* pDC, CRect rcFrame, int nPrintSheetIndex, BOOL bShowDetails);
	CRect						DrawPlanningViewLayoutInfo(CDC* pDC, CRect rcFrame, int nPrintSheetIndex, CDisplayList* pDisplayList = NULL);
	CRect						DrawPlanningView		  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground);
	CRect						DrawComponents			  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground);
	void						DrawPlanningTableLinks	  (CDC* pDC, BOOL bHighlight, CDisplayList* pDisplayList = NULL);
	CString						CalcTotalQuantity(BOOL bReset = FALSE);
	int							GetNumComponents();
	BOOL						GetPaperName(CString& strPaperName);
	BOOL						HasObjects();
	CLayout*					SplitLayout();
	int							GetMaxNumCloneable(CFoldSheet* pFoldSheet, int nNumPagesToImpose);
	void						GetFoldSheetLayerReflines(int nComponentRefIndex, int nSide, int nReferenceLineX, int nReferenceLineY, float& fLeft, float& fBottom);
	void						SetFoldSheetVisible(CFoldSheet& rFoldSheet, BOOL bVisible);
	void						ReorganizeColorInfos();
};

template <> void AFXAPI SerializeElements <CPrintSheet> (CArchive& ar, CPrintSheet* pPrintSheet, INT_PTR nCount);
void AFXAPI DestructElements(CPrintSheet* pPrintSheet, INT_PTR nCount);





/////////////////////////////////////////////////////////////////////////////
// CPrintSheetList itself


typedef struct
{
	int nComponentRefIndex;
	int nFoldSheetIndex;
}LAYERSORTLIST;


class CPrintSheetList : public CList <CPrintSheet, CPrintSheet&>	// CList is derived from CObject
{																	// - so serialization will run
public:
    DECLARE_SERIAL( CPrintSheetList )
	CPrintSheetList();

	enum { NOTIFY_START_OUTPUT_SHEET = WM_USER + 1, NOTIFY_END_OUTPUT_SHEET, NOTIFY_UPDATE_VIEW};


//Attributes:
	CLayoutList		m_Layouts;
	CPDFTargetList	m_pdfTargets;
	CString			m_strIPUJobname;
	BOOL			m_bIPUAllcolors;
	CString			m_strIPUColorString;


private:
	CArray <CLayout*, CLayout*> m_alreadyProcessed;


public:
//Operations:
	CPrintSheet* GetFirstPrintSheet(int nProductPartIndex = ALL_PARTS);
	CPrintSheet* GetNextPrintSheet(CPrintSheet* pPrintSheet, int nProductPartIndex = ALL_PARTS);
	CPrintSheet* GetPrintSheetByID(CString& strID);
	CPrintSheet* GetPrintSheetFromNumber(int nPrintSheetNumber);
	int			 GetPrintSheetIndex(CPrintSheet* pPrintSheet);
	CLayout*	 GetFirstLayout();
	CLayout*	 GetNextLayout(CLayout* pLayout);
	BOOL		 LayoutAlreadyProcessed(CLayout* pLayout);
	void		 ReIndexFoldSheets(int nIndex);
	void		 RemoveFoldSheet(int nIndex);
	void		 RemoveFlatProductType(int nFlatProductIndex);
	void		 CheckLayoutObjectFormats(int nRefIndex, float fWidth, float fHeight, const CString strFormatName, int nOrientation, BOOL bTypeLabel = FALSE);
	void		 TakeOverNewFoldSheets(CPrintSheet* pPrintSheet = NULL, CLayout* pLayout = NULL, int nProductPartIndex = ALL_PARTS);
	void		 RemoveUnneededFoldSheetsComingAndGoing();
	BOOL		 CheckFoldSheetComingAndGoing(CFoldSheet& rFoldSheet);
	int			 FoldSheetLayerIsAssigned(int nFoldSheetIndex, int nFoldSheetLayerIndex);
	int			 GetInsertionPos(CFoldSheet& rFoldSheet, int nFoldSheetNumber);
	CPrintSheet* FindSuitablePrintSheet(CFoldSheet& rFoldSheet, int nLayerIndex);
	void		 ReNumberPrintSheets();
	static int	 CompareLayoutPos(const void *p1, const void *p2);
	void		 InvalidateAllPlates();
	void		 SetGlobalMaskAllPlates(float fValue, CLayout* pLayout = NULL);
	void		 SetLocalMaskAllExposures(CExposure* pExposure, int nPosition, float fValue);
	void		 UnlockLocalMaskAllExposures(CExposure* pExposure);
	void		 AdjustContent2LocalMaskAllExposures(CExposure* pExposure);

public:
	void		 Serialize(CArchive& ar);
	void		 RemoveAll();			// Override Remove functions, because after removing a printsheet
	void		 RemoveAt(POSITION pos);// we have to do some reorganization
	void		 RemoveAt(CPrintSheet* pPrintSheet);
	void		 RemovePrintSheet(CPrintSheet* pPrintSheet);
	CPrintSheet* InsertBefore(CPrintSheet* pRefPrintSheet, CPrintSheet& rNewPrintSheet);
	CPrintSheet* InsertAfter (CPrintSheet* pRefPrintSheet, CPrintSheet& rNewPrintSheet);
	void		 CopyAndPasteFoldSheet(CPrintSheet* pPrintSheet, int nComponentRefIndex);
	void		 CutAndPasteFoldSheet (CPrintSheet* pTargetPrintSheet, BOOL bReverse);
	void		 DecrementColorDefIndices(short nColorDefinitionIndex);
	void		 InitializeFoldSheetTypeLists();
	void		 InitializeFlatProductTypeLists();
	void		 RemoveFlatProductRefs(CLayout* pLayout, int nFlatProductRefIndex);
	void		 EnableOutput(BOOL bEnable = TRUE);
	BOOL		 OutputPDFCheckPageSourceLinks(const CString& strDocTitle);
	int			 OutputImposition(const CString& strDocTitle);
	int			 OutputCuttingData(const CString& strDocTitle);
	int			 OutputSheetsAuto(const CString& strDocTitle);
	BOOL		 ReadyForOutput(int nSide, BOOL bAutoCmd);
	void		 ResetLastOutputDates();
	void		 GetOutputSheetInfo(CJobInfoExchange& rJobInfoExchange);
	void		 NotifyClients(int nMsg);
	void		 ReorganizeColorInfos();
	void		 GetProcessColors(CStringArray& plateNames);
	void		 SortByFoldSheets();
	CPrintSheet* AddBlankSheet(CPrintingProfile& rPrintingProfile, int nInsertionIndex = -1, CPrintGroup* pPrintGroup = NULL);
	CPrintSheet* GetBlankSheet(int nProductPartIndex = ALL_PARTS);
	BOOL		 ImposeNUp(CFoldSheet* pFoldSheet, CPrintSheet* pTargetPrintSheet, int nNUp = 1);
	BOOL		 ImposeNUpLayout(CFoldSheet* pFoldSheet, CPrintSheet* pTargetPrintSheet, int nNUp = INT_MAX);
	BOOL		 ImposeCollectLayout(CFoldSheet* pFoldSheet, CPrintSheet* pTargetPrintSheet, int nNumSheets);
	CFoldSheet*	 ReImposePrintSheetType(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, CPrintSheet* pTargetPrintSheet, CPrintSheet& newPrintSheet, CLayout& newLayout);
	BOOL		 RearrangeLayout(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, CPrintSheet* pTargetPrintSheet, int nComponentRefIndex, int nNUp = INT_MAX);
	BOOL		 BookblockChangeFoldSheets(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, CPrintSheet* pRefPrintSheet, int nComponentRefIndex, int nNumNewSheets);
	CFoldSheet*	 AddFoldSheet(CFoldSheet& rNewFoldSheet, int nNUp, CPrintSheet* pTargetPrintSheet, CFoldSheet* pFoldSheetInsertAfter = NULL, int nPrintSheetNumSheets = INT_MAX);
	static int	 CompareFoldSheetIndex(const void *p1, const void *p2);
	void		 ClonePrintSheets(CFoldSheet& rNewFoldSheet, int nLayerIndex, CPrintSheet* pSourcePrintSheet, int nMaxNumSheets = -1);
	CFoldSheet*	 GetNextUnassignedFoldsheetLayer(CFoldSheet& rRefFoldSheet, int& nFoldSheetIndex, int nPagesX, int nPagesY, int& nLayerIndex);
	BOOL		 DuplicateFoldSheet(CFoldSheet& rNewFoldSheet, int nNUp, CPrintSheet* pTargetPrintSheet);
	int			 GetNumOutputObjects(int nObjectType);
	POSITION	 GetPos(CPrintSheet& rPrintSheet);
	BOOL		 PrintSheetExist(CPrintSheet* pPrintSheet);
	CFoldSheet*	 GetNextNewFoldSheet(CArray <int, int>& newFoldSheets, int nNewFoldSheetIndex);
	CPrintSheet* JDFAddPrintSheet(CString strSheetName, float fPaperWidth, float fPaperHeight, float fPlateWidth, float fPlateHeight, CString strPressDeviceName, CString strPressDeviceID, int nProductPartIndex, CPrintingProfile& rPrintingProfile);
	CPrintSheet* FindPrintSheet(CString strSheetName);
	BOOL		 DoAutoMarks();
	void		 UpdateQuantities();
	CRect		 Draw(CDC* pDC, CRect rcFrame, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList = NULL, CPrintSheet** ppPrintSheet = NULL);
	CRect		 PrintHeader(CDC* pDC, CRect rcFrame);
	CRect		 DrawPrintSheetRow(CDC* pDC, CRect rcFrame, CPrintSheet& rPrintSheet, int nPrintSheetIndex, CArray <int, int>& rColumnWidths, CDisplayList* pDisplayList = NULL);
	CRect		 DrawPrintSheetComponents(CDC* pDC, CRect rcFrame, CPrintSheet& rPrintSheet, CArray <int, int>& rColumnWidths);

protected:
	CPrintSheet RemoveHead();	// Protect these functions for unauthorized access
	CPrintSheet RemoveTail();
};



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Struct - Declarations needed for Trim - functions

class CTrimOptions	// Button-Settings for Trimmings
{
public:
	BOOL m_bChannelThrough,	// All trough or single
		 m_bChannelSingle,

		 m_bHoriUp,			// Moving - directions
		 m_bHoriLow,
		 m_bHoriSym,

		 m_bVertLeft,
		 m_bVertRight,
		 m_bVertSym;
};


/////////////////////////////////////////////////////////////////////////////
// CScreenLayout ////////////////////////////////////////////////////////////

class CZoomState : public CObject
{
public:
    DECLARE_SERIAL( CZoomState )
	CZoomState();
	CZoomState(const CZoomState& rZoomState); // copy constructor
	CZoomState(CSize viewportSize, CPoint scrollPos) { m_viewportSize = viewportSize; m_scrollPos = scrollPos; };

//Attributes:
public:
	CSize  m_viewportSize;
	CPoint m_scrollPos;

//Operations:
public:
	const CZoomState& operator=(const CZoomState& rZoomState);
	void Serialize(CArchive& ar);
};


class CScreenLayout : public CObject
{
public:
    DECLARE_SERIAL( CScreenLayout )
	CScreenLayout();

//Attributes:
public:
	CZoomState m_zoomStateBookBlockView;
	BOOL	   m_bLabelsShowFrontBack;
	BOOL	   m_bLabelsShowBoxShapes;

//Operations:
public:
	void Serialize(CArchive& ar);
};


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// The Imposition Manager Document itself:


#include "ImpManDocProducts.h"
#include "ImpManDocPrintComponents.h"
#include "ImpManDocDeprecated.h"


class CImpManDoc : public COleDocument
{
protected: // create from serialization only
    DECLARE_SERIAL( CImpManDoc )
	CImpManDoc();

// Attributes
public:
	// products
	CGlobalData					m_GlobalData;
	CBoundProductList			m_boundProducts;
	CFlatProductList			m_flatProducts;
	//CFoldedProductList		m_foldedProducts;
	CPrintComponentsList		m_printComponentsList;
	CPrintGroupList				m_printGroupList;
	CBookblock					m_Bookblock;

	// production
	CProductionSetup			m_productionSetup;
	//CProductionProfiles			m_productionProfiles;	deprecated since 7.0.1.5
	CPrintingProfiles			m_printingProfiles;

	// content
	CPageSourceList				m_PageSourceList;
	CPageSourceList				m_MarkSourceList;
	CPageTemplateList			m_PageTemplateList;
	CPageTemplateList			m_MarkTemplateList;
	CColorDefinitionTable		m_ColorDefinitionTable;

	// print sheets etc.
	CPrintSheetList				m_PrintSheetList;
	int							m_nDefaultPressDeviceIndex;
	CProductionData				m_productionData;
	CGraphicList				m_graphicList;

	int							m_nDongleStatus;
	CScreenLayout				m_screenLayout;

	int			 m_nActPlate;		// Actual selected Plate in TreeView or -1
	CWnd*		 m_pWndMain;
	CTrimOptions m_TrimOptions;		// Button-Settings for Trimmings

// Operations
public:
	CBoundProduct&		GetBoundProduct	   (int nProductIndex) { return m_boundProducts.GetProduct(nProductIndex); };
	CBoundProductPart&	GetBoundProductPart(int nProductPartIndex);
	int					GetFlatProductsIndex();
	CString				AssignDefaultProductName();
	int					GetNumDefaultProductName(CString strProductName);

	static CImpManDoc* GetDoc();
	BOOL   FoldSheetExisting(CFoldSheet* pFoldSheet);
	void   ResetPublicFlags();
	void   MessageIfFilesAreMissing(int ObjectType); // PS/EPS/BMP-files for loading Page || ControlMark

	int	   ProcessCommand(int nCommand, const CString& strPDFDoc = _T(""), CString strStartPage = _T(""), CString strPDFInfoFile = _T(""), CString strPreviewsFolder = _T(""));	
	int	   WriteJobInfoExchangeFile(const CString& strPath);
	void   UpdateOldFlatProductRefs();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImpManDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL SaveModified();
	virtual void OnCloseDocument();
	//}}AFX_VIRTUAL
	virtual void DeleteContents();  // delete complete document structure
public:
	virtual void SetModifiedFlag( BOOL bModified = TRUE, CRuntimeClass *pClass = NULL, BOOL bUpdateAutoMarks = TRUE, BOOL bAnalyze = TRUE );


// Implementation
public:
	virtual ~CImpManDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
// Document - Analyse - functions 
protected:
	//void AnalysePressDeviceIndices();						  // Execute after loading/creating document 
	int  CompareWithAppList(CPressDevice* PressDeviceBufDoc); // Compare CPressDevice-entries
public:
	void ReorganizePressDeviceIndices(int nLayoutSide);		  // Execute after changing PressDevices; Look for double entries etc.
	BOOL MoreThanOnePressdevice();							  // For use in DlgPressDevice and ExposerMediaSettings

///////////////////////////////////////////////////////////////////////////////////////////////
// Helper - functions 
	void SetPrintSheetPressDevice(int nIndex, int nLayoutSide, CLayout* pActLayout = NULL, BOOL bReorganize = TRUE);	// Set the PrintSheet PressDevice in Document from given selection of ListBox
//	void FillPressDeviceListBox(CComboBox* pBox);				// Fill the ListBoxes in JobSettingsPage2 and in DialogBar of PrintSheetFrame for the PressDevices 


	CPrintSheet*  		GetActPrintSheet();			// Get the active PrintSheet selected in TreeView
	CLayout*	  		GetActLayout();				// Get the active Layout selected in TreeView
	CPressDevice* 		GetActPressDevice(int nSide = 0);	// Get the active PressDevice for the active Layoutside selected in TreeView or by parameter
	int			  		GetActSight();				// Get the active LayoutSide : FRONTSIDE | BACKSIDE | BOTHSIDES | ALLSIDES
	CPlate*		  		GetActPlate();				// Get the active Plate selected in TreeView
	static CFrameWnd*	GetFrame(const CRuntimeClass* pClass);
	BOOL				DoSave(LPCTSTR lpszPathName, BOOL bReplace = TRUE);
	void				SaveToStorage(CObject* pObject);
	void				OnFileAutoBackup();			// Save the current document in temp-directory

// Generated message map functions
protected:
	//{{AFX_MSG(CImpManDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileSave();
	afx_msg void OnFileSaveAs();
	afx_msg void OnFileClose();


// deprecated
	void SerializeDeprecated(CArchive& ar);
	void SerializeOldPrintSheetList(CArchive& ar, CPrintSheetList& rOldPrintSheetList, CPressDeviceList& rPressDevices);
	void ConvertOldProductPartList(CProductPartList& productPartListDeprecated, CBoundProduct& rBoundProduct, CLabelList& rLabelList, CPressDeviceList& rPressDevices);
	void ConvertOldTextmarks();
	void ConvertOldFlatProductRefIndices();
	void ConvertBoundProductsPostpressParams(CProductionProfiles& rProductionProfiles);
	void ConvertProductionToPrintingProfiles(CProductionProfiles& rProductionProfiles);
	void ConvertFlatProductRefs();
///////////////////////////////////////////////////////////////////////////////////////////////
// Implementationfile ImpManDocCipCut.cpp
	void		  CopyPrintSheetToCutSheetList() {};						// During serialization if loading an empty cutsheetlist
	CPrintSheet*  GetActCutSheet() { return NULL; };						// Get the active PrintSheet for cutting selected in TreeView
	CLayout*	  GetActCutLayout() { return NULL; };;						// Get the active Layout for cutting selected in TreeView
	CPressDevice* GetActCutPressDevice(int /*nSide= 0*/) { return NULL; };; // Get the active PressDevice for the active Layoutside selected in TreeView or by parameter
	void		  RemovePlacedProducts();
	void		  RemoveUnplacedProducts();
//
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

