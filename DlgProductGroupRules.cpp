// DlgProductGroupRules.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductGroupRules.h"


// CDlgProductGroupRules-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductGroupRules, CDialog)

CDlgProductGroupRules::CDlgProductGroupRules(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductGroupRules::IDD, pParent)
{

}

CDlgProductGroupRules::~CDlgProductGroupRules()
{
}

void CDlgProductGroupRules::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PRINTING_DUEDATE_SPIN, m_printingDuedateSpin);
}


BEGIN_MESSAGE_MAP(CDlgProductGroupRules, CDialog)
END_MESSAGE_MAP()


// CDlgProductGroupRules-Meldungshandler

BOOL CDlgProductGroupRules::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_printingDuedateSpin.SetRange(0, 999);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}
