// DlgProductionSetups.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionSetups.h"
#include "DlgProductionManager.h"


// CDlgProductionSetup-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionSetups, CDialog)

CDlgProductionSetups::CDlgProductionSetups(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionSetups::IDD, pParent)
	, m_strComment(_T(""))
	, m_strName(_T(""))
	, m_fMaxOverProduction(0)
	, m_nRange1Min(0)
	, m_nRange2Min(0)
	, m_nRange3Min(0)
	, m_nRange4Min(0)
	, m_nRange1Max(0)
	, m_nRange2Max(0)
	, m_nRange3Max(0)
	, m_nRange4Max(0)
	, m_strProductionProfile(_T(""))
	, m_bOneTypePerSheet(FALSE)
{
	
}

CDlgProductionSetups::~CDlgProductionSetups()
{
}

void CDlgProductionSetups::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PRODUCTIONSETUP_LIST, m_productionSetupsCombo);
	DDX_Text(pDX, IDC_PRODUCTIONSETUP_COMMENT_EDIT, m_strComment);
	DDX_CBStringExact(pDX, IDC_PRODUCTIONSETUP_LIST, m_strName);
	DDX_Text(pDX, IDC_PS_PRODUCTION_PROFILE, m_strProductionProfile);
	DDX_Text(pDX, IDC_PP_MAX_OVERPRODUCTION, m_fMaxOverProduction);
	DDX_Text(pDX, IDC_PS_RANGE1_MIN, m_nRange1Min);
	DDX_Text(pDX, IDC_PS_RANGE2_MIN, m_nRange2Min);
	DDX_Text(pDX, IDC_PS_RANGE3_MIN, m_nRange3Min);
	DDX_Text(pDX, IDC_PS_RANGE4_MIN, m_nRange4Min);
	DDX_Text(pDX, IDC_PS_RANGE1_MAX, m_nRange1Max);
	DDX_Text(pDX, IDC_PS_RANGE2_MAX, m_nRange2Max);
	DDX_Text(pDX, IDC_PS_RANGE3_MAX, m_nRange3Max);
	DDX_Text(pDX, IDC_PS_RANGE4_MAX, m_nRange4Max);
	DDX_Check(pDX, IDC_PP_ONE_TYPE_PER_SHEET, m_bOneTypePerSheet);
}


BEGIN_MESSAGE_MAP(CDlgProductionSetups, CDialog)
	ON_LBN_SELCHANGE(IDC_PRODUCTIONSETUP_LIST, OnSelchangeProductionSetupList)
	ON_BN_CLICKED(IDC_SAVE, &CDlgProductionSetups::OnBnClickedSave)
	ON_BN_CLICKED(IDC_REMOVE, &CDlgProductionSetups::OnBnClickedRemove)
	ON_BN_CLICKED(IDC_PS_PRODUCTION_PROFILE_BUTTON, &CDlgProductionSetups::OnBnClickedPsProductionProfileButton)
	ON_BN_CLICKED(IDOK, &CDlgProductionSetups::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgProductionSetup-Meldungshandler

BOOL CDlgProductionSetups::OnInitDialog()
{
	CDialog::OnInitDialog();

	InitProductionSetupsCombo();

	m_oldProductionSetup = m_productionSetup;

	return TRUE;
}

void CDlgProductionSetups::InitProductionSetupsCombo()
{
	m_productionSetupsCombo.ResetContent();

	CString strWildCard;
	strWildCard.Format(_T("%s\\ProductionSetups\\*.set"), theApp.GetDataFolder()); 

	CString strNoSetup, strLabel;
	strNoSetup.LoadString(IDS_NO_SETUP);
	strLabel.Format(_T("<%s>"), strNoSetup);
	m_productionSetupsCombo.AddString(strLabel);

	CFileFind		 finder;
	BOOL			 bFound = finder.FindFile(strWildCard);
	while (bFound)
	{
		bFound = finder.FindNextFile();
		m_productionSetupsCombo.AddString(finder.GetFileTitle());
	}
	finder.Close();

	int nSel = m_productionSetupsCombo.FindStringExact(-1, m_strName);
	if (nSel == CB_ERR)
		nSel = 0;

	m_productionSetup = CProductionSetup();
	if (nSel < m_productionSetupsCombo.GetCount())
	{
		m_productionSetup = CProductionSetup();
		CString strSetup;
		m_productionSetupsCombo.GetLBText(nSel, strSetup);
		m_productionSetup.Load(strSetup);
	}

	InitData();
}

void CDlgProductionSetups::InitData()
{
	m_strName				= m_productionSetup.m_strName;
	m_strComment			= m_productionSetup.m_strComment;
	m_strProductionProfile	= m_productionSetup.m_strProductionProfile;
	m_fMaxOverProduction	= m_productionSetup.m_fMaxOverProduction;
	m_bOneTypePerSheet		= m_productionSetup.m_bOneTypePerSheet;

	m_nRange1Min = m_nRange1Max = 0;
	m_nRange2Min = m_nRange2Max = 0;
	m_nRange3Min = m_nRange3Max = 0;
	m_nRange4Min = m_nRange4Max = 0;

	for (int i = 0; i < m_productionSetup.m_quantityRanges.GetSize(); i++)
	{
		switch (i)
		{
			case 0:	m_nRange1Min = m_productionSetup.m_quantityRanges[i].nQuantityMin;
					m_nRange1Max = m_productionSetup.m_quantityRanges[i].nQuantityMax;
					break;
			case 1:	m_nRange2Min = m_productionSetup.m_quantityRanges[i].nQuantityMin;
					m_nRange2Max = m_productionSetup.m_quantityRanges[i].nQuantityMax;
					break;
			case 2:	m_nRange3Min = m_productionSetup.m_quantityRanges[i].nQuantityMin;
					m_nRange3Max = m_productionSetup.m_quantityRanges[i].nQuantityMax;
					break;
			case 3:	m_nRange4Min = m_productionSetup.m_quantityRanges[i].nQuantityMin;
					m_nRange4Max = m_productionSetup.m_quantityRanges[i].nQuantityMax;
					break;
		}
	}

	UpdateData(FALSE);
}

void CDlgProductionSetups::SaveData()
{
	UpdateData(TRUE);

	m_productionSetup.m_strName					= m_strName;
	m_productionSetup.m_strComment				= m_strComment;
	m_productionSetup.m_strProductionProfile	= m_strProductionProfile;
	m_productionSetup.m_fMaxOverProduction		= m_fMaxOverProduction;
	m_productionSetup.m_bOneTypePerSheet		= m_bOneTypePerSheet;

	CProductionSetup::QUANTITY_RANGE range;
	m_productionSetup.m_quantityRanges.RemoveAll();
	if ( (m_nRange1Min > 0) && (m_nRange1Max > m_nRange1Min) )
	{
		range.nQuantityMin = m_nRange1Min;
		range.nQuantityMax = m_nRange1Max;
		m_productionSetup.m_quantityRanges.Add(range);
	}
	if ( (m_nRange2Min > 0) && (m_nRange2Max > m_nRange2Min) )
	{
		range.nQuantityMin = m_nRange2Min;
		range.nQuantityMax = m_nRange2Max;
		m_productionSetup.m_quantityRanges.Add(range);
	}
	if ( (m_nRange3Min > 0) && (m_nRange3Max > m_nRange3Min) )
	{
		range.nQuantityMin = m_nRange3Min;
		range.nQuantityMax = m_nRange3Max;
		m_productionSetup.m_quantityRanges.Add(range);
	}
	if ( (m_nRange4Min > 0) && (m_nRange4Max > m_nRange4Min) )
	{
		range.nQuantityMin = m_nRange4Min;
		range.nQuantityMax = m_nRange4Max;
		m_productionSetup.m_quantityRanges.Add(range);
	}
}

void CDlgProductionSetups::OnSelchangeProductionSetupList() 
{
	int nSel = m_productionSetupsCombo.GetCurSel();
	if (nSel != CB_ERR)
	{
		m_productionSetup = CProductionSetup();
		CString strSetup;
		m_productionSetupsCombo.GetLBText(nSel, strSetup);
		m_productionSetup.Load(strSetup);
		InitData();

		m_oldProductionSetup = m_productionSetup;
	}
}

void CDlgProductionSetups::OnBnClickedSave()
{
	SaveData();

	m_productionSetup.m_strName	   = m_strName;
	m_productionSetup.m_strComment = m_strComment;
	m_productionSetup.Save(m_productionSetup.m_strName);
	InitProductionSetupsCombo();

	m_oldProductionSetup = m_productionSetup;
}

void CDlgProductionSetups::OnBnClickedRemove()
{
	m_productionSetup.Remove(m_productionSetup.m_strName);
	InitProductionSetupsCombo();
}

void CDlgProductionSetups::OnBnClickedPsProductionProfileButton()
{
	UpdateData(TRUE);

	CDlgProductionManager dlg;
	dlg.m_productionProfile.m_strName = m_strProductionProfile;

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_strProductionProfile = dlg.m_strProfileName;

	UpdateData(FALSE);
}

void CDlgProductionSetups::CheckModified()
{
	if (m_productionSetup != m_oldProductionSetup)
	{
		OnBnClickedSave();
		//BOOL bEntryIsNew = TRUE; 
		//POSITION pos = theApp.m_productionProfiles.GetHeadPosition();
		//while (pos)                        
		//{
		//	CProductionProfile& rProfile = theApp.m_productionProfiles.GetAt(pos);
		//	if (m_productionSetup.m_strName == rProfile.m_strName)
		//		bEntryIsNew = FALSE;
		//	theApp.m_productionProfiles.GetNext(pos);
		//}

		//CString strMsg; 
		//if (bEntryIsNew)
		//	OnNew();
		//else
		//	OnChange();
	}
}

void CDlgProductionSetups::OnBnClickedOk()
{
	SaveData();
	CheckModified();

	OnOK();
}
