#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CDlgProductionPunchingParams-Dialogfeld

class CDlgProductionPunchingParams : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionPunchingParams)

public:
	CDlgProductionPunchingParams(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionPunchingParams();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTION_PUNCHINGPARAMS };


public:
	CPoint	m_ptInitialPos;
	BOOL	m_bLocked;
	int		m_nCurSel;

protected:
	CListBox m_punchToolList;
	CSpinButtonCtrl m_punchToolNXSpin;
	CSpinButtonCtrl m_punchToolNYSpin;
	CString m_strPunchToolName;
	UINT m_nPunchToolID;
	UINT m_nPunchToolNX;
	UINT m_nPunchToolNY;
	float m_fPunchToolStepX;
	float m_fPunchToolStepY;
	float m_fPunchToolGutterX;
	float m_fPunchToolGutterY;

public:
	CProductionProfile& GetProductionProfile();
	void				InitData();
	void				SaveData();


protected:
	virtual BOOL Create(UINT nIDTemplate, CWnd* pParentWnd = NULL);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedPunchtoolSave();
	afx_msg void OnUpdatePunchtoolSave(CCmdUI* pCmdUI);
	afx_msg void OnLbnSelchangePunchtoolList();
	afx_msg void OnBnClickedPunchtoolDelete();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEnChangePunchtoolName();
	afx_msg void OnBnClickedPunchtoolDetails();
};
