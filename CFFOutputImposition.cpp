#include "stdafx.h"
#include "imposition manager.h"
#include "CFFOutputImposition.h"
#include <locale.h>



CCFFOutputImposition::CCFFOutputImposition(void)
{
	setlocale(LC_NUMERIC, "english");	// because when german setted, Format() does produce numbers with Comma like "0,138"
}

CCFFOutputImposition::~CCFFOutputImposition(void)
{
	CString strLanguage;
	switch (theApp.settings.m_iLanguage)
	{
		case 0: strLanguage = _T("GER"); setlocale(LC_ALL, "german");	break;
		case 1: strLanguage = _T("ENG"); setlocale(LC_ALL, "english");	break;
		case 2: strLanguage = _T("FRA"); break;
		case 3: strLanguage = _T("ITA"); break;
		case 4: strLanguage = _T("SPA"); break;
		case 5: strLanguage = _T("HOL"); break;	// No holl. MFC4x.dll available
		//Default would already be set at reading ini-file
	}
}

void CCFFOutputImposition::DoExport(CLayout& rLayout, CString strFilePath)
{
	CPrintSheet* pPrintSheet = rLayout.GetFirstPrintSheet();
	if ( ! pPrintSheet)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	FILE* fp;

	fp = fopen(strFilePath, _T("w"));

	// write header 
	fprintf(fp, "$BOF\n");
	fprintf(fp, "V2\n");
	fprintf(fp, "ORDER\n");
	fprintf(fp, "%%1: Krause/ArtPro CFF2-Schnittstelle Version %s\n", "1.0");//prg_version);
	fprintf(fp, "%%2: Entgegen CFF2-Definition sind Sortennamen bis 15 Zeichen lang\n");
	fprintf(fp, "END\n");
	fprintf(fp, "MAIN,%s\n", rLayout.m_strLayoutName);
	if (1)//(opt.masseinheit == MILLI)
	fprintf(fp, "UM\n");
	else
	fprintf(fp, "UI\n");
	if (1)//(opt.masseinheit == MILLI)
	{
	fprintf(fp, "LL,0.0,0.0\n");
	fprintf(fp, "UR,%4.2f,%4.2f\n", rLayout.m_FrontSide.m_fPaperWidth, rLayout.m_FrontSide.m_fPaperHeight);
	}
	else
	{
	fprintf(fp, "LL,0.0,0.0\n");
	fprintf(fp, "UR,%3.3f,%3.3f\n", rLayout.m_FrontSide.m_fPaperWidth, rLayout.m_FrontSide.m_fPaperHeight);
	}
	fprintf(fp, "SCALE,1,1\n");

	int		 nAngle = 0;
	float	 fXOff	= 0.0f, fYOff = 0.0f;
	POSITION pos	= rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject	 = rLayout.m_FrontSide.m_ObjectList.GetNext(pos);
		CPageTemplate* pTemplate = rObject.GetCurrentFlatProductTemplate(pPrintSheet);
		if ( ! pTemplate)
			continue;
		if (rObject.m_nType == CLayoutObject::ControlMark)
			continue;

		switch (rObject.m_nHeadPosition)
		{
		case TOP:		nAngle = 0;		fXOff = 0.0f;				fYOff = 0.0f;					break;
		case LEFT:		nAngle = 90;	fXOff = rObject.GetWidth();	fYOff = 0.0f;					break;
		case RIGHT:		nAngle = 270;	fXOff = 0.0f;				fYOff = rObject.GetHeight();	break;
		case BOTTOM:	nAngle = 180;	fXOff = rObject.GetWidth(); fYOff = rObject.GetHeight();	break;
		}

		if (1)//(opt.masseinheit == MILLI)
			fprintf(fp, "C,%s,%4.2f,%4.2f,%d,1,1\n", pTemplate->m_strPageID, rObject.GetLeft() - rLayout.m_FrontSide.m_fPaperLeft + fXOff, rObject.GetBottom() - rLayout.m_FrontSide.m_fPaperBottom + fYOff, nAngle);
		else
			fprintf(fp, "C,%s,%3.3f,%3.3f,%d,1,1\n", pTemplate->m_strPageID, rObject.GetLeft() - rLayout.m_FrontSide.m_fPaperLeft + fXOff, rObject.GetBottom() - rLayout.m_FrontSide.m_fPaperBottom + fYOff, nAngle);
	}

	fprintf(fp, "END\n");


	//// shape description
/*
	to be drawn in the following manner

  |<-----^
  |      |
  |      |
  |      |
  V----->|

*/
	CArray <CLayoutObject*, CLayoutObject*> objectList;
	pos = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = rLayout.m_FrontSide.m_ObjectList.GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
			continue;
		if (AlreadyInList(&rObject, objectList, pPrintSheet))
			continue;

		objectList.Add(&rObject);
	}

	float fWidth = 0.0f, fHeight = 0.0f;
	for (int i = 0; i < objectList.GetSize(); i++)
	{
		CLayoutObject* pObject	 = objectList[i];
		CPageTemplate* pTemplate = (pObject) ? pObject->GetCurrentFlatProductTemplate(pPrintSheet) : NULL;
		if ( ! pTemplate)
			continue;
	
		fprintf(fp, "SUB,%s\n", pTemplate->m_strPageID);

		switch (pObject->m_nHeadPosition)
		{
		case TOP:
		case BOTTOM:	fWidth  = pObject->GetWidth();
						fHeight = pObject->GetHeight();
						if (1)//(opt.masseinheit == MILLI)
						{
							fprintf(fp, "L,2,1,0,0.0,0.0,%4.2f,0.0,0,0\n", fWidth);
							fprintf(fp, "L,2,1,0,%4.2f,0.0,%4.2f,%4.2f,0,0\n", fWidth, fWidth, fHeight);
							fprintf(fp, "L,2,1,0,%4.2f,%4.2f,0.0,%4.2f,0,0\n", fWidth, fHeight, fHeight);
							fprintf(fp, "L,2,1,0,0.0,%4.2f,0.0,0.0,0,0\n", fHeight);
						}
						else
						{
							fprintf(fp, "L,2,1,0,0.0,0.0,%3.3f,0.0,0,0\n", fWidth);
							fprintf(fp, "L,2,1,0,%3.3f,0.0,%3.3f,%3.3f,0,0\n", fWidth, fWidth, fHeight);
							fprintf(fp, "L,2,1,0,%3.3f,%3.3f,0.0,%3.3f,0,0\n", fWidth, fHeight, fHeight);
							fprintf(fp, "L,2,1,0,0.0,%3.3f,0.0,0.0,0,0\n", fHeight);
						}
	                    break;

		case RIGHT:		// object will be rotated when readed in, so needa to be stored rotated
		case LEFT:		fWidth  = pObject->GetHeight();
						fHeight = pObject->GetWidth();
						if (1)//(opt.masseinheit == MILLI)
						{
							fprintf(fp, "L,2,1,0,0.0,0.0,%4.2f,0.0,0,0\n", fWidth);
							fprintf(fp, "L,2,1,0,%4.2f,0.0,%4.2f,%4.2f,0,0\n", fWidth, fWidth, fHeight);
							fprintf(fp, "L,2,1,0,%4.2f,%4.2f,0.0,%4.2f,0,0\n", fWidth, fHeight, fHeight);
							fprintf(fp, "L,2,1,0,0.0,%4.2f,0.0,0.0,0,0\n", fHeight);
						}
						else
						{
							fprintf(fp, "L,2,1,0,0.0,0.0,%3.3f,0.0,0,0\n", fWidth);
							fprintf(fp, "L,2,1,0,%3.3f,0.0,%3.3f,%3.3f,0,0\n", fWidth, fWidth, fHeight);
							fprintf(fp, "L,2,1,0,%3.3f,%3.3f,0.0,%3.3f,0,0\n", fWidth, fHeight, fHeight);
							fprintf(fp, "L,2,1,0,0.0,%3.3f,0.0,0.0,0,0\n", fHeight);
						}
						break;
		}

		fprintf(fp, "END\n");
	}

	fprintf(fp, "$EOF\n");
	
	fclose(fp);
}

BOOL CCFFOutputImposition::AlreadyInList(CLayoutObject* pObject, CArray <CLayoutObject*, CLayoutObject*>& rObjectList, CPrintSheet* pPrintSheet)
{
	if ( ! pObject)
		return FALSE;

	int nFlatProductIndex = (pObject->m_nComponentRefIndex >= pPrintSheet->m_flatProductRefs.GetSize()) ? pObject->m_nComponentRefIndex : pPrintSheet->m_flatProductRefs[pObject->m_nComponentRefIndex].m_nFlatProductIndex;
	for (int i = 0; i < rObjectList.GetSize(); i++)
	{
		if (rObjectList[i])
		{
			int nRefFlatProductIndex = (rObjectList[i]->m_nComponentRefIndex >= pPrintSheet->m_flatProductRefs.GetSize()) ? rObjectList[i]->m_nComponentRefIndex : pPrintSheet->m_flatProductRefs[rObjectList[i]->m_nComponentRefIndex].m_nFlatProductIndex;
			if (nFlatProductIndex == nRefFlatProductIndex)
				return TRUE;
		}
	}

	return FALSE;
}