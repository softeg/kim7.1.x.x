#if !defined(AFX_PRODUCTPARTSFRAME_H__6A965488_5C6F_415C_992E_0AE02CA5B200__INCLUDED_)
#define AFX_PRODUCTPARTSFRAME_H__6A965488_5C6F_415C_992E_0AE02CA5B200__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProductPartsFrame.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Rahmen CProductPartsFrame 

class CProductPartsFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CProductPartsFrame)
protected:
	CProductPartsFrame();           // Dynamische Erstellung verwendet geschützten Konstruktor

// Attribute
public:

// Operationen
public:

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CProductPartsFrame)
	//}}AFX_VIRTUAL

// Implementierung
protected:
	virtual ~CProductPartsFrame();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CProductPartsFrame)
		// HINWEIS - Der Klassen-Assistent fügt hier Member-Funktionen ein und entfernt diese.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_PRODUCTPARTSFRAME_H__6A965488_5C6F_415C_992E_0AE02CA5B200__INCLUDED_
