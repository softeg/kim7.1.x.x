// PrintSheetFrameLeftToolsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetFrameLeftToolsView.h"
#include "NewPrintSheetFrame.h"
#include "DlgBoundProduct.h"
#include "DlgFlatProductData.h"
#include "XMLInput.h"
#include "CSVInput.h"
#include "CFFInput.h"
#include "DlgPDFLibStatus.h"
#include "DlgPDFImport.h"
#include "GraphicComponent.h"


// CPrintSheetFrameLeftToolsView

IMPLEMENT_DYNCREATE(CPrintSheetFrameLeftToolsView, CFormView)

CPrintSheetFrameLeftToolsView::CPrintSheetFrameLeftToolsView()
	: CFormView(CPrintSheetFrameLeftToolsView::IDD)
{
	m_pPopupMenu = NULL;
	m_pPageSource = NULL;
	m_nProductPartIndex = 0;
}

CPrintSheetFrameLeftToolsView::~CPrintSheetFrameLeftToolsView()
{
	if (m_pPageSource)
	{
		delete m_pPageSource;
		m_pPageSource = NULL;
	}
}

void CPrintSheetFrameLeftToolsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL CPrintSheetFrameLeftToolsView::OnEraseBkgnd(CDC* pDC)
{
	CRect rcClient; GetClientRect(rcClient);
	pDC->FillSolidRect(rcClient, RGB(211,218,228));
	return TRUE;// CFormView::OnEraseBkgnd(pDC);
}

void CPrintSheetFrameLeftToolsView::UpdateBackground(CDC* pDC, CRect rcRect, CWnd* /*pWnd*/)
{
	pDC->FillSolidRect(rcRect, RGB(211,218,228));
}

#define ID_SHOW_COMPONENTS		1234
#define ID_PREVIOUS				ID_SHOW_COMPONENTS + 1
#define ID_SHOW_PRINTSHEETLIST	ID_SHOW_COMPONENTS + 2


BEGIN_MESSAGE_MAP(CPrintSheetFrameLeftToolsView, CFormView)
	ON_COMMAND(ID_NEW_PRODUCT, OnNewProduct)
	ON_COMMAND(ID_NEW_BOUND, OnNewBound)
	ON_COMMAND(ID_NEW_FLAT, OnNewFlat)
	ON_COMMAND(ID_DEL_PRODUCT, OnDeleteProduct)
	ON_COMMAND(ID_IMPORT_PRODUCTS, OnImportProducts)
	ON_COMMAND(ID_IMPORT_XML, OnImportXML)
	ON_COMMAND(ID_IMPORT_CSV, OnImportCSV)
	ON_COMMAND(ID_IMPORT_PDF, OnImportPDF)
	ON_COMMAND(ID_IMPORT_CFF, OnImportCFF)
	ON_UPDATE_COMMAND_UI(ID_NEW_PRODUCT, OnUpdateNewProduct)
	ON_UPDATE_COMMAND_UI(ID_NEW_BOUND, OnUpdateNewBound)
	ON_UPDATE_COMMAND_UI(ID_NEW_FLAT, OnUpdateNewFlat)
	ON_UPDATE_COMMAND_UI(ID_DEL_PRODUCT, OnUpdateDeleteProduct)
	ON_UPDATE_COMMAND_UI(ID_IMPORT_PRODUCTS, OnUpdateImportProducts)
	ON_COMMAND(ID_PREVIOUS, OnPrevious)
	ON_COMMAND(ID_SHOW_PRINTSHEETLIST, OnShowPrintSheetList)
	ON_UPDATE_COMMAND_UI(ID_PREVIOUS, OnUpdatePrevious)
	ON_UPDATE_COMMAND_UI(ID_SHOW_PRINTSHEETLIST, OnUpdateShowPrintSheetList)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
END_MESSAGE_MAP()


// CPrintSheetFrameLeftToolsView-Diagnose

#ifdef _DEBUG
void CPrintSheetFrameLeftToolsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetFrameLeftToolsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrintSheetFrameLeftToolsView-Meldungshandler

void CPrintSheetFrameLeftToolsView::OnInitialUpdate()
{
	m_toolbarButtons.Create(this, this, NULL, IDB_PRINTSHEETFRAME_LEFTTOOLS, IDB_PRINTSHEETFRAME_LEFTTOOLS, CSize(25, 25), UpdateBackground);

	CRect rcButton; 
	GetDlgItem(IDC_PRINTSHEET_LEFTTOOLS_BUTTONFRAME)->GetWindowRect(rcButton);	ScreenToClient(rcButton);
	rcButton.bottom = rcButton.top + 35;

	//m_toolbarButtons.AddButton(-1, IDS_COMPONENTS,		ID_SHOW_COMPONENTS,		rcButton, 0);	rcButton.OffsetRect(0, rcButton.Height() * 2);
	m_toolbarButtons.AddButton(-1, IDS_NEW_PRODUCT,		ID_NEW_PRODUCT,			rcButton, 1);	rcButton.OffsetRect(0, rcButton.Height()); 
	m_toolbarButtons.AddButton(-1, IDS_IMPORT_PRODUCTS,	ID_IMPORT_PRODUCTS,		rcButton, 2);	rcButton.OffsetRect(0, rcButton.Height());
	m_toolbarButtons.AddButton(-1, IDS_DEL_PRODUCT,		ID_DEL_PRODUCT,			rcButton, 3);	rcButton.OffsetRect(0, rcButton.Height());
	m_toolbarButtons.AddButton(-1, IDS_NAV_NEXT,		ID_PREVIOUS,			rcButton, 4);	rcButton.OffsetRect(0, rcButton.Height());
	m_toolbarButtons.AddButton(-1, IDS_PRINTSHEETLIST,	ID_SHOW_PRINTSHEETLIST,	rcButton, 5);	rcButton.OffsetRect(0, rcButton.Height());

	CFormView::OnInitialUpdate();

	CRect rcClient; GetClientRect(rcClient);
	SetScaleToFitSize(rcClient.Size());	//suppress scrollbars
}

void CPrintSheetFrameLeftToolsView::OnNewProduct()
{
    CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_NEW_PRODUCT));
	m_pPopupMenu = menu.GetSubMenu(0);
	if ( ! m_pPopupMenu)
		return;

    MENUITEMINFO mi;
    mi.cbSize = sizeof(MENUITEMINFO);
    mi.fMask = MIIM_TYPE;
    mi.fType = MFT_OWNERDRAW;
//     Making the menu item ownerdrawn:
	m_pPopupMenu->SetMenuItemInfo(ID_NEW_BOUND,	&mi);
	m_pPopupMenu->SetMenuItemInfo(ID_NEW_FLAT,	&mi);

	CRect rcClient;
	COwnerDrawnButton* pButton = m_toolbarButtons.GetButtonByCommand(ID_NEW_PRODUCT);
	if (pButton)
		pButton->GetWindowRect(rcClient);

	m_pPopupMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rcClient.TopLeft().x, rcClient.TopLeft().y + rcClient.Height() - 1, this);
	m_pPopupMenu = NULL;

	if (pButton)
		pButton->SetCheck(BST_UNCHECKED);
}

void CPrintSheetFrameLeftToolsView::OnNewBound()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strProduct; strProduct.LoadString(IDS_PRODUCT); 
	CDlgBoundProduct dlgBoundProduct;
	dlgBoundProduct.m_boundProduct.m_strProductName							= pDoc->AssignDefaultProductName();
	dlgBoundProduct.m_boundProduct.m_strFormatName							= theApp.settings.m_szLastPageFormat;
	dlgBoundProduct.m_boundProduct.m_bindingParams.m_bindingDefs.m_strName	= theApp.settings.m_szLastBindingStyle;

	if (dlgBoundProduct.DoModal() == IDCANCEL)
		return;

	if (dlgBoundProduct.m_boundProduct.m_parts.GetSize() <= 0)
		return;

	pDoc->m_boundProducts.AddTail(dlgBoundProduct.m_boundProduct);

	CBoundProduct& rNewBoundProduct = pDoc->m_boundProducts.GetTail();
	int nProductIndex = rNewBoundProduct.GetIndex();

	int nNewProductPartIndex = rNewBoundProduct.m_parts[0].GetIndex();
	pDoc->m_PageTemplateList.ShiftProductPartIndices(nNewProductPartIndex, rNewBoundProduct.m_parts.GetSize());
	pDoc->m_Bookblock.ShiftProductPartIndices(nNewProductPartIndex, rNewBoundProduct.m_parts.GetSize());

	for (int i = 0; i < rNewBoundProduct.m_parts.GetSize(); i++)
		pDoc->m_PageTemplateList.InsertProductPartPages(rNewBoundProduct.m_parts[i].m_nNumPages, nProductIndex, rNewBoundProduct.m_parts[i].GetIndex());

	pDoc->m_PageSourceList.InitializePageTemplateRefs();

	pDoc->SetModifiedFlag();

	pDoc->UpdateAllViews(NULL);
}

void CPrintSheetFrameLeftToolsView::OnNewFlat()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgFlatProductData dlg;
	dlg.m_pFlatProductList	= &pDoc->m_flatProducts;
	dlg.m_nFlatProductIndex = -1;		// -1 means new product
	if (dlg.DoModal() == IDCANCEL)
		return;
}

void CPrintSheetFrameLeftToolsView::OnDeleteProduct()
{
	CProductsView* pView = GetProductsView();
	if (pView)
		pView->SendMessage(WM_COMMAND, ID_DEL_PRODUCT);
}

void CPrintSheetFrameLeftToolsView::OnImportProducts()
{
    CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_IMPORT_PRODUCTS));
	m_pPopupMenu = menu.GetSubMenu(0);
	if ( ! m_pPopupMenu)
		return;

    MENUITEMINFO mi;
    mi.cbSize = sizeof(MENUITEMINFO);
    mi.fMask = MIIM_TYPE;
    mi.fType = MFT_OWNERDRAW;
//     Making the menu item ownerdrawn:
	m_pPopupMenu->SetMenuItemInfo(ID_IMPORT_XML,	&mi);
	m_pPopupMenu->SetMenuItemInfo(ID_IMPORT_CSV,	&mi);
	m_pPopupMenu->SetMenuItemInfo(ID_IMPORT_PDF,	&mi);
	m_pPopupMenu->SetMenuItemInfo(ID_IMPORT_CFF,	&mi);

	CRect rcClient;
	COwnerDrawnButton* pButton = m_toolbarButtons.GetButtonByCommand(ID_IMPORT_PRODUCTS);
	if (pButton)
		pButton->GetWindowRect(rcClient);

	m_pPopupMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rcClient.TopLeft().x, rcClient.TopLeft().y + rcClient.Height() - 1, this);
	m_pPopupMenu = NULL;

	if (pButton)
		pButton->SetCheck(BST_UNCHECKED);
}

void CPrintSheetFrameLeftToolsView::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType == ODT_MENU)
	{
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		CRect rcItem = lpDrawItemStruct->rcItem;

		pDC->FillSolidRect(rcItem, RGB(252, 252, 249));//CREME);

		CRect rcIcon = rcItem;
		rcIcon.right = rcIcon.left + 40;

		if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			CGraphicComponent gc;
			gc.DrawTransparentFrame(pDC, rcItem, RGB(120,160,220), 2);
		}				

		switch (lpDrawItemStruct->itemID)
		{
		case ID_NEW_BOUND:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_BOUND_PRODUCT));	break;
		case ID_NEW_FLAT:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_FLAT_PRODUCT));	break;
		case ID_IMPORT_XML:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_XML));			break;
		case ID_IMPORT_CSV:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_CSV));			break;
		case ID_IMPORT_PDF:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_PDF_LARGE));		break;
		case ID_IMPORT_CFF:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 2, theApp.LoadIcon(IDI_CFF));			break;
		}

		pDC->SelectObject(GetFont());
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(DARKBLUE);
		CRect rcText = rcItem;
		rcText.left = rcIcon.right + 5;
		if (m_pPopupMenu)
		{
			CString strText;
			m_pPopupMenu->GetMenuString(lpDrawItemStruct->itemID, strText, MF_BYCOMMAND);
			pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		}
	}
	else
		CScrollView::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CPrintSheetFrameLeftToolsView::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct->CtlType == ODT_MENU)
	{
		int	nMaxLen = 0;
		if (m_pPopupMenu)
		{
			CString strText;
			m_pPopupMenu->GetMenuString(lpMeasureItemStruct->itemID, strText, MF_BYCOMMAND);
			CClientDC dc(this);
			dc.SelectObject(GetFont());
			nMaxLen = dc.GetTextExtent(strText).cx + 40;
		}
		lpMeasureItemStruct->itemWidth  = nMaxLen;
		lpMeasureItemStruct->itemHeight = 40;
	}
	else
		CScrollView::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CPrintSheetFrameLeftToolsView::OnImportXML()
{
	CString strFilter;
	strFilter.LoadString(IDS_XMLFILE_FILTER);
	CFileDialog dlgFile(TRUE, _T("xml"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING, strFilter, this);

	CString strTitle;
	strTitle.LoadString(IDS_IMPORT_XML);

	CString strInitialFolder	  = theApp.GetRegistryKey(_T("XMLFolder"), CString(theApp.settings.m_szDataDir) + _T("\\XML-Data"));
	dlgFile.m_ofn.lpstrInitialDir = strInitialFolder.GetBuffer(MAX_PATH);
	dlgFile.m_ofn.lpstrTitle = strTitle;

	if (dlgFile.DoModal() == IDCANCEL)
		return;

	strInitialFolder.TrimRight(dlgFile.GetFileName());
	theApp.SetRegistryKey(_T("XMLFolder"), strInitialFolder);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		BeginWaitCursor();
		CXMLInput xmlInput;
		if ( ! xmlInput.ParseXML(dlgFile.GetPathName())) 
		{
			EndWaitCursor();
			return;
		}
		EndWaitCursor();

		pDoc->SetModifiedFlag();
		pDoc->UpdateAllViews(NULL);
	}
}

void CPrintSheetFrameLeftToolsView::OnImportCSV()
{
	CString strFilter;
	strFilter.LoadString(IDS_CSVFILE_FILTER);
	CFileDialog dlgFile(TRUE, _T("csv"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING, strFilter, this);

	CString strTitle;
	strTitle.LoadString(IDS_IMPORT_CSV);

	CString strInitialFolder	  = theApp.GetRegistryKey(_T("CSVFolder"), CString(theApp.settings.m_szDataDir) + _T("\\CSV-Data"));
	dlgFile.m_ofn.lpstrInitialDir = strInitialFolder.GetBuffer(MAX_PATH);
	dlgFile.m_ofn.lpstrTitle = strTitle;

	if (dlgFile.DoModal() == IDCANCEL)
		return;

	strInitialFolder.TrimRight(dlgFile.GetFileName());
	theApp.SetRegistryKey(_T("CSVFolder"), strInitialFolder);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		BeginWaitCursor();
		CCSVInput csvInput(pDoc->GetFlatProductsIndex());
		if ( ! csvInput.DoImport(dlgFile.GetPathName()))
		{
			EndWaitCursor();
			return;
		}
		EndWaitCursor();

		pDoc->SetModifiedFlag();
		pDoc->UpdateAllViews(NULL);
	}
}

void CPrintSheetFrameLeftToolsView::OnImportPDF()
{
	CDlgPDFImport dlg;

	if (dlg.DoModal() == IDCANCEL)
		return;
}

void CPrintSheetFrameLeftToolsView::OnImportCFF()
{
	CString strFilter;
	strFilter.LoadString(IDS_CFFFILE_FILTER);
	CFileDialog dlgFile(TRUE, _T("cf2"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING, strFilter, this);

	CString strTitle;
	strTitle.LoadString(IDS_IMPORT_CFF);

	CString strInitialFolder	  = theApp.GetRegistryKey(_T("CFFFolder"), theApp.GetDefaultInstallationFolder() + _T("\\CFF-Data"));
	dlgFile.m_ofn.lpstrInitialDir = strInitialFolder.GetBuffer(MAX_PATH);
	dlgFile.m_ofn.lpstrTitle = strTitle;

	if (dlgFile.DoModal() == IDCANCEL)
		return;

	strInitialFolder.ReleaseBuffer();
	strInitialFolder = dlgFile.GetPathName();
	strInitialFolder.TrimRight(dlgFile.GetFileName());
	theApp.SetRegistryKey(_T("CFFFolder"), strInitialFolder);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CCFFInput cffInput(pDoc->GetFlatProductsIndex());
	if ( ! cffInput.DoImport(dlgFile.GetPathName()))
		return;

	//m_labelList.RemoveAll();
	int i = 0;
	POSITION pos = m_flatProducts.GetHeadPosition();
	while (pos)
	{ 
		//if (pDoc->m_flatProducts.FindByIndex(i).GetUnassignedProductGroupID() != m_nProductPartIndex)
		//	continue;
		m_flatProducts.AddTail(pDoc->m_flatProducts.FindByIndex(i));
		i++;
	}

	pDoc->SetModifiedFlag();

	pDoc->UpdateAllViews(NULL);

	UpdateData(FALSE);
}


void CPrintSheetFrameLeftToolsView::OnUpdateNewProduct(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetFrameLeftToolsView::OnUpdateNewBound(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetFrameLeftToolsView::OnUpdateNewFlat(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetFrameLeftToolsView::OnUpdateDeleteProduct(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintSheetFrameLeftToolsView::OnUpdateImportProducts(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetFrameLeftToolsView::OnPrevious()
{
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_ORDERDATA);
}

void CPrintSheetFrameLeftToolsView::OnShowPrintSheetList()
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	pParentFrame->m_browserInfo.m_nShowInfo = (pParentFrame->m_browserInfo.m_nShowInfo & CNewPrintSheetFrame::ShowPrintSheetList) ? (pParentFrame->m_browserInfo.m_nShowInfo & ~CNewPrintSheetFrame::ShowPrintSheetList)
																																  : (pParentFrame->m_browserInfo.m_nShowInfo |  CNewPrintSheetFrame::ShowPrintSheetList);
	pParentFrame->SetupViewPrintSheetList();
}

void CPrintSheetFrameLeftToolsView::OnUpdatePrevious(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetFrameLeftToolsView::OnUpdateShowPrintSheetList(CCmdUI* pCmdUI)
{
	CNewPrintSheetFrame* pParentFrame = (CNewPrintSheetFrame*)GetParentFrame();
	COwnerDrawnButtonList::SetCheck(pCmdUI, (pParentFrame->m_browserInfo.m_nShowInfo & CNewPrintSheetFrame::ShowPrintSheetList) ? TRUE : FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CPrintSheetFrameLeftToolsView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if ( ! m_toolbarButtons.GetSize())
		return;

	COwnerDrawnButton* pButton = m_toolbarButtons.GetButtonByCommand(ID_PREVIOUS);
	CRect rcButton;
	pButton->GetWindowRect(rcButton);
	CRect rcClient;
	GetClientRect(rcClient);
	pButton->MoveWindow(rcClient.left, rcClient.CenterPoint().y - rcButton.Height()/2, rcButton.Width(), rcButton.Height());

	pButton = m_toolbarButtons.GetButtonByCommand(ID_SHOW_PRINTSHEETLIST);
	pButton->GetWindowRect(rcButton);
	pButton->MoveWindow(rcClient.left, rcClient.bottom - rcButton.Height() - 10, rcButton.Width(), rcButton.Height());
}

BOOL CPrintSheetFrameLeftToolsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_toolbarButtons.m_pToolTip)            
		m_toolbarButtons.m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

CProductsView* CPrintSheetFrameLeftToolsView::GetProductsView()
{
	CNewPrintSheetFrame* pParentFrame  = (CNewPrintSheetFrame*)GetParentFrame();
	return (pParentFrame) ? pParentFrame->GetProductsView() : NULL;
}