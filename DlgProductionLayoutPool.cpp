// DlgProductionLayoutPool.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionLayoutPool.h"
#include "DlgProductionManager.h"
#include "DlgLayoutPoolSelection.h"



// CDlgProductionLayoutPool-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionLayoutPool, CDialog)

CDlgProductionLayoutPool::CDlgProductionLayoutPool(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionLayoutPool::IDD, pParent)
{
	m_bLocked = FALSE;

	m_smallFont.CreateFont (11, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_pLayoutPoolImageList = new CImageList();
}

CDlgProductionLayoutPool::~CDlgProductionLayoutPool()
{
	m_smallFont.DeleteObject();
	delete m_pLayoutPoolImageList;
}

int CDlgProductionLayoutPool::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		SetParent(m_pParentWnd);
		ModifyStyle(WS_OVERLAPPEDWINDOW | WS_POPUPWINDOW, WS_CHILD);
		ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);
	}

	return 0;
}

BOOL CDlgProductionLayoutPool::Create(UINT nIDTemplate, CWnd* pParentWnd)
{
	m_pParentWnd = pParentWnd;
	return CDialog::Create(nIDTemplate, pParentWnd);
}

void CDlgProductionLayoutPool::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
		{
			if (m_hWnd)
			{
				if ( ! m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
				{
					m_bLocked = TRUE;
					EndModalLoop(IDCANCEL);
					DestroyWindow();
				}
			}
		}
	}
	else
		m_bLocked = FALSE;
}

void CDlgProductionLayoutPool::OnBnClickedOk()
{
	SaveData();
	EndModalLoop(IDOK);
	m_bLocked = TRUE;
	DestroyWindow();
}

CProductionProfile& CDlgProductionLayoutPool::GetProductionProfile()
{
	static CProductionProfile nullProductionProfile;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile;
	}
	//else
	//	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CProfileSummaryWindow)))
	//	{
	//		CProfileSummaryWindow*	pParent	= (CProfileSummaryWindow*)m_pParentWnd;
	//		return pParent->GetProductionProfile();
	//	}

	return nullProductionProfile;
}

CBoundProductPart& CDlgProductionLayoutPool::GetBoundProductPart()
{
	static CBoundProductPart nullProductPart;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent				= (CDlgProductionManager*)m_pParentWnd;
		CProductionProfile&		rProductionProfile	= pParent->m_productionProfile;
		CPrintGroup*			pPrintGroup			= pParent->m_pPrintGroup;
		int						nComponentIndex		= pParent->m_nComponentIndex;
		CPrintGroupComponent*	pComponent			= (pPrintGroup) ? ( (nComponentIndex == -1) ? pPrintGroup->GetFirstBoundComponent() : &pPrintGroup->m_componentList[nComponentIndex]) : NULL;
		int						nProductPartIndex	= (pComponent) ? pComponent->m_nProductClassIndex : -1;
		return (CImpManDoc::GetDoc()) ? CImpManDoc::GetDoc()->GetBoundProductPart(nProductPartIndex) : nullProductPart;
	}

	return nullProductPart;
}

void CDlgProductionLayoutPool::OnNMCustomdrawLayoutPoollist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);
			CPrintSheet* pPrintSheet = (lvitem.iItem < m_sampleList.GetSize()) ? m_sampleList[lvitem.iItem].pPrintSheet : NULL;
			CLayout*	 pLayout	 = (lvitem.iItem < m_sampleList.GetSize()) ? m_sampleList[lvitem.iItem].pLayout		: NULL;
			if ( ! pLayout || ! pPrintSheet)
				break;

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			float fPaperWidth  = (pLayout) ? pLayout->m_FrontSide.m_fPaperWidth  : 0.0f;
			float fPaperHeight = (pLayout) ? pLayout->m_FrontSide.m_fPaperHeight : 0.0f;

			CProductionProfile& rProductionProfile	= GetProductionProfile();
			CFoldingProcess&	rFoldingProcess		= rProductionProfile.m_postpress.m_folding;
			CFoldSheet  foldSheet = (rFoldingProcess.m_foldSchemeList.GetSize()) ? rFoldingProcess.m_foldSchemeList.GetHead() : CFoldSheet();

			rcItem.DeflateRect(5, 5);
			pLayout->Draw(pDC, rcItem, pPrintSheet, NULL, TRUE, FALSE, FALSE, &foldSheet, TRUE);		

			CRect rcLabel;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			if (rcLabel.IsRectEmpty())
				break;

			COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

			CFont* pOldFont = pDC->SelectObject(&m_smallFont);
			CRect rcText = rcLabel; rcText.left += 5; rcText.top = rcText.CenterPoint().y - 20; rcText.bottom = rcText.top + 50;
			//pDC->DrawText(pLayout->m_strLayoutName, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);
			pDC->DrawText(pLayout->m_strLayoutName, -1, rcText, DT_VCENTER | DT_LEFT | DT_END_ELLIPSIS);
			rcText.OffsetRect(0, 15);

			pDC->SetTextColor(DARKGRAY);
			CString string;
			//string.Format(_T("%.1f x %.1f"), pPressDevice->m_fPlateWidth, pPressDevice->m_fPlateHeight);
			//pDC->DrawText(string, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);

			pDC->SetTextColor(crOldTextColor);
			pDC->SelectObject(pOldFont);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}


void CDlgProductionLayoutPool::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LAYOUTPOOL_LIST, m_layoutPoolList);
}


BEGIN_MESSAGE_MAP(CDlgProductionLayoutPool, CDialog)
	ON_WM_CREATE()
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LAYOUTPOOL_LIST, OnNMCustomdrawLayoutPoollist)
	ON_BN_CLICKED(IDC_LAYOUTPOOL_ADD, OnBnClickedLayoutpoolAdd)
	ON_BN_CLICKED(IDC_LAYOUTPOOL_DELETE, OnBnClickedLayoutpoolDelete)
END_MESSAGE_MAP()


// CDlgProductionLayoutPool-Meldungshandler


BOOL CDlgProductionLayoutPool::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);

	if ( ! m_pLayoutPoolImageList->m_hImageList)
	{
		CBitmap bitmap;
		m_pLayoutPoolImageList->Create(96, 96, ILC_COLOR8, 1, 0);	// List not to grow
		bitmap.LoadBitmap(IDB_BKSCHEMELIST);
		m_pLayoutPoolImageList->Add(&bitmap, BLACK);
		m_layoutPoolList.SetImageList(m_pLayoutPoolImageList, LVSIL_SMALL);
		bitmap.DeleteObject();
	}

	InitLayoutPoolList();

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}


void CDlgProductionLayoutPool::InitData()
{
	CProductionProfileLayoutPool& rProductionProfileLayoutPool	= GetProductionProfile().m_layoutPool;

	for (int i = 0; i < m_sampleList.GetSize(); i++)
	{
		if (m_sampleList[i].pPrintSheet)
			delete m_sampleList[i].pPrintSheet;
		if (m_sampleList[i].pLayout)
			delete m_sampleList[i].pLayout;
	}
	m_sampleList.RemoveAll();
	m_layoutPoolList.DeleteAllItems();
	
	CPrintSheet printSheet;
	printSheet.Create(-1, -1);

	LV_ITEM item;
	item.mask		= LVIF_PARAM | LVIF_IMAGE | LVIF_STATE;
	item.stateMask	= LVIS_SELECTED;
	item.state		= 0;
	item.cchTextMax = MAX_TEXT;
	item.iItem		= 0; 
	item.iSubItem	= 0;
	item.lParam		= 0;//(LPARAM)&pl;
	item.iImage		= 0;
	//m_layoutPoolList.InsertItem(&item);

	for (int i = 0; i < rProductionProfileLayoutPool.GetSize(); i++)
	{
		CProductionProfileLayoutTemplate& rLayoutTemplate = rProductionProfileLayoutPool[i];

		LP_PRINTLAYOUT pl1r  = {new (CPrintSheet), new (CLayout)};
		*pl1r.pPrintSheet = rLayoutTemplate.m_printSheet;
		*pl1r.pLayout	  = rLayoutTemplate.m_layout;
		m_sampleList.Add(pl1r);
		item.iItem = m_layoutPoolList.GetItemCount(); 
		m_layoutPoolList.InsertItem(&item);
	}

	UpdateData(FALSE);
}

void CDlgProductionLayoutPool::SaveData()
{
	UpdateData(TRUE);

	CProductionProfileLayoutPool& rProductionProfileLayoutPool	= GetProductionProfile().m_layoutPool;
}

void CDlgProductionLayoutPool::InitLayoutPoolList()
{
}

void CDlgProductionLayoutPool::OnBnClickedLayoutpoolAdd()
{
	CDlgLayoutPoolSelection dlg;

	dlg.m_productionProfile = GetProductionProfile();
	dlg.m_productPart		= GetBoundProductPart();
	if (dlg.m_productPart.IsNull())
	{
		dlg.m_productPart.m_fPageWidth  = 210.0f;
		dlg.m_productPart.m_fPageHeight = 297.0f;
	}

	if (dlg.DoModal() == IDCANCEL)
		return;

	CProductionProfileLayoutPool& rProductionProfileLayoutPool	= GetProductionProfile().m_layoutPool;
	for (int i = 0; i < dlg.m_productionProfile.m_layoutPool.GetSize(); i++)
		rProductionProfileLayoutPool.Add(dlg.m_productionProfile.m_layoutPool[i]);

	InitData();

	m_layoutPoolList.Invalidate();
	m_layoutPoolList.UpdateWindow();

	CDlgProductionManager* pParent = (CDlgProductionManager*)m_pParentWnd;
	pParent->m_productionStepButtons.Redraw();
}

void CDlgProductionLayoutPool::OnBnClickedLayoutpoolDelete()
{
	CProductionProfileLayoutPool& rProductionProfileLayoutPool	= GetProductionProfile().m_layoutPool;

	int		 nNumDeleted = 0;
	POSITION pos	     = m_layoutPoolList.GetFirstSelectedItemPosition();
	while (pos)
	{
		int nSel = m_layoutPoolList.GetNextSelectedItem(pos);
		if ( ((nSel - nNumDeleted) >= 0) && ((nSel - nNumDeleted) < rProductionProfileLayoutPool.GetSize()) )
		{
			rProductionProfileLayoutPool.RemoveAt(nSel - nNumDeleted);
			nNumDeleted++;
		}
	}

	InitData();

	m_layoutPoolList.Invalidate();
	m_layoutPoolList.UpdateWindow();

	CDlgProductionManager* pParent = (CDlgProductionManager*)m_pParentWnd;
	pParent->m_productionStepButtons.Redraw();
}
