// DlgSelectSheetPopup.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgSelectSheetPopup.h"


// CDlgSelectSheetPopup-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgSelectSheetPopup, CDialog)

CDlgSelectSheetPopup::CDlgSelectSheetPopup(CWnd* pParent /*=NULL*/, BOOL bRunModal /* = FALSE*/)
	: CDialog(CDlgSelectSheetPopup::IDD, pParent)
{
	m_smallFont.CreateFont (12, 0, 0, 0, FW_NORMAL,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_pSheetImageList  = new CImageList();
	//m_pParent = (CDlgProductionPlannerBound*)pParent;
	m_pPrintSheet = NULL;
	m_pLayout = NULL;
	m_pPressDevice = NULL;
	m_pFnSelChangeHandler = NULL;
	m_bLocked = FALSE;
	m_bRunModal	= bRunModal;
}

CDlgSelectSheetPopup::~CDlgSelectSheetPopup()
{
	m_smallFont.DeleteObject();
	delete m_pSheetImageList;
}

void CDlgSelectSheetPopup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SS_SHEETLIST, m_sheetList);
}


BEGIN_MESSAGE_MAP(CDlgSelectSheetPopup, CDialog)
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDC_SS_DEFINE_SHEET_BUTTON, &CDlgSelectSheetPopup::OnBnClickedSsDefineSheetButton)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SS_SHEETLIST, &CDlgSelectSheetPopup::OnNMCustomdrawSsSheetlist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SS_SHEETLIST, &CDlgSelectSheetPopup::OnLvnItemchangedSsSheetlist)
	ON_WM_ERASEBKGND()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CDlgSelectSheetPopup-Meldungshandler

BOOL CDlgSelectSheetPopup::OnEraseBkgnd(CDC* pDC)
{
	CRect rcFrame;
	GetClientRect(rcFrame);

	COLORREF crStartColor = RGB(243,245,247);
	COLORREF crEndColor = RGB(236,233,212);
	Graphics graphics(pDC->m_hDC);
	Rect rc(rcFrame.left, rcFrame.top, rcFrame.Width(), rcFrame.Height());
	LinearGradientBrush br(rc, Color::Color(GetRValue(crStartColor), GetGValue(crStartColor), GetBValue(crStartColor)), 
							   Color::Color(GetRValue(crEndColor),	 GetGValue(crEndColor),	  GetBValue(crEndColor)), 
							   0);

	graphics.FillRectangle(&br, rc);

	return TRUE;
	//return CDialog::OnEraseBkgnd(pDC);
}

BOOL CDlgSelectSheetPopup::OnInitDialog()
{
	CDialog::OnInitDialog();

	if ( ! m_pSheetImageList->m_hImageList)
	{
		CBitmap bitmap;
		m_pSheetImageList->Create(48, 48, ILC_COLOR8, 1, 0);	// List not to grow
		bitmap.LoadBitmap(IDB_BKSCHEMELIST);
		m_pSheetImageList->Add(&bitmap, BLACK);
		m_sheetList.SetImageList(m_pSheetImageList, LVSIL_SMALL);
		bitmap.DeleteObject();
	}
	InitSheetList();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgSelectSheetPopup::InitSheetList()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_bLocked = TRUE;

	m_dlgSheet.m_sheetList.RemoveAll();
	m_dlgSheet.LoadSheets();

	m_sheetList.DeleteAllItems();

	int nSel = -1;
	POSITION pos = m_dlgSheet.m_sheetList.GetHeadPosition();
	while (pos) 
	{
		CSheet& rSheet = m_dlgSheet.m_sheetList.GetNext(pos);

		LV_ITEM item;
		item.mask		= LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE | LVIF_STATE;
		item.stateMask	= LVIS_SELECTED;
		item.state		= 0;
		item.cchTextMax = MAX_TEXT;
		item.pszText	= rSheet.m_strSheetName.GetBuffer(MAX_TEXT);
		item.iItem		= m_sheetList.GetItemCount(); 
		item.iSubItem	= 0;
		item.lParam		= (LPARAM)&rSheet;
		item.iImage		= 0;
		if (m_pLayout)
			if (m_pLayout->m_FrontSide.m_strFormatName == rSheet.m_strSheetName)
			{
				item.state = LVIS_SELECTED;
				nSel = item.iItem;
			}
		m_sheetList.InsertItem(&item);
	}

	m_sheetList.EnsureVisible(nSel, FALSE);

	m_bLocked = FALSE;
}

void CDlgSelectSheetPopup::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
			if (m_hWnd)
				if ( ! m_bRunModal)
				{
					m_bLocked = TRUE;
					DestroyWindow();
				}
				else
					OnCancel();
	}
	else
		m_bLocked = FALSE;
}

void CDlgSelectSheetPopup::OnNMCustomdrawSsSheetlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
    LPNMLVCUSTOMDRAW pNMLVCD = (LPNMLVCUSTOMDRAW) pNMHDR;

    HWND hWndListCtrl = pNMHDR->hwndFrom;

	switch(pNMLVCD->nmcd.dwDrawStage) 
	{
	case CDDS_ITEMPOSTPAINT:
		{
			CDC* pDC = CDC::FromHandle(pNMCD->hdc);
			CRect rcItem;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcItem, LVIR_ICON);
			if (rcItem.IsRectEmpty())
				break;
			pDC->FillSolidRect(rcItem, ListView_GetBkColor(hWndListCtrl));
			LVITEM lvitem;
			lvitem.mask	= LVIF_PARAM | LVIF_STATE;
			lvitem.stateMask = LVIS_SELECTED;
			lvitem.iItem = pNMCD->dwItemSpec;
			lvitem.iSubItem = 0;
			ListView_GetItem(hWndListCtrl, &lvitem);
			CSheet* pSheet = (CSheet*)lvitem.lParam;
			if ( ! pSheet)
				break;

			CPen	pen(PS_SOLID, 1, (lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(120,140,180));
			CBrush	brush((lvitem.state == 0) ? ::GetSysColor(COLOR_WINDOW) : RGB(193,211,251));
			CPen*	pOldPen	  = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			CRect rcAll;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcAll, LVIR_SELECTBOUNDS);
			pDC->Rectangle(rcAll);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);

			if (m_pPressDevice)
				m_pPressDevice->DrawPlate(pDC, rcItem, pSheet->m_fWidth, pSheet->m_fHeight);			

			CRect rcLabel;
			ListView_GetItemRect(hWndListCtrl, pNMCD->dwItemSpec, &rcLabel, LVIR_LABEL);
			if (rcLabel.IsRectEmpty())
				break;

			COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);

			CRect rcText = rcLabel; rcText.left += 5; rcText.top = rcText.CenterPoint().y - 20; rcText.bottom = rcText.top + 15;
			pDC->DrawText(pSheet->m_strSheetName, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT);// | DT_WORD_ELLIPSIS);
			rcText.OffsetRect(0, 15);

			CFont* pOldFont = pDC->SelectObject(&m_smallFont);
			pDC->SetTextColor(DARKGRAY);
			CString string;
			string.Format(_T("%.1f x %.1f"), pSheet->m_fWidth, pSheet->m_fHeight);
			pDC->DrawText(string, -1, rcText, DT_SINGLELINE | DT_VCENTER | DT_LEFT | DT_WORD_ELLIPSIS);

			pDC->SetTextColor(crOldTextColor);
			pDC->SelectObject(pOldFont);
		}
		break;

	case CDDS_SUBITEM|CDDS_ITEMPOSTPAINT:
		break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}

void CDlgSelectSheetPopup::OnLvnItemchangedSsSheetlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	if ( ! m_pPrintSheet || ! m_pLayout || ! m_pPressDevice)
		return;

	if (m_bLocked)
		return;

    HWND hWndListCtrl = pNMHDR->hwndFrom;
	if (pNMLV->iItem < 0)
		return;
	if (pNMLV->uNewState == pNMLV->uOldState)
		return;
	if ( ! (pNMLV->uNewState & LVIS_SELECTED) )
		return;

	LVITEM lvitem;
	lvitem.mask	= LVIF_PARAM;
	lvitem.iItem = pNMLV->iItem;
	lvitem.iSubItem = 0;
	ListView_GetItem(hWndListCtrl, &lvitem);
	CSheet* pSheet = (CSheet*)lvitem.lParam;
	if ( ! pSheet)
		return;

	if (m_pFnSelChangeHandler)
		m_pFnSelChangeHandler(pSheet, this);

	m_bLocked = TRUE;
	if (m_hWnd)
		if (m_bRunModal)
		{
			int nIndex = m_dlgSheet.m_sheetList.FindIndexByName(pSheet->m_strSheetName);
			if (nIndex == -1)
				EndDialog(IDCANCEL);
			else
				EndDialog(-nIndex);
		}
		else
			DestroyWindow();
}

void CDlgSelectSheetPopup::OnBnClickedSsDefineSheetButton()
{
	m_bLocked = TRUE;

	CDlgSheet dlg(FALSE, m_pPrintSheet);  // don't modify direct inside dialog
	if (dlg.DoModal() == IDOK)
	{
		m_pLayout->m_FrontSide.m_fPaperWidth		= dlg.m_fWidth;
		m_pLayout->m_FrontSide.m_fPaperHeight		= dlg.m_fHeight;
		m_pLayout->m_FrontSide.m_nPaperOrientation	= (unsigned char)( (dlg.m_fWidth > dlg.m_fHeight) ? LANDSCAPE : PORTRAIT);
		m_pLayout->m_FrontSide.m_strFormatName		= dlg.m_strName;
		m_pLayout->m_FrontSide.m_bAdjustFanout		= (dlg.m_FanoutList.GetSize()) ? TRUE : FALSE;
		m_pLayout->m_FrontSide.m_FanoutList			= dlg.m_FanoutList;
		m_pLayout->m_FrontSide.PositionLayoutSide(FLT_MAX, m_pPressDevice);

		m_pLayout->m_BackSide.m_fPaperWidth			= dlg.m_fWidth;
		m_pLayout->m_BackSide.m_fPaperHeight		= dlg.m_fHeight;
		m_pLayout->m_BackSide.m_nPaperOrientation	= m_pLayout->m_FrontSide.m_nPaperOrientation;
		m_pLayout->m_BackSide.m_strFormatName		= dlg.m_strName;
		m_pLayout->m_BackSide.m_bAdjustFanout		= m_pLayout->m_FrontSide.m_bAdjustFanout;
		m_pLayout->m_BackSide.m_FanoutList			= dlg.m_FanoutList;
		m_pLayout->m_BackSide.PositionLayoutSide(FLT_MAX, m_pPressDevice);

		//if (pDlg->m_pFoldSheet)
		//	pDlg->m_pPrintSheet->ReImpose(pDlg->m_pFoldSheet, pDlg->m_pFoldSheet);

//		m_pParent->UpdateProductComponentsList();
	}

	m_bLocked = FALSE;

	InitSheetList();
}

void CDlgSelectSheetPopup::OnDestroy()
{
	CDialog::OnDestroy();

	m_pSheetImageList->DeleteImageList();
}
