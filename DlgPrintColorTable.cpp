// DlgPrintColorTable.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgPrintColorTable.h"
#include "DlgPrintColorProps.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// CPrintingColorList - custom CListCtrl - containing printing color definitions

CPrintingColorList::CPrintingColorList()
{
}

void CPrintingColorList::Initialize(CWnd* pParent, BOOL bShowBasicColorsOnly)
{
	if ( ! m_hWnd)
	{
		VERIFY(SubclassDlgItem(IDC_PRINTING_COLORS,	pParent));

		CString string;
		string.LoadString(IDS_COLOR);
		InsertColumn(0, string, LVCFMT_LEFT, 180, 0);
		string.LoadString(IDS_COLOR_TYPE);
		InsertColumn(1, string,   LVCFMT_LEFT, 180, 0);

		m_imageList.Create(16, 16, TRUE, 4, 4);
		m_imageList.Add(theApp.LoadIcon(IDI_LINK));
		SetImageList(&m_imageList, LVSIL_SMALL);
	}

	DeleteAllItems();
	m_colorListItems.RemoveAll();

	CArray <CColorDefinition*, CColorDefinition*> sortList;
	for (int i = 0; i < m_colorDefTable.GetSize(); i++)
		sortList.Add(&m_colorDefTable[i]);
	qsort((void*)sortList.GetData(), sortList.GetSize(), sizeof(CColorDefinition*), CompareColorDefs);

	CColorDefinitionTable tempColorDefTable;
	for (int i = 0; i < sortList.GetSize(); i++)
		tempColorDefTable.Add(*sortList[i]);

	m_colorDefTable.RemoveAll();
	for (int i = 0; i < tempColorDefTable.GetSize(); i++)
		m_colorDefTable.Add(tempColorDefTable[i]);

	for (int i = 0; i < sortList.GetSize(); i++)
		AddColorItem(m_colorDefTable[i].m_rgb, m_colorDefTable[i].m_strColorName, m_colorDefTable[i].m_nColorType, i, -1, 
					 m_colorDefTable[i].m_nColorIndex); 

	if (bShowBasicColorsOnly)
		return;

	for (int i = 0; i < m_colorDefTable.GetSize(); i++)
	{
		int j = 0;
		POSITION pos = m_colorDefTable[i].m_AliasNameList.GetHeadPosition();
		while (pos)
		{
			CString strAlias = m_colorDefTable[i].m_AliasNameList.GetNext(pos);
			AddColorItem(m_colorDefTable[i].m_rgb, strAlias, CColorDefinition::Alternate, i, j, m_colorDefTable[i].m_nColorIndex); 
			j++;
		}
	}
}

int CPrintingColorList::CompareColorDefs(const void *p1, const void *p2)
{
	CColorDefinition* pColorDefinition1 = *(CColorDefinition**)p1;
	CColorDefinition* pColorDefinition2 = *(CColorDefinition**)p2;

	if (pColorDefinition1->IsProcessColor() && ! pColorDefinition2->IsProcessColor() )
		return -1;
	else
		if ( ! pColorDefinition1->IsProcessColor() && pColorDefinition2->IsProcessColor() )
			return 1;
		else
			if ( (pColorDefinition1->m_nColorType == CColorDefinition::Spot) && (pColorDefinition1->m_nColorType == CColorDefinition::Alternate) )
				return -1;
			else
				if ( (pColorDefinition1->m_nColorType == CColorDefinition::Alternate) && (pColorDefinition1->m_nColorType == CColorDefinition::Spot) )
					return 1;
				else
					return pColorDefinition1->m_strColorName.CompareNoCase(pColorDefinition2->m_strColorName);

	return 0;
}

void CPrintingColorList::AddColorItem(COLORREF color, CString& strName, int nType, int nBasicColorIndex, int nAlternateColorIndex, int nIPUIndex)
{
	CPrintingColorListItem clItem(color, strName, nType, nBasicColorIndex, nAlternateColorIndex, nIPUIndex);
	m_colorListItems.Add(clItem);

	LVITEM lvitem;
	lvitem.iItem	= GetItemCount();
	lvitem.iSubItem = 0;
	lvitem.mask	    = LVIF_TEXT | LVIF_PARAM;
	lvitem.pszText  = NULL;		// text will be taken from m_colorListItems by DrawItem()
	lvitem.lParam	= (LPARAM)&m_colorListItems[m_colorListItems.GetSize() - 1];
	InsertItem(&lvitem); 

	lvitem.iSubItem = 1;
	lvitem.pszText  = NULL;
	SetItem(&lvitem);
}

void CPrintingColorList::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC	= CDC::FromHandle(lpDIS->hDC);
	int nIndex	= (int)lpDIS->itemID; // Index in item data
	if ((nIndex < 0) || (nIndex >= m_colorListItems.GetSize()))
		return;

	/////////// 1st column
	int nOff			 = 4;
	int nColorRectWidth  = 11;
	int nColorRectHeight = 11;
	int nPosLeft		 = 0;
	int nPosRight		 = GetColumnWidth(0);

	COLORREF cr = m_colorListItems[nIndex].m_crRGB;
	if ( (lpDIS->itemAction & ODA_DRAWENTIRE))
	{
		CRect rect(lpDIS->rcItem.left, lpDIS->rcItem.top, lpDIS->rcItem.left + nColorRectWidth, lpDIS->rcItem.top + nColorRectHeight);
		rect.OffsetRect(0, 1);
		// Paint the color item in the color requested
		CBrush colorBrush(cr);
		CPen   framePen(PS_SOLID, 1, DARKGRAY);
		CBrush* pOldBrush = pDC->SelectObject(&colorBrush);
		CPen*	pOldPen	  = pDC->SelectObject(&framePen);
		pDC->Rectangle(rect);
		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		colorBrush.DeleteObject();
		framePen.DeleteObject();
	}

	pDC->SetBkMode(TRANSPARENT);
	if ((lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// item has been selected - hilite 
		CRect rect = lpDIS->rcItem;
		rect.left  = nPosLeft + nColorRectWidth + nOff;
		rect.right = nPosRight;
		CBrush fillBrush(::GetSysColor(COLOR_HIGHLIGHT));
		pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
		pDC->FillRect(rect, &fillBrush);
		pDC->DrawText(m_colorListItems[nIndex].m_strColorName, rect, DT_LEFT | DT_VCENTER);
		fillBrush.DeleteObject();
	}

	if (!(lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// Item has been de-selected - remove hilite
		CRect rect = lpDIS->rcItem;
		rect.left  = nPosLeft + nColorRectWidth + nOff;
		rect.right = nPosRight;
		CBrush fillBrush(pDC->GetBkColor());
		pDC->SetTextColor(::GetSysColor(COLOR_MENUTEXT));
		pDC->FillRect(rect, &fillBrush);
		pDC->DrawText(m_colorListItems[nIndex].m_strColorName, rect, DT_LEFT | DT_VCENTER);
		fillBrush.DeleteObject();
	}						

	nPosLeft  = nPosRight;
	nPosRight+= GetColumnWidth(1);

	CString string;
	/////////// 2nd column
	if ( (lpDIS->itemAction & ODA_DRAWENTIRE))
	{
		CRect rect = lpDIS->rcItem;
		rect.left  = nPosLeft + nOff;
		rect.right = nPosRight;
		pDC->SetTextColor(BLACK);
		switch (m_colorListItems[nIndex].m_nColorType)
		{
		case CColorDefinition::ProcessC:	pDC->DrawText("Process C", rect, DT_LEFT | DT_VCENTER); break;
		case CColorDefinition::ProcessM:	pDC->DrawText("Process M", rect, DT_LEFT | DT_VCENTER); break;
		case CColorDefinition::ProcessY:	pDC->DrawText("Process Y", rect, DT_LEFT | DT_VCENTER); break;
		case CColorDefinition::ProcessK:	pDC->DrawText("Process K", rect, DT_LEFT | DT_VCENTER); break;
		case CColorDefinition::Spot: 		pDC->DrawText("Spot", rect, DT_LEFT | DT_VCENTER); 		break;
		case CColorDefinition::Alternate:	pDC->DrawIcon(rect.left + 1, rect.top + 1, theApp.LoadIcon(IDI_PRINTCOLOR_LINK));
											rect.left += 15;
											pDC->DrawText(m_colorDefTable[m_colorListItems[nIndex].m_nBasicColorIndex].m_strColorName, 
											rect, DT_LEFT | DT_VCENTER); 
											break;
		}		
	}

	nPosLeft  = nPosRight;
	nPosRight+= GetColumnWidth(1);

#ifdef CTP
	/////////// 3rd column
	if ( (lpDIS->itemAction & ODA_DRAWENTIRE))
	{
		CRect rect = lpDIS->rcItem;
		rect.left  = nPosLeft + nOff;
		rect.right = nPosRight;
		pDC->SetTextColor(BLACK);
		string.Format(_T("%d"), m_colorListItems[nIndex].m_nIPUIndex);
		pDC->DrawText(string, rect, DT_LEFT | DT_VCENTER);
	}
#endif
}




/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPrintColorTable 


CDlgPrintColorTable::CDlgPrintColorTable(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPrintColorTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPrintColorTable)
	m_bShowBasicColorsOnly = FALSE;
	//}}AFX_DATA_INIT
}


void CDlgPrintColorTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPrintColorTable)
	DDX_Check(pDX, IDC_SHOW_BASICCOLORS_ONLY, m_bShowBasicColorsOnly);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPrintColorTable, CDialog)
	//{{AFX_MSG_MAP(CDlgPrintColorTable)
	ON_BN_CLICKED(IDC_PRINTCOLOR_PROPS, OnPrintcolorProps)
	ON_NOTIFY(NM_DBLCLK, IDC_PRINTING_COLORS, OnDblclkPrintingColors)
	ON_BN_CLICKED(IDC_SHOW_BASICCOLORS_ONLY, OnShowBasiccolorsOnly)
	ON_BN_CLICKED(IDC_ADD_PRINTCOLOR, OnAddPrintcolor)
	ON_BN_CLICKED(IDC_REMOVE_PRINTCOLOR, OnRemovePrintcolor)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPrintColorTable 

BOOL CDlgPrintColorTable::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_printingColorList.m_colorDefTable.Append(theApp.m_colorDefTable);

	m_printingColorList.Initialize(this, m_bShowBasicColorsOnly);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgPrintColorTable::AddUndefinedJobPrintColor(CColorDefinition* pColorDef)
{
	CDlgPrintColorProps dlg;
	dlg.m_colorDefTable.Append(m_printingColorList.m_colorDefTable);
	dlg.m_strPrintColorName	 = pColorDef->m_strColorName;
	dlg.m_rgbColor			 = pColorDef->m_rgb; 
	dlg.m_nIPUIndex			 = 0;
	dlg.m_nProcessCMYK		 = -1;
	dlg.m_nPrintColorType	 = pColorDef->m_nColorType;	

	if (dlg.DoModal() == IDCANCEL)
		return;

	switch (dlg.m_nPrintColorType)
	{
	case 0:	
	case 1: {
				CColorDefinition colorDef(dlg.m_strPrintColorName, dlg.m_rgbColor, CMYK(dlg.m_nCyan, dlg.m_nMagenta, dlg.m_nYellow, dlg.m_nKey), (dlg.m_nPrintColorType == 0) ? dlg.m_nProcessCMYK : CColorDefinition::Spot);
				m_printingColorList.m_colorDefTable.InsertColorDef(colorDef);
				m_printingColorList.Initialize(this, m_bShowBasicColorsOnly);
			}
			break;

	case 2: {
				int nBasicColorIndex = m_printingColorList.m_colorDefTable.FindColorDefinitionIndex(dlg.m_strColorMappingTarget);
				if (nBasicColorIndex >= 0)
					m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.AddTail(dlg.m_strPrintColorName);
				m_printingColorList.Initialize(this, m_bShowBasicColorsOnly);
			}
			break;
	}
}

void CDlgPrintColorTable::OnAddPrintcolor() 
{
	CString strNewColor, strColorName;
	strNewColor.LoadString(IDS_NEW_PRINTCOLOR_NAME);
	strColorName = strNewColor;
	int i = 1;
	while  ( ! m_printingColorList.m_colorDefTable.FindColorReferenceName(strColorName).IsEmpty() && (i < 100))
	{
		strColorName.Format(_T("%s%d"), strNewColor, i++);
	}

	CDlgPrintColorProps dlg;
	dlg.m_colorDefTable.Append(m_printingColorList.m_colorDefTable);
	dlg.m_strPrintColorName	 = strColorName;
	dlg.m_rgbColor			 = WHITE; 
	dlg.m_nIPUIndex			 = 0;
	dlg.m_nProcessCMYK		 = -1;
	dlg.m_nPrintColorType	 = 1;	

	if (dlg.DoModal() == IDCANCEL)
		return;

	switch (dlg.m_nPrintColorType)
	{
	case 0:	
	case 1: {
				CColorDefinition colorDef(dlg.m_strPrintColorName, dlg.m_rgbColor, CMYK(dlg.m_nCyan, dlg.m_nMagenta, dlg.m_nYellow, dlg.m_nKey), 
										 (dlg.m_nPrintColorType == 0) ? dlg.m_nProcessCMYK : CColorDefinition::Spot);
				m_printingColorList.m_colorDefTable.InsertColorDef(colorDef);
				m_printingColorList.Initialize(this, m_bShowBasicColorsOnly);
			}
			break;

	case 2: {
				int nBasicColorIndex = m_printingColorList.m_colorDefTable.FindColorDefinitionIndex(dlg.m_strColorMappingTarget);
				if (nBasicColorIndex >= 0)
					m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.AddTail(dlg.m_strPrintColorName);
				m_printingColorList.Initialize(this, m_bShowBasicColorsOnly);
			}
			break;
	}
}

void CDlgPrintColorTable::OnRemovePrintcolor() 
{
	POSITION pos = m_printingColorList.GetFirstSelectedItemPosition();
	if (pos)
	{
		int nItem = m_printingColorList.GetNextSelectedItem(pos);
		if (nItem >= 0)
		{
			CString strMsg;
			AfxFormatString1(strMsg, IDS_REMOVE_PRINTCOLOR_WARNING, m_printingColorList.m_colorListItems[nItem].m_strColorName);
			if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
				return;
			int nBasicColorIndex	 = m_printingColorList.m_colorListItems[nItem].m_nBasicColorIndex;
			int nAlternateColorIndex = m_printingColorList.m_colorListItems[nItem].m_nAlternateColorIndex;
			if (nAlternateColorIndex >= 0)
			{
				pos = m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.FindIndex(nAlternateColorIndex);
				if (pos)
					m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.RemoveAt(pos);
			}
			else
			{
				POSITION pos = m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.GetHeadPosition();
				while (pos)
				{
					CString& rstrColorName = m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.GetNext(pos);
					CColorDefinition colorDef(rstrColorName, m_printingColorList.m_colorDefTable[nBasicColorIndex].m_rgb, 
											  CColorDefinition::Spot, m_printingColorList.m_colorDefTable[nBasicColorIndex].m_nColorIndex);
					m_printingColorList.m_colorDefTable.InsertColorDef(colorDef);
				}
				m_printingColorList.m_colorDefTable.RemoveAt(nBasicColorIndex);
			}
			m_printingColorList.Initialize(this, m_bShowBasicColorsOnly);
		}
	}
}

void CDlgPrintColorTable::OnPrintcolorProps() 
{
	POSITION pos = m_printingColorList.GetFirstSelectedItemPosition();
	if ( ! pos)
		return;
	int nItem = m_printingColorList.GetNextSelectedItem(pos);
	if (nItem < 0)
		return;

	CDlgPrintColorProps dlg;
	dlg.m_colorDefTable.Append(m_printingColorList.m_colorDefTable);
	int nBasicColorIndex	 = m_printingColorList.m_colorListItems[nItem].m_nBasicColorIndex;
	int nAlternateColorIndex = m_printingColorList.m_colorListItems[nItem].m_nAlternateColorIndex;
	dlg.m_strPrintColorName	 = m_printingColorList.m_colorListItems[nItem].m_strColorName;
	dlg.m_rgbColor			 = m_printingColorList.m_colorListItems[nItem].m_crRGB; 
	dlg.m_nIPUIndex			 = m_printingColorList.m_colorListItems[nItem].m_nIPUIndex;
	dlg.m_nProcessCMYK		 = -1;

	if (nBasicColorIndex < m_printingColorList.m_colorDefTable.GetSize())
	{
		dlg.m_nCyan		= GetCValue(m_printingColorList.m_colorDefTable[nBasicColorIndex].m_cmyk);
		dlg.m_nMagenta	= GetMValue(m_printingColorList.m_colorDefTable[nBasicColorIndex].m_cmyk);
		dlg.m_nYellow	= GetYValue(m_printingColorList.m_colorDefTable[nBasicColorIndex].m_cmyk);
		dlg.m_nKey		= GetKValue(m_printingColorList.m_colorDefTable[nBasicColorIndex].m_cmyk);
	}

	switch (m_printingColorList.m_colorListItems[nItem].m_nColorType)
	{
	case CColorDefinition::ProcessC: dlg.m_nProcessCMYK = 0; dlg.m_nPrintColorType = 0; break;
	case CColorDefinition::ProcessM: dlg.m_nProcessCMYK = 1; dlg.m_nPrintColorType = 0; break;
	case CColorDefinition::ProcessY: dlg.m_nProcessCMYK = 2; dlg.m_nPrintColorType = 0; break;
	case CColorDefinition::ProcessK: dlg.m_nProcessCMYK = 3; dlg.m_nPrintColorType = 0; break;	

	case CColorDefinition::Spot: 		dlg.m_nPrintColorType = 1;	break;

	case CColorDefinition::Alternate:	
		{
			dlg.m_nPrintColorType = 2;
			if (nBasicColorIndex < m_printingColorList.m_colorDefTable.GetSize())
			{
				dlg.m_strColorMappingTarget = m_printingColorList.m_colorDefTable[nBasicColorIndex].m_strColorName;
				dlg.m_rgbColor				= m_printingColorList.m_colorDefTable[nBasicColorIndex].m_rgb;
			}
		}
		break;
	}

	if (dlg.DoModal() == IDCANCEL)
		return;

	switch (dlg.m_nPrintColorType)
	{
	case 0:	
	case 1: if (nAlternateColorIndex >= 0)	// previously being alternate color
			{
				CColorDefinition colorDef(dlg.m_strPrintColorName, dlg.m_rgbColor, CMYK(dlg.m_nCyan, dlg.m_nMagenta, dlg.m_nYellow, dlg.m_nKey),
										 (dlg.m_nPrintColorType == 0) ? dlg.m_nProcessCMYK : CColorDefinition::Spot);
				m_printingColorList.m_colorDefTable.InsertColorDef(colorDef);
				POSITION pos = m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.FindIndex(nAlternateColorIndex);
				if (pos)
					m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.RemoveAt(pos);
			}
			else
			{
				m_printingColorList.m_colorDefTable[nBasicColorIndex].m_strColorName = dlg.m_strPrintColorName;
				m_printingColorList.m_colorDefTable[nBasicColorIndex].m_rgb			 = dlg.m_rgbColor;
				m_printingColorList.m_colorDefTable[nBasicColorIndex].m_cmyk		 = CMYK(dlg.m_nCyan, dlg.m_nMagenta, dlg.m_nYellow, dlg.m_nKey);
				m_printingColorList.m_colorDefTable[nBasicColorIndex].m_nColorType	 = (dlg.m_nPrintColorType == 0) ? dlg.m_nProcessCMYK : CColorDefinition::Spot;
				m_printingColorList.m_colorDefTable[nBasicColorIndex].m_nColorIndex  = dlg.m_nIPUIndex;
			}
			m_printingColorList.Initialize(this, m_bShowBasicColorsOnly);
			break;

	case 2: if (nAlternateColorIndex < 0)	// previously being process or spot color
			{
				m_printingColorList.m_colorDefTable.RemoveAt(nBasicColorIndex);
				nBasicColorIndex = m_printingColorList.m_colorDefTable.FindColorDefinitionIndex(dlg.m_strColorMappingTarget);
				if (nBasicColorIndex >= 0)
					m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.AddTail(dlg.m_strPrintColorName);
			}
			else
			{
				POSITION pos = m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.FindIndex(nAlternateColorIndex);
				if (pos)
				{
					CString& rstrAlternateName = m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.GetAt(pos);
					rstrAlternateName = dlg.m_strPrintColorName;
					if (dlg.m_strColorMappingTarget != m_printingColorList.m_colorDefTable[nBasicColorIndex].m_strColorName)	// mapping target changed
					{
						m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.RemoveAt(pos);
						nBasicColorIndex = m_printingColorList.m_colorDefTable.FindColorDefinitionIndex(dlg.m_strColorMappingTarget);
						if (nBasicColorIndex >= 0)
							m_printingColorList.m_colorDefTable[nBasicColorIndex].m_AliasNameList.AddTail(dlg.m_strPrintColorName);
					}
				}
			}
			m_printingColorList.m_colorDefTable[nBasicColorIndex].m_cmyk = CMYK(dlg.m_nCyan, dlg.m_nMagenta, dlg.m_nYellow, dlg.m_nKey);
			m_printingColorList.Initialize(this, m_bShowBasicColorsOnly);
			break;
	}

}

void CDlgPrintColorTable::OnDblclkPrintingColors(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnPrintcolorProps();
	
	*pResult = 0;
}

void CDlgPrintColorTable::OnShowBasiccolorsOnly() 
{
	UpdateData(TRUE);
	m_printingColorList.Initialize(this, m_bShowBasicColorsOnly);
}

void CDlgPrintColorTable::OnOK() 
{
	theApp.m_colorDefTable.RemoveAll();
	theApp.m_colorDefTable.Append(m_printingColorList.m_colorDefTable);
	theApp.m_colorDefTable.Save();
	
	CDialog::OnOK();
}

void CDlgPrintColorTable::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);

//	if ( ! bShow)
//		return;
//
//// look for undefined colors in job
//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
//	if (pDoc)
//	{
//		BOOL bMessageDisplayed = FALSE;
//		pDoc->m_ColorDefinitionTable.UpdateColorInfos();
//		for (int i = 0; i < pDoc->m_ColorDefinitionTable.GetSize(); i++)
//		{
//			CString strMappedColor = theApp.MapPrintColor(pDoc->m_ColorDefinitionTable[i].m_strColorName);
//			if (strMappedColor.CompareNoCase(_T("Composite")) == 0)
//				continue;
//
//			if (strMappedColor == pDoc->m_ColorDefinitionTable[i].m_strColorName)	// Basic colors first
//			{
//				if (pDoc->m_PageTemplateList.ColorInUse(pDoc, pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(strMappedColor)) )
//				{
//					if ( ! theApp.m_colorDefTable.FindColorDefinition(pDoc->m_ColorDefinitionTable[i].m_strColorName))
//					{
//						if ( ! bMessageDisplayed)
//						{
//							if (AfxMessageBox(IDS_MSG_TAKEOVER_UNDEFINED_COLOR, MB_YESNO) == IDNO)
//								break;
//							else
//								bMessageDisplayed = TRUE;
//						}
//						AddUndefinedJobPrintColor(&pDoc->m_ColorDefinitionTable[i]);
//					}
//				}
//			}
//		}
//	}
}
