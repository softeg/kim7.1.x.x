#pragma once
#include "afxcmn.h"


// CDlgConnectToServer-Dialogfeld

class CDlgConnectToServer : public CDialog
{
	DECLARE_DYNAMIC(CDlgConnectToServer)

public:
	CDlgConnectToServer(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgConnectToServer();

// Dialogfelddaten
	enum { IDD = IDD_CONNECT_TO_SERVER };

	int			  m_nProgressCount;
	CProgressCtrl m_progressBar;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
