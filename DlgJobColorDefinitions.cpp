// DlgJobColorDefinitions.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgJobColorDefinitions.h"
#include "DlgPrintColorTable.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CDlgJobColorDefinitions dialog


CDlgJobColorDefinitions::CDlgJobColorDefinitions(BOOL bOverwriteMode, BOOL bHideCompositeButton, CWnd* pParent /*=NULL*/)
	: CDialog(CDlgJobColorDefinitions::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgJobColorDefinitions)
	//}}AFX_DATA_INIT

	m_bHideCompositeButton = FALSE;
	m_bOverwriteMode	   = bOverwriteMode;
	m_bHideCompositeButton = bHideCompositeButton;
}


void CDlgJobColorDefinitions::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgJobColorDefinitions)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgJobColorDefinitions, CDialog)
	//{{AFX_MSG_MAP(CDlgJobColorDefinitions)
	ON_BN_CLICKED(IDC_REMOVE_COLOR_BUTTON, OnRemoveColorButton)
	ON_LBN_DBLCLK(IDC_JOB_COLORS, OnDblclkJobColors)
	ON_BN_CLICKED(IDC_COPY_BUTTON, OnCopyButton)
	ON_LBN_SELCHANGE(IDC_JOB_COLORS, OnSelchangeJobColors)
	ON_BN_CLICKED(IDC_NEW_COLOR_BUTTON, OnNewColorButton)
	ON_LBN_DBLCLK(IDC_PAGESOURCE_COLORS, OnDblclkPagesourceColors)
	ON_BN_CLICKED(IDC_ASSIGN_COMPOSITE_BUTTON, OnAssignCompositeButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgJobColorDefinitions message handlers

BOOL CDlgJobColorDefinitions::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// subclass the control
	VERIFY(m_JobColorListBox.SubclassDlgItem(IDC_JOB_COLORS, this));
	VERIFY(m_PSColorListBox.SubclassDlgItem(IDC_PAGESOURCE_COLORS, this));
	m_JobColorListBox.m_ColorDefTable.Append(theApp.m_colorDefTable);

	for (int i = 0; i < m_JobColorListBox.m_ColorDefTable.GetSize(); i++)
		m_JobColorListBox.AddString((LPCTSTR)0);

	if (m_JobColorListBox.GetCount() > 0)
	{
		m_JobColorListBox.SetCurSel(0);	
		CColorDefinition& rColorDef = m_JobColorListBox.m_ColorDefTable[0];
		m_nColorIndex = rColorDef.m_nColorIndex;
	}

//	m_PSColorDefTable.RemoveAll();
// initialize PSColorDefTable	(changed on 03/03/99 by DT)
	for (int i = 0; i < m_PSColorDefTable.GetSize(); i++)
	{
		CColorDefinition& rColorDef = m_PSColorDefTable[i];
		m_PSColorListBox.m_ColorDefTable.Add(rColorDef);
		m_PSColorListBox.AddString((LPCTSTR)0);
	}

	if (m_PSColorListBox.GetCount() > 0)
	{
		m_PSColorListBox.SetCurSel(0);	
		CColorDefinition& rColorDef = m_PSColorListBox.m_ColorDefTable[0];
		m_nColorIndex = rColorDef.m_nColorIndex;
	}
/////

#ifdef CTP
	GetDlgItem(IDC_ASSIGN_COMPOSITE_BUTTON)->ShowWindow(SW_HIDE);
#else
	if (m_bHideCompositeButton)
		GetDlgItem(IDC_ASSIGN_COMPOSITE_BUTTON)->ShowWindow(SW_HIDE);
#endif

	if (m_bOverwriteMode)
		GetDlgItem(IDC_REMOVE_COLOR_BUTTON)->ShowWindow(SW_HIDE);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgJobColorDefinitions::OnRemoveColorButton() 
{
	int nSel;
	if ((nSel = m_PSColorListBox.GetCurSel()) != LB_ERR)
		m_PSColorListBox.DeleteColorItem(nSel);	

	if (m_PSColorListBox.GetCount() > 0)
		m_PSColorListBox.SetCurSel(0);	
	else
		m_PSColorListBox.SetCurSel(-1);	
}

void CDlgJobColorDefinitions::OnDblclkPagesourceColors() 
{
	if ( ! m_bOverwriteMode)
		OnRemoveColorButton();
}

void CDlgJobColorDefinitions::OnCopyButton() 
{
	int nSelJob;
	if ((nSelJob = m_JobColorListBox.GetCurSel()) == LB_ERR)
		return;

	CColorDefinition& rColorDef = m_JobColorListBox.m_ColorDefTable[nSelJob];
	if (m_bOverwriteMode)
	{
		int nSelPS;
		if ((nSelPS = m_PSColorListBox.GetCurSel()) == LB_ERR)
			return;
		m_PSColorListBox.m_ColorDefTable[nSelPS] = rColorDef;
		if (nSelPS + 1 < m_PSColorListBox.GetCount())
			m_PSColorListBox.SetCurSel(nSelPS + 1);
		m_PSColorListBox.Invalidate();
		m_PSColorListBox.UpdateWindow();
	}
	else
		m_PSColorListBox.AddColorItem(rColorDef.m_rgb, rColorDef.m_strColorName, rColorDef.m_nColorIndex, rColorDef.m_nColorType);
}

void CDlgJobColorDefinitions::OnDblclkJobColors() 
{
	OnCopyButton();	
}

void CDlgJobColorDefinitions::OnSelchangeJobColors() 
{
	int nSel;
	if ((nSel = m_JobColorListBox.GetCurSel()) == LB_ERR)
		return;

	CColorDefinition& rColorDef = m_JobColorListBox.m_ColorDefTable[nSel];
	m_nColorIndex = rColorDef.m_nColorIndex;
	UpdateData(FALSE);
}

void CDlgJobColorDefinitions::OnNewColorButton() 
{
	CDlgPrintColorTable dlg;
	if (dlg.DoModal() == IDCANCEL)
		return;

	//Clears JobColorListBox
	int nCount = m_JobColorListBox.GetCount();
	for (int i = 0; i < nCount; i++)
		m_JobColorListBox.DeleteColorItem(i);

	//Refill JobColorListBox
	m_JobColorListBox.m_ColorDefTable.Load();
	for (int i = 0; i < m_JobColorListBox.m_ColorDefTable.GetSize(); i++)
		m_JobColorListBox.AddString((LPCTSTR)0);

	if (m_JobColorListBox.GetCount() > 0)
	{
		m_JobColorListBox.SetCurSel(0);	
		CColorDefinition& rColorDef = m_JobColorListBox.m_ColorDefTable[0];
		m_nColorIndex = rColorDef.m_nColorIndex;
	}

	m_PSColorDefTable.RemoveAll();

	UpdateData(FALSE);
}

void CDlgJobColorDefinitions::OnAssignCompositeButton() 
{
	CString string("Composite");

	if (m_bOverwriteMode)
	{
		int nSelPS;
		if ((nSelPS = m_PSColorListBox.GetCurSel()) == LB_ERR)
			return;
		m_PSColorListBox.m_ColorDefTable[nSelPS].m_rgb			= WHITE;
		m_PSColorListBox.m_ColorDefTable[nSelPS].m_strColorName = string;
		m_PSColorListBox.m_ColorDefTable[nSelPS].m_nColorIndex	= 0;
		m_PSColorListBox.m_ColorDefTable[nSelPS].m_nColorType	= -1;

		if (nSelPS + 1 < m_PSColorListBox.GetCount())
			m_PSColorListBox.SetCurSel(nSelPS + 1);
		m_PSColorListBox.Invalidate();
		m_PSColorListBox.UpdateWindow();
	}
	else
		m_PSColorListBox.AddColorItem(WHITE, string, 0, -1);
}

void CDlgJobColorDefinitions::OnOK() 
{
	if ( ! m_bHideCompositeButton)
		if (m_PSColorListBox.GetCount() == 0)
		{
			AfxMessageBox(IDS_SELECT_COLOR_MESSAGE, MB_OK);
			return;
		}

	m_PSColorDefTable.RemoveAll();
	for (int i = 0; i < m_PSColorListBox.GetCount(); i++)
		m_PSColorDefTable.Add(m_PSColorListBox.m_ColorDefTable[i]);

	CDialog::OnOK();
}
