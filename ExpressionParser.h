#pragma once

class CExpressionParser
{
public:
	~CExpressionParser(void);

public:
	enum defines { NumTokens = 27, TokenLenght = 255, EndOfFile = 256 };
	enum symbols { NoRuleSym = 0, PrintingMachineSym, WorkStyleSym, BindingStyleSym, NumColorsSym, NumSpotColorsSym, SheetFormatSym, SheetSideSym, SheetWidthSym, SheetHeightSym, PaperNameSym, PaperTypeSym, ColorSym, DeviceID_PrintingMachineSym,
				   EqualSym, UnequalSym, SmallerSym, GreaterSym, IncludeSym, ActionSym, ProductTypeSym, ProductPartSym, PageFormatSym, PageNumAtProductSym, PageNumAtProductPartSym, PageNumAtFoldSheetSym, FoldSheetSideSym,
				   LeftBracketSym, RightBracketSym, QuotationSym, /*LeftSquareBracketSym, RightSquareBracketSym,*/ SlashSym, IdentSym, NumSym, NullSym, EndOfFileSym };


    CExpressionParser()
	{
		m_symbolTable[0]  = NoRuleSym;
		m_symbolTable[1]  = PrintingMachineSym;
		m_symbolTable[2]  = WorkStyleSym;
		m_symbolTable[3]  = BindingStyleSym;
		m_symbolTable[4]  = NumColorsSym;
		m_symbolTable[5]  = NumSpotColorsSym;
		m_symbolTable[6]  = SheetFormatSym;
		m_symbolTable[7]  = SheetSideSym;
		m_symbolTable[8]  = SheetWidthSym;
		m_symbolTable[9]  = SheetHeightSym;
		m_symbolTable[10] = PaperNameSym;
		m_symbolTable[11] = PaperTypeSym;
		m_symbolTable[12] = ColorSym;
		m_symbolTable[13] = DeviceID_PrintingMachineSym;
		m_symbolTable[14] = EqualSym;
		m_symbolTable[15] = UnequalSym;
		m_symbolTable[16] = SmallerSym;
		m_symbolTable[17] = GreaterSym;
		m_symbolTable[18] = IncludeSym;
		m_symbolTable[19] = ActionSym;
		m_symbolTable[20] = ProductTypeSym;
		m_symbolTable[21] = ProductPartSym;
		m_symbolTable[22] = PageFormatSym;
		m_symbolTable[23] = PageNumAtProductSym;
		m_symbolTable[24] = PageNumAtProductPartSym;
		m_symbolTable[25] = PageNumAtFoldSheetSym;
		m_symbolTable[26] = FoldSheetSideSym;

		m_pTokens[0]	  = _T("NoRule");
		m_pTokens[1]	  = _T("PrintingMachine");
		m_pTokens[2]	  = _T("WorkStyle");
		m_pTokens[3]	  = _T("BindingStyle");
		m_pTokens[4]	  = _T("NumColors");
		m_pTokens[5]	  = _T("NumSpotColors");
		m_pTokens[6]	  = _T("SheetFormat");
		m_pTokens[7]	  = _T("SheetSide");
		m_pTokens[8]	  = _T("SheetWidth");
		m_pTokens[9]	  = _T("SheetHeight");
		m_pTokens[10]	  = _T("PaperName");
		m_pTokens[11]	  = _T("PaperType");
		m_pTokens[12]	  = _T("Color");
		m_pTokens[13]	  = _T("DeviceID_PrintingMachine");
		m_pTokens[14]	  = _T("=");
		m_pTokens[15]	  = _T("!=");
		m_pTokens[16]	  = _T("<");
		m_pTokens[17]	  = _T(">");
		m_pTokens[18]	  = _T("INCLUDE");
		m_pTokens[19]	  = _T("Action");
		m_pTokens[20]	  = _T("ProductType");
		m_pTokens[21]	  = _T("ProductPartName");
		m_pTokens[22]	  = _T("PageFormat");
		m_pTokens[23]	  = _T("PageNum@Product");
		m_pTokens[24]	  = _T("PageNum@ProductPart");
		m_pTokens[25]	  = _T("PageNum@FoldSheet");
		m_pTokens[26]	  = _T("FoldSheetSide");

		m_fValue	= 0.0f;
		m_nLineNum  = 0;
		for (int i = 0; i < NumTokens; i++)
		{
			m_foundSymbols[i]	 = FALSE;
			m_requiredSymbols[i] = FALSE;
		}
	};


//Attributes:
public:
	int			m_cChar;
	int			m_nSymbol;
	TCHAR		m_strID[TokenLenght + 1];
	CString		m_strVariable;
	CString		m_strOperator;
	CString		m_strValue;
	float		m_fValue;
	int			m_nLineNum;
	CStdioFile	m_file;
	CString		m_strInputStream;
	int			m_nStreamPos;
	int			m_nFoundSymbols;
	int			m_symbolTable[NumTokens];
	TCHAR*		m_pTokens[NumTokens];
	BOOL		m_foundSymbols[NumTokens];
	BOOL		m_requiredSymbols[NumTokens];

//Results:


//Operations:
	void	Reset();
	void	Configure(int nSymbol, ...);
	void	ParseFile(const CString& strFilePath);
	void	ParseString(const CString& strInputStream);
	void	AnalyseSymbol();
	void	GetSymbol();
	void	GetString();
	void	GetLine();
	void	GetNextChar();
	int		SearchSymbol(TCHAR* pString);
	BOOL	RequiredSymbolsFound(int nLastSymbolFound);
	float	Ascii2Float(CString strBuffer);
};
