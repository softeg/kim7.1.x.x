// DlgTrimboxWarning.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgTrimboxWarning.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgTrimboxWarning 


CDlgTrimboxWarning::CDlgTrimboxWarning(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgTrimboxWarning::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgTrimboxWarning)
	m_strText = _T("");
	m_bTrimboxWarningCheck = FALSE;
	//}}AFX_DATA_INIT
}


void CDlgTrimboxWarning::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgTrimboxWarning)
	DDX_Text(pDX, IDC_TRIMBOX_WARNING_TEXT, m_strText);
	DDX_Check(pDX, IDC_TRIMBOX_WARNING_CHECK, m_bTrimboxWarningCheck);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgTrimboxWarning, CDialog)
	//{{AFX_MSG_MAP(CDlgTrimboxWarning)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgTrimboxWarning 

BOOL CDlgTrimboxWarning::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgTrimboxWarning::OnOK() 
{
	UpdateData(TRUE);

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	long ret = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	if (m_bTrimboxWarningCheck)
		RegSetValueEx(hKey, _T("TrimboxPagesizeMismatchWarning"), 0, REG_SZ, (LPBYTE)_T("FALSE"), 6 * sizeof(TCHAR));
	else
		RegSetValueEx(hKey, _T("TrimboxPagesizeMismatchWarning"), 0, REG_SZ, (LPBYTE)_T("TRUE"), 5 * sizeof(TCHAR));
	
	ret = RegCloseKey(hKey);

	theApp.settings.OnLoadingSettings(); // reload new settings

	CDialog::OnOK();
}

