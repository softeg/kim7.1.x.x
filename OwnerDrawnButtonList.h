#pragma once

#define ODB_NORMAL		1
#define ODB_HOT			2
#define ODB_CHECKED		4
#define ODB_HOTCHECKED	8
#define ODB_DISABLED	16

#define ODBL_HORIZONTAL 1
#define ODBL_VERTICAL	2


// CPPFoldSheetGroupButton

class COwnerDrawnButton : public CButton
{
	DECLARE_DYNAMIC(COwnerDrawnButton)

public:
	COwnerDrawnButton();
	virtual ~COwnerDrawnButton();

public:
	int							 m_nIndex;
	int							 m_nBitmapIndex;
	int							 m_nCommandID;
	BOOL						 m_bPushed, m_bEnabled;
	class COwnerDrawnButtonList* m_pButtonList;
	BOOL						 m_bMouseOverButton;
	(ULONG_PTR)					 m_itemData;
	CBrush						 m_hollowBrush, m_bkBrush;
	BOOL						 m_bCloseButton;
	CRect						 m_rcClose;
	BOOL						 m_bMouseOverClose;
	CBitmap*					 m_pBkBitmap;

public:
	void DrawButtonBackground(CDC* pDC, int nState, CRect rcItem);
	void DrawBackgroundNormalState(CDC* pDC, CRect& rcFrame);
	void SaveBackground(CDC* pDC, const CRect& rect);
	void RestoreBackground(CDC* pDC, const CRect& rect);

	void SetCheck(int nCheck);
	void Enable(BOOL bEnable);

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnUpdateODButton(CCmdUI* pCmdUI);
	afx_msg void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnBnClicked();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
public:
	afx_msg HBRUSH CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

// COwnerDrawnButtonList 

class COwnerDrawnButtonList : public CArray<COwnerDrawnButton*, COwnerDrawnButton*>
{
public:
	COwnerDrawnButtonList ();
	~COwnerDrawnButtonList ();

public:
	CFont			m_font;
	CRect			m_rcFrame;
	int				m_nOrientation;
	CImage 			m_imgCold;
	CImage 			m_imgHot;
	CSize			m_sizeBitmap;
	CWnd*			m_pWndParent;
	CWnd*			m_pWndMsgTarget;
	int				m_nCurSelection;
	void			(*m_pFnDrawItem)(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	void			(*m_pFnDrawBackground)(CDC* pDC, CRect rcUpdateRgn, CWnd* pUpdateWnd);
	BOOL			m_bGroup;
	CStringW		m_strTheme;
	CToolTipCtrl*	m_pToolTip;
	int				m_nShow;


public:
	void		Create(int nFrameID, int nOrientation, CWnd* pWndParent, CWnd* pWndMsgTarget, void (*pFnDrawItem)(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState) = NULL, int nIDBitmapHot = -1, int nIDBitmapCold = -1, CSize sizeBitmap = CSize(0, 0), BOOL bGroup = TRUE);
	void		Create(CWnd* pWndParent, CWnd* pWndMsgTarget, void (*pFnDrawItem)(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState) = NULL, int nIDBitmapHot = -1, int nIDBitmapCold = -1, 
						CSize sizeBitmap = CSize(0, 0), void (*pFnDrawBackground)(CDC* pDC, CRect rcUpdateRgn, CWnd* pUpdateWnd) = NULL);
	void		SetTheme(const CString& strTheme) { m_strTheme = strTheme;};
	void		AddButton(int nLabelID,	int nTooltipID, int nCommandID, int nSize, int nBitmapIndex = -1, ULONG_PTR itemData = NULL, BOOL bPushed = FALSE);
	void		AddButton(const CString& strLabel, const CString& strTooltip, int nCommandID, int nSize, int nBitmapIndex = -1, ULONG_PTR itemData = NULL, BOOL bPushed = FALSE);
	void		AddButton(int nLabelID,	int nTooltipID, int nCommandID, CRect& rcFrame, int nBitmapIndex = -1, ULONG_PTR itemData = NULL, BOOL bPushed = FALSE, BOOL bCloseButton = FALSE);
	void		AddButton(const CString& strLabel, const CString& strTooltip, int nCommandID, CRect& rcFrame, int nBitmapIndex = -1, ULONG_PTR itemData = NULL, BOOL bPushed = FALSE, BOOL bCloseButton = FALSE);
	CRect		GetButtonClientRect(int nIndex);
	CRect		GetButtonClientRectByCommand(int nIDCommand);
	void		Show();
	void		Hide();
	void		Redraw();
	void		RemoveAll();
	void		ResetData();
	void		SelectButton(int nIndex);
	void		SelectButton(ULONG_PTR itemData);
	void		DeselectAll() { for (int i = 0; i < GetSize(); i++) ElementAt(i)->m_bPushed = FALSE; m_nCurSelection = -1; };
	int			GetSelection() { return m_nCurSelection; };
	int			GetSelectedButtonID()	{ return ( (GetSelection() >= 0) && (GetSelection() < GetSize()) ) ? ElementAt(GetSelection())->m_nCommandID : -1;   };
	ULONG_PTR	GetSelectedButtonData() { return ( (GetSelection() >= 0) && (GetSelection() < GetSize()) ) ? ElementAt(GetSelection())->m_itemData	 : NULL; };
	void		OffsetFrame(int nXOffset, int nYOffset) { m_rcFrame.OffsetRect(nXOffset, nYOffset); };
	static void	SetCheck(CCmdUI* pCmdUI, BOOL bCheck);
	static void Enable(CCmdUI* pCmdUI, BOOL bEnable);
	static void	Show(CCmdUI* pCmdUI, int nShow);
	BOOL		SelectedButtonClosePressed();
	COwnerDrawnButton*	GetButtonByCommand(int nIDCommand);
	void				EnableButton(int nIDCommand, BOOL bEnable);
	void				CheckButton(int nIDCommand, BOOL bEnable);
	void		ShowButton(int nIDCommand, int nShow);
	void		ShowButtonByIndex(int nIndex, int nShow);
	void		MoveButtons(CPoint ptTopLeft);
	void		ClipButtons(CDC* pDC);
	void		Destroy();
};

