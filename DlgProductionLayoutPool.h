#pragma once


typedef struct
{
	CPrintSheet* pPrintSheet;
	CLayout*	 pLayout;
}LP_PRINTLAYOUT;


// CDlgProductionLayoutPool-Dialogfeld

class CDlgProductionLayoutPool : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionLayoutPool)

public:
	CDlgProductionLayoutPool(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionLayoutPool();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTION_LAYOUTPOOL };


public:
	BOOL				m_bLocked;
	CFont				m_smallFont;
	CImageList*			m_pLayoutPoolImageList;
	CArray<LP_PRINTLAYOUT, LP_PRINTLAYOUT> m_sampleList;


public:
	CProductionProfile& GetProductionProfile();
	CBoundProductPart&	GetBoundProductPart();
	void 				InitData();
	void 				SaveData();

public:
	void InitLayoutPoolList();


protected:
	CListCtrl m_layoutPoolList;


public:
	virtual BOOL Create(UINT nIDTemplate, CWnd* pParentWnd = NULL);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedOk();
	afx_msg void OnNMCustomdrawLayoutPoollist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedFoldingparamsAddscheme();
public:
	afx_msg void OnBnClickedLayoutpoolAdd();
	afx_msg void OnBnClickedLayoutpoolDelete();
};
