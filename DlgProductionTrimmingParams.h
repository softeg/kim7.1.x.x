#pragma once


// CPTPCustomPaintFrame

class CPTPCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CPTPCustomPaintFrame)

public:
	CPTPCustomPaintFrame();
	virtual ~CPTPCustomPaintFrame();

public:
	void DrawPages(CDC* pDC, CRect frameRect);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};


// CDlgProductionTrimmingParams-Dialogfeld

class CDlgProductionTrimmingParams : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionTrimmingParams)

public:
	CDlgProductionTrimmingParams(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionTrimmingParams();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTION_TRIMMINGPARAMS };

public:
	CPoint				 m_ptInitialPos;
	BOOL				 m_bLocked;
	CPTPCustomPaintFrame m_pagesPreviewFrame;
	int					 m_nPagePreviewIndex;

public:
	CProductionProfile& GetProductionProfile();
	CTrimmingProcess&	GetTrimmingProcess();
	CFoldingProcess&	GetFoldingProcess();
	CBoundProductPart&	GetBoundProductPart();
	void 				InitData();
	void 				SaveData();
	void 				UpdatePreview();


protected:
	CSpinButtonCtrl	m_sideTrimSpin;
	CSpinButtonCtrl	m_headTrimSpin;
	CSpinButtonCtrl	m_footTrimSpin;
public:
	float	m_fHeadTrim;
	float	m_fFootTrim;
	float	m_fSideTrim;


public:
	virtual BOOL Create(UINT nIDTemplate, CWnd* pParentWnd = NULL);
protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedOk();
	afx_msg void OnDeltaposPdPagespreviewSpin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposFoottrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposHeadtrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSidetrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusFoottrimEdit();
	afx_msg void OnKillfocusHeadtrimEdit();
	afx_msg void OnKillfocusSidetrimEdit();
};
