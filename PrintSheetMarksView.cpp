// PrintSheetMarksView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "DlgJobColorDefinitions.h"
#include "MainFrm.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "PrintSheetFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetMarksView.h"
#include "PrintSheetObjectPreview.h"
#ifdef PDF
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"
#endif 


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif 



/////////////////////////////////////////////////////////////////////////////
// CPrintSheetMarksView 
 
IMPLEMENT_DYNCREATE(CPrintSheetMarksView, CFormView)

CPrintSheetMarksView::CPrintSheetMarksView()
	: CFormView(CPrintSheetMarksView::IDD) 
{
	//{{AFX_DATA_INIT(CPrintSheetMarksView)
	m_bOpenMarklib = FALSE;
	//}}AFX_DATA_INIT
 
	m_nCurObjectSel	 = 0;
	m_pCurrentLayout = NULL;

	m_pMarkSetImageList = new CImageList();
	m_pMarkImageList	= new CImageList();
	m_pBkBrush			= new CBrush(RGB(205,205,215));
	m_pHeaderBrush		= new CBrush(RGB(255,180,180));		

	m_pToolTip = NULL;
}

CPrintSheetMarksView::~CPrintSheetMarksView()
{
	delete m_pToolTip;
	delete m_pBkBrush;
	delete m_pHeaderBrush;
}

HBRUSH CPrintSheetMarksView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	switch (nCtlColor)
	{
	case CTLCOLOR_STATIC:	 pDC->SetBkMode(TRANSPARENT);
							 return (HBRUSH)(m_pHeaderBrush->GetSafeHandle());
	case CTLCOLOR_SCROLLBAR: CView::OnCtlColor(pDC, pWnd, nCtlColor);
							 break;
	default:				 return (HBRUSH)(m_pBkBrush->GetSafeHandle());
	}
	return CView::OnCtlColor(pDC, pWnd, nCtlColor);
}

void CPrintSheetMarksView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintSheetMarksView)
	DDX_Control(pDX, IDC_MARK_LIST, m_objectListCtrl);
	DDX_Check(pDX, IDC_OPEN_MARKLIB, m_bOpenMarklib);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPrintSheetMarksView, CFormView)
	//{{AFX_MSG_MAP(CPrintSheetMarksView)
	ON_BN_CLICKED(IDC_MARK_DELETE, OnMarkDelete)
	ON_BN_CLICKED(IDC_MARK_NEW, OnMarkNew)
	ON_BN_CLICKED(IDC_MARK_DUPLICATE, OnMarkDuplicate)
	ON_BN_CLICKED(ID_CLOSE_MARKS_VIEW, OnCloseMarksView)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_MARK_LIST, OnEndlabeleditObjectList)
	ON_WM_DESTROY()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_MARK_LIST, OnItemchangedObjectList)
	ON_NOTIFY(NM_CLICK, IDC_MARK_LIST, OnClickObjectList)
	ON_BN_CLICKED(IDC_OPEN_MARKLIB, OnOpenMarklib)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Diagnose CPrintSheetMarksView

#ifdef _DEBUG
void CPrintSheetMarksView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPrintSheetMarksView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CImpManDoc* CPrintSheetMarksView::GetDocument() // non-debug version is inline
{
    ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CImpManDoc)));
    return (CImpManDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CPrintSheetMarksView 

void CPrintSheetMarksView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CRect rect;
	GetWindowRect(rect);
	if (rect.Width() > 1000)
	{
		const int nOffset = 50;
		GetDlgItem(IDC_MARKSET_HEADER	)->GetWindowRect(rect); ScreenToClient(rect);
		GetDlgItem(IDC_MARKSET_HEADER	)->SetWindowPos(NULL, rect.left, rect.top, rect.Width() + nOffset, rect.Height(), SWP_SHOWWINDOW);
		GetDlgItem(IDC_MARK_LIST		)->GetWindowRect(rect); ScreenToClient(rect);
		GetDlgItem(IDC_MARK_LIST		)->SetWindowPos(NULL, rect.left, rect.top, rect.Width() + nOffset, rect.Height(), SWP_SHOWWINDOW);
		GetDlgItem(IDC_MARKPROPS_PREVIEW)->GetWindowRect(rect); ScreenToClient(rect);
		GetDlgItem(IDC_MARKPROPS_PREVIEW)->SetWindowPos(NULL, rect.left, rect.top, rect.Width() + nOffset, rect.Height(), SWP_HIDEWINDOW);
		GetDlgItem(IDC_MARKPROPS_FRAME	)->GetWindowRect(rect); ScreenToClient(rect);
		GetDlgItem(IDC_MARKPROPS_FRAME	)->SetWindowPos(NULL, rect.left + nOffset, rect.top, rect.Width(), rect.Height(), SWP_HIDEWINDOW);
		GetDlgItem(ID_CLOSE_MARKS_VIEW	)->GetWindowRect(rect); ScreenToClient(rect);
		GetDlgItem(ID_CLOSE_MARKS_VIEW	)->SetWindowPos(NULL, rect.left + nOffset, rect.top, rect.Width(), rect.Height(), SWP_SHOWWINDOW);
		GetDlgItem(IDC_APPLY_BUTTON		)->GetWindowRect(rect); ScreenToClient(rect);
		GetDlgItem(IDC_APPLY_BUTTON		)->SetWindowPos(NULL, rect.left + nOffset, rect.top, rect.Width(), rect.Height(), SWP_SHOWWINDOW);
	}

	pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// collect garbage

	m_dlgSelectNewMark.Create(IDD_SELECT_NEW_MARK, this);
	m_dlgSelectNewMark.m_pParent = this;

// Set up button icons
	((CButton*)GetDlgItem(IDC_MARK_NEW		 ))->SetIcon(theApp.LoadIcon(IDI_MARK_NEW));
	((CButton*)GetDlgItem(IDC_OPEN_MARKLIB	 ))->SetIcon(theApp.LoadIcon(IDI_MARKLIB_OPEN));
	((CButton*)GetDlgItem(IDC_MARK_DUPLICATE ))->SetIcon(theApp.LoadIcon(IDI_MARK_DUPLICATE));
	((CButton*)GetDlgItem(IDC_MARK_DELETE	 ))->SetIcon(theApp.LoadIcon(IDI_DELETE));

// Set up tooltips       
	m_pToolTip = new CToolTipCtrl;
    m_pToolTip->Create(this);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_NEW)),		 IDS_MARK_NEW_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_OPEN_MARKLIB)),	 IDS_MARKLIB_OPEN_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_DUPLICATE)),	 IDS_MARK_DUPLICATE_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MARK_DELETE)),	 IDS_MARK_DELETE_TIP);
	m_pToolTip->Activate(TRUE);


// Set up image lists
	CBitmap bitmap;
	m_pMarkImageList->Create(16, 16, ILC_COLOR8, 3, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_CUSTOM_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_TEXT_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_SECTION_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_CROP_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_FOLD_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_BARCODE_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	bitmap.LoadBitmap(IDB_DRAWING_MARK);
	m_pMarkImageList->Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	m_objectListCtrl.SetImageList(m_pMarkImageList, LVSIL_SMALL);

//	m_bOpenMarklib = FALSE;
//	UpdateData(FALSE);
//	OnOpenMarklib();

	UpdateWindow();

	GetDlgItem(IDC_MARKSET_HEADER)->GetWindowText(m_strHeaderText);

	InitObjectList();

	m_dlgLayoutObjectControl.Initialize(IDC_MARKPROPS_FRAME,   GetCurrentLayoutObject(), GetPrintSheet(), this);
}

void CPrintSheetMarksView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView) 
{
	if (bActivate)
		m_dlgLayoutObjectControl.ObjectPreviewWindow_LoadData();

	CFormView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}

BOOL CPrintSheetMarksView::PreTranslateMessage(MSG* pMsg) 
{
	if (NULL != m_pToolTip)            
		m_pToolTip->RelayEvent(pMsg);	

	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_ESCAPE)
		{
			OnCloseMarksView();
			return TRUE;
		}

	return CFormView::PreTranslateMessage(pMsg);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
// mark sets

CString CPrintSheetMarksView::GetMarksetFolder()
{
#ifdef PDF
	return (CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets"));
#endif
#ifdef CTP
	return (CString(theApp.GetDataFolder()) + _T("\\BMP-Marksets"));
#endif
}


CString CPrintSheetMarksView::GetTempMarksetFolder()
{
	return CString(theApp.settings.m_szTempDir) + _T("\\Temporary Marks");
}


CString CPrintSheetMarksView::GetMarksetFilename(const CString& strName)
{
	return GetMarksetFolder() + "\\" + strName + _T(".mks");
}


CString CPrintSheetMarksView::GetTempMarksetFilename(const CString& strName)
{
	return GetTempMarksetFolder() + "\\" + strName + _T(".mks");
}

CLayout* CPrintSheetMarksView::GetLayout()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if (pDoc->GetActLayout())
		return pDoc->GetActLayout();
	else
	{
		CPrintSheet* pFirstPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
		return (pFirstPrintSheet) ? pFirstPrintSheet->GetLayout() : NULL;
	}
}

CPrintSheet* CPrintSheetMarksView::GetPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if (pDoc->GetActPrintSheet())
		return pDoc->GetActPrintSheet();
	else
		return (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
}

CLayoutObject* CPrintSheetMarksView::GetCurrentLayoutObject()
{
	return ( (m_nCurObjectSel >= 0) && (m_nCurObjectSel < m_objectList.GetSize()) ? m_objectList[m_nCurObjectSel].m_pLayoutObject : NULL);
}

void CPrintSheetMarksView::InitObjectList(CLayoutObject* pLayoutObject)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pPrintSheet || ! pLayout)
		return;

	CString strTitle;
	strTitle.Format(m_strHeaderText, (pLayout) ? pLayout->m_strLayoutName : "");
	GetDlgItem(IDC_MARKSET_HEADER)->SetWindowText(strTitle);

	m_objectListCtrl.DeleteAllItems();

	pLayout->m_FrontSide.m_ObjectList.GetSheetObjectRefList(pPrintSheet, &m_objectList, CLayoutObject::ControlMark);

	BOOL bNeedToSort = FALSE;
	CArray <CSheetObjectRefs, CSheetObjectRefs&> backSideObjectRefs;
	pLayout->m_BackSide.m_ObjectList.GetSheetObjectRefList(pPrintSheet, &backSideObjectRefs, CLayoutObject::ControlMark);
	for (int i = 0; i < backSideObjectRefs.GetSize(); i++)
	{
		CLayoutObject* pLayoutObject = backSideObjectRefs[i].m_pLayoutObject;
		CPageTemplate* pTemplate	 = backSideObjectRefs[i].m_pObjectTemplate;
		if ( ! pLayoutObject || ! pTemplate)
			continue;

		if ( ! pLayoutObject->FindCounterpartControlMark())
		{
			m_objectList.Add(backSideObjectRefs[i]);
			bNeedToSort = TRUE;
		}
	}
	if (bNeedToSort)
		qsort((void*)m_objectList.GetData(), m_objectList.GetSize(), sizeof(CSheetObjectRefs), CSheetObjectRefs::Compare);

	if ( ! m_objectList.GetSize()) 
	{
		GetDlgItem(IDC_MARK_DUPLICATE )->EnableWindow(FALSE);
		GetDlgItem(IDC_MARK_DELETE	  )->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_MARK_DUPLICATE )->EnableWindow(TRUE);
		GetDlgItem(IDC_MARK_DELETE    )->EnableWindow(TRUE);
	}

	for (int i = 0; i < m_objectList.GetSize(); i ++)                        
	{
		LV_ITEM item;
		item.mask		= LVIF_TEXT | LVIF_IMAGE;
		item.cchTextMax = MAX_TEXT;
		item.iItem		= i; 
		item.iSubItem	= 0;
		item.pszText	= m_objectList[i].m_pObjectTemplate->m_strPageID.GetBuffer(MAX_TEXT);
		switch (m_objectList[i].m_nMarkType)
		{
		case CMark::CustomMark:		item.iImage	= 0; break;
		case CMark::TextMark:		item.iImage	= 1; break;
		case CMark::SectionMark:	item.iImage	= 2; break;
		case CMark::CropMark:		item.iImage	= 3; break;
		case CMark::FoldMark:		item.iImage	= 4; break;
		case CMark::BarcodeMark:	item.iImage	= 5; break;
		case CMark::DrawingMark:	item.iImage	= 6; break;
		}
		m_objectListCtrl.InsertItem(&item);

		if (pLayoutObject == m_objectList[i].m_pLayoutObject)
			m_nCurObjectSel = i;
	}

	if ( ! m_objectList.GetSize())
		m_nCurObjectSel = -1;
	else
		if ( ! pLayoutObject)		// if no layout object -> keep current selection if inside valid range
			if ( (m_nCurObjectSel < 0) || (m_nCurObjectSel >= m_objectList.GetSize()) )
				m_nCurObjectSel = 0;

	if ( (m_nCurObjectSel >= 0) && (m_nCurObjectSel < m_objectList.GetSize()) )
		m_objectListCtrl.SetItemState(m_nCurObjectSel, LVIS_SELECTED, LVIS_SELECTED);

	if ( (m_nCurObjectSel >= 0) && (m_nCurObjectSel < m_objectList.GetSize()) )
		if ( (m_objectList[m_nCurObjectSel].m_nMarkType == CMark::CustomMark) || (m_objectList[m_nCurObjectSel].m_nMarkType == CMark::TextMark)  || (m_objectList[m_nCurObjectSel].m_nMarkType == CMark::BarcodeMark) || (m_objectList[m_nCurObjectSel].m_nMarkType == CMark::DrawingMark) )
			GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(TRUE);
		else
			GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(FALSE);

	m_pCurrentLayout = pLayout;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
// mark list

BOOL CPrintSheetMarksView::IsSelected(CLayoutObject* pObject)
{
	if ( ! this)
		return FALSE;
	if ( ! pObject)
		return FALSE;
	if (pObject->m_nType != CLayoutObject::ControlMark)
		return FALSE;
	if ( (m_nCurObjectSel < 0) || (m_nCurObjectSel >= m_objectList.GetSize()) )
		return FALSE;

	for (int i = 0; i < m_objectList.GetSize(); i++)
	{
		if (m_objectListCtrl.GetItemState(i, LVIS_SELECTED))
		{
			if (m_objectList[i].m_pLayoutObject == pObject)
				return TRUE;

			CLayoutObject* pCounterpartObject = pObject->FindCounterpartControlMark();
			if (pCounterpartObject)
				if (m_objectList[i].m_pLayoutObject == pCounterpartObject)
					return TRUE;

			CPageTemplate* pObjectTemplate = pObject->GetCurrentMarkTemplate();
			if ( ! pObjectTemplate)
				return FALSE;

			int nMarkType = pObjectTemplate->GetMarkType();
			if ( (nMarkType == CMark::SectionMark) || (nMarkType == CMark::FoldMark) || (nMarkType == CMark::CropMark) )
				if ( (nMarkType == m_objectList[i].m_nMarkType) && (pObjectTemplate->m_strPageID == m_objectList[i].m_pObjectTemplate->m_strPageID) )
					return TRUE;
		}
	}

	return FALSE;
}

void CPrintSheetMarksView::OnItemchangedObjectList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	
	*pResult = 0;

	int nNewSel;
	if (pNMListView->uNewState & LVNI_SELECTED)
		nNewSel = pNMListView->iItem;
	else
		return;
	if (nNewSel == m_nCurObjectSel)
		return;
	if ((nNewSel < 0) || (nNewSel >= m_objectListCtrl.GetItemCount()))
		return;
	if ((m_nCurObjectSel < 0) || (m_nCurObjectSel >= m_objectListCtrl.GetItemCount()))
		return;
	if (m_objectListCtrl.GetSelectedCount() > 1)
		return;

	UpdateData(TRUE);

	m_dlgLayoutObjectControl.DataExchangeSave();
	m_nCurObjectSel = nNewSel;
	if ( (m_nCurObjectSel >= 0) && (m_nCurObjectSel < m_objectList.GetSize()) )
	{
		m_dlgLayoutObjectControl.ReinitDialog(m_objectList[m_nCurObjectSel].m_pLayoutObject, GetPrintSheet());
		if ( (m_objectList[m_nCurObjectSel].m_nMarkType == CMark::CustomMark) || (m_objectList[m_nCurObjectSel].m_nMarkType == CMark::TextMark)  || (m_objectList[m_nCurObjectSel].m_nMarkType == CMark::BarcodeMark) || (m_objectList[m_nCurObjectSel].m_nMarkType == CMark::DrawingMark) )
			GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(TRUE);
		else
			GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(FALSE);
	}
	else
		GetDlgItem(IDC_MARK_DUPLICATE)->EnableWindow(FALSE);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CRect clientRect;
	pView->GetClientRect(clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		return;

/*
	if ( (m_nCurObjectSel >= 0) && (m_nCurObjectSel < m_objectList.GetSize()) )
	{
		CMark* pMark = m_objectList[m_nCurObjectSel];
		if ( (pMark->m_nType != CMark::CropMark) && (pMark->m_nType != CMark::FoldMark) )
		{
			CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData(pMark);
			if (pDI)
				if (pDI->m_bIsVisible != CDisplayItem::isCompleteVisible) 
				{
					pView->Invalidate();
					CPoint scrollPos = pView->GetScrollPosition();
					int nOffX = (clientRect.Width()  - pDI->m_BoundingRect.Width()) /2;
					int nOffY = (clientRect.Height() - pDI->m_BoundingRect.Height())/2;
					pView->ScrollToPosition(CPoint(scrollPos.x + pDI->m_BoundingRect.left - nOffX, scrollPos.y + pDI->m_BoundingRect.top - nOffY));
				}
		}
	}
*/
}

void CPrintSheetMarksView::OnClickObjectList(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	if (m_objectListCtrl.GetNextItem(-1, LVIS_SELECTED) == -1)	// clicked outside any item
	{
		m_objectListCtrl.SetItemState(m_nCurObjectSel, LVIS_SELECTED, LVIS_SELECTED);
		UpdateSheetPreview();
	}
	else
		if (m_objectListCtrl.GetSelectedCount() > 1)
			UpdateSheetPreview();

	*pResult = 0;
}

void CPrintSheetMarksView::OnEndlabeleditObjectList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	
	*pResult = 0;

	if ( (pDispInfo->item.iItem < 0) || (pDispInfo->item.iItem >= m_objectList.GetSize()) )
		return;

	CString strMarkName;
	m_objectListCtrl.GetEditControl()->GetWindowText(strMarkName);
	m_objectList[pDispInfo->item.iItem].m_pObjectTemplate->m_strPageID = strMarkName;
	m_objectListCtrl.SetItemText(pDispInfo->item.iItem, 0, (LPCTSTR)strMarkName);

	InitObjectList(m_objectList[pDispInfo->item.iItem].m_pLayoutObject);

	m_dlgLayoutObjectControl.m_pObjectTemplate->m_strPageID = strMarkName;
}


void CPrintSheetMarksView::OnMarkNew() 
{
	CRect rect;
	GetDlgItem(IDC_MARK_NEW)->GetWindowRect(rect);
	m_dlgSelectNewMark.MoveWindow(rect.right, rect.top-50, 0, 0);
	m_dlgSelectNewMark.ShowWindow(SW_SHOW);
}

void CPrintSheetMarksView::AddNewMark(int nMarkType) 
{
   	CPrintSheetFrame* pParentFrame = (CPrintSheetFrame*)GetParentFrame();
	if ( ! pParentFrame)
		return;
	if ( ! pParentFrame->m_pDlgLayoutObjectProperties)
		return;

	m_dlgLayoutObjectControl.DataExchangeSave();

	CMark*  pMark = NULL;
	switch (nMarkType)
	{
	case ID_NEW_CUSTOMMARK:
	case ID_NEW_TEXTMARK:
		{
			if (nMarkType == ID_NEW_CUSTOMMARK)
			{
				pMark = (CCustomMark*) new CCustomMark;
				((CCustomMark*)pMark)->Create(theApp.m_colorDefTable);
				pMark->m_strMarkName = FindFreeMarkName(pMark->m_strMarkName);
				pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
			}
			else
			{
				pMark = (CTextMark*) new CTextMark;
				((CTextMark*)pMark)->Create(theApp.m_colorDefTable);
				pMark->m_strMarkName = FindFreeMarkName(pMark->m_strMarkName);
				pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
			}
		}
		break;

	case ID_NEW_SECTIONMARK:
		pMark = (CSectionMark*) new CSectionMark;
		((CSectionMark*)pMark)->Create(theApp.m_colorDefTable);
		pMark->m_strMarkName = FindFreeMarkName(pMark->m_strMarkName);
		pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
		break;

	case ID_NEW_CROPMARK:
		pMark = (CCropMark*) new CCropMark;
		((CCropMark*)pMark)->Create(theApp.m_colorDefTable);
		pMark->m_strMarkName = FindFreeMarkName(pMark->m_strMarkName);
		pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
		break;

	case ID_NEW_FOLDMARK:
		pMark = (CFoldMark*) new CFoldMark;
		((CFoldMark*)pMark)->Create(theApp.m_colorDefTable);
		pMark->m_strMarkName = FindFreeMarkName(pMark->m_strMarkName);
		pMark->m_markTemplate.m_strPageID = pMark->m_strMarkName;
		break;
	}

	if ( ! pParentFrame->m_pDlgLayoutObjectProperties->m_hWnd)
		pParentFrame->m_pDlgLayoutObjectProperties->Create(pMark, GetPrintSheet(), this);
	else
		pParentFrame->m_pDlgLayoutObjectProperties->ReinitDialog(pMark, GetPrintSheet());

#ifdef PDF
	if (nMarkType == ID_NEW_CUSTOMMARK)
		pParentFrame->m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgCustomMarkContent.OnRegisterPDF();
#endif
}

CString CPrintSheetMarksView::FindFreeMarkName(CString strNewMark)
{
	CString strFinal = strNewMark;
	int nCount = 1;
	int i	   = 0;
	while (i < m_objectList.GetSize())
	{
		if ( ! m_objectList[i].m_pObjectTemplate)
		{
			i++;
			continue;
		}
		if (strFinal == m_objectList[i].m_pObjectTemplate->m_strPageID)	// mark name exists
		{
			strFinal.Format(_T("%s (%d)"), strNewMark, nCount++);
			i = 0;
		}
		else
			i++;
	}

	return strFinal;
}

void CPrintSheetMarksView::OnMarkDuplicate() 
{
	if ((m_nCurObjectSel < 0) || (m_nCurObjectSel >= m_objectList.GetSize()))
		return;
	if ( (m_objectList[m_nCurObjectSel].m_nMarkType != CMark::CustomMark) && (m_objectList[m_nCurObjectSel].m_nMarkType != CMark::TextMark) && (m_objectList[m_nCurObjectSel].m_nMarkType != CMark::BarcodeMark) && (m_objectList[m_nCurObjectSel].m_nMarkType != CMark::DrawingMark) )
		return;

	CLayoutObject* pObject			  = m_objectList[m_nCurObjectSel].m_pLayoutObject;
	CLayoutObject* pCounterPartObject = (pObject) ? pObject->FindCounterpartControlMark() : NULL;
	CLayoutSide*   pLayoutSide		  = (pObject) ? pObject->GetLayoutSide() : NULL;
	CLayoutSide*   pCounterPartSide	  = (pCounterPartObject) ? pCounterPartObject->GetLayoutSide() : NULL;
	if ( ! pLayoutSide)
		return;

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	float fXOffset = max(2 * min(pObject->GetWidth(), pObject->GetHeight()), 10.0f);
	float fYOffset = fXOffset;
	// Insert before modify, because Offset function needs pointer to layout side for invalidation
	POSITION pos = pLayoutSide->m_ObjectList.AddTail(*pObject);	 
	CLayoutObject& rNewLayoutObject = pLayoutSide->m_ObjectList.GetAt(pos); 
	rNewLayoutObject.Offset(fXOffset, fYOffset);
	if (pCounterPartSide)
	{
		pos = pCounterPartSide->m_ObjectList.AddTail(*pCounterPartObject);	 
		CLayoutObject& rNewCounterPartObject = pCounterPartSide->m_ObjectList.GetAt(pos); 
		rNewCounterPartObject.AlignToCounterPart(&rNewLayoutObject);
	}
	rNewLayoutObject.m_reflinePosParams.m_fDistanceX += fXOffset;
	rNewLayoutObject.m_reflinePosParams.m_fDistanceY += fYOffset;

	CImpManDoc* pDoc = GetDocument();
	if (pDoc)
	{
		pDoc->SetModifiedFlag();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
	}

	InitObjectList(&rNewLayoutObject);

	m_dlgLayoutObjectControl.ReinitDialog(GetCurrentLayoutObject(), GetPrintSheet());
	UpdateSheetPreview();
}

void CPrintSheetMarksView::OnMarkDelete() 
{
	if ( ! m_objectList.GetSize()) 
		return;
	if ( (m_nCurObjectSel < 0) || (m_nCurObjectSel >= m_objectList.GetSize()) )
		return;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return;
	if (AfxMessageBox(IDS_DELETE_SELECTIONS_WARNING, MB_YESNO) == IDNO)	
		return;

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	POSITION pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		POSITION prevPos = pos;
		CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
		if (IsSelected(&rObject))
		{
			CLayoutObject* pCounterPartObject = NULL;
			pCounterPartObject = rObject.FindCounterpartControlMark();
			pLayout->m_FrontSide.m_ObjectList.RemoveAt(prevPos);
			if (pCounterPartObject)
			{
				POSITION pos = pLayout->m_BackSide.m_ObjectList.GetHeadPosition();
				while (pos)
				{
					POSITION	   prevPos = pos;
					CLayoutObject& rObject = pLayout->m_BackSide.m_ObjectList.GetNext(pos);
					if (&rObject == pCounterPartObject)
						pLayout->m_BackSide.m_ObjectList.RemoveAt(prevPos);
				}
			}
		}
	}

	pos = pLayout->m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		POSITION prevPos = pos;
		CLayoutObject& rObject = pLayout->m_BackSide.m_ObjectList.GetNext(pos);
		if (IsSelected(&rObject))
			pLayout->m_BackSide.m_ObjectList.RemoveAt(prevPos);
	}

	InitObjectList();

	m_dlgLayoutObjectControl.m_pLayoutObject   = NULL;
	m_dlgLayoutObjectControl.m_pObjectTemplate = NULL;
	m_dlgLayoutObjectControl.ReinitDialog(GetCurrentLayoutObject(), GetPrintSheet());

	CPrintSheetFrame* pFrame = (CPrintSheetFrame*)GetParentFrame();
	if (pFrame)
	{
		if (pFrame->m_pDlgLayoutObjectProperties->m_hWnd)
		{
			pFrame->m_pDlgLayoutObjectProperties->m_pLayoutObject = NULL;
			pFrame->m_pDlgLayoutObjectProperties->m_pTemplate	  = NULL;
			pFrame->m_pDlgLayoutObjectProperties->ReinitDialog(GetCurrentLayoutObject(), GetPrintSheet());
		}
	}

	if ( ! m_objectList.GetSize())
	{
		GetDlgItem(IDC_MARK_DUPLICATE )->EnableWindow(FALSE);
		GetDlgItem(IDC_MARK_DELETE    )->EnableWindow(FALSE);
	}

	CImpManDoc* pDoc = GetDocument();
	if (pDoc)
	{
		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();

		pDoc->m_MarkSourceList.RemoveUnusedSources();
		pDoc->SetModifiedFlag();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////



void CPrintSheetMarksView::PostNcDestroy() 
{
	m_objectList.RemoveAll();  // List-contents must be deleted

   	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		pView->m_reflineXSpin.ShowWindow(SW_HIDE);
		pView->m_reflineYSpin.ShowWindow(SW_HIDE);
		pView->m_reflineDistanceEdit.ShowWindow(SW_HIDE);
	}

	CFormView::PostNcDestroy();
}

void CPrintSheetMarksView::UpdateSheetPreview(BOOL bUpdateMarkOnly)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;
	CRect clientRect;
	pView->GetClientRect(&clientRect);
    BOOL bZoom = ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) ) ? FALSE : TRUE;

	CSize  oldDocSize = pView->m_docSize;
	CPoint oldDocOrg  = pView->m_docOrg;
	if ( ! bZoom)
	{
		pView->CalcDocExtents();
		pView->CalcViewportExtents();
	}

	if (bUpdateMarkOnly && (oldDocSize == pView->m_docSize) && (oldDocOrg == pView->m_docOrg) )	// updatemarkonly only if docsize hasn't changed
	{
		pView->LockUpdateStatusBars();
		if (pView->m_bReflineUpdateEraseFront)
			pView->RedrawWindow(pView->m_reflineUpdateRectFront);
		else
			pView->RedrawWindow(pView->m_reflineUpdateRectFront, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
		if (pView->m_bReflineUpdateEraseBack)
			pView->RedrawWindow(pView->m_reflineUpdateRectBack);
		else
			pView->RedrawWindow(pView->m_reflineUpdateRectBack,  NULL, RDW_INVALIDATE | RDW_UPDATENOW);
		pView->UnlockUpdateStatusBars();
	}
	else
	{
		if ( ! bZoom)
		{
			pView->CalcScrollSizes();	
			pView->SetScrollSizes(MM_TEXT, pView->m_sizeTotal, pView->m_sizePage, pView->m_sizeLine);
		}
		pView->m_DisplayList.Invalidate();
		pView->Invalidate();
		pView->UpdateWindow();
	}
}

void CPrintSheetMarksView::OnDestroy() 
{
	m_dlgLayoutObjectControl.DataExchangeSave();

	m_pMarkSetImageList->DeleteImageList();
	delete m_pMarkSetImageList;

	m_pMarkImageList->DeleteImageList();
	delete m_pMarkImageList;

	//CPrintSheetFrame* pFrame = (CPrintSheetFrame*)GetParentFrame();
	//if (pFrame)
	//	if (pFrame->m_pDlgLayoutObjectProperties)
	//		if ( ! pFrame->m_pDlgLayoutObjectProperties->m_hWnd)
	//		{
	//			CDlgObjectPreviewWindow* pDlg = m_dlgLayoutObjectControl.GetObjectPreviewWindow();
	//			if (pDlg)
	//				pDlg->DestroyWindow();
	//		}

	CFormView::OnDestroy();
}


void CPrintSheetMarksView::OnOpenMarklib() 
{
	CPrintSheetFrame* pFrame = (CPrintSheetFrame*)GetParentFrame();
	if (pFrame)
	{
		if ( ! pFrame->m_pDlgMarkSet->m_hWnd)
			pFrame->m_pDlgMarkSet->Create(IDD_MARKSET, this);
		else
		{
			pFrame->m_pDlgMarkSet->OnClose();
			if (pFrame->m_pDlgMarkSet->m_hWnd)	// close rejected
				((CButton*)GetDlgItem(IDC_OPEN_MARKLIB))->SetCheck(1);
		}
	}
}

void CPrintSheetMarksView::OnApplyButton() 
{
	m_dlgLayoutObjectControl.Apply();	

	CPrintSheetFrame* pFrame = (CPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;

	if (pFrame->m_pDlgLayoutObjectProperties->m_hWnd)
		if (IsSelected( pFrame->m_pDlgLayoutObjectProperties->m_pLayoutObject ))
			pFrame->m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.DataExchangeLoad();
}

void CPrintSheetMarksView::OnCloseMarksView() 
{
	CPrintSheetFrame* pFrame = (CPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return;

	pFrame->OnSetMark();
}
