// ToolBarGroup.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "ToolBarGroup.h"
#include "GraphicComponent.h"


// CToolBarGroup

IMPLEMENT_DYNAMIC(CToolBarGroup, CStatic)

CToolBarGroup::CToolBarGroup()
{
	m_font.CreateFont (12, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CToolBarGroup::~CToolBarGroup()
{
	m_font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CToolBarGroup, CStatic)
	ON_WM_DRAWITEM()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CToolBarGroup-Meldungshandler

void CToolBarGroup::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect rcFrame = lpDrawItemStruct->rcItem;
//	rcFrame.top += 10;
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	pDC->SelectObject(&pen);
	pDC->SelectStockObject(HOLLOW_BRUSH);//HOLLOW_BRUSH);
	pDC->RoundRect(rcFrame, CPoint(8, 8));
	pen.DeleteObject();

	CString strText;
	GetWindowText(strText);

	CRect rcHeader = rcFrame; rcHeader.bottom = rcHeader.top + 15;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, strText, WHITE, RGB(179,207,252), 90);
}

BOOL CToolBarGroup::OnEraseBkgnd(CDC* pDC)
{
	//CRect rcRect;
	//GetClientRect(rcRect);
	//pDC->FillSolidRect(rcRect, GUICOLOR_GROUP);

	return TRUE;
}


// CToolBarGroup-Meldungshandler



BOOL CToolBarGroup::PreTranslateMessage(MSG* pMsg)
{
	CWnd* pWnd = GetParent();
	if (pWnd)
		pWnd->UpdateDialogControls( pWnd, FALSE );

	return CStatic::PreTranslateMessage(pMsg);
}
