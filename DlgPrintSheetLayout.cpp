// DlgPrintSheetLayout.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "DlgImpositionScheme.h"
#include "PrintSheetFrame.h"
#include "PrintSheetView.h"
#include "DlgPressDevice.h"
#include "DlgSheet.h"
#include "DlgPrintSheetLayout.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPrintSheetLayout 


CDlgPrintSheetLayout::CDlgPrintSheetLayout(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPrintSheetLayout::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPrintSheetLayout)
	m_nPrintingType = -1;
	m_nTurningType = -1;
	m_strPaperFormat = _T("");
	m_strPlateFormat = _T("");
	m_strPrintLayout = _T("");
	m_strPrintMachine = _T("");
	m_strPrintMarkset = _T("");
	//}}AFX_DATA_INIT

	m_smallFont.CreateFont (14, 0, 0, 0, FW_BOLD,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_BkBrush.CreateSolidBrush(RGB(252, 220, 150));
}


void CDlgPrintSheetLayout::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPrintSheetLayout)
	DDX_CBIndex(pDX, IDC_PRINTINGTYPE_COMBO, m_nPrintingType);
	DDX_CBIndex(pDX, IDC_TURNINGTYPE_COMBO, m_nTurningType);
	DDX_Text(pDX, IDC_PP_PAPERFORMAT, m_strPaperFormat);
	DDX_Text(pDX, IDC_PP_PLATEFORMAT, m_strPlateFormat);
	DDX_Text(pDX, IDC_PP_PRINTLAYOUT, m_strPrintLayout);
	DDX_Text(pDX, IDC_PP_PRINTMACHINE, m_strPrintMachine);
	DDX_Text(pDX, IDC_PRINTMARKSET, m_strPrintMarkset);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPrintSheetLayout, CDialog)
	//{{AFX_MSG_MAP(CDlgPrintSheetLayout)
	ON_BN_CLICKED(IDC_PP_PRINTMACHINE_BUTTON, OnPpPrintmachineButton)
	ON_BN_CLICKED(IDC_PP_PAPER_BUTTON, OnPpPaperButton)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_PRINTMARKSET, OnPrintmarkset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPrintSheetLayout 

BOOL CDlgPrintSheetLayout::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	GetDlgItem(IDC_LAYOUT_PREVIEW_FRAME)->GetWindowRect(m_layoutPreviewFrame);
	ScreenToClient(m_layoutPreviewFrame);

	if (m_pressDevice.m_strName.IsEmpty())
	{
		POSITION pos  = theApp.m_PressDeviceList.FindIndex(0);
		m_pressDevice = theApp.m_PressDeviceList.GetAt(pos);
	}

	m_printSheet.LinkLayout(&m_layout);

	UpdatePrintSheetControls();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgPrintSheetLayout::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	DrawPrintSheetLayout(&dc);
}

void CDlgPrintSheetLayout::DrawPrintSheetLayout(CDC* pDC)
{
	pDC->FillSolidRect(m_layoutPreviewFrame, RGB(144, 153, 174));

	m_layout.Draw(pDC, m_layoutPreviewFrame, &m_printSheet, &m_pressDevice);
}

HBRUSH CDlgPrintSheetLayout::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
    if(nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_PP_PRINTLAYOUT:
				pDC->SelectObject(&m_smallFont);
				pDC->SetBkMode(TRANSPARENT);
				hbr = (HBRUSH)m_BkBrush.GetSafeHandle();
				break;

		default:break;
		}
    }

	return hbr;
}

void CDlgPrintSheetLayout::UpdatePrintSheetControls()
{
	m_strPaperFormat.Format(_T("%.1f x %.1f"), m_layout.m_FrontSide.m_fPaperWidth, m_layout.m_FrontSide.m_fPaperHeight);
	m_strPlateFormat.Format(_T("%.1f x %.1f"), m_pressDevice.m_fPlateWidth, m_pressDevice.m_fPlateHeight);
	m_strPrintLayout	= m_layout.m_strLayoutName;
	m_strPrintMachine	= m_pressDevice.m_strName;
	m_strPrintMarkset	= "";
	m_nPrintingType		= 0;
	m_nTurningType		= 0;

	UpdateData(FALSE);
}

void CDlgPrintSheetLayout::OnPpPrintmachineButton() 
{
	CDlgPressDevice dlg(FALSE);  // don't modify direct inside dialog

	if (dlg.DoModal() == IDCANCEL)
		return;

	CPressDevice oldPressDevice;
	oldPressDevice = m_pressDevice;
	m_pressDevice  = dlg.m_pressDevice;
	float fGripper = CDlgPressDevice::CheckGripper(&oldPressDevice, &m_pressDevice, &m_layout.m_FrontSide);

	m_layout.m_FrontSide.PositionLayoutSide(fGripper, &m_pressDevice);
	m_layout.m_BackSide.PositionLayoutSide (fGripper, &m_pressDevice);

	UpdatePrintSheetControls();
	InvalidateRect(m_layoutPreviewFrame);
	UpdateWindow();
}

void CDlgPrintSheetLayout::OnPpPaperButton() 
{
	CDlgSheet dlg(FALSE);  // don't modify direct inside dialog

	if (dlg.DoModal() == IDCANCEL)
		return;

	if ( (dlg.m_fWidth > m_pressDevice.m_fPlateWidth) || (dlg.m_fHeight > m_pressDevice.m_fPlateHeight))
		if (AfxMessageBox(IDS_SHEET_GREATER_PLATE , MB_YESNO|MB_ICONSTOP) == IDNO)
			return;

	m_layout.m_FrontSide.m_fPaperWidth		 = dlg.m_fWidth;
	m_layout.m_FrontSide.m_fPaperHeight		 = dlg.m_fHeight;
	m_layout.m_FrontSide.m_nPaperOrientation = (unsigned char)dlg.m_nPageOrientation;
	m_layout.m_FrontSide.m_strFormatName	 = dlg.m_strName;
	m_layout.m_FrontSide.m_bAdjustFanout	 = dlg.m_bFanoutCheck;
	m_layout.m_FrontSide.m_FanoutList		 = dlg.m_FanoutList;
	m_layout.m_FrontSide.PositionLayoutSide(FLT_MAX, &m_pressDevice);

	m_layout.m_BackSide.m_fPaperWidth		 = dlg.m_fWidth;
	m_layout.m_BackSide.m_fPaperHeight		 = dlg.m_fHeight;
	m_layout.m_BackSide.m_nPaperOrientation  = (unsigned char)dlg.m_nPageOrientation;
	m_layout.m_BackSide.m_strFormatName		 = dlg.m_strName;
	m_layout.m_BackSide.m_bAdjustFanout		 = dlg.m_bFanoutCheck;
	m_layout.m_BackSide.m_FanoutList		 = dlg.m_FanoutList;
	m_layout.m_BackSide.PositionLayoutSide(FLT_MAX, &m_pressDevice);

	UpdatePrintSheetControls();
	InvalidateRect(m_layoutPreviewFrame);
	UpdateWindow();
}

void CDlgPrintSheetLayout::OnPrintmarkset() 
{
	CPrintSheetFrame* pFrame = (CPrintSheetFrame*)GetParentFrame();
	if (pFrame)
	{
		if ( ! pFrame->m_pDlgMarkSet->m_hWnd)
			pFrame->m_pDlgMarkSet->Create(IDD_MARKSET, this);
		else
		{
			pFrame->m_pDlgMarkSet->OnClose();
			if (pFrame->m_pDlgMarkSet->m_hWnd)	// close rejected
				((CButton*)GetDlgItem(IDC_OPEN_MARKLIB))->SetCheck(1);
		}
	}
}
