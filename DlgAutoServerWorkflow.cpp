// DlgAutoServerWorkflow.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgAutoServerWorkflow.h"
#include "DlgProductionSetups.h"
#include "DlgProductionManager.h"


// CDlgAutoServerWorkflow-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgAutoServerWorkflow, CDialog)

CDlgAutoServerWorkflow::CDlgAutoServerWorkflow(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgAutoServerWorkflow::IDD, pParent)
	, m_strName(_T(""))
	, m_nJobdataSource(0)
	, m_strProductionProfile(_T(""))
	, m_nPDFPreviewSettings(0)
	, m_nDataOutputPrintSheets(0)
	, m_nDataOutputFoldSheets(0)
	, m_nDataOutputBooklets(0)
	, m_nDataOutputResults(0)
	, m_nActionImpose(0)
	, m_nActionWhenFinished(0)
	, m_nActionDelay(0)
	, m_nActionDelayUnit(0)
{

}

CDlgAutoServerWorkflow::~CDlgAutoServerWorkflow()
{
}

void CDlgAutoServerWorkflow::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_WF_NAME, m_strName);
	DDX_CBIndex(pDX, IDC_WF_JOBDATA_SOURCE, m_nJobdataSource);
	DDX_CBString(pDX, IDC_WF_PRODUCTION_PROFILE, m_strProductionProfile);
	DDX_Control(pDX, IDC_WF_PRODUCTION_PROFILE, m_productionProfileCombo);
	DDX_CBIndex(pDX, IDC_WF_PDF_PREVIEW_SETTINGS, m_nPDFPreviewSettings);
	DDX_CBIndex(pDX, IDC_WF_DATAOUTPUT_PRINTSHEETS, m_nDataOutputPrintSheets);
	DDX_CBIndex(pDX, IDC_WF_DATAOUTPUT_FOLDSHEETS, m_nDataOutputFoldSheets);
	DDX_CBIndex(pDX, IDC_WF_DATAOUTPUT_BOOKLETS, m_nDataOutputBooklets);
	DDX_CBIndex(pDX, IDC_WF_DATAOUTPUT_RESULTS, m_nDataOutputResults);
	DDX_CBIndex(pDX, IDC_WF_ACTION_IMPOSE, m_nActionImpose);
	DDX_CBIndex(pDX, IDC_WF_ACTION_WHEN_FINISHED, m_nActionWhenFinished);
	DDX_Text(pDX, IDC_WF_ACTION_DELAY_EDIT, m_nActionDelay);
	DDX_CBIndex(pDX, IDC_WF_ACTION_DELAY_UNIT, m_nActionDelayUnit);
}


BEGIN_MESSAGE_MAP(CDlgAutoServerWorkflow, CDialog)
	ON_BN_CLICKED(IDC_WF_PRODUCTION_PROFILE_BUTTON, &CDlgAutoServerWorkflow::OnBnClickedWfProductionProfileButton)
	ON_BN_CLICKED(IDOK, &CDlgAutoServerWorkflow::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_WF_ACTION_WHEN_FINISHED, &CDlgAutoServerWorkflow::OnCbnSelchangeWfActionFinished)
END_MESSAGE_MAP()


// CDlgAutoServerWorkflow-Meldungshandler

BOOL CDlgAutoServerWorkflow::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_nDBAttribute & kimAutoDB::DataOutput_PagePreview36)
		m_nPDFPreviewSettings = 1;
	else
	if (m_nDBAttribute & kimAutoDB::DataOutput_PagePreview72)
		m_nPDFPreviewSettings = 2;
	else
	if (m_nDBAttribute & kimAutoDB::DataOutput_PagePreview144)
		m_nPDFPreviewSettings = 3;
	else
		m_nPDFPreviewSettings = 0;

	m_nDataOutputPrintSheets = (m_nDBAttribute & kimAutoDB::DataOutput_PrintSheets)			   ? 1 : 0;
	m_nDataOutputFoldSheets  = (m_nDBAttribute & kimAutoDB::DataOutput_FoldSheets)			   ? 1 : 0;
	m_nDataOutputBooklets	 = (m_nDBAttribute & kimAutoDB::DataOutput_Booklets)			   ? 1 : 0;
	m_nDataOutputResults	 = (m_nDBAttribute & kimAutoDB::DataOutput_ResultsBackToSourceXML) ? 1 : 0;
	m_nActionImpose			 = (m_nDBAttribute & kimAutoDB::Action_ImposeFoldSheetsOnly)	   ? 2 : ( (m_nDBAttribute & kimAutoDB::Action_ImposeByProfile) ? 1 : 0);

	switch (m_nDBActionWhenFinished)
	{
	case kimAutoDB::ActionNothing:		m_nActionWhenFinished = 0;	m_nActionDelay = 0;	break;
	case kimAutoDB::ActionDeleteJob:	m_nActionWhenFinished = 1;	
										if (m_nDBActionDelayMinutes == 0)
										{
											m_nActionDelay = 0;
											m_nActionDelayUnit = 0;
										}
										else
											if ( (m_nDBActionDelayMinutes % 1440) == 0)
											{
												m_nActionDelay = m_nDBActionDelayMinutes / 1440;
												m_nActionDelayUnit = 2;
											}
											else
												if ( (m_nDBActionDelayMinutes % 60) == 0)
												{
													m_nActionDelay = m_nDBActionDelayMinutes / 60;
													m_nActionDelayUnit = 1;
												}
												else
												{
													m_nActionDelay = m_nDBActionDelayMinutes;
													m_nActionDelayUnit = 0;
												}
										break;
	}


	if (m_strProductionProfile.IsEmpty())
		m_strProductionProfile = _T("-");

	CDlgProductionManager::LoadProfileList();
	m_productionProfileCombo.AddString(_T("-"));
	POSITION pos = theApp.m_productionProfiles.GetHeadPosition();
	while (pos)                        
	{
		CProductionProfile& rProfile = theApp.m_productionProfiles.GetAt(pos);
		m_productionProfileCombo.AddString(rProfile.m_strName);
		theApp.m_productionProfiles.GetNext(pos);
	}
	int nSel = m_productionProfileCombo.FindStringExact(-1, m_strProductionProfile);
	if ( (nSel < 0) && (m_productionProfileCombo.GetCount() > 0) )
		nSel = 0;
	m_productionProfileCombo.SetCurSel(nSel);
	if (nSel != CB_ERR)
		m_productionProfileCombo.GetLBText(nSel, m_strProductionProfile);

	if (m_nActionWhenFinished == 0)	
	{
		GetDlgItem(IDC_WF_ACTION_DELAY_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WF_ACTION_DELAY_EDIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WF_ACTION_DELAY_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WF_ACTION_DELAY_UNIT)->ShowWindow(SW_HIDE);
	}
	((CSpinButtonCtrl*)GetDlgItem(IDC_WF_ACTION_DELAY_SPIN))->SetRange(0, 10000);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgAutoServerWorkflow::OnBnClickedWfProductionProfileButton()
{
	UpdateData(TRUE);

	CDlgProductionManager dlg;
	dlg.m_productionProfile.m_strName = m_strProductionProfile;

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_strProductionProfile = dlg.m_strProfileName;

	m_productionProfileCombo.ResetContent();
	m_productionProfileCombo.AddString(_T("-"));
	POSITION pos = theApp.m_productionProfiles.GetHeadPosition();
	while (pos)                        
	{
		CProductionProfile& rProfile = theApp.m_productionProfiles.GetAt(pos);
		m_productionProfileCombo.AddString(rProfile.m_strName);
		theApp.m_productionProfiles.GetNext(pos);
	}
	int nSel = m_productionProfileCombo.FindStringExact(-1, m_strProductionProfile);
	if ( (nSel < 0) && (m_productionProfileCombo.GetCount() > 0) )
		nSel = 0;
	m_productionProfileCombo.SetCurSel(nSel);
	if (nSel != CB_ERR)
		m_productionProfileCombo.GetLBText(nSel, m_strProductionProfile);

	UpdateData(FALSE);
}

void CDlgAutoServerWorkflow::OnCbnSelchangeWfActionFinished()
{
	UpdateData(TRUE);

	if (m_nActionWhenFinished == 0)	
	{
		GetDlgItem(IDC_WF_ACTION_DELAY_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WF_ACTION_DELAY_EDIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WF_ACTION_DELAY_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_WF_ACTION_DELAY_UNIT)->ShowWindow(SW_HIDE);
	}
	else
	{
		GetDlgItem(IDC_WF_ACTION_DELAY_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_WF_ACTION_DELAY_EDIT)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_WF_ACTION_DELAY_SPIN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_WF_ACTION_DELAY_UNIT)->ShowWindow(SW_SHOW);
	}
}

void CDlgAutoServerWorkflow::OnBnClickedOk()
{
	UpdateData(TRUE);

	m_nDBAttribute = 0;
	switch (m_nPDFPreviewSettings)
	{
	case 0:	break;
	case 1: m_nDBAttribute = kimAutoDB::DataOutput_PagePreview36;	break;
	case 2: m_nDBAttribute = kimAutoDB::DataOutput_PagePreview72;	break;
	case 3: m_nDBAttribute = kimAutoDB::DataOutput_PagePreview144;	break;
	}

	m_nDBAttribute = (m_nDataOutputPrintSheets == 1) ? m_nDBAttribute | kimAutoDB::DataOutput_PrintSheets			 : m_nDBAttribute;
	m_nDBAttribute = (m_nDataOutputFoldSheets == 1)  ? m_nDBAttribute | kimAutoDB::DataOutput_FoldSheets			 : m_nDBAttribute;
	m_nDBAttribute = (m_nDataOutputBooklets == 1)	 ? m_nDBAttribute | kimAutoDB::DataOutput_Booklets				 : m_nDBAttribute;
	m_nDBAttribute = (m_nDataOutputResults == 1)	 ? m_nDBAttribute | kimAutoDB::DataOutput_ResultsBackToSourceXML : m_nDBAttribute;
	m_nDBAttribute = (m_nActionImpose == 2)			 ? m_nDBAttribute | kimAutoDB::Action_ImposeFoldSheetsOnly		 : ( (m_nActionImpose == 1) ? m_nDBAttribute | kimAutoDB::Action_ImposeByProfile : m_nDBAttribute | kimAutoDB::Action_ImposeByInput);

	switch (m_nActionWhenFinished)
	{
	case 0: m_nDBActionWhenFinished = kimAutoDB::ActionNothing;	m_nDBActionDelayMinutes = 0; break;
	case 1: m_nDBActionWhenFinished = kimAutoDB::ActionDeleteJob;		
			switch (m_nActionDelayUnit)
			{
			case 0:	m_nDBActionDelayMinutes = m_nActionDelay;			break;
			case 1: m_nDBActionDelayMinutes = m_nActionDelay * 60;		break;
			case 2:	m_nDBActionDelayMinutes = m_nActionDelay * 1440;	break;
			}
			break;
	}

	UpdateData(FALSE);

	OnOK();
}
