#if !defined(AFX_DLGSELECTSECTIONMARK_H__B352AF71_DB2B_11D3_878E_204C4F4F5020__INCLUDED_)
#define AFX_DLGSELECTSECTIONMARK_H__B352AF71_DB2B_11D3_878E_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSelectSectionMark.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSelectSectionMark 

class CDlgSelectSectionMark : public CDialog
{
// Konstruktion
public:
	CDlgSelectSectionMark(CWnd* pParent = NULL);   // Standardkonstruktor

public:
	CDialog*	 m_pParent;
protected:
	CToolBarCtrl m_toolBar;
	TBBUTTON*	 m_pTBButtons;
	int			 m_nSectionMarkTypeID;


// Dialogfelddaten
	//{{AFX_DATA(CDlgSelectSectionMark)
	enum { IDD = IDD_SELECT_SECTION_MARK };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgSelectSectionMark)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:


protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgSelectSectionMark)
	virtual BOOL OnInitDialog();
	afx_msg void OnSectionmarkType1();
	afx_msg void OnSectionmarkType2();
	afx_msg void OnSectionmarkType3();
	afx_msg void OnSectionmarkType4();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGSELECTSECTIONMARK_H__B352AF71_DB2B_11D3_878E_204C4F4F5020__INCLUDED_
