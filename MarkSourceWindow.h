/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMarkSourceWindow window

#pragma once


class CMarkSourceWindow : public CStatic
{
// Construction
public:
	CMarkSourceWindow();

// Attributes
public:
	CDisplayList m_DisplayList;
	DECLARE_DISPLAY_ITEM(CMarkSourceWindow, PageSource,		  OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CMarkSourceWindow, PageSourceHeader, OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)

	CPageSource* m_pMarkSource;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMarkSourceWindow)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMarkSourceWindow();

	// Generated message map functions
//Must have have access from CDlgSetMarks to deselect selections in MarkSourceWindow
//protected: 
	//{{AFX_MSG(CMarkSourceWindow)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
