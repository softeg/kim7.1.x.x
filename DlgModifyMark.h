#pragma once

#include "DlgLayoutObjectControl.h"


// CDlgModifyMark-Dialogfeld

class CDlgModifyMark : public CDialog
{
	DECLARE_DYNAMIC(CDlgModifyMark)

public:
	CDlgModifyMark(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgModifyMark();

// Dialogfelddaten
	enum { IDD = IDD_MODIFY_MARK };

public:
	CString					m_strTitle;
	CMark*					m_pMark;
	CPrintSheet*			m_pPrintSheet;
	CDlgLayoutObjectControl	m_dlgLayoutObjectControl;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
public:
	afx_msg void OnBnClickedOk();
};
