// DlgFoldMarkGeometry.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgFoldMarkGeometry 


CDlgFoldMarkGeometry::CDlgFoldMarkGeometry(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgFoldMarkGeometry::IDD, pParent)
	, m_bSetMarksBetween(FALSE)
{
	//{{AFX_DATA_INIT(CDlgFoldMarkGeometry)
	m_fLineDistance = 0.0f;
	m_fLineLength = 0.0f;
	m_fLineWidth = 0.0f;
	m_fCrossHeight = 0.0f;
	m_fCrossWidth = 0.0f;
	m_fCrosslineWidth = 0.0f;
	m_fMinDistance = 0.0f;
	//}}AFX_DATA_INIT

	m_pParent  = NULL;
}

CDlgFoldMarkGeometry::~CDlgFoldMarkGeometry()
{
}

BOOL CDlgFoldMarkGeometry::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}	

void CDlgFoldMarkGeometry::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgFoldMarkGeometry)
	DDX_Control(pDX, IDC_FOLDMARKS_CROSSLINEWIDTH_SPIN, m_crosslineWidthSpin);
	DDX_Control(pDX, IDC_FOLDMARKS_CROSSWIDTH_SPIN, m_crossWidthSpin);
	DDX_Control(pDX, IDC_FOLDMARKS_CROSSHEIGHT_SPIN, m_crossHeightSpin);
	DDX_Control(pDX, IDC_FOLDMARKS_LINEWIDTH_SPIN, m_lineWidthSpin);
	DDX_Control(pDX, IDC_FOLDMARKS_LINELENGTH_SPIN, m_lineLengthSpin);
	DDX_Control(pDX, IDC_FOLDMARKS_DISTANCE_SPIN, m_lineDistanceSpin);
	DDX_Measure(pDX, IDC_FOLDMARKS_DISTANCE, m_fLineDistance);
	DDX_Measure(pDX, IDC_FOLDMARKS_LINELENGTH, m_fLineLength);
	DDX_Measure(pDX, IDC_FOLDMARKS_LINEWIDTH, m_fLineWidth);
	DDX_Measure(pDX, IDC_FOLDMARKS_CROSSHEIGHT, m_fCrossHeight);
	DDX_Measure(pDX, IDC_FOLDMARKS_CROSSWIDTH, m_fCrossWidth);
	DDX_Measure(pDX, IDC_FOLDMARKS_CROSSLINEWIDTH, m_fCrosslineWidth);
	DDX_Control(pDX, IDC_FOLDMARKS_MINDISTANCE_SPIN, m_minDistanceSpin);
	DDX_Measure(pDX, IDC_FOLDMARKS_MINDISTANCE, m_fMinDistance);
	DDX_Check(pDX, IDC_FOLDMARKS_BETWEEN, m_bSetMarksBetween);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgFoldMarkGeometry, CDialog)
	//{{AFX_MSG_MAP(CDlgFoldMarkGeometry)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FOLDMARKS_DISTANCE_SPIN, OnDeltaposFoldmarksDistanceSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FOLDMARKS_LINELENGTH_SPIN, OnDeltaposFoldmarksLinelengthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FOLDMARKS_LINEWIDTH_SPIN, OnDeltaposFoldmarksLinewidthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FOLDMARKS_CROSSHEIGHT_SPIN, OnDeltaposFoldmarksCrossheightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FOLDMARKS_CROSSWIDTH_SPIN, OnDeltaposFoldmarksCrosswidthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FOLDMARKS_CROSSLINEWIDTH_SPIN, OnDeltaposFoldmarksCrosslinewidthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FOLDMARKS_MINDISTANCE_SPIN, OnDeltaposFoldmarksMinDistanceSpin)
	ON_BN_CLICKED(IDC_FOLDMARKS_BETWEEN, OnFoldmarksBetween)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgFoldMarkGeometry 

BOOL CDlgFoldMarkGeometry::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_pParent = (CDlgLayoutObjectControl*)GetParent();
	
	m_lineDistanceSpin.SetRange  (0, 1);
	m_lineLengthSpin.SetRange    (0, 1);
	m_lineWidthSpin.SetRange     (0, 1);
	m_crossHeightSpin.SetRange   (0, 1);
	m_crossWidthSpin.SetRange	 (0, 1);
	m_crosslineWidthSpin.SetRange(0, 1);
	m_minDistanceSpin.SetRange	 (0, 1);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgFoldMarkGeometry::OnDeltaposFoldmarksDistanceSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_lineDistanceSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgFoldMarkGeometry::OnDeltaposFoldmarksLinelengthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_lineLengthSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgFoldMarkGeometry::OnDeltaposFoldmarksLinewidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_lineWidthSpin.GetBuddy(), pNMUpDown->iDelta, 0.1f);
	
	*pResult = 1;
}

void CDlgFoldMarkGeometry::OnDeltaposFoldmarksCrossheightSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_crossHeightSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgFoldMarkGeometry::OnDeltaposFoldmarksCrosswidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_crossWidthSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgFoldMarkGeometry::OnDeltaposFoldmarksCrosslinewidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_crosslineWidthSpin.GetBuddy(), pNMUpDown->iDelta, 0.1f);
	
	*pResult = 1;
}

void CDlgFoldMarkGeometry::OnDeltaposFoldmarksMinDistanceSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_minDistanceSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgFoldMarkGeometry::OnFoldmarksBetween() 
{
	Apply();

	m_pParent->UpdateView();
}

void CDlgFoldMarkGeometry::LoadData()
{
	if ( ! m_hWnd)
		return;

	CMark* pMark = m_pParent->GetFirstMark();
	while (pMark)
	{
		if (pMark->m_nType == CMark::FoldMark)
		{
			CFoldMark* pFoldMark = (CFoldMark*)pMark;
			m_fLineDistance		= pFoldMark->m_fLineDistance;
			m_fLineWidth		= pFoldMark->m_fLineWidth;
			m_fLineLength		= pFoldMark->m_fLineLength;
			m_fCrossWidth		= pFoldMark->m_fCrossWidth;
			m_fCrossHeight		= pFoldMark->m_fCrossHeight;
			m_fCrosslineWidth	= pFoldMark->m_fCrosslineWidth;
			m_fMinDistance		= pFoldMark->m_fMinGap;
			m_bSetMarksBetween	= pFoldMark->m_bSetMarksBetween;
		}

		pMark = m_pParent->GetNextMark(pMark);
	}

	UpdateData(FALSE);
}

void CDlgFoldMarkGeometry::SaveData()
{
	if ( ! m_hWnd)
		return;

	UpdateData(TRUE);

	CMark* pMark = m_pParent->GetFirstMark();
	while (pMark)
	{
		if (pMark->m_nType == CMark::FoldMark)
		{
			CFoldMark* pFoldMark = (CFoldMark*)pMark;
			pFoldMark->m_fLineDistance		= m_fLineDistance;
			pFoldMark->m_fLineWidth			= m_fLineWidth;
			pFoldMark->m_fLineLength		= m_fLineLength;
			pFoldMark->m_fCrossWidth		= m_fCrossWidth;
			pFoldMark->m_fCrossHeight		= m_fCrossHeight;
			pFoldMark->m_fCrosslineWidth	= m_fCrosslineWidth;
			pFoldMark->m_fMinGap			= m_fMinDistance;
			pFoldMark->m_bSetMarksBetween	= m_bSetMarksBetween;

			int	nIndex = pMark->m_MarkSource.m_strSystemCreationInfo.Find(_T(" "));
			if (nIndex != -1)	// invalid string
			{
				CString strContentType = pMark->m_MarkSource.m_strSystemCreationInfo.Left(nIndex);
				CString strParams	   = pMark->m_MarkSource.m_strSystemCreationInfo.Right(pMark->m_MarkSource.m_strSystemCreationInfo.GetLength() - nIndex);
				strParams.TrimLeft(); strParams.TrimRight();

				CFoldMark* pFoldMark = (CFoldMark*)pMark;
				pFoldMark->m_MarkSource.m_nType = CPageSource::SystemCreated;

				if (strContentType == "$FOLD_MARK")
					pFoldMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$FOLD_MARK %s %s %s"),	(pFoldMark->m_nLinetype == CFoldMark::Solid)	   ? _T("/Solid")		: _T("/Dashed"),   
																										(pFoldMark->m_nBkMode   == CFoldMark::Transparent) ? _T("/Transparent") : _T("/Opaque"),
																										(pFoldMark->m_nZOrder   == CFoldMark::Background)  ? _T("/Background")  : _T("/Foreground") );
			}
		}

		pMark = m_pParent->GetNextMark(pMark);
	}
}

void CDlgFoldMarkGeometry::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}
