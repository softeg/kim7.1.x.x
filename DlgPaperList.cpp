// DlgPaperList.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgPaperList.h"
#include "InplaceEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPaperList 

IMPLEMENT_DYNCREATE(CDlgPaperList, CDialog)


CDlgPaperList::CDlgPaperList(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaperList::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPaperList)
	//}}AFX_DATA_INIT
}


void CDlgPaperList::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPaperList)
	DDX_Control(pDX, IDC_PAPERLIST_TABLE, m_paperListTable);
	//}}AFX_DATA_MAP
}
 

BEGIN_MESSAGE_MAP(CDlgPaperList, CDialog)
	//{{AFX_MSG_MAP(CDlgPaperList)
	ON_NOTIFY(NM_CLICK, IDC_PAPERLIST_TABLE, OnClickPaperlistTable)
	ON_NOTIFY(NM_DBLCLK, IDC_PAPERLIST_TABLE, OnDblclkPaperlistTable)
	ON_BN_CLICKED(IDC_ADD_PAPERENTRY, OnAddPaperentry)
	ON_BN_CLICKED(IDC_REMOVE_PAPERENTRY, OnRemovePaperentry)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP() 

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPaperList  

BOOL CDlgPaperList::OnInitDialog() 
{
	CDialog::OnInitDialog(); 

	ListView_SetExtendedListViewStyle(m_paperListTable.m_hWnd, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	CString string;
	string.LoadString(IDS_PAPERLIST_TYPE);
	m_paperListTable.InsertColumn(0, string, LVCFMT_LEFT, 150, 0);
	string.LoadString(IDS_PAPERLIST_FORMAT);
	m_paperListTable.InsertColumn(1, string, LVCFMT_LEFT,  80, 0);
	string.LoadString(IDS_PAPERLIST_FORMS);
	m_paperListTable.InsertColumn(2, string, LVCFMT_LEFT,  50, 0);
	string.LoadString(IDS_PAPERLIST_QUANTITY);
	m_paperListTable.InsertColumn(3, string, LVCFMT_LEFT,  70, 0);
	string.LoadString(IDS_PAPERLIST_EXTRA);
	m_paperListTable.InsertColumn(4, string, LVCFMT_LEFT,  70, 0);
	string.LoadString(IDS_PAPERLIST_TOTAL);
	m_paperListTable.InsertColumn(5, string, LVCFMT_LEFT,  70, 0);
	string.LoadString(IDS_PAPERLIST_COMMENT);
	m_paperListTable.InsertColumn(6, string, LVCFMT_LEFT, 190, 0);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		m_paperList.RemoveAll();
		m_paperList.Append(pDoc->m_productionData.m_paperList);
	}

	//LVITEM lvitem;
	//for (int i = 0; i < m_paperList.GetSize(); i++)
	//{
	//	lvitem.iItem	= m_paperListTable.GetItemCount();
	//	lvitem.iSubItem = 0;
	//	lvitem.mask	    = LVIF_TEXT;
	//	lvitem.pszText  = m_paperList[i].m_strName.GetBuffer(MAX_TEXT);		
	//	m_paperListTable.InsertItem(&lvitem); 

	//	lvitem.iSubItem = 1;
	//	lvitem.pszText  = m_paperList[i].m_strFormat.GetBuffer(MAX_TEXT);
	//	m_paperListTable.SetItem(&lvitem);

	//	string.Format(_T("%3d"), m_paperList[i].m_nNumSheets);
	//	lvitem.iSubItem = 2;
	//	lvitem.pszText  = string.GetBuffer(MAX_TEXT);
	//	m_paperListTable.SetItem(&lvitem);

	//	string.Format(_T("%8d"), m_paperList[i].m_nQuantity);
	//	lvitem.iSubItem = 3;
	//	lvitem.pszText  = string.GetBuffer(MAX_TEXT);
	//	m_paperListTable.SetItem(&lvitem);

	//	string.Format(_T("%8d"), m_paperList[i].m_nExtra);
	//	lvitem.iSubItem = 4;
	//	lvitem.pszText  = string.GetBuffer(MAX_TEXT);
	//	m_paperListTable.SetItem(&lvitem);

	//	string.Format(_T("%8d"), m_paperList[i].m_nTotal);
	//	lvitem.iSubItem = 5;
	//	lvitem.pszText  = string.GetBuffer(MAX_TEXT);
	//	m_paperListTable.SetItem(&lvitem);

	//	lvitem.iSubItem = 6;
	//	lvitem.pszText  = m_paperList[i].m_strComment.GetBuffer(MAX_TEXT);
	//	m_paperListTable.SetItem(&lvitem);
	//}

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgPaperList::OnClickPaperlistTable(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if ( ! m_dlgPaperListEntry.m_hWnd)
		m_dlgPaperListEntry.Create(IDD_PAPERLIST_ENTRY, this);
	else
		if ( ! m_dlgPaperListEntry.Position())
			m_dlgPaperListEntry.OnOK();
	
	*pResult = 0;
}

void CDlgPaperList::OnDblclkPaperlistTable(NMHDR* pNMHDR, LRESULT* pResult) 
{
	if ( ! m_dlgPaperListEntry.m_hWnd)
		m_dlgPaperListEntry.Create(IDD_PAPERLIST_ENTRY, this);
	else
		if ( ! m_dlgPaperListEntry.Position())
			m_dlgPaperListEntry.OnOK();
	
	*pResult = 0;
}

BOOL CDlgPaperList::PreTranslateMessage(MSG* pMsg) 
{
	if (m_dlgPaperListEntry.m_hWnd)
		if ( (pMsg->message == WM_LBUTTONDOWN) || (pMsg->message == WM_NCLBUTTONDOWN) )
		{
			CPoint ptMouse;
			GetCursorPos(&ptMouse);
			CRect rect;
			m_dlgPaperListEntry.GetWindowRect(rect);
			if ( ! rect.PtInRect(ptMouse))
				m_dlgPaperListEntry.OnOK();
		}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgPaperList::OnAddPaperentry() 
{
	//CPaperListEntry paperListEntry;

	//m_paperList.Add(paperListEntry);

	//int i = m_paperList.GetSize() - 1;

	//CString string;
	//LVITEM  lvitem;
	//lvitem.iItem	= m_paperListTable.GetItemCount();
	//lvitem.iSubItem = 0;
	//lvitem.mask	    = LVIF_TEXT;
	//lvitem.pszText  = m_paperList[i].m_strName.GetBuffer(MAX_TEXT);		
	//m_paperListTable.InsertItem(&lvitem); 

	//lvitem.iSubItem = 1;
	//lvitem.pszText  = m_paperList[i].m_strFormat.GetBuffer(MAX_TEXT);
	//m_paperListTable.SetItem(&lvitem);

	//string.Format(_T("%3d"), m_paperList[i].m_nNumSheets);
	//lvitem.iSubItem = 2;
	//lvitem.pszText  = string.GetBuffer(MAX_TEXT);
	//m_paperListTable.SetItem(&lvitem);

	//string.Format(_T("%8d"), m_paperList[i].m_nQuantity);
	//lvitem.iSubItem = 3;
	//lvitem.pszText  = string.GetBuffer(MAX_TEXT);
	//m_paperListTable.SetItem(&lvitem);

	//string.Format(_T("%8d"), m_paperList[i].m_nExtra);
	//lvitem.iSubItem = 4;
	//lvitem.pszText  = string.GetBuffer(MAX_TEXT);
	//m_paperListTable.SetItem(&lvitem);

	//string.Format(_T("%8d"), m_paperList[i].m_nTotal);
	//lvitem.iSubItem = 5;
	//lvitem.pszText  = string.GetBuffer(MAX_TEXT);
	//m_paperListTable.SetItem(&lvitem);

	//lvitem.iSubItem = 6;
	//lvitem.pszText  = m_paperList[i].m_strComment.GetBuffer(MAX_TEXT);
	//m_paperListTable.SetItem(&lvitem);

	//m_paperListTable.SetItemState(i, LVIS_SELECTED, LVIS_SELECTED);

	//if ( ! m_dlgPaperListEntry.m_hWnd)
	//	m_dlgPaperListEntry.Create(IDD_PAPERLIST_ENTRY, this);

	//m_dlgPaperListEntry.Position();
}

void CDlgPaperList::OnRemovePaperentry() 
{
	POSITION pos = m_paperListTable.GetFirstSelectedItemPosition();
	int nItem = m_paperListTable.GetNextSelectedItem(pos);
	if (nItem >= 0)
	{
		m_paperList.RemoveAt(nItem);
		m_paperListTable.DeleteItem(nItem);
	}
}

void CDlgPaperList::OnOK() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_productionData.m_paperList.RemoveAll();
		pDoc->m_productionData.m_paperList.Append(m_paperList);

		pDoc->SetModifiedFlag();
	}

	CDialog::OnOK();
}

