// DlgChangeFormats.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgChangeFormats.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PageListView.h"
#include "PageListTableView.h"
#ifdef PDF
#include "PDFReader.h"
#endif
#include "PageListDetailView.h"
#include "PrintSheetNavigationView.h"
#include "PrintSheetViewStrippingData.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgChangeFormats dialog


CDlgChangeFormats::CDlgChangeFormats(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgChangeFormats::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgChangeFormats)
	m_nPageOrientation = -1;
	m_fHeight = 0.0f;
	m_fWidth = 0.0f;
	m_strFormatName = _T("");
	//}}AFX_DATA_INIT
	m_bModified = FALSE;
}


void CDlgChangeFormats::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgChangeFormats)
	DDX_Control(pDX, IDC_WIDTH_SPIN, m_WidthSpin);
	DDX_Control(pDX, IDC_HEIGHT_SPIN, m_HeightSpin);
	DDX_Radio(pDX, IDC_PORTRAIT, m_nPageOrientation);
	DDX_Measure(pDX, IDC_HEIGHT, m_fHeight);
	DDX_Measure(pDX, IDC_WIDTH, m_fWidth);
	DDX_CBString(pDX, IDC_FORMAT, m_strFormatName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgChangeFormats, CDialog)
	//{{AFX_MSG_MAP(CDlgChangeFormats)
	ON_BN_CLICKED(IDC_DELETE_FORMAT, OnDeleteFormat)
	ON_BN_CLICKED(IDC_ADD_FORMAT, OnAddFormat)
	ON_CBN_SELCHANGE(IDC_FORMAT, OnSelchangeFormat)
	ON_NOTIFY(UDN_DELTAPOS, IDC_HEIGHT_SPIN, OnDeltaposHeightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_WIDTH_SPIN, OnDeltaposWidthSpin)
	ON_BN_CLICKED(IDC_LANDSCAPE, OnLandscape)
	ON_BN_CLICKED(IDC_PORTRAIT, OnPortrait)
	ON_EN_CHANGE(IDC_HEIGHT, OnChangeHeight)
	ON_EN_CHANGE(IDC_WIDTH, OnChangeWidth)
	ON_BN_CLICKED(IDC_CHANGE, OnChange)
	ON_CBN_EDITCHANGE(IDC_FORMAT, OnEditchangeFormat)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgChangeFormats message handlers

BOOL CDlgChangeFormats::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_WidthSpin.SetRange(0, 1);  
	m_HeightSpin.SetRange(0, 1);  

	CView* pView;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	POSITION pos = pDoc->GetFirstViewPosition();
	while (pos != NULL)
	{
		pView = pDoc->GetNextView(pos);
		if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetView)))
			break;
	}
	CDisplayItem* pDI = ((CPrintSheetView*)pView)->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));

	if (pDI)
	{
		CLayoutObject *pObject = (CLayoutObject*)pDI->m_pData;

		m_fWidth		   = pObject->GetWidth();
		m_fHeight		   = pObject->GetHeight();
		m_strFormatName	   = pObject->m_strFormatName;
		m_nPageOrientation = pObject->m_nObjectOrientation;

		while (pDI)
		{
			if (m_fWidth != pObject->GetWidth() || m_fHeight != pObject->GetHeight() || 
				m_strFormatName != pObject->m_strFormatName || m_nPageOrientation != pObject->m_nObjectOrientation)
			{
				m_fWidth		   = 0.0f;
				m_fHeight		   = 0.0f;
				m_strFormatName	   = _T("");
				m_nPageOrientation = -1;
				break;
			}
			pDI = ((CPrintSheetView*)pView)->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			if (pDI)
				pObject = (CLayoutObject*)pDI->m_pData;
		}
	}

	m_PageFormatSelection.InitComboBox(this, m_strFormatName, &m_fWidth, &m_fHeight, IDC_FORMAT);

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
}

void CDlgChangeFormats::OnDeleteFormat() 
{
	m_PageFormatSelection.DeleteEntry(this, &m_fWidth, &m_fHeight, m_strFormatName, m_nPageOrientation, IDC_FORMAT);
	m_bModified = TRUE;
}

void CDlgChangeFormats::OnAddFormat() 
{
	m_PageFormatSelection.NewEntry(this, &m_fWidth, &m_fHeight, m_strFormatName, m_nPageOrientation, IDC_FORMAT);
	m_bModified = TRUE;
}

void CDlgChangeFormats::OnChange() 
{
	m_PageFormatSelection.ChangeEntry(this, &m_fWidth, &m_fHeight, m_strFormatName, m_nPageOrientation, IDC_FORMAT);
	m_bModified = TRUE;
}

void CDlgChangeFormats::OnSelchangeFormat() 
{
	m_PageFormatSelection.SelchangeFormatList(this, &m_fWidth, &m_fHeight, m_strFormatName, &m_nPageOrientation, IDC_FORMAT);
	m_bModified = TRUE;
	CButton *pButton = (CButton*)GetDlgItem(IDC_CHANGE);
	pButton->EnableWindow(TRUE);
}

void CDlgChangeFormats::OnEditchangeFormat() 
{
	CButton *pButton = (CButton*)GetDlgItem(IDC_CHANGE);
	pButton->EnableWindow(FALSE);
}

void CDlgChangeFormats::OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_HeightSpin.GetBuddy(), pNMUpDown->iDelta);
	*pResult = 0;
	m_bModified = TRUE;
}

void CDlgChangeFormats::OnChangeHeight() 
{
	m_bModified = TRUE;
}

void CDlgChangeFormats::OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_WidthSpin.GetBuddy(), pNMUpDown->iDelta);
	*pResult = 0;
	m_bModified = TRUE;
}

void CDlgChangeFormats::OnChangeWidth() 
{
	m_bModified = TRUE;
}

void CDlgChangeFormats::OnLandscape() 
{
	if (m_nPageOrientation == LANDSCAPE)// already landscape
		return;
	
	m_nPageOrientation = LANDSCAPE;		// switch to portrait
	m_bModified = TRUE;

	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);
}

void CDlgChangeFormats::OnPortrait() 
{
	if (m_nPageOrientation == PORTRAIT)	// already portrait
		return;

	m_nPageOrientation = PORTRAIT;		// switch to portrait
	m_bModified = TRUE;

	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);
}

void CDlgChangeFormats::OnOK() 
{
	UpdateData(TRUE);

	if (m_bModified == TRUE)	
	{
		CLayoutObject *pObject, *pCounterpartObject;
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		pDoc->ResetPublicFlags();

		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (! pView)
			return;

		pView->UndoSave();

		CDisplayItem* pDI = ((CPrintSheetView*)pView)->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));

		pObject = (CLayoutObject*)pDI->m_pData;
		if (pObject)
		{	// Analyze the neighbourhood
			pObject->m_LeftNeighbours.GetSize();			// To start an analyzing-process
			pCounterpartObject = pObject->FindCounterpartObject();
			if (pCounterpartObject)
				pCounterpartObject->m_LeftNeighbours.GetSize();	// To start an analyzing-process for the other layoutside

			while (pDI)
			{
				pCounterpartObject = (theApp.settings.m_bFrontRearParallel) ? pObject->FindCounterpartObject() : NULL;	// Find the counterpart object
																														// before the Layout will be changed

				//pObject->m_nPublicFlag = TRUE;
				ModifyObject(pObject, (CPrintSheet*)pDI->m_pAuxData);
				if (pCounterpartObject)
				{
					//pCounterpartObject->m_nPublicFlag = TRUE;
					ModifyObject(pCounterpartObject, (CPrintSheet*)pDI->m_pAuxData);
				}
					
				pDI = ((CPrintSheetView*)pView)->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
				if (pDI)
					pObject = (CLayoutObject*)pDI->m_pData;
			}

			pObject->GetLayoutSide()->Invalidate();
			pObject->m_LeftNeighbours.GetSize();			// To start an analyzing-process
			if (pCounterpartObject)
			{
				pCounterpartObject->GetLayoutSide()->Invalidate();
				pCounterpartObject->m_LeftNeighbours.GetSize();			// To start an analyzing-process
			}

			pObject->GetLayout()->AnalyzeMarks();

			pDoc->SetModifiedFlag();

			theApp.OnUpdateView(RUNTIME_CLASS(CPageListView),					NULL, 0, NULL);
			theApp.OnUpdateView(RUNTIME_CLASS(CPageListDetailView),				NULL, 0, NULL);
			theApp.OnUpdateView(RUNTIME_CLASS(CPageListTableView),				NULL, 0, NULL);
			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),					NULL, 0, NULL);
			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);

			CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
			if (pNavigationView)
				pNavigationView->UpdatePrintSheets(pObject->GetLayout());
		}
	}
	CDialog::OnOK();
}

void CDlgChangeFormats::ModifyObject(CLayoutObject *pObject, CPrintSheet* pPrintSheet)
{
	pObject->m_strFormatName	  = m_strFormatName;
	pObject->m_nObjectOrientation = (unsigned char)m_nPageOrientation;
	pObject->ModifySize(m_fWidth, m_fHeight, pPrintSheet);
}

