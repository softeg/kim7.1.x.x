// ProductPartsFrame.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "ProductPartsFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProductPartsFrame

IMPLEMENT_DYNCREATE(CProductPartsFrame, CMDIChildWnd)

CProductPartsFrame::CProductPartsFrame()
{
}

CProductPartsFrame::~CProductPartsFrame()
{
}


BEGIN_MESSAGE_MAP(CProductPartsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CProductPartsFrame)
		// HINWEIS - Der Klassen-Assistent f�gt hier Zuordnungsmakros ein und entfernt diese.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CProductPartsFrame 
