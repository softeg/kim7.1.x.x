// OwnerDrawnButtonList.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "OwnerDrawnButtonList.h"


// COwnerDrawnButtonList 

COwnerDrawnButtonList::COwnerDrawnButtonList()
{
	m_rcFrame			 = CRect(0, 0, 0, 0);
	m_pWndParent		 = NULL;
	m_pWndMsgTarget		 = NULL;
	m_nCurSelection		 = -1;
	m_pFnDrawItem		 = NULL;
	m_pFnDrawBackground = NULL;
	m_sizeBitmap		 = CSize(0, 0);
	m_bGroup			 = TRUE;
	m_font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_pToolTip			 = NULL;
}

COwnerDrawnButtonList::~COwnerDrawnButtonList() 
{
	for (int i = 0; i < GetSize(); i++)
		delete ElementAt(i);
	m_imgCold.Destroy();
	m_imgHot.Destroy();
	m_font.DeleteObject();
	delete m_pToolTip;
}

void COwnerDrawnButtonList::Create(int nFrameID, int nOrientation, CWnd* pWndParent, CWnd* pWndMsgTarget, void (*pFnDrawItem)(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState), int nIDBitmapHot, int nIDBitmapCold, CSize sizeBitmap, BOOL bGroup)
{
	pWndParent->GetDlgItem(nFrameID)->GetWindowRect(m_rcFrame);
	pWndParent->ScreenToClient(m_rcFrame);
	m_rcFrame.DeflateRect(5, 5); m_rcFrame.OffsetRect(0, 20);
	m_nOrientation = nOrientation;
	if (nIDBitmapHot != -1)
		m_imgHot.LoadFromResource(AfxGetApp()->m_hInstance, nIDBitmapHot);
	if (nIDBitmapCold != -1)
		m_imgCold.LoadFromResource(AfxGetApp()->m_hInstance, nIDBitmapCold);
	m_sizeBitmap	= sizeBitmap;
	m_pWndParent	= pWndParent;
	m_pWndMsgTarget = pWndMsgTarget;
	m_pFnDrawItem	= pFnDrawItem;
	m_bGroup		= bGroup;
	m_strTheme		= _T("TOOLBAR");
	m_pToolTip = new CToolTipCtrl;
    m_pToolTip->Create(m_pWndParent);
	m_nShow			= SW_SHOW;

	RemoveAll();
}

void COwnerDrawnButtonList::Create(CWnd* pWndParent, CWnd* pWndMsgTarget, void (*pFnDrawItem)(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState), int nIDBitmapHot, int nIDBitmapCold, 
								   CSize sizeBitmap, void (*pFnDrawBackground)(CDC* pDC, CRect rcUpdateRgn, CWnd* pUpdateWnd))
{
	m_nOrientation = 0;
	if (nIDBitmapHot != -1)
		m_imgHot.LoadFromResource(AfxGetApp()->m_hInstance, nIDBitmapHot);
	if (nIDBitmapCold != -1)
		m_imgCold.LoadFromResource(AfxGetApp()->m_hInstance, nIDBitmapCold);
	m_sizeBitmap		= sizeBitmap;
	m_pWndParent		= pWndParent;
	m_pWndMsgTarget		= pWndMsgTarget;
	m_pFnDrawItem		= pFnDrawItem;
	m_pFnDrawBackground	= pFnDrawBackground;
	m_strTheme			= _T("TOOLBAR");
	m_pToolTip = new CToolTipCtrl;
    m_pToolTip->Create(m_pWndParent);
	m_nShow				= SW_SHOW;

	RemoveAll();
}

void COwnerDrawnButtonList::AddButton(int nLabelID, int nTooltipID, int nCommandID, int nSize, int nBitmapIndex, ULONG_PTR itemData, BOOL bPushed)
{
	CString strLabel, strTooltip;
	strLabel.LoadString(nLabelID);
	strTooltip.LoadString(nTooltipID);
	AddButton(strLabel, strTooltip, nCommandID, nSize, nBitmapIndex, itemData, bPushed);
}

void COwnerDrawnButtonList::AddButton(const CString& strLabel, const CString& strTooltip, int nCommandID, int nSize, int nBitmapIndex, ULONG_PTR itemData, BOOL bPushed)
{
	int nIndex = GetSize();
	int nOffset = (m_nOrientation == ODBL_HORIZONTAL) ? ( (nIndex > 0) ? GetButtonClientRect(nIndex - 1).right : 0 ) : ( (nIndex > 0) ? GetButtonClientRect(nIndex - 1).bottom : 0 );
	CRect rcButton;
	if (m_nOrientation == ODBL_HORIZONTAL)
	{
		rcButton.left = m_rcFrame.left + nOffset; rcButton.top = m_rcFrame.top; rcButton.right = m_rcFrame.left + nOffset + nSize; rcButton.bottom = m_rcFrame.top + m_rcFrame.Height();
	}
	else
	{
		rcButton.left = m_rcFrame.left; rcButton.top = m_rcFrame.top + nOffset; rcButton.right = m_rcFrame.left + m_rcFrame.Width(); rcButton.bottom = m_rcFrame.top + nOffset + nSize;
	}
	COwnerDrawnButton* pButton = new COwnerDrawnButton();

	pButton->m_nIndex		= nIndex;
	pButton->m_nBitmapIndex	= nBitmapIndex;
	pButton->m_nCommandID	= nCommandID;
	pButton->m_bPushed		= bPushed;
	pButton->m_itemData		= itemData;
	if (bPushed)
		m_nCurSelection = nIndex;

	pButton->Create(strLabel, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, rcButton, m_pWndParent, nCommandID);

	pButton->ModifyStyleEx(0, WS_EX_TRANSPARENT);
	pButton->m_pButtonList = this;

	Add(pButton);

	m_pToolTip->AddTool(pButton, strTooltip);
	m_pToolTip->Activate(TRUE);
}

void COwnerDrawnButtonList::AddButton(int nLabelID,	int nTooltipID, int nCommandID, CRect& rcFrame, int nBitmapIndex, ULONG_PTR itemData, BOOL bPushed, BOOL bCloseButton)
{
	CString strLabel, strTooltip;
	strLabel.LoadString(nLabelID);
	strTooltip.LoadString(nTooltipID);
	AddButton(strLabel, strTooltip, nCommandID, rcFrame, nBitmapIndex, itemData, bPushed, bCloseButton);
}

void COwnerDrawnButtonList::AddButton(const CString& strLabel, const CString& strTooltip, int nCommandID, CRect& rcFrame, int nBitmapIndex, ULONG_PTR itemData, BOOL bPushed, BOOL bCloseButton)
{
	int	  nIndex   = GetSize();
	CSize sizeText(rcFrame.Width(), rcFrame.Height());
	if ( ! m_pFnDrawItem)
	{
		CDC* pDC = m_pWndParent->GetDC();
		if (pDC)
		{
			CFont* pOldFont = pDC->SelectObject(&m_font);
			sizeText = pDC->GetTextExtent(strLabel);
			pDC->SelectObject(pOldFont);
		}
		if ( ! strLabel.IsEmpty())
			rcFrame.right = rcFrame.left + m_sizeBitmap.cx + 10 + sizeText.cx + 5;
		else
			rcFrame.right = rcFrame.left + m_sizeBitmap.cx + 10;
	}
	CRect rcButton = rcFrame;
	COwnerDrawnButton* pButton = new COwnerDrawnButton();

	pButton->m_nIndex		= nIndex;
	pButton->m_nBitmapIndex	= nBitmapIndex;
	pButton->m_nCommandID	= nCommandID;
	pButton->m_bPushed		= bPushed;
	pButton->m_itemData		= itemData;
	pButton->m_bCloseButton = bCloseButton;
	if (bCloseButton)
	{
		pButton->m_rcClose = rcButton; 	pButton->m_rcClose.MoveToXY(0, 0);
		pButton->m_rcClose.left = pButton->m_rcClose.right - 16; pButton->m_rcClose.bottom = pButton->m_rcClose.top + 16;
	}

	if (bPushed)
		m_nCurSelection = nIndex;

	pButton->Create(strLabel, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, rcButton, m_pWndParent, nCommandID);

	pButton->ModifyStyleEx(0, WS_EX_TRANSPARENT);
	pButton->m_pButtonList = this;

	Add(pButton);

	m_pToolTip->AddTool(pButton, strTooltip);
	m_pToolTip->Activate(TRUE);
}

CRect COwnerDrawnButtonList::GetButtonClientRect(int nIndex)
{
	if ( (nIndex < 0) || (nIndex >= GetSize()) )
		return CRect(0,0,0,0);

	CRect rcButton;
	ElementAt(nIndex)->GetWindowRect(rcButton);
	m_pWndParent->ScreenToClient(rcButton);
	rcButton.OffsetRect(-m_rcFrame.left, -m_rcFrame.top);
	return rcButton;
}

CRect COwnerDrawnButtonList::GetButtonClientRectByCommand(int nIDCommand)
{
	COwnerDrawnButton* pButton = GetButtonByCommand(nIDCommand);
	if ( ! pButton)
		return CRect(0,0,0,0);

	CRect rcButton;
	pButton->GetWindowRect(rcButton);
	m_pWndParent->ScreenToClient(rcButton);
	rcButton.OffsetRect(-m_rcFrame.left, -m_rcFrame.top);
	return rcButton;
}

void COwnerDrawnButtonList::Show()
{
	m_nShow = SW_SHOW;
	for (int i = 0; i < GetSize(); i++)
	{
		ElementAt(i)->EnableWindow(TRUE);
		ElementAt(i)->ShowWindow(SW_SHOW);
	}
}

void COwnerDrawnButtonList::Hide()
{
	m_nShow	= SW_HIDE;
	for (int i = 0; i < GetSize(); i++)
	{
		ElementAt(i)->EnableWindow(FALSE);
		ElementAt(i)->ShowWindow(SW_HIDE);
	}
}

void COwnerDrawnButtonList::Redraw()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i)->m_pBkBitmap)
		{
			ElementAt(i)->m_pBkBitmap->DeleteObject();
			delete ElementAt(i)->m_pBkBitmap;
			ElementAt(i)->m_pBkBitmap = NULL;
		}
		ElementAt(i)->Invalidate();
		ElementAt(i)->UpdateWindow();
	}
}

void COwnerDrawnButtonList::RemoveAll()
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i)->m_pBkBitmap)
		{
			ElementAt(i)->m_pBkBitmap->DeleteObject();
			delete ElementAt(i)->m_pBkBitmap;
			ElementAt(i)->m_pBkBitmap = NULL;
		}
		ElementAt(i)->DestroyWindow();
		delete ElementAt(i);
	}
	CArray::RemoveAll();
}

void COwnerDrawnButtonList::ResetData()
{
	for (int i = 0; i < GetSize(); i++)
	{
		ElementAt(i)->m_itemData = 0;
	}
}

void COwnerDrawnButtonList::SelectButton(int nIndex)
{
	if ( (nIndex < 0) || (nIndex >= GetSize()) )
		return;

	for (int i = 0; i < GetSize(); i++)
		ElementAt(i)->m_bPushed = FALSE;

	ElementAt(nIndex)->m_bPushed = TRUE;
	m_nCurSelection = nIndex;

	for (int i = 0; i < GetSize(); i++)
	{
		ElementAt(i)->Invalidate();
		ElementAt(i)->UpdateWindow();
	}
}

void COwnerDrawnButtonList::SelectButton(ULONG_PTR itemData)
{
	int nIndex = -1;
	for (int i = 0; i < GetSize(); i++)
	{
		ElementAt(i)->m_bPushed = FALSE;
		if (ElementAt(i)->m_itemData == itemData)
			nIndex = i;
	}
	m_nCurSelection = nIndex;

	if ( (nIndex >= 0) && (nIndex < GetSize()) )
		ElementAt(nIndex)->m_bPushed = TRUE;

	for (int i = 0; i < GetSize(); i++)
	{
		ElementAt(i)->Invalidate();
		ElementAt(i)->UpdateWindow();
	}
}

void COwnerDrawnButtonList::SetCheck(CCmdUI* pCmdUI, BOOL bCheck)
{
	if ( ! pCmdUI->m_pOther)
		return;
	COwnerDrawnButton* pButton = (COwnerDrawnButton*)CWnd::FromHandle(pCmdUI->m_pOther->m_hWnd);
	if (pButton->m_pButtonList->m_nShow	== SW_HIDE)
		return;
	((COwnerDrawnButton*)pButton)->SetCheck(bCheck);
}

void COwnerDrawnButtonList::Enable(CCmdUI* pCmdUI, BOOL bEnable)
{
	if ( ! pCmdUI->m_pOther)
		return;
	COwnerDrawnButton* pButton = (COwnerDrawnButton*)CWnd::FromHandle(pCmdUI->m_pOther->m_hWnd);
	if (pButton->m_pButtonList->m_nShow	== SW_HIDE)
		return;
	((COwnerDrawnButton*)pButton)->Enable(bEnable);
}

void COwnerDrawnButtonList::Show(CCmdUI* pCmdUI, int nShow)
{
	if ( ! pCmdUI->m_pOther)
		return;
	COwnerDrawnButton* pButton = (COwnerDrawnButton*)CWnd::FromHandle(pCmdUI->m_pOther->m_hWnd);
	if (pButton->m_pButtonList->m_nShow	== SW_HIDE)
		return;
	((COwnerDrawnButton*)pButton)->ShowWindow(nShow);
}

BOOL COwnerDrawnButtonList::SelectedButtonClosePressed()
{
	int nCurSel = GetSelection();
	if ( (nCurSel < 0) || (nCurSel >= GetSize()) )
		return FALSE;

	return ElementAt(nCurSel)->m_bMouseOverClose;
}

COwnerDrawnButton* COwnerDrawnButtonList::GetButtonByCommand(int nIDCommand)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i)->m_nCommandID == nIDCommand)
			return ElementAt(i);
	}

	return NULL;
}

void COwnerDrawnButtonList::EnableButton(int nIDCommand, BOOL bEnable)
{
	COwnerDrawnButton* pButton = GetButtonByCommand(nIDCommand);
	if (pButton)
		pButton->EnableWindow(bEnable);
}

void COwnerDrawnButtonList::CheckButton(int nIDCommand, BOOL bCheck)
{
	COwnerDrawnButton* pButton = GetButtonByCommand(nIDCommand);
	if (pButton)
		pButton->SetCheck((bCheck) ? BST_CHECKED : BST_UNCHECKED);
}

void COwnerDrawnButtonList::ShowButton(int nIDCommand, int nShow)
{
	COwnerDrawnButton* pButton = GetButtonByCommand(nIDCommand);
	if (pButton)
		pButton->ShowWindow(nShow);
}

void COwnerDrawnButtonList::ShowButtonByIndex(int nIndex, int nShow)
{
	if (nIndex < GetSize())
		ElementAt(nIndex)->ShowWindow(nShow);
}

void COwnerDrawnButtonList::MoveButtons(CPoint ptTopLeft)
{
	if (GetSize() <= 0)
		return;

	CRect rcButton;
	GetAt(0)->GetWindowRect(rcButton);
	m_pWndParent->ScreenToClient(rcButton);
	CSize szOffset = ptTopLeft - rcButton.TopLeft();
	for (int i = 0; i < GetSize(); i++)
	{
		GetAt(i)->GetWindowRect(rcButton); m_pWndParent->ScreenToClient(rcButton);
		rcButton.OffsetRect(szOffset);
		GetAt(i)->MoveWindow(rcButton); 
	}
}

void COwnerDrawnButtonList::ClipButtons(CDC* pDC)
{
	if (GetSize() <= 0)
		return;

	CRect clipRect;
	for (int i = 0; i < GetSize(); i++)
	{
		if ( ! GetAt(i)->IsWindowVisible())
			continue;

		GetAt(i)->GetWindowRect(clipRect); m_pWndParent->ScreenToClient(clipRect);
		pDC->DPtoLP(clipRect);
		pDC->BeginPath();
			pDC->Rectangle(clipRect);	
		pDC->EndPath();
		pDC->SelectClipPath(RGN_DIFF);
	}
}


void COwnerDrawnButtonList::Destroy()
{
	m_imgCold.Destroy();
	m_imgHot.Destroy();
}


// COwnerDrawnButton

IMPLEMENT_DYNAMIC(COwnerDrawnButton, CButton)

COwnerDrawnButton::COwnerDrawnButton()
{
	m_nIndex			= -1;
	m_nBitmapIndex		= -1;
	m_nCommandID		= -1;
	m_bPushed			= FALSE;
	m_bEnabled			= TRUE;
	m_bMouseOverButton	= FALSE;
	m_itemData			= NULL;
	m_bCloseButton		= FALSE;
	m_rcClose			= CRect(0,0,0,0);
	m_bMouseOverClose	= FALSE;
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
	m_bkBrush.CreateSolidBrush(GUICOLOR_GROUP);
	m_pBkBitmap = NULL;
}

COwnerDrawnButton::~COwnerDrawnButton()
{
	m_hollowBrush.DeleteObject();
	m_bkBrush.DeleteObject();
	if (m_pBkBitmap)
	{
		m_pBkBitmap->DeleteObject();
		delete m_pBkBitmap;
		m_pBkBitmap = NULL;
	}
}

BEGIN_MESSAGE_MAP(COwnerDrawnButton, CButton)
	ON_WM_DRAWITEM()
	ON_WM_MOUSEMOVE()
	ON_CONTROL_REFLECT(BN_CLICKED, &COwnerDrawnButton::OnBnClicked)
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


void COwnerDrawnButton::OnUpdateODButton(CCmdUI* pCmdUI) 
{
}

void COwnerDrawnButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect rcItem = lpDrawItemStruct->rcItem;

	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

	lpDrawItemStruct->itemData = m_itemData;

	if (lpDrawItemStruct->itemState & ODS_SELECTED)
		DrawButtonBackground(pDC, TS_PRESSED, &rcItem);
	else
		if (m_bPushed && m_bEnabled)
		{
			if ( ! m_pButtonList->m_imgCold.IsNull() && ! m_pButtonList->m_imgHot.IsNull())
				DrawButtonBackground(pDC, TS_CHECKED, &rcItem);
			else
				DrawButtonBackground(pDC, (m_bMouseOverButton) ? TS_HOTCHECKED : TS_CHECKED, &rcItem);
			if (m_pButtonList->m_pFnDrawItem)
				m_pButtonList->m_pFnDrawItem(lpDrawItemStruct, (m_bMouseOverButton) ? ODB_HOTCHECKED : ODB_CHECKED);
		}
		else
			if (m_bMouseOverButton)
			{
				DrawButtonBackground(pDC, TS_HOT, &rcItem);
				if (m_pButtonList->m_pFnDrawItem)
					m_pButtonList->m_pFnDrawItem(lpDrawItemStruct, (m_bEnabled) ? ODB_HOT : ODB_DISABLED);
			}
			else
			{
				DrawButtonBackground(pDC, TS_NORMAL, &rcItem);
				if (m_pButtonList->m_pFnDrawItem)
					m_pButtonList->m_pFnDrawItem(lpDrawItemStruct, (m_bEnabled) ? ODB_NORMAL : ODB_DISABLED);
			}

	CString strLabel;
	GetWindowText(strLabel);
	if ( ! rcItem.IsRectEmpty() && ! strLabel.IsEmpty() && ! m_pButtonList->m_pFnDrawItem )
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor((m_bEnabled) ? GUICOLOR_DLG_STATIC_TEXT : LIGHTGRAY);
		pDC->SelectObject(&m_pButtonList->m_font);
		CRect rcText = rcItem; rcText.OffsetRect(m_pButtonList->m_sizeBitmap.cx + 10, 0);
		pDC->DrawText(strLabel, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	}

	if (m_bCloseButton)
		pDC->DrawIcon(m_rcClose.left, m_rcClose.bottom - 33, theApp.LoadIcon((m_bMouseOverClose) ? IDI_SYSTEM_CLOSE_HOT : IDI_SYSTEM_CLOSE));
}

void COwnerDrawnButton::DrawButtonBackground(CDC* pDC, int nState, CRect rcItem)
{
	HTHEME hThemeToolbar = theApp._OpenThemeData(GetParent(), m_pButtonList->m_strTheme);

	if (hThemeToolbar)
	{
		if (m_pButtonList->m_strTheme == _T("TOOLBAR"))
		{
			DrawBackgroundNormalState(pDC, rcItem);
			switch (nState)
			{
			//case TS_NORMAL: 		DrawBackgroundNormalState(pDC, rcItem);																break;
			case TS_NORMAL: 		//DrawBackgroundNormalState(pDC, rcItem);
									DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TP_BUTTON, TS_NORMAL,		&rcItem, 0);	break;
			case TS_HOT:			DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TP_BUTTON, TS_HOT,			&rcItem, 0);	break;
			case TS_CHECKED:		DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TP_BUTTON, TS_CHECKED,		&rcItem, 0);	break;
			case TS_HOTCHECKED:		DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TP_BUTTON, TS_HOTCHECKED,	&rcItem, 0);	break;
			//case TS_NORMAL:		DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TP_BUTTON, TS_HOT,			&rcItem, 0);	break;
			default:				break;
			}
		}
		else
		if (m_pButtonList->m_strTheme == _T("TAB"))
		{
			switch (nState)
			{
			case TS_NORMAL: 		DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TABP_TABITEM, TIS_NORMAL,	&rcItem, 0);	break;
			case TS_PRESSED:		DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TABP_TABITEM, TIS_SELECTED,	&rcItem, 0);	break;
			case TS_HOT:			DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TABP_TABITEM, TIS_HOT,		&rcItem, 0);	break;
			case TS_CHECKED:		DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TABP_TABITEM, TIS_SELECTED,	&rcItem, 0);	break;
			case TS_HOTCHECKED:		DrawThemeBackgroundEx(hThemeToolbar, pDC->GetSafeHdc(), TABP_TABITEM, TIS_SELECTED,	&rcItem, 0);	break;
			default:				break;
			}
		}
	}
	else
	{
		switch (nState)
		{
		case TS_NORMAL: 		DrawBackgroundNormalState(pDC, rcItem);															break;
		case TS_PRESSED:		DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);		break;
		case TS_HOT:			DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_HOT);			break;
		case TS_CHECKED:		DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_CHECKED);		break;
		case TS_HOTCHECKED:		DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_CHECKED);		break;
		}
	}

	if ( m_pButtonList->m_imgCold.IsNull() || m_pButtonList->m_imgHot.IsNull())
		return;

	int nOffset = 0;//( (nState == TS_PRESSED) || (nState == TS_HOT) || (nState == TS_HOTCHECKED) ) ? 2 : 0;
	rcItem.OffsetRect(5, (rcItem.Height() - m_pButtonList->m_sizeBitmap.cy) / 2);
	int nBitmapIndex = (m_nBitmapIndex >= 0) ? m_nBitmapIndex : m_nIndex;
	CRect rcButton(m_pButtonList->m_sizeBitmap.cx * nBitmapIndex, 0, m_pButtonList->m_sizeBitmap.cx * (nBitmapIndex + 1), m_pButtonList->m_sizeBitmap.cy);

	if (m_bEnabled)
		m_pButtonList->m_imgHot.TransparentBlt(pDC->m_hDC,	rcItem.left, rcItem.top, m_pButtonList->m_sizeBitmap.cx, m_pButtonList->m_sizeBitmap.cy, 
															rcButton.left + nOffset, rcButton.top + nOffset, rcButton.Width(), rcButton.Height(), RGB(244,244,238));
	else
		m_pButtonList->m_imgCold.TransparentBlt(pDC->m_hDC, rcItem.left, rcItem.top, m_pButtonList->m_sizeBitmap.cx, m_pButtonList->m_sizeBitmap.cy, 
															rcButton.left + nOffset, rcButton.top + nOffset, rcButton.Width(), rcButton.Height(), RGB(244,244,238));

	theApp._CloseThemeData(hThemeToolbar);
}

void COwnerDrawnButton::DrawBackgroundNormalState(CDC* pDC, CRect& rcFrame)
{
	if (m_pButtonList->m_pFnDrawBackground)
		m_pButtonList->m_pFnDrawBackground(pDC, rcFrame, m_pButtonList->m_pWndParent);
	else
	{
		HTHEME hTheme = NULL;
		if (theApp.m_nOSVersion == CImpManApp::WinXP)	// only XP needs theming here - Vista runs correctly with "pDC->FillSolidRect(rcFrame, ::GetSysColor(CTLCOLOR_DLG))"
#ifdef UNICODE
			hTheme = theApp._OpenThemeData(GetParent(), _T("WINDOW"));
#else
			hTheme = theApp._OpenThemeData(GetParent(), CA2W("WINDOW"));
#endif

		if (hTheme)
			DrawThemeBackground(hTheme, pDC->GetSafeHdc(), WP_DIALOG, 0, &rcFrame, NULL);	
		else
			pDC->FillSolidRect(rcFrame, ::GetSysColor(CTLCOLOR_DLG));
		theApp._CloseThemeData(hTheme);
	}

	//RestoreBackground(pDC, rcFrame);
	//SaveBackground(pDC, rcFrame);		
}

void COwnerDrawnButton::OnBnClicked()
{
	if ( ! m_bEnabled)
		return;

	if (m_pButtonList->m_bGroup)
	{
		for (int i = 0; i < m_pButtonList->GetSize(); i++)
		{
			if (m_pButtonList->ElementAt(i)->m_bPushed)
				m_pButtonList->ElementAt(i)->InvalidateRect(NULL);
			m_pButtonList->ElementAt(i)->m_bPushed = FALSE;
		}
	}
	if (m_pButtonList->m_bGroup)
		m_bPushed = TRUE;

	if (m_bPushed)
		m_pButtonList->m_nCurSelection = m_nIndex;

	m_pButtonList->m_pWndMsgTarget->PostMessage(WM_COMMAND, m_nCommandID);  

	Invalidate();
	UpdateWindow();

	CWnd* pWnd = GetParent();
	if (pWnd)
		pWnd->UpdateDialogControls( pWnd, FALSE );
}

void COwnerDrawnButton::OnMouseMove(UINT nFlags, CPoint point)
{
	if ( ! m_bMouseOverButton)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = this->m_hWnd;
		if (::_TrackMouseEvent(&tme))
			m_bMouseOverButton = TRUE;
		InvalidateRect(NULL);
	}
		
	if (m_rcClose.PtInRect(point))
	{
		if ( ! m_bMouseOverClose)
		{
			m_bMouseOverClose = TRUE;
			InvalidateRect(m_rcClose);
		}
	}
	else
	{
		if (m_bMouseOverClose)
		{
			m_bMouseOverClose = FALSE;
			InvalidateRect(m_rcClose);
		}
	}

	CButton::OnMouseMove(nFlags, point);
}

LRESULT COwnerDrawnButton::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	m_bMouseOverButton = m_bMouseOverClose = FALSE;
	InvalidateRect(NULL);
	UpdateWindow();
	return TRUE;
}

HBRUSH COwnerDrawnButton::CtlColor(CDC* pDC, UINT nCtlColor)
{
//	return (HBRUSH)m_bkBrush;
	return (HBRUSH)m_hollowBrush;

//	return NULL;
}

void COwnerDrawnButton::SetCheck(int nCheck)
{
	if (nCheck == BST_CHECKED)
		if (m_bPushed == TRUE)
			return;
		else
			m_bPushed = TRUE;
	else
		if (m_bPushed == FALSE)
			return;
		else
			m_bPushed = FALSE;
	InvalidateRect(NULL);
	UpdateWindow();
}

void COwnerDrawnButton::Enable(BOOL bEnable)
{
	if (m_bEnabled == bEnable)
		return;
	m_bEnabled = bEnable;
	if ( ! m_bEnabled)
		m_bPushed = FALSE;
	InvalidateRect(NULL);
	UpdateWindow();
}

void COwnerDrawnButton::SaveBackground(CDC* pDC, const CRect& rect)
{
	if (m_pBkBitmap)
	{
		//return;
		m_pBkBitmap->DeleteObject();
		delete m_pBkBitmap;
	}
	m_pBkBitmap = new CBitmap;

 	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	m_pBkBitmap->CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBkBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), pDC, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void COwnerDrawnButton::RestoreBackground(CDC* pDC, const CRect& rect)
{
	if (!m_pBkBitmap)
		return;

	CDC	 memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = memDC.SelectObject(m_pBkBitmap);
	pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();

	//m_pBkBitmap->DeleteObject();
	//delete m_pBkBitmap;
	//m_pBkBitmap = NULL;

	GetParent()->ValidateRect(rect);
}

BOOL COwnerDrawnButton::OnEraseBkgnd(CDC* pDC)
{
//	BOOL bRet = CButton::OnEraseBkgnd(pDC);
//
//if (m_nCommandID == ID_UNDO)
//{
//	int nDummy = 0;
//}
//
//	CRect rcItem; GetWindowRect(rcItem);
//	ScreenToClient(rcItem);
//	SaveBackground(pDC, rcItem);		
//
//	return bRet;
	return TRUE;
}
