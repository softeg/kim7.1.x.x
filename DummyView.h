#pragma once


// CDummyView-Ansicht

class CDummyView : public CView
{
	DECLARE_DYNCREATE(CDummyView)

protected:
	CDummyView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CDummyView();

public:
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
};


