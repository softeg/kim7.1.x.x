// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "MainFrm.h"
#include "OrderDataFrame.h"
#include "ThemedTabCtrl.h"
#include "BookBlockDataView.h"
#include "BookBlockView.h"
#include "PrintSheetView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetTreeView.h"
#include "PrintSheetTableView.h"
#include "PrintSheetMarksView.h"
#include "PageListView.h"
#include "Splash.h"
#include "PostScriptParser.h"
#ifdef PDF
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"
#include "DlgDemoOrRegisterDongle.h"
#include "spromeps.h"
#endif
#include "DlgMarkSet.h"
#include "JobDocumentationView.h"
#include "OrderDataView.h"
#include "DlgProdDataTool.h"
#include "ProdDataFrame.h"
#include "ProdDataHeaderView.h"
#include "DlgJobData.h"
#include "JDFInputMIS.h"
#include "JDFOutputMIS.h"
#include "ModalFrame.h"
#include "BookBlockFrame.h"
#include "DlgLicenseManager.h"
#include "DlgPageFormats.h"
#include "DlgPaperDefs.h"
#include "DlgSheet.h"
#include "DlgProductionManager.h"
#include "GraphicComponent.h"

#include "psapi.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif 



void CMyStatusBar::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
   switch(lpDrawItemStruct->itemID)
   {
   case 1:
		{
			if (strlen(theApp.settings.m_szAutoServerNameIP))
			{
				// Attach to a CDC object
				CDC dc;
				dc.Attach(lpDrawItemStruct->hDC);

				CString strInfo; 
				if (theApp.m_bServerConnected)
					AfxFormatString1(strInfo, IDS_CONNECTED_TO, theApp.settings.m_szAutoServerNameIP);
				else
					strInfo.LoadString(IDS_DISCONNECTED);

				CFont font;
				font.CreateFont(13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
				dc.SelectObject(&font);

				CSize size = dc.GetTextExtent(strInfo);
				UINT nID, nStyle;
				int  nWidth;
				GetPaneInfo(1, nID, nStyle, nWidth);
				SetPaneInfo(1, nID, nStyle, size.cx + 50);
				// Get the pane rectangle and calculate text coordinates
				CRect rect(&lpDrawItemStruct->rcItem);
 
				CImage img;
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_SERVER));
				if ( ! img.IsNull())
					img.TransparentBlt(dc.m_hDC, rect.left, rect.top, img.GetWidth(), img.GetHeight(), WHITE);
				
				rect.left += img.GetWidth() + 5;
				dc.SetBkMode(OPAQUE);
				dc.SetTextColor(BLACK);//(theApp.m_bServerConnected) ? BLACK : LIGHTGRAY);
				dc.SetBkColor((theApp.m_bServerConnected) ? RGB(220, 235, 255) : RGB(245,205,190));
				dc.TextOut(rect.left, rect.top, strInfo);

				font.DeleteObject();

				// Detach from the CDC object, otherwise the hDC will be
				// destroyed when the CDC object goes out of scope
				dc.Detach();
			}
		}
		return;
   case 2:
		{
			int	nDocsDongleStatus  = (m_pDoc) ? m_pDoc->m_nDongleStatus : CImpManApp::StandardDongle;
			if ( theApp.m_bDemoMode || (theApp.m_nDongleStatus == CImpManApp::ClientDongle) || 
				 (nDocsDongleStatus == CImpManApp::NoDongle) || (nDocsDongleStatus == CImpManApp::ClientDongle) )
			{
				// Attach to a CDC object
				CDC dc;
				dc.Attach(lpDrawItemStruct->hDC);

				CString strMode;
				if (theApp.m_bDemoMode || (nDocsDongleStatus == CImpManApp::NoDongle))
					strMode.LoadString(ID_INDICATOR_DEMOMODE);
				else
					strMode.LoadString(IDS_CLIENT_MODE);
				strMode.Insert(0, ' '); 

				CFont font;
				font.CreateFont(13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
				dc.SelectObject(&font);

				CSize size = dc.GetTextExtent(strMode);
				UINT nID, nStyle;
				int  nWidth;
				GetPaneInfo(2, nID, nStyle, nWidth);
				SetPaneInfo(2, nID, nStyle, size.cx);

				// Get the pane rectangle and calculate text coordinates
				CRect rect(&lpDrawItemStruct->rcItem);
				dc.FillSolidRect(rect, (theApp.m_bDemoMode || (nDocsDongleStatus == CImpManApp::NoDongle)) ? RGB(250, 180, 170) : RGB(180,180,255));
				dc.SetBkMode(TRANSPARENT);
				dc.SetTextColor(BLACK);
				dc.TextOut(rect.left, rect.top, strMode);

				font.DeleteObject();

				// Detach from the CDC object, otherwise the hDC will be
				// destroyed when the CDC object goes out of scope
				dc.Detach();
			}
		}
		return;
   }

   CStatusBar::DrawItem(lpDrawItemStruct);
}


#define ID_NAV_SEPARATOR_1 111
#define ID_NAV_SEPARATOR_2 222


/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_WM_INITMENUPOPUP()
	ON_WM_MENUSELECT()
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnFileSaveAs)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_DATE, OnUpdateDate)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_COMMAND(ID_FILE_PAGE_SETUP, OnFilePageSetup)
	ON_COMMAND(ID_LICENSE_SETUP, OnLicenseSetup)
	ON_COMMAND(ID_KRAUSE_HOMEPAGE, OnKrauseHomepage)
	ON_COMMAND(ID_KIM_UPDATES, OnKimUpdates)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_CLOSE()
	ON_COMMAND(ID_HELP, OnHelp)
	ON_COMMAND(ID_HELP_FINDER, OnHelpFinder)
	ON_COMMAND(ID_KIM_HOMEPAGE, OnKimHomepage)
	ON_COMMAND(ID_NAV_AUTOSERVER, OnNavAutoServer)
	ON_COMMAND(ID_NAV_AUTOSERVER_CLOSE, OnNavAutoServerClose)
	ON_COMMAND(ID_NAV_ORDERDATA, OnNavOrderdata)
	ON_COMMAND(ID_NAV_PRINTSHEETLIST, OnNavPrintsheetlist)
	ON_COMMAND(ID_NAV_PRINTSHEETMARKS, OnNavPrintsheetMarks)
	ON_COMMAND(ID_NAV_BOOKBLOCK, OnNavBookblock)
	ON_COMMAND(ID_NAV_PAGELIST, OnNavPagelist)
	ON_COMMAND(ID_NAV_PREPRESS, OnNavPrepress)
	ON_COMMAND(ID_NAV_OUTPUT, OnNavOutput)
	ON_COMMAND(ID_NAV_JOBDOCUMENTATION, OnNavJobDocumentation)
	ON_COMMAND(ID_NAV_RESOURCES, OnNavResources)
	ON_UPDATE_COMMAND_UI(ID_NAV_AUTOSERVER, OnUpdateNavAutoServer)
	ON_UPDATE_COMMAND_UI(ID_NAV_AUTOSERVER_CLOSE, OnUpdateNavAutoServerClose)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateFileNew)
	ON_UPDATE_COMMAND_UI(ID_NAV_ORDERDATA, OnUpdateNavOrderdata)
	ON_UPDATE_COMMAND_UI(ID_NAV_PRINTSHEETLIST, OnUpdateNavPrintsheetlist)
	ON_UPDATE_COMMAND_UI(ID_NAV_PRINTSHEETMARKS, OnUpdateNavPrintsheetMarks)
	ON_UPDATE_COMMAND_UI(ID_NAV_BOOKBLOCK, OnUpdateNavBookblock)
	ON_UPDATE_COMMAND_UI(ID_NAV_PREPRESS, OnUpdateNavPrepress)
	ON_UPDATE_COMMAND_UI(ID_NAV_OUTPUT, OnUpdateNavOutput)
	ON_UPDATE_COMMAND_UI(ID_NAV_PAGELIST, OnUpdateNavPagelist)
	ON_UPDATE_COMMAND_UI(ID_NAV_JOBDOCUMENTATION, OnUpdateNavJobDocumentation)
	ON_UPDATE_COMMAND_UI(ID_NAV_RESOURCES, OnUpdateNavResources)
	//}}AFX_MSG_MAP
	// Global help commands
	ON_COMMAND(ID_HELP_FINDER, CMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_CONTEXT_HELP, CMDIFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CMDIFrameWnd::OnHelpFinder)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	ON_COMMAND(ID_IMPORT_JDF, OnImportJdf)
	ON_COMMAND(ID_EXPORT_JDF, OnExportJdf)
	ON_UPDATE_COMMAND_UI(ID_IMPORT_JDF, OnUpdateImportJdf)
	ON_UPDATE_COMMAND_UI(ID_EXPORT_JDF, OnUpdateExportJdf)
	ON_COMMAND(ID_MODIFY_ORDERDATA, OnModifyOrderData)
	ON_COMMAND(ID_MODIFY_PRODUCT,   OnModifyProduct)
	ON_COMMAND(ID_MODIFY_STRIPPING, OnModifyStripping)
	ON_COMMAND(ID_RESOURCE_SHEETFORMATS, OnResourceSheetformats)
	ON_COMMAND(ID_RESOURCE_PAGEFORMATS, OnResourcePageformats)
	ON_COMMAND(ID_RESOURCE_PAPERS, OnResourcePapers)
	ON_COMMAND(ID_RESOURCE_PRODUCTIONPROFILES, OnResourceProductionprofiles)
	ON_COMMAND(ID_RESOURCE_CONTROLMARKS, OnResourceControlMarks)
	ON_COMMAND(ID_RESOURCE_PRINTSHEET_OUTPUTPROFILES, OnResourcePrintSheetOutputProfiles)
	ON_COMMAND(ID_RESOURCE_FOLDSHEET_OUTPUTPROFILES, OnResourceFoldSheetOutputProfiles)
	ON_COMMAND(ID_RESOURCE_BOOKLET_OUTPUTPROFILES, OnResourceBookletOutputProfiles)
	ON_WM_ACTIVATE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_SERVER,
	ID_INDICATOR_DEMOMODE,
	ID_INDICATOR_NUM,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// CG: The following block was inserted by 'Status Bar' component.
	{
		m_nStatusPane1Width = -1;
		m_bMenuSelect = FALSE;
	}

	m_nNavSelection = 0;
}

CMainFrame::~CMainFrame()
{
	 m_wndMDIClient.Detach();
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

    if (m_wndMDIClient.Attach(m_hWndMDIClient) == 0)
    {
        TRACE0("Failed to attach MDIClient.\n");
        return -1;      // fail to create
    }

	//if (!m_wndToolBar.Create(this) ||
	//	!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	//{
	//	TRACE0("Failed to create toolbar\n");
	//	return -1;      // fail to create
	//}

	if (!m_wndReBar.Create(this, 0))
	{
		TRACE0("Infoleiste konnte nicht erstellt werden\n");
		return -1;      // Fehler bei Erstellung
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
	//m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
	//	CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC | CBRS_GRIPPER);

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
//	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY); 
//	EnableDocking(CBRS_ALIGN_ANY);
//	DockControlBar(&m_wndToolBar, AFX_IDW_DOCKBAR_TOP);
	//m_wndToolBar.ModifyStyle(0, TBSTYLE_FLAT);

	// CG: The following line was added by the Splash Screen component.
	CSplashWnd::ShowSplashScreen();//(this);	// Mit this-Zeiger l�uft es nicht -> 
												// Muss NULL sein
	// CG: The following block was inserted by 'Status Bar' component.
	{
		// Find out the size of the static variable 'indicators' defined
		// by AppWizard and copy it
		int nOrigSize = sizeof(indicators) / sizeof(UINT);

		UINT* pIndicators = new UINT[nOrigSize + 2];
		memcpy(pIndicators, indicators, sizeof(indicators));

		// Call the Status Bar Component's status bar creation function
		if (!InitStatusBar(pIndicators, nOrigSize, 60))
		{
			TRACE0("Failed to initialize Status Bar\n");
			return -1;
		}
		delete[] pIndicators;

		m_wndStatusBar.SetPaneStyle(1, SBPS_NORMAL | SBT_OWNERDRAW);

		if (theApp.m_bDemoMode || (theApp.m_nDongleStatus == CImpManApp::ClientDongle) )
			m_wndStatusBar.SetPaneStyle(2, SBPS_NORMAL | SBT_OWNERDRAW);
		else
			m_wndStatusBar.SetPaneStyle(2, SBPS_NOBORDERS | SBT_OWNERDRAW);
	}

	if (theApp.settings.m_bAutoBackup)
		SetTimer(AutoBackupTimerID, theApp.settings.m_iAutoBackupTime * 60000, NULL);  // 1 minute == 60000

	SetTimer(DongleQueryTimerID,		 10000,  NULL);  // 10 seconds
	SetTimer(WatchPageSourceTimerID,	 60000,  NULL);  //  1 minute == 60000
	SetTimer(CheckRemainingDaysTimerID, 120000,  NULL);	 //  2 minutes
	SetTimer(CheckServerConnection,		 10000,  NULL);	 // 10 seconds

	//if ( (theApp.m_nGUIStyle == CImpManApp::GUIAutoImpose) || (theApp.m_nGUIStyle == CImpManApp::GUINewTemplate) ||
	//	 (theApp.m_nGUIStyle == CImpManApp::GUIPageList) )
	//{
	//	m_wndToolBar.ShowWindow(SW_HIDE);
	//	m_wndStatusBar.ShowWindow(SW_HIDE);
	//}

	//m_wndToolBar.ShowWindow(SW_HIDE);

	InitNavigationToolbar();

	m_wndReBar.AddBar(&m_wndNavigationBar);


///// KIM Logo
	if ( ! m_wndKIMLogoBar.m_hWnd)
	{
		if ( ! m_wndKIMLogoBar.CreateEx(this))
		{
			TRACE0("Symbolleiste konnte nicht erstellt werden\n");
			return -1;      // Fehler bei Erstellung
		}
	}

	int nNumButtons = 1;
	CSize sizeBitmap(149, 35);
	m_wndKIMLogoBar.GetToolBarCtrl().SetBitmapSize(sizeBitmap);
    m_wndKIMLogoBar.LoadTrueColorToolBar(sizeBitmap.cx, IDB_KIMLOGOBAR);

	m_wndKIMLogoBar.ModifyStyle(0, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT | TBSTYLE_LIST | TBSTYLE_AUTOSIZE);
	m_wndKIMLogoBar.SetButtons(NULL, nNumButtons);

	m_wndKIMLogoBar.SetButtonInfo(0, ID_APP_ABOUT,	TBBS_BUTTON | TBBS_AUTOSIZE, 0);

	CRect rectToolBar;
	m_wndKIMLogoBar.GetItemRect(0, &rectToolBar);
	m_wndKIMLogoBar.SetSizes(rectToolBar.Size(), CSize(149,35));
	m_wndKIMLogoBar.SetBarStyle(m_wndKIMLogoBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_SIZE_DYNAMIC);

	m_wndReBar.AddBar(&m_wndKIMLogoBar, NULL, NULL, RBBS_FIXEDSIZE);

	return 0;
}

void CMainFrame::InitNavigationToolbar()
{
	if ( ! m_wndNavigationBar.m_hWnd)
	{
		if ( ! m_wndNavigationBar.CreateEx(this))
		{
			TRACE0("Symbolleiste konnte nicht erstellt werden\n");
			return;      // Fehler bei Erstellung
		}
		m_wndNavigationBar.SetDlgCtrlID(99);
	}

	int nNumButtons = 14;
	CSize sizeBitmap(50, 35);
	m_wndNavigationBar.GetToolBarCtrl().SetBitmapSize(sizeBitmap);
    m_wndNavigationBar.LoadTrueColorToolBar(sizeBitmap.cx, IDB_NAVTOOLBAR);

	m_wndNavigationBar.ModifyStyle(0, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT | TBSTYLE_LIST | TBSTYLE_AUTOSIZE);
	m_wndNavigationBar.GetToolBarCtrl().SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);
	m_wndNavigationBar.SetButtons(NULL, nNumButtons);

	CDocument* pDoc = CImpManDoc::GetDoc();

	// set up each toolbar button
	CString str;
	int nIndex = 0;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_AUTOSERVER,			TBBS_BUTTON | TBBS_AUTOSIZE, 11);
	str.LoadString(IDS_NAV_AUTOSERVER);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_AUTOSERVER_CLOSE,	TBBS_BUTTON | TBBS_AUTOSIZE, 8);
	str.LoadString(IDS_NAV_AUTOSERVER_CLOSE);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_SEPARATOR_1,		TBBS_SEPARATOR, -1); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_FILE_SAVE,				TBBS_BUTTON | TBBS_AUTOSIZE, 9);
	str = _T("");//.LoadString(IDS_NAV_SAVE);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_ORDERDATA,			TBBS_BUTTON | TBBS_AUTOSIZE, 0);
	str.LoadString(IDS_NAV_ORDERDATA);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_FILE_NEW,				TBBS_BUTTON | TBBS_AUTOSIZE, 0);	
	str.LoadString(IDS_NAV_NEWJOB);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_PRINTSHEETLIST,		TBBS_BUTTON | TBBS_AUTOSIZE, 4);
	str.LoadString(IDS_NAV_PRINTSHEETLIST);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_PRINTSHEETMARKS,	TBBS_BUTTON | TBBS_AUTOSIZE, 7);
	str.LoadString(IDS_NAV_PRINTSHEETMARKS);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_BOOKBLOCK,			TBBS_BUTTON | TBBS_AUTOSIZE, 1);
	str.LoadString(IDS_NAV_BOOKBLOCK);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_PAGELIST,			TBBS_BUTTON | TBBS_AUTOSIZE, 3);
	str.LoadString(IDS_NAV_PAGELIST);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_OUTPUT,				TBBS_BUTTON | TBBS_AUTOSIZE, 6);
	str.LoadString(IDS_NAV_OUTPUT);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_JOBDOCUMENTATION,	TBBS_BUTTON | TBBS_AUTOSIZE, 2);
	str.LoadString(IDS_NAV_JOBDOCUMENTATION);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_SEPARATOR_2,		TBBS_SEPARATOR,				-1); nIndex++;

	m_wndNavigationBar.SetButtonInfo(nIndex, ID_NAV_RESOURCES,			TBBS_BUTTON | TBBS_AUTOSIZE, 10);
	str.LoadString(IDS_NAV_RESOURCES);
	m_wndNavigationBar.SetButtonText(nIndex, str); nIndex++;

	// set up toolbar button sizes 
	CRect rectToolBar;
	m_wndNavigationBar.GetItemRect(0, &rectToolBar);
	m_wndNavigationBar.SetSizes(rectToolBar.Size(), CSize(50,35));

	m_wndNavigationBar.SetBarStyle(m_wndNavigationBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_SIZE_DYNAMIC);

	m_nNavSelection = 0;

	////////////////////////////////////////
}
 
void CMainFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CMDIFrameWnd::OnActivate(nState, pWndOther, bMinimized);
}

void CMainFrame::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)		// menu drawing
{
	if (lpDrawItemStruct->CtlType == ODT_MENU)
		if(IsMenu((HMENU)lpDrawItemStruct->hwndItem))
		{
			CMenu* pMenu = CMenu::FromHandle((HMENU)lpDrawItemStruct->hwndItem);
			if (pMenu)
			{
				CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
				CRect rcItem = lpDrawItemStruct->rcItem;

				pDC->FillSolidRect(rcItem, RGB(252, 252, 249));//CREME);

				CString strText;
				pMenu->GetMenuString(lpDrawItemStruct->itemID, strText, MF_BYCOMMAND);

				CRect rcIcon = rcItem;
				rcIcon.right = rcIcon.left + 40;

				if (lpDrawItemStruct->itemState & ODS_SELECTED)
				{
					CGraphicComponent gc;
					gc.DrawTransparentFrame(pDC, rcItem, RGB(120,160,220), 2);
				}				

				CImage img;
				switch (lpDrawItemStruct->itemID)
				{
				case ID_RESOURCE_PRODUCTIONPROFILES:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_RULES));				break;
				case ID_SETTINGS_PRESSDEVICE:				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_PRINTING_MACHINE));	break;
				case ID_IMPOSE:								img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_FOLDSHEET));			break;
				case ID_REFERENCE_COLOR:					img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_COLORS));			break;
				case ID_RESOURCE_SHEETFORMATS:				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_PRINTSHEET));		break;
				case ID_RESOURCE_PAGEFORMATS:				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_UNIMPOSED_PAGE));	break;
				case ID_RESOURCE_PAPERS:					img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_LABELS));			break;
				case ID_RESOURCE_CONTROLMARKS:				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MARKSET));
															rcIcon.left += 5; rcIcon.top += 5;														break;
				case ID_RESOURCE_PRINTSHEET_OUTPUTPROFILES:	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_OUTPUT_PRINTSHEET));	break;
				case ID_RESOURCE_FOLDSHEET_OUTPUTPROFILES:	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_OUTPUT_FOLDSHEET));	break;
				case ID_RESOURCE_BOOKLET_OUTPUTPROFILES:	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_OUTPUT_BOOKLET));	break;
				default:																															break;
				}
				if ( ! img.IsNull())
					img.TransparentBlt(pDC->m_hDC, rcIcon.left + 5, rcIcon.top + 5, img.GetWidth(), img.GetHeight(), WHITE);

				pDC->SelectObject(GetFont());
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(BLACK);
				CRect rcText = rcItem;
				rcText.left = rcIcon.right + 5;
				pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

			}
		}

	CMDIFrameWnd::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CMainFrame::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct->CtlType == ODT_MENU)
	{
		int	nMaxLen = 0;
		if(IsMenu((HMENU)lpMeasureItemStruct->itemData))
		{
			CMenu* pMenu = CMenu::FromHandle((HMENU)lpMeasureItemStruct->itemData);
			if (pMenu)
			{
				CString strText;
				pMenu->GetMenuString(lpMeasureItemStruct->itemID, strText, MF_BYCOMMAND);
				CClientDC dc(this);
				dc.SelectObject(GetFont());
				nMaxLen = dc.GetTextExtent(strText).cx + 35;
			}
		}
		lpMeasureItemStruct->itemWidth  = nMaxLen;
		lpMeasureItemStruct->itemHeight = 38;
	}
	else
		CMDIFrameWnd::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if ( (theApp.m_nGUIStyle == CImpManApp::GUINewTemplate) ||
		 (theApp.m_nGUIStyle == CImpManApp::GUIOpenTemplate) || 
		 (theApp.m_nGUIStyle == CImpManApp::GUIOpenOutputList) )
	{
		cs.style = WS_OVERLAPPED | WS_CAPTION | WS_THICKFRAME | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;
		cs.lpszName = _T("Job Template");
	}
	else
	if (theApp.m_nGUIStyle == CImpManApp::GUIAutoImpose)
	{
		cs.style = WS_OVERLAPPED;
		cs.lpszName = _T("Auto-Impose");
	}
	else
	if (theApp.m_nGUIStyle == CImpManApp::GUIPageList)
	{
		cs.style = WS_OVERLAPPED | WS_CAPTION | WS_THICKFRAME | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;
		CString strTitle;
		strTitle.LoadString(IDS_PAGELIST_TITLE);
		cs.lpszName = strTitle;
	}
	else
		cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE | WS_THICKFRAME | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE;


    cs.lpszClass = theApp.m_strClassName;

	return CMDIFrameWnd::PreCreateWindow(cs);
}

CAutoServerFrame* CMainFrame::CreateOrActivateAutoServerFrame(CDocTemplate* pTemplate, CDocument* pDoc)
{
	theApp.m_nPreviousFrame = theApp.m_nActualFrame;
	theApp.m_nActualFrame	= IDR_AUTOSERVERFRAME;

	CView* pView = theApp.GetAutoServerJobsView();
	if (pView)
	{
		CFrameWnd* pFrame = pView->GetParentFrame();
		if (pFrame->IsKindOf(RUNTIME_CLASS(CAutoServerFrame)))
			pFrame->ActivateFrame();
		return (CAutoServerFrame*)pView->GetParentFrame();
	}

	CAutoServerFrame* pAutoServerFrame = (CAutoServerFrame*)(pTemplate->CreateNewFrame(pDoc, NULL));
	if (pAutoServerFrame == NULL)
		return NULL;     // not created
	ASSERT_KINDOF(CMDIChildWnd, pAutoServerFrame);
	pTemplate->InitialUpdateFrame(pAutoServerFrame, pDoc);
	pAutoServerFrame->RecalcLayout();

	return pAutoServerFrame;
}

COrderDataFrame* CMainFrame::CreateOrActivateOrderDataFrame(CDocTemplate* pTemplate, CDocument* pDoc)
{
	theApp.m_nPreviousFrame = theApp.m_nActualFrame;
	theApp.m_nActualFrame	= IDR_ORDERDATAFRAME;

	CView* pView = theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	if (pView)
	{
		CFrameWnd* pFrame = pView->GetParentFrame();
		if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
			pFrame->ActivateFrame();
		return (COrderDataFrame*)pView->GetParentFrame();
	}

	COrderDataFrame* pOrderDataFrame = (COrderDataFrame*)(pTemplate->CreateNewFrame(pDoc, NULL));
	if (pOrderDataFrame == NULL)
		return NULL;     // not created
	ASSERT_KINDOF(CMDIChildWnd, pOrderDataFrame);
	pTemplate->InitialUpdateFrame(pOrderDataFrame, pDoc);
	pOrderDataFrame->RecalcLayout();

	return pOrderDataFrame;
}

CJobDocumentationFrame* CMainFrame::CreateOrActivateJobDocumentationFrame(CDocTemplate* pTemplate, CDocument* pDoc)
{
	theApp.m_nPreviousFrame = theApp.m_nActualFrame;
	theApp.m_nActualFrame	= IDR_JOBDOCUMENTATIONFRAME;

	CView* pView = theApp.GetView(RUNTIME_CLASS(CJobDocumentationView));
	if (pView)
	{
		pView->GetParentFrame()->ActivateFrame();
		return (CJobDocumentationFrame*)pView->GetParentFrame();
	}

	CJobDocumentationFrame* pJobDocumentationFrame = (CJobDocumentationFrame*)(pTemplate->CreateNewFrame(pDoc, NULL));
	if (pJobDocumentationFrame == NULL)
		return NULL;     // not created
	ASSERT_KINDOF(CMDIChildWnd, pJobDocumentationFrame);
	pTemplate->InitialUpdateFrame(pJobDocumentationFrame, pDoc);
//	pJobDocumentationFrame->RecalcLayout();

	return pJobDocumentationFrame;
}

CBookblockFrame* CMainFrame::CreateOrActivateBookblockFrame(CDocTemplate* pTemplate, CDocument* pDoc)
{
	theApp.m_nPreviousFrame = theApp.m_nActualFrame;
	theApp.m_nActualFrame	= IDR_BOOKBLOCKFRAME;

	CView* pView = theApp.GetView(RUNTIME_CLASS(CBookBlockView));
	if (pView)
	{
		CFrameWnd* pFrame = pView->GetParentFrame();
		if (pFrame->IsKindOf(RUNTIME_CLASS(CBookblockFrame)))
		{
			pFrame->ActivateFrame();
			return (CBookblockFrame*)pFrame;
		}
	}

	CBookblockFrame* pBookblockFrame = (CBookblockFrame*)(pTemplate->CreateNewFrame(pDoc, NULL));
	if (pBookblockFrame == NULL)
		return NULL;     // not created
	ASSERT_KINDOF(CMDIChildWnd, pBookblockFrame);
	pTemplate->InitialUpdateFrame(pBookblockFrame, pDoc);
	pBookblockFrame->RecalcLayout();

	return pBookblockFrame;
}

CPrintSheetFrame* CMainFrame::CreateOrActivatePrintSheetFrame(CDocTemplate* pTemplate, CDocument* pDoc)
{
	theApp.m_nPreviousFrame = theApp.m_nActualFrame;
	theApp.m_nActualFrame	= IDR_PRINTSHEETFRAME;

	CView* pView = theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
	if (pView)
	{
		CFrameWnd* pFrame = pView->GetParentFrame();
		if (pFrame->IsKindOf(RUNTIME_CLASS(CPrintSheetFrame)))
		{
			pFrame->ActivateFrame();
			return (CPrintSheetFrame*)pFrame;
		}
	}

	CPrintSheetFrame* pPrintSheetFrame = (CPrintSheetFrame*)(pTemplate->CreateNewFrame(pDoc, NULL));
	if (pPrintSheetFrame == NULL)
		return NULL;     // not created
	ASSERT_KINDOF(CMDIChildWnd, pPrintSheetFrame);
	pTemplate->InitialUpdateFrame(pPrintSheetFrame, pDoc);
	pPrintSheetFrame->RecalcLayout();

	return pPrintSheetFrame;
}

CNewPrintSheetFrame* CMainFrame::CreateOrActivateNewPrintSheetFrame(CDocTemplate* pTemplate, CDocument* pDoc)
{
	theApp.m_nPreviousFrame = theApp.m_nActualFrame;
	theApp.m_nActualFrame	= IDR_NEWPRINTSHEETFRAME;

	CView* pView = theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CFrameWnd* pFrame = pView->GetParentFrame();
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		{
			pFrame->ActivateFrame();
			return (CNewPrintSheetFrame*)pFrame;
		}
	}

	CNewPrintSheetFrame* pNewPrintSheetFrame = (CNewPrintSheetFrame*)(pTemplate->CreateNewFrame(pDoc, NULL));
	if (pNewPrintSheetFrame == NULL)
		return NULL;     // not created
	ASSERT_KINDOF(CMDIChildWnd, pNewPrintSheetFrame);
	pTemplate->InitialUpdateFrame(pNewPrintSheetFrame, pDoc);
	pNewPrintSheetFrame->RecalcLayout();

	return pNewPrintSheetFrame;
}

CPageListFrame* CMainFrame::CreateOrActivatePageListFrame(CDocTemplate* pTemplate, CDocument* pDoc)
{
	theApp.m_nPreviousFrame = theApp.m_nActualFrame;
	theApp.m_nActualFrame	= IDR_PAGELISTFRAME;

	CView* pView = theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pView)
	{
		pView->GetParentFrame()->ActivateFrame();
		return (CPageListFrame*)pView->GetParentFrame();
	}

	CPageListFrame* pPageListFrame = (CPageListFrame*)(pTemplate->CreateNewFrame(pDoc, NULL));
	if (pPageListFrame == NULL)
		return NULL;     // not created
	ASSERT_KINDOF(CMDIChildWnd, pPageListFrame);
	pTemplate->InitialUpdateFrame(pPageListFrame, pDoc);
	pPageListFrame->RecalcLayout();

	return pPageListFrame;

}
/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

BOOL CMainFrame::InitStatusBar(UINT *pIndicators, int nSize, int nSeconds)
{
	// Create an index for the DATE pane
	m_nDatePaneNo = nSize++;
	pIndicators[m_nDatePaneNo] = ID_INDICATOR_DATE;

	// TODO: Select an appropriate time interval for updating
	// the status bar when idling.
	m_wndStatusBar.SetTimer(0x1000, nSeconds * 1000, NULL);

	return m_wndStatusBar.SetIndicators(pIndicators, nSize);
}

void CMainFrame::OnFileSaveAs(CCmdUI* pCmdUI)
{
	//if (theApp.m_bServerConnected)
	//	if (pCmdUI->m_pMenu!=NULL)
	//	{
	//		pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
	//		return;
	//	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	if ((pDoc->m_nDongleStatus == CImpManApp::NoDongle) && ! pDoc->m_GlobalData.m_strJobTicketFile.IsEmpty())	// job created by JDF import w/o JDF-MIS license -> not allowed to be saved
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

void CMainFrame::OnUpdateDate(CCmdUI* pCmdUI)
{
	// Get current date and format it
	CTime time = CTime::GetCurrentTime();
	CString strDate = time.Format(_T("%#x"));
	strDate.Insert(0, ' '); strDate += ' ';

	// compute the width of the date string
	CSize size = GetStatusBarStringWidth(strDate);
	// Update the pane to reflect the current date
	UINT nID, nStyle;
	int nWidth;
	m_wndStatusBar.GetPaneInfo(m_nDatePaneNo, nID, nStyle, nWidth);
	m_wndStatusBar.SetPaneInfo(m_nDatePaneNo, nID, nStyle, size.cx);
	pCmdUI->SetText(strDate);
	pCmdUI->Enable(TRUE);
}

CSize CMainFrame::GetStatusBarStringWidth(CString string)
{
	CSize size;
	HGDIOBJ hOldFont = NULL;
	HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
	CClientDC dc(NULL);
	if (hFont != NULL) 
		hOldFont = dc.SelectObject(hFont);
	size = dc.GetTextExtent(string);
	if (hOldFont != NULL) 
		dc.SelectObject(hOldFont);

	return size;
}

void CMainFrame::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu)
{
  	CMDIFrameWnd::OnMenuSelect(nItemID, nFlags, hSysMenu);
	// CG: The following block was inserted by 'Status Bar' component.
	{
		// Restore first pane of the statusbar?
		if (nFlags == 0xFFFF && hSysMenu == 0 && m_nStatusPane1Width != -1)
		{
			m_bMenuSelect = FALSE;
			m_wndStatusBar.SetPaneInfo(0, 
				m_nStatusPane1ID, m_nStatusPane1Style, m_nStatusPane1Width);
			m_nStatusPane1Width = -1;   // Set it to illegal value
		}
		else 
			m_bMenuSelect = TRUE;
	}
}

void CMainFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
  	CMDIFrameWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	// CG: The following block was inserted by 'Status Bar' component.
	{
		// store width of first pane and its style
		if (m_nStatusPane1Width == -1 && m_bMenuSelect)
		{
			m_wndStatusBar.GetPaneInfo(0, m_nStatusPane1ID, 
				m_nStatusPane1Style, m_nStatusPane1Width);
			m_wndStatusBar.SetPaneInfo(0, m_nStatusPane1ID, 
				SBPS_NOBORDERS|SBPS_STRETCH, 16384);
		}
	}
}

void CMainFrame::OnTimer(UINT_PTR nIDEvent) 
{
	switch (nIDEvent)
	{
		case AutoBackupTimerID: 	
			{
				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if (pDoc)
					pDoc->OnFileAutoBackup();
			}
			break;

		case DongleQueryTimerID:
			{
				int nOldStatus = theApp.m_nDongleStatus;
				CheckDonglePDF(FALSE, TRUE);
				if (theApp.m_bDemoMode && (theApp.m_nRemainingDays <= 0) )
				{
					CDlgDemoOrRegisterDongle dlg;
					dlg.DoModal();
					OnClose();
					return;
				}
				UpdateDemoStatusBarPane(CImpManDoc::GetDoc());
				OnUpdateFrameTitle(TRUE);
				CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
				if (pPrintSheetTableView)
				{
					if ( (theApp.m_nDongleStatus == CImpManApp::ClientDongle) && (theApp.m_nGUIStyle == CImpManApp::GUIStandard) )
					{
						pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_START_OUTPUT,  FALSE);
						pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_CANCEL_OUTPUT, FALSE);
					}
					else
						if (nOldStatus == CImpManApp::ClientDongle)
						{
							pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_START_OUTPUT,  TRUE);
							pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_CANCEL_OUTPUT, TRUE);
						}

				}
			}
			break;

		case WatchPageSourceTimerID:
			{
				if (theApp.settings.m_bAutoWatchPageSources)
				{
					CImpManDoc* pDoc = CImpManDoc::GetDoc();
					if (pDoc)
					{
						pDoc->m_PageSourceList.CheckPDFsModified();
						pDoc->m_PageSourceList.CheckDocuments();
					}
				}
			}
			break;

		case CheckRemainingDaysTimerID: 
			theApp.CheckRemainingDays();
			break;

		case CheckServerConnection:
			{
#ifndef KIM_ENGINE
				if ( ! theApp.m_bServerConnected && strlen(theApp.settings.m_szAutoServerNameIP))	// if server lost or not yet connected -> try again
				{
					if ( ! theApp.m_bSocketsInitialized)
					{
						if( AfxSocketInit() != FALSE)
							theApp.m_bSocketsInitialized = TRUE;
					}
					if (theApp.m_bSocketsInitialized)
					{
						if (theApp.m_clientSocket.m_hSocket != INVALID_SOCKET)
							theApp.m_clientSocket.Detach();
						if (theApp.m_clientSocket.Create())
						{
							theApp.m_clientSocket.Connect(theApp.settings.m_szAutoServerNameIP, 1001);
						}
					}
				}
#endif
			}
			break;

		default : nIDEvent = nIDEvent;
				break;
	}
}

void CMainFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	CMDIFrameWnd::OnUpdateFrameTitle(bAddToTitle);

	CString strTitle;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		strTitle.Format(_T("%s - %s"), pDoc->GetTitle(), theApp.BuildKIMTitle(m_strTitle));
	else
		strTitle = theApp.BuildKIMTitle(m_strTitle);

	SetWindowText(strTitle);
}

void CMainFrame::OnFilePageSetup() 
{
	PRINTDLG printDlg;
	theApp.GetPrinterDeviceDefaults(&printDlg);

	CPageSetupDialog dlg;
	dlg.m_psd.lStructSize		= sizeof(PAGESETUPDLG);
	dlg.m_psd.hwndOwner			= m_hWnd;
	dlg.m_psd.hDevNames			= printDlg.hDevNames;
	dlg.m_psd.hDevMode			= printDlg.hDevMode;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	TCHAR szMargins[100];
	DWORD dwSize = sizeof(szMargins);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szMargins, &dwSize) == ERROR_SUCCESS)
		_stscanf(szMargins, _T("%ld %ld %ld %ld"), &dlg.m_psd.rtMargin.left, &dlg.m_psd.rtMargin.top, &dlg.m_psd.rtMargin.right, &dlg.m_psd.rtMargin.bottom);
	else
		dlg.m_psd.rtMargin = CRect(2500, 2500, 2500, 2500);

	dlg.DoModal();

	theApp.SelectPrinter(dlg.m_psd.hDevNames, dlg.m_psd.hDevMode);

	_stprintf(szMargins, _T("%ld %ld %ld %ld"), dlg.m_psd.rtMargin.left, dlg.m_psd.rtMargin.top, dlg.m_psd.rtMargin.right, dlg.m_psd.rtMargin.bottom);
	RegSetValueEx(hKey, _T("PrintMargins"), 0, REG_SZ, (LPBYTE)szMargins, (_tcslen(szMargins) + 1) * sizeof(TCHAR));
	RegCloseKey(hKey);
}

void CMainFrame::OnLicenseSetup() 
{
	CDlgLicenseManager dlg;

	dlg.DoModal();
}

void CMainFrame::OnKimHomepage() 
{
	switch (theApp.settings.m_iLanguage)
	{
	case 0:		ShellExecute(NULL, _T("open"), _T("http://www.krause-imposition-manager.de"),					NULL, NULL, SW_SHOWNORMAL);	break;
	case 1:		ShellExecute(NULL, _T("open"), _T("http://www.krause-imposition-manager.de/index_engl.html"),	NULL, NULL, SW_SHOWNORMAL);	break;
	default:	ShellExecute(NULL, _T("open"), _T("http://www.krause-imposition-manager.de"),					NULL, NULL, SW_SHOWNORMAL);	break;
	}
}

void CMainFrame::OnKrauseHomepage() 
{
	ShellExecute(NULL, _T("open"), _T("http://www.krause.de"), NULL, NULL, SW_SHOWNORMAL);	
}

void CMainFrame::OnKimUpdates() 
{
	switch (theApp.settings.m_iLanguage)
	{
		case 0:  ShellExecute(NULL, _T("open"), _T("http://www.krause-imposition-manager.de/Sites/Patch.html"),		NULL, NULL, SW_SHOWNORMAL);	break;
		case 1:  ShellExecute(NULL, _T("open"), _T("http://www.krause-imposition-manager.de/Sites/Patch_engl.html"),	NULL, NULL, SW_SHOWNORMAL);	break;
		default: ShellExecute(NULL, _T("open"), _T("http://www.krause-imposition-manager.de/Sites/Patch.html"),		NULL, NULL, SW_SHOWNORMAL);	break;
	}
}

void CMainFrame::SaveFrameStatus(CWnd* pWnd, CSplitterWnd* pWndSplitter1, CSplitterWnd* pWndSplitter2, CSplitterWnd* pWndSplitter3, CSplitterWnd* pWndSplitter4, CSplitterWnd* pWndSplitter5, int nSizePrivate, LPBYTE lpPrivate, CString str2ndName)
{
	CRuntimeClass* prt = pWnd->GetRuntimeClass();
	if ( ! prt)
		return;
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\") + CString(prt->m_lpszClassName);
	if ( ! str2ndName.IsEmpty())
		strKey += _T("@") + str2ndName;

	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		WINDOWPLACEMENT wp;
		pWnd->GetWindowPlacement(&wp);

		if (prt->IsDerivedFrom(RUNTIME_CLASS(CMDIChildWnd)))
		{
			CMDIFrameWnd* pFrameWnd = ((CMDIChildWnd*)pWnd)->GetMDIFrame();
			ASSERT_VALID(pFrameWnd);
			BOOL bMaximized;
			pFrameWnd->MDIGetActive(&bMaximized);
			if (bMaximized)
				wp.showCmd = SW_SHOWMAXIMIZED;
		}

		RegSetValueEx(hKey, _T("Placement"), 0, REG_BINARY, (LPBYTE)&wp, wp.length * sizeof(TCHAR));

		int			nCur, nMin;
		CDWordArray splitterInfo;
		if (pWndSplitter1)
			if (pWndSplitter1->m_hWnd)
			{
				for (int i = 0; i < pWndSplitter1->GetRowCount(); i++)
				{
					pWndSplitter1->GetRowInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
				for (int i = 0; i < pWndSplitter1->GetColumnCount(); i++)
				{
					pWndSplitter1->GetColumnInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
			}
		if (pWndSplitter2)
			if (pWndSplitter2->m_hWnd)
			{
				for (int i = 0; i < pWndSplitter2->GetRowCount(); i++)
				{
					pWndSplitter2->GetRowInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
				for (int i = 0; i < pWndSplitter2->GetColumnCount(); i++)
				{
					pWndSplitter2->GetColumnInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
			}
		if (pWndSplitter3)
			if (pWndSplitter3->m_hWnd)
			{
				for (int i = 0; i < pWndSplitter3->GetRowCount(); i++)
				{
					pWndSplitter3->GetRowInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
				for (int i = 0; i < pWndSplitter3->GetColumnCount(); i++)
				{
					pWndSplitter3->GetColumnInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
			}
		if (pWndSplitter4)
			if (pWndSplitter4->m_hWnd)
			{
				for (int i = 0; i < pWndSplitter4->GetRowCount(); i++)
				{
					pWndSplitter4->GetRowInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
				for (int i = 0; i < pWndSplitter4->GetColumnCount(); i++)
				{
					pWndSplitter4->GetColumnInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
			}
		if (pWndSplitter5)
			if (pWndSplitter5->m_hWnd)
			{
				for (int i = 0; i < pWndSplitter5->GetRowCount(); i++)
				{
					pWndSplitter5->GetRowInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
				for (int i = 0; i < pWndSplitter5->GetColumnCount(); i++)
				{
					pWndSplitter5->GetColumnInfo(i, nCur, nMin);
					splitterInfo.Add((DWORD)nCur); splitterInfo.Add((DWORD)nMin);
				}
			}
		if (splitterInfo.GetSize())
			RegSetValueEx(hKey, _T("SplitterInfo"), 0, REG_BINARY, (LPBYTE)splitterInfo.GetData(), splitterInfo.GetSize() * sizeof(DWORD));
		if (nSizePrivate)
			RegSetValueEx(hKey, _T("PrivateData"), 0, REG_BINARY, (LPBYTE)lpPrivate, nSizePrivate);

		RegCloseKey(hKey);
	}
}

void CMainFrame::RestoreFrameStatus(CWnd* pWnd, BOOL& rbStatusRestored, int& rnCmdShow, CSplitterWnd* pWndSplitter1, CSplitterWnd* pWndSplitter2, CSplitterWnd* pWndSplitter3, CSplitterWnd* pWndSplitter4, CSplitterWnd* pWndSplitter5, int nSizePrivate, LPBYTE lpPrivate, CString str2ndName)
{
	if (rbStatusRestored)	// status restored only once - at first ActivateFrame()
		return;
	CRuntimeClass* prt = pWnd->GetRuntimeClass();
	if ( ! prt)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\") + CString(prt->m_lpszClassName);
	if ( ! str2ndName.IsEmpty())
		strKey += _T("@") + str2ndName;

	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		DWORD dwSize;
		if (rnCmdShow != -2)	// -2 -> only restore splitter positions (and private data)
		{
			WINDOWPLACEMENT wp;
			wp.length = sizeof(WINDOWPLACEMENT);
			dwSize	  = sizeof(WINDOWPLACEMENT);
			if (RegQueryValueEx(hKey, _T("Placement"), 0, NULL, (LPBYTE)&wp, &dwSize) == ERROR_SUCCESS)
			{
				rnCmdShow = wp.showCmd;
				if (rnCmdShow == SW_SHOWNORMAL)
					pWnd->MoveWindow(&wp.rcNormalPosition, FALSE);
			}
		}

		const int nBufSize = 1024;
		DWORD	  dwBuffer[nBufSize];
		dwSize = sizeof(dwBuffer);
		if (RegQueryValueEx(hKey, _T("SplitterInfo"), 0, NULL, (LPBYTE)&dwBuffer, &dwSize) == ERROR_SUCCESS)
		{
			int	nBufIndex = 0;
			if (pWndSplitter1)
				if (pWndSplitter1->m_hWnd)
				{
					for (int i = 0; (i < pWndSplitter1->GetRowCount())	  && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter1->SetRowInfo	(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]); 
						nBufIndex += 2;
					}
					for (int i = 0; (i < pWndSplitter1->GetColumnCount()) && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter1->SetColumnInfo(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]);
						nBufIndex += 2;
					}
					pWndSplitter1->RecalcLayout();
				}
			if (pWndSplitter2)
				if (pWndSplitter2->m_hWnd)
				{
					for (int i = 0; (i < pWndSplitter2->GetRowCount())	  && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter2->SetRowInfo	(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]);
						nBufIndex += 2;
					}
					for (int i = 0; (i < pWndSplitter2->GetColumnCount()) && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter2->SetColumnInfo(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]);
						nBufIndex += 2;
					}
					pWndSplitter2->RecalcLayout();
				}
			if (pWndSplitter3)
				if (pWndSplitter3->m_hWnd)
				{
					for (int i = 0; (i < pWndSplitter3->GetRowCount())	  && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter3->SetRowInfo	(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]);
						nBufIndex += 2;
					}
					for (int i = 0; (i < pWndSplitter3->GetColumnCount()) && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter3->SetColumnInfo(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]);
						nBufIndex += 2;
					}
					pWndSplitter3->RecalcLayout();
				}
			if (pWndSplitter4)
				if (pWndSplitter4->m_hWnd)
				{
					for (int i = 0; (i < pWndSplitter4->GetRowCount())	  && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter4->SetRowInfo	(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]);
						nBufIndex += 2;
					}
					for (int i = 0; (i < pWndSplitter4->GetColumnCount()) && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter4->SetColumnInfo(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]);
						nBufIndex += 2;
					}
					pWndSplitter4->RecalcLayout();
				}
			if (pWndSplitter5)
				if (pWndSplitter5->m_hWnd)
				{
					for (int i = 0; (i < pWndSplitter5->GetRowCount())	  && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter5->SetRowInfo	(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]);
						nBufIndex += 2;
					}
					for (int i = 0; (i < pWndSplitter5->GetColumnCount()) && (nBufIndex < nBufSize - 1); i++)
					{
						pWndSplitter5->SetColumnInfo(i, (int)dwBuffer[nBufIndex], (int)dwBuffer[nBufIndex + 1]);
						nBufIndex += 2;
					}
					pWndSplitter5->RecalcLayout();
				}
		}

		if (lpPrivate)
		{
			DWORD dwSize = nSizePrivate;
			RegQueryValueEx(hKey, _T("PrivateData"), 0, NULL, (LPBYTE)lpPrivate, &dwSize);
		}

		RegCloseKey(hKey);

		rbStatusRestored = TRUE;
	}
	else
		rnCmdShow = SW_SHOWMAXIMIZED;

	if (prt->IsDerivedFrom(RUNTIME_CLASS(CMDIChildWnd)))
	{
		CMDIFrameWnd* pFrameWnd = ((CMDIChildWnd*)pWnd)->GetMDIFrame();
		ASSERT_VALID(pFrameWnd);
		BOOL bMaximized;
		if (pFrameWnd->MDIGetActive(&bMaximized))
		{
			if (bMaximized)
				rnCmdShow = SW_SHOWMAXIMIZED;
			else
				rnCmdShow = SW_SHOWNORMAL;
		}
	}
}

void CMainFrame::OnDestroy() 
{
	CMDIFrameWnd::OnDestroy();
	SaveFrameStatus(this);		// RestoreFrameStatus() in InitInstance()
}

void CMainFrame::OnClose() 
{
	CView* pView = theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)pView->GetParentFrame();
		if (pFrame)
			if (pFrame->m_pDlgMarkSet)
				if (pFrame->m_pDlgMarkSet->m_hWnd)
				{
					pFrame->m_pDlgMarkSet->OnClose();
					if (pFrame->m_pDlgMarkSet->m_hWnd)	// close rejected
						return;
				}
	}

	CMDIFrameWnd::OnClose();
}

void CMainFrame::OnHelp() 
{
	CString strLanguage(_T("Eng"));
	switch (theApp.settings.m_iLanguage)
	{
	case 0:  strLanguage = _T("Ger");	break;	// german
	case 1:  strLanguage = _T("Eng");	break;	// english
	default: break;								// default english
	}

	CString strHelpfile;
	strHelpfile.Format(_T("%s\\KIM7.1_Manual_%s.pdf"), theApp.settings.m_szWorkDir, strLanguage);
	ShellExecute(NULL, _T("open"), strHelpfile, NULL, NULL, SW_SHOWNORMAL);	
}

void CMainFrame::OnHelpFinder() 
{
	OnHelp();
}

void CMainFrame::OnNavJobMap() 
{
	// TODO: Code f�r Befehlsbehandlungsroutine hier einf�gen
	
}

void CMainFrame::ShowToolBarCtrl(int nID, BOOL bShow)
{
	for (int i = 0; i < m_wndNavigationBar.GetToolBarCtrl().GetButtonCount(); i++)
	{
		if (nID == m_wndNavigationBar.GetItemID(i))
		{
			m_wndNavigationBar.GetToolBarCtrl().SetState(nID, (bShow) ? TBSTATE_ENABLED : TBSTATE_HIDDEN);
			break;
		}
	}
}

void CMainFrame::OnNavAutoServer() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();	
	if (pDoc)
	{
		if ( ! pDoc->SaveModified())
			return;
		pDoc->OnCloseDocument();	// In debug version after OnCloseDocument() we get an assertion later on when next dialog is opened
		pDoc = NULL;				// Didn't find out any reason. Seems not to be critical, so ignore this bug so far.
	}
	m_nNavSelection = NavSelectionAutoServer;
	CMainFrame::CreateOrActivateAutoServerFrame(theApp.m_pAutoServerTemplate, pDoc);
}

void CMainFrame::OnUpdateNavAutoServer(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && strlen(theApp.settings.m_szAutoServerNameIP)) ? TRUE : FALSE);
	pCmdUI->SetCheck((m_nNavSelection == NavSelectionAutoServer) ? TRUE : FALSE);
	pCmdUI->Enable((theApp.m_bServerConnected) ? TRUE : FALSE);
}

void CMainFrame::OnNavAutoServerClose() 
{
	if (theApp.GetAutoServerFrame())
	{
		m_nNavSelection = 0;
		theApp.GetAutoServerFrame()->SendMessage(WM_CLOSE);
	}
}

void CMainFrame::OnUpdateNavAutoServerClose(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, (m_nNavSelection == NavSelectionAutoServer));
	pCmdUI->SetCheck(FALSE);

	ShowToolBarCtrl(ID_NAV_SEPARATOR_1, (m_nNavSelection == NavSelectionAutoServer) ? FALSE : TRUE);	// handle separators here in this OnUpdate, because seps don't have their own handlers
	ShowToolBarCtrl(ID_NAV_SEPARATOR_2, (m_nNavSelection == NavSelectionAutoServer) ? FALSE : TRUE);
}

void CMainFrame::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && CImpManDoc::GetDoc()) ? TRUE : FALSE);
	pCmdUI->SetCheck(FALSE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		if ((pDoc->m_nDongleStatus == CImpManApp::NoDongle) && ! pDoc->m_GlobalData.m_strJobTicketFile.IsEmpty())	// job created by JDF import w/o JDF-MIS license -> not allowed to be saved
			pCmdUI->Enable(FALSE);
		else
			pCmdUI->Enable(TRUE);
	}
}

void CMainFrame::OnUpdateFileNew(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && ! CImpManDoc::GetDoc()) ? TRUE : FALSE);
	pCmdUI->SetCheck(FALSE);
}

void CMainFrame::OnNavOrderdata() 
{
	m_nNavSelection = NavSelectionOrderData;
	CMainFrame::CreateOrActivateOrderDataFrame(theApp.m_pOrderDataTemplate, CImpManDoc::GetDoc());

	ShowToolBarCtrl(ID_NAV_SEPARATOR_1, (m_nNavSelection == NavSelectionAutoServer) ? FALSE : TRUE);	// handle separators here in this OnUpdate, because seps don't have their own handlers
	ShowToolBarCtrl(ID_NAV_SEPARATOR_2, (m_nNavSelection == NavSelectionAutoServer) ? FALSE : TRUE);
}

void CMainFrame::OnUpdateNavOrderdata(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && CImpManDoc::GetDoc()) ? TRUE : FALSE);
	pCmdUI->SetCheck((m_nNavSelection == NavSelectionOrderData) ? TRUE : FALSE);
}

void CMainFrame::OnNavPrintsheetlist() 
{
	m_nNavSelection = NavSelectionPrintSheets;
	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	if (pFrame)
	{
		if (pFrame->GetActSight() == ALLSIDES)
			pFrame->SetupViewAllSheets();
		else
			pFrame->SetupViewSingleSheet();
	}
}

void CMainFrame::OnUpdateNavPrintsheetlist(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && CImpManDoc::GetDoc()) ? TRUE : FALSE);
	pCmdUI->SetCheck((m_nNavSelection == NavSelectionPrintSheets) ? TRUE : FALSE);
}

void CMainFrame::OnNavPrintsheetMarks() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_nNavSelection = NavSelectionPrintSheetMarks;

	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, pDoc);
	if (pFrame->GetActSight() == ALLSIDES)
		pFrame->SetupViewMarksAllSheets();
	else
		pFrame->SetupViewMarksSingleSheet();
}

void CMainFrame::OnUpdateNavPrintsheetMarks(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && CImpManDoc::GetDoc()) ? TRUE : FALSE);
	pCmdUI->SetCheck((m_nNavSelection == NavSelectionPrintSheetMarks) ? TRUE : FALSE);
}

void CMainFrame::OnNavBookblock() 
{
	m_nNavSelection = NavSelectionBookBlock;
	CMainFrame::CreateOrActivateBookblockFrame(theApp.m_pBookBlockTemplate, CImpManDoc::GetDoc());
}

void CMainFrame::OnUpdateNavBookblock(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	BOOL bShow = ( (m_nNavSelection != NavSelectionAutoServer) && pDoc) ? ((pDoc->m_boundProducts.GetSize()) ? TRUE : FALSE) : FALSE;
	ShowToolBarCtrl(pCmdUI->m_nID, bShow);
	pCmdUI->SetCheck((m_nNavSelection == NavSelectionBookBlock) ? TRUE : FALSE);
}

void CMainFrame::OnNavPagelist() 
{
	m_nNavSelection = NavSelectionPageList;
	CMainFrame::CreateOrActivatePageListFrame(theApp.m_pPageListTemplate, CImpManDoc::GetDoc());
}

void CMainFrame::OnUpdateNavPagelist(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && CImpManDoc::GetDoc()) ? TRUE : FALSE);
	pCmdUI->SetCheck((m_nNavSelection == NavSelectionPageList) ? TRUE : FALSE);
}

void CMainFrame::OnNavPrepress() 
{
	m_nNavSelection = NavSelectionPrepress;
	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	if (pFrame)
	{
		if (pFrame->GetActSight() == ALLSIDES)
			pFrame->SetupViewPrepressAllSheets();
		else
			pFrame->SetupViewPrepressSingleSheet();
	}
}

void CMainFrame::OnUpdateNavPrepress(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && CImpManDoc::GetDoc()) ? TRUE : FALSE);
	pCmdUI->SetCheck((m_nNavSelection == NavSelectionPrepress) ? TRUE : FALSE);
}

void CMainFrame::OnNavOutput() 
{
	m_nNavSelection = NavSelectionOutput;
	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	if (pFrame)
	{
		if (pFrame->GetActSight() == ALLSIDES)
			pFrame->SetupViewOutputAllSheets();
		else
			pFrame->SetupViewOutputSingleSheet();
	}
}

void CMainFrame::OnUpdateNavOutput(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && CImpManDoc::GetDoc()) ? TRUE : FALSE);
	pCmdUI->SetCheck((m_nNavSelection == NavSelectionOutput) ? TRUE : FALSE);
}

void CMainFrame::OnNavJobDocumentation() 
{
	m_nNavSelection = NavSelectionJobDocumentation;
	CMainFrame::CreateOrActivateJobDocumentationFrame(theApp.m_pJobDocumentationTemplate, CImpManDoc::GetDoc());
}

void CMainFrame::OnUpdateNavJobDocumentation(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, ((m_nNavSelection != NavSelectionAutoServer) && CImpManDoc::GetDoc()) ? TRUE : FALSE);
	pCmdUI->SetCheck((m_nNavSelection == NavSelectionJobDocumentation) ? TRUE : FALSE);
}

void CMainFrame::OnNavResources() 
{
    CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_RESOURCES));
	CMenu* pPopup = menu.GetSubMenu(0);
	if ( ! pPopup)
		return;

    MENUITEMINFO mi;
    mi.cbSize	  = sizeof(MENUITEMINFO);
    mi.fMask	  = MIIM_TYPE | MIIM_DATA;
    mi.fType	  = MFT_OWNERDRAW;	//   Making the menu item ownerdrawn:
	mi.dwItemData = (ULONG_PTR)menu.m_hMenu;

	for (int i = 0; i < pPopup->GetMenuItemCount(); i++)
	{
		pPopup->SetMenuItemInfo(i, &mi, TRUE);
		CMenu* pSubMenu = pPopup->GetSubMenu(i);
		if (pSubMenu)
		{
			for (int j = 0; j < pSubMenu->GetMenuItemCount(); j++)
				pSubMenu->SetMenuItemInfo(j, &mi, TRUE);
		}
	}

	CRect rcClient;
	m_wndNavigationBar.GetItemRect(m_wndNavigationBar.CommandToIndex(ID_NAV_RESOURCES), rcClient);
	ClientToScreen(&rcClient);

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rcClient.TopLeft().x, rcClient.bottom, this);
}

void CMainFrame::OnUpdateNavResources(CCmdUI* pCmdUI) 
{
	ShowToolBarCtrl(pCmdUI->m_nID, TRUE);
	pCmdUI->SetCheck(FALSE);
}

void CMainFrame::OnImportJdf()
{
	if ( ! (theApp.m_nAddOnsLicensed & CImpManApp::JDFMIS))
		KIMOpenLogMessage(IDS_NO_ADDON_LICENSE);

	CString strFilter = "JDF File (*.jdf)|*.jdf|Alle Dateien (*.*)|*.*||";
	CFileDialog dlgFile(TRUE, _T("jdf"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING | OFN_DONTADDTORECENT | OFN_NOCHANGEDIR, strFilter, this);

	CString strTitle; strTitle.LoadString(IDS_IMPORT_JDF_JOBTICKET);

	CString strInitialFolder	  = theApp.GetRegistryKey(_T("JDFInputFolder"), theApp.GetDefaultInstallationFolder() + _T("\\JDF-Input"));
	dlgFile.m_ofn.lpstrInitialDir = strInitialFolder.GetBuffer(MAX_PATH);
	dlgFile.m_ofn.lpstrTitle = strTitle;
 
	if (dlgFile.DoModal() == IDCANCEL)
		return;

	strInitialFolder.ReleaseBuffer();
	strInitialFolder = dlgFile.GetPathName();
	strInitialFolder.TrimRight(dlgFile.GetFileName());
	theApp.SetRegistryKey(_T("JDFInputFolder"), strInitialFolder);

	BeginWaitCursor();

	theApp.m_bInsideInitInstance = TRUE;
	theApp.OnFileNew();
	theApp.m_bInsideInitInstance = FALSE;

	CJDFInputMIS jdfInputMIS;
	jdfInputMIS.Open(dlgFile.GetPathName());

	EndWaitCursor();
}

void CMainFrame::OnExportJdf()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strFilter = "JDF File (*.jdf)|*.jdf|Alle Dateien (*.*)|*.*||";
	CFileDialog dlgFile(FALSE, _T("jdf"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_ENABLESIZING, strFilter, this);

	CString strTitle; strTitle.LoadString(IDS_EXPORT_JDF_JOBTICKET);

	CString strCurrentFile = pDoc->m_GlobalData.m_strJobTicketFile;
	dlgFile.m_ofn.lpstrTitle = strTitle;

	if (dlgFile.DoModal() == IDCANCEL)
		return;

	BeginWaitCursor();

	CJDFOutputMIS jdfOutputMIS;
	jdfOutputMIS.Export(pDoc->m_GlobalData.m_strJobTicketFile, dlgFile.GetPathName());

	EndWaitCursor();
}

void CMainFrame::OnUpdateImportJdf(CCmdUI* pCmdUI)
{
	//if (theApp.m_bServerConnected)
	//	if (pCmdUI->m_pMenu!=NULL)
	//	{
	//		pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
	//		return;
	//	}

	pCmdUI->Enable(TRUE);
}

void CMainFrame::OnUpdateExportJdf(CCmdUI* pCmdUI)
{
	//if (theApp.m_bServerConnected)
	//	if (pCmdUI->m_pMenu!=NULL)
	//	{
	//		pCmdUI->m_pMenu->DeleteMenu(pCmdUI->m_nID, MF_BYCOMMAND);
	//		return;
	//	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
	{
		pCmdUI->Enable(FALSE);
		return;
	}
	if ( pDoc->m_GlobalData.m_strJobTicketFile.IsEmpty())	
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

void CMainFrame::OnModifyOrderData()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgJobData dlgJobData(IDD_ORDERDATA, 0, (ULONG_PTR)0);
	if (dlgJobData.DoModal() == IDCANCEL)
		return;
	else
		pDoc->SetModifiedFlag();

	pDoc->UpdateAllViews(NULL);
} 

void CMainFrame::OnModifyProduct()
{
	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;

	//CDlgJobData dlgJobData(IDD_PRODUCTDATA, 0, (ULONG_PTR)0);
	//if (dlgJobData.DoModal() == IDCANCEL)
	//	return;
	//else
	//	pDoc->SetModifiedFlag();

	//pDoc->UpdateAllViews(NULL);
}

void CMainFrame::OnModifyStripping()
{
	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;

	//CDlgJobData dlgJobData(IDD_PRODUCTIONPLANNER, 0, (ULONG_PTR)0);//nProductPartIndex);
	//if (dlgJobData.DoModal() == IDCANCEL)
	//	return;
	//else
	//	pDoc->SetModifiedFlag();

	//pDoc->UpdateAllViews(NULL);
}

void CMainFrame::OnResourceSheetformats()
{
	CDlgSheet dlg;  
	dlg.DoModal();
}

void CMainFrame::OnResourcePageformats()
{
	CDlgPageFormats dlg;
	dlg.DoModal();
}

void CMainFrame::OnResourcePapers()
{
	CDlgPaperDefs dlg;
	dlg.DoModal();
}

void CMainFrame::OnResourceProductionprofiles()
{
	CDlgProductionManager dlg;
	dlg.DoModal();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
}

void CMainFrame::OnResourceControlMarks()
{
	CDlgMarkSet* pDlgMarkSet		  = new CDlgMarkSet(this, TRUE);
	pDlgMarkSet->m_bBrowseOnly		  = TRUE;
	pDlgMarkSet->m_strMarksetFullPath = CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets");
	pDlgMarkSet->DoModal();
	delete pDlgMarkSet;
}

void CMainFrame::OnResourcePrintSheetOutputProfiles()
{
	CDlgPDFOutputMediaSettings* pDlgOutputProfile = new CDlgPDFOutputMediaSettings(this, TRUE);
	pDlgOutputProfile->m_nOutputTarget			  = CPDFTargetList::PrintSheetTarget;
	pDlgOutputProfile->m_bNoSheetSelection		  = TRUE;
	pDlgOutputProfile->m_nWhatSheets			  = -1;	
	pDlgOutputProfile->m_strWhatLayout			  = _T("");
	pDlgOutputProfile->DoModal();
	delete pDlgOutputProfile;
}

void CMainFrame::OnResourceFoldSheetOutputProfiles()
{
	CDlgPDFOutputMediaSettings* pDlgOutputProfile = new CDlgPDFOutputMediaSettings(this, TRUE);
	pDlgOutputProfile->m_nOutputTarget			  = CPDFTargetList::FoldSheetTarget;
	pDlgOutputProfile->m_bNoSheetSelection		  = TRUE;
	pDlgOutputProfile->m_nWhatSheets			  = -1;	
	pDlgOutputProfile->m_strWhatLayout			  = _T("");
	pDlgOutputProfile->DoModal();
	delete pDlgOutputProfile;
}

void CMainFrame::OnResourceBookletOutputProfiles()
{
	CDlgPDFOutputMediaSettings* pDlgOutputProfile = new CDlgPDFOutputMediaSettings(this, TRUE);
	pDlgOutputProfile->m_nOutputTarget			  = CPDFTargetList::BookletTarget;
	pDlgOutputProfile->m_bNoSheetSelection		  = TRUE;
	pDlgOutputProfile->m_nWhatSheets			  = -1;	
	pDlgOutputProfile->m_strWhatLayout			  = _T("");
	pDlgOutputProfile->DoModal();
	delete pDlgOutputProfile;
}

//----------------------------------------------------------------
// This function finds the CMDIChildWnd in the list of windows
// maintained by the application's MDIClient window following the
// one pointed to by the member variable m_pWndCurrentChild. If no
// further CMDIChildWnds are in the list, NULL is returned.
//----------------------------------------------------------------

CMDIChildWnd* CMainFrame::GetNextMDIChildWnd()
{
   if (!m_pWndCurrentChild)
     {
      // Get the first child window.
      m_pWndCurrentChild = m_wndMDIClient.GetWindow(GW_CHILD);
     }
   else
     {
      // Get the next child window in the list.
        m_pWndCurrentChild=
           (CMDIChildWnd*)m_pWndCurrentChild->GetWindow(GW_HWNDNEXT);
     }

   if (!m_pWndCurrentChild)
     {
      // No child windows exist in the MDIClient,
      // or you are at the end of the list. This check
      // will terminate any recursion.
      return NULL;
     }

  // Check the kind of window
    if (!m_pWndCurrentChild->GetWindow(GW_OWNER))
      {
        if (m_pWndCurrentChild->
                           IsKindOf(RUNTIME_CLASS(CMDIChildWnd)))
          {
                 // CMDIChildWnd or a derived class.
                 return (CMDIChildWnd*)m_pWndCurrentChild;
          }
        else
          {
                 // Window is foreign to the MFC framework.
                 // Check the next window in the list recursively.
                 return GetNextMDIChildWnd();
          }
      }
    else
      {
          // Title window associated with an iconized child window.
          // Recurse over the window manager's list of windows.
          return GetNextMDIChildWnd();
      }
}


BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
	CKIMCommandLineInfo clInfo; clInfo.m_nParam = -1;

	typedef struct 
	{
		TCHAR szCommand[1000];
		int	  nJobID;
		TCHAR szParam1[1000];
		TCHAR szParam2[1000];
		TCHAR szParam3[1000];
		TCHAR szParam4[1000];
		TCHAR szParam5[1000];
	}DATASTRUCT;
	DATASTRUCT* pData = ((DATASTRUCT*)pCopyDataStruct->lpData);

	CString strCommand(pData->szCommand);
	clInfo.m_strJobTemplate		= CString(pData->szParam1);
	clInfo.m_nJobID				= pData->nJobID;
	clInfo.m_strPDFDoc			= CString(pData->szParam2);
	clInfo.m_strStartPage		= CString(pData->szParam3);
	clInfo.m_strPDFInfoFile		= CString(pData->szParam4);
	clInfo.m_strPreviewsFolder	= CString(pData->szParam5);

	strCommand.MakeUpper();
	if (strCommand == "IB")	// is busy?
	{
		if (clInfo.m_nJobID >= 0)
		{
			if (theApp.m_nDBCurrentJobID == clInfo.m_nJobID)
				return 1;
			return 0;
		}
		else
			return (CImpManDoc::GetDoc()) ? 1 : 0;
	}

	if (strCommand == "XX")	// abort
	{
		if (clInfo.m_nJobID >= 0)
		{
			if (theApp.m_nDBCurrentJobID == clInfo.m_nJobID)
				theApp.m_bKimEngineAbort = TRUE;
		}
		else
			theApp.m_bKimEngineAbort = TRUE;
		return  0;
	}

	if (strCommand == "N")	// new template
		clInfo.m_nKIMCommand = CImpManApp::CmdNew;

	if (strCommand == "O")	// open template
		clInfo.m_nKIMCommand = CImpManApp::CmdOpen;

	if (strCommand == "OL")	// open template's output list
		clInfo.m_nKIMCommand = CImpManApp::CmdOpenOutputList;

	if (strCommand == "OI")	// get last output information (PDFSheets)
		clInfo.m_nKIMCommand = CImpManApp::CmdGetOutputInfo;

	if (strCommand == "CR")	// check registered pdf docs
		clInfo.m_nKIMCommand = CImpManApp::CmdCheckRegisteredPDFs;

	if (strCommand == "IR")	// register pdf doc
		clInfo.m_nKIMCommand = CImpManApp::CmdIsRegistered;

	if (strCommand == "R")	// register pdf doc
		clInfo.m_nKIMCommand = CImpManApp::CmdRegister;

	if (strCommand == "UR")	// register pdf doc
		clInfo.m_nKIMCommand = CImpManApp::CmdUnregister;

	if (strCommand == "I")	// impose
		clInfo.m_nKIMCommand = CImpManApp::CmdImpose;

	if (strCommand == "IQ")	// impose quiet
		clInfo.m_nKIMCommand = CImpManApp::CmdImposeQuiet;

	if (strCommand == "IC")	// impose csv file
		clInfo.m_nKIMCommand = CImpManApp::CmdImposeCSV;

	if (strCommand == "IX")	// impose xml file
		clInfo.m_nKIMCommand = CImpManApp::CmdImposeXML;

	if (strCommand == "IJ")	// impose jdf file
		clInfo.m_nKIMCommand = CImpManApp::CmdImposeJDF;

	if (strCommand == "DO")	// do output
		clInfo.m_nKIMCommand = CImpManApp::CmdDoOutput;

	if (strCommand == "RD")	// reset page source (pdf doc)
		clInfo.m_nKIMCommand = CImpManApp::CmdResetPageSource;

	if (strCommand == "XD")	// reset page source (pdf doc) w/o resetting page numbers to originals, because doc is about to be exchanged (new version)
		clInfo.m_nKIMCommand = CImpManApp::CmdResetPageSourceX;

	if (strCommand == "RP")	// reset pagelist
		clInfo.m_nKIMCommand = CImpManApp::CmdResetPageList;

	if (strCommand == "P")	// show pagelist
		clInfo.m_nKIMCommand = CImpManApp::CmdPageList;

	if (strCommand == "Q")	// quiet mode - don't show user interface
		clInfo.m_bQuiet = TRUE;


	theApp.m_bInsideInitInstance = TRUE;	// simulate same behaviour as command line call

	theApp.m_nDBCurrentJobID		 = clInfo.m_nJobID;
	theApp.m_bKimEngineAbort		 = FALSE;
	theApp.m_strDBCurrentJobFullpath = clInfo.m_strJobTemplate;

	int	nError;
	int nRet = theApp.ProcessCommand(clInfo, nError);

	if (theApp.m_bKimEngineAbort) 
	{
		KIMLogMessage(IDS_ABORTED_BY_USER, MB_ICONWARNING);
		nRet = -2;
	}

	theApp.m_nDBCurrentJobID		 = -1;
	theApp.m_bKimEngineAbort		 = FALSE;
	theApp.m_strDBCurrentJobFullpath = _T("");

	theApp.m_bInsideInitInstance = FALSE;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();	
	if (pDoc)
		pDoc->OnCloseDocument();	

#ifdef KIM_ENGINE
    PROCESS_MEMORY_COUNTERS pmc;
    GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	if (pmc.PagefileUsage > 500000000L)
	{
		theApp.m_nAppError = nRet;
		OnClose();
	}
#endif

	return nRet;
}

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	CFrameWnd* pFrame = GetActiveFrame();

	if (pFrame->IsKindOf(RUNTIME_CLASS(CAutoServerFrame)))
		m_nNavSelection = CMainFrame::NavSelectionAutoServer; 
	else
	if (pFrame->IsKindOf(RUNTIME_CLASS(COrderDataFrame)))
		m_nNavSelection = CMainFrame::NavSelectionOrderData;
	else
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
	{
		if ( ((CNewPrintSheetFrame*)pFrame)->GetPrintComponentsView())
			m_nNavSelection = CMainFrame::NavSelectionPrintSheets;
		else
		if ( ((CNewPrintSheetFrame*)pFrame)->GetProdDataPrintView())
			m_nNavSelection = CMainFrame::NavSelectionPrintSheetMarks;
		else
			m_nNavSelection = CMainFrame::NavSelectionOutput;
	}
	else
	if (pFrame->IsKindOf(RUNTIME_CLASS(CBookblockFrame)))
		m_nNavSelection = CMainFrame::NavSelectionBookBlock;
	else
	if (pFrame->IsKindOf(RUNTIME_CLASS(CPageListFrame)))
		m_nNavSelection = CMainFrame::NavSelectionPageList;
	else
	if (pFrame->IsKindOf(RUNTIME_CLASS(CJobDocumentationFrame)))
		m_nNavSelection = CMainFrame::NavSelectionJobDocumentation;
	else
		m_nNavSelection = 0;

	return CMDIFrameWnd::PreTranslateMessage(pMsg);
}
