#if !defined(AFX_DLGMULTISELECT_H__81A3F634_E9D3_11D2_9547_204C4F4F5020__INCLUDED_)
#define AFX_DLGMULTISELECT_H__81A3F634_E9D3_11D2_9547_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgMultiSelect.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMultiSelect 

class CDlgMultiSelect : public CDialog
{
// Konstruktion
public:
	CDlgMultiSelect(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgMultiSelect)
	enum { IDD = IDD_MULTISELECT };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgMultiSelect)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgMultiSelect)
	afx_msg void OnSelectAll();
	afx_msg void OnSelectMarks();
	afx_msg void OnSelectPages();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGMULTISELECT_H__81A3F634_E9D3_11D2_9547_204C4F4F5020__INCLUDED_
