#pragma once


// CDlgPlaceholderSyntaxInfo-Dialogfeld

class CDlgPlaceholderSyntaxInfo : public CDialog
{
	DECLARE_DYNAMIC(CDlgPlaceholderSyntaxInfo)

public:
	CDlgPlaceholderSyntaxInfo(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgPlaceholderSyntaxInfo();

// Dialogfelddaten
	enum { IDD =  IDD_PLACEHOLDER_SYNTAX_INFO};

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
};
