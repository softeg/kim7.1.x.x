#if !defined(AFX_PRODUCTPARTSVIEW_H__DFD28D33_52AA_42C8_9FFD_1E902C8FA307__INCLUDED_)
#define AFX_PRODUCTPARTSVIEW_H__DFD28D33_52AA_42C8_9FFD_1E902C8FA307__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProductPartsView.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Formularansicht CProductPartsView 

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CProductPartsView : public CFormView
{
protected:
	CProductPartsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	DECLARE_DYNCREATE(CProductPartsView)

// Formulardaten
public:
	//{{AFX_DATA(CProductPartsView)
	enum { IDD = IDD_PRODUCTPARTSVIEW_FORM };
	CSpinButtonCtrl	m_spineTrimSpin;
	CSpinButtonCtrl	m_millingEdgeSpin;
	CSpinButtonCtrl	m_sideTrimSpin;
	CSpinButtonCtrl	m_overFoldSpin;
	CSpinButtonCtrl	m_headTrimSpin;
	CSpinButtonCtrl	m_footTrimSpin;
	CTabCtrl	m_productPartsTab;
	CListCtrl	m_ppFoldSheetTypeList;
	CListCtrl	m_ppPrintSheetList;
	CListCtrl	m_ppFoldSheetList;
	CString	m_strPrintMachine;
	CString	m_strPlateFormat;
	int		m_nFoldSheetTypeNum;
	CString	m_strNumPages;
	CString	m_strPagesNotAssigned;
	int		m_nBindingMethod;
	int		m_nProductionType;
	CString	m_strFoldSheet;
	CString	m_strPrintLayout;
	CString	m_strMarkset;
	CString	m_strPaperFormat;
	CString	m_strPrintingType;
	CString	m_strTurningType;
	CString	m_strShinglingFoot;
	CString	m_strShinglingHead;
	CString	m_strPageFormat;
	float	m_fFootTrim;
	float	m_fHeadTrim;
	BOOL	m_bOverFoldBack;
	float	m_fOverFold;
	BOOL	m_bOverFoldFront;
	float	m_fSideTrim;
	BOOL	m_bTrimsIdentical;
	float	m_fMillingEdge;
	float	m_fSpineTrim;
	//}}AFX_DATA

// Attribute
public:
	CFont								m_smallFont, m_mediumFont, m_bigFont;
	CThemedTabCtrl						m_themedTabCtrl;
	CBrush								m_BkBrush;
	CRect								m_layoutPreviewFrame;
	CRect								m_foldSheetPreviewFrame;
	CRect								m_bookblockPreviewFrame;
	int									m_nLayoutSide;
	CArray <CFoldSheet*, CFoldSheet*>	m_foldSheetTypes;
	CArray <int, int>					m_foldSheetTypeNums;



// Operationen
public:
	void			GetThemedTabCtrlBackground();
	int				GetCurrentProductPartIndex();
	void			ShowControls(BOOL bShow1, BOOL bShow2);
	void			SetModified();
	void			InitializeLists();
	void			InitializeFoldSheetList();
	void			InitializeFoldSheetTypeList();
	void			InitializePrintSheetList();
	CPrintSheet*	GetCurrentPrintSheet();
	CLayout*		GetCurrentLayout();
	int				GetCurrentLayoutSide();
	CFoldSheet*		GetCurrentFoldSheet(int* pNum = NULL);
	CFoldSheetLayer* GetCurrentFoldSheetLayer();
	void			DrawPrintSheetLayout(CDC* pDC);
	void			DrawBookBlock(CDC* pDC);
	void			DrawFoldSheet(CDC* pDC);
	void			UpdateAll();


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CProductPartsView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementierung
protected:
	virtual ~CProductPartsView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CProductPartsView)
	afx_msg void OnPaint();
	afx_msg void OnItemchangedPpFoldsheettypelist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedPpFoldsheetlist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPpRemoveFoldsheet();
	afx_msg void OnDeltaposPpNumfoldsheetSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPpAddFoldsheet();
	afx_msg void OnSelchangePpBindingMethod();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSelchangeProductpartsTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPpRemoveFoldsheetButton();
	afx_msg void OnPpAddFoldsheetButton();
	afx_msg void OnPpModifyLayout();
	afx_msg void OnPpShingling();
	afx_msg void OnPpPrintsheetDetails();
	afx_msg void OnPpBookblockDetails();
	afx_msg void OnDeltaposFoottrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposHeadtrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposMillingedgeSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposOverfoldSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSidetrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpinetrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTrimsIdentical();
	afx_msg void OnKillfocusFoottrimEdit();
	afx_msg void OnKillfocusHeadtrimEdit();
	afx_msg void OnKillfocusSpinetrimEdit();
	afx_msg void OnKillfocusMillingedgeEdit();
	afx_msg void OnKillfocusOverfoldEdit();
	afx_msg void OnKillfocusSidetrimEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_PRODUCTPARTSVIEW_H__DFD28D33_52AA_42C8_9FFD_1E902C8FA307__INCLUDED_
