// DlgProdDataFoldPrint.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProdDataFoldPrint.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "BookBlockView.h"
#include "Shlwapi.h"
#include "DlgPressDevice.h"
#include "DlgSheet.h"
#include "ProdDataFoldPrintView.h"
#include "ProdDataWizard.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "DlgModifyMark.h"
#include "DlgShinglingAssistent.h"
#include "DlgFoldSchemeBrowser.h"
#include "ModalFrame.h"
#include "ProdDataPrintView.h"
#include "ProductionDataFrame.h"
#include "GraphicComponent.h"



#define ID_MODIFY_FOLDSHEET 9999

// CLayoutPreviewFrame

IMPLEMENT_DYNAMIC(CButtonModifyFoldSheet, CButton)

CButtonModifyFoldSheet::CButtonModifyFoldSheet()
{
	m_bMouseOverButton = FALSE;
}

CButtonModifyFoldSheet::~CButtonModifyFoldSheet()
{
}


BEGIN_MESSAGE_MAP(CButtonModifyFoldSheet, CButton)
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()


void CButtonModifyFoldSheet::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	HTHEME hThemeButton = theApp._OpenThemeData(GetParent(), L"COMBOBOX");

	CRect rcItem = lpDrawItemStruct->rcItem;
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

	if (hThemeButton)
	{
		if (lpDrawItemStruct->itemState & ODS_SELECTED)
			DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), CP_DROPDOWNBUTTON, CBXS_PRESSED,		&rcItem, 0);	
		else
		{
			CPoint ptMouse;
			GetCursorPos(&ptMouse);
			CRect rcButton = rcItem;
			ClientToScreen(rcButton);
			if (m_bMouseOverButton)
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), CP_DROPDOWNBUTTON, CBXS_HOT,		&rcItem, 0);	
			else
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), CP_DROPDOWNBUTTON, CBXS_NORMAL,	&rcItem, 0);	
		}
	}
	else
	{
		if (lpDrawItemStruct->itemState & ODS_SELECTED)
			DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);	
		else
			DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_TRANSPARENT);	
		
		CImage img;
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_DROPDOWN));
		img.TransparentBlt(pDC->m_hDC, rcItem.left + 4, rcItem.top + 8, img.GetWidth(), img.GetHeight(), WHITE);
	}

	theApp._CloseThemeData(hThemeButton);
}

void CButtonModifyFoldSheet::OnMouseMove(UINT nFlags, CPoint point)
{
	if ( ! m_bMouseOverButton)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = this->m_hWnd;
		if (::_TrackMouseEvent(&tme))
			m_bMouseOverButton = TRUE;
		InvalidateRect(NULL);
	}

	CButton::OnMouseMove(nFlags, point);
}

LRESULT CButtonModifyFoldSheet::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	m_bMouseOverButton = FALSE;
	InvalidateRect(NULL);
	UpdateWindow();
	return TRUE;
}


// CLayoutPreviewFrame

IMPLEMENT_DYNAMIC(CLayoutPreviewFrame, CStatic)

CLayoutPreviewFrame::CLayoutPreviewFrame()
{
	m_DisplayList.Create(this);
}

CLayoutPreviewFrame::~CLayoutPreviewFrame()
{
}


BEGIN_MESSAGE_MAP(CLayoutPreviewFrame, CStatic)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_MODIFY_FOLDSHEET, ModifyFoldSheet)
END_MESSAGE_MAP()


void CLayoutPreviewFrame::ModifyFoldSheet()
{
	CDlgProdDataFoldPrint* pDlg = (CDlgProdDataFoldPrint*)GetParent();
	if ( ! pDlg)
		return;

	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU_MODIFY_FOLDSHEET));
	CMenu* pPopup = menu.GetSubMenu(0);
	if ( ! pPopup)
		return;

    MENUITEMINFO mi;
    mi.cbSize = sizeof(MENUITEMINFO);
    mi.fMask = MIIM_TYPE | MIIM_DATA;
    mi.fType = MFT_OWNERDRAW;	//     Making the menu item ownerdrawn:
	mi.dwItemData = (ULONG_PTR)menu.m_hMenu;
	pPopup->SetMenuItemInfoA(ID_FOLDSHEET_ROTATE,	&mi);
	pPopup->SetMenuItemInfoA(ID_FOLDSHEET_TURN,		&mi);
	pPopup->SetMenuItemInfoA(ID_FOLDSHEET_TUMBLE,	&mi);
	pPopup->SetMenuItemInfoA(ID_FOLDSHEET_REMOVE,	&mi);

	CRect rcClient;
	m_modifyFoldSheetButton.GetWindowRect(rcClient);

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, rcClient.TopLeft().x, rcClient.TopLeft().y, GetParent());
}

void CLayoutPreviewFrame::OnLButtonDown(UINT nFlags, CPoint point)
{
//	m_DisplayList.OnLButtonDown(point); // pass message to display list

	CStatic::OnLButtonDown(nFlags, point);
}

void CLayoutPreviewFrame::OnMouseMove(UINT nFlags, CPoint point)
{
	CClientDC dc(this);

	BOOL bInsideAny = FALSE;
	for (int i = 0; i < m_DisplayList.m_List.GetSize(); i++)
	{
		if (m_DisplayList.m_List[i].m_BoundingRect.PtInRect(point))
		{
			if ( ! m_modifyFoldSheetButton.m_hWnd)
				m_modifyFoldSheetButton.Create(_T(""), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON|BS_OWNERDRAW, CRect(m_DisplayList.m_List[i].m_BoundingRect.CenterPoint() - CSize(9,9), CSize(19, 19) ), this, ID_MODIFY_FOLDSHEET);
			if (m_modifyFoldSheetButton.m_hWnd)
				if ( ! m_modifyFoldSheetButton.IsWindowVisible())
				{
					m_modifyFoldSheetButton.MoveWindow(CRect(m_DisplayList.m_List[i].m_BoundingRect.CenterPoint() - CSize(9,9), CSize(19, 19) ));
					m_modifyFoldSheetButton.ShowWindow(SW_SHOW);
				}

			CDlgProdDataFoldPrint* pDlg = (CDlgProdDataFoldPrint*)GetParent();
			if (pDlg)
				pDlg->m_nActLayerRefIndex = (int)m_DisplayList.m_List[i].m_pData;
			bInsideAny = TRUE;
		}
	}

	if ( ! bInsideAny)
		if (m_modifyFoldSheetButton.m_hWnd)
			if (m_modifyFoldSheetButton.IsWindowVisible())
				m_modifyFoldSheetButton.ShowWindow(SW_HIDE);

	CStatic::OnMouseMove(nFlags, point);
}


// CLayoutPreviewFrame-Meldungshandler

void CLayoutPreviewFrame::OnPaint()
{
	CDlgProdDataFoldPrint* pDlg = (CDlgProdDataFoldPrint*)GetParent();
	if ( ! pDlg)
		return;

	CPaintDC dc(this); // device context for painting

	CRect rcFrame;
	GetClientRect(rcFrame);
	dc.FillSolidRect(rcFrame, RGB(144, 153, 174));

	if ( ! pDlg->m_pFoldSheet || ! pDlg->m_pPrintSheet || ! pDlg->m_pLayout)
		return;
//	int nActLayerRefIndex = m_pPrintSheet->GetNUpsFoldSheetLayerRefIndex(m_pFoldSheet->m_strFoldSheetTypeName, 0, m_nActNUP + 1);
	if ( (pDlg->m_nActLayerRefIndex < 0) || (pDlg->m_nActLayerRefIndex >= pDlg->m_pPrintSheet->m_FoldSheetLayerRefs.GetCount()) )
	{
		dc.SelectObject(&pDlg->m_smallFont);
		dc.SetBkMode(TRANSPARENT);
		dc.SetTextColor(WHITE);
		CImpManDoc*  pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			CString strOut;
			strOut.Format(_T("\n\nEs verbleiben %d Seiten zum Zuordnen eines Falzschemas."), pDoc->m_PageTemplateList.GetNumUnimposedPages(pDlg->m_nProductPartIndex));
			strOut += _T("\n\nW�hlen Sie bitte die Standbogendaten und ein Falzschema aus!");
			dc.DrawText(strOut, rcFrame, DT_VCENTER | DT_CENTER);
		}
	}
	else
	{
		CImpManDoc*  pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return;
		CLayout*	  pLayout		= pDoc->m_PrintSheetList.m_Layouts.FindLayout(pDlg->m_pLayout->m_strLayoutName);
		CPrintSheet*  pPrintSheet	= (pLayout)		? pLayout->GetFirstPrintSheet() : NULL;
		CFoldSheet*	  pFoldSheet	= (pPrintSheet)	? pPrintSheet->GetFoldSheet(pDlg->m_nActLayerRefIndex) : NULL;
		CPressDevice* pPressDevice	= (pLayout)		? pLayout->m_FrontSide.GetPressDevice() : NULL;

		if (pLayout)
		{
			m_DisplayList.Invalidate();	
			m_DisplayList.BeginRegisterItems(&dc);

			CMediaSize*	pMediaSize = (pDlg->m_pDlgOutputProfile) ? pDlg->m_pDlgOutputProfile->GetCurMedia(pPrintSheet) : NULL;
			pLayout->Draw(&dc, rcFrame, pPrintSheet, pPressDevice, -1, pFoldSheet, FALSE, pDlg->m_bNUpMode, &pDlg->m_markList, pMediaSize, &m_DisplayList);

			m_DisplayList.EndRegisterItems(&dc);
		}
	}
}


// CFPCustomPaintFrame

IMPLEMENT_DYNAMIC(CFPCustomPaintFrame, CStatic)

CFPCustomPaintFrame::CFPCustomPaintFrame()
{

}

CFPCustomPaintFrame::~CFPCustomPaintFrame()
{
}


BEGIN_MESSAGE_MAP(CFPCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// CFPCustomPaintFrame-Meldungshandler

void CFPCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CDlgProdDataFoldPrint* pDlg = (CDlgProdDataFoldPrint*)GetParent();
	if ( ! pDlg)
		return;
	CRect rcFrame;
	GetClientRect(rcFrame);
	CString strText;
	switch (GetDlgCtrlID())
	{
	case IDC_PD_PRODUCTPART_FRAME:		pDlg->DrawProductPartInfos(&dc,	rcFrame);	
										break;

	case IDC_PD_PREVIEW_HEADER:			{
											CGraphicComponent gp;
											gp.DrawTitleBar(&dc, rcFrame, L"", RGB(255,240,180), RGB(235,220,160), 90, FALSE);
											CRect rcText = rcFrame; rcText.left += 10;
											strText = _T("Falzschema: ");
											dc.SetTextColor(LIGHTBLUE);
											dc.DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

											rcText.left += dc.GetTextExtent(strText).cx;
											strText = (pDlg->m_pFoldSheet) ? pDlg->m_pFoldSheet->m_strFoldSheetTypeName : L"";
											dc.SetTextColor(GUICOLOR_CAPTION);
											dc.DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

											rcText.left += dc.GetTextExtent(strText).cx + 20;
											strText = _T("Standbogen: ");
											dc.SetTextColor(LIGHTBLUE);
											dc.DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

											rcText.left += dc.GetTextExtent(strText).cx;
											strText = (pDlg->m_pLayout) ? pDlg->m_pLayout->m_strLayoutName : L"";
											dc.SetTextColor(GUICOLOR_CAPTION);
											dc.DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

											CImpManDoc* pDoc = CImpManDoc::GetDoc();
											if ( ! pDoc)
												break;
											if (pDoc->m_productPartList.GetSize() <= 1)
												break;
											rcText.left += dc.GetTextExtent(strText).cx + 20;
											strText = _T("Produktteil: ");
											dc.SetTextColor(LIGHTBLUE);
											dc.DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

											rcText.left += dc.GetTextExtent(strText).cx;
											strText = (pDlg->GetActProductPart()) ? pDlg->GetActProductPart()->m_strName : L"";
											dc.SetTextColor(GUICOLOR_CAPTION);
											dc.DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
										}
										break;

	case IDC_PD_FOLDING_GROUP:	
	case IDC_PD_PRINTING_GROUP:			
	case IDC_PD_PREPRESS_GROUP:			{
											CPen pen(PS_SOLID, 1, LIGHTGRAY);
											dc.SelectObject(&pen);
											CBrush brush(GUICOLOR_GROUP);
											dc.SelectObject(&brush);
											dc.RoundRect(rcFrame, CPoint(8, 8));

											switch (GetDlgCtrlID())
											{
											case IDC_PD_FOLDING_GROUP:	strText = _T("Falzschema"); break; //strText.LoadStringA(IDS_PROCESS_FOLDING);	break;
											//case IDC_PD_PRINTING_GROUP:	strText.LoadStringA(IDS_PROCESS_PRINTING);	break;		
											case IDC_PD_PREPRESS_GROUP:	strText.LoadStringA(IDS_PROCESS_PREPRESS);	break;
											}
											if (strText.IsEmpty())
												break;
											
											dc.SelectStockObject(NULL_PEN);
											dc.FillSolidRect(CRect(rcFrame.left + 8, rcFrame.top, rcFrame.left + dc.GetTextExtent(strText).cx, rcFrame.top + 10), GUICOLOR_GROUP);

											dc.SelectObject(pDlg->m_smallFont);
											dc.SetBkMode(TRANSPARENT);
											dc.SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
											rcFrame.DeflateRect(12, -7);
											dc.DrawText(strText, rcFrame, DT_LEFT | DT_TOP);
										}
										break;
	default:	break;
	}
}



// CToolBarContainer

IMPLEMENT_DYNAMIC(CToolBarContainer, CStatic)

CToolBarContainer::CToolBarContainer()
{
}

CToolBarContainer::~CToolBarContainer()
{
}


BEGIN_MESSAGE_MAP(CToolBarContainer, CStatic)
	ON_WM_DRAWITEM()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CToolBarContainer-Meldungshandler

void CToolBarContainer::DrawItem(LPDRAWITEMSTRUCT)
{
}

BOOL CToolBarContainer::OnEraseBkgnd(CDC* pDC)
{
	CRect rcRect;
	GetClientRect(rcRect);
	pDC->FillSolidRect(rcRect, GUICOLOR_GROUP);

	return TRUE;
}

BOOL CToolBarContainer::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
	case 6:	((CDlgProdDataFoldPrint*)GetParent())->SetFoldMarks();		break;
	case 7:	((CDlgProdDataFoldPrint*)GetParent())->SetShingling();		break;
	}

	return CStatic::OnCommand(wParam, lParam);
}


// CToolBarContainer

IMPLEMENT_DYNAMIC(CFPToolBar, CToolBar)

CFPToolBar::CFPToolBar()
{
}

CFPToolBar::~CFPToolBar()
{
}


BEGIN_MESSAGE_MAP(CFPToolBar, CToolBar)
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CFPToolBar-Meldungshandler

BOOL CFPToolBar::OnEraseBkgnd(CDC* pDC)
{
	CRect rcRect;
	GetClientRect(rcRect);
	pDC->FillSolidRect(rcRect, GUICOLOR_GROUP);

	return TRUE;
}



// CDlgProdDataFoldPrint-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProdDataFoldPrint, CPropertyPage)

//CDlgProdDataFoldPrint::CDlgProdDataFoldPrint(CWnd* pParent /*=NULL*/)
//	: CDialog(CDlgProdDataFoldPrint::IDD, pParent)
CDlgProdDataFoldPrint::CDlgProdDataFoldPrint() : CPropertyPage(CDlgProdDataFoldPrint::IDD)
	, m_strPrintLayout(_T(""))
	, m_strPlateFormat(_T(""))
	, m_strPaperFormat(_T(""))
	, m_strPressMarkset(_T(""))
	, m_nPrintingType(0)
	, m_nTurningType(0)
	, m_nActLayerRefIndex(FALSE)
	, m_strFoldSheetNameHeader(_T(""))
	, m_strNUp(_T(""))
	, m_bFoldMarks(FALSE)
	, m_nNumSheets(0)
	, m_bJDFFold(FALSE)
	, m_bJDFCut(FALSE)
	, m_bJDFPrint(FALSE)
	, m_strOutputProfile(_T(""))
{
	m_smallFont.CreateFont (13, 0, 0, 0, FW_BOLD,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	m_mediumFont.CreateFont(14, 0, 0, 0, FW_BOLD,	  FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Tahoma");
	m_bkBrush.CreateSolidBrush(GUICOLOR_GROUP);
	m_bkBrushOrange.CreateSolidBrush(RGB(252, 220, 150));
	m_bkBrushBlue.CreateSolidBrush(RGB(153, 204, 255));
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);

	m_bNUpMode		    = TRUE;
	m_pPrintSheet		= NULL;
	m_pLayout			= NULL;
	m_pPressDevice		= NULL;
	m_pFoldSheet		= NULL;
	m_nProductPartIndex = 0;

	CString strFullPath = theApp.settings.m_szDataDir + CString(_T("\\SysFoldMarks.def"));
	if ( ! m_foldMarkList.Load(strFullPath))
	{
		CFoldMark* pFoldMark = new CFoldMark;
		pFoldMark->Create(theApp.m_colorDefTable);
		m_foldMarkList.Add(pFoldMark);
	}

	m_bIsDialog	  = FALSE;
	m_bAddNewMode = FALSE;

	m_pButtonOK = NULL;
	m_pButtonCancel = NULL;

	m_pDlgMarkSet = NULL;
	m_pDlgOutputProfile = NULL;
	m_bInitializeActual = FALSE;
}

CDlgProdDataFoldPrint::~CDlgProdDataFoldPrint()
{
	m_smallFont.DeleteObject();
	m_mediumFont.DeleteObject();
	m_bkBrush.DeleteObject();
	m_bkBrushOrange.DeleteObject();
	m_bkBrushBlue.DeleteObject();
	m_hollowBrush.DeleteObject();

	for (int i = 0; i < m_statusMap.GetSize(); i++)
	{
		m_statusMap[i]->SetLength(0);
		delete m_statusMap[i];
	}
	m_statusMap.RemoveAll();

	m_markList.RemoveAll();
	m_markListPreviousPage.RemoveAll();
	m_foldMarkList.RemoveAll();
	m_printMarkList.RemoveAll();

	delete m_pButtonOK;
	delete m_pButtonCancel;

	//delete m_pFoldSheet;	
	//delete m_pLayout;		
	//delete m_pPrintSheet;	
	//delete m_pPressDevice;	//TODO: BUG - crashes when delete is called
	m_pPressDevice	= NULL;		
}

void CDlgProdDataFoldPrint::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PD_PRINTLAYOUT, m_strPrintLayout);
	DDX_Text(pDX, IDC_PD_PLATEFORMAT, m_strPlateFormat);
	DDX_Text(pDX, IDC_PD_PAPERFORMAT, m_strPaperFormat);
	DDX_Text(pDX, IDC_PD_PRESSMARKSET, m_strPressMarkset);
	DDX_CBIndex(pDX, IDC_PD_PRINTINGTYPE_COMBO, m_nPrintingType);
	DDX_CBIndex(pDX, IDC_PD_TURNINGTYPE_COMBO, m_nTurningType);
	DDX_Text(pDX, IDC_PD_FOLDSHEETNAME_HEADER, m_strFoldSheetNameHeader);
	DDX_Control(pDX, IDC_PD_PRESSDEVICE_COMBO, m_pressDeviceCombo);
	DDX_Control(pDX, IDC_PD_PAPER_COMBO, m_paperCombo);
	//	DDX_Text(pDX, IDC_FP_NUP_STATIC, m_strNUp);
	DDX_Text(pDX, IDC_PD_NUM_SHEETS, m_nNumSheets);
	//	DDX_Check(pDX, IDC_FP_JDFFOLD_CHECK, m_bJDFFold);
	//	DDX_Check(pDX, IDC_FP_JDFCUT_CHECK, m_bJDFCut);
	//	DDX_Check(pDX, IDC_FP_JDFPRINT_CHECK, m_bJDFPrint);
	//	DDX_Text(pDX, IDC_PD_OUTPUTPROFILE, m_strOutputProfile);
}


BEGIN_MESSAGE_MAP(CDlgProdDataFoldPrint, CPropertyPage)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_PD_PRESSDEVICE_BUTTON, &CDlgProdDataFoldPrint::OnBnClickedPdPressdeviceButton)
	ON_BN_CLICKED(IDC_PD_PAPER_BUTTON, &CDlgProdDataFoldPrint::OnBnClickedPdPaperButton)
	ON_CBN_SELCHANGE(IDC_PD_PRESSDEVICE_COMBO, &CDlgProdDataFoldPrint::OnCbnSelchangePdPressdeviceCombo)
	ON_CBN_SELCHANGE(IDC_PD_PAPER_COMBO, &CDlgProdDataFoldPrint::OnCbnSelchangePdPaperCombo)
	ON_CBN_SELCHANGE(IDC_PD_PRINTINGTYPE_COMBO, &CDlgProdDataFoldPrint::OnCbnSelchangePrintingType)
	ON_CBN_SELCHANGE(IDC_PD_TURNINGTYPE_COMBO, &CDlgProdDataFoldPrint::OnCbnSelchangeTurningType)
	ON_BN_CLICKED(IDC_PD_SHINGLING, &CDlgProdDataFoldPrint::OnBnClickedPdShingling)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_ACTFOLDSHEET_SPIN, &CDlgProdDataFoldPrint::OnDeltaposPdActFoldSheetSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_NUM_SHEETS_SPIN, &CDlgProdDataFoldPrint::OnDeltaposPdNumSheetsSpin)
	ON_EN_KILLFOCUS(IDC_PD_NUM_SHEETS, &CDlgProdDataFoldPrint::OnKillfocusPdNumSheets)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PP_ACTLAYERREFINDEX_SPIN, &CDlgProdDataFoldPrint::OnDeltaposPpActlayerrefindexSpin)
	ON_BN_CLICKED(IDC_FP_MODIFY_FOLDMARKS, &CDlgProdDataFoldPrint::OnBnClickedFpModifyFoldMarks)
	ON_BN_CLICKED(IDC_FP_SHINGLING_BUTTON, &CDlgProdDataFoldPrint::OnBnClickedFpShinglingButton)
	ON_BN_CLICKED(IDC_FP_BROWSE_SCHEME, &CDlgProdDataFoldPrint::OnBnClickedFpBrowseScheme)
	ON_BN_CLICKED(IDC_FP_DELETE_SCHEME, &CDlgProdDataFoldPrint::OnBnClickedFpDeleteScheme)
	ON_COMMAND(ID_FOLDSHEET_ROTATE,  RotateFoldSheet)
	ON_COMMAND(ID_FOLDSHEET_TURN,  TurnFoldSheet)
	ON_COMMAND(ID_FOLDSHEET_TUMBLE,  TumbleFoldSheet)
	ON_COMMAND(ID_FOLDSHEET_REMOVE,  RemoveFoldSheet)
	ON_COMMAND(ID_FOLDSHEET_NUP,  NUpFoldSheet)
	ON_COMMAND(ID_FOLDSHEET_ONGOING,  OngoingFoldSheet)
	ON_COMMAND(ID_SELECT_FOLDSCHEME, OnSelectFoldSheet)
	ON_UPDATE_COMMAND_UI(ID_FOLDSHEET_NUP,  OnUpdateNUpFoldSheet)
	ON_UPDATE_COMMAND_UI(ID_FOLDSHEET_ONGOING,  OnUpdateOngoingFoldSheet)
	ON_BN_CLICKED(IDC_FP_JDFFOLD_CHECK, &CDlgProdDataFoldPrint::OnBnClickedFpJDFFold)
	ON_BN_CLICKED(IDC_FP_JDFCUT_CHECK, &CDlgProdDataFoldPrint::OnBnClickedFpJDFCut)
	ON_BN_CLICKED(IDC_FP_JDFPRINT_CHECK, &CDlgProdDataFoldPrint::OnBnClickedFpJDFPrint)
	ON_BN_CLICKED(IDC_FP_BROWSE_MARKSET, &CDlgProdDataFoldPrint::OnBnClickedFpBrowseMarkset)
	ON_BN_CLICKED(IDC_FP_BROWSE_OUTPUTPROFILE, &CDlgProdDataFoldPrint::OnBnClickedFpBrowseOutputProfile)
	ON_COMMAND(ID_UPDATE_MARKSET, &CDlgProdDataFoldPrint::OnUpdateMarkset)
	ON_COMMAND(ID_UPDATE_OUTPUTMEDIA, &CDlgProdDataFoldPrint::OnUpdateOutputMedia)
	ON_BN_CLICKED(IDC_PD_PRINTSHEET_DETAILS, &CDlgProdDataFoldPrint::OnBnClickedPdPrintsheetDetails)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
END_MESSAGE_MAP()


void CDlgProdDataFoldPrint::DrawItemFoldSheetBrowser(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState)
{
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem = lpDrawItemStruct->rcItem;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CFoldSheet* pFoldSheet = (CFoldSheet*)lpDrawItemStruct->itemData;
	CRect rcFrame = rcItem;
	rcFrame.DeflateRect(5, 5);
	pDoc->m_Bookblock.DrawFoldSheetList(pDC, rcFrame, pFoldSheet->m_nProductPartIndex, pFoldSheet, TRUE, TRUE, (nState == ODB_NORMAL) ? TRUE : FALSE);
}


void CDlgProdDataFoldPrint::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect rect;
	m_layoutPreviewFrame.GetWindowRect(rect);
	ScreenToClient(rect);
	m_layoutPreviewFrame.OnLButtonDown(nFlags, point - rect.TopLeft());

	CPropertyPage::OnLButtonDown(nFlags, point);
}

void CDlgProdDataFoldPrint::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect rect;
	m_layoutPreviewFrame.GetWindowRect(rect);
	ScreenToClient(rect);
	m_layoutPreviewFrame.OnMouseMove(nFlags, point - rect.TopLeft());

	CPropertyPage::OnMouseMove(nFlags, point);
}

void CDlgProdDataFoldPrint::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType == ODT_MENU)
	{
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		CRect rcItem = lpDrawItemStruct->rcItem;

		if ( ! (lpDrawItemStruct->itemState & ODS_SELECTED) )
			pDC->FillSolidRect(rcItem, RGB(252, 252, 249));//CREME);

		CRect rcIcon = rcItem;	rcIcon.right = rcIcon.left + 45;
		Graphics graphics(lpDrawItemStruct->hDC);
		Rect rc(rcIcon.left, rcIcon.top, rcIcon.Width(), rcIcon.Height());
		LinearGradientBrush br(rc, Color::Color(255,245,210), Color::Color(255,180,100), 0);
		graphics.FillRectangle(&br, rc);

		if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			CPen pen(PS_SOLID, 1, DARKBLUE);
			CBrush brush(RGB(193,210,238));
			CPen* pOldPen = pDC->SelectObject(&pen);
			CBrush* pOldBrush = pDC->SelectObject(&brush);
			pDC->Rectangle(rcItem);
			pDC->SelectObject(pOldPen);
			pDC->SelectObject(pOldBrush);
		}

		CImage img;
		switch (lpDrawItemStruct->itemID)
		{
		case ID_FOLDSHEET_ROTATE:	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_FOLDSHEET_ROTATE));		break;
		case ID_FOLDSHEET_TURN:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_FOLDSHEET_TURN));		break;
		case ID_FOLDSHEET_TUMBLE:	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_FOLDSHEET_TUMBLE));		break;
		case ID_FOLDSHEET_REMOVE:	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_FOLDSHEET_REMOVE));		break;
		}
		img.TransparentBlt(pDC->m_hDC, rcIcon.left + 5, rcIcon.top + 5, img.GetWidth(), img.GetHeight(), RGB(192,192,192));

		pDC->SelectObject(GetFont());
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(DARKBLUE);
		CRect rcText = rcItem;
		rcText.left = rcIcon.right + 5;
		if(IsMenu((HMENU)lpDrawItemStruct->hwndItem))
		{
			CMenu* pMenu = CMenu::FromHandle((HMENU)lpDrawItemStruct->hwndItem);
			if (pMenu)
			{
				CString strText; 
				pMenu->GetMenuStringA(lpDrawItemStruct->itemID, strText, MF_BYCOMMAND);
				pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
			}
		}
	}
	else
		CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CDlgProdDataFoldPrint::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct->CtlType == ODT_MENU)
	{
		int	nMaxLen = 0;
		if(IsMenu((HMENU)lpMeasureItemStruct->itemData))
		{
			CMenu* pMenu = CMenu::FromHandle((HMENU)lpMeasureItemStruct->itemData);
			if (pMenu)
			{
				CString strText;
				pMenu->GetMenuStringA(lpMeasureItemStruct->itemID, strText, MF_BYCOMMAND);
				CClientDC dc(this);
				dc.SelectObject(GetFont());
				nMaxLen = dc.GetTextExtent(strText).cx + 40;
			}
		}
		lpMeasureItemStruct->itemWidth  = nMaxLen;
		lpMeasureItemStruct->itemHeight = 40;
	}
	else
		CDialog::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CDlgProdDataFoldPrint::DrawProductPartInfos(CDC* pDC, CRect rcFrame)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	Graphics graphics(pDC->m_hDC);
	Rect rc(rcFrame.left, rcFrame.top, rcFrame.Width(), rcFrame.Height());
	//LinearGradientBrush br(rc, Color::Color(241,245,254), Color::Color(193,211,251), 0);
	LinearGradientBrush br(rc, Color::Color(160,215,0), Color::Color(120,180,0), 0);
	CPen pen(PS_SOLID, 1, RGB(100,160,0));//LIGHTBLUE);
	//CBrush bkBrush(GUICOLOR_GROUP);
	CBrush hotBrush(GUICOLOR_ITEM_HOT);
	pDC->SelectObject(&pen);
	//pDC->SelectObject(&bkBrush);
	pDC->SelectStockObject(HOLLOW_BRUSH);
	graphics.FillRectangle(&br, rc);
	pDC->RoundRect(rcFrame, CPoint(6,6));

	pDC->SelectObject(&m_smallFont);
	pDC->SetBkMode(TRANSPARENT);

	CRect rcClient;
	GetDlgItem(IDC_PD_PRODUCTPART_FRAME)->GetWindowRect(rcClient);
	ScreenToClient(rcClient);
	int			  nXOff	  = rcClient.left;
	CRect		  rcText  = rcFrame; rcText.bottom = rcText.top + 15; 
	CProductPart* pLastPP = NULL;
	int			  nIndex  = 0;
	for (int i = 0; i < m_foldSheetBrowserButtons.GetSize(); i++) 
	{
		CFoldSheet* pFoldSheet = (CFoldSheet*)m_foldSheetBrowserButtons(i).GetData();

			CProductPart* pProductPart = pFoldSheet->GetProductPart();

			if (pProductPart != pLastPP)
			{
				CRect rcButton = m_foldSheetBrowserButtons.GetButtonClientRect(nIndex); 
				if (nIndex == 1)
				{
					pDC->MoveTo(rcButton.left - nXOff - 4, rcFrame.top);
					pDC->LineTo(rcButton.left - nXOff - 4, rcFrame.bottom);
					pDC->MoveTo(rcButton.left - nXOff - 3, rcFrame.top);
					pDC->LineTo(rcButton.left - nXOff - 3, rcFrame.bottom);
				}

				rcText.left = rcButton.left - nXOff + 2;
				pDC->SetTextColor( (pFoldSheet->m_nProductPartIndex == m_nProductPartIndex) ? RGB(70,100,0) : RGB(100,160,0));
				pDC->DrawText((pProductPart) ? pProductPart->m_strName : L"", rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
				pLastPP = pProductPart;
			}

			rcText.OffsetRect(rcText.Width() + 5, 0);
			nIndex++;
		}
	}

	pen.DeleteObject();
	//bkBrush.DeleteObject();
	hotBrush.DeleteObject();
}

void CDlgProdDataFoldPrint::DrawSignatures(CDC* pDC, CRect rcFrame)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	pDoc->m_Bookblock.DrawFoldSheetList(pDC, rcFrame, m_nProductPartIndex, m_pFoldSheet);
}


// CDlgProdDataFoldPrint-Meldungshandler

BOOL CDlgProdDataFoldPrint::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	CPropertySheet* psheet = (CPropertySheet*) GetParent();  
	if ( ! psheet->IsKindOf(RUNTIME_CLASS(CPropertySheet)))	
	{
		m_bIsDialog = TRUE;
		if (m_pFoldSheet->m_strFoldSheetTypeName.IsEmpty())
			m_bAddNewMode = TRUE;
		FitDialog();
	}
	else
	{
		GetDlgItem(IDC_FP_DELETE_SCHEME)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_FP_SPLIT_SCHEME)->ShowWindow(SW_HIDE);
	}

	m_productPartFrame.SubclassDlgItem	(IDC_PD_PRODUCTPART_FRAME,	this);
	m_previewHeader.SubclassDlgItem		(IDC_PD_PREVIEW_HEADER,		this);
	m_signaturePreview.SubclassDlgItem	(IDC_PAGES_PREVIEW_FRAME,	this);
	m_layoutPreviewFrame.SubclassDlgItem(IDC_PD_LAYOUT_PREVIEWFRAME,this);
	m_foldingGroup.SubclassDlgItem		(IDC_PD_FOLDING_GROUP,		this);
	m_printingGroup.SubclassDlgItem		(IDC_PD_PRINTING_GROUP,		this);
	m_prepressGroup.SubclassDlgItem		(IDC_PD_PREPRESS_GROUP,		this);

	if (m_pLayout)
		m_bFoldMarks = m_pLayout->HasMarks(CMark::FoldMark);

	m_foldSheetBrowserButtons.Create(this, this, &DrawItemFoldSheetBrowser);
	InitFoldSheetBrowser();

	//GetDlgItem(IDC_FP_MODIFY_FOLDMARKS)->EnableWindow(m_bFoldMarks);
	//GetDlgItem(IDC_FP_SHINGLING_BUTTON)->EnableWindow((pDoc) ? pDoc->m_Bookblock.m_bShinglingActive : FALSE);

// toolbar
//	InitToolbars();

//	((CSpinButtonCtrl*)GetDlgItem(IDC_PD_ACTFOLDSHEET_SPIN))->SetRange(0,1);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PD_NUM_SHEETS_SPIN))->SetRange(0,1);
//	((CSpinButtonCtrl*)GetDlgItem(IDC_PP_ACTLAYERREFINDEX_SPIN))->SetRange(0,1);

	if (m_bIsDialog)	// we are not inside wizard (stand alone dialog)
	{														
		if (m_bAddNewMode)
			InitializeNew();
		else
			UpdatePrintSheetControls();

		m_strOutputProfile = m_pPrintSheet->m_strPDFTarget;
	}

	AssembleMarkList();

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	if (pDoc)
		pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, m_nProductPartIndex, m_pFoldSheet);
	m_nNumSheets = sortedFoldSheets.GetSize();

	UpdateData(FALSE);

	return TRUE;  
}

void CDlgProdDataFoldPrint::FitDialog()
{
	if (m_bIsDialog)	// we are not inside wizard (stand alone mode)
	{
		CRect rcFrameDlg;
		GetWindowRect(rcFrameDlg); 
		//if ( ! m_bAddNewMode)	
		//{
		//	SetWindowText(_T("Falzen + Schneiden"));

		//	CRect rcRefFrame, rcGroupFrame, rcProdPartFrame, rcPagesPreviewFrame;
		//	GetDlgItem(IDC_PD_LAYOUT_PREVIEWFRAME)->GetWindowRect(rcRefFrame);

		//	GetDlgItem(IDC_PD_PRODUCTPART_FRAME)->GetWindowRect(rcProdPartFrame);
		//	GetDlgItem(IDC_PAGES_PREVIEW_FRAME)->GetWindowRect(rcPagesPreviewFrame);
		//	GetDlgItem(IDC_PD_FOLDING_GROUP)->GetWindowRect(rcGroupFrame);
		//	rcProdPartFrame.right = rcPagesPreviewFrame.right = rcGroupFrame.right; 
		//	ScreenToClient(rcProdPartFrame);
		//	GetDlgItem(IDC_PD_PRODUCTPART_FRAME)->MoveWindow(rcProdPartFrame);
		//	ScreenToClient(rcPagesPreviewFrame);
		//	GetDlgItem(IDC_PAGES_PREVIEW_FRAME)->MoveWindow(rcPagesPreviewFrame);

		//	rcFrameDlg.right = rcRefFrame.right + 20;
		//}

		rcFrameDlg.bottom += 40;
		MoveWindow(rcFrameDlg);

		CRect rcBtn(rcFrameDlg.left + 20, rcFrameDlg.bottom - 30, rcFrameDlg.left + 100, rcFrameDlg.bottom - 8);
		ScreenToClient(rcBtn);
		m_pButtonOK = new CButton;
		m_pButtonOK->Create(_T("OK"), WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, rcBtn, this, IDOK);
		m_pButtonOK->SetFont(GetFont());
		rcBtn.OffsetRect(90, 0);
		m_pButtonCancel = new CButton;
		m_pButtonCancel->Create(_T("Abbruch"), WS_VISIBLE | WS_CHILD, rcBtn, this, IDCANCEL);
		m_pButtonCancel->SetFont(GetFont());
	}
}

void CDlgProdDataFoldPrint::InitializeNew()
{
	m_pLayout = new CLayout;
	m_pLayout->Create();
	m_pPrintSheet = new CPrintSheet;
	m_pPrintSheet->Create(-1, m_pLayout);
	m_pFoldSheet = new CFoldSheet;
	m_pFoldSheet->Create(0, 0, _T(""), m_nProductPartIndex);
	m_pPressDevice = new CPressDevice;
	m_pPressDevice->Create();

	UpdatePrintSheetControls();

	Invalidate();
	UpdateWindow();

	theApp.OnUpdateView(RUNTIME_CLASS(CProdDataFoldPrintView), NULL, 0, NULL);
}

void CDlgProdDataFoldPrint::InitializeActual()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, m_nProductPartIndex);

	int nLayerIndex = 0;
	CFoldSheet* pFoldSheet = (CFoldSheet*)m_foldSheetBrowserButtons.GetSelectedButtonData();
	//if ( ! pFoldSheet)
	//	pFoldSheet = pDoc->m_Bookblock.FindFoldSheet((sortedFoldSheets.GetSize()) ? sortedFoldSheets[sortedFoldSheets.GetSize()-1]->m_strFoldSheetTypeName : _T(""), m_nProductPartIndex);
	if (pFoldSheet)
		*m_pFoldSheet = *pFoldSheet;
	CPrintSheet* pPrintSheet = (pFoldSheet) ? pFoldSheet->GetPrintSheet(nLayerIndex) : NULL;
	if (pPrintSheet)
		*m_pPrintSheet = *pPrintSheet;
	CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if (pLayout)
		*m_pLayout = *pLayout;
	CPressDevice* pPressDevice	= (pLayout)	? pLayout->m_FrontSide.GetPressDevice() : NULL;
	if (pPressDevice)
		*m_pPressDevice = *pPressDevice;

	if (pLayout)
		m_nActLayerRefIndex = 0;
}

void CDlgProdDataFoldPrint::InitLayerRefIndex()
{
	if (m_nActLayerRefIndex >= m_pPrintSheet->m_FoldSheetLayerRefs.GetCount())
		m_nActLayerRefIndex = m_pPrintSheet->m_FoldSheetLayerRefs.GetCount() - 1;
	else
		if (m_nActLayerRefIndex == -1)
			if (m_pPrintSheet->m_FoldSheetLayerRefs.GetCount())
				m_nActLayerRefIndex = 0;

//	GetDlgItem(IDC_PP_ACTLAYERREFINDEX_SPIN	)->ShowWindow(m_pPrintSheet->m_FoldSheetLayerRefs.GetCount() > 1);
//	GetDlgItem(IDC_FP_NUP_STATIC			)->ShowWindow(m_bNUpMode && (m_pPrintSheet->m_FoldSheetLayerRefs.GetCount() > 1) );
	if (m_pPrintSheet->m_FoldSheetLayerRefs.GetCount() > 1)
		m_strNUp.Format(_T("%d/%d"), m_nActLayerRefIndex + 1, m_pPrintSheet->m_FoldSheetLayerRefs.GetCount());

	UpdateData(FALSE);
}

void CDlgProdDataFoldPrint::InitFoldSheetBrowser()
{
	CFoldSheet* pFoldSheet = (CFoldSheet*)m_foldSheetBrowserButtons.GetSelectedButtonData();

	m_foldSheetBrowserButtons.RemoveAll();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CRect rcButtonFrame;
	m_signaturePreview.GetWindowRect(rcButtonFrame);
	ScreenToClient(rcButtonFrame);
	rcButtonFrame.right = rcButtonFrame.left + 150;
	for (int nProductPartIndex = 0; nProductPartIndex < pDoc->m_productPartList.GetSize(); nProductPartIndex++)
	{
		CArray<CFoldSheet*, CFoldSheet*> foldSheetGroups;
		CBookblock::GetFoldSheetGroups(foldSheetGroups, nProductPartIndex);
		for (int i = 0; i < foldSheetGroups.GetSize(); i++)
		{
			m_foldSheetBrowserButtons.AddButton(L"", ID_SELECT_FOLDSCHEME, rcButtonFrame, (ULONG_PTR)foldSheetGroups[i], FALSE);

			rcButtonFrame.OffsetRect(rcButtonFrame.Width() + 5, 0);
		}

		int nUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(nProductPartIndex);
		if (nUnImposedPages > 0)
		{
			CFoldSheet* pDummyFoldSheet = new CFoldSheet;
			pDummyFoldSheet->m_nProductPartIndex = nProductPartIndex;
			m_foldSheetBrowserButtons.AddButton(L"", ID_SELECT_FOLDSCHEME, rcButtonFrame, (ULONG_PTR)pDummyFoldSheet, FALSE);

			rcButtonFrame.OffsetRect(rcButtonFrame.Width() + 5, 0);
		}
	}

	m_foldSheetBrowserButtons.SelectButton((ULONG_PTR)pFoldSheet);
}

void CDlgProdDataFoldPrint::InitToolbars()
{
	m_toolBarContainer.SubclassDlgItem(IDC_PD_TOOLBAR_FRAME, this);
	m_toolBar.Create(&m_toolBarContainer);

	int nNumBitmaps = 1;
	int nNumButtons = 1;
	CSize sizeBitmap(50, 32);
	m_toolBar.GetToolBarCtrl().SetBitmapSize(sizeBitmap);
	m_toolBar.GetToolBarCtrl().AddBitmap(nNumBitmaps, IDB_PD_FOLDSHEET_TOOLBAR);
	m_toolBar.ModifyStyle(0, TBSTYLE_FLAT);
	m_toolBar.SetButtons(NULL, nNumButtons);

	int nIndex = 0;
//	m_toolBar.SetButtonInfo(nIndex,		6,		TBBS_CHECKBOX,	 6);		m_toolBar.SetButtonText(nIndex++,	_T("Falzmarken"));
	m_toolBar.SetButtonInfo(nIndex,		7,		TBBS_CHECKBOX,	 7);		m_toolBar.SetButtonText(nIndex++,	_T("Verdr�ngung"));

	int nCommandID = (m_bNUpMode) ? 4 : 5;
	m_toolBar.GetToolBarCtrl().SetState(nCommandID, m_toolBar.GetToolBarCtrl().GetState(nCommandID) | TBSTATE_CHECKED);
	if (m_bFoldMarks)
		m_toolBar.GetToolBarCtrl().SetState(6, m_toolBar.GetToolBarCtrl().GetState(6) | TBSTATE_CHECKED);
	if (CImpManDoc::GetDoc())
		if (CImpManDoc::GetDoc()->m_Bookblock.m_bShinglingActive)
			m_toolBar.GetToolBarCtrl().SetState(7, m_toolBar.GetToolBarCtrl().GetState(7) | TBSTATE_CHECKED);

// set up toolbar button sizes 
	CRect rectToolBar;
	m_toolBar.GetItemRect(0, &rectToolBar);
	m_toolBar.SetSizes(rectToolBar.Size(), CSize(50,32));
	m_toolBar.SetBarStyle(m_toolBar.GetBarStyle() | CBRS_FLYBY | CBRS_SIZE_FIXED);

	GetDlgItem(IDC_PD_TOOLBAR_FRAME)->RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);


// nUp toolbar
	m_nUpToolBarGroup.SubclassDlgItem(IDC_PD_NUP_TOOLBAR_FRAME, this);
	m_nUpToolBarGroup.SetWindowText(_T("Nutzen"));
	m_nUpToolBar.Create(IDC_PD_NUP_TOOLBAR_FRAME, ODBL_VERTICAL, this, this, NULL, IDB_PD_NUP_TOOLBAR, IDB_PD_NUP_TOOLBAR_COLD, CSize(50, 32), FALSE);
	m_nUpToolBar.AddButton(_T("Drehen"),		ID_FOLDSHEET_ROTATE,	35, 0);
	m_nUpToolBar.AddButton(_T("Umschlagen"),	ID_FOLDSHEET_TURN,		35, 1);
	m_nUpToolBar.AddButton(_T("Umst�lpen"),		ID_FOLDSHEET_TUMBLE,	35, 2);
	m_nUpToolBar.AddButton(_T("L�schen"),		ID_FOLDSHEET_REMOVE,	35, 3);
	m_nUpToolBar.AddButton(_T("Nutzendruck"),	ID_FOLDSHEET_NUP,		35, 4);
	m_nUpToolBar.AddButton(_T("Fortlaufend"),	ID_FOLDSHEET_ONGOING,	35, 5);
}


////// printsheet layout part /////////////////////////////////////////////////////

void CDlgProdDataFoldPrint::UpdatePrintSheetControls()
{
	if ( ! m_pFoldSheet || ! m_pPrintSheet || ! m_pLayout)
		return;

	m_strFoldSheetNameHeader = m_pFoldSheet->m_strFoldSheetTypeName;
	m_nActLayerRefIndex = 0;

	InitLayerRefIndex();

	if (m_pPressDevice)
		if (m_pPressDevice->m_strName.IsEmpty())
		{
			POSITION pos  = theApp.m_PressDeviceList.FindIndex(0);
			if (pos)
				m_pPressDevice = &theApp.m_PressDeviceList.GetAt(pos);
		}

	m_pPrintSheet->LinkLayout(m_pLayout);

	m_pressDeviceCombo.ResetContent();
	POSITION pos = theApp.m_PressDeviceList.GetHeadPosition();
	while (pos) 
	{
		CPressDevice& rPressDevice = theApp.m_PressDeviceList.GetNext(pos);
		m_pressDeviceCombo.AddString(rPressDevice.m_strName);
	}
	int nIndex = m_pressDeviceCombo.FindStringExact(-1, m_pPressDevice->m_strName);
	if (nIndex != CB_ERR)
		m_pressDeviceCombo.SetCurSel(nIndex);

	CDlgSheet dlgSheet;
	dlgSheet.LoadSheets();
	m_paperCombo.ResetContent();
	pos = dlgSheet.m_sheetList.GetHeadPosition();
	while (pos) 
	{
		CSheet& rSheet = dlgSheet.m_sheetList.GetNext(pos);
		m_paperCombo.AddString(rSheet.m_strSheetName);
	}
	nIndex = m_paperCombo.FindStringExact(-1, m_pLayout->m_FrontSide.m_strFormatName);
	if (nIndex != CB_ERR)
		m_paperCombo.SetCurSel(nIndex);

	m_strPaperFormat.Format("%.1f x %.1f", m_pLayout->m_FrontSide.m_fPaperWidth, m_pLayout->m_FrontSide.m_fPaperHeight);
	m_strPlateFormat.Format("%.1f x %.1f", (m_pPressDevice) ? m_pPressDevice->m_fPlateWidth : 0.0f, m_pPressDevice ? m_pPressDevice->m_fPlateHeight : 0.0f);
	m_strPrintLayout	= m_pLayout->m_strLayoutName;
//	m_strPressMarkset	= "";
	if (m_pLayout)
	{
		switch(m_pLayout->m_nProductType)
		{
		default:			  m_nPrintingType = 0;	break;
		case WORK_AND_TURN	: m_nPrintingType = 1;	break;
		case WORK_AND_TUMBLE: m_nPrintingType = 2;	break;
		}
		m_nTurningType = (m_pLayout->KindOfProduction() == WORK_AND_TURN) ? 0 : 1;
	}

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
		pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, m_nProductPartIndex, m_pFoldSheet);
		m_nNumSheets = sortedFoldSheets.GetSize();
		if ( (m_nProductPartIndex >= 0) && (m_nProductPartIndex < pDoc->m_productPartList.GetSize()) )
		{
			BOOL bLastProductPart = (m_nProductPartIndex == pDoc->m_productPartList.GetSize() - 1) ? TRUE : FALSE;
			CPropertySheet* psheet = (CPropertySheet*) GetParent();  
			if (pDoc->m_PageTemplateList.GetNumUnimposedPages(m_nProductPartIndex) == 0)	// all pages imposed
				//psheet->SetWizardButtons((bLastProductPart) ? PSWIZB_BACK | PSWIZB_FINISH : PSWIZB_BACK | PSWIZB_NEXT);
				psheet->SetWizardButtons(PSWIZB_BACK | PSWIZB_FINISH);
			else
				psheet->SetWizardButtons(PSWIZB_BACK);
				//if (pDoc->m_PageTemplateList.GetNumUnimposedPages(m_nProductPartIndex) >= pDoc->m_productPartList[m_nProductPartIndex].m_nNumPages)		// all pages unimposed 
				//	psheet->SetWizardButtons(PSWIZB_BACK);
				//else
				//	psheet->SetWizardButtons(PSWIZB_BACK | PSWIZB_NEXT);	
		}
	}

	UpdateData(FALSE);
}

void CDlgProdDataFoldPrint::DrawFoldSheetPreview(CDC* pDC)
{
	pDC->FillSolidRect(m_foldsheetPreviewFrame, RGB(144, 153, 174));

//	int nLayerRefIndex = m_pPrintSheet->GetNUpsFoldSheetLayerRefIndex(m_pFoldSheet->m_strFoldSheetTypeName, 0, m_nActNUP + 1);
	if ( (m_nActLayerRefIndex < 0) || (m_nActLayerRefIndex >= m_pPrintSheet->m_FoldSheetLayerRefs.GetCount()) )
		return;

	m_pFoldSheet->Draw(pDC, m_foldsheetPreviewFrame, m_nActLayerRefIndex, m_pPrintSheet, m_pLayout);
}

HBRUSH CDlgProdDataFoldPrint::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CPropertyPage::OnCtlColor(pDC, pWnd, nCtlColor);
	
    if(nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_FP_FOLDSCHEME_HEADER:
		case IDC_FP_PRINTLAYOUT_HEADER:
		case IDC_FP_NUP_STATIC:
				pDC->SelectObject(&m_smallFont);
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				hbr = (HBRUSH)m_bkBrush.GetSafeHandle();
				break;
		case IDC_PD_PAGES_STATIC:
		case IDC_PD_PROCESS_STATIC:
				pDC->SelectObject(&m_smallFont);
				pDC->SetBkMode(TRANSPARENT);
				hbr = (HBRUSH)m_hollowBrush.GetSafeHandle();
				break;
		case IDC_PD_PRINTLAYOUT:
		case IDC_PD_FOLDSHEETNAME_HEADER:
				pDC->SelectObject(&m_smallFont);
				pDC->SetBkMode(TRANSPARENT);
				hbr = (HBRUSH)m_bkBrushOrange.GetSafeHandle();
				break;

		case IDC_PD_PRESSMARKSET:
		case IDC_PD_OUTPUTPROFILE:
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(BLACK);
				hbr = (HBRUSH)m_bkBrush.GetSafeHandle();
				break;

		case IDC_PD_PRODPART_CAPTION:
		case IDC_PD_SHEET_CAPTION:
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				hbr = (HBRUSH)m_hollowBrush.GetSafeHandle();
				break;

		default:pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				hbr = (HBRUSH)m_bkBrush.GetSafeHandle();
				break;
		}
    }

	return hbr;
}

void CDlgProdDataFoldPrint::OnBnClickedFpBrowseScheme()
{
	CDlgFoldSchemeBrowser dlg(this);

	if (dlg.DoModal() == IDCANCEL)
		if (m_nActLayerRefIndex >= 0)
		{
			//RestoreStatus();
			//UpdatePrintSheetControls();
			//m_layoutPreviewFrame.Invalidate();
			//m_layoutPreviewFrame.UpdateWindow();
		}
}

void CDlgProdDataFoldPrint::OnBnClickedFpDeleteScheme()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! m_pFoldSheet)
		return;
	CString strMsg;
	strMsg.Format(_T("Wollen Sie das Schema '%s' wirklich insgesamt l�schen"), m_pFoldSheet->m_strFoldSheetTypeName);
	if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
		return;

	int		nProductPartIndex	 = m_pFoldSheet->m_nProductPartIndex;
	CString strFoldSheetTypeName = m_pFoldSheet->m_strFoldSheetTypeName;
	BOOL	bDeleted			 = FALSE;
	for (int nLayerIndex = 0; nLayerIndex < m_pFoldSheet->m_LayerList.GetCount(); nLayerIndex++)
	{
		CFoldSheetLayer* pLayer		 = (m_pFoldSheet) ? m_pFoldSheet->GetLayer(nLayerIndex) : NULL;
		CPrintSheet*	 pPrintSheet = (pLayer) ? pLayer->GetFirstPrintSheet() : NULL;
		CLayout*		 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		if ( ! pLayer || ! pPrintSheet || ! pLayout)
			continue;
		int		 nLayerRefIndex	= pPrintSheet->FoldSheetLayerToLayerRef(pLayer);
		int		 nLayoutIndex	= pPrintSheet->m_nLayoutIndex;
		POSITION pos			= pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
		while (pos)
		{
			CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
			if (rFoldSheet.m_nProductPartIndex == nProductPartIndex)
				if (rFoldSheet.m_strFoldSheetTypeName == strFoldSheetTypeName)
					if (rFoldSheet.GetLayout(nLayerIndex) == pLayout)
					{
						CPrintSheet* pPrintSheet = rFoldSheet.GetLayer(nLayerIndex)->GetFirstPrintSheet();
						while (pPrintSheet)
						{
							if (pPrintSheet->RemoveFoldSheetLayer(nLayerRefIndex, FALSE, TRUE, FALSE, TRUE))
								bDeleted = TRUE;
							pPrintSheet = rFoldSheet.GetLayer(nLayerIndex)->GetFirstPrintSheet();
						}
					}
		}
	}

	if (bDeleted)	
	{
		pDoc->m_Bookblock.Reorganize(NULL, NULL);//, nProductPartIndex);
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.FindPrintSheetLocations(nProductPartIndex);
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();

		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

		pDoc->m_MarkSourceList.RemoveUnusedSources();
		pDoc->SetModifiedFlag();										

		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
		theApp.OnUpdateView(RUNTIME_CLASS(CBookBlockView),			NULL, 0, NULL);	
		theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),	NULL, 0, NULL);	
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),			NULL, 0, NULL);	
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));

		Invalidate();
		UpdateWindow();

		CDialog::OnOK();
	}
}

void CDlgProdDataFoldPrint::ChangeScheme(const CString strPath, const CString& strScheme)
{
	CFileException fileException;
	CFile		   file;
	CString strFullPath = strPath + _T("\\") + strScheme + _T(".isc");
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return; 
	}
	CArchive archive(&file, CArchive::load);

	CFoldSheet foldSheet;
	DestructElements(&foldSheet, 1);
	SerializeElements(archive, &foldSheet, 1);
	archive.Close();
	file.Close();

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nNewPages = 0;
	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
		nNewPages = foldSheet.m_nPagesLeft;
	else
		nNewPages = foldSheet.m_nPagesLeft + foldSheet.m_nPagesRight;
	if (nNewPages == 0)
		return;
	int nOldPages = 0;
	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
		nOldPages = m_pFoldSheet->m_nPagesLeft;
	else
		nOldPages = m_pFoldSheet->m_nPagesLeft + m_pFoldSheet->m_nPagesRight;

	int nNumPagesToImpose = pDoc->m_PageTemplateList.GetNumUnimposedPages(m_nProductPartIndex) + m_nNumSheets * nOldPages;
	int	nSheets	= nNumPagesToImpose / nNewPages;
	if (nSheets == 0)	
	{
		AfxMessageBox("Nicht gen�gend unausgeschossene Seiten f�r diesen Falzbogen!", MB_OK);
		return;
	}

	foldSheet.m_strFoldSheetTypeName = strScheme;
	foldSheet.m_nProductPartIndex	 = m_nProductPartIndex;

	ArrangeNewPrintSheetLayout(&foldSheet);

	UpdatePrintSheetControls();
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OnBnClickedPdPressdeviceButton()
{
	CDlgPressDevice dlg(FALSE, FRONTSIDE, m_pPressDevice, m_pLayout);  // don't modify direct inside dialog

	dlg.m_pressDevice = *m_pPressDevice;
	if (dlg.DoModal() == IDCANCEL)
		return;

	CPressDevice oldPressDevice;
	oldPressDevice	= *m_pPressDevice;
	*m_pPressDevice = dlg.m_pressDevice;
	float fGripper  = CDlgPressDevice::CheckGripper(&oldPressDevice, m_pPressDevice, &m_pLayout->m_FrontSide);

	m_pLayout->m_FrontSide.PositionLayoutSide(fGripper, m_pPressDevice);
	m_pLayout->m_BackSide.PositionLayoutSide (fGripper, m_pPressDevice);

	ArrangeNewPrintSheetLayout();

	UpdatePrintSheetControls();
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OnBnClickedPdPaperButton()
{
	CDlgSheet dlg(FALSE);  // don't modify direct inside dialog

	if (dlg.DoModal() == IDCANCEL)
		return;

	if (m_pPressDevice)
		if ( (dlg.m_fWidth > m_pPressDevice->m_fPlateWidth) || (dlg.m_fHeight > m_pPressDevice->m_fPlateHeight))
			if (AfxMessageBox(IDS_SHEET_GREATER_PLATE , MB_YESNO|MB_ICONSTOP) == IDNO)
				return;

	m_pLayout->m_FrontSide.m_fPaperWidth		= dlg.m_fWidth;
	m_pLayout->m_FrontSide.m_fPaperHeight		= dlg.m_fHeight;
	m_pLayout->m_FrontSide.m_nPaperOrientation	= (unsigned char)dlg.m_nPageOrientation;
	m_pLayout->m_FrontSide.m_strFormatName		= dlg.m_strName;
	m_pLayout->m_FrontSide.m_bAdjustFanout		= dlg.m_bFanoutCheck;
	m_pLayout->m_FrontSide.m_FanoutList			= dlg.m_FanoutList;
	m_pLayout->m_FrontSide.PositionLayoutSide(FLT_MAX, m_pPressDevice);

	m_pLayout->m_BackSide.m_fPaperWidth			= dlg.m_fWidth;
	m_pLayout->m_BackSide.m_fPaperHeight		= dlg.m_fHeight;
	m_pLayout->m_BackSide.m_nPaperOrientation	= (unsigned char)dlg.m_nPageOrientation;
	m_pLayout->m_BackSide.m_strFormatName		= dlg.m_strName;
	m_pLayout->m_BackSide.m_bAdjustFanout		= dlg.m_bFanoutCheck;
	m_pLayout->m_BackSide.m_FanoutList			= dlg.m_FanoutList;
	m_pLayout->m_BackSide.PositionLayoutSide(FLT_MAX, m_pPressDevice);

	ArrangeNewPrintSheetLayout();

	UpdatePrintSheetControls();
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::SwitchLayer()
{
	UpdateData(TRUE);
	InvalidateRect(m_foldsheetPreviewFrame);
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OnSelectFoldSheet()
{
	CFoldSheet* pFoldSheet = (CFoldSheet*)m_foldSheetBrowserButtons.GetSelectedButtonData();
	if ( ! pFoldSheet)
		return;

	m_nProductPartIndex = pFoldSheet->m_nProductPartIndex;

	m_bInitializeActual = TRUE;
	if (pFoldSheet)
		if (pFoldSheet->m_strFoldSheetTypeName.IsEmpty())
		{
			m_bInitializeActual = FALSE;
			OnSetActive();
			return;
		}

	m_pFoldSheet = pFoldSheet;
	int nLayerIndex = 0;
	m_pPrintSheet = m_pFoldSheet->GetPrintSheet(nLayerIndex);
	CLayout* pLayout = (m_pPrintSheet) ? m_pPrintSheet->GetLayout() : NULL;
	if (pLayout)
		m_pLayout = pLayout;
	CPressDevice* pPressDevice	= (pLayout)	? pLayout->m_FrontSide.GetPressDevice() : NULL;
	if (pPressDevice)
		m_pPressDevice = pPressDevice;

	UpdatePrintSheetControls();
	Invalidate();
	UpdateWindow();
}

CProductPart* CDlgProdDataFoldPrint::GetActProductPart()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if ( (m_nProductPartIndex < 0 ) || (m_nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return NULL;
	return &pDoc->m_productPartList[m_nProductPartIndex];
}

CLayout* CDlgProdDataFoldPrint::GetActLayout()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	return pDoc->m_PrintSheetList.m_Layouts.FindLayout(m_pLayout->m_strLayoutName);
}

CPrintSheet* CDlgProdDataFoldPrint::GetActPrintSheet()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	CLayout* pLayout = GetActLayout();
	return (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
}

CFoldSheet* CDlgProdDataFoldPrint::GetActFoldSheet()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	return (pPrintSheet) ? pPrintSheet->GetFoldSheet(m_nActLayerRefIndex) : NULL;
}

void CDlgProdDataFoldPrint::RotateFoldSheet()
{
//	int nLayerRefIndex = m_pPrintSheet->GetNUpsFoldSheetLayerRefIndex(m_pFoldSheet->m_strFoldSheetTypeName, 0, m_nActNUP + 1);
	if ( (m_nActLayerRefIndex < 0) || (m_nActLayerRefIndex >= m_pPrintSheet->m_FoldSheetLayerRefs.GetCount()) )
		return;
	if ( ! GetActLayout())
		return;
	GetActLayout()->RotateFoldSheet90(m_nActLayerRefIndex);
	InvalidateRect(m_foldsheetPreviewFrame);
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::TurnFoldSheet()
{
//	int nLayerRefIndex = m_pPrintSheet->GetNUpsFoldSheetLayerRefIndex(m_pFoldSheet->m_strFoldSheetTypeName, 0, m_nActNUP + 1);
	if ( (m_nActLayerRefIndex < 0) || (m_nActLayerRefIndex >= m_pPrintSheet->m_FoldSheetLayerRefs.GetCount()) )
		return;
	if ( ! GetActLayout())
		return;
	GetActLayout()->TurnFoldSheet(m_nActLayerRefIndex);
	m_pLayout->m_nProductType = GetActLayout()->m_nProductType;
	InvalidateRect(m_foldsheetPreviewFrame);
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
	UpdatePrintSheetControls();
}

void CDlgProdDataFoldPrint::TumbleFoldSheet()
{
//	int nLayerRefIndex = m_pPrintSheet->GetNUpsFoldSheetLayerRefIndex(m_pFoldSheet->m_strFoldSheetTypeName, 0, m_nActNUP + 1);
	if ( (m_nActLayerRefIndex < 0) || (m_nActLayerRefIndex >= m_pPrintSheet->m_FoldSheetLayerRefs.GetCount()) )
		return;
	if ( ! GetActLayout())
		return;
	GetActLayout()->TurnFoldSheet(m_nActLayerRefIndex);
	GetActLayout()->RotateFoldSheet90(m_nActLayerRefIndex);
	GetActLayout()->RotateFoldSheet90(m_nActLayerRefIndex);
	m_pLayout->m_nProductType = GetActLayout()->m_nProductType;
	InvalidateRect(m_foldsheetPreviewFrame);
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
	UpdatePrintSheetControls();
}

void CDlgProdDataFoldPrint::RemoveFoldSheet()
{
//	int nLayerRefIndex = m_pPrintSheet->GetNUpsFoldSheetLayerRefIndex(m_pFoldSheet->m_strFoldSheetTypeName, 0, m_nActNUP + 1);
	if ( (m_nActLayerRefIndex < 0) || (m_nActLayerRefIndex >= m_pPrintSheet->m_FoldSheetLayerRefs.GetCount()) )
		return;
	if ( ! GetActLayout())
		return;
	GetActLayout()->RemoveFoldSheetLayer(m_nActLayerRefIndex, m_pPressDevice);
	m_pPrintSheet->RemoveFoldSheetLayer(m_nActLayerRefIndex, FALSE, FALSE, FALSE, FALSE, m_pPressDevice);
	InitLayerRefIndex();
	InvalidateRect(m_foldsheetPreviewFrame);
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::NUpFoldSheet()
{
	m_bNUpMode = TRUE;
	InitLayerRefIndex();
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OngoingFoldSheet()
{
	m_bNUpMode = FALSE;
	InitLayerRefIndex();
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OnUpdateNUpFoldSheet(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pPrintSheet->m_FoldSheetLayerRefs.GetCount() > 1));
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_bNUpMode);
}

void CDlgProdDataFoldPrint::OnUpdateOngoingFoldSheet(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, (m_pPrintSheet->m_FoldSheetLayerRefs.GetCount() > 1));
	COwnerDrawnButtonList::SetCheck(pCmdUI, ! m_bNUpMode);
}

void CDlgProdDataFoldPrint::SetFoldMarks()
{
	m_bFoldMarks = ! m_bFoldMarks;
	GetDlgItem(IDC_FP_MODIFY_FOLDMARKS)->EnableWindow(m_bFoldMarks);

	AssembleMarkList();
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::SetShingling()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	pDoc->m_Bookblock.m_bShinglingActive = ! pDoc->m_Bookblock.m_bShinglingActive;
	//GetDlgItem(IDC_FP_SHINGLING_BUTTON)->EnableWindow(pDoc->m_Bookblock.m_bShinglingActive);

	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OnCbnSelchangePdPressdeviceCombo()
{
	int nSel = m_pressDeviceCombo.GetCurSel();
	if (nSel == CB_ERR)
		return;

	POSITION pos = theApp.m_PressDeviceList.FindIndex(nSel);
	if ( ! pos)
		return;

	if (m_nActLayerRefIndex >= 0)
	{
		int nSide = BOTHSIDES;

		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
			pDoc->SetPrintSheetPressDevice(nSel, nSide, m_pLayout);

		CPressDevice* pOldPressDevice = (nSide == BACKSIDE) ? m_pLayout->m_BackSide.GetPressDevice() : m_pLayout->m_FrontSide.GetPressDevice();
		pDoc->SetPrintSheetPressDevice(nSel, nSide);
		CPressDevice* pNewPressDevice = (nSide == BACKSIDE) ? m_pLayout->m_BackSide.GetPressDevice() : m_pLayout->m_FrontSide.GetPressDevice();
		if ( ! pOldPressDevice || ! pNewPressDevice)
			return;
														
		float fGripper = CDlgPressDevice::CheckGripper(pOldPressDevice, pNewPressDevice, (nSide == BACKSIDE) ? &m_pLayout->m_BackSide : &m_pLayout->m_FrontSide);

		m_pLayout->m_FrontSide.PositionLayoutSide(fGripper);
		m_pLayout->m_BackSide.PositionLayoutSide (fGripper);
	}
	else
	{
		CPressDevice& rPressDevice = theApp.m_PressDeviceList.GetAt(pos);

		CPressDevice oldPressDevice;
		oldPressDevice  = *m_pPressDevice;
		*m_pPressDevice = rPressDevice;
		float fGripper  = CDlgPressDevice::CheckGripper(&oldPressDevice, m_pPressDevice, &m_pLayout->m_FrontSide);

		m_pLayout->m_FrontSide.PositionLayoutSide(fGripper, m_pPressDevice);
		m_pLayout->m_BackSide.PositionLayoutSide (fGripper, m_pPressDevice);
	}

	//ArrangeNewPrintSheetLayout();

	UpdatePrintSheetControls();
	InvalidateRect(m_foldsheetPreviewFrame);
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OnCbnSelchangePdPaperCombo()
{
	int nSel = m_paperCombo.GetCurSel();
	if (nSel == CB_ERR)
		return;

	CDlgSheet dlg;
	dlg.LoadSheets();
	POSITION pos = dlg.m_sheetList.FindIndex(nSel);
	if ( ! pos)
		return;

	CSheet& rSheet = dlg.m_sheetList.GetAt(pos);

	if (m_pPressDevice)
		if ( (rSheet.m_fWidth > m_pPressDevice->m_fPlateWidth) || (rSheet.m_fHeight > m_pPressDevice->m_fPlateHeight))
			if (AfxMessageBox(IDS_SHEET_GREATER_PLATE , MB_YESNO|MB_ICONSTOP) == IDNO)
				return;

	m_pLayout->m_FrontSide.m_fPaperWidth		= rSheet.m_fWidth;
	m_pLayout->m_FrontSide.m_fPaperHeight		= rSheet.m_fHeight;
	m_pLayout->m_FrontSide.m_nPaperOrientation	= (unsigned char)( (rSheet.m_fWidth > rSheet.m_fHeight) ? LANDSCAPE : PORTRAIT);
	m_pLayout->m_FrontSide.m_strFormatName		= rSheet.m_strSheetName;
	m_pLayout->m_FrontSide.m_bAdjustFanout		= (rSheet.m_FanoutList.GetSize()) ? TRUE : FALSE;
	m_pLayout->m_FrontSide.m_FanoutList			= rSheet.m_FanoutList;
	m_pLayout->m_FrontSide.PositionLayoutSide(FLT_MAX, m_pPressDevice);

	m_pLayout->m_BackSide.m_fPaperWidth			= rSheet.m_fWidth;
	m_pLayout->m_BackSide.m_fPaperHeight		= rSheet.m_fHeight;
	m_pLayout->m_BackSide.m_nPaperOrientation	= m_pLayout->m_FrontSide.m_nPaperOrientation;
	m_pLayout->m_BackSide.m_strFormatName		= rSheet.m_strSheetName;
	m_pLayout->m_BackSide.m_bAdjustFanout		= m_pLayout->m_FrontSide.m_bAdjustFanout;
	m_pLayout->m_BackSide.m_FanoutList			= rSheet.m_FanoutList;
	m_pLayout->m_BackSide.PositionLayoutSide(FLT_MAX, m_pPressDevice);

	ArrangeNewPrintSheetLayout();

	UpdatePrintSheetControls();
	InvalidateRect(m_foldsheetPreviewFrame);
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OnCbnSelchangePrintingType()
{
	UpdateData(TRUE);

	CLayout* pActLayout = GetActLayout();
	if (!pActLayout)
		return;
	switch (m_nPrintingType)
	{
	case 0: if ( (pActLayout->m_nProductType == WORK_AND_TURN) || (pActLayout->m_nProductType == WORK_AND_TUMBLE) )
			{
				pActLayout->m_bNOCheckForProductType = TRUE;
				pActLayout->CheckForProductType();
				m_pLayout->m_nProductType = pActLayout->m_nProductType;
				theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView));
				UpdatePrintSheetControls();
			}
			break;

	case 1: 
	case 2:
			pActLayout->m_bNOCheckForProductType = FALSE;
			pActLayout->CheckForProductType();
			m_pLayout->m_nProductType = pActLayout->m_nProductType;
			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView));
			UpdatePrintSheetControls();
			break;
	}
}

void CDlgProdDataFoldPrint::OnCbnSelchangeTurningType()
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	CLayout* pActLayout = GetActLayout();
	if (!pActLayout)
		return;
	if ( (pActLayout->KindOfProduction() == WORK_AND_TURN) && (m_nTurningType == 0) )
		return;
	if ( (pActLayout->KindOfProduction() == WORK_AND_TUMBLE) && (m_nTurningType == 1) )
		return;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	if (m_nTurningType == 0)
	{
		pActLayout->m_BackSide.TurnAll();
		pActLayout->m_BackSide.TumbleAll();
	}
	else
	{
		pActLayout->m_BackSide.TumbleAll();
		pActLayout->m_BackSide.TurnAll();
	}

	pActLayout->CheckForProductType();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgProdDataFoldPrint::ArrangeNewPrintSheetLayout(CFoldSheet* pNewFoldSheet)
{
	//if (m_bAddNewMode)
	//	return;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	BOOL bDoRestore = (m_nActLayerRefIndex >= 0) ? TRUE : FALSE;

	if (pNewFoldSheet)
		*m_pFoldSheet = *pNewFoldSheet;
	//if (bDoRestore)
	{
	//	RestoreStatus(FALSE);
		CLayout* pLayout = new CLayout;
		pLayout->Create();
		*pLayout = *m_pLayout; 

		CPrintSheet* pPrintSheet = new CPrintSheet;
		pPrintSheet->Create(-1, m_pLayout);
		*pPrintSheet = *m_pPrintSheet; 

		CFoldSheet* pFoldSheet = new CFoldSheet;
		pFoldSheet->Create(0, 0, _T(""), m_nProductPartIndex);
		*pFoldSheet = *m_pFoldSheet; 

		CPressDevice* pPressDevice = new CPressDevice;
		pPressDevice->Create();
		*pPressDevice = *m_pPressDevice; 

		while (m_pPrintSheet->m_FoldSheetLayerRefs.GetSize())
			m_pPrintSheet->RemoveFoldSheetLayer(0, FALSE, TRUE, FALSE, TRUE);

		pDoc->m_Bookblock.Reorganize(NULL, NULL);//, nProductPartIndex);
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.FindPrintSheetLocations(m_nProductPartIndex);
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();

		m_pLayout		= pLayout;
		m_pPrintSheet	= pPrintSheet;
		m_pFoldSheet	= pFoldSheet;
		m_pPressDevice	= pPressDevice;

		m_pLayout->RemoveFoldSheetLayerAll();
		m_pPrintSheet->RemoveFoldSheetLayerAll();
	}

	int nNumMin = INT_MAX;
	for (int i = 0; i < m_pPrintSheet->m_FoldSheetLayerRefs.GetCount(); i++)
	{
		CFoldSheet* pFoldSheet = m_pPrintSheet->GetFoldSheet(i);
		if (pFoldSheet)
			nNumMin = min(nNumMin, pFoldSheet->m_nFoldSheetNumber);
	}
	if (nNumMin == INT_MAX)
		m_pFoldSheet->m_nFoldSheetNumber = 1;
	else
		m_pFoldSheet->m_nFoldSheetNumber = nNumMin;

	CFoldSheetLayer* pLayer = m_pFoldSheet->GetLayer(0);
	if ( ! pLayer)
		return;
	if ( ! pLayer->m_HeadPositionArray.GetSize())
		return;

	if ( (m_nProductPartIndex < 0) || (m_nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;

	int		nPageOrientation	= pDoc->m_productPartList[m_nProductPartIndex].m_nPageOrientation;	
	float	fPageWidth			= pDoc->m_productPartList[m_nProductPartIndex].m_fPageWidth;
	float	fPageHeight			= pDoc->m_productPartList[m_nProductPartIndex].m_fPageHeight;
	CString strFormatName		= pDoc->m_productPartList[m_nProductPartIndex].m_strFormatName;
	float	fHeadTrim			= pDoc->m_productPartList[m_nProductPartIndex].m_fHeadTrim;
	float	fFootTrim			= pDoc->m_productPartList[m_nProductPartIndex].m_fFootTrim;
	float	fSideTrim			= pDoc->m_productPartList[m_nProductPartIndex].m_fSideTrim;
	float	fSpineTrim			= pDoc->m_productPartList[m_nProductPartIndex].m_fSpineTrim;
	float	fMillingEdge		= pDoc->m_productPartList[m_nProductPartIndex].m_fMillingEdge;
	fSpineTrim /= 2.0f;	// user defines space between left and right page for binding -> trim for each page is the half
	fSpineTrim = max(fSpineTrim, fMillingEdge);	// if we have both -> take bigger one
	float	fPageBoxWidth		= fPageWidth  + fSpineTrim + fSideTrim;
	float	fPageBoxHeight		= fPageHeight + fHeadTrim  + fFootTrim;

	if ( (pLayer->m_HeadPositionArray[0] == LEFT) || (pLayer->m_HeadPositionArray[0] == RIGHT) )
	{ 
		float fTemp = fPageBoxWidth; fPageBoxWidth = fPageBoxHeight; fPageBoxHeight = fTemp; 
			  fTemp = fPageWidth;	 fPageWidth	   = fPageHeight;	 fPageHeight	= fTemp; 
	}

	CSize pageMatrix		  = m_pFoldSheet->GetMatrix();
	float fFoldSheetWidth	  = fPageBoxWidth  * pageMatrix.cx;
	float fFoldSheetHeight	  = fPageBoxHeight * pageMatrix.cy; 

	if ( (m_pLayout->m_FrontSide.m_fPaperWidth < 1.0f) || (m_pLayout->m_FrontSide.m_fPaperHeight < 1.0f) )	// no paper size selected
	{
OnceAgain:
		float fPaperWidth  = fFoldSheetWidth;
		float fPaperHeight = fFoldSheetHeight + ( (m_pPressDevice->m_nPressType == CPressDevice::SheetPress) ? m_pPressDevice->m_fGripper : 0);	// TODO: check this (see also: FindFoldSheetMatrix())
		m_pLayout->m_FrontSide.FindSuitablePaper(FALSE, fPaperWidth, fPaperHeight, m_pPressDevice);
		m_pLayout->m_FrontSide.PositionLayoutSide(m_pPressDevice->m_fGripper, m_pPressDevice);
		m_pLayout->m_BackSide.FindSuitablePaper(FALSE, fPaperWidth, fPaperHeight, m_pPressDevice);
		m_pLayout->m_BackSide.PositionLayoutSide(m_pPressDevice->m_fGripper, m_pPressDevice);
		m_strPaperFormat.Format("%.1f x %.1f", m_pLayout->m_FrontSide.m_fPaperWidth, m_pLayout->m_FrontSide.m_fPaperHeight);
	}

	BOOL  bRotate = FALSE;
	CSize foldSheetMatrix;
	foldSheetMatrix = m_pLayout->FindFoldSheetMatrix(fFoldSheetWidth, fFoldSheetHeight, m_pPressDevice, bRotate);
	if ( (foldSheetMatrix.cx == 0) || (foldSheetMatrix.cy == 0) )
	{
		CString strMsg;
		strMsg.Format(_T("Das Schema %s passt nicht auf den gew�hlten Papierbogen!\nPassendes Format suchen?"), m_pFoldSheet->m_strFoldSheetTypeName);
		if (AfxMessageBox(strMsg, MB_YESNO) == IDYES)
			goto OnceAgain;
		else
			return;
	}

	if (bRotate) { float fTemp = fFoldSheetWidth; fFoldSheetWidth = fFoldSheetHeight; fFoldSheetHeight = fTemp; }

	int   nLayerRefIndex = 0;
	float fFoldSheetPosY = (m_pPressDevice->m_nPressType == CPressDevice::SheetPress) ? m_pPressDevice->m_fPlateEdgeToPrintEdge : m_pPressDevice->m_fCenterOfSection - (fFoldSheetHeight*foldSheetMatrix.cy)/2.0f;
	for (int ny = 0; ny < foldSheetMatrix.cy; ny++)
	{
		float fFoldSheetPosX = m_pPressDevice->m_fPlateWidth/2.0f - (fFoldSheetWidth*foldSheetMatrix.cx)/2.0f;
		for (int nx = 0; nx < foldSheetMatrix.cx; nx++)
		{
			m_pLayout->AddFoldSheetLayer(pLayer, nLayerRefIndex, fFoldSheetPosX, fFoldSheetPosY, fPageBoxWidth, fPageBoxHeight, fHeadTrim, fSideTrim, fFootTrim, fSpineTrim, 
									   fPageWidth, fPageHeight, nPageOrientation, strFormatName, bRotate);
			fFoldSheetPosX += fFoldSheetWidth;

			CFoldSheetLayerRef layerRef(-1, 0);
			m_pPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);
			nLayerRefIndex++;
		}
		fFoldSheetPosY += fFoldSheetHeight;
	}

	InitLayerRefIndex();

	DoImposition(bDoRestore);
}

void CDlgProdDataFoldPrint::OnBnClickedPdShingling()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}

void CDlgProdDataFoldPrint::OnBnClickedFpModifyFoldMarks()
{
	CDlgModifyMark dlg;
	dlg.m_pPrintSheet = m_pPrintSheet;
	dlg.m_pMark		  = (m_foldMarkList.GetSize()) ? m_foldMarkList[0] : NULL;
	dlg.m_strTitle	  = _T("Falzmarken");
	if (dlg.DoModal() == IDOK)
	{
		CString strFullPath = theApp.settings.m_szDataDir + CString(_T("\\SysFoldMarks.def"));
		m_foldMarkList.Save(strFullPath);
		AssembleMarkList();
	}

	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OnUpdateMarkset()	// message from DlgMarkSet
{
	if (m_pDlgMarkSet)
	{
		m_printMarkList.RemoveAll();
		for (int i = 0; i < m_pDlgMarkSet->m_markList.GetSize(); i++)
		{
			CMark* pMark = CMark::_new(m_pDlgMarkSet->m_markList[i]->m_nType);
			CMark::_Copy(pMark, m_pDlgMarkSet->m_markList[i]);	// add copy
			m_printMarkList.Add(pMark);
		}
	}
	AssembleMarkList();
	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::AssembleMarkList()
{
	m_markList.RemoveAll();	

	for (int i = 0; i < m_markListPreviousPage.GetSize(); i++)
	{
		CMark* pMark = CMark::_new(m_markListPreviousPage[i]->m_nType);
		CMark::_Copy(pMark, m_markListPreviousPage[i]);	// add copy
		m_markList.Add(pMark);
	}

	if (m_bFoldMarks && m_foldMarkList.GetSize() && ! m_printMarkList.HasMarks(CMark::FoldMark))
	{
		CMark* pMark = CMark::_new(m_foldMarkList[0]->m_nType);
		CMark::_Copy(pMark, m_foldMarkList[0]);	// add copy
		m_markList.Add(pMark);
	}
	if (m_pLayout)
		if ( ! m_bFoldMarks && m_pLayout->HasMarks(CMark::FoldMark))
			m_pLayout->RemoveMarks(CMark::FoldMark);	

	for (int i = 0; i < m_printMarkList.GetSize(); i++)
	{
		CMark* pMark = CMark::_new(m_printMarkList[i]->m_nType);
		CMark::_Copy(pMark, m_printMarkList[i]);	// add copy
		m_markList.Add(pMark);
	}
}

void CDlgProdDataFoldPrint::OnBnClickedFpShinglingButton()
{
	CDlgShinglingAssistent dlg(this, TRUE, m_pFoldSheet->m_nProductPartIndex);		// bRunModal = TRUE
	dlg.DoModal();
}

void CDlgProdDataFoldPrint::OnDeltaposPdActFoldSheetSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, m_nProductPartIndex, m_pFoldSheet);

	int nLayerIndex = 0;
	if (pNMUpDown->iDelta > 0)
	{
		for (int i = 0; i < sortedFoldSheets.GetSize(); i++)
		{
			CFoldSheet* pFoldSheet = sortedFoldSheets[i];
			if (pFoldSheet == m_pFoldSheet)
			{
				if (i < sortedFoldSheets.GetSize() - 1)
				{
					m_pFoldSheet = sortedFoldSheets[i + 1];
					m_pPrintSheet = m_pFoldSheet->GetPrintSheet(nLayerIndex);
					break;
				}
			}
		}
	}
	else
	{
		for (int i = sortedFoldSheets.GetSize() - 1; i >= 0; i--)
		{
			CFoldSheet* pFoldSheet = sortedFoldSheets[i];
			if (pFoldSheet == m_pFoldSheet)
				if (i > 0)
				{
					m_pFoldSheet = sortedFoldSheets[i - 1];
					m_pPrintSheet = m_pFoldSheet->GetPrintSheet(nLayerIndex);
					break;
				}
		}
	}

	Invalidate();
	UpdateWindow();
}

void CDlgProdDataFoldPrint::OnDeltaposPdNumSheetsSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	int nNum = m_nNumSheets + pNMUpDown->iDelta;
	if ((nNum <= 0) || (nNum > 999))
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nPages = 0;
	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
		nPages = m_pFoldSheet->m_nPagesLeft;
	else
		nPages = m_pFoldSheet->m_nPagesLeft + m_pFoldSheet->m_nPagesRight;

	int nNumUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(m_nProductPartIndex);
	if (pNMUpDown->iDelta > 0)
		if (nPages > nNumUnImposedPages)	// too many sheets
			return;

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, m_nProductPartIndex, m_pFoldSheet);

	if (m_nNumSheets < nNum)
		pDoc->m_Bookblock.AddFoldSheets(GetActFoldSheet(), nNum - m_nNumSheets, m_nProductPartIndex);
	else
		pDoc->m_Bookblock.RemoveFoldSheets(GetActFoldSheet(), m_nNumSheets - nNum, m_nProductPartIndex);

	pDoc->m_Bookblock.Reorganize(GetActPrintSheet(), GetActLayout());//, m_nProductPartIndex);

	if (pDoc->m_Bookblock.GetFoldSheetIndex(GetActFoldSheet()) == -1)	// foldsheet obviously be removed
		InitializeActual();

	Invalidate();
	UpdateWindow();

	m_nNumSheets = nNum;

	UpdatePrintSheetControls();

	UpdateData(FALSE);
}

void CDlgProdDataFoldPrint::OnKillfocusPdNumSheets() 
{
	if (m_nNumSheets == 0)	// we must add new foldscheme
		return;

	int nOldNum = m_nNumSheets;

	UpdateData(TRUE);

	int nNum = m_nNumSheets;
	if ((nNum <= 0) || (nNum > 999))
	{
		m_nNumSheets = nOldNum;
		UpdateData(FALSE);
		return;
	}
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nPages = 0;
	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
		nPages = m_pFoldSheet->m_nPagesLeft;
	else
		nPages = m_pFoldSheet->m_nPagesLeft + m_pFoldSheet->m_nPagesRight;

	int nNumUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(m_nProductPartIndex);
	if (nNum > nOldNum)
	{
		int nSheetsToAdd = nNumUnImposedPages / nPages;
		if (nSheetsToAdd == 0)	// not enough unimposed pages
		{
			m_nNumSheets = nOldNum;
			UpdateData(FALSE);
			return;
		}
		if (nNum > nOldNum + nSheetsToAdd)
			nNum = nSheetsToAdd + nOldNum;
		m_nNumSheets = nNum;
		UpdateData(FALSE);
	}

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, m_nProductPartIndex, m_pFoldSheet);

	if (nOldNum < nNum)
		pDoc->m_Bookblock.AddFoldSheets(GetActFoldSheet(), nNum - nOldNum, m_nProductPartIndex);
	else
		pDoc->m_Bookblock.RemoveFoldSheets(GetActFoldSheet(), nOldNum - nNum, m_nProductPartIndex);

	pDoc->m_Bookblock.Reorganize(GetActPrintSheet(), m_pLayout);//, m_nProductPartIndex);

	if (pDoc->m_Bookblock.GetFoldSheetIndex(GetActFoldSheet()) == -1)	// foldsheet obviously be removed
		InitializeActual();

	Invalidate();
	UpdateWindow();

	UpdatePrintSheetControls();
}

void CDlgProdDataFoldPrint::OnDeltaposPpActlayerrefindexSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	m_nActLayerRefIndex += pNMUpDown->iDelta;
	if (m_nActLayerRefIndex < 0)
		m_nActLayerRefIndex = m_pPrintSheet->m_FoldSheetLayerRefs.GetCount() - 1;
	else
		if (m_nActLayerRefIndex >= m_pPrintSheet->m_FoldSheetLayerRefs.GetCount())
			m_nActLayerRefIndex = 0;

	if (m_pPrintSheet->m_FoldSheetLayerRefs.GetCount() > 1)
		m_strNUp.Format(_T("%d/%d"), m_nActLayerRefIndex + 1, m_pPrintSheet->m_FoldSheetLayerRefs.GetCount());

	UpdateData(FALSE);

	m_layoutPreviewFrame.Invalidate();
	m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::DoImposition(BOOL bDoRestore)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_pPrintSheet->LinkLayout(m_pLayout);

	//if (bDoRestore)
	//	RestoreStatus(FALSE);

	int nPages = 0;
	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
		nPages = m_pFoldSheet->m_nPagesLeft;
	else
		nPages = m_pFoldSheet->m_nPagesLeft + m_pFoldSheet->m_nPagesRight;
	int nNumUnImposedPages = pDoc->m_PageTemplateList.GetNumUnimposedPages(m_nProductPartIndex);
	int	nSheets	= nNumUnImposedPages / nPages;

	pDoc->m_Bookblock.AddFoldSheets(m_pFoldSheet, nSheets, m_nProductPartIndex);
	pDoc->m_Bookblock.Reorganize(m_pPrintSheet, m_pLayout);//, m_nProductPartIndex);

	//m_pLayout		= pDoc->m_PrintSheetList.m_Layouts.FindLayout(m_pLayout->m_strLayoutName);
	//m_pPrintSheet	= (m_pLayout)		? m_pLayout->GetFirstPrintSheet() : NULL;
	//m_pFoldSheet	= (m_pPrintSheet)	? m_pPrintSheet->GetFoldSheet(m_nActLayerRefIndex) : NULL;
	//m_pPressDevice	= (m_pLayout)		? m_pLayout->m_FrontSide.GetPressDevice() : NULL;

	CArray <CFoldSheet*, CFoldSheet*> sortedFoldSheets;
	pDoc->m_Bookblock.InitSortedFoldSheets(sortedFoldSheets, m_nProductPartIndex, m_pFoldSheet);
	m_nNumSheets = sortedFoldSheets.GetSize();

	InitFoldSheetBrowser();

	UpdatePrintSheetControls();

	Invalidate();
	UpdateWindow();

	theApp.OnUpdateView(RUNTIME_CLASS(CProdDataFoldPrintView), NULL, 0, NULL);
}

void CDlgProdDataFoldPrint::OnBnClickedFpJDFFold()
{
	AfxMessageBox(_T("Sie verf�gen nicht �ber den notwendigen Lizenzschl�ssel\nzum Exportieren von JDF-Falzdaten!\n\nWeitere Informationen erhalten Sie unter +49 521 4599127."), MB_OK);
	m_bJDFFold = FALSE;
	UpdateData(FALSE);
}

void CDlgProdDataFoldPrint::OnBnClickedFpJDFCut()
{
	AfxMessageBox(_T("Sie verf�gen nicht �ber den notwendigen Lizenzschl�ssel\nzum Exportieren von JDF-Schneiddaten!\n\nWeitere Informationen erhalten Sie unter +49 521 4599127."), MB_OK);
	m_bJDFCut = FALSE;
	UpdateData(FALSE);
}

void CDlgProdDataFoldPrint::OnBnClickedFpJDFPrint()
{
	AfxMessageBox(_T("Sie verf�gen nicht �ber den notwendigen Lizenzschl�ssel\nzum Exportieren von JDF-Druckdaten!\n\nWeitere Informationen erhalten Sie unter +49 521 4599127."), MB_OK);
	m_bJDFPrint = FALSE;
	UpdateData(FALSE);
}

void CDlgProdDataFoldPrint::OnBnClickedFpBrowseMarkset()
{
	UpdateData(TRUE);

	m_pDlgMarkSet = new CDlgMarkSet(this, TRUE);
	m_pDlgMarkSet->m_bBrowseOnly	   = TRUE;
	m_pDlgMarkSet->m_pActPrintSheet	   = m_pPrintSheet;
	m_pDlgMarkSet->m_strCurrentMarkset = m_strPressMarkset;

	int nIDRet = m_pDlgMarkSet->DoModal();

	CString strPressMarkset = m_pDlgMarkSet->m_strCurrentMarkset;
	delete m_pDlgMarkSet;
	m_pDlgMarkSet = NULL;
	if (nIDRet == IDCANCEL)
	{
		m_printMarkList.RemoveAll();
		m_strPressMarkset = _T("");
	}
	else
	{
		m_strPressMarkset = strPressMarkset;
		if (m_pLayout)
			if (m_pLayout->HasMarks(CMark::FoldMark))
				m_bFoldMarks = FALSE;
	}
	AssembleMarkList();		// remove marks

	UpdateData(FALSE);

	Invalidate();
	UpdateWindow();
	//m_layoutPreviewFrame.Invalidate();
	//m_layoutPreviewFrame.UpdateWindow();
}

void CDlgProdDataFoldPrint::OnBnClickedFpBrowseOutputProfile()
{
	m_pDlgOutputProfile = new CDlgPDFOutputMediaSettings(this, TRUE);
	m_pDlgOutputProfile->m_strPDFTargetName		= m_strOutputProfile;
	m_pDlgOutputProfile->m_pWndMsgTarget		= this;
	m_pDlgOutputProfile->m_bNoSheetSelection	= TRUE;
	m_pDlgOutputProfile->m_nWhatSheets			= 1;	// all of type
	m_pDlgOutputProfile->m_strWhatLayout		= (m_pLayout) ? m_pLayout->m_strLayoutName : _T("");

	if (m_pDlgOutputProfile->DoModal() != IDCANCEL)
	{
		m_strOutputProfile = m_pDlgOutputProfile->m_strPDFTargetName;
		if( GetActPrintSheet())
			GetActPrintSheet()->m_strPDFTarget = m_strOutputProfile;
		UpdateData(FALSE);
	}

	delete m_pDlgOutputProfile;
	m_pDlgOutputProfile = NULL;
}

void CDlgProdDataFoldPrint::OnUpdateOutputMedia()	// message from DlgPDFOutputMediaSettings
{
	Invalidate();
	UpdateWindow();
}

void CDlgProdDataFoldPrint::SaveStatus()
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CMemFile* pMemFile = new CMemFile();

	pMemFile->SeekToBegin();
	CArchive archive(pMemFile, CArchive::store);
	pDoc->Serialize(archive);
	archive.Close();

	m_statusMap.Add(pMemFile);
}

void CDlgProdDataFoldPrint::RestoreStatus(BOOL bRemoveFromStack, int nMapIndex)
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! m_statusMap.GetSize())
		return;
	if (nMapIndex < 0)
		nMapIndex = m_statusMap.GetSize() - 1;

	if (nMapIndex >= m_statusMap.GetSize())
		return;

	CMemFile* pMemFile = m_statusMap.ElementAt(nMapIndex);
	if (pMemFile->GetLength() <= 0)
		return;

	pMemFile->SeekToBegin();
	CArchive archive(pMemFile, CArchive::load);

	pDoc->DeleteContents();
	pDoc->Serialize(archive);

	archive.Close();

	if (bRemoveFromStack)
	{
		pMemFile->SetLength(0);
		delete pMemFile;
		m_statusMap.RemoveAt(nMapIndex);
	}

	theApp.OnUpdateView(RUNTIME_CLASS(CProdDataFoldPrintView), NULL, 0, NULL);
}

BOOL CDlgProdDataFoldPrint::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (GetFocus() == GetDlgItem(IDC_PD_NUM_SHEETS))
			{
				OnKillfocusPdNumSheets();
				return FALSE;
			}
		}
	if (m_bIsDialog)
		return CDialog::PreTranslateMessage(pMsg);
	else
		return CPropertyPage::PreTranslateMessage(pMsg);
}

void CDlgProdDataFoldPrint::OnOK()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CLayout* pLayout = pDoc->m_PrintSheetList.m_Layouts.FindLayout(m_pLayout->m_strLayoutName);
		if (pLayout)
		{
			for (int i = 0; i < m_markList.GetSize(); i++)
			{
				CDlgMarkSet::PositionAndCreateMarkAuto(m_markList[i], &m_markList, _T(""), pLayout->GetFirstPrintSheet());
			}
			pDoc->m_MarkTemplateList.FindPrintSheetLocations();
			pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
		}
	}

	CDialog::OnOK();
}

void CDlgProdDataFoldPrint::OnCancel()
{
	CDialog::OnCancel();
}

BOOL CDlgProdDataFoldPrint::OnSetActive()
{
	if (m_bInitializeActual)
		InitializeActual();
	else
	{
//		SaveStatus();
		InitializeNew();
	}
	
	InitFoldSheetBrowser();

//	GetDlgItem(IDC_FP_MODIFY_FOLDMARKS)->EnableWindow(m_bFoldMarks);

	CPropertySheet* psheet = (CPropertySheet*) GetParent();  
	if (psheet->IsKindOf(RUNTIME_CLASS(CPropertySheet)))	
	{
		CDlgProdDataBindTrim* pDlg = (CDlgProdDataBindTrim*)psheet->GetPage(1);
		for (int i = 0; i < pDlg->m_markList.GetSize(); i++)
		{
			CMark* pMark = CMark::_new(pDlg->m_markList[i]->m_nType);
			CMark::_Copy(pMark, pDlg->m_markList[i]);	// add copy
			m_markListPreviousPage.Add(pMark);
		}
	}

	return CPropertyPage::OnSetActive();
}

LRESULT CDlgProdDataFoldPrint::OnWizardNext()
{
	if (m_nActLayerRefIndex == -1)
		return -1;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CLayout* pLayout = pDoc->m_PrintSheetList.m_Layouts.FindLayout(m_pLayout->m_strLayoutName);
		if (pLayout)
		{
			for (int i = 0; i < m_markList.GetSize(); i++)
			{
				CDlgMarkSet::PositionAndCreateMarkAuto(m_markList[i], &m_markList, _T(""), pLayout->GetFirstPrintSheet());
			}
			pDoc->m_MarkTemplateList.FindPrintSheetLocations();
			pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
		}

		//if (pDoc->m_PageTemplateList.GetNumUnimposedPages(m_nProductPartIndex) == 0)
		//{
		//	SaveStatus();
		//	m_nProductPartIndex++;
		//	CProdDataWizard* psheet = (CProdDataWizard*) GetParent();  
		//	if (psheet)
		//		psheet->m_bindTrimPage.m_nProductPartIndex = m_nProductPartIndex;
		//	return IDD_PRODDATA_BIND_TRIM;
		//}
	}

	InitializeNew();

	return CPropertyPage::OnWizardNext();
}

LRESULT CDlgProdDataFoldPrint::OnWizardBack()
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	//int nNumPagesToImpose = pDoc->m_PageTemplateList.GetNumUnimposedPages(m_nProductPartIndex);

	////RestoreStatus();

	//if (nNumPagesToImpose >= pDoc->m_productPartList[m_nProductPartIndex].m_nNumPages)		// all pages unimposed 
		return CPropertyPage::OnWizardBack();	// goto bindtrim data page

	//InitializeActual();

	//UpdatePrintSheetControls();
	//Invalidate();
	//UpdateWindow();
	//theApp.OnUpdateView(RUNTIME_CLASS(CProdDataFoldPrintView), NULL, 0, NULL);

	return -1;
}

BOOL CDlgProdDataFoldPrint::OnWizardFinish()
{
	OnWizardNext();

	return CPropertyPage::OnWizardFinish();
}

BOOL CDlgProdDataFoldPrint::OnKillActive() 
{
	return CPropertyPage::OnKillActive();
}

void CDlgProdDataFoldPrint::OnBnClickedPdPrintsheetDetails()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CCreateContext context;
	context.m_pCurrentDoc = pDoc;
	context.m_pNewViewClass = RUNTIME_CLASS(CProdDataPrintView);

	CMemFile memFile;
	theApp.UndoSave(&memFile);
	if ( ! CModalFrame::Run(*RUNTIME_CLASS(CProductionDataFrame), FALSE, IDR_PRODUCTIONDATAFRAME, WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, this, &context))
		theApp.UndoRestore(&memFile);
	else
		pDoc->SetModifiedFlag();
}
