// PrintSheetComponentsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "NewPrintSheetFrame.h"
#include "PrintSheetComponentsView.h"
#include "GraphicComponent.h"


#define IDC_PRINTSHEETCOMPONENTSVIEW_HEADER 11114


// CPrintSheetComponentsView

IMPLEMENT_DYNCREATE(CPrintSheetComponentsView, CScrollView)

CPrintSheetComponentsView::CPrintSheetComponentsView()
{
	m_docSize = CSize(0, 0);
	m_nToolBarHeight = 0;
	m_nHeaderItemsTotalWidth = 0;
	m_nHeaderHeight = 0;
	m_headerFont.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CPrintSheetComponentsView::~CPrintSheetComponentsView()
{
	m_headerFont.DeleteObject();
}


BEGIN_MESSAGE_MAP(CPrintSheetComponentsView, CScrollView)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_NOTIFY(HDN_TRACK,	IDC_PRINTSHEETCOMPONENTSVIEW_HEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK, IDC_PRINTSHEETCOMPONENTSVIEW_HEADER, OnHeaderEndTrack)
END_MESSAGE_MAP()


// CPrintSheetComponentsView-Zeichnung

BOOL CPrintSheetComponentsView::OnEraseBkgnd(CDC* pDC)
{
//	m_toolBar.ClipButtons(pDC);

	CRect rect;
    GetClientRect(rect);
	rect.top = m_nToolBarHeight + m_nHeaderHeight;
    CBrush brush(RGB(240,241,244));//237,239,243));
    pDC->FillRect(&rect, &brush);

	CRect rcHeader = rect;
	rcHeader.top = 0; rcHeader.bottom = m_nToolBarHeight;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);

	rcHeader.top = rcHeader.bottom - 3;
	pDC->FillSolidRect(rcHeader, RGB(215,220,225));//RGB(190,202,210));

    brush.DeleteObject();

	return TRUE;

//	return CScrollView::OnEraseBkgnd(pDC);
}

void CPrintSheetComponentsView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

}

void CPrintSheetComponentsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	CPrintSheet* pPrintSheet = GetActPrintSheet();
	if (pPrintSheet)
	{
		if (m_HeaderCtrl.GetSafeHwnd())
			m_HeaderCtrl.DestroyWindow();

		if ( ! m_HeaderCtrl.GetSafeHwnd())
		{
			m_headerColumnWidths.RemoveAll();
			//m_HeaderCtrl.DestroyWindow();
			m_nHeaderHeight = 0;

			m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,100,20), this, IDC_PRINTSHEETCOMPONENTSVIEW_HEADER);	
			m_HeaderCtrl.SetFont(&m_headerFont, FALSE);

			HD_ITEM hdi;
			hdi.mask		= HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
			hdi.fmt			= HDF_LEFT | HDF_STRING;

			m_headerColumnWidths.RemoveAll();

			int		nIndex = 0;
			CString string, string1;

			if (pPrintSheet->HasFoldSheetsOnly())
				string.LoadString(IDS_FOLDSHEET_LABEL);
			else
				if (pPrintSheet->HasFlatProductsOnly())
					string.LoadString(IDS_PRODUCT);
				else
				{
					string.LoadString(IDS_FOLDSHEET_LABEL); string1.LoadString(IDS_PRODUCT); string += _T(" / ") + string1;
				}
			hdi.pszText		= string.GetBuffer(0);
			hdi.cxy			= GetHeaderWidth(nIndex, 200);
			hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
			m_headerColumnWidths.Add(hdi.cxy);
			m_HeaderCtrl.InsertItem(nIndex++, &hdi);

			if (pPrintSheet->HasFoldSheets())
			{
				string.LoadString(IDS_PAGES_HEADER);
				hdi.pszText		= string.GetBuffer(0);
				hdi.cxy			= GetHeaderWidth(nIndex, 150);
				hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
				m_headerColumnWidths.Add(hdi.cxy);
				m_HeaderCtrl.InsertItem(nIndex++, &hdi);
			}

			string.LoadString(IDS_NUP);
			hdi.pszText		= string.GetBuffer(0);
			hdi.cxy			= GetHeaderWidth(nIndex, 80);
			hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
			m_headerColumnWidths.Add(hdi.cxy);
			m_HeaderCtrl.InsertItem(nIndex++, &hdi);

			string.LoadString(IDS_PAPERLIST_FORMAT);
			hdi.pszText		= string.GetBuffer(0);
			hdi.cxy			= GetHeaderWidth(nIndex, 100);
			hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
			m_headerColumnWidths.Add(hdi.cxy);
			m_HeaderCtrl.InsertItem(nIndex++, &hdi);

			//if (pPrintSheet->HasFlatProducts())
			{
				string.LoadString(IDS_QUANTITY); 
				hdi.pszText		= string.GetBuffer(0);
				hdi.cxy			= GetHeaderWidth(nIndex, 100);
				hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
				m_headerColumnWidths.Add(hdi.cxy);
				m_HeaderCtrl.InsertItem(nIndex++, &hdi);

				string.LoadString(IDS_COVERAGE);
				hdi.pszText		= string.GetBuffer(0);
				hdi.cxy			= GetHeaderWidth(nIndex, 100);
				hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
				m_headerColumnWidths.Add(hdi.cxy);
				m_HeaderCtrl.InsertItem(nIndex++, &hdi);

				string.LoadString(IDS_OVERPRODUCTION);
				hdi.pszText		= string.GetBuffer(0);
				hdi.cxy			= GetHeaderWidth(nIndex, 150);
				hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
				m_headerColumnWidths.Add(hdi.cxy);
				m_HeaderCtrl.InsertItem(nIndex++, &hdi);
			}

			m_nHeaderItemsTotalWidth = 0;
			for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
				m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];
		}
	}

	OnSize(0, 0, 0);

	ModifyStyle(0, WS_CLIPCHILDREN);	// Clip header control 

	Invalidate();

	m_docSize = CalcExtents();
	SetScrollSizes(MM_TEXT, m_docSize);
}

void CPrintSheetComponentsView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);

	if (m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdl;
		WINDOWPOS wp;
		hdl.prc = &clientRect;
		hdl.pwpos = &wp;
		m_HeaderCtrl.Layout(&hdl);	
		m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y + m_nToolBarHeight, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_nHeaderHeight = wp.cy;
	} 
}

void CPrintSheetComponentsView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 20)
		hdn->pitem->cxy = 20;
}

void CPrintSheetComponentsView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	SetHeaderWidth(hdn->iItem, hdn->pitem->cxy);

	if (m_headerColumnWidths.GetSize() > hdn->iItem)
		m_headerColumnWidths[hdn->iItem] = hdn->pitem->cxy; 

	m_nHeaderItemsTotalWidth = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
		m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];

	m_docSize = CalcExtents();

	Invalidate();
	SetScrollSizes(MM_TEXT, m_docSize);
}

void CPrintSheetComponentsView::SetHeaderWidth(int nIndex, int nValue)
{
	if (nIndex >= 20)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\PrintSheetComponentsHeader");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			columnWidthArray[nIndex] = nValue;			
		else
		{
			ZeroMemory(columnWidthArray, sizeof(columnWidthArray));
			columnWidthArray[nIndex] = nValue;			
		}

		dwSize = sizeof(columnWidthArray);
		RegSetValueEx(hKey, _T("Columns"), 0, REG_BINARY, (LPBYTE)&columnWidthArray, dwSize);

		RegCloseKey(hKey);
	}
}

int CPrintSheetComponentsView::GetHeaderWidth(int nIndex, int nDefault)
{
	if (nIndex >= 20)
		return nDefault;

	int nWidth = nDefault;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\PrintSheetComponentsHeader");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			nWidth = columnWidthArray[nIndex];			
		else
			nWidth = nDefault;

		RegCloseKey(hKey);
	}

	if ( (nWidth <= 10) || (nWidth > 1000) )
		nWidth = nDefault;

	return nWidth;
}

// CPrintSheetComponentsView-Diagnose

#ifdef _DEBUG
void CPrintSheetComponentsView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetComponentsView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG



CSize CPrintSheetComponentsView::CalcExtents()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CRect rcBox = Draw(&dc, NULL);

	dc.DeleteDC();

	return CSize(rcBox.right, rcBox.bottom);
}

void CPrintSheetComponentsView::OnDraw(CDC* pDC)
{
	CRect rcClientRect, rcClip;
	GetClientRect(rcClientRect); rcClip = rcClientRect;
	rcClip.top = m_nToolBarHeight + m_nHeaderHeight;
	rcClip.OffsetRect(-pDC->GetViewportOrg());
	pDC->IntersectClipRect(rcClip);

	Draw(pDC, NULL);
}

void CPrintSheetComponentsView::ResetDisplayColums()
{
	m_displayColumns.RemoveAll();
}

void CPrintSheetComponentsView::AddDisplayColumn(int nColID, int nWidth)
{
	struct displayColumn dc = {nColID, nWidth};
	m_displayColumns.Add(dc);
}

CRect CPrintSheetComponentsView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(0,0,1,1);

	CPrintSheet* pPrintSheet = GetActPrintSheet();
	if ( ! pPrintSheet)
		return CRect(0,0,1,1);

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	CRect rcFrame;
	GetClientRect(rcFrame); 
	
	rcFrame.top += m_nToolBarHeight + m_nHeaderHeight;	
	rcFrame.right = rcFrame.left + 500;
	rcFrame.top += 5;
	rcFrame.left += 5;

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nColIndex = 0;
	ResetDisplayColums();
	AddDisplayColumn(ColComponent,			m_headerColumnWidths[nColIndex++]);
	if (pPrintSheet->HasFoldSheets())
		AddDisplayColumn(ColPages,			m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColNUp,				m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColFormat,				m_headerColumnWidths[nColIndex++]);
	//if (pPrintSheet->HasFlatProducts() && (m_headerColumnWidths.GetSize() > nColIndex) )
	{
		AddDisplayColumn(ColQuantity,		m_headerColumnWidths[nColIndex++]);
		AddDisplayColumn(ColSheetCoverage,	m_headerColumnWidths[nColIndex++]);
		AddDisplayColumn(ColOverProduction,	m_headerColumnWidths[nColIndex++]);
	}

	int nRowHeight = 20;
	CRect rcRow = rcFrame; rcRow.bottom = rcRow.top + nRowHeight;

	///// foldsheets
	for (int nFoldSheetTypeIndex = 0; nFoldSheetTypeIndex < pPrintSheet->m_FoldSheetTypeList.GetSize(); nFoldSheetTypeIndex++)
	{
		/////// columns
		CRect rcColumn = rcRow; 
		int	  nRight   = 0;
		
		CFoldSheet* pFoldSheet  = pPrintSheet->m_FoldSheetTypeList[nFoldSheetTypeIndex].m_pFoldSheet;
		int			nLayerIndex	= pPrintSheet->m_FoldSheetTypeList[nFoldSheetTypeIndex].m_nFoldSheetLayerIndex;
		if ( ! pFoldSheet)
			continue;
		int	  nNUp	  		= pPrintSheet->m_FoldSheetTypeList[nFoldSheetTypeIndex].m_nNumUp;
		float fWidth  		= pFoldSheet->GetProductPart().m_fPageWidth;
		float fHeight 		= pFoldSheet->GetProductPart().m_fPageHeight;
		int	  nQuantity 	= pFoldSheet->GetQuantityPlanned();
		float fCoverage 	= pFoldSheet->CalcCoverage(pPrintSheet);
		int   nOver			= pFoldSheet->GetQuantityActual() - pFoldSheet->GetQuantityPlanned();
		float fOverPercent	= (pFoldSheet->GetQuantityPlanned() != 0.0f) ? (((float)pFoldSheet->GetQuantityActual()/(float)pFoldSheet->GetQuantityPlanned() - 1.0f) * 100.0f) : 0.0f;
		CRect rcRet, rcThumbnail;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColComponent:			rcRet = DrawFoldSheetTypeInfo 	   (pDC, rcColumn, pFoldSheet, nLayerIndex);	break;
			case ColPages:				rcRet = DrawFoldSheetTypePages	   (pDC, rcColumn, pFoldSheet, nLayerIndex);	break;
			case ColNUp:				rcRet = DrawComponentNUp	  	   (pDC, rcColumn, nNUp);						break;
			case ColFormat:				rcRet = DrawComponentFormat	  	   (pDC, rcColumn, fWidth, fHeight);			break;
			case ColQuantity:			rcRet = DrawComponentQuantity      (pDC, rcColumn, nQuantity);					break;
			case ColSheetCoverage:		rcRet = DrawComponentCoverage	   (pDC, rcColumn, fCoverage);					break;
			case ColOverProduction:		rcRet = DrawComponentOverProduction(pDC, rcColumn, nOver, fOverPercent);		break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + 5;
		}
		rcRow.top	 = rcBox.bottom + Pixel2MM(pDC, 5);
		rcRow.bottom = rcRow.top	+ nRowHeight;
	}

	rcRow.bottom = rcRow.top + 20;
	///// flat products
	for (int nFlatProductTypeIndex = 0; nFlatProductTypeIndex < pPrintSheet->m_flatProductTypeList.GetSize(); nFlatProductTypeIndex++)
	{
		/////// columns
		CRect rcColumn = rcRow; 
		int	  nRight   = 0;
		
		CFlatProduct* pFlatProduct = pPrintSheet->m_flatProductTypeList[nFlatProductTypeIndex].m_pFlatProduct;
		if ( ! pFlatProduct)
			continue;
		int	  nNUp			= pPrintSheet->m_flatProductTypeList[nFlatProductTypeIndex].m_nNumUp;
		float fWidth		= pFlatProduct->m_fTrimSizeWidth;
		float fHeight		= pFlatProduct->m_fTrimSizeHeight;
		int	  nQuantity 	= pFlatProduct->m_nDesiredQuantity;
		float fCoverage 	= pFlatProduct->CalcCoverage(pPrintSheet);
		int   nOver			= pFlatProduct->m_nRealQuantity - pFlatProduct->m_nDesiredQuantity;
		float fOverPercent	= (pFlatProduct->m_nDesiredQuantity != 0.0f) ? (((float)pFlatProduct->m_nRealQuantity/(float)pFlatProduct->m_nDesiredQuantity - 1.0f) * 100.0f) : 0.0f;
		CRect rcRet, rcThumbnail;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColComponent:			rcRet = DrawFlatProductTypeInfo    (pDC, rcColumn, pFlatProduct);		 break;
			case ColNUp:				rcRet = DrawComponentNUp	       (pDC, rcColumn, nNUp);				 break;
			case ColFormat:				rcRet = DrawComponentFormat	       (pDC, rcColumn, fWidth, fHeight);	 break;
			case ColQuantity:			rcRet = DrawComponentQuantity      (pDC, rcColumn, nQuantity);			 break;
			case ColSheetCoverage:		rcRet = DrawComponentCoverage	   (pDC, rcColumn, fCoverage);			 break;
			case ColOverProduction:		rcRet = DrawComponentOverProduction(pDC, rcColumn, nOver, fOverPercent); break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + 5;
		}
		rcRow.top	 = rcBox.bottom + Pixel2MM(pDC, 5);
		rcRow.bottom = rcRow.top	+ nRowHeight;
	}

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CPrintSheetComponentsView::DrawFoldSheetTypeInfo(CDC* pDC, CRect rcFrame, CFoldSheet* pFoldSheet, int nLayerIndex)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pFoldSheet)
		return rcBox;

	int	nGap		= Pixel2MM(pDC, 11);
	int nBorder		= Pixel2MM(pDC, 5);
	int nTextHeight	= Pixel2MM(pDC, 18);
	int nOffset		= Pixel2MM(pDC, 35);

	float fRatio = (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nGap			= (int)((float)nGap				* fRatio);
	nBorder			= (int)((float)nBorder			* fRatio);	
	nTextHeight		= (int)((float)nTextHeight		* fRatio);
	nOffset			= (int)((float)nOffset			* fRatio);

	int nNameLeft = rcFrame.left + nOffset + 2 * nBorder;

	CFont	smallFont, boldFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(RGB(50,50,50));

	CRect rcThumbnail = rcFrame; rcThumbnail.left += nBorder;
	rcThumbnail = pFoldSheet->DrawThumbnail(pDC, rcThumbnail, TRUE, FALSE, nLayerIndex);	
	rcBox.UnionRect(rcBox, rcThumbnail);

	int nMaxFoldSheetNumWidth = pDoc->m_printGroupList.GetLongestFoldSheetNumString(pDC, pFoldSheet->GetPrintSheet(0)) + pDC->GetTextExtent(_T("  :")).cx;

	pDC->SelectObject(&boldFont);
	pDC->SetTextColor(RGB(255, 150, 50));

	CRect rcText = rcFrame; rcText.left += nOffset + nBorder; rcText.right = rcText.left + min(nMaxFoldSheetNumWidth, rcFrame.Width()) - nBorder; 
	rcText.bottom = rcText.top + nTextHeight;

	CString strOut;
	strOut.Format(_T("%s:"), pFoldSheet->GetNumber());
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	pDC->SelectObject(&smallFont);

	pDC->SetTextColor(GUICOLOR_CAPTION);
	rcText.left = rcText.right + nGap; rcText.right = rcFrame.left + rcFrame.Width() - nBorder; 
	strOut.Format(_T("%s"), pFoldSheet->m_strFoldSheetTypeName);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	boldFont.DeleteObject();
	smallFont.DeleteObject();

	return rcBox;
}

CRect CPrintSheetComponentsView::DrawFoldSheetTypePages(CDC* pDC, CRect rcFrame, CFoldSheet* pFoldSheet, int nLayerIndex)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pFoldSheet)
		return rcBox;

	int nBorder		= Pixel2MM(pDC, 5);
	int nTextHeight	= Pixel2MM(pDC, 18);

	float fRatio = (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nBorder			= (int)((float)nBorder		* fRatio);	
	nTextHeight		= (int)((float)nTextHeight	* fRatio);

	CFont smallFont, boldFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_CAPTION);

	CRect rcText = rcFrame; rcText.right -= nBorder; 
	rcText.bottom = rcText.top + nTextHeight;

	CString strOut; strOut.Format(_T("%d"), (pFoldSheet) ? pFoldSheet->GetNumPages() : 0);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	pDC->SelectObject(&smallFont);
	pDC->SetTextColor(RGB(50,50,50));
	rcText.left += pDC->GetTextExtent(strOut).cx + 2*nBorder;
	strOut.Format(_T("%s"), (pFoldSheet) ? pFoldSheet->GetPageRange() : _T(""));
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	smallFont.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CPrintSheetComponentsView::DrawComponentNUp(CDC* pDC, CRect rcFrame, int nNUp)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nBorder		= Pixel2MM(pDC, 5);
	int nTextHeight	= Pixel2MM(pDC, 18);

	float fRatio	= (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nBorder			= (int)((float)nBorder		* fRatio);	
	nTextHeight		= (int)((float)nTextHeight	* fRatio);

	CFont smallFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame; rcText.right -= nBorder; 
	rcText.bottom = rcText.top + nTextHeight;

	CString strOut; strOut.Format(_T("%d"), nNUp);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	smallFont.DeleteObject();

	return rcBox;
}

CRect CPrintSheetComponentsView::DrawComponentFormat(CDC* pDC, CRect rcFrame, float fWidth, float fHeight)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nBorder		= Pixel2MM(pDC, 5);
	int nTextHeight	= Pixel2MM(pDC, 18);

	float fRatio	= (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nBorder			= (int)((float)nBorder		* fRatio);	
	nTextHeight		= (int)((float)nTextHeight	* fRatio);

	CFont smallFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame; rcText.right -= nBorder; 
	rcText.bottom = rcText.top + nTextHeight;

	CString strWidth, strHeight, strOut;
	CPrintSheetView::TruncateNumber(strWidth, fWidth);
	CPrintSheetView::TruncateNumber(strHeight, fHeight);
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	smallFont.DeleteObject();

	return rcBox;
}

CRect CPrintSheetComponentsView::DrawFlatProductTypeInfo(CDC* pDC, CRect rcFrame, CFlatProduct* pFlatProduct)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pFlatProduct)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	float fMaxHeight = pDoc->m_flatProducts.GetMaxHeight();

	CString strOut;
	CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + nTextHeight;
	pDC->SetTextColor(RGB(200,100,0));
	strOut.Format(_T("%d: "), pFlatProduct->GetIndex() + 1);
	DrawTextMeasured(pDC, strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	int nLeft = rcTitle.left + pDC->GetTextExtent(strOut).cx;

	int nHeight = (int)((float)(nTextHeight) * (pFlatProduct->m_fTrimSizeHeight / fMaxHeight));		
	int nWidth	= 2*nTextHeight;
	CRect rcImg = CRect(CPoint(nLeft, rcFrame.top),  CSize(nWidth, nHeight));	

	POSITION pos = pDoc->m_PageTemplateList.FindIndex(pFlatProduct->GetIndex() * 2, pDoc->GetFlatProductsIndex());
	CRect rcProduct = pFlatProduct->DrawPreview(pDC, rcImg + CSize(0, 3), (pos) ? &pDoc->m_PageTemplateList.GetAt(pos) : NULL, NULL, FALSE, TRUE);
	rcBox.UnionRect(rcBox, rcProduct);

	pDC->SetTextColor(GUICOLOR_CAPTION);
	rcTitle = rcFrame; rcTitle.left = rcImg.right + Pixel2MM(pDC, 11); rcTitle.bottom = rcTitle.top + nTextHeight;
	pDC->DrawText(pFlatProduct->m_strProductName, rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	rcTitle.right = rcTitle.left + pDC->GetTextExtent(pFlatProduct->m_strProductName).cx;
	rcBox.UnionRect(rcBox, rcTitle);

	font.DeleteObject();

	return rcBox;
}

CRect CPrintSheetComponentsView::DrawComponentQuantity(CDC* pDC, CRect rcFrame, int nQuantity)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nBorder		= Pixel2MM(pDC, 5);
	int nTextHeight	= Pixel2MM(pDC, 18);

	float fRatio	= (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nBorder			= (int)((float)nBorder		* fRatio);	
	nTextHeight		= (int)((float)nTextHeight	* fRatio);

	CFont smallFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;  rcText.right = rcText.left + pDC->GetTextExtent(_T("00.000.000")).cx + nBorder; 
	rcText.bottom = rcText.top + nTextHeight;

	CString strOut = FormatQuantityString(nQuantity);
	pDC->DrawText(strOut, rcText, DT_RIGHT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	smallFont.DeleteObject();

	return rcBox;
}

CRect CPrintSheetComponentsView::DrawComponentCoverage(CDC* pDC, CRect rcFrame, float fCoverage)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nBorder		= Pixel2MM(pDC, 5);
	int nTextHeight	= Pixel2MM(pDC, 18);

	float fRatio	= (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nBorder			= (int)((float)nBorder		* fRatio);	
	nTextHeight		= (int)((float)nTextHeight	* fRatio);

	CFont smallFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame; rcText.right = rcText.left + pDC->GetTextExtent(_T("000.0%%")).cx + nBorder; 
	rcText.bottom = rcText.top + nTextHeight;

	CString strOut; strOut.Format(_T("%.1f%%"), fCoverage * 100.0f);
	pDC->DrawText(strOut, rcText, DT_RIGHT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcBox.bottom = max(rcBox.bottom, rcText.bottom);

	smallFont.DeleteObject();
	return rcBox;
}

CRect CPrintSheetComponentsView::DrawComponentOverProduction(CDC* pDC, CRect rcFrame, int nOver, float fOverPercent)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nBorder		= Pixel2MM(pDC, 70);
	int nTextHeight	= Pixel2MM(pDC, 18);

	float fRatio	= (pDC->IsPrinting()) ? 0.6f : 1.0f;
	nBorder			= (int)((float)nBorder		* fRatio);	
	nTextHeight		= (int)((float)nTextHeight	* fRatio);

	CFont smallFont;
	smallFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&smallFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame; rcText.right -= nBorder; 
	rcText.bottom = rcText.top + nTextHeight;

	CString strOut = FormatQuantityString(nOver);
	pDC->DrawText(strOut, rcText, DT_RIGHT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
	rcText.left = rcText.right + 5; rcText.right = rcFrame.right - 5;
	CString strPercentage;	strPercentage.Format(_T("%.1f"), fOverPercent);
	strOut.Format(_T("%s%%"), strPercentage);
	if (nOver < 0)
		pDC->SetTextColor(RED);
	else
		pDC->SetTextColor(RGB(80,150,80));
	pDC->DrawText(strOut, rcText, DT_RIGHT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);

	smallFont.DeleteObject();
	return rcBox;
}

CPrintSheet* CPrintSheetComponentsView::GetActPrintSheet()
{
	return CNewPrintSheetFrame::GetActPrintSheet();
}

CLayout* CPrintSheetComponentsView::GetActLayout()
{
	return CNewPrintSheetFrame::GetActLayout();
}


// CPrintSheetComponentsView-Meldungshandler

