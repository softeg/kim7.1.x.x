// BookBlockView.cpp : implementation of the CFoldSheetView class
//

#include "stdafx.h"

#include "CntrItem.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "BookBlockView.h"
#include "DlgShinglingAssistent.h"
#include "MyRectTracker.h"
#include "BookblockFrame.h"
#include "BookBlockDataView.h"
#include "BookBlockProdDataView.h"
#include "PrintSheetView.h"
#include "ProductsView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
// CBookBlockView

IMPLEMENT_DYNCREATE(CBookBlockView, CScrollView)

BEGIN_MESSAGE_MAP(CBookBlockView, CScrollView)
	//{{AFX_MSG_MAP(CBookBlockView)
    ON_WM_ERASEBKGND()
	ON_WM_CONTEXTMENU()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_COMMAND(ID_OLE_INSERT_NEW, OnInsertObject)
	ON_COMMAND(ID_CANCEL_EDIT_CNTR, OnCancelEditCntr)
	ON_WM_LBUTTONDOWN()
	ON_WM_CREATE()
	ON_WM_KEYDOWN()
	ON_WM_SETCURSOR()
	ON_UPDATE_COMMAND_UI(ID_DELETE_SELECTED_FOLDSHEET, OnUpdateDeleteSelectedFoldsheet)
	ON_WM_KEYUP()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_DELETE_SELECTED_FOLDSHEET, OnDeleteSelectedFoldsheet)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
	ON_WM_NCCALCSIZE()
	//ON_WM_NCPAINT()
	ON_WM_NCMOUSEMOVE()
	ON_WM_NCLBUTTONDOWN()
	ON_WM_NCHITTEST()
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

///////////////////////////////////////////////////////////////////////////////////////////////////
// CBookBlockView construction/destruction

CBookBlockView::CBookBlockView()
{
	m_nProductIndex				= 0;
	m_nProductPartIndex			= 0;
	m_pSelection				= NULL;
	m_nViewBorder				= 40;
	m_nShinglingValueTotal		= 0;
	m_rcPrintMargins			= CRect(0,0,0,0);
	m_fScreenPrinterRatioX		= 1.0f;
	m_fScreenPrinterRatioY		= 1.0f;
	m_bFoldSheetSelected		= FALSE;
	m_sizeFont					= CSize(0, 60);
	m_sizeBookBlockDP			= CSize(1,1);
	m_rectGotoImpositionScheme	= CRect(0,0,0,0);
	m_nZoomToFitMode			= TRUE;
	m_nMaxPaperThickness		= 10;

	m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CBookBlockView));	// TRUE = OLE DragDrop
}

CBookBlockView::~CBookBlockView()
{
}

BOOL CBookBlockView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CScrollView::PreCreateWindow(cs);
}

BOOL CBookBlockView::OnEraseBkgnd(CDC* pDC) 
{
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CBookBlockView message handlers

void CBookBlockView::OnInitialUpdate() 
{
	m_nViewBorder = 40;	

	CScrollView::OnInitialUpdate();

	EnableScrollBar(SB_HORZ, ESB_DISABLE_BOTH);
}

void CBookBlockView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/) 
{
	CBookblockFrame* pFrame = (CBookblockFrame*)GetParentFrame();
	if (pFrame)
	{
		CBookBlockDataView* pView = (CBookBlockDataView*)pFrame->m_wndSplitter.GetPane(0, 0);
		if (pView)
		{
			CBoundProduct&		rBoundProduct	  = pView->GetActiveBoundProduct();
			CBoundProductPart&	rBoundProductPart = pView->GetActiveBoundProductPart();
			m_nProductIndex		= rBoundProduct.GetIndex();
			m_nProductPartIndex = rBoundProductPart.GetIndex();
		}
	}

	GetBookBlockSize(-m_nProductIndex - 1);

	SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));

	Invalidate();	
}

void CBookBlockView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	CScrollView::OnPrepareDC(pDC, pInfo);

	pDC->SetMapMode(MM_ISOTROPIC); 

	m_fScreenPrinterRatioX = 1.0f;
	m_fScreenPrinterRatioY = 1.0f;

	if (m_nZoomToFitMode)
	{
		CRect  clientRect;
		GetClientRect(&clientRect);
		m_viewPortSize.cx = max(clientRect.Width()  - 2 * m_nViewBorder, m_nViewBorder);
		m_viewPortSize.cy = max(clientRect.Height() - 2 * m_nViewBorder, m_nViewBorder);
	}

	GetBookBlockSize(-m_nProductIndex - 1);
	if ((m_docSize.cx <= 0) || (m_docSize.cy <= 0))
		return;

	if (m_nZoomToFitMode)
	{
		if ( ((float)m_viewPortSize.cy / (float)m_docSize.cy) * m_nMaxPaperThickness > 10)	// limit paper thickness to 10 pixel when ZoomToFit
		{
			m_viewPortSize.cy = (10 * m_docSize.cy ) / m_nMaxPaperThickness;	
			GetBookBlockSize(-m_nProductIndex - 1);
		}
	}

	pDC->SetWindowExt(m_docSize);
	pDC->SetWindowOrg(0, m_docSize.cy);
	pDC->SetViewportExt(m_viewPortSize.cx, -m_viewPortSize.cy);    
	CPoint ptScrollPos = GetScrollPosition();
	pDC->SetViewportOrg(CPoint(m_nViewBorder/2, m_nViewBorder) - CSize(ptScrollPos.x, ptScrollPos.y));
}

/////////////////////////////////////////////////////////////////////////////
// CBookBlockView drawing

void CBookBlockView::OnDraw(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(rcClient);

	CDC	memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap bitmap;	bitmap.CreateCompatibleBitmap(pDC, rcClient.Width(), rcClient.Height());
	memDC.SelectObject(&bitmap);

	CRect rcBackGround = rcClient; 
	memDC.FillSolidRect(rcBackGround, RGB(127,162,207));

	memDC.SaveDC();
	OnPrepareDC(&memDC);

	Draw(&memDC);
	//Draw(pDC);
	
	memDC.RestoreDC(-1);

	pDC->SaveDC();
	pDC->SetMapMode(MM_TEXT);
	pDC->SetWindowOrg(0, 0);
	pDC->SetWindowExt(rcClient.Width(), rcClient.Height());
	pDC->SetViewportOrg(0, 0);
	pDC->SetViewportExt(rcClient.Width(), rcClient.Height());

	pDC->BitBlt(0, 0, rcClient.Width(), rcClient.Height(), &memDC, 0, 0, SRCCOPY);
	pDC->RestoreDC(-1);
	bitmap.DeleteObject();
	memDC.DeleteDC();
}

void CBookBlockView::Draw(CDC* pDC)
{
	CImpManDoc*	pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return;
	if ( ! pDoc->GetBoundProduct(m_nProductIndex).GetNumPages())
		return;

	pDoc->m_screenLayout.m_zoomStateBookBlockView.m_viewportSize = m_viewPortSize;
	pDoc->m_screenLayout.m_zoomStateBookBlockView.m_scrollPos	 = GetScrollPosition();

	m_DisplayList.Invalidate();
	m_DisplayList.BeginRegisterItems(pDC);

	DrawBookBlock(pDC);

	m_DisplayList.EndRegisterItems(pDC);

	if (pDC->IsPrinting())
	{
		pDC->SetMapMode(MM_TEXT); 
		pDC->SetWindowOrg(0, 0);

		CFont mediumFont;
		mediumFont.CreateFont((int)(16.0f * m_fScreenPrinterRatioX), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		CFont* pOldFont = pDC->SelectObject(&mediumFont);
		CString strOut;
		strOut.LoadString(IDS_BOOKBLOCK);
		int nMaxRight = (pDC->IsPrinting()) ? pDC->GetDeviceCaps(HORZRES) - m_rcPrintMargins.left - m_rcPrintMargins.right : pDC->GetDeviceCaps(HORZRES);
		CRect headerRect(CPoint(0, 0), CSize(nMaxRight, (int)(20.0f * m_fScreenPrinterRatioY)));
		headerRect.NormalizeRect();
		headerRect.OffsetRect(0, (int)(-30.0f * m_fScreenPrinterRatioY));
		pDC->FillSolidRect(headerRect, LIGHTGRAY);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(DARKBLUE);
		pDC->DrawText(strOut, CRect((int)(10.0f * m_fScreenPrinterRatioX), headerRect.top, headerRect.right, headerRect.bottom), DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		pDC->SelectObject(pOldFont);
		mediumFont.DeleteObject();
	}
}

int	CBookBlockView::GetPaperThickness(int nProductPartIndex)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 10;

	CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);

	return (int)( max(rProductPart.GetShinglingValueX(TRUE) * 1000.0f + 0.5f, 10.0) );	// min. paper thickness 0.01 mm
}

int	CBookBlockView::GetFoldSheetGap(int nProductPartIndex)
{
	return (int)( ((float)GetPaperThickness(nProductPartIndex) + 0.5f)/3.0);

	//CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	//CPrintGroup* pProductGroup = (pDoc) ? pDoc->m_printGroupList.GetPart(nProductPartIndex) : NULL;
	//if (pProductGroup)
	//{
	//	return (int)(max(pProductGroup->GetProductionProfile().m_postpress.m_folding.m_fShinglingValue * 100.0f + 0.5f, 3.0f) / 6.0f);
	//}
	//else
	//	return 1;
}



CPoint 		ptLeftTop;
CPoint 		ptLeftBottom;
CPoint 		ptRightTop;
CPoint 		ptRightBottom;
CPoint		ptShinglingLeftMin;
CPoint		ptShinglingRightMax;
int			g_nShingling;
int	   		nPageNum;
BOOL   		bBreakPainting = FALSE;
BOOL   		bCoverPainted  = FALSE;
COLORREF	g_crBkColor = WHITE;//RGB(255,245,220);

void CBookBlockView::DrawBookBlock(CDC* pDC)
{
	CImpManDoc*	pDoc = GetDocument();
	if (pDoc->m_Bookblock.IsEmpty())
		return;

	pDoc->m_Bookblock.CheckFoldSheetsPlausibility();

	ptLeftTop = ptLeftBottom = ptRightTop = ptRightBottom = CPoint(0,0);
	CPoint startPoint(0, m_docSize.cy);// - ( (pDC->IsPrinting()) ? m_nViewBorder/2 : m_nViewBorder) );
	nPageNum = 1;

	bBreakPainting = FALSE;
	bCoverPainted  = FALSE;

	m_sizeFont = CSize(0, (pDC->IsPrinting()) ? 50 : 65);
	pDC->LPtoDP(&m_sizeFont);
	int nThreshold = (int)(13.0f * m_fScreenPrinterRatioY);
	m_sizeFont.cy = (m_sizeFont.cy > nThreshold) ? nThreshold : m_sizeFont.cy;
	if (m_sizeFont.cy < 11) 
		m_sizeFont.cy = 11;
	pDC->DPtoLP(&m_sizeFont);
	CFont font;
	font.CreateFont(m_sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = (CFont*)pDC->SelectObject(&font);
	m_maxNameSize	   = pDoc->m_Bookblock.GetMaxFoldSheetNameExtent(pDC);
	m_maxPageRangeSize = pDoc->m_Bookblock.GetMaxFoldSheetPageRangeExtent(pDC, -m_nProductIndex - 1);

	g_nShingling = 0;
	POSITION pos = pDoc->m_Bookblock.GetHeadPosition_DescriptionList(m_nProductIndex);
	while (pos && !bBreakPainting)
		DrawFoldSheet(pDC, pos, startPoint, -m_nProductIndex - 1);

	pDC->SelectObject(pOldFont);
	font.DeleteObject();

	CPoint p[4];
	p[0] = ptLeftBottom;
	p[1] = ptLeftTop; 
	p[2] = ptRightTop;
	p[3] = ptRightBottom;

	CPen whitePen, dashedPen;
	whitePen.CreatePen (PS_SOLID, 4, RGB(128,128,150));//DARKGRAY);
	dashedPen.CreatePen(PS_DOT,   1, MIDDLEGRAY);
	CPen* pOldPen = (CPen*)pDC->SelectObject(&whitePen);
	//if ( ! pDC->IsPrinting())
	//	pDC->Polyline(p, 3);
	if (pDoc->GetBoundProduct(m_nProductIndex).m_bindingParams.m_bComingAndGoing)
	{
		CPoint p1(p[0].x + ((p[1].x - p[0].x)*3)/5, p[0].y + ((p[1].y - p[0].y)*3)/5);
		CPoint p2(p[3].x + ((p[2].x - p[3].x)*3)/5, p1.y);
		pDC->SelectObject(&dashedPen);
		pDC->MoveTo(p1);
		pDC->LineTo(p2);
	}

	CString strMeasure;
	float	fShinglingOffsetX, fShinglingOffsetXFoot, fShinglingOffsetY;
	pDoc->m_PageTemplateList.CalcShinglingOffsets(fShinglingOffsetX, fShinglingOffsetXFoot, fShinglingOffsetY, -m_nProductIndex - 1);
	if (fabs(fShinglingOffsetX) >= 0.001)
	{
		SetMeasure(0, fShinglingOffsetX, strMeasure);
		int nTop = ptRightTop.y + (ptRightTop.y - ptRightBottom.y)/2;
		CPoint pt[2] = {CPoint(ptShinglingLeftMin.x, nTop), CPoint(ptShinglingRightMax.x, nTop)};
		CPrintSheetView::DrawHMeasuring(pDC, pt, strMeasure, ptShinglingLeftMin, ptShinglingRightMax, FALSE);
	}

	pDC->SelectObject(pOldPen);
	whitePen.DeleteObject();
	dashedPen.DeleteObject();
}

void CBookBlockView::DrawFoldSheet(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex)
{
	CDescriptionItem Item = GetDocument()->m_Bookblock.m_DescriptionList.GetAt(pos);

	CPoint endPointLeft = DrawFoldSheetLeftPart(pDC, pos, startPoint, nProductPartIndex);

	while ((GetFoldSheetPart(pos) == CDescriptionItem::FoldSheetLeftPart) && !bBreakPainting)
		DrawFoldSheet(pDC, pos, startPoint, nProductPartIndex);

	CPoint endPointRight = DrawFoldSheetRightPart(pDC, pos, startPoint, nProductPartIndex);

	DrawConnectionLeftRight(pDC, Item, endPointLeft, endPointRight, nProductPartIndex);
}


int CBookBlockView::GetFoldSheetPart(POSITION& pos)
{
	if (!pos)
		return CDescriptionItem::FoldSheetRightPart;

	CDescriptionItem Item = GetDocument()->m_Bookblock.m_DescriptionList.GetAt(pos);
	return Item.m_nFoldSheetPart;
}

CPoint CBookBlockView::DrawFoldSheetLeftPart(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex)
{
	if (!pos)
		return startPoint;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return startPoint;

	CDescriptionItem& rItem			  = pDoc->m_Bookblock.m_DescriptionList.GetAt(pos);
	CFoldSheet&		  rFoldSheet	  = rItem.GetFoldSheet(&(pDoc->m_Bookblock));

	int			r	 = GetFoldSheetRadiusLeftPart(pos, rFoldSheet.m_nProductPartIndex);
	CPoint		cp	 = startPoint + CSize(r, -r);

	//if (nProductPartIndex != -1)
	//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
	//	{
	//		pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
	//		return startPoint;
	//	}

	int	nSheetThickness = (rFoldSheet.m_nPagesLeft/2)*GetPaperThickness(rFoldSheet.m_nProductPartIndex);	
	if (nSheetThickness == 0)	// if only one page is on the sheet
		nSheetThickness = GetPaperThickness(rFoldSheet.m_nProductPartIndex) / 2;
	r-= GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nSheetThickness/2;

	RenderFoldSheet(pDC, rItem, nSheetThickness, cp, r, nProductPartIndex);

	BOOL bAboutToClose = FALSE;
	POSITION pos2nd = pos; 
	pDoc->m_Bookblock.GetNext_DescriptionList(m_nProductIndex, pos2nd);
	if (pos2nd)
	{
		CDescriptionItem& r2ndItem = pDoc->m_Bookblock.m_DescriptionList.GetAt(pos2nd);
		if ( (r2ndItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart) && 
			 (r2ndItem.m_nFoldSheetIndex == r2ndItem.m_nFoldSheetIndex) )		
			bAboutToClose = TRUE;
	}
	if ( ! bAboutToClose || (rFoldSheet.GetNumPages() <= 2))	// if sheet will be closed in next step, we draw text when right part is painted (see DrawFoldSheetRightPart())
	{															// or we have just one page
		CString  strText;
		int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
		COLORREF crOldBkColor	= pDC->SetBkColor(g_crBkColor);
		COLORREF crOldTextColor = pDC->SetTextColor(RED);
		strText.Format(_T(" %s "), rFoldSheet.GetNumber()); 

		CSize szDistance(20,20);
		pDC->DPtoLP(&szDistance);
		int nXPos = cp.x + szDistance.cx;
		int nYPos = (rFoldSheet.GetNumPages() <= 2) ? (cp.y + 2*(m_sizeFont.cy/3)) : (cp.y + r + m_sizeFont.cy/2);
		pDC->TextOut(nXPos, nYPos, strText);

		strText.Format(_T("%s"), rFoldSheet.m_strFoldSheetTypeName); 

		pDC->SetBkColor(g_crBkColor);
		pDC->SetTextColor(BLACK);
		nXPos += 4 * m_sizeFont.cy;
		pDC->TextOut(nXPos, nYPos, strText);

		pDC->LPtoDP(&m_sizeFont);
		BOOL bShowText = (m_sizeFont.cy < 8) ? FALSE : TRUE;
		pDC->DPtoLP(&m_sizeFont);

		if (bShowText)
		{
			pDC->SetTextColor(DARKBLUE);
			strText.Format(_T(" %s "), rFoldSheet.GetPageRange(LEFT));
			pDC->TextOut(nXPos + m_maxNameSize.cx + m_sizeFont.cy, nYPos, strText);

			CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();
			if ( ! rProductPart.IsNull() && ! rProductPart.m_strName.IsEmpty())
			{
				CSize sizeFont(11,11);
				pDC->DPtoLP(&sizeFont);
				CFont font;
				font.CreateFont(sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
				CFont* pOldFont = (CFont*)pDC->SelectObject(&font);

				strText.Format(_T("%s"), rProductPart.m_strName); 
				CSize sizeText = pDC->GetTextExtent(strText);
				int   nXPosText = nXPos + m_maxNameSize.cx + m_maxPageRangeSize.cx + 5 * m_sizeFont.cy;
				if (nXPosText + sizeText.cx < ptRightBottom.x)
				{
					pDC->SetBkColor(RGB(233,240,130));
					pDC->TextOut(nXPosText, nYPos, strText);
				}

				pDC->SelectObject(pOldFont);
				font.DeleteObject();
			}
		}

		pDC->SetBkMode(nOldBkMode);
		pDC->SetBkColor(crOldBkColor);
		pDC->SetTextColor(crOldTextColor);
	}

	int d = 2*GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nSheetThickness; 
	startPoint+= CSize(d,-d);

//	TRACE("<%d ", Item.m_nFoldSheetIndex); 

	pDoc->m_Bookblock.GetNext_DescriptionList(m_nProductIndex, pos);

	return CPoint(cp.x - r, cp.y);
}

CPoint CBookBlockView::DrawFoldSheetRightPart(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex)
{
	if (!pos)
		return startPoint;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return startPoint;

	CDescriptionItem& rItem			  = pDoc->m_Bookblock.m_DescriptionList.GetAt(pos);
	CFoldSheet&		  rFoldSheet	  = rItem.GetFoldSheet(&(pDoc->m_Bookblock));
	//if (nProductPartIndex != -1)
	//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
	//	{
	//		pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
	//		return startPoint;
	//	}

	int	nSheetThickness = (rFoldSheet.m_nPagesRight/2)*GetPaperThickness(rFoldSheet.m_nProductPartIndex);	
	if (nSheetThickness == 0)	// if only one page is on the sheet
		nSheetThickness = GetPaperThickness(rFoldSheet.m_nProductPartIndex) / 2;

	int d = 2*GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nSheetThickness; 
	startPoint+= CSize(-d,-d);

	int	   r  = GetFoldSheetRadiusRightPart(pos, rFoldSheet.m_nProductPartIndex);
	CPoint cp = startPoint + CSize(r, r);

	r-= GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nSheetThickness/2;

	RenderFoldSheet(pDC, rItem, nSheetThickness, cp, r, nProductPartIndex);

	BOOL bJustClosed = FALSE;
	POSITION pos2nd = pos;
	pDoc->m_Bookblock.GetPrev_DescriptionList(m_nProductIndex, pos2nd);
	if (pos2nd)
	{
		CDescriptionItem& r2ndItem = pDoc->m_Bookblock.m_DescriptionList.GetAt(pos2nd);
		if ( (r2ndItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) && 
			 (r2ndItem.m_nFoldSheetIndex == rItem.m_nFoldSheetIndex) )		
			bJustClosed = TRUE;
	}
	BOOL bAddText = (rFoldSheet.GetNumPages() > 2) ? TRUE : FALSE;

	CString  strText;
	int		 nOldBkMode		= pDC->SetBkMode(OPAQUE);
	COLORREF crOldBkColor	= pDC->SetBkColor(g_crBkColor);
	COLORREF crOldTextColor = pDC->SetTextColor(RED);
	strText.Format(_T(" %s "), rFoldSheet.GetNumber()); 
	CSize szDistance(20,20);
	pDC->DPtoLP(&szDistance);
	int nXPos = cp.x + szDistance.cx;
	int nYPos = (bJustClosed) ? (cp.y + m_sizeFont.cy/2) : (cp.y - r + m_sizeFont.cy/2);
	if (bAddText)
		pDC->TextOut(nXPos, nYPos, strText);

	nXPos += 4 * m_sizeFont.cy;
	if (bAddText)
	{
		if (bJustClosed)
		{
			strText.Format(_T("%s"), rFoldSheet.m_strFoldSheetTypeName); 
			pDC->SetBkColor(g_crBkColor);
			pDC->SetTextColor(BLACK);
			pDC->TextOut(nXPos, nYPos, strText);
		}

		pDC->SetTextColor(DARKBLUE);
		strText.Format(_T(" %s "), rFoldSheet.GetPageRange((bJustClosed) ? BOTHSIDES : RIGHT));
		pDC->TextOut(nXPos + m_maxNameSize.cx + m_sizeFont.cy, nYPos, strText);

		CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();
		if ( ! rProductPart.IsNull() && ! rProductPart.m_strName.IsEmpty())
		{
			CSize sizeFont(11,11);
			pDC->DPtoLP(&sizeFont);
			CFont font;
			font.CreateFont(sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
			CFont* pOldFont = (CFont*)pDC->SelectObject(&font);

			strText.Format(_T("%s"), rProductPart.m_strName); 
			CSize sizeText = pDC->GetTextExtent(strText);
			int   nXPosText = nXPos + m_maxNameSize.cx + m_maxPageRangeSize.cx + 5 * m_sizeFont.cy;
			if (nXPosText + sizeText.cx < ptRightBottom.x)
			{
				pDC->SetBkColor(RGB(233,240,130));
				pDC->TextOut(nXPos + m_maxNameSize.cx + m_maxPageRangeSize.cx + 5 * m_sizeFont.cy, nYPos, strText);
			}

			pDC->SelectObject(pOldFont);
			font.DeleteObject();
		}
	}

	pDC->SetBkMode(nOldBkMode);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetTextColor(crOldTextColor);

//	TRACE("%d> ", Item.m_nFoldSheetIndex); // close foldsheet 

	pDoc->m_Bookblock.GetNext_DescriptionList(m_nProductIndex, pos);

	return CPoint(cp.x - r, cp.y);
}

void CBookBlockView::DrawConnectionLeftRight(CDC* pDC, CDescriptionItem& Item, CPoint endPointLeft, CPoint endPointRight, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CFoldSheet&	rFoldSheet = Item.GetFoldSheet(&(GetDocument()->m_Bookblock));

	if ((rFoldSheet.m_nPagesLeft == 0) || (rFoldSheet.m_nPagesRight == 0))
		return;

	if (endPointLeft == endPointRight)
		return;

	int	nSheetThickness = min((rFoldSheet.m_nPagesLeft/2)*GetPaperThickness(rFoldSheet.m_nProductPartIndex), (rFoldSheet.m_nPagesRight/2)*GetPaperThickness(rFoldSheet.m_nProductPartIndex));	
	if (nSheetThickness == 0)	// if only one page is on the sheet
		nSheetThickness = GetPaperThickness(rFoldSheet.m_nProductPartIndex) / 2;

	int nPages;
	if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		nPages = rFoldSheet.m_nPagesLeft;
	else
		nPages = rFoldSheet.m_nPagesRight;

	BOOL bIsCover	  = (pDoc->GetBoundProduct(m_nProductIndex).GetCoverProductPartIndex() == rFoldSheet.m_nProductPartIndex) ? TRUE : FALSE;
	int nPenThickness = max(GetPaperThickness(rFoldSheet.m_nProductPartIndex), 1);
	CPen whitePen;
	whitePen.CreatePen(PS_SOLID, nPenThickness, (bIsCover) ? LIGHTGRAY : WHITE);
	CPen* pOldPen = (CPen*)pDC->SelectObject(&whitePen);

	endPointLeft.x+=  nSheetThickness/2;
	endPointRight.x+= nSheetThickness/2;
	for (int i = 0; i < nPages; i++)
	{
		pDC->MoveTo(endPointLeft);
		if (i%2)
			pDC->LineTo(endPointRight);

		endPointLeft.x-=  nSheetThickness/nPages;
		endPointRight.x-= nSheetThickness/nPages;
	}

	pDC->SelectObject(pOldPen);
	whitePen.DeleteObject();
}

void CBookBlockView::RenderFoldSheet(CDC* pDC, CDescriptionItem& Item, int nSheetThickness, CPoint& cp, int nRadius, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CFoldSheet&	rFoldSheet = Item.GetFoldSheet(&(pDoc->m_Bookblock));
	int			nPages;

	if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		nPages = rFoldSheet.m_nPagesLeft;
	else
		nPages = rFoldSheet.m_nPagesRight;

	if (nPages == 0)
		return;

	CBoundProductPart& rProductPart = rFoldSheet.GetProductPart();

	BOOL bIsCover		   = (rProductPart.IsCover()) ? TRUE : FALSE;
	int   nSideTrim		   = (int)(rProductPart.m_trimmingParams.m_fSideTrim * 1000.0f + 0.5f);
	BOOL  bShinglingActive = rProductPart.m_foldingParams.m_bShinglingActive;

	if (rProductPart.GetBoundProduct().m_bindingParams.m_bComingAndGoing)
		nPages/= 2;

	if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
		nRadius+= nSheetThickness/2;
	else
		nRadius-= nSheetThickness/2;

	CRect  arcRect;
	CPoint startPt, endPt;
	CRect  rect;

	int nPenThickness = max(GetPaperThickness(rFoldSheet.m_nProductPartIndex), 1);
	CPoint pt(m_docSize.cx - m_nShinglingValueTotal, 0);

	pt.x = max(pt.x, cp.x);

	rect.TopLeft()	   = cp + CSize(0, (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) ? nRadius : -nRadius);
	rect.BottomRight() = CPoint(pt.x, rect.top - nSheetThickness);
	rect.InflateRect(nPenThickness/5, nPenThickness/5);	// inflate means deflate, because not normalized
	CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, rect, rect, (void*)&Item, DISPLAY_ITEM_REGISTRY(CBookBlockView, FoldSheet), NULL, CDisplayItem::ForceRegister, NULL);
	if (pDI)
		pDI->SetHighlightColor(RED);

	if ( ! pDC->IsPrinting())
	{
		if ( ! m_DisplayList.IsInvalid())
		{
			CRect clientRect;
			GetClientRect(clientRect);
			clientRect.bottom += GetScrollPos(SB_VERT);
			pDC->DPtoLP(clientRect);
			if (rect.top < clientRect.bottom)	// break painting
			{
				bBreakPainting = TRUE;
				return;
			}
		}
	}

//	if (!pDC->RectVisible(rect))	// results in corrupt painting of bookblock parts when moving the scrollbar
//		return;

	int nPagePreviewIndex1 = -1;
	CBookBlockProdDataView* pView = (CBookBlockProdDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockProdDataView));
	if (pView)
		nPagePreviewIndex1 = pView->m_nPagePreviewIndex;

	int nPagePreviewIndex2 = (nPagePreviewIndex1 == 0) ? 0 : nPagePreviewIndex1 + 1;

	CSize szPenDP(3,3);
	pDC->DPtoLP(&szPenDP);
	szPenDP.cx = max(min(nPenThickness/2, szPenDP.cx), 1);
	szPenDP.cy = max(min(nPenThickness/2, szPenDP.cy), 1);

	CPen whitePen, lightgrayPen, grayPen, yellowPen, lightYellowPen, bluePen, darkBluePen, greenPen;
	whitePen.CreatePen		(PS_SOLID, nPenThickness, RGB(250,250,255));
	lightgrayPen.CreatePen	(PS_SOLID, nPenThickness, LIGHTGRAY);
	yellowPen.CreatePen		(PS_SOLID, szPenDP.cy,	  RGB(130,90,   3));
	lightYellowPen.CreatePen(PS_SOLID, szPenDP.cy,	  RGB(252,211,129));
	darkBluePen.CreatePen	(PS_SOLID, szPenDP.cy,	  DARKBLUE);
	grayPen.CreatePen		(PS_SOLID, 1,			  MIDDLEGRAY);
	CPen* pOldPen = (CPen*)pDC->SelectObject(&whitePen);

	CArray <short*, short*> PageSequence;
	int						nPageSeqIndex = 0;

	if (rFoldSheet.GetBoundProduct().m_bindingParams.m_bComingAndGoing)
	{
		rFoldSheet.GetPageSequence(PageSequence, CDescriptionItem::FoldSheetLeftPart);
		rFoldSheet.ReducePageSequence(PageSequence);
		if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			PageSequence.RemoveAt(rFoldSheet.m_nPagesLeft/2, rFoldSheet.m_nPagesLeft/2); 
		else
			PageSequence.RemoveAt(0, rFoldSheet.m_nPagesLeft/2); 
	}
	else
	{
		rFoldSheet.GetPageSequence(PageSequence, Item.m_nFoldSheetPart);
		rFoldSheet.ReducePageSequence(PageSequence);
	}

	CString strMinPage, strMaxPage;
	for (int i = 0; i < nPages; i++)
	{
		if (PageSequence.GetSize() == 0) // If the foldsheet isn't correct signed
			break;						 // -> you can get this effect

		if (i%2 || nPages == 1)
		{
			if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
				g_nShingling -= (int)((float)rProductPart.GetShinglingValueX()*1000.0f + 0.5f);

			if ( (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart) || (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight <= 2) )
			{
				pt.y = cp.y + nRadius;

				if (ptLeftBottom == ptLeftTop)	// points not yet initialized
				{
					CSize size3D(40, 15);
					pDC->DPtoLP(&size3D);
					int delta	  = (int)((float)nRadius/8.0f);
					ptLeftBottom  = cp + CSize(-delta, nRadius);//CSize(-delta, delta);
					ptLeftTop	  = cp + ( (pDC->IsPrinting()) ? CSize(0, nRadius) : CSize(size3D.cx, nRadius + size3D.cy) );
					ptRightTop	  = pt + ( (pDC->IsPrinting()) ? CSize(0, 0)	   : CSize(-size3D.cx, size3D.cy) );
					if (ptLeftTop.x < ptRightTop.x)
					{
						ptRightBottom = CPoint(pt.x, ptLeftBottom.y);
						ptShinglingLeftMin  = CPoint(INT_MAX, ptRightBottom.y);
						ptShinglingRightMax = CPoint(0,		  ptRightBottom.y);
						CPoint ptCover[5];
						ptCover[0] = ptLeftBottom;
						ptCover[1] = ptLeftTop;
						ptCover[2] = ptRightTop;
						ptCover[3] = ptRightBottom;
						ptCover[4] = ptLeftBottom;//cp + CSize(0, nRadius);
						CBrush fillBrush;
						fillBrush.CreateSolidBrush(RGB(240,240,240));
						CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&fillBrush);
						pDC->BeginPath();
						pDC->Polyline(ptCover, 5);
						//pDC->Arc(arcRect, cp + CSize(0, nRadius), cp + CSize(-nRadius, nRadius));
						pDC->EndPath();
						pDC->FillPath();
						pDC->SelectObject(pOldBrush);
						fillBrush.DeleteObject();
					}
					bCoverPainted = TRUE;
				}
			}
			else
			{
				if (nPages < 1)	// It's a very special case
					break;		// where the right part doesn't exist  

				pt.y = cp.y - nRadius;
			}

			int	  nCurPageIndex1   = -1;
			int	  nCurPageIndex2   = -1;
			float fShinglingValue1 = 0.0f;
			float fShinglingValue2 = 0.0f;
			int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(rFoldSheet.m_nProductPartIndex);
			POSITION pos = (nPageSeqIndex < PageSequence.GetSize()) ? pDoc->m_PageTemplateList.FindIndex(*PageSequence[nPageSeqIndex] - 1, -nProductIndex - 1) : NULL;
			if (pos)
			{
				CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
				if ( i == 1)
					strMinPage   = rPageTemplate.m_strPageID;
				nCurPageIndex1	 = pDoc->m_PageTemplateList.GetIndex(&rPageTemplate, -rPageTemplate.m_nProductIndex - 1);
				fShinglingValue1 = rPageTemplate.m_fShinglingOffsetX;
			}
			pos = (nPageSeqIndex + 1 < PageSequence.GetSize()) ? pDoc->m_PageTemplateList.FindIndex(*PageSequence[nPageSeqIndex+1] - 1, -nProductIndex - 1) : NULL;
			if (pos)
			{
				CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
				if (i == nPages - 1)
					strMaxPage = rPageTemplate.m_strPageID;
				nCurPageIndex2	 = pDoc->m_PageTemplateList.GetIndex(&rPageTemplate, -rPageTemplate.m_nProductIndex - 1);
				fShinglingValue2 = rPageTemplate.m_fShinglingOffsetX;
			}

			CPoint ptShinglingRight (pt.x + g_nShingling, pt.y);
			CPoint ptNetRight		(ptShinglingRight);// - CSize(nShinglingValue, 0));

			pDC->MoveTo(ptShinglingRight);	

			if (ptShinglingLeftMin.x + nPenThickness/2 <= ptShinglingLeftMin.x)
				ptShinglingLeftMin = ptNetRight + CSize(nPenThickness/2, 0);
			if (ptShinglingRight.x + nPenThickness/2 >= ptShinglingRightMax.x)
				ptShinglingRightMax = ptShinglingRight + CSize(nPenThickness/2, 0);

			pDC->SelectObject((bIsCover) ? &lightgrayPen : &whitePen);
			pDC->LineTo(CPoint(cp.x, pt.y));

			nPageSeqIndex+= 2;

			int nPenOffset = (int)((float)nPenThickness / 2.0f + 0.5f);
			arcRect.TopLeft()	  = cp + CSize(-nRadius, nRadius);
			arcRect.BottomRight() = cp + CSize(nRadius, -nRadius);
			CRect  rcArc1 = arcRect, rcArc2 = arcRect; 
			CPoint startPt1, startPt2, endPt1, endPt2;
			if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			{
				rcArc1.left -= nPenOffset; rcArc1.top += nPenOffset; rcArc1.bottom -= nPenOffset; 
				rcArc2.left += nPenOffset; rcArc2.top -= nPenOffset; rcArc2.bottom += nPenOffset;
				startPt  = CPoint(cp.x, arcRect.top); endPt  = CPoint(arcRect.left, cp.y);
				startPt1 = CPoint(cp.x, rcArc1.top);  endPt1 = CPoint(rcArc1.left, cp.y);
				startPt2 = CPoint(cp.x, rcArc2.top);  endPt2 = CPoint(rcArc2.left, cp.y);
				g_nShingling += (int)((float)rProductPart.GetShinglingValueX()*1000.0f + 0.5f);
			}
			else
			{
				rcArc1.left += nPenOffset; rcArc1.top -= nPenOffset; rcArc1.bottom += nPenOffset; 
				rcArc2.left -= nPenOffset; rcArc2.top += nPenOffset; rcArc2.bottom -= nPenOffset;
				startPt = cp + CSize(-nRadius, 0);	endPt   = cp + CSize(0, -nRadius);
				startPt  = CPoint(arcRect.left, cp.y); endPt  = CPoint(cp.x, arcRect.bottom);
				startPt1 = CPoint(rcArc1.left,  cp.y); endPt1 = CPoint(cp.x, rcArc1.bottom);
				startPt2 = CPoint(rcArc2.left,  cp.y); endPt2 = CPoint(cp.x, rcArc2.bottom);
			}
			//if (pDC->RectVisible(arcRect))
				if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)		// if <= 2 we do have no spine
					pDC->Arc(arcRect, startPt, endPt);

			BOOL bHighlight1 = ((nPagePreviewIndex1 == nCurPageIndex1) || (nPagePreviewIndex2 == nCurPageIndex1)) ? TRUE : FALSE;
			BOOL bHighlight2 = ((nPagePreviewIndex1 == nCurPageIndex2) || (nPagePreviewIndex2 == nCurPageIndex2)) ? TRUE : FALSE;
			CPoint ptNetRight1, ptNetRight2;
			if (bShinglingActive)
			{
				ptNetRight1 = CPoint(ptShinglingRight - CSize((int)(fabs(fShinglingValue1) * 1000.0f + 0.5f), -nPenOffset )); ptNetRight1.x = max(ptNetRight1.x, cp.x);
				ptNetRight2 = CPoint(ptShinglingRight - CSize((int)(fabs(fShinglingValue2) * 1000.0f + 0.5f),  nPenOffset )); ptNetRight2.x = max(ptNetRight2.x, cp.x);
	
				pDC->SelectObject(&grayPen);
				pDC->MoveTo(ptShinglingRight.x, ptNetRight1.y);	
				pDC->LineTo(cp.x, ptNetRight1.y);

				pDC->SelectObject((bHighlight1) ? &yellowPen : &lightYellowPen);
				pDC->MoveTo(CPoint(ptNetRight1));	
				pDC->LineTo(CPoint(cp.x, ptNetRight1.y));
				pDC->SelectObject((bHighlight2) ? &yellowPen : &lightYellowPen);
				pDC->MoveTo(CPoint(ptNetRight2));	
				pDC->LineTo(CPoint(cp.x, ptNetRight2.y));
				if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)		// if <= 2 we do have no spine
				{
					pDC->SelectObject((bHighlight1) ? &yellowPen : &lightYellowPen);
					pDC->Arc(rcArc1, startPt1, endPt1);
					pDC->SelectObject((bHighlight2) ? &yellowPen : &lightYellowPen);
					pDC->Arc(rcArc2, startPt2, endPt2);
				}
				if (bHighlight1 || bHighlight2)
				{
					CString string;
					CPrintSheetView::TruncateNumber(string, abs((bHighlight1) ? fShinglingValue1 : fShinglingValue2));
					CPageTemplate::DrawSingleMeasure(pDC, string, CPoint(ptShinglingRight.x + nPenThickness/2, ptShinglingRight.y), LEFT, RIGHT, TRUE);
				}
			}
			else
			{
				ptNetRight1 = CPoint(ptShinglingRight - CSize(0, -nPenOffset));
				ptNetRight2 = CPoint(ptShinglingRight - CSize(0,  nPenOffset));

				pDC->SelectObject(&grayPen);
				pDC->MoveTo(ptShinglingRight.x, ptNetRight1.y);	
				pDC->LineTo(cp.x, ptNetRight1.y);

				pDC->SelectObject((bHighlight1) ? &darkBluePen : &grayPen);
				if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)		// if <= 2 we do have no spine
					pDC->Arc(rcArc1, startPt1, endPt1);
				pDC->MoveTo(CPoint(ptNetRight1));	
				pDC->LineTo(CPoint(cp.x, ptNetRight1.y));
				pDC->SelectObject((bHighlight2) ? &darkBluePen : &grayPen);
				if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)		// if <= 2 we do have no spine
					pDC->Arc(rcArc2, startPt2, endPt2);
				pDC->MoveTo(CPoint(ptNetRight2));	
				pDC->LineTo(CPoint(cp.x, ptNetRight2.y));
			}
			pDC->SelectObject((bIsCover) ? &lightgrayPen : &whitePen);
		}

		if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			nRadius-= nSheetThickness/nPages;
		else
			nRadius+= nSheetThickness/nPages;
	}
/*
	pDC->LPtoDP(&m_sizeFont);
	BOOL bShowText = (m_sizeFont.cy < 8) ? FALSE : TRUE;
	pDC->DPtoLP(&m_sizeFont);

	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor((pDC->IsPrinting()) ? WHITE : LIGHTGRAY);
	pDC->SetTextColor(RED);
	CString strText;
	if (pDoc->m_productionData.m_nProductionType == 1)		// Coming and going
	{
		int nNumFoldSheets = pDoc->m_Bookblock.m_FoldSheetList.GetCount();
		strText.Format(_T(" %d/%d "), rFoldSheet.m_nFoldSheetNumber, nNumFoldSheets - rFoldSheet.m_nFoldSheetNumber + 1); 
	}
	else
		strText.Format(_T(" %d "), rFoldSheet.m_nFoldSheetNumber); 
	pDC->TextOut(rect.left + 50, rect.bottom + (rect.top - rect.bottom)/2 + m_sizeFont.cy/2, strText);

	if (bShowText)
	{
		pDC->SetTextColor((pDC->IsPrinting()) ? DARKGRAY : BLACK);
		strText.Format(_T(" %s - %s "), strMinPage, strMaxPage);
		pDC->TextOut(rect.left + 1000, rect.bottom + (rect.top - rect.bottom)/2 + m_sizeFont.cy/2, strText);
	}
*/
	pDC->SelectObject(pOldPen);

	whitePen.DeleteObject();
	lightgrayPen.DeleteObject();
	yellowPen.DeleteObject();
	lightYellowPen.DeleteObject();
	darkBluePen.DeleteObject();
	grayPen.DeleteObject();
}

int CBookBlockView::GetFoldSheetRadiusLeftPart(POSITION pos, int nProductPartIndex)
{
	CImpManDoc*		 pDoc = GetDocument();
	int				 r	  = 0;
	CDescriptionItem Item;
	
	while (pos)
	{
		Item = pDoc->m_Bookblock.GetNext_DescriptionList(m_nProductIndex, pos);

		if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
			break;
		else
		{
			CFoldSheet& rFoldSheet = Item.GetFoldSheet(&(pDoc->m_Bookblock));
			r+= 2*GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + (rFoldSheet.m_nPagesLeft/2) * GetPaperThickness(rFoldSheet.m_nProductPartIndex); 
		}
	}

	return r;
}

int CBookBlockView::GetFoldSheetRadiusRightPart(POSITION pos, int nProductPartIndex)
{
	CImpManDoc*		 pDoc = GetDocument();
	int				 r	  = 0;
	CDescriptionItem Item;
	
	Item = pDoc->m_Bookblock.m_DescriptionList.GetAt(pos);
	while (pos)
	{
		if (Item.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			break;
		else
		{
			CFoldSheet& rFoldSheet = Item.GetFoldSheet(&(pDoc->m_Bookblock));
			r+= 2*GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + (rFoldSheet.m_nPagesRight/2) * GetPaperThickness(rFoldSheet.m_nProductPartIndex); 
		}

		Item = pDoc->m_Bookblock.GetPrev_DescriptionList(m_nProductIndex, pos);
	}

	return r;
}


/////////////////////////////////////////////////////////////////////////////
// OLE Client support and commands

BOOL CBookBlockView::IsSelected(const CObject* pDocItem) const
{
	// The implementation below is adequate if your selection consists of
	//  only CImpManCntrItem objects.  To handle different selection
	//  mechanisms, the implementation here should be replaced.

	// TODO: implement this function that tests for a selected OLE client item

	return pDocItem == m_pSelection;
}

void CBookBlockView::OnInsertObject()
{
	// Invoke the standard Insert Object dialog box to obtain information
	//  for new CImpManCntrItem object.
	COleInsertDialog dlg;
	if (dlg.DoModal() != IDOK)
		return;

	BeginWaitCursor();

	CImpManCntrItem* pItem = NULL;
	TRY
	{
		// Create new item connected to this document.
		CImpManDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);
		pItem = new CImpManCntrItem(pDoc);
		ASSERT_VALID(pItem);

		// Initialize the item from the dialog data.
		if (!dlg.CreateItem(pItem))
			AfxThrowMemoryException();  // any exception will do
		ASSERT_VALID(pItem);

		// If item created from class list (not from file) then launch
		//  the server to edit the item.
		if (dlg.GetSelectionType() == COleInsertDialog::createNewItem)
			pItem->DoVerb(OLEIVERB_SHOW, this);

		ASSERT_VALID(pItem);

		// As an arbitrary user interface design, this sets the selection
		//  to the last item inserted.

		// TODO: reimplement selection as appropriate for your application

		m_pSelection = pItem;   // set selection to last inserted item
		pDoc->SetModifiedFlag(TRUE);
		pDoc->UpdateAllViews(NULL);
	}
	CATCH(CException, e)
	{
		if (pItem != NULL)
		{
			ASSERT_VALID(pItem);
			pItem->Delete();
		}
		AfxMessageBox(IDP_FAILED_TO_CREATE);
	}
	END_CATCH

	EndWaitCursor();
}

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the container (not the server) causes the deactivation.
void CBookBlockView::OnCancelEditCntr()
{
	// Close any in-place active item on this view.
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != NULL)
	{
		pActiveItem->Close();
	}
	ASSERT(GetDocument()->GetInPlaceActiveItem(this) == NULL);
}

// Special handling of OnSetFocus and OnSize are required for a container
//  when an object is being edited in-place.
void CBookBlockView::OnSetFocus(CWnd* pOldWnd)
{
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != NULL &&
		pActiveItem->GetItemState() == COleClientItem::activeUIState)
	{
		// need to set focus to this item if it is in the same view
		CWnd* pWnd = pActiveItem->GetInPlaceWindow();
		if (pWnd != NULL)
		{
			pWnd->SetFocus();   // don't call the base class
			return;
		}
	}

	CScrollView::OnSetFocus(pOldWnd);
}

void CBookBlockView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);

	CRect  clientRect;
	GetClientRect(&clientRect);
    if ( (GetTotalSize().cx <= clientRect.Width()) && (GetTotalSize().cy <= clientRect.Height()) )
	{
		ZoomToFit();
		Invalidate();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CBookBlockView diagnostics

#ifdef _DEBUG
void CBookBlockView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CBookBlockView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CImpManDoc* CBookBlockView::GetDocument() // non-debug version is inline
{
	if ( ! m_pDocument)
		m_pDocument = CImpManDoc::GetDoc();
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CImpManDoc)));
	return (CImpManDoc*)m_pDocument;
}
#endif //_DEBUG


CSize CBookBlockView::GetBookBlockSize(int nProductPartIndex)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CSize(0, 0);

	pDoc->m_Bookblock.CheckFoldSheetsPlausibility();

	m_nMaxPaperThickness = 10;

	long	 lHeight = 0;
	POSITION pos	 = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem		 = pDoc->m_Bookblock.GetNext_DescriptionList(m_nProductIndex, pos);
		CFoldSheet&		  rFoldSheet = rItem.GetFoldSheet(&(pDoc->m_Bookblock));
		if ( ! rFoldSheet.IsPartOfBoundProduct(nProductPartIndex))
			continue;
		//if (nProductPartIndex != -1)
		//	if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
		//		continue;

		int nPaperThickness = GetPaperThickness(rFoldSheet.m_nProductPartIndex);
		m_nMaxPaperThickness = max(m_nMaxPaperThickness, nPaperThickness);

		if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetLeftPart)
			lHeight+= 2*GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + (rFoldSheet.m_nPagesLeft/2)*nPaperThickness;	
		else
			if (rFoldSheet.m_nPagesLeft + rFoldSheet.m_nPagesRight > 2)
				lHeight+= 2*GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + (rFoldSheet.m_nPagesRight/2)*nPaperThickness;	
			else
				lHeight+= 3*GetFoldSheetGap(rFoldSheet.m_nProductPartIndex) + nPaperThickness;	
	}

	float fWidth = 0;
	CBoundProduct& rBoundProduct = pDoc->GetBoundProduct(m_nProductIndex);
	for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
	{
		CBoundProductPart& rProductPart = rBoundProduct.m_parts[i];
		
		fWidth = max(fWidth, rProductPart.m_fPageWidth);

		int nThisProductPartIndex = rProductPart.GetIndex();
		pDoc->m_Bookblock.SetShinglingValuesX(rProductPart.GetShinglingValueX(), FLT_MAX, nThisProductPartIndex);
		pDoc->m_Bookblock.SetShinglingValuesX(FLT_MAX, rProductPart.GetShinglingValueXFoot(), nThisProductPartIndex);
		pDoc->m_Bookblock.SetShinglingValuesY(rProductPart.GetShinglingValueY(), nThisProductPartIndex);
	}

	CSize size((int)(fWidth * 1000.0f + 0.5f), lHeight);

	float fShinglingValueXTotal, fShinglingValueXFootTotal, fShinglingValueYTotal;
	pDoc->m_PageTemplateList.CalcShinglingOffsets(fShinglingValueXTotal, fShinglingValueXFootTotal, fShinglingValueYTotal, -m_nProductIndex - 1);
	m_nShinglingValueTotal = (int)((float)fShinglingValueXTotal * 1000.0f + 0.5f);//, (int)((float)fShinglingValueFootTotal * 1000.0f + 0.5f));

	size.cx += m_nShinglingValueTotal;

	size.cx = (int)((float)size.cy / (float)m_viewPortSize.cy * (float)m_viewPortSize.cx + 0.5f);
	m_docSize = size;

	if ((size.cx == 0) || (size.cy == 0))
		return CSize(0, 0);
	else
		return size;
}



//////////////////////////////////////////////////////////////////////////////////////////////////
// enable OLE DragDrop functionality - needed in conjunction with display list

int CBookBlockView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if( m_dropTarget.Register( this ) )
		return 0;
	else
		return -1;
}


DROPEFFECT CBookBlockView::OnDragOver(COleDataObject* /*pDataObject*/, DWORD dwKeyState, CPoint /*point*/) 
{
	DROPEFFECT de;

	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		if ((dwKeyState & MK_ALT) == MK_ALT)
			de = DROPEFFECT_MOVE;
		else
			de = DROPEFFECT_MOVE;

	return de;
}



//////////////////////////////////////////////////////////////////////////////////////////////////
// display list message handlers

void CBookBlockView::OnLButtonDown(UINT /*nFlags*/, CPoint point) 
{
//    CSummaryFrame* pParentFrame = (CSummaryFrame*)GetParentFrame();

	if ( (GetKeyState(VK_SHIFT) < 0) || IsHandActive() )
	{
		m_lastHandPos = point;
		return;
	}

	if (IsZoomMode())
		ZoomWindow(point);
	else
	{
		m_DisplayList.OnLButtonDown(point); // pass message to display list

		// if Description item(s) has been selected, select corresponding left/right part(s)
   		CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CBookBlockView, FoldSheet));
		while (pDI)
		{
			GetDocument()->m_Bookblock.SelectFoldSheet(((CDescriptionItem*)pDI->m_pData)->m_nFoldSheetIndex, m_DisplayList);

			pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CBookBlockView, FoldSheet));
		}
	}
}

void CBookBlockView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	if (nFlags & MK_CONTROL)
	{
		SetCursor(theApp.m_CursorMagnifyDecrease);
		SetZoomMode(TRUE);
	}
	else
		if (m_DisplayList.BeginDrag(10))
		{
			SetZoomMode(TRUE);
			SetCursor(theApp.m_CursorMagnifyIncrease);
			ZoomWindow(point, TRUE);
			SetCursor(theApp.m_CursorStandard);
			SetZoomMode(FALSE);
		}
		else
		{
			m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list

			CPoint pt = point;
			ClientToScreen(&pt);
			OnContextMenu(NULL, pt);
		}
}

void CBookBlockView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	if (nFlags & MK_CONTROL)
		if (GetCursor() == theApp.m_CursorMagnifyDecrease)
		{
			ZoomWindow(point);
			SetCursor(theApp.m_CursorStandard);
			SetZoomMode(FALSE);
		}
}

void CBookBlockView::OnMouseMove(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnMouseMove(point);

	if ( (GetKeyState(VK_SHIFT) < 0) || IsHandActive()) 
		if (GetKeyState(VK_LBUTTON) < 0)
		{
			CSize szScroll = CSize(m_lastHandPos - point);
			szScroll.cx *= 4; szScroll.cy *= 4;
			OnScrollBy(szScroll);
			m_lastHandPos = point;
			return;
		}
	
	CScrollView::OnMouseMove(nFlags, point);
}

void CBookBlockView::ZoomIncrease() 
{
	m_nZoomToFitMode = FALSE;
	m_viewPortSize.cy = (int)((float)m_viewPortSize.cy * 1.5f);
	SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));

	GetBookBlockSize(-m_nProductIndex - 1);

	Invalidate();
	UpdateWindow();

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		pDoc->SetModifiedFlag();	// in order to save screen status
}

void CBookBlockView::ZoomDecrease() 
{
	m_nZoomToFitMode = FALSE;
	if ( ! m_zoomSteps.GetCount())
	{
		m_viewPortSize.cy = max((int)((float)m_viewPortSize.cy / 1.5f), 2);
		SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));

		Invalidate();
		UpdateWindow();
	}
	else
	{
		m_nZoomToFitMode = FALSE;
		m_viewPortSize   = m_zoomSteps.GetTail().m_viewportSize;
		CPoint scrollPos = m_zoomSteps.GetTail().m_scrollPos;
		m_zoomSteps.RemoveTail();
		SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));
		Invalidate();
   		ScrollToPosition(scrollPos);
	}

	GetBookBlockSize(-m_nProductIndex - 1);

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		pDoc->SetModifiedFlag();	// in order to save screen status
}

void CBookBlockView::ZoomToFit() 
{
	SetZoomMode(FALSE);
	m_nZoomToFitMode = TRUE;
	CRect  clientRect;
	GetClientRect(&clientRect);
	m_viewPortSize.cx = max(clientRect.Width()  - 2 * m_nViewBorder, 40);
	m_viewPortSize.cy = max(clientRect.Height() - 2 * m_nViewBorder, 40);

	SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));

	GetBookBlockSize(-m_nProductIndex - 1);

	//Invalidate();
	//UpdateWindow();
}

void CBookBlockView::ZoomWindow(const CPoint& rPoint, BOOL bRButton) 
{
	if (GetCursor() == theApp.m_CursorMagnifyDecrease)
	{
		if ( ! m_zoomSteps.GetCount())
		{
			SetZoomMode(FALSE);
			m_nZoomToFitMode = TRUE;
			CRect  clientRect;
			GetClientRect(&clientRect);
			float fRatio = (float)m_docSize.cx / (float)m_docSize.cy;

			m_viewPortSize.cy = clientRect.Height() - 2 * m_nViewBorder;
			SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));
		}
		else
		{
			m_nZoomToFitMode = FALSE;
			m_viewPortSize   = m_zoomSteps.GetTail().m_viewportSize;
			CPoint scrollPos = m_zoomSteps.GetTail().m_scrollPos;
			m_zoomSteps.RemoveTail();
			SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));
			Invalidate();
       		ScrollToPosition(scrollPos);
		}
		GetBookBlockSize(-m_nProductIndex - 1);
	}
	else
	{
		BOOL		   bTracked = FALSE;
	    CRectTracker   tracker;
	    CMyRectTracker rbTracker;
		bTracked = (bRButton) ? rbTracker.TrackRubberBand(this, rPoint, TRUE) : tracker.TrackRubberBand(this, rPoint, TRUE);

		if (bTracked)
		{
			m_nZoomToFitMode = FALSE;

			CRect trackRect = (bRButton) ? rbTracker.m_rect : tracker.m_rect;
			trackRect.NormalizeRect(); 

			CZoomState zoomState(m_viewPortSize, GetScrollPosition());	// save old status
			m_zoomSteps.AddTail(zoomState);

			CPoint ScrollPos = GetScrollPosition() + trackRect.TopLeft();
			CRect  clientRect;
			GetClientRect(&clientRect);
			float factor  = (float)clientRect.Width()/(float)trackRect.Width();
			float fRatioY = (float)((clientRect.Height()) / (float)trackRect.Height());
			m_viewPortSize.cy = (int)((float)m_viewPortSize.cy * fRatioY);
			SetScrollSizes(MM_TEXT, m_viewPortSize + CSize(0, 2 * m_nViewBorder));

			ScrollPos.x = 0;	//(long)((float)ScrollPos.x * factor);
			ScrollPos.y = (long)((float)ScrollPos.y * fRatioY);//factor);
			ScrollToPosition(ScrollPos);
			
			GetBookBlockSize(-m_nProductIndex - 1);
		}
	}

	Invalidate();
}

// Description item has been selected -> so select corresponding left/right part
BOOL CBookBlockView::OnSelectFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM) 
{
	m_bFoldSheetSelected = TRUE;

	GetDocument()->m_Bookblock.SelectFoldSheet(((CDescriptionItem*)pDI->m_pData)->m_nFoldSheetIndex, m_DisplayList);

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(((CDescriptionItem*)pDI->m_pData)->m_nFoldSheetIndex);
	if ( ! pos)
		return FALSE;
	CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);

	CBookBlockDataView* pBookBlockDataView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
	if (pBookBlockDataView)
		pBookBlockDataView->SelectFoldSheet(&rFoldSheet, ((CDescriptionItem*)pDI->m_pData)->m_nFoldSheetPart);

	return TRUE;
}

BOOL CBookBlockView::OnDeselectFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM) 
{
	m_bFoldSheetSelected = FALSE;

	//CImpManDoc*			  pDoc				   = CImpManDoc::GetDoc();
 // 	CBookBlockDetailView* pBookBlockDetailView = (CBookBlockDetailView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDetailView));
 //  	if (! pBookBlockDetailView || ! pDoc)
	//	return;
	//POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(((CDescriptionItem*)pDI->m_pData)->m_nFoldSheetIndex);
	//if ( ! pos)
	//	return;
	//CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);
	//pDI = pBookBlockDetailView->m_DisplayList.GetItemFromData((void*)&rFoldSheet);
	//if ( ! pDI)
	//	return;
	//pDI->m_nState = CDisplayItem::Normal;
	//pBookBlockDetailView->m_DisplayList.InvalidateItem(pDI);

	return TRUE;
}

BOOL CBookBlockView::OnStatusChanged()	// display list status has changed (any item selected / deselected)
{
	//CSummaryFrame* pParentFrame = (CSummaryFrame*)GetParentFrame();
	//if (pParentFrame)
	//	if (pParentFrame->m_pDlgFoldSheetNumeration->m_hWnd)
	//	{
	//		pParentFrame->m_pDlgFoldSheetNumeration->m_nSelection = CDlgNumeration::SelectionMarked;		
	//		pParentFrame->m_pDlgFoldSheetNumeration->UpdateData(FALSE);
	//	}

	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////



BOOL CBookBlockView::OnDrop(COleDataObject* /*pDataObject*/, DROPEFFECT dropEffect, CPoint point) 
{
	if (dropEffect == DROPEFFECT_NONE)
		return FALSE;

	int	nIndexDI = -1;

	// find bookblock insertion index ////////////////////////////////////////
	int			  i	  = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CBookBlockView, FoldSheet));
	while (pDI)
	{
		CRect& rRect = pDI->m_BoundingRect;
		if (rRect.top > point.y)
			if ((rRect.left <= point.x) && (point.x <= rRect.right))
			{
				nIndexDI = i;
				break;
			}
		pDI = m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CBookBlockView, FoldSheet));
		i++;
	}

	BOOL bInsertAfter = FALSE;
	if (nIndexDI == -1)
	{
		pDI = m_DisplayList.GetLastItem(DISPLAY_ITEM_TYPEID(CBookBlockView, FoldSheet));
		CRect& rRect = pDI->m_BoundingRect;
		if ((rRect.left <= point.x) && (point.x <= rRect.right))	// add to end of bookblock
		{
			nIndexDI = m_DisplayList.GetLastItemIndex(DISPLAY_ITEM_TYPEID(CBookBlockView, FoldSheet));
			bInsertAfter = TRUE;
		}
		//else
		//	return FALSE;
	}
	//////////////////////////////////////////////////////////////////////////

	CImpManDoc* pDoc = GetDocument();
	if ( ! pDoc)
		return FALSE;
	
	if (dropEffect == DROPEFFECT_MOVE)
		pDoc->m_Bookblock.CutAndPaste (nIndexDI, bInsertAfter, m_DisplayList);
	else
		pDoc->m_Bookblock.CopyAndPaste(nIndexDI, bInsertAfter, m_DisplayList, m_nProductIndex);

	m_DisplayList.DeselectAll();

	pDoc->m_Bookblock.Reorganize(NULL, NULL);//, nProductPartIndex);

	Invalidate();
	UpdateWindow();	// Update this window separately, to prevent window from rescale

	pDoc->SetModifiedFlag(TRUE);

	pDoc->UpdateAllViews(NULL);	

	CBookBlockDataView* pView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
	if (pView)
		if (pView->m_dlgShinglingAssistent.m_hWnd)
		{
			pView->m_dlgShinglingAssistent.InitShinglingValuesXTotal();
			pView->m_dlgShinglingAssistent.InitShinglingValuesYTotal();
		}

	return TRUE;
}


void CBookBlockView::OnKeyDown(UINT nChar, UINT /*nRepCnt*/, UINT /*nFlags*/) 
{
	switch (nChar)
	{
	case VK_DELETE: 
		OnDeleteSelectedFoldsheet();
		break;
	case VK_HOME: 
		OnVScroll(SB_TOP, 0, NULL);
		OnHScroll(SB_LEFT, 0, NULL);
		break;
	case VK_END: 
		OnVScroll(SB_BOTTOM, 0, NULL);
		OnHScroll(SB_RIGHT, 0, NULL);
		break;
	case VK_UP: 
		OnVScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_DOWN: 
		OnVScroll(SB_LINEDOWN, 0, NULL);
		break;
	case VK_PRIOR: 
		OnVScroll(SB_PAGEUP, 0, NULL);
		break;
	case VK_NEXT: 
		OnVScroll(SB_PAGEDOWN, 0, NULL);
		break;
	case VK_LEFT: 
		OnHScroll(SB_LINELEFT, 0, NULL);
		break;
	case VK_RIGHT: 
		OnHScroll(SB_LINERIGHT, 0, NULL);
		break;
	case VK_CONTROL:
		if (IsZoomMode())
			if (GetCursor() == theApp.m_CursorMagnifyIncrease)
				SetCursor(theApp.m_CursorMagnifyDecrease);
		break;
	case VK_SHIFT:
		{
			CRect clientRect;
			GetClientRect(&clientRect);
			if ( (GetTotalSize().cx > clientRect.Width()) || (GetTotalSize().cy > clientRect.Height()) )
				SetCursor(theApp.m_CursorHand);
		}
		break;
	default: 
		break;
	}
}

void CBookBlockView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if (nChar == VK_CONTROL)
	{
		if (IsZoomMode())
			if (GetCursor() == theApp.m_CursorMagnifyDecrease)
				SetCursor(theApp.m_CursorMagnifyIncrease);
	}
	if (nChar == VK_SHIFT)
		SetCursor(theApp.m_CursorStandard);
	
	CScrollView::OnKeyUp(nChar, nRepCnt, nFlags);
}

DROPEFFECT CBookBlockView::OnDragScroll(DWORD dwKeyState, CPoint point)
{
	return CView::OnDragScroll(dwKeyState, point);
}

void CBookBlockView::OnContextMenu(CWnd*, CPoint point)
{
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_BOOKBLOCKVIEW_POPUP));

	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);

	CWnd* pWndPopupOwner = this;
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y,
		pWndPopupOwner);
}

BOOL CBookBlockView::PreTranslateMessage(MSG* pMsg)
{
	// CG: This block was added by the Pop-up Menu component
	{
		// Shift+F10: show pop-up menu.
		if ((((pMsg->message == WM_KEYDOWN || pMsg->message == WM_SYSKEYDOWN) && // If we hit a key and
			(pMsg->wParam == VK_F10) && (GetKeyState(VK_SHIFT) & ~1)) != 0) ||	// it's Shift+F10 OR
			(pMsg->message == WM_CONTEXTMENU))									// Natural keyboard key
		{
			CRect rect;
			GetClientRect(rect);
			ClientToScreen(rect);

			CPoint point = rect.TopLeft();
			point.Offset(5, 5);
			OnContextMenu(NULL, point);

			return TRUE;
		}
	}

	return CScrollView::PreTranslateMessage(pMsg);
}


BOOL CBookBlockView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
//	CSummaryFrame* pParentFrame = (CSummaryFrame*)GetParentFrame();
//	if (pParentFrame)
	{
		if (GetAsyncKeyState(VK_SHIFT))
			return CScrollView::OnSetCursor(pWnd, nHitTest, message);

		if (IsZoomMode())	
			if (GetKeyState(VK_CONTROL) < 0)
   				SetCursor(theApp.m_CursorMagnifyDecrease);
			else
				SetCursor(theApp.m_CursorMagnifyIncrease);
		else
			if (IsHandActive())
				SetCursor(theApp.m_CursorHand);
			else
				SetCursor(theApp.m_CursorStandard);
		}

	return CScrollView::OnSetCursor(pWnd, nHitTest, message);
}


BOOL CBookBlockView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	CBrush* pBkgrdBrush = new CBrush;
	pBkgrdBrush->CreateSolidBrush(RGB(127,162,207));

	lpszClassName = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)*pBkgrdBrush, 0);

	BOOL ret = CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	pBkgrdBrush->Detach();
	delete pBkgrdBrush;

	return ret;
}

void CBookBlockView::OnDeleteSelectedFoldsheet() 
{
	CImpManDoc* pDoc;

	if (AfxMessageBox(IDS_DELETE_FOLDSHEET_WARNING, MB_YESNO) == IDNO)
		return;
	pDoc = GetDocument();
	pDoc->m_Bookblock.RemoveSelectedFoldSheets(m_DisplayList);
	pDoc->m_Bookblock.Reorganize(NULL, NULL, -m_nProductIndex - 1, TRUE);
	pDoc->SetModifiedFlag(TRUE);
	pDoc->UpdateAllViews(NULL);	

	CBookBlockDataView* pView = (CBookBlockDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockDataView));
	if (pView)
		if (pView->m_dlgShinglingAssistent.m_hWnd)
		{
			pView->m_dlgShinglingAssistent.InitShinglingValuesXTotal();
			pView->m_dlgShinglingAssistent.InitShinglingValuesYTotal();
		}

	m_bFoldSheetSelected = FALSE;
}

void CBookBlockView::OnUpdateDeleteSelectedFoldsheet(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bFoldSheetSelected);
}

void CBookBlockView::OnFilePrint() 
{
	CView::OnFilePrint();	
}

void CBookBlockView::OnFilePrintPreview() 
{
	CView::OnFilePrintPreview();	
}

void CBookBlockView::OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{
	CView::OnNcCalcSize(bCalcValidRects, lpncsp);

	//if (bCalcValidRects)
	//	lpncsp->rgrc[0].top += 15;		
}

//
//void CBookBlockView::OnNcPaint()
//{
//	CView::OnNcPaint();
//
//	CDC* pDC = GetWindowDC();
//
//	//CFont mediumFont;
//	//mediumFont.CreateFont(16, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, "Arial");
//	//CFont* pOldFont = pDC->SelectObject(&mediumFont);
//	//CString strOut;
//	//strOut.LoadString(IDS_BOOKBLOCK);
//	//int nMaxRight = pDC->GetDeviceCaps(HORZRES);
//	//CRect headerRect(CPoint(0, 0), CSize(nMaxRight, 20));
//	//headerRect.NormalizeRect();
//	//pDC->FillSolidRect(headerRect, LIGHTGRAY);
//	//pDC->SetBkMode(TRANSPARENT);
//	//pDC->SetTextColor(DARKBLUE);
//	//pDC->DrawText(strOut, CRect(10, headerRect.top, headerRect.right, headerRect.bottom), DT_LEFT | DT_VCENTER | DT_SINGLELINE);
//	//int   nXIcon = 10 + pDC->GetTextExtent(strOut).cx + 5;
//	//CRect rect(nXIcon, headerRect.top - 6, nXIcon + 32, headerRect.top + 25);
//	//pDC->DrawIcon(rect.left, rect.top, theApp.LoadIcon(IDI_GOTO));
//	//m_rectGotoImpositionScheme = CRect(nXIcon, -20, nXIcon + 32, 0);
//
//	//pDC->SelectObject(pOldFont);
//	//mediumFont.DeleteObject();
//
//
//	CString strText; strText.LoadString(IDS_BOOKBLOCK);
//
//	CRect rcFrame;
//	GetClientRect(rcFrame); rcFrame.bottom = rcFrame.top + 15;
//	CPen pen(PS_SOLID, 1, LIGHTGRAY);
//	CBrush brush(RGB(193,211,251));
//	pDC->SelectObject(&pen);
//	pDC->SelectObject(&brush);
//	pDC->RoundRect(rcFrame, CPoint(6,6));
//	pen.DeleteObject();
//	brush.DeleteObject();
//
//	CFont font;
//	font.CreateFont (13, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
//	pDC->SelectObject(&font);
//	pDC->SetBkMode(TRANSPARENT);
//	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
//	pDC->DrawText(strText, rcFrame, DT_CENTER | DT_TOP);
//
//	font.DeleteObject();
//
//	ReleaseDC(pDC);
//}

void CBookBlockView::OnNcMouseMove(UINT nHitTest, CPoint point)
{
	if (nHitTest == HTCAPTION)
	{
		CRect rectScreen(m_rectGotoImpositionScheme);
		ClientToScreen(rectScreen);
		if (rectScreen.PtInRect(point))
			SetCursor(theApp.LoadCursor(IDC_POINTING_HAND)); 
	}
	else
		CView::OnNcMouseMove(nHitTest, point);
}

void CBookBlockView::OnNcLButtonDown(UINT nHitTest, CPoint point)
{
	if (nHitTest == HTCAPTION)
	{
		CRect rectScreen(m_rectGotoImpositionScheme);
		ClientToScreen(rectScreen);
		if (rectScreen.PtInRect(point))
		{
			//if ((CSummaryFrame*)GetParentFrame())
			//	((CSummaryFrame*)GetParentFrame())->OnImpose();
		}
	}
	else
		CView::OnNcLButtonDown(nHitTest, point);
}

LRESULT CBookBlockView::OnNcHitTest(CPoint point)
{
	UINT nHitCode = CView::OnNcHitTest(point);
	if (nHitCode == HTNOWHERE)
	{
		CPoint pointClient(point);
		ScreenToClient(&pointClient);
		if ( (pointClient.y < 0) && (pointClient.y >= -20) )
			return HTCAPTION;
	}
	return nHitCode;
}

BOOL CBookBlockView::OnScroll(UINT nScrollCode, UINT nPos, BOOL bDoScroll) 
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if (pDoc)
		pDoc->SetModifiedFlag();	// in order to save screen status
	
	return CScrollView::OnScroll(nScrollCode, nPos, bDoScroll);
}

BOOL CBookBlockView::IsZoomMode()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CBookblockFrame)))
		return ( ((CBookblockFrame*)pFrame)->m_bZoomMode);
	return FALSE;
}

void CBookBlockView::SetZoomMode(BOOL bMode)
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CBookblockFrame)))
		((CBookblockFrame*)pFrame)->m_bZoomMode = bMode;
}

BOOL CBookBlockView::IsHandActive()
{
	CFrameWnd* pFrame = GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	if (pFrame->IsKindOf(RUNTIME_CLASS(CBookblockFrame)))
		return ((CBookblockFrame*)pFrame)->m_bHandActive;
	return FALSE;
}

BOOL CBookBlockView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);

	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}
