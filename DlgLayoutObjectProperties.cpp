// DlgLayoutObjectProperties.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetMarksView.h"
#include "PageListView.h"
#include "DlgMarkSet.h"
#include "ProdDataPrintView.h"
#include "PrintSheetObjectPreview.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgLayoutObjectProperties 


CDlgLayoutObjectProperties::CDlgLayoutObjectProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgLayoutObjectProperties::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgLayoutObjectProperties)
	m_strObjectName = _T("");
	m_strMarkType = _T("");
	//}}AFX_DATA_INIT

	m_pLayoutObject  = NULL;
	m_pTemplate		 = NULL;
	m_pPrintSheet	 = NULL;
	m_pMark			 = NULL;
	m_nMarkType		 = -1;
	m_pColorDefTable = NULL;
	m_nInitialPage	 = 0;
	m_sizeViewport	 = CSize(1,1);
	m_bModeModify	 = FALSE;
}

CDlgLayoutObjectProperties::~CDlgLayoutObjectProperties()
{
	DestroyWindow();
}

HBRUSH CDlgLayoutObjectProperties::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
}

BOOL CDlgLayoutObjectProperties::Create(CLayoutObject* pLayoutObject, CMark* pMark, CPrintSheet* pPrintSheet, CWnd* pParent, int nInitialPage, BOOL bModeModify)
{
	m_pLayoutObject	= pLayoutObject;
	m_pMark			= pMark;
	m_pPrintSheet	= pPrintSheet;
	m_pTemplate		= (m_pLayoutObject) ? m_pLayoutObject->GetCurrentObjectTemplate(pPrintSheet) : NULL;
	m_strObjectName = (m_pTemplate) ? m_pTemplate->m_strPageID : "";
	m_nInitialPage  = nInitialPage;
	m_bModeModify	= bModeModify;

	if (m_bModeModify)
	{
		CLayout* pLayout = (m_pPrintSheet) ? m_pPrintSheet->GetLayout() : NULL;
		if (pLayout && pMark)
		{
			pLayout->m_FrontSide.SetMarkVisible(pMark->m_strMarkName, FALSE);
			pLayout->m_BackSide.SetMarkVisible(pMark->m_strMarkName, FALSE);
		}
	}
	else
	{
		CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
		if (pView)
			pView->EnableWindow(FALSE);
	}

	int nRet = CDialog::Create(IDD_LAYOUTOBJECT_PROPERTIES,	pParent);
	return nRet;
}

BOOL CDlgLayoutObjectProperties::Create(CMark* pMark, CPrintSheet* pPrintSheet, CWnd* pParent, int nInitialPage, BOOL bModeModify)
{
	if (! pMark)
		return FALSE;

	m_pLayoutObject	= NULL;
	m_pMark			= pMark;
	m_pPrintSheet	= pPrintSheet;
	m_pTemplate		= (m_pMark) ? &m_pMark->m_markTemplate : NULL;
	m_strObjectName = (m_pTemplate) ? m_pTemplate->m_strPageID : "";
	m_nInitialPage  = nInitialPage;
	m_bModeModify   = bModeModify;

	if (m_bModeModify)
	{
		CLayout* pLayout = (m_pPrintSheet) ? m_pPrintSheet->GetLayout() : NULL;
		if (pLayout && pMark)
		{
			pLayout->m_FrontSide.SetMarkVisible(pMark->m_strMarkName, FALSE);
			pLayout->m_BackSide.SetMarkVisible(pMark->m_strMarkName, FALSE);
		}
	}
	else
	{
		CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
		if (pView)
			pView->EnableWindow(FALSE);
	}
	
	int nRet = CDialog::Create(IDD_LAYOUTOBJECT_PROPERTIES,	pParent);
//	m_dlgLayoutObjectControl.ObjectPreviewWindow_LoadData();
	return nRet;
}

void CDlgLayoutObjectProperties::ReinitDialog(CLayoutObject* pNewLayoutObject, CPrintSheet* pNewPrintSheet)
{
	m_pLayoutObject	= pNewLayoutObject;
	m_pMark			= NULL;
	m_pPrintSheet	= pNewPrintSheet;
	m_pTemplate		= (m_pLayoutObject) ? m_pLayoutObject->GetCurrentObjectTemplate(pNewPrintSheet) : NULL;
	m_strObjectName = (m_pTemplate) ? m_pTemplate->m_strPageID : "";

	if (m_hWnd)
		GetDlgItem(IDC_ASSIGN_MARK)->ShowWindow(SW_HIDE);

	m_dlgLayoutObjectControl.ReinitDialog(pNewLayoutObject, pNewPrintSheet);
	//m_dlgObjectPreviewControl.ReinitDialog(pNewLayoutObject, pNewPrintSheet);
	UpdateData(FALSE);
	InitObjectType();
	SetTitle();
}

void CDlgLayoutObjectProperties::ReinitDialog(CMark* pNewMark, CPrintSheet* pNewPrintSheet)
{
	m_pLayoutObject	= NULL;
	m_pMark			= pNewMark;
	m_pPrintSheet	= pNewPrintSheet;
	m_pTemplate		= (m_pMark) ? &m_pMark->m_markTemplate : NULL;
	m_strObjectName = (m_pTemplate) ? m_pTemplate->m_strPageID : "";

	if (m_hWnd)
		GetDlgItem(IDC_ASSIGN_MARK)->ShowWindow((m_bModeModify) ? SW_HIDE : SW_SHOW);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	CColorDefinitionTable* pColorDefTable = ( ! m_bModeModify) ? &theApp.m_colorDefTable : ( (pDoc) ? &pDoc->m_ColorDefinitionTable : &theApp.m_colorDefTable);
	m_dlgLayoutObjectControl.ReinitDialog(pNewMark, pNewPrintSheet, pColorDefTable);
	//m_dlgObjectPreviewControl.ReinitDialog(pNewMark, pNewPrintSheet, &theApp.m_colorDefTable);
	UpdateData(FALSE);
	InitObjectType();
	SetTitle();
}

void CDlgLayoutObjectProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgLayoutObjectProperties)
	DDX_Text(pDX, IDC_LAYOUTOBJECT_NAME, m_strObjectName);
	DDX_Text(pDX, IDC_MARKTYPE, m_strMarkType);
	//}}AFX_DATA_MAP
}

void CDlgLayoutObjectProperties::SetTitle(BOOL bInitial)
{
	if (bInitial)
		GetWindowText(m_strInitialTitle);

	CString strTitle;
	if (m_dlgLayoutObjectControl.GetObjectType() == CDlgLayoutObjectControl::PageObject)
		strTitle.Format(_T("%s %s '%s'"), m_strInitialTitle, m_strMarkType, m_strObjectName);
	else
		strTitle.Format(_T("%s '%s'"), m_strInitialTitle, m_strObjectName);

	SetWindowText(strTitle);
}


BEGIN_MESSAGE_MAP(CDlgLayoutObjectProperties, CDialog)
	//{{AFX_MSG_MAP(CDlgLayoutObjectProperties)
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()
	ON_EN_CHANGE(IDC_LAYOUTOBJECT_NAME, OnChangeLayoutobjectName)
	ON_BN_CLICKED(IDC_ASSIGN_MARK, OnAssignMark)
	//}}AFX_MSG_MAP
	ON_WM_ACTIVATE()
	ON_COMMAND(ID_UPDATE_SUBDIALOGS, OnUpdateSubDialogs)
	ON_COMMAND(ID_UPDATE_MARKNAME, OnUpdateMarkname)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgLayoutObjectProperties 

BOOL CDlgLayoutObjectProperties::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CDisplayItem* pDI = NULL;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		if (pDI)
		{
			pView->m_DisplayList.DeselectAll();
			pView->m_DisplayList.SelectItem(*pDI);		// properties works only for one object
		}
	}

	m_dlgLayoutObjectControl.m_bLockUpdateView = TRUE;

	if (m_pLayoutObject && ! m_pMark)
	{
		GetDlgItem(IDC_ASSIGN_MARK)->ShowWindow(SW_HIDE);
		m_dlgLayoutObjectControl.Initialize(IDC_LAYOUTOBJECT_CONTROLFRAME, m_pLayoutObject, m_pPrintSheet, this);
	}
	else
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		GetDlgItem(IDC_ASSIGN_MARK)->ShowWindow((m_bModeModify) ? SW_HIDE : SW_SHOW);
		CColorDefinitionTable* pColorDefTable = ( ! m_bModeModify) ? &theApp.m_colorDefTable : ( (pDoc) ? &pDoc->m_ColorDefinitionTable : &theApp.m_colorDefTable);
		m_dlgLayoutObjectControl.Initialize(IDC_LAYOUTOBJECT_CONTROLFRAME, m_pMark, m_pPrintSheet, pColorDefTable, this);
	}

	theApp.RestoreWindowPlacement(this, (m_pMark) ? _T("CDlgLayoutObjectProperties_NewMark") : _T("CDlgLayoutObjectProperties"));

	InitObjectType();

	SetTitle(TRUE);

	UpdateData(FALSE);

	m_dlgLayoutObjectControl.m_bLockUpdateView = FALSE;

	m_dlgLayoutObjectControl.UpdateView();	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgLayoutObjectProperties::OnDestroy() 
{
	if (m_bModeModify)
	{
		CLayout* pLayout = (m_pPrintSheet) ? m_pPrintSheet->GetLayout() : NULL;
		if (pLayout)
		{
			pLayout->m_FrontSide.SetMarkVisible((m_pMark) ? m_pMark->m_strMarkName : _T(""), TRUE);
			pLayout->m_BackSide.SetMarkVisible((m_pMark) ? m_pMark->m_strMarkName : _T(""), TRUE);
		}
	}

	theApp.SaveWindowPlacement(this, (m_pMark) ? _T("CDlgLayoutObjectProperties_NewMark") : _T("CDlgLayoutObjectProperties"));

	CDialog::OnDestroy();
}

void CDlgLayoutObjectProperties::PostNcDestroy()
{
	if ( ! m_bModeModify)
	{
		CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
		if (pView)
			pView->EnableWindow(TRUE);
	}

	CDialog::PostNcDestroy();
}

void CDlgLayoutObjectProperties::OnActivate(UINT nState, CWnd* /*pWndOther*/, BOOL /*bMinimized*/ )
{
	if ( (nState == WA_ACTIVE) || (nState == WA_CLICKACTIVE) )
		m_dlgLayoutObjectControl.ObjectPreviewWindow_LoadData();//theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CDlgLayoutObjectProperties::OnUpdateSubDialogs()
{
	if (m_pLayoutObject)
	{
		m_dlgLayoutObjectControl.ReinitDialog(GetFirstLayoutObject(), GetPrintSheet());
	}
	else
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		CColorDefinitionTable* pColorDefTable = ( ! m_bModeModify) ? &theApp.m_colorDefTable : ( (pDoc) ? &pDoc->m_ColorDefinitionTable : &theApp.m_colorDefTable);
		m_dlgLayoutObjectControl.ReinitDialog(GetFirstMark(), GetPrintSheet(), pColorDefTable);
	}
}

void CDlgLayoutObjectProperties::OnUpdateMarkname()
{
	if (m_pMark)
	{
		CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
		if (pView)
		{
			m_pTemplate->m_strPageID = pView->FindFreeMarkName(m_pTemplate->m_strPageID);
			m_pMark->m_strMarkName = m_pTemplate->m_strPageID;
		}
	}
	m_strObjectName = m_pTemplate->m_strPageID;
	UpdateData(FALSE);
}

void CDlgLayoutObjectProperties::InitObjectType() 
{
	BOOL bIsFlatProduct = (m_pLayoutObject) ? m_pLayoutObject->IsFlatProduct() : FALSE;
	BOOL bUnassigned = (m_bModeModify) ? FALSE : TRUE;
	switch (m_dlgLayoutObjectControl.GetObjectType())
	{
	case CDlgLayoutObjectControl::PageObject:	SetIcon(theApp.LoadIcon(IDI_SHEET_PAGE),	TRUE);	m_strMarkType.LoadString(( ! bIsFlatProduct) ? IDS_OBJECTTYPE_PAGE : IDS_LABELTYPE);	break;
	case CMark::CustomMark:						SetIcon(theApp.LoadIcon((bUnassigned) ? IDI_CUSTOM_MARK_UNASSIGNED	: IDI_CUSTOM_MARK),		TRUE);	m_strMarkType.LoadString(IDS_MARKTYPE_CUSTOM_PDF);	break;
	case CMark::TextMark:						SetIcon(theApp.LoadIcon((bUnassigned) ? IDI_TEXT_MARK_UNASSIGNED	: IDI_TEXT_MARK),		TRUE);	m_strMarkType.LoadString(IDS_MARKTYPE_TEXT);		break;
	case CMark::SectionMark:					SetIcon(theApp.LoadIcon((bUnassigned) ? IDI_SECTION_MARK_UNASSIGNED	: IDI_SECTION_MARK),	TRUE);	m_strMarkType.LoadString(IDS_MARKTYPE_SECTION);		break;
	case CMark::CropMark:						SetIcon(theApp.LoadIcon((bUnassigned) ? IDI_CROP_MARK_UNASSIGNED	: IDI_CROP_MARK),		TRUE);	m_strMarkType.LoadString(IDS_MARKTYPE_CROP);		break;
	case CMark::FoldMark:						SetIcon(theApp.LoadIcon((bUnassigned) ? IDI_FOLD_MARK_UNASSIGNED	: IDI_FOLD_MARK),		TRUE);	m_strMarkType.LoadString(IDS_MARKTYPE_FOLD);		break;
	case CMark::BarcodeMark:					SetIcon(theApp.LoadIcon((bUnassigned) ? IDI_BARCODE_MARK_UNASSIGNED	: IDI_BARCODE_MARK),	TRUE);	m_strMarkType.LoadString(IDS_MARKTYPE_BARCODE);		break;
	case CMark::DrawingMark:					SetIcon(theApp.LoadIcon((bUnassigned) ? IDI_DRAWING_MARK_UNASSIGNED	: IDI_DRAWING_MARK),	TRUE);	m_strMarkType.LoadString(IDS_MARKTYPE_DRAWING);		break;
	default:									SetIcon(NULL, TRUE);																				m_strMarkType = "";									break;
	}

	UpdateData(FALSE);
}

BOOL CDlgLayoutObjectProperties::IsActiveReflineControl()
{
	return m_dlgLayoutObjectControl.IsActiveReflineControl();
}

void CDlgLayoutObjectProperties::DataExchangeSave()
{
	if (m_pPrintSheet)
		if (m_pPrintSheet->GetLayout())
		{
			CLayout* pLayout = m_pPrintSheet->GetLayout();
			if (m_bModeModify && m_pMark)
			{
				pLayout->m_FrontSide.SetMarkVisible(m_pMark->m_strMarkName, TRUE);
				pLayout->m_BackSide.SetMarkVisible(m_pMark->m_strMarkName, TRUE);
			}
		}

	m_dlgLayoutObjectControl.DataExchangeSave();
}

void CDlgLayoutObjectProperties::OnApplyButton() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	Apply();

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
}

void CDlgLayoutObjectProperties::Apply(BOOL bUpdateView) 
{
	CString strOldName = (m_pMark) ? m_pMark->m_strMarkName : ( (m_pLayoutObject) ? m_pLayoutObject->m_strFormatName : _T(""));

	UpdateData(TRUE);

	if (strOldName != m_strObjectName)
	{
		if (m_pTemplate)
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			if (pDoc && m_bModeModify)
			{
				CPageTemplate* pMarkTemplate = pDoc->m_MarkTemplateList.FindTemplate(strOldName);
				if (pMarkTemplate)
					pMarkTemplate->m_strPageID = m_strObjectName;
			}
			m_pTemplate->m_strPageID = m_strObjectName;
		}
		if (m_pLayoutObject)
		{
			if (m_pLayoutObject->m_nType == CLayoutObject::ControlMark)
				m_pLayoutObject->m_strFormatName = m_strObjectName;
		}
		if (m_bModeModify)
		{
			CLayout* pLayout = (m_pPrintSheet) ? m_pPrintSheet->GetLayout() : NULL;
			if (pLayout)
			{
				CLayoutObject* pLayoutObject = pLayout->m_FrontSide.FindFirstLayoutObject(strOldName, CLayoutObject::ControlMark);
				while (pLayoutObject)
				{
					pLayoutObject->m_strFormatName = m_strObjectName;
					pLayoutObject = pLayout->m_FrontSide.FindNextLayoutObject(pLayoutObject, strOldName, CLayoutObject::ControlMark);
				}
				pLayoutObject = pLayout->m_BackSide.FindFirstLayoutObject(strOldName, CLayoutObject::ControlMark);
				while (pLayoutObject)
				{
					pLayoutObject->m_strFormatName = m_strObjectName;
					pLayoutObject = pLayout->m_BackSide.FindNextLayoutObject(pLayoutObject, strOldName, CLayoutObject::ControlMark);
				}
			}
		}

		if (m_pMark)
			m_pMark->m_strMarkName = m_strObjectName;
	}

	m_dlgLayoutObjectControl.Apply();
	//m_dlgObjectPreviewControl.Apply();

	CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pProdDataPrintView)
	{
		pProdDataPrintView->InitObjectList();
		if (pProdDataPrintView->IsSelected(m_pLayoutObject))
			if (pProdDataPrintView->GetDlgLayoutObjectProperties()->m_hWnd)
			{
				pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.DataExchangeLoad();
				//pView->m_dlgObjectPreviewControl.PrepareUpdatePreview();
			}
	}

	if (m_pPrintSheet)
		if (m_pPrintSheet->GetLayout())
		{
			CLayout* pLayout = m_pPrintSheet->GetLayout();
			if (m_bModeModify)
			{
				CString strMarkName = (m_pMark) ? m_pMark->m_strMarkName : _T("");
				pLayout->AnalyzeMarks(FALSE, m_pMark);
				m_pMark = pLayout->m_markList.FindMark(strMarkName);	// because mark pointer is changed in AnalyzeMarks()
				m_dlgLayoutObjectControl.m_pMark = m_pMark;
				m_pTemplate	= (m_pMark) ? &m_pMark->m_markTemplate : NULL;
				if (pProdDataPrintView)
					pProdDataPrintView->InitObjectList();
			}
			else
				pLayout->AnalyzeMarks();
			if (m_bModeModify && m_pMark)
			{
				pLayout->m_FrontSide.SetMarkVisible(m_pMark->m_strMarkName, FALSE);
				pLayout->m_BackSide.SetMarkVisible(m_pMark->m_strMarkName, FALSE);
			}
		}

	if (bUpdateView)
		m_dlgLayoutObjectControl.UpdateView();
}

void CDlgLayoutObjectProperties::OnOK() 
{
	Apply(FALSE);

	if ( ! m_bModeModify)
	{
		CLayout* pLayout = (m_pPrintSheet) ? m_pPrintSheet->GetLayout() : NULL;
		if (pLayout && m_pMark)
		{
			if ( ! pLayout->m_markList.FindMark(m_pMark->m_strMarkName))
			{
				if (AfxMessageBox(IDS_PDFLIB_OUTPUT_MARKS, MB_YESNO) == IDYES)
				{
					OnAssignMark();
					m_dlgLayoutObjectControl.UpdateView();
					return;
				}
			}
		}
	}

	DestroyWindow();
	m_dlgLayoutObjectControl.UpdateView();
}

void CDlgLayoutObjectProperties::OnCancel() 
{
	if ( ! m_bModeModify)
	{
		CLayout* pLayout = (m_pPrintSheet) ? m_pPrintSheet->GetLayout() : NULL;
		if (pLayout && m_pMark)
		{
			if ( ! pLayout->m_markList.FindMark(m_pMark->m_strMarkName))
			{
				if (AfxMessageBox(IDS_PDFLIB_OUTPUT_MARKS, MB_YESNO) == IDYES)
				{
					OnAssignMark();
					m_dlgLayoutObjectControl.UpdateView();
					return;
				}
			}
		}
	}

	DestroyWindow();
	m_dlgLayoutObjectControl.UpdateView();
}

CLayoutObject* CDlgLayoutObjectProperties::GetFirstLayoutObject()
{
	if (this == NULL)
		return NULL;

	return m_pLayoutObject;
}

CLayoutObject* CDlgLayoutObjectProperties::GetNextLayoutObject(CLayoutObject* /*pLayoutObject*/)
{
	if (this == NULL)
		return NULL;

	return NULL;
}

CMark* CDlgLayoutObjectProperties::GetFirstMark()
{
	if (this == NULL)
		return NULL;

	return m_pMark;
}

CMark* CDlgLayoutObjectProperties::GetNextMark(CMark* /*pMark*/)
{
	if (this == NULL)
		return NULL;

	return NULL;
}

CPrintSheet* CDlgLayoutObjectProperties::GetPrintSheet()
{
	if (this == NULL)
		return NULL;

	return m_pPrintSheet;
}

void CDlgLayoutObjectProperties::OnChangeLayoutobjectName() 
{
	UpdateData(TRUE);

	SetTitle();
}

void CDlgLayoutObjectProperties::OnAssignMark() 
{
	OnApplyButton();
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (m_pMark)
	{
		if ( (m_pMark->m_nType == CMark::TextMark) || (m_pMark->m_nType == CMark::BarcodeMark) )
		{
			CString strLayout;
			if ( ! CDlgMarkSet::CheckTextmarkPlausibility(m_pMark, strLayout))
			{
				CString strMsg;
				strMsg.Format(IDS_TEXTMARK_UNPLAUSIBLE, strLayout);
				AfxMessageBox(strMsg, MB_OK);

				if (m_dlgLayoutObjectControl.m_dlgTextMarkContent.m_hWnd)
					if (m_dlgLayoutObjectControl.m_dlgTextMarkContent.GetDlgItem(IDC_TEXTMARKSTRING))
					{
						m_dlgLayoutObjectControl.m_dlgTextMarkContent.GetDlgItem(IDC_TEXTMARKSTRING)->SetFocus();
						((CEdit*)m_dlgLayoutObjectControl.m_dlgTextMarkContent.GetDlgItem(IDC_TEXTMARKSTRING))->SetSel(0, -1);
					}

				m_dlgLayoutObjectControl.UpdateView();
				return;
			}
		}

		if ( ! CDlgMarkSet::CheckMarkWidthHeight(m_pMark))
		{
			AfxMessageBox(IDS_MARK_DIMS_UNPLAUSIBLE, MB_OK);
			m_dlgLayoutObjectControl.UpdateView();
			return;
		}

		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			pView->UndoSave();

		if (m_pMark->IsEnabled(m_pPrintSheet, BOTHSIDES))
		{
			if (CDlgMarkSet::PositionAndCreateMarkAuto(m_pMark, NULL))
			{
				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if (pDoc)
				{
					pDoc->SetModifiedFlag();	// DoAutoMarks is needed, because otherwise new mark will be added at the end of the auto marks list
					pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();	// in order to ClipToObjects() for Crop and FoldMarks regarding to CustomMarks
					//pDoc->m_MarkTemplateList.FindPrintSheetLocations();
					pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
					//pDoc->m_PrintSheetList.ReorganizeColorInfos();
				}

				DestroyWindow();
				if ( ! pView)
					m_dlgLayoutObjectControl.UpdateView();
				else
				{
					pView->Invalidate();
					pView->UpdateWindow();
				}

   				CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
				if (pProdDataPrintView)
				{
					pProdDataPrintView->InitObjectList(m_pMark);
					pProdDataPrintView->UpdateLayoutMarkObjects();
				}
				theApp.UpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));
			}
		}
	}
	//m_dlgLayoutObjectControl.UpdateView();
}
