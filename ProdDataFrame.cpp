// ProdDataFrame.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "ProdDataFrame.h"
#include "ProdDataGraphicOverView.h"
#include "ProdDataProcessOverView.h"
#include "ProdDataHeaderView.h"



IMPLEMENT_DYNAMIC(CProdDataSplitter, CSplitterWnd)

BEGIN_MESSAGE_MAP(CProdDataSplitter, CSplitterWnd)
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()

// CProdDataSplitter message handlers
afx_msg LRESULT CProdDataSplitter::OnNcHitTest(CPoint point)
{
//    return HTNOWHERE;
	return CSplitterWnd::OnNcHitTest(point);
}

void CProdDataSplitter::OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect)
{
	if (pDC == NULL)
	{
		RedrawWindow(rect, NULL, RDW_INVALIDATE|RDW_NOCHILDREN);
		return;
	}
//	if ( (nType == splitBar) )// || (nType == splitBox) || (nType == splitIntersection) )
	if (1)
	{
		CPen pen(PS_SOLID, 1, (nType == splitBar) ? RGB(165,190,250) : RGB(236,233,216));
		CBrush brush((nType == splitBar) ? RGB(193,211,251) : RGB(236,233,216)); 
		pDC->SelectObject(&pen);
		pDC->SelectObject(&brush);
		pDC->Rectangle(rect);
		//pDC->FillSolidRect(rect, RGB(193,211,251));
	}
	else
	{
		CSplitterWnd::OnDrawSplitter(pDC,nType,rect);
		return;
	}
}



// CProdDataFrame

IMPLEMENT_DYNCREATE(CProdDataFrame, CMDIChildWnd)

CProdDataFrame::CProdDataFrame()
{

}

CProdDataFrame::~CProdDataFrame()
{
}

void CProdDataFrame::SwitchGraphicView()
{
	CView* pView = (CView*)m_wndSplitter.GetPane(1, 0);
	if (pView->IsKindOf(RUNTIME_CLASS(CProdDataGraphicOverView)))	// is already this view
		return;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CProdDataGraphicOverView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter.DeleteView(1, 0);
	m_wndSplitter.CreateView(1, 0, RUNTIME_CLASS(CProdDataGraphicOverView),	CSize(0, 0), &context);

	CProdDataGraphicOverView* pProdDataGraphicOverView = (CProdDataGraphicOverView*)m_wndSplitter.GetPane(1, 0);
	SetActiveView(pProdDataGraphicOverView);
	m_wndSplitter.RecalcLayout();
	m_wndSplitter.Invalidate();
	m_wndSplitter.UpdateWindow();
	pProdDataGraphicOverView->OnInitialUpdate();
	pProdDataGraphicOverView->Invalidate();
	pProdDataGraphicOverView->UpdateWindow();
}

void CProdDataFrame::SwitchTableView()
{
	CView* pView = (CView*)m_wndSplitter.GetPane(1, 0);
	if (pView->IsKindOf(RUNTIME_CLASS(CProdDataProcessOverView)))	// is already this view
		return;

	CCreateContext context;
	context.m_pNewViewClass	  = RUNTIME_CLASS(CProdDataProcessOverView);
	context.m_pCurrentDoc	  = CImpManDoc::GetDoc();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter.DeleteView(1, 0);
	m_wndSplitter.CreateView(1, 0, RUNTIME_CLASS(CProdDataProcessOverView),	CSize(0, 0), &context);

	CProdDataProcessOverView* pProdDataProcessOverView = (CProdDataProcessOverView*)m_wndSplitter.GetPane(1, 0);
	SetActiveView(pProdDataProcessOverView);
	m_wndSplitter.RecalcLayout();
	m_wndSplitter.Invalidate();
	m_wndSplitter.UpdateWindow();
	pProdDataProcessOverView->OnInitialUpdate();
	pProdDataProcessOverView->Invalidate();
	pProdDataProcessOverView->UpdateWindow();
}


BEGIN_MESSAGE_MAP(CProdDataFrame, CMDIChildWnd)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CProdDataFrame-Meldungshandler



BOOL CProdDataFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// create a splitter with 2 rows, 1 column
	if (!m_wndSplitter.CreateStatic(this, 2, 1))
		return FALSE;
	if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CProdDataHeaderView), CSize(0, 80), pContext))
		return FALSE;
	if (!m_wndSplitter.CreateView(1, 0, pContext->m_pNewViewClass, CSize(0, 0), pContext))
		return FALSE;

	return TRUE;
}

void CProdDataFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	//CMDIChildWnd::OnUpdateFrameTitle(bAddToTitle);    // do nothing (done in CMainFrame)
	//::SetWindowText(this->GetSafeHwnd(), _T(""));

	CDocument* pDoc = GetActiveDocument();
	if (pDoc)
	{
		CString title = pDoc->GetTitle();
		//title += _T(" - ");
		//CString string;
		//string.LoadString(IDS_NAV_PRODUCTIONDATA);
		//title += string;
		::SetWindowText(this->GetSafeHwnd(), title);
	}
}

void CProdDataFrame::OnSize(UINT nType, int cx, int cy) 
{
	if (m_wndSplitter.m_hWnd)
		if ( m_wndSplitter.GetRowCount() && m_wndSplitter.GetColumnCount() )
			if (m_wndSplitter.GetDlgItem(m_wndSplitter.IdFromRowCol(0, 0)))
			{
				RecalcLayout();
				CRect rcFrame;
				GetClientRect(rcFrame);
				int nHeight0 = ((CScrollView*)m_wndSplitter.GetPane(0, 0))->GetTotalSize().cy;
				int nHeight1 = rcFrame.Height() - nHeight0 - 10;	// 10 = gap between splitters

				m_wndSplitter.SetRowInfo	(0, (nHeight0 >= 0) ? nHeight0 : 0, 10);
				m_wndSplitter.SetRowInfo	(1, (nHeight1 >= 0) ? nHeight1 : 0, 10);

				CMDIChildWnd::OnSize(nType, cx, cy);
			}
}

