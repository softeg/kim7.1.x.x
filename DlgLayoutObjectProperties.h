#if !defined(AFX_DLGLAYOUTOBJECTPROPERTIES_H__D934A8E3_B425_11D5_88BC_0040F68C1EF7__INCLUDED_)
#define AFX_DLGLAYOUTOBJECTPROPERTIES_H__D934A8E3_B425_11D5_88BC_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgLayoutObjectProperties.h : Header-Datei
//





//#ifndef OBJECTPROPS
#include "PrintColorList.h"
#include "PlaceholderEdit.h"
#include "DlgSystemGenMarkColorControl.h"
#include "DlgSelectSectionMark.h"
#include "DlgSectionMarkGeometry.h"
#include "DlgSectionMarkContent.h"
#include "DlgCropMarkGeometry.h"
#include "DlgCropMarkContent.h"
#include "DlgFoldMarkGeometry.h"
#include "DlgFoldMarkContent.h"
#include "MarkSourceWindow.h"
#include "DlgSelectNewMark.h"
#include "MarkPivotControl.h"
#include "ObjectPivotControl.h"
#include "DlgLayoutObjectGeometry.h"
#include "DlgMarkPlacement.h"
#include "DlgObjectZOrder.h"
#include "DlgMoveObjects.h"
#include "DlgPDFObjectContent.h"
#include "DlgTextMarkObjectContent.h"
//#define OBJECTPROPS
//#endif



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgLayoutObjectProperties 

class CDlgLayoutObjectProperties : public CDialog
{
// Konstruktion
public:
	CDlgLayoutObjectProperties(CWnd* pParent = NULL);   // Standardkonstruktor

// Destruktion
public:
	~CDlgLayoutObjectProperties();   


// Dialogfelddaten
	//{{AFX_DATA(CDlgLayoutObjectProperties)
	enum { IDD = IDD_LAYOUTOBJECT_PROPERTIES };
	CString	m_strObjectName;
	CString	m_strMarkType;
	//}}AFX_DATA

	enum ObjectType {ObjectTypeError, PageObject, CustomMarkObject, TextMarkObject, SectionMarkObject, CropMarkObject, FoldMarkObject};

public:
	CString						m_strInitialTitle;
	CRect						m_tabRect;
	CLayoutObject*				m_pLayoutObject;
	CPrintSheet*				m_pPrintSheet;
	CPageTemplate*				m_pTemplate;
	CMark*						m_pMark;
	CPageSource					m_pObjectSource;
	int							m_nMarkType;
	CDlgLayoutObjectControl		m_dlgLayoutObjectControl;
//	CDlgObjectPreviewControl		m_dlgObjectPreviewControl;
	CColorDefinitionTable*		m_pColorDefTable;
	int							m_nInitialPage;
	CSize						m_sizeViewport;
	BOOL						m_bModeModify;



public:
	BOOL					Create(CLayoutObject* pLayoutObject, CMark* pMark, CPrintSheet* pPrintSheet, CWnd* pParent, int nInitialPage = 0, BOOL bModeModify = FALSE);
	BOOL					Create(CMark* pMark, CPrintSheet* pPrintSheet, CWnd* pParent, int nInitialPage = 0, BOOL bModeModify = FALSE);
	void					ReinitDialog(CLayoutObject* pNewLayoutObject, CPrintSheet* pNewPrintSheet);
	void					ReinitDialog(CMark* pNewMark, CPrintSheet* pNewPrintSheet);
	void					InitObjectType();
	void 					SetTitle(BOOL bInitial = FALSE);
	BOOL					IsActiveReflineControl();
	void					DataExchangeLoad();
	void					DataExchangeSave();
	CLayoutObject*			GetFirstLayoutObject();
	CLayoutObject*			GetNextLayoutObject(CLayoutObject* pLayoutObject);
	CMark*					GetFirstMark();
	CMark*					GetNextMark(CMark* pMark);
	CPrintSheet*			GetPrintSheet();
	void					InitPageObject();
	void					LoadPageObjectData();
	void					SavePageObjectData();
	void					InitCustomMarkObject();
	void					LoadCustomMarkObjectData();
	void					SaveCustomMarkObjectData();
	void					InitTextMarkObject();
	void					LoadTextMarkObjectData();
	void					SaveTextMarkObjectData();
	void					InitSectionMarkObject();
	void					LoadSectionMarkObjectData();
	void					SaveSectionMarkObjectData();
	void					InitCropMarkObject();
	void					LoadCropMarkObjectData();
	void					SaveCropMarkObjectData();
	void					InitFoldMarkObject();
	void					LoadFoldMarkObjectData();
	void					SaveFoldMarkObjectData();
	void					UpdateView();
	CColorDefinitionTable*	GetActColorDefTable();
	int						ColorDefTable_GetSize();
	CColorDefinition*		ColorDefTable_GetColorDef(int nColorDefinitionIndex);
	int						ColorDefTable_FindColorDefinitionIndex(const CString& strColorName);
	int						ColorDefTable_InsertColorDef(CColorDefinition& rColorDef);
	void					PrepareUpdatePreview();
	void					GetBoxes(CRect& outBBox, CRect& trimBox);
	void					DrawPreview(CDC* pDC, CRect& previewFrameRect, 
									    CPageSource* pObjectSource, CPageSourceHeader* pObjectSourceHeader, CPageTemplate* pTemplate, CRect& outBBox, CRect& trimBox,
										CPrintSheet* pPrintSheet);
	void					Apply(BOOL bUpdateView = TRUE);


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgLayoutObjectProperties)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgLayoutObjectProperties)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnApplyButton();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDestroy();
	afx_msg void OnChangeLayoutobjectName();
	afx_msg void OnAssignMark();
	//}}AFX_MSG
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized );
	afx_msg void OnUpdateSubDialogs();
	afx_msg void OnUpdateMarkname();
	DECLARE_MESSAGE_MAP()
	virtual void PostNcDestroy();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGLAYOUTOBJECTPROPERTIES_H__D934A8E3_B425_11D5_88BC_0040F68C1EF7__INCLUDED_
