#pragma once
#include "afxwin.h"

class CSelectProcessesListBox : public CListCtrl
{

// Implementation
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
};


// CDlgSelectProcesses-Dialogfeld

class CDlgSelectProcesses : public CDialog
{
	DECLARE_DYNAMIC(CDlgSelectProcesses)

public:
	CDlgSelectProcesses(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgSelectProcesses();

// Dialogfelddaten
	enum { IDD = IDD_SELECT_PROCESSES };

public:
	CArray<int, int>			m_selectedProcesses;
	CSelectProcessesListBox		m_allProcessesList;
	CSelectProcessesListBox		m_selectedProcessesList;
	CImageList*					m_pImageList;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedSelectprocessAdd();
	afx_msg void OnBnClickedSelectprocessRemove();
	afx_msg void OnBnClickedOk();
	afx_msg void OnNMDblclkAllprocessesList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkSelectedprocessesList(NMHDR *pNMHDR, LRESULT *pResult);
};
