// DlgLayoutObjectGeometry.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgLayoutObjectControl.h"
#include "DlgChangeFormats.h"
#include "PrintSheetNavigationView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgLayoutObjectGeometry 


CDlgLayoutObjectGeometry::CDlgLayoutObjectGeometry(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgLayoutObjectGeometry::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgLayoutObjectGeometry)
	m_fObjectHeight = 0.0f;
	m_fObjectWidth = 0.0f;
	m_fDistanceX = 0.0f;
	m_fDistanceY = 0.0f;
	m_bVariableWidth = FALSE;
	m_bVariableHeight = FALSE;
	m_nMarkRefSelector = 0;
	m_nMarkAnchor = 0;
	//}}AFX_DATA_INIT

	m_pParent = NULL;
}

CDlgLayoutObjectGeometry::~CDlgLayoutObjectGeometry()
{
}

BOOL CDlgLayoutObjectGeometry::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgLayoutObjectGeometry::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgLayoutObjectGeometry)
	DDX_Control(pDX, IDC_DISTANCEY_SPIN, m_distanceYSpin);
	DDX_Control(pDX, IDC_DISTANCEX_SPIN, m_distanceXSpin);
	DDX_Control(pDX, IDC_WIDTH_SPIN, m_widthSpin);
	DDX_Control(pDX, IDC_HEIGHT_SPIN, m_heightSpin);
	DDX_Measure(pDX, IDC_MARK_HEIGHT, m_fObjectHeight);
	DDX_Measure(pDX, IDC_MARK_WIDTH, m_fObjectWidth);
	DDX_Measure(pDX, IDC_DISTANCEX, m_fDistanceX);
	DDX_Measure(pDX, IDC_DISTANCEY, m_fDistanceY);
	DDX_Check(pDX, IDC_MARKWIDTH_VARIABLE, m_bVariableWidth);
	DDX_Check(pDX, IDC_MARKHEIGHT_VARIABLE, m_bVariableHeight);
	DDX_Radio(pDX, IDC_MARKREF_1, m_nMarkRefSelector);
	DDX_Radio(pDX, IDC_MARKANCHOR_LINES, m_nMarkAnchor);
	//}}AFX_DATA_MAP
}



BEGIN_MESSAGE_MAP(CDlgLayoutObjectGeometry, CDialog)
	//{{AFX_MSG_MAP(CDlgLayoutObjectGeometry)
	ON_NOTIFY(UDN_DELTAPOS, IDC_HEIGHT_SPIN, OnDeltaposHeightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_WIDTH_SPIN, OnDeltaposWidthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_MARK_ORIENTATION_SPIN, OnDeltaposMarkOrientationSpin)
	ON_WM_MOUSEMOVE()
	ON_NOTIFY(UDN_DELTAPOS, IDC_DISTANCEX_SPIN, OnDeltaposDistancexSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_DISTANCEY_SPIN, OnDeltaposDistanceySpin)
	ON_COMMAND(ID_CHANGED_PIVOT, OnChangedPivot)
	ON_BN_CLICKED(IDC_MARKWIDTH_VARIABLE, OnWidthVariable)
	ON_BN_CLICKED(IDC_MARKHEIGHT_VARIABLE, OnHeightVariable)
	ON_BN_CLICKED(IDC_MARKANCHOR_LINES, OnMarkAnchorLines)
	ON_BN_CLICKED(IDC_MARKANCHOR_PAGES, OnMarkAnchorPages)
	ON_BN_CLICKED(IDC_MARKANCHOR_FOLDSHEETS, OnMarkAnchorFoldSheets)
	ON_BN_CLICKED(IDC_MARKREF_1, OnMarkref_1)
	ON_BN_CLICKED(IDC_MARKREF_2, OnMarkref_2)
	ON_BN_CLICKED(IDC_MARKREF_3, OnMarkref_3)
	ON_BN_CLICKED(IDC_MARKREF_4, OnMarkref_4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgLayoutObjectGeometry 

BOOL CDlgLayoutObjectGeometry::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_widthSpin.SetRange (0, 1);  
	m_heightSpin.SetRange(0, 1);  

	UpdateHeadPositionControl();

	m_markPivotCtrl.SubclassDlgItem(IDC_MEDIA_PIVOT, this);
	m_markPivotCtrl.m_pParent = this;
	m_anchorPivotCtrl.SubclassDlgItem(IDC_ANCHOR_PIVOT, this);
	m_anchorPivotCtrl.m_pParent = this;

	m_distanceXSpin.SetRange(0, 1);  
	m_distanceYSpin.SetRange(0, 1);  

	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_DISTANCEX);
	pEdit->SetSel(0, -1);

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
		pLayoutObject->UpdateReflineDistances();

	m_strLeft.LoadString(IDS_LEFT); m_strRight.LoadString(IDS_RIGHT); m_strBottom.LoadString(IDS_BOTTOM); m_strTop.LoadString(IDS_TOP);
	m_strVertical.LoadString(IDS_VERTICAL); m_strHorizontal.LoadString(IDS_HORIZONTAL);

	UpdatePositionControls();
	UpdateMarkAnchorSetting();
	UpdateMarkrefSelectors();
	m_nMarkRefSelector = 0;

	UpdateData(FALSE);

	m_markPivotCtrl.m_nActiveCell = -1;
	m_anchorPivotCtrl.m_nActiveCell = -1;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgLayoutObjectGeometry::UpdatePositionControls()
{
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	int nShow = (pLayoutObject) ? SW_HIDE : SW_SHOW;

	GetDlgItem(IDC_MARKWIDTH_VARIABLE)->ShowWindow(nShow);
	GetDlgItem(IDC_MARKHEIGHT_VARIABLE)->ShowWindow(nShow);
	GetDlgItem(IDC_POSITION_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_PIVOT_STATIC3)->ShowWindow(nShow);
	GetDlgItem(IDC_MARKANCHOR_LINES)->ShowWindow(nShow);
	GetDlgItem(IDC_MARKANCHOR_PAGES)->ShowWindow(nShow);
	GetDlgItem(IDC_MARKANCHOR_FOLDSHEETS)->ShowWindow(nShow);
	GetDlgItem(IDC_DISTANCEX_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_DISTANCEY_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_DISTANCEX)->ShowWindow(nShow);
	GetDlgItem(IDC_DISTANCEY)->ShowWindow(nShow);
	GetDlgItem(IDC_DISTANCEX_SPIN)->ShowWindow(nShow);
	GetDlgItem(IDC_DISTANCEY_SPIN)->ShowWindow(nShow);
	GetDlgItem(IDC_PIVOT_STATIC1)->ShowWindow(nShow);
	GetDlgItem(IDC_PIVOT_STATIC)->ShowWindow(nShow);
	GetDlgItem(IDC_MEDIA_PIVOT)->ShowWindow(nShow);
}

void CDlgLayoutObjectGeometry::UpdateMarkAnchorSetting()
{
	UpdateData(TRUE);

	if (m_pParent->GetFirstLayoutObject())
	{
		GetDlgItem(IDC_ANCHOR_PIVOT_STATIC)->ShowWindow(SW_HIDE); 
		GetDlgItem(IDC_ANCHOR_PIVOT)->ShowWindow(SW_HIDE); 
	}
	else
	{
		switch (m_nMarkAnchor)
		{
		case 0:	GetDlgItem(IDC_ANCHOR_PIVOT_STATIC)->ShowWindow(SW_HIDE); 
				GetDlgItem(IDC_ANCHOR_PIVOT)->ShowWindow(SW_HIDE); 
				break;
		case 1:	GetDlgItem(IDC_ANCHOR_PIVOT_STATIC)->ShowWindow(SW_SHOW); 
				GetDlgItem(IDC_ANCHOR_PIVOT)->ShowWindow(SW_SHOW); 
				break;
		case 2:	GetDlgItem(IDC_ANCHOR_PIVOT_STATIC)->ShowWindow(SW_SHOW); 
				GetDlgItem(IDC_ANCHOR_PIVOT)->ShowWindow(SW_SHOW); 
				break;
		}
	}
}

void CDlgLayoutObjectGeometry::UpdateMarkrefSelectors()
{
	UpdateData(TRUE);

	if (m_bVariableWidth && m_bVariableHeight)
	{
		GetDlgItem(IDC_MARKREF_1)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_1)->SetWindowText(m_strLeft);
		GetDlgItem(IDC_MARKREF_2)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_2)->SetWindowText(m_strRight);
		GetDlgItem(IDC_MARKREF_3)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_3)->SetWindowText(m_strBottom);
		GetDlgItem(IDC_MARKREF_4)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_4)->SetWindowText(m_strTop);
		GetDlgItem(IDC_MARK_WIDTH)->EnableWindow(FALSE);  GetDlgItem(IDC_WIDTH_SPIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MARK_HEIGHT)->EnableWindow(FALSE); GetDlgItem(IDC_HEIGHT_SPIN)->EnableWindow(FALSE);
	}
	else
	if (m_bVariableWidth && ! m_bVariableHeight)
	{
		GetDlgItem(IDC_MARKREF_1)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_1)->SetWindowText(m_strLeft);
		GetDlgItem(IDC_MARKREF_2)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_2)->SetWindowText(m_strRight);
		GetDlgItem(IDC_MARKREF_3)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_3)->SetWindowText(m_strVertical);
		GetDlgItem(IDC_MARKREF_4)->ShowWindow(SW_HIDE); 
		GetDlgItem(IDC_MARK_WIDTH)->EnableWindow(FALSE); GetDlgItem(IDC_WIDTH_SPIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MARK_HEIGHT)->EnableWindow(TRUE); GetDlgItem(IDC_HEIGHT_SPIN)->EnableWindow(TRUE);
		if (m_nMarkRefSelector > 2)
			m_nMarkRefSelector = 0;
	}
	else
	if ( ! m_bVariableWidth && m_bVariableHeight)
	{
		GetDlgItem(IDC_MARKREF_1)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_1)->SetWindowText(m_strBottom);
		GetDlgItem(IDC_MARKREF_2)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_2)->SetWindowText(m_strTop);
		GetDlgItem(IDC_MARKREF_3)->ShowWindow(SW_SHOW); GetDlgItem(IDC_MARKREF_3)->SetWindowText(m_strHorizontal);
		GetDlgItem(IDC_MARKREF_4)->ShowWindow(SW_HIDE); 
		GetDlgItem(IDC_MARK_WIDTH)->EnableWindow(TRUE);   GetDlgItem(IDC_WIDTH_SPIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_MARK_HEIGHT)->EnableWindow(FALSE); GetDlgItem(IDC_HEIGHT_SPIN)->EnableWindow(FALSE);
		if (m_nMarkRefSelector > 2)
			m_nMarkRefSelector = 0;
	}
	else
	{
		GetDlgItem(IDC_MARKREF_1)->ShowWindow(SW_HIDE); GetDlgItem(IDC_MARKREF_1)->SetWindowText(_T(""));
		GetDlgItem(IDC_MARKREF_2)->ShowWindow(SW_HIDE); GetDlgItem(IDC_MARKREF_1)->SetWindowText(_T("")); 
		GetDlgItem(IDC_MARKREF_3)->ShowWindow(SW_HIDE); GetDlgItem(IDC_MARKREF_1)->SetWindowText(_T("")); 
		GetDlgItem(IDC_MARKREF_4)->ShowWindow(SW_HIDE); GetDlgItem(IDC_MARKREF_1)->SetWindowText(_T("")); 
		GetDlgItem(IDC_MARK_WIDTH)->EnableWindow(TRUE);  GetDlgItem(IDC_WIDTH_SPIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_MARK_HEIGHT)->EnableWindow(TRUE); GetDlgItem(IDC_HEIGHT_SPIN)->EnableWindow(TRUE);
		m_nMarkRefSelector = 0;
	}
	UpdateData(FALSE);

	OnSelchangeMarkRefSelector();
}

void CDlgLayoutObjectGeometry::OnWidthVariable()
{
	UpdateMarkrefSelectors();
	m_nMarkRefSelector = 0;
	UpdateData(FALSE);

	Apply();
	m_pParent->UpdateView();
	LoadData();
}

void CDlgLayoutObjectGeometry::OnHeightVariable()
{
	UpdateMarkrefSelectors();
	m_nMarkRefSelector = 0;
	UpdateData(FALSE);

	Apply();
	m_pParent->UpdateView();
	LoadData();
}

void CDlgLayoutObjectGeometry::OnMarkAnchorLines()
{
	UpdateMarkAnchorSetting();

	UpdateMarkrefSelectors();
	m_nMarkRefSelector = 0;
	UpdateData(FALSE);

	Apply();
	m_pParent->UpdateView();
	LoadData();
}

void CDlgLayoutObjectGeometry::OnMarkAnchorPages()
{
	UpdateMarkAnchorSetting();

	m_bVariableWidth = m_bVariableHeight = FALSE;
	UpdateData(FALSE);
	UpdateMarkrefSelectors();
	m_nMarkRefSelector = 0;
	UpdateData(FALSE);

	Apply();
	m_pParent->UpdateView();
	LoadData();
}

void CDlgLayoutObjectGeometry::OnMarkAnchorFoldSheets()
{
	UpdateMarkAnchorSetting();

	m_bVariableWidth = m_bVariableHeight = FALSE;
	UpdateData(FALSE);
	UpdateMarkrefSelectors();
	m_nMarkRefSelector = 0;
	UpdateData(FALSE);

	Apply();
	m_pParent->UpdateView();
	LoadData();
}

void CDlgLayoutObjectGeometry::OnMarkref_1() { SaveData(); OnSelchangeMarkRefSelector(); LoadData(); m_pParent->UpdateView(); }
void CDlgLayoutObjectGeometry::OnMarkref_2() { SaveData(); OnSelchangeMarkRefSelector(); LoadData(); m_pParent->UpdateView(); }
void CDlgLayoutObjectGeometry::OnMarkref_3() { SaveData(); OnSelchangeMarkRefSelector(); LoadData(); m_pParent->UpdateView(); }
void CDlgLayoutObjectGeometry::OnMarkref_4() { SaveData(); OnSelchangeMarkRefSelector(); LoadData(); m_pParent->UpdateView(); }

void CDlgLayoutObjectGeometry::OnSelchangeMarkRefSelector()
{
	if (m_pParent->GetFirstLayoutObject())	// only for marks
		return;

	UpdateData(TRUE);

	CString strLabel;
	switch (m_nMarkRefSelector)
	{
	case 0: GetDlgItem(IDC_MARKREF_1)->GetWindowText(strLabel); break;
	case 1: GetDlgItem(IDC_MARKREF_2)->GetWindowText(strLabel); break;
	case 2: GetDlgItem(IDC_MARKREF_3)->GetWindowText(strLabel); break;
	case 3: GetDlgItem(IDC_MARKREF_4)->GetWindowText(strLabel); break;
	}

	GetDlgItem(IDC_PIVOT_STATIC)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_MEDIA_PIVOT)->ShowWindow(SW_HIDE);
	if ( (strLabel == m_strLeft) || (strLabel == m_strRight) )
	{
		GetDlgItem(IDC_DISTANCEX_STATIC)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEX)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEX_SPIN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_DISTANCEY_STATIC)->ShowWindow(SW_HIDE); GetDlgItem(IDC_DISTANCEY)->ShowWindow(SW_HIDE); GetDlgItem(IDC_DISTANCEY_SPIN)->ShowWindow(SW_HIDE);
	}
	else
	if ( (strLabel == m_strBottom) || (strLabel == m_strTop) )
	{
		GetDlgItem(IDC_DISTANCEX_STATIC)->ShowWindow(SW_HIDE); GetDlgItem(IDC_DISTANCEX)->ShowWindow(SW_HIDE); GetDlgItem(IDC_DISTANCEX_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_DISTANCEY_STATIC)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEY)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEY_SPIN)->ShowWindow(SW_SHOW);
	}
	else
	if (strLabel == m_strVertical)
	{
		GetDlgItem(IDC_DISTANCEX_STATIC)->ShowWindow(SW_HIDE); GetDlgItem(IDC_DISTANCEX)->ShowWindow(SW_HIDE); GetDlgItem(IDC_DISTANCEX_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_DISTANCEY_STATIC)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEY)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEY_SPIN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_PIVOT_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_MEDIA_PIVOT)->ShowWindow(SW_SHOW);
	}
	else
	if (strLabel == m_strHorizontal)
	{
		GetDlgItem(IDC_DISTANCEX_STATIC)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEX)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEX_SPIN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_DISTANCEY_STATIC)->ShowWindow(SW_HIDE); GetDlgItem(IDC_DISTANCEY)->ShowWindow(SW_HIDE); GetDlgItem(IDC_DISTANCEY_SPIN)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PIVOT_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_MEDIA_PIVOT)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_DISTANCEX_STATIC)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEX)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEX_SPIN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_DISTANCEY_STATIC)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEY)->ShowWindow(SW_SHOW); GetDlgItem(IDC_DISTANCEY_SPIN)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_PIVOT_STATIC)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_MEDIA_PIVOT)->ShowWindow(SW_SHOW);
	}
}

int CDlgLayoutObjectGeometry::GetReflinePosParamsID()
{
	CString strLabel;
	switch (m_nMarkRefSelector)
	{
	case 0: GetDlgItem(IDC_MARKREF_1)->GetWindowText(strLabel); break;
	case 1: GetDlgItem(IDC_MARKREF_2)->GetWindowText(strLabel); break;
	case 2: GetDlgItem(IDC_MARKREF_3)->GetWindowText(strLabel); break;
	case 3: GetDlgItem(IDC_MARKREF_4)->GetWindowText(strLabel); break;
	}

	if (strLabel == m_strLeft)
		return 1;
	else
	if (strLabel == m_strRight)
		return 2;
	else
	if (strLabel == m_strBottom)
		return 1;
	else
	if (strLabel == m_strTop)
		return 2;
	else
		return 0;
}

int CDlgLayoutObjectGeometry::GetMarkEdge()
{
	CString strLabel;
	switch (m_nMarkRefSelector)
	{
	case 0: GetDlgItem(IDC_MARKREF_1)->GetWindowText(strLabel); break;
	case 1: GetDlgItem(IDC_MARKREF_2)->GetWindowText(strLabel); break;
	case 2: GetDlgItem(IDC_MARKREF_3)->GetWindowText(strLabel); break;
	case 3: GetDlgItem(IDC_MARKREF_4)->GetWindowText(strLabel); break;
	}

	if (strLabel == m_strLeft)
		return LEFT;
	else
	if (strLabel == m_strRight)
		return RIGHT;
	else
	if (strLabel == m_strBottom)
		return BOTTOM;
	else
	if (strLabel == m_strTop)
		return TOP;
	else
		return 0;
}

BOOL CDlgLayoutObjectGeometry::HorizontalOnly()
{
	CString strLabel;
	switch (m_nMarkRefSelector)
	{
	case 0: GetDlgItem(IDC_MARKREF_1)->GetWindowText(strLabel); break;
	case 1: GetDlgItem(IDC_MARKREF_2)->GetWindowText(strLabel); break;
	case 2: GetDlgItem(IDC_MARKREF_3)->GetWindowText(strLabel); break;
	case 3: GetDlgItem(IDC_MARKREF_4)->GetWindowText(strLabel); break;
	}

	if (strLabel == m_strHorizontal)
		return TRUE;
	else
		return FALSE;
}

BOOL CDlgLayoutObjectGeometry::VerticalOnly()
{
	CString strLabel;
	switch (m_nMarkRefSelector)
	{
	case 0: GetDlgItem(IDC_MARKREF_1)->GetWindowText(strLabel); break;
	case 1: GetDlgItem(IDC_MARKREF_2)->GetWindowText(strLabel); break;
	case 2: GetDlgItem(IDC_MARKREF_3)->GetWindowText(strLabel); break;
	case 3: GetDlgItem(IDC_MARKREF_4)->GetWindowText(strLabel); break;
	}

	if (strLabel == m_strVertical)
		return TRUE;
	else
		return FALSE;
}

void CDlgLayoutObjectGeometry::CheckSwapReflinePosParams(CReflinePositionParams& newReflinePosParams)
{
	CMark* pMark = m_pParent->GetFirstMark();
	if ( ! pMark)
		return;
	CReflineMark* pReflineMark = (CReflineMark*)pMark;

	int nMarkEdge = GetMarkEdge();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	int nSide = (pView) ? pView->GetActSight() : FRONTSIDE;

	CReflinePositionParams* pReflinePosParamsAnchor = pReflineMark->GetReflinePosParams(0);
	CReflinePositionParams* pReflinePosParamsLL		= pReflineMark->GetReflinePosParams(1);
	CReflinePositionParams* pReflinePosParamsUR		= pReflineMark->GetReflinePosParams(2);

	float fLeftLL, fBottomLL, fLeftUR, fBottomUR, fLeftNew, fBottomNew;
	pReflinePosParamsLL->GetSheetPosition(m_pParent->m_pPrintSheet, nSide, fLeftLL,  fBottomLL);
	pReflinePosParamsUR->GetSheetPosition(m_pParent->m_pPrintSheet, nSide, fLeftUR,  fBottomUR);
	newReflinePosParams.GetSheetPosition (m_pParent->m_pPrintSheet, nSide, fLeftNew, fBottomNew);

	switch (nMarkEdge)
	{
	case LEFT:		if (fLeftNew > fLeftUR + pReflineMark->m_fMarkWidth)
					{
						SwitchToEdge(m_strRight);
						pReflinePosParamsUR->m_nReferenceItemInX   = newReflinePosParams.m_nReferenceItemInX;
						pReflinePosParamsUR->m_nReferenceLineInX   = newReflinePosParams.m_nReferenceLineInX;
						pReflinePosParamsUR->m_hReferenceObjectInX = newReflinePosParams.m_hReferenceObjectInX;
					}
					else
						*pReflinePosParamsLL = newReflinePosParams;
					break;
	case RIGHT:		if (fLeftNew < fLeftLL)
					{
						SwitchToEdge(m_strLeft);
						pReflinePosParamsLL->m_nReferenceItemInX   = newReflinePosParams.m_nReferenceItemInX;
						pReflinePosParamsLL->m_nReferenceLineInX   = newReflinePosParams.m_nReferenceLineInX;
						pReflinePosParamsLL->m_hReferenceObjectInX = newReflinePosParams.m_hReferenceObjectInX;
					}
					else
						*pReflinePosParamsUR = newReflinePosParams;
					break;
	case BOTTOM:	if (fBottomNew > fBottomUR + pReflineMark->m_fMarkHeight)
					{
						SwitchToEdge(m_strTop);
						pReflinePosParamsUR->m_nReferenceItemInY   = newReflinePosParams.m_nReferenceItemInY;
						pReflinePosParamsUR->m_nReferenceLineInY   = newReflinePosParams.m_nReferenceLineInY;
						pReflinePosParamsUR->m_hReferenceObjectInY = newReflinePosParams.m_hReferenceObjectInY;
					}
					else
						*pReflinePosParamsLL = newReflinePosParams;
					break;
	case TOP:		if (fBottomNew < fBottomLL)
					{
						SwitchToEdge(m_strBottom);
						pReflinePosParamsLL->m_nReferenceItemInY   = newReflinePosParams.m_nReferenceItemInY;
						pReflinePosParamsLL->m_nReferenceLineInY   = newReflinePosParams.m_nReferenceLineInY;
						pReflinePosParamsLL->m_hReferenceObjectInY = newReflinePosParams.m_hReferenceObjectInY;
					}
					else
						*pReflinePosParamsUR = newReflinePosParams;
					break;
	default:		*pReflinePosParamsAnchor = newReflinePosParams;
					break;
	}
}

void CDlgLayoutObjectGeometry::SwitchToEdge(CString strEdge)
{
	CString strLabel1, strLabel2, strLabel3, strLabel4;
	GetDlgItem(IDC_MARKREF_1)->GetWindowText(strLabel1); 
	GetDlgItem(IDC_MARKREF_2)->GetWindowText(strLabel2); 
	GetDlgItem(IDC_MARKREF_3)->GetWindowText(strLabel3); 
	GetDlgItem(IDC_MARKREF_4)->GetWindowText(strLabel4); 

	if (strLabel1 == strEdge)
		m_nMarkRefSelector = 0;
	else
	if (strLabel2 == strEdge)
		m_nMarkRefSelector = 1;
	else
	if (strLabel3 == strEdge)
		m_nMarkRefSelector = 2;
	else
	if (strLabel4 == strEdge)
		m_nMarkRefSelector = 3;
	
	UpdateData(FALSE);
}

void CDlgLayoutObjectGeometry::OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_widthSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgLayoutObjectGeometry::OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_heightSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 1;
}

void CDlgLayoutObjectGeometry::OnDeltaposMarkOrientationSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int nIDBitmap;
	if (pNMUpDown->iDelta < 0)
	{
		switch (m_nHeadPosition)
		{
		case TOP:	 nIDBitmap = IDB_PORTRAIT_90;  m_nHeadPosition = LEFT;	break;
		case LEFT:	 nIDBitmap = IDB_PORTRAIT_180; m_nHeadPosition = BOTTOM;break;
		case BOTTOM: nIDBitmap = IDB_PORTRAIT_270; m_nHeadPosition = RIGHT;	break;
		case RIGHT:  nIDBitmap = IDB_PORTRAIT;	   m_nHeadPosition = TOP;	break;
		default:	 nIDBitmap = -1;			   m_nHeadPosition = TOP;	break;
		}
	}
	else
	{
		switch (m_nHeadPosition)
		{
		case TOP:	 nIDBitmap = IDB_PORTRAIT_270; m_nHeadPosition = RIGHT;	break;
		case LEFT:	 nIDBitmap = IDB_PORTRAIT;	   m_nHeadPosition = TOP;	break;
		case BOTTOM: nIDBitmap = IDB_PORTRAIT_90;  m_nHeadPosition = LEFT;	break;
		case RIGHT:  nIDBitmap = IDB_PORTRAIT_180; m_nHeadPosition = BOTTOM;break;
		default:	 nIDBitmap = -1;			   m_nHeadPosition = TOP;	break;
		}
	}

	if ((HBITMAP)m_bmMarkOrientation)
		m_bmMarkOrientation.DeleteObject();
	m_bmMarkOrientation.LoadBitmap(nIDBitmap); 
	((CStatic*)GetDlgItem(IDC_MARK_ORIENTATION))->SetBitmap((HBITMAP)m_bmMarkOrientation);

	if (m_pParent->GetFirstLayoutObject())
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
		{
			pView->UndoSave();

			CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			while (pDI)
			{
				CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
				pLayoutObject->m_nHeadPosition = m_nHeadPosition;
				pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			}
		}
	}

	SaveData();

	m_pParent->UpdateView();

	*pResult = 1;
}

void CDlgLayoutObjectGeometry::OnMouseMove(UINT nFlags, CPoint point) 
{
	m_markPivotCtrl.OnMouseMove(nFlags, point);
	m_anchorPivotCtrl.OnMouseMove(nFlags, point);

	CDialog::OnMouseMove(nFlags, point);
}

void CDlgLayoutObjectGeometry::OnDeltaposDistancexSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_distanceXSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 0;
}

void CDlgLayoutObjectGeometry::OnDeltaposDistanceySpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_distanceYSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	*pResult = 0;
}

CLayoutSide* CDlgLayoutObjectGeometry::GetLayoutObjectsBBox(float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop)
{
	fBBoxLeft	=  FLT_MAX; 
	fBBoxBottom =  FLT_MAX; 
	fBBoxRight	= -FLT_MAX; 
	fBBoxTop	= -FLT_MAX;

	CLayoutSide*   pLayoutSide	 = NULL;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	while (pLayoutObject)
	{
		fBBoxLeft	  = min(fBBoxLeft,	 pLayoutObject->GetLeft());
		fBBoxBottom   = min(fBBoxBottom, pLayoutObject->GetBottom());
		fBBoxRight	  = max(fBBoxRight,	 pLayoutObject->GetRight());
		fBBoxTop	  = max(fBBoxTop,	 pLayoutObject->GetTop());
		pLayoutSide   = pLayoutObject->GetLayoutSide();
		pLayoutObject = m_pParent->GetNextLayoutObject(pLayoutObject);
	}

	return pLayoutSide;
}

void CDlgLayoutObjectGeometry::UpdateHeadPositionControl()
{
	int nIDBitmap;
	switch (m_nHeadPosition)
	{
	case LEFT:	 nIDBitmap = IDB_PORTRAIT_90;  break;
	case BOTTOM: nIDBitmap = IDB_PORTRAIT_180; break;
	case RIGHT:	 nIDBitmap = IDB_PORTRAIT_270; break;
	case TOP:	 nIDBitmap = IDB_PORTRAIT;	   break;
	default:	 nIDBitmap = IDB_PORTRAIT;	   break;
	}
	if ((HBITMAP)m_bmMarkOrientation)
		m_bmMarkOrientation.DeleteObject();
	m_bmMarkOrientation.LoadBitmap(nIDBitmap); 
	((CStatic*)GetDlgItem(IDC_MARK_ORIENTATION))->SetBitmap((HBITMAP)m_bmMarkOrientation);
}

void CDlgLayoutObjectGeometry::UpdateLayoutObject(CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CLayoutObject* pCounterpartObject = (theApp.settings.m_bFrontRearParallel) ? pLayoutObject->FindCounterpartObject() : NULL;	

		pLayoutObject->m_LeftNeighbours.GetSize();			// To start an analyzing-process
		if (pCounterpartObject)
			pCounterpartObject->m_LeftNeighbours.GetSize();	// To start an analyzing-process for the other layoutside

		pLayoutObject->m_nHeadPosition = m_nHeadPosition;

		float fDiffX = (pLayoutObject->GetWidth()  - m_fObjectWidth)  / 2.0f;
		float fDiffY = (pLayoutObject->GetHeight() - m_fObjectHeight) / 2.0f;
		if ( (fabs(fDiffX) > 0.001f) || (fabs(fDiffY) > 0.001f) )
		{
			pLayoutObject->SetWidth(m_fObjectWidth);
			pLayoutObject->SetHeight(m_fObjectHeight);

			pLayoutObject->SetLeft  (pLayoutObject->GetLeft()	+ fDiffX, FALSE);
			pLayoutObject->SetRight (pLayoutObject->GetRight()	- fDiffX, FALSE);
			pLayoutObject->SetBottom(pLayoutObject->GetBottom()	+ fDiffY, FALSE);
			pLayoutObject->SetTop	(pLayoutObject->GetTop()	- fDiffY, FALSE);

			pLayoutObject->RecalcObject(pPrintSheet, LEFT);
			pLayoutObject->RecalcObject(pPrintSheet, RIGHT);
			pLayoutObject->RecalcObject(pPrintSheet, TOP);
			pLayoutObject->RecalcObject(pPrintSheet, BOTTOM);
		}

		float fObjectXRefline = pLayoutObject->GetLayoutObjectRefline(pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInX);
		float fObjectYRefline = pLayoutObject->GetLayoutObjectRefline(pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInY);
		CLayoutSide* pLayoutSide = pLayoutObject->GetLayoutSide();
		if (pLayoutSide)
		{
			float fSheetXRefline = pLayoutSide->GetRefline(pLayoutObject->m_reflinePosParams.m_nReferenceItemInX, pLayoutObject->m_reflinePosParams.m_nReferenceLineInX, &pLayoutObject->m_reflinePosParams.m_hReferenceObjectInX);
			float fSheetYRefline = pLayoutSide->GetRefline(pLayoutObject->m_reflinePosParams.m_nReferenceItemInY, pLayoutObject->m_reflinePosParams.m_nReferenceLineInY, &pLayoutObject->m_reflinePosParams.m_hReferenceObjectInY);
			fDiffX = pLayoutObject->m_reflinePosParams.m_fDistanceX - (fObjectXRefline - ( (fSheetXRefline != FLT_MAX) ? fSheetXRefline : 0.0f ));
			fDiffY = pLayoutObject->m_reflinePosParams.m_fDistanceY - (fObjectYRefline - ( (fSheetYRefline != FLT_MAX) ? fSheetYRefline : 0.0f ));
			pLayoutObject->Offset(fDiffX, fDiffY);
		}

		if (pCounterpartObject)
		{
			switch (pCounterpartObject->GetLayout()->KindOfProduction())
			{
			case WORK_AND_TURN  : pCounterpartObject->m_nHeadPosition = (m_nHeadPosition == LEFT) ? RIGHT  : (m_nHeadPosition == RIGHT)  ? LEFT : m_nHeadPosition; break;
        	case WORK_AND_TUMBLE: pCounterpartObject->m_nHeadPosition = (m_nHeadPosition == TOP)  ? BOTTOM : (m_nHeadPosition == BOTTOM) ? TOP  : m_nHeadPosition; break;
        	}

			if ( (fabs(fDiffX) > 0.001f) || (fabs(fDiffY) > 0.001f) )
			{
				pCounterpartObject->SetLeft  (pCounterpartObject->GetLeft()		+ fDiffX, FALSE);
				pCounterpartObject->SetRight (pCounterpartObject->GetRight()	- fDiffX, FALSE);
				pCounterpartObject->SetBottom(pCounterpartObject->GetBottom()	+ fDiffY, FALSE);
				pCounterpartObject->SetTop	 (pCounterpartObject->GetTop()		- fDiffY, FALSE);
				pCounterpartObject->RecalcObject(pPrintSheet, LEFT);
				pCounterpartObject->RecalcObject(pPrintSheet, RIGHT);
				pCounterpartObject->RecalcObject(pPrintSheet, TOP);
				pCounterpartObject->RecalcObject(pPrintSheet, BOTTOM);
			}

			pCounterpartObject->m_reflinePosParams.m_fDistanceX = m_fDistanceX;
			pCounterpartObject->m_reflinePosParams.m_fDistanceY = m_fDistanceY;
			m_markPivotCtrl.SaveData(pCounterpartObject->m_reflinePosParams.m_nObjectReferenceLineInX, pCounterpartObject->m_reflinePosParams.m_nObjectReferenceLineInY);

			pCounterpartObject->AlignToCounterPart(pLayoutObject);
		}

		if (pLayoutObject->GetLayout())
			pLayoutObject->GetLayout()->AnalyzeMarks();

/*
//		if (m_nMarkType == CMark::TextMark)
//		{
//			CPageTemplate* pMarkTemplate = pLayoutObject->GetCurrentMarkTemplate();
//			if (pMarkTemplate)
//				pMarkTemplate->SetMarkText(m_strOutputText);
//		}

		pDoc->SetModifiedFlag();

		theApp.OnUpdateView(RUNTIME_CLASS(CPageListView),		NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListDetailView), NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListTableView),	NULL, 0, NULL);
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),		NULL, 0, NULL);
*/
	}
}

void CDlgLayoutObjectGeometry::LoadData()
{
	if ( ! m_hWnd)
		return;

	int nReflinePosParamsID = GetReflinePosParamsID();

	float		   fBBoxLeft = FLT_MAX, fBBoxBottom = FLT_MAX, fBBoxRight = -FLT_MAX, fBBoxTop = -FLT_MAX;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	while (pLayoutObject)
	{
		m_fObjectWidth	= pLayoutObject->GetWidth();
		m_fObjectHeight	= pLayoutObject->GetHeight();
		m_nHeadPosition	= pLayoutObject->m_nHeadPosition;

		UpdateHeadPositionControl();

		if (pLayoutObject->m_reflinePosParams.IsUndefined())
			pLayoutObject->m_reflinePosParams.SetDefault(pLayoutObject);

		pLayoutObject->UpdateReflineDistances();

		m_fDistanceX = pLayoutObject->m_reflinePosParams.m_fDistanceX;
		m_fDistanceY = pLayoutObject->m_reflinePosParams.m_fDistanceY;
		m_markPivotCtrl.LoadData(pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInX, pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInY, HorizontalOnly(), VerticalOnly());

		CPageTemplate* pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																					 pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				if (pObjectSource)
					if (pObjectSource->m_nType == CPageSource::SystemCreated)
						for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
						{
							CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[i];
							rObjectSourceHeader.m_fBBoxRight = pLayoutObject->GetWidth();
							rObjectSourceHeader.m_fBBoxTop	 = pLayoutObject->GetHeight();
						}
			}
		}

		pLayoutObject = m_pParent->GetNextLayoutObject(pLayoutObject);
	}

	CMark* pMark = m_pParent->GetFirstMark();
	while (pMark)
	{
		if ( (pMark->m_nType == CMark::CustomMark) || (pMark->m_nType == CMark::TextMark) || (pMark->m_nType == CMark::BarcodeMark) || (pMark->m_nType == CMark::DrawingMark) )
		{
			m_fObjectWidth	= pMark->m_fMarkWidth;
			m_fObjectHeight	= pMark->m_fMarkHeight;
			m_nHeadPosition	= pMark->m_nHeadPosition;

			UpdateHeadPositionControl();

			CReflineMark*			pReflineMark	  = (CReflineMark*)pMark;
			CReflinePositionParams* pReflinePosParams = pReflineMark->GetReflinePosParams(nReflinePosParamsID);
			m_bVariableWidth  = pReflineMark->m_bVariableWidth; 
			m_bVariableHeight = pReflineMark->m_bVariableHeight; 

			if (pReflinePosParams)
			{
				m_fDistanceX  = pReflinePosParams->m_fDistanceX;
				m_fDistanceY  = pReflinePosParams->m_fDistanceY;
				m_nMarkAnchor = pReflinePosParams->m_nAnchor;

				m_markPivotCtrl.LoadData(pReflinePosParams->m_nObjectReferenceLineInX, pReflinePosParams->m_nObjectReferenceLineInY, HorizontalOnly(), VerticalOnly());
				m_anchorPivotCtrl.LoadData(pReflinePosParams->m_nReferenceLineInX, pReflinePosParams->m_nReferenceLineInY);
			}
		}

		pMark = m_pParent->GetNextMark(pMark);
	}

	UpdateData(FALSE);

	UpdatePositionControls();
	UpdateMarkAnchorSetting();
	UpdateMarkrefSelectors();
}

void CDlgLayoutObjectGeometry::SaveData()
{
	if ( ! m_hWnd)
		return;

	int nReflinePosParamsID = GetReflinePosParamsID();

	UpdateData(TRUE);

	CLayout*	   pLayout		 = NULL;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	while (pLayoutObject)
	{
		pLayout = pLayoutObject->GetLayout();

		pLayoutObject->m_reflinePosParams.m_fDistanceX = m_fDistanceX;
		pLayoutObject->m_reflinePosParams.m_fDistanceY = m_fDistanceY;
		m_markPivotCtrl.SaveData(pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInX, pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInY);

		UpdateLayoutObject(pLayoutObject, m_pParent->GetPrintSheet());

		CPageTemplate* pTemplate = (pLayoutObject->m_nType == CLayoutObject::Page) ? pLayoutObject->GetCurrentPageTemplate(m_pParent->m_pPrintSheet) :
																					 pLayoutObject->GetCurrentMarkTemplate();
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource = pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				if (pObjectSource)
					if (pObjectSource->m_nType == CPageSource::SystemCreated)
						for (int i = 0; i < pObjectSource->m_PageSourceHeaders.GetSize(); i++)
						{
							CPageSourceHeader& rObjectSourceHeader = pObjectSource->m_PageSourceHeaders[i];
							rObjectSourceHeader.m_fBBoxRight = pLayoutObject->GetWidth();
							rObjectSourceHeader.m_fBBoxTop	 = pLayoutObject->GetHeight();
						}
			}
		}

		pLayoutObject = m_pParent->GetNextLayoutObject(pLayoutObject);
	}
	if (pLayout)
	{
		pLayout->CheckForProductType();
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();	// because panorama pages could be arised or destroyed
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	while (pMark)
	{
		pMark->m_fMarkWidth    = m_fObjectWidth;
		pMark->m_fMarkHeight   = m_fObjectHeight;
		pMark->m_nHeadPosition = m_nHeadPosition;
		if (pMark->m_MarkSource.m_nType == CPageSource::SystemCreated)
			for (int i = 0; i < pMark->m_MarkSource.m_PageSourceHeaders.GetSize(); i++)
			{
				CPageSourceHeader& rObjectSourceHeader = pMark->m_MarkSource.m_PageSourceHeaders[0];
				rObjectSourceHeader.m_fBBoxRight = m_fObjectWidth;
				rObjectSourceHeader.m_fBBoxTop	 = m_fObjectHeight;
			}

		CReflineMark*			pReflineMark	  = (CReflineMark*)pMark;
		CReflinePositionParams* pReflinePosParams = pReflineMark->GetReflinePosParams(nReflinePosParamsID);
		pReflineMark->m_bVariableWidth  = m_bVariableWidth; 
		pReflineMark->m_bVariableHeight = m_bVariableHeight; 

		if (pReflinePosParams)
		{
			pReflinePosParams->m_fDistanceX = m_fDistanceX;
			pReflinePosParams->m_fDistanceY = m_fDistanceY;
			pReflinePosParams->m_nAnchor	= m_nMarkAnchor;

			m_markPivotCtrl.SaveData(pReflinePosParams->m_nObjectReferenceLineInX, pReflinePosParams->m_nObjectReferenceLineInY);
			m_anchorPivotCtrl.SaveData(pReflinePosParams->m_nReferenceLineInX, pReflinePosParams->m_nReferenceLineInY);
		}

		pMark = m_pParent->GetNextMark(pMark);
	}
}

void CDlgLayoutObjectGeometry::OnChangedPivot()
{
	if (m_pParent->GetFirstLayoutObject())
	{
		CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
		while (pLayoutObject)
		{
			m_markPivotCtrl.SaveData(pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInX, pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInY);
			pLayoutObject->UpdateReflineDistances();
			m_fDistanceX = pLayoutObject->m_reflinePosParams.m_fDistanceX;
			m_fDistanceY = pLayoutObject->m_reflinePosParams.m_fDistanceY;

			pLayoutObject = m_pParent->GetNextLayoutObject(pLayoutObject);
		}
	}

	if (m_pParent->GetFirstMark())
	{
		int nReflinePosParamsID = GetReflinePosParamsID();
	
		CMark* pMark = m_pParent->GetFirstMark();
		while (pMark)
		{
			CReflineMark*			pReflineMark	  = (CReflineMark*)pMark;
			CReflinePositionParams* pReflinePosParams = pReflineMark->GetReflinePosParams(nReflinePosParamsID);

			if (pReflinePosParams)
			{
				m_markPivotCtrl.SaveData(pReflinePosParams->m_nObjectReferenceLineInX, pReflinePosParams->m_nObjectReferenceLineInY);
				m_anchorPivotCtrl.SaveData(pReflinePosParams->m_nReferenceLineInX, pReflinePosParams->m_nReferenceLineInY);
			}

			pMark = m_pParent->GetNextMark(pMark);
		}
	}

	UpdateData(FALSE);

	m_pParent->UpdateView();
}

void CDlgLayoutObjectGeometry::Apply() 
{
	if (m_hWnd)
		if (IsWindowVisible()) 
		{
			SaveData();
			CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
			if (pNavigationView)
			{
				CLayout* pLayout = (m_pParent->GetFirstLayoutObject()) ? m_pParent->GetFirstLayoutObject()->GetLayout() : NULL;
				pNavigationView->UpdatePrintSheets(pLayout);
			}
		}
}

void CDlgLayoutObjectGeometry::OnLayoutDistanceChanged() 
{
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
		UpdateLayoutObject(pLayoutObject, m_pParent->GetPrintSheet());
	LoadData();
	m_pParent->UpdateView();
}

void CDlgLayoutObjectGeometry::OnLayoutReflineChanged() 
{
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	while (pLayoutObject)
	{
		float fObjectXRefline = pLayoutObject->GetLayoutObjectRefline(pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInX);
		float fObjectYRefline = pLayoutObject->GetLayoutObjectRefline(pLayoutObject->m_reflinePosParams.m_nObjectReferenceLineInY);
		CLayoutSide* pLayoutSide = pLayoutObject->GetLayoutSide();
		if (pLayoutSide)
		{
			float fSheetXRefline = pLayoutSide->GetRefline(pLayoutObject->m_reflinePosParams.m_nReferenceItemInX, pLayoutObject->m_reflinePosParams.m_nReferenceLineInX, &pLayoutObject->m_reflinePosParams.m_hReferenceObjectInX);
			float fSheetYRefline = pLayoutSide->GetRefline(pLayoutObject->m_reflinePosParams.m_nReferenceItemInY, pLayoutObject->m_reflinePosParams.m_nReferenceLineInY, &pLayoutObject->m_reflinePosParams.m_hReferenceObjectInY);
			pLayoutObject->m_reflinePosParams.m_fDistanceX = fObjectXRefline - ( (fSheetXRefline != FLT_MAX) ? fSheetXRefline : 0.0f );
			pLayoutObject->m_reflinePosParams.m_fDistanceY = fObjectYRefline - ( (fSheetYRefline != FLT_MAX) ? fSheetYRefline : 0.0f );
		}

		pLayoutObject = m_pParent->GetNextLayoutObject(pLayoutObject);
	}

	LoadData();
}
