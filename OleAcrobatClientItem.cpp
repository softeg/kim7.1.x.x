// OleAcrobatClientItem.cpp: Implementierung der Klasse COleAcrobatClientItem.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OleAcrobatClientItem.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

COleAcrobatClientItem::COleAcrobatClientItem(COleDocument* pContainer) : COleClientItem(pContainer)
{

}

COleAcrobatClientItem::~COleAcrobatClientItem()
{

}

