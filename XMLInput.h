#pragma once

#define PROJ_JDFTOOLSLIB
#include <jdf/util/PlatformUtils.h>
#include "JDF.h"
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>

using namespace std;
using namespace JDF;
using namespace XERCES_CPP_NAMESPACE;


class CXMLInput
{
public:
	CXMLInput(void);
	~CXMLInput(void);


public:
	JDFParser*			parser;
	JDFDoc*				m_pJDFDoc;	
	CPageSource*		m_pPageSource;
	CProductionSetup	m_productionSetup;
	CProductionProfile	m_defaultProductionProfile;
	BOOL				m_bUserNotified_PressDeviceNotDefined;
	BOOL				m_bPreImpose;


public:
	BOOL 				ParseXML(const CString& strFilePath, BOOL bPreImpose = TRUE, CProductionProfile defaultProductionProfile = CProductionProfile());
	BOOL 				ProcessJobNode(DOMNode* jobNode);
	BOOL 				ProcessJobDataSectionNode(DOMNode* jobDataSectionNode);
	BOOL 				ProcessOrderNode(DOMNode* orderNode);
	BOOL 				ProcessCustomerNode(DOMNode* customerNode);
	BOOL 				ProcessResourceSectionNode(DOMNode* resourceSectionNode);
	BOOL 				ProcessPaperResourceNode(DOMNode* paperNode);
	BOOL 				ProcessPrepressNode(DOMNode* prepressProcessNode);
	BOOL 				ProcessOutputProfileNode(DOMNode* outputProfileNode, CString& strOutputProfile);
	BOOL 				ProcessPrintingNode(DOMNode* printingProcessNode);
	BOOL 				ProcessPrintingDeviceNode(DOMNode* printingProcessDeviceNode, CPressParams& rPrintingProcess);
	BOOL 				ProcessFoldingNode(DOMNode* foldingProcessNode);
	BOOL 				ProcessFoldingSchemePoolNode(DOMNode* foldingProcessSchemePoolNode, CFoldingProcess& rFoldingProcess);
	BOOL 				ProcessFoldingShinglingNode(DOMNode* foldingProcessShinglingNode, CFoldingProcess& rFoldingProcess);
	BOOL 				ProcessBindingNode(DOMNode* bindingProcessNode);
	BOOL 				ProcessBindingCollectingNode(DOMNode* bindingProcessCollectingNode, CBindingProcess& rBindingProcess);
	BOOL 				ProcessBindingGatheringNode(DOMNode* bindingProcessGatheringNode, CBindingProcess& rBindingProcess);
	BOOL 				ProcessTrimmingNode(DOMNode* trimmingProcessNode);
	BOOL 				ProcessImpositionNode(DOMNode* impositionProcessNode);
	BOOL 				ProcessProductSectionNode(DOMNode* productSectionNode);
	BOOL 				ProcessProductNode(DOMNode* productNode);
	BOOL 				ProcessBoundProductNode(DOMNode* boundProductNode, CBoundProduct& rBoundProduct);
	BOOL 				ProcessBoundProductLayoutNode(DOMNode* boundProductLayoutNode, CBoundProduct& rBoundProduct);
	BOOL 				ProcessBoundProductQuantityNode(DOMNode* boundProductQuantityNode, CBoundProduct& rBoundProduct);
	BOOL 				ProcessBoundProductDeliveryNode(DOMNode* boundProductDeliveryNode, CBoundProduct& rBoundProduct);
	BOOL 				ProcessBoundProductContentNode(DOMNode* boundProductContentNode, CBoundProduct& rBoundProduct);
	BOOL 				ProcessBoundProductPartNode(DOMNode* boundProductPartNode, CBoundProduct& rBoundProduct);
	BOOL 				ProcessBoundProductBindingInfoNode(DOMNode* boundProductBindingInfoNode, CBoundProduct& rBoundProduct);
	BOOL 				ProcessBoundProductPartLayoutNode(DOMNode* boundProductPartLayoutNode, CBoundProductPart& rBoundProductPart);
	BOOL 				ProcessFlatProductNode(DOMNode* flatProductNode, CFlatProduct& rFlatProduct);
	BOOL 				ProcessFlatProductLayoutNode(DOMNode* flatProductLayoutNode, CFlatProduct& rFlatProduct);
	BOOL 				ProcessBoundProductPartColorIntentsNode(DOMNode* boundProductPartColorIntentsNode, CBoundProductPart& rBoundProductPart);
	BOOL 				ProcessFlatProductQuantityNode(DOMNode* flatProductQuantityNode, CFlatProduct& rFlatProduct);
	BOOL 				ProcessFlatProductDeliveryNode(DOMNode* flatProductDeliveryNode, CFlatProduct& rFlatProduct);
	BOOL 				ProcessFlatProductContentNode(DOMNode* flatProductContentNode, CFlatProduct& rFlatProduct);
	BOOL 				ProcessFlatProductColorIntentsNode(DOMNode* flatProductColorIntentsNode, CFlatProduct& rFlatProduct);
	BOOL 				ProcessStrippingSectionNode(DOMNode* strippingSectionNode);
	BOOL 				ProcessPrintSheetNode(DOMNode* printSheetNode);
	BOOL 				ProcessPaperNode(DOMNode* paperNode, CPrintSheet* pPrintSheet);
	BOOL 				ProcessMarksetNode(DOMNode* marksetNode, CPrintSheet* pPrintSheet);
	BOOL 				ProcessFoldSheetNode(DOMNode* foldSheetNode, CPrintSheet* pPrintSheet);
	void 				AutoImposeFoldSheets(CBoundProduct& rProduct);
	void 				PrintSheetGetFoldSheetPlacement(CLayout* pLayout, float fGrossWidth, float fGrossHeight, const CString strSector, const CString strSubSector, const CString strOrientation, float& fFoldSheetPosX, float& fFoldSheetPosY);
	BOOL 				PrintSheetPlaceFoldSheetLayer(CLayout* pLayout, CFoldSheet* pFoldSheet, int nLayerIndex, float fFoldSheetPosX, float fFoldSheetPosY, int nOrientation, CBoundProductPart& rProductPart, int nSpreadAxis = 0, float fSpreadGap = 0.0f, int nSpreadIndex = -1);
	BOOL 				ProcessFlatComponentNode(DOMNode* flatComponentNode, CPrintSheet* pPrintSheet);
	void 				PrintSheetGetFlatComponentPlacement(CLayout* pLayout, float fGrossWidth, float fGrossHeight, const CString strSector, const CString strSubSector, const CString strOrientation, float& fFlatPosX, float& fFlatPosY);
	BOOL 				PrintSheetPlaceFlatComponent(CPrintSheet* pPrintSheet, CFlatProduct& rFlatProduct, float fFlatPosX, float fFlatPosY, int nOrientation);
	int	 				ReplicatePDFFlats(CFlatProduct& rMasterProduct, CPageSource* pNewObjectSource, int nNumPDFPages, int nSides);
	BOOL 				AnyContentFileExisting();
	CPaperDefs			GetPaperResource  (const CString& strID);
	CPrepressParams		GetPrepressProcess(const CString& strID);
	CPressParams		GetPressProcess	  (const CString& strID);
	CCuttingProcess		GetCuttingProcess (const CString& strID);
	CFoldingProcess		GetFoldingProcess (const CString& strID);
	CBindingProcess		GetBindingProcess (const CString& strID);
	CTrimmingProcess	GetTrimmingProcess(const CString& strID);
};
