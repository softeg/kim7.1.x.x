
#pragma once


#include "TrueColorToolBar.h"



class COrderDataSplitter : public CSplitterWnd
{
    DECLARE_DYNAMIC(COrderDataSplitter)
public:
	COrderDataSplitter() { }

friend class COrderDataFrame;

protected:
	virtual void OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect);
	virtual void StopTracking(BOOL bAccept);
	virtual void TrackColumnSize(int x, int col);

    DECLARE_MESSAGE_MAP()
    afx_msg LRESULT OnNcHitTest(CPoint point);
	void OnSize(UINT nType, int cx, int cy);
};




// OrderDataFrame.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Rahmen COrderDataFrame 

class COrderDataFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(COrderDataFrame)
protected:
	COrderDataFrame();           // Dynamische Erstellung verwendet geschützten Konstruktor


public:
	enum { ShowProducts = 1 };

public:
	BOOL			   m_bUndoActive;
	COrderDataSplitter m_wndSplitter, m_wndSplitter1, m_wndSplitter2, m_wndSplitter3;
	struct s_browserInfo
	{
		int	m_nShowInfo;
		int m_nProductsViewHeight;
	}m_browserInfo;

protected:
	BOOL m_bStatusRestored;


// Attribute
public:

// Operationen
public:
	void								RearrangeSplitters();
	void								SetupViewProducts();
	class CProductsView*				GetProductsView();
	class COrderDataView*				GetOrderDataView();
	class CPrintComponentsView*			GetPrintComponentsView();
	class CPrintSheetPlanningView*		GetPrintSheetPlanningView();
	class COrderDataFrameLeftToolsView* GetOrderDataFrameLeftToolsView();


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(COrderDataFrame)
	//}}AFX_VIRTUAL

// Implementierung
protected:
	virtual ~COrderDataFrame();

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(COrderDataFrame)
		// HINWEIS - Der Klassen-Assistent fügt hier Member-Funktionen ein und entfernt diese.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
};

/////////////////////////////////////////////////////////////////////////////

