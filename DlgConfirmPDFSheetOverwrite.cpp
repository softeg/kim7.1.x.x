// DlgConfirmPDFSheetOverwrite.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgConfirmPDFSheetOverwrite.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgConfirmPDFSheetOverwrite 


CDlgConfirmPDFSheetOverwrite::CDlgConfirmPDFSheetOverwrite(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgConfirmPDFSheetOverwrite::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgConfirmPDFSheetOverwrite)
	m_strPDFFilename = _T("");
	//}}AFX_DATA_INIT
}


void CDlgConfirmPDFSheetOverwrite::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgConfirmPDFSheetOverwrite)
	DDX_Text(pDX, IDC_PDFSHEET_NAME, m_strPDFFilename);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgConfirmPDFSheetOverwrite, CDialog)
	//{{AFX_MSG_MAP(CDlgConfirmPDFSheetOverwrite)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_USE_EXISTING, OnUseExisting)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgConfirmPDFSheetOverwrite 

BOOL CDlgConfirmPDFSheetOverwrite::OnInitDialog() 
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_PDFSHEET_NAME)->ModifyStyle(0, SS_PATHELLIPSIS);

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}


void CDlgConfirmPDFSheetOverwrite::OnNew() 
{
	EndDialog(IDC_NEW);
}

void CDlgConfirmPDFSheetOverwrite::OnUseExisting() 
{
	EndDialog(IDC_USE_EXISTING);
}
