#pragma once

#include "MyBitmapButton.h"


#define ID_MAGNIFY_SHEET 1234
#define ID_GOTO_DETAILS  ID_MAGNIFY_SHEET + 1


class CPrintSheetInfo
{
public:
	CPrintSheet* m_pPrintSheet;
	CRect		 m_rcDisplay;
};



// CPrintSheetPlanningView-Ansicht

class CPrintSheetPlanningView : public CScrollView
{
	DECLARE_DYNCREATE(CPrintSheetPlanningView)

protected:
	CPrintSheetPlanningView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetPlanningView();

public:
	DECLARE_DISPLAY_LIST(CPrintSheetPlanningView, OnMultiSelect, YES, OnStatusChanged, YES)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, FoldSheetComponent, OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, UnassignedPages, OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, FoldSheet, OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, YES, OnDropped, NO, OnMouseMove, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, LabelComponent, OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, UnassignedLabel, OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PrintSheetDetails, OnSelect, YES, OnDeselect, NO, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, SheetFrame, OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, ControlMarks, OnSelect, YES, OnDeselect, NO, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, IsSelectable, TRUE)

	CSize										m_docSize;
	COleDropTarget								m_dropTarget;
	CRect										m_catchRect;
	CPoint										m_ptDragIcon;
	HICON										m_hDragIcon;
	CBitmap										m_bmDragBackground;
	CRect										m_rectDragIcon;
	CString										m_strDragIcon;
	CMyBitmapButton								m_magnifyButton, m_gotoDetailsButton;
	int											m_nProductPartIndex;
	CArray <CPrintSheetInfo, CPrintSheetInfo&>	m_printSheetInfos;
	int											m_nLastSelectedFoldSheetComponentIndex;


public:
	class COrderDataFrame*		GetOrderDataFrame();
	CSize						CalcDocSize();
	int							GetSelProductPartIndex();
	CFoldSheet* 				GetSelFoldSheet ();
	int							GetSelLayerIndex();
	CLabelListEntry*			GetSelLabelListEntry();
	CPrintSheet*				GetSelPrintSheet();
	CLayout*					GetSelLayout();
	CPressDevice*				GetSelPressDevice();
	int							GetComponentIndex(CDisplayItem& rDI);
	int							GetPrintSheetIndex(CDisplayItem& rDI);
	int							GetSelectedComponentIndex();
	BOOL						IsSelectedComponentIndex(int nComponentIndex);
	void						SelectComponentByIndex(int nSelIndex);
	void						SelectComponentByFoldSheet(CFoldSheet* pFoldSheet, BOOL bScrollTo = TRUE);
	int							GetSelectedPrintSheetIndex();
	BOOL						IsSelectedPrintSheetIndex(int nPrintSheetIndex);
	void						SaveBitmap	 (CBitmap* pBitmap, const CRect& rect);
	void						RestoreBitmap(CBitmap* pBitmap, const CRect& rect);
	DROPEFFECT					OnDragOverFoldSheet(CDisplayItem& di, DWORD dwKeyState, CPoint point);
	BOOL						OnDropFoldSheet	   (CDisplayItem& di, DROPEFFECT dropEffect, CPoint point);
	void						UndoSave();
	void						UndoRestore();
	CFoldSheet*					GetFoldSheet();
	int							GetLayerIndex();
	CPrintSheet*				GetPrintSheet();
	void						HandleOnSelectComponent();
	void						HandleOnSelectPrintSheet();
	BOOL						IsOpenPageNumeration();
	BOOL						IsOpenFoldSheetNumeration();
	BOOL						IsOpenPrintSheetNumeration();
	BOOL						IsInsideNumerationRange(int nComponentIndex);


public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	virtual void OnInitialUpdate();     // Erster Aufruf nach Erstellung
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
protected:

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
protected:
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	virtual void OnDragLeave();
	virtual DROPEFFECT OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnMagnifySheet();
	afx_msg void OnGotoDetails();
	afx_msg void OnNcMouseMove(UINT nHitTest, CPoint point);
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	afx_msg void OnNcPaint();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


