// DlgPDFTargetProps.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgJobColorDefinitions.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgDirPicker.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#ifdef PDF
#include "PDFEngineInterface.h"
#include "PDFOutput.h"
#endif
#include "PageListFrame.h"
#include "DlgPlaceholderSyntaxInfo.h"
#include "DlgJDFOutputDetails.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


int SFW[] = {0,6,7};
int SFH[] = {0,3,6};
int NEWSW[] = {0,4,9};
int NEWSH[] = {0,4,6};
int DIGITALW[] = {0,6,5};
int DIGITALH[] = {0,8,4};



/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFTargetProps 


CDlgPDFTargetProps::CDlgPDFTargetProps(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPDFTargetProps::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPDFTargetProps)
	m_fMediaHeight = 0.0f;
	m_fMediaWidth = 0.0f;
	m_strPDFTargetName = _T("");
	m_strFolder = _T("");
	m_strExposer = _T("");
	m_bMarkFrameCheck = FALSE;
	m_bPageFrameCheck = FALSE;
	m_bPlateFrameCheck = FALSE;
	m_bSheetFrameCheck = FALSE;
	m_bShrinkToFit = FALSE;
	m_bUsePrinterHalftones = FALSE;
	m_fMediaDistanceX = 0.0f;
	m_fMediaDistanceY = 0.0f;
	m_fMediaCorrectionX = 0.0f;
	m_fMediaCorrectionY = 0.0f;
	m_strMediaName = _T("");
	m_strOutputFilename = _T("");
	m_nJDFCompatibility = -1;
	m_bJDFMarksInFolder = FALSE;
	m_strJDFMarksFolder = _T("");
	m_bJDFOutputMime = FALSE;
	m_bOutputCuttingData = FALSE;
	m_strCuttingDataFolder = _T("");
	m_strCuttingDataFilename = _T("");
	m_nPlaceholderMessageSource = IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER;

	m_bBoxShapeCheck = FALSE;
	m_nTargetPathSide = -1;
	m_nAutoAlignTo = CMediaSize::AlignNone;
	m_nBookblockDoubleSingle = -1;
	m_nOutputTarget = CPDFTargetList::PrintSheetTarget;

	//}}AFX_DATA_INIT

	m_nMediaRotation = 0;
	m_pToolTip = NULL;
	m_pWndMsgTarget = NULL;
}

CDlgPDFTargetProps::~CDlgPDFTargetProps()
{
	delete m_pToolTip;
}

void CDlgPDFTargetProps::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPDFTargetProps)
	DDX_Control(pDX, IDC_MEDIANAME_COMBO, m_mediaNameCombo);
	DDX_Control(pDX, IDC_MEDIA_CORRECTY_SPIN, m_correctionYSpin);
	DDX_Control(pDX, IDC_MEDIA_CORRECTX_SPIN, m_correctionXSpin);
	DDX_Control(pDX, IDC_DISTANCEY_SPIN, m_distanceYSpin);
	DDX_Control(pDX, IDC_DISTANCEX_SPIN, m_distanceXSpin);
	DDX_Control(pDX, IDC_EXPOSER_COMBO, m_exposerCombo);
	DDX_Control(pDX, IDC_WIDTH_SPIN, m_widthSpin);
	DDX_Control(pDX, IDC_HEIGHT_SPIN, m_heightSpin);
	DDX_Measure(pDX, IDC_MEDIA_HEIGHT, m_fMediaHeight);
	DDX_Measure(pDX, IDC_MEDIA_WIDTH, m_fMediaWidth);
	DDX_Text(pDX, IDC_PDFTARGET_NAME, m_strPDFTargetName);
	DDX_Text(pDX, IDC_FOLDER, m_strFolder);
	DDX_CBStringExact(pDX, IDC_EXPOSER_COMBO, m_strExposer);
	DDX_Check(pDX, IDC_MARKFRAME_CHECK, m_bMarkFrameCheck);
	DDX_Check(pDX, IDC_PAGEFRAME_CHECK, m_bPageFrameCheck);
	DDX_Check(pDX, IDC_PLATEFRAME_CHECK, m_bPlateFrameCheck);
	DDX_Check(pDX, IDC_SHEETFRAME_CHECK, m_bSheetFrameCheck);
	DDX_Check(pDX, IDC_SHRINKTO_FIT, m_bShrinkToFit);
	DDX_Check(pDX, IDC_USE_PRINTER_HALFTONES, m_bUsePrinterHalftones);
	DDX_Measure(pDX, IDC_MEDIA_DISTANCEX, m_fMediaDistanceX);
	DDX_Measure(pDX, IDC_MEDIA_DISTANCEY, m_fMediaDistanceY);
	DDX_Measure(pDX, IDC_MEDIA_CORRECTX, m_fMediaCorrectionX);
	DDX_Measure(pDX, IDC_MEDIA_CORRECTY, m_fMediaCorrectionY);
	DDX_CBString(pDX, IDC_MEDIANAME_COMBO, m_strMediaName);
	DDX_Text(pDX, IDC_OUTPUT_FILENAME, m_strOutputFilename);
	DDX_Check(pDX, IDC_BOXSHAPE_CHECK, m_bBoxShapeCheck);
	DDX_Radio(pDX, IDC_TARGETPATH_FRONT, m_nTargetPathSide);
	DDX_CBIndex(pDX, IDC_JDF_COMPATIBILITY, m_nJDFCompatibility);
	DDX_Check(pDX, IDC_TP_MARKSINFOLDER_CHECK, m_bJDFMarksInFolder);
	DDX_Text(pDX, IDC_TP_MARKSINFOLDER_EDIT, m_strJDFMarksFolder);
	DDX_Check(pDX, IDC_OUTPUT_MIME_CHECK, m_bJDFOutputMime);
	DDX_Radio(pDX, IDC_BOOKBLOCK_DOUBLEPAGE, m_nBookblockDoubleSingle);
	DDX_Check(pDX, IDC_CUTTINGDATA_CHECK, m_bOutputCuttingData);
	DDX_Text(pDX, IDC_CUTTINGDATA_FOLDER, m_strCuttingDataFolder);
	DDX_Text(pDX, IDC_CUTTINGDATA_FILENAME, m_strCuttingDataFilename);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPDFTargetProps, CDialog)
	//{{AFX_MSG_MAP(CDlgPDFTargetProps)
	ON_BN_CLICKED(IDC_BROWSE_FOLDER, OnBrowseFolder)
	ON_BN_CLICKED(IDC_BROWSE_MARKFILE_FOLDER, OnBrowseMarkfileFolder)
	ON_NOTIFY(UDN_DELTAPOS, IDC_HEIGHT_SPIN, OnDeltaposHeightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_WIDTH_SPIN, OnDeltaposWidthSpin)
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_EXPOSER_COMBO, OnSelchangeExposerCombo)
	ON_BN_CLICKED(IDC_ADD_TILE, OnAddTile)
	ON_BN_CLICKED(IDC_DELETE_TILE, OnDeleteTile)
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	ON_EN_CHANGE(IDC_MEDIA_HEIGHT, OnChangeMediaHeight)
	ON_EN_CHANGE(IDC_MEDIA_WIDTH, OnChangeMediaWidth)
	ON_EN_CHANGE(IDC_PDFTARGET_NAME, OnChangePdftargetName)
	ON_NOTIFY(UDN_DELTAPOS, IDC_TILE_SPIN, OnDeltaposTileSpin)
	ON_EN_CHANGE(IDC_OUTPUT_FILENAME, OnChangeOutputFilename)
	ON_NOTIFY(UDN_DELTAPOS, IDC_MEDIA_ORIENTATION_SPIN, OnDeltaposMediaOrientationSpin)
	ON_WM_MOUSEMOVE()
	ON_NOTIFY(UDN_DELTAPOS, IDC_DISTANCEX_SPIN, OnDeltaposDistancexSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_DISTANCEY_SPIN, OnDeltaposDistanceySpin)
	ON_EN_CHANGE(IDC_MEDIA_DISTANCEX, OnChangeMediaDistancex)
	ON_EN_CHANGE(IDC_MEDIA_DISTANCEY, OnChangeMediaDistancey)
	ON_EN_CHANGE(IDC_MEDIA_CORRECTX, OnChangeMediaCorrectx)
	ON_EN_CHANGE(IDC_MEDIA_CORRECTY, OnChangeMediaCorrecty)
	ON_NOTIFY(UDN_DELTAPOS, IDC_MEDIA_CORRECTX_SPIN, OnDeltaposMediaCorrectxSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_MEDIA_CORRECTY_SPIN, OnDeltaposMediaCorrectySpin)
	ON_BN_CLICKED(IDC_TEST_OUTPUT, OnTestOutput)
	ON_BN_CLICKED(IDC_LAUNCH_ACROBAT, OnLaunchAcrobat)
	ON_BN_CLICKED(IDC_SHRINKTO_FIT, OnShrinktoFit)
	ON_BN_CLICKED(IDC_OUTPUTFRAME_COLORANTS, OnOutputframeColorants)
	ON_BN_CLICKED(IDC_REMOVE_FORMAT, OnRemoveFormat)
	ON_BN_CLICKED(IDC_ADD_FORMAT, OnAddFormat)
	ON_CBN_SELCHANGE(IDC_MEDIANAME_COMBO, OnSelchangeMedianameCombo)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_PATH_PLACEHOLDER, OnButtonSelectPathPlaceholder)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER, OnButtonSelectFilenamePlaceholder)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSHEETNUM,			OnPlaceholderFoldsheetNum)
	ON_COMMAND(ID_PLACEHOLDER_SHEETSIDE,			OnPlaceholderSheetside)
	ON_COMMAND(ID_PLACEHOLDER_WEBNUM,				OnPlaceholderWebNum)
	ON_COMMAND(ID_PLACEHOLDER_COLORNUM,				OnPlaceholderColorNum)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSHEETNUM_CG,		OnPlaceholderFoldsheetNumCG)
	ON_COMMAND(ID_PLACEHOLDER_JOBTITLE,				OnPlaceholderJobTitle)
	ON_COMMAND(ID_PLACEHOLDER_TILENUM,				OnPlaceholderTileNum)
	ON_COMMAND(ID_PLACEHOLDER_SHEETNUM,				OnPlaceholderSheetNum)
	ON_COMMAND(ID_PLACEHOLDER_FILE,					OnPlaceholderFile)
	ON_COMMAND(ID_PLACEHOLDER_LOWPAGE_SIDE,			OnPlaceholderLowpageSide)
	ON_COMMAND(ID_PLACEHOLDER_LOWPAGE_SHEET,		OnPlaceholderLowpageSheet)
	ON_COMMAND(ID_PLACEHOLDER_HIGHPAGE_SIDE,		OnPlaceholderHighpageSide)
	ON_COMMAND(ID_PLACEHOLDER_HIGHPAGE_SHEET,		OnPlaceholderHighpageSheet)
	ON_COMMAND(ID_PLACEHOLDER_PRESSDEVICE,			OnPlaceholderPressDevice)
	ON_COMMAND(ID_PLACEHOLDER_JOB,					OnPlaceholderJob)
	ON_COMMAND(ID_PLACEHOLDER_JOBNUM,				OnPlaceholderJobNum)
	ON_COMMAND(ID_PLACEHOLDER_CUSTOMER,				OnPlaceholderCustomer)
	ON_COMMAND(ID_PLACEHOLDER_CUSTOMERNUM,			OnPlaceholderCustomerNum)
	ON_COMMAND(ID_PLACEHOLDER_CREATOR,				OnPlaceholderCreator)
	ON_COMMAND(ID_PLACEHOLDER_CREATIONDATE,			OnPlaceholderCreationDate)
	ON_COMMAND(ID_PLACEHOLDER_LASTMODIFIED,			OnPlaceholderLastModified)
	ON_COMMAND(ID_PLACEHOLDER_SCHEDULEDTOPRINT,		OnPlaceholderScheduledToPrint)
	ON_COMMAND(ID_PLACEHOLDER_OUTPUTDATE,			OnPlaceholderOutputDate)
	ON_COMMAND(ID_PLACEHOLDER_NOTES,				OnPlaceholderNotes)
	ON_COMMAND(ID_PLACEHOLDER_COLORNAME,			OnPlaceholderColorName)
	ON_COMMAND(ID_PLACEHOLDER_PAGENUMLIST,			OnPlaceholderPageNumList)
	ON_COMMAND(ID_PLACEHOLDER_PAGENUM,				OnPlaceholderPageNum)
	ON_COMMAND(ID_PLACEHOLDER_PRODUCTNAME,			OnPlaceholderProductName)
	ON_COMMAND(ID_PLACEHOLDER_PRODUCTPARTNAME,		OnPlaceholderProductPartName)
	ON_COMMAND(ID_PLACEHOLDER_PAPERGRAIN,			OnPlaceholderPaperGrain)
	ON_COMMAND(ID_PLACEHOLDER_PAPERTYPE,			OnPlaceholderPaperType)
	ON_COMMAND(ID_PLACEHOLDER_PAPERGRAMMAGE,		OnPlaceholderPaperGrammage)
	ON_COMMAND(ID_PLACEHOLDER_WORKSTYLE,			OnPlaceholderWorkStyle)
	ON_COMMAND(ID_PLACEHOLDER_FOLDSCHEME,			OnPlaceholderFoldScheme)
	ON_EN_KILLFOCUS(IDC_MEDIA_HEIGHT, OnKillfocusMediaHeight)
	ON_EN_KILLFOCUS(IDC_MEDIA_WIDTH, OnKillfocusMediaWidth)
	ON_BN_CLICKED(IDC_ALIGN2PLATE, OnAlign2Plate)
	ON_BN_CLICKED(IDC_ALIGN2SHEET, OnAlign2Sheet)
	ON_BN_CLICKED(IDC_TARGETPATH_FRONT, OnTargetpathFront)
	ON_BN_CLICKED(IDC_TARGETPATH_BACK, OnTargetpathBack)
	ON_BN_CLICKED(IDC_BUTTON_FILENAME_PLACEHOLDER_INFO, OnBnClickedButtonFilenamePlaceholderInfo)
	ON_BN_CLICKED(IDC_TP_MARKSINFOLDER_CHECK, OnMaskInFolderCheck)
	ON_BN_CLICKED(IDC_OUTPUT_MIME_CHECK, OnOutputMimeCheck)
	ON_BN_CLICKED(IDC_JDF_OUTPUT_DETAILS, OnJDFOutputDetails)
	ON_BN_CLICKED(IDC_BOOKBLOCK_DOUBLEPAGE, OnBookblockDoublePage)
	ON_BN_CLICKED(IDC_BOOKBLOCK_SINGLEPAGE, OnBookblockSinglePage)
	ON_BN_CLICKED(IDC_CUTTINGDATA_BROWSEFOLDER, OnCuttingDataBrowseFolder)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER, OnButtonSelectCuttingDataFilenamePlaceholder)
	ON_BN_CLICKED(IDC_BUTTON_CUTTINGDATA_FILENAME_PLACEHOLDER_INFO, OnBnClickedButtonCuttingDataFilenamePlaceholderInfo)
	//}}AFX_MSG_MAP
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNeedText )
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPDFTargetProps 

BOOL CDlgPDFTargetProps::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_buttonSelectPathPlaceholder.SubclassDlgItem(IDC_BUTTON_SELECT_PATH_PLACEHOLDER, this);
	m_buttonSelectPathPlaceholder.SetTheme(_T("COMBOBOX"), CP_DROPDOWNBUTTON, XCENTER);
	m_buttonSelectPathPlaceholder.SetBitmap(IDB_DROPDOWN, IDB_DROPDOWN, -1, XCENTER);

	m_buttonSelectFileNamePlaceholder.SubclassDlgItem(IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER, this);
	m_buttonSelectFileNamePlaceholder.SetTheme(_T("COMBOBOX"), CP_DROPDOWNBUTTON, XCENTER);
	m_buttonSelectFileNamePlaceholder.SetBitmap(IDB_DROPDOWN, IDB_DROPDOWN, -1, XCENTER);

	m_buttonSelectCuttingDataFilenamePlaceholder.SubclassDlgItem(IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER, this);
	m_buttonSelectCuttingDataFilenamePlaceholder.SetTheme(_T("COMBOBOX"), CP_DROPDOWNBUTTON, XCENTER);
	m_buttonSelectCuttingDataFilenamePlaceholder.SetBitmap(IDB_DROPDOWN, IDB_DROPDOWN, -1, XCENTER);

	if ( ! m_placeholderMenu.m_hMenu)
	{
		switch (m_nOutputTarget)
		{
		case CPDFTargetList::PrintSheetTarget:	VERIFY(m_placeholderMenu.LoadMenu(IDR_PLACEHOLDER_POPUP));				break;
		case CPDFTargetList::FoldSheetTarget:	VERIFY(m_placeholderMenu.LoadMenu(IDR_PLACEHOLDER_BOOKBLOCKOUT_POPUP));	break;
		case CPDFTargetList::BookletTarget:		VERIFY(m_placeholderMenu.LoadMenu(IDR_PLACEHOLDER_BOOKBLOCKOUT_POPUP));	break;
		}
	}

	if ( ! m_cuttingDataPlaceholderMenu.m_hMenu)
		VERIFY(m_cuttingDataPlaceholderMenu.LoadMenu(IDR_PLACEHOLDER_CUTTINGDATAOUT_POPUP));

	VERIFY(m_pathPlaceholderEdit.SubclassDlgItem(IDC_FOLDER, this));
	m_pathPlaceholderEdit.m_pParent			 = this;
	m_pathPlaceholderEdit.m_pPlaceholderMenu = &m_placeholderMenu;
	m_pathPlaceholderEdit.m_nIDButton		 = IDC_BUTTON_SELECT_PATH_PLACEHOLDER;
	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_PATH_PLACEHOLDER))->SetIcon(theApp.LoadIcon(IDI_DROPDOWN_ARROW));
	m_pathPlaceholderEdit.m_pfnOnChangeHandler = NULL;

	VERIFY(m_fileNamePlaceholderEdit.SubclassDlgItem(IDC_OUTPUT_FILENAME, this));
	m_fileNamePlaceholderEdit.m_pParent			 = this;
	m_fileNamePlaceholderEdit.m_pPlaceholderMenu = &m_placeholderMenu;
	m_fileNamePlaceholderEdit.m_nIDButton		 = IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER;
	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER))->SetIcon(theApp.LoadIcon(IDI_DROPDOWN_ARROW));
	m_fileNamePlaceholderEdit.m_pfnOnChangeHandler = CheckTargetPathSideButtons;

	VERIFY(m_cuttingDataPlaceholderEdit.SubclassDlgItem(IDC_CUTTINGDATA_FILENAME, this));
	m_cuttingDataPlaceholderEdit.m_pParent			  = this;
	m_cuttingDataPlaceholderEdit.m_pPlaceholderMenu   = &m_cuttingDataPlaceholderMenu;
	m_cuttingDataPlaceholderEdit.m_nIDButton		  = IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER;
	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER))->SetIcon(theApp.LoadIcon(IDI_DROPDOWN_ARROW));
	m_cuttingDataPlaceholderEdit.m_pfnOnChangeHandler = NULL;

	m_mediaPivotCtrl.SubclassDlgItem(IDC_MEDIA_PIVOT, this);
	m_mediaPivotCtrl.m_pParent = this;

	((CButton*)GetDlgItem(IDC_LAUNCH_ACROBAT))->SetIcon(theApp.LoadIcon(IDI_ACROBAT));

	m_strPDFTargetName			= m_pdfTargetProps.m_strTargetName;
	m_strFolder					= m_pdfTargetProps.m_strLocation;
	m_strExposer				= m_pdfTargetProps.m_strLocation;
	m_strOutputFilename			= m_pdfTargetProps.m_strOutputFilename;
	((CComboBox*)GetDlgItem(IDC_PS_LEVEL))->SetCurSel(m_pdfTargetProps.m_nPSLevel - 1);
	m_bUsePrinterHalftones 		= ! m_pdfTargetProps.m_bEmitHalftones;
	m_nJDFCompatibility	   		= m_pdfTargetProps.m_nJDFCompatibility;
	m_bJDFMarksInFolder	   		= m_pdfTargetProps.m_bJDFMarksInFolder;
	m_strJDFMarksFolder	   		= m_pdfTargetProps.m_strJDFMarksFolder;
	m_bJDFOutputMime	   		= m_pdfTargetProps.m_bJDFOutputMime;
	m_bOutputCuttingData   		= m_pdfTargetProps.m_bOutputCuttingData;
	m_strCuttingDataFolder		= m_pdfTargetProps.m_strCuttingDataFolder;
	m_strCuttingDataFilename	= m_pdfTargetProps.m_strCuttingDataFilename;

	if ( ! m_pdfTargetProps.m_mediaList.GetSize())
	{
		CString strUnknown;
		strUnknown.LoadString(IDS_UNKNOWN_STR);
		CMediaSize mediaSize(strUnknown, 0.0f, 0.0f, CMediaSize::Portrait, 0.0f, 0.0f, FALSE);
		m_pdfTargetProps.m_mediaList.Add(mediaSize);
	}

	CPrintSheet* pPrintSheet = NULL;
	CPlate*		 pPlate		 = NULL;
	int			 nSide		 = FRONTSIDE;
	GetCurrentSelection(&pPrintSheet, &pPlate, nSide);

	m_fMediaWidth		 = m_pdfTargetProps.m_mediaList[0].GetMediaWidth(pPrintSheet);
	m_fMediaHeight		 = m_pdfTargetProps.m_mediaList[0].GetMediaHeight(pPrintSheet);
	if (m_pdfTargetProps.m_mediaList[0].m_nAutoAlignTo == CMediaSize::AlignNone)
		m_strMediaName	 = m_pdfTargetProps.m_mediaList[0].m_strMediaName;
	else
	{
		CString strMeasureFormat;
		strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
		m_strMediaName.Format((LPCTSTR)strMeasureFormat, MeasureFormat(m_fMediaWidth), MeasureFormat(m_fMediaHeight));
	}
	m_bShrinkToFit		 		= m_pdfTargetProps.m_mediaList[0].m_bShrinkToFit;
	m_bPageFrameCheck	 		= m_pdfTargetProps.m_mediaList[0].m_bPageFrame;
	m_bMarkFrameCheck	 		= m_pdfTargetProps.m_mediaList[0].m_bMarkFrame;
	m_bSheetFrameCheck	 		= m_pdfTargetProps.m_mediaList[0].m_bSheetFrame;
	m_bPlateFrameCheck	 		= m_pdfTargetProps.m_mediaList[0].m_bPlateFrame;
	m_bBoxShapeCheck	 		= m_pdfTargetProps.m_mediaList[0].m_bBoxShape;
	m_nAutoAlignTo		 		= m_pdfTargetProps.m_mediaList[0].m_nAutoAlignTo;
	m_nBookblockDoubleSingle	= (m_nAutoAlignTo == CMediaSize::AlignToBookblockDoublePage) ? 0 : ( (m_nAutoAlignTo == CMediaSize::AlignToBookblockSinglePage) ? 1 : -1);
	m_nMediaRotation			= m_pdfTargetProps.m_mediaList[0].m_posInfos[0].m_nRotation;
	InitRotationBitmap();
	
	GetWindowText(m_strDlgTitle);
	CString strNewDlgTitle = m_strDlgTitle + CString(" ") + m_strPDFTargetName;
	SetWindowText(strNewDlgTitle);

	m_widthSpin.SetRange (0, 1);  
	m_heightSpin.SetRange(0, 1);  
	m_distanceXSpin.SetRange (0, 1);  
	m_distanceYSpin.SetRange (0, 1);  
	m_correctionXSpin.SetRange(0, 1);  
	m_correctionYSpin.SetRange(0, 1);  

	CRect rectDlg, rectSep;
	GetDlgItem(IDC_CUTTINGDATA_CHECK)->GetWindowRect(rectSep); 
	GetWindowRect(rectDlg); 

	switch (m_pdfTargetProps.m_nType)
	{
	case CPDFTargetProperties::TypePDFFolder:
		GetDlgItem(IDC_PRINTER_ICON		 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_JDF_ICON			 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EXPOSER_COMBO	 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EXPOSER_STATIC	 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_JDF_COMPATIBILITY )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_JDFCOMP_STATIC	 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_JDF_OUTPUT_DETAILS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PS_LEVEL			 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_USE_PRINTER_HALFTONES)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_OUTPUT_MIME_CHECK)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TP_MARKSINFOLDER_CHECK)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TP_MARKSINFOLDER_EDIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BROWSE_MARKFILE_FOLDER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SHRINKTO_FIT )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MEDIA_WIDTH  )->EnableWindow(TRUE);
		GetDlgItem(IDC_MEDIA_HEIGHT )->EnableWindow(TRUE);
		GetDlgItem(IDC_WIDTH_SPIN   )->EnableWindow(TRUE);
		GetDlgItem(IDC_HEIGHT_SPIN  )->EnableWindow(TRUE);

		rectDlg.bottom -= (rectDlg.bottom - rectSep.top) + 10;
		MoveWindow(rectDlg);
		break;

	case CPDFTargetProperties::TypePDFPrinter:
		GetDlgItem(IDC_PDF_ICON			 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_JDF_ICON			 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_FOLDER			 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BROWSE_FOLDER	 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_FOLDER_STATIC	 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_JDF_COMPATIBILITY )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_JDFCOMP_STATIC	 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_JDF_OUTPUT_DETAILS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PS_LEVEL			 )->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_USE_PRINTER_HALFTONES)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TARGETPATH_FRONT  )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TARGETPATH_BACK   )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MEDIANAME_COMBO	 )->EnableWindow(FALSE);
		GetDlgItem(IDC_ADD_FORMAT		 )->EnableWindow(FALSE);
		GetDlgItem(IDC_REMOVE_FORMAT	 )->EnableWindow(FALSE);
		GetDlgItem(IDC_OUTPUT_MIME_CHECK )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TP_MARKSINFOLDER_CHECK)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TP_MARKSINFOLDER_EDIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BROWSE_MARKFILE_FOLDER)->ShowWindow(SW_HIDE);
		if ( ! m_bShrinkToFit)
		{
			GetDlgItem(IDC_MEDIA_WIDTH  )->EnableWindow(FALSE);
			GetDlgItem(IDC_MEDIA_HEIGHT )->EnableWindow(FALSE);
			GetDlgItem(IDC_WIDTH_SPIN   )->EnableWindow(FALSE);
			GetDlgItem(IDC_HEIGHT_SPIN  )->EnableWindow(FALSE);
		}
		InitExposerCombo();

		rectDlg.bottom -= (rectDlg.bottom - rectSep.top) + 10;
		MoveWindow(rectDlg);
		break;

	case CPDFTargetProperties::TypeJDFFolder:
		GetDlgItem(IDC_PDF_ICON      )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PRINTER_ICON  )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EXPOSER_COMBO )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_EXPOSER_STATIC)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PS_LEVEL		 )->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_USE_PRINTER_HALFTONES)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TP_MARKSINFOLDER_CHECK)->ShowWindow((m_bJDFOutputMime) ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_TP_MARKSINFOLDER_EDIT)->ShowWindow((m_bJDFOutputMime)  ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_BROWSE_MARKFILE_FOLDER)->ShowWindow((m_bJDFOutputMime) ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_SHRINKTO_FIT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_MEDIA_WIDTH  )->EnableWindow(TRUE);
		GetDlgItem(IDC_MEDIA_HEIGHT )->EnableWindow(TRUE);
		GetDlgItem(IDC_WIDTH_SPIN   )->EnableWindow(TRUE);
		GetDlgItem(IDC_HEIGHT_SPIN  )->EnableWindow(TRUE);

		break;
	}

	switch (m_nOutputTarget)
	{
	case CPDFTargetList::PrintSheetTarget:	
		{
			GetDlgItem(IDC_BOOKBLOCK_DOUBLEPAGE)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BOOKBLOCK_SINGLEPAGE)->ShowWindow(SW_HIDE);
		}
		break;
	case CPDFTargetList::FoldSheetTarget:		
		{
			GetDlgItem(IDC_ADD_TILE)->GetWindowRect(rectSep); 
			GetWindowRect(rectDlg); 
			rectDlg.bottom -= (rectDlg.bottom - rectSep.top) + 10;
			MoveWindow(rectDlg);
			GetDlgItem(IDC_BOOKBLOCK_DOUBLEPAGE)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BOOKBLOCK_SINGLEPAGE)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_ALIGN2SHEET)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_ALIGN2PLATE)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_SHEETFRAME_CHECK)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_PLATEFRAME_CHECK)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BOXSHAPE_CHECK)->ShowWindow(SW_HIDE);
		}
		break;
	case CPDFTargetList::BookletTarget:		
		{
			GetDlgItem(IDC_ADD_TILE)->GetWindowRect(rectSep); 
			GetWindowRect(rectDlg); 
			rectDlg.bottom -= (rectDlg.bottom - rectSep.top) + 10;
			MoveWindow(rectDlg);
			GetDlgItem(IDC_ALIGN2SHEET)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_ALIGN2PLATE)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_SHEETFRAME_CHECK)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_PLATEFRAME_CHECK)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_BOXSHAPE_CHECK)->ShowWindow(SW_HIDE);
		}
		break;
	}

//	CheckTargetPathSideButtons(this);
	int nRelation = CPrintSheet::GetOutfileRelation(m_strOutputFilename);
	int nShow = ( (nRelation != CPrintSheet::SidePerFile) && (nRelation != CPrintSheet::PlatePerFile) ) ? SW_HIDE : SW_SHOW;
	GetDlgItem(IDC_TARGETPATH_FRONT )->ShowWindow(nShow);
	GetDlgItem(IDC_TARGETPATH_BACK  )->ShowWindow(nShow);
	m_nTargetPathSide = 0;

	((CButton*)GetDlgItem(IDC_ADD_FORMAT   ))->SetIcon(theApp.LoadIcon(IDI_ADD_FORMAT));
	((CButton*)GetDlgItem(IDC_REMOVE_FORMAT))->SetIcon(theApp.LoadIcon(IDI_REMOVE_FORMAT));
	((CButton*)GetDlgItem(IDC_ADD_TILE   ))->SetIcon(theApp.LoadIcon(IDI_ADD_TILE));
	((CButton*)GetDlgItem(IDC_DELETE_TILE))->SetIcon(theApp.LoadIcon(IDI_DELETE_TILE));

	m_pToolTip = new CToolTipCtrl;
    m_pToolTip->Create(this);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_ADD_FORMAT   )), IDS_ADD_FORMAT_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_REMOVE_FORMAT)), IDS_REMOVE_FORMAT_TIP);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_ADD_TILE	   )), IDS_ADD_TILE);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_DELETE_TILE  )), IDS_DELETE_TILE);
	m_pToolTip->Activate(TRUE);

	GetDlgItem(IDC_TILE_NUMBER)->GetWindowText(m_strTileNumber);
	m_nCurTileIndex = 0;
	InitTileSpin();

	for (int i = 0; i < m_defaultFormats.GetSize(); i++)
		m_mediaNameCombo.AddString(m_defaultFormats[i].m_strName);

	UpdateControls();

	switch (m_nAutoAlignTo)
	{
	case CMediaSize::AlignNone:			((CButton*)GetDlgItem(IDC_ALIGN2PLATE))->SetCheck(0);	((CButton*)GetDlgItem(IDC_ALIGN2SHEET))->SetCheck(0);	break;
	case CMediaSize::AlignToPlate:		((CButton*)GetDlgItem(IDC_ALIGN2PLATE))->SetCheck(1);	((CButton*)GetDlgItem(IDC_ALIGN2SHEET))->SetCheck(0);	break;
	case CMediaSize::AlignToSheet:		
	case CMediaSize::AlignToFoldSheet:	((CButton*)GetDlgItem(IDC_ALIGN2PLATE))->SetCheck(0);	((CButton*)GetDlgItem(IDC_ALIGN2SHEET))->SetCheck(1);	break;
	}

	if ( (m_nAutoAlignTo == CMediaSize::AlignToPlate) || (m_nAutoAlignTo == CMediaSize::AlignToSheet) || (m_nAutoAlignTo == CMediaSize::AlignToFoldSheet) )
	{
		GetDlgItem(IDC_MEDIANAME_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_MEDIA_WIDTH)->EnableWindow(FALSE);
		GetDlgItem(IDC_WIDTH_SPIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_MEDIA_HEIGHT)->EnableWindow(FALSE);
		GetDlgItem(IDC_HEIGHT_SPIN)->EnableWindow(FALSE);
	}

	UpdatePrintSheetView();

	//GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow(FALSE);

	m_mediaPivotCtrl.m_nActiveCell = -1;
	m_mediaPivotCtrl.Update();

	GetDlgItem(IDC_TP_MARKSINFOLDER_EDIT)->EnableWindow(m_bJDFMarksInFolder);
	GetDlgItem(IDC_BROWSE_MARKFILE_FOLDER)->EnableWindow(m_bJDFMarksInFolder);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgPDFTargetProps::CheckTargetPathSideButtons(CDialog* pDlg)
{
	CDlgPDFTargetProps* pThis = (CDlgPDFTargetProps*)pDlg;

	CString strOldName = pThis->m_strOutputFilename;
	pThis->UpdateData(TRUE);
	int nCharPos = pThis->m_strOutputFilename.FindOneOf(_T("\"*/:<>?|"));
	if (nCharPos >= 0)
	{
		CString strPlaceholder; 
		if (nCharPos >= 2)
		{
			strPlaceholder.AppendChar(pThis->m_strOutputFilename[nCharPos - 2]);
			strPlaceholder.AppendChar(pThis->m_strOutputFilename[nCharPos - 1]);
		}
		if ( ! CPlaceholderEdit::IsExtSyntax(pThis->m_strOutputFilename[nCharPos], pThis->m_strOutputFilename[nCharPos + 1], strPlaceholder))
		{
			pThis->m_strOutputFilename = strOldName;
			pThis->UpdateData(FALSE);
			((CEdit*)pThis->GetDlgItem(IDC_OUTPUT_FILENAME))->SetSel(nCharPos, nCharPos);
			AfxMessageBox(IDS_INVALID_FILENAMECHARACTER);
			return;
		}
	}

	int nRelation = CPrintSheet::GetOutfileRelation(pThis->m_strOutputFilename);
	int nShow = ( (nRelation != CPrintSheet::SidePerFile) && (nRelation != CPrintSheet::PlatePerFile) ) ? SW_HIDE : SW_SHOW;
	pThis->GetDlgItem(IDC_TARGETPATH_FRONT )->ShowWindow(nShow);
	pThis->GetDlgItem(IDC_TARGETPATH_BACK  )->ShowWindow(nShow);

	if (nShow == SW_HIDE)
	{
		pThis->m_nTargetPathSide = 0;
		pThis->UpdateData(FALSE);
	}
}

void CDlgPDFTargetProps::OnMouseMove(UINT nFlags, CPoint point) 
{
	m_mediaPivotCtrl.OnMouseMove(nFlags, point);

	CDialog::OnMouseMove(nFlags, point);
}

void CDlgPDFTargetProps::OnChangePdftargetName() 
{
	UpdateData(TRUE);
	CString strNewDlgTitle = m_strDlgTitle + CString(" ") + m_strPDFTargetName;
	SetWindowText(strNewDlgTitle);
}

void CDlgPDFTargetProps::InitExposerCombo()
{
	CImageList imageList;
	imageList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 1, 0);	// List not to grow
	imageList.Add(theApp.LoadIcon(IDI_PRINTER));
	m_exposerCombo.SetImageList(&imageList);
	imageList.Detach();

	DWORD			 dwBytesNeeded;	// pointer to variable with no. of bytes copied (or required) 
	DWORD			 dwReturned;	// pointer to variable with no. of printer info. structures copied 
	LPPRINTER_INFO_4 lpPrinterInfo4 = NULL;
	LPPRINTER_INFO_5 lpPrinterInfo5 = NULL;

	m_exposerCombo.ResetContent();
 
	BOOL bOK = TRUE;
	if (theApp.m_dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		// get byte count needed for buffer, alloc buffer, then enum the printers
		EnumPrinters(PRINTER_ENUM_LOCAL | PRINTER_ENUM_CONNECTIONS, NULL, 4, NULL, 0, &dwBytesNeeded, &dwReturned);
		lpPrinterInfo4 = (LPPRINTER_INFO_4) LocalAlloc (LPTR, dwBytesNeeded);
		// now find the printers	
		bOK = EnumPrinters(PRINTER_ENUM_LOCAL | PRINTER_ENUM_CONNECTIONS, NULL, 4, (LPBYTE) lpPrinterInfo4, dwBytesNeeded, &dwBytesNeeded, &dwReturned);
	}
	else
	{
		// get byte count needed for buffer, alloc buffer, then enum the printers
		EnumPrinters(PRINTER_ENUM_LOCAL, NULL, 5, NULL, 0, &dwBytesNeeded, &dwReturned);
		lpPrinterInfo5 = (LPPRINTER_INFO_5) LocalAlloc (LPTR, dwBytesNeeded);
		// now find the printers	
		bOK = EnumPrinters (PRINTER_ENUM_LOCAL, NULL, 5, (LPBYTE) lpPrinterInfo5, dwBytesNeeded, &dwBytesNeeded, &dwReturned);
	}

	if ( ! bOK)
	{
		int nError = GetLastError();
		CString strMsg;
		strMsg.Format(_T("Error %d in 'EnumPrinters'!"), nError);
		AfxMessageBox(strMsg, MB_OK);
	}

	if (dwReturned > 0)
	{
		for (DWORD i = 0; i < dwReturned; i++)
		{
			COMBOBOXEXITEM item;
			item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;
			item.iItem			= m_exposerCombo.GetCount();
			if (theApp.m_dwPlatformId == VER_PLATFORM_WIN32_NT)
				item.pszText	=  (lpPrinterInfo4 + i)->pPrinterName;
			else
				item.pszText	=  (lpPrinterInfo5 + i)->pPrinterName;
			item.iImage			= 0;
			item.iSelectedImage = 0;
			m_exposerCombo.InsertItem(&item);
		}
	}
	else
		m_strPDFTargetName.Empty();

	if (theApp.m_dwPlatformId == VER_PLATFORM_WIN32_NT)
		LocalFree(LocalHandle(lpPrinterInfo4));
	else
		LocalFree(LocalHandle(lpPrinterInfo5));
}

void CDlgPDFTargetProps::OnBrowseFolder() 
{
	CDlgDirPicker dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, _T(" |*.___||"), this);
	CString strTitle;
	strTitle.LoadString(IDS_DIRECTORY_STRING);
	dlg.m_ofn.lpstrTitle	  = strTitle;
	dlg.m_ofn.lpstrInitialDir = (m_strFolder.IsEmpty()) ? (LPCSTR)theApp.settings.m_szWorkDir : m_strFolder;

	if (dlg.DoModal() == IDOK)
		m_strFolder = dlg.m_strPath;

	GetDlgItem(IDC_APPLY_BUTTON)->SetFocus();
	((CButton*)GetDlgItem(IDC_APPLY_BUTTON))->SetButtonStyle(BS_DEFPUSHBUTTON);
	((CButton*)GetDlgItem(IDC_BROWSE_FOLDER))->SetButtonStyle(BS_PUSHBUTTON);

	UpdateData(FALSE);
}

void CDlgPDFTargetProps::OnBrowseMarkfileFolder() 
{
	CDlgDirPicker dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, _T(" |*.___||"), this);
	CString strTitle;
	strTitle.LoadString(IDS_DIRECTORY_STRING);
	dlg.m_ofn.lpstrTitle	  = strTitle;
	dlg.m_ofn.lpstrInitialDir = (m_strJDFMarksFolder.IsEmpty()) ? (LPCSTR)theApp.settings.m_szWorkDir : m_strJDFMarksFolder;

	if (dlg.DoModal() == IDOK)
		m_strJDFMarksFolder = dlg.m_strPath;

	GetDlgItem(IDC_APPLY_BUTTON)->SetFocus();
	((CButton*)GetDlgItem(IDC_APPLY_BUTTON))->SetButtonStyle(BS_DEFPUSHBUTTON);
	((CButton*)GetDlgItem(IDC_BROWSE_FOLDER))->SetButtonStyle(BS_PUSHBUTTON);

	UpdateData(FALSE);
}

void CDlgPDFTargetProps::OnSelchangeExposerCombo() 
{
	UpdateData(TRUE);

	HANDLE hPrinter;
	if (OpenPrinter((LPTSTR)m_strExposer.GetBuffer(30), &hPrinter, NULL)) 
	{
		DWORD	  dwNeeded  = DocumentProperties(m_hWnd, hPrinter, (LPTSTR)m_strExposer.GetBuffer(30), NULL, NULL, 0);
		LPDEVMODE lpDevMode = (LPDEVMODE)LocalAlloc(LPTR, dwNeeded); 
		if (DocumentProperties(m_hWnd, hPrinter, (LPTSTR)m_strExposer.GetBuffer(30), lpDevMode, NULL, DM_OUT_BUFFER) < 0)
		{
			m_strMediaName.LoadString(IDS_UNKNOWN_STR);
			m_fMediaWidth = m_fMediaHeight = 0.0f;
		}
		else
		{
			m_strMediaName = GetCurrentForm(m_strExposer, lpDevMode->dmPaperSize, &m_fMediaWidth, &m_fMediaHeight, lpDevMode);
			if (lpDevMode->dmOrientation == DMORIENT_LANDSCAPE)
			{
				float fTemp = m_fMediaWidth; m_fMediaWidth = m_fMediaHeight; m_fMediaHeight = fTemp;
			}
		}

		m_pdfTargetProps.m_mediaList[0].SetMediaWidth(m_fMediaWidth);	
		m_pdfTargetProps.m_mediaList[0].SetMediaHeight(m_fMediaHeight);

		UpdateData(FALSE);

		UpdatePrintSheetView();

		LocalFree(LocalHandle(lpDevMode));  
		ClosePrinter(hPrinter);
	}
}

CString CDlgPDFTargetProps::GetCurrentForm(CString strPrinterName, short dmPaperSize, float* pfPaperWidth, float* pfPaperHeight, PDEVMODE pDevMode)
{
	typedef char PAPERNAME[64];
	DWORD		 dwNumPaperSizes = 0, dwNumPaperNames = 0, dwNumPaperList = 0;
	POINT		 *paperSizes;
	PAPERNAME	 *paperNames;
	WORD		 *paperList;

	// get number of paper names
	dwNumPaperNames = DeviceCapabilities((LPTSTR )strPrinterName.GetBuffer(30), NULL, (WORD)DC_PAPERNAMES,
										 (LPTSTR )NULL, (LPDEVMODE)pDevMode);
	// get number of paper sizes
	dwNumPaperSizes = DeviceCapabilities((LPTSTR )strPrinterName.GetBuffer(30), NULL, (WORD)DC_PAPERSIZE,
										 (LPTSTR )NULL, (LPDEVMODE)pDevMode);
	// get number of paper list elements
	dwNumPaperList  = DeviceCapabilities((LPTSTR )strPrinterName.GetBuffer(30), NULL, (WORD)DC_PAPERS,
										 (LPTSTR )NULL, (LPDEVMODE)pDevMode);

	if ((dwNumPaperNames == -1) || (dwNumPaperSizes == -1) || (dwNumPaperList == -1))
	{
		//TODO:Search for predefined papersizes like DMPAPER__LETTER ...
		*pfPaperWidth  = 0.0f;
		*pfPaperHeight = 0.0f;
		return CString("");
	}
	
	// allocate space for paper names
	paperNames = (PAPERNAME *)LocalAlloc(LPTR, dwNumPaperNames * sizeof(PAPERNAME));
	// allocate space for paper sizes
	paperSizes = (POINT *)	  LocalAlloc(LPTR, dwNumPaperSizes * sizeof(POINT));
	// allocate space for paper list
	paperList  = (WORD *)	  LocalAlloc(LPTR, dwNumPaperSizes * sizeof(DWORD));

	// fill buffer with paper names
	dwNumPaperNames = DeviceCapabilities((LPTSTR )strPrinterName.GetBuffer(30), NULL, (WORD)DC_PAPERNAMES,
					 				     (LPTSTR )paperNames, (LPDEVMODE)pDevMode);
	// fill buffer with paper sizes
	dwNumPaperSizes = DeviceCapabilities((LPTSTR )strPrinterName.GetBuffer(30), NULL, (WORD)DC_PAPERSIZE,
										 (LPTSTR )paperSizes, (LPDEVMODE)pDevMode);
	// fill buffer with paper list
    dwNumPaperList  = DeviceCapabilities((LPTSTR )strPrinterName.GetBuffer(30), NULL, (WORD)DC_PAPERS,
										 (LPTSTR )paperList,  (LPDEVMODE)pDevMode);
	int nPaperIndex = 0;
	for (int i = 0; i < (int)dwNumPaperList; i++)
		if (paperList[i] == dmPaperSize)
		{
			nPaperIndex = i;
			break;
		}

	CString strPaperName;
	if ((dwNumPaperNames > 0) && (dwNumPaperSizes > 0))
	{
		strPaperName   = paperNames[nPaperIndex];
		*pfPaperWidth  = paperSizes[nPaperIndex].x / 10.0f;
		*pfPaperHeight = paperSizes[nPaperIndex].y / 10.0f;
	}

	LocalFree(LocalHandle(paperNames));
	LocalFree(LocalHandle(paperSizes));
	LocalFree(LocalHandle(paperList));

	return strPaperName;
}

void CDlgPDFTargetProps::OnChangeMediaHeight() 
{
	CButton* pButton = (CButton*)GetDlgItem(IDC_APPLY_BUTTON);
	if (pButton)
		pButton->EnableWindow();
}

void CDlgPDFTargetProps::OnChangeMediaWidth() 
{
	CButton* pButton = (CButton*)GetDlgItem(IDC_APPLY_BUTTON);
	if (pButton)
		pButton->EnableWindow();
}

void CDlgPDFTargetProps::OnKillfocusMediaHeight() 
{
	UpdateData(TRUE);
	CheckOutputFormat();
}

void CDlgPDFTargetProps::OnKillfocusMediaWidth() 
{
	UpdateData(TRUE);
	CheckOutputFormat();
}

void CDlgPDFTargetProps::CheckOutputFormat()
{
	if ( (theApp.m_nDongleStatus != CImpManApp::SmallFormatDongle) && (theApp.m_nDongleStatus != CImpManApp::NewsDongle) && (theApp.m_nDongleStatus != CImpManApp::DigitalDongle) )
		return;

	float& fWidth  = (m_fMediaWidth > m_fMediaHeight) ? m_fMediaWidth  : m_fMediaHeight;
	float& fHeight = (m_fMediaWidth > m_fMediaHeight) ? m_fMediaHeight : m_fMediaWidth;

	BOOL bFormatExceeded = FALSE;
	if (theApp.m_nDongleStatus == CImpManApp::SmallFormatDongle)
	{
		if (fWidth > _SFW())
		{ 
			fWidth = _SFW(); 
			bFormatExceeded = TRUE;
		}

		if (fHeight > _SFH())
		{ 
			fHeight = _SFH(); 
			bFormatExceeded = TRUE;
		}
	}
	else
		if (theApp.m_nDongleStatus == CImpManApp::NewsDongle)
		{
			if (fWidth > _NEWSW())
			{ 
				fWidth = _NEWSW(); 
				bFormatExceeded = TRUE;
			}

			if (fHeight > _NEWSH())
			{ 
				fHeight = _NEWSH(); 
				bFormatExceeded = TRUE;
			}
		}
		else
		{
			if (fWidth > _DIGITALW())
			{ 
				fWidth = _DIGITALW(); 
				bFormatExceeded = TRUE;
			}

			if (fHeight > _DIGITALH())
			{ 
				fHeight = _DIGITALH(); 
				bFormatExceeded = TRUE;
			}
		}

	if (bFormatExceeded)
	{
		UpdateData(FALSE);
		if (theApp.m_nDongleStatus == CImpManApp::SmallFormatDongle)
			AfxMessageBox(IDS_SMALLFORMAT_EDITION_HINT);
		else
			if (theApp.m_nDongleStatus == CImpManApp::NewsDongle)
				AfxMessageBox(IDS_NEWS_EDITION_HINT);
			else
				AfxMessageBox(IDS_DIGITAL_EDITION_HINT);
	}
}

void CDlgPDFTargetProps::OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_widthSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow();
}

void CDlgPDFTargetProps::OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_heightSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow();
}

void CDlgPDFTargetProps::GetCurrentSelection(CPrintSheet** pPrintSheet, CPlate** pPlate, int& nSide)
{
	*pPrintSheet = NULL;
	*pPlate		 = NULL;
	nSide		 = FRONTSIDE;

	//if (m_pWndMsgTarget)
	//	if (m_pWndMsgTarget->IsKindOf(RUNTIME_CLASS(CDlgProdDataPrepress)))
	//	{
	//		*pPrintSheet = ((CDlgProdDataPrepress*)m_pWndMsgTarget)->m_pPrintSheet;
	//		return;
	//	}

	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if ( ! pPrintSheetTableView)
		return;

	CDisplayItem* pDI = pPrintSheetTableView->m_DisplayList.GetFirstSelectedItem();
	if ( ! pDI)
		return;

	if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheet)) ||
		pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetFile)) ) 
	{
		*pPrintSheet = (CPrintSheet*)pDI->m_pData;
		nSide = FRONTSIDE;
	}
	else
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetSide)) ||
			pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetSideFile)) )
		{
			*pPrintSheet = (CPrintSheet*)pDI->m_pData;
			nSide = (int)pDI->m_pAuxData;
		}
		else
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetPlate)) ||
				pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetTableView, PDFSheetPlateFile)) )
			{
				*pPrintSheet = (CPrintSheet*)pDI->m_pData;
				*pPlate = (CPlate*)pDI->m_pAuxData;
				nSide = (*pPlate)->GetSheetSide();
			}
}

void CDlgPDFTargetProps::OnShrinktoFit() 
{
	UpdateData(TRUE);

	if (m_bShrinkToFit)
	{
		CPrintSheet* pPrintSheet = NULL;
		CPlate*		 pPlate		 = NULL;
		int			 nSide		 = FRONTSIDE;

		GetCurrentSelection(&pPrintSheet, &pPlate, nSide);
		if ( ! pPrintSheet)
			return;
		CLayout* pLayout = pPrintSheet->GetLayout();
		if ( ! pLayout)
			return;
		CPressDevice* pPressDevice = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.GetPressDevice() : pLayout->m_BackSide.GetPressDevice();
		if ( ! pPressDevice)
			return;

		m_fMediaWidth  = pPressDevice->m_fPlateWidth;
		m_fMediaHeight = pPressDevice->m_fPlateHeight;
		m_pdfTargetProps.m_mediaList[0].SetMediaWidth(m_fMediaWidth);	
		m_pdfTargetProps.m_mediaList[0].SetMediaHeight(m_fMediaHeight);
		for (int i = 0; i < m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize(); i++)
		{
			m_pdfTargetProps.m_mediaList[0].m_posInfos[i].m_reflinePosParams.m_fDistanceX				= 0.0f;
			m_pdfTargetProps.m_mediaList[0].m_posInfos[i].m_reflinePosParams.m_fDistanceY				= 0.0f;
			m_pdfTargetProps.m_mediaList[0].m_posInfos[i].m_reflinePosParams.m_nReferenceItemInX		= PLATE_REFLINE;
			m_pdfTargetProps.m_mediaList[0].m_posInfos[i].m_reflinePosParams.m_nReferenceItemInY		= PLATE_REFLINE;
			m_pdfTargetProps.m_mediaList[0].m_posInfos[i].m_reflinePosParams.m_nReferenceLineInX		= XCENTER;
			m_pdfTargetProps.m_mediaList[0].m_posInfos[i].m_reflinePosParams.m_nReferenceLineInY		= BOTTOM;
			m_pdfTargetProps.m_mediaList[0].m_posInfos[i].m_reflinePosParams.m_nObjectReferenceLineInX	= XCENTER;
			m_pdfTargetProps.m_mediaList[0].m_posInfos[i].m_reflinePosParams.m_nObjectReferenceLineInY	= BOTTOM;
		}

		UpdateData(FALSE);
		UpdatePrintSheetView();

		GetDlgItem(IDC_MEDIA_WIDTH    )->EnableWindow(TRUE);
		GetDlgItem(IDC_MEDIA_HEIGHT   )->EnableWindow(TRUE);
		GetDlgItem(IDC_WIDTH_SPIN     )->EnableWindow(TRUE);
		GetDlgItem(IDC_HEIGHT_SPIN    )->EnableWindow(TRUE);
	}
	else
	{
		OnSelchangeExposerCombo();
		GetDlgItem(IDC_MEDIA_WIDTH	  )->EnableWindow(FALSE);
		GetDlgItem(IDC_MEDIA_HEIGHT   )->EnableWindow(FALSE);
		GetDlgItem(IDC_WIDTH_SPIN     )->EnableWindow(FALSE);
		GetDlgItem(IDC_HEIGHT_SPIN    )->EnableWindow(FALSE);
	}
}

void CDlgPDFTargetProps::OnChangeMediaDistancex() 
{
	CButton* pButton = (CButton*)GetDlgItem(IDC_APPLY_BUTTON);
	if (pButton)
		pButton->EnableWindow();
}

void CDlgPDFTargetProps::OnChangeMediaDistancey() 
{
	CButton* pButton = (CButton*)GetDlgItem(IDC_APPLY_BUTTON);
	if (pButton)
		pButton->EnableWindow();
}

void CDlgPDFTargetProps::OnDeltaposDistancexSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_distanceXSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow();
}

void CDlgPDFTargetProps::OnDeltaposDistanceySpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_distanceYSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow();
}


void CDlgPDFTargetProps::OnChangeMediaCorrectx() 
{
	CButton* pButton = (CButton*)GetDlgItem(IDC_APPLY_BUTTON);
	if (pButton)
		pButton->EnableWindow();
}

void CDlgPDFTargetProps::OnChangeMediaCorrecty() 
{
	CButton* pButton = (CButton*)GetDlgItem(IDC_APPLY_BUTTON);
	if (pButton)
		pButton->EnableWindow();
}

void CDlgPDFTargetProps::OnDeltaposMediaCorrectxSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_correctionXSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow();
}

void CDlgPDFTargetProps::OnDeltaposMediaCorrectySpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_correctionYSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow();
}

void CDlgPDFTargetProps::UpdateControls()
{
	m_fMediaDistanceX	= m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceX;
	m_fMediaDistanceY	= m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceY;

	m_fMediaCorrectionX = m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionX;
	m_fMediaCorrectionY = m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionY;

	m_nMediaRotation	= m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_nRotation;

	m_mediaPivotCtrl.Update();

	UpdateData(FALSE);
}

void CDlgPDFTargetProps::InitTileSpin()
{
	if (m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize() <= 1)
	{
		m_nCurTileIndex = 0;
		GetDlgItem(IDC_TILE_NUMBER)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TILE_SPIN  )->ShowWindow(SW_HIDE);
		return;
	}
	else
	{
		GetDlgItem(IDC_TILE_NUMBER)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TILE_SPIN  )->ShowWindow(SW_SHOW);
		CString string;
		string.Format(m_strTileNumber, m_nCurTileIndex + 1, m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize());
		GetDlgItem(IDC_TILE_NUMBER)->SetWindowText(string);
	}

	UpdateControls();
}

void CDlgPDFTargetProps::OnDeltaposTileSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	UpdateData(TRUE);

	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceX = m_fMediaDistanceX;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceY = m_fMediaDistanceY;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionX = m_fMediaCorrectionX;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionY = m_fMediaCorrectionY;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_nRotation	   = m_nMediaRotation;

	if (pNMUpDown->iDelta < 0)
	{
		m_nCurTileIndex++;
		if (m_nCurTileIndex >= m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize())
			m_nCurTileIndex = 0;
	}
	else
	{
		m_nCurTileIndex--;
		if (m_nCurTileIndex < 0)
			m_nCurTileIndex = m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize() - 1;
	}

	InitTileSpin();
	InitRotationBitmap();
	UpdatePrintSheetView();
	m_mediaPivotCtrl.Update();
}

void CDlgPDFTargetProps::OnAddTile() 
{
	CMediaPosInfo mediaPosInfo(0.0f, 0.0f, FALSE);
	m_pdfTargetProps.m_mediaList[0].m_posInfos.Add(mediaPosInfo);
	m_nCurTileIndex = m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize() - 1;
	InitTileSpin();
	UpdatePrintSheetView();
	m_mediaPivotCtrl.Update();

	((CButton*)GetDlgItem(IDC_ADD_TILE    ))->SetButtonStyle(BS_PUSHBUTTON);
	((CButton*)GetDlgItem(IDC_APPLY_BUTTON))->SetButtonStyle(BS_DEFPUSHBUTTON);
}

void CDlgPDFTargetProps::OnDeleteTile() 
{
	if (m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize() <= 1)
		return;
	m_pdfTargetProps.m_mediaList[0].m_posInfos.RemoveAt(m_nCurTileIndex);
	if (m_nCurTileIndex >= m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize())
		m_nCurTileIndex--;
	InitTileSpin();
	UpdatePrintSheetView();
	m_mediaPivotCtrl.Update();

	((CButton*)GetDlgItem(IDC_DELETE_TILE ))->SetButtonStyle(BS_PUSHBUTTON);
	((CButton*)GetDlgItem(IDC_APPLY_BUTTON))->SetButtonStyle(BS_DEFPUSHBUTTON);
}

void CDlgPDFTargetProps::OnApplyButton() 
{
	UpdateData(TRUE);

	CWnd* pFocusWnd = GetFocus();

	if ( (GetDlgItem(IDC_MEDIA_WIDTH)	  != pFocusWnd) && (GetDlgItem(IDC_MEDIA_HEIGHT)	!= pFocusWnd) && 
		 (GetDlgItem(IDC_MEDIA_DISTANCEX) != pFocusWnd) && (GetDlgItem(IDC_MEDIA_DISTANCEY) != pFocusWnd) && 
		 (GetDlgItem(IDC_MEDIA_CORRECTX)  != pFocusWnd) && (GetDlgItem(IDC_MEDIA_CORRECTY)	!= pFocusWnd) )
	{
		 OnOK();
		 return;
	}

	CheckOutputFormat();

	m_pdfTargetProps.m_mediaList[0].SetMediaWidth(m_fMediaWidth);	
	m_pdfTargetProps.m_mediaList[0].SetMediaHeight(m_fMediaHeight);

	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceX = m_fMediaDistanceX;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceY = m_fMediaDistanceY;

	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionX = m_fMediaCorrectionX;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionY = m_fMediaCorrectionY;

	UpdatePrintSheetView();
	//GetDlgItem(IDC_APPLY_BUTTON)->EnableWindow(FALSE);
}

void CDlgPDFTargetProps::OnDeltaposMediaOrientationSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	
	if (pNMUpDown->iDelta < 0)
	{
		switch (m_nMediaRotation)
		{
		case 0:	  m_nMediaRotation = 90;	break;
		case 90:  m_nMediaRotation = 180;	break;
		case 180: m_nMediaRotation = 270;	break;
		case 270: m_nMediaRotation = 0;		break;
		default:  m_nMediaRotation = 0;		break;
		}
	}
	else
	{
		switch (m_nMediaRotation)
		{
		case 0:	  m_nMediaRotation = 270;	break;
		case 90:  m_nMediaRotation = 0;		break;
		case 180: m_nMediaRotation = 90;	break;
		case 270: m_nMediaRotation = 180;	break;
		default:  m_nMediaRotation = 0;		break;
		}
	}

	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_nRotation = m_nMediaRotation;

	InitRotationBitmap();
	UpdatePrintSheetView();

	*pResult = 1;
}

void CDlgPDFTargetProps::InitRotationBitmap()
{
	int nIDBitmap;
	switch (m_nMediaRotation)
	{
	case 0:	  nIDBitmap = IDB_PORTRAIT;		break;
	case 90:  nIDBitmap = IDB_PORTRAIT_90;	break;
	case 180: nIDBitmap = IDB_PORTRAIT_180; break;
	case 270: nIDBitmap = IDB_PORTRAIT_270;	break;
	default:  nIDBitmap = -1;				break;
	}
	
	if ((HBITMAP)m_bmRotation)
		m_bmRotation.DeleteObject();
	m_bmRotation.LoadBitmap(nIDBitmap); 
	((CStatic*)GetDlgItem(IDC_ROTATION_BITMAP))->SetBitmap((HBITMAP)m_bmRotation);
}

void CDlgPDFTargetProps::OnLaunchAcrobat() 
{
#ifdef PDF
	CalibrateOutput(TRUE);
#endif
}

void CDlgPDFTargetProps::OnTestOutput() 
{
#ifdef PDF
	CalibrateOutput(FALSE);
#endif
}

#ifdef PDF
void CDlgPDFTargetProps::CalibrateOutput(BOOL bOpenInAcrobat) 
{
	CPrintSheet* pPrintSheet = NULL;
	CPlate*		 pPlate		 = NULL;
	int			 nSide		 = FRONTSIDE;
	GetCurrentSelection(&pPrintSheet, &pPlate, nSide);

	if ( ! pPrintSheet)
		return;
	if ( ! pPlate)
		pPlate = (nSide == FRONTSIDE) ? &pPrintSheet->m_FrontSidePlates.m_defaultPlate : &pPrintSheet->m_BackSidePlates.m_defaultPlate;

	UpdateData(TRUE);

	m_pdfTargetProps.m_mediaList[0].m_strMediaName	= m_strMediaName;		
	m_pdfTargetProps.m_mediaList[0].SetMediaWidth(m_fMediaWidth);	
	m_pdfTargetProps.m_mediaList[0].SetMediaHeight(m_fMediaHeight);
	m_pdfTargetProps.m_mediaList[0].m_bShrinkToFit  = m_bShrinkToFit;
	m_pdfTargetProps.m_mediaList[0].m_bPageFrame	= m_bPageFrameCheck;
	m_pdfTargetProps.m_mediaList[0].m_bMarkFrame	= m_bMarkFrameCheck;
	m_pdfTargetProps.m_mediaList[0].m_bSheetFrame	= m_bSheetFrameCheck;
	m_pdfTargetProps.m_mediaList[0].m_bPlateFrame	= m_bPlateFrameCheck;
	m_pdfTargetProps.m_mediaList[0].m_bBoxShape		= m_bBoxShapeCheck;
	m_pdfTargetProps.m_mediaList[0].m_nAutoAlignTo  = m_nAutoAlignTo;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceX = m_fMediaDistanceX;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceY = m_fMediaDistanceY;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionX					= m_fMediaCorrectionX;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionY					= m_fMediaCorrectionY;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_nRotation						= m_nMediaRotation;

	CPDFOutput pdfOutput(m_pdfTargetProps, _T("KIM_Output_Calibration"));

	float fXPos, fYPos;
	pdfOutput.m_fMediaWidth  = m_pdfTargetProps.m_mediaList[0].GetMediaWidth(pPrintSheet);
	pdfOutput.m_fMediaHeight = m_pdfTargetProps.m_mediaList[0].GetMediaHeight(pPrintSheet);
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.GetSheetPosition(pPrintSheet, nSide, fXPos, fYPos, pdfOutput.m_fMediaWidth, pdfOutput.m_fMediaHeight);

	pdfOutput.m_strTmpOutputPDFFile.Format(_T("%s\\%s.pdf"), theApp.settings.m_szTempDir, pdfOutput.m_strDocTitle);
	if (bOpenInAcrobat)
		pdfOutput.m_strOutputPDFFile = pdfOutput.m_strTmpOutputPDFFile;
	else
		pdfOutput.m_strOutputPDFFile.Format(_T("%s\\%s.pdf"), m_pdfTargetProps.m_strLocation, pdfOutput.m_strDocTitle);

	BOOL bOverwriteAll = TRUE;
	if ( ! pdfOutput.OpenStream(pPrintSheet, bOverwriteAll, (bOpenInAcrobat) ? FALSE : TRUE, nSide, pPlate))
		return;

	pdfOutput.ProcessTile(pPlate, (m_pdfTargetProps.m_mediaList[0].m_posInfos.GetSize() > 1) ? TRUE : FALSE, fXPos, fYPos, 
						   m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_nRotation, 
						   m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionX, 
						   m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionY, 
						   m_nCurTileIndex + 1);

	pdfOutput.CloseStream((bOpenInAcrobat) ? FALSE : TRUE);

	if (bOpenInAcrobat)
	{
		BeginWaitCursor();     
		CPageListFrame::LaunchAcrobat(pdfOutput.m_strOutputPDFFile, -1);
		EndWaitCursor();     
	}
}
#endif

void CDlgPDFTargetProps::UpdatePrintSheetView()
{
	if (m_hWnd)
		UpdateData(TRUE);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		pView->m_DisplayList.Invalidate();
		pView->Invalidate();
		pView->OnUpdate(NULL, 0, NULL); 
	}

	//if (m_pWndMsgTarget)
	//	m_pWndMsgTarget->PostMessage(WM_COMMAND, ID_UPDATE_OUTPUTMEDIA);
}

BOOL CDlgPDFTargetProps::PreTranslateMessage(MSG* pMsg) 
{
	if (NULL != m_pToolTip)            
		m_pToolTip->RelayEvent(pMsg);	

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgPDFTargetProps::OnChangeOutputFilename() 
{
	CString strOldName = m_strOutputFilename;
	UpdateData(TRUE);
	if (m_strOutputFilename.FindOneOf(_T("\"*/:<>?|")))
	{
		m_strOutputFilename = strOldName;
		UpdateData(FALSE);
	}
}

CString CDlgPDFTargetProps::AddFilenamePlaceHolder(int nSel, CString strFilename)
{
	switch (nSel)
	{
	case  0: strFilename+= "$2"; break;
	case  1: strFilename+= "$3"; break;
	case  2: strFilename+= "$7"; break;
	case  3: strFilename+= "$9"; break;
	case  4: strFilename+= "$B"; break;
	case  5: strFilename+= "$C"; break;
	case  6: strFilename+= "$D"; break;
	case  7: strFilename+= "$E"; break;
	case  8: strFilename+= "$F"; break;
	case  9: strFilename+= "$G"; break;
	case 10: strFilename+= "$H"; break;
	case 11: strFilename+= "$I"; break;
	case 12: strFilename+= "$J"; break;
	case 13: strFilename+= "$K"; break;
	case 14: strFilename+= "$L"; break;
	case 15: strFilename+= "$M"; break;
	default:break;
	}

	return strFilename;
}

void CDlgPDFTargetProps::OnOutputframeColorants() 
{
	CDlgJobColorDefinitions dlg(FALSE, TRUE);	// no modify mode, hide composite button
	CColorDefinition colorDef;
	for (int i = 0; i < m_pdfTargetProps.m_mediaList[0].m_frameSource.m_PageSourceHeaders.GetSize(); i++)
	{
		colorDef.m_strColorName = m_pdfTargetProps.m_mediaList[0].m_frameSource.m_PageSourceHeaders[i].m_strColorName;
		colorDef.m_rgb			= m_pdfTargetProps.m_mediaList[0].m_frameSource.m_PageSourceHeaders[i].m_rgb;
		colorDef.m_nColorIndex	= m_pdfTargetProps.m_mediaList[0].m_frameSource.m_PageSourceHeaders[i].m_nColorIndex;
		dlg.m_PSColorDefTable.InsertColorDef(colorDef);
	}

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_pdfTargetProps.m_mediaList[0].m_frameSource.m_PageSourceHeaders.RemoveAll();
	int	nNumColor = dlg.m_PSColorDefTable.GetSize();
	if (nNumColor > 0)
	{	
		CPageSourceHeader frameSourceHeaderToAdd;
		frameSourceHeaderToAdd.Create(0);

		frameSourceHeaderToAdd.m_bPageNumberFix = FALSE;
		frameSourceHeaderToAdd.m_nPageNumber = 1;
		frameSourceHeaderToAdd.m_fBBoxBottom = 0.0f;
		frameSourceHeaderToAdd.m_fBBoxLeft   = 0.0f;
		frameSourceHeaderToAdd.m_fBBoxRight  = 0.0f;
		frameSourceHeaderToAdd.m_fBBoxTop	 = 0.0f;
		frameSourceHeaderToAdd.m_strPageName = _T("ControlFrame");

		m_pdfTargetProps.m_mediaList[0].m_frameSource.m_strContentFilesPath = _T("");
		for (int i = 0; i < nNumColor; i++)
		{
			CColorDefinition& rColorDef			  = dlg.m_PSColorDefTable[i];
			frameSourceHeaderToAdd.m_strColorName = rColorDef.m_strColorName;
			frameSourceHeaderToAdd.m_rgb		  = rColorDef.m_rgb;
			frameSourceHeaderToAdd.m_nColorIndex  = rColorDef.m_nColorIndex;

			m_pdfTargetProps.m_mediaList[0].m_frameSource.m_PageSourceHeaders.SetAtGrow(i, frameSourceHeaderToAdd);
		}
	}

	m_pdfTargetProps.m_mediaList[0].m_frameSource.ReorganizePageNumbers();
}

void CDlgPDFTargetProps::OnSelchangeMedianameCombo() 
{
	int nSel = m_mediaNameCombo.GetCurSel();
	if (nSel == CB_ERR)
		return;

	m_mediaNameCombo.GetLBText(nSel, m_strMediaName);

	m_defaultFormats.GetDefaultFormat(m_strMediaName, &m_fMediaWidth, &m_fMediaHeight);

	UpdateData(FALSE);

	m_pdfTargetProps.m_mediaList[0].SetMediaWidth(m_fMediaWidth);	
	m_pdfTargetProps.m_mediaList[0].SetMediaHeight(m_fMediaHeight);

	UpdatePrintSheetView();
}

void CDlgPDFTargetProps::OnAddFormat() 
{
	UpdateData(TRUE);

	if (m_defaultFormats.GetDefaultFormat(m_strMediaName) == -1)
	{
		COutputFormat outputFormat(m_strMediaName, m_fMediaWidth, m_fMediaHeight);
		m_defaultFormats.Add(outputFormat);
		m_mediaNameCombo.AddString(m_strMediaName);
		int nSel = m_mediaNameCombo.FindString(-1, m_strMediaName);
		if (nSel != CB_ERR)
		{
			m_mediaNameCombo.SetCurSel(nSel);
			OnSelchangeMedianameCombo();
		}
	}
}

void CDlgPDFTargetProps::OnRemoveFormat() 
{
	UpdateData(TRUE);

	int nSel = m_mediaNameCombo.GetCurSel();
	if (nSel == CB_ERR)
		return;

	m_defaultFormats.RemoveDefaultFormat(m_strMediaName);

	m_mediaNameCombo.DeleteString(nSel);
	if (nSel >= m_mediaNameCombo.GetCount())
		nSel--;

	m_mediaNameCombo.SetCurSel(nSel);
	OnSelchangeMedianameCombo();
}

void CDlgPDFTargetProps::OnOK() 
{
	UpdateData(TRUE);

	int nPSLevel = ((CComboBox*)GetDlgItem(IDC_PS_LEVEL))->GetCurSel() + 1;

	DestroyWindow();

	m_pdfTargetProps.m_strTargetName			= m_strPDFTargetName;
	m_pdfTargetProps.m_strOutputFilename		= m_strOutputFilename;
	m_pdfTargetProps.m_nPSLevel					= nPSLevel;
	m_pdfTargetProps.m_bEmitHalftones			= ! m_bUsePrinterHalftones;
	m_pdfTargetProps.m_nJDFCompatibility		= m_nJDFCompatibility;
	m_pdfTargetProps.m_bJDFMarksInFolder		= m_bJDFMarksInFolder;
	m_pdfTargetProps.m_strJDFMarksFolder		= m_strJDFMarksFolder;
	m_pdfTargetProps.m_bJDFOutputMime			= m_bJDFOutputMime;
	m_pdfTargetProps.m_bOutputCuttingData		= m_bOutputCuttingData;
	m_pdfTargetProps.m_strCuttingDataFolder 	= m_strCuttingDataFolder;
	m_pdfTargetProps.m_strCuttingDataFilename	= m_strCuttingDataFilename;
	switch (m_pdfTargetProps.m_nType)
	{
	case CPDFTargetProperties::TypePDFFolder:
		if (m_nTargetPathSide == 0)
			m_pdfTargetProps.m_strLocation	   = m_strFolder;	
		else
			m_pdfTargetProps.m_strLocationBack = m_strFolder;	
		break;
	case CPDFTargetProperties::TypePDFPrinter:
		m_pdfTargetProps.m_strLocation	= m_strExposer;	
		break;
	case CPDFTargetProperties::TypeJDFFolder:
		if (m_nTargetPathSide == 0)
			m_pdfTargetProps.m_strLocation	   = m_strFolder;	
		else
			m_pdfTargetProps.m_strLocationBack = m_strFolder;	
		break;
	}

	m_pdfTargetProps.m_mediaList[0].m_strMediaName	= m_strMediaName;		
	m_pdfTargetProps.m_mediaList[0].SetMediaWidth(m_fMediaWidth);	
	m_pdfTargetProps.m_mediaList[0].SetMediaHeight(m_fMediaHeight);
	m_pdfTargetProps.m_mediaList[0].m_bShrinkToFit  = m_bShrinkToFit;
	m_pdfTargetProps.m_mediaList[0].m_bPageFrame	= m_bPageFrameCheck;
	m_pdfTargetProps.m_mediaList[0].m_bMarkFrame	= m_bMarkFrameCheck;
	m_pdfTargetProps.m_mediaList[0].m_bSheetFrame	= m_bSheetFrameCheck;
	m_pdfTargetProps.m_mediaList[0].m_bPlateFrame	= m_bPlateFrameCheck;
	m_pdfTargetProps.m_mediaList[0].m_bBoxShape		= m_bBoxShapeCheck;
	m_pdfTargetProps.m_mediaList[0].m_nAutoAlignTo	= m_nAutoAlignTo;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceX = m_fMediaDistanceX;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_reflinePosParams.m_fDistanceY = m_fMediaDistanceY;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionX					= m_fMediaCorrectionX;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_fCorrectionY					= m_fMediaCorrectionY;
	m_pdfTargetProps.m_mediaList[0].m_posInfos[m_nCurTileIndex].m_nRotation						= m_nMediaRotation;

	m_pfnCallback(this, m_pCallWnd, IDOK);
}

void CDlgPDFTargetProps::OnCancel() 
{
	UpdateData(TRUE);
	if (theApp.m_pMainWnd)
		if (theApp.m_pMainWnd->m_hWnd)
			theApp.m_pMainWnd->SetActiveWindow();
	DestroyWindow();
	m_pfnCallback(this, m_pCallWnd, IDCANCEL);
}

void CDlgPDFTargetProps::OnClose() 
{
	UpdateData(TRUE);
	CDialog::OnClose();
	m_pfnCallback(this, m_pCallWnd, IDCANCEL);
}

void CDlgPDFTargetProps::OnButtonSelectPathPlaceholder() 
{
	m_pathPlaceholderEdit.SetFocus();
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup)
	{
		CRect buttonRect;
		GetDlgItem(IDC_BUTTON_SELECT_PATH_PLACEHOLDER)->GetWindowRect(buttonRect);
		m_nPlaceholderMessageSource = IDC_BUTTON_SELECT_PATH_PLACEHOLDER;
		pPopup->TrackPopupMenu(TPM_RIGHTALIGN | TPM_RIGHTBUTTON, buttonRect.right, buttonRect.bottom, this);
	}

	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER))->SetCheck(0);
}

void CDlgPDFTargetProps::OnButtonSelectFilenamePlaceholder() 
{
	m_fileNamePlaceholderEdit.SetFocus();
	CMenu* pPopup = m_placeholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup)
	{
		CRect buttonRect;
		GetDlgItem(IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER)->GetWindowRect(buttonRect);
		m_nPlaceholderMessageSource = IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER;
		pPopup->TrackPopupMenu(TPM_RIGHTALIGN | TPM_RIGHTBUTTON, buttonRect.right, buttonRect.bottom, this);
	}

	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER))->SetCheck(0);
}

void CDlgPDFTargetProps::OnSelectPlaceholder(int nID)
{
	CMenu* pPopup = NULL;
	switch (m_nPlaceholderMessageSource)
	{
	case IDC_BUTTON_SELECT_PATH_PLACEHOLDER:					
	case IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER:				pPopup = m_placeholderMenu.GetSubMenu(0);				break;
	case IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER:	pPopup = m_cuttingDataPlaceholderMenu.GetSubMenu(0);	break; 
	}

	ASSERT(pPopup != NULL);
	if ( ! pPopup)
		return;

	CString strPlaceholder;
	pPopup->GetMenuString(nID, strPlaceholder, MF_BYCOMMAND);
	strPlaceholder = strPlaceholder.Left(3);
	strPlaceholder.Delete(1, 1);	// remove &

	UpdateData(TRUE);

	int nStartChar, nEndChar;
	switch (m_nPlaceholderMessageSource)
	{
	case IDC_BUTTON_SELECT_PATH_PLACEHOLDER:					((CEdit*)GetDlgItem(IDC_FOLDER)				 )->GetSel(nStartChar, nEndChar);	break;
	case IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER:				((CEdit*)GetDlgItem(IDC_OUTPUT_FILENAME)	 )->GetSel(nStartChar, nEndChar);	break;
	case IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER:	((CEdit*)GetDlgItem(IDC_CUTTINGDATA_FILENAME))->GetSel(nStartChar, nEndChar);	break; 
	}

	if (nStartChar == nEndChar)
		if (nStartChar > 0)
		{
			switch (m_nPlaceholderMessageSource)
			{
			case IDC_BUTTON_SELECT_PATH_PLACEHOLDER:					if (m_strFolder[nStartChar - 1] == '$')					// assume user already typed $ -> remove $ to prevent from being inserted twice
																			nStartChar--;
																		break;
			case IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER:				if (m_strOutputFilename[nStartChar - 1] == '$')			// assume user already typed $ -> remove $ to prevent from being inserted twice
																			nStartChar--;
																		break;
			case IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER:	if (m_strCuttingDataFilename[nStartChar - 1] == '$')	// assume user already typed $ -> remove $ to prevent from being inserted twice
																			nStartChar--;
																		break;
			}
		}

	switch (m_nPlaceholderMessageSource)
	{
	case IDC_BUTTON_SELECT_PATH_PLACEHOLDER:					m_strFolder.Delete(nStartChar, nEndChar - nStartChar);
																m_strFolder.Insert(nStartChar, strPlaceholder);
																break;
	case IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER:				m_strOutputFilename.Delete(nStartChar, nEndChar - nStartChar);
																m_strOutputFilename.Insert(nStartChar, strPlaceholder);
																break;
	case IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER:	m_strCuttingDataFilename.Delete(nStartChar, nEndChar - nStartChar);
																m_strCuttingDataFilename.Insert(nStartChar, strPlaceholder);
																break;
	}

	UpdateData(FALSE);

	switch (m_nPlaceholderMessageSource)
	{
	case IDC_BUTTON_SELECT_PATH_PLACEHOLDER:					((CEdit*)GetDlgItem(IDC_FOLDER))->SetSel(nStartChar + 2, nStartChar + 2);				break;
	case IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER:				((CEdit*)GetDlgItem(IDC_OUTPUT_FILENAME))->SetSel(nStartChar + 2, nStartChar + 2);		break;
	case IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER:	((CEdit*)GetDlgItem(IDC_CUTTINGDATA_FILENAME))->SetSel(nStartChar + 2, nStartChar + 2);	break;
	}

	if (m_nPlaceholderMessageSource == IDC_BUTTON_SELECT_FILENAME_PLACEHOLDER)
		CheckTargetPathSideButtons(this);
}

void CDlgPDFTargetProps::OnPlaceholderFoldsheetNum()		{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSHEETNUM);		}
void CDlgPDFTargetProps::OnPlaceholderSheetside()			{ OnSelectPlaceholder(ID_PLACEHOLDER_SHEETSIDE);		}		
void CDlgPDFTargetProps::OnPlaceholderWebNum()				{ OnSelectPlaceholder(ID_PLACEHOLDER_WEBNUM);			}
void CDlgPDFTargetProps::OnPlaceholderColorNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_COLORNUM);			}	
void CDlgPDFTargetProps::OnPlaceholderFoldsheetNumCG()		{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSHEETNUM_CG);	}
void CDlgPDFTargetProps::OnPlaceholderJobTitle()			{ OnSelectPlaceholder(ID_PLACEHOLDER_JOBTITLE);			}
void CDlgPDFTargetProps::OnPlaceholderTileNum()				{ OnSelectPlaceholder(ID_PLACEHOLDER_TILENUM);			}
void CDlgPDFTargetProps::OnPlaceholderSheetNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_SHEETNUM);			}
void CDlgPDFTargetProps::OnPlaceholderFile()				{ OnSelectPlaceholder(ID_PLACEHOLDER_FILE);				}
void CDlgPDFTargetProps::OnPlaceholderLowpageSide()			{ OnSelectPlaceholder(ID_PLACEHOLDER_LOWPAGE_SIDE);		}
void CDlgPDFTargetProps::OnPlaceholderLowpageSheet()		{ OnSelectPlaceholder(ID_PLACEHOLDER_LOWPAGE_SHEET);	}
void CDlgPDFTargetProps::OnPlaceholderHighpageSide()		{ OnSelectPlaceholder(ID_PLACEHOLDER_HIGHPAGE_SIDE);	}
void CDlgPDFTargetProps::OnPlaceholderHighpageSheet()		{ OnSelectPlaceholder(ID_PLACEHOLDER_HIGHPAGE_SHEET);	}
void CDlgPDFTargetProps::OnPlaceholderPressDevice()			{ OnSelectPlaceholder(ID_PLACEHOLDER_PRESSDEVICE);		}
void CDlgPDFTargetProps::OnPlaceholderJob()					{ OnSelectPlaceholder(ID_PLACEHOLDER_JOB);				}
void CDlgPDFTargetProps::OnPlaceholderJobNum()				{ OnSelectPlaceholder(ID_PLACEHOLDER_JOBNUM);			}
void CDlgPDFTargetProps::OnPlaceholderCustomer()			{ OnSelectPlaceholder(ID_PLACEHOLDER_CUSTOMER);			}
void CDlgPDFTargetProps::OnPlaceholderCustomerNum()			{ OnSelectPlaceholder(ID_PLACEHOLDER_CUSTOMERNUM);		}
void CDlgPDFTargetProps::OnPlaceholderCreator()				{ OnSelectPlaceholder(ID_PLACEHOLDER_CREATOR);			}
void CDlgPDFTargetProps::OnPlaceholderCreationDate()		{ OnSelectPlaceholder(ID_PLACEHOLDER_CREATIONDATE);		}
void CDlgPDFTargetProps::OnPlaceholderLastModified()		{ OnSelectPlaceholder(ID_PLACEHOLDER_LASTMODIFIED);		}
void CDlgPDFTargetProps::OnPlaceholderScheduledToPrint()	{ OnSelectPlaceholder(ID_PLACEHOLDER_SCHEDULEDTOPRINT);	}
void CDlgPDFTargetProps::OnPlaceholderOutputDate()			{ OnSelectPlaceholder(ID_PLACEHOLDER_OUTPUTDATE);		}
void CDlgPDFTargetProps::OnPlaceholderNotes()				{ OnSelectPlaceholder(ID_PLACEHOLDER_NOTES);			}
void CDlgPDFTargetProps::OnPlaceholderColorName()			{ OnSelectPlaceholder(ID_PLACEHOLDER_COLORNAME);		}
void CDlgPDFTargetProps::OnPlaceholderPageNumList()			{ OnSelectPlaceholder(ID_PLACEHOLDER_PAGENUMLIST);		}
void CDlgPDFTargetProps::OnPlaceholderPageNum()				{ OnSelectPlaceholder(ID_PLACEHOLDER_PAGENUM);			}
void CDlgPDFTargetProps::OnPlaceholderProductName()			{ OnSelectPlaceholder(ID_PLACEHOLDER_PRODUCTNAME);		}
void CDlgPDFTargetProps::OnPlaceholderProductPartName()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PRODUCTPARTNAME);	}
void CDlgPDFTargetProps::OnPlaceholderPaperGrain()			{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERGRAIN);		}
void CDlgPDFTargetProps::OnPlaceholderPaperType()			{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERTYPE);		}
void CDlgPDFTargetProps::OnPlaceholderPaperGrammage()		{ OnSelectPlaceholder(ID_PLACEHOLDER_PAPERGRAMMAGE);	}
void CDlgPDFTargetProps::OnPlaceholderWorkStyle()			{ OnSelectPlaceholder(ID_PLACEHOLDER_WORKSTYLE);		}
void CDlgPDFTargetProps::OnPlaceholderFoldScheme()			{ OnSelectPlaceholder(ID_PLACEHOLDER_FOLDSCHEME);		}


BOOL CDlgPDFTargetProps::OnToolTipNeedText(UINT /*id*/, NMHDR * pNMHDR, LRESULT * /*pResult*/)
{
	CString strPlaceholder;
	CMenu* pPopup = NULL;

	for (int i = 0; i < 3; i++)
	{
		switch (i)
		{
		case 0:	strPlaceholder = m_pathPlaceholderEdit.GetPlaceholderHit();
				pPopup = m_placeholderMenu.GetSubMenu(0);				
				break;
		case 1:	strPlaceholder = m_fileNamePlaceholderEdit.GetPlaceholderHit();
				pPopup = m_placeholderMenu.GetSubMenu(0);				
				break;
		case 2:	strPlaceholder = m_cuttingDataPlaceholderEdit.GetPlaceholderHit();
				pPopup = m_cuttingDataPlaceholderMenu.GetSubMenu(0);	
				break; 
		}
		if ( ! pPopup)
			continue;
		if (strPlaceholder.IsEmpty())
			continue;

		TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
		pTTT->szText[0] = 0;
		strPlaceholder.Insert(1, '&');
		for (UINT i = 0; i < pPopup->GetMenuItemCount(); i++)
		{
			CString strMenu;
			pPopup->GetMenuString(i, strMenu, MF_BYPOSITION);
			if (strPlaceholder == strMenu.Left(3))
			{
				strMenu = strMenu.Right(strMenu.GetLength() - 4);
				_tcscpy(pTTT->szText, strMenu);
				return TRUE;
			}
		}
	}

	return FALSE;
}

void CDlgPDFTargetProps::OnTargetpathFront() 
{
	UpdateData(TRUE);
	m_pdfTargetProps.m_strLocationBack = m_strFolder;	
	m_strFolder = m_pdfTargetProps.m_strLocation;
	UpdateData(FALSE);
}

void CDlgPDFTargetProps::OnTargetpathBack() 
{
	UpdateData(TRUE);
	m_pdfTargetProps.m_strLocation = m_strFolder;	
	m_strFolder = m_pdfTargetProps.m_strLocationBack;
	UpdateData(FALSE);
}

void CDlgPDFTargetProps::OnAlign2Plate()
{
	UpdateData(TRUE);

	switch (m_nAutoAlignTo)
	{
	case CMediaSize::AlignToPlate:
		{
			m_nAutoAlignTo = CMediaSize::AlignNone;
			((CButton*)GetDlgItem(IDC_ALIGN2PLATE))->SetCheck(0);	
			((CButton*)GetDlgItem(IDC_ALIGN2SHEET))->SetCheck(0);	

			GetDlgItem(IDC_MEDIANAME_COMBO)->EnableWindow(TRUE);
			GetDlgItem(IDC_MEDIA_WIDTH)->EnableWindow(TRUE);
			GetDlgItem(IDC_WIDTH_SPIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_MEDIA_HEIGHT)->EnableWindow(TRUE);
			GetDlgItem(IDC_HEIGHT_SPIN)->EnableWindow(TRUE);
		}
		break;
	case CMediaSize::AlignNone:
	case CMediaSize::AlignToSheet:
		{
			m_nAutoAlignTo = CMediaSize::AlignToPlate;
			((CButton*)GetDlgItem(IDC_ALIGN2PLATE))->SetCheck(1);	
			((CButton*)GetDlgItem(IDC_ALIGN2SHEET))->SetCheck(0);	

			GetDlgItem(IDC_MEDIANAME_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_MEDIA_WIDTH)->EnableWindow(FALSE);
			GetDlgItem(IDC_WIDTH_SPIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_MEDIA_HEIGHT)->EnableWindow(FALSE);
			GetDlgItem(IDC_HEIGHT_SPIN)->EnableWindow(FALSE);

			if (m_nOutputTarget == CPDFTargetList::PrintSheetTarget)
			{
				CPrintSheet* pPrintSheet = NULL;
				CPlate*		 pPlate		 = NULL;
				int			 nSide		 = FRONTSIDE;

				GetCurrentSelection(&pPrintSheet, &pPlate, nSide);
				if ( ! pPrintSheet)
					return;
				CLayout* pLayout = pPrintSheet->GetLayout();
				if ( ! pLayout)
					return;
				CPressDevice* pPressDevice = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.GetPressDevice() : pLayout->m_BackSide.GetPressDevice();
				if ( ! pPressDevice)
					return;

				m_fMediaWidth  = pPressDevice->m_fPlateWidth;
				m_fMediaHeight = pPressDevice->m_fPlateHeight;
				m_pdfTargetProps.m_mediaList[0].SetMediaWidth(m_fMediaWidth);	
				m_pdfTargetProps.m_mediaList[0].SetMediaHeight(m_fMediaHeight);
				m_strMediaName.Format(_T("%.1f x %.1f"), m_fMediaWidth, m_fMediaHeight);
			}
		}
		break;
	case CMediaSize::AlignToFoldSheet:	
		break;
	}
	m_pdfTargetProps.m_mediaList[0].SetAlign(m_nAutoAlignTo);

	UpdateData(FALSE);
	UpdatePrintSheetView();
}

void CDlgPDFTargetProps::OnAlign2Sheet()
{
	UpdateData(TRUE);

	switch (m_nAutoAlignTo)
	{
	case CMediaSize::AlignToSheet:
	case CMediaSize::AlignToFoldSheet:
		{
			m_nAutoAlignTo = CMediaSize::AlignNone;
			((CButton*)GetDlgItem(IDC_ALIGN2PLATE))->SetCheck(0);	
			((CButton*)GetDlgItem(IDC_ALIGN2SHEET))->SetCheck(0);	

			GetDlgItem(IDC_MEDIANAME_COMBO)->EnableWindow(TRUE);
			GetDlgItem(IDC_MEDIA_WIDTH)->EnableWindow(TRUE);
			GetDlgItem(IDC_WIDTH_SPIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_MEDIA_HEIGHT)->EnableWindow(TRUE);
			GetDlgItem(IDC_HEIGHT_SPIN)->EnableWindow(TRUE);
		}
		break;
	case CMediaSize::AlignNone:
	case CMediaSize::AlignToPlate:
		{
			m_nAutoAlignTo = (m_nOutputTarget == CPDFTargetList::PrintSheetTarget) ? CMediaSize::AlignToSheet : ( (m_nOutputTarget == CPDFTargetList::FoldSheetTarget) ? CMediaSize::AlignToSheet : CMediaSize::AlignNone);

			((CButton*)GetDlgItem(IDC_ALIGN2PLATE))->SetCheck(0);	
			((CButton*)GetDlgItem(IDC_ALIGN2SHEET))->SetCheck(1);	

			GetDlgItem(IDC_MEDIANAME_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_MEDIA_WIDTH)->EnableWindow(FALSE);
			GetDlgItem(IDC_WIDTH_SPIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_MEDIA_HEIGHT)->EnableWindow(FALSE);
			GetDlgItem(IDC_HEIGHT_SPIN)->EnableWindow(FALSE);

			if (m_nOutputTarget == CPDFTargetList::PrintSheetTarget)
			{
				CPrintSheet* pPrintSheet = NULL;
				CPlate*		 pPlate		 = NULL;
				int			 nSide		 = FRONTSIDE;

				GetCurrentSelection(&pPrintSheet, &pPlate, nSide);
				if ( ! pPrintSheet)
					return;
				CLayout* pLayout = pPrintSheet->GetLayout();
				if ( ! pLayout)
					return;

				m_fMediaWidth  = pLayout->m_FrontSide.m_fPaperWidth;	
				m_fMediaHeight = pLayout->m_FrontSide.m_fPaperHeight;
				m_pdfTargetProps.m_mediaList[0].SetMediaWidth(m_fMediaWidth);	
				m_pdfTargetProps.m_mediaList[0].SetMediaHeight(m_fMediaHeight);
				m_strMediaName.Format(_T("%.1f x %.1f"), m_fMediaWidth, m_fMediaHeight);
			}
		}
		break;
	}
	m_pdfTargetProps.m_mediaList[0].SetAlign(m_nAutoAlignTo);

	UpdateData(FALSE);
	UpdatePrintSheetView();
}

void CDlgPDFTargetProps::OnBnClickedButtonFilenamePlaceholderInfo()
{
	CDlgPlaceholderSyntaxInfo dlg;
	dlg.DoModal();
}

void CDlgPDFTargetProps::OnMaskInFolderCheck()
{
	UpdateData(TRUE);

	GetDlgItem(IDC_TP_MARKSINFOLDER_EDIT)->EnableWindow(m_bJDFMarksInFolder);
	GetDlgItem(IDC_BROWSE_MARKFILE_FOLDER)->EnableWindow(m_bJDFMarksInFolder);
}

void CDlgPDFTargetProps::OnOutputMimeCheck()
{
	UpdateData(TRUE);

	GetDlgItem(IDC_TP_MARKSINFOLDER_CHECK)->ShowWindow((m_bJDFOutputMime) ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_TP_MARKSINFOLDER_EDIT)->ShowWindow((m_bJDFOutputMime)  ? SW_HIDE : SW_SHOW);
	GetDlgItem(IDC_BROWSE_MARKFILE_FOLDER)->ShowWindow((m_bJDFOutputMime) ? SW_HIDE : SW_SHOW);

	CString strExtension = m_strOutputFilename.Right(4);
	if (m_bJDFOutputMime)
	{
		if (strExtension.CompareNoCase(_T(".jdf")) == 0)
		{
			m_strOutputFilename.Truncate(m_strOutputFilename.GetLength() - 3);
			m_strOutputFilename += "mjd";
			UpdateData(FALSE);
		}
	}
	else
	{
		if (strExtension.CompareNoCase(_T(".mjd")) == 0)
		{
			m_strOutputFilename.Truncate(m_strOutputFilename.GetLength() - 3);
			m_strOutputFilename += "jdf";
			UpdateData(FALSE);
		}
	}
}

void CDlgPDFTargetProps::OnJDFOutputDetails()
{
	CDlgJDFOutputDetails dlg;

	dlg.m_strJDFJobIDMap			= m_pdfTargetProps.m_strJDFJobIDMap;
	dlg.m_strJDFJobPartIDMap		= m_pdfTargetProps.m_strJDFJobPartIDMap;
	dlg.m_strJDFDescriptiveNameMap	= m_pdfTargetProps.m_strJDFJobDescriptiveNameMap;
	dlg.m_strApogeeTrappingSet		= GetKeyValue(m_pdfTargetProps.m_apogeeParameterSets, _T("Trapping"));
	dlg.m_strApogeeSeparationSet	= GetKeyValue(m_pdfTargetProps.m_apogeeParameterSets, _T("Separation"));
	dlg.m_strApogeeRenderingSet		= GetKeyValue(m_pdfTargetProps.m_apogeeParameterSets, _T("Rendering"));
	dlg.m_strApogeeScreeningSet		= GetKeyValue(m_pdfTargetProps.m_apogeeParameterSets, _T("Screening"));
	dlg.m_strApogeePrintSet			= GetKeyValue(m_pdfTargetProps.m_apogeeParameterSets, _T("print"));
	dlg.m_strApogeeOutputSet		= GetKeyValue(m_pdfTargetProps.m_apogeeParameterSets, _T("output"));
	dlg.m_strApogeeImageSet			= GetKeyValue(m_pdfTargetProps.m_apogeeParameterSets, _T("image"));

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_pdfTargetProps.m_strJDFJobIDMap							 = dlg.m_strJDFJobIDMap;
	m_pdfTargetProps.m_strJDFJobPartIDMap						 = dlg.m_strJDFJobPartIDMap;
	m_pdfTargetProps.m_strJDFJobDescriptiveNameMap				 = dlg.m_strJDFDescriptiveNameMap;
	m_pdfTargetProps.m_apogeeParameterSets.RemoveAll();
	m_pdfTargetProps.m_apogeeParameterSets.Add(_T("Trapping ")	 + dlg.m_strApogeeTrappingSet);
	m_pdfTargetProps.m_apogeeParameterSets.Add(_T("Separation ") + dlg.m_strApogeeSeparationSet);
	m_pdfTargetProps.m_apogeeParameterSets.Add(_T("Rendering ")  + dlg.m_strApogeeRenderingSet);
	m_pdfTargetProps.m_apogeeParameterSets.Add(_T("Screening ")  + dlg.m_strApogeeScreeningSet);
	m_pdfTargetProps.m_apogeeParameterSets.Add(_T("print ")		 + dlg.m_strApogeePrintSet);
	m_pdfTargetProps.m_apogeeParameterSets.Add(_T("output ")	 + dlg.m_strApogeeOutputSet);
	m_pdfTargetProps.m_apogeeParameterSets.Add(_T("image ")		 + dlg.m_strApogeeImageSet);
}

void CDlgPDFTargetProps::OnBookblockDoublePage() 
{
	m_nAutoAlignTo = CMediaSize::AlignToBookblockDoublePage;
}

void CDlgPDFTargetProps::OnBookblockSinglePage() 
{
	m_nAutoAlignTo = CMediaSize::AlignToBookblockSinglePage;
}

void CDlgPDFTargetProps::OnCuttingDataBrowseFolder() 
{
	CDlgDirPicker dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, _T(" |*.___||"), this);
	CString strTitle;
	strTitle.LoadString(IDS_DIRECTORY_STRING);
	dlg.m_ofn.lpstrTitle	  = strTitle;
	dlg.m_ofn.lpstrInitialDir = (m_strCuttingDataFolder.IsEmpty()) ? (LPCSTR)theApp.settings.m_szWorkDir : m_strCuttingDataFolder;

	if (dlg.DoModal() == IDOK)
		m_strCuttingDataFolder = dlg.m_strPath;

	GetDlgItem(IDC_APPLY_BUTTON)->SetFocus();
	((CButton*)GetDlgItem(IDC_APPLY_BUTTON))->SetButtonStyle(BS_DEFPUSHBUTTON);
	((CButton*)GetDlgItem(IDC_BROWSE_FOLDER))->SetButtonStyle(BS_PUSHBUTTON);

	UpdateData(FALSE);
}

void CDlgPDFTargetProps::OnButtonSelectCuttingDataFilenamePlaceholder() 
{
	m_cuttingDataPlaceholderEdit.SetFocus();
	CMenu* pPopup = m_cuttingDataPlaceholderMenu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup)
	{
		CRect buttonRect;
		GetDlgItem(IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER)->GetWindowRect(buttonRect);
		m_nPlaceholderMessageSource = IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER;
		pPopup->TrackPopupMenu(TPM_RIGHTALIGN | TPM_RIGHTBUTTON, buttonRect.right, buttonRect.bottom, this);
	}

	((CButton*)GetDlgItem(IDC_BUTTON_SELECT_CUTTINGDATA_FILENAME_PLACEHOLDER))->SetCheck(0);
}

void CDlgPDFTargetProps::OnBnClickedButtonCuttingDataFilenamePlaceholderInfo()
{
	CDlgPlaceholderSyntaxInfo dlg;
	dlg.DoModal();
}
