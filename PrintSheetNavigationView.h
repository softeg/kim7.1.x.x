#pragma once

#include "DlgSelectPressDevicePopup.h"
#include "DlgSelectFoldSchemePopup.h"
#include "PrintSheetViewStrippingData.h"


// CPrintSheetNavigationView-Ansicht

class CPrintSheetNavigationView : public CScrollView
{
	DECLARE_DYNCREATE(CPrintSheetNavigationView)

protected:
	CPrintSheetNavigationView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetNavigationView();

public:
	DECLARE_DISPLAY_LIST(CPrintSheetNavigationView, OnMultiSelect, YES, OnStatusChanged, YES)
	DECLARE_DISPLAY_ITEM(CPrintSheetNavigationView, LayoutBar,		OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetNavigationView, SheetFrame,		OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
																																									    

public:
	CSize								m_docTotalSize, m_docPageSize, m_docLineSize;
	CFont								m_boldFont, m_smallFont;
	int									m_nToolBarHeight;
	CBitmap								m_bmDIBackground;
	CRect								m_rcDIBackground;
	CDlgSelectPressDevicePopup			m_dlgPressDeviceSelector;
	int									m_nProductPartIndex;
	COleDropTarget						m_dropTarget;
	CDisplayItem						m_selectedDI;
	int									m_nBottomRow1, m_nBottomRow2;
	CArray<CPrintSheet*, CPrintSheet*>	m_drawnPrintSheets;
	int									m_nActPrintSheetIndex, m_nLastPrintSheetIndex, m_nLastComponentIndex;
	CPoint								m_ptMouse;
	CRect								m_catchRect;


public:
	CRect						Draw	  (CDC* pDC, CDisplayList* pDisplayList = NULL);
	CRect						DrawSheets(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	BOOL						IsOpenDlgComponentImposer();
	CDlgComponentImposer*		GetDlgComponentImposer();
	BOOL						IsOpenDlgFoldSchemeSelector();
	CPrintSheet*				GetActPrintSheet();
	CLayout*					GetActLayout();
	CPressDevice*				GetActPressDevice();
	int							GetActSight();
	BOOL						SetActPrintSheet(CPrintSheet* pActPrintSheet, BOOL bNoUpdate = FALSE);
	void						EnsureVisiblePrintSheet(CPrintSheet* pPrintSheet);
	CPrintComponent				GetActPrintComponent();
	void						SetActComponent(CPrintSheet* pNewPrintSheet, int nNewComponentIndex, BOOL bNoUpdate = FALSE);
	void						SetActFlatProductComponent(CPrintSheet* pPrintSheet, int nFlatProductIndex, BOOL bNoUpdate = FALSE);
	CSize						CalcExtents();
	void						ClipHeader(CDC* pDC);
	void						SaveBitmap(CBitmap* pBitmap, const CRect& rect);
	void						RestoreBitmap(CBitmap* pBitmap, CRect rect, BOOL bInflate = FALSE);
	void						UpdatePrintSheets(CLayout* pLayout);
	void						InvalidateSheetFrame(CPrintSheet* pPrintSheet);
	DROPEFFECT					OnDragEnterPrintComponentFoldSheet(CDisplayItem& sourceDI, DWORD dwKeyState, CPoint point);
	DROPEFFECT					OnDragEnterPrintComponentPageSection(CDisplayItem& sourceDI, DWORD dwKeyState, CPoint point);
	DROPEFFECT					OnDragOverPrintComponentFoldSheet(CDisplayItem& sourceDI, DWORD dwKeyState, CPoint point);


public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	virtual void OnInitialUpdate();     // Erster Aufruf nach Erstellung
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
protected:
	virtual void ScrollToPosition(POINT pt);
	virtual void ScrollToDevicePosition(POINT ptDev);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
	virtual DROPEFFECT OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	virtual void OnDragLeave();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};


