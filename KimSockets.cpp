// KimSockets.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "KimSocketsCommon.h"
#include "KimSockets.h"
#include "AutoServerFrame.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "MainFrm.h"


// CClientSocket

CClientSocket::CClientSocket()
{
	m_bConnectionRefused = FALSE;
}

CClientSocket::~CClientSocket()
{
}

void CClientSocket::OnConnect(int nErrorCode) 
{
	if (nErrorCode != 0)
	{
		  //switch(nErrorCode)
		  //{
			 //case WSAEADDRINUSE: 
				//AfxMessageBox(_T("The specified address is already in use.\n"));
				//break;
			 //case WSAEADDRNOTAVAIL: 
				//AfxMessageBox(_T("The specified address is not available from ")
				//_T("the local machine.\n"));
				//break;
			 //case WSAEAFNOSUPPORT: 
				//AfxMessageBox(_T("Addresses in the specified family cannot be ")
				//_T("used with this socket.\n"));
				//break;
			 //case WSAECONNREFUSED: 
				//AfxMessageBox(_T("The attempt to connect was forcefully rejected.\n"));
				//break;
			 //case WSAEDESTADDRREQ: 
				//AfxMessageBox(_T("A destination address is required.\n"));
				//break;
			 //case WSAEFAULT: 
				//AfxMessageBox(_T("The lpSockAddrLen argument is incorrect.\n"));
				//break;
			 //case WSAEINVAL: 
				//AfxMessageBox(_T("The socket is already bound to an address.\n"));
				//break;
			 //case WSAEISCONN: 
				//AfxMessageBox(_T("The socket is already connected.\n"));
				//break;
			 //case WSAEMFILE: 
				//AfxMessageBox(_T("No more file descriptors are available.\n"));
				//break;
			 //case WSAENETUNREACH: 
				//AfxMessageBox(_T("The network cannot be reached from this host ")
				//_T("at this time.\n"));
				//break;
			 //case WSAENOBUFS: 
				//AfxMessageBox(_T("No buffer space is available. The socket ")
				//   _T("cannot be connected.\n"));
				//break;
			 //case WSAENOTCONN: 
				//AfxMessageBox(_T("The socket is not connected.\n"));
				//break;
			 //case WSAENOTSOCK: 
				//AfxMessageBox(_T("The descriptor is a file, not a socket.\n"));
				//break;
			 //case WSAETIMEDOUT: 
				//AfxMessageBox(_T("The attempt to connect timed out without ")
				//   _T("establishing a connection. \n"));
				//break;
			 //default:
				//TCHAR szError[256];
				//_stprintf_s(szError, _T("OnConnect error: %d"), nErrorCode);
				//AfxMessageBox(szError);
				//break;
		  //}
	}
	else
	{
		theApp.m_kaDB.kA_Connect("kimAutoDB", "", theApp.settings.m_szAutoServerNameIP, "SYSDBA", "masterkey");
		theApp.m_bServerConnected = TRUE;
		theApp.UpdateAutoServerJobsView();
		theApp.UpdateAutoServerWorkflowsView();
		theApp.UpdateAutoServerLogsView();
		theApp.UpdateAutoServerDetailsView();
		if ((CMainFrame*)theApp.m_pMainWnd)
		{
			((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.Invalidate();
			((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.UpdateWindow();
		}
	}

	CAsyncSocket::OnConnect(nErrorCode);
}

int CClientSocket::CommandToServer(int nCmd, int nParam1, int nParam2)
{
	SOCKETCOMMAND serverCmd = {0};
	serverCmd.nCommand = nCmd;
	serverCmd.param1   = nParam1;
	serverCmd.param2   = nParam2;

	Send((void*)&serverCmd, sizeof(SOCKETCOMMAND));

	return 0;
}

int CClientSocket::SendMessageToServer(const CString& strMsg)
{
	//SOCKETCOMMAND cmd;
	//cmd.nCommand = 111;
	//memcpy((void*)cmd.param1, (void*)szMsg, 1000);

	//Send((void*)&cmd, sizeof(SOCKETCOMMAND));

	return 0;
}

void CClientSocket::OnReceive(int nErrorCode) 
{
	SOCKETCOMMAND cmd;
	int nRead;
	nRead = Receive((void*)&cmd, sizeof(SOCKETCOMMAND)); 
	
	if (nRead == 0)
		return;
	if (nRead == SOCKET_ERROR)
	{
		if (GetLastError() != WSAEWOULDBLOCK) 
		{
			AfxMessageBox(_T("Socket Error occurred"));
			return;
		}
	}

	CString			  strServerNameIP = theApp.settings.m_szAutoServerNameIP;
	BOOL			  bUpdateViews = FALSE;
	CAutoServerFrame* pFrame	   = theApp.GetAutoServerFrame();
	switch (cmd.nCommand)
	{
	//case MSG_CONNECTION_REFUSED:		if ( ! m_bConnectionRefused)	// if connection has already been refused -> dont't rise message again
	//									{
	//										m_bConnectionRefused = TRUE;
	//										CString strMsg; strMsg.LoadString(IDS_MAXCLIENTS_EXCEEDED);
	//										AfxMessageBox(strMsg);	
	//										CheckDonglePDF(TRUE, TRUE);
	//										bUpdateViews = TRUE;
	//										_tcscpy_s(theApp.GetDataFolder(), _MAX_PATH, theApp.m_strClientOldDataFolder.GetBuffer());
	//									}
	//									break;
	case MSG_CONNECTION_REFUSED:
	case MSG_CONNECTION_ACCEPTED:		m_bConnectionRefused	  = FALSE;
										theApp.m_bServerConnected = TRUE;
										theApp.m_kaDB.kA_Connect("kimAutoDB", "", theApp.settings.m_szAutoServerNameIP, "SYSDBA", "masterkey");
										CheckDonglePDF(TRUE, TRUE);	// check, because maybe we are in demo mode, but server allows to connect -> switch to client mode
										bUpdateViews = TRUE;
										break;
	case MSG_JOB_DELETED:	
	case MSG_JOB_CHANGED:				if (pFrame)
											pFrame->UpdateJobViews(cmd.param1);
										theApp.UpdateAutoServerLogsView();
										break;
	case MSG_JOB_ABORT:					theApp.m_bKimEngineAbort = TRUE;
										break;
	case MSG_JOB_STATUS_CHANGED:		theApp.UpdateAutoServerJobsView();
										break;
	case MSG_PRODUCT_STATUS_CHANGED:	
	case MSG_INPUTPDF_STATUS_CHANGED:	
	case MSG_OUTPUTPDF_STATUS_CHANGED:	if (pFrame)
										{
											if (cmd.param1 >= 0)	// job id
											{
												int nActJobID = pFrame->GetActJobID();
												if (cmd.param1 == nActJobID)
													theApp.UpdateAutoServerDetailsView();
											}
											else
												theApp.UpdateAutoServerDetailsView();
										}
										break;
	case MSG_WORKFLOW_DELETED:		
	case MSG_WORKFLOW_CHANGED:			theApp.UpdateAutoServerWorkflowsView();
										break;
	case MSG_MESSAGELOGS_CHANGED:		theApp.UpdateAutoServerLogsView();
										break;
	case MSG_MESSAGELOGS_DELETED:		theApp.UpdateAutoServerLogsView();
										break;
	}

	if (bUpdateViews)
	{
		theApp.UpdateAutoServerJobsView();
		theApp.UpdateAutoServerWorkflowsView();
		theApp.UpdateAutoServerLogsView();
		theApp.UpdateAutoServerDetailsView();
		if ((CMainFrame*)theApp.m_pMainWnd)
		{
			((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.Invalidate();
			((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.UpdateWindow();
		}
	}

	CAsyncSocket::OnReceive(nErrorCode);
}

void CClientSocket::OnClose(int nErrorCode) 
{
	theApp.AutoServerDisconnect();

	theApp.UpdateAutoServerJobsView();
	theApp.UpdateAutoServerWorkflowsView();
	theApp.UpdateAutoServerLogsView();
	theApp.UpdateAutoServerDetailsView();
	if ((CMainFrame*)theApp.m_pMainWnd)
	{
		((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.Invalidate();
		((CMainFrame*)theApp.m_pMainWnd)->m_wndStatusBar.UpdateWindow();
	}

	CAsyncSocket::OnClose(nErrorCode);
}