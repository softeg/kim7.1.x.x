#if !defined(AFX_DLGOBJECTPREVIEW_H__96121F2D_7B66_4B39_9F0D_23A8B973DAF5__INCLUDED_)
#define AFX_DLGOBJECTPREVIEW_H__96121F2D_7B66_4B39_9F0D_23A8B973DAF5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgObjectPreview.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgObjectPreview 

class CDlgObjectPreview : public CDialog
{
// Konstruktion
public:
	CDlgObjectPreview(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgObjectPreview)
	enum { IDD = IDD_OBJECT_PREVIEW };
	CStatic	m_objectPreviewFrame;
	CScrollBar	m_scrollbarV;
	CScrollBar	m_scrollbarH;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectProperties* m_pParent;
protected:
	CSize							  m_sizeViewport;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgObjectPreview)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void PrepareUpdatePreview();

protected:
	void GetBoxes(CRect& outBBox, CRect& trimBox);

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgObjectPreview)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGOBJECTPREVIEW_H__96121F2D_7B66_4B39_9F0D_23A8B973DAF5__INCLUDED_
