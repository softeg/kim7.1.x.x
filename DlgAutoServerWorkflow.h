#pragma once
#include "afxwin.h"


// CDlgAutoServerWorkflow-Dialogfeld

class CDlgAutoServerWorkflow : public CDialog
{
	DECLARE_DYNAMIC(CDlgAutoServerWorkflow)

public:
	CDlgAutoServerWorkflow(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgAutoServerWorkflow();

// Dialogfelddaten
	enum { IDD = IDD_AUTOSERVER_WORKFLOW };

	int m_nDBAttribute;
	int m_nDBActionWhenFinished;
	int m_nDBActionDelayMinutes;


public:
	CString m_strName;
	int m_nJobdataSource;
	CString m_strProductionProfile;
	CComboBox m_productionProfileCombo;
	int m_nPDFPreviewSettings;
	int m_nDataOutputPrintSheets;
	int m_nDataOutputFoldSheets;
	int m_nDataOutputBooklets;
	int m_nDataOutputResults;
	int m_nActionImpose;
	int m_nActionWhenFinished;
	int m_nActionDelay;
	int m_nActionDelayUnit;

protected:
	DECLARE_MESSAGE_MAP()
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedWfProductionProfileButton();
	afx_msg void OnCbnSelchangeWfActionFinished();
	afx_msg void OnBnClickedOk();
};
