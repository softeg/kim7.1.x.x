// DlgToolBarPopup.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgToolBarPopup.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgToolBarPopup 


CDlgToolBarPopup::CDlgToolBarPopup(CPoint ptDlgTopLeft, int nIDColdToolBar, int nIDHotToolBar, 
								   CArray <int, int>& stringIDs, 
								   CSize sizeBitmap, int nBitmaps, int nRows = 1, int nTBStyle = TBSTYLE_FLAT, CWnd* pParent)
	: CDialog(CDlgToolBarPopup::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgToolBarPopup)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	m_ptDlgTopLeft		= ptDlgTopLeft;
	m_nIDColdToolBar	= nIDColdToolBar;
	m_nIDHotToolBar		= nIDHotToolBar;
	m_stringIDs.RemoveAll();
	for (int i = 0; i < stringIDs.GetSize(); i++)
		m_stringIDs.Add(stringIDs[i]);
	m_sizeBitmap		= sizeBitmap;
	m_nBitmaps			= nBitmaps;
	m_nRows				= (nRows > 0) ? nRows : 1;
	m_nTBStyle			= nTBStyle;
	m_pParent			= pParent;

	// verify m_nRows
	if (m_nRows > m_nBitmaps)
		m_nRows = m_nBitmaps;
}


void CDlgToolBarPopup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgToolBarPopup)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgToolBarPopup, CDialog)
	//{{AFX_MSG_MAP(CDlgToolBarPopup)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgToolBarPopup 

BOOL CDlgToolBarPopup::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_toolBarCtrl.Create(WS_VISIBLE | WS_CHILD | CCS_NODIVIDER | CCS_TOP | TBSTYLE_WRAPABLE | TBSTYLE_TRANSPARENT | m_nTBStyle, 
						 CRect(0,0,0,0), this, m_nIDColdToolBar);

	m_toolBarCtrl.SetBitmapSize(m_sizeBitmap);

	VERIFY(m_toolBarCtrl.AddBitmap(m_nBitmaps, m_nIDColdToolBar) != -1);

	m_pTBButtons = new TBBUTTON[m_nBitmaps];

	CString strText;
	int		nStringID;
	for (int nIndex = 0; (nIndex < m_nBitmaps) && (nIndex < m_stringIDs.GetSize()); nIndex++)
	{
		nStringID = m_stringIDs[nIndex];
		strText.LoadString(nStringID);
		strText.Insert(0, _T("  ")); strText += "  ";
		int nStringLength = strText.GetLength() + 1;
		TCHAR * pString   = strText.GetBufferSetLength(nStringLength);	pString[nStringLength] = 0;
		strText.ReleaseBuffer();

		m_pTBButtons[nIndex].fsState   = TBSTATE_ENABLED;
		m_pTBButtons[nIndex].fsStyle   = TBSTYLE_BUTTON;		
		m_pTBButtons[nIndex].dwData    = 0;
		m_pTBButtons[nIndex].iBitmap   = nIndex;
		m_pTBButtons[nIndex].iString   = m_toolBarCtrl.AddStrings(pString);
		m_pTBButtons[nIndex].idCommand = nStringID;
	}

	VERIFY(m_toolBarCtrl.AddButtons(m_nBitmaps, m_pTBButtons));

	if (m_nIDHotToolBar != -1)
	{
		CImageList img;
		img.Create(m_nIDHotToolBar, m_sizeBitmap.cx, 0, LIGHTGRAY);
		m_toolBarCtrl.SetHotImageList(&img);
		img.Detach();
	}

	CRect rect;
	m_toolBarCtrl.GetItemRect(0, rect);	// all buttons has same size
	CSize buttonSize(rect.Width(), rect.Height());
	int nX = m_nBitmaps / m_nRows + ((m_nBitmaps % m_nRows) ? 1 : 0);
	int nY = m_nBitmaps / nX	  + ((m_nBitmaps % m_nRows) ? 1 : 0);
	MoveWindow(m_ptDlgTopLeft.x, m_ptDlgTopLeft.y, (nX * buttonSize.cx) + 6, (nY * buttonSize.cy) + 6, FALSE);
	
	m_toolBarCtrl.AutoSize();
	
	m_toolBarCtrl.SetCapture();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

BOOL CDlgToolBarPopup::PreTranslateMessage(MSG* pMsg) 
{
	// If we get a keyboard or mouse message, hide the toolbar.
	if (pMsg->message == WM_LBUTTONDOWN   || pMsg->message == WM_RBUTTONDOWN   || pMsg->message == WM_MBUTTONDOWN ||
	    pMsg->message == WM_NCLBUTTONDOWN || pMsg->message == WM_NCRBUTTONDOWN || pMsg->message == WM_NCMBUTTONDOWN)
	{
		CPoint ptMouse;
		GetCursorPos(&ptMouse);
		CRect rect;
		GetWindowRect(rect);
		if ( ! rect.PtInRect(ptMouse))
		{
			EndDialog(IDCANCEL);
			return TRUE;	// message handled here
		}
	}
	else
		if (pMsg->message == WM_KEYDOWN || pMsg->message == WM_SYSKEYDOWN)
		{
			EndDialog(IDCANCEL);
			return TRUE;	// message handled here
		}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgToolBarPopup::OnDestroy() 
{
	CDialog::OnDestroy();
	
	ReleaseCapture();
}

BOOL CDlgToolBarPopup::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	EndDialog(wParam);

	return CDialog::OnCommand(wParam, lParam);
}
