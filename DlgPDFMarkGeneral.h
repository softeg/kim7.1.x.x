#if !defined(AFX_DLGPDFMARKGENERAL_H__68AB1A03_B0D5_11D5_88B7_0040F68C1EF7__INCLUDED_)
#define AFX_DLGPDFMARKGENERAL_H__68AB1A03_B0D5_11D5_88B7_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPDFMarkGeneral.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFMarkGeneral 

class CDlgPDFMarkGeneral : public CDialog
{
// Konstruktion
public:
	CDlgPDFMarkGeneral(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgPDFMarkGeneral)
	enum { IDD = IDD_PDFMARK_GENERAL };
	CSpinButtonCtrl	m_widthSpin;
	CSpinButtonCtrl	m_heightSpin;
	float	m_fMarkHeight;
	float	m_fMarkWidth;
	//}}AFX_DATA

public:
	class CDlgMarkProperties*	m_pParent;
	BYTE						m_nHeadPosition;
	CBitmap						m_bmMarkOrientation;
	int							m_nMarkType;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPDFMarkGeneral)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void		   Apply();

protected:
	CLayoutObject* GetMarkObject();


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPDFMarkGeneral)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposMarkOrientationSpin(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPDFMARKGENERAL_H__68AB1A03_B0D5_11D5_88B7_0040F68C1EF7__INCLUDED_
