// OleAcrobatClientItem.h: Schnittstelle f�r die Klasse COleAcrobatClientItem.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OLEACROBATCLIENTITEM_H__4DFFABA3_A7F4_11D3_877C_0040F68C1EF7__INCLUDED_)
#define AFX_OLEACROBATCLIENTITEM_H__4DFFABA3_A7F4_11D3_877C_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class COleAcrobatClientItem : public COleClientItem  
{
public:
	COleAcrobatClientItem(COleDocument* pContainer);
	virtual ~COleAcrobatClientItem();
};

#endif // !defined(AFX_OLEACROBATCLIENTITEM_H__4DFFABA3_A7F4_11D3_877C_0040F68C1EF7__INCLUDED_)
