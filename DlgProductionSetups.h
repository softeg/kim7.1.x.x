#pragma once
#include "afxwin.h"


// CDlgProductionSetup-Dialogfeld

class CDlgProductionSetups : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionSetups)

public:
	CDlgProductionSetups(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionSetups();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTIONSETUPS };
	CComboBox m_productionSetupsCombo;
	CString m_strName;
	CString m_strComment;
	CString m_strProductionProfile;
	float m_fMaxOverProduction;
	BOOL  m_bOneTypePerSheet;
	int m_nRange1Min;
	int m_nRange2Min;
	int m_nRange3Min;
	int m_nRange4Min;
	int m_nRange1Max;
	int m_nRange2Max;
	int m_nRange3Max;
	int m_nRange4Max;

public:
	CProductionSetup m_productionSetup;
	CProductionSetup m_oldProductionSetup;

public:
	void InitProductionSetupsCombo();
	void InitData();
	void SaveData();
	void CheckModified();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSelchangeProductionSetupList();
	afx_msg void OnBnClickedSave();
	afx_msg void OnBnClickedRemove();
	afx_msg void OnBnClickedPsProductionProfileButton();
	afx_msg void OnBnClickedOk();
};
