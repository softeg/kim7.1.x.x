// GraphicPath.cpp: Implementierung der Klasse CGraphicPath.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "GraphicPath.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CGraphicPathElement, CObject, VERSIONABLE_SCHEMA | 1)

BOOL g_bIsContentView = FALSE;


int WorldToLP(float fValue)
{
	if (g_bIsContentView)
		return (int)fValue;
	else
		return CPrintSheetView::WorldToLP(fValue);
}


CGraphicPathElement::CGraphicPathElement()
{
	m_crColor = 0;
}

CGraphicPathElement::CGraphicPathElement(int nType, CObject* pgpElement)
{
	m_crColor = 0;
}

CGraphicPathElement::CGraphicPathElement(const CGraphicPathElement& gpElement) // copy constructor
{
	*this = gpElement;
}

void CGraphicPathElement::Copy(const CGraphicPathElement& gpElement) 
{
	m_crColor = gpElement.m_crColor;
}

const CGraphicPathElement& CGraphicPathElement::operator=(const CGraphicPathElement& gpElement)
{
	Copy(gpElement);

	return *this;
}

void CGraphicPathElement::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		ar << m_crColor;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();
		switch (nVersion)
		{
		case 1: ar >> m_crColor;
				break;
		}
	}
}

void CGraphicPathElement::RotatePoint(double& fX, double& fY, int nAngle)
{
	double fTempX, fTempY;
	
	
	switch (nAngle)
	{
	case  90: fTempX = -fY;	fTempY =  fX; break;
	case 180: fTempX = -fX;	fTempY = -fY; break;
	case 270: fTempX =  fY;	fTempY = -fX; break;
	default : return;
	}
	
	fX = fTempX; fY = fTempY;
}

void CGraphicPathElement::MirrorPoint(double& fX, double& fY, int nMirrorAxis)
{
	switch (nMirrorAxis)
	{
	case HORIZONTAL: fY = -fY;	break;
	case VERTICAL:	 fX = -fX;	break;
	default : return;
	}
}


// CGraphicPathArc ////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CGraphicPathArc, CGraphicPathElement, VERSIONABLE_SCHEMA | 1)

CGraphicPathArc::CGraphicPathArc() 
{
}

void CGraphicPathArc::Copy(const CGraphicPathElement& gpElement)
{
	CGraphicPathElement::Copy(gpElement);

	CGraphicPathArc& gpArc = (CGraphicPathArc&)gpElement;
	m_ptCenter.m_fX = gpArc.m_ptCenter.m_fX;
	m_ptCenter.m_fY = gpArc.m_ptCenter.m_fY;
	m_fRadius		= gpArc.m_fRadius;
	m_fStartAngle	= gpArc.m_fStartAngle;
	m_fEndAngle		= gpArc.m_fEndAngle;
}

const CGraphicPathArc& CGraphicPathArc::operator=(const CGraphicPathArc& gpArc)
{
	Copy(gpArc);

	return *this;
}


void CGraphicPathArc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		ar << m_ptCenter.m_fX;
		ar << m_ptCenter.m_fY;
		ar << m_fRadius;
		ar << m_fStartAngle;
		ar << m_fEndAngle;
	}
	else
	{
		ar >> m_ptCenter.m_fX;
		ar >> m_ptCenter.m_fY;
		ar >> m_fRadius;
		ar >> m_fStartAngle;
		ar >> m_fEndAngle;
	}

   ar.SerializeClass(RUNTIME_CLASS(CGraphicPathElement));
   CGraphicPathElement::Serialize(ar);
}


CGraphicPathArc::CGraphicPathArc(double fXStart, double fYStart, double fXEnd, double fYEnd, double fIndent)
{
	GP_VECTOR pt1, pt2, pt3, ptc;
	GP_LINE	  ms1;
	double	  fR, fH, fS, fDeltaX, fDeltaY, fAlpha, fArg;

	pt1[0] = fXStart; pt1[1] = fYStart;
	pt2[0] = fXEnd;	  pt2[1] = fYEnd;
	
	fS = sqrt((pt2[0] - pt1[0]) * (pt2[0] - pt1[0]) + (pt2[1] - pt1[1]) * (pt2[1] - pt1[1]));	// vector length
	fH = (fIndent * fS) / 2.0f;

	MittelSenkrechte(&ms1, pt1, pt2);
	PointOnLine(pt3, &ms1, -fH);  // -fH, because fIndent is defined in neg. direction

	if(CoordIdentical(fXStart, fYStart, fXEnd, fYEnd))
		return;

	if ( ! CreateCircle(ptc, fR, pt1, pt2, pt3))
		return;

	if (fIndent > 0.0f)
		m_fRadius =  fR;
	else
		m_fRadius = -fR;
	
	m_ptCenter.m_fX = ptc[0]; m_ptCenter.m_fY = ptc[1];
	
	fDeltaX = pt1[0] - ptc[0]; fDeltaY = pt1[1] - ptc[1];
	fArg = fDeltaX / fR;
	if (fArg < -1.0f)
		fArg = -1.0f;
	if (fArg > 1.0f)
		fArg = 1.0f;
	fAlpha = acos(fArg);
	if (fDeltaY < 0.0f)
		fAlpha = CGraphicPath::PI_2() - fAlpha;
	
	m_fStartAngle = (fAlpha / CGraphicPath::PI_2()) * 360.0f;
	if (m_fStartAngle < 0.00001f && m_fStartAngle > -0.00001f)
		m_fStartAngle = 0.0f;
	
	fDeltaX = pt2[0] - ptc[0]; fDeltaY = pt2[1] - ptc[1];
	fArg = fDeltaX / fR;
	if (fArg < -1.0f)
		fArg = -1.0f;
	if (fArg > 1.0f)
		fArg = 1.0f;
	fAlpha = acos(fArg);
	if (fDeltaY < 0.0f)
		fAlpha = CGraphicPath::PI_2() - fAlpha;
	
	m_fEndAngle = (fAlpha / CGraphicPath::PI_2()) * 360.0f;
	if (m_fEndAngle < 0.00001f && m_fEndAngle > -0.00001f)
		m_fEndAngle = 0.0f;
}

void CGraphicPathArc::MittelSenkrechte(GP_LINE *ms, GP_VECTOR pt1, GP_VECTOR pt2)
{
  GP_LINE   g;
  GP_VECTOR pn1, pn2, l, ph;

  Line(&g, pt1, pt2);
  PointOnLine(pn1, &g, 0.5f);
  ph[0] = pt2[0] - pt1[0];
  ph[1] = pt2[1] - pt1[1];
  Lot(l, ph);
  pn2[0] = pn1[0] + l[0];
  pn2[1] = pn1[1] + l[1];

  Line(ms, pn1, pn2);
}

void CGraphicPathArc::Line(GP_LINE *g, GP_VECTOR pt1, GP_VECTOR pt2)
{
  g->a[0] = pt1[0];
  g->a[1] = pt1[1];

  g->b[0] = pt2[0] - pt1[0];
  g->b[1] = pt2[1] - pt1[1];
}

void CGraphicPathArc::PointOnLine(GP_VECTOR pt, GP_LINE *g, double fT)
{
  pt[0] = g->a[0] + fT * g->b[0];
  pt[1] = g->a[1] + fT * g->b[1];
}

void CGraphicPathArc::Lot(GP_VECTOR l, GP_VECTOR v)
{
  double fLenght;

  l[0] = -v[1];
  l[1] =  v[0];

  fLenght = sqrt( (l[0] * l[0]) + (l[1] * l[1]) );
  l[0] = l[0] / fLenght;
  l[1] = l[1] / fLenght;
}

BOOL CGraphicPathArc::CoordIdentical(double fX1, double fY1, double fX2, double fY2)
{
	double a, b;
	
	a = fabs((fX1 - fX2));
	b = fabs((fY1 - fY2));
	
	if ( a < 0.001 )
		if ( b < 0.001 )
			return TRUE;
		else
			return FALSE;
	else
		return FALSE;
}

BOOL CGraphicPathArc::CreateCircle(GP_VECTOR ptc, double& fRadius, GP_VECTOR pt1, GP_VECTOR pt2, GP_VECTOR pt3)
{
	GP_LINE ms1, ms2;
	
	MittelSenkrechte(&ms1, pt1, pt2);
	MittelSenkrechte(&ms2, pt1, pt3);
	
	if ( ! SchnittpGerGer(ptc, &ms1, &ms2))
		return FALSE;
	fRadius = sqrt( (ptc[0] - pt1[0]) * (ptc[0] - pt1[0]) + (ptc[1] - pt1[1]) * (ptc[1] - pt1[1]) );
	return TRUE;
}

BOOL CGraphicPathArc::SchnittpGerGer(GP_VECTOR pts, GP_LINE *g1, GP_LINE *g2)
{
  double	d, d2, t1;
  GP_VECTOR s1, s2;


  s1[0] = g2->b[0];
  s1[1] = g2->b[1];
  s2[0] = -(g1->b[0]);
  s2[1] = -(g1->b[1]);

  d = s1[0] * s2[1] - s2[0] * s1[1];	// Determinante
  if (fabs(d) < 0.000001f)   // Geraden sind parallel 
    return FALSE;

  s2[0] = g1->a[0] - g2->a[0];
  s2[1] = g1->a[1] - g2->a[1];

  d2 = s1[0] * s2[1] - s2[0] * s1[1];

  t1 = d2/d;

  pts[0] = g1->a[0] + t1 * g1->b[0];
  pts[1] = g1->a[1] + t1 * g1->b[1];

  return TRUE;
}

void CGraphicPathArc::Draw(CDC* pDC, COLORREF crColor)
{
	double xw, yw, xa, ya, xe, ye, aw, sw;
	int    xs1, ys1, xs2, ys2, xsa, ysa, xse, yse;
	
	if (m_fRadius < 0.0f)		// Clockwise
	{
		aw = m_fEndAngle;		// exchange because CDC::Arc() rotates CCW
		sw = m_fStartAngle;
	}
	else
	{
		aw = m_fStartAngle;
		sw = m_fEndAngle;
	}
	aw = (aw/360.0f) * CGraphicPath::PI_2();
	sw = (sw/360.0f) * CGraphicPath::PI_2();
	
	xa = m_ptCenter.m_fX + cos(aw) * fabs(m_fRadius);
	ya = m_ptCenter.m_fY + sin(aw) * fabs(m_fRadius);
	xe = m_ptCenter.m_fX + cos(sw) * fabs(m_fRadius);
	ye = m_ptCenter.m_fY + sin(sw) * fabs(m_fRadius);
	
	xsa = WorldToLP((float)xa);
	ysa = WorldToLP((float)ya);
	xse = WorldToLP((float)xe);
	yse = WorldToLP((float)ye);
	
	xw  = m_ptCenter.m_fX - m_fRadius;
	yw  = m_ptCenter.m_fY - m_fRadius;
	xs1 = WorldToLP((float)xw);
	ys1 = WorldToLP((float)yw);
	/* linke untere Ecke des umschr. Rechtecks */
	xw  = m_ptCenter.m_fX + m_fRadius;
	yw  = m_ptCenter.m_fY + m_fRadius;
	xs2 = WorldToLP((float)xw);
	ys2 = WorldToLP((float)yw);
	/* rechte obere Ecke des umschr. Rechtecks */

	if (aw == sw) /* Vollkreis */
		pDC->Ellipse(xs1, ys1, xs2, ys2);
	else
		pDC->Arc(xs1, ys1, xs2, ys2, xsa, ysa, xse, yse);
}

/* 4/3 * (1-cos 45�)/sin 45� = 4/3 * sqrt(2) - 1 */
#define ARC_MAGIC   (0.552284749)
#define PDC_DEG2RAD 0.0174532925199433

CString CGraphicPathArc::MakePDFStream()
{
	double fAlpha = m_fStartAngle;
	double fBeta  = m_fEndAngle;
	if (m_fRadius < 0.0f)		// Clockwise
	{
		fAlpha	   = m_fEndAngle;		
		fBeta	   = m_fStartAngle;
		m_fRadius *= -1;
	}

	double rad_a  = fAlpha * PDC_DEG2RAD;
	double startx = (m_ptCenter.m_fX + m_fRadius * cos(rad_a));
	double starty = (m_ptCenter.m_fY + m_fRadius * sin(rad_a));
	
	CString strTemp;
	strTemp.Format(_T("%.3f %.3f m "), startx, starty);		// moveto
	
	while (fBeta < fAlpha)
		fBeta += 360.0f;
	
	if (fAlpha == fBeta)
		return _T("");
	
	while (fBeta - fAlpha > 90.0f)
	{
		strTemp += PDFShortArc(fAlpha, fAlpha + 90);
		fAlpha += 90;
	}
	
	if (fAlpha != fBeta)
		strTemp += PDFShortArc(fAlpha, fBeta);

	return strTemp;
}

CString CGraphicPathArc::PDFShortArc(double fAlpha, double fBeta)
{
    double bcp;
    double cos_alpha, cos_beta, sin_alpha, sin_beta;

    fAlpha = fAlpha * PDC_DEG2RAD;
    fBeta  = fBeta  * PDC_DEG2RAD;

    /* This formula yields ARC_MAGIC for alpha == 0, beta == 90 degrees */
    bcp = (4.0/3 * (1 - cos((fBeta - fAlpha)/2)) / sin((fBeta - fAlpha)/2));

    sin_alpha = sin(fAlpha);
    sin_beta  = sin(fBeta);
    cos_alpha = cos(fAlpha);
    cos_beta  = cos(fBeta);

    return PDFCurveTo(  m_ptCenter.m_fX + m_fRadius * (cos_alpha - bcp * sin_alpha),          /* p1 */
						m_ptCenter.m_fY + m_fRadius * (sin_alpha + bcp * cos_alpha),
						m_ptCenter.m_fX + m_fRadius * (cos_beta + bcp * sin_beta),            /* p2 */
						m_ptCenter.m_fY + m_fRadius * (sin_beta - bcp * cos_beta),
						m_ptCenter.m_fX + m_fRadius * cos_beta,                               /* p3 */
						m_ptCenter.m_fY + m_fRadius * sin_beta);
}

CString CGraphicPathArc::PDFCurveTo(double x_1, double y_1, double x_2, double y_2, double x_3, double y_3)
{
	CString strTemp;
    if (x_2 == x_3 && y_2 == y_3)  /* second c.p. coincides with final point */
        strTemp.Format(_T("%f %f %f %f y "), x_1, y_1, x_3, y_3);

    else                        /* general case with four distinct points */
        strTemp.Format(_T("%f %f %f %f %f %f c "), x_1, y_1, x_2, y_2, x_3, y_3);

	return strTemp;
}

CString CGraphicPathArc::PDFCircle(double x, double y, double r)
{
    if (r < 0)
		return _T("");

    /* draw four Bezier curves to approximate a circle */
	CString strTemp;
	strTemp.Format(_T("%.3f %.3f m "), x, y);		// moveto
    PDFCurveTo(x + r, y + r*ARC_MAGIC, x + r*ARC_MAGIC, y + r, x, y + r);
    PDFCurveTo(x - r*ARC_MAGIC, y + r, x - r, y + r*ARC_MAGIC, x - r, y);
    PDFCurveTo(x - r, y - r*ARC_MAGIC, x - r*ARC_MAGIC, y - r, x, y - r);
    PDFCurveTo(x + r*ARC_MAGIC, y - r, x + r, y - r*ARC_MAGIC, x + r, y);

//    pdf__closepath(p);
	return _T("");
}


// CGraphicPathCircle ////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CGraphicPathCircle, CGraphicPathElement, VERSIONABLE_SCHEMA | 1)

CGraphicPathCircle::CGraphicPathCircle() 
{
}

void CGraphicPathCircle::Copy(const CGraphicPathElement& gpElement) 
{
	CGraphicPathElement::Copy(gpElement);

	CGraphicPathCircle& gpCircle = (CGraphicPathCircle&)gpElement;
	m_ptCenter.m_fX = gpCircle.m_ptCenter.m_fX;
	m_ptCenter.m_fY = gpCircle.m_ptCenter.m_fY;
	m_fRadius		= gpCircle.m_fRadius;
}

const CGraphicPathCircle& CGraphicPathCircle::operator=(const CGraphicPathCircle& gpCircle)
{
	Copy(gpCircle);

	return *this;
}

void CGraphicPathCircle::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		ar << m_ptCenter.m_fX;
		ar << m_ptCenter.m_fY;
		ar << m_fRadius;
	}
	else
	{
		ar >> m_ptCenter.m_fX;
		ar >> m_ptCenter.m_fY;
		ar >> m_fRadius;
	}

   ar.SerializeClass(RUNTIME_CLASS(CGraphicPathElement));
   CGraphicPathElement::Serialize(ar);
}



// CGraphicPathVertex ////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CGraphicPathVertex, CGraphicPathElement, VERSIONABLE_SCHEMA | 1)

CGraphicPathVertex::CGraphicPathVertex() 
{
}

void CGraphicPathVertex::Copy(const CGraphicPathElement& gpElement) 
{
	CGraphicPathElement::Copy(gpElement);

	CGraphicPathVertex& gpVertex = (CGraphicPathVertex&)gpElement;
	m_ptCoord.m_fX = gpVertex.m_ptCoord.m_fX;
	m_ptCoord.m_fY = gpVertex.m_ptCoord.m_fY;
	m_fIndent	   = gpVertex.m_fIndent;
}

const CGraphicPathVertex& CGraphicPathVertex::operator=(const CGraphicPathVertex& gpVertex)
{
	Copy(gpVertex);

	return *this;
}

void CGraphicPathVertex::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		ar << m_ptCoord.m_fX;
		ar << m_ptCoord.m_fY;
		ar << m_fIndent;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();
		ar >> m_ptCoord.m_fX;
		ar >> m_ptCoord.m_fY;
		ar >> m_fIndent;
	}

   ar.SerializeClass(RUNTIME_CLASS(CGraphicPathElement));
   CGraphicPathElement::Serialize(ar);
}




// CGraphicPathSeqend ////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CGraphicPathSeqend, CGraphicPathElement, VERSIONABLE_SCHEMA | 1)

CGraphicPathSeqend::CGraphicPathSeqend() 
{
}

void CGraphicPathSeqend::Copy(const CGraphicPathElement& gpElement) 
{
	CGraphicPathElement::Copy(gpElement);
}

const CGraphicPathSeqend& CGraphicPathSeqend::operator=(const CGraphicPathSeqend& gpSeqend)
{
	Copy(gpSeqend);

	return *this;
}

void CGraphicPathSeqend::Serialize(CArchive& ar)
{
   ar.SerializeClass(RUNTIME_CLASS(CGraphicPathElement));
   CGraphicPathElement::Serialize(ar);
}




// CGraphicPath //////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CGraphicPath, CGraphicItem, VERSIONABLE_SCHEMA | 1)

CGraphicPath::CGraphicPath()
{
	m_fScaleX = m_fScaleY = 1.0f;
}

CGraphicPath::~CGraphicPath()
{
	for (int i = 0; i < m_elements.GetSize(); i++)
	{
		delete m_elements[i];
	}
	m_elements.RemoveAll();
}

void CGraphicPath::Copy(CGraphicItem* pgItem)
{
	CGraphicPath* pgpPath = (CGraphicPath*)pgItem;
	for (int i = 0; i < pgpPath->m_elements.GetSize(); i++)
	{
		CGraphicPathElement* pgpElem;
		if (pgpPath->m_elements[i]->IsKindOf(RUNTIME_CLASS(CGraphicPathArc)))
			pgpElem = new CGraphicPathArc;
		else
		if (pgpPath->m_elements[i]->IsKindOf(RUNTIME_CLASS(CGraphicPathCircle)))
			pgpElem = new CGraphicPathCircle;
		else
		if (pgpPath->m_elements[i]->IsKindOf(RUNTIME_CLASS(CGraphicPathVertex)))
			pgpElem = new CGraphicPathVertex;
		else
		if (pgpPath->m_elements[i]->IsKindOf(RUNTIME_CLASS(CGraphicPathSeqend)))
			pgpElem = new CGraphicPathSeqend;
		else
			pgpElem = new CGraphicPathElement;

		pgpElem->Copy(*pgpPath->m_elements[i]);
		m_elements.Add(pgpElem);
	}
}

void CGraphicPath::Serialize(CArchive& ar)
{ 
	if (ar.IsStoring())
	{
		ar << m_fScaleX;
		ar << m_fScaleY;
	}
	else
	{
		ar >> m_fScaleX;
		ar >> m_fScaleY;
	}
	m_elements.Serialize(ar); 

   ar.SerializeClass(RUNTIME_CLASS(CGraphicItem));
   CGraphicItem::Serialize(ar);
}

template <> void AFXAPI SerializeElements(CArchive& ar, CGraphicPathElement** pGraphicPathElement, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CGraphicPathElement));

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << *pGraphicPathElement;
		}
		else
		{
			ar >> *pGraphicPathElement;
		}

		pGraphicPathElement++;
	}
}

void CGraphicPath::AppendCircle(double fCenterX, double fCenterY, double fRadius, int cColor)
{
	CGraphicPathCircle* pgpCircle = new (CGraphicPathCircle);
	pgpCircle->m_ptCenter.m_fX	  = fCenterX;
	pgpCircle->m_ptCenter.m_fY	  = fCenterY; 
	pgpCircle->m_fRadius		  = fRadius;

	m_elements.Add(pgpCircle);
}

void CGraphicPath::AppendVertex(double fStartX, double fStartY, int cColor)
{
	CGraphicPathVertex* pgpVertex = new (CGraphicPathVertex);
	pgpVertex->m_ptCoord.m_fX	  = fStartX;
	pgpVertex->m_ptCoord.m_fY	  = fStartY;
	pgpVertex->m_fIndent		  = 0.0f;

	m_elements.Add(pgpVertex);
}

void CGraphicPath::AppendSeqend()
{
	CGraphicPathSeqend* pgpSeqend = new (CGraphicPathSeqend);

	m_elements.Add(pgpSeqend);
}

BOOL CGraphicPath::PosIdentical(double fX1,double fY1,double fX2,double fY2)
{
  if ( (fabs(fX1 - fX2) < 0.01f) && (fabs(fY1 - fY2) < 0.01f) )
    return TRUE;
  else
    return FALSE;
}

void CGraphicPath::MoveToOrigin(double fX0, double fY0)
{
	for (int i = 0; i < m_elements.GetSize(); i++)
	{
		if (m_elements[i]->IsVertex())
		{
			CGraphicPathVertex* pVertex = (CGraphicPathVertex*)m_elements[i];
			pVertex->m_ptCoord.m_fX -= fX0;
			pVertex->m_ptCoord.m_fY -= fY0;
		}

		if (m_elements[i]->IsCircle())
		{
			CGraphicPathCircle* pCircle = (CGraphicPathCircle*)m_elements[i];
			pCircle->m_ptCenter.m_fX -= fX0;
			pCircle->m_ptCenter.m_fY -= fY0;
		}
	}
}

void CGraphicPath::Draw(CDC* pDC, double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis)
{
	int    nAngle	= 0;
	double fOffsetX = fXPos;
	double fOffsetY = fYPos;
	switch (nOrientation)
	{
	case LEFT  : nAngle =  90; fOffsetX += fWidth;	fOffsetY += 0.0;		break;
	case RIGHT : nAngle = 270; fOffsetX += 0.0;		fOffsetY += fHeight;	break;
	case BOTTOM: nAngle = 180; fOffsetX += fWidth;	fOffsetY += fHeight;	break;
	case TOP   : nAngle =   0; fOffsetX += 0.0;		fOffsetY += 0.0;		break;
	}
	switch (nMirrorAxis)
	{
	case HORIZONTAL  : fOffsetY += ((nAngle == 0) || (nAngle == 90))  ? fHeight : -fHeight;	break;
	case VERTICAL:	   fOffsetX += ((nAngle == 0) || (nAngle == 270)) ? fWidth  : -fWidth;	break;
	}

	double fXStart, fYStart, fXEnd, fYEnd, fIndent;
	for (int i = 0; i < m_elements.GetSize(); i++)
	{
		if (m_elements[i]->IsVertex())
		{
			fXStart = m_elements[i]->GetVertexCoord().m_fX; fYStart = m_elements[i]->GetVertexCoord().m_fY; fIndent = m_elements[i]->GetVertexIndent();
				
			fXStart *= m_fScaleX; fYStart *= m_fScaleY;
			CGraphicPathElement::RotatePoint(fXStart, fYStart, nAngle);
			CGraphicPathElement::MirrorPoint(fXStart, fYStart, nMirrorAxis);
			fXStart+= fOffsetX; fYStart+= fOffsetY;

			CheckNegativeScale(fXStart, fYStart, fWidth, fHeight, nAngle);
											
			i++;
			for (; i < m_elements.GetSize(); i++)
			{
				if ( ! m_elements[i]->IsVertex())
					break;

				fXEnd = m_elements[i]->GetVertexCoord().m_fX; fYEnd = m_elements[i]->GetVertexCoord().m_fY;
				
				fXEnd *= m_fScaleX; fYEnd *= m_fScaleY;
				CGraphicPathElement::RotatePoint(fXEnd, fYEnd, nAngle);
				CGraphicPathElement::MirrorPoint(fXEnd, fYEnd, nMirrorAxis);
				fXEnd+= fOffsetX; fYEnd+= fOffsetY;
	
				CheckNegativeScale(fXEnd, fYEnd, fWidth, fHeight, nAngle);
				
				if (fIndent != 0.0)
				{
					if ( !((m_fScaleX < 0.0 && m_fScaleY < 0.0) || (m_fScaleX > 0.0 && m_fScaleY > 0.0)))
						fIndent*= -1;
					if ( (nMirrorAxis == HORIZONTAL) || (nMirrorAxis == VERTICAL) )
						fIndent*= -1;

					CGraphicPathArc gpArc(fXStart, fYStart, fXEnd, fYEnd, fIndent);
					gpArc.Draw(pDC, 0);
				}
				else
				{
					pDC->MoveTo(WorldToLP((float)fXStart), WorldToLP((float)fYStart));
					pDC->LineTo(WorldToLP((float)fXEnd),   WorldToLP((float)fYEnd));
				}
				
				fXStart = fXEnd; fYStart = fYEnd;
				fIndent = m_elements[i]->GetVertexIndent();
			}
		} // if (m_elements[i]->IsVertex())
		
		if (i < m_elements.GetSize())
			if (m_elements[i]->IsCircle())
			{
				CGraphicPathArc gpArc;
				gpArc.m_fStartAngle = 0.0f; 
				gpArc.m_fEndAngle	= 0.0f;
				gpArc.m_fRadius		= m_elements[i]->GetCircleRadius();
				gpArc.m_ptCenter	= m_elements[i]->GetCircleCenter();
				CGraphicPathElement::RotatePoint(gpArc.m_ptCenter.m_fX, gpArc.m_ptCenter.m_fY, nAngle);
				CGraphicPathElement::MirrorPoint(gpArc.m_ptCenter.m_fX, gpArc.m_ptCenter.m_fY, nMirrorAxis);
				gpArc.m_ptCenter.m_fX += fOffsetX; gpArc.m_ptCenter.m_fY += fOffsetY;
				gpArc.Draw(pDC, 0);
				i++;
			}
	}
}

void CGraphicPath::CheckNegativeScale(double& fX, double& fY, double fWidth, double fHeight, int nAngle)
{
	if (m_fScaleX < 0.0)
	{
		switch (nAngle)
		{
		case 0:		fX = fX + fWidth;	break;
		case 90:	fY = fY + fHeight;	break;
		case 180:	fX = fX - fWidth;	break;
		case 270:	fY = fY - fHeight;	break;
		default:	fX = fX + fWidth;	break;
		}
	}
				
	if (m_fScaleY < 0.0)
	{
		switch (nAngle)
		{
		case 0:		fY = fY + fHeight;	break;
		case 90:	fX = fX + fWidth;	break;
		case 180:	fY = fY - fHeight;	break;
		case 270:	fX = fX - fWidth;	break;
		default:	fY = fY + fHeight;	break;
		}
	}
}

CString	CGraphicPath::MakePDFStream(double fOutputMediaX, double fOutputMediaY, double fOutputMediaWidth, double fOutputMediaHeight, int nOutputMediaOrientation,
									double fXPos, double fYPos, double fWidth, double fHeight, int nOrientation, int nMirrorAxis)
{
	CString strPDFStream;
	int		nAngle	= 0;
	double	fOffsetX = fXPos;
	double	fOffsetY = fYPos;

	switch (nOrientation)
	{
	case LEFT  : nAngle =  90; fOffsetX += fWidth;	fOffsetY += 0.0;		break;
	case RIGHT : nAngle = 270; fOffsetX += 0.0;		fOffsetY += fHeight;	break;
	case BOTTOM: nAngle = 180; fOffsetX += fWidth;	fOffsetY += fHeight;	break;
	case TOP   : nAngle =   0; fOffsetX += 0.0;		fOffsetY += 0.0;		break;
	}
	switch (nMirrorAxis)
	{
	case HORIZONTAL  : fOffsetY += ((nAngle == 0) || (nAngle == 90))  ? fHeight : -fHeight;	break;
	case VERTICAL:	   fOffsetX += ((nAngle == 0) || (nAngle == 270)) ? fWidth  : -fWidth;	break;
	}

	double fXStart, fYStart, fXEnd, fYEnd, fIndent;
	for (int i = 0; i < m_elements.GetSize(); i++)
	{
		if (m_elements[i]->IsVertex())
		{
			fXStart = m_elements[i]->GetVertexCoord().m_fX; fYStart = m_elements[i]->GetVertexCoord().m_fY; fIndent = m_elements[i]->GetVertexIndent();
				
			fXStart *= m_fScaleX; fYStart *= m_fScaleY;
			CGraphicPathElement::RotatePoint(fXStart, fYStart, nAngle);
			CGraphicPathElement::MirrorPoint(fXStart, fYStart, nMirrorAxis);
			fXStart+= fOffsetX; fYStart+= fOffsetY;

			CheckNegativeScale(fXStart, fYStart, fWidth, fHeight, nAngle);
			TransformToOutputMedia(fOutputMediaX, fOutputMediaY, fOutputMediaWidth, fOutputMediaHeight, nOutputMediaOrientation, fXStart, fYStart);
											
			i++;
			for (; i < m_elements.GetSize(); i++)
			{
				if ( ! m_elements[i]->IsVertex())
					break;

				fXEnd = m_elements[i]->GetVertexCoord().m_fX; fYEnd = m_elements[i]->GetVertexCoord().m_fY;
				
				fXEnd *= m_fScaleX; fYEnd *= m_fScaleY;
				CGraphicPathElement::RotatePoint(fXEnd, fYEnd, nAngle);
				CGraphicPathElement::MirrorPoint(fXEnd, fYEnd, nMirrorAxis);
				fXEnd+= fOffsetX; fYEnd+= fOffsetY;
	
				CheckNegativeScale(fXEnd, fYEnd, fWidth, fHeight, nAngle);
				TransformToOutputMedia(fOutputMediaX, fOutputMediaY, fOutputMediaWidth, fOutputMediaHeight, nOutputMediaOrientation, fXEnd,   fYEnd);
				
				CString strTemp;
				if (fIndent != 0.0)
				{
					if ( !((m_fScaleX < 0.0 && m_fScaleY < 0.0) || (m_fScaleX > 0.0 && m_fScaleY > 0.0)))
						fIndent*= -1;
					if ( (nMirrorAxis == HORIZONTAL) || (nMirrorAxis == VERTICAL) )
						fIndent*= -1;

					CGraphicPathArc gpArc(fXStart/fPSPoint, fYStart/fPSPoint, fXEnd/fPSPoint, fYEnd/fPSPoint, fIndent);
					strTemp = gpArc.MakePDFStream();
				}
				else
					strTemp.Format(_T("%.3f %.3f m %.3f %.3f l "), fXStart/fPSPoint, fYStart/fPSPoint, fXEnd/fPSPoint, fYEnd/fPSPoint); 

				strPDFStream += strTemp;
				
				fXStart = fXEnd; fYStart = fYEnd;
				fIndent = m_elements[i]->GetVertexIndent();
			}
		} // if (m_elements[i]->IsVertex())
		
		if (i < m_elements.GetSize())
			if (m_elements[i]->IsCircle())
			{
				CGraphicPathArc gpArc;
				gpArc.m_fStartAngle = 0.0f; 
				gpArc.m_fEndAngle	= 0.0f;
				gpArc.m_fRadius		= m_elements[i]->GetCircleRadius();
				gpArc.m_ptCenter	= m_elements[i]->GetCircleCenter();
				CGraphicPathElement::RotatePoint(gpArc.m_ptCenter.m_fX, gpArc.m_ptCenter.m_fY, nAngle);
				CGraphicPathElement::MirrorPoint(gpArc.m_ptCenter.m_fX, gpArc.m_ptCenter.m_fY, nMirrorAxis);
				gpArc.m_ptCenter.m_fX += fOffsetX; gpArc.m_ptCenter.m_fY += fOffsetY;
				TransformToOutputMedia(fOutputMediaX, fOutputMediaY, fOutputMediaWidth, fOutputMediaHeight, nOutputMediaOrientation, gpArc.m_ptCenter.m_fX, gpArc.m_ptCenter.m_fY);
				gpArc.m_ptCenter.m_fX /= fPSPoint;
				gpArc.m_ptCenter.m_fY /= fPSPoint;

				gpArc.MakePDFStream();
				i++;
			}
	}

	strPDFStream += _T("S\n");	// stroke
	return strPDFStream;
}

void CGraphicPath::TransformToOutputMedia(double fCorrectionX, double fCorrectionY, double fMediaWidth, double fMediaHeight, int nRotation, double& fX, double& fY)
{
	double fXTrans, fYTrans;
	switch (nRotation)
	{
	case 0:		
		fXTrans	= fX;
		fYTrans = fY;
		break;
	case 90:	
		fXTrans	= fY;
		fYTrans = fMediaWidth - fX;
		break;
	case 180:	
		fXTrans	= fMediaWidth  - fX;
		fYTrans = fMediaHeight - fY;
		break;
	case 270:	
		fXTrans = fMediaHeight - fY;
		fYTrans = fX;
		break;
	default:		
		fXTrans	= fX;
		fYTrans = fY;
		break;
	}
	
	fX = fXTrans + fCorrectionX;
	fY = fYTrans + fCorrectionY;
}
