// JDFOutputImposition.h: Schnittstelle f�r die Klasse CJDFOutputImposition.
//
//////////////////////////////////////////////////////////////////////

// fudge for declspec dll...
//#define PROJ_JDFTOOLS
#define PROJ_JDFTOOLSLIB
#include <jdf/util/PlatformUtils.h>
#include "JDF.h"
#include "JDFSourceResource.h"
#include <jdf/io/File.h>
#include <jdf/io/FileInputStream.h>
#include <jdf/io/FileOutputStream.h>
#include <jdf/io/BufferedOutputStream.h>
#include "jdf\mime\MIMEBodyPart.h"
#include <jdf/mime/MIMEMessage.h>
#include <jdf/mime/MIMEMessagePart.h>
#include <jdf/mime/MIMEBasicPart.h>
#include <jdf/mime/MIMEMultiPart.h>
#include <jdf/mime/FileMIMEType.h>
#include <jdf/mime/FileNameMap.h>
#include <jdf/net/URLConnection.h>
#include <iostream>

#include <xercesc/parsers/XercesDOMParser.hpp>

#include <vector>



using namespace std;
using namespace JDF;


#if !defined(AFX_JDFOUTPUTIMPOSITION_H__B0E80AE8_34A3_4A4A_ABC5_1F116854A988__INCLUDED_)
#define AFX_JDFOUTPUTIMPOSITION_H__B0E80AE8_34A3_4A4A_ABC5_1F116854A988__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CJDFOutputImposition  
{
public:
	CJDFOutputImposition(CPDFTargetProperties& rPDFTarget);
	virtual ~CJDFOutputImposition();

//Attributes:
public:
	JDFNode						m_impositionNode;
	JDFColorantControl			m_colorantControl;
	JDFNode						m_cuttingGroup;
	JDFComponent				m_componentSheet;
	JDFCuttingParams			m_cuttingParams;
	JDFDoc*						m_pJDFDoc;
	JDFBindingIntent			m_bindingIntent;
	JDFLayout					m_JDFLayout;
	JDFSignature*				m_pJDFSignature;
	JDFSheet*					m_pJDFSheet;
	JDFSurface*					m_pJDFSurface;
	JDFCuttingParams*			m_pJDFCuttingParamsRoot;
	int							m_nError;
	BOOL						m_bCancelJob;
	float						m_fMediaWidth;
	float						m_fMediaHeight;
	CString						m_strTargetName;
	CString						m_strLocation;
	CString						m_strOutputJDFPath;
	CString						m_strOutputMimePath;
	CString						m_strOutputFilename;
	CString						m_strTmpOutputJDFPath;
	CString						m_strFilenameTemplate;
	BOOL						m_bJDFOutputMime;
	int							m_nTargetType;
	CString						m_strSysGenMarksStream;
	int							m_nCurrentPage;
	CMediaSize					m_media;
	int							m_nPlatesProcessed;
	int							m_nMarkOrd;
	int							m_nCompatibility;
	BOOL						m_bMarksInFolder;
	CString						m_strMarksFolder;
	BOOL						m_bOutputFormatExceeded;
	CString						m_strJDFJobIDMap;
	CString						m_strJDFJobPartIDMap;
	CString						m_strJDFJobDescriptiveNameMap;
	CArray <CString, CString&>	m_apogeeParameterSets;			


//Operations:
public:
	BOOL				Open(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, int nSide, CPlate* pPlate = NULL);
	BOOL				Open(CBookblock*  pBookblock,  BOOL& bOverwriteAll, int nSide);
	void				Close();
	//static IPL_bool_t	ProgressClose( long lowrange, long highrange, long counter, long lparam);
	void				JDFOUT_AppendApogeeParameterSets(JDFNode root);
	void				JDFOUT_AppendBindingIntent();
	void				JDFOUT_AppendLayout();
	void				JDFOUT_AppendSignature(CPrintSheet& rPrintSheet);
	void				JDFOUT_AppendSignatureBookblock(int nNumber);
	void				JDFOUT_AppendSurface(CPrintSheet& rPrintSheet, int nSide, IPL_TDRECT& mediaBox);
	void				JDFOUT_AppendSurfaceBookblock(int nSide, IPL_TDRECT& mediaBox);
	void				JDFOUT_AppendContentObject(int nOrd, IPL_TDMATRIX& ctm, IPL_TDMATRIX& trimctm, IPL_TDRECT& sheetClipBox, IPL_TDRECT& objectTrimBox);
	void				JDFOUT_AppendMarkObject(IPL_TDRECT& mediaBox);
	void				JDFOUT_AppendPagesRunList();
	POSITION			GetPageListSequence(POSITION pos, CPageSource* pRefPageSource, int& nLastPage);
	void				JDFOUT_AppendMarksRunList();
	void				JDFOUT_AppendOutputRunList();
	void				JDFOUT_AppendCuttingParams(CPrintSheet* pPrintSheet = NULL);
	BOOL				ProcessPrintSheet		  (CPrintSheet* pPrintSheet, BOOL& bOverwriteAll);
	BOOL				ProcessPrintSheetsJobInOne(CPrintSheetList* pPrintSheetList, BOOL& bOverwriteAll, BOOL bAutoCmd, CPrintSheet** pPrintSheet, int* pnSheetsOut);
	CString				GetMarksfileLocation();
	BOOL				CheckTargetProps(CPrintSheet& rPrintSheet);
	BOOL				ProcessPrintSheetAllInOne (CPrintSheet* pPrintSheet, BOOL& bOverwriteAll);
	BOOL				ProcessPrintSheetSidewise (CPrintSheet* pPrintSheet, BOOL& bOverwriteAll);
	BOOL				ProcessPrintSheetPlatewise(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll);
	BOOL				ProcessBookblockJobInOne(CBookblock* pBookblock, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL (*pFnProgressCallback)(int nTotalPages, int nCurrentPage, const CString& strFoldSheetNumber, LPVOID lpData));
	BOOL				ProcessBookblockDoublePage(int nSide, POSITION pos, POSITION pos2);
	BOOL				ProcessBookblockSinglePage(int nSide, POSITION pos);
	void				CheckOutputControlFrames  (CPrintSheet* pPrintSheet, int nSide);
	BOOL				ProcessPlate		(CPlate* pPlate);
	BOOL				ProcessTile			(CPlate* pPlate, BOOL bTiling = FALSE, float fXPos = 0.0f, float fYPos = 0.0f, int nRotation = 0, float fCorrectionX = 0.0f, float fCorrectionY = 0.0f, int nTile = 0);
	BOOL				ProcessTileBookblock(CPlate* pPlate, int nSide, BOOL bTiling = FALSE, float fXPos = 0.0f, float fYPos = 0.0f, int nRotation = 0, float fCorrectionX = 0.0f, float fCorrectionY = 0.0f, int nTile = 0, CLayoutObject* pFilterObject1 = NULL, CLayoutObject* pFilterObject2 = NULL);
	BOOL				ProcessControlFrames(CPrintSheet* pPrintSheet, CPlate* pPlate, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile);
	BOOL				ProcessExposures	(CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile, BYTE nObjectType, int nMarkType = -1, int nZOrder = CFoldMark::Background, CLayoutObject* pFilterObject1 = NULL, CLayoutObject* pFilterObject2 = NULL);
	void				TransformToOutputMedia(int nRotation, float fCorrectionX, float fCorrectionY, float m_fMediaWidth, float m_fMediaHeight, 
											   float& fObjectTrimBoxLeft, float& fObjectTrimBoxBottom, float& fObjectTrimBoxRight, float& fObjectTrimBoxTop,
											   float& fSheetClipBoxLeft,  float& fSheetClipBoxBottom,  float& fSheetClipBoxRight,  float& fSheetClipBoxTop, 
											   float& fSheetExposureCenterX, float& fSheetExposureCenterY, int& nHeadPosition);
	void				TransformToOutputMedia(int nRotation, float fCorrectionX, float fCorrectionY, float fMediaWidth, float fMediaHeight, 
											   float& fSheetClipBoxLeft, float& fSheetClipBoxBottom, float& fSheetClipBoxRight, float& fSheetClipBoxTop);
	BOOL				PDFSheet_AddPage(float fSheetExposureCenterX,	float fSheetExposureCenterY, int nHeadPosition,
										 float fObjectTrimBoxLeft,		float fObjectTrimBoxBottom, 
										 float fObjectTrimBoxRight,		float fObjectTrimBoxTop,
										 float fSheetClipBoxLeft,		float fSheetClipBoxBottom, 
										 float fSheetClipBoxRight,		float fSheetClipBoxTop,
										 float fObjectBBoxCenterX,		float fObjectBBoxCenterY,
										 float fObjectTrimBoxWidth,		float fObjectTrimBoxHeight,
										 float fContentRotation,		float fContentScaleX,	float fContentScaleY,
									     float fBottlingAngle,			float fBottlingOffsetX, float fBottlingOffsetY,
										 BOOL bSkipIfDemo, int nOrd);
	void				CreateMimeFile();
};

#endif // !defined(AFX_JDFOUTPUTIMPOSITION_H__B0E80AE8_34A3_4A4A_ABC5_1F116854A988__INCLUDED_)
