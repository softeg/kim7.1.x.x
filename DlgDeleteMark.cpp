// DlgDeleteMark.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgDeleteMark.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgDeleteMark 


CDlgDeleteMark::CDlgDeleteMark(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgDeleteMark::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgDeleteMark)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT 
}


void CDlgDeleteMark::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgDeleteMark)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgDeleteMark, CDialog)
	//{{AFX_MSG_MAP(CDlgDeleteMark)
	ON_BN_CLICKED(IDC_DELETE_FRONTMARK_BUTTON, OnDeleteFrontmarkButton)
	ON_BN_CLICKED(IDC_DELETE_BOTHMARK_BUTTON, OnDeleteBothmarkButton)
	ON_BN_CLICKED(IDC_DELETE_BACKMARK_BUTTON, OnDeleteBackmarkButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgDeleteMark 

BOOL CDlgDeleteMark::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Zus�tzliche Initialisierung hier einf�gen
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgDeleteMark::OnDeleteFrontmarkButton() 
{
	EndDialog(IDC_DELETE_FRONTMARK_BUTTON);	
}

void CDlgDeleteMark::OnDeleteBackmarkButton() 
{
	EndDialog(IDC_DELETE_BACKMARK_BUTTON);	
}

void CDlgDeleteMark::OnDeleteBothmarkButton() 
{
	EndDialog(IDC_DELETE_BOTHMARK_BUTTON);	
}
