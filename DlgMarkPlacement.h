#if !defined(AFX_DLGMARKPLACEMENT_H__146E9AD1_B3DE_11D5_88BB_0040F68C1EF7__INCLUDED_)
#define AFX_DLGMARKPLACEMENT_H__146E9AD1_B3DE_11D5_88BB_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgMarkPlacement.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMarkPlacement 

class CDlgMarkPlacement : public CDialog
{
// Konstruktion
public:
	CDlgMarkPlacement(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgMarkPlacement();

// Dialogfelddaten
	//{{AFX_DATA(CDlgMarkPlacement)
	enum { IDD = IDD_MARK_PLACEMENT };
	int		m_nPlaceOn;
	int		m_nPlaceOnBoth;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl*  m_pParent;
	CBitmap							m_bmMirrorPos;
	CBitmap							m_bmSamePos;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgMarkPlacement)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void LoadData();
	void SaveData();
	void Apply();
	int  GetLayoutObjectPlacement(CLayoutObject* pLayoutObject);
	void SetLayoutObjectPlacement(CLayoutObject* pLayoutObject, int nPlaceOn);


protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgMarkPlacement)
	virtual BOOL OnInitDialog();
	afx_msg void OnPlaceonFront();
	afx_msg void OnPlaceonBack();
	afx_msg void OnPlaceonBoth();
	afx_msg void OnPlaceonMirror();
	afx_msg void OnPlaceonSamepos();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGMARKPLACEMENT_H__146E9AD1_B3DE_11D5_88BB_0040F68C1EF7__INCLUDED_
