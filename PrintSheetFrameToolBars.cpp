// PrintSheetFrameToolBars.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "PrintSheetTreeView.h"
#include "PrintSheetMarksView.h"
#include "PageListView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// MainToolBar -> IDR_PRINTSHEETFRAME_TOOLBAR

void CPrintSheetFrame::CreateMainToolBar()
{
	if (!m_wndPrintSheetFrameToolBar.Create(this,  WS_CHILD | WS_VISIBLE | CBRS_TOP, IDR_PRINTSHEETFRAME_TOOLBAR) ||
		!m_wndPrintSheetFrameToolBar.LoadToolBar(IDR_PRINTSHEETFRAME_TOOLBAR))
		TRACE0("Failed to create toolbar\n");
	m_wndPrintSheetFrameToolBar.SetBarStyle(m_wndPrintSheetFrameToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_ALIGN_TOP | CBRS_SIZE_DYNAMIC | CBRS_GRIPPER);
	m_wndPrintSheetFrameToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndPrintSheetFrameToolBar,AFX_IDW_DOCKBAR_TOP);
	m_wndPrintSheetFrameToolBar.ModifyStyle(0, TBSTYLE_FLAT);
	ShowControlBar(&m_wndPrintSheetFrameToolBar, TRUE, FALSE); 
}

void CPrintSheetFrame::OnViewPrintsheetframeBar() 
{
	if (m_bMainToolBarActive)
	{
		ShowControlBar(&m_wndPrintSheetFrameToolBar, FALSE, FALSE); 
		ShowControlBar(&m_wndFlatWorkToolBar, FALSE, FALSE); 
		m_bMainToolBarActive = FALSE;
	}
	else
	{
		ShowControlBar(&m_wndPrintSheetFrameToolBar, TRUE, FALSE);
		m_bMainToolBarActive = TRUE;
	}
}

void CPrintSheetFrame::OnUpdateViewPrintsheetframeBar(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bMainToolBarActive);
}

/////////////////////////////////////////////////////////////////////////////
// MainToolBar-buttons also "Ansicht" - Menu

void CPrintSheetFrame::OnFoldsheetcheck() 
{
	m_bShowFoldSheet = ! m_bShowFoldSheet;

	if (m_bShowFoldSheet)
	{
		m_bFoldSheetRotateActive = FALSE;
		m_bFoldSheetTurnActive   = FALSE;
		m_bFoldSheetTumbleActive = FALSE;
		ShowControlBar(&m_wndFoldSheetToolBar, TRUE, FALSE); 
	}
	else
	{
		m_bFoldSheetRotateActive = FALSE;
		m_bFoldSheetTurnActive   = FALSE;
		m_bFoldSheetTumbleActive = FALSE;
		ShowControlBar(&m_wndFoldSheetToolBar, FALSE, FALSE); 
	}

	CheckTrimToolCtrls();
	OnUpdateSectionMenu();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CPrintSheetFrame::OnUpdateFoldsheetcheck(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnableFoldSheetButton);
	pCmdUI->SetCheck(m_bShowFoldSheet);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnMeasurecheck() 
{
	m_bShowMeasure = ! m_bShowMeasure;

	// Don't update PrintSheetTreeView there would be changed the current tree-selection
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CPrintSheetFrame::OnUpdateMeasurecheck(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnableMeasureButton);
	pCmdUI->SetCheck(m_bShowMeasure);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnPlatecheck() 
{
	m_bShowPlate = ! m_bShowPlate;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	int nSide = pDoc->GetActSight();

	if ( ! m_bShowPlate)
	{
		if (m_bShowBitmap)
			m_bShowBitmap = FALSE;

		if (m_bShowExposures) 
			m_bShowExposures = FALSE;
	}
	else
	{
		if (nSide == FRONTSIDE || nSide == BACKSIDE)
			if (pDoc->GetActPressDevice(nSide))
				if ((pDoc->GetActPressDevice(nSide))->m_nPressType == CPressDevice::WebPress)
					m_bAlignSectionMenuActive = TRUE;
	}

	CheckTrimToolCtrls();
	OnUpdateSectionMenu();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();

	if (nSide == ALLSIDES)// || ( ! m_bZoomModeActive))
		// Don't update PrintSheetTreeView there would be changed the current tree-selection
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	else
		// Use UpdateWindow() to keep sheet be zoomed (if zoomed by mouse)
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	//	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CPrintSheetFrame::OnUpdatePlatecheck(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnablePlateButton);
	pCmdUI->SetCheck(m_bShowPlate);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnExposuresCheck() 
{
	m_bShowExposures		  = ! m_bShowExposures;
	m_bEnablePlateTableButton = m_bShowExposures;

	if (m_bShowExposures)
	{
		if ( ! m_bShowPlate)
			m_bShowPlate = TRUE;

		if (m_bShowBitmap)
			m_bShowBitmap = FALSE;
	}

	CheckTrimToolCtrls();
	OnUpdateSectionMenu();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc->GetActSight() == ALLSIDES)// || ( ! m_bZoomModeActive))
		// Don't update PrintSheetTreeView there would be changed the current tree-selection
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	else
		// Use UpdateWindow() to keep sheet be zoomed (if zoomed by mouse)
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
//		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CPrintSheetFrame::OnUpdateExposuresCheck(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnableExposuresButton);
	pCmdUI->SetCheck(m_bShowExposures);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnReleaseBleed() 
{
	CImpManDoc* pDoc	   = CImpManDoc::GetDoc();
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->UndoSave();

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	while (pDI)
	{
		pDoc->m_PrintSheetList.UnlockLocalMaskAllExposures((CExposure*)pDI->m_pData);

		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	}
    pDoc->SetModifiedFlag();
	if (pView)
   		pView->UpdateWindow();

	m_bEnableReleaseBleedButton = FALSE;
}

void CPrintSheetFrame::OnUpdateReleaseBleed(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnableReleaseBleedButton);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnPlatetablecheck() 
{
	m_bShowPlateTable = !m_bShowPlateTable;

}

void CPrintSheetFrame::OnUpdatePlatetablecheck(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnablePlateTableButton);
	pCmdUI->SetCheck(m_bShowPlateTable);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnBitmapcheck() 
{
	m_bShowBitmap = ! m_bShowBitmap;

	BOOL bResetView = ( ! m_bShowPlate) ? TRUE : FALSE;	// if plate is not displayed yet, reset view

	if (m_bShowBitmap)
	{
		m_bEnablePlateTableButton = FALSE;

		if ( ! m_bShowPlate)
			m_bShowPlate = TRUE;

		if (m_bShowExposures)
			m_bShowExposures = FALSE;
	}

	CheckTrimToolCtrls();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc->GetActSight() == ALLSIDES)
		// Don't update PrintSheetTreeView there would be changed the current tree-selection
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	else
		if (bResetView)
			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
		else
			// Use UpdateWindow() to keep sheet be zoomed (if zoomed by mouse)
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CPrintSheetFrame::OnUpdateBitmapcheck(CCmdUI* pCmdUI) 
{	
	pCmdUI->Enable(m_bEnableBitmapButton);
	pCmdUI->SetCheck(m_bShowBitmap);
} 

void CPrintSheetFrame::OnShowFrontback() 
{
	m_bShowFrontback = ! m_bShowFrontback;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->m_DisplayList.Invalidate();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_screenLayout.m_bLabelsShowFrontBack = m_bShowFrontback;
		CLayout* pActLayout = pDoc->GetActLayout();
		if (pActLayout)
			pActLayout->CheckForProductType();
		pDoc->SetModifiedFlag();
	}

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),		 NULL, 0, NULL);
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView), NULL, 0, NULL);
	CPageListView* pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pPageListView)
	{
		pPageListView->m_DisplayList.Invalidate();
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListView),		 NULL, 0, NULL);
	}

	m_bTurnTumbleMenuActive = m_bShowFrontback;
}

void CPrintSheetFrame::OnUpdateShowFrontback(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_bShowFrontback);
}

void CPrintSheetFrame::OnShowBoxshapes() 
{
	m_bShowBoxShapes = ! m_bShowBoxShapes;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_screenLayout.m_bLabelsShowBoxShapes = m_bShowBoxShapes;
		pDoc->SetModifiedFlag();
	}

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CPrintSheetFrame::OnUpdateShowBoxshapes(CCmdUI* pCmdUI) 
{
	BOOL bEnable = FALSE;
	pCmdUI->Enable(bEnable);
	pCmdUI->SetCheck(m_bShowBoxShapes);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnCtpTable() 
{
#ifdef CTP
	m_bShowTableCtp = ! m_bShowTableCtp;

	if (m_pDlgTableCtp && m_bShowTableCtp)
		m_pDlgTableCtp->Create(IDD_TABLE_CTP);

	if (m_pDlgTableCtp && ! m_bShowTableCtp)
		m_pDlgTableCtp->DestroyWindow();
#endif
}

void CPrintSheetFrame::OnUpdateCtpTable(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bShowTableCtp);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnOutputPlates() 
{
	m_bShowOutputPlates = ! m_bShowOutputPlates;

	if (m_pDlgOutputMedia->m_hWnd && ! m_bShowOutputPlates)
	{
		m_pDlgOutputMedia->DestroyWindow();

		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	}
	if ( ! m_pDlgOutputMedia->m_hWnd && m_bShowOutputPlates)
	{
		if ( ! m_bShowPlate)
			OnPlatecheck();
		
		m_bEnablePlateButton	  = FALSE;
		m_bEnablePlateTableButton = FALSE;
	}
}

void CPrintSheetFrame::OnUpdateOutputPlates(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bEnableOutputPlatesButton);
	pCmdUI->SetCheck(m_bShowOutputPlates);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::CheckTrimToolCtrls()
{
	if (! m_bShowFoldSheet && /*! m_bShowPlate &&*/ ! m_bShowExposures && ! m_bShowBitmap)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		int nSide		 = pDoc->GetActSight();
		if (((nSide == FRONTSIDE || nSide == BACKSIDE || nSide == BOTHSIDES) && pDoc->m_nActPlate == -1))
			m_bTrimToolMenuActive = TRUE;
	}
	else
		CloseTrimTool();
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// ToolBar -> IDR_TRIMTOOLBAR

void CPrintSheetFrame::CreateTrimToolBar()
{
	if (!m_wndTrimToolBar.Create(this,  WS_CHILD | WS_VISIBLE | CBRS_TOP, IDR_TRIMTOOLBAR) ||
		!m_wndTrimToolBar.LoadToolBar(IDR_TRIMTOOLBAR))
		TRACE0("Failed to create toolbar\n");

// TODO: Remove this if you don't want tool tips or a resizeable toolbar
	m_wndTrimToolBar.SetBarStyle(m_wndTrimToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_ALIGN_RIGHT | CBRS_SIZE_DYNAMIC | CBRS_GRIPPER);

	CString string; string.LoadString(IDS_TRIMMINGS);
	m_wndTrimToolBar.SetWindowText(LPCTSTR(string));
// TODO: Delete these three lines if you don't want the toolbar to be dockable
	m_wndTrimToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndTrimToolBar,AFX_IDW_DOCKBAR_RIGHT);
	m_wndTrimToolBar.ModifyStyle(0, TBSTYLE_FLAT);
	int nCount = m_wndTrimToolBar.GetToolBarCtrl().GetButtonCount();
	for (int i=0; i < nCount; i++)
	{
		UINT nStyle = m_wndTrimToolBar.GetButtonStyle(i);
		if(!(nStyle & TBBS_SEPARATOR))
			nStyle = TBBS_CHECKGROUP;
		if (i == 0 || i == 3 || i == 7)
			nStyle = TBBS_CHECKGROUP|TBBS_CHECKED;
		m_wndTrimToolBar.SetButtonStyle(i, nStyle);
	}
	ShowControlBar(&m_wndTrimToolBar, FALSE, FALSE); 
}

void CPrintSheetFrame::OnDefineTrim() 
{
	if (m_bTrimToolActive)
	{
		ShowControlBar(&m_wndTrimToolBar, FALSE, FALSE); 
		m_bTrimToolActive = FALSE;
		::SetCursor(theApp.m_CursorStandard);
	}
	else
	{
		ShowControlBar(&m_wndTrimToolBar, TRUE, FALSE);
		m_bTrimToolActive = TRUE;
		::SetCursor(theApp.m_CursorTrim);

		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		pDoc->m_TrimOptions.m_bChannelThrough = TRUE;
		pDoc->m_TrimOptions.m_bChannelSingle  = FALSE;
		pDoc->m_TrimOptions.m_bHoriUp		  = TRUE;
		pDoc->m_TrimOptions.m_bHoriLow		  = FALSE;
		pDoc->m_TrimOptions.m_bHoriSym		  = FALSE;
		pDoc->m_TrimOptions.m_bVertLeft		  = FALSE;
		pDoc->m_TrimOptions.m_bVertRight	  = FALSE;
		pDoc->m_TrimOptions.m_bVertSym		  = TRUE;

		CView* pView = theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			((CPrintSheetView *)pView)->m_TrimChannel.OldRect = CRect(0, 0, 0, 0);
	}

	OnUpdateSectionMenu();
}
void CPrintSheetFrame::OnUpdateDefineTrim(CCmdUI* pCmdUI) 
{
//	pCmdUI->Enable(m_bTrimToolMenuActive);
	pCmdUI->SetCheck(m_bTrimToolActive);	
}

void CPrintSheetFrame::OnTrim1() //Toolbar Button
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bChannelThrough = TRUE;
	pDoc->m_TrimOptions.m_bChannelSingle  = FALSE;
}
void CPrintSheetFrame::OnUpdateTrim1(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pCmdUI->SetRadio(pDoc->m_TrimOptions.m_bChannelThrough);	
}
void CPrintSheetFrame::OnTrim2()  //Toolbar Button
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bChannelThrough = FALSE;
	pDoc->m_TrimOptions.m_bChannelSingle  = TRUE;
}
void CPrintSheetFrame::OnUpdateTrim2(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pCmdUI->SetRadio(pDoc->m_TrimOptions.m_bChannelSingle);	
}
void CPrintSheetFrame::OnTrim3()  //Toolbar Button
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bHoriUp		  = TRUE;
	pDoc->m_TrimOptions.m_bHoriLow		  = FALSE;
	pDoc->m_TrimOptions.m_bHoriSym		  = FALSE;
}
void CPrintSheetFrame::OnUpdateTrim3(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pCmdUI->SetRadio(pDoc->m_TrimOptions.m_bHoriUp);	
}
void CPrintSheetFrame::OnTrim4()  //Toolbar Button
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bHoriUp		  = FALSE;
	pDoc->m_TrimOptions.m_bHoriLow		  = TRUE;
	pDoc->m_TrimOptions.m_bHoriSym		  = FALSE;
}
void CPrintSheetFrame::OnUpdateTrim4(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pCmdUI->SetRadio(pDoc->m_TrimOptions.m_bHoriLow);	
}
void CPrintSheetFrame::OnTrim5()  //Toolbar Button
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bHoriUp		  = FALSE;
	pDoc->m_TrimOptions.m_bHoriLow		  = FALSE;
	pDoc->m_TrimOptions.m_bHoriSym		  = TRUE;
}
void CPrintSheetFrame::OnUpdateTrim5(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pCmdUI->SetRadio(pDoc->m_TrimOptions.m_bHoriSym);	
}
void CPrintSheetFrame::OnTrim6()  //Toolbar Button
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bVertLeft		  = TRUE;
	pDoc->m_TrimOptions.m_bVertRight	  = FALSE;
	pDoc->m_TrimOptions.m_bVertSym		  = FALSE;
}
void CPrintSheetFrame::OnUpdateTrim6(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pCmdUI->SetRadio(pDoc->m_TrimOptions.m_bVertLeft);	
}
void CPrintSheetFrame::OnTrim7()  //Toolbar Button
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bVertLeft		  = FALSE;
	pDoc->m_TrimOptions.m_bVertRight	  = TRUE;
	pDoc->m_TrimOptions.m_bVertSym		  = FALSE;
}
void CPrintSheetFrame::OnUpdateTrim7(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pCmdUI->SetRadio(pDoc->m_TrimOptions.m_bVertRight);	
}
void CPrintSheetFrame::OnTrim8()  //Toolbar Button
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_TrimOptions.m_bVertLeft		  = FALSE;
	pDoc->m_TrimOptions.m_bVertRight	  = FALSE;
	pDoc->m_TrimOptions.m_bVertSym		  = TRUE;
}
void CPrintSheetFrame::OnUpdateTrim8(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pCmdUI->SetRadio(pDoc->m_TrimOptions.m_bVertSym);	
}

void CPrintSheetFrame::CloseTrimTool()
{
	m_bTrimToolMenuActive = FALSE;
	if (m_bTrimToolActive)
	{
		ShowControlBar(&m_wndTrimToolBar, FALSE, FALSE); 
		m_bTrimToolActive = FALSE;
	}
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// ToolBar -> IDR_FOLDTOOLBAR

void CPrintSheetFrame::CreateFoldToolBar()
{
	if (!m_wndFoldSheetToolBar.Create(this,  WS_CHILD | WS_VISIBLE | CBRS_TOP, IDR_FOLDTOOLBAR) ||
		!m_wndFoldSheetToolBar.LoadToolBar(IDR_FOLDTOOLBAR))
		TRACE0("Failed to create toolbar\n");
	m_wndFoldSheetToolBar.SetBarStyle(m_wndFoldSheetToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_ALIGN_RIGHT | CBRS_SIZE_DYNAMIC | CBRS_GRIPPER);
	m_bFoldSheetRotateActive = FALSE;
	m_bFoldSheetTurnActive   = FALSE;
	m_bFoldSheetTumbleActive = FALSE;
	m_wndFoldSheetToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndFoldSheetToolBar,AFX_IDW_DOCKBAR_RIGHT);
	m_wndFoldSheetToolBar.ModifyStyle(0, TBSTYLE_FLAT);
	ShowControlBar(&m_wndFoldSheetToolBar, FALSE, FALSE); 
}
//
//void CPrintSheetFrame::OnFoldsheetRotate() 
//{
//	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
//	if (pView)
//	{
//		CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
//
//		if (pDI)
//		{
//			pView->UndoSave();
//
//			((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->RotateFoldSheet90((int)pDI->m_pData);
//			
//			pView->GetDocument()->SetModifiedFlag();	// use base class method to prevent display list from being invalidated
//														// in order to keep foldsheet selected
//			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
//		}
//	}
//}
//
//void CPrintSheetFrame::OnUpdateFoldsheetRotate(CCmdUI* pCmdUI) 
//{
//	pCmdUI->Enable(m_bFoldSheetRotateActive);
//}
//
//void CPrintSheetFrame::OnFoldsheetTurn() 
//{
//	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
//	if (pView)
//	{
//		CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
//		if (pDI)
//		{
//			pView->UndoSave();
//
//			((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->TurnFoldSheet((int)pDI->m_pData);
//			
//			pView->GetDocument()->SetModifiedFlag();	// use base class method to prevent display list from being invalidated
//														// in order to keep foldsheet selected
//			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
//
//			CImpManDoc::GetDoc()->m_PageTemplateList.FindPrintSheetLocations();
//			CImpManDoc::GetDoc()->m_PageTemplateList.ReorganizePrintSheetPlates();
//			CImpManDoc::GetDoc()->m_PrintSheetList.ReorganizeColorInfos();
//		}
//	}
//}
//
//void CPrintSheetFrame::OnUpdateFoldsheetTurn(CCmdUI* pCmdUI) 
//{
//	pCmdUI->Enable(m_bFoldSheetTurnActive);
//}
//
//void CPrintSheetFrame::OnFoldsheetTumble() 
//{
//	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
//	if (pView)
//	{
//		CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
//		if (pDI)
//		{
//			pView->UndoSave();
//
//			((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->TurnFoldSheet((int)pDI->m_pData);
//			((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->RotateFoldSheet90((int)pDI->m_pData);
//			((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->RotateFoldSheet90((int)pDI->m_pData);
//			
//			pView->GetDocument()->SetModifiedFlag();	// use base class method to prevent display list from being invalidated
//														// in order to keep foldsheet selected
//			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
//
//			CImpManDoc::GetDoc()->m_PageTemplateList.FindPrintSheetLocations();
//			CImpManDoc::GetDoc()->m_PageTemplateList.ReorganizePrintSheetPlates();
//			CImpManDoc::GetDoc()->m_PrintSheetList.ReorganizeColorInfos();
//		}
//	}
//}
//
//void CPrintSheetFrame::OnUpdateFoldsheetTumble(CCmdUI* pCmdUI) 
//{
//	pCmdUI->Enable(m_bFoldSheetTumbleActive);
//}
//////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// ToolBar -> IDR_FLATWORK_TOOLBAR

void CPrintSheetFrame::CreateFlatWorkToolBar()
{
	if (!m_wndFlatWorkToolBar.Create(this,  WS_CHILD | WS_VISIBLE | CBRS_TOP, IDR_FLATWORK_TOOLBAR) ||
		!m_wndFlatWorkToolBar.LoadToolBar(IDR_FLATWORK_TOOLBAR))
		TRACE0("Failed to create toolbar\n");
	m_wndFlatWorkToolBar.SetBarStyle(m_wndFlatWorkToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_ALIGN_RIGHT | CBRS_SIZE_DYNAMIC | CBRS_GRIPPER);
	m_wndFlatWorkToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);

	CRect rect;
	RecalcLayout();
	m_wndPrintSheetFrameToolBar.GetWindowRect(&rect);
	rect.OffsetRect(1,0);
	DockControlBar(&m_wndFlatWorkToolBar, AFX_IDW_DOCKBAR_TOP, rect);
	m_wndFlatWorkToolBar.ModifyStyle(0, TBSTYLE_FLAT);

	BOOL bShow = FALSE;
	ShowControlBar(&m_wndFlatWorkToolBar, bShow, FALSE); 
}
/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnUndo() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->UndoRestore();

	m_bUndoActive = FALSE;

	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_hWnd)
			m_pDlgLayoutObjectProperties->ReinitDialog((CLayoutObject*)NULL, (CPrintSheet*)NULL);
	CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pMarksView)
	{
		pMarksView->InitObjectList();
		if (pMarksView->m_dlgLayoutObjectControl)
			if (pMarksView->m_dlgLayoutObjectControl.m_hWnd)
				pMarksView->m_dlgLayoutObjectControl.ReinitDialog((CLayoutObject*)NULL, (CPrintSheet*)NULL);
		if (pMarksView->m_dlgObjectPreviewControl)
			if (pMarksView->m_dlgObjectPreviewControl.m_hWnd)
				pMarksView->m_dlgObjectPreviewControl.ReinitDialog();
	}

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTreeView),		NULL, 0, NULL);	// important: update tree view before printsheetview
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	// because while drawing printsheets GetAct...() calls will be made
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);
}

void CPrintSheetFrame::OnUpdateUndo(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bUndoActive);
}

//void CPrintSheetFrame::OnLabelrotateClock() 
//{
//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
//	if ( ! pDoc)
//		return;
//	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
//	if ( ! pView)
//		return;
//
//	pView->UndoSave();
//
//	CLayout* pActLayout = pDoc->GetActLayout();
//	if (pActLayout)
//	{
//		pActLayout->RotateLayoutObjects(TRUE);
//	}
//	pDoc->SetModifiedFlag();	
//	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
//}
//
//void CPrintSheetFrame::OnUpdateLabelrotateClock(CCmdUI* pCmdUI) 
//{
//	pCmdUI->Enable(m_bLayoutObjectSelected);
//}
//
//void CPrintSheetFrame::OnLabelrotateCounterclock() 
//{
//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
//	if ( ! pDoc)
//		return;
//	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
//	if ( ! pView)
//		return;
//
//	pView->UndoSave();
//
//	CLayout* pActLayout = pDoc->GetActLayout();
//	if (pActLayout)
//	{
//		pActLayout->RotateLayoutObjects(FALSE);
//		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
//	}
//	pDoc->SetModifiedFlag();	
//	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
//}
//
//void CPrintSheetFrame::OnUpdateLabelrotateCounterclock(CCmdUI* pCmdUI) 
//{
//	pCmdUI->Enable(m_bLayoutObjectSelected);
//}
