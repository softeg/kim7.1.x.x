#pragma once


// CInplaceComboBox


#define DECLARE_INPLACE_COMBOBOX(CtrlName, CWndName) \
	CInplaceComboBox m_##CtrlName; \
	void CWndName::OnSelChange##CtrlName(); \
	static void FnSelChangeHandler##CtrlName(CWnd* pWnd) {((CWndName*)pWnd)->OnSelChange##CtrlName();}; 


#define CREATE_INPLACE_COMBOBOX(CtrlName, nTooltipID) \
	m_##CtrlName.Create(WS_CHILD, CRect(0,0,0,0), this, 0, nTooltipID); \
	m_##CtrlName.m_pFnSelChangeHandler = &FnSelChangeHandler##CtrlName; 


class CInplaceComboBox : public CComboBox
{
	DECLARE_DYNAMIC(CInplaceComboBox)

public:
	CInplaceComboBox();
	virtual ~CInplaceComboBox();

// Attribute
public:
	LPARAM		 m_lParam;
	CBitmap*	 m_pBitmap;
	CBitmap		 m_bmSurface;
	CDisplayItem m_di;
	CToolTipCtrl m_tooltip;
	int			 m_nTooltipID;
	CFont		 m_Font;

// Operationen
public:
	void	Activate(CRect rect, void (*pFnInitializeCallback)(CComboBox* pBox), const CString& rString, LPARAM lParam = 0, CDisplayItem di = CDisplayItem());
	void	Move(CRect rect, const CString& rString, LPARAM lParam);
	void	Deactivate();
	void	SaveBackground(const CRect& rect);
	void	RestoreBackground(const CRect& rect);
	CString GetSelString();

	void (*m_pFnSelChangeHandler)(CWnd* pWnd);


// ‹berschreibungen
	BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, int nTooltipID = -1 );


protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSelChange();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
public:
	afx_msg void OnCbnCloseup();
};


