#include "stdafx.h"
#include "imposition manager.h"
#include "OwnerDrawnMenu.h"
#include "GraphicComponent.h"

COwnerDrawnMenu::COwnerDrawnMenu(void)
{
	m_font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

COwnerDrawnMenu::~COwnerDrawnMenu(void)
{
	m_font.DeleteObject();
}


void COwnerDrawnMenu::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem = lpDrawItemStruct->rcItem;

	pDC->FillSolidRect(rcItem, RGB(252, 252, 249));//CREME);

	CRect rcIcon = rcItem;
	rcIcon.right = rcIcon.left + 45;

	if (lpDrawItemStruct->itemState & ODS_SELECTED)
	{
		CGraphicComponent gc;
		gc.DrawTransparentFrame(pDC, rcItem, RGB(120,160,220), 2);
	}				
	switch (lpDrawItemStruct->itemID)
	{
	case ID_FILE_NEW:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 5, theApp.LoadIcon(IDI_PDF_LARGE));		break;
	case ID_FILE_OPEN:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 5, theApp.LoadIcon(IDI_PRINTER_LARGE));	break;
	case ID_FILE_SAVE:	pDC->DrawIcon(rcIcon.left + 5, rcIcon.top + 5, theApp.LoadIcon(IDI_JDF_LARGE));		break;
	}

	pDC->SelectObject(&m_font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(DARKBLUE);
	CRect rcText = rcItem;
	rcText.left = rcIcon.right + 5;
	CString strText;
	GetMenuString(lpDrawItemStruct->itemID, strText, MF_BYCOMMAND);
	pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
}

void COwnerDrawnMenu::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	CString strText;
	GetMenuString(lpMeasureItemStruct->itemID, strText, MF_BYCOMMAND);
	//CWnd* pWnd = FromHandle();
	//CClientDC dc(pWnd);
	//dc.SelectObject(GetFont());
	int nMaxLen = 100;//dc.GetTextExtent(strText).cx + 50;

	lpMeasureItemStruct->itemWidth  = nMaxLen;
	lpMeasureItemStruct->itemHeight = 45;
}
