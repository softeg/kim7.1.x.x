#pragma once

class CCFFOutputImposition
{
public:
	CCFFOutputImposition(void);
	~CCFFOutputImposition(void);

public:
	void DoExport(CLayout& rLayout, CString strFilePath);
	BOOL AlreadyInList(CLayoutObject* pObject, CArray <CLayoutObject*, CLayoutObject*>& rObjectList, CPrintSheet* pPrintSheet);
};
