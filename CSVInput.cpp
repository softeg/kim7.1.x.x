#include "stdafx.h"
#include "Imposition Manager.h"
#include "CSVInput.h"
#include <locale.h>
#include "DlgPDFLibStatus.h"
#include "KimSocketsCommon.h"



CCSVInput::CCSVInput(void)
{
	m_nProductPartIndex = -1;
	m_pPageSource		= NULL;

	setlocale(LC_NUMERIC, "english");	// because when german setted, Format() does produce numbers with Comma like "0,138"
}

CCSVInput::CCSVInput(int nProductPartIndex)
{
	m_nProductPartIndex = nProductPartIndex;
	m_pPageSource		= NULL;

	setlocale(LC_NUMERIC, "english");	// because when german setted, Format() does produce numbers with Comma like "0,138"
}

CCSVInput::~CCSVInput()
{
	CString strLanguage;
	switch (theApp.settings.m_iLanguage)
	{
		case 0: strLanguage = _T("GER"); setlocale(LC_ALL, "german");	break;
		case 1: strLanguage = _T("ENG"); setlocale(LC_ALL, "english");	break;
		case 2: strLanguage = _T("FRA"); break;
		case 3: strLanguage = _T("ITA"); break;
		case 4: strLanguage = _T("SPA"); break;
		case 5: strLanguage = _T("HOL"); break;	// No holl. MFC4x.dll available
		//Default would already be set at reading ini-file
	}

	if (m_pPageSource)
		delete m_pPageSource;
	m_pPageSource = NULL;
}

BOOL CCSVInput::DoImport(const CString& strFilePath)
{
	if ( ! m_CSVFile.Open(strFilePath, CFile::modeRead))
	{
		CFileException fileException;
		TCHAR szMsgError[255];
		fileException.GetErrorMessage(szMsgError, 255);
		CString strMsg;
		strMsg.Format(_T("Can't open file %s, error = %s\n"), strFilePath, szMsgError);
		if (theApp.m_nGUIStyle != CImpManApp::GUIStandard)
			theApp.LogEntry(strMsg);
		return FALSE;
	}
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CString strDrive, strDir, strFname, strExt;
	_splitpath((LPCTSTR)strFilePath, strDrive.GetBuffer(_MAX_DRIVE), strDir.GetBuffer(_MAX_DIR), strFname.GetBuffer(_MAX_FNAME), strExt.GetBuffer(_MAX_EXT));
	pDoc->SetTitle(strFname);

	if ( ! m_pPageSource)
		m_pPageSource = new CPageSource;
	else
		m_pPageSource->m_PageSourceHeaders.RemoveAll();

	theApp.m_pDlgPDFLibStatus->m_pPageSource = m_pPageSource;
	theApp.m_pDlgPDFLibStatus->LaunchPDFEngine("", theApp.m_pMainWnd, CDlgPDFLibStatus::RegisterPages);

	int nBoundProductIndex = -1;
	CString strDataSet;
	while (m_CSVFile.ReadString(strDataSet))
	{
		int		nStringPos = 0;
		CString strToken   = GetNextToken(strDataSet, nStringPos);
		int		nIndex	   = 0;
		while (nStringPos != -1)
		{
			switch (nIndex)
			{
			case 0:		m_strProductID			= strToken;										break;	// ProductID
			case 1:		m_strSubProductName		= strToken;										break;	// SubName
			case 2:		m_strProductType		= strToken;										break;	// ProductType
			case 3:		m_strComment			= strToken;										break;	// Comment
			case 4:		m_fTrimSizeWidth		= (float)atof(strToken);						break;	// FinalSizeWidth
			case 5:		m_fTrimSizeHeight		= (float)atof(strToken);						break;	// FinalSizeHeight
			case 6:		m_nDesiredQuantity		= atoi(strToken);								break;	// OrderedQuantity
			case 7:		m_strPaper				= strToken;										break;	// MediaName
			case 8:		m_nPaperGrain			= GetPaperGrain(strToken);						break;	// PaperGrainOrientation
			case 9:		m_nSides				= (strToken.IsEmpty()) ? 2 : atoi(strToken);	break;	// Single sided or two sided
			case 10:	m_nNumPages				= (strToken.IsEmpty()) ? 1 : atoi(strToken);	break;	// NumPages
			case 11:	m_nStartPage			= atoi(strToken);								break;	// PageNum of first page
			case 12:	m_colorantList			= GetColors(strToken);							break;	// FrontSideColors
			case 13:																			break;	// BackSideColors
			case 14:	m_strContentFilePath	= strToken;										break;	// ContentFileName
			case 15:	m_fLeftTrim				= (float)atof(strToken);						break;	// MinTrimLeft
			case 16:	m_fBottomTrim			= (float)atof(strToken);						break;	// MinTrimBottom
			case 17:	m_fRightTrim			= (float)atof(strToken);						break;	// MinTrimRight
			case 18:	m_fTopTrim				= (float)atof(strToken);						break;	// MinTrimTop
			case 19:	m_printDeadline.ParseDateTime(strToken);								break;	// PrintDeadline
			case 20:																			break;	// MaxOverproduction
			case 21:	m_strBindingOrder		= strToken;										break;	// BindingOrder
			case 22:																			break;	// FoldScheme
			case 23:	m_strPunchTool			= strToken;										break;	// PunchTool
			}
			strToken.ReleaseBuffer();

			strToken = GetNextToken(strDataSet, nStringPos);
			nIndex++;
		}
		if (nIndex < 23)
			break;

		if ( ! m_strContentFilePath.IsEmpty())
		{
			theApp.m_pDlgPDFLibStatus->ProcessPDFPages(m_strContentFilePath, m_pPageSource);
			setlocale(LC_NUMERIC, "english");	// locale has been resetted in ProcessPDFPages() -> set back to english
		}

		if (m_strProductType.CompareNoCase(_T("Flat")) == 0)
		{
			CFlatProduct product;
			product.m_strProductName	= m_strSubProductName;
			product.m_strComment		= m_strComment;	
			product.m_fTrimSizeWidth	= m_fTrimSizeWidth;	
			product.m_fTrimSizeHeight	= m_fTrimSizeHeight;	
			product.m_nDesiredQuantity	= m_nDesiredQuantity;
			product.m_strPaper			= m_strPaper;		
			product.m_nPaperGrain		= m_nPaperGrain;		
			product.m_nNumPages			= m_nSides;
			product.m_colorantList		= m_colorantList;		
			product.m_fLeftTrim			= m_fLeftTrim;
			product.m_fBottomTrim		= m_fBottomTrim;
			product.m_fRightTrim		= m_fRightTrim;
			product.m_fTopTrim			= m_fTopTrim;
			product.m_printDeadline		= m_printDeadline;		
			product.m_strPunchTool		= m_strPunchTool;
			if ( ! m_strContentFilePath.IsEmpty() && (m_pPageSource->m_PageSourceHeaders.GetSize() > 0) )
			{
				for (int i = 0; i < m_pPageSource->m_PageSourceHeaders[0].m_processColors.GetSize(); i++)
				{
					CColorant colorant;
					colorant.m_strName = m_pPageSource->m_PageSourceHeaders[0].m_processColors[i];
					colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
					product.m_colorantList.Add(colorant);
				}
				for (int i = 0; i < m_pPageSource->m_PageSourceHeaders[0].m_spotColors.GetSize(); i++)
				{
					CColorant colorant;
					colorant.m_strName = m_pPageSource->m_PageSourceHeaders[0].m_spotColors[i];
					colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
					product.m_colorantList.Add(colorant);
				}
				//if (m_pPageSource->m_PageSourceHeaders[0].HasTrimBox())
				//{
				//	product.m_fTrimSizeWidth  = m_pPageSource->m_PageSourceHeaders[0].m_fTrimBoxRight  - m_pPageSource->m_PageSourceHeaders[0].m_fTrimBoxLeft;
				//	product.m_fTrimSizeHeight = m_pPageSource->m_PageSourceHeaders[0].m_fTrimBoxTop	   - m_pPageSource->m_PageSourceHeaders[0].m_fTrimBoxBottom;
				//}
				//else
				//{
				//	product.m_fTrimSizeWidth  = m_pPageSource->m_PageSourceHeaders[0].m_fBBoxRight	   - m_pPageSource->m_PageSourceHeaders[0].m_fBBoxLeft;
				//	product.m_fTrimSizeHeight = m_pPageSource->m_PageSourceHeaders[0].m_fBBoxTop	   - m_pPageSource->m_PageSourceHeaders[0].m_fBBoxBottom;
				//}
				//switch (theApp.settings.m_iUnit)
				//{
				//case MILLIMETER:	product.m_fTrimSizeWidth  = (long)(product.m_fTrimSizeWidth  * 10.0f + 0.5f) / 10.0f;	// round to 1 digit after decimal
				//					product.m_fTrimSizeHeight = (long)(product.m_fTrimSizeHeight * 10.0f + 0.5f) / 10.0f;
				//					break;
				//case CENTIMETER:	
				//case INCH:			product.m_fTrimSizeWidth  = (long)(product.m_fTrimSizeWidth  * 100.0f + 0.5f) / 100.0f;	// round to 2 digits after decimal
				//					product.m_fTrimSizeHeight = (long)(product.m_fTrimSizeHeight * 100.0f + 0.5f) / 100.0f;
				//					break;
				//}
			}
			CString strName = product.m_strProductName;
			for (int i = 0; i < m_nNumPages; i++)
			{
				if (m_nNumPages > 1)
				{
					CString strPostfix; strPostfix.Format(_T("-%d"), i + 1);
					product.m_strProductName = strName + strPostfix;
				}
				pDoc->m_flatProducts.AddTail(product);
			}
			m_nProductPartIndex = pDoc->GetFlatProductsIndex();
			pDoc->m_PageTemplateList.SynchronizeToFlatProductList(pDoc->m_flatProducts, m_nProductPartIndex);
		}
		else
		if ( (m_strProductType.CompareNoCase(_T("BoundCover")) == 0) || (m_strProductType.CompareNoCase(_T("BoundContent")) == 0) )
		{
			CBoundProduct& rBoundProduct = pDoc->m_boundProducts.FindProduct(m_strProductID);
			if (rBoundProduct.IsNull())
			{
				CBoundProduct newBoundProduct;
				newBoundProduct.m_strProductName  = m_strProductID;
				newBoundProduct.m_fTrimSizeWidth  = m_fTrimSizeWidth;
				newBoundProduct.m_fTrimSizeHeight = m_fTrimSizeHeight;
				if (m_strBindingOrder.CompareNoCase(_T("Gathering")) == 0)
				{
					newBoundProduct.m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::PerfectBound;
					newBoundProduct.m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_PERFECTBOUND_TOOLTIP);
				}
				else
				{
					newBoundProduct.m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::SaddleStitched;
					newBoundProduct.m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_SADDLESTITCHED_TOOLTIP);
				}
				pDoc->m_boundProducts.AddTail(newBoundProduct);
				nBoundProductIndex++;
			}
			else
				nBoundProductIndex = rBoundProduct.GetIndex();

			CBoundProductPart product;
			product.m_strName			= m_strSubProductName;
			product.m_strComment		= m_strComment;
			product.m_fPageWidth		= m_fTrimSizeWidth;	
			product.m_fPageHeight		= m_fTrimSizeHeight;
			product.m_nDesiredQuantity	= m_nDesiredQuantity;
			product.m_strPaper			= m_strPaper;		
			//product.m_nPaperGrain
			product.m_nNumPages			= m_nNumPages;
			product.m_colorantList		= m_colorantList;
			//product.m_printDeadline
			if ( ! m_strContentFilePath.IsEmpty() && (m_pPageSource->m_PageSourceHeaders.GetSize() > 0) )
			{
				for (int i = 0; i < m_pPageSource->m_PageSourceHeaders[0].m_processColors.GetSize(); i++)
				{
					CColorant colorant;
					colorant.m_strName = m_pPageSource->m_PageSourceHeaders[0].m_processColors[i];
					colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
					product.m_colorantList.Add(colorant);
				}
				for (int i = 0; i < m_pPageSource->m_PageSourceHeaders[0].m_spotColors.GetSize(); i++)
				{
					CColorant colorant;
					colorant.m_strName = m_pPageSource->m_PageSourceHeaders[0].m_spotColors[i];
					colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
					product.m_colorantList.Add(colorant);
				}
				//if (m_pPageSource->m_PageSourceHeaders[0].HasTrimBox())
				//{
				//	product.m_fPageWidth  = m_pPageSource->m_PageSourceHeaders[0].m_fTrimBoxRight  - m_pPageSource->m_PageSourceHeaders[0].m_fTrimBoxLeft;
				//	product.m_fPageHeight = m_pPageSource->m_PageSourceHeaders[0].m_fTrimBoxTop	   - m_pPageSource->m_PageSourceHeaders[0].m_fTrimBoxBottom;
				//}
				//else
				//{
				//	product.m_fPageWidth  = m_pPageSource->m_PageSourceHeaders[0].m_fBBoxRight	   - m_pPageSource->m_PageSourceHeaders[0].m_fBBoxLeft;
				//	product.m_fPageHeight = m_pPageSource->m_PageSourceHeaders[0].m_fBBoxTop	   - m_pPageSource->m_PageSourceHeaders[0].m_fBBoxBottom;
				//}
				//switch (theApp.settings.m_iUnit)
				//{
				//case MILLIMETER:	product.m_fPageWidth  = (long)(product.m_fPageWidth  * 10.0f + 0.5f) / 10.0f;	// round to 1 digit after decimal
				//					product.m_fPageHeight = (long)(product.m_fPageHeight * 10.0f + 0.5f) / 10.0f;
				//					break;
				//case CENTIMETER:	
				//case INCH:			product.m_fPageWidth  = (long)(product.m_fPageWidth  * 100.0f + 0.5f) / 100.0f;	// round to 2 digits after decimal
				//					product.m_fPageHeight = (long)(product.m_fPageHeight * 100.0f + 0.5f) / 100.0f;
				//					break;
				//}
			}
			pDoc->m_boundProducts.GetProduct(nBoundProductIndex).m_parts.Add(product);
			m_nProductPartIndex = pDoc->m_boundProducts.GetProduct(nBoundProductIndex).m_parts[pDoc->GetBoundProduct(nBoundProductIndex).m_parts.GetSize() - 1].GetIndex();

			pDoc->m_PageTemplateList.ShiftProductPartIndices(m_nProductPartIndex);
			pDoc->m_Bookblock.ShiftProductPartIndices(m_nProductPartIndex);
			pDoc->m_PageTemplateList.InsertProductPartPages(product.m_nNumPages, nBoundProductIndex, m_nProductPartIndex);

#ifdef KIM_ENGINE
			int nDBProductID = KimEngineDBSetProduct(theApp.m_nDBCurrentJobID, -1, kimAutoDB_product::Bound, rBoundProduct.m_strProductName, product.m_strName, product.m_fPageWidth, product.m_fPageHeight, product.m_nNumPages);
			KimEngineNotifyServer(MSG_PRODUCT_STATUS_CHANGED, nDBProductID);
#endif
		}

		if (m_pPageSource->m_PageSourceHeaders.GetSize())
		{
			pDoc->m_PageSourceList.AddTail(*m_pPageSource);
			int nNumPageSourceHeaders = pDoc->m_PageSourceList.GetTail().m_PageSourceHeaders.GetSize();
			int nCount				  = (m_strProductType.CompareNoCase(_T("Flat")) == 0) ? (int)ceil(float(2 * m_nNumPages * m_nSides) / (float)nNumPageSourceHeaders) - 1 : (int)ceil(float(m_nNumPages * m_nSides) / (float)nNumPageSourceHeaders) - 1;
			while (nCount)
			{
				pDoc->m_PageTemplateList.AutoAssignPageSource(&pDoc->m_PageSourceList.GetTail(), m_nProductPartIndex);
				nCount--;
			}
			pDoc->m_PageSourceList.InitializePageTemplateRefs();
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
			m_pPageSource->m_PageSourceHeaders.RemoveAll();
			*m_pPageSource = CPageSource();
		}
	}

	if (theApp.m_pDlgPDFLibStatus->m_hWnd)
		theApp.m_pDlgPDFLibStatus->DestroyWindow();
	
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	
	theApp.m_pDlgPDFLibStatus->m_pPageSource = NULL;
	if (m_pPageSource)
		delete m_pPageSource;
	m_pPageSource = NULL;

	return TRUE;
}

CString CCSVInput::GetNextToken(CString& strData, int& nStartPos)
{
	int nPos = strData.Find(_T(","), nStartPos);
	if (nPos == -1)
	{
		nStartPos = -1;
		return _T("");
	}

	CString strToken = strData.Mid(nStartPos, nPos - nStartPos);
	nStartPos = nPos + 1;
	return strToken;
}

int CCSVInput::GetPaperGrain(CString strPaperGrain)
{
	strPaperGrain.MakeUpper();
	if (strPaperGrain == _T("VERTICAL"))
		return GRAIN_VERTICAL;
	else
	if (strPaperGrain == _T("HORIZONTAL"))
		return GRAIN_HORIZONTAL;
	else
		return GRAIN_EITHER;
}

CColorantList CCSVInput::GetColors(CString strColorCode)
{
	static CColorantList nullColorantList;

	strColorCode.Trim();
	if (strColorCode.IsEmpty())
		return nullColorantList;

	CColorantList colorantList;
	int			  nStringPos = 0;
	CString		  strToken   = strColorCode.Tokenize(_T("/"), nStringPos);
	while (nStringPos != -1)
	{
		CColorant colorant;
		if (strToken == _T("C"))
			colorant.m_strName = theApp.m_colorDefTable.GetCyanName();
		else
		if (strToken == _T("M"))
			colorant.m_strName = theApp.m_colorDefTable.GetMagentaName();
		else
		if (strToken == _T("Y"))
			colorant.m_strName = theApp.m_colorDefTable.GetYellowName();
		else
		if (strToken == _T("K"))
			colorant.m_strName = theApp.m_colorDefTable.GetKeyName();
		else
			colorant.m_strName = strToken;

		if ( ! colorant.m_strName.IsEmpty())
		{
			colorant.m_crRGB   = theApp.m_colorDefTable.FindColorRef(colorant.m_strName);
			colorantList.Add(colorant);
		}

		strToken = strColorCode.Tokenize(_T("/"), nStringPos);
	}

	return colorantList;
}
