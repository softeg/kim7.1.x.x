// DlgPrintColorProps.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgPrintColorProps.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPrintColorProps 


CDlgPrintColorProps::CDlgPrintColorProps(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPrintColorProps::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPrintColorProps)
	m_strPrintColorName = _T("");
	m_nPrintColorType = -1;
	m_strColorMappingTarget = _T("");
	m_nProcessCMYK = -1;
	m_nIPUIndex = 0;
	m_nCyan = 0;
	m_nKey = 0;
	m_nMagenta = 0;
	m_nYellow = 0;
	//}}AFX_DATA_INIT
}


void CDlgPrintColorProps::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPrintColorProps)
	DDX_Control(pDX, IDC_COLOR_PREVIEW, m_colorPreview);
	DDX_Control(pDX, IDC_COLORMAPPING_TARGET, m_colorMappingTargetCombo);
	DDX_Text(pDX, IDC_PRINTCOLOR_NAME, m_strPrintColorName);
	DDX_Radio(pDX, IDC_PRINTCOLOR_IS_PROCESS, m_nPrintColorType);
	DDX_CBString(pDX, IDC_COLORMAPPING_TARGET, m_strColorMappingTarget);
	DDX_Radio(pDX, IDC_PROCESS_CYAN, m_nProcessCMYK);
	DDX_Text(pDX, IDC_REFERENCE_INDEX, m_nIPUIndex);
	DDX_Text(pDX, IDC_CYAN_EDIT, m_nCyan);
	DDV_MinMaxInt(pDX, m_nCyan, 0, 100);
	DDX_Text(pDX, IDC_KEY_EDIT, m_nKey);
	DDV_MinMaxInt(pDX, m_nKey, 0, 100);
	DDX_Text(pDX, IDC_MAGENTA_EDIT, m_nMagenta);
	DDV_MinMaxInt(pDX, m_nMagenta, 0, 100);
	DDX_Text(pDX, IDC_YELLOW_EDIT, m_nYellow);
	DDV_MinMaxInt(pDX, m_nYellow, 0, 100);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPrintColorProps, CDialog)
	//{{AFX_MSG_MAP(CDlgPrintColorProps)
	ON_BN_CLICKED(IDC_PRINTCOLOR_IS_PROCESS, OnPrintcolorIsProcess)
	ON_BN_CLICKED(IDC_PRINTCOLOR_IS_MAPPED, OnPrintcolorIsMapped)
	ON_BN_CLICKED(IDC_PRINTCOLOR_IS_SPOT, OnPrintcolorIsSpot)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_DEFINE_SPOTCOLOR, OnDefineSpotcolor)
	ON_CBN_SELCHANGE(IDC_COLORMAPPING_TARGET, OnSelchangeColormappingTarget)
	ON_BN_CLICKED(IDC_PROCESS_CYAN, OnProcessCyan)
	ON_BN_CLICKED(IDC_PROCESS_KEY, OnProcessKey)
	ON_BN_CLICKED(IDC_PROCESS_MAGENTA, OnProcessMagenta)
	ON_BN_CLICKED(IDC_PROCESS_YELLOW, OnProcessYellow)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgPrintColorProps 

BOOL CDlgPrintColorProps::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect winRect, divRect;
	GetWindowRect(winRect);
	GetDlgItem(IDC_IPUINDEX_DIVIDER)->GetWindowRect(divRect);
	winRect.bottom = divRect.top - 15;
	MoveWindow(winRect);

	((CSpinButtonCtrl*)GetDlgItem(IDC_REFERENCE_INDEX_SPIN))->SetRange(0, 99);

	((CButton*)GetDlgItem(IDC_PROCESS_CYAN)	  )->SetIcon(theApp.LoadIcon(IDI_CYAN));
	((CButton*)GetDlgItem(IDC_PROCESS_MAGENTA))->SetIcon(theApp.LoadIcon(IDI_MAGENTA));
	((CButton*)GetDlgItem(IDC_PROCESS_YELLOW) )->SetIcon(theApp.LoadIcon(IDI_YELLOW));
	((CButton*)GetDlgItem(IDC_PROCESS_KEY)	  )->SetIcon(theApp.LoadIcon(IDI_KEY));

	if (m_nProcessCMYK == -1)
		m_nProcessCMYK = 0;

	for (int i = 0; i < m_colorDefTable.GetSize(); i++) 
		m_colorMappingTargetCombo.AddString(m_colorDefTable[i].m_strColorName);

	m_colorMappingTargetCombo.SetCurSel(0);

	UpdateData(FALSE);
	
	UpdateControls();

	GetDlgItem(IDC_PRINTCOLOR_NAME)->SetFocus();
	((CEdit*)GetDlgItem(IDC_PRINTCOLOR_NAME))->SetSel(0, -1);

	m_strInitialPrintColorName = m_strPrintColorName;

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}


void CDlgPrintColorProps::UpdateControls()
{
	switch (m_nPrintColorType)
	{
	case 0:	GetDlgItem(IDC_PROCESS_CYAN		  )->EnableWindow(TRUE);
			GetDlgItem(IDC_PROCESS_MAGENTA	  )->EnableWindow(TRUE);
			GetDlgItem(IDC_PROCESS_YELLOW	  )->EnableWindow(TRUE);
			GetDlgItem(IDC_PROCESS_KEY		  )->EnableWindow(TRUE);
			GetDlgItem(IDC_DEFINE_SPOTCOLOR	  )->EnableWindow(FALSE);
			GetDlgItem(IDC_COLORMAPPING_TARGET)->EnableWindow(FALSE);
			switch (m_nProcessCMYK)
			{
			case CColorDefinition::ProcessC:	m_rgbColor = CYAN;		m_nCyan = 100; m_nMagenta = 0;   m_nYellow = 0;   m_nKey = 0;   break;
			case CColorDefinition::ProcessM:	m_rgbColor = MAGENTA;	m_nCyan = 0;   m_nMagenta = 100; m_nYellow = 0;   m_nKey = 0;   break;
			case CColorDefinition::ProcessY:	m_rgbColor = YELLOW;	m_nCyan = 0;   m_nMagenta = 0;   m_nYellow = 100; m_nKey = 0;   break;
			case CColorDefinition::ProcessK:	m_rgbColor = BLACK;		m_nCyan = 0;   m_nMagenta = 0;   m_nYellow = 0;   m_nKey = 100; break;
			}
			GetDlgItem(IDC_CYAN_EDIT	)->EnableWindow(FALSE);
			GetDlgItem(IDC_MAGENTA_EDIT	)->EnableWindow(FALSE);
			GetDlgItem(IDC_YELLOW_EDIT	)->EnableWindow(FALSE);
			GetDlgItem(IDC_KEY_EDIT		)->EnableWindow(FALSE);
			break;
	case 1: GetDlgItem(IDC_PROCESS_CYAN		  )->EnableWindow(FALSE);
			GetDlgItem(IDC_PROCESS_MAGENTA	  )->EnableWindow(FALSE);
			GetDlgItem(IDC_PROCESS_YELLOW	  )->EnableWindow(FALSE);
			GetDlgItem(IDC_PROCESS_KEY		  )->EnableWindow(FALSE);
			GetDlgItem(IDC_DEFINE_SPOTCOLOR	  )->EnableWindow(TRUE);
			GetDlgItem(IDC_COLORMAPPING_TARGET)->EnableWindow(FALSE);
			GetDlgItem(IDC_CYAN_EDIT	)->EnableWindow(TRUE);
			GetDlgItem(IDC_MAGENTA_EDIT	)->EnableWindow(TRUE);
			GetDlgItem(IDC_YELLOW_EDIT	)->EnableWindow(TRUE);
			GetDlgItem(IDC_KEY_EDIT		)->EnableWindow(TRUE);
			if ( (m_rgbColor == WHITE) || (m_rgbColor == CYAN) || (m_rgbColor == MAGENTA) || (m_rgbColor == YELLOW) || (m_rgbColor == BLACK) )
				m_rgbColor = m_colorDefTable.FindColorRef(m_strPrintColorName);
			break;
	case 2: GetDlgItem(IDC_PROCESS_CYAN		  )->EnableWindow(FALSE);
			GetDlgItem(IDC_PROCESS_MAGENTA	  )->EnableWindow(FALSE);
			GetDlgItem(IDC_PROCESS_YELLOW	  )->EnableWindow(FALSE);
			GetDlgItem(IDC_PROCESS_KEY		  )->EnableWindow(FALSE);
			GetDlgItem(IDC_DEFINE_SPOTCOLOR	  )->EnableWindow(FALSE);
			GetDlgItem(IDC_COLORMAPPING_TARGET)->EnableWindow(TRUE);
			GetDlgItem(IDC_CYAN_EDIT	)->EnableWindow(FALSE);
			GetDlgItem(IDC_MAGENTA_EDIT	)->EnableWindow(FALSE);
			GetDlgItem(IDC_YELLOW_EDIT	)->EnableWindow(FALSE);
			GetDlgItem(IDC_KEY_EDIT		)->EnableWindow(FALSE);
			m_rgbColor  = m_colorDefTable.FindColorRef(m_strColorMappingTarget);
			m_nIPUIndex = m_colorDefTable.GetColorIndex(m_colorDefTable.FindColorDefinitionIndex(m_strColorMappingTarget));
			CColorDefinition* pColorDef = m_colorDefTable.FindColorDefinition(m_strColorMappingTarget);
			m_nCyan		= GetCValue((pColorDef) ? pColorDef->m_cmyk : 0);
			m_nMagenta	= GetMValue((pColorDef) ? pColorDef->m_cmyk : 0);
			m_nYellow	= GetYValue((pColorDef) ? pColorDef->m_cmyk : 0);
			m_nKey		= GetKValue((pColorDef) ? pColorDef->m_cmyk : 0);
			break;
	}

	UpdateData(FALSE);

	CRect colorRect;
	m_colorPreview.GetClientRect(colorRect);						  
	m_colorPreview.MapWindowPoints(this, colorRect);
	InvalidateRect(colorRect);
	UpdateWindow();
}	

void CDlgPrintColorProps::OnPrintcolorIsProcess() 
{
	UpdateData(TRUE);
	UpdateControls();
}

void CDlgPrintColorProps::OnProcessCyan() 
{
	UpdateData(TRUE);
	UpdateControls();
}

void CDlgPrintColorProps::OnProcessMagenta() 
{
	UpdateData(TRUE);
	UpdateControls();
}

void CDlgPrintColorProps::OnProcessYellow() 
{
	UpdateData(TRUE);
	UpdateControls();
}

void CDlgPrintColorProps::OnProcessKey() 
{
	UpdateData(TRUE);
	UpdateControls();
}

void CDlgPrintColorProps::OnPrintcolorIsMapped() 
{
	UpdateData(TRUE);
	UpdateControls();
}

void CDlgPrintColorProps::OnPrintcolorIsSpot() 
{
	UpdateData(TRUE);
	UpdateControls();
}

void CDlgPrintColorProps::OnSelchangeColormappingTarget() 
{
	UpdateData(TRUE);
	UpdateControls();
}

void CDlgPrintColorProps::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CRect colorRect;
	m_colorPreview.GetClientRect(colorRect);						  
	m_colorPreview.MapWindowPoints(this, colorRect);
	dc.FillSolidRect(colorRect, m_rgbColor);

	if (m_nPrintColorType == 2)
		dc.DrawIcon(colorRect.TopLeft(), theApp.LoadIcon(IDI_PRINTCOLOR_LINK));
}

void CDlgPrintColorProps::OnDefineSpotcolor() 
{
	UpdateData(TRUE);

	COLORREF crColor = m_rgbColor;
	if ( (m_rgbColor == WHITE) && (CMYK(m_nCyan, m_nMagenta, m_nYellow, m_nKey) != 0) )
		crColor = CColorDefinition::CMYK2RGB(CMYK(m_nCyan, m_nMagenta, m_nYellow, m_nKey));

	CColorDialog dlg(crColor);
	if (dlg.DoModal() == IDCANCEL)
		return;

	m_nPrintColorType = 1;
	
	m_rgbColor = dlg.GetColor();

	if (CMYK(m_nCyan, m_nMagenta, m_nYellow, m_nKey) == 0)
	{
		COLORREF crCMYK = CColorDefinition::RGB2CMYK(m_rgbColor);
		m_nCyan		= GetCValue(crCMYK);
		m_nMagenta	= GetMValue(crCMYK);
		m_nYellow	= GetYValue(crCMYK);
		m_nKey		= GetKValue(crCMYK);
	}

	UpdateControls();
}

void CDlgPrintColorProps::OnOK() 
{
	UpdateData(TRUE);

	if (m_strInitialPrintColorName != m_strPrintColorName)
		if ( ! m_colorDefTable.FindColorReferenceName(m_strPrintColorName).IsEmpty())
		{
			CString strMsg;
			AfxFormatString1(strMsg, IDS_PRINTCOLOR_EXIST_MESSAGE, m_strPrintColorName);
			AfxMessageBox(strMsg, MB_OK);
			return;
		}
	
	CDialog::OnOK();
}
