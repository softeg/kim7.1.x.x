#pragma once

#include "MyBitmapButton.h"
#include "OwnerDrawnButtonList.h"
#include "DlgSelectFoldSchemePopup.h"
#include "DlgSelectPressDevicePopup.h"
#include "DlgSelectFoldSchemePopup.h"
#include "DlgNumeration.h"
#include "PrintSheetNavigationView.h"
#include "ProductsView.h"


#define ID_MAGNIFY_SHEET 1234
#define ID_GOTO_DETAILS  ID_MAGNIFY_SHEET + 1


class CPrintSheetInfo
{
public:
	CPrintSheet* m_pPrintSheet;
	CRect		 m_rcDisplay;
};


///////////////////////////////////////////////////////////////////////////////////////////////////
// bars for separation of print sheet columns

class CSeparatorBars
{
public:
	CSeparatorBars();           


// Attributes
public:
	BOOL				 m_bMouseIsOver;
	BOOL				 m_bDragging;
	int					 m_nDragIndex;
	CArray<CRect, CRect> m_rectList, m_rectListOld;

// Operations
public:
	void OnMouseMove(BOOL bLButton, CPoint& rPoint, class CPrintSheetPlanningView* pView);
	void BeginDrag(CPoint& rPoint, class CPrintSheetPlanningView* pView);
	void OnDrag	  (CPoint& rPoint, class CPrintSheetPlanningView* pView);
	void OnDrop	  (CPoint& rPoint, class CPrintSheetPlanningView* pView);
};
///////////////////////////////////////////////////////////////////////////////////////////////////


// CPrintSheetPlanningView-Ansicht

class CPrintSheetPlanningView : public CScrollView
{
	DECLARE_DYNCREATE(CPrintSheetPlanningView)

protected:
	CPrintSheetPlanningView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetPlanningView();

	DECLARE_INPLACE_EDIT(InplaceEdit, CPrintSheetPlanningView);
	DECLARE_INPLACE_COMBOBOX(InplaceComboBox, CPrintSheetPlanningView);

public:
	DECLARE_DISPLAY_LIST(CPrintSheetPlanningView, OnMultiSelect, YES, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, ProductGroup,				OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PrintingProfile,			OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, DefaultPrintingProfile,	OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PrintGroup,				OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PrintGroupComponent,		OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, BoundProduct,				OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, BoundProductPart,			OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PrintFoldSheetComponent,	OnSelect, NO,  OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, LayoutObject,				OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, FALSE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, FoldSheet,				OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, YES, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, LayoutData,				OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PrintSheetDetails,		OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, SheetFrame,				OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, ControlMarks,				OnSelect, YES, OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PrintSheetNumber,			OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, Layout,					OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, LayoutName,				OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PrintSheetQuantity,		OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PrintSheetQuantityExtra,	OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, FoldSheetNumber,			OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, FoldSheetQuantityPlanned, OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, FoldSheetQuantityExtra,	OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, FoldSheetPaperName,		OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PageNumber,				OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetPlanningView, PaperListEntryComment,	OnSelect, YES, OnDeselect, NO,  OnActivate, YES, OnDeactivate, NO,  OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)


enum displayColIDs {ColPrintGroup, ColPrintingProfile, ColPrinting, ColCutting, ColCollecting, ColBinding};
typedef struct displayColumn
{
	int nID;
	int nWidth;
};
typedef CArray <struct displayColumn, struct displayColumn&> DISPLAY_COLUMN_LIST;

public:
	DISPLAY_COLUMN_LIST		m_displayColumns;


enum {ShowImposition, ShowPrintSheetLayouts, ShowPrintSheetList, ShowFoldSheetList, ShowPaperList, ShowPages};

	int											m_nShowPlanningView;
	CHeaderCtrl  								m_HeaderCtrl;
	int			 								m_nHeaderHeight;
	int											m_nCurrentHeaderType;
	COwnerDrawnButtonList						m_planningToolBar;
	COwnerDrawnButtonList						m_strippingToolBar;
	COwnerDrawnButtonList						m_printSheetListToolBar;
	COwnerDrawnButtonList						m_foldSheetListToolBar;
	COwnerDrawnButtonList						m_pagesListToolBar;
	int											m_nToolBarHeight;
	int											m_nHeaderItemsTotalWidth;
	CFont		 								m_HeaderFont;
	CArray <int, int>							m_headerColumnWidths;
	CSize										m_docSize;
	COleDropTarget								m_dropTarget;
	CRect										m_catchRect;
	CPoint										m_ptDragIcon;
	HICON										m_hDragIcon;
	CBitmap										m_bmDragBackground;
	CRect										m_rectDragIcon;
	CString										m_strDragIcon;
	CPoint										m_dragPoint;
	CRect										m_rectBk;
	HICON										m_hArrowIcon;
	CBitmap*									m_pBkBitmap;
	CPen										m_redPen;
	CMyBitmapButton								m_magnifyButton, m_gotoDetailsButton;
	int											m_nProductPartIndex;
	CArray <CPrintSheetInfo, CPrintSheetInfo&>	m_printSheetInfos;
	int											m_nLastSelectedComponentIndex;
	CSeparatorBars								m_separatorBars;
	int											m_nPrintSheetRowHeight;
	CDlgSelectPressDevicePopup					m_dlgPressDeviceSelector;
	CDlgSelectFoldSchemePopup					m_dlgFoldSchemeSelector;
	CDlgNumeration								m_dlgNumeration;
	CBitmap										m_bkMouseMove;
	COLORREF									m_boxFrameColor;


public:
	void							ResetDisplayColums();
	void							AddDisplayColumn(int nColID, int nWidth);
	class COrderDataFrame*			GetOrderDataFrame();
	CSize							CalcDocSize();
	CPrintGroupComponent&			GetSelPrintGroupComponent();
	CFoldSheet* 					GetSelFoldSheet ();
	int								GetSelLayerIndex();
	CFlatProduct*					GetSelFlatProduct();
	CPrintSheet*					GetSelPrintSheet();
	CLayout*						GetSelLayout();
	CPressDevice*					GetSelPressDevice();
	int								GetActiveProductGroupIndex();
	int								GetActiveLabelIndex();
	int								GetComponentIndex(CDisplayItem& rDI);
	int								GetPrintSheetIndex(CDisplayItem& rDI);
	int								GetSelectedComponentIndex();
	BOOL							IsSelectedComponentIndex(int nComponentIndex);
	void							SelectComponentByIndex(int nSelIndex);
	void							SelectComponentByFoldSheet(CFoldSheet* pFoldSheet, BOOL bScrollTo = TRUE);
	void							SelectComponentByFlatProduct(CFlatProduct* pFlatProduct, BOOL bScrollTo = TRUE);
	void							SetProductionPlanningHeaderWidth(int nIndex, int nValue);
	int								GetProductionPlanningHeaderWidth(int nIndex, int nDefault);
	int								GetSelectedPrintSheetIndex();
	BOOL							IsSelectedPrintSheetIndex(int nPrintSheetIndex);
	void							ClipHeader(CDC* pDC);
	void							SaveBitmap	 (CBitmap* pBitmap, const CRect& rect);
	void							RestoreBitmap(CBitmap* pBitmap, const CRect& rect);
	void							SaveBackground	 (CDC* pDC, const CRect& rect);
	void							RestoreBackground(CDC* pDC, const CRect& rect);
	DROPEFFECT						OnDragOverPrintGroupComponent(CDisplayItem& di, DWORD dwKeyState, CPoint point);
	DROPEFFECT						OnDragOverFoldSheet(CDisplayItem& di, DWORD dwKeyState, CPoint point);
	BOOL							OnDropFoldSheet	   (CDisplayItem& di, DROPEFFECT dropEffect, CPoint point);
	BOOL							MoveCopyLayout(CDisplayItem& sourceDI, CDisplayItem& targetDI, int nSide, DROPEFFECT dropEffect);
	void							UndoSave();
	void							UndoRestore();
	int								GetPrintGroupIndex();
	CFoldSheet*						GetFoldSheet();
	CFoldSheet*						GetPrevFoldSheet(CFoldSheet* pFoldSheet);
	CFoldSheet*						GetNextFoldSheet(CFoldSheet* pFoldSheet);
	int								GetLayerIndex();
	CPrintSheet*					GetPrintSheet();
	CPageTemplate*					GetPageTemplate();
	CLayout*						GetLayout();
	CPressDevice*					GetPressDevice();
	CPrintSheetView*				GetPrintSheetView();
	CPrintSheetPlanningView*		GetPrintSheetPlanningView();
	CPrintSheetNavigationView*		GetPrintSheetNavigationView();
	CPrintSheetViewStrippingData*	GetPrintSheetViewStrippingData();
	CProductsView*					GetProductsView();
	void							HandleSelectBoundPrintGroupComponent(CPrintGroup& rPrintGroup, CPrintGroupComponent& rComponent);
	void							HandleSelectFlatPrintGroupComponent(CPrintGroup& rPrintGroup, CPrintGroupComponent& rComponent);
	void							HandleOnSelectFoldSheetComponent();
	void							HandleOnSelectFoldSheetComponentPrintSheetFrame();
	void							HandleOnSelectLabelComponent();
	void							HandleOnSelectLabelComponentPrintSheetFrame();
	void							HandleOnSelectPrintSheet();
	static void						DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd);
	void							ImposeLabels();
	void							AddRemoveFoldsheet(BOOL bAdd);
	BOOL							SelectUnassignedPages();
	BOOL							SelectFirstComponent();
	void							AddNewPrintSheet(int nPressDeviceIndex);
	void							AddNewPrintSheetPickupPressDevice(CPoint ptPos);
	static void						OnSelchangePressDeviceSelector(CPressDevice* /*pNewPressDevice*/, int nPressDeviceIndex, CDlgSelectPressDevicePopup* pDlgSelector);
	void							UpdatePrintSheetNumber();
	void							UpdateLayoutName();
	void							UpdatePrintSheetQuantity();
	void							UpdatePrintSheetQuantityExtra();
	void							UpdatePaperListEntryComment();
	void							UpdateFoldSheetNumber();
	void							UpdatePageNumber();
	void							Draw(CDC* pDC, CDisplayList* pDisplayList = NULL);
	CRect							DrawPlanningTable			(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	CRect							DrawUnassignedProductGroups	(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	CRect							DrawGroupsAndProfiles		(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	CRect							DrawPrintingAndCutting		(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	CRect							DrawCollecting				(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	CRect							DrawBinding					(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	void							DrawPlanningTableLinks		(CDC* pDC,				  CDisplayList* pDisplayList = NULL);
	CRect							DrawPrintSheetList			(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	CRect							DrawFoldSheetList			(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	CRect							DrawPaperList				(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	CRect							DrawPageList				(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList = NULL);
	void							ZoomWindow(const CPoint& rPoint, BOOL bRButton = FALSE);
	BOOL							NoFoldSheetComponents();
	BOOL							NoLabelComponents();
	void							LoadStandardLayout(CLayout* pLayout, CPrintSheet* pPrintSheet);
	void							SaveStandardLayout(CLayout* pLayout, CPrintSheet* pPrintSheet);
	CString							ExtractStandardName(CString strFileTitle);
	void							NumberPrintSheets();
	static BOOL						InitPrintSheetNumerationDialog(CDlgNumeration* pDlg);
	static void						ChangePrintSheetNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex);
	void							SelectAllPrintSheets();
	void							SelectRangePrintSheets(int nFrom, int nTo);
	BOOL							IsOpenPrintSheetNumeration();
	BOOL							IsOpenFoldSheetSelector();
	BOOL							IsSelectedPrintSheetNumber(int nPageIndex);
	BOOL							IsInsideNumerationRange(int nIndex);
	void							RemovePrintSheet(CPrintSheet* pPrintSheet);
	void							RemovePrintGroupPrintSheets(CPrintGroup& rPrintGroup);
	void							NumberFoldSheets();
	static BOOL						InitFoldSheetNumerationDialog(CDlgNumeration* pDlg);
	static void						ChangeFoldSheetNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex);
	void							SelectAllFoldSheets();
	void							SelectRangeFoldSheets(int nFrom, int nTo);
	void							SelectProductPartFoldSheets(int nProductPartIndex);
	void		 					UpdateFoldSheetQuantityPlanned();
	void		 					UpdateFoldSheetQuantityExtra();
	void							UpdateFoldSheetPaperName();
	void							NumberPages();
	static BOOL						InitPageNumerationDialog(CDlgNumeration* pDlg);
	static void						ChangePageNumeration(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex);
	void							SelectAllPages();
	void							SelectRangePages(int nFrom, int nTo);
	void							SelectProductPartPages(int nProductPartIndex);
	BOOL							IsOpenPageNumeration();
	BOOL							IsSelectedPageNumber(int nPageIndex);
	static void	 					InitializePaperNameCombo(CComboBox* pBox);
	BOOL							IsOpenFoldSheetNumeration();
	BOOL							IsSelectedFoldSheetNumber(int nPageIndex);


public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	virtual void OnInitialUpdate();     // Erster Aufruf nach Erstellung
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
protected:

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
protected:
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	virtual void OnDragLeave();
	virtual DROPEFFECT OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual void ScrollToPosition(POINT pt);
	virtual void ScrollToDevicePosition(POINT ptDev);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnMagnifySheet();
	afx_msg void OnGotoDetails();
	afx_msg void OnNcMouseMove(UINT nHitTest, CPoint point);
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	afx_msg void OnNcPaint();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnHeaderTrack	 (NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnProductGroupRules();
	afx_msg void OnDoImpose();
	afx_msg void OnDoImposeRemoveAll();
	afx_msg void OnUpdateProductGroupRules(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDoImpose(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDoImposeRemoveAll(CCmdUI* pCmdUI);
	afx_msg void OnDeleteSelection();
	afx_msg void OnUndo();
	afx_msg void OnLoadStandard();
	afx_msg void OnStoreStandard();
	afx_msg void OnStoreCFF();
	afx_msg void OnDoNumber();
	afx_msg void OnUpdateDeleteSelection(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUndo(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLoadStandard(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStoreStandard(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStoreCFF(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDoNumber(CCmdUI* pCmdUI);
	afx_msg void OnCommandNextStep();
};


