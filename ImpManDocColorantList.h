

////////////////////////////// CColorant ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CColorant : public CObject
{
public:
    DECLARE_SERIAL( CColorant )
	CColorant();
	CColorant(COLORREF crRGB, const CString& strName);
	CColorant(const CColorant& rColorant); // copy constructor

//Attributes:
public:
	COLORREF	m_crRGB;
	CString		m_strName;

//Operations:
public:
	const CColorant& operator= (const CColorant& rColorant);
	BOOL operator==(const CColorant& rColorant);
	BOOL operator!=(const CColorant& rColorant);
	void  Copy(const CColorant& rColorant);
};

template <> void AFXAPI SerializeElements <CColorant> (CArchive& ar, CColorant* pColorant, INT_PTR nCount);

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////



///////////////////////////// CColorantList //////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CColorantList : public CArray <CColorant, CColorant&> 
{
public:
    DECLARE_SERIAL( CColorantList )
	CColorantList();
	CColorantList(CColorantList& rColorantList);

//Attributes:
public:

//Operations:
public:
	const CColorantList& operator= (const CColorantList& rColorantList);
	BOOL				 operator==(const CColorantList& rColorantList);
	CColorant*	FindColorant(CString& strName);
	void		AddSeparation(CString strSep, class CSpotcolorDefList* pSpotcolorDefList = NULL);
	void		AddSeparations(CStringArray& strSeps);
	void		MixColors(CColorantList& rColorantList);
	CString		GetProcessColorCode();
	void		GetSpotColors(CStringArray& spotColors);
	BOOL		HasCyan();
	BOOL		HasMagenta();
	BOOL		HasYellow();
	BOOL		HasKey();
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
