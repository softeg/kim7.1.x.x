#pragma once


// CGroupControl

class CGroupControl : public CStatic
{
	DECLARE_DYNAMIC(CGroupControl)

public:
	CGroupControl();
	virtual ~CGroupControl();

public:
	CFont m_font;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
	afx_msg void DrawItem(LPDRAWITEMSTRUCT);
};


