// DlgMarkProperties.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "ObjectPivotControl.h"
#include "DlgPDFMarkGeneral.h"
#include "DlgMarkPositioning.h"
#include "DlgMoveObjects.h"
#include "DlgMarkProperties.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMarkProperties 


CDlgMarkProperties::CDlgMarkProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgMarkProperties::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgMarkProperties)
	//}}AFX_DATA_INIT

	m_pMarkObject = NULL;
}

CDlgMarkProperties::~CDlgMarkProperties()
{
	DestroyWindow();
}

void CDlgMarkProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgMarkProperties)
	DDX_Control(pDX, IDC_MARKPROPS_TAB, m_markPropsTab);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgMarkProperties, CDialog)
	//{{AFX_MSG_MAP(CDlgMarkProperties)
	ON_NOTIFY(TCN_SELCHANGE, IDC_MARKPROPS_TAB, OnSelchangeMarkpropsTab)
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgMarkProperties 

BOOL CDlgMarkProperties::OnInitDialog() 
{
	CDialog::OnInitDialog();

	InitPDFMarkDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgMarkProperties::InitPDFMarkDialog()
{
	m_markPropsTab.DeleteAllItems();

	CString strGeneral("Allgemein");
	CString strPositioning("Positionierung");

	TC_ITEM tcItem;
	tcItem.mask			= TCIF_TEXT;
	tcItem.cchTextMax	= 0;
	tcItem.iImage		= -1;
	tcItem.lParam		= 0;
	tcItem.pszText		= strGeneral.GetBuffer(MAX_TEXT);
	m_markPropsTab.InsertItem(0, &tcItem);

	tcItem.pszText		= strPositioning.GetBuffer(MAX_TEXT);
	m_markPropsTab.InsertItem(1, &tcItem);

	m_markPropsTab.SetCurSel(0);


	m_dlgPDFMarkGeneral.m_pParent = this;
	m_dlgPDFMarkGeneral.Create(IDD_PDFMARK_GENERAL, this);

	CRect dlgRect, tabRect;
	m_dlgPDFMarkGeneral.GetWindowRect(dlgRect);
	m_markPropsTab.GetWindowRect(tabRect);
	TabCtrl_AdjustRect(m_markPropsTab.m_hWnd, FALSE, &tabRect);
	ScreenToClient(tabRect);
	m_dlgPDFMarkGeneral.SetWindowPos(NULL, tabRect.left, tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);


	m_dlgMoveMark.m_objectPivotCtrl.Reinit(IDD_MARK_POSITIONING);
	m_dlgMoveMark.Create(IDD_MARK_POSITIONING, this);

	m_dlgMoveMark.GetWindowRect(dlgRect);
	m_dlgMoveMark.SetWindowPos(NULL, tabRect.left, tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);

	m_dlgMoveMark.ShowWindow(SW_HIDE);
}

void CDlgMarkProperties::OnSelchangeMarkpropsTab(NMHDR* /* pNMHDR */, LRESULT* pResult) 
{
	int nSel = m_markPropsTab.GetCurSel();
	switch (nSel)
	{
	case 0:	m_dlgPDFMarkGeneral.ShowWindow(SW_SHOW);	
			m_dlgMoveMark.ShowWindow	  (SW_HIDE);	
			break;
	case 1: m_dlgPDFMarkGeneral.ShowWindow(SW_HIDE);	
			m_dlgMoveMark.ShowWindow	  (SW_SHOW);	
			break;
	}
	
	*pResult = 0;
}

void CDlgMarkProperties::OnApplyButton() 
{
	if (m_dlgPDFMarkGeneral.IsWindowVisible())
		m_dlgPDFMarkGeneral.Apply();
	if (m_dlgMoveMark.IsWindowVisible())
		m_dlgMoveMark.OnApplyButton();
}

void CDlgMarkProperties::OnOK() 
{
	DestroyWindow();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	pView->m_DisplayList.Invalidate();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CDlgMarkProperties::OnCancel() 
{
	DestroyWindow();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	pView->m_DisplayList.Invalidate();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

