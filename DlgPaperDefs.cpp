// DlgPaperDefs.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgPaperDefs.h"


// CDlgPaperDefs-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgPaperDefs, CDialog)

CDlgPaperDefs::CDlgPaperDefs(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaperDefs::IDD, pParent)
	, m_nPaperGrammage(0)
	, m_fPaperVolume(0)
	, m_fPageShingling(0)
{

}

CDlgPaperDefs::~CDlgPaperDefs()
{
}

void CDlgPaperDefs::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PD_PAPERGRAMMAGE, m_nPaperGrammage);
	DDX_Text(pDX, IDC_PD_PAPERVOLUME, m_fPaperVolume);
	m_paperSelection.DDX_PaperDefsCombo(pDX, IDC_PD_PAPERNAME, m_strPaperName);
	DDX_Text(pDX, IDC_PD_PAPER_SHINGLING, m_fPageShingling);
	DDV_MinMaxFloat(pDX, m_fPageShingling, 0, 10);
}


BEGIN_MESSAGE_MAP(CDlgPaperDefs, CDialog)
	ON_CBN_SELCHANGE(IDC_PD_PAPERNAME, &CDlgPaperDefs::OnCbnSelchangePdPapername)
	ON_BN_CLICKED(IDC_PD_SAVE, &CDlgPaperDefs::OnBnClickedPdSave)
	ON_BN_CLICKED(IDC_PD_DELETE, &CDlgPaperDefs::OnBnClickedPdDelete)
	ON_BN_CLICKED(IDC_PD_CHANGE, &CDlgPaperDefs::OnBnClickedPdChange)
	ON_EN_KILLFOCUS(IDC_PD_PAPERGRAMMAGE, &CDlgPaperDefs::OnEnKillfocusPdPapergrammage)
	ON_EN_KILLFOCUS(IDC_PD_PAPERVOLUME, &CDlgPaperDefs::OnEnKillfocusPdPapervolume)
END_MESSAGE_MAP()


// CDlgPaperDefs-Meldungshandler

BOOL CDlgPaperDefs::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_paperSelection.InitComboBox(this, m_strPaperName, &m_nPaperGrammage, &m_fPaperVolume, &m_fPageShingling, IDC_PD_PAPERNAME);
	OnCbnSelchangePdPapername();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgPaperDefs::OnCbnSelchangePdPapername()
{
	UpdateData(TRUE);
	m_paperSelection.SelchangeFormatList(this, &m_nPaperGrammage, &m_fPaperVolume, &m_fPageShingling, m_strPaperName, IDC_PD_PAPERNAME);
}

void CDlgPaperDefs::OnBnClickedPdSave()
{
	UpdateData(TRUE);
	m_paperSelection.NewEntry(this, &m_nPaperGrammage, &m_fPaperVolume, &m_fPageShingling, m_strPaperName, IDC_PD_PAPERNAME);
}

void CDlgPaperDefs::OnBnClickedPdDelete()
{
	UpdateData(TRUE);
	m_paperSelection.DeleteEntry(this, &m_nPaperGrammage, &m_fPaperVolume, &m_fPageShingling, m_strPaperName, IDC_PD_PAPERNAME);
}

void CDlgPaperDefs::OnBnClickedPdChange()
{
	UpdateData(TRUE);
	m_paperSelection.ChangeEntry(this, &m_nPaperGrammage, &m_fPaperVolume, &m_fPageShingling, m_strPaperName, IDC_PD_PAPERNAME);
}

void CDlgPaperDefs::OnEnKillfocusPdPapergrammage()
{
	UpdateData(TRUE);
	if (m_fPageShingling == 0.0f)
		m_fPageShingling = ((float)m_nPaperGrammage / 1000.0f) * m_fPaperVolume;
	UpdateData(FALSE);
}

void CDlgPaperDefs::OnEnKillfocusPdPapervolume()
{
	UpdateData(TRUE);
	if (m_fPageShingling == 0.0f)
		m_fPageShingling = ((float)m_nPaperGrammage / 1000.0f) * m_fPaperVolume;
	UpdateData(FALSE);
}
