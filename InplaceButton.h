#if !defined(AFX_INPLACEBUTTON_H__A4542514_FD67_11D2_9557_204C4F4F5020__INCLUDED_)
#define AFX_INPLACEBUTTON_H__A4542514_FD67_11D2_9557_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InplaceButton.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Fenster CInplaceButton 



#define DECLARE_INPLACE_BUTTON(CtrlName, CWndName) \
	CInplaceButton m_##CtrlName; \
	void CWndName::OnClicked##CtrlName(); \
	static void FnClickedHandler##CtrlName(CWnd* pWnd) {((CWndName*)pWnd)->OnClicked##CtrlName();}; 


#define CREATE_INPLACE_BUTTON(CtrlName, BitmapID, nTooltipID) \
	m_##CtrlName.Create(_T(""), BitmapID, WS_CHILD, CRect(0,0,0,0), this, 0, nTooltipID); \
	m_##CtrlName.m_pFnClickedHandler = &FnClickedHandler##CtrlName; 



class CInplaceButton : public CButton
{
// Konstruktion
public:
	CInplaceButton();

// Attribute
public:
	LPARAM		 m_lParam;
	CBitmap*	 m_pBitmap;
	CBitmap		 m_bmSurface;
	CDisplayItem m_di;
	CToolTipCtrl m_tooltip;
	int			 m_nTooltipID;


// Operationen
public:
	void Activate(CRect rect, const CString& rString, LPARAM lParam = 0, CDisplayItem di = CDisplayItem());
	void Move(CRect rect, const CString& rString, LPARAM lParam);
	void Deactivate();
	void SaveBackground(const CRect& rect);
	void RestoreBackground(const CRect& rect);

	void (*m_pFnClickedHandler)(CWnd* pWnd);


// Überschreibungen
	BOOL Create(LPCTSTR lpszCaption, int nBitmapID, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, int nTooltipID = -1 );

	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CInplaceButton)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~CInplaceButton();

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CInplaceButton)
	afx_msg void OnClicked();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_INPLACEBUTTON_H__A4542514_FD67_11D2_9557_204C4F4F5020__INCLUDED_
