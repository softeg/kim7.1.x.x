// ExtendedListCtrl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "ExtendedListCtrl.h"
#include "GraphicComponent.h"


// CExtendedListCtrl

IMPLEMENT_DYNAMIC(CExtendedListCtrl, CListCtrl)

CExtendedListCtrl::CExtendedListCtrl()
{
	m_bMouseIsOver = FALSE;

	m_bkBrush.CreateSolidBrush(GUICOLOR_GROUP);

	m_iItem = m_iSubItem = -1;

	for (int i = 0; i < 50; i++)
	{
		m_inplaceControlInfo[i].nInplaceControlType = InplaceNone;
		m_inplaceControlInfo[i].nInplaceControlPos	= 0;
		m_inplaceControlInfo[i].m_pInplaceControl	= NULL;
	}
}

CExtendedListCtrl::~CExtendedListCtrl()
{
	m_bkBrush.DeleteObject();

	for (int i = 0; i < 50; i++)
	{
		if (m_inplaceControlInfo[i].m_pInplaceControl)
		{
			m_inplaceControlInfo[i].m_pInplaceControl->DestroyWindow();
			delete m_inplaceControlInfo[i].m_pInplaceControl;
		}
	}
}

int CExtendedListCtrl::InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat, int nWidth, int nSubItem, int nInplaceControlType, int nInplaceControlPos, int nIDInplaceControl)
{
	m_inplaceControlInfo[nCol].nInplaceControlType	= nInplaceControlType;
	m_inplaceControlInfo[nCol].nInplaceControlPos	= nInplaceControlPos;
	m_inplaceControlInfo[nCol].bLocked				= FALSE;

	switch(m_inplaceControlInfo[nCol].nInplaceControlType)
	{
	case InplaceNone:		m_inplaceControlInfo[nCol].m_pInplaceControl = NULL;
							break;

	case InplaceEdit:		{
								CInplaceListItemControl* pCtrl = new CInplaceListItemControl;
								pCtrl->Create(_T("EDIT"), NULL, WS_CHILD | WS_BORDER, CRect(0,0,0,0), this, nIDInplaceControl);
								pCtrl->SetFont(GetFont());
								m_inplaceControlInfo[nCol].m_pInplaceControl = (CInplaceListItemControl*)pCtrl;
							}
							break;

	case InplaceSpinEdit:		{
								CEdit* pEdit = new CEdit;
								pEdit->Create(WS_CHILD | WS_BORDER, CRect(0,0,0,0), this, nIDInplaceControl - 1);
								pEdit->SetFont(GetFont());
								CSpinButtonCtrl* pSpinCtrl = new CSpinButtonCtrl;
								pSpinCtrl->Create(WS_CHILD | UDS_AUTOBUDDY | UDS_ALIGNRIGHT | UDS_SETBUDDYINT, CRect(0,0,0,0), this, nIDInplaceControl);
								pSpinCtrl->SetBuddy(pEdit);
								pSpinCtrl->SetRange(0, 1);
								m_inplaceControlInfo[nCol].m_pInplaceControl = (CInplaceListItemControl*)pSpinCtrl;
							}
							break;

	case InplaceComboBox:	{
								CInplaceListItemControl* pCtrl = new CInplaceListItemControl;
								pCtrl->Create(_T("COMBOBOX"), NULL, WS_CHILD | WS_VSCROLL | CBS_DROPDOWNLIST, CRect(0,0,0,0), this, nIDInplaceControl);
								pCtrl->SetFont(GetFont());
								m_inplaceControlInfo[nCol].m_pInplaceControl = (CInplaceListItemControl*)pCtrl;
							}
							break;

	case InplaceDropDown:	{
								CInplaceListItemDropDownButton* pCtrl = new CInplaceListItemDropDownButton;
								pCtrl->Create(_T(""), WS_CHILD | WS_BORDER | BS_PUSHBUTTON | BS_OWNERDRAW, CRect(0,0,0,0), this, nIDInplaceControl);
								pCtrl->SetFont(GetFont());
								m_inplaceControlInfo[nCol].m_pInplaceControl = (CInplaceListItemControl*)pCtrl;
							}
							break;

	//case InplaceExternal:	m_inplaceControlInfo[m_iSubItem].m_pInplaceControl->MoveWindow(rcCtrl);
	//						break;

	default:				{
								CInplaceListItemControl* pCtrl = new CInplaceListItemControl;
								pCtrl->Create(_T("STATIC"), NULL, WS_CHILD, CRect(0,0,0,0), this, nIDInplaceControl);
								pCtrl->SetFont(GetFont());
								m_inplaceControlInfo[nCol].m_pInplaceControl = (CInplaceListItemControl*)pCtrl;
							}
							break;
	}

	return CListCtrl::InsertColumn(nCol, lpszColumnHeading, nFormat, nWidth, nSubItem);
}

//int CExtendedListCtrl::InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat, int nWidth, int nSubItem, CWnd* pInplaceControl, int nInplaceControlPos)
//{
//	m_inplaceControlInfo[nCol].nInplaceControlType	= (pInplaceControl) ? InplaceExternal : InplaceNone;
//	m_inplaceControlInfo[nCol].m_pInplaceControl	= (CInplaceListItemControl*)pInplaceControl;
//	m_inplaceControlInfo[nCol].nInplaceControlPos	= nInplaceControlPos;
//
//	return CListCtrl::InsertColumn(nCol, lpszColumnHeading, nFormat, nWidth, nSubItem);
//}



BEGIN_MESSAGE_MAP(CExtendedListCtrl, CListCtrl)
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()



BOOL CExtendedListCtrl::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if ((pMsg->wParam == VK_ESCAPE) || (pMsg->wParam == VK_UP) || (pMsg->wParam == VK_DOWN) || (pMsg->wParam == VK_TAB) || (pMsg->wParam == VK_RETURN) )
		{
			return CListCtrl::PreTranslateMessage(pMsg);
		}
	}

	if (pMsg->message == WM_MOUSEMOVE)
	{
		CPoint ptMouse;
		GetCursorPos(&ptMouse);
		ScreenToClient(&ptMouse);
		OnMouseMove(0, ptMouse);
//		return TRUE;
	}

	if (pMsg->message == WM_MOUSELEAVE)
	{
		OnMouseLeave(0, 0);
//		return TRUE;
	}

	if ( (pMsg->message == WM_LBUTTONDOWN) || (pMsg->message == WM_LBUTTONDBLCLK) )
	{
		CPoint ptMouse;
		GetCursorPos(&ptMouse);
		ScreenToClient(&ptMouse);
		LVHITTESTINFO lvhti;
		lvhti.pt = ptMouse;
		SubItemHitTest(&lvhti);
		if ( (lvhti.iItem < 0) || (lvhti.iSubItem < 0) )  //&& (lvhti.flags & LVHT_ONITEM ) )		
		{
			CloseInplaceControl();
			return TRUE;	// prevent from current selection being disabled -> make sure always one item selected
		}
		m_iItem = lvhti.iItem; m_iSubItem = lvhti.iSubItem;
	}

	return CListCtrl::PreTranslateMessage(pMsg);
}

void CExtendedListCtrl::OnMouseMove(UINT nFlags, CPoint point)
{
	if ( ! m_bMouseIsOver)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = this->m_hWnd;
		if (::_TrackMouseEvent(&tme))
			m_bMouseIsOver = TRUE;
	}

	LVHITTESTINFO lvhti;
	lvhti.pt = point;
	SubItemHitTest(&lvhti);

	POSITION pos = GetFirstSelectedItemPosition();
	int nSel = (pos) ? GetNextSelectedItem(pos) : -1;

	if ( (lvhti.iItem >= 0) && (lvhti.iItem == nSel) )
	{
		if ( ! InplaceControlIsVisible())
			OpenInplaceControl();
	}
	else
		CloseInplaceControl();

	//m_iItem = lvhti.iItem; m_iSubItem = lvhti.iSubItem;

	CListCtrl::OnMouseMove(nFlags, point);
}

LRESULT CExtendedListCtrl::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	if ( ! MouseOverInplaceControl())
	{
		CloseInplaceControl();
		m_bMouseIsOver = FALSE;
	}
	return TRUE;
}

BOOL CExtendedListCtrl::MouseOverInplaceControl()
{
	CPoint ptMouse;
	GetCursorPos(&ptMouse);

	for (int i = 0; i < GetHeaderCtrl()->GetItemCount(); i++)
	{
		if (m_inplaceControlInfo[i].m_pInplaceControl)
			if (m_inplaceControlInfo[i].m_pInplaceControl->m_hWnd)
			{
				CRect rcWin;
				m_inplaceControlInfo[i].m_pInplaceControl->GetWindowRect(rcWin);
				if (rcWin.PtInRect(ptMouse))
					return TRUE;
				if (m_inplaceControlInfo[i].nInplaceControlType == InplaceSpinEdit)
				{
					CWnd* pEdit = ((CSpinButtonCtrl*)m_inplaceControlInfo[i].m_pInplaceControl)->GetBuddy();
					pEdit->GetWindowRect(rcWin);
					if (rcWin.PtInRect(ptMouse))
						return TRUE;
				}
			}
	}

	return FALSE;
}

void CExtendedListCtrl::LockInplaceControl()
{
	for (int i = 0; i < 50; i++)
		m_inplaceControlInfo[i].bLocked = TRUE;	
}

void CExtendedListCtrl::ReleaseInplaceControl()
{
	for (int i = 0; i < 50; i++)
		m_inplaceControlInfo[i].bLocked = FALSE;	
}

CWnd* CExtendedListCtrl::GetInplaceControl(int nID)
{
	for (int i = 0; i < GetHeaderCtrl()->GetItemCount(); i++)
		if (m_inplaceControlInfo[i].m_pInplaceControl)
			if (m_inplaceControlInfo[i].m_pInplaceControl->GetDlgCtrlID() == nID)
				return m_inplaceControlInfo[i].m_pInplaceControl;

	return NULL;
}

BOOL CExtendedListCtrl::InplaceControlIsVisible()
{
	for (int i = 0; i < GetHeaderCtrl()->GetItemCount(); i++)
		if (m_inplaceControlInfo[i].m_pInplaceControl)
			if (m_inplaceControlInfo[i].m_pInplaceControl->m_hWnd)
				return m_inplaceControlInfo[i].m_pInplaceControl->IsWindowVisible();

	return FALSE;
}

void CExtendedListCtrl::OpenInplaceControl()
{
	//CloseInplaceControl();

	POSITION pos = GetFirstSelectedItemPosition();
	int iItem = (pos) ? GetNextSelectedItem(pos) : -1;
	if (iItem < 0)
		return;

	for (int i = 0; i < GetHeaderCtrl()->GetItemCount(); i++)
	{
		if ( ! m_inplaceControlInfo[i].m_pInplaceControl)
			continue;
		if (m_inplaceControlInfo[i].nInplaceControlType == InplaceNone)
			continue;
		if (m_inplaceControlInfo[i].bLocked)
			continue;

		InitInplaceControl(iItem, i, m_inplaceControlInfo[i].nInplaceControlType, m_inplaceControlInfo[i].m_pInplaceControl);

		CRect rcItem;
		GetSubItemRect(iItem, i, LVIR_LABEL, rcItem);

		switch (m_inplaceControlInfo[i].nInplaceControlType)
		{
		case InplaceSpinEdit:
			{
				CSpinButtonCtrl* pSpin = (CSpinButtonCtrl*)m_inplaceControlInfo[i].m_pInplaceControl;
				CEdit*			 pEdit = (CEdit*)pSpin->GetBuddy();
				CRect rcEdit  = rcItem;
				rcEdit.bottom = rcEdit.top + 20;
				//rcEdit.top	  = rcEdit.CenterPoint().y - 6;
				//rcEdit.bottom = rcEdit.CenterPoint().y + 6;
				rcEdit.left  += m_inplaceControlInfo[i].nInplaceControlPos;
				rcEdit.right -= 20;
				pEdit->MoveWindow(rcEdit);
				pSpin->SetWindowPos(pEdit, rcEdit.right, rcEdit.top, 15, rcEdit.Height(), SWP_SHOWWINDOW);
				pEdit->ShowWindow(SW_SHOW);
				pSpin->ShowWindow(SW_SHOW);
			}
			break;

		default:
			{
				CRect rcCtrl  = rcItem;
				rcCtrl.bottom = rcCtrl.top + 20;
				//rcCtrl.top	  = rcCtrl.CenterPoint().y - 6;
				//rcCtrl.bottom = rcCtrl.CenterPoint().y + 6;
				rcCtrl.right -= 5;
				rcCtrl.left  += m_inplaceControlInfo[i].nInplaceControlPos;

				m_inplaceControlInfo[i].m_pInplaceControl->MoveWindow(rcCtrl);
				m_inplaceControlInfo[i].m_pInplaceControl->ShowWindow(SW_SHOW);
			}
			break;
		}
	}
}

void CExtendedListCtrl::CloseInplaceControl()
{
	for (int i = 0; i < GetHeaderCtrl()->GetItemCount(); i++)
	{
		if ( ! m_inplaceControlInfo[i].m_pInplaceControl)
			continue;
		if ( ! m_inplaceControlInfo[i].m_pInplaceControl->m_hWnd)
			continue;
		if (m_inplaceControlInfo[i].bLocked)
			continue;

		CRect rcCtrl;
		switch (m_inplaceControlInfo[i].nInplaceControlType)
		{
		case InplaceSpinEdit:
			{
				CSpinButtonCtrl* pSpin = (CSpinButtonCtrl*)m_inplaceControlInfo[i].m_pInplaceControl;
				CEdit*			 pEdit = (CEdit*)pSpin->GetBuddy();
				pSpin->ShowWindow(SW_HIDE);
				pEdit->ShowWindow(SW_HIDE);
				pEdit->GetWindowRect(rcCtrl);
				InvalidateRect(rcCtrl);
			}
			break;

		default:
			m_inplaceControlInfo[i].m_pInplaceControl->ShowWindow(SW_HIDE);
			m_inplaceControlInfo[i].m_pInplaceControl->GetWindowRect(rcCtrl);
			InvalidateRect(rcCtrl);
			break;
		}
	}
}


// CInplaceListItemControl

IMPLEMENT_DYNAMIC(CInplaceListItemControl, CWnd)

CInplaceListItemControl::CInplaceListItemControl()
{
}

CInplaceListItemControl::~CInplaceListItemControl()
{
}


BEGIN_MESSAGE_MAP(CInplaceListItemControl, CWnd)
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
END_MESSAGE_MAP()

BOOL CInplaceListItemControl::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if ((pMsg->wParam == VK_ESCAPE) || (pMsg->wParam == VK_UP) || (pMsg->wParam == VK_DOWN) || (pMsg->wParam == VK_TAB) || (pMsg->wParam == VK_RETURN) )
		{
			//if (pMsg->wParam == VK_ESCAPE)
			//	Undo();

			//NMKEY nmkey;
			//nmkey.hdr.hwndFrom = m_hWnd;
			//nmkey.hdr.idFrom = GetDlgCtrlID();
			//nmkey.hdr.code = LISTINPLACECTRL_NM_VK;
			//nmkey.nVKey = pMsg->wParam;
			//nmkey.uFlags = 0;
			//GetParent()->SendMessage( WM_NOTIFY, GetDlgCtrlID(), (LPARAM)&nmkey);
			//
			//return TRUE;
		}

	//if (pMsg->message == WM_LBUTTONDOWN)
	//{
	//	CExtendedListCtrl* pListCtrl = (CExtendedListCtrl*)GetParent();
	//	pListCtrl->SetItemState(pListCtrl->m_iItem, LVIS_SELECTED, LVIS_SELECTED);
	//}

	return CWnd::PreTranslateMessage(pMsg);
}

void CInplaceListItemControl::OnSetFocus(CWnd* pOldWnd)
{
	CWnd::OnSetFocus(pOldWnd);
}

void CInplaceListItemControl::OnKillFocus(CWnd* pNewWnd)
{
	CWnd::OnKillFocus(pNewWnd);

	//CExtendedListCtrl* pListCtrl = (CExtendedListCtrl*)GetParent();
	//pListCtrl->CloseInplaceControl();
}





// CInplaceListItemComboBox

IMPLEMENT_DYNAMIC(CInplaceListItemComboBox, CComboBox)

CInplaceListItemComboBox::CInplaceListItemComboBox()
{
	m_bkBrush.CreateSolidBrush(GUICOLOR_GROUP);
}

CInplaceListItemComboBox::~CInplaceListItemComboBox()
{
	m_bkBrush.DeleteObject();
}


BEGIN_MESSAGE_MAP(CInplaceListItemComboBox, CComboBox)
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()


BOOL CInplaceListItemComboBox::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;

	//return CComboBox::OnEraseBkgnd(pDC);
}

HBRUSH CInplaceListItemComboBox::CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/)
{
	// TODO:  �ndern Sie hier alle Attribute f�r den DC.

	// TODO:  Gib einen nicht-Nullpinsel zur�ck, wenn der Handler der �bergeordneten Ebene nicht aufgerufen werden soll.  
	return NULL;
}

BOOL CInplaceListItemComboBox::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_ERASEBKGND)
	{
		return CWnd::PreTranslateMessage(pMsg);
	}

	return CWnd::PreTranslateMessage(pMsg);
}



// CInplaceListItemEdit

IMPLEMENT_DYNAMIC(CInplaceListItemEdit, CEdit)

CInplaceListItemEdit::CInplaceListItemEdit()
{
	m_bkBrush.CreateSolidBrush(GUICOLOR_GROUP);
}

CInplaceListItemEdit::~CInplaceListItemEdit()
{
	m_bkBrush.DeleteObject();
}


BEGIN_MESSAGE_MAP(CInplaceListItemEdit, CEdit)
END_MESSAGE_MAP()



// CInplaceListItemDropDownButton

IMPLEMENT_DYNAMIC(CInplaceListItemDropDownButton, CButton)

CInplaceListItemDropDownButton::CInplaceListItemDropDownButton()
{
	m_bMouseOverButton = FALSE;
}

CInplaceListItemDropDownButton::~CInplaceListItemDropDownButton()
{
}


BEGIN_MESSAGE_MAP(CInplaceListItemDropDownButton, CButton)
	ON_WM_ERASEBKGND()
	ON_WM_KILLFOCUS()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()



// CInplaceListItemDropDownButton-Meldungshandler


BOOL CInplaceListItemDropDownButton::OnEraseBkgnd(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(rcClient);
	pDC->FillSolidRect(rcClient, WHITE);

	return TRUE;

//	return CButton::OnEraseBkgnd(pDC);
}

BOOL CInplaceListItemDropDownButton::PreTranslateMessage(MSG* pMsg) 
{
	//if (pMsg->message == WM_LBUTTONDOWN)
	//{
	//	CExtendedListCtrl* pListCtrl = (CExtendedListCtrl*)GetParent();
	//	pListCtrl->SetItemState(pListCtrl->m_iItem, LVIS_SELECTED, LVIS_SELECTED);
	//}

	return CButton::PreTranslateMessage(pMsg);
}

void CInplaceListItemDropDownButton::OnKillFocus(CWnd* pNewWnd)
{
	CButton::OnKillFocus(pNewWnd);

	CExtendedListCtrl* pListCtrl = (CExtendedListCtrl*)GetParent();
	pListCtrl->CloseInplaceControl();
}

void CInplaceListItemDropDownButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect rcItem = lpDrawItemStruct->rcItem;
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

//	ThemeButton(pDC, rcItem, m_bMouseOverButton, (lpDrawItemStruct->itemState & ODS_SELECTED), GetParent());
	pDC->FillSolidRect(rcItem, WHITE);

	//pDC->SelectObject(GetParent()->GetFont());
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString strText;
	GetWindowText(strText);
	rcItem.left += 8;
	pDC->DrawText(strText, rcItem, DT_LEFT | DT_VCENTER | DT_SINGLELINE);

	ThemeDropDown(pDC, rcItem, m_bMouseOverButton, (lpDrawItemStruct->itemState & ODS_SELECTED), GetParent());
}

void CInplaceListItemDropDownButton::ThemeButton(CDC* pDC, CRect rcItem, BOOL bMouseOverButton, BOOL bSelected, CWnd* pParentWnd)
{
#ifdef UNICODE
	HTHEME hThemeButton = theApp._OpenThemeData(pParentWnd, _T("BUTTON"));
#else
	HTHEME hThemeButton = theApp._OpenThemeData(pParentWnd, CA2W("BUTTON"));
#endif
	if (hThemeButton)
	{
		if (bSelected)
			DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), BP_PUSHBUTTON, PBS_PRESSED,	&rcItem, 0);	
		else
		{
			CPoint ptMouse;
			GetCursorPos(&ptMouse);
			CRect rcButton = rcItem;
			pParentWnd->ClientToScreen(rcButton);
			if (bMouseOverButton)
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), BP_PUSHBUTTON, PBS_HOT,		&rcItem, 0);	
			else
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), BP_PUSHBUTTON, PBS_NORMAL,	&rcItem, 0);	
		}
	}
	else
	{
		if (bSelected)
			DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);	
		else
			DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_TRANSPARENT);	
		//
		//CImage img;
		//img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_DROPDOWN));
		//img.TransparentBlt(pDC->m_hDC, rcItem.left + 4, rcItem.top + 8, img.GetWidth(), img.GetHeight(), WHITE);
	}

	theApp._CloseThemeData(hThemeButton);
}

void CInplaceListItemDropDownButton::ThemeDropDown(CDC* pDC, CRect rcItem, BOOL bMouseOverButton, BOOL bSelected, CWnd* pParentWnd)
{
	rcItem.left = rcItem.right - 16; //rcItem.right -= 1;
//	rcItem.DeflateRect(0, 2);

#ifdef UNICODE
	HTHEME hThemeButton = theApp._OpenThemeData(pParentWnd, _T("COMBOBOX"));
#else
	HTHEME hThemeButton = theApp._OpenThemeData(pParentWnd, CA2W("COMBOBOX"));
#endif

	if (hThemeButton)
	{
		if (bSelected)
			DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), CP_DROPDOWNBUTTON, CBXS_PRESSED,		&rcItem, 0);	
		else
		{
			CPoint ptMouse;
			GetCursorPos(&ptMouse);
			CRect rcButton = rcItem;
			pParentWnd->ClientToScreen(rcButton);
			if (bMouseOverButton)
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), CP_DROPDOWNBUTTON, CBXS_HOT,		&rcItem, 0);	
			else
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), CP_DROPDOWNBUTTON, CBXS_NORMAL,	&rcItem, 0);	
		}
	}
	else
	{
		//if (lpDrawItemStruct->itemState & ODS_SELECTED)
		//	DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);	
		//else
		//	DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_TRANSPARENT);	
		//
		CImage img;
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_DROPDOWN));
		img.TransparentBlt(pDC->m_hDC, rcItem.left, rcItem.top + 6, img.GetWidth(), img.GetHeight(), WHITE);
	}

	theApp._CloseThemeData(hThemeButton);
}

void CInplaceListItemDropDownButton::OnMouseMove(UINT nFlags, CPoint point)
{
	if ( ! m_bMouseOverButton)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(TRACKMOUSEEVENT);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = this->m_hWnd;
		if (::_TrackMouseEvent(&tme))
			m_bMouseOverButton = TRUE;
	}

	CButton::OnMouseMove(nFlags, point);
}

LRESULT CInplaceListItemDropDownButton::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	m_bMouseOverButton = FALSE;
	UpdateWindow();
	return TRUE;
}
