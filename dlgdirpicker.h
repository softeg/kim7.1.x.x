// DlgDirPicker.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgDirPicker dialog

class CDlgDirPicker : public CFileDialog
{
	DECLARE_DYNAMIC(CDlgDirPicker)

public:
	CDlgDirPicker(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL);

public:
	CString m_strPath;
	CString m_strDirToAdd;

protected:
	//{{AFX_MSG(CDlgDirPicker)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	virtual void OnFolderChange();
	DECLARE_MESSAGE_MAP()
};
