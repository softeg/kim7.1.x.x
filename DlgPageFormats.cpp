// DlgPageFormats.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgPageFormats.h"


// CDlgPageFormats-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgPageFormats, CDialog)

CDlgPageFormats::CDlgPageFormats(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPageFormats::IDD, pParent) 
{
	m_nPageOrientation = 0;
	m_fHeight = 0.0f;
	m_fWidth = 0.0f;
}

CDlgPageFormats::~CDlgPageFormats()
{
}

void CDlgPageFormats::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgProductDetails)
	DDX_Control(pDX, IDC_PF_WIDTH_SPIN, m_widthSpin);
	DDX_Control(pDX, IDC_PF_HEIGHT_SPIN, m_heightSpin);
	DDX_Radio(pDX, IDC_PF_PORTRAIT, m_nPageOrientation);
	DDX_Measure(pDX, IDC_PF_HEIGHT, m_fHeight);
	DDX_Measure(pDX, IDC_PF_WIDTH, m_fWidth);
	//}}AFX_DATA_MAP
	m_formatSelection.DDX_PageFormatCombo(pDX, IDC_PF_FORMAT, m_strFormatName);
}


BEGIN_MESSAGE_MAP(CDlgPageFormats, CDialog)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PF_HEIGHT_SPIN, OnDeltaposHeightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PF_WIDTH_SPIN, OnDeltaposWidthSpin)
	ON_BN_CLICKED(IDC_PF_PORTRAIT, OnPortrait)
	ON_BN_CLICKED(IDC_PF_LANDSCAPE, OnLandscape)
	ON_CBN_SELCHANGE(IDC_PF_FORMAT, OnSelchangeFormat)
	ON_BN_CLICKED(IDC_PF_SAVE, &CDlgPageFormats::OnBnClickedPfSave)
	ON_BN_CLICKED(IDC_PF_DELETE, &CDlgPageFormats::OnBnClickedPfDelete)
END_MESSAGE_MAP()


// CDlgPageFormats-Meldungshandler

BOOL CDlgPageFormats::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_formatSelection.InitComboBox(this, m_strFormatName, &m_fWidth, &m_fHeight, IDC_PF_FORMAT);
	UpdateData(FALSE);

	OnSelchangeFormat();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgPageFormats::OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_heightSpin.GetBuddy(), pNMUpDown->iDelta);
	
	*pResult = 0;
}

void CDlgPageFormats::OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_widthSpin.GetBuddy(), pNMUpDown->iDelta);
	
	*pResult = 0;
}

void CDlgPageFormats::OnPortrait() 
{
	if (m_nPageOrientation == PORTRAIT)	// already portrait
		return;
	m_nPageOrientation = PORTRAIT;		// switch to portrait
	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_PF_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_PF_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);
}

void CDlgPageFormats::OnLandscape() 
{
	if (m_nPageOrientation == LANDSCAPE)	// already portrait
		return;
	m_nPageOrientation = LANDSCAPE;		// switch to portrait
	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_PF_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_PF_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);
}

void CDlgPageFormats::OnSelchangeFormat() 
{
	UpdateData(TRUE);
	m_formatSelection.SelchangeFormatList(this, &m_fWidth, &m_fHeight, m_strFormatName, &m_nPageOrientation, IDC_PF_FORMAT);
}


void CDlgPageFormats::OnBnClickedPfSave()
{
	UpdateData(TRUE);
	m_formatSelection.NewEntry(this, &m_fWidth, &m_fHeight, m_strFormatName, m_nPageOrientation, IDC_PF_FORMAT);
}

void CDlgPageFormats::OnBnClickedPfDelete()
{
	UpdateData(TRUE);
	m_formatSelection.DeleteEntry(this, &m_fWidth, &m_fHeight, m_strFormatName, m_nPageOrientation, IDC_PF_FORMAT);
}
