// PrintSheetFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "MainFrm.h"
#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "PrintSheetFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "DlgSheet.h"
#include "DlgChangeFormats.h" 
#include "DlgJobColorDefinitions.h"
#include "DlgPressDevice.h"
#include "DlgNotes.h"
#include "DlgEditTempString.h"
#include "PrintSheetTableView.h"
#include "PrintSheetMarksView.h"
#include "BookBlockView.h"
#include "DlgDeleteMark.h"
#include "PrintSheetPlanningView.h"
#include "PrintComponentsView.h"
#include "PrintSheetComponentsView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE 
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetFrame

IMPLEMENT_DYNCREATE(CPrintSheetFrame, CMDIChildWnd)

CPrintSheetFrame::CPrintSheetFrame()
{
	m_nViewMode				  = ID_VIEW_ALL_SHEETS;

	m_bUndoActive			  = FALSE;
//	Toolbars
	m_bMainToolBarActive	  = TRUE;
	m_bTrimToolActive		  = FALSE;
	//
	m_bFoldSheetTurnActive	  = FALSE;
	m_bFoldSheetTumbleActive  = FALSE;
	m_bFoldSheetRotateActive  = FALSE;
//	Main-Toolbar objects-actions
	m_bShowFoldSheet		  = FALSE;
	m_bShowPlate			  = FALSE;
	m_bShowMeasure			  = FALSE;
	m_bShowExposures		  = FALSE;
	m_bShowBitmap			  = FALSE;
	m_bShowFrontback		  = FALSE;
	m_bShowBoxShapes		  = TRUE;
	m_bShowOutputPlates		  = FALSE;

	m_bReleaseBleed			  = FALSE;
	m_bShowPlateTable		  = FALSE;
	m_bShowPrintSheetTable	  = TRUE;
	m_bMaxPrintSheetTable	  = FALSE;

	m_bOverbleedChanged		  = FALSE;
//	Main-Toolbar objects-state
	m_bEnableFoldSheetButton	= TRUE;
	m_bEnablePlateButton		= TRUE;
	m_bEnableMeasureButton		= TRUE;
	m_bEnableExposuresButton	= TRUE;
	m_bEnableBitmapButton		= TRUE;
	m_bEnableOutputPlatesButton	= TRUE;

	m_bEnableReleaseBleedButton	= FALSE;
	m_bEnablePlateTableButton	= FALSE;

	m_bMarksViewActive			= FALSE;

//	Menu entries
	m_bTrimToolMenuActive	  = TRUE;
	m_bPressDeviceMenuActive  = FALSE;
	m_bAlignSectionMenuActive = FALSE;
	m_bAlignSectionActive	  = FALSE;
	m_bTurnTumbleMenuActive	  = FALSE;
	m_bShowPrintMenu		  = TRUE;
	m_bShowCenterMenu		  = TRUE;
//	Displaylist
	m_bLayoutObjectSelected	  = FALSE;
	m_bFoldSheetSelected	  = FALSE;
//	Dialogs nomodal
	m_pDlgOutputMedia			 = new CDlgPDFOutputMediaSettings;
	m_pDlgMoveObjects			 = new CDlgMoveObjects;
	m_pDlgLayoutObjectProperties = new CDlgLayoutObjectProperties;
	m_pDlgMarkSet				 = new CDlgMarkSet;
// CTP-Version
#ifdef CTP
	m_pDlgTableCtp		 = new CDlgTableCtp;
#else
	m_pDlgTableCtp		 = NULL;
#endif
	m_pDlgMarks			 = NULL;

	m_bShowTableCtp		 = FALSE;

	m_bHandActive		 = FALSE;

//	initialize some members
	m_fOverbleed		= 0.0;
	m_bSplittersCreated	= FALSE;
	m_bStatusRestored	= FALSE;
}

CPrintSheetFrame::~CPrintSheetFrame()
{
	delete m_pDlgOutputMedia;
	delete m_pDlgMoveObjects;
	delete m_pDlgLayoutObjectProperties;
	delete m_pDlgMarkSet;
}


BEGIN_MESSAGE_MAP(CPrintSheetFrame, CMDIChildWnd)
	ON_WM_INITMENUPOPUP()
	ON_WM_MENUSELECT()
	ON_WM_CREATE()
	//{{AFX_MSG_MAP(CPrintSheetFrame)
	ON_COMMAND(ID_DEFINE_SHEET, OnDefineSheet)
	ON_COMMAND(ID_DEFINE_TRIM, OnDefineTrim)
	ON_COMMAND(ID_TRIM1, OnTrim1)
	ON_UPDATE_COMMAND_UI(ID_TRIM1, OnUpdateTrim1)
	ON_COMMAND(ID_TRIM2, OnTrim2)
	ON_UPDATE_COMMAND_UI(ID_TRIM2, OnUpdateTrim2)
	ON_COMMAND(ID_TRIM3, OnTrim3)
	ON_UPDATE_COMMAND_UI(ID_TRIM3, OnUpdateTrim3)
	ON_COMMAND(ID_TRIM4, OnTrim4)
	ON_UPDATE_COMMAND_UI(ID_TRIM4, OnUpdateTrim4)
	ON_COMMAND(ID_TRIM5, OnTrim5)
	ON_UPDATE_COMMAND_UI(ID_TRIM5, OnUpdateTrim5)
	ON_COMMAND(ID_TRIM6, OnTrim6)
	ON_UPDATE_COMMAND_UI(ID_TRIM6, OnUpdateTrim6)
	ON_COMMAND(ID_TRIM7, OnTrim7)
	ON_UPDATE_COMMAND_UI(ID_TRIM7, OnUpdateTrim7)
	ON_COMMAND(ID_TRIM8, OnTrim8)
	ON_UPDATE_COMMAND_UI(ID_TRIM8, OnUpdateTrim8)
	ON_COMMAND(ID_DEFINE_MARK, OnDefineMark)
	ON_COMMAND(ID_SETTINGS_PRESSDEVICE, OnSettingsPressDevice)
	ON_WM_MDIACTIVATE()
	ON_COMMAND(ID_CHANGE_FORMATS, OnChangeFormats)
	ON_COMMAND(ID_ADD_EXPOSURE_PRINTSHEET, OnAddExposurePrintsheet)
	ON_UPDATE_COMMAND_UI(ID_DEFINE_TRIM, OnUpdateDefineTrim)
	ON_COMMAND(ID_NOTES, OnNotes)
	ON_UPDATE_COMMAND_UI(ID_CHANGE_FORMATS, OnUpdateChangeFormats)
	ON_UPDATE_COMMAND_UI(ID_SETTINGS_PRESSDEVICE, OnUpdateSettingsPressDevice)
	ON_COMMAND(ID_PAGELIST_VIEW, OnPagelistView)
	ON_WM_CREATE()
	ON_COMMAND(ID_TURN_TUMBLE, OnTurnTumblePrintSheet)
	ON_COMMAND(IDC_FOLDSHEETCHECK, OnFoldsheetcheck)
	ON_UPDATE_COMMAND_UI(IDC_FOLDSHEETCHECK, OnUpdateFoldsheetcheck)
	ON_UPDATE_COMMAND_UI(IDC_PLATECHECK, OnUpdatePlatecheck)
	ON_COMMAND(IDC_MEASURECHECK, OnMeasurecheck)
	ON_UPDATE_COMMAND_UI(IDC_MEASURECHECK, OnUpdateMeasurecheck)
	ON_UPDATE_COMMAND_UI(IDC_EXPOSURECHECK, OnUpdateExposuresCheck)
	ON_COMMAND(ID_DELETE_SELECTION, OnDeleteSelection)
	ON_COMMAND(ID_MOVE_OBJECT, OnMoveObjects)
	ON_UPDATE_COMMAND_UI(IDC_BITMAPCHECK, OnUpdateBitmapcheck)
	ON_UPDATE_COMMAND_UI(IDC_PRINTPLATES, OnUpdateOutputPlates)
	ON_UPDATE_COMMAND_UI(ID_MOVE_OBJECT, OnUpdateMoveObject)
	ON_UPDATE_COMMAND_UI(ID_DELETE_SELECTION, OnUpdateDeleteSelection)
	ON_UPDATE_COMMAND_UI(ID_TURN_TUMBLE, OnUpdateTurnTumble)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdateFilePrintPreview)
	ON_COMMAND(IDC_PLATETABLECHECK, OnPlatetablecheck)
	ON_UPDATE_COMMAND_UI(IDC_PLATETABLECHECK, OnUpdatePlatetablecheck)
	ON_COMMAND(IDC_RELEASE_BLEED, OnReleaseBleed)
	ON_UPDATE_COMMAND_UI(IDC_RELEASE_BLEED, OnUpdateReleaseBleed)
	ON_COMMAND(ID_VIEW_PRINTSHEETFRAME_BAR, OnViewPrintsheetframeBar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PRINTSHEETFRAME_BAR, OnUpdateViewPrintsheetframeBar)
	ON_COMMAND(IDC_CTP_TABLE, OnCtpTable)
	ON_UPDATE_COMMAND_UI(IDC_CTP_TABLE, OnUpdateCtpTable)
	ON_COMMAND(ID_WORK_ON_MARK, OnWorkOnMark)
	ON_COMMAND(ID_SET_MARK, OnSetMark)
	ON_UPDATE_COMMAND_UI(ID_SET_MARK, OnUpdateSetMark)
	ON_UPDATE_COMMAND_UI(ID_WORK_ON_MARK, OnUpdateWorkOnMark)
	ON_UPDATE_COMMAND_UI(ID_DEFINE_MARK, OnUpdateDefineMark)
	ON_COMMAND(ID_ALIGN_SECTION, OnAlignSection)
	ON_UPDATE_COMMAND_UI(ID_ALIGN_SECTION, OnUpdateAlignSection)
	ON_COMMAND(ID_RESET_SECTION, OnResetSection)
	ON_UPDATE_COMMAND_UI(ID_RESET_SECTION, OnUpdateResetSection)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ALL_SHEETS, OnUpdateViewAllSheets)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SINGLE_SHEETS, OnUpdateViewSingleSheets)
	ON_COMMAND(ID_ZOOM, OnZoom)
	ON_UPDATE_COMMAND_UI(ID_ZOOM, OnUpdateZoom)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PRINTSHEETTABLE, OnUpdateViewPrintsheettable)
	ON_COMMAND(ID_ZOOM_DECREASE, OnZoomDecrease)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_DECREASE, OnUpdateZoomDecrease)
	ON_COMMAND(ID_ZOOM_FULLVIEW, OnZoomFullview)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_FULLVIEW, OnUpdateZoomFullview)
	ON_COMMAND(ID_FILE_PAGE_SETUP, OnFilePageSetup)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_WM_CLOSE()
	ON_COMMAND(ID_DELETE_MARKS, OnDeleteMarks)
	ON_UPDATE_COMMAND_UI(ID_PDF_OUTPUT, OnUpdatePdfOutput)
	ON_COMMAND(ID_VIEW_PRINTSHEETTABLE, OnViewPrintsheettable)
	ON_COMMAND(ID_BMP_OUTPUT, OnBmpOutput)
	ON_UPDATE_COMMAND_UI(ID_BMP_OUTPUT, OnUpdateBmpOutput)
	ON_WM_DESTROY()
	ON_COMMAND(ID_SCROLLHAND, OnScrollhand)
	ON_UPDATE_COMMAND_UI(ID_SCROLLHAND, OnUpdateScrollhand)
	ON_COMMAND(IDC_SHOW_FRONTBACK, OnShowFrontback)
	ON_UPDATE_COMMAND_UI(IDC_SHOW_FRONTBACK, OnUpdateShowFrontback)
	ON_COMMAND(ID_UNDO, OnUndo)
	ON_UPDATE_COMMAND_UI(ID_UNDO, OnUpdateUndo)
	ON_WM_TIMER()
	ON_COMMAND(ID_PDF_OUTPUT, OnPdfOutput)
	ON_COMMAND(IDC_BITMAPCHECK, OnBitmapcheck)
	ON_WM_SIZE()
	ON_COMMAND(ID_VIEW_ALL_SHEETS, OnViewAllSheets)
	ON_COMMAND(ID_VIEW_SINGLE_SHEETS, OnViewSingleSheets)
	ON_COMMAND(IDC_EXPOSURECHECK, OnExposuresCheck)
	ON_COMMAND(IDC_PLATECHECK, OnPlatecheck)
	ON_COMMAND(IDC_PRINTPLATES, OnOutputPlates)
	ON_COMMAND(ID_OBJECT_PROPERTIES, OnObjectProperties)
	ON_COMMAND(ID_SHOW_BOXSHAPES, OnShowBoxshapes)
	ON_UPDATE_COMMAND_UI(ID_SHOW_BOXSHAPES, OnUpdateShowBoxshapes)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CPrintSheetFrame message handlers

int CPrintSheetFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	//lpCreateStruct->dwExStyle |= WS_EX_TOPMOST;
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
#ifdef CIPCUT
	MDIMaximize();
#endif
	return 0;
}


BOOL CPrintSheetFrame::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CMDIFrameWnd* pParentWnd, CCreateContext* pContext) 
{
	BOOL ret = CMDIChildWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, pContext);
	if (ret == 0)
		return FALSE;
	else
	{
		if ( (theApp.m_nGUIStyle == CImpManApp::GUIStandard) || 
			 (theApp.m_nGUIStyle == CImpManApp::GUIOpenTemplate) ||
			 (theApp.m_nGUIStyle == CImpManApp::GUIOpenOutputList) )
		{
			CreateMainToolBar();
			CreateTrimToolBar();
			CreateFoldToolBar();
			CreateFlatWorkToolBar();
		}

		return TRUE;
	}
}


BOOL CPrintSheetFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* pContext)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		m_bShowFrontback = pDoc->m_screenLayout.m_bLabelsShowFrontBack;
		m_bShowBoxShapes = pDoc->m_screenLayout.m_bLabelsShowBoxShapes;
	}

	MoveWindow(CRect(40,40,1040,640), FALSE);	// TODO Get information about screen to make window resolution-independent

	// create a splitter with 2 rows, 1 column
	if (!m_wndSplitter.CreateStatic(this, 2, 1))
	{
		TRACE0("Failed to CreateStaticSplitter\n");
		return FALSE;
	}

	// add the first splitter pane - the default view in column 0
	if (!m_wndSplitter.CreateView(1, 0,
		RUNTIME_CLASS(CPrintSheetTableView), CSize(800, 300), pContext))
	{
		TRACE0("Failed to create first pane\n");
		return FALSE;
	}

	// add the second splitter pane - which is a nested splitter with 2 columns
	if (!m_wndSplitter2.CreateStatic(
		&m_wndSplitter,     // our parent window is the first splitter
		1, 2,               // the new splitter is 1 row, 2 columns
		WS_CHILD | WS_VISIBLE | WS_BORDER,  // style, WS_BORDER is needed
		m_wndSplitter.IdFromRowCol(0, 0)
			// new splitter is in the first row, 2nd column of first splitter
	   ))
	{
		TRACE0("Failed to create nested splitter\n");
		return FALSE;
	}

	// now create the two views inside the nested splitter
	if (!m_wndSplitter2.CreateView(0, 1,
		pContext->m_pNewViewClass, CSize(800, 300), pContext))
	{
		TRACE0("Failed to create second pane\n");
		return FALSE;
	}
	if (!m_wndSplitter2.CreateView(0, 0,
		RUNTIME_CLASS(CPrintSheetTreeView), CSize((theApp.m_nGUIStyle == CImpManApp::GUIAutoImpose) ? 0 : 150, 600), pContext))
	{
		TRACE0("Failed to create third pane\n");
		return FALSE;
	}

	SetActiveView((CView*)m_wndSplitter2.GetPane(0,1));
	m_wndSplitter.SetRowInfo(0, 400, 0);
	m_wndSplitter.SetRowInfo(1,  50, 0);

	m_bSplittersCreated = TRUE;

	// it all worked, we now have two splitter windows which contain
	//  three different views
	return TRUE;
}

void CPrintSheetFrame::Resize()
{
	if (m_wndSplitter.m_hWnd)
	{
		CRect clientRect;
		GetClientRect(clientRect);

		if (m_bShowPrintSheetTable || m_bMarksViewActive)
		{
			CView* pView = (CView*)m_wndSplitter.GetPane(1, 0);		
			if (pView)
			{
				if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetTableView)))
				{
					if (m_bMaxPrintSheetTable)
					{
						int nPrintSheetTableViewHeight = clientRect.Height();
						m_wndSplitter.SetRowInfo(0, 0, 0);
						m_wndSplitter.SetRowInfo(1, nPrintSheetTableViewHeight, 0);
					}
					else
					{
						int nPrintSheetViewHeight	   = max(200, clientRect.Height() - 200);
						int nPrintSheetTableViewHeight = max(  0, clientRect.Height() - nPrintSheetViewHeight);
						m_wndSplitter.SetRowInfo(0, nPrintSheetViewHeight,		0);
						m_wndSplitter.SetRowInfo(1, nPrintSheetTableViewHeight, 0);
					}
				}
				else
					if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetMarksView)))
					{
						int nPrintSheetMarkViewHeight = ((CPrintSheetMarksView*)pView)->GetTotalSize().cy + 45;
						int nPrintSheetViewHeight	  = max(0, clientRect.Height() - nPrintSheetMarkViewHeight);

						m_wndSplitter.SetRowInfo(1, nPrintSheetMarkViewHeight, 0);
						m_wndSplitter.SetRowInfo(0, nPrintSheetViewHeight,	   0);
					}
			}
		}
		else
		{
			int nPrintSheetViewHeight = clientRect.Height();
			m_wndSplitter.SetRowInfo(0, nPrintSheetViewHeight,	0);
			m_wndSplitter.SetRowInfo(1, 0, 0);
		}
		m_wndSplitter.RecalcLayout();
//		m_wndSplitter.UpdateWindow();
	}
}

void CPrintSheetFrame::OnSize(UINT nType, int cx, int cy) 
{
	CMDIChildWnd::OnSize(nType, cx, cy);	
}

BOOL CPrintSheetFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	if(CMDIChildWnd::PreCreateWindow(cs) == 0)
		return FALSE;
	cs.style&=~(LONG)FWS_ADDTOTITLE;	// Delete default title

	return TRUE;
}

BOOL CPrintSheetFrame::PreTranslateMessage(MSG* pMsg) 
{
	// VK_DELETE is defined as accelarator key ...
	if ((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_DELETE))
		if ( (GetFocus() == &((CPrintSheetView*)GetActiveView())->m_InplaceEditPageNum) ||
		     (GetFocus() == &((CPrintSheetView*)GetActiveView())->m_InplaceEditBleed) ||	
		     (GetFocus() == &((CPrintSheetView*)GetActiveView())->m_InplaceEditGlobalMask) )	
			return FALSE;	// ... so if InplaceEdit has the focus, don't translate the message				
							// but let InplaceEdit receive it.

	return CMDIChildWnd::PreTranslateMessage(pMsg);
}

void CPrintSheetFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd) 
{
	if ( ! bActivate) // If frame will be deactivated close an open DlgExposureMediaSettings dialog
	{
		if (m_pDlgOutputMedia->m_hWnd)
			OnOutputPlates();

		if (m_pDlgMoveObjects->m_hWnd)
			OnMoveObjects();

		if (m_pDlgLayoutObjectProperties->m_hWnd)
			OnObjectProperties();
	}

	CMDIChildWnd::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	if (bActivate) 
	{
		CMenu newMenu;
		newMenu.LoadMenu(IDR_PRINTSHEETFRAME);

		CMenu* pTopMenu = AfxGetMainWnd()->GetMenu();
		pTopMenu->ModifyMenu(2, MF_POPUP, (UINT)newMenu.m_hMenu);
	}

	OnUpdateFrameTitle(TRUE);
}

void CPrintSheetFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	// update our parent window first
	GetMDIFrame()->OnUpdateFrameTitle(bAddToTitle);

	CDocument* pDoc = GetActiveDocument();
	if (pDoc)
	{
		if ( (theApp.m_nGUIStyle == CImpManApp::GUINewTemplate) || (theApp.m_nGUIStyle == CImpManApp::GUIPageList) )
			::SetWindowText(this->GetSafeHwnd(), pDoc->GetPathName());
		else
		if ( (theApp.m_nGUIStyle == CImpManApp::GUIOpenTemplate) || (theApp.m_nGUIStyle == CImpManApp::GUIOpenOutputList) )
			::SetWindowText(this->GetSafeHwnd(), pDoc->GetPathName());
		else
		if (theApp.m_nGUIStyle == CImpManApp::GUIAutoImpose)
			::SetWindowText(this->GetSafeHwnd(), pDoc->GetPathName());
		else
		{
			CString title = pDoc->GetTitle();
			title += _T(" - ");
			CString string;
			string.LoadString(IDS_PRINTSHEET_TITLE);
			title += string;
			::SetWindowText(this->GetSafeHwnd(), title);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// Main - Menu

void CPrintSheetFrame::OnFilePageSetup() 
{
	PRINTDLG printDlg;
	theApp.GetPrinterDeviceDefaults(&printDlg);

	CPageSetupDialog dlg;
	dlg.m_psd.lStructSize		= sizeof(PAGESETUPDLG);
	dlg.m_psd.hwndOwner			= m_hWnd;
	dlg.m_psd.hDevNames			= printDlg.hDevNames;
	dlg.m_psd.hDevMode			= printDlg.hDevMode;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	TCHAR szMargins[100];
	DWORD dwSize = sizeof(szMargins);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szMargins, &dwSize) == ERROR_SUCCESS)
		_stscanf(szMargins, _T("%ld %ld %ld %ld"), &dlg.m_psd.rtMargin.left, &dlg.m_psd.rtMargin.top, &dlg.m_psd.rtMargin.right, &dlg.m_psd.rtMargin.bottom);
	else
		dlg.m_psd.rtMargin = CRect(2500, 2500, 2500, 2500);

	dlg.DoModal();

	theApp.SelectPrinter(dlg.m_psd.hDevNames, dlg.m_psd.hDevMode);

	_stprintf(szMargins, _T("%ld %ld %ld %ld"), dlg.m_psd.rtMargin.left, dlg.m_psd.rtMargin.top, dlg.m_psd.rtMargin.right, dlg.m_psd.rtMargin.bottom);
	RegSetValueEx(hKey, _T("PrintMargins"), 0, REG_SZ, (LPBYTE)szMargins, (_tcslen(szMargins) + 1) * sizeof(TCHAR));
}

void CPrintSheetFrame::OnFilePrintPreview() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->OnFilePrintPreview();
}

void CPrintSheetFrame::OnUpdateFilePrintPreview(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bShowPrintMenu);
}

void CPrintSheetFrame::OnFilePrint() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	pView->OnFilePrint();
}

void CPrintSheetFrame::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bShowPrintMenu);
}


/////////////////////////////////////////////////////////////////////////////
// "Bearbeiten" - Menu

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CPrintSheetFrame::OnNotes() 
{
	CDlgNotes notesDlg;
	notesDlg.DoModal();
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CPrintSheetFrame::OnDefineSheet()
{
	CDlgSheet sheetDlg;
	sheetDlg.DoModal();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////


void CPrintSheetFrame::OnSetMark() 
{
	CView* pOldView = (CView*)m_wndSplitter.GetPane(1, 0);		
	if (pOldView->IsKindOf(RUNTIME_CLASS(CPrintSheetMarksView)))	
	{
		m_bMarksViewActive = TRUE;

		if (IsOpenMarkSetDlg())
		{
			GetMarkSetDlg()->OnClose();
			if (IsOpenMarkSetDlg())		// close cancelled
				return;
		}

		int  nCmdShow = -2;	// only restore splitter positions and status array
		BOOL statusArray[2];
		CMainFrame::RestoreFrameStatus(this, m_bStatusRestored, nCmdShow, &m_wndSplitter, &m_wndSplitter2, NULL, NULL, NULL, sizeof(statusArray), (LPBYTE)&statusArray);
		m_bShowPrintSheetTable = statusArray[0];
		m_bMaxPrintSheetTable  = statusArray[1];
	}
	else
	{
		m_bMarksViewActive = FALSE;
		BOOL statusArray[2] = {m_bShowPrintSheetTable, m_bMaxPrintSheetTable};
		CMainFrame::SaveFrameStatus(this, &m_wndSplitter, &m_wndSplitter2, NULL, NULL, NULL, sizeof(statusArray), (LPBYTE)&statusArray);
		m_bStatusRestored = FALSE;
	}

	CView* pActiveView = GetActiveView();
	BOOL   bSetActive  = ( !pActiveView || (pActiveView == pOldView)) ? TRUE : FALSE;

	CCreateContext context;
	context.m_pNewViewClass	  = (m_bMarksViewActive) ? RUNTIME_CLASS(CPrintSheetTableView) : RUNTIME_CLASS(CPrintSheetMarksView);
	context.m_pCurrentDoc	  = pOldView->GetDocument();
	context.m_pNewDocTemplate = NULL;
	context.m_pLastView		  = NULL;
	context.m_pCurrentFrame	  = NULL;

	m_wndSplitter.DeleteView(1, 0);
	m_wndSplitter.CreateView(1, 0, context.m_pNewViewClass, CSize(0, 0), &context);

	CView* pNewView = (CView*)m_wndSplitter.GetPane(1, 0);
	if (bSetActive)
		SetActiveView(pNewView);

	m_wndSplitter.RecalcLayout();

	m_bMarksViewActive = !m_bMarksViewActive;

	if (m_bMarksViewActive && ! m_bShowPlate)
		OnPlatecheck();

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	pView->m_DisplayList.Invalidate();

	if (m_bMarksViewActive)
	{
		if (m_nViewMode != ID_VIEW_ALL_SHEETS)
			OnZoomDecrease();

		Resize();
		BeginWaitCursor();	
		pNewView->OnInitialUpdate();
		EndWaitCursor();
	}
	else
	{
		pNewView->OnInitialUpdate();
   		CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
		if (pView)
		{
			CButton* pButton = ((CButton*)pView->GetDlgItem(IDC_MAXMIN_BUTTON));
			if (pButton)
				pButton->SetIcon(theApp.LoadIcon((m_bMaxPrintSheetTable) ? IDI_MINIMIZE_WINDOW : IDI_MAXIMIZE_WINDOW));
		}

		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	}
}


void CPrintSheetFrame::OnUpdateSetMark(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bMarksViewActive);
}

void CPrintSheetFrame::OnDefineMark() 
{
//	CDefineMarksSheet propSheet;
//	propSheet.DoModal();
}
void CPrintSheetFrame::OnWorkOnMark() 
{
//	CDlgMarks marksDlg;
//	marksDlg.DoModal();
}
void CPrintSheetFrame::OnUpdateWorkOnMark(CCmdUI* pCmdUI) 
{
#ifdef _DEBUG
	pCmdUI->Enable(TRUE);	
#else
	pCmdUI->Enable(FALSE);	
#endif	
}
void CPrintSheetFrame::OnUpdateDefineMark(CCmdUI* pCmdUI) 
{
#ifdef _DEBUG
	pCmdUI->Enable(TRUE);	
#else
	pCmdUI->Enable(FALSE);	
#endif	
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CPrintSheetFrame::OnChangeFormats() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));

	pView->OnActivateLayoutObject(pDI, CPoint(pDI->m_AuxRect.left + 12, pDI->m_AuxRect.bottom - 12), 0);
	return;

	if ( ! pDI)
	{
		AfxMessageBox(IDS_NO_OBJECT_SELECTED, MB_OK|MB_ICONSTOP);
		return;
	}

	CDlgChangeFormats formatsDlg;
	formatsDlg.DoModal();
}
void CPrintSheetFrame::OnUpdateChangeFormats(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bLayoutObjectSelected);	
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CPrintSheetFrame::OnAddExposurePrintsheet() 
{
	//CDlgAddExposure addexpDlg;
	//addexpDlg.DoModal();
}
/////////////////////////////////////////////////////////////////////////////
void CPrintSheetFrame::OnTurnTumblePrintSheet() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	CLayout* pActLayout = pDoc->GetActLayout();

	if (!pActLayout)
		return;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	if (pActLayout->KindOfProduction() == WORK_AND_TURN)
	{
		pActLayout->m_BackSide.TurnAll();
		pActLayout->m_BackSide.TumbleAll();
	}
	else
	{
		pActLayout->m_BackSide.TumbleAll();
		pActLayout->m_BackSide.TurnAll();
	}

	pActLayout->CheckForProductType();

	pDoc->SetModifiedFlag();
	Invalidate();

	UpdateMarkDialogs();
}

void CPrintSheetFrame::OnUpdateTurnTumble(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bTurnTumbleMenuActive);
}
/////////////////////////////////////////////////////////////////////////////
void CPrintSheetFrame::OnMoveObjects() 
{
	if (m_pDlgMoveObjects->m_hWnd)
		m_pDlgMoveObjects->DestroyWindow();
	else
		m_pDlgMoveObjects->Create(IDD_MOVE_OBJECTS, this);

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CPrintSheetFrame::OnUpdateMoveObject(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bLayoutObjectSelected);
	if (m_pDlgMoveObjects->m_hWnd)
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);
}

/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnAlignSection() 
{
	m_bAlignSectionActive = ! m_bAlignSectionActive;
	Invalidate();	// Hide or show the dotted lines for the sections
}

void CPrintSheetFrame::OnUpdateAlignSection(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bAlignSectionMenuActive);	
	pCmdUI->SetCheck(m_bAlignSectionActive);
}

void CPrintSheetFrame::OnResetSection() 
{
	CImpManDoc* pDoc = ((CImpManDoc*)GetActiveDocument());
	CLayout* pLayout = pDoc->GetActLayout();
	if (pLayout)
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			pView->UndoSave();

		if (theApp.settings.m_bFrontRearParallel)
		{
			pLayout->m_FrontSide.PositionLayoutSide();
			pLayout->m_BackSide.PositionLayoutSide();
		}
		else
		{
			switch (pDoc->GetActSight())
			{
			case BOTHSIDES: pLayout->m_FrontSide.PositionLayoutSide();
							pLayout->m_BackSide.PositionLayoutSide();
							break;
			case FRONTSIDE: pLayout->m_FrontSide.PositionLayoutSide();
							break;
			case BACKSIDE : pLayout->m_BackSide.PositionLayoutSide();
							break;
			}
		}
		pDoc->SetModifiedFlag();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
	}
}
void CPrintSheetFrame::OnUpdateResetSection(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(m_bAlignSectionMenuActive);	
}

void CPrintSheetFrame::OnUpdateSectionMenu() 
{
	m_bAlignSectionMenuActive = FALSE;
	m_bAlignSectionActive	  = FALSE;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	int nActSight = pDoc->GetActSight();
	if (nActSight != ALLSIDES)
		if (m_bShowPlate && ! m_bShowFoldSheet && ! m_bShowExposures && ! m_bTrimToolActive)
		{
			CPressDevice* pPressDevice = pDoc->GetActPressDevice(nActSight);
			if (pPressDevice) // For BOTHSIDES we get the FRONTSIDE-Device
				if (pPressDevice->m_nPressType == CPressDevice::WebPress)
					m_bAlignSectionMenuActive = TRUE; // Enable Align-Section menu
		}
}


/////////////////////////////////////////////////////////////////////////////
// "Ansicht" - Menu

void CPrintSheetFrame::OnPagelistView() 
{
	CMainFrame frame;

	CDocument* pDoc = GetActiveDocument();
	ASSERT(pDoc != NULL);
	frame.CreateOrActivatePageListFrame(theApp.m_pPageListTemplate, pDoc);
}

/////////////////////////////////////////////////////////////////////////////
// "Settings" - Menu

void CPrintSheetFrame::OnSettingsPressDevice() 
{
	CDlgPressDevice pDDlg;
	pDDlg.DoModal();
}
void CPrintSheetFrame::OnUpdateSettingsPressDevice(CCmdUI* pCmdUI) 
{
}

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

void CPrintSheetFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
  	CMDIChildWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	// CG: The following block was inserted by 'Status Bar' component.
	{
		GetParentFrame()->PostMessage(WM_INITMENUPOPUP,
			(WPARAM)pPopupMenu->GetSafeHmenu(), MAKELONG(nIndex, bSysMenu));
	}
}

void CPrintSheetFrame::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu)
{
  	CMDIChildWnd::OnMenuSelect(nItemID, nFlags, hSysMenu);
	// CG: The following block was inserted by 'Status Bar' component.
	{
		static BOOL bMenuActive = FALSE;
		if (bMenuActive || hSysMenu != NULL)
		{
			GetParentFrame()->PostMessage(WM_MENUSELECT, 
				MAKELONG(nItemID, nFlags), (LPARAM)hSysMenu);
		}
		bMenuActive = hSysMenu != NULL;
	}
}

void CPrintSheetFrame::OnDeleteSelection() 
{
	DeleteSelection(&m_bLayoutObjectSelected, &m_bFoldSheetSelected);
}

void CPrintSheetFrame::OnUpdateDeleteSelection(CCmdUI* pCmdUI) 
{
	if (m_bLayoutObjectSelected || m_bFoldSheetSelected || m_bMarksViewActive)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}
//
//void CPrintSheetFrame::DeleteSelection(BOOL* bLayoutObjectSelected, BOOL* bFoldSheetSelected) 
//{
//	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
//	CDisplayItem*	 pDI   = pView->m_DisplayList.GetFirstSelectedItem();
//	if (pDI)
//	{
//		if (AfxMessageBox(IDS_DELETE_SELECTIONS_WARNING, MB_YESNO) == IDNO)
//			return;
//	}
//	else
//	{
//		CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
//		if (pMarksView)
//			pMarksView->OnMarkDelete();
//	}
//
//	BOOL		 bSaved				= FALSE;
//	int			 nRet				= -1;
//	BOOL		 bFoldSheetDeleted	= FALSE;
//	BOOL		 bObjectDeleted		= FALSE;
//	BOOL		 bMarkDeleted		= FALSE;
//	CImpManDoc*	 pDoc				= CImpManDoc::GetDoc();
//	CLayout*	 pLayout			= NULL;
//	CLayoutSide* pLayoutSide		= NULL;
//	int			 nDeleteSide		= -1;
//	while (pDI)
//	{
//		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
//		{
//			if ( ! bSaved && pView)
//			{
//				pView->UndoSave();
//				bSaved = TRUE;
//			}
//
//			BOOL bPrintSheetRemoved = FALSE;
//			if (((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
//			{
//				if (nRet == -1)
//					nRet = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNO);
//				if (nRet == IDYES)
//				{
//					BOOL bFirst			= TRUE;
//					int nLayoutIndex	= ((CPrintSheet*)pDI->m_pAuxData)->m_nLayoutIndex;
//					int nNewLayoutIndex = nLayoutIndex;
//					POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
//					while (pos)
//					{
//						CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
//						if (rPrintSheet.m_nLayoutIndex == nLayoutIndex)
//						{
//							if (!rPrintSheet.RemoveFoldSheetLayer((int)pDI->m_pData, FALSE, (bFirst) ? TRUE : FALSE, (bFirst) ? FALSE : TRUE) )
//								break;
//							else
//							{
//								pView->m_DisplayList.DeselectItemsByData(pDI->m_pData, (void*)&rPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
//								bFoldSheetDeleted = TRUE;
//							}
//							if (bFirst)
//								nNewLayoutIndex = rPrintSheet.m_nLayoutIndex;
//							else
//								rPrintSheet.m_nLayoutIndex = nNewLayoutIndex;
//							bFirst = FALSE;
//						}
//					}
//					break;
//				}
//				else
//					bFoldSheetDeleted = ((CPrintSheet*)pDI->m_pAuxData)->RemoveFoldSheetLayer((int)pDI->m_pData, FALSE, FALSE, FALSE, FALSE, NULL, bPrintSheetRemoved);
//			}
//			else
//				bFoldSheetDeleted = ((CPrintSheet*)pDI->m_pAuxData)->RemoveFoldSheetLayer((int)pDI->m_pData, FALSE, TRUE, FALSE, FALSE, NULL, bPrintSheetRemoved);
//
//			if (bFoldSheetDeleted)	// Deselect corresponding display items
//			{
//				if (pDoc->m_PrintSheetList.GetPrintSheetFromNumber( ((CPrintSheet*)pDI->m_pAuxData)->m_nPrintSheetNumber))	// if not existing, sheet has been removed in RemoveFoldSheetLayer()
//				{
//					pView->m_DisplayList.DeselectItemsByData(pDI->m_pData, pDI->m_pAuxData, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
//					pLayout = (bPrintSheetRemoved) ? NULL : ((CPrintSheet*)pDI->m_pAuxData)->GetLayout();
//				}
//			}
//		}
//		else
//			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
//			{
//				if ( ! bSaved && pView)
//				{
//					pView->UndoSave();
//					bSaved = TRUE;
//				}
//
//				pLayoutSide  = ((CLayoutObject*)pDI->m_pData)->GetLayoutSide();
//				POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
//				while (pos)
//				{
//					POSITION	   prevPos = pos;
//					CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
//					if (&rObject == (CLayoutObject*)pDI->m_pData)
//					{
//						if ( ! rObject.IsFlatProduct())
//						{
//							if ((rObject.m_nComponentRefIndex >= 0) && (rObject.m_nType == CLayoutObject::Page))
//								if (rObject.GetCurrentFoldSheet((CPrintSheet*)pDI->m_pAuxData))
//								{
//									AfxMessageBox(IDS_DELETE_PAGE_MESSAGE, MB_OK);
//									return;
//								}
//						}
//
//						if (nDeleteSide == -1)
//							if (rObject.m_nType == CLayoutObject::ControlMark)
//								if (rObject.FindCounterpartControlMark())
//								{
//									CDlgDeleteMark dlg;
//									switch (dlg.DoModal())
//									{
//									case IDC_DELETE_FRONTMARK_BUTTON:	nDeleteSide = FRONTSIDE; break;
//									case IDC_DELETE_BACKMARK_BUTTON:	nDeleteSide = BACKSIDE;  break;
//									case IDC_DELETE_BOTHMARK_BUTTON:	nDeleteSide = BOTHSIDES; break;
//									case IDCANCEL:	return;
//									}
//								}
//								else
//									nDeleteSide = rObject.GetLayoutSideIndex();
//
//						int nComponentRefIndex = rObject.m_nComponentRefIndex;
//
//						CLayoutObject* pCounterPartObject = NULL;
//						if (rObject.m_nType == CLayoutObject::ControlMark)
//						{
//							pCounterPartObject = rObject.FindCounterpartControlMark();
//							bMarkDeleted = TRUE;
//							if ( ( (rObject.GetLayoutSideIndex() == FRONTSIDE) && (nDeleteSide == FRONTSIDE) ) ||
//								 ( (rObject.GetLayoutSideIndex() == BACKSIDE)  && (nDeleteSide == BACKSIDE) ) || (nDeleteSide == BOTHSIDES) )
//							{
//								if (rObject.IsDynamicMark())
//									rObject.RemoveMarkInstance();
//								else
//									pLayoutSide->m_ObjectList.RemoveAt(prevPos);
//							}
//						}
//						else
//						{
//							pCounterPartObject = rObject.FindCounterpartObject();
//							pLayoutSide->m_ObjectList.RemoveAt(prevPos);
//						}
//
//						if (pCounterPartObject)
//						{
//							CLayoutSide* pCounterPartSide = pCounterPartObject->GetLayoutSide();
//							POSITION pos = pCounterPartSide->m_ObjectList.GetHeadPosition();
//							while (pos)
//							{
//								POSITION	   prevPos = pos;
//								CLayoutObject& rCounterObject = pCounterPartSide->m_ObjectList.GetNext(pos);
//								if (&rCounterObject == pCounterPartObject)
//									if (rCounterObject.m_nType == CLayoutObject::ControlMark)
//									{
//										if ( ( (rCounterObject.GetLayoutSideIndex() == FRONTSIDE) && (nDeleteSide == FRONTSIDE) ) ||
//											 ( (rCounterObject.GetLayoutSideIndex() == BACKSIDE)  && (nDeleteSide == BACKSIDE) ) || (nDeleteSide == BOTHSIDES) )
//										{
//											if (rCounterObject.IsDynamicMark())
//												rCounterObject.RemoveMarkInstance();
//											else
//												pCounterPartSide->m_ObjectList.RemoveAt(prevPos);
//										}
//									}
//									else
//										pCounterPartSide->m_ObjectList.RemoveAt(prevPos);
//							}
//						}
//
//						bObjectDeleted = TRUE;
//
//						pDoc->m_PrintSheetList.RemoveFlatProductRefs(pLayout = ((CPrintSheet*)pDI->m_pAuxData)->GetLayout(), nComponentRefIndex);
//						break;
//					}
//				}
//
//				pLayout = pLayoutSide->GetLayout();
//			}
//
//		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI);
//	}
//
//	if (bFoldSheetDeleted || bObjectDeleted)
//	{
//		pDoc->m_Bookblock.Reorganize(NULL, NULL);
//		pDoc->m_PageSourceList.InitializePageTemplateRefs();
//		pDoc->m_PageTemplateList.FindPrintSheetLocations();
//		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
//		pDoc->m_PrintSheetList.ReorganizeColorInfos();
//
//		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
//		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
//		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
//
//		pDoc->m_MarkSourceList.RemoveUnusedSources();
//		
//		if (bLayoutObjectSelected)
//			*bLayoutObjectSelected = FALSE;
//		if (bFoldSheetSelected)
//			*bFoldSheetSelected = FALSE;
//
//		if (bFoldSheetDeleted)	// treeview only needs to be updated (eventually) if foldsheet has been deleted
//		{						// but not, if object has been deleted
//			if (pLayout)
//				pLayout->CheckForProductType();
//		}
//
//		if (bMarkDeleted)
//		{
//			CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
//			if (pMarksView)
//			{
//				pMarksView->InitObjectList();
//				if (pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_hWnd)
//				{
//					switch (nDeleteSide)
//					{
//					case FRONTSIDE: pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 1; 
//									pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
//					case BACKSIDE:  pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
//									pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
//					case BOTHSIDES: pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
//									pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
//					default:		pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
//									pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
//					}
//					pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.UpdateData(FALSE);
//				}
//				pMarksView->m_dlgLayoutObjectControl.ReinitDialog(pMarksView->GetCurrentLayoutObject(), pMarksView->GetPrintSheet());
//				pMarksView->m_dlgObjectPreviewControl.ReinitDialog();
//			}
//			CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
//			if (pProdDataPrintView)
//			{
//				pProdDataPrintView->InitObjectList();
//				if (pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_hWnd)
//				{
//					switch (nDeleteSide)
//					{
//					case FRONTSIDE: pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 1; 
//									pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
//					case BACKSIDE:  pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
//									pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
//					case BOTHSIDES: pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
//									pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
//					default:		pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
//									pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
//					}
//					pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.UpdateData(FALSE);
//				}
//				if (pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_hWnd)
//					pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.ReinitDialog(pProdDataPrintView->GetCurrentMark(), pProdDataPrintView->GetActPrintSheet(), &pProdDataPrintView->m_objectList.m_colorDefTable);
//				//pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgObjectPreviewControl.ReinitDialog(pProdDataPrintView->GetCurrentMarkObject(), pProdDataPrintView->GetActPrintSheet());
//			}
//		}
//
//		if (pLayout)
//			pLayout->AnalyzeMarks();
//
//		pDoc->SetModifiedFlag(TRUE, NULL, FALSE);	// auto marks should not be changed after deletion
////		Invalidate();
//
//		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
//		if (pView)
//		{
//			pView->m_DisplayList.Invalidate();
//			//pView->m_DisplayList.DeselectAll();
//		}
//
//		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
//
//		//CPrintSheetNavigationView* pNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
//		//if (pNavigationView)
//		//	pNavigationView->UpdatePrintSheets(pLayout);	// when components move from sheet to unassigned, this does not work
//		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
//
//		theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
//		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),	NULL, 0, NULL, TRUE);	
//		theApp.UpdateView(RUNTIME_CLASS(CProductsView));
//		//theApp.UpdateView(RUNTIME_CLASS(CPrintComponentsView));
//		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
//		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
//		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));	
//		theApp.UpdateView(RUNTIME_CLASS(CProdDataPrintView));
//		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));
//	}
//}

void CPrintSheetFrame::DeleteSelection(BOOL* bLayoutObjectSelected, BOOL* bFoldSheetSelected) 
{
	CPrintSheetView*	  pPrintSheetView		= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintComponentsView* pPrintComponentsView	= (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
	CDisplayItem*	 pDI			 = pPrintSheetView->m_DisplayList.GetFirstSelectedItem();
	if (pDI)
	{
		if (AfxMessageBox(IDS_DELETE_SELECTIONS_WARNING, MB_YESNO) == IDNO)
			return;
	}
	else
	{
		CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
		if (pMarksView)
			pMarksView->OnMarkDelete();
	}

	if (pPrintSheetView)
		pPrintSheetView->UndoSave();

	int			 		nRet				= -1;
	BOOL		 		bFoldSheetDeleted	= FALSE;
	BOOL		 		bFlatProductDeleted	= FALSE;
	BOOL		 		bMarkDeleted		= FALSE;
	CImpManDoc*	 		pDoc				= CImpManDoc::GetDoc();
	CPrintComponent&	rComponent			= (pPrintComponentsView) ? pPrintComponentsView->GetActComponent() : CPrintComponent();
	CPrintSheet* 		pPrintSheet			= (pPrintSheetView)		 ? pPrintSheetView->GetActPrintSheet() : NULL;
	CLayout*	 		pLayout				= NULL;
	CLayoutSide* 		pLayoutSide			= NULL;
	int			 		nDeleteSide			= -1;
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet)))
		{
			if ( ! DeleteFoldSheet(pDI, nRet, bFoldSheetDeleted, &pLayout))
				break;
			pDI->Invalidate(pPrintSheetView->m_DisplayList, TRUE, FALSE);
		}
		else
		{
			if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)))
			{
				CLayoutObject *pLayoutObject = (CLayoutObject*)pDI->m_pData;
				if (pLayoutObject->IsFlatProduct())
				{
					if ( ! DeleteFlatProduct(pDI, nRet, bFlatProductDeleted, &pLayout))
						break;
					pDI->Invalidate(pPrintSheetView->m_DisplayList, TRUE, FALSE);
				}
				else
				{
					pLayoutSide  = ((CLayoutObject*)pDI->m_pData)->GetLayoutSide();
					POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
					while (pos)
					{
						POSITION	   prevPos = pos;
						CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
						if (&rObject == (CLayoutObject*)pDI->m_pData)
						{
							if (nDeleteSide == -1)
								if (rObject.m_nType == CLayoutObject::ControlMark)
									if (rObject.FindCounterpartControlMark())
									{
										CDlgDeleteMark dlg;
										switch (dlg.DoModal())
										{
										case IDC_DELETE_FRONTMARK_BUTTON:	nDeleteSide = FRONTSIDE; break;
										case IDC_DELETE_BACKMARK_BUTTON:	nDeleteSide = BACKSIDE;  break;
										case IDC_DELETE_BOTHMARK_BUTTON:	nDeleteSide = BOTHSIDES; break;
										case IDCANCEL:	return;
										}
									}
									else
										nDeleteSide = rObject.GetLayoutSideIndex();

							CLayoutObject* pCounterPartObject = NULL;
							if (rObject.m_nType == CLayoutObject::ControlMark)
							{
								pCounterPartObject = rObject.FindCounterpartControlMark();
								bMarkDeleted = TRUE;
								if ( ( (rObject.GetLayoutSideIndex() == FRONTSIDE) && (nDeleteSide == FRONTSIDE) ) ||
									 ( (rObject.GetLayoutSideIndex() == BACKSIDE)  && (nDeleteSide == BACKSIDE) ) || (nDeleteSide == BOTHSIDES) )
								{
									if (rObject.IsDynamicMark())
										rObject.RemoveMarkInstance();
									else
										pLayoutSide->m_ObjectList.RemoveAt(prevPos);
								}
							}

							if (pCounterPartObject)
							{
								CLayoutSide* pCounterPartSide = pCounterPartObject->GetLayoutSide();
								POSITION pos = pCounterPartSide->m_ObjectList.GetHeadPosition();
								while (pos)
								{
									POSITION	   prevPos = pos;
									CLayoutObject& rCounterObject = pCounterPartSide->m_ObjectList.GetNext(pos);
									if (&rCounterObject == pCounterPartObject)
										if (rCounterObject.m_nType == CLayoutObject::ControlMark)
										{
											if ( ( (rCounterObject.GetLayoutSideIndex() == FRONTSIDE) && (nDeleteSide == FRONTSIDE) ) ||
												 ( (rCounterObject.GetLayoutSideIndex() == BACKSIDE)  && (nDeleteSide == BACKSIDE) ) || (nDeleteSide == BOTHSIDES) )
											{
												if (rCounterObject.IsDynamicMark())
													rCounterObject.RemoveMarkInstance();
												else
													pCounterPartSide->m_ObjectList.RemoveAt(prevPos);
											}
										}
								}
							}

							break;
						}
					}

					pLayout = pLayoutSide->GetLayout();
				}
			}
		}
		
		pDI = pPrintSheetView->m_DisplayList.GetNextSelectedItem(pDI);
	}

	if (bFoldSheetDeleted || bFlatProductDeleted || bMarkDeleted)
	{
		pDoc->m_Bookblock.Reorganize(NULL, NULL);
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();

		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

		pDoc->m_MarkSourceList.RemoveUnusedSources();
		
		if (bLayoutObjectSelected)
			*bLayoutObjectSelected = FALSE;
		if (bFoldSheetSelected)
			*bFoldSheetSelected = FALSE;

		if (bFoldSheetDeleted)	// treeview only needs to be updated (eventually) if foldsheet has been deleted
		{						// but not, if object has been deleted
			if (pLayout)
				pLayout->CheckForProductType();
		}

		if (bMarkDeleted)
		{
			CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
			if (pMarksView)
			{
				pMarksView->InitObjectList();
				if (pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_hWnd)
				{
					switch (nDeleteSide)
					{
					case FRONTSIDE: pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 1; 
									pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
					case BACKSIDE:  pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
									pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
					case BOTHSIDES: pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
									pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
					default:		pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
									pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
					}
					pMarksView->m_dlgLayoutObjectControl.m_dlgMarkPlacement.UpdateData(FALSE);
				}
				pMarksView->m_dlgLayoutObjectControl.ReinitDialog(pMarksView->GetCurrentLayoutObject(), pMarksView->GetPrintSheet());
				pMarksView->m_dlgObjectPreviewControl.ReinitDialog();
			}
			CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
			if (pProdDataPrintView)
			{
				pProdDataPrintView->InitObjectList();
				if (pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_hWnd)
				{
					switch (nDeleteSide)
					{
					case FRONTSIDE: pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 1; 
									pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
					case BACKSIDE:  pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
									pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
					case BOTHSIDES: pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
									pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
					default:		pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOn	   = 0; 
									pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.m_nPlaceOnBoth = 0; break;
					}
					pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_dlgMarkPlacement.UpdateData(FALSE);
				}
				if (pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.m_hWnd)
					pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgLayoutObjectControl.ReinitDialog(pProdDataPrintView->GetCurrentMark(), pProdDataPrintView->GetActPrintSheet(), &pProdDataPrintView->m_objectList.m_colorDefTable);
				//pProdDataPrintView->GetDlgLayoutObjectProperties()->m_dlgObjectPreviewControl.ReinitDialog(pProdDataPrintView->GetCurrentMarkObject(), pProdDataPrintView->GetActPrintSheet());
			}
		}

		if (pLayout)
		{
			pLayout->AnalyzeMarks();
			pPrintSheet = pLayout->GetFirstPrintSheet();
			while (pPrintSheet)
			{
				pPrintSheet->CalcTotalQuantity(TRUE);
				pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
			}
		}

		pDoc->SetModifiedFlag(TRUE, NULL, FALSE);	// auto marks should not be changed after deletion

		CPrintSheetViewStrippingData* pStrippingView = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
		if (pStrippingView)
		{
			if (pStrippingView->m_layoutDataWindow.m_dlgComponentImposer.m_hWnd)
				pStrippingView->m_layoutDataWindow.m_dlgComponentImposer.InitData(TRUE);
		}

		if (pPrintSheetView)
		{
			pPrintSheetView->m_DisplayList.Invalidate();
			pPrintSheetView->Invalidate();
			pPrintSheetView->UpdateWindow();
		}

		theApp.UpdateView(RUNTIME_CLASS(CBookBlockView));
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),	NULL, 0, NULL, TRUE);	
		theApp.UpdateView(RUNTIME_CLASS(CProductsView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));	
		theApp.UpdateView(RUNTIME_CLASS(CProdDataPrintView));
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));
	}
}

BOOL CPrintSheetFrame::DeleteFoldSheet(CDisplayItem* pDI, int& nRet, BOOL& bFoldSheetDeleted, CLayout** ppLayout) 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return TRUE;
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	int	 nComponentRefIndex = (int)pDI->m_pData;
	BOOL bPrintSheetRemoved = FALSE;
	if (((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
	{
		if (nRet == -1)
			nRet = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNO);
		if (nRet == IDYES)
		{
			BOOL bFirst			= TRUE;
			int nLayoutIndex	= ((CPrintSheet*)pDI->m_pAuxData)->m_nLayoutIndex;
			int nNewLayoutIndex = nLayoutIndex;
			POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
			while (pos)
			{
				CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
				if (rPrintSheet.m_nLayoutIndex == nLayoutIndex)
				{
					if (!rPrintSheet.RemoveFoldSheetLayer(nComponentRefIndex, FALSE, (bFirst) ? TRUE : FALSE, (bFirst) ? FALSE : TRUE) )
						break;
					else
					{
						pView->m_DisplayList.DeselectItemsByData(pDI->m_pData, (void*)&rPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
						bFoldSheetDeleted = TRUE;
					}
					if (bFirst)
						nNewLayoutIndex = rPrintSheet.m_nLayoutIndex;
					else
						rPrintSheet.m_nLayoutIndex = nNewLayoutIndex;
					bFirst = FALSE;
				}
			}
		}
		else
			bFoldSheetDeleted = ((CPrintSheet*)pDI->m_pAuxData)->RemoveFoldSheetLayer(nComponentRefIndex, FALSE, FALSE, FALSE, FALSE, NULL, bPrintSheetRemoved);
	}
	else
		bFoldSheetDeleted = ((CPrintSheet*)pDI->m_pAuxData)->RemoveFoldSheetLayer(nComponentRefIndex, FALSE, TRUE, FALSE, FALSE, NULL, bPrintSheetRemoved);

	if (bFoldSheetDeleted && ! bPrintSheetRemoved)	// Deselect corresponding display items
	{
		if (pDoc->m_PrintSheetList.GetPrintSheetFromNumber( ((CPrintSheet*)pDI->m_pAuxData)->m_nPrintSheetNumber))	
		{
			pView->m_DisplayList.DeselectItemsByData(pDI->m_pData, pDI->m_pAuxData, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
			if (ppLayout)
				*ppLayout = (bPrintSheetRemoved) ? NULL : ((CPrintSheet*)pDI->m_pAuxData)->GetLayout();

			CDisplayItem* pDI = pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
			while (pDI)
			{
				CDisplayItem* pDIPrev = pDI;
				pDI = pView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
				if ((int)pDIPrev->m_pData > nComponentRefIndex)
					pDIPrev->m_pData = (void*)((int)pDIPrev->m_pData - 1);
			}
		}
	}

	return bFoldSheetDeleted;
}

BOOL CPrintSheetFrame::DeleteFlatProduct(CDisplayItem* pDI, int& nRet, BOOL& bProductDeleted, CLayout** ppLayout) 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return TRUE;
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;
	CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
	if ( ! pLayoutObject)
		return TRUE;

	BOOL bPrintSheetRemoved = FALSE;
	int  nComponentRefIndex = pLayoutObject->m_nComponentRefIndex;
	if (((CPrintSheet*)pDI->m_pAuxData)->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
	{
		if (nRet == -1)
			nRet = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNO);
		if (nRet == IDYES)
		{
			BOOL bFirst			= TRUE;
			int nLayoutIndex	= ((CPrintSheet*)pDI->m_pAuxData)->m_nLayoutIndex;
			int nNewLayoutIndex = nLayoutIndex;
			POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
			while (pos)
			{
				CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
				if (rPrintSheet.m_nLayoutIndex == nLayoutIndex)
				{
					if (!rPrintSheet.RemoveFlatProductType(nComponentRefIndex, FALSE, (bFirst) ? TRUE : FALSE, (bFirst) ? FALSE : TRUE) )
						break;
					else
					{
						pView->m_DisplayList.DeselectItemsByData(pDI->m_pData, (void*)&rPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
						bProductDeleted = TRUE;
					}
					if (bFirst)
						nNewLayoutIndex = rPrintSheet.m_nLayoutIndex;
					else
						rPrintSheet.m_nLayoutIndex = nNewLayoutIndex;
					bFirst = FALSE;
				}
			}
		}
		else
			bProductDeleted = ((CPrintSheet*)pDI->m_pAuxData)->RemoveFlatProductType(nComponentRefIndex, FALSE, FALSE, FALSE, FALSE, NULL, bPrintSheetRemoved);
	}
	else
		bProductDeleted = ((CPrintSheet*)pDI->m_pAuxData)->RemoveFlatProductType(nComponentRefIndex, FALSE, TRUE, FALSE, FALSE, NULL, bPrintSheetRemoved);

	if (bProductDeleted)	// Deselect corresponding display items
	{
		if (pDoc->m_PrintSheetList.GetPrintSheetFromNumber( ((CPrintSheet*)pDI->m_pAuxData)->m_nPrintSheetNumber))	// if not existing, sheet has been removed in RemoveFoldSheetLayer()
		{
			pView->m_DisplayList.DeselectItemsByData(pDI->m_pData, pDI->m_pAuxData, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
			if (ppLayout)
				*ppLayout = (bPrintSheetRemoved) ? NULL : ((CPrintSheet*)pDI->m_pAuxData)->GetLayout();
		}
	}

	return bProductDeleted;
}

void CPrintSheetFrame::OnViewSingleSheets() 
{
	if (m_nViewMode == ID_VIEW_SINGLE_SHEETS)
		return;

	m_nViewMode = ID_VIEW_SINGLE_SHEETS;

	CPrintSheetTreeView* pView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
	if ( ! pView)
		return;

	pView->SelectPrintSheetTreeItem(1); 

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CPrintSheetFrame::OnUpdateViewSingleSheets(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
	if (m_nViewMode == ID_VIEW_SINGLE_SHEETS)
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);
}

void CPrintSheetFrame::OnViewAllSheets() 
{
	if (m_nViewMode == ID_VIEW_ALL_SHEETS)
		return;

	m_nViewMode = ID_VIEW_ALL_SHEETS;

	CPrintSheetTreeView* pView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
	if ( ! pView)
		return;

	pView->SelectRootTreeItem(); 

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);
}

void CPrintSheetFrame::OnUpdateViewAllSheets(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
	if (m_nViewMode == ID_VIEW_ALL_SHEETS)
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);
}

void CPrintSheetFrame::OnViewPrintsheettable() 
{
	m_bShowPrintSheetTable = !m_bShowPrintSheetTable;
	Resize();
}

void CPrintSheetFrame::OnUpdateViewPrintsheettable(CCmdUI* pCmdUI) 
{
	if (m_bShowPrintSheetTable)
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);
}

void CPrintSheetFrame::OnZoom() 
{
	if (m_nViewMode	!= ID_ZOOM)
	{
		m_nViewMode	  = ID_ZOOM;
		m_bHandActive = FALSE;
	}
	else
		m_nViewMode	  = ID_VIEW_SINGLE_SHEETS;

	if (m_wndSplitter2.m_hWnd)
		SetActiveView((CView*)m_wndSplitter2.GetPane(0,1));
	else
		if (m_wndSplitter.m_hWnd)
			SetActiveView((CView*)m_wndSplitter.GetPane(0,0));
}

void CPrintSheetFrame::OnUpdateZoom(CCmdUI* pCmdUI) 
{
	if (m_nViewMode == ID_VIEW_ALL_SHEETS)
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);

	if (m_nViewMode == ID_ZOOM)
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);
}

void CPrintSheetFrame::OnZoomDecrease() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	SetCursor(theApp.m_CursorMagnifyDecrease);
	pView->ZoomWindow(CPoint(0,0)); 
}

void CPrintSheetFrame::OnUpdateZoomDecrease(CCmdUI* pCmdUI) 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

void CPrintSheetFrame::OnZoomFullview() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	SetCursor(theApp.m_CursorMagnifyDecrease);
	pView->m_zoomSteps.RemoveAll();
	pView->ZoomWindow(CPoint(0,0)); 
}

void CPrintSheetFrame::OnUpdateZoomFullview(CCmdUI* pCmdUI) 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		pCmdUI->Enable(FALSE);
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

void CPrintSheetFrame::OnScrollhand() 
{
	m_bHandActive = ! m_bHandActive;
	m_nViewMode	  = ID_VIEW_SINGLE_SHEETS;
}

void CPrintSheetFrame::OnUpdateScrollhand(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck(m_bHandActive);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		pCmdUI->Enable(FALSE);
		m_bHandActive = FALSE;
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
	{
		pCmdUI->Enable(FALSE);
		m_bHandActive = FALSE;
	}
	else
		pCmdUI->Enable(TRUE);
}

void CPrintSheetFrame::OnDeleteMarks() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int	nSide	 = pDoc->GetActSight();
	int	nRemoved = 0;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));

	switch (nSide)
	{
	case ALLSIDES:	
		{
			if (AfxMessageBox(IDS_DELETE_MARKS_ALL, MB_YESNO) == IDNO)
				return;

			if (pView)
				pView->UndoSave();
			POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
			while (pos)
				nRemoved += pDoc->m_PrintSheetList.m_Layouts.GetNext(pos).RemoveMarkObjects();
		}
		break;

	case BOTHSIDES:
		{
			if (AfxMessageBox(IDS_DELETE_MARKS_LAYOUT, MB_YESNO) == IDNO)
				return;

			if (pView)
				pView->UndoSave();
			CLayout* pLayout = pDoc->GetActLayout();
			if (pLayout)
				nRemoved = pLayout->RemoveMarkObjects();
		}
		break;

	case FRONTSIDE:
		{
			if (AfxMessageBox(IDS_DELETE_MARKS_LAYOUTSIDE, MB_YESNO) == IDNO)
				return;

			if (pView)
				pView->UndoSave();
			CLayout* pLayout = pDoc->GetActLayout();
			if (pLayout)
				nRemoved = pLayout->m_FrontSide.RemoveMarkObjects();
		}
		break;

	case BACKSIDE:
		{
			if (AfxMessageBox(IDS_DELETE_MARKS_LAYOUTSIDE, MB_YESNO) == IDNO)
				return;

			if (pView)
				pView->UndoSave();
			CLayout* pLayout = pDoc->GetActLayout();
			if (pLayout)
				nRemoved = pLayout->m_BackSide.RemoveMarkObjects();
		}
		break;

	}

	if (nRemoved)
	{
		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();

		pDoc->m_MarkSourceList.RemoveUnusedSources();
		
		CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
		if (pMarksView)
		{
			pMarksView->InitObjectList();
//			pMarksView->m_dlgLayoutObjectControl.ReinitDialog(pMarksView->GetCurrentLayoutObject(), pMarksView->GetPrintSheet());
//			pMarksView->m_dlgObjectPreviewControl.ReinitDialog(pMarksView->GetCurrentLayoutObject(), pMarksView->GetPrintSheet());
			pMarksView->m_dlgLayoutObjectControl.ReinitDialog((CLayoutObject*)NULL, (CPrintSheet*)NULL);
			pMarksView->m_dlgObjectPreviewControl.ReinitDialog();
		}

		pDoc->SetModifiedFlag();										
		Invalidate();

		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	}
}

void CPrintSheetFrame::OnPdfOutput() 
{
	CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pView)
	{
		pView->m_DisplayList.Invalidate();
		pView->Invalidate();
		pView->m_PrintSheetTable.Initialize(CPrintSheetTable::ShowOutputList);
	}
}

void CPrintSheetFrame::OnUpdatePdfOutput(CCmdUI* pCmdUI) 
{
	CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pView)
		pCmdUI->SetCheck((pView->m_PrintSheetTable.m_nWhatToShow == CPrintSheetTable::ShowOutputList) ? TRUE : FALSE);
}

void CPrintSheetFrame::OnBmpOutput() 
{
	CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pView)
	{
		pView->m_DisplayList.Invalidate();
		pView->Invalidate();
		pView->m_PrintSheetTable.Initialize(CPrintSheetTable::ShowOutputList);
	}
}

void CPrintSheetFrame::OnUpdateBmpOutput(CCmdUI* pCmdUI) 
{
	CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pView)
		pCmdUI->SetCheck((pView->m_PrintSheetTable.m_nWhatToShow == CPrintSheetTable::ShowOutputList) ? TRUE : FALSE);
}

void CPrintSheetFrame::OnClose() 
{
	if (IsOpenMarkSetDlg())
	{
		GetMarkSetDlg()->OnClose();
		if (IsOpenMarkSetDlg())		// close cancelled
			return;
	}
	CMDIChildWnd::OnClose();
}

void CPrintSheetFrame::OnObjectProperties() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView)
		return;

	CLayoutObject* pObject = NULL;
	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	if (pDI)
		pObject = (CLayoutObject*)pDI->m_pData;
	else
	{
		pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
		if (pDI)
		{
			CExposure* pExposure = (CExposure*)pDI->m_pData;
			pObject = (pExposure) ? pExposure->GetLayoutObject() : NULL;
		}
	}

	if ( ! m_pDlgLayoutObjectProperties->m_hWnd)
		m_pDlgLayoutObjectProperties->Create(pObject, NULL, (pDI) ? (CPrintSheet*)pDI->m_pAuxData : NULL, this, FALSE, TRUE);
}

void CPrintSheetFrame::ActivateFrame(int nCmdShow) 
{
	BOOL statusArray[2];
	CMainFrame::RestoreFrameStatus(this, m_bStatusRestored, nCmdShow, &m_wndSplitter, &m_wndSplitter2, NULL, NULL, NULL, sizeof(statusArray), (LPBYTE)&statusArray);
	m_bShowPrintSheetTable = statusArray[0];
	m_bMaxPrintSheetTable  = statusArray[1];

	CMDIChildWnd::ActivateFrame(nCmdShow);

   	CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pView)
	{
		CButton* pButton = ((CButton*)pView->GetDlgItem(IDC_MAXMIN_BUTTON));
		if (pButton)
			pButton->SetIcon(theApp.LoadIcon((m_bMaxPrintSheetTable) ? IDI_MINIMIZE_WINDOW : IDI_MAXIMIZE_WINDOW));
	}
}

void CPrintSheetFrame::OnDestroy() 
{
	BOOL statusArray[2] = {m_bShowPrintSheetTable, m_bMaxPrintSheetTable};

	CMDIChildWnd::OnDestroy();
	CMainFrame::SaveFrameStatus(this, &m_wndSplitter, &m_wndSplitter2, NULL, NULL, NULL, sizeof(statusArray), (LPBYTE)&statusArray);
}

BOOL CPrintSheetFrame::IsOpenDlgLayoutObjectGeometry()
{
	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.IsOpenDlgGeometry())
			return TRUE;

	if (m_pDlgMarkSet)
		if (m_pDlgMarkSet->m_dlgLayoutObjectControl.IsOpenDlgGeometry())
			return TRUE;

	CPrintSheetMarksView* pView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pView)
		if (pView->m_dlgLayoutObjectControl.IsOpenDlgGeometry())
			return TRUE;

	return FALSE;
}

CDlgLayoutObjectGeometry* CPrintSheetFrame::GetDlgLayoutObjectGeometry()
{
	CDlgLayoutObjectGeometry* pDlg = NULL;

	if (m_pDlgLayoutObjectProperties)
		if ((pDlg = m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.GetDlgGeometry()))
			return pDlg;

	if (m_pDlgMarkSet)
		if (pDlg = m_pDlgMarkSet->m_dlgLayoutObjectControl.GetDlgGeometry())
			return pDlg;

	CPrintSheetMarksView* pView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pView)
		if (pDlg = pView->m_dlgLayoutObjectControl.GetDlgGeometry())
			return pDlg;

	return NULL;
}

BOOL CPrintSheetFrame::IsOpenMarkSetDlg()
{
	CDlgMarkSet* pDlg = m_pDlgMarkSet;
	if (pDlg)
		if (pDlg->m_hWnd)
			return TRUE;

	return FALSE;
}

CDlgMarkSet* CPrintSheetFrame::GetMarkSetDlg()
{
	CDlgMarkSet* pDlg = m_pDlgMarkSet;
	if (pDlg)
		if (pDlg->m_hWnd)
			return pDlg;

	return NULL;
}

BOOL CPrintSheetFrame::IsOpenDlgNewMark()
{
	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_hWnd)
			if (m_pDlgLayoutObjectProperties->IsWindowVisible())
				if (m_pDlgLayoutObjectProperties->m_pMark)
					return TRUE;

	return FALSE;
}

BOOL CPrintSheetFrame::IsOpenDlgNewMarkGeometry()
{
	if (IsOpenDlgNewMark())
	{
		if (m_pDlgLayoutObjectProperties)
			if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgCustomMarkGeometry.m_hWnd)
				if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgCustomMarkGeometry.IsWindowVisible())
					if (m_pDlgLayoutObjectProperties->IsActiveReflineControl())
						return TRUE;
		if (m_pDlgLayoutObjectProperties)
			if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgTextMarkGeometry.m_hWnd)
				if (m_pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_dlgTextMarkGeometry.IsWindowVisible())
					if (m_pDlgLayoutObjectProperties->IsActiveReflineControl())
						return TRUE;
	}

	return FALSE;
}

CDlgLayoutObjectProperties* CPrintSheetFrame::GetDlgNewMark()
{
	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_hWnd)
			if (m_pDlgLayoutObjectProperties->IsWindowVisible())
				if (m_pDlgLayoutObjectProperties->m_pMark)
					return m_pDlgLayoutObjectProperties;

	return NULL;
}

void CPrintSheetFrame::UpdateMarkDialogs()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheet* pPrintSheet = pDoc->GetActPrintSheet();
	if ( ! pPrintSheet)
		pPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;

	if (m_pDlgMarkSet)
		if (m_pDlgMarkSet->m_hWnd)
		{
			m_pDlgMarkSet->m_dlgLayoutObjectControl.ReinitDialog( ((m_pDlgMarkSet->m_nCurMarkSel >= 0) && (m_pDlgMarkSet->m_nCurMarkSel < m_pDlgMarkSet->m_markList.GetSize())) ? 
																  m_pDlgMarkSet->m_markList[m_pDlgMarkSet->m_nCurMarkSel] : NULL, pPrintSheet, &m_pDlgMarkSet->m_markList.m_colorDefTable);
			return;
		}

	if (m_pDlgLayoutObjectProperties)
		if (m_pDlgLayoutObjectProperties->m_hWnd)
		{
			if (m_pDlgLayoutObjectProperties->GetFirstMark())
				m_pDlgLayoutObjectProperties->ReinitDialog(m_pDlgLayoutObjectProperties->GetFirstMark(), m_pDlgLayoutObjectProperties->GetPrintSheet());
			else
				m_pDlgLayoutObjectProperties->ReinitDialog(m_pDlgLayoutObjectProperties->GetFirstLayoutObject(), m_pDlgLayoutObjectProperties->GetPrintSheet());
			return;
		}

	CPrintSheetMarksView* pView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pView)
	{
		pView->m_dlgLayoutObjectControl.DataExchangeSave();

		if (pView->m_pCurrentLayout != pDoc->GetActLayout())
			pView->InitObjectList();

		pView->m_dlgLayoutObjectControl.ReinitDialog(((pView->m_nCurObjectSel >= 0) && (pView->m_nCurObjectSel < pView->m_objectList.GetSize())) ? pView->m_objectList[pView->m_nCurObjectSel].m_pLayoutObject : NULL, 
													pPrintSheet);

		pView->m_dlgObjectPreviewControl.ReinitDialog();
	}
}

void CPrintSheetFrame::OpenDetailedView(CPrintSheet* pPrintSheet, int nSubView)
{
	CPrintSheetTreeView* pView = (CPrintSheetTreeView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTreeView));
	if (pPrintSheet)
	{
		m_nViewMode = ID_VIEW_SINGLE_SHEETS;
		pView->SelectPrintSheetTreeItem(pPrintSheet->m_nPrintSheetNumber); 
	}
	else
	{
		m_nViewMode = ID_VIEW_ALL_SHEETS;
		pView->SelectRootTreeItem(); 
	}

	switch (nSubView)
	{
	case SubViewMarks:			if ( ! m_bMarksViewActive) 
									OnSetMark();	
								break;
	case SubViewPrintSheets:	{
									CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
									if (pView)
										pView->OnPrintsheetList();
								}
								break;
	case SubViewOutput:			if (m_bMarksViewActive) 
									OnSetMark();
								if (m_nViewMode != ID_VIEW_ALL_SHEETS)
								{
									m_nViewMode = ID_VIEW_ALL_SHEETS;
									pView->SelectRootTreeItem(); 
								}
								OnPdfOutput();
								break;
	}
}
