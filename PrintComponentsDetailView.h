#pragma once



// CPrintComponentsDetailView-Ansicht

class CPrintComponentsDetailView : public CScrollView
{
	DECLARE_DYNCREATE(CPrintComponentsDetailView)

protected:
	CPrintComponentsDetailView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintComponentsDetailView();


// Attributes
public:
	DECLARE_DISPLAY_LIST(CPrintComponentsDetailView, OnMultiSelect, NO, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CPrintComponentsDetailView, FlatProduct,	   OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintComponentsDetailView, PageSectionProps, OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintComponentsDetailView, FoldSheetProps,   OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO,  OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
						
public:
	CSize					m_docSize;

public:
	CPrintSheet*			GetActPrintSheet();
	CPrintComponent 		GetActComponent();
	CDlgComponentImposer*	GetDlgComponentImposer();
	BOOL					IsOpenDlgComponentImposer();
	CSize					CalcExtents();
	void					ResizeView();
	CRect					Draw(CDC* pDC, CDisplayList* pDisplayList);
	void					UndoSave();



public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen


	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg void OnPaint();
};


