// PrintComponentsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintComponentsView.h"
#include "OrderDataFrame.h"
#include "GraphicComponent.h"
#include "NewPrintSheetFrame.h"
#include "PrintSheetView.h"
#include "DlgFlatProductData.h"
#include "PrintComponentsDetailView.h"


#define IDC_PRINTCOMPONENTSVIEW_HEADER 11114
#define ID_SHOW_UNPRINTED	   10000
#define ID_CLEAN_COMPONENTLIST 10001

// CPrintComponentsView

IMPLEMENT_DYNCREATE(CPrintComponentsView, CScrollView)

CPrintComponentsView::CPrintComponentsView()
{
    m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CPrintComponentsView));
	m_DisplayList.SetClassName(_T("CPrintComponentsView"));
 //   m_NcDisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CPrintComponentsView));
	//m_NcDisplayList.SetClassName(_T("CPrintComponentsView"));

	m_nHeaderItemsTotalWidth = 0;
	m_nHeaderHeight = 0;
	m_headerFont.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));

	m_clipBox.SetRectEmpty();
	m_nToolBarHeight = 34;
	m_nStatusBarHeight = 0;
	m_bShowUnprinted = FALSE;
	m_bShowPrintedOnly = FALSE;
}

CPrintComponentsView::~CPrintComponentsView()
{
	m_headerFont.DeleteObject();
}

BEGIN_MESSAGE_MAP(CPrintComponentsView, CScrollView)
    ON_WM_CREATE()
    ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_NCLBUTTONDOWN()
	ON_WM_NCMOUSEMOVE()
	ON_WM_NCMOUSELEAVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_SIZE()
	ON_NOTIFY(HDN_TRACK,	IDC_PRINTCOMPONENTSVIEW_HEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK, IDC_PRINTCOMPONENTSVIEW_HEADER, OnHeaderEndTrack)
	ON_COMMAND(ID_SHOW_UNPRINTED, OnShowUnprinted)
	ON_COMMAND(ID_CLEAN_COMPONENTLIST, OnClearComponentList)
	ON_UPDATE_COMMAND_UI(ID_SHOW_UNPRINTED, OnUpdateShowUnprinted)
	ON_UPDATE_COMMAND_UI(ID_CLEAN_COMPONENTLIST, OnUpdateSaveUnprinted)
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	ON_WM_NCHITTEST()
END_MESSAGE_MAP()


BOOL CPrintComponentsView::OnEraseBkgnd(CDC* pDC) 
{
	m_toolBar.ClipButtons(pDC);

	CRect rect;
    GetClientRect(rect);
	rect.top = m_nToolBarHeight;

	COLORREF crBackGround = WHITE;
	CBrush brush(crBackGround);
    //pDC->FillRect(&rect, &brush);

	CRect rcHeader = rect;
	rcHeader.top = 0; rcHeader.bottom = m_nToolBarHeight;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);

	rcHeader.top = rcHeader.bottom - 3;
	pDC->FillSolidRect(rcHeader, RGB(215,220,225));

	//CRect rcStatusBar = rect; rcStatusBar.top = rcStatusBar.bottom - m_nStatusBarHeight;
	//pDC->FillSolidRect(rcStatusBar, RGB(242,242,244));

    brush.DeleteObject();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// enable OLE DragDrop functionality - needed in conjunction with display list for foldsheet manipulation

int CPrintComponentsView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	if( m_dropTarget.Register( this ) )
		return 0;
	else
		return -1;
}


// CProductsView-Diagnose

#ifdef _DEBUG
void CPrintComponentsView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintComponentsView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG
///

void CPrintComponentsView::DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd)
{
	CRect rcHeader = rcFrame;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcHeader, _T(""), WHITE, RGB(240,240,240), 90, -1);
}

void CPrintComponentsView::OnInitialUpdate()
{
	CRect rcButton; 
	m_toolBar.Create(this, GetParentFrame(), NULL, IDB_PRINTCOMPONENTS_TOOLBAR, IDB_PRINTCOMPONENTS_TOOLBAR, CSize(25, 25), &DrawToolsBackground);
	rcButton = CRect(CPoint(10,0), CSize(28,30));
	m_toolBar.AddButton(-1, IDS_SHOW_UNPRINTED,			ID_SHOW_UNPRINTED,		rcButton, 0);	rcButton.OffsetRect(rcButton.Width(), 0);	
	m_toolBar.AddButton(-1, IDS_CLEAN_COMPONENTLIST,	ID_CLEAN_COMPONENTLIST,	rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);	

	m_nToolBarHeight = 34;
	m_nStatusBarHeight = 0;

	CScrollView::OnInitialUpdate();
}

void CPrintComponentsView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
	CScrollView::OnUpdate(pSender, lHint, pHint);
	m_headerColumnWidths.RemoveAll();
	m_HeaderCtrl.DestroyWindow();
	m_nHeaderHeight = 0;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if ( ! m_HeaderCtrl.GetSafeHwnd())
	{
		m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, IDC_PRINTCOMPONENTSVIEW_HEADER);	
		m_HeaderCtrl.SetFont(&m_headerFont, FALSE);

		HD_ITEM hdi;
		hdi.mask		= HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
		hdi.fmt			= HDF_LEFT | HDF_STRING;

		m_headerColumnWidths.RemoveAll();

		int		nIndex = 0;
		CString string;
		string.LoadString(IDS_PRODUCTPOOL);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetProductGroupsTableHeaderWidth(nIndex, 200);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_PRINTSHEET_SHORT); 
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetProductGroupsTableHeaderWidth(nIndex, 50);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_AMOUNT); 
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetProductGroupsTableHeaderWidth(nIndex, 80);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(IDS_DIFFERENCE); 
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetProductGroupsTableHeaderWidth(nIndex, 120);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		m_nHeaderItemsTotalWidth = 0;
		for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
			m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];
	}

	OnSize(0, 0, 0);

	ModifyStyle(0, WS_CLIPCHILDREN);	// Clip header control 

	Invalidate();

	m_docSize = CalcExtents();

	CRect rcClient;
	GetClientRect(rcClient);
	CSize pageSize, lineSize;
	pageSize.cx = lineSize.cx = m_docSize.cx;

	int nNumComponents = pDoc->m_printComponentsList.GetNumComponentsAll();
	pageSize.cy = rcClient.Height() - m_nToolBarHeight;
	lineSize.cy = (nNumComponents > 0) ? m_docSize.cy / pDoc->m_printComponentsList.GetNumComponentsAll() : pageSize.cy;

	SetScrollSizes(MM_TEXT, CSize(1, m_docSize.cy), pageSize, lineSize);	// suppress horizontal scrollbar by setting cx to 1

	m_toolBar.MoveButtons(CPoint(10,0));
	m_toolBar.Redraw();
}

void CPrintComponentsView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);

	if (m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdl;
		WINDOWPOS wp;
		hdl.prc = &clientRect;
		hdl.pwpos = &wp;
		m_HeaderCtrl.Layout(&hdl);	
		m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y + m_nToolBarHeight, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_nHeaderHeight = wp.cy;
	} 

	ResizeDetailsView();
}

// This functions are overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling

void CPrintComponentsView::ScrollToPosition(POINT pt)    // logical coordinates
{
	ASSERT(m_nMapMode > 0);     // not allowed for shrink to fit
	if (m_nMapMode != MM_TEXT)
	{
		CWindowDC dc(NULL);
		dc.SetMapMode(m_nMapMode);
		dc.LPtoDP((LPPOINT)&pt);
	}

	// now in device coordinates - limit if out of range
	int xMax = GetScrollLimit(SB_HORZ);
	int yMax = GetScrollLimit(SB_VERT);
	if (pt.x < 0)
		pt.x = 0;
	else if (pt.x > xMax)
		pt.x = xMax;
	if (pt.y < 0)
		pt.y = 0;
	else if (pt.y > yMax)
		pt.y = yMax;

	ScrollToDevicePosition(pt);
}

void CPrintComponentsView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);
	
	CRect scrollRect, clipRect;
	GetClientRect(scrollRect); clipRect = scrollRect;
	clipRect.top += m_nToolBarHeight + m_nHeaderHeight;	// Protect header control from being vertically scrolled
	clipRect.bottom -= m_nStatusBarHeight;
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, clipRect);

	if (m_HeaderCtrl.GetSafeHwnd())
		if (ptDev.x != 0)				// Scroll header control horizontally
			m_HeaderCtrl.MoveWindow(-ptDev.x, m_nToolBarHeight, scrollRect.Width() + ptDev.x, m_nHeaderHeight);

	//if (m_InplaceEdit.GetSafeHwnd())
	//	m_InplaceEdit.Deactivate();
}

BOOL CPrintComponentsView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect, clipRect;
		GetClientRect(scrollRect); clipRect = scrollRect;
		//if (m_HeaderCtrl.GetSafeHwnd())
			clipRect.top += m_nToolBarHeight + m_nHeaderHeight;	// Protect header control from being vertically scrolled
			clipRect.bottom -= m_nStatusBarHeight;
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, clipRect);

		if (m_HeaderCtrl.GetSafeHwnd())
			if (sizeScroll.cx != 0)				// Scroll header control horizontally
				m_HeaderCtrl.MoveWindow(-x, m_nToolBarHeight, scrollRect.Width() + x, m_nHeaderHeight);

		//if (m_InplaceEdit.GetSafeHwnd())
		//	m_InplaceEdit.Deactivate();
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

void CPrintComponentsView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 20)
		hdn->pitem->cxy = 20;
}

void CPrintComponentsView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	SetProductGroupsTableHeaderWidth(hdn->iItem, hdn->pitem->cxy);

	if (m_headerColumnWidths.GetSize() > hdn->iItem)
		m_headerColumnWidths[hdn->iItem] = hdn->pitem->cxy; 

	m_nHeaderItemsTotalWidth = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
		m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];

	CRect clientRect;
	GetClientRect(&clientRect);

	InvalidateRect(clientRect);
	UpdateWindow(); 
}

void CPrintComponentsView::SetProductGroupsTableHeaderWidth(int nIndex, int nValue)
{
	if (nIndex >= 20)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\PrintComponentsViewHeader");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			columnWidthArray[nIndex] = nValue;			
		else
		{
			ZeroMemory(columnWidthArray, sizeof(columnWidthArray));
			columnWidthArray[nIndex] = nValue;			
		}

		dwSize = sizeof(columnWidthArray);
		RegSetValueEx(hKey, _T("Columns"), 0, REG_BINARY, (LPBYTE)&columnWidthArray, dwSize);

		RegCloseKey(hKey);
	}
}

int CPrintComponentsView::GetProductGroupsTableHeaderWidth(int nIndex, int nDefault)
{
	if (nIndex >= 20)
		return nDefault;

	int nWidth = nDefault;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\PrintComponentsViewHeader");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			nWidth = columnWidthArray[nIndex];			
		else
			nWidth = nDefault;

		RegCloseKey(hKey);
	}

	if ( (nWidth <= 10) || (nWidth > 1000) )
		nWidth = nDefault;

	return nWidth;
}

void CPrintComponentsView::ResetDisplayColums()
{
	m_displayColumns.RemoveAll();
}

void CPrintComponentsView::AddDisplayColumn(int nColID, int nWidth)
{
	struct displayColumn dc = {nColID, nWidth};
	m_displayColumns.Add(dc);
}

CSize CPrintComponentsView::CalcExtents()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CRect rcBox = DrawListPart(&dc, NULL);

	dc.DeleteDC();

	return CSize(rcBox.right, rcBox.bottom + 10 + m_nStatusBarHeight);
}

void CPrintComponentsView::ClipHeader(CDC* pDC)
{
	CRect rcClientRect;
	GetClientRect(rcClientRect); m_clipBox = rcClientRect;
	m_clipBox.top = m_nToolBarHeight + m_nHeaderHeight;
	m_clipBox.bottom -= m_nStatusBarHeight;
	m_clipBox.OffsetRect(-pDC->GetViewportOrg());
	pDC->IntersectClipRect(m_clipBox);
}

void CPrintComponentsView::OnDraw(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(rcClient); 
	CRect rcFrame(CPoint(0, 0), CSize(rcClient.Width(), max(rcClient.Height(), m_docSize.cy + m_nToolBarHeight)));

	CDC	memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap bitmap;	bitmap.CreateCompatibleBitmap(pDC, rcFrame.Width(), rcFrame.Height());
	memDC.SelectObject(&bitmap);

	memDC.FillSolidRect(rcFrame, WHITE);//RGB(227,232,238));

	memDC.SaveDC();
	DrawListPart(&memDC, &m_DisplayList);
	memDC.RestoreDC(-1);
	
	pDC->SaveDC();	// save current clipping region
	ClipHeader(pDC);

	pDC->BitBlt(0, m_nToolBarHeight, rcFrame.Width(), rcFrame.Height(), &memDC, 0, m_nToolBarHeight, SRCCOPY);

	bitmap.DeleteObject();
	memDC.DeleteDC();

	pDC->RestoreDC(-1);
}

CRect CPrintComponentsView::DrawListPart(CDC* pDC, CDisplayList* pDisplayList)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(0,0,1,1);

	if (m_headerColumnWidths.GetSize() <= 0)
		return CRect(0,0,1,1);

	CRect rcClient;
	GetClientRect(rcClient); 

	CRect rcFrame = rcClient;
	rcFrame.top  += m_nToolBarHeight + m_nHeaderHeight;	

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC, FALSE, rcFrame);

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nColIndex = 0;
	ResetDisplayColums();
	AddDisplayColumn(ColInfo,					m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColPrintSheetLoc,			m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColProductQuantity,		m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColProductQuantityDiff, 	m_headerColumnWidths[nColIndex++]);

	BOOL bOldShowState = m_bShowUnprinted;
	CRect rcRet;
	if (m_bShowUnprinted)
	{
		CString strUnplaced; strUnplaced.LoadString(IDS_SHOW_UNPRINTED);
		CGraphicComponent gc;
		CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + 20;
		gc.DrawTitleBar(pDC, rcTitle, strUnplaced, RGB(206,102,104), RGB(206,102,104), 0.0f, TRUE, TRUE);//RGB(255,200,200), RGB(255,230,230), 0, 0);
		rcFrame.top = rcTitle.bottom;// + 5;
		rcRet = DrawCompleteProducts(pDC, rcFrame, pDisplayList);
		rcBox.UnionRect(rcBox, rcRet);
		rcFrame.top = rcBox.bottom + 20;
	}
	else
	{
	m_bShowUnprinted = FALSE;
	//if (bOldShowState)
	//{
	//	CString strFinished; strFinished.LoadString(IDS_FINISHED);
	//	m_bShowPrintedOnly = TRUE;
	//	CGraphicComponent gc;
	//	CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + 20;
	//	gc.DrawTitleBar(pDC, rcTitle, strFinished, RGB(204,219,68), RGB(204,219,68));//RGB(200,240,0), RGB(230,255,0), 0, 0);
	//	rcFrame.top = rcTitle.bottom + 5;
	//}
	rcRet = DrawCompleteProducts(pDC, rcFrame, pDisplayList);
	rcBox.UnionRect(rcBox, rcRet);
	m_bShowUnprinted = bOldShowState;;
	m_bShowPrintedOnly = FALSE;
	}

	rcFrame.top = rcBox.bottom + 20;
	rcRet = DrawIncompleteProducts(pDC, rcFrame, pDisplayList);
	rcBox.UnionRect(rcBox, rcRet);

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CPrintComponentsView::DrawStatusPart(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	//CRect rcClient;
	//GetClientRect(rcClient); 
	//CRect rcFrame = rcClient; rcFrame.top = rcFrame.bottom - m_nStatusBarHeight + 10; rcFrame.left += 10; rcFrame.right -= 10; rcFrame.bottom -= 10;
	//rcFrame.OffsetRect(-pDC->GetViewportOrg());

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CPrintSheet*	pPrintSheet = GetActPrintSheet();
	CPrintComponent component   = GetActComponent();
	if ( ! component.IsNull())
	{
		if (component.m_nType == CPrintComponent::Flat)
		{
			CFlatProduct* pFlatProduct = component.GetFlatProduct();
			if (pFlatProduct)
			{
				CRect rcRet = pFlatProduct->DrawSummary(pDC, rcFrame, &m_DisplayList, pPrintSheet);	rcRet.right = rcFrame.right;
				rcBox.UnionRect(rcBox, rcRet); 
			}
		}
		else
			if (component.m_nType == CPrintComponent::FoldSheet)
			{
				CFoldSheet* pFoldSheet = component.GetFoldSheet();
				if (pFoldSheet)
				{
					CRect rcRet = pFoldSheet->DrawSummary(pDC, rcFrame, &m_DisplayList, pPrintSheet);	rcRet.right = rcFrame.right;
					rcBox.UnionRect(rcBox, rcRet); 
				}
			}
	}

	return rcBox;
}

CRect CPrintComponentsView::DrawCompleteProducts(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CGraphicComponent gc;
	CPen linePen(PS_SOLID, 1, LIGHTGRAY);
	CPen* pOldPen = pDC->SelectObject(&linePen);

	pDC->SetBkMode(TRANSPARENT);

	CPrintSheet*			pPrintSheet				 = GetActPrintSheet();
	CPrintComponent&		rActComponent			 = GetActComponent();
	CPrintComponentsGroup*	pActPrintComponentsGroup = NULL;
	if ( ! rActComponent.IsNull())
		pActPrintComponentsGroup = &rActComponent.GetPrintComponentsGroup();
	else
	{
		CImpManDoc*  pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			rActComponent = pDoc->m_printComponentsList.GetComponent(0);
			if (pDoc->m_printComponentsList.GetSize() > 0)
				pActPrintComponentsGroup = &pDoc->m_printComponentsList[0];
		}
		if (pPrintSheet) 
		{
			if ( ! pPrintSheet->GetFirstPrintComponentsGroup().IsNull())
			{
				pActPrintComponentsGroup = &pPrintSheet->GetFirstPrintComponentsGroup();
				if (pActPrintComponentsGroup->m_printComponents.GetSize() > 0)
					rActComponent = pActPrintComponentsGroup->m_printComponents[0];
			}
		}
	}

	int	  nFlatProductPartIndex = pDoc->GetFlatProductsIndex();
	int	  nPrevProductPartIndex = -1;
	int	  nRowHeight = 20;
	CRect rcRow = rcFrame; rcRow.bottom = rcRow.top + nRowHeight;
	for (int nComponentGroupIndex = 0; nComponentGroupIndex < pDoc->m_printComponentsList.GetSize(); nComponentGroupIndex++)
	{
		CPrintComponentsGroup& rComponentGroup = pDoc->m_printComponentsList[nComponentGroupIndex];

		if (nComponentGroupIndex > 0)
			rcRow.OffsetRect(0, 10);

		BOOL bDrawGroupHeader = FALSE;
		if (m_bShowUnprinted)
		{
			if (rComponentGroup.HasUnprinted())
				bDrawGroupHeader = TRUE;
		}
		else
			if (m_bShowPrintedOnly)
			{
				if (rComponentGroup.HasPrinted())
					bDrawGroupHeader = TRUE;
			}
			else
				bDrawGroupHeader = TRUE;

		int nTop = 0;
		if (bDrawGroupHeader && pActPrintComponentsGroup)
		{
			BOOL bEnableHeader = (rActComponent.IsNull() && (nComponentGroupIndex == 0) ) ? TRUE : FALSE; 
			BOOL bEnabled = ( (pActPrintComponentsGroup == &rComponentGroup) || bEnableHeader) ? TRUE : FALSE;
			CRect rcHeader = rComponentGroup.DrawHeader(pDC, rcRow, bEnabled, FALSE, TRUE, pDisplayList);
			rcRow.top += rcHeader.Height();
			rcRow.bottom = rcRow.top + nRowHeight;
			nTop = rcHeader.top;
		}

		for (int nComponentIndex = 0; nComponentIndex < rComponentGroup.m_printComponents.GetSize(); nComponentIndex++)
		{
			CPrintComponent& rComponent = rComponentGroup.m_printComponents[nComponentIndex];

			BOOL bSkip = FALSE;
			if (m_bShowUnprinted)
				if (rComponent.IsPlaced())
					bSkip = TRUE;

			if ((nComponentIndex > 0) && ! bSkip)
			{
				BOOL bDrawSeperator = FALSE;
				CPrintComponent& rPrevComponent = rComponentGroup.m_printComponents[nComponentIndex - 1];
				if ( ((rPrevComponent.m_nType == CPrintComponent::FoldSheet) || (rPrevComponent.m_nType == CPrintComponent::PageSection)) && (rComponent.m_nType == CPrintComponent::Flat) )
					bDrawSeperator = TRUE;
				else
					if ( (rPrevComponent.m_nType == CPrintComponent::Flat) && ((rComponent.m_nType == CPrintComponent::FoldSheet) || (rComponent.m_nType == CPrintComponent::PageSection)) )
						bDrawSeperator = TRUE;
					else
						if (rPrevComponent.GetBoundProduct() != rComponent.GetBoundProduct())
							bDrawSeperator = TRUE;

				if (bDrawSeperator)
				{
					pDC->MoveTo(rcFrame.left,  rcRow.top - 2);
					pDC->LineTo(rcFrame.right, rcRow.top - 2);
					rcRow.OffsetRect(0, 4);
				}
			}

			int	 nCurProductPartIndex = (rComponent.m_nType == CPrintComponent::Flat) ? nFlatProductPartIndex : rComponent.m_nProductPartIndex;
			BOOL bDrawProductHeader = (nCurProductPartIndex != nPrevProductPartIndex) ? TRUE : FALSE;	
			nPrevProductPartIndex = (rComponent.m_nType == CPrintComponent::Flat) ? pDoc->GetFlatProductsIndex() : rComponent.m_nProductPartIndex;
	
			BOOL  bHasDrawn = FALSE;
			for (int nSheetRefIndex = 0; nSheetRefIndex < rComponent.m_sheetRefs.GetSize(); nSheetRefIndex++)
			{
				PrintComponentSheetRef& rSheetRef = rComponent.m_sheetRefs[nSheetRefIndex];

				BOOL bDrawThumbnail = (nSheetRefIndex > 0) ? FALSE : TRUE;

				/////// columns
				CRect rcColumn = rcRow; 
				rcColumn.left += 5;

				int	  nRight = 0;
				CRect rcRet, rcThumbnail;
				for (int i = 0; i < m_displayColumns.GetSize(); i++)
				{
					nRight += m_displayColumns[i].nWidth;
					rcColumn.right = nRight;
					switch (m_displayColumns[i].nID)
					{
					case ColInfo:					rcRet = rComponent.DrawInfo			(pDC, rcColumn,	nSheetRefIndex, bDrawThumbnail,	bDrawProductHeader, m_bShowUnprinted, m_bShowPrintedOnly, pDisplayList);	break;
					case ColPrintSheetLoc:			rcRet = rComponent.DrawPrintSheetLoc(pDC, rcColumn, nSheetRefIndex,	bDrawThumbnail,	bDrawProductHeader, m_bShowUnprinted, m_bShowPrintedOnly, pDisplayList);	break;
					case ColProductQuantity:		rcRet = rComponent.DrawQuantity		(pDC, rcColumn, nSheetRefIndex,	bDrawThumbnail,	bDrawProductHeader, m_bShowUnprinted, m_bShowPrintedOnly, pDisplayList);	break;
					case ColProductQuantityDiff:	rcRet = rComponent.DrawQuantityDiff	(pDC, rcColumn, nSheetRefIndex,	bDrawThumbnail,	bDrawProductHeader, m_bShowUnprinted, m_bShowPrintedOnly, pDisplayList);	break;
					}
					rcBox.UnionRect(rcBox, rcRet);

					if ( (rcRet.bottom - rcRet.top) > 1)
						bHasDrawn = TRUE;

					rcColumn.left = nRight + 5;
				}

				bDrawProductHeader = FALSE;

				if (bHasDrawn)
				{
					rcRow.top	 = rcBox.bottom + 10;
					rcRow.bottom = rcRow.top + nRowHeight;
				}
			}

			if (bHasDrawn)
			{
				rcRow.top	 = rcBox.bottom + 10;
				rcRow.bottom = rcRow.top + nRowHeight;
			}

			int  nNextProductPartIndex = (rComponent.m_nType == CPrintComponent::Flat) ? pDoc->GetFlatProductsIndex() : ( (nComponentIndex < rComponentGroup.m_printComponents.GetSize() - 1) ? rComponentGroup.m_printComponents[nComponentIndex + 1].m_nProductPartIndex : nCurProductPartIndex);
			if (nNextProductPartIndex != nCurProductPartIndex)
			{
				BOOL bShowLine = TRUE;
				if (m_bShowUnprinted)
				{
					CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nCurProductPartIndex);
					if ( (rProductPart.GetRealQuantity() - rProductPart.GetDesiredQuantity()) >= 0)
						bShowLine = FALSE;
				}
				if (m_bShowPrintedOnly)
				{
					CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(nCurProductPartIndex);
					if ( (rProductPart.GetRealQuantity() - rProductPart.GetDesiredQuantity()) < 0)
						bShowLine = FALSE;
				}
				//if (bShowLine)
				//{
				//	pDC->MoveTo(rcRow.left,  rcRow.top);
				//	pDC->LineTo(rcRow.right, rcRow.top);
				//	rcRow.OffsetRect(0, 10);
				//}
			}
		}
		if (bDrawGroupHeader)
		{
			CRect rcHeader = rComponentGroup.GetDisplayBox(); 
			rcHeader.bottom = rcBox.bottom - nTop + 20;
			rComponentGroup.SetDisplayBox(rcHeader);
		}
	}

	linePen.DeleteObject();

	return rcBox;
}

CRect CPrintComponentsView::DrawIncompleteProducts(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	return rcBox;
}

BOOL CPrintComponentsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_toolBar.m_pToolTip)            
		m_toolBar.m_pToolTip->RelayEvent(pMsg);	

	if (pMsg->message == WM_LBUTTONDOWN)
	{
		return CScrollView::PreTranslateMessage(pMsg);
	}

	return CScrollView::PreTranslateMessage(pMsg);
}

CPrintComponentsGroup* g_pPrintComponentsGroup = NULL;

void CPrintComponentsView::OnLButtonDown(UINT nFlags, CPoint point)
{
	g_pPrintComponentsGroup = &GetActComponent().GetPrintComponentsGroup();

	m_DisplayList.OnLButtonDown(point);

	CScrollView::OnLButtonDown(nFlags, point);
}

void CPrintComponentsView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

void CPrintComponentsView::OnMouseLeave()
{
	m_DisplayList.OnMouseLeave();

	CScrollView::OnMouseLeave();
}

BOOL CPrintComponentsView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// The default call of OnMouseWheel causes an assertion
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion

//	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}

BOOL CPrintComponentsView::OnSelectPageSection(CDisplayItem* pDI, CPoint point, LPARAM)
{
	CPrintSheetView*	 pView	= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CNewPrintSheetFrame* pFrame = (pView) ? (CNewPrintSheetFrame*)pView->GetParentFrame() : NULL;
	if ( ! pFrame)
		return FALSE;

	HandleOnSelectComponent(pDI, point);
	ResizeDetailsView();

	if (IsOpenDlgFoldSchemeSelector())
		return FALSE;

	CDlgSelectFoldSchemePopup* pDlg = GetDlgFoldSchemeSelector();
	if ( ! pDlg)
		return FALSE;

	CRect rcTool = pDI->m_BoundingRect; 
	if (rcTool.PtInRect(point))
	{
		CRect rcTarget = pDI->m_BoundingRect; rcTarget.left += 60; ClientToScreen(rcTarget);
		pDlg->m_ptInitialPos = CPoint(0, 0);
		pDlg->m_bVisible	 = FALSE;
		pDlg->Create(IDD_SELECT_FOLDSCHEME_POPUP, pFrame);
		CRect rcDlg; pDlg->GetClientRect(rcDlg);
		rcTarget.right = rcTarget.left + rcDlg.Width(); rcTarget.bottom = rcTarget.top + rcDlg.Height();
		CRect rcClient; GetClientRect(rcClient); ClientToScreen(rcClient);
		if (rcTarget.bottom > rcClient.bottom)
		{
			rcTarget.top = rcClient.bottom - rcDlg.Height(); 
			rcTarget.bottom = rcTarget.top + rcDlg.Height();
		}
		pDlg->MoveWindow(rcTarget);
		pDlg->m_bVisible = TRUE;
		pDlg->ShowWindow(SW_SHOW);
	}

	return TRUE;
}

BOOL CPrintComponentsView::OnDeselectPageSection(CDisplayItem* pDI, CPoint /*point*/, LPARAM)
{
	return TRUE;
}

BOOL CPrintComponentsView::OnMouseMovePageSection(CDisplayItem* pDI, CPoint point, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 

				CRect rcRect = pDI->m_BoundingRect;
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcRect, RGB(205,182,156));

				CImage img;
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_DROPDOWN));
				CRect rcImg = rcRect; rcImg.left += 60; 
				img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top + 6, img.GetWidth(), img.GetHeight(), WHITE);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintComponentsView::OnActivatePageSection(CDisplayItem* pDI, CPoint point, LPARAM)
{
	pDI->m_nState = CDisplayItem::Selected;

	OnSelectPageSection(pDI, point, 0);

	return TRUE;
}

BOOL CPrintComponentsView::OnMouseMoveFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 

				CRect rcRect = pDI->m_BoundingRect; 
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcRect, RGB(205,182,156));

				CRect rcTools = pDI->m_AuxRect;  

				int nNumSheets = 0;
				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if (pDoc)
				{
					CPrintComponent&	rComponent	 = pDoc->m_printComponentsList.GetComponent((int)pDI->m_pData);
					CBoundProductPart*	pProductPart = rComponent.GetBoundProductPart();
					if (pProductPart)
					{
						int nNumUnfoldedPages = pProductPart->GetNumUnfoldedPages();
						if (nNumUnfoldedPages > 0)
						{
							CFoldSheetLayer* pLayer = rComponent.GetFoldSheetLayer();
							nNumSheets = (pLayer) ? nNumUnfoldedPages / pLayer->GetRealNumPages() : 0;
						}
					}
				}
				if (nNumSheets < 1)
					rcTools.right -= 20;

				pDC->FillSolidRect(rcTools, RGB(246,240,235));

				CImage img;
				CRect rcImg = rcTools; 
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_ARROW_RIGHT_HOT));
				img.TransparentBlt(pDC->m_hDC, rcImg.left + 3, rcImg.top + 5, img.GetWidth(), img.GetHeight(), WHITE);

				rcImg.left += 20;
				img.Destroy();
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MINUS_1));
				img.TransparentBlt(pDC->m_hDC, rcImg.left + 3, rcImg.top + 5, img.GetWidth(), img.GetHeight(), WHITE);

				rcImg.left += 18;
				if (nNumSheets >= 1)
				{
					img.Destroy();
					img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_PLUS_1));
					img.TransparentBlt(pDC->m_hDC, rcImg.left + 3, rcImg.top + 5, img.GetWidth(), img.GetHeight(), WHITE);
				}

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}


extern BOOL g_bLockPrintGroupList;

BOOL CPrintComponentsView::OnSelectFoldSheet(CDisplayItem* pDI, CPoint point, LPARAM lParam)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CRect rcDelete = pDI->m_AuxRect;
	rcDelete.left += 20; rcDelete.right = rcDelete.left + 20; rcDelete.bottom = rcDelete.top + 25;

	if (rcDelete.PtInRect(point))
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
			pView->UndoSave();

		BOOL		  bRemoved = FALSE;
		CDisplayItem* pDI	   = m_DisplayList.GetFirstSelectedItem();
		while (pDI)
		{
			CPrintComponent component = pDoc->m_printComponentsList.GetComponent((int)pDI->m_pData);
			if ( ! component.IsNull())
			{
				pDoc->m_Bookblock.RemoveFoldSheet(component.m_nFoldSheetIndex);
				bRemoved = TRUE;
			}
			pDI = m_DisplayList.GetNextSelectedItem(pDI);
		}
		if (bRemoved)
		{
			pDoc->m_Bookblock.Reorganize();
			pDoc->SetModifiedFlag(TRUE, NULL, TRUE);

			BOOL bUpdate = TRUE;
			if (IsOpenDlgComponentImposer())
			{
				GetDlgComponentImposer()->InitData(TRUE);
				bUpdate = FALSE;
			}
			if (bUpdate)
			{
				theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL, TRUE);
				theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
			}
			theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView), NULL, 0, NULL, TRUE);
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
		}
	}
	else
	{
		int nNumSheets = 0;
		CPrintComponent		component	 = pDoc->m_printComponentsList.GetComponent((int)pDI->m_pData);
		CBoundProductPart*	pProductPart = component.GetBoundProductPart();
		if (pProductPart)
		{
			int nNumUnfoldedPages = pProductPart->GetNumUnfoldedPages();
			if (nNumUnfoldedPages > 0)
			{
				CFoldSheetLayer* pLayer = component.GetFoldSheetLayer();
				nNumSheets = (pLayer) ? nNumUnfoldedPages / pLayer->GetRealNumPages() : 0;
			}
		}
		if (nNumSheets >= 1)
		{
			CRect rcPlus = pDI->m_AuxRect; rcPlus.left = rcPlus.right - 20;
			if (rcPlus.PtInRect(point))
			{
				CFoldSheet* pFoldSheet = component.GetFoldSheet();
				if (pFoldSheet)
				{
					CFoldSheet newFoldSheet = *pFoldSheet;
					newFoldSheet.m_nProductPartIndex = component.m_nProductPartIndex;
					int nNumPagesImposed = pDoc->m_Bookblock.ImposePageSection(pProductPart, newFoldSheet, pFoldSheet->GetNumPages(), 1);

					pDoc->m_printComponentsList.Analyze();
				}
			}
			else
				HandleOnSelectComponent(pDI, point);
		}
		else
			HandleOnSelectComponent(pDI, point);
	}

	ResizeDetailsView();

	return TRUE;
}

BOOL CPrintComponentsView::OnDeselectFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM)
{
	HandleOnDeselectComponent(pDI);

	return TRUE;
}

BOOL CPrintComponentsView::OnActivateFoldSheet(CDisplayItem* pDI, CPoint point, LPARAM)
{
	pDI->m_nState = CDisplayItem::Selected;

	OnSelectFoldSheet(pDI, point, 0);

	return TRUE;
}

BOOL CPrintComponentsView::OnSelectFlatProduct(CDisplayItem* pDI, CPoint point, LPARAM lParam)
{
	HandleOnSelectComponent(pDI, point);
	ResizeDetailsView();

	return TRUE;
}

BOOL CPrintComponentsView::OnDeselectFlatProduct(CDisplayItem* pDI, CPoint /*point*/, LPARAM)
{
	HandleOnDeselectComponent(pDI);

	return TRUE;
}

BOOL CPrintComponentsView::OnActivateFlatProduct(CDisplayItem* pDI, CPoint point, LPARAM)
{
	pDI->m_nState = CDisplayItem::Selected;

	OnSelectFlatProduct(pDI, point, 0);

	return TRUE;
}

BOOL CPrintComponentsView::OnMouseMoveFlatProduct(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 

				CRect rcRect = pDI->m_BoundingRect; 
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcRect, RGB(205,182,156));

				CRect rcTools = pDI->m_AuxRect;  
				pDC->FillSolidRect(rcTools, RGB(246,240,235));

				CImage img;
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_ARROW_RIGHT_HOT));
				CRect rcImg = rcTools; 
				img.TransparentBlt(pDC->m_hDC, rcImg.left + 3, rcImg.CenterPoint().y - img.GetHeight()/2, img.GetWidth(), img.GetHeight(), WHITE);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

void CPrintComponentsView::HandleOnSelectComponent(CDisplayItem* pDI, CPoint point)
{
	if (m_DisplayList.m_bIsInsideDragging)
		return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheetNavigationView*		pNavigationView				 = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	CPrintSheetViewStrippingData*	pPrintSheetViewStrippingData = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	CPrintSheet*					pPrevPrintSheet				 = GetActPrintSheet();
	CPrintComponent					component					 = pDoc->m_printComponentsList.GetComponent((int)pDI->m_pData);
	CPrintSheet*					pPrintSheet					 = component.GetFirstPrintSheet();

	if (IsOpenDlgFoldSchemeSelector())
	{
		if (component.m_nType == CPrintComponent::PageSection)
		{
			GetDlgFoldSchemeSelector()->InitSchemeList();
			GetDlgFoldSchemeSelector()->InitData();
		}
		else
			GetDlgFoldSchemeSelector()->DestroyWindow();
	}

	CRect rcImpose = pDI->m_AuxRect; rcImpose.right = rcImpose.left + 30;
	if (rcImpose.PtInRect(point))
	{
		if (pPrintSheetViewStrippingData)
		{
			CDisplayItem* pDI = pPrintSheetViewStrippingData->m_layoutDataWindow.m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CLayoutDataWindow, OpenComponentImposer));
			if ( ! pPrintSheetViewStrippingData->m_layoutDataWindow.m_dlgComponentImposer.m_hWnd)
				pPrintSheetViewStrippingData->m_layoutDataWindow.OnSelectOpenComponentImposer(pDI, CPoint(0,0), 0);
		}
	}
	else
	{
		if ( ! pPrintSheet)		// unplaced component
		{
			pPrintSheet = component.GetSuitablePrintSheet();
			if ( ! pPrintSheet)
				pPrintSheet = component.GetSuitableEmptyPrintSheet();
		}

		if ( (pPrevPrintSheet != pPrintSheet) && pPrintSheet)
		{
			if (pNavigationView)
			{
				if ( ! pNavigationView->SetActPrintSheet(pPrintSheet, TRUE))	// no update to avoid frame CNewPrintSheetFrame::UpdateFrameLayout(), because if that happens, this view will be invalid
				{
					CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
					if (pPrintSheetView)
					{
						pPrintSheetView->m_DisplayList.Invalidate();
						pPrintSheetView->Invalidate();
						pPrintSheetView->UpdateWindow();
					}
				}

				Invalidate();	// when sheet changed -> complete invalidate needed because all components belonging to the new sheet should be marked
			}
		}
		else
			if (pNavigationView)
				pNavigationView->InvalidateSheetFrame(pPrintSheet);	
	}
}

void CPrintComponentsView::HandleOnDeselectComponent(CDisplayItem* pDI)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheetNavigationView*	pNavigationView	= (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	CPrintComponent				component		= pDoc->m_printComponentsList.GetComponent((int)pDI->m_pData);
	CPrintSheet*				pPrintSheet		= component.GetFirstPrintSheet();
	if (pNavigationView)
		pNavigationView->InvalidateSheetFrame(pPrintSheet);	
}

void CPrintComponentsView::ResizeDetailsView()
{
	CPrintComponentsDetailView*	pDetailsView = (CPrintComponentsDetailView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsDetailView));
	if (pDetailsView)
		pDetailsView->ResizeView();

	EnsureVisibleComponent(GetActComponent());
	m_toolBar.MoveButtons(CPoint(10,0));
}

BOOL CPrintComponentsView::OnStatusChanged()
{
	CPrintComponent					component			= GetActComponent();
	CPrintSheet*					pPrintSheet			= component.GetFirstPrintSheet();
	CPrintSheetNavigationView*		pNavigationView		= (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	CPrintSheetViewStrippingData*	pStrippingDataView	= (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	CPrintSheetView*				pPrintSheetView		= (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintComponentsDetailView*		pDetailsView		= (CPrintComponentsDetailView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsDetailView));

	//if (g_pPrintComponentsGroup != &component.GetPrintComponentsGroup())
	//{
	//	Invalidate();
	//}

	if (pStrippingDataView)
	{
		pStrippingDataView->Invalidate();
		pStrippingDataView->UpdateWindow();
	}

	if (pPrintSheetView)
	{
		BOOL bPrintSheetViewUpdated = FALSE;
		if ( ! IsOpenDlgComponentImposer())
		{
			SelectPrintSheetViewDisplayItems(pPrintSheetView, component);
			pPrintSheetView->UpdateWindow();
		}
		else
			GetDlgComponentImposer()->InitData(FALSE);
	}

	if (pDetailsView)
	{
		pDetailsView->Invalidate();
		pDetailsView->UpdateWindow();
	}

	if (pNavigationView)
	{
		pNavigationView->m_DisplayList.Invalidate();
		pNavigationView->UpdateWindow();
	}

	if (component.IsNull())
	{
		ResizeDetailsView();
	}

	return TRUE;
}

void CPrintComponentsView::SelectPrintSheetViewDisplayItems(CPrintSheetView* pPrintSheetView, CPrintComponent& rComponent)
{
	if ( ! pPrintSheetView)
		return;

		//// reset layout objects
	CDisplayItem* pDI = pPrintSheetView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	while (pDI)
	{
		pPrintSheetView->m_DisplayList.DeselectItem(*pDI, TRUE);
		pDI = pPrintSheetView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	}
	//// reset foldsheets
	pDI = pPrintSheetView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	while (pDI)
	{
		pPrintSheetView->m_DisplayList.DeselectItem(*pDI, TRUE);
		pDI = pPrintSheetView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
	}

	if (rComponent.m_nType == CPrintComponent::Flat)
	{
		CFlatProduct* pFlatProductComp = rComponent.GetFlatProduct();
		CDisplayItem* pDI = pPrintSheetView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		while (pDI)
		{
			CLayoutObject* pLayoutObject = (CLayoutObject*)pDI->m_pData;
			if (pLayoutObject->IsFlatProduct())
			{
				if (&pLayoutObject->GetCurrentFlatProduct((CPrintSheet*)pDI->m_pAuxData) == pFlatProductComp)
				{
					pDI->m_nState = CDisplayItem::Selected;
					pPrintSheetView->m_DisplayList.InvalidateItem(pDI);
				}
			}
			pDI = pPrintSheetView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
		}
	}
	else
	{
		if (rComponent.m_nType == CPrintComponent::FoldSheet)
		{
			CFoldSheet* pFoldSheetComp = rComponent.GetFoldSheet();
			CDisplayItem* pDI = pPrintSheetView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
			while (pDI)
			{
				CPrintSheet* pActPrintSheet = (CPrintSheet*)pDI->m_pAuxData;
				CFoldSheet* pFoldSheet = (pActPrintSheet) ? pActPrintSheet->GetFoldSheet((int)pDI->m_pData) : NULL;
				if (pFoldSheet == pFoldSheetComp)
				{
					pDI->m_nState = CDisplayItem::Selected;
					pPrintSheetView->m_DisplayList.InvalidateItem(pDI);
				}
				pDI = pPrintSheetView->m_DisplayList.GetNextItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet));
			}
		}
	}
}

CPrintComponent CPrintComponentsView::GetActComponent()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CPrintComponent();
	if (pDoc->m_printComponentsList.GetSize() <= 0)
		return CPrintComponent();

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem();
	if (pDI)
		return pDoc->m_printComponentsList.GetComponent((int)pDI->m_pData);
	else
		return CPrintComponent();

	//pDI = m_DisplayList.GetFirstItem();
	//while (pDI)
	//{
	//	if ((int)pDI->m_pData == 0)
	//		pDI->m_nState = CDisplayItem::Selected;
	//	else
	//		pDI->m_nState = CDisplayItem::Normal;
	//	m_DisplayList.InvalidateItem(pDI);
	//	pDI = m_DisplayList.GetNextItem(pDI);
	//}

	//return pDoc->m_printComponentsList.GetComponent(0);
}

void CPrintComponentsView::SetActComponent(CPrintComponent& rComponent)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nIndex = rComponent.GetIndex();

	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if (nIndex == (int)pDI->m_pData)
			pDI->m_nState = CDisplayItem::Selected;
		else
			pDI->m_nState = CDisplayItem::Normal;

		m_DisplayList.InvalidateItem(pDI);

		pDI = m_DisplayList.GetNextItem(pDI);
	}

	EnsureVisibleComponent(rComponent);

	ResizeDetailsView();
}

CPrintSheet* CPrintComponentsView::GetActPrintSheet()
{
	CPrintSheetNavigationView* pView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if ( ! pView)
		return NULL;

	return pView->GetActPrintSheet();
}

void CPrintComponentsView::SetActPrintSheet(CPrintSheet* pActPrintSheet, BOOL bNoUpdate)
{
	CPrintSheetNavigationView* pView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
	if ( ! pView)
		return;

	pView->m_DisplayList.Invalidate();

	pView->SetActPrintSheet(pActPrintSheet, bNoUpdate);
}

BOOL CPrintComponentsView::IsOpenDlgFoldSchemeSelector()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	return pFrame->IsOpenDlgFoldSchemeSelector();
}

CDlgSelectFoldSchemePopup* CPrintComponentsView::GetDlgFoldSchemeSelector()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return NULL;
	return pFrame->GetDlgFoldSchemeSelector();
}

CDlgComponentImposer* CPrintComponentsView::GetDlgComponentImposer()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	return pFrame->GetDlgComponentImposer();
}

BOOL CPrintComponentsView::IsOpenDlgComponentImposer()
{
	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! pFrame)
		return FALSE;
	return pFrame->IsOpenDlgComponentImposer();
}

void CPrintComponentsView::OnShowUnprinted()
{
	m_bShowUnprinted = ! m_bShowUnprinted;

	Invalidate();
	m_DisplayList.Invalidate();
	OnUpdate(NULL, 0, NULL);
	UpdateWindow();
	ResizeDetailsView();
}

void CPrintComponentsView::OnClearComponentList()
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	// save modified doc to new path
	CDocTemplate* pTemplate = pDoc->GetDocTemplate();
	CString strNewFilePath = pDoc->GetTitle();
	ASSERT(pTemplate != NULL);

	if ( ! theApp.DoPromptFileName(strNewFilePath, AFX_IDS_SAVEFILE, OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_NOCHANGEDIR | OFN_OVERWRITEPROMPT | OFN_ENABLESIZING, FALSE, pTemplate))
		return;       

	pDoc->OnSaveDocument(strNewFilePath);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pPrintSheetView)
		pPrintSheetView->UndoSave();

	//// remove printed products
	pDoc->RemovePlacedProducts(); 

	CString strTitle = pDoc->GetTitle();
	theApp.RemoveExtension(strTitle);
	strTitle += _T("_1.job");
	pDoc->SetTitle(strTitle);

	Invalidate();
	UpdateWindow();
}

void CPrintComponentsView::OnUpdateShowUnprinted(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_bShowUnprinted);
}

void CPrintComponentsView::OnUpdateSaveUnprinted(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPrintComponentsView::EnsureVisibleComponent(CPrintComponent& rComponent)
{
	CDisplayItem* pDI = m_DisplayList.GetItemFromData((void*)rComponent.GetIndex());
	if ( ! pDI)
		return;

	if ( ! pDI->m_bIsVisible)	
	{
		Invalidate();
		OnVScroll(SB_THUMBTRACK, pDI->m_BoundingRect.top - m_nToolBarHeight - m_nHeaderHeight + GetScrollPos(SB_VERT), NULL);
	}
	else
	{
		Invalidate();
		UpdateWindow();
	}
}

void CPrintComponentsView::OnNcActivate(BOOL bActive)
{
	CScrollView::OnNcActivate(bActive);
}

void CPrintComponentsView::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{
	//if (bCalcValidRects)
	//{
	//	lpncsp->rgrc[0].bottom -= 150;//m_nStatusBarHeight;		
	//	lpncsp->rgrc[1].bottom -= 150;//m_nStatusBarHeight;		
	//	lpncsp->rgrc[2].bottom -= 150;//m_nStatusBarHeight;		
	//}

	CScrollView::OnNcCalcSize(bCalcValidRects, lpncsp);
}


void CPrintComponentsView::OnNcPaint()
{
	CScrollView::OnNcPaint();

	//CDC* pDC = GetWindowDC();

	//m_NcDisplayList.BeginRegisterItems(pDC);

	//CRect rcClient;
	//GetClientRect(rcClient); 
	//rcClient.top = rcClient.bottom; rcClient.bottom += m_nStatusBarHeight; 
	//CRect rcFrame;
	//GetWindowRect(rcFrame); 
	//rcClient.right = rcClient.left + rcFrame.Width();

	//BOOL bHasHorzBar, bHasVertBar;
	//CheckScrollBars(bHasHorzBar, bHasVertBar);	
	//if (bHasHorzBar)
	//	rcClient.top += GetSystemMetrics(SM_CYHSCROLL);

	//pDC->FillSolidRect(rcClient, RGB(242,242,250));

	//DrawStatusPart(pDC, rcClient, &m_NcDisplayList);

	//m_NcDisplayList.EndRegisterItems(pDC);
}

void CPrintComponentsView::OnNcLButtonDown(UINT nFlags, CPoint point)
{
	//m_NcDisplayList.OnLButtonDown(point, TRUE);

	CScrollView::OnLButtonDown(nFlags, point);
}

void CPrintComponentsView::OnNcMouseMove(UINT nHitTest, CPoint point)
{
	//m_NcDisplayList.OnMouseMove(point, TRUE);

	CScrollView::OnNcMouseMove(nHitTest, point);
}

void CPrintComponentsView::OnNcMouseLeave()
{
	//m_NcDisplayList.OnMouseLeave(TRUE);

	CScrollView::OnNcMouseLeave();
}


LRESULT CPrintComponentsView::OnNcHitTest(CPoint point)
{
	// TODO: F�gen Sie hier Ihren Meldungsbehandlungscode ein, und/oder benutzen Sie den Standard.

	return CScrollView::OnNcHitTest(point);
}

DROPEFFECT CPrintComponentsView::OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return DROPEFFECT_NONE;

	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	DROPEFFECT de = ((dwKeyState & MK_CONTROL) == MK_CONTROL) ? DROPEFFECT_COPY : DROPEFFECT_MOVE;

	CDisplayItem* pSelectedDI = m_DisplayList.GetFirstSelectedItem();
	while (pSelectedDI)
	{
		if ( (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FlatProduct))) || (pSelectedDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet))) )
		{
			pSelectedDI->m_dragSize  = CSize(40,40);
			pSelectedDI->m_dragPoint = point - pSelectedDI->m_dragSize;
			CRect rcFrame(pSelectedDI->m_dragPoint, pSelectedDI->m_dragSize);
			pSelectedDI->m_dragDropObject.DragInitialize(NULL, rcFrame, 255);

			CClientDC cdc(this);
			CDC memDC;
			memDC.CreateCompatibleDC(&cdc);
			pSelectedDI->m_dragDropObject.m_bmObject.CreateCompatibleBitmap(&cdc, rcFrame.Width(), rcFrame.Height());
			memDC.SelectObject(&pSelectedDI->m_dragDropObject.m_bmObject);

			pSelectedDI->m_dragDropObject.m_rcFrame = CRect(CPoint(point.x - rcFrame.Size().cx/2, point.y - rcFrame.Size().cy/2), rcFrame.Size());

			CPrintComponent component = pDoc->m_printComponentsList.GetComponent((int)pSelectedDI->m_pData);
			rcFrame.OffsetRect(-rcFrame.TopLeft());

			if (component.m_nType == CPrintComponent::Flat)
			{
				CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetProduct(component.m_nFlatProductIndex);
				rFlatProduct.DrawSheetPreview(&memDC, rcFrame, NULL, TRUE);
				pSelectedDI->m_dragDropObject.DragEnter(&cdc);
			}
			else
				if (component.m_nType == CPrintComponent::FoldSheet)
				{
					int			nLayerIndex = component.m_nFoldSheetLayerIndex;
					CFoldSheet* pFoldSheet  = pDoc->m_Bookblock.GetFoldSheetByIndex(component.m_nFoldSheetIndex);
					pFoldSheet->DrawSheetPreview(&memDC, rcFrame, nLayerIndex, 0, 0, TRUE);
					pSelectedDI->m_dragDropObject.DragEnter(&cdc);
				}

			memDC.DeleteDC();
		}

		pSelectedDI	= m_DisplayList.GetNextSelectedItem(pSelectedDI);
	}

	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		return DROPEFFECT_COPY;
	else 
		return DROPEFFECT_MOVE;
}

DROPEFFECT CPrintComponentsView::OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	DROPEFFECT de;
	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		de = DROPEFFECT_MOVE;

	m_DisplayList.GiveDragFeedback(de, point);

	return de;
}

BOOL CPrintComponentsView::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point) 
{
	// clean up focus rect
	m_DisplayList.RemoveDragFeedback();

	if (dropEffect == DROPEFFECT_NONE)
		return FALSE;

	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return FALSE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	return FALSE;
}

void CPrintComponentsView::OnDragLeave() 
{
	m_DisplayList.RemoveDragFeedback();
}

BOOL CPrintComponentsView::OnFilterSnapGridFoldSheet(CDisplayItem* pDI)
{
    return FALSE;
}

void CPrintComponentsView::OnGetSnapCoordsFoldSheet(CDisplayItem* pDI, unsigned nSnapPosition, float* x, float* y)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintComponent component = pDoc->m_printComponentsList.GetComponent((int)pDI->m_pData);
	if (component.IsNull())
		return;
	CFoldSheet* pFoldSheet = component.GetFoldSheet();
	if ( ! pFoldSheet)
		return;

	int nLayerIndex = component.m_nFoldSheetLayerIndex;
	float fLeft = 0.0f, fBottom = 0.0f, fRight, fTop, fWidth, fHeight;
	pFoldSheet->GetCutSize(nLayerIndex, fWidth, fHeight);
	fRight = fWidth; fTop = fHeight;

    switch (nSnapPosition & ~CDisplayItem::snapOutside)
    {
    case CDisplayItem::snapTopLeft:		*x = fLeft;				*y = fTop;						break;
    case CDisplayItem::snapTopRight:	*x = fRight;			*y = fTop;						break;
    case CDisplayItem::snapBottomRight: *x = fRight;			*y = fBottom;					break;
    case CDisplayItem::snapBottomLeft:	*x = fLeft;				*y = fBottom;					break;
    case CDisplayItem::snapTop:			*x = fLeft + fWidth/2;	*y = fTop;						break;
    case CDisplayItem::snapRight:		*x = fRight;			*y = fBottom + fHeight/2;		break;
    case CDisplayItem::snapBottom:		*x = fLeft + fWidth/2;	*y = fBottom;					break;
    case CDisplayItem::snapLeft:		*x = fLeft;				*y = fBottom + fHeight/2;		break;
    case CDisplayItem::snapMiddle:		*x = fLeft + fWidth/2;	*y = fBottom + fHeight/2;		break;
    default:							*x = fLeft;				*y = fTop;						break;
    }
}

BOOL CPrintComponentsView::OnFilterSnapGridFlatProduct(CDisplayItem* pDI)
{
    return FALSE;
}

void CPrintComponentsView::OnGetSnapCoordsFlatProduct(CDisplayItem* pDI, unsigned nSnapPosition, float* x, float* y)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintComponent component = pDoc->m_printComponentsList.GetComponent((int)pDI->m_pData);
	if (component.IsNull())
		return;
	CFlatProduct* pFlatProduct = component.GetFlatProduct();
	if ( ! pFlatProduct)
		return;

	float fLeft = 0.0f, fBottom = 0.0f, fRight, fTop, fWidth, fHeight;
	pFlatProduct->GetCutSize(fWidth, fHeight);
	fRight = fWidth; fTop = fHeight;

    switch (nSnapPosition & ~CDisplayItem::snapOutside)
    {
    case CDisplayItem::snapTopLeft:		*x = fLeft;				*y = fTop;						break;
    case CDisplayItem::snapTopRight:	*x = fRight;			*y = fTop;						break;
    case CDisplayItem::snapBottomRight: *x = fRight;			*y = fBottom;					break;
    case CDisplayItem::snapBottomLeft:	*x = fLeft;				*y = fBottom;					break;
    case CDisplayItem::snapTop:			*x = fLeft + fWidth/2;	*y = fTop;						break;
    case CDisplayItem::snapRight:		*x = fRight;			*y = fBottom + fHeight/2;		break;
    case CDisplayItem::snapBottom:		*x = fLeft + fWidth/2;	*y = fBottom;					break;
    case CDisplayItem::snapLeft:		*x = fLeft;				*y = fBottom + fHeight/2;		break;
    case CDisplayItem::snapMiddle:		*x = fLeft + fWidth/2;	*y = fBottom + fHeight/2;		break;
    default:							*x = fLeft;				*y = fTop;						break;
    }
}
