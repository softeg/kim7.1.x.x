// DlgRemoveControlMarks.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgRemoveControlMarks.h"


// CDlgRemoveControlMarks-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgRemoveControlMarks, CDialog)

CDlgRemoveControlMarks::CDlgRemoveControlMarks(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgRemoveControlMarks::IDD, pParent)
{

}

CDlgRemoveControlMarks::~CDlgRemoveControlMarks()
{
}

void CDlgRemoveControlMarks::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgRemoveControlMarks, CDialog)
	ON_BN_CLICKED(IDC_REMOVE_SELECTED_MARKS, &CDlgRemoveControlMarks::OnBnClickedRemoveSelectedMarks)
END_MESSAGE_MAP()


// CDlgRemoveControlMarks-Meldungshandler

BOOL CDlgRemoveControlMarks::OnInitDialog()
{
	CDialog::OnInitDialog();

	if ( ! m_strSelectedMarkName.IsEmpty())
	{
		CString strFormat, strTextOut;
		GetDlgItem(IDC_MSG_REMOVE_MARKS)->GetWindowText(strFormat);
		strTextOut.Format(strFormat, m_strSelectedMarkName);
		GetDlgItem(IDC_MSG_REMOVE_MARKS)->SetWindowText(strTextOut);
	}
	//else
	//	GetDlgItem(IDC_REMOVE_SELECTED_MARKS)->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgRemoveControlMarks::OnBnClickedRemoveSelectedMarks()
{
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
}
