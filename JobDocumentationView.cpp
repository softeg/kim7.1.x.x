// JobDocumentationView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "JobDocumentationSettingsView.h"
#include "JobDocumentationView.h"
#include "PrintSheetPlanningView.h"
#include "PrintSheetView.h"
#include "PrintJobDocumentation.h"
#include "GraphicComponent.h"


// CJobDocumentationView

IMPLEMENT_DYNCREATE(CJobDocumentationView, CScrollView)

CJobDocumentationView::CJobDocumentationView()
{
    m_DisplayList.Create(this, FALSE, DISPLAY_LIST_TYPE(CJobDocumentationView)); 
	m_nNumPages = 0;
	m_docSize = CSize(1000,1000);
	m_rcPrevClientRect.SetRectEmpty();
}

CJobDocumentationView::~CJobDocumentationView()
{
}

BEGIN_MESSAGE_MAP(CJobDocumentationView, CScrollView)
    ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
    ON_WM_KEYDOWN()
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()


// CJobDocumentationView-Zeichnung

CSize CJobDocumentationView::CalcDocSize()
{
	m_nNumPages = 0;
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CSize sizeDoc = Draw(&dc);

	dc.DeleteDC();

	sizeDoc += CSize(10, 20);

	return sizeDoc;
}

void CJobDocumentationView::OnDraw(CDC* pDC)
{
	Draw(pDC, &m_DisplayList);
}

CSize CJobDocumentationView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	CSize docSize = CSize(1000,1000);

	CImpManDoc* pDoc = (CImpManDoc*)GetDocument();
	if ( ! pDoc)
		return docSize;
	CJobDocumentationSettingsView* pView = (CJobDocumentationSettingsView*)theApp.GetView(RUNTIME_CLASS(CJobDocumentationSettingsView));
	if ( ! pView)
		return docSize;

	CRect rcClient;	GetClientRect(rcClient);

	CPrintJobDocumentation printJob;
	CRect rcRealPage(CPoint(0,0), printJob.GetPageSize());
	CRect rcDstPage (CPoint(10,10), CSize(rcRealPage.Width() / 100, rcRealPage.Height() / 100));	// screen page in pixels

	if (rcRealPage.IsRectNull())
		return docSize;

	float fPageSideRatio = (float)rcRealPage.Height() / (float)rcRealPage.Width();
	switch (pView->m_nZoomState)
	{
	case CJobDocumentationSettingsView::ZoomMultiPage:	break;
	case CJobDocumentationSettingsView::ZoomPageFit:	rcDstPage.bottom = rcClient.bottom - 20; rcDstPage.right = rcDstPage.left + (int)((float)rcDstPage.Height() / fPageSideRatio + 0.5f); 
														if (rcDstPage.right > rcClient.right)
														{
															rcDstPage.right = rcClient.right - 10; rcDstPage.bottom = rcDstPage.top + (int)((float)rcDstPage.Width() * fPageSideRatio + 0.5f); 
														}
														break;
	case CJobDocumentationSettingsView::ZoomPageWidth:	rcDstPage.right = rcClient.right - 10; rcDstPage.bottom = rcDstPage.top + (int)((float)rcDstPage.Width() * fPageSideRatio + 0.5f);
														break;
	}

	CFont font;
	font.CreateFont(13, 0, 0, 0, FW_NORMAL,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(LIGHTGRAY);
	pDC->SetTextColor(BLACK);
	CString string;

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	CGraphicComponent gp;
	int nPageNum = 1;

	CRect	rcText;
	CString strPage;
	//if (pDoc->m_printGroupList.HasBoundedProductGroup())
	//{
	//	///////// bookblock
	//	gp.DrawShadowFrame(pDC, rcDstPage, 5);
	//	pDC->FillSolidRect(rcDstPage, WHITE);
	//	if (pDisplayList)
	//		if (pView->m_nZoomState == CJobDocumentationSettingsView::ZoomMultiPage)
	//			pDisplayList->RegisterItem(pDC, rcDstPage, rcDstPage, (void*)nPageNum, DISPLAY_ITEM_REGISTRY(CJobDocumentationView, PrintPage), NULL, CDisplayItem::ForceRegister); 

	//	rcText = rcDstPage; rcText.top = rcText.bottom + 5; rcText.bottom += 25;
	//	strPage.LoadString(IDS_OBJECTTYPE_PAGE);
	//	string.Format(_T("%s %d"), strPage, nPageNum);
	//	pDC->DrawText(string, rcText, DT_BOTTOM | DT_CENTER);

	//	pDC->m_bPrinting = TRUE;
	//	rcDstPage += pDC->GetViewportOrg();
	//	printJob.PrintBookBlock(pDC, nPageNum, rcRealPage, rcDstPage);
	//	rcDstPage -= pDC->GetViewportOrg();
	//	pDC->m_bPrinting = FALSE;

	//	rcDstPage.OffsetRect(rcDstPage.Width() + 20, 0);
	//	if (rcDstPage.right > rcClient.right)
	//		rcDstPage.OffsetRect(-(rcDstPage.left - 10), rcDstPage.Height() + 40);

	//	nPageNum++;


	//	/////////// product parts 
	//	gp.DrawShadowFrame(pDC, rcDstPage, 5);
	//	pDC->FillSolidRect(rcDstPage, WHITE);
	//	if (pDisplayList)
	//		if (pView->m_nZoomState == CJobDocumentationSettingsView::ZoomMultiPage)
	//			pDisplayList->RegisterItem(pDC, rcDstPage, rcDstPage, (void*)nPageNum, DISPLAY_ITEM_REGISTRY(CJobDocumentationView, PrintPage), NULL, CDisplayItem::ForceRegister); 

	//	rcText = rcDstPage; rcText.top = rcText.bottom + 5; rcText.bottom += 25;
	//	strPage; strPage.LoadString(IDS_OBJECTTYPE_PAGE);
	//	string.Format(_T("%s %d"), strPage, nPageNum);
	//	pDC->DrawText(string, rcText, DT_BOTTOM | DT_CENTER);

	//	pDC->m_bPrinting = TRUE;
	//	rcDstPage += pDC->GetViewportOrg();
	//	printJob.PrintProductGroupInfoPages(pDC, nPageNum, rcRealPage, rcDstPage);
	//	rcDstPage -= pDC->GetViewportOrg();
	//	pDC->m_bPrinting = FALSE;

	//	docSize = rcDstPage.BottomRight();

	//	rcDstPage.OffsetRect(rcDstPage.Width() + 20, 0);
	//	if (rcDstPage.right > rcClient.right)
	//		rcDstPage.OffsetRect(-(rcDstPage.left - 10), rcDstPage.Height() + 40);

	//	nPageNum++;
	//}

	//if (pDoc->m_printGroupList.HasUnboundedProductGroup())
	//{
	//	gp.DrawShadowFrame(pDC, rcDstPage, 5);
	//	pDC->FillSolidRect(rcDstPage, WHITE);
	//	if (pView->m_nZoomState == CJobDocumentationSettingsView::ZoomMultiPage)
	//		m_DisplayList.RegisterItem(pDC, rcDstPage, rcDstPage, (void*)nPageNum, DISPLAY_ITEM_REGISTRY(CJobDocumentationView, PrintPage), NULL, CDisplayItem::ForceRegister); 

	//	CRect rcText = rcDstPage; rcText.top = rcText.bottom + 5; rcText.bottom += 25;
	//	CString strPage; strPage.LoadString(IDS_OBJECTTYPE_PAGE);
	//	string.Format(_T("%s %d"), strPage, nPageNum);
	//	pDC->DrawText(string, rcText, DT_BOTTOM | DT_CENTER);

	//	pDC->m_bPrinting = TRUE;
	//	rcDstPage += pDC->GetViewportOrg();
	//	printJob.PrintLabelComponentPages(pDC, nPageNum, rcRealPage, rcDstPage);
	//	rcDstPage -= pDC->GetViewportOrg();
	//	pDC->m_bPrinting = FALSE;

	//	rcDstPage.OffsetRect(rcDstPage.Width() + 20, 0);
	//	if (rcDstPage.right > rcClient.right)
	//		rcDstPage.OffsetRect(-(rcDstPage.left - 10), rcDstPage.Height() + 40);

	//	nPageNum++;
	//}

	//////// print sheet planning
	int nComponentIndex = 0;
	//do
	//{
	//	gp.DrawShadowFrame(pDC, rcDstPage, 5);
	//	pDC->FillSolidRect(rcDstPage, WHITE);
	//	if (pDisplayList)
	//		if (pView->m_nZoomState == CJobDocumentationSettingsView::ZoomMultiPage)
	//			pDisplayList->RegisterItem(pDC, rcDstPage, rcDstPage, (void*)nPageNum, DISPLAY_ITEM_REGISTRY(CJobDocumentationView, PrintPage), NULL, CDisplayItem::ForceRegister); 

	//	rcText = rcDstPage; rcText.top = rcText.bottom + 5; rcText.bottom += 25;
	//	strPage.LoadString(IDS_OBJECTTYPE_PAGE);
	//	string.Format(_T("%s %d"), strPage, nPageNum);
	//	pDC->DrawText(string, rcText, DT_BOTTOM | DT_CENTER);

	//	pDC->m_bPrinting = TRUE;
	//	rcDstPage += pDC->GetViewportOrg();
	//	nComponentIndex = printJob.PrintComponentPlanningPages(pDC, nPageNum, rcRealPage, rcDstPage, nComponentIndex);
	//	rcDstPage -= pDC->GetViewportOrg();
	//	pDC->m_bPrinting = FALSE;

	//	docSize = rcDstPage.BottomRight();

	//	rcDstPage.OffsetRect(rcDstPage.Width() + 20, 0);
	//	if (rcDstPage.right > rcClient.right)
	//		rcDstPage.OffsetRect(-(rcDstPage.left - 10), rcDstPage.Height() + 40);

	//	nPageNum++;
	//}
	//while (nComponentIndex >= 0);

	//////// print sheet list
	//CPrintSheet* pPrintSheet = NULL;
	//do
	//{
	//	gp.DrawShadowFrame(pDC, rcDstPage, 5);
	//	pDC->FillSolidRect(rcDstPage, WHITE);
	//	if (pDisplayList)
	//		if (pView->m_nZoomState == CJobDocumentationSettingsView::ZoomMultiPage)
	//		{
	//			CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDstPage, rcDstPage, (void*)nPageNum, DISPLAY_ITEM_REGISTRY(CJobDocumentationView, PrintPage), NULL, CDisplayItem::ForceRegister); 
	//			if (pDI)
	//				pDI->SetHighlightColor(RED);
	//		}

	//	rcText = rcDstPage; rcText.top = rcText.bottom + 5; rcText.bottom += 25;
	//	strPage.LoadString(IDS_OBJECTTYPE_PAGE);
	//	string.Format(_T("%s %d"), strPage, nPageNum);
	//	pDC->DrawText(string, rcText, DT_BOTTOM | DT_CENTER);

	//	pDC->m_bPrinting = TRUE;
	//	rcDstPage += pDC->GetViewportOrg();
	//	pPrintSheet = printJob.PrintPrintSheetListPages(pDC, nPageNum, rcRealPage, rcDstPage, pPrintSheet);
	//	rcDstPage -= pDC->GetViewportOrg();
	//	pDC->m_bPrinting = FALSE;

	//	rcDstPage.OffsetRect(rcDstPage.Width() + 20, 0);
	//	if (rcDstPage.right > rcClient.right)
	//		rcDstPage.OffsetRect(-(rcDstPage.left - 10), rcDstPage.Height() + 40);

	//	nPageNum++;
	//}
	//while (pPrintSheet);

	///////// paper list
	//gp.DrawShadowFrame(pDC, rcDstPage, 5);
	//pDC->FillSolidRect(rcDstPage, WHITE);
	//if (pDisplayList)
	//	if (pView->m_nZoomState == CJobDocumentationSettingsView::ZoomMultiPage)
	//	{
	//		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDstPage, rcDstPage, (void*)nPageNum, DISPLAY_ITEM_REGISTRY(CJobDocumentationView, PrintPage), NULL, CDisplayItem::ForceRegister); 
	//		if (pDI)
	//			pDI->SetHighlightColor(RED);
	//	}

	//rcText = rcDstPage; rcText.top = rcText.bottom + 5; rcText.bottom += 25;
	//strPage.LoadString(IDS_OBJECTTYPE_PAGE);
	//string.Format(_T("%s %d"), strPage, nPageNum);
	//pDC->DrawText(string, rcText, DT_BOTTOM | DT_CENTER);

	//pDC->m_bPrinting = TRUE;
	//rcDstPage += pDC->GetViewportOrg();
	//printJob.PrintPaperListPages(pDC, nPageNum, rcRealPage, rcDstPage);
	//rcDstPage -= pDC->GetViewportOrg();
	//pDC->m_bPrinting = FALSE;

	//rcDstPage.OffsetRect(rcDstPage.Width() + 20, 0);
	//if (rcDstPage.right > rcClient.right)
	//	rcDstPage.OffsetRect(-(rcDstPage.left - 10), rcDstPage.Height() + 40);

	//nPageNum++;


	/////// print layouts
	if (pDoc->m_PrintSheetList.m_Layouts.GetCount())
	{
		CPrintSheetList oldPrintSheetList;
		POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
		while (pos)
		{
			CPrintSheet& rSrcPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
			oldPrintSheetList.AddTail(rSrcPrintSheet);
			oldPrintSheetList.GetTail().LinkLayout(rSrcPrintSheet.GetLayout());
		}

		int		 nNextGroupIndex = 0;
		CLayout* pLayout		 = pDoc->m_PrintSheetList.GetFirstLayout();
		while (pLayout)
		{
			if (pLayout->IsEmpty())
				pLayout = pDoc->m_PrintSheetList.GetNextLayout(pLayout);
			else
			{
				gp.DrawShadowFrame(pDC, rcDstPage, 5);
				pDC->FillSolidRect(rcDstPage, WHITE);
				if (pDisplayList)
					if (pView->m_nZoomState == CJobDocumentationSettingsView::ZoomMultiPage)
					{
						CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDstPage, rcDstPage, (void*)nPageNum, DISPLAY_ITEM_REGISTRY(CJobDocumentationView, PrintPage), NULL, CDisplayItem::ForceRegister); 
						if (pDI)
							pDI->SetHighlightColor(RED);
					}

				CRect rcText = rcDstPage; rcText.top = rcText.bottom + 5; rcText.bottom += 25;
				CString strPage; strPage.LoadString(IDS_OBJECTTYPE_PAGE);
				string.Format(_T("%s %d"), strPage, nPageNum);
				pDC->DrawText(string, rcText, DT_BOTTOM | DT_CENTER);

				pDC->m_bPrinting = TRUE;
				rcDstPage += pDC->GetViewportOrg();
				pLayout = printJob.PrintLayoutPages(pDC, nPageNum, rcRealPage, rcDstPage, pLayout, nNextGroupIndex);
				rcDstPage -= pDC->GetViewportOrg();
				pDC->m_bPrinting = FALSE;

				docSize = rcDstPage.BottomRight();

				rcDstPage.OffsetRect(rcDstPage.Width() + 20, 0);
				if (rcDstPage.right > rcClient.right)
					rcDstPage.OffsetRect(-(rcDstPage.left - 10), rcDstPage.Height() + 40);

				nPageNum++;
			}
		}

		pos = oldPrintSheetList.GetHeadPosition();
		POSITION posDst = pDoc->m_PrintSheetList.GetHeadPosition();
		while (pos)
		{
			CPrintSheet& rSrcPrintSheet = oldPrintSheetList.GetNext(pos);
			CPrintSheet& rDstPrintSheet = pDoc->m_PrintSheetList.GetNext(posDst);
			rDstPrintSheet = rSrcPrintSheet;
			rDstPrintSheet.LinkLayout(rSrcPrintSheet.GetLayout());
			rDstPrintSheet.InitializeFlatProductTypeList();
		}
	}


	/////// print sheets
    if (pDoc->m_PrintSheetList.GetCount())
	{
		int			 nNextComponentIndex = 0;
		CPrintSheet* pPrintSheet		 = &pDoc->m_PrintSheetList.GetHead();
		while (pPrintSheet)
		{
			if (pPrintSheet->IsEmpty())
				pPrintSheet = pDoc->m_PrintSheetList.GetNextPrintSheet(pPrintSheet);

			if (pPrintSheet)
			{
				gp.DrawShadowFrame(pDC, rcDstPage, 5);
				pDC->FillSolidRect(rcDstPage, WHITE);
				if (pDisplayList)
					if (pView->m_nZoomState == CJobDocumentationSettingsView::ZoomMultiPage)
					{
						CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDstPage, rcDstPage, (void*)nPageNum, DISPLAY_ITEM_REGISTRY(CJobDocumentationView, PrintPage), NULL, CDisplayItem::ForceRegister); 
						if (pDI)
							pDI->SetHighlightColor(RED);
					}

				CRect rcText = rcDstPage; rcText.top = rcText.bottom + 5; rcText.bottom += 25;
				CString strPage; strPage.LoadString(IDS_OBJECTTYPE_PAGE);
				string.Format(_T("%s %d"), strPage, nPageNum);
				pDC->DrawText(string, rcText, DT_BOTTOM | DT_CENTER);

				pDC->m_bPrinting = TRUE;
				rcDstPage += pDC->GetViewportOrg();
				pPrintSheet = printJob.PrintSheetPages(pDC, nPageNum, rcRealPage, rcDstPage, pPrintSheet, nNextComponentIndex);
				rcDstPage -= pDC->GetViewportOrg();
				pDC->m_bPrinting = FALSE;

				docSize = rcDstPage.BottomRight();

				rcDstPage.OffsetRect(rcDstPage.Width() + 20, 0);
				if (rcDstPage.right > rcClient.right)
					rcDstPage.OffsetRect(-(rcDstPage.left - 10), rcDstPage.Height() + 40);

				nPageNum++;
			}
		}
	}

	font.DeleteObject();

	m_nNumPages = nPageNum - 1;

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return docSize;
}


// CJobDocumentationView-Diagnose

#ifdef _DEBUG
void CJobDocumentationView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CJobDocumentationView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CJobDocumentationView-Meldungshandler

void CJobDocumentationView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
	CScrollView::OnPrepareDC(pDC, pInfo);

	pDC->SetMapMode(MM_TEXT);
	pDC->SetWindowOrg(0, 0);
}

BOOL CJobDocumentationView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return DoPreparePrinting(pInfo);
}

void CJobDocumentationView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
}

void CJobDocumentationView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	CPrintJobDocumentation printJob;
	m_sizePrintPage.cx = (int)((float)printJob.GetPageSize().cx/100.0f + 0.5f);	// hundreds of millimeter
	m_sizePrintPage.cy = (int)((float)printJob.GetPageSize().cy/100.0f + 0.5f);	// hundreds of millimeter

	m_docSize = CalcDocSize();

	SetScrollSizes(MM_TEXT, m_docSize);

	m_DisplayList.Invalidate();
}

void CJobDocumentationView::OnSize(UINT nType, int cx, int cy) 
{
    CScrollView::OnSize(nType, cx, cy);

	CRect rcClientRect;
	GetClientRect(rcClientRect);
	if ( m_rcPrevClientRect != rcClientRect)
	{
		m_docSize = CalcDocSize();
		m_rcPrevClientRect = rcClientRect;
	}

	SetScrollSizes(MM_TEXT, m_docSize);
}

BOOL CJobDocumentationView::OnEraseBkgnd(CDC* pDC)
{
	//COLORREF crStartColor = RGB(216,228,243);
	//COLORREF crEndColor = RGB(174,200,232);
	//Graphics graphics(pDC->m_hDC);
	//Rect rc(rect.left, rect.top, rect.Width(), rect.Height());
	//LinearGradientBrush br(rc, Color::Color(GetRValue(crStartColor), GetGValue(crStartColor), GetBValue(crStartColor)), 
	//						   Color::Color(GetRValue(crEndColor),	 GetGValue(crEndColor),	  GetBValue(crEndColor)), 
	//						   90);

	//graphics.FillRectangle(&br, rc);

    CRect rect;
    GetClientRect(rect);
    CBrush brush(RGB(223,234,247));//RGB(230,230,230));
    pDC->FillRect(&rect, &brush);

    brush.DeleteObject();

	return TRUE;//CScrollView::OnEraseBkgnd(pDC);
}

void CJobDocumentationView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point); // pass message to display list

    CScrollView::OnLButtonDown(nFlags, point);
}

void CJobDocumentationView::OnKeyDown(UINT nChar, UINT /*nRepCnt*/, UINT /*nFlags*/)
{
    switch (nChar)
    {
    case VK_HOME: 
		OnVScroll(SB_TOP, 0, NULL);
        OnHScroll(SB_LEFT, 0, NULL);
        break;
    case VK_END: 
		OnVScroll(SB_BOTTOM, 0, NULL);
        OnHScroll(SB_RIGHT, 0, NULL);
        break;
    case VK_UP: 
		OnVScroll(SB_LINEUP, 0, NULL);
        break;
    case VK_DOWN: 
		OnVScroll(SB_LINEDOWN, 0, NULL);
        break;
    case VK_PRIOR: 
		OnVScroll(SB_PAGEUP, 0, NULL);
        break;
    case VK_NEXT: 
		OnVScroll(SB_PAGEDOWN, 0, NULL);
        break;
    case VK_LEFT: 
		OnHScroll(SB_LINELEFT, 0, NULL);
        break;
    case VK_RIGHT: 
		OnHScroll(SB_LINERIGHT, 0, NULL);
        break;
	default: 
		break;
    }

//	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CJobDocumentationView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// The default call of OnMouseWheel causes an assertion
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion
}
