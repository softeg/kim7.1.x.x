#if !defined(AFX_DLGLAYOUTOBJECTGEOMETRY_H__68AB1A03_B0D5_11D5_88B7_0040F68C1EF7__INCLUDED_)
#define AFX_DLGLAYOUTOBJECTGEOMETRY_H__68AB1A03_B0D5_11D5_88B7_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgLayoutObjectGeometry.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgLayoutObjectGeometry 

class CDlgLayoutObjectGeometry : public CDialog
{
// Konstruktion
public:
	CDlgLayoutObjectGeometry(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgLayoutObjectGeometry();

// Dialogfelddaten
	//{{AFX_DATA(CDlgLayoutObjectGeometry)
	enum { IDD = IDD_LAYOUTOBJECT_GEOMETRY };
	CSpinButtonCtrl	m_distanceYSpin;
	CSpinButtonCtrl	m_distanceXSpin;
	CSpinButtonCtrl	m_widthSpin;
	CSpinButtonCtrl	m_heightSpin;
	float	m_fObjectHeight;
	float	m_fObjectWidth;
	float	m_fDistanceX;
	float	m_fDistanceY;
	BOOL	m_bVariableWidth;
	BOOL	m_bVariableHeight;
	int		m_nMarkRefSelector;
	int		m_nMarkAnchor;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl*	m_pParent;
	BYTE							m_nHeadPosition;
	CBitmap							m_bmMarkOrientation;
	int								m_nMarkType;
	CObjectPivotControl				m_markPivotCtrl, m_anchorPivotCtrl;
	CString							m_strLeft, m_strRight, m_strBottom, m_strTop, m_strVertical, m_strHorizontal;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgLayoutObjectGeometry)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void			UpdatePositionControls();
	void			UpdateMarkAnchorSetting();
	void			UpdateMarkrefSelectors();
	void			OnSelchangeMarkRefSelector();
	int				GetReflinePosParamsID();
	int				GetMarkEdge();
	BOOL			HorizontalOnly();
	BOOL			VerticalOnly();
	void			CheckSwapReflinePosParams(CReflinePositionParams& newReflinePosParams);
	void			SwitchToEdge(CString strEdge);
	CLayoutSide*	GetLayoutObjectsBBox(float& fBBoxLeft, float& fBBoxBottom, float& fBBoxRight, float& fBBoxTop);
	void			UpdateHeadPositionControl();
	void			UpdateLayoutObject(CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet);
	void			LoadData();
	void			SaveData();
	void			Apply();
	void			OnLayoutDistanceChanged();
	void			OnLayoutReflineChanged();

protected:
	DECLARE_MESSAGE_MAP()
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgLayoutObjectGeometry)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposMarkOrientationSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnDeltaposDistancexSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposDistanceySpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangedPivot();
	afx_msg void OnWidthVariable();
	afx_msg void OnHeightVariable();
	afx_msg void OnMarkAnchorLines();
	afx_msg void OnMarkAnchorPages();
	afx_msg void OnMarkAnchorFoldSheets();
	afx_msg void OnMarkref_1();
	afx_msg void OnMarkref_2();
	afx_msg void OnMarkref_3();
	afx_msg void OnMarkref_4();
	//}}AFX_MSG
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGLAYOUTOBJECTGEOMETRY_H__68AB1A03_B0D5_11D5_88B7_0040F68C1EF7__INCLUDED_
