// DlgMarkPlacement.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"
#include "PrintSheetMarksView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMarkPlacement 


CDlgMarkPlacement::CDlgMarkPlacement(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgMarkPlacement::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgMarkPlacement)
	m_nPlaceOn = -1;
	m_nPlaceOnBoth = -1;
	//}}AFX_DATA_INIT

	m_pParent = NULL;
}

CDlgMarkPlacement::~CDlgMarkPlacement()
{
}

BOOL CDlgMarkPlacement::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgMarkPlacement::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgMarkPlacement)
	DDX_Radio(pDX, IDC_PLACEON_FRONT, m_nPlaceOn);
	DDX_Radio(pDX, IDC_PLACEON_MIRROR, m_nPlaceOnBoth);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgMarkPlacement, CDialog)
	//{{AFX_MSG_MAP(CDlgMarkPlacement)
	ON_BN_CLICKED(IDC_PLACEON_FRONT, OnPlaceonFront)
	ON_BN_CLICKED(IDC_PLACEON_BACK, OnPlaceonBack)
	ON_BN_CLICKED(IDC_PLACEON_BOTH, OnPlaceonBoth)
	ON_BN_CLICKED(IDC_PLACEON_MIRROR, OnPlaceonMirror)
	ON_BN_CLICKED(IDC_PLACEON_SAMEPOS, OnPlaceonSamepos)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgMarkPlacement 

BOOL CDlgMarkPlacement::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgMarkPlacement::LoadData()
{
	if ( ! m_pParent)
		return;

	int nKindOfProduction = WORK_AND_TURN;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		int nPlaceOn = -1;
		switch (m_pParent->GetObjectType())
		{
		case CMark::CustomMark: 
		case CMark::TextMark:	
		case CMark::BarcodeMark:	
		case CMark::DrawingMark:	nPlaceOn = GetLayoutObjectPlacement(pLayoutObject);	break;
		}
		switch (nPlaceOn)
		{
		case CReflinePositionParams::PlaceOnFront:			m_nPlaceOn = 0; m_nPlaceOnBoth = 0; break;
		case CReflinePositionParams::PlaceOnBack:			m_nPlaceOn = 1; m_nPlaceOnBoth = 0; break;
		case CReflinePositionParams::PlaceOnBothMirror:		m_nPlaceOn = 2; m_nPlaceOnBoth = 0; break;
		case CReflinePositionParams::PlaceOnBothSamePos:	m_nPlaceOn = 2; m_nPlaceOnBoth = 1; break;
		}
		if (pLayoutObject->GetLayout())
			nKindOfProduction = pLayoutObject->GetLayout()->KindOfProduction();
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		CReflinePositionParams* pParams = ((CReflineMark*)pMark)->GetReflinePosParams(0);
		int nPlaceOn = (pParams) ? pParams->m_nPlaceOn : CReflinePositionParams::PlaceOnBothSamePos;	

		switch (nPlaceOn)
		{
		case CReflinePositionParams::PlaceOnFront:			m_nPlaceOn = 0; m_nPlaceOnBoth = 0; break;
		case CReflinePositionParams::PlaceOnBack:			m_nPlaceOn = 1; m_nPlaceOnBoth = 0; break;
		case CReflinePositionParams::PlaceOnBothMirror:		m_nPlaceOn = 2; m_nPlaceOnBoth = 0; break;
		case CReflinePositionParams::PlaceOnBothSamePos:	m_nPlaceOn = 2; m_nPlaceOnBoth = 1; break;
		}

		if (CImpManDoc::GetDoc())
			if (CImpManDoc::GetDoc()->GetActLayout())
				nKindOfProduction = CImpManDoc::GetDoc()->GetActLayout()->KindOfProduction();
	}

	if (nKindOfProduction == WORK_AND_TURN)
	{
		if ((HBITMAP)m_bmMirrorPos)	m_bmMirrorPos.DeleteObject();
		m_bmMirrorPos.LoadBitmap(IDB_PLACEON_MIRROR_TURN); 
		((CStatic*)GetDlgItem(IDC_BMP_MIRRORPOS))->SetBitmap((HBITMAP)m_bmMirrorPos);
		if ((HBITMAP)m_bmSamePos)	m_bmSamePos.DeleteObject();
		m_bmSamePos.LoadBitmap(IDB_PLACEON_SAMEPOS_TURN); 
		((CStatic*)GetDlgItem(IDC_BMP_SAMEPOS))->SetBitmap((HBITMAP)m_bmSamePos);
	}
	else
	{
		if ((HBITMAP)m_bmMirrorPos)	m_bmMirrorPos.DeleteObject();
		m_bmMirrorPos.LoadBitmap(IDB_PLACEON_MIRROR_TUMBLE); 
		((CStatic*)GetDlgItem(IDC_BMP_MIRRORPOS))->SetBitmap((HBITMAP)m_bmMirrorPos);
		if ((HBITMAP)m_bmSamePos)	m_bmSamePos.DeleteObject();
		m_bmSamePos.LoadBitmap(IDB_PLACEON_SAMEPOS_TUMBLE); 
		((CStatic*)GetDlgItem(IDC_BMP_SAMEPOS))->SetBitmap((HBITMAP)m_bmSamePos);
	}

	if (m_nPlaceOn == 2)
	{
		GetDlgItem(IDC_PLACEON_MIRROR )->EnableWindow(TRUE);	
		GetDlgItem(IDC_PLACEON_SAMEPOS)->EnableWindow(TRUE);	
	}
	else
	{
		GetDlgItem(IDC_PLACEON_MIRROR )->EnableWindow(FALSE);	
		GetDlgItem(IDC_PLACEON_SAMEPOS)->EnableWindow(FALSE);	
	}
		
	UpdateData(FALSE);
}

void CDlgMarkPlacement::SaveData()
{
	if ( ! m_hWnd)
		return;

	UpdateData(TRUE);

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		int nPlaceOn = -1;
		switch (m_nPlaceOn)
		{
		case 0:	nPlaceOn = CReflinePositionParams::PlaceOnFront; break;
		case 1:	nPlaceOn = CReflinePositionParams::PlaceOnBack;  break;

		case 2:	switch (m_nPlaceOnBoth)
				{
				case 0:	nPlaceOn = CReflinePositionParams::PlaceOnBothMirror;  break;
				case 1:	nPlaceOn = CReflinePositionParams::PlaceOnBothSamePos; break;
				}
				break;
		}
		switch (m_pParent->GetObjectType())
		{
		case CMark::CustomMark: 
		case CMark::TextMark:	
		case CMark::BarcodeMark:	
		case CMark::DrawingMark:	SetLayoutObjectPlacement(pLayoutObject, nPlaceOn);	
									break;
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		int nPlaceOn = -1;
		switch (m_nPlaceOn)
		{ 
		case 0:	nPlaceOn = CReflinePositionParams::PlaceOnFront; break;
		case 1:	nPlaceOn = CReflinePositionParams::PlaceOnBack;  break;

		case 2:	switch (m_nPlaceOnBoth)
				{
				case 0:	nPlaceOn = CReflinePositionParams::PlaceOnBothMirror;  break;
				case 1:	nPlaceOn = CReflinePositionParams::PlaceOnBothSamePos; break;
				}
				break;
		}

		((CReflineMark*)pMark)->GetReflinePosParams(0)->m_nPlaceOn = nPlaceOn;
	}
}

void CDlgMarkPlacement::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}

void CDlgMarkPlacement::OnPlaceonFront() 
{
	GetDlgItem(IDC_PLACEON_MIRROR )->EnableWindow(FALSE);	
	GetDlgItem(IDC_PLACEON_SAMEPOS)->EnableWindow(FALSE);	
	SaveData();
	m_pParent->UpdateView();
}

void CDlgMarkPlacement::OnPlaceonBack() 
{
	GetDlgItem(IDC_PLACEON_MIRROR )->EnableWindow(FALSE);	
	GetDlgItem(IDC_PLACEON_SAMEPOS)->EnableWindow(FALSE);	
	SaveData();
	m_pParent->UpdateView();
}

void CDlgMarkPlacement::OnPlaceonBoth() 
{
	GetDlgItem(IDC_PLACEON_MIRROR )->EnableWindow(TRUE);	
	GetDlgItem(IDC_PLACEON_SAMEPOS)->EnableWindow(TRUE);	
	SaveData();
	m_pParent->UpdateView();
}

void CDlgMarkPlacement::OnPlaceonMirror() 
{
	SaveData();
	m_pParent->UpdateView();
}

void CDlgMarkPlacement::OnPlaceonSamepos() 
{
	SaveData();
	m_pParent->UpdateView();
}

int CDlgMarkPlacement::GetLayoutObjectPlacement(CLayoutObject* pLayoutObject)
{
	if ( ! pLayoutObject)
		return -1;

	pLayoutObject->FindCounterpartControlMark();	// call to initialize m_nPlaceOn if eventually undefined
	return pLayoutObject->m_reflinePosParams.m_nPlaceOn;
}

void CDlgMarkPlacement::SetLayoutObjectPlacement(CLayoutObject* pLayoutObject, int nPlaceOn)
{
	if ( ! pLayoutObject)
		return;
	CLayout* pLayout = pLayoutObject->GetLayout();
	if ( ! pLayout)
		return;

	CLayoutObject* pCounterPartLayoutObject = pLayoutObject->FindCounterpartControlMark();	
	CLayoutObject* pFrontObject = (pLayoutObject->GetLayoutSideIndex() == FRONTSIDE) ? pLayoutObject : pCounterPartLayoutObject;
	CLayoutObject* pBackObject  = (pLayoutObject->GetLayoutSideIndex() == BACKSIDE)  ? pLayoutObject : pCounterPartLayoutObject;
	if ( ! pFrontObject && ! pBackObject)
		return;

	CLayoutSide* pFrontLayoutSide = &pLayout->m_FrontSide;
	CLayoutSide* pBackLayoutSide  = &pLayout->m_BackSide;
	if ( ! pFrontLayoutSide || ! pBackLayoutSide)
		return;

	switch (nPlaceOn)
	{
	case CReflinePositionParams::PlaceOnFront:			if (pFrontObject)
														{
															if (pBackObject)
																pBackLayoutSide->m_ObjectList.RemoveObject(pBackObject);
														}
														else
															if (pBackObject)
															{
																POSITION pos =  pFrontLayoutSide->m_ObjectList.AddTail(*pBackObject);	 
																pFrontObject = &pFrontLayoutSide->m_ObjectList.GetAt(pos); 
																if (pFrontObject)
																	pFrontObject->AlignToCounterPart(pBackObject);
																if (pBackObject)
																	pBackLayoutSide->m_ObjectList.RemoveObject(pBackObject);
															}
														m_pParent->m_pLayoutObject = pFrontObject;
														break;

	case CReflinePositionParams::PlaceOnBack:			if (pBackObject)
														{
															if (pFrontObject)
																pFrontLayoutSide->m_ObjectList.RemoveObject(pFrontObject);
														}
														else
															if (pFrontObject)
															{
																POSITION pos =  pBackLayoutSide->m_ObjectList.AddTail(*pFrontObject);	 
																pBackObject  = &pBackLayoutSide->m_ObjectList.GetAt(pos); 
																if (pBackObject)
																	pBackObject->AlignToCounterPart(pFrontObject);
																if (pFrontObject)
																	pFrontLayoutSide->m_ObjectList.RemoveObject(pFrontObject);
															}
														m_pParent->m_pLayoutObject = pBackObject;
														break;

	case CReflinePositionParams::PlaceOnBothMirror:		
	case CReflinePositionParams::PlaceOnBothSamePos:	if (pFrontObject && ! pBackObject)
														{
															pFrontObject->m_reflinePosParams.m_nPlaceOn = nPlaceOn;
															POSITION pos =  pBackLayoutSide->m_ObjectList.AddTail(*pFrontObject);	 
															pBackObject  = &pBackLayoutSide->m_ObjectList.GetAt(pos); 
															if (pBackObject)
															{
																pBackObject->m_reflinePosParams.m_nPlaceOn = nPlaceOn;
																pBackObject->AlignToCounterPart(pFrontObject);
															}
														}
														else
															if ( ! pFrontObject && pBackObject)
															{
																pBackObject->m_reflinePosParams.m_nPlaceOn = nPlaceOn;
																POSITION pos  =  pFrontLayoutSide->m_ObjectList.AddTail(*pBackObject);	 
																pFrontObject  = &pFrontLayoutSide->m_ObjectList.GetAt(pos); 
																if (pFrontObject)
																{
																	pFrontObject->m_reflinePosParams.m_nPlaceOn = nPlaceOn;
																	pFrontObject->AlignToCounterPart(pBackObject);
																}
															}
															else
																if (pFrontObject && pBackObject)
																{
																	pFrontObject->m_reflinePosParams.m_nPlaceOn = nPlaceOn;
																	pBackObject->m_reflinePosParams.m_nPlaceOn  = nPlaceOn;
																	pBackObject->AlignToCounterPart(pFrontObject);
																}

														m_pParent->m_pLayoutObject = pFrontObject;
														break;
	}

	CPrintSheetMarksView* pMarksView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pMarksView)
		pMarksView->InitObjectList();

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_MarkTemplateList.FindPrintSheetLocations();
		pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
	}
}
