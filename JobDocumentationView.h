#pragma once


// CJobDocumentationView-Ansicht

class CJobDocumentationView : public CScrollView
{
	DECLARE_DYNCREATE(CJobDocumentationView)

protected:
	CJobDocumentationView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CJobDocumentationView();

public:
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	DECLARE_DISPLAY_LIST(CJobDocumentationView, OnMultiSelect, NO, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CJobDocumentationView, PrintPage, OnSelect, NO, OnDeselect, NO, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, TRUE)



public:
	int	  m_nNumPages;
	CSize m_sizePrintPage;
	CSize m_docSize;
	CRect m_rcPrevClientRect;


public:
	CSize CalcDocSize();
	CSize Draw(CDC* pDC, CDisplayList* pDisplayList = NULL);


protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
public:
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};


