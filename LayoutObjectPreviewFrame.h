#if !defined(AFX_LAYOUTOBJECTPREVIEWFRAME_H__384168B4_38D4_4F91_973E_916A5B05BF0A__INCLUDED_)
#define AFX_LAYOUTOBJECTPREVIEWFRAME_H__384168B4_38D4_4F91_973E_916A5B05BF0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LayoutObjectPreviewFrame.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Fenster CLayoutObjectPreviewFrame 

class CLayoutObjectPreviewFrame : public CStatic
{
// Konstruktion
public:
	CLayoutObjectPreviewFrame();

// Attribute
public:
	class CDlgObjectPreviewControl*	m_pParent;
	CSize							m_docSize;
	CSize							m_sizeViewport;


// Operationen
public:
	void					DrawPreview(CDC* pDC, CPageSource* pObjectSource, CPageSourceHeader* pObjectSourceHeader, CPageTemplate* pTemplate, 
										CRect& outBBox, CRect& trimBox, CRect& clipBox,	CRect previewFrameRect, CPrintSheet* pPrintSheet);
	void					PrepareUpdatePreview();
	void					GetBoxes(CRect& outBBox, CRect& trimBox, CRect& clipBox);
	CPoint					Rotate(CPoint pt, int nRotation, CPoint ptCenter);
	CMark*					GetMark();
	CLayoutObject*			GetLayoutObject();
	CPrintSheet*			GetPrintSheet();
	CPageSource*			GetObjectSource();
	CPageSourceHeader*		GetObjectSourceHeader(); 
	CPageTemplate*			GetObjectTemplate();
	CColorDefinitionTable*	GetColorDefTable();


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CLayoutObjectPreviewFrame)
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~CLayoutObjectPreviewFrame();

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CLayoutObjectPreviewFrame)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_LAYOUTOBJECTPREVIEWFRAME_H__384168B4_38D4_4F91_973E_916A5B05BF0A__INCLUDED_
