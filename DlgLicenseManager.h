#pragma once


// CDlgLicenseManager-Dialogfeld

class CDlgLicenseManager : public CDialog
{
	DECLARE_DYNAMIC(CDlgLicenseManager)

public:
	CDlgLicenseManager(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgLicenseManager();

// Dialogfelddaten
	enum { IDD = IDD_LICENSEMANAGER };

public:
	void InitData();


public:
	CFont	m_boldFont;
	CBrush	m_bkBrushGray, m_bkBrushRed, m_bkBrushYellow, m_bkBrushGreen;
	CString m_strProgrammVersion;
	CString m_strPDFSheetOutStatus;
	CString m_strJDFMisAddOnStatus;
	CString m_strJDFImpoAddOnStatus;
	CString m_strJDFCutAddOnStatus;
	CString m_strJDFFoldAddOnStatus;
	CString m_strBarcodeAddOnStatus;
	CString m_strOptimizeAddOnStatus;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedLmGetlicenseButton();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
