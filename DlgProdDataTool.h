#pragma once


// CDlgProdDataTool-Dialogfeld

class CDlgProdDataTool : public CDialog
{
	DECLARE_DYNAMIC(CDlgProdDataTool)

public:
	CDlgProdDataTool(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProdDataTool();

// Dialogfelddaten
	enum { IDD = IDD_DLGPRODDATATOOL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
public:
	afx_msg void OnMove(int x, int y);
};
