// PrintSheetTreeView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPrintSheetTreeView view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif


class CTreeItem
{
// Attributes
public:
	enum ItemType {Root, PrintSheet, FrontSide, BackSide, FrontPlate, BackPlate};

	HTREEITEM hTreeItem;
	int		  nType;
	int		  nPrintSheetNumber;
	int		  nPlateIndex;
};

struct _TREEITEM {}; // -> Query in Books Online : _TREEITEM
typedef CList <HTREEITEM,HTREEITEM&> CHTreeItemList;

typedef CList <CTreeItem,CTreeItem&> CTreeItemList;

class CPrintSheetTreeView : public CTreeView
{
protected:
	CPrintSheetTreeView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPrintSheetTreeView)

// Attributes
protected:
	CHTreeItemList	m_hTreeItemList;		// List of TreeItems of PrintSheets
	CImageList*     m_pimagelist;
	CDC*			m_pMemDC;
	CBitmap*		m_pColorBitmap;
	BOOL		    m_bAllSidesClicked;
	CBrush*			m_pBkBrush;

public:
	CTreeItemList	m_TreeItemList;
	int	 m_ActPrintSheetNumber;	// Which PrintSheet TreeItem will be active (Index for m_hTreeItemList)
	int  m_nActSight;			// Kind of sight : FRONTSIDE | BACKSIDE | BOTHSIDES | ALLSIDES

// Operations
public:
	CImpManDoc* GetDocument();
	void		SynchronizePlateTreeItems(CDisplayList& displayList);
	void		SelectTreeItem(int nPrintSheetNumber, int nSight);
	void		SelectRootTreeItem		();
	void		SelectPrintSheetTreeItem(int nPrintSheetNumber);
	void		SelectPlateTreeItem		(CPrintSheet* pPrintSheet, CPlate* pPlate);
	void		EnablePlateTreeItem		(CPrintSheet* pPrintSheet, CPlate* pPlate, BOOL bEnable = TRUE);
	CTreeItem*	FindRootTreeItem		();
	CTreeItem*	FindPrintSheetTreeItem	(int nPrintSheetNumber, int nSight = -1);
	CTreeItem*  FindPlateTreeItem		(CPrintSheet* pPrintSheet, CPlate* pPlate);
	void		HighlightTreeItems(CLayout* pLayout = NULL);
	void		LowlightTreeItems(CDisplayList& displayList);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintSheetTreeView)
	public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPrintSheetTreeView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void AddPlateColorsToTree();	// Set the imagelist for the plate-colors
	void ShowTreeCtrlAllSides();	// Main function to show the tree
	void ShowTreeCtrlOneLayout();	// For use if you need only one Layout (CipCut ...) 

	// Generated message map functions
	//{{AFX_MSG(CPrintSheetTreeView)
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in CPrintSheetTreeView.cpp
inline CImpManDoc* CPrintSheetTreeView::GetDocument()
   { return (CImpManDoc*)m_pDocument; }
#endif
/////////////////////////////////////////////////////////////////////////////
