// PDFEngineInterface.h : header file for interface to callas PDFEngine
//



#pragma warning (disable : 4201)	// supress warning 4201 (struct/union w/o name) - happens in typedef struct _TDRECT

#pragma pack(8)	// set struct align to 8 bytes (desired by PDFEngine)

#include "PdfImpositionLib.h"
#include "ptbExport.h"
#include "ptbLib.h"
#include "ptbError.h"
#include "ptbStrings.h"
#include "ptbEncoding.h"
#include "ptbPreflight.h"

#pragma warning (default : 4201)

#pragma pack()	// reset struct align to KIM projects default


