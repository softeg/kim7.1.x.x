// PrintSheetMaskSettingsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "NewPrintSheetFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "PrintSheetMaskSettingsView.h"
#include "PageListTableView.h"
#include "GraphicComponent.h"




// CMaskSettingsTableHeader

IMPLEMENT_DYNAMIC(CMaskSettingsTableHeader, CStatic)

CMaskSettingsTableHeader::CMaskSettingsTableHeader()
{
	m_font.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CMaskSettingsTableHeader::~CMaskSettingsTableHeader()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CMaskSettingsTableHeader, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CMaskSettingsTableHeader::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheetMaskSettingsView* pView = (CPrintSheetMaskSettingsView*)GetParent();
	if ( ! pView)
		return;
	CPrintSheetMaskSettingsTable* pTable = &pView->m_maskSettingsTable;
	if ( ! pTable)
		return;

	CRect rcFrame;
	GetClientRect(rcFrame);

	CGraphicComponent gp;
	switch (GetDlgCtrlID())
	{
	case IDC_PRINTVIEW_MASKSETTINGS_TITLE_FRAME:
		{
			CString string; string.LoadString(IDS_MASKING);
			CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + 20;
			gp.DrawTitleBar(&dc, rcTitle, string, RGB(242,243,240), RGB(238,235,217), 90, -1);
		}
		break;

	case IDC_PRINTVIEW_MASKSETTINGS_HEADER_FRAME:
		{
			CPen pen(PS_SOLID, 1, LIGHTGRAY);
			dc.SelectObject(&pen);
			dc.MoveTo(rcFrame.left,  rcFrame.top);
			dc.LineTo(rcFrame.right, rcFrame.top);

			CString string; 
			gp.DrawTitleBar(&dc, rcFrame, _T(""), RGB(242,243,240), RGB(238,235,217), 90, -1);

			dc.MoveTo(rcFrame.left,  rcFrame.bottom);
			dc.LineTo(rcFrame.right, rcFrame.bottom);

			rcFrame.top += 5;

			dc.SelectObject(&m_font);
			dc.SetBkMode(TRANSPARENT);

			int nOffsetY = 0;
			for (int i = 0; i < pTable->m_ColumnRefList.GetSize(); i++)
			{
				string = pTable->m_ColumnRefList[i].m_pColumn->m_strColumnLabel;

				CRect rcText(pTable->m_ColumnRefList[i].m_pColumn->m_nColumnLeft + 5, rcFrame.top + 1, rcFrame.right, rcFrame.top + 15);
				
				dc.SetTextColor(BLACK);
				CPageListTableView::FitStringToWidth(&dc, string, rcText.Width());
				dc.DrawText(string, rcText, DT_LEFT | DT_TOP);
			}

			pen.DeleteObject();
		}
		break;
	}
}



//////////////////////////////////

void CPrintSheetMaskSettingsTable::Create(CPrintSheetMaskSettingsView* pParent)
{
	m_pParent	  = pParent;
	m_nWhatToShow = UnInitialized;
}

void CPrintSheetMaskSettingsTable::Initialize(int nWhatToShow)
{
	//if (m_nWhatToShow == nWhatToShow)	// already initialized
	//	return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CRect rcTitleFrame;
	m_pParent->GetDlgItem(IDC_PRINTVIEW_MASKSETTINGS_TITLE_FRAME)->GetWindowRect(rcTitleFrame);
	m_pParent->ScreenToClient(rcTitleFrame);
	m_pParent->GetDlgItem(IDC_PRINTVIEW_MASKSETTINGS_HEADER_FRAME)->GetWindowRect(m_headerFrameRect);
	m_pParent->ScreenToClient(m_headerFrameRect);

	int nHeaderHeight = 25;	

	m_nWhatToShow = nWhatToShow;

	for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
		delete m_ColumnRefList[i].m_pColumn;

	m_ColumnRefList.RemoveAll();

	CArray<CPrintSheetTableColumn*, CPrintSheetTableColumn*> pColumns;

	CString string;
	string.LoadString(IDS_SHEET_TEXT);
	pColumns.Add(new CColumnSheetMaskSettingsSheet(160, string, this, 0));

	string.LoadString(IDS_GLOBAL_TITLE);
	pColumns.Add(new CColumnSheetMaskSettingsGlobal(60, string, this, 1));

	string.LoadString(IDS_LOCAL_TITLE);
	pColumns.Add(new CColumnSheetMaskSettingsLocal(80, string, this, 1));

	string.LoadString(IDS_CONTENT_ADJUSTED_TITLE);
	pColumns.Add(new CColumnSheetMaskSettingsContent(85, string, this, 1));

	for (int i = 0; i < pColumns.GetSize(); i++)
	{
		CColumnRef column(i, pColumns[i]);
		m_ColumnRefList.Add(column);
	}

	CClientDC dc(m_pParent);
	dc.SelectObject(&((CPrintSheetMaskSettingsView*)m_pParent)->m_maskSettingsTableHeader.m_font);
	TEXTMETRIC tm;
	dc.GetTextMetrics(&tm); 
	m_nLineHeight = max(14, tm.tmHeight) + 6;	// 14 = sheet icon height

	int nXPos = 0;
	for (int i = 0; i < m_ColumnRefList.GetSize(); i++)
	{
		m_ColumnRefList[i].m_pColumn->m_nColumnLeft = nXPos;

		nXPos += m_ColumnRefList[i].m_pColumn->m_nColumnWidth;

		m_ColumnRefList[i].m_pColumn->m_pDisplayList = &((CPrintSheetMaskSettingsView*)m_pParent)->m_DisplayList;
	}

	m_nHeaderWidth = nXPos;

	rcTitleFrame.right = nXPos;
	m_pParent->GetDlgItem(IDC_PRINTVIEW_MASKSETTINGS_TITLE_FRAME)->MoveWindow(rcTitleFrame);

	m_headerFrameRect.bottom = m_headerFrameRect.top  + nHeaderHeight;
	m_headerFrameRect.right  = nXPos;
	m_pParent->GetDlgItem(IDC_PRINTVIEW_MASKSETTINGS_HEADER_FRAME)->MoveWindow(m_headerFrameRect);

	((CPrintSheetMaskSettingsView*)m_pParent)->OnUpdate(NULL, 0, 0);
	m_pParent->UpdateWindow();
}

int CPrintSheetMaskSettingsTable::GetCellLines(CPrintSheet& rPrintSheet)
{
	if ( ! rPrintSheet.GetLayout())
		return 0;

	int nLines = 1;
	if (rPrintSheet.m_bDisplayOpen)
	{
		CLayout* pLayout = rPrintSheet.GetLayout();
		if ( ! pLayout)
			return nLines;

		int nSide = -1;
		do
		{
			nLines++;

			nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;
			CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;
			if ( (rObjectList.m_bDisplayOpen))
				nLines += GetNumLockedObjects(rPrintSheet, nSide);

			if (pLayout->m_nProductType)
				nSide = BACKSIDE;	// skip backside
		}
		while (nSide != BACKSIDE);
	}

	return nLines;
}

int CPrintSheetMaskSettingsTable::GetNumLockedObjects(CPrintSheet& rPrintSheet, int nSide)
{
	CLayout* pLayout = rPrintSheet.GetLayout();
	if ( ! pLayout)
		return 0;

	int nNumLockedObjects = 0;
	CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;
	CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;
	CPlate&		rPlate	   = (rPlateList.GetCount()) ? rPlateList.GetHead() : rPlateList.m_defaultPlate;

	CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
	rObjectList.GetSheetObjectRefList(&rPrintSheet, &objectRefList);
	for (int i = 0; i < objectRefList.GetSize(); i++)
	{
		CLayoutObject* pObject		   = objectRefList[i].m_pLayoutObject;
		CPageTemplate* pObjectTemplate = objectRefList[i].m_pObjectTemplate;
		CExposure*	   pExposure	   = (pObject) ? pObject->GetExposure(&rPlate) : NULL;
		if ( ! pObject || ! pObjectTemplate || ! pExposure)
			continue;
		if ( ! pExposure->IsBleedLocked(LEFT | RIGHT | BOTTOM | TOP))
			continue;

		nNumLockedObjects++;
	}

	return nNumLockedObjects;
}

void CColumnSheetMaskSettingsSheet::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheetMaskSettingsView* pView = (CPrintSheetMaskSettingsView*)m_pParent->m_pParent;
	if ( ! pView)
		return;

	HICON	 hIconPDF		  = theApp.LoadIcon(IDI_OUT_SHEET);
	HICON	 hIconFront		  = theApp.LoadIcon(IDI_OUT_SHEET_FRONT);
	HICON	 hIconBack		  = theApp.LoadIcon(IDI_OUT_SHEET_BACK);
	HICON	 hIconPage		  = theApp.LoadIcon(IDI_SHEET_PAGE);
	HICON	 hIconCustomMark  = theApp.LoadIcon(IDI_CUSTOM_MARK);
	HICON	 hIconCropMark	  = theApp.LoadIcon(IDI_CROP_MARK);
	HICON	 hIconFoldMark	  = theApp.LoadIcon(IDI_FOLD_MARK);
	HICON	 hIconBarcodeMark = theApp.LoadIcon(IDI_BARCODE_MARK);
	HICON	 hIconDrawingMark = theApp.LoadIcon(IDI_DRAWING_MARK);
	HICON	 hIconSectionMark = theApp.LoadIcon(IDI_SECTION_MARK);
	HICON	 hIconTextMark	  = theApp.LoadIcon(IDI_TEXT_MARK);
	HICON	 hIconPlus		  = theApp.LoadIcon(IDI_PLUS);
	HICON	 hIconMinus		  = theApp.LoadIcon(IDI_MINUS);
	int		 nLine			  = 0;
	POSITION pos			  = pDoc->m_PrintSheetList.GetHeadPosition();
	CString	 strOut;
	pDC->SetTextColor(BLACK);

	CRect rcParent; m_pParent->m_pParent->GetClientRect(rcParent); rcParent -= pDC->GetViewportOrg();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		CLayout*	 pLayout	 = rPrintSheet.GetLayout();
		if ( ! pLayout)
			continue;

		pDC->DrawIcon(CPoint(0, nLine * m_pParent->m_nLineHeight + 5), (rPrintSheet.m_bDisplayOpen) ? hIconMinus : hIconPlus);

		pDC->DrawIcon(CPoint(12, nLine * m_pParent->m_nLineHeight + 2), hIconPDF);

		strOut.Format(_T("%s (%s)"), rPrintSheet.GetNumber(), pLayout->m_strLayoutName);
		CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 40);
		pDC->TextOut(40, GETTEXTPOS_VERT(nLine), strOut);

		CSize textExtent = pDC->GetTextExtent(strOut);
		CRect regRect(CPoint(rcParent.left, nLine * m_pParent->m_nLineHeight), CSize(rcParent.Width(), 20));
		//m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetMaskSettingsView, PDFSheet), NULL, CDisplayItem::ForceRegister);

		int	nCount = 0;
		if (rPrintSheet.m_bDisplayOpen)
		{
			int nSide = -1;
			do
			{
				nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;

				nLine++;

				pDC->DrawIcon(CPoint(30, nLine * m_pParent->m_nLineHeight + 2), (nSide == FRONTSIDE) ? hIconFront : hIconBack);

				switch (pLayout->m_nProductType)
				{
				case WORK_AND_BACK:		strOut = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_strSideName : pLayout->m_BackSide.m_strSideName; break;
				case WORK_AND_TURN  :	strOut = theApp.settings.m_szWorkAndTurnName;		break;
				case WORK_AND_TUMBLE:	strOut = theApp.settings.m_szWorkAndTumbleName;		break;
				case WORK_SINGLE_SIDE:	strOut = theApp.settings.m_szSingleSidedName;		break;
				default:				strOut = "";										break;
				}
					
				CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 50);
				pDC->TextOut(50, GETTEXTPOS_VERT(nLine), strOut);

				textExtent = pDC->GetTextExtent(strOut);
				CRect regRect = CRect(CPoint(rcParent.left + 2, nLine * m_pParent->m_nLineHeight - 1), CSize(rcParent.Width() - 4, 19));
				CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetMaskSettingsView, PDFSheetSide), (void*)nSide, CDisplayItem::ForceRegister);
				if (pDI)
				{
					pDI->SetMouseOverFeedback();
					pDI->SetNoStateFeedback();
					if (pDI->m_nState == CDisplayItem::Selected)
					{
						pDC->SaveDC();
						pDC->SelectClipRgn(NULL);
						CRect rcFeedback = pDI->m_BoundingRect;
						rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
						Graphics graphics(pDC->m_hDC);
						Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
						rc.Inflate(-1, -1);
						COLORREF crHighlightColor = RGB(165,175,200);
						SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
						graphics.FillRectangle(&br, rc);
						graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
						pDC->RestoreDC(-1);
					}
				}

				nCount++;
				CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;
				CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
				rObjectList.GetSheetObjectRefList(&rPrintSheet, &objectRefList);

				if (objectRefList.GetSize())
				{
					if (((CPrintSheetMaskSettingsTable*)m_pParent)->GetNumLockedObjects(rPrintSheet, nSide))
						pDC->DrawIcon(CPoint(17, nLine * m_pParent->m_nLineHeight + 5), (rObjectList.m_bDisplayOpen) ? hIconMinus : hIconPlus);

					if ( (rObjectList.m_bDisplayOpen))
					{
						CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;
						CPlate&		rPlate	   = (rPlateList.GetCount()) ? rPlateList.GetHead() : rPlateList.m_defaultPlate;

						for (int i = 0; i < objectRefList.GetSize(); i++)
						{
							CLayoutObject* pObject		   = objectRefList[i].m_pLayoutObject;
							CPageTemplate* pObjectTemplate = objectRefList[i].m_pObjectTemplate;
							CExposure*	   pExposure	   = (pObject) ? pObject->GetExposure(&rPlate) : NULL;
							if ( ! pObject || ! pObjectTemplate || ! pExposure)
								continue;
							if ( ! pExposure->IsBleedLocked(LEFT | RIGHT | BOTTOM | TOP))
								continue;

							nLine++;
						
							if (objectRefList[i].m_nObjectType == CLayoutObject::Page)
								pDC->DrawIcon(CPoint(40, nLine * m_pParent->m_nLineHeight + 3), hIconPage);
							else
							{
								HICON hIcon = hIconCustomMark;
								switch (objectRefList[i].m_nMarkType)
								{
								case CMark::CustomMark:		hIcon = hIconCustomMark;	break;
								case CMark::TextMark:		hIcon = hIconTextMark;		break;
								case CMark::SectionMark:	hIcon = hIconSectionMark;	break;
								case CMark::CropMark:		hIcon = hIconCropMark;		break;
								case CMark::FoldMark:		hIcon = hIconFoldMark;		break;
								case CMark::BarcodeMark:	hIcon = hIconBarcodeMark;	break;
								case CMark::DrawingMark:	hIcon = hIconDrawingMark;	break;
								}
								pDC->DrawIcon(CPoint(38, nLine * m_pParent->m_nLineHeight + 2), hIcon);
							}

							strOut = pObjectTemplate->m_strPageID;
							CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width() - 43);
							pDC->TextOut(55, GETTEXTPOS_VERT(nLine), strOut);

							textExtent = pDC->GetTextExtent(strOut);
							//CRect regRect(CPoint(35, nLine * m_pParent->m_nLineHeight), CSize(25 + textExtent.cx, 15));
							//regRect.right = min(regRect.right, m_columnClipRect.Width());
							//m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)pObject,
							// 							 DISPLAY_ITEM_REGISTRY(CPrintSheetMaskSettingsView, PDFSheetObject), (void*)&rPrintSheet, CDisplayItem::ForceRegister);

							nCount++;
						}
					}
				}

				if (pLayout->m_nProductType)
					nSide = BACKSIDE;	// skip backside
			}
			while (nSide != BACKSIDE);
		}
	
		nLine += GetCellLines(rPrintSheet) - nCount;

		regRect.bottom = nLine * m_pParent->m_nLineHeight; regRect.right = rcParent.right;
		CDisplayItem* pDI = m_pDisplayList->RegisterItem(pDC, regRect, regRect, (void*)&rPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintSheetMaskSettingsView, PDFSheet), NULL, CDisplayItem::ForceRegister);
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				pDC->SaveDC();
				pDC->SelectClipRgn(NULL);
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				Graphics graphics(pDC->m_hDC);
				Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
				rc.Inflate(-1, -1);
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(100, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(120, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);
				pDC->RestoreDC(-1);
			}
		}
	}
}

void CColumnSheetMaskSettingsGlobal::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int		 nLine			= 0;
	POSITION pos			= pDoc->m_PrintSheetList.GetHeadPosition();
	CString	 strOut;
	COLORREF crOldTextColor	= pDC->GetTextColor();
	while (pos)
	{
		CPrintSheet&  rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);

		if ( ! rPrintSheet.m_bDisplayOpen)
		{
			strOut = rPrintSheet.GetGlobalMaskValueString(BOTHSIDES);
			CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
			pDC->SetTextColor(BLACK);
			pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
		}

		int	nCount = 0;
		if (rPrintSheet.m_bDisplayOpen)
		{
			CLayout* pLayout = rPrintSheet.GetLayout();
			if ( ! pLayout)
				continue;

			int nSide = -1;
			do
			{
				nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;
				CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;

				nLine++;

				if ( ! rPlateList.m_bDisplayOpen)
				{
					strOut = rPrintSheet.GetGlobalMaskValueString(nSide);
					CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());	
					pDC->SetTextColor(BLACK);
					pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
				}

				nCount++;

				if (pLayout->m_nProductType)
					nSide = BACKSIDE;	// skip backside
			}
			while (nSide != BACKSIDE);
		}
	
		nLine += GetCellLines(rPrintSheet) - nCount;
	}
	pDC->SetTextColor(crOldTextColor);
}

void CColumnSheetMaskSettingsLocal::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int		 nLine			= 0;
	POSITION pos			= pDoc->m_PrintSheetList.GetHeadPosition();
	CString	 strOut;
	COLORREF crOldTextColor	= pDC->GetTextColor();
	while (pos)
	{
		CPrintSheet&  rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);

		if ( ! rPrintSheet.m_bDisplayOpen)
		{
			strOut = (rPrintSheet.HasLocalMaskValues(BOTHSIDES)) ? _T("...") : _T("");
			CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
			pDC->SetTextColor(BLACK);
			pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
		}

		int	nCount = 0;
		if (rPrintSheet.m_bDisplayOpen)
		{
			CLayout* pLayout = rPrintSheet.GetLayout();
			if ( ! pLayout)
				continue;

			int nSide = -1;
			do
			{
				nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;
				CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;

				nLine++;

				nCount++;
				CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;
				CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
				rObjectList.GetSheetObjectRefList(&rPrintSheet, &objectRefList);

				if (objectRefList.GetSize())
				{
					if ( (rObjectList.m_bDisplayOpen))
					{
						CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;
						CPlate&		rPlate	   = (rPlateList.GetCount()) ? rPlateList.GetHead() : rPlateList.m_defaultPlate;

						for (int i = 0; i < objectRefList.GetSize(); i++)
						{
							CLayoutObject* pObject		   = objectRefList[i].m_pLayoutObject;
							CPageTemplate* pObjectTemplate = objectRefList[i].m_pObjectTemplate;
							CExposure*	   pExposure	   = (pObject) ? pObject->GetExposure(&rPlate) : NULL;
							if ( ! pObject || ! pObjectTemplate || ! pExposure)
								continue;
							if ( ! pExposure->IsBleedLocked(LEFT | RIGHT | BOTTOM | TOP))
								continue;

							nLine++;
						
							strOut.Empty();
							CString strMeasure;
							if (pExposure->IsBleedLocked(LEFT))
							{
								SetMeasure(0, pExposure->GetBleedValue(LEFT), strMeasure, TRUE);
								strOut += _T("<") + strMeasure + _T(" ");
							}
							if (pExposure->IsBleedLocked(RIGHT))
							{
								SetMeasure(0, pExposure->GetBleedValue(RIGHT), strMeasure, TRUE);
								strOut += _T(">") + strMeasure + _T(" ");
							}
							if (pExposure->IsBleedLocked(BOTTOM))
							{
								SetMeasure(0, pExposure->GetBleedValue(BOTTOM), strMeasure, TRUE);
								strOut += _T("v") + strMeasure + _T(" ");
							}
							if (pExposure->IsBleedLocked(TOP))
							{
								SetMeasure(0, pExposure->GetBleedValue(TOP), strMeasure, TRUE);
								strOut += _T("^") + strMeasure;
							}
							CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
							pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);

							nCount++;
						}
					}
					else
					{
						strOut = (rPrintSheet.HasLocalMaskValues(nSide)) ? _T("...") : _T("");
						CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());	
						pDC->SetTextColor(BLACK);
						pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
					}

				}
				if (pLayout->m_nProductType)
					nSide = BACKSIDE;	// skip backside
			}
			while (nSide != BACKSIDE);
		}
	
		nLine += GetCellLines(rPrintSheet) - nCount;
	}
	pDC->SetTextColor(crOldTextColor);
}

void CColumnSheetMaskSettingsContent::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	HICON	 hCheckIcon		= theApp.LoadIcon(IDI_CHECK);
	int		 nLine			= 0;
	POSITION pos			= pDoc->m_PrintSheetList.GetHeadPosition();
	CString	 strOut;
	COLORREF crOldTextColor	= pDC->GetTextColor();
	while (pos)
	{
		CPrintSheet&  rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);

		if ( ! rPrintSheet.m_bDisplayOpen)
		{
			strOut = (rPrintSheet.HasAdjustContentToMask(BOTHSIDES)) ? _T("...") : _T("");
			CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
			pDC->SetTextColor(BLACK);
			pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
		}

		int	nCount = 0;
		if (rPrintSheet.m_bDisplayOpen)
		{
			CLayout* pLayout = rPrintSheet.GetLayout();
			if ( ! pLayout)
				continue;

			int nSide = -1;
			do
			{
				nSide = (nSide == -1) ? FRONTSIDE : BACKSIDE;
				CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;

				nLine++;

				nCount++;
				CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;
				CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
				rObjectList.GetSheetObjectRefList(&rPrintSheet, &objectRefList);

				if (objectRefList.GetSize())
				{
					if ( (rObjectList.m_bDisplayOpen))
					{
						CPlateList& rPlateList = (nSide == FRONTSIDE) ? rPrintSheet.m_FrontSidePlates : rPrintSheet.m_BackSidePlates;
						CPlate&		rPlate	   = (rPlateList.GetCount()) ? rPlateList.GetHead() : rPlateList.m_defaultPlate;

						for (int i = 0; i < objectRefList.GetSize(); i++)
						{
							CLayoutObject* pObject		   = objectRefList[i].m_pLayoutObject;
							CPageTemplate* pObjectTemplate = objectRefList[i].m_pObjectTemplate;
							CExposure*	   pExposure	   = (pObject) ? pObject->GetExposure(&rPlate) : NULL;
							if ( ! pObject || ! pObjectTemplate || ! pExposure)
								continue;
							if ( ! pObjectTemplate->m_bAdjustContentToMask)
								continue;

							nLine++;
						
							pDC->DrawIcon(-2, GETTEXTPOS_VERT(nLine) + 2, hCheckIcon);
							//strOut = _T("X");
							//CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());
							//pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);

							nCount++;
						}
					}
					else
					{
						strOut = (rPrintSheet.HasAdjustContentToMask(nSide)) ? _T("...") : _T("");
						CPageListTableView::FitStringToWidth(pDC, strOut, m_columnClipRect.Width());	
						pDC->SetTextColor(BLACK);
						pDC->TextOut(0, GETTEXTPOS_VERT(nLine), strOut);
					}

				}
				if (pLayout->m_nProductType)
					nSide = BACKSIDE;	// skip backside
			}
			while (nSide != BACKSIDE);
		}
	
		nLine += GetCellLines(rPrintSheet) - nCount;
	}
	pDC->SetTextColor(crOldTextColor);
}


// CPrintSheetMaskSettingsView

IMPLEMENT_DYNCREATE(CPrintSheetMaskSettingsView, CFormView)

CPrintSheetMaskSettingsView::CPrintSheetMaskSettingsView()
	: CFormView(CPrintSheetMaskSettingsView::IDD)
{
	m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CPrintSheetMaskSettingsView));
	m_fOverbleed  = 0.0f;
	m_nWhatSheets = SheetAll;
	m_pToolTip	  = NULL;
}

CPrintSheetMaskSettingsView::~CPrintSheetMaskSettingsView()
{
	if (m_pToolTip)
		delete m_pToolTip;
}

void CPrintSheetMaskSettingsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Measure(pDX, IDC_MASKSETTINGS_OVERBLEED_EDIT, m_fOverbleed);
}

BEGIN_MESSAGE_MAP(CPrintSheetMaskSettingsView, CFormView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_EN_KILLFOCUS(IDC_MASKSETTINGS_OVERBLEED_EDIT, OnKillFocusMaskSettingsOverbleedEdit)
	ON_NOTIFY(UDN_DELTAPOS, IDC_MASKSETTINGS_OVERBLEED_SPIN, OnDeltaposMaskSettingsOverbleedSpin)
	ON_EN_SETFOCUS(IDC_MASKSETTINGS_OVERBLEED_EDIT, &CPrintSheetMaskSettingsView::OnEnSetfocusMasksettingsOverbleedEdit)
	ON_BN_CLICKED(IDC_MASKSETTINGS_RELEASE_BLEED, &CPrintSheetMaskSettingsView::OnBnClickedMasksettingsReleaseBleed)
	ON_UPDATE_COMMAND_UI(IDC_MASKSETTINGS_RELEASE_BLEED, OnUpdateMasksettingsReleaseBleed)
	ON_BN_CLICKED(IDC_MASKSETTINGS_ADJUST_CONTENT, &CPrintSheetMaskSettingsView::OnBnClickedMasksettingsAdjustContent)
	ON_UPDATE_COMMAND_UI(IDC_MASKSETTINGS_ADJUST_CONTENT, OnUpdateMasksettingsAdjustContent)
END_MESSAGE_MAP()


// CPrintSheetMaskSettingsView-Diagnose

#ifdef _DEBUG
void CPrintSheetMaskSettingsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetMaskSettingsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrintSheetMaskSettingsView-Meldungshandler

void CPrintSheetMaskSettingsView::OnInitialUpdate()
{
	m_maskSettingsTableTitle.SubclassDlgItem (IDC_PRINTVIEW_MASKSETTINGS_TITLE_FRAME, this);
	m_maskSettingsTableHeader.SubclassDlgItem(IDC_PRINTVIEW_MASKSETTINGS_HEADER_FRAME, this);

	m_maskSettingsTable.Create(this);
	m_maskSettingsTable.Initialize(-1);//CPrintSheetTable::ShowPrintColorInfo);	

	m_releaseButton.SubclassDlgItem	(IDC_MASKSETTINGS_RELEASE_BLEED, this);
	m_releaseButton.SetBitmap		(IDB_RELEASE_BLEED_HOT, IDB_RELEASE_BLEED_HOT, IDB_RELEASE_BLEED_COLD, XCENTER);
	m_adjustButton.SubclassDlgItem	(IDC_MASKSETTINGS_ADJUST_CONTENT, this);
	m_adjustButton.SetBitmap		(IDB_ADJUST_CONTENT_HOT, IDB_ADJUST_CONTENT_HOT, IDB_ADJUST_CONTENT_COLD, XCENTER);
	//m_releaseButton.SetTransparent(TRUE);
	//m_releaseButton.AlignToBitmap();
	m_pToolTip = new CToolTipCtrl;
    m_pToolTip->Create(this);
	m_pToolTip->AddTool(((CButton*)GetDlgItem(IDC_MASKSETTINGS_RELEASE_BLEED)),	IDS_RELEASE_MASK);
	m_pToolTip->Activate(TRUE);

	CFormView::OnInitialUpdate();

	((CSpinButtonCtrl*)GetDlgItem(IDC_MASKSETTINGS_OVERBLEED_SPIN))->SetRange(0, 1);
}

void CPrintSheetMaskSettingsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_DisplayList.Invalidate();	// for now it's easier always to invalidate, because no. items changes when items are expanded

	m_maskSettingsTable.CalcTableLenght();

	CRect clientRect;
	GetClientRect(clientRect);

	CSize scrollSize(m_maskSettingsTable.m_nHeaderWidth, m_maskSettingsTable.m_nTableLenght + m_maskSettingsTable.m_headerFrameRect.bottom);
	CSize pageSize  (m_maskSettingsTable.m_nHeaderWidth, clientRect.Height() - m_maskSettingsTable.m_nHeaderHeight);
	CSize lineSize  (m_maskSettingsTable.m_nHeaderWidth, m_maskSettingsTable.m_nLineHeight);
	SetScrollSizes(MM_TEXT, scrollSize, pageSize, lineSize);

	Invalidate();

	InitData();

	UpdateDialogControls( this, TRUE );
}

void CPrintSheetMaskSettingsView::InitData()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	BOOL  bFoundDifferent = FALSE;
	CString str1, str2, strPrev;
	float fValue = 0.0f;
	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (IsSheetSideEffected(rPrintSheet, FRONTSIDE))
			str1 = rPrintSheet.m_FrontSidePlates.GetGlobalMaskValueString();
		if (IsSheetSideEffected(rPrintSheet, BACKSIDE))
			str2 = rPrintSheet.m_BackSidePlates.GetGlobalMaskValueString();
		if (strPrev.IsEmpty())
			strPrev = str1;
		if ( (str1 != str2) || (str1 != strPrev) )
		{
			bFoundDifferent = TRUE;
			break;
		}
		strPrev = str1;
	}

	if (bFoundDifferent)
	{
		GetDlgItem(IDC_MASKSETTINGS_OVERBLEED_EDIT)->SetWindowText(_T(""));
		m_fOverbleed = 0.0f;
	}
	else
	{
		m_fOverbleed = (float)_tstof(str1);
		UpdateData(FALSE);
	}
}

BOOL CPrintSheetMaskSettingsView::IsSheetEffected(CPrintSheet& rPrintSheet, BOOL bTrueIfNothing)
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheet));
	if ( ! pDI)
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheetSide));
	if ( ! pDI)		// nothing selected means all sheets effected
		return (bTrueIfNothing) ? TRUE : FALSE;
	
	pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheet));
	while (pDI)
	{
		CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
		if (pPrintSheet == &rPrintSheet)
			return TRUE;
		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheet));
	}

	return FALSE;
}

BOOL CPrintSheetMaskSettingsView::IsSheetSideEffected(CPrintSheet& rPrintSheet, int nSide, BOOL bTrueIfNothing)
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheet));
	if ( ! pDI)
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheetSide));
	if ( ! pDI)		// nothing selected means all sheets effected
		return (bTrueIfNothing) ? TRUE : FALSE;
	
	pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheetSide));
	while (pDI)
	{
		CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
		int			 nCurSide	 = (int)pDI->m_pAuxData;
		if ( (pPrintSheet == &rPrintSheet) && (nCurSide == nSide) )
			return TRUE;
		pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheetSide));
	}

	return (bTrueIfNothing) ? CPrintSheetMaskSettingsView::IsSheetEffected(rPrintSheet, bTrueIfNothing) : FALSE;
}

void CPrintSheetMaskSettingsView::OnDraw(CDC* pDC)
{
	if ( ! m_maskSettingsTable.m_ColumnRefList.GetSize())
		return;

	m_DisplayList.BeginRegisterItems(pDC);

	m_maskSettingsTable.OnDraw(pDC);

	m_DisplayList.EndRegisterItems(pDC);
}

void CPrintSheetMaskSettingsView::OnEnSetfocusMasksettingsOverbleedEdit()
{
}

void CPrintSheetMaskSettingsView::OnKillFocusMaskSettingsOverbleedEdit()
{
}

void CPrintSheetMaskSettingsView::ModifyBleeds()
{
	UpdateData(TRUE);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (IsSheetSideEffected(rPrintSheet, FRONTSIDE))
			rPrintSheet.m_FrontSidePlates.SetGlobalMaskAllPlates(m_fOverbleed);

		if (IsSheetSideEffected(rPrintSheet, BACKSIDE))
			rPrintSheet.m_BackSidePlates.SetGlobalMaskAllPlates(m_fOverbleed);
	}

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView));
	CImpManDoc::GetDoc()->SetModifiedFlag();
	
	Invalidate();
	UpdateWindow();
}

void CPrintSheetMaskSettingsView::OnDeltaposMaskSettingsOverbleedSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(((CSpinButtonCtrl*)GetDlgItem(IDC_MASKSETTINGS_OVERBLEED_SPIN))->GetBuddy(), pNMUpDown->iDelta);

	ModifyBleeds();
}

BOOL CPrintSheetMaskSettingsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_pToolTip)            
		m_pToolTip->RelayEvent(pMsg);	

	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (GetFocus() == GetDlgItem(IDC_MASKSETTINGS_OVERBLEED_EDIT))
			{
				ModifyBleeds();
				return TRUE;
			}
		}

	return CFormView::PreTranslateMessage(pMsg);
}

void CPrintSheetMaskSettingsView::OnLButtonDown(UINT nFlags, CPoint point)
{
    //SetFocus();	// In order to deactivate eventually active InplaceEdit

	m_DisplayList.OnLButtonDown(point); // pass message to display list

	CFormView::OnLButtonDown(nFlags, point);
}

void CPrintSheetMaskSettingsView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CFormView::OnMouseMove(nFlags, point);
}

LRESULT CPrintSheetMaskSettingsView::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	m_DisplayList.OnMouseLeave();
	return TRUE;
}

BOOL CPrintSheetMaskSettingsView::OnActivatePDFSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been activated
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
	pPrintSheet->m_bDisplayOpen = ! pPrintSheet->m_bDisplayOpen;

	pDI->m_nState = CDisplayItem::Normal;

	OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CPrintSheetMaskSettingsView::OnSelectPDFSheet(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;
	CRect rect(pDI->m_BoundingRect.TopLeft(), CSize(12, pDI->m_BoundingRect.Height()));
	if (rect.PtInRect(point))
		OnActivatePDFSheet(pDI, point, 0);
	else
	{
		CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
		CPrintSheetNavigationView* pView = (pFrame) ? pFrame->GetPrintSheetNavigationView() : NULL;
		if (pView && (m_DisplayList.GetNumSelectedItems() == 1) )
		{
			pView->SetActPrintSheet((pDI) ? (CPrintSheet*)pDI->m_pData : NULL, TRUE);
			pView->Invalidate();
			pView->UpdateWindow();
		}
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView));
		InitData();
	}

	return TRUE;
}

BOOL CPrintSheetMaskSettingsView::OnDeselectPDFSheet(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	Invalidate();
	OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CPrintSheetMaskSettingsView::OnMouseMovePDFSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				rc.Inflate(-1, -1);
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(50, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 1), rc);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CPrintSheetMaskSettingsView::OnActivatePDFSheetSide(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	// object(s) has been activated
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if (pLayout)
		if ((int)pDI->m_pAuxData == FRONTSIDE)
			pLayout->m_FrontSide.m_ObjectList.m_bDisplayOpen = ! pLayout->m_FrontSide.m_ObjectList.m_bDisplayOpen;
		else
			pLayout->m_BackSide.m_ObjectList.m_bDisplayOpen  = ! pLayout->m_BackSide.m_ObjectList.m_bDisplayOpen;

	pDI->m_nState = CDisplayItem::Normal;

	OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CPrintSheetMaskSettingsView::OnSelectPDFSheetSide(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	CDisplayItem di = *pDI;
	CRect rect(pDI->m_BoundingRect.TopLeft(), CSize(12, pDI->m_BoundingRect.Height()));
	if (rect.PtInRect(point))
		OnActivatePDFSheetSide(pDI, point, 0);
	else
	{
		CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
		CPrintSheetNavigationView* pView = (pFrame) ? pFrame->GetPrintSheetNavigationView() : NULL;
		if (pView && (m_DisplayList.GetNumSelectedItems() == 1) )
		{
			pView->SetActPrintSheet((pDI) ? (CPrintSheet*)pDI->m_pData : NULL, TRUE);
			CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
			if (pFrame)
			{
				switch ((int)pDI->m_pAuxData)
				{
				case FRONTSIDE: pFrame->OnViewFrontside(); break;
				case BACKSIDE:  pFrame->OnViewBackside();  break;
				}
			}
			pView->Invalidate();
			pView->UpdateWindow();
		}
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView));
		InitData();
	}

	return TRUE;
}

BOOL CPrintSheetMaskSettingsView::OnDeselectPDFSheetSide(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	Invalidate();
	OnUpdate(NULL, 0, NULL);

	return TRUE;
}

BOOL CPrintSheetMaskSettingsView::OnMouseMovePDFSheetSide(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width(), pDI->m_BoundingRect.Height());
				rc.Inflate(-1, -1);
				COLORREF crHighlightColor = RGB(165,175,200);
				SolidBrush br(Color::Color(50, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

CPrintSheet* CPrintSheetMaskSettingsView::GetActPrintSheet()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheet));
	if ( ! pDI)
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetMaskSettingsView, PDFSheetSide));

	if (pDI)
		return (CPrintSheet*)pDI->m_pData;
	else
		return NULL;
}

void CPrintSheetMaskSettingsView::OnBnClickedMasksettingsReleaseBleed()
{
	CImpManDoc* pDoc	   = CImpManDoc::GetDoc();
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || ! pDoc)
		return;

	pView->UndoSave();

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	while (pDI)
	{
		pDoc->m_PrintSheetList.UnlockLocalMaskAllExposures((CExposure*)pDI->m_pData);

		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	}
    pDoc->SetModifiedFlag();
	if (pView)
   		pView->UpdateWindow();

	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if (pFrame)
		pFrame->m_bEnableReleaseBleedButton = FALSE;

	OnUpdate(NULL, 0, NULL);
	Invalidate();
	UpdateWindow();
}

void CPrintSheetMaskSettingsView::OnUpdateMasksettingsReleaseBleed(CCmdUI* pCmdUI)
{
	if ( ! m_releaseButton.m_hWnd)
		return;

	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	m_releaseButton.EnableWindow((pFrame) ? pFrame->m_bEnableReleaseBleedButton : FALSE);
}

void CPrintSheetMaskSettingsView::OnBnClickedMasksettingsAdjustContent()
{
	CImpManDoc* pDoc	   = CImpManDoc::GetDoc();
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || ! pDoc)
		return;

	pView->UndoSave();

	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	while (pDI)
	{
		pDoc->m_PrintSheetList.AdjustContent2LocalMaskAllExposures((CExposure*)pDI->m_pData);

		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
	}
    pDoc->SetModifiedFlag();
	if (pView)
   		pView->UpdateWindow();

	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if (pFrame)
		pFrame->m_bEnableReleaseBleedButton = FALSE;

	OnUpdate(NULL, 0, NULL);
	Invalidate();
	UpdateWindow();
}

void CPrintSheetMaskSettingsView::OnUpdateMasksettingsAdjustContent(CCmdUI* pCmdUI)
{
	if ( ! m_adjustButton.m_hWnd)
		return;

	CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)GetParentFrame();
	m_adjustButton.EnableWindow((pFrame) ? pFrame->m_bEnableReleaseBleedButton : FALSE);
}


// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
BOOL CPrintSheetMaskSettingsView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect;
		GetClientRect(scrollRect);
		scrollRect.top+= m_maskSettingsTable.m_headerFrameRect.bottom;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, scrollRect);

		//if (sizeScroll.cx != 0)				// Scroll header control horizontally
		//	m_maskSettingsTable.m_HeaderCtrl.MoveWindow(-x, m_maskSettingsTable.m_headerFrameRect.top, scrollRect.Width() + x, m_maskSettingsTable.m_nHeaderHeight);
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

#include "Afxpriv.h"

// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
void CPrintSheetMaskSettingsView::UpdateBars()
{
	// UpdateBars may cause window to be resized - ignore those resizings
	if (m_bInsideUpdate)
		return;         // Do not allow recursive calls

	// Lock out recursion
	m_bInsideUpdate = TRUE;

	// update the horizontal to reflect reality
	// NOTE: turning on/off the scrollbars will cause 'OnSize' callbacks
	ASSERT(m_totalDev.cx >= 0 && m_totalDev.cy >= 0);

	CRect rectClient;
	BOOL bCalcClient = TRUE;

	// allow parent to do inside-out layout first
	CWnd* pParentWnd = GetParent();
	if (pParentWnd != NULL)
	{
		// if parent window responds to this message, use just
		//  client area for scroll bar calc -- not "true" client area
		if ((BOOL)pParentWnd->SendMessage(WM_RECALCPARENT, 0,
			(LPARAM)(LPCRECT)&rectClient) != 0)
		{
			// use rectClient instead of GetTrueClientSize for
			//  client size calculation.
			bCalcClient = FALSE;
		}
	}

	CSize sizeClient;
	CSize sizeSb;

	if (bCalcClient)
	{
		// get client rect
		if (!GetTrueClientSize(sizeClient, sizeSb))
		{
			// no room for scroll bars (common for zero sized elements)
			CRect rect;
			GetClientRect(&rect);
			if (rect.right > 0 && rect.bottom > 0)
			{
				// if entire client area is not invisible, assume we have
				//  control over our scrollbars
				EnableScrollBarCtrl(SB_BOTH, FALSE);
			}
			m_bInsideUpdate = FALSE;
			return;
		}
	}
	else
	{
		// let parent window determine the "client" rect
		GetScrollBarSizes(sizeSb);
		sizeClient.cx = rectClient.right - rectClient.left;
		sizeClient.cy = rectClient.bottom - rectClient.top;
	}

	// enough room to add scrollbars
	CSize sizeRange;
	CPoint ptMove;
	CSize needSb;

	// get the current scroll bar state given the true client area
	GetScrollBarState(sizeClient, needSb, sizeRange, ptMove, bCalcClient);
	if (needSb.cx)
		sizeClient.cy -= sizeSb.cy;
	if (needSb.cy)
		sizeClient.cx -= sizeSb.cx;

	// first scroll the window as needed
	ScrollToDevicePosition(ptMove); // will set the scroll bar positions too

	// this structure needed to update the scrollbar page range
	SCROLLINFO info;
	info.fMask = SIF_PAGE|SIF_RANGE;
	info.nMin = 0;

	// now update the bars as appropriate
	EnableScrollBarCtrl(SB_HORZ, needSb.cx);
	if (needSb.cx)
	{
		info.nPage = sizeClient.cx;
		info.nMax = m_totalDev.cx-1;
		if (!SetScrollInfo(SB_HORZ, &info, TRUE))
			SetScrollRange(SB_HORZ, 0, sizeRange.cx, TRUE);
	}
	EnableScrollBarCtrl(SB_VERT, needSb.cy);
	if (needSb.cy)
	{
		info.nPage = sizeClient.cy;
		info.nMax = m_totalDev.cy-1;
		if (!SetScrollInfo(SB_VERT, &info, TRUE))
			SetScrollRange(SB_VERT, 0, sizeRange.cy, TRUE);
	}

	// remove recursion lockout
	m_bInsideUpdate = FALSE;
}

// This function is overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling
void CPrintSheetMaskSettingsView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);

	// This part is changed - rest is taken from MFC
	CRect scrollRect;
	GetClientRect(scrollRect);
	scrollRect.top+= m_maskSettingsTable.m_headerFrameRect.bottom;	// Protect header control from being vertically scrolled
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, scrollRect);
//	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y);	// original implementation
}

void CPrintSheetMaskSettingsView::SetScrollSizes(int nMapMode, SIZE sizeTotal,
	const SIZE& sizePage, const SIZE& sizeLine)
{
	ASSERT(sizeTotal.cx >= 0 && sizeTotal.cy >= 0);
	ASSERT(nMapMode > 0);
	ASSERT(nMapMode != MM_ISOTROPIC && nMapMode != MM_ANISOTROPIC);

	int nOldMapMode = m_nMapMode;
	m_nMapMode = nMapMode;
	m_totalLog = sizeTotal;

	//BLOCK: convert logical coordinate space to device coordinates
	{
		CWindowDC dc(NULL);
		ASSERT(m_nMapMode > 0);
		dc.SetMapMode(m_nMapMode);

		// total size
		m_totalDev = m_totalLog;
		dc.LPtoDP((LPPOINT)&m_totalDev);
		m_pageDev = sizePage;
		dc.LPtoDP((LPPOINT)&m_pageDev);
		m_lineDev = sizeLine;
		dc.LPtoDP((LPPOINT)&m_lineDev);
		if (m_totalDev.cy < 0)
			m_totalDev.cy = -m_totalDev.cy;
		if (m_pageDev.cy < 0)
			m_pageDev.cy = -m_pageDev.cy;
		if (m_lineDev.cy < 0)
			m_lineDev.cy = -m_lineDev.cy;
	} // release DC here

	// now adjust device specific sizes
	ASSERT(m_totalDev.cx >= 0 && m_totalDev.cy >= 0);
	if (m_pageDev.cx == 0)
		m_pageDev.cx = m_totalDev.cx / 10;
	if (m_pageDev.cy == 0)
		m_pageDev.cy = m_totalDev.cy / 10;
	if (m_lineDev.cx == 0)
		m_lineDev.cx = m_pageDev.cx / 10;
	if (m_lineDev.cy == 0)
		m_lineDev.cy = m_pageDev.cy / 10;

	if (m_hWnd != NULL)
	{
		// window has been created, invalidate now
		UpdateBars();
		if (nOldMapMode != m_nMapMode)
			Invalidate(TRUE);
	}
}
