// ImpManCtrls.cpp : implementation file
//

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//	Custom controls for Imposition Manager



#include "stdafx.h"
#include "Imposition Manager.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define COLOR_ITEM_HEIGHT		 20
#define COLOR_ITEM_HEIGHT_COMBO  16

/////////////////////////////////////////////////////////////////////////////
// CColorInfoListBox

void CColorInfoListBox::AddColorItem(COLORREF color, CString& strName, int nHeaderIndex)
{
	// add a listbox item
	CColorDefinition colorDef;
	colorDef.m_rgb			= color;
	colorDef.m_strColorName = strName;
	colorDef.m_nColorIndex	= 0;
	m_ColorDefTable.Add(colorDef);
	AddString((LPCTSTR)0);
		// Listbox does not have the LBS_HASSTRINGS style, so the
		//  normal listbox string is used to store the index
	SetItemData(GetCount() - 1, (DWORD)nHeaderIndex);

	CClientDC dc(this);
	int nWidth = COLOR_ITEM_HEIGHT + 5 + dc.GetTextExtent(strName).cx;
	SetHorizontalExtent(max(GetHorizontalExtent(), nWidth));
}

void CColorInfoListBox::DeleteColorItem(int nIndex)
{ 
	if (nIndex == CB_ERR)	// No item selected
		return;
	if (nIndex >= GetSizeOfColorDefTable())  // wrong index
		return;

	m_ColorDefTable.RemoveAt(nIndex);
	DeleteString(nIndex);
}

int CColorInfoListBox::GetSizeOfColorDefTable()
{
	return (m_ColorDefTable.GetSize());
}

/////////////////////////////////////////////////////////////////////////////

void CColorInfoListBox::MeasureItem(LPMEASUREITEMSTRUCT lpMIS)
{
	// all items are of fixed size
	// must use LBS_OWNERDRAWVARIABLE for this to work
	lpMIS->itemHeight = COLOR_ITEM_HEIGHT - 4;
}

void CColorInfoListBox::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC	= CDC::FromHandle(lpDIS->hDC);
	int nIndex	= (int)lpDIS->itemID; // Index in item data
	if ((nIndex < 0) || (nIndex >= m_ColorDefTable.GetSize()))
		return;

	int nOff			 = 4;
	int nColorRectWidth  = 12;
	int nColorRectHeight = 12;

	COLORREF cr = m_ColorDefTable[nIndex].m_rgb;
	if ( (lpDIS->itemAction & ODA_DRAWENTIRE))
	{
		CRect rect(lpDIS->rcItem.left + 1, lpDIS->rcItem.top, lpDIS->rcItem.left + 1 + nColorRectWidth, lpDIS->rcItem.top + nColorRectHeight);
		rect.OffsetRect(0, 1);
		if (m_ColorDefTable[nIndex].m_strColorName == _T("Composite"))
			pDC->DrawIcon(rect.left, rect.top + 1, theApp.LoadIcon(IDI_CMYK));
		else
		{
			// Paint the color item in the color requested
			CBrush colorBrush(cr);
			CPen   framePen(PS_SOLID, 1, DARKGRAY);
			CBrush* pOldBrush = pDC->SelectObject(&colorBrush);
			CPen*	pOldPen	  = pDC->SelectObject(&framePen);
			pDC->Rectangle(rect);
			pDC->SelectObject(pOldBrush);
			pDC->SelectObject(pOldPen);
			colorBrush.DeleteObject();
			framePen.DeleteObject();
		}
		rect = lpDIS->rcItem;
		rect.left+= COLOR_ITEM_HEIGHT + 5 + nOff;
	}

	CString strColorName = m_ColorDefTable[nIndex].m_strColorName;
	if (strColorName.CompareNoCase(_T("Composite")) == 0)
		strColorName.LoadString(IDS_PROCESS);

	if (m_nSepStatus == CColorDefinition::Mixed)
		if ((int)lpDIS->itemData == -1)
			strColorName += _T(" (unsep.)");
		else
			strColorName += _T(" (sep.)");

	pDC->SetBkMode(TRANSPARENT);
	if ((lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// item has been selected - hilite 
		CRect rect = lpDIS->rcItem;
		rect.left  = nColorRectWidth + nOff;
		CBrush fillBrush(::GetSysColor(COLOR_HIGHLIGHT));
		pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
		pDC->FillRect(rect, &fillBrush);
		pDC->DrawText(strColorName, rect, DT_LEFT | DT_VCENTER);
		fillBrush.DeleteObject();
	}

	if (!(lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// Item has been de-selected - remove hilite
		CRect rect = lpDIS->rcItem;
		rect.left  = nColorRectWidth + nOff;
		CBrush fillBrush(pDC->GetBkColor());
		pDC->SetTextColor(::GetSysColor(COLOR_MENUTEXT));
		pDC->FillRect(rect, &fillBrush);
		pDC->DrawText(strColorName, rect, DT_LEFT | DT_VCENTER);
		fillBrush.DeleteObject();
	}						
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Custom CColorListBox - containing colors

void CColorListBox::AddColorItem(COLORREF color, CString& strName, int nColorIndex, int nColorType)
{
	// add a listbox item
	CColorDefinition colorDef;
	colorDef.m_rgb			= color;
	colorDef.m_strColorName = strName;
	colorDef.m_nColorIndex	= nColorIndex;
	colorDef.m_nColorType	= nColorType;
	m_ColorDefTable.Add(colorDef);
	AddString((LPCTSTR)0);
		// Listbox does not have the LBS_HASSTRINGS style, so the
		//  normal listbox string is used to store the index
}

void CColorListBox::DeleteColorItem(int nIndex)
{
	if (nIndex == CB_ERR)	// No item selected
		return;
	if (nIndex >= GetSizeOfColorDefTable())  // wrong index
		return;

	m_ColorDefTable.RemoveAt(nIndex);
	DeleteString(nIndex);
}

int CColorListBox::GetSizeOfColorDefTable()
{
	return (m_ColorDefTable.GetSize());
}

/////////////////////////////////////////////////////////////////////////////

void CColorListBox::MeasureItem(LPMEASUREITEMSTRUCT lpMIS)
{
	// all items are of fixed size
	// must use LBS_OWNERDRAWVARIABLE for this to work
	lpMIS->itemHeight = COLOR_ITEM_HEIGHT;
}

/*
void CColorListBox::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	int	nIndex = (int)lpDIS->itemID; // Index in item data

	if ((nIndex < 0) || (nIndex >= m_ColorDefTable.GetSize()))
		return;

	CDC dc;
	dc.Attach(lpDIS->hDC);

	CRect rc(lpDIS->rcItem);
	if (lpDIS->itemState & ODS_FOCUS)
		dc.DrawFocusRect(&rc);

	COLORREF cr = (lpDIS->itemState & ODS_SELECTED) ? 
		::GetSysColor(COLOR_HIGHLIGHT) :
		dc.GetBkColor();

	CBrush brushFill(cr);
	cr = dc.GetTextColor();

	if (lpDIS->itemState & ODS_SELECTED)
		dc.SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));

	int nBkMode = dc.SetBkMode(TRANSPARENT);
	dc.FillRect(&rc, &brushFill);

	CString	strColor = m_ColorDefTable[nIndex].m_strColorName;
	cr				 = m_ColorDefTable[nIndex].m_rgb;
	rc.left += 50;
	dc.TextOut(rc.left,rc.top,strColor,strColor.GetLength());
	rc.left -= 45;
	rc.top += 2;
	rc.bottom -= 2;
	rc.right = rc.left + 40;
	CBrush brush(cr);
	CBrush* pOldBrush = dc.SelectObject(&brush);
	dc.Rectangle(rc);

	dc.SelectObject(pOldBrush);
	dc.SetTextColor(cr);
	dc.SetBkMode(nBkMode);
	
	dc.Detach();
}
*/
void CColorListBox::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC	= CDC::FromHandle(lpDIS->hDC);
	int nIndex	= (int)lpDIS->itemID; // Index in item data
	if ((nIndex < 0) || (nIndex >= m_ColorDefTable.GetSize()))
		return;

#ifdef CTP
	int nOff = 20;
#else
	int nOff = 0;
#endif

	COLORREF cr = m_ColorDefTable[nIndex].m_rgb;
	if (m_printStates.GetSize())
		if ( ! m_printStates[nIndex])
			cr = LIGHTGRAY;

	if (lpDIS->itemAction & ODA_DRAWENTIRE)
	{
		// Paint the color item in the color requested
		CBrush colorBrush(cr);
		CRect rect = lpDIS->rcItem;
		rect.right = rect.left + 21;
		rect.DeflateRect(1, 1);
		pDC->FillRect(rect, &colorBrush);
#ifdef CTP
		rect = lpDIS->rcItem;
		rect.right = rect.left + COLOR_ITEM_HEIGHT + 17;
		rect.left += 20;
		rect.top+= 1;
		CString strColorIndex;
		strColorIndex.Format(_T("%d "), m_ColorDefTable[nIndex].m_nColorIndex);
		pDC->SetTextColor(BLACK);
		pDC->DrawText(strColorIndex, rect, DT_RIGHT | DT_VCENTER);
#endif
		CBrush frameBr(BLACK);
		rect = lpDIS->rcItem;
		rect.right = rect.left + COLOR_ITEM_HEIGHT + nOff;
		rect.DeflateRect(1, 1);
		pDC->FrameRect(rect, &frameBr);
#ifdef CTP
		pDC->MoveTo(rect.right - 19, rect.top);
		pDC->LineTo(rect.right - 19, rect.bottom);
#endif
		rect = lpDIS->rcItem;
		rect.left+= COLOR_ITEM_HEIGHT + 5 + nOff;
		pDC->SetTextColor(BLACK);
		if (m_printStates.GetSize())
			if ( ! m_printStates[nIndex])
				pDC->SetTextColor(DARKGRAY);
		pDC->DrawText(m_ColorDefTable[nIndex].m_strColorName, rect, DT_LEFT | DT_VCENTER);
	}

	CBrush fillBrush;
	CRect  rect = lpDIS->rcItem;
	rect.DeflateRect(1, 1);
	rect.left  += COLOR_ITEM_HEIGHT - 2 + nOff;
	pDC->SetBkMode(TRANSPARENT);

	if ((lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
//	if (lpDIS->itemState & ODS_FOCUS)
	{
		// item has been selected - hilite 
		fillBrush.CreateSolidBrush(::GetSysColor(COLOR_HIGHLIGHT));
		pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
		if (m_printStates.GetSize())
			if ( ! m_printStates[nIndex])
				pDC->SetTextColor(DARKGRAY);
		pDC->FillRect(rect, &fillBrush);
		rect.left+= 5;
		pDC->DrawText(m_ColorDefTable[nIndex].m_strColorName, rect, DT_LEFT | DT_VCENTER);
	}

	if (!(lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
//	if (!(lpDIS->itemState & ODS_FOCUS)) 
	{
		// Item has been de-selected - remove hilite
		fillBrush.CreateSolidBrush(pDC->GetBkColor());
		pDC->SetTextColor(::GetSysColor(COLOR_MENUTEXT));
		if (m_printStates.GetSize())
			if ( ! m_printStates[nIndex])
				pDC->SetTextColor(DARKGRAY);
		pDC->FillRect(rect, &fillBrush);
		rect.left+= 5;
		pDC->DrawText(m_ColorDefTable[nIndex].m_strColorName, rect, DT_LEFT | DT_VCENTER);
	}						
}

/////////////////////////////////////////////////////////////////////////////
// Custom ComboBox - containing colors

/////////////////////////////////////////////////////////////////////////////
// The C++ class ColorComboBox can be made visible to the dialog manager
//   by registering a window class for it
// The C++ class 'CColorComboBoxExported' is used to implement the
//   creation and destruction of a C++ object as if it were just
//   a normal Windows control.
// In order to hook in the class creation we must provide a special
//   WndProc to create the C++ object and override the PostNcDestroy
//   message to destroy it

class CColorComboBoxExported : public CColorComboBox      // WNDCLASS exported class
{
public:
	CColorComboBoxExported() { };
	BOOL RegisterControlClass();

// Implementation: (all is implementation since the public interface of
//    this class is identical to CColorComboBox)
protected:
	virtual void PostNcDestroy();
	static LRESULT CALLBACK EXPORT WndProcHook(HWND, UINT, WPARAM, LPARAM);
	static WNDPROC lpfnSuperComboBox;

	friend class CColorComboBox;       // for RegisterControlClass

	//{{AFX_MSG(CColorComboBoxExported)
	afx_msg int OnNcCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP();
};

/////////////////////////////////////////////////////////////////////////////
// Special create hooks

LRESULT CALLBACK EXPORT
CColorComboBoxExported::WndProcHook(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// create new item and attach it
	CColorComboBoxExported* pBox = new CColorComboBoxExported();
	pBox->Attach(hWnd);

	// set up wndproc to AFX one, and call it
	pBox->m_pfnSuper = CColorComboBoxExported::lpfnSuperComboBox;
	::SetWindowLong(hWnd, GWLP_WNDPROC, (DWORD)AfxWndProc);

#ifndef _MAC
	// Since this window is really an combobox control but does not
	//  have "combobox" as its class name, to make it work correctly
	//  with CTL3D, we have to tell CTL3D that is "really is" an
	//  combobox control.  This uses a relatively new API in CTL3D
	//  called Ctl3dSubclassCtlEx.
//	pBox->SubclassCtl3d(CTL3D_COMBO_CTL);
#endif

	// then call it for this first message
#ifdef STRICT
	return ::CallWindowProc(AfxWndProc, hWnd, msg, wParam, lParam);
#else
	return ::CallWindowProc((FARPROC)AfxWndProc, hWnd, msg, wParam, lParam);
#endif
}

BEGIN_MESSAGE_MAP(CColorComboBoxExported, CColorComboBox)
	//{{AFX_MSG_MAP(CColorComboBoxExported)
	ON_WM_NCCREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int CColorComboBoxExported::OnNcCreate(LPCREATESTRUCT lpCreateStruct)
{
	// special create hook
	// example of stripping the sub-style bits from the style specified
	// in the dialog template to use for some other reason
	//m_wParseStyle = LOWORD(lpCreateStruct->style);
	DWORD dwBoxStyle = MAKELONG(CBS_NOINTEGRALHEIGHT|CBS_DROPDOWNLIST|CBS_OWNERDRAWFIXED, 
								HIWORD(lpCreateStruct->style));

	::SetWindowLong(m_hWnd, GWL_STYLE, dwBoxStyle);
	lpCreateStruct->style = dwBoxStyle;
	return CColorComboBox::OnNcCreate(lpCreateStruct);
}

void CColorComboBoxExported::PostNcDestroy()
{
	// needed to clean up the C++ CWnd object
	delete this;
}

/////////////////////////////////////////////////////////////////////////////
// Routine to register the class

WNDPROC CColorComboBoxExported::lpfnSuperComboBox = NULL;

BOOL CColorComboBox::RegisterControlClass()
{
	WNDCLASS wcls;

	// check to see if class already registered
	static const TCHAR szClass[] = _T("colorcombobox");
	if (::GetClassInfo(AfxGetInstanceHandle(), szClass, &wcls))
	{
		// name already registered - ok if it was us
		return (wcls.lpfnWndProc == (WNDPROC)CColorComboBoxExported::WndProcHook);
	}

	// Use standard "combobox" control as a template.
	VERIFY(::GetClassInfo(NULL, _T("combobox"), &wcls));
	CColorComboBoxExported::lpfnSuperComboBox = wcls.lpfnWndProc;

	// set new values
	wcls.lpfnWndProc = CColorComboBoxExported::WndProcHook;
	wcls.hInstance = AfxGetInstanceHandle();
	wcls.lpszClassName = szClass;
	return (RegisterClass(&wcls) != 0);
}

/////////////////////////////////////////////////////////////////////////////

void CColorComboBox::AddColorItem(COLORREF color, CString& strName)
{
	// add a listbox item
	CColorDefinition colorDef;
	colorDef.m_rgb			= color;
	colorDef.m_strColorName = strName;
	m_ColorDefTable.Add(colorDef);
	AddString((LPCTSTR)0);
		// Listbox does not have the LBS_HASSTRINGS style, so the
		//  normal listbox string is used to store the index
}

void CColorComboBox::DeleteColorItem(int nIndex)
{
	if (nIndex == CB_ERR)	// No item selected
		return;
	if (nIndex >= GetSizeOfColorDefTable())  // wrong index
		return;

	m_ColorDefTable.RemoveAt(nIndex);
	DeleteString(nIndex);
}

int CColorComboBox::GetSizeOfColorDefTable()
{
	return (m_ColorDefTable.GetSize());
}

/////////////////////////////////////////////////////////////////////////////

void CColorComboBox::MeasureItem(LPMEASUREITEMSTRUCT lpMIS)
{
	// all items are of fixed size
	// must use LBS_OWNERDRAWVARIABLE for this to work
	lpMIS->itemHeight = COLOR_ITEM_HEIGHT_COMBO;
}

void CColorComboBox::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC	= CDC::FromHandle(lpDIS->hDC);
	int nIndex	= (int)lpDIS->itemID; // Index in item data
	if ((nIndex < 0) || (nIndex >= m_ColorDefTable.GetSize()))
		return;

	COLORREF cr = m_ColorDefTable[nIndex].m_rgb;

	if (lpDIS->itemAction & ODA_DRAWENTIRE)
	{
		// Paint the color item in the color requested
		CBrush colorBrush(cr);
		CRect  rect = lpDIS->rcItem;
		rect.right	= rect.left + COLOR_ITEM_HEIGHT_COMBO;
		rect.DeflateRect(1, 1);
		pDC->FillRect(rect, &colorBrush);
		CBrush frameBr(BLACK);
		pDC->FrameRect(rect, &frameBr);

		rect = lpDIS->rcItem;
		rect.left+= COLOR_ITEM_HEIGHT_COMBO + 5;
		pDC->DrawText(m_ColorDefTable[nIndex].m_strColorName, rect, DT_LEFT | DT_VCENTER);
	}

	CBrush fillBrush;
	CRect  rect = lpDIS->rcItem;
	rect.DeflateRect(1, 1);
	rect.left  += COLOR_ITEM_HEIGHT_COMBO - 2;
	pDC->SetBkMode(TRANSPARENT);

	if ((lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
//	if (lpDIS->itemState & ODS_FOCUS)
	{
		// item has been selected - hilite 
		fillBrush.CreateSolidBrush(::GetSysColor(COLOR_HIGHLIGHT));
		pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
		pDC->FillRect(rect, &fillBrush);
		rect.left+= 5;
		pDC->DrawText(m_ColorDefTable[nIndex].m_strColorName, rect, DT_LEFT | DT_VCENTER);
	}

	if (!(lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
//	if (!(lpDIS->itemState & ODS_FOCUS)) 
	{
		// Item has been de-selected - remove hilite
		fillBrush.CreateSolidBrush(pDC->GetBkColor());
		pDC->SetTextColor(::GetSysColor(COLOR_MENUTEXT));
		pDC->FillRect(rect, &fillBrush);
		rect.left+= 5;
		pDC->DrawText(m_ColorDefTable[nIndex].m_strColorName, rect, DT_LEFT | DT_VCENTER);
	}						
}

