#pragma once


// CDlgProductionProfileRules-Dialogfeld

class CDlgProductionProfileRules : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionProfileRules)

public:
	CDlgProductionProfileRules(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionProfileRules();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTIONPROFILE_RULES };


protected:
	CListBox	  m_rulesList;
	CRichEditCtrl m_rulesEdit;
	int			  m_nRulesRelation;


public:
	void InitData();
	void SaveData();
	BOOL CheckSelection();



protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedAddRuleButton();
	afx_msg void OnBnClickedModifyRuleButton();
	afx_msg void OnBnClickedRemoveRuleButton();
	afx_msg void OnUpdateModifyRuleButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRemoveRuleButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRuleRelationAnd(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRuleRelationOr(CCmdUI* pCmdUI);
	afx_msg void OnLbnDblclkRulesList();
	afx_msg void OnBnClickedRuleRelationAnd();
	afx_msg void OnBnClickedRuleRelationOr();
	afx_msg void OnBnClickedRulesBrowsePdfpath();
};
