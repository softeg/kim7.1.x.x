// MarkPreViewWindow.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "MarkSourceWindow.h"
#include "MarkPreViewWindow.h"
#include "DlgCropMarks.h"
#include "DlgSelectSectionMark.h"
#include "DlgSectionMark.h"
#include "MarkPivotControl.h"
#include "PrintSheetMarksView.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CMarkPreViewWindow

CMarkPreViewWindow::CMarkPreViewWindow()
{
	m_bLockWindow  = FALSE;
	m_bOnSetCursor = FALSE;
	m_fZoomFactor  = 1.0f;
	m_nBorder	   = 6;

	mScrollPos = CPoint(0, 0);
	mScrollSize = CSize(0, 0);
}

CMarkPreViewWindow::~CMarkPreViewWindow()
{
}

BEGIN_MESSAGE_MAP(CMarkPreViewWindow, CStatic)
	//{{AFX_MSG_MAP(CMarkPreViewWindow)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMarkPreViewWindow message handlers

void CMarkPreViewWindow::OnPaint() 
{
//	if (m_bLockWindow)
//		return;

////////////////////////////////////////////////////////////////////////////////////////////////

	CImpManDoc*	 pDoc	     = CImpManDoc::GetDoc();
	CPrintSheet* pPrintSheet = pDoc->GetActPrintSheet();
	int			 nSide	     = pDoc->GetActSight();

// Determine the client-rectangle of PreView-Window
	CRect clientRect;
	GetClientRect(&clientRect);
	clientRect.DeflateRect(m_nBorder, m_nBorder); // Make a logical border in PreView-Window

	CMark* pMark = NULL;//((CPrintSheetMarksView*)GetParent())->RetrieveDialog();

	if ( ! pMark)
		return;

	float  fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;
	pMark->GetSheetTrimBox(pPrintSheet, (pMark->m_nType == CMark::SectionMark) ? 0 : nSide,	0, fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop);

	float fFactorWidth  = (fTrimBoxRight - fTrimBoxLeft)   / (float)clientRect.Width();
	float fFactorHeight = (fTrimBoxTop   - fTrimBoxBottom) / (float)clientRect.Height();
	m_fScale  = max(fFactorWidth, fFactorHeight);
	m_fScale /= m_fZoomFactor;
	
	CRgn rgn;
	rgn.CreateRectRgn(clientRect.left, clientRect.top, clientRect.right, clientRect.bottom);
	CPaintDC dc(this);
	dc.SelectClipRgn(&rgn, RGN_COPY);	// Set the clipping region

	dc.SetMapMode(MM_ISOTROPIC); 
	dc.SetWindowExt(CSize(clientRect.right, clientRect.bottom));
	dc.SetWindowOrg(0, clientRect.bottom);
	dc.SetViewportExt(clientRect.right, -clientRect.bottom);

    CBrush brush(LIGHTGRAY);
    dc.FillRect(&clientRect, &brush);	// Erase the background
    brush.DeleteObject();

	CPen redPen;
	redPen.CreatePen(PS_SOLID, 0, RED);

	CPen* pOldPen = dc.SelectObject(&redPen);
//	pMark->DrawDlgPreview(&dc, pPrintSheet, nSide, m_fScale, CSize(m_nBorder - mScrollPos.x, m_nBorder - mScrollPos.y));
	delete pMark;

	dc.SelectObject(pOldPen);

//	m_bLockWindow = TRUE;
}


BOOL CMarkPreViewWindow::OnEraseBkgnd(CDC* /*pDC*/) 
{
	return TRUE;
}


BOOL CMarkPreViewWindow::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	m_bOnSetCursor = TRUE;
	return CStatic::OnSetCursor(pWnd, nHitTest, message);
}


void CMarkPreViewWindow::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CRectTracker tracker;

	CRect  clientRect;
	GetClientRect(&clientRect);

	if (tracker.TrackRubberBand(this, point, TRUE))	
	{
		tracker.m_rect.NormalizeRect(); 

		if (tracker.m_rect.right > clientRect.right)	// if the rubber-rectangle is outside 
			tracker.m_rect.right = clientRect.right;	// of the PreView-Window
		if (tracker.m_rect.bottom > clientRect.bottom)
			tracker.m_rect.bottom = clientRect.bottom;
		if (tracker.m_rect.top < 0)
			tracker.m_rect.top = 0;
		if (tracker.m_rect.left < 0)
			tracker.m_rect.left = 0;

		if (mScrollSize == CSize(0, 0))	// 0,0 comes from the constructor
			mScrollSize = CSize(clientRect.Width(), clientRect.Height());
		
		CPoint ScrollPos;
		ScrollPos.x = m_pScrollBarWidth->GetScrollPos();
		ScrollPos.y = m_pScrollBarHeight->GetScrollPos();
		ScrollPos.x += (long)((float)tracker.m_rect.left / m_fZoomFactor);
		ScrollPos.y += (long)((float)tracker.m_rect.top  / m_fZoomFactor);

		float fOldZoomFaktor = m_fZoomFactor;
		m_fZoomFactor = (float)clientRect.Width()/(float)tracker.m_rect.Width();
		mScrollSize.cx = (long)((float)mScrollSize.cx * m_fZoomFactor);
		mScrollSize.cy = (long)((float)mScrollSize.cy * m_fZoomFactor);
		ScrollPos.x  = (long)(ScrollPos.x * m_fZoomFactor);
		ScrollPos.y  = (long)(ScrollPos.y * m_fZoomFactor);
		m_fZoomFactor *= fOldZoomFaktor;
		mScrollPos.x = ScrollPos.x;
		mScrollPos.y = mScrollSize.cy - (long)(tracker.m_rect.bottom * m_fZoomFactor);

		SCROLLINFO info;
		info.fMask = SIF_ALL;
		info.nMin = 0;

		info.nPage = clientRect.Height();
		info.nMax  = mScrollSize.cy;
		info.nPos  = ScrollPos.y;
		m_pScrollBarHeight->SetScrollInfo(&info, TRUE);

		info.nPage = clientRect.Width();
		info.nMax  = mScrollSize.cx;
		info.nPos  = ScrollPos.x;
		m_pScrollBarWidth->SetScrollInfo(&info, TRUE);

		m_pScrollBarHeight->ShowScrollBar(TRUE);
		m_pScrollBarWidth->ShowScrollBar(TRUE);
	}
	else
		if (GetKeyState(VK_CONTROL) < 0)
		{
			m_fZoomFactor  = 1.0f;
			mScrollPos  = CPoint(0, 0);
			mScrollSize = CSize(clientRect.Width(), clientRect.Height());
			m_pScrollBarWidth->SetScrollPos(0, FALSE);
			m_pScrollBarHeight->SetScrollPos(0, FALSE);
			m_pScrollBarHeight->ShowScrollBar(FALSE);
			m_pScrollBarWidth->ShowScrollBar(FALSE);
		}

	m_bLockWindow = FALSE;
	Invalidate();
	
	CStatic::OnLButtonDown(nFlags, point);
}
