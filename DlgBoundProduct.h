#pragma once


#include "DlgNumeration.h"
#include "BindingDefsSelection.h"


// CPPCCustomPaintFrame

class CPPCCustomPaintFrame : public CStatic
{
	DECLARE_DYNAMIC(CPPCCustomPaintFrame)

public:
	CPPCCustomPaintFrame();
	virtual ~CPPCCustomPaintFrame();

public:
	void DrawBoundProductPartColors (CDC* pDC, CRect rcFrame, int nBoundProductPartIndex);
	void DrawBoundProductPartDetails(CDC* pDC, CRect rcFrame, int nBoundProductPartIndex);

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};



// CDlgBoundProduct-Dialogfeld

class CDlgBoundProduct : public CDialog
{
	DECLARE_DYNAMIC(CDlgBoundProduct)

public:
	CDlgBoundProduct(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgBoundProduct();

// Dialogfelddaten
public:
	//{{AFX_DATA(COrderDataView)
	enum { IDD = IDD_BOUNDPRODUCT };
	CSpinButtonCtrl	m_ODQuantitySpin;
	CSpinButtonCtrl	m_ODPagesSpin;
	CComboBox	m_ODFormatCombo;
	CSpinButtonCtrl	m_ODWidthSpin;
	CSpinButtonCtrl	m_ODHeightSpin;
	CString m_strODProductName;
	int		m_nODPages;
	int		m_nODBindingMethod;
	float	m_fODHeight;
	float	m_fODWidth;
	int		m_nODPageOrientation;
	int		m_nODQuantity;
	CString	m_strProductPartName1;
	CString	m_strProductPartName2;
	CString	m_strProductPartName3;
	CString	m_strProductPartName4;
	CString	m_strProductPartName5;
	CString	m_strProductPartName6;
	CString	m_strProductPartName7;
	CString	m_strProductPartName8;
	int		m_nProductPartNumPages1;
	int		m_nProductPartNumPages2;
	int		m_nProductPartNumPages3;
	int		m_nProductPartNumPages4;
	int		m_nProductPartNumPages5;
	int		m_nProductPartNumPages6;
	int		m_nProductPartNumPages7;
	int		m_nProductPartNumPages8;
	CString m_strODBindingComment;
	CString m_strNumbering1;
	CString m_strNumbering2;
	CString m_strNumbering3;
	CString m_strNumbering4;
	CString m_strNumbering5;
	CString m_strNumbering6;
	CString m_strNumbering7;
	CString m_strNumbering8;
	//}}AFX_DATA


// Attribute
public:
	CBoundProduct				m_boundProduct;
	CBrush						m_bkBrush;
	CString						m_strODFormatName;
	CPageFormatSelection		m_ODPageFormatSelection;
	CString						m_strBindingName;
	CBindingDefsSelection		m_bindingDefsSelection;
	CImageList					m_bindingImageList;
	CDlgNumeration*		 		m_pDlgNumeration;
	CPPCCustomPaintFrame		m_productPart1Colors, m_productPart2Colors, m_productPart3Colors, m_productPart4Colors;
	CPPCCustomPaintFrame		m_productPart5Colors, m_productPart6Colors, m_productPart7Colors, m_productPart8Colors;
	CPPCCustomPaintFrame		m_productPart1Details, m_productPart2Details, m_productPart3Details, m_productPart4Details;
	CPPCCustomPaintFrame		m_productPart5Details, m_productPart6Details, m_productPart7Details, m_productPart8Details;

protected:
	CString						m_strOldFormatName;
	float						m_fOldWidth, m_fOldHeight;
	int							m_nOldPageOrientation;


// Operationen
public:
	void	InitData();
	void	SaveData();
	BOOL	CheckData();
	void	KeepCurrentPageFormat();
	BOOL	PageFormatChanged();
	void	ChangeProductPartName(int nIndex);
	void	ChangeProductPartNumPages(int nIndex, int nDelta);
	void	ChangeNumeration(int nIndex);
	void	InitNumberingString(int nIndex);
	void	RemoveProductPart(int nProductPartIndex);
	void	InitProductPartColor(int nProductPartIndex);
	void	UpdateColors(int nProductPartIndex);
	void	OpenDetails(int nProductPartIndex);
	void	UpdateDetails(int nProductPartIndex);


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgBoundProduct)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDeltaposODHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposODWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEnKillfocusOdHeight();
	afx_msg void OnEnKillfocusOdWidth();
	afx_msg void OnODLandscape();
	afx_msg void OnODPortrait();
	afx_msg void OnSelchangeODFormat();
	afx_msg void OnSelchangeODBinding();
	afx_msg void OnDeltaposODQuantitySpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedOdDefineBinding();
	afx_msg void OnBnClickedOdModifyFormat();
	afx_msg void OnCbnSelchangeProductpart1Combo();
	afx_msg void OnCbnSelchangeProductpart2Combo();
	afx_msg void OnCbnSelchangeProductpart3Combo();
	afx_msg void OnCbnSelchangeProductpart4Combo();
	afx_msg void OnCbnSelchangeProductpart5Combo();
	afx_msg void OnCbnSelchangeProductpart6Combo();
	afx_msg void OnCbnSelchangeProductpart7Combo();
	afx_msg void OnCbnSelchangeProductpart8Combo();
	afx_msg void OnKillfocusProductpart1Combo();
	afx_msg void OnKillfocusProductpart2Combo();
	afx_msg void OnKillfocusProductpart3Combo();
	afx_msg void OnKillfocusProductpart4Combo();
	afx_msg void OnKillfocusProductpart5Combo();
	afx_msg void OnKillfocusProductpart6Combo();
	afx_msg void OnKillfocusProductpart7Combo();
	afx_msg void OnKillfocusProductpart8Combo();
	afx_msg void OnDeltaposProductpart1NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposProductpart2NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposProductpart3NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposProductpart4NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposProductpart5NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposProductpart6NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposProductpart7NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposProductpart8NumPagesSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusNumPages1();
	afx_msg void OnKillfocusNumPages2();
	afx_msg void OnKillfocusNumPages3();
	afx_msg void OnKillfocusNumPages4();
	afx_msg void OnKillfocusNumPages5();
	afx_msg void OnKillfocusNumPages6();
	afx_msg void OnKillfocusNumPages7();
	afx_msg void OnKillfocusNumPages8();
	afx_msg void OnEnChangeProductpart1Numpages();
	afx_msg void OnEnChangeProductpart2Numpages();
	afx_msg void OnEnChangeProductpart3Numpages();
	afx_msg void OnEnChangeProductpart4Numpages();
	afx_msg void OnEnChangeProductpart5Numpages();
	afx_msg void OnEnChangeProductpart6Numpages();
	afx_msg void OnEnChangeProductpart7Numpages();
	afx_msg void OnEnChangeProductpart8Numpages();
	afx_msg void OnBnClickedProductpart1NumButton();
	afx_msg void OnBnClickedProductpart2NumButton();
	afx_msg void OnBnClickedProductpart3NumButton();
	afx_msg void OnBnClickedProductpart4NumButton();
	afx_msg void OnBnClickedProductpart5NumButton();
	afx_msg void OnBnClickedProductpart6NumButton();
	afx_msg void OnBnClickedProductpart7NumButton();
	afx_msg void OnBnClickedProductpart8NumButton();
	afx_msg void OnBnClickedProductpart1DetailsButton();
	afx_msg void OnBnClickedProductpart2DetailsButton();
	afx_msg void OnBnClickedProductpart3DetailsButton();
	afx_msg void OnBnClickedProductpart4DetailsButton();
	afx_msg void OnBnClickedProductpart5DetailsButton();
	afx_msg void OnBnClickedProductpart6DetailsButton();
	afx_msg void OnBnClickedProductpart7DetailsButton();
	afx_msg void OnBnClickedProductpart8DetailsButton();
	afx_msg void OnBnClickedProductpart1Remove();
	afx_msg void OnBnClickedProductpart2Remove();
	afx_msg void OnBnClickedProductpart3Remove();
	afx_msg void OnBnClickedProductpart4Remove();
	afx_msg void OnBnClickedProductpart5Remove();
	afx_msg void OnBnClickedProductpart6Remove();
	afx_msg void OnBnClickedProductpart7Remove();
	afx_msg void OnBnClickedProductpart8Remove();
	afx_msg void OnBnClickedProductpart1ColorsButton();
	afx_msg void OnBnClickedProductpart2ColorsButton();
	afx_msg void OnBnClickedProductpart3ColorsButton();
	afx_msg void OnBnClickedProductpart4ColorsButton();
	afx_msg void OnBnClickedProductpart5ColorsButton();
	afx_msg void OnBnClickedProductpart6ColorsButton();
	afx_msg void OnBnClickedProductpart7ColorsButton();
	afx_msg void OnBnClickedProductpart8ColorsButton();
	//}}AFX_MSG
	afx_msg void OnBnClickedOk();
};
