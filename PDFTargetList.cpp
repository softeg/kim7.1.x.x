// PDFTargetList.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "JDFOutputImposition.h"
#include "XMLCommon.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




////////////////////////////// CPDFTargetList /////////////////////////////
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CPDFTargetList, CObject, VERSIONABLE_SCHEMA | 1)


CPDFTargetList::CPDFTargetList()
{
}



/////////////////////////////////////////////////////////////////////////////
// CPDFTargetProperties

IMPLEMENT_SERIAL(CPDFTargetProperties, CObject, VERSIONABLE_SCHEMA | 11)

CPDFTargetProperties::CPDFTargetProperties()
{
	m_nType = TypePDFFolder;
}


CPDFTargetProperties::CPDFTargetProperties(int nType, CString strTargetName, CString strLocation, CMediaSize mediaSize)
{
	m_nType						= nType;
	m_strTargetName				= strTargetName;
	m_strLocation				= strLocation;
	m_strLocationBack			= _T("");
	m_nPSLevel					= 2;
	m_bEmitHalftones			= FALSE;
	m_nJDFCompatibility 		= JDFComp13;
	m_bJDFMarksInFolder 		= FALSE;
	m_strJDFMarksFolder 		= _T("");
	m_bJDFOutputMime			= FALSE;
	m_bOutputCuttingData		= FALSE;
	m_strCuttingDataFolder		= _T("");
	m_strCuttingDataFilename	= _T("");

	switch (nType)
	{
	case TypePDFFolder:		m_strOutputFilename		 = _T("KIM_$7_$9.pdf");  break;
	case TypePDFPrinter:	m_strOutputFilename		 = _T("KIM_TMP_$9.pdf");	break;
	case TypeJDFFolder:		m_strOutputFilename		 = _T("KIM_$7.jdf");		
							m_strCuttingDataFilename = _T("KIM_$7.jdf");		break;
	}
	m_apogeeParameterSets.RemoveAll();
	m_tagList.RemoveAll();
	m_mediaList.RemoveAll();
	m_mediaList.Add(mediaSize);
};

// operator overload for CPDFTargetProperties - needed by CList functions:

const CPDFTargetProperties& CPDFTargetProperties::operator=(const CPDFTargetProperties& rPDFTargetProperties)
{
	m_nType							= rPDFTargetProperties.m_nType;
	m_strTargetName					= rPDFTargetProperties.m_strTargetName;
	m_strLocation					= rPDFTargetProperties.m_strLocation;
	m_strLocationBack				= rPDFTargetProperties.m_strLocationBack;
	m_strOutputFilename				= rPDFTargetProperties.m_strOutputFilename;
	m_nPSLevel						= rPDFTargetProperties.m_nPSLevel;
	m_bEmitHalftones				= rPDFTargetProperties.m_bEmitHalftones;
	m_nJDFCompatibility 			= rPDFTargetProperties.m_nJDFCompatibility;
	m_bJDFMarksInFolder 			= rPDFTargetProperties.m_bJDFMarksInFolder;
	m_strJDFMarksFolder 			= rPDFTargetProperties.m_strJDFMarksFolder;
	m_bJDFOutputMime				= rPDFTargetProperties.m_bJDFOutputMime;
	m_strJDFJobIDMap				= rPDFTargetProperties.m_strJDFJobIDMap;
	m_strJDFJobPartIDMap			= rPDFTargetProperties.m_strJDFJobPartIDMap;
	m_strJDFJobDescriptiveNameMap	= rPDFTargetProperties.m_strJDFJobDescriptiveNameMap;
	m_bOutputCuttingData			= rPDFTargetProperties.m_bOutputCuttingData;
	m_strCuttingDataFolder			= rPDFTargetProperties.m_strCuttingDataFolder;
	m_strCuttingDataFilename		= rPDFTargetProperties.m_strCuttingDataFilename;

	m_apogeeParameterSets.RemoveAll();
	m_apogeeParameterSets.Append(rPDFTargetProperties.m_apogeeParameterSets);
	m_tagList.RemoveAll();
	m_tagList.Append(rPDFTargetProperties.m_tagList);
	m_mediaList.RemoveAll();
	m_mediaList.Append(rPDFTargetProperties.m_mediaList);

	return *this;
}


// helper function for PDFTargetList elements
template <> void AFXAPI SerializeElements <CPDFTargetProperties> (CArchive& ar, CPDFTargetProperties* pPDFTargetProperties, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		ar.SerializeClass(RUNTIME_CLASS(CPDFTargetProperties));

		if (ar.IsStoring())
		{
			ar << pPDFTargetProperties->m_nType;
			ar << pPDFTargetProperties->m_strTargetName;
			ar << pPDFTargetProperties->m_strLocation;
			ar << pPDFTargetProperties->m_strLocationBack;
			ar << pPDFTargetProperties->m_strOutputFilename;
			ar << pPDFTargetProperties->m_nPSLevel;
			ar << pPDFTargetProperties->m_bEmitHalftones;
			ar << pPDFTargetProperties->m_nJDFCompatibility;
			ar << pPDFTargetProperties->m_bJDFMarksInFolder;
			ar << pPDFTargetProperties->m_strJDFMarksFolder;
			ar << pPDFTargetProperties->m_bJDFOutputMime;
			ar << pPDFTargetProperties->m_strJDFJobIDMap;
			ar << pPDFTargetProperties->m_strJDFJobPartIDMap;
			ar << pPDFTargetProperties->m_strJDFJobDescriptiveNameMap;
			ar << pPDFTargetProperties->m_bOutputCuttingData;
			ar << pPDFTargetProperties->m_strCuttingDataFolder;
			ar << pPDFTargetProperties->m_strCuttingDataFilename;
			pPDFTargetProperties->m_apogeeParameterSets.Serialize(ar);
			pPDFTargetProperties->m_tagList.Serialize(ar);
		}
		else
		{
			UINT nVersion = ar.GetObjectSchema();
			pPDFTargetProperties->m_bJDFMarksInFolder = FALSE;
			pPDFTargetProperties->m_strJDFMarksFolder = _T("");
			pPDFTargetProperties->m_apogeeParameterSets.RemoveAll();
			pPDFTargetProperties->m_bOutputCuttingData = FALSE;
			pPDFTargetProperties->m_strCuttingDataFolder = _T("");
			pPDFTargetProperties->m_strCuttingDataFilename = _T("");
			switch (nVersion)
			{
			case 1:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				switch (pPDFTargetProperties->m_nType)
				{
				case CPDFTargetProperties::TypePDFFolder:	pPDFTargetProperties->m_strOutputFilename	= _T("KIM_$7_$9.pdf");	break;
				case CPDFTargetProperties::TypePDFPrinter:	pPDFTargetProperties->m_strOutputFilename	= _T("KIM_TMP_$9.pdf"); break;
				}
				pPDFTargetProperties->m_nPSLevel	      = 2;
				pPDFTargetProperties->m_bEmitHalftones	  = FALSE;
				break;
			case 2:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				pPDFTargetProperties->m_nPSLevel	      = 2;
				pPDFTargetProperties->m_bEmitHalftones    = FALSE;
				break;
			case 3:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				ar >> pPDFTargetProperties->m_nPSLevel;
				ar >> pPDFTargetProperties->m_bEmitHalftones;
				break;
			case 4:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				ar >> pPDFTargetProperties->m_nPSLevel;
				ar >> pPDFTargetProperties->m_bEmitHalftones;
				pPDFTargetProperties->m_tagList.Serialize(ar);
				break;
			case 5:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				ar >> pPDFTargetProperties->m_nPSLevel;
				ar >> pPDFTargetProperties->m_bEmitHalftones;
				ar >> pPDFTargetProperties->m_nJDFCompatibility;
				pPDFTargetProperties->m_tagList.Serialize(ar);
				break;
			case 6:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strLocationBack;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				ar >> pPDFTargetProperties->m_nPSLevel;
				ar >> pPDFTargetProperties->m_bEmitHalftones;
				ar >> pPDFTargetProperties->m_nJDFCompatibility;
				pPDFTargetProperties->m_tagList.Serialize(ar);
				break;
			case 7:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strLocationBack;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				ar >> pPDFTargetProperties->m_nPSLevel;
				ar >> pPDFTargetProperties->m_bEmitHalftones;
				ar >> pPDFTargetProperties->m_nJDFCompatibility;
				ar >> pPDFTargetProperties->m_bJDFMarksInFolder;
				ar >> pPDFTargetProperties->m_strJDFMarksFolder;
				pPDFTargetProperties->m_tagList.Serialize(ar);
				break;
			case 8:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strLocationBack;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				ar >> pPDFTargetProperties->m_nPSLevel;
				ar >> pPDFTargetProperties->m_bEmitHalftones;
				ar >> pPDFTargetProperties->m_nJDFCompatibility;
				ar >> pPDFTargetProperties->m_bJDFMarksInFolder;
				ar >> pPDFTargetProperties->m_strJDFMarksFolder;
				ar >> pPDFTargetProperties->m_bJDFOutputMime;
				pPDFTargetProperties->m_tagList.Serialize(ar);
				break;
			case 9:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strLocationBack;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				ar >> pPDFTargetProperties->m_nPSLevel;
				ar >> pPDFTargetProperties->m_bEmitHalftones;
				ar >> pPDFTargetProperties->m_nJDFCompatibility;
				ar >> pPDFTargetProperties->m_bJDFMarksInFolder;
				ar >> pPDFTargetProperties->m_strJDFMarksFolder;
				ar >> pPDFTargetProperties->m_bJDFOutputMime;
				ar >> pPDFTargetProperties->m_strJDFJobIDMap;
				ar >> pPDFTargetProperties->m_strJDFJobPartIDMap;
				ar >> pPDFTargetProperties->m_strJDFJobDescriptiveNameMap;
				pPDFTargetProperties->m_tagList.Serialize(ar);
				break;
			case 10:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strLocationBack;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				ar >> pPDFTargetProperties->m_nPSLevel;
				ar >> pPDFTargetProperties->m_bEmitHalftones;
				ar >> pPDFTargetProperties->m_nJDFCompatibility;
				ar >> pPDFTargetProperties->m_bJDFMarksInFolder;
				ar >> pPDFTargetProperties->m_strJDFMarksFolder;
				ar >> pPDFTargetProperties->m_bJDFOutputMime;
				ar >> pPDFTargetProperties->m_strJDFJobIDMap;
				ar >> pPDFTargetProperties->m_strJDFJobPartIDMap;
				ar >> pPDFTargetProperties->m_strJDFJobDescriptiveNameMap;
				pPDFTargetProperties->m_apogeeParameterSets.Serialize(ar);
				pPDFTargetProperties->m_tagList.Serialize(ar);
				break;
			case 11:
				ar >> pPDFTargetProperties->m_nType;
				ar >> pPDFTargetProperties->m_strTargetName;
				ar >> pPDFTargetProperties->m_strLocation;
				ar >> pPDFTargetProperties->m_strLocationBack;
				ar >> pPDFTargetProperties->m_strOutputFilename;
				ar >> pPDFTargetProperties->m_nPSLevel;
				ar >> pPDFTargetProperties->m_bEmitHalftones;
				ar >> pPDFTargetProperties->m_nJDFCompatibility;
				ar >> pPDFTargetProperties->m_bJDFMarksInFolder;
				ar >> pPDFTargetProperties->m_strJDFMarksFolder;
				ar >> pPDFTargetProperties->m_bJDFOutputMime;
				ar >> pPDFTargetProperties->m_strJDFJobIDMap;
				ar >> pPDFTargetProperties->m_strJDFJobPartIDMap;
				ar >> pPDFTargetProperties->m_strJDFJobDescriptiveNameMap;
				ar >> pPDFTargetProperties->m_bOutputCuttingData;
				ar >> pPDFTargetProperties->m_strCuttingDataFolder;
				ar >> pPDFTargetProperties->m_strCuttingDataFilename;
				pPDFTargetProperties->m_apogeeParameterSets.Serialize(ar);
				pPDFTargetProperties->m_tagList.Serialize(ar);
				break;
			}
		}

		//if (pPDFTargetProperties->m_strLocation.Find(_T("\\\\172.25.2.20\\BigBen-Neu\\Etiketten_KIM_7\\Output")) >= 0)
		//	pPDFTargetProperties->m_strLocation.Replace(_T("\\\\172.25.2.20\\BigBen-Neu\\Etiketten_KIM_7\\Output"), _T("C:\\Program Files (x86)\\KIM7.1\\PDF-Output"));

		
		pPDFTargetProperties->m_mediaList.Serialize(ar);
		
		pPDFTargetProperties++;
	}
}


void AFXAPI DestructElements(CPDFTargetProperties* pPDFTargetProperties, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pPDFTargetProperties->m_strTargetName.Empty();
		pPDFTargetProperties->m_strLocation.Empty();
		pPDFTargetProperties->m_strLocationBack.Empty();
		pPDFTargetProperties->m_strOutputFilename.Empty();
		pPDFTargetProperties->m_strJDFMarksFolder.Empty();
		pPDFTargetProperties->m_strJDFJobIDMap.Empty();
		pPDFTargetProperties->m_strJDFJobPartIDMap.Empty();
		pPDFTargetProperties->m_strJDFJobDescriptiveNameMap.Empty();
		pPDFTargetProperties->m_strCuttingDataFolder.Empty();
		pPDFTargetProperties->m_apogeeParameterSets.RemoveAll();
		pPDFTargetProperties->m_tagList.RemoveAll();
		pPDFTargetProperties->m_mediaList.RemoveAll();

		pPDFTargetProperties++;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CPDFTargetList

BOOL CPDFTargetList::Load(int nTarget)
{
	TCHAR			pszFileName[MAX_PATH];
	CFile			file;
	CFileException	fileException;

	_tcscpy(pszFileName, theApp.GetDataFolder()); // TODO: Verify the path
	switch (nTarget)
	{
	case PrintSheetTarget:	_tcscat(pszFileName, _T("\\PDFTargets.def"));				break;
	case FoldSheetTarget:	_tcscat(pszFileName, _T("\\OutputTargetsFoldSheets.def"));	break;
	case BookletTarget:		_tcscat(pszFileName, _T("\\OutputTargetsBookblock.def"));	break;
	}

	if (!file.Open(pszFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&file, CArchive::load);
		RemoveAll();
		Serialize(archive);

		m_defaultFormats.Load();

		archive.Close();
		file.Close();
	}

	if (GetSize() == 0) 
		return FALSE; 
	else
		return TRUE;  	
}


BOOL CPDFTargetList::Save(int nTarget)
{
	SaveAsXML(nTarget);

	TCHAR			pszFileName[MAX_PATH];
	CFile			file;
	CFileException	fileException;

	_tcscpy(pszFileName, theApp.GetDataFolder()); // TODO: Verify the path
	switch (nTarget)
	{
	case PrintSheetTarget:	_tcscat(pszFileName, _T("\\PDFTargets.def"));				break;
	case FoldSheetTarget:	_tcscat(pszFileName, _T("\\OutputTargetsFoldSheets.def"));	break;
	case BookletTarget:		_tcscat(pszFileName, _T("\\OutputTargetsBookblock.def"));	break;
	}

	if (!file.Open(pszFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&file, CArchive::store);
		Serialize(archive);

		m_defaultFormats.Save();

		archive.Close();
		file.Close();

		return TRUE;
	}
}

BOOL CPDFTargetList::SaveAsXML(int nTarget)
{
	XMLDoc* pXMLDoc = new XMLDoc(_T("KIM-PDFTargets"));
	KElement root = pXMLDoc->GetRoot();

	for (int i = 0; i < GetSize(); i++)
	{
		KElement targetProps = root.AppendElement(_T("PDFTargetProperties"));
		targetProps.AppendAttribute(_T("Name"), (WString)ElementAt(i).m_strTargetName);
	}

	try
	{
		CString strFile = theApp.GetDataFolder();
		switch (nTarget)
		{
		case PrintSheetTarget:	strFile += _T("\\PDFTargets.xml");				break;
		case FoldSheetTarget:	strFile += _T("\\OutputTargetsFoldSheets.xml");	break;
		case BookletTarget:		strFile += _T("\\OutputTargetsBookblock.xml");	break;
		}

		pXMLDoc->Write2URL((WString)WindowsPath2URL(strFile));
	}
	catch (JDF::JDFException e)
	{
		CString strMsg;
		strMsg.Format(_T("JDF API error: %s"), e.getMessage().getBytes());
		if (theApp.m_nGUIStyle != CImpManApp::GUIStandard)
			theApp.LogEntry(strMsg);
		else
			AfxMessageBox(strMsg);
	}

	delete pXMLDoc;

	return 0;
}

CPDFTargetProperties* CPDFTargetList::Find(CString& strPDFTargetName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i).m_strTargetName == strPDFTargetName)
			return &ElementAt(i);
	}
	return NULL;
}

int CPDFTargetList::FindIndex(CString& strPDFTargetName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (ElementAt(i).m_strTargetName == strPDFTargetName)
			return i;
	}
	return -1;
}




/////////////////////////////////////////////////////////////////////////////
// CMediaSize

IMPLEMENT_SERIAL(CMediaSize, CObject, VERSIONABLE_SCHEMA | 7)

CMediaSize::CMediaSize()
{
	m_fMediaWidth  = 0.0f;
	m_fMediaHeight = 0.0f;
	m_nReserved	   = 0;
	m_bShrinkToFit = FALSE;
	m_bPageFrame   = FALSE;
	m_bMarkFrame   = FALSE;
	m_bSheetFrame  = FALSE;
	m_bPlateFrame  = FALSE;
	m_bBoxShape	   = FALSE;
	m_nAutoAlignTo = AlignNone;

	m_frameSource.Clear();

	CMediaPosInfo mpInfo(0.0f, 0.0f, FALSE);
	m_posInfos.Add(mpInfo);
}

CMediaSize::CMediaSize(CString strMediaName, float fWidth, float fHeight, int nReserved, float fXPos, float fYPos, BOOL bRotated)
{
	m_strMediaName = strMediaName;
	m_fMediaWidth  = fWidth;
	m_fMediaHeight = fHeight;
	m_nReserved	   = nReserved;
	m_bShrinkToFit = FALSE;
	m_bPageFrame   = FALSE;
	m_bMarkFrame   = FALSE;
	m_bSheetFrame  = FALSE;
	m_bPlateFrame  = FALSE;
	m_bBoxShape	   = FALSE;
	m_nAutoAlignTo = AlignNone;

	m_frameSource.Clear();

	CMediaPosInfo mpInfo(fXPos, fYPos, bRotated);
	m_posInfos.Add(mpInfo);
}

CMediaSize::CMediaSize(const CMediaSize& rMediaSize)
{
	m_strMediaName = rMediaSize.m_strMediaName;
	m_fMediaWidth  = rMediaSize.m_fMediaWidth;
	m_fMediaHeight = rMediaSize.m_fMediaHeight;
	m_nReserved	   = rMediaSize.m_nReserved;
	m_bShrinkToFit = rMediaSize.m_bShrinkToFit;
	m_bPageFrame   = rMediaSize.m_bPageFrame;
	m_bMarkFrame   = rMediaSize.m_bMarkFrame;
	m_bSheetFrame  = rMediaSize.m_bSheetFrame;
	m_bPlateFrame  = rMediaSize.m_bPlateFrame;
	m_bBoxShape	   = rMediaSize.m_bBoxShape;
	m_nAutoAlignTo = rMediaSize.m_nAutoAlignTo;

	m_frameSource  = rMediaSize.m_frameSource;

	m_posInfos.Append(rMediaSize.m_posInfos);
}


// operator overload for CMediaSize - needed by CArray functions:

const CMediaSize& CMediaSize::operator=(const CMediaSize& rMediaSize)
{
	m_strMediaName = rMediaSize.m_strMediaName;
	m_fMediaWidth  = rMediaSize.m_fMediaWidth;
	m_fMediaHeight = rMediaSize.m_fMediaHeight;
	m_nReserved	   = rMediaSize.m_nReserved;
	m_bShrinkToFit = rMediaSize.m_bShrinkToFit;
	m_bPageFrame   = rMediaSize.m_bPageFrame;
	m_bMarkFrame   = rMediaSize.m_bMarkFrame;
	m_bSheetFrame  = rMediaSize.m_bSheetFrame;
	m_bPlateFrame  = rMediaSize.m_bPlateFrame;
	m_bBoxShape	   = rMediaSize.m_bBoxShape;
	m_nAutoAlignTo = rMediaSize.m_nAutoAlignTo;

	m_frameSource  = rMediaSize.m_frameSource;

	m_posInfos.RemoveAll();
	m_posInfos.Append(rMediaSize.m_posInfos);

	return *this;
}


// helper function for CMediaSize elements
void AFXAPI SerializeElements <CMediaSize> (CArchive& ar, CMediaSize* pMediaSize, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pMediaSize->Serialize(ar);
		pMediaSize++;
	}
}

void CMediaSize::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CMediaSize));

	if (ar.IsStoring())
	{
		ar << m_strMediaName;
		ar << m_fMediaWidth;
		ar << m_fMediaHeight;
		ar << m_nReserved;
		ar << m_bShrinkToFit;
		ar << m_bPageFrame;
		ar << m_bMarkFrame;
		ar << m_bSheetFrame;
		ar << m_bPlateFrame;
		ar << m_bBoxShape;
		ar << m_nAutoAlignTo;

		SerializeElements(ar, &m_frameSource, 1);
		m_posInfos.Serialize(ar);
	}
	else
	{
		m_frameSource.Clear();
		UINT nVersion = ar.GetObjectSchema();
		switch (nVersion)
		{
		case 1:
			ar >> m_strMediaName;
			ar >> m_fMediaWidth;
			ar >> m_fMediaHeight;
			ar >> m_nReserved;		// former m_nOrientation
			m_nAutoAlignTo = CMediaSize::AlignNone;
			m_bShrinkToFit = FALSE;
			break;
		case 2:
			ar >> m_strMediaName;
			ar >> m_fMediaWidth;
			ar >> m_fMediaHeight;
			ar >> m_nReserved;		// former m_nOrientation
			ar >> m_bPageFrame;
			ar >> m_bMarkFrame;
			ar >> m_bSheetFrame;
			ar >> m_bPlateFrame;
			m_nAutoAlignTo = CMediaSize::AlignNone;
			m_bShrinkToFit = FALSE;
			break;
		case 3:
			ar >> m_strMediaName;
			ar >> m_fMediaWidth;
			ar >> m_fMediaHeight;
			ar >> m_nReserved;		// former m_nOrientation
			ar >> m_bShrinkToFit;
			ar >> m_bPageFrame;
			ar >> m_bMarkFrame;
			ar >> m_bSheetFrame;
			ar >> m_bPlateFrame;
			m_nAutoAlignTo = CMediaSize::AlignNone;
			break;
		case 4:
			ar >> m_strMediaName;
			ar >> m_fMediaWidth;
			ar >> m_fMediaHeight;
			ar >> m_nReserved;
			ar >> m_bShrinkToFit;
			ar >> m_bPageFrame;
			ar >> m_bMarkFrame;
			ar >> m_bSheetFrame;
			ar >> m_bPlateFrame;
			m_nAutoAlignTo = CMediaSize::AlignNone;
			if (m_nReserved == 1)	// 1 = former orientation was Landscape
			{
				float fTemp = m_fMediaWidth; m_fMediaWidth = m_fMediaHeight; m_fMediaHeight = fTemp;
			}
			break;
		case 5:
			ar >> m_strMediaName;
			ar >> m_fMediaWidth;
			ar >> m_fMediaHeight;
			ar >> m_nReserved;
			ar >> m_bShrinkToFit;
			ar >> m_bPageFrame;
			ar >> m_bMarkFrame;
			ar >> m_bSheetFrame;
			ar >> m_bPlateFrame;
			m_nAutoAlignTo = CMediaSize::AlignNone;
			SerializeElements(ar, &m_frameSource, 1);
			break;
		case 6:
			ar >> m_strMediaName;
			ar >> m_fMediaWidth;
			ar >> m_fMediaHeight;
			ar >> m_nReserved;
			ar >> m_bShrinkToFit;
			ar >> m_bPageFrame;
			ar >> m_bMarkFrame;
			ar >> m_bSheetFrame;
			ar >> m_bPlateFrame;
			ar >> m_bBoxShape;
			m_nAutoAlignTo = CMediaSize::AlignNone;
			SerializeElements(ar, &m_frameSource, 1);
			break;
		case 7:
			ar >> m_strMediaName;
			ar >> m_fMediaWidth;
			ar >> m_fMediaHeight;
			ar >> m_nReserved;
			ar >> m_bShrinkToFit;
			ar >> m_bPageFrame;
			ar >> m_bMarkFrame;
			ar >> m_bSheetFrame;
			ar >> m_bPlateFrame;
			ar >> m_bBoxShape;
			ar >> m_nAutoAlignTo;
			SerializeElements(ar, &m_frameSource, 1);
			break;
		}

		m_posInfos.Serialize(ar);

		if (nVersion <= 3)
		{
			for (int i = 0; i < m_posInfos.GetSize(); i++)
			{
				if (m_nReserved == 1)	// 1 = former orientation was Landscape
					m_posInfos[i].m_nRotation = (m_posInfos[i].m_nRotation == 0) ? 90 : 270;
			}
		}
	}
}


void AFXAPI DestructElements(CMediaSize* pMediaSize, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pMediaSize->m_strMediaName.Empty();
		pMediaSize->m_frameSource.Clear();
		pMediaSize->m_posInfos.RemoveAll();

		pMediaSize++;
	}
}

float CMediaSize::GetMediaWidth(CPrintSheet* pPrintSheet)  
{ 
	if ( (m_nAutoAlignTo == AlignNone) || ! pPrintSheet)
		return m_fMediaWidth; 

	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return m_fMediaWidth;

	if (m_nAutoAlignTo == AlignToPlate)
	{
		CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
		if ( ! pPressDevice)
			return m_fMediaWidth;
		else
			return pPressDevice->m_fPlateWidth;
	}
	else
		if (m_nAutoAlignTo == AlignToSheet)
			return pLayout->m_FrontSide.m_fPaperWidth;

	return m_fMediaWidth; 
}

float CMediaSize::GetMediaHeight(CPrintSheet* pPrintSheet) 
{ 
	if (m_nAutoAlignTo == AlignNone)
		return m_fMediaHeight; 

	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return m_fMediaHeight;

	if (m_nAutoAlignTo == AlignToPlate)
	{
		CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
		if ( ! pPressDevice)
			return m_fMediaHeight;
		else
			return pPressDevice->m_fPlateHeight;
	}
	else
		if (m_nAutoAlignTo == AlignToSheet)
			return pLayout->m_FrontSide.m_fPaperHeight;

	return m_fMediaHeight; 
}

void CMediaSize::SetMediaWidth(float fMediaWidth)  
{ 
	m_fMediaWidth = fMediaWidth; 
}

void CMediaSize::SetMediaHeight(float fMediaHeight) 
{ 
	m_fMediaHeight = fMediaHeight; 
}

void CMediaSize::DrawTile(CDC* pDC, CPrintSheet* pPrintSheet, int nSide, int nTileIndex, BOOL bShowMeasuring, BOOL bShowHighlighted) 
{
	if ( ! pPrintSheet)
		return;
	if ( (nTileIndex < 0) || (nTileIndex > m_posInfos.GetSize()) )	
		return;

	CReflinePositionParams* pReflinePosParams = &m_posInfos[nTileIndex].m_reflinePosParams;

	float fMediaWidth  = GetMediaWidth(pPrintSheet);
	float fMediaHeight = GetMediaHeight(pPrintSheet);
	float fTrimBoxLeft, fTrimBoxBottom, fTrimBoxRight, fTrimBoxTop;
	// Get sheet trimbox
	pReflinePosParams->GetSheetPosition(pPrintSheet, nSide, fTrimBoxLeft, fTrimBoxBottom, fMediaWidth, fMediaHeight);
	fTrimBoxRight = fTrimBoxLeft   + fMediaWidth;
	fTrimBoxTop   = fTrimBoxBottom + fMediaHeight;

	CRect mediaRect;
	mediaRect.left	 = CPrintSheetView::WorldToLP(fTrimBoxLeft);
	mediaRect.bottom = CPrintSheetView::WorldToLP(fTrimBoxBottom);
	mediaRect.right	 = CPrintSheetView::WorldToLP(fTrimBoxRight);
	mediaRect.top	 = CPrintSheetView::WorldToLP(fTrimBoxTop);

	if (bShowHighlighted)
	{
   		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
   		if (pView)
			pView->m_DisplayList.RegisterItem(pDC, mediaRect, mediaRect, (void*)pPrintSheet, 
											  SNAP_DISPLAY_ITEM_REGISTRY(CPrintSheetView, OutputMedia), (void*)nSide, CDisplayItem::ForceRegister); 
	}

	if (pDC->RectVisible(mediaRect))
	{
		CPen	framePen(PS_SOLID, 1, (bShowHighlighted) ? BLUE : LIGHTBLUE);	
		CPen*	pOldPen	  = pDC->SelectObject(&framePen);
		CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

//		CPrintSheetView::FillTransparent (pDC, mediaRect, (bShowHighlighted) ? BLUE : LIGHTBLUE);
	CRect rect = mediaRect;
	pDC->LPtoDP(rect);
	rect.NormalizeRect();

	int nDC = pDC->SaveDC();
	pDC->SetMapMode(MM_TEXT);
	pDC->SetViewportOrg(0, 0);
	pDC->SetWindowOrg(0, 0);

	CRect clipBox;
	pDC->GetClipBox(clipBox);	
	rect.IntersectRect(rect, clipBox);
	rect.left -= (rect.left%2) ? 1 : 0;
	rect.top  -= (rect.top%2)  ? 1 : 0;

	Graphics graphics(pDC->m_hDC);
	Rect rc(rect.left, rect.top, rect.Width(), rect.Height());
	SolidBrush br(Color::Color(50, GetRValue((bShowHighlighted) ? BLUE : LIGHTBLUE), GetGValue((bShowHighlighted) ? BLUE : LIGHTBLUE), GetBValue((bShowHighlighted) ? BLUE : LIGHTBLUE)));
	graphics.FillRectangle(&br, rc);

	pDC->RestoreDC(nDC);

		CPrintSheetView::DrawRectangleExt(pDC, mediaRect);

		if (bShowMeasuring)
			DrawCoordSys(pDC, mediaRect, m_posInfos[nTileIndex].m_nRotation, m_posInfos[nTileIndex].m_fCorrectionX, 
																			 m_posInfos[nTileIndex].m_fCorrectionY);

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
		framePen.DeleteObject();

		CRect textRect = mediaRect;
		if (bShowHighlighted)	// take care about center cross
		{
			CSize sizeCross (CPrintSheetView::WorldToLP(60.0f), CPrintSheetView::WorldToLP(60.0f));
			pDC->LPtoDP(&sizeCross);
			if (sizeCross.cx > 10)		// if cross is wider than 10 pixels (i.e. relative wide zoomed in) center text with offset
			{
				CSize sizeOffset = CSize(10, 15);
				pDC->DPtoLP(&sizeOffset);
				textRect += sizeOffset;	
			}
		}
		if (m_posInfos.GetSize() > 1)
		{
			CFont*	 pOldFont		= (CFont*)pDC->SelectStockObject(ANSI_VAR_FONT);
			COLORREF crOldTextColor = pDC->SetTextColor(WHITE);
			COLORREF crOldBkColor	= pDC->SetBkColor  (DARKBLUE);//RGB(220,220,255));	// very light blue
			int		 nOldBkMode		= pDC->SetBkMode   (OPAQUE);
			CString strNum; strNum.Format(_T(_T("%d")), nTileIndex + 1);
			pDC->DrawText(strNum, textRect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
			pDC->SelectObject(pOldFont);
			pDC->SetTextColor(crOldTextColor);
			pDC->SetBkColor(crOldBkColor);
			pDC->SetBkMode(nOldBkMode);
		}

	}

	if (bShowMeasuring)
		pReflinePosParams->DrawSheetReflines(pDC, pPrintSheet, nSide, 0, fMediaWidth, fMediaHeight);
}

void CMediaSize::DrawCoordSys(CDC* pDC, CRect& mediaRect, int nRotation, float fCorrectionX, float fCorrectionY)
{
	CFont*	 pOldFont		= (CFont*)pDC->SelectStockObject(ANSI_VAR_FONT);
	COLORREF crOldTextColor = pDC->SetTextColor(DARKBLUE);
	COLORREF crOldBkColor	= pDC->SetBkColor  (RGB(180,180,255));
	int		 nOldBkMode		= pDC->SetBkMode   (OPAQUE);
	CSize	 sizeText		= pDC->GetTextExtent("X");
	CFont	 font;
	CSize	 sizeFont(mediaRect.Height()/3, mediaRect.Height()/3);
	int		 nTextPosX = mediaRect.CenterPoint().x;
	int		 nTextPosY = mediaRect.CenterPoint().y;

	CSize sizeCoordSys = CSize(CPrintSheetView::WorldToLP(100.0f), CPrintSheetView::WorldToLP(100.0f));
	pDC->LPtoDP(&sizeCoordSys);
	CPoint ptCoordSys;
	switch (nRotation)
	{
	case 0:		ptCoordSys = CPoint(mediaRect.left, mediaRect.bottom);
				ptCoordSys += CSize(CPrintSheetView::WorldToLP(fCorrectionX), CPrintSheetView::WorldToLP(fCorrectionY));
				pDC->LPtoDP(&ptCoordSys);
				CPrintSheetView::DrawArrow(pDC, RIGHT, ptCoordSys + CSize(sizeCoordSys.cx,  0), sizeCoordSys.cx, DARKBLUE, LIGHTBLUE);
				CPrintSheetView::DrawArrow(pDC, TOP,   ptCoordSys + CSize(0, -sizeCoordSys.cy), sizeCoordSys.cy, DARKBLUE, LIGHTBLUE);
				pDC->DPtoLP(&sizeCoordSys);
				pDC->DPtoLP(&ptCoordSys);
				pDC->TextOut(ptCoordSys.x + sizeCoordSys.cx + sizeText.cx/2, ptCoordSys.y,											" x ");			
				pDC->TextOut(ptCoordSys.x - sizeText.cx*2,				     ptCoordSys.y + sizeCoordSys.cy + (sizeText.cy*3)/2,	" y ");

				font.CreateFont(sizeFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
				pDC->SelectObject(&font);
				nTextPosX -= pDC->GetTextExtent("A").cx/2;
				nTextPosY += pDC->GetTextExtent("A").cy/2;
				break;

	case 90:	ptCoordSys = CPoint(mediaRect.right, mediaRect.bottom);
				ptCoordSys += CSize( - CPrintSheetView::WorldToLP(fCorrectionY), CPrintSheetView::WorldToLP(fCorrectionX));
				pDC->LPtoDP(&ptCoordSys);
				CPrintSheetView::DrawArrow(pDC, LEFT,  ptCoordSys + CSize(-sizeCoordSys.cx, 0), sizeCoordSys.cy, DARKBLUE, LIGHTBLUE);
				CPrintSheetView::DrawArrow(pDC, TOP,   ptCoordSys + CSize(0, -sizeCoordSys.cy), sizeCoordSys.cx, DARKBLUE, LIGHTBLUE);
				pDC->DPtoLP(&sizeCoordSys);
				pDC->DPtoLP(&ptCoordSys);
				pDC->TextOut(ptCoordSys.x,									 ptCoordSys.y + sizeCoordSys.cy + (sizeText.cy*3)/2,	" x ");			
				pDC->TextOut(ptCoordSys.x - sizeCoordSys.cx - sizeText.cx*2, ptCoordSys.y,											" y ");

				font.CreateFont(sizeFont.cy, 0, -900, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
				pDC->SelectObject(&font);
				nTextPosX -= pDC->GetTextExtent("A").cy/2;
				nTextPosY -= pDC->GetTextExtent("A").cx/2;
				break;
	case 180:	ptCoordSys = CPoint(mediaRect.right, mediaRect.top);
				ptCoordSys += CSize( - CPrintSheetView::WorldToLP(fCorrectionX), - CPrintSheetView::WorldToLP(fCorrectionY));
				pDC->LPtoDP(&ptCoordSys);
				CPrintSheetView::DrawArrow(pDC, LEFT,   ptCoordSys + CSize(-sizeCoordSys.cx, 0), sizeCoordSys.cx, DARKBLUE, LIGHTBLUE);
				CPrintSheetView::DrawArrow(pDC, BOTTOM, ptCoordSys + CSize(0,  sizeCoordSys.cy), sizeCoordSys.cy, DARKBLUE, LIGHTBLUE);
				pDC->DPtoLP(&sizeCoordSys);
				pDC->DPtoLP(&ptCoordSys);
				pDC->TextOut(ptCoordSys.x - sizeCoordSys.cx - sizeText.cx*2, ptCoordSys.y + sizeText.cy,						" x ");			
				pDC->TextOut(ptCoordSys.x,									 ptCoordSys.y - sizeCoordSys.cy - sizeText.cy/2,	" y ");
				
				font.CreateFont(sizeFont.cy, 0, -1800, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
				pDC->SelectObject(&font);
				nTextPosX += pDC->GetTextExtent("A").cx/2;
				nTextPosY -= pDC->GetTextExtent("A").cy/2;
				break;
	case 270:	ptCoordSys = CPoint(mediaRect.left, mediaRect.top);
				ptCoordSys += CSize( CPrintSheetView::WorldToLP(fCorrectionY), - CPrintSheetView::WorldToLP(fCorrectionX));
				pDC->LPtoDP(&ptCoordSys);
				CPrintSheetView::DrawArrow(pDC, RIGHT,  ptCoordSys + CSize(sizeCoordSys.cx,  0), sizeCoordSys.cy, DARKBLUE, LIGHTBLUE);
				CPrintSheetView::DrawArrow(pDC, BOTTOM, ptCoordSys + CSize(0,  sizeCoordSys.cy), sizeCoordSys.cx, DARKBLUE, LIGHTBLUE);
				pDC->DPtoLP(&sizeCoordSys);
				pDC->DPtoLP(&ptCoordSys);
				pDC->TextOut(ptCoordSys.x - sizeText.cx*2,					 ptCoordSys.y - sizeCoordSys.cy - sizeText.cy/2,	" x ");
				pDC->TextOut(ptCoordSys.x + sizeCoordSys.cx + sizeText.cx/2, ptCoordSys.y + sizeText.cy,						" y ");			

				font.CreateFont(sizeFont.cy, 0, -2700, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, _T("Tahoma"));	
				pDC->SelectObject(&font);
				nTextPosX += pDC->GetTextExtent("A").cy/2;
				nTextPosY += pDC->GetTextExtent("A").cx/2;
				break;
	}
	pDC->SetTextColor(RGB(180,180,255));
	pDC->SetBkMode   (TRANSPARENT);
	pDC->TextOut(nTextPosX, nTextPosY, "A");

	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldTextColor);
	pDC->SetBkColor(crOldBkColor);
	pDC->SetBkMode(nOldBkMode);

	font.DeleteObject();
}

void CMediaSize::SetAlign(int nAlignTo)
{
	m_nAutoAlignTo = nAlignTo;
	if (nAlignTo == AlignNone)
		return;

	for (int i = 0; i < m_posInfos.GetSize(); i++)
	{
		m_posInfos[i].m_reflinePosParams.m_fDistanceX				= 0.0f;
		m_posInfos[i].m_reflinePosParams.m_fDistanceY				= 0.0f;
		m_posInfos[i].m_reflinePosParams.m_nReferenceItemInX		= (nAlignTo == AlignToSheet) ? PAPER_REFLINE : PLATE_REFLINE;
		m_posInfos[i].m_reflinePosParams.m_nReferenceItemInY		= (nAlignTo == AlignToSheet) ? PAPER_REFLINE : PLATE_REFLINE;
		m_posInfos[i].m_reflinePosParams.m_nReferenceLineInX		= XCENTER;
		m_posInfos[i].m_reflinePosParams.m_nReferenceLineInY		= BOTTOM;
		m_posInfos[i].m_reflinePosParams.m_nObjectReferenceLineInX	= XCENTER;
		m_posInfos[i].m_reflinePosParams.m_nObjectReferenceLineInY	= BOTTOM;
	}
}


/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CMediaPosInfo

IMPLEMENT_SERIAL(CMediaPosInfo, CObject, VERSIONABLE_SCHEMA | 2)

CMediaPosInfo::CMediaPosInfo()
{
	m_fCorrectionX	= m_fCorrectionY = 0.0f;
	m_nRotation		= 0;
	m_reflinePosParams.m_nPlaceOn = CReflinePositionParams::PlaceOnBothSamePos;
	m_reflinePosParams.m_nReferenceLineInX		 = XCENTER;
	m_reflinePosParams.m_nReferenceLineInY		 = BOTTOM;
	m_reflinePosParams.m_nObjectReferenceLineInX = XCENTER;
	m_reflinePosParams.m_nObjectReferenceLineInY = BOTTOM;
}

CMediaPosInfo::CMediaPosInfo(float fCorrectionX, float fCorrectionY, int nRotation)
{
	m_fCorrectionX = fCorrectionX;
	m_fCorrectionY = fCorrectionY;
	m_nRotation = nRotation;
	m_reflinePosParams.m_nPlaceOn = CReflinePositionParams::PlaceOnBothSamePos;
	m_reflinePosParams.m_nReferenceLineInX		 = XCENTER;
	m_reflinePosParams.m_nReferenceLineInY		 = BOTTOM;
	m_reflinePosParams.m_nObjectReferenceLineInX = XCENTER;
	m_reflinePosParams.m_nObjectReferenceLineInY = BOTTOM;
}

CMediaPosInfo::~CMediaPosInfo()
{
}

const CMediaPosInfo& CMediaPosInfo::operator=(const CMediaPosInfo& rMediaPosInfo)
{
	m_reflinePosParams = rMediaPosInfo.m_reflinePosParams;
	m_fCorrectionX	   = rMediaPosInfo.m_fCorrectionX;
	m_fCorrectionY	   = rMediaPosInfo.m_fCorrectionY;
	m_nRotation		   = rMediaPosInfo.m_nRotation;

	return *this;
}


// helper function for CMediaPosInfo elements
template <> void AFXAPI SerializeElements <CMediaPosInfo> (CArchive& ar, CMediaPosInfo* pMediaPosInfo, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		ar.SerializeClass(RUNTIME_CLASS(CMediaPosInfo));

		if (ar.IsStoring())
		{
			ar << pMediaPosInfo->m_fCorrectionX;
			ar << pMediaPosInfo->m_fCorrectionY;
			ar << pMediaPosInfo->m_nRotation;
		}
		else
		{
			UINT nVersion = ar.GetObjectSchema();
			switch (nVersion)
			{
			case 1:
				BOOL bRotated;
				ar >> pMediaPosInfo->m_fCorrectionX;
				ar >> pMediaPosInfo->m_fCorrectionY;
				ar >> bRotated;
				pMediaPosInfo->m_nRotation = (bRotated) ? 90 : 0;
				break;
			case 2:
				ar >> pMediaPosInfo->m_fCorrectionX;
				ar >> pMediaPosInfo->m_fCorrectionY;
				ar >> pMediaPosInfo->m_nRotation;
				break;
			}
		}

		pMediaPosInfo->m_reflinePosParams.Serialize(ar);

		pMediaPosInfo++;
	}
}

/////////////////////////////////////////////////////////////////////////////


IMPLEMENT_SERIAL(COutputFormatList, CObject, VERSIONABLE_SCHEMA | 1)


COutputFormatList::COutputFormatList()
{
}

BOOL COutputFormatList::Load()
{
	TCHAR			pszFileName[MAX_PATH];
	CFile			file;
	CFileException	fileException;

	_tcscpy(pszFileName, theApp.GetDataFolder()); // TODO: Verify the path
	_tcscat(pszFileName, _T(_T("\\OutputFormats.def")));
	if (!file.Open(pszFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&file, CArchive::load);
		RemoveAll();
		archive.SerializeClass(RUNTIME_CLASS(COutputFormatList));
		UINT nVersion = archive.GetObjectSchema();
		Serialize(archive);

		archive.Close();
		file.Close();
	}

	if (GetSize() == 0) 
		return FALSE; 
	else
		return TRUE;  	
}


BOOL COutputFormatList::Save()
{
	TCHAR			pszFileName[MAX_PATH];
	CFile			file;
	CFileException	fileException;

	_tcscpy(pszFileName, theApp.GetDataFolder()); // TODO: Verify the path
	_tcscat(pszFileName, _T(_T("\\OutputFormats.def")));

	if (!file.Open(pszFileName, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		CArchive archive(&file, CArchive::store);
		archive.SerializeClass(RUNTIME_CLASS(COutputFormatList));
		Serialize(archive);

		archive.Close();
		file.Close();

		return TRUE;
	}
}

int COutputFormatList::GetDefaultFormat(const CString& strName, float* pWidth, float* pHeight)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (strName == GetAt(i).m_strName)
		{
			if (pWidth && pHeight)
			{
				*pWidth  = GetAt(i).m_fWidth;
				*pHeight = GetAt(i).m_fHeight;
			}
			return i;
		}
	}
	return -1;
}

void COutputFormatList::RemoveDefaultFormat(const CString& strName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (strName == GetAt(i).m_strName)
		{
			RemoveAt(i);
			break;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// COutputFormat

IMPLEMENT_SERIAL(COutputFormat, CObject, VERSIONABLE_SCHEMA | 1)

COutputFormat::COutputFormat()
{
}

COutputFormat::COutputFormat(CString strName, float fWidth, float fHeight)
{
	m_strName = strName;
	m_fWidth  = fWidth;
	m_fHeight = fHeight;
}

COutputFormat::COutputFormat(const COutputFormat& rOutputFormat)
{
	m_strName = rOutputFormat.m_strName;
	m_fWidth  = rOutputFormat.m_fWidth;
	m_fHeight = rOutputFormat.m_fHeight;
}

COutputFormat::~COutputFormat()
{
}

const COutputFormat& COutputFormat::operator=(const COutputFormat& rOutputFormat)
{
	m_strName = rOutputFormat.m_strName;
	m_fWidth  = rOutputFormat.m_fWidth;
	m_fHeight = rOutputFormat.m_fHeight;

	return *this;
}

// helper function for CMediaPosInfo elements
template <> void AFXAPI SerializeElements <COutputFormat> (CArchive& ar, COutputFormat* pOutputFormat, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		ar.SerializeClass(RUNTIME_CLASS(COutputFormat));

		if (ar.IsStoring())
		{
			ar << pOutputFormat->m_strName;
			ar << pOutputFormat->m_fWidth;
			ar << pOutputFormat->m_fHeight;
		}
		else
		{
			UINT nVersion = ar.GetObjectSchema();
			switch (nVersion)
			{
			case 1:
				ar >> pOutputFormat->m_strName;
				ar >> pOutputFormat->m_fWidth;
				ar >> pOutputFormat->m_fHeight;
				break;
			}
		}

		pOutputFormat++;
	}
}
/////////////////////////////////////////////////////////////////////////////

