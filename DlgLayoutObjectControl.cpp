// DlgLayoutObjectControl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetMarksView.h"
#include "PageListView.h"
#include "PrintSheetFrame.h"
#include "DlgJobData.h"
#include "ProdDataPrintView.h"
#include "PrintSheetObjectPreview.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgLayoutObjectControl 

IMPLEMENT_DYNCREATE(CDlgLayoutObjectControl, CDialog)


CDlgLayoutObjectControl::CDlgLayoutObjectControl(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgLayoutObjectControl::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgLayoutObjectControl)
	//}}AFX_DATA_INIT

	m_pLayoutObject		= NULL;
	m_pObjectTemplate	= NULL;
	m_pPrintSheet		= NULL;
	m_pMark				= NULL;
	m_nObjectType		= -1;
	m_pColorDefTable	= NULL;
	m_nInitialPage		= 0;
	m_sizeViewport		= CSize(1,1);
	m_pBkBrush			= new CBrush;
	m_bLockUpdateView	= FALSE;
}

CDlgLayoutObjectControl::~CDlgLayoutObjectControl()
{
	DestroyWindow();
	
	delete m_pBkBrush;
}

void CDlgLayoutObjectControl::OnDestroy() 
{
	CDialog::OnDestroy();
	
	m_pBkBrush->DeleteObject();
}

void CDlgLayoutObjectControl::PostNcDestroy()
{
	CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	if (pView)
		pView->UpdateLayoutMarkObjects();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetObjectPreview));

	CDialog::PostNcDestroy();
}

void CDlgLayoutObjectControl::Initialize(int nIDFrame, CLayoutObject* pLayoutObject, CPrintSheet* pPrintSheet, CWnd* pParent, int nInitialPage)
{
	m_pLayoutObject		  = NULL;
	m_pObjectTemplate	  = NULL;
	m_pObjectSource		  = NULL;
	m_pObjectSourceHeader = NULL;
	m_pPrintSheet		  = NULL;
	m_pMark				  = NULL;
	m_nObjectType		  = -1;
	m_pColorDefTable	  = NULL;
	m_nInitialPage		  = nInitialPage;

	CDialog::Create(IDD_LAYOUTOBJECT_CONTROL, pParent);

	CreateObjectPreviewWindow();

	if (pParent->GetDlgItem(nIDFrame))
	{
		CRect frameRect, dlgRect;
		pParent->GetDlgItem(nIDFrame)->GetWindowRect(frameRect);
		pParent->ScreenToClient(frameRect);
		GetWindowRect(dlgRect);
		SetWindowPos(NULL, frameRect.left, frameRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}

	ReinitDialog(pLayoutObject, NULL, pPrintSheet, NULL);
}

void CDlgLayoutObjectControl::Initialize(int nIDFrame, CMark* pMark, CPrintSheet* pPrintSheet, CColorDefinitionTable* pColorDefTable, CWnd* pParent, int nInitialPage)
{
	m_pLayoutObject		  = NULL;
	m_pObjectTemplate	  = NULL;
	m_pObjectSource		  = NULL;
	m_pObjectSourceHeader = NULL;
	m_pPrintSheet		  = NULL;
	m_pMark				  = NULL;
	m_nObjectType		  = -1;
	m_pColorDefTable	  = NULL;
	m_nInitialPage		  = nInitialPage;

	CDialog::Create(IDD_LAYOUTOBJECT_CONTROL, pParent);

	CreateObjectPreviewWindow();

	CRect frameRect, dlgRect;
	pParent->GetDlgItem(nIDFrame)->GetWindowRect(frameRect);
	pParent->ScreenToClient(frameRect);
	GetWindowRect(dlgRect);
	SetWindowPos(NULL, frameRect.left, frameRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);

	ReinitDialog(NULL, pMark, pPrintSheet, pColorDefTable, FALSE);

	UpdateView();
}

void CDlgLayoutObjectControl::ReinitDialog(CLayoutObject* pNewLayoutObject, CPrintSheet* pNewPrintSheet, BOOL bDoUpdateView)
{
	if ( ! m_hWnd)
		return;

	ReinitDialog(pNewLayoutObject, NULL, pNewPrintSheet, NULL, bDoUpdateView);
}

void CDlgLayoutObjectControl::ReinitDialog(CMark* pNewMark, CPrintSheet* pNewPrintSheet, CColorDefinitionTable* pColorDefTable, BOOL bDoUpdateView)
{
	if ( ! m_hWnd)
		return;

	ReinitDialog(NULL, pNewMark, pNewPrintSheet, pColorDefTable, bDoUpdateView);
}

void CDlgLayoutObjectControl::ReinitDialog(CLayoutObject* pNewLayoutObject, CMark* pNewMark, CPrintSheet* pNewPrintSheet, CColorDefinitionTable* pColorDefTable, BOOL bDoUpdateView)
{
	m_pObjectSource		  = NULL;
	m_pObjectSourceHeader = NULL;
	if (! pNewLayoutObject && ! pNewMark)
	{
		BOOL bUpdateView = TRUE; // ( ( ! pNewLayoutObject && m_pLayoutObject) || ( ! pNewMark && m_pMark) ) ? TRUE : FALSE;
		SetWindowText(_T(""));
		InitSubDialogTab(-1, -1, -1);
		DlgHideAll();
		m_pLayoutObject		  = NULL;
		m_pObjectTemplate	  = NULL;
		m_pPrintSheet		  = NULL;
		m_pMark				  = NULL;
		m_nObjectType		  = -1;
		if (bUpdateView)
			UpdateView();
		ObjectPreviewWindow_LoadData();
		return;
	}

	if (m_pLayoutObject)
		DataExchangeSave();
//	else
//		DataExchangeSave();		not possible here, because maybe m_pMark is no longer valid at this stage (i.e. markList has been changed)
//								so save before ReinitDialog() in PrintSheetMarksView

	int			   nOldObjectType	= (m_pLayoutObject) ? GetObjectType() : -1;
	CLayoutObject* pOldLayoutObject = m_pLayoutObject;
	m_pLayoutObject = pNewLayoutObject;
	m_pPrintSheet	= pNewPrintSheet;
	if (m_pLayoutObject)
	{
		m_pObjectTemplate = (m_pLayoutObject->m_nType == CLayoutObject::Page) ? m_pLayoutObject->GetCurrentPageTemplate(m_pPrintSheet) : m_pLayoutObject->GetCurrentMarkTemplate();
		if (m_pObjectTemplate)
		{
//			if (m_pObjectTemplate->m_ColorSeparations.GetSize())
			{
				m_pObjectSource		  = m_pObjectTemplate->GetObjectSource		( (m_pObjectTemplate->m_ColorSeparations.GetSize()) ? m_pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1);
				m_pObjectSourceHeader = m_pObjectTemplate->GetObjectSourceHeader( (m_pObjectTemplate->m_ColorSeparations.GetSize()) ? m_pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1);
			}
		}
	}

	CMark* pOldMark  = m_pMark;
	m_pMark			 = pNewMark;
	m_pColorDefTable = pColorDefTable;
	if (m_pMark)
	{
		m_pObjectTemplate = &m_pMark->m_markTemplate;
		m_pObjectSource	  = &m_pMark->m_MarkSource;
		if (m_pObjectSource->m_PageSourceHeaders.GetSize())
			m_pObjectSourceHeader = &m_pObjectSource->m_PageSourceHeaders[0];
	}

	int nNewObjectType = GetObjectType();
	if (nNewObjectType != nOldObjectType)
	{
		switch (nNewObjectType)
		{
			case PageObject:			InitPageObject();	break;
			case CMark::CustomMark:		InitCustomMark();	break;
			case CMark::TextMark:		InitTextMark();		break;
			case CMark::SectionMark:	InitSectionMark();	break;
			case CMark::CropMark:		InitCropMark();		break;
			case CMark::FoldMark:		InitFoldMark();		break;
			case CMark::BarcodeMark:	InitBarcodeMark();	break;
			case CMark::DrawingMark:	InitDrawingMark();	break;
		}
		m_nObjectType = nNewObjectType;

		DataExchangeLoad();

		LRESULT result;
		OnSelchangeSubdialogTab((bDoUpdateView) ? (NMHDR*)TRUE : NULL, &result);
	}
	else
	{
		DataExchangeLoad();
		if ( (pOldLayoutObject != pNewLayoutObject) || (pOldMark != pNewMark) )
			UpdateView();
	}
}

CDlgObjectPreviewWindow* CDlgLayoutObjectControl::GetObjectPreviewWindow()
{
	CPrintSheetObjectPreview* pView = (CPrintSheetObjectPreview*)theApp.GetView(RUNTIME_CLASS(CPrintSheetObjectPreview));
	if ( ! pView)
		return FALSE;
	return &pView->m_dlgObjectPreviewWindow;
}

void CDlgLayoutObjectControl::CreateObjectPreviewWindow()
{
	//CDlgObjectPreviewWindow* pWindow = GetObjectPreviewWindow(); 
	//if ( ! pWindow)
	//	return;
	//pWindow->m_pParent = this;
	//if (pWindow->m_hWnd)
	//	pWindow->DestroyWindow();

	//CProdDataPrintView* pView = (CProdDataPrintView*)theApp.GetView(RUNTIME_CLASS(CProdDataPrintView));
	//pWindow->Create(IDD_OBJECT_PREVIEW_WINDOW, pView);
}

int CDlgLayoutObjectControl::GetObjectType()
{
	if (m_pLayoutObject)
	{
		switch (m_pLayoutObject->m_nType)
		{
			case CLayoutObject::Page:			return PageObject;

			case CLayoutObject::ControlMark:	if ( ! m_pObjectTemplate)
													return -1;
												else
													return m_pObjectTemplate->GetMarkType();
		}
	}
	else
		if (m_pMark)
			return m_pMark->m_nType;

	return ObjectTypeError;
}

void CDlgLayoutObjectControl::DlgHideAll()
{
	if (m_dlgPageObjectContent.m_hWnd)		m_dlgPageObjectContent.ShowWindow	(SW_HIDE);
	if (m_dlgCustomMarkContent.m_hWnd)		m_dlgCustomMarkContent.ShowWindow	(SW_HIDE);
	if (m_dlgTextMarkContent.m_hWnd)		m_dlgTextMarkContent.ShowWindow		(SW_HIDE);
	if (m_dlgSectionMarkContent.m_hWnd)		m_dlgSectionMarkContent.ShowWindow	(SW_HIDE);
	if (m_dlgCropMarkContent.m_hWnd)		m_dlgCropMarkContent.ShowWindow		(SW_HIDE);
	if (m_dlgFoldMarkContent.m_hWnd)		m_dlgFoldMarkContent.ShowWindow		(SW_HIDE);
	if (m_dlgBarcodeMarkContent.m_hWnd)		m_dlgBarcodeMarkContent.ShowWindow	(SW_HIDE);
	if (m_dlgDrawingMarkContent.m_hWnd)		m_dlgDrawingMarkContent.ShowWindow	(SW_HIDE);
	if (m_dlgLayoutObjectGeometry.m_hWnd)	m_dlgLayoutObjectGeometry.ShowWindow(SW_HIDE);
	if (m_dlgCustomMarkGeometry.m_hWnd)		m_dlgCustomMarkGeometry.ShowWindow	(SW_HIDE);
	if (m_dlgTextMarkGeometry.m_hWnd)		m_dlgTextMarkGeometry.ShowWindow	(SW_HIDE);
	if (m_dlgSectionMarkGeometry.m_hWnd)	m_dlgSectionMarkGeometry.ShowWindow	(SW_HIDE);
	if (m_dlgCropMarkGeometry.m_hWnd)		m_dlgCropMarkGeometry.ShowWindow	(SW_HIDE);
	if (m_dlgFoldMarkGeometry.m_hWnd)		m_dlgFoldMarkGeometry.ShowWindow	(SW_HIDE);
	if (m_dlgBarcodeMarkGeometry.m_hWnd)	m_dlgBarcodeMarkGeometry.ShowWindow	(SW_HIDE);
	if (m_dlgDrawingMarkGeometry.m_hWnd)	m_dlgDrawingMarkGeometry.ShowWindow	(SW_HIDE);
	if (m_dlgMarkPlacement.m_hWnd)			m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
	if (m_dlgObjectZOrder.m_hWnd)			m_dlgObjectZOrder.ShowWindow		(SW_HIDE);
	if (m_dlgMarkRules.m_hWnd)				m_dlgMarkRules.ShowWindow			(SW_HIDE);
}

inline BOOL CDlgLayoutObjectControl::EnableRulesTab()
{
	return TRUE;
	//CWnd* pParent = GetParent();
	//if (pParent)
	//	if (pParent->IsKindOf(RUNTIME_CLASS(CDlgMarkSet)))
	//		if ( ((CDlgMarkSet*)pParent)->m_bOpenAutoMarks)
	//			return TRUE;

	//return FALSE;
}

void CDlgLayoutObjectControl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgLayoutObjectControl)
	DDX_Control(pDX, IDC_SUBDIALOG_TAB, m_subDialogTab);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgLayoutObjectControl, CDialog)
	//{{AFX_MSG_MAP(CDlgLayoutObjectControl)
	ON_WM_DESTROY()
	ON_NOTIFY(TCN_SELCHANGE, IDC_SUBDIALOG_TAB, OnSelchangeSubdialogTab)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgLayoutObjectControl 

BOOL CDlgLayoutObjectControl::OnInitDialog() 
{
	CDialog::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgLayoutObjectControl::InitSubDialogTab(int nID1, int nID2, int nID3, int nID4)
{
	CString str1, str2, str3, str4;
	if (nID1 != -1)
		str1.LoadString(nID1);
	if (nID2 != -1)
		str2.LoadString(nID2);
	if (nID3 != -1)
		str3.LoadString(nID3);
	if (nID4 != -1)
		str4.LoadString(nID4);

	int nOldSel = m_subDialogTab.GetCurSel();

	m_subDialogTab.DeleteAllItems();

	TC_ITEM tcItem;
	tcItem.mask			= TCIF_TEXT;
	tcItem.cchTextMax	= 0;
	tcItem.iImage		= -1;
	tcItem.lParam		= 0;
	if ( ! str1.IsEmpty())
	{
		tcItem.pszText = str1.GetBuffer(MAX_TEXT);
		m_subDialogTab.InsertItem(m_subDialogTab.GetItemCount(), &tcItem);
	}
	if ( ! str2.IsEmpty())
	{
		tcItem.pszText = str2.GetBuffer(MAX_TEXT);
		m_subDialogTab.InsertItem(m_subDialogTab.GetItemCount(), &tcItem);
	}
	if ( ! str3.IsEmpty())
	{
		tcItem.pszText = str3.GetBuffer(MAX_TEXT);
		m_subDialogTab.InsertItem(m_subDialogTab.GetItemCount(), &tcItem);
	}
	if ( ! str4.IsEmpty())
	{
		tcItem.pszText = str4.GetBuffer(MAX_TEXT);
		m_subDialogTab.InsertItem(m_subDialogTab.GetItemCount(), &tcItem);
	}
	m_subDialogTab.GetWindowRect(m_tabRect);
	TabCtrl_AdjustRect(m_subDialogTab.m_hWnd, FALSE, &m_tabRect);
	ScreenToClient(m_tabRect);

	if ( (nOldSel == -1) && m_subDialogTab.GetItemCount() )
		m_subDialogTab.SetCurSel(0);
	else
		if (nOldSel < m_subDialogTab.GetItemCount())
			m_subDialogTab.SetCurSel(nOldSel);
		else
			m_subDialogTab.SetCurSel(m_subDialogTab.GetItemCount() - 1);
}

void CDlgLayoutObjectControl::OnSelchangeSubdialogTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	Apply();	// DT 05/20/05: added because newly created textmarks lost 'output text' after -> switching to geometry -> draganddropping the mark and than -> assigning to sheet
				//				because of DataExchangeLoad() at the end of OnDrop()

	int nSel  = m_subDialogTab.GetCurSel();

	//if ( (m_nObjectType == CMark::SectionMark) || (m_nObjectType == CMark::CropMark) || (m_nObjectType == CMark::FoldMark) )
	//	if (EnableRulesTab())	// geometry tab is not initialized
	//		if (nSel > 0)
	//			nSel++;

	CDialog* pDlgGeometry;
	if (m_pLayoutObject)
		pDlgGeometry = &m_dlgLayoutObjectGeometry;
	else
		switch (m_nObjectType)
		{
		case CMark::CustomMark:  pDlgGeometry = &m_dlgCustomMarkGeometry;	break;
		case CMark::TextMark:	 pDlgGeometry = &m_dlgTextMarkGeometry;		break;
		case CMark::SectionMark: pDlgGeometry = &m_dlgSectionMarkGeometry;	break;
		case CMark::CropMark:	 pDlgGeometry = &m_dlgCropMarkGeometry;		break;
		case CMark::FoldMark:	 pDlgGeometry = &m_dlgFoldMarkGeometry;		break;
		case CMark::BarcodeMark: pDlgGeometry = &m_dlgBarcodeMarkGeometry;	break;
		case CMark::DrawingMark: pDlgGeometry = &m_dlgDrawingMarkGeometry;	break;
		default:				 pDlgGeometry = &m_dlgLayoutObjectGeometry;	break;
		}

	switch (nSel)
	{
	case 0:	if (m_dlgPageObjectContent.m_hWnd		&& (m_nObjectType == PageObject))			m_dlgPageObjectContent.ShowWindow	(SW_SHOW);	
			if (m_dlgCustomMarkContent.m_hWnd		&& (m_nObjectType == CMark::CustomMark))	m_dlgCustomMarkContent.ShowWindow	(SW_SHOW);	
			if (m_dlgTextMarkContent.m_hWnd			&& (m_nObjectType == CMark::TextMark))		m_dlgTextMarkContent.ShowWindow		(SW_SHOW);
			if (m_dlgSectionMarkContent.m_hWnd		&& (m_nObjectType == CMark::SectionMark))	m_dlgSectionMarkContent.ShowWindow	(SW_SHOW);
			if (m_dlgCropMarkContent.m_hWnd			&& (m_nObjectType == CMark::CropMark))		m_dlgCropMarkContent.ShowWindow		(SW_SHOW);
			if (m_dlgFoldMarkContent.m_hWnd			&& (m_nObjectType == CMark::FoldMark))		m_dlgFoldMarkContent.ShowWindow		(SW_SHOW);
			if (m_dlgBarcodeMarkContent.m_hWnd		&& (m_nObjectType == CMark::BarcodeMark))	m_dlgBarcodeMarkContent.ShowWindow	(SW_SHOW);
			if (m_dlgDrawingMarkContent.m_hWnd		&& (m_nObjectType == CMark::DrawingMark))	m_dlgDrawingMarkContent.ShowWindow	(SW_SHOW);
			if (pDlgGeometry->m_hWnd)															pDlgGeometry->ShowWindow			(SW_HIDE);	
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::CustomMark))	m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::TextMark))		m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::BarcodeMark))	m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::DrawingMark))	m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgObjectZOrder.m_hWnd			&& (m_nObjectType == CMark::FoldMark))		m_dlgObjectZOrder.ShowWindow		(SW_HIDE);
			if (m_dlgMarkRules.m_hWnd)															m_dlgMarkRules.ShowWindow			(SW_HIDE);
			break;

	case 1:	if (m_dlgPageObjectContent.m_hWnd		&& (m_nObjectType == PageObject))			m_dlgPageObjectContent.ShowWindow	(SW_HIDE);	
			if (m_dlgCustomMarkContent.m_hWnd		&& (m_nObjectType == CMark::CustomMark))	m_dlgCustomMarkContent.ShowWindow	(SW_HIDE);	
			if (m_dlgTextMarkContent.m_hWnd			&& (m_nObjectType == CMark::TextMark))		m_dlgTextMarkContent.ShowWindow		(SW_HIDE);
			if (m_dlgSectionMarkContent.m_hWnd		&& (m_nObjectType == CMark::SectionMark))	m_dlgSectionMarkContent.ShowWindow	(SW_HIDE);
			if (m_dlgCropMarkContent.m_hWnd			&& (m_nObjectType == CMark::CropMark))		m_dlgCropMarkContent.ShowWindow		(SW_HIDE);
			if (m_dlgFoldMarkContent.m_hWnd			&& (m_nObjectType == CMark::FoldMark))		m_dlgFoldMarkContent.ShowWindow		(SW_HIDE);
			if (m_dlgBarcodeMarkContent.m_hWnd		&& (m_nObjectType == CMark::BarcodeMark))	m_dlgBarcodeMarkContent.ShowWindow	(SW_HIDE);
			if (m_dlgDrawingMarkContent.m_hWnd		&& (m_nObjectType == CMark::DrawingMark))	m_dlgDrawingMarkContent.ShowWindow	(SW_HIDE);
			if (pDlgGeometry->m_hWnd)															pDlgGeometry->ShowWindow			(SW_SHOW);	
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::CustomMark))	m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::TextMark))		m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::BarcodeMark))	m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::DrawingMark))	m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgObjectZOrder.m_hWnd			&& (m_nObjectType == CMark::FoldMark))		m_dlgObjectZOrder.ShowWindow		(SW_HIDE);
			if (m_dlgMarkRules.m_hWnd)															m_dlgMarkRules.ShowWindow			(SW_HIDE);
			break;

	case 2:	if (m_dlgPageObjectContent.m_hWnd		&& (m_nObjectType == PageObject))			m_dlgPageObjectContent.ShowWindow	(SW_HIDE);	
			if (m_dlgCustomMarkContent.m_hWnd		&& (m_nObjectType == CMark::CustomMark))	m_dlgCustomMarkContent.ShowWindow	(SW_HIDE);	
			if (m_dlgTextMarkContent.m_hWnd			&& (m_nObjectType == CMark::TextMark))		m_dlgTextMarkContent.ShowWindow		(SW_HIDE);
			if (m_dlgSectionMarkContent.m_hWnd		&& (m_nObjectType == CMark::SectionMark))	m_dlgSectionMarkContent.ShowWindow	(SW_HIDE);
			if (m_dlgCropMarkContent.m_hWnd			&& (m_nObjectType == CMark::CropMark))		m_dlgCropMarkContent.ShowWindow		(SW_HIDE);
			if (m_dlgFoldMarkContent.m_hWnd			&& (m_nObjectType == CMark::FoldMark))		m_dlgFoldMarkContent.ShowWindow		(SW_HIDE);
			if (m_dlgBarcodeMarkContent.m_hWnd		&& (m_nObjectType == CMark::BarcodeMark))	m_dlgBarcodeMarkContent.ShowWindow	(SW_HIDE);
			if (m_dlgDrawingMarkContent.m_hWnd		&& (m_nObjectType == CMark::DrawingMark))	m_dlgDrawingMarkContent.ShowWindow	(SW_HIDE);
			if (pDlgGeometry->m_hWnd)															pDlgGeometry->ShowWindow			(SW_HIDE);	
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::CustomMark))	m_dlgMarkPlacement.ShowWindow		(SW_SHOW);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::TextMark))		m_dlgMarkPlacement.ShowWindow		(SW_SHOW);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::BarcodeMark))	m_dlgMarkPlacement.ShowWindow		(SW_SHOW);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::DrawingMark))	m_dlgMarkPlacement.ShowWindow		(SW_SHOW);
			//if (m_dlgObjectZOrder.m_hWnd			&& (m_nObjectType == CMark::FoldMark))		m_dlgObjectZOrder.ShowWindow		(SW_SHOW);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::CustomMark))	m_dlgMarkRules.ShowWindow			(SW_HIDE);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::TextMark))		m_dlgMarkRules.ShowWindow			(SW_HIDE);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::BarcodeMark))	m_dlgMarkRules.ShowWindow			(SW_HIDE);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::DrawingMark))	m_dlgMarkRules.ShowWindow			(SW_HIDE);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::FoldMark))		m_dlgMarkRules.ShowWindow			(SW_SHOW);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::SectionMark))	m_dlgMarkRules.ShowWindow			(SW_SHOW);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::CropMark))		m_dlgMarkRules.ShowWindow			(SW_SHOW);
			break;

	case 3:	if (m_dlgPageObjectContent.m_hWnd		&& (m_nObjectType == PageObject))			m_dlgPageObjectContent.ShowWindow	(SW_HIDE);	
			if (m_dlgCustomMarkContent.m_hWnd		&& (m_nObjectType == CMark::CustomMark))	m_dlgCustomMarkContent.ShowWindow	(SW_HIDE);	
			if (m_dlgTextMarkContent.m_hWnd			&& (m_nObjectType == CMark::TextMark))		m_dlgTextMarkContent.ShowWindow		(SW_HIDE);
			if (m_dlgSectionMarkContent.m_hWnd		&& (m_nObjectType == CMark::SectionMark))	m_dlgSectionMarkContent.ShowWindow	(SW_HIDE);
			if (m_dlgCropMarkContent.m_hWnd			&& (m_nObjectType == CMark::CropMark))		m_dlgCropMarkContent.ShowWindow		(SW_HIDE);
			if (m_dlgFoldMarkContent.m_hWnd			&& (m_nObjectType == CMark::FoldMark))		m_dlgFoldMarkContent.ShowWindow		(SW_HIDE);
			if (m_dlgBarcodeMarkContent.m_hWnd		&& (m_nObjectType == CMark::BarcodeMark))	m_dlgBarcodeMarkContent.ShowWindow	(SW_HIDE);
			if (m_dlgDrawingMarkContent.m_hWnd		&& (m_nObjectType == CMark::DrawingMark))	m_dlgDrawingMarkContent.ShowWindow	(SW_HIDE);
			if (pDlgGeometry->m_hWnd)															pDlgGeometry->ShowWindow			(SW_HIDE);	
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::CustomMark))	m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::TextMark))		m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::BarcodeMark))	m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgMarkPlacement.m_hWnd			&& (m_nObjectType == CMark::DrawingMark))	m_dlgMarkPlacement.ShowWindow		(SW_HIDE);
			if (m_dlgObjectZOrder.m_hWnd			&& (m_nObjectType == CMark::FoldMark))		m_dlgObjectZOrder.ShowWindow		(SW_HIDE);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::CustomMark))	m_dlgMarkRules.ShowWindow			(SW_SHOW);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::TextMark))		m_dlgMarkRules.ShowWindow			(SW_SHOW);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::BarcodeMark))	m_dlgMarkRules.ShowWindow			(SW_SHOW);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::DrawingMark))	m_dlgMarkRules.ShowWindow			(SW_SHOW);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::FoldMark))		m_dlgMarkRules.ShowWindow			(SW_HIDE);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::SectionMark))	m_dlgMarkRules.ShowWindow			(SW_HIDE);
			if (m_dlgMarkRules.m_hWnd				&& (m_nObjectType == CMark::CropMark))		m_dlgMarkRules.ShowWindow			(SW_HIDE);
			break;
	}

	Invalidate();
	UpdateWindow();

	if (pNMHDR)
		UpdateView();	

	*pResult = 0;
}

BOOL CDlgLayoutObjectControl::IsActiveReflineControl()
{
	if (m_subDialogTab.GetCurSel() == 1)
		return TRUE;
	else
		return FALSE;
}

void CDlgLayoutObjectControl::DataExchangeLoad()
{
	switch (GetObjectType())
	{
		case PageObject:			LoadPageObjectData();	break;
		case CMark::CustomMark:		LoadCustomMarkData();	break;
		case CMark::TextMark:		LoadTextMarkData();		break;
		case CMark::SectionMark:	LoadSectionMarkData();	break;
		case CMark::CropMark:		LoadCropMarkData();		break;
		case CMark::FoldMark:		LoadFoldMarkData();		break;
		case CMark::BarcodeMark:	LoadBarcodeMarkData();	break;
		case CMark::DrawingMark:	LoadDrawingMarkData();	break;
		default:											break;
	}
	ObjectPreviewWindow_LoadData();
}

void CDlgLayoutObjectControl::DataExchangeSave()
{
	switch (GetObjectType())
	{
		case PageObject:			SavePageObjectData();	break;
		case CMark::CustomMark:		SaveCustomMarkData();	break;
		case CMark::TextMark:		SaveTextMarkData();		break;
		case CMark::SectionMark:	SaveSectionMarkData();	break;
		case CMark::CropMark:		SaveCropMarkData();		break;
		case CMark::FoldMark:		SaveFoldMarkData();		break;
		case CMark::BarcodeMark:	SaveBarcodeMarkData();	break;
		case CMark::DrawingMark:	SaveDrawingMarkData();	break;
		default:											break;
	}
}

CLayoutObject* CDlgLayoutObjectControl::GetFirstLayoutObject()
{
	if (this == NULL)
		return NULL;

	return m_pLayoutObject;
}

CLayoutObject* CDlgLayoutObjectControl::GetNextLayoutObject(CLayoutObject* pLayoutObject)
{
	if (this == NULL)
		return NULL;

	//CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	//if (pView)
	//{
	//	CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	//	while (pDI)
	//	{
	//		CLayoutObject* pRefObject = (CLayoutObject*)pDI->m_pData;
	//		if (pRefObject)
	//			if (pRefObject == pLayoutObject)
	//			{
	//				pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	//				pRefObject = (pDI) ? (CLayoutObject*)pDI->m_pData : NULL;
	//				if (pRefObject)
	//					if (pRefObject != m_pLayoutObject)
	//						return pRefObject;
	//				break;
	//			}
	//		pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
	//	}
	//}

	return NULL;
}

CMark* CDlgLayoutObjectControl::GetFirstMark()
{
	if (this == NULL)
		return NULL;

	return m_pMark;
}

CMark* CDlgLayoutObjectControl::GetNextMark(CMark* pMark)
{
	if (this == NULL)
		return NULL;

	return NULL;
}

CPrintSheet* CDlgLayoutObjectControl::GetPrintSheet()
{
	if (this == NULL)
		return NULL;

	return m_pPrintSheet;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Page object ///////////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgLayoutObjectControl::InitPageObject()
{
	InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, -1);
	DlgHideAll();

	CRect dlgRect;
	if ( ! m_dlgPageObjectContent.m_hWnd)
	{
#ifdef PDF
		m_dlgPageObjectContent.Create(IDD_PDFOBJECT_CONTENT, this);
#else
		m_dlgPageObjectContent.Create(IDD_BMPOBJECT_CONTENT, this);
#endif
		m_dlgPageObjectContent.GetWindowRect(dlgRect);
		m_dlgPageObjectContent.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgLayoutObjectGeometry.m_hWnd)
	{
		m_dlgLayoutObjectGeometry.Create(IDD_LAYOUTOBJECT_GEOMETRY, this);
		m_dlgLayoutObjectGeometry.GetWindowRect(dlgRect);
		m_dlgLayoutObjectGeometry.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
}

void CDlgLayoutObjectControl::LoadPageObjectData()
{
	m_dlgPageObjectContent.LoadData();
	m_dlgLayoutObjectGeometry.LoadData();
}

void CDlgLayoutObjectControl::SavePageObjectData()
{
	m_dlgPageObjectContent.SaveData();
	m_dlgLayoutObjectGeometry.SaveData();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CustomMark object ///////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgLayoutObjectControl::InitCustomMark()
{
	if (EnableRulesTab())
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_PLACEMENT, IDS_RULES);
	else
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_PLACEMENT);
	DlgHideAll();

	CDlgLayoutObjectGeometry* pDlgGeometry = (m_pLayoutObject) ? &m_dlgLayoutObjectGeometry : &m_dlgCustomMarkGeometry;
	CRect					  dlgRect;
	if ( ! m_dlgCustomMarkContent.m_hWnd)
	{
		m_dlgCustomMarkContent.Create(IDD_PDFOBJECT_CONTENT, this);
		m_dlgCustomMarkContent.GetWindowRect(dlgRect);
		m_dlgCustomMarkContent.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! pDlgGeometry->m_hWnd)
	{
		if (m_nObjectType != PageObject)
			pDlgGeometry->m_markPivotCtrl.Reinit(IDD_LAYOUTOBJECT_GEOMETRY);

		pDlgGeometry->Create(IDD_LAYOUTOBJECT_GEOMETRY, this);
		pDlgGeometry->GetWindowRect(dlgRect);
		pDlgGeometry->SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkPlacement.m_hWnd)
	{
		m_dlgMarkPlacement.Create(IDD_MARK_PLACEMENT, this);
		m_dlgMarkPlacement.GetWindowRect(dlgRect);
		m_dlgMarkPlacement.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkRules.m_hWnd)
	{
		m_dlgMarkRules.Create(IDD_MARK_RULES, this);
		m_dlgMarkRules.GetWindowRect(dlgRect);
		m_dlgMarkRules.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
}

void CDlgLayoutObjectControl::LoadCustomMarkData()
{
	m_dlgCustomMarkContent.LoadData();
	m_dlgLayoutObjectGeometry.LoadData();
	m_dlgCustomMarkGeometry.LoadData();
	m_dlgMarkPlacement.LoadData();
	m_dlgMarkRules.LoadData();
}

void CDlgLayoutObjectControl::SaveCustomMarkData()
{
	m_dlgCustomMarkContent.SaveData();
	m_dlgLayoutObjectGeometry.SaveData();
	m_dlgCustomMarkGeometry.SaveData();
	m_dlgMarkPlacement.SaveData();
	m_dlgMarkRules.SaveData();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TextMark object ///////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgLayoutObjectControl::InitTextMark()
{
	if (EnableRulesTab())
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_PLACEMENT, IDS_RULES);
	else
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_PLACEMENT);
	DlgHideAll();

	CDlgLayoutObjectGeometry* pDlgGeometry = (m_pLayoutObject) ? &m_dlgLayoutObjectGeometry : &m_dlgTextMarkGeometry;
	CRect					  dlgRect;
	if ( ! m_dlgTextMarkContent.m_hWnd)
	{
		m_dlgTextMarkContent.Create(IDD_TEXTMARKOBJECT_CONTENT, this);
		m_dlgTextMarkContent.GetWindowRect(dlgRect);
		m_dlgTextMarkContent.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! pDlgGeometry->m_hWnd)
	{
		if (m_nObjectType != PageObject)
			pDlgGeometry->m_markPivotCtrl.Reinit(IDD_LAYOUTOBJECT_GEOMETRY);

		pDlgGeometry->Create(IDD_LAYOUTOBJECT_GEOMETRY, this);
		pDlgGeometry->GetWindowRect(dlgRect);
		pDlgGeometry->SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkPlacement.m_hWnd)
	{
		m_dlgMarkPlacement.Create(IDD_MARK_PLACEMENT, this);
		m_dlgMarkPlacement.GetWindowRect(dlgRect);
		m_dlgMarkPlacement.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkRules.m_hWnd)
	{
		m_dlgMarkRules.Create(IDD_MARK_RULES, this);
		m_dlgMarkRules.GetWindowRect(dlgRect);
		m_dlgMarkRules.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
}

void CDlgLayoutObjectControl::LoadTextMarkData()
{
	m_dlgTextMarkContent.LoadData();
	m_dlgLayoutObjectGeometry.LoadData();
	m_dlgTextMarkGeometry.LoadData();
	m_dlgMarkPlacement.LoadData();
	m_dlgMarkRules.LoadData();
}

void CDlgLayoutObjectControl::SaveTextMarkData()
{
	m_dlgTextMarkContent.SaveData();
	m_dlgLayoutObjectGeometry.SaveData();
	m_dlgTextMarkGeometry.SaveData();
	m_dlgMarkPlacement.SaveData();
	m_dlgMarkRules.SaveData();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SectionMark object ///////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgLayoutObjectControl::InitSectionMark()
{
	if (EnableRulesTab())
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_RULES);
	else
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY);
	DlgHideAll();

	CDialog* pDlgGeometry = (m_pLayoutObject) ? (CDialog*)&m_dlgLayoutObjectGeometry : (CDialog*)&m_dlgSectionMarkGeometry;
	CRect    dlgRect;
	if ( ! m_dlgSectionMarkContent.m_hWnd)
	{
		m_dlgSectionMarkContent.Create(IDD_SECTIONMARK_CONTENT, this);
		m_dlgSectionMarkContent.GetWindowRect(dlgRect);
		m_dlgSectionMarkContent.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! pDlgGeometry->m_hWnd)
	{
		if (m_pLayoutObject)
			if (m_nObjectType != PageObject)
				m_dlgLayoutObjectGeometry.m_markPivotCtrl.Reinit(IDD_LAYOUTOBJECT_GEOMETRY);

		pDlgGeometry->Create((m_pLayoutObject) ? IDD_LAYOUTOBJECT_GEOMETRY : IDD_SECTIONMARK_GEOMETRY, this);
		pDlgGeometry->GetWindowRect(dlgRect);
		pDlgGeometry->SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkRules.m_hWnd)
	{
		m_dlgMarkRules.Create(IDD_MARK_RULES, this);
		m_dlgMarkRules.GetWindowRect(dlgRect);
		m_dlgMarkRules.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
}

void CDlgLayoutObjectControl::LoadSectionMarkData()
{
	m_dlgSectionMarkContent.LoadData();
	m_dlgLayoutObjectGeometry.LoadData();
	m_dlgSectionMarkGeometry.LoadData();
	m_dlgMarkRules.LoadData();
}

void CDlgLayoutObjectControl::SaveSectionMarkData()
{
	m_dlgSectionMarkContent.SaveData();
	m_dlgLayoutObjectGeometry.SaveData();
	m_dlgSectionMarkGeometry.SaveData();
	m_dlgMarkRules.SaveData();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CropMark object ///////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgLayoutObjectControl::InitCropMark()
{
	if (EnableRulesTab())
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_RULES);
	else
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY);
	DlgHideAll();

	CDialog* pDlgGeometry = (m_pLayoutObject) ? (CDialog*)&m_dlgLayoutObjectGeometry : (CDialog*)&m_dlgCropMarkGeometry;
	CRect    dlgRect;
	if ( ! m_dlgCropMarkContent.m_hWnd)
	{
		m_dlgCropMarkContent.Create(IDD_CROPMARK_CONTENT, this);
		m_dlgCropMarkContent.GetWindowRect(dlgRect);
		m_dlgCropMarkContent.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! pDlgGeometry->m_hWnd)
	{
		if (m_pLayoutObject)
			if (m_nObjectType != PageObject)
				m_dlgLayoutObjectGeometry.m_markPivotCtrl.Reinit(IDD_LAYOUTOBJECT_GEOMETRY);

		pDlgGeometry->Create((m_pLayoutObject) ? IDD_LAYOUTOBJECT_GEOMETRY : IDD_CROPMARK_GEOMETRY, this);
		pDlgGeometry->GetWindowRect(dlgRect);
		pDlgGeometry->SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkRules.m_hWnd)
	{
		m_dlgMarkRules.Create(IDD_MARK_RULES, this);
		m_dlgMarkRules.GetWindowRect(dlgRect);
		m_dlgMarkRules.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
}

void CDlgLayoutObjectControl::LoadCropMarkData()
{
	m_dlgCropMarkContent.LoadData();
	m_dlgLayoutObjectGeometry.LoadData();
	m_dlgCropMarkGeometry.LoadData();
	m_dlgMarkRules.LoadData();
}

void CDlgLayoutObjectControl::SaveCropMarkData()
{
	m_dlgCropMarkContent.SaveData();
	m_dlgLayoutObjectGeometry.SaveData();
	m_dlgCropMarkGeometry.SaveData();
	m_dlgMarkRules.SaveData();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FoldMark object ///////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgLayoutObjectControl::InitFoldMark()
{
	if (EnableRulesTab())
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY/*, IDS_ZORDER*/, IDS_RULES);
	else
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY/*, IDS_ZORDER*/);
	DlgHideAll();

	CDialog* pDlgGeometry = (m_pLayoutObject) ? (CDialog*)&m_dlgLayoutObjectGeometry : (CDialog*)&m_dlgFoldMarkGeometry;
	CRect    dlgRect;
	if ( ! m_dlgFoldMarkContent.m_hWnd)
	{
		m_dlgFoldMarkContent.Create(IDD_FOLDMARK_CONTENT, this);
		m_dlgFoldMarkContent.GetWindowRect(dlgRect);
		m_dlgFoldMarkContent.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! pDlgGeometry->m_hWnd)
	{
		if (m_pLayoutObject)
			if (m_nObjectType != PageObject)
				m_dlgLayoutObjectGeometry.m_markPivotCtrl.Reinit(IDD_LAYOUTOBJECT_GEOMETRY);

		pDlgGeometry->Create((m_pLayoutObject) ? IDD_LAYOUTOBJECT_GEOMETRY : IDD_FOLDMARK_GEOMETRY, this);
		pDlgGeometry->GetWindowRect(dlgRect);
		pDlgGeometry->SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	//if ( ! m_dlgObjectZOrder.m_hWnd)
	//{
	//	m_dlgObjectZOrder.Create(IDD_OBJECT_ZORDER, this);
	//	m_dlgObjectZOrder.GetWindowRect(dlgRect);
	//	m_dlgObjectZOrder.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	//}
	if ( ! m_dlgMarkRules.m_hWnd)
	{
		m_dlgMarkRules.Create(IDD_MARK_RULES, this);
		m_dlgMarkRules.GetWindowRect(dlgRect);
		m_dlgMarkRules.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
}

void CDlgLayoutObjectControl::LoadFoldMarkData()
{
	m_dlgFoldMarkContent.LoadData();
	m_dlgLayoutObjectGeometry.LoadData();
	m_dlgFoldMarkGeometry.LoadData();
	m_dlgObjectZOrder.LoadData();
	m_dlgMarkRules.LoadData();
}

void CDlgLayoutObjectControl::SaveFoldMarkData()
{
	m_dlgFoldMarkContent.SaveData();
	m_dlgLayoutObjectGeometry.SaveData();
	m_dlgFoldMarkGeometry.SaveData();
	m_dlgObjectZOrder.SaveData();
	m_dlgMarkRules.SaveData();
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BarcodeMark object ///////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgLayoutObjectControl::InitBarcodeMark()
{
	if (EnableRulesTab())
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_PLACEMENT, IDS_RULES);
	else
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_PLACEMENT);
	DlgHideAll();

	CDlgLayoutObjectGeometry* pDlgGeometry = (m_pLayoutObject) ? &m_dlgLayoutObjectGeometry : &m_dlgBarcodeMarkGeometry;
	CRect					  dlgRect;
	if ( ! m_dlgBarcodeMarkContent.m_hWnd)
	{
		m_dlgBarcodeMarkContent.Create(IDD_BARCODEMARK_CONTENT, this);
		m_dlgBarcodeMarkContent.GetWindowRect(dlgRect);
		m_dlgBarcodeMarkContent.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! pDlgGeometry->m_hWnd)
	{
		if (m_nObjectType != PageObject)
			pDlgGeometry->m_markPivotCtrl.Reinit(IDD_LAYOUTOBJECT_GEOMETRY);

		pDlgGeometry->Create(IDD_LAYOUTOBJECT_GEOMETRY, this);
		pDlgGeometry->GetWindowRect(dlgRect);
		pDlgGeometry->SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkPlacement.m_hWnd)
	{
		m_dlgMarkPlacement.Create(IDD_MARK_PLACEMENT, this);
		m_dlgMarkPlacement.GetWindowRect(dlgRect);
		m_dlgMarkPlacement.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkRules.m_hWnd)
	{
		m_dlgMarkRules.Create(IDD_MARK_RULES, this);
		m_dlgMarkRules.GetWindowRect(dlgRect);
		m_dlgMarkRules.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
}

void CDlgLayoutObjectControl::LoadBarcodeMarkData()
{
	m_dlgBarcodeMarkContent.LoadData();
	m_dlgLayoutObjectGeometry.LoadData();
	m_dlgBarcodeMarkGeometry.LoadData();
	m_dlgMarkPlacement.LoadData();
	m_dlgMarkRules.LoadData();
}

void CDlgLayoutObjectControl::SaveBarcodeMarkData()
{
	m_dlgBarcodeMarkContent.SaveData();
	m_dlgLayoutObjectGeometry.SaveData();
	m_dlgBarcodeMarkGeometry.SaveData();
	m_dlgMarkPlacement.SaveData();
	m_dlgMarkRules.SaveData();
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DrawingMark object ///////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgLayoutObjectControl::InitDrawingMark()
{
	if (EnableRulesTab())
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_PLACEMENT, IDS_RULES);
	else
		InitSubDialogTab(IDS_CONTENT, IDS_GEOMETRY, IDS_PLACEMENT);
	DlgHideAll();

	CDlgLayoutObjectGeometry* pDlgGeometry = (m_pLayoutObject) ? &m_dlgLayoutObjectGeometry : &m_dlgDrawingMarkGeometry;
	CRect					  dlgRect;
	if ( ! m_dlgDrawingMarkContent.m_hWnd)
	{
		m_dlgDrawingMarkContent.Create(IDD_DRAWINGMARK_CONTENT, this);
		m_dlgDrawingMarkContent.GetWindowRect(dlgRect);
		m_dlgDrawingMarkContent.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! pDlgGeometry->m_hWnd)
	{
		if (m_nObjectType != PageObject)
			pDlgGeometry->m_markPivotCtrl.Reinit(IDD_LAYOUTOBJECT_GEOMETRY);

		pDlgGeometry->Create(IDD_LAYOUTOBJECT_GEOMETRY, this);
		pDlgGeometry->GetWindowRect(dlgRect);
		pDlgGeometry->SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkPlacement.m_hWnd)
	{
		m_dlgMarkPlacement.Create(IDD_MARK_PLACEMENT, this);
		m_dlgMarkPlacement.GetWindowRect(dlgRect);
		m_dlgMarkPlacement.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
	if ( ! m_dlgMarkRules.m_hWnd)
	{
		m_dlgMarkRules.Create(IDD_MARK_RULES, this);
		m_dlgMarkRules.GetWindowRect(dlgRect);
		m_dlgMarkRules.SetWindowPos(NULL, m_tabRect.left, m_tabRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);
	}
}

void CDlgLayoutObjectControl::LoadDrawingMarkData()
{
	m_dlgDrawingMarkContent.LoadData();
	m_dlgLayoutObjectGeometry.LoadData();
	m_dlgDrawingMarkGeometry.LoadData();
	m_dlgMarkPlacement.LoadData();
	m_dlgMarkRules.LoadData();
}

void CDlgLayoutObjectControl::SaveDrawingMarkData()
{
	m_dlgDrawingMarkContent.SaveData();
	m_dlgLayoutObjectGeometry.SaveData();
	m_dlgDrawingMarkGeometry.SaveData();
	m_dlgMarkPlacement.SaveData();
	m_dlgMarkRules.SaveData();
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgLayoutObjectControl::Apply() 
{
	//CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));	// moved to OnApplyButton()
	//if (pView)
	//	pView->UndoSave();

	if (m_dlgPageObjectContent.m_hWnd)		m_dlgPageObjectContent.Apply();
	if (m_dlgCustomMarkContent.m_hWnd)		m_dlgCustomMarkContent.Apply();
	if (m_dlgTextMarkContent.m_hWnd)		m_dlgTextMarkContent.Apply();
	if (m_dlgSectionMarkContent.m_hWnd)		m_dlgSectionMarkContent.Apply();
	if (m_dlgCropMarkContent.m_hWnd)		m_dlgCropMarkContent.Apply();
	if (m_dlgFoldMarkContent.m_hWnd)		m_dlgFoldMarkContent.Apply();
	if (m_dlgBarcodeMarkContent.m_hWnd)		m_dlgBarcodeMarkContent.Apply();
	if (m_dlgDrawingMarkContent.m_hWnd)		m_dlgDrawingMarkContent.Apply();
	if (m_dlgLayoutObjectGeometry.m_hWnd)	m_dlgLayoutObjectGeometry.Apply();
	if (m_dlgCustomMarkGeometry.m_hWnd)		m_dlgCustomMarkGeometry.Apply();
	if (m_dlgTextMarkGeometry.m_hWnd)		m_dlgTextMarkGeometry.Apply();
	if (m_dlgSectionMarkGeometry.m_hWnd)	m_dlgSectionMarkGeometry.Apply();
	if (m_dlgCropMarkGeometry.m_hWnd)		m_dlgCropMarkGeometry.Apply();
	if (m_dlgFoldMarkGeometry.m_hWnd)		m_dlgFoldMarkGeometry.Apply();
	if (m_dlgBarcodeMarkGeometry.m_hWnd)	m_dlgBarcodeMarkGeometry.Apply();
	if (m_dlgDrawingMarkGeometry.m_hWnd)	m_dlgDrawingMarkGeometry.Apply();
	if (m_dlgMarkPlacement.m_hWnd)			m_dlgMarkPlacement.Apply();
	if (m_dlgObjectZOrder.m_hWnd)			m_dlgObjectZOrder.Apply();
	if (m_dlgMarkRules.m_hWnd)				m_dlgMarkRules.Apply();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( pDoc)
	{
		pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
		if (m_pLayoutObject)
		{
			pDoc->m_PageSourceList.InitializePageTemplateRefs();
			pDoc->m_MarkSourceList.InitializePageTemplateRefs();
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
		}
	}

	//UpdateView();

	// Reload data, because they could be changed by reorganization
	if (m_dlgPageObjectContent.m_hWnd)		m_dlgPageObjectContent.LoadData(); 
	if (m_dlgCustomMarkContent.m_hWnd)		m_dlgCustomMarkContent.LoadData();
	if (m_dlgTextMarkContent.m_hWnd)		m_dlgTextMarkContent.LoadData();
	if (m_dlgSectionMarkContent.m_hWnd)		m_dlgSectionMarkContent.LoadData();
	if (m_dlgCropMarkContent.m_hWnd)		m_dlgCropMarkContent.LoadData();
	if (m_dlgFoldMarkContent.m_hWnd)		m_dlgFoldMarkContent.LoadData();
	if (m_dlgBarcodeMarkContent.m_hWnd)		m_dlgBarcodeMarkContent.LoadData();
	if (m_dlgDrawingMarkContent.m_hWnd)		m_dlgDrawingMarkContent.LoadData();
	if (m_dlgLayoutObjectGeometry.m_hWnd)	m_dlgLayoutObjectGeometry.LoadData();
	if (m_dlgCustomMarkGeometry.m_hWnd)		m_dlgCustomMarkGeometry.LoadData();
	if (m_dlgTextMarkGeometry.m_hWnd)		m_dlgTextMarkGeometry.LoadData();
	if (m_dlgSectionMarkGeometry.m_hWnd)	m_dlgSectionMarkGeometry.LoadData();
	if (m_dlgCropMarkGeometry.m_hWnd)		m_dlgCropMarkGeometry.LoadData();
	if (m_dlgFoldMarkGeometry.m_hWnd)		m_dlgFoldMarkGeometry.LoadData();
	if (m_dlgBarcodeMarkGeometry.m_hWnd)	m_dlgBarcodeMarkGeometry.LoadData();
	if (m_dlgDrawingMarkGeometry.m_hWnd)	m_dlgDrawingMarkGeometry.LoadData();
	if (m_dlgMarkPlacement.m_hWnd)			m_dlgMarkPlacement.LoadData();
	if (m_dlgObjectZOrder.m_hWnd)			m_dlgObjectZOrder.LoadData();
	if (m_dlgMarkRules.m_hWnd)				m_dlgMarkRules.LoadData();

	ObjectPreviewWindow_LoadData();
}

void CDlgLayoutObjectControl::OK() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	pView->m_DisplayList.Invalidate();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( pDoc)
	{
		pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
		if (m_pLayoutObject)
		{
			pDoc->m_PageSourceList.InitializePageTemplateRefs();
			pDoc->m_MarkSourceList.InitializePageTemplateRefs();
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
			pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
		}
	}

	DestroyWindow();
}

void CDlgLayoutObjectControl::Cancel() 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	pView->m_DisplayList.Invalidate();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),	NULL, 0, NULL);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( pDoc)
	{
		pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
		if (m_pLayoutObject)
		{
			pDoc->m_PageSourceList.InitializePageTemplateRefs();
			pDoc->m_MarkSourceList.InitializePageTemplateRefs();
			pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_PrintSheetList.ReorganizeColorInfos();
			pDoc->SetModifiedFlag(TRUE, NULL, FALSE);
		}
	}

	DestroyWindow();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void CDlgLayoutObjectControl::UpdateView()
{
	if (m_bLockUpdateView)
		return;

	CPrintSheetMarksView* pView = (CPrintSheetMarksView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetMarksView));
	if (pView)
		pView->UpdateSheetPreview();
	else
	{
		CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView)
		{
			pView->m_DisplayList.Invalidate();
			//pView->m_DisplayList.DeselectAll();
			pView->OnUpdate(NULL, 0, NULL);
			pView->Invalidate();
			pView->UpdateWindow();
		}
	}
	if ( ! m_hWnd)
		return;

//	CWnd* pWnd = GetParent();
//	if (pWnd)
////		if (pWnd->IsKindOf(RUNTIME_CLASS(CDlgMarkSet)))
//		{
//			pWnd = pWnd->GetParent();
//			if (pWnd)
//				if (pWnd->IsKindOf(RUNTIME_CLASS(CDlgJobData)))
//					((CDlgJobData*)pWnd)->m_printDataPage.PostMessage(WM_COMMAND, ID_UPDATE_MARKSET);
//		}
}

int CDlgLayoutObjectControl::ColorDefTable_GetSize()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	return pDoc->m_ColorDefinitionTable.GetSize();
}

CColorDefinitionTable* CDlgLayoutObjectControl::GetActColorDefTable()
{
	if (this == NULL)
		return NULL;

	if (GetFirstLayoutObject())
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return NULL;
		return &pDoc->m_ColorDefinitionTable;
	}
	else
		if (GetFirstMark())
			return m_pColorDefTable;
		else
			return NULL;
}

CColorDefinition* CDlgLayoutObjectControl::ColorDefTable_GetColorDef(int nColorDefinitionIndex)
{
	if (this == NULL)
		return NULL;

	CColorDefinitionTable* pColorDefTable = GetActColorDefTable();
	if ( ! pColorDefTable)
		return NULL;

	if ( (nColorDefinitionIndex >= 0) && (nColorDefinitionIndex < pColorDefTable->GetSize()) )
		return &pColorDefTable->ElementAt(nColorDefinitionIndex);
	else
		return NULL;
}

int CDlgLayoutObjectControl::ColorDefTable_FindColorDefinitionIndex(const CString& strColorName)
{
	if (this == NULL)
		return -1;

	CColorDefinitionTable* pColorDefTable = GetActColorDefTable();
	if ( ! pColorDefTable)
		return -1;

	return pColorDefTable->FindColorDefinitionIndex(strColorName);
}

int CDlgLayoutObjectControl::ColorDefTable_InsertColorDef(CColorDefinition& rColorDef)
{
	if (this == NULL)
		return -1;

	CColorDefinitionTable* pColorDefTable = GetActColorDefTable();
	if ( ! pColorDefTable)
		return -1;

	return pColorDefTable->InsertColorDef(rColorDef);
}

BOOL CDlgLayoutObjectControl::IsEditMode()
{
	if ( ! m_hWnd)
		return FALSE;

	CWnd* pParent = GetParent();
	if (pParent)
		if (pParent->IsKindOf(RUNTIME_CLASS(CDlgMarkSet)))
			if ( ((CDlgMarkSet*)pParent)->m_bIsEditMode)
				return TRUE;
			else
				return FALSE;

	return TRUE;	// if not embedded in DlgMarkSet, always edit mode
}

BOOL CDlgLayoutObjectControl::IsOpenDlgGeometry()
{
	if ( ! IsEditMode())
		return FALSE;

	if (m_dlgLayoutObjectGeometry.m_hWnd)
		if (m_dlgLayoutObjectGeometry.IsWindowVisible())
			return TRUE;

	if (m_dlgCustomMarkGeometry.m_hWnd)
		if (m_dlgCustomMarkGeometry.IsWindowVisible())
			return TRUE;

	if (m_dlgTextMarkGeometry.m_hWnd)
		if (m_dlgTextMarkGeometry.IsWindowVisible())
			return TRUE;

	if (m_dlgBarcodeMarkGeometry.m_hWnd)
		if (m_dlgBarcodeMarkGeometry.IsWindowVisible())
			return TRUE;

	if (m_dlgDrawingMarkGeometry.m_hWnd)
		if (m_dlgDrawingMarkGeometry.IsWindowVisible())
			return TRUE;

	return FALSE;
}

CDlgLayoutObjectGeometry* CDlgLayoutObjectControl::GetDlgGeometry()
{
	if (m_dlgLayoutObjectGeometry.m_hWnd)
		if (m_dlgLayoutObjectGeometry.IsWindowVisible())
			return &m_dlgLayoutObjectGeometry;

	if (m_dlgCustomMarkGeometry.m_hWnd)
		if (m_dlgCustomMarkGeometry.IsWindowVisible())
			return &m_dlgCustomMarkGeometry;

	if (m_dlgTextMarkGeometry.m_hWnd)
		if (m_dlgTextMarkGeometry.IsWindowVisible())
			return &m_dlgTextMarkGeometry;

	if (m_dlgBarcodeMarkGeometry.m_hWnd)
		if (m_dlgBarcodeMarkGeometry.IsWindowVisible())
			return &m_dlgBarcodeMarkGeometry;

	if (m_dlgDrawingMarkGeometry.m_hWnd)
		if (m_dlgDrawingMarkGeometry.IsWindowVisible())
			return &m_dlgDrawingMarkGeometry;

	return NULL;
}

BOOL CDlgLayoutObjectControl::IsOpenDlgPDFObjectContent()
{
	if ( ! IsEditMode())
		return FALSE;

	if (m_dlgCustomMarkContent.m_hWnd)
		if (m_dlgCustomMarkContent.IsWindowVisible())
			return TRUE;

	if (m_dlgPageObjectContent.m_hWnd)
		if (m_dlgPageObjectContent.IsWindowVisible())
			return TRUE;

	return FALSE;
}

BOOL CDlgLayoutObjectControl::IsOpenDlgObjectContent()
{
	if ( ! IsEditMode())
		return FALSE;

	if (m_dlgCustomMarkContent.m_hWnd)
		if (m_dlgCustomMarkContent.IsWindowVisible())
			return TRUE;
	if (m_dlgTextMarkContent.m_hWnd)
		if (m_dlgTextMarkContent.IsWindowVisible())
			return TRUE;
	if (m_dlgSectionMarkContent.m_hWnd)
		if (m_dlgSectionMarkContent.IsWindowVisible())
			return TRUE;
	if (m_dlgCropMarkContent.m_hWnd)
		if (m_dlgCropMarkContent.IsWindowVisible())
			return TRUE;
	if (m_dlgFoldMarkContent.m_hWnd)
		if (m_dlgFoldMarkContent.IsWindowVisible())
			return TRUE;
	if (m_dlgBarcodeMarkContent.m_hWnd)
		if (m_dlgBarcodeMarkContent.IsWindowVisible())
			return TRUE;

	if (m_dlgDrawingMarkContent.m_hWnd)
		if (m_dlgDrawingMarkContent.IsWindowVisible())
			return TRUE;

	if (m_dlgPageObjectContent.m_hWnd)
		if (m_dlgPageObjectContent.IsWindowVisible())
			return TRUE;

	return FALSE;
}

BOOL CDlgLayoutObjectControl::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		return CDialog::PreTranslateMessage(pMsg);
	else
		return CDialog::PreTranslateMessage(pMsg);
}

void CDlgLayoutObjectControl::ObjectPreviewWindow_LoadData()
{
	CPrintSheetObjectPreview* pView = (CPrintSheetObjectPreview*)theApp.GetView(RUNTIME_CLASS(CPrintSheetObjectPreview));
	if (pView)
		pView->m_dlgObjectPreviewWindow.LoadData();
}
