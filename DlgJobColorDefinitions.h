// DlgJobColorDefinitions.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// CDlgJobColorDefinitions dialog

class CDlgJobColorDefinitions : public CDialog
{
// Construction
public:
	CDlgJobColorDefinitions(BOOL bOverwriteMode = FALSE, BOOL bHideCompositeButton = FALSE, CWnd* pParent = NULL);   // standard constructor

public:
	CColorDefinitionTable m_PSColorDefTable;
	BOOL				  m_bHideCompositeButton;

protected:
	CColorListBox	m_JobColorListBox;
	CColorListBox	m_PSColorListBox;
	int				m_nColorIndex;
	BOOL			m_bOverwriteMode;

// Dialog Data
	//{{AFX_DATA(CDlgJobColorDefinitions)
	enum { IDD = IDD_JOB_COLOR_DEFINITIONS };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgJobColorDefinitions)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:


protected:
	// Generated message map functions
	//{{AFX_MSG(CDlgJobColorDefinitions)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnRemoveColorButton();
	afx_msg void OnDblclkJobColors();
	afx_msg void OnCopyButton();
	afx_msg void OnSelchangeJobColors();
	afx_msg void OnNewColorButton();
	afx_msg void OnDblclkPagesourceColors();
	afx_msg void OnAssignCompositeButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
