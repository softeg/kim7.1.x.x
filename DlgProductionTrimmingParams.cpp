// DlgProductionTrimmingParams.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionTrimmingParams.h"
#include "DlgProductionManager.h"
#include "GraphicComponent.h"
#include "PrintComponentsView.h"
#include "InplaceEdit.h"
#include "LayoutDataWindow.h"



// CPTPCustomPaintFrame

IMPLEMENT_DYNAMIC(CPTPCustomPaintFrame, CStatic)

CPTPCustomPaintFrame::CPTPCustomPaintFrame()
{
}

CPTPCustomPaintFrame::~CPTPCustomPaintFrame()
{
}


BEGIN_MESSAGE_MAP(CPTPCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CPTPCustomPaintFrame-Meldungshandler

void CPTPCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CString strText;
	CRect rcFrame;
	GetClientRect(rcFrame);

	switch (GetDlgCtrlID())
	{
	case IDC_PP_PAGES_PREVIEWFRAME:		DrawPages(&dc,	rcFrame);	break;
	default:														break;
	}
}


void CPTPCustomPaintFrame::DrawPages(CDC* pDC, CRect frameRect)
{
	CDlgProductionTrimmingParams* pDlg = (CDlgProductionTrimmingParams*)GetParent();
	if ( ! pDlg)
		return;

	CProductionProfile& rProductionProfile = pDlg->GetProductionProfile();
	CFoldingProcess&	rFoldingProcess	   = pDlg->GetFoldingProcess();
	CBoundProductPart&	rProductPart	   = pDlg->GetBoundProductPart();
	int					nProductPartIndex  = rProductPart.GetIndex();

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBrush brush(RGB(127,162,207));
	CPen   pen(PS_SOLID, 1, LIGHTGRAY);
	CBrush* pOldBrush = pDC->SelectObject(&brush);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);

	pDC->SaveDC();	// save current clipping region
	CRect rcClip;
	pDlg->GetDlgItem(IDC_PD_PAGESPREVIEW_SPIN)->GetClientRect(rcClip);
	rcClip.OffsetRect(1, 1);
	pDC->ExcludeClipRect(rcClip);
	pDC->Rectangle(frameRect);
	pDC->RestoreDC(-1);;

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	brush.DeleteObject();
	pen.DeleteObject();

	if (rFoldingProcess.m_bShinglingActive)
	{
		CRect rcText = frameRect; 
		rcText.left += 2; rcText.bottom -= 2;
		CFont font;
		font.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		CFont* pOldFont = pDC->SelectObject(&font);
		pDC->SetTextColor(BROWN);
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(255,230,180));
		CString string; string.LoadString(IDS_SHINGLING_ACTIVE);
		pDC->DrawText(string, rcText, DT_LEFT | DT_BOTTOM | DT_SINGLELINE);
		pDC->SelectObject(pOldFont);
		font.DeleteObject();
	}

	frameRect.DeflateRect(15, 15);

	if (nProductPartIndex >= 0)
	{
		if (pDlg->m_nPagePreviewIndex < 0)
			pDlg->m_nPagePreviewIndex = 0;
		else
			if (pDlg->m_nPagePreviewIndex >= rProductPart.m_nNumPages)
				pDlg->m_nPagePreviewIndex = rProductPart.m_nNumPages - 1;

		frameRect.DeflateRect(5,10); frameRect.left += 10; frameRect.top += 5;
		pDoc->m_PageTemplateList.Draw(pDC, frameRect, pDlg->m_nPagePreviewIndex, nProductPartIndex, TRUE, FALSE, TRUE, TRUE, NULL, FALSE, FALSE, 
									  0.0f, 0.0f, 0.0f, pDlg->m_fHeadTrim, pDlg->m_fFootTrim, pDlg->m_fSideTrim);
	}
}



// CDlgProductionTrimmingParams-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionTrimmingParams, CDialog)

CDlgProductionTrimmingParams::CDlgProductionTrimmingParams(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionTrimmingParams::IDD, pParent)
{
	m_fHeadTrim		 	= 0.0f;
	m_fFootTrim		 	= 0.0f;
	m_fSideTrim		 	= 0.0f;

	m_nPagePreviewIndex = 0;
	m_bLocked = FALSE;
	m_ptInitialPos = CPoint(0,0);
}

CDlgProductionTrimmingParams::~CDlgProductionTrimmingParams()
{
}

int CDlgProductionTrimmingParams::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		SetParent(m_pParentWnd);
		ModifyStyle(WS_OVERLAPPEDWINDOW | WS_POPUPWINDOW, WS_CHILD);
		ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);
	}

	return 0;
}

BOOL CDlgProductionTrimmingParams::Create(UINT nIDTemplate, CWnd* pParentWnd)
{
	m_pParentWnd = pParentWnd;
	return CDialog::Create(nIDTemplate, pParentWnd);
}

BOOL CDlgProductionTrimmingParams::OnEraseBkgnd(CDC* pDC)
{
	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		return CDialog::OnEraseBkgnd(pDC);

	CRect rcFrame;
	GetClientRect(rcFrame);

	CGraphicComponent gc;
	gc.DrawPopupWindowBackground(pDC, rcFrame);

	return TRUE;
}

void CDlgProductionTrimmingParams::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
		{
			if (m_hWnd)
			{
				if ( ! m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
				{
					m_bLocked = TRUE;
					EndModalLoop(IDCANCEL);
					DestroyWindow();
				}
			}
		}
	}
	else
		m_bLocked = FALSE;
}

void CDlgProductionTrimmingParams::OnBnClickedOk()
{
	SaveData();
	EndModalLoop(IDOK);
	m_bLocked = TRUE;
	DestroyWindow();
}

CProductionProfile& CDlgProductionTrimmingParams::GetProductionProfile()
{
	static CProductionProfile nullProductionProfile;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile;
	}
	//else
	//	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CProfileSummaryWindow)))
	//	{
	//		CProfileSummaryWindow*	pParent	= (CProfileSummaryWindow*)m_pParentWnd;
	//		return pParent->GetProductionProfile();
	//	}

	return nullProductionProfile;
}

CTrimmingProcess& CDlgProductionTrimmingParams::GetTrimmingProcess()
{
	static CTrimmingProcess nullTrimmingProcess;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile.m_postpress.m_trimming;
	}
	else
		if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
		{
			return GetBoundProductPart().m_trimmingParams;
		}

	return nullTrimmingProcess;
}

CFoldingProcess& CDlgProductionTrimmingParams::GetFoldingProcess()
{
	static CFoldingProcess nullFoldingProcess;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile.m_postpress.m_folding;
	}
	else
		if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
		{
			return GetBoundProductPart().m_foldingParams;
		}

	return nullFoldingProcess;
}

CBoundProductPart& CDlgProductionTrimmingParams::GetBoundProductPart()
{
	static CBoundProductPart nullProductPart;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent				= (CDlgProductionManager*)m_pParentWnd;
		CProductionProfile&		rProductionProfile	= pParent->m_productionProfile;
		CPrintGroup*			pPrintGroup			= pParent->m_pPrintGroup;
		int						nComponentIndex		= pParent->m_nComponentIndex;
		CPrintGroupComponent*	pComponent			= (pPrintGroup) ? ( (nComponentIndex == -1) ? pPrintGroup->GetFirstBoundComponent() : &pPrintGroup->m_componentList[nComponentIndex]) : NULL;
		int						nProductPartIndex	= (pComponent) ? pComponent->m_nProductClassIndex : -1;
		return (CImpManDoc::GetDoc()) ? CImpManDoc::GetDoc()->GetBoundProductPart(nProductPartIndex) : nullProductPart;
	}
	else
		if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
		{
			CPrintComponentsView* pView		   = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
			CPrintComponent		  actComponent = pView->GetActComponent();
			CBoundProductPart*	  pProductPart = actComponent.GetBoundProductPart();
			return (pProductPart) ? *pProductPart : nullProductPart;
		}

	return nullProductPart;
}

void CDlgProductionTrimmingParams::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PD_SIDETRIM_SPIN, m_sideTrimSpin);
	DDX_Control(pDX, IDC_PD_HEADTRIM_SPIN, m_headTrimSpin);
	DDX_Control(pDX, IDC_PD_FOOTTRIM_SPIN, m_footTrimSpin);
	DDX_Measure(pDX, IDC_PD_FOOTTRIM_EDIT, m_fFootTrim);
	DDX_Measure(pDX, IDC_PD_HEADTRIM_EDIT, m_fHeadTrim);
	DDX_Measure(pDX, IDC_PD_SIDETRIM_EDIT, m_fSideTrim);
}


BEGIN_MESSAGE_MAP(CDlgProductionTrimmingParams, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_PAGESPREVIEW_SPIN, OnDeltaposPdPagespreviewSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_FOOTTRIM_SPIN, OnDeltaposFoottrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_HEADTRIM_SPIN, OnDeltaposHeadtrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_SIDETRIM_SPIN, OnDeltaposSidetrimSpin)
	ON_EN_KILLFOCUS(IDC_PD_FOOTTRIM_EDIT, OnKillfocusFoottrimEdit)
	ON_EN_KILLFOCUS(IDC_PD_HEADTRIM_EDIT, OnKillfocusHeadtrimEdit)
	ON_EN_KILLFOCUS(IDC_PD_SIDETRIM_EDIT, OnKillfocusSidetrimEdit)
END_MESSAGE_MAP()


// CDlgProductionTrimmingParams-Meldungshandler

BOOL CDlgProductionTrimmingParams::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_ptInitialPos != CPoint(0,0))
	{
		CSize szScreen = CSize(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
		CRect rcDlg; GetWindowRect(rcDlg);
		if ( (m_ptInitialPos.y + rcDlg.Height()) <= szScreen.cy)
			SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		else
			SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y - rcDlg.Height() - 12, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
	}

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);

	m_pagesPreviewFrame.SubclassDlgItem	(IDC_PP_PAGES_PREVIEWFRAME,	this);

	m_headTrimSpin.SetRange(0, 1);  
	m_sideTrimSpin.SetRange(0, 1);  
	m_footTrimSpin.SetRange(0, 1);  

	((CSpinButtonCtrl*)GetDlgItem(IDC_PD_PAGESPREVIEW_SPIN))->SetRange(0, 1);

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProductionTrimmingParams::InitData()
{
	CTrimmingProcess& rTrimmingProcess = GetTrimmingProcess();
	m_fHeadTrim = rTrimmingProcess.m_fHeadTrim;
	m_fFootTrim = rTrimmingProcess.m_fFootTrim;
	m_fSideTrim = rTrimmingProcess.m_fSideTrim;

	UpdateData(FALSE);
}

void CDlgProductionTrimmingParams::SaveData()
{
	UpdateData(TRUE);

	CTrimmingProcess& rTrimmingProcess = GetTrimmingProcess();
	rTrimmingProcess.m_fHeadTrim = m_fHeadTrim;
	rTrimmingProcess.m_fFootTrim = m_fFootTrim;
	rTrimmingProcess.m_fSideTrim = m_fSideTrim;
}

void CDlgProductionTrimmingParams::OnDeltaposHeadtrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_headTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusHeadtrimEdit();
	*pResult = 0;
}

void CDlgProductionTrimmingParams::OnDeltaposFoottrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_footTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusFoottrimEdit();
	*pResult = 0;
}

void CDlgProductionTrimmingParams::OnDeltaposSidetrimSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_sideTrimSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	OnKillfocusSidetrimEdit();
	*pResult = 0;
}

void CDlgProductionTrimmingParams::OnKillfocusHeadtrimEdit() 
{
	UpdateData(TRUE);
	UpdatePreview();
}

void CDlgProductionTrimmingParams::OnKillfocusFoottrimEdit() 
{
	UpdateData(TRUE);
	UpdatePreview();
}

void CDlgProductionTrimmingParams::OnKillfocusSidetrimEdit() 
{
	UpdateData(TRUE);
	UpdatePreview();
}

void CDlgProductionTrimmingParams::UpdatePreview()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CTrimmingProcess& rTrimmingProcess = GetProductionProfile().m_postpress.m_trimming;
	rTrimmingProcess.m_fHeadTrim = m_fHeadTrim;
	rTrimmingProcess.m_fFootTrim = m_fFootTrim;
	rTrimmingProcess.m_fSideTrim = m_fSideTrim;

	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}

void CDlgProductionTrimmingParams::OnDeltaposPdPagespreviewSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: F�gen Sie hier Ihren Kontrollbehandlungscode f�r die Benachrichtigung ein.
	*pResult = 0;

	m_nPagePreviewIndex += (pNMUpDown->iDelta > 0) ? 2 : -2;

	if (m_nPagePreviewIndex < 0)
		m_nPagePreviewIndex = 0;
	else
	{
		CBoundProductPart& rProductPart	= GetBoundProductPart();
		if ( ! rProductPart.IsNull())
		{
			if (m_nPagePreviewIndex >= rProductPart.m_nNumPages)
				m_nPagePreviewIndex = rProductPart.m_nNumPages - 1;
		}
	}

	if (m_nPagePreviewIndex > 0)
		if ( ! (m_nPagePreviewIndex%2) )	// even
			m_nPagePreviewIndex--;			// always set to odd index -> left page

	UpdateData(FALSE);

	m_pagesPreviewFrame.Invalidate();
	m_pagesPreviewFrame.UpdateWindow();
}
