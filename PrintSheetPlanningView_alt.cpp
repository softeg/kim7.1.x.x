// PrintSheetPlanningView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PrintSheetPlanningView.h"
#include "PrintComponentsView.h"
#include "ProdDataGraphicOverView.h"
#include "GraphicComponent.h"
#include "DlgJobData.h"
#include "DlgPressDevice.h"
#include "NewPrintSheetFrame.h"
#include "MainFrm.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "PageListFrame.h"
#include "PageListView.h"
#include "PrintSheetPlanningToolsView.h"


// CPrintSheetPlanningView

IMPLEMENT_DYNCREATE(CPrintSheetPlanningView, CScrollView)

CPrintSheetPlanningView::CPrintSheetPlanningView()
{
	m_ptDragIcon = CPoint(0, 0);
	m_hDragIcon	 = theApp.LoadIcon(IDI_DRAG_FOLDSHEET);
    m_DisplayList.Create(this, TRUE);//, DISPLAY_LIST_TYPE(CPrintSheetView), &WorldToLP); // Ole Drag/Drop
	m_docSize = CSize(0,0);
	m_nLastSelectedFoldSheetComponentIndex = -1;
}

CPrintSheetPlanningView::~CPrintSheetPlanningView()
{
}

COrderDataFrame* CPrintSheetPlanningView::GetOrderDataFrame()
{
	return (COrderDataFrame*)GetParentFrame();
}

BEGIN_MESSAGE_MAP(CPrintSheetPlanningView, CScrollView)
    ON_WM_ERASEBKGND()
	ON_WM_CREATE()
    ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_COMMAND(ID_MAGNIFY_SHEET, OnMagnifySheet)
	ON_COMMAND(ID_GOTO_DETAILS, OnGotoDetails)
	ON_WM_NCMOUSEMOVE()
	ON_WM_NCCALCSIZE()
	ON_WM_NCPAINT()
	ON_WM_MOUSEWHEEL()
	ON_WM_SIZE()
	ON_WM_NCCALCSIZE()
END_MESSAGE_MAP()



BOOL CPrintSheetPlanningView::OnEraseBkgnd(CDC* pDC) 
{
    CRect rect;
    GetClientRect(rect);
    CBrush brush(RGB(230,233,238));//RGB(238,243,251));//RGB(252,252,250));
    pDC->FillRect(&rect, &brush);

    brush.DeleteObject();

	return TRUE;

//    return CScrollView::OnEraseBkgnd(pDC);
}


// CPrintSheetPlanningView-Zeichnung

void CPrintSheetPlanningView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	SetScrollSizes(MM_TEXT, CalcDocSize());
	Invalidate();
}

void CPrintSheetPlanningView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	SetScrollSizes(MM_TEXT, CalcDocSize());

	//if ( ! m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent)))
	//{	// if no sheet selected at this stage -> select first one
	//	CDisplayItem* pDI = m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	//	if (pDI)
	//	{
	//		pDI->m_nState = CDisplayItem::Selected;
	//		m_DisplayList.InvalidateItem(pDI);
	//	}
	//}

	Invalidate();
}

void CPrintSheetPlanningView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);
}

CSize CPrintSheetPlanningView::CalcDocSize()
{
	m_docSize = CSize(0,0);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return m_docSize;

	CImage img;
	img.LoadFromResource(theApp.m_hInstance, IDB_PRESSMACHINE);

	CSize sizeFrame(150, 150);
	CRect rcFrame(CPoint(10, 20), sizeFrame);

	CLayout* pLayout = pDoc->m_PrintSheetList.GetFirstLayout();
	while (pLayout)
	{
		sizeFrame = CSize(150, 150);
		CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
		while (pPrintSheet)
		{
			if (pPrintSheet->m_bDisplayOpen)
			{
				sizeFrame = CSize(500,400);
				break;
			}
			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}

		rcFrame.right  = (pLayout->m_nProductType == WORK_AND_BACK) ? (rcFrame.left + 2 * sizeFrame.cx) : (rcFrame.left + sizeFrame.cx);
		rcFrame.bottom = rcFrame.top  + sizeFrame.cy;

		CRect rcTitle = rcFrame; rcTitle.left += 5; rcTitle.top += 5; rcTitle.bottom = rcTitle.top + 15; rcTitle.right = rcTitle.left + img.GetWidth() + 30;

		rcTitle.OffsetRect(0, 15);
		rcTitle.OffsetRect(0, 15);
		rcFrame.top = rcTitle.bottom;

		int nIndex = 0;
		pPrintSheet = pLayout->GetFirstPrintSheet();
		int nSizeXMax = 0;
		while (pPrintSheet)
		{
			sizeFrame = (pPrintSheet->m_bDisplayOpen) ? CSize(500,400) : CSize(150,150);
			nSizeXMax = max(nSizeXMax, (pLayout->m_nProductType == WORK_AND_BACK) ? 2 * sizeFrame.cx : sizeFrame.cx);

			rcFrame.right  = (pLayout->m_nProductType == WORK_AND_BACK) ? (rcFrame.left + 2 * sizeFrame.cx) : (rcFrame.left + sizeFrame.cx);
			rcFrame.bottom = rcFrame.top  + sizeFrame.cy;

			CRect rcLayout = rcFrame; rcLayout.top += 15;
			rcTitle = rcLayout; rcTitle.top = rcTitle.bottom + 5; rcTitle.bottom = rcTitle.top + 15;

			m_docSize = CSize(max(m_docSize.cx, rcFrame.right), max(m_docSize.cy, rcFrame.bottom));
			rcFrame.OffsetRect(0, rcFrame.Height() + 5);
			nIndex++;

			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}

		rcFrame.OffsetRect(nSizeXMax + 40, 20 - rcFrame.top);
		pLayout = pDoc->m_PrintSheetList.GetNextLayout(pLayout);
	}

	img.Destroy();

	m_docSize += CSize(5, 5);

	return m_docSize;
}

int	CPrintSheetPlanningView::GetComponentIndex(CDisplayItem& rDI)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (&rDI == pDI)	
				return nIndex;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return -1;
}

int	CPrintSheetPlanningView::GetPrintSheetIndex(CDisplayItem& rDI)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, SheetFrame)))
		{
			if (&rDI == pDI)	
				return nIndex;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return -1;
}

int CPrintSheetPlanningView::GetSelectedComponentIndex()
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				return nIndex;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return -1;
}

BOOL CPrintSheetPlanningView::IsSelectedComponentIndex(int nComponentIndex)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				if (nComponentIndex == nIndex)
					return TRUE;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return FALSE;
}

void CPrintSheetPlanningView::SelectComponentByIndex(int nSelIndex)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if ( (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (nIndex == nSelIndex)
				pDI->m_nState = CDisplayItem::Selected;
			else
				pDI->m_nState = CDisplayItem::Normal;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
}

void CPrintSheetPlanningView::SelectComponentByFoldSheet(CFoldSheet* pFoldSheet, BOOL bScrollTo)
{
	CDisplayItem* pDIScrollTo = NULL;
	CDisplayItem* pDI		  = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent)))
		{
			if (pDI->m_pData == pFoldSheet)
			{
				pDI->m_nState = CDisplayItem::Selected;
				pDIScrollTo	  = pDI;
			}
			else
				pDI->m_nState = CDisplayItem::Normal;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}

	if (pDIScrollTo)	
		if ( ! pDIScrollTo->m_bIsVisible)	
		{
			Invalidate();
			OnHScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.left - 20, NULL);
			OnVScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.top  - 20, NULL);
		}
}

int CPrintSheetPlanningView::GetSelectedPrintSheetIndex()
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, SheetFrame)))
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				return nIndex;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return -1;
}

BOOL CPrintSheetPlanningView::IsSelectedPrintSheetIndex(int nPrintSheetIndex)
{
	int nIndex = 0;
	CDisplayItem* pDI = m_DisplayList.GetFirstItem();
	while (pDI)
	{
		if (pDI->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, SheetFrame)))
		{
			if (pDI->m_nState != CDisplayItem::Normal)	// selected or active
				if (nPrintSheetIndex == nIndex)
					return TRUE;
			nIndex++;
		}
		pDI = m_DisplayList.GetNextItem(pDI);
	}
	return FALSE;
}

void CPrintSheetPlanningView::OnDraw(CDC* pDC)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_DisplayList.BeginRegisterItems(pDC);

	CRect rcClientRect;
	GetClientRect(rcClientRect);

	CRect rcFrame = rcClientRect;
	rcFrame.left += 10; rcFrame.top += 10;
	CRect rcBox = pDoc->m_PrintSheetList.DrawPlanningList(pDC, rcFrame, &m_DisplayList);
	int nPrintSheetRight = (pDoc->m_productPartList.m_printSheetInfos.GetSize()) ? pDoc->m_productPartList.m_printSheetInfos[0].m_rcDisplay.right : 360; 
	
	if (pDoc->m_productPartList.HasBoundedProductPart())
		rcFrame.top = rcBox.bottom + 40; 
	rcFrame.right = 360;//(pDoc->m_productPartList.m_printSheetInfos.GetSize()) ? pDoc->m_productPartList.m_printSheetInfos[0].m_rcDisplay.left - 25 : 500; 
	rcBox = pDoc->m_productPartList.DrawLabelComponentList(pDC, rcFrame, &m_DisplayList);
	nPrintSheetRight = max(nPrintSheetRight, rcBox.right);

	m_DisplayList.EndRegisterItems(pDC);

	SetScrollSizes(MM_TEXT, CSize(rcBox.right + 10, rcBox.bottom + 10));
}

// CPrintSheetPlanningView-Diagnose

#ifdef _DEBUG
void CPrintSheetPlanningView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CPrintSheetPlanningView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPrintSheetPlanningView-Meldungshandler

int CPrintSheetPlanningView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	if( m_dropTarget.Register( this ) )
		return 0;
	else
		return -1;

	return 0;
}

void CPrintSheetPlanningView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
	pDC->SetMapMode(MM_TEXT); 

	CScrollView::OnPrepareDC(pDC, pInfo);
}

///////////////////////////////////////////////////////////////////
// mouse events

void CPrintSheetPlanningView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if (m_DisplayList.OnLButtonDown(point))
	{
		//CImpManDoc* pDoc = CImpManDoc::GetDoc();
		//if (pDoc)
		//{
		//	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
		//	while (pos)
		//	{
		//		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		//		rPrintSheet.m_bDisplayOpen = FALSE;
		//	}
		//}
		//OnUpdate(NULL, 0, NULL);
		//Invalidate();
	}

    CScrollView::OnLButtonDown(nFlags, point);
}

void CPrintSheetPlanningView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	//GetParentFrame()->SetActiveView(this);

	CScrollView::OnMouseMove(nFlags, point);
}

LRESULT CPrintSheetPlanningView::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	m_DisplayList.OnMouseLeave();
	return TRUE;
}

BOOL CPrintSheetPlanningView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// The default call of OnMouseWheel causes an assertion
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion

//	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}

int CPrintSheetPlanningView::GetSelProductPartIndex()
{
	CFoldSheet* pFoldSheet = GetSelFoldSheet();
	return (pFoldSheet) ? pFoldSheet->m_nProductPartIndex : 0;
}

CFoldSheet* CPrintSheetPlanningView::GetSelFoldSheet()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if ( ! pDI)
	{
		pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages));
		if (pDI)
		{
			CFoldSheet*	pDummyFoldSheet = new CFoldSheet;
			pDummyFoldSheet->m_nProductPartIndex = (int)pDI->m_pData;
			return pDummyFoldSheet;
		}
		else
			return NULL;
	}
	else
		return (CFoldSheet*)pDI->m_pData;
}

int	CPrintSheetPlanningView::GetSelLayerIndex()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if ( ! pDI)
		return 0;
	return (int)pDI->m_pAuxData;
}

CLabelListEntry* CPrintSheetPlanningView::GetSelLabelListEntry()
{
	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent));
	if ( ! pDI)
		return NULL;
	return (CLabelListEntry*)pDI->m_pData;
}

CPrintSheet* CPrintSheetPlanningView::GetSelPrintSheet()
{
	CFoldSheet* pFoldSheet  = GetFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			return (pDoc) ? pDoc->m_PrintSheetList.GetBlankSheet(pFoldSheet->m_nProductPartIndex) : NULL;
		}
		else
			return pFoldSheet->GetPrintSheet(GetLayerIndex());
	else
		return NULL;
}

CLayout* CPrintSheetPlanningView::GetSelLayout()
{
	CFoldSheet* pFoldSheet  = GetFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
		{
			CPrintSheet* pPrintSheet = GetPrintSheet();
			return (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
		}

	if (pFoldSheet)
		return pFoldSheet->GetLayout(GetLayerIndex());
	else
		return NULL;
}

CPressDevice* CPrintSheetPlanningView::GetSelPressDevice()
{
	CFoldSheet* pFoldSheet  = GetFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
		{
			CLayout* pLayout = GetSelLayout();
			return (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
		}

	if (pFoldSheet)
	{
		CLayout* pLayout = pFoldSheet->GetLayout(GetLayerIndex());
		return (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	}
	else
		return NULL;
}

void CPrintSheetPlanningView::OnSelectFoldSheetComponent(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	m_nLastSelectedFoldSheetComponentIndex = -1;
	int nIndex = 0;
	CDisplayItem* pDITmp = m_DisplayList.GetFirstItem();
	while (pDITmp)
	{
		if ( (pDITmp->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDITmp->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (pDI == pDITmp)
			{
				m_nLastSelectedFoldSheetComponentIndex = nIndex;
				break;
			}
			nIndex++;
		}
		pDITmp = m_DisplayList.GetNextItem(pDITmp);
	}
	
	// force OnNcCalcSize
    SetWindowPos(NULL,0,0,0,0,SWP_FRAMECHANGED|SWP_NOMOVE|SWP_NOSIZE|SWP_NOZORDER|SWP_NOACTIVATE);   

	HandleOnSelectComponent();
}

void CPrintSheetPlanningView::OnDeselectFoldSheetComponent(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	m_nLastSelectedFoldSheetComponentIndex = -1;
	// force OnNcCalcSize
    SetWindowPos(NULL,0,0,0,0,SWP_FRAMECHANGED|SWP_NOMOVE|SWP_NOSIZE|SWP_NOZORDER|SWP_NOACTIVATE);   
	HandleOnSelectComponent();
}

void CPrintSheetPlanningView::OnMouseMoveFoldSheetComponent(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width()-1, pDI->m_BoundingRect.Height()-1);
				COLORREF crHighlightColor = RGB(180,200,220);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}
}

void CPrintSheetPlanningView::OnSelectUnassignedPages(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	m_nLastSelectedFoldSheetComponentIndex = -1;
	int nIndex = 0;
	CDisplayItem* pDITmp = m_DisplayList.GetFirstItem();
	while (pDITmp)
	{
		if ( (pDITmp->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent))) || (pDITmp->IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, UnassignedPages))) )
		{
			if (pDI == pDITmp)
			{
				m_nLastSelectedFoldSheetComponentIndex = nIndex;
				break;
			}
			nIndex++;
		}
		pDITmp = m_DisplayList.GetNextItem(pDITmp);
	}

	CPrintSheetPlanningToolsView* pView = (CPrintSheetPlanningToolsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningToolsView));
	if (pView)
		pView->SendMessage(WM_COMMAND, IDC_IA_FOLDSCHEME_BUTTON);
}

void CPrintSheetPlanningView::OnActivateUnassignedPages(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheetPlanningToolsView* pView = (CPrintSheetPlanningToolsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningToolsView));
	if (pView)
		pView->SendMessage(WM_COMMAND, IDC_IA_FOLDSCHEME_BUTTON);
	pDI->m_nState = CDisplayItem::Selected;
}

void CPrintSheetPlanningView::OnDeselectUnassignedPages(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	m_nLastSelectedFoldSheetComponentIndex = -1;
}

void CPrintSheetPlanningView::OnMouseMoveUnassignedPages(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width()-1, pDI->m_BoundingRect.Height()-1);
				COLORREF crHighlightColor = RGB(180,200,220);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}
}

void CPrintSheetPlanningView::OnSelectLabelComponent(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int			 nLabelRefIndex = pDoc->m_labelList.FindLabelIndex((CLabelListEntry*)pDI->m_pData);
	CPrintSheet* pPrintSheet	= (CPrintSheet*)pDI->m_pAuxData;
	CLayout*	 pLayout		= (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout || ! pPrintSheet)
		return;

	POSITION pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
		if ( ! rObject.IsLabel())
			continue;
		if (rObject.m_nLabelRefIndex == nLabelRefIndex)
		{
			CDisplayItem* pDISelect = m_DisplayList.GetItemFromData((void*)&rObject, (void*)pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutObject));
			if (pDISelect)
			{
				pDISelect->m_nState = CDisplayItem::Selected;
				m_DisplayList.InvalidateItem(pDISelect);
			}
		}
	}


	// force OnNcCalcSize
    SetWindowPos(NULL,0,0,0,0,SWP_FRAMECHANGED|SWP_NOMOVE|SWP_NOSIZE|SWP_NOZORDER|SWP_NOACTIVATE);   

	Invalidate();
	UpdateWindow();
}

void CPrintSheetPlanningView::OnDeselectLabelComponent(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	// force OnNcCalcSize
    SetWindowPos(NULL,0,0,0,0,SWP_FRAMECHANGED|SWP_NOMOVE|SWP_NOSIZE|SWP_NOZORDER|SWP_NOACTIVATE);   

	Invalidate();
	UpdateWindow();
}

void CPrintSheetPlanningView::OnMouseMoveLabelComponent(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width()-1, pDI->m_BoundingRect.Height()-1);
				COLORREF crHighlightColor = RGB(180,200,220);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}
}

void CPrintSheetPlanningView::OnSelectUnassignedLabel(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETLIST);		
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_ADD_LABELS);		

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		CNewPrintSheetFrame* pFrame = (CNewPrintSheetFrame*)pView->GetParentFrame();
		if (pFrame)
		{
			CImpManDoc*	pDoc = CImpManDoc::GetDoc();
			int	nLabelIndex = (pDoc) ? pDoc->m_labelList.FindLabelIndex((CLabelListEntry*)pDI->m_pData) : 0;
			pFrame->m_dlgAddLabels.m_labelListBox.SetCurSel(nLabelIndex);
		}
	}
}

void CPrintSheetPlanningView::OnActivateUnassignedLabel(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	pDI->m_nState = CDisplayItem::Selected;
}

void CPrintSheetPlanningView::OnDeselectUnassignedLabel(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
}

void CPrintSheetPlanningView::OnMouseMoveUnassignedLabel(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width()-1, pDI->m_BoundingRect.Height()-1);
				COLORREF crHighlightColor = RGB(180,200,220);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}
}

void CPrintSheetPlanningView::OnSelectSheetFrame(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	HandleOnSelectPrintSheet();
}

void CPrintSheetPlanningView::OnActivateSheetFrame(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	pDI->m_nState = CDisplayItem::Selected;
}

void CPrintSheetPlanningView::OnDeselectSheetFrame(CDisplayItem* pDI, CPoint point, LPARAM)	// object(s) has been selected
{
	HandleOnSelectPrintSheet();
}
//
//void CPrintSheetPlanningView::OnMouseMoveSheetFrame(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
//{
//	switch (lParam)
//	{
//	case 1:	// mouse enter
//			{
//				CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
//				if (pPrintSheet)
//					if (pPrintSheet->m_bDisplayOpen)
//						break;
//
//				//CRect rcButton(pDI->m_BoundingRect.left + 5, pDI->m_BoundingRect.top + 10, pDI->m_BoundingRect.left + 145, pDI->m_BoundingRect.top + 30);
//				//rcButton.left = rcButton.right - 20;
//				//if ( ! m_magnifyButton.m_hWnd)
//				//{
//				//	m_magnifyButton.Create(_T(""), WS_CHILD | BS_OWNERDRAW, rcButton, this, ID_MAGNIFY_SHEET);
//				//	m_magnifyButton.SetBitmap(IDB_MAGNIFY, IDB_MAGNIFY, -1, XCENTER);
//				//	m_magnifyButton.SetTransparent(TRUE);
//				//	m_magnifyButton.m_lParam = (LPARAM)pDI->m_pData;
//				//}
//
//				//if ( ! m_gotoDetailsButton.m_hWnd)
//				//{
//				//	rcButton.OffsetRect(-rcButton.Width() - 5, 0);
//				//	m_gotoDetailsButton.Create(_T(""), WS_CHILD | BS_OWNERDRAW, rcButton, this, ID_GOTO_DETAILS);
//				//	m_gotoDetailsButton.SetBitmap(IDB_ARROW_RIGHT_HOT, IDB_ARROW_RIGHT_COLD, -1, XCENTER);
//				//	m_gotoDetailsButton.SetTransparent(TRUE);
//				//	m_gotoDetailsButton.m_lParam = (LPARAM)pDI->m_pData;
//				//}
//				//m_magnifyButton.ShowWindow(SW_SHOW);
//				//m_gotoDetailsButton.ShowWindow(SW_SHOW);
//			}
//			break;
//
//	case 2:	// mouse leave
//			//if (m_magnifyButton.m_hWnd)
//			//	m_magnifyButton.DestroyWindow();
//			//if (m_gotoDetailsButton.m_hWnd)
//			//	m_gotoDetailsButton.DestroyWindow();
//			break;
//
//	default:// mouse move
//			break;
//	}
//}

void CPrintSheetPlanningView::HandleOnSelectComponent()
{
	CPrintSheetPlanningToolsView* pView = (CPrintSheetPlanningToolsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningToolsView));
	if (pView)
		if (pView->m_dlgFoldSheetNumeration.m_hWnd)
		{
			pView->m_dlgFoldSheetNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
			pView->m_dlgFoldSheetNumeration.m_pfnInitCallback(&pView->m_dlgFoldSheetNumeration);
			m_DisplayList.DeselectAll(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
			m_DisplayList.InvalidateItem();
			UpdateWindow();
			return;
		}

	int			nLayerIndex = GetSelLayerIndex();
	CFoldSheet* pFoldSheet  = GetSelFoldSheet();
	if (pFoldSheet)
		if (pFoldSheet->IsEmpty())
			pFoldSheet = NULL;

	if (pFoldSheet)
	{
		CPrintSheetViewStrippingData* pPrintSheetViewStrippingData = (CPrintSheetViewStrippingData*)theApp.GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
		if (pPrintSheetViewStrippingData)
		{
			pPrintSheetViewStrippingData->OnUpdate(NULL, 0, NULL);
			pPrintSheetViewStrippingData->UpdateWindow();
		}

		CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pPrintSheetView)
		{
			pPrintSheetView->m_DisplayList.Invalidate();
			pPrintSheetView->Invalidate();
			pPrintSheetView->OnUpdate(NULL, 0, NULL);
			pPrintSheetView->UpdateWindow();
		}

		m_DisplayList.DeselectAll(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
		m_DisplayList.InvalidateItem();

		CDisplayItem*	pDIScrollTo = NULL;
		CPrintSheet*	pPrintSheet	= pFoldSheet->GetPrintSheet(nLayerIndex);
		while (pPrintSheet)
		{
			for (int nLayerRefIndex = 0; nLayerRefIndex < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); nLayerRefIndex++)
			{
				if (pPrintSheet->GetFoldSheet(nLayerRefIndex) == pFoldSheet)
				{
					CDisplayItem* pDISelect = m_DisplayList.GetItemFromData((void*)nLayerRefIndex, (void*)pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
					if (pDISelect)
					{
						pDISelect->m_nState = CDisplayItem::Selected;
						m_DisplayList.InvalidateItem(pDISelect);
						if ( ! pDIScrollTo)
							pDIScrollTo = pDISelect;
					}
				}
			}
			pPrintSheet	= pFoldSheet->GetNextPrintSheet(pPrintSheet, nLayerIndex);
		}
		if (pDIScrollTo)	
			if ( ! pDIScrollTo->m_bIsVisible)	
			{
				Invalidate();
				OnHScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.left - 20, NULL);
				OnVScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.top  - 20, NULL);
			}
	}

	m_DisplayList.Invalidate();
	Invalidate();
	UpdateWindow();

	if (pView)
		if (pView->m_dlgFoldSchemeSelector.m_hWnd)		// is open
		{
			pView->m_dlgFoldSchemeSelector.InitData();	
			pView->m_dlgFoldSchemeSelector.InitSchemeBrowser(pView->m_dlgFoldSchemeSelector.m_strCurrentSchemeFolder);
		}
}

void CPrintSheetPlanningView::HandleOnSelectPrintSheet()
{
	CPrintSheetPlanningToolsView* pView = (CPrintSheetPlanningToolsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningToolsView));
	if ( ! pView)
		return;
	if (pView->m_dlgFoldSheetNumeration.m_hWnd)
	{
		pView->m_dlgFoldSheetNumeration.m_nSelection = CDlgNumeration::SelectionMarked;
		pView->m_dlgFoldSheetNumeration.m_pfnInitCallback(&pView->m_dlgFoldSheetNumeration);
	}

	UpdateWindow();
}

void CPrintSheetPlanningView::OnSelectFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
//	GetOrderDataFrame()->GetPrintSheetPlanningToolsView()->UpdateDialogControls(GetOrderDataFrame()->GetPrintSheetPlanningToolsView(), TRUE);

	//if ( ! pDI)
	//	return;

	//int			 nLayerRefIndex = (int)pDI->m_pData;
	//CPrintSheet* pPrintSheet	= (CPrintSheet*)pDI->m_pAuxData;
	//if ( ! pPrintSheet)
	//	return;
	//CFoldSheet* pFoldSheet = pPrintSheet->GetFoldSheet(nLayerRefIndex); 
	//if ( ! pFoldSheet)
	//	return;

	//CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
	//if ( ! pPrintComponentsView)
	//	return;
	//pPrintComponentsView->m_DisplayList.DeselectAll();
	//pPrintComponentsView->m_DisplayList.InvalidateItem();
	//CDisplayItem* pDISelect = pPrintComponentsView->m_DisplayList.GetItemFromData((void*)pFoldSheet, (void*)NULL, DISPLAY_ITEM_TYPEID(CPrintComponentsView, FoldSheet));
	//if ( ! pDISelect)
	//	return;
	//pDISelect->m_nState = CDisplayItem::Selected;
	//pPrintComponentsView->m_DisplayList.InvalidateItem(pDISelect);
	//if ( ! pDISelect->m_bIsVisible)	
	//{
	//	pPrintComponentsView->Invalidate();
	//	pPrintComponentsView->OnHScroll(SB_THUMBTRACK, pDISelect->m_BoundingRect.left - 20, NULL);
	//	pPrintComponentsView->OnVScroll(SB_THUMBTRACK, pDISelect->m_BoundingRect.top  - 20, NULL);
	//}
	//pPrintComponentsView->UpdateWindow();
}

void CPrintSheetPlanningView::OnDeselectFoldSheet(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)		
{
//	GetOrderDataFrame()->GetPrintSheetPlanningToolsView()->UpdateDialogControls(GetOrderDataFrame()->GetPrintSheetPlanningToolsView(), TRUE);
}

void CPrintSheetPlanningView::OnActivateFoldSheet(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)	
{
//	GetOrderDataFrame()->GetPrintSheetPlanningToolsView()->UpdateDialogControls(GetOrderDataFrame()->GetPrintSheetPlanningToolsView(), TRUE);
}

void CPrintSheetPlanningView::OnDeactivateFoldSheet(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM)		
{
	//GetOrderDataFrame()->GetPrintSheetPlanningToolsView()->UpdateDialogControls(GetOrderDataFrame()->GetPrintSheetPlanningToolsView(), TRUE);
}
//
//void CPrintSheetPlanningView::OnMouseMoveFoldSheet(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
//{
//	switch (lParam)
//	{
//	case 1:	// mouse enter
//			{
//	 		//	CDC* pDC = GetDC(); 
//				//CRect trackerRect = pDI->m_BoundingRect;
//				//trackerRect.left   -= (trackerRect.left%2)   ? 1 : 0;
//				//trackerRect.top    -= (trackerRect.top%2)    ? 1 : 0;
//				//trackerRect.right  -= (trackerRect.right%2)  ? 1 : 1;
//				//trackerRect.bottom -= (trackerRect.bottom%2) ? 1 : 1;
//				//trackerRect.DeflateRect(2, 2);
//				//CRectTracker tracker(trackerRect, CRectTracker::dottedLine);
//				//tracker.Draw(pDC);
//				//ReleaseDC(pDC);
//				pDI->m_nLastPaintedState = -1;
//			}
//			break;
//
//	case 2:	// mouse leave
//			m_DisplayList.InvalidateItem(pDI);
//			break;
//
//	default:// mouse move
//			break;
//	}
//}

void CPrintSheetPlanningView::OnSelectPrintSheetDetails(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
	if ( ! pPrintSheet)
		return;

	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETLIST);
	pFrame->SelectPrintSheet(pPrintSheet);
}

void CPrintSheetPlanningView::OnMouseMovePrintSheetDetails(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width()-1, pDI->m_BoundingRect.Height()-1);
				COLORREF crHighlightColor = RGB(180,200,220);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				//CRect rcTriangle(pDI->m_BoundingRect.CenterPoint() - CSize(20,20), pDI->m_BoundingRect.CenterPoint() + CSize(20,20));
				//CGraphicComponent gp;
				//gp.DrawTransparentTriangle(pDC, CPoint(rcTriangle.left, rcTriangle.top), CPoint(rcTriangle.right, rcTriangle.CenterPoint().y), CPoint(rcTriangle.left, rcTriangle.bottom), DARKGRAY, TRUE);

				CImage img;
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GOTO_NEXT_HOT));
				CRect rcImg = pDI->m_BoundingRect; 
				img.TransparentBlt(pDC->m_hDC, rcImg.CenterPoint().x - img.GetWidth()/2, rcImg.CenterPoint().y - img.GetHeight()/2, img.GetWidth(), img.GetHeight(), WHITE);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}
}

void CPrintSheetPlanningView::OnSelectControlMarks(CDisplayItem* pDI, CPoint /*point*/, LPARAM)	
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)pDI->m_pData;
	if ( ! pPrintSheet)
		return;

	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETMARKS);
	pFrame->SelectPrintSheet(pPrintSheet);
}

void CPrintSheetPlanningView::OnMouseMoveControlMarks(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
				CDC* pDC = GetDC(); 
				Graphics graphics(pDC->m_hDC);
				Rect rc(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top, pDI->m_BoundingRect.Width()-1, pDI->m_BoundingRect.Height()-1);
				COLORREF crHighlightColor = RGB(180,200,220);
				SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
				graphics.FillRectangle(&br, rc);
				graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);

				CImage img;
				img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GOTO_NEXT_HOT));
				CRect rcImg = pDI->m_BoundingRect; 
				img.TransparentBlt(pDC->m_hDC, rcImg.CenterPoint().x - img.GetWidth()/2, rcImg.CenterPoint().y - img.GetHeight()/2, img.GetWidth(), img.GetHeight(), WHITE);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}
}

void CPrintSheetPlanningView::OnMagnifySheet()
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_magnifyButton.m_lParam;
	if ( ! pPrintSheet)
		return;
	pPrintSheet->m_bDisplayOpen = TRUE;
	OnUpdate(NULL, 0, NULL);
	Invalidate();
	if (m_magnifyButton.m_hWnd)
		m_magnifyButton.DestroyWindow();
	if (m_gotoDetailsButton.m_hWnd)
		m_gotoDetailsButton.DestroyWindow();
}

void CPrintSheetPlanningView::OnGotoDetails()
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_gotoDetailsButton.m_lParam;
	if ( ! pPrintSheet)
		return;

	CNewPrintSheetFrame* pFrame = CMainFrame::CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	theApp.m_pMainWnd->SendMessage(WM_COMMAND, ID_NAV_PRINTSHEETLIST);
	pFrame->SelectPrintSheet(pPrintSheet);

	if (m_magnifyButton.m_hWnd)
		m_magnifyButton.DestroyWindow();
	if (m_gotoDetailsButton.m_hWnd)
		m_gotoDetailsButton.DestroyWindow();
}

DROPEFFECT CPrintSheetPlanningView::OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet)))
	{
		int nRefIndex			 = (int)sourceDI.m_pData;
		CPrintSheet* pPrintSheet = (CPrintSheet*)sourceDI.m_pAuxData;
		CFoldSheet*  pFoldSheet  = pPrintSheet->GetFoldSheet(nRefIndex);
		m_strDragIcon.Format("%s [%s]", pFoldSheet->GetNumber(), pFoldSheet->m_strFoldSheetTypeName);

		CClientDC dc(this);
		dc.SelectStockObject(ANSI_VAR_FONT);
		dc.SetTextColor(BLACK);
		dc.SetBkColor(LIGHTGRAY);
		CSize textExtent = dc.GetTextExtent(m_strDragIcon);
		m_rectDragIcon.left   = min(point.x - 40, point.x - 24 - textExtent.cx/2 - 1);
		m_rectDragIcon.right  = max(point.x + 8,  point.x - 24 + textExtent.cx/2 + 1); 
		m_rectDragIcon.top	  = point.y - 16;
		m_rectDragIcon.bottom = point.y + 16 + 2 * textExtent.cy;

		m_ptDragIcon = CPoint(0,0);
		SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
		dc.DrawIcon(point - CSize(40,16), m_hDragIcon);
		dc.TextOut(point.x - 24 - textExtent.cx/2, point.y + 5 + textExtent.cy, m_strDragIcon);
		m_ptDragIcon = point;
	}

	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		return DROPEFFECT_COPY;
	else 
		return DROPEFFECT_MOVE;
}


DROPEFFECT CPrintSheetPlanningView::OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return DROPEFFECT_NONE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet)))
		return OnDragOverFoldSheet(sourceDI, dwKeyState, point);


	return DROPEFFECT_NONE;
}

void CPrintSheetPlanningView::OnDragLeave() 
{
	CClientDC  dc(this);

	RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	if (!m_catchRect.IsRectNull())
	{
		dc.InvertRect(m_catchRect);
		m_catchRect = CRect(0,0,0,0);
	}
}

BOOL CPrintSheetPlanningView::OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point) 
{
	// clean up focus rect
	m_DisplayList.RemoveDragFeedback();

	if (dropEffect == DROPEFFECT_NONE)
		return FALSE;

	if (!pDataObject->IsDataAvailable(CDisplayList::m_ClipboardFormat))
		return FALSE;

	CDisplayItem sourceDI;
	sourceDI.GetFromClipboard(pDataObject);

	if (sourceDI.IsItemOfType(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet)))
		return OnDropFoldSheet(sourceDI, dropEffect, point);

	return FALSE;
}

DROPEFFECT CPrintSheetPlanningView::OnDragOverFoldSheet(CDisplayItem& di, DWORD dwKeyState, CPoint point)
{
	DROPEFFECT de;
	if ((dwKeyState & MK_CONTROL) == MK_CONTROL)
		de = DROPEFFECT_COPY;
	else 
		de = DROPEFFECT_MOVE;

	CClientDC dc(this);
	BOOL bBitmapRestored = FALSE;
	if (point != m_ptDragIcon)
		RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	int			 nSide;
	CDisplayItem targetDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
	if (!targetDI.IsNull())
	{
		if ((di.m_pData == targetDI.m_pData) && (di.m_pAuxData == targetDI.m_pAuxData) && (de == DROPEFFECT_MOVE))	// drag and drop on same printsheet
			de = DROPEFFECT_COPY;																					// and with same foldsheet 
																													// -> only COPY makes sense
		CRect rect = targetDI.m_BoundingRect;
		switch (nSide)
		{
		case LEFT:		rect.left  -= 2; rect.right  = rect.left   + 4; break;
		case RIGHT:		rect.right += 2; rect.left   = rect.right  - 4; break;
		case BOTTOM:	rect.bottom-= 2; rect.top    = rect.bottom + 4; break;
		case TOP:		rect.top   += 2; rect.bottom = rect.top    - 4; break;
		}

		if (m_catchRect != rect)
		{
			if (point == m_ptDragIcon)	// bitmap needs to be restored even if point has not changed
			{
				RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);
				bBitmapRestored = TRUE;
			}
			dc.InvertRect(m_catchRect);
			dc.InvertRect(m_catchRect = rect);
		}										 
	}
	else
	{
		CDisplayItem closestDI, trimDI;
		CDisplayItem insideDI = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
		if (insideDI.IsNull())
		{
			//if (ViewMode() == ID_VIEW_ALL_SHEETS)
				closestDI = m_DisplayList.GetClosestItem(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Sheet));
		}
		else
		{
			trimDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutObject));
			if ( ! trimDI.IsNull())
			{
				CRect rect = trimDI.m_BoundingRect;
				switch (nSide)
				{
				case LEFT:		rect.left  -= 2; rect.right  = rect.left   + 4; rect.top  = insideDI.m_BoundingRect.top;  rect.bottom = insideDI.m_BoundingRect.bottom; break;
				case RIGHT:		rect.right += 2; rect.left   = rect.right  - 4; rect.top  = insideDI.m_BoundingRect.top;  rect.bottom = insideDI.m_BoundingRect.bottom; break;
				case BOTTOM:	rect.bottom-= 2; rect.top    = rect.bottom + 4; rect.left = insideDI.m_BoundingRect.left; rect.right  = insideDI.m_BoundingRect.right;  break;
				case TOP:		rect.top   += 2; rect.bottom = rect.top    - 4; rect.left = insideDI.m_BoundingRect.left; rect.right  = insideDI.m_BoundingRect.right;  break;
				}

				if (m_catchRect != rect)
				{
					if (point == m_ptDragIcon)	// bitmap needs to be restored even if point has not changed
					{
						RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);
						bBitmapRestored = TRUE;
					}
					dc.InvertRect(m_catchRect);
					dc.InvertRect(m_catchRect = rect);
				}										 
			}
		}

		if (trimDI.IsNull())
			if (!m_catchRect.IsRectNull())
			{
				dc.InvertRect(m_catchRect);
				m_catchRect = CRect(0,0,0,0);
			}

		if (closestDI.IsNull() && insideDI.IsNull())
			de = DROPEFFECT_NONE;
		
		if ( ! closestDI.IsNull())
			if (insideDI.IsNull())
				if ((nSide == LEFT) || (nSide == RIGHT))
					de = DROPEFFECT_NONE;

		if ( ! insideDI.IsNull())
			if (insideDI.IsEqual(&di) && (de == DROPEFFECT_MOVE))	// drag and drop on same printsheet -> only COPY makes sense
				de = DROPEFFECT_COPY;
	}

	if ((point != m_ptDragIcon) || bBitmapRestored)
	{
		m_rectDragIcon += point - m_ptDragIcon;
		SaveBitmap(&m_bmDragBackground, m_rectDragIcon);
		dc.DrawIcon(point - CSize(40,16), m_hDragIcon);
		dc.SelectStockObject(ANSI_VAR_FONT);
		dc.SetTextColor(BLACK);
		dc.SetBkColor(LIGHTGRAY);
		CSize textExtent = dc.GetTextExtent(m_strDragIcon);
		dc.TextOut(point.x - 24 - textExtent.cx/2, point.y + 5 + textExtent.cy, m_strDragIcon);
		m_ptDragIcon = point;
	}
 
	return de;
}

BOOL CPrintSheetPlanningView::OnDropFoldSheet(CDisplayItem& di, DROPEFFECT dropEffect, CPoint point)
{
	CClientDC dc(this);

	RestoreBitmap(&m_bmDragBackground, m_rectDragIcon);

	if (!m_catchRect.IsRectNull())
	{
		dc.InvertRect(m_catchRect);
		m_catchRect = CRect(0,0,0,0);
	}

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	BOOL		 bFurtherQuestions = TRUE;
	int			 nSide;
	CDisplayItem closestDI;
	CDisplayItem insideDI;
	CDisplayItem targetDI  = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));
	if (targetDI.IsNull())
		closestDI = m_DisplayList.GetClosestItem(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, Sheet));
	if (closestDI.IsNull())
		insideDI = m_DisplayList.GetItemPtInside(point, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheet));

	CLayoutObject* pSpreadObject = NULL;
	if ( targetDI.IsNull() && !insideDI.IsNull())
	{
		CDisplayItem trimDI = m_DisplayList.GetItemNearby(point, &nSide, DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LayoutObject));
		if ( ! trimDI.IsNull())
		{
			targetDI	  = insideDI;
			pSpreadObject = (CLayoutObject*)trimDI.m_pData;
		}
	}

	if ( ! targetDI.IsNull())
	{
		CPrintSheet* pSourcePrintSheet = (CPrintSheet*)di.m_pAuxData;
		CPrintSheet* pTargetPrintSheet = (CPrintSheet*)targetDI.m_pAuxData;
		BOOL		 bSourceInnerForm  = (di.m_AuxRect.Width()		  <= 2) ? TRUE : FALSE; // trick: use auxrect width to indicate if source is outer or inner form.
		BOOL		 bTargetBackSide   = (targetDI.m_AuxRect.Height() <= 2) ? TRUE : FALSE; // trick: use auxrect height to indicate if target is on layout backside
		BOOL		 bAutoAssign	   = FALSE;										  

		CFoldSheetDropTargetInfo dropTargetInfo(CFoldSheetDropTargetInfo::DropTargetLayoutObject, pTargetPrintSheet, NULL, NULL, (int)targetDI.m_pData, 0.0f, 0.0f, 0.0f, 0.0f, nSide, BOTTOM, FALSE);

		if (pSourcePrintSheet->GetLayout() == pTargetPrintSheet->GetLayout())
		{
			if (pSourcePrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 1)
			{
				int ret = IDNO;
				// If source and target layout are equal -> ask user if he wants
				// this operation be performed on all printsheets of this type   
				if (pSourcePrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() > 3)		// in order to perform this action to multiple sheets, we need min. 2 pairs of sheets
					ret = AfxMessageBox(IDS_MULTIPLE_PRINTSHEET_PERFORM, MB_YESNOCANCEL);
				switch (ret)
				{
				case IDYES	  :	bAutoAssign = TRUE;		
								if (pSourcePrintSheet != pTargetPrintSheet)	// If different printsheets and AutoAssign 
									dropEffect = DROPEFFECT_MOVE;				// -> only MOVE makes sense
								break;
				case IDNO	  : bFurtherQuestions = FALSE;
								break;
				case IDCANCEL : return FALSE;
				}
			}
		}

		if (dropEffect == DROPEFFECT_COPY)
		{
			//if (bFurtherQuestions)
			//	if (AfxMessageBox(IDS_FOLDSHEET_REALLY_COPY, MB_YESNO) == IDNO)
			//		return FALSE;

			UndoSave();
			BeginWaitCursor();	// copy/cut and paste could take a while, in case of many printsheets
			pTargetPrintSheet->CopyAndPasteFoldSheet(pSourcePrintSheet, (int)di.m_pData, bSourceInnerForm, dropTargetInfo, bTargetBackSide, bAutoAssign, pSpreadObject);
			if (bAutoAssign)
				pDoc->m_PrintSheetList.CopyAndPasteFoldSheet(pTargetPrintSheet, (int)di.m_pData);
			EndWaitCursor();
		}
		else
		{
			//if (bFurtherQuestions)
			//	if (AfxMessageBox(IDS_FOLDSHEET_REALLY_MOVE, MB_YESNO) == IDNO)
			//		return FALSE;

			BOOL bReverse = (pSourcePrintSheet->m_nPrintSheetNumber <				// Determine direction of foldsheet move
							 pTargetPrintSheet->m_nPrintSheetNumber) ? TRUE : FALSE;
			UndoSave();
			BeginWaitCursor();
			pTargetPrintSheet->CutAndPasteFoldSheet (pSourcePrintSheet, (int)di.m_pData, bSourceInnerForm, dropTargetInfo, bTargetBackSide, bAutoAssign, pSpreadObject);
			if (bAutoAssign)
				pDoc->m_PrintSheetList.CutAndPasteFoldSheet(pTargetPrintSheet, bReverse);
			EndWaitCursor();
		}

		pSourcePrintSheet->GetLayout()->CheckForProductType(); 
		pTargetPrintSheet->GetLayout()->CheckForProductType();
										   
		pDoc->SetModifiedFlag();										
	}
	else
	{
		if ( ! closestDI.IsNull() && (nSide != LEFT) && (nSide != RIGHT))
		{
			UndoSave();

			CPrintSheet* pSourcePrintSheet = (CPrintSheet*)di.m_pAuxData;
			CPrintSheet* pTargetPrintSheet = (CPrintSheet*)closestDI.m_pAuxData;
			BOOL		 bSourceInnerForm  = (di.m_AuxRect.Width()		  <= 2) ? TRUE : FALSE; // trick: use auxrect width to indicate if source is outer or inner form.
			BOOL		 bTargetBackSide   = (targetDI.m_AuxRect.Height() <= 2) ? TRUE : FALSE; // trick: use auxrect height to indicate if target is on layout backside
			CFoldSheet*  pSourceFoldSheet  = pSourcePrintSheet->GetFoldSheet((int)di.m_pData);
			CPrintSheet  newPrintSheet;																					
			CFoldSheetDropTargetInfo dropTargetInfo(CFoldSheetDropTargetInfo::DropTargetLayoutObject, pTargetPrintSheet, NULL, NULL, -1, 0.0f, 0.0f, 0.0f, 0.0f, -1, -1, FALSE);

			newPrintSheet.Create(CFoldSheetLayerRef(-1, -1), -1, (pSourceFoldSheet) ? pSourceFoldSheet->m_nProductPartIndex : 0);	
			if (nSide == BOTTOM)
				pTargetPrintSheet = pDoc->m_PrintSheetList.InsertBefore(pTargetPrintSheet, newPrintSheet);
			else
				pTargetPrintSheet = pDoc->m_PrintSheetList.InsertAfter(pTargetPrintSheet, newPrintSheet);

			if (dropEffect == DROPEFFECT_COPY)
			{
				//if (bFurtherQuestions)
				//	if (AfxMessageBox(IDS_FOLDSHEET_REALLY_COPY, MB_YESNO) == IDNO)
				//		return FALSE;

				BeginWaitCursor();
				pTargetPrintSheet->CopyAndPasteFoldSheet(pSourcePrintSheet, (int)di.m_pData, bSourceInnerForm, dropTargetInfo, bTargetBackSide, FALSE, NULL);
				EndWaitCursor();
			}
			else
			{
				if (bFurtherQuestions)
					if (AfxMessageBox(IDS_FOLDSHEET_REALLY_MOVE, MB_YESNO) == IDNO)
						return FALSE;

				BeginWaitCursor();
				pTargetPrintSheet->CutAndPasteFoldSheet (pSourcePrintSheet, (int)di.m_pData, bSourceInnerForm, dropTargetInfo, bTargetBackSide, FALSE, NULL);
				EndWaitCursor();
			}

			pSourcePrintSheet->GetLayout()->CheckForProductType();
			pTargetPrintSheet->GetLayout()->CheckForProductType();

			pDoc->SetModifiedFlag();										
		}
	}

	if (pDoc->IsModified())
	{
		pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),		NULL, 0, NULL);	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));	
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),				NULL, 0, NULL);	// because while drawing printsheets GetAct...() calls will be made
		theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetStrippingView),	NULL, 0, NULL);	
		Invalidate();
		UpdateWindow();

		pDoc->UpdateAllViews(NULL);
	}

	return TRUE;
}


///////////////////////////////////////////////////////////////////
// bitmap save/restore
void CPrintSheetPlanningView::SaveBitmap(CBitmap* pBitmap, const CRect& rect)
{
	if (!pBitmap)
		return;

	pBitmap->DeleteObject();

	CClientDC dc(this);
 	CDC		  memDC;
	memDC.CreateCompatibleDC(&dc);
	pBitmap->CreateCompatibleBitmap(&dc, rect.Width(), rect.Height());
	CBitmap* pOldBitmap = memDC.SelectObject(pBitmap);
	memDC.BitBlt(0, 0, rect.Width(), rect.Height(), &dc, rect.left, rect.top, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}

void CPrintSheetPlanningView::RestoreBitmap(CBitmap* pBitmap, const CRect& rect)
{
	if (!pBitmap)
		return;

	CClientDC dc(this);
	CDC		  memDC;
	memDC.CreateCompatibleDC(&dc);
	CBitmap* pOldBitmap = memDC.SelectObject(pBitmap);
	dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
}
///////////////////////////////////////////////////////////////////

void CPrintSheetPlanningView::UndoSave()
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	theApp.m_undoBuffer.SeekToBegin();
	CArchive archive(&theApp.m_undoBuffer, CArchive::store);

	pDoc->Serialize(archive);

	archive.Close();

	//GetOrderDataFrame()->m_bUndoActive = TRUE;
	//GetOrderDataFrame()->GetPrintSheetPlanningToolsView()->UpdateDialogControls(GetOrderDataFrame()->GetPrintSheetPlanningToolsView(), TRUE);
}

void CPrintSheetPlanningView::UndoRestore()
{
    CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if (theApp.m_undoBuffer.GetLength() <= 0)
		return;

	theApp.m_undoBuffer.SeekToBegin();
	CArchive archive(&theApp.m_undoBuffer, CArchive::load);

	pDoc->DeleteContents();
	pDoc->Serialize(archive);

	archive.Close();
	theApp.m_undoBuffer.SetLength(0);

	m_DisplayList.Invalidate();

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetStrippingView), NULL, 0, NULL, TRUE);	// reorganize pointers to foldsheets in table
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));	// reorganize pointers to foldsheets in table

	CPageListView* pView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pView)
	{
		CPageListFrame* pPageListFrame = (CPageListFrame*)pView->GetParentFrame();
		if (pPageListFrame)
			pPageListFrame->ReinitProductPartToolbar();
		theApp.UpdateView(RUNTIME_CLASS(CPageListView));
	}

	pDoc->UpdateAllViews(NULL);
}

CFoldSheet* CPrintSheetPlanningView::GetFoldSheet()
{
	if (m_DisplayList.IsInvalid())		// if UndoRestore() happens before, display list is invalid since foldsheet pointer has changed, so reinit (this method needs to be improved later)
	{
		int nIndex = GetSelectedComponentIndex();
		UpdateWindow();
		SelectComponentByIndex(nIndex);
	}

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if ( ! pDI)
		return NULL;
	return (CFoldSheet*)pDI->m_pData;
}

int	CPrintSheetPlanningView::GetLayerIndex()
{
	if (m_DisplayList.IsInvalid())		// if UndoRestore() happens before, display list is invalid since foldsheet pointer has changed, so reinit (this method needs to be improved later)
	{
		int nIndex = GetSelectedComponentIndex();
		UpdateWindow();
		SelectComponentByIndex(nIndex);
	}

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if ( ! pDI)
		return NULL;
	return (int)pDI->m_pAuxData;
}

CPrintSheet* CPrintSheetPlanningView::GetPrintSheet()
{
	if (m_DisplayList.IsInvalid())		// if UndoRestore() happens before, display list is invalid since foldsheet pointer has changed, so reinit (this method needs to be improved later)
	{
		int nIndex = GetSelectedComponentIndex();
		UpdateWindow();
		SelectComponentByIndex(nIndex);
	}

	CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, FoldSheetComponent));
	if (pDI)
	{
		CFoldSheet* pFoldSheet = (CFoldSheet*)pDI->m_pData;
		if (pFoldSheet)
			if (pFoldSheet->IsEmpty())
			{
				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				return (pDoc) ? pDoc->m_PrintSheetList.GetBlankSheet(pFoldSheet->m_nProductPartIndex) : NULL;
			}
			else
				return pFoldSheet->GetPrintSheet(GetLayerIndex());
	}

	pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, LabelComponent));
	if (pDI)
		return (CPrintSheet*)pDI->m_pAuxData;
	else
		return NULL;
}

BOOL CPrintSheetPlanningView::IsOpenPageNumeration()
{
	CPrintSheetPlanningToolsView* pView = (CPrintSheetPlanningToolsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningToolsView));
	if (pView)
		if (pView->m_dlgFoldSheetNumeration.m_hWnd)
			if (pView->m_dlgFoldSheetNumeration.m_pfnApplyCallback == CPrintSheetPlanningToolsView::ChangePageNumeration)
				return TRUE;
	return FALSE;
}

BOOL CPrintSheetPlanningView::IsOpenFoldSheetNumeration()
{
	CPrintSheetPlanningToolsView* pView = (CPrintSheetPlanningToolsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningToolsView));
	if (pView)
		if (pView->m_dlgFoldSheetNumeration.m_hWnd)
			if (pView->m_dlgFoldSheetNumeration.m_pfnApplyCallback == CPrintSheetPlanningToolsView::ChangeFoldSheetNumeration)
				return TRUE;
	return FALSE;
}

BOOL CPrintSheetPlanningView::IsOpenPrintSheetNumeration()
{
	CPrintSheetPlanningToolsView* pView = (CPrintSheetPlanningToolsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningToolsView));
	if (pView)
		if (pView->m_dlgFoldSheetNumeration.m_hWnd)
			if (pView->m_dlgFoldSheetNumeration.m_pfnApplyCallback == CPrintSheetPlanningToolsView::ChangePrintSheetNumeration)
				return TRUE;
	return FALSE;
}

BOOL CPrintSheetPlanningView::IsInsideNumerationRange(int nComponentIndex)
{
	CPrintSheetPlanningToolsView* pView = (CPrintSheetPlanningToolsView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningToolsView));
	if (pView)
		if (pView->m_dlgFoldSheetNumeration.m_hWnd)
			if (pView->IsInsideNumerationRange(nComponentIndex))
				return TRUE;
	return FALSE;
}

void CPrintSheetPlanningView::OnNcMouseMove(UINT nHitTest, CPoint point)
{
	// TODO: F�gen Sie hier Ihren Meldungsbehandlungscode ein, und/oder benutzen Sie den Standard.

	CScrollView::OnNcMouseMove(nHitTest, point);
}

void CPrintSheetPlanningView::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{
	if (bCalcValidRects && GetPrintSheet())
	{
		lpncsp->rgrc[0].right = 860;		
	}

	CScrollView::OnNcCalcSize(bCalcValidRects, lpncsp);
}

void CPrintSheetPlanningView::OnNcPaint()
{
	CRect rcClientRect;
	GetClientRect(rcClientRect);
	CRect rcWin;
	GetWindowRect(rcWin);

	CDC* pDC = GetWindowDC();

	CPrintSheet* pPrintSheet = GetPrintSheet();
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if (pLayout)
	{
		CRect rcLayout(rcClientRect.right + 20, rcClientRect.top, rcWin.Width(), rcWin.Height()); 
		pDC->FillSolidRect(rcLayout, RGB(230,233,238));//RGB(220,220,220));
		CRect rcBoxLayout = pLayout->Draw(pDC, rcLayout, pPrintSheet, pLayout->m_FrontSide.GetPressDevice(), TRUE, FALSE, TRUE, NULL, TRUE, FRONTSIDE, NULL, NULL, &m_DisplayList);
	}

	ReleaseDC(pDC);
}