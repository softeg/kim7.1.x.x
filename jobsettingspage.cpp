// JobSettingsPage1.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgAddExposure.h"
#include "DlgApplicationBook.h"
#include "DlgApplicationLabels.h"
#include "JobSettingsSheet.h"
#include "JobSettingsPage.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgPaperList.h"
#include "MainFrm.h"
#include "ProductDataView.h"
#include "BookBlockView.h"
#include "DlgSheet.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPressDevice.h"
#include "DlgPDFOutputMediaSettings.h"



#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CJobSettingsPage1, CPropertyPage)
IMPLEMENT_DYNCREATE(CJobSettingsPage2, CPropertyPage)
IMPLEMENT_DYNCREATE(CJobSettingsPage3, CPropertyPage)
IMPLEMENT_DYNCREATE(CJobSettingsPage4, CPropertyPage)


/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPage1 property page

CJobSettingsPage1::CJobSettingsPage1() : CPropertyPage(CJobSettingsPage1::IDD)
{
	//{{AFX_DATA_INIT(CJobSettingsPage1)
	m_strCreator = _T("");
	m_strCustomer = _T("");
	m_strCustomerNumber = _T("");
	m_strJob = _T("");
	m_strNotes = _T("");
	m_strTitle = _T("");
	m_CreationDate = time_t(0);
	m_LastModifiedDate = time_t(0);
	m_strScheduledToPrint = _T("");
	m_scheduledToPrint = time_t(0);
	//}}AFX_DATA_INIT
	m_bModified = FALSE;
}

CJobSettingsPage1::~CJobSettingsPage1()
{
}

void CJobSettingsPage1::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJobSettingsPage1)
	DDX_Text(pDX, IDC_CREATOR, m_strCreator);
	DDX_Text(pDX, IDC_CUSTOMER, m_strCustomer);
	DDX_Text(pDX, IDC_CUSTOMER_NUMBER, m_strCustomerNumber);
	DDX_Text(pDX, IDC_JOB_NUMBER, m_strJob);
	DDX_Text(pDX, IDC_NOTES, m_strNotes);
	DDV_MaxChars(pDX, m_strNotes, 1000);
	DDX_Text(pDX, IDC_TITLE, m_strTitle);
	DDX_Text(pDX, IDC_CREATION_DATE, m_CreationDate);
	DDX_Text(pDX, IDC_LAST_MODIFIED_DATE, m_LastModifiedDate);
	DDX_Text(pDX, IDC_SCHEDULED_TO_PRINT, m_strScheduledToPrint);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_scheduledToPrint);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CJobSettingsPage1, CPropertyPage)
	//{{AFX_MSG_MAP(CJobSettingsPage1)
	ON_EN_CHANGE(IDC_CREATOR, OnChangeCreator)
	ON_EN_CHANGE(IDC_CUSTOMER, OnChangeCustomer)
	ON_EN_CHANGE(IDC_CUSTOMER_NUMBER, OnChangeCustomerNumber)
	ON_EN_CHANGE(IDC_JOB_NUMBER, OnChangeJobNumber)
	ON_EN_CHANGE(IDC_NOTES, OnChangeNotes)
	ON_EN_CHANGE(IDC_TITLE, OnChangeTitle)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER1, OnDatetimechangeDatetimepicker1)
	ON_EN_CHANGE(IDC_SCHEDULED_TO_PRINT, OnChangeScheduledToPrint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CJobSettingsPage1::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
// initialize page members from document
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	CGlobalData* pGD  = &pDoc->m_GlobalData;

	m_strTitle			  = pDoc->GetTitle();
	m_strJob			  = pGD->m_strJob;  
	m_strCustomer		  = pGD->m_strCustomer;      
	m_strCustomerNumber   = pGD->m_strCustomerNumber;
	m_strCreator		  = pGD->m_strCreator; 
	m_CreationDate		  = pGD->m_CreationDate;
	m_LastModifiedDate	  = pGD->m_LastModifiedDate;
	m_strScheduledToPrint = pGD->m_strScheduledToPrint;
	m_strNotes			  = pGD->m_strNotes;	 
	
	if (m_CreationDate.m_dt == 0.0)
		m_CreationDate = COleDateTime::GetCurrentTime();
	if (m_LastModifiedDate.m_dt == 0.0)
		m_LastModifiedDate = COleDateTime::GetCurrentTime();

	m_scheduledToPrint = COleDateTime::GetCurrentTime();

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CJobSettingsPage1::OnChangeTitle() 
{
#ifdef CTP
	UpdateData(TRUE);
	char ch = m_strTitle.GetAt(m_strTitle.GetLength()-1);
	if (ch == _T(' ') || ch == _T('*') || ch == _T('?') || ch == _T(':') || ch == _T('/'))
	{	// characters not allowed for IPU
		m_strTitle.GetBufferSetLength(m_strTitle.GetLength()-1);
		UpdateData(FALSE);
		AfxMessageBox(IDS_CHAR_NOT_ALLOWED_FOR_IPU , MB_OK);
	}
#endif
	m_bModified = TRUE;
}

void CJobSettingsPage1::OnChangeCreator() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage1::OnChangeCustomer() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage1::OnChangeCustomerNumber() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage1::OnChangeJobNumber() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage1::OnChangeNotes() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage1::OnChangeScheduledToPrint() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage1::OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	UpdateData(TRUE);

	m_bModified = TRUE;

	m_strScheduledToPrint = CPrintSheetView::ConvertToString(m_scheduledToPrint, FALSE);

	UpdateData(FALSE);

	*pResult = 0;
}

void CJobSettingsPage1::OnOK() 
{
	if (m_bModified)
	{
		UpdateData(TRUE);
		CImpManDoc*  pDoc = CImpManDoc::GetDoc();
		CGlobalData* pGD = &pDoc->m_GlobalData;

		pDoc->SetTitle(m_strTitle);
		pGD->m_strJob				= m_strJob;
		pGD->m_strCustomer			= m_strCustomer;
		pGD->m_strCustomerNumber	= m_strCustomerNumber;
		pGD->m_strCreator			= m_strCreator;
		pGD->m_CreationDate			= m_CreationDate;
		pGD->m_LastModifiedDate		= m_LastModifiedDate;
		pGD->m_strScheduledToPrint	= m_strScheduledToPrint;
		pGD->m_strNotes				= m_strNotes;	
		
		pDoc->SetModifiedFlag(TRUE);
	}
	CPropertyPage::OnOK();
}

LRESULT CJobSettingsPage1::OnWizardNext() 
{
	UpdateData(TRUE);

	m_bModified = TRUE;		// in wizard mode always modified (new job)

	if (m_strTitle.GetLength() == 0)
	{
		AfxMessageBox(IDS_NO_TITLE_TEXT , MB_OK);
		return -1;	//Prevent the page from changing
	}
	else
		return CPropertyPage::OnWizardNext();
}

BOOL CJobSettingsPage1::OnSetActive() 
{
	if (theApp.m_bInsideDocNewWizard)
	{
		CString string;
		CJobSettingsSheet* pSheet;

		pSheet = (CJobSettingsSheet*)GetParentOwner();
		pSheet->SetWizardButtons(PSWIZB_BACK|PSWIZB_NEXT);
	}
	return CPropertyPage::OnSetActive();
}



/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPage2 property page

CJobSettingsPage2::CJobSettingsPage2() : CPropertyPage(CJobSettingsPage2::IDD)
{
	//{{AFX_DATA_INIT(CJobSettingsPage2)
	//}}AFX_DATA_INIT
	m_bModified = FALSE;
}

CJobSettingsPage2::~CJobSettingsPage2()
{
}

void CJobSettingsPage2::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJobSettingsPage2)
	DDX_Control(pDX, IDC_APPLICATION_CATEGORY, m_applicationCategory);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CJobSettingsPage2, CPropertyPage)
	//{{AFX_MSG_MAP(CJobSettingsPage2)
	ON_CBN_SELCHANGE(IDC_APPLICATION_CATEGORY, OnSelchangeApplicationCategory)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CJobSettingsPage2::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	CRect targetRect, dlgRect;
	GetDlgItem(IDC_APPLICATION_DATA_FRAME)->GetWindowRect(targetRect);
	ScreenToClient(targetRect);

	m_dlgApplicationBook.Create(IDD_APPLICATION_BOOK, this);
	m_dlgApplicationBook.GetWindowRect(dlgRect);
	m_dlgApplicationBook.SetWindowPos(NULL, targetRect.left, targetRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);

	m_dlgApplicationLabels.Create(IDD_APPLICATION_LABELS, this);
	m_dlgApplicationLabels.GetWindowRect(dlgRect);
	m_dlgApplicationLabels.SetWindowPos(NULL, targetRect.left, targetRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOZORDER);

	m_dlgApplicationBook.ShowWindow  ((theApp.m_nApplication == APPLICATION_BOOK) ? SW_SHOW : SW_HIDE);
	m_dlgApplicationLabels.ShowWindow((theApp.m_nApplication == APPLICATION_BOOK) ? SW_HIDE : SW_SHOW);

	switch (theApp.m_nApplication)
	{
	case APPLICATION_BOOK:		m_applicationCategory.SetCurSel(0); break;
	case APPLICATION_LABELS:	m_applicationCategory.SetCurSel(1); break;
	}

	if (theApp.m_bInsideDocNewWizard)
	{
		CImpManDoc*  pDoc = CImpManDoc::GetDoc();
		if (pDoc)
			pDoc->m_FinalProductDescription.m_nApplicationCategory = theApp.m_nApplication;	
		m_applicationCategory.EnableWindow(TRUE);
	}
	else
		m_applicationCategory.EnableWindow(FALSE);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CJobSettingsPage2::OnSelchangeApplicationCategory() 
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CFinalProductDescription* pFPD = &pDoc->m_FinalProductDescription;
	int nSel = m_applicationCategory.GetCurSel();
	if (nSel != CB_ERR)
	{
		switch (nSel)
		{
		case 0: pFPD->m_nApplicationCategory = APPLICATION_BOOK;	break;
		case 1: pFPD->m_nApplicationCategory = APPLICATION_LABELS;	break;
		}
		m_dlgApplicationBook.ShowWindow  ((pFPD->m_nApplicationCategory == APPLICATION_BOOK) ? SW_SHOW : SW_HIDE);
		m_dlgApplicationLabels.ShowWindow((pFPD->m_nApplicationCategory == APPLICATION_BOOK) ? SW_HIDE : SW_SHOW);
		theApp.m_nApplication = pFPD->m_nApplicationCategory;
	}
}

void CJobSettingsPage2::OnOK() 
{
	//switch (theApp.m_nApplication)
	//{
	//case APPLICATION_BOOK:		m_dlgApplicationBook.OnOK();	break;
	//case APPLICATION_LABELS:	m_dlgApplicationLabels.OnOK();	break;
	//}

	//theApp.InitializeApplication();

	CPropertyPage::OnOK();
}

LRESULT CJobSettingsPage2::OnWizardNext() 
{
	UpdateData(TRUE);

	CJobSettingsSheet* pSheet = (CJobSettingsSheet*)GetParentOwner();
	switch (theApp.m_nApplication)
	{
	case APPLICATION_BOOK:		if (pSheet->GetPageIndex(&pSheet->m_Page3) == -1)
								{
									pSheet->RemovePage(&pSheet->m_Page4);
									pSheet->AddPage	  (&pSheet->m_Page3);
								}
								if (m_dlgApplicationBook.OnWizardNext() == -1)
									return -1;
								break;
	case APPLICATION_LABELS:	if (pSheet->GetPageIndex(&pSheet->m_Page4) == -1)
								{
									pSheet->RemovePage(&pSheet->m_Page3);
									pSheet->AddPage	  (&pSheet->m_Page4);
								}
								if (m_dlgApplicationLabels.OnWizardNext() == -1)
									return -1;
								break;
	}

	return CPropertyPage::OnWizardNext();
}

BOOL CJobSettingsPage2::OnSetActive() 
{
	if (theApp.m_bInsideDocNewWizard)
	{
		CString string;
		CJobSettingsSheet* pSheet;

		pSheet = (CJobSettingsSheet*)GetParentOwner();
		pSheet->SetWizardButtons(PSWIZB_BACK|PSWIZB_NEXT);
	}
	return CPropertyPage::OnSetActive();
}



/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPage3 property page

CJobSettingsPage3::CJobSettingsPage3() : CPropertyPage(CJobSettingsPage3::IDD)
{
	//{{AFX_DATA_INIT(CJobSettingsPage3)
	m_fSideTrim = 0.0f;
	m_fHeadTrim = 0.0f;
	m_fFootTrim = 0.0f;
	m_fBackTrim = 0.0f;
	m_nProductionType = -1;
	m_strPressDeviceName = _T("");
	//}}AFX_DATA_INIT
	m_bModified = FALSE;
}

CJobSettingsPage3::~CJobSettingsPage3()
{
}

void CJobSettingsPage3::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJobSettingsPage3)
	DDX_Control(pDX, IDC_BACK_TRIM_SPIN, m_BackTrimSpin);
	DDX_Control(pDX, IDC_FOOT_TRIM_SPIN, m_FootTrimSpin);
	DDX_Control(pDX, IDC_SIDE_TRIM_SPIN, m_SideTrimSpin);
	DDX_Control(pDX, IDC_HEAD_TRIM_SPIN, m_HeadTrimSpin);
	DDX_Measure(pDX, IDC_SIDE_TRIM, m_fSideTrim);
	DDX_Measure(pDX, IDC_HEAD_TRIM, m_fHeadTrim);
	DDX_Measure(pDX, IDC_FOOT_TRIM, m_fFootTrim);
	DDX_Measure(pDX, IDC_BACK_TRIM, m_fBackTrim);
	DDX_CBIndex(pDX, IDC_PRODUCTION_TYPE, m_nProductionType);
	DDX_CBString(pDX, IDC_PRESSDEVICE_LIST, m_strPressDeviceName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CJobSettingsPage3, CPropertyPage)
	//{{AFX_MSG_MAP(CJobSettingsPage3)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SIDE_TRIM_SPIN, OnDeltaposSideTrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_HEAD_TRIM_SPIN, OnDeltaposHeadTrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_FOOT_TRIM_SPIN, OnDeltaposFootTrimSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_BACK_TRIM_SPIN, OnDeltaposBackTrimSpin)
	ON_EN_CHANGE(IDC_BACK_TRIM, OnChangeBackTrim)
	ON_EN_CHANGE(IDC_FOOT_TRIM, OnChangeFootTrim)
	ON_EN_CHANGE(IDC_HEAD_TRIM, OnChangeHeadTrim)
	ON_EN_CHANGE(IDC_SIDE_TRIM, OnChangeSideTrim)
	ON_CBN_SELCHANGE(IDC_PRESSDEVICE_LIST, OnSelchangePressdeviceList)
	ON_CBN_SELCHANGE(IDC_PRODUCTION_TYPE, OnSelchangeProductionType)
	ON_BN_CLICKED(IDC_PAPERLIST_BUTTON, OnPaperlistButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CJobSettingsPage3::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	m_HeadTrimSpin.SetRange(0, 1);  // we do not need a special range because we only use delta pos
	m_SideTrimSpin.SetRange(0, 1);	// from 0 to 1 meens:	upper spin button increases value
	m_FootTrimSpin.SetRange(0, 1);	//						lower spin button decreases value
	m_BackTrimSpin.SetRange(0, 1);


// initialize page members from document
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	m_fHeadTrim		  = pDoc->m_productionData.m_fHeadTrim;
	m_fSideTrim		  = pDoc->m_productionData.m_fSideTrim;		
	m_fFootTrim		  = pDoc->m_productionData.m_fFootTrim;		
	m_fBackTrim		  = pDoc->m_productionData.m_fBackTrim;		
	m_nProductionType = pDoc->m_productionData.m_nProductionType;
	CComboBox* pBox = (CComboBox*)GetDlgItem (IDC_PRODUCTION_TYPE);
	pBox->SetCurSel(m_nProductionType);

	CPressDevice PressDeviceBuf;
	pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);
	int numberDevices = theApp.m_PressDeviceList.GetCount();
	POSITION pos = theApp.m_PressDeviceList.GetHeadPosition();
	for (int i = 0; i < numberDevices; i ++)                        
	{
		PressDeviceBuf = theApp.m_PressDeviceList.GetAt(pos);
		pBox->AddString(PressDeviceBuf.m_strName);
		theApp.m_PressDeviceList.GetNext(pos);
	}
///////////////////////////////////////////////////////////////////////////////////////////////
	if (pDoc->m_nDefaultPressDeviceIndex == -1)
	{
		pBox->SetCurSel(0);		// take first machine in list as default
		return 0;
	}
	else
	{
		pos = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(pDoc->m_nDefaultPressDeviceIndex);
		PressDeviceBuf = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
		int nIndex = pBox->FindStringExact(-1, PressDeviceBuf.m_strName);
		if (nIndex != CB_ERR)
			pBox->SetCurSel(nIndex);
		else
			pBox->SetCurSel(-1);
		m_strPressDeviceName = PressDeviceBuf.m_strName;
	}
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CJobSettingsPage3::OnChangeHeadTrim() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnDeltaposHeadTrimSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_HeadTrimSpin.GetBuddy(), pNMUpDown->iDelta);
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnChangeSideTrim() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnDeltaposSideTrimSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_SideTrimSpin.GetBuddy(), pNMUpDown->iDelta);
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnChangeFootTrim() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnDeltaposFootTrimSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_FootTrimSpin.GetBuddy(), pNMUpDown->iDelta);
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnChangeBackTrim() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnDeltaposBackTrimSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_BackTrimSpin.GetBuddy(), pNMUpDown->iDelta);
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnSelchangePressdeviceList() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnSelchangeProductionType() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage3::OnOK() 
{
	if (m_bModified)
	{
		UpdateData(TRUE);
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return;

		pDoc->m_productionData.m_fHeadTrim		 = m_fHeadTrim;
		pDoc->m_productionData.m_fSideTrim		 = m_fSideTrim;
		pDoc->m_productionData.m_fFootTrim		 = m_fFootTrim;
		pDoc->m_productionData.m_fBackTrim		 = m_fBackTrim;
		pDoc->m_productionData.m_nProductionType = m_nProductionType;
		
		CComboBox *pBox = (CComboBox*)GetDlgItem(IDC_PRESSDEVICE_LIST);
		int		 nIndex = pBox->GetCurSel();
		if (nIndex != CB_ERR) // Only if something selected
		{
			pDoc->SetPrintSheetPressDevice(nIndex, ALLSIDES); // Set the default PressDevice

			int nLayouts = pDoc->m_PrintSheetList.m_Layouts.GetCount();
			if (nLayouts) // For a new job nLayouts is 0
			{
				CLayout* pActLayout;
				POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
				for (int i = 0; i < nLayouts; i++)
				{
					pActLayout = &(pDoc->m_PrintSheetList.m_Layouts.GetAt(pos));
					pActLayout->m_FrontSide.PositionLayoutSide();
					pActLayout->m_BackSide.PositionLayoutSide();
					pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
				}
			}
		}
		
		pDoc->SetModifiedFlag(TRUE);
		pDoc->UpdateAllViews(NULL);
	}
	CPropertyPage::OnOK();
}

void CJobSettingsPage3::OnPaperlistButton() 
{
	CDlgPaperList dlg;
	
	dlg.DoModal();
}

BOOL CJobSettingsPage3::OnSetActive() 
{
	if (theApp.m_bInsideDocNewWizard)
	{
		CString string;
		CJobSettingsSheet* pSheet;

		pSheet = (CJobSettingsSheet*)GetParentOwner();
		pSheet->SetWizardButtons(PSWIZB_BACK|PSWIZB_FINISH);
	}
	return CPropertyPage::OnSetActive();
}

BOOL CJobSettingsPage3::OnWizardFinish() 
{
	m_bModified = TRUE;		// in wizard mode always modified (new job)

	CJobSettingsSheet* pSheet = (CJobSettingsSheet*)GetParentOwner();
	pSheet->m_Page1.OnOK();
	pSheet->m_Page2.OnOK();
	OnOK(); 

	return CPropertyPage::OnWizardFinish();
}


/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPage4 property page
// Needed for CipCut or similar programs
CJobSettingsPage4::CJobSettingsPage4() : CPropertyPage(CJobSettingsPage4::IDD)
{
	//{{AFX_DATA_INIT(CJobSettingsPage4)
	m_strPressDeviceName = _T("");
	m_strOutputProfile = _T("");
	//}}AFX_DATA_INIT
	m_bModified = FALSE;
}

CJobSettingsPage4::~CJobSettingsPage4()
{
}

void CJobSettingsPage4::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJobSettingsPage4)
	DDX_Control(pDX, IDC_EXPOSER_COMBO, m_pdfTargetCombo);
	DDX_CBString(pDX, IDC_PRESSDEVICE_LIST, m_strPressDeviceName);
	DDX_CBString(pDX, IDC_EXPOSER_COMBO, m_strOutputProfile);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CJobSettingsPage4, CPropertyPage)
	//{{AFX_MSG_MAP(CJobSettingsPage4)
	ON_CBN_SELCHANGE(IDC_PRESSDEVICE_LIST, OnSelchangePressdeviceList)
	ON_CBN_SELCHANGE(IDC_EXPOSER_COMBO, OnSelchangeExposerCombo)
	ON_CBN_SELCHANGE(IDC_SHEET_LIST, OnSelchangeSheetList)
	ON_BN_CLICKED(IDC_MODIFY_PRESSDEVICE, OnModifyPressdevice)
	ON_BN_CLICKED(IDC_MODIFY_SHEET, OnModifySheet)
	ON_BN_CLICKED(IDC_MODIFY_OUTPUTPROFILE, OnModifyOutputprofile)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPage4 message handlers

BOOL CJobSettingsPage4::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	UpdateData(FALSE);

	InitPressDeviceCombo();
	InitSheetCombo();
	InitOutputProfileCombo();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CJobSettingsPage4::InitPressDeviceCombo()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheet* pPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;

	// press devices
	CComboBox* pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);
	pBox->ResetContent();

	POSITION pos = theApp.m_PressDeviceList.GetHeadPosition();
	while (pos)                        
	{
		CPressDevice& rPressDevice = theApp.m_PressDeviceList.GetNext(pos);
		pBox->AddString(rPressDevice.m_strName);
	}
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	CString string;
	if (pPressDevice) 
		string = pPressDevice->m_strName;
	if (string.GetLength())
		pBox->SelectString(-1, string);
	else
		pBox->SetCurSel(0);
}

void CJobSettingsPage4::InitSheetCombo()
{
	// sheets
	CDlgSheet dlg;		
	CComboBox* pBox = (CComboBox*)GetDlgItem (IDC_SHEET_LIST);
	pBox->ResetContent();

	if (dlg.LoadSheets() == TRUE)
	{
		POSITION pos = dlg.m_sheetList.GetHeadPosition();
		while (pos)                        
		{
			CSheet& rSheet = dlg.m_sheetList.GetNext(pos);
			pBox->AddString(rSheet.m_strSheetName);
		}	
	}

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheet* pPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	CString string;
	if (pLayout) 
		string = pLayout->m_FrontSide.m_strFormatName;
	if (string.GetLength())
		pBox->SelectString(-1, string);
	else
		pBox->SetCurSel(0);
}

void CJobSettingsPage4::InitOutputProfileCombo()
{
	// output profiles
	CImageList imageList;
	imageList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 3, 0);	// List not to grow
	imageList.Add(theApp.LoadIcon(IDI_PDF));
	imageList.Add(theApp.LoadIcon(IDI_PRINTER));
	imageList.Add(theApp.LoadIcon(IDI_JDF));
	m_pdfTargetCombo.SetImageList(&imageList);
	imageList.Detach();

	m_pdfTargetList.Load();
	if (m_pdfTargetList.GetSize() == 0)
		GetDlgItem(IDC_PRINTER_PROPS)->EnableWindow(FALSE);

	m_pdfTargetCombo.ResetContent();

	for (int i = 0; i < m_pdfTargetList.GetSize(); i++)
	{
		COMBOBOXEXITEM item;
		item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;
		item.iItem			= i;
		item.pszText		=  m_pdfTargetList[i].m_strTargetName.GetBuffer(MAX_TEXT);
		item.iImage			= (m_pdfTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 0 : (m_pdfTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 1 : 2;
		item.iSelectedImage = (m_pdfTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 0 : (m_pdfTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 1 : 2;
		m_pdfTargetCombo.InsertItem(&item);
	}

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CPrintSheet* pPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
	int nIndex = (pPrintSheet) ? m_pdfTargetList.FindIndex(pPrintSheet->m_strPDFTarget) : 0;
	m_pdfTargetCombo.SetCurSel((nIndex == -1) ? 0 : nIndex);
}

void CJobSettingsPage4::OnSelchangePressdeviceList() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage4::OnSelchangeExposerCombo() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage4::OnSelchangeSheetList() 
{
	m_bModified = TRUE;
}

void CJobSettingsPage4::OnModifyPressdevice() 
{
	CDlgPressDevice dlg;
	if (dlg.DoModal() == IDOK)
		m_bModified = TRUE;

	InitPressDeviceCombo();
}

void CJobSettingsPage4::OnModifySheet() 
{
	CDlgSheet sheetDlg;
	if (sheetDlg.DoModal() == IDOK)
		m_bModified = TRUE;

	InitSheetCombo();
}

void CJobSettingsPage4::OnModifyOutputprofile() 
{
	m_bModified = TRUE;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheet* pPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
	if (pPrintSheet)
	{
		CString string;
		int nIndex = m_pdfTargetCombo.GetCurSel();
		m_pdfTargetCombo.GetLBText(nIndex, string);
		pPrintSheet->m_strPDFTarget = string;
	}

	CDlgPDFOutputMediaSettings dlg(this, TRUE);
	dlg.DoModal();

	InitOutputProfileCombo();
}

void CJobSettingsPage4::OnOK() 
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (theApp.m_bInsideDocNewWizard)
		if ( ! pDoc->m_PrintSheetList.GetCount())
			pDoc->m_PrintSheetList.AddBlankSheet(0);

	if (m_bModified || theApp.m_bInsideDocNewWizard)
	{
		CPrintSheet* pPrintSheet = (pDoc->m_PrintSheetList.GetCount()) ? &pDoc->m_PrintSheetList.GetHead() : NULL;
		CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;

		// press devices
		CComboBox* pBox = (CComboBox*)GetDlgItem (IDC_PRESSDEVICE_LIST);
		int		 nIndex = pBox->GetCurSel();
//		CDlgPressDevice::SetPressDevice(BOTHSIDES, pBox->GetCurSel());

		// sheet
		CPressDevice* pActPressDevice = pDoc->GetActPressDevice();
		pBox   = (CComboBox*)GetDlgItem (IDC_SHEET_LIST);
		nIndex = pBox->GetCurSel();
		if (pLayout && pActPressDevice && (nIndex != CB_ERR))
		{	
			CDlgSheet dlg;
			dlg.LoadSheets();
			CString strName;
			pBox->GetLBText(nIndex, strName);
			POSITION pos = dlg.m_sheetList.FindByName(strName);
			if (pos)
			{
				CSheet& rSheet	   = dlg.m_sheetList.GetAt(pos);
				BOOL	bStoreData = TRUE;
				if ((rSheet.m_fWidth > pActPressDevice->m_fPlateWidth) || (rSheet.m_fHeight > pActPressDevice->m_fPlateHeight))
					if (AfxMessageBox(IDS_SHEET_GREATER_PLATE , MB_YESNO|MB_ICONSTOP) == IDNO)
						bStoreData = FALSE;

				if (bStoreData)
				{
					CLayoutSide* pLayoutSide = &pLayout->m_FrontSide;
					pLayoutSide->m_fPaperWidth		 = rSheet.m_fWidth;
					pLayoutSide->m_fPaperHeight		 = rSheet.m_fHeight;		
					pLayoutSide->m_nPaperOrientation = (rSheet.m_fWidth > rSheet.m_fHeight) ? LANDSCAPE : PORTRAIT;
					pLayoutSide->m_strFormatName	 = rSheet.m_strSheetName;
					pLayoutSide->m_bAdjustFanout	 = rSheet.m_FanoutList.GetSize();
					pLayoutSide->m_FanoutList		 = rSheet.m_FanoutList;
					if (theApp.m_bInsideDocNewWizard)
						pLayoutSide->PositionLayoutSide(pActPressDevice->m_fGripper);
					else
						pLayoutSide->PositionLayoutSide();

					pLayoutSide						 = &pLayout->m_BackSide;
					pLayoutSide->m_fPaperWidth		 = rSheet.m_fWidth;
					pLayoutSide->m_fPaperHeight		 = rSheet.m_fHeight;		
					pLayoutSide->m_nPaperOrientation = (rSheet.m_fWidth > rSheet.m_fHeight) ? LANDSCAPE : PORTRAIT;
					pLayoutSide->m_strFormatName	 = rSheet.m_strSheetName;
					pLayoutSide->m_bAdjustFanout	 = rSheet.m_FanoutList.GetSize();
					pLayoutSide->m_FanoutList		 = rSheet.m_FanoutList;
					if (theApp.m_bInsideDocNewWizard)
						pLayoutSide->PositionLayoutSide(pActPressDevice->m_fGripper);
					else
						pLayoutSide->PositionLayoutSide();
				}
			}
		}
	
		// output profiles
		if (pPrintSheet)
		{
			nIndex = m_pdfTargetCombo.GetCurSel();
			if (nIndex != CB_ERR)
			{
				CString string;
				m_pdfTargetCombo.GetLBText(nIndex, string);
				pPrintSheet->m_strPDFTarget = string;
			}

			CPDFTargetList pdfTargetList;
			pdfTargetList.Load();
			pDoc->m_PrintSheetList.m_pdfTargets.RemoveAll();
			for (int i = 0; i < pdfTargetList.GetSize(); i++)
				pDoc->m_PrintSheetList.m_pdfTargets.Add(pdfTargetList.ElementAt(i));
		}

		pDoc->SetModifiedFlag(TRUE);
		pDoc->UpdateAllViews(NULL);
	}
	CPropertyPage::OnOK();
}

BOOL CJobSettingsPage4::OnSetActive() 
{
	if (theApp.m_bInsideDocNewWizard)
	{
		CString string;
		CJobSettingsSheet* pSheet;

		pSheet = (CJobSettingsSheet*)GetParentOwner();
		pSheet->SetWizardButtons(PSWIZB_BACK|PSWIZB_FINISH);
	}
	return CPropertyPage::OnSetActive();
}

BOOL CJobSettingsPage4::OnWizardFinish() 
{
	UpdateData(TRUE);

	CJobSettingsSheet* pSheet;
	pSheet = (CJobSettingsSheet*)GetParentOwner();
	pSheet->m_Page1.OnOK();
	pSheet->m_Page2.OnOK();
	OnOK(); //page4
	
	return CPropertyPage::OnWizardFinish();
}

LRESULT CJobSettingsPage4::OnWizardNext() 
{
	UpdateData(TRUE);

	return CPropertyPage::OnWizardNext();
}
