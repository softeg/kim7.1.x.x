// ProdDataHeaderView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "ProdDataHeaderView.h"
#include "OrderDataFrame.h"
#include "DlgJobData.h"
#include "GraphicComponent.h"
#include "ProdDataProcessOverView.h"
#include "MainFrm.h"
#include "NewPrintSheetFrame.h"



// CPDHCustomPaintFrame

IMPLEMENT_DYNAMIC(CPDHCustomPaintFrame, CStatic)

CPDHCustomPaintFrame::CPDHCustomPaintFrame()
{
}

CPDHCustomPaintFrame::~CPDHCustomPaintFrame()
{
}

BEGIN_MESSAGE_MAP(CPDHCustomPaintFrame, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()


void CPDHCustomPaintFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CProdDataHeaderView* pView = (CProdDataHeaderView*)GetParent();
	if ( ! pView)
		return;
	CRect rcFrame;
	GetClientRect(rcFrame);
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	switch (GetDlgCtrlID())
	{
	case IDC_PDH_JOBTITLE_HEADER:			{
												CGraphicComponent gp;
												gp.DrawTitleBar(&dc, rcFrame, _T(""), RGB(160,180,255), RGB(120,140,230), 90);
												//gp.DrawTitleBar(&dc, rcFrame, _T(""), RGB(255, 240, 180), RGB(235, 225, 155), 90, FALSE);

												dc.SetTextColor(RGB(100, 120, 200));
												dc.SetBkMode(TRANSPARENT);
												dc.SelectObject(&pView->m_bigFont);
												CString strOut;
												rcFrame.OffsetRect(15, 0);
												if (pDoc)
													dc.DrawText(pDoc->GetTitle(), rcFrame, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
											}
											break;

	case IDC_JC_ORDERDATA_FRAME:			
	case IDC_JC_PRODUCTDATA_FRAME:			{
												CPen pen(PS_SOLID, 1, MIDDLEGRAY);
												dc.SelectObject(&pen);
												CBrush brush(RGB(250, 250, 245));
												dc.SelectObject(&brush);
												dc.RoundRect(rcFrame, CPoint(6,6));
												if (pDoc)
													switch (GetDlgCtrlID())
													{
													case IDC_JC_ORDERDATA_FRAME:	pDoc->m_GlobalData.Draw(&dc, rcFrame, NULL, TRUE);			break;
													case IDC_JC_PRODUCTDATA_FRAME:	break;
													}
											}
											break;

	case IDC_JC_ORDERDATA_HEADER:			
	case IDC_JC_PRODUCTDATA_HEADER:			{
												CString strOut;
												switch (GetDlgCtrlID())
												{
												case IDC_JC_ORDERDATA_HEADER:	strOut.LoadString(IDS_GLOBAL);		break;
												case IDC_JC_PRODUCTDATA_HEADER: strOut.LoadString(IDS_PRODUCT);	break;
												}
												CGraphicComponent gp;
												gp.DrawTitleBar(&dc, rcFrame, strOut, RGB(203,221,255), RGB(160,180,255), 90);
											}
											break;

	default:								break;
	}
}


// CProdDataHeaderView

IMPLEMENT_DYNCREATE(CProdDataHeaderView, CFormView)

CProdDataHeaderView::CProdDataHeaderView()
	: CFormView(CProdDataHeaderView::IDD)
{
	m_bigFont.CreateFont(22, 0, 0, 0, FW_BOLD,	FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	m_hollowBrush.CreateStockObject(HOLLOW_BRUSH);
}

CProdDataHeaderView::~CProdDataHeaderView()
{
	m_bigFont.DeleteObject();
	m_hollowBrush.DeleteObject();
}

void CProdDataHeaderView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CProdDataHeaderView, CFormView)
	ON_WM_SIZE()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CProdDataHeaderView-Diagnose

#ifdef _DEBUG
void CProdDataHeaderView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CProdDataHeaderView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CProdDataHeaderView-Meldungshandler

void CProdDataHeaderView::OnInitialUpdate()
{
	m_jobTitleHeader.SubclassDlgItem	(IDC_PDH_JOBTITLE_HEADER,		this);
	m_orderDataHeader.SubclassDlgItem	(IDC_JC_ORDERDATA_HEADER,		this);
	m_orderDataFrame.SubclassDlgItem	(IDC_JC_ORDERDATA_FRAME,		this);
	m_productDataHeader.SubclassDlgItem	(IDC_JC_PRODUCTDATA_HEADER,		this);
	m_productDataFrame.SubclassDlgItem	(IDC_JC_PRODUCTDATA_FRAME,		this);

	CFormView::OnInitialUpdate();
}

void CProdDataHeaderView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	//if ( ! m_jobTitleHeader.m_hWnd)
	//	return;

	//CRect rcTitle, rcTarget;
	//m_jobTitleHeader.GetClientRect(rcTitle);
	//m_jobTitleHeader.ClientToScreen(rcTitle);
	//ScreenToClient(rcTitle);
	//GetClientRect(rcTarget);
	//m_jobTitleHeader.SetWindowPos(NULL, rcTitle.left, rcTitle.top, rcTarget.Width(), rcTitle.Height(), SWP_NOZORDER);
}

HBRUSH CProdDataHeaderView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

    if (nCtlColor == CTLCOLOR_STATIC)
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_STATIC:
				pDC->SetBkMode(TRANSPARENT);
				pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
				hbr = (HBRUSH)m_hollowBrush.GetSafeHandle();
				break;

		default:break;
		}
    }

	return hbr;
}
