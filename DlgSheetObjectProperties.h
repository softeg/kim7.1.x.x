#if !defined(AFX_DLGSHEETOBJECTPROPERTIES_H__C01FC225_7C68_11D5_8877_204C4F4F5020__INCLUDED_)
#define AFX_DLGSHEETOBJECTPROPERTIES_H__C01FC225_7C68_11D5_8877_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSheetObjectProperties.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgSheetObjectProperties

class CDlgSheetObjectProperties : public CDialog
{
// Konstruktion
public:
	CDlgSheetObjectProperties(CWnd* pParent = NULL);   // Standardkonstruktor

// Destruktion
public:
	~CDlgSheetObjectProperties();

friend class CDlgLayoutObjectProperties;

enum Category{ LayoutObject, UnassignedMark };

// Dialogfelddaten
	//{{AFX_DATA(CDlgSheetObjectProperties)
	enum { IDD = IDD_SHEETOBJECT_PROPERTIES };
	CTabCtrl	m_sheetObjectPropsTab;
	//}}AFX_DATA

public:
	int							m_nObjectCategory;
	int							m_nObjectType;
	CLayoutObject*				m_pLayoutObject;
	CPageTemplate*				m_pTemplate;
	CMark*						m_pMark;
	CDlgSheetObjectPDFProps		m_dlgSheetObjectPDFProps;
	CDlgSheetObjectPositioning	m_dlgSheetObjectPosition;
	CDlgMarkPlacement			m_dlgMarkPlacement;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgSheetObjectProperties)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	BOOL Create(CLayoutObject* pLayoutObject, CPageTemplate* pTemplate, CWnd* pParent);
	BOOL Create(CMark* pMark, CWnd* pParent);
protected:
	void InitCustomMarkDialog();
	void InitTextMarkDialog();
	void InitSectionMarkDialog();
	void InitCropMarkDialog();
	void InitLayoutObjectDialog();
	void LoadLayoutObjectData();		
	void SaveLayoutObjectData();		
	void LoadCustomMarkData();


	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgSheetObjectProperties)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeSheetObjectPropsTab(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGSHEETOBJECTPROPERTIES_H__C01FC225_7C68_11D5_8877_204C4F4F5020__INCLUDED_
