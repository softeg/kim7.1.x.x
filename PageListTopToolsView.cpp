// PageListTopToolsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "PageListFrame.h"
#include "PageListTopToolsView.h"
#include "PDFReader.h"
#include "PageListView.h"
#include "PageListDetailView.h"
#include "GraphicComponent.h"


// CPageListTopToolsView

IMPLEMENT_DYNCREATE(CPageListTopToolsView, CFormView)

CPageListTopToolsView::CPageListTopToolsView()
	: CFormView(CPageListTopToolsView::IDD)
{

}

CPageListTopToolsView::~CPageListTopToolsView()
{
}

BOOL CPageListTopToolsView::OnEraseBkgnd(CDC* pDC)
{
    CRect rcRect;
    GetClientRect(rcRect);

	UpdateBackground(pDC, rcRect, this);

	return TRUE;

//	return CFormView::OnEraseBkgnd(pDC);
}

void CPageListTopToolsView::UpdateBackground(CDC* pDC, CRect rcRect, CWnd* /*pWnd*/)
{
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcRect, _T(""), RGB(234,237,242), RGB(211,218,228), 90, -1);
}

void CPageListTopToolsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PAGELIST_TOOLBAR_COMBO, m_productCombo);
}

BEGIN_MESSAGE_MAP(CPageListTopToolsView, CFormView)
	ON_WM_ERASEBKGND()
	ON_UPDATE_COMMAND_UI(IDC_PAGESLIST, OnUpdatePageslist)
	ON_UPDATE_COMMAND_UI(IDC_PAGESOURCE, OnUpdatePagesource)
	ON_UPDATE_COMMAND_UI(IDC_PAGEDETAIL, OnUpdatePagedetail)
	ON_UPDATE_COMMAND_UI(IDC_PAGEPROPS, OnUpdatePageprops)
	ON_UPDATE_COMMAND_UI(ID_PAGINATION, OnUpdatePagination)
	ON_UPDATE_COMMAND_UI(ID_DELETE_SELECTION, OnUpdateDeleteSelection)
	ON_UPDATE_COMMAND_UI(ID_REGISTER_PDF_DOC, OnUpdateRegisterPDFDoc)
	ON_UPDATE_COMMAND_UI(ID_OPEN_PDF_DOC, OnUpdateOpenPDFDoc)
	ON_UPDATE_COMMAND_UI(ID_REFRESH_PAGESOURCES, OnUpdateRefreshPagesources)
	ON_UPDATE_COMMAND_UI(ID_ZOOM, OnUpdateZoom)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_DECREASE, OnUpdateZoomDecrease)
	ON_CBN_SELCHANGE(IDC_PAGELIST_TOOLBAR_COMBO, &CPageListTopToolsView::OnCbnSelchangePagelistToolbarCombo)
END_MESSAGE_MAP()


// CPageListTopToolsView-Diagnose

#ifdef _DEBUG
void CPageListTopToolsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPageListTopToolsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPageListTopToolsView-Meldungshandler

void CPageListTopToolsView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	m_productImageList.Create(16, 16, ILC_COLOR24 | ILC_MASK, 2, 0);	// List not to grow
	m_productImageList.Add(theApp.LoadIcon(IDI_BOUND_PRODUCT));
	m_productImageList.Add(theApp.LoadIcon(IDI_BOUND_PRODUCTPART));
	m_productImageList.Add(theApp.LoadIcon(IDI_FLAT_PRODUCT));
	m_productCombo.SetImageList(&m_productImageList);

	CRect rcButton; 
	m_pageListToolbar.Create(this, GetParentFrame(), NULL, IDB_PAGELIST_TOOLBAR, IDB_PAGELIST_TOOLBAR_COLD, CSize(20, 22), &UpdateBackground);
	GetDlgItem(IDC_PAGELIST_TOOLBAR_FRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton);	rcButton.right = rcButton.CenterPoint().x;
	m_pageListToolbar.AddButton(-1, IDC_PAGESLIST,				IDC_PAGESLIST,				rcButton, 0);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, IDC_PAGESOURCE,				IDC_PAGESOURCE,				rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, IDC_PAGEDETAIL,				IDC_PAGEDETAIL,				rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, IDC_PAGEPROPS,				IDC_PAGEPROPS,				rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, ID_PAGINATION,				ID_PAGINATION,				rcButton, 4);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, ID_DELETE_SELECTION,		ID_DELETE_SELECTION,		rcButton, 5);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, ID_REGISTER_PDF_DOC,		ID_REGISTER_PDF_DOC,		rcButton, 6);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, ID_OPEN_PDF_DOC,			ID_OPEN_PDF_DOC,			rcButton, 7);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, ID_REFRESH_PAGESOURCES,		ID_REFRESH_PAGESOURCES,		rcButton, 8);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, ID_ZOOM,					ID_ZOOM,					rcButton, 9);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_pageListToolbar.AddButton(-1, ID_ZOOM_DECREASE,			ID_ZOOM_DECREASE,			rcButton,10);	rcButton.OffsetRect(rcButton.Width(), 0);

	SetScaleToFitSize(CSize(1, rcButton.top + rcButton.bottom));	//suppress scrollbars
}

void CPageListTopToolsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	CPageListFrame* pFrame = (CPageListFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_productCombo.ResetContent();
	COMBOBOXEXITEM item;
	item.mask = CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE | CBEIF_INDENT | CBEIF_LPARAM;
	for (int i = 0; i < pDoc->m_boundProducts.GetSize(); i++)
	{
		CBoundProduct& rBoundProduct = pDoc->GetBoundProduct(i);

		item.iItem			= m_productCombo.GetCount(); 
		item.pszText		= rBoundProduct.m_strProductName.GetBuffer();
		item.cchTextMax		= MAX_TEXT;
		item.lParam			= -i - 1;
		item.iImage			= 0;
		item.iSelectedImage = 0;
		item.iIndent		= 0;
		m_productCombo.InsertItem(&item);

		for (int j = 0; j < rBoundProduct.m_parts.GetSize(); j++)
		{
			CBoundProductPart& rBoundProductPart = rBoundProduct.m_parts[j];

			item.iItem			= m_productCombo.GetCount(); 
			item.pszText		= rBoundProductPart.m_strName.GetBuffer();
			item.cchTextMax		= MAX_TEXT;
			item.lParam			= rBoundProduct.TransformLocalToGlobalPartIndex(j);
			item.iImage			= 1;
			item.iSelectedImage = 1;
			item.iIndent		= 1;
			m_productCombo.InsertItem(&item);
		}	
	}
	if (pDoc->m_flatProducts.GetSize() > 0)
	{
		static CString string; string.LoadString(IDS_UNBOUND);
		item.iItem			= m_productCombo.GetCount(); 
		item.pszText		= string.GetBuffer();
		item.cchTextMax		= MAX_TEXT;
		item.lParam			= pDoc->GetFlatProductsIndex();
		item.iImage			= 2;
		item.iSelectedImage = 2;
		item.iIndent		= 0;
		m_productCombo.InsertItem(&item);
	}

	m_productCombo.SetCurSel(0);

	UpdateData(FALSE);
}

void CPageListTopToolsView::OnCbnSelchangePagelistToolbarCombo()
{
	theApp.UpdateView(RUNTIME_CLASS(CPageListView), TRUE);
}

BOOL CPageListTopToolsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_pageListToolbar.m_pToolTip)            
		m_pageListToolbar.m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

void CPageListTopToolsView::OnUpdatePageslist(CCmdUI* pCmdUI)
{
	CPageListFrame* pFrame = (CPageListFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::Enable(pCmdUI, pFrame->m_bEnablePagesListButton);
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowPagesList);
}

void CPageListTopToolsView::OnUpdatePagesource(CCmdUI* pCmdUI)
{
	CPageListFrame* pFrame = (CPageListFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::Enable(pCmdUI, pFrame->m_bEnablePageSourceButton);
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowPageSource);
}

void CPageListTopToolsView::OnUpdatePagedetail(CCmdUI* pCmdUI)
{
	CPageListFrame* pFrame = (CPageListFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::Enable(pCmdUI, pFrame->m_bEnablePageDetailButton);
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowPageDetail);

	if ( ! pFrame->m_bShowPageDetail)
		pFrame->m_bShowPropsDlg = FALSE;
}

void CPageListTopToolsView::OnUpdatePageprops(CCmdUI* pCmdUI)
{
	CPageListFrame* pFrame = (CPageListFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::Enable(pCmdUI, pFrame->m_bEnablePropsDlgButton);
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bShowPropsDlg);
}

void CPageListTopToolsView::OnUpdatePagination(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPageListTopToolsView::OnUpdateDeleteSelection(CCmdUI* pCmdUI)
{
	CPageListFrame* pFrame = (CPageListFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::Enable(pCmdUI, pFrame->m_bDeleteSelectionMenuActive);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPageListTopToolsView::OnUpdateRegisterPDFDoc(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPageListTopToolsView::OnUpdateOpenPDFDoc(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPageListTopToolsView::OnUpdateRefreshPagesources(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CPageListTopToolsView::OnUpdateZoom(CCmdUI* pCmdUI)
{
	CPageListFrame* pFrame = (CPageListFrame*)GetParentFrame();
	if ( ! pFrame)
		return;
	COwnerDrawnButtonList::SetCheck(pCmdUI, pFrame->m_bZoomMode);
	COwnerDrawnButtonList::Enable(pCmdUI, pFrame->m_bZoomMenuActive);
}

void CPageListTopToolsView::OnUpdateZoomDecrease(CCmdUI* pCmdUI)
{
	CPageListDetailView* pView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
	if ( ! pView)
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		return;
	}
	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}
