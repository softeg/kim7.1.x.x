// KIMFileDialog.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "KIMFileDialog.h"


// CKIMFileDialog

IMPLEMENT_DYNAMIC(CKIMFileDialog, CFileDialog)

CKIMFileDialog::CKIMFileDialog(BOOL bOpenFileDialog, LPCTSTR lpszDefExt, LPCTSTR lpszFileName,
		DWORD dwFlags, LPCTSTR lpszFilter, CWnd* pParentWnd) :
		CFileDialog(bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags, lpszFilter, pParentWnd)
{

}

CKIMFileDialog::~CKIMFileDialog()
{
}


BEGIN_MESSAGE_MAP(CKIMFileDialog, CFileDialog)
	ON_WM_DESTROY()
	ON_WM_SIZE()
END_MESSAGE_MAP()



// CKIMFileDialog-Meldungshandler



BOOL CKIMFileDialog::OnInitDialog()
{
    // Get a pointer to the original dialog box.
    CWnd *wndDlg = GetParent();

	CRect rcOrigSize;
	wndDlg->GetWindowRect(rcOrigSize);

	CRect rcFrame = rcOrigSize;
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\") + _T("CKIMFileDialog");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		DWORD dwSize = sizeof(rcFrame);
		if (RegQueryValueEx(hKey, _T("Placement"), 0, NULL, (LPBYTE)&rcFrame, &dwSize) == ERROR_SUCCESS)
			wndDlg->SetWindowPos(NULL, 0, 0, rcFrame.Width(), rcFrame.Height(), SWP_FRAMECHANGED);

		RegCloseKey(hKey);
	}

    // Number of controls in the File Dialog
    const UINT nControls = 8;

    // Control ID's - defined in <dlgs.h>
    UINT Controls[nControls] = {stc3, stc2, // The two label controls
                                cmb13, chx1, cmb1, // The eidt control and the drop-down box
                                IDOK, IDCANCEL,
                                lst1}; // The Explorer window

	int iExtraSizeX = rcFrame.Width()  - rcOrigSize.Width();
	int iExtraSizeY = rcFrame.Height() - rcOrigSize.Height();

    // Go through each of the controls in the dialog box, and move them to a new position
	CRect Rect;
    for (int i=0 ; i<nControls ; i++)
    {
        CWnd *wndCtrl = wndDlg->GetDlgItem(Controls[i]);
		if ( ! wndCtrl)
			continue;
        wndCtrl->GetWindowRect(&Rect);
        wndDlg->ScreenToClient(&Rect); // Remember it is child controls

        // Move all the controls according to the new size of the dialog.
        if (Controls[i] != lst1)
            wndCtrl->SetWindowPos(NULL,
                            Rect.left + iExtraSizeX, Rect.top + iExtraSizeY,
                            0, 0, SWP_NOSIZE);
        else // This is the explorer like window. It should be sized - not moved.
            wndCtrl->SetWindowPos(NULL, 0, 0,
                                Rect.right - Rect.left + iExtraSizeX,
                                Rect.bottom - Rect.top + iExtraSizeY,
                                SWP_NOMOVE);
    }

	CFileDialog::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CKIMFileDialog::OnDestroy()
{
	CFileDialog::OnDestroy();

	CRect rcFrame;
	GetParent()->GetWindowRect(rcFrame);

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\") + _T("CKIMFileDialog");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		RegSetValueEx(hKey, _T("Placement"), 0, REG_BINARY, (LPBYTE)&rcFrame, sizeof(rcFrame));
		RegCloseKey(hKey);
	}
}

void CKIMFileDialog::OnSize(UINT nType, int cx, int cy)
{
	CFileDialog::OnSize(nType, cx, cy);

	// TODO: F�gen Sie hier Ihren Meldungsbehandlungscode ein.
}
