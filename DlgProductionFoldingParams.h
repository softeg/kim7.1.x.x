#pragma once
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "afxcmn.h"


// CDlgProductionFoldingParams-Dialogfeld

class CDlgProductionFoldingParams : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionFoldingParams)

public:
	CDlgProductionFoldingParams(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionFoldingParams();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTION_FOLDINGPARAMS };


public:
	CPoint			m_ptInitialPos;
	BOOL			m_bLocked;
	CImageList*		m_pSchemeImageList;
	CDlgMarkSet*	m_pDlgMarkSet;


public:
	CProductionProfile& GetProductionProfile();
	void 				InitData();
	void 				SaveData();
	void 				InitSchemeList(CFoldSheetList& rSchemeList);
	void 				InitShinglingStyle();


protected:
	CListCtrl	m_schemeList;
	BOOL		m_bShinglingActive;
	int			m_nShinglingMethod;
	int			m_nShinglingDirection;
	CComboBoxEx	m_shinglingStyleCombo;
	int			m_nShinglingStyle;
	BOOL		m_bShinglingCropMarks;
	CString		m_strMarkset;

public:
	virtual BOOL Create(UINT nIDTemplate, CWnd* pParentWnd = NULL);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedFoldingparamsAddscheme();
	afx_msg void OnBnClickedFoldingparamsDeletescheme();
	afx_msg void OnNMCustomdrawPdSchemelist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeShinglingStyleCombo();
	afx_msg void OnBnClickedPdBrowseMarkset();
	afx_msg void OnBnClickedFpRemoveMarkset();
};
