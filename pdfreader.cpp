// Mit Microsoft Visual C++ automatisch erstellte IDispatch-Kapselungsklasse(n).

// HINWEIS: Die Inhalte dieser Datei nicht ändern. Wenn Microsoft Visual C++
// diese Klasse erneuert, werden Ihre Änderungen überschrieben.


#include "stdafx.h"
#include "pdfreader.h"

/////////////////////////////////////////////////////////////////////////////
// CPdfReader

IMPLEMENT_DYNCREATE(CPdfReader, CWnd)

/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CPdfReader 

CString CPdfReader::GetSrc()
{
	CString result;
	GetProperty(0x1, VT_BSTR, (void*)&result);
	return result;
}

void CPdfReader::SetSrc(LPCTSTR propVal)
{
	SetProperty(0x1, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// Operationen CPdfReader 

BOOL CPdfReader::LoadFile(LPCTSTR fileName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		fileName);
	return result;
}

void CPdfReader::setShowToolbar(BOOL On)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 On);
}

void CPdfReader::gotoFirstPage()
{
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void CPdfReader::gotoLastPage()
{
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void CPdfReader::gotoNextPage()
{
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void CPdfReader::gotoPreviousPage()
{
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void CPdfReader::setCurrentPage(long n)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 n);
}

void CPdfReader::goForwardStack()
{
	InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void CPdfReader::goBackwardStack()
{
	InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void CPdfReader::setPageMode(LPCTSTR pageMode)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 pageMode);
}

void CPdfReader::setLayoutMode(LPCTSTR layoutMode)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 layoutMode);
}

void CPdfReader::setNamedDest(LPCTSTR namedDest)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 namedDest);
}

void CPdfReader::Print()
{
	InvokeHelper(0xe, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void CPdfReader::printWithDialog()
{
	InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void CPdfReader::setZoom(float percent)
{
	static BYTE parms[] =
		VTS_R4;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 percent);
}

void CPdfReader::setZoomScroll(float percent, float left, float top)
{
	static BYTE parms[] =
		VTS_R4 VTS_R4 VTS_R4;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 percent, left, top);
}

void CPdfReader::setView(LPCTSTR viewMode)
{
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 viewMode);
}

void CPdfReader::setViewScroll(LPCTSTR viewMode, float offset)
{
	static BYTE parms[] =
		VTS_BSTR VTS_R4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 viewMode, offset);
}

void CPdfReader::setViewRect(float left, float top, float width, float height)
{
	static BYTE parms[] =
		VTS_R4 VTS_R4 VTS_R4 VTS_R4;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 left, top, width, height);
}

void CPdfReader::printPages(long from, long to)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 from, to);
}

void CPdfReader::printPagesFit(long from, long to, BOOL shrinkToFit)
{
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 from, to, shrinkToFit);
}

void CPdfReader::printAll()
{
	InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void CPdfReader::printAllFit(BOOL shrinkToFit)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 shrinkToFit);
}

void CPdfReader::setShowScrollbars(BOOL On)
{
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 On);
}

void CPdfReader::AboutBox()
{
	InvokeHelper(0xfffffdd8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}
