#if !defined(AFX_DLGTRIMBOXWARNING_H__B7C3E628_98A7_4B7F_8F39_1E6D2EDC1B13__INCLUDED_)
#define AFX_DLGTRIMBOXWARNING_H__B7C3E628_98A7_4B7F_8F39_1E6D2EDC1B13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgTrimboxWarning.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgTrimboxWarning 

class CDlgTrimboxWarning : public CDialog
{
// Konstruktion
public:
	CDlgTrimboxWarning(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgTrimboxWarning)
	enum { IDD = IDD_TRIMBOX_WARNING };
	CString	m_strText;
	BOOL	m_bTrimboxWarningCheck;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgTrimboxWarning)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgTrimboxWarning)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGTRIMBOXWARNING_H__B7C3E628_98A7_4B7F_8F39_1E6D2EDC1B13__INCLUDED_
