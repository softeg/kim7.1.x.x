// DlgProductionPlannerUnbound.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionPlannerUnbound.h"
#include "DlgJobData.h"
#include "DlgPressDevice.h"
#include "GraphicComponent.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"



#define ID_PRESSDEVICE_DROPDOWN		90001
#define ID_PRINTTYPE_COMBO			90002
#define ID_PRINTSHEET_DROPDOWN		90003
#define ID_PRINTLAYOUT_DROPDOWN		90007
#define ID_FUNCTIONS_DROPDOWN		90008


// CUnboundComponentsListCtrl

IMPLEMENT_DYNAMIC(CUnboundComponentsListCtrl, CExtendedListCtrl)

CUnboundComponentsListCtrl::CUnboundComponentsListCtrl()
{
	m_bkBrush.CreateSolidBrush(BLACK);
	m_nHighlight = HIGHLIGHT_ROW;	
	m_nPrevSel	 = -1;
}

CUnboundComponentsListCtrl::~CUnboundComponentsListCtrl()
{
	m_bkBrush.DeleteObject();
}


void CUnboundComponentsListCtrl::Initialize()
{
	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	if (pDlg->m_bMagnify)
		ListView_SetExtendedListViewStyle(m_hWnd, LVS_EX_FULLROWSELECT);
	else
		ListView_SetExtendedListViewStyle(m_hWnd, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	CString string;
	int nIndex = 0;
	string = _T("Druckmaschine");
	InsertColumn(nIndex++,  string, LVCFMT_LEFT,  150, 0,	InplaceDropDown, 5, ID_PRESSDEVICE_DROPDOWN);

	string = _T("Druckart");
	InsertColumn(nIndex++,  string, LVCFMT_LEFT,  140, 0,	InplaceComboBox, 5, ID_PRINTTYPE_COMBO);

	string = _T("Papier");
	InsertColumn(nIndex++,  string, LVCFMT_LEFT,  120, 0,	InplaceDropDown, 5, ID_PRINTSHEET_DROPDOWN);

	string = _T("Standbogen");//.LoadString(IDS_PRINTSHEETLAYOUT);
	InsertColumn(nIndex++,  string, LVCFMT_LEFT,  180, 0);//	InplaceDropDown, 5, ID_PRINTLAYOUT_DROPDOWN);

	string = _T("Funktionen");
	InsertColumn(nIndex++,  string, LVCFMT_LEFT,  120, 0,	InplaceComboBox, 5, ID_FUNCTIONS_DROPDOWN);
}

void CUnboundComponentsListCtrl::InitInplaceControl(int iItem, int iSubItem, int nCtrlType, CWnd* pCtrl)
{
	CString strText;
	LV_ITEM lvitem;
	lvitem.mask		  = LVIF_TEXT | LVIF_PARAM;
	lvitem.stateMask  = 0xFFFF;
	lvitem.iItem	  = iItem;
	lvitem.iSubItem   = iSubItem;
	lvitem.pszText	  = strText.GetBuffer(MAX_TEXT);
	lvitem.cchTextMax = MAX_TEXT;
	GetItem(&lvitem);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	CPrintSheet* pBlankPrintSheet = NULL;//(pDoc) ? pDoc->m_PrintSheetList.GetBlankSheet() : NULL;
	CLayout*	 pBlankLayout	  = NULL;//(pBlankPrintSheet) ? pBlankPrintSheet->GetLayout() : NULL;

	switch (iSubItem)
	{
	case 0:		{
					CPrintSheet* pPrintSheet = (CPrintSheet*)lvitem.lParam;
					CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
					CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
					pCtrl->SetWindowText( (pPressDevice) ? pPressDevice->m_strName : _T("W�hlen ..."));
				}
				break;

	case 1:		{
					CString str1 = _T("S+W"), str2 = _T("Z. Umschlagen"), str3 = _T("Z. Umst�lpen");
					((CComboBox*)pCtrl)->ResetContent();
					((CComboBox*)pCtrl)->AddString(str1);
					((CComboBox*)pCtrl)->AddString(str2);
					((CComboBox*)pCtrl)->AddString(str3);
					CPrintSheet* pPrintSheet = (CPrintSheet*)lvitem.lParam;
					CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
					if ( ! pLayout)
						break;

					switch (pLayout->m_nProductType)
					{
					case FALSE:				((CComboBox*)pCtrl)->SelectString(-1, str1);	break;
					case WORK_AND_TURN:		((CComboBox*)pCtrl)->SelectString(-1, str2);	break;
					case WORK_AND_TUMBLE:	((CComboBox*)pCtrl)->SelectString(-1, str3);	break;
					}
				}
				break;

	case 2:		{
					CPrintSheet* pPrintSheet = (CPrintSheet*)lvitem.lParam;
					CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
					pCtrl->SetWindowText( (pLayout) ? pLayout->m_FrontSide.m_strFormatName : _T(""));
				}
				break;

	//case 3:		{
	//				CPrintSheet* pPrintSheet = (CPrintSheet*)lvitem.lParam;
	//				CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	//				pCtrl->SetWindowText( (pLayout) ? pLayout->m_strLayoutName : _T(""));
	//			}
	//			break;

	case 4:		{
					((CComboBox*)pCtrl)->ResetContent();
					//((CComboBox*)pCtrl)->AddString(_T("Druckmarken"));
					//((CComboBox*)pCtrl)->AddString(_T("Ausgabeprofil"));
					((CComboBox*)pCtrl)->AddString(_T("L�schen"));
					((CComboBox*)pCtrl)->SelectString(-1, _T("Druckmarken"));
				}
				break;

	default:	break;
	}
}




BEGIN_MESSAGE_MAP(CUnboundComponentsListCtrl, CExtendedListCtrl)
	ON_WM_MEASUREITEM_REFLECT()
	ON_BN_CLICKED(ID_PRESSDEVICE_DROPDOWN, &CUnboundComponentsListCtrl::OnInplacePressDeviceDropDown)
	ON_CBN_SELCHANGE(ID_PRINTTYPE_COMBO, &CUnboundComponentsListCtrl::OnInplacePrintingTypeSelChange)
	ON_BN_CLICKED(ID_PRINTSHEET_DROPDOWN, &CUnboundComponentsListCtrl::OnInplacePrintSheetDropDown)
	ON_BN_CLICKED(ID_PRINTLAYOUT_DROPDOWN, &CUnboundComponentsListCtrl::OnInplacePrintLayoutDropDown)
	ON_CBN_SELCHANGE(ID_FUNCTIONS_DROPDOWN, &CUnboundComponentsListCtrl::OnInplaceFunctionsSelChange)
END_MESSAGE_MAP()


// CExtendedListCtrl-Meldungshandler


void CUnboundComponentsListCtrl::OnInplacePressDeviceDropDown()
{
	CRect rcButton, rcDlg, rcList, rcWin;
	if ( ! GetInplaceControl(ID_PRESSDEVICE_DROPDOWN))
		return;
	GetInplaceControl(ID_PRESSDEVICE_DROPDOWN)->GetWindowRect(rcButton);
	GetWindowRect(rcList);
	rcList.DeflateRect(2,2);

	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	pDlg->m_dlgPressDeviceSelector.m_pParent			 = this;
	pDlg->m_dlgPressDeviceSelector.m_pPressDevice 		 = pDlg->m_pPressDevice;
	pDlg->m_dlgPressDeviceSelector.m_pLayout	  		 = pDlg->m_pLayout;
	pDlg->m_dlgPressDeviceSelector.m_pFnSelChangeHandler = &OnSelchangePressDeviceSelector;
	if ( ! pDlg->m_dlgPressDeviceSelector.m_hWnd)
	{
		pDlg->m_dlgPressDeviceSelector.Create(IDD_SELECT_PRESSDEVICE_POPUP, pDlg);
		pDlg->m_dlgPressDeviceSelector.GetWindowRect(rcWin);
		pDlg->m_dlgPressDeviceSelector.MoveWindow(rcButton.left, rcButton.bottom, rcWin.Width(), rcWin.Height());
		pDlg->m_dlgPressDeviceSelector.ShowWindow(SW_SHOW);
	}
	else
	{
		pDlg->m_dlgPressDeviceSelector.GetWindowRect(rcWin);
		pDlg->m_dlgPressDeviceSelector.MoveWindow(rcButton.left, rcButton.bottom, rcWin.Width(), rcWin.Height());
		pDlg->m_dlgPressDeviceSelector.ShowWindow(SW_SHOW);
	}
}

void CUnboundComponentsListCtrl::OnSelchangePressDeviceSelector(CPressDevice* pNewPressDevice, int nPressDeviceIndex, CDlgSelectPressDevicePopup* pDlgSelector)
{
	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)pDlgSelector->m_pParent->GetParent();

	if ( ! pDlg->m_pPressDevice)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return;
		pDoc->m_PrintSheetList.AddBlankSheet(pDlg->m_nProductPartIndex);
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetTail();
		CLayout*	 pLayout	 = rPrintSheet.GetLayout();
		if (! pLayout)
			return;
		pDoc->SetPrintSheetPressDevice(nPressDeviceIndex, BOTHSIDES, pDoc->m_PrintSheetList.m_Layouts.FindLayout(pLayout->m_strLayoutName));
		CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
		if (! pPressDevice)
			return;
		CSheet sheet;
		if ( ! pPressDevice->FindLargestPaper(sheet))
			return;
		pLayout->AssignSheet(sheet);
	}
	else
	{
		if ( ! pNewPressDevice)
			return;
		if ( ! pDlg->m_pPressDevice || ! pDlg->m_pLayout)
			return;

		CPressDevice oldPressDevice;
		oldPressDevice  = *pDlg->m_pPressDevice;
		*pDlg->m_pPressDevice = *pNewPressDevice;
		float fGripper  = CDlgPressDevice::CheckGripper(&oldPressDevice, pDlg->m_pPressDevice, &pDlg->m_pLayout->m_FrontSide);
		pDlg->m_pLayout->m_FrontSide.PositionLayoutSide(fGripper, pDlg->m_pPressDevice);
		pDlg->m_pLayout->m_BackSide.PositionLayoutSide (fGripper, pDlg->m_pPressDevice);
	}

	pDlg->UpdateProductComponentsList();
}

void CUnboundComponentsListCtrl::OnInplacePrintingTypeSelChange()
{
	UpdateData(TRUE);

	int nSel = ((CComboBox*)GetDlgItem(ID_PRINTTYPE_COMBO))->GetCurSel();
	if (nSel == CB_ERR)
		return;

	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	if ( ! pDlg->m_pLayout)
		return;

	switch (nSel)
	{
	case 0: if ( (pDlg->m_pLayout->m_nProductType == WORK_AND_TURN) || (pDlg->m_pLayout->m_nProductType == WORK_AND_TUMBLE) )
			{
				pDlg->m_pLayout->m_bNOCheckForProductType = TRUE;
				pDlg->m_pLayout->CheckForProductType();
				pDlg->m_pLayout->m_nProductType = FALSE;
			}
			break;

	case 1: 
	case 2:
			pDlg->m_pLayout->m_bNOCheckForProductType = FALSE;
			pDlg->m_pLayout->CheckForProductType();
			pDlg->m_pLayout->m_nProductType = (nSel == 1) ? WORK_AND_TURN : WORK_AND_TUMBLE;
			break;
	}
	pDlg->UpdateProductComponentsList();
}

void CUnboundComponentsListCtrl::OnInplacePrintSheetDropDown()
{
	CRect rcButton, rcDlg, rcList, rcWin;
	if ( ! GetInplaceControl(ID_PRINTSHEET_DROPDOWN))
		return;
	GetInplaceControl(ID_PRINTSHEET_DROPDOWN)->GetWindowRect(rcButton);
	GetWindowRect(rcList);
	rcList.DeflateRect(2,2);

	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	pDlg->m_dlgSheetSelector.m_pParent		= this;
	pDlg->m_dlgSheetSelector.m_pPrintSheet  = pDlg->m_pPrintSheet;
	pDlg->m_dlgSheetSelector.m_pLayout		= pDlg->m_pLayout;
	pDlg->m_dlgSheetSelector.m_pPressDevice = pDlg->m_pPressDevice;
	pDlg->m_dlgSheetSelector.m_pFnSelChangeHandler = &OnSelchangeSheetSelector;
	if ( ! pDlg->m_dlgSheetSelector.m_hWnd)
	{
		pDlg->m_dlgSheetSelector.Create(IDD_SELECT_SHEET_POPUP, pDlg);
		pDlg->m_dlgSheetSelector.GetWindowRect(rcWin);
		pDlg->m_dlgSheetSelector.MoveWindow(rcButton.left, rcButton.bottom, rcWin.Width(), rcWin.Height());
		pDlg->m_dlgSheetSelector.ShowWindow(SW_SHOW);
	}
	else
	{
		pDlg->m_dlgSheetSelector.GetWindowRect(rcWin);
		pDlg->m_dlgSheetSelector.MoveWindow(rcButton.left, rcButton.bottom, rcWin.Width(), rcWin.Height());
		pDlg->m_dlgSheetSelector.ShowWindow(SW_SHOW);
	}
}

void CUnboundComponentsListCtrl::OnSelchangeSheetSelector(CSheet* pSheet, CDlgSelectSheetPopup* pDlgSelector)
{
	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)pDlgSelector->m_pParent->GetParent();

	pDlg->m_pLayout->AssignSheet(*pSheet);

	pDlg->UpdateProductComponentsList();
}

void CUnboundComponentsListCtrl::OnInplacePrintLayoutDropDown()
{
	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();

	CDlgJobData dlgJobData(IDD_PRODDATA_PRINT, (ULONG_PTR)((pDlg->m_pLayout) ? pDlg->m_pLayout->GetFirstPrintSheet() : NULL));
	
	dlgJobData.DoModal();
}

void CUnboundComponentsListCtrl::OnInplaceFunctionsSelChange()
{
	int nSel = ((CComboBox*)GetDlgItem(ID_FUNCTIONS_DROPDOWN))->GetCurSel();
	switch (nSel)
	{
	case 0:		FunctionRemove();	
				break;

	default:	break;
	}
}

void CUnboundComponentsListCtrl::FunctionRemove()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	if ( ! pDlg->m_pPrintSheet)
		return;
	CString strMsg;
	strMsg.Format(_T("Wollen Sie den gesamten Druckbogen wirklich l�schen"));
	if (AfxMessageBox(strMsg, MB_YESNO) == IDNO)
		return;

	pDoc->m_PrintSheetList.RemoveAt(pDlg->m_pPrintSheet);

	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations(pDlg->m_nProductPartIndex);
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

	pDoc->m_MarkSourceList.RemoveUnusedSources();
	pDoc->SetModifiedFlag();										

	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),	NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetTableView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),			NULL, 0, NULL);	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));

	pDlg->InitData();
}

// CUnboundComponentsListCtrl-Meldungshandler

void CUnboundComponentsListCtrl::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();

	CRect rcClient;
	GetClientRect(rcClient);
	if (pDlg->m_bMagnify)
	{
		lpMeasureItemStruct->itemHeight = rcClient.Height() - 20;
	}
	else
	{
		int nHeight = (GetItemCount()) ? (rcClient.Height() - 20) / GetItemCount() : 80;
		nHeight = min(nHeight, 160);
		nHeight = max(nHeight, 80);
		lpMeasureItemStruct->itemHeight = nHeight;
	}
}


#define TITLECOLOR_L_1 WHITE//RGB(255,250,200)
#define TITLECOLOR_L_2 RGB(240,240,240)//RGB(255,240,180)
#define TITLECOLOR_H_1 WHITE//RGB(255,250,200)
#define TITLECOLOR_H_2 RGB(190,210,250)//RGB(255,240,180)
#define TITLEBAR (bHighlight) ? TITLECOLOR_H_1 : TITLECOLOR_L_2, (bHighlight) ? TITLECOLOR_H_2 : TITLECOLOR_L_1, 90, -1
int const g_nTitleHeight = 18;

void CUnboundComponentsListCtrl::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
	int nItem = lpDrawItemStruct->itemID;

	// Save dc state
	int nSavedDC = pDC->SaveDC();

	CFont font;
	font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);

	// Get item image and state info
	LV_ITEM lvi;
	lvi.mask = LVIF_STATE | LVIF_PARAM;
	lvi.iItem = nItem;
	lvi.iSubItem = 0;
	lvi.stateMask = 0xFFFF;		// get all state flags
	GetItem(&lvi);

	// Should the item be highlighted
	BOOL bHighlight = ((lvi.state & LVIS_DROPHILITED) || ( (lvi.state & LVIS_SELECTED) && ((GetFocus() == this))) );
//	BOOL bHighlight = ( (lvi.state & LVIS_SELECTED) && (GetFocus() == this) );

	if (GetStyle() & LVS_SHOWSELALWAYS)
	{
		POSITION pos = GetFirstSelectedItemPosition();
		if (GetNextSelectedItem(pos) == lvi.iItem)
			bHighlight = TRUE;
	}

	// Get rectangles for drawing
	CRect rcBounds, rcLabel, rcIcon;
	GetItemRect(nItem, rcBounds, LVIR_BOUNDS);
	GetItemRect(nItem, rcLabel, LVIR_LABEL);
	GetItemRect(nItem, rcIcon, LVIR_ICON);
	CRect rcCol( rcBounds );

	CString sLabel = GetItemText( nItem, 0 );

	// Labels are offset by a certain amount  
	// This offset is related to the width of a space character
	int offset = pDC->GetTextExtent(_T(" "), 1 ).cx*2;

	CRect rcHighlight;
	CRect rcWnd;
	GetClientRect(&rcWnd);
	int nExt;
	switch( m_nHighlight )
	{
	case 0:
		nExt = pDC->GetOutputTextExtent(sLabel).cx + offset;
		rcHighlight = rcLabel;
		if( rcLabel.left + nExt < rcLabel.right )
			rcHighlight.right = rcLabel.left + nExt;
		break;
	case 1:
		rcHighlight = rcBounds;
		rcHighlight.left = rcLabel.left;
		break;
	case 2:
		rcHighlight = rcBounds;
//		rcHighlight.left = rcLabel.left;
//		rcHighlight.right = rcWnd.right;
		break;
	default:
		rcHighlight = rcLabel;
	}

	CPrintSheet* pPrintSheet = (CPrintSheet*)lvi.lParam;
	CLayout*	 pLayout	 = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	//if ( ! pLayout)
	//{
	//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//	if ( pDoc)
	//	{
	//		CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.GetBlankSheet();
	//		pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	//	}
	//}

	// Draw the background color
	if( bHighlight )
	{
		pDC->SetTextColor(DARKBLUE);//::GetSysColor(COLOR_HIGHLIGHTTEXT));
		pDC->SetBkColor(RGB(193,211,251));//::GetSysColor(COLOR_HIGHLIGHT));

		pDC->FillRect(rcHighlight, &CBrush(RGB(193,211,251)));//::GetSysColor(COLOR_HIGHLIGHT)));
	}
	else
	{
		pDC->FillRect(rcHighlight, &CBrush(::GetSysColor(COLOR_WINDOW)));
	}

	// Set clip region
	rcCol.right = rcCol.left + GetColumnWidth(0);
	CRgn rgn;
	rgn.CreateRectRgnIndirect(&rcCol);
	pDC->SelectClipRgn(&rgn);
	rgn.DeleteObject();

	// Draw item label - Column 0
	DrawPressDeviceInfos(pDC, pPrintSheet, pLayout, rcCol, bHighlight);

	// Draw labels for remaining columns
	LV_COLUMN lvc;
	lvc.mask = LVCF_FMT | LVCF_WIDTH;

	if ( m_nHighlight == HIGHLIGHT_NORMAL )		// Highlight only first column
	{
		pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));
		pDC->SetBkColor(::GetSysColor(COLOR_WINDOW));
	}

	rcBounds.right = rcHighlight.right > rcBounds.right ? rcHighlight.right :
							rcBounds.right;
	rgn.CreateRectRgnIndirect(&rcBounds);
	pDC->SelectClipRgn(&rgn);

	for(int nColumn = 1; GetColumn(nColumn, &lvc); nColumn++)
	{
		rcCol.left = rcCol.right;
		rcCol.right += lvc.cx;

		// Draw the background if needed
		if ( m_nHighlight == HIGHLIGHT_NORMAL )
			pDC->FillRect(rcCol, &CBrush(::GetSysColor(COLOR_WINDOW)));

		sLabel = GetItemText(nItem, nColumn);

		CRect rcControl;
		switch (nColumn)
		{
		case 1:		DrawPrintingTypeInfos(pDC, pPrintSheet, pLayout, rcCol, bHighlight);
					break;
		case 2:		DrawSheetInfos(pDC, pPrintSheet, pLayout, rcCol, bHighlight);
					break;
		case 3:		DrawLayoutInfos(pDC, pPrintSheet, pLayout, rcCol, bHighlight);
					break;
		default:	//rcLabel = rcCol;
					//rcLabel.left += 5; rcLabel.top += 5; rcLabel.bottom = rcLabel.top + 15;
					CRect rcTitle = rcCol; rcTitle.bottom = rcTitle.top + g_nTitleHeight;
					CGraphicComponent gp;
					gp.DrawTitleBar(pDC, rcTitle, sLabel, TITLEBAR);
					//pDC->DrawText(sLabel, -1, rcLabel, DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS);
					break;
		}
	}

	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	if (pDlg->m_bMagnify)
	{
		GetColumn(0, &lvc);
		rcBounds.left += lvc.cx;
		rcBounds.top  += 30;
		DrawLayoutMagnified(pDC, pPrintSheet, pLayout, rcBounds, bHighlight);
	}

	font.DeleteObject();
	pDC->RestoreDC( nSavedDC );
}

void CUnboundComponentsListCtrl::DrawPressDeviceInfos(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight)
{
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;

	CRect rcLabel;
	rcLabel = rcCol; 
	rcLabel.left += 5; rcLabel.top += 5; rcLabel.bottom = rcLabel.top + 15;
	CRect rcTitle = rcCol; rcTitle.bottom = rcTitle.top + g_nTitleHeight;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcTitle, (pPressDevice) ? pPressDevice->m_strName : _T("W�hlen ..."), TITLEBAR);

	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	if (pDlg->m_bMagnify)
		return;
	if ( ! pPressDevice)
		return;
	rcLabel.OffsetRect(0, 30);
	CString string = _T("Plattenformat:");
	pDC->DrawText(string, -1, rcLabel, DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS);

	rcLabel.OffsetRect(0, 20);
	string.Format(_T("%.1f x %.1f"), pPressDevice->m_fPlateWidth, pPressDevice->m_fPlateHeight);
	pDC->DrawText(string, -1, rcLabel, DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS);
}

void CUnboundComponentsListCtrl::DrawPrintingTypeInfos(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight)
{
	CRect rcLabel;
	rcLabel = rcCol; 
	rcLabel.left += 5; rcLabel.top += 5; rcLabel.bottom = rcLabel.top + 15;
	CRect rcTitle = rcCol; rcTitle.bottom = rcTitle.top + g_nTitleHeight;
	CString strOut = _T("");
	if (pLayout)
	{
		switch (pLayout->m_nProductType)
		{
		case FALSE:				strOut = _T("S+W");			break;
		case WORK_AND_TURN:		strOut = _T("Z. Umschlagen");	break;
		case WORK_AND_TUMBLE:	strOut = _T("Z. Umst�lpen");	break;
		}
	}
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcTitle, strOut, TITLEBAR);

	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	if (pDlg->m_bMagnify)
		return;
	rcLabel.OffsetRect(0, 30);
//	pDC->DrawText(string, -1, rcLabel, DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS);
}

void CUnboundComponentsListCtrl::DrawSheetInfos(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight)
{
	CRect rcLabel;
	rcLabel = rcCol; 
	rcLabel.left += 5; rcLabel.top += 5; rcLabel.bottom = rcLabel.top + 15;
	CRect rcTitle = rcCol; rcTitle.bottom = rcTitle.top + g_nTitleHeight;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcTitle, (pLayout) ? pLayout->m_FrontSide.m_strFormatName : _T(""), TITLEBAR);

	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	if (pDlg->m_bMagnify)
		return;
	if ( ! pLayout)
		return;
	if ((abs(pLayout->m_FrontSide.m_fPaperWidth) < 0.001f) || (abs(pLayout->m_FrontSide.m_fPaperWidth) < 0.001f))
		return;

	rcLabel.OffsetRect(0, 30);
	CString string = _T("Format:");
	pDC->DrawText(string, -1, rcLabel, DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS);

	rcLabel.OffsetRect(0, 20);
	string.Format(_T("%.1f x %.1f"), pLayout->m_FrontSide.m_fPaperWidth, pLayout->m_FrontSide.m_fPaperHeight);
	pDC->DrawText(string, -1, rcLabel, DT_SINGLELINE | DT_NOPREFIX | DT_VCENTER | DT_END_ELLIPSIS);
}

void CUnboundComponentsListCtrl::DrawLayoutInfos(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight)
{
	CRect rcIcon, rcLabel;
	rcLabel = rcCol; 
	rcLabel.left += 5; rcLabel.top += 5; rcLabel.bottom = rcLabel.top + 15;
	CRect rcTitle = rcCol; rcTitle.bottom = rcTitle.top + g_nTitleHeight;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcTitle, (pLayout) ? pLayout->m_strLayoutName : _T(""), TITLEBAR);

	CDlgProductionPlannerUnbound* pDlg = (CDlgProductionPlannerUnbound*)GetParent();
	if (pDlg->m_bMagnify)
		return;

	rcIcon = rcCol;
	rcIcon.top += 20; rcIcon.bottom -= 5;
	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	if (pLayout)
		pLayout->Draw(pDC, rcIcon, pPrintSheet, pPressDevice, TRUE, FALSE, FALSE, NULL, TRUE);
}

void CUnboundComponentsListCtrl::DrawLayoutMagnified(CDC* pDC, CPrintSheet* pPrintSheet, CLayout* pLayout, CRect rcCol, BOOL bHighlight)
{
	if ( ! pLayout)
		return;

	CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	pLayout->Draw(pDC, rcCol, pPrintSheet, pPressDevice, TRUE, FALSE, TRUE, NULL, TRUE, BOTHSIDES);
}





// CDlgProductionPlannerUnbound-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionPlannerUnbound, CDialog)

CDlgProductionPlannerUnbound::CDlgProductionPlannerUnbound(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionPlannerUnbound::IDD, pParent)
{
	m_nProductPartIndex = 0;
	m_pPrintSheet		= NULL;
	m_pPressDevice		= NULL;
	m_pLayout			= NULL;
}

CDlgProductionPlannerUnbound::~CDlgProductionPlannerUnbound()
{
}

BOOL CDlgProductionPlannerUnbound::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;

	//return CDialog::OnEraseBkgnd(pDC);
}

void CDlgProductionPlannerUnbound::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgProductionPlannerUnbound, CDialog)
	ON_WM_ERASEBKGND()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_UNBOUND_PRODUCT_COMPONENTS_LIST, &CDlgProductionPlannerUnbound::OnLvnItemchangedUnboundProductComponentsList)
	ON_BN_CLICKED(IDC_PP_MAGNIFY_BUTTON, &CDlgProductionPlannerUnbound::OnBnClickedPpMagnifyButton)
	ON_WM_DRAWITEM()
END_MESSAGE_MAP()


// CDlgProductionPlannerUnbound-Meldungshandler

BOOL CDlgProductionPlannerUnbound::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_componentsList.SubclassDlgItem(IDC_UNBOUND_PRODUCT_COMPONENTS_LIST, this);
	m_componentsList.Initialize();

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}




void CDlgProductionPlannerUnbound::InitData()
{
	UpdateProductComponentsList();

	m_pPrintSheet	= GetSelectedPrintSheet();
	m_pLayout		= (m_pPrintSheet) ? m_pPrintSheet->GetLayout()				 : NULL;
	m_pPressDevice	= (m_pLayout)	  ? m_pLayout->m_FrontSide.GetPressDevice()  : NULL;
}

void CDlgProductionPlannerUnbound::SaveData()
{
}
void CDlgProductionPlannerUnbound::UpdateProductComponentsList()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (m_bMagnify)
		ListView_SetExtendedListViewStyle(m_componentsList.m_hWnd, LVS_EX_FULLROWSELECT);
	else
		ListView_SetExtendedListViewStyle(m_componentsList.m_hWnd, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);

	POSITION pos = m_componentsList.GetFirstSelectedItemPosition();
	int nCurSel = (pos) ? m_componentsList.GetNextSelectedItem(pos) : 0;

	m_componentsList.DeleteAllItems();

	if ( (m_nProductPartIndex < 0) || (m_nProductPartIndex >= pDoc->m_productPartList.GetSize()) )
		return;
	CProductPart& rProductPart = pDoc->m_productPartList[m_nProductPartIndex];

 // Cause WM_MEASUREITEM to be sent by pretending to move CListCtrls window
	CRect rc;
	m_componentsList.GetWindowRect( &rc );
	WINDOWPOS wp;
	wp.hwnd = m_hWnd;
	wp.cx = rc.Width();
	wp.cy = rc.Height();
	wp.flags = SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER;
	m_componentsList.SendMessage(WM_WINDOWPOSCHANGED, 0, (LPARAM)&wp);

	LVITEM   lvitem;
	int		 nSubItem = 0;
	CString  string, string1, strMeasureFormat;
	CPrintSheet* pPrintSheet = rProductPart.GetFirstPrintSheet();
	while (pPrintSheet)
	{
		CLayout*	  pLayout	   = pPrintSheet->GetLayout();
		CPressDevice* pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
		if ( ! pLayout || ! pPressDevice)
			continue;

		nSubItem = 0;
		string = pPressDevice->m_strName;
		lvitem.iItem	= m_componentsList.GetItemCount();
		lvitem.iSubItem = nSubItem++;
		lvitem.mask	    = LVIF_TEXT | LVIF_PARAM;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		lvitem.lParam	= (LPARAM)pPrintSheet;
		if (nCurSel == lvitem.iItem)
			m_componentsList.m_nPrevSel = lvitem.iItem;
		m_componentsList.InsertItem(&lvitem); 

		string = _T("");
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_componentsList.SetItem(&lvitem); 

		string = pLayout->m_FrontSide.m_strFormatName;
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_componentsList.SetItem(&lvitem); 

		string.Format(_T("%s"), (pLayout) ? pLayout->m_strLayoutName : "");
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_componentsList.SetItem(&lvitem); 

		string = _T("Druckmarken");
		lvitem.iSubItem = nSubItem++;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		m_componentsList.SetItem(&lvitem); 

		pPrintSheet = rProductPart.GetNextPrintSheet(pPrintSheet);
	}

	nSubItem = 0;
	string = _T("");
	lvitem.iItem	= m_componentsList.GetItemCount();
	lvitem.iSubItem = nSubItem++;
	lvitem.mask	    = LVIF_TEXT | LVIF_PARAM;
	lvitem.pszText  = string.GetBuffer(MAX_TEXT);;
	lvitem.lParam	= 0;
	m_componentsList.InsertItem(&lvitem); 

	if (nCurSel >= 0)
	{
		m_componentsList.SetItemState(nCurSel, LVIS_SELECTED, LVIS_SELECTED);
		m_componentsList.EnsureVisible(nCurSel, FALSE);
	}

 // Cause WM_MEASUREITEM to be sent by pretending to move CListCtrls window
	m_componentsList.GetWindowRect( &rc );
	wp.hwnd = m_hWnd;
	wp.cx = rc.Width();
	wp.cy = rc.Height();
	wp.flags = SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER;
	m_componentsList.SendMessage(WM_WINDOWPOSCHANGED, 0, (LPARAM)&wp);

}

void CDlgProductionPlannerUnbound::OnLvnItemchangedUnboundProductComponentsList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	if ( ! (pNMLV->uNewState & LVNI_SELECTED) )
		return;

	CPrintSheet* pPrintSheet = NULL;

	if ( (pNMLV->iItem >= 0) && (pNMLV->iSubItem >= 0) )
		pPrintSheet = (CPrintSheet*)pNMLV->lParam;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_pPrintSheet = pPrintSheet;
	if ( ! pPrintSheet)
	{
		m_pPrintSheet	= NULL;//(pDoc) ? pDoc->m_PrintSheetList.GetBlankSheet() : NULL;
		m_pLayout		= (m_pPrintSheet) ? m_pPrintSheet->GetLayout() : NULL;
		m_pPressDevice	= (m_pLayout) ? m_pLayout->m_FrontSide.GetPressDevice() : NULL;
	}
	else
	{
		m_pLayout			= pPrintSheet->GetLayout();
		m_pPressDevice		= (m_pLayout) ? m_pLayout->m_FrontSide.GetPressDevice() : NULL;
	}
}

CPrintSheet* CDlgProductionPlannerUnbound::GetSelectedPrintSheet()
{
	POSITION pos = m_componentsList.GetFirstSelectedItemPosition();
	if ( ! pos)
		return NULL;
	int nCurSel = m_componentsList.GetNextSelectedItem(pos);
	if (nCurSel < 0)
		return NULL;
	return (CPrintSheet*)m_componentsList.GetItemData(nCurSel);
}

void CDlgProductionPlannerUnbound::OnBnClickedPpMagnifyButton()
{
	m_bMagnify = ! m_bMagnify;

	UpdateProductComponentsList();
}

void CDlgProductionPlannerUnbound::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType == ODT_BUTTON)
	{
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		CRect rcItem = lpDrawItemStruct->rcItem;

#ifdef UNICODE
		HTHEME hThemeButton = theApp._OpenThemeData(this, _T("BUTTON"));
#else
		HTHEME hThemeButton = theApp._OpenThemeData(this, CA2W("BUTTON"));
#endif

		if (hThemeButton)
		{
			if (m_bMagnify)
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), BP_PUSHBUTTON, PBS_PRESSED,	&rcItem, 0);	
			else
				DrawThemeBackgroundEx(hThemeButton, pDC->GetSafeHdc(), BP_PUSHBUTTON, PBS_NORMAL,	&rcItem, 0);	
			theApp._CloseThemeData(hThemeButton);
		}
		else
		{
			if (m_bMagnify)
				DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);	
			else
				DrawFrameControl(pDC->GetSafeHdc(), &rcItem, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_TRANSPARENT);	
		}

		CImage img;
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MAGNIFY));
		img.TransparentBlt(pDC->m_hDC, rcItem.left + 5, rcItem.top + 5, img.GetWidth(), img.GetHeight(), WHITE);
	}

	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
