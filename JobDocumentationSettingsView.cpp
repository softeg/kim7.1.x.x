// JobDocumentationSettingsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "JobDocumentationView.h"
#include "JobDocumentationSettingsView.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PrintJobDocumentation.h"


// CJobDocumentationSettingsView

IMPLEMENT_DYNCREATE(CJobDocumentationSettingsView, CFormView)

CJobDocumentationSettingsView::CJobDocumentationSettingsView()
	: CFormView(CJobDocumentationSettingsView::IDD)
{
	m_nShowSide		  = BOTHSIDES;
	m_nZoomState	  = ZoomMultiPage;
	m_bShowFoldSheet  = FALSE;
	m_bShowMeasure	  = TRUE;
	m_bShowPlate	  = FALSE;
	m_bShowBitmap	  = FALSE;
	m_bShowExposures  = FALSE;
	m_bShowCutBlocks  = FALSE;
	m_bShowBoxShapes  = FALSE;
	m_nSheetZoomLevel = 4;
}

CJobDocumentationSettingsView::~CJobDocumentationSettingsView()
{
}

void CJobDocumentationSettingsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_SHEETZOOMLEVEL_EDIT, m_nSheetZoomLevel);
	DDV_MinMaxInt(pDX, m_nSheetZoomLevel, 1, 10);
	DDX_Control(pDX, IDC_SHEETZOOMLEVEL_SPIN, m_sheetZoomLevelSpin);
}

BEGIN_MESSAGE_MAP(CJobDocumentationSettingsView, CFormView)
	ON_BN_CLICKED(IDC_JOBDOC_PRINT_BUTTON, &CJobDocumentationSettingsView::OnBnClickedJobdocPrintButton)
	ON_BN_CLICKED(IDC_JOBDOC_PAGESETUP_BUTTON, &CJobDocumentationSettingsView::OnBnClickedJobdocPagesetupButton)
	ON_COMMAND(ID_VIEW_FRONTSIDE, OnViewFrontside)
	ON_COMMAND(ID_VIEW_BACKSIDE, OnViewBackside)
	ON_COMMAND(ID_VIEW_BOTHSIDES, OnViewBothsides)
	ON_COMMAND(IDC_FOLDSHEETCHECK, OnFoldsheetCheck)
	ON_COMMAND(IDC_MEASURECHECK, OnMeasureCheck)
	ON_COMMAND(IDC_PLATECHECK, OnPlateCheck)
	ON_COMMAND(IDC_BITMAPCHECK, OnBitmapCheck)
	ON_COMMAND(IDC_EXPOSURECHECK, OnExposureCheck)
	ON_COMMAND(ID_SHOW_CUTBLOCKS, OnShowCutBlocks)
	ON_COMMAND(ID_SHOW_BOXSHAPES, OnShowBoxshapes)
	ON_COMMAND(ID_JOBDOC_ZOOM_MULTIPAGE, OnZoomMultipage)
	ON_COMMAND(ID_JOBDOC_ZOOM_PAGEFIT, OnZoomPageFit)
	ON_COMMAND(ID_JOBDOC_ZOOM_PAGEWIDTH, OnZoomPageWidth)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FRONTSIDE, OnUpdateViewFrontside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BACKSIDE, OnUpdateViewBackside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BOTHSIDES, OnUpdateViewBothsides)
	ON_UPDATE_COMMAND_UI(IDC_FOLDSHEETCHECK, OnUpdateFoldsheetCheck)
	ON_UPDATE_COMMAND_UI(IDC_MEASURECHECK, OnUpdateMeasureCheck)
	ON_UPDATE_COMMAND_UI(IDC_PLATECHECK, OnUpdatePlateCheck)
	ON_UPDATE_COMMAND_UI(IDC_BITMAPCHECK, OnUpdateBitmapCheck)
	ON_UPDATE_COMMAND_UI(IDC_EXPOSURECHECK, OnUpdateExposureCheck)
	ON_UPDATE_COMMAND_UI(ID_SHOW_CUTBLOCKS, OnUpdateShowCutBlocks)
	ON_UPDATE_COMMAND_UI(ID_SHOW_BOXSHAPES, OnUpdateShowBoxshapes)
	ON_UPDATE_COMMAND_UI(ID_JOBDOC_ZOOM_MULTIPAGE, OnUpdateZoomMultipage)
	ON_UPDATE_COMMAND_UI(ID_JOBDOC_ZOOM_PAGEFIT, OnUpdateZoomPageFit)
	ON_UPDATE_COMMAND_UI(ID_JOBDOC_ZOOM_PAGEWIDTH, OnUpdateZoomPageWidth)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SHEETZOOMLEVEL_SPIN, &CJobDocumentationSettingsView::OnDeltaposSheetZoomLevelSpin)
	ON_EN_KILLFOCUS(IDC_SHEETZOOMLEVEL_EDIT, &CJobDocumentationSettingsView::OnEnKillfocusSheetZoomLevelEdit)
END_MESSAGE_MAP()


// CJobDocumentationSettingsView-Diagnose

#ifdef _DEBUG
void CJobDocumentationSettingsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CJobDocumentationSettingsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CJobDocumentationSettingsView-Meldungshandler

BOOL CJobDocumentationSettingsView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return DoPreparePrinting(pInfo);

//	return CFormView::OnPreparePrinting(pInfo);
}

void CJobDocumentationSettingsView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	m_toolbar1Buttons.Create(this, GetParentFrame(), NULL, IDB_PRINTVIEW_TOOLBAR2, IDB_PRINTVIEW_TOOLBAR2_COLD, CSize(40, 20));
	CRect rcButton; 
	GetDlgItem(IDC_JOBDOC_SHOWSIDETOOLBAR_FRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton); rcButton.right = rcButton.left + 100;
	CString strFront, strBack;
	AfxFormatString1(strFront, IDS_FRONT, theApp.settings.m_szFrontName);
	AfxFormatString1(strBack,  IDS_BACK,  theApp.settings.m_szBackName);
	m_toolbar1Buttons.AddButton(_T(""),	strFront,			ID_VIEW_FRONTSIDE,		rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_toolbar1Buttons.AddButton(_T(""),	strBack,			ID_VIEW_BACKSIDE,		rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_toolbar1Buttons.AddButton(-1,		IDS_BOTH,			ID_VIEW_BOTHSIDES,		rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);

	m_toolbar2Buttons.Create(this, GetParentFrame(), NULL, IDB_PRINTVIEW_TOOLBAR3, IDB_PRINTVIEW_TOOLBAR3_COLD, CSize(30, 20));
	GetDlgItem(IDC_JOBDOC_TOOLBAR_FRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton); rcButton.right = rcButton.left + 100;
	m_toolbar2Buttons.AddButton(-1, 	IDS_SHOW_FOLDSHEETS,	IDC_FOLDSHEETCHECK, 	rcButton, 0);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_toolbar2Buttons.AddButton(-1, 	IDS_SHOW_MEASURING,		IDC_MEASURECHECK,		rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_toolbar2Buttons.AddButton(-1, 	IDS_SHOW_PLATE,			IDC_PLATECHECK,			rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_toolbar2Buttons.AddButton(-1, 	IDS_SHOW_PREVIEWS,		IDC_BITMAPCHECK,		rcButton, 3);	rcButton.OffsetRect(rcButton.Width(), 0);
	//m_toolbar2Buttons.AddButton(-1, 	IDS_SHOW_CUTBLOCKS,		ID_SHOW_CUTBLOCKS,		rcButton, 5);	rcButton.OffsetRect(rcButton.Width(), 0);	
	//m_toolbar2Buttons.AddButton(-1, 	IDS_SHOW_BOXSHAPES,		ID_SHOW_BOXSHAPES,		rcButton, 6);	

	m_toolbar3Buttons.Create(this, GetParentFrame(), NULL, IDB_JOBDOC_ZOOMSTATE_TOOLBAR, IDB_JOBDOC_ZOOMSTATE_TOOLBAR, CSize(30, 28));
	GetDlgItem(IDC_JOBDOC_ZOOMTOOLBAR_FRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton); rcButton.right = rcButton.left + 100;
	m_toolbar3Buttons.AddButton(-1, 	IDS_JOBDOC_ZOOM_MULTIPAGE,	ID_JOBDOC_ZOOM_MULTIPAGE, 	rcButton, 0);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_toolbar3Buttons.AddButton(-1, 	IDS_JOBDOC_ZOOM_PAGEFIT,	ID_JOBDOC_ZOOM_PAGEFIT,		rcButton, 1);	rcButton.OffsetRect(rcButton.Width(), 0);
	m_toolbar3Buttons.AddButton(-1, 	IDS_JOBDOC_ZOOM_PAGEWIDTH,	ID_JOBDOC_ZOOM_PAGEWIDTH,	rcButton, 2);	rcButton.OffsetRect(rcButton.Width(), 0);

	m_sheetZoomLevelSpin.SetRange(0,1);
}

void CJobDocumentationSettingsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	TCHAR szValue[100];
	DWORD dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("JobTicketSheetZoomLevel"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_stscanf(szValue, _T("%d"), &m_nSheetZoomLevel);
	else
		m_nSheetZoomLevel = 4;
	RegCloseKey(hKey);

	UpdateData(FALSE);
}

void CJobDocumentationSettingsView::OnBnClickedJobdocPrintButton()
{
	CJobDocumentationView* pView = (CJobDocumentationView*)theApp.GetView(RUNTIME_CLASS(CJobDocumentationView));
	BOOL bSelected = (pView) ? ( (pView->m_DisplayList.GetFirstSelectedItem()) ? TRUE : FALSE) : FALSE;
	int nFlags = (bSelected) ? PD_SELECTION : 0;

	CPrintDialog printDlg(FALSE, nFlags | PD_USEDEVMODECOPIES | PD_HIDEPRINTTOFILE);
	printDlg.m_pd.nMinPage = printDlg.m_pd.nFromPage = 1;
	printDlg.m_pd.nMaxPage = printDlg.m_pd.nToPage   = (pView) ? pView->m_nNumPages : 1;

	HDC hDC = NULL;
	if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
	{
		HKEY hKey;
		CString strKey = theApp.m_strRegistryKey;
		RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

		HGLOBAL  hDevMode = GlobalAlloc(GHND, sizeof(DEVMODE));
		DEVMODE* pDevMode = (DEVMODE*)::GlobalLock(hDevMode);
		DWORD dwSize = sizeof(DEVMODE);
		if (RegQueryValueEx(hKey, _T("PrintDevMode"), 0, NULL, (LPBYTE)pDevMode, &dwSize) == ERROR_SUCCESS)
			printDlg.m_pd.hDevMode = hDevMode;

		if (printDlg.DoModal() == IDCANCEL)
		{
			RegCloseKey(hKey);
			::GlobalUnlock(hDevMode);
			return;
		}
		::GlobalUnlock(hDevMode);

		hDC = printDlg.GetPrinterDC();

		theApp.SelectPrinter(printDlg.m_pd.hDevNames, printDlg.m_pd.hDevMode);

		pDevMode = (DEVMODE*)::GlobalLock(printDlg.m_pd.hDevMode);
		RegSetValueEx(hKey, _T("PrintDevMode"), 0, REG_BINARY, (LPBYTE)pDevMode, sizeof(DEVMODE));
		::GlobalUnlock(printDlg.m_pd.hDevMode);
		RegCloseKey(hKey);
	}
	else
		hDC = CreateDC(NULL, "Adobe PDF", NULL, NULL);
	
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));

	CPrintJobDocumentation printJob;
	printJob.Print(hDC, &printDlg);
}

void CJobDocumentationSettingsView::OnBnClickedJobdocPagesetupButton()
{
	CPageSetupDialog dlg;
	dlg.m_psd.lStructSize		= sizeof(PAGESETUPDLG);
	dlg.m_psd.hwndOwner			= m_hWnd;
	dlg.m_psd.hDevNames			= NULL;
	dlg.m_psd.hDevMode			= NULL;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	HGLOBAL hDevMode = GlobalAlloc(GHND, sizeof(DEVMODE));
	DEVMODE* pDevMode = (DEVMODE*)::GlobalLock(hDevMode);
	DWORD dwSize = sizeof(DEVMODE);
	if (RegQueryValueEx(hKey, _T("PrintDevMode"), 0, NULL, (LPBYTE)pDevMode, &dwSize) == ERROR_SUCCESS)
		dlg.m_psd.hDevMode = hDevMode;
	::GlobalUnlock(hDevMode);

	TCHAR szValue[100];
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_stscanf(szValue, _T("%ld %ld %ld %ld"), &dlg.m_psd.rtMargin.left, &dlg.m_psd.rtMargin.top, &dlg.m_psd.rtMargin.right, &dlg.m_psd.rtMargin.bottom);
	else
		dlg.m_psd.rtMargin = CRect(2500, 2500, 2500, 2500);

	if (dlg.DoModal() == IDOK)
	{
		_stprintf(szValue, _T("%ld %ld %ld %ld"), dlg.m_psd.rtMargin.left, dlg.m_psd.rtMargin.top, dlg.m_psd.rtMargin.right, dlg.m_psd.rtMargin.bottom);
		RegSetValueEx(hKey, _T("PrintMargins"), 0, REG_SZ, (LPBYTE)szValue, (_tcslen(szValue) + 1) * sizeof(TCHAR));

		pDevMode = (DEVMODE*)::GlobalLock(dlg.m_psd.hDevMode);
		if (pDevMode->dmOrientation == DMORIENT_PORTRAIT)
		{
			pDevMode->dmPaperWidth  = (int)(((float)dlg.m_psd.ptPaperSize.x + 0.5f) / 10.0f);
			pDevMode->dmPaperLength = (int)(((float)dlg.m_psd.ptPaperSize.y + 0.5f) / 10.0f);
		}
		else
		{
			pDevMode->dmPaperLength  = (int)(((float)dlg.m_psd.ptPaperSize.x + 0.5f) / 10.0f);
			pDevMode->dmPaperWidth   = (int)(((float)dlg.m_psd.ptPaperSize.y + 0.5f) / 10.0f);
		}

		theApp.SelectPrinter(dlg.m_psd.hDevNames, dlg.m_psd.hDevMode);

		RegSetValueEx(hKey, _T("PrintDevMode"), 0, REG_BINARY, (LPBYTE)pDevMode, sizeof(DEVMODE));
		
		::GlobalUnlock(dlg.m_psd.hDevMode);
	}
	RegCloseKey(hKey);

	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

BOOL CJobDocumentationSettingsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_toolbar1Buttons.m_pToolTip)            
		m_toolbar1Buttons.m_pToolTip->RelayEvent(pMsg);	

	if (m_toolbar2Buttons.m_pToolTip)            
		m_toolbar2Buttons.m_pToolTip->RelayEvent(pMsg);	

	if (m_toolbar3Buttons.m_pToolTip)            
		m_toolbar3Buttons.m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

void CJobDocumentationSettingsView::OnViewFrontside()
{
	if (m_nShowSide == FRONTSIDE)
		return;
	m_nShowSide = FRONTSIDE;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnViewBackside()
{
	if (m_nShowSide == BACKSIDE)
		return;
	m_nShowSide = BACKSIDE;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnViewBothsides()
{
	if (m_nShowSide == BOTHSIDES)
		return;
	m_nShowSide = BOTHSIDES;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnFoldsheetCheck()
{
	m_bShowFoldSheet = ! m_bShowFoldSheet;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnMeasureCheck()
{
	m_bShowMeasure = ! m_bShowMeasure;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnPlateCheck()
{
	m_bShowPlate = ! m_bShowPlate;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnBitmapCheck()
{
	m_bShowBitmap = ! m_bShowBitmap;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnExposureCheck()
{
	m_bShowExposures = ! m_bShowExposures;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnShowCutBlocks() 
{
	m_bShowCutBlocks = ! m_bShowCutBlocks;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnShowBoxshapes() 
{
	m_bShowBoxShapes = ! m_bShowBoxShapes;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnZoomMultipage()
{
	if (m_nZoomState == ZoomMultiPage)
		return;
	m_nZoomState = ZoomMultiPage;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnZoomPageFit()
{
	if (m_nZoomState == ZoomPageFit)
		return;
	m_nZoomState = ZoomPageFit;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnZoomPageWidth()
{
	if (m_nZoomState == ZoomPageWidth)
		return;
	m_nZoomState = ZoomPageWidth;
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnUpdateViewFrontside(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_nShowSide == FRONTSIDE);
}

void CJobDocumentationSettingsView::OnUpdateViewBackside(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_nShowSide == BACKSIDE);
}

void CJobDocumentationSettingsView::OnUpdateViewBothsides(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_nShowSide == BOTHSIDES);
}

void CJobDocumentationSettingsView::OnUpdateFoldsheetCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_bShowFoldSheet);
}

void CJobDocumentationSettingsView::OnUpdateMeasureCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_bShowMeasure);
}

void CJobDocumentationSettingsView::OnUpdatePlateCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_bShowPlate);
}

void CJobDocumentationSettingsView::OnUpdateBitmapCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_bShowBitmap);
}

void CJobDocumentationSettingsView::OnUpdateExposureCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_bShowExposures);
}

void CJobDocumentationSettingsView::OnUpdateShowCutBlocks(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_bShowCutBlocks);
}

void CJobDocumentationSettingsView::OnUpdateShowBoxshapes(CCmdUI* pCmdUI) 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	COwnerDrawnButtonList::Enable(pCmdUI, pDoc->m_flatProducts.HasGraphics());
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_bShowBoxShapes);
}

void CJobDocumentationSettingsView::OnUpdateZoomMultipage(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_nZoomState == ZoomMultiPage);
}

void CJobDocumentationSettingsView::OnUpdateZoomPageFit(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_nZoomState == ZoomPageFit);
}

void CJobDocumentationSettingsView::OnUpdateZoomPageWidth(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_nZoomState == ZoomPageWidth);
}

void CJobDocumentationSettingsView::OnDeltaposSheetZoomLevelSpin(NMHDR *pNMHDR, LRESULT *pResult)
{
	*pResult = 0;

	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	if ( (m_nSheetZoomLevel <= 1)  && (pNMUpDown->iDelta < 0) )
		return;
	if ( (m_nSheetZoomLevel >= 10) && (pNMUpDown->iDelta > 0) )
		return;

	m_nSheetZoomLevel += pNMUpDown->iDelta;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	TCHAR szValue[100];
	_stprintf(szValue, _T("%d"), m_nSheetZoomLevel);
	DWORD dwSize = sizeof(szValue);
	RegSetValueEx(hKey, _T("JobTicketSheetZoomLevel"), 0, REG_SZ, (LPBYTE)szValue, (_tcslen(szValue) + 1) * sizeof(TCHAR));
	RegCloseKey(hKey);

	UpdateData(FALSE);
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}

void CJobDocumentationSettingsView::OnEnKillfocusSheetZoomLevelEdit()
{
	UpdateData(TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CJobDocumentationView));
}
