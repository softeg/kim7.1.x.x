#pragma once

#include "MyBitmapButton.h"


// CMaskSettingsTableHeader

class CMaskSettingsTableHeader : public CStatic
{
	DECLARE_DYNAMIC(CMaskSettingsTableHeader)

public:
	CMaskSettingsTableHeader();
	virtual ~CMaskSettingsTableHeader();

public:
	CFont m_font;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};




DECLARE_TABLECOLUMN(CColumnSheetMaskSettingsSheet);
DECLARE_TABLECOLUMN(CColumnSheetMaskSettingsGlobal);
DECLARE_TABLECOLUMN(CColumnSheetMaskSettingsLocal);
DECLARE_TABLECOLUMN(CColumnSheetMaskSettingsContent);


class CPrintSheetMaskSettingsTable : public CPrintSheetTable
{
public:
	void Create(class CPrintSheetMaskSettingsView* pParent);
	void Initialize(int nWhatToShow);
	int	 GetCellLines(CPrintSheet& rPrintSheet);
	int  GetNumLockedObjects(CPrintSheet& rPrintSheet, int nSide);
};



// CPrintSheetMaskSettingsView-Formularansicht

class CPrintSheetMaskSettingsView : public CFormView
{
	DECLARE_DYNCREATE(CPrintSheetMaskSettingsView)

protected:
	CPrintSheetMaskSettingsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetMaskSettingsView();

public:
	enum { IDD = IDD_PRINTSHEETMASKSETTINGSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	DECLARE_DISPLAY_LIST(CPrintSheetMaskSettingsView, OnMultiSelect, NO, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CPrintSheetMaskSettingsView, PDFSheet,		  OnSelect, YES,  OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetMaskSettingsView, PDFSheetSide,	  OnSelect, YES,  OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPrintSheetMaskSettingsView, PDFSheetPlate,  OnSelect, NO,   OnDeselect, NO,  OnActivate, NO,  OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO,  OnDrawState, NO, IsSelectable, TRUE)


enum {SheetAll, SheetSelected};

public:
	CMaskSettingsTableHeader	 m_maskSettingsTableTitle, m_maskSettingsTableHeader;
	CPrintSheetMaskSettingsTable m_maskSettingsTable;
	CMyBitmapButton				 m_releaseButton, m_adjustButton;
	CToolTipCtrl*				 m_pToolTip;


public:
	void		 InitData();
	void		 ModifyBleeds();
	BOOL		 IsSheetEffected	(CPrintSheet& rPrintSheet, BOOL bTrueIfNothing = TRUE);
	BOOL		 IsSheetSideEffected(CPrintSheet& rPrintSheet, int nSide, BOOL bTrueIfNothing = TRUE);
	CPrintSheet* GetActPrintSheet();

protected:
	float m_fOverbleed;
	int	  m_nWhatSheets;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	virtual void OnDraw(CDC* /*pDC*/);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	void UpdateBars();
	void ScrollToDevicePosition(POINT ptDev); // explicit scrolling no checking
	void SetScrollSizes(int nMapMode, SIZE sizeTotal, const SIZE& sizePage, const SIZE& sizeLine);
	afx_msg void OnKillFocusMaskSettingsOverbleedEdit();
	afx_msg void OnDeltaposMaskSettingsOverbleedSpin(NMHDR* pNMHDR, LRESULT* /*pResult*/);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wParam, LPARAM lParam);
	afx_msg void OnEnSetfocusMasksettingsOverbleedEdit();
	afx_msg void OnBnClickedMasksettingsReleaseBleed();
	afx_msg void OnUpdateMasksettingsReleaseBleed(CCmdUI* pCmdUI);
	afx_msg void OnBnClickedMasksettingsAdjustContent();
	afx_msg void OnUpdateMasksettingsAdjustContent(CCmdUI* pCmdUI);
};


