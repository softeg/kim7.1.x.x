#pragma once

#include "tbarcode.h"


// CDlgBarcodeMarkContent-Dialogfeld

class CDlgBarcodeMarkContent : public CDialog
{
	DECLARE_DYNAMIC(CDlgBarcodeMarkContent)

public:
	CDlgBarcodeMarkContent(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgBarcodeMarkContent();

// Dialogfelddaten
	enum { IDD = IDD_BARCODEMARK_CONTENT};


public:
	t_BarCode*						m_pbarCode;
	class CDlgLayoutObjectControl*	m_pParent;
	BOOL							m_bInitialShowFoldSheet;
	CDlgSystemGenMarkColorControl	m_printColorControl;
	CMenu							m_placeholderMenu;
	CPlaceholderEdit				m_placeholderEdit;


// controls
	CString							m_strOutputText;
	CComboBox						m_barcodeType;
	CComboBox						m_barcodeTextCombo;
	CSpinButtonCtrl					m_barcodeTextsizeSpin;
	float							m_fBarcodeTextsize;

// Implementierung
public:
	void	CheckBarcodeForFoldsheetView();
	void	LoadData();
	void	SaveData();
	int		GetMarkHeadPosition();
	float	GetMarkWidth();
	float	GetMarkHeight();
	void	Apply();
	void	OnSelectPlaceholder(int nID);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSelectPlaceholder();
	afx_msg void OnPlaceholderFoldsheetNum();
	afx_msg void OnPlaceholderColorName();
	afx_msg void OnPlaceholderSheetside();
	afx_msg void OnPlaceholderWebNum();
	afx_msg void OnPlaceholderColorNum();
	afx_msg void OnPlaceholderFoldsheetNumCG();
	afx_msg void OnPlaceholderJobTitle();
	afx_msg void OnPlaceholderTileNum();
	afx_msg void OnPlaceholderSheetNum();
	afx_msg void OnPlaceholderFile();
	afx_msg void OnPlaceholderLowpageSide();
	afx_msg void OnPlaceholderLowpageSheet();
	afx_msg void OnPlaceholderHighpageSide();
	afx_msg void OnPlaceholderHighpageSheet();
	afx_msg void OnPlaceholderPressDevice();
	afx_msg void OnPlaceholderJob();
	afx_msg void OnPlaceholderJobNum();
	afx_msg void OnPlaceholderCustomer();
	afx_msg void OnPlaceholderCustomerNum();
	afx_msg void OnPlaceholderCreator();
	afx_msg void OnPlaceholderCreationDate();
	afx_msg void OnPlaceholderLastModified();
	afx_msg void OnPlaceholderScheduledToPrint();
	afx_msg void OnPlaceholderOutputDate();
	afx_msg void OnPlaceholderNotes();
	afx_msg void OnPlaceholderAsir();
	afx_msg void OnPlaceholderPageNumList();
	afx_msg void OnPlaceholderPageNum();
	afx_msg void OnPlaceholderProductName();
	afx_msg void OnPlaceholderProductPartName();
	afx_msg void OnPlaceholderPaperGrain();
	afx_msg void OnPlaceholderPaperType();
	afx_msg void OnPlaceholderPaperGrammage();
	afx_msg void OnPlaceholderWorkStyle();
	afx_msg void OnPlaceholderFoldScheme();
	afx_msg void OnPlaceholderNumFoldsheets();
	afx_msg void OnPlaceholderBlank();
	afx_msg void OnPaint();
	afx_msg BOOL OnToolTipNeedText(UINT id, NMHDR * pNMHDR, LRESULT * pResult);
	afx_msg void OnCbnSelchangeBarcodeTypeCombo();
	afx_msg void OnEnChangeBarcodemarkstring();
	afx_msg void OnDeltaposBarcodeTextsizeSpin(NMHDR* pNMHDR, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnUpdateBarcodemarkstring();
	afx_msg void OnEnKillfocusBarcodeTextsize();
	afx_msg void OnCbnSelchangeBarcodeTextCombo();
	afx_msg void OnBnClickedButtonPlaceholderInfo();
};
