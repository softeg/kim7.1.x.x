#pragma once

#include "DlgLayoutObjectControl.h"
#include "DlgObjectPreviewWindow.h"



// CPrintSheetObjectPreview-Formularansicht

class CPrintSheetObjectPreview : public CFormView
{
	DECLARE_DYNCREATE(CPrintSheetObjectPreview)

protected:
	CPrintSheetObjectPreview();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetObjectPreview();

public:
	enum { IDD = IDD_PRINTSHEETOBJECT_PREVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	CDlgObjectPreviewWindow	m_dlgObjectPreviewWindow;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


