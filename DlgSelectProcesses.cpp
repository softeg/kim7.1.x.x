// DlgSelectProcesses.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgSelectProcesses.h"


void CSelectProcessesListBox::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC*	pDC = CDC::FromHandle(lpDIS->hDC);
	CString string;
	CImage  img;
	switch (lpDIS->itemData)
	{
	case CProductionProfile::Prepress:	img.LoadFromResource(theApp.m_hInstance, IDB_PREPRESS);				string.LoadString(IDS_PROCESS_PREPRESS);	break;
	case CProductionProfile::Printing:	img.LoadFromResource(theApp.m_hInstance, IDB_PRINTING_MACHINE);		string.LoadString(IDS_PROCESS_PRINTING);	break;
	case CProductionProfile::Cutting:	img.LoadFromResource(theApp.m_hInstance, IDB_CUTMACHINE);			string.LoadString(IDS_PROCESS_CUTTING);		break;
	case CProductionProfile::Folding:	img.LoadFromResource(theApp.m_hInstance, IDB_FOLD_CUT);				string.LoadString(IDS_PROCESS_FOLDING);		break;
	case CProductionProfile::Binding:	img.LoadFromResource(theApp.m_hInstance, IDB_BINDING);				string.LoadString(IDS_PROCESS_BINDING);		break;
	case CProductionProfile::Trimming:	img.LoadFromResource(theApp.m_hInstance, IDB_TRIMMING);				string.LoadString(IDS_PROCESS_TRIMMING);	break;
	//case CProductionProfile::Punching:	img.LoadFromResource(theApp.m_hInstance, IDB_PUNCHING);				string.LoadString(IDS_PROCESS_PUNCHING);	break;
	}

	int nOff		= 4;
	int nImgWidth	= 36;
	int nImgHeight	= 36;
	int nPosLeft	= 0;
	int nPosRight	= GetColumnWidth(0);

	pDC->SetBkMode(TRANSPARENT);
	if ((lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// item has been selected - hilite 
		CRect rect = lpDIS->rcItem;
		rect.left  = nPosLeft;
		rect.right = nPosRight;
		CBrush fillBrush(::GetSysColor(COLOR_HIGHLIGHT));
		pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
		pDC->FillRect(rect, &fillBrush);
		rect.left += nImgWidth + nOff;
		pDC->DrawText(string, rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		fillBrush.DeleteObject();
	}

	if (!(lpDIS->itemState & ODS_SELECTED) &&
		(lpDIS->itemAction & (ODA_SELECT | ODA_DRAWENTIRE)))
	{
		// Item has been de-selected - remove hilite
		CRect rect = lpDIS->rcItem;
		rect.left  = nPosLeft;// + nImgWidth + nOff;
		rect.right = nPosRight;
		CBrush fillBrush(pDC->GetBkColor());
		pDC->SetTextColor(::GetSysColor(COLOR_MENUTEXT));
		pDC->FillRect(rect, &fillBrush);
		rect.left += nImgWidth + nOff;
		pDC->DrawText(string, rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		fillBrush.DeleteObject();
	}						

	if ( (lpDIS->itemAction & ODA_DRAWENTIRE))
	{
		if ( ! img.IsNull())
		{
			CRect rcImg;
			rcImg.left = lpDIS->rcItem.left + nOff; rcImg.top = CRect(lpDIS->rcItem).CenterPoint().y - img.GetHeight() / 2; rcImg.right = rcImg.left + img.GetWidth(); rcImg.bottom = rcImg.top + img.GetHeight();
			img.TransparentBlt(lpDIS->hDC, rcImg, RGB(192,192,192));
		}
	}
}


// CDlgSelectProcesses-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgSelectProcesses, CDialog)

CDlgSelectProcesses::CDlgSelectProcesses(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSelectProcesses::IDD, pParent)
{
	m_pImageList = new CImageList();
}

CDlgSelectProcesses::~CDlgSelectProcesses()
{
	delete m_pImageList;
}

void CDlgSelectProcesses::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgSelectProcesses, CDialog)
	ON_BN_CLICKED(IDC_SELECTPROCESS_ADD, &CDlgSelectProcesses::OnBnClickedSelectprocessAdd)
	ON_BN_CLICKED(IDC_SELECTPROCESS_REMOVE, &CDlgSelectProcesses::OnBnClickedSelectprocessRemove)
	ON_BN_CLICKED(IDOK, &CDlgSelectProcesses::OnBnClickedOk)
	ON_NOTIFY(NM_DBLCLK, IDC_ALLPROCESSES_LIST, &CDlgSelectProcesses::OnNMDblclkAllprocessesList)
	ON_NOTIFY(NM_DBLCLK, IDC_SELECTEDPROCESSES_LIST, &CDlgSelectProcesses::OnNMDblclkSelectedprocessesList)
END_MESSAGE_MAP()


// CDlgSelectProcesses-Meldungshandler

BOOL CDlgSelectProcesses::OnInitDialog()
{
	CDialog::OnInitDialog();

	VERIFY(m_allProcessesList.SubclassDlgItem(IDC_ALLPROCESSES_LIST, this));
	VERIFY(m_selectedProcessesList.SubclassDlgItem(IDC_SELECTEDPROCESSES_LIST, this));

	m_allProcessesList.InsertColumn(0, "Verf�gbare",  LVCFMT_LEFT, 180, 0);
	m_selectedProcessesList.InsertColumn(0, "Ausgew�hlte", LVCFMT_LEFT, 180, 0);

	LVITEM lvitem;
	lvitem.iSubItem = 0;
	lvitem.mask	    = LVIF_TEXT | LVIF_PARAM;
	lvitem.pszText  = NULL;		
	lvitem.iItem	= 0; lvitem.lParam	= (LPARAM)CProductionProfile::Prepress;	m_allProcessesList.InsertItem(&lvitem); 
	lvitem.iItem	= 1; lvitem.lParam	= (LPARAM)CProductionProfile::Printing;	m_allProcessesList.InsertItem(&lvitem); 
	lvitem.iItem	= 2; lvitem.lParam	= (LPARAM)CProductionProfile::Cutting;	m_allProcessesList.InsertItem(&lvitem); 
	lvitem.iItem	= 3; lvitem.lParam	= (LPARAM)CProductionProfile::Folding;	m_allProcessesList.InsertItem(&lvitem); 
	lvitem.iItem	= 4; lvitem.lParam	= (LPARAM)CProductionProfile::Binding;	m_allProcessesList.InsertItem(&lvitem); 
	lvitem.iItem	= 5; lvitem.lParam	= (LPARAM)CProductionProfile::Trimming;	m_allProcessesList.InsertItem(&lvitem); 
	//lvitem.iItem	= 6; lvitem.lParam	= (LPARAM)CProductionProfile::Punching;	m_allProcessesList.InsertItem(&lvitem); 

	for (int i = 0; i < m_selectedProcesses.GetSize(); i++)
	{
		lvitem.iItem  = i;
		lvitem.lParam = (LPARAM)m_selectedProcesses[i];
		m_selectedProcessesList.InsertItem(&lvitem); 
	}

	CBitmap bitmap;
	m_pImageList->Create(36, 36, ILC_COLOR8, 1, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_BKSCHEMELIST);
	m_pImageList->Add(&bitmap, BLACK);
	m_allProcessesList.SetImageList(m_pImageList, LVSIL_SMALL);
	m_selectedProcessesList.SetImageList(m_pImageList, LVSIL_SMALL);
	bitmap.DeleteObject();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgSelectProcesses::OnBnClickedSelectprocessAdd()
{
	POSITION pos = m_allProcessesList.GetFirstSelectedItemPosition();
	if ( ! pos)
		return;
	int nSel = (LPARAM)m_allProcessesList.GetNextSelectedItem(pos);
	int nID  = m_allProcessesList.GetItemData(nSel);

	for (int i = 0; i < m_selectedProcessesList.GetItemCount(); i++)
	{
		if (nID == m_selectedProcessesList.GetItemData(i))	// already there
			return;
	}

	LVITEM lvitem;
	lvitem.iItem	= m_selectedProcessesList.GetItemCount();
	lvitem.iSubItem = 0;
	lvitem.mask	    = LVIF_TEXT | LVIF_PARAM;
	lvitem.pszText  = NULL;		
	lvitem.lParam = (LPARAM)nID;
	m_selectedProcessesList.InsertItem(&lvitem); 
}

void CDlgSelectProcesses::OnBnClickedSelectprocessRemove()
{
	POSITION pos = m_selectedProcessesList.GetFirstSelectedItemPosition();
	if ( ! pos)
		return;
	int nSel = (LPARAM)m_selectedProcessesList.GetNextSelectedItem(pos);
	int nID  = m_selectedProcessesList.GetItemData(nSel);
	//if (nID == CProductionProfile::Prepress)	// prepress process cannot be removed
	//	return;
	//if (nID == CProductionProfile::Printing)	// print process cannot be removed
	//	return;

	m_selectedProcessesList.DeleteItem(nSel);
}

void CDlgSelectProcesses::OnNMDblclkAllprocessesList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	OnBnClickedSelectprocessAdd();
	*pResult = 0;
}

void CDlgSelectProcesses::OnNMDblclkSelectedprocessesList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	OnBnClickedSelectprocessRemove();
	*pResult = 0;
}

void CDlgSelectProcesses::OnBnClickedOk()
{
	m_selectedProcesses.RemoveAll();

	for (int i = 0; i < m_selectedProcessesList.GetItemCount(); i++)
	{
		m_selectedProcesses.Add(m_selectedProcessesList.GetItemData(i));
	}

	OnOK();
}
