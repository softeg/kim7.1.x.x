
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DlgNumeration.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgNumeration dialog

class CDlgNumeration : public CDialog
{
// Construction
public:
	CDlgNumeration(CWnd* pParent = NULL, BOOL bRunModal = FALSE);   // standard constructor
	~CDlgNumeration();


enum { SelectionMarked = 0, SelectionAll = 1, SelectionRange = 2 , SelectionProductPart = 3 };

// Dialog Data
	//{{AFX_DATA(CDlgNumeration)
	enum { IDD = IDD_NUMERATION_DIALOG };
	CSpinButtonCtrl	m_SpinStartPage;
	int		m_nStartNum;
	UINT	m_nRangeTo;
	UINT	m_nRangeFrom;
	int		m_nSelection;
	CString	m_strNumerationStyle;
	BOOL	m_bReverse;
	CComboBox m_productGroupCombo;
	//}}AFX_DATA

public:
	BOOL	m_bRunModal;
	int		m_nProductPartIndex;
	CString m_strTitle;
	UINT	m_nRangeToMax;
	void	(*m_pfnApplyCallback)(int nSelection, int nRangeFrom, int nRangeTo, int nStartNum, CString strStyle, BOOL bReverse, int nProductPartIndex);
	BOOL	(*m_pfnInitCallback) (CDlgNumeration* pDlg);
	CView*  m_pViewToUpdate1;
	CView*  m_pViewToUpdate2;

public:
	static CString	GenerateNumber(int nNumber, CString strNumerationStyle, int nNumerationOffset);
	static int		GetSeqLen(CString strNumerationStyle);
	static CString	Arab2Roman(int nNumber);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgNumeration)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgNumeration)
	virtual BOOL OnInitDialog();
	afx_msg void OnApplyButton();
	virtual void OnOK();
	afx_msg void OnSetfocusRangeFrom();
	afx_msg void OnSetfocusRangeTo();
	afx_msg void OnKillfocusRangeFrom();
	afx_msg void OnKillfocusRangeTo();
	virtual void OnCancel();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnNumerateReverse();
	afx_msg void OnBnClickedNumerateReset();
	afx_msg void OnSelectAll();
	afx_msg void OnSelectMarked();
	afx_msg void OnSelectRange();
	afx_msg void OnSelectProductGroup();
	afx_msg void OnCbnSelchangeSelectProductpartCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

