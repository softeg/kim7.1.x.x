// BookBlockToolsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "BookBlockToolsView.h"
#include "BookblockFrame.h"
#include "BookBlockView.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "BookBlockProdDataView.h"
#include "PrintSheetTableView.h"


// CBookBlockToolsView

IMPLEMENT_DYNCREATE(CBookBlockToolsView, CFormView)

CBookBlockToolsView::CBookBlockToolsView()
	: CFormView(CBookBlockToolsView::IDD)
{
}

CBookBlockToolsView::~CBookBlockToolsView()
{
}

void CBookBlockToolsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CBookBlockToolsView, CFormView)
	ON_UPDATE_COMMAND_UI(ID_ZOOM, OnUpdateZoomIncrease)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_DECREASE, OnUpdateZoomDecrease)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_FULLVIEW, OnUpdateZoomToFit)
	ON_UPDATE_COMMAND_UI(ID_SCROLLHAND, OnUpdateScrollHand)
	ON_UPDATE_COMMAND_UI(ID_BOOKBLOCK_PROOF, OnUpdateBookblockProof)
	ON_UPDATE_COMMAND_UI(ID_BOOKBLOCK_PROOF_OPENFILE, OnUpdateBookblockProofOpenfile)
	ON_BN_CLICKED(IDC_GOTO_PREVPAGE_BUTTON, OnGotoPrevPage)
	ON_BN_CLICKED(IDC_GOTO_NEXTPAGE_BUTTON, OnGotoNextPage)
	ON_UPDATE_COMMAND_UI(IDC_GOTO_PREVPAGE_BUTTON, OnUpdateGotoPrevButton)
	ON_UPDATE_COMMAND_UI(IDC_GOTO_NEXTPAGE_BUTTON, OnUpdateGotoNextButton)
END_MESSAGE_MAP()


// CBookBlockToolsView-Diagnose

#ifdef _DEBUG
void CBookBlockToolsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CBookBlockToolsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CBookBlockToolsView-Meldungshandler

void CBookBlockToolsView::OnInitialUpdate()
{
	m_pBookblockFrame = (CBookblockFrame*)GetParentFrame();

	CFormView::OnInitialUpdate(); 

	CRect rcButton; 
	m_toolBar.Create(this, GetParentFrame(), NULL, IDB_BOOKBLOCK_TOOLBAR, IDB_BOOKBLOCK_TOOLBAR_COLD, CSize(30, 35));
	GetDlgItem(IDC_BINDTOOL_ZOOM_FRAME)->GetWindowRect(rcButton); ScreenToClient(rcButton); rcButton.bottom = rcButton.top + 37;
	m_toolBar.AddButton(-1, IDS_ZOOMIN_TOOLTIP,						ID_ZOOM,					 rcButton, 0); rcButton.OffsetRect(0, rcButton.Height());
	m_toolBar.AddButton(-1,	IDS_ZOOMOUT_TOOLTIP,					ID_ZOOM_DECREASE,			 rcButton, 1); rcButton.OffsetRect(0, rcButton.Height());
	m_toolBar.AddButton(-1,	IDS_ZOOMALL_TOOLTIP,					ID_ZOOM_FULLVIEW,			 rcButton, 2); rcButton.OffsetRect(0, rcButton.Height());
	m_toolBar.AddButton(-1,	IDS_ZOOMMOVE_TOOLTIP,					ID_SCROLLHAND,				 rcButton, 3); rcButton.OffsetRect(0, rcButton.Height() * 3);
	m_toolBar.AddButton(-1,	IDS_BOOKBLOCKPROOF_TOOLTIP,				ID_BOOKBLOCK_PROOF,			 rcButton, 4); rcButton.OffsetRect(0, rcButton.Height() + rcButton.Height()/4);
	m_toolBar.AddButton(-1,	IDS_BOOKBLOCKPROOF_OPENFILE_TOOLTIP,	ID_BOOKBLOCK_PROOF_OPENFILE, rcButton, 5);

	m_gotoPrevPageButton.SubclassDlgItem(IDC_GOTO_PREVPAGE_BUTTON, this);
	m_gotoPrevPageButton.SetBitmap		(IDB_GOTO_PREV_HOT, IDB_GOTO_PREV, -1, XCENTER);
	m_gotoPrevPageButton.SetTransparent	(TRUE);
	m_gotoNextPageButton.SubclassDlgItem(IDC_GOTO_NEXTPAGE_BUTTON, this);
	m_gotoNextPageButton.SetBitmap		(IDB_GOTO_NEXT_HOT, IDB_GOTO_NEXT, -1, XCENTER);
	m_gotoNextPageButton.SetTransparent	(TRUE);

	CRect rcClient; GetClientRect(rcClient);
	SetScaleToFitSize(rcClient.Size());	//suppress scrollbars
}

BOOL CBookBlockToolsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_toolBar.m_pToolTip)            
		m_toolBar.m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

void CBookBlockToolsView::OnUpdateZoomIncrease(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);

	//if ( ! m_pBookblockFrame)
	//	return;
	//COwnerDrawnButtonList::SetCheck(pCmdUI, m_pBookblockFrame->m_bZoomMode);
}

void CBookBlockToolsView::OnUpdateZoomDecrease(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);

	//if ( ! m_pBookblockFrame)
	//	return;

	//CBookBlockView* pView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));

	//CRect clientRect;
	//pView->GetClientRect(&clientRect);
 //   if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
	//	COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
	//else
	//	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CBookBlockToolsView::OnUpdateZoomToFit(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);

	//CBookBlockView* pView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));

	//CRect clientRect;
	//pView->GetClientRect(&clientRect);
 //   if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
	//	COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
	//else
	//	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CBookBlockToolsView::OnUpdateScrollHand(CCmdUI* pCmdUI) 
{
	if ( ! m_pBookblockFrame)
		return;

	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pBookblockFrame->m_bHandActive);

	CBookBlockView* pView = (CBookBlockView*)theApp.GetView(RUNTIME_CLASS(CBookBlockView));
	if ( ! pView)
		return;
	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		m_pBookblockFrame->m_bHandActive = FALSE;
	}
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CBookBlockToolsView::OnUpdateBookblockProof(CCmdUI* pCmdUI) 
{
	if ( (theApp.m_nDongleStatus == CImpManApp::ClientDongle) && (theApp.m_nGUIStyle == CImpManApp::GUIStandard) )
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		return;
	}

	CImpManDoc*	pDoc	= CImpManDoc::GetDoc();
	BOOL		bEnable = (pDoc) ? (pDoc->m_Bookblock.m_FoldSheetList.GetCount() > 0) : FALSE;
	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CBookBlockToolsView::OnUpdateBookblockProofOpenfile(CCmdUI* pCmdUI) 
{
	if ( (theApp.m_nDongleStatus == CImpManApp::ClientDongle) && (theApp.m_nGUIStyle == CImpManApp::GUIStandard) )
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		return;
	}

	CImpManDoc*	pDoc		= CImpManDoc::GetDoc();
	BOOL		bFileExists = (pDoc) ? pDoc->m_Bookblock.OutFileExists(BOTHSIDES, NULL) : FALSE;
	COwnerDrawnButtonList::Enable(pCmdUI, bFileExists);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CBookBlockToolsView::OnGotoPrevPage() 
{
	CBookBlockProdDataView* pBookBlockProdDataView = (CBookBlockProdDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockProdDataView));
	if (pBookBlockProdDataView)
		pBookBlockProdDataView->OnGotoPrevPage();
}

void CBookBlockToolsView::OnGotoNextPage() 
{
	CBookBlockProdDataView* pBookBlockProdDataView = (CBookBlockProdDataView*)theApp.GetView(RUNTIME_CLASS(CBookBlockProdDataView));
	if (pBookBlockProdDataView)
		pBookBlockProdDataView->OnGotoNextPage();
}

void CBookBlockToolsView::OnUpdateGotoPrevButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

void CBookBlockToolsView::OnUpdateGotoNextButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}
