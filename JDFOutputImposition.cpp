// JDFOutputImposition.cpp: Implementierung der Klasse CJDFOutputImposition.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "imposition manager.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "MainFrm.h"
#include "PDFEngineInterface.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "DlgPDFLibStatus.h"
#include "DlgConfirmPDFSheetOverwrite.h"
#include "PDFOutput.h"
#include "spromeps.h"
#include "DlgDemoOrRegisterDongle.h"
#include "JDFOutputImposition.h"
#include "DlgStopOutput.h"
#include <Shlwapi.h>
#include <locale.h>
#include "DlgBookblockProofProgress.h"
#include "XMLCommon.h"



#ifdef _DEBUG 
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



//////////////////////////////////////////////////////////////////////
// Konstruktion/Destruktion
//////////////////////////////////////////////////////////////////////

CJDFOutputImposition::CJDFOutputImposition(CPDFTargetProperties& rPDFTarget)
{
	m_nTargetType		  			= rPDFTarget.m_nType;
	m_strTargetName		  			= rPDFTarget.m_strTargetName;
	m_strLocation		  			= rPDFTarget.m_strLocation;
	m_strFilenameTemplate 			= rPDFTarget.m_strOutputFilename;
	m_bJDFOutputMime				= rPDFTarget.m_bJDFOutputMime;
	m_fMediaWidth		  			= 0.0f;
	m_fMediaHeight		  			= 0.0f;
	m_nError			  			= 0;
	m_bCancelJob		  			= FALSE;
	m_media				  			= rPDFTarget.m_mediaList[0];
	m_nPlatesProcessed	  			= 0;
	m_nMarkOrd			  			= 0;
	m_nCompatibility	  			= rPDFTarget.m_nJDFCompatibility;
	m_bMarksInFolder	  			= rPDFTarget.m_bJDFMarksInFolder;
	m_strMarksFolder	  			= rPDFTarget.m_strJDFMarksFolder;
	m_bOutputFormatExceeded 		= FALSE;
	m_strJDFJobIDMap				= rPDFTarget.m_strJDFJobIDMap;
	m_strJDFJobPartIDMap			= rPDFTarget.m_strJDFJobPartIDMap;
	m_strJDFJobDescriptiveNameMap	= rPDFTarget.m_strJDFJobDescriptiveNameMap;
	m_apogeeParameterSets.RemoveAll();
	m_apogeeParameterSets.Append(rPDFTarget.m_apogeeParameterSets);

	setlocale(LC_NUMERIC, "english");	// because when german setted, Format() does produce numbers with Comma like "0,138"
}

CJDFOutputImposition::~CJDFOutputImposition()
{
	CString strLanguage;
	switch (theApp.settings.m_iLanguage)
	{
		case 0: strLanguage = _T("GER"); setlocale(LC_ALL, "german");	break;
		case 1: strLanguage = _T("ENG"); setlocale(LC_ALL, "english");	break;
		case 2: strLanguage = _T("FRA"); break;
		case 3: strLanguage = _T("ITA"); break;
		case 4: strLanguage = _T("SPA"); break;
		case 5: strLanguage = _T("HOL"); break;	// No holl. MFC4x.dll available
		//Default would already be set at reading ini-file
	}
}

BOOL CJDFOutputImposition::Open(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll, int nSide, CPlate* pPlate)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return FALSE; 

	m_strTmpOutputJDFPath = pPrintSheet->GetOutFileTmpFullPath((nSide == ALLSIDES) ? BOTHSIDES : nSide, pPlate);
	m_strOutputJDFPath	  = pPrintSheet->GetOutFileFullPath	  ((nSide == ALLSIDES) ? BOTHSIDES : nSide, pPlate);
	m_strOutputFilename	  = pPrintSheet->GetOutFilename		  ((nSide == ALLSIDES) ? BOTHSIDES : nSide, pPlate);
	if (m_bJDFOutputMime)
	{
		m_strOutputMimePath = m_strOutputJDFPath;
		m_strOutputJDFPath = m_strTmpOutputJDFPath;
		theApp.RemoveExtension(m_strOutputJDFPath);		// change .mjd extension to .jdf
		m_strOutputJDFPath += _T(".jdf");
	}

	m_nMarkOrd = 0;

	CPrintSheetTableView*  pPrintSheetTableView  = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pPrintSheetTableView)
		pPrintSheetTableView->HighlightPDFOutFile(pPrintSheet, (nSide == ALLSIDES) ? BOTHSIDES : nSide, pPlate);

	CFileStatus	fileStatus;
	if ( (bOverwriteAll == -1) || ! bOverwriteAll)
	{
		BOOL bFileExists = FALSE;
		if (m_bJDFOutputMime)
		{
			if (CFile::GetStatus(m_strOutputMimePath, fileStatus))	// target file exists
				bFileExists = TRUE;
		}
		else
		{
			if (CFile::GetStatus(m_strOutputJDFPath, fileStatus))	// target file exists
				bFileExists = TRUE;
		}

		if (bFileExists)
		{
			int nRet = IDC_USE_EXISTING;
			if (bOverwriteAll == -1)
			{
				CDlgConfirmPDFSheetOverwrite dlg;
				dlg.m_strPDFFilename = (m_bJDFOutputMime) ? m_strOutputMimePath : m_strOutputJDFPath;
				nRet = dlg.DoModal();
			}
			switch (nRet)
			{
			case IDC_NEW:			bOverwriteAll = TRUE;
									break;
			case IDC_USE_EXISTING:	bOverwriteAll = FALSE;
									if (pPrintSheetTableView)
										pPrintSheetTableView->StopPDFOutFeedback();
									m_bCancelJob = TRUE;
									return FALSE;
			case IDCANCEL:			m_bCancelJob = TRUE;
									if (pPrintSheetTableView)
										pPrintSheetTableView->StopPDFOutFeedback();
									return FALSE;
			}
		}
	}

	if (pPrintSheetTableView)
		if (nSide == ALLSIDES)
			pPrintSheetTableView->StartPDFOutFeedback(pDoc->m_PrintSheetList.GetNumOutputObjects(CLayoutObject::Page));
		else
			pPrintSheetTableView->StartPDFOutFeedback(pPrintSheet->GetNumOutputObjects(nSide, pPlate, CLayoutObject::Page));

	m_nCurrentPage = 1;

	theApp.m_pDlgPDFLibStatus->m_strFilename = theApp.m_pDlgPDFLibStatus->m_strFullFilename = m_strOutputJDFPath;
	theApp.m_pDlgPDFLibStatus->UpdateStatus(CDlgPDFLibStatus::OpenDocument);

	// setup indentation for the output - 2 blanks/level
//	XMLIndent(2,true);

	
	m_pJDFDoc = new JDFDoc(0);
	JDFNode root = m_pJDFDoc->GetRoot();

	JDFOUT_AppendApogeeParameterSets(root);

	root.init();
	if (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy2215)
		root.SetVersion(_T("1.0"));
	else
		if (m_nCompatibility != CPDFTargetProperties::JDFComp13)
			root.SetVersion(_T("1.2"));

	CString	strJobTitle = theApp.RemoveFileExtension(pDoc->GetTitle(), _T(".job") ); 

	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)m_strJDFJobDescriptiveNameMap, NULL, BOTHSIDES, NULL, -1, "", -1, -1);	
	root.SetDescriptiveName(( ! _tcslen(szDest)) ? (KString)strJobTitle : (KString)szDest);

	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)m_strJDFJobPartIDMap, NULL, BOTHSIDES, NULL, -1, "", -1, -1);	
	root.SetJobPartID(( ! _tcslen(szDest)) ? (KString)strJobTitle : (KString)szDest);

	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)m_strJDFJobIDMap, NULL, BOTHSIDES, NULL, -1, "", -1, -1);	
	if ( ! _tcslen(szDest))
		if ( ! pDoc->m_GlobalData.m_strJobNumber.IsEmpty())
			root.SetJobID((KString)pDoc->m_GlobalData.m_strJobNumber);
		else
			root.SetJobID((KString)strJobTitle);
	else
		root.SetJobID((KString)szDest);

	root.SetMaxVersion(_T("1.3"));
	root.SetICSVersions(vWString(_T("Base_L0-1.3 LayCrImp_L1-1.3")));

	//root.SetTypes(vWString(_T("Imposition Cutting")));	//doesn't pass CheckJDF

	switch (m_nCompatibility)
	{
	case CPDFTargetProperties::JDFCompPrinergy2215:		
		root.SetAttribute(_T("xmlns:CPC"), _T("http://www.creo.com/CPC/JDFExtensions.xsd"));
		root.SetAttribute(_T("xmlns:SSi"), _T("//SSiJDFExtensions.xsd"));
		break;
	case CPDFTargetProperties::JDFCompPrinergy23:		
		root.SetAttribute(_T("xmlns"), _T("http://www.CIP4.org/JDFSchema_1_1"));
		root.SetAttribute(_T("xmlns:SSi"), _T("//SSiJDFExtensions.xsd"));
		break;
	case CPDFTargetProperties::JDFComp12:		
		root.SetAttribute(_T("xmlns"), _T("http://www.CIP4.org/JDFSchema_1_1"));
		break;
	}

	//root.SetEnumType(JDFNode::Type_Product);//JDFNode::Type_ProcessGroup);

	//JDFComponent component = root.AppendMatchingResource(JDFElement::elm_Component, JDFNode::ProcessUsage_AnyOutput);
	//component.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	//component.SetComponentType(JDFComponent::ComponentType_FinalProduct);

	root.SetEnumType(JDFNode::Type_ProcessGroup);
	m_impositionNode = root.AddJDFNode(JDF::JDFNode::Type_Imposition);
	m_impositionNode.SetDescriptiveName(_T("Imposition"));
	m_impositionNode.SetID(_T("ProcessImposition"));

	m_cuttingGroup	 = root.AddJDFNode(JDF::JDFNode::Type_Cutting);
	m_cuttingGroup.SetDescriptiveName(_T("Cutting"));
	m_cuttingGroup.SetID(_T("ProcessCutting"));
	m_componentSheet = m_cuttingGroup.AppendMatchingResource(JDFElement::elm_Component, JDFNode::ProcessUsage_AnyInput);
	m_componentSheet.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	m_componentSheet.SetComponentType(JDFComponent::ComponentType_Sheet);
	m_componentSheet.setPartIDKeys(vWString(_T("SignatureName SheetName")));
	JDFComponent compBlock = m_cuttingGroup.AppendMatchingResource(JDFElement::elm_Component, JDFNode::ProcessUsage_AnyOutput);
	compBlock.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	compBlock.SetComponentType(JDFComponent::ComponentType_Block);

	m_cuttingParams = m_cuttingGroup.AppendMatchingResource(JDFElement::elm_CuttingParams, JDFNode::ProcessUsage_AnyInput);
	m_cuttingParams.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	m_cuttingParams.SetStatus(JDF::JDFResource::Status_Available);
	m_cuttingParams.setPartIDKeys(vWString(_T("SignatureName SheetName")));

	///////// test highwater
	//root.SetTypes(vWString(_T("Cutting Imposition Separation")));
	//JDFColorPool colorPool = root.AppendMatchingResource(JDFElement::elm_ColorPool, JDFNode::ProcessUsage_AnyInput);
	//JDFColor color = colorPool.AppendColor();
	//color.SetName(_T("Cyan"));
	//color.SetCMYK(JDFCMYKColor(1,0,0,0));
	//color = colorPool.AppendColor();
	//color.SetName(_T("Magenta"));
	//color.SetCMYK(JDFCMYKColor(0,1,0,0));
	//color = colorPool.AppendColor();
	//color.SetName(_T("Yellow"));
	//color.SetCMYK(JDFCMYKColor(0,0,1,0));
	//color = colorPool.AppendColor();
	//color.SetName(_T("Black"));
	//color.SetCMYK(JDFCMYKColor(0,0,0,1));
	//color = colorPool.AppendColor();
	//color.SetName(_T("Pantone 375 C"));
	//m_colorantControl = root.AppendMatchingResource(JDFElement::elm_ColorantControl, JDFNode::ProcessUsage_AnyInput);
	///////////////////////////

	JDFAuditPool auditPool=root.GetAuditPool();
	mAttribute attMap;
	JDFAudit audit = auditPool.GetAudit(0, JDFAudit::AuditType_Created, attMap);

	CString strKIMTitle  = theApp.BuildKIMTitle(theApp.strName);
	CString strAgentName = strKIMTitle;
	CString strAgentVersion; strAgentVersion.Format(_T("%s (Build: %s)"), theApp.strVersion, CString(__DATE__));
	switch (m_nCompatibility)
	{
	case CPDFTargetProperties::JDFCompPrinergy2215:		
		{
		CString strAuthor = _T("Creo Preps Version 4.2.1  (407)");
		audit.SetAuthor((KString)strAuthor);
		}
		break;
	case CPDFTargetProperties::JDFCompPrinergy23:		
		{
		CString strAuthor;	strAuthor.Format(_T("%s  %s (Build: %s)"), strKIMTitle, theApp.strVersion, CString(__DATE__));
		audit.SetAuthor((WString)strAuthor);
		}
		break;
	case CPDFTargetProperties::JDFComp12:		
	case CPDFTargetProperties::JDFComp13:		
		audit.SetAgentName((WString)strAgentName);
		audit.SetAgentVersion((WString)strAgentVersion);
		break;
	}

	auditPool = m_impositionNode.AppendAuditPool();
	audit	  = auditPool.AddCreated();
	audit.SetAgentName((WString)strAgentName);
	audit.SetAgentVersion((WString)strAgentVersion);

	auditPool = m_cuttingGroup.AppendAuditPool();
	audit	  = auditPool.AddCreated();
	audit.SetAgentName((WString)strAgentName);
	audit.SetAgentVersion((WString)strAgentVersion);
	
	return TRUE;
}

BOOL CJDFOutputImposition::Open(CBookblock* pBookblock, BOOL& bOverwriteAll, int nSide)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return FALSE; 

	m_strTmpOutputJDFPath = pBookblock->GetOutFileTmpFullPath((nSide == ALLSIDES) ? BOTHSIDES : nSide, NULL);
	m_strOutputJDFPath	  = pBookblock->GetOutFileFullPath	 ((nSide == ALLSIDES) ? BOTHSIDES : nSide, NULL);
	m_strOutputFilename	  = pBookblock->GetOutFilename		 ((nSide == ALLSIDES) ? BOTHSIDES : nSide, NULL);

	if (m_bJDFOutputMime)
	{
		m_strOutputMimePath = m_strOutputJDFPath;
		m_strOutputJDFPath = m_strTmpOutputJDFPath;
		theApp.RemoveExtension(m_strOutputJDFPath);		// change .mjd extension to .jdf
		m_strOutputJDFPath += _T(".jdf");
	}

	m_nMarkOrd = 0;

	CFileStatus	fileStatus;
	if ( (bOverwriteAll == -1) || ! bOverwriteAll)
	{
		BOOL bFileExists = FALSE;
		if (CFile::GetStatus(m_strOutputJDFPath, fileStatus))	// target file exists
			bFileExists = TRUE;

		if (bFileExists)
		{
			int nRet = IDC_USE_EXISTING;
			if (bOverwriteAll == -1)
			{
				CDlgConfirmPDFSheetOverwrite dlg;
				dlg.m_strPDFFilename = m_strOutputJDFPath;
				nRet = dlg.DoModal();
			}
			switch (nRet)
			{
			case IDC_NEW:			bOverwriteAll = TRUE;
									break;
			case IDC_USE_EXISTING:	bOverwriteAll = FALSE;
									m_bCancelJob = TRUE;
									return FALSE;
			case IDCANCEL:			m_bCancelJob = TRUE;
									return FALSE;
			}
		}
	}

	m_nCurrentPage = 1;

	//theApp.m_pDlgPDFLibStatus->m_strFilename = theApp.m_pDlgPDFLibStatus->m_strFullFilename = m_strOutputJDFPath;
	//theApp.m_pDlgPDFLibStatus->UpdateStatus(CDlgPDFLibStatus::OpenDocument);

	
	m_pJDFDoc = new JDFDoc(0);
	JDFNode root = m_pJDFDoc->GetRoot();

	JDFOUT_AppendApogeeParameterSets(root);

	root.init();
	if (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy2215)
		root.SetVersion(_T("1.0"));
	else
		if (m_nCompatibility != CPDFTargetProperties::JDFComp13)
			root.SetVersion(_T("1.2"));

	CString	strJobTitle = theApp.RemoveFileExtension(pDoc->GetTitle(), _T(".job") ); 

	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)m_strJDFJobDescriptiveNameMap, NULL, BOTHSIDES, NULL, -1, "", -1, -1);	
	root.SetDescriptiveName(( ! _tcslen(szDest)) ? (KString)strJobTitle : (KString)szDest);

	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)m_strJDFJobPartIDMap, NULL, BOTHSIDES, NULL, -1, "", -1, -1);	
	root.SetJobPartID(( ! _tcslen(szDest)) ? (KString)strJobTitle : (KString)szDest);

	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)m_strJDFJobIDMap, NULL, BOTHSIDES, NULL, -1, "", -1, -1);	
	if ( ! _tcslen(szDest))
		if ( ! pDoc->m_GlobalData.m_strJobNumber.IsEmpty())
			root.SetJobID((KString)pDoc->m_GlobalData.m_strJobNumber);
		else
			root.SetJobID((KString)strJobTitle);
	else
		root.SetJobID((KString)szDest);

	root.SetMaxVersion(_T("1.3"));
	root.SetICSVersions(vWString(_T("Base_L0-1.3 LayCrImp_L1-1.3")));

	//root.SetTypes(vWString(_T("Imposition Cutting")));	//doesn't pass CheckJDF

	switch (m_nCompatibility)
	{
	case CPDFTargetProperties::JDFCompPrinergy2215:		
		root.SetAttribute(_T("xmlns:CPC"), _T("http://www.creo.com/CPC/JDFExtensions.xsd"));
		root.SetAttribute(_T("xmlns:SSi"), _T("//SSiJDFExtensions.xsd"));
		break;
	case CPDFTargetProperties::JDFCompPrinergy23:		
		root.SetAttribute(_T("xmlns"), _T("http://www.CIP4.org/JDFSchema_1_1"));
		root.SetAttribute(_T("xmlns:SSi"), _T("//SSiJDFExtensions.xsd"));
		break;
	case CPDFTargetProperties::JDFComp12:		
		root.SetAttribute(_T("xmlns"), _T("http://www.CIP4.org/JDFSchema_1_1"));
		break;
	}

	root.SetEnumType(JDFNode::Type_ProcessGroup);
	m_impositionNode = root.AddJDFNode(JDF::JDFNode::Type_Imposition);
	m_impositionNode.SetDescriptiveName(_T("Imposition"));
	m_impositionNode.SetID(_T("ProcessImposition"));

	JDFAuditPool auditPool=root.GetAuditPool();
	mAttribute attMap;
	JDFAudit audit = auditPool.GetAudit(0, JDFAudit::AuditType_Created, attMap);

	CString strKIMTitle  = theApp.BuildKIMTitle(theApp.strName);
	CString strAgentName = strKIMTitle;
	CString strAgentVersion; strAgentVersion.Format(_T("%s (Build: %s)"), theApp.strVersion, CString(__DATE__));
	switch (m_nCompatibility)
	{
	case CPDFTargetProperties::JDFCompPrinergy2215:		
		{
		CString strAuthor = _T("Creo Preps Version 4.2.1  (407)");
		audit.SetAuthor((KString)strAuthor);
		}
		break;
	case CPDFTargetProperties::JDFCompPrinergy23:		
		{
		CString strAuthor;	strAuthor.Format(_T("%s  %s (Build: %s)"), strKIMTitle, theApp.strVersion, CString(__DATE__));
		audit.SetAuthor((WString)strAuthor);
		}
		break;
	case CPDFTargetProperties::JDFComp12:		
	case CPDFTargetProperties::JDFComp13:		
		audit.SetAgentName((WString)strAgentName);
		audit.SetAgentVersion((WString)strAgentVersion);
		break;
	}

	auditPool = m_impositionNode.AppendAuditPool();
	audit	  = auditPool.AddCreated();
	audit.SetAgentName((WString)strAgentName);
	audit.SetAgentVersion((WString)strAgentVersion);
	
	return TRUE;
}

void CJDFOutputImposition::Close()
{
	CPrintSheetTableView*  pPrintSheetTableView  = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if (pPrintSheetTableView)
	{
		pPrintSheetTableView->StopPDFOutFeedback();
		pPrintSheetTableView->m_strPDFOutputMessage.LoadString(IDS_CLOSE_PDF_OUTPUT);
		pPrintSheetTableView->UpdateData(FALSE);
	}

	if (pPrintSheetTableView)
		pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_SHOW);

	//SetProgressFunction( ProgressClose, (long)pPrintSheetTableView);
	if (pPrintSheetTableView)
		pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_HIDE);

	if (pPrintSheetTableView)
		pPrintSheetTableView->LowlightPDFOutFile();

	JDFOUT_AppendOutputRunList();

	HANDLE hFile = CreateFile(m_strOutputJDFPath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
	if (hFile != INVALID_HANDLE_VALUE)	// workaround since exception handling of Write2File does not work:
	{									// check if target file can be created (i.e. path exists) - otherwise we get a crash in Write2File()
		CloseHandle(hFile);
		//try
		{
			m_pJDFDoc->Write2File((WString)m_strOutputJDFPath);
		}
		//catch (JDFException e)
		//{
		//	CString strMsg;
		//	strMsg.Format(_T("JDF API error: %s"), e.getMessage());
		//	KIMOpenLogMessage(strMsg);
		//}
	}

	delete m_pJDFDoc;
}

//void CJDFOutputImposition::Close()
//{
//	CPrintSheetTableView*  pPrintSheetTableView  = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
//	if (pPrintSheetTableView)
//	{
//		pPrintSheetTableView->StopPDFOutFeedback();
//		pPrintSheetTableView->m_strPDFOutputMessage.LoadString(IDS_CLOSE_PDF_OUTPUT);
//		pPrintSheetTableView->UpdateData(FALSE);
//	}
//
//	if (pPrintSheetTableView)
//		pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_SHOW);
//
//	//SetProgressFunction( ProgressClose, (long)pPrintSheetTableView);
//	if (pPrintSheetTableView)
//		pPrintSheetTableView->m_pdfOutputProgress.ShowWindow(SW_HIDE);
//
//	if (pPrintSheetTableView)
//		pPrintSheetTableView->LowlightPDFOutFile();
//
//	JDFOUT_AppendOutputRunList();
//
//	try
//	{
//		m_pJDFDoc->Write2URL((WString)WindowsPath2URL(m_strOutputJDFPath));
//	}
//	catch (JDF::JDFException e)
//	{
//		CString strMsg;
//		strMsg.Format(_T("JDF API error: %s"), e.getMessage().getBytes());
//		KIMOpenLogMessage(strMsg);
//	}
//
//	if (pPrintSheetTableView)
//	{
//		pPrintSheetTableView->m_strPDFOutputMessage  = "";
//		pPrintSheetTableView->UpdateData(FALSE);
//	}
//
//	delete m_pJDFDoc;
//}
//
//IPL_bool_t CJDFOutputImposition::ProgressClose( long lowrange, long highrange, long counter, long lparam)
//{
//	CPrintSheetTableView* pView = (CPrintSheetTableView*)lparam;
//	if (pView)
//	{
//		pView->m_pdfOutputProgress.SetRange((short)lowrange, (short)highrange);
//		pView->m_pdfOutputProgress.SetPos(counter);
//	}
//
//	return TRUE;
//}


void CJDFOutputImposition::JDFOUT_AppendApogeeParameterSets(JDFNode root)
{
	if (m_apogeeParameterSets.IsEmpty())
		return;

	XERCES_CPP_NAMESPACE::DOMDocument* domDoc = m_pJDFDoc->GetDOMDocument(); 
	CString strApogeeTrappingSet	= GetKeyValue(m_apogeeParameterSets, _T("Trapping"));
	CString strApogeeSeparationSet	= GetKeyValue(m_apogeeParameterSets, _T("Separation"));
	CString strApogeeRenderingSet	= GetKeyValue(m_apogeeParameterSets, _T("Rendering"));
	CString strApogeeScreeningSet	= GetKeyValue(m_apogeeParameterSets, _T("Screening"));
	CString strApogeePrintSet		= GetKeyValue(m_apogeeParameterSets, _T("print"));
	CString strApogeeOutputSet		= GetKeyValue(m_apogeeParameterSets, _T("output"));
	CString strApogeeImageSet		= GetKeyValue(m_apogeeParameterSets, _T("image"));
	
	CString strXMLComment = _T(" Commandline:; setNoConsole@true; addNamedFeatures \"");
	if ( ! strApogeeImageSet.IsEmpty())			strXMLComment += _T("image ")		+ strApogeeImageSet;
	if ( ! strApogeeOutputSet.IsEmpty())		strXMLComment += _T(" output ")		+ strApogeeOutputSet;
	if ( ! strApogeePrintSet.IsEmpty())			strXMLComment += _T(" print ")		+ strApogeePrintSet;
	if ( ! strApogeeScreeningSet.IsEmpty())		strXMLComment += _T(" Screening ")	+ strApogeeScreeningSet;
	if ( ! strApogeeRenderingSet.IsEmpty())		strXMLComment += _T(" Rendering ")	+ strApogeeRenderingSet;
	if ( ! strApogeeSeparationSet.IsEmpty())	strXMLComment += _T(" Separation ")	+ strApogeeSeparationSet;
	if ( ! strApogeeTrappingSet.IsEmpty())		strXMLComment += _T(" Trapping ")	+ strApogeeTrappingSet;
	strXMLComment += _T("\" ");

	XERCES_CPP_NAMESPACE::DOMComment* comment = domDoc->createComment(WString(strXMLComment).c_str());
	XERCES_CPP_NAMESPACE::DOMElement* rootElement = root.GetDOMElement();
	domDoc->insertBefore((XERCES_CPP_NAMESPACE::DOMNode*)comment, (XERCES_CPP_NAMESPACE::DOMNode*)rootElement);
}

void CJDFOutputImposition::JDFOUT_AppendBindingIntent()
{
	return;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return; 
	if ( ! pDoc->m_boundProducts.GetSize())
		return;

	m_bindingIntent = m_impositionNode.AppendMatchingResource(JDFElement::elm_BindingIntent, JDFNode::ProcessUsage_AnyInput);
	m_bindingIntent.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	m_bindingIntent.SetStatus(JDF::JDFResource::Status_Available);

	if (pDoc->GetBoundProduct(0).m_bindingParams.m_bindingDefs.m_nBinding == CBindingDefs::PerfectBound)
	{
		m_bindingIntent.AppendAttribute(_T("BindingOrder"), _T("Gathering"));
		JDFSpanBindingType bindingType = m_bindingIntent.AppendBindingType();
		bindingType.SetActual(JDFSpanBindingType::SpanBindingType_Adhesive);
	}
	else
	{
		m_bindingIntent.AppendAttribute(_T("BindingOrder"), _T("Collecting"));
		JDFSpanBindingType bindingType = m_bindingIntent.AppendBindingType();
		bindingType.SetActual(JDFSpanBindingType::SpanBindingType_SaddleStitch);
	}
}

void CJDFOutputImposition::JDFOUT_AppendLayout()
{
	JDFNode impositionNode = m_impositionNode; 
	m_JDFLayout = impositionNode.AppendMatchingResource(JDFElement::elm_Layout,JDFNode::ProcessUsage_AnyInput);
	m_JDFLayout.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	m_JDFLayout.SetStatus(JDF::JDFResource::Status_Available);

	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return; 
	//if ( ! pDoc->m_printGroupList.HasBoundedProductGroup())
	//	return;

	//JDFSourceResource sourceResource = m_JDFLayout.AppendSourceResource();
	//KElement bindingIntentRef = sourceResource.AppendElement(_T("BindingIntentRef"));
	//bindingIntentRef.AppendAttribute(_T("rRef"), m_bindingIntent.GetID());
}

void CJDFOutputImposition::JDFOUT_AppendSignature(CPrintSheet& rPrintSheet)
{
	CString strName;

	m_pJDFSignature  = new JDFSignature;
	*m_pJDFSignature = m_JDFLayout.AppendSignature();

	if ( (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy2215) || (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy23) )
		m_pJDFSignature->SetAttribute(_T("SSi:PressRunNo"), (WString)rPrintSheet.GetNumber());//Int2Ascii(rPrintSheet.GetNumberOnly()));	// changed 03/21/2013

	m_pJDFSheet  = new JDFSheet;
	*m_pJDFSheet = m_pJDFSignature->AppendSheet();
	strName.Format(_T("Sheet-%s"), rPrintSheet.GetNumber());

	m_pJDFSheet->SetName((WString)strName);

	CString strCoords = _T("0 0 0 0");
	if (m_media.m_posInfos.GetSize())
	{
		float fXPos, fYPos;
		m_media.m_posInfos[0].m_reflinePosParams.GetSheetPosition(&rPrintSheet, FRONTSIDE, fXPos, fYPos, m_media.GetMediaWidth(&rPrintSheet), m_media.GetMediaHeight(&rPrintSheet));
		float  fWidth		= ( (m_media.m_posInfos[0].m_nRotation == 0) || (m_media.m_posInfos[0].m_nRotation == 180) ) ? m_media.GetMediaWidth(&rPrintSheet)  : m_media.GetMediaHeight(&rPrintSheet);
		float  fHeight		= ( (m_media.m_posInfos[0].m_nRotation == 0) || (m_media.m_posInfos[0].m_nRotation == 180) ) ? m_media.GetMediaHeight(&rPrintSheet) : m_media.GetMediaWidth(&rPrintSheet);
		IPL_TDRECT mediaBox	= {  m_media.m_posInfos[0].m_fCorrectionX/fPSPoint, m_media.m_posInfos[0].m_fCorrectionY/fPSPoint, 
								(fWidth + m_media.m_posInfos[0].m_fCorrectionX)/fPSPoint, (fHeight + m_media.m_posInfos[0].m_fCorrectionY)/fPSPoint };
		strCoords.Format(_T("%.3f %.3f %.3f %.3f"), mediaBox.fr.left, mediaBox.fr.bottom, mediaBox.fr.right, mediaBox.fr.top);
	}
	// set the surface contents box
	m_pJDFSheet->SetSurfaceContentsBox((WString)strCoords);

	CLayout* pLayout = rPrintSheet.GetLayout();
	if (pLayout)
	{
		switch (pLayout->m_nProductType)
		{
			case WORK_AND_BACK:		if (pLayout->KindOfProduction() == WORK_AND_TURN)
										m_pJDFSheet->SetSourceWorkStyle(JDFSheet::SourceWorkStyle_WorkAndBack);		
									else
										m_pJDFSheet->SetSourceWorkStyle(JDFSheet::SourceWorkStyle_Perfecting);		
									break;
			case WORK_AND_TURN:		m_pJDFSheet->SetSourceWorkStyle(JDFSheet::SourceWorkStyle_WorkAndTurn);		break;
			case WORK_AND_TUMBLE:	m_pJDFSheet->SetSourceWorkStyle(JDFSheet::SourceWorkStyle_WorkAndTumble);	break;
			case WORK_SINGLE_SIDE:	m_pJDFSheet->SetSourceWorkStyle(JDFSheet::SourceWorkStyle_Simplex);			break;
			default:				m_pJDFSheet->SetSourceWorkStyle(JDFSheet::SourceWorkStyle_WorkAndBack);		break;
		}
	}
	
	if ( (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy2215) || (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy23) )
	{
		CString  strWorkStyle = _T("SW");
		CLayout* pLayout = rPrintSheet.GetLayout();
		if (pLayout)
		{
			switch (pLayout->m_nProductType)
			{
				case WORK_AND_TURN:		strWorkStyle = _T("TN");	break;
				case WORK_AND_TUMBLE:	strWorkStyle = _T("TB");	break;
				case WORK_SINGLE_SIDE:	strWorkStyle = _T("SS");	break;
				default:											break;
			}
		}
		m_pJDFSheet->SetAttribute(_T("SSi:WorkStyle"), (WString)strWorkStyle);
	}
}

void CJDFOutputImposition::JDFOUT_AppendSignatureBookblock(int nNumber)
{
	CString strName, strNumber;

	strNumber.Format(_T("%d"), nNumber);

	m_pJDFSignature  = new JDFSignature;
	*m_pJDFSignature = m_JDFLayout.AppendSignature();

	if ( (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy2215) || (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy23) )
		m_pJDFSignature->SetAttribute(_T("SSi:PressRunNo"), (WString)strNumber);//Int2Ascii(rPrintSheet.GetNumberOnly()));	// changed 03/21/2013

	m_pJDFSheet  = new JDFSheet;
	*m_pJDFSheet = m_pJDFSignature->AppendSheet();
	strName.Format(_T("Sheet-%s"), strNumber);

	m_pJDFSheet->SetName((WString)strName);

	CString strCoords = _T("0 0 0 0");
	if (m_media.m_posInfos.GetSize())
	{
		float  fWidth		= m_media.GetMediaWidth(NULL);
		float  fHeight		= m_media.GetMediaHeight(NULL);
		IPL_TDRECT mediaBox	= {  m_media.m_posInfos[0].m_fCorrectionX/fPSPoint, m_media.m_posInfos[0].m_fCorrectionY/fPSPoint, (fWidth + m_media.m_posInfos[0].m_fCorrectionX)/fPSPoint, (fHeight + m_media.m_posInfos[0].m_fCorrectionY)/fPSPoint };
		strCoords.Format(_T("%.3f %.3f %.3f %.3f"), mediaBox.fr.left, mediaBox.fr.bottom, mediaBox.fr.right, mediaBox.fr.top);
	}

	m_pJDFSheet->SetSurfaceContentsBox((WString)strCoords);
	m_pJDFSheet->SetSourceWorkStyle(JDFSheet::SourceWorkStyle_WorkAndBack);		
	
	if ( (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy2215) || (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy23) )
	{
		CString  strWorkStyle = _T("SW");
		m_pJDFSheet->SetAttribute(_T("SSi:WorkStyle"), (WString)strWorkStyle);
	}
}

void CJDFOutputImposition::JDFOUT_AppendSurface(CPrintSheet& rPrintSheet, int nSide, IPL_TDRECT& mediaBox)
{
	m_pJDFSurface  = new JDFSurface;
	*m_pJDFSurface = (nSide == FRONTSIDE) ? m_pJDFSheet->AppendFrontSurface() : m_pJDFSheet->AppendBackSurface();

	CLayout*	 pLayout	 = rPrintSheet.GetLayout();
	CLayoutSide* pLayoutSide = (pLayout) ? ( (nSide == FRONTSIDE) ? &pLayout->m_FrontSide : &pLayout->m_BackSide) : NULL;

	CString		 strCoords;
	strCoords.Format(_T("%.3f %.3f %.3f %.3f"), mediaBox.fr.left, mediaBox.fr.bottom, mediaBox.fr.right, mediaBox.fr.top);
	m_pJDFSurface->SetSurfaceContentsBox((WString)strCoords);

	if (pLayoutSide)
	{
		switch (m_nCompatibility)
		{
		case CPDFTargetProperties::JDFCompPrinergy2215:		
			strCoords.Format(_T("%.3f %.3f"), pLayoutSide->m_fPaperWidth/fPSPoint, pLayoutSide->m_fPaperHeight/fPSPoint);
			m_pJDFSurface->SetAttribute(_T("Dimension"), (WString)strCoords);
			strCoords.Format(_T("%.3f %.3f"), pLayoutSide->m_fPaperLeft /fPSPoint, pLayoutSide->m_fPaperBottom/fPSPoint);
			m_pJDFSurface->SetAttribute(_T("MediaOrigin"), (WString)strCoords);
			break;
		case CPDFTargetProperties::JDFCompPrinergy23:		
			strCoords.Format(_T("%.3f %.3f"), pLayoutSide->m_fPaperWidth/fPSPoint, pLayoutSide->m_fPaperHeight/fPSPoint);
			m_pJDFSurface->SetAttribute(_T("SSi:Dimension"), (WString)strCoords);
			strCoords.Format(_T("%.3f %.3f"), pLayoutSide->m_fPaperLeft /fPSPoint, pLayoutSide->m_fPaperBottom/fPSPoint);
			m_pJDFSurface->SetAttribute(_T("SSi:MediaOrigin"), (WString)strCoords);
			break;
		case CPDFTargetProperties::JDFComp12:		
			break;
		}
	}
}

void CJDFOutputImposition::JDFOUT_AppendSurfaceBookblock(int nSide, IPL_TDRECT& mediaBox)
{
	m_pJDFSurface  = new JDFSurface;
	*m_pJDFSurface = (nSide == FRONTSIDE) ? m_pJDFSheet->AppendFrontSurface() : m_pJDFSheet->AppendBackSurface();

	//CLayout*	 pLayout	 = rPrintSheet.GetLayout();
	//CLayoutSide* pLayoutSide = (pLayout) ? ( (nSide == FRONTSIDE) ? &pLayout->m_FrontSide : &pLayout->m_BackSide) : NULL;

	CString		 strCoords;
	strCoords.Format(_T("%.3f %.3f %.3f %.3f"), mediaBox.fr.left, mediaBox.fr.bottom, mediaBox.fr.right, mediaBox.fr.top);
	m_pJDFSurface->SetSurfaceContentsBox((WString)strCoords);

	//if (pLayoutSide)
	//{
	//	switch (m_nCompatibility)
	//	{
	//	case CPDFTargetProperties::JDFCompPrinergy2215:		
	//		strCoords.Format(_T("%.3f %.3f"), pLayoutSide->m_fPaperWidth/fPSPoint, pLayoutSide->m_fPaperHeight/fPSPoint);
	//		m_pJDFSurface->SetAttribute(_T("Dimension"), (WString)strCoords);
	//		strCoords.Format(_T("%.3f %.3f"), pLayoutSide->m_fPaperLeft /fPSPoint, pLayoutSide->m_fPaperBottom/fPSPoint);
	//		m_pJDFSurface->SetAttribute(_T("MediaOrigin"), (WString)strCoords);
	//		break;
	//	case CPDFTargetProperties::JDFCompPrinergy23:		
	//		strCoords.Format(_T("%.3f %.3f"), pLayoutSide->m_fPaperWidth/fPSPoint, pLayoutSide->m_fPaperHeight/fPSPoint);
	//		m_pJDFSurface->SetAttribute(_T("SSi:Dimension"), (WString)strCoords);
	//		strCoords.Format(_T("%.3f %.3f"), pLayoutSide->m_fPaperLeft /fPSPoint, pLayoutSide->m_fPaperBottom/fPSPoint);
	//		m_pJDFSurface->SetAttribute(_T("SSi:MediaOrigin"), (WString)strCoords);
	//		break;
	//	case CPDFTargetProperties::JDFComp12:		
	//		break;
	//	}
	//}
}

void CJDFOutputImposition::JDFOUT_AppendContentObject(int nOrd, IPL_TDMATRIX& ctm, IPL_TDMATRIX& trimctm, IPL_TDRECT& sheetClipBox, IPL_TDRECT& objectTrimBox)
{
	JDFContentObject contentObject = m_pJDFSurface->AppendContentObject();
	CString strCoords;
	strCoords.Format(_T("%.6f %.6f %.6f %.6f %.3f %.3f"), ctm.a, ctm.b, ctm.c, ctm.d, ctm.h, ctm.v);
	contentObject.SetCTM(JDFMatrix((WString)strCoords));
	contentObject.SetOrd(nOrd);
	contentObject.init();
//	contentObject.RemoveType();
	strCoords.Format(_T("%.3f %.3f %.3f %.3f"), sheetClipBox.fr.left, sheetClipBox.fr.bottom, sheetClipBox.fr.right, sheetClipBox.fr.top);
	contentObject.SetClipBox(JDFRectangle((WString)strCoords));

	if ( (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy2215) || (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy23) )
	{
		strCoords.Format(_T("%.3f %.3f %.3f %.3f"), objectTrimBox.fr.left, objectTrimBox.fr.bottom, objectTrimBox.fr.right, objectTrimBox.fr.top);
		contentObject.SetAttribute(_T("SSi:TrimBox"), (WString)strCoords);
	}

	if (m_nCompatibility != CPDFTargetProperties::JDFCompPrinergy2215)
	{
		strCoords.Format(_T("%.6f %.6f %.6f %.6f %.3f %.3f"), trimctm.a, trimctm.b, trimctm.c, trimctm.d, trimctm.h, trimctm.v);
		contentObject.SetAttribute(_T("TrimCTM"),  (WString)strCoords);
		contentObject.SetTrimCTM(JDFMatrix((WString)strCoords));
		strCoords.Format(_T("%.3f %.3f"), fabs(objectTrimBox.fr.right - objectTrimBox.fr.left), fabs(objectTrimBox.fr.top - objectTrimBox.fr.bottom));
		contentObject.SetAttribute(_T("TrimSize"), (WString)strCoords);
		contentObject.SetTrimSize(JDFXYPair((WString)strCoords));
	}
}

void CJDFOutputImposition::JDFOUT_AppendMarkObject(IPL_TDRECT& mediaBox)
{
	// append a markobject 
	JDFMarkObject markObject = m_pJDFSurface->AppendMarkObject();
	markObject.SetCTM(JDFMatrix("1 0 0 1 0 0"));
	markObject.SetOrd(m_nMarkOrd++);
	markObject.init();
//	markObject.RemoveType();
	CString strCoords;
	strCoords.Format(_T("%.3f %.3f %.3f %.3f"), mediaBox.fr.left, mediaBox.fr.bottom, mediaBox.fr.right, mediaBox.fr.top);
	markObject.SetClipBox(JDFRectangle((WString)strCoords));
}

void CJDFOutputImposition::JDFOUT_AppendPagesRunList() 
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc(); 
	if ( ! pDoc)
		return;

	if (m_nCompatibility == CPDFTargetProperties::JDFCompPrinergy2215)
	{
		JDFNode	   impositionNode = m_impositionNode; //m_pJDFDoc->GetRoot();
		JDFRunList docList		  = impositionNode.AppendMatchingResource(JDFElement::elm_RunList,JDFNode::ProcessUsage_Document);
		docList.SetStatus(JDF::JDFResource::Status_Available);
		docList.RemoveAttributes((vWString)_T("AgentName AgentVersion"));

		int nNumPages = (pDoc) ? pDoc->m_PageTemplateList.GetCount() : 0;
		for (int nIndex = 0; nIndex < nNumPages; nIndex++)
		{
			CString strFilename(theApp.settings.m_szPDFInputFolder + CString(_T("\\")) + CString(_T("blankpage.pdf")) );
			JDFRunList run = docList.AddRun((WString)WindowsPath2URL(strFilename, FALSE), 0, 0);

			CString strIndex;
			strIndex.Format(_T("%d"), nIndex);
			run.SetRun((KString)strIndex);
			JDFLayoutElement layoutElement = run.GetLayoutElement();
			JDFFileSpec		 fileSpec = layoutElement.GetFileSpec();
			fileSpec.SetMimeType(_T("application/pdf"));
		}
	}
	else
	{
		JDFNode	   impositionNode = m_impositionNode; //m_pJDFDoc->GetRoot();
		JDFRunList docList		  = impositionNode.AppendMatchingResource(JDFElement::elm_RunList,JDFNode::ProcessUsage_Document);
		docList.SetStatus(JDF::JDFResource::Status_Available);
		docList.RemoveAttributes((vWString)_T("AgentName AgentVersion"));

		int		 nRunIndex = 0;
		POSITION pos	   = pDoc->m_PageTemplateList.GetHeadPosition();
		while (pos)
		{
			CString strRunIndex;
			strRunIndex.Format(_T("%d"), nRunIndex);

			CPageTemplate& rTemplate = pDoc->m_PageTemplateList.CList::GetNext(pos);
			if ( ! rTemplate.FlatProductHasBackSide() && rTemplate.m_bLabelBackSide)
				continue;

			SEPARATIONINFO* pSepInfo = (rTemplate.m_ColorSeparations.GetSize()) ? &rTemplate.m_ColorSeparations[0] : NULL;
			CPageSource* pPageSource = rTemplate.GetObjectSource((pSepInfo) ? pSepInfo->m_nColorDefinitionIndex : -1);
			if (pPageSource)
			{
				int nFirstPage = pSepInfo->m_nPageNumInSourceFile - 1;
				int nLastPage  = nFirstPage;
				pos = GetPageListSequence(pos, pPageSource, nLastPage);

				JDFRunList run = docList.AddRun((KString)WindowsPath2URL(pPageSource->m_strDocumentFullpath, FALSE), nFirstPage, nLastPage);
				run.SetRun((KString)strRunIndex);
				JDFLayoutElement layoutElement = run.GetLayoutElement();
				JDFFileSpec		 fileSpec = layoutElement.GetFileSpec();
				fileSpec.SetMimeType(_T("application/pdf"));
			}
			else
			{
				int nFirstPage = 0;
				int nLastPage  = nFirstPage;
				pos = GetPageListSequence(pos, NULL, nLastPage);

				JDFRunList run = docList.AddRun(KString(""), 0, nLastPage);
				run.SetRun((KString)strRunIndex);
				JDFLayoutElement layoutElement = run.GetLayoutElement();
				layoutElement.SetElementType(JDFAutoLayoutElement::ElementType_Reservation);
				JDFFileSpec		 fileSpec = layoutElement.GetFileSpec();
				fileSpec.SetMimeType(_T("application/pdf"));
			}
			nRunIndex++;
		}
	}
}

POSITION CJDFOutputImposition::GetPageListSequence(POSITION pos, CPageSource* pRefPageSource, int& nLastPage)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc(); 
	if ( ! pDoc)
		return pos;

	while (pos)
	{
		CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetAt(pos);
		
		if ( ! rTemplate.FlatProductHasBackSide() && rTemplate.m_bLabelBackSide)
		{
			pDoc->m_PageTemplateList.CList::GetNext(pos);
			continue;
		}

		SEPARATIONINFO* pSepInfo = (rTemplate.m_ColorSeparations.GetSize()) ? &rTemplate.m_ColorSeparations[0] : NULL;
		if (pSepInfo)
		{
			CPageSource* pPageSource = rTemplate.GetObjectSource((pSepInfo) ? pSepInfo->m_nColorDefinitionIndex : -1);
			if (pPageSource == pRefPageSource)
				if ( (pSepInfo->m_nPageNumInSourceFile - 1) == (nLastPage + 1) )
					nLastPage++;
				else
					return pos;
			else
				return pos;
		}
		else
			if ( ! pRefPageSource)
				nLastPage++;
			else
				return pos;

		pDoc->m_PageTemplateList.CList::GetNext(pos);
	}
	return NULL;
}

void CJDFOutputImposition::JDFOUT_AppendMarksRunList()
{
	JDFNode	   impositionNode = m_impositionNode; //m_pJDFDoc->GetRoot();
	JDFRunList markList		  = impositionNode.AppendMatchingResource(JDFElement::elm_RunList,JDFNode::ProcessUsage_Marks);
	markList.SetStatus(JDF::JDFResource::Status_Available);
	markList.RemoveAttributes((vWString)_T("AgentName AgentVersion"));

	CString strFilename;
	strFilename = m_strOutputFilename;
	theApp.RemoveExtension(strFilename);
	strFilename += _T("_Marks.pdf");

	CString strPath;
	if (m_bJDFOutputMime)
		strPath.Format(_T("cid:%s"), strFilename);
	else
		if (m_bMarksInFolder &&  ! m_strMarksFolder.IsEmpty())
			strPath = m_strMarksFolder + _T("\\") + strFilename;
		else
			strPath = strFilename;

	JDFRunList run = markList.AddRun((KString)WindowsPath2URL(strPath, FALSE), 0, m_nMarkOrd - 1);
	run.SetRun(_T("0"));
	JDFLayoutElement layoutElement = run.GetLayoutElement();
	JDFFileSpec		 fileSpec = layoutElement.GetFileSpec();
	fileSpec.SetMimeType(_T("application/pdf"));
}

void CJDFOutputImposition::JDFOUT_AppendOutputRunList()
{
	JDFNode	   impositionNode = m_impositionNode; //m_pJDFDoc->GetRoot();
	JDFRunList outList		  = impositionNode.AppendMatchingResource(JDFElement::elm_RunList,JDFNode::ProcessUsage_AnyOutput);
	outList.RemoveAttributes((vWString)_T("AgentName AgentVersion"));
	//JDFLayoutElement outle		 = outList.AppendLayoutElement();
	//JDFFileSpec		 outfilespec = outle.AppendFileSpec();
	//outfilespec.SetURL("file:///outputFile");
} 

void CJDFOutputImposition::JDFOUT_AppendCuttingParams(CPrintSheet* pPrintSheet)
{
	if ( ! pPrintSheet)		
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;

	CString strID;
	strID.Format(_T("Signature %s"), pPrintSheet->GetNumber());
	JDFComponent compSignature = m_componentSheet.AddPartition(JDF::JDFResource::PartIDKey_SignatureName, (WString)strID);  
	strID.Format(_T("Sheet %s"), pPrintSheet->GetNumber());
	JDFComponent compSheet = compSignature.AddPartition(JDF::JDFResource::PartIDKey_SheetName, (WString)strID);
	compSheet.SetDimensions(JDFShape(pLayout->m_FrontSide.m_fPaperWidth / fPSPoint, pLayout->m_FrontSide.m_fPaperHeight / fPSPoint, 0.0f));

	strID.Format(_T("Signature %s"), pPrintSheet->GetNumber());
	JDFCuttingParams cuttingParamsSignature = m_cuttingParams.AddPartition(JDF::JDFResource::PartIDKey_SignatureName, (WString)strID);  
	strID.Format(_T("Sheet %s"), pPrintSheet->GetNumber());
	JDFCuttingParams cuttingParamsSheet = cuttingParamsSignature.AddPartition(JDF::JDFResource::PartIDKey_SheetName, (WString)strID);

	if (theApp.m_nAddOnsLicensed & CImpManApp::JDFCutting)
	{
		POSITION pos	   = pLayout->m_FrontSide.m_cutBlocks.GetHeadPosition();
		int		 nBlockNum = 1;
		while (pos)
		{
			int nNX, nNY;
			float fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight; 

			CLayoutObject& rCutBlockObject = pLayout->m_FrontSide.m_cutBlocks.GetNext(pos);
			rCutBlockObject.GetCurrentGrossCutBlock(pPrintSheet, fCropBoxX, fCropBoxY, fCropBoxWidth, fCropBoxHeight);
			rCutBlockObject.GetCutblockMatrix(nNX, nNY);

			JDFCutBlock cutBlock = cuttingParamsSheet.AppendCutBlock();
			cutBlock.SetBlockType(JDF::JDFAutoCutBlock::BlockType_CutBlock);
			strID.Format(_T("B %d"), nBlockNum++);
			cutBlock.SetBlockName((WString)strID);
			float fBlockSizeX = fCropBoxWidth  / nNX / fPSPoint;
			float fBlockSizeY = fCropBoxHeight / nNY / fPSPoint;
			cutBlock.SetBlockSize(JDFXYPair(fBlockSizeX, fBlockSizeY));
			cutBlock.SetBlockTrf(JDFMatrix(1.0f, 0.0f, 0.0f, 1.0f, fCropBoxX / fPSPoint, fCropBoxY / fPSPoint));
			cutBlock.SetBlockSubdivision(JDFXYPair((double)nNX, (double)nNY));
		}
	}
}

BOOL CJDFOutputImposition::ProcessPrintSheet(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll) 
{
	if (!pPrintSheet)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return TRUE;

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(pPrintSheet->m_strPDFTarget);
	if ( ! pTargetProps)
		return TRUE;

	int nRet = TRUE;
	switch (pPrintSheet->GetOutfileRelation(pTargetProps->m_strOutputFilename))
	{
	case CPrintSheet::SheetPerFile: nRet = ProcessPrintSheetAllInOne (pPrintSheet, bOverwriteAll);	break;
	case CPrintSheet::SidePerFile:	nRet = ProcessPrintSheetSidewise (pPrintSheet, bOverwriteAll);	break;
	case CPrintSheet::PlatePerFile:	nRet = ProcessPrintSheetPlatewise(pPrintSheet, bOverwriteAll);	break;
	}

	return nRet;
}

BOOL CJDFOutputImposition::ProcessPrintSheetsJobInOne(CPrintSheetList* pPrintSheetList, BOOL& bOverwriteAll, BOOL bAutoCmd, CPrintSheet** pCurPrintSheet, int* pnSheetsOut) 
{
	CPrintSheet* pPrintSheet = &pPrintSheetList->GetHead();
	if (pCurPrintSheet)
		pPrintSheet = (*pCurPrintSheet) ? *pCurPrintSheet : &pPrintSheetList->GetHead();
	if ( ! pPrintSheet)
		return m_bCancelJob;

	CPrintSheet* pStartPrintSheet = pPrintSheet;
	CPlate*		 pPlate			  = &pPrintSheet->m_FrontSidePlates.m_defaultPlate; 
	if ( ! Open(pPrintSheet, bOverwriteAll, ALLSIDES, pPlate))
		return m_bCancelJob;

	if (pnSheetsOut)
		*pnSheetsOut = 0;

	JDFOUT_AppendBindingIntent();
	JDFOUT_AppendLayout();
//	JDFOUT_AppendCuttingParams();

	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));

	POSITION pos = pPrintSheetList->GetHeadPosition();
	if (pCurPrintSheet)
		pos = (*pCurPrintSheet) ? pPrintSheetList->GetPos(**pCurPrintSheet) : pPrintSheetList->GetHeadPosition();
	while (pos && ! m_bCancelJob)
	{
		CPrintSheet& rPrintSheet = pPrintSheetList->GetNext(pos);
		if (pCurPrintSheet)
			*pCurPrintSheet = &rPrintSheet;

		if ( ! bAutoCmd)
			if (rPrintSheet.IsOutputDisabled())
				continue;

		if (CheckTargetProps(rPrintSheet))
		{
			m_bCancelJob = 1;	// abort current sheet
			pos = pPrintSheetList->GetPos(rPrintSheet);
			pPrintSheetList->GetPrev(pos);			// go back to previous in order to let calling function resume with this sheet
			if (pos)
				if (pCurPrintSheet)
					*pCurPrintSheet = &pPrintSheetList->GetAt(pos);	
			break;
		}
		

		JDFOUT_AppendSignature(rPrintSheet);
		JDFOUT_AppendCuttingParams(&rPrintSheet);

		POSITION platePos = rPrintSheet.m_FrontSidePlates.GetHeadPosition();
		while ( ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? rPrintSheet.m_FrontSidePlates.GetNext(platePos) : rPrintSheet.m_FrontSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
			}
			if ( ! platePos)
				break;
		}

		if (rPrintSheet.GetLayout()->m_nProductType == WORK_AND_BACK)	// work and turn / work and tumble / flat work is single sided
		{
			platePos = rPrintSheet.m_BackSidePlates.GetHeadPosition();
			while ( ! m_bCancelJob)
			{
				CPlate& rPlate = (platePos) ? rPrintSheet.m_BackSidePlates.GetNext(platePos) : rPrintSheet.m_BackSidePlates.m_defaultPlate;
				if ( ! rPlate.m_bOutputDisabled)
				{
					rPlate.m_OutputDate.m_dt = 0.0f;
					if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
						rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
				}
				if ( ! platePos)
					break;
			}
		}

		if (m_bCancelJob == TRUE)
			m_bCancelJob = 0;

		if (pnSheetsOut)
			if (m_nPlatesProcessed)
				(*pnSheetsOut)++;
	}

	JDFOUT_AppendPagesRunList();
	JDFOUT_AppendMarksRunList();

	Close();

	if (m_bCancelJob != 2)
	{
		// output marks to pdf file
		CPDFTargetProperties targetProps(CPDFTargetProperties::TypeJDFFolder, m_strTargetName, GetMarksfileLocation(), m_media);
		targetProps.m_strOutputFilename = m_strFilenameTemplate;
		targetProps.m_bJDFOutputMime = m_bJDFOutputMime;
		CPDFOutput pdfOutput(targetProps, _T(""));
		BOOL bPDFOverwriteAll = TRUE;
		m_bCancelJob = pdfOutput.ProcessPrintSheetsJobInOne(pPrintSheetList, bPDFOverwriteAll, FALSE, FALSE, TRUE, &pStartPrintSheet);	// not AutoCmd, not SendIfPrinter, marks only
	}

	CreateMimeFile();

	return m_bCancelJob;
}

CString CJDFOutputImposition::GetMarksfileLocation()
{
	if (m_bJDFOutputMime)
		return theApp.settings.m_szTempDir;

	CString strLocation = m_strLocation;

	if (m_bMarksInFolder && ! m_strMarksFolder.IsEmpty())
	{
		if (PathIsRelative(m_strMarksFolder))
			strLocation = m_strLocation + _T("\\") + m_strMarksFolder;
		else
			strLocation = m_strMarksFolder;
	}

	return strLocation;
}

BOOL CJDFOutputImposition::CheckTargetProps(CPrintSheet& rPrintSheet)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
	if ( ! pTargetProps)
		return TRUE;

	if (m_nTargetType		  != pTargetProps->m_nType)
		return TRUE;
	if (m_strFilenameTemplate != pTargetProps->m_strOutputFilename)
		return TRUE;
	if (m_strLocation		  != pTargetProps->m_strLocation)
		return TRUE;
	//if (m_nCompatibility	  != pTargetProps->m_nJDFCompatibility)
	//	return TRUE;
	//if (m_bMarksInFolder	  != pTargetProps->m_bJDFMarksInFolder)
	//	return TRUE;
	//if (m_strMarksFolder	  != pTargetProps->m_strJDFMarksFolder)
	//	return TRUE;

	m_media	= pTargetProps->m_mediaList[0];

	return FALSE;
}

BOOL CJDFOutputImposition::ProcessPrintSheetAllInOne(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll) 
{
	if (Open(pPrintSheet, bOverwriteAll, BOTHSIDES))
	{
		JDFOUT_AppendLayout();
		JDFOUT_AppendSignature(*pPrintSheet);
		JDFOUT_AppendCuttingParams(pPrintSheet);

		POSITION platePos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
		while ( ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? pPrintSheet->m_FrontSidePlates.GetNext(platePos) : pPrintSheet->m_FrontSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
			}
			if ( ! platePos)
				break;
		}

		if (pPrintSheet->GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
		{
			JDFOUT_AppendPagesRunList();
			JDFOUT_AppendMarksRunList();

			Close();

			//if ( ! m_bCancelJob && (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp))
			if ( ! m_bCancelJob)
			{
				// output marks to pdf file
				CPDFTargetProperties targetProps(CPDFTargetProperties::TypePDFFolder, _T("JDF Marks Output"), GetMarksfileLocation(), m_media);
				targetProps.m_bJDFOutputMime = m_bJDFOutputMime;
				CPDFOutput pdfOutput(targetProps, _T(""));
				BOOL bPDFOverwriteAll = TRUE;
				m_bCancelJob = pdfOutput.ProcessPrintSheetAllInOne(pPrintSheet, bPDFOverwriteAll, FALSE, FALSE, TRUE);	// not AutoCmd, not SendIfPrinter, Marks only
			}

			CreateMimeFile();

			return m_bCancelJob;
		}

		platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
		while ( ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? pPrintSheet->m_BackSidePlates.GetNext(platePos) : pPrintSheet->m_BackSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
			}
			if ( ! platePos)
				break;
		}

		JDFOUT_AppendPagesRunList();
		JDFOUT_AppendMarksRunList();

		Close();

		//if ( ! m_bCancelJob && (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp))
		if ( ! m_bCancelJob)
		{
			// output marks to pdf file
			CPDFTargetProperties targetProps(CPDFTargetProperties::TypePDFFolder, _T("JDF Marks Output"), GetMarksfileLocation(), m_media);
			targetProps.m_bJDFOutputMime = m_bJDFOutputMime;
			CPDFOutput pdfOutput(targetProps, _T(""));
			BOOL bPDFOverwriteAll = TRUE;
			m_bCancelJob = pdfOutput.ProcessPrintSheetAllInOne(pPrintSheet, bPDFOverwriteAll, FALSE, FALSE, TRUE);	// not AutoCmd, not SendIfPrinter, Marks only
		}

		CreateMimeFile();
	}
	return m_bCancelJob;
}

BOOL CJDFOutputImposition::ProcessPrintSheetSidewise(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll) 
{
	if (Open(pPrintSheet, bOverwriteAll, FRONTSIDE, (pPrintSheet->m_FrontSidePlates.GetCount()) ? &pPrintSheet->m_FrontSidePlates.GetHead() : &pPrintSheet->m_FrontSidePlates.m_defaultPlate))
	{
		JDFOUT_AppendLayout();
		JDFOUT_AppendSignature(*pPrintSheet);
		JDFOUT_AppendCuttingParams(pPrintSheet);

		POSITION platePos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
		while ( ! m_bCancelJob)
		{
			CPlate& rPlate = (platePos) ? pPrintSheet->m_FrontSidePlates.GetNext(platePos) : pPrintSheet->m_FrontSidePlates.m_defaultPlate;
			if ( ! rPlate.m_bOutputDisabled)
			{
				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
			}
			if ( ! platePos)
				break;
		}

		JDFOUT_AppendPagesRunList();
		JDFOUT_AppendMarksRunList();

		Close();
	}

	if (pPrintSheet->GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
	{
		//if ( ! m_bCancelJob && (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp))
		if ( ! m_bCancelJob)
		{
			// output marks to pdf file
			CPDFTargetProperties targetProps(CPDFTargetProperties::TypePDFFolder, _T("JDF Marks Output"), GetMarksfileLocation(), m_media);
			targetProps.m_bJDFOutputMime = m_bJDFOutputMime;
			CPDFOutput pdfOutput(targetProps, _T(""));
			BOOL bPDFOverwriteAll = TRUE;
			m_bCancelJob = pdfOutput.ProcessPrintSheetSidewise(pPrintSheet, bPDFOverwriteAll, FALSE, FALSE, TRUE);	// not AutoCmd, not SendIfPrinter, Marks only
		}
		return m_bCancelJob;
	}

	CreateMimeFile();

	if ( ! m_bCancelJob)
	{
		if (Open(pPrintSheet, bOverwriteAll, BACKSIDE, (pPrintSheet->m_BackSidePlates.GetCount()) ? &pPrintSheet->m_BackSidePlates.GetHead() : &pPrintSheet->m_BackSidePlates.m_defaultPlate))
		{
			JDFOUT_AppendLayout();
			JDFOUT_AppendSignature(*pPrintSheet);

			POSITION platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
			while ( ! m_bCancelJob)
			{
				CPlate& rPlate = (platePos) ? pPrintSheet->m_BackSidePlates.GetNext(platePos) : pPrintSheet->m_BackSidePlates.m_defaultPlate;
				if ( ! rPlate.m_bOutputDisabled)
				{
					rPlate.m_OutputDate.m_dt = 0.0f;
					if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
						rPlate.m_OutputDate = COleDateTime::GetCurrentTime();
				}
				if ( ! platePos)
					break;
			}

			JDFOUT_AppendPagesRunList();
			JDFOUT_AppendMarksRunList();

			Close();
		}

		//if ( ! m_bCancelJob && (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp))
		if ( ! m_bCancelJob)
		{
			// output marks to pdf file
			CPDFTargetProperties targetProps(CPDFTargetProperties::TypePDFFolder, _T("JDF Marks Output"), GetMarksfileLocation(), m_media);
			targetProps.m_bJDFOutputMime = m_bJDFOutputMime;
			CPDFOutput pdfOutput(targetProps, _T(""));
			BOOL bPDFOverwriteAll = TRUE;
			m_bCancelJob = pdfOutput.ProcessPrintSheetSidewise(pPrintSheet, bPDFOverwriteAll, FALSE, FALSE, TRUE);	// not AutoCmd, not SendIfPrinter, Marks only
		}

		CreateMimeFile();
	}

	return m_bCancelJob;
}


BOOL CJDFOutputImposition::ProcessPrintSheetPlatewise(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll) 
{
	POSITION platePos = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
	while ( ! m_bCancelJob)
	{
		CPlate& rPlate = (platePos) ? pPrintSheet->m_FrontSidePlates.GetNext(platePos) : pPrintSheet->m_FrontSidePlates.m_defaultPlate;
		if ( ! rPlate.m_bOutputDisabled)
		{
			if (Open(pPrintSheet, bOverwriteAll, -1, &rPlate))
			{
				JDFOUT_AppendLayout();
				JDFOUT_AppendSignature(*pPrintSheet);

				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();

				JDFOUT_AppendPagesRunList();
				JDFOUT_AppendMarksRunList();

				Close();
			}
		}
		if ( ! platePos)
			break;
	}

	if (pPrintSheet->GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
	{
		//if ( ! m_bCancelJob && (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp))
		if ( ! m_bCancelJob)
		{
			// output marks to pdf file
			CPDFTargetProperties targetProps(CPDFTargetProperties::TypePDFFolder, _T("JDF Marks Output"), GetMarksfileLocation(), m_media);
			targetProps.m_bJDFOutputMime = m_bJDFOutputMime;
			CPDFOutput pdfOutput(targetProps, _T(""));
			BOOL bPDFOverwriteAll = TRUE;
			m_bCancelJob = pdfOutput.ProcessPrintSheetPlatewise(pPrintSheet, bPDFOverwriteAll, FALSE, FALSE, TRUE);	// not AutoCmd, not SendIfPrinter, Marks only
		}
		return m_bCancelJob;
	}

	platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
	while ( ! m_bCancelJob)
	{
		CPlate& rPlate = (platePos) ? pPrintSheet->m_BackSidePlates.GetNext(platePos) : pPrintSheet->m_BackSidePlates.m_defaultPlate;
		if ( ! rPlate.m_bOutputDisabled)
		{
			if (Open(pPrintSheet, bOverwriteAll, -1, &rPlate))
			{
				JDFOUT_AppendLayout();
				JDFOUT_AppendSignature(*pPrintSheet);

				rPlate.m_OutputDate.m_dt = 0.0f;
				if ( ! (m_bCancelJob = ProcessPlate(&rPlate)) )
					rPlate.m_OutputDate = COleDateTime::GetCurrentTime();

				JDFOUT_AppendPagesRunList();
				JDFOUT_AppendMarksRunList();

				Close();
			}
		}
		if ( ! platePos)
			break;
	}

	//if ( ! m_bCancelJob && (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp))
	if ( ! m_bCancelJob)
	{
		// output marks to pdf file
		CPDFTargetProperties targetProps(CPDFTargetProperties::TypePDFFolder, _T("JDF Marks Output"), GetMarksfileLocation(), m_media);
		targetProps.m_bJDFOutputMime = m_bJDFOutputMime;
		CPDFOutput pdfOutput(targetProps, _T(""));
		BOOL bPDFOverwriteAll = TRUE;
		m_bCancelJob = pdfOutput.ProcessPrintSheetPlatewise(pPrintSheet, bPDFOverwriteAll, FALSE, FALSE, TRUE);	// not AutoCmd, not SendIfPrinter, Marks only
	}

	return m_bCancelJob;
}


//////////////////////////// booklet


CDlgBookblockProofProgress g_dlgMarksBookblockProofProgress;

BOOL OutputProofProgessCallback(int nTotalPages, int nCurrentPage, const CString& strFoldSheetNumber, LPVOID lpData)
{
	if (g_dlgMarksBookblockProofProgress.m_strProgressText != strFoldSheetNumber)
	{
		g_dlgMarksBookblockProofProgress.m_strProgressText = strFoldSheetNumber;
		g_dlgMarksBookblockProofProgress.UpdateData(FALSE);
	}
	g_dlgMarksBookblockProofProgress.m_progressBar.SetRange((short)1, (short)nTotalPages);
	g_dlgMarksBookblockProofProgress.m_progressBar.SetPos(nCurrentPage);

	MSG msg;
	if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		if ( (msg.message == WM_KEYDOWN) && (msg.wParam == VK_ESCAPE) )
			if (KIMOpenLogMessage(IDS_PDFLIB_REALLY_CANCEL, MB_YESNO, IDNO) == IDYES)
				return TRUE;
	}

	return FALSE;
}


BOOL CJDFOutputImposition::ProcessBookblockJobInOne(CBookblock* pBookblock, BOOL& bOverwriteAll, BOOL bAutoCmd, BOOL (*pFnProgressCallback)(int nTotalPages, int nCurrentPage, const CString& strFoldSheetNumber, LPVOID lpData)) 
{
	pBookblock->m_OutputDate.m_dt = 0;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return m_bCancelJob;
	if ( ! pDoc->m_PrintSheetList.GetCount())
		return m_bCancelJob;

	if ( ! Open(pBookblock, bOverwriteAll, ALLSIDES))
		return m_bCancelJob;

	JDFOUT_AppendLayout();

	int nSide		 = FRONTSIDE;
	int nSheetNum	 = 1;
	int nTotalPages  = pBookblock->GetNumPages(-1, TRUE);
	int nCurrentPage = 0;
	CArray <short*, short*> pageSequence;
	POSITION pos = pDoc->m_Bookblock.m_DescriptionList.GetHeadPosition();
	while (pos)
	{
		CDescriptionItem& rItem		 = pDoc->m_Bookblock.m_DescriptionList.GetNext(pos);
		CFoldSheet&		  rFoldSheet = rItem.GetFoldSheet(&(pDoc->m_Bookblock));
		if (rFoldSheet.m_bOutputDisabled)
			continue;
		
		if (rItem.m_nFoldSheetPart == CDescriptionItem::FoldSheetRightPart)
			continue;

		int nCurrentPageIndex = -1;
		rFoldSheet.GetPageSequence(pageSequence,  -1);
		for (int nPageSeqIndex = 0; nPageSeqIndex < pageSequence.GetSize(); nPageSeqIndex++)
		{
			if (nCurrentPageIndex == *pageSequence[nPageSeqIndex])		// check if pages are nUP -> output only once
				continue;
			nCurrentPageIndex = *pageSequence[nPageSeqIndex];

			if (m_media.m_nAutoAlignTo == CMediaSize::AlignToBookblockDoublePage)
				if (nPageSeqIndex >= pageSequence.GetSize()/2)
					break;

			if (nSide == FRONTSIDE)		// new signature every second page
			{
				JDFOUT_AppendSignatureBookblock(nSheetNum);
				nSheetNum++;
			}

			int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(rFoldSheet.m_nProductPartIndex);
			if (m_media.m_nAutoAlignTo == CMediaSize::AlignToBookblockDoublePage)
			{
				POSITION pos  = pDoc->m_PageTemplateList.FindIndex(*pageSequence[nPageSeqIndex] - 1, -nProductIndex - 1);
				POSITION pos2 = pDoc->m_PageTemplateList.FindIndex(*pageSequence[pageSequence.GetSize() - 1 - nPageSeqIndex] - 1, -nProductIndex - 1);
				ProcessBookblockDoublePage(nSide, pos, pos2);
				nCurrentPage += 2;
				if (pFnProgressCallback)
					m_bCancelJob = pFnProgressCallback(nTotalPages, nCurrentPage, rFoldSheet.GetNumber(), NULL);
			}
			else
			{
				POSITION pos  = pDoc->m_PageTemplateList.FindIndex(*pageSequence[nPageSeqIndex] - 1, -nProductIndex - 1);
				ProcessBookblockSinglePage(nSide, pos);
				nCurrentPage++;
				if (pFnProgressCallback)
					m_bCancelJob = pFnProgressCallback(nTotalPages, nCurrentPage, rFoldSheet.GetNumber(), NULL);
			}

			nSide = (nSide == FRONTSIDE) ? BACKSIDE : FRONTSIDE;

			if (m_bCancelJob)
				break;
		}

		if (m_bCancelJob)
			break;
	}

	JDFOUT_AppendPagesRunList();
	JDFOUT_AppendMarksRunList();

	Close();

	if ( ! m_bCancelJob)
	{
		// output marks to pdf file
		CPDFTargetProperties targetProps(CPDFTargetProperties::TypeJDFFolder, m_strTargetName, GetMarksfileLocation(), m_media);
		targetProps.m_strOutputFilename = m_strFilenameTemplate;
		targetProps.m_bJDFOutputMime = m_bJDFOutputMime;
		CPDFOutput pdfOutput(targetProps, _T(""));
		BOOL bPDFOverwriteAll = TRUE;
	
		CString strWinText; strWinText.LoadString(IDS_NAV_PRINTSHEETMARKS);
		g_dlgMarksBookblockProofProgress.Create(IDD_BOOKBLOCK_PROOF_PROGRESS);
		g_dlgMarksBookblockProofProgress.SetWindowText(strWinText);
		m_bCancelJob = pdfOutput.ProcessBookblockJobInOne(pBookblock, bPDFOverwriteAll, FALSE, FALSE, TRUE, OutputProofProgessCallback);	// not AutoCmd, not SendIfPrinter, marks only
		g_dlgMarksBookblockProofProgress.DestroyWindow();
	}

	CreateMimeFile();

	if ( ! m_bCancelJob)
		pBookblock->m_OutputDate = COleDateTime::GetCurrentTime();
	if (m_nError != 0)
		pBookblock->m_OutputDate.m_dt = 0;
	
	return m_bCancelJob;
}


BOOL CJDFOutputImposition::ProcessBookblockDoublePage(int nSide, POSITION pos, POSITION pos2)
{
	if ( ! pos)
		return m_bCancelJob;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return m_bCancelJob;

	CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
	CLayoutObject* pLayoutObject = rPageTemplate.GetLayoutObject();
	CPrintSheet*   pPrintSheet   = rPageTemplate.GetPrintSheet();
	if ( ! pLayoutObject || ! pPrintSheet)
		return m_bCancelJob;
	CPlateList&	plateList = (pLayoutObject->GetLayoutSideIndex() == FRONTSIDE) ? pPrintSheet->m_FrontSidePlates : pPrintSheet->m_BackSidePlates;
	CPlate*		pPlate	  = ( ! plateList.GetCount()) ? &plateList.m_defaultPlate : &plateList.GetHead(); 

	m_fMediaWidth  = ( (pLayoutObject->m_nHeadPosition == TOP) || (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaWidth(NULL)  : m_media.GetMediaHeight(NULL);
	m_fMediaHeight = ( (pLayoutObject->m_nHeadPosition == TOP) || (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaHeight(NULL) : m_media.GetMediaWidth(NULL);

	int   nRotation = 0;
	float fXPos, fYPos, fXCenter, fYCenter;
	switch (pLayoutObject->m_nHeadPosition)
	{
	case TOP:		nRotation = 0;
					fXCenter  = (pLayoutObject->m_nIDPosition == RIGHT) ? CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, LEFT)   : CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, RIGHT);
					fYCenter  = pLayoutObject->GetBottom() + pLayoutObject->GetHeight() / 2.0f;
					break;
	case BOTTOM:	nRotation = 180;	
					fXCenter  = (pLayoutObject->m_nIDPosition == LEFT)  ? CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, LEFT)   : CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, RIGHT);
					fYCenter  = pLayoutObject->GetBottom() + pLayoutObject->GetHeight() / 2.0f;
					break;
	case LEFT:		nRotation =  90;	
					fXCenter  = pLayoutObject->GetLeft() + pLayoutObject->GetWidth() / 2.0f;
					fYCenter  = (pLayoutObject->m_nIDPosition == RIGHT) ? CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, BOTTOM) : CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, TOP);
					break;
	case RIGHT:		nRotation = 270;	
					fXCenter  = pLayoutObject->GetLeft() + pLayoutObject->GetWidth() / 2.0f;
					fYCenter  = (pLayoutObject->m_nIDPosition == LEFT)  ? CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, BOTTOM) : CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, TOP);
					break;
	}

	fXPos = fXCenter - m_fMediaWidth  / 2.0f;
	fYPos = fYCenter - m_fMediaHeight / 2.0f;
	IPL_int32_t nSheetIndex = -1;

	if (pos2)
	{
		CPageTemplate& rPageTemplate2 = pDoc->m_PageTemplateList.GetAt(pos2);
		CLayoutObject* pLayoutObject2 = rPageTemplate2.GetLayoutObject();
		CPrintSheet*   pPrintSheet2   = rPageTemplate2.GetPrintSheet();
		if ( ! pLayoutObject2 || ! pPrintSheet2)
			return m_bCancelJob;

		CLayoutObject* pNeighbour = NULL;
		switch (pLayoutObject->m_nHeadPosition)
		{
		case TOP:		pNeighbour = (pLayoutObject->m_nIDPosition == RIGHT) ? ((pLayoutObject->m_LeftNeighbours.GetSize())  ? pLayoutObject->m_LeftNeighbours[0]  : NULL) : ((pLayoutObject->m_RightNeighbours.GetSize()) ? pLayoutObject->m_RightNeighbours[0] : NULL);
						break;
		case BOTTOM:	pNeighbour = (pLayoutObject->m_nIDPosition == LEFT)  ? ((pLayoutObject->m_LeftNeighbours.GetSize())  ? pLayoutObject->m_LeftNeighbours[0]  : NULL) : ((pLayoutObject->m_RightNeighbours.GetSize()) ? pLayoutObject->m_RightNeighbours[0] : NULL);
						break;
		case LEFT:		pNeighbour = (pLayoutObject->m_nIDPosition == RIGHT) ? ((pLayoutObject->m_LowerNeighbours.GetSize()) ? pLayoutObject->m_LowerNeighbours[0] : NULL) : ((pLayoutObject->m_UpperNeighbours.GetSize()) ? pLayoutObject->m_UpperNeighbours[0] : NULL);
						break;
		case RIGHT:		pNeighbour = (pLayoutObject->m_nIDPosition == LEFT)  ? ((pLayoutObject->m_LowerNeighbours.GetSize()) ? pLayoutObject->m_LowerNeighbours[0] : NULL) : ((pLayoutObject->m_UpperNeighbours.GetSize()) ? pLayoutObject->m_UpperNeighbours[0] : NULL);
						break;
		}

		if (pNeighbour)
			if (pNeighbour == pLayoutObject2)
				return ProcessTileBookblock(pPlate, nSide, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f, 1, pLayoutObject, pLayoutObject2);

		m_bCancelJob = ProcessTileBookblock(pPlate, nSide, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f, 1, pLayoutObject, NULL);

		CPlateList&	plateList2 = (pLayoutObject2->GetLayoutSideIndex() == FRONTSIDE) ? pPrintSheet2->m_FrontSidePlates : pPrintSheet2->m_BackSidePlates;
		CPlate*		pPlate2	   = ( ! plateList2.GetCount()) ? &plateList2.m_defaultPlate : &plateList2.GetHead(); 

		m_fMediaWidth  = ( (pLayoutObject2->m_nHeadPosition == TOP) || (pLayoutObject2->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaWidth(NULL)  : m_media.GetMediaHeight(NULL);
		m_fMediaHeight = ( (pLayoutObject2->m_nHeadPosition == TOP) || (pLayoutObject2->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaHeight(NULL) : m_media.GetMediaWidth(NULL);

		switch (pLayoutObject->m_nHeadPosition)
		{
		case TOP:		fXCenter  = (pLayoutObject2->m_nIDPosition == RIGHT) ? CPDFOutput::GetPageEdge(pLayoutObject2, pPrintSheet2, LEFT)   : CPDFOutput::GetPageEdge(pLayoutObject2, pPrintSheet2, RIGHT);
						fYCenter  = pLayoutObject2->GetBottom() + pLayoutObject2->GetHeight() / 2.0f;
						break;
		case BOTTOM:	fXCenter  = (pLayoutObject2->m_nIDPosition == LEFT)  ? CPDFOutput::GetPageEdge(pLayoutObject2, pPrintSheet2, LEFT)   : CPDFOutput::GetPageEdge(pLayoutObject2, pPrintSheet2, RIGHT);
						fYCenter  = pLayoutObject2->GetBottom() + pLayoutObject2->GetHeight() / 2.0f;
						break;
		case LEFT:		fXCenter  = pLayoutObject2->GetLeft() + pLayoutObject2->GetWidth() / 2.0f;
						fYCenter  = (pLayoutObject2->m_nIDPosition == RIGHT) ? CPDFOutput::GetPageEdge(pLayoutObject2, pPrintSheet2, BOTTOM) : CPDFOutput::GetPageEdge(pLayoutObject2, pPrintSheet2, TOP);
						break;
		case RIGHT:		fXCenter  = pLayoutObject2->GetLeft() + pLayoutObject2->GetWidth() / 2.0f;
						fYCenter  = (pLayoutObject2->m_nIDPosition == LEFT)  ? CPDFOutput::GetPageEdge(pLayoutObject2, pPrintSheet2, BOTTOM) : CPDFOutput::GetPageEdge(pLayoutObject2, pPrintSheet2, TOP);
						break;
		}

		fXPos = fXCenter - m_fMediaWidth  / 2.0f;
		fYPos = fYCenter - m_fMediaHeight / 2.0f;
		return ProcessTileBookblock(pPlate2, nSide, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f, 1, pLayoutObject2, NULL);
	}

	return ProcessTileBookblock(pPlate, nSide, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f, 1, pLayoutObject, NULL);
}

BOOL CJDFOutputImposition::ProcessBookblockSinglePage(int nSide, POSITION pos)
{
	if ( ! pos)
		return m_bCancelJob;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return m_bCancelJob;

	CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.GetAt(pos);
	CLayoutObject* pLayoutObject = rPageTemplate.GetLayoutObject();
	CPrintSheet*   pPrintSheet   = rPageTemplate.GetPrintSheet();
	if ( ! pLayoutObject || ! pPrintSheet)
		return m_bCancelJob;
	CPlateList&	plateList = (pLayoutObject->GetLayoutSideIndex() == FRONTSIDE) ? pPrintSheet->m_FrontSidePlates : pPrintSheet->m_BackSidePlates;
	CPlate*		pPlate	  = ( ! plateList.GetCount()) ? &plateList.m_defaultPlate : &plateList.GetHead(); 

	m_fMediaWidth  = ( (pLayoutObject->m_nHeadPosition == TOP) || (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaWidth(NULL)  : m_media.GetMediaHeight(NULL);
	m_fMediaHeight = ( (pLayoutObject->m_nHeadPosition == TOP) || (pLayoutObject->m_nHeadPosition == BOTTOM) ) ? m_media.GetMediaHeight(NULL) : m_media.GetMediaWidth(NULL);

	int   nRotation = 0;
	float fXPos, fYPos;
	switch (pLayoutObject->m_nHeadPosition)
	{
	case TOP:		nRotation = 0;
					fXPos	  = (pLayoutObject->m_nIDPosition == RIGHT) ? CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, LEFT)   : (CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, RIGHT)    - m_fMediaWidth);
					fYPos	  = pLayoutObject->GetBottom() + pLayoutObject->GetHeight() / 2.0f - m_fMediaHeight / 2.0f;
					break;
	case BOTTOM:	nRotation = 180;	
					fXPos	  = (pLayoutObject->m_nIDPosition == LEFT) ? CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, LEFT)    : (CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, RIGHT)	   - m_fMediaWidth);
					fYPos	  = pLayoutObject->GetBottom() + pLayoutObject->GetHeight() / 2.0f - m_fMediaHeight / 2.0f;
					break;
	case LEFT:		nRotation =  90;	
					fXPos	  = pLayoutObject->GetLeft()   + pLayoutObject->GetWidth()  / 2.0f - m_fMediaWidth  / 2.0f;
					fYPos	  = (pLayoutObject->m_nIDPosition == RIGHT) ? CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, BOTTOM) : (CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, TOP)	   - m_fMediaHeight);
					break;
	case RIGHT:		nRotation = 270;	
					fXPos	  = pLayoutObject->GetLeft()   + pLayoutObject->GetWidth()  / 2.0f - m_fMediaWidth  / 2.0f;;
					fYPos	  = (pLayoutObject->m_nIDPosition == LEFT)  ? CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, BOTTOM) : (CPDFOutput::GetPageEdge(pLayoutObject, pPrintSheet, TOP) 	   - m_fMediaHeight);
					break;
	}

	m_bCancelJob = ProcessTileBookblock(pPlate, nSide, FALSE, fXPos, fYPos, nRotation, 0.0f, 0.0f, 1, NULL, pLayoutObject);

	return m_bCancelJob;
}

BOOL CJDFOutputImposition::ProcessPlate(CPlate* pPlate) 
{
	if (!pPlate)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}

	CPrintSheet* pPrintSheet = pPlate->GetPrintSheet();
	if (!pPrintSheet)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}

//	theApp.m_pDlgPDFLibStatus->UpdateStatus(CDlgPDFLibStatus::OutputPlate,  0, pPlate->GetLayoutSide()->m_strSideName, pPlate->m_strPlateName);

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
//	if (pPrintSheetView)
//	{
//		pPlate->SetTopmostPlate();
//		pPrintSheetView->Invalidate();
//		pPrintSheetView->UpdateWindow();
//	}

	m_fMediaWidth  = m_media.GetMediaWidth(pPrintSheet);
	m_fMediaHeight = m_media.GetMediaHeight(pPrintSheet);

	for (int nTileIndex = 0; (nTileIndex < m_media.m_posInfos.GetSize()) && ! m_bCancelJob; nTileIndex++)
	{
		float fXPos, fYPos;
		m_media.m_posInfos[nTileIndex].m_reflinePosParams.GetSheetPosition(pPrintSheet, pPlate->GetSheetSide(), fXPos, fYPos, m_fMediaWidth, m_fMediaHeight);
		m_bCancelJob = ProcessTile(pPlate, (m_media.m_posInfos.GetSize() > 1) ? TRUE : FALSE, fXPos, fYPos, 
								   m_media.m_posInfos[nTileIndex].m_nRotation, 
								   m_media.m_posInfos[nTileIndex].m_fCorrectionX, 
								   m_media.m_posInfos[nTileIndex].m_fCorrectionY, 
								   nTileIndex + 1);
	}

	if ( ! m_bCancelJob)
		m_nPlatesProcessed++;

	return m_bCancelJob;
}

BOOL CJDFOutputImposition::ProcessTile(CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile)
{
	if (!pPlate)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}
	CPrintSheet* pPrintSheet = pPlate->GetPrintSheet();
	if (!pPrintSheet)
	{
		m_nError = CImpManApp::PDFOutputError;
		return TRUE;
	}

	float fWidth  = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaWidth  : m_fMediaHeight;
	float fHeight = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaHeight : m_fMediaWidth;
	IPL_TDRECT mediaBox	= { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };
	IPL_TDRECT cropBox	= { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };

	JDFOUT_AppendSurface(*pPrintSheet, (pPlate->GetLayoutSide()) ? pPlate->GetLayoutSide()->m_nLayoutSide : FRONTSIDE, mediaBox);

	if (bTiling)
		theApp.m_pDlgPDFLibStatus->UpdateStatus(CDlgPDFLibStatus::OutputTile, nTile);

	m_bCancelJob = ProcessExposures(pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::Page);

	JDFOUT_AppendMarkObject(mediaBox);

	return m_bCancelJob;
}

BOOL CJDFOutputImposition::ProcessTileBookblock(CPlate* pPlate, int nSide, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile, CLayoutObject* pFilterObject1, CLayoutObject* pFilterObject2)
{
	float fWidth  = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaWidth  : m_fMediaHeight;
	float fHeight = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaHeight : m_fMediaWidth;
	IPL_TDRECT mediaBox	= { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };
	IPL_TDRECT cropBox	= { fCorrectionX/fPSPoint, fCorrectionY/fPSPoint, (fWidth + fCorrectionX)/fPSPoint, (fHeight + fCorrectionY)/fPSPoint };

	JDFOUT_AppendSurfaceBookblock(nSide, mediaBox);

	if (bTiling)
		theApp.m_pDlgPDFLibStatus->UpdateStatus(CDlgPDFLibStatus::OutputTile, nTile);

	m_bCancelJob = ProcessExposures(pPlate, bTiling, fXPos, fYPos, nRotation, fCorrectionX, fCorrectionY, nTile, CLayoutObject::Page, -1, -1, pFilterObject1, pFilterObject2);

	JDFOUT_AppendMarkObject(mediaBox);

	return m_bCancelJob;
}

BOOL CJDFOutputImposition::ProcessExposures(CPlate* pPlate, BOOL bTiling, float fXPos, float fYPos, int nRotation, float fCorrectionX, float fCorrectionY, int nTile, BYTE nObjectType, int nMarkType, int nZOrder, CLayoutObject* pFilterObject1, CLayoutObject* pFilterObject2)
{
	if ( ! pPlate)
		return TRUE;

	CPrintSheet* pPrintSheet = pPlate->GetPrintSheet();
	if ( ! pPrintSheet)
		return TRUE;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return TRUE;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	int			 nActLayoutSide = pPlate->GetSheetSide();
	POSITION	 pos			= pPlate->m_ExposureSequence.GetHeadPosition();
	CString		 strColorName	= pPlate->m_strPlateName;
	CString		 strSideName	= (pPlate->GetLayoutSide()) ? pPlate->GetLayoutSide()->m_strSideName : "";

	srand( (unsigned)time( NULL ) );
	int nRan = rand();
	int nNumPages = pPrintSheet->GetNumPages(nActLayoutSide);
	int nRandomPageIndex = (nNumPages > 0) ? nRan % nNumPages : 0;

	CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CDC* pDCPrintSheetView = (pPrintSheetView) ? pPrintSheetView->GetDC() : NULL;

	while (pos)
	{
		CExposure&	   rExposure = pPlate->m_ExposureSequence.GetNext(pos);
		CLayoutObject* pObject	 = rExposure.GetLayoutObject(pPrintSheet->m_nLayoutIndex, nActLayoutSide);
		if (!pObject)
			continue;
		if (pObject->m_nType != nObjectType)
			continue;
		if (pFilterObject1 && pFilterObject2)
			if ( (pObject != pFilterObject1) && (pObject != pFilterObject2) )
				continue;
		if (pFilterObject1 && ! pFilterObject2)
			if (pObject != pFilterObject1)
				continue;
		if ( ! pFilterObject1 && pFilterObject2)
			if (pObject != pFilterObject2)
				continue;

		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPrintSheet);
		if (pObjectTemplate)
		{
			CPageSource*		pPageSource		  = pObjectTemplate->GetObjectSource	  (pPlate->m_nColorDefinitionIndex, TRUE, pPrintSheet, nActLayoutSide);
			CPageSourceHeader*	pPageSourceHeader = pObjectTemplate->GetObjectSourceHeader(pPlate->m_nColorDefinitionIndex, TRUE, pPrintSheet, nActLayoutSide);
			SEPARATIONINFO*		pSepInfo		  = pObjectTemplate->GetSeparationInfo	  (pPlate->m_nColorDefinitionIndex, TRUE, pPrintSheet, nActLayoutSide);

			float fSheetClipBoxLeft	  = rExposure.GetSheetBleedBox(LEFT,   pObject, pObjectTemplate) - fXPos;
			float fSheetClipBoxBottom = rExposure.GetSheetBleedBox(BOTTOM, pObject, pObjectTemplate) - fYPos;
			float fSheetClipBoxRight  = rExposure.GetSheetBleedBox(RIGHT,  pObject, pObjectTemplate) - fXPos;
			float fSheetClipBoxTop	  = rExposure.GetSheetBleedBox(TOP,	   pObject, pObjectTemplate) - fYPos;

			float fSheetExposureCenterX = rExposure.GetContentTrimBoxCenterX(pObject, pObjectTemplate) - fXPos;
			float fSheetExposureCenterY = rExposure.GetContentTrimBoxCenterY(pObject, pObjectTemplate) - fYPos;
			float fObjectWidth			= pObject->GetWidth();
			float fObjectHeight			= pObject->GetHeight();
			float fShinglingOffsetX, fShinglingOffsetY;
			switch (pObject->m_nHeadPosition)
			{
			case TOP:		fShinglingOffsetX =  pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, NULL, &fObjectWidth,  TRUE);	
							fShinglingOffsetY =  pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, NULL, &fObjectHeight, TRUE);	
							break;
			case LEFT:		fShinglingOffsetX = -pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, NULL, &fObjectHeight, TRUE);	
							fShinglingOffsetY =  pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, NULL, &fObjectWidth,  TRUE);	
							break;
			case BOTTOM:	fShinglingOffsetX = -pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, NULL, &fObjectWidth,  TRUE);	
							fShinglingOffsetY = -pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, NULL, &fObjectHeight, TRUE);	
							break;
			case RIGHT:		fShinglingOffsetX =  pObjectTemplate->GetContentOffsetY(CPageTemplate::AddShinglingOffset, NULL, &fObjectHeight, TRUE);	
							fShinglingOffsetY = -pObjectTemplate->GetContentOffsetX(CPageTemplate::AddShinglingOffset, NULL, &fObjectWidth,  TRUE);	
							break;
			}

			fSheetExposureCenterX += fShinglingOffsetX;	
			fSheetExposureCenterY += fShinglingOffsetY;	
			float fObjectTrimBoxLeft   = pObject->GetLeft()		+ fShinglingOffsetX - fXPos;
			float fObjectTrimBoxBottom = pObject->GetBottom()	+ fShinglingOffsetY - fYPos;
			float fObjectTrimBoxRight  = pObject->GetRight()	+ fShinglingOffsetX - fXPos;
			float fObjectTrimBoxTop    = pObject->GetTop()		+ fShinglingOffsetY - fYPos;
			float fObjectTrimBoxWidth  = ( (nRotation == 0) || (nRotation == 180) ) ? pObject->GetRealWidth()  : pObject->GetRealHeight();
			float fObjectTrimBoxHeight = ( (nRotation == 0) || (nRotation == 180) ) ? pObject->GetRealHeight() : pObject->GetRealWidth();

			int nHeadPosition = pObject->m_nHeadPosition;
			TransformToOutputMedia(nRotation, fCorrectionX, fCorrectionY, m_fMediaWidth, m_fMediaHeight, 
								   fObjectTrimBoxLeft, fObjectTrimBoxBottom, fObjectTrimBoxRight, fObjectTrimBoxTop, 
								   fSheetClipBoxLeft,  fSheetClipBoxBottom,  fSheetClipBoxRight,  fSheetClipBoxTop, 
								   fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition);

			m_bOutputFormatExceeded = CDlgPDFOutputMediaSettings::OutputFormatExceeded(m_fMediaWidth, m_fMediaHeight);

			if (bTiling && (pObject->m_nType == CLayoutObject::Page) )
			{
				float fWidth  = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaWidth  : m_fMediaHeight;
				float fHeight = ( (nRotation == 0) || (nRotation == 180) ) ? m_fMediaHeight : m_fMediaWidth ;
				if ((fSheetClipBoxLeft < -0.1f) || (fSheetClipBoxBottom < -0.1f))
					continue;
				if ((fSheetClipBoxRight > fWidth  + 0.1f) || (fSheetClipBoxTop > fHeight + 0.1f))
					continue;
			}

			CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
			// feedback to CPrintSheetView
			if (pDCPrintSheetView)
			{
				CDisplayItem* pDI = pPrintSheetView->m_DisplayList.GetItemFromData(&rExposure, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, Exposure));
				if ( ! pDI)
					pDI = pPrintSheetView->m_DisplayList.GetItemFromData(pObject, pPrintSheet, DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject));
				if (pDI) 
					CPrintSheetView::FillTransparent(pDCPrintSheetView, pDI->m_BoundingRect, LIGHTGREEN, TRUE);
			}

			float fObjectBBoxCenterX = pObject->GetRealWidth()  / 2.0f;
			float fObjectBBoxCenterY = pObject->GetRealHeight() / 2.0f;
			if (pPageSource && pPageSourceHeader)
			{
				fObjectBBoxCenterX = pPageSource->GetBBox(LEFT,   pPageSourceHeader) + ( pPageSource->GetBBox(RIGHT, pPageSourceHeader) - pPageSource->GetBBox(LEFT,   pPageSourceHeader) ) / 2.0f;
				fObjectBBoxCenterY = pPageSource->GetBBox(BOTTOM, pPageSourceHeader) + ( pPageSource->GetBBox(TOP,   pPageSourceHeader) - pPageSource->GetBBox(BOTTOM, pPageSourceHeader) ) / 2.0f;
			}

			// feedback to CPrintSheetTableView
			if (pPrintSheetTableView)
			{
				if ( (pPlate->m_strPlateName == "Composite") || (pPlate->m_strPlateName == _T("")) )
					pPrintSheetTableView->m_strCurrentOutputPage = pObjectTemplate->m_strPageID;
				else
					pPrintSheetTableView->m_strCurrentOutputPage.Format(_T("%s (%s)"), pObjectTemplate->m_strPageID, strColorName);
				pPrintSheetTableView->UpdateData(FALSE);
			}

			int nOrd = pDoc->m_PageTemplateList.GetIndex(pObjectTemplate);

			float fBottlingAngle = 0.0f, fBottlingOffsetX = 0.0f, fBottlingOffsetY = 0.0f;
			CPDFOutput::CalcBottlingParams(fBottlingAngle, fBottlingOffsetX, fBottlingOffsetY, pObject, pObjectTemplate, fObjectBBoxCenterX, fObjectBBoxCenterY);
			float fContentScaleX = pObjectTemplate->m_fContentScaleX * pObjectTemplate->CalcShinglingScaleX();
			float fContentScaleY = pObjectTemplate->m_fContentScaleY * pObjectTemplate->CalcShinglingScaleY();

			if (PDFSheet_AddPage(fSheetExposureCenterX, fSheetExposureCenterY, nHeadPosition, 
							     fObjectTrimBoxLeft, fObjectTrimBoxBottom, fObjectTrimBoxRight, fObjectTrimBoxTop, 
			 					 fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop,
								 fObjectBBoxCenterX, fObjectBBoxCenterY,
								 fObjectTrimBoxWidth, fObjectTrimBoxHeight,
								 pObjectTemplate->m_fContentRotation, fContentScaleX, fContentScaleY,
								 fBottlingAngle, fBottlingOffsetX, fBottlingOffsetY, 
								 (nRandomPageIndex == 0), nOrd) )
				return m_bCancelJob; // break

			nRandomPageIndex--;

			if (pPrintSheetTableView)
				pPrintSheetTableView->m_pdfOutputProgress.SetPos(m_nCurrentPage++);	// no progress feedback for system created marks
		}
	}

	return FALSE;
}


void CJDFOutputImposition::TransformToOutputMedia(int nRotation, float fCorrectionX, float fCorrectionY, float fMediaWidth, float fMediaHeight, 
												  float& fObjectTrimBoxLeft,	float& fObjectTrimBoxBottom,  float& fObjectTrimBoxRight, float& fObjectTrimBoxTop, 
												  float& fSheetClipBoxLeft,		float& fSheetClipBoxBottom,	  float& fSheetClipBoxRight,  float& fSheetClipBoxTop, 
												  float& fSheetExposureCenterX, float& fSheetExposureCenterY, int& nHeadPosition)
{
	float fObjectTrimBoxLeftTrans, fObjectTrimBoxBottomTrans, fObjectTrimBoxRightTrans, fObjectTrimBoxTopTrans;
	float fSheetClipBoxLeftTrans,  fSheetClipBoxBottomTrans,  fSheetClipBoxRightTrans,  fSheetClipBoxTopTrans;
	float fSheetExposureCenterXTrans, fSheetExposureCenterYTrans;
	switch (nRotation)
	{
	case 0:		
		fSheetClipBoxLeftTrans	   = fSheetClipBoxLeft;
		fSheetClipBoxBottomTrans   = fSheetClipBoxBottom;
		fSheetClipBoxRightTrans	   = fSheetClipBoxRight;
		fSheetClipBoxTopTrans	   = fSheetClipBoxTop;

		fObjectTrimBoxLeftTrans	   = fObjectTrimBoxLeft;
		fObjectTrimBoxBottomTrans  = fObjectTrimBoxBottom;
		fObjectTrimBoxRightTrans   = fObjectTrimBoxRight;
		fObjectTrimBoxTopTrans	   = fObjectTrimBoxTop;

		fSheetExposureCenterXTrans = fSheetExposureCenterX;
		fSheetExposureCenterYTrans = fSheetExposureCenterY;
		break;
	case 90:	
		fSheetClipBoxLeftTrans	   = fSheetClipBoxBottom;
		fSheetClipBoxBottomTrans   = fMediaWidth - fSheetClipBoxRight;
		fSheetClipBoxRightTrans	   = fSheetClipBoxTop;
		fSheetClipBoxTopTrans	   = fMediaWidth - fSheetClipBoxLeft;

		fObjectTrimBoxLeftTrans	   = fObjectTrimBoxBottom;
		fObjectTrimBoxBottomTrans  = fMediaWidth - fObjectTrimBoxRight;
		fObjectTrimBoxRightTrans   = fObjectTrimBoxTop;
		fObjectTrimBoxTopTrans	   = fMediaWidth - fObjectTrimBoxLeft;

		fSheetExposureCenterXTrans = fSheetExposureCenterY;
		fSheetExposureCenterYTrans = fMediaWidth - fSheetExposureCenterX;
		break;
	case 180:	
		fSheetClipBoxLeftTrans	   = fMediaWidth  - fSheetClipBoxRight;
		fSheetClipBoxBottomTrans   = fMediaHeight - fSheetClipBoxTop;
		fSheetClipBoxRightTrans	   = fMediaWidth  - fSheetClipBoxLeft;
		fSheetClipBoxTopTrans	   = fMediaHeight - fSheetClipBoxBottom;

		fObjectTrimBoxLeftTrans	   = fMediaWidth  - fObjectTrimBoxRight;
		fObjectTrimBoxBottomTrans  = fMediaHeight - fObjectTrimBoxTop;
		fObjectTrimBoxRightTrans   = fMediaWidth  - fObjectTrimBoxLeft;
		fObjectTrimBoxTopTrans	   = fMediaHeight - fObjectTrimBoxBottom;

		fSheetExposureCenterXTrans = fMediaWidth  - fSheetExposureCenterX;
		fSheetExposureCenterYTrans = fMediaHeight - fSheetExposureCenterY;
		break;
	case 270:	
		fSheetClipBoxLeftTrans	   = fMediaHeight - fSheetClipBoxTop;
		fSheetClipBoxBottomTrans   = fSheetClipBoxLeft;
		fSheetClipBoxRightTrans	   = fMediaHeight - fSheetClipBoxBottom;
		fSheetClipBoxTopTrans	   = fSheetClipBoxRight;

		fObjectTrimBoxLeftTrans	   = fMediaHeight - fObjectTrimBoxTop;
		fObjectTrimBoxBottomTrans  = fObjectTrimBoxLeft;
		fObjectTrimBoxRightTrans   = fMediaHeight - fObjectTrimBoxBottom;
		fObjectTrimBoxTopTrans	   = fObjectTrimBoxRight;

		fSheetExposureCenterXTrans = fMediaHeight - fSheetExposureCenterY;
		fSheetExposureCenterYTrans = fSheetExposureCenterX;
		break;
	default:		
		fSheetClipBoxLeftTrans	   = fSheetClipBoxLeft;
		fSheetClipBoxBottomTrans   = fSheetClipBoxBottom;
		fSheetClipBoxRightTrans	   = fSheetClipBoxRight;
		fSheetClipBoxTopTrans	   = fSheetClipBoxTop;

		fObjectTrimBoxLeftTrans	   = fObjectTrimBoxLeft;
		fObjectTrimBoxBottomTrans  = fObjectTrimBoxBottom;
		fObjectTrimBoxRightTrans   = fObjectTrimBoxRight;
		fObjectTrimBoxTopTrans	   = fObjectTrimBoxTop;

		fSheetExposureCenterXTrans = fSheetExposureCenterX;
		fSheetExposureCenterYTrans = fSheetExposureCenterY;
		break;
	}
	
	fSheetClipBoxLeft	  = fSheetClipBoxLeftTrans		+ fCorrectionX;
	fSheetClipBoxBottom	  = fSheetClipBoxBottomTrans	+ fCorrectionY;
	fSheetClipBoxRight	  = fSheetClipBoxRightTrans		+ fCorrectionX;
	fSheetClipBoxTop	  = fSheetClipBoxTopTrans		+ fCorrectionY;

	fObjectTrimBoxLeft	  = fObjectTrimBoxLeftTrans		+ fCorrectionX;
	fObjectTrimBoxBottom  = fObjectTrimBoxBottomTrans	+ fCorrectionY;
	fObjectTrimBoxRight	  = fObjectTrimBoxRightTrans	+ fCorrectionX;
	fObjectTrimBoxTop	  = fObjectTrimBoxTopTrans		+ fCorrectionY;

	fSheetExposureCenterX = fSheetExposureCenterXTrans	+ fCorrectionX;
	fSheetExposureCenterY = fSheetExposureCenterYTrans	+ fCorrectionY;

	switch (nRotation)
	{
	case 0:	  
		switch (nHeadPosition)
		{
		case TOP:		nHeadPosition = TOP;	break;
		case RIGHT:		nHeadPosition = RIGHT;	break;
		case BOTTOM:	nHeadPosition = BOTTOM; break;
		case LEFT:		nHeadPosition = LEFT;	break;
		}
		break;
	case 90:  
		switch (nHeadPosition)
		{
		case TOP:		nHeadPosition = RIGHT;	break;
		case RIGHT:		nHeadPosition = BOTTOM;	break;
		case BOTTOM:	nHeadPosition = LEFT;	break;
		case LEFT:		nHeadPosition = TOP;	break;
		}
		break;
	case 180: 
		switch (nHeadPosition)
		{
		case TOP:		nHeadPosition = BOTTOM;	break;
		case RIGHT:		nHeadPosition = LEFT;	break;
		case BOTTOM:	nHeadPosition = TOP;	break;
		case LEFT:		nHeadPosition = RIGHT;	break;
		}
		break;
	case 270: 
		switch (nHeadPosition)
		{
		case TOP:		nHeadPosition = LEFT;	break;
		case RIGHT:		nHeadPosition = TOP;	break;
		case BOTTOM:	nHeadPosition = RIGHT;	break;
		case LEFT:		nHeadPosition = BOTTOM;	break;
		}
		break;
	}
}

void CJDFOutputImposition::TransformToOutputMedia(int nRotation, float fCorrectionX, float fCorrectionY, float fMediaWidth, float fMediaHeight, 
									   float& fSheetClipBoxLeft, float& fSheetClipBoxBottom, float& fSheetClipBoxRight, float& fSheetClipBoxTop)
{
	float fSheetClipBoxLeftTrans, fSheetClipBoxBottomTrans, fSheetClipBoxRightTrans, fSheetClipBoxTopTrans;
	switch (nRotation)
	{
	case 0:		
		fSheetClipBoxLeftTrans	   = fSheetClipBoxLeft;
		fSheetClipBoxBottomTrans   = fSheetClipBoxBottom;
		fSheetClipBoxRightTrans	   = fSheetClipBoxRight;
		fSheetClipBoxTopTrans	   = fSheetClipBoxTop;
		break;
	case 90:	
		fSheetClipBoxLeftTrans	   = fSheetClipBoxBottom;
		fSheetClipBoxBottomTrans   = fMediaWidth - fSheetClipBoxRight;
		fSheetClipBoxRightTrans	   = fSheetClipBoxTop;
		fSheetClipBoxTopTrans	   = fMediaWidth - fSheetClipBoxLeft;
		break;
	case 180:	
		fSheetClipBoxLeftTrans	   = fMediaWidth  - fSheetClipBoxRight;
		fSheetClipBoxBottomTrans   = fMediaHeight - fSheetClipBoxTop;
		fSheetClipBoxRightTrans	   = fMediaWidth  - fSheetClipBoxLeft;
		fSheetClipBoxTopTrans	   = fMediaHeight - fSheetClipBoxBottom;
		break;
	case 270:	
		fSheetClipBoxLeftTrans	   = fMediaHeight - fSheetClipBoxTop;
		fSheetClipBoxBottomTrans   = fSheetClipBoxLeft;
		fSheetClipBoxRightTrans	   = fMediaHeight - fSheetClipBoxBottom;
		fSheetClipBoxTopTrans	   = fSheetClipBoxRight;
		break;
	default:		
		fSheetClipBoxLeftTrans	   = fSheetClipBoxLeft;
		fSheetClipBoxBottomTrans   = fSheetClipBoxBottom;
		fSheetClipBoxRightTrans	   = fSheetClipBoxRight;
		fSheetClipBoxTopTrans	   = fSheetClipBoxTop;
		break;
	}
	
	fSheetClipBoxLeft	= fSheetClipBoxLeftTrans   + fCorrectionX;
	fSheetClipBoxBottom	= fSheetClipBoxBottomTrans + fCorrectionY;
	fSheetClipBoxRight	= fSheetClipBoxRightTrans  + fCorrectionX;
	fSheetClipBoxTop	= fSheetClipBoxTopTrans	   + fCorrectionY;
}

BOOL CJDFOutputImposition::PDFSheet_AddPage(float fSheetExposureCenterX, float fSheetExposureCenterY, int nHeadPosition,
											float fObjectTrimBoxLeft,	 float fObjectTrimBoxBottom, 
											float fObjectTrimBoxRight,	 float fObjectTrimBoxTop,
											float fSheetClipBoxLeft,	 float fSheetClipBoxBottom, 
											float fSheetClipBoxRight,	 float fSheetClipBoxTop,
											float fObjectBBoxCenterX,	 float fObjectBBoxCenterY,
											float fObjectTrimBoxWidth,	 float fObjectTrimBoxHeight,
											float fContentRotation,		 float fContentScaleX,	float fContentScaleY,
										    float fBottlingAngle,	     float fBottlingOffsetX, float fBottlingOffsetY,
											BOOL bSkipIfDemo, int nOrd)
{
	if (m_nError != 0)
		return TRUE;

	// add content rotation and convert to angles between 0 and 270
	double fIntPart;
	double fAngle = fContentRotation;
	switch (nHeadPosition)
	{
	case LEFT:	fAngle = (float)modf(( 90.0f + fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case BOTTOM:fAngle = (float)modf((180.0f + fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	case RIGHT: fAngle = (float)modf((270.0f + fContentRotation) / 360.0f, &fIntPart) * 360.0f; break;
	}

	float fScaleX = (CPDFOutput::AngleEquals( fContentRotation, 0.0f ) || CPDFOutput::AngleEquals( fContentRotation, 180.0f )) ? fContentScaleX : fContentScaleY;
	float fScaleY = (CPDFOutput::AngleEquals( fContentRotation, 0.0f ) || CPDFOutput::AngleEquals( fContentRotation, 180.0f )) ? fContentScaleY : fContentScaleX;

	fObjectBBoxCenterX	  *= fScaleX;
	fObjectBBoxCenterY	  *= fScaleY;
	fObjectBBoxCenterX	  /= fPSPoint; fObjectBBoxCenterY	 /= fPSPoint;
	fSheetExposureCenterX /= fPSPoint; fSheetExposureCenterY /= fPSPoint;
	fSheetClipBoxLeft	  /= fPSPoint; fSheetClipBoxBottom	 /= fPSPoint;
	fSheetClipBoxRight	  /= fPSPoint; fSheetClipBoxTop		 /= fPSPoint;
	fObjectTrimBoxLeft	  /= fPSPoint; fObjectTrimBoxRight	 /= fPSPoint;
	fObjectTrimBoxBottom  /= fPSPoint; fObjectTrimBoxTop	 /= fPSPoint;
	fObjectTrimBoxWidth	  /= fPSPoint; fObjectTrimBoxHeight	 /= fPSPoint;
	fBottlingOffsetX	  /= fPSPoint; fBottlingOffsetY		 /= fPSPoint;

	float fOriginX, fOriginY;
	CPDFOutput::CalcPDFObjectOrigin(fOriginX, fOriginY, (float)fAngle, fSheetExposureCenterX, fSheetExposureCenterY, fObjectBBoxCenterX, fObjectBBoxCenterY);
	IPL_TDMATRIX ctm = CPDFOutput::CalcPDFObjectMatrix (fOriginX + fBottlingOffsetX, fOriginY + fBottlingOffsetY, (float)fAngle + fBottlingAngle, fScaleX, fScaleY);

	float fSheetTrimBoxCenterX = ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? fObjectTrimBoxLeft   + fObjectTrimBoxWidth /2.0f : fObjectTrimBoxLeft   + fObjectTrimBoxHeight/2.0f;
	float fSheetTrimBoxCenterY = ((nHeadPosition == TOP) || (nHeadPosition == BOTTOM)) ? fObjectTrimBoxBottom + fObjectTrimBoxHeight/2.0f : fObjectTrimBoxBottom + fObjectTrimBoxWidth /2.0f;
	CPDFOutput::CalcPDFObjectOrigin(fOriginX, fOriginY, (float)fAngle, fSheetTrimBoxCenterX, fSheetTrimBoxCenterY, fObjectTrimBoxWidth/2.0f, fObjectTrimBoxHeight/2.0f);
	IPL_TDMATRIX trimctm = CPDFOutput::CalcPDFObjectMatrix (fOriginX, fOriginY, (float)fAngle, 1.0f, 1.0f);		// 08//14/2012:	changed from fScaleX, fScaleY to 1.0f, 1.0f, because trimbox has nothing to do with content (ctm)

	CheckDonglePDF(FALSE, FALSE);

	CPrintSheetTableView* pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	//if (! pPrintSheetTableView)
	//	return m_bCancelJob;

	CImpManDoc* pDoc			   = CImpManDoc::GetDoc();
	int			nDocsDongleStatus  = (pDoc) ? pDoc->m_nDongleStatus : CImpManApp::NoDongle;
	BOOL		bAddWatermarks     = TRUE;
	switch (theApp.m_nDongleStatus)
	{
	case CImpManApp::NoDongle:			
	case CImpManApp::ClientDongle:		bAddWatermarks = TRUE; break;

	case CImpManApp::SmallFormatDongle:	
	case CImpManApp::StandardDongle:	
	case CImpManApp::NewsDongle:	
	case CImpManApp::DigitalDongle:	
	case CImpManApp::AutoDongle:		if ( (theApp.m_nGUIStyle == CImpManApp::GUIStandard) && (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp) )
											bAddWatermarks = ( (nDocsDongleStatus == CImpManApp::NoDongle) || 
															   (nDocsDongleStatus == CImpManApp::ClientDongle) ) ? TRUE  : FALSE;
										else
											bAddWatermarks = TRUE;	
										if ( (theApp.m_nDongleStatus == CImpManApp::SmallFormatDongle) && ! bAddWatermarks)
											if (m_bOutputFormatExceeded)
												bAddWatermarks = TRUE;
										if ( (theApp.m_nDongleStatus == CImpManApp::NewsDongle) && ! bAddWatermarks)
											if (m_bOutputFormatExceeded)
												bAddWatermarks = TRUE;
										if ( (theApp.m_nDongleStatus == CImpManApp::DigitalDongle) && ! bAddWatermarks)
											if (m_bOutputFormatExceeded)
												bAddWatermarks = TRUE;
										break;

	default:							bAddWatermarks = TRUE; break;
	}

	CRect cancelRect;
	if (pPrintSheetTableView)
	{
		int nButtonIndex = pPrintSheetTableView->m_outputListToolBarCtrl.CommandToIndex(ID_CANCEL_OUTPUT);
		pPrintSheetTableView->m_outputListToolBarCtrl.GetItemRect(nButtonIndex, cancelRect);
		pPrintSheetTableView->m_outputListToolBarCtrl.ClientToScreen(cancelRect);
	}

	if (bAddWatermarks)
		if (bSkipIfDemo)
		{
			fObjectTrimBoxWidth  /= 2.0f;
			fObjectTrimBoxHeight /= 2.0f;
			float fWidth  = fSheetClipBoxRight - fSheetClipBoxLeft;
			float fHeight = fSheetClipBoxTop   - fSheetClipBoxBottom;
			fSheetClipBoxLeft   += fWidth  / 4.0f;
			fSheetClipBoxRight  -= fWidth  / 4.0f;
			fSheetClipBoxBottom += fHeight / 4.0f;
			fSheetClipBoxTop	-= fHeight / 4.0f;
		}

	IPL_TDRECT sheetClipBox  = {fSheetClipBoxLeft, fSheetClipBoxBottom, fSheetClipBoxRight, fSheetClipBoxTop};
	IPL_TDRECT objectTrimBox = {0.0f, 0.0f, fObjectTrimBoxWidth, fObjectTrimBoxHeight};

	JDFOUT_AppendContentObject(nOrd, ctm, trimctm, sheetClipBox, objectTrimBox);

	CString strCancelPDFOutput;
	strCancelPDFOutput.LoadString(ID_CANCEL_OUTPUT);

	if (pPrintSheetTableView)
	{
		// peek for next input messages
		MSG msg;
		while (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_LBUTTONDOWN)
			{
				CPoint ptMousePos;
				GetCursorPos(&ptMousePos);
				if (cancelRect.PtInRect(ptMousePos))
				{
					//pPrintSheetTableView->m_wndAnimate.Stop(); 
					CDlgStopOutput dlg;
					switch (dlg.DoModal())
					{
					case IDC_STOP_OUTPUT_ALL:	m_bCancelJob = 2;	  break;
					case IDC_STOP_OUTPUT_SHEET: m_bCancelJob = TRUE;  break;
					case IDC_PROCEED_OUTPUT:	m_bCancelJob = FALSE; break;
					}
					//pPrintSheetTableView->m_wndAnimate.Play(0, (unsigned)-1, (unsigned)-1);
				}
			}
			if (msg.message == WM_MOUSEMOVE)
			{
				CPoint ptMousePos;
				GetCursorPos(&ptMousePos);
				if (cancelRect.PtInRect(ptMousePos))
					pPrintSheetTableView->m_strPDFOutputMessage = strCancelPDFOutput;
				else
					pPrintSheetTableView->m_strPDFOutputMessage = "";
				pPrintSheetTableView->UpdateData(FALSE);
			}
			if (msg.message == WM_PAINT)
				if (!AfxGetApp()->PreTranslateMessage(&msg))
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
		}
	}

	return m_bCancelJob;
}

void CJDFOutputImposition::CreateMimeFile()
{
	if ( ! m_bJDFOutputMime)
		return;

	CString			strPDFFile;
	JDFDoc*			pJMFDoc = NULL;
	MIMEMessage*	mmsg	= NULL;
	MIMEBasicPart*  mbp		= NULL;
	MIMEMultiPart*	mmp		= NULL;
	MIMEBodyPart*	mbpdf	= NULL;
	try
	{
		pJMFDoc = new JDFDoc(1);
		JDFJMF jmf = pJMFDoc->GetRoot(); 
		jmf.init();
		jmf.SetSenderID("MessageSenderDevice");

		CString strFilename = m_strOutputFilename;
		theApp.RemoveExtension(strFilename);

		JDFCommand commandSQE=jmf.AppendCommand(JDFCommand::Type_SubmitQueueEntry);
		JDFQueueSubmissionParams qsp=commandSQE.AppendQueueSubmissionParams();
		qsp.SetURL((WString)CString(_T("cid:") + strFilename + _T(".jdf")));

		mmsg=pJMFDoc->CreateMIMEMessage();

/////// include JDF part
		mmp=(MIMEMultiPart*)mmsg->getBody(false);
		int iJDFPart=mmp->addBodyPart(WString(m_strOutputJDFPath), MIMEBodyPart::BINARY);
		mbpdf=mmp->getBodyPart(iJDFPart,false);
		mbpdf->setContentID((WString)CString(_T("<") + strFilename + _T(".jdf") + _T(">")));
///////////

/////// include PDF part
		//if (m_bMarksInFolder &&  ! m_strMarksFolder.IsEmpty())
		//	strPDFFile = m_strMarksFolder + _T("\\") + strFilename;		// in case of mime, marks always located in temp dir
		//else
			strPDFFile.Format(_T("%s\\%s_Marks.pdf"), theApp.settings.m_szTempDir, strFilename);		

		int iPDFPart=mmp->addBodyPart(WString(strPDFFile), MIMEBodyPart::BINARY);
		mbpdf=mmp->getBodyPart(iPDFPart,false);
		mbpdf->setContentID((WString)CString(_T("<") + strFilename + _T("_Marks.pdf") + _T(">")));
////////////

		mmsg->Write2URL((WString)WindowsPath2URL(m_strOutputMimePath));
		delete pJMFDoc;
		delete mmsg;
	} catch (JDF::Exception& )
	{
		if (pJMFDoc)
			delete pJMFDoc;
		if (mmsg)
			delete mmsg;
	}

	DeleteFile(m_strOutputJDFPath);
	DeleteFile(strPDFFile);
}