// DlgProductionPrepressParams.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionManager.h"
#include "DlgProductionPrepressParams.h"


// CDlgProductionPrepressParams-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionPrepressParams, CDialog)

CDlgProductionPrepressParams::CDlgProductionPrepressParams(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionPrepressParams::IDD, pParent)
{
	m_fBleed = 0.0f;
	m_strNoOutput.LoadString(IDS_NO_OUTPUT);
}

CDlgProductionPrepressParams::~CDlgProductionPrepressParams()
{
}

void CDlgProductionPrepressParams::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PD_BLEED_SPIN, m_bleedSpin);
	DDX_Measure(pDX, IDC_PD_BLEED_EDIT, m_fBleed);
	DDX_Control(pDX, IDC_PD_PRINTSHEET_OUTPUTPROFILE_COMBO, m_printSheetOutputProfileCombo);
	DDX_Control(pDX, IDC_PD_FOLDSHEET_OUTPUTPROFILE_COMBO, m_foldSheetOutputProfileCombo);
	DDX_Control(pDX, IDC_PD_BOOKLET_OUTPUTPROFILE_COMBO, m_bookletOutputProfileCombo);
}

BEGIN_MESSAGE_MAP(CDlgProductionPrepressParams, CDialog)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PD_BLEED_SPIN, OnDeltaposBleedSpin)
	ON_EN_KILLFOCUS(IDC_PD_BLEED_EDIT, OnKillfocusBleedEdit)
	ON_CBN_SELCHANGE(IDC_PD_PRINTSHEET_OUTPUTPROFILE_COMBO, OnSelchangePrintSheetOutputProfileCombo)
	ON_CBN_SELCHANGE(IDC_PD_FOLDSHEET_OUTPUTPROFILE_COMBO, OnSelchangeFoldSheetOutputProfileCombo)
	ON_CBN_SELCHANGE(IDC_PD_BOOKLET_OUTPUTPROFILE_COMBO, OnSelchangeBookletOutputProfileCombo)
	ON_BN_CLICKED(IDC_PD_PRINTSHEET_BROWSE_OUTPUTPROFILE, OnBnClickedPdBrowsePrintSheetOutputProfile)
	ON_BN_CLICKED(IDC_PD_FOLDSHEET_BROWSE_OUTPUTPROFILE, OnBnClickedPdBrowseFoldSheetOutputProfile)
	ON_BN_CLICKED(IDC_PD_BOOKLET_BROWSE_OUTPUTPROFILE, OnBnClickedPdBrowseBookletOutputProfile)
END_MESSAGE_MAP()


// CDlgProductionPrepressParams-Meldungshandler

BOOL CDlgProductionPrepressParams::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_bleedSpin.SetRange(0, 1);  

	CBitmap bitmap;
	bitmap.LoadBitmap(IDB_ABORT_COLD);
	CImageList imageList;
	imageList.Create(16, 16, ILC_COLORDDB|ILC_MASK, 4, 0);	// List not to grow
	imageList.Add(&bitmap, BLACK);
	imageList.Add(theApp.LoadIcon(IDI_PDF));
	imageList.Add(theApp.LoadIcon(IDI_PRINTER));
	imageList.Add(theApp.LoadIcon(IDI_JDF));
	m_printSheetOutputProfileCombo.SetImageList(&imageList);
	m_foldSheetOutputProfileCombo.SetImageList(&imageList);
	m_bookletOutputProfileCombo.SetImageList(&imageList);
	imageList.Detach();

	COMBOBOXEXITEM item;
	item.mask = CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;

	m_printSheetTargetList.Load(CPDFTargetList::PrintSheetTarget);
	m_printSheetOutputProfileCombo.ResetContent();
	if (m_printSheetTargetList.GetSize())
	{
		item.iItem			= 0;
		item.pszText		= m_strNoOutput.GetBuffer();
		item.iImage			= 0;
		item.iSelectedImage = 0;
		m_printSheetOutputProfileCombo.InsertItem(&item);
		for (int i = 0; i < m_printSheetTargetList.GetSize(); i++)
		{
			item.iItem			= m_printSheetOutputProfileCombo.GetCount();
			item.pszText		=  m_printSheetTargetList[i].m_strTargetName.GetBuffer(MAX_TEXT);
			item.iImage			= (m_printSheetTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 1 : (m_printSheetTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 2 : 3;
			item.iSelectedImage = item.iImage;
			m_printSheetOutputProfileCombo.InsertItem(&item);
		}
	}

	m_foldSheetTargetList.Load(CPDFTargetList::FoldSheetTarget);
	m_foldSheetOutputProfileCombo.ResetContent();
	if (m_foldSheetTargetList.GetSize())
	{
		item.iItem			= 0;
		item.pszText		= m_strNoOutput.GetBuffer();
		item.iImage			= 0;
		item.iSelectedImage = 0;
		m_foldSheetOutputProfileCombo.InsertItem(&item);
		for (int i = 0; i < m_foldSheetTargetList.GetSize(); i++)
		{
			item.iItem			= m_foldSheetOutputProfileCombo.GetCount();
			item.pszText		=  m_foldSheetTargetList[i].m_strTargetName.GetBuffer(MAX_TEXT);
			item.iImage			= (m_foldSheetTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 1 : (m_foldSheetTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 2 : 3;
			item.iSelectedImage = item.iImage;
			m_foldSheetOutputProfileCombo.InsertItem(&item);
		}
	}

	m_bookletTargetList.Load(CPDFTargetList::BookletTarget);
	m_bookletOutputProfileCombo.ResetContent();
	if (m_bookletTargetList.GetSize())
	{
		item.iItem			= 0;
		item.pszText		= m_strNoOutput.GetBuffer();
		item.iImage			= 0;
		item.iSelectedImage = 0;
		m_bookletOutputProfileCombo.InsertItem(&item);
		for (int i = 0; i < m_bookletTargetList.GetSize(); i++)
		{
			item.iItem			= m_bookletOutputProfileCombo.GetCount();
			item.pszText		=  m_bookletTargetList[i].m_strTargetName.GetBuffer(MAX_TEXT);
			item.iImage			= (m_bookletTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 1 : (m_bookletTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 2 : 3;
			item.iSelectedImage = item.iImage;
			m_bookletOutputProfileCombo.InsertItem(&item);
		}
	}

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}


void CDlgProductionPrepressParams::InitData()
{
	CDlgProductionManager*	pParent			= (CDlgProductionManager*)GetParent();
	CPrepressParams&		rPrepressParams	= pParent->m_productionProfile.m_prepress;

	m_fBleed					 = rPrepressParams.m_fBleed;
	m_strPrintSheetOutputProfile = rPrepressParams.m_strPrintSheetOutputProfile;
	m_strFoldSheetOutputProfile  = rPrepressParams.m_strFoldSheetOutputProfile;
	m_strBookletOutputProfile	 = rPrepressParams.m_strBookletOutputProfile;

	if (m_strPrintSheetOutputProfile.IsEmpty())
		m_strPrintSheetOutputProfile = m_strNoOutput;
	if (m_strFoldSheetOutputProfile.IsEmpty())
		m_strFoldSheetOutputProfile = m_strNoOutput;
	if (m_strBookletOutputProfile.IsEmpty())
		m_strBookletOutputProfile = m_strNoOutput;

	int nIndex = m_printSheetOutputProfileCombo.FindStringExact(-1, m_strPrintSheetOutputProfile);
	m_printSheetOutputProfileCombo.SetCurSel(nIndex);
	nIndex = m_foldSheetOutputProfileCombo.FindStringExact(-1, m_strFoldSheetOutputProfile);
	m_foldSheetOutputProfileCombo.SetCurSel(nIndex);
	nIndex = m_bookletOutputProfileCombo.FindStringExact(-1, m_strBookletOutputProfile);
	m_bookletOutputProfileCombo.SetCurSel(nIndex);

	UpdateData(FALSE);
}

void CDlgProductionPrepressParams::SaveData()
{
	UpdateData(TRUE);

	CDlgProductionManager*	pParent			= (CDlgProductionManager*)GetParent();
	CPrepressParams&		rPrepressParams	= pParent->m_productionProfile.m_prepress;

	if (m_strPrintSheetOutputProfile == m_strNoOutput)
		m_strPrintSheetOutputProfile.Empty();
	if (m_strFoldSheetOutputProfile == m_strNoOutput)
		m_strFoldSheetOutputProfile.Empty();
	if (m_strBookletOutputProfile == m_strNoOutput)
		m_strBookletOutputProfile.Empty();

	rPrepressParams.m_fBleed					 = m_fBleed;
	rPrepressParams.m_strPrintSheetOutputProfile = m_strPrintSheetOutputProfile;
	rPrepressParams.m_strFoldSheetOutputProfile	 = m_strFoldSheetOutputProfile;
	rPrepressParams.m_strBookletOutputProfile	 = m_strBookletOutputProfile;
}

void CDlgProductionPrepressParams::OnDeltaposBleedSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_bleedSpin.GetBuddy(), pNMUpDown->iDelta, 0.5f);
	*pResult = 0;

	OnKillfocusBleedEdit();
}

void CDlgProductionPrepressParams::OnKillfocusBleedEdit() 
{
	UpdateData(TRUE);
}

void CDlgProductionPrepressParams::OnSelchangePrintSheetOutputProfileCombo() 
{
	UpdateData(TRUE);

	int	nSel = m_printSheetOutputProfileCombo.GetCurSel();
	m_printSheetOutputProfileCombo.GetLBText(nSel, m_strPrintSheetOutputProfile);
}

void CDlgProductionPrepressParams::OnBnClickedPdBrowsePrintSheetOutputProfile()
{
	m_pDlgOutputProfile = new CDlgPDFOutputMediaSettings(this, TRUE);
	m_pDlgOutputProfile->m_nOutputTarget	    = CPDFTargetList::PrintSheetTarget;
	m_pDlgOutputProfile->m_strPDFTargetName		= m_strPrintSheetOutputProfile;
	m_pDlgOutputProfile->m_pWndMsgTarget		= this;
	m_pDlgOutputProfile->m_bNoSheetSelection	= TRUE;
	m_pDlgOutputProfile->m_nWhatSheets			= -1;	
	m_pDlgOutputProfile->m_strWhatLayout		= _T("");

	if (m_pDlgOutputProfile->DoModal() == IDOK)
	{
		m_printSheetTargetList.Load(CPDFTargetList::PrintSheetTarget);
		m_printSheetOutputProfileCombo.ResetContent();

		COMBOBOXEXITEM item;
		item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;
		item.iItem			= 0;
		item.pszText		= m_strNoOutput.GetBuffer();
		item.iImage			= 0;
		item.iSelectedImage = 0;
		m_printSheetOutputProfileCombo.InsertItem(&item);
		for (int i = 0; i < m_printSheetTargetList.GetSize(); i++)
		{
			item.iItem			= m_printSheetOutputProfileCombo.GetCount();
			item.pszText		=  m_printSheetTargetList[i].m_strTargetName.GetBuffer(MAX_TEXT);
			item.iImage			= (m_printSheetTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 1 : (m_printSheetTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 2 : 3;
			item.iSelectedImage = item.iImage;
			m_printSheetOutputProfileCombo.InsertItem(&item);
		}
		m_strPrintSheetOutputProfile = m_pDlgOutputProfile->m_strPDFTargetName;
	}

	delete m_pDlgOutputProfile;
	m_pDlgOutputProfile = NULL;

	int nIndex = m_printSheetOutputProfileCombo.FindStringExact(-1, m_strPrintSheetOutputProfile);
	m_printSheetOutputProfileCombo.SetCurSel(nIndex);
}

void CDlgProductionPrepressParams::OnSelchangeFoldSheetOutputProfileCombo() 
{
	UpdateData(TRUE);

	int	nSel = m_foldSheetOutputProfileCombo.GetCurSel();
	m_foldSheetOutputProfileCombo.GetLBText(nSel, m_strFoldSheetOutputProfile);
}

void CDlgProductionPrepressParams::OnBnClickedPdBrowseFoldSheetOutputProfile()
{
	m_pDlgOutputProfile = new CDlgPDFOutputMediaSettings(this, TRUE);
	m_pDlgOutputProfile->m_strPDFTargetName		= m_strFoldSheetOutputProfile;
	m_pDlgOutputProfile->m_nOutputTarget	    = CPDFTargetList::FoldSheetTarget;
	m_pDlgOutputProfile->m_pWndMsgTarget		= this;
	m_pDlgOutputProfile->m_bNoSheetSelection	= TRUE;
	m_pDlgOutputProfile->m_nWhatSheets			= -1;	
	m_pDlgOutputProfile->m_strWhatLayout		= _T("");

	if (m_pDlgOutputProfile->DoModal() == IDOK)
	{
		m_foldSheetTargetList.Load(CPDFTargetList::FoldSheetTarget);
		m_foldSheetOutputProfileCombo.ResetContent();
		if (m_foldSheetTargetList.GetSize())
		{
			COMBOBOXEXITEM item;
			item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;
			item.iItem			= 0;
			item.pszText		= m_strNoOutput.GetBuffer();
			item.iImage			= 0;
			item.iSelectedImage = 0;
			m_foldSheetOutputProfileCombo.InsertItem(&item);
			for (int i = 0; i < m_foldSheetTargetList.GetSize(); i++)
			{
				item.iItem			= m_foldSheetOutputProfileCombo.GetCount();
				item.pszText		=  m_foldSheetTargetList[i].m_strTargetName.GetBuffer(MAX_TEXT);
				item.iImage			= (m_foldSheetTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 1 : (m_foldSheetTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 2 : 3;
				item.iSelectedImage = item.iImage;
				m_foldSheetOutputProfileCombo.InsertItem(&item);
			}
		}
		m_strFoldSheetOutputProfile = m_pDlgOutputProfile->m_strPDFTargetName;
	}

	delete m_pDlgOutputProfile;
	m_pDlgOutputProfile = NULL;

	int nIndex = m_foldSheetOutputProfileCombo.FindStringExact(-1, m_strFoldSheetOutputProfile);
	m_foldSheetOutputProfileCombo.SetCurSel(nIndex);
}

void CDlgProductionPrepressParams::OnSelchangeBookletOutputProfileCombo() 
{
	UpdateData(TRUE);

	int	nSel = m_bookletOutputProfileCombo.GetCurSel();
	m_bookletOutputProfileCombo.GetLBText(nSel, m_strBookletOutputProfile);
}

void CDlgProductionPrepressParams::OnBnClickedPdBrowseBookletOutputProfile()
{
	m_pDlgOutputProfile = new CDlgPDFOutputMediaSettings(this, TRUE);
	m_pDlgOutputProfile->m_nOutputTarget	    = CPDFTargetList::BookletTarget;
	m_pDlgOutputProfile->m_strPDFTargetName		= m_strBookletOutputProfile;
	m_pDlgOutputProfile->m_pWndMsgTarget		= this;
	m_pDlgOutputProfile->m_bNoSheetSelection	= TRUE;
	m_pDlgOutputProfile->m_nWhatSheets			= -1;	
	m_pDlgOutputProfile->m_strWhatLayout		= _T("");

	if (m_pDlgOutputProfile->DoModal() == IDOK)
	{
		m_bookletTargetList.Load(CPDFTargetList::BookletTarget);
		m_bookletOutputProfileCombo.ResetContent();
		if (m_bookletTargetList.GetSize())
		{
			COMBOBOXEXITEM item;
			item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;
			item.iItem			= 0;
			item.pszText		= m_strNoOutput.GetBuffer();
			item.iImage			= 0;
			item.iSelectedImage = 0;
			m_bookletOutputProfileCombo.InsertItem(&item);
			for (int i = 0; i < m_bookletTargetList.GetSize(); i++)
			{
				item.iItem			= m_bookletOutputProfileCombo.GetCount();
				item.pszText		=  m_bookletTargetList[i].m_strTargetName.GetBuffer(MAX_TEXT);
				item.iImage			= (m_bookletTargetList[i].m_nType == CPDFTargetProperties::TypePDFFolder) ? 1 : (m_bookletTargetList[i].m_nType == CPDFTargetProperties::TypePDFPrinter) ? 2 : 3;
				item.iSelectedImage = item.iImage;
				m_bookletOutputProfileCombo.InsertItem(&item);
			}
		}
		m_strBookletOutputProfile = m_pDlgOutputProfile->m_strPDFTargetName;
	}

	delete m_pDlgOutputProfile;
	m_pDlgOutputProfile = NULL;

	int nIndex = m_bookletOutputProfileCombo.FindStringExact(-1, m_strBookletOutputProfile);
	m_bookletOutputProfileCombo.SetCurSel(nIndex);
}
