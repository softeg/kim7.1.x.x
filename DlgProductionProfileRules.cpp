// DlgProductionProfileRules.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionProfileRules.h"
#include "DlgProductionManager.h"
#include "DlgEditMarkRule.h"
#include "ExpressionParser.h"


// CDlgProductionProfileRules-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionProfileRules, CDialog)

CDlgProductionProfileRules::CDlgProductionProfileRules(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProductionProfileRules::IDD, pParent)
{

}

CDlgProductionProfileRules::~CDlgProductionProfileRules()
{
}

void CDlgProductionProfileRules::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PPRULES_LIST, m_rulesList);
	DDX_Radio(pDX, IDC_PPRULE_RELATION_AND, m_nRulesRelation);
}


BEGIN_MESSAGE_MAP(CDlgProductionProfileRules, CDialog)
	ON_BN_CLICKED(IDC_ADD_PPRULE_BUTTON, OnBnClickedAddRuleButton)
	ON_BN_CLICKED(IDC_MODIFY_PPRULE_BUTTON, OnBnClickedModifyRuleButton)
	ON_BN_CLICKED(IDC_REMOVE_PPRULE_BUTTON, OnBnClickedRemoveRuleButton)
	ON_UPDATE_COMMAND_UI(IDC_MODIFY_PPRULE_BUTTON, OnUpdateModifyRuleButton)
	ON_UPDATE_COMMAND_UI(IDC_REMOVE_PPRULE_BUTTON, OnUpdateRemoveRuleButton)
	ON_UPDATE_COMMAND_UI(IDC_PPRULE_RELATION_AND, OnUpdateRuleRelationAnd)
	ON_UPDATE_COMMAND_UI(IDC_PPRULE_RELATION_OR, OnUpdateRuleRelationOr)
	ON_LBN_DBLCLK(IDC_PPRULES_LIST, OnLbnDblclkRulesList)
	ON_BN_CLICKED(IDC_PPRULE_RELATION_AND, &CDlgProductionProfileRules::OnBnClickedRuleRelationAnd)
	ON_BN_CLICKED(IDC_PPRULE_RELATION_OR, &CDlgProductionProfileRules::OnBnClickedRuleRelationOr)
END_MESSAGE_MAP()


// CDlgProductionProfileRules-Meldungshandler

BOOL CDlgProductionProfileRules::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls(this, TRUE);

	return CDialog::PreTranslateMessage(pMsg);
}


BOOL CDlgProductionProfileRules::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_MODIFY_PPRULE_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_REMOVE_PPRULE_BUTTON)->EnableWindow(FALSE);

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}


void CDlgProductionProfileRules::InitData()
{
	m_rulesList.ResetContent();

	CDlgProductionManager* 	pParent	= (CDlgProductionManager*)GetParent();
	CKimRules&				rRules  = pParent->m_productionProfile.m_rules;

	CString strExpression = rRules.GetRuleExpression(0);
	if ( ! strExpression.IsEmpty())
	{
		m_nRulesRelation = 0;

		CString strAction;
		int		nCurPos	 = 0;
		CString strToken = strExpression.Tokenize(_T("\n"), nCurPos);
		while (strToken != _T(""))
		{
			strToken.TrimLeft(_T("(")); strToken.TrimRight(_T(")"));
			if (strToken.CompareNoCase(_T("and")) == 0)
				m_nRulesRelation = 0;
			else
			if (strToken.CompareNoCase(_T("or")) == 0)
				m_nRulesRelation = 1;
			m_rulesList.AddString(strToken);
			strToken = strExpression.Tokenize(_T("\n"), nCurPos);
		}
	}

	UpdateData(FALSE);
}

void CDlgProductionProfileRules::SaveData()
{
	UpdateData(TRUE);

	CDlgProductionManager* 	pParent	= (CDlgProductionManager*)GetParent();
	CKimRules&				rRules  = pParent->m_productionProfile.m_rules;

	CString strExpression;
	for (int i = 0; i < m_rulesList.GetCount(); i++)
	{
		CString strLine;
		m_rulesList.GetText(i, strLine);
	
		CString strOut;
		if ( (strLine != _T("and")) && (strLine != _T("or")) )
			strOut.Format(_T("(%s)\n"), strLine);
		else
			strOut.Format(_T("%s\n"), strLine);

		strExpression += strOut;
	}
	strExpression.TrimLeft(_T("\n "));
	strExpression.TrimRight(_T("\n "));

	//if (rRules.m_ruleExpressions.GetSize() > 0)
		rRules.SetRuleExpression(0, strExpression);
}

void CDlgProductionProfileRules::OnBnClickedAddRuleButton()
{
	CDlgEditMarkRule dlg;
	dlg.m_nRuleSet = CDlgEditMarkRule::ProductionProfileRules;

	if (dlg.DoModal() == IDCANCEL)
		return;

	CString strExpression;
	strExpression.Format(_T("[%s] %s \"%s\""), dlg.m_strVariable, dlg.m_strOperator, dlg.m_strValue);
	if (m_rulesList.GetCount() <= 0)
		m_rulesList.AddString(strExpression);
	else
	{
		m_rulesList.AddString((m_nRulesRelation == 0) ? _T("and") : _T("or"));
		m_rulesList.AddString(strExpression);
	}
}

void CDlgProductionProfileRules::OnBnClickedModifyRuleButton()
{
	int nSel = m_rulesList.GetCurSel();
	if (nSel == LB_ERR)
		return;

	CDlgEditMarkRule dlg;
	dlg.m_nRuleSet = CDlgEditMarkRule::ProductionProfileRules;

	CString strExpression;
	m_rulesList.GetText(nSel, strExpression);

	CExpressionParser parser;
	parser.ParseString(strExpression);

	dlg.m_strVariable = parser.m_strVariable;
	dlg.m_strOperator = parser.m_strOperator;
	dlg.m_strValue	  = parser.m_strValue;

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_rulesList.DeleteString(nSel);
	if (dlg.m_strVariable == _T("NoRule"))
		strExpression.Format(_T("[%s]"), dlg.m_strVariable);
	else
		strExpression.Format(_T("[%s] %s \"%s\""), dlg.m_strVariable, dlg.m_strOperator, dlg.m_strValue);
	m_rulesList.InsertString(nSel, strExpression);
}

void CDlgProductionProfileRules::OnBnClickedRemoveRuleButton()
{
	int nSel = m_rulesList.GetCurSel();
	if (nSel == LB_ERR)
		return;

	if (m_rulesList.GetCount() == 1)
		m_rulesList.DeleteString(nSel);
	else
		if (nSel == 0)
		{
			m_rulesList.DeleteString(nSel);
			m_rulesList.DeleteString(nSel);
		}
		else
		{
			m_rulesList.DeleteString(nSel-1);
			m_rulesList.DeleteString(nSel-1);
		}
}

void CDlgProductionProfileRules::OnUpdateModifyRuleButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CheckSelection());
}

void CDlgProductionProfileRules::OnUpdateRemoveRuleButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(CheckSelection());
}

void CDlgProductionProfileRules::OnUpdateRuleRelationAnd(CCmdUI* pCmdUI)
{
	pCmdUI->Enable((m_rulesList.GetCount() > 1));
}

void CDlgProductionProfileRules::OnUpdateRuleRelationOr(CCmdUI* pCmdUI)
{
	pCmdUI->Enable((m_rulesList.GetCount() > 1));
}

BOOL CDlgProductionProfileRules::CheckSelection()
{
	UpdateData(TRUE);

	int nSel = m_rulesList.GetCurSel();
	if (nSel == LB_ERR)
		return FALSE;

	CString strExpression;
	m_rulesList.GetText(nSel, strExpression);

	if ( (strExpression == _T("and")) || (strExpression == _T("or")) )
		return FALSE;
	else
		return TRUE;
}

void CDlgProductionProfileRules::OnLbnDblclkRulesList()
{
	OnBnClickedModifyRuleButton();
}

void CDlgProductionProfileRules::OnBnClickedRuleRelationAnd()
{
	UpdateData(TRUE);

	CString strExpression;
	for (int i = 0; i < m_rulesList.GetCount(); i++)
	{
		CString strLine;
		m_rulesList.GetText(i, strLine);
		if (strLine == _T("or"))
		{
			m_rulesList.DeleteString(i);
			m_rulesList.InsertString(i, _T("and"));
		}
	}

	SaveData();
}

void CDlgProductionProfileRules::OnBnClickedRuleRelationOr()
{
	UpdateData(TRUE);

	CString strExpression;
	for (int i = 0; i < m_rulesList.GetCount(); i++)
	{
		CString strLine;
		m_rulesList.GetText(i, strLine);
		if (strLine == _T("and"))
		{
			m_rulesList.DeleteString(i);
			m_rulesList.InsertString(i, _T("or"));
		}
	}

	SaveData();
}
