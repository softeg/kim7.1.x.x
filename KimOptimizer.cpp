#include "stdafx.h"
#include "Imposition Manager.h"
#include "KimOptimizer.h"
#include "KimSocketsCommon.h"


BOOL g_bReady = FALSE;


CKimOptimizer::CKimOptimizer(CDlgOptimizationMonitor* pMonitor)
{
	m_pMonitor = pMonitor;
}

CKimOptimizer::~CKimOptimizer(void)
{
}

BOOL CKimOptimizer::DoOptimizePrintGroup(CPrintGroup& rPrintGroup)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CPrintSheet* pPrintSheet = rPrintGroup.GetFirstPrintSheet();
	if ( ! pPrintSheet)
		return FALSE;

	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pPrintSheet || ! pLayout)
		return FALSE;

	CPrintingProfile& rPrintingProfile = rPrintGroup.GetPrintingProfile();

	pPrintSheet->m_strPDFTarget = rPrintingProfile.m_prepress.m_strPrintSheetOutputProfile;

	if ( ! rPrintingProfile.m_press.m_availableSheets.GetSize())
		return FALSE;

	pLayout->m_variants.RemoveAll();

	for (int i = 0; i < rPrintingProfile.m_press.m_availableSheets.GetSize(); i++)
	{
		CSheet sheet = rPrintingProfile.m_press.m_availableSheets[i];

		if (m_pMonitor)
		{
			m_pMonitor->m_strCurrentSheet = pPrintSheet->GetNumber();
			m_pMonitor->m_strCurrentPaper = sheet.m_strSheetName;
			m_pMonitor->UpdateData(FALSE);
		}

		float fGripper = (rPrintingProfile.m_press.m_pressDevice.m_nPressType == CPressDevice::SheetPress) ? rPrintingProfile.m_press.m_pressDevice.m_fGripper : 0.0f;
		float fSheetWidth  = sheet.m_fWidth;
		float fSheetHeight = sheet.m_fHeight - fGripper;

		CCuttingProcess cuttingProcess  = (rPrintingProfile.ProcessIsActive(CProductionProfile::Cutting)) ? rPrintingProfile.m_cutting : CCuttingProcess();
		fSheetWidth  -= cuttingProcess.m_fLeftBorder  + cuttingProcess.m_fRightBorder;
		fSheetHeight -= cuttingProcess.m_fLowerBorder + cuttingProcess.m_fUpperBorder;

		CLayoutVariant variant;
		int nResult = OptimizeVariant(variant, rPrintGroup, rPrintingProfile, fSheetWidth, fSheetHeight, sheet.m_nPaperGrain);
		if (nResult <= 0)
		{
			rPrintGroup.SetLock(TRUE);
			return FALSE;
		}

		for (int i = 0; i < m_sheetResult.nNumSheetObjects; i++)
		{
			m_sheetResult.sheetObjects[i].fPositionX += cuttingProcess.m_fLeftBorder;
			m_sheetResult.sheetObjects[i].fPositionY += cuttingProcess.m_fLowerBorder + fGripper;
		}

		variant.m_fWidth		 = sheet.m_fWidth;
		variant.m_fHeight		 = sheet.m_fHeight;
		variant.m_strFormatName  = sheet.m_strSheetName;
		variant.m_nPaperGrain	 = sheet.m_nPaperGrain;
		variant.m_nNumSheets	 = m_sheetResult.nSheetCopies;
		for (int j = 0; j < m_sheetResult.nNumSheetObjects; j++)
		{
			CLayoutVariantObject object;
			object.m_nObjectType	= (m_sheetResult.sheetObjects[j].szID >= 1000) ? CLayoutVariantObject::FoldSheet : CLayoutVariantObject::FlatProduct;
			object.m_nObjectID		= (m_sheetResult.sheetObjects[j].szID >= 1000) ? m_sheetResult.sheetObjects[j].szID - 1000 : m_sheetResult.sheetObjects[j].szID; 
			object.m_fPositionX		= m_sheetResult.sheetObjects[j].fPositionX; 
			object.m_fPositionY		= m_sheetResult.sheetObjects[j].fPositionY;
			object.m_nRotation		= m_sheetResult.sheetObjects[j].bturned;

			if (fabs(object.m_fPositionX) >  999999.0f)
				continue;
			if (fabs(object.m_fPositionY) >  999999.0f)
				continue;

			variant.m_objects.Add(object);
		}
		pLayout->m_variants.Add(variant);
	}

	pLayout->SelectLayoutVariant(*pPrintSheet, 0);

#ifdef KIM_ENGINE
	int nDBPdfOutputID = KimEngineDBSetOutputSheet(theApp.m_nDBCurrentJobID, -1, *pPrintSheet, 0, kimAutoDB::outPDF_idle);
	KimEngineNotifyServer(MSG_OUTPUTPDF_STATUS_CHANGED, nDBPdfOutputID);
#endif

	pPrintSheet->CalcTotalQuantity();

	if (m_pMonitor)
	{
		for (int i = 0; i < pPrintSheet->m_FoldSheetLayerRefs.GetSize(); i++)
		{
			CFoldSheetLayer* pLayer = pPrintSheet->GetFoldSheetLayer(i);
			if (pLayer)
				m_pMonitor->m_nProgress += pLayer->GetRealNumPages();
		}
		m_pMonitor->m_nProgress += pPrintSheet->m_flatProductRefs.GetSize();
		m_pMonitor->SetProgress(m_pMonitor->m_nProgress);
	}

	return TRUE;
}

int CKimOptimizer::OptimizeVariant(CLayoutVariant& rVariant, CPrintGroup& rPrintGroup, CPrintingProfile& rPrintingProfile, float fSheetWidth, float fSheetHeight, int nPaperGrain)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	CCuttingProcess&  rCuttingProcess  = rPrintingProfile.m_cutting;

	m_sheetGlobal.fSheetFormatX = fSheetWidth;
	m_sheetGlobal.fSheetFormatY = fSheetHeight;
	m_sheetGlobal.iConcision	= ( (rCuttingProcess.m_nBaseCutAlignment == BOTTOM) || (rCuttingProcess.m_nBaseCutAlignment == LEFT) ) ? 0 : 1;	
	m_sheetGlobal.nColumnWay	= (rCuttingProcess.m_nBaseCutDirection == VERTICAL) ? 2 : 1;
	m_sheetGlobal.nCopies		= 100000;

	CPrintGroupComponentList	componentList	= rPrintGroup.PrepareComponentsToOptimize(fSheetWidth, fSheetHeight, rPrintingProfile);
	int			  				nNumSorts		= 0;							
	int			  				nFoldSheetIndex = 0;							
	SHEETOPT_SORT 				sort; 
	for (int i = 0; i < componentList.GetSize(); i++)
	{
		if (componentList[i].m_nProductClass == CPrintGroupComponent::Bound)
		{
			CBoundProductPart& rProductPart = pDoc->m_boundProducts.GetProductPart(componentList[i].m_nProductClassIndex);
			int nNumUnassignedPages = componentList[i].m_nNumBoundPages;
			while (nNumUnassignedPages > 0)
			{
				CFoldSheet& rFoldSheet = rProductPart.m_foldingParams.FindBestFoldScheme(nNumUnassignedPages);
				if ( ! rFoldSheet.IsEmpty())
				{
					rFoldSheet.m_nProductPartIndex = componentList[i].m_nProductClassIndex;
					rFoldSheet.SetQuantityPlanned(rProductPart.m_nDesiredQuantity, FALSE);

					sort = CopyFoldSheetLayerToSort(rFoldSheet, fSheetWidth, fSheetHeight, rPrintingProfile, nPaperGrain);
					if ( (sort.nCopies > 0) && (nNumSorts < 256) )
					{
						sort.szID = 1000 + nFoldSheetIndex;
						m_sheetGlobal.sorts[nNumSorts] = sort;
						nNumSorts++;
						rVariant.m_foldSheetList.AddTail(rFoldSheet);
						nFoldSheetIndex++;
					}
					nNumUnassignedPages -= rFoldSheet.GetNumPages();
				}
				else
					break;
			}
		}
		else
		{
			CFlatProduct&  rFlatProduct  = pDoc->m_flatProducts.FindByIndex(componentList[i].m_nFlatProductIndex);
			sort = CopyFlatToSort(rFlatProduct, fSheetWidth, fSheetHeight, nPaperGrain);
			if ( (sort.nCopies > 0) && (nNumSorts < 256) )
			{
				sort.szID = componentList[i].m_nFlatProductIndex;
				m_sheetGlobal.sorts[nNumSorts] = sort;
				nNumSorts++;
			}
		}
	}

	if (nNumSorts == 0)
		return 0;

	if ( ! (theApp.m_nAddOnsLicensed & CImpManApp::Optimize))
		if (nNumSorts > 4)
		{
			KIMOpenLogMessage(IDS_OPTIMIZATION_NUMOBJECTS_EXCEEDED);
			nNumSorts = 4;
		}

	m_sheetGlobal.nNumSorts	= nNumSorts;

	m_sheetResult.nSheetCopies	   = 0;
	m_sheetResult.nNumSheetObjects = 0;
	for (int i = 0; i < 1024; i++)
	{
		m_sheetResult.sheetObjects[i].fPositionX = 0.0f;
		m_sheetResult.sheetObjects[i].fPositionY = 0.0f;
		m_sheetResult.sheetObjects[i].szID		 = -1;
		m_sheetResult.sheetObjects[i].bturned	 = 0;
	}

	g_bReady = FALSE;
	PrecutThread* pt = new PrecutThread(preCutReady); 
	BOOL bGoodEnough = FALSE;
	MSG  msg;
	int nCount = 0;
	while ( ! bGoodEnough && ! m_pMonitor->m_bEscapePressed && (nCount < 3) && (m_sheetGlobal.nNumSorts > 0) )
	{
		pt->runExecute1(m_sheetGlobal, &m_sheetResult);
		bGoodEnough = CheckResult(m_sheetResult, m_sheetGlobal, pDoc->m_productionSetup);
		if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_KEYDOWN && msg.wParam == VK_ESCAPE)
				if (AfxMessageBox(_T("Optimierung abbrechen?"), MB_YESNO) == IDYES)
					m_pMonitor->m_bEscapePressed = TRUE;
		}
		nCount++;
	}
	delete pt;

	float fMinX = FLT_MAX, fMinY = FLT_MAX;
	for (int i = 0; i < m_sheetResult.nNumSheetObjects; i++)
	{
		fMinX = min(fMinX, m_sheetResult.sheetObjects[i].fPositionX);
		fMinY = min(fMinY, m_sheetResult.sheetObjects[i].fPositionY);
	}
	for (int i = 0; i < m_sheetResult.nNumSheetObjects; i++)
	{
		m_sheetResult.sheetObjects[i].fPositionX -= fMinX;
		m_sheetResult.sheetObjects[i].fPositionY -= fMinY;
	}

	return (m_pMonitor->m_bEscapePressed) ? 0 : m_sheetResult.nNumSheetObjects;
}

BOOL CKimOptimizer::CheckResult(SHEETOPT_RESULT& rSheetResult, SHEETOPT_GLOBAL& rSheetGlobal, CProductionSetup& rProductionSetup)
{
	if (rProductionSetup.m_fMaxOverProduction <= 0.0f)
		return TRUE;

	typedef struct
	{
		int nID;
		int nTotalCopies;
	}SORT_TOTALS;

	CArray < SORT_TOTALS, SORT_TOTALS&> totals;

	for (int i = 0; i < rSheetGlobal.nNumSorts; i++)
	{
		SORT_TOTALS sortTotals = {rSheetGlobal.sorts[i].szID, 0};
		totals.Add(sortTotals);
	}

	for (int i = 0; i < totals.GetSize(); i++)
	{
		for (int j = 0; j < rSheetResult.nNumSheetObjects; j++)
		{
			if (totals[i].nID == rSheetResult.sheetObjects[j].szID)
				totals[i].nTotalCopies++;
		}
	}

	int j = 0;
	SHEETOPT_GLOBAL newSheetGlobal = rSheetGlobal;
	for (int i = 0; i < rSheetGlobal.nNumSorts; i++)
	{
		int	nRealQuantity = totals[i].nTotalCopies * rSheetResult.nSheetCopies;
		if (nRealQuantity <= 0)
		{
			newSheetGlobal.sorts[j] = rSheetGlobal.sorts[i];
			j++;
			continue;
		}
		int	  nDesiredQuantity = rSheetGlobal.sorts[i].nCopies;
		float fOverPercent	   = ((float)nRealQuantity/(float)nDesiredQuantity - 1.0f) * 100.0f;
		if (fOverPercent < rProductionSetup.m_fMaxOverProduction)
		{
			newSheetGlobal.sorts[j] = rSheetGlobal.sorts[i];
			j++;
		}
	}
	newSheetGlobal.nNumSorts = j;

	BOOL bGoodEnough = (newSheetGlobal.nNumSorts < rSheetGlobal.nNumSorts) ? FALSE : TRUE;

	rSheetGlobal = newSheetGlobal;

	return bGoodEnough;
}

SHEETOPT_SORT CKimOptimizer::CopyFoldSheetLayerToSort(CFoldSheet& rFoldSheet, float fSheetWidth, float fSheetHeight, CPrintingProfile& rPrintingProfile, int nPaperGrain)
{
	int nLayerIndex = 0;
	float fFoldSheetCutWidth, fFoldSheetCutHeight; 
	rFoldSheet.GetCutSize(nLayerIndex, fFoldSheetCutWidth, fFoldSheetCutHeight);

	SHEETOPT_SORT sort;

	if ( ! ObjectFitsToSheet(fFoldSheetCutWidth, fFoldSheetCutHeight, fSheetWidth, fSheetHeight, GRAIN_EITHER, nPaperGrain) )
	{
		sort.nCopies = 0;
		return sort;
	}

	int nFoldSheetGrain = GRAIN_EITHER;
	CFoldSheetLayer* pLayer = rFoldSheet.GetLayer(0);
	if (pLayer)
	{
		if (pLayer->m_HeadPositionArray.GetSize())
		{
			int nHeadPosition = pLayer->m_HeadPositionArray[0];
			switch (nHeadPosition)
			{
			case TOP:
			case BOTTOM:	nFoldSheetGrain = GRAIN_VERTICAL;	break;
			case LEFT:
			case RIGHT:		nFoldSheetGrain = GRAIN_HORIZONTAL; break;
			}
		}
	}

	sort.nPunchID 			= 0;
	sort.nPunchX  			= 0;
	sort.nPunchY  			= 0;
	sort.nPunchXOddEven		= 0;	
	sort.nPunchYOddEven		= 0;
	sort.fFormatX 			= fFoldSheetCutWidth;
	sort.fFormatY 			= fFoldSheetCutHeight;
	sort.fOffsetX 			= 0.0f;
	sort.fOffsetY 			= 0.0f;

	sort.fIndentRows	  	= 0.0f;
	sort.fIndentCols   		= 0.0f;
	sort.bRotateRows   		= FALSE;
	sort.bRotateCols   		= FALSE;
	sort.nIndentRowsOddEven = 0;
	sort.nIndentColsOddEven = 0;
	sort.nRotateRowsOddEven = 0;
	sort.nRotateColsOddEven = 0;

	if ( (nFoldSheetGrain != GRAIN_EITHER) && (nFoldSheetGrain != nPaperGrain) )
	{
		float fTemp = sort.fFormatX; sort.fFormatX = sort.fFormatY; sort.fFormatY = fTemp;
		int	  nTemp = sort.nPunchX;	 sort.nPunchX  = sort.nPunchY;	sort.nPunchY  = nTemp;
			  fTemp = sort.fOffsetX; sort.fOffsetX = sort.fOffsetY; sort.fOffsetY = fTemp;
	}
	sort.nCopies   = (rFoldSheet.GetQuantityPlanned() <= 0) ? 1 : rFoldSheet.GetQuantityPlanned();
	sort.pivot	   = (nFoldSheetGrain == GRAIN_EITHER) ? 1 : 0;
	sort.nPriority = 0;

	return sort;
}

SHEETOPT_SORT CKimOptimizer::CopyFlatToSort(CFlatProduct& rFlatProduct, float fSheetWidth, float fSheetHeight, int nPaperGrain)
{
	SHEETOPT_SORT sort;

	CPunchingTool punchingTool;
	punchingTool.Load(rFlatProduct.m_strPunchTool);
	punchingTool.AssignID();

	if ( ! punchingTool.IsNull())
	{
		sort.nPunchID 			= punchingTool.m_nToolID + 1;
		sort.nPunchX  			= punchingTool.m_nPunchesX;
		sort.nPunchY  			= punchingTool.m_nPunchesY;
		sort.nPunchXOddEven 	= (punchingTool.m_nNXOptions >= 2) ? ( (punchingTool.m_nOffsetRowsOddEven == 0) ? 1 : 2) : 0;	// NXOptions: 0 = nothing, 1 = variable; 2 = alternate; 2 = variable+alternate
		sort.nPunchYOddEven 	= (punchingTool.m_nNYOptions >= 2) ? ( (punchingTool.m_nOffsetColsOddEven == 0) ? 1 : 2) : 0;
		sort.fFormatX 			= punchingTool.m_fStepX;
		sort.fFormatY 			= punchingTool.m_fStepY;
		sort.fOffsetX 			= ((rFlatProduct.m_fTrimSizeWidth  + punchingTool.m_fGutterX * 2.0f) - punchingTool.m_fStepX) / 2.0f;
		sort.fOffsetY 			= ((rFlatProduct.m_fTrimSizeHeight + punchingTool.m_fGutterY * 2.0f) - punchingTool.m_fStepY) / 2.0f;

		sort.fIndentRows	  	= punchingTool.m_fOffsetRows;
		sort.fIndentCols   		= punchingTool.m_fOffsetCols;
		sort.bRotateRows   		= (punchingTool.m_bRotateRows) ? true : false;
		sort.bRotateCols   		= (punchingTool.m_bRotateCols) ? true : false;
		sort.nIndentRowsOddEven = punchingTool.m_nOffsetRowsOddEven;
		sort.nIndentColsOddEven = punchingTool.m_nOffsetColsOddEven;
		sort.nRotateRowsOddEven = punchingTool.m_nRotateRowsOddEven;
		sort.nRotateColsOddEven = punchingTool.m_nRotateColsOddEven;
	}
	else
	{
		sort.nPunchID 			= 0;
		sort.nPunchX  			= 0;
		sort.nPunchY  			= 0;
		sort.nPunchXOddEven		= 0;	
		sort.nPunchYOddEven		= 0;
		sort.fFormatX 			= rFlatProduct.m_fTrimSizeWidth  + rFlatProduct.m_fLeftTrim   + rFlatProduct.m_fRightTrim;
		sort.fFormatY 			= rFlatProduct.m_fTrimSizeHeight + rFlatProduct.m_fBottomTrim + rFlatProduct.m_fTopTrim;
		sort.fOffsetX 			= 0.0f;
		sort.fOffsetY 			= 0.0f;

		sort.fIndentRows	  	= 0.0f;
		sort.fIndentCols   		= 0.0f;
		sort.bRotateRows   		= FALSE;
		sort.bRotateCols   		= FALSE;
		sort.nIndentRowsOddEven = 0;
		sort.nIndentColsOddEven = 0;
		sort.nRotateRowsOddEven = 0;
		sort.nRotateColsOddEven = 0;

		if ( ! ObjectFitsToSheet(sort.fFormatX, sort.fFormatY, fSheetWidth, fSheetHeight, rFlatProduct.m_nPaperGrain, nPaperGrain))
		{
			sort.nCopies = 0;
			return sort;
		}
	}

	if ( (rFlatProduct.m_nPaperGrain != GRAIN_EITHER) && (rFlatProduct.m_nPaperGrain != nPaperGrain) )
	{
		float fTemp = sort.fFormatX; sort.fFormatX = sort.fFormatY; sort.fFormatY = fTemp;
		int	  nTemp = sort.nPunchX;	 sort.nPunchX  = sort.nPunchY;	sort.nPunchY  = nTemp;
			  fTemp = sort.fOffsetX; sort.fOffsetX = sort.fOffsetY; sort.fOffsetY = fTemp;
	}

	sort.nCopies   = (rFlatProduct.m_nDesiredQuantity <= 0) ? 1 : rFlatProduct.m_nDesiredQuantity;
	sort.pivot	   = (rFlatProduct.m_nPaperGrain == GRAIN_EITHER) ? 1 : 0;
	sort.nPriority = rFlatProduct.m_nPriority;

	return sort;
}

BOOL CKimOptimizer::ObjectFitsToSheet(float fObjectWidth, float fObjectHeight, float fSheetWidth, float fSheetHeight, int nObjectGrain, int nSheetGrain)
{
	if ( (nObjectGrain != GRAIN_EITHER) && (nObjectGrain != nSheetGrain) )
	{
		float fTemp = fObjectWidth; fObjectWidth = fObjectHeight; fObjectHeight = fTemp;
	}

	if ( (fObjectWidth > fSheetWidth) || (fObjectHeight > fSheetHeight) )
		return FALSE;
	else
		return TRUE;
}

int CKimOptimizer::preCutReady(bool bReady, int /*nStatus*/, int nProgress)
{
	g_bReady = bReady;
	if (bReady)
		return 1;

	return 1;
}
