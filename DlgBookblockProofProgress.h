#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CDlgBookblockProofProgress-Dialogfeld

class CDlgBookblockProofProgress : public CDialog
{
	DECLARE_DYNAMIC(CDlgBookblockProofProgress)

public:
	CDlgBookblockProofProgress(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgBookblockProofProgress();

// Dialogfelddaten
	enum { IDD = IDD_BOOKBLOCK_PROOF_PROGRESS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CProgressCtrl m_progressBar;
	CString m_strProgressText;
	CStatic m_progressStatic;
};
