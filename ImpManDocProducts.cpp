// ImpManDocProducts.cpp : implementation of the CImpManDoc class
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgSelectNewMark.h"
#include "DlgMarkSet.h"
#include "PaperDefsSelection.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "PageListView.h"
#include "PrintSheetPlanningView.h"
#include "GraphicComponent.h"
#include "PrintComponentsView.h"
#include "PrintComponentsDetailView.h"



/////////////////////////////////////////////////////////////////////////////
// CBoundProduct ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CBoundProduct, CObject, VERSIONABLE_SCHEMA | 5)

CBoundProduct::CBoundProduct()
{
	m_pParentList = NULL;
	m_strProductID		= _T("");
	m_strProductName	= _T("");
	m_nProductType		= 0;
	m_strComment		= _T("");
	m_nNumPages			= 0;
	m_fTrimSizeWidth	= 0.0f;
	m_fTrimSizeHeight	= 0.0f;
	m_strFormatName		= _T("");
	m_nPageOrientation	= 0;
	m_strBindingComment = _T("");
	m_nDesiredQuantity	= 0;
	m_nRealQuantity		= 0;
}

CBoundProduct::CBoundProduct(const CBoundProduct& rBoundProduct)
{
	m_pParentList = NULL;

	*this = rBoundProduct;
}

void CBoundProduct::Copy(const CBoundProduct& rBoundProduct)
{
	m_strProductID			= rBoundProduct.m_strProductID;
	m_strProductName		= rBoundProduct.m_strProductName;
	m_nProductType			= rBoundProduct.m_nProductType;
	m_strComment			= rBoundProduct.m_strComment;
	m_nNumPages				= rBoundProduct.m_nNumPages;
	m_fTrimSizeWidth		= rBoundProduct.m_fTrimSizeWidth;
	m_fTrimSizeHeight		= rBoundProduct.m_fTrimSizeHeight;
	m_strFormatName			= rBoundProduct.m_strFormatName;
	m_nPageOrientation		= rBoundProduct.m_nPageOrientation;
	m_strBindingComment		= rBoundProduct.m_strBindingComment;
	m_nDesiredQuantity		= rBoundProduct.m_nDesiredQuantity;
	m_bindingParams			= rBoundProduct.m_bindingParams;
	m_parts.Copy(rBoundProduct.m_parts);
}

const CBoundProduct& CBoundProduct::operator=(const CBoundProduct& rBoundProduct)
{
	Copy(rBoundProduct);

	return *this;
}

int CBoundProduct::GetIndex()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	int nIndex = 0;
	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		if (&pDoc->m_boundProducts.GetNext(pos) == this)
			return nIndex;
		nIndex++;
	}
	return -1;
}

CBoundProductPart& CBoundProduct::GetPart(int nLocalProductPartIndex)
{
	static private CBoundProductPart nullBoundProductPart;

	if ( (nLocalProductPartIndex < 0) || (nLocalProductPartIndex >= m_parts.GetSize()) )
		return nullBoundProductPart;

	return m_parts[nLocalProductPartIndex];
}

CBoundProductPart& CBoundProduct::FindPart(CString& strPartName)
{
	static CBoundProductPart nullBoundProductPart;;
	nullBoundProductPart = CBoundProductPart();

	for (int i = 0; i < m_parts.GetSize(); i++)
	{
		if (m_parts[i].m_strName.CompareNoCase(strPartName) == 0)
			return m_parts[i];
	}

	return nullBoundProductPart;
}

CBoundProductPart& CBoundProduct::JDFAddProductPart(CString strJDFNodeID, CString strJDFProductType, CString strName, int nNumPages, float fPageWidth, float fPageHeight, CString strFormatName, CBoundProduct& rBoundProduct)
{
	static private CBoundProductPart nullBoundProductPart;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullBoundProductPart;

	///// add product part
	CBoundProductPart boundProductPart;
	boundProductPart.m_strName			= strName;
	boundProductPart.m_nNumPages		= nNumPages;
	boundProductPart.m_fPageWidth		= fPageWidth;
	boundProductPart.m_fPageHeight		= fPageHeight;
	boundProductPart.m_strFormatName	= strFormatName;
	boundProductPart.SetID(strJDFNodeID);
	CNumbering numbering;
	numbering.m_nRangeFrom			= 1;	
	numbering.m_nRangeTo			= nNumPages;
	numbering.m_nStartNum			= 1;
	numbering.m_strNumberingStyle	= _T("$n");
	numbering.m_bReverse			= FALSE;
	boundProductPart.m_numberingList.Add(numbering);

	boundProductPart.m_nSubType = CBoundProductPart::Body;
	if (strJDFProductType == _T("Cover"))
	{
		boundProductPart.m_nSubType = CBoundProductPart::Cover;
		boundProductPart.m_strName.LoadString(IDS_COVER_NAME);	// needed by ArrangeCover() to identify. Original name is lost. Needs to be changed in future
	}
	else
	if (strJDFProductType == _T("Body"))
		boundProductPart.m_nSubType = CBoundProductPart::Body;
	else
	if (strJDFProductType == _T("Insert"))
		boundProductPart.m_nSubType = CBoundProductPart::Insert;
	else
	{
		CString string = strName;
		string.MakeUpper(); 
		if (string.Find(_T("UMSCHLAG")) >= 0)
		{
			boundProductPart.m_nSubType = CBoundProductPart::Cover;
			boundProductPart.m_strName.LoadString(IDS_COVER_NAME);	// needed by ArrangeCover() to identify. Original name is lost. Needs to be changed in future
		}
	}

	rBoundProduct.m_parts.Add(boundProductPart);

	int nProductPartIndex = rBoundProduct.m_parts.GetSize() - 1;
	CBoundProductPart& rNewBoundProductPart = rBoundProduct.m_parts.GetAt(nProductPartIndex);

	pDoc->m_PageTemplateList.ShiftProductPartIndices(nProductPartIndex);
	pDoc->m_Bookblock.ShiftProductPartIndices(nProductPartIndex);
	pDoc->m_PageTemplateList.InsertProductPartPages(boundProductPart.m_nNumPages, rBoundProduct.GetIndex(), nProductPartIndex);

	return rNewBoundProductPart;
}

int	CBoundProduct::GetNumPages()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return m_nNumPages;

	m_nNumPages = 0;
	for (int i = 0; i < m_parts.GetSize(); i++)
		m_nNumPages += m_parts[i].m_nNumPages;

	return m_nNumPages; 
}

// helper function for CBoundProduct elements
template <> void AFXAPI SerializeElements <CBoundProduct> (CArchive& ar, CBoundProduct* pBoundProduct, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CBoundProduct));

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pBoundProduct->GetProductID();
			ar << pBoundProduct->m_strProductName;
			ar << pBoundProduct->m_nProductType;
			ar << pBoundProduct->m_strComment;
			ar << pBoundProduct->m_nNumPages;
			ar << pBoundProduct->m_fTrimSizeWidth;
			ar << pBoundProduct->m_fTrimSizeHeight;
			ar << pBoundProduct->m_strFormatName;
			ar << pBoundProduct->m_nPageOrientation;
			ar << pBoundProduct->m_strBindingComment;
			ar << pBoundProduct->GetDesiredQuantity();
			pBoundProduct->m_bindingParams.Serialize(ar);
			pBoundProduct->m_parts.Serialize(ar);
		}
		else
		{
			long			 nTemp;
			CString			 strTemp;
			CTrimmingProcess trimmingParamsTemp;
			UINT nVersion = ar.GetObjectSchema();
			switch (nVersion)
			{
			case 1: ar >> pBoundProduct->m_strProductName;
					ar >> pBoundProduct->m_strComment;
					ar >> pBoundProduct->m_nNumPages;
					ar >> pBoundProduct->m_fTrimSizeWidth;
					ar >> pBoundProduct->m_fTrimSizeHeight;
					ar >> pBoundProduct->m_strFormatName;
					ar >> pBoundProduct->m_nPageOrientation;
					ar >> pBoundProduct->m_strBindingComment;
					ar >> nTemp;
					pBoundProduct->SetDesiredQuantity(nTemp);
					{
						CBindingDefs bindingDefs;
						SerializeElements(ar, &bindingDefs, 1);
						pBoundProduct->m_bindingParams.m_bindingDefs = bindingDefs;
					}
					pBoundProduct->m_parts.Serialize(ar);
					break;
			case 2: ar >> pBoundProduct->m_strProductName;
					ar >> pBoundProduct->m_strComment;
					ar >> pBoundProduct->m_nNumPages;
					ar >> pBoundProduct->m_fTrimSizeWidth;
					ar >> pBoundProduct->m_fTrimSizeHeight;
					ar >> pBoundProduct->m_strFormatName;
					ar >> pBoundProduct->m_nPageOrientation;
					ar >> pBoundProduct->m_strBindingComment;
					ar >> nTemp;
					pBoundProduct->SetDesiredQuantity(nTemp);
					pBoundProduct->m_bindingParams.Serialize(ar);
					trimmingParamsTemp.Serialize(ar);
					pBoundProduct->m_parts.Serialize(ar);
					break;
			case 3: ar >> strTemp;	pBoundProduct->SetProductID(strTemp);
					ar >> pBoundProduct->m_strProductName;
					ar >> pBoundProduct->m_strComment;
					ar >> pBoundProduct->m_nNumPages;
					ar >> pBoundProduct->m_fTrimSizeWidth;
					ar >> pBoundProduct->m_fTrimSizeHeight;
					ar >> pBoundProduct->m_strFormatName;
					ar >> pBoundProduct->m_nPageOrientation;
					ar >> pBoundProduct->m_strBindingComment;
					ar >> nTemp;
					pBoundProduct->SetDesiredQuantity(nTemp);
					pBoundProduct->m_bindingParams.Serialize(ar);
					trimmingParamsTemp.Serialize(ar);
					pBoundProduct->m_parts.Serialize(ar);
					break;
			case 4: ar >> strTemp;	pBoundProduct->SetProductID(strTemp);
					ar >> pBoundProduct->m_strProductName;
					ar >> pBoundProduct->m_nProductType;
					ar >> pBoundProduct->m_strComment;
					ar >> pBoundProduct->m_nNumPages;
					ar >> pBoundProduct->m_fTrimSizeWidth;
					ar >> pBoundProduct->m_fTrimSizeHeight;
					ar >> pBoundProduct->m_strFormatName;
					ar >> pBoundProduct->m_nPageOrientation;
					ar >> pBoundProduct->m_strBindingComment;
					ar >> nTemp;
					pBoundProduct->SetDesiredQuantity(nTemp);
					pBoundProduct->m_bindingParams.Serialize(ar);
					trimmingParamsTemp.Serialize(ar);
					pBoundProduct->m_parts.Serialize(ar);
					break;
			case 5: ar >> strTemp;	pBoundProduct->SetProductID(strTemp);
					ar >> pBoundProduct->m_strProductName;
					ar >> pBoundProduct->m_nProductType;
					ar >> pBoundProduct->m_strComment;
					ar >> pBoundProduct->m_nNumPages;
					ar >> pBoundProduct->m_fTrimSizeWidth;
					ar >> pBoundProduct->m_fTrimSizeHeight;
					ar >> pBoundProduct->m_strFormatName;
					ar >> pBoundProduct->m_nPageOrientation;
					ar >> pBoundProduct->m_strBindingComment;
					ar >> nTemp;
					pBoundProduct->SetDesiredQuantity(nTemp);
					pBoundProduct->m_bindingParams.Serialize(ar);
					pBoundProduct->m_parts.Serialize(ar);
					break;
			}
			if (nVersion < 5)
			{
				for (int i = 0; i < pBoundProduct->m_parts.GetSize(); i++)
				{
					pBoundProduct->m_parts[i].m_trimmingParams = trimmingParamsTemp;
				}
			}
		}

		pBoundProduct++;
	}
}

void CBoundProduct::ChangePageFormat(CString strFormatName, float fWidth, float fHeight, int nPageOrientation, int nProductPartIndex)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		rLayout.m_nFlag = 0;
	}

	for (int i = 0; i < m_parts.GetSize(); i++)
	{
		if (nProductPartIndex >= 0)
			if (nProductPartIndex != i)
				continue;

		CBoundProductPart& rBoundProductPart = m_parts.GetAt(i);
		rBoundProductPart.m_strFormatName = strFormatName;
		rBoundProductPart.m_fPageWidth	  = fWidth;
		rBoundProductPart.m_fPageHeight	  = fHeight;

		POSITION pos = pDoc->m_PageTemplateList.GetHeadPosition(nProductPartIndex);
		while (pos)
		{
			CPageTemplate& rTemplate = pDoc->m_PageTemplateList.GetNext(pos, nProductPartIndex);

			int			   nIndex		 = 0;
			CLayoutObject* pLayoutObject = rTemplate.GetLayoutObject(nIndex);
			CPrintSheet*   pPrintSheet   = rTemplate.GetPrintSheet(nIndex);
			while (pLayoutObject)
			{
				pLayoutObject->m_strFormatName	  = strFormatName;
				pLayoutObject->m_nObjectOrientation = (unsigned char)nPageOrientation;
				BOOL bRotate = ( (pLayoutObject->m_nHeadPosition == LEFT) || (pLayoutObject->m_nHeadPosition == RIGHT) ) ? TRUE : FALSE;
				pLayoutObject->ModifySize(fWidth, fHeight, pPrintSheet, bRotate);

				//if (pLayoutObject->GetLayoutSide())
				//	pLayoutObject->GetLayoutSide()->Invalidate();

				if (pLayoutObject->GetLayout())
					pLayoutObject->GetLayout()->m_nFlag = 1;

				pLayoutObject = rTemplate.GetLayoutObject(++nIndex);
				pPrintSheet   = rTemplate.GetPrintSheet(nIndex);
			}
		}
	}

	pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		if (rLayout.m_nFlag)
		{
			CPrintingProfile& rProfile = rLayout.GetPrintingProfile();
			rLayout.m_FrontSide.PositionLayoutSide(rProfile.m_press.m_pressDevice.m_fGripper, &rProfile.m_press.m_pressDevice);
			rLayout.m_BackSide.PositionLayoutSide(rProfile.m_press.m_pressDevice.m_fGripper, &rProfile.m_press.m_pressDevice);
			rLayout.AnalyzeMarks();
		}
	}
}

void CBoundProduct::ReorganizeReferences(int nIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.m_nProductPartIndex > nIndex)
			rFoldSheet.m_nProductPartIndex--;
	}

	pos = pDoc->m_PageTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.CList::GetNext(pos);
		if (rPageTemplate.m_nProductPartIndex > nIndex)
			rPageTemplate.m_nProductPartIndex--;
	}
}

BOOL CBoundProduct::HasCover()
{
	return (GetCoverProductPartIndex() >= 0) ? TRUE : FALSE;
}

int	CBoundProduct::GetCoverProductPartIndex()
{
	for (int i = 0; i < m_parts.GetSize(); i++)
	{
		if (m_parts.GetAt(i).m_strName.CompareNoCase(theApp.m_strCoverName) == 0)
		{
			return TransformLocalToGlobalPartIndex(i);
		}
	}
	return -1;
}

int	CBoundProduct::GetGlobalProductPartIndex(const CString& strID)
{
	for (int i = 0; i < m_parts.GetSize(); i++)
		if (m_parts.GetAt(i).GetID() == strID)
			return TransformLocalToGlobalPartIndex(i);
	return -1;
}

int	CBoundProduct::TransformLocalToGlobalPartIndex(int nLocalProductPartIndex)
{
	return GetPart(nLocalProductPartIndex).GetIndex();
}

CRect CBoundProduct::DrawInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground)
{
	//if (pDisplayList)
	//{
	//	CRect rcDI = rcFrame; 
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, BoundProduct), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//		if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
	//		{
	//			CRect rcFeedback = pDI->m_BoundingRect; //rcFeedback.right -= 1; rcFeedback.bottom -= 1;
	//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
	//			//m_DisplayList.InvalidateItem(pDI);
	//			Graphics graphics(pDC->m_hDC);
	//			Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
	//			COLORREF crHighlightColor = RGB(255,195,15);
	//			SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
	//			graphics.FillRectangle(&br, rc);
	//			graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
	//		}
	//	}
	//}

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);
	
	pDC->SaveDC();
	CRect rcClip = rcFrame; rcClip.bottom -= nTextHeight;
	pDC->IntersectClipRect(rcClip);

	CFont font;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_CAPTION);	
	//pDC->SetTextColor(RGB(40,120,30));

	rcFrame.left += 5; rcFrame.top += 5;

	CRect rcTextMeasure;
	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	DrawTextMeasured(pDC, m_strProductName, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	pDC->SelectObject(&font);

	font.DeleteObject();

	rcBox.right += 5; rcBox.bottom += 5;

	pDC->RestoreDC(-1);

	return rcBox;
}

CRect CBoundProduct::DrawThumbnail(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CRect rcImg = rcFrame; rcImg.top += 2; 
	CImage img;
	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_PRODUCT_BOOK));				
	if ( ! img.IsNull())
		img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
	rcBox.UnionRect(rcBox, rcImg);

	CString strOut;
	pDC->SelectObject(&font);
	pDC->SetTextColor(GUICOLOR_CAPTION);	
	//pDC->SetTextColor(RGB(40,120,30));
	//strOut.Format(_T(" %d "), m_nNumPages);
	//CRect rcPages = rcImg; rcPages.top = rcImg.CenterPoint().y - 6; rcPages.bottom = rcImg.CenterPoint().y + 8; rcPages.OffsetRect(rcImg.Width() - 4, 0); rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
	//rcPages.InflateRect(1, 1);
	//CBrush brush(WHITE);
	//CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
	//CBrush* pOldBrush = pDC->SelectObject(&brush);
	//CPen*	pOldPen	  = pDC->SelectObject(&pen);
	//pDC->RoundRect(rcPages, CPoint(8, 8));
	//pDC->SelectObject(pOldBrush);
	//pDC->SelectObject(pOldPen);
	CRect rcText = rcFrame; rcText.left += 38;
	DrawTextMeasured(pDC, m_strProductName, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	//if (pDisplayList)
	//{
	//	CRect rcDI = rcImg; rcDI.bottom = rcDI.top + 2*nTextHeight; 
	//	CProductDataView* pView = (CProductDataView*)pDisplayList->m_pParent;
	//	if (pView)
	//	{
	//		CRect rcClient;
	//		pView->GetClientRect(rcClient);
	//		rcDI.right = rcClient.right - 5;
	//	}
	//	rcDI.left -= 10; rcDI.top -= 3; 
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)&rFlatProduct, DISPLAY_ITEM_REGISTRY(CProductsView, BoundProductPart), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//		pDI->SetMouseOverFeedback();
	//}

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CFoldSheet* CBoundProduct::GetFoldSheetByID(const CString& strID)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int nProductIndex = GetIndex();

	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if ( ! rFoldSheet.IsPartOfBoundProduct(-nProductIndex - 1))
			continue;

		if (rFoldSheet.GetID().CompareNoCase(strID) == 0)
			return &rFoldSheet;
	}

	return NULL;
}

void CBoundProduct::AssignProductionProfilePool(CProductionProfile& rProductionProfile)
{
	CBindingDefs bindingDefs = m_bindingParams.m_bindingDefs;	// keep bindingdefs
	m_bindingParams = (rProductionProfile.ProcessIsActive(CProductionProfile::Binding)) ? rProductionProfile.m_postpress.m_binding : CBindingProcess();
	m_bindingParams.m_bindingDefs = bindingDefs;
}

void CBoundProduct::AssignDuplicateName()
{
	if (m_strProductName.IsEmpty())
	{
		CString strDefaultName; strDefaultName.LoadString(IDS_PRODUCT); 
		m_strProductName = strDefaultName;
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strNewName;
	int nNumProductsTotal = pDoc->m_boundProducts.GetCount() + pDoc->m_flatProducts.GetCount();
	for (int i = 1; i <= nNumProductsTotal; i++) 
	{
		BOOL bOccupied = FALSE;
		POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
		while (pos)
		{
			CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
			if (rBoundProduct.GetNumDuplicateName(m_strProductName) == i)
			{
				bOccupied = TRUE;
				break;
			}
		}
		if ( ! bOccupied)
		{
			pos = pDoc->m_flatProducts.GetHeadPosition();
			while (pos)
			{
				CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);
				if (rFlatProduct.GetNumDuplicateName(m_strProductName) == i)
				{
					bOccupied = TRUE;
					break;
				}
			}
			if ( ! bOccupied)
			{
				strNewName.Format(_T("%s-%d"), m_strProductName, i);
				m_strProductName = strNewName;
				return;
			}
		}
	}

	strNewName.Format(_T("%s-%d"), m_strProductName, pDoc->m_boundProducts.GetCount() + 1);
	m_strProductName = strNewName;
}

int CBoundProduct::GetNumDuplicateName(CString strName)
{
	CString  string = CString(strName) + "-";
	const TCHAR* poi = _tcsstr((LPCTSTR)m_strProductName, (LPCTSTR)string);
	if (poi)
	{
		poi = _tcschr(poi, '-');
		poi++;
		if (isdigit(*poi))	
			return _ttoi(poi);
	}

	return -1;
}

BOOL CBoundProduct::PageSourceIsAssigned(CString strDocumentFullpath)
{
	if (strDocumentFullpath.IsEmpty())
		return FALSE;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	int		 nProductIndex = GetIndex();
	POSITION pos		   = pDoc->m_PageTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.CList::GetNext(pos);
		if (rPageTemplate.m_nProductIndex != nProductIndex)
			continue;

		for (int i = 0; i < rPageTemplate.m_ColorSeparations.GetSize(); i++)
		{
			POSITION posPageSource = pDoc->m_PageSourceList.FindIndex(rPageTemplate.m_ColorSeparations[i].m_nPageSourceIndex);
			if (posPageSource)
			{
				CPageSource& rCurPageSource = pDoc->m_PageSourceList.GetAt(posPageSource);
				if (rCurPageSource.m_strDocumentFullpath.CompareNoCase(strDocumentFullpath) == 0)
					return TRUE;
			}
		}
	}
	return FALSE;
}



/////////////////////////////////////////////////////////////////////////////
// CBoundProductList ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CBoundProductList, CObject, VERSIONABLE_SCHEMA | 1)

CBoundProductList::CBoundProductList()
{
}

void CBoundProductList::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CBoundProductList));

	if (ar.IsStoring())
	{
		ar.WriteCount(GetCount());
		for (CNode* pNode = m_pNodeHead; pNode != NULL; pNode = pNode->pNext)
		{
			SerializeElements(ar, &pNode->data, 1);
		}
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();
		switch (nVersion)
		{
		case 1: break;
		}

		DWORD_PTR nNewCount = ar.ReadCount();
		while (nNewCount--)
		{
			CBoundProduct newBoundProduct;
			SerializeElements(ar, &newBoundProduct, 1);
			AddTail(newBoundProduct);
		}
	}
}

POSITION CBoundProductList::AddTail(CBoundProduct& rBoundProduct)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rCurBoundProduct = GetNext(pos);
		if (rCurBoundProduct.m_strProductName.CompareNoCase(rBoundProduct.m_strProductName) == 0)
		{
			rBoundProduct.AssignDuplicateName();
			break;
		}
	}

	pos = CList <CBoundProduct, CBoundProduct&>::AddTail(rBoundProduct);
	//CList <CBoundProduct, CBoundProduct&>::GetAt(pos).m_pParentList = this;
	//CList <CBoundProduct, CBoundProduct&>::GetAt(pos).m_nID			= nNewID;

	return pos;
}

CBoundProduct& CBoundProductList::GetProduct(CString strProductID)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		if (rProduct.GetProductID() == strProductID)
			return rProduct;
	}

	return m_nullProduct;
}

CBoundProduct& CBoundProductList::GetProduct(int nIndex) 
{
	int		 nProductIndex = 0;
	POSITION pos		   = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		if (nProductIndex == nIndex)
			return rProduct;
		nProductIndex++;
	}

	return m_nullProduct;
}

CBoundProduct& CBoundProductList::GetFirstProduct()
{
	return GetHead();
}

CBoundProduct& CBoundProductList::GetNextProduct(CBoundProduct& rCurrentProduct)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		if (&rProduct == &rCurrentProduct)
			break;
	}

	return (pos) ? GetAt(pos) : m_nullProduct;
}

CBoundProductPart& CBoundProductList::GetProductPart(int nProductPartIndex)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
		{
			if (nIndex == nProductPartIndex)
				return rProduct.m_parts[i];
			nIndex++;
		}
	}

	return m_nullBoundProductPart;
}

CBoundProductPart* CBoundProductList::GetFirstPartOfBoundProduct(int nProductPartIndex)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if (nProductPartIndex >= 0)
		return &pDoc->GetBoundProductPart(nProductPartIndex);
	else
	{
		CBoundProduct& rBoundProduct = pDoc->GetBoundProduct(-nProductPartIndex - 1);	// nProductPartIndex == -1 means 1st bound product (index 0), nProductPartIndex == -2 means 2nd bound product (index 1), ...
		return &rBoundProduct.GetPart(0);
	}
	return NULL;
}

CBoundProductPart* CBoundProductList::GetNextPartOfBoundProduct(CBoundProductPart* pProductPart, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if (nProductPartIndex >= 0)		// there is only one 
		return NULL;
	else
	{
		CBoundProduct& rBoundProduct = pDoc->GetBoundProduct(-nProductPartIndex - 1);	// nProductPartIndex == -1 means 1st bound product (index 0), nProductPartIndex == -2 means 2nd bound product (index 1), ...
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			if (&rBoundProduct.m_parts[i] == pProductPart)
			{
				i++;
				if (i < rBoundProduct.m_parts.GetSize())
					return &rBoundProduct.m_parts[i];
			}
		}
	}
	return NULL;
}

int	CBoundProductList::GetNumPartsTotal()
{
	int		 nNumParts = 0;
	POSITION pos	   = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		nNumParts += rProduct.m_parts.GetSize();
	}
	return nNumParts;
}

CBoundProductPart& 	CBoundProductList::GetPartTotal(int nProductPartIndex)
{
	static private CBoundProductPart nullBoundProductPart;

	int		 nIndex = 0;
	POSITION pos    = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
		{
			if (nProductPartIndex == nIndex)
				return rProduct.m_parts[i];
			nIndex++;
		}
	}
	return nullBoundProductPart;
}

int	CBoundProductList::TransformPartToProductIndex(int nProductPartIndex)
{
	if (nProductPartIndex < 0)
		return abs(nProductPartIndex) - 1;
	else
		return GetProductPart(nProductPartIndex).GetBoundProduct().GetIndex();
}

int	CBoundProductList::TransformGlobalToLocalPartIndex(int nProductPartIndex)
{
	int		 nIndex = 0;
	POSITION pos    = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
		{
			if (nProductPartIndex == nIndex)
				return i;
			nIndex++;
		}
	}
	return nProductPartIndex;
}

void CBoundProductList::RemoveProductPart(CBoundProductPart& rProductPart)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CBoundProduct& rProduct = rProductPart.GetBoundProduct();

	int nProductPartIndex = rProductPart.GetIndex();

	pDoc->m_Bookblock.RemoveFoldSheets(nProductPartIndex);
	pDoc->m_Bookblock.Reorganize();
	pDoc->m_PageTemplateList.RemoveProductGroupTemplates(nProductPartIndex);
	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

	pDoc->m_MarkSourceList.RemoveUnusedSources();

	rProduct.m_parts.RemoveAt(TransformGlobalToLocalPartIndex(nProductPartIndex));
	rProduct.ReorganizeReferences(nProductPartIndex);
	if (rProduct.m_parts.GetSize() <= 0)
	{
		POSITION pos = pDoc->m_boundProducts.FindIndex(rProduct.GetIndex());
		if (pos)
			pDoc->m_boundProducts.RemoveAt(pos);
	}
}

void CBoundProductList::UpdateQuantities()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = GetNext(pos);
		rBoundProduct.SetDesiredQuantity(0);
		rBoundProduct.SetRealQuantity(INT_MAX);
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			CBoundProductPart& rProductPart = rBoundProduct.m_parts[i];

			CFoldSheet* pFoldSheet = rProductPart.GetFirstFoldSheet();
			while (pFoldSheet)
			{
				pFoldSheet->CalcQuantityActual();
				pFoldSheet = rProductPart.GetNextFoldSheet(pFoldSheet);
			}

			rProductPart.CalcRealQuantity();
			rBoundProduct.SetDesiredQuantity(max(rBoundProduct.GetDesiredQuantity(), rProductPart.GetDesiredQuantity()));
			rBoundProduct.SetRealQuantity	(min(rBoundProduct.GetRealQuantity(),	 rProductPart.GetRealQuantity()));

			pFoldSheet = rProductPart.GetFirstFoldSheet();
			while (pFoldSheet)
			{
				pFoldSheet->SetQuantityPlanned(rProductPart.GetDesiredQuantity());
				pFoldSheet = rProductPart.GetNextFoldSheet(pFoldSheet);
			}
		}
		if (rBoundProduct.GetRealQuantity() == INT_MAX)
			rBoundProduct.SetRealQuantity(0);
	}
}

void CBoundProductList::DeleteContents()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		//rProduct.m_bookBlock.m_FoldSheetList.RemoveAll();
		//rProduct.m_bookBlock.m_DescriptionList.RemoveAll();
	}
}

void CBoundProductList::ResetProductPartDisplayBoxes(BOOL bRemove)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		for (int i = 0; i < rProduct.m_parts.GetSize(); i++)
			rProduct.m_parts[i].ResetDisplayBoxes(bRemove);
	}
}

CBoundProduct& CBoundProductList::FindProductByID(const CString& strID)
{
	static private CBoundProduct nullBoundProduct;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		if (rProduct.GetProductID() == strID)
			return rProduct;
	}

	return nullBoundProduct;
}

CBoundProduct& CBoundProductList::FindProduct(const CString& strProductName)
{
	static private CBoundProduct nullBoundProduct;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rProduct = GetNext(pos);
		if (rProduct.m_strProductName == strProductName)
			return rProduct;
	}

	return nullBoundProduct;
}

void CBoundProductList::GetProductInfo(CJobInfoExchange& rJobInfoExchange)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);

		for (int nProductPartIndex = 0; nProductPartIndex < rBoundProduct.m_parts.GetSize(); nProductPartIndex++)
		{
			CBoundProductPart& rProductPart = rBoundProduct.m_parts[nProductPartIndex];
			CProductInfo productInfo;
			productInfo.m_nType			= CProductInfo::Bound;
			productInfo.m_strName		= rBoundProduct.m_strProductName;
			productInfo.m_strPartName	= rProductPart.m_strName;
			productInfo.m_fWidth		= rProductPart.m_fPageWidth;
			productInfo.m_fHeight		= rProductPart.m_fPageHeight;
			productInfo.m_nNumPages		= rProductPart.m_nNumPages;

			rJobInfoExchange.m_productInfoList.AddTail(productInfo);
		}
	}
}

void CBoundProductList::NormalizeIDs()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = GetNext(pos);
		rBoundProduct.SetProductID(rBoundProduct.m_strProductName);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CBoundProductPart ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CBoundProductPart, CObject, VERSIONABLE_SCHEMA | 6)

CBoundProductPart::CBoundProductPart()
{
	m_strName			= _T("");
	m_nSubType			= Body;
	m_strName.Empty();
	m_nNumPages			= 0;
	m_numberingList.RemoveAll();
	m_fPageWidth		= 0.0f;
	m_fPageHeight		= 0.0f;
	m_strFormatName.Empty();
	m_nPageOrientation	= PORTRAIT;
	m_strPaper			= _T("");
	m_nPaperGrammage	= 0;
	m_fPaperVolume		= 0;
	m_nPaperType		= 0;
	m_nPaperGrain		= GRAIN_EITHER;
	m_nDesiredQuantity	= 0;
	m_nExtra			= 0;
	m_strComment.Empty();
}

CBoundProductPart::CBoundProductPart(const CBoundProductPart& rBoundProductPart)
{
	*this = rBoundProductPart;
}

const CBoundProductPart& CBoundProductPart::operator=(const CBoundProductPart& rBoundProductPart)
{
	//Copy(rBoundProductPart);
	m_strID					= rBoundProductPart.m_strID;
	m_strName				= rBoundProductPart.m_strName;
	m_nSubType				= rBoundProductPart.m_nSubType;
	m_nNumPages				= rBoundProductPart.m_nNumPages;
	m_numberingList.Copy(rBoundProductPart.m_numberingList);
	m_fPageWidth			= rBoundProductPart.m_fPageWidth;
	m_fPageHeight			= rBoundProductPart.m_fPageHeight;
	m_strFormatName			= rBoundProductPart.m_strFormatName;
	m_nPageOrientation		= rBoundProductPart.m_nPageOrientation;
	m_colorantList.Copy(rBoundProductPart.m_colorantList);
	m_strPaper				= rBoundProductPart.m_strPaper;
	m_nPaperGrammage		= rBoundProductPart.m_nPaperGrammage;
	m_fPaperVolume			= rBoundProductPart.m_fPaperVolume;
	m_nPaperType			= rBoundProductPart.m_nPaperType;
	m_nPaperGrain			= rBoundProductPart.m_nPaperGrain;
	m_nDesiredQuantity		= rBoundProductPart.m_nDesiredQuantity;
	m_nExtra				= rBoundProductPart.m_nExtra;
	m_strComment			= rBoundProductPart.m_strComment;
	m_foldingParams			= rBoundProductPart.m_foldingParams;
	m_trimmingParams		= rBoundProductPart.m_trimmingParams;

	return *this;
}

// helper function for CBoundProduct elements
template <> void AFXAPI SerializeElements <CBoundProductPart> (CArchive& ar, CBoundProductPart* pBoundProductPart, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CBoundProductPart));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			CString strID = pBoundProductPart->GetID();
			ar << strID;
			ar << pBoundProductPart->m_strName;
			ar << pBoundProductPart->m_nSubType;
			ar << pBoundProductPart->m_nNumPages;
			ar << pBoundProductPart->m_fPageWidth;
			ar << pBoundProductPart->m_fPageHeight;
			ar << pBoundProductPart->m_strFormatName;
			ar << pBoundProductPart->m_nPageOrientation;
			ar << pBoundProductPart->m_strPaper;
			ar << pBoundProductPart->m_nPaperGrammage;
			ar << pBoundProductPart->m_fPaperVolume;
			ar << pBoundProductPart->m_nPaperType;
			ar << pBoundProductPart->m_nPaperGrain;
			ar << pBoundProductPart->m_nDesiredQuantity;
			ar << pBoundProductPart->m_nExtra;
			ar << pBoundProductPart->m_strComment;
			pBoundProductPart->m_numberingList.Serialize(ar);
			pBoundProductPart->m_colorantList.Serialize(ar);
			pBoundProductPart->m_foldingParams.Serialize(ar);
			pBoundProductPart->m_trimmingParams.Serialize(ar);
		}
		else
		{
			CString strID;
			switch (nVersion)
			{
			case 1: ar >> pBoundProductPart->m_strName;
					ar >> pBoundProductPart->m_nSubType;
					ar >> pBoundProductPart->m_nNumPages;
					ar >> pBoundProductPart->m_fPageWidth;
					ar >> pBoundProductPart->m_fPageHeight;
					ar >> pBoundProductPart->m_strFormatName;
					ar >> pBoundProductPart->m_nPageOrientation;
					ar >> pBoundProductPart->m_strPaper;
					ar >> pBoundProductPart->m_nPaperGrammage;
					ar >> pBoundProductPart->m_fPaperVolume;
					ar >> pBoundProductPart->m_nDesiredQuantity;
					ar >> pBoundProductPart->m_nExtra;
					ar >> pBoundProductPart->m_strComment;
					ar >> strID;
					pBoundProductPart->m_numberingList.Serialize(ar);
					{
						CBindingDefs bindingDefs;
						bindingDefs.Serialize(ar);
					}
					pBoundProductPart->m_colorantList.Serialize(ar);
					break;
			case 2: ar >> pBoundProductPart->m_strName;
					ar >> pBoundProductPart->m_nSubType;
					ar >> pBoundProductPart->m_nNumPages;
					ar >> pBoundProductPart->m_fPageWidth;
					ar >> pBoundProductPart->m_fPageHeight;
					ar >> pBoundProductPart->m_strFormatName;
					ar >> pBoundProductPart->m_nPageOrientation;
					ar >> pBoundProductPart->m_strPaper;
					ar >> pBoundProductPart->m_nPaperGrammage;
					ar >> pBoundProductPart->m_fPaperVolume;
					ar >> pBoundProductPart->m_nDesiredQuantity;
					ar >> pBoundProductPart->m_nExtra;
					ar >> pBoundProductPart->m_strComment;
					ar >> strID;
					pBoundProductPart->m_numberingList.Serialize(ar);
					pBoundProductPart->m_colorantList.Serialize(ar);
					pBoundProductPart->m_foldingParams.Serialize(ar);
					break;
			case 3: ar >> pBoundProductPart->m_strName;
					ar >> pBoundProductPart->m_nSubType;
					ar >> pBoundProductPart->m_nNumPages;
					ar >> pBoundProductPart->m_fPageWidth;
					ar >> pBoundProductPart->m_fPageHeight;
					ar >> pBoundProductPart->m_strFormatName;
					ar >> pBoundProductPart->m_nPageOrientation;
					ar >> pBoundProductPart->m_strPaper;
					ar >> pBoundProductPart->m_nPaperGrammage;
					ar >> pBoundProductPart->m_fPaperVolume;
					ar >> pBoundProductPart->m_nPaperType;
					ar >> pBoundProductPart->m_nDesiredQuantity;
					ar >> pBoundProductPart->m_nExtra;
					ar >> pBoundProductPart->m_strComment;
					ar >> strID;
					pBoundProductPart->m_numberingList.Serialize(ar);
					pBoundProductPart->m_colorantList.Serialize(ar);
					pBoundProductPart->m_foldingParams.Serialize(ar);
					break;
			case 4: ar >> pBoundProductPart->m_strName;
					ar >> pBoundProductPart->m_nSubType;
					ar >> pBoundProductPart->m_nNumPages;
					ar >> pBoundProductPart->m_fPageWidth;
					ar >> pBoundProductPart->m_fPageHeight;
					ar >> pBoundProductPart->m_strFormatName;
					ar >> pBoundProductPart->m_nPageOrientation;
					ar >> pBoundProductPart->m_strPaper;
					ar >> pBoundProductPart->m_nPaperGrammage;
					ar >> pBoundProductPart->m_fPaperVolume;
					ar >> pBoundProductPart->m_nPaperType;
					ar >> pBoundProductPart->m_nDesiredQuantity;
					ar >> pBoundProductPart->m_nExtra;
					ar >> pBoundProductPart->m_strComment;
					ar >> strID;
					pBoundProductPart->m_numberingList.Serialize(ar);
					pBoundProductPart->m_colorantList.Serialize(ar);
					pBoundProductPart->m_foldingParams.Serialize(ar);
					pBoundProductPart->m_trimmingParams.Serialize(ar);
					break;
			case 5: ar >> pBoundProductPart->m_strName;
					ar >> pBoundProductPart->m_nSubType;
					ar >> pBoundProductPart->m_nNumPages;
					ar >> pBoundProductPart->m_fPageWidth;
					ar >> pBoundProductPart->m_fPageHeight;
					ar >> pBoundProductPart->m_strFormatName;
					ar >> pBoundProductPart->m_nPageOrientation;
					ar >> pBoundProductPart->m_strPaper;
					ar >> pBoundProductPart->m_nPaperGrammage;
					ar >> pBoundProductPart->m_fPaperVolume;
					ar >> pBoundProductPart->m_nPaperType;
					ar >> pBoundProductPart->m_nPaperGrain;
					ar >> pBoundProductPart->m_nDesiredQuantity;
					ar >> pBoundProductPart->m_nExtra;
					ar >> pBoundProductPart->m_strComment;
					ar >> strID;
					pBoundProductPart->m_numberingList.Serialize(ar);
					pBoundProductPart->m_colorantList.Serialize(ar);
					pBoundProductPart->m_foldingParams.Serialize(ar);
					pBoundProductPart->m_trimmingParams.Serialize(ar);
					break;
			case 6: ar >> strID;
					ar >> pBoundProductPart->m_strName;
					ar >> pBoundProductPart->m_nSubType;
					ar >> pBoundProductPart->m_nNumPages;
					ar >> pBoundProductPart->m_fPageWidth;
					ar >> pBoundProductPart->m_fPageHeight;
					ar >> pBoundProductPart->m_strFormatName;
					ar >> pBoundProductPart->m_nPageOrientation;
					ar >> pBoundProductPart->m_strPaper;
					ar >> pBoundProductPart->m_nPaperGrammage;
					ar >> pBoundProductPart->m_fPaperVolume;
					ar >> pBoundProductPart->m_nPaperType;
					ar >> pBoundProductPart->m_nPaperGrain;
					ar >> pBoundProductPart->m_nDesiredQuantity;
					ar >> pBoundProductPart->m_nExtra;
					ar >> pBoundProductPart->m_strComment;
					pBoundProductPart->m_numberingList.Serialize(ar);
					pBoundProductPart->m_colorantList.Serialize(ar);
					pBoundProductPart->m_foldingParams.Serialize(ar);
					pBoundProductPart->m_trimmingParams.Serialize(ar);
					break;
			}
		}

		pBoundProductPart++;
	}
}

int	CBoundProductPart::GetIndex()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	int		 nProductPartIndex = 0;
	POSITION pos			   = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			if (&rBoundProduct.m_parts[i] == this)
				return nProductPartIndex;
			nProductPartIndex++;
		}
	}

	return -1;
}

void CBoundProductPart::AddDisplayBox(CRect& rcBox) 
{ 
	for (int i = 0; i < m_dspi.rcDisplayBoxes.GetSize(); i++)
	{
		if (m_dspi.rcDisplayBoxes[i] == rcBox)
			return;
	}
	m_dspi.rcDisplayBoxes.Add(rcBox); 
}


CBoundProduct& CBoundProductPart::GetBoundProduct(int nThisProductPartIndex)
{
	static private CBoundProduct nullBoundProduct;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullBoundProduct;
	
	nThisProductPartIndex	= (nThisProductPartIndex == -1) ? GetIndex() : nThisProductPartIndex;

	int		 nProductPartIndex		= 0;
	POSITION pos					= pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			if (nProductPartIndex == nThisProductPartIndex)
				return rBoundProduct;
			nProductPartIndex++;
		}
	}

	return nullBoundProduct;
}

CFoldSheet*	CBoundProductPart::GetFirstFoldSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return NULL;
	return pDoc->m_Bookblock.GetFirstFoldSheet(GetIndex());
}

CFoldSheet*	CBoundProductPart::GetNextFoldSheet(CFoldSheet* pFoldSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return NULL;
	return pDoc->m_Bookblock.GetNextFoldSheet(pFoldSheet, GetIndex());
}

CPrintGroup& CBoundProductPart::GetFirstPrintGroup()
{
	static CPrintGroup nullPrintGroup;
	
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return nullPrintGroup;

	int nProductPartIndex = GetIndex();

	for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
	{
		CPrintGroup&  rPrintGroup  = pDoc->m_printGroupList[i];
		CPrintGroupComponent* pComponent = rPrintGroup.GetFirstBoundComponent();
		while (pComponent)
		{
			if (pComponent->m_nProductClassIndex == nProductPartIndex)
				return rPrintGroup;
			pComponent = rPrintGroup.GetNextBoundComponent(pComponent);
		}
	}	

	return nullPrintGroup;
}

CPrintGroup& CBoundProductPart::GetLastPrintGroup()
{
	static CPrintGroup nullPrintGroup;
	
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return nullPrintGroup;

	int nProductPartIndex = GetIndex();

	for (int i = pDoc->m_printGroupList.GetSize() - 1; i >= 0; i--)
	{
		CPrintGroup&  rPrintGroup  = pDoc->m_printGroupList[i];
		CPrintGroupComponent* pComponent = rPrintGroup.GetFirstBoundComponent();
		while (pComponent)
		{
			if (pComponent->m_nProductClassIndex == nProductPartIndex)
				return rPrintGroup;
			pComponent = rPrintGroup.GetNextBoundComponent(pComponent);
		}
	}	

	return nullPrintGroup;
}

BOOL CBoundProductPart::IsCover()
{
	CBoundProduct& rBoundProduct = GetBoundProduct();
	if (rBoundProduct.GetCoverProductPartIndex() == GetIndex())
		return TRUE;
	else
		return FALSE;
}

int	CBoundProductPart::GetNumUnassignedPages()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nNumAssignedPages = 0;
	int nProductPartIndex = GetIndex();
	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		nNumAssignedPages += rPrintSheet.GetNumPagesProductPart(nProductPartIndex);
	}
	return m_nNumPages - nNumAssignedPages;
}

int	CBoundProductPart::GetNumUnfoldedPages()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nNumFoldedPages = 0;
	int nProductPartIndex = GetIndex();
	CFoldSheet* pFoldSheet = GetFirstFoldSheet();
	while (pFoldSheet)
	{
		nNumFoldedPages += pFoldSheet->GetNumPages();
		pFoldSheet = GetNextFoldSheet(pFoldSheet);
	}
	return m_nNumPages - nNumFoldedPages;
}

int	CBoundProductPart::GetNumPlacedPages()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	int nNumPlacedPages   = 0;
	int nProductPartIndex = GetIndex();
	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		nNumPlacedPages += rPrintSheet.GetNumPagesProductPart(nProductPartIndex);
	}
	return nNumPlacedPages;
}

float CBoundProductPart::GetPaperThickness()
{
	if (m_nPaperGrammage < 1)
		return 0.0f;

	if (fabs(m_fPaperVolume) < 0.1f)
		return (float)m_nPaperGrammage / 1000.0f;
	else 
		return ((float)m_nPaperGrammage / 1000.0f) * m_fPaperVolume;
}

float CBoundProductPart::GetShinglingValueX(BOOL bTakeFromPaper)
{
	//return 10.0f;	// sleepy

	float fValue = 0.0f;
	if (bTakeFromPaper)
	{
		POSITION pos = theApp.m_paperDefsList.GetHeadPosition();
		while (pos)
		{
			CPaperDefs& rPaperDefs = theApp.m_paperDefsList.GetNext(pos);
			if (rPaperDefs.m_strName == m_strPaper)
			{
				if (rPaperDefs.m_fShingling > 0.0f)
					return rPaperDefs.m_fShingling;
			}
		}

		//if (m_nPaperGrammage < 1)
		//	return 0.0f;

		//if (fabs(m_fPaperVolume) < 0.1f)
		//	return (float)m_nPaperGrammage / 1000.0f;
		//else
		//	return ((float)m_nPaperGrammage / 1000.0f) * m_fPaperVolume;

		if ( (fabs(m_fPaperVolume) > 0.1f) && (m_nPaperGrammage > 1) )
			return ((float)m_nPaperGrammage / 1000.0f) * m_fPaperVolume;
	}

	fValue = m_foldingParams.GetShinglingValueX(*this);
	m_nPaperGrammage = (int)(fValue * 1000.0f); m_fPaperVolume = 1.0f;
	return fValue;
}

float CBoundProductPart::GetShinglingValueXFoot(BOOL bTakeFromPaper)
{
	float fValue = 0.0f;
	if (bTakeFromPaper)
	{
//// actually same as before - needs to be changed
		POSITION pos = theApp.m_paperDefsList.GetHeadPosition();
		while (pos)
		{
			CPaperDefs& rPaperDefs = theApp.m_paperDefsList.GetNext(pos);
			if (rPaperDefs.m_strName == m_strPaper)
			{
				if (rPaperDefs.m_fShingling > 0.0f)
					return rPaperDefs.m_fShingling;
			}
		}

		if (m_nPaperGrammage < 1)
			return 0.0f;

		if (fabs(m_fPaperVolume) < 0.1f)
			return (float)m_nPaperGrammage / 1000.0f;
		else
			return ((float)m_nPaperGrammage / 1000.0f) * m_fPaperVolume;
	}

	fValue = m_foldingParams.GetShinglingValueXFoot(*this);
	return fValue;
}

float CBoundProductPart::GetShinglingValueY(BOOL bTakeFromPaper)
{
	float fValue = 0.0f;
	if (bTakeFromPaper)
	{
		POSITION pos = theApp.m_paperDefsList.GetHeadPosition();
		while (pos)
		{
			CPaperDefs& rPaperDefs = theApp.m_paperDefsList.GetNext(pos);
			if (rPaperDefs.m_strName == m_strPaper)
			{
				if (rPaperDefs.m_fShingling > 0.0f)
					return rPaperDefs.m_fShingling;
			}
		}

		if (m_nPaperGrammage < 1)
			return 0.0f;

		if (fabs(m_fPaperVolume) < 0.1f)
			return (float)m_nPaperGrammage / 1000.0f;
		else
			return ((float)m_nPaperGrammage / 1000.0f) * m_fPaperVolume;
	}

	fValue = m_foldingParams.GetShinglingValueY(*this);
	return fValue;
}

CString CBoundProductPart::InitPaperInfo()
{
	if (this == NULL)
		return _T("");

	CString strPaperInfo;
	strPaperInfo.LoadString(IDS_NO_PAPER);

	CString strVolume;
	CPrintSheetView::TruncateNumber(strVolume, m_fPaperVolume);

	CString strPaperVolume, strPaperWeight;
	strPaperVolume.LoadString(IDS_PAPER_VOLUME);
	if ( (m_nPaperGrammage > 0) && (m_fPaperVolume > 0.0f) )
		strPaperInfo.Format(_T("%d g/m� | %s: %s"), m_nPaperGrammage, strPaperVolume, strVolume);
	else
		if (m_nPaperGrammage > 0)
			strPaperInfo.Format(_T("%d g/m�"), m_nPaperGrammage);
		else
			if (m_fPaperVolume > 0.0f)
				strPaperInfo.Format(_T("%s: %s"), strPaperVolume, strVolume);
			else
				if ( ! m_strPaper.IsEmpty())
					strPaperInfo = m_strPaper;

	return strPaperInfo;
}

void CBoundProductPart::CalcRealQuantity()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	m_nRealQuantity = INT_MAX;

	if (GetNumUnfoldedPages() <= 0)
	{
		int nProductPartIndex = GetIndex();
		POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
		while (pos)
		{
			CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
			if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
				continue;

			m_nRealQuantity = min(m_nRealQuantity, rFoldSheet.GetQuantityActual());
		}
	}

	if (m_nRealQuantity == INT_MAX)
		m_nRealQuantity = 0;
}

void CBoundProductPart::SetFoldSheetsDesiredQuantity(int nDesiredQuantity)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int nProductPartIndex = GetIndex();
	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.m_nProductPartIndex != nProductPartIndex)
			continue;

		rFoldSheet.SetQuantityPlanned(nDesiredQuantity);
	}
}

CRect CBoundProductPart::DrawSummary(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, int nNumSectionPages, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT); 

	rcFrame.left += 10; rcFrame.top += 3;

	int nThumbnailWidth = 50;
	CRect rcThumbnail = rcFrame; rcThumbnail.top += 3; rcThumbnail.bottom = rcThumbnail.top + 30;
	rcThumbnail = DrawThumbnail(pDC, rcThumbnail, nNumSectionPages, nThumbnailWidth);
	rcBox.UnionRect(rcBox, rcThumbnail);

	rcFrame.top += 2 * nTextHeight + 5;

	CString strLabel; 
	strLabel.LoadString(IDS_PAPERLIST_FORMAT);
	int nLen = pDC->GetTextExtent(strLabel).cx;
	strLabel.LoadString(IDS_AMOUNT);
	nLen = max(nLen, pDC->GetTextExtent(strLabel).cx);
	nLen += 5;

	CRect rcRet;
	rcFrame.left += nThumbnailWidth + 5; 
	rcRet = DrawSize(pDC, rcFrame, nLen);
	rcBox.UnionRect(rcBox, rcRet);

	rcFrame.top = rcRet.bottom + 5;
	rcRet = DrawQuantity(pDC, rcFrame, nLen);
	rcBox.UnionRect(rcBox, rcRet);

	rcBox.bottom += 5;

	if (pDisplayList)
	{
		CRect rcDI = rcThumbnail; 
		rcDI.left = rcFrame.left - 5; rcDI.right = min(rcBox.right + 20, rcFrame.right); rcDI.top -= 5; rcDI.bottom = rcBox.bottom;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)pPrintSheet, DISPLAY_ITEM_REGISTRY(CPrintComponentsDetailView, PageSectionProps), (void*)this, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			CImage  img; img.LoadFromResource(theApp.m_hInstance, IDB_DROPDOWN);		
			if ( ! img.IsNull())
			{
				CRect rcImg = rcDI; rcImg.left = rcDI.right - 10; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcThumbnail.top + 5; rcImg.bottom = rcImg.top + img.GetHeight();
				img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
			}
		}
	}

	font.DeleteObject();

	return rcBox;
}

CRect CBoundProductPart::DrawThumbnail(CDC* pDC, CRect rcFrame, int nNumSectionPages, int nThumbnailWidth, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CRect rcImg = rcFrame; rcImg.right = (nThumbnailWidth > 0) ? rcImg.left + nThumbnailWidth : rcImg.right; rcImg.DeflateRect(0, 1); rcImg.OffsetRect(0, -2);
	rcImg = pDoc->m_PageTemplateList.DrawSinglePage(pDC, rcImg, 0, GetIndex(), FALSE, FALSE, TRUE, TRUE, TRUE);
	if (rcImg.top == rcImg.bottom)
	{
		rcImg.top = rcFrame.top; rcImg.bottom = rcImg.top + 15;
	}
	rcBox.UnionRect(rcBox, rcImg);

	CString strOut;
	pDC->SelectObject(&boldFont);
	pDC->SetTextColor(GUICOLOR_CAPTION);
	strOut.Format(_T(" %d "), (nNumSectionPages != -1) ? nNumSectionPages : m_nNumPages);
	CRect rcPages = rcImg; rcPages.top = rcImg.CenterPoint().y - 6; rcPages.bottom = rcImg.CenterPoint().y + 8; rcPages.OffsetRect(6, 0); rcPages.right = rcPages.left + pDC->GetTextExtent(strOut).cx;
	rcPages.InflateRect(1, 1);
	CBrush brush(WHITE);
	CPen   pen(PS_SOLID, 1, GUICOLOR_CAPTION);
	CBrush* pOldBrush = pDC->SelectObject(&brush);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	pDC->RoundRect(rcPages, CPoint(8, 8));
	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	DrawTextMeasured(pDC, strOut, rcPages, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);


	CBoundProduct& rBoundProduct = GetBoundProduct();

	int nTextValign = DT_TOP;

	pDC->SetTextColor(GUICOLOR_CAPTION);	
	CRect rcText = rcFrame; rcText.left = (nThumbnailWidth > 0) ? rcText.left + nThumbnailWidth + 5 : rcText.left + Pixel2MM(pDC, 40); rcText.bottom = rcText.top + nTextHeight;
	DrawTextMeasured(pDC, rBoundProduct.m_strProductName, rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);

	rcText.left += pDC->GetTextExtent(rBoundProduct.m_strProductName).cx + 5;
	pDC->SetTextColor(RGB(40,120,30));
	DrawTextMeasured(pDC, _T("| ") + m_strName, rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CBoundProductPart::DrawSize(CDC* pDC, CRect rcFrame, int nLabelWidth, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString	 strWidth, strHeight;

	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));

	int nTextValign = DT_VCENTER;
	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_PAPERLIST_FORMAT); strLabel += _(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);
		pDC->SetTextColor(BLACK);
		rcText.left = rcLabel.right + 5;
	}

	CPrintSheetView::TruncateNumber(strWidth,  m_fPageWidth);
	CPrintSheetView::TruncateNumber(strHeight, m_fPageHeight);
	CString strOut;
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CBoundProductPart::DrawQuantity(CDC* pDC, CRect rcFrame, int nLabelWidth, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;	rcText.bottom = rcText.top + nTextHeight; rcText.right = rcText.left + pDC->GetTextExtent(_T("00.000.000")).cx + 5; 

	int nTextHalign = DT_RIGHT;
	int nTextValign = DT_TOP;

	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_AMOUNT); strLabel += _T(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);
		pDC->SetTextColor(BLACK);
		rcText.left = rcLabel.right + 5;
		rcText.right = rcText.left + pDC->GetTextExtent(_T("00.000.000")).cx + 5;
		nTextHalign = DT_LEFT;
	}

	CString strOut = FormatQuantityString(m_nDesiredQuantity);
	DrawTextMeasured(pDC, strOut, rcText, nTextHalign | nTextValign | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	rcBox.UnionRect(rcBox, rcText);
	
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, BoundProductPartDesiredQuantity), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	font.DeleteObject();

	return rcBox;
}

CRect CBoundProductPart::DrawOverProduction(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;
	rcText.bottom = rcText.top + nTextHeight;

	int nTextHalign = DT_RIGHT;
	int nTextValign = DT_TOP;

	CString strOut = FormatQuantityString(m_nRealQuantity - m_nDesiredQuantity);
	if (m_nDesiredQuantity > 0)
	{
		rcText.right = rcFrame.right - 70;
		DrawTextMeasured(pDC, strOut, rcText, nTextHalign | nTextValign | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		rcText.left = rcText.right + 5; rcText.right = rcFrame.right - 20;
		CString strPercentage;	strPercentage.Format(_T("%.1f"), ((float)m_nRealQuantity/(float)m_nDesiredQuantity - 1.0f) * 100.0f);
		strOut.Format(_T("%s%%"), strPercentage);
		if (m_nRealQuantity < m_nDesiredQuantity)
			pDC->SetTextColor(RED);
		else
			pDC->SetTextColor(RGB(80,150,80));
		DrawTextMeasured(pDC, strOut, rcText, nTextHalign | nTextValign | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	}

	font.DeleteObject();

	return rcBox;
}

CRect CBoundProductPart::DrawPaperGrain(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	int nPaperGrain = GRAIN_VERTICAL;
	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));
	CImage img;;
	switch (nPaperGrain)
	{
	case GRAIN_HORIZONTAL:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAIN_HORIZONTAL));	break;
	case GRAIN_VERTICAL:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAIN_VERTICAL));	break;
	case GRAIN_EITHER:			break;
	}
	if ( ! img.IsNull())
	{
		CRect rcImg(rcFrame.TopLeft(), CSize(img.GetWidth(), img.GetHeight()));
		img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
	}

	font.DeleteObject();

	return rcBox;
}

CRect CBoundProductPart::DrawTrims(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString strOut;
	CImage img;
	CRect rcTrims(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));
	CRect rcImg(rcTrims.TopLeft(), CSize(10, 10)); rcImg.top += 2;
	CRect rcText = rcTrims; rcText.left += 10;

	//if (abs(m_fLeftTrim) > 0.01)
	//{
	//	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_TRIM_ARROW_LEFT));	
	//	if ( ! img.IsNull())
	//	{
	//		img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
	//		img.Destroy();
	//	}
	//	CPrintSheetView::TruncateNumber(strOut, m_fLeftTrim);
	//	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	//	rcBox.UnionRect(rcBox, rcText);
	//	rcImg.left  += 30; rcText.left += 30;
	//}

	if (abs(m_trimmingParams.m_fSideTrim) > 0.01)
	{
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_TRIM_ARROW_RIGHT));	
		if ( ! img.IsNull())
		{
			img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
			img.Destroy();
		}
		CPrintSheetView::TruncateNumber(strOut, m_trimmingParams.m_fSideTrim);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
		rcImg.left  += 30; rcText.left += 30;
	}

	if (abs(m_trimmingParams.m_fFootTrim) > 0.01)
	{
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_TRIM_ARROW_DOWN));	
		if ( ! img.IsNull())
		{
			img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
			img.Destroy();
		}
		CPrintSheetView::TruncateNumber(strOut, m_trimmingParams.m_fFootTrim);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
		rcImg.left  += 30; rcText.left += 30;
	}

	if (abs(m_trimmingParams.m_fHeadTrim) > 0.01)
	{
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_TRIM_ARROW_UP));	
		if ( ! img.IsNull())
		{
			img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
			img.Destroy();
		}
		CPrintSheetView::TruncateNumber(strOut, m_trimmingParams.m_fHeadTrim);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
	}	

	font.DeleteObject();

	return rcBox;
}

CRect CBoundProductPart::DrawPaperInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));
	pDC->DrawText(m_strPaper, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);
	
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, BoundProductPartPaperInfo), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	font.DeleteObject();

	return rcBox;
}

CRect CBoundProductPart::DrawInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, COLORREF crBackground)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	//if (pDisplayList)
	//{
	//	CRect rcDI = rcFrame; 
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CPrintSheetPlanningView, BoundProductPart), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//		if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
	//		{
	//			CRect rcFeedback = pDI->m_BoundingRect; //rcFeedback.right -= 1; rcFeedback.bottom -= 1;
	//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
	//			//m_DisplayList.InvalidateItem(pDI);
	//			Graphics graphics(pDC->m_hDC);
	//			Rect rc(rcFeedback.left, rcFeedback.top, rcFeedback.Width(), rcFeedback.Height());
	//			COLORREF crHighlightColor = RGB(255,195,15);
	//			SolidBrush br(Color::Color(70, GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)));
	//			graphics.FillRectangle(&br, rc);
	//			graphics.DrawRectangle(&Pen(Color::Color(GetRValue(crHighlightColor), GetGValue(crHighlightColor), GetBValue(crHighlightColor)), 0), rc);
	//		}
	//	}
	//}


	int	nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : Pixel2MM(pDC, 15);
	CString strOut;
	
	pDC->SaveDC();
	//CRect rcClip = rcFrame; rcClip.bottom -= nTextHeight;
	//pDC->IntersectClipRect(rcClip);

	CFont font;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	//pDC->SetTextColor(GUICOLOR_CAPTION);
	pDC->SetTextColor(RGB(40,120,30));

	rcFrame.left += 5; rcFrame.top += 5;

	CRect rcTextMeasure;
	CRect rcText = rcFrame; rcText.bottom = rcText.top + nTextHeight;
	DrawTextMeasured(pDC, m_strName, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	if (pDoc->m_boundProducts.GetSize() > 1)
	{
		pDC->SetTextColor(GUICOLOR_CAPTION);
		//pDC->SetTextColor(RGB(40,120,30));	// green
		rcText.left += pDC->GetTextExtent(m_strName).cx;
		strOut.Format(_T(" | %s"), GetBoundProduct().m_strProductName);
		DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);
	}

	pDC->SelectObject(&font);

	font.DeleteObject();

	rcBox.right += 5; rcBox.bottom += 5;

	pDC->RestoreDC(-1);

	//CGraphicComponent gc;
	//gc.DrawTransparentFrame(pDC, rcBox, crBackground, TRUE);

	return rcBox;
}

void CBoundProductPart::AssignProductionProfilePool(CProductionProfile& rProductionProfile)
{
	m_trimmingParams = (rProductionProfile.ProcessIsActive(CProductionProfile::Trimming)) ? rProductionProfile.m_postpress.m_trimming	: CTrimmingProcess();
	m_foldingParams	 = (rProductionProfile.ProcessIsActive(CProductionProfile::Folding))  ? rProductionProfile.m_postpress.m_folding	: CFoldingProcess();
}


/////////////////////////////////////////////////////////////////////////////
// CBoundProductPartComponent

IMPLEMENT_SERIAL(CBoundProductPartComponent, CBoundProductPartComponent, VERSIONABLE_SCHEMA | 1)

CBoundProductPartComponent::CBoundProductPartComponent()
{
}

CBoundProductPartComponent::CBoundProductPartComponent(const CBoundProductPartComponent& rBoundProductPartComponent)
{
	Copy(rBoundProductPartComponent);
}

const CBoundProductPartComponent& CBoundProductPartComponent::operator=(const CBoundProductPartComponent& rBoundProductPartComponent)
{
	Copy(rBoundProductPartComponent);

	return *this;
}

void CBoundProductPartComponent::Copy(const CBoundProductPartComponent& rBoundProductPartComponent)
{
}

// helper function for CBoundProductPartComponent elements
template <> void AFXAPI SerializeElements <CBoundProductPartComponent> (CArchive& ar, CBoundProductPartComponent* pBoundProductPartComponent, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CBoundProductPartComponent));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
		}	
		else
		{ 
			switch (nVersion)
			{
				case 1:	break;
			}
		}

		pBoundProductPartComponent++;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CBoundProductPartComponentList

CBoundProductPartComponentList::CBoundProductPartComponentList()
{
}

CBoundProductPartComponentList::CBoundProductPartComponentList(const CBoundProductPartComponentList& rBoundProductPartComponentList)
{
	Copy(rBoundProductPartComponentList);
}


const CBoundProductPartComponentList& CBoundProductPartComponentList::operator=(const CBoundProductPartComponentList& rBoundProductPartComponentList)
{
	Copy(rBoundProductPartComponentList);
	return *this;
}

void CBoundProductPartComponentList::Copy(const CBoundProductPartComponentList& rBoundProductPartComponentList)
{
	RemoveAll();
	for (int i = 0; i < rBoundProductPartComponentList.GetSize(); i++)
	{
		CBoundProductPartComponent boundProductPartComponent = rBoundProductPartComponentList[i];
		Add(boundProductPartComponent);
	}
}




/////////////////////////////////////////////////////////////////////////////
// CFlatProduct ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CFlatProduct, CObject, VERSIONABLE_SCHEMA | 3)

CFlatProduct::CFlatProduct()
{
	m_strProductID		= _T("");
	m_strProductName	= _T("");
	m_strComment		= _T("");
	m_nNumPages			= 1;
	m_colorantList.RemoveAll();
	m_strPaper			= _T("");
	m_nPaperGrammage	= 0;
	m_fPaperVolume		= 0.0f;
	m_nPaperType		= 0;
	m_fTrimSizeWidth	= 0.0f;
	m_fTrimSizeHeight	= 0.0f;
	m_strFormatName.Empty();
	m_nPageOrientation	= PORTRAIT;
	m_nDesiredQuantity	= 0;
	m_nRealQuantity		= 0;
	m_fLeftTrim			= 0.0f; 
	m_fBottomTrim		= 0.0f;
	m_fRightTrim		= 0.0f;
	m_fTopTrim			= 0.0f;
	m_nPaperGrain		= GRAIN_EITHER;
	m_nGroup			= 0;
	m_strPunchTool		= _T("");
	m_nPriority			= 0;
	m_printDeadline		= COleDateTime::GetCurrentTime();
	m_graphicList.m_elements.RemoveAll();

	m_nFlag = 0;
}

CFlatProduct::CFlatProduct(const CFlatProduct& rFlatProduct)
{
	Copy(rFlatProduct);
}

const CFlatProduct& CFlatProduct::operator=(const CFlatProduct& rFlatProduct)
{
	Copy(rFlatProduct);
	return *this;
}

CFlatProduct& CFlatProduct::Copy(const CFlatProduct& rFlatProduct, BOOL bChangedOnly)
{
	if ( ! bChangedOnly)
	{
		m_strProductID		= rFlatProduct.m_strProductID;
		m_strProductName	= rFlatProduct.m_strProductName;
		m_strComment		= rFlatProduct.m_strComment;
		m_nNumPages			= rFlatProduct.m_nNumPages;
		m_colorantList.Copy(rFlatProduct.m_colorantList);
		m_strPaper			= rFlatProduct.m_strPaper;
		m_nPaperGrammage	= rFlatProduct.m_nPaperGrammage;
		m_fPaperVolume		= rFlatProduct.m_fPaperVolume;
		m_nPaperType		= rFlatProduct.m_nPaperType;
		m_fTrimSizeWidth	= rFlatProduct.m_fTrimSizeWidth;
		m_fTrimSizeHeight	= rFlatProduct.m_fTrimSizeHeight;
		m_strFormatName		= rFlatProduct.m_strFormatName;
		m_nPageOrientation	= rFlatProduct.m_nPageOrientation;
		m_nDesiredQuantity	= rFlatProduct.m_nDesiredQuantity;
		m_nRealQuantity		= rFlatProduct.m_nRealQuantity;
		m_fLeftTrim			= rFlatProduct.m_fLeftTrim; 
		m_fBottomTrim		= rFlatProduct.m_fBottomTrim;
		m_fRightTrim		= rFlatProduct.m_fRightTrim;
		m_fTopTrim			= rFlatProduct.m_fTopTrim;
		m_nPaperGrain		= rFlatProduct.m_nPaperGrain;
		m_nGroup			= rFlatProduct.m_nGroup;
		m_strPunchTool		= rFlatProduct.m_strPunchTool;
		m_nPriority			= rFlatProduct.m_nPriority;
		m_printDeadline		= rFlatProduct.m_printDeadline;
		m_graphicList.m_elements.Copy(rFlatProduct.m_graphicList.m_elements);
		m_nFlag				= rFlatProduct.m_nFlag;

		return *this;
	}
	else
	{
		if ( ! rFlatProduct.m_strProductName.IsEmpty())
			m_strProductName		= rFlatProduct.m_strProductName;

		if ( ! rFlatProduct.m_strComment.IsEmpty())
			m_strComment			= rFlatProduct.m_strComment;

		if (rFlatProduct.m_nNumPages != 0)
			m_nNumPages				= rFlatProduct.m_nNumPages;

		if (rFlatProduct.m_colorantList.GetSize())
			m_colorantList.Copy(rFlatProduct.m_colorantList);

		if ( ! rFlatProduct.m_strPaper.IsEmpty())
		{
			m_strPaper				= rFlatProduct.m_strPaper;
			m_nPaperGrammage		= rFlatProduct.m_nPaperGrammage;
			m_fPaperVolume			= rFlatProduct.m_fPaperVolume;
			m_nPaperType			= rFlatProduct.m_nPaperType;
		}

		if (rFlatProduct.m_fTrimSizeWidth != 0.0f)
			m_fTrimSizeWidth		= rFlatProduct.m_fTrimSizeWidth;

		if (rFlatProduct.m_fTrimSizeHeight != 0.0f)
			m_fTrimSizeHeight		= rFlatProduct.m_fTrimSizeHeight;

		if (rFlatProduct.m_strFormatName)
			m_strFormatName			= rFlatProduct.m_strFormatName;

		if (rFlatProduct.m_nPageOrientation != 0)
			m_nPageOrientation		= rFlatProduct.m_nPageOrientation;

		if (rFlatProduct.m_nDesiredQuantity != 0)
			m_nDesiredQuantity		= rFlatProduct.m_nDesiredQuantity;

		//if (rFlatProduct.m_nRealQuantity)
		//	m_nRealQuantity			= rFlatProduct.m_nRealQuantity;

		if (rFlatProduct.m_fLeftTrim != 0.0f)
			m_fLeftTrim				= rFlatProduct.m_fLeftTrim; 

		if (rFlatProduct.m_fBottomTrim != 0.0f)
			m_fBottomTrim			= rFlatProduct.m_fBottomTrim;

		if (rFlatProduct.m_fRightTrim != 0.0f)
			m_fRightTrim			= rFlatProduct.m_fRightTrim;

		if (rFlatProduct.m_fTopTrim != 0.0f)
			m_fTopTrim				= rFlatProduct.m_fTopTrim;

		if (rFlatProduct.m_nPaperGrain != -1)
			m_nPaperGrain			= rFlatProduct.m_nPaperGrain;

		if (rFlatProduct.m_nGroup != 0)
			m_nGroup				= rFlatProduct.m_nGroup;

		if ( ! rFlatProduct.m_strPunchTool.IsEmpty())
			m_strPunchTool			= rFlatProduct.m_strPunchTool;

		if (rFlatProduct.m_nPriority != -1)
			m_nPriority				= rFlatProduct.m_nPriority;

		if (rFlatProduct.m_printDeadline != 0)
			m_printDeadline			= rFlatProduct.m_printDeadline;

		if (rFlatProduct.m_graphicList.m_elements.GetSize())
			m_graphicList.m_elements.Copy(rFlatProduct.m_graphicList.m_elements);

		if (rFlatProduct.m_nFlag != 0)
			m_nFlag					= rFlatProduct.m_nFlag;

		return *this;
	}
}

int	CFlatProduct::GetIndex()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return -1;

	int		 nIndex = 0;
	POSITION pos	= pDoc->m_flatProducts.GetHeadPosition();
	while (pos)
	{
		if (&pDoc->m_flatProducts.GetNext(pos) == this)
			return nIndex;
		nIndex++;
	}

	return -1;
}

int	CFlatProduct::GetFlatProductsIndex()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return -1;

	return pDoc->GetFlatProductsIndex();
}

void CFlatProduct::AddGraphicObject(CGraphicObject* pgObject)
{
	m_graphicList.CopyGraphicObject(pgObject);
}

CPrintSheet* CFlatProduct::GetFirstPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return NULL;

	int			   nProductIndex = pDoc->m_flatProducts.FindProductIndex(this);
	CPageTemplate* pTemplate	 = pDoc->m_PageTemplateList.FindFlatProductTemplate(nProductIndex, pDoc->GetFlatProductsIndex());
	if ( ! pTemplate)
		return NULL;
	if (pTemplate->m_PrintSheetLocations.GetSize() <= 0)
		return NULL;

	POSITION pos = pDoc->m_PrintSheetList.FindIndex(pTemplate->m_PrintSheetLocations[0].m_nPrintSheetIndex);
	if (!pos)
		return NULL;
	CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetAt(pos);
	return &rPrintSheet;
}

CPrintSheet* CFlatProduct::GetNextPrintSheet(CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return NULL;

	int			   nProductIndex = pDoc->m_flatProducts.FindProductIndex(this);
	CPageTemplate* pTemplate	 = pDoc->m_PageTemplateList.FindFlatProductTemplate(nProductIndex, pDoc->GetFlatProductsIndex());
	if ( ! pTemplate)
		return NULL;
	if (pTemplate->m_PrintSheetLocations.GetSize() <= 0)
		return NULL;

	int nIndex = 0;
	while (nIndex < pTemplate->m_PrintSheetLocations.GetSize())
	{
		POSITION pos = pDoc->m_PrintSheetList.FindIndex(pTemplate->m_PrintSheetLocations[nIndex].m_nPrintSheetIndex);
		if (pos)
		{
			if (pPrintSheet == &pDoc->m_PrintSheetList.GetAt(pos))
			{
				nIndex++;
				while (nIndex < pTemplate->m_PrintSheetLocations.GetSize())
				{
					pos = pDoc->m_PrintSheetList.FindIndex(pTemplate->m_PrintSheetLocations[nIndex].m_nPrintSheetIndex);
					if (pos)
						if (pPrintSheet != &pDoc->m_PrintSheetList.GetAt(pos))
							return &pDoc->m_PrintSheetList.GetAt(pos);

					nIndex++;
				}
			}
		}
		nIndex++;
	}

	return NULL;
}

CPrintGroup& CFlatProduct::GetFirstPrintGroup()
{
	static CPrintGroup nullPrintGroup;
	
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return nullPrintGroup;

	for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
	{
		CPrintGroup&  rPrintGroup  = pDoc->m_printGroupList[i];
		CFlatProduct* pFlatProduct = rPrintGroup.GetFirstFlatProduct();
		while (pFlatProduct)
		{
			if (pFlatProduct == this)
				return rPrintGroup;
			pFlatProduct = rPrintGroup.GetNextFlatProduct(pFlatProduct);
		}
	}	

	return nullPrintGroup;
}

CLayoutObject* CFlatProduct::GetFirstLayoutObject(CPrintSheet* pPrintSheet)
{
	if ( ! pPrintSheet)
		return NULL;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return NULL;

	int		 nFlatProductIndex = GetIndex();
	POSITION pos			   = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rLayoutObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
		if ( ! rLayoutObject.IsFlatProduct())
			continue;
		if (rLayoutObject.m_nComponentRefIndex < 0)
			continue;
		if (rLayoutObject.m_nComponentRefIndex >= pPrintSheet->m_flatProductRefs.GetSize())
			continue;
		if (nFlatProductIndex == pPrintSheet->m_flatProductRefs[rLayoutObject.m_nComponentRefIndex].m_nFlatProductIndex)
			return &rLayoutObject;
	}
	return NULL;
}

int	CFlatProduct::GetNUpPrintLayout(CPrintSheet* pPrintSheet)
{
	if ( ! pPrintSheet)
		return 0;

	pPrintSheet->InitializeFlatProductTypeList();

	for (int i = 0; i < pPrintSheet->m_flatProductTypeList.GetSize(); i++)
	{
		CFlatProduct* pFlatProduct = pPrintSheet->m_flatProductTypeList[i].m_pFlatProduct;
		if (pFlatProduct == this)
			return pPrintSheet->m_flatProductTypeList[i].m_nNumUp;
	}
	return 0;
}

int	CFlatProduct::GetNUpAll()
{
	int			 nNUp = 0;
	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	while (pPrintSheet)
	{
		pPrintSheet->InitializeFlatProductTypeList();

		for (int i = 0; i < pPrintSheet->m_flatProductTypeList.GetSize(); i++)
		{
			CFlatProduct* pFlatProduct = pPrintSheet->m_flatProductTypeList[i].m_pFlatProduct;
			if (pFlatProduct == this)
				nNUp += pPrintSheet->m_flatProductTypeList[i].m_nNumUp;
		}

		pPrintSheet = GetNextPrintSheet(pPrintSheet);
	}

	return nNUp;
}

void CFlatProduct::CalcRealQuantity()
{
	m_nRealQuantity = 0;
	CPrintSheet* pPrintSheet = GetFirstPrintSheet();
	while (pPrintSheet)
	{
		int nNumUp = GetNUpPrintLayout(pPrintSheet);
		m_nRealQuantity += (pPrintSheet->m_nQuantityTotal - pPrintSheet->m_nQuantityExtra) * nNumUp;

		pPrintSheet = GetNextPrintSheet(pPrintSheet);
	}
}

float CFlatProduct::CalcCoverage(CPrintSheet* pPrintSheet)
{
	CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return 0.0f;
	else
		return pLayout->CalcUtilization(pPrintSheet, this);
}

void CFlatProduct::GetCutSize(float& fWidth, float& fHeight)
{
	fWidth  = m_fTrimSizeWidth  + m_fLeftTrim   + m_fRightTrim;
	fHeight = m_fTrimSizeHeight + m_fBottomTrim + m_fTopTrim;
}

// helper function for CFlatProduct elements
template <> void AFXAPI SerializeElements <CFlatProduct> (CArchive& ar, CFlatProduct* pFlatProduct, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CFlatProduct));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pFlatProduct->m_strProductName;
			ar << pFlatProduct->m_nNumPages;
			ar << pFlatProduct->m_strPaper;
			ar << pFlatProduct->m_nPaperGrammage;
			ar << pFlatProduct->m_fPaperVolume;
			ar << pFlatProduct->m_nPaperType;
			ar << pFlatProduct->m_fTrimSizeWidth;
			ar << pFlatProduct->m_fTrimSizeHeight;
			ar << pFlatProduct->m_strFormatName;
			ar << pFlatProduct->m_nPageOrientation;
			ar << pFlatProduct->m_nDesiredQuantity;
			ar << pFlatProduct->m_nRealQuantity;
			ar << pFlatProduct->m_fLeftTrim; 
			ar << pFlatProduct->m_fBottomTrim;
			ar << pFlatProduct->m_fRightTrim;
			ar << pFlatProduct->m_fTopTrim;
			ar << pFlatProduct->m_nPaperGrain;
			ar << pFlatProduct->m_nGroup;
			ar << pFlatProduct->m_strPunchTool;
			ar << pFlatProduct->m_nPriority;
			ar << pFlatProduct->m_printDeadline;
		}
		else
		{
			switch (nVersion)
			{
			case 1: ar >> pFlatProduct->m_strProductName;
					ar >> pFlatProduct->m_nNumPages;
					ar >> pFlatProduct->m_strPaper;
					ar >> pFlatProduct->m_nPaperGrammage;
					ar >> pFlatProduct->m_fPaperVolume;
					ar >> pFlatProduct->m_fTrimSizeWidth;
					ar >> pFlatProduct->m_fTrimSizeHeight;
					ar >> pFlatProduct->m_strFormatName;
					ar >> pFlatProduct->m_nPageOrientation;
					ar >> pFlatProduct->m_nDesiredQuantity;
					ar >> pFlatProduct->m_nRealQuantity;
					ar >> pFlatProduct->m_fLeftTrim; 
					ar >> pFlatProduct->m_fBottomTrim;
					ar >> pFlatProduct->m_fRightTrim;
					ar >> pFlatProduct->m_fTopTrim;
					ar >> pFlatProduct->m_nPaperGrain;
					ar >> pFlatProduct->m_nGroup;
					ar >> pFlatProduct->m_nPriority;
					ar >> pFlatProduct->m_printDeadline;
					break; 
			case 2: ar >> pFlatProduct->m_strProductName;
					ar >> pFlatProduct->m_nNumPages;
					ar >> pFlatProduct->m_strPaper;
					ar >> pFlatProduct->m_nPaperGrammage;
					ar >> pFlatProduct->m_fPaperVolume;
					ar >> pFlatProduct->m_fTrimSizeWidth;
					ar >> pFlatProduct->m_fTrimSizeHeight;
					ar >> pFlatProduct->m_strFormatName;
					ar >> pFlatProduct->m_nPageOrientation;
					ar >> pFlatProduct->m_nDesiredQuantity;
					ar >> pFlatProduct->m_nRealQuantity;
					ar >> pFlatProduct->m_fLeftTrim; 
					ar >> pFlatProduct->m_fBottomTrim;
					ar >> pFlatProduct->m_fRightTrim;
					ar >> pFlatProduct->m_fTopTrim;
					ar >> pFlatProduct->m_nPaperGrain;
					ar >> pFlatProduct->m_nGroup;
					ar >> pFlatProduct->m_strPunchTool;
					ar >> pFlatProduct->m_nPriority;
					ar >> pFlatProduct->m_printDeadline;
					break; 
			case 3: ar >> pFlatProduct->m_strProductName;
					ar >> pFlatProduct->m_nNumPages;
					ar >> pFlatProduct->m_strPaper;
					ar >> pFlatProduct->m_nPaperGrammage;
					ar >> pFlatProduct->m_fPaperVolume;
					ar >> pFlatProduct->m_nPaperType;
					ar >> pFlatProduct->m_fTrimSizeWidth;
					ar >> pFlatProduct->m_fTrimSizeHeight;
					ar >> pFlatProduct->m_strFormatName;
					ar >> pFlatProduct->m_nPageOrientation;
					ar >> pFlatProduct->m_nDesiredQuantity;
					ar >> pFlatProduct->m_nRealQuantity;
					ar >> pFlatProduct->m_fLeftTrim; 
					ar >> pFlatProduct->m_fBottomTrim;
					ar >> pFlatProduct->m_fRightTrim;
					ar >> pFlatProduct->m_fTopTrim;
					ar >> pFlatProduct->m_nPaperGrain;
					ar >> pFlatProduct->m_nGroup;
					ar >> pFlatProduct->m_strPunchTool;
					ar >> pFlatProduct->m_nPriority;
					ar >> pFlatProduct->m_printDeadline;
					break; 
			}
		}

		pFlatProduct->m_colorantList.Serialize(ar);
		pFlatProduct->m_graphicList.Serialize(ar);

		pFlatProduct++;
	}
}

void AFXAPI DestructElements(CFlatProduct* pFlatProduct, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pFlatProduct->m_strProductName.Empty();
		pFlatProduct->m_strComment.Empty();
		pFlatProduct->m_graphicList.Destruct();

		pFlatProduct++;
	}
}

CRect CFlatProduct::DrawThumbnail(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth, BOOL bRelativSize)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_CAPTION);

	float fMaxHeight = pDoc->m_flatProducts.GetMaxHeight();

	CRect rcImg;
	if (bRelativSize)
	{
		int nHeight = (int)((float)(nTextHeight) * (m_fTrimSizeHeight / fMaxHeight)); 	
		int nWidth	= 2*nTextHeight;
		nHeight = max(nHeight, 5); nWidth = max(nWidth, 5);
		rcImg = CRect(CPoint(rcFrame.left, rcFrame.top),  CSize(nWidth, nHeight));	
	}
	else
		rcImg = rcFrame;	

	if (nLabelWidth > 0)
		rcImg.right = min(rcImg.right, rcImg.left + nLabelWidth);
	else
		rcImg.right = min(rcImg.right, rcImg.left + 45);

	int nProductIndex = pDoc->m_flatProducts.FindProductIndex(this);
	POSITION pos = pDoc->m_PageTemplateList.FindIndex(nProductIndex * 2, pDoc->GetFlatProductsIndex());
	CRect rcProduct = DrawPreview(pDC, rcImg + CSize(0, 3), (pos) ? &pDoc->m_PageTemplateList.GetAt(pos) : NULL, NULL, FALSE, TRUE);
	rcBox.UnionRect(rcBox, rcProduct);

	BOOL bSingleLine = (rcFrame.Height() < 2 * nTextHeight) ? TRUE : FALSE;
	int nTextValign = (bSingleLine) ? DT_VCENTER : DT_TOP;

	CRect rcTitle = rcFrame; rcTitle.left = (nLabelWidth == 0) ? rcImg.right + Pixel2MM(pDC, 10) : rcTitle.left + nLabelWidth + 8; rcTitle.bottom = rcTitle.top + nTextHeight;
	pDC->DrawText(m_strProductName, rcTitle, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS);
	rcTitle.right = min(rcTitle.left + pDC->GetTextExtent(m_strProductName).cx, rcTitle.right);
	rcBox.UnionRect(rcBox, rcTitle);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawSize(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString	 strWidth, strHeight;

	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));

	int nTextValign = DT_VCENTER;

	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_PAPERLIST_FORMAT); strLabel += _(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);
		pDC->SetTextColor(BLACK);
		rcText.left = rcLabel.right + 5;
	}

	CPrintSheetView::TruncateNumber(strWidth,  m_fTrimSizeWidth);
	CPrintSheetView::TruncateNumber(strHeight, m_fTrimSizeHeight);
	CString strOut;
	strOut.Format(_T("%s x %s"), strWidth, strHeight);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawTargetQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;	rcText.bottom = rcText.top + nTextHeight;  

	int nTextHAlign = DT_RIGHT;

	CString strOut = FormatQuantityString(m_nDesiredQuantity);
	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_TARGET_QUANTITY); strLabel += _T(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		rcText.left = rcLabel.right + 5;
		nTextHAlign = DT_LEFT;
	}
	else
		rcText.right = rcText.left + pDC->GetTextExtent(_T("00.000.000")).cx + 5; 

	pDC->SetTextColor(BLACK);
	DrawTextMeasured(pDC, strOut, rcText, nTextHAlign | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, FlatProductDesiredQuantity), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawActualQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;	rcText.bottom = rcText.top + nTextHeight;  

	CString strOut = FormatQuantityString(m_nRealQuantity);
	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_ACTUAL_QUANTITY); strLabel += _T(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		rcText.left = rcLabel.right + 5;
	}

	pDC->SetTextColor(BLACK);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, FlatProductDesiredQuantity), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawDifferenceQuantity(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;
	rcText.bottom = rcText.top + nTextHeight;

	int		nDifference = m_nRealQuantity - m_nDesiredQuantity;
	CString strOut1		= FormatQuantityString(nDifference);
	CString strOut2;	  strOut2.Format(_T("%.1f%%"), fabs(((float)m_nRealQuantity/(float)m_nDesiredQuantity - 1.0f) * 100.0f));
	COLORREF crColor1	= BLACK;
	COLORREF crColor2	= (nDifference < 0) ? RED : RGB(80,150,80);

	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_DIFFERENCE); strLabel += _T(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS, rcBox);
		rcText.left = rcLabel.right + 5;
	}

	if (nDifference < 0)
		rcText.left -= pDC->GetTextExtent(_T("-")).cx;
	else
		if (nDifference > 0)
		{
			rcText.left -= pDC->GetTextExtent(_T("+")).cx;
			strOut1.Insert(0, _T("+"));
		}

	pDC->SetTextColor(crColor1);
	DrawTextMeasured(pDC, strOut1, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	rcText.left = rcBox.right + 5;

	pDC->SetTextColor(crColor2);
	DrawTextMeasured(pDC, strOut2, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawOverProduction(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText = rcFrame;
	rcText.bottom = rcText.top + nTextHeight;

	int nTextHalign = DT_RIGHT;
	int nTextValign = DT_TOP;

	CString strOut = FormatQuantityString(m_nRealQuantity - m_nDesiredQuantity);
	if (m_nDesiredQuantity > 0)
	{
		if (nLabelWidth > 0)
		{
			pDC->SetTextColor(DARKGRAY);
			CString strLabel; strLabel.LoadString(IDS_OVERPRODUCTION); strLabel += _T(":");
			CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
			DrawTextMeasured(pDC, strLabel, rcLabel, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);
			if (m_nRealQuantity < m_nDesiredQuantity)
				pDC->SetTextColor(RED);
			else
				pDC->SetTextColor(BLACK);
			rcText.left = rcLabel.right + 5;
			nTextHalign = DT_LEFT;
			rcText.right = rcText.left + pDC->GetTextExtent(strOut).cx + 10;
		}
		else
			rcText.right = rcFrame.right - 70;

		DrawTextMeasured(pDC, strOut, rcText, nTextHalign | nTextValign | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		rcText.left = rcText.right + 5; rcText.right = rcFrame.right - 20;
		CString strPercentage;	strPercentage.Format(_T("%.1f"), ((float)m_nRealQuantity/(float)m_nDesiredQuantity - 1.0f) * 100.0f);
		strOut.Format(_T("%s%%"), strPercentage);
		if (m_nRealQuantity < m_nDesiredQuantity)
			pDC->SetTextColor(RED);
		else
			pDC->SetTextColor(RGB(80,150,80));
		DrawTextMeasured(pDC, strOut, rcText, nTextHalign | nTextValign | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	}

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawCoverage(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, int nLabelWidth, CPrintSheet* pPrintSheet)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont	 font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	int nTab = rcFrame.Width()/2;
	CRect rcText = rcFrame;
	rcText.bottom = rcText.top + nTextHeight;

	int nTextValign = DT_TOP;

	CString strOut;	
	if (nLabelWidth > 0)
	{
		pDC->SetTextColor(DARKGRAY);
		CString strLabel; strLabel.LoadString(IDS_COVERAGE); strLabel += _(":");
		CRect rcLabel = rcText; rcLabel.right = rcLabel.left + nLabelWidth;
		DrawTextMeasured(pDC, strLabel, rcText, DT_LEFT | DT_SINGLELINE | nTextValign | DT_END_ELLIPSIS, rcBox);
		pDC->SetTextColor(BLACK);
		rcText.left = rcLabel.right + 5;
	}

	CString strPercentage;	strPercentage.Format(_T("%.1f"), CalcCoverage(pPrintSheet) * 100.0f);
	strOut.Format(_T("%s%%"), strPercentage);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | nTextValign | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawPunchInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));
	pDC->DrawText(m_strPunchTool, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);
	
	//if (pDisplayList)
	//{
	//	CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, FlatProductPaperInfo), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//		if (pDI->m_nState == CDisplayItem::Selected)
	//		{
	//			CRect rcFeedback = pDI->m_BoundingRect;
	//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
	//			CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
	//		}
	//	}
	//}

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawZone(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;
	if (m_nPriority == 0)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString strOut; strOut.Format(_T("%d"), m_nPriority);
	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);
	
	//if (pDisplayList)
	//{
	//	CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, FlatProductPaperInfo), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//		if (pDI->m_nState == CDisplayItem::Selected)
	//		{
	//			CRect rcFeedback = pDI->m_BoundingRect;
	//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
	//			CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
	//		}
	//	}
	//}

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawPaperInfo(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));
	pDC->DrawText(m_strPaper, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);
	
	if (pDisplayList)
	{
		CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, FlatProductPaperInfo), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)
			{
				CRect rcFeedback = pDI->m_BoundingRect;
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
			}
		}
	}

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawPaperGrain(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcText(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));
	CImage img;
	switch (m_nPaperGrain)
	{
	case GRAIN_HORIZONTAL:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAIN_HORIZONTAL));	break;
	case GRAIN_VERTICAL:		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_GRAIN_VERTICAL));	break;
	case GRAIN_EITHER:			break;
	}
	if ( ! img.IsNull())
	{
		CRect rcImg(rcFrame.TopLeft(), CSize(img.GetWidth(), img.GetHeight()));
		img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
	}

	//pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS);
	//rcBox.UnionRect(rcBox, rcText);
	
	//if (pDisplayList)
	//{
	//	CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, FlatProductPaperInfo), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//		if (pDI->m_nState == CDisplayItem::Selected)
	//		{
	//			CRect rcFeedback = pDI->m_BoundingRect;
	//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
	//			CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
	//		}
	//	}
	//}

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawTrims(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CString strOut;
	CImage img;
	CRect rcTrims(rcFrame.TopLeft(), CSize(rcFrame.Width(), nTextHeight));
	CRect rcImg(rcTrims.TopLeft(), CSize(10, 10)); rcImg.top += 2;
	CRect rcText = rcTrims; rcText.left += 10;

	if (abs(m_fLeftTrim) > 0.01)
	{
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_TRIM_ARROW_LEFT));	
		if ( ! img.IsNull())
		{
			img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
			img.Destroy();
		}
		CPrintSheetView::TruncateNumber(strOut, m_fLeftTrim);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
		rcImg.left  += 30; rcText.left += 30;
	}

	if (abs(m_fRightTrim) > 0.01)
	{
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_TRIM_ARROW_RIGHT));	
		if ( ! img.IsNull())
		{
			img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
			img.Destroy();
		}
		CPrintSheetView::TruncateNumber(strOut, m_fRightTrim);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
		rcImg.left  += 30; rcText.left += 30;
	}

	if (abs(m_fBottomTrim) > 0.01)
	{
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_TRIM_ARROW_DOWN));	
		if ( ! img.IsNull())
		{
			img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
			img.Destroy();
		}
		CPrintSheetView::TruncateNumber(strOut, m_fBottomTrim);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
		rcImg.left  += 30; rcText.left += 30;
	}

	if (abs(m_fTopTrim) > 0.01)
	{
		img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_TRIM_ARROW_UP));	
		if ( ! img.IsNull())
		{
			img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top, img.GetWidth(), img.GetHeight(), WHITE);
			img.Destroy();
		}
		CPrintSheetView::TruncateNumber(strOut, m_fTopTrim);
		pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
		rcBox.UnionRect(rcBox, rcText);
	}	
	//if (pDisplayList)
	//{
	//	CRect rcDI = rcText; rcDI.top -= 1; rcDI.left -= 5; rcDI.right -= 1; rcDI.bottom = rcDI.top + nTextHeight + 1;
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CProductsView, FlatProductPaperInfo), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//	{
	//		pDI->SetMouseOverFeedback();
	//		pDI->SetNoStateFeedback();
	//		if (pDI->m_nState == CDisplayItem::Selected)
	//		{
	//			CRect rcFeedback = pDI->m_BoundingRect;
	//			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
	//			CPrintSheetView::FillTransparent(pDC, rcFeedback, RGB(127,162,207), TRUE, 40);
	//		}
	//	}
	//}

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawSummary(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	int nTextHeight = 15;

	CString strLabel; 
	strLabel.LoadString(IDS_PAPERLIST_FORMAT);
	int nLen = pDC->GetTextExtent(strLabel).cx;
	if (m_nDesiredQuantity > 0)
	{
		strLabel.LoadString(IDS_TARGET_QUANTITY);
		nLen = max(nLen, pDC->GetTextExtent(strLabel).cx);
		strLabel.LoadString(IDS_ACTUAL_QUANTITY);
		nLen = max(nLen, pDC->GetTextExtent(strLabel).cx);
		strLabel.LoadString(IDS_DIFFERENCE);
		nLen = max(nLen, pDC->GetTextExtent(strLabel).cx);
	}
	nLen += 5;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT); 

	rcFrame.left += 5;
	CRect rcText = rcFrame; rcText.top += 2; rcText.bottom = rcText.top + nTextHeight;

	CRect rcThumbnail = rcFrame; rcThumbnail.bottom = rcThumbnail.top + 50;
	rcThumbnail = DrawThumbnail(pDC, rcThumbnail, pDisplayList, 50, FALSE);

	rcFrame.left += 58; rcFrame.top += nTextHeight + 5;
	CRect rcRet = DrawSize(pDC, rcFrame, NULL, nLen);
	rcBox.UnionRect(rcBox, rcRet);

	if (m_nDesiredQuantity > 1)
	{
		rcFrame.top = rcRet.bottom + 5;
		rcRet = DrawTargetQuantity(pDC, rcFrame, NULL, nLen);
		rcBox.UnionRect(rcBox, rcRet);

		rcFrame.top = rcRet.bottom + 5;
		rcRet = DrawActualQuantity(pDC, rcFrame, NULL, nLen);
		rcBox.UnionRect(rcBox, rcRet);

		rcFrame.top = rcRet.bottom + 5;
		rcRet = DrawDifferenceQuantity(pDC, rcFrame, NULL, nLen);
		rcBox.UnionRect(rcBox, rcRet);
	}

	if (pDisplayList)
	{
		CRect rcDI = rcBox; rcDI.right += 20; rcDI.bottom += 5;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)GetIndex(), DISPLAY_ITEM_REGISTRY(CPrintComponentsDetailView, FlatProduct), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			CImage  img; img.LoadFromResource(theApp.m_hInstance, IDB_DROPDOWN);		
			if ( ! img.IsNull())
			{
				CRect rcImg = rcDI; rcImg.left = rcDI.right - 10; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcBox.top + 5; rcImg.bottom = rcImg.top + img.GetHeight();
				img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
			}
		}
	}

	rcBox.bottom += 5;

	font.DeleteObject();

	return rcBox;
}

CRect CFlatProduct::DrawPreview(CDC* pDC, CRect rcFrame, CPageTemplate* pPageTemplate, CLayoutObject* pObject, BOOL bShowTrims, BOOL bShowBitmap)
{
	CRect rcBox(0,0,1,1);

	float fWidth = 0.0f, fHeight = 0.0f, fSpine = 0.0f;

	if (pObject)
	{
		fWidth  = pObject->GetRealWidth();
		fHeight = pObject->GetRealHeight();
	}
	else
	{
		fWidth  = m_fTrimSizeWidth;
		fHeight = m_fTrimSizeHeight;
	}

	pDC->LPtoDP(&rcFrame);

	CSize szExtents;
	if (bShowTrims)
		szExtents = CSize(CPrintSheetView::WorldToLP(fWidth + m_fLeftTrim + m_fRightTrim), CPrintSheetView::WorldToLP(fHeight + m_fBottomTrim + m_fTopTrim));
	else
		szExtents = CSize(CPrintSheetView::WorldToLP(fWidth), CPrintSheetView::WorldToLP(fHeight));
	if ( (szExtents.cx <= 0) || (szExtents.cy <= 0) )
		return rcBox;

	CRect rcViewPort = rcFrame;

	pDC->SaveDC();
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(szExtents);
	pDC->SetWindowOrg(0, szExtents.cy);

	float fFactorX = (float)rcFrame.Width() /(float)szExtents.cx;
	float fFactorY = (float)rcFrame.Height()/(float)szExtents.cy;
	int nViewportWidth  = rcFrame.Width();
	int nViewportHeight = -(int)((float)szExtents.cy * min(fFactorX, fFactorY));
	nViewportWidth  = (abs(nViewportWidth)  <= 0) ?  1 : nViewportWidth;
	nViewportHeight = (abs(nViewportHeight) <= 0) ? -1 : nViewportHeight;
	pDC->SetViewportExt(nViewportWidth, nViewportHeight);  
	pDC->SetViewportOrg(rcViewPort.left, rcViewPort.CenterPoint().y - abs(nViewportHeight)/2);

	BOOL bShowMasking  = FALSE;
	BOOL bShowPageNums = FALSE;

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	// Draw page
	CRect rect(0,0,0,0), trimRect(0,0,0,0);
	rect.left  = CPrintSheetView::WorldToLP((bShowTrims) ? m_fLeftTrim : 0.0f); 
	rect.top   = szExtents.cy;
	rect.right = rect.left + CPrintSheetView::WorldToLP(fWidth);	rect.bottom = rect.top - CPrintSheetView::WorldToLP(fHeight);
	int nTextY = rect.top;
	trimRect   = rect; 
	trimRect.top	 += CPrintSheetView::WorldToLP(m_fTopTrim	);
	trimRect.bottom	 -= CPrintSheetView::WorldToLP(m_fBottomTrim);
	trimRect.left	 -= CPrintSheetView::WorldToLP(m_fLeftTrim  );
	trimRect.right	 += CPrintSheetView::WorldToLP(m_fRightTrim );

	int nOffset = trimRect.top - szExtents.cy;
	if (bShowTrims)
	{
		rect.OffsetRect			(0, -nOffset);
		trimRect.OffsetRect		(0, -nOffset);
	}

	CRect pageRect  = rect;

	if (bShowTrims)
	{
		CPen* pOldPen = (CPen*)pDC->SelectStockObject(BLACK_PEN);
		CPrintSheetView::FillRectangleExt(pDC, trimRect, (bShowMasking) ? WHITE : RGB(221,236,247));
		trimRect.NormalizeRect();
		rcBox.UnionRect(rcBox, trimRect);
		pDC->SelectObject(pOldPen);
	}

	CPrintSheetView::FillRectangleExt(pDC, pageRect, (bShowMasking) ? WHITE : RGB(240,240,240)); 

	CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
	CPen	pen(PS_SOLID, 1, LIGHTGRAY);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CPrintSheetView::DrawRectangleExt(pDC, pageRect);

	pageRect.NormalizeRect();
	rcBox.UnionRect(rcBox, pageRect);

	if (bShowBitmap && pPageTemplate)
	{
		if (pPageTemplate->m_ColorSeparations.GetSize() > 0)
		{
			int				   nColorDefinitionIndex = pPageTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex;
			CPageSource*	   pPageSource			 = pPageTemplate->GetObjectSource	   (nColorDefinitionIndex);
			CPageSourceHeader* pPageSourceHeader	 = pPageTemplate->GetObjectSourceHeader(nColorDefinitionIndex);
			if (pPageSource && pPageSourceHeader)
			{
				CRect outBBox, pageBBox = rect;
				int	  nOutBBoxWidth, nOutBBoxHeight;
				if ((pPageTemplate->m_fContentRotation == 0.0f) || (pPageTemplate->m_fContentRotation == 180.0f))
				{
					nOutBBoxWidth  = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxRight - pPageSourceHeader->m_fBBoxLeft)   * pPageTemplate->m_fContentScaleX);
					nOutBBoxHeight = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxTop   - pPageSourceHeader->m_fBBoxBottom) * pPageTemplate->m_fContentScaleY);
				}
				else
				{
					nOutBBoxWidth  = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxTop   - pPageSourceHeader->m_fBBoxBottom) * pPageTemplate->m_fContentScaleX);
					nOutBBoxHeight = CPrintSheetView::WorldToLP((pPageSourceHeader->m_fBBoxRight - pPageSourceHeader->m_fBBoxLeft)   * pPageTemplate->m_fContentScaleY);
				}
				outBBox.left   = pageBBox.CenterPoint().x - nOutBBoxWidth  / 2;
				outBBox.bottom = pageBBox.CenterPoint().y - nOutBBoxHeight / 2;
				outBBox.right  = pageBBox.CenterPoint().x + nOutBBoxWidth  / 2;
				outBBox.top    = pageBBox.CenterPoint().y + nOutBBoxHeight / 2;
				outBBox		  += CSize(CPrintSheetView::WorldToLP(pPageTemplate->GetContentOffsetX()), CPrintSheetView::WorldToLP(pPageTemplate->GetContentOffsetY()));
				
				POSITION pos = pDoc->m_PageSourceList.FindIndex(pPageTemplate->m_ColorSeparations[0].m_nPageSourceIndex);
				if (pos)
				{
					int nColorSel = 0;
					if (nColorSel >= 0 && nColorSel < pPageTemplate->m_ColorSeparations.GetSize())
					{
						CString strPreviewFile;
						strPreviewFile = pDoc->m_PageSourceList.GetAt(pos).GetPreviewFileName(pPageTemplate->m_ColorSeparations[nColorSel].m_nPageNumInSourceFile);
						pDC->IntersectClipRect(pageRect);
						CPageListView::DrawPreview(pDC, outBBox, TOP, pPageTemplate->m_fContentRotation, strPreviewFile);
						pDC->SelectClipRgn(NULL);
					}
				}
			}
		}
	}

	if (bShowTrims && ! bShowMasking)
		DrawPreviewMeasures(pDC, pageRect, trimRect);

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (bShowPageNums)
	{
		CFont font;
		CSize szFont(1500, 1500);
		font.CreateFont(szFont.cy, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
		CFont* pOldFont = pDC->SelectObject(&font);

		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(230,230,230));
		CSize szSize(3,3);
		pDC->DPtoLP(&szSize);
		pageRect.DeflateRect(szSize.cx, -szSize.cy);
		CString strOut;
		strOut.Format(_T(" %s "), m_strProductName);
		pDC->SetTextColor(DARKBLUE);
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(RGB(220,225,240));//RGB(220,220,220));
		pDC->DrawText(strOut, pageRect, DT_SINGLELINE|DT_BOTTOM|DT_LEFT);

		pDC->SelectObject(pOldFont);
		font.DeleteObject();
	}

	pDC->LPtoDP(&rcBox);
	rcBox.NormalizeRect();

	pDC->RestoreDC(-1);

	pDC->DPtoLP(&rcBox);

	return rcBox;
}

void CFlatProduct::DrawPreviewMeasures(CDC* pDC, CRect pageRect, CRect trimRect)
{
	CSize sizeDistance(6,6);
	pDC->DPtoLP(&sizeDistance);
	CPen pen(PS_SOLID, 1, MIDDLEGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);

	CString string;
	CPoint  hotSpot1, hotSpot2;
	CPrintSheetView::TruncateNumber(string, m_fLeftTrim);
	hotSpot1 = CPoint(trimRect.left,  pageRect.CenterPoint().y); 
	CPageTemplate::DrawSingleMeasure(pDC, string, hotSpot1, RIGHT);

	CPrintSheetView::TruncateNumber(string, m_fRightTrim);
	hotSpot1 = CPoint(trimRect.right, pageRect.CenterPoint().y); 
	CPageTemplate::DrawSingleMeasure(pDC, string, hotSpot1, LEFT);

	hotSpot1 = CPoint(pageRect.CenterPoint().x, trimRect.bottom);  
	hotSpot2 = CPoint(pageRect.CenterPoint().x, trimRect.top);  
	CPrintSheetView::TruncateNumber(string, m_fTopTrim);
	CPageTemplate::DrawSingleMeasure(pDC, string, hotSpot1, BOTTOM);
	CPrintSheetView::TruncateNumber(string, m_fBottomTrim);
	CPageTemplate::DrawSingleMeasure(pDC, string, hotSpot2, TOP);

	pDC->SelectObject(pOldPen);
}

CRect CFlatProduct::DrawSheetPreview(CDC* pDC, CRect rcFrame, CLayoutObject* pLayoutObject, BOOL bShowContent)
{
	pDC->FillSolidRect(rcFrame, WHITE);	//background

	int nHeadPosition = (pLayoutObject) ? pLayoutObject->m_nHeadPosition : TOP;

	float fWidth, fHeight; 
	if ( (nHeadPosition == TOP) || (nHeadPosition == BOTTOM) )
		GetCutSize(fWidth, fHeight);
	else
		GetCutSize(fHeight, fWidth);

	int		nExtentX	= CPrintSheetView::WorldToLP(fWidth);
	int		nExtentY	= CPrintSheetView::WorldToLP(fHeight);
	int		nXOrg		= 0;
	int		nYOrg		= 0;
	CRect	rcViewPort	= rcFrame;
	rcViewPort.DeflateRect(1, 1);

	pDC->SaveDC();
	pDC->SetMapMode(MM_ISOTROPIC); 
	pDC->SetWindowExt(nExtentX, nExtentY);
	pDC->SetWindowOrg(nXOrg,	nExtentY + nYOrg);
	pDC->SetViewportExt(rcViewPort.Width(), -rcViewPort.Height());
	pDC->SetViewportOrg(rcViewPort.left, rcViewPort.top);

	CPrintSheet printSheet;
	CLayout		layout;
	printSheet.Create(0, -1);
	layout.Create(CPrintingProfile());
	printSheet.LinkLayout(&layout);
	layout.SetObjectsStatusAll(FALSE);

	CRect rcPlaceArea(0, 0, (long)(fWidth * 1000.0f), (long)(fHeight * 1000.0f));

	layout.m_FrontSide.m_fPaperLeft = 0.0f; layout.m_FrontSide.m_fPaperBottom = 0.0f; layout.m_FrontSide.m_fPaperRight = fWidth; layout.m_FrontSide.m_fPaperTop = fHeight; layout.m_FrontSide.m_fPaperWidth = fWidth; layout.m_FrontSide.m_fPaperHeight = fHeight; 
	layout.m_BackSide.m_fPaperLeft  = 0.0f; layout.m_BackSide.m_fPaperBottom  = 0.0f; layout.m_BackSide.m_fPaperRight  = fWidth; layout.m_BackSide.m_fPaperTop  = fHeight; layout.m_BackSide.m_fPaperWidth  = fWidth; layout.m_BackSide.m_fPaperHeight  = fHeight; 
	layout.MakeNUp(FALSE, printSheet, this, 1, nHeadPosition, NULL, FALSE, rcPlaceArea);

	POSITION pos = layout.m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = layout.m_FrontSide.m_ObjectList.GetNext(pos);
		rObject.Draw(pDC, TRUE, FALSE, FALSE, &printSheet);
		if (bShowContent)
			rObject.DrawPreview(pDC, &printSheet, -1);
	}
	printSheet.DeleteContents();
	layout.DeleteContents();

	CRect rcBox = CRect(CPoint(nXOrg, nYOrg), CSize(nExtentX, nExtentY));
	pDC->LPtoDP(&rcBox);
	rcBox.NormalizeRect();

	pDC->RestoreDC(-1);

	return rcBox;
}

void CFlatProduct::AssignDuplicateName()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CString strNewName;
	for (int i = 1; i <= pDoc->m_flatProducts.GetCount(); i++) 
	{
		BOOL bOccupied = FALSE;
		POSITION pos = pDoc->m_flatProducts.GetHeadPosition();
		while (pos)
		{
			CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);
			if (rFlatProduct.GetNumDuplicateName(m_strProductName) == i)
			{
				bOccupied = TRUE;
				break;
			}
		}
		if ( ! bOccupied)
		{
			strNewName.Format(_T("%s#%d"), m_strProductName, i);
			m_strProductName = strNewName;
			return;
		}
	}

	strNewName.Format(_T("%s#%d"), m_strProductName, pDoc->m_flatProducts.GetCount() + 1);
	m_strProductName = strNewName;
}

int CFlatProduct::GetNumDuplicateName(CString strName)
{
	CString  string = CString(strName) + "#";
	const TCHAR* poi = _tcsstr((LPCTSTR)m_strProductName, (LPCTSTR)string);
	if (poi)
	{
		poi = _tcschr(poi, '#');
		poi++;
		if (isdigit(*poi))	
			return _ttoi(poi);
	}

	return -1;
}

BOOL CFlatProduct::PageSourceIsAssigned(CString strDocumentFullpath)
{
	if (strDocumentFullpath.IsEmpty())
		return FALSE;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	int		 nFlatProductsIndex = pDoc->GetFlatProductsIndex();
	POSITION pos = pDoc->m_PageTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rPageTemplate = pDoc->m_PageTemplateList.CList::GetNext(pos);
		if (rPageTemplate.m_nProductPartIndex != nFlatProductsIndex)
			continue;
		if (rPageTemplate.m_strPageID.CompareNoCase(m_strProductName) != 0)
			continue;

		for (int i = 0; i < rPageTemplate.m_ColorSeparations.GetSize(); i++)
		{
			POSITION posPageSource = pDoc->m_PageSourceList.FindIndex(rPageTemplate.m_ColorSeparations[i].m_nPageSourceIndex);
			if (posPageSource)
			{
				CPageSource& rCurPageSource = pDoc->m_PageSourceList.GetAt(posPageSource);
				if (rCurPageSource.m_strDocumentFullpath == strDocumentFullpath)
					return TRUE;
			}
		}
	}
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
// CFlatProductList ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CFlatProductList, CObject, VERSIONABLE_SCHEMA | 1)

CFlatProductList::CFlatProductList()
{
}

void CFlatProductList::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CFlatProductList));

	if (ar.IsStoring())
	{
		ar.WriteCount(GetCount());
		for (CNode* pNode = m_pNodeHead; pNode != NULL; pNode = pNode->pNext)
		{
			SerializeElements(ar, &pNode->data, 1);
		}
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();
		switch (nVersion)
		{
		case 1: break;
		}

		DWORD_PTR nNewCount = ar.ReadCount();
		while (nNewCount--)
		{
			CFlatProduct newFlatProduct;
			SerializeElements(ar, &newFlatProduct, 1);
			AddTail(newFlatProduct);
		}
	}
}

void CFlatProductList::Copy(CFlatProductList& rFlatProductList)
{
	POSITION pos = rFlatProductList.GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rProduct = rFlatProductList.GetNext(pos);
		AddTail(rProduct);
	}
}

POSITION CFlatProductList::AddTail(CFlatProduct& rFlatProduct)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rCurFlatProduct = GetNext(pos);
		if (rCurFlatProduct.m_strProductName.CompareNoCase(rFlatProduct.m_strProductName) == 0)
		{
			rFlatProduct.AssignDuplicateName();
			break;
		}
	}

	pos = CList <CFlatProduct, CFlatProduct&>::AddTail(rFlatProduct);
	CList <CFlatProduct, CFlatProduct&>::GetAt(pos).m_pParentList = this;

	return pos;
}

CFlatProduct& CFlatProductList::FindByIndex(INT_PTR nIndex)
{
	static private CFlatProduct nullProduct;

	if ( (nIndex < 0) || (nIndex >= GetSize()) )
		return nullProduct;

	POSITION pos = FindIndex(nIndex);
	if (pos)
		return GetAt(pos);
	else
		return nullProduct;
}

CFlatProduct& CFlatProductList::FindProductByID(CString strID)
{
	static private CFlatProduct nullProduct;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rProduct = GetNext(pos);
		if (rProduct.GetProductID() == strID)
			return rProduct;
	}
	return nullProduct;
}

CFlatProduct& CFlatProductList::FindProduct(CString strProductName)
{
	static private CFlatProduct nullProduct;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rProduct = GetNext(pos);
		if (rProduct.m_strProductName == strProductName)
			return rProduct;
	}
	return nullProduct;
}

int CFlatProductList::FindProductIndex(CString strProductName)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rProduct = GetNext(pos);
		if (rProduct.m_strProductName == strProductName)
			return nIndex;
		nIndex++;
	}

	return -1;
}

int CFlatProductList::FindProductIndex(CFlatProduct* pFlatProduct)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rProduct = GetNext(pos);
		if (pFlatProduct == &rProduct)
			return nIndex;
		nIndex++;
	}

	return -1;
}

void CFlatProductList::CheckLayoutObjectFormats()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc) 
		return;

	int		 i	 = 0;
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = GetNext(pos);
		pDoc->m_PrintSheetList.CheckLayoutObjectFormats(i, rFlatProduct.m_fTrimSizeWidth, rFlatProduct.m_fTrimSizeHeight, "", -1, TRUE);
		i++;
	}
}

BOOL CFlatProductList::HasGraphics()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = GetNext(pos);
		if (rFlatProduct.m_graphicList.m_elements.GetSize())
			return TRUE;
	}
	return FALSE;
}

float CFlatProductList::GetMaxHeight(int nNum)
{
	if (nNum == -1)
		nNum = GetSize();
	else
		nNum = min(nNum, GetSize());

	int		 i			= 0;
	float	 fMaxHeight = 0.0f;
	POSITION pos		= GetHeadPosition();
	while (pos && (i < nNum))
	{
		CFlatProduct& rFlatProduct = GetNext(pos);
		fMaxHeight = max(fMaxHeight, rFlatProduct.m_fTrimSizeHeight);
		i++;
	}
	return fMaxHeight;
}

void CFlatProductList::UpdateQuantities()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = GetNext(pos);
		rFlatProduct.CalcRealQuantity();
	}
}

CRect CFlatProductList::DrawHeader(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int	  nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;
	HICON hIconPlus	  = theApp.LoadIcon(IDI_PLUS);
	HICON hIconMinus  = theApp.LoadIcon(IDI_MINUS);

	CRect rcIcon = rcFrame; rcIcon.top += 3; rcIcon.right = rcIcon.left + 9; rcIcon.bottom = rcIcon.top + 9;
	pDC->DrawIcon(rcIcon.TopLeft(), hIconMinus);//(DoShowProductsExpanded()) ? hIconMinus : hIconPlus);

	//if (pDisplayList)
	//{
	//	CRect rcDI = rcIcon; rcDI.InflateRect(5,5);
	//	CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)m_nID, DISPLAY_ITEM_REGISTRY(CProductsView, ProductGroupExpander), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	pDI->SetMouseOverFeedback();
	//}

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(RGB(40,120,30));

	CString strOut;
	CRect rcText = rcFrame; rcText.left += 10; rcText.bottom = rcText.top + nTextHeight; 
	strOut.LoadString(IDS_PRODUCTS);
	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	rcBox.UnionRect(rcBox, rcText);

	//rcText.left += pDC->GetTextExtent(strOut).cx + 5;
	//CRect rcColors = DrawColorInfo(pDC, rcText, pDisplayList);

	//if ( ! m_strPaper.IsEmpty())
	//{
	//	pDC->SetTextColor(DARKGRAY);
	//	rcText.left = rcColors.right + 5;
	//	strOut = _T(" |  ") + m_strPaper;
	//	pDC->DrawText(strOut, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	//	rcBox.UnionRect(rcBox, rcText);
	//}

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcFrame.left,  rcText.bottom);
	pDC->LineTo(rcFrame.right, rcText.bottom);
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	font.DeleteObject();

	return rcBox;
}

int	CFlatProductList::GetNumUnImposedFormats(float fWidth, float fHeight)
{
	int nNum = 0;
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = GetNext(pos);
		if ( (fabs(rFlatProduct.m_fTrimSizeWidth - fWidth) < 0.01f) && (fabs(rFlatProduct.m_fTrimSizeHeight - fHeight) < 0.01f) )
			if ( ! rFlatProduct.GetFirstPrintSheet())	// unassigned
				nNum++;
	}

	return nNum;
}

CFlatProduct& CFlatProductList::SkipToSuitableUnassignedProduct(float fWidth, float fHeight, int& nFlatProductIndex, int nSkip)
{
	static CFlatProduct nullFlatProduct;

	nFlatProductIndex = 0;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = GetNext(pos);
		if ( (fabs(rFlatProduct.m_fTrimSizeWidth - fWidth) < 0.01f) && (fabs(rFlatProduct.m_fTrimSizeHeight - fHeight) < 0.01f) )
			if (rFlatProduct.m_nFlag == 0)	// unassigned
				nSkip--;

		nFlatProductIndex++;

		if (nSkip == 0)
			return rFlatProduct;
	}

	return nullFlatProduct;
}

void CFlatProductList::ResetFlags()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		GetNext(pos).m_nFlag = 0;
	}
}

CFlatProduct& CFlatProductList::GetProduct(CString strProductID)
{
	static CFlatProduct nullProduct;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rProduct = GetNext(pos);
		if (rProduct.GetProductID() == strProductID)
			return rProduct;
	}

	return nullProduct;
}

void CFlatProductList::GetProductInfo(CJobInfoExchange& rJobInfoExchange)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	POSITION pos = pDoc->m_flatProducts.GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);

		CProductInfo productInfo;
		productInfo.m_nType			= CProductInfo::Flat;
		productInfo.m_strName		= rFlatProduct.m_strProductName;
		productInfo.m_strPartName	= _T("");
		productInfo.m_fWidth		= rFlatProduct.m_fTrimSizeWidth;
		productInfo.m_fHeight		= rFlatProduct.m_fTrimSizeHeight;
		productInfo.m_nNumPages		= rFlatProduct.m_nNumPages;

		rJobInfoExchange.m_productInfoList.AddTail(productInfo);
	}
}
