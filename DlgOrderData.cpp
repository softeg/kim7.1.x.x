// DlgOrderData.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgJobData.h"
#include "DlgOrderData.h"
#include "PrintColorList.h"
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"
#include "OrderDataView.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "GraphicComponent.h"


// CDlgOrderData-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgOrderData, CDialog)

CDlgOrderData::CDlgOrderData(CWnd* pParent /*=NULL*/) : CDialog(CDlgOrderData::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgOrderData)
	m_strODCreator = _T("");
	m_strODCustomer = _T("");
	m_strODCustomerNumber = _T("");
	m_strODJob = _T("");
	m_strODJobNumber = _T("");
	m_strODPhone = _T("");
	m_strODTitle = _T("");
	m_strODNotes = _T("");
	m_strODScheduledToPrint = _T("");
	m_CreationDate = time_t(0);
	m_LastModifiedDate = time_t(0);
	m_scheduledToPrint = time_t(0);
	m_strODNumPagesComment = _T("");
	m_strODQuantityComment = _T("");
	//}}AFX_DATA_INIT

	m_bIsDialog = FALSE;

	m_bInitialized = FALSE;

	m_pButtonOK = NULL;
	m_pButtonCancel = NULL;

	m_smallFont.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CDlgOrderData::~CDlgOrderData()
{
	delete m_pButtonOK;
	delete m_pButtonCancel;
	m_smallFont.DeleteObject();
}


// CDlgOrderData-Meldungshandler

void CDlgOrderData::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgOrderData)
	DDX_Text(pDX, IDC_OD_CREATOR, m_strODCreator);
	DDX_Text(pDX, IDC_OD_CUSTOMER, m_strODCustomer);
	DDX_Text(pDX, IDC_OD_CUSTOMER_NUMBER, m_strODCustomerNumber);
	DDX_Text(pDX, IDC_OD_JOB, m_strODJob);
	DDX_Text(pDX, IDC_OD_JOB_NUMBER, m_strODJobNumber);
	DDX_Text(pDX, IDC_OD_PHONE, m_strODPhone);
	DDX_Text(pDX, IDC_OD_PRINT_DEADLINE, m_strODScheduledToPrint);
	DDX_Text(pDX, IDC_OD_TITLE, m_strODTitle);
	DDX_Text(pDX, IDC_OD_NOTES, m_strODNotes);
	DDX_DateTimeCtrl(pDX, IDC_OD_DATETIMEPICKER, m_scheduledToPrint);
	DDX_Text(pDX, IDC_OD_CREATION_DATE, m_CreationDate);
	DDX_Text(pDX, IDC_OD_LAST_MODIFIED_DATE, m_LastModifiedDate);
	//DDX_Text(pDX, IDC_OD_NUMPAGES_COMMENT, m_strODNumPagesComment);
	//DDX_Text(pDX, IDC_OD_QUANTITY_COMMENT, m_strODQuantityComment);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgOrderData, CDialog)
	//{{AFX_MSG_MAP(CDlgOrderData)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_OD_DATETIMEPICKER, &CDlgOrderData::OnDtnDatetimechangeOdDatetimepicker)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgOrderData 

BOOL CDlgOrderData::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_bInitialized = TRUE;

	UpdateData(FALSE);

	AddOKCancel();

	InitData();

	GetDlgItem(IDC_OD_TITLE)->SetFocus();
	((CEdit*)GetDlgItem(IDC_OD_TITLE))->SetSel(0, -1);

//	((CDlgJobData*)GetParent())->GetDlgItem(IDC_NEXT_BUTTON)->EnableWindow( (m_dlgProductGroupBound.m_nODPages > 0) ? TRUE : FALSE);

	return FALSE;//TRUE;
}

void CDlgOrderData::AddOKCancel()
{
	if (m_bIsDialog)	// we are not inside wizard (stand alone mode)
	{
		CRect rcFrame;
		GetWindowRect(rcFrame); 
		rcFrame.bottom += 40;
		MoveWindow(rcFrame);
		CRect rcBtn(rcFrame.left + 20, rcFrame.bottom - 30, rcFrame.left + 100, rcFrame.bottom - 8);
		ScreenToClient(rcBtn);
		m_pButtonOK = new CButton;
		m_pButtonOK->Create(_T("OK"), WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, rcBtn, this, IDOK);
		m_pButtonOK->SetFont(GetFont());
		rcBtn.OffsetRect(90, 0);
		m_pButtonCancel = new CButton;
		m_pButtonCancel->Create(_T("Abbruch"), WS_VISIBLE | WS_CHILD, rcBtn, this, IDCANCEL);
		m_pButtonCancel->SetFont(GetFont());
	}
}


void CDlgOrderData::InitData()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CGlobalData* pGD		= &pDoc->m_GlobalData;
	m_strODTitle			= pDoc->GetTitle();	
	theApp.RemoveExtension(m_strODTitle);
	m_strODJob				= pGD->m_strJob;
	m_strODJobNumber		= pGD->m_strJobNumber;  
	m_strODCustomer			= pGD->m_strCustomer;      
	m_strODCustomerNumber	= pGD->m_strCustomerNumber;
	m_strODCreator			= pGD->m_strCreator; 
	m_strODPhone			= pGD->m_strPhoneNumber;
	m_CreationDate			= pGD->m_CreationDate;
	m_LastModifiedDate		= pGD->m_LastModifiedDate;
	m_strODScheduledToPrint = pGD->m_strScheduledToPrint;
	m_strODNotes			= pGD->m_strNotes;	 

	if (m_CreationDate.m_dt == 0.0)
		m_CreationDate = COleDateTime::GetCurrentTime();
	if (m_LastModifiedDate.m_dt == 0.0)
		m_LastModifiedDate = COleDateTime::GetCurrentTime();

	m_scheduledToPrint = COleDateTime::GetCurrentTime();

	UpdateData(FALSE);
}

void CDlgOrderData::SaveData()
{
	UpdateData(TRUE);

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CGlobalData* pGD = &pDoc->m_GlobalData;
		pDoc->SetTitle(m_strODTitle);
		pGD->m_strJob				= m_strODJob;
		pGD->m_strJobNumber			= m_strODJobNumber;  
		pGD->m_strCustomer      	= m_strODCustomer;
		pGD->m_strCustomerNumber	= m_strODCustomerNumber; 
		pGD->m_strCreator 			= m_strODCreator; 
		pGD->m_strPhoneNumber		= m_strODPhone;
		pGD->m_CreationDate			= m_CreationDate; 
		pGD->m_LastModifiedDate		= m_LastModifiedDate; 
		pGD->m_strScheduledToPrint	= m_strODScheduledToPrint;
		pGD->m_strNotes	 			= m_strODNotes;

		if (m_CreationDate.m_dt == 0.0)
			m_CreationDate = COleDateTime::GetCurrentTime();
		if (m_LastModifiedDate.m_dt == 0.0)
			m_LastModifiedDate = COleDateTime::GetCurrentTime();

		m_scheduledToPrint = COleDateTime::GetCurrentTime();

		pDoc->SetModifiedFlag();
	}
}

void CDlgOrderData::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CString strOut;
	CRect controlRect;
	GetDlgItem(IDC_OD_HEADER1)->GetWindowRect(controlRect); 
	ScreenToClient(controlRect);
	strOut.LoadString(IDS_GLOBAL);
	CGraphicComponent gp;
	gp.DrawTitleBar(&dc, controlRect, strOut, RGB(203,221,255), RGB(160,180,255), 90);
}

void CDlgOrderData::OnKillfocusOdTitle() 
{
	CString strOldTitle = m_strODTitle;
	UpdateData(TRUE);

	if (strOldTitle != m_strODTitle)
	{
		CImpManDoc*  pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return;
		pDoc->SetTitle(m_strODTitle);
		CString strDrive, strDir, strFname, strExt;
		_tsplitpath((LPCTSTR)pDoc->GetPathName(), strDrive.GetBuffer(_MAX_DRIVE), strDir.GetBuffer(_MAX_DIR), strFname.GetBuffer(_MAX_FNAME), strExt.GetBuffer(_MAX_EXT));
		strDrive.ReleaseBuffer(); strDir.ReleaseBuffer(); strFname.ReleaseBuffer(); strExt.ReleaseBuffer();
		if ( ! strFname.IsEmpty() && ! strExt.IsEmpty() )
		{
			CString strNewPathName;
			strNewPathName.Format(_T("%s%s\\%s%s"), strDrive, strDir, m_strODTitle, strExt);
			pDoc->SetPathName(strNewPathName);
		}
	}
}

HBRUSH CDlgOrderData::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
    if(nCtlColor == CTLCOLOR_STATIC)
    {
		//pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
    }
	else
    {
		UINT nID = pWnd->GetDlgCtrlID();
		switch (nID)
		{
		case IDC_OD_NUMPAGES:	//pDC->SetBkMode(TRANSPARENT);
								pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
								break;
		default:	break;
		}
    }

	return hbr;
}

BOOL CDlgOrderData::PreTranslateMessage(MSG* pMsg)
{
	if (m_bIsDialog)
		return CDialog::PreTranslateMessage(pMsg);
	else
		return CDialog::PreTranslateMessage(pMsg);
}

void CDlgOrderData::OnDtnDatetimechangeOdDatetimepicker(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
//	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);

	UpdateData(TRUE);

	m_strODScheduledToPrint = CPrintSheetView::ConvertToString(m_scheduledToPrint, FALSE);

	UpdateData(FALSE);

	*pResult = 0;
}

