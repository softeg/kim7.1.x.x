#pragma once

#include "OwnerDrawnButtonList.h"
#include "ToolBarGroup.h"


// CPrintSheetOutputToolsView-Formularansicht

class CPrintSheetOutputToolsView : public CFormView
{
	DECLARE_DYNCREATE(CPrintSheetOutputToolsView)

protected:
	CPrintSheetOutputToolsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetOutputToolsView();

public:
	enum { IDD = IDD_PRINTSHEETOUTPUTTOOLSVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	class CNewPrintSheetFrame*	m_pNewPrintSheetFrame;
	COwnerDrawnButtonList		m_zoomToolBar, m_displayToolBar;
	CToolBarGroup				m_zoomToolBarGroup, m_displayToolBarGroup;

public:
	void InitializeToolbars();

public:
	virtual void OnInitialUpdate();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
	afx_msg void OnUpdateZoom(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomDecrease(CCmdUI* pCmdUI);
	afx_msg void OnUpdateZoomFullview(CCmdUI* pCmdUI);
	afx_msg void OnUpdateScrollHand(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewFrontside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBackside(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewBothsides(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFoldsheetCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlateCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateBitmapCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMeasureCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateExposuresCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateReleaseBleed(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDefineTrim(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFoldsheetTurn(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFoldsheetTumble(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLabelrotateClock(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLabelrotateCounterclock(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteSelection(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUndo(CCmdUI* pCmdUI);
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
protected:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
};


