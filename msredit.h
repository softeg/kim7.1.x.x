#if !defined(AFX_MSREDIT_H__5BAE0AEC_DD78_413A_8800_6FF682D3911D__INCLUDED_)
#define AFX_MSREDIT_H__5BAE0AEC_DD78_413A_8800_6FF682D3911D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Mit Microsoft Visual C++ automatisch erstellte IDispatch-Kapselungsklasse(n). 

// HINWEIS: Die Inhalte dieser Datei nicht �ndern. Wenn Microsoft Visual C++
// diese Klasse erneuert, werden Ihre �nderungen �berschrieben.


// Dispatch-Schnittstellen, auf die von dieser Schnittstelle verwiesen wird
class COleFont;

/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CMSREdit 

class CMSREdit : public CWnd
{
protected:
	DECLARE_DYNCREATE(CMSREdit)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0x2f7fc18e, 0x292b, 0x11d2, { 0xa7, 0x95, 0xdf, 0xaa, 0x79, 0x8e, 0x91, 0x48 } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName,
		LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect,
		CWnd* pParentWnd, UINT nID,
		CCreateContext* pContext = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); }

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect, CWnd* pParentWnd, UINT nID,
		CFile* pPersist = NULL, BOOL bStorage = FALSE,
		BSTR bstrLicKey = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); }

// Attribute
public:

// Operationen
public:
	void SetAutoSize(BOOL bNewValue);
	BOOL GetAutoSize();
	void SetBackColor(unsigned long newValue);
	unsigned long GetBackColor();
	void SetBackStyle(long nNewValue);
	long GetBackStyle();
	void SetBorderColor(unsigned long newValue);
	unsigned long GetBorderColor();
	void SetBorderStyle(long nNewValue);
	long GetBorderStyle();
	void SetBorderWidth(long nNewValue);
	long GetBorderWidth();
	void SetRefFont(LPDISPATCH newValue);
	void SetFont(LPDISPATCH newValue);
	COleFont GetFont();
	void SetForeColor(unsigned long newValue);
	unsigned long GetForeColor();
	long GetWindow();
	void SetText(LPCTSTR lpszNewValue);
	CString GetText();
	void SetBorderVisible(BOOL bNewValue);
	BOOL GetBorderVisible();
	void SetAppearance(short nNewValue);
	short GetAppearance();
	long GetSelLength();
	void SetSelLength(long nNewValue);
	long GetSelStart();
	void SetSelStart(long nNewValue);
	short GetRightMargin();
	void SetRightMargin(short nNewValue);
	long GetScrollBars();
	void SetScrollBars(long nNewValue);
	long GetDisableNoScroll();
	void SetDisableNoScroll(long nNewValue);
	CString GetTextRTF();
	void SetTextRTF(LPCTSTR lpszNewValue);
	// Methode 'GetHwnd' wird wegen eines ung�ltigen R�ckgabetyps oder Parametertyps nicht verwendet
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f�gt unmittelbar vor der vorhergehenden Zeile zus�tzliche Deklarationen ein.

#endif // AFX_MSREDIT_H__5BAE0AEC_DD78_413A_8800_6FF682D3911D__INCLUDED_
