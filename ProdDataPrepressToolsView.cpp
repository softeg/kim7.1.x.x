// ProdDataPrepressToolsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "NewPrintSheetFrame.h"
#include "ProdDataPrepressToolsView.h"
#include "PrintSheetView.h"


// CProdDataPrepressToolsView

IMPLEMENT_DYNCREATE(CProdDataPrepressToolsView, CFormView)

CProdDataPrepressToolsView::CProdDataPrepressToolsView()
	: CFormView(CProdDataPrepressToolsView::IDD)
{

}

CProdDataPrepressToolsView::~CProdDataPrepressToolsView()
{
}

void CProdDataPrepressToolsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CProdDataPrepressToolsView, CFormView)
	ON_UPDATE_COMMAND_UI(ID_ZOOM, OnUpdateZoom)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_DECREASE, &CProdDataPrepressToolsView::OnUpdateZoomDecrease)
	ON_UPDATE_COMMAND_UI(ID_ZOOM_FULLVIEW, &CProdDataPrepressToolsView::OnUpdateZoomFullview)
	ON_UPDATE_COMMAND_UI(ID_SCROLLHAND, &CProdDataPrepressToolsView::OnUpdateScrollHand)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FRONTSIDE, &CProdDataPrepressToolsView::OnUpdateViewFrontside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BACKSIDE, &CProdDataPrepressToolsView::OnUpdateViewBackside)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BOTHSIDES, &CProdDataPrepressToolsView::OnUpdateViewBothsides)
	ON_UPDATE_COMMAND_UI(IDC_MEASURECHECK, &CProdDataPrepressToolsView::OnUpdateMeasureCheck)
	ON_UPDATE_COMMAND_UI(IDC_EXPOSURECHECK, &CProdDataPrepressToolsView::OnUpdateExposuresCheck)
	ON_UPDATE_COMMAND_UI(IDC_BITMAPCHECK, &CProdDataPrepressToolsView::OnUpdateBitmapCheck)
	ON_UPDATE_COMMAND_UI(IDC_RELEASE_BLEED, &CProdDataPrepressToolsView::OnUpdateReleaseBleed)
END_MESSAGE_MAP()


// CProdDataPrepressToolsView-Diagnose

#ifdef _DEBUG
void CProdDataPrepressToolsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CProdDataPrepressToolsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CProdDataPrepressToolsView-Meldungshandler

void CProdDataPrepressToolsView::OnInitialUpdate()
{
	m_pNewPrintSheetFrame = (CNewPrintSheetFrame*)GetParentFrame();
	if ( ! m_pNewPrintSheetFrame)
		return;

	m_zoomToolBarGroup.SubclassDlgItem(IDC_PREPRESSTOOL_ZOOM_FRAME,			this);
	m_displayToolBarGroup.SubclassDlgItem(IDC_PREPRESSTOOL_DISPLAY_FRAME,	this);

	CFormView::OnInitialUpdate(); 

	CRect rcWin;
	m_zoomToolBarGroup.GetWindowRect(rcWin); 
	ScreenToClient(rcWin); rcWin.left += 5; rcWin.top += 25;
	CRect rcButton; rcButton = CRect(rcWin.TopLeft(), CSize(40, 30)); 

	CString string;
	string.LoadString(IDS_ZOOM);
	m_zoomToolBarGroup.SetWindowText(string);
	m_zoomToolBar.Create(IDC_PREPRESSTOOL_ZOOM_FRAME, ODBL_VERTICAL, this, GetParentFrame(), NULL, IDB_ZOOM_TOOLBAR, IDB_ZOOM_TOOLBAR_COLD, CSize(40, 26), FALSE);
	m_zoomToolBar.AddButton(_T(""),	_T(""),		ID_ZOOM,			rcButton, 0); rcButton.OffsetRect(  rcButton.Width() + 10, 0);
	m_zoomToolBar.AddButton(_T(""),	_T(""),		ID_ZOOM_DECREASE,	rcButton, 1); rcButton.OffsetRect(-(rcButton.Width() + 10), rcButton.Height() + 5);
	m_zoomToolBar.AddButton(_T(""),	_T(""),		ID_ZOOM_FULLVIEW,	rcButton, 2); rcButton.OffsetRect(  rcButton.Width() + 10, 0);
	m_zoomToolBar.AddButton(_T(""),	_T(""),		ID_SCROLLHAND,		rcButton, 3);


	string.LoadString(IDS_SHOW);
	m_displayToolBarGroup.SetWindowText(string);
	m_displayToolBar.Create(IDC_PREPRESSTOOL_DISPLAY_FRAME, ODBL_VERTICAL, this, GetParentFrame(), NULL, IDB_FOLDPRINTVIEW_TOOLBAR, IDB_FOLDPRINTVIEW_TOOLBAR_COLD, CSize(40, 26), FALSE);
	m_displayToolBar.AddButton(-1, IDS_MEASURING,		IDC_MEASURECHECK,	35, 4);
	m_displayToolBar.AddButton(-1, IDS_MASKING,			IDC_EXPOSURECHECK,	35, 13);
	m_displayToolBar.AddButton(-1, IDS_RELEASE_MASK,	IDC_RELEASE_BLEED,	35, 14);
	m_displayToolBar.AddButton(-1, IDS_CONTENTS,		IDC_BITMAPCHECK,	35, 6);
}

BOOL CProdDataPrepressToolsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	return CFormView::PreTranslateMessage(pMsg);
}

void CProdDataPrepressToolsView::OnUpdateZoom(CCmdUI* pCmdUI) 
{
	if (m_pNewPrintSheetFrame->m_nViewMode == ID_ZOOM)
		COwnerDrawnButtonList::SetCheck(pCmdUI, TRUE);
	else
		COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
}

void CProdDataPrepressToolsView::OnUpdateZoomDecrease(CCmdUI* pCmdUI) 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (m_pNewPrintSheetFrame->m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CProdDataPrepressToolsView::OnUpdateZoomFullview(CCmdUI* pCmdUI) 
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (m_pNewPrintSheetFrame->m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CProdDataPrepressToolsView::OnUpdateScrollHand(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bHandActive);

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if ( ! pView || (m_pNewPrintSheetFrame->m_nViewMode == ID_VIEW_ALL_SHEETS))
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		m_pNewPrintSheetFrame->m_bHandActive = FALSE;
		return;
	}

	CRect clientRect;
	pView->GetClientRect(&clientRect);
    if ( (pView->GetTotalSize().cx <= clientRect.Width()) && (pView->GetTotalSize().cy <= clientRect.Height()) )
	{
		COwnerDrawnButtonList::Enable(pCmdUI, FALSE);
		m_pNewPrintSheetFrame->m_bHandActive = FALSE;
	}
	else
		COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CProdDataPrepressToolsView::OnUpdateViewFrontside(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_nViewSide == FRONTSIDE);
}

void CProdDataPrepressToolsView::OnUpdateViewBackside(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_nViewSide == BACKSIDE);
}

void CProdDataPrepressToolsView::OnUpdateViewBothsides(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_nViewSide == BOTHSIDES);
}

void CProdDataPrepressToolsView::OnUpdateMeasureCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowMeasure);
}

void CProdDataPrepressToolsView::OnUpdateExposuresCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowExposures);
//	pCmdUI->Enable(m_bEnableExposuresButton);
}

void CProdDataPrepressToolsView::OnUpdateBitmapCheck(CCmdUI* pCmdUI)
{
	COwnerDrawnButtonList::SetCheck(pCmdUI, m_pNewPrintSheetFrame->m_bShowBitmap);
}

void CProdDataPrepressToolsView::OnUpdateReleaseBleed(CCmdUI* pCmdUI) 
{
	COwnerDrawnButtonList::Enable(pCmdUI, m_pNewPrintSheetFrame->m_bEnableReleaseBleedButton);
}
