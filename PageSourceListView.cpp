// PageSourceListView.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PageSourceListView.h"
#include "PageListView.h"
#include "PageListFrame.h"
#include "DlgPageSourceProps.h"
#include "MainFrm.h"
#include "PrintSheetView.h"
#include "PageListTopToolsView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPageSourceListView

IMPLEMENT_DYNCREATE(CPageSourceListView, CScrollView)

CPageSourceListView::CPageSourceListView()
{
	m_DisplayList.Create(this, TRUE);	// TRUE = OLE DragDrop
}

CPageSourceListView::~CPageSourceListView()
{
}
	   

BEGIN_MESSAGE_MAP(CPageSourceListView, CScrollView)
	//{{AFX_MSG_MAP(CPageSourceListView)
	ON_WM_LBUTTONDOWN()
	ON_WM_CONTEXTMENU()
	ON_WM_KEYDOWN()
	ON_WM_SIZE()
	ON_WM_RBUTTONDOWN()
	ON_WM_SETCURSOR()
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPageSourceListView drawing

void CPageSourceListView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CalcPageSourceListSize();
	SetScrollSizes(MM_TEXT, m_PageSourceListSize);

	m_bInitialUpdate = TRUE;
}

void CPageSourceListView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/) 
{
	CalcPageSourceListSize();
	SetScrollSizes(MM_TEXT, m_PageSourceListSize);

	Invalidate();	
}

void CPageSourceListView::OnSize(UINT nType, int cx, int cy) 
{
	CScrollView::OnSize(nType, cx, cy);
	
	CalcPageSourceListSize();
	SetScrollSizes(MM_TEXT, m_PageSourceListSize);
}

void CPageSourceListView::CalcPageSourceListSize()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CRect rcBox = Draw(&dc, TRUE);

	dc.DeleteDC();

	m_PageSourceListSize = rcBox.Size() + CSize(0, 20);
}

int CPageSourceListView::GetCurProductPartIndex()
{
	CPageSourceListView* pView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));	// this function needs to be static -> get view 
	if (pView)
	{
		CPageListFrame* pFrame = (CPageListFrame*)pView->GetParentFrame();
		if (pFrame)
			return pFrame->GetActProductPartIndex();
		else
			return 0;
	}
	else
		return 0;
}

void CPageSourceListView::OnDraw(CDC* pDC)
{
	m_DisplayList.BeginRegisterItems(pDC);

	Draw(pDC);

	m_DisplayList.EndRegisterItems(pDC);

	if (m_bInitialUpdate)	// if first OnDraw, find and select links to eventually selected page templates
	{
		CPageListView*  pPageListView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
		if (!pPageListView)
			return;

		CDisplayItem* pDIScrollTo = NULL;
		CDisplayItem* pDISelect	  = pPageListView->m_DisplayList.GetFirstSelectedItem();
		while (pDISelect)
		{
			CPageTemplate* pTemplate = (CPageTemplate*)pDISelect->m_pData;
			for (int i = 0; i < pTemplate->m_ColorSeparations.GetSize(); i++)
			{
				CPageSourceHeader* pPageSourceHeader = pTemplate->GetObjectSourceHeader(pTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex);
				if (pPageSourceHeader)
					pDISelect = m_DisplayList.GetItemFromData(pPageSourceHeader);

				if (pDISelect)
				{
					m_DisplayList.SelectItem(*pDISelect);
					m_DisplayList.InvalidateItem(pDISelect);

					if (!pDIScrollTo)
						pDIScrollTo = pDISelect;
				}
			}
			pDISelect = pPageListView->m_DisplayList.GetNextSelectedItem(pDISelect);
		}

		if (pDIScrollTo)
		{
			if ( ! pDIScrollTo->m_bIsVisible)	
			{
				Invalidate();
				OnVScroll(SB_THUMBTRACK, pDIScrollTo->m_BoundingRect.top - 20, NULL);
			}
		}

		m_bInitialUpdate = FALSE;
	}
}

CRect CPageSourceListView::Draw(CDC* pDC, BOOL bGetBoxOnly)
{
	CFont font, fontBold;
	font.CreateFont	   (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	fontBold.CreateFont(13, 0, 0, 0, FW_BOLD,   FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont*	 pOldFont		 = (CFont*)pDC->SelectObject(&font);
	COLORREF crOldTextColor  = pDC->SetTextColor(BLACK);
	COLORREF crOldBkColor	 = pDC->GetBkColor();
	int		 nOldBkMode		 = pDC->SetBkMode(TRANSPARENT);

	HICON hIcon	= AfxGetApp()->LoadIcon(IDI_LINK);

	CSize textExtLabel, textExtPath;
	CRect textRect(5, 5, 5, 5);
	int	  nIndex = 0;
	CRect clientRect;
	GetClientRect(clientRect);

	CRect rcBox = textRect;

	CString	strPath;
	CString strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);

	int			nProductPartIndex = GetCurProductPartIndex();
	CImpManDoc* pDoc			  = GetDocument();
	POSITION	pos				  = pDoc->m_PageSourceList.GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = pDoc->m_PageSourceList.GetNext(pos);
		//if ( ! rPageSource.IsMemberOfProductGroup(nProductPartIndex))
		//	continue;
		strPath					 = (rPageSource.m_strDocumentFullpath.IsEmpty()) ? strUnknown : "  " + rPageSource.m_strDocumentFullpath;

		CString strSourceLabel;
		switch (rPageSource.m_nType) 
		{
		case CPageSource::PDFFile:	strSourceLabel.LoadString(IDS_PDFSOURCE_LABEL);		break;
		case CPageSource::PSFile:	strSourceLabel.LoadString(IDS_PSSOURCE_LABEL);		break;
		case CPageSource::Bitmap:	strSourceLabel.LoadString(IDS_BITMAPSOURCE_LABEL);	break;
		}

		pDC->SelectObject(&font);
		textExtLabel	= pDC->GetTextExtent(strSourceLabel);
		pDC->SelectObject(&fontBold);
		textExtPath		= pDC->GetTextExtent(strPath);

		textRect.right	= textRect.left + textExtLabel.cx + textExtPath.cx;
		textRect.bottom	= textRect.top  + textExtPath.cy;

		pDC->SetTextColor(BLACK);
		pDC->SelectObject(&font);
		pDC->SetBkMode(OPAQUE);
		pDC->SetBkColor(PALEGREEN);
		pDC->TextOut(textRect.left, textRect.top, strSourceLabel);

		rcBox.UnionRect(rcBox, textRect);

		pDC->SelectObject(&fontBold);
		pDC->SetBkMode(TRANSPARENT); 

		if ( ! rPageSource.DocumentExists())
			pDC->SetTextColor(MIDDLEGRAY);

		pDC->TextOut(textRect.left + textExtLabel.cx, textRect.top, strPath);
		pDC->SelectObject(&font);

		if ( ! bGetBoxOnly)
			m_DisplayList.RegisterItem(pDC, textRect, textRect, (void*)&rPageSource, DISPLAY_ITEM_REGISTRY(CPageSourceListView, PageSource), NULL, CDisplayItem::ForceRegister, NULL);

		CRect	headerRect(textRect.left, textRect.bottom + 15, textRect.left + 55, textRect.bottom + 80);
		CString string;
		int		nCurPageNum = 0;
		for (int i = 0; i < rPageSource.m_PageSourceHeaders.GetSize(); i++)
		{
			CPageSourceHeader& rPageSourceHeader = rPageSource.m_PageSourceHeaders[i];

			BOOL bLastSep = FALSE;

			if (i < rPageSource.m_PageSourceHeaders.GetSize() - 1)
			{
				if (rPageSourceHeader.m_nPageNumber != rPageSource.m_PageSourceHeaders[i + 1].m_nPageNumber)
					bLastSep = TRUE;
			}
			else
				bLastSep = TRUE;

			if (rPageSourceHeader.m_nPageNumber != nCurPageNum)
				headerRect.OffsetRect(50, 0);

			nCurPageNum = rPageSourceHeader.m_nPageNumber;

			CPageTemplate* pTemplate = rPageSourceHeader.GetFirstPageTemplate();

			CRect rcBBox = DrawPagePreview(pDC, headerRect, rPageSource, rPageSourceHeader, (pTemplate) ? FALSE : TRUE, bGetBoxOnly);
			rcBox.UnionRect(rcBox, rcBBox);

			if (bLastSep)
			{
				COLORREF crText = pDC->GetTextColor();
				if ( ! pTemplate)
					pDC->SetTextColor(LIGHTGRAY);
				string.Format(_T("%d"), rPageSourceHeader.m_nPageNumber);//nCurPageNum);
				CRect rcText = rcBBox;
				rcText.bottom += 15;
				pDC->DrawText(string, rcText, DT_SINGLELINE | DT_CENTER | DT_BOTTOM);
				pDC->SetTextColor(crText);
				rcBox.UnionRect(rcBox, rcText);
			}

			if (rPageSourceHeader.m_rgb != WHITE)
			{
				CBrush colorBrush(rPageSourceHeader.m_rgb);
				CRect  colorRect(headerRect.left, headerRect.top, headerRect.left + 8, headerRect.top + 8);
				CBrush* pOldBrush = (CBrush*)pDC->SelectObject(&colorBrush);
				CPen*	pOldPen	  = (CPen*)  pDC->SelectStockObject(BLACK_PEN);
				pDC->Rectangle(colorRect);
				pDC->SelectObject(pOldBrush);
				pDC->SelectObject(pOldPen);
				colorBrush.DeleteObject();
			}
			else
			{
				CRect rcColorants = headerRect;
				rcColorants.bottom = rcColorants.top + 9;
				CRect rcColorBox = DrawColorants(pDC, rcColorants, rPageSourceHeader);
				if ( ! pTemplate)
					CPrintSheetView::FillTransparent(pDC, CRect(rcColorBox.TopLeft(), CSize(rcColorBox.Width() + 1, rcColorBox.Height() + 1)), WHITE, TRUE, 150);
			}

			if (pTemplate)
				pDC->DrawIcon(rcBBox.BottomRight() - CSize(10, 10), hIcon);
			//else
			//	CPrintSheetView::FillTransparent(pDC, CRect(rcBBox.TopLeft(), CSize(rcBBox.Width() + 1, rcBBox.Height() + 1)), LIGHTGRAY, TRUE, 150);

			CRect registerRect = (bLastSep) ? rcBBox : CRect(rcBBox.left, rcBBox.top, rcBBox.left + 9, rcBBox.bottom);
			CRect linkRect	   = (bLastSep) ? CRect(rcBBox.BottomRight() - CSize(12, 12), rcBBox.BottomRight()) : CRect(0,0,0,0);

			if ( ! bGetBoxOnly)
				m_DisplayList.RegisterItem(pDC, registerRect, linkRect, (void*)&rPageSourceHeader, DISPLAY_ITEM_REGISTRY(CPageSourceListView, PageSourceHeader), (void*)&rPageSource, CDisplayItem::ForceRegister, NULL);

			headerRect.OffsetRect(9, 0);
			if (headerRect.right >= clientRect.right - 40)
				headerRect.OffsetRect(-(headerRect.left - clientRect.left - 5), rcBox.bottom - headerRect.top + 20);
		}

		textRect.top = rcBox.bottom + 35;// headerRect.bottom + 35;//10;
		nIndex++;
	}
	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(crOldTextColor);    
	pDC->SetBkColor	 (crOldBkColor);    
	pDC->SetBkMode	 (nOldBkMode);
	
	font.DeleteObject();
	fontBold.DeleteObject();

	return rcBox;
}

CRect CPageSourceListView::DrawPagePreview(CDC* pDC, CRect rcFrame, CPageSource& rPageSource, CPageSourceHeader& rPageSourceHeader, BOOL bDimmed, BOOL bGetBoxOnly)
{
	CRect previewFrameRect = rcFrame;

	CRect outBBox, trimBox;
	outBBox.left   = 0; 
	outBBox.top    = 0; 
	outBBox.right  = (int)((rPageSourceHeader.m_fBBoxRight - rPageSourceHeader.m_fBBoxLeft)   / PICA_POINTS);
	outBBox.bottom = (int)((rPageSourceHeader.m_fBBoxTop   - rPageSourceHeader.m_fBBoxBottom) / PICA_POINTS); 

	trimBox.left   = (int)((rPageSourceHeader.m_fTrimBoxLeft - rPageSourceHeader.m_fBBoxLeft)	  / PICA_POINTS);
	trimBox.top	   = (int)((rPageSourceHeader.m_fBBoxTop	 - rPageSourceHeader.m_fTrimBoxTop) / PICA_POINTS);
	trimBox.right  = trimBox.left + (int)((rPageSourceHeader.m_fTrimBoxRight - rPageSourceHeader.m_fTrimBoxLeft)   / PICA_POINTS);	  
	trimBox.bottom = trimBox.top  + (int)((rPageSourceHeader.m_fTrimBoxTop   - rPageSourceHeader.m_fTrimBoxBottom) / PICA_POINTS); 

	float fScale;
	if ((float)outBBox.Height() / (float)outBBox.Width() >= 
		(float)previewFrameRect.Height() / (float)previewFrameRect.Width())
		fScale = (float)outBBox.Height() / (float)previewFrameRect.Height();
	else
		fScale = (float)outBBox.Width()  / (float)previewFrameRect.Width();
	outBBox.left   = (int)((float)outBBox.left   / fScale + 0.5f); 
	outBBox.bottom = (int)((float)outBBox.bottom / fScale + 0.5f); 
	outBBox.right  = (int)((float)outBBox.right  / fScale + 0.5f); 
	outBBox.top    = (int)((float)outBBox.top    / fScale + 0.5f); 
	trimBox.left   = (int)((float)trimBox.left   / fScale + 0.5f); 
	trimBox.bottom = (int)((float)trimBox.bottom / fScale + 0.5f); 
	trimBox.right  = (int)((float)trimBox.right  / fScale + 0.5f); 
	trimBox.top    = (int)((float)trimBox.top    / fScale + 0.5f); 
	outBBox += previewFrameRect.TopLeft();
	trimBox += previewFrameRect.TopLeft();

	if (bGetBoxOnly)
		return outBBox;
	if ( ! pDC->RectVisible(outBBox))
		return outBBox;

	CPen*	pOldPen	  = (CPen*)	 pDC->SelectStockObject(NULL_PEN);
	CBrush*	pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);

	CString strPreviewFile;
	int nHeaderIndex = rPageSource.GetHeaderIndex(&rPageSourceHeader);
	strPreviewFile = rPageSource.GetPreviewFileName(nHeaderIndex + 1);

	CPageListView::DrawPreview(pDC, outBBox, TOP, 0.0f, strPreviewFile, FALSE, bDimmed, &rPageSourceHeader.m_pThumbnailImage);

	CPen pen(PS_SOLID, 1, (bDimmed) ? WHITE : MIDDLEGRAY);
	pDC->SelectObject(&pen);
	pDC->Rectangle(CRect(outBBox.TopLeft(), CSize(outBBox.Width() + 1, outBBox.Height() + 1)));
	pen.DeleteObject();
	if (rPageSourceHeader.m_bOutputBlocked)
	{
		pen.CreatePen(PS_SOLID, 3, RED);
		pDC->SelectObject(&pen);
		pDC->MoveTo(outBBox.left  - 3, outBBox.bottom + 3);
		pDC->LineTo(outBBox.right + 3, outBBox.top    - 3);
		pen.DeleteObject();
	}

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);

	return outBBox;
}


CRect CPageSourceListView::DrawColorants(CDC* pDC, CRect rcRect, CPageSourceHeader& rPageSourceHeader)
{
	CRect rcBox(rcRect.TopLeft(), CSize(1, 1));

	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CSize size(6, 6);
	pDC->DPtoLP(&size);
	CRect rcIcon(rcRect.left, rcRect.bottom - size.cy, rcRect.left + size.cx, rcRect.bottom);
	rcIcon.OffsetRect(0, -size.cy/2);

	CPen pen(PS_SOLID, 1, MIDDLEGRAY);
	CBrush brush;
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CBrush* pOldBrush = pDC->GetCurrentBrush();

	for (int j = 0; j < rPageSourceHeader.m_processColors.GetSize(); j++)
	{
		if (theApp.m_colorDefTable.IsCyan(rPageSourceHeader.m_processColors[j]))
			brush.CreateSolidBrush(CYAN);		
		else
			if (theApp.m_colorDefTable.IsMagenta(rPageSourceHeader.m_processColors[j]))
				brush.CreateSolidBrush(MAGENTA);	
			else
				if (theApp.m_colorDefTable.IsYellow(rPageSourceHeader.m_processColors[j]))
					brush.CreateSolidBrush(YELLOW);		
				else
					if (theApp.m_colorDefTable.IsKey(rPageSourceHeader.m_processColors[j]))
						brush.CreateSolidBrush(BLACK);		
					else
						brush.CreateSolidBrush(WHITE);		

		pDC->SelectObject(&brush);
		pDC->Rectangle(rcIcon);
		rcBox.UnionRect(rcBox, rcIcon);
		brush.DeleteObject();
		rcIcon.OffsetRect(rcIcon.Width(), 0);
	}

	for (int j = 0; j < rPageSourceHeader.m_spotColors.GetSize(); j++)
	{
		//COLORREF rcColor = pDoc->m_ColorDefinitionTable.FindColorRef(rPageSourceHeader.m_spotColors[j]);
		COLORREF rcColor = pDoc->m_PageSourceList.GetAlternateRGB(rPageSourceHeader.m_spotColors[j]);
		brush.CreateSolidBrush(rcColor);
		pDC->SelectObject(&brush);
		pDC->Rectangle(rcIcon);
		rcBox.UnionRect(rcBox, rcIcon);
		brush.DeleteObject();
		rcIcon.OffsetRect(rcIcon.Width(), 0);
	}

	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();

	return rcBox;
}


/////////////////////////////////////////////////////////////////////////////
// CPageSourceListView diagnostics

#ifdef _DEBUG
void CPageSourceListView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CPageSourceListView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CImpManDoc* CPageSourceListView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CImpManDoc)));
	return (CImpManDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPageSourceListView message handlers

void CPageSourceListView::OnContextMenu(CWnd* /*pWnd*/, CPoint point) 
{
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_PAGELIST_RIGHTVIEWS_POPUP));

	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);

	CWnd* pWndPopupOwner = this;
	while (pWndPopupOwner->GetStyle() & WS_CHILD)
		pWndPopupOwner = pWndPopupOwner->GetParent();

	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
}



//////////////////////////////////////////////////////////////////////////////////////////////////
// display list message handlers

void CPageSourceListView::OnLButtonDown(UINT /*nFlags*/, CPoint point) 
{
	// First of all deselect items in counterpart pane
	CPageListView* pView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pView)
	{
		pView->m_DisplayList.DeselectAll();			
		pView->m_DisplayList.InvalidateItem(NULL);
		pView->UpdateWindow();
	}
	//////////////////////////////////////////////////

	m_DisplayList.OnLButtonDown(point); // pass message to display list

	CPageListTopToolsView* pTopToolsView = (CPageListTopToolsView*)theApp.GetView(RUNTIME_CLASS(CPageListTopToolsView));
	if (pTopToolsView)
		pTopToolsView->UpdateDialogControls(pTopToolsView, TRUE);
}

void CPageSourceListView::OnRButtonDown(UINT /*nFlags*/, CPoint point) 
{
	// First of all deselect items in counterpart pane
	CPageListView* pView = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	if (pView)
	{
		pView->m_DisplayList.DeselectAll();			
		pView->m_DisplayList.InvalidateItem(NULL);
		pView->UpdateWindow();
	}
	//////////////////////////////////////////////////
	m_DisplayList.OnLButtonDown(point, TRUE);
}

BOOL g_bLockSelection = FALSE;

BOOL CPageSourceListView::OnSelectPageSource(CDisplayItem* pDI, CPoint /*point*/, LPARAM) 
{
	CPageSource* pPageSource = (CPageSource*)pDI->m_pData;

	g_bLockSelection = TRUE;
	for (int i = 0; i < pPageSource->m_PageSourceHeaders.GetSize(); i++)
	{
		CDisplayItem* pDI = m_DisplayList.GetItemFromData(&pPageSource->m_PageSourceHeaders[i], (void*)pPageSource,
														  DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader));
		if (pDI)
			m_DisplayList.SelectItem(*pDI);
	}
	g_bLockSelection = FALSE;

	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bDeleteSelectionMenuActive	  = TRUE;
	pParentFrame->m_bAssigncolorsPsfileMenuActive = TRUE;
	pParentFrame->m_bPagesourcePropsMenuActive	  = FALSE;
	pParentFrame->m_bRipMessagesMenuActive		  = TRUE;
	pParentFrame->m_bShowLinkMenuActive			  = TRUE;

	return TRUE;
}

BOOL CPageSourceListView::OnDeselectPageSource(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM) 
{
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bDeleteSelectionMenuActive	  = FALSE;
	pParentFrame->m_bAssigncolorsPsfileMenuActive = FALSE;
	pParentFrame->m_bPagesourcePropsMenuActive	  = FALSE;
	pParentFrame->m_bRipMessagesMenuActive		  = FALSE;
	pParentFrame->m_bShowLinkMenuActive			  = FALSE;

	return TRUE;
}

#ifdef POSTSCRIPT
extern BOOL bLszPsRipIsRunning;
#endif

BOOL CPageSourceListView::OnActivatePageSource(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM) 
{
	return TRUE;
}

BOOL CPageSourceListView::OnSelectPageSourceHeader(CDisplayItem* pDI, CPoint point, LPARAM) 
{
	if (g_bLockSelection)
		return FALSE;

	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bAssigncolorsPsfileMenuActive = TRUE;
	pParentFrame->m_bPagesourcePropsMenuActive	  = TRUE;
	pParentFrame->m_bShowLinkMenuActive			  = TRUE;

	if (!pDI->m_AuxRect.PtInRect(point))	// clicked into link symbol?
		return FALSE;

	CPageListView* pView	   = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
	CPageTemplate* pTemplate   = ((CPageSourceHeader*)pDI->m_pData)->GetFirstPageTemplate();
	CDisplayItem*  pDIScrollTo = NULL;
	while (pTemplate && pView)
	{
		CDisplayItem* pDITemplate = pView->m_DisplayList.GetItemFromData(pTemplate);
		if (pDITemplate)
		{
			pView->m_DisplayList.SelectItem(*pDITemplate);
			pView->m_DisplayList.InvalidateItem(pDITemplate);
			if (!pDIScrollTo)
				pDIScrollTo = pDITemplate;
		}
		pTemplate = ((CPageSourceHeader*)pDI->m_pData)->GetNextPageTemplate(pTemplate);
	}

	if (pDIScrollTo)
	{
		if ( ! pDIScrollTo->m_bIsVisible)	
		{
			pView->Invalidate();
			CPoint ScrollPos = pView->GetScrollPosition();
			pView->ScrollToPosition(CPoint(0, ScrollPos.y + pDIScrollTo->m_BoundingRect.top - 20));
		}
	}

	return TRUE;
}

BOOL CPageSourceListView::OnDeselectPageSourceHeader(CDisplayItem* /*pDI*/, CPoint /*point*/, LPARAM) 
{
	CPageListFrame* pParentFrame = (CPageListFrame*)GetParentFrame();
	pParentFrame->m_bDeleteSelectionMenuActive	  = FALSE;
	pParentFrame->m_bAssigncolorsPsfileMenuActive = FALSE;
	pParentFrame->m_bPagesourcePropsMenuActive	  = FALSE;
	pParentFrame->m_bShowLinkMenuActive			  = FALSE;

	return TRUE;
}

BOOL CPageSourceListView::OnActivatePageSourceHeader(CDisplayItem* pDI, CPoint point, LPARAM) 
{
	if (pDI->m_AuxRect.PtInRect(point))
	{
		CPageListView* pView	 = (CPageListView*)theApp.GetView(RUNTIME_CLASS(CPageListView));
		CPageTemplate* pTemplate = ((CPageSourceHeader*)pDI->m_pData)->GetFirstPageTemplate();
		if (pTemplate && pView)
		{
			CDisplayItem* pDITemplate = pView->m_DisplayList.GetItemFromData(pTemplate);
			if (pDITemplate)
			{
				pView->m_DisplayList.SelectItem(*pDITemplate);
				pView->m_DisplayList.InvalidateItem(NULL);
			}
		}
	}

	CDlgPageSourceProps dlg;
	dlg.DoModal();

	return TRUE;
}

void CPageSourceListView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
	{
	case VK_HOME	: OnVScroll(SB_TOP, 0, NULL);
					  OnHScroll(SB_LEFT, 0, NULL);
					  break;
	case VK_END		: OnVScroll(SB_BOTTOM, 0, NULL);
					  OnHScroll(SB_RIGHT, 0, NULL);
					  break;
	case VK_UP		: OnVScroll(SB_LINEUP, 0, NULL);
					  break;
	case VK_DOWN	: OnVScroll(SB_LINEDOWN, 0, NULL);
					  break;
	case VK_PRIOR	: OnVScroll(SB_PAGEUP, 0, NULL);
					  break;
	case VK_NEXT	: OnVScroll(SB_PAGEDOWN, 0, NULL);
					  break;
	case VK_LEFT	: OnHScroll(SB_LINELEFT, 0, NULL);
					  break;
	case VK_RIGHT	: OnHScroll(SB_LINERIGHT, 0, NULL);
					  break;
	default			: break;
	}
	
	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CPageSourceListView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
	// The default call of OnMouseWheel causes an assertion or doesn't work
	// So we take the direct call to DoMouseWheel
	
	CScrollView::DoMouseWheel(nFlags, zDelta, pt);
	return TRUE; // Always return TRUE -> otherwise you get an assertion
}

void CPageSourceListView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

BOOL CPageSourceListView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// 2nd parameter must be 0 (means no standard-cursor will be registered), 
	// if we want draw out own cursors
	CBrush* pBkgrdBrush = new CBrush;
	pBkgrdBrush->CreateSolidBrush(TABLECOLOR_BACKGROUND_DARK);

	lpszClassName = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, 0, (HBRUSH)(*pBkgrdBrush), 0);

	BOOL ret = CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);

	pBkgrdBrush->Detach();
	delete pBkgrdBrush;

	return ret;
}

BOOL CPageSourceListView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	SetCursor(theApp.m_CursorStandard);
	
	return CScrollView::OnSetCursor(pWnd, nHitTest, message);
}

void CPageSourceListView::PostNcDestroy() 
{
	// Free the C++ class.
	delete this;
}
