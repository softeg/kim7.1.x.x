#include "stdafx.h"
#include "Imposition Manager.h"
#include "PrintJobDocumentation.h"
#include "JobDocumentationSettingsView.h"
#include "JobDocumentationView.h"
#include "PrintSheetComponentsView.h"


#define IN2MM(in) ( (int)((float)in * 25.4f * 100.0f) )


CPrintJobDocumentation::CPrintJobDocumentation(void)
{
	m_nPrinterDpiX    = 0;
	m_nPrinterDpiY    = 0;
	m_nPrinterPixelsX = 0;
	m_nPrinterPixelsY = 0;
	m_nPrinterOffsetX = 0;
	m_nPrinterOffsetY = 0;
	m_nPrinterSizeX	  = 0;
	m_nPrinterSizeY   = 0;
	m_sizePrintPage	  = CSize(21000,29700);
	m_nPageOrientation = PORTRAIT;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	long ret = RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);

	HGLOBAL hDevMode = GlobalAlloc(GHND, sizeof(DEVMODE));
	DEVMODE* pDevMode = (DEVMODE*)::GlobalLock(hDevMode);
	DWORD dwSize = sizeof(DEVMODE);
	if (RegQueryValueEx(hKey, _T("PrintDevMode"), 0, NULL, (LPBYTE)pDevMode, &dwSize) == ERROR_SUCCESS)
	{
		m_sizePrintPage.cx = pDevMode->dmPaperWidth  * 10;
		m_sizePrintPage.cy = pDevMode->dmPaperLength * 10;
		if ( (m_sizePrintPage.cx == 0) || (m_sizePrintPage.cy == 0) )
			if (pDevMode->dmPaperSize != 0)
			{
				switch (pDevMode->dmPaperSize)
				{
				case DMPAPER_LETTER:		m_sizePrintPage.cx = IN2MM(8.5);	m_sizePrintPage.cy = IN2MM(11);		break;	/* Letter 8 1/2 x 11 in               */
				case DMPAPER_LETTERSMALL:	m_sizePrintPage.cx = IN2MM(8.5);	m_sizePrintPage.cy = IN2MM(11);		break;	/* Letter Small 8 1/2 x 11 in         */
				case DMPAPER_TABLOID:		m_sizePrintPage.cx = IN2MM(11);		m_sizePrintPage.cy = IN2MM(17);		break;	/* Tabloid 11 x 17 in                 */
				case DMPAPER_LEDGER:		m_sizePrintPage.cx = IN2MM(17);		m_sizePrintPage.cy = IN2MM(11);		break;	/* Ledger 17 x 11 in                  */
				case DMPAPER_LEGAL:			m_sizePrintPage.cx = IN2MM(8.5);	m_sizePrintPage.cy = IN2MM(14);		break;	/* Legal 8 1/2 x 14 in                */
				case DMPAPER_STATEMENT:		m_sizePrintPage.cx = IN2MM(5.5);	m_sizePrintPage.cy = IN2MM(8.5);	break;	/* Statement 5 1/2 x 8 1/2 in         */
				case DMPAPER_EXECUTIVE:		m_sizePrintPage.cx = IN2MM(7.25);	m_sizePrintPage.cy = IN2MM(10.5);	break;	/* Executive 7 1/4 x 10 1/2 in        */
				case DMPAPER_A3:			m_sizePrintPage.cx = 29700;			m_sizePrintPage.cy = 42000;			break;	/* A3 297 x 420 mm                    */
				case DMPAPER_A4:			m_sizePrintPage.cx = 21000; 		m_sizePrintPage.cy = 29700; 		break;	/* A4 210 x 297 mm                    */
				case DMPAPER_A4SMALL:		m_sizePrintPage.cx = 21000; 		m_sizePrintPage.cy = 29700; 		break;	/* A4 Small 210 x 297 mm              */
				case DMPAPER_A5:			m_sizePrintPage.cx = 14800; 		m_sizePrintPage.cy = 21000; 		break;	/* A5 148 x 210 mm                    */
				case DMPAPER_B4:			m_sizePrintPage.cx = 25000; 		m_sizePrintPage.cy = 35400; 		break;	/* B4 (JIS) 250 x 354                 */
				case DMPAPER_B5:			m_sizePrintPage.cx = 18200; 		m_sizePrintPage.cy = 25700;			break;	/* B5 (JIS) 182 x 257 mm              */
				case DMPAPER_FOLIO:			m_sizePrintPage.cx = IN2MM(8.5); 	m_sizePrintPage.cy = IN2MM(13);		break;	/* Folio 8 1/2 x 13 in                */
				case DMPAPER_QUARTO:		m_sizePrintPage.cx = 21500; 		m_sizePrintPage.cy = 27500;			break;	/* Quarto 215 x 275 mm                */
				case DMPAPER_10X14:			m_sizePrintPage.cx = IN2MM(10); 	m_sizePrintPage.cy = IN2MM(14); 	break;	/* 10x14 in                           */
				case DMPAPER_11X17:			m_sizePrintPage.cx = IN2MM(11); 	m_sizePrintPage.cy = IN2MM(17); 	break;	/* 11x17 in                           */
				case DMPAPER_NOTE:			m_sizePrintPage.cx = IN2MM(8.5);	m_sizePrintPage.cy = IN2MM(11); 	break;	/* Note 8 1/2 x 11 in                 */
				case DMPAPER_9X11:			m_sizePrintPage.cx = IN2MM(9); 		m_sizePrintPage.cy = IN2MM(11); 	break;	/* 9 x 11 in                          */
				case DMPAPER_10X11:			m_sizePrintPage.cx = IN2MM(10);		m_sizePrintPage.cy = IN2MM(11); 	break;	/* 10 x 11 in                         */
				case DMPAPER_15X11:			m_sizePrintPage.cx = IN2MM(15);		m_sizePrintPage.cy = IN2MM(11); 	break;	/* 15 x 11 in                         */
				default:					m_sizePrintPage.cx = 21000;			m_sizePrintPage.cy = 29700;			break;	/* A4 210 x 297 mm                    */
				}
			}	
			else
				m_sizePrintPage	= CSize(21000,29700);

		m_nPageOrientation = (pDevMode->dmOrientation == DMORIENT_PORTRAIT) ? PORTRAIT : LANDSCAPE;
		if (m_nPageOrientation == LANDSCAPE)
		{
			int nTemp = m_sizePrintPage.cx; m_sizePrintPage.cx = m_sizePrintPage.cy; m_sizePrintPage.cy = nTemp;
		}
	}
	::GlobalUnlock(hDevMode);

	TCHAR szValue[100];
	dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_stscanf(szValue, _T("%ld %ld %ld %ld"), &m_rcPrintMargins.left, &m_rcPrintMargins.top, &m_rcPrintMargins.right, &m_rcPrintMargins.bottom);
	else
		m_rcPrintMargins = CRect(2500, 2500, 2500, 2500);

	RegCloseKey(hKey);
}

CPrintJobDocumentation::~CPrintJobDocumentation(void)
{
}

void CPrintJobDocumentation::GetPrinterCaps(CDC* pDC)
{
	m_nPrinterDpiX = pDC->GetDeviceCaps(LOGPIXELSX);
	m_nPrinterDpiY = pDC->GetDeviceCaps(LOGPIXELSY);
	m_nPrinterPixelsX = pDC->GetDeviceCaps(HORZRES);
	m_nPrinterPixelsY = pDC->GetDeviceCaps(VERTRES);
	m_nPrinterOffsetX = pDC->GetDeviceCaps(PHYSICALOFFSETX);
	m_nPrinterOffsetY = pDC->GetDeviceCaps(PHYSICALOFFSETY);
	m_nPrinterSizeX = pDC->GetDeviceCaps(PHYSICALWIDTH);
	m_nPrinterSizeY = pDC->GetDeviceCaps(PHYSICALHEIGHT);
}

CSize CPrintJobDocumentation::GetPageSize(CDC* pDC)
{
	if (pDC)
	{
		GetPrinterCaps(pDC);
		// Calculate the size of the page in logical pixels
		double dSizeX = (double)m_nPrinterSizeX;// / (double)m_nPrinterDpiX * 2540.0;
		double dSizeY = (double)m_nPrinterSizeY;// / (double)m_nPrinterDpiY * 2540.0;
		return CSize((long)dSizeX, (long)dSizeY);
	}

	return m_sizePrintPage;
}

CSize CPrintJobDocumentation::Initialize(CDC* /*pDC*/)
{
	// Set the resolution to 100 pixels per milimeter in both directions
	//pDC->SetMapMode(MM_ANISOTROPIC);
	//pDC->SetViewportExt(m_nPrinterDpiX, m_nPrinterDpiY);
	//pDC->SetWindowExt(2540, 2540);

	//// Set the print origin to the upper left corner of the paper page
 //   pDC->SetViewportOrg(-m_nPrinterOffsetX, -m_nPrinterOffsetY);

	// Calculate the size of the page in logical pixels
	double dSizeX = (double)m_nPrinterSizeX / (double)m_nPrinterDpiX * 2540.0;
	double dSizeY = (double)m_nPrinterSizeY / (double)m_nPrinterDpiY * 2540.0;
	return CSize((long)dSizeX, (long)dSizeY);
}

void CPrintJobDocumentation::Print(HDC hDC, CPrintDialog* pPrintDlg)
{
	if ( ! hDC)
	{
		KIMOpenLogMessage(_T("Print Error!"), MB_ICONERROR);
		return;
	}

	CDC dc;
	dc.Attach(hDC);
	dc.m_bPrinting = TRUE;

	CRect rcRealPage(CPoint(0,0), GetPageSize());		// real world page in hundreds of millimeter
	CRect rcDstPage (CPoint(0,0), GetPageSize(&dc));	// printer page in printer points

	if (rcRealPage.IsRectNull())
		return;

	// Set the resolution to 100 pixels per milimeter in both directions
	dc.SetMapMode(MM_ANISOTROPIC);
	dc.SetWindowOrg(0, 0);
	dc.SetWindowExt(rcRealPage.Width(), rcRealPage.Height());
	dc.SetViewportExt(rcDstPage.Width(), rcDstPage.Height());

	// Print the pages
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		if (pDoc->m_PrintSheetList.GetCount())
		{
			dc.StartDoc(_T("KIM Print Job"));

			int nPageNum = 1;
			//if (pDoc->m_printGroupList.HasBoundedProductGroup())
			//{
			//	if (PageToPrint(pPrintDlg, nPageNum))
			//	{
			//		dc.StartPage();
			//		PrintBookBlock(&dc, nPageNum, rcRealPage, rcDstPage);
			//		dc.EndPage();
			//	}
			//	nPageNum++;
			//	if (PageToPrint(pPrintDlg, nPageNum))
			//	{
			//		dc.StartPage();
			//		PrintProductGroupInfoPages(&dc, nPageNum, rcRealPage, rcDstPage);
			//		dc.EndPage();
			//	}
			//	nPageNum++;
			//}

			//if (pDoc->m_printGroupList.HasUnboundedProductGroup())
			//{
			//	if (PageToPrint(pPrintDlg, nPageNum))
			//	{
			//		dc.StartPage();
			//		PrintLabelComponentPages(&dc, nPageNum, rcRealPage, rcDstPage);
			//		dc.EndPage();
			//	}
			//	nPageNum++;
			//}

			int nComponentIndex = 0;
			//do
			//{
			//	if ( ! PageToPrint(pPrintDlg, nPageNum))
			//	{
			//		CDC dcDummy;
			//		dcDummy.CreateCompatibleDC(&dc);
			//		dcDummy.m_bPrinting = TRUE;
			//		nComponentIndex = PrintComponentPlanningPages(&dcDummy, nPageNum, rcRealPage, rcDstPage, nComponentIndex);
			//	}
			//	else
			//	{
			//		dc.StartPage();
			//		nComponentIndex = PrintComponentPlanningPages(&dc, nPageNum, rcRealPage, rcDstPage, nComponentIndex);
			//		dc.EndPage();
			//	}

			//	nPageNum++;
			//}
			//while (nComponentIndex >= 0);

			CPrintSheet* pPrintSheet = NULL;
			//do
			//{
			//	if ( ! PageToPrint(pPrintDlg, nPageNum))
			//	{
			//		CDC dcDummy;
			//		dcDummy.CreateCompatibleDC(&dc);
			//		dcDummy.m_bPrinting = TRUE;
			//		pPrintSheet = PrintPrintSheetListPages(&dcDummy, nPageNum, rcRealPage, rcDstPage, pPrintSheet);
			//	}
			//	else
			//	{
			//		dc.StartPage();
			//		pPrintSheet = PrintPrintSheetListPages(&dc, nPageNum, rcRealPage, rcDstPage, pPrintSheet);
			//		dc.EndPage();
			//	}

			//	nPageNum++;
			//}
			//while (pPrintSheet);

			//if (PageToPrint(pPrintDlg, nPageNum))
			//{
			//	dc.StartPage();
			//	PrintPaperListPages(&dc, nPageNum, rcRealPage, rcDstPage);
			//	dc.EndPage();
			//}

			//nPageNum++;

			CPrintSheetList oldPrintSheetList;
			POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
			while (pos)
			{
				CPrintSheet& rSrcPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
				oldPrintSheetList.AddTail(rSrcPrintSheet);
				oldPrintSheetList.GetTail().LinkLayout(rSrcPrintSheet.GetLayout());
			}
			int	nNextGroupIndex = 0;
			CLayout* pLayout = pDoc->m_PrintSheetList.GetFirstLayout();
			while (pLayout)
			{
				if (pLayout->IsEmpty())
					pLayout = pDoc->m_PrintSheetList.GetNextLayout(pLayout);
				else
				{
					if ( ! PageToPrint(pPrintDlg, nPageNum))
					{
						pLayout = pDoc->m_PrintSheetList.GetNextLayout(pLayout);
						nPageNum++;
						continue;
					}
					dc.StartPage();
					pLayout = PrintLayoutPages(&dc, nPageNum, rcRealPage, rcDstPage, pLayout, nNextGroupIndex);
					nPageNum++;
					dc.EndPage();
				}
			}
			pos = oldPrintSheetList.GetHeadPosition();
			POSITION posDst = pDoc->m_PrintSheetList.GetHeadPosition();
			while (pos)
			{
				CPrintSheet& rSrcPrintSheet = oldPrintSheetList.GetNext(pos);
				CPrintSheet& rDstPrintSheet = pDoc->m_PrintSheetList.GetNext(posDst);
				rDstPrintSheet = rSrcPrintSheet;
				rDstPrintSheet.LinkLayout(rSrcPrintSheet.GetLayout());
				rDstPrintSheet.InitializeFlatProductTypeList();
			}



			int	nNextComponentIndex = 0;
			pPrintSheet = pDoc->m_PrintSheetList.GetFirstPrintSheet();
			while (pPrintSheet)
			{
				if (pPrintSheet->IsEmpty())
					pPrintSheet = pDoc->m_PrintSheetList.GetNextPrintSheet(pPrintSheet);

				if (pPrintSheet)
				{
					if ( ! PageToPrint(pPrintDlg, nPageNum))
					{
						pPrintSheet = pDoc->m_PrintSheetList.GetNextPrintSheet(pPrintSheet);
						nPageNum++;
						continue;
					}
					dc.StartPage();
					pPrintSheet = PrintSheetPages(&dc, nPageNum, rcRealPage, rcDstPage, pPrintSheet, nNextComponentIndex);
					nPageNum++;
					dc.EndPage();
				}
			}

			dc.EndDoc();
		}
	}
}

BOOL CPrintJobDocumentation::PageToPrint(CPrintDialog* pPrintDlg, int nPageNum)
{
	if ( ! pPrintDlg)	// auto server prints all
		return TRUE;
	else
		if (pPrintDlg->PrintAll())
			return TRUE;
		else
			if (pPrintDlg->PrintRange())
			{
				if ( (pPrintDlg->GetFromPage() <= nPageNum) && (nPageNum <= pPrintDlg->GetToPage()) )
					return TRUE;
			}
			else
				if (pPrintDlg->PrintSelection())
				{
					CJobDocumentationView* pView = (CJobDocumentationView*)theApp.GetView(RUNTIME_CLASS(CJobDocumentationView));
					CDisplayItem* pDI = (pView) ? pView->m_DisplayList.GetItemFromData((void*)nPageNum) : NULL;
					if (pDI)
						if (pDI->m_nState == CDisplayItem::Selected)
							return TRUE;
				}

	return FALSE;
}

int CPrintJobDocumentation::PrintBookBlock(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcRealPage.top;
	if (rcRealPage.IsRectEmpty())
		return rcRealPage.top;

	CRect rcPageMargins;
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	TCHAR szValue[100];
	DWORD dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_stscanf(szValue, _T("%ld %ld %ld %ld"), &rcPageMargins.left, &rcPageMargins.top, &rcPageMargins.right, &rcPageMargins.bottom);
	else
		rcPageMargins = CRect(2500, 2500, 2500, 2500);
	RegCloseKey(hKey);


	pDC->SaveDC();
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rcRealPage.Width(), rcRealPage.Height());
	pDC->SetWindowOrg(0, 0);
	pDC->SetViewportExt(rcDstPage.Width(), rcDstPage.Height());
	pDC->SetViewportOrg(rcDstPage.TopLeft());

	CRect rcFrame = rcRealPage; rcFrame.TopLeft() += rcPageMargins.TopLeft(); rcFrame.BottomRight() -= rcPageMargins.BottomRight();
	rcFrame.bottom = rcFrame.top + 1000;
	DrawPageHeader(pDC, rcFrame);

	CPen pen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);

	rcFrame.top = rcFrame.bottom; rcFrame.bottom = rcFrame.top + 4000;
	CRect rcGlobalDataBox = pDoc->m_GlobalData.Draw(pDC, rcFrame);	
	rcFrame.bottom = rcGlobalDataBox.bottom + 200;

	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));

	rcFrame.top	= rcFrame.bottom + 1000; rcFrame.bottom = rcRealPage.bottom - rcPageMargins.bottom;
	////// bookblock
	CRect rcTitleBox = pDoc->m_Bookblock.DrawTitle(pDC, rcFrame);
	rcFrame.top = rcTitleBox.bottom + Pixel2MM(pDC, 10);

	CRect rcBookblock = rcFrame; rcBookblock.top += Pixel2MM(pDC, 10); rcBookblock.right -= Pixel2MM(pDC, 5);
	pDoc->m_Bookblock.Draw(pDC, rcBookblock, -1, _T(""), WHITE);
	rcBox.UnionRect(rcBox, rcBookblock);

	pDC->RestoreDC(-1);

	return rcBox.bottom;
}

int CPrintJobDocumentation::PrintProductGroupInfoPages(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcRealPage.top;
	if (rcRealPage.IsRectEmpty())
		return rcRealPage.top;

	CRect rcPageMargins;
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	TCHAR szValue[100];
	DWORD dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_stscanf(szValue, _T("%ld %ld %ld %ld"), &rcPageMargins.left, &rcPageMargins.top, &rcPageMargins.right, &rcPageMargins.bottom);
	else
		rcPageMargins = CRect(2500, 2500, 2500, 2500);
	RegCloseKey(hKey);


	pDC->SaveDC();
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rcRealPage.Width(), rcRealPage.Height());
	pDC->SetWindowOrg(0, 0);
	pDC->SetViewportExt(rcDstPage.Width(), rcDstPage.Height());
	pDC->SetViewportOrg(rcDstPage.TopLeft());

	CRect rcFrame = rcRealPage; rcFrame.TopLeft() += rcPageMargins.TopLeft(); rcFrame.BottomRight() -= rcPageMargins.BottomRight();
	rcFrame.bottom = rcFrame.top + 1000;
	DrawPageHeader(pDC, rcFrame);

	CPen pen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);

	rcFrame.top = rcFrame.bottom; rcFrame.bottom = rcFrame.top + 4000;
	CRect rcGlobalDataBox = pDoc->m_GlobalData.Draw(pDC, rcFrame);	
	rcFrame.bottom = rcGlobalDataBox.bottom + 200;

	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	rcFrame.top	= rcFrame.bottom + 1000; rcFrame.bottom = rcFrame.top + 1000;
	CRect rcBox;// = pDoc->m_printGroupList.DrawBoundProductsHeader(pDC, rcFrame, TRUE);

	rcFrame.top	= rcBox.bottom + 200; rcFrame.bottom = rcFrame.top + 6000;
	////// product part data
	pen.DeleteObject();
	pen.CreatePen(PS_SOLID, 1, (pDC->IsPrinting()) ? MIDDLEGRAY : LIGHTGRAY);
	pDC->SelectObject(&pen);
	//int	  nRowHeight = max(Pixel2MM(pDC, 70), rcFrame.Height() / pDoc->m_printGroupList.GetSize()) - Pixel2MM(pDC, 10);
	int	  nRowHeight = Pixel2MM(pDC, 70);
	CRect rcRow = rcFrame;
	rcRow.bottom = rcRow.top + nRowHeight;
	//for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
	//{
	//	//if (pDoc->m_printGroupList[i].IsFlat())
	//	//	continue;
	//	//CRect rcPartBox = pDoc->m_printGroupList.Draw(pDC, rcRow, i);
	//	//rcBox.UnionRect(rcBox, rcPartBox);
	//	//rcRow.top = rcBox.bottom + Pixel2MM(pDC, 10); rcRow.bottom = rcRow.top + nRowHeight; 
	//	//if (i < pDoc->m_printGroupList.GetSize() - 1)
	//	//{
	//	//	pDC->MoveTo(rcRow.left, rcRow.top - Pixel2MM(pDC, 5)); pDC->LineTo(rcBox.right, rcRow.top - Pixel2MM(pDC, 5));
	//	//}
	//}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	pDC->RestoreDC(-1);

	return rcBox.bottom;
}

int CPrintJobDocumentation::PrintLabelComponentPages(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage)
{

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcRealPage.top;
	if (rcRealPage.IsRectEmpty())
		return rcRealPage.top;

	//if ( ! pDoc->m_printGroupList.HasUnboundedProductGroup())
	//	return rcRealPage.top;

	//CRect rcPageMargins;
	//HKEY hKey;
	//CString strKey = theApp.m_strRegistryKey;
	//RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	//TCHAR szValue[100];
	//DWORD dwSize = sizeof(szValue);
	//if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
	//	_stscanf(szValue, _T("%ld %ld %ld %ld"), &rcPageMargins.left, &rcPageMargins.top, &rcPageMargins.right, &rcPageMargins.bottom);
	//else
	//	rcPageMargins = CRect(2500, 2500, 2500, 2500);
	//RegCloseKey(hKey);


	//pDC->SaveDC();
	//pDC->SetMapMode(MM_ANISOTROPIC);
	//pDC->SetWindowExt(rcRealPage.Width(), rcRealPage.Height());
	//pDC->SetWindowOrg(0, 0);
	//pDC->SetViewportExt(rcDstPage.Width(), rcDstPage.Height());
	//pDC->SetViewportOrg(rcDstPage.TopLeft());

	//CRect rcFrame = rcRealPage; rcFrame.TopLeft() += rcPageMargins.TopLeft(); rcFrame.BottomRight() -= rcPageMargins.BottomRight();
	//rcFrame.bottom = rcFrame.top + 1000;
	//DrawPageHeader(pDC, rcFrame);

	//CPen pen(PS_SOLID, 1, DARKGRAY);
	//CPen* pOldPen = pDC->SelectObject(&pen);
	//pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);

	//rcFrame.top = rcFrame.bottom; rcFrame.bottom = rcFrame.top + 4000;
	//CRect rcGlobalDataBox = pDoc->m_GlobalData.Draw(pDC, rcFrame);	
	//rcFrame.bottom = rcGlobalDataBox.bottom + 200;

	//pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);
	//pDC->SelectObject(pOldPen);
	//pen.DeleteObject();

	//rcFrame.top	= rcFrame.bottom + 1000; rcFrame.bottom = rcFrame.top + 1000;
	//CRect rcBox;// = pDoc->m_printGroupList.DrawBoundProductsHeader(pDC, rcFrame, TRUE);

	//rcFrame.top	= rcBox.bottom + 200; rcFrame.bottom = rcFrame.top + 6000;
	//////// product part data
	//pen.DeleteObject();
	//pen.CreatePen(PS_SOLID, 1, (pDC->IsPrinting()) ? MIDDLEGRAY : LIGHTGRAY);
	//pDC->SelectObject(&pen);
	//int	  nRowHeight = max(Pixel2MM(pDC, 70), rcFrame.Height() / pDoc->m_printGroupList.GetSize()) - Pixel2MM(pDC, 10);
	//CRect rcRow = rcFrame;
	//rcRow.bottom = rcRow.top + nRowHeight;

	//for (int i = 0; i < pDoc->m_printGroupList.GetSize(); i++)
	//{
	//	//if (pDoc->m_printGroupList[i].IsBound())
	//	//	continue;
	//	//CRect rcPartBox = pDoc->m_printGroupList.Draw(pDC, rcRow, i);
	//	//rcBox.UnionRect(rcBox, rcPartBox);
	//	//rcRow.top = rcBox.bottom + Pixel2MM(pDC, 10); rcRow.bottom = rcRow.top + nRowHeight; 
	//	//if (i < pDoc->m_printGroupList.GetSize() - 1)
	//	//{
	//	//	pDC->MoveTo(rcRow.left, rcRow.top - Pixel2MM(pDC, 5)); pDC->LineTo(rcBox.right, rcRow.top - Pixel2MM(pDC, 5));
	//	//}
	//}

	//pDC->SelectObject(pOldPen);
	//pen.DeleteObject();

	//pDC->RestoreDC(-1);

	return 0;//rcBox.bottom;
}

int CPrintJobDocumentation::PrintComponentPlanningPages(CDC* pDC, int nPageNum, CRect rcRealPage, CRect rcDstPage, int nComponentIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nComponentIndex;
	if (rcRealPage.IsRectEmpty())
		return nComponentIndex;

	//CRect rcPageMargins;
	//HKEY hKey;
	//CString strKey = theApp.m_strRegistryKey;
	//RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	//TCHAR szValue[100];
	//DWORD dwSize = sizeof(szValue);
	//if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
	//	_stscanf(szValue, _T("%ld %ld %ld %ld"), &rcPageMargins.left, &rcPageMargins.top, &rcPageMargins.right, &rcPageMargins.bottom);
	//else
	//	rcPageMargins = CRect(2500, 2500, 2500, 2500);
	//RegCloseKey(hKey);


	//pDC->SaveDC();
	//pDC->SetMapMode(MM_ANISOTROPIC);
	//pDC->SetWindowExt(rcRealPage.Width(), rcRealPage.Height());
	//pDC->SetWindowOrg(0, 0);
	//pDC->SetViewportExt(rcDstPage.Width(), rcDstPage.Height());
	//pDC->SetViewportOrg(rcDstPage.TopLeft());

	//CRect rcBox;

	//CRect rcFrame = rcRealPage; rcFrame.TopLeft() += rcPageMargins.TopLeft(); rcFrame.BottomRight() -= rcPageMargins.BottomRight();
	//rcFrame.bottom = rcFrame.top + 1000;
	//DrawPageHeader(pDC, rcFrame);

	//CPen pen(PS_SOLID, 1, DARKGRAY);
	//CPen* pOldPen = pDC->SelectObject(&pen);
	//pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);

	//rcFrame.top = rcFrame.bottom; rcFrame.bottom = rcFrame.top + 4000;
	//CRect rcGlobalDataBox = pDoc->m_GlobalData.Draw(pDC, rcFrame);	
	//rcFrame.bottom = rcGlobalDataBox.bottom + 200;

	//pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);
	//pDC->SelectObject(pOldPen);
	//pen.DeleteObject();

	//rcFrame.top = rcFrame.bottom + 1000; rcFrame.bottom = rcRealPage.bottom - rcPageMargins.bottom;
	//int nPrintSheetRowHeight = pDoc->m_printGroupList.m_nPrintSheetRowHeight;
	//pDoc->m_printGroupList.m_nPrintSheetRowHeight = Pixel2MM(pDC, 60);

	////pDoc->m_printGroupList.DrawComponentList(pDC, rcFrame, -1, FALSE, NULL, &nComponentIndex);	
	//
	//pDoc->m_printGroupList.m_nPrintSheetRowHeight = nPrintSheetRowHeight;

	//pDC->SelectObject(pOldPen);
	//pen.DeleteObject();

	//pDC->RestoreDC(-1);

	return nComponentIndex;
}

CPrintSheet* CPrintJobDocumentation::PrintPrintSheetListPages(CDC* pDC, int nPageNum, CRect rcRealPage, CRect rcDstPage, CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if (rcRealPage.IsRectEmpty())
		return NULL;

	CRect rcPageMargins;
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	TCHAR szValue[100];
	DWORD dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_stscanf(szValue, _T("%ld %ld %ld %ld"), &rcPageMargins.left, &rcPageMargins.top, &rcPageMargins.right, &rcPageMargins.bottom);
	else
		rcPageMargins = CRect(2500, 2500, 2500, 2500);
	RegCloseKey(hKey);


	pDC->SaveDC();
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rcRealPage.Width(), rcRealPage.Height());
	pDC->SetWindowOrg(0, 0);
	pDC->SetViewportExt(rcDstPage.Width(), rcDstPage.Height());
	pDC->SetViewportOrg(rcDstPage.TopLeft());

	CRect rcBox;

	CRect rcFrame = rcRealPage; rcFrame.TopLeft() += rcPageMargins.TopLeft(); rcFrame.BottomRight() -= rcPageMargins.BottomRight();
	rcFrame.bottom = rcFrame.top + 1000;
	DrawPageHeader(pDC, rcFrame);

	CPen pen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);

	rcFrame.top = rcFrame.bottom; rcFrame.bottom = rcFrame.top + 4000;
	CRect rcGlobalDataBox = pDoc->m_GlobalData.Draw(pDC, rcFrame, NULL, TRUE);	
	rcFrame.bottom = rcGlobalDataBox.bottom + 200;

	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	rcFrame.top = rcFrame.bottom + 1000; rcFrame.bottom = rcRealPage.bottom - rcPageMargins.bottom;

	CArray <int, int> dummyColumnWidths;
	pDoc->m_PrintSheetList.Draw(pDC, rcFrame, dummyColumnWidths, NULL, &pPrintSheet);

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	pDC->RestoreDC(-1);

	return pPrintSheet;
}

int CPrintJobDocumentation::PrintPaperListPages(CDC* pDC, int nPageNum, CRect rcRealPage, CRect rcDstPage)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcRealPage.top;
	if (rcRealPage.IsRectEmpty())
		return rcRealPage.top;

	CRect rcPageMargins;
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	TCHAR szValue[100];
	DWORD dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_stscanf(szValue, _T("%ld %ld %ld %ld"), &rcPageMargins.left, &rcPageMargins.top, &rcPageMargins.right, &rcPageMargins.bottom);
	else
		rcPageMargins = CRect(2500, 2500, 2500, 2500);
	RegCloseKey(hKey);


	pDC->SaveDC();
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rcRealPage.Width(), rcRealPage.Height());
	pDC->SetWindowOrg(0, 0);
	pDC->SetViewportExt(rcDstPage.Width(), rcDstPage.Height());
	pDC->SetViewportOrg(rcDstPage.TopLeft());

	CRect rcBox;

	CRect rcFrame = rcRealPage; rcFrame.TopLeft() += rcPageMargins.TopLeft(); rcFrame.BottomRight() -= rcPageMargins.BottomRight();
	rcFrame.bottom = rcFrame.top + 1000;
	DrawPageHeader(pDC, rcFrame);

	CPen pen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);

	rcFrame.top = rcFrame.bottom; rcFrame.bottom = rcFrame.top + 4000;
	CRect rcGlobalDataBox = pDoc->m_GlobalData.Draw(pDC, rcFrame);	
	rcFrame.bottom = rcGlobalDataBox.bottom + 200;

	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	rcFrame.top = rcFrame.bottom + 1000; rcFrame.bottom = rcRealPage.bottom - rcPageMargins.bottom;

	CArray <int, int> dummyColumnWidths;
	pDoc->m_productionData.m_paperList.Draw(pDC, rcFrame, dummyColumnWidths);

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	pDC->RestoreDC(-1);

	return rcBox.bottom;
}

CLayout* CPrintJobDocumentation::PrintLayoutPages(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage, CLayout* pNextLayout, int& nNextGroupIndex)
{
	if ( ! pNextLayout)
		return NULL;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if (rcRealPage.IsRectEmpty())
		return NULL;

	CRect rcPageMargins;
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	TCHAR szValue[100];
	DWORD dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_stscanf(szValue, _T("%ld %ld %ld %ld"), &rcPageMargins.left, &rcPageMargins.top, &rcPageMargins.right, &rcPageMargins.bottom);
	else
		rcPageMargins = CRect(2500, 2500, 2500, 2500);
	RegCloseKey(hKey);


	pDC->SaveDC();
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rcRealPage.Width(), rcRealPage.Height());
	pDC->SetWindowOrg(0, 0);
	pDC->SetViewportExt(rcDstPage.Width(), rcDstPage.Height());
	pDC->SetViewportOrg(rcDstPage.TopLeft());

	CRect rcFrame = rcRealPage; rcFrame.TopLeft() += rcPageMargins.TopLeft(); rcFrame.BottomRight() -= rcPageMargins.BottomRight();
	rcFrame.bottom = rcFrame.top + 1000;
	DrawPageHeader(pDC, rcFrame);

	CPen pen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);

	rcFrame.top = rcFrame.bottom; rcFrame.bottom = rcFrame.top + 4000;
	CRect rcGlobalDataBox = pDoc->m_GlobalData.Draw(pDC, rcFrame, NULL, TRUE);	
	rcFrame.bottom = rcGlobalDataBox.bottom + 200;

	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);
	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	rcFrame.top	   = rcFrame.bottom + 1000;
	rcFrame.bottom = rcRealPage.bottom - rcPageMargins.bottom;
	CJobDocumentationSettingsView* pView = (CJobDocumentationSettingsView*)theApp.GetView(RUNTIME_CLASS(CJobDocumentationSettingsView));
	int  nShowSide		= (pView) ? pView->m_nShowSide		: BOTHSIDES;
	BOOL bShowPlate		= (pView) ? pView->m_bShowPlate		: FALSE;
	BOOL bShowFoldSheet = (pView) ? pView->m_bShowFoldSheet : TRUE;
	BOOL bShowMeasure	= (pView) ? pView->m_bShowMeasure	: TRUE;
	BOOL bShowBitmap	= FALSE;	// layouts with measures should be w/o bitmaps due to better overview

	CPrintSheet* pPrintSheet = pNextLayout->GetFirstPrintSheet();
	if (pPrintSheet)
	{
		CRect rcGroups = rcFrame; 
		if (nNextGroupIndex <= 0)	// if not first group, the group drawing reached end of page on last page -> dont draw layout again
		{
			CPrintSheet printSheetBackup = *pPrintSheet;
			pPrintSheet->InitializeFlatProductRefsByFormats();

			if (rcRealPage.Width() < rcRealPage.Height())
			{
				CPressDevice* pPressDevice = pNextLayout->m_FrontSide.GetPressDevice();

				CRect rcHeader = rcFrame; rcHeader.bottom = rcHeader.top + 1000;
				pNextLayout->DrawHeader(pDC, rcHeader, NULL, pPressDevice, TRUE);

				CRect rcSheet = rcFrame;
				rcSheet.top	   = rcHeader.bottom + 500;

				if ( (nShowSide == BOTHSIDES) && (pNextLayout->GetCurrentWorkStyle() == WORK_AND_BACK) )
					rcSheet.bottom = rcSheet.top + rcSheet.Height() / 2 - 500;
				if ( (nShowSide == FRONTSIDE) || (nShowSide == BOTHSIDES) )
				{
					CRect rcClip = pNextLayout->GetDrawingBox(pDC, rcSheet, pPrintSheet, bShowPlate ? pPressDevice : NULL, FRONTSIDE, bShowMeasure, FALSE);
					rcClip.InflateRect(100,100);
					pDC->IntersectClipRect(rcClip);
					rcSheet = pNextLayout->Draw(pDC, rcSheet, pPrintSheet, bShowPlate ? pPressDevice : NULL, bShowFoldSheet, FALSE, bShowMeasure, NULL, FALSE, FRONTSIDE, NULL, NULL, NULL, TRUE, bShowBitmap, FALSE, FALSE, TRUE);
					pDC->SelectClipRgn(NULL);
				}
				if ( (nShowSide == BACKSIDE) || (nShowSide == BOTHSIDES) )
				{
					if (pNextLayout->m_nProductType == WORK_AND_BACK)
					{
						rcSheet.OffsetRect(0, rcSheet.Height() + 1000);
						CRect rcClip = pNextLayout->GetDrawingBox(pDC, rcSheet, pPrintSheet, bShowPlate ? pPressDevice : NULL, BACKSIDE, bShowMeasure, FALSE);
						rcClip.InflateRect(100,100);
						pDC->IntersectClipRect(rcClip);
						rcSheet = pNextLayout->Draw(pDC, rcSheet, pPrintSheet, bShowPlate ? pPressDevice : NULL, bShowFoldSheet, FALSE, bShowMeasure, NULL, FALSE, BACKSIDE,  NULL, NULL, NULL, TRUE, bShowBitmap, FALSE, FALSE, TRUE);
						pDC->SelectClipRgn(NULL);
					}
				}
				rcGroups.top = rcSheet.bottom + 400;
			}
			else
			{
				CPressDevice* pPressDevice = pNextLayout->m_FrontSide.GetPressDevice();

				CRect rcHeader = rcFrame; rcHeader.bottom = rcHeader.top + 1000;
				pNextLayout->DrawHeader(pDC, rcHeader, pPrintSheet, pPressDevice, TRUE);

				CRect rcSheet = rcFrame;
				rcSheet.top = rcHeader.bottom + 500;

				CRect rcClip = pNextLayout->GetDrawingBox(pDC, rcSheet, pPrintSheet, bShowPlate ? pPressDevice : NULL, nShowSide, bShowMeasure, FALSE);
				rcClip.InflateRect(100,100);
				pDC->IntersectClipRect(rcClip);
				rcSheet = pNextLayout->Draw(pDC, rcSheet, pPrintSheet, bShowPlate ? pPressDevice : NULL, bShowFoldSheet, FALSE, bShowMeasure, NULL, FALSE, nShowSide, NULL, NULL, NULL, TRUE, bShowBitmap, FALSE, FALSE, TRUE);
				pDC->SelectClipRgn(NULL);
				rcGroups.top = rcSheet.bottom + 400;
			}
		}

		rcGroups = DrawLayoutFormatGroups(pDC, rcGroups, pPrintSheet, nNextGroupIndex);
			
		if (nNextGroupIndex >= pPrintSheet->m_flatProductTypeList.GetSize())	// no groups remaining to draw -> begin new page
			pNextLayout = pDoc->m_PrintSheetList.GetNextLayout(pNextLayout);
	}
	else
		pNextLayout = pDoc->m_PrintSheetList.GetNextLayout(pNextLayout);


	pDC->RestoreDC(-1);

	return pNextLayout;
}


CPrintSheet* CPrintJobDocumentation::PrintSheetPages(CDC* pDC, int nPageNo, CRect rcRealPage, CRect rcDstPage, CPrintSheet* pNextPrintSheet, int& nNextComponentIndex)
{
	if ( ! pNextPrintSheet)
		return NULL;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if (rcRealPage.IsRectEmpty())
		return NULL;

	CRect rcPageMargins;
	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey;
	RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey);
	TCHAR szValue[100];
	DWORD dwSize = sizeof(szValue);
	if (RegQueryValueEx(hKey, _T("PrintMargins"), 0, NULL, (LPBYTE)szValue, &dwSize) == ERROR_SUCCESS)
		_stscanf(szValue, _T("%ld %ld %ld %ld"), &rcPageMargins.left, &rcPageMargins.top, &rcPageMargins.right, &rcPageMargins.bottom);
	else
		rcPageMargins = CRect(2500, 2500, 2500, 2500);
	RegCloseKey(hKey);


	pDC->SaveDC();
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(rcRealPage.Width(), rcRealPage.Height());
	pDC->SetWindowOrg(0, 0);
	pDC->SetViewportExt(rcDstPage.Width(), rcDstPage.Height());
	pDC->SetViewportOrg(rcDstPage.TopLeft());

	CRect rcFrame = rcRealPage; rcFrame.TopLeft() += rcPageMargins.TopLeft(); rcFrame.BottomRight() -= rcPageMargins.BottomRight();
	rcFrame.bottom = rcFrame.top + 1000;
	DrawPageHeader(pDC, rcFrame);

	CPen pen(PS_SOLID, 1, DARKGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);
	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);

	rcFrame.top = rcFrame.bottom; rcFrame.bottom = rcFrame.top + 4000;
	CRect rcGlobalDataBox = pDoc->m_GlobalData.Draw(pDC, rcFrame);	
	rcFrame.bottom = rcGlobalDataBox.bottom + 200;

	pDC->MoveTo(rcFrame.left, rcFrame.bottom); pDC->LineTo(rcFrame.right, rcFrame.bottom);

	CJobDocumentationSettingsView* pView = (CJobDocumentationSettingsView*)theApp.GetView(RUNTIME_CLASS(CJobDocumentationSettingsView));
	if ( ! pView)
		return NULL;

	int  nSheetZoomLevel	= (pView) ? max(pView->m_nSheetZoomLevel, 1) : 4;
	int  nShowSide			= (pView) ? pView->m_nShowSide		: BOTHSIDES;
	BOOL bShowPlate			= (pView) ? pView->m_bShowPlate		: FALSE;
	BOOL bShowFoldSheet		= (pView) ? pView->m_bShowFoldSheet : FALSE;
	BOOL bShowBitmap		= (pView) ? pView->m_bShowBitmap	: TRUE;
	
	rcFrame.top	   = rcFrame.bottom + 1000;
	rcFrame.bottom = rcRealPage.bottom - rcPageMargins.bottom;
	int nHeight	   = rcFrame.bottom - rcFrame.top;

	BOOL bIsFirstSheet = TRUE;
	while (pNextPrintSheet)
	{
		CLayout* pLayout = pNextPrintSheet->GetLayout();
		if ( ! pLayout)
		{
			pNextPrintSheet = NULL;
			break;
		}

		if ( ! pNextPrintSheet->IsEmpty())
		{
			CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice(); 

			CRect rcHeader = rcFrame; rcHeader.bottom = rcHeader.top + 1000;

			CRect rcSheet = rcFrame;
			rcSheet.top = rcHeader.bottom + 200; rcSheet.bottom = rcFrame.top + nHeight/(11 - nSheetZoomLevel) - 200;

			CRect rcBox = pLayout->GetDrawingBox(pDC, rcSheet, pNextPrintSheet, bShowPlate ? pPressDevice : NULL, nShowSide, FALSE, FALSE); 
			if (rcSheet.top + abs(rcBox.Height() + 200 + 3 * Pixel2MM(pDC, 20)) > rcFrame.bottom)		// 200 + 3 * ... -> min. 2 component rows (plus header row) should fit on the page, otherwise its better to begin new page
				break;

			BOOL bDrawLayout = TRUE;
			if (bIsFirstSheet)
			{
				bDrawLayout = (nNextComponentIndex > 0) ? FALSE : TRUE;		// if first sheet and not first component, the component drawing reached end of page on last page -> dont draw layout again
				bIsFirstSheet = FALSE;
			}
			else
			{
				pDC->MoveTo(rcFrame.left, rcHeader.top - 200); pDC->LineTo(rcFrame.right, rcHeader.top - 200);
			}

			CRect rcComponents = rcFrame; 

			if (bDrawLayout)		// if end of page reached on last page, don't draw layout again on new page
			{
				pLayout->DrawHeader(pDC, rcHeader, pNextPrintSheet, pPressDevice, FALSE);

				rcBox.InflateRect(200,200);
				pDC->IntersectClipRect(rcBox);
				rcSheet = pLayout->Draw(pDC, rcSheet, pNextPrintSheet, bShowPlate ? pPressDevice : NULL, bShowFoldSheet, FALSE, FALSE, NULL, FALSE, nShowSide, NULL, NULL, NULL, TRUE, bShowBitmap);
				pDC->SelectClipRgn(NULL);
				rcComponents.top = rcSheet.bottom + 400;
			}

			rcComponents = DrawPrintSheetComponents(pDC, rcComponents, pNextPrintSheet, nNextComponentIndex);

			rcFrame.top = rcComponents.bottom + 1000;
		}

		if (nNextComponentIndex < pNextPrintSheet->GetNumComponents())	// there are components remaining to draw (because end of page has been reached) -> break and begin new page
			break;
		else
		{
			pNextPrintSheet = pDoc->m_PrintSheetList.GetNextPrintSheet(pNextPrintSheet);
			nNextComponentIndex = 0;
			if (pView->m_nSheetZoomLevel > 8)	// in case of big layouts, always begin sheet at new page
				break;	
		}
	}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	pDC->RestoreDC(-1);

	return pNextPrintSheet;
}

void CPrintJobDocumentation::DrawPageHeader(CDC* pDC, CRect rcFrame)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CFont font;
	font.CreateFont (Pixel2MM(pDC, 20), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);

	CString strOut = pDoc->GetTitle(); 
	theApp.RemoveExtension(strOut);
	pDC->DrawText(strOut, rcFrame, DT_LEFT | DT_TOP);

	CBitmap bitmap;	bitmap.LoadBitmap(IDB_KIMLOGOBAR);
	BITMAP bm;   	bitmap.GetBitmap(&bm);
	CDC memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap* pOldBitmap = memDC.SelectObject(&bitmap);
	CRect rcBitmap(rcFrame.right - 3000, rcFrame.top, rcFrame.right, rcFrame.top + 3000 / (bm.bmWidth / bm.bmHeight));
	pDC->StretchBlt(rcBitmap.left, rcBitmap.top, rcBitmap.Width(), rcBitmap.Height(), &memDC, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);
	memDC.SelectObject(pOldBitmap);
	bitmap.DeleteObject();
}

void CPrintJobDocumentation::AddDisplayColumn(int nColID, int nWidth, CString strLabel, int nDT_Attribute)
{
	struct displayColumn dc = {nColID, nWidth, _T(""), nDT_Attribute};
	strncpy(dc.szLabel, strLabel.GetBuffer(), 100);
	m_displayColumns.Add(dc);
}

CRect CPrintJobDocumentation::DrawDisplayColumnHeader(CDC* pDC, CRect rcFrame)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CFont boldFont;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  10) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 
	pDC->SetTextColor(BLACK);

	CRect rcColumn = rcFrame; 
	int	  nRight   = rcColumn.left;
	for (int i = 0; i < m_displayColumns.GetSize(); i++)
	{
		nRight += m_displayColumns[i].nWidth;
		rcColumn.right = nRight;

		DrawTextMeasured(pDC, m_displayColumns[i].szLabel, rcColumn, m_displayColumns[i].nDT_Attribute | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	
		rcColumn.left = nRight + Pixel2MM(pDC, 5);
	}

	pDC->SelectObject(pOldFont);
	boldFont.DeleteObject();

	return rcBox;
}

CRect CPrintJobDocumentation::DrawPrintSheetComponents(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, int& nNextComponentIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(0,0,1,1);
	if ( ! pPrintSheet)
		return CRect(0,0,1,1);

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	m_displayColumns.RemoveAll();

	CString string, string1;

	if (pPrintSheet->HasFoldSheetsOnly())
		string.LoadString(IDS_FOLDSHEET_LABEL);
	else
		if (pPrintSheet->HasFlatProductsOnly())
			string.LoadString(IDS_PRODUCT);
		else
		{
			string.LoadString(IDS_FOLDSHEET_LABEL); string1.LoadString(IDS_PRODUCT); string += _T(" / ") + string1;
		}

	AddDisplayColumn(ColComponent, Pixel2MM(pDC, 200), string);
	if (pPrintSheet->HasFoldSheets())
	{
		string.LoadString(IDS_PAGES_HEADER);	AddDisplayColumn(ColPages,			Pixel2MM(pDC, 110), string);
	}
	string.LoadString(IDS_NUP);					AddDisplayColumn(ColNUp,			Pixel2MM(pDC,  50), string);
	string.LoadString(IDS_PAPERLIST_FORMAT);	AddDisplayColumn(ColFormat,			Pixel2MM(pDC,  60), string);
	string.LoadString(IDS_QUANTITY);			AddDisplayColumn(ColQuantity,		Pixel2MM(pDC,  50), string, DT_RIGHT);
	string.LoadString(IDS_COVERAGE);			AddDisplayColumn(ColSheetCoverage,	Pixel2MM(pDC,  50), string, DT_RIGHT);
	string.LoadString(IDS_OVERPRODUCTION);		AddDisplayColumn(ColOverProduction,	Pixel2MM(pDC, 110), string, DT_RIGHT);

	int nRowHeight = Pixel2MM(pDC, 20);
	CRect rcRow = rcFrame; rcRow.left += 1000; rcRow.bottom = rcRow.top + nRowHeight;

	CRect rcHeader = DrawDisplayColumnHeader(pDC, rcRow);
	rcRow.top	 = rcHeader.bottom + Pixel2MM(pDC, 5);
	rcRow.bottom = rcRow.top	   + nRowHeight;

	BOOL bReachEndOfPage = FALSE;
	///// foldsheets
	for (int nFoldSheetTypeIndex = 0; ! bReachEndOfPage && (nFoldSheetTypeIndex < pPrintSheet->m_FoldSheetTypeList.GetSize()); nFoldSheetTypeIndex++)
	{
		if (nFoldSheetTypeIndex < nNextComponentIndex)		// search for start index
			continue;

		/////// columns
		CRect rcColumn = rcRow; 
		int	  nRight   = rcColumn.left;
		
		CFoldSheet* pFoldSheet  = pPrintSheet->m_FoldSheetTypeList[nFoldSheetTypeIndex].m_pFoldSheet;
		int			nLayerIndex	= pPrintSheet->m_FoldSheetTypeList[nFoldSheetTypeIndex].m_nFoldSheetLayerIndex;
		if ( ! pFoldSheet)
			continue;
		int	  nNUp	  		= pPrintSheet->m_FoldSheetTypeList[nFoldSheetTypeIndex].m_nNumUp;
		float fWidth  		= pFoldSheet->GetProductPart().m_fPageWidth;
		float fHeight 		= pFoldSheet->GetProductPart().m_fPageHeight;
		int	  nQuantity 	= pFoldSheet->GetQuantityPlanned();
		float fCoverage 	= pFoldSheet->CalcCoverage(pPrintSheet);
		int   nOver			= pFoldSheet->GetQuantityActual() - pFoldSheet->GetQuantityPlanned();
		float fOverPercent	= (pFoldSheet->GetQuantityPlanned() != 0.0f) ? (((float)pFoldSheet->GetQuantityActual()/(float)pFoldSheet->GetQuantityPlanned() - 1.0f) * 100.0f) : 0.0f;
		CRect rcRet, rcThumbnail;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColComponent:			rcRet = CPrintSheetComponentsView::DrawFoldSheetTypeInfo 		(pDC, rcColumn, pFoldSheet, nLayerIndex);	break;
			case ColPages:				rcRet = CPrintSheetComponentsView::DrawFoldSheetTypePages		(pDC, rcColumn, pFoldSheet, nLayerIndex);	break;
			case ColNUp:				rcRet = CPrintSheetComponentsView::DrawComponentNUp	  			(pDC, rcColumn, nNUp);						break;
			case ColFormat:				rcRet = CPrintSheetComponentsView::DrawComponentFormat	  		(pDC, rcColumn, fWidth, fHeight);			break;
			case ColQuantity:			rcRet = CPrintSheetComponentsView::DrawComponentQuantity		(pDC, rcColumn, nQuantity);					break;
			case ColSheetCoverage:		rcRet = CPrintSheetComponentsView::DrawComponentCoverage		(pDC, rcColumn, fCoverage);					break;
			case ColOverProduction:		rcRet = CPrintSheetComponentsView::DrawComponentOverProduction	(pDC, rcColumn, nOver, fOverPercent);		break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + Pixel2MM(pDC, 5);
		}
		rcRow.top	 = rcBox.bottom + Pixel2MM(pDC, 5);
		rcRow.bottom = rcRow.top	+ nRowHeight;

		nNextComponentIndex++;
		if (rcRow.bottom > rcFrame.bottom)
			bReachEndOfPage = TRUE;
	}

	rcRow.bottom = rcRow.top + Pixel2MM(pDC, 20);
	///// flat products
	for (int nFlatProductTypeIndex = 0; ! bReachEndOfPage && (nFlatProductTypeIndex < pPrintSheet->m_flatProductTypeList.GetSize()); nFlatProductTypeIndex++)
	{
		if ( (nFlatProductTypeIndex + pPrintSheet->m_FoldSheetTypeList.GetSize()) < nNextComponentIndex)		// search for start index
			continue;

		/////// columns
		CRect rcColumn = rcRow; 
		int	  nRight   = rcColumn.left;
		
		CFlatProduct* pFlatProduct = pPrintSheet->m_flatProductTypeList[nFlatProductTypeIndex].m_pFlatProduct;
		if ( ! pFlatProduct)
			continue;
		int	  nNUp			= pPrintSheet->m_flatProductTypeList[nFlatProductTypeIndex].m_nNumUp;
		float fWidth		= pFlatProduct->m_fTrimSizeWidth;
		float fHeight		= pFlatProduct->m_fTrimSizeHeight;
		int	  nQuantity 	= pFlatProduct->m_nDesiredQuantity;
		float fCoverage 	= pFlatProduct->CalcCoverage(pPrintSheet);
		int   nOver			= pFlatProduct->m_nRealQuantity - pFlatProduct->m_nDesiredQuantity;
		float fOverPercent	= (pFlatProduct->m_nDesiredQuantity != 0.0f) ? (((float)pFlatProduct->m_nRealQuantity/(float)pFlatProduct->m_nDesiredQuantity - 1.0f) * 100.0f) : 0.0f;
		CRect rcRet, rcThumbnail;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColComponent:			rcRet = CPrintSheetComponentsView::DrawFlatProductTypeInfo		(pDC, rcColumn, pFlatProduct);			break;
			case ColNUp:				rcRet = CPrintSheetComponentsView::DrawComponentNUp				(pDC, rcColumn, nNUp);					break;
			case ColFormat:				rcRet = CPrintSheetComponentsView::DrawComponentFormat			(pDC, rcColumn, fWidth, fHeight);		break;
			case ColQuantity:			rcRet = CPrintSheetComponentsView::DrawComponentQuantity		(pDC, rcColumn, nQuantity);				break;
			case ColSheetCoverage:		rcRet = CPrintSheetComponentsView::DrawComponentCoverage		(pDC, rcColumn, fCoverage);				break;
			case ColOverProduction:		rcRet = CPrintSheetComponentsView::DrawComponentOverProduction	(pDC, rcColumn, nOver, fOverPercent);	break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + Pixel2MM(pDC, 5);
		}
		rcRow.top	 = rcBox.bottom + Pixel2MM(pDC, 5);
		rcRow.bottom = rcRow.top	+ nRowHeight;

		nNextComponentIndex++;
		if (rcRow.bottom > rcFrame.bottom)
			bReachEndOfPage = TRUE;
	}

	return rcBox;
}

CRect CPrintJobDocumentation::DrawLayoutFormatGroups(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, int& nNextGroupIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CRect(0,0,1,1);
	if ( ! pPrintSheet)
		return CRect(0,0,1,1);
	if ( ! pPrintSheet->HasFlatProductsOnly())
		return CRect(0,0,1,1);
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return CRect(0,0,1,1);

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	m_displayColumns.RemoveAll();

	CString string;
	string.LoadString(IDS_NUMBER_TEXT);			AddDisplayColumn(ColFormatIndex, Pixel2MM(pDC,  30),  string);
	string.LoadString(IDS_PAPERLIST_FORMAT);	AddDisplayColumn(ColFormat,		 Pixel2MM(pDC,  120), string);

	int nRowHeight = Pixel2MM(pDC, 20);
	CRect rcRow = rcFrame; rcRow.left += 1000; rcRow.bottom = rcRow.top + nRowHeight;

	CRect rcHeader = DrawDisplayColumnHeader(pDC, rcRow);
	rcRow.top	 = rcHeader.bottom + Pixel2MM(pDC, 5);
	rcRow.bottom = rcRow.top	   + nRowHeight;

	BOOL bReachEndOfPage = FALSE;
	///// flat products
	for (int nFlatProductTypeIndex = 0; ! bReachEndOfPage && (nFlatProductTypeIndex < pPrintSheet->m_flatProductTypeList.GetSize()); nFlatProductTypeIndex++)
	{
		if (nFlatProductTypeIndex < nNextGroupIndex)		// search for start index
			continue;

		/////// columns
		CRect rcColumn = rcRow; 
		int	  nRight   = rcColumn.left;
		
		CFlatProduct* pFlatProduct = pPrintSheet->m_flatProductTypeList[nFlatProductTypeIndex].m_pFlatProduct;
		if ( ! pFlatProduct)
			continue;
		CLayoutObject* pLayoutObject = pFlatProduct->GetFirstLayoutObject(pPrintSheet);
		if ( ! pLayoutObject)
			continue;

		CRect rcRet, rcThumbnail;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColFormatIndex:		rcRet = DrawFormatIndex									(pDC, rcColumn, pLayoutObject->m_nGroupIndex);										break;
			case ColFormat:				rcRet = CPrintSheetComponentsView::DrawComponentFormat	(pDC, rcColumn, pFlatProduct->m_fTrimSizeWidth, pFlatProduct->m_fTrimSizeHeight);	break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + Pixel2MM(pDC, 5);
		}
		rcRow.top	 = rcBox.bottom + Pixel2MM(pDC, 5);
		rcRow.bottom = rcRow.top	+ nRowHeight;

		nNextGroupIndex++;
		if (rcRow.bottom > rcFrame.bottom)
			bReachEndOfPage = TRUE;
	}

	return rcBox;
}

CRect CPrintJobDocumentation::DrawFormatIndex(CDC* pDC, CRect rcFrame, int nIndex)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font;
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);

	CString strOut;
	CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + nTextHeight;
	pDC->SetTextColor(RGB(200,100,0));
	strOut.Format(_T("%d: "), nIndex + 1);
	DrawTextMeasured(pDC, strOut, rcTitle, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();

	return rcBox;
}

