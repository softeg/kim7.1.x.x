// DlgPageSourceProps.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgPageSourceProps.h"
#include "PageSourceListView.h"
#include "PageListView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPageSourceProps dialog


CDlgPageSourceProps::CDlgPageSourceProps(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPageSourceProps::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPageSourceProps)
	m_strBoundingBox = _T("");
	m_strDocumentPath = _T("");
	m_strPageName = _T("");
	m_strCreator = _T("");
	m_strCreationDate = _T("");
	m_strDocumentTitle = _T("");
	m_strTrimBox = _T("");
	//}}AFX_DATA_INIT

	m_pImageList = NULL;
}

CDlgPageSourceProps::~CDlgPageSourceProps()
{
	if( m_pImageList != NULL)
		delete m_pImageList;
}

void CDlgPageSourceProps::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPageSourceProps)
	DDX_Control(pDX, IDC_PAGE_PREVIEW, m_PagePreviewFrame);
	DDX_Text(pDX, IDC_BOUNDING_BOX, m_strBoundingBox);
	DDX_Text(pDX, IDC_DOCUMENT_PATH, m_strDocumentPath);
	DDX_Text(pDX, IDC_DOCUMENT_CREATOR, m_strCreator);
	DDX_Text(pDX, IDC_DOCUMENT_DATE, m_strCreationDate);
	DDX_Text(pDX, IDC_DOCUMENT_TITLE, m_strDocumentTitle);
	DDX_Text(pDX, IDC_TRIM_BOX, m_strTrimBox);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPageSourceProps, CDialog)
	//{{AFX_MSG_MAP(CDlgPageSourceProps)
	ON_WM_PAINT()
	ON_LBN_SELCHANGE(IDC_SPOTCOLORLIST, OnSelchangeSpotcolorlist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPageSourceProps message handlers

BOOL CDlgPageSourceProps::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	VERIFY(m_ColorInfoListBox.SubclassDlgItem(IDC_SPOTCOLORLIST, this));

	m_pPageSourceHeader = NULL;
	m_pPageSource		= NULL;

	CPageSourceListView* pPageSourceListView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
	CDisplayItem*		 pDI				 = pPageSourceListView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader));

	BOOL bMultiplePagesSelected = FALSE;
	if (pDI)
	{
		m_pPageSourceHeader = (CPageSourceHeader*)pDI->m_pData;
		m_pPageSource		= (CPageSource*)	  pDI->m_pAuxData;

		pDI = pPageSourceListView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CPageSourceListView, PageSourceHeader));
		if (pDI)
			bMultiplePagesSelected = TRUE;
	}

	m_strDocumentPath = "";
	m_strPageName	  = "";
	m_strBoundingBox  = "";
	m_strTrimBox  = "";

	if (m_pPageSourceHeader)
	{
		CString strTitle, strWinText;	 
		TCHAR szPathBuffer[_MAX_PATH], 
			 szDrive[_MAX_DRIVE], 
			 szDir[_MAX_DIR], 
			 szFileName[_MAX_FNAME], 
			 szExtension[_MAX_EXT];

		m_nHeaderIndex = m_pPageSource->GetHeaderIndex(m_pPageSourceHeader);
		_tcsncpy(szPathBuffer, (LPCTSTR)m_pPageSource->m_strDocumentFullpath, MAX_PATH);
		_tsplitpath(szPathBuffer, szDrive, szDir, szFileName, szExtension);
		CString strFileName; 
		strFileName.Format(_T("%s%s"), szFileName, szExtension);

		GetWindowText(strWinText);	// Use window text as control string
		strTitle.Format(strWinText, (LPCTSTR)strFileName, m_pPageSourceHeader->m_nPageNumber);
		if(bMultiplePagesSelected)
			strTitle+= " , ...";
		SetWindowText(strTitle);

		m_strDocumentPath.Format(_T("%s%s"), szDrive, szDir);

		m_strDocumentTitle	= m_pPageSource->m_strDocumentTitle;
		m_strCreator		= m_pPageSource->m_strCreator;							   
		m_strCreationDate	= m_pPageSource->m_strCreationDate;

//		if (!bMultiplePagesSelected)	// Only one page selected
		{
			if (m_pPageSource->m_strPreviewFilesPath.IsEmpty() && m_strCreator == _T("RIP"))
				m_strBoundingBox.LoadString(IDS_UNKNOWN_STR);
			else
			{
				// Sets the FormatString dependent on unit
				CString strMeasureFormat;
				strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.2f"), (LPCTSTR)MeasureFormat("%.2f"));
				m_strBoundingBox.Format((LPCTSTR)strMeasureFormat, MeasureFormat(m_pPageSourceHeader->m_fBBoxRight - m_pPageSourceHeader->m_fBBoxLeft), 
																   MeasureFormat(m_pPageSourceHeader->m_fBBoxTop   - m_pPageSourceHeader->m_fBBoxBottom));
				m_strTrimBox.Format((LPCTSTR)strMeasureFormat,	   MeasureFormat(m_pPageSourceHeader->m_fTrimBoxRight - m_pPageSourceHeader->m_fTrimBoxLeft), 
																   MeasureFormat(m_pPageSourceHeader->m_fTrimBoxTop   - m_pPageSourceHeader->m_fTrimBoxBottom));
			}
		}

		CImpManDoc*	pDoc = CImpManDoc::GetDoc();

		m_ColorInfoListBox.m_nSepStatus	= 0;
		BOOL bComposite = FALSE;
		BOOL bSeparated = FALSE;
		int  nCurSel = -1;
		for (int i = 0; i < m_pPageSource->m_PageSourceHeaders.GetSize(); i++)
		{
			if (m_pPageSource->m_PageSourceHeaders[i].m_nPageNumber == m_pPageSourceHeader->m_nPageNumber)
			{
				CString strColorName = m_pPageSource->m_PageSourceHeaders[i].m_strColorName;
				if (strColorName.CompareNoCase(_T("Composite")) == 0)
				{
					for (int j = 0; j < m_pPageSource->m_PageSourceHeaders[i].m_processColors.GetSize(); j++)
					{
						COLORREF crColor = WHITE;
						if (theApp.m_colorDefTable.IsCyan(m_pPageSource->m_PageSourceHeaders[i].m_processColors[j]))
							crColor = CYAN;		
						else
							if (theApp.m_colorDefTable.IsMagenta(m_pPageSource->m_PageSourceHeaders[i].m_processColors[j]))
								crColor = MAGENTA;	
							else
								if (theApp.m_colorDefTable.IsYellow(m_pPageSource->m_PageSourceHeaders[i].m_processColors[j]))
									crColor = YELLOW;		
								else
									if (theApp.m_colorDefTable.IsKey(m_pPageSource->m_PageSourceHeaders[i].m_processColors[j]))
										crColor = BLACK;		
						m_ColorInfoListBox.AddColorItem(crColor, m_pPageSource->m_PageSourceHeaders[i].m_processColors[j], -1); 
					}
					//m_ColorInfoListBox.AddColorItem(m_pPageSource->m_PageSourceHeaders[i].m_rgb, strColorName, -1); 
				}
				else
					m_ColorInfoListBox.AddColorItem(m_pPageSource->m_PageSourceHeaders[i].m_rgb, strColorName, i); 

				if (m_pPageSourceHeader == &m_pPageSource->m_PageSourceHeaders[i])
					nCurSel = m_ColorInfoListBox.GetCount() - 1;

				for (int j = 0; j < m_pPageSource->m_PageSourceHeaders[i].m_spotColors.GetSize(); j++)
					m_ColorInfoListBox.AddColorItem(m_pPageSource->m_spotcolorDefList.GetAlternateRGB(m_pPageSource->m_PageSourceHeaders[i].m_spotColors[j]), 
													m_pPageSource->m_PageSourceHeaders[i].m_spotColors[j], -1); 
					//m_ColorInfoListBox.AddColorItem(theApp.m_colorDefTable.FindColorRef(m_pPageSource->m_PageSourceHeaders[i].m_spotColors[j]), m_pPageSource->m_PageSourceHeaders[i].m_spotColors[j], -1); 

				if (m_pPageSource->m_PageSourceHeaders[i].m_strColorName.CompareNoCase(_T("Composite")) == 0)
					bComposite = TRUE;
				else
					bSeparated = TRUE;
			}
		}
		m_ColorInfoListBox.SetCurSel(nCurSel);

		CString strComp = _T("Composite");
		CString strSep;
		strSep.LoadString(IDS_SEPARATION);
		strTitle = (bComposite && bSeparated) ? (strComp + " + " + strSep) : ( (bComposite) ? strComp : strSep );
		GetDlgItem(IDC_COLORINFO_STATIC)->SetWindowText(strTitle);
		m_ColorInfoListBox.m_nSepStatus = (bComposite && bSeparated) ? CColorDefinition::Mixed : 
										  ( (bComposite) ? CColorDefinition::Unseparated : CColorDefinition::Separated );

		m_pImageList = new (CImageList);
		m_pImageList->Create(16, 16, TRUE, 4, 4);
		m_pImageList->Add(theApp.LoadIcon(IDI_LINK));

		CListCtrl* pCtrl = (CListCtrl*)GetDlgItem(IDC_ASSIGNED_PAGES);
		pCtrl->SetImageList(m_pImageList, LVSIL_SMALL);
		CRect rect;
		pCtrl->GetWindowRect(&rect);

		LVITEM lvitem;
		lvitem.iItem	= 0;
		lvitem.iSubItem = 0;
		lvitem.iImage   = 0;
		CPageTemplate* pTemplate = m_pPageSourceHeader->GetFirstPageTemplate();
		if (pTemplate)
		{
			CString strPagina, strSize;
			strPagina.LoadString(IDS_PAGELISTHEADER_PAGENAME);
			strSize.LoadString	(IDS_PAGELISTHEADER_FORMAT);
			pCtrl->InsertColumn(0, strPagina, LVCFMT_LEFT, rect.Width()/2, 0);
			pCtrl->InsertColumn(1, strSize,   LVCFMT_LEFT, rect.Width()/2 - 4, 0);
		}
		else
		{
			CString string;
			string.LoadString(IDS_NO_PAGE_ASSIGNED);
			pCtrl->ModifyStyle(LVS_REPORT, LVS_SMALLICON);
			pCtrl->InsertColumn(0, _T(""), LVCFMT_LEFT, rect.Width() - 4, 0);
			lvitem.mask	   = LVIF_TEXT;
			lvitem.pszText = string.GetBuffer(MAX_TEXT);
			pCtrl->InsertItem(&lvitem); 
		}

		while (pTemplate)
		{
			lvitem.iSubItem = 0;
			lvitem.mask	    = LVIF_TEXT | LVIF_IMAGE;
			lvitem.pszText  = pTemplate->m_strPageID.GetBuffer(MAX_TEXT);
			pCtrl->InsertItem(&lvitem); 

			CString		   strMeasureFormat, string;
			CLayoutObject* pObject = pTemplate->GetLayoutObject();
			strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
			float fWidth  = (pObject) ? pObject->GetRealWidth()  : ((pDoc) ? pTemplate->GetProductPart().m_fPageWidth  : 210.0f);
			float fHeight = (pObject) ? pObject->GetRealHeight() : ((pDoc) ? pTemplate->GetProductPart().m_fPageHeight : 297.0f);
			string.Format((LPCTSTR)strMeasureFormat, MeasureFormat(fWidth), MeasureFormat(fHeight));

			lvitem.mask	    = LVIF_TEXT;
			lvitem.iSubItem = 1;
			lvitem.pszText  = string.GetBuffer(MAX_TEXT);
			pCtrl->SetItem(&lvitem); 

			pTemplate = m_pPageSourceHeader->GetNextPageTemplate(pTemplate);

			lvitem.iItem++;
		}
	}

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgPageSourceProps::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	if (!m_pPageSource || !m_pPageSourceHeader)
		return;

	CRect previewFrameRect;
	m_PagePreviewFrame.GetClientRect(previewFrameRect);
	m_PagePreviewFrame.MapWindowPoints(this, previewFrameRect);

	CRect outBBox, trimBox;
	outBBox.left   = 0; 
	outBBox.top    = 0; 
	outBBox.right  = (int)((m_pPageSourceHeader->m_fBBoxRight - m_pPageSourceHeader->m_fBBoxLeft)   / PICA_POINTS);
	outBBox.bottom = (int)((m_pPageSourceHeader->m_fBBoxTop   - m_pPageSourceHeader->m_fBBoxBottom) / PICA_POINTS); 

	trimBox.left   = (int)((m_pPageSourceHeader->m_fTrimBoxLeft - m_pPageSourceHeader->m_fBBoxLeft)	  / PICA_POINTS);
	trimBox.top	   = (int)((m_pPageSourceHeader->m_fBBoxTop		- m_pPageSourceHeader->m_fTrimBoxTop) / PICA_POINTS);
	trimBox.right  = trimBox.left + (int)((m_pPageSourceHeader->m_fTrimBoxRight - m_pPageSourceHeader->m_fTrimBoxLeft)   / PICA_POINTS);	  
	trimBox.bottom = trimBox.top  + (int)((m_pPageSourceHeader->m_fTrimBoxTop   - m_pPageSourceHeader->m_fTrimBoxBottom) / PICA_POINTS); 

	float fScale;
	if ((float)outBBox.Height() / (float)outBBox.Width() >= 
		(float)previewFrameRect.Height() / (float)previewFrameRect.Width())
		fScale = (float)outBBox.Height() / (float)previewFrameRect.Height();
	else
		fScale = (float)outBBox.Width()  / (float)previewFrameRect.Width();
	outBBox.left   = (int)((float)outBBox.left   / fScale + 0.5f); 
	outBBox.bottom = (int)((float)outBBox.bottom / fScale + 0.5f); 
	outBBox.right  = (int)((float)outBBox.right  / fScale + 0.5f); 
	outBBox.top    = (int)((float)outBBox.top    / fScale + 0.5f); 
	trimBox.left   = (int)((float)trimBox.left   / fScale + 0.5f); 
	trimBox.bottom = (int)((float)trimBox.bottom / fScale + 0.5f); 
	trimBox.right  = (int)((float)trimBox.right  / fScale + 0.5f); 
	trimBox.top    = (int)((float)trimBox.top    / fScale + 0.5f); 
	outBBox += previewFrameRect.TopLeft();
	trimBox += previewFrameRect.TopLeft();

	CPen*	pOldPen	  = (CPen*)	 dc.SelectStockObject(NULL_PEN);
	CBrush*	pOldBrush = (CBrush*)dc.SelectStockObject(WHITE_BRUSH);
	dc.Rectangle(outBBox);

	CString strPreviewFile;
	m_nHeaderIndex = m_pPageSource->GetHeaderIndex(m_pPageSourceHeader);
	strPreviewFile = m_pPageSource->GetPreviewFileName(m_nHeaderIndex + 1);

	if ( ! CPageListView::DrawPreview(&dc, outBBox, TOP, 0.0f, strPreviewFile))
	{
		CBrush*	pOldBrush = (CBrush*)dc.SelectStockObject(WHITE_BRUSH);
		CPen*	pOldPen	  = (CPen*)  dc.SelectStockObject(BLACK_PEN);
		dc.Rectangle(previewFrameRect);
		dc.SelectObject(pOldPen);
		dc.SelectObject(pOldBrush);

		CString strNoPreview;
		strNoPreview.LoadString(IDS_NO_PREVIEW);
		CFont* pOldFont = (CFont*)dc.SelectStockObject(ANSI_FIXED_FONT);
		dc.DrawText(strNoPreview, previewFrameRect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		dc.SelectObject(pOldFont);
		return;
	}

	CPen solidPen(PS_SOLID, 1, RED);
	dc.SelectObject(&solidPen);
	CPoint cp = outBBox.CenterPoint();
	dc.MoveTo(cp - CSize(8, 0));
	dc.LineTo(cp + CSize(8, 0));
	dc.MoveTo(cp - CSize(0, 8));
	dc.LineTo(cp + CSize(0, 8));

	CPen dottedPen(PS_DOT, 1, BLUE);
	dc.SelectObject(&dottedPen);
	dc.SelectStockObject(HOLLOW_BRUSH);
	dc.SetBkColor(WHITE);
	dc.Rectangle(trimBox);

	dc.SelectObject(pOldPen);
	dc.SelectObject(pOldBrush);
	solidPen.DeleteObject();
	dottedPen.DeleteObject();

	outBBox.InflateRect(2, 2);
	dc.DrawEdge(outBBox, EDGE_SUNKEN, BF_RECT);
}

void CDlgPageSourceProps::OnSelchangeSpotcolorlist() 
{
	UpdateData(TRUE);

	int nSel;
	if ((nSel = m_ColorInfoListBox.GetCurSel()) == LB_ERR)
		return;

	nSel = (int)m_ColorInfoListBox.GetItemData(nSel);
	if (nSel == -1)	// spot color clicked
		return;
	if (nSel >= m_pPageSource->m_PageSourceHeaders.GetSize())
		return;

	m_pPageSourceHeader = &m_pPageSource->m_PageSourceHeaders[m_nHeaderIndex = nSel];
	m_pPageSource->InitBBox(m_pPageSourceHeader);

	CRect previewFrameRect;
	m_PagePreviewFrame.GetClientRect(previewFrameRect);
	m_PagePreviewFrame.MapWindowPoints(this, previewFrameRect);
	previewFrameRect.InflateRect(2,2);
	InvalidateRect(previewFrameRect);
	UpdateWindow();

	CListCtrl* pCtrl = (CListCtrl*)GetDlgItem(IDC_ASSIGNED_PAGES);
	while (pCtrl->GetHeaderCtrl()->GetItemCount())
		pCtrl->DeleteColumn(0);
	pCtrl->DeleteAllItems();

	CRect rect;
	pCtrl->GetWindowRect(&rect);

	LVITEM lvitem;
	lvitem.iItem	= 0;
	lvitem.iSubItem = 0;
	lvitem.iImage   = 0;
	CPageTemplate* pTemplate = m_pPageSourceHeader->GetFirstPageTemplate();
	if (pTemplate)
	{
		CString strPagina, strSize;
		strPagina.LoadString(IDS_PAGELISTHEADER_PAGENAME);
		strSize.LoadString	(IDS_PAGELISTHEADER_FORMAT);
		pCtrl->ModifyStyle(LVS_SMALLICON, LVS_REPORT);
		pCtrl->InsertColumn(0, strPagina, LVCFMT_LEFT, rect.Width()/2, 0);
		pCtrl->InsertColumn(1, strSize,   LVCFMT_LEFT, rect.Width()/2 - 4, 0);
	}
	else
	{
		CString string;
		string.LoadString(IDS_NO_PAGE_ASSIGNED);
		pCtrl->ModifyStyle(LVS_REPORT, LVS_SMALLICON);
		pCtrl->InsertColumn(0, _T(""), LVCFMT_LEFT, rect.Width() - 4, 0);
		lvitem.mask	   = LVIF_TEXT;
		lvitem.pszText = string.GetBuffer(MAX_TEXT);
		pCtrl->InsertItem(&lvitem); 
	}

	while (pTemplate)
	{
		lvitem.iSubItem = 0;
		lvitem.mask	    = LVIF_TEXT | LVIF_IMAGE;
		lvitem.pszText  = pTemplate->m_strPageID.GetBuffer(MAX_TEXT);
		pCtrl->InsertItem(&lvitem); 

		CImpManDoc*	   pDoc = CImpManDoc::GetDoc();
		static CString		   strMeasureFormat, string;
		CLayoutObject* pObject = pTemplate->GetLayoutObject();
		strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
		float fWidth  = (pObject) ? pObject->GetRealWidth()  : ((pDoc) ? pTemplate->GetProductPart().m_fPageWidth  : 210.0f);
		float fHeight = (pObject) ? pObject->GetRealHeight() : ((pDoc) ? pTemplate->GetProductPart().m_fPageHeight : 297.0f);
		string.Format((LPCTSTR)strMeasureFormat, MeasureFormat(fWidth), MeasureFormat(fHeight));

		lvitem.mask	    = LVIF_TEXT;
		lvitem.iSubItem = 1;
		lvitem.pszText  = string.GetBuffer(MAX_TEXT);
		pCtrl->SetItem(&lvitem); 

		pTemplate = m_pPageSourceHeader->GetNextPageTemplate(pTemplate);

		lvitem.iItem++;
	}

#ifdef CTP
		m_strDocumentTitle = m_pPageSource->GetHiResBMPFileName(m_pPageSourceHeader);
#endif

	UpdateData(FALSE);
}
