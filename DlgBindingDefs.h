#pragma once
#include "afxwin.h"
#include "BindingDefsSelection.h"


// CDlgBindingDefs-Dialogfeld

class CDlgBindingDefs : public CDialog
{
	DECLARE_DYNAMIC(CDlgBindingDefs)

public:
	CDlgBindingDefs(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgBindingDefs();

// Dialogfelddaten
	enum { IDD = IDD_BINDING_DEFS };

public:
	CString					m_strBindingName;
	int						m_nBinding;
	CBindingDefsSelection	m_bindingSelection;
	CImageList				m_bindingImageList;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeBdBindingname();
	afx_msg void OnBnClickedBdSave();
	afx_msg void OnBnClickedBdDelete();
};
