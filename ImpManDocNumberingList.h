

///////////////////////////// CNumberingList //////////////////////////////
/////////////////////////////////////////////////////////////////////////////

class CNumbering : public CObject
{
public:
    DECLARE_SERIAL( CNumbering )
	CNumbering();
	CNumbering(const CNumbering& rNumbering); // copy constructor

//Attributes:
public:
	int		m_nStartNum;
	UINT	m_nRangeTo;
	UINT	m_nRangeFrom;
	CString	m_strNumberingStyle;
	BOOL	m_bReverse;


//Operations:
public:
	const CNumbering& operator= (const CNumbering& rNumbering);
	void  Copy(const CNumbering& rNumbering);
};

template <> void AFXAPI SerializeElements <CNumbering> (CArchive& ar, CNumbering* pNumbering, INT_PTR nCount);


class CNumberingList : public CArray <CNumbering, CNumbering&> 
{
public:
    DECLARE_SERIAL( CNumberingList )
	CNumberingList();

//Attributes:
public:

//Operations:
public:
//    void Serialize( CArchive& archive );
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
