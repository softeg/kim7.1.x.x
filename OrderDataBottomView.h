#pragma once



// COrderDataBottomView-Formularansicht

class COrderDataBottomView : public CFormView
{
	DECLARE_DYNCREATE(COrderDataBottomView)

protected:
	COrderDataBottomView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~COrderDataBottomView();

public:
	enum { IDD = IDD_ORDERDATABOTTOMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedJdModifyJobdata();
};
