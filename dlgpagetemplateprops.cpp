// DlgPageTemplateProps.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgPageTemplateProps.h"
#include "PageListView.h"
#ifdef PDF
#include "PDFReader.h"
#endif
#include "PageListDetailView.h"
#include "PageListFrame.h"
#include "OleAcrobatClientItem.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPageTemplateProps dialog


CDlgPageTemplateProps::CDlgPageTemplateProps(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPageTemplateProps::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPageTemplateProps)
	m_fContentOffsetX = 0.0f;
	m_fContentOffsetY = 0.0f;
	m_fContentScaleX = 0.0f;
	m_fContentScaleY = 0.0f;
	//}}AFX_DATA_INIT
	m_bModified = FALSE;
	m_pPageTemplate		= NULL;
	m_pPageSource		= NULL;
	m_pPageSourceHeader = NULL;
}

CDlgPageTemplateProps::~CDlgPageTemplateProps()
{
	DestroyWindow();
}

void CDlgPageTemplateProps::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPageTemplateProps)
	DDX_Measure(pDX, IDC_CONTENTOFFSET_X, m_fContentOffsetX);
	DDX_Measure(pDX, IDC_CONTENTOFFSET_Y, m_fContentOffsetY);
	DDX_Text(pDX, IDC_CONTENTSCALE_X, m_fContentScaleX);
	DDV_MinMaxFloat(pDX, m_fContentScaleX, 0.f, 9999.f);
	DDX_Text(pDX, IDC_CONTENTSCALE_Y, m_fContentScaleY);
	DDV_MinMaxFloat(pDX, m_fContentScaleY, 0.f, 9999.f);
	DDX_Measure(pDX, IDC_TRIMBOX_WIDTH, m_fTrimboxWidth);
	DDX_Measure(pDX, IDC_TRIMBOX_HEIGHT, m_fTrimboxHeight);
	DDX_Control(pDX, IDC_CONTENTOFFSET_X_SPIN, m_contentOffsetXSpin);
	DDX_Control(pDX, IDC_CONTENTOFFSET_Y_SPIN, m_contentOffsetYSpin);
	DDX_Control(pDX, IDC_CONTENTSCALE_X_SPIN, m_contentScaleXSpin);
	DDX_Control(pDX, IDC_CONTENTSCALE_Y_SPIN, m_contentScaleYSpin);
	DDX_Control(pDX, IDC_TRIMBOX_WIDTH_SPIN, m_trimboxWidthSpin);
	DDX_Control(pDX, IDC_TRIMBOX_HEIGHT_SPIN, m_trimboxHeightSpin);
	//}}AFX_DATA_MAP
} 


BEGIN_MESSAGE_MAP(CDlgPageTemplateProps, CDialog)
	//{{AFX_MSG_MAP(CDlgPageTemplateProps)
	ON_BN_CLICKED(IDC_ALL_PAGES, OnAllPages)
	ON_BN_CLICKED(IDC_THIS_DOC_PAGES, OnThisDocPages)
	ON_BN_CLICKED(IDC_THIS_PAGE, OnThisPage)
	ON_BN_CLICKED(ID_APPLY_NOW, OnApplyNow)
	ON_EN_CHANGE(IDC_CONTENTOFFSET_X, OnChangeCenteroffsetX)
	ON_EN_CHANGE(IDC_CONTENTOFFSET_Y, OnChangeCenteroffsetY)
	ON_BN_CLICKED(IDC_LEFT_PAGES, OnLeftPages)
	ON_BN_CLICKED(IDC_LEFT_RIGHT_PAGES, OnLeftRightPages)
	ON_BN_CLICKED(IDC_RIGHT_PAGES, OnRightPages)
	ON_EN_CHANGE(IDC_CONTENTSCALE_X, OnChangeScaleX)
	ON_EN_CHANGE(IDC_CONTENTSCALE_Y, OnChangeScaleY)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTOFFSET_X_SPIN, OnDeltaposContentOffsetXSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTOFFSET_Y_SPIN, OnDeltaposContentOffsetYSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTSCALE_X_SPIN, OnDeltaposContentScaleXSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CONTENTSCALE_Y_SPIN, OnDeltaposContentScaleYSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_TRIMBOX_WIDTH_SPIN, OnDeltaposTrimboxWidthSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_TRIMBOX_HEIGHT_SPIN, OnDeltaposTrimboxHeightSpin)
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_PAGE_ROTATION_COMBO, OnSelchangePageRotationCombo)
	//}}AFX_MSG_MAP
	ON_EN_KILLFOCUS(IDC_TRIMBOX_WIDTH, &CDlgPageTemplateProps::OnEnKillfocusTrimboxWidth)
	ON_EN_KILLFOCUS(IDC_TRIMBOX_HEIGHT, &CDlgPageTemplateProps::OnEnKillfocusTrimboxHeight)
	ON_EN_KILLFOCUS(IDC_CONTENTSCALE_X, &CDlgPageTemplateProps::OnEnKillfocusContentscaleX)
	ON_EN_KILLFOCUS(IDC_CONTENTSCALE_Y, &CDlgPageTemplateProps::OnEnKillfocusContentscaleY)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPageTemplateProps message handlers

BOOL CDlgPageTemplateProps::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if (m_pPageTemplate)// && m_pLayoutObject)	// should run w/o layout object as well (i.e. printsheets not yet present)
	{
		int nSize = m_pPageTemplate->m_ColorSeparations.GetSize();
		for (int i = 0; i < nSize; i++)
		{
			int nColorDefinitionIndex = m_pPageTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex;

			if (i == 0)
			{
				m_pPageSource		= m_pPageTemplate->GetObjectSource		(nColorDefinitionIndex);
				m_pPageSourceHeader	= m_pPageTemplate->GetObjectSourceHeader(nColorDefinitionIndex);
				if (m_pPageSource && m_pPageSourceHeader)
				{
					CString string;
					string.Format(_T("%s [%d]"), (LPCTSTR)m_pPageSource->m_strDocumentFullpath, m_pPageSourceHeader->m_nPageNumber);
					SetDlgItemText(IDC_PAGESOURCE_DOC, string);
				}
			}
		}
		if (nSize < 1)
			SetDlgItemText(IDC_PAGESOURCE_DOC, _T(""));

		m_fContentOffsetX	 = m_pPageTemplate->GetContentOffsetX();
		m_fContentOffsetY	 = m_pPageTemplate->GetContentOffsetY();
		m_fContentScaleX	 = m_pPageTemplate->m_fContentScaleX * 100.0f;
		m_fContentScaleY	 = m_pPageTemplate->m_fContentScaleY * 100.0f;

		m_fTrimboxWidth  = GetPageSourceHeaderWidth()  * m_pPageTemplate->m_fContentScaleX;
		m_fTrimboxHeight = GetPageSourceHeaderHeight() * m_pPageTemplate->m_fContentScaleY;

		int nSel = (m_pPageTemplate->m_fContentRotation ==   0.0f) ? 0 :
				   (m_pPageTemplate->m_fContentRotation ==  90.0f) ? 1 :
				   (m_pPageTemplate->m_fContentRotation == 180.0f) ? 2 : 3;

		((CComboBox*)GetDlgItem(IDC_PAGE_ROTATION_COMBO))->SetCurSel(nSel);
		OnSelchangePageRotationCombo();

		CString strTitle;
		strTitle.Format(IDS_DLGPAGEPROPS_TITLE, (LPCTSTR)m_pPageTemplate->m_strPageID);
		SetWindowText(strTitle);

		CheckRadioButton(IDC_ALL_PAGES,  IDC_THIS_PAGE,		   IDC_THIS_PAGE);
		CheckRadioButton(IDC_LEFT_PAGES, IDC_LEFT_RIGHT_PAGES, IDC_LEFT_RIGHT_PAGES);
		GetDlgItem(IDC_LEFT_PAGES)->EnableWindow(FALSE);
		GetDlgItem(IDC_RIGHT_PAGES)->EnableWindow(FALSE);
		GetDlgItem(IDC_LEFT_RIGHT_PAGES)->EnableWindow(FALSE);
	}
	else
	{
		m_pPageSource		= NULL;
		m_pPageSourceHeader = NULL;
	}

	m_contentOffsetXSpin.SetRange(0,1);;
	m_contentOffsetYSpin.SetRange(0,1);;
	m_contentScaleXSpin.SetRange(0,1);;
	m_contentScaleYSpin.SetRange(0,1);;
	m_trimboxWidthSpin.SetRange(0,1);;
	m_trimboxHeightSpin.SetRange(0,1);

	// Create the ToolTip control.
	m_tooltip.Create(this);
	m_tooltip.Activate(TRUE);
	
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

float CDlgPageTemplateProps::GetPageSourceHeaderWidth()
{
	float fWidth = 0.0f;
	if (m_pPageSourceHeader)
		fWidth  = (m_pPageSourceHeader->HasTrimBox()) ? (m_pPageSourceHeader->m_fTrimBoxRight - m_pPageSourceHeader->m_fTrimBoxLeft)   : (m_pPageSourceHeader->m_fBBoxRight - m_pPageSourceHeader->m_fBBoxLeft);
	else
		fWidth  = (m_pLayoutObject) ? m_pLayoutObject->GetRealWidth()  : 0.0f;
	return fWidth;
}

float CDlgPageTemplateProps::GetPageSourceHeaderHeight()
{
	float fHeight = 0.0f;
	if (m_pPageSourceHeader)
		fHeight = (m_pPageSourceHeader->HasTrimBox()) ? (m_pPageSourceHeader->m_fTrimBoxTop   - m_pPageSourceHeader->m_fTrimBoxBottom) : (m_pPageSourceHeader->m_fBBoxTop   - m_pPageSourceHeader->m_fBBoxBottom);
	else
		fHeight = (m_pLayoutObject) ? m_pLayoutObject->GetRealHeight() : 0.0f;
	return fHeight;
}

void CDlgPageTemplateProps::OnApplyNow()
{
	if ( ! m_bModified)
		return;

	SetNewPageTemplateData();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	pDoc->m_PageTemplateList.ReorganizeGluelineScaling();
	pDoc->SetModifiedFlag();

	theApp.UpdateView(RUNTIME_CLASS(CPageListView));
	theApp.UpdateView(RUNTIME_CLASS(CPageListDetailView));
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
}

void CDlgPageTemplateProps::SetNewPageTemplateData() 
{
	UpdateData(TRUE);

	int nID1 = GetCheckedRadioButton(IDC_ALL_PAGES,  IDC_THIS_PAGE);
	int nID2 = GetCheckedRadioButton(IDC_LEFT_PAGES, IDC_LEFT_RIGHT_PAGES);
#if defined(POSTSCRIPT) || defined(PDF)
	int nSel = ((CComboBox*)GetDlgItem(IDC_PAGE_ROTATION_COMBO))->GetCurSel();
#else
	int nSel = 0;
#endif

	if (nID1 == IDC_THIS_PAGE)
	{
		if ( (m_fContentOffsetX == 0.0f) && (m_fContentOffsetY == 0.0f) && (m_fContentScaleX == 100.0f) && (m_fContentScaleY == 100.0f) )
			m_pPageTemplate->m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
		else
			if ( (m_fContentOffsetX != m_pPageTemplate->GetContentOffsetX()) || (m_fContentOffsetY != m_pPageTemplate->GetContentOffsetY()) || (m_fContentScaleX/100.0f != m_pPageTemplate->m_fContentScaleX) || (m_fContentScaleY/100.0f != m_pPageTemplate->m_fContentScaleY) )
				m_pPageTemplate->m_nAttribute |= CPageTemplate::ScaleOffsetModified;

		m_pPageTemplate->SetContentOffsetX(m_fContentOffsetX);
		m_pPageTemplate->SetContentOffsetY(m_fContentOffsetY);
		m_pPageTemplate->m_fContentScaleX = m_fContentScaleX / 100.0f;
		m_pPageTemplate->m_fContentScaleY = m_fContentScaleY / 100.0f;
		switch (nSel)
		{
		case 0: m_pPageTemplate->m_fContentRotation =   0.0f; break; case 1: m_pPageTemplate->m_fContentRotation =  90.0f; break;
		case 2: m_pPageTemplate->m_fContentRotation = 180.0f; break; case 3: m_pPageTemplate->m_fContentRotation = 270.0f; break;
		}

		if (m_pPageTemplate->m_nMultipageGroupNum >= 0)
		{
			CPageTemplate* pTemplate = m_pPageTemplate->GetFirstMultipagePartner();
			if ( (m_fContentOffsetX == 0.0f) && (m_fContentOffsetY == 0.0f) && (m_fContentScaleX == 100.0f) && (m_fContentScaleY == 100.0f) )
				pTemplate->m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
			else
				if ( (m_fContentOffsetX != pTemplate->GetContentOffsetX()) || (m_fContentOffsetY != pTemplate->GetContentOffsetY()) || (m_fContentScaleX/100.0f != pTemplate->m_fContentScaleX) || (m_fContentScaleY/100.0f != pTemplate->m_fContentScaleY) )
					pTemplate->m_nAttribute |= CPageTemplate::ScaleOffsetModified;

			pTemplate->SetContentOffsetX(m_fContentOffsetX);
			pTemplate->SetContentOffsetY(m_fContentOffsetY);
			pTemplate->m_fContentScaleX = m_fContentScaleX / 100.0f;
			pTemplate->m_fContentScaleY	= m_fContentScaleY / 100.0f;
			switch (nSel)
			{
			case 0: pTemplate->m_fContentRotation =   0.0f; break; case 1: pTemplate->m_fContentRotation =  90.0f; break;
			case 2: pTemplate->m_fContentRotation = 180.0f; break; case 3: pTemplate->m_fContentRotation = 270.0f; break;
			}
		}
	}
	else
	{
		CImpManDoc::GetDoc()->m_PageTemplateList.ResetFlags();

		int		 nIndex = 0;
		POSITION pos	= CImpManDoc::GetDoc()->m_PageTemplateList.GetHeadPosition();
		while (pos)
		{
			CPageTemplate& rPageTemplate = CImpManDoc::GetDoc()->m_PageTemplateList.CList::GetNext(pos);

			if (((nID2 == IDC_LEFT_PAGES) && !(nIndex%2)) ||
				((nID2 == IDC_RIGHT_PAGES) &&  nIndex%2))
			{
				nIndex++; 
				continue;
			}

			if (nID1 == IDC_THIS_DOC_PAGES)
			{
				if ((rPageTemplate.m_ColorSeparations.GetSize() > 0) && (m_pPageTemplate->m_ColorSeparations.GetSize() > 0))
					if (rPageTemplate.m_ColorSeparations[0].m_nPageSourceIndex ==
						m_pPageTemplate->m_ColorSeparations[0].m_nPageSourceIndex)
					{
						if ( (m_fContentOffsetX == 0.0f) && (m_fContentOffsetY == 0.0f) && (m_fContentScaleX == 100.0f) && (m_fContentScaleY == 100.0f) )
							rPageTemplate.m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
						else
							if ( (m_fContentOffsetX != rPageTemplate.GetContentOffsetX()) || (m_fContentOffsetY != rPageTemplate.GetContentOffsetY()) || (m_fContentScaleX/100.0f != rPageTemplate.m_fContentScaleX) || (m_fContentScaleY/100.0f != rPageTemplate.m_fContentScaleY) )
								rPageTemplate.m_nAttribute |= CPageTemplate::ScaleOffsetModified;
						rPageTemplate.SetContentOffsetX(m_fContentOffsetX);
						rPageTemplate.SetContentOffsetY(m_fContentOffsetY);
						rPageTemplate.m_fContentScaleX = m_fContentScaleX / 100.0f;
						rPageTemplate.m_fContentScaleY = m_fContentScaleY / 100.0f;
						rPageTemplate.m_bFlag		   = TRUE;
						switch (nSel)
						{
						case 0: rPageTemplate.m_fContentRotation =   0.0f; break; case 1: rPageTemplate.m_fContentRotation =  90.0f; break;
						case 2: rPageTemplate.m_fContentRotation = 180.0f; break; case 3: rPageTemplate.m_fContentRotation = 270.0f; break;
						}

						if (rPageTemplate.m_nMultipageGroupNum >= 0)
						{
							CPageTemplate* pTemplate	= rPageTemplate.GetFirstMultipagePartner();
							if (!pTemplate->m_bFlag)
							{
								if ( (m_fContentOffsetX == 0.0f) && (m_fContentOffsetY == 0.0f) && (m_fContentScaleX == 100.0f) && (m_fContentScaleY == 100.0f) )
									pTemplate->m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
								else
									if ( (m_fContentOffsetX != pTemplate->GetContentOffsetX()) || (m_fContentOffsetY != pTemplate->GetContentOffsetY()) || (m_fContentScaleX/100.0f != pTemplate->m_fContentScaleX) || (m_fContentScaleY/100.0f != pTemplate->m_fContentScaleY) )
										pTemplate->m_nAttribute |= CPageTemplate::ScaleOffsetModified;

								pTemplate->SetContentOffsetX(m_fContentOffsetX);
								pTemplate->SetContentOffsetY(m_fContentOffsetY);
								pTemplate->m_fContentScaleX	= m_fContentScaleX / 100.0f;
								pTemplate->m_fContentScaleY	= m_fContentScaleY / 100.0f;
								pTemplate->m_bFlag		    = TRUE;
								switch (nSel)
								{
								case 0: pTemplate->m_fContentRotation =   0.0f; break; case 1: pTemplate->m_fContentRotation =  90.0f; break;
								case 2: pTemplate->m_fContentRotation = 180.0f; break; case 3: pTemplate->m_fContentRotation = 270.0f; break;
								}
							}
						}
					}
			}
			else
				if (nID1 == IDC_ALL_PAGES)
				{
					if ( (m_fContentOffsetX == 0.0f) && (m_fContentOffsetY == 0.0f) && (m_fContentScaleX == 100.0f) && (m_fContentScaleY == 100.0f) )
						rPageTemplate.m_nAttribute &= ~CPageTemplate::ScaleOffsetModified;
					else
						if ( (m_fContentOffsetX != rPageTemplate.GetContentOffsetX()) || (m_fContentOffsetY != rPageTemplate.GetContentOffsetY()) || (m_fContentScaleX/100.0f != rPageTemplate.m_fContentScaleX) || (m_fContentScaleY/100.0f != rPageTemplate.m_fContentScaleY) )
							rPageTemplate.m_nAttribute |= CPageTemplate::ScaleOffsetModified;
					rPageTemplate.SetContentOffsetX(m_fContentOffsetX);
					rPageTemplate.SetContentOffsetY(m_fContentOffsetY);
					rPageTemplate.m_fContentScaleX = m_fContentScaleX / 100.0f;
					rPageTemplate.m_fContentScaleY = m_fContentScaleY / 100.0f;
					switch (nSel)
					{
					case 0: rPageTemplate.m_fContentRotation =   0.0f; break; case 1: rPageTemplate.m_fContentRotation =  90.0f; break;
					case 2: rPageTemplate.m_fContentRotation = 180.0f; break; case 3: rPageTemplate.m_fContentRotation = 270.0f; break;
					}
				}

			nIndex++;
		}

		CImpManDoc::GetDoc()->m_PageTemplateList.ResetFlags();
	}
}

#ifdef PDF
void CDlgPageTemplateProps::OnPrintButton() 
{
	CPageListDetailView* pPageListDetailView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
	if (! pPageListDetailView)
		return;
	if(pPageListDetailView->m_nColorSel < 0)
		return;
	if (pPageListDetailView->m_nColorSel >= m_pPageTemplate->m_ColorSeparations.GetSize())
		return;

	CString strFile = m_pPageSource->GetPreviewFileName(m_pPageTemplate->m_ColorSeparations[pPageListDetailView->m_nColorSel].m_nPageNumInSourceFile);

//	CDlgPDFReader dlg;

//	dlg.DoModal();


/*
	OleInitialize(NULL);

	BeginWaitCursor();     
	COleAcrobatClientItem* pItem = NULL;     
	TRY     
	{
        pItem = new COleAcrobatClientItem(CImpManDoc::GetDoc());         
		ASSERT_VALID(pItem);
	    CLSID clsid;
		if(FAILED(::CLSIDFromProgID(_T("Excel.Sheet"), &clsid)))
			AfxThrowMemoryException();         // Create the Acrobat embedded item
		if(!pItem->CreateNewItem(clsid))
			AfxThrowMemoryException();  // any exception will do

//        if(!pItem->CreateFromFile("G:\\out.pdf"))
//			AfxThrowMemoryException();  // any exception will do
        ASSERT_VALID(pItem);         
        pItem->DoVerb(OLEIVERB_SHOW, pPageListDetailView);  // Launch the server to view the document.
		ASSERT_VALID(pItem);
	}     
	
	CATCH(CException, e)     
	{
		if (pItem != NULL)         
		{           
			ASSERT_VALID(pItem);
			pItem->Delete();         
		}
        
		AfxMessageBox(IDP_FAILED_TO_CREATE);     
	}     
	END_CATCH
     
	EndWaitCursor();

									// DevMode struct will be freed by CPostScriptOutput destructor
	CPostScriptOutput postScriptJob(this, dlgPrint.GetDeviceName(), strOutput, dlgPrint.GetDevMode());	
	postScriptJob.PrintSinglePage(strFile, strAllDfnFile, m_pPageSource->m_strContentFilesPath,
									m_pPageSource->m_fBBoxLeft,			 m_pPageSource->m_fBBoxBottom, 
									m_pPageSource->m_fBBoxRight,		 m_pPageSource->m_fBBoxTop, 
									m_fContentOffsetX,					 m_fContentOffsetY, 
									m_fContentScaleX,					 m_fContentScaleY, 
									m_pLayoutObject->GetRealWidth(),	 m_pLayoutObject->GetRealHeight(),
									m_pPageTemplate->m_fContentRotation, pPageListDetailView->m_nColorFrameSelection);
*/
}

#else

void CDlgPageTemplateProps::OnPrintButton() 
{
	CPageListDetailView* pPageListDetailView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
	if (! pPageListDetailView)
		return;
	if(pPageListDetailView->m_nColorSel < 0)
		return;
	if (pPageListDetailView->m_nColorSel >= m_pPageTemplate->m_ColorSeparations.GetSize())
		return;

	CString strFile = m_pPageSource->GetPreviewFileName(m_pPageTemplate->m_ColorSeparations[pPageListDetailView->m_nColorSel].m_nPageNumInSourceFile, m_pPageSourceHeader);

	CString strAllDfnFile;
	strAllDfnFile.Format(_T("%sall_dfn.tmp"), m_pPageSource->m_strContentFilesPath);

	CPrintDialog dlgPrint(FALSE, PD_NOPAGENUMS|PD_USEDEVMODECOPIES, theApp.m_pMainWnd);
	if (dlgPrint.DoModal() == IDCANCEL)
		return;

	if (dlgPrint.GetDeviceName().IsEmpty())
		return;

	CString strOutput;
	if (dlgPrint.m_pd.Flags & PD_PRINTTOFILE)
	{
		// construct CFileDialog for browsing
		CString strDef(MAKEINTRESOURCE(AFX_IDS_PRINTDEFAULTEXT));
		CString strPrintDef(MAKEINTRESOURCE(AFX_IDS_PRINTDEFAULT));
		CString strFilter(MAKEINTRESOURCE(AFX_IDS_PRINTFILTER));
		CString strCaption(MAKEINTRESOURCE(AFX_IDS_PRINTCAPTION));
		CFileDialog dlg(FALSE, strDef, strPrintDef, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter);
		dlg.m_ofn.lpstrTitle = strCaption;

		if (dlg.DoModal() != IDOK)
		{
			if (dlgPrint.m_pd.hDC)
				::DeleteDC(dlgPrint.m_pd.hDC);
			return;
		}

		// set output device to resulting path name
		strOutput = dlg.GetPathName();
	}

/*
									// DevMode struct will be freed by CPostScriptOutput destructor
	CPostScriptOutput postScriptJob(this, dlgPrint.GetDeviceName(), strOutput, dlgPrint.GetDevMode());	
	postScriptJob.PrintSinglePage(strFile, strAllDfnFile, m_pPageSource->m_strContentFilesPath,
									m_pPageSource->m_fBBoxLeft,			 m_pPageSource->m_fBBoxBottom, 
									m_pPageSource->m_fBBoxRight,		 m_pPageSource->m_fBBoxTop, 
									m_fContentOffsetX,					 m_fContentOffsetY, 
									m_fContentScaleX,					 m_fContentScaleY, 
									m_pLayoutObject->GetRealWidth(),	 m_pLayoutObject->GetRealHeight(),
									m_pPageTemplate->m_fContentRotation, pPageListDetailView->m_nColorFrameSelection);
	if (dlgPrint.m_pd.hDC)
		::DeleteDC(dlgPrint.m_pd.hDC);
*/
}
#endif


void CDlgPageTemplateProps::OnOK() 
{	// Make sure not to leave the dialog uncontrolled
//	SetNewPageTemplateData();

//	CImpManDoc::GetDoc()->SetModifiedFlag();
//	CImpManDoc::GetDoc()->UpdateAllViews(NULL);
	
//	CDialog::OnOK();
}
void CDlgPageTemplateProps::OnCancel()
{	// Make sure not to leave the dialog uncontrolled
	CDialog::OnCancel();
	OnClose();
}
void CDlgPageTemplateProps::OnClose() 
{
	CPageListFrame* pParentFrame = NULL;
	CView* pView;
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();

	POSITION pos = pDoc->GetFirstViewPosition();
	while (pos != NULL)
	{
		pView = pDoc->GetNextView(pos);
		if (pView->IsKindOf(RUNTIME_CLASS(CPageListView)))
			pParentFrame = (CPageListFrame*)pView->GetParentFrame();
	}
	CDialog::OnClose();
	DestroyWindow();

	if (pParentFrame)
		pParentFrame->OnPageprops();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CDlgPageTemplateProps::OnAllPages() 
{
	GetDlgItem(IDC_LEFT_PAGES)->EnableWindow(TRUE);
	GetDlgItem(IDC_RIGHT_PAGES)->EnableWindow(TRUE);
	GetDlgItem(IDC_LEFT_RIGHT_PAGES)->EnableWindow(TRUE);
	m_bModified = TRUE;
}
void CDlgPageTemplateProps::OnThisDocPages() 
{
	GetDlgItem(IDC_LEFT_PAGES)->EnableWindow(TRUE);
	GetDlgItem(IDC_RIGHT_PAGES)->EnableWindow(TRUE);
	GetDlgItem(IDC_LEFT_RIGHT_PAGES)->EnableWindow(TRUE);
	m_bModified = TRUE;
}
void CDlgPageTemplateProps::OnThisPage() 
{
	GetDlgItem(IDC_LEFT_PAGES)->EnableWindow(FALSE);
	GetDlgItem(IDC_RIGHT_PAGES)->EnableWindow(FALSE);
	GetDlgItem(IDC_LEFT_RIGHT_PAGES)->EnableWindow(FALSE);
	m_bModified = TRUE;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CDlgPageTemplateProps::OnChangeCenteroffsetX() 
{
	m_bModified = TRUE;
}

void CDlgPageTemplateProps::OnChangeCenteroffsetY() 
{
	m_bModified = TRUE;
}

void CDlgPageTemplateProps::OnChangeScaleX() 
{
	m_bModified = TRUE;
}

void CDlgPageTemplateProps::OnChangeScaleY() 
{
	m_bModified = TRUE;
}

void CDlgPageTemplateProps::OnLeftPages() 
{
	m_bModified = TRUE;
}

void CDlgPageTemplateProps::OnLeftRightPages() 
{
	m_bModified = TRUE;
}

void CDlgPageTemplateProps::OnRightPages() 
{
	m_bModified = TRUE;
}

void CDlgPageTemplateProps::OnDeltaposContentOffsetXSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_contentOffsetXSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	OnApplyNow();

	*pResult = 1;
}

void CDlgPageTemplateProps::OnDeltaposContentOffsetYSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_contentOffsetYSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);
	
	OnApplyNow();
	
	*pResult = 1;
}

void CDlgPageTemplateProps::OnDeltaposContentScaleXSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	CString strDummy;
	SpinMeasure(m_contentScaleXSpin.GetBuddy(), pNMUpDown->iDelta, 0.1f, strDummy, TRUE);

	OnEnKillfocusContentscaleX();
	
	OnApplyNow();

	*pResult = 1;
}

void CDlgPageTemplateProps::OnDeltaposContentScaleYSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	CString strDummy;
	SpinMeasure(m_contentScaleYSpin.GetBuddy(), pNMUpDown->iDelta, 0.1f, strDummy, TRUE);
	
	OnEnKillfocusContentscaleY();
	
	OnApplyNow();

	*pResult = 1;
}

void CDlgPageTemplateProps::OnDeltaposTrimboxWidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_trimboxWidthSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);

	OnEnKillfocusTrimboxWidth();

	*pResult = 1;
}

void CDlgPageTemplateProps::OnDeltaposTrimboxHeightSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_trimboxHeightSpin.GetBuddy(), pNMUpDown->iDelta, 1.0f);

	OnEnKillfocusTrimboxHeight();

	*pResult = 1;
}

void CDlgPageTemplateProps::OnEnKillfocusContentscaleX()
{
	UpdateData(TRUE);
	m_fTrimboxWidth = (m_fContentScaleX / 100.0f) * GetPageSourceHeaderWidth();
	SetMeasure(GetDlgItem(IDC_TRIMBOX_WIDTH)->m_hWnd, m_fTrimboxWidth);		//UpdateData(FALSE) doesn't work, because it suppresses the selection of next edit field after tab
}

void CDlgPageTemplateProps::OnEnKillfocusContentscaleY()
{
	UpdateData(TRUE);
	m_fTrimboxHeight = (m_fContentScaleY / 100.0f) * GetPageSourceHeaderHeight();
	SetMeasure(GetDlgItem(IDC_TRIMBOX_HEIGHT)->m_hWnd, m_fTrimboxHeight);	//UpdateData(FALSE) doesn't work, because it suppresses the selection of next edit field after tab
}

void CDlgPageTemplateProps::OnEnKillfocusTrimboxWidth()
{
	UpdateData(TRUE);
	m_fContentScaleX = (m_fTrimboxWidth / GetPageSourceHeaderWidth()) * 100.0f;
	CString strDummy;
	SetMeasure(GetDlgItem(IDC_CONTENTSCALE_X)->m_hWnd, m_fContentScaleX, strDummy, TRUE);	//UpdateData(FALSE) doesn't work, because it suppresses the selection of next edit field after tab
}

void CDlgPageTemplateProps::OnEnKillfocusTrimboxHeight()
{
	UpdateData(TRUE);
	m_fContentScaleY = (m_fTrimboxHeight / GetPageSourceHeaderHeight()) * 100.0f;
	CString strDummy;
	SetMeasure(GetDlgItem(IDC_CONTENTSCALE_Y)->m_hWnd, m_fContentScaleY, strDummy, TRUE);	//UpdateData(FALSE) doesn't work, because it suppresses the selection of next edit field after tab
}

BOOL CDlgPageTemplateProps::DestroyWindow() 
{
	return CDialog::DestroyWindow();
}

BOOL CDlgPageTemplateProps::PreTranslateMessage(MSG* pMsg) 
{
	// Let the ToolTip process this message.
	if (m_tooltip.m_hWnd)
		m_tooltip.RelayEvent(pMsg);

	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			CWnd* pWnd = GetFocus();
			GetDlgItem(ID_APPLY_NOW)->SetFocus();	// trick to force KillFocus in order to recalc scale/trimbox relationship
			pWnd->SetFocus();
		}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgPageTemplateProps::OnSelchangePageRotationCombo() 
{
	int nIDBitmap;
	int nSel = ((CComboBox*)GetDlgItem(IDC_PAGE_ROTATION_COMBO))->GetCurSel();
	if (nSel == CB_ERR)
		return;

	switch (nSel)
	{
	case 0: nIDBitmap = IDB_PORTRAIT;	  break;
	case 1: nIDBitmap = IDB_PORTRAIT_90;  break;
	case 2: nIDBitmap = IDB_PORTRAIT_180; break;
	case 3: nIDBitmap = IDB_PORTRAIT_270; break;
	default:nIDBitmap = -1;				  break;
	}

	if ((HBITMAP)m_bmPageRotation)
		m_bmPageRotation.DeleteObject();
	m_bmPageRotation.LoadBitmap(nIDBitmap); 
	((CStatic*)GetDlgItem(IDC_PAGE_ROTATION_BITMAP))->SetBitmap((HBITMAP)m_bmPageRotation);

	m_bModified = TRUE;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Executed from PageListView

void CDlgPageTemplateProps::UpdateDialogFromPageListView()
{
	if (m_pPageTemplate)// && m_pLayoutObject)	// should run w/o layout object as well (i.e. printsheets not yet present)
	{
		int nSize = m_pPageTemplate->m_ColorSeparations.GetSize();
		for (int i = 0; i < nSize; i++)
		{
			int	nColorDefinitionIndex = m_pPageTemplate->m_ColorSeparations[i].m_nColorDefinitionIndex;
			//CColorDefinition& rColorDef				= CImpManDoc::GetDoc()->m_ColorDefinitionTable[nColorDefinitionIndex];

			if (i == 0)
			{
				m_pPageSource		= m_pPageTemplate->GetObjectSource		(nColorDefinitionIndex);
				m_pPageSourceHeader	= m_pPageTemplate->GetObjectSourceHeader(nColorDefinitionIndex);
				if (m_pPageSource && m_pPageSourceHeader)
				{
					CString string;
#if defined(POSTSCRIPT) || defined(PDF)
					string.Format(_T("%s [%d]"), (LPCTSTR)m_pPageSource->m_strDocumentFullpath, m_pPageSourceHeader->m_nPageNumber);
#else
					string.Format(_T("%s [%d]"), (LPCTSTR)m_pPageSource->m_strContentFilesPath, m_pPageSourceHeader->m_nPageNumber);
#endif
					SetDlgItemText(IDC_PAGESOURCE_DOC, string);
				}
			}
		}
		if (nSize < 1)
			SetDlgItemText(IDC_PAGESOURCE_DOC, _T(""));

		m_fContentOffsetX = m_pPageTemplate->GetContentOffsetX();
		m_fContentOffsetY = m_pPageTemplate->GetContentOffsetY();
		m_fContentScaleX  = m_pPageTemplate->m_fContentScaleX * 100.0f;
		m_fContentScaleY  = m_pPageTemplate->m_fContentScaleY * 100.0f;

		m_fTrimboxWidth  = GetPageSourceHeaderWidth()  * m_pPageTemplate->m_fContentScaleX;
		m_fTrimboxHeight = GetPageSourceHeaderHeight() * m_pPageTemplate->m_fContentScaleY;

#if defined(POSTSCRIPT) || defined(PDF)
		int nSel = (m_pPageTemplate->m_fContentRotation ==   0.0f) ? 0 :
				   (m_pPageTemplate->m_fContentRotation ==  90.0f) ? 1 :
				   (m_pPageTemplate->m_fContentRotation == 180.0f) ? 2 : 3;

		((CComboBox*)GetDlgItem(IDC_PAGE_ROTATION_COMBO))->SetCurSel(nSel);
		OnSelchangePageRotationCombo();
#endif

		CString strTitle;
		strTitle.Format(IDS_DLGPAGEPROPS_TITLE, (LPCTSTR)m_pPageTemplate->m_strPageID);
		SetWindowText(strTitle);

		CheckRadioButton(IDC_ALL_PAGES,  IDC_THIS_PAGE,		   IDC_THIS_PAGE);
		CheckRadioButton(IDC_LEFT_PAGES, IDC_LEFT_RIGHT_PAGES, IDC_LEFT_RIGHT_PAGES);
		GetDlgItem(IDC_LEFT_PAGES)->EnableWindow(FALSE);
		GetDlgItem(IDC_RIGHT_PAGES)->EnableWindow(FALSE);
		GetDlgItem(IDC_LEFT_RIGHT_PAGES)->EnableWindow(FALSE);

		GetDlgItem(ID_APPLY_NOW)->EnableWindow(TRUE);
		GetDlgItem(ID_APPLY_NOW)->ShowWindow(SW_SHOW);
	}
	else
	{
		m_pPageSource		= NULL;
		m_pPageSourceHeader = NULL;

		SetDlgItemText(IDC_PAGESOURCE_DOC, _T(""));
		m_fContentOffsetX	 = 0.0f;
		m_fContentOffsetY	 = 0.0f;
		m_fContentScaleX	 = 0.0f;
		m_fContentScaleY	 = 0.0f;
		m_fTrimboxWidth		 = 0.0f;
		m_fTrimboxHeight	 = 0.0f;
		SetWindowText(_T(""));

		GetDlgItem(ID_APPLY_NOW)->EnableWindow(FALSE);
		GetDlgItem(ID_APPLY_NOW)->ShowWindow(SW_HIDE);
	} 
	
	UpdateData(FALSE);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////

