#pragma once




class CProdDataSplitter : public CSplitterWnd
{
    DECLARE_DYNAMIC(CProdDataSplitter)
public:
	CProdDataSplitter() { }

protected:
	virtual void OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect);

    DECLARE_MESSAGE_MAP()
    afx_msg LRESULT OnNcHitTest(CPoint point);
};




// CProdDataFrame-Rahmen

class CProdDataFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CProdDataFrame)
protected:
	CProdDataFrame();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CProdDataFrame();

public:
	CProdDataSplitter m_wndSplitter;

public:
	void SwitchGraphicView();
	void SwitchTableView();


protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


