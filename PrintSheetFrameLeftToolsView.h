#pragma once

#include "OwnerDrawnButtonList.h"
#include "ProductsView.h"



// CPrintSheetFrameLeftToolsView-Formularansicht

class CPrintSheetFrameLeftToolsView : public CFormView
{
	DECLARE_DYNCREATE(CPrintSheetFrameLeftToolsView)

protected:
	CPrintSheetFrameLeftToolsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintSheetFrameLeftToolsView();

public:
	enum { IDD = IDD_PRINTSHEETFRAMELEFTTOOLSVIEW};
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


public:
	COwnerDrawnButtonList	m_toolbarButtons;
	CMenu*					m_pPopupMenu;
	CPageSource*			m_pPageSource;
	CFlatProductList		m_flatProducts;
	int						m_nProductPartIndex;


public:
	static void		UpdateBackground(CDC* pDC, CRect rcRect, CWnd* /*pWnd*/);
	CProductsView*	GetProductsView();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void OnInitialUpdate();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnNewProduct();
	afx_msg void OnNewBound();
	afx_msg void OnNewFlat();
	afx_msg void OnDeleteProduct();
	afx_msg void OnImportProducts();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnImportXML();
	afx_msg void OnImportCSV();
	afx_msg void OnImportPDF();
	afx_msg void OnImportCFF();
	afx_msg void OnPrevious();
	afx_msg void OnShowPrintSheetList();
	afx_msg void OnUpdateNewProduct(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNewBound(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNewFlat(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDeleteProduct(CCmdUI* pCmdUI);
	afx_msg void OnUpdateImportProducts(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePrevious(CCmdUI* pCmdUI);
	afx_msg void OnUpdateShowPrintSheetList(CCmdUI* pCmdUI);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};


