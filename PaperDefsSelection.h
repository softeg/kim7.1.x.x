#pragma once

class CPaperDefs : public CObject
{
    DECLARE_SERIAL( CPaperDefs )

public:
	CPaperDefs();
	CPaperDefs(CPaperDefs& rPaperDefs);
	~CPaperDefs();

protected:
	CString m_strID;

public:
	CString m_strName;
	int		m_nGrammage;
	float	m_fVolume;
	float	m_fThickness;
	int		m_nGrade;
	int		m_nType;
	float	m_fShingling;

public:
	const CPaperDefs&	operator=(const CPaperDefs& rPaperDefs);
	void				SetID(const CString& strID) { m_strID = strID; };
	CString&			GetID() { return m_strID; };
};

template <> void AFXAPI SerializeElements <CPaperDefs> (CArchive& ar, CPaperDefs* pPaperDefs, INT_PTR nCount);


class CPaperDefsList : public CList <CPaperDefs, CPaperDefs&>
{
public:
	CPaperDefsList();

public:
	BOOL LoadData(void);
	BOOL SaveData(void);
};



class CPaperDefsSelection : public CObject
{
public:
	CPaperDefsSelection(void);
	~CPaperDefsSelection(void);

public:
	BOOL InitComboBox(CWnd* pDlg, CString& rstrPaperName, int* pnGrammage, float* pfVolume, float* pfShingling, int nIDComboBox);
	void SelchangeFormatList(CWnd* pDlg, int* pnGrammage, float* pfVolume, float* pfShingling, CString& strPaperName, int nIDComboBox);
	void NewEntry	(CWnd* pDlg, int* pnGrammage, float* pfVolume, float* pfShingling, CString& rstrPaperName, int nIDComboBox);
	void ChangeEntry(CWnd* pDlg, int* pnGrammage, float* pfVolume, float* pfShingling, CString& rstrPaperName, int nIDComboBox);
	void DeleteEntry(CWnd* pDlg, int* pnGrammage, float* pfVolume, float* pfShingling, CString& rstrPaperName, int nIDComboBox);
	void AFXAPI DDX_PaperDefsCombo(CDataExchange* pDX, int nIDC, CString& strValue);
};
