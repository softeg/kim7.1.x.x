// LayoutDataWindow.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "LayoutDataWindow.h"
#include "PrintSheetViewStrippingData.h"
#include "PrintSheetView.h"
#include "PrintSheetNavigationView.h"
#include "PrintSheetPlanningView.h"
#include "GraphicComponent.h"
#include "DlgLayoutData.h"
#include "DlgProductionPressParams.h"
#include "DlgProductionCuttingParams.h"
#include "DlgProductionBindingParams.h"
#include "DlgProductionTrimmingParams.h"
#include "NewPrintSheetFrame.h"
#include "DlgFlatProductData.h"
#include "DlgBoundProduct.h"
#include "PrintSheetComponentsView.h"


// CLayoutDataWindow

IMPLEMENT_DYNAMIC(CLayoutDataWindow, CStatic)


CLayoutDataWindow::CLayoutDataWindow()
{
	m_DisplayList.Create(this, TRUE, DISPLAY_LIST_TYPE(CLayoutDataWindow)); // Ole Drag/Drop
	m_bColorsExpanded = FALSE;
}

CLayoutDataWindow::~CLayoutDataWindow()
{
}

CPrintingProfile& CLayoutDataWindow::GetPrintingProfile()
{
	static CPrintingProfile nullPrintingProfile;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullPrintingProfile;

	CPrintSheetViewStrippingData* pParent = (CPrintSheetViewStrippingData*)GetParent();

	CPrintSheet* pPrintSheet = pParent->GetActPrintSheet();
	if ( ! pPrintSheet)
		return nullPrintingProfile;
	else
		return pPrintSheet->GetPrintingProfile();
}

CPrintSheet* CLayoutDataWindow::GetPrintSheet()
{
	CPrintSheetViewStrippingData* pParent = (CPrintSheetViewStrippingData*)GetParent();
	return pParent->GetActPrintSheet();
}

CLayout* CLayoutDataWindow::GetLayout()
{
	CPrintSheetViewStrippingData* pParent = (CPrintSheetViewStrippingData*)GetParent();
	return pParent->GetActLayout();
}

CPrintComponent CLayoutDataWindow::GetActComponent()
{
	CPrintSheetView*	 pPrintSheetView  = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CNewPrintSheetFrame* pFrame			  = (pPrintSheetView) ? (CNewPrintSheetFrame*)pPrintSheetView->GetParentFrame() : NULL;
	if ( ! pFrame)
		return CPrintComponent();

	CPrintComponentsView* pView = pFrame->GetPrintComponentsView();
	if (pView)
		return pView->GetActComponent();

	return CPrintComponent();
}

CBoundProduct* CLayoutDataWindow::GetActProduct()
{
	return GetActComponent().GetBoundProduct();
}

CBoundProductPart* CLayoutDataWindow::GetActProductPart()
{
	return GetActComponent().GetBoundProductPart();
}

CFoldSheet* CLayoutDataWindow::GetActFoldSheet()
{
	return GetActComponent().GetFoldSheet();
}

CFlatProduct* CLayoutDataWindow::GetActFlatProduct()
{
	return GetActComponent().GetFlatProduct();
}


BEGIN_MESSAGE_MAP(CLayoutDataWindow, CStatic)
	//{{AFX_MSG_MAP(CLayoutDataWindow)
	ON_WM_ERASEBKGND()
	ON_WM_SETFOCUS()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_WM_PAINT()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_KILLFOCUS()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



// CLayoutDataWindow-Meldungshandler

BOOL CLayoutDataWindow::OnEraseBkgnd(CDC* pDC) 
{
    //CRect rcFrame;
    //GetClientRect(rcFrame);
	//pDC->FillSolidRect(rcFrame, RGB(227,232,238));//RGB(211,218,228));

	return TRUE; //CScrollView::OnEraseBkgnd(pDC);
}

void CLayoutDataWindow::PreSubclassWindow() 
{
	CREATE_INPLACE_EDIT_EX(InplaceEdit);

	CStatic::PreSubclassWindow();
}

void CLayoutDataWindow::OnPaint() 
{
	CPaintDC dc(this);						

	CRect rcClient;
	GetClientRect(rcClient);

	CDC	memDC;		memDC.CreateCompatibleDC(&dc);
	CBitmap bitmap;	bitmap.CreateCompatibleBitmap(&dc, rcClient.Width(), rcClient.Height());
	memDC.SelectObject(&bitmap);

	CRect rcBackGround = rcClient;
	memDC.FillSolidRect(rcBackGround, RGB(227,232,238));

	Draw(&memDC);

	dc.BitBlt(0, 0, rcClient.Width(), rcClient.Height(), &memDC, 0, 0, SRCCOPY);

	bitmap.DeleteObject();
	memDC.DeleteDC();
}

void CLayoutDataWindow::Draw(CDC* pDC) 
{
	CPrintSheetViewStrippingData* pParent = (CPrintSheetViewStrippingData*)GetParent();

	CRect rcFrame;
	pParent->GetClientRect(&rcFrame);

	CRect rcBox(rcFrame.TopLeft(), CSize(rcFrame.Width(), 1));

	CPrintSheetView*		pPrintSheetView	 = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	CPrintSheet*			pPrintSheet		 = pParent->GetActPrintSheet();
	CLayout*				pLayout			 = pParent->GetActLayout();
	CPrintGroup&			rPrintGroup		 = pParent->GetActPrintGroup();
	CPrintingProfile&		rProfile		 = GetPrintingProfile();
	if ( ! pLayout)
		return;

	CPrintComponent		   component		= pParent->GetActComponent();
	CPrintComponentsGroup* pComponentsGroup = NULL;
	if ( ! component.IsNull())
		pComponentsGroup = &component.GetPrintComponentsGroup();
	else
	{
		CImpManDoc*  pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			component = pDoc->m_printComponentsList.GetComponent(0);
			if (pDoc->m_printComponentsList.GetSize() > 0)
				pComponentsGroup = &pDoc->m_printComponentsList[0];
		}
		if (pPrintSheet) 
		{
			if ( ! pPrintSheet->GetFirstPrintComponentsGroup().IsNull())
			{
				pComponentsGroup = &pPrintSheet->GetFirstPrintComponentsGroup();
				if (pComponentsGroup->m_printComponents.GetSize() > 0)
					component = pComponentsGroup->m_printComponents[0];
			}
		}
	}

	CBoundProductPart*	pProductPart		= component.GetBoundProductPart();
	CBoundProduct&		rBoundProduct		= (pProductPart) ? pProductPart->GetBoundProduct() : CBoundProduct();
	CFoldSheet*			pFoldSheet			= component.GetFoldSheet();
	CDisplayItem*		pDILayoutObject		= (pPrintSheetView) ? pPrintSheetView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject)) : NULL;
	CDisplayItem*		pDIFoldSheet		= (pPrintSheetView) ? pPrintSheetView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet))	  : NULL;
	int					nNumLayoutObjects	= (pPrintSheetView) ? pPrintSheetView->m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetView, LayoutObject))  : NULL;
	int					nNumFoldSheets		= (pPrintSheetView) ? pPrintSheetView->m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetView, FoldSheet))	  : NULL;
	CLayoutObject*		pLayoutObject		= (nNumLayoutObjects == 1) ? ((pDILayoutObject) ? (CLayoutObject*)pDILayoutObject->m_pData : NULL) : NULL;
	int					nFoldSheetRefIndex	= (nNumFoldSheets	 == 1) ? ((pDIFoldSheet) ? (int)pDIFoldSheet->m_pData : -1) : -1;

	CalcSectionBoxes(pDC, rcFrame, pPrintSheet, pLayout, pLayoutObject, nFoldSheetRefIndex, &rProfile, &rBoundProduct, pProductPart, pFoldSheet, &rPrintGroup, &component);

	m_DisplayList.Invalidate();	// always full item registering - not time critical, otherwise invalidation needed when switching between front/back/both
	m_DisplayList.BeginRegisterItems(pDC);

	CRect rcRet;
	if (pComponentsGroup)
	{
		rcRet = pComponentsGroup->DrawHeader(pDC, rcFrame, TRUE, TRUE, TRUE, NULL);
		rcBox.UnionRect(rcBox, rcRet); 
		rcFrame.top = rcBox.bottom;
	}

	DrawSectionBackground(pDC, rcFrame, m_rcLayoutDataBox);
	rcRet = DrawSectionLayoutData(pDC, rcFrame, &m_DisplayList, pPrintSheet, pLayout, &component);	rcRet.right = rcFrame.right;
	rcBox.UnionRect(rcBox, rcRet); 

	rcFrame.top = rcBox.bottom;
	DrawSectionBackground(pDC, rcFrame, m_rcImposeBox, (m_dlgComponentImposer.m_hWnd) ? RGB(225,234,253) : RGB(245,245,245));
	rcRet = DrawSectionImpose(pDC, rcFrame, &m_DisplayList, pPrintSheet);	rcRet.right = rcFrame.right;
	rcBox.UnionRect(rcBox, rcRet); 

	rcFrame.top = rcBox.bottom;
	DrawSectionBackground(pDC, rcFrame, m_rcAlignObjectsBox, (m_dlgAlignObjects.m_hWnd) ? RGB(225,234,253) : RGB(245,245,245));
	rcRet = DrawSectionAlignObjects(pDC, rcFrame, &m_DisplayList, pPrintSheet, pLayoutObject, nFoldSheetRefIndex);	rcRet.right = rcFrame.right;
	rcBox.UnionRect(rcBox, rcRet); 

	rcFrame.top = rcBox.bottom;
	DrawSectionBackground(pDC, rcFrame, m_rcPressBox);
	rcRet = DrawSectionPress(pDC, rcFrame, &m_DisplayList, pPrintSheet, pLayout, &rProfile);	rcRet.right = rcFrame.right;
	rcBox.UnionRect(rcBox, rcRet); 

	//if (rProfile.ProcessIsActive(CProductionProfile::Cutting) && rPrintGroup.HasFlatProducts())
	//{
	//	rcFrame.top = rcBox.bottom;
	//	rcRet = rProfile.m_postpress.m_cutting.DrawData(pDC, rcFrame, pPrintSheet, NULL);				rcRet.right = rcFrame.right;
	//	RegisterSectionData(pDC, rcRet, CProductionProfile::Cutting);
	//	rcBox.UnionRect(rcBox, rcRet); 
	//}
	if ( (component.m_nType == CPrintComponent::FoldSheet) || (component.m_nType == CPrintComponent::PageSection))
	{
		if ( ! rBoundProduct.IsNull())
		{
			//rcFrame.top = rcBox.bottom + nGap;
			//DrawSectionBackground(pDC, rcFrame, m_rcFoldingBox);
			//rcRet = DrawSectionFolding(pDC, rcFrame, &m_DisplayList, pPrintSheet, &rBoundProduct, pProductPart, pFoldSheet, &rPrintGroup);	rcRet.right = rcFrame.right;
			//rcBox.UnionRect(rcBox, rcRet); 

			rcFrame.top = rcBox.bottom;
			DrawSectionBackground(pDC, rcFrame, m_rcBindingBox);
			rcRet = DrawSectionBinding(pDC, rcFrame, &m_DisplayList, pPrintSheet, &rBoundProduct, &component);	rcRet.right = rcFrame.right;
			rcBox.UnionRect(rcBox, rcRet); 

			rcFrame.top = rcBox.bottom;
			DrawSectionBackground(pDC, rcFrame, m_rcTrimmingBox);
			rcRet = DrawSectionTrimming(pDC, rcFrame, &m_DisplayList, pPrintSheet, pProductPart, &component);	rcRet.right = rcFrame.right;
			rcBox.UnionRect(rcBox, rcRet); 
		}
	}

	m_DisplayList.EndRegisterItems(pDC);
} 

void CLayoutDataWindow::CalcSectionBoxes(CDC* pDC, CRect rcFrame, CPrintSheet* pPrintSheet, CLayout* pLayout, CLayoutObject* pLayoutObject, int nFoldSheetRefIndex, CPrintingProfile* pPrintingProfile, CBoundProduct* pProduct, CBoundProductPart* pProductPart, CFoldSheet* pFoldSheet, CPrintGroup* pPrintGroup, CPrintComponent* pComponent)
{
	CClientDC cdc(this);
	CDC dc; 
	dc.CreateCompatibleDC(&cdc);

	if (pComponent)
		pComponent->GetPrintComponentsGroup().DrawHeader(&dc, rcFrame, TRUE, TRUE, TRUE, NULL);

	m_rcLayoutDataBox	= DrawSectionLayoutData	 (&dc, rcFrame, NULL, pPrintSheet, pLayout, pComponent);						
	m_rcImposeBox		= DrawSectionImpose		 (&dc, rcFrame, NULL, pPrintSheet);								
	m_rcAlignObjectsBox	= DrawSectionAlignObjects(&dc, rcFrame, NULL, pPrintSheet, pLayoutObject, nFoldSheetRefIndex);								
	m_rcPressBox		= DrawSectionPress		 (&dc, rcFrame, NULL, pPrintSheet, pLayout,		 pPrintingProfile);	
	m_rcFoldingBox		= DrawSectionFolding	 (&dc, rcFrame, NULL, pPrintSheet, pProduct,	 pProductPart, pFoldSheet, pPrintGroup);		
	m_rcBindingBox		= DrawSectionBinding	 (&dc, rcFrame, NULL, pPrintSheet, pProduct,	 pComponent);		
	m_rcTrimmingBox		= DrawSectionTrimming	 (&dc, rcFrame, NULL, pPrintSheet, pProductPart, pComponent);					

	dc.DeleteDC();
}

void CLayoutDataWindow::DrawSectionBackground(CDC* pDC, CRect rcFrame, CRect rcSectionBox, COLORREF crColor)
{
	CRect rcBox = rcFrame;
	rcBox.right  = rcBox.left + rcSectionBox.Width();
	rcBox.bottom = rcBox.top  + rcSectionBox.Height() + 1;

	rcBox.InflateRect(20,0);

	CPen	pen(PS_SOLID, 1, DARKGRAY);
	CBrush  brush(crColor);
	CPen*	pOldPen	  = pDC->SelectObject(&pen);
	CBrush* pOldBrush = pDC->SelectObject(&brush);
	pDC->RoundRect(rcBox, CPoint(8, 8));
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();
}

CRect CLayoutDataWindow::DrawSectionImpose(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = 15;
	int nIndent		= 32;

	CFont boldFont, font;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	rcFrame.top  += 5;
	rcFrame.left += 5;
	CRect rcText = rcFrame; rcText.top += 2; rcText.bottom = rcText.top + nTextHeight;	rcText.left = rcFrame.left + nIndent;

	CBoundProductPart*	pProductPart = m_dlgComponentImposer.GetActBoundProductPart();
	CFoldSheet*			pFoldSheet   = m_dlgComponentImposer.GetActFoldSheet();
	CFlatProduct*		pFlatProduct = m_dlgComponentImposer.GetActFlatProduct();
	CPrintComponent		component	 = m_dlgComponentImposer.GetActComponent();

	CString strOut, strPages; 
	strOut.LoadString(IDS_IMPOSE);
	strPages.LoadString(IDS_PAGES_HEADER);
	pDC->SetTextColor(BLACK);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	pDC->SetTextColor(GUICOLOR_CAPTION);

	rcText.OffsetRect(0, nTextHeight + 8);
	if (pFlatProduct)
	{
		CRect rcImg; rcImg.left = rcFrame.left + 2; rcImg.right = rcImg.left + 25; rcImg.top = rcFrame.top + 2; rcImg.bottom = rcImg.top + 35;
		pFlatProduct->DrawThumbnail(pDC, rcImg, NULL);
		DrawTextMeasured(pDC, pFlatProduct->m_strProductName, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
	}
	else
		if (pFoldSheet)
		{
			CRect rcImg; rcImg.left = rcFrame.left + 2; rcImg.right = rcImg.left + 22; rcImg.top = rcFrame.top + 6; rcImg.bottom = rcImg.top + 35;
			pFoldSheet->DrawThumbnail(pDC, rcImg, TRUE, FALSE);
			DrawTextMeasured(pDC, pFoldSheet->m_strFoldSheetTypeName, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		}
		else
			if (pProductPart &&  ! component.IsNull())
			{
				CRect rcImg; rcImg.left = rcFrame.left; rcImg.right = rcImg.left + 25; rcImg.top = rcFrame.top + 6; rcImg.bottom = rcImg.top + 25;
				rcImg = pDoc->m_PageTemplateList.DrawSinglePage(pDC, rcImg, 0, pProductPart->GetIndex(), FALSE, FALSE, TRUE, TRUE, TRUE);
				strOut.Format(_T("%d %s |"), component.m_nNumPages, strPages);
				DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
				CRect rcText2 = rcText;	rcText2.left += pDC->GetTextExtent(strOut).cx + 5;
				pDC->SetTextColor(RGB(40,120,30));	// green 
				DrawTextMeasured(pDC, pProductPart->m_strName, rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
			}

	rcBox.right = rcFrame.right;
	rcBox.bottom += 10;

	pDC->SelectObject(&font);

	if (pDisplayList)
	{
		CRect rcDI = rcBox; rcDI.bottom += 6;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)NULL, DISPLAY_ITEM_REGISTRY(CLayoutDataWindow, OpenComponentImposer), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();

			CImage img;
			img.LoadFromResource(theApp.m_hInstance, (m_dlgComponentImposer.m_hWnd) ? IDB_MINUS : IDB_DROPDOWN);		
			if ( ! img.IsNull())
			{
				CRect rcImg = rcDI; rcImg.left = rcDI.right - 15; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcDI.top + 6; rcImg.bottom = rcImg.top + img.GetHeight();
				if (m_dlgComponentImposer.m_hWnd)
					rcImg.OffsetRect(-img.GetWidth()/2, 0);
				img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
			}

			if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
			{
				CGraphicComponent gc;
				CRect rcFeedback = pDI->m_BoundingRect; 
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.top++; rcRect.right -=2; rcRect.bottom -= 2;
				gc.DrawTransparentFrame(pDC, rcRect, LIGHTBLUE);
			}
		}
	}

	if (m_dlgComponentImposer.m_hWnd)
	{
		int nOffset1 = 0, nOffset2 = 0;
		CRect rcDlg; m_dlgComponentImposer.GetWindowRect(rcDlg); ScreenToClient(rcDlg);
		CRect rcTarget;	GetClientRect(rcTarget); rcTarget.top = rcBox.bottom - nOffset2;
		rcDlg.OffsetRect(rcTarget.TopLeft() - rcDlg.TopLeft());
		if (pDisplayList)
			m_dlgComponentImposer.MoveWindow(rcDlg, FALSE);
		rcBox.bottom += rcDlg.Height() + nOffset1;	// nOffset1: trick due to confusion about placement !?!?
	}

	font.DeleteObject();
	boldFont.DeleteObject();

	rcBox.bottom += 5;

	return rcBox;
}

CRect CLayoutDataWindow::DrawSectionAlignObjects(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CLayoutObject* pLayoutObject, int nFoldSheetRefIndex)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	int nTextHeight = 15;

	CFont boldFont, font;
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	font.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC,  8) : Pixel2MM(pDC, 13), 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&boldFont);
	pDC->SetBkMode(TRANSPARENT); 

	rcFrame.top  += 5;
	rcFrame.left += 5;
	CRect rcText = rcFrame; rcText.top += 2; rcText.bottom = rcText.top + nTextHeight;

	CImage img; img.LoadFromResource(theApp.m_hInstance, IDB_POSITIONING);		
	if ( ! img.IsNull())
	{
		CRect rcImg; rcImg.left = rcFrame.left; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcFrame.top + 5; rcImg.bottom = rcImg.top + img.GetHeight();
		img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
		rcText.left = rcFrame.left + 32;
		rcBox.bottom = rcImg.bottom;
	}

	CString strOut; 
	strOut.LoadString(IDS_GEOMETRY);
	pDC->SetTextColor(BLACK);
	DrawTextMeasured(pDC, strOut, rcText, DT_LEFT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

	pDC->SelectObject(&font);

	pDC->SetTextColor(GUICOLOR_CAPTION);
	if (pLayoutObject)
	{
		CLayoutSide* pLayoutSide = pLayoutObject->GetLayoutSide();
		if (pLayoutSide)
		{
			CPrintSheetView* pPrintSheetView  = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
			BOOL bShowPlate = (pPrintSheetView) ? pPrintSheetView->ShowPlate() : FALSE;
			float fLeft	  = (bShowPlate) ? pLayoutObject->GetLeft()   : pLayoutObject->GetLeft()   - pLayoutSide->m_fPaperLeft;
			float fBottom = (bShowPlate) ? pLayoutObject->GetBottom() : pLayoutObject->GetBottom() - pLayoutSide->m_fPaperBottom;
			CString strWLabel, strHLabel; strWLabel.LoadString(IDS_WIDTH_SHORT); strWLabel += _T(":"); strHLabel.LoadString(IDS_HEIGHT_SHORT); strHLabel += _T(":"); 
			CString strXPos, strYPos, strWidth, strHeight;
			CPrintSheetView::TruncateNumber(strXPos, fLeft);					  CPrintSheetView::TruncateNumber(strYPos, fBottom);
			CPrintSheetView::TruncateNumber(strWidth, pLayoutObject->GetWidth()); CPrintSheetView::TruncateNumber(strHeight, pLayoutObject->GetHeight());
			
			int nTab1 = 20;
			int nTab2 = 70;
			rcText.OffsetRect(0, nTextHeight + 3);
			CRect rcText1 = rcText;	
			CRect rcText2 = rcText1;	rcText2.left = rcText1.left + nTab1;
			CRect rcText3 = rcText;		rcText3.left += nTab2;
			CRect rcText4 = rcText3;	rcText4.left = rcText3.left + nTab1;

			pDC->SetTextColor(DARKGRAY); 
			DrawTextMeasured(pDC, _T("X:"),  rcText1, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
			pDC->SetTextColor(BLACK); 
			DrawTextMeasured(pDC, strXPos,	 rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
			pDC->SetTextColor(DARKGRAY); 
			DrawTextMeasured(pDC, _T("Y:"),  rcText3, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
			pDC->SetTextColor(BLACK); 
			DrawTextMeasured(pDC, strYPos,   rcText4, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);

			rcText.OffsetRect(0, nTextHeight + 3);

			rcText1 = rcText;	
			rcText2 = rcText1;	rcText2.left = rcText1.left + nTab1;
			rcText3 = rcText;	rcText3.left += nTab2;
			rcText4 = rcText3;	rcText4.left = rcText3.left + nTab1;
			pDC->SetTextColor(DARKGRAY); 
			DrawTextMeasured(pDC, strWLabel, rcText1, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
			pDC->SetTextColor(BLACK); 
			DrawTextMeasured(pDC, strWidth,  rcText2, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
			pDC->SetTextColor(DARKGRAY); 
			DrawTextMeasured(pDC, strHLabel, rcText3, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
			pDC->SetTextColor(BLACK); 
			DrawTextMeasured(pDC, strHeight, rcText4, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS, rcBox);
		}
	}

	rcBox.right = rcFrame.right;
	rcBox.bottom += 5;

	if (pDisplayList)
	{
		CRect rcDI = rcBox; rcDI.bottom += 6;
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)NULL, DISPLAY_ITEM_REGISTRY(CLayoutDataWindow, OpenAlignObjects), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();

			img.Destroy();
			img.LoadFromResource(theApp.m_hInstance, (m_dlgAlignObjects.m_hWnd) ? IDB_MINUS : IDB_DROPDOWN);		
			if ( ! img.IsNull())
			{
				CRect rcImg = rcDI; rcImg.left = rcDI.right - 15; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcDI.top + 6; rcImg.bottom = rcImg.top + img.GetHeight();
				if (m_dlgAlignObjects.m_hWnd)
					rcImg.OffsetRect(-img.GetWidth()/2, 0);
				img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
			}

			if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
			{
				CGraphicComponent gc;
				CRect rcFeedback = pDI->m_BoundingRect; 
				rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.top++; rcRect.right -=2; rcRect.bottom -= 2;
				gc.DrawTransparentFrame(pDC, rcRect, LIGHTBLUE);
			}
		}
	}

	if (m_dlgAlignObjects.m_hWnd)
	{
		CRect rcDlg; m_dlgAlignObjects.GetWindowRect(rcDlg); ScreenToClient(rcDlg);
		CRect rcTarget;	GetClientRect(rcTarget); rcTarget.top = rcBox.bottom;
		rcDlg.OffsetRect(rcTarget.TopLeft() - rcDlg.TopLeft());
		m_dlgAlignObjects.MoveWindow(rcDlg, FALSE);
		rcBox.bottom = rcDlg.bottom;
	}

	font.DeleteObject();
	boldFont.DeleteObject();

	rcBox.bottom += 5;

	return rcBox;
}

CRect CLayoutDataWindow::DrawSectionLayoutData(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CLayout* pLayout, CPrintComponent* pComponent)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pLayout)
		return rcBox;

	rcBox = pLayout->DrawData(pDC, rcFrame, pPrintSheet, pComponent, pDisplayList); rcBox.right = rcFrame.right;
	RegisterSection(pDC, rcBox, 0);
	rcBox.bottom += 5;
	return rcBox;
}

CRect CLayoutDataWindow::DrawSectionPress(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CLayout* pLayout, CPrintingProfile* pPrintingProfile)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pPrintingProfile)
		return rcBox;

	rcBox = pPrintingProfile->m_press.DrawData(pDC, rcFrame, pLayout, pDisplayList);	rcBox.right = rcFrame.right;
	RegisterSection(pDC, rcBox, 1);
	rcBox.bottom += 5;
	return rcBox;
}
//
//CRect CLayoutDataWindow::DrawSectionCutting(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet)
//{
//	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
//	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
//	if ( ! pDoc)
//		return rcBox;
//
//	//DrawSectionBackground(pDC, Impose, rcFrame, pPrintSheet);	
//	rcBox.bottom += 5;
//	return rcBox;
//}

CRect CLayoutDataWindow::DrawSectionFolding(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CBoundProduct* pProduct, CBoundProductPart* pProductPart, CFoldSheet* pFoldSheet, CPrintGroup* pPrintGroup)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pProductPart)
		return rcBox;

	rcBox = pProductPart->m_foldingParams.DrawData(pDC, rcFrame, pFoldSheet, pProductPart, pDisplayList); rcBox.right = rcFrame.right;

	RegisterSection(pDC, rcBox, 2);
	rcBox.bottom += 5;
	return rcBox;
}

CRect CLayoutDataWindow::DrawSectionBinding(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CBoundProduct* pProduct, CPrintComponent* pComponent)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pProduct)
		return rcBox;

	rcBox = pProduct->m_bindingParams.DrawData(pDC, rcFrame, pPrintSheet, *pComponent, pDisplayList); rcBox.right = rcFrame.right;
	RegisterSection(pDC, rcBox, 3);
	rcBox.bottom += 5;
	return rcBox;
}

CRect CLayoutDataWindow::DrawSectionTrimming(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList, CPrintSheet* pPrintSheet, CBoundProductPart* pProductPart, CPrintComponent* pComponent)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1, 1));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc || ! pProductPart)
		return rcBox;

	rcBox = pProductPart->m_trimmingParams.DrawData(pDC, rcFrame, pPrintSheet, *pComponent, pDisplayList); rcBox.right = rcFrame.right;
	RegisterSection(pDC, rcBox, 4);
	rcBox.bottom += 5;
	return rcBox;
}

void CLayoutDataWindow::RegisterSection(CDC* pDC, CRect rcDI, int nSection)
{
	CImage  img; img.LoadFromResource(theApp.m_hInstance, IDB_DROPDOWN);		
	if ( ! img.IsNull())
	{
		CRect rcImg = rcDI; rcImg.left = rcDI.right - 15; rcImg.right = rcImg.left + img.GetWidth(); rcImg.top = rcDI.top + 6; rcImg.bottom = rcImg.top + img.GetHeight();
		img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);
	}

	rcDI.bottom += 6;
	CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI, (void*)nSection, DISPLAY_ITEM_REGISTRY(CLayoutDataWindow, LayoutData), NULL, CDisplayItem::ForceRegister); // register item into display list
	if (pDI)
	{
		pDI->SetMouseOverFeedback();
		pDI->SetNoStateFeedback();
		if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
		{
			CGraphicComponent gc;
			CRect rcFeedback = pDI->m_BoundingRect; 
			rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
			CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.top++; rcRect.right -=2; rcRect.bottom -= 2;
			gc.DrawTransparentFrame(pDC, rcRect, LIGHTBLUE);
		}
	}
}

void CLayoutDataWindow::ShowHideComponentImposer()
{
	Invalidate();
	UpdateWindow();
}

void CLayoutDataWindow::OnSetFocus(CWnd* pOldWnd) 
{
	CStatic::OnSetFocus(pOldWnd);
}


void CLayoutDataWindow::OnKillFocus(CWnd* pNewWnd) 
{											   
	CStatic::OnKillFocus(pNewWnd);			   

	m_DisplayList.DeselectAll();
}

void CLayoutDataWindow::OnLButtonDown(UINT nFlags, CPoint point) 
{
    SetFocus();	// In order to deactivate eventually active InplaceEdit

	m_DisplayList.OnLButtonDown(point); // pass message to display list

	CStatic::OnLButtonDown(nFlags, point);
}


void CLayoutDataWindow::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point); // pass second click to display list

	CStatic::OnLButtonDblClk(nFlags, point);
}

void CLayoutDataWindow::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CStatic::OnMouseMove(nFlags, point);
}

LRESULT CLayoutDataWindow::OnMouseLeave(WPARAM wParam, LPARAM lParam)
{
	m_DisplayList.OnMouseLeave();
	return TRUE;
}

void CLayoutDataWindow::OnDestroy() 
{		
	m_DisplayList.m_List.RemoveAll();
	m_DisplayList.m_ListOld.RemoveAll();
	CStatic::OnDestroy();		
}


/////////////////////////////////////////////////////////////////////////////
// Messages back from display list

BOOL CLayoutDataWindow::OnSelectLayoutData(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{
	int nSection = (int)pDI->m_pData;

	CRect rcWin; 
	GetWindowRect(rcWin); 
	CPoint ptInitialPos = CPoint(pDI->m_BoundingRect.left, pDI->m_BoundingRect.top + 20) + rcWin.TopLeft();

	CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
	CDialog* pDlg = NULL;
	int		 nID  = -1;
	switch (nSection)
	{
	case 0:	pDlg = new CDlgLayoutData;				((CDlgLayoutData*)pDlg)->m_ptInitialPos					= ptInitialPos; nID = IDD_LAYOUTDATA;					break;
	case 1:	pDlg = new CDlgProductionPressParams;	((CDlgProductionPressParams*)pDlg)->m_ptInitialPos		= ptInitialPos; nID = IDD_PRODUCTION_PRESSPARAMS;		break;
	case 2:	pDlg = new CDlgSelectFoldSchemePopup;	((CDlgSelectFoldSchemePopup*)pDlg)->m_ptInitialPos		= ptInitialPos; nID = IDD_SELECT_FOLDSCHEME_POPUP;		break;
	case 3:	pDlg = new CDlgProductionBindingParams;	((CDlgProductionBindingParams*)pDlg)->m_ptInitialPos	= ptInitialPos; nID = IDD_PRODUCTION_BINDINGPARAMS;		break;
	case 4:	pDlg = new CDlgProductionTrimmingParams;((CDlgProductionTrimmingParams*)pDlg)->m_ptInitialPos	= ptInitialPos; nID = IDD_PRODUCTION_TRIMMINGPARAMS;	break;
	}

	if ( ! pDlg)
		return FALSE;

	pDI->m_nState = CDisplayItem::Normal;

	CNewPrintSheetFrame* pFrame = NULL;
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
	{
		pView->UndoSave();
		pFrame = (CNewPrintSheetFrame*)pView->GetParentFrame();
	}

	if ( (nID == IDD_PRODUCTION_BINDINGPARAMS) || (nID == IDD_PRODUCTION_TRIMMINGPARAMS) )
	{
		CPrintSheet*	pPrintSheet = GetPrintSheet();
		CPrintComponent component	= GetActComponent();
		if (component.IsNull())
		{
			CImpManDoc*  pDoc = CImpManDoc::GetDoc();
			if (pDoc)
				component = pDoc->m_printComponentsList.GetComponent(0);

			if (pPrintSheet) 
			{
				if ( ! pPrintSheet->GetFirstPrintComponentsGroup().IsNull())
				{
					if (pPrintSheet->GetFirstPrintComponentsGroup().m_printComponents.GetSize() > 0)
						component = pPrintSheet->GetFirstPrintComponentsGroup().m_printComponents[0];
				}
			}
		}
		CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
		if (pPrintComponentsView)
			pPrintComponentsView->SetActComponent(component);
	}

	CBoundProductPart*	pProductPart		= GetActProductPart();
	CBoundProduct*		pBoundProduct		= GetActProduct();
	CPrintingProfile	oldProfile			= GetPrintingProfile();
	CBindingProcess		oldBindingProcess	= (pBoundProduct) ? pBoundProduct->m_bindingParams  : CBindingProcess();
	CTrimmingProcess	oldTrimmingProcess	= (pProductPart)  ? pProductPart->m_trimmingParams  : CTrimmingProcess();

	pDlg->Create(nID, this);

	int nRet = pDlg->RunModalLoop();
	if (nRet == IDCANCEL)
	{
		if (nID == IDD_PRODUCTION_PRESSPARAMS)
			GetPrintingProfile() = oldProfile;
		else
			if ( (nID == IDD_PRODUCTION_BINDINGPARAMS) && pBoundProduct)
				pBoundProduct->m_bindingParams = oldBindingProcess;
			else
				if ( (nID == IDD_PRODUCTION_TRIMMINGPARAMS) && pBoundProduct)
					pProductPart->m_trimmingParams = oldTrimmingProcess;
	}
	else
	{
		BOOL bChanged = FALSE;
		if (nID == IDD_PRODUCTION_PRESSPARAMS)
		{
			if (GetPrintingProfile().ApplyModifications(oldProfile))
				bChanged = TRUE;
		}
		else
			if ( (nID == IDD_PRODUCTION_BINDINGPARAMS) && pBoundProduct)
				pBoundProduct->m_bindingParams.ApplyModifications(GetLayout(), oldBindingProcess, bChanged);
			else
				if ( (nID == IDD_PRODUCTION_TRIMMINGPARAMS) && pBoundProduct)
					pProductPart->m_trimmingParams.ApplyModifications(GetLayout(), oldTrimmingProcess, bChanged);

		if (bChanged)
		{
			if (CImpManDoc::GetDoc())
				CImpManDoc::GetDoc()->SetModifiedFlag();

			if (m_dlgComponentImposer.m_hWnd)
				m_dlgComponentImposer.InitData(FALSE);
			
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
			theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
		}
	}
	delete pDlg;

	Invalidate();
	UpdateWindow();

	return TRUE;
}

BOOL CLayoutDataWindow::OnActivateLayoutData(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{
	pDI->m_nState = CDisplayItem::Normal;

	return TRUE;
}

BOOL CLayoutDataWindow::OnMouseMoveLayoutData(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.right -=2; rcRect.bottom -= 2;
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CLayoutDataWindow::OnSelectLayoutNumSheets(CDisplayItem* pDI, CPoint point, LPARAM lParam)	
{
	pDI->m_nState = CDisplayItem::Normal;

	if (pDI->m_AuxRect.PtInRect(point))
	{
		OnActivateLayoutNumSheets(pDI, point, lParam);
		return TRUE;
	}

	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return FALSE;

	CRect rcRect1 = pDI->m_BoundingRect; rcRect1.left = pDI->m_AuxRect.right; rcRect1.right = rcRect1.left + 20;
	CRect rcRect2 = rcRect1; rcRect2.OffsetRect(rcRect1.Width(), 0);
	CRect rcRect3 = rcRect2; rcRect3.OffsetRect(rcRect2.Width(), 0);
	CRect rcRect4 = rcRect3; rcRect4.OffsetRect(rcRect3.Width(), 0);

	int nNewNumSheets = 0;
	if (rcRect1.PtInRect(point))
		nNewNumSheets = 1;
	else
	if (rcRect2.PtInRect(point))
		nNewNumSheets = pLayout->GetNumPrintSheetsBasedOn() - 1;
	else
	if (rcRect3.PtInRect(point))
		nNewNumSheets = pLayout->GetNumPrintSheetsBasedOn() + 1;
	else
	if (rcRect4.PtInRect(point))
		nNewNumSheets = pLayout->GetMaxNumPrintSheetsBasedOn();
	else
		return FALSE;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	ChangeNumSheets(nNewNumSheets);

	return TRUE;
}

void CLayoutDataWindow::ChangeNumSheets(int nNewNumSheets)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintSheet*	pPrintSheet  = GetPrintSheet();
	CLayout*		pLayout		 = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;

	int nPrevActPrintSheetNum = pPrintSheet->GetNumberOnly();
	int nOldNumSheets		  = pLayout->GetNumPrintSheetsBasedOn();
	BOOL bSelectNewPrintSheet = FALSE;
	int  nChanged			  = 0;

	if (nNewNumSheets > nOldNumSheets)
	{
		nChanged = pLayout->AddPrintSheetInstance(nNewNumSheets - nOldNumSheets);
	}
	else
	{
		if (nOldNumSheets > 1)
		{
			nChanged = pLayout->RemovePrintSheetInstance(nOldNumSheets - nNewNumSheets, TRUE);
			pPrintSheet = pDoc->m_PrintSheetList.GetPrintSheetFromNumber(nPrevActPrintSheetNum);
			bSelectNewPrintSheet = ( ! pPrintSheet) ? TRUE : ( (pPrintSheet->GetLayout() != pLayout) ? TRUE : FALSE);	// maybe last act printsheet is no longer valid -> select last of this layout
			if (bSelectNewPrintSheet)
				pPrintSheet = pLayout->GetLastPrintSheet();
		}
	}

	if (nChanged <= 0)
		return;

	pDoc->m_Bookblock.Reorganize(NULL, NULL);
	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->SetModifiedFlag();										
	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();

	if (bSelectNewPrintSheet)
	{
		CPrintSheetNavigationView* pPrintSheetNavigationView = (CPrintSheetNavigationView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
		if (pPrintSheetNavigationView)
		{
			pPrintSheetNavigationView->SetActPrintSheet(pPrintSheet);
			CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
			if (pPrintComponentsView)
			{
				CPrintComponent component = pDoc->m_printComponentsList.FindPrintSheet(pPrintSheet);
				pPrintComponentsView->SetActComponent(component);
			}
		}
	}

	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetNavigationView),	NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetNavigationView));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
	theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetPlanningView),	NULL, 0, NULL, TRUE);
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView));
	theApp.UpdateView(RUNTIME_CLASS(CProductsView));	
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetComponentsView));
}

BOOL CLayoutDataWindow::OnActivateLayoutNumSheets(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{
	CDisplayItem di = *pDI;

	//m_DisplayList.DeactivateItem(*pDI);
 //   m_DisplayList.InvalidateItem(pDI);
	//UpdateWindow();

	//CRect rect = di.m_BoundingRect; //rect.InflateRect(1,1);

	//CPrintSheet* pPrintSheet = (CPrintSheet*)di.m_pData;
	//if (pPrintSheet)
	//{
	//	CString strQuantity; strQuantity.Format(_T("%d"), pPrintSheet->m_nQuantityTotal);
	//	m_InplaceEdit.Activate(rect, strQuantity, CInplaceEdit::FixedSize, 0, di);
	//}

	return TRUE;
}

BOOL CLayoutDataWindow::OnMouseMoveLayoutNumSheets(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; rcRect.right = pDI->m_AuxRect.left; rcRect.bottom -= 1;
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);

				rcRect = pDI->m_AuxRect;
				CPrintSheetView::FillTransparent(pDC, rcRect, WHITE, TRUE, 120, 1);
				CPen pen(PS_SOLID, 1, LIGHTBLUE);
				CPen*	pOldPen	  = pDC->SelectObject(&pen);
				CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
				pDC->Rectangle(rcRect);
				pDC->SelectObject(pOldPen);
				pDC->SelectObject(pOldBrush);
				pen.DeleteObject();

				//CLayout* pLayout = GetLayout();
				//CImage   img;
				//COLORREF crColor = RGB(250, 230, 200);
				//CString  strMaxNum; strMaxNum.Format(_T("%d"), (pLayout) ? pLayout->GetMaxNumPrintSheetsBasedOn() : 1);

				//CRect rcImg = pDI->m_BoundingRect; rcImg.left = rcRect.right + 4; rcImg.right = rcImg.right = rcImg.left + pDC->GetTextExtent(strMaxNum).cx + 10; rcImg.bottom -= 3;
				//gc.DrawTitleBar(pDC, rcImg, _T("1"), crColor, crColor, 0, 1);

				//img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_MINUS_1));
				//rcImg.OffsetRect(23, 0);
				//img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top + 2, img.GetWidth(), img.GetHeight(), WHITE);

				//img.Destroy();
				//img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_PLUS_1));
				//rcImg.OffsetRect(16, 0);
				//img.TransparentBlt(pDC->m_hDC, rcImg.left, rcImg.top + 2, img.GetWidth(), img.GetHeight(), WHITE);

				//rcImg.OffsetRect(20, 0); rcImg.right = rcImg.left + pDC->GetTextExtent(strMaxNum).cx + 10;
				//gc.DrawTitleBar(pDC, rcImg, strMaxNum, crColor, crColor, 0, 1);

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CLayoutDataWindow::OnSelectPrintSheetQuantity(CDisplayItem* pDI, CPoint point, LPARAM lParam)	
{
	OnActivatePrintSheetQuantity(pDI, point, lParam);

	return TRUE;
}

BOOL CLayoutDataWindow::OnActivatePrintSheetQuantity(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)	
{
	CDisplayItem di = *pDI;

	m_DisplayList.DeactivateItem(*pDI);
    m_DisplayList.InvalidateItem(pDI);
	UpdateWindow();

	CRect rect = di.m_BoundingRect; //rect.InflateRect(1,1);

	CPrintSheet* pPrintSheet = (CPrintSheet*)di.m_pData;
	if (pPrintSheet)
	{
		CString strQuantity; strQuantity.Format(_T("%d"), pPrintSheet->m_nQuantityTotal);
		m_InplaceEdit.Activate(rect, strQuantity, CInplaceEdit::FixedSize, 0, di);
	}

	return TRUE;
}

BOOL CLayoutDataWindow::OnMouseMovePrintSheetQuantity(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; rcRect.right = pDI->m_AuxRect.left; rcRect.bottom -= 1;
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);

				rcRect = pDI->m_AuxRect;
				CPrintSheetView::FillTransparent(pDC, rcRect, WHITE, TRUE, 120, 1);
				CPen pen(PS_SOLID, 1, LIGHTBLUE);
				CPen*	pOldPen	  = pDC->SelectObject(&pen);
				CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(HOLLOW_BRUSH);
				pDC->Rectangle(rcRect);
				pDC->SelectObject(pOldPen);
				pDC->SelectObject(pOldBrush);
				pen.DeleteObject();

				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

void CLayoutDataWindow::OnKillFocusInplaceEdit() 		// Message from InplaceEdit
{
}

void CLayoutDataWindow::OnCharInplaceEdit(UINT nChar) // Message from InplaceEdit
{
	if (nChar == VK_RETURN)	
	{
		if (m_InplaceEdit.m_di.m_strItemTypeID == DISPLAY_ITEM_TYPEID(CLayoutDataWindow, PrintSheetQuantity))
		{
			UpdatePrintSheetQuantity();
			//CDisplayItem* pDI = m_DisplayList.GetNextItem(&m_InplaceEdit.m_di, m_InplaceEdit.m_di.m_strItemTypeID);
			//if (pDI)
			//{
			//	m_DisplayList.DeselectAll(m_InplaceEdit.m_di.m_strItemTypeID);
			//	m_DisplayList.InvalidateItem();
			//	OnActivatePrintSheetQuantity(pDI, CPoint(0,0), 0);
			//	pDI->m_nState = CDisplayItem::Selected;
			//}
			return;
		}
	}
}

void CLayoutDataWindow::UpdatePrintSheetQuantity()
{
	CPrintSheet* pPrintSheet = (CPrintSheet*)m_InplaceEdit.m_di.m_pData;
	int nValue = _ttoi(m_InplaceEdit.GetString());

	m_InplaceEdit.Deactivate();

	if ( ! pPrintSheet)
		return;
	if ( (pPrintSheet->m_nQuantityTotal == nValue) && (m_DisplayList.GetNumSelectedItems(DISPLAY_ITEM_TYPEID(CPrintSheetPlanningView, PrintSheetQuantity)) == 1) )
		return;

	int nQuantityTotal = nValue;

	pPrintSheet->m_nQuantityTotal = nValue;
	pPrintSheet->CalcTotalQuantity();

	CImpManDoc::GetDoc()->SetModifiedFlag(TRUE, NULL, FALSE);	

	if (m_dlgComponentImposer.m_hWnd)
		m_dlgComponentImposer.InitData(FALSE);

	CRect rcInvalidate(0, m_InplaceEdit.m_di.m_BoundingRect.top, 10000, m_InplaceEdit.m_di.m_BoundingRect.bottom);
	InvalidateRect(rcInvalidate);
	//m_DisplayList.InvalidateItem(&m_InplaceEdit.m_di);
	UpdateWindow();	

	theApp.UpdateView(RUNTIME_CLASS(CProductsView));
}

BOOL CLayoutDataWindow::OnSelectExpandColors(CDisplayItem* pDI, CPoint point, LPARAM lParam)	
{
	pDI->m_nState = CDisplayItem::Normal;

	m_bColorsExpanded = ! m_bColorsExpanded;

	CRect rcClient; GetClientRect(rcClient); rcClient.top = pDI->m_BoundingRect.top;
	InvalidateRect(rcClient);
	m_DisplayList.Invalidate();

	if ( ! m_bColorsExpanded)
	{
		//m_DisplayList.DeselectAll(FALSE);	// DeselectAll() doesn't work because of CtrlDeselectType
		CDisplayItem* pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CLayoutDataWindow, PrintColor));
		while (pDI)
		{
			pDI->m_nState = CDisplayItem::Normal;
			pDI = m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CLayoutDataWindow, PrintColor));
		}
		theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));
	}

	return TRUE;
}

BOOL CLayoutDataWindow::OnMouseMoveExpandColors(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.right -=2; rcRect.bottom -= 2;
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CLayoutDataWindow::OnSelectPrintColor(CDisplayItem* pDI, CPoint point, LPARAM lParam)	
{
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));

	return TRUE;
}

BOOL CLayoutDataWindow::OnActivatePrintColor(CDisplayItem* pDI, CPoint point, LPARAM lParam)	
{
	pDI->m_nState = CDisplayItem::Normal;
	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetView));

	return TRUE;
}

BOOL CLayoutDataWindow::OnMouseMovePrintColor(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.right -=2; rcRect.bottom -= 2;
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CLayoutDataWindow::OnSelectOpenComponentImposer(CDisplayItem* pDI, CPoint point, LPARAM lParam)	
{
	if (pDI)
		pDI->m_nState = CDisplayItem::Normal;

	CPrintComponentsView* pComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));

	CRect rcTarget;	GetClientRect(rcTarget); rcTarget.top = pDI->m_BoundingRect.bottom;
	if ( ! m_dlgComponentImposer.m_hWnd)
	{
		//if (m_dlgAlignObjects.m_hWnd)
		//{
		//	m_dlgAlignObjects.DestroyWindow();
		//	m_dlgAlignObjects.UpdatePrintSheetView();
		//}

		CPrintSheet*	pPrintSheet = GetPrintSheet();
		CPrintComponent component	= GetActComponent();
		if (component.IsNull())
		{
			CImpManDoc*  pDoc = CImpManDoc::GetDoc();
			if (pDoc)
				component = pDoc->m_printComponentsList.GetComponent(0);
			if (pPrintSheet) 
			{
				if (pPrintSheet->GetFirstPrintComponentsGroup().m_printComponents.GetSize() > 0)
					component = pPrintSheet->GetFirstPrintComponentsGroup().m_printComponents[0];
			}
		}
		CPrintComponentsView* pPrintComponentsView = (CPrintComponentsView*)theApp.GetView(RUNTIME_CLASS(CPrintComponentsView));
		if (pPrintComponentsView)
			pPrintComponentsView->SetActComponent(component);

		m_dlgComponentImposer.m_ptInitialPos = rcTarget.TopLeft();
		m_dlgComponentImposer.Create(IDD_COMPONENT_IMPOSER, this);
		if ( ! m_dlgComponentImposer.IsOpenDlgFoldSchemeSelector())
		{
			if (pComponentsView)
			{
				CDisplayItem* pDIComponent = pComponentsView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CPrintComponentsView, PageSection));
				if (pDIComponent)
				{
					CRect rcTool = pDIComponent->m_BoundingRect; rcTool.right = rcTool.left + 70;
					pComponentsView->OnSelectPageSection(pDIComponent, rcTool.CenterPoint(), 9);
				}
			}
		}
		else
			m_dlgComponentImposer.GetDlgFoldSchemeSelector()->InitData();
	}
	else
	{
		m_dlgComponentImposer.DestroyWindow();
		m_dlgComponentImposer.UpdatePrintSheetView();
		m_dlgComponentImposer.SwitchPrintSheetViewSheetGrain(FALSE);
	}

	Invalidate();
	UpdateWindow();

	return TRUE;
}

BOOL CLayoutDataWindow::OnMouseMoveOpenComponentImposer(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.right -=2; rcRect.bottom -= 2;
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CLayoutDataWindow::OnSelectOpenAlignObjects(CDisplayItem* pDI, CPoint point, LPARAM lParam)	
{
	if (pDI)
		pDI->m_nState = CDisplayItem::Normal;

	CRect rcTarget;	GetClientRect(rcTarget); rcTarget.top = pDI->m_BoundingRect.bottom;
	if ( ! m_dlgAlignObjects.m_hWnd)
	{
		//if (m_dlgComponentImposer.m_hWnd)
		//{
		//	m_dlgComponentImposer.DestroyWindow();
		//	m_dlgComponentImposer.UpdatePrintSheetView();
		//	m_dlgComponentImposer.SwitchPrintSheetViewSheetGrain(FALSE);
		//}
		m_dlgAlignObjects.m_ptInitialPos = rcTarget.TopLeft();
		m_dlgAlignObjects.Create(IDD_ALIGN_OBJECTS, this);
	}
	else
	{
		m_dlgAlignObjects.DestroyWindow();
		m_dlgAlignObjects.UpdatePrintSheetView();
	}

	Invalidate();
	UpdateWindow();

	return TRUE;
}

BOOL CLayoutDataWindow::OnMouseMoveOpenAlignObjects(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				CRect rcRect = pDI->m_BoundingRect; rcRect.left++; rcRect.right -=2; rcRect.bottom -= 2;
				CGraphicComponent gc;
				gc.DrawTransparentSmoothFrame(pDC, rcRect, WHITE, GUICOLOR_CAPTION, 90, -1);
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}

BOOL CLayoutDataWindow::OnStatusChanged()
{
	return TRUE;
}
