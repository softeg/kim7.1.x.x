// DlgBookblockProofProgress.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgBookblockProofProgress.h"


// CDlgBookblockProofProgress-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgBookblockProofProgress, CDialog)

CDlgBookblockProofProgress::CDlgBookblockProofProgress(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgBookblockProofProgress::IDD, pParent)
	, m_strProgressText(_T(""))
{

}

CDlgBookblockProofProgress::~CDlgBookblockProofProgress()
{
}

void CDlgBookblockProofProgress::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BOOKBLOCK_PROOF_PROGRESS, m_progressBar);
	DDX_Text(pDX, IDC_BOOKBLOCK_PROOF_PROGRESS_STATIC, m_strProgressText);
	DDX_Control(pDX, IDC_BOOKBLOCK_PROOF_PROGRESS_STATIC, m_progressStatic);
}


BEGIN_MESSAGE_MAP(CDlgBookblockProofProgress, CDialog)
END_MESSAGE_MAP()


// CDlgBookblockProofProgress-Meldungshandler

BOOL CDlgBookblockProofProgress::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_progressBar.SetPos(0);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}
