// DlgModifyMark.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgModifyMark.h"


// CDlgModifyMark-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgModifyMark, CDialog)

CDlgModifyMark::CDlgModifyMark(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgModifyMark::IDD, pParent)
{
	m_strTitle.Empty();
	m_pMark			= NULL;
	m_pPrintSheet	= NULL;
}

CDlgModifyMark::~CDlgModifyMark()
{
}

void CDlgModifyMark::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgModifyMark, CDialog)
	ON_BN_CLICKED(IDOK, &CDlgModifyMark::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgModifyMark-Meldungshandler

BOOL CDlgModifyMark::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(m_strTitle);

	m_dlgLayoutObjectControl.Initialize(IDC_LAYOUTOBJECT_CONTROLFRAME, m_pMark, m_pPrintSheet, &theApp.m_colorDefTable, this);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgModifyMark::OnBnClickedOk()
{
	m_dlgLayoutObjectControl.Apply();
	m_dlgLayoutObjectControl.UpdateView();

	OnOK();
}
