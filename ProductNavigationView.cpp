// ProductNavigationView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h" 
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "ProductNavigationView.h"
#include "OrderDataFrame.h"
#include "GraphicComponent.h"
#include "DlgJobData.h"
#include "PrintSheetPlanningView.h"


// CProductNavigationView

IMPLEMENT_DYNCREATE(CProductNavigationView, CFormView)

CProductNavigationView::CProductNavigationView()
	: CFormView(CProductNavigationView::IDD)
{
	m_DisplayList.Create(this);	
}

CProductNavigationView::~CProductNavigationView()
{
}


#define ID_SELECT_PRODUCTTYPE  9997
#define ID_SELECT_PRODUCTDATA  9998
#define ID_SELECT_PLANNINGVIEW 9999

BEGIN_MESSAGE_MAP(CProductNavigationView, CFormView)
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_COMMAND(ID_SELECT_PRODUCTTYPE, OnSelectProductType)
	ON_COMMAND(ID_SELECT_PRODUCTDATA, OnSelectProductData)
	ON_COMMAND(ID_SELECT_PLANNINGVIEW, OnSelectPlanningView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
END_MESSAGE_MAP()



// CProductNavigationView-Meldungshandler


BOOL CProductNavigationView::OnEraseBkgnd(CDC* pDC)
{
    //CRect rect;
    //GetClientRect(rect);
    //pDC->FillSolidRect(&rect, RGB(209,215,226));

	//DrawBkgndProductDataPane(pDC);
	//DrawBkgndPlanningPane(pDC);

	//return TRUE;

	return CFormView::OnEraseBkgnd(pDC);
}

void CProductNavigationView::DrawBkgndProductDataPane(CDC* pDC)
{
	COrderDataFrame* pFrame = (COrderDataFrame*)GetParentFrame();
	if ( ! pFrame)
		return;

	//CWnd* pProductDataView = pFrame->m_wndSplitter2.GetPane(0, 0);
	//CRect rcPane;
	//pProductDataView->GetWindowRect(rcPane);
	//ScreenToClient(rcPane);
	//CRect rcFrame = rcPane; rcFrame.left -= 1; rcFrame.top -= 46; rcFrame.bottom = rcPane.top; rcFrame.right += 2;
	//CGraphicComponent gp;
	//gp.DrawTitleBar(pDC, rcFrame, _T(""), RGB(230,233,238), RGB(209,215,226), 90, FALSE);
}

void CProductNavigationView::DrawBkgndPlanningPane(CDC* pDC)
{
	COrderDataFrame* pFrame = (COrderDataFrame*)GetParentFrame();
	if ( ! pFrame)
		return;

	CWnd* pPlanningView = pFrame->m_wndSplitter2.GetPane(1, 0);
	CRect rcPane;
	pPlanningView->GetWindowRect(rcPane);
	ScreenToClient(rcPane);
	CRect rcFrame = rcPane; rcFrame.left -= 2; rcFrame.top -= 46; rcFrame.bottom = rcPane.top; rcFrame.right++;
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcFrame, _T(""), RGB(245,250,255), RGB(230,233,238), 90, FALSE);
}

void CProductNavigationView::UpdateBackgroundProductTypeSelector(CDC* pDC, CRect rcRect, CWnd* pWnd)
{
    pDC->FillSolidRect(rcRect, RGB(209,215,226));
}

void CProductNavigationView::UpdateBackgroundProductDataSelector(CDC* pDC, CRect rcRect, CWnd* pWnd)
{
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcRect, _T(""), RGB(230,233,238), RGB(209,215,226), 90, -1);
}

void CProductNavigationView::UpdateBackgroundPrintSheetPlanningSelector(CDC* pDC, CRect rcRect, CWnd* pWnd)
{
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcRect, _T(""), RGB(245,250,255), RGB(235,240,245), 90, -1);
}

void CProductNavigationView::DrawItemProductTypeSelector(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState)
{
	//CDC*	pDC			= CDC::FromHandle(lpDrawItemStruct->hDC);
	//CRect	rcItem		= lpDrawItemStruct->rcItem;
	//CString strText;
	//CImage  img;
	//CRect	rcSrc, rcDest;
	//switch ((int)lpDrawItemStruct->itemData)
	//{
	//case 0:		img.LoadFromResource(theApp.m_hInstance, IDB_PRODUCT_CATEGORIES);	
	//			if ( ! img.IsNull())
	//				rcSrc = CRect(0, 0, img.GetWidth()/2, img.GetHeight());
	//			break;
	//case 1:		img.LoadFromResource(theApp.m_hInstance, IDB_PRODUCT_CATEGORIES);	
	//			if ( ! img.IsNull())
	//				rcSrc = CRect(img.GetWidth()/2, 0, img.GetWidth(), img.GetHeight());
	//			break;
	//default:	break;
	//}

	//if ( ! img.IsNull())
	//{
	//	rcDest.left = rcItem.left + 5; rcDest.top = rcItem.top + 5; rcDest.right = rcDest.left + rcSrc.Width(); rcDest.bottom = rcDest.top + rcSrc.Height();
	//	img.TransparentBlt(lpDrawItemStruct->hDC, rcDest, rcSrc, RGB(255,255,255));
	//}

	//switch ((int)lpDrawItemStruct->itemData)
	//{
	//case 0:		strText.LoadString(IDS_BOUND);		break;
	//case 1:		strText.LoadString(IDS_UNBOUND);	break;
	//default:	break;
	//}

	//CFont font;
	//font.CreateFont(11, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	//pDC->SelectObject(&font);
	//pDC->SetBkMode(TRANSPARENT);
	//pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	//
	//CRect rcText = rcItem; rcText.left += 10 + img.GetWidth()/2; 
	//pDC->DrawText(strText, rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	//font.DeleteObject();
}

void CProductNavigationView::DrawItemProductDataSelector(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState)
{
	CDC*	pDC			= CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect	rcItem		= lpDrawItemStruct->rcItem;
	CString strText;
	//CImage  img;
	//switch ((int)lpDrawItemStruct->itemData)
	//{
	//case 0:		img.LoadFromResource(theApp.m_hInstance, IDB_PRODUCTGROUPTYPE_BOUND);		strText.LoadString(IDS_BOUND);		break;
	//case 1:		img.LoadFromResource(theApp.m_hInstance, IDB_PRODUCTGROUPTYPE_UNBOUND);		strText.LoadString(IDS_UNBOUND);	break;
	//default:	break;
	//}

	//CRect rcImg;
	//if ( ! img.IsNull())
	//{
	//	rcImg.left = rcItem.CenterPoint().x - img.GetWidth()/2; rcImg.top = rcItem.top + 20; rcImg.right = rcImg.left + img.GetWidth(); rcImg.bottom = rcImg.top + img.GetHeight();
	//	img.TransparentBlt(lpDrawItemStruct->hDC, rcImg, RGB(235,235,235));
	//}

	//switch ((int)lpDrawItemStruct->itemData)
	//{
	//case 0:		strText.LoadString(IDS_BOOKBLOCK_PRODUCTGROUPS);	break;
	//case 1:		strText.LoadString(IDS_PAGELISTHEADER_PAGES);		break;
	//case 2:		strText.LoadString(IDS_FOLDSHEET_LABEL);			break;
	//default:	break;
	//}

	//CFont font;
	//font.CreateFont(11, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	//pDC->SelectObject(&font);
	//pDC->SetBkMode(TRANSPARENT);
	//pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	//
	//CRect rcText = rcItem; //rcText.top = rcItem.top + 5; 
	//pDC->DrawText(strText, rcText, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	//font.DeleteObject();
}

void CProductNavigationView::DrawItemPrintSheetPlanningSelector(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState)
{
	CDC*	pDC			= CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect	rcItem		= lpDrawItemStruct->rcItem;
	CString strText;
	//CImage  img;
	//switch ((int)lpDrawItemStruct->itemData)
	//{
	//case 0:		img.LoadFromResource(theApp.m_hInstance, IDB_PRODUCTGROUPTYPE_BOUND);		strText.LoadString(IDS_BOUND);		break;
	//case 1:		img.LoadFromResource(theApp.m_hInstance, IDB_PRODUCTGROUPTYPE_UNBOUND);		strText.LoadString(IDS_UNBOUND);	break;
	//default:	break;
	//}

	//CRect rcImg;
	//if ( ! img.IsNull())
	//{
	//	rcImg.left = rcItem.CenterPoint().x - img.GetWidth()/2; rcImg.top = rcItem.top + 20; rcImg.right = rcImg.left + img.GetWidth(); rcImg.bottom = rcImg.top + img.GetHeight();
	//	img.TransparentBlt(lpDrawItemStruct->hDC, rcImg, RGB(235,235,235));
	//}

	switch ((int)lpDrawItemStruct->itemData)
	{
	case 0:		strText.LoadString(IDS_SHEETPLANNING);		break;
	//case 1:		strText.LoadString(IDS_PRINTFORM_LABEL);break;
	case 1:		strText.LoadString(IDS_PRINTSHEETLIST);		break;
	case 2:		strText.LoadString(IDS_FOLDSHEETLIST);		break;
	case 3:		strText.LoadString(IDS_PAPERLIST_LABEL);	break;
	case 4:		strText.LoadString(IDS_PAGELIST_TITLE);		break;
	default:	break;
	}

	CFont font;
	font.CreateFont(11, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	
	CRect rcText = rcItem; //rcText.top = rcItem.top + 5; 
	pDC->DrawText(strText, rcText, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	font.DeleteObject();
}

void CProductNavigationView::OnInitialUpdate()
{
	CRect rcButtonFrame;
	m_printSheetPlanningSelectorFrame.SubclassDlgItem(IDC_PRINTSHEETPLANNING_SELECTOR, this);
	m_printSheetPlanningSelectorButtons.Create(this, this, DrawItemPrintSheetPlanningSelector, -1, -1, CSize(0,0), NULL);//&UpdateBackgroundPrintSheetPlanningSelector);
	m_printSheetPlanningSelectorFrame.GetWindowRect(rcButtonFrame);

	rcButtonFrame.left = 20;
	//ScreenToClient(rcButtonFrame);
	rcButtonFrame.right = rcButtonFrame.left + 120; rcButtonFrame.DeflateRect(0, 2);
	m_printSheetPlanningSelectorButtons.AddButton(_T(""), _T(""), ID_SELECT_PLANNINGVIEW, rcButtonFrame, -1, (ULONG_PTR)0, FALSE);
	rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
	m_printSheetPlanningSelectorButtons.AddButton(_T(""), _T(""), ID_SELECT_PLANNINGVIEW, rcButtonFrame, -1, (ULONG_PTR)1, FALSE);
	rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
	m_printSheetPlanningSelectorButtons.AddButton(_T(""), _T(""), ID_SELECT_PLANNINGVIEW, rcButtonFrame, -1, (ULONG_PTR)2, FALSE);
	rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
	m_printSheetPlanningSelectorButtons.AddButton(_T(""), _T(""), ID_SELECT_PLANNINGVIEW, rcButtonFrame, -1, (ULONG_PTR)3, FALSE);
	rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
	m_printSheetPlanningSelectorButtons.AddButton(_T(""), _T(""), ID_SELECT_PLANNINGVIEW, rcButtonFrame, -1, (ULONG_PTR)4, FALSE);

	m_printSheetPlanningSelectorButtons.SelectButton(0);

	CFormView::OnInitialUpdate();

	CRect rcClient; GetClientRect(rcClient); rcClient.bottom = max(rcClient.bottom, 37);	// when window is small so scrollbars would appear, GetClientRect() returns 20 pixel hight instead of 37
	SetScaleToFitSize(rcClient.Size());	//suppress scrollbars								
}

void CProductNavigationView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	if ( ! m_printSheetPlanningSelectorFrame.m_hWnd)
		return;

	COrderDataFrame* pFrame = (COrderDataFrame*)GetParentFrame();
	if (pFrame)
	{
		CRect rcPane, rcButtonFrame;
		int	  nOffset = 0;//(bNoButtons) ? 0 : 40;
		//CProductDataView* pView = (CProductDataView*)theApp.GetView(RUNTIME_CLASS(CProductDataView));
		//if ( ! pView)
		//	return;
		//if (m_productTypeSelectorButtons.GetSelectedButtonData() == 0)
		//{
		//	pView->m_nShowProductCategorie = CProductDataView::CategorieBound;		

		//	CWnd* pPane = pFrame->m_wndSplitter2.GetPane(0, 0);
		//	pPane->GetWindowRect(rcPane);
		//	ScreenToClient(rcPane);

		//	m_productDataSelectorFrame.GetWindowRect(rcButtonFrame);
		//	ScreenToClient(rcButtonFrame);

		//	if (nNumBoundProductGroups > 0)
		//	{
		//		rcButtonFrame.OffsetRect(rcPane.left - rcButtonFrame.left + 10, nOffset);
		//		rcButtonFrame.right = rcButtonFrame.left + 140; rcButtonFrame.DeflateRect(0, 2);
		//		m_productDataSelectorButtons[0]->MoveWindow(rcButtonFrame);
		//		rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
		//		rcButtonFrame.right = rcButtonFrame.left + 80; 
		//		m_productDataSelectorButtons[1]->MoveWindow(rcButtonFrame);
		//		rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
		//		m_productDataSelectorButtons[2]->MoveWindow(rcButtonFrame);

		//		m_productDataSelectorButtons.Show();
		//	}
		//	else
		//		m_productDataSelectorButtons.Hide();
		//}
		//else
		//{
		//	pView->m_nShowProductCategorie = CProductDataView::CategorieUnbound;		
		//	m_productDataSelectorButtons.Hide();
		//}

		CWnd* pPane = pFrame->m_wndSplitter2.GetPane(0, 0);
		pPane->GetWindowRect(rcPane);
		ScreenToClient(rcPane);

		m_printSheetPlanningSelectorFrame.GetWindowRect(rcButtonFrame);
		ScreenToClient(rcButtonFrame);

		rcButtonFrame.OffsetRect(rcPane.left - rcButtonFrame.left + 10, nOffset);
		rcButtonFrame.right = rcButtonFrame.left + 120; rcButtonFrame.DeflateRect(0, 2);
		m_printSheetPlanningSelectorButtons[0]->MoveWindow(rcButtonFrame);
		rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
		m_printSheetPlanningSelectorButtons[1]->MoveWindow(rcButtonFrame);
		rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
		m_printSheetPlanningSelectorButtons[2]->MoveWindow(rcButtonFrame);
		rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
		m_printSheetPlanningSelectorButtons[3]->MoveWindow(rcButtonFrame);
		rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
		m_printSheetPlanningSelectorButtons[4]->MoveWindow(rcButtonFrame);

		int nShow = ( ! pDoc->m_boundProducts.GetSize() && ! pDoc->m_flatProducts.GetSize() ) ? SW_HIDE : SW_SHOW;
		m_printSheetPlanningSelectorButtons.ShowButtonByIndex(0, nShow);
		m_printSheetPlanningSelectorButtons.ShowButtonByIndex(1, nShow);
		m_printSheetPlanningSelectorButtons.ShowButtonByIndex(2, nShow);
		m_printSheetPlanningSelectorButtons.ShowButtonByIndex(3, nShow);
		m_printSheetPlanningSelectorButtons.ShowButtonByIndex(4, nShow);
	}
}

void CProductNavigationView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	// TODO: F�gen Sie hier Ihren Meldungsbehandlungscode ein.
}

void CProductNavigationView::OnSelectProductType()
{
	//int nIndex = m_productTypeSelectorButtons.GetSelectedButtonData();
	//if (nIndex < 0)
	//	return;

	//CProductDataView* pView = (CProductDataView*)theApp.GetView(RUNTIME_CLASS(CProductDataView));
	//switch (nIndex)
	//{
	//case 0:		pView->m_nShowProductCategorie = CProductDataView::CategorieBound;		break;
	//case 1:		pView->m_nShowProductCategorie = CProductDataView::CategorieUnbound;	break;
	//default:	break;
	//}

	//OnSelectProductData();

	//OnUpdate(NULL, 0, NULL);
	//Invalidate();
	//UpdateWindow();
}

void CProductNavigationView::OnSelectProductData()
{
	//CProductDataView* pView = (CProductDataView*)theApp.GetView(RUNTIME_CLASS(CProductDataView));
	//if (pView->m_nShowProductCategorie == CProductDataView::CategorieUnbound)
	//{
	//	pView->m_nShowProductData = CProductDataView::ShowBookBlock;
	//}
	//else
	//{
	//	int nIndex = m_productDataSelectorButtons.GetSelectedButtonData();
	//	if (nIndex < 0)
	//		return;

	//	switch (nIndex)
	//	{
	//	case 0:		pView->m_nShowProductData = CProductDataView::ShowBookBlock;	break;
	//	case 1:		pView->m_nShowProductData = CProductDataView::ShowPages;		break;
	//	case 2:		pView->m_nShowProductData = CProductDataView::ShowFoldSheets;	break;
	//	default:	break;
	//	}
	//}

	//theApp.OnUpdateView(RUNTIME_CLASS(CProductDataView), NULL, 0, NULL, TRUE);
	//theApp.UpdateView(RUNTIME_CLASS(CProductDataView), TRUE);
}

void CProductNavigationView::OnSelectPlanningView()
{
	int nIndex = m_printSheetPlanningSelectorButtons.GetSelectedButtonData();
	if (nIndex < 0)
		return;

	CPrintSheetPlanningView* pView = (CPrintSheetPlanningView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
	switch (nIndex)
	{
	case 0:		pView->m_nShowPlanningView = CPrintSheetPlanningView::ShowImposition;			break;
	//case 1:		pView->m_nShowPlanningView = CPrintSheetPlanningView::ShowPrintSheetLayouts;break;
	case 1:		pView->m_nShowPlanningView = CPrintSheetPlanningView::ShowPrintSheetList;		break;
	case 2:		pView->m_nShowPlanningView = CPrintSheetPlanningView::ShowFoldSheetList;		break;
	case 3:		pView->m_nShowPlanningView = CPrintSheetPlanningView::ShowPaperList;			break;
	case 4:		pView->m_nShowPlanningView = CPrintSheetPlanningView::ShowPages;				break;
	default:	break;
	}

	theApp.UpdateView(RUNTIME_CLASS(CPrintSheetPlanningView), TRUE);
}

void CProductNavigationView::OnDraw(CDC* pDC)
{
	//CImpManDoc* pDoc = (CImpManDoc*)GetDocument();
	//if ( ! pDoc)
	//	return;
	//CProductDataView* pProductDataView = (CProductDataView*)theApp.GetView(RUNTIME_CLASS(CProductDataView));
	//if ( ! pProductDataView)
	//	return;
	//COrderDataFrame* pFrame = (COrderDataFrame*)GetParentFrame();
	//if ( ! pFrame)
	//	return;

	//m_DisplayList.BeginRegisterItems(pDC);

	//CWnd* pPane = pFrame->m_wndSplitter2.GetPane(0, 0);
	//CRect rcPane;
	//pPane->GetWindowRect(rcPane);
	//ScreenToClient(rcPane);
	//CRect rcFrame = rcPane; rcFrame.top -= 46; rcFrame.bottom = rcPane.top; rcFrame.right += 2;

	//if (pProductDataView->m_nShowProductCategorie == CProductDataView::CategorieBound)
	//{
	//	CRect rcBox = pDoc->m_Bookblock.DrawTitle(pDC, rcFrame);

	//	CRect rcDI = rcBox; rcDI.right -= 2; rcDI.bottom -= 2;
	//	CDisplayItem* pDI = m_DisplayList.RegisterItem(pDC, rcDI, rcDI, (void*)0, DISPLAY_ITEM_REGISTRY(CProductNavigationView, BoundProductHeader), NULL, CDisplayItem::ForceRegister); // register item into display list
	//	if (pDI)
	//		pDI->SetMouseOverFeedback();
	//}

	//m_DisplayList.EndRegisterItems(pDC);
}

void CProductNavigationView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_DisplayList.OnLButtonDown(point);

	CFormView::OnLButtonDown(nFlags, point);
}

void CProductNavigationView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CFormView::OnMouseMove(nFlags, point);
}

void CProductNavigationView::OnMouseLeave()
{
	m_DisplayList.OnMouseLeave();

	CFormView::OnMouseLeave();
}
