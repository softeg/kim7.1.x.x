#if !defined(AFX_DLGPDFOBJECTCONTENT_H__2FB7E5D1_B6EF_11D5_88C0_0040F68C1EF7__INCLUDED_)
#define AFX_DLGPDFOBJECTCONTENT_H__2FB7E5D1_B6EF_11D5_88C0_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPDFObjectContent.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgPDFObjectContent 

class CDlgPDFObjectContent : public CDialog
{
	DECLARE_DYNCREATE(CDlgPDFObjectContent)

// Konstruktion
public:
	CDlgPDFObjectContent(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgPDFObjectContent();

// Dialogfelddaten
	//{{AFX_DATA(CDlgPDFObjectContent)
	enum { IDD = IDD_PDFOBJECT_CONTENT };
	CSpinButtonCtrl	m_contentScaleSpinX;
	CSpinButtonCtrl	m_contentScaleSpinY;
	CSpinButtonCtrl	m_contentOffsetSpinX;
	CSpinButtonCtrl	m_contentOffsetSpinY;
	CSpinButtonCtrl	m_contentGridSpinX;
	CSpinButtonCtrl	m_contentGridSpinY;
	CString	m_strDocFullpath;
	float	m_fContentOffsetX;
	float	m_fContentOffsetY;
	float	m_fContentScaleX;
	float	m_fContentScaleY;
	BOOL	m_bContentDuplicate;
	BOOL	m_bContentTrim;
	float	m_fContentGridX;
	float	m_fContentGridY;
	BOOL	m_bMap2AllColors;
	BOOL	m_bMap2SpotColors;
	int		m_nMap2SpotColorIndex;
	//}}AFX_DATA


public:
	class CDlgLayoutObjectControl* m_pParent;
	CPDFObjectPrintColorList	   m_printColorList;
	CObjectPivotControl			   m_contentPivotCtrl;
	CSpotColorMappingTable		   m_spotColorMappingTable;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgPDFObjectContent)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	int						AssignObjectSource(int nStartIndex, CPageTemplate* pTemplate, CPageSource* pNewObjectSource, short nObjectSourceIndex);
	void					PDFEngineDataExchange(CPageSource* pNewObjectSource);
	static long				GetOptPreviewResolution(CPageSource& rObjectSource);
	void					AssignColorspace(int nColorSpace);
	void					LoadData();
	void					SaveData();
	void					Apply();

protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgPDFObjectContent)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposContentoffsetspinX(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposContentoffsetspinY(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposContentscalespinX(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposContentscalespinY(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnModifyPrintcolor();
	afx_msg void OnInsertPrintcolor();
	afx_msg void OnAssignAllPrintcolors();
	afx_msg void OnItemchangedObjectcolorControl(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMapSpotcolors();
	afx_msg void OnPaint();
	afx_msg void OnLaunchAcrobat();
	afx_msg void OnRemovePrintcolor();
	afx_msg void OnRemovePdffile();
	//}}AFX_MSG
public:
	afx_msg void OnRegisterPDF();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDeltaposContentgridspinX(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposContentgridspinY(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnChangedPivot();
	afx_msg void OnCbnSelchangeMapSpotcolorIndexCombo();
	afx_msg void OnUpdateMapSpotcolorIndexCombo(CCmdUI* pCmdUI);
	afx_msg void OnContentDuplicate();
	afx_msg void OnContentTrim();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGPDFOBJECTCONTENT_H__2FB7E5D1_B6EF_11D5_88C0_0040F68C1EF7__INCLUDED_
