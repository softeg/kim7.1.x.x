#if !defined(AFX_MEDIAPIVOTCONTROL_H__183501E1_0C8E_11D5_87FF_0040F68C1EF7__INCLUDED_)
#define AFX_MEDIAPIVOTCONTROL_H__183501E1_0C8E_11D5_87FF_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MediaPivotControl.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Fenster CMediaPivotControl 

class CMediaPivotControl : public CStatic
{
// Konstruktion
public:
	CMediaPivotControl();

friend class CDlgPDFTargetProps;

// Attribute
public:
	class CDlgPDFTargetProps*	m_pParent;
	CBitmap						m_bmMediaPivot1;
	CBitmap						m_bmMediaPivot2;
	CBitmap						m_bmMediaPivot3;
	CBitmap						m_bmMediaPivot4;
	CBitmap						m_bmMediaPivot5;
	CBitmap						m_bmMediaPivot6;
	CBitmap						m_bmMediaPivot7;
	CBitmap						m_bmMediaPivot8;
	CBitmap						m_bmMediaPivot9;
	BOOL						m_bLockCheckButton;
	int							m_nCurNX;
	int							m_nCurNY;
	int							m_nActiveCell;

// Operationen
public:
	int	 GetCurrentCell();
	void ChooseBitmap(int nCell);
	void CheckButton(int nCell);
	void Update();

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CMediaPivotControl)
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~CMediaPivotControl();

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CMediaPivotControl)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_MEDIAPIVOTCONTROL_H__183501E1_0C8E_11D5_87FF_0040F68C1EF7__INCLUDED_
