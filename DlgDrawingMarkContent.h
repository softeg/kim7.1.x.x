#pragma once

#define ID_COLORLISTITEM_CHANGED 100

// CDlgDrawingMarkContent-Dialogfeld

class CDlgDrawingMarkContent : public CDialog
{
	DECLARE_DYNAMIC(CDlgDrawingMarkContent)

public:
	CDlgDrawingMarkContent(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgDrawingMarkContent();

// Dialogfelddaten
	enum { IDD = IDD_DRAWINGMARK_CONTENT };
	float m_fDrawingColorIntensity;

public:
	class CDlgLayoutObjectControl*	m_pParent;
	CDlgSystemGenMarkColorControl	m_printColorControl;


// Implementierung
public:
	void	LoadData();
	void	SaveData();
	void	Apply();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnColorListItemChanged();
};
