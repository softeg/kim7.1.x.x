#pragma once

#include "PrecutThread.h"
#include "DlgOptimizationMonitor.h"


extern BOOL g_bReady;


class CKimOptimizer
{
public:
	CKimOptimizer(CDlgOptimizationMonitor* pMonitor);
	~CKimOptimizer(void);

public:
	SHEETOPT_GLOBAL				m_sheetGlobal;
	SHEETOPT_RESULT				m_sheetResult;
	CDlgOptimizationMonitor*	m_pMonitor;


public:
	BOOL			DoOptimizePrintGroup(CPrintGroup& rPrintGroup);
	int				OptimizeVariant(CLayoutVariant& rVariant, CPrintGroup& rPrintGroup, CPrintingProfile& rPrintingProfile, float fSheetWidth, float fSheetHeight, int nPaperGrain);
	BOOL			CheckResult(SHEETOPT_RESULT& rSheetResult, SHEETOPT_GLOBAL& rSheetGlobal, CProductionSetup& rProductionSetup);
	SHEETOPT_SORT	CopyFoldSheetLayerToSort(CFoldSheet& rFoldSheet, float fSheetWidth, float fSheetHeight, CPrintingProfile& rPrintingProfile, int nPaperGrain);
	SHEETOPT_SORT	CopyFlatToSort(CFlatProduct& rFlatProduct, float fSheetWidth, float fSheetHeight, int nPaperGrain);
	BOOL			ObjectFitsToSheet(float fObjectWidth, float fObjectHeight, float fSheetWidth, float fSheetHeight, int nObjectGrain, int nSheetGrain);
	void			UpdatePrintLayout(float fSheetLeft, float fSheetBottom);
	void			InitData();
	static int		preCutReady(bool bReady, int nStatus, int nProgress);
};
