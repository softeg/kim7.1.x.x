#pragma once


typedef struct
{
	CPrintSheet* pPrintSheet;
	CLayout*	 pLayout;
}LPS_PRINTLAYOUT;


// CDlgLayoutPoolSelection-Dialogfeld

class CDlgLayoutPoolSelection : public CDialog
{
	DECLARE_DYNAMIC(CDlgLayoutPoolSelection)

public:
	CDlgLayoutPoolSelection(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgLayoutPoolSelection();

// Dialogfelddaten
	enum { IDD = IDD_LAYOUTPOOL_SELECTION };


public:
	CFont										m_smallFont;
	CImageList									m_layoutImageList;
	int											m_nNup;
	CArray<LPS_PRINTLAYOUT, LPS_PRINTLAYOUT>	m_sampleList1;
	CArray<LPS_PRINTLAYOUT, LPS_PRINTLAYOUT>	m_sampleList2;
	CProductionProfile							m_productionProfile;
	CBoundProductPart							m_productPart;


public:
	void InitNUp();
	void InitDefaultLayoutList();
	void InitJobLayoutList();

protected:
	int		  m_nFoldSchemeIndex;
	CComboBox m_foldSchemeCombo;
	CComboBox m_nUpCombo;
	CListCtrl m_defaultLayoutList;
	CListCtrl m_jobLayoutList;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnCbnSelchangeLayoutselectionSchemeCombo();
	afx_msg void OnCbnSelchangeNUpCombo();
	afx_msg void OnNMCustomdrawDefaultLayoutList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawJobLayoutList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk();
};
