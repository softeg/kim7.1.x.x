// DlgChangeFormats.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgChangeFormats dialog

class CDlgChangeFormats : public CDialog
{
// Construction
public:
	CDlgChangeFormats(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgChangeFormats)
	enum { IDD = IDD_CHANGE_FORMATS };
	CSpinButtonCtrl	m_WidthSpin;
	CSpinButtonCtrl	m_HeightSpin;
	int		m_nPageOrientation;
	float	m_fHeight;
	float	m_fWidth;
	CString	m_strFormatName;
	//}}AFX_DATA
	CPageFormatSelection m_PageFormatSelection;

protected:
	BOOL m_bModified;

	void ModifyObject(CLayoutObject *pObject, CPrintSheet* pPrintSheet);

public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgChangeFormats)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDlgChangeFormats)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDeleteFormat();
	afx_msg void OnAddFormat();
	afx_msg void OnSelchangeFormat();
	afx_msg void OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLandscape();
	afx_msg void OnPortrait();
	afx_msg void OnChangeHeight();
	afx_msg void OnChangeWidth();
	afx_msg void OnChange();
	afx_msg void OnEditchangeFormat();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
