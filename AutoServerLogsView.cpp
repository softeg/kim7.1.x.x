// AutoServerLogsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "AutoServerLogsView.h"
#include "AutoServerJobsView.h"
#include "GraphicComponent.h"
#include "InplaceEdit.h"	// used in PrintSheetView.h
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "JDFOutputImposition.h"
#include "KimSocketsCommon.h"



#define IDC_AUTOSERVERLOGSVIEW_HEADER 12123


CJobMessageLog::CJobMessageLog()
{
	m_nMsgType			= 0;
	m_strLogDateTime	= _T("");
	m_strMessage		= _T("");
}

CJobMessageLog::CJobMessageLog(CJobMessageLog& rJobMessageLog)
{
	m_nMsgType			= rJobMessageLog.m_nMsgType;
	m_strLogDateTime	= rJobMessageLog.m_strLogDateTime;
	m_strMessage		= rJobMessageLog.m_strMessage;
}

const CJobMessageLog& CJobMessageLog::operator=(const CJobMessageLog& rJobMessageLog)
{
	m_nMsgType			= rJobMessageLog.m_nMsgType;
	m_strLogDateTime	= rJobMessageLog.m_strLogDateTime;
	m_strMessage		= rJobMessageLog.m_strMessage;
	return *this;
}


void CJobMessageReport::ReadFile(int nJobID)
{
	kimAutoDB::_jobs job;
	theApp.m_kaDB.ka_GetJob(&job, nJobID);
	kimAutoDB::_workflows workflow;
	theApp.m_kaDB.ka_GetWorkflow(&workflow, job.WorkflowID);

	CString strPath(workflow.hotFolderPath.c_str());
	TCHAR szPath[_MAX_PATH]; strcpy(szPath, strPath.GetBuffer());
	PathStripPath(szPath);
	strPath.Format(_T("\\\\%s\\KimAutoJobs\\%s\\%s"), theApp.settings.m_szAutoServerNameIP, szPath, job.Name.c_str());

	CString		 strInfoFile;
	CStringArray strErrors;
	strInfoFile.Format(_T("%s\\JobMessages.xml"), strPath);

	XMLDoc*	pXMLDoc = new XMLDoc();

	try
	{
		pXMLDoc->Parse((WString)strInfoFile);
	}
	catch (JDFException e)
	{
		return;
	}
	if (pXMLDoc->isNull())
	{
		return;
	}

	KElement root = pXMLDoc->GetRoot();
	CString strNodeName = root.GetDocRootName().getBytes();
	
	if (strNodeName.CompareNoCase(_T("KIM-JobMessages")) != 0)
		return;

	KElement messageLogNode = root.GetFirstChildElement();
	while ( ! messageLogNode.isNull())
	{
		strNodeName = messageLogNode.GetNodeName().getBytes();
		if (strNodeName == _T("MessageLog"))
		{
			CJobMessageLog messageLog;
			CString		   strMsgType = messageLogNode.GetAttribute((WString)_T("Type")).getBytes();

			if (strMsgType.CompareNoCase(_T("Information")) == 0)
				messageLog.m_nMsgType = KIM_MSGTYPE_INFO;
			else
			if (strMsgType.CompareNoCase(_T("Warning")) == 0)
				messageLog.m_nMsgType = KIM_MSGTYPE_WARNING;
			else
			if (strMsgType.CompareNoCase(_T("Error")) == 0)
				messageLog.m_nMsgType = KIM_MSGTYPE_ERROR;

			messageLog.m_strLogDateTime = messageLogNode.GetAttribute((WString)_T("DateTime")).getBytes();

			KElement messageTextNode = messageLogNode.GetFirstChildElement();
			while ( ! messageTextNode.isNull())
			{
				strNodeName = messageTextNode.GetNodeName().getBytes();
				if (strNodeName == _T("Message"))
				{
					messageLog.m_strMessage	= messageTextNode.GetText().getBytes();
					break;
				}
			}
			Add(messageLog);
		}
		messageLogNode = messageLogNode.GetNextSiblingElement();
	}

	delete pXMLDoc;
}



// CAutoServerLogsView

IMPLEMENT_DYNCREATE(CAutoServerLogsView, CScrollView)

CAutoServerLogsView::CAutoServerLogsView()
{
    m_DisplayList.Create(this, FALSE, DISPLAY_LIST_TYPE(CAutoServerLogsView));
	m_DisplayList.SetClassName(_T("CAutoServerLogsView"));

	m_nHeaderItemsTotalWidth = 0;
	m_nHeaderHeight = 0;
	m_headerFont.CreateFont(13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
}

CAutoServerLogsView::~CAutoServerLogsView()
{
	m_headerFont.DeleteObject();
}

void CAutoServerLogsView::DoDataExchange(CDataExchange* pDX)
{
	CScrollView::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAutoServerLogsView, CScrollView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY(HDN_TRACK,	IDC_AUTOSERVERLOGSVIEW_HEADER, OnHeaderTrack)
	ON_NOTIFY(HDN_ENDTRACK, IDC_AUTOSERVERLOGSVIEW_HEADER, OnHeaderEndTrack)
END_MESSAGE_MAP()

// CAutoServerLogsView-Diagnose

#ifdef _DEBUG
void CAutoServerLogsView::AssertValid() const
{
	CScrollView::AssertValid();
}

#ifndef _WIN32_WCE
void CAutoServerLogsView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif
#endif //_DEBUG


BOOL CAutoServerLogsView::OnEraseBkgnd(CDC* pDC)
{
	m_toolBar.ClipButtons(pDC);

	CRect rect;
    GetClientRect(rect);
	rect.top = 0;
    //CBrush brush(RGB(230,233,238));
    //CBrush brush(RGB(240,241,244));//237,239,243));
    CBrush brush(WHITE);
    //pDC->FillRect(&rect, &brush);

    brush.DeleteObject();

	return TRUE;

//	return CScrollView::OnEraseBkgnd(pDC);
}

int CAutoServerLogsView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	//CREATE_INPLACE_EDIT_EX(InplaceEdit);
	//CREATE_INPLACE_COMBOBOX(InplaceComboBox, 0);

	return 0;
}

void CAutoServerLogsView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);

	if (m_HeaderCtrl.GetSafeHwnd())
	{
		CRect clientRect;
		GetClientRect(clientRect);

		HD_LAYOUT hdl;
		WINDOWPOS wp;
		hdl.prc = &clientRect;
		hdl.pwpos = &wp;
		m_HeaderCtrl.Layout(&hdl);	
		m_HeaderCtrl.SetWindowPos(CWnd::FromHandle(wp.hwndInsertAfter), wp.x, wp.y, wp.cx, wp.cy, wp.flags|SWP_SHOWWINDOW);
		m_nHeaderHeight = wp.cy;
	} 
}

// CAutoServerLogsView-Zeichnung

void CAutoServerLogsView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
}

void CAutoServerLogsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	m_headerColumnWidths.RemoveAll();
	m_HeaderCtrl.DestroyWindow();
	m_nHeaderHeight = 0;

	if ( ! m_HeaderCtrl.GetSafeHwnd())
	{
		m_HeaderCtrl.Create(WS_CHILD|WS_VISIBLE|HDS_HORZ, CRect(0,0,0,0), this, IDC_AUTOSERVERLOGSVIEW_HEADER);	
		m_HeaderCtrl.SetFont(&m_headerFont, FALSE);

		HD_ITEM hdi;
		hdi.mask		= HDI_TEXT | HDI_FORMAT | HDI_WIDTH;
		hdi.fmt			= HDF_LEFT | HDF_STRING;

		m_headerColumnWidths.RemoveAll();

		int		nIndex = 0;
		CString string;

		string = "Typ";
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 150);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string.LoadString(ID_INDICATOR_DATE);
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 180);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string = "Job";
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 300);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		string = "Meldung";
		hdi.pszText		= string.GetBuffer(0);
		hdi.cxy			= GetHeaderColumnWidth(nIndex, 1000);
		hdi.cchTextMax	= lstrlen(hdi.pszText) + 1;
		m_headerColumnWidths.Add(hdi.cxy);
		m_HeaderCtrl.InsertItem(nIndex++, &hdi);

		m_nHeaderItemsTotalWidth = 0;
		for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
			m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];
	}

	OnSize(0, 0, 0);

	ModifyStyle(0, WS_CLIPCHILDREN);	// Clip header control 

	Invalidate();

	m_DisplayList.Invalidate();

	m_docSize = CalcExtents();
	SetScrollSizes(MM_TEXT, m_docSize);
}

int	CAutoServerLogsView::GetActJobID()
{
	CAutoServerJobsView* pView = theApp.GetAutoServerJobsView();
	return (pView) ? pView->GetActJobID() : -1;
}

CSize CAutoServerLogsView::CalcExtents()
{
	CClientDC cdc(this);
	CDC dc;
	dc.CreateCompatibleDC(&cdc);

	CRect rcBox = Draw(&dc, NULL);

	dc.DeleteDC();

	return CSize(rcBox.right + 10, rcBox.bottom + 10);
}

void CAutoServerLogsView::ResetDisplayColums()
{
	m_displayColumns.RemoveAll();
}

void CAutoServerLogsView::AddDisplayColumn(int nColID, int nWidth)
{
	struct displayColumn dc = {nColID, nWidth};
	m_displayColumns.Add(dc);
}

void CAutoServerLogsView::OnDraw(CDC* pDC)
{
	//ClipHeader(pDC);
	//Draw(pDC, &m_DisplayList);

	CRect rcClient;
	GetClientRect(rcClient); 
	CRect rcFrame(CPoint(0, 0), CSize(rcClient.Width(), max(rcClient.Height(), m_docSize.cy + m_nHeaderHeight)));

	CDC	memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap bitmap;	bitmap.CreateCompatibleBitmap(pDC, rcFrame.Width(), rcFrame.Height());
	memDC.SelectObject(&bitmap);

	memDC.FillSolidRect(rcFrame, WHITE);//RGB(227,232,238));

	memDC.SaveDC();
	Draw(&memDC, &m_DisplayList);
	memDC.RestoreDC(-1);
	
	pDC->SaveDC();	// save current clipping region
	ClipHeader(pDC);

	pDC->BitBlt(0, m_nHeaderHeight, rcFrame.Width(), rcFrame.Height(), &memDC, 0, m_nHeaderHeight, SRCCOPY);

	bitmap.DeleteObject();
	memDC.DeleteDC();

	pDC->RestoreDC(-1);
}

void CAutoServerLogsView::ClipHeader(CDC* pDC)
{
	CRect rcClientRect, rcClipRect;
	GetClientRect(rcClientRect); rcClipRect = rcClientRect;
	rcClipRect.top = m_nHeaderHeight;
	rcClipRect.OffsetRect(-pDC->GetViewportOrg());
	pDC->IntersectClipRect(rcClipRect);
}

CRect CAutoServerLogsView::Draw(CDC* pDC, CDisplayList* pDisplayList)
{
	CRect rcBox(0,0,0,0);

	return DrawLogsTable(pDC, pDisplayList);	
}

CRect CAutoServerLogsView::DrawLogsTable(CDC* pDC, CDisplayList* pDisplayList)
{
	CRect rcFrame;
	GetClientRect(rcFrame); 
	
	rcFrame.top += m_nHeaderHeight;	
	//rcFrame.right = rcFrame.left + 500;
	rcFrame.top += 5;
	rcFrame.left += 5;

	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	//int nJobID = GetActJobID();
	//if (nJobID < 0)
	//	return rcBox;

	//CJobMessageReport jobMessages;
	//jobMessages.ReadFile(nJobID);

	if (pDisplayList)
		pDisplayList->BeginRegisterItems(pDC);

	int nColIndex = 0;
	ResetDisplayColums();
	AddDisplayColumn(ColLogType,	m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColLogDate,	m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColLogJob,		m_headerColumnWidths[nColIndex++]);
	AddDisplayColumn(ColLogMessage,	m_headerColumnWidths[nColIndex++]);

	CGraphicComponent gc;

	CPen pen(PS_SOLID, 1, LIGHTGRAY);
	CPen* pOldPen = pDC->SelectObject(&pen);

	int nRowHeight = 20;
	CRect rcRow = rcFrame; rcRow.top += 5; rcRow.bottom = rcRow.top + nRowHeight;

	/////// columns
	CRect rcColumn = rcRow; 
	rcColumn.left += 5;

	kimAutoDB::_messageLog messageLogs[1000];
	theApp.m_kaDB.ka_GetMessageLogs(messageLogs, 1000, -1);

	for (int nLogIndex = 0; nLogIndex < 1000; nLogIndex++)
	{
		if (messageLogs[nLogIndex].messageText.empty())
			continue;

		/////// columns
		CRect rcColumn = rcRow;  
		rcColumn.left += 5;

		int	  nRight = 0;
		CRect rcRet, rcThumbnail;
		for (int i = 0; i < m_displayColumns.GetSize(); i++)
		{
			nRight += m_displayColumns[i].nWidth;
			rcColumn.right = nRight;
			switch (m_displayColumns[i].nID)
			{
			case ColLogType:			rcRet = DrawLogType		(pDC, rcColumn, messageLogs[nLogIndex].messageType,			messageLogs[nLogIndex].id,	pDisplayList); break;
			case ColLogDate:			rcRet = DrawLogDate		(pDC, rcColumn, messageLogs[nLogIndex].TimeStamp,										pDisplayList); break;
			case ColLogJob:				rcRet = DrawLogJob		(pDC, rcColumn, messageLogs[nLogIndex].jobName.c_str(),									pDisplayList); break;
			case ColLogMessage:			rcRet = DrawLogMessage	(pDC, rcColumn, messageLogs[nLogIndex].messageText.c_str(),								pDisplayList); break;
			}
			rcBox.UnionRect(rcBox, rcRet);

			rcColumn.left = nRight + 5;
		}
		rcRow.OffsetRect(0, rcRow.Height());
	}

	pDC->SelectObject(pOldPen);
	pen.DeleteObject();

	if (pDisplayList)
		pDisplayList->EndRegisterItems(pDC);

	return rcBox;
}

CRect CAutoServerLogsView::DrawLogType(CDC* pDC, CRect rcFrame, int nMsgType, int nMsgID, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int		nIDBitmap = IDB_INFO;
	CString strMsgType;
	switch (nMsgType)
	{
	case KIM_MSGTYPE_INFO:		nIDBitmap = IDB_INFO;		
								strMsgType.LoadString(IDS_INFO);
								break;
	case KIM_MSGTYPE_WARNING:	nIDBitmap = IDB_WARNING;	
								strMsgType.LoadString(IDS_WARNING);
								break;
	case KIM_MSGTYPE_ERROR:		nIDBitmap = IDB_ERROR;		
								strMsgType.LoadString(IDS_ERROR);
								break;
	}

	CImage img;
	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(nIDBitmap));
	CRect rcImg(CPoint(rcFrame.left - 5, rcFrame.top), CSize(img.GetWidth(), img.GetHeight()));
	img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_CAPTION);

	CRect rcTitle = rcFrame; rcTitle.left += 20; rcTitle.bottom = rcTitle.top + nTextHeight;
	DrawTextMeasured(pDC, strMsgType, rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	if (pDisplayList)
	{
		CRect rcDI = rcBox; rcDI.left -= 4; rcDI.top -= 3; rcDI.right += 2; rcDI.bottom += 1; 
		rcDI.OffsetRect(-GetScrollPos(SB_HORZ), -GetScrollPos(SB_VERT)); 
		CDisplayItem* pDI = pDisplayList->RegisterItem(pDC, rcDI, rcDI, (void*)nMsgID, DISPLAY_ITEM_REGISTRY(CAutoServerLogsView, MessageLog), NULL, CDisplayItem::ForceRegister); // register item into display list
		if (pDI)
		{
			pDI->SetMouseOverFeedback();
			pDI->SetNoStateFeedback();
			if (pDI->m_nState == CDisplayItem::Selected)	// give special feedback here
			{
				CRect rcFeedback = pDI->m_BoundingRect; 
				//rcFeedback.OffsetRect(-pDC->GetViewportOrg().x, -pDC->GetViewportOrg().y);
				rcFeedback.OffsetRect(GetScrollPos(SB_HORZ), GetScrollPos(SB_VERT));
				COLORREF crHighlightColor = RGB(255,195,15);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, rcFeedback, crHighlightColor, TRUE);	
			}
		}
	}

	return rcBox;
}

CRect CAutoServerLogsView::DrawLogDate(CDC* pDC, CRect rcFrame, SYSTEMTIME& timeStamp, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	COleDateTime dateTime(timeStamp);
	CString strTimeStamp = CPrintSheetView::ConvertToString(dateTime, TRUE, TRUE);
	CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + nTextHeight;
	DrawTextMeasured(pDC, strTimeStamp, rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CAutoServerLogsView::DrawLogMessage(CDC* pDC, CRect rcFrame, CString strMessage, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcTitle = rcFrame; rcTitle.bottom = rcTitle.top + nTextHeight;
	DrawTextMeasured(pDC, strMessage, rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

CRect CAutoServerLogsView::DrawLogJob(CDC* pDC, CRect rcFrame, CString strJobname, CDisplayList* pDisplayList)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(1,1));

	CImage img;
	img.LoadFromResource(AfxGetApp()->m_hInstance, MAKEINTRESOURCE(IDB_KIM_JOB));
	CRect rcImg(CPoint(rcFrame.left, rcFrame.top), CSize(img.GetWidth(), img.GetHeight()));
	img.TransparentBlt(pDC->m_hDC, rcImg, WHITE);

	int nTextHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 12) : 15;

	CFont font, boldFont;
	font.CreateFont	   ((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	boldFont.CreateFont((pDC->IsPrinting()) ? Pixel2MM(pDC, 11) : 13, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(BLACK);

	CRect rcTitle = rcFrame; rcTitle.left += 20; rcTitle.bottom = rcTitle.top + nTextHeight;
	//CRect rcTitle = rcFrame; rcTitle.left; rcTitle.bottom = rcTitle.top + nTextHeight;

	DrawTextMeasured(pDC, strJobname, rcTitle, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS, rcBox);

	font.DeleteObject();
	boldFont.DeleteObject();

	return rcBox;
}

// CAutoServerLogsView-Meldungshandler

void CAutoServerLogsView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_DisplayList.OnLButtonDown(point);

	CScrollView::OnLButtonDown(nFlags, point);
}

void CAutoServerLogsView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
    OnLButtonDown(nFlags, point);	// Handle second click 

	CScrollView::OnLButtonDblClk(nFlags, point);
}

void CAutoServerLogsView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	m_DisplayList.OnLButtonDown(point, TRUE); // pass message to display list
	CPoint pt = point;
	ClientToScreen(&pt);
	OnContextMenu(NULL, pt);
}

void CAutoServerLogsView::OnContextMenu(CWnd*, CPoint point)
{
	//CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;

	//int			  nMenuID = -1;
	//CDisplayItem* pDI	  = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerLogsView, BoundProduct));
	//if (pDI)
	//{
	//	nMenuID = IDR_PRODUCTSVIEW_POPUP;
	//}
	////else
	////{
	////	pDI = m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerLogsView, BoundProduct));
	////	if (pDI)
	////		nMenuID = ;
	////}

	//if (nMenuID < 0)
	//	return;

 //   CMenu menu;
	//VERIFY(menu.LoadMenu(nMenuID));

	//CMenu* pPopup = menu.GetSubMenu(0);
	//if ( ! pPopup)
	//	return;

	//pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
}

void CAutoServerLogsView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_DisplayList.OnMouseMove(point);

	CScrollView::OnMouseMove(nFlags, point);
}

void CAutoServerLogsView::OnMouseLeave()
{
	m_DisplayList.OnMouseLeave();

	CScrollView::OnMouseLeave();
}

BOOL CAutoServerLogsView::OnMouseMoveMessageLog(CDisplayItem* pDI, CPoint /*point*/, LPARAM lParam)		
{
	switch (lParam)
	{
	case 1:	// mouse enter
			{
	 			CDC* pDC = GetDC(); 
				COLORREF crHighlightColor = RGB(165,175,200);
				CGraphicComponent gc;
				gc.DrawTransparentFrame(pDC, pDI->m_BoundingRect, crHighlightColor, TRUE);	
				pDI->m_nLastPaintedState = -1;
			}
			break;

	case 2:	// mouse leave
			break;

	default:// mouse move
			break;
	}

	return TRUE;
}


// This functions are overridden in order to:
//		- protect header control from being scrolled vertically 
//		- implement header control's horizontal scrolling

void CAutoServerLogsView::ScrollToPosition(POINT pt)    // logical coordinates
{
	ASSERT(m_nMapMode > 0);     // not allowed for shrink to fit
	if (m_nMapMode != MM_TEXT)
	{
		CWindowDC dc(NULL);
		dc.SetMapMode(m_nMapMode);
		dc.LPtoDP((LPPOINT)&pt);
	}

	// now in device coordinates - limit if out of range
	int xMax = GetScrollLimit(SB_HORZ);
	int yMax = GetScrollLimit(SB_VERT);
	if (pt.x < 0)
		pt.x = 0;
	else if (pt.x > xMax)
		pt.x = xMax;
	if (pt.y < 0)
		pt.y = 0;
	else if (pt.y > yMax)
		pt.y = yMax;

	ScrollToDevicePosition(pt);
}

void CAutoServerLogsView::ScrollToDevicePosition(POINT ptDev)
{
	ASSERT(ptDev.x >= 0);
	ASSERT(ptDev.y >= 0);

	// Note: ScrollToDevicePosition can and is used to scroll out-of-range
	//  areas as far as CScrollView is concerned -- specifically in
	//  the print-preview code.  Since OnScrollBy makes sure the range is
	//  valid, ScrollToDevicePosition does not vector through OnScrollBy.

	int xOrig = GetScrollPos(SB_HORZ);
	SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = GetScrollPos(SB_VERT);
	SetScrollPos(SB_VERT, ptDev.y);
	
	CRect scrollRect, clipRect;
	GetClientRect(scrollRect); clipRect = scrollRect;
	clipRect.top += m_nHeaderHeight;	// Protect header control from being vertically scrolled
	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y, scrollRect, clipRect);

	if (m_HeaderCtrl.GetSafeHwnd())
		if (ptDev.x != 0)				// Scroll header control horizontally
			m_HeaderCtrl.MoveWindow(-ptDev.x, 0, scrollRect.Width() + ptDev.x, m_nHeaderHeight);

	//if (m_InplaceEdit.GetSafeHwnd())
	//	m_InplaceEdit.Deactivate();
}

BOOL CAutoServerLogsView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	int xOrig, x;
	int yOrig, y;

	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		sizeScroll.cy = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		sizeScroll.cx = 0;
	}

	// adjust current x position
	xOrig = x = GetScrollPos(SB_HORZ);
	int xMax = GetScrollLimit(SB_HORZ);
	x += sizeScroll.cx;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	yOrig = y = GetScrollPos(SB_VERT);
	int yMax = GetScrollLimit(SB_VERT);
	y += sizeScroll.cy;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	// did anything change?
	if (x == xOrig && y == yOrig)
		return FALSE;

	if (bDoScroll)
	{
		// do scroll and update scroll positions

		// This part is changed - rest is taken from MFC
		CRect scrollRect, clipRect;
		GetClientRect(scrollRect); clipRect = scrollRect;
		//if (m_HeaderCtrl.GetSafeHwnd())
			clipRect.top += m_nHeaderHeight;	// Protect header control from being vertically scrolled
		ScrollWindow(-(x-xOrig), -(y-yOrig), scrollRect, clipRect);

		if (m_HeaderCtrl.GetSafeHwnd())
			if (sizeScroll.cx != 0)				// Scroll header control horizontally
				m_HeaderCtrl.MoveWindow(-x, 0, scrollRect.Width() + x, m_nHeaderHeight);

		//if (m_InplaceEdit.GetSafeHwnd())
		//	m_InplaceEdit.Deactivate();
		////////////////////////////////////////////////

		if (x != xOrig)
			SetScrollPos(SB_HORZ, x);
		if (y != yOrig)
			SetScrollPos(SB_VERT, y);
	}
	return TRUE;
}

void CAutoServerLogsView::OnHeaderTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	if (hdn->pitem->cxy < 20)
		hdn->pitem->cxy = 20;
}

void CAutoServerLogsView::OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* /*result*/) 
{
	HD_NOTIFY* hdn = (HD_NOTIFY*)pNotifyStruct;

	SetHeaderColumnWidth(hdn->iItem, hdn->pitem->cxy);

	if (m_headerColumnWidths.GetSize() > hdn->iItem)
		m_headerColumnWidths[hdn->iItem] = hdn->pitem->cxy; 

	m_nHeaderItemsTotalWidth = 0;
	for (int i = 0; i < m_headerColumnWidths.GetSize(); i++)
		m_nHeaderItemsTotalWidth += m_headerColumnWidths[i];

	CRect clientRect;
	GetClientRect(&clientRect);

	InvalidateRect(clientRect);
	UpdateWindow(); 

	//CAutoServerToolsView* pView = theApp.GetAutoServerToolsView();
	//if (pView)
	//	pView->SendMessage(WM_SIZE);
}

void CAutoServerLogsView::SetHeaderColumnWidth(int nIndex, int nValue)
{
	if (nIndex >= 20)
		return;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\AutoServerLogsViewHeader");
	if (RegCreateKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			columnWidthArray[nIndex] = nValue;			
		else
		{
			ZeroMemory(columnWidthArray, sizeof(columnWidthArray));
			columnWidthArray[nIndex] = nValue;			
		}

		dwSize = sizeof(columnWidthArray);
		RegSetValueEx(hKey, _T("Columns"), 0, REG_BINARY, (LPBYTE)&columnWidthArray, dwSize);

		RegCloseKey(hKey);
	}
}

int CAutoServerLogsView::GetHeaderColumnWidth(int nIndex, int nDefault)
{
	if (nIndex >= 20)
		return nDefault;

	int nWidth = nDefault;

	HKEY hKey;
	CString strKey = theApp.m_strRegistryKey + _T("\\WindowStates\\AutoServerLogsViewHeader");
	if (RegOpenKey(HKEY_LOCAL_MACHINE, (LPCTSTR)strKey, &hKey) == ERROR_SUCCESS)
	{
		int		columnWidthArray[20];
		DWORD	dwSize = sizeof(columnWidthArray);
		if (RegQueryValueEx(hKey, _T("Columns"), 0, NULL, (LPBYTE)&columnWidthArray, &dwSize) == ERROR_SUCCESS)
			nWidth = columnWidthArray[nIndex];			
		else
			nWidth = nDefault;

		RegCloseKey(hKey);
	}

	if ( (nWidth <= 10) || (nWidth > 1000) )
		nWidth = nDefault;

	return nWidth;
}