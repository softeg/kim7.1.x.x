// Von Klassen-Assistent automatisch erstellte IDispatch-Kapselungsklasse(n).

#include "stdafx.h"
#include "acrobat.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroApp 

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroApp 

BOOL CAcroApp::Exit()
{
	BOOL result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroApp::Hide()
{
	BOOL result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroApp::Show()
{
	BOOL result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroApp::CloseAllDocs()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroApp::MenuItemExecute(LPCTSTR szMenuItemName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szMenuItemName);
	return result;
}

CString CAcroApp::GetActiveTool()
{
	CString result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL CAcroApp::SetActiveTool(LPCTSTR szButtonName, BOOL bPersistent)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szButtonName, bPersistent);
	return result;
}

BOOL CAcroApp::ToolButtonRemove(LPCTSTR szButtonName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szButtonName);
	return result;
}

BOOL CAcroApp::ToolButtonIsEnabled(LPCTSTR szButtonName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szButtonName);
	return result;
}

BOOL CAcroApp::MenuItemRemove(LPCTSTR szMenuItemName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szMenuItemName);
	return result;
}

BOOL CAcroApp::MenuItemIsEnabled(LPCTSTR szMenuItemName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szMenuItemName);
	return result;
}

BOOL CAcroApp::MenuItemIsMarked(LPCTSTR szMenuItemName)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szMenuItemName);
	return result;
}

long CAcroApp::GetNumAVDocs()
{
	long result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH CAcroApp::GetAVDoc(long nIndex)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nIndex);
	return result;
}

LPDISPATCH CAcroApp::GetActiveDoc()
{
	LPDISPATCH result;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

CString CAcroApp::GetLanguage()
{
	CString result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL CAcroApp::Lock(LPCTSTR szLockedBy)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szLockedBy);
	return result;
}

BOOL CAcroApp::Unlock()
{
	BOOL result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroApp::SetPreference(short nType, long nValue)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2 VTS_I4;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nType, nValue);
	return result;
}

long CAcroApp::GetPreference(short nType)
{
	long result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		nType);
	return result;
}

BOOL CAcroApp::Maximize(BOOL bMaxSize)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bMaxSize);
	return result;
}

BOOL CAcroApp::SetFrame(LPDISPATCH iAcroRect)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		iAcroRect);
	return result;
}

LPDISPATCH CAcroApp::GetFrame()
{
	LPDISPATCH result;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL CAcroApp::Minimize(BOOL bMinimize)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bMinimize);
	return result;
}

BOOL CAcroApp::Restore(BOOL bRestore)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bRestore);
	return result;
}

BOOL CAcroApp::UnlockEx(LPCTSTR szLockedBy)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szLockedBy);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroPDDoc 

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroPDDoc 

BOOL CAcroPDDoc::Open(LPCTSTR szFullPath)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szFullPath);
	return result;
}

BOOL CAcroPDDoc::Close()
{
	BOOL result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDDoc::InsertPages(long nInsertPageAfter, LPDISPATCH iPDDocSource, long nStartPage, long nNumPages, BOOL bBookmarks)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nInsertPageAfter, iPDDocSource, nStartPage, nNumPages, bBookmarks);
	return result;
}

BOOL CAcroPDDoc::ReplacePages(long nStartPage, LPDISPATCH iPDDocSource, long nStartSourcePage, long nNumPages, BOOL bMergeTextAnnotations)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH VTS_I4 VTS_I4 VTS_BOOL;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nStartPage, iPDDocSource, nStartSourcePage, nNumPages, bMergeTextAnnotations);
	return result;
}

BOOL CAcroPDDoc::DeletePages(long nStartPage, long nEndPage)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nStartPage, nEndPage);
	return result;
}

long CAcroPDDoc::GetNumPages()
{
	long result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDDoc::Create()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString CAcroPDDoc::GetInfo(LPCTSTR szInfoKey)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		szInfoKey);
	return result;
}

BOOL CAcroPDDoc::SetInfo(LPCTSTR szInfoKey, LPCTSTR szBuffer)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szInfoKey, szBuffer);
	return result;
}

BOOL CAcroPDDoc::DeleteThumbs(long nStartPage, long nEndPage)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nStartPage, nEndPage);
	return result;
}

BOOL CAcroPDDoc::MovePage(long nMoveAfterThisPage, long nPageToMove)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nMoveAfterThisPage, nPageToMove);
	return result;
}

CString CAcroPDDoc::GetFileName()
{
	CString result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

long CAcroPDDoc::GetPageMode()
{
	long result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDDoc::SetPageMode(long nPageMode)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nPageMode);
	return result;
}

BOOL CAcroPDDoc::CreateThumbs(long nFirstPage, long nLastPage)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nFirstPage, nLastPage);
	return result;
}

LPDISPATCH CAcroPDDoc::CreateTextSelect(long nPage, LPDISPATCH iAcroRect)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nPage, iAcroRect);
	return result;
}

LPDISPATCH CAcroPDDoc::AcquirePage(long nPage)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nPage);
	return result;
}

CString CAcroPDDoc::GetInstanceID()
{
	CString result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

CString CAcroPDDoc::GetPermanentID()
{
	CString result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

long CAcroPDDoc::GetFlags()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDDoc::SetFlags(long nFlags)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nFlags);
	return result;
}

LPDISPATCH CAcroPDDoc::OpenAVDoc(LPCTSTR szTempTitle)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		szTempTitle);
	return result;
}

BOOL CAcroPDDoc::Save(short nType, LPCTSTR szFullPath)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2 VTS_BSTR;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nType, szFullPath);
	return result;
}

BOOL CAcroPDDoc::ClearFlags(long nFlags)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nFlags);
	return result;
}

BOOL CAcroPDDoc::SetOpenInfo(long pgNum, short viewMode, LPCTSTR magnification)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I2 VTS_BSTR;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		pgNum, viewMode, magnification);
	return result;
}

BOOL CAcroPDDoc::CropPages(long nStartPage, long nEndPage, short nOddOrEvenPagesOnly, LPDISPATCH iAcroRect)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I2 VTS_DISPATCH;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nStartPage, nEndPage, nOddOrEvenPagesOnly, iAcroRect);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroAVDoc 

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroAVDoc 

BOOL CAcroAVDoc::Open(LPCTSTR szFullPath, LPCTSTR szTempTitle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szFullPath, szTempTitle);
	return result;
}

LPDISPATCH CAcroAVDoc::GetPDDoc()
{
	LPDISPATCH result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH CAcroAVDoc::GetAVPageView()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVDoc::SetViewMode(long nType)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nType);
	return result;
}

BOOL CAcroAVDoc::FindText(LPCTSTR szText, BOOL bCaseSensitive, BOOL bWholeWordsOnly, BOOL bReset)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BOOL VTS_BOOL VTS_BOOL;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szText, bCaseSensitive, bWholeWordsOnly, bReset);
	return result;
}

BOOL CAcroAVDoc::Close(BOOL bNoSave)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bNoSave);
	return result;
}

long CAcroAVDoc::GetViewMode()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVDoc::PrintPages(long nFirstPage, long nLastPage, long nPSLevel, BOOL bBinaryOk, BOOL bShrinkToFit)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nFirstPage, nLastPage, nPSLevel, bBinaryOk, bShrinkToFit);
	return result;
}

BOOL CAcroAVDoc::ClearSelection()
{
	BOOL result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVDoc::BringToFront()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString CAcroAVDoc::GetTitle()
{
	CString result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVDoc::Maximize(BOOL bMaxSize)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bMaxSize);
	return result;
}

BOOL CAcroAVDoc::SetTitle(LPCTSTR szTitle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szTitle);
	return result;
}

BOOL CAcroAVDoc::OpenInWindow(LPCTSTR szFullPath, short hWnd)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I2;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szFullPath, hWnd);
	return result;
}

BOOL CAcroAVDoc::SetTextSelection(LPDISPATCH iAcroPDTextSelect)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		iAcroPDTextSelect);
	return result;
}

BOOL CAcroAVDoc::ShowTextSelect()
{
	BOOL result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVDoc::SetFrame(LPDISPATCH iAcroRect)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		iAcroRect);
	return result;
}

LPDISPATCH CAcroAVDoc::GetFrame()
{
	LPDISPATCH result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVDoc::IsValid()
{
	BOOL result;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVDoc::OpenInWindowEx(LPCTSTR szFullPath, long hWnd, long openFlgs, BOOL UseOpenParams, long pgNum, short pageMode, short ZoomType, long Zoom, short Top, short Left)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_I4 VTS_I4 VTS_BOOL VTS_I4 VTS_I2 VTS_I2 VTS_I4 VTS_I2 VTS_I2;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szFullPath, hWnd, openFlgs, UseOpenParams, pgNum, pageMode, ZoomType, Zoom, Top, Left);
	return result;
}

BOOL CAcroAVDoc::PrintPagesSilent(long nFirstPage, long nLastPage, long nPSLevel, BOOL bBinaryOk, BOOL bShrinkToFit)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nFirstPage, nLastPage, nPSLevel, bBinaryOk, bShrinkToFit);
	return result;
}

BOOL CAcroAVDoc::PrintPagesEx(long nFirstPage, long nLastPage, long nPSLevel, BOOL bBinaryOk, BOOL bShrinkToFit, BOOL bReverse, BOOL bFarEastFontOpt, BOOL bEmitHalftones, long iPageOption)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x16, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nFirstPage, nLastPage, nPSLevel, bBinaryOk, bShrinkToFit, bReverse, bFarEastFontOpt, bEmitHalftones, iPageOption);
	return result;
}

BOOL CAcroAVDoc::PrintPagesSilentEx(long nFirstPage, long nLastPage, long nPSLevel, BOOL bBinaryOk, BOOL bShrinkToFit, BOOL bReverse, BOOL bFarEastFontOpt, BOOL bEmitHalftones, long iPageOption)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_I4 VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4;
	InvokeHelper(0x17, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nFirstPage, nLastPage, nPSLevel, bBinaryOk, bShrinkToFit, bReverse, bFarEastFontOpt, bEmitHalftones, iPageOption);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroAVPageView 

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroAVPageView 

BOOL CAcroAVPageView::GoTo(long nPage)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nPage);
	return result;
}

BOOL CAcroAVPageView::ZoomTo(short nType, short nScale)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2 VTS_I2;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nType, nScale);
	return result;
}

BOOL CAcroAVPageView::ScrollTo(short nX, short nY)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2 VTS_I2;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nX, nY);
	return result;
}

BOOL CAcroAVPageView::ReadPageUp()
{
	BOOL result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVPageView::ReadPageDown()
{
	BOOL result;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVPageView::DoGoBack()
{
	BOOL result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroAVPageView::DoGoForward()
{
	BOOL result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

LPDISPATCH CAcroAVPageView::GetAVDoc()
{
	LPDISPATCH result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH CAcroAVPageView::GetPage()
{
	LPDISPATCH result;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH CAcroAVPageView::GetDoc()
{
	LPDISPATCH result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long CAcroAVPageView::GetZoom()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

short CAcroAVPageView::GetZoomType()
{
	short result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_I2, (void*)&result, NULL);
	return result;
}

long CAcroAVPageView::GetPageNum()
{
	long result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH CAcroAVPageView::PointToDevice(LPDISPATCH iAcroPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		iAcroPoint);
	return result;
}

LPDISPATCH CAcroAVPageView::DevicePointToPage(LPDISPATCH iAcroPoint)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		iAcroPoint);
	return result;
}

LPDISPATCH CAcroAVPageView::GetAperture()
{
	LPDISPATCH result;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroPDPage 

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroPDPage 

LPDISPATCH CAcroPDPage::GetSize()
{
	LPDISPATCH result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

LPDISPATCH CAcroPDPage::GetAnnot(long nIndex)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nIndex);
	return result;
}

LPDISPATCH CAcroPDPage::AddNewAnnot(long nIndexAddAfter, LPCTSTR szSubType, LPDISPATCH iAcroRect)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_I4 VTS_BSTR VTS_DISPATCH;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		nIndexAddAfter, szSubType, iAcroRect);
	return result;
}

BOOL CAcroPDPage::AddAnnot(long nIndexAddAfter, LPDISPATCH i)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_DISPATCH;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nIndexAddAfter, i);
	return result;
}

BOOL CAcroPDPage::RemoveAnnot(long nIndex)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nIndex);
	return result;
}

long CAcroPDPage::GetAnnotIndex(LPDISPATCH i)
{
	long result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
		i);
	return result;
}

long CAcroPDPage::GetNumAnnots()
{
	long result;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH CAcroPDPage::CreatePageHilite(LPDISPATCH iAcroHiliteList)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		iAcroHiliteList);
	return result;
}

LPDISPATCH CAcroPDPage::CreateWordHilite(LPDISPATCH iAcroHiliteList)
{
	LPDISPATCH result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
		iAcroHiliteList);
	return result;
}

LPDISPATCH CAcroPDPage::GetDoc()
{
	LPDISPATCH result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long CAcroPDPage::GetNumber()
{
	long result;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDPage::Draw(short nHWND, short nHDC, short nXOrigin, short nYOrigin, short nZoom)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2 VTS_I2 VTS_I2 VTS_I2 VTS_I2;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nHWND, nHDC, nXOrigin, nYOrigin, nZoom);
	return result;
}

short CAcroPDPage::GetRotate()
{
	short result;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_I2, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDPage::SetRotate(short nRotate)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nRotate);
	return result;
}

BOOL CAcroPDPage::DrawEx(long nHWND, long nHDC, LPDISPATCH updateRect, short nXOrigin, short nYOrigin, short nZoom)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4 VTS_I4 VTS_DISPATCH VTS_I2 VTS_I2 VTS_I2;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nHWND, nHDC, updateRect, nXOrigin, nYOrigin, nZoom);
	return result;
}

BOOL CAcroPDPage::CopyToClipboard(LPDISPATCH boundRect, short nXOrigin, short nYOrigin, short nZoom)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_I2 VTS_I2 VTS_I2;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		boundRect, nXOrigin, nYOrigin, nZoom);
	return result;
}

BOOL CAcroPDPage::CropPage(LPDISPATCH iAcroRect)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		iAcroRect);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroPDAnnot 

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroPDAnnot 

BOOL CAcroPDAnnot::IsValid()
{
	BOOL result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString CAcroPDAnnot::GetSubtype()
{
	CString result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDAnnot::IsEqual(LPDISPATCH PDAnnot)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		PDAnnot);
	return result;
}

long CAcroPDAnnot::GetColor()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDAnnot::SetColor(long nRGBColor)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nRGBColor);
	return result;
}

CString CAcroPDAnnot::GetTitle()
{
	CString result;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDAnnot::SetTitle(LPCTSTR szTitle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szTitle);
	return result;
}

CString CAcroPDAnnot::GetContents()
{
	CString result;
	InvokeHelper(0x8, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDAnnot::SetContents(LPCTSTR szContents)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x9, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szContents);
	return result;
}

BOOL CAcroPDAnnot::IsOpen()
{
	BOOL result;
	InvokeHelper(0xa, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDAnnot::SetOpen(BOOL bIsOpen)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BOOL;
	InvokeHelper(0xb, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		bIsOpen);
	return result;
}

LPDISPATCH CAcroPDAnnot::GetRect()
{
	LPDISPATCH result;
	InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDAnnot::SetRect(LPDISPATCH iAcroRect)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xd, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		iAcroRect);
	return result;
}

LPDISPATCH CAcroPDAnnot::GetDate()
{
	LPDISPATCH result;
	InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDAnnot::SetDate(LPDISPATCH iAcroTime)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0xf, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		iAcroTime);
	return result;
}

BOOL CAcroPDAnnot::Perform(LPDISPATCH iAcroAVDoc)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x10, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		iAcroAVDoc);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroPoint 

short CAcroPoint::GetX()
{
	short result;
	GetProperty(0x1, VT_I2, (void*)&result);
	return result;
}

void CAcroPoint::SetX(short propVal)
{
	SetProperty(0x1, VT_I2, propVal);
}

short CAcroPoint::GetY()
{
	short result;
	GetProperty(0x2, VT_I2, (void*)&result);
	return result;
}

void CAcroPoint::SetY(short propVal)
{
	SetProperty(0x2, VT_I2, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroPoint 


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroRect 

short CAcroRect::GetLeft()
{
	short result;
	GetProperty(0x1, VT_I2, (void*)&result);
	return result;
}

void CAcroRect::SetLeft(short propVal)
{
	SetProperty(0x1, VT_I2, propVal);
}

short CAcroRect::GetTop()
{
	short result;
	GetProperty(0x2, VT_I2, (void*)&result);
	return result;
}

void CAcroRect::SetTop(short propVal)
{
	SetProperty(0x2, VT_I2, propVal);
}

short CAcroRect::GetRight()
{
	short result;
	GetProperty(0x3, VT_I2, (void*)&result);
	return result;
}

void CAcroRect::SetRight(short propVal)
{
	SetProperty(0x3, VT_I2, propVal);
}

short CAcroRect::GetBottom()
{
	short result;
	GetProperty(0x4, VT_I2, (void*)&result);
	return result;
}

void CAcroRect::SetBottom(short propVal)
{
	SetProperty(0x4, VT_I2, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroRect 


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroTime 

short CAcroTime::GetYear()
{
	short result;
	GetProperty(0x1, VT_I2, (void*)&result);
	return result;
}

void CAcroTime::SetYear(short propVal)
{
	SetProperty(0x1, VT_I2, propVal);
}

short CAcroTime::GetMonth()
{
	short result;
	GetProperty(0x2, VT_I2, (void*)&result);
	return result;
}

void CAcroTime::SetMonth(short propVal)
{
	SetProperty(0x2, VT_I2, propVal);
}

short CAcroTime::GetDate()
{
	short result;
	GetProperty(0x3, VT_I2, (void*)&result);
	return result;
}

void CAcroTime::SetDate(short propVal)
{
	SetProperty(0x3, VT_I2, propVal);
}

short CAcroTime::GetHour()
{
	short result;
	GetProperty(0x4, VT_I2, (void*)&result);
	return result;
}

void CAcroTime::SetHour(short propVal)
{
	SetProperty(0x4, VT_I2, propVal);
}

short CAcroTime::GetMinute()
{
	short result;
	GetProperty(0x5, VT_I2, (void*)&result);
	return result;
}

void CAcroTime::SetMinute(short propVal)
{
	SetProperty(0x5, VT_I2, propVal);
}

short CAcroTime::GetSecond()
{
	short result;
	GetProperty(0x6, VT_I2, (void*)&result);
	return result;
}

void CAcroTime::SetSecond(short propVal)
{
	SetProperty(0x6, VT_I2, propVal);
}

short CAcroTime::GetMillisecond()
{
	short result;
	GetProperty(0x7, VT_I2, (void*)&result);
	return result;
}

void CAcroTime::SetMillisecond(short propVal)
{
	SetProperty(0x7, VT_I2, propVal);
}

short CAcroTime::GetDay()
{
	short result;
	GetProperty(0x8, VT_I2, (void*)&result);
	return result;
}

void CAcroTime::SetDay(short propVal)
{
	SetProperty(0x8, VT_I2, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroTime 


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroPDTextSelect 

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroPDTextSelect 

BOOL CAcroPDTextSelect::Destroy()
{
	BOOL result;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

long CAcroPDTextSelect::GetNumText()
{
	long result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

LPDISPATCH CAcroPDTextSelect::GetBoundingRect()
{
	LPDISPATCH result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
	return result;
}

long CAcroPDTextSelect::GetPage()
{
	long result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString CAcroPDTextSelect::GetText(long nTextIndex)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		nTextIndex);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroHiliteList 

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroHiliteList 

BOOL CAcroHiliteList::Add(short nOffset, short nLength)
{
	BOOL result;
	static BYTE parms[] =
		VTS_I2 VTS_I2;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		nOffset, nLength);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// Eigenschaften CAcroPDBookmark 

/////////////////////////////////////////////////////////////////////////////
// Operationen CAcroPDBookmark 

BOOL CAcroPDBookmark::GetByTitle(LPDISPATCH iAcroPDDoc, LPCTSTR szBookmarkTitle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		iAcroPDDoc, szBookmarkTitle);
	return result;
}

BOOL CAcroPDBookmark::Destroy()
{
	BOOL result;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDBookmark::IsValid()
{
	BOOL result;
	InvokeHelper(0x3, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString CAcroPDBookmark::GetTitle()
{
	CString result;
	InvokeHelper(0x4, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL CAcroPDBookmark::SetTitle(LPCTSTR szNewTitle)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR;
	InvokeHelper(0x5, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		szNewTitle);
	return result;
}

BOOL CAcroPDBookmark::Perform(LPDISPATCH iAcroAVDoc)
{
	BOOL result;
	static BYTE parms[] =
		VTS_DISPATCH;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		iAcroAVDoc);
	return result;
}
