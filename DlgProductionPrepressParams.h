#pragma once

#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"


// CDlgProductionPrepressParams-Dialogfeld

class CDlgProductionPrepressParams : public CDialog
{
	DECLARE_DYNAMIC(CDlgProductionPrepressParams)

public:
	CDlgProductionPrepressParams(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgProductionPrepressParams();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTION_PREPRESSPARAMS };

public:
	CPDFTargetList				m_printSheetTargetList;
	CPDFTargetList				m_foldSheetTargetList;
	CPDFTargetList				m_bookletTargetList;
	CDlgPDFOutputMediaSettings*	m_pDlgOutputProfile;
	CString						m_strPrintSheetOutputProfile;
	CString						m_strFoldSheetOutputProfile;
	CString						m_strBookletOutputProfile;
	CString						m_strNoOutput;


public:
	void InitData();
	void SaveData();

public:
	float m_fBleed;
	CSpinButtonCtrl	m_bleedSpin;
	CComboBoxEx	m_printSheetOutputProfileCombo;
	CComboBoxEx	m_foldSheetOutputProfileCombo;
	CComboBoxEx	m_bookletOutputProfileCombo;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
	afx_msg void OnDeltaposBleedSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusBleedEdit();
	afx_msg void OnSelchangePrintSheetOutputProfileCombo();
	afx_msg void OnSelchangeFoldSheetOutputProfileCombo();
	afx_msg void OnSelchangeBookletOutputProfileCombo();
	afx_msg void OnBnClickedPdBrowsePrintSheetOutputProfile();
	afx_msg void OnBnClickedPdBrowseFoldSheetOutputProfile();
	afx_msg void OnBnClickedPdBrowseBookletOutputProfile();
};
