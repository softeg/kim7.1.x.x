// DlgSheet.cpp : implementation file
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "DlgSheet.h"
#include "MainFrm.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "ThemedTabCtrl.h"
#include "DlgFanout.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CDlgSheet::CDlgSheet(BOOL bModifyDirect /*=TRUE*/, CPrintSheet* pActPrintSheet /*= NULL*/, CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSheet::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSheet)
	m_nPageOrientation = -1;
	m_fHeight = 0.0f;
	m_fWidth = 0.0f;
	m_strName = _T("");
	m_nPaperGrain = 0;
	m_bFanoutCheck = FALSE;
	//}}AFX_DATA_INIT
	m_bModified = FALSE;
	m_FanoutList.RemoveAll();
	m_bModifyDirect = bModifyDirect;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		m_nActSight		  = (pActPrintSheet)   ? BOTHSIDES : pDoc->GetActSight();
		m_pActPrintSheet  = (pActPrintSheet)   ? pActPrintSheet : pDoc->GetActPrintSheet();
		m_pActLayout	  = (m_pActPrintSheet) ? m_pActPrintSheet->GetLayout() : pDoc->GetActLayout();
		m_pActPressDevice = (m_pActLayout)	   ? m_pActLayout->m_FrontSide.GetPressDevice() : pDoc->GetActPressDevice();
	}
}


void CDlgSheet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSheet)
	DDX_Control(pDX, IDC_WIDTH_SPIN, m_WidthSpin);
	DDX_Control(pDX, IDC_HEIGHT_SPIN, m_HeightSpin);
	DDX_Radio(pDX, IDC_PORTRAIT, m_nPageOrientation);
	DDX_Measure(pDX, IDC_SHEET_HEIGHT, m_fHeight);
	DDX_Measure(pDX, IDC_SHEET_WIDTH, m_fWidth);
	DDX_CBString(pDX, IDC_SHEET_LIST, m_strName);
	DDX_CBIndex(pDX, IDC_SHEET_GRAIN_COMBO, m_nPaperGrain);
	DDX_Check(pDX, IDC_CHECK_FANOUT, m_bFanoutCheck);
	//}}AFX_DATA_MAP
	//m_PageFormatSelection.DDX_PageFormatCombo(pDX, IDC_SHEET_LIST, m_strName);
}


BEGIN_MESSAGE_MAP(CDlgSheet, CDialog)
	//{{AFX_MSG_MAP(CDlgSheet)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_LBN_SELCHANGE(IDC_SHEET_LIST, OnSelchangeSheetList)
	ON_NOTIFY(UDN_DELTAPOS, IDC_HEIGHT_SPIN, OnDeltaposHeightSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_WIDTH_SPIN, OnDeltaposWidthSpin)
	ON_BN_CLICKED(IDC_PORTRAIT, OnPortrait)
	ON_BN_CLICKED(IDC_LANDSCAPE, OnLandscape)
	ON_EN_CHANGE(IDC_SHEET_HEIGHT, OnChangeSheetHeight)
	ON_EN_CHANGE(IDC_SHEET_WIDTH, OnChangeSheetWidth)
	ON_BN_CLICKED(IDC_CHANGE, OnChange)
	ON_CBN_EDITCHANGE(IDC_SHEET_LIST, OnEditchangeSheetList)
	ON_BN_CLICKED(IDC_DEFINE_FANOUT, OnDefineFanout)
	ON_BN_CLICKED(IDC_CHECK_FANOUT, OnCheckFanout)
	ON_EN_KILLFOCUS(IDC_SHEET_HEIGHT, OnKillfocusSheetHeight)
	ON_EN_KILLFOCUS(IDC_SHEET_WIDTH, OnKillfocusSheetWidth)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgSheet message handlers

BOOL CDlgSheet::OnInitDialog() 
{
	CSheet	 sheetBuffer;
	UINT	 numberFormats;
	POSITION pos;
	int		 nIndex;

	CDialog::OnInitDialog();
	m_WidthSpin.SetRange(0, 1);  
	m_HeightSpin.SetRange(0, 1);  
	m_nPageOrientation = LANDSCAPE;

	CComboBox* pBox = (CComboBox*)GetDlgItem (IDC_SHEET_LIST);
	if (LoadSheets() == TRUE)
	{
		numberFormats = m_sheetList.GetCount(); 
		pos = m_sheetList.GetHeadPosition();
		for (UINT i = 0; i < numberFormats; i ++)                        
		{
			sheetBuffer = m_sheetList.GetAt(pos);
			pBox->AddString(sheetBuffer.m_strSheetName);
			m_sheetList.GetNext(pos);
		}	
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	switch(m_nActSight)
	{
	case ALLSIDES:	{
						POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
						if (pos)
						{
							BOOL	 bDifferentFormats  = FALSE;
							CLayout& rFirstLayout		= pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
							while (pos)
							{
								CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
								if ( (rLayout.m_FrontSide.m_strFormatName != rFirstLayout.m_FrontSide.m_strFormatName) ||
									 (rLayout.m_BackSide.m_strFormatName  != rFirstLayout.m_BackSide.m_strFormatName) )
								{
									bDifferentFormats = TRUE;
									break;
								}
							}
							if ( ! bDifferentFormats)
							{
								nIndex = pBox->FindStringExact(-1, (LPCTSTR)rFirstLayout.m_FrontSide.m_strFormatName);
								if (nIndex != CB_ERR)
									pBox->SetCurSel(nIndex);
								else
									pBox->SetCurSel(-1);
								m_strName		   = rFirstLayout.m_FrontSide.m_strFormatName;
								m_fWidth		   = rFirstLayout.m_FrontSide.m_fPaperWidth;
								m_fHeight		   = rFirstLayout.m_FrontSide.m_fPaperHeight;
								m_nPageOrientation = rFirstLayout.m_FrontSide.m_nPaperOrientation;
								m_nPaperGrain	   = (rFirstLayout.m_FrontSide.m_nPaperGrain == GRAIN_HORIZONTAL) ? 0 : 1;
								m_bFanoutCheck	   = rFirstLayout.m_FrontSide.m_bAdjustFanout;
								m_FanoutList	   = rFirstLayout.m_FrontSide.m_FanoutList;
							}
							else
								pBox->SetCurSel(-1);
						}
					}
					break;

	case BOTHSIDES :
	case FRONTSIDE :{
						 if (m_pActLayout)
						 {
							 nIndex = pBox->FindStringExact(-1, (LPCTSTR)m_pActLayout->m_FrontSide.m_strFormatName);
							 if (nIndex != CB_ERR)
								 pBox->SetCurSel(nIndex);
							 else
								 pBox->SetCurSel(-1);
							 m_strName			= m_pActLayout->m_FrontSide.m_strFormatName;
							 m_fWidth			= m_pActLayout->m_FrontSide.m_fPaperWidth;
							 m_fHeight			= m_pActLayout->m_FrontSide.m_fPaperHeight;
							 m_nPageOrientation = m_pActLayout->m_FrontSide.m_nPaperOrientation;
							 m_nPaperGrain	    = (m_pActLayout->m_FrontSide.m_nPaperGrain == GRAIN_HORIZONTAL) ? 0 : 1;
							 m_bFanoutCheck		= m_pActLayout->m_FrontSide.m_bAdjustFanout;
							 m_FanoutList		= m_pActLayout->m_FrontSide.m_FanoutList;
						 }
					}
					break;

	case BACKSIDE:	{
						if (m_pActLayout)
						{
							nIndex = pBox->FindStringExact(-1, (LPCTSTR)m_pActLayout->m_BackSide.m_strFormatName);
							if (nIndex != CB_ERR)
								pBox->SetCurSel(nIndex);
							else
								pBox->SetCurSel(-1);
							m_strName		   = m_pActLayout->m_BackSide.m_strFormatName;
							m_fWidth		   = m_pActLayout->m_BackSide.m_fPaperWidth;
							m_fHeight		   = m_pActLayout->m_BackSide.m_fPaperHeight;
							m_nPageOrientation = m_pActLayout->m_BackSide.m_nPaperOrientation;
							m_nPaperGrain	   = (m_pActLayout->m_BackSide.m_nPaperGrain == GRAIN_HORIZONTAL) ? 0 : 1;
							m_bFanoutCheck	   = m_pActLayout->m_BackSide.m_bAdjustFanout;
							m_FanoutList	   = m_pActLayout->m_BackSide.m_FanoutList;
						}
					}
					break;

	default :		break; //TODO Errormessage
	}

	GetDlgItem(IDC_CHECK_FANOUT)->EnableWindow(m_FanoutList.GetSize());

	UpdateData(FALSE);      

	return TRUE;  // return TRUE unless you set the focus to a control
}

BOOL CDlgSheet::LoadSheets()
{
	if (theApp.strVersion < "3.0.0.6")
		return LoadOldStyleSheets();
	else
	{
		CFileException fileException;
		CString		   strFullPath;
		
		strFullPath = theApp.GetDataFolder(); // TODO: Verify the path
		strFullPath+= "\\Sheets.def";
		
		CFile file;
		if (!file.Open((LPCTSTR)strFullPath, CFile::modeRead, &fileException))
		{
			TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
			return FALSE; 
		}
		
		CArchive archive(&file, CArchive::load);
		
		m_sheetList.RemoveAll();
		m_sheetList.Serialize(archive);
		
		archive.Close();
		file.Close();
		
		return TRUE;
	}
}

BOOL CDlgSheet::LoadOldStyleSheets()
{
	TCHAR		   pszFileName[MAX_PATH];
	CFile		   SheetDefFile;
	CFileException fileException;
	UINT		   nBytes = sizeof(CSheetOldStyle);
	CSheetOldStyle sheetBufferOldStyle;

	_tcscpy(pszFileName, theApp.GetDataFolder()); // TODO: Verify the path
	_tcscat(pszFileName, _T("\\bogform.def"));
	if (!SheetDefFile.Open(pszFileName, CFile::modeReadWrite|CFile::shareDenyWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n",
				pszFileName, fileException.m_cause);
		return FALSE; 
	}
	else
	{
		m_sheetList.RemoveAll(); // Be sure to work with an empty list
		SheetDefFile.Seek(0, CFile::begin);
		while (nBytes == sizeof(CSheetOldStyle))
		{
			nBytes = SheetDefFile.Read(&sheetBufferOldStyle, sizeof(CSheetOldStyle));
			if (nBytes == sizeof(CSheetOldStyle))
			{
				CSheet sheetBuffer;
				sheetBuffer.m_strSheetName = sheetBufferOldStyle.m_szName;
				sheetBuffer.m_fWidth	   = sheetBufferOldStyle.m_fWidth;
				sheetBuffer.m_fHeight	   = sheetBufferOldStyle.m_fHeight;
				sheetBuffer.m_FanoutList.RemoveAll();
				m_sheetList.AddTail(sheetBuffer);
			}
		}
		SheetDefFile.Close();  //TODO: Should be closed after closing dialog
		if (m_sheetList.IsEmpty()) 
			return FALSE; 
		else
			return TRUE;  	
	}
}

void CDlgSheet::OnSelchangeSheetList() 
{
	UINT nIndex;
	POSITION pos;
	CSheet sheetBuffer;

	CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_SHEET_LIST);
	nIndex = pBox->GetCurSel();
	if(nIndex != CB_ERR) 
	{
		CString strName;
		pBox->GetLBText(nIndex, strName);
		pos				   = m_sheetList.FindByName(strName);
		sheetBuffer		   = m_sheetList.GetAt(pos);
		m_fWidth		   = sheetBuffer.m_fWidth;
		m_fHeight		   = sheetBuffer.m_fHeight;
		m_strName		   = sheetBuffer.m_strSheetName;
		if (m_fWidth > m_fHeight)
			m_nPageOrientation = LANDSCAPE;
		else
			m_nPageOrientation = PORTRAIT;
		m_nPaperGrain	   = (sheetBuffer.m_nPaperGrain == GRAIN_HORIZONTAL) ? 0 : 1;
		m_FanoutList	   = sheetBuffer.m_FanoutList;
		
		m_bModified = TRUE;
		UpdateData(FALSE);

		CButton *pButton = (CButton*)GetDlgItem(IDC_CHANGE);
		pButton->EnableWindow(TRUE);
	}
	else
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		switch(m_nActSight)
		{
			case FRONTSIDE : m_strName = m_pActLayout->m_FrontSide.m_strFormatName;
							 break;
			case BACKSIDE  : m_strName = m_pActLayout->m_BackSide.m_strFormatName;
							 break;
			default :		 break; //TODO Errormessage
		}
	}

	GetDlgItem(IDC_CHECK_FANOUT)->EnableWindow(m_FanoutList.GetSize());
	m_bFanoutCheck = m_FanoutList.GetSize();

	UpdateData(FALSE);
}

void CDlgSheet::OnEditchangeSheetList() 
{
	CButton *pButton = (CButton*)GetDlgItem(IDC_CHANGE);
	pButton->EnableWindow(FALSE);
	m_bModified = TRUE;
}

BOOL CDlgSheet::SaveSheets()
{
	CFileException fileException;
	CString		   strFullPath;

	strFullPath = theApp.GetDataFolder(); // TODO: Verify the path
	strFullPath+= _T("\\Sheets.def");

	CFile file;
	if (!file.Open((LPCTSTR)strFullPath, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", strFullPath, fileException.m_cause);
		return FALSE; 
	}

	CArchive archive(&file, CArchive::store);

	m_sheetList.Serialize(archive);

	archive.Close();
	file.Close();

	return TRUE;
}

void CDlgSheet::OnDel() 
{
	int nIndex;
	POSITION pos;
	if (m_sheetList.IsEmpty()) 
		return;
	if (AfxMessageBox(IDS_DEL_LISTENTRY, MB_YESNO) == IDYES)	
	{
		CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_SHEET_LIST);
		nIndex = pBox->GetCurSel();
		CString strName;
		pBox->GetLBText(nIndex, strName);
		pos	= m_sheetList.FindByName(strName);
		m_sheetList.RemoveAt(pos);
		pBox->DeleteString(nIndex);
		if (m_sheetList.IsEmpty()) 
		{
			m_strName		   = _T("");
			m_fHeight		   = 0.0f;
			m_fWidth		   = 0.0f;
			m_nPageOrientation = LANDSCAPE;
			m_nPaperGrain	   = GRAIN_VERTICAL;
			m_FanoutList.RemoveAll();
			UpdateData(FALSE);      
		}
		else
		{
			if (nIndex >= pBox->GetCount())
				nIndex--;
			pBox->SetCurSel(nIndex);
			OnSelchangeSheetList();	
		}
		if (SaveSheets() == FALSE)
		{
			AfxMessageBox(IDS_DEL_NOTOK, MB_OK);
			LoadSheets();
			UpdateData(FALSE);      
		}
		m_bModified = TRUE;
	}
}

void CDlgSheet::OnNew() 
{
	UpdateData(TRUE);

	m_strName.TrimLeft();
	m_strName.TrimRight();
	if (m_strName.IsEmpty())
	{
		AfxMessageBox(IDS_INVALID_NAME);
		return;
	}
	
	CString strUserDefined; 
	strUserDefined.LoadString(IDS_USERDEFINED);
	int nRet = (m_strName != strUserDefined) ? AfxMessageBox(IDS_NEW_LISTENTRY, MB_YESNO) : IDYES;
	if (nRet == IDYES)	
	{
		CSheet sheetBuffer;
		sheetBuffer.m_strSheetName = m_strName;
		sheetBuffer.m_fWidth	   = m_fWidth;
		sheetBuffer.m_fHeight	   = m_fHeight;
		sheetBuffer.m_nPaperGrain  = (m_nPaperGrain == 0) ? GRAIN_HORIZONTAL : GRAIN_VERTICAL;
		sheetBuffer.m_FanoutList   = m_FanoutList;

		if ( ! m_sheetList.FindByName(m_strName))
		{
			m_sheetList.AddTail(sheetBuffer);
			if (SaveSheets() == FALSE)
				AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

			LoadSheets(); //reload data
			CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_SHEET_LIST);
			pBox->AddString(sheetBuffer.m_strSheetName);
			pBox->SetCurSel(pBox->FindStringExact(-1, sheetBuffer.m_strSheetName));
			OnSelchangeSheetList();	
		}
		else // Name already exists
			if ((m_strName == strUserDefined))
				OnChange();
			else
				AfxMessageBox(IDS_DOUBLEENTRY_NOTOK, MB_OK);

		m_bModified = TRUE;
	}
}

void CDlgSheet::OnChange() 
{
	UpdateData(TRUE);

	POSITION pos = m_sheetList.FindByName(m_strName);
	if ( ! pos)
		return;

	CSheet& sheet = m_sheetList.GetAt(pos);
	int nPaperGrainTransformed = (m_nPaperGrain == 0) ? GRAIN_HORIZONTAL : GRAIN_VERTICAL;
	if ( (sheet.m_fWidth == m_fWidth) && (sheet.m_fHeight == m_fHeight) && (sheet.m_nPaperGrain == nPaperGrainTransformed) )	// nothing changed
		return;

	CString strUserDefined; 
	strUserDefined.LoadString(IDS_USERDEFINED);
	int nRet = (m_strName != strUserDefined) ? AfxMessageBox(IDS_CHANGE_ENTRY, MB_YESNO) : IDYES;
	if (nRet == IDYES)	
	{
		CSheet sheetBuffer;
		sheetBuffer.m_strSheetName = m_strName;
		sheetBuffer.m_fWidth	   = m_fWidth;
		sheetBuffer.m_fHeight	   = m_fHeight;
		sheetBuffer.m_nPaperGrain  = (m_nPaperGrain == 0) ? GRAIN_HORIZONTAL : GRAIN_VERTICAL;
		sheetBuffer.m_FanoutList   = m_FanoutList;

		CComboBox *pBox = (CComboBox*)GetDlgItem (IDC_SHEET_LIST);
		int nIndex = pBox->GetCurSel();
		if (nIndex != CB_ERR)
		{
			CString strName;
			pBox->GetLBText(nIndex, strName);
			POSITION pos = m_sheetList.FindByName(strName);
			m_sheetList.SetAt(pos, sheetBuffer);
			if (SaveSheets() == FALSE)
				AfxMessageBox(IDS_NEW_NOTOK, MB_OK);

			LoadSheets(); //reload data
			pBox->SetCurSel(pBox->FindStringExact(-1, sheetBuffer.m_strSheetName));
			OnSelchangeSheetList();	

			m_bModified = TRUE;
		}
	}
	else
	{
		m_strName.LoadString(IDS_USERDEFINED);
		UpdateData(FALSE);
		OnNew();
	}
}

void CDlgSheet::OnChangeSheetHeight() 
{
	m_bModified = TRUE;
}

void CDlgSheet::OnKillfocusSheetHeight() 
{
	if (m_fWidth > m_fHeight)
	{
		m_nPageOrientation = LANDSCAPE;
		((CButton*)GetDlgItem(IDC_PORTRAIT))->SetCheck(0);
		((CButton*)GetDlgItem(IDC_LANDSCAPE))->SetCheck(1);
	}
	else
	{
		m_nPageOrientation = PORTRAIT;
		((CButton*)GetDlgItem(IDC_PORTRAIT))->SetCheck(1);
		((CButton*)GetDlgItem(IDC_LANDSCAPE))->SetCheck(0);		//	per UpdateData(FALSE) not possible, because when skipping from width to height field, highlighted selection will be removed
	}
}

void CDlgSheet::OnDeltaposHeightSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_HeightSpin.GetBuddy(), pNMUpDown->iDelta);
	UpdateData(TRUE);
	if (m_fWidth > m_fHeight)
		m_nPageOrientation = LANDSCAPE;
	else
		m_nPageOrientation = PORTRAIT;
	*pResult = 0;
	m_bModified = TRUE;
}

void CDlgSheet::OnChangeSheetWidth() 
{
	m_bModified = TRUE;
}

void CDlgSheet::OnKillfocusSheetWidth() 
{
	if (m_fWidth > m_fHeight)
	{
		m_nPageOrientation = LANDSCAPE;
		((CButton*)GetDlgItem(IDC_PORTRAIT))->SetCheck(0);
		((CButton*)GetDlgItem(IDC_LANDSCAPE))->SetCheck(1);
	}
	else
	{
		m_nPageOrientation = PORTRAIT;
		((CButton*)GetDlgItem(IDC_PORTRAIT))->SetCheck(1);
		((CButton*)GetDlgItem(IDC_LANDSCAPE))->SetCheck(0);		//	per UpdateData(FALSE) not possible, because when skipping from width to height field, highlighted selection will be removed
	}
}

void CDlgSheet::OnDeltaposWidthSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_WidthSpin.GetBuddy(), pNMUpDown->iDelta);
	UpdateData(TRUE);
	if (m_fWidth > m_fHeight)
		m_nPageOrientation = LANDSCAPE;
	else
		m_nPageOrientation = PORTRAIT;
	*pResult = 0;
	m_bModified = TRUE;
}

void CDlgSheet::OnPortrait() 
{
	if (m_nPageOrientation == PORTRAIT)	// already portrait
		return;

	m_nPageOrientation = PORTRAIT;		// switch to portrait
	m_bModified = TRUE;

	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_SHEET_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_SHEET_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);
}

void CDlgSheet::OnLandscape() 
{
	if (m_nPageOrientation == LANDSCAPE)// already landscape
		return;
	
	m_nPageOrientation = LANDSCAPE;		// switch to portrait
	m_bModified = TRUE;

	CString strWidth, strHeight;
	CWnd    *pWidth = GetDlgItem(IDC_SHEET_WIDTH);
	CWnd    *pHeight = GetDlgItem(IDC_SHEET_HEIGHT);

	pWidth->GetWindowText(strWidth);	// change width and height
	pHeight->GetWindowText(strHeight);

	pWidth->SetWindowText(strHeight);
	pHeight->SetWindowText(strWidth);
}

void CDlgSheet::OnCheckFanout() 
{
	m_bModified = TRUE;	
}

void CDlgSheet::OnOK() 
{
	CDialog::OnOK();

	UpdateData(TRUE);

	if (m_bModified)	
	{
		if ( ! m_sheetList.FindByName(m_strName))
			OnNew();
		else
			OnChange();

		if (m_bModifyDirect)
		{
			CImpManDoc* pDoc = CImpManDoc::GetDoc();
			if ( ! pDoc)
				return;

			if (m_nActSight == ALLSIDES)
			{
				if (pDoc->m_PrintSheetList.m_Layouts.GetCount() > 1)	// more than one layout type
					if (AfxMessageBox(IDS_MODIFY_ALL_SHEETS_PROMPT, MB_YESNO|MB_ICONQUESTION) == IDNO)
						return;

				POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
				while (pos)
				{
					CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
					UpdateSheetSide(&rLayout.m_FrontSide);
					UpdateSheetSide(&rLayout.m_BackSide);
					rLayout.AnalyzeMarks();
				}
				pDoc->SetModifiedFlag();
				theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),					NULL, 0, NULL);
				theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),			NULL, 0, NULL);
				theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);
			}
			else
			{
				if (m_pActPressDevice)
				{	
					BOOL bStoreData = TRUE;
					if ((m_fWidth > m_pActPressDevice->m_fPlateWidth) || (m_fHeight > m_pActPressDevice->m_fPlateHeight))
						if (AfxMessageBox(IDS_SHEET_GREATER_PLATE , MB_YESNO|MB_ICONSTOP) == IDNO)
							bStoreData = FALSE;

					if (bStoreData)
					{
						if (theApp.settings.m_bFrontRearParallel)
						{
							UpdateSheetSide(&m_pActLayout->m_FrontSide);
							UpdateSheetSide(&m_pActLayout->m_BackSide);
							m_pActLayout->AnalyzeMarks();
						}
						else
						{
							switch(m_nActSight)
							{
							case FRONTSIDE : UpdateSheetSide(&m_pActLayout->m_FrontSide);
											 m_pActLayout->AnalyzeMarks();
											 break;
							case BACKSIDE  : UpdateSheetSide(&m_pActLayout->m_BackSide);
											 m_pActLayout->AnalyzeMarks();
											 break;
							default :		 break; //TODO Errormessage
							}
						}
						pDoc->SetModifiedFlag();
						theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetView),					NULL, 0, NULL);
						theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetTableView),			NULL, 0, NULL);
						theApp.OnUpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData),	NULL, 0, NULL);
						theApp.UpdateView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
					}
				}
			}
		}
	}
}

void CDlgSheet::UpdateSheetSide(CLayoutSide* pLayoutSide)
{
	pLayoutSide->m_fPaperWidth		 = m_fWidth;
	pLayoutSide->m_fPaperHeight		 = m_fHeight;
	pLayoutSide->m_nPaperOrientation = (unsigned char)m_nPageOrientation;
	pLayoutSide->m_nPaperGrain		 = (m_nPaperGrain == 0) ? GRAIN_HORIZONTAL : GRAIN_VERTICAL;
	pLayoutSide->m_strFormatName	 = m_strName;
	pLayoutSide->m_bAdjustFanout	 = m_bFanoutCheck;
	pLayoutSide->m_FanoutList		 = m_FanoutList;
	pLayoutSide->PositionLayoutSide();
}

void CDlgSheet::PostNcDestroy() 
{
	m_sheetList.RemoveAll();  // DlgListCtrl contents must be deleted
	CDialog::PostNcDestroy();
}

void CDlgSheet::OnDefineFanout() 
{
	UpdateData(TRUE);

	CDlgFanout dlg;
	dlg.m_strPaperName = m_strName;
	dlg.m_FanoutList   = m_FanoutList;

	if (dlg.DoModal() == IDCANCEL)
		return;

	m_FanoutList = dlg.m_FanoutList;
	m_bModified  = TRUE;

	GetDlgItem(IDC_CHECK_FANOUT)->EnableWindow(m_FanoutList.GetSize());
	m_bFanoutCheck = m_FanoutList.GetSize();

	UpdateData(FALSE);
}

