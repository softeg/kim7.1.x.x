// DlgDrawingMarkContent.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgJobColorDefinitions.h"
#include "DlgPlaceholderSyntaxInfo.h"
#include "DlgDrawingMarkContent.h"


// CDlgDrawingMarkContent-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgDrawingMarkContent, CDialog)

CDlgDrawingMarkContent::CDlgDrawingMarkContent(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgDrawingMarkContent::IDD, pParent)
	, m_fDrawingColorIntensity(100)
{
	m_pParent = NULL;
}

CDlgDrawingMarkContent::~CDlgDrawingMarkContent()
{
}

void CDlgDrawingMarkContent::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_DRAWINGCOLOR_INTENSITY, m_fDrawingColorIntensity);
	DDV_MinMaxFloat(pDX, m_fDrawingColorIntensity, 0, 100);
}


BEGIN_MESSAGE_MAP(CDlgDrawingMarkContent, CDialog)
	ON_COMMAND(ID_COLORLISTITEM_CHANGED, OnColorListItemChanged)
END_MESSAGE_MAP()


// CDlgDrawingMarkContent-Meldungshandler


BOOL CDlgDrawingMarkContent::OnInitDialog()
{
	CDialog::OnInitDialog();

	if ( (theApp.m_nOSVersion == CImpManApp::WinXP) || (theApp.m_nOSVersion == CImpManApp::WinVista) || (theApp.m_nOSVersion == CImpManApp::Win7) || (theApp.m_nOSVersion == CImpManApp::Win8) )
		EnableThemeDialogTexture(m_hWnd, ETDT_ENABLETAB);

	m_pParent = (CDlgLayoutObjectControl*)GetParent();

	m_printColorControl.Create(this, m_pParent);

	CRect controlRect, frameRect;
	m_printColorControl.GetWindowRect(controlRect);
	GetDlgItem(IDC_COLORCONTROL_FRAME)->GetWindowRect(frameRect);
	ScreenToClient(frameRect);
	m_printColorControl.SetWindowPos(NULL, frameRect.left, frameRect.top, controlRect.Width(), controlRect.Height(), SWP_NOZORDER);

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

BOOL CDlgDrawingMarkContent::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_pParent)
				if (m_pParent->GetParent())
				{
					Invalidate();
					m_pParent->GetParent()->PostMessage(WM_COMMAND, IDC_APPLY_BUTTON);
					return TRUE;
				}
		}
		else
			if (pMsg->wParam == VK_ESCAPE)
			{
				if (m_pParent)
					if (m_pParent->GetParent())
					{
						m_pParent->GetParent()->PostMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
						return TRUE;
					}
			}

	return CDialog::PreTranslateMessage(pMsg);
}	


void CDlgDrawingMarkContent::LoadData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::DrawingMark)
		return;

	CString strContentType, strRectangleStyle;
	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;
				if (pObjectSource)
				{
					strContentType	  =	GetStringToken(pObjectSource->m_strSystemCreationInfo, 0, _T("|"));
					strRectangleStyle =	GetStringToken(pObjectSource->m_strSystemCreationInfo, 1, _T("|"));
				}
				m_printColorControl.LoadData(pTemplate, pObjectSource, nObjectSourceIndex);
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		strContentType	  =	GetStringToken(pMark->m_MarkSource.m_strSystemCreationInfo, 0, _T("|"));
		strRectangleStyle =	GetStringToken(pMark->m_MarkSource.m_strSystemCreationInfo, 1, _T("|"));

		POSITION pos = m_printColorControl.m_printColorList.GetFirstSelectedItemPosition();
		if ( ! pos)
		{
			m_printColorControl.m_printColorList.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);
			pos = m_printColorControl.m_printColorList.GetFirstSelectedItemPosition();
		}
		int nSel = (pos) ? m_printColorControl.m_printColorList.GetNextSelectedItem(pos) : -1;

		CPageSourceHeader* pObjectSourceHeader = (nSel >= 0) ? m_printColorControl.m_printColorList.m_printColorListItems[nSel].m_pObjectSourceHeader : NULL;
		m_fDrawingColorIntensity = ((pObjectSourceHeader) ? pObjectSourceHeader->m_fColorIntensity : 1.0f) * 100.0f;

		m_printColorControl.LoadData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);
	}

	UpdateData(FALSE);
}

void CDlgDrawingMarkContent::SaveData()
{
	if ( ! m_hWnd)
		return;
	if (m_pParent->GetObjectType() != CMark::DrawingMark)
		return;

	UpdateData(TRUE);

	CLayoutObject* pLayoutObject = m_pParent->GetFirstLayoutObject();
	if (pLayoutObject)
	{
		CPageTemplate* pTemplate = m_pParent->m_pObjectTemplate;
		if (pTemplate)
		{
			if (pTemplate->m_ColorSeparations.GetSize())
			{
				CPageSource* pObjectSource		= pTemplate->GetObjectSource(pTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex);
				int			 nObjectSourceIndex = pTemplate->m_ColorSeparations[0].m_nPageSourceIndex;

				CString strContentType = GetStringToken(pObjectSource->m_strSystemCreationInfo, 0, _T("|"));
				if (strContentType == "$RECTANGLE")
				{
					pObjectSource->m_strSystemCreationInfo.Format(_T("$RECTANGLE | %s"), _T("SOLID"));
				}

				pObjectSource->m_nType = CPageSource::SystemCreated;

				m_printColorControl.SaveData(pTemplate, pObjectSource, nObjectSourceIndex);
			}
		}
	}

	CMark* pMark = m_pParent->GetFirstMark();
	if (pMark)
	{
		CString strContentType = GetStringToken(pMark->m_MarkSource.m_strSystemCreationInfo, 0, _T("|"));
		CDrawingMark* pDrawingMark = (CDrawingMark*)pMark;
		pDrawingMark->m_MarkSource.m_nType = CPageSource::SystemCreated;

		if (strContentType == "$RECTANGLE")
		{
			pMark->m_MarkSource.m_strSystemCreationInfo.Format(_T("$RECTANGLE | %s"), _T("SOLID"));
		}

		m_printColorControl.SaveData(&pMark->m_markTemplate, &pMark->m_MarkSource, -1);

		POSITION pos = m_printColorControl.m_printColorList.GetFirstSelectedItemPosition();
		int nSel = (pos) ? m_printColorControl.m_printColorList.GetNextSelectedItem(pos) : -1;
		CPageSourceHeader* pObjectSourceHeader = (nSel >= 0) ? m_printColorControl.m_printColorList.m_printColorListItems[nSel].m_pObjectSourceHeader : NULL;
		if (pObjectSourceHeader) 
			pObjectSourceHeader->m_fColorIntensity = m_fDrawingColorIntensity / 100.0f;
	}
}

void CDlgDrawingMarkContent::Apply()
{
	if (m_hWnd)
		if (IsWindowVisible()) 
			SaveData();
}

void CDlgDrawingMarkContent::OnColorListItemChanged()
{
	POSITION pos = m_printColorControl.m_printColorList.GetFirstSelectedItemPosition();
	int nSel = (pos) ? m_printColorControl.m_printColorList.GetNextSelectedItem(pos) : -1;
	CPageSourceHeader* pObjectSourceHeader = (nSel >= 0) ? m_printColorControl.m_printColorList.m_printColorListItems[nSel].m_pObjectSourceHeader : NULL;
	m_fDrawingColorIntensity = ((pObjectSourceHeader) ? pObjectSourceHeader->m_fColorIntensity : 1.0f) * 100.0f;

	UpdateData(FALSE);
}