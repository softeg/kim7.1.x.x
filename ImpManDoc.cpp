// ImpManDoc.cpp : implementation of the CImpManDoc class
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "MainFrm.h"
#include "PageListFrame.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "DlgMarkSet.h"
#include "PrintSheetFrame.h"
#include "NewPrintSheetFrame.h"
#include "ProductsView.h"
#include "BookBlockView.h"
#include "BookBlockProdDataView.h"
#include "PrintSheetView.h"
#include "PrintSheetTreeView.h"
#include "PrintSheetTableView.h"
#include "PageListView.h"
#include "PageListTableView.h"
#include "PageSourceListView.h"
#ifdef PDF
#include "PDFReader.h"
#include "PDFEngineInterface.h"
#include "DlgPDFLibStatus.h"
#include "spromeps.h"
#endif
#include "PageListDetailView.h"
#include "ThemedTabCtrl.h"
#include "DlgEditTempString.h"
#include "DlgPrintColorProps.h"
#include "io.h"	
#include "DlgTrimboxWarning.h"
#include "GraphicComponent.h"
#include "DlgSheet.h"
#include "PrintSheetColorSettingsView.h"
#include "PrintSheetMaskSettingsView.h"
#include "PrintSheetPlanningView.h"
#include "OrderDataView.h"
#include "PDFEngineXMLParser.h"
#include "CntrItem.h"
#include "PDFImpositionTypes.h"
#include "PrintComponentsView.h"
#include "KimSocketsCommon.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImpManDoc


IMPLEMENT_SERIAL(CImpManDoc, COleDocument, VERSIONABLE_SCHEMA | 9)

BEGIN_MESSAGE_MAP(CImpManDoc, COleDocument)
	//{{AFX_MSG_MAP(CImpManDoc)
	ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_FILE_CLOSE, OnFileClose)
	//}}AFX_MSG_MAP
	// Enable default OLE container implementation
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, COleDocument::OnUpdatePasteMenu)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE_LINK, COleDocument::OnUpdatePasteLinkMenu)
	ON_UPDATE_COMMAND_UI(ID_OLE_EDIT_CONVERT, COleDocument::OnUpdateObjectVerbMenu)
	ON_COMMAND(ID_OLE_EDIT_CONVERT, COleDocument::OnEditConvert)
	ON_UPDATE_COMMAND_UI(ID_OLE_EDIT_LINKS, COleDocument::OnUpdateEditLinksMenu)
	ON_COMMAND(ID_OLE_EDIT_LINKS, COleDocument::OnEditLinks)
	ON_UPDATE_COMMAND_UI(ID_OLE_VERB_FIRST, COleDocument::OnUpdateObjectVerbMenu)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CImpManDoc construction/destruction

CImpManDoc::CImpManDoc()
{
	// Use OLE compound files
	EnableCompoundFile();

	m_pWndMain = NULL;

	m_nDefaultPressDeviceIndex = -1;
	m_nDongleStatus			   = theApp.m_nDongleStatus;
	// TODO: add one-time construction code here
}

CImpManDoc::~CImpManDoc()
{
}

BOOL CImpManDoc::OnNewDocument()
{
	m_pWndMain = theApp.m_pMainWnd;

	if (!COleDocument::OnNewDocument())
		return FALSE;

	return TRUE;
}

BOOL CImpManDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	m_pWndMain = theApp.m_pMainWnd;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();	// If we have a document at this stage, we are opening via MRU list.
	if (pDoc)									// So for that reason we need this construct here at this position.
	{											// For FileNew and FileOpen see implementation of CImpManApp::OnFileNew()/CImpManApp::OnFileOpen()
		if ( ! pDoc->SaveModified())			// DT 12/20/99
			return FALSE;
		pDoc->OnCloseDocument();	
	}								

	if (!COleDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
		if ( (theApp.m_nDongleStatus == CImpManApp::StandardDongle)		|| (theApp.m_nDongleStatus == CImpManApp::AutoDongle) ||
			 (theApp.m_nDongleStatus == CImpManApp::SmallFormatDongle)	|| (theApp.m_nDongleStatus == CImpManApp::NewsDongle) || (theApp.m_nDongleStatus == CImpManApp::DigitalDongle) )
		{
			if (m_nDongleStatus == CImpManApp::NoDongle)
				AfxMessageBox(IDS_DEMO_TEMPLATE_WARNING, MB_OK);
			else
				if (m_nDongleStatus == CImpManApp::ClientDongle)
					AfxMessageBox(IDS_CLIENT_TEMPLATE_WARNING, MB_OK);
		}
		else
			if (theApp.m_nDongleStatus == CImpManApp::ClientDongle)
				if (m_nDongleStatus == CImpManApp::NoDongle)
					AfxMessageBox(IDS_DEMO_TEMPLATE_WARNING, MB_OK);

	UpdateDemoStatusBarPane(this);

	return TRUE;
}

void CImpManDoc::OnCloseDocument() 
{
	COleDocument::OnCloseDocument();

	CMainFrame* pMainFrm = (CMainFrame*)AfxGetMainWnd();
	if (pMainFrm)
		pMainFrm->m_wndStatusBar.m_pDoc = NULL;

	theApp.m_pActiveDoc = NULL;
}

void CImpManDoc::DeleteContents()  
{
	m_boundProducts.RemoveAll();
	m_flatProducts.RemoveAll();
	m_PageSourceList.RemoveAll();				// this function needs to be checked before release
	m_MarkSourceList.RemoveAll();
	m_PageTemplateList.RemoveAll();
	m_MarkTemplateList.RemoveAll();
	m_ColorDefinitionTable.RemoveAll();
	m_Bookblock.m_FoldSheetList.RemoveAll();
	m_Bookblock.m_DescriptionList.RemoveAll();
	m_PrintSheetList.RemoveAll();
	m_productionData.m_paperList.RemoveAll();
}

void CImpManDoc::SetModifiedFlag( BOOL bModified, CRuntimeClass *pClass, BOOL bUpdateAutoMarks, BOOL bAnalyze)
{
	if (this == NULL)
		return;

	CDocument::SetModifiedFlag(bModified);

	CView* pView;
	if (pClass)
	{
		pView = theApp.CImpManApp::GetView(pClass);
		if (pView)
		{
			if (pView->IsKindOf(RUNTIME_CLASS(CProductsView)))
			{
				((CProductsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CBookBlockView)))
			{
				((CBookBlockView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CBookBlockProdDataView)))
			{
				//((CBookBlockProdDataView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPageListView)))
			{
				((CPageListView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPageListDetailView)))
			{
				((CPageListDetailView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPageListTableView)))
			{
				((CPageListTableView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPageSourceListView)))
			{
				((CPageSourceListView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetView)))
			{
				((CPrintSheetView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetTableView)))
			{
				((CPrintSheetTableView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetPlanningView)))
			{
				((CPrintSheetPlanningView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetNavigationView)))
			{
				((CPrintSheetNavigationView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetColorSettingsView)))
			{
				((CPrintSheetColorSettingsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintSheetMaskSettingsView)))
			{
				((CPrintSheetMaskSettingsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerJobsView)))
			{
				((CAutoServerJobsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerWorkflowsView)))
			{
				((CAutoServerWorkflowsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerLogsView)))
			{
				((CAutoServerLogsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CAutoServerDetailsView)))
			{
				((CAutoServerDetailsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
			if (pView->IsKindOf(RUNTIME_CLASS(CPrintComponentsView)))
			{
				((CPrintComponentsView*)pView)->m_DisplayList.Invalidate();
				return;
			}
		}
	}
	else	// if no pClass invalidate all views display lists
	{
		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CProductsView));
		if (pView) ((CProductsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CBookBlockView));
		if (pView) ((CBookBlockView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPageListView));
		if (pView) ((CPageListView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPageListDetailView));
		if (pView) ((CPageListDetailView*)	pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPageListTableView));
		if (pView) ((CPageListTableView*)	pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPageSourceListView));
		if (pView) ((CPageSourceListView*)	pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetView));
		if (pView) ((CPrintSheetView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetTableView));
		if (pView) ((CPrintSheetTableView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetPlanningView));
		if (pView) ((CPrintSheetPlanningView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetNavigationView));
		if (pView) ((CPrintSheetNavigationView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetColorSettingsView));
		if (pView) ((CPrintSheetColorSettingsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetMaskSettingsView));
		if (pView) ((CPrintSheetMaskSettingsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CAutoServerJobsView));
		if (pView) ((CAutoServerJobsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CAutoServerWorkflowsView));
		if (pView) ((CAutoServerWorkflowsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CAutoServerLogsView));
		if (pView) ((CAutoServerLogsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CAutoServerDetailsView));
		if (pView) ((CAutoServerDetailsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintComponentsView));
		if (pView) ((CPrintComponentsView*)		pView)->m_DisplayList.Invalidate();

		pView = theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetViewStrippingData));
		if (pView) 
		{
			CDlgComponentImposer& rDlg = ((CPrintSheetViewStrippingData*)pView)->m_layoutDataWindow.m_dlgComponentImposer;
			if (rDlg.m_hWnd)
				rDlg.EnableAll();
		}
	}

	if (bUpdateAutoMarks)
		m_PrintSheetList.DoAutoMarks();

	m_PrintSheetList.UpdateQuantities();
	m_boundProducts.UpdateQuantities();
	m_flatProducts.UpdateQuantities();
	m_productionData.m_paperList.UpdateEntries();
	
	if (bAnalyze)
		m_printGroupList.Analyze();
}


/////////////////////////////////////////////////////////////////////////////
// CImpManDoc serialization

void CImpManDoc::Serialize(CArchive& ar)
{
	m_GlobalData.Serialize(ar);
	
	CString strVersion = m_GlobalData.GetVersionNumber();
	if (ar.IsLoading())
	{
		if (strVersion < "7.0.0.0")
		{
			SerializeDeprecated(ar);
			return;
		}
	}

	ar.SerializeClass(RUNTIME_CLASS(CImpManDoc));
	UINT nVersion = ar.GetObjectSchema();
 
	theApp.m_strPSFileNotFound.Empty();
	theApp.m_strContentFileNotFound.Empty();
	theApp.m_strPreviewFileNotFound.Empty();

	m_boundProducts.Serialize(ar);
	m_flatProducts.Serialize(ar);
	m_printGroupList.Serialize(ar);

	m_Bookblock.Serialize(ar);

	if (nVersion >= 9)
		m_productionSetup.Serialize(ar);

	CProductionProfiles productionProfiles;
	if (ar.IsLoading())
	{
		if (strVersion <= "7.0.1.5")
		{
			productionProfiles.Serialize(ar);
			ConvertProductionToPrintingProfiles(productionProfiles);
		}
		else
			m_printingProfiles.Serialize(ar);
	}
	else
		m_printingProfiles.Serialize(ar);

	m_PageSourceList.Serialize(ar);
	m_MarkSourceList.Serialize(ar);
	CFile* fp = ar.GetFile();
	if (fp->GetFilePath() != _T("UndoRestore") && ar.IsLoading())
	{
		// Check for missing files in PageSource and give a message to the user
		// Strings will be filled in "m_PageSourceList.Serialize(ar);"
		//CFileStatus status;
		//POSITION pos = m_PageSourceList.GetHeadPosition();
		//while (pos)
		//{
		//	CPageSource& rPageSource = m_PageSourceList.GetNext(pos);
		//	if ( ! CFile::GetStatus(rPageSource.m_strPreviewFilesPath, status))
		//	{
		//		theApp.m_strPreviewFileNotFound += rPageSource.m_strPreviewFilesPath;
		//		theApp.m_strPreviewFileNotFound += _T("\r\n");
		//		rPageSource.m_strPreviewFilesPath.Empty();
		//	}
		//}

		//CImpManDoc::MessageIfFilesAreMissing(CLayoutObject::Page);
		//CImpManDoc::MessageIfFilesAreMissing(CLayoutObject::ControlMark);
	}

	m_PageTemplateList.Serialize(ar);
	m_MarkTemplateList.Serialize(ar);
	m_ColorDefinitionTable.Serialize(ar);

	m_PrintSheetList.Serialize(ar);

	if (ar.IsLoading())
	{
		if (strVersion <= "7.0.1.5")
		{
			theApp.m_pActiveDoc = this; 
			ConvertFlatProductRefs();
			theApp.m_pActiveDoc = NULL;
		}
		
		if (strVersion < "7.0.1.5")
		{
			theApp.m_pActiveDoc = this;
			ConvertBoundProductsPostpressParams(productionProfiles);
			theApp.m_pActiveDoc = NULL;
		}

		ar >> m_nDefaultPressDeviceIndex;
		//if (ar.GetFile() != &theApp.m_undoBuffer)
		//	AnalysePressDeviceIndices();
		m_productionData.Serialize(ar);
		ar >> m_nDongleStatus;
		m_screenLayout.Serialize(ar);
		theApp.m_pActiveDoc = this;
		if (strVersion < "7.0.1.4")
			UpdateOldFlatProductRefs();
		m_PageSourceList.InitializePageTemplateRefs(&m_PageTemplateList, &m_ColorDefinitionTable);
		theApp.m_pActiveDoc = NULL;
	}
	else
	{
		ar << m_nDefaultPressDeviceIndex;
		m_productionData.Serialize(ar);
		ar << m_nDongleStatus;
		m_screenLayout.Serialize(ar);
	}

	if (ar.IsLoading())
	{
		m_PageTemplateList.InvalidateProductRefs();

		if (fp->GetFilePath() != _T("UndoRestore"))
		{
			theApp.m_pActiveDoc = this;
			m_PrintSheetList.m_Layouts.AnalyzeMarks();
			m_PrintSheetList.DoAutoMarks();
			theApp.m_pActiveDoc = NULL;
		}

		theApp.m_pActiveDoc = this;
		m_Bookblock.CheckFoldSheetQuantities();
		m_Bookblock.m_FoldSheetList.UpdatePaperNames();
		m_boundProducts.UpdateQuantities();
		m_PrintSheetList.UpdateQuantities();
		m_productionData.m_paperList.UpdateEntries();
		m_ColorDefinitionTable.UpdateColorInfos();
		m_PageTemplateList.ReorganizePrintSheetPlates();
		m_MarkTemplateList.ReorganizePrintSheetPlates();
		m_PrintSheetList.ReorganizeColorInfos();
		m_printGroupList.Analyze();
		theApp.m_pActiveDoc = NULL;
	}
}

void CImpManDoc::UpdateOldFlatProductRefs()
{
	POSITION layoutPos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (layoutPos)
	{
		CLayout& rLayout = m_PrintSheetList.m_Layouts.GetNext(layoutPos);

		CPrintSheet* pPrintSheet = rLayout.GetFirstPrintSheet();
		while (pPrintSheet)
		{
			CArray <CFlatProductRef, CFlatProductRef&> oldFlatProductRefs;
			oldFlatProductRefs.Append(pPrintSheet->m_flatProductRefs);
			pPrintSheet->m_flatProductRefs.RemoveAll();

			POSITION pos = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
			while (pos)
			{
				CLayoutObject& rObject = rLayout.m_FrontSide.m_ObjectList.GetNext(pos);
				if (rObject.m_nType == CLayoutObject::ControlMark)
					continue;
				if ( ! rObject.IsFlatProduct())
					continue;

				int nFlatProductIndex = ( (rObject.m_nComponentRefIndex >= 0) && (rObject.m_nComponentRefIndex < oldFlatProductRefs.GetSize()) ) ? oldFlatProductRefs[rObject.m_nComponentRefIndex].m_nFlatProductIndex : -1;
				rObject.m_nComponentRefIndex = pPrintSheet->MakeFlatProductRefIndex(nFlatProductIndex);
				CLayoutObject* pCounterPartObject = rObject.FindCounterpartObject();
				if (pCounterPartObject)
					pCounterPartObject->m_nComponentRefIndex = rObject.m_nComponentRefIndex;
			}
			
			pPrintSheet = rLayout.GetNextPrintSheet(pPrintSheet);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CImpManDoc diagnostics

#ifdef _DEBUG
void CImpManDoc::AssertValid() const
{
	COleDocument::AssertValid();
}

void CImpManDoc::Dump(CDumpContext& dc) const
{
	COleDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImpManDoc commands

CImpManDoc* CImpManDoc::GetDoc()
{
	if (theApp.m_pActiveDoc)
		return theApp.m_pActiveDoc;

	CMDIChildWnd* pChild = NULL;
	CDocument*	  pDoc	 = NULL;
	CWinApp*	  pApp	 = AfxGetApp();
	if (pApp->m_pMainWnd)
		pChild = ((CMDIFrameWnd*)(AfxGetApp()->m_pMainWnd))->MDIGetActive();

	pDoc = (pChild) ? pChild->GetActiveDocument() : NULL;
	if( ! pDoc)
		return theApp.m_pActiveDoc;

	if ( ! pDoc->IsKindOf(RUNTIME_CLASS(CImpManDoc)))
		return theApp.m_pActiveDoc;

	return theApp.m_pActiveDoc = (CImpManDoc*)pDoc;
}

CBoundProductPart& CImpManDoc::GetBoundProductPart(int nProductPartIndex)
{
	int		 nIndex = 0;
	POSITION pos    = m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = m_boundProducts.GetNext(pos);
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			if (nIndex == nProductPartIndex)
				return rBoundProduct.m_parts[i];
			nIndex++;
		}
	}

	static private CBoundProductPart nullBoundProductPart;
	return nullBoundProductPart;
}

int	CImpManDoc::GetFlatProductsIndex()
{
	int		 nProductPartIndex = 0;
	POSITION pos			   = m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = m_boundProducts.GetNext(pos);
		nProductPartIndex += rBoundProduct.m_parts.GetSize();
	}
	return nProductPartIndex;
}

CString	CImpManDoc::AssignDefaultProductName()
{
	CString strDefaultName; strDefaultName.LoadString(IDS_PRODUCT); 

	CString strNewName;
	int nNumProductsTotal = m_boundProducts.GetCount() + m_flatProducts.GetCount();
	for (int i = 1; i <= nNumProductsTotal; i++) 
	{
		BOOL bOccupied = FALSE;
		POSITION pos = m_boundProducts.GetHeadPosition();
		while (pos)
		{
			CBoundProduct& rBoundProduct = m_boundProducts.GetNext(pos);
			if (GetNumDefaultProductName(rBoundProduct.m_strProductName) == i)
			{
				bOccupied = TRUE;
				break;
			}
		}
		if ( ! bOccupied)
		{
			pos = m_flatProducts.GetHeadPosition();
			while (pos)
			{
				CFlatProduct& rFlatProduct = m_flatProducts.GetNext(pos);
				if (GetNumDefaultProductName(rFlatProduct.m_strProductName) == i)
				{
					bOccupied = TRUE;
					break;
				}
			}
			if ( ! bOccupied)
			{
				strNewName.Format(_T("%s-%d"), strDefaultName, i);
				return strNewName;
			}
		}
	}

	strNewName.Format(_T("%s-%d"), strDefaultName, nNumProductsTotal + 1);
	return strNewName;
}

int	CImpManDoc::GetNumDefaultProductName(CString strProductName)
{
	CString strDefaultName; strDefaultName.LoadString(IDS_PRODUCT); 
	
	CString  string = CString(strDefaultName) + "-";
	const TCHAR* poi = _tcsstr((LPCTSTR)strProductName, (LPCTSTR)string);
	if (poi)
	{
		poi = _tcschr(poi, '-');
		poi++;
		if (isdigit(*poi))	
			return _ttoi(poi);
	}

	return -1;
}

BOOL CImpManDoc::FoldSheetExisting(CFoldSheet* pFoldSheet)
{
	POSITION pos = m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (&rFoldSheet == pFoldSheet)
			return TRUE;
	}

	return FALSE;
}

void CImpManDoc::ResetPublicFlags()
{
	POSITION pos, pos2;

	pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = m_PrintSheetList.m_Layouts.GetNext(pos);
		pos2 = rLayout.m_FrontSide.m_ObjectList.GetHeadPosition();
		while (pos2)
			rLayout.m_FrontSide.m_ObjectList.GetNext(pos2).m_nPublicFlag = 0;
		pos2 = rLayout.m_BackSide.m_ObjectList.GetHeadPosition();
		while (pos2)
			rLayout.m_BackSide.m_ObjectList.GetNext(pos2).m_nPublicFlag = 0;
	}
}

void CImpManDoc::MessageIfFilesAreMissing(int ObjectType)
{
	if ( ! theApp.m_strPSFileNotFound.IsEmpty() ||  
		 ! theApp.m_strContentFileNotFound.IsEmpty() ||  
		 ! theApp.m_strPreviewFileNotFound.IsEmpty())
	{
		CString strLabel;
		if (ObjectType == CLayoutObject::Page)
			theApp.m_strTempTitle.LoadString(IDS_PAGEFILES_NOT_FOUND);
		else
			theApp.m_strTempTitle.LoadString(IDS_MARKFILES_NOT_FOUND);
		if ( ! theApp.m_strPSFileNotFound.IsEmpty())
		{
			strLabel.LoadString(IDS_PSFILE_NOT_FOUND);
			theApp.m_strTemp += strLabel;//"\r\nPostScript-Quelldateien :\r\n";
			theApp.m_strTemp += theApp.m_strPSFileNotFound;
			theApp.m_strPSFileNotFound.Empty();
			strLabel.Empty();
		}
		if ( ! theApp.m_strContentFileNotFound.IsEmpty())
		{
			strLabel.LoadString(IDS_EPSFILE_NOT_FOUND);
			theApp.m_strTemp += strLabel;//"\r\nEPS-Zieldateien im Verzeichnis :\r\n";
			theApp.m_strTemp += theApp.m_strContentFileNotFound;
			theApp.m_strContentFileNotFound.Empty();
			strLabel.Empty();
		}
		if ( ! theApp.m_strPreviewFileNotFound.IsEmpty())
		{
			strLabel.LoadString(IDS_PREVIEWFILE_NOT_FOUND);
			theApp.m_strTemp += strLabel;//"\r\nPreView-Dateien im Verzeichnis :\r\n";
			theApp.m_strTemp += theApp.m_strPreviewFileNotFound;
			theApp.m_strPreviewFileNotFound.Empty();
			strLabel.Empty();
		}

		CDlgEditTempString stringDlg;
		stringDlg.DoModal();

		theApp.m_strTemp.Empty();
		theApp.m_strTempTitle.Empty();
	}
}

// Implementation of classes belonging to document:

/////////////////////////////////////////////////////////////////////////////
// CGlobalData

IMPLEMENT_SERIAL(CGlobalData, CObject, VERSIONABLE_SCHEMA | 3)

CGlobalData::CGlobalData()
{
	m_strCreationSoftwareName = theApp.strName;//"Imposition Manager PostScript";
	m_strDocumentVersion.Format(_T("%s (Build: %s)"), theApp.strVersion, CString(__DATE__));
	m_nCreationSoftware = 0;
	m_nMeasuringUnit	= 0;
	// CStrings has not to be initialized because they have their own constructor
}


void CGlobalData::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		m_strDocumentVersion.Format(_T("%s (Build: %s)"), theApp.strVersion, CString(__DATE__));
		ar << m_strDocumentVersion;
		ar.SerializeClass(RUNTIME_CLASS(CGlobalData));	
		ar << m_nCreationSoftware;
		ar << m_strCreationSoftwareName;
		ar << m_strJob;
		ar << m_strJobNumber;
		ar << m_strCustomer;
		ar << m_strCustomerNumber;
		ar << m_strCreator;
		ar << m_strPhoneNumber;
		if (m_CreationDate.m_dt == 0.0)
			m_CreationDate = COleDateTime::GetCurrentTime();
		ar << m_CreationDate;
		m_LastModifiedDate = COleDateTime::GetCurrentTime();
		ar << m_LastModifiedDate;
		ar << m_strScheduledToPrint;
		ar << m_strNotes;
		ar << m_nMeasuringUnit;
		ar << m_strJobTicketFile;
	}
	else
	{ 
		ar >> m_strDocumentVersion;

		UINT	nSchema    = 1;
		CString strVersion = GetVersionNumber();
		if (strVersion > "7.1.0.3")
		{
			ar.SerializeClass(RUNTIME_CLASS(CGlobalData));	
			nSchema = ar.GetObjectSchema();
		}
		else
		{
			if (strVersion >= "6.0.0.0")
				nSchema = 3;  
			else
				if (strVersion > "3.6.0.3")
					nSchema = 2;  
		}

		switch (nSchema)
		{
		case 1: ar >> m_nCreationSoftware;
				ar >> m_strCreationSoftwareName;
				ar >> m_strJob; 
				ar >> m_strCustomer;
				ar >> m_strCustomerNumber;
				ar >> m_strCreator;
				ar >> m_CreationDate;
				ar >> m_LastModifiedDate;
				ar >> m_strNotes;
				ar >> m_nMeasuringUnit;
				ar >> m_strJobTicketFile;
				m_strScheduledToPrint = "";
				m_strJobNumber = _T("");
				m_strPhoneNumber = _T("");
				break;
		case 2: ar >> m_nCreationSoftware;
				ar >> m_strCreationSoftwareName;
				ar >> m_strJob;
				ar >> m_strCustomer;
				ar >> m_strCustomerNumber;
				ar >> m_strCreator;
				ar >> m_CreationDate;
				ar >> m_LastModifiedDate;
				ar >> m_strScheduledToPrint;
				ar >> m_strNotes;
				ar >> m_nMeasuringUnit;
				ar >> m_strJobTicketFile;
				m_strJobNumber = _T("");
				m_strPhoneNumber = _T("");
				break;
		case 3: ar >> m_nCreationSoftware;
				ar >> m_strCreationSoftwareName;
				ar >> m_strJob;
				ar >> m_strJobNumber;
				ar >> m_strCustomer;
				ar >> m_strCustomerNumber;
				ar >> m_strCreator;
				ar >> m_strPhoneNumber;
				ar >> m_CreationDate;
				ar >> m_LastModifiedDate;
				ar >> m_strScheduledToPrint;
				ar >> m_strNotes;
				ar >> m_nMeasuringUnit;
				ar >> m_strJobTicketFile;
				break;
		}

		if (strVersion <= "3.0.0.1")
			KIMOpenLogMessage(_T("Diese Datei ist mit einer �lteren Version erstellt\nworden und kann nicht geladen werden!!") , MB_ICONERROR);
		else
			if (strVersion <= "7.1.0.3")
				ar.SerializeClass(RUNTIME_CLASS(CGlobalData));
	}
}

CString CGlobalData::GetVersionNumber()
{
	return m_strDocumentVersion.Left(7);
}



/////////////////////////////////////////////////////////////////////////////
// CProductionData

IMPLEMENT_SERIAL(CProductionData, CObject, VERSIONABLE_SCHEMA | 1)

CProductionData::CProductionData()
{
	m_fHeadTrim		  = 0.0f;
	m_fSideTrim		  = 0.0f;
	m_fFootTrim		  = 0.0f;
	m_fBackTrim		  = 0.0f;
	m_nProductionType = (theApp.settings.m_szLastProductionStyle == CString("Normal")) ? 0 : 1;
}

void CProductionData::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CProductionData));	

	if (ar.IsStoring())
	{
		ar << m_fHeadTrim;
		ar << m_fSideTrim;
		ar << m_fFootTrim;
		ar << m_fBackTrim;
		ar << m_nProductionType;		
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1: ar >> m_fHeadTrim;
				ar >> m_fSideTrim;
				ar >> m_fFootTrim;
				ar >> m_fBackTrim;
				ar >> m_nProductionType;		
				break;
		}
	}

	m_paperList.Serialize(ar);
}


/////////////////////////////////////////////////////////////////////////////
// CPaperList

IMPLEMENT_SERIAL(CPaperList, CObject, VERSIONABLE_SCHEMA | 1)

CPaperList::CPaperList()
{
}

void CPaperList::UpdateEntries()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return;
	if (pDoc->m_PrintSheetList.GetCount() <= 0)
		return;

	for (int i = 0; i < GetSize(); i++)
	{
		GetAt(i).m_nNumSheets	  = 0;
		GetAt(i).m_nQuantityTotal = 0;
	}

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet&  rPrintSheet    = pDoc->m_PrintSheetList.GetNext(pos);

		CLayout*	  pLayout	     = rPrintSheet.GetLayout();
		CPressDevice* pPressDevice   = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
		CString		  strPaperName;	   rPrintSheet.GetPaperName(strPaperName);
		CString		  strPressName   = (pPressDevice) ? pPressDevice->m_strName				 : _T("");
		CString		  strLayoutName  = (pLayout)	  ? pLayout->m_strLayoutName			 : _T("");
		CString		  strFormatName  = (pLayout)	  ? pLayout->m_FrontSide.m_strFormatName : _T("");

		CPaperListEntry* pEntry = FindEntry(strPaperName, strPressName, strLayoutName, strFormatName);
		if (pEntry)
		{
			pEntry->m_nNumSheets++;
			pEntry->m_nQuantityTotal += rPrintSheet.m_nQuantityTotal;
			if (pEntry->m_nNumSheets == 1)
				pEntry->m_pPrintSheet = &rPrintSheet;	// memorize first sheet
		}
		else
		{
			CPaperListEntry entry;
			entry.m_strPaperName 	= strPaperName;
			entry.m_strPressName 	= strPressName;
			entry.m_strLayoutName 	= strLayoutName;
			entry.m_strFormatName 	= strFormatName;
			entry.m_nNumSheets		= 1;
			entry.m_nQuantityTotal	= rPrintSheet.m_nQuantityTotal;
			entry.m_strComment		= _T("");
			entry.m_pPrintSheet		= &rPrintSheet;
			Add(entry);
		}
	}

	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_nNumSheets == 0)
		{
			RemoveAt(i); i--;
		}
	}

	//// sort for print sheet numbers
	BOOL bChanged = FALSE;
	do
	{
		bChanged = FALSE;
		for (int i = 0; i < GetSize(); i++)
		{
			for (int j = i + 1; j < GetSize(); j++)
			{
				if (GetAt(i).m_pPrintSheet->GetNumberOnly() > GetAt(j).m_pPrintSheet->GetNumberOnly())
				{
					CPaperListEntry tmpEntry = GetAt(i); GetAt(i) = GetAt(j); GetAt(j) = tmpEntry;
					bChanged = TRUE;
				}
			}
		}
	}while (bChanged);

	//// build groups
	for (int i = 0; i < GetSize(); i++)
	{
		for (int j = i + 1; j < GetSize(); j++)
		{
			if ( (GetAt(i).m_strPaperName == GetAt(j).m_strPaperName) && (GetAt(i).m_strPressName == GetAt(j).m_strPressName) )
				if (j > i + 1)
				{
					CPaperListEntry tmpEntry = GetAt(i + 1); GetAt(i + 1) = GetAt(j); GetAt(j) = tmpEntry;
					break;
				}
		}
	}

	for (int i = 0; i < GetSize(); i++)
	{
		for (int j = i + 1; j < GetSize(); j++)
		{
			if (GetAt(i).m_strPaperName == GetAt(j).m_strPaperName)
				if (j > i + 1)
				{
					CPaperListEntry tmpEntry = GetAt(i + 1); GetAt(i + 1) = GetAt(j); GetAt(j) = tmpEntry;
					break;
				}
		}
	}
}

CPaperListEntry* CPaperList::FindEntry(const CString& strPaperName, const CString& strPressName, const CString& strLayoutName, const CString& strFormatName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if ( ! strPaperName.IsEmpty())
			if (GetAt(i).m_strPaperName  != strPaperName)
				continue;
		if ( ! strPressName.IsEmpty())
			if (GetAt(i).m_strPressName  != strPressName)
				continue;
		if ( ! strLayoutName.IsEmpty())
			if (GetAt(i).m_strLayoutName != strLayoutName)
				continue;
		if ( ! strFormatName.IsEmpty())
			if (GetAt(i).m_strFormatName != strFormatName)
				continue;
		return &GetAt(i);
	}

	return NULL;
}


/////////////////////////////////////////////////////////////////////////////
// CPaperListEntry

IMPLEMENT_SERIAL(CPaperListEntry, CObject, VERSIONABLE_SCHEMA | 2)

CPaperListEntry::CPaperListEntry()
{
	m_strPaperName.Empty();
	m_strPressName.Empty();
	m_strLayoutName.Empty();
	m_strFormatName.Empty();
	m_nNumSheets		= 0;
	m_nQuantityTotal	= 0;
	m_strComment.Empty();

	m_pPrintSheet = NULL;
}

CPaperListEntry::CPaperListEntry(const CPaperListEntry& rPaperListEntry)
{
	Copy(rPaperListEntry);
}

void CPaperListEntry::Copy(const CPaperListEntry& rPaperListEntry)
{
	m_strPaperName		= rPaperListEntry.m_strPaperName;
	m_strPressName		= rPaperListEntry.m_strPressName;
	m_strLayoutName		= rPaperListEntry.m_strLayoutName;
	m_strFormatName		= rPaperListEntry.m_strFormatName;
	m_nNumSheets		= rPaperListEntry.m_nNumSheets;
	m_nQuantityTotal	= rPaperListEntry.m_nQuantityTotal;
	m_strComment		= rPaperListEntry.m_strComment;

	m_pPrintSheet		= rPaperListEntry.m_pPrintSheet;
}

const CPaperListEntry& CPaperListEntry::operator=(const CPaperListEntry& rPaperListEntry)
{
	Copy(rPaperListEntry);

	return *this;
}

// helper function for CPaperListEntry elements
template <> void AFXAPI SerializeElements <CPaperListEntry> (CArchive& ar, CPaperListEntry* pPaperListEntry, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPaperListEntry));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPaperListEntry->m_strPaperName;
			ar << pPaperListEntry->m_strPressName;
			ar << pPaperListEntry->m_strLayoutName;
			ar << pPaperListEntry->m_strFormatName;
			ar << pPaperListEntry->m_nNumSheets;
			ar << pPaperListEntry->m_nQuantityTotal;
			ar << pPaperListEntry->m_strComment;
		}
		else
		{
			CString strDummy;
			int		nDummy;
			switch (nVersion)
			{
			case 1: ar >> strDummy;	//pPaperListEntry->m_strName;
					ar >> strDummy;	//pPaperListEntry->m_strFormat;
					ar >> nDummy;	//pPaperListEntry->m_nNumSheets;
					ar >> nDummy;	//pPaperListEntry->m_nQuantity;
					ar >> nDummy;	//pPaperListEntry->m_nExtra;
					ar >> nDummy;	//pPaperListEntry->m_nTotal;
					ar >> strDummy;	//pPaperListEntry->m_strComment;
					break;

			case 2:	ar >> pPaperListEntry->m_strPaperName;
					ar >> pPaperListEntry->m_strPressName;
					ar >> pPaperListEntry->m_strLayoutName;
					ar >> pPaperListEntry->m_strFormatName;
					ar >> pPaperListEntry->m_nNumSheets;
					ar >> pPaperListEntry->m_nQuantityTotal;
					ar >> pPaperListEntry->m_strComment;
					break;
			}
		}
		pPaperListEntry++;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CPageSourceList

IMPLEMENT_SERIAL(CPageSourceList, CPageSourceList, VERSIONABLE_SCHEMA | 1)

CPageSourceList::CPageSourceList()
{
}


short CPageSourceList::GetPageSourceIndex(CPageSource* pPageSource)
{
	POSITION pos = GetHeadPosition();
	short	 i	 = 0;
	while (pos)
	{
		if (&GetNext(pos) == pPageSource)
			return i;
		i++;
	}
	return -1;
}

short CPageSourceList::GetPageSourceIndex(CString strDocName)
{
	strDocName.MakeUpper();
	POSITION pos = GetHeadPosition();
	short	 i	 = 0;
	while (pos)
	{
		CString strTest = GetNext(pos).m_strDocumentFullpath;
		strTest.MakeUpper();
		if (strTest == strDocName)
			return i;
		i++;
	}

	return -1;
}


void CPageSourceList::ResetFontsCopyStatus()
{
	POSITION pos = GetHeadPosition();
	while (pos)
		GetNext(pos).m_bFontsCopied = FALSE;
}

short CPageSourceList::FindObjectSourceIndex(CString strPath)
{
	if (strPath.IsEmpty())
		return -1;

	short	 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		if (rPageSource.m_strContentFilesPath == strPath)
			return nIndex;

		nIndex++;
	}

	return -1;
}

short CPageSourceList::FindMarkSourceIndex(CMark* pMark)
{
	if (! pMark)
		return -1;

	short	 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);

		switch (pMark->m_nType)
		{
		case CMark::CustomMark:		if (rPageSource.m_strContentFilesPath == pMark->m_MarkSource.m_strContentFilesPath)
										return nIndex;
									break;

		case CMark::TextMark:		if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMark->m_MarkSource.m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::SectionMark:	if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMark->m_MarkSource.m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::CropMark:		if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMark->m_MarkSource.m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::FoldMark:		if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMark->m_MarkSource.m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::BarcodeMark:	if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMark->m_MarkSource.m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::DrawingMark:	if ( ! pMark->m_MarkSource.m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMark->m_MarkSource.m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMark->m_MarkSource.m_PageSourceHeaders) )
												return nIndex;
									break;

		default:					if (rPageSource.m_strContentFilesPath == pMark->m_MarkSource.m_strContentFilesPath)
										return nIndex;
									break;
		}


		nIndex++;
	}

	return -1;
}


short CPageSourceList::FindMarkSourceIndex(CPageSource* pMarkSource, int nMarkType)
{
	if (! pMarkSource)
		return -1;

	short	 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);

		switch (nMarkType)
		{
		case CMark::CustomMark:		if ( ! pMarkSource->m_strContentFilesPath.GetLength())
										return -2;
									else
										if (rPageSource.m_strContentFilesPath == pMarkSource->m_strContentFilesPath)
											return nIndex;
									break;

		case CMark::TextMark:		if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMarkSource->m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::SectionMark:	if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMarkSource->m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::CropMark:		if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMarkSource->m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::FoldMark:		if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMarkSource->m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::BarcodeMark:	if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMarkSource->m_PageSourceHeaders) )
											return nIndex;
									break;

		case CMark::DrawingMark:	if ( ! pMarkSource->m_strSystemCreationInfo.GetLength())
										return -2;
									else
										if ( (rPageSource.m_strSystemCreationInfo == pMarkSource->m_strSystemCreationInfo) && rPageSource.IsEqualPageSourceHeaders(pMarkSource->m_PageSourceHeaders) )
											return nIndex;
									break;

		default:					if ( ! pMarkSource->m_strContentFilesPath.GetLength())
										return -2;
									else
										if (rPageSource.m_strContentFilesPath == pMarkSource->m_strContentFilesPath)
											return nIndex;
									break;
		}


		nIndex++;
	}

	return -1;
}


CPageSource* CPageSourceList::FindByDocumentFullPath(const CString& strPath)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		if (rPageSource.m_strDocumentFullpath == strPath)
			return &rPageSource;
	}
	return NULL;
}

CPageSource* CPageSourceList::FindBySystemCreationInfo(const CString& strSystemCreationInfo)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		if (rPageSource.m_strSystemCreationInfo == strSystemCreationInfo)
			return &rPageSource;
	}
	return NULL;
}


void CPageSourceList::RemoveUnusedSources()
{
	int		 nPageSourceIndex = 0;
	POSITION pos			  = GetHeadPosition();
	while (pos)
	{
		POSITION prevPos = pos;
		GetNext(pos);	

		BOOL	 bDelete	 = TRUE;
		POSITION templatePos = CImpManDoc::GetDoc()->m_MarkTemplateList.GetHeadPosition();
		while (templatePos)
		{
			CPageTemplate& rMarkTemplate = CImpManDoc::GetDoc()->m_MarkTemplateList.CList::GetNext(templatePos);

			for (int i = 0; i < rMarkTemplate.m_ColorSeparations.GetSize(); i++)
			{
				SEPARATIONINFO& rSepInfo = rMarkTemplate.m_ColorSeparations[i];
				if (rSepInfo.m_nPageSourceIndex == nPageSourceIndex) 
				{
					bDelete = FALSE;
					break;
				}
			}
			if (!bDelete)
				break;
		}

		if (bDelete)
		{
			RemoveAt(prevPos);
			templatePos = CImpManDoc::GetDoc()->m_MarkTemplateList.GetHeadPosition();
			while (templatePos)
			{
				CPageTemplate& rMarkTemplate = CImpManDoc::GetDoc()->m_MarkTemplateList.CList::GetNext(templatePos);

				for (int i = 0; i < rMarkTemplate.m_ColorSeparations.GetSize(); i++)
				{
					SEPARATIONINFO& rSepInfo = rMarkTemplate.m_ColorSeparations[i];
					if (rSepInfo.m_nPageSourceIndex >= nPageSourceIndex) 
						rSepInfo.m_nPageSourceIndex--;
				}
			}
		}
		else
			nPageSourceIndex++;
	}
}

void CPageSourceList::InitializePageTemplateRefs(CPageTemplateList* pPageTemplateList, CColorDefinitionTable* pColorDefinitionTable)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		for (int i = 0; i < rPageSource.m_PageSourceHeaders.GetSize(); i++)
			rPageSource.m_PageSourceHeaders[i].m_PageTemplateRefs.RemoveAll();
	}

	if (!pPageTemplateList)
		if (CImpManDoc::GetDoc())
			pPageTemplateList = &CImpManDoc::GetDoc()->m_PageTemplateList;

	if (!pPageTemplateList)
		return;

	if (!pColorDefinitionTable)
		if (CImpManDoc::GetDoc())
			pColorDefinitionTable = &CImpManDoc::GetDoc()->m_ColorDefinitionTable;
	if (!pColorDefinitionTable)
		return;

	pos	= pPageTemplateList->GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = pPageTemplateList->CList::GetNext(pos);
		for (int i = 0; i < rTemplate.m_ColorSeparations.GetSize(); i++)
		{
			CPageSourceHeader* pPageSourceHeader = rTemplate.GetObjectSourceHeader(rTemplate.m_ColorSeparations[i], this, pColorDefinitionTable);

			if (pPageSourceHeader)
			{
				BOOL bExist = FALSE;
				for (int i = 0; i < pPageSourceHeader->m_PageTemplateRefs.GetSize(); i++)
					if (pPageSourceHeader->m_PageTemplateRefs[i] == &rTemplate)
					{
						bExist = TRUE;
						break;
					}
				if ( ! bExist)
					pPageSourceHeader->m_PageTemplateRefs.Add(&rTemplate);
			}
		}
	}
}


// Merge two lists together - only not yet existing elements will be appended
//							- links from page template list will be updated
void CPageSourceList::MergeElements(CPageSourceList& rPageSourceList, CPageTemplateList& rPageTemplateList)
{
	rPageTemplateList.ResetFlags();

	short	 nOldPageSourceIndex = 0;
	POSITION pos				 = rPageSourceList.GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = rPageSourceList.GetNext(pos);

		BOOL	 bFound				 = FALSE;
		short 	 nNewPageSourceIndex = 0;
		POSITION pos2				 = GetHeadPosition();
		while (pos2)
		{
			if (rPageSource == GetNext(pos2))
			{
				bFound = TRUE;
				rPageTemplateList.UpdateMarkSourceLinks(nOldPageSourceIndex, nNewPageSourceIndex);
				break;
			}
			nNewPageSourceIndex++;
		}

		if ( ! bFound)
		{
			AddTail(rPageSource);
			rPageTemplateList.UpdateMarkSourceLinks(nOldPageSourceIndex, (short)(GetCount() - 1));
		}

		nOldPageSourceIndex++;
	}

	rPageTemplateList.ResetFlags();
}


int	CPageSourceList::GetTotalNumPages()
{
	int		 nPages = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		nPages += rPageSource.GetNumPages();
	}
	return nPages;
}

//// check if PDF sources plausible for Auto Server (docs exists and located in same folder as template itself)
// check if PDF sources plausible for Auto Server (docs exists)
BOOL CPageSourceList::CheckAutoPlausibility(CImpManDoc* pDoc)		
{
	if ( ! pDoc)
		return FALSE;

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFileName[_MAX_FNAME], szExtension[_MAX_EXT];
	_tsplitpath_s(pDoc->GetPathName(), szDrive, _MAX_DRIVE, szDir, _MAX_DIR, szFileName, _MAX_FNAME, szExtension, _MAX_EXT);
	CString strTemplateFolder = CString(szDrive) + CString(szDir);
	strTemplateFolder.MakeUpper();

	BOOL	 bDeleted		  = FALSE;
	int		 nPageSourceIndex = 0;
	POSITION pos			  = GetHeadPosition();
	while (pos)
	{
		BOOL		 bUnplausible = FALSE;
		POSITION	 prevPos	  = pos;
		CPageSource& rPageSource  = GetNext(pos);
		if ( ! rPageSource.DocumentExists())
			bUnplausible = TRUE;
		//else
		//{
		//	_tsplitpath_s(rPageSource.m_strDocumentFullpath, szDrive, _MAX_DRIVE, szDir, _MAX_DIR, szFileName, _MAX_FNAME, szExtension, _MAX_EXT);
		//	CString strDocumentFolder = CString(szDrive) + CString(szDir);
		//	strDocumentFolder.MakeUpper();
		//	if (strDocumentFolder != strTemplateFolder)
		//		bUnplausible = TRUE;
		//}

		if (bUnplausible)
		{
			RemoveAt(prevPos);
			pDoc->m_PageTemplateList.RemovePageSource(nPageSourceIndex, FALSE);
			bDeleted = TRUE;
		}
		else
			nPageSourceIndex++;
	}

	if (bDeleted)
	{
		InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->SetModifiedFlag();
	}

	if ( ! GetCount() || bDeleted)
	{
		pDoc->m_Bookblock.m_OutputDate.m_dt = 0;
		return TRUE;
	}

	return bDeleted; 
} 

BOOL CPageSourceList::SourceDocumentExists(CString strDocName)
{
	strDocName.MakeUpper();
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CString strTest = GetNext(pos).m_strDocumentFullpath;
		strTest.MakeUpper();
		if (strTest == strDocName)
			return TRUE;
	}

	return FALSE; 
} 
    
#ifdef PDF
void CPageSourceList::CheckPDFsModified()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource	= GetNext(pos);
		CString		 strPDFFile		= rPageSource.m_strDocumentFullpath;
		int			 nDot			= strPDFFile.ReverseFind('.');
		CString		 strFileTitle	= (nDot >= 0) ? strPDFFile.Left(nDot) : strPDFFile;

		CString strPDFInfoFile;
		if (theApp.settings.m_bPDFPreviewsOrigPlace)
			strPDFInfoFile = strFileTitle + _T(".pdfinfo");
		else
		{
			TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
			_tsplitpath(strPDFFile, szDrive, szDir, szFname, szExt);
			strPDFInfoFile = theApp.settings.m_szPDFPreviewFolder + CString("\\") + CString(szFname) + _T(".pdfinfo");
		}

		CTime		dateNewMethod(2000, 9, 18, 0, 0, 0);	// since release 09/18/2000 we give pdfinfos same timestamp as pdf file
		BOOL		bUpdate = FALSE;						// to avoid problems with system time synchronisation (see ProcessPDFPages())
		CFileStatus statusPDFFile, statusPDFInfoFile;
		if (CFile::GetStatus(strPDFFile, statusPDFFile))		
			if (CFile::GetStatus(strPDFInfoFile, statusPDFInfoFile))		
			{
				if (statusPDFInfoFile.m_mtime < dateNewMethod)
				{
					if (statusPDFInfoFile.m_mtime < statusPDFFile.m_mtime)		// use old method to check if pdf info is out of date
						bUpdate = TRUE;
				}
				else
					if (statusPDFInfoFile.m_mtime != statusPDFFile.m_mtime)		// use new method to check if pdf info is out of date
						bUpdate = TRUE;
			}
			else
				bUpdate = TRUE;	// also update if no pdf info file present

		if (bUpdate)
		{
			theApp.m_pDlgPDFLibStatus->LaunchPDFEngine("", theApp.m_pMainWnd, CDlgPDFLibStatus::UpdatePages);
			theApp.m_pDlgPDFLibStatus->ProcessPDFPages(strPDFFile, &rPageSource);
			theApp.m_pDlgPDFLibStatus->OnCancel();

			BOOL bKeepContentOffset = UNDEFINED;
			BOOL bTrimBoxWarned		= FALSE;
			for (int i = 0; i < rPageSource.m_PageSourceHeaders.GetSize(); i++)
			{
				CPageSourceHeader& rPageSourceHeader = rPageSource.m_PageSourceHeaders[i];
				CPageTemplate*	   pPageTemplate	 = rPageSourceHeader.GetFirstPageTemplate();
				while (pPageTemplate)
				{
					if (bKeepContentOffset == UNDEFINED)	// not yet prompted
						if (pPageTemplate->HasContentOffset() || pPageTemplate->IsScaled())
							if (KIMOpenLogMessage(IDS_KEEP_CONTENTOFFSET_PROMPT, MB_YESNO, IDYES) == IDYES)
								bKeepContentOffset = TRUE;
							else
								bKeepContentOffset = FALSE;

					if ( ! bKeepContentOffset || (bKeepContentOffset == UNDEFINED) )
					{
						if (rPageSourceHeader.HasTrimBox())
						{
							if ( ! bTrimBoxWarned && theApp.settings.m_bTrimboxPagesizeMismatchWarning)
							{
								CLayoutObject* pObject = pPageTemplate->GetLayoutObject();
								if (pObject)
								{
									if ( (fabs( (rPageSourceHeader.m_fTrimBoxRight - rPageSourceHeader.m_fTrimBoxLeft)   - pObject->GetRealWidth())  >= 0.5) ||
										 (fabs( (rPageSourceHeader.m_fTrimBoxTop   - rPageSourceHeader.m_fTrimBoxBottom) - pObject->GetRealHeight()) >= 0.5) )
									{
										CDlgTrimboxWarning dlg;
										dlg.m_strText = rPageSource.m_strDocumentFullpath;
										dlg.DoModal();
										bTrimBoxWarned = TRUE;
									}
								}
							}
							float fTrimBoxCenterX = rPageSourceHeader.m_fTrimBoxLeft   + (rPageSourceHeader.m_fTrimBoxRight - rPageSourceHeader.m_fTrimBoxLeft)  /2.0f;
							float fTrimBoxCenterY = rPageSourceHeader.m_fTrimBoxBottom + (rPageSourceHeader.m_fTrimBoxTop   - rPageSourceHeader.m_fTrimBoxBottom)/2.0f;
							float fBBoxCenterX	  = rPageSourceHeader.m_fBBoxLeft	   + (rPageSourceHeader.m_fBBoxRight	- rPageSourceHeader.m_fBBoxLeft)	 /2.0f;
							float fBBoxCenterY	  = rPageSourceHeader.m_fBBoxBottom	   + (rPageSourceHeader.m_fBBoxTop	    - rPageSourceHeader.m_fBBoxBottom)	 /2.0f;
							pPageTemplate->SetContentOffsetX(fBBoxCenterX - fTrimBoxCenterX);
							pPageTemplate->SetContentOffsetY(fBBoxCenterY - fTrimBoxCenterY);
							CPageListDetailView* pView = (CPageListDetailView*)theApp.GetView(RUNTIME_CLASS(CPageListDetailView));
							if (pView)
								pView->OnUpdate(NULL, 0, NULL);
						}
	//					if (rPageSourceHeader.m_bPageNumberFix)
	//						pPageTemplate->m_strPageID = rPageSourceHeader.m_strPageName;
						pPageTemplate->m_fContentScaleX = 1.0f;
						pPageTemplate->m_fContentScaleY = 1.0f;
					}

					pPageTemplate = rPageSourceHeader.GetNextPageTemplate(pPageTemplate);
				}
			}
			CPageSourceListView* pView = (CPageSourceListView*)theApp.GetView(RUNTIME_CLASS(CPageSourceListView));
			if (pView)
				pView->OnUpdate(NULL, 0, NULL);
		}
		else
			if (rPageSource.m_pdfInfoDate < statusPDFInfoFile.m_mtime)		// also update (but not re-read content) if page source data is older than pdf info
				rPageSource.UpdatePDFInfo(strPDFFile, strPDFInfoFile);
	}
}

void CPageSourceList::CheckDocuments()
{
	if ( ! theApp.IsViewVisible(RUNTIME_CLASS(CPageSourceListView)) && ! theApp.IsViewVisible(RUNTIME_CLASS(CProductsView)) )
		return;

	BOOL bDoUpdateViews = FALSE;
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		if ( ! rPageSource.DocumentExists())
		{
			if (rPageSource.m_bDocExist)
			{
				bDoUpdateViews = TRUE;
				rPageSource.m_bDocExist = FALSE;
			}
		}
		else
		{
			if ( ! rPageSource.m_bDocExist)
			{
				bDoUpdateViews = TRUE;
				rPageSource.m_bDocExist = TRUE;
			}
		}
	}

	if (bDoUpdateViews)
	{
		theApp.UpdateView(RUNTIME_CLASS(CPageSourceListView));	// update views to display pathname lowlighted (gray)
		theApp.UpdateView(RUNTIME_CLASS(CProductsView));
	}
}
#endif


void CPageSourceList::UpdateColorInfos()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rObjectSource = GetNext(pos);
		for (int i = 0; i < rObjectSource.m_PageSourceHeaders.GetSize(); i++)
		{
			rObjectSource.m_PageSourceHeaders[i].m_rgb  = theApp.m_colorDefTable.FindColorRef(rObjectSource.m_PageSourceHeaders[i].m_strColorName);
		}
	}
}

BOOL CPageSourceList::IsSpotcolorDefined(const CString& strSpotColorName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		if (rPageSource.m_spotcolorDefList.IsSpotcolorDefined(strSpotColorName))
			return TRUE;
	}

	return FALSE;
}

COLORREF CPageSourceList::GetAlternateCMYK(const CString& strSpotColorName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		if (rPageSource.m_spotcolorDefList.IsSpotcolorDefined(strSpotColorName))
			return rPageSource.m_spotcolorDefList.GetAlternateCMYK(strSpotColorName);
	}

	return CMYK(0,0,0,0);
}

COLORREF CPageSourceList::GetAlternateRGB(const CString& strSpotColorName)
{
	COLORREF crCMYK = GetAlternateCMYK(strSpotColorName);
	if (crCMYK == CMYK(0,0,0,0))
		return WHITE;
	else
		return CColorDefinition::CMYK2RGB(crCMYK);
}

void CPageSourceList::GetPDFInputInfo(CJobInfoExchange& rJobInfoExchange)
{
	rJobInfoExchange.m_pdfInputInfoList.RemoveAll();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	TCHAR szDrive[_MAX_DRIVE], szDir[_MAX_DIR], szFname[_MAX_FNAME], szExt[_MAX_EXT];
	CPDFInputInfo pdfInputInfo;
	POSITION  pos = GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = GetNext(pos);
		_tsplitpath((LPCTSTR)rPageSource.m_strDocumentFullpath, szDrive, szDir, szFname, szExt);
		pdfInputInfo.m_strName			= CString(szFname) + CString(szExt);
		pdfInputInfo.m_strDocPath		= CString(szDrive) + _T("\\") + CString(szDir);

		_tsplitpath((LPCTSTR)rPageSource.m_strPreviewFilesPath, szDrive, szDir, szFname, szExt);
		pdfInputInfo.m_strPreviewPath	= CString(szDrive) + _T("\\") + CString(szDir);

		pdfInputInfo.m_nMinPage = 1;
		pdfInputInfo.m_nMaxPage = rPageSource.m_PageSourceHeaders.GetSize();
		pdfInputInfo.m_nNumPages = pdfInputInfo.m_nMaxPage - pdfInputInfo.m_nMinPage + 1;

		rJobInfoExchange.m_pdfInputInfoList.AddTail(pdfInputInfo);
	}
}



/////////////////////////////////////////////////////////////////////////////
// CPageSource

IMPLEMENT_SERIAL(CPageSource, CPageSource, VERSIONABLE_SCHEMA | 6)

CPageSource::CPageSource()
{
	m_nType					 = PDFFile;
	m_nColorant				 = ColorantUnknown;
	m_fGlobalBBoxLeft		 = 0.0f;
	m_fGlobalBBoxBottom		 = 0.0f;
	m_fGlobalBBoxRight		 = 0.0f; 
	m_fGlobalBBoxTop		 = 0.0f;
	m_pdfInfoDate			 = 0;
	m_bBmpFP				 = FALSE;
	m_bPreviewsInHiresFolder = FALSE;

	m_bFontsCopied = FALSE;
	m_bDocExist	   = TRUE;

	m_spotcolorDefList.RemoveAll();
}

// copy constructor for CPageSource - needed by CList functions:

CPageSource::CPageSource(const CPageSource& rPageSource) 
{
	Copy(rPageSource);
}


const CPageSource& CPageSource::operator=(const CPageSource& rPageSource)
{
	Copy(rPageSource);

	return *this;
}


BOOL CPageSource::operator==(const CPageSource& rPageSource)
{
	if (m_nType						!= rPageSource.m_nType)
		return FALSE;				
	if (m_strDocumentFullpath		!= rPageSource.m_strDocumentFullpath)
		return FALSE;				
	if (m_strContentFilesPath		!= rPageSource.m_strContentFilesPath)
		return FALSE;				
	if (m_strPreviewFilesPath		!= rPageSource.m_strPreviewFilesPath)
		return FALSE;				
	if (m_strDocumentTitle			!= rPageSource.m_strDocumentTitle)
		return FALSE;				
	if (m_strCreator				!= rPageSource.m_strCreator)
		return FALSE;				
	if (m_strCreationDate			!= rPageSource.m_strCreationDate)
		return FALSE;				
	if (m_strSystemCreationInfo		!= rPageSource.m_strSystemCreationInfo)
		return FALSE;				
	if (m_nColorant					!= rPageSource.m_nColorant)
		return FALSE;				
	if (m_fGlobalBBoxLeft			!= rPageSource.m_fGlobalBBoxLeft)
		return FALSE;				
	if (m_fGlobalBBoxBottom			!= rPageSource.m_fGlobalBBoxBottom)
		return FALSE;				
	if (m_fGlobalBBoxRight			!= rPageSource.m_fGlobalBBoxRight)
		return FALSE;				
	if (m_fGlobalBBoxTop			!= rPageSource.m_fGlobalBBoxTop)
		return FALSE;				
	if (m_pdfInfoDate				!= rPageSource.m_pdfInfoDate)
		return FALSE;				
	if (m_bBmpFP					!= rPageSource.m_bBmpFP)
		return FALSE;				
	if (m_bPreviewsInHiresFolder	!= rPageSource.m_bPreviewsInHiresFolder)
		return FALSE;

	if (m_PageSourceHeaders.GetSize() != rPageSource.m_PageSourceHeaders.GetSize())
		return FALSE;

	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		if (m_PageSourceHeaders[i] != rPageSource.m_PageSourceHeaders[i])
			return FALSE;
	}

	for (int i = 0; i < m_spotcolorDefList.GetSize(); i++)
	{
		if (m_spotcolorDefList[i].m_strSpotColorName != m_spotcolorDefList[i].m_strSpotColorName)
			return FALSE;
		if (m_spotcolorDefList[i].m_alternateValues.fC  != m_spotcolorDefList[i].m_alternateValues.fC)
			return FALSE;
		if (m_spotcolorDefList[i].m_alternateValues.fM  != m_spotcolorDefList[i].m_alternateValues.fM)
			return FALSE;
		if (m_spotcolorDefList[i].m_alternateValues.fY  != m_spotcolorDefList[i].m_alternateValues.fY)
			return FALSE;
		if (m_spotcolorDefList[i].m_alternateValues.fK  != m_spotcolorDefList[i].m_alternateValues.fK)
			return FALSE;
	}

	return TRUE;
}


BOOL CPageSource::operator!=(const CPageSource& rPageSource)
{
	return !(*this == rPageSource);
}


void CPageSource::Copy(const CPageSource& rPageSource)
{
	m_nType					 = rPageSource.m_nType;
	m_strDocumentFullpath	 = rPageSource.m_strDocumentFullpath;
	m_strContentFilesPath	 = rPageSource.m_strContentFilesPath;
	m_strPreviewFilesPath	 = rPageSource.m_strPreviewFilesPath;
	m_strDocumentTitle		 = rPageSource.m_strDocumentTitle;
	m_strCreator			 = rPageSource.m_strCreator;
	m_strCreationDate		 = rPageSource.m_strCreationDate;
	m_strSystemCreationInfo  = rPageSource.m_strSystemCreationInfo;
	m_nColorant				 = rPageSource.m_nColorant;
	m_fGlobalBBoxLeft		 = rPageSource.m_fGlobalBBoxLeft;
	m_fGlobalBBoxBottom		 = rPageSource.m_fGlobalBBoxBottom;	
	m_fGlobalBBoxRight		 = rPageSource.m_fGlobalBBoxRight;
	m_fGlobalBBoxTop		 = rPageSource.m_fGlobalBBoxTop;		
	m_pdfInfoDate			 = rPageSource.m_pdfInfoDate;
	m_bBmpFP				 = rPageSource.m_bBmpFP;
	m_bPreviewsInHiresFolder = rPageSource.m_bPreviewsInHiresFolder;

	CPageSourceHeader PageSourceHeader;
	m_PageSourceHeaders.RemoveAll();
	for (int i = 0; i < rPageSource.m_PageSourceHeaders.GetSize(); i++)
	{
		PageSourceHeader = rPageSource.m_PageSourceHeaders[i];
		m_PageSourceHeaders.Add(PageSourceHeader);
		//m_PageSourceHeaders.Add((CPageSourceHeader)rPageSource.m_PageSourceHeaders[i]);
	}

	m_spotcolorDefList.RemoveAll();
	m_spotcolorDefList.Append(rPageSource.m_spotcolorDefList);

	m_bDocExist	= rPageSource.m_bDocExist;
}


// helper function for PageSourceList elements
template <> void AFXAPI SerializeElements <CPageSource> (CArchive& ar, CPageSource* pPageSource, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CPageSource));

	if (ar.IsStoring())
	{
		ar << pPageSource->m_nType;
		ar << pPageSource->m_strDocumentFullpath;
		ar << pPageSource->m_strContentFilesPath;
		ar << pPageSource->m_strPreviewFilesPath;
		ar << pPageSource->m_strDocumentTitle;
		ar << pPageSource->m_strCreator;
		ar << pPageSource->m_strCreationDate;
		ar << pPageSource->m_strSystemCreationInfo;
		ar << pPageSource->m_nColorant;
		ar << pPageSource->m_fGlobalBBoxLeft;
		ar << pPageSource->m_fGlobalBBoxBottom;
		ar << pPageSource->m_fGlobalBBoxRight;
		ar << pPageSource->m_fGlobalBBoxTop;
		ar << pPageSource->m_pdfInfoDate;
		ar << pPageSource->m_bBmpFP;
		ar << pPageSource->m_bPreviewsInHiresFolder;
		pPageSource->m_spotcolorDefList.Serialize(ar);
		pPageSource->m_PageSourceHeaders.Serialize(ar);
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		pPageSource->m_PageSourceHeaders.RemoveAll();
		pPageSource->m_spotcolorDefList.RemoveAll();
		switch (nVersion)
		{
		case 1:
			{
			pPageSource->m_nType = CPageSource::PDFFile;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			pPageSource->m_nColorant = CPageSource::ColorantUnknown;
			}
			break;

		case 2:
			{
			ar >> pPageSource->m_nType;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_strSystemCreationInfo;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			pPageSource->m_nColorant = CPageSource::ColorantUnknown;
			}
			break;

		case 3:
			{
			ar >> pPageSource->m_nType;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_strSystemCreationInfo;
			ar >> pPageSource->m_nColorant;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			}
			break;

		case 4:
			{
			ar >> pPageSource->m_nType;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_strSystemCreationInfo;
			ar >> pPageSource->m_nColorant;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			ar >> pPageSource->m_pdfInfoDate;
			}
			break;

		case 5:
			{
			ar >> pPageSource->m_nType;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_strSystemCreationInfo;
			ar >> pPageSource->m_nColorant;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			ar >> pPageSource->m_pdfInfoDate;
			ar >> pPageSource->m_bBmpFP;
			ar >> pPageSource->m_bPreviewsInHiresFolder;
			}
			break;

		case 6:
			{
			ar >> pPageSource->m_nType;
			ar >> pPageSource->m_strDocumentFullpath;
			ar >> pPageSource->m_strContentFilesPath;
			ar >> pPageSource->m_strPreviewFilesPath;
			ar >> pPageSource->m_strDocumentTitle;
			ar >> pPageSource->m_strCreator;
			ar >> pPageSource->m_strCreationDate;
			ar >> pPageSource->m_strSystemCreationInfo;
			ar >> pPageSource->m_nColorant;
			ar >> pPageSource->m_fGlobalBBoxLeft;
			ar >> pPageSource->m_fGlobalBBoxBottom;
			ar >> pPageSource->m_fGlobalBBoxRight;
			ar >> pPageSource->m_fGlobalBBoxTop;
			ar >> pPageSource->m_pdfInfoDate;
			ar >> pPageSource->m_bBmpFP;
			ar >> pPageSource->m_bPreviewsInHiresFolder;
			pPageSource->m_spotcolorDefList.Serialize(ar);
			}
			break;
		}

		if (pPageSource->m_strDocumentFullpath.Find(_T("\\\\172.25.2.20\\BigBen-Neu\\Etiketten_KIM_7\\Input\\")) >= 0)
			pPageSource->m_strDocumentFullpath.Replace(_T("\\\\172.25.2.20\\BigBen-Neu\\Etiketten_KIM_7\\Input\\"), _T("C:\\Program Files (x86)\\KIM7.1\\PDF-Input\\H.O.Persiehl\\"));
		if (pPageSource->m_strContentFilesPath.Find(_T("\\\\172.25.2.20\\BigBen-Neu\\Etiketten_KIM_7\\Input\\")) >= 0)
			pPageSource->m_strContentFilesPath.Replace(_T("\\\\172.25.2.20\\BigBen-Neu\\Etiketten_KIM_7\\Input\\"), _T("C:\\Program Files (x86)\\KIM7.1\\PDF-Input\\H.O.Persiehl\\"));
		if (pPageSource->m_strPreviewFilesPath.Find(_T("\\\\172.25.2.20\\BigBen-Neu\\Etiketten_KIM_7\\Input\\")) >= 0)
			pPageSource->m_strPreviewFilesPath.Replace(_T("\\\\172.25.2.20\\BigBen-Neu\\Etiketten_KIM_7\\Input\\"), _T("C:\\Program Files (x86)\\KIM7.1\\PDF-Input\\H.O.Persiehl\\"));

		pPageSource->m_PageSourceHeaders.Serialize(ar);

		if (nVersion < 6)	// old jobs: take spotcolor defs from system color table
		{
			for (int i = 0; i < pPageSource->m_PageSourceHeaders.GetSize(); i++)
			{
				CPageSourceHeader& rHeader = pPageSource->m_PageSourceHeaders[i];
				for (int j = 0; j < pPageSource->m_PageSourceHeaders[i].m_spotColors.GetSize(); j++)
				{
					CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(pPageSource->m_PageSourceHeaders[i].m_spotColors[j]);
					if (pColorDef)
					{
						COLORREF crCMYK = (pColorDef->m_cmyk == 0) ? CColorDefinition::RGB2CMYK(pColorDef->m_rgb) : pColorDef->m_cmyk;
						CSpotcolorDefs spotcolorDefs;
						spotcolorDefs.m_strSpotColorName = pPageSource->m_PageSourceHeaders[i].m_spotColors[j];
						spotcolorDefs.m_alternateValues.fC = (float)GetCValue(crCMYK)/100.0f;
						spotcolorDefs.m_alternateValues.fM = (float)GetMValue(crCMYK)/100.0f;
						spotcolorDefs.m_alternateValues.fY = (float)GetYValue(crCMYK)/100.0f;
						spotcolorDefs.m_alternateValues.fK = (float)GetKValue(crCMYK)/100.0f;
						pPageSource->m_spotcolorDefList.InsertSpotcolorDefs(spotcolorDefs);
					}
				}
			}
		}
	}
}


void AFXAPI DestructElements(CPageSource* pPageSource, INT_PTR nCount)
{
	ASSERT(nCount == 1); // in CList context nCount has to be 1

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pPageSource->m_strDocumentFullpath.Empty();
	pPageSource->m_strContentFilesPath.Empty();
	pPageSource->m_strPreviewFilesPath.Empty();
	pPageSource->m_strDocumentTitle.Empty();
	pPageSource->m_strCreator.Empty();
	pPageSource->m_strCreationDate.Empty();
	pPageSource->m_strSystemCreationInfo.Empty();
	pPageSource->m_PageSourceHeaders.RemoveAll();
	pPageSource->m_spotcolorDefList.RemoveAll();
}


BOOL CPageSource::Create(int nType, const CString& strDocumentFullpath, const CString& strContentFilesPath, const CString& strPreviewFilesPath)
{
	m_nType				  = nType;
	m_strDocumentFullpath = strDocumentFullpath; 
	m_strContentFilesPath = strContentFilesPath;	 
	m_strPreviewFilesPath = strPreviewFilesPath;	 
	m_fGlobalBBoxLeft	  =  FLT_MAX;
	m_fGlobalBBoxBottom	  =  FLT_MAX;
	m_fGlobalBBoxRight	  = -FLT_MAX; 
	m_fGlobalBBoxTop	  = -FLT_MAX;

	if ( ! CPageSource::IsPdfEngineReport(strContentFilesPath))
	{
		CPostScriptParser parser;
		parser.Configure(CPostScriptParser::DocTitleSym, CPostScriptParser::PDICreatorSym, CPostScriptParser::PDICreationDateSym, CPostScriptParser::PDIColorantSym, -1);
		parser.ParsePSFile(strContentFilesPath);	// in PDF version strContentFilesPath holds the pdf info file fullpath

		m_strDocumentTitle	= parser.m_strDocTitle;
		m_strCreator		= parser.m_strCreator;	  
		m_strCreationDate	= parser.m_strCreationDate; 
		m_nColorant			= parser.m_nColorant;
	}

//	return ( ! strPreviewFilesPath.IsEmpty()) ? CreateDirectory(m_strPreviewFilesPath, NULL) : TRUE;
	return FALSE;
}

IPL_bool_t CPageSource::EnumPagePropertyCallBack( IPL_TENUMPAGEPROPERTY* pageprop, void* data )
{
	PDFEnginePageList* pPageList = (PDFEnginePageList*)data;

	CPDFEnginePageListEntry entry;
	entry.m_nIndex			   = pageprop->pageindex;
	entry.m_strSeparationColor = pageprop->separation;
	entry.m_nPageNum		   = ( (pageprop->separationnum > 0) && ! entry.m_strSeparationColor.IsEmpty()) ? pageprop->separationnum : entry.m_nIndex + 1;

	entry.m_fArtBoxBottom	= pageprop->artbox.fr.bottom;
	entry.m_fArtBoxTop		= pageprop->artbox.fr.top;
	entry.m_fArtBoxLeft		= pageprop->artbox.fr.left; 
	entry.m_fArtBoxRight	= pageprop->artbox.fr.right; 
 
	entry.m_fBleedBoxBottom = pageprop->bleedbox.fr.bottom;
	entry.m_fBleedBoxTop	= pageprop->bleedbox.fr.top;
	entry.m_fBleedBoxLeft	= pageprop->bleedbox.fr.left;
	entry.m_fBleedBoxRight  = pageprop->bleedbox.fr.right;

	entry.m_fCropBoxBottom	= pageprop->cropbox.fr.bottom;
	entry.m_fCropBoxTop		= pageprop->cropbox.fr.top;
	entry.m_fCropBoxLeft	= pageprop->cropbox.fr.left;
	entry.m_fCropBoxRight	= pageprop->cropbox.fr.right;

	entry.m_fMediaBoxBottom = pageprop->mediabox.fr.bottom;
	entry.m_fMediaBoxTop	= pageprop->mediabox.fr.top;
	entry.m_fMediaBoxLeft   = pageprop->mediabox.fr.left;
	entry.m_fMediaBoxRight  = pageprop->mediabox.fr.right;

	entry.m_fTrimBoxBottom	= pageprop->trimbox.fr.bottom;
	entry.m_fTrimBoxTop		= pageprop->trimbox.fr.top;
	entry.m_fTrimBoxLeft	= pageprop->trimbox.fr.left;
	entry.m_fTrimBoxRight	= pageprop->trimbox.fr.right;

	pPageList->Add(entry);

	return true;
}

void CPageSource::RegisterPDFPages(const CString& strFileName, const CString& strInfoFileName, int nStartPage)
{
	CPDFEngineXMLParser xmlParser;
	if ( ! CPageSource::IsPdfEngineReport(strInfoFileName))
	{
		CPostScriptParser parser;
		parser.Configure(CPostScriptParser::PDIPageListSym, -1);
		parser.ParsePSFile(strInfoFileName);
		xmlParser.CopyOldPDFPageList(parser);
		m_strDocumentTitle	= parser.m_strDocTitle;
		m_strCreator		= parser.m_strCreator;	  
		m_strCreationDate	= parser.m_strCreationDate; 
		m_nColorant			= parser.m_nColorant;
	}
	else
	{
		CSysToUTF8Converter s2utf8;
		IPL_PDFDocument_EnumPageProperties( s2utf8.Convert(strFileName), 0, EnumPagePropertyCallBack, (void*)&xmlParser.m_pdfPageList);
		xmlParser.ParseXML(strInfoFileName);

		m_strDocumentTitle	= xmlParser.m_strDocumentTitle;
		m_strCreator		= xmlParser.m_strCreator;	  
		m_strCreationDate	= xmlParser.m_strCreationDate; 
		if (xmlParser.m_pdfPageList.GetSize())
			m_nColorant = (xmlParser.m_pdfPageList[0].m_strSeparationColor.IsEmpty()) ? CPageSource::Composite : CPageSource::Separated;
		else
			m_nColorant = CPageSource::ColorantUnknown;
	
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			for (int i = 0; i < xmlParser.m_spotcolorDefList.GetSize(); i++)
				m_spotcolorDefList.InsertSpotcolorDefs(xmlParser.m_spotcolorDefList[i]);
		}
	}

	int				  nPageNum	      = (nStartPage != -1) ? nStartPage : 0;
	int				  nPrevPageNum	  = (xmlParser.m_pdfPageList.GetSize()) ? xmlParser.m_pdfPageList[0].m_nPageNum : -1;
	BOOL			  bNotYetPrompted = TRUE;
	CPageSourceHeader pageSourceHeader;
	for (int i = 0; i < xmlParser.m_pdfPageList.GetSize(); i++)
	{
		CPDFEnginePageListEntry& entry = xmlParser.m_pdfPageList[i];

		CString strColorant = entry.m_strSeparationColor;
		if ( strColorant.IsEmpty())
			strColorant = _T("Composite");

		if (nStartPage != -1)	// page numbering via naming convention (AUTO only)
		{
			if (nPrevPageNum != entry.m_nPageNum)	// increase absolute page num only if relativ page num changes -> relevant for separated jobs
				nPageNum++;
			nPrevPageNum = entry.m_nPageNum;
			entry.m_nPageNum = nPageNum;
			entry.m_strPageLabel.Empty();
		}

		if ( !entry.CropBoxEmpty())
			pageSourceHeader.Create(entry.m_nPageNum, entry.m_strPageLabel, 
									entry.m_fCropBoxLeft,  entry.m_fCropBoxBottom, entry.m_fCropBoxRight, entry.m_fCropBoxTop, 
									strColorant);
		else
			pageSourceHeader.Create(entry.m_nPageNum, entry.m_strPageLabel, 
									entry.m_fMediaBoxLeft,  entry.m_fMediaBoxBottom, entry.m_fMediaBoxRight, entry.m_fMediaBoxTop, 
									strColorant);

		entry.m_fTrimBoxLeft *= PICA_POINTS; entry.m_fTrimBoxRight *= PICA_POINTS; entry.m_fTrimBoxTop *= PICA_POINTS; entry.m_fTrimBoxBottom *= PICA_POINTS;
		// if trimbox equals crop/mediabox we define trimbox as not existent to avoid final pagesize mismatch warnings while assigning pages
		if ( (fabs(entry.m_fTrimBoxLeft - pageSourceHeader.m_fBBoxLeft) < 0.01) && (fabs(entry.m_fTrimBoxRight  - pageSourceHeader.m_fBBoxRight)  < 0.01) &&
			 (fabs(entry.m_fTrimBoxTop  - pageSourceHeader.m_fBBoxTop)  < 0.01) && (fabs(entry.m_fTrimBoxBottom - pageSourceHeader.m_fBBoxBottom) < 0.01) )
		{
			pageSourceHeader.m_fTrimBoxLeft = pageSourceHeader.m_fTrimBoxRight = pageSourceHeader.m_fTrimBoxTop = pageSourceHeader.m_fTrimBoxBottom = 0.0f;
		}
		else
		{
			pageSourceHeader.m_fTrimBoxLeft  = entry.m_fTrimBoxLeft;  pageSourceHeader.m_fTrimBoxBottom = entry.m_fTrimBoxBottom;
			pageSourceHeader.m_fTrimBoxRight = entry.m_fTrimBoxRight; pageSourceHeader.m_fTrimBoxTop	= entry.m_fTrimBoxTop;
		}

		pageSourceHeader.GetProcessColors(entry.m_plateNames);
		pageSourceHeader.GetSpotColors(entry.m_plateNames);

		m_PageSourceHeaders.Add(pageSourceHeader);

		m_fGlobalBBoxLeft	= min(m_fGlobalBBoxLeft,   pageSourceHeader.m_fBBoxLeft);	// Find document's global bbox
		m_fGlobalBBoxBottom = min(m_fGlobalBBoxBottom, pageSourceHeader.m_fBBoxBottom);
		m_fGlobalBBoxRight  = max(m_fGlobalBBoxRight,  pageSourceHeader.m_fBBoxRight);
		m_fGlobalBBoxTop	= max(m_fGlobalBBoxTop,	   pageSourceHeader.m_fBBoxTop);
	}

	CFileStatus statusInfoFile;
	if (CFile::GetStatus(strInfoFileName, statusInfoFile))		
		m_pdfInfoDate = statusInfoFile.m_mtime;
}
 
void CPageSource::UpdatePDFInfo(const CString& strFileName, const CString& strInfoFileName)
{
	CFileStatus status;
	if ( ! CFile::GetStatus(strFileName, status))		
		return;

	m_PageSourceHeaders.RemoveAll();
	RegisterPDFPages(strFileName, strInfoFileName);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_PageTemplateList.CheckPageSourceHeaders();
		pDoc->m_PageSourceList.InitializePageTemplateRefs();
		pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
		pDoc->m_PrintSheetList.ReorganizeColorInfos();
		pDoc->SetModifiedFlag(TRUE, NULL, FALSE,FALSE);										
		theApp.OnUpdateView(RUNTIME_CLASS(CPageListView), NULL, 0, NULL);
	}
}

void CPageSource::ReorganizePageNumbers()
{
	int nCurPageNum = (m_PageSourceHeaders.GetSize() > 0) ? m_PageSourceHeaders[0].m_nPageNumber : 0;
	int nPageNum	= 1;
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		if (m_PageSourceHeaders[i].m_nPageNumber != nCurPageNum)
			nPageNum++;
		nCurPageNum = m_PageSourceHeaders[i].m_nPageNumber;
//		if (!m_PageSourceHeaders[i].m_bPageNumberFix)
			m_PageSourceHeaders[i].m_nPageNumber = nPageNum;
	}
}


short CPageSource::GetHeaderIndex(CPageSourceHeader* pPageSourceHeader)
{
	if ( ! pPageSourceHeader)
		return -1;

	for (short i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		if (&m_PageSourceHeaders[i] == pPageSourceHeader)
			return i;
	}
	return -1;
}


void CPageSource::Clear()
{
	m_nType = PSFile;
	m_strDocumentFullpath.Empty();
	m_strContentFilesPath.Empty();
	m_strPreviewFilesPath.Empty();
	m_strDocumentTitle.Empty();
	m_strCreator.Empty();
	m_strCreationDate.Empty();
	m_strSystemCreationInfo.Empty();

	m_nColorant		  = ColorantUnknown;
	m_fGlobalBBoxLeft = m_fGlobalBBoxBottom = m_fGlobalBBoxRight = m_fGlobalBBoxTop = 0.0f;
	m_PageSourceHeaders.RemoveAll();
	m_bFontsCopied	  = FALSE;
	m_bDocExist		  = TRUE;
}


// CTP-Version: Initialize BBox by using preview file 
// If file does not exist, bbox is set to 0,0,0,0
void CPageSource::InitBBox(CPageSourceHeader* pPageSourceHeader, CPrintSheet* pPrintSheet, CPlate* pPlate)
{
#ifdef CTP
	CString	strPreviewFile = GetPreviewFileName(0, pPageSourceHeader, pPrintSheet, pPlate);
	CFileStatus status;
	if ( ! CFile::GetStatus(strPreviewFile, status))
		return;

	extern int nJPEGOutWidth, nJPEGOutHeight; 
	nJPEGOutWidth  = 0; // used by paintlib in CJPEGDecoder::DoDecode() (jpegdec.cpp)
	nJPEGOutHeight = 0;

    CWinBmpEx* pDib = new CWinBmpEx;

    try
    {
		CAnyPicDecoder decoder;
        decoder.MakeBmpFromFile(strPreviewFile, pDib, 0, NULL);
    }
    catch (CTextException e)
    {
        TRACE(e);
		delete pDib;
		return;
    }
 
	if ( ! pDib)
		return;

//	int nWidth  = ( (nHeadPosition == TOP) || (nHeadPosition == BOTTOM) ) ? pDib->GetWidth()  : pDib->GetHeight();
//	int nHeight = ( (nHeadPosition == TOP) || (nHeadPosition == BOTTOM) ) ? pDib->GetHeight() : pDib->GetWidth();
	int nWidth  = pDib->GetWidth();
	int nHeight = pDib->GetHeight();
	if (pPageSourceHeader)
	{
		pPageSourceHeader->m_fBBoxLeft	 = 0.0f;
		pPageSourceHeader->m_fBBoxBottom = 0.0f;
		pPageSourceHeader->m_fBBoxRight	 = (float)((nWidth  / 72.0) * 25.4);
		pPageSourceHeader->m_fBBoxTop	 = (float)((nHeight / 72.0) * 25.4);
		for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
		{
			m_fGlobalBBoxLeft	= min(m_fGlobalBBoxLeft,   m_PageSourceHeaders[i].m_fBBoxLeft);	// Find global bbox
			m_fGlobalBBoxBottom = min(m_fGlobalBBoxBottom, m_PageSourceHeaders[i].m_fBBoxBottom);
			m_fGlobalBBoxRight  = max(m_fGlobalBBoxRight,  m_PageSourceHeaders[i].m_fBBoxRight);
			m_fGlobalBBoxTop	= max(m_fGlobalBBoxTop,	   m_PageSourceHeaders[i].m_fBBoxTop);
		}
	}
	else
	{
		m_fGlobalBBoxLeft	= 0.0f;
		m_fGlobalBBoxBottom = 0.0f;
		m_fGlobalBBoxRight	= (float)((nWidth  / 72.0) * 25.4);
		m_fGlobalBBoxTop	= (float)((nHeight / 72.0) * 25.4);
	}

	delete pDib;

#endif
}


// Get number of different pages (not page separations)
int	CPageSource::GetNumPages()
{
	int	nCurPageNum = 0;
	int nPages		= 0;
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		if (m_PageSourceHeaders[i].m_nPageNumber != nCurPageNum)
			nPages++;

		nCurPageNum = m_PageSourceHeaders[i].m_nPageNumber;
	}
	return nPages;
}

#ifdef CTP
CString	CPageSource::GetHiResBMPFileName(CPageSourceHeader* pObjectSourceHeader)
{
	if (m_strContentFilesPath.GetLength() > 0)
		if (m_strContentFilesPath[m_strContentFilesPath.GetLength() - 1] != '\\')
			m_strContentFilesPath += '\\';

	CString strFileName;
	if (m_bBmpFP)
	{
		if (theApp.settings.m_bExtendedColorHandling)
			strFileName.Format(_T("%04d\\*_%s"),   (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_strColorName : "000");
		else
			strFileName.Format(_T("%04d\\*_%03d"), (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_nColorIndex : 0);

		WIN32_FIND_DATA findData;
		HANDLE hfileSearch = FindFirstFile(m_strContentFilesPath + strFileName, &findData);
		if (hfileSearch != INVALID_HANDLE_VALUE)
			strFileName.Format(_T("%04d\\%s"), (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, findData.cFileName);
	}
	else
	{
		if (theApp.settings.m_bExtendedColorHandling)
			strFileName.Format(_T("%08d.%s"),   (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_strColorName : "000");
		else
			strFileName.Format(_T("%08d.%03d"), (pObjectSourceHeader) ? pObjectSourceHeader->m_nPageNumber : 0, (pObjectSourceHeader) ? pObjectSourceHeader->m_nColorIndex : 0);
	}

	return strFileName;
}
#endif

CString	CPageSource::GetPreviewFileName(int nPageNumInSourceFile)
{
	if (m_strPreviewFilesPath.GetLength() > 0)
		if (m_strPreviewFilesPath[m_strPreviewFilesPath.GetLength() - 1] != '\\')
			m_strPreviewFilesPath += '\\';

	CString strFileName; strFileName.Format(_T("%sKim%05d"), m_strPreviewFilesPath, nPageNumInSourceFile - 1);	// numbers beginning with 0
	return strFileName;
}

BOOL CPageSource::HasLinks(void)
{
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
		if (m_PageSourceHeaders[i].m_PageTemplateRefs.GetSize())
			return TRUE;

	return FALSE;
}

CPrintSheet* CPageSource::GetFirstPrintSheet()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	POSITION	pos  = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.HasPageSource(this))
			return &rPrintSheet;
	}

	return NULL;
}

CPrintSheet* CPageSource::GetNextPrintSheet(CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	POSITION	pos	 = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (&rPrintSheet == pPrintSheet)
			break;
	}

	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.HasPageSource(this))
			return &rPrintSheet;
	}

	return NULL;
}

BOOL CPageSource::DocumentExists(CPrintSheet* pPrintSheet, BOOL bLogMessage)
{
	CString	strDoc = m_strDocumentFullpath;
	if (pPrintSheet)
	{
		TCHAR szDest[1024];
		CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strDoc, pPrintSheet, NULL, -1);
		strDoc = szDest;
	}

	CFileStatus fileStatus;
	if (CFile::GetStatus(strDoc, fileStatus))	// file exists
		return TRUE;
	else
	{
#ifdef KIM_ENGINE
		if ( ! bLogMessage)
			return FALSE;
		CString strMsg, strCannotOpenFile;
		strCannotOpenFile.LoadString(IDS_PRINTJOB_OPENFILE_ERROR);
		strMsg.Format(_T("%s - %s"), strDoc, strCannotOpenFile);
		KIMLogMessage(strMsg, KIM_MSGTYPE_ERROR);	
#endif
		return FALSE;
	}
}

int	CPageSource::GetSystemGeneratedMarkType()
{
	if (m_nType != SystemCreated)
		return -1;

	int	nIndex = m_strSystemCreationInfo.Find(_T(" "));
#ifdef PDF
	if (nIndex == -1)	// invalid string
		return -1;
#endif

	CString strContentType = m_strSystemCreationInfo.Left(nIndex);

	if (strContentType == "$TEXT")
		return CMark::TextMark;
	else
		if (strContentType == "$SECTION_BAR")
			return CMark::SectionMark;
		else
			if (strContentType == "$CROP_LINE")
				return CMark::CropMark;
			else
				if (strContentType == "$FOLD_MARK")
					return CMark::FoldMark;
				else
					if (strContentType == "$BARCODE")
						return CMark::BarcodeMark;
					else
						if (strContentType == "$RECTANGLE")
							return CMark::DrawingMark;
						else
							return CMark::CustomMark;
}

float CPageSource::GetBBox(int nSide, CPageSourceHeader* pObjectSourceHeader)
{
	switch (nSide)
	{
	case LEFT:		return (pObjectSourceHeader) ? pObjectSourceHeader->m_fBBoxLeft	  : m_fGlobalBBoxLeft;	 break;
	case RIGHT:		return (pObjectSourceHeader) ? pObjectSourceHeader->m_fBBoxRight  : m_fGlobalBBoxRight;  break;
	case BOTTOM:	return (pObjectSourceHeader) ? pObjectSourceHeader->m_fBBoxBottom : m_fGlobalBBoxBottom; break;
	case TOP:		return (pObjectSourceHeader) ? pObjectSourceHeader->m_fBBoxTop	  : m_fGlobalBBoxTop;	 break;
	}
	return 0.0f;
}

CString	CPageSource::GetSpotColor(int nIndex)
{
	int nNum = 0;
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(m_PageSourceHeaders[i].m_strColorName);
		if (pColorDef)
			if (pColorDef->m_nColorType == CColorDefinition::Spot)
			{
				if (nNum == nIndex)
					return pColorDef->m_strColorName;
				nNum++;
			}
	}
	return "";
}

BOOL CPageSource::IsMemberOfProductGroup(int nProductPartIndex)
{
	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
	{
		CPageTemplate* pTemplate = m_PageSourceHeaders[i].GetFirstPageTemplate();
		while (pTemplate)
		{
			if (pTemplate->m_nProductPartIndex == nProductPartIndex)
				return TRUE;
			pTemplate = m_PageSourceHeaders[i].GetNextPageTemplate(pTemplate);
		}
	}
	return FALSE;
}

BOOL CPageSource::IsPdfEngineReport(const CString& strInfoFileName)
{
	CPostScriptParser parser;
	if ( ! parser.IsValidFile(strInfoFileName))
		return TRUE;
	else
		return FALSE;
}

BOOL CPageSource::IsEqualPageSourceHeaders(CArray <CPageSourceHeader, CPageSourceHeader&>& rPageSourceHeaders)
{
	if (m_PageSourceHeaders.GetSize() != rPageSourceHeaders.GetSize())
		return FALSE;

	for (int i = 0; i < m_PageSourceHeaders.GetSize(); i++)
		if (m_PageSourceHeaders[i] != rPageSourceHeaders[i])
			return FALSE;

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CPageSourceHeader

IMPLEMENT_SERIAL(CPageSourceHeader, CPageSourceHeader, VERSIONABLE_SCHEMA | 9)

CPageSourceHeader::CPageSourceHeader()
{
	m_nPageNumber	   = 0;
	m_bPageNumberFix   = FALSE;
	m_fColorIntensity  = 1.0f;
	m_rgb			   = WHITE;
	m_nColorIndex      = 0;
	m_fBBoxLeft		   = 0.0f;
	m_fBBoxBottom	   = 0.0f;
	m_fBBoxRight	   = 0.0f; 
	m_fBBoxTop		   = 0.0f;
	m_fTrimBoxLeft	   = 0.0f;
	m_fTrimBoxBottom   = 0.0f;
	m_fTrimBoxRight	   = 0.0f; 
	m_fTrimBoxTop	   = 0.0f;
	m_bOutputBlocked   = FALSE;
	m_pThumbnailImage  = NULL;
}

CPageSourceHeader::~CPageSourceHeader()
{
	if (m_pThumbnailImage)
	{
		delete m_pThumbnailImage;
		m_pThumbnailImage = NULL;
	}
}

CPageSourceHeader::CPageSourceHeader(const CPageSourceHeader& rPageSourceHeader)
{
	Copy(rPageSourceHeader);
}

const CPageSourceHeader& CPageSourceHeader::operator=(const CPageSourceHeader& rPageSourceHeader)
{
	Copy(rPageSourceHeader);

	return *this;
}

BOOL CPageSourceHeader::operator==(const CPageSourceHeader& rPageSourceHeader)
{
	if (m_strPageName		!= rPageSourceHeader.m_strPageName)
		return FALSE;
	if (m_nPageNumber		!= rPageSourceHeader.m_nPageNumber)
		return FALSE;
	if (m_bPageNumberFix	!= rPageSourceHeader.m_bPageNumberFix)
		return FALSE;
	if (m_strColorName		!= rPageSourceHeader.m_strColorName)
		return FALSE;
	if (m_fColorIntensity	!= rPageSourceHeader.m_fColorIntensity)
		return FALSE;
	if (m_rgb				!= rPageSourceHeader.m_rgb)
		return FALSE;
	if (m_fBBoxLeft			!= rPageSourceHeader.m_fBBoxLeft)
		return FALSE;
	if (m_fBBoxBottom		!= rPageSourceHeader.m_fBBoxBottom)
		return FALSE;
	if (m_fBBoxRight		!= rPageSourceHeader.m_fBBoxRight)
		return FALSE;
	if (m_fBBoxTop			!= rPageSourceHeader.m_fBBoxTop)
		return FALSE;
	if (m_fTrimBoxLeft		!= rPageSourceHeader.m_fTrimBoxLeft)
		return FALSE;
	if (m_fTrimBoxBottom	!= rPageSourceHeader.m_fTrimBoxBottom)
		return FALSE;
	if (m_fTrimBoxRight		!= rPageSourceHeader.m_fTrimBoxRight)
		return FALSE;
	if (m_fTrimBoxTop		!= rPageSourceHeader.m_fTrimBoxTop)
		return FALSE;
	if (m_bOutputBlocked	!= rPageSourceHeader.m_bOutputBlocked)
		return FALSE;

	return TRUE;
}

BOOL CPageSourceHeader::operator!=(const CPageSourceHeader& rPageSourceHeader)
{
	return !(*this == rPageSourceHeader);
}


void CPageSourceHeader::Copy(const CPageSourceHeader& rPageSourceHeader)
{
	m_strPageName		= rPageSourceHeader.m_strPageName;
	m_nPageNumber		= rPageSourceHeader.m_nPageNumber;
	m_bPageNumberFix	= rPageSourceHeader.m_bPageNumberFix;
	m_strColorName		= rPageSourceHeader.m_strColorName;
	m_fColorIntensity	= rPageSourceHeader.m_fColorIntensity;
	m_rgb				= rPageSourceHeader.m_rgb;
	m_nColorIndex		= rPageSourceHeader.m_nColorIndex;
	m_fBBoxLeft			= rPageSourceHeader.m_fBBoxLeft;
	m_fBBoxBottom		= rPageSourceHeader.m_fBBoxBottom;	
	m_fBBoxRight		= rPageSourceHeader.m_fBBoxRight;
	m_fBBoxTop			= rPageSourceHeader.m_fBBoxTop;		
	m_fTrimBoxLeft		= rPageSourceHeader.m_fTrimBoxLeft;
	m_fTrimBoxBottom	= rPageSourceHeader.m_fTrimBoxBottom;	
	m_fTrimBoxRight		= rPageSourceHeader.m_fTrimBoxRight;
	m_fTrimBoxTop		= rPageSourceHeader.m_fTrimBoxTop;		
	m_bOutputBlocked	= rPageSourceHeader.m_bOutputBlocked;
	m_pThumbnailImage   = rPageSourceHeader.m_pThumbnailImage;

	for (int i = 0; i < m_processColors.GetSize(); i++)
		m_processColors[i].Empty();
	m_processColors.RemoveAll();
	m_processColors.Append(rPageSourceHeader.m_processColors);

	for (int i = 0; i < m_spotColors.GetSize(); i++)
		m_spotColors[i].Empty();
	m_spotColors.RemoveAll();
	m_spotColors.Append(rPageSourceHeader.m_spotColors);
}


// helper function for PageSourceList elements
template <> void AFXAPI SerializeElements <CPageSourceHeader> (CArchive& ar, CPageSourceHeader* pPageSourceHeader, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CPageSourceHeader));
	
	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();
	
	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pPageSourceHeader->m_strPageName;
			ar << pPageSourceHeader->m_nPageNumber;
			ar << pPageSourceHeader->m_bPageNumberFix;
			ar << pPageSourceHeader->m_strColorName;
			ar << pPageSourceHeader->m_fColorIntensity;
			ar << pPageSourceHeader->m_rgb;
			ar << pPageSourceHeader->m_nColorIndex;
			ar << pPageSourceHeader->m_fBBoxLeft;
			ar << pPageSourceHeader->m_fBBoxBottom;
			ar << pPageSourceHeader->m_fBBoxRight;
			ar << pPageSourceHeader->m_fBBoxTop;
			ar << pPageSourceHeader->m_fTrimBoxLeft;
			ar << pPageSourceHeader->m_fTrimBoxBottom;
			ar << pPageSourceHeader->m_fTrimBoxRight;
			ar << pPageSourceHeader->m_fTrimBoxTop;
			ar << pPageSourceHeader->m_bOutputBlocked;
			pPageSourceHeader->m_processColors.Serialize(ar);
			pPageSourceHeader->m_spotColors.Serialize(ar);
		}
		else
		{
			pPageSourceHeader->m_processColors.RemoveAll();
			pPageSourceHeader->m_spotColors.RemoveAll();
			switch (nVersion)
			{
			case 1:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				pPageSourceHeader->m_bOutputBlocked = FALSE;
				pPageSourceHeader->m_fColorIntensity = 1.0f;
				break;
			case 2:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				pPageSourceHeader->m_bOutputBlocked = FALSE;
				pPageSourceHeader->m_fColorIntensity = 1.0f;
				break;
			case 3:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				pPageSourceHeader->m_bOutputBlocked = FALSE;
				pPageSourceHeader->m_fColorIntensity = 1.0f;
				break;
			case 4:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				ar >> pPageSourceHeader->m_fTrimBoxLeft;
				ar >> pPageSourceHeader->m_fTrimBoxBottom;
				ar >> pPageSourceHeader->m_fTrimBoxRight;
				ar >> pPageSourceHeader->m_fTrimBoxTop;
				pPageSourceHeader->m_bOutputBlocked = FALSE;
				pPageSourceHeader->m_fColorIntensity = 1.0f;
				break;
			case 5:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				ar >> pPageSourceHeader->m_fTrimBoxLeft;
				ar >> pPageSourceHeader->m_fTrimBoxBottom;
				ar >> pPageSourceHeader->m_fTrimBoxRight;
				ar >> pPageSourceHeader->m_fTrimBoxTop;
				pPageSourceHeader->m_bOutputBlocked = FALSE;
				pPageSourceHeader->m_fColorIntensity = 1.0f;
				BOOL bDummy;
				ar >> bDummy;	// former m_bRemapSpotColors
				pPageSourceHeader->m_spotColors.Serialize(ar);
				break;
			case 6:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				ar >> pPageSourceHeader->m_fTrimBoxLeft;
				ar >> pPageSourceHeader->m_fTrimBoxBottom;
				ar >> pPageSourceHeader->m_fTrimBoxRight;
				ar >> pPageSourceHeader->m_fTrimBoxTop;
				pPageSourceHeader->m_bOutputBlocked = FALSE;
				pPageSourceHeader->m_fColorIntensity = 1.0f;
				pPageSourceHeader->m_spotColors.Serialize(ar);
				break;
			case 7:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				ar >> pPageSourceHeader->m_fTrimBoxLeft;
				ar >> pPageSourceHeader->m_fTrimBoxBottom;
				ar >> pPageSourceHeader->m_fTrimBoxRight;
				ar >> pPageSourceHeader->m_fTrimBoxTop;
				ar >> pPageSourceHeader->m_bOutputBlocked;
				pPageSourceHeader->m_fColorIntensity = 1.0f;
				pPageSourceHeader->m_spotColors.Serialize(ar);
				break;
			case 8:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				ar >> pPageSourceHeader->m_fTrimBoxLeft;
				ar >> pPageSourceHeader->m_fTrimBoxBottom;
				ar >> pPageSourceHeader->m_fTrimBoxRight;
				ar >> pPageSourceHeader->m_fTrimBoxTop;
				ar >> pPageSourceHeader->m_bOutputBlocked;
				pPageSourceHeader->m_fColorIntensity = 1.0f;
				pPageSourceHeader->m_processColors.Serialize(ar);
				pPageSourceHeader->m_spotColors.Serialize(ar);
				break;
			case 9:
				ar >> pPageSourceHeader->m_strPageName;
				ar >> pPageSourceHeader->m_nPageNumber;
				ar >> pPageSourceHeader->m_bPageNumberFix;
				ar >> pPageSourceHeader->m_strColorName;
				ar >> pPageSourceHeader->m_fColorIntensity;
				ar >> pPageSourceHeader->m_rgb;
				ar >> pPageSourceHeader->m_nColorIndex;
				ar >> pPageSourceHeader->m_fBBoxLeft;
				ar >> pPageSourceHeader->m_fBBoxBottom;
				ar >> pPageSourceHeader->m_fBBoxRight;
				ar >> pPageSourceHeader->m_fBBoxTop;
				ar >> pPageSourceHeader->m_fTrimBoxLeft;
				ar >> pPageSourceHeader->m_fTrimBoxBottom;
				ar >> pPageSourceHeader->m_fTrimBoxRight;
				ar >> pPageSourceHeader->m_fTrimBoxTop;
				ar >> pPageSourceHeader->m_bOutputBlocked;
				pPageSourceHeader->m_processColors.Serialize(ar);
				pPageSourceHeader->m_spotColors.Serialize(ar);
				break;
			}

			if (nVersion < 8)	// old jobs: set process colors to cmyk
			{
				pPageSourceHeader->m_processColors.Add(_T("Cyan"));
				pPageSourceHeader->m_processColors.Add(_T("Magenta"));
				pPageSourceHeader->m_processColors.Add(_T("Yellow"));
				pPageSourceHeader->m_processColors.Add(_T("Black"));
			}
		}
		pPageSourceHeader++;
	}
}


void AFXAPI DestructElements(CPageSourceHeader* pPageSourceHeader, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pPageSourceHeader->m_strPageName.Empty();
		pPageSourceHeader->m_strColorName.Empty();

		for (int i = 0; i < pPageSourceHeader->m_processColors.GetSize(); i++)
			pPageSourceHeader->m_processColors[i].Empty();
		pPageSourceHeader->m_processColors.RemoveAll();

		for (int i = 0; i < pPageSourceHeader->m_spotColors.GetSize(); i++)
			pPageSourceHeader->m_spotColors[i].Empty();
		pPageSourceHeader->m_spotColors.RemoveAll();

		pPageSourceHeader++;
	}
}

// Used for registering PDF-Documents in PDF-Version
void CPageSourceHeader::Create(int nPageNum, const CString& strPageName, float fBBoxLeft, float fBBoxBottom, float fBBoxRight, float fBBoxTop, const CString& strColorName, BOOL bDefineColorPrompt)
{
	m_nPageNumber    = nPageNum;
	m_bPageNumberFix = (strPageName.IsEmpty()) ? FALSE : TRUE;
	if (strPageName.IsEmpty())
		m_strPageName.Format(_T("%d"), nPageNum);
	else
		m_strPageName = strPageName;
	m_fBBoxLeft		 = fBBoxLeft   * PICA_POINTS;
	m_fBBoxBottom	 = fBBoxBottom * PICA_POINTS;
	m_fBBoxRight	 = fBBoxRight  * PICA_POINTS;
	m_fBBoxTop		 = fBBoxTop	   * PICA_POINTS;

/////////////////////////////////////////////////////////////////////////////
// Look for the right color-name and RGB-value
	CString strUnknown;
	strUnknown.LoadString(IDS_UNKNOWN_STR);
 
	if (strColorName.IsEmpty())
	{
		m_strColorName = strUnknown;
		m_rgb		   = WHITE;
	}
	else
	{
		m_strColorName = theApp.m_colorDefTable.FindColorReferenceName(strColorName);
		if (m_strColorName.IsEmpty())
		{
			//if (bDefineColorPrompt)
			//{
			//	CString strMessage;
			//	AfxFormatString1(strMessage, IDS_NEW_COLOR_NAME_FOUND, strColorName);
			//	if (AfxMessageBox(strMessage, MB_ICONQUESTION | MB_YESNO) == IDYES)
			//	{
			//		CDlgPrintColorProps dlg;
			//		dlg.m_colorDefTable.Append(theApp.m_colorDefTable);
			//		dlg.m_strPrintColorName	 = strColorName;
			//		dlg.m_rgbColor			 = WHITE; 
			//		dlg.m_nIPUIndex			 = 0;
			//		dlg.m_nProcessCMYK		 = -1;
			//		dlg.m_nPrintColorType	 = 1;	

			//		if (dlg.DoModal() == IDCANCEL)
			//		{
			//			m_strColorName = strColorName;
			//			m_rgb		   = WHITE;
			//		}
			//		else
			//		{
			//			switch (dlg.m_nPrintColorType)
			//			{
			//			case 0:	
			//			case 1: {
			//						CColorDefinition colorDef(dlg.m_strPrintColorName, dlg.m_rgbColor, 
			//												 (dlg.m_nPrintColorType == 0) ? dlg.m_nProcessCMYK : CColorDefinition::Spot, dlg.m_nIPUIndex);
			//						theApp.m_colorDefTable.InsertColorDef(colorDef);
			//					}
			//					break;
			//			case 2: {
			//						int nBasicColorIndex = theApp.m_colorDefTable.FindColorDefinitionIndex(dlg.m_strColorMappingTarget);
			//						if (nBasicColorIndex >= 0)
			//							theApp.m_colorDefTable[nBasicColorIndex].m_AliasNameList.AddTail(dlg.m_strPrintColorName);
			//					}
			//					break;
			//			}
			//			m_strColorName = strColorName;
			//			m_rgb		   = theApp.m_colorDefTable.FindColorRef(strColorName); 
			//			theApp.m_colorDefTable.Save();
			//		}
			//	}
			//	else
			//	{
			//		m_strColorName = strColorName;
			//		m_rgb		   = WHITE;
			//	}
			//}
			//else
			{
				m_strColorName = strColorName;
				m_rgb		   = WHITE;
			}
		}
		else
		{
			m_rgb  = theApp.m_colorDefTable.FindColorRef(m_strColorName); 
		}
	}
}


// Used for assign manually pages in CTP-Version
void CPageSourceHeader::Create(int nIndex)
{
	m_strPageName.Format(_T("%d"), nIndex + 1);
	m_nPageNumber = nIndex + 1;
}


// Used for registering BMP-Files in CTP-Version
void CPageSourceHeader::Create(int nIndex, const CString& strFilePath, int nColorIndex)
{
    CFile		   file;
	CFileException fileException;

    if (!file.Open( (LPCTSTR)strFilePath, CFile::modeRead, &fileException)) // Will be closed in the destructor
	{
		TRACE	("Can't open file %s, error = %u\n",
				strFilePath, fileException.m_cause);
	}
	else
	{
		CString message;
		int nCount, nSize;
		BITMAPFILEHEADER bmfh;
	    LPBITMAPINFOHEADER m_lpBMIH; // buffer containing the BITMAPINFOHEADER
		TRY {
			nCount = file.Read((LPVOID) &bmfh, sizeof(BITMAPFILEHEADER));
			if(nCount != sizeof(BITMAPFILEHEADER))
			{
				message.Format(_T("read error 1: %s"), strFilePath);
				KIMOpenLogMessage(message, MB_ICONERROR);
				return;
			}
			if(bmfh.bfType != 0x4d42)
			{
				message.Format(_T("Invalid bitmap file: %s"), strFilePath);
				KIMOpenLogMessage(message, MB_ICONERROR);
				return;
			}
			nSize		 = bmfh.bfOffBits - sizeof(BITMAPFILEHEADER);
			m_lpBMIH	 = (LPBITMAPINFOHEADER) new char[nSize];
			//m_nBmihAlloc = m_nImageAlloc = crtAlloc;
			nCount		 = file.Read(m_lpBMIH, nSize); // info hdr & color table
			m_fBBoxLeft		= 0.0f;
			m_fBBoxBottom	= 0.0f;
			m_fBBoxRight	= (float)((m_lpBMIH->biWidth  / 72.0) * 25.4);
			m_fBBoxTop		= (float)((m_lpBMIH->biHeight / 72.0) * 25.4);

			delete m_lpBMIH;
		}
		CATCH (CException, e)
		{
			KIMOpenLogMessage(_T("Read error 1"), MB_ICONERROR);
			return;
		}
		END_CATCH
	}

	m_strPageName.Format(_T("%d"), nIndex + 1);
	m_nPageNumber = nIndex + 1;

	CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(nColorIndex);
	if (pColorDef)
	{
		m_strColorName	= pColorDef->m_strColorName;
		m_rgb			= pColorDef->m_rgb;
		m_nColorIndex	= pColorDef->m_nColorIndex;
	}
	else
	{
		m_strColorName 	= "Undefined";
		m_rgb			= WHITE;
		m_nColorIndex	= 0;
	}
}


CPageTemplate* CPageSourceHeader::GetFirstPageTemplate()
{
	if (m_PageTemplateRefs.GetSize())
		return m_PageTemplateRefs[0];
	else
		return NULL;
}

CPageTemplate* CPageSourceHeader::GetNextPageTemplate(CPageTemplate* pPageTemplate)
{
	int i;
	for (i = 0; i < m_PageTemplateRefs.GetSize(); i++)
		if (m_PageTemplateRefs[i] == pPageTemplate)
			break;

	i++;
	if (i < m_PageTemplateRefs.GetSize())
		return m_PageTemplateRefs[i];			
	else
		return NULL;
}

BOOL CPageSourceHeader::PreviewExists(CPageSource* pPageSource, int nColorSepIndex)
{
#ifdef CTP
	CString strPreviewFile;
	strPreviewFile = pPageSource->GetPreviewFileName(0, this);
	nColorSepIndex = nColorSepIndex;	// not used in CTP version
	return (_access((LPCTSTR)strPreviewFile, 0) == -1) ? FALSE : TRUE;
#else
	CString strPreviewFile;
	CPageTemplate* pTemplate = GetFirstPageTemplate();
	strPreviewFile = pPageSource->GetPreviewFileName(pTemplate->m_ColorSeparations[nColorSepIndex].m_nPageNumInSourceFile);
	return (_taccess((LPCTSTR)strPreviewFile, 0) == -1) ? FALSE : TRUE;
#endif

	return FALSE;
}

void CPageSourceHeader::GetProcessColors(CStringArray& plateNames)
{
	m_processColors.RemoveAll();
	for (int i = 0; i < plateNames.GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsProcessColor(plateNames[i]))
			m_processColors.Add(plateNames[i]);
	}
}

void CPageSourceHeader::GetSpotColors(CStringArray& plateNames)
{
	m_spotColors.RemoveAll();
	for (int i = 0; i < plateNames.GetSize(); i++)
	{
		if ( ! theApp.m_colorDefTable.IsProcessColor(plateNames[i]))
			m_spotColors.Add(plateNames[i]);
	}
}

int	CPageSourceHeader::GetColorType()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	CColorDefinition* pColorDef = NULL;
	if (pDoc)
		pColorDef = pDoc->m_ColorDefinitionTable.FindColorDefinition(m_strColorName);
	if ( ! pColorDef)
		pColorDef = theApp.m_colorDefTable.FindColorDefinition(m_strColorName);
	if ( ! pColorDef)
		return CColorDefinition::Spot;
	else
		return pColorDef->m_nColorType;
}

BOOL CPageSourceHeader::HasCyan()
{
	for (int i = 0; i < m_processColors.GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsCyan(m_processColors[i]))
			return TRUE;
	}
	return FALSE;
}

BOOL CPageSourceHeader::HasMagenta()
{
	for (int i = 0; i < m_processColors.GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsMagenta(m_processColors[i]))
			return TRUE;
	}
	return FALSE;
}

BOOL CPageSourceHeader::HasYellow()
{
	for (int i = 0; i < m_processColors.GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsYellow(m_processColors[i]))
			return TRUE;
	}
	return FALSE;
}

BOOL CPageSourceHeader::HasKey()
{
	for (int i = 0; i < m_processColors.GetSize(); i++)
	{
		if (theApp.m_colorDefTable.IsKey(m_processColors[i]))
			return TRUE;
	}
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// CColorDefinitionTable

IMPLEMENT_SERIAL(CColorDefinitionTable, CColorDefinitionTable, VERSIONABLE_SCHEMA | 1)

int gColorDefSchema = 0;

CColorDefinitionTable::CColorDefinitionTable()
{
}


short CColorDefinitionTable::InsertColorDef(CColorDefinition& rColorDefToInsert)
{
	for (short i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName == rColorDefToInsert.m_strColorName)
		{
			if ( (rColorDef.m_rgb == WHITE) && (rColorDefToInsert.m_rgb != WHITE) )
				rColorDef.m_rgb = rColorDefToInsert.m_rgb;
			if ( (rColorDef.m_cmyk == CMYK(0,0,0,0)) && (rColorDefToInsert.m_cmyk != CMYK(0,0,0,0)) )
				rColorDef.m_cmyk = rColorDefToInsert.m_cmyk;
			return i;
		}
	}

	Add(rColorDefToInsert);
	return (short)(GetSize() - 1);
}


void CColorDefinitionTable::Load()
{
	CFile		   file;
	CFileException fileException;
	TCHAR		   pszFileName[MAX_PATH];
	_tcscpy(pszFileName, theApp.GetDataFolder());
	_tcscat(pszFileName, _T("\\Colors.def"));
	if (!file.Open(pszFileName, CFile::modeRead, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return; 
	}
	
	gColorDefSchema = 0;
	CArchive archive(&file, CArchive::load);
	Serialize(archive);
	archive.Close();
	file.Close();

	if (gColorDefSchema < 4)
	{
		for (int i = 0; i < GetSize(); i++)
			if (ElementAt(i).m_strColorName.CompareNoCase(_T("COMPOSITE")) == 0)
			{
				RemoveAt(i);
				i--;
			}
	}
}

void CColorDefinitionTable::Save()
{
	CFile		   file;
	CFileException fileException;
	TCHAR		   pszFileName[MAX_PATH];
	_tcscpy(pszFileName, theApp.GetDataFolder());
	_tcscat(pszFileName, _T("\\Colors.def"));
	if (!file.Open(pszFileName, CFile::modeCreate|CFile::modeWrite, &fileException))
	{
		TRACE	("Can't open file %s, error = %u\n", pszFileName, fileException.m_cause);
		return; 
	}
	CArchive archive(&file, CArchive::store);
	Serialize(archive);
	archive.Close();
	file.Close();
}

CString	CColorDefinitionTable::GetColorName(int nColorDefinitionIndex)
{
	if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= GetSize()) )
		return "";

	return ElementAt(nColorDefinitionIndex).m_strColorName;
}

CString CColorDefinitionTable::FindColorReferenceName(const CString& strColor)
{
	if (strColor.CompareNoCase(_T("Composite")) == 0)
		return strColor;

	BOOL	bColorFound = FALSE;
	CString strColorName;

	int nSize = GetSize();
	for (int i = 0; i < nSize; i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName == strColor)
		{
			strColorName = rColorDef.m_strColorName;
			bColorFound	   = TRUE;
			break;
		}
		else
		{
			POSITION pos = rColorDef.m_AliasNameList.GetHeadPosition();
			while(pos)
			{
				CString& rAlias = rColorDef.m_AliasNameList.GetNext(pos);
				if (rAlias == strColor)
				{
					strColorName = rColorDef.m_strColorName;
					bColorFound	   = TRUE;
					break;
				}
			}
			if (bColorFound)
				break;
		}
	}
	if (bColorFound)
		return (strColorName);
	else
		return (_T(""));
}

COLORREF CColorDefinitionTable::FindColorRef(const CString& strColor)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName == strColor)
			return rColorDef.m_rgb;
		else
		{
			POSITION pos = rColorDef.m_AliasNameList.GetHeadPosition();
			while(pos)
			{
				CString& rAlias = rColorDef.m_AliasNameList.GetNext(pos);
				if (rAlias == strColor)
					return rColorDef.m_rgb;
			}
		}
	}
	return WHITE;
}

short CColorDefinitionTable::FindColorDefinitionIndex(const CString& strColor)
{
	for (short i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName == strColor)
			return i;
	}
	return -1;
}

CColorDefinition* CColorDefinitionTable::FindColorDefinition(int nColorIndex)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorIndex == nColorIndex)
			if (rColorDef.m_strColorName.CompareNoCase(_T("Composite")) == 0)		// composite should not be defined in color table
				return NULL;
			else
				return &rColorDef;
	}
	return NULL;
}

CColorDefinition* CColorDefinitionTable::FindColorDefinition(const CString& strColor)
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName == strColor)
			if (rColorDef.m_strColorName.CompareNoCase(_T("Composite")) == 0)		// composite should not be defined in color table
				return NULL;
			else
				return &rColorDef;
	}
	return NULL;
}

CString	CColorDefinitionTable::GetCyanName()
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorType == CColorDefinition::ProcessC)
			return rColorDef.m_strColorName;
	}
	return _T("Cyan");
}

CString	CColorDefinitionTable::GetMagentaName()
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorType == CColorDefinition::ProcessM)
			return rColorDef.m_strColorName;
	}
	return _T("Magenta");
}

CString	CColorDefinitionTable::GetYellowName()
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorType == CColorDefinition::ProcessY)
			return rColorDef.m_strColorName;
	}
	return _T("Yellow");
}

CString	CColorDefinitionTable::GetKeyName()
{
	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_nColorType == CColorDefinition::ProcessK)
			return rColorDef.m_strColorName;
	}
	return _T("Black");
}

void CColorDefinitionTable::UpdateColorInfos()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (!pDoc)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)
		return;

	pDoc->m_MarkTemplateList.RemoveUnusedTemplates();	// first remove eventually unused mark templates to avoid incorrect results
	for (short i = 0; i < GetSize(); i++)
	{
		if ( ! pDoc->m_PageTemplateList.ColorInUse(pDoc, i))	// remove unused colors
		{
			if ( ! pDoc->m_MarkTemplateList.ColorInUse(pDoc, i) && ! pDoc->m_PrintSheetList.m_Layouts.ColorInUseMarkList(pDoc, i) )
			{
				RemoveAt(i);				
				pDoc->m_PageTemplateList.DecrementColorDefIndices(i); 
				pDoc->m_MarkTemplateList.DecrementColorDefIndices(i);
				pDoc->m_PrintSheetList.m_Layouts.DecrementMarkListColorDefIndices(i);
				pDoc->m_PrintSheetList.DecrementColorDefIndices(i);
				i--;
			}
		}
		if ( (i >= 0) && (i < GetSize()) )
		{
			COLORREF crRGB  = WHITE;
			COLORREF crCMYK = CMYK(0,0,0,0);
			if (pDoc->m_PageSourceList.IsSpotcolorDefined(ElementAt(i).m_strColorName))
			{
				crRGB  = pDoc->m_PageSourceList.GetAlternateRGB(ElementAt(i).m_strColorName);
				crCMYK = pDoc->m_PageSourceList.GetAlternateCMYK(ElementAt(i).m_strColorName);
			}
			else
				if (pDoc->m_MarkSourceList.IsSpotcolorDefined(ElementAt(i).m_strColorName))
				{
					crRGB  = pDoc->m_MarkSourceList.GetAlternateRGB(ElementAt(i).m_strColorName);
					crCMYK = pDoc->m_MarkSourceList.GetAlternateCMYK(ElementAt(i).m_strColorName);
				}
				else
				{
					CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(ElementAt(i).m_strColorName);
					if (pColorDef)
					{
						crRGB  = pColorDef->m_rgb;
						crCMYK = pColorDef->m_cmyk;
					}
				}
			ElementAt(i).m_rgb  = crRGB;
			ElementAt(i).m_cmyk = crCMYK;
		}
			//ElementAt(i).m_rgb = theApp.m_colorDefTable.FindColorRef(ElementAt(i).m_strColorName);
	}

	POSITION pos = pDoc->m_PageSourceList.GetHeadPosition();
	while (pos)
	{
		CPageSource& rPageSource = pDoc->m_PageSourceList.GetNext(pos);
		for (int i = 0; i < rPageSource.m_PageSourceHeaders.GetSize(); i++)
		{
			CPageSourceHeader& rPageSourceHeader = rPageSource.m_PageSourceHeaders[i];
			if (rPageSourceHeader.GetFirstPageTemplate())	// check if assigned to any page
				if (rPageSourceHeader.m_strColorName.CompareNoCase(_T("Composite")) == 0)
				{
					for (int j = 0; j < rPageSourceHeader.m_spotColors.GetSize(); j++)
					{
						COLORREF crRGB  = WHITE;
						COLORREF crCMYK = CMYK(0,0,0,0);
						if (pDoc->m_PageSourceList.IsSpotcolorDefined(rPageSourceHeader.m_spotColors[j]))
						{
							crRGB  = pDoc->m_PageSourceList.GetAlternateRGB(rPageSourceHeader.m_spotColors[j]);
							crCMYK = pDoc->m_PageSourceList.GetAlternateCMYK(rPageSourceHeader.m_spotColors[j]);
						}
						else
						{
							CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(rPageSourceHeader.m_spotColors[j]);
							if (pColorDef)
							{
								crRGB  = pColorDef->m_rgb;
								crCMYK = pColorDef->m_cmyk;
							}
						}
						CColorDefinition colorDef(rPageSourceHeader.m_spotColors[j], crRGB, crCMYK, CColorDefinition::Spot);
						InsertColorDef(colorDef);
					}
				}
		}
	}

	pos = pDoc->m_boundProducts.GetHeadPosition();
	while (pos)
	{
		CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);

		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			CBoundProductPart& rProductPart = rBoundProduct.m_parts[i];
			for (int j = 0; j < rProductPart.m_colorantList.GetSize(); j++)
			{
				if (rProductPart.m_colorantList[j].m_crRGB == WHITE)
				{
					CColorDefinition* pColorDef = pDoc->m_ColorDefinitionTable.FindColorDefinition(rProductPart.m_colorantList[j].m_strName);
					if ( ! pColorDef)
						pColorDef = theApp.m_colorDefTable.FindColorDefinition(rProductPart.m_colorantList[j].m_strName);
					if (pColorDef)
						rProductPart.m_colorantList[j].m_crRGB = pColorDef->m_rgb;
				}
			}
		}
	}

	pos = pDoc->m_flatProducts.GetHeadPosition();
	while (pos)
	{
		CFlatProduct& rFlatProduct = pDoc->m_flatProducts.GetNext(pos);

		for (int j = 0; j < rFlatProduct.m_colorantList.GetSize(); j++)
		{
			if (rFlatProduct.m_colorantList[j].m_crRGB == WHITE)
			{
				CColorDefinition* pColorDef = pDoc->m_ColorDefinitionTable.FindColorDefinition(rFlatProduct.m_colorantList[j].m_strName);
				if ( ! pColorDef)
					pColorDef = theApp.m_colorDefTable.FindColorDefinition(rFlatProduct.m_colorantList[j].m_strName);
				if (pColorDef)
					rFlatProduct.m_colorantList[j].m_crRGB = pColorDef->m_rgb;
			}
		}
	}

	pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = pDoc->m_PrintSheetList.m_Layouts.GetNext(pos);
		rLayout.m_markList.m_colorDefTable.RemoveAll();
		rLayout.m_markList.m_colorDefTable.Append(pDoc->m_ColorDefinitionTable);
	}
}

int	CColorDefinitionTable::GetColorIndex(int nColorDefinitionIndex)
{
	if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= GetSize()))
		return 0;

	return ElementAt(nColorDefinitionIndex).m_nColorIndex;
}

int	CColorDefinitionTable::GetCurSpotColorNum(CString& strColor)
{
	if (strColor.CompareNoCase(_T("Composite")) == 0)
		return -1;
	CColorDefinition* pSystemColorDef = theApp.m_colorDefTable.FindColorDefinition(strColor);
	if (pSystemColorDef)
		if (pSystemColorDef->m_nColorType != CColorDefinition::Spot)
			return -1;

	BOOL bColorFound = FALSE;
	int  nCurSpotColorNum = 0;
	for (short i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);

		if (rColorDef.m_strColorName.CompareNoCase(_T("Composite")) != 0)
		{
			pSystemColorDef = theApp.m_colorDefTable.FindColorDefinition(rColorDef.m_strColorName);

			if ( ! pSystemColorDef)
				nCurSpotColorNum++;
			else
				if (pSystemColorDef->m_nColorType == CColorDefinition::Spot)
					nCurSpotColorNum++;

			if (rColorDef.m_strColorName == strColor)
			{
				bColorFound = TRUE;
				break;
			}
		}
	}
	return (bColorFound) ? nCurSpotColorNum : -1;
}

BOOL CColorDefinitionTable::IsProcessColor(CString& strColor)
{
	if (IsCyan(strColor))
		return TRUE;
	else
		if (IsMagenta(strColor))
			return TRUE;
		else
			if (IsYellow(strColor))
				return TRUE;
			else
				if (IsKey(strColor))
					return TRUE;

	for (int i = 0; i < GetSize(); i++)
	{
		CColorDefinition& rColorDef = ElementAt(i);
		if (rColorDef.m_strColorName.CompareNoCase(strColor) == 0)
			if ( (rColorDef.m_nColorType != CColorDefinition::Spot) && (rColorDef.m_nColorType != CColorDefinition::Alternate) )
				return TRUE;
	}
	return FALSE;
}

BOOL CColorDefinitionTable::IsCyan(CString& strColor)
{
	if (strColor.CompareNoCase(_T("CYAN")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(_T("ZYAN")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(GetCyanName()) == 0)
		return TRUE;
	return FALSE;
}

BOOL CColorDefinitionTable::IsMagenta(CString& strColor)
{
	if (strColor.CompareNoCase(_T("MAGENTA")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(GetMagentaName()) == 0)
		return TRUE;
	return FALSE;
}

BOOL CColorDefinitionTable::IsYellow(CString& strColor)
{
	if (strColor.CompareNoCase(_T("YELLOW")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(_T("GELB")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(GetYellowName()) == 0)
		return TRUE;
	return FALSE;
}

BOOL CColorDefinitionTable::IsKey(CString& strColor)
{
	if (strColor.CompareNoCase(_T("KEY")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(_T("BLACK")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(_T("SCHWARZ")) == 0)
		return TRUE;
	if (strColor.CompareNoCase(GetKeyName()) == 0)
		return TRUE;
	return FALSE;
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CSpotcolorDefs

IMPLEMENT_SERIAL(CSpotcolorDefs, CSpotcolorDefs, VERSIONABLE_SCHEMA | 1)

CSpotcolorDefs::CSpotcolorDefs()
{
	m_strSpotColorName.Empty();
	m_alternateValues.fC = 0.0f;
	m_alternateValues.fM = 0.0f;
	m_alternateValues.fY = 0.0f;
	m_alternateValues.fK = 0.0f;
}

CSpotcolorDefs::CSpotcolorDefs(const CSpotcolorDefs& rSpotcolorDefs)
{
	Copy(rSpotcolorDefs);
}

CSpotcolorDefs::~CSpotcolorDefs()
{
	m_strSpotColorName.Empty();
}

const CSpotcolorDefs& CSpotcolorDefs::operator=(const CSpotcolorDefs& rSpotcolorDefs)
{
	Copy(rSpotcolorDefs);

	return *this;
}


void CSpotcolorDefs::Copy(const CSpotcolorDefs& rSpotcolorDefs)
{
	m_strSpotColorName = rSpotcolorDefs.m_strSpotColorName;
	m_alternateValues  = rSpotcolorDefs.m_alternateValues;
}

// helper function for CColorDefinitionTable elements
template <> void AFXAPI SerializeElements <CSpotcolorDefs> (CArchive& ar, CSpotcolorDefs* pSpotcolorDefs, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CSpotcolorDefs));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();
		
	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pSpotcolorDefs->m_strSpotColorName;
			ar << pSpotcolorDefs->m_alternateValues.fC;
			ar << pSpotcolorDefs->m_alternateValues.fM;
			ar << pSpotcolorDefs->m_alternateValues.fY;
			ar << pSpotcolorDefs->m_alternateValues.fK;
		}
		else
		{
			switch (nVersion)
			{
			case 1:
				ar >> pSpotcolorDefs->m_strSpotColorName;
				ar >> pSpotcolorDefs->m_alternateValues.fC;
				ar >> pSpotcolorDefs->m_alternateValues.fM;
				ar >> pSpotcolorDefs->m_alternateValues.fY;
				ar >> pSpotcolorDefs->m_alternateValues.fK;
				break;
			}
		}
		pSpotcolorDefs++;
	}
}

void AFXAPI DestructElements(CSpotcolorDefs* pSpotcolorDefs, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pSpotcolorDefs->m_strSpotColorName.Empty();
		pSpotcolorDefs++;
	}
}


/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CSpotcolorDefList

void CSpotcolorDefList::AddEntry(CSpotcolorDefs& entry)
{
	if (IsSpotcolorDefined(entry.m_strSpotColorName))
	{
		COLORREF crCMYK = CMYK((int)(entry.m_alternateValues.fC*100), (int)(entry.m_alternateValues.fM*100), (int)(entry.m_alternateValues.fY*100), (int)(entry.m_alternateValues.fK*100));

		if ( (GetAlternateCMYK(entry.m_strSpotColorName) == CMYK(0,0,0,0)) && (crCMYK != 0) )
			SetEntry(entry);
	}

	Add(entry);
}

void CSpotcolorDefList::SetEntry(CSpotcolorDefs& entry)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_strSpotColorName.CompareNoCase(entry.m_strSpotColorName) == 0)
		{
			GetAt(i).m_alternateValues = entry.m_alternateValues;
			return;
		}
	}
}

void CSpotcolorDefList::InsertSpotcolorDefs(CSpotcolorDefs &spotcolorDefs)
{
	if ( ! IsSpotcolorDefined(spotcolorDefs.m_strSpotColorName))
		Add(spotcolorDefs);
}

BOOL CSpotcolorDefList::IsSpotcolorDefined(const CString& strSpotColorName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_strSpotColorName.CompareNoCase(strSpotColorName) == 0)
			return TRUE;
	}

	return FALSE;
}

COLORREF CSpotcolorDefList::GetAlternateCMYK(const CString& strSpotColorName)
{
	for (int i = 0; i < GetSize(); i++)
	{
		if (GetAt(i).m_strSpotColorName.CompareNoCase(strSpotColorName) == 0)
		{
			CSpotcolorDefs::alternateValues values = GetAt(i).m_alternateValues;
			return CMYK((int)(values.fC*100), (int)(values.fM*100), (int)(values.fY*100), (int)(values.fK*100));
		}
	}

	return CMYK(0,0,0,0);
}

COLORREF CSpotcolorDefList::GetAlternateRGB(const CString& strSpotColorName)
{
	COLORREF crCMYK = GetAlternateCMYK(strSpotColorName);
	if (crCMYK == CMYK(0,0,0,0))
		return WHITE;
	else
		return CColorDefinition::CMYK2RGB(crCMYK);
}

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// CColorDefinition

IMPLEMENT_SERIAL(CColorDefinition, CColorDefinition, VERSIONABLE_SCHEMA | 5)

CColorDefinition::CColorDefinition()
{
	m_rgb		  = 0;
	m_cmyk		  = CMYK(0,0,0,0);
	m_nColorIndex = 0;
}


const CColorDefinition& CColorDefinition::operator=(const CColorDefinition& rColorDefinition)
{
	Copy(rColorDefinition);

	return *this;
}


void CColorDefinition::Copy(const CColorDefinition& rColorDefinition)
{
	m_strColorName = rColorDefinition.m_strColorName;
	m_rgb		   = rColorDefinition.m_rgb;
	m_cmyk		   = rColorDefinition.m_cmyk;
	m_nColorType   = rColorDefinition.m_nColorType;
	m_nColorIndex  = rColorDefinition.m_nColorIndex;

	m_AliasNameList.RemoveAll();  // no append, so target list has to be empty
	unsigned nStrings = rColorDefinition.m_AliasNameList.GetCount(); 
	POSITION pos	  = rColorDefinition.m_AliasNameList.GetHeadPosition();
	CString  StringBuf;
	CString& rStringBuf = StringBuf;

	for (unsigned i = 0; i < nStrings; i++)						
	{
		rStringBuf = rColorDefinition.m_AliasNameList.GetAt(pos);
		m_AliasNameList.AddTail(rStringBuf);
		rColorDefinition.m_AliasNameList.GetNext(pos);
	}	
}


// helper function for CColorDefinitionTable elements
template <> void AFXAPI SerializeElements <CColorDefinition> (CArchive& ar, CColorDefinition* pColorDefinition, INT_PTR nCount)
{
	ar.SerializeClass(RUNTIME_CLASS(CColorDefinition));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();
		
	for (; nCount > 0; nCount--)
	{
		if (ar.IsStoring())
		{
			ar << pColorDefinition->m_strColorName;
			ar << pColorDefinition->m_rgb;
			ar << pColorDefinition->m_cmyk;
			ar << pColorDefinition->m_nColorType;
			ar << pColorDefinition->m_nColorIndex;
			pColorDefinition->m_AliasNameList.Serialize(ar);
		}
		else
		{
			pColorDefinition->m_AliasNameList.RemoveAll();
			switch (nVersion)
			{
			case 1:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				break;
			case 2:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				ar >> pColorDefinition->m_nColorIndex;
				break;
			case 3:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				ar >> pColorDefinition->m_nColorIndex;
				pColorDefinition->m_cmyk = CColorDefinition::RGB2CMYK(pColorDefinition->m_rgb, pColorDefinition);
				pColorDefinition->m_AliasNameList.Serialize(ar);
				break;
			case 4:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				ar >> pColorDefinition->m_nColorType;
				ar >> pColorDefinition->m_nColorIndex;
				pColorDefinition->m_cmyk = CColorDefinition::RGB2CMYK(pColorDefinition->m_rgb, pColorDefinition);
				pColorDefinition->m_AliasNameList.Serialize(ar);
				break;
			case 5:
				ar >> pColorDefinition->m_strColorName;
				ar >> pColorDefinition->m_rgb;
				ar >> pColorDefinition->m_cmyk;
				ar >> pColorDefinition->m_nColorType;
				ar >> pColorDefinition->m_nColorIndex;
				pColorDefinition->m_AliasNameList.Serialize(ar);
				break;
			}
			CColorDefinition::CheckProcessColorPlausibility(pColorDefinition);

			if (nVersion < 4)
				pColorDefinition->m_cmyk = CColorDefinition::RGB2CMYK(pColorDefinition->m_rgb, pColorDefinition);

			gColorDefSchema = nVersion;
		}
		pColorDefinition++;
	}
}

void CColorDefinition::CheckProcessColorPlausibility(CColorDefinition* pColorDefinition)
{
	pColorDefinition->m_nColorType = Spot;
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("BLACK")) == 0)
		pColorDefinition->m_nColorType = ProcessK;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("TIEFE")) == 0)
		pColorDefinition->m_nColorType = ProcessK;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("KEY")) == 0)
		pColorDefinition->m_nColorType = ProcessK;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("SCHWARZ")) == 0)
		pColorDefinition->m_nColorType = ProcessK;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("CYAN")) == 0)
		pColorDefinition->m_nColorType = ProcessC;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("ZYAN")) == 0)
		pColorDefinition->m_nColorType = ProcessC;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("MAGENTA")) == 0)
		pColorDefinition->m_nColorType = ProcessM;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("YELLOW")) == 0)
		pColorDefinition->m_nColorType = ProcessY;
	else
	if (pColorDefinition->m_strColorName.CompareNoCase(_T("GELB")) == 0)
		pColorDefinition->m_nColorType = ProcessY;

	switch (pColorDefinition->m_nColorType)
	{
	case CColorDefinition::ProcessC:	pColorDefinition->m_rgb = CYAN;		break;
	case CColorDefinition::ProcessM:	pColorDefinition->m_rgb = MAGENTA;	break;
	case CColorDefinition::ProcessY:	pColorDefinition->m_rgb = YELLOW;	break;
	case CColorDefinition::ProcessK:	pColorDefinition->m_rgb = BLACK;	break;
	default:																break;
	}
}

COLORREF CColorDefinition::RGB2CMYK(COLORREF crRGB, CColorDefinition* pColorDefinition)
{
	float fCyan		= 0.0f;
	float fMagenta	= 0.0f;
	float fYellow	= 0.0f;
	float fKey		= 0.0f;
	if (pColorDefinition)
	{
		switch (pColorDefinition->m_nColorType)
		{
		case ProcessC:	return CMYK(100, 0, 0, 0);	
		case ProcessM:	return CMYK(0, 100, 0, 0);
		case ProcessY:	return CMYK(0, 0, 100, 0);
		case ProcessK:	return CMYK(0, 0, 0, 100);
		default:		break;
		}
	}
	fCyan		= 1.0f - (float)GetRValue(crRGB)/255.0f;	// convert to values between 0 and 1
	fMagenta	= 1.0f - (float)GetGValue(crRGB)/255.0f;	
	fYellow		= 1.0f - (float)GetBValue(crRGB)/255.0f;	
//	fKey		= min(fCyan, min(fMagenta, fYellow));

	float fVarKey = 1.0f;

	if ( fCyan	  < fVarKey )   fVarKey = fCyan;
	if ( fMagenta < fVarKey )   fVarKey = fMagenta;
	if ( fYellow  < fVarKey )   fVarKey = fYellow;
	if ( fVarKey == 1.0f ) 
	{ //Black
	   fCyan	= 0.0f;
	   fMagenta = 0.0f;
	   fYellow	= 0.0f;
	}
	else 
	{
	   fCyan	= ( fCyan	 - fVarKey ) / ( 1.0f - fVarKey );
	   fMagenta = ( fMagenta - fVarKey ) / ( 1.0f - fVarKey );
	   fYellow	= ( fYellow  - fVarKey ) / ( 1.0f - fVarKey );
	}
	fKey = fVarKey;

	return CMYK((WORD)(fCyan * 100.0f), (WORD)(fMagenta * 100.0f), (WORD)(fYellow * 100.0f), (WORD)(fKey * 100.0f));
}

COLORREF CColorDefinition::CMYK2RGB(COLORREF crCMYK)
{
	//CMYK values = 0 � 1

	float fC = GetCValue(crCMYK) / 100.0f;
	float fM = GetMValue(crCMYK) / 100.0f;
	float fY = GetYValue(crCMYK) / 100.0f;
	float fK = GetKValue(crCMYK) / 100.0f;

	float fCyan		= fC * ( 1.0f - fK ) + fK;
	float fMagenta	= fM * ( 1.0f - fK ) + fK;
	float fYellow	= fY * ( 1.0f - fK ) + fK;

	//RGB  values = 0 � 255

	float fR = ( 1 - fCyan )	* 255;
	float fG = ( 1 - fMagenta ) * 255;
	float fB = ( 1 - fYellow )	* 255;


	return RGB(fR, fG, fB);
}

COLORREF CColorDefinition::LAB2RGB(float fL, float fA, float fB)
{
	float fRatio = 110.0f/128.0f;	// ranges of this algorithm: 0 100, -110 110, -110 110
	fA *= fRatio;					// ranges of pdf:			 0 100, -128 128, -128 128
	fB *= fRatio;					// so needs to be converted

	double lab[] = {fL, fA, fB};

	double Xn[] = { 0.950456, 1, 1.088754 };	// white point
	
	//-----------------------------------------------------------------
	double XYZ[3];
	const double T1 = 0.008856;
	const double T2 = 0.206893;

	// Y berechnen:
	double fY = pow((lab[0] + 16.0f) / 116.0f, 3);
	bool YT = fY > T1;
	fY = YT ? fY : lab[0] / 903.3;
	XYZ[1] = fY;

	// fY leicht modifizieren f�r die weiteren Berechnungen:
	fY = YT ? pow(fY, (double)(1.0f / 3.0f)) : 7.787 * fY + 16.0f / 116.0f;

	// X berechnen:
	double fX = lab[1] / 500.0f + fY;
	XYZ[0] = fX > T2 ? pow(fX, 3) : (fX - 16.0f / 116.0f) / 7.787;

	// Z berechnen:
	double fZ = fY - lab[2] / 200.0f;
	XYZ[2] = fZ > T2 ? pow(fZ, 3) : (fZ - 16.0f / 116.0f) / 7.787;

	// Auf Referenzwei� D65 normalisieren:
	for (int i = 0; i < 3; i++)
		XYZ[i] *= Xn[i];

	// XYZ -> RGB. Dabei selben Referenzwei�punkt D65 verwenden:
	double rgb[3];
	double C_rx[3][3] =
	{
		{ 3.240479, -1.537150, -0.498535 },
		{ -0.969256, 1.875992, 0.041556 },
		{ 0.055648, -0.204043, 1.057311 }
	};

	rgb[0] = C_rx[0][0] * XYZ[0] + C_rx[0][1] * XYZ[1] + C_rx[0][2] * XYZ[2];
	rgb[1] = C_rx[1][0] * XYZ[0] + C_rx[1][1] * XYZ[1] + C_rx[1][2] * XYZ[2];
	rgb[2] = C_rx[2][0] * XYZ[0] + C_rx[2][1] * XYZ[1] + C_rx[2][2] * XYZ[2];

	// RGB in [0,1] -> RGB in [0,255]. Dabei auch die Wert in dieses
	// Intervall zwingen:
	for (int i = 0; i < 3; i++)
	{
		rgb[i] *= 255;

		if (rgb[i] < 0) rgb[i] = 0;
		if (rgb[i] > 255) rgb[i] = 255;
	}

	return RGB((unsigned)rgb[0], (unsigned)rgb[1], (unsigned)rgb[2]);
}

COLORREF CColorDefinition::LAB2CMYK(float fL, float fA, float fB)
{
	return RGB2CMYK(LAB2RGB(fL, fA, fB));
}

void AFXAPI DestructElements(CColorDefinition* pColorDefinition, INT_PTR nCount)
{
	for (; nCount > 0; nCount--)
	{
		pColorDefinition->m_strColorName.Empty();
		pColorDefinition->m_AliasNameList.RemoveAll();
		pColorDefinition++;
	}
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Implementation of CPlateList

IMPLEMENT_SERIAL(CPlateList, CObject, VERSIONABLE_SCHEMA | 2)

CPlateList::CPlateList()
{
	m_nTopmostPlate				= 0;
	m_bDisplayOpen				= FALSE;
	m_defaultPlate.m_pPlateList = this;
	m_pPrintSheet				= NULL;
}


void CPlateList::Copy(const CPlateList& rPlateList)
{
	m_nTopmostPlate = rPlateList.m_nTopmostPlate;
	m_bDisplayOpen  = rPlateList.m_bDisplayOpen;
	m_nSheetSide	= rPlateList.m_nSheetSide;
	CPlate plate;
 	POSITION pos = rPlateList.GetHeadPosition();
 	while (pos)
	{
		plate = rPlateList.GetNext(pos);
 		AddTail(plate);
	}
	m_defaultPlate = rPlateList.m_defaultPlate;
}


void CPlateList::Serialize(CArchive& ar)
{
	CList <CPlate, CPlate&>::Serialize(ar);

	ar.SerializeClass(RUNTIME_CLASS(CPlateList));

	if (ar.IsStoring())
	{
		ar << m_nTopmostPlate;
		SerializeElements <> (ar, &m_defaultPlate, 1);
	}
	else																		 
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1:
			ar >> m_nTopmostPlate;
			break;
		case 2:
			ar >> m_nTopmostPlate;
			SerializeElements(ar, &m_defaultPlate, 1);
			break;
		}
	}
}

POSITION CPlateList::AddTail(CPlate& rPlate)
{
	POSITION pos = CList <CPlate, CPlate&>::AddTail(rPlate);
	CList <CPlate, CPlate&>::GetAt(pos).m_pPlateList = this;
	
	if (m_nTopmostPlate < 0)
		m_nTopmostPlate = 0;

	return pos;
}

void CPlateList::RemoveAt(POSITION pos)
{
	CList <CPlate, CPlate&>::RemoveAt(pos);
	if (m_nTopmostPlate >= GetCount())
		m_nTopmostPlate--;
}

CPlate*	CPlateList::GetTopmostPlate()
{
	CPlate* pPlate = NULL;

	if ( ! GetCount())
	{
		m_defaultPlate.UpdateExposures();
		return &m_defaultPlate;
	}

	if ((m_nTopmostPlate >= 0) && (m_nTopmostPlate < GetCount()))
	{
		POSITION pos = FindIndex(m_nTopmostPlate);
		if (pos)
			pPlate = &GetAt(pos);
	}

	if ( ! pPlate)
		if (GetCount())
			pPlate = &GetHead();

	return pPlate;
}

void CPlateList::InvalidateAllPlates()
{
	POSITION pos = GetHeadPosition();
	while (pos) 
	{
		GetNext(pos).Invalidate();
	}

	m_defaultPlate.Invalidate();
}

void CPlateList::SetGlobalMaskAllPlates(float fValue)
{
	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	POSITION pos = GetHeadPosition();
	while (pos) 
	{
		BOOL	bChange	= TRUE;
		CPlate& rPlate	= GetNext(pos);
		
		if (pView)
		{
			CDisplayItem* pPlateTabDI = pView->m_DisplayList.GetItemFromData((void*)&rPlate, (void*)rPlate.GetPrintSheet(),
				DISPLAY_ITEM_TYPEID(CPrintSheetView, PlateTab));
			if (pPlateTabDI)
				if (pPlateTabDI->m_nState == CDisplayItem::Selected)	// if plate tab is selected with ctrl key 
					bChange = FALSE;									// assume plate is disabled
		}
		if (bChange)
		{
			rPlate.m_fOverbleedOutside = fValue;
			rPlate.Invalidate();
		}
	}

	// same mod�fication on default plate
	m_defaultPlate.m_fOverbleedOutside = fValue;
	m_defaultPlate.Invalidate();
}

CString CPlateList::GetGlobalMaskValueString()
{
	if (GetCount() <= 0)
	{
		m_defaultPlate.UpdateExposures();
		return m_defaultPlate.GetGlobalMaskValueString();
	}

	POSITION pos = GetHeadPosition();
	if ( ! pos)
		return _T("");

	float fPrevValue = GetNext(pos).m_fOverbleedOutside;
	while (pos) 
	{
		CPlate& rPlate = GetNext(pos);
		if (rPlate.m_fOverbleedOutside != fPrevValue)
			return _T("...");

		fPrevValue = rPlate.m_fOverbleedOutside;
	}

	CString strMeasure;
	SetMeasure(0, fPrevValue, strMeasure);
	return strMeasure;
}

CPlate* CPlateList::GetPlateByColor(short nColorDefinitionIndex)
{
	if (GetCount())
	{
		POSITION pos = GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = GetNext(pos);
			if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
				return &rPlate;
		}
	}
	else
		return &m_defaultPlate;

	return NULL;
}

CPlate* CPlateList::GetPlateByColorName(const CString strColorName)
{
	if (GetCount())
	{
		POSITION pos = GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = GetNext(pos);
			if (rPlate.m_strPlateName == strColorName)
				return &rPlate;
		}
	}
	else
		return &m_defaultPlate;

	return NULL;
}

CPlate* CPlateList::GetPlateByIndex(int nIndex)
{
	POSITION pos = FindIndex(nIndex);
	return ((pos) ? &GetAt(pos) : NULL);
}

int CPlateList::GetPlateIndex(CPlate* pPlate)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (&GetNext(pos) == pPlate)
			return nIndex;
		nIndex++;
	}
	return -1;
}

COleDateTime CPlateList::GetLastOutputTime()
{
	COleDateTime lastOutput;
	POSITION pos = GetHeadPosition();
	if ( ! pos)
		return m_defaultPlate.m_OutputDate;
	while (pos)
	{
		CPlate& rPlate = GetNext(pos);
		lastOutput = max(lastOutput, rPlate.m_OutputDate);
	}

	return lastOutput; 
}

BOOL CPlateList::PlateExists(const CString& strPlateName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPlate& rPlate = GetNext(pos);
		if (rPlate.m_strPlateName == strPlateName)
			return TRUE;
	}
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CPlate

IMPLEMENT_SERIAL(CPlate, CObject, VERSIONABLE_SCHEMA | 4)

CPlate::CPlate()
{
	m_nColorDefinitionIndex		= -1;
	m_bInvalid					= TRUE;
	m_nAutoBleedMethod			= AutoBleedNegativ;
	m_fOverbleedOutside			= 0.0f;	 
	m_fOverbleedInside			= 0.0f;	  
	m_fOverbleedOverlap			= 0.0f;	 
	m_fOverbleedControlMarks	= 0.0f;
	m_nExposingDeviceIndex		= -1; 
//	m_OutputDate				= time_t(0); // error: time_t(0) generates 01.01.70
	m_ExposureSequence.m_pPlate = this;
}

CPrintSheet* CPlate::GetPrintSheet() 
{
	if (this)
		return m_pPlateList->GetPrintSheet(); 
	else
		return NULL;
}

int	CPlate::GetSheetSide()  
{
	return m_pPlateList->GetSheetSide();
}

CLayoutSide* CPlate::GetLayoutSide()
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return NULL;

	switch (GetSheetSide())
	{
	case FRONTSIDE:	return &(pLayout->m_FrontSide);
	case BACKSIDE:	return &(pLayout->m_BackSide);
	default:		return &(pLayout->m_FrontSide);
	}
}

void CPlate::Create(CPlate* pDefaultPlate, CExposure& rExposure, short nColorDefinitionIndex, CPrintSheet* pPrintSheet)
{
	float fDefaultBleed = 0.0f;
	if (pPrintSheet)
		fDefaultBleed = pPrintSheet->GetPrintingProfile().m_prepress.m_fBleed;
	if (fabs(fDefaultBleed) < 0.001f)
		fDefaultBleed = theApp.settings.m_fDefaultBleed;

	if ( (nColorDefinitionIndex >= 0) && (nColorDefinitionIndex < CImpManDoc::GetDoc()->m_ColorDefinitionTable.GetSize()) )
		m_strPlateName		 = CImpManDoc::GetDoc()->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName;
	else
		m_strPlateName		 = _T("");
	m_nColorDefinitionIndex	 = nColorDefinitionIndex;
//	m_OutputDate			 = time_t(0); // error: time_t(0) generates 01.01.70
	m_nAutoBleedMethod		 = AutoBleedNegativ;
	m_fOverbleedOutside		 = fDefaultBleed;
	m_fOverbleedInside		 = 0.0f;	  
	m_fOverbleedOverlap		 = 0.0f;	 
	m_fOverbleedControlMarks = 0.0f;

	if (pDefaultPlate)
	{
		m_nAutoBleedMethod		 = pDefaultPlate->m_nAutoBleedMethod;
		m_fOverbleedOutside		 = pDefaultPlate->m_fOverbleedOutside;	 
		m_fOverbleedInside		 = pDefaultPlate->m_fOverbleedInside;	  
		m_fOverbleedOverlap		 = pDefaultPlate->m_fOverbleedOverlap;	 
		m_fOverbleedControlMarks = pDefaultPlate->m_fOverbleedControlMarks;
	}

	m_nExposingDeviceIndex = -1; // TODO: later: GetActExposingDevice()

	m_ExposureSequence.AddTail(rExposure);
}


// operator overload for CPrintSheet - needed by CList functions:

const CPlate& CPlate::operator=(const CPlate& rPlate)
{
	Copy(rPlate);

	return *this;
}


void CPlate::Copy(const CPlate& rPlate)
{
	m_strPlateName			 = rPlate.m_strPlateName;
	m_nColorDefinitionIndex	 = rPlate.m_nColorDefinitionIndex;
	m_OutputDate			 = rPlate.m_OutputDate;
	m_nAutoBleedMethod		 = rPlate.m_nAutoBleedMethod;
	m_fOverbleedOutside		 = rPlate.m_fOverbleedOutside;	 
	m_fOverbleedInside		 = rPlate.m_fOverbleedInside;	  
	m_fOverbleedOverlap		 = rPlate.m_fOverbleedOverlap;	 
	m_fOverbleedControlMarks = rPlate.m_fOverbleedControlMarks;
	m_nExposingDeviceIndex	 = rPlate.m_nExposingDeviceIndex;
	m_ExposureSequence.RemoveAll();  // no append, so target list has to be empty

	int		 nExposures = rPlate.m_ExposureSequence.GetCount(); 
	POSITION pos		= rPlate.m_ExposureSequence.GetHeadPosition();
	CExposure  ExposureBuf;
	CExposure& rExposureBuf = ExposureBuf;

	for (int i = 0; i < nExposures; i++)						
	{
		rExposureBuf = rPlate.m_ExposureSequence.GetAt(pos);
		m_ExposureSequence.AddTail(rExposureBuf);
		rPlate.m_ExposureSequence.GetNext(pos);
	}	

	for (int i = 0; i < m_processColors.GetSize(); i++)
		m_processColors[i].Empty();
	m_processColors.RemoveAll();
	m_processColors.Append(rPlate.m_processColors);

	for (int i = 0; i < m_spotColors.GetSize(); i++)
		m_spotColors[i].Empty();
	m_spotColors.RemoveAll();
	m_spotColors.Append(rPlate.m_spotColors);
}

// copy constructor for CPlate - needed by CList functions:

CPlate::CPlate(const CPlate& rPlate) 
{
	m_bInvalid = TRUE;
	m_ExposureSequence.m_pPlate = this;

	Copy(rPlate);
}


// helper function for PlateList elements
template <> void AFXAPI SerializeElements (CArchive& ar, CPlate* pPlate, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CPlate));

	if (ar.IsStoring())
	{
		ar << pPlate->m_strPlateName;
		ar << pPlate->m_nColorDefinitionIndex;
		ar << pPlate->m_OutputDate;
		ar << pPlate->m_nAutoBleedMethod;
		ar << pPlate->m_fOverbleedOutside;
		ar << pPlate->m_fOverbleedInside;	
		ar << pPlate->m_fOverbleedOverlap;	   
		ar << pPlate->m_fOverbleedControlMarks;
		ar << pPlate->m_nExposingDeviceIndex;	
		pPlate->m_ExposureSequence.Serialize(ar);
		pPlate->m_processColors.Serialize(ar);
		pPlate->m_spotColors.Serialize(ar);
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		pPlate->m_ExposureSequence.RemoveAll();  // no append, so target list has to be empty 
		pPlate->m_spotColors.RemoveAll();
		switch (nVersion)
		{
		case 1:
			ar >> pPlate->m_strPlateName;
			ar >> pPlate->m_nColorDefinitionIndex;
			ar >> pPlate->m_nAutoBleedMethod;
			ar >> pPlate->m_fOverbleedOutside;
			ar >> pPlate->m_fOverbleedInside;	
			ar >> pPlate->m_fOverbleedOverlap;	   
			ar >> pPlate->m_fOverbleedControlMarks;
			ar >> pPlate->m_nExposingDeviceIndex;
			pPlate->m_ExposureSequence.Serialize(ar);
			break;
		case 2:
			ar >> pPlate->m_strPlateName;
			ar >> pPlate->m_nColorDefinitionIndex;
			ar >> pPlate->m_OutputDate;				// new in version 2
			ar >> pPlate->m_nAutoBleedMethod;
			ar >> pPlate->m_fOverbleedOutside;
			ar >> pPlate->m_fOverbleedInside;	
			ar >> pPlate->m_fOverbleedOverlap;	   
			ar >> pPlate->m_fOverbleedControlMarks;
			ar >> pPlate->m_nExposingDeviceIndex;
			pPlate->m_ExposureSequence.Serialize(ar);
			break;
		case 3:
			ar >> pPlate->m_strPlateName;
			ar >> pPlate->m_nColorDefinitionIndex;
			ar >> pPlate->m_OutputDate;				
			ar >> pPlate->m_nAutoBleedMethod;
			ar >> pPlate->m_fOverbleedOutside;
			ar >> pPlate->m_fOverbleedInside;	
			ar >> pPlate->m_fOverbleedOverlap;	   
			ar >> pPlate->m_fOverbleedControlMarks;
			ar >> pPlate->m_nExposingDeviceIndex;
			pPlate->m_ExposureSequence.Serialize(ar);
			pPlate->m_spotColors.Serialize(ar);
			break;
		case 4:
			ar >> pPlate->m_strPlateName;
			ar >> pPlate->m_nColorDefinitionIndex;
			ar >> pPlate->m_OutputDate;			
			ar >> pPlate->m_nAutoBleedMethod;
			ar >> pPlate->m_fOverbleedOutside;
			ar >> pPlate->m_fOverbleedInside;	
			ar >> pPlate->m_fOverbleedOverlap;	   
			ar >> pPlate->m_fOverbleedControlMarks;
			ar >> pPlate->m_nExposingDeviceIndex;
			pPlate->m_ExposureSequence.Serialize(ar);
			pPlate->m_processColors.Serialize(ar);
			pPlate->m_spotColors.Serialize(ar);
			break;
		}
	}
}


void AFXAPI DestructElements(CPlate* pPlate, INT_PTR nCount)
{
	ASSERT(nCount == 1); // in CList context nCount has to be 1

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pPlate->m_strPlateName.Empty();
	pPlate->m_ExposureSequence.RemoveAll();

	for (int i = 0; i < pPlate->m_processColors.GetSize(); i++)
		pPlate->m_processColors[i].Empty();
	pPlate->m_processColors.RemoveAll();

	for (int i = 0; i < pPlate->m_spotColors.GetSize(); i++)
		pPlate->m_spotColors[i].Empty();
	pPlate->m_spotColors.RemoveAll();
}

// Update plate (usually default plate) regarding exposures to be added.
// This is neccessary only after objects has been added to layout side, so we do not use ReorganizePrintSheetPlates() in that case.
// Removal of objects is handled by RemoveCorrespondingExposures() in CLayoutObjectList::RemoveAt().
void CPlate::UpdateExposures()
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return;
	int nSide = GetSheetSide();
	if ( (nSide != FRONTSIDE) && (nSide != BACKSIDE) )
		return;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;
	CLayoutSide* pLayoutSide = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;

	if ( ! m_ExposureSequence.IsInvalid())
		return;

	m_ExposureSequence.Validate();

	// look for exposures to add/remove
	int		 nLayoutObjectIndex = 0;
	POSITION pos				= pLayoutSide->m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject			= pLayoutSide->m_ObjectList.GetNext(pos);
		CExposure*	   pExposure		= m_ExposureSequence.GetExposure(nLayoutObjectIndex);
		BOOL		   bIsRightPanorama = (rObject.IsPanoramaPage(*pPrintSheet) && rObject.IsMultipagePart(*pPrintSheet, RIGHT)) ? TRUE : FALSE;
		BOOL		   bSkipExposure	= (bIsRightPanorama) ? TRUE : FALSE;
		if ( ! bSkipExposure)
		{
			CPageTemplate* pObjectTemplate  = (rObject.m_nType == CLayoutObject::ControlMark) ? rObject.GetCurrentMarkTemplate() : rObject.GetCurrentPageTemplate(pPrintSheet);
			if (pObjectTemplate)
				if (pObjectTemplate->IsFlatSingleSidedBackSide())
					bSkipExposure = TRUE;
		}

		if ( ! pExposure && ! bSkipExposure)
		{
			CExposure exposure;
			exposure.Create(NULL, nLayoutObjectIndex);	// NULL = no defaults
			if ( ! m_ExposureSequence.GetCount())
				Create(NULL, exposure, -1, pPrintSheet);
			else
				m_ExposureSequence.AddTail(exposure);
			pExposure = &m_ExposureSequence.GetTail();
		}
		else
		{
			if (bSkipExposure)	// remove exposure of right panorama page (this is by definition) or backside of flat product which is only single sided
			{
				POSITION seqPos = m_ExposureSequence.GetHeadPosition();
				while (seqPos)
				{
					CExposure& rExposure = m_ExposureSequence.GetAt(seqPos);
					if (pExposure == &rExposure)
					{
						m_ExposureSequence.RemoveAt(seqPos);
						break;
					}
					m_ExposureSequence.GetNext(seqPos);
				}
			}
		}

		nLayoutObjectIndex++;
	}
}

CExposure* CPlate::GetDefaultExposure(int nLayoutObjectIndex)
{
	if (m_pPlateList)
	{
		m_pPlateList->m_defaultPlate.UpdateExposures();
		return m_pPlateList->m_defaultPlate.m_ExposureSequence.GetExposure(nLayoutObjectIndex);
	}
	else
		return NULL;
}

void CPlate::CalcAutoBleeds()
{
	POSITION pos = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);

		if (!rExposure.IsBleedLocked(TOP))	  rExposure.CalcAutoBleed(TOP,	  m_fOverbleedOutside);
		if (!rExposure.IsBleedLocked(BOTTOM)) rExposure.CalcAutoBleed(BOTTOM, m_fOverbleedOutside);
		if (!rExposure.IsBleedLocked(LEFT))	  rExposure.CalcAutoBleed(LEFT,	  m_fOverbleedOutside);
		if (!rExposure.IsBleedLocked(RIGHT))  rExposure.CalcAutoBleed(RIGHT,  m_fOverbleedOutside);
	}
}


void CPlate::SetBleedValue(int nSide, float fValue, CLayoutObject* pLayoutObject, BOOL bPanoramaPageClicked, CPrintSheetView* pView)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return;

	BOOL	 bIsPanoramaPage = pLayoutObject->IsPanoramaPage(*pPrintSheet);
	POSITION pos			 = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		
		if (rExposure.GetLayoutObject() == pLayoutObject)
		{
			if (bPanoramaPageClicked)
			{
				if (bIsPanoramaPage)
					rExposure.SetBleedValue(nSide, fValue, TRUE, pView);
			}
			else
			{
				if ( ! bIsPanoramaPage)
					rExposure.SetBleedValue(nSide, fValue, TRUE, pView);
			}
			//rExposure.SetBleedValuePanorama(nSide, fValue, TRUE, pView);
			break;
		}
	}

	// same modification on default plate
	if ( ! m_strPlateName.IsEmpty())	// if not already default
	{
		pos = m_pPlateList->m_defaultPlate.m_ExposureSequence.GetHeadPosition();
		while (pos)
		{
			CExposure& rExposure = m_pPlateList->m_defaultPlate.m_ExposureSequence.GetNext(pos);
			
			if (rExposure.GetLayoutObject() == pLayoutObject)
			{
				if (bPanoramaPageClicked)
				{
					if (bIsPanoramaPage)
						rExposure.SetBleedValue(nSide, fValue, TRUE, pView);
				}
				else
				{
					if ( ! bIsPanoramaPage)
						rExposure.SetBleedValue(nSide, fValue, TRUE, pView);
				}
				//rExposure.SetBleedValuePanorama(nSide, fValue, TRUE, pView);
				break;
			}
		}
	}
}

void CPlate::UnlockLocalMask(CLayoutObject* pLayoutObject, BOOL bPanoramaPageClicked, CPrintSheetView* pView)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return;

	BOOL	 bIsPanoramaPage = pLayoutObject->IsPanoramaPage(*pPrintSheet);
	BOOL	 bUnlocked		 = FALSE;
	POSITION pos			 = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		
		if (rExposure.GetLayoutObject() == pLayoutObject)
		{
			if (bPanoramaPageClicked)
			{
				if (bIsPanoramaPage)
				{
					rExposure.UnlockBleed(LEFT,   pView);
					rExposure.UnlockBleed(RIGHT,  pView);
					rExposure.UnlockBleed(TOP,	  pView);
					rExposure.UnlockBleed(BOTTOM, pView);
					bUnlocked = TRUE;
				}
			}
			else
			{
				if ( ! bIsPanoramaPage)
				{
					rExposure.UnlockBleed(LEFT,   pView);
					rExposure.UnlockBleed(RIGHT,  pView);
					rExposure.UnlockBleed(TOP,	  pView);
					rExposure.UnlockBleed(BOTTOM, pView);
					bUnlocked = TRUE;
					CPageTemplate* pTemplate = pLayoutObject->GetCurrentObjectTemplate(pPrintSheet);
					pTemplate->m_bAdjustContentToMask = FALSE;
					pTemplate->SetContentOffsetX(0.0f);
					pTemplate->SetContentOffsetY(0.0f);
					pTemplate->m_fContentScaleX = 1.0f;
					pTemplate->m_fContentScaleY = 1.0f;
				}
			}
			break;
		}
	}
	if (bUnlocked)
		Invalidate();
}

void CPlate::AdjustContent2LocalMask(CLayoutObject* pLayoutObject, BOOL bPanoramaPageClicked, CPrintSheetView* pView)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet || ! pLayoutObject)
		return;

	BOOL	 bIsPanoramaPage = pLayoutObject->IsPanoramaPage(*pPrintSheet);
	BOOL	 bAdjusted		 = FALSE;
	POSITION pos			 = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		
		if (rExposure.GetLayoutObject() == pLayoutObject)
		{
			CPageTemplate* pTemplate = pLayoutObject->GetCurrentObjectTemplate(pPrintSheet);
			if (bPanoramaPageClicked)
			{
				if (bIsPanoramaPage)
				{
					bAdjusted = TRUE;
				}
			}
			else
			{
				if ( ! bIsPanoramaPage)
				{
					float fLeft   = ((rExposure.IsBleedLocked(LEFT))   ? rExposure.GetBleedValue(LEFT)   : 0.0f);
					float fRight  = ((rExposure.IsBleedLocked(RIGHT))  ? rExposure.GetBleedValue(RIGHT)  : 0.0f);
					float fBottom = ((rExposure.IsBleedLocked(BOTTOM)) ? rExposure.GetBleedValue(BOTTOM) : 0.0f);
					float fTop	  = ((rExposure.IsBleedLocked(TOP))    ? rExposure.GetBleedValue(TOP)    : 0.0f);
					float fDiffX  = fLeft   + fRight;
					float fDiffY  = fBottom + fTop;
					switch (pLayoutObject->m_nHeadPosition)
					{
					case TOP:		
					case BOTTOM:	pTemplate->m_fContentScaleX  = 1.0f + fDiffX / pLayoutObject->GetWidth();
									pTemplate->m_fContentScaleY  = 1.0f + fDiffY / pLayoutObject->GetHeight();
									break;
					case LEFT:		
					case RIGHT:		pTemplate->m_fContentScaleX  = 1.0f + fDiffY / pLayoutObject->GetHeight();
									pTemplate->m_fContentScaleY  = 1.0f + fDiffX / pLayoutObject->GetWidth();
									break;
					}
					float fXOffset = 0.0f;
					float fYOffset = 0.0f;
					switch (pLayoutObject->m_nHeadPosition)
					{
					case TOP:		fXOffset = (pLayoutObject->m_nIDPosition == LEFT) ? -(fDiffX/2.0f - fRight)  : (fDiffX/2.0f - fLeft);		break;
					case BOTTOM:	fXOffset = (pLayoutObject->m_nIDPosition == LEFT) ? -(fDiffX/2.0f - fLeft)   : (fDiffX/2.0f - fRight);		break;
					case LEFT:		fXOffset = (pLayoutObject->m_nIDPosition == LEFT) ? -(fDiffY/2.0f - fTop)    : (fDiffY/2.0f - fBottom);		break;
					case RIGHT:		fXOffset = (pLayoutObject->m_nIDPosition == LEFT) ? -(fDiffY/2.0f - fBottom) : (fDiffY/2.0f - fTop);		break;
					}
					pTemplate->SetContentOffsetX(fXOffset);
					pTemplate->SetContentOffsetY(fYOffset);

					pTemplate->m_bAdjustContentToMask = TRUE;
					bAdjusted = TRUE;
				}
			}
			break;
		}
	}
	if (bAdjusted)
		Invalidate();
}

CString CPlate::GetGlobalMaskValueString()
{
	CString strMeasure;
	SetMeasure(0, m_fOverbleedOutside, strMeasure);
	return strMeasure;
}

void CPlate::SetTopmostPlate()
{
	int		 nPlateIndex = 0;
	POSITION pos		 = m_pPlateList->GetHeadPosition();
	while (pos)
	{
		CPlate& rPlate = m_pPlateList->GetNext(pos);
		if (&rPlate == this)
			break;
		nPlateIndex++;
	}
	m_pPlateList->m_nTopmostPlate = nPlateIndex;
}


void CPlate::RemoveExposure(int nLayoutObjectIndex)
{
	POSITION pos = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		POSITION   curPos	 = pos;
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		if (rExposure.m_nLayoutObjectIndex == nLayoutObjectIndex)
			m_ExposureSequence.RemoveAt(curPos);
		else
			if (rExposure.m_nLayoutObjectIndex > nLayoutObjectIndex)
				rExposure.m_nLayoutObjectIndex--;
	}
}

void CPlate::RotateLocalMaskAllExposures(int nComponentRefIndex, BOOL bClockwise)
{
	POSITION pos = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = m_ExposureSequence.GetNext(pos);
		CLayoutObject* pLayoutObject = rExposure.GetLayoutObject();
		if ( ! pLayoutObject)
			continue;
		if (nComponentRefIndex >= 0)
			if (pLayoutObject->m_nComponentRefIndex != nComponentRefIndex)
				continue;

		float fTopValue		= rExposure.GetBleedValue(TOP);
		BOOL  bTopLocked	= rExposure.IsBleedLocked(TOP);
		float fRightValue   = rExposure.GetBleedValue(RIGHT);
		BOOL  bRightLocked  = rExposure.IsBleedLocked(RIGHT);
		float fBottomValue  = rExposure.GetBleedValue(BOTTOM);
		BOOL  bBottomLocked = rExposure.IsBleedLocked(BOTTOM);
		float fLeftValue    = rExposure.GetBleedValue(LEFT);
		BOOL  bLeftLocked   = rExposure.IsBleedLocked(LEFT);
		if (bClockwise)
		{
			if (bLeftLocked)
				rExposure.SetBleedValue(TOP,	fLeftValue,		TRUE);
			else	
				rExposure.UnlockBleed(TOP);

			if (bTopLocked) 
				rExposure.SetBleedValue(RIGHT,	fTopValue,		TRUE);
			else	
				rExposure.UnlockBleed(RIGHT);

			if (bRightLocked)
				rExposure.SetBleedValue(BOTTOM, fRightValue,	TRUE);
			else	
				rExposure.UnlockBleed(BOTTOM);

			if (bBottomLocked)
				rExposure.SetBleedValue(LEFT,	fBottomValue,	TRUE);
			else	
				rExposure.UnlockBleed(LEFT);
		}
		else
		{
			if (bRightLocked) 
				rExposure.SetBleedValue(TOP,	fRightValue,	TRUE);
			else	
				rExposure.UnlockBleed(TOP);

			if (bTopLocked)
				rExposure.SetBleedValue(LEFT,	fTopValue,		TRUE);
			else
				rExposure.UnlockBleed(LEFT);

			if (bLeftLocked)
				rExposure.SetBleedValue(BOTTOM, fLeftValue,		TRUE);
			else	
				rExposure.UnlockBleed(BOTTOM);

			if (bBottomLocked)
				rExposure.SetBleedValue(RIGHT,	fBottomValue,	TRUE);
			else
				rExposure.UnlockBleed(RIGHT);
		}
	}

	Invalidate();
}

void CPlate::TurnLocalMaskAllExposures(int nComponentRefIndex)
{
	POSITION pos = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure&	   rExposure	 = m_ExposureSequence.GetNext(pos);
		CLayoutObject* pLayoutObject = rExposure.GetLayoutObject();
		if ( ! pLayoutObject)
			continue;
		if (nComponentRefIndex >= 0)
			if (pLayoutObject->m_nComponentRefIndex != nComponentRefIndex)
				continue;
		
		float fLeftValue   = rExposure.GetBleedValue(LEFT);
		BOOL  bLeftLocked  = rExposure.IsBleedLocked(LEFT);
		float fRightValue  = rExposure.GetBleedValue(RIGHT);
		BOOL  bRightLocked = rExposure.IsBleedLocked(RIGHT);
		if (bRightLocked)
			rExposure.SetBleedValue(LEFT, fRightValue, TRUE);
		else
			rExposure.UnlockBleed(LEFT);

		if (bLeftLocked)
			rExposure.SetBleedValue(RIGHT, fLeftValue, TRUE);
		else
			rExposure.UnlockBleed(RIGHT);
	}
}

void CPlate::TumbleLocalMaskAllExposures(int nComponentRefIndex)
{
	POSITION pos = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure&	   rExposure	 = m_ExposureSequence.GetNext(pos);
		CLayoutObject* pLayoutObject = rExposure.GetLayoutObject();
		if ( ! pLayoutObject)
			continue;
		if (nComponentRefIndex >= 0)
			if (pLayoutObject->m_nComponentRefIndex != nComponentRefIndex)
				continue;
		
		float fTopValue		= rExposure.GetBleedValue(TOP);
		BOOL  bTopLocked	= rExposure.IsBleedLocked(TOP);
		float fBottomValue  = rExposure.GetBleedValue(BOTTOM);
		BOOL  bBottomLocked = rExposure.IsBleedLocked(BOTTOM);
		if (bBottomLocked)
			rExposure.SetBleedValue(TOP, fBottomValue, TRUE);
		else
			rExposure.UnlockBleed(TOP);

		if (bTopLocked)
			rExposure.SetBleedValue(BOTTOM, fTopValue, TRUE);
		else
			rExposure.UnlockBleed(BOTTOM);
	}
}


float CPlate::GetObjectTrimBox(int nSide, CLayoutObject& rLayoutObject)
{
	float		 fLeft = 0.0f, fRight = 0.0f, fBottom = 0.0f, fTop = 0.0f;
	CPrintSheet* pPrintSheet = GetPrintSheet();

	if (rLayoutObject.IsPanoramaPage(*pPrintSheet))	// panorama page
	{
		if (rLayoutObject.IsMultipagePart(*pPrintSheet, RIGHT))	// only one half needs to be drawn - per definition we take the left one
			return 0.0f;

		CPageTemplate* pObjectTemplate = (rLayoutObject.m_nType == CLayoutObject::ControlMark) ?
										  rLayoutObject.GetCurrentMarkTemplate() :
										  rLayoutObject.GetCurrentPageTemplate(GetPrintSheet());

		CLayoutObject* pBuddy = rLayoutObject.GetCurrentMultipagePartner(pObjectTemplate, TRUE);

		if ((nSide == LEFT) || (nSide == RIGHT))
		{
			fLeft	= min(rLayoutObject.GetLeft(),   pBuddy->GetLeft());
			fRight	= max(rLayoutObject.GetRight(),  pBuddy->GetRight());
		}
		else
		{
			fBottom	= min(rLayoutObject.GetBottom(), pBuddy->GetBottom());
			fTop	= max(rLayoutObject.GetTop(),    pBuddy->GetTop());
		}
	}
	else
	{
		if ((nSide == LEFT) || (nSide == RIGHT))
		{
			fLeft	= rLayoutObject.GetLeft();
			fRight	= rLayoutObject.GetRight();
		}
		else
		{
			fBottom = rLayoutObject.GetBottom();
			fTop	= rLayoutObject.GetTop();
		}
	}

	switch (nSide)
	{
	case LEFT:		return fLeft   + rLayoutObject.GetLayoutSide()->GetFanoutOffsetX(m_strPlateName, fLeft + (fRight - fLeft) / 2.0f);

	case RIGHT:		return fRight  + rLayoutObject.GetLayoutSide()->GetFanoutOffsetX(m_strPlateName, fLeft + (fRight - fLeft) / 2.0f);

	case BOTTOM:	return fBottom + rLayoutObject.GetLayoutSide()->GetFanoutOffsetY(m_strPlateName, fBottom + (fTop - fBottom) / 2.0f);

	case TOP:		return fTop	   + rLayoutObject.GetLayoutSide()->GetFanoutOffsetY(m_strPlateName, fBottom + (fTop - fBottom) / 2.0f);

	default:		return 0.0f;
	}
}

int CPlate::GetNumOutputObjects(int nObjectType)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	int			 nNumPages	 = 0;
	POSITION	 pos		 = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure&	   rExposure = m_ExposureSequence.GetNext(pos);
		CLayoutObject* pObject	 = rExposure.GetLayoutObject(pPrintSheet->m_nLayoutIndex, GetSheetSide());
		if (!pObject)
			continue;

		if (nObjectType != -1)
			if (nObjectType != pObject->m_nType)
				continue;

		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPrintSheet);
		if ( ! pObjectTemplate)
			continue;

		int nColorDefinitionIndex = m_nColorDefinitionIndex;
		if (nColorDefinitionIndex == -1)	// if default plate 
			if (pObject->m_nType == CLayoutObject::ControlMark)		// if control mark, take 1st sep of mark
				nColorDefinitionIndex = (pObjectTemplate->m_ColorSeparations.GetSize()) ? pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1;
			else
			{
				nNumPages++;
				continue;
			}

		CPageSource* pPageSource = pObjectTemplate->GetObjectSource(nColorDefinitionIndex, TRUE, pPrintSheet, GetSheetSide());
		if (pPageSource)
			if (pPageSource->m_nType != CPageSource::SystemCreated)
				nNumPages++;
	}		
	return nNumPages;
}

// if not already in list, insert indicated process colors
void CPlate::InsertProcessColors(int nColor1, int nColor2, int nColor3, int nColor4)
{
	int processColors[4] = {nColor1, nColor2, nColor3, nColor4};
	for (int i = 0; i < 4; i++)
	{
		if (processColors[i] < 0)
			continue;
		CString strProcessColor;
		switch (processColors[i])
		{
		case CColorDefinition::ProcessC:	strProcessColor = theApp.m_colorDefTable.GetCyanName();		break;
		case CColorDefinition::ProcessM:	strProcessColor = theApp.m_colorDefTable.GetMagentaName();	break;
		case CColorDefinition::ProcessY:	strProcessColor = theApp.m_colorDefTable.GetYellowName();	break;
		case CColorDefinition::ProcessK:	strProcessColor = theApp.m_colorDefTable.GetKeyName();		break;
		}

		BOOL bOK = FALSE;
		for (int j = 0; j < m_processColors.GetSize(); j++)
		{
			if (strProcessColor == m_processColors[j])	// color already in list
			{
				bOK = TRUE;
				break;
			}
		}
		if ( ! bOK)
			m_processColors.Add(strProcessColor);
	}
}

// if not already in list, insert spot colors in alphabetical order
void CPlate::InsertSpotColors(CStringArray& rSpotColors)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	for (int i = 0; i < rSpotColors.GetSize(); i++)
	{
		CString strSpotColor = theApp.MapPrintColor(rSpotColors[i]);
		if ( ! m_spotColors.GetSize())
			m_spotColors.Add(strSpotColor);
		else
		{
			BOOL bOK = FALSE;
			for (int j = 0; j < m_spotColors.GetSize(); j++)
			{
				if (m_spotColors[j] > strSpotColor)
				{
					m_spotColors.InsertAt(j, strSpotColor);
					bOK = TRUE;
					break;
				}
				else
					if (strSpotColor == m_spotColors[j])	// color already in list
					{
						bOK = TRUE;
						break;
					}
			}
			if ( ! bOK)
				m_spotColors.Add(strSpotColor);
		}
		if (pDoc)
		{
			COLORREF crRGB  = WHITE;
			COLORREF crCMYK = CMYK(0,0,0,0);
			if (strSpotColor != rSpotColors[i])		// mapped
			{
				CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(strSpotColor);
				if (pColorDef)
				{
					crRGB  = pColorDef->m_rgb;
					crCMYK = pColorDef->m_cmyk;
				}
				CColorDefinition colorDef(strSpotColor, crRGB, crCMYK, CColorDefinition::Spot);
				pDoc->m_ColorDefinitionTable.InsertColorDef(colorDef);
			}
			else
			{
				if (pDoc->m_PageSourceList.IsSpotcolorDefined(rSpotColors[i]))
				{
					crRGB  = pDoc->m_PageSourceList.GetAlternateRGB(rSpotColors[i]);
					crCMYK = pDoc->m_PageSourceList.GetAlternateCMYK(rSpotColors[i]);
				}
				else
				{
					CColorDefinition* pColorDef = theApp.m_colorDefTable.FindColorDefinition(rSpotColors[i]);
					if (pColorDef)
					{
						crRGB  = pColorDef->m_rgb;
						crCMYK = pColorDef->m_cmyk;
					}
				}
				CColorDefinition colorDef(rSpotColors[i], crRGB, crCMYK, CColorDefinition::Spot);
				pDoc->m_ColorDefinitionTable.InsertColorDef(colorDef);
			}
		}
	}
}

void CPlate::CheckSourceLinks(CArray <CString, CString>& missingLinks, BOOL bJDFMarksOutput)
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return;

	TCHAR		szDest[1024];
	CString		strDoc;
	CFileStatus fileStatus;
	int			nActLayoutSide = GetSheetSide();
	POSITION	pos			   = m_ExposureSequence.GetHeadPosition();
	while (pos)
	{
		CExposure&	   rExposure = m_ExposureSequence.GetNext(pos);
		CLayoutObject* pObject	 = rExposure.GetLayoutObject(pPrintSheet->m_nLayoutIndex, nActLayoutSide);
		if (!pObject)
			continue;
		if (bJDFMarksOutput)
			if (pObject->m_nType != CLayoutObject::ControlMark)		// JDF -> only check marks
				continue;

		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPrintSheet);
		if (pObjectTemplate)
		{
			int nColorDefinitionIndex = m_nColorDefinitionIndex;
			if (bJDFMarksOutput)
				if (nColorDefinitionIndex == -1)	// if no plates, output 1st sep of marks
					nColorDefinitionIndex = (pObjectTemplate->m_ColorSeparations.GetSize()) ? pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1;

			CPageSource* pPageSource = pObjectTemplate->GetObjectSource(m_nColorDefinitionIndex, TRUE, pPrintSheet, GetSheetSide());

			if (pPageSource)
			{
				if (pPageSource->m_nType != CPageSource::PDFFile)
					continue;

				strDoc = pPageSource->m_strDocumentFullpath;
				CPrintSheetView::ResolvePlaceholdersExt(szDest, (LPCTSTR)strDoc, pPrintSheet, NULL, -1);
				strDoc = szDest;
				if ( ! CFile::GetStatus(strDoc, fileStatus))	// file does not exist
				{
					BOOL bAlreadyInList = FALSE;
					for (int i = 0; i < missingLinks.GetSize(); i++)
					{
						if (missingLinks[i] == strDoc)
						{
							bAlreadyInList = TRUE;
							break;
						}
					}
					if ( ! bAlreadyInList)
						missingLinks.Add(strDoc);
				}
			}
		}
	}
} 

void CPlate::AddSeparation(CString strSep)
{
	BOOL bFound = FALSE;

	if (theApp.m_colorDefTable.IsProcessColor(strSep))
	{
		for (int i = 0; i < m_processColors.GetSize(); i++)
			if (m_processColors[i].CompareNoCase(strSep) == 0)
				bFound = TRUE;

		if ( ! bFound)
			m_processColors.Add(strSep);
	}
	else
	{
		for (int i = 0; i < m_spotColors.GetSize(); i++)
			if (m_spotColors[i].CompareNoCase(strSep) == 0)
				bFound = TRUE;

		if ( ! bFound)
			m_spotColors.Add(strSep);
	}
}

BOOL CPlate::SeparationExists(CString strSep)
{
	for (int i = 0; i < m_processColors.GetSize(); i++)
		if (m_processColors[i].CompareNoCase(strSep) == 0)
			return TRUE;

	for (int i = 0; i < m_spotColors.GetSize(); i++)
		if (m_spotColors[i].CompareNoCase(strSep) == 0)
			return TRUE;
	
	return FALSE;
}

void CPlate::ReorganizeColorInfos()
{
	CPrintSheet* pPrintSheet = GetPrintSheet();
	if ( ! pPrintSheet)
		return;

	m_processColors.RemoveAll();
	m_spotColors.RemoveAll();

	CImpManDoc* pDoc		   = CImpManDoc::GetDoc();
	BOOL		bIsUnpopulated = (pDoc) ? pDoc->m_PageTemplateList.IsUnpopulated() : FALSE;
	if (bIsUnpopulated)		
	{
		CLayoutSide* pLayoutSide = GetLayoutSide();
		if ( ! pLayoutSide)
			return;

		POSITION pos = pLayoutSide->m_ObjectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = pLayoutSide->m_ObjectList.GetNext(pos);
			CPageTemplate* pObjectTemplate = (rObject.m_nType == CLayoutObject::ControlMark) ?
											  rObject.GetCurrentMarkTemplate() :
											  rObject.GetCurrentPageTemplate(pPrintSheet);
			if ( ! pObjectTemplate)
				continue;
			if (rObject.m_nType == CLayoutObject::Page)
			{
				CColorantList colorantList;
				if (pObjectTemplate->IsFlatProduct())
					colorantList = pObjectTemplate->GetFlatProduct().m_colorantList;
				else
					colorantList = pObjectTemplate->GetProductPart().m_colorantList;

				BOOL bTakeFromProduct = (colorantList.GetSize()) ? TRUE : FALSE;
				int nCyan	 = (bTakeFromProduct) ? (colorantList.HasCyan()	   ? CColorDefinition::ProcessC : -1) : CColorDefinition::ProcessC;
				int nMagenta = (bTakeFromProduct) ? (colorantList.HasMagenta() ? CColorDefinition::ProcessM : -1) : CColorDefinition::ProcessM;
				int nYellow  = (bTakeFromProduct) ? (colorantList.HasYellow()  ? CColorDefinition::ProcessY : -1) : CColorDefinition::ProcessY;
				int nKey	 = (bTakeFromProduct) ? (colorantList.HasKey()	   ? CColorDefinition::ProcessK : -1) : CColorDefinition::ProcessK;
				InsertProcessColors(nCyan, nMagenta, nYellow, nKey);
				if (bTakeFromProduct)
				{
					CStringArray spotColors;
					colorantList.GetSpotColors(spotColors);
					InsertSpotColors(spotColors);
				}
			}
			else
			{
			}
		}
		m_strPlateName = _T("Composite");	
	}


	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout)
		return;

	int		 nSide	 = GetSheetSide();
	POSITION expPos  = m_ExposureSequence.GetHeadPosition();
	while (expPos)
	{
		CExposure& rExposure   = m_ExposureSequence.GetNext(expPos);
		//CLayoutObject* pObject = rExposure.GetLayoutObject(pPrintSheet->m_nLayoutIndex, nSide);
		CLayoutObject* pObject = rExposure.GetLayoutObject(pLayout, nSide);
		if (!pObject)
			continue;
		CPageTemplate* pObjectTemplate = (pObject->m_nType == CLayoutObject::ControlMark) ?
										  pObject->GetCurrentMarkTemplate() :
										  pObject->GetCurrentPageTemplate(pPrintSheet);
		if ( ! pObjectTemplate)
			continue;

		int nColorDefinitionIndex = m_nColorDefinitionIndex;
		if (bIsUnpopulated)
			if ( (m_nColorDefinitionIndex == -1) && (pObject->m_nType == CLayoutObject::ControlMark) )	// if unpopulated, take 1st sep of marks
				nColorDefinitionIndex = (pObjectTemplate->m_ColorSeparations.GetSize()) ? pObjectTemplate->m_ColorSeparations[0].m_nColorDefinitionIndex : -1;
		
		if ( (pObject->m_nType == CLayoutObject::ControlMark) && pObject->IsAutoMarkDisabled(pPrintSheet, nSide))
			;
		else
			if ( (pObject->m_nType == CLayoutObject::ControlMark) && pObjectTemplate->IsSystemCreated())
			{
				if ( ! pObjectTemplate->m_bMap2SpotColors)
				{
					CPageSource* pPageSource = pObjectTemplate->GetObjectSource(nColorDefinitionIndex);
					if (pPageSource)
					{
						CStringArray spotColors;
						for (int i = 0; i < pPageSource->m_PageSourceHeaders.GetSize(); i++)
						{
							if (pPageSource->m_PageSourceHeaders[i].m_strColorName.CompareNoCase(_T("Composite")) != 0)
								if ( ! theApp.m_colorDefTable.IsProcessColor(pPageSource->m_PageSourceHeaders[i].m_strColorName) )
									spotColors.Add(pPageSource->m_PageSourceHeaders[i].m_strColorName);
						}
						InsertSpotColors(spotColors);
					}
				}
			}
			else
			{
				CPageSourceHeader* pPageSourceHeader = pObjectTemplate->GetObjectSourceHeader(nColorDefinitionIndex);
				if (pPageSourceHeader)
				{
					if (pPageSourceHeader->m_strColorName.CompareNoCase(_T("Composite")) == 0)
					{
						int nCyan	 = (pPageSourceHeader->HasCyan())	 ? CColorDefinition::ProcessC : -1;
						int nMagenta = (pPageSourceHeader->HasMagenta()) ? CColorDefinition::ProcessM : -1;
						int nYellow  = (pPageSourceHeader->HasYellow())  ? CColorDefinition::ProcessY : -1;
						int nKey	 = (pPageSourceHeader->HasKey())	 ? CColorDefinition::ProcessK : -1;
						InsertProcessColors(nCyan, nMagenta, nYellow, nKey);
					}
					if ( (pObject->m_nType == CLayoutObject::Page) || ( (pObject->m_nType == CLayoutObject::ControlMark) && ! pObjectTemplate->m_bMap2SpotColors) )
						InsertSpotColors(pPageSourceHeader->m_spotColors);
				}
			}
	}
}

int CPlate::GetNumColors(BOOL bSpotsOnly)
{
	if (m_strPlateName == _T("Composite"))
		if (bSpotsOnly)
			return m_spotColors.GetSize();
		else
			return m_processColors.GetSize() + m_spotColors.GetSize();
	
	return 0;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Implementation of CExposureSequence

IMPLEMENT_SERIAL(CExposureSequence, CObject, VERSIONABLE_SCHEMA | 1)

CExposureSequence::CExposureSequence()
{
	m_bInvalid = TRUE;
}

POSITION CExposureSequence::AddTail(CExposure& rExposure)
{
	GetPlate()->Invalidate();
	POSITION pos = CList <CExposure, CExposure&>::AddTail(rExposure);
	CList <CExposure, CExposure&>::GetAt(pos).m_pSequence = this;
	return pos;
}


void CExposureSequence::RemoveAll()
{
	CList <CExposure, CExposure&>::RemoveAll();
	GetPlate()->Invalidate();
}


void CExposureSequence::RemoveAt(POSITION pos)
{
	CList <CExposure, CExposure&>::RemoveAt(pos);
	GetPlate()->Invalidate();
}

BOOL CExposureSequence::HasNoPages()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure   = GetNext(pos);
		CLayoutObject* pObject = rExposure.GetLayoutObject();
		if (pObject)
			if (rExposure.GetLayoutObject()->m_nType == CLayoutObject::Page)
				return FALSE;
	}
	return TRUE;
}


CExposure* CExposureSequence::GetExposure(short nLayoutObjectIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CExposure& rExposure = GetNext(pos);
		if (rExposure.m_nLayoutObjectIndex == nLayoutObjectIndex)
			return &rExposure;
	}
	return NULL;
}





/////////////////////////////////////////////////////////////////////////////
// CExposure

IMPLEMENT_SERIAL(CExposure, CObject, VERSIONABLE_SCHEMA | 1)

CExposure::CExposure()
{
	m_fLeftBleed			= 0.0;
	m_fRightBleed			= 0.0;
	m_fUpperBleed			= 0.0;
	m_fLowerBleed			= 0.0;
	m_nLockedBleeds			= 0;
	m_bEnabled				= TRUE;
	m_nAction				= 0;
	m_nDiffusionPercentage	= 0;
	m_PreVacuumTime			= 0;
	m_ExposingClocks		= 0;
	m_nLayoutObjectIndex	= 0;

	m_pSequence				= NULL;
}

// copy constructor for CExposure - needed by CList functions:
CExposure::CExposure(const CExposure& rExposure) 
{
	m_pSequence	= NULL;

	Copy(rExposure);
}

CPlate*	CExposure::GetPlate() 
{
	if (this)
		if (m_pSequence)
			return m_pSequence->GetPlate(); 
	return NULL;
}

void CExposure::Create(CPlate* pDefaultPlate, short nLayoutObjectIndex)
{
	m_fLeftBleed			= 0.0f;
	m_fRightBleed			= 0.0f;
	m_fUpperBleed			= 0.0f;
	m_fLowerBleed			= 0.0f;
	m_nLockedBleeds			= 0;
	m_bEnabled				= TRUE;
	m_nAction				= 0;
	m_nDiffusionPercentage	= 0;
	m_PreVacuumTime			= 0;
	m_ExposingClocks		= 0;
	m_nLayoutObjectIndex	= nLayoutObjectIndex;

	if (pDefaultPlate)
	{
		CExposure* pDefaultExposure = pDefaultPlate->m_ExposureSequence.GetExposure(nLayoutObjectIndex);
		if (pDefaultExposure)
			*this = *pDefaultExposure;
	}
}

// operator overload for CExposure - needed by CList functions:
const CExposure& CExposure::operator=(const CExposure& rExposure)
{
	Copy(rExposure);

	return *this;
}


void CExposure::Copy(const CExposure& rExposure)
{
	m_fLeftBleed			= rExposure.m_fLeftBleed;
	m_fRightBleed			= rExposure.m_fRightBleed;
	m_fUpperBleed			= rExposure.m_fUpperBleed;
	m_fLowerBleed			= rExposure.m_fLowerBleed;
	m_nLockedBleeds			= rExposure.m_nLockedBleeds;
	m_bEnabled				= rExposure.m_bEnabled;
	m_nAction				= rExposure.m_nAction;
	m_nDiffusionPercentage	= rExposure.m_nDiffusionPercentage;
	m_PreVacuumTime			= rExposure.m_PreVacuumTime;
	m_ExposingClocks		= rExposure.m_ExposingClocks;
	m_nLayoutObjectIndex	= rExposure.m_nLayoutObjectIndex;
}

CLayoutObject* CExposure::GetLayoutObject(int nLayoutIndex, int nLayoutSide)
{
	if (GetPlate()->GetLayoutSide()->m_nLayoutSide != nLayoutSide)
		return NULL;

	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	POSITION	 pos  = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nLayoutIndex);
	if (pos == NULL)
		return NULL;

	CLayoutSide* pLayoutSide;
	if (nLayoutSide == FRONTSIDE)
		pLayoutSide = &(pDoc->m_PrintSheetList.m_Layouts.GetAt(pos).m_FrontSide);
	else
		pLayoutSide = &(pDoc->m_PrintSheetList.m_Layouts.GetAt(pos).m_BackSide);

	pos = pLayoutSide->m_ObjectList.FindIndex(m_nLayoutObjectIndex);
	if (pos == NULL)
		return NULL;

	return &(pLayoutSide->m_ObjectList.GetAt(pos));
}

CLayoutObject* CExposure::GetLayoutObject(CLayout* pLayout, int nLayoutSide)
{
	if ( ! pLayout)
		return NULL;
	if (GetPlate()->GetLayoutSide()->m_nLayoutSide != nLayoutSide)
		return NULL;

	CLayoutSide* pLayoutSide;
	if (nLayoutSide == FRONTSIDE)
		pLayoutSide = &pLayout->m_FrontSide;
	else
		pLayoutSide = &pLayout->m_BackSide;

	POSITION pos = pLayoutSide->m_ObjectList.FindIndex(m_nLayoutObjectIndex);
	if (pos == NULL)
		return NULL;

	return &(pLayoutSide->m_ObjectList.GetAt(pos));
}

CLayoutObject* CExposure::GetLayoutObject()
{
	CPrintSheet* pPrintSheet = GetPlate()->GetPrintSheet();
	CLayoutSide* pLayoutSide = NULL;
	CLayout*	 pLayout	 = pPrintSheet->GetLayout();
	POSITION	 platePos	 = pPrintSheet->m_FrontSidePlates.GetHeadPosition();
	while (platePos)
	{
		CPlate&  rPlate		 = pPrintSheet->m_FrontSidePlates.GetNext(platePos);
		POSITION exposurePos = rPlate.m_ExposureSequence.GetHeadPosition();
		while (exposurePos && !pLayoutSide)
		{
			CExposure& rExposure = rPlate.m_ExposureSequence.GetNext(exposurePos);
			if (this == &rExposure)
			{
				pLayoutSide = &pLayout->m_FrontSide;
				break;
			}
		}
	}
	if (!pLayoutSide)
	{
		platePos = pPrintSheet->m_BackSidePlates.GetHeadPosition();
		while (platePos)
		{
			CPlate&  rPlate		 = pPrintSheet->m_BackSidePlates.GetNext(platePos);
			POSITION exposurePos = rPlate.m_ExposureSequence.GetHeadPosition();
			while (exposurePos && !pLayoutSide)
			{
				CExposure& rExposure = rPlate.m_ExposureSequence.GetNext(exposurePos);
				if (this == &rExposure)
				{
					pLayoutSide = &pLayout->m_BackSide;
					break;
				}
			}
		}
	}

	if ( ! pLayoutSide)
	{
		// either on frontside plates nor on backside plates found -> inspect default plates
		POSITION exposurePos = pPrintSheet->m_FrontSidePlates.m_defaultPlate.m_ExposureSequence.GetHeadPosition();
		while (exposurePos && !pLayoutSide)
		{
			CExposure& rExposure = pPrintSheet->m_FrontSidePlates.m_defaultPlate.m_ExposureSequence.GetNext(exposurePos);
			if (this == &rExposure)
			{
				pLayoutSide = &pLayout->m_FrontSide;
				break;
			}
		}
		if ( ! pLayoutSide)
		{
			POSITION exposurePos = pPrintSheet->m_BackSidePlates.m_defaultPlate.m_ExposureSequence.GetHeadPosition();
			while (exposurePos && !pLayoutSide)
			{
				CExposure& rExposure = pPrintSheet->m_BackSidePlates.m_defaultPlate.m_ExposureSequence.GetNext(exposurePos);
				if (this == &rExposure)
				{
					pLayoutSide = &pLayout->m_BackSide;
					break;
				}
			}
		}
	}

	if ( ! pLayoutSide)
		return NULL;

	POSITION pos = pLayoutSide->m_ObjectList.FindIndex(m_nLayoutObjectIndex);
	if (pos == NULL)
		return NULL;

	return &(pLayoutSide->m_ObjectList.GetAt(pos));
}

float CExposure::GetBleedValue(int nSide)
{
	if (GetPlate())
		if (GetPlate()->IsInvalid())
		{
			GetPlate()->Validate();	// Validate in order to prevent for recursive calls
			GetPlate()->CalcAutoBleeds();
		}

	switch (nSide)
	{
	case TOP:		return m_fUpperBleed; 
	case BOTTOM:	return m_fLowerBleed; 
	case LEFT:		return m_fLeftBleed;  
	case RIGHT:		return m_fRightBleed; 
	default:		return 0.0f; 
	}
}

float CExposure::GetRealBleedValue(int nSide)
{
	if (GetPlate())
		if (GetPlate()->IsInvalid())
		{
			GetPlate()->Validate();	// Validate in order to prevent for recursive calls
			GetPlate()->CalcAutoBleeds();
		}

	int nHeadPosition = GetLayoutObject()->m_nHeadPosition;

	switch (nSide)
	{
	case TOP:
		switch (nHeadPosition)
		{
		case TOP:		return m_fUpperBleed; 
		case BOTTOM:	return m_fLowerBleed; 
		case LEFT:		return m_fLeftBleed;  
		case RIGHT:		return m_fRightBleed; 
		}
	case BOTTOM:
		switch (nHeadPosition)
		{
		case TOP:		return m_fLowerBleed; 
		case BOTTOM:	return m_fUpperBleed; 
		case LEFT:		return m_fRightBleed; 
		case RIGHT:		return m_fLeftBleed;  
		}
	case LEFT:
		switch (nHeadPosition)
		{
		case TOP:		return m_fLeftBleed;  
		case BOTTOM:	return m_fRightBleed; 
		case LEFT:		return m_fLowerBleed; 
		case RIGHT:		return m_fUpperBleed; 
		}
	case RIGHT:
		switch (nHeadPosition)
		{
		case TOP:		return m_fRightBleed; 
		case BOTTOM:	return m_fLeftBleed;  
		case LEFT:		return m_fUpperBleed; 
		case RIGHT:		return m_fLowerBleed; 
		}
	}

	return 0.0f;
}

void CExposure::CalcAutoBleed(int nSide, float fValue)
{
	CLayoutObject* pLayoutObject = GetLayoutObject();
	if (!pLayoutObject)
		return;

	if (pLayoutObject->m_nType == CLayoutObject::ControlMark)
	{
		SetBleedValue(nSide, 0.0f);
		return;
	}

	CPrintSheet* pPrintSheet = (GetPlate()) ? GetPlate()->GetPrintSheet() : NULL;
	if ( ! pPrintSheet)
		return;
	CPageTemplate* pObjectTemplate = (pLayoutObject->m_nType == CLayoutObject::ControlMark) ?
									  pLayoutObject->GetCurrentMarkTemplate() :
									  pLayoutObject->GetCurrentPageTemplate(pPrintSheet);
	if ( ! pObjectTemplate)
		return;

	float fBorder = FLT_MAX;
	if (pLayoutObject->IsPanoramaPage(*pPrintSheet))	// panorama page
	{
		CLayoutObject* pBuddy = pLayoutObject->GetCurrentMultipagePartner(pObjectTemplate, TRUE);

		ASSERT(pBuddy);

		if (pLayoutObject->PanoramaHasNeighbour(pBuddy, nSide))
		{
			switch (nSide)
			{
			case TOP:		fBorder = max(pLayoutObject->GetTop() + pLayoutObject->m_fUpperBorder, pBuddy->GetTop() + pBuddy->m_fUpperBorder) -
									  max(pLayoutObject->GetTop(), pBuddy->GetTop());
							break;
			case BOTTOM:	fBorder = min(pLayoutObject->GetBottom(), pBuddy->GetBottom()) - 
									  min(pLayoutObject->GetBottom() - pLayoutObject->m_fLowerBorder, pBuddy->GetBottom() - pBuddy->m_fLowerBorder);
							break;
			case LEFT:		fBorder = min(pLayoutObject->GetLeft(),	pBuddy->GetLeft()) - 
									  min(pLayoutObject->GetLeft() - pLayoutObject->m_fLeftBorder, pBuddy->GetLeft() - pBuddy->m_fLeftBorder);
							break;
			case RIGHT:		fBorder = max(pLayoutObject->GetRight() + pLayoutObject->m_fRightBorder, pBuddy->GetRight() + pBuddy->m_fRightBorder) -
									  max(pLayoutObject->GetRight(), pBuddy->GetRight());
							break;
			default:		fBorder = FLT_MAX;
							break;
			}			
			fBorder = min(fValue, fBorder);
		}
		else
			fBorder = fValue;
	}
	else
	{
		switch (nSide)
		{
		case TOP:	fBorder = min(fValue, (pLayoutObject->m_UpperNeighbours.GetSize()) ? pLayoutObject->m_fUpperBorder : FLT_MAX); break;
		case RIGHT:	fBorder = min(fValue, (pLayoutObject->m_RightNeighbours.GetSize()) ? pLayoutObject->m_fRightBorder : FLT_MAX); break;
		case BOTTOM:fBorder = min(fValue, (pLayoutObject->m_LowerNeighbours.GetSize()) ? pLayoutObject->m_fLowerBorder : FLT_MAX); break;
		case LEFT:	fBorder = min(fValue, (pLayoutObject->m_LeftNeighbours.GetSize() ) ? pLayoutObject->m_fLeftBorder  : FLT_MAX); break;
		default:	return;
		}

		if ( (pLayoutObject->IsFlatProduct()) && (fBorder < -0.001f) )	// in case of label job and negative bleed (because of overlappings) disable automatizm (set bleed to global value)
			fBorder = fValue;
		else
		{
			if ( (pLayoutObject->m_nType == CLayoutObject::Page) && ! pLayoutObject->IsFlatProduct())
			{
				if (nSide == pLayoutObject->GetSpineSide())
				{
					CBoundProduct& rProduct = pObjectTemplate->GetProductPart().GetBoundProduct();

					if ( (fabs(rProduct.m_bindingParams.m_fMillingDepth + rProduct.m_bindingParams.m_fGlueLineWidth) > 0.01) && (rProduct.m_bindingParams.m_bindingDefs.m_nBinding == CFinalProductDescription::PerfectBound))
					{
						if ( ! pObjectTemplate->GetProductPart().IsCover())
							fBorder = -rProduct.m_bindingParams.m_fGlueLineWidth;	// if milling edge, set mask to zero minus glueline
					}
					else
						if ( (fabs(rProduct.m_bindingParams.m_fGlueLineWidth) > 0.01) && (rProduct.m_bindingParams.m_bindingDefs.m_nBinding == CFinalProductDescription::SaddleStitched))
						{
							if ( ! pObjectTemplate->IsFirstLastOrInnerPage())
							{
								if (fabs(rProduct.m_bindingParams.m_fGlueLineWidth) > 0.01)
									fBorder = -rProduct.m_bindingParams.m_fGlueLineWidth;	// set mask to zero minus glueline
							}
						}
				}
			}
		}
	}

	SetBleedValue(nSide, fBorder);
}

void CExposure::SetBleedValue(int nSide, float fValue, BOOL bLock, CPrintSheetView* pView)
{
	float fDiff	= fValue - GetBleedValue(nSide);	

	switch (nSide)
	{
	case TOP:	m_fUpperBleed = fValue; break;
	case RIGHT:	m_fRightBleed = fValue; break;
	case BOTTOM:m_fLowerBleed = fValue; break;
	case LEFT:	m_fLeftBleed  = fValue; break; 
	default:	return;
	}

	if (bLock)
		m_nLockedBleeds|= nSide; 
	else
		m_nLockedBleeds&= ~nSide; 


	if (fDiff == 0.0f)
		return;

	if (pView)
	{
		CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData((void*)this);
		if (pDI)
		{
			CRect newRect = pDI->m_BoundingRect;
			switch (nSide)
			{
			case TOP:	newRect.top   += (long)(CPrintSheetView::WorldToLP(fDiff) / pView->m_DisplayList.m_fViewportRatioY + 2); break;
			case RIGHT:	newRect.right += (long)(CPrintSheetView::WorldToLP(fDiff) / pView->m_DisplayList.m_fViewportRatioX + 2); break;
			case BOTTOM:newRect.bottom-= (long)(CPrintSheetView::WorldToLP(fDiff) / pView->m_DisplayList.m_fViewportRatioY + 2); break;
			case LEFT:	newRect.left  -= (long)(CPrintSheetView::WorldToLP(fDiff) / pView->m_DisplayList.m_fViewportRatioX + 2); break;
			default: return;
			}

			newRect.NormalizeRect();
			CRect unionRect;
			unionRect.UnionRect(newRect, pDI->m_AuxRect);
			unionRect.UnionRect(newRect, pDI->m_BoundingRect);
			unionRect.InflateRect(5,5);
			pView->InvalidateRect(unionRect);
		}
	}
}

void CExposure::LockBleed(int nSide)
{
	m_nLockedBleeds|= nSide; 
}

void CExposure::UnlockBleed(int nSide, CPrintSheetView* pView)
{
	if (pView)
	{
		CDisplayItem* pDI = pView->m_DisplayList.GetItemFromData((void*)this);
		if (pDI)
		{
			CRect rect;
			rect.UnionRect(pDI->m_AuxRect, pDI->m_BoundingRect);
			rect.InflateRect(5,5);
			pView->InvalidateRect(rect);
		}
	}
 
	m_nLockedBleeds&= ~nSide; 
}

BOOL CExposure::IsBleedLocked(int nSide)
{
	return (m_nLockedBleeds & nSide) ? TRUE : FALSE;
}


// Get virtual trim box of page contents in sheet coordinates. In case of
//		- normal single pages it is just the layout object box,
//		- separated double pages it is the virtual box, as if the corresponding buddy is moved beside the layout object,
//		- real panorama pages it is the maximum box of layout object and buddy.
float CExposure::GetContentTrimBox(int nSide, CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	if (!pObject)
		return 0.0f;

	float fLeft			 = pObject->GetLeft();		// normal single page
	float fBottom		 = pObject->GetBottom();
	float fRight		 = pObject->GetRight();	
	float fTop			 = pObject->GetTop(); 
	float fFanoutOffsetX = pObject->GetLayoutSide()->GetFanoutOffsetX(GetPlate()->m_strPlateName, fLeft   + (fRight - fLeft)   / 2.0f);
	float fFanoutOffsetY = pObject->GetLayoutSide()->GetFanoutOffsetY(GetPlate()->m_strPlateName, fBottom + (fTop   - fBottom) / 2.0f);

	if (pTemplate->m_nMultipageGroupNum != -1)	// double (multi) page
	{
		CPrintSheet* pPrintSheet = GetPlate()->GetPrintSheet();
		
		if (pObject->IsPanoramaPage(*pPrintSheet))	// panorama page
		{
			CLayoutObject* pBuddy = pObject->GetCurrentMultipagePartner(pTemplate, TRUE);
			fLeft		   = min(fLeft,   pBuddy->GetLeft() );
			fBottom		   = min(fBottom, pBuddy->GetBottom() );
			fRight		   = max(fRight,  pBuddy->GetRight() );
			fTop		   = max(fTop,    pBuddy->GetTop() );
			// for panorama page fanout corresponds to content box, because we have only one exposure -> so recalc fanout
			fFanoutOffsetX = pObject->GetLayoutSide()->GetFanoutOffsetX(GetPlate()->m_strPlateName, fLeft   + (fRight - fLeft)   / 2.0f);
			fFanoutOffsetY = pObject->GetLayoutSide()->GetFanoutOffsetY(GetPlate()->m_strPlateName, fBottom + (fTop   - fBottom) / 2.0f);

		}
		else										// separated panorama page
		{
			CLayoutObject* pBuddy	   = pObject->GetCurrentMultipagePartner(pTemplate, FALSE);
			BOOL		   bIsLeftPage = pObject->IsMultipagePart(*pPrintSheet, LEFT);
			switch (pObject->m_nHeadPosition)
			{
			case TOP:		fLeft   -= (!bIsLeftPage) ? pBuddy->GetWidth()  : 0.0f; 
							fRight  += ( bIsLeftPage) ? pBuddy->GetWidth()  : 0.0f; break;
			case BOTTOM:	fLeft   -= ( bIsLeftPage) ? pBuddy->GetWidth()  : 0.0f; 
							fRight  += (!bIsLeftPage) ? pBuddy->GetWidth()  : 0.0f; break;
			case LEFT:		fBottom	-= (!bIsLeftPage) ? pBuddy->GetHeight() : 0.0f;
							fTop	+= ( bIsLeftPage) ? pBuddy->GetHeight() : 0.0f; break;
			case RIGHT:		fBottom	-= ( bIsLeftPage) ? pBuddy->GetHeight() : 0.0f;
							fTop	+= (!bIsLeftPage) ? pBuddy->GetHeight() : 0.0f; break;
			}
		}
	}

	switch (nSide)
	{
	case LEFT:		return fLeft   + fFanoutOffsetX;
	case BOTTOM:	return fBottom + fFanoutOffsetY;
	case RIGHT:		return fRight  + fFanoutOffsetX;
	case TOP:		return fTop	   + fFanoutOffsetY;
	default:		return 0.0f;											   
	}
}


float CExposure::GetContentTrimBoxCenterX(CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	float fTrimBoxLeft  = GetContentTrimBox(LEFT,  pObject, pTemplate);
	float fTrimBoxRight = GetContentTrimBox(RIGHT, pObject, pTemplate);

	return fTrimBoxLeft + (fTrimBoxRight - fTrimBoxLeft) / 2.0f;
}

float CExposure::GetContentTrimBoxCenterY(CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	float fTrimBoxBottom = GetContentTrimBox(BOTTOM, pObject, pTemplate);
	float fTrimBoxTop	 = GetContentTrimBox(TOP,	pObject, pTemplate);

	return fTrimBoxBottom + (fTrimBoxTop - fTrimBoxBottom) / 2.0f;
}


// Get sheet layout trim box of page in sheet coordinates. In case of
//		- normal single pages or separated double pages it is just the layout object box,
//		- real panorama pages it is the maximum box of layout object and buddy.
float CExposure::GetSheetTrimBox(int nSide, CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	if (!pObject)
		return 0.0f;

	float		 fLeft, fBottom, fRight, fTop, fFanoutOffsetX, fFanoutOffsetY;
	CPrintSheet* pPrintSheet = GetPlate()->GetPrintSheet();
		
	if (pObject->IsPanoramaPage(*pPrintSheet))	// panorama page
	{
		CLayoutObject* pBuddy = pObject->GetCurrentMultipagePartner(pTemplate, TRUE);

		fLeft		   = min(pObject->GetLeft(),   pBuddy->GetLeft() );
		fBottom		   = min(pObject->GetBottom(), pBuddy->GetBottom() );
		fRight		   = max(pObject->GetRight(),  pBuddy->GetRight() );
		fTop		   = max(pObject->GetTop(),    pBuddy->GetTop() );
		// for panorama page fanout corresponds to content box, because we have only one exposure 
		fFanoutOffsetX = pObject->GetLayoutSide()->GetFanoutOffsetX(GetPlate()->m_strPlateName, fLeft   + (fRight - fLeft)   / 2.0f);
		fFanoutOffsetY = pObject->GetLayoutSide()->GetFanoutOffsetY(GetPlate()->m_strPlateName, fBottom + (fTop   - fBottom) / 2.0f);

	}
	else	// normal single page or separated double page
	{
		fLeft		   = pObject->GetLeft();		
		fBottom		   = pObject->GetBottom();
		fRight		   = pObject->GetRight();	
		fTop		   = pObject->GetTop(); 
		fFanoutOffsetX = pObject->GetLayoutSide()->GetFanoutOffsetX(GetPlate()->m_strPlateName, fLeft   + (fRight - fLeft)   / 2.0f);
		fFanoutOffsetY = pObject->GetLayoutSide()->GetFanoutOffsetY(GetPlate()->m_strPlateName, fBottom + (fTop   - fBottom) / 2.0f);
	}

	switch (nSide)
	{
	case LEFT:		return fLeft   + fFanoutOffsetX;
	case BOTTOM:	return fBottom + fFanoutOffsetY;
	case RIGHT:		return fRight  + fFanoutOffsetX;
	case TOP:		return fTop	   + fFanoutOffsetY;
	default:		return 0.0f;											   
	}
}


// Get the area which will be exposed 
float CExposure::GetSheetBleedBox(int nSide, CLayoutObject* pObject, CPageTemplate* pTemplate)
{
	if (!pObject)
		return 0.0f;

	CPlate* pPlate = GetPlate();
	if (pPlate->IsInvalid())
	{
		pPlate->Validate();	// Validate in order to prevent for recursive calls
		pPlate->CalcAutoBleeds();
	}

	switch (nSide)
	{
	case LEFT:	 return GetSheetTrimBox(nSide, pObject, pTemplate) - m_fLeftBleed;  
	case BOTTOM: return GetSheetTrimBox(nSide, pObject, pTemplate) - m_fLowerBleed; 
	case RIGHT:	 return GetSheetTrimBox(nSide, pObject, pTemplate) + m_fRightBleed; 
	case TOP:	 return GetSheetTrimBox(nSide, pObject, pTemplate) + m_fUpperBleed; 
	default:	 return 0.0f;											   
	}
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////




////////////////////////////// CPressDeviceList /////////////////////////////
/////////////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CPressDeviceList, CObject, VERSIONABLE_SCHEMA | 1)


CPressDeviceList::CPressDeviceList()
{
}


// add device to list only if not already in - if so, just return the index
int CPressDeviceList::AddDevice(CPressDevice& rPressDevice, BOOL& bRenamed)
{
	bRenamed = FALSE;

	int		 nIndex	= 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (GetNext(pos) == rPressDevice)	// device already in list - so do not add, but return the index
			return nIndex;				// TODO: comparison of names not enough - comparing of complete parameters would be correct
		nIndex++;
	}

	if (DeviceNameExist(rPressDevice.m_strName))		// list contains device with same name but different parameters - so rename new device
	{
		CString strName, strNum;
		for (int i = 1; i <= GetCount() + 1; i++)
		{
			strNum.Format(_T("%d"), i);
			strName = rPressDevice.m_strName + strNum;
			if ( ! DeviceNameExist(strName))
				break;
		}

		rPressDevice.m_strName = strName;
		bRenamed = TRUE;
	}

	AddTail(rPressDevice);

	return GetCount() - 1;
}

BOOL CPressDeviceList::DeviceNameExist(const CString& strName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
		if (GetNext(pos).m_strName.CompareNoCase(strName) == 0)
			return TRUE;

	return FALSE;
}

int CPressDeviceList::GetDeviceIndex(const CString& strName)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (GetNext(pos).m_strName.CompareNoCase(strName) == 0)
			return nIndex;
		nIndex++;
	}

	return -1;
}

int CPressDeviceList::GetDeviceIndexByID(const CString& strID)
{
	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (GetNext(pos).m_strDeviceID.CompareNoCase(strID) == 0)
			return nIndex;
		nIndex++;
	}

	return -1;
}

POSITION CPressDeviceList::GetDevicePos(const CString& strName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		if (GetAt(pos).m_strName.CompareNoCase(strName) == 0)
			return pos;
		GetNext(pos);
	}

	return NULL;
}

CPressDevice* CPressDeviceList::GetDevice(const CString& strName)
{
	if (strName.IsEmpty())
		return NULL;

	int		 nIndex = 0;
	POSITION pos	= GetHeadPosition();
	while (pos)
	{
		if (GetAt(pos).m_strName.CompareNoCase(strName) == 0)
			return &GetAt(pos);

		nIndex++;
		GetNext(pos);
	}

	return NULL;
}


/////////////////////////////////////////////////////////////////////////////
// CPressDevice

IMPLEMENT_SERIAL(CPressDevice, CObject, VERSIONABLE_SCHEMA | 3)

CPressDevice::CPressDevice()
{
	m_strName				= _T("");
	m_strDeviceID			= _T("");
	m_nPressType			= SheetPress;
	m_fPlateWidth			= 0.0f;
	m_fPlateHeight			= 0.0f; 
	m_fPlateEdgeToPrintEdge = 0.0f;
	m_fGripper				= 0.0f;
	m_fXRefLine				= 0.0f;
	m_fYRefLine1			= 0.0f;
	m_fYRefLine2			= 0.0f;
	m_strNotes				= _T("");
}

CPressDevice::CPressDevice(CPressDevice& rPressDevice)
{
	*this = rPressDevice;
}

CPressDevice::~CPressDevice()
{
	m_strName.Empty();
	m_strDeviceID.Empty();
	m_strNotes.Empty();
	m_fYRefLine1 = 0.0f;
	m_fYRefLine2 = 0.0f;
}

// operator overload for CPressDevice - needed by CList functions:

const CPressDevice& CPressDevice::operator=(const CPressDevice& rPressDevice)
{
	m_strName				= rPressDevice.m_strName;
	m_strDeviceID			= rPressDevice.m_strDeviceID;
	m_nPressType			= rPressDevice.m_nPressType;
	m_fPlateWidth			= rPressDevice.m_fPlateWidth;
	m_fPlateHeight			= rPressDevice.m_fPlateHeight; 
	m_fPlateEdgeToPrintEdge = rPressDevice.m_fPlateEdgeToPrintEdge;
	m_fGripper				= rPressDevice.m_fGripper;
	m_fXRefLine				= rPressDevice.m_fXRefLine;
	m_fYRefLine1			= rPressDevice.m_fYRefLine1;
	m_fYRefLine2			= rPressDevice.m_fYRefLine2;
	m_strNotes				= rPressDevice.m_strNotes;

	return *this;
}


BOOL CPressDevice::operator==(const CPressDevice& rPressDevice)
{
	if (m_strName		!= rPressDevice.m_strName)
		return FALSE;
	if (m_strDeviceID	!= rPressDevice.m_strDeviceID)
		return FALSE;
	if (m_nPressType	!= rPressDevice.m_nPressType)
		return FALSE;
	if (m_fPlateWidth	!= rPressDevice.m_fPlateWidth)
		return FALSE;
	if (m_fPlateHeight	!= rPressDevice.m_fPlateHeight)
		return FALSE;
		
	switch (m_nPressType)
	{
	case CPressDevice::SheetPress: if (m_fPlateEdgeToPrintEdge	!= rPressDevice.m_fPlateEdgeToPrintEdge)
									   return FALSE;
								   if (m_fGripper				!= rPressDevice.m_fGripper)
									   return FALSE;
								   break;
	case CPressDevice::WebPress:   if (m_fXRefLine				!= rPressDevice.m_fXRefLine) 
									   return FALSE;
								   if (m_fYRefLine1				!= rPressDevice.m_fYRefLine1)
									   return FALSE;
								   if (m_fYRefLine2				!= rPressDevice.m_fYRefLine2)
									   return FALSE;
								   break;
	}

	return TRUE;
}

BOOL CPressDevice::operator!=(const CPressDevice& rPressDevice)
{
	return !(*this == rPressDevice);
}

float CPressDevice::GetPlateRefline(int nReferenceLine)
{
	float fPlateLeft   = 0.0f;
	float fPlateBottom = 0.0f;
	float fPlateRight  = fPlateLeft   + m_fPlateWidth;
	float fPlateTop	   = fPlateBottom + m_fPlateHeight;

	switch (nReferenceLine)
	{
	case LEFT:		return fPlateLeft;
	case RIGHT:		return fPlateRight;
	case XCENTER:	return fPlateLeft   + (fPlateRight - fPlateLeft) / 2;
	case BOTTOM:	return fPlateBottom;
	case TOP:		return fPlateTop;
	case YCENTER:	return fPlateBottom + (fPlateTop - fPlateBottom) / 2; 
	}

	return 0.0f;
}

BOOL CPressDevice::FindLargestPaper(CSheet& rLargestSheet)
{
	CSheetList sheetList;		
	sheetList.Load();
	
	rLargestSheet.m_fWidth = rLargestSheet.m_fHeight = 0.0f;

	BOOL	 bFound = FALSE;
	POSITION pos	= sheetList.GetHeadPosition();
	while (pos)
	{
		CSheet& rRefSheet = sheetList.GetNext(pos);
		if (SheetFitsToPlate(rRefSheet))
			if ( (rRefSheet.m_fWidth * rRefSheet.m_fHeight) > (rLargestSheet.m_fWidth * rLargestSheet.m_fHeight) )
			{
				rLargestSheet = rRefSheet;
				bFound = TRUE;
			}
	}

	return bFound;
}

BOOL CPressDevice::FindPaper(CSheet& rSheet, CString strPaperName)
{
	CSheetList sheetList;		
	sheetList.Load();
	
	POSITION pos = sheetList.GetHeadPosition();
	while (pos)
	{
		CSheet& rRefSheet = sheetList.GetNext(pos);
		if (rRefSheet.m_strSheetName == strPaperName)
		{
			rSheet = rRefSheet;
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPressDevice::SheetFitsToPlate(CSheet& rSheet)
{
	float platemid = m_fPlateWidth / 2.0f;
	float fPaperLeft  = platemid - (rSheet.m_fWidth / 2.0f);
	float fPaperRight = fPaperLeft +  rSheet.m_fWidth;
	float fPaperBottom, fPaperTop;
	if (m_nPressType == SheetPress)
		fPaperBottom = m_fPlateEdgeToPrintEdge - m_fGripper;
	else
		fPaperBottom = GetYCenterOfSection() - (rSheet.m_fHeight / 2.0f);
	fPaperTop = fPaperBottom + rSheet.m_fHeight;

	if ( (fPaperLeft < 0.0f) || (fPaperBottom < 0.0f) || (fPaperRight > m_fPlateWidth) || (fPaperTop > m_fPlateHeight) )		// TODO: check for non printable area
		return FALSE;
	else
		return TRUE;
}



// helper function for PressDevice elements
template <> void AFXAPI SerializeElements <CPressDevice> (CArchive& ar, CPressDevice* pPressDevice, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CPressDevice));

	float fReserved = 0.0f;
	if (ar.IsStoring())
	{
		ar << pPressDevice->m_strName;
		ar << pPressDevice->m_strDeviceID;
		ar << pPressDevice->m_nPressType;
		ar << pPressDevice->m_fPlateWidth;
		ar << pPressDevice->m_fPlateHeight;
		ar << pPressDevice->m_fPlateEdgeToPrintEdge;
		ar << pPressDevice->m_fGripper;
		ar << pPressDevice->m_fXRefLine;
		ar << pPressDevice->m_fYRefLine1;
		ar << pPressDevice->m_fYRefLine2;
		ar << fReserved;
		ar << pPressDevice->m_strNotes;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();
		
		switch (nVersion)
		{
		case 1:
			ar >> pPressDevice->m_strName;
			ar >> pPressDevice->m_nPressType;
			ar >> pPressDevice->m_fPlateWidth;
			ar >> pPressDevice->m_fPlateHeight;
			ar >> pPressDevice->m_fPlateEdgeToPrintEdge;
			ar >> pPressDevice->m_fGripper;
			ar >> pPressDevice->m_fXRefLine;
			ar >> pPressDevice->m_fYRefLine1;
			ar >> pPressDevice->m_strNotes;
			pPressDevice->m_fYRefLine2 = 0.0f;
			break;
		case 2:
			ar >> pPressDevice->m_strName;
			ar >> pPressDevice->m_strDeviceID;
			ar >> pPressDevice->m_nPressType;
			ar >> pPressDevice->m_fPlateWidth;
			ar >> pPressDevice->m_fPlateHeight;
			ar >> pPressDevice->m_fPlateEdgeToPrintEdge;
			ar >> pPressDevice->m_fGripper;
			ar >> pPressDevice->m_fXRefLine;
			ar >> pPressDevice->m_fYRefLine1;
			ar >> pPressDevice->m_strNotes;
			pPressDevice->m_fYRefLine2 = 0.0f;
			break;
		case 3:
			ar >> pPressDevice->m_strName;
			ar >> pPressDevice->m_strDeviceID;
			ar >> pPressDevice->m_nPressType;
			ar >> pPressDevice->m_fPlateWidth;
			ar >> pPressDevice->m_fPlateHeight;
			ar >> pPressDevice->m_fPlateEdgeToPrintEdge;
			ar >> pPressDevice->m_fGripper;
			ar >> pPressDevice->m_fXRefLine;
			ar >> pPressDevice->m_fYRefLine1;
			ar >> pPressDevice->m_fYRefLine2;
			ar >> fReserved;
			ar >> pPressDevice->m_strNotes;
			break;
		}
	}
}





///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// Document Device Analyse - functions 

// Compares PressDevice entries of the document with the global PressDeviceList 
// If not fully equal take the PressDeviceList from document
//void CImpManDoc::AnalysePressDeviceIndices()
//{	
//	int numberDevices, i;
//	int nIndex;
//	CPressDevice  PressDeviceBufDoc, PressDeviceBufApp;
//
//	nIndex = -1; // Predefine with "NotFound"
//
//	if ( ! (m_PrintSheetList.m_Layouts.m_PressDevices.IsEmpty())) 
//	{
//		// Search to Find the default PressDevice of the document in the global theApp.m_PressDeviceList
//		POSITION pos = m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(m_nDefaultPressDeviceIndex);
//		if (pos)
//		{
//			PressDeviceBufDoc = m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
//
//			nIndex = CompareWithAppList(&PressDeviceBufDoc); //Compare default PressDevice
//
//			if (nIndex != -1)
//					// Default PressDevice of the document found in the global theApp.m_PressDeviceList
//			{		// Now compare the other PressDevices
//				numberDevices = m_PrintSheetList.m_Layouts.m_PressDevices.GetCount();
//				if (numberDevices > 1) // if more than the DefaultPressDevice only
//				{
//					pos = m_PrintSheetList.m_Layouts.m_PressDevices.GetHeadPosition();
//					for (i = 0; i < numberDevices; i ++)
//					{
//						PressDeviceBufDoc = m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
//						nIndex = CompareWithAppList(&PressDeviceBufDoc);
//						if (nIndex == -1)
//							break;
//						m_PrintSheetList.m_Layouts.m_PressDevices.GetNext(pos);
//					}
//				}
//			}
//			//Compare not succesful -> Message Window
//			if (nIndex == -1)
//			{
//				if (theApp.m_nGUIStyle == CImpManApp::GUIStandard)
//				{
//					CString string;
//					string.Format(IDS_PRESS_NOTFOUND, GetTitle());
//					AfxMessageBox(string, MB_OK);
//				}
//			}
//		}
//	}
//	else
//		; // TODO : Errormessage: There must be a default pressdevice
//}

///////////////////////////////////////////////////////////////////////////////////////////////
// Compares PressDevice entries with the global PressDeviceList 
int CImpManDoc::CompareWithAppList(CPressDevice* pPressDeviceBufDoc)
{
	CPressDevice PressDeviceBufApp;
	int nIndex = -1;

	int numberDevices = theApp.m_PressDeviceList.GetCount();
	POSITION pos	  = theApp.m_PressDeviceList.GetHeadPosition();

	for (int i = 0; i < numberDevices; i ++)
	{
		PressDeviceBufApp = theApp.m_PressDeviceList.GetAt(pos);

		if (pPressDeviceBufDoc->m_strName == PressDeviceBufApp.m_strName)
		{
			if ((pPressDeviceBufDoc->m_nPressType	== PressDeviceBufApp.m_nPressType) &&
				(pPressDeviceBufDoc->m_fPlateWidth  == PressDeviceBufApp.m_fPlateWidth) &&
				(pPressDeviceBufDoc->m_fPlateHeight == PressDeviceBufApp.m_fPlateHeight))
			{
				if (pPressDeviceBufDoc->m_nPressType == CPressDevice::SheetPress)
				{
					if ((pPressDeviceBufDoc->m_fPlateEdgeToPrintEdge == PressDeviceBufApp.m_fPlateEdgeToPrintEdge) &&
						(pPressDeviceBufDoc->m_fGripper				 == PressDeviceBufApp.m_fGripper))
							nIndex = i;	// PressDevice of Document found in AppList 
				}
				else //WebPress
				{
					if ((pPressDeviceBufDoc->m_fXRefLine  == PressDeviceBufApp.m_fXRefLine) &&
						(pPressDeviceBufDoc->m_fYRefLine1 == PressDeviceBufApp.m_fYRefLine1) && (pPressDeviceBufDoc->m_fYRefLine2 == PressDeviceBufApp.m_fYRefLine2))
							nIndex = i;	// PressDevice of Document found in AppList
				}
			}
		}
		theApp.m_PressDeviceList.GetNext(pos);
	}
	return nIndex;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Look for unused entries in document
void CImpManDoc::ReorganizePressDeviceIndices(int nLayoutSide)
{
	//CLayout* pLayout;
	//POSITION pos;
	//BOOL	 PressDeviceFound;

	//pos = m_PrintSheetList.m_Layouts.GetHeadPosition(); // if pos == NULL we are creating a new document
	//if ( ! pos)											// clicking OK in JobSettingsPage2
	//	return;

	//int numberDevices = m_PrintSheetList.m_Layouts.m_PressDevices.GetCount();
	//int	nIndex		  = numberDevices - 1;

	//while (numberDevices)
	//{
	//	PressDeviceFound = FALSE;
	//	pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	//	while (pos && ! PressDeviceFound)
	//	{
	//		pLayout = &(m_PrintSheetList.m_Layouts.GetNext(pos));
	//		if (pLayout->m_FrontSide.m_nPressDeviceIndex == nIndex)
	//			PressDeviceFound = TRUE;
	//		if (pLayout->m_BackSide.m_nPressDeviceIndex == nIndex)
	//			PressDeviceFound = TRUE;
	//	}
	//	if ( ! PressDeviceFound)
	//	{
	//		pos = m_PrintSheetList.m_Layouts.m_PressDevices.FindIndex(nIndex);
	//		m_PrintSheetList.m_Layouts.m_PressDevices.RemoveAt(pos);

	//		pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	//		while (pos)
	//		{
	//			pLayout = &(m_PrintSheetList.m_Layouts.GetNext(pos));
	//			if (pLayout->m_FrontSide.m_nPressDeviceIndex > nIndex)
	//				pLayout->m_FrontSide.m_nPressDeviceIndex--;
	//			if (pLayout->m_BackSide.m_nPressDeviceIndex > nIndex)
	//				pLayout->m_BackSide.m_nPressDeviceIndex--;
	//		}

	//		if (nLayoutSide == ALLSIDES)
	//			if (m_nDefaultPressDeviceIndex > nIndex)
	//				m_nDefaultPressDeviceIndex--;
	//	}
	//
	//	nIndex--;
	//	numberDevices--;
	//}
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Check if there are more than one PressDevice in Document for selection in TreeView
BOOL CImpManDoc::MoreThanOnePressdevice()
{
	//int nSize;
	//CLayout* pActLayout;
	//CPressDevice* pFrontPressDevice;
	//CPressDevice* pBackPressDevice;

	//int nSide = GetActSight();
	//switch (nSide)
	//{
	//case ALLSIDES:	nSize = m_PrintSheetList.m_Layouts.m_PressDevices.GetCount();
	//				if (nSize > 1)
	//					return TRUE;
	//				else
	//					return FALSE;
	//				break;
	//case BOTHSIDES:	pActLayout	= GetActLayout();
	//				if ( ! pActLayout)
	//					return FALSE;
	//				pFrontPressDevice = pActLayout->m_FrontSide.GetPressDevice();
	//				pBackPressDevice  = pActLayout->m_BackSide.GetPressDevice();
	//				if (pFrontPressDevice != pBackPressDevice)
	//					return TRUE;
	//				else
	//					return FALSE;
	//				break;
	//default:		return FALSE; // Front- or Backside has only ONE PressDevice
	//				break;
	//}

	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// Helper - functions 

///////////////////////////////////////////////////////////////////////////////////////////////
// Set the PrintSheet PressDevice in Document from given selection of ListBox
void CImpManDoc::SetPrintSheetPressDevice(int nIndex, int nLayoutSide, CLayout* pActLayout, BOOL bReorganize)
{
	CPressDevice PressDeviceBufDoc, PressDeviceBufApp;
	BOOL		 PressDeviceFoundInDoc = FALSE;

	if ( ! pActLayout)
		pActLayout = GetActLayout();
	if ( ! pActLayout)
		return;
	if ( ! theApp.m_PressDeviceList.GetCount())
		return;

	if (nIndex <= 0)
		nIndex = 0;
	POSITION pos		= theApp.m_PressDeviceList.FindIndex(nIndex);
	if ( ! pos)
		return;
	PressDeviceBufApp	= theApp.m_PressDeviceList.GetAt(pos);
	//pos					= m_PrintSheetList.m_Layouts.m_PressDevices.GetHeadPosition();
	//int numberDevices	= m_PrintSheetList.m_Layouts.m_PressDevices.GetCount();

	//for (int i = 0; i < numberDevices; i ++)
	//{
	//	PressDeviceBufDoc = m_PrintSheetList.m_Layouts.m_PressDevices.GetAt(pos);
	//	if ( (PressDeviceBufDoc.m_strName.CompareNoCase(PressDeviceBufApp.m_strName) == 0) &&
	//		PressDeviceBufDoc.m_nPressType			  == PressDeviceBufApp.m_nPressType &&
	//		PressDeviceBufDoc.m_fPlateWidth			  == PressDeviceBufApp.m_fPlateWidth &&
	//		PressDeviceBufDoc.m_fPlateHeight		  == PressDeviceBufApp.m_fPlateHeight &&
	//		PressDeviceBufDoc.m_fPlateEdgeToPrintEdge == PressDeviceBufApp.m_fPlateEdgeToPrintEdge &&
	//		PressDeviceBufDoc.m_fGripper			  == PressDeviceBufApp.m_fGripper &&
	//		PressDeviceBufDoc.m_fXRefLine			  == PressDeviceBufApp.m_fXRefLine &&
	//		PressDeviceBufDoc.m_fYRefLine1			  == PressDeviceBufApp.m_fYRefLine1 &&
	//		PressDeviceBufDoc.m_fYRefLine2			  == PressDeviceBufApp.m_fYRefLine2 )
	//	{
	//		switch (nLayoutSide)
	//		{
	//		case FRONTSIDE : pActLayout->m_FrontSide.m_nPressDeviceIndex = i;
	//						 break;
	//		case BACKSIDE  : pActLayout->m_BackSide.m_nPressDeviceIndex  = i;
	//						 break;
	//		case BOTHSIDES : pActLayout->m_FrontSide.m_nPressDeviceIndex = i;
	//						 pActLayout->m_BackSide.m_nPressDeviceIndex  = i;
	//						 break;
	//		case ALLSIDES  : m_nDefaultPressDeviceIndex = i;
	//						 break;
	//		}
	//		PressDeviceFoundInDoc = TRUE;
	//	}
	//	m_PrintSheetList.m_Layouts.m_PressDevices.GetNext(pos);
	//}
	//if ( ! PressDeviceFoundInDoc)
	//{
	//	m_PrintSheetList.m_Layouts.m_PressDevices.AddTail(PressDeviceBufApp);
		
	//	switch (nLayoutSide)
	//	{	
	//	case FRONTSIDE : pActLayout->m_FrontSide.m_nPressDeviceIndex = numberDevices;
	//		break;
	//	case BACKSIDE  : pActLayout->m_BackSide.m_nPressDeviceIndex  = numberDevices;
	//		break;
	//	case BOTHSIDES : pActLayout->m_FrontSide.m_nPressDeviceIndex = numberDevices;
	//					 pActLayout->m_BackSide.m_nPressDeviceIndex  = numberDevices;
	//		break;
	//	case ALLSIDES  : m_nDefaultPressDeviceIndex = numberDevices;
	//		break;
	//	}
	//}
	//if (nLayoutSide == ALLSIDES)
	//{
	//	int nLayouts = m_PrintSheetList.m_Layouts.GetCount();
	//	if (nLayouts)
	//	{
	//		pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
	//		for (int i = 0; i < nLayouts; i++)
	//		{
	//			CLayout& rActLayout = m_PrintSheetList.m_Layouts.GetAt(pos);
	//			rActLayout.m_FrontSide.m_nPressDeviceIndex = m_nDefaultPressDeviceIndex;
	//			rActLayout.m_BackSide.m_nPressDeviceIndex  = m_nDefaultPressDeviceIndex;
	//			m_PrintSheetList.m_Layouts.GetNext(pos);
	//		}
	//	}
	//}

	//if (bReorganize)
	//	ReorganizePressDeviceIndices(nLayoutSide); // Look for double or unused entries

	switch (nLayoutSide)
	{	
		case FRONTSIDE :	pActLayout->GetPrintingProfile().m_press.m_pressDevice		   = PressDeviceBufApp;
							break;
		case BACKSIDE  :	pActLayout->GetPrintingProfile().m_press.m_backSidePressDevice = PressDeviceBufApp;
							break;
		case BOTHSIDES :	pActLayout->GetPrintingProfile().m_press.m_pressDevice		   = PressDeviceBufApp;
							pActLayout->GetPrintingProfile().m_press.m_backSidePressDevice = PressDeviceBufApp;
							break;
		case ALLSIDES  :	break;
	}

	if (nLayoutSide == ALLSIDES)
	{
		int nLayouts = m_PrintSheetList.m_Layouts.GetCount();
		if (nLayouts)
		{
			pos = m_PrintSheetList.m_Layouts.GetHeadPosition();
			for (int i = 0; i < nLayouts; i++)
			{
				CLayout& rActLayout = m_PrintSheetList.m_Layouts.GetAt(pos);
				rActLayout.GetPrintingProfile().m_press.m_pressDevice		  = PressDeviceBufApp;
				rActLayout.GetPrintingProfile().m_press.m_backSidePressDevice = PressDeviceBufApp;
				m_PrintSheetList.m_Layouts.GetNext(pos);
			}
		}
	}
}

void CImpManDoc::OnFileSaveAs() 
{
	CDocument::OnFileSaveAs();

	theApp.SetMyCurrentDirectory(theApp.settings.m_szWorkDir, TRUE);
}

void CImpManDoc::OnFileClose() 
{
	CDocument::OnFileClose();	
}

void CImpManDoc::OnFileSave() 
{
	theApp.SetMyCurrentDirectory(theApp.settings.m_szJobsDir, TRUE);

	CDocument::OnFileSave();

	theApp.SetMyCurrentDirectory(theApp.settings.m_szWorkDir, TRUE);
}

BOOL CImpManDoc::DoSave(LPCTSTR lpszPathName, BOOL bReplace)
	// Save the document data to a file
	// lpszPathName = path name where to save document file
	// if lpszPathName is NULL then the user will be prompted (SaveAs)
	// note: lpszPathName can be different than 'm_strPathName'
	// if 'bReplace' is TRUE will change file name if successful (SaveAs)
	// if 'bReplace' is FALSE will not change path name (SaveCopyAs)
{
	CString newName = lpszPathName;
	if (newName.IsEmpty())
	{
		CDocTemplate* pTemplate = GetDocTemplate();
		ASSERT(pTemplate != NULL);

		newName = m_strPathName;
		if (bReplace && newName.IsEmpty())
		{
			newName = m_strTitle;
			// check for dubious filename
			int iBad = newName.FindOneOf(_T("#%;/\\"));
			if (iBad != -1)
				newName.ReleaseBuffer(iBad);

			// append the default suffix if there is one
			CString strExt;
			if (pTemplate->GetDocString(strExt, CDocTemplate::filterExt) &&
			  !strExt.IsEmpty())
			{
				ASSERT(strExt[0] == '.');
				newName += strExt;
			}
		}

		if ( ! theApp.m_bServerConnected || (theApp.m_nDBCurrentJobID == -1) )
		{
			if ( ! theApp.DoPromptFileName(newName, bReplace ? AFX_IDS_SAVEFILE : AFX_IDS_SAVEFILECOPY, OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_NOCHANGEDIR | OFN_OVERWRITEPROMPT | OFN_ENABLESIZING, FALSE, pTemplate))
				return FALSE;       // don't even attempt to save
		}
		else
		{
			CString strJobFolder, strFolderDst, strTemplateFileDst;
			strJobFolder.Format		 (_T("%s\\%s"),		theApp.GetAutoServerJobFolderRoot(), _T("\\Manuel Jobs"));
			strFolderDst.Format		 (_T("%s\\%s"),		strJobFolder,  newName);
			strTemplateFileDst.Format(_T("%s\\%s\\%s"), strJobFolder,  newName, newName);
			if (strTemplateFileDst.Right(4).CompareNoCase(_T(".job")) != 0)
				strTemplateFileDst += _T(".job");

			CreateDirectory(strFolderDst, NULL);

			newName = strTemplateFileDst;
		}
	}

	CWaitCursor wait;

	if (!OnSaveDocument(newName))
	{
		if (lpszPathName == NULL)
		{
			// be sure to delete the file
			CFile::Remove(newName);
		}
		return FALSE;
	}

	// reset the title and change the document name
	if (bReplace)
		SetPathName(newName);

	return TRUE;        // success
}

void CImpManDoc::SaveToStorage(CObject* pObject)
{
	ASSERT(m_lpRootStg != NULL);

	// create Contents stream
	COleStreamFile file;
	CFileException fe;
	if (!file.CreateStream(m_lpRootStg, _T("Contents"),
			CFile::modeReadWrite|CFile::shareExclusive|CFile::modeCreate, &fe))
	{
		if (fe.m_cause == CFileException::fileNotFound)
			AfxThrowArchiveException(CArchiveException::badSchema);
		else
			AfxThrowFileException(fe.m_cause, fe.m_lOsError);
	}

	// save to Contents stream
	CArchive saveArchive(&file, CArchive::store | CArchive::bNoFlushOnDelete);
	saveArchive.m_pDocument = this;
	saveArchive.m_bForceFlat = FALSE;

	TRY
	{
		// save the contents
		if (pObject != NULL)
			pObject->Serialize(saveArchive);
		else
			Serialize(saveArchive);
		saveArchive.Close();
		file.Close();

		// commit the root storage
		SCODE sc = m_lpRootStg->Commit(STGC_ONLYIFCURRENT);
		if (sc != S_OK)
			AfxThrowOleException(sc);
	}
	CATCH_ALL(e)
	{
		file.Abort();   // will not throw an exception
		CommitItems(FALSE); // abort save in progress
		//NO_CPP_EXCEPTION(saveArchive.Abort());
		saveArchive.Abort();
#ifdef KIM_ENGINE
		theApp.LogEntry(_T("Failed to save document"));
#else
		THROW_LAST();
#endif
	}
	END_CATCH_ALL
}

void CImpManDoc::OnFileAutoBackup()
{
	TCHAR szCurrentDir[_MAX_PATH];
	DWORD nBufferLength = _MAX_PATH;

	if ( ! GetCurrentDirectory(nBufferLength, szCurrentDir))
		return;
	if ( ! theApp.SetMyCurrentDirectory(theApp.settings.m_szTempDir, FALSE))
		return;

	CString strPathName;

	strPathName  = theApp.settings.m_szTempDir;
	strPathName += "\\AutoBackup";

	//CString strBaseName, strExt;
	//CDocTemplate* pDocTemplate = GetDocTemplate();
	//if (!pDocTemplate->GetDocString(strBaseName, CDocTemplate::docName)
	//	|| !pDocTemplate->GetDocString(strExt, CDocTemplate::filterExt))
	//{
	//	AfxThrowUserException(); // These doc template strings will
	//							 // be available if you created the application using AppWizard
	//							 // and specified the file extension as an option for
	//							 // the document class produced by AppWizard.
	//}

	strPathName += _T(".job");//strExt;
/////////////////////////////////////////////////////////////////////////////
//	Look, if an existing autobackup.job-file is opened
	BOOL bStoreOK;
	CFile file;
	CFileException fileException;
	WIN32_FIND_DATA findData;
	HANDLE hfileSearch = FindFirstFile(strPathName, &findData);
	if (hfileSearch != INVALID_HANDLE_VALUE)
	{
	// we found an autobackup.job -> now look if we can open the file for writing.
	//								 If open fails the file is still open in our program
		if (file.Open(strPathName, CFile::modeWrite, &fileException))
		{
			bStoreOK = TRUE;
			file.Close();
		}
		else
			bStoreOK = FALSE;
	}
	else
		bStoreOK = TRUE;

	FindClose(hfileSearch);

	if (bStoreOK)
	{
	//	Now it's safe to store the backup-file
	//	If you work seperate with "OnSaveDocument" the saved file stays open !!!
		BOOL bTemp;
		bTemp = m_bRemember;
		LPSTORAGE lpOrigStg = m_lpRootStg;
		m_lpRootStg = NULL; // ignore embedded storage for now
		TRY
		{
			m_bRemember = FALSE;
			COleDocument::OnSaveDocument(strPathName);
		}
		END_TRY
		m_lpRootStg = lpOrigStg;
		m_bRemember = bTemp;
	}

	theApp.SetMyCurrentDirectory(szCurrentDir, TRUE);
}

BOOL CImpManDoc::SaveModified() 
{
	if (m_strPathName.IsEmpty())	// Only changeDirectory for a new file without path-information
		theApp.SetMyCurrentDirectory(theApp.settings.m_szJobsDir, TRUE);

	BOOL ret;
	CString strFileString;
	ret = COleDocument::SaveModified();

	theApp.SetMyCurrentDirectory(theApp.settings.m_szWorkDir, TRUE);

	return ret;
}

CPrintSheet* CImpManDoc::GetActPrintSheet() 
{
	CFrameWnd* pFrame = ((CMainFrame*)theApp.m_pMainWnd)->GetActiveFrame();
	if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
		return ((CNewPrintSheetFrame*)pFrame)->GetActPrintSheet();

	return (NULL);
}

CLayout* CImpManDoc::GetActLayout() 
{
	CPrintSheet* pActPrintSheet = GetActPrintSheet();

	if (pActPrintSheet)
		return (pActPrintSheet->GetLayout());
	else
		return (NULL);
}

CPressDevice* CImpManDoc::GetActPressDevice(int nSide/*= 0*/)
{
	CLayout* pActLayout = GetActLayout();

	if (pActLayout)
	{
		switch (nSide)
		{
			case FRONTSIDE : return (pActLayout->m_FrontSide.GetPressDevice());
			case BACKSIDE  : return (pActLayout->m_BackSide.GetPressDevice());
			default		   : switch (GetActSight())
							 {
								case BOTHSIDES :
								case FRONTSIDE : return (pActLayout->m_FrontSide.GetPressDevice());
								case BACKSIDE  : return (pActLayout->m_BackSide.GetPressDevice());
								default		   : return (NULL);
							 }
		}
	}
	else
		return (NULL);
}

int CImpManDoc::GetActSight()
{
	CFrameWnd* pFrame = GetFrame(RUNTIME_CLASS(CNewPrintSheetFrame));
	if (pFrame)
		return ((CNewPrintSheetFrame*)pFrame)->GetActSight();

	pFrame = ((CMainFrame*)theApp.m_pMainWnd)->GetActiveFrame();
	if (pFrame)
		if (pFrame->IsKindOf(RUNTIME_CLASS(CNewPrintSheetFrame)))
			return ((CNewPrintSheetFrame*)pFrame)->GetActSight();

	return BOTHSIDES;
}

CPlate* CImpManDoc::GetActPlate()
{
	CPrintSheet* pActPrintSheet = GetActPrintSheet();
	if (!pActPrintSheet)
		return NULL;

	int nActSight = GetActSight();
	if (nActSight == -1)
		return NULL;

	CPlateList* pPlateList;
	switch (nActSight)
	{
	case FRONTSIDE: pPlateList = &pActPrintSheet->m_FrontSidePlates; break;
	case BACKSIDE:  pPlateList = &pActPrintSheet->m_BackSidePlates;	 break;
	default:		return NULL;
	}

	if ((m_nActPlate < 0) || (m_nActPlate >= pPlateList->GetCount()))
		return NULL;

	POSITION pos = pPlateList->FindIndex(m_nActPlate);
	if (!pos)
		return NULL;
	else
		return &pPlateList->GetAt(pos);
}

CFrameWnd* CImpManDoc::GetFrame(const CRuntimeClass* pClass)
{
	CWnd *pPrevWnd, *pNextWnd;
	for (pPrevWnd = theApp.m_pMainWnd; (pNextWnd = pPrevWnd->GetWindow(GW_HWNDPREV)) != NULL; pPrevWnd = pNextWnd)
    {
		if (pNextWnd->IsKindOf(pClass))
			return (CFrameWnd*)pNextWnd;
    }

	return NULL;
}

void CImpManDoc::RemovePlacedProducts()
{
	POSITION pos = m_PageTemplateList.GetHeadPosition();
	while (pos)
	{
		CPageTemplate& rTemplate = m_PageTemplateList.CList::GetNext(pos);
		rTemplate.m_bPageNumLocked = TRUE;
	}
	pos = m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = m_Bookblock.m_FoldSheetList.GetNext(pos);
		rFoldSheet.m_bFoldSheetNumLocked = TRUE;
	}

	int nNumRemoved = 0;

	for (int nComponentGroupIndex = 0; nComponentGroupIndex < m_printComponentsList.GetSize(); nComponentGroupIndex++)
	{
		CPrintComponentsGroup& rComponentGroup = m_printComponentsList[nComponentGroupIndex];

		for (int nComponentIndex = 0; nComponentIndex < rComponentGroup.m_printComponents.GetSize(); nComponentIndex++)
		{
			CPrintComponent& rComponent = rComponentGroup.m_printComponents[nComponentIndex];

			if (rComponent.m_nType == CPrintComponent::PageSection)
			{
			}
			else
			if (rComponent.m_nType == CPrintComponent::FoldSheet)
			{
				if (rComponent.IsPlaced())
				{
					CFoldSheet* pFoldSheet = m_Bookblock.GetFoldSheetByIndex(rComponent.m_nFoldSheetIndex);
					if (pFoldSheet)
					{
						CFoldSheetLayer* pLayer = pFoldSheet->GetLayer(rComponent.m_nFoldSheetLayerIndex);
						if (pLayer)
						{
							int nPagesRemoved = 0;
							for (int i = 0; i < pLayer->m_FrontPageArray.GetSize(); i++)
							{
								POSITION pos = m_PageTemplateList.FindIndex(pLayer->m_FrontPageArray[i] - 1 - nPagesRemoved, pFoldSheet->m_nProductPartIndex);
								if (pos)
								{
									m_PageTemplateList.RemoveAt(pos);
									nPagesRemoved++;
								}
							}
							for (int i = 0; i < pLayer->m_BackPageArray.GetSize(); i++)
							{
								POSITION pos = m_PageTemplateList.FindIndex(pLayer->m_BackPageArray[i] - 1 - nPagesRemoved, pFoldSheet->m_nProductPartIndex);
								if (pos)
								{
									m_PageTemplateList.RemoveAt(pos);
									nPagesRemoved++;
								}
							}
							m_Bookblock.RemoveFoldSheet(rComponent.m_nFoldSheetIndex);
							CBoundProductPart& rProductPart  = m_boundProducts.GetProductPart(rComponent.m_nProductPartIndex);
							rProductPart.m_nNumPages -= pFoldSheet->GetNumPages();;
						}
					}
				}
			}
			else
			{
				int nIndex = rComponent.m_nFlatProductIndex - nNumRemoved;
				CFlatProduct& rFlatProduct = m_flatProducts.FindByIndex(nIndex);
				if (rComponent.IsPlaced())
				{
					POSITION pos = m_flatProducts.FindIndex(nIndex);
					if (pos)
					{
						m_flatProducts.RemoveAt(pos);
						m_PrintSheetList.RemoveFlatProductType(rComponent.m_nFlatProductIndex);
						POSITION pos1 = m_PageTemplateList.FindLabelPos(nIndex, FRONTSIDE);
						POSITION pos2 = m_PageTemplateList.FindLabelPos(nIndex, BACKSIDE);
						m_PageTemplateList.RemoveAt(pos1);
						m_PageTemplateList.RemoveAt(pos2);
						nNumRemoved++;
					}
				}
			}
		}
	}

	m_Bookblock.Reorganize();
	m_PageSourceList.InitializePageTemplateRefs();
	m_PageSourceList.RemoveUnusedSources();

	nNumRemoved = 0;
	int nIndex = 0;
	while (1)
	{
		CBoundProductPart& rBoundProductPart = m_boundProducts.GetProductPart(nIndex - nNumRemoved);
		if (rBoundProductPart.IsNull())
			break;
		
		if (rBoundProductPart.m_nNumPages <= 0)
		{
//			m_boundProducts.RemoveProductPart(rBoundProductPart);

			CBoundProduct& rProduct = rBoundProductPart.GetBoundProduct();
			int nProductPartIndex = rBoundProductPart.GetIndex();
			rProduct.m_parts.RemoveAt(m_boundProducts.TransformGlobalToLocalPartIndex(nProductPartIndex));
			rProduct.ReorganizeReferences(nProductPartIndex);
			if (rProduct.m_parts.GetSize() <= 0)
			{
				POSITION pos = m_boundProducts.FindIndex(rProduct.GetIndex());
				if (pos)
				{
					m_boundProducts.RemoveAt(pos);
					nNumRemoved++;
				}
			}
		}
		nIndex++;
	}

	SetModifiedFlag();
	UpdateAllViews(NULL);
}

void CImpManDoc::RemoveUnplacedProducts()
{
}



/////////////////////////////////////////////////////////////////////////////
// CZoomState ///////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CZoomState, CObject, VERSIONABLE_SCHEMA | 1)

CZoomState::CZoomState()
{
	m_viewportSize = CSize(0, 0);
	m_scrollPos	   = CPoint(0, 0);
}

CZoomState::CZoomState(const CZoomState& rZoomState)
{
	*this = rZoomState;
}

const CZoomState& CZoomState::operator=(const CZoomState& rZoomState)
{
	m_viewportSize = rZoomState.m_viewportSize;
	m_scrollPos	   = rZoomState.m_scrollPos;

	return *this;
}

void CZoomState::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CZoomState));

	if (ar.IsStoring())
	{
		ar << m_viewportSize;
		ar << m_scrollPos;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1: ar >> m_viewportSize;
				ar >> m_scrollPos;
				break;
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CScreenLayout ////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CScreenLayout, CObject, VERSIONABLE_SCHEMA | 3)

CScreenLayout::CScreenLayout()
{
	m_bLabelsShowFrontBack = FALSE;
	m_bLabelsShowBoxShapes = TRUE;
}

void CScreenLayout::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CScreenLayout));

	if (ar.IsStoring())
	{
		m_zoomStateBookBlockView.Serialize(ar);
		ar << m_bLabelsShowFrontBack;
		ar << m_bLabelsShowBoxShapes;
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		switch (nVersion)
		{
		case 1: m_zoomStateBookBlockView.Serialize(ar);
				m_bLabelsShowFrontBack = FALSE;
				m_bLabelsShowBoxShapes = TRUE;
				break;
		case 2: m_zoomStateBookBlockView.Serialize(ar);
				ar >> m_bLabelsShowFrontBack;
				m_bLabelsShowBoxShapes = TRUE;
				break;
		case 3: m_zoomStateBookBlockView.Serialize(ar);
				ar >> m_bLabelsShowFrontBack;
				ar >> m_bLabelsShowBoxShapes;
				break;
		}
	}
}


int g_nProductID = 0;
