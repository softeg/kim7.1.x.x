// DlgMarkPositioning.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "ObjectPivotControl.h"
#include "DlgMarkPositioning.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgMarkPositioning 


CDlgMarkPositioning::CDlgMarkPositioning(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgMarkPositioning::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgMarkPositioning)
	m_fDistanceX = 0.0f;
	m_fDistanceY = 0.0f;
	//}}AFX_DATA_INIT
}


void CDlgMarkPositioning::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgMarkPositioning)
	DDX_Control(pDX, IDC_DISTANCEY_SPIN, m_distanceYSpin);
	DDX_Control(pDX, IDC_DISTANCEX_SPIN, m_distanceXSpin);
	DDX_Measure(pDX, IDC_MARK_DISTANCEX, m_fDistanceX);
	DDX_Measure(pDX, IDC_MARK_DISTANCEY, m_fDistanceY);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgMarkPositioning, CDialog)
	//{{AFX_MSG_MAP(CDlgMarkPositioning)
	ON_WM_MOUSEMOVE()
	ON_NOTIFY(UDN_DELTAPOS, IDC_DISTANCEX_SPIN, OnDeltaposDistancexSpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_DISTANCEY_SPIN, OnDeltaposDistanceySpin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgMarkPositioning 

BOOL CDlgMarkPositioning::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_markObjectPivotCtrl.SubclassDlgItem(IDC_MEDIA_PIVOT, this);
//	m_markObjectPivotCtrl.m_pParent = this;

	m_distanceXSpin.SetRange(0, 1);  
	m_distanceYSpin.SetRange(0, 1);  

	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_MARK_DISTANCEX);
	pEdit->SetSel(0, -1);

	//InitReflinePosParams();

	UpdateData(FALSE);

	m_markObjectPivotCtrl.m_nActiveCell = -1;
	m_markObjectPivotCtrl.Update();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CDlgMarkPositioning::OnMouseMove(UINT nFlags, CPoint point) 
{
	m_markObjectPivotCtrl.OnMouseMove(nFlags, point);

	CDialog::OnMouseMove(nFlags, point);
}

void CDlgMarkPositioning::OnDeltaposDistancexSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_distanceXSpin.GetBuddy(), pNMUpDown->iDelta);
	
	*pResult = 1;
}

void CDlgMarkPositioning::OnDeltaposDistanceySpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	SpinMeasure(m_distanceYSpin.GetBuddy(), pNMUpDown->iDelta);
	
	*pResult = 1;
}
