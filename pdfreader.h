#if !defined(AFX_PDFREADER_H__6E403891_A89C_11D3_877D_0040F68C1EF7__INCLUDED_)
#define AFX_PDFREADER_H__6E403891_A89C_11D3_877D_0040F68C1EF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Mit Microsoft Visual C++ automatisch erstellte IDispatch-Kapselungsklasse(n). 

// HINWEIS: Die Inhalte dieser Datei nicht �ndern. Wenn Microsoft Visual C++
// diese Klasse erneuert, werden Ihre �nderungen �berschrieben.

/////////////////////////////////////////////////////////////////////////////
// Wrapper-Klasse CPdfReader 

class CPdfReader : public CWnd
{
protected:
	DECLARE_DYNCREATE(CPdfReader)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0xca8a9780, 0x280d, 0x11cf, { 0xa2, 0x4d, 0x44, 0x45, 0x53, 0x54, 0x0, 0x0 } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName,
		LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect,
		CWnd* pParentWnd, UINT nID,
		CCreateContext* pContext = NULL)
	{	pContext = pContext; /*supress unref warnings */ 
		lpszClassName = lpszClassName;
		return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); }

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect, CWnd* pParentWnd, UINT nID,
		CFile* pPersist = NULL, BOOL bStorage = FALSE,
		BSTR bstrLicKey = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); }

// Attribute
public:
	CString GetSrc();
	void SetSrc(LPCTSTR);

// Operationen
public:
	BOOL LoadFile(LPCTSTR fileName);
	void setShowToolbar(BOOL On);
	void gotoFirstPage();
	void gotoLastPage();
	void gotoNextPage();
	void gotoPreviousPage();
	void setCurrentPage(long n);
	void goForwardStack();
	void goBackwardStack();
	void setPageMode(LPCTSTR pageMode);
	void setLayoutMode(LPCTSTR layoutMode);
	void setNamedDest(LPCTSTR namedDest);
	void Print();
	void printWithDialog();
	void setZoom(float percent);
	void setZoomScroll(float percent, float left, float top);
	void setView(LPCTSTR viewMode);
	void setViewScroll(LPCTSTR viewMode, float offset);
	void setViewRect(float left, float top, float width, float height);
	void printPages(long from, long to);
	void printPagesFit(long from, long to, BOOL shrinkToFit);
	void printAll();
	void printAllFit(BOOL shrinkToFit);
	void setShowScrollbars(BOOL On);
	void AboutBox();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f�gt unmittelbar vor der vorhergehenden Zeile zus�tzliche Deklarationen ein.

#endif // AFX_PDFREADER_H__6E403891_A89C_11D3_877D_0040F68C1EF7__INCLUDED_
