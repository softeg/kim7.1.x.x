#pragma once


// CDlgRemoveControlMarks-Dialogfeld

class CDlgRemoveControlMarks : public CDialog
{
	DECLARE_DYNAMIC(CDlgRemoveControlMarks)

public:
	CDlgRemoveControlMarks(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgRemoveControlMarks();

// Dialogfelddaten
	enum { IDD = IDD_REMOVE_CONTROLMARKS};

public:
	CString m_strSelectedMarkName;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRemoveSelectedMarks();
	virtual BOOL OnInitDialog();
};
