#pragma once

#include "OwnerDrawnButtonList.h"
#include "OrderDataFrame.h"


// CProductNavigationView

class CProductNavigationView : public CFormView
{
	DECLARE_DYNCREATE(CProductNavigationView)

	DECLARE_DISPLAY_LIST(CProductNavigationView, OnMultiSelect, NO, OnStatusChanged, NO)

public:
	CProductNavigationView();
	virtual ~CProductNavigationView();

public:
	enum { IDD = IDD_PRODUCTNAVIGATIONVIEW };


public:
	CStatic					m_productTypeSelectorFrame;
	COwnerDrawnButtonList	m_productTypeSelectorButtons;
	CStatic					m_productDataSelectorFrame;
	COwnerDrawnButtonList	m_productDataSelectorButtons;
	CStatic					m_printSheetPlanningSelectorFrame;
	COwnerDrawnButtonList	m_printSheetPlanningSelectorButtons;


public:
	void		DrawBkgndProductDataPane(CDC* pDC);
	void		DrawBkgndPlanningPane(CDC* pDC);
	static void DrawItemProductTypeSelector(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void DrawItemProductDataSelector(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void DrawItemPrintSheetPlanningSelector(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState);
	static void UpdateBackgroundProductTypeSelector(CDC* pDC, CRect rcRect, CWnd* pWnd);
	static void UpdateBackgroundProductDataSelector(CDC* pDC, CRect rcRect, CWnd* pWnd);
	static void UpdateBackgroundPrintSheetPlanningSelector(CDC* pDC, CRect rcRect, CWnd* pWnd);


public:
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void OnInitialUpdate();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelectProductType();
	afx_msg void OnSelectProductData();
	afx_msg void OnSelectPlanningView();
protected:
	virtual void OnDraw(CDC* /*pDC*/);
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
};


