// ObjectPivotControl.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "ObjectPivotControl.h"
#include "DlgMoveObjects.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CObjectPivotControl

CObjectPivotControl::CObjectPivotControl()
{
	m_bmObjectPivot1.LoadBitmap(IDB_MEDIA_PIVOT1); 
	m_bmObjectPivot2.LoadBitmap(IDB_MEDIA_PIVOT2); 
	m_bmObjectPivot3.LoadBitmap(IDB_MEDIA_PIVOT3); 
	m_bmObjectPivot4.LoadBitmap(IDB_MEDIA_PIVOT4); 
	m_bmObjectPivot5.LoadBitmap(IDB_MEDIA_PIVOT5); 
	m_bmObjectPivot6.LoadBitmap(IDB_MEDIA_PIVOT6); 
	m_bmObjectPivot7.LoadBitmap(IDB_MEDIA_PIVOT7); 
	m_bmObjectPivot8.LoadBitmap(IDB_MEDIA_PIVOT8); 
	m_bmObjectPivot9.LoadBitmap(IDB_MEDIA_PIVOT9); 

	m_bmObjectPivoth1.LoadBitmap(IDB_MARK_PIVOTH1); 
	m_bmObjectPivoth2.LoadBitmap(IDB_MARK_PIVOTH2); 
	m_bmObjectPivoth3.LoadBitmap(IDB_MARK_PIVOTH3); 
	m_bmObjectPivotv1.LoadBitmap(IDB_MARK_PIVOTV1); 
	m_bmObjectPivotv2.LoadBitmap(IDB_MARK_PIVOTV2); 
	m_bmObjectPivotv3.LoadBitmap(IDB_MARK_PIVOTV3); 

	m_bLockCheckButton = FALSE;
	m_nXRef		  = XCENTER;
	m_nYRef		  = YCENTER;
	m_nCurNX	  = -1;
	m_nCurNY	  = -1;
	m_nActiveCell = -1;
	m_bHorizontalOnly = FALSE;
	m_bVerticalOnly	  = FALSE;
}

CObjectPivotControl::~CObjectPivotControl()
{
	m_bmObjectPivot1.DeleteObject(); 
	m_bmObjectPivot2.DeleteObject(); 
	m_bmObjectPivot3.DeleteObject(); 
	m_bmObjectPivot4.DeleteObject(); 
	m_bmObjectPivot5.DeleteObject(); 
	m_bmObjectPivot6.DeleteObject(); 
	m_bmObjectPivot7.DeleteObject(); 
	m_bmObjectPivot8.DeleteObject(); 
	m_bmObjectPivot9.DeleteObject(); 

	m_bmObjectPivoth1.DeleteObject(); 
	m_bmObjectPivoth2.DeleteObject(); 
	m_bmObjectPivoth3.DeleteObject(); 
	m_bmObjectPivotv1.DeleteObject(); 
	m_bmObjectPivotv2.DeleteObject(); 
	m_bmObjectPivotv3.DeleteObject(); 
}


BEGIN_MESSAGE_MAP(CObjectPivotControl, CStatic)
	//{{AFX_MSG_MAP(CObjectPivotControl)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CObjectPivotControl 

void CObjectPivotControl::OnMouseMove(UINT nFlags, CPoint point) 
{
	CPoint ptMouse;
	GetCursorPos(&ptMouse);
	ScreenToClient(&ptMouse);

	CRect rectPivot;
	GetClientRect(rectPivot);
	if ( ! rectPivot.PtInRect(ptMouse))
	{
		Update();
		m_nCurNX = -1; m_nCurNY = -1;
		return;
	}

	int nDX = rectPivot.Width() /3;
	int nDY = rectPivot.Height()/3;
	int nNX = (ptMouse.x < rectPivot.left + nDX) ? 0 : ((ptMouse.x < rectPivot.left + 2*nDX) ? 1 : 2);
	int nNY = (ptMouse.y < rectPivot.top  + nDY) ? 0 : ((ptMouse.y < rectPivot.top  + 2*nDY) ? 1 : 2);

	CRect rectButton  = rectPivot;
	rectButton.right  = rectButton.left + rectPivot.Width() / 3;
	rectButton.bottom = rectButton.top  + rectPivot.Height()/ 3;
	rectButton.OffsetRect(nNX * rectButton.Width(), nNY * rectButton.Height());	
	rectButton.DeflateRect(3, 3);

	if ( (nNX != m_nCurNX) || (nNY != m_nCurNY) )
	{
		m_bLockCheckButton = TRUE;

		ChooseBitmap(GetCurrentCell());
		Invalidate();
		UpdateWindow();

		rectButton = ConsolidateButtonRect(rectButton, rectPivot);

		CClientDC dc(this);
		dc.Draw3dRect(rectButton, WHITE/*RGB(231,231,214)*/, DARKGRAY);

		m_bLockCheckButton = FALSE;

		m_nCurNX = nNX; m_nCurNY = nNY;

		m_nActiveCell = -1;
	}

	CStatic::OnMouseMove(nFlags, point);
}

int CObjectPivotControl::GetCurrentCell()
{
	if ( ! m_pParent)
		return 0;

	int nXRef = m_nXRef;
	int nYRef = m_nYRef;
	int nCell = 1;
	switch (nYRef)
	{
	case TOP:		switch (nXRef)
					{
					case LEFT:		nCell = 1; break;
					case XCENTER:	nCell = 2; break;
					case RIGHT:		nCell = 3; break;
					}
					break;
	case YCENTER:	switch (nXRef)
					{
					case LEFT:		nCell = 4; break;
					case XCENTER:	nCell = 5; break;
					case RIGHT:		nCell = 6; break;
					}
					break;
	case BOTTOM:	switch (nXRef)
					{
					case LEFT:		nCell = 7; break;
					case XCENTER:	nCell = 8; break;
					case RIGHT:		nCell = 9; break;
					}
					break;
	}

	return nCell;
}

void CObjectPivotControl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CPoint ptMouse;
	GetCursorPos(&ptMouse);

	CRect rectPivot;
	GetWindowRect(rectPivot);

	int nDX = rectPivot.Width() /3;
	int nDY = rectPivot.Height()/3;
	int nNX = (ptMouse.x < rectPivot.left + nDX) ? 1 : ((ptMouse.x < rectPivot.left + 2*nDX) ? 2 : 3);
	int nNY = (ptMouse.y < rectPivot.top  + nDY) ? 1 : ((ptMouse.y < rectPivot.top  + 2*nDY) ? 2 : 3);
	int nCell = (nNY - 1) * 3 + nNX;

	int nXRef = LEFT;
	int nYRef = TOP;
	switch (nCell)
	{
	case 1:	nXRef = LEFT;		nYRef = TOP;		break;
	case 2:	nXRef = XCENTER;	nYRef = TOP;		break;
	case 3:	nXRef = RIGHT;		nYRef = TOP;		break;
	case 4:	nXRef = LEFT;		nYRef = YCENTER;	break;
	case 5:	nXRef = XCENTER;	nYRef = YCENTER;	break;
	case 6:	nXRef = RIGHT;		nYRef = YCENTER;	break;
	case 7:	nXRef = LEFT;		nYRef = BOTTOM;		break;
	case 8:	nXRef = XCENTER;	nYRef = BOTTOM;		break;
	case 9:	nXRef = RIGHT;		nYRef = BOTTOM;		break;
	default:return;
	}

	m_nXRef = nXRef;
	m_nYRef = nYRef;

	ChooseBitmap(nCell);	// forces OnPaint() to be called, which then calls CheckButton()

	m_pParent->SendMessage(WM_COMMAND, ID_CHANGED_PIVOT);

	CStatic::OnLButtonDown(nFlags, point);
}

void CObjectPivotControl::LoadData(unsigned char nXRef, unsigned char nYRef, BOOL bHorizontalOnly, BOOL bVerticalOnly)
{
	m_nXRef = nXRef;
	m_nYRef = nYRef;
	m_bHorizontalOnly = bHorizontalOnly;
	m_bVerticalOnly	  = bVerticalOnly;
	Update();
}

void CObjectPivotControl::SaveData(unsigned char& nXRef, unsigned char& nYRef)
{
	nXRef = m_nXRef;
	nYRef = m_nYRef;
}

void CObjectPivotControl::ChooseBitmap(int nCell)
{
	if (m_bVerticalOnly)
	{
		switch (nCell)
		{
		case 1: case 2: case 3:	SetBitmap((HBITMAP)m_bmObjectPivoth3); break;
		case 4: case 5: case 6: SetBitmap((HBITMAP)m_bmObjectPivoth2); break;
		case 7: case 8: case 9: SetBitmap((HBITMAP)m_bmObjectPivoth1); break;
		}
	}
	else
	if (m_bHorizontalOnly)
	{
		switch (nCell)
		{
		case 1: case 4: case 7:	SetBitmap((HBITMAP)m_bmObjectPivotv1); break;
		case 2: case 5: case 8: SetBitmap((HBITMAP)m_bmObjectPivotv2); break;
		case 3: case 6: case 9: SetBitmap((HBITMAP)m_bmObjectPivotv3); break;
		}
	}
	else
	{
		switch (nCell)
		{
		case 1:		SetBitmap((HBITMAP)m_bmObjectPivot1);  break;
		case 2:		SetBitmap((HBITMAP)m_bmObjectPivot2);  break;
		case 3:		SetBitmap((HBITMAP)m_bmObjectPivot3);  break;
		case 4:		SetBitmap((HBITMAP)m_bmObjectPivot4);  break;
		case 5:		SetBitmap((HBITMAP)m_bmObjectPivot5);  break;
		case 6:		SetBitmap((HBITMAP)m_bmObjectPivot6);  break;
		case 7:		SetBitmap((HBITMAP)m_bmObjectPivot7);  break;
		case 8:		SetBitmap((HBITMAP)m_bmObjectPivot8);  break;
		case 9:		SetBitmap((HBITMAP)m_bmObjectPivot9);  break;
		}
	}
}

void CObjectPivotControl::CheckButton(int nCell)
{
	//if (m_bLockCheckButton)
	//	return;

	//CRect rectPivot;
	//GetClientRect(rectPivot);
	//CRect rectButton  = rectPivot;
	//rectButton.right  = rectButton.left + rectPivot.Width() / 3;
	//rectButton.bottom = rectButton.top  + rectPivot.Height()/ 3;

	//int nNX	  = 0;
	//int nNY   = 0;
	//switch (nCell)
	//{
	//case 1:	nNX = 0; nNY = 0; break;
	//case 2:	nNX = 1; nNY = 0; break;
	//case 3:	nNX = 2; nNY = 0; break;
	//case 4:	nNX = 0; nNY = 1; break;
	//case 5:	nNX = 1; nNY = 1; break;
	//case 6:	nNX = 2; nNY = 1; break;
	//case 7:	nNX = 0; nNY = 2; break;
	//case 8:	nNX = 1; nNY = 2; break;
	//case 9:	nNX = 2; nNY = 2; break;
	//default:return;
	//}

	//rectButton.OffsetRect(nNX * rectButton.Width(), nNY * rectButton.Height());	
	//rectButton.DeflateRect(3, 3);

	//rectButton = ConsolidateButtonRect(rectButton, rectPivot);

	//CClientDC dc(this);
	//dc.Draw3dRect(rectButton, DARKGRAY, RGB(231,231,214));

	//m_nActiveCell = nCell;
}

void CObjectPivotControl::Update()
{
	int nCell = GetCurrentCell();
	if (nCell != m_nActiveCell)
	{
		ChooseBitmap(nCell);
		CheckButton(nCell);
	}
}

void CObjectPivotControl::OnPaint() 
{
	CStatic::OnPaint();		// to draw selected bitmap

	CheckButton(GetCurrentCell());
}

void CObjectPivotControl::Reinit(int nIDD)
{
	m_bmObjectPivot1.DeleteObject(); 
	m_bmObjectPivot2.DeleteObject(); 
	m_bmObjectPivot3.DeleteObject(); 
	m_bmObjectPivot4.DeleteObject(); 
	m_bmObjectPivot5.DeleteObject(); 
	m_bmObjectPivot6.DeleteObject(); 
	m_bmObjectPivot7.DeleteObject(); 
	m_bmObjectPivot8.DeleteObject(); 
	m_bmObjectPivot9.DeleteObject(); 

	m_bmObjectPivoth1.DeleteObject(); 
	m_bmObjectPivoth2.DeleteObject(); 
	m_bmObjectPivoth3.DeleteObject(); 
	m_bmObjectPivotv1.DeleteObject(); 
	m_bmObjectPivotv2.DeleteObject(); 
	m_bmObjectPivotv3.DeleteObject(); 

	switch (nIDD)
	{
	case IDD_MOVE_OBJECTS:				m_bmObjectPivot1.LoadBitmap(IDB_MEDIA_PIVOT1); 
										m_bmObjectPivot2.LoadBitmap(IDB_MEDIA_PIVOT2); 
										m_bmObjectPivot3.LoadBitmap(IDB_MEDIA_PIVOT3); 
										m_bmObjectPivot4.LoadBitmap(IDB_MEDIA_PIVOT4); 
										m_bmObjectPivot5.LoadBitmap(IDB_MEDIA_PIVOT5); 
										m_bmObjectPivot6.LoadBitmap(IDB_MEDIA_PIVOT6); 
										m_bmObjectPivot7.LoadBitmap(IDB_MEDIA_PIVOT7); 
										m_bmObjectPivot8.LoadBitmap(IDB_MEDIA_PIVOT8); 
										m_bmObjectPivot9.LoadBitmap(IDB_MEDIA_PIVOT9); 
										break;
	case IDD_LAYOUTOBJECT_GEOMETRY:		m_bmObjectPivot1.LoadBitmap(IDB_MARK_PIVOT1); 
										m_bmObjectPivot2.LoadBitmap(IDB_MARK_PIVOT2); 
										m_bmObjectPivot3.LoadBitmap(IDB_MARK_PIVOT3); 
										m_bmObjectPivot4.LoadBitmap(IDB_MARK_PIVOT4); 
										m_bmObjectPivot5.LoadBitmap(IDB_MARK_PIVOT5); 
										m_bmObjectPivot6.LoadBitmap(IDB_MARK_PIVOT6); 
										m_bmObjectPivot7.LoadBitmap(IDB_MARK_PIVOT7); 
										m_bmObjectPivot8.LoadBitmap(IDB_MARK_PIVOT8); 
										m_bmObjectPivot9.LoadBitmap(IDB_MARK_PIVOT9); 
										m_bmObjectPivoth1.LoadBitmap(IDB_MARK_PIVOTH1); 
										m_bmObjectPivoth2.LoadBitmap(IDB_MARK_PIVOTH2); 
										m_bmObjectPivoth3.LoadBitmap(IDB_MARK_PIVOTH3); 
										m_bmObjectPivotv1.LoadBitmap(IDB_MARK_PIVOTV1); 
										m_bmObjectPivotv2.LoadBitmap(IDB_MARK_PIVOTV2); 
										m_bmObjectPivotv3.LoadBitmap(IDB_MARK_PIVOTV3); 
										break;
	case IDD_PDFOBJECT_CONTENT:			m_bmObjectPivot1.LoadBitmap(IDB_CONTENT_PIVOT1); 
										m_bmObjectPivot2.LoadBitmap(IDB_CONTENT_PIVOT2);
										m_bmObjectPivot3.LoadBitmap(IDB_CONTENT_PIVOT3);
										m_bmObjectPivot4.LoadBitmap(IDB_CONTENT_PIVOT4);
										m_bmObjectPivot5.LoadBitmap(IDB_CONTENT_PIVOT5);
										m_bmObjectPivot6.LoadBitmap(IDB_CONTENT_PIVOT6);
										m_bmObjectPivot7.LoadBitmap(IDB_CONTENT_PIVOT7);
										m_bmObjectPivot8.LoadBitmap(IDB_CONTENT_PIVOT8);
										m_bmObjectPivot9.LoadBitmap(IDB_CONTENT_PIVOT9);
										break;
	}
}

CRect CObjectPivotControl::ConsolidateButtonRect(CRect rectButton, CRect rectPivot)
{
	if (m_bHorizontalOnly)	
	{
		rectButton.bottom = rectPivot.bottom; rectButton.top = rectPivot.top;
		rectButton.DeflateRect(0, 3);
	}
	if (m_bVerticalOnly)	
	{
		rectButton.left = rectPivot.left; rectButton.right = rectPivot.right;
		rectButton.DeflateRect(3, 0);
	}

	return rectButton;
}
