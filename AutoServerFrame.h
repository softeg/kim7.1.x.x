#pragma once

#include "AutoServerJobsView.h"
#include "AutoServerWorkflowsView.h"
#include "AutoServerLogsView.h"
#include "AutoServerDetailsView.h"


class CAutoServerSplitter : public CSplitterWnd
{
    DECLARE_DYNAMIC(CAutoServerSplitter)
public:
	CAutoServerSplitter() { }

friend class CAutoServerFrame;

protected:
	virtual void OnDrawSplitter(CDC* pDC, ESplitType nType, const CRect& rect);
	virtual void StopTracking(BOOL bAccept);
	virtual void TrackColumnSize(int x, int col);

    DECLARE_MESSAGE_MAP()
    afx_msg LRESULT OnNcHitTest(CPoint point);
	void OnSize(UINT nType, int cx, int cy);
};


// CAutoServerFrame-Rahmen

class CAutoServerFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CAutoServerFrame)
protected:
	CAutoServerFrame();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CAutoServerFrame();

public:
	CAutoServerSplitter m_wndSplitter, m_wndSplitter1, m_wndSplitter2;


enum currentView {ViewJobs, ViewWorkflows, ViewLogs};


protected:
	BOOL						m_bStatusRestored;
public:
	int							m_nJobViewWidth;
	int							m_nCurrentView;

public:
	BOOL						SetupViewJobs();
	BOOL						SetupViewWorkflows();
	BOOL						SetupViewLogs();
	void						RearrangeSplitters();
	int							GetActJobID();
	CAutoServerJobsView*		GetAutoServerJobsView();
	CAutoServerWorkflowsView*	GetAutoServerWorkflowsView();
	CAutoServerDetailsView*		GetAutoServerDetailsView();
	CAutoServerLogsView*		GetAutoServerLogsView();
	CAutoServerToolsView*		GetAutoServerToolsView();
	void						UpdateJobViews(int nJobID);


protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void ActivateFrame(int nCmdShow = -1);
	afx_msg void OnDestroy();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};


