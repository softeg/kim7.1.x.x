#if !defined(AFX_PRINTSHEETMARKSVIEW_H__B9445481_74C6_11D3_872E_204C4F4F5020__INCLUDED_)
#define AFX_PRINTSHEETMARKSVIEW_H__B9445481_74C6_11D3_872E_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintSheetMarksView.h : Header-Datei
//



/////////////////////////////////////////////////////////////////////////////
// Formularansicht CPrintSheetMarksView 

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CPrintSheetMarksView : public CFormView
{
protected:
	CPrintSheetMarksView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	DECLARE_DYNCREATE(CPrintSheetMarksView)

public:
	CSize						m_sizeViewport;
	CBrush*						m_pBkBrush;
	CBrush*						m_pHeaderBrush;
	CString						m_strHeaderText;
	CToolTipCtrl*				m_pToolTip;
	CImageList*					m_pMarkSetImageList;
	CImageList*					m_pMarkImageList;
	CString						m_strMarkSetName;
	CArray <CSheetObjectRefs, 
			CSheetObjectRefs&>	m_objectList;
	CString						m_strMarkName;
	int							m_nCurObjectSel;
	CLayout*					m_pCurrentLayout;
	CDlgSelectNewMark			m_dlgSelectNewMark;
	CDlgLayoutObjectControl		m_dlgLayoutObjectControl;
	CDlgObjectPreviewControl		m_dlgObjectPreviewControl;


protected:
	CArray <SEPARATIONINFO, SEPARATIONINFO>	m_ColorSeparations;			




// Formulardaten
public:
	//{{AFX_DATA(CPrintSheetMarksView)
	enum { IDD = IDD_MARKS };
	CListCtrl	m_objectListCtrl;
	BOOL	m_bOpenMarklib;
	//}}AFX_DATA

// Attribute
public:

// Operationen
public:
	CImpManDoc* GetDocument();

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPrintSheetMarksView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void PostNcDestroy();
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

friend class CPrintSheetView;		// access to OnClickObjectList()
friend class CPrintSheetFrame;		// access to MarkSetsSaveModified()
friend class CImpManApp;			// access to MarkSetsSaveModified()
friend class CMarkPivotControl;		// access to UpdateSheetPreview()


// Implementierung
protected:
	virtual ~CPrintSheetMarksView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CString			GetMarksetFolder();
	CString			GetTempMarksetFolder();
	CString			GetMarksetFilename(const CString& strName);
	CString			GetTempMarksetFilename(const CString& strName);
public:
	CLayout*		GetLayout();
	CPrintSheet*	GetPrintSheet();
	CLayoutObject*	GetCurrentLayoutObject();
	void			InitObjectList(CLayoutObject* pLayoutObject = NULL);
	void			GetBoxes(CRect& outBBox, CRect& trimBox);
	void			UpdateSheetPreview(BOOL bUpdateMarkOnly = FALSE);
	void			PDFEngineDataExchange(CPageSource* pMarkSource);
	static long		GetOptPreviewResolution(CPageSource& rMarkSource);
	BOOL			IsSelected(CLayoutObject* pObject);
	void			AddNewMark(int nMarkType);
	CString			FindFreeMarkName(CString strNewMark);

protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CPrintSheetMarksView)
	afx_msg void OnMarkDelete();
	afx_msg void OnMarkNew();
	afx_msg void OnMarkDuplicate();
	afx_msg void OnCloseMarksView();
	afx_msg void OnEndlabeleditObjectList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDestroy();
	afx_msg void OnItemchangedObjectList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickObjectList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnOpenMarklib();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnApplyButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PrintSheetMarksView.cpp
inline CImpManDoc* CPrintSheetMarksView::GetDocument()
   { return (CImpManDoc*)m_pDocument; }
#endif
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_PRINTSHEETMARKSVIEW_H__B9445481_74C6_11D3_872E_204C4F4F5020__INCLUDED_
