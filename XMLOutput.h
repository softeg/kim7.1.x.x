#pragma once

//#define PROJ_JDFTOOLSLIB
#include <jdf/util/PlatformUtils.h>
#include "JDF.h"
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/dom/DOMElement.hpp>

//using namespace std;
//using namespace JDF;
using namespace XERCES_CPP_NAMESPACE;


class CXMLOutput
{
public:
	CXMLOutput(void);
	~CXMLOutput(void);


public:
	XERCES_CPP_NAMESPACE::DOMDocument* m_domDoc;


public:
	BOOL UpdateXML(const CString& strFilePath);

	BOOL ProcessJobNode(DOMNode* jobNode);
	//BOOL ProcessJobDataSectionNode(DOMNode* jobDataSectionNode);
	//BOOL ProcessOrderNode(DOMNode* orderNode);
	//BOOL ProcessCustomerNode(DOMNode* customerNode);
	//BOOL ProcessResourceSectionNode(DOMNode* resourceSectionNode);
	//BOOL ProcessPaperResourceNode(DOMNode* paperNode);
	//BOOL ProcessPrepressNode(DOMNode* prepressProcessNode);
	//BOOL ProcessOutputProfileNode(DOMNode* outputProfileNode, CPrepressParams& rPrepressProcess);
	//BOOL ProcessPrintingNode(DOMNode* printingProcessNode);
	//BOOL ProcessPrintingDeviceNode(DOMNode* printingProcessDeviceNode, CPressParams& rPrintingProcess);
	//BOOL ProcessFoldingNode(DOMNode* foldingProcessNode);
	//BOOL ProcessFoldingSchemePoolNode(DOMNode* foldingProcessSchemePoolNode, CFoldingProcess& rFoldingProcess);
	//BOOL ProcessFoldingShinglingNode(DOMNode* foldingProcessShinglingNode, CFoldingProcess& rFoldingProcess);
	//BOOL ProcessBindingNode(DOMNode* bindingProcessNode);
	//BOOL ProcessBindingCollectingNode(DOMNode* bindingProcessCollectingNode, CBindingProcess& rBindingProcess);
	//BOOL ProcessBindingGatheringNode(DOMNode* bindingProcessGatheringNode, CBindingProcess& rBindingProcess);
	//BOOL ProcessTrimmingNode(DOMNode* trimmingProcessNode);
	BOOL ProcessProductSectionNode(DOMNode* productSectionNode);
	BOOL ProcessProductNode(DOMNode* productNode);
	BOOL ProcessBoundProductNode(DOMNode* boundProductNode, CBoundProduct& rBoundProduct);
	//BOOL ProcessBoundProductLayoutNode(DOMNode* boundProductLayoutNode, CBoundProduct& rBoundProduct);
	//BOOL ProcessBoundProductQuantityNode(DOMNode* boundProductQuantityNode, CBoundProduct& rBoundProduct);
	//BOOL ProcessBoundProductDeliveryNode(DOMNode* boundProductDeliveryNode, CBoundProduct& rBoundProduct);
	//BOOL ProcessBoundProductContentNode(DOMNode* boundProductContentNode, CBoundProduct& rBoundProduct);
	//BOOL ProcessBoundProductPartNode(DOMNode* boundProductPartNode, CBoundProduct& rBoundProduct);
	BOOL ProcessBoundProductBindingInfoNode(DOMNode* boundProductBindingInfoNode, CBoundProduct& rBoundProduct);
	//BOOL ProcessBoundProductPartLayoutNode(DOMNode* boundProductPartLayoutNode, CBoundProductPart& rBoundProductPart);
	//BOOL ProcessFlatProductNode(DOMNode* flatProductNode, CFlatProduct& rFlatProduct);
	//BOOL ProcessFlatProductLayoutNode(DOMNode* flatProductLayoutNode, CFlatProduct& rFlatProduct);
	//BOOL ProcessBoundProductPartColorIntentsNode(DOMNode* boundProductPartColorIntentsNode, CBoundProductPart& rBoundProductPart);
	//BOOL ProcessFlatProductQuantityNode(DOMNode* flatProductQuantityNode, CFlatProduct& rFlatProduct);
	//BOOL ProcessFlatProductDeliveryNode(DOMNode* flatProductDeliveryNode, CFlatProduct& rFlatProduct);
	//BOOL ProcessFlatProductContentNode(DOMNode* flatProductContentNode, CFlatProduct& rFlatProduct);
	//BOOL ProcessFlatProductColorIntentsNode(DOMNode* flatProductColorIntentsNode, CFlatProduct& rFlatProduct);
	BOOL ProcessComponentsSectionNode(DOMNode* componentsSectionNode);
	//BOOL ProcessStrippingSectionNode(DOMNode* strippingSectionNode);
	//BOOL ProcessPrintSheetNode(DOMNode* printSheetNode);
	//BOOL ProcessPaperNode(DOMNode* paperNode, CPrintSheet* pPrintSheet);
	//BOOL ProcessMarksetNode(DOMNode* marksetNode, CPrintSheet* pPrintSheet);
	//BOOL ProcessFoldSheetNode(DOMNode* foldSheetNode, CPrintSheet* pPrintSheet);
	//void AutoImposeFoldSheets(CBoundProduct& rProduct);
	//void PrintSheetGetFoldSheetPlacement(CLayout* pLayout, float fGrossWidth, float fGrossHeight, const CString strSector, const CString strSubSector, const CString strOrientation, float& fFoldSheetPosX, float& fFoldSheetPosY);
	//BOOL PrintSheetPlaceFoldSheetLayer(CLayout* pLayout, CFoldSheet* pFoldSheet, int nLayerIndex, float fFoldSheetPosX, float fFoldSheetPosY, int nOrientation, CBoundProductPart& rProductPart, int nSpreadAxis = 0, float fSpreadGap = 0.0f, int nSpreadIndex = -1);
	//BOOL ProcessFlatComponentNode(DOMNode* flatComponentNode, CPrintSheet* pPrintSheet);
	//void PrintSheetGetFlatComponentPlacement(CLayout* pLayout, float fGrossWidth, float fGrossHeight, const CString strSector, const CString strSubSector, const CString strOrientation, float& fFlatPosX, float& fFlatPosY);
	//BOOL PrintSheetPlaceFlatComponent(CPrintSheet* pPrintSheet, CFlatProduct& rFlatProduct, float fFlatPosX, float fFlatPosY, int nOrientation);
};
