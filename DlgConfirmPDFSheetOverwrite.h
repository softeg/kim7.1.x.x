#if !defined(AFX_DLGCONFIRMPDFSHEETOVERWRITE_H__07CC8EB2_7A62_11D4_87AC_204C4F4F5020__INCLUDED_)
#define AFX_DLGCONFIRMPDFSHEETOVERWRITE_H__07CC8EB2_7A62_11D4_87AC_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgConfirmPDFSheetOverwrite.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgConfirmPDFSheetOverwrite 

class CDlgConfirmPDFSheetOverwrite : public CDialog
{
// Konstruktion
public:
	CDlgConfirmPDFSheetOverwrite(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CDlgConfirmPDFSheetOverwrite)
	enum { IDD = IDD_CONFIRM_PDFSHEET_OVERWRITE };
	CString	m_strPDFFilename;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgConfirmPDFSheetOverwrite)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgConfirmPDFSheetOverwrite)
	virtual BOOL OnInitDialog();
	afx_msg void OnNew();
	afx_msg void OnUseExisting();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGCONFIRMPDFSHEETOVERWRITE_H__07CC8EB2_7A62_11D4_87AC_204C4F4F5020__INCLUDED_
