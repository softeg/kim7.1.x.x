#pragma once

#include "OwnerDrawnButtonList.h"
#include "DlgComponentImposer.h"


// CPrintComponentsView-Ansicht

class CPrintComponentsView : public CScrollView
{
	DECLARE_DYNCREATE(CPrintComponentsView)

protected:
	CPrintComponentsView();           // Dynamische Erstellung verwendet geschützten Konstruktor
	virtual ~CPrintComponentsView();

public:
	DECLARE_DISPLAY_LIST	 (CPrintComponentsView, OnMultiSelect, NO, OnStatusChanged, YES)
	DECLARE_DISPLAY_ITEM	 (CPrintComponentsView, PageSection, OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_SNAP_DISPLAY_ITEM(CPrintComponentsView, FoldSheet,	 OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_SNAP_DISPLAY_ITEM(CPrintComponentsView, FlatProduct, OnSelect, YES, OnDeselect, YES, OnActivate, YES, OnDeactivate, NO, OnDropped, NO, OnMouseMove, YES, OnDrawState, NO, IsSelectable, TRUE)

enum displayColIDs {ColInfo, ColPrintSheetLoc, ColProductQuantity, ColProductQuantityDiff};

typedef struct displayColumn
{
	int nID;
	int nWidth;
};
typedef CArray <struct displayColumn, struct displayColumn&> DISPLAY_COLUMN_LIST;

public:
	DISPLAY_COLUMN_LIST		m_displayColumns;
	COleDropTarget			m_dropTarget;


public:
	CSize					m_docSize;
	CRect					m_clipBox;
	CHeaderCtrl  			m_HeaderCtrl;
	int			 			m_nHeaderHeight;
	int						m_nHeaderItemsTotalWidth;
	CFont		 			m_headerFont;
	CArray <int, int>		m_headerColumnWidths;
	COwnerDrawnButtonList	m_toolBar;
	int						m_nToolBarHeight;
	int						m_nStatusBarHeight;
	BOOL					m_bShowUnprinted;
	BOOL					m_bShowPrintedOnly;


public:
	void						SetProductGroupsTableHeaderWidth(int nIndex, int nValue);
	int							GetProductGroupsTableHeaderWidth(int nIndex, int nDefault);
	void						ResetDisplayColums();
	void						AddDisplayColumn(int nColID, int nWidth);
	CSize						CalcExtents();
	void						ClipHeader(CDC* pDC);
	static void					DrawToolsBackground(CDC* pDC, CRect rcFrame, CWnd* pWnd);
	CRect						DrawListPart(CDC* pDC, CDisplayList* pDisplayList);
	CRect						DrawStatusPart(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect						DrawCompleteProducts  (CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	CRect						DrawIncompleteProducts(CDC* pDC, CRect rcFrame, CDisplayList* pDisplayList);
	void						SelectPrintSheetViewDisplayItems(CPrintSheetView* pPrintSheetView, CPrintComponent& rComponent);
	CPrintComponent				GetActComponent();
	void						SetActComponent(CPrintComponent& rComponent);
	CPrintSheet*				GetActPrintSheet();
	void						SetActPrintSheet(CPrintSheet* pActPrintSheet, BOOL bNoUpdate);
	BOOL						IsOpenDlgFoldSchemeSelector();
	CDlgSelectFoldSchemePopup*	GetDlgFoldSchemeSelector();
	CDlgComponentImposer*		GetDlgComponentImposer();
	BOOL						IsOpenDlgComponentImposer();
	void						EnsureVisibleComponent(CPrintComponent& rComponent);
	void						HandleOnSelectComponent(CDisplayItem* pDI, CPoint point);
	void						HandleOnDeselectComponent(CDisplayItem* pDI);
	void						ResizeDetailsView();



public:
	virtual void OnDraw(CDC* pDC);      // Überladen, um diese Ansicht darzustellen
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public: 
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnInitialUpdate();
	virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
	virtual void ScrollToPosition(POINT pt);
	virtual void ScrollToDevicePosition(POINT ptDev);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	virtual DROPEFFECT OnDragEnter(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	virtual void OnDragLeave();

	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMouseLeave();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnHeaderTrack	 (NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnShowUnprinted();
	afx_msg void OnClearComponentList();
	afx_msg void OnUpdateShowUnprinted(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSaveUnprinted(CCmdUI* pCmdUI);
	afx_msg void OnNcActivate(BOOL bActive);
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	afx_msg void OnNcPaint();
	afx_msg void OnNcMouseMove(UINT nHitTest, CPoint point);
	afx_msg void OnNcMouseLeave();
	afx_msg void OnNcLButtonDown(UINT nFlags, CPoint point);
	afx_msg LRESULT OnNcHitTest(CPoint point);
};

