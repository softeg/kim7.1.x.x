#include "stdafx.h"
#include "Imposition Manager.h"
#include "JDFInputMIS.h"
#include "DlgPressDevice.h"


CJDFInputMIS::CJDFInputMIS(void)
{
	setlocale(LC_NUMERIC, "english");	// because when german setted, sscanf() does expect numbers with Comma like "0,138"
}

CJDFInputMIS::~CJDFInputMIS(void)
{
	switch (theApp.settings.m_iLanguage)
	{
		case 0: setlocale(LC_ALL, "german");	break;
		case 1: setlocale(LC_ALL, "english");	break;
		case 2: break;
		case 3: break;
		case 4: break;
		case 5: break;	// No holl. MFC4x.dll available
		//Default would already be set at reading ini-file
	}
}

BOOL CJDFInputMIS::Open(CString strJDFFile)
{
	m_strProtocol.Empty();

	m_pJDFDoc = new JDFDoc(0);
	parser	  = new JDFParser();

	try
	{
		parser->Parse((WString)strJDFFile);
	}catch (JDFException e)
	{
		CString strMsg;
		strMsg.Format(_T("JDF API error: %s"), e.getMessage().getBytes());
		KIMOpenLogMessage(strMsg, MB_ICONERROR);
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	*m_pJDFDoc = parser->GetRoot();

	// set up the root process
	JDFNode root=m_pJDFDoc->GetJDFRoot();

	if (root.isNull())
	{
		KIMOpenLogMessage(_T("JDF error! Root is NULL"), MB_ICONERROR);
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	JDFAuditPool auditPool = root.GetAuditPool();
	if ( ! auditPool.isNull())
	{
		JDFAudit audit = auditPool.GetAudit(0, JDFAudit::AuditType_Created);
		if ( ! audit.isNull())
			m_strAgent = audit.GetAgentName().getBytes();
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
	{
		delete parser;
		delete m_pJDFDoc;
		return FALSE;
	}

	CBoundProduct newBoundProduct;
	newBoundProduct.m_strProductName.LoadString(IDS_NEW_BOUND);
	pDoc->m_boundProducts.AddTail(newBoundProduct);

	BOOL bNoStrippingParams = TRUE;
	if (root.GetType() == L"Product")
	{
		ProcessProductGlobal(root);

		vJDFNode vNode=root.GetvJDFNode();//L"", JDFNode::Activation_Unknown, true);
		for(int i = 0; i < vNode.size(); i++)
		{
			JDFNode node=vNode[i];

			JDFResourceLinkPool linkPool = node.GetResourceLinkPool();
			vElement vComp = linkPool.GetInOutLinks(FALSE, FALSE, L"Component");
			if (vComp.size())
			{
				JDFComponent comp = vComp[0];
				if ( ! comp.isNull())
				{
					if (comp.GetAttribute(L"ComponentType").HasToken(L"FinalProduct"))
					{
						if (comp.GetAttribute(L"DescriptiveName").length() > 0)
							pDoc->m_boundProducts.GetTail().m_strProductName = comp.GetAttribute(L"DescriptiveName").getBytes();
						else
							pDoc->m_boundProducts.GetTail().m_strProductName = comp.GetAttribute(L"ProductType").getBytes();
					}
				}
			}

			if (node == root)
				continue;
			if (node.GetType() == L"Product")
			{
				if (ProcessProductGroup(node))
					bNoStrippingParams = FALSE;	// at least one set of stripping params found
			}
		}
		for (int i = 0; i < vNode.size(); i++)
		{
			JDFNode node=vNode[i];
			if (node.GetType() == L"ProcessGroup")
			{
				if (ProcessProcessGroup(node))
					bNoStrippingParams = FALSE;	// at least one set of stripping params found
			}
		}
	}

	if (bNoStrippingParams)
	{
		if ( ! ProcessStrippingParams(root, CBoundProductPart(), -1, CPrintingProfile()))	// look for global stripping params
		{
			CString string;
			string.Format(_T("No StrippingParams found!\n"));
			m_strProtocol += string;
		}
	}

	delete m_pJDFDoc;
	delete parser;

	CString strDrive, strDir, strFname, strExt;
	_splitpath((LPCTSTR)strJDFFile, strDrive.GetBuffer(_MAX_DRIVE), strDir.GetBuffer(_MAX_DIR), strFname.GetBuffer(_MAX_FNAME), strExt.GetBuffer(_MAX_EXT));
	pDoc->SetTitle(strFname);
	pDoc->m_GlobalData.m_CreationDate	  = COleDateTime::GetCurrentTime();
	pDoc->m_GlobalData.m_LastModifiedDate = COleDateTime::GetCurrentTime();

	for (int nProductID = 0; nProductID < pDoc->m_boundProducts.GetSize(); nProductID++)
	{
		UINT nPages = 0;
		for (int j = 0; j < pDoc->GetBoundProduct(nProductID).m_parts.GetSize(); j++)
			nPages += pDoc->GetBoundProduct(nProductID).m_parts[j].m_nNumPages;
		pDoc->GetBoundProduct(nProductID).SetNumPages(nPages);

		if ( (pDoc->GetBoundProduct(nProductID).m_fTrimSizeWidth == 0.0f) || (pDoc->GetBoundProduct(nProductID).m_fTrimSizeHeight == 0.0f) )
		{
			if (pDoc->GetBoundProduct(nProductID).m_parts.GetSize() > 0)
			{
				pDoc->GetBoundProduct(nProductID).m_fTrimSizeWidth  = pDoc->GetBoundProduct(nProductID).m_parts[0].m_fPageWidth;
				pDoc->GetBoundProduct(nProductID).m_fTrimSizeHeight = pDoc->GetBoundProduct(nProductID).m_parts[0].m_fPageHeight;
				pDoc->GetBoundProduct(nProductID).m_strFormatName   = pDoc->GetBoundProduct(nProductID).m_parts[0].m_strFormatName;
			}
		}
	}

	pDoc->m_GlobalData.m_strJobTicketFile = strJDFFile;

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		CLayout* pLayout = rPrintSheet.GetLayout();
		if (pLayout)
		{
			pLayout->AlignLayoutObjects(XCENTER, &rPrintSheet);
			CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
			if (pPressDevice)
				if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
				{
					float fBoundingRectLeft, fBoundingRectBottom, fBoundingRectRight, fBoundingRectTop, fBoundingRectWidth, fBoundingRectHeight;
					pLayout->m_FrontSide.CalcBoundingRectObjects(&fBoundingRectLeft, &fBoundingRectBottom, &fBoundingRectRight, &fBoundingRectTop, &fBoundingRectWidth, &fBoundingRectHeight, FALSE, -1, FALSE, NULL);
					if (fabs(fBoundingRectBottom - pLayout->m_FrontSide.m_fPaperBottom) < pPressDevice->m_fGripper)
						pLayout->AlignLayoutObjects(BOTTOM, &rPrintSheet);
				}
		}
	}

	pDoc->m_Bookblock.ArrangeAssemblySections();
	pDoc->m_Bookblock.ArrangeCover();
	pDoc->m_Bookblock.ReNumberFoldSheets(-1);
	pDoc->m_PageTemplateList.SortPages();
	pDoc->m_PrintSheetList.SortByFoldSheets();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();

	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	//pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();	// in SetModifiedFlag()
	pDoc->m_Bookblock.CheckFoldSheetQuantities();
	pDoc->m_Bookblock.m_FoldSheetList.UpdatePaperNames();

	theApp.m_bInsideNewJobLoop = TRUE;
	pDoc->SetModifiedFlag();
	pDoc->UpdateAllViews(NULL, 0, NULL);

	if ( ! m_strProtocol.IsEmpty())
	{
		KIMOpenLogMessage(m_strProtocol, MB_ICONERROR);
		//if (m_strProtocol.Find(_T("Druckmaschine/Kostenstelle")) >= 0)
		//{
		//	CDlgPressDevice dlg;
		//	dlg.DoModal();
		//}
	}

	return TRUE;
}

BOOL CJDFInputMIS::ProcessProductGlobal(JDFNode node)
{
	JDFResourceLinkPool linkPool = node.GetResourceLinkPool();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	pDoc->m_productionData.m_nProductionType = 0;	// set to normal, because could be setted to C&G in constructor as result of LastProductionStyle

	if ( ! (theApp.m_nAddOnsLicensed & CImpManApp::JDFMIS))
		pDoc->m_nDongleStatus = CImpManApp::NoDongle;

	pDoc->m_GlobalData.m_strJob		  = node.GetDescriptiveName().getBytes();
	pDoc->m_GlobalData.m_strJobNumber = node.GetJobID().getBytes();

	ParseCustomerInfo(node);
	
	int nProductID = pDoc->m_boundProducts.GetSize() - 1;

	vElement vBindingIntents = linkPool.GetInOutLinks(TRUE, FALSE, L"BindingIntent");
	if (vBindingIntents.size())
	{
		JDFBindingIntent bi = vBindingIntents[0];
		if ( ! bi.isNull())
		{
			JDFSpanBindingType bindingType = bi.GetBindingType();
			switch (bindingType.GetActual())
			{
			case JDFSpanBindingType::SpanBindingType_SaddleStitch:
			case JDFSpanBindingType::SpanBindingType_SideStitch:
				pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::SaddleStitched;
				pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_SADDLESTITCHED_TOOLTIP);
				break;
			case JDFSpanBindingType::SpanBindingType_LooseBinding:
				pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::PerfectBound;
				pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_LOOSE_BINDING);
				break;
			default:
				pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::PerfectBound;
				pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_PERFECTBOUND_TOOLTIP);
				break;
			}

			if ( ! bi.GetAttribute(L"BindingOrder").empty())
			{
				JDFAutoBindingIntent::EnumBindingOrder bindingOrder = bi.GetBindingOrder();
				switch (bindingOrder)
				{
				case JDFAutoBindingIntent::BindingOrder_Collecting:
						pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::SaddleStitched;
						pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_SADDLESTITCHED_TOOLTIP);
						break;
				case JDFAutoBindingIntent::BindingOrder_Gathering:
						pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::PerfectBound;
						pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_PERFECTBOUND_TOOLTIP);
						break;
				case JDFAutoBindingIntent::BindingOrder_List:
						pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_nBinding = CBindingDefs::PerfectBound;
						pDoc->GetBoundProduct(nProductID).m_bindingParams.m_bindingDefs.m_strName.LoadStringA(IDS_PERFECTBOUND_TOOLTIP);
						break;
				case JDFAutoBindingIntent::BindingOrder_Unknown:	
						break;
				}
			}
		}
	}

	return TRUE;
}

CString CJDFInputMIS::GetPageFormatName(float fWidth, float fHeight)
{
	CString strFormatName;

	CPageFormatSelection pageFormats;
	pageFormats.LoadData();
	CPageFormat* pPageFormat = pageFormats.FindBySize(fWidth, fHeight);
	if (pPageFormat)
		strFormatName = pPageFormat->m_szName;
	else
	{
		CString strMeasureFormat;
		strMeasureFormat.Format("%s x %s", (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));
		strFormatName.Format((LPCTSTR)strMeasureFormat, MeasureFormat((float)fWidth), MeasureFormat((float)fHeight));
	}

	return strFormatName;
}

void CJDFInputMIS::ParseCustomerInfo(JDFNode node)
{
	JDFCustomerInfo customerInfo = node.GetCustomerInfo();
	if ( ! customerInfo.isNull())
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if ( ! pDoc)
			return;

		pDoc->m_GlobalData.m_strCustomerNumber	= customerInfo.GetCustomerID().getBytes();

		JDFContact contact = customerInfo.GetContact();
		if ( ! contact.isNull())
		{
			JDFCompany company = contact.GetCompany();
			pDoc->m_GlobalData.m_strCustomer = company.GetOrganizationName().getBytes();
		}
	}
}

BOOL CJDFInputMIS::ProcessProductGroup(JDFNode node)
{
	JDFResourceLinkPool linkPool = node.GetResourceLinkPool();

	CString strProductType = _T("Body");
	vElement vComp = linkPool.GetInOutLinks(FALSE, FALSE, L"Component");
	if (vComp.size())
	{
		JDFComponent comp = vComp[0];
		if ( ! comp.isNull())
		{
			if (comp.GetAttribute(L"ComponentType").HasToken(L"PartialProduct") || comp.GetAttribute(L"ComponentType").HasToken(L"FinalProduct") || comp.GetAttribute(L"ComponentType").HasToken(L"Sheet"))
				strProductType = comp.GetProductType().getBytes();
		}
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	int	  nNumPages	  = 0;
	float fPageWidth  = 0.0f;
	float fPageHeight = 0.0f;
	vElement vLayoutIntents = linkPool.GetInOutLinks(TRUE, FALSE, L"LayoutIntent");
	if ( ! vLayoutIntents.size())
		return FALSE;
	JDFLayoutIntent layoutIntent = vLayoutIntents[0];
	if ( ! layoutIntent.isNull())
	{
		JDFIntegerSpan pages = layoutIntent.GetPages();
		nNumPages = pages.GetActual();
		JDFShapeSpan dimensions = layoutIntent.GetFinishedDimensions();
		fPageWidth  = (float)dimensions.GetActual().GetA() * fPSPoint;
		fPageHeight = (float)dimensions.GetActual().GetB() * fPSPoint;
	}

	CString strFormatName = GetPageFormatName(fPageWidth, fPageHeight);

	CString strName = node.GetAttribute(L"DescriptiveName").getBytes();
	if (strName.IsEmpty())
		strName = node.GetJobPartID().getBytes();

	int				   nProductID	 = pDoc->m_boundProducts.GetSize() - 1;
	CBoundProduct&	   rBoundProduct = pDoc->GetBoundProduct(nProductID);
	CBoundProductPart& rProductPart  = rBoundProduct.JDFAddProductPart(node.GetID().getBytes(), strProductType, strName, nNumPages, fPageWidth, fPageHeight, strFormatName, rBoundProduct);

	int nProductPartIndex = rProductPart.GetIndex();

	JDFMediaIntent mediaIntent;
	vElement	  vMediaIntent = linkPool.GetInOutLinks(TRUE, FALSE, L"MediaIntent");
	if (vMediaIntent.size())
	{
		mediaIntent = vMediaIntent[0];
		if ( ! mediaIntent.isNull())
		{
			rProductPart.m_strPaper = mediaIntent.GetAttribute(L"DescriptiveName").getBytes();
			if (rProductPart.m_strPaper.IsEmpty())
			{
				JDFStringSpan stockBrand = mediaIntent.GetStockBrand();
				rProductPart.m_strPaper = stockBrand.GetActual().getBytes();
			}
			JDFNumberSpan weight = mediaIntent.GetWeight();
			rProductPart.m_nPaperGrammage = (int)weight.GetActual();
			JDFNumberSpan thickness = mediaIntent.GetThickness();
			float fThickness = ( ! thickness.isNull()) ? (float)thickness.GetActual() : 0.0f;
			if (rProductPart.m_nPaperGrammage > 0)
				rProductPart.m_fPaperVolume = (fabs(fThickness) > 0.0001) ? fThickness/(float)rProductPart.m_nPaperGrammage : 1.0f;
			else
				rProductPart.m_fPaperVolume = 1.0f;
			rProductPart.m_foldingParams.SetShinglingValueX(rProductPart.GetShinglingValueX(TRUE));
			rProductPart.m_foldingParams.SetShinglingValueXFoot(rProductPart.GetShinglingValueXFoot(TRUE)); 
			rProductPart.m_foldingParams.SetShinglingValueY(rProductPart.GetShinglingValueY(TRUE));
		}
	}

	CPrintingProfile printingProfile;
	printingProfile.m_strName = node.GetDescriptiveName().getBytes();
	BOOL bStrippingParamsFound = ProcessStrippingParams(node, rProductPart, nProductPartIndex, printingProfile);

	if (rProductPart.m_nNumPages == 0)		// pages in LayoutIntent has been undefined -> PageTemplateList has not been filled in JDFAddProductGroup() -> let's make it now and here
	{
		rProductPart.m_nNumPages = pDoc->m_Bookblock.GetNumPages(nProductPartIndex);

		pDoc->m_PageTemplateList.ShiftProductPartIndices(nProductPartIndex);
		pDoc->m_Bookblock.ShiftProductPartIndices(nProductPartIndex);
		pDoc->m_PageTemplateList.InsertProductPartPages(rProductPart.m_nNumPages, nProductID, nProductPartIndex);
	}

	JDFColorIntent colorIntent;
	vElement	  vColorIntent = linkPool.GetInOutLinks(TRUE, FALSE, L"ColorIntent");
	for (int i = 0; i < vColorIntent.size(); i++)
	{
		colorIntent = vColorIntent[i];

		vmAttribute vmAtt = colorIntent.GetPartMapVector(TRUE);
		if ( ! vmAtt.size())
			continue;

		for (int i = 0; i < vmAtt.size(); i++)
		{
			JDFColorIntent part = colorIntent.GetPartition(vmAtt[i]);
			CString strSheetName = part.GetSheetName().getBytes();
			JDFPart::EnumSide side = part.GetSide();

			JDFSeparationList separationList = part.GetColorsUsed();
			if ( ! separationList.isNull())
			{
				vWString separations = separationList.GetSeparations();
				for (int k = 0; k < separations.size(); k++)
				{
					CString strSep = separations[k].getBytes();
					CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.FindPrintSheet(strSheetName);
					if (pPrintSheet)
					{
						CPlate* pPlate = NULL;
						switch (side)
						{
						case JDFPart::Side_Front:	pPlate = pPrintSheet->m_FrontSidePlates.GetTopmostPlate(); break;
						case JDFPart::Side_Back:	pPlate = pPrintSheet->m_BackSidePlates.GetTopmostPlate();  break;
						case JDFPart::Side_Unknown:	break;
						}
						if (pPlate) 
							pPlate->AddSeparation(strSep);
					}
					rProductPart.m_colorantList.AddSeparation(strSep);
				}
			}
		}
	}

	POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (rPrintSheet.HasBoundProductPart(nProductPartIndex))
		{
			CPlate* pPlate = rPrintSheet.m_FrontSidePlates.GetTopmostPlate();
			if (pPlate)
			{
				if ( ! pPlate->m_processColors.GetSize() && ! pPlate->m_spotColors.GetSize() )
				{
					for (int i = 0; i < rProductPart.m_colorantList.GetSize(); i++)
					{
						CColorant colorant = rProductPart.m_colorantList[i];
						pPlate->AddSeparation(colorant.m_strName);
					}
				}
			}

			pPlate = rPrintSheet.m_BackSidePlates.GetTopmostPlate();
			if (pPlate)
			{
				if ( ! pPlate->m_processColors.GetSize() && ! pPlate->m_spotColors.GetSize() )
				{
					for (int i = 0; i < rProductPart.m_colorantList.GetSize(); i++)
					{
						CColorant colorant = rProductPart.m_colorantList[i];
						pPlate->AddSeparation(colorant.m_strName);
					}
				}
			}
		}
	}

	if ( ! bStrippingParamsFound)
	{
		if (m_strAgent.CompareNoCase(_T("Prinance")) == 0)	
			ProcessPrinanceBullShit(node, nProductPartIndex);

		return FALSE;	
	}
	else
		return TRUE;
}

BOOL CJDFInputMIS::ProcessProcessGroup(JDFNode node)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	int nProductPartIndex = 0;
	CBoundProductPart& rProductPart = pDoc->GetBoundProductPart(nProductPartIndex);

	CPrintingProfile printingProfile;
	printingProfile.m_strName = node.GetDescriptiveName().getBytes();
	return ProcessStrippingParams(node, rProductPart, nProductPartIndex, printingProfile);
}

BOOL CJDFInputMIS::ProcessStrippingParams(JDFNode node, CBoundProductPart& rProductPart, int nProductPartIndex, CPrintingProfile& rCPrintingProfile)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	float	fPaperWidth  = 0.0f, fPaperHeight = 0.0f;
	float	fPlateWidth	 = 0.0f, fPlateHeight = 0.0f;
	CString strSignatureName, strPressDevice = L"";

	BOOL bStrippingParamsFound = FALSE;
	vJDFNode vNode = node.GetvJDFNode(L"", JDFNode::Activation_Unknown, true);	// true = not recursive
	for (int i = 0; i < vNode.size(); i++)
	{
		JDFNode node = vNode[i];

		JDFResourceLinkPool linkPool = node.GetResourceLinkPool();
		vElement vStrippingParams = linkPool.GetInOutLinks(TRUE, FALSE, L"StrippingParams");
		if ( ! vStrippingParams.size())
			continue;

		bStrippingParamsFound = TRUE;

		for (int k = 0; k < vStrippingParams.size(); k++)
		{
			JDFStrippingParams strippingParams = vStrippingParams[k];
			vmAttribute vmAtt = strippingParams.GetPartMapVector(TRUE);

			int nPartMapPos = 0;
			while (nPartMapPos < vmAtt.size())
			{
				JDFStrippingParams part = strippingParams.GetPartition(vmAtt[nPartMapPos++]);
				if ( ! part.GetSignatureName().size())
					continue;

				strSignatureName = part.GetSignatureName().getBytes();

				StrippingGetSheetMedia(part, fPaperWidth, fPaperHeight, rProductPart);
				StrippingGetPlateMedia(part, fPlateWidth, fPlateHeight);

				////// goto next part having bindery signature
				JDFBinderySignature binderySignature = part.GetBinderySignature();
				if (binderySignature.isNull())
					continue;
				//////

				CPrintSheet* pPrintSheet = StripSheet(strippingParams, nPartMapPos, nProductPartIndex, strSignatureName, fPaperWidth, fPaperHeight, fPlateWidth, fPlateHeight, rCPrintingProfile);
				if (pPrintSheet)
				{
					CLayout* pLayout = pPrintSheet->GetLayout();
					if ( ! pLayout)
						continue;

					int nIndex = pDoc->m_PrintSheetList.m_Layouts.GetSimilarLayout(pLayout);
					if (nIndex >= 0)
					{
						POSITION pos = pDoc->m_printingProfiles.FindIndex(pPrintSheet->m_nLayoutIndex);
						if (pos)
							pDoc->m_printingProfiles.RemoveAt(pos);

						pos = pDoc->m_PrintSheetList.m_Layouts.FindIndex(pPrintSheet->m_nLayoutIndex);
						pDoc->m_PrintSheetList.m_Layouts.RemoveAt(pos);
						if (nIndex > pPrintSheet->m_nLayoutIndex)
							nIndex--;
						pPrintSheet->m_nLayoutIndex = nIndex;
						pPrintSheet->LinkLayout(NULL);
					}
				}
			}
		}
	}

	return bStrippingParamsFound;
}

CPrintSheet* CJDFInputMIS::StripSheet(JDFStrippingParams strippingParams, int& nPartMapPos, int nProductPartIndex, CString strSignatureName, float& fPaperWidth, float& fPaperHeight, float& fPlateWidth, float& fPlateHeight, CPrintingProfile& rPrintingProfile)
{
	nPartMapPos--;
	vmAttribute vmAtt = strippingParams.GetPartMapVector(TRUE);
	JDFStrippingParams part = strippingParams.GetPartition(vmAtt[nPartMapPos++]);

	JDFBinderySignature binderySignature = part.GetBinderySignature();
	if (binderySignature.isNull())
		return NULL;

	CString strPressDeviceName, strPressDeviceID;
	StrippingGetPressDevice(part, strPressDeviceName, strPressDeviceID);

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	CString strBinderySignatureID = binderySignature.GetID().c_str();
	if ( ! strBinderySignatureID.IsEmpty())	
	{
		BOOL	 bFound = FALSE;
		POSITION pos	= pDoc->m_boundProducts.GetHeadPosition();
		while (pos && ! bFound) 
		{
			CBoundProduct& rBoundProduct = pDoc->m_boundProducts.GetNext(pos);
			for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
			{
				if (strBinderySignatureID.Find(rBoundProduct.m_parts[i].GetID()) >= 0)
				{
					bFound = TRUE;
					nProductPartIndex = rBoundProduct.TransformLocalToGlobalPartIndex(i);
					break;
				}
			}
		}
	}

	CString			   strSheetName  = part.GetSheetName().getBytes();
	CBoundProductPart& rProductPart  = pDoc->m_boundProducts.GetProductPart(nProductPartIndex);
	CBoundProduct&	   rBoundProduct = rProductPart.GetBoundProduct();

	CFoldSheet foldSheet; foldSheet.m_nProductPartIndex = nProductPartIndex;
	CString strLooseBinding; strLooseBinding.LoadStringA(IDS_LOOSE_BINDING);
	if ( ! binderySignature.GetFoldCatalog().empty())	// fold catalog has priority over signature cell
	{
		CString strFoldSchemeFullPath = theApp.settings.m_szSchemeDir + CString("\\JDF Folding Catalog\\") + binderySignature.GetFoldCatalog().getBytes() + CString(".isc");
		foldSheet.CreateFromISC(strFoldSchemeFullPath, binderySignature.GetDescriptiveName().getBytes(), nProductPartIndex);
	}
	else
		if ( ! binderySignature.GetSignatureCell().isNull())	
			JDFCreateFoldSheet(binderySignature, foldSheet, nProductPartIndex);

	if (foldSheet.IsEmpty())
		if (rBoundProduct.m_bindingParams.m_bindingDefs.m_strName == strLooseBinding)
			foldSheet.Create((int)binderySignature.GetNumberUp().GetX(), (int)binderySignature.GetNumberUp().GetY(), L"", nProductPartIndex);

	if (foldSheet.IsEmpty())
	{
		CString string; string.Format(_T("BinderySignature '%s' not found!\n"), binderySignature.GetFoldCatalog().getBytes());
		if (m_strProtocol.Find(string) < 0)
			m_strProtocol += string;
		return NULL;
	}

	int			   nPart	   = nProductPartIndex;
	int			   nSection	   = -1;
	CString		   strSection; 
	JDFIntegerList sectionList = part.GetSectionList();
	if (sectionList.IsValid())
	{
		strSection = sectionList.GetString().getBytes(); 
		if ( ! strSection.IsEmpty())
		{
			CString strToken = GetStringToken(strSection, 0, _T(" "));
			nSection = atoi(strToken);
		}
	}

	if (pDoc->m_Bookblock.AddFoldSheetsSimple(&foldSheet, 1, NULL, nPart, nSection) != 1)
		return NULL;

	CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.FindPrintSheet(strSheetName);
	if ( ! pPrintSheet || (m_strAgent.CompareNoCase(_T("Prinance")) == 0))
		pPrintSheet = pDoc->m_PrintSheetList.JDFAddPrintSheet(strSheetName, fPaperWidth, fPaperHeight, fPlateWidth, fPlateHeight, strPressDeviceName, strPressDeviceID, nProductPartIndex, rPrintingProfile);
	
	CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return NULL;
	CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();

	pPrintSheet->SetPrintGroupIndex(nProductPartIndex);

	switch (part.GetWorkStyle())
	{
	case JDFStrippingParams::WorkStyle_Perfecting:		pLayout->m_nProductType = WORK_AND_BACK;	
														switch (pLayout->m_FrontSide.m_nPaperRefEdge)	// prepare for tumble 
														{
														case LEFT_BOTTOM:	pLayout->m_BackSide.m_nPaperRefEdge = LEFT_TOP;		break;
														case RIGHT_BOTTOM:	pLayout->m_BackSide.m_nPaperRefEdge = RIGHT_TOP;	break;
														case LEFT_TOP:		pLayout->m_BackSide.m_nPaperRefEdge = LEFT_BOTTOM;	break;
														case RIGHT_TOP:		pLayout->m_BackSide.m_nPaperRefEdge = RIGHT_BOTTOM;	break;
														}
														break;
	case JDFStrippingParams::WorkStyle_WorkAndBack:		pLayout->m_nProductType = WORK_AND_BACK;	break;
	case JDFStrippingParams::WorkStyle_WorkAndTurn:		pLayout->m_nProductType = WORK_AND_TURN;	break;
	case JDFStrippingParams::WorkStyle_WorkAndTumble:	pLayout->m_nProductType = WORK_AND_TUMBLE;	break;
	case JDFStrippingParams::WorkStyle_Simplex:			pLayout->m_nProductType = WORK_SINGLE_SIDE;	break;
	default:											pLayout->m_nProductType = WORK_AND_BACK;	break;
	}

	JDFStripCellParams stripCellParams = part.GetStripCellParams(0);

	AssignJDFStripCellParams(rProductPart, stripCellParams);

	BOOL bAutoCalcPaper = FALSE;
	if ( (fPaperWidth == 0.0f) || (fPaperHeight == 0.0f) )
	{
		bAutoCalcPaper = TRUE;
		JDFStripCalcSheetDims(part, pLayout, foldSheet, fPaperWidth, fPaperHeight);
	}

	float		fPaperLeft	 = pLayout->m_FrontSide.m_fPaperLeft;
	float		fPaperBottom = pLayout->m_FrontSide.m_fPaperBottom;
	float		fDisplayAreaLeft, fDisplayAreaBottom, fDisplayAreaRight, fDisplayAreaTop;
	int nOldPartMapPos = nPartMapPos;
	while (nPartMapPos < vmAtt.size())
	{
		part = strippingParams.GetPartition(vmAtt[nPartMapPos++]);
		if ( ! part.GetPosition(0).isNull())
			break;
	}
	nPartMapPos = nOldPartMapPos;

	int	nPosIndex = 0;
	JDFPosition position = part.GetPosition(nPosIndex++);
	while ( ! position.isNull())
	{
		JDFRectangle relBox = position.GetRelativeBox();
		
		fDisplayAreaLeft   = fPaperLeft	  + fPaperWidth  * (float)relBox.GetLlx() + (float)position.GetMarginLeft()   * fPSPoint;
		fDisplayAreaBottom = fPaperBottom + fPaperHeight * (float)relBox.GetLly() + (float)position.GetMarginBottom() * fPSPoint;
		fDisplayAreaRight  = fPaperLeft   + fPaperWidth  * (float)relBox.GetUrx();
		fDisplayAreaTop    = fPaperBottom + fPaperHeight * (float)relBox.GetUry();

		int nOrientation = StrippingParamsMapOrientation(position.GetOrientation());
		int nLayerIndex = 0;
		JDFStripFoldSheet(pPrintSheet, strippingParams, nPartMapPos, &foldSheet, nLayerIndex, fDisplayAreaLeft, fDisplayAreaBottom, rProductPart, nOrientation);

		position = part.GetPosition(nPosIndex++);
	}

	if (bAutoCalcPaper)
	{
		pLayout->m_FrontSide.FindSuitablePaper();
		pLayout->m_FrontSide.PositionLayoutSide((pPressDevice) ? pPressDevice->m_fGripper : 0.0f);
		pLayout->m_BackSide.FindSuitablePaper();
		pLayout->m_BackSide.PositionLayoutSide((pPressDevice) ? pPressDevice->m_fGripper : 0.0f);
	}

	pLayout->m_FrontSide.Invalidate();
	pLayout->m_BackSide.Invalidate();

	if (part.GetBinderySignatureName().size())	// go until bindery signature name changes
	{
		WString strBinderySignatureName = part.GetBinderySignatureName();
		while (nPartMapPos < vmAtt.size())
		{
			JDFStrippingParams part = strippingParams.GetPartition(vmAtt[nPartMapPos++]);
			if (part.GetBinderySignatureName() != strBinderySignatureName)
			{
				nPartMapPos--;
				break;
			}
		}
	}
	else	// go until signature name changes
	{
		WString strSignatureName = part.GetSignatureName();
		while (nPartMapPos < vmAtt.size())
		{
			JDFStrippingParams part = strippingParams.GetPartition(vmAtt[nPartMapPos++]);
			if (part.GetSignatureName() != strSignatureName)
			{
				nPartMapPos--;
				fPaperWidth  = fPaperHeight = 0.0f;
				fPlateWidth	 = fPlateHeight = 0.0f;
				break;
			}
		}
	}

	return pPrintSheet;
}

void CJDFInputMIS::AssignJDFStripCellParams(CBoundProductPart& rProductPart, JDFStripCellParams stripCellParams)
{
	CBoundProduct& rProduct = rProductPart.GetBoundProduct();

	if ( ! stripCellParams.GetAttribute(L"TrimHead").empty())
		rProductPart.m_trimmingParams.m_fHeadTrim	 = (float)stripCellParams.GetTrimHead()	    * fPSPoint;
	if ( ! stripCellParams.GetAttribute(L"TrimFace").empty())
		rProductPart.m_trimmingParams.m_fSideTrim	 = (float)stripCellParams.GetTrimFace()	    * fPSPoint;
	if ( ! stripCellParams.GetAttribute(L"TrimFoot").empty())
		rProductPart.m_trimmingParams.m_fFootTrim	 = (float)stripCellParams.GetTrimFoot()	    * fPSPoint;
	if ( ! stripCellParams.GetAttribute(L"Spine").empty())
		rProduct.m_bindingParams.m_fSpineArea		 = (float)stripCellParams.GetSpine()		* fPSPoint / 2.0f;
	if ( ! stripCellParams.GetAttribute(L"MillingDepth").empty())
		rProduct.m_bindingParams.m_fMillingDepth	 = (float)stripCellParams.GetMillingDepth() * fPSPoint;

	rProduct.m_bindingParams.m_fSpineArea = max(rProduct.m_bindingParams.m_fSpineArea, rProduct.m_bindingParams.m_fMillingDepth * 2.0f);
	
	if ( ! stripCellParams.GetAttribute(L"TrimSize").empty())
	{
		JDFXYPair xyPair = stripCellParams.GetTrimSize();
		rProductPart.m_fPageWidth  = (float)xyPair.GetX()*fPSPoint;
		rProductPart.m_fPageHeight = (float)xyPair.GetY()*fPSPoint;
	}

	if ( ! stripCellParams.GetAttribute(L"FrontOverfold").empty())
	{
		rProduct.m_bindingParams.m_fOverFold	 = (float)stripCellParams.GetFrontOverfold() * fPSPoint;
		rProduct.m_bindingParams.m_nOverFoldSide = CBindingProcess::OverFoldFront;
	}
	else
		if ( ! stripCellParams.GetAttribute(L"BackOverfold").empty())
		{
			rProduct.m_bindingParams.m_fOverFold	 = (float)stripCellParams.GetBackOverfold() * fPSPoint;
			rProduct.m_bindingParams.m_nOverFoldSide = CBindingProcess::OverFoldBack;
		}
}

void CJDFInputMIS::GetJDFStripCellParams(float& fPageWidth, float& fPageHeight, float& fOverFold, int& nOverFoldSide, float& fHeadTrim, float& fFootTrim, float& fSideTrim, float& fSpine, JDFStripCellParams stripCellParams)
{
	nOverFoldSide = CBindingProcess::OverFoldNone;
	fPageHeight   = fOverFold = fHeadTrim = fFootTrim = fSideTrim = fSpine = 0.0f;

	if ( ! stripCellParams.GetAttribute(L"TrimHead").empty())
		fHeadTrim	 = (float)stripCellParams.GetTrimHead()	    * fPSPoint;
	if ( ! stripCellParams.GetAttribute(L"TrimFace").empty())
		fSideTrim	 = (float)stripCellParams.GetTrimFace()	    * fPSPoint;
	if ( ! stripCellParams.GetAttribute(L"TrimFoot").empty())
		fFootTrim	 = (float)stripCellParams.GetTrimFoot()	    * fPSPoint;
	if ( ! stripCellParams.GetAttribute(L"Spine").empty())
		fSpine		 = (float)stripCellParams.GetSpine()		* fPSPoint / 2.0f;
	if ( ! stripCellParams.GetAttribute(L"MillingDepth").empty())
		fSpine		 = max(fSpine, (float)stripCellParams.GetMillingDepth() * fPSPoint * 2.0f);
	
	if ( ! stripCellParams.GetAttribute(L"TrimSize").empty())
	{
		JDFXYPair xyPair = stripCellParams.GetTrimSize();
		fPageWidth  = (float)xyPair.GetX()*fPSPoint;
		fPageHeight = (float)xyPair.GetY()*fPSPoint;
	}

	if ( ! stripCellParams.GetAttribute(L"FrontOverfold").empty())
	{
		fOverFold	  = (float)stripCellParams.GetFrontOverfold() * fPSPoint;
		nOverFoldSide = CBindingProcess::OverFoldFront;
	}
	else
		if ( ! stripCellParams.GetAttribute(L"BackOverfold").empty())
		{
			fOverFold	  = (float)stripCellParams.GetBackOverfold() * fPSPoint;
			nOverFoldSide = CBindingProcess::OverFoldBack;
		}
}

void CJDFInputMIS::StrippingGetSheetMedia(JDFStrippingParams strippingParams, float& fPaperWidth, float& fPaperHeight, CBoundProductPart& rProductPart)
{
	JDFMedia media;
	for (int nSkip = 0; ;nSkip++)
	{
		media = strippingParams.GetMedia(nSkip);
		if (media.isNull())
			break;
		if (media.GetAttribute(L"MediaType") != L"Paper")
			continue;

		WString strDims = media.GetAttribute(L"Dimension");
		if (strDims.length() > 1)
		{
			fPaperWidth  = ((float)(double)strDims.Token(0)) * fPSPoint;
			fPaperHeight = ((float)(double)strDims.Token(1)) * fPSPoint;
		}

		vmAttribute vmAtt = media.GetPartMapVector(TRUE);
		if ( ! vmAtt.size())
			continue;

		JDFMedia part;
		for (int i = 0; i < vmAtt.size(); i++)
		{
			for (int j = 0; j < vmAtt[i].size(); j++)
			{
				WString strKey	 = vmAtt[i].GetKeyByPos(j);
				WString strValue = vmAtt[i].GetValueByPos(j);
				if (strKey == L"SheetName")
				{
					part = media.GetPartition(vmAtt[i]);
					WString strDims = part.GetAttribute(L"Dimension");
					fPaperWidth  = ((float)(double)strDims.Token(0)) * fPSPoint;
					fPaperHeight = ((float)(double)strDims.Token(1)) * fPSPoint;

					if (rProductPart.m_strPaper.IsEmpty())
						rProductPart.m_strPaper = media.GetAttribute(L"DescriptiveName").getBytes();
					if (rProductPart.m_strPaper.IsEmpty())
						rProductPart.m_strPaper = media.GetBrand().getBytes();
					if (rProductPart.m_nPaperGrammage == 0)
						rProductPart.m_nPaperGrammage = (int)media.GetWeight();
					if (rProductPart.m_fPaperVolume == 0.0f)
					{
						float fThickness = (float)media.GetThickness();
						if (rProductPart.m_nPaperGrammage > 0)
							rProductPart.m_fPaperVolume = (fabs(fThickness) > 0.0001) ? fThickness/(float)rProductPart.m_nPaperGrammage : 1.0f;
						else
							rProductPart.m_fPaperVolume = 1.0f;
					}
					rProductPart.m_foldingParams.SetShinglingValueX(rProductPart.GetShinglingValueX());
					rProductPart.m_foldingParams.SetShinglingValueXFoot(rProductPart.GetShinglingValueXFoot()); 
					rProductPart.m_foldingParams.SetShinglingValueY(rProductPart.GetShinglingValueY());
					break;
				}
			}
		}
	}
}

void CJDFInputMIS::StrippingGetPlateMedia(JDFStrippingParams strippingParams, float& fPlateWidth, float& fPlateHeight)
{
	JDFMedia media;
	for (int nSkip = 0; ;nSkip++)
	{
		media = strippingParams.GetMedia(nSkip);
		if (media.isNull())
			break;
		if (media.GetAttribute(L"MediaType") != L"Plate")
			continue;

		WString strDims = media.GetAttribute(L"Dimension");
		if (strDims.length() > 1)
		{
			fPlateWidth  = ((float)(double)strDims.Token(0)) * fPSPoint;
			fPlateHeight = ((float)(double)strDims.Token(1)) * fPSPoint;
		}

		vmAttribute vmAtt = media.GetPartMapVector(TRUE);
		if ( ! vmAtt.size())
			continue;

		JDFMedia part;
		for (int i = 0; i < vmAtt.size(); i++)
		{
			for (int j = 0; j < vmAtt[i].size(); j++)
			{
				WString strKey	 = vmAtt[i].GetKeyByPos(j);
				WString strValue = vmAtt[i].GetValueByPos(j);
				if (strKey == L"SheetName")
				{
					part = media.GetPartition(vmAtt[i]);
					WString strDims = part.GetAttribute(L"Dimension");
					if (strDims.length() > 0)
					{
						fPlateWidth  = ((float)(double)strDims.Token(0)) * fPSPoint;
						fPlateHeight = ((float)(double)strDims.Token(1)) * fPSPoint;
					}
					break;
				}
			}
		}
	}
}

void CJDFInputMIS::StrippingGetPressDevice(JDFStrippingParams strippingParams, CString& strPressDeviceName, CString& strPressDeviceID)
{
	JDFDevice device = strippingParams.GetDevice();
	if (device.isNull())
		return;

	CString strID = device.GetID().getBytes();

	strPressDeviceName = device.GetDescriptiveName().getBytes();
	strPressDeviceID = device.GetDeviceID().getBytes();

	if ( ! strPressDeviceID.IsEmpty())
	{
		if (theApp.m_PressDeviceList.GetDeviceIndexByID(strPressDeviceID) < 0)
		{
			CString string; string.Format(_T("DeviceID '%s' not found!\n"), strPressDeviceID);
			if (m_strProtocol.Find(string) < 0)
				m_strProtocol += string;
		}
	}
}

BOOL CJDFInputMIS::ProcessPrinanceBullShit(JDFNode node, int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	vJDFNode vNode = node.GetvJDFNode(L"");
	for (int i = 0; i < vNode.size(); i++)
	{
		JDFNode node = vNode[i];
		if (node.GetType() == L"ConventionalPrinting") 
		{
			CString strPressDeviceName;
			int nPressDeviceIndex = -1;
			JDFResourceLinkPool linkPool = node.GetResourceLinkPool();
			vElement vDevice = linkPool.GetInOutLinks(TRUE, FALSE, L"Device");
			if (vDevice.size())
			{
				JDFDevice device = vDevice[0];
				if ( ! device.isNull())
				{
					strPressDeviceName = device.GetDescriptiveName().getBytes();
					nPressDeviceIndex = theApp.m_PressDeviceList.GetDeviceIndex(strPressDeviceName);
				}
			}

			JDFNodeInfo nodeInfo = node.GetNodeInfo();

			vmAttribute vmAtt = nodeInfo.GetPartMapVector(TRUE);
			if ( ! vmAtt.size())
				continue;

			for (int k = 0; k < vmAtt.size(); k++)
			{
				JDFNodeInfo part = nodeInfo.GetPartition(vmAtt[k]);
				if (part.isNull())
					continue;

				CString strSheetName = part.GetSheetName().getBytes();
				if ( ! strSheetName.IsEmpty())
				{
					pDoc->m_PrintSheetList.AddBlankSheet(CPrintingProfile());
					CPrintSheet* pPrintSheet = &pDoc->m_PrintSheetList.GetTail();
					if (pPrintSheet)
					{
						pPrintSheet->m_strNumerationStyle = strSheetName;
						//CPressDevice* pPressDevice = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetDevice(strPressDeviceName);
						//if ( ! pPressDevice)
						//{
							CPressDevice pressDevice; 
							pressDevice.Create();
							pressDevice.m_strName				= strPressDeviceName;
							pressDevice.m_fPlateWidth			= 0.0f;
							pressDevice.m_fPlateHeight			= 0.0f;
							pressDevice.m_nPressType			= CPressDevice::SheetPress;
							pressDevice.m_fPlateEdgeToPrintEdge	= 0.0f;
							pressDevice.m_fGripper				= 0.0f;
							pressDevice.m_fXRefLine				= 0.0f;
							pressDevice.m_fYRefLine1			= 0.0f;
							pressDevice.m_fYRefLine2			= 0.0f;
							//pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.AddTail(pressDevice);
							//nPressDeviceIndex = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetCount() - 1;
						//}
						//else
						//	nPressDeviceIndex = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetDeviceIndex(strPressDeviceName);
						CLayout* pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
						if (pLayout)
						{
							pLayout->GetPrintingProfile().m_press.m_pressDevice			= pressDevice;
							pLayout->GetPrintingProfile().m_press.m_backSidePressDevice	= pressDevice;
						}
							//pLayout->m_FrontSide.m_nPressDeviceIndex = pLayout->m_BackSide.m_nPressDeviceIndex = nPressDeviceIndex;
					}
				}
			}
		}
	}

	return TRUE;
}

int CJDFInputMIS::MapOrientation(int nJDFOrientation)
{
	switch (nJDFOrientation)
	{
	case JDFElement::Orientation_Rotate0:	return ROTATE0;
	case JDFElement::Orientation_Rotate90:	return ROTATE90;
	case JDFElement::Orientation_Rotate180:	return ROTATE180;
	case JDFElement::Orientation_Rotate270:	return ROTATE270;
	case JDFElement::Orientation_Flip0:		return FLIP0;
	case JDFElement::Orientation_Flip90:	return FLIP90;
	case JDFElement::Orientation_Flip180:	return FLIP180;
	case JDFElement::Orientation_Flip270:	return FLIP270;
	}

	return ROTATE0;
}

void CJDFInputMIS::JDFStripCalcSheetDims(JDFStrippingParams part, CLayout* pLayout, CFoldSheet& rFoldSheet, float& fWidth, float& fHeight)
{
	int			nPosIndex = 0;
	JDFPosition position  = part.GetPosition(nPosIndex++);
	while ( ! position.isNull())
	{
		JDFRectangle relBox = position.GetRelativeBox();
		
		int nOrientation = MapOrientation(position.GetOrientation());

		int nLayerIndex = 0;
		float fCutWidth, fCutHeight;
		rFoldSheet.GetCutSize(nLayerIndex, fCutWidth, fCutHeight);
		switch (nOrientation)
		{
		case ROTATE90:	
		case ROTATE270: 
		case FLIP90:	
		case FLIP270:	{ 
							float fTemp = fCutWidth; fCutWidth = fCutHeight; fCutHeight = fTemp; 
						}
						break;
		default:		break;
		}

		float fRelBoxWidth  = (float)(relBox.GetUrx() - relBox.GetLlx());
		float fRelBoxHeight = (float)(relBox.GetUry() - relBox.GetLly());
		if ( (fRelBoxWidth > 0.0f) && (fRelBoxHeight > 0.0f) )
		{
			fWidth  = max(fWidth,  fCutWidth  / fRelBoxWidth);
			fHeight = max(fHeight, fCutHeight / fRelBoxHeight);
		}

		position = part.GetPosition(nPosIndex++);
	}
}

BOOL CJDFInputMIS::JDFStripFoldSheet(CPrintSheet* pPrintSheet, JDFStrippingParams strippingParams, int nPartMapPos, CFoldSheet* pFoldSheet, int nLayerIndex, float fFoldSheetPosX, float fFoldSheetPosY, CBoundProductPart& rProductPart, int nOrientation)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if ( ! pLayout || ! pFoldSheet)
		return FALSE;

	CFoldSheetLayerRef layerRef((short)(pDoc->m_Bookblock.m_FoldSheetList.GetSize() - 1), (short)0);
	pPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);

	JDFAddFoldSheetLayer(pLayout, pFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, rProductPart, nOrientation, strippingParams, nPartMapPos);

	return TRUE;
}

void CJDFInputMIS::JDFAddFoldSheetLayer(CLayout* pLayout, CFoldSheet* pFoldSheet, int nLayerIndex, float fFoldSheetPosX, float fFoldSheetPosY, CBoundProductPart& rProductPart, int nOrientation, JDFStrippingParams strippingParams, int nPartMapPos)
{
	CFoldSheetLayer* pLayer = pFoldSheet->GetLayer(nLayerIndex);
	if ( ! pLayer)
		return;
	if ( ! pLayer->m_HeadPositionArray.GetSize())
		return;

	int nPartMapPosStart = nPartMapPos;

	CString strPage; 
	int		nPageIndex = -1;
	int		nComponentRefIndex = pLayout->m_FoldSheetLayerRotation.GetSize();
	int		nNumX = pLayer->m_nPagesX;
	int		nNumY = pLayer->m_nPagesY;
	float	fLeft = fFoldSheetPosX, fBottom = fFoldSheetPosY;
	for (int ny = 0; ny < nNumY; ny++)
	{
		float fPageRowHeight = 0.0f;
		for (int nx = 0; nx < nNumX; nx++)
		{
			nPageIndex++;

			JDFStripCellParams stripCellParams;
			vmAttribute vmAtt = strippingParams.GetPartMapVector(TRUE);
			nPartMapPos = nPartMapPosStart;
			while (nPartMapPos < vmAtt.size())
			{
				JDFStrippingParams part = strippingParams.GetPartition(vmAtt[nPartMapPos++]);
				if (part.GetCellIndex().size())
				{
					JDF::vint vInt = part.GetCellIndex().ToVInt();
					if (vInt.size())
						if (vInt[0] == nPageIndex)
						{
							stripCellParams = part.GetStripCellParams();
							break;
						}
				}
			}

			float fPageWidth  = rProductPart.m_fPageWidth;
			float fPageHeight = rProductPart.m_fPageHeight;

			float fPageBoxWidth, fPageBoxHeight, fPageLeft, fPageBottom, fPageRight, fPageTop;
			if ( ! stripCellParams.isNull())
			{
				int	  nOverFoldSide;
				float fOverFold, fHeadTrim, fFootTrim, fSideTrim, fSpine;
				GetJDFStripCellParams(fPageWidth, fPageHeight, fOverFold, nOverFoldSide, fHeadTrim, fFootTrim, fSideTrim, fSpine, stripCellParams);
				pFoldSheet->GetPageCutBox(pLayer, nPageIndex, rProductPart.m_nPageOrientation, fPageWidth, fPageHeight, fOverFold, nOverFoldSide, fHeadTrim, fFootTrim, fSideTrim, fSpine/2.0f, fPageBoxWidth, fPageBoxHeight, fPageLeft, fPageBottom, fPageRight, fPageTop);
			}
			else
				pFoldSheet->GetPageCutBox(pLayer, nPageIndex, rProductPart, fPageBoxWidth, fPageBoxHeight, fPageLeft, fPageBottom, fPageRight, fPageTop);

			if ( (pLayer->m_HeadPositionArray[nPageIndex] == LEFT) || (pLayer->m_HeadPositionArray[nPageIndex] == RIGHT) )
			{
				float fTemp = fPageWidth; fPageWidth = fPageHeight; fPageHeight = fTemp;
			}

			///// frontside
			strPage.Format("%d", pLayer->m_FrontPageArray[nPageIndex]);
			CLayoutObject layoutObjectFront;
			layoutObjectFront.CreatePage(fLeft + fPageLeft, fBottom + fPageBottom, fPageWidth, fPageHeight, strPage, rProductPart.m_strFormatName, (BYTE)rProductPart.m_nPageOrientation, 
										 *pLayer, (BYTE)nPageIndex, (BYTE)nComponentRefIndex, (BYTE)FRONTSIDE);
			fLeft += fPageBoxWidth;
			fPageRowHeight = max(fPageRowHeight, fPageBoxHeight);

			pLayout->m_FrontSide.m_ObjectList.AddTail(layoutObjectFront);

			///// backside
			strPage.Format("%d", pLayer->m_BackPageArray[nPageIndex]);
			CLayoutObject layoutObjectBack;
			layoutObjectBack.CreatePage(fLeft + fPageLeft, fBottom + fPageBottom, fPageWidth, fPageHeight, strPage, rProductPart.m_strFormatName, (BYTE)rProductPart.m_nPageOrientation, 
										*pLayer, (BYTE)nPageIndex, (BYTE)nComponentRefIndex, (BYTE)BACKSIDE);

			pLayout->m_BackSide.m_ObjectList.AddTail(layoutObjectBack);
			pLayout->m_BackSide.m_ObjectList.GetTail().AlignToCounterPart(&pLayout->m_FrontSide.m_ObjectList.GetTail());
		}

		fLeft	 = fFoldSheetPosX;
		fBottom += fPageRowHeight;
	}

	pLayout->m_FoldSheetLayerRotation.Add(0);
	pLayout->m_FoldSheetLayerTurned.Add(FALSE);

	switch (nOrientation)
	{
	case ROTATE0:	break;

	case ROTATE90:	pLayout->RotateFoldSheet90(nComponentRefIndex);			break;

	case ROTATE180: pLayout->RotateFoldSheet90(nComponentRefIndex); 
					pLayout->RotateFoldSheet90(nComponentRefIndex);			break;

	case ROTATE270: pLayout->RotateFoldSheet90(nComponentRefIndex, TRUE);	break;

	case FLIP0:		pLayout->TurnFoldSheet	  (nComponentRefIndex);
					pLayout->RotateFoldSheet90(nComponentRefIndex);
					pLayout->RotateFoldSheet90(nComponentRefIndex);			break;

	case FLIP90:	pLayout->RotateFoldSheet90(nComponentRefIndex);
					pLayout->TurnFoldSheet	  (nComponentRefIndex);
					pLayout->RotateFoldSheet90(nComponentRefIndex);
					pLayout->RotateFoldSheet90(nComponentRefIndex);			break;

	case FLIP180:	pLayout->TurnFoldSheet	  (nComponentRefIndex);			break;

	case FLIP270:	pLayout->RotateFoldSheet90(nComponentRefIndex);
					pLayout->TurnFoldSheet	  (nComponentRefIndex);			break;
	}

	CObArray objectPtrList;
	float fSourceBBoxLeft, fSourceBBoxBottom, fSourceBBoxRight, fSourceBBoxTop;
	pLayout->m_FrontSide.GetFoldSheetObjects(nComponentRefIndex, &objectPtrList, &fSourceBBoxLeft,  &fSourceBBoxBottom, &fSourceBBoxRight, &fSourceBBoxTop);
	float fXDiff = fFoldSheetPosX - fSourceBBoxLeft;
	float fYDiff = fFoldSheetPosY - fSourceBBoxBottom;
	for (int i = 0; i < objectPtrList.GetSize(); i++)
	{
		CLayoutObject* pLayoutObject = (CLayoutObject*)objectPtrList[i];	 
		CLayoutObject* pCounterPartObject = pLayoutObject->FindCounterpartObject();
		pLayoutObject->Offset(fXDiff, fYDiff);
		pCounterPartObject->AlignToCounterPart(pLayoutObject);
	}

	pLayout->m_FrontSide.FindCutBlocks();
	pLayout->m_FrontSide.AnalyseCutblocks();
	pLayout->m_BackSide.FindCutBlocks();
	pLayout->m_BackSide.AnalyseCutblocks();
}

void CJDFInputMIS::JDFCreateFoldSheet(JDFBinderySignature binderySignature, CFoldSheet& rFoldSheet, int nProductPartIndex)
{
	JDFXYPair numUp = binderySignature.GetNumberUp();
	rFoldSheet.Create((int)numUp.GetX(), (int)numUp.GetY(), binderySignature.GetDescriptiveName().getBytes(), nProductPartIndex);
	CFoldSheetLayer* pLayer = rFoldSheet.GetLayer(0);
	if ( ! pLayer)
		return;

	int nPageArrayIndex		= 0;
	int nSignatureCellIndex = 0;
	JDFSignatureCell signatureCell = binderySignature.GetSignatureCell(nSignatureCellIndex++);
	while ( ! signatureCell.isNull())
	{
		JDFIntegerList frontPageList = signatureCell.GetFrontPages();
		JDFIntegerList backPageList  = signatureCell.GetBackPages();
		for (int i = 0; i < frontPageList.size(); i++)
		{
			if (nPageArrayIndex < pLayer->m_FrontPageArray.GetSize())
			{
				pLayer->m_FrontPageArray[nPageArrayIndex] = (int)frontPageList[i] + 1;
				switch(signatureCell.GetOrientation())
				{
				case JDFSignatureCell::Orientation_Up:		pLayer->m_HeadPositionArray[nPageArrayIndex] = TOP;		break;
				case JDFSignatureCell::Orientation_Down:	pLayer->m_HeadPositionArray[nPageArrayIndex] = BOTTOM;	break;
				case JDFSignatureCell::Orientation_Left:	pLayer->m_HeadPositionArray[nPageArrayIndex] = LEFT;	break;
				case JDFSignatureCell::Orientation_Right:	pLayer->m_HeadPositionArray[nPageArrayIndex] = RIGHT;	break;
				}
			}
			if (nPageArrayIndex < pLayer->m_BackPageArray.GetSize())
				if (i < backPageList.size())
					pLayer->m_BackPageArray[nPageArrayIndex]  = (int)backPageList[i] + 1;
			nPageArrayIndex++;
		}

		signatureCell = binderySignature.GetSignatureCell(nSignatureCellIndex++);
	}

	/// find max pagenum in order to get where the spine is, because we might have signatures with multiple, identical signature cells
	int nPageMax = 0;
	for (int i = 0; i < pLayer->m_FrontPageArray.GetSize(); i++)
		nPageMax = max(nPageMax, pLayer->m_FrontPageArray[i]);
	for (int i = 0; i < pLayer->m_BackPageArray.GetSize(); i++)
		nPageMax = max(nPageMax, pLayer->m_BackPageArray[i]);

	rFoldSheet.m_nPagesLeft  = nPageMax/2;
	rFoldSheet.m_nPagesRight = nPageMax/2;
	if (rFoldSheet.m_nPagesLeft % 2)	// workaround to determine left/right pages in case of unsymmetrical sheets
	{									// this does work correctly with 50% chance. I have actually no algorithm to find out left/right correctly.
		rFoldSheet.m_nPagesLeft--;
		rFoldSheet.m_nPagesRight++;
	}
	for (int i = 0; i < pLayer->m_FrontPageArray.GetSize(); i++)
		pLayer->m_FrontSpineArray[i] = (pLayer->m_FrontPageArray[i] <= rFoldSheet.m_nPagesLeft) ? LEFT : RIGHT;
	for (int i = 0; i < pLayer->m_BackPageArray.GetSize(); i++)
		pLayer->m_BackSpineArray[i] = (pLayer->m_BackPageArray[i]   <= rFoldSheet.m_nPagesLeft) ? LEFT : RIGHT;
}
