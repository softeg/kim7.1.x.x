#if !defined(AFX_PAGELISTTABLEVIEW_H__CB106D30_1F02_11D1_8BDB_40411A000000__INCLUDED_)
#define AFX_PAGELISTTABLEVIEW_H__CB106D30_1F02_11D1_8BDB_40411A000000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PageListTableView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPageListTableView view

class CPageListTableView : public CScrollView
{
protected:
	CPageListTableView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPageListTableView)

// Attributes
protected:
	CHeaderCtrl  m_HeaderCtrl;
	int			 m_nHeaderHeight;
	CFont		 m_HeaderFont;
	int			 m_nHeaderItemWidthIndex;
	int			 m_nHeaderItemWidthPageName;
	int			 m_nHeaderItemWidthPSpath;
	int			 m_nHeaderItemWidthFormat;
	int			 m_nHeaderItemWidthOffset;
	int			 m_nHeaderItemWidthScale;
	int			 m_nHeaderItemsTotalWidth; 
	CSize 		 m_TemplateListSize;
	int			 m_nViewBorder;
	CPoint		 m_p[15];
	int			 m_nCharHeight, m_nCharWidth;

	float		 m_fScreenPrinterRatioX;
	float		 m_fScreenPrinterRatioY;
	CRect		 m_rcPrintMargins;
	BOOL		 m_bInitialUpdate;
	int			 m_nStartIndex;
	int			 m_nCurPrintPage;

public:
	// Must have access to m_DisplayList from PageListframe & PageListView
	DECLARE_DISPLAY_LIST(CPageListTableView, OnMultiSelect, YES, OnStatusChanged, NO)
	DECLARE_DISPLAY_ITEM(CPageListTableView, PageTemplate,  OnSelect, NO,  OnDeselect, NO,  OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)
	DECLARE_DISPLAY_ITEM(CPageListTableView, PDFDocument,   OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)

// Operations
protected:
	CImpManDoc* GetDocument();
	void		CalcTemplateListSize();
	void		DrawTemplateInfos(CDC* pDC, CPageTemplate* pTemplate, POSITION& rPos, CRect& rOutRect, int& rIndex, BOOL& rBackGround);
	int			GetNumLines(CPageTemplate* pTemplate);
	inline int	GetHeaderItemWidthIndex()		{return (int)(m_nHeaderItemWidthIndex		* m_fScreenPrinterRatioX); };
	inline int	GetHeaderItemWidthPageName()	{return (int)(m_nHeaderItemWidthPageName	* m_fScreenPrinterRatioX); };
	inline int	GetHeaderItemWidthPSpath()		{return (int)(m_nHeaderItemWidthPSpath		* m_fScreenPrinterRatioX); };
	inline int	GetHeaderItemWidthFormat()		{return (int)(m_nHeaderItemWidthFormat		* m_fScreenPrinterRatioX); };
	inline int	GetHeaderItemWidthOffset()		{return (int)(m_nHeaderItemWidthOffset		* m_fScreenPrinterRatioX); };
	inline int	GetHeaderItemWidthScale()		{return (int)(m_nHeaderItemWidthScale		* m_fScreenPrinterRatioX); };
	inline int  GetCharWidth()					{return (int)(m_nCharWidth					* m_fScreenPrinterRatioX); };
	inline int  GetCharHeight()					{return (int)(m_nCharHeight					* m_fScreenPrinterRatioX); };
public:
	static void	FitStringToWidth(CDC* pDC, CString& rString, int nWidth);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageListTableView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();     // first time after construct
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	virtual void PostNcDestroy();
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPageListTableView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CPageListTableView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg void OnHeaderTrack	 (NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnHeaderEndTrack(NMHDR* pNotifyStruct, LRESULT* result);
};

#ifndef _DEBUG  // debug version in PageListTableView.cpp
inline CImpManDoc* CPageListTableView::GetDocument()
   { return (CImpManDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAGELISTTABLEVIEW_H__CB106D30_1F02_11D1_8BDB_40411A000000__INCLUDED_)
