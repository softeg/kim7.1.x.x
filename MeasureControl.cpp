
/////////////////////////////////////////////////////////////////////////////
// special data exchange for measurement edit controls


#include "stdafx.h"
#include "Imposition Manager.h"


#define _DECIMALPOINT() ( (theApp.settings.m_iLanguage == 0) ? ',' : '.')


TCHAR	szSupportedUnits[NO_SUPPORTED_UNITS][3] = {_T("mm"), _T("cm"), _T("in")};
CString strDummy("");


/////////////////////////////////////////////////////////////////////////////
// Public functions

void AFXAPI DDX_Measure(CDataExchange* pDX, int nIDC, float& fValue, int nPrecision)
{
	HWND hWndCtrl = pDX->PrepareEditCtrl(nIDC);
	if (pDX->m_bSaveAndValidate)
	{
		if (!GetMeasure(hWndCtrl, fValue))
		{
			AfxMessageBox(IDS_INVALID_MEASURE);
			pDX->Fail();
		}
	}
	else
	{
		CString strDummy;
		SetMeasure(hWndCtrl, fValue, strDummy, FALSE, nPrecision);
	}
}


BOOL GetMeasure(HWND hWnd, float& fValue, CString& rstrValue)
{
	TCHAR szValuePart[20], szUnitPart[5];
	int	  nUnit;


	if (hWnd)
		::GetWindowText(hWnd, szValuePart, 19);
	else
		_tcscpy(szValuePart, (LPCTSTR)rstrValue);
	
	if (!SeparateMeasureString(szValuePart, szUnitPart))
		return FALSE;

	fValue = Ascii2Float(szValuePart);

	if (!_tcslen(szUnitPart))
		nUnit = theApp.settings.m_iUnit;	// no unit, take from settings
	else
	{
		nUnit = DetermineUnit(szUnitPart);
		if (nUnit == -1)
			return FALSE;			// not mm, cm or inch -> invalid
	}

	switch (nUnit)
	{
	case MILLIMETER:	// mm, no conversion necessary
			break;

	case CENTIMETER:	// cm, convert to mm
		fValue*= 10.0f;
		break;

	case INCH:			// inch, convert to mm
		fValue*= 25.4f;
		break;
	}

	return TRUE;		// entered string is valid
}


BOOL SeparateMeasureString(TCHAR *szValuePart, TCHAR *szUnitPart)
{	
	TCHAR *poi;


	if (!_tcslen(szValuePart))
	{
		_tcscpy(szValuePart, _T("0"));
		_tcscpy(szUnitPart, _T(""));
		return TRUE;
	}

	_tcslwr(szValuePart);
	poi = szValuePart;	
													// scan until end is reached
	while (!InFirstOfUnit(*poi) && (*poi!= '\0'))	// or 1st character of unit was found
		if (!isdigit(*poi) && (*poi != '+') && (*poi != '-') && (*poi != '.') && (*poi != ',') && (*poi != ' '))	// '.' or ',' both allowed as decimal
			return FALSE;							// if anything other than digit, '+', '-', '.', ',' or ' '
		else										// is found and not InFirstOfUnit -> invalid
			poi++;									
													
	if (*poi != '\0')			
	{
		_tcsncpy(szUnitPart, poi, 2); szUnitPart[2] = '\0';
		*poi = '\0';  // end of 1st part
	}
	else
		_tcscpy(szUnitPart, _T(""));

	poi = _tcschr(szValuePart, ',');	
	if ( ! poi)
		poi = _tcschr(szValuePart, '.');	

	if (poi)	// if point or comma found (decimal), change to current language dependend decimal, so sscanf can work properly
		*poi = _DECIMALPOINT();

	return TRUE;
}

BOOL InFirstOfUnit(TCHAR cChar)	// look if character is 1st character of unit token
{
	for (int i = 0; i < NO_SUPPORTED_UNITS; i++)
		if (cChar == szSupportedUnits[i][0])
			return TRUE;

	return FALSE;
}


int DetermineUnit(TCHAR *szString)	// look if szString is a supported unit
{									// and if yes, return index
	for (int i = 0; i < NO_SUPPORTED_UNITS; i++)
		if (_tcscmp(szString, szSupportedUnits[i]) == 0)
			return i;

	return -1;
}


void SetMeasure(HWND hWnd, float fValue, CString& rstrValue, BOOL bNoUnit, int nPrecision)
{
	CString strValuePart, strUnitPart;

	strValuePart = GetValueString(fValue, nPrecision);
	strUnitPart	= (bNoUnit) ? _T("") : GetUnitString();

	TrimMeasure(strValuePart.GetBuffer(MAX_TEXT));
	strValuePart.ReleaseBuffer();

	if (hWnd)
		::SetWindowText(hWnd, strValuePart + strUnitPart);
	else
		rstrValue = strValuePart + strUnitPart;
}


CString GetValueString(float fValue, int nPrecision)
{
	CString strValue;

	if ( (fValue < -999999.9f) || (fValue > 999999.9f) )
		return _T("");

	CString strFormat;
	if (nPrecision > 0)
		strFormat.Format(_T("%%.%df"), nPrecision);
	else
		strFormat.Format(_T("%%.%df"), ( (theApp.settings.m_iUnit == MILLIMETER) || (theApp.settings.m_iUnit == CENTIMETER) ) ? 2 : 3);

	switch (theApp.settings.m_iUnit)
	{
	case MILLIMETER:
		strValue.Format(strFormat, fValue);
		break;
	case CENTIMETER:
		strValue.Format(strFormat, fValue/10.0f);
		break;
	case INCH:
		strValue.Format(strFormat, fValue/25.4f);
		break;
	default:
		strValue.Format(strFormat, fValue);
		break;
	}

	return strValue;
}


CString GetUnitString()
{
	CString strUnit;

	switch (theApp.settings.m_iUnit)
	{
	case MILLIMETER:
		strUnit = _T(" mm");
		break;
	case CENTIMETER:
		strUnit = _T(" cm");
		break;
	case INCH:
		strUnit = _T(" in");
		break;
	default:
		strUnit = _T(" mm");
		break;
	}

	return strUnit;
}


void TrimMeasure(TCHAR *szValueString)
{
	TCHAR *poi;


	poi = &szValueString[_tcslen(szValueString) - 1];
	while ((*poi == '0') || (*poi == _DECIMALPOINT()))	// truncate trailing zeros and 
	{										// eventually the decimal point/comma
		if (*poi == _DECIMALPOINT())
		{
			*poi = '\0';
			break;
		}

		*poi = '\0';
		poi--;
	}

	poi = _tcschr(szValueString, _DECIMALPOINT());

	if (poi)
		if (theApp.settings.m_iLanguage == 0)  // German
			*poi = ',';
}


void SpinMeasure(CWnd *pWnd, int iDelta, float fStep, CString& rstrValue, BOOL bNoUnit)	// spin button pressed -> decrease/increase value
{
	TCHAR szValuePart[20], szUnitPart[5], *poi; 
	float fValue;
	int   nUnit;


	if (pWnd)
		pWnd->GetWindowText(szValuePart, 19);
	else
		_tcscpy(szValuePart, (LPCTSTR)rstrValue);

	if (bNoUnit)
		nUnit = NO_UNIT;
	else
	{
		if (!SeparateMeasureString(szValuePart, szUnitPart))
		{
			_tcscpy(szValuePart, _T("0"));	// if invalid string, set value to 0.0 and unit to default
			nUnit = theApp.settings.m_iUnit;
		}
		else
			nUnit = DetermineUnit(szUnitPart);
	}

	fValue = Ascii2Float(szValuePart);
		
	switch (nUnit)
	{
	case MILLIMETER:
		fValue+= fStep * (float)iDelta;
		_stprintf(szValuePart, _T("%.2f"), fValue);
		_tcscpy(szUnitPart, _T(" mm"));
		break;
	case CENTIMETER:
		fValue+= fStep * (float)iDelta;
		_stprintf(szValuePart, _T("%.3f"), fValue);
		_tcscpy(szUnitPart, _T(" cm"));
		break;
	case INCH:
		fValue+= 0.01f * (float)iDelta;
		_stprintf(szValuePart, _T("%.3f"), fValue);
		_tcscpy(szUnitPart, _T(" in"));
		break;
	case NO_UNIT:
		fValue+= fStep * (float)iDelta;
		_stprintf(szValuePart, _T("%.2f"), fValue);
		_tcscpy(szUnitPart, _T(""));
		break;
	default:
		fValue+= fStep * (float)iDelta;
		_stprintf(szValuePart, _T("%.2f"), fValue);
		_tcscpy(szUnitPart, _T(" mm"));
		break;
	}

 	poi = _tcschr(szValuePart, _DECIMALPOINT());
	if (poi)
		if (theApp.settings.m_iLanguage == 0)  // German
			*poi = ',';

	if (pWnd)
		pWnd->SetWindowText(_tcscat(szValuePart, szUnitPart));
	else
		rstrValue = CString(szValuePart) + CString(szUnitPart);
}



float MeasureFormat(float fFloatValue)
{
	switch (theApp.settings.m_iUnit)
	{
	case MILLIMETER	: return fFloatValue;
	case CENTIMETER	: return fFloatValue / 10.0f;
	case INCH		: return fFloatValue / 25.4f;
	default			: return fFloatValue;
	}
}

CString MeasureFormat(CString strFormat)
{
	TCHAR c;
	int i;
	BOOL bDotFound = FALSE;
	int nLength = strFormat.GetLength();

	switch (theApp.settings.m_iUnit)
	{
	case MILLIMETER	: return strFormat;
	case CENTIMETER	: 
	case INCH		: for (i = 0; i < nLength; i++)
					  {
						  c = strFormat.GetAt(i);
						  if (c == '%' || c == _DECIMALPOINT() || c == 'f' || c == 'F')
						  {
							  if (c == _DECIMALPOINT())
								  bDotFound = TRUE;
							  continue;
						  }
						  if (bDotFound)
							c++;
						  else
							c--;
						  strFormat.SetAt(i, c);
					  }
					  return strFormat;
	default			: return strFormat;
	}
}
