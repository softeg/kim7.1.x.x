#pragma once

#include "PaperDefsSelection.h"
#include "DlgPageFormats.h"
#include "DlgPaperDefs.h"
#include "BindingDefsSelection.h"


// CDlgBoundProductDetails-Dialogfeld

class CDlgBoundProductDetails : public CDialog
{
	DECLARE_DYNAMIC(CDlgBoundProductDetails)

public:
	CDlgBoundProductDetails(CWnd* pParent = NULL);   // Standardkonstruktor
	virtual ~CDlgBoundProductDetails();

// Dialogfelddaten
	enum { IDD = IDD_PRODUCTGROUPBOUNDDETAILS };

public:
	CBoundProductPart		m_boundProductPart;
	int						m_nBoundProductPartIndex;
	CImageList*				m_pBindingImageList;
	CPageFormatSelection 	m_boundProductPartFormatSelection;
	CPaperDefsSelection	 	m_boundProductPartPaperDefsSelection;
	CString				 	m_strBoundProductPartFormatName;
	CString				 	m_strBoundProductPartPaper;

protected:
	CString	m_strOldFormatName;
	float	m_fOldWidth, m_fOldHeight;
	int		m_nOldPageOrientation;
	CSpinButtonCtrl	m_boundProductPartWidthSpin;
	CSpinButtonCtrl	m_boundProductPartHeightSpin;
	int		m_nBoundProductPartPageOrientation;
	float	m_fBoundProductPartHeight;
	float	m_fBoundProductPartWidth;
	int		m_nBindingMethod;
	CBindingDefsSelection	m_bindingDefsSelection;
	CString m_strBindingName;
	int		m_nBoundProductPartPaperGrammage;
	float	m_fBoundProductPartPaperVolume;
	int		m_nBoundProductPartAmount;
	int		m_nBoundProductPartExtra;


protected:
	void KeepCurrentPageFormat();
	BOOL PageFormatChanged();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposProductpartHeightSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposProductpartWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEnKillfocusBoundProductPartHeight();
	afx_msg void OnEnKillfocusBoundProductPartWidth();
	afx_msg void OnProductpartPortrait();
	afx_msg void OnProductpartLandscape();
	afx_msg void OnSelchangeProductpartPaper();
	afx_msg void OnBnClickedModifyProductpartFormat();
	afx_msg void OnBnClickedModifyProductpartPaper();
	afx_msg void OnSelchangeProductpartFormat();
	afx_msg void OnBnClickedOk();
	afx_msg void OnDeltaposODQuantitySpin(NMHDR* pNMHDR, LRESULT* pResult);
	CString m_strComment;
};
