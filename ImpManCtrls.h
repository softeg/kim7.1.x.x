// ImpmanCtrls.h : header file
//


/////////////////////////////////////////////////////////////////////////////////////////////

class CColorInfoListBox : public CListBox
{
public:
	CColorDefinitionTable m_ColorDefTable;

//Attributes:
public:
	int m_nSepStatus;


// Operations
	void AddColorItem(COLORREF color, CString& strName, int nHeaderIndex);
	void DeleteColorItem(int nIndex);
	int  GetSizeOfColorDefTable();

// Implementation
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMIS);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
};

/////////////////////////////////////////////////////////////////////////////////////////////

class CColorListBox : public CListBox
{
public:
	CColorDefinitionTable	m_ColorDefTable;
	CArray <BOOL, BOOL>		m_printStates;


// Operations
	void AddColorItem(COLORREF color, CString& strName, int nColorIndex, int nColorType);
	void DeleteColorItem(int nIndex);
	int  GetSizeOfColorDefTable();

// Implementation
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMIS);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
};

/////////////////////////////////////////////////////////////////////////////////////////////

class CColorComboBox : public CComboBox
{
public:
	CColorDefinitionTable m_ColorDefTable;

	// for WNDCLASS registered window
	static BOOL RegisterControlClass();

// Operations
	void AddColorItem(COLORREF color, CString& strName);
	void DeleteColorItem(int nIndex);
	int  GetSizeOfColorDefTable();

// Implementation
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMIS);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDIS);
};


