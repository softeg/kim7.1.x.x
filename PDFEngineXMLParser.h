#pragma once

#define PROJ_JDFTOOLSLIB
#include <jdf/util/PlatformUtils.h>
#include "JDF.h"
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>

#include "PostScriptParser.h"


using namespace std;
using namespace JDF;
using namespace XERCES_CPP_NAMESPACE;


class CPDFEnginePageListEntry
{
public:
	CPDFEnginePageListEntry()
	{
		m_nIndex   = 0;
		m_nPageNum = 0;
		m_strPageLabel = _T("");
		m_strSeparationColor = _T("");
		m_fTrimBoxLeft	= m_fTrimBoxBottom	= m_fTrimBoxRight  = m_fTrimBoxTop  = 0.0f;
		m_fBleedBoxLeft = m_fBleedBoxBottom = m_fBleedBoxRight = m_fBleedBoxTop = 0.0f;
		m_fCropBoxLeft	= m_fCropBoxBottom	= m_fCropBoxRight  = m_fCropBoxTop  = 0.0f;
		m_fMediaBoxLeft = m_fMediaBoxBottom = m_fMediaBoxRight = m_fMediaBoxTop = 0.0f;
		m_fArtBoxLeft	= m_fArtBoxBottom	= m_fArtBoxRight   = m_fArtBoxTop	= 0.0f;
	};

//Attributes:
public:
	int		m_nIndex;
	int		m_nPageNum;
	CString m_strPageLabel;
	CString m_strSeparationColor;
	float	m_fTrimBoxLeft,	 m_fTrimBoxBottom,	m_fTrimBoxRight,  m_fTrimBoxTop;
	float	m_fBleedBoxLeft, m_fBleedBoxBottom, m_fBleedBoxRight, m_fBleedBoxTop;
	float	m_fCropBoxLeft,  m_fCropBoxBottom,	m_fCropBoxRight,  m_fCropBoxTop;
	float	m_fMediaBoxLeft, m_fMediaBoxBottom, m_fMediaBoxRight, m_fMediaBoxTop;
	float	m_fArtBoxLeft,	 m_fArtBoxBottom,	m_fArtBoxRight,	  m_fArtBoxTop;
	CStringArray m_plateNames;

//Operations:
public:

	const CPDFEnginePageListEntry& operator= (const CPDFEnginePageListEntry& rEntry)
	{
		m_nIndex			 = rEntry.m_nIndex;
		m_nPageNum			 = rEntry.m_nPageNum;
		m_strPageLabel		 = rEntry.m_strPageLabel;
		m_strSeparationColor = rEntry.m_strSeparationColor;
		m_fTrimBoxLeft	= rEntry.m_fTrimBoxLeft;  m_fTrimBoxBottom	= rEntry.m_fTrimBoxBottom;  m_fTrimBoxRight  = rEntry.m_fTrimBoxRight;  m_fTrimBoxTop  = rEntry.m_fTrimBoxTop;
		m_fBleedBoxLeft = rEntry.m_fBleedBoxLeft; m_fBleedBoxBottom = rEntry.m_fBleedBoxBottom; m_fBleedBoxRight = rEntry.m_fBleedBoxRight; m_fBleedBoxTop = rEntry.m_fBleedBoxTop;
		m_fCropBoxLeft	= rEntry.m_fCropBoxLeft;  m_fCropBoxBottom	= rEntry.m_fCropBoxBottom;  m_fCropBoxRight  = rEntry.m_fCropBoxRight;  m_fCropBoxTop  = rEntry.m_fCropBoxTop;
		m_fMediaBoxLeft = rEntry.m_fMediaBoxLeft; m_fMediaBoxBottom = rEntry.m_fMediaBoxBottom; m_fMediaBoxRight = rEntry.m_fMediaBoxRight; m_fMediaBoxTop = rEntry.m_fMediaBoxTop;
		m_fArtBoxLeft	= rEntry.m_fArtBoxLeft;   m_fArtBoxBottom	= rEntry.m_fArtBoxBottom;	m_fArtBoxRight   = rEntry.m_fArtBoxRight;   m_fArtBoxTop   = rEntry.m_fArtBoxTop;
		m_plateNames.RemoveAll();
		m_plateNames.Append(rEntry.m_plateNames);

		return rEntry;
	};

	inline BOOL TrimBoxEmpty()  { return ( (m_fTrimBoxLeft  == 0.0f) && (m_fTrimBoxBottom  == 0.0f) && (m_fTrimBoxRight  == 0.0f) && (m_fTrimBoxTop  == 0.0f) ) ? TRUE : FALSE; };
	inline BOOL BleedBoxEmpty() { return ( (m_fBleedBoxLeft == 0.0f) && (m_fBleedBoxBottom == 0.0f) && (m_fBleedBoxRight == 0.0f) && (m_fBleedBoxTop == 0.0f) ) ? TRUE : FALSE; };
	inline BOOL CropBoxEmpty()  { return ( (m_fCropBoxLeft  == 0.0f) && (m_fCropBoxBottom  == 0.0f) && (m_fCropBoxRight  == 0.0f) && (m_fCropBoxTop  == 0.0f) ) ? TRUE : FALSE; };
	inline BOOL MediaBoxEmpty() { return ( (m_fMediaBoxLeft == 0.0f) && (m_fMediaBoxBottom == 0.0f) && (m_fMediaBoxRight == 0.0f) && (m_fMediaBoxTop == 0.0f) ) ? TRUE : FALSE; };
	inline BOOL ArtBoxEmpty()	{ return ( (m_fArtBoxLeft   == 0.0f) && (m_fArtBoxBottom   == 0.0f) && (m_fArtBoxRight   == 0.0f) && (m_fArtBoxTop   == 0.0f) ) ? TRUE : FALSE; };
};

typedef CArray <CPDFEnginePageListEntry, CPDFEnginePageListEntry&> PDFEnginePageList;



class CPDFEngineXMLParser
{
public:
	CPDFEngineXMLParser(void);
	~CPDFEngineXMLParser(void);

public:
	JDFParser*			parser;
	JDFDoc*				m_pJDFDoc;	
	CString				m_strDocumentTitle;
	CString				m_strCreator;
	CString				m_strCreationDate;
	PDFEnginePageList	m_pdfPageList;
	CSpotcolorDefList	m_spotcolorDefList;


public:
	BOOL ParseXML(const CString& strFilePath);
	BOOL ProcessDocumentNode(DOMNode* documentNode);
	BOOL ProcessDocInfoNode(DOMNode* pagesNode);
	BOOL ProcessPagesNode(DOMNode* pagesNode);
	BOOL ProcessPageNode(DOMNode* pageNode);
	BOOL ProcessPlatenamesNode(DOMNode* platenamesNode, CStringArray& plateNames);
	void CopyOldPDFPageList(CPostScriptParser& postScriptParser);
	BOOL ProcessResourcesNode(DOMNode* pagesNode);
	BOOL ProcessColorspacesNode(DOMNode* colorspacesNode);
	BOOL ProcessColorspaceNode(DOMNode* colorspaceNode);
	BOOL ProcessColorspaceSeparationNode(DOMNode* separationNode);
	BOOL ProcessColorspaceDeviceNNode(DOMNode* DeviceNNode);
	BOOL ProcessColorspaceColorantNode(DOMNode* colorantNode);
	BOOL ProcessAlternateValuesNode(DOMNode* alternateValuesNode, CSpotcolorDefs& entry, const CString& strAlternateColorspace);
	BOOL ColorSpaceIsSpot(const CString& strColorSpace);
};
