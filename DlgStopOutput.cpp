// DlgStopOutput.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgStopOutput.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgStopOutput 


CDlgStopOutput::CDlgStopOutput(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgStopOutput::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgStopOutput)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
}


void CDlgStopOutput::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgStopOutput)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgStopOutput, CDialog)
	//{{AFX_MSG_MAP(CDlgStopOutput)
	ON_BN_CLICKED(IDC_STOP_OUTPUT_ALL, OnStopOutputAll)
	ON_BN_CLICKED(IDC_STOP_OUTPUT_SHEET, OnStopOutputSheet)
	ON_BN_CLICKED(IDC_PROCEED_OUTPUT, OnProceedOutput)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDlgStopOutput 

void CDlgStopOutput::OnStopOutputAll() 
{
	EndDialog(IDC_STOP_OUTPUT_ALL);
}

void CDlgStopOutput::OnStopOutputSheet() 
{
	EndDialog(IDC_STOP_OUTPUT_SHEET);
}

void CDlgStopOutput::OnProceedOutput() 
{
	EndDialog(IDC_PROCEED_OUTPUT);
}
 