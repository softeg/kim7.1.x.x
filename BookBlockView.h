///////////////////////////////////////////////////////////////////////////////////////////////////
// BookBlockView.h : interface of the CBookBlockView class
//


class CImpManCntrItem;

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

class CBookBlockView : public CScrollView
{
protected: // create from serialization only
	CBookBlockView();
	DECLARE_DYNCREATE(CBookBlockView)

//	enum {PaperThickness = 6, FoldSheetGap = 2};	// assume paper is 0.2 mm thick
													// assume gap between foldsheets in bookblock is 0.2 mm
// Attributes 

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CImpManDoc* GetDocument();
	// m_pSelection holds the selection to the current CImpManCntrItem.
	// For many applications, such a member variable isn't adequate to
	//  represent a selection, such as a multiple selection or a selection
	//  of objects that are not CImpManCntrItem objects.  This selection
	//  mechanism is provided just to help you get started.

	// TODO: replace this selection mechanism with one appropriate to your app.
	CImpManCntrItem* m_pSelection;

	int		m_nProductIndex;
	int		m_nProductPartIndex;
	int		m_nViewBorder;	// minimum border around bookblock
	CSize	m_docSize;
	CSize	m_viewPortSize;
	int		m_nMaxPaperThickness;
	int		m_nShinglingValueTotal;
	CSize	m_sizeFont;
	CSize	m_maxNameSize, m_maxPageRangeSize;
	CRect	m_rcPrintMargins;
	float	m_fScreenPrinterRatioX,
			m_fScreenPrinterRatioY;
	CSize	m_sizeBookBlockDP;
	CRect	m_rectGotoImpositionScheme;
	CPoint	m_lastHandPos;
	BOOL	m_nZoomToFitMode;

	CList <CZoomState, CZoomState&> m_zoomSteps;	



	DECLARE_DISPLAY_LIST(CBookBlockView, OnMultiSelect, NO, OnStatusChanged, YES)

	DECLARE_DISPLAY_ITEM(CBookBlockView, FoldSheet,	OnSelect, YES, OnDeselect, YES, OnActivate, NO, OnDeactivate, NO, OnDropped, NO, OnMouseMove, NO, OnDrawState, NO, IsSelectable, TRUE)


	COleDropTarget  m_dropTarget;

	BOOL m_bFoldSheetSelected;

// Operations
public:
	static int		GetPaperThickness(int nProductPartIndex);
	static int		GetFoldSheetGap(int nProductPartIndex);
	void			Draw(CDC* pDC);
	void			DrawBookBlock(CDC* pDC);
	void			DrawFoldSheet(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex);
	int				GetFoldSheetPart(POSITION& pos);
	CPoint			DrawFoldSheetLeftPart(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex);
	CPoint			DrawFoldSheetRightPart(CDC* pDC, POSITION& pos, CPoint& startPoint, int nProductPartIndex);
	void			RenderFoldSheet(CDC* pDC, CDescriptionItem& Item, int nSheetThickness, CPoint& cp, int nRadius, int nProductPartIndex);
	void			DrawConnectionLeftRight(CDC* pDC, CDescriptionItem& Item, CPoint endPointLeft, CPoint endPointRight, int nProductPartIndex);
	int				GetFoldSheetRadiusLeftPart(POSITION pos, int nProductPartIndex);
	int				GetFoldSheetRadiusRightPart(POSITION pos, int nProductPartIndex);
	void			ZoomIncrease();
	void			ZoomDecrease();
	void			ZoomToFit();
	void			ZoomWindow(const CPoint& rPoint, BOOL bRButton = FALSE);
	CSize			GetBookBlockSize(int nProductPartIndex);
	BOOL			IsZoomMode();
	void			SetZoomMode(BOOL bMode);
	BOOL			IsHandActive();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBookBlockView)
	public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual void OnInitialUpdate();
	virtual BOOL OnScroll(UINT nScrollCode, UINT nPos, BOOL bDoScroll = TRUE);
	protected:
	virtual BOOL IsSelected(const CObject* pDocItem) const;// Container support
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBookBlockView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	virtual DROPEFFECT OnDragScroll(DWORD dwKeyState, CPoint point);

protected:
// Generated message map functions
	//{{AFX_MSG(CBookBlockView)
	afx_msg void OnContextMenu(CWnd*, CPoint point);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnInsertObject();
	afx_msg void OnCancelEditCntr();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnUpdateDeleteSelectedFoldsheet(CCmdUI* pCmdUI);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDeleteSelectedFoldsheet();
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	afx_msg void OnNcCalcSize( BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
	afx_msg void OnNcPaint();
	afx_msg void OnNcMouseMove	(UINT nHitTest, CPoint point);
	afx_msg void OnNcLButtonDown(UINT nHitTest, CPoint point);
//	afx_msg UINT OnNcHitTest	(				CPoint point);
public:
	afx_msg LRESULT OnNcHitTest(CPoint point);
//	afx_msg void OnPaint();
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};

///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _DEBUG  // debug version in BookBlockView.cpp
inline CImpManDoc* CBookBlockView::GetDocument()
   { return (CImpManDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
