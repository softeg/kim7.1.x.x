#pragma once

#include "afxinet.h"
#include <iostream>

#include "DlgOptimizationMonitor.h"


#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>

using namespace XERCES_CPP_NAMESPACE;


class CsP1LayoutTask : public DOMNode
{
public:
	BOOL AddChildNodes();
};

class CsP1Parameters : public DOMNode
{
public:
	BOOL AddChildNodes();
};

class CsP1Setup : public DOMNode
{
public:
	BOOL AddChildNodes();
};

class CsP1Medias : public DOMNode
{
public:
	BOOL AddMedia(CSheet& rSheet, CString& strSheetID);
};

class CsP1MountedMedias : public DOMNode
{
public:
	BOOL AddMountedMedia(CSheet& rSheet, CString& strSheetID, CPressParams& rPressParams);
};

class CsP1MountedMedia : public DOMNode
{
public:
	BOOL AddPerformance();
};

class CsP1Devices : public DOMNode
{
public:
	BOOL AddPrintingDevice(CPressParams& rPressParams);
};

class CsP1StartSchedule : public DOMNode
{
public:
	BOOL AddChildNodes();
};

class CsP1BinderySignaturePool : public DOMNode
{
public:
	BOOL AddChildNodes();
};

class CsP1BinderySignatures : public DOMNode
{
public:
	BOOL AddBinderySignature(CFoldSheet& rFoldSheet);
	BOOL AddBinderySignature(CFlatProduct& rFlatProduct);
};

class CsP1ConstraintConfiguration : public DOMNode
{
public:
	BOOL AddChildNodes();
};

class CsP1PrintData : public DOMNode
{
public:
	BOOL AddChildNodes();
};

class CsP1EngineConfiguration : public DOMNode
{
public:
	BOOL AddChildNodes();
};

class CsP1GangJobEvent : public DOMNode
{
public:
	BOOL ProcessGangJob();
};

class CsP1GangJob : public DOMNode
{
public:
	BOOL ProcessForm();
};

class CsP1Form : public DOMNode
{
public:
	BOOL ProcessPlacementZone(CPrintSheet* pPrintSheet);
};

class CsP1PlacementZone : public DOMNode
{
public:
	BOOL ProcessBinderySignaturePlacements(CPrintSheet* pPrintSheet);
};

class CsP1BinderySignaturePlacement : public DOMNode
{
public:
	BOOL Process(CPrintSheet* pPrintSheet);
};



class CsPrintOneOptimizer
{
public:
	CsPrintOneOptimizer(CDlgOptimizationMonitor* pMonitor);
	~CsPrintOneOptimizer(void);

public:
	CDlgOptimizationMonitor* m_pMonitor;

	DOMImplementation*		 m_pDOMImplementation;
	XercesDOMParser*		 m_pDOMParser;
	xercesc::DOMDocument*	 m_pRequestDOMDoc;
	xercesc::DOMDocument*	 m_pResponseDOMDoc;
	std::string				 m_XMLInMemBuf;


public:
	void			NotifyStatus(CString strSubject, CString strMsg, BOOL bOverwriteMode = FALSE);
	BOOL			DoOptimizeAll();
	BOOL			DoOptimizePrintGroup(CPrintGroup& rPrintGroup);
	BOOL			MakeCloudServerLayoutTaskRequest();
	BOOL			CancelledByUser();
	int				CloudServerQueryStatusCode(CHttpFile* pFile);
	CString			CloudServerQueryTaskID(CHttpFile* pFile);
	CString			CloudServerGetTaskInfoState(CHttpFile* pFile);
	CString			CloudServerGetLayoutTask(CHttpFile* pFile);
	BOOL			CloudServerParseResponse(CHttpFile* pFile, CString& rstrTitle, CString& rstrMessage);
	BOOL			CreateLayoutTask();
	BOOL			ReleaseLayoutTask();
	BOOL			WriteDOMDocToXMLString(CString& rstrXML, xercesc::DOMDocument* domDoc);
	BOOL			WriteDOMDocToFile(CString strFilePath, xercesc::DOMDocument* domDoc);
	BOOL			ParseXMLStringToDOMDoc(CString& strXML, xercesc::DOMDocument** domDoc);
	BOOL			LayoutTask_AddPrintingDevice(CPressParams& rPressParams);
	BOOL			LayoutTask_AddMedia(CSheet& rSheet, CString& strSheetID);
	BOOL			LayoutTask_AddMountedMedia(CSheet& rSheet, CString& strSheetID, CPressParams& rPressParams);
	BOOL			LayoutTask_AddBinderySignature(CFoldSheet& rFoldSheet);
	BOOL			LayoutTask_AddBinderySignature(CFlatProduct& rFlatProduct);
	BOOL			LayoutTask_ProcessGangJobEvents(CPrintGroup& rPrintGroup);
	static CString	DateTimeToUnixTimestamp(CTime time);
};
