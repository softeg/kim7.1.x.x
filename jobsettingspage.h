// JobSettingsPage.h : header file
//

#ifndef __JOBSETTINGSPAGE_H__
#define __JOBSETTINGSPAGE_H__

/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPage1 dialog

class CJobSettingsPage1 : public CPropertyPage
{
	DECLARE_DYNCREATE(CJobSettingsPage1)

// Construction
public:
	CJobSettingsPage1();
	~CJobSettingsPage1();

// Dialog Data
	//{{AFX_DATA(CJobSettingsPage1)
	enum { IDD = IDD_JOB_SETTINGS_PAGE1 };
	CString	m_strCreator;
	CString	m_strCustomer;
	CString	m_strCustomerNumber;
	CString	m_strJob;
	CString	m_strNotes;
	CString	m_strTitle;
	COleDateTime	m_CreationDate;
	COleDateTime	m_LastModifiedDate;
	CString	m_strScheduledToPrint;
	COleDateTime	m_scheduledToPrint;
	//}}AFX_DATA
	BOOL m_bModified;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CJobSettingsPage1)
	public:
	virtual void OnOK();
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CJobSettingsPage1)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeCreator();
	afx_msg void OnChangeCustomer();
	afx_msg void OnChangeCustomerNumber();
	afx_msg void OnChangeJobNumber();
	afx_msg void OnChangeNotes();
	afx_msg void OnChangeTitle();
	afx_msg void OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeScheduledToPrint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPage2 dialog

class CJobSettingsPage2 : public CPropertyPage
{
	DECLARE_DYNCREATE(CJobSettingsPage2)

// Construction
public:
	CJobSettingsPage2();
	~CJobSettingsPage2();

// Dialog Data
	//{{AFX_DATA(CJobSettingsPage2)
	enum { IDD = IDD_JOB_SETTINGS_PAGE2 };
	CComboBox	m_applicationCategory;
	//}}AFX_DATA
	CDlgApplicationBook	  m_dlgApplicationBook;
	CDlgApplicationLabels m_dlgApplicationLabels;
	BOOL				  m_bModified;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CJobSettingsPage2)
	public:
	virtual void OnOK();
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CJobSettingsPage2)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeApplicationCategory();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPage3 dialog

class CJobSettingsPage3 : public CPropertyPage
{
	DECLARE_DYNCREATE(CJobSettingsPage3)

// Construction
public:
	CJobSettingsPage3();
	~CJobSettingsPage3();

// Dialog Data
	//{{AFX_DATA(CJobSettingsPage3)
	enum { IDD = IDD_JOB_SETTINGS_PAGE3 };
	CSpinButtonCtrl	m_BackTrimSpin;
	CSpinButtonCtrl	m_FootTrimSpin;
	CSpinButtonCtrl	m_SideTrimSpin;
	CSpinButtonCtrl	m_HeadTrimSpin;
	float	m_fSideTrim;
	float	m_fHeadTrim;
	float	m_fFootTrim;
	float	m_fBackTrim;
	int		m_nProductionType;
	CString	m_strPressDeviceName;
	//}}AFX_DATA
	BOOL m_bModified;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CJobSettingsPage3)
	public:
	virtual void OnOK();
	virtual BOOL OnSetActive();
	virtual BOOL OnWizardFinish();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CJobSettingsPage3)
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSideTrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposHeadTrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposFootTrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposBackTrimSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeBackTrim();
	afx_msg void OnChangeFootTrim();
	afx_msg void OnChangeHeadTrim();
	afx_msg void OnChangeSideTrim();
	afx_msg void OnSelchangePressdeviceList();
	afx_msg void OnSelchangeProductionType();
	afx_msg void OnPaperlistButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CJobSettingsPage4 dialog

class CJobSettingsPage4 : public CPropertyPage
{
	DECLARE_DYNCREATE(CJobSettingsPage4)

// Construction
public:
	CJobSettingsPage4();
	~CJobSettingsPage4();

// Dialog Data
	//{{AFX_DATA(CJobSettingsPage4)
	enum { IDD = IDD_JOB_SETTINGS_PAGE4 };
	CComboBoxEx	m_pdfTargetCombo;
	CString	m_strPressDeviceName;
	CString	m_strOutputProfile;
	//}}AFX_DATA

public:
	BOOL m_bModified;
	CPDFTargetList m_pdfTargetList;


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CJobSettingsPage4)
	public:
	virtual void OnOK();
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
	virtual BOOL OnWizardFinish();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL OnLoadingBlockData();
	BOOL OnUpdateBlockData();
	void InitPressDeviceCombo();
	void InitSheetCombo();
	void InitOutputProfileCombo();

	// Generated message map functions
	//{{AFX_MSG(CJobSettingsPage4)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangePressdeviceList();
	afx_msg void OnSelchangeExposerCombo();
	afx_msg void OnSelchangeSheetList();
	afx_msg void OnModifyPressdevice();
	afx_msg void OnModifySheet();
	afx_msg void OnModifyOutputprofile();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};
#endif // __JOBSETTINGSPAGE_H__
