#if !defined(AFX_DLGSHINGLINGASSISTENT_H__13445003_F243_11D2_9551_204C4F4F5020__INCLUDED_)
#define AFX_DLGSHINGLINGASSISTENT_H__13445003_F243_11D2_9551_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "PaperDefsSelection.h"



// DlgShinglingAssistent.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgShinglingAssistent 

class CDlgShinglingAssistent : public CDialog
{
// Konstruktion
public:
	CDlgShinglingAssistent(CWnd* pParent = NULL, BOOL bRunModal = FALSE, int nProductPartIndex = 0);   // Standardkonstruktor
	~CDlgShinglingAssistent();   

// Dialogfelddaten
	//{{AFX_DATA(CDlgShinglingAssistent)
	enum { IDD = IDD_SHINGLING_ASSISTENT };
	CSpinButtonCtrl	m_ShinglingValueXFootTotalSpin;
	CSpinButtonCtrl	m_shinglingValueXFootSpin;
	CSpinButtonCtrl	m_shinglingValueXSpin;
	CSpinButtonCtrl	m_ShinglingValueXTotalSpin;
	CSpinButtonCtrl	m_shinglingValueYSpin;
	CSpinButtonCtrl	m_ShinglingValueYTotalSpin;
	BOOL	m_bShinglingActive;
	float	m_fShinglingValueXTotal;
	float	m_fShinglingValueX;
	float	m_fShinglingValueXFoot;
	float	m_fShinglingValueXFootTotal;
	float	m_fShinglingValueYTotal;
	float	m_fShinglingValueY;
	CComboBoxEx	m_productPartCombo;
	int			m_nLocalProductPartIndex;
	CComboBoxEx	m_shinglingStyleCombo;
	CComboBoxEx m_shinglingDirectionCombo;
	int			m_nShinglingStyle;
	int			m_nShinglingDirection;
	CString		m_strPaperInfo;
	BOOL		m_bShinglingCropMarks;
	//}}AFX_DATA


// Attributes:
public:
	int					m_nProductIndex;
	int					m_nGlobalProductPartIndex;
	CWnd*				m_pParentWnd;
	BOOL				m_bRunModal;
	BOOL				m_bRunEmbedded;
	CFont				m_boldFont;
	int					m_nShinglingMethod;
	CPaperDefsSelection	m_paperDefsSelection;
	CString				m_strPaper;
	int					m_nPaperGrammage;
	float				m_fPaperVolume;
	CString				m_strHeadStaticOrg;


// Operations:
public:
	int					ProductPartGlobalToLocal(int nGlobalProductPart);
	int					ProductPartLocalToGlobal(int nLocalProductPart);
	void				InitShinglingStyle();
	void				InitShinglingDirection();
	CView*				GetBookBlockView();
	void				UpdateViews();
	void				InitProductPartCombo();
	void				InitBottling();
	void				InitShinglingValuesX();
	void				InitShinglingValuesXTotal();
	void				InitShinglingValuesY();
	void				InitShinglingValuesYTotal();
	static float		ShinglingValue2Display(CFoldSheet& rFoldSheet, float fValue, BOOL bShowPaperThickness);
	void				ChangeProductPart();
	void				SetShinglingMethod(int nShinglingMethod);
	void				SetShinglingValueX(float fShinglingValueX);
	void				SetShinglingValueY(float fShinglingValueY);
	void				SetShinglingValueXFoot(float fShinglingValueXFoot);
	void				SetShinglingActive(BOOL bShinglingActive);
	void				SetShinglingDirection(int nShinglingDirection);
	void				SetShinglingCropMarks(BOOL bShinglingCropMarks);



// Überschreibungen
	BOOL Create( UINT nIDTemplate, CWnd* pParentWnd = NULL );

	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgShinglingAssistent)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementierung
protected:
	CToolTipCtrl m_tooltip;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgShinglingAssistent)
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedShinglingPaperButton();
	afx_msg void OnDeltaposShinglingXTotalSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposShinglingXSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposShinglingXFootSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposShinglingXFootTotalSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposShinglingYTotalSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposShinglingYSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusShinglingXEdit();
	afx_msg void OnKillfocusShinglingXFootEdit();
	afx_msg void OnKillfocusShinglingXTotalEdit();
	afx_msg void OnKillfocusShinglingXFootTotalEdit();
	afx_msg void OnKillfocusShinglingYEdit();
	afx_msg void OnKillfocusShinglingYTotalEdit();
	afx_msg void OnShinglingMethodBottling();
	afx_msg void OnCbnSelchangeShinglingProductpartCombo();
	afx_msg void OnCbnSelchangeShinglingStyleCombo();
	afx_msg void OnCbnSelchangeShinglingDirectionCombo();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeShinglingPaperCombo();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedShinglingCropmarks();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGSHINGLINGASSISTENT_H__13445003_F243_11D2_9551_204C4F4F5020__INCLUDED_
