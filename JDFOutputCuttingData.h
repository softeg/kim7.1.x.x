// CJDFOutputCuttingData-Befehlsziel

// fudge for declspec dll...
//#define PROJ_JDFTOOLS
#define PROJ_JDFTOOLSLIB
#include <jdf/util/PlatformUtils.h>
#include "JDF.h"
#include "JDFSourceResource.h"
#include <jdf/io/File.h>
#include <jdf/io/FileInputStream.h>
#include <jdf/io/FileOutputStream.h>
#include <jdf/io/BufferedOutputStream.h>
#include "jdf\mime\MIMEBodyPart.h"
#include <jdf/mime/MIMEMessage.h>
#include <jdf/mime/MIMEMessagePart.h>
#include <jdf/mime/MIMEBasicPart.h>
#include <jdf/mime/MIMEMultiPart.h>
#include <jdf/mime/FileMIMEType.h>
#include <jdf/mime/FileNameMap.h>
#include <jdf/net/URLConnection.h>
#include <iostream>

#include <xercesc/parsers/XercesDOMParser.hpp>

#include <vector>



using namespace std;
using namespace JDF;

#pragma once


class CJDFOutputCuttingData : public CObject
{
public:
	CJDFOutputCuttingData(CPDFTargetProperties& rPDFTarget);
	virtual ~CJDFOutputCuttingData();


//Attributes:
public:
	JDFNode						m_cuttingGroup;
	JDFComponent				m_componentSheet;
	JDFCuttingParams			m_cuttingParams;
	JDFDoc*						m_pJDFDoc;
	JDFCuttingParams*			m_pJDFCuttingParamsRoot;
	int							m_nError;
	BOOL						m_bCancelJob;
	float						m_fMediaWidth;
	float						m_fMediaHeight;
	CString						m_strTargetName;
	CString						m_strLocation;
	CString						m_strOutputJDFPath;
	CString						m_strOutputFilename;
	CString						m_strTmpOutputJDFPath;
	CString						m_strFilenameTemplate;
	int							m_nTargetType;
	int							m_nCurrentPage;
	CMediaSize					m_media;
	int							m_nPlatesProcessed;
	int							m_nCompatibility;
	BOOL						m_bOutputFormatExceeded;


//Operations:
public:
	BOOL				Open(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll);
	void				Close();
	//static IPL_bool_t	ProgressClose( long lowrange, long highrange, long counter, long lparam);
	void				JDFOUT_AppendCuttingParams(CPrintSheet* pPrintSheet = NULL);
	BOOL				ProcessPrintSheet(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll) ;
	BOOL				ProcessPrintSheetsJobInOne(CPrintSheetList* pPrintSheetList, BOOL& bOverwriteAll, CPrintSheet** pPrintSheet);
	BOOL				ProcessPrintSheetAllInOne(CPrintSheet* pPrintSheet, BOOL& bOverwriteAll) ;
	BOOL				CheckTargetProps(CPrintSheet& rPrintSheet);
};


