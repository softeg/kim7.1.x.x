// OrderDataBottomView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "OrderDataFrame.h"
#include "OrderDataBottomView.h"
#include "DlgJobData.h"
#include "MainFrm.h"


// COrderDataBottomView

IMPLEMENT_DYNCREATE(COrderDataBottomView, CFormView)

COrderDataBottomView::COrderDataBottomView()
	: CFormView(COrderDataBottomView::IDD)
{

}

COrderDataBottomView::~COrderDataBottomView()
{
}

void COrderDataBottomView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(COrderDataBottomView, CFormView)
	ON_BN_CLICKED(IDC_JD_MODIFY_JOBDATA, &COrderDataBottomView::OnBnClickedJdModifyJobdata)
END_MESSAGE_MAP()


// COrderDataBottomView-Diagnose

#ifdef _DEBUG
void COrderDataBottomView::AssertValid() const
{
	CFormView::AssertValid();
}
 
#ifndef _WIN32_WCE
void COrderDataBottomView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// COrderDataBottomView-Meldungshandler

void COrderDataBottomView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
}

void COrderDataBottomView::OnBnClickedJdModifyJobdata()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CDlgJobData dlgJobData(IDD_ORDERDATA, 0, (ULONG_PTR)0);
	if (dlgJobData.DoModal() == IDCANCEL)
		return;

	pDoc->SetModifiedFlag();

	Invalidate();
	UpdateWindow();
}
