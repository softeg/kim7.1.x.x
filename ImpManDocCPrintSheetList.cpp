// ImpManDocCPrintSheetList.cpp : implementation of the CImpManDoc subclass CPrintSheetList
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "InplaceEdit.h"	// needed by PrintSheetView.h
#include "InplaceButton.h"

#include "DlgToolBarPopup.h"
#include "MediaPivotControl.h"
#include "DlgPDFTargetProps.h"
#include "DlgPDFOutputMediaSettings.h"
#include "MainFrm.h"
#include "DlgLayoutObjectControl.h"
#include "DlgLayoutObjectProperties.h"
#include "PrintSheetFrame.h"
#include "PrintSheetView.h"
#include "PrintSheetTableView.h"
#include "PrintSheetTreeView.h"
#include "DlgSheet.h"
#include "DlgMarkSet.h"
#include "PrintSheetPlanningView.h"
#include "DlgOptimizationMonitor.h"

 
#include "PDFEngineInterface.h" 
#include "PDFOutput.h"
#include "JDFOutputImposition.h"
#include "JDFOutputCuttingData.h"

#include "CntrItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CPrintSheetList

IMPLEMENT_SERIAL(CPrintSheetList, CObject, VERSIONABLE_SCHEMA | 3)


CPrintSheetList::CPrintSheetList()
{
	m_strIPUJobname.Empty();
	m_bIPUAllcolors	= TRUE;
	m_strIPUColorString.Empty();
}

					   
void CPrintSheetList::Serialize(CArchive& ar)
{
	ar.SerializeClass(RUNTIME_CLASS(CPrintSheetList));

	UINT nVersion = 1;
	if (ar.IsLoading())
		nVersion = ar.GetObjectSchema();

	CList <CPrintSheet, CPrintSheet&>::Serialize(ar);
	m_Layouts.Serialize(ar);

	if (ar.IsLoading())
	{
		m_pdfTargets.RemoveAll();

		switch (nVersion)
		{
		case 1: break;
		case 2: m_pdfTargets.Serialize(ar); 
				break;
		case 3: m_pdfTargets.Serialize(ar); 
				ar >> m_strIPUJobname;
				ar >> m_bIPUAllcolors;
				ar >> m_strIPUColorString;
				break;
		}
	}
	else
	{
		m_pdfTargets.Serialize(ar);
		ar << m_strIPUJobname;
		ar << m_bIPUAllcolors;
		ar << m_strIPUColorString;
	}
}

CPrintSheet* CPrintSheetList::GetFirstPrintSheet(int nProductPartIndex)
{
	if (nProductPartIndex < 0)
	//if (nProductPartIndex == -1)
	{
		if (GetCount())
			return &GetHead();
	}
	else
	{
		POSITION pos = GetHeadPosition();
		while (pos)
		{
			CPrintSheet& rPrintSheet = GetAt(pos);
			if (rPrintSheet.GetPrintGroupIndex() == nProductPartIndex)
				return &GetAt(pos);
			GetNext(pos);
		}
	}
	return NULL;
}

CPrintSheet* CPrintSheetList::GetNextPrintSheet(CPrintSheet* pPrintSheet, int nProductPartIndex)
{
	if (nProductPartIndex < 0)
	//if (nProductPartIndex == -1)
	{
		POSITION pos = GetHeadPosition();
		while (pos)
		{
			CPrintSheet& rPrintSheet = GetNext(pos);
			if (&rPrintSheet == pPrintSheet)
				if (pos)
					return &GetAt(pos);
		}
	}
	else
	{
		BOOL	 bFound = FALSE;
		POSITION pos	= GetHeadPosition();
		while (pos)
		{
			CPrintSheet& rPrintSheet = GetNext(pos);
			if (&rPrintSheet == pPrintSheet)
			{
				bFound = TRUE;
				break;
			}
		}
		if ( ! bFound)
			return NULL;
		while (pos)
		{
			CPrintSheet& rPrintSheet = GetAt(pos);
			if (rPrintSheet.GetPrintGroupIndex() == nProductPartIndex)
				return &GetAt(pos);
			GetNext(pos);
		}
	}
	return NULL;
}

CPrintSheet* CPrintSheetList::GetPrintSheetByID(CString& strID)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (rPrintSheet.GetID() == strID)
			return &rPrintSheet;
	}
	return NULL;
}

CPrintSheet* CPrintSheetList::GetPrintSheetFromNumber(int nPrintSheetNumber)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (rPrintSheet.m_nPrintSheetNumber == nPrintSheetNumber)
			return &rPrintSheet;
	}
	return NULL;
}

int	CPrintSheetList::GetPrintSheetIndex(CPrintSheet* pPrintSheet)
{
	int nIndex = 0;
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (&rPrintSheet == pPrintSheet)
			return nIndex;
		nIndex++;
	}
	return -1;
}

CLayout* CPrintSheetList::GetFirstLayout()
{
	m_alreadyProcessed.RemoveAll();

	if ( ! GetCount())
		return NULL;

	m_alreadyProcessed.Add(GetHead().GetLayout());

	return GetHead().GetLayout();
}

CLayout* CPrintSheetList::GetNextLayout(CLayout* pLayout)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (rPrintSheet.GetLayout() == pLayout)
			break;
	}

	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		pLayout = rPrintSheet.GetLayout();

		if ( ! LayoutAlreadyProcessed(pLayout))
		{
			m_alreadyProcessed.Add(rPrintSheet.GetLayout());
			return pLayout;
		}
	}

	return NULL;
}

BOOL CPrintSheetList::LayoutAlreadyProcessed(CLayout* pLayout)
{
	for (int i = 0; i < m_alreadyProcessed.GetSize(); i++)
	{
		if (m_alreadyProcessed[i] == pLayout)
			return TRUE;
	}
	return FALSE;
}


// After a foldsheet has been removed we have to decrement all indices above the index of deleted foldsheet
void CPrintSheetList::ReIndexFoldSheets(int nIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		for (int i = 0; i < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
			if (rPrintSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex > nIndex)
				rPrintSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex--;				
	}
}


void CPrintSheetList::RemoveFoldSheet(int nIndex)
{
	int		 nPrintSheetNum = 1;	
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		POSITION prevPos = pos;
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (rPrintSheet.m_FoldSheetLayerRefs.GetSize() == 0)
		{
			nPrintSheetNum++;
			continue;
		}

		for (int i = 0; i < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
			if (rPrintSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex == nIndex)
			{
				rPrintSheet.RemoveFoldSheetLayer(i, FALSE, TRUE);
				//rPrintSheet.m_FoldSheetLayerRefs.RemoveAt(i);				
				i--;
			}
		rPrintSheet.m_nPrintSheetNumber = nPrintSheetNum;	// renumber printsheets 
		if ( (rPrintSheet.m_FoldSheetLayerRefs.GetSize() == 0) && ! rPrintSheet.HasFlatProducts() )
			; // RemoveAt(prevPos); already done in RemoveFoldSheetLayer()
		else
			nPrintSheetNum++;

		//TODO: disable corresponding layout objects
	}
}

void CPrintSheetList::RemoveFlatProductType(int nFlatProductIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		for (int i = 0; i < rPrintSheet.m_flatProductRefs.GetSize(); i++)
		{
			if (rPrintSheet.m_flatProductRefs[i].m_nFlatProductIndex == nFlatProductIndex)
			{
				rPrintSheet.RemoveFlatProductType(i, FALSE, TRUE);
				i--;
			}
		}
	}

	pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		for (int i = 0; i < rPrintSheet.m_flatProductRefs.GetSize(); i++)
		{
			if (rPrintSheet.m_flatProductRefs[i].m_nFlatProductIndex > nFlatProductIndex)
				rPrintSheet.m_flatProductRefs[i].m_nFlatProductIndex--;
		}
	}
}

void CPrintSheetList::CheckLayoutObjectFormats(int nRefIndex, float fWidth, float fHeight, const CString strFormatName, int nOrientation, BOOL bTypeLabel)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		CLayout* pLayout = rPrintSheet.GetLayout();
		if (pLayout)
			pLayout->CheckLayoutObjectFormats(nRefIndex, fWidth, fHeight, strFormatName, nOrientation, bTypeLabel);
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// Look for foldsheets which are not yet assigned to any printsheet
// Put those foldsheets to a new printsheet based either on an existing layout or on an newly created 
void CPrintSheetList::TakeOverNewFoldSheets(CPrintSheet* pPrintSheet, CLayout* pLayout, int nProductPartIndex)
{
	return;

	//CImpManDoc* pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return;

	//short nFoldSheetIndex  = 0;
	//POSITION pos  = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	//while (pos)
	//{
	//	CFoldSheet& rFoldSheet = CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.GetNext(pos);
	//	for (short nLayerIndex = 0; nLayerIndex < rFoldSheet.m_LayerList.GetCount(); nLayerIndex++)
	//	{
	//		if ( (FoldSheetLayerIsAssigned(nFoldSheetIndex, nLayerIndex) == 0) && CheckFoldSheetComingAndGoing(rFoldSheet))
	//		{
	//			int		 nLayoutIndex = 0;
	//			CPrintSheet* pPrintSheet = FindSuitablePrintSheet(rFoldSheet, nLayerIndex);	// Look if there is already a suitable printsheet
	//			if (pPrintSheet)													// based on a layout which can be used for the new 
	//				nLayoutIndex = pPrintSheet->m_nLayoutIndex;						// foldsheet layer
	//			else
	//			{
	//				CLayout newLayout;
	//				newLayout.Create(rFoldSheet, nLayerIndex, nProductPartIndex);
	//				m_Layouts.AddTail(newLayout);
	//				nLayoutIndex = m_Layouts.GetCount() - 1;
	//			}
	//			POSITION pos	 = m_Layouts.FindIndex(nLayoutIndex);
	//			CLayout* pLayout = (pos) ? &m_Layouts.GetAt(pos) : NULL;

	//			if (pLayout)
	//			{
	//				CPrintSheet newPrintSheet;
	//				newPrintSheet.Create(nLayoutIndex, rFoldSheet.m_nProductPartIndex, pLayout);	
	//				for (int i = 0; i < pLayout->m_FoldSheetLayerRotation.GetSize(); i++)	// do nUps
	//				{
	//					CFoldSheetLayerRef layerRef(nFoldSheetIndex, nLayerIndex); // foldsheet index, layer index, copy index, layer orientation, layer turned
	//					newPrintSheet.m_FoldSheetLayerRefs.Add(layerRef);
	//				}

	//				int nPrintSheetIndex = GetInsertionPos(rFoldSheet, rFoldSheet.m_nFoldSheetNumber);
	//				if (nPrintSheetIndex < 0)
	//					AddHead(newPrintSheet);
	//				else
	//					if (nPrintSheetIndex >= GetCount())
	//						AddTail(newPrintSheet);
	//					else
	//					{
	//						POSITION sheetPos = FindIndex(nPrintSheetIndex);
	//						CList <CPrintSheet, CPrintSheet&>::InsertAfter(sheetPos, newPrintSheet);
	//					}
	//			}
	//		}
	//	}
	//	nFoldSheetIndex++;
	//}

	//ReNumberPrintSheets();
}

//
//void CPrintSheetList::TakeOverNewFoldSheets(CPrintSheet* pPrintSheet, CLayout* pLayout, int nProductPartIndex)
//{
//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
//	if ( ! pDoc)
//		return;
//
//	short nFoldSheetIndex  = 0;
//	POSITION pos  = pDoc->m_Bookblock.m_FoldSheetList.GetHeadPosition();
//	while (pos)
//	{
//		CFoldSheet& rFoldSheet = CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.GetNext(pos);
//		for (short nLayerIndex = 0; nLayerIndex < rFoldSheet.m_LayerList.GetCount(); nLayerIndex++)
//		{
//			if ( (FoldSheetLayerIsAssigned(nFoldSheetIndex, nLayerIndex) == 0) && CheckFoldSheetComingAndGoing(rFoldSheet))
//			{
//				CPrintSheet newPrintSheet;
//				if ( ! pPrintSheet)
//				{
//					CLayout* pLayout = NULL;
//					int		 nLayoutIndex;
//					CPrintSheet* pSuitablePrintSheet = FindSuitablePrintSheet(rFoldSheet, nLayerIndex);	// Look if there is already a suitable printsheet
//					if (pSuitablePrintSheet)													// based on a layout which can be used for the new 
//					{																			// foldsheet layer
//						newPrintSheet = *pSuitablePrintSheet;
//						newPrintSheet.SubstituteFoldSheetIndex(rFoldSheet, nFoldSheetIndex, nLayerIndex);
//					}
//					else
//					{
//						CLayout newLayout;
//						newLayout.Create(rFoldSheet, nLayerIndex);
//						m_Layouts.AddTail(newLayout);
//						pLayout		 = &m_Layouts.GetTail();
//						nLayoutIndex = m_Layouts.GetCount() - 1;
//						CFoldSheetLayerRef layerRef(nFoldSheetIndex, nLayerIndex); // foldsheet index, layer index, copy index, layer orientation, layer turned
//						newPrintSheet.Create(layerRef, nLayoutIndex, nProductPartIndex, pLayout);	
//					}
//				}
//				else	// printsheet and layout has been selected by user
//				{
//					CPrintSheet* pSuitablePrintSheet = FindSuitablePrintSheet(rFoldSheet, nLayerIndex);	
//					if (pSuitablePrintSheet)
//					{
//						newPrintSheet = *pSuitablePrintSheet;
//					}
//					else
//					{
//						m_Layouts.AddTail((pLayout) ? *pLayout : *pPrintSheet->GetLayout());
//						pPrintSheet->LinkLayout(&m_Layouts.GetTail());
//						pPrintSheet->m_nLayoutIndex = m_Layouts.GetCount() - 1;
//						newPrintSheet = *pPrintSheet;
//					}
//					newPrintSheet.SubstituteFoldSheetIndex(rFoldSheet, nFoldSheetIndex, nLayerIndex);
//					//pDoc->SetPrintSheetPressDevice(newPrintSheet.GetLayout()->m_FrontSide.m_nPressDeviceIndex, BOTHSIDES, newPrintSheet.GetLayout());
//				}
//
//				int nPrintSheetIndex = GetInsertionPos(rFoldSheet, rFoldSheet.m_nFoldSheetNumber);
//				if (nPrintSheetIndex < 0)
//					AddHead(newPrintSheet);
//				else
//					if (nPrintSheetIndex >= GetCount())
//						AddTail(newPrintSheet);
//					else
//					{
//						POSITION sheetPos = FindIndex(nPrintSheetIndex);
//						CList <CPrintSheet, CPrintSheet&>::InsertAfter(sheetPos, newPrintSheet);
//					}
//			}
//		}
//		nFoldSheetIndex++;
//	}
//
//	ReNumberPrintSheets();
//}


// Check for unneeded fold sheets in case of ComingAndGoing
void CPrintSheetList::RemoveUnneededFoldSheetsComingAndGoing()
{
	int		 nIndex	= 0;
	POSITION pos	= CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.GetHeadPosition();
	while (pos)
	{
		CFoldSheet& rFoldSheet = CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (!CImpManDoc::GetDoc()->m_PrintSheetList.CheckFoldSheetComingAndGoing(rFoldSheet))
		{
			CImpManDoc::GetDoc()->m_PrintSheetList.RemoveFoldSheet(nIndex);
//			CImpManDoc::GetDoc()->m_PrintSheetList.ReIndexFoldSheets(nIndex);
		}
		nIndex++;
	}
}

BOOL CPrintSheetList::CheckFoldSheetComingAndGoing(CFoldSheet& rFoldSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return TRUE;

	if (rFoldSheet.GetBoundProduct().m_bindingParams.m_bComingAndGoing)		
	{
		int nFoldSheetIndex = pDoc->m_Bookblock.GetFoldSheetIndex(&rFoldSheet);
		if (pDoc->m_Bookblock.m_FoldSheetList.CGGetSimilarFoldSheet(rFoldSheet, nFoldSheetIndex))
			return FALSE;
		else
			return TRUE;
	}
	else
		return TRUE;
}

//BOOL CPrintSheetList::CheckFoldSheetComingAndGoing(CFoldSheet& rFoldSheet)
//{
//	int			nFoldSheets;
//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
//	if ( ! pDoc)
//		return TRUE;
//
//	int nFoldSheetIndex = pDoc->m_Bookblock.GetFoldSheetIndex(&rFoldSheet);
//	if (pDoc->m_productionData.m_nProductionType == 1)	// Coming and going
//	{
//		nFoldSheets = pDoc->m_Bookblock.m_FoldSheetList.GetCount()/2;
//		if (pDoc->m_Bookblock.m_FoldSheetList.GetCount() % 2)
//			nFoldSheets++;
//		else
//			if ((nFoldSheets >= 1) && (nFoldSheets < pDoc->m_Bookblock.m_FoldSheetList.GetCount()))
//			{
//				CFoldSheet* pFoldSheet1 = pDoc->m_Bookblock.GetFoldSheetByNum(nFoldSheets);
//				CFoldSheet* pFoldSheet2 = pDoc->m_Bookblock.GetFoldSheetByNum(nFoldSheets + 1);
//
//				if (pFoldSheet1 && pFoldSheet2)
//					if (pFoldSheet1->m_strFoldSheetTypeName != pFoldSheet2->m_strFoldSheetTypeName)
//						nFoldSheets++;
//			}
//
//		//if (rFoldSheet.m_nFoldSheetNumber <= nFoldSheets)	// before 11/03/2008
//		if (nFoldSheetIndex <= nFoldSheets - 1)
//			return TRUE;
//		else
//			return FALSE;
//	}
//	else
//		return TRUE;
//}


// Look how many times foldsheet layer is assigned to the printsheets
int CPrintSheetList::FoldSheetLayerIsAssigned(int nFoldSheetIndex, int nFoldSheetLayerIndex)
{
	int		 nNum = 0;
	POSITION pos  = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		for (int i = 0; i < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
			if ((rPrintSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex == nFoldSheetIndex) && (rPrintSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetLayerIndex == nFoldSheetLayerIndex))
				nNum++;				
	}
	return nNum;
}

// Get last printsheet in list after which 'rFoldSheet' can be inserted
int CPrintSheetList::GetInsertionPos(CFoldSheet& rFoldSheet, int /*nFoldSheetNumber*/)
{
	int		 nPrintSheetIndex = 0, nInsertIndex = INT_MAX;
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		for (int i = 0; i < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
		{
			POSITION pos = CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.FindIndex(rPrintSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex);
			if (pos)
			{
				if (CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.GetAt(pos).m_nFoldSheetNumber <= rFoldSheet.m_nFoldSheetNumber)
					nInsertIndex = nPrintSheetIndex;
//				if (CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.GetAt(pos).m_nFoldSheetNumber == rFoldSheet.m_nFoldSheetNumber + 1)
//					return nPrintSheetIndex - 1;
			}
		}
		nPrintSheetIndex++;
	}

	return nInsertIndex;
}

// Find printsheet where 'rFoldSheet' with 'nLayerIndex' is on - return NULL if no such sheet found
CPrintSheet* CPrintSheetList::FindSuitablePrintSheet(CFoldSheet& rFoldSheet, int nLayerIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		//if ((rPrintSheet.m_FoldSheetLayerRefs.GetSize() == 1) && (rPrintSheet.m_nLayoutIndex >= 0))
		//{
		//	POSITION pos = CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.FindIndex(rPrintSheet.m_FoldSheetLayerRefs[0].m_nFoldSheetIndex);
		//	if (pos)
		//	{
		//		if (CImpManDoc::GetDoc()->m_Bookblock.m_FoldSheetList.GetAt(pos).m_strFoldSheetTypeName == rFoldSheet.m_strFoldSheetTypeName)
		//			if (rPrintSheet.m_FoldSheetLayerRefs[0].m_nFoldSheetLayerIndex == nLayerIndex)
		//				return &rPrintSheet;
		//	}
		//}
		if (rPrintSheet.m_nLayoutIndex < 0)
			continue;

		for (int i = 0; i < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
		{
			POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(rPrintSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetIndex);
			if (pos)
			{
				CFoldSheet& rRefFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);
				if (rRefFoldSheet.m_strFoldSheetTypeName == rFoldSheet.m_strFoldSheetTypeName)
					if (rRefFoldSheet.m_nProductPartIndex == rFoldSheet.m_nProductPartIndex)
						if (rPrintSheet.m_FoldSheetLayerRefs[i].m_nFoldSheetLayerIndex == nLayerIndex)
							return &rPrintSheet;
			}
		}
	}
	return NULL;	
}


// Assign new printsheet numbers
void CPrintSheetList::ReNumberPrintSheets()
{
	int		 nNum = 1;
	POSITION pos  = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;
		rPrintSheet.m_nPrintSheetNumber = nNum++;
	}

	pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if ( ! rPrintSheet.IsEmpty())
			continue;
		rPrintSheet.m_nPrintSheetNumber = nNum++;
	}

	CArray <CLayout*, CLayout*> sortedLayouts;
	pos = m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = m_Layouts.GetNext(pos);
		sortedLayouts.Add(&rLayout);
	}
	qsort((void*)sortedLayouts.GetData(), sortedLayouts.GetSize(), sizeof(CLayout*), CompareLayoutPos);

	nNum = 1;
	for (int i = 0; i < sortedLayouts.GetSize(); i++)
	{
		if (sortedLayouts[i]->GetNumDefaultName() != -1)
			sortedLayouts[i]->m_strLayoutName.Format(_T("%s-%d"), theApp.settings.m_szLayoutName, nNum++);
	}
}

int CPrintSheetList::CompareLayoutPos(const void *p1, const void *p2)
{
	CLayout*	 pLayout1			= *(CLayout**)p1;
	CLayout*	 pLayout2			= *(CLayout**)p2;
	CPrintSheet* pPrintSheet1		= pLayout1->GetFirstPrintSheet();
	CPrintSheet* pPrintSheet2		= pLayout2->GetFirstPrintSheet();
	int			 nPrintSheetNumber1 = (pPrintSheet1) ? pPrintSheet1->m_nPrintSheetNumber : INT_MAX; 
	int			 nPrintSheetNumber2 = (pPrintSheet2) ? pPrintSheet2->m_nPrintSheetNumber : INT_MAX; 
	if (nPrintSheetNumber1 < nPrintSheetNumber2)
		return -1;
	else
		if (nPrintSheetNumber1 > nPrintSheetNumber2)
			return 1;
		else
			return 0;
}

void CPrintSheetList::InvalidateAllPlates()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		rPrintSheet.m_FrontSidePlates.InvalidateAllPlates();
		rPrintSheet.m_BackSidePlates.InvalidateAllPlates();
	}
}

void CPrintSheetList::SetGlobalMaskAllPlates(float fValue, CLayout* pLayout)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (pLayout)
			if (rPrintSheet.GetLayout() != pLayout)
				continue;
		rPrintSheet.m_FrontSidePlates.SetGlobalMaskAllPlates(fValue);
		rPrintSheet.m_BackSidePlates.SetGlobalMaskAllPlates(fValue);
	}
}

void CPrintSheetList::SetLocalMaskAllExposures(CExposure* pExposure, int nPosition, float fValue)
{
	if (!pExposure)
		return;
	CLayoutObject* pObject = pExposure->GetLayoutObject();
	if (!pObject)
		return;

	BOOL bPanoramaPageClicked = FALSE;
	if (pObject)					   
	{
		CPrintSheet* pPrintSheet = pExposure->GetPlate()->GetPrintSheet();
		if (pPrintSheet)
			bPanoramaPageClicked = pObject->IsPanoramaPage(*pPrintSheet);
		pObject->SetLocalMaskAllExposures(nPosition, fValue, bPanoramaPageClicked);
	}

	CLayoutObject* pCounterPartObject = pObject->FindCounterpartObject();
	if (pCounterPartObject) 
		pCounterPartObject->SetLocalMaskAllExposures(pObject->FindCounterpartClickSide(nPosition), fValue, bPanoramaPageClicked);
}

void CPrintSheetList::UnlockLocalMaskAllExposures(CExposure* pExposure)
{
	if (!pExposure)
		return;
	CLayoutObject* pObject = pExposure->GetLayoutObject();
	if (!pObject)
		return;

	BOOL bPanoramaPageClicked = FALSE;
	if (pObject)					   
	{
		CPrintSheet* pPrintSheet = pExposure->GetPlate()->GetPrintSheet();
		if (pPrintSheet)
			bPanoramaPageClicked = pObject->IsPanoramaPage(*pPrintSheet);
		pObject->UnlockLocalMaskAllExposures(bPanoramaPageClicked);
	}

	CLayoutObject* pCounterPartObject = pObject->FindCounterpartObject();
	if (pCounterPartObject) 
		pCounterPartObject->UnlockLocalMaskAllExposures(bPanoramaPageClicked);
}

void CPrintSheetList::AdjustContent2LocalMaskAllExposures(CExposure* pExposure)
{
	if (!pExposure)
		return;
	CLayoutObject* pObject = pExposure->GetLayoutObject();
	if (!pObject)
		return;

	BOOL bPanoramaPageClicked = FALSE;
	if (pObject)					   
	{
		CPrintSheet* pPrintSheet = pExposure->GetPlate()->GetPrintSheet();
		if (pPrintSheet)
			bPanoramaPageClicked = pObject->IsPanoramaPage(*pPrintSheet);
		pObject->AdjustContent2LocalMaskAllExposures(bPanoramaPageClicked);
	}

	CLayoutObject* pCounterPartObject = pObject->FindCounterpartObject();
	if (pCounterPartObject) 
		pCounterPartObject->AdjustContent2LocalMaskAllExposures(bPanoramaPageClicked);
}

 
void CPrintSheetList::RemoveAll()
{
	if ( ! GetCount())
		return;

	CList <CPrintSheet, CPrintSheet&>::RemoveAll(); 
	m_Layouts.RemoveAll();
	//m_Layouts.m_PressDevices.RemoveAll();
	m_pdfTargets.RemoveAll();
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		pDoc->m_PageTemplateList.FindPrintSheetLocations();
		pDoc->m_printingProfiles.RemoveAll();
		pDoc->m_printGroupList.RemoveAll();
	}
}


void CPrintSheetList::RemoveAt(POSITION pos)
{
	CLayout* pLayout	  = GetAt(pos).GetLayout();
	int		 nLayoutIndex = GetAt(pos).m_nLayoutIndex; 
	CList <CPrintSheet, CPrintSheet&>::RemoveAt(pos); 
	ReNumberPrintSheets();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	if (pLayout->GetNumPrintSheetsBasedOn() == 0)	// If no more printsheet based on this layout, 
	{												// we have to remove layout as well
		pos = m_Layouts.FindIndex(nLayoutIndex);
		if (pos)
			m_Layouts.RemoveAt(pos);
		
		pos = pDoc->m_printingProfiles.FindIndex(nLayoutIndex);
		if (pos)
			pDoc->m_printingProfiles.RemoveAt(pos);

		if ( (nLayoutIndex >= 0) && (nLayoutIndex < pDoc->m_printGroupList.GetSize()) )
			pDoc->m_printGroupList.RemoveAt(nLayoutIndex);
	}
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
}


// Overloaded version of base class representation
void CPrintSheetList::RemoveAt(CPrintSheet* pPrintSheet)
{
	POSITION pos = GetHeadPosition();	
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetAt(pos);
		if (&rPrintSheet == pPrintSheet)
		{
			RemoveAt(pos);
			break;
		}
		CImpManDoc::GetDoc()->m_PrintSheetList.GetNext(pos);
	}
}

void CPrintSheetList::RemovePrintSheet(CPrintSheet* pPrintSheet)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	BOOL bRemoveAll = FALSE;
	CLayout* pLayout = pPrintSheet->GetLayout();
	if (pLayout)
		if (pLayout->GetNumPrintSheetsBasedOn() == 1)
			bRemoveAll = TRUE;

	BOOL bPrintSheetReallyRemoved = FALSE;
	while (pPrintSheet->m_FoldSheetLayerRefs.GetSize() > 0)
	{
		pPrintSheet->RemoveFoldSheetLayer(0, FALSE, FALSE, TRUE, TRUE, NULL, bPrintSheetReallyRemoved); 
		if (bPrintSheetReallyRemoved)	// break here, because pPrintSheet pointer no longer valid after removal
			break;
	}

	if ( ! bPrintSheetReallyRemoved)
	{
		while (pPrintSheet->m_flatProductRefs.GetSize() > 0)
		{
			BOOL bPrintSheetReallyRemoved = FALSE;
			pPrintSheet->RemoveFlatProductType(0, FALSE, FALSE, TRUE, TRUE, NULL, bPrintSheetReallyRemoved); 
			if (bPrintSheetReallyRemoved)	// break here, because pPrintSheet pointer no longer valid after removal
				break;
		}
	}

	if (bRemoveAll)
		pLayout->DeleteContents();

	pDoc->m_Bookblock.Reorganize(NULL, NULL);
	pDoc->m_PageSourceList.InitializePageTemplateRefs();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();

	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
	pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();

	pDoc->m_MarkSourceList.RemoveUnusedSources();

	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
}


// Overloaded version of base class representation
CPrintSheet* CPrintSheetList::InsertBefore(CPrintSheet* pRefPrintSheet, CPrintSheet& rNewPrintSheet)
{
	POSITION pos = GetHeadPosition();	
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetAt(pos);
		if (&rPrintSheet == pRefPrintSheet)
		{
			pos = CList <CPrintSheet, CPrintSheet&>::InsertBefore(pos, rNewPrintSheet);
			ReNumberPrintSheets();
			return &(GetAt(pos));
		}
		CImpManDoc::GetDoc()->m_PrintSheetList.GetNext(pos);
	}

	return NULL;
}


// Overloaded version of base class representation
CPrintSheet* CPrintSheetList::InsertAfter(CPrintSheet* pRefPrintSheet, CPrintSheet& rNewPrintSheet)
{
	POSITION pos = GetHeadPosition();	
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetAt(pos);
		if (&rPrintSheet == pRefPrintSheet)
		{
			pos = CList <CPrintSheet, CPrintSheet&>::InsertAfter(pos, rNewPrintSheet);
			ReNumberPrintSheets();
			return &(GetAt(pos));
		}
		CImpManDoc::GetDoc()->m_PrintSheetList.GetNext(pos);
	}

	return NULL;
}
 

// CopyAndPaste together with AutoAssign only makes sense with same printsheet as source and target
void CPrintSheetList::CopyAndPasteFoldSheet(CPrintSheet* pPrintSheet, int nComponentRefIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (rPrintSheet.GetLayout() == pPrintSheet->GetLayout())
			if (&rPrintSheet != pPrintSheet)	// CopyAndPasteFoldSheet for pPrintSheet already performed in CPrintSheet::CopyAndPasteFoldSheet()
				rPrintSheet.CopyFoldSheetLayerData(&rPrintSheet, nComponentRefIndex);
	}
	CImpManDoc::GetDoc()->m_PageTemplateList.FindPrintSheetLocations();
	CImpManDoc::GetDoc()->m_PageTemplateList.ReorganizePrintSheetPlates();
	CImpManDoc::GetDoc()->m_PrintSheetList.ReorganizeColorInfos();
}
 

// For all printsheets of type pTargetPrintSheet generate the same foldsheet pattern as on pTargetPrintSheet
void CPrintSheetList::CutAndPasteFoldSheet(CPrintSheet* pTargetPrintSheet, BOOL bReverse)
{
	CPrintSheet *pSrcPrintSheet = NULL, *pDstPrintSheet = NULL;
	int			nLayers			= pTargetPrintSheet->m_FoldSheetLayerRefs.GetSize();// + 1;  _CHECK_CUTANDPASTE_
	int			i				= nLayers;
	BOOL		bDstNotFull		= FALSE;
	POSITION	pos				= (bReverse) ? GetTailPosition() : GetHeadPosition();
	while (pos)
	{
		if (i == nLayers) // Dst full, so get next 
		{
			if (pSrcPrintSheet == NULL)
			{
				pDstPrintSheet = NULL;
				while (pos)
				{
					pDstPrintSheet = (bReverse) ? &GetPrev(pos) : &GetNext(pos);
					if (pDstPrintSheet->m_nLayoutIndex == pTargetPrintSheet->m_nLayoutIndex)
						break;
					pDstPrintSheet = NULL;
				}
				if (!pDstPrintSheet)
					break;
			}
			else
				pDstPrintSheet = pSrcPrintSheet;

			i = pDstPrintSheet->m_FoldSheetLayerRefs.GetSize();
		}
										 
		pSrcPrintSheet = NULL;
		while (pos)
		{
			pSrcPrintSheet = (bReverse) ? &GetPrev(pos) : &GetNext(pos);
			if (pSrcPrintSheet->m_nLayoutIndex == pTargetPrintSheet->m_nLayoutIndex)
				break;
			pSrcPrintSheet = NULL;
		}
		if (!pSrcPrintSheet)
		{
			if (i < nLayers) 
				bDstNotFull = TRUE;	
			break;
		}

		for (; i < nLayers; i++)
		{
			pDstPrintSheet->CopyFoldSheetLayerData(pSrcPrintSheet, 0);
			pSrcPrintSheet->m_FoldSheetLayerRefs.RemoveAt(0);
			if (pSrcPrintSheet->m_FoldSheetLayerRefs.GetSize() == 0)			
			{
				CImpManDoc::GetDoc()->m_PrintSheetList.RemoveAt(pSrcPrintSheet);
				pSrcPrintSheet = NULL;
				i++;
				break;
			}
		}			
	}

	if (bDstNotFull)	// not enought sheets to fill up dst - so create new layout
	{
		CLayout layout;
		layout.Copy(*pTargetPrintSheet->GetLayout());	// Create and append new layout
		layout.AssignDefaultName();

		int i = pDstPrintSheet->m_FoldSheetLayerRefs.GetSize();
		while (i < layout.m_FoldSheetLayerRotation.GetSize())
			layout.RemoveFoldSheetLayer(i);

		CImpManDoc::GetDoc()->m_PrintSheetList.m_Layouts.AddTail(layout);
		pDstPrintSheet->m_nLayoutIndex = CImpManDoc::GetDoc()->m_PrintSheetList.m_Layouts.GetCount() - 1;
		pDstPrintSheet->GetLayout()->m_FrontSide.Invalidate();
		pDstPrintSheet->GetLayout()->m_BackSide.Invalidate();
		pDstPrintSheet->GetLayout()->m_FrontSide.Analyse();
		pDstPrintSheet->GetLayout()->m_BackSide.Analyse();
	}

	ReNumberPrintSheets();
	CImpManDoc::GetDoc()->m_PageTemplateList.FindPrintSheetLocations();
	CImpManDoc::GetDoc()->m_PageTemplateList.ReorganizePrintSheetPlates();
	CImpManDoc::GetDoc()->m_PrintSheetList.ReorganizeColorInfos();
}


void CPrintSheetList::DecrementColorDefIndices(short nColorDefinitionIndex)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);

		POSITION platePos = rPrintSheet.m_FrontSidePlates.GetHeadPosition();
		while (platePos)
		{
			CPlate& rPlate = rPrintSheet.m_FrontSidePlates.GetNext(platePos);
			if (rPlate.m_nColorDefinitionIndex > nColorDefinitionIndex)
				rPlate.m_nColorDefinitionIndex--;
		}
		platePos = rPrintSheet.m_BackSidePlates.GetHeadPosition();
		while (platePos)
		{
			CPlate& rPlate = rPrintSheet.m_BackSidePlates.GetNext(platePos);
			if (rPlate.m_nColorDefinitionIndex > nColorDefinitionIndex)
				rPlate.m_nColorDefinitionIndex--;
		}
	}
}

void CPrintSheetList::InitializeFoldSheetTypeLists()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		rPrintSheet.InitializeFoldSheetTypeList();
	}
}

void CPrintSheetList::InitializeFlatProductTypeLists()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		rPrintSheet.InitializeFlatProductTypeList();
	}
}

void CPrintSheetList::RemoveFlatProductRefs(CLayout* pLayout, int nFlatProductRefIndex)
{
	if ( ! pLayout)
		return;
	if (nFlatProductRefIndex < 0)
		return;

	BOOL	 bFound = FALSE;
	POSITION pos	= pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
		if ( ! rObject.IsFlatProduct())
			continue;
		if (rObject.m_nComponentRefIndex == nFlatProductRefIndex)
		{
			bFound = TRUE;
			break;
		}
	}
	if (bFound)
		return;

	pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (rPrintSheet.GetLayout() != pLayout)
			continue;

		if (nFlatProductRefIndex < rPrintSheet.m_flatProductRefs.GetSize())
			rPrintSheet.m_flatProductRefs.RemoveAt(nFlatProductRefIndex);
	}

	pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
		if ( ! rObject.IsFlatProduct())
			continue;
		if (rObject.m_nComponentRefIndex > nFlatProductRefIndex)
			rObject.m_nComponentRefIndex--;
	}
	pos = pLayout->m_BackSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = pLayout->m_BackSide.m_ObjectList.GetNext(pos);
		if ( ! rObject.IsFlatProduct())
			continue;
		if (rObject.m_nComponentRefIndex > nFlatProductRefIndex)
			rObject.m_nComponentRefIndex--;
	}
}

void CPrintSheetList::EnableOutput(BOOL bEnable)
{
	POSITION pos = GetHeadPosition();
	while (pos)
		GetNext(pos).EnableOutput(bEnable);
}

BOOL CPrintSheetList::OutputPDFCheckPageSourceLinks(const CString& strDocTitle)
{
	CArray <CString, CString> missingLinks;
	CPageSource*			  pPageSource = NULL;
	POSITION				  pos		  = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);

		if (rPrintSheet.IsOutputDisabled())
			continue;

		CPDFTargetProperties* pTargetProps = m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
		if ( ! pTargetProps)
			continue;

		POSITION platePos = rPrintSheet.m_FrontSidePlates.GetHeadPosition();
		if ( ! platePos && (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder) )		// unpopulated JDF -> check default plates regarding marks
			rPrintSheet.m_FrontSidePlates.m_defaultPlate.CheckSourceLinks(missingLinks, (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder) ? TRUE : FALSE);
		else
		{
			while (platePos)
			{
				CPlate& rPlate = rPrintSheet.m_FrontSidePlates.GetNext(platePos);
				if ( ! rPlate.m_bOutputDisabled)
					rPlate.CheckSourceLinks(missingLinks, (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder) ? TRUE : FALSE);
			}
		}

		if ( ! rPrintSheet.GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
		{
			platePos = rPrintSheet.m_BackSidePlates.GetHeadPosition();
			if ( ! platePos && (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder) )
				rPrintSheet.m_BackSidePlates.m_defaultPlate.CheckSourceLinks(missingLinks, (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder) ? TRUE : FALSE);
			else
			{
				while (platePos)
				{
					CPlate& rPlate = rPrintSheet.m_BackSidePlates.GetNext(platePos);
					if ( ! rPlate.m_bOutputDisabled)
						rPlate.CheckSourceLinks(missingLinks, (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder) ? TRUE : FALSE);
				}
			}
		}
	}

	if (missingLinks.GetSize())
	{
		CString strMissingLinks, strMessage;
		for (int i = 0; i < missingLinks.GetSize(); i++)
		{
			if (i == 10)
			{
				strMissingLinks += "...\n";
				break;
			}
			strMissingLinks += missingLinks[i] + "\n";
		}
		strMessage.Format(IDS_MISSING_LINKS, strMissingLinks);
		if (KIMOpenLogMessage(strMessage, MB_YESNO | MB_ICONWARNING, IDNO) == IDYES)
			return FALSE;
	}

	return TRUE;
}

int CPrintSheetList::OutputImposition(const CString& strDocTitle)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	
	BOOL		 bAlreadyWarned = FALSE;
	BOOL		 bBreak		    = FALSE;
	BOOL		 bOverwriteAll  = -1;		// unknown yet
	CPrintSheet* pCurPrintSheet = NULL;
	POSITION	 pos			= GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		pCurPrintSheet = &rPrintSheet;

		if (rPrintSheet.IsOutputDisabled())
			continue;

		CPDFTargetProperties* pTargetProps = m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
		if ( ! pTargetProps)
			continue;

		if ( (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder) && ! bAlreadyWarned )
		{
			if ( ! (theApp.m_nAddOnsLicensed & CImpManApp::JDFLayCrImp) || (pDoc->m_nDongleStatus == CImpManApp::NoDongle) || (pDoc->m_nDongleStatus == CImpManApp::ClientDongle))
			{
				if (KIMOpenLogMessage(IDS_DEMO_JDFOUTPUT_WARNING, MB_OKCANCEL | MB_ICONWARNING, IDOK) == IDCANCEL)
					return 0;
				bAlreadyWarned = TRUE;
			}
		}

		if (rPrintSheet.GetOutfileRelation(pTargetProps->m_strOutputFilename) == CPrintSheet::JobPerFile)	
		{
			if (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder)
			{
				CJDFOutputImposition jdfOutput(*pTargetProps);
				bBreak = jdfOutput.ProcessPrintSheetsJobInOne(this, bOverwriteAll, FALSE, &pCurPrintSheet, NULL);
			}
			else
			{
				CPDFOutput pdfOutput(*pTargetProps, strDocTitle);
				bBreak = pdfOutput.ProcessPrintSheetsJobInOne(this, bOverwriteAll, FALSE, TRUE, FALSE, &pCurPrintSheet);
			}
			pos = GetPos(*pCurPrintSheet);
			GetNext(pos);
			if ( (bBreak == 0) || (bBreak == 2) )	// if complete(0) or abort all(2)
				break;								
			else
				continue;							// abort current sheet only(1) -> continue
		}

		if (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder)
		{
			CJDFOutputImposition jdfOutput(*pTargetProps);
			bBreak = jdfOutput.ProcessPrintSheet(&rPrintSheet, bOverwriteAll);	
		}
		else
		{
			CPDFOutput pdfOutput(*pTargetProps, strDocTitle);
			bBreak = pdfOutput.ProcessPrintSheet(&rPrintSheet, bOverwriteAll, FALSE);	// FALSE = not AutoCmd
		}

		if (bBreak == 2)
			break;
	}

	if (bBreak == 2)
		return 2;
	else
		return 0;
}

int CPrintSheetList::OutputCuttingData(const CString& strDocTitle)
{
	CImpManDoc*	pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	
	BOOL		 bBreak		    = FALSE;
	BOOL		 bOverwriteAll  = TRUE;
	CPrintSheet* pCurPrintSheet = NULL;
	POSITION	 pos			= GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		pCurPrintSheet = &rPrintSheet;

		if (rPrintSheet.IsOutputDisabled())
			continue;

		CPDFTargetProperties* pTargetProps = m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
		if ( ! pTargetProps)
			continue;
		if ( ! pTargetProps->m_bOutputCuttingData)
			continue;

		if (rPrintSheet.GetOutfileRelation(pTargetProps->m_strCuttingDataFilename) == CPrintSheet::JobPerFile)	
		{
			pCurPrintSheet = &rPrintSheet;
			CJDFOutputCuttingData jdfOutput(*pTargetProps);
			bBreak = jdfOutput.ProcessPrintSheetsJobInOne(this, bOverwriteAll, &pCurPrintSheet);
			pos = GetPos(*pCurPrintSheet);
			GetNext(pos);
			if ( (bBreak == 0) || (bBreak == 2) )	// if complete(0) or abort all(2)
				break;								
			else
				continue;							// abort current sheet only(1) -> continue
		}

		CJDFOutputCuttingData jdfOutput(*pTargetProps);
		bBreak = jdfOutput.ProcessPrintSheetAllInOne(&rPrintSheet, bOverwriteAll);

		if (bBreak == 2)
			break;
	}

	if (bBreak == 2)
		return 2;
	else
		return 0;
}

int CPrintSheetList::OutputSheetsAuto(const CString& strDocTitle)
{
	EnableOutput(TRUE);

#ifdef KIM_ENGINE
	if (theApp.m_kaDB.kA_Connect("kimAutoDB", "", theApp.settings.m_szAutoServerNameIP, "SYSDBA", "masterkey") != -1)
		theApp.m_bServerConnected = TRUE;
#endif

	int			 nError			= 0;
	int			 nSheetsOut		= 0;
	BOOL		 bJobCancelled	= FALSE;
	BOOL		 bOverwriteAll  = TRUE;		
	CPrintSheet* pCurPrintSheet = NULL;
	POSITION	 pos			= GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		pCurPrintSheet = &rPrintSheet;

		CPDFTargetProperties* pTargetProps = m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);
		if ( ! pTargetProps)
			continue;

		if (rPrintSheet.GetOutfileRelation(pTargetProps->m_strOutputFilename) == CPrintSheet::JobPerFile)	
		{
			if (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder)
			{
				CJDFOutputImposition jdfOutput(*pTargetProps);
				jdfOutput.ProcessPrintSheetsJobInOne(this, bOverwriteAll, TRUE, &pCurPrintSheet, &nSheetsOut);
				break;
			}
			else
			{
				CPDFOutput pdfOutput(*pTargetProps, strDocTitle);
				bJobCancelled = pdfOutput.ProcessPrintSheetsJobInOne(this, bOverwriteAll, TRUE, TRUE, FALSE, &pCurPrintSheet);	// TRUE = AutoCmd
			}
			pos = GetPos(*pCurPrintSheet);
			GetNext(pos);
		}

		if (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder)
		{
			CJDFOutputImposition jdfOutput(*pTargetProps);
			bJobCancelled = jdfOutput.ProcessPrintSheet(&rPrintSheet, bOverwriteAll);	
			nError = jdfOutput.m_nError;
			if (jdfOutput.m_nPlatesProcessed)
				nSheetsOut++;
		}
		else
		{
			CPDFOutput pdfOutput(*pTargetProps, strDocTitle);
			bJobCancelled = pdfOutput.ProcessPrintSheet(&rPrintSheet, bOverwriteAll, TRUE);	// TRUE = AutoCmd
			nError = pdfOutput.m_nError;
			if (pdfOutput.m_nPlatesProcessed)
				nSheetsOut++;
		}

		if (KimEngineDBJobAbort(theApp.m_nDBCurrentJobID))
			bJobCancelled = 2;

		if (bJobCancelled == 2)
			break;
	}

#ifndef KIM_ENGINE
	if ( (theApp.m_nGUIStyle != CImpManApp::GUIStandard)  && theApp.m_bServerConnected)
	{
		theApp.m_kaDB.kA_Disconnect();
		theApp.m_bServerConnected = FALSE;
	}
#endif

	if (bJobCancelled)
		nError = CImpManApp::ProcessCancelled;

	switch (nError)
	{
	case CImpManApp::PDFOutputError:	
		return nError;	

	case CImpManApp::ProcessCancelled:	
		{
			CString strMsg; 
			strMsg.LoadString(IDS_MSG_JOB_CANCELLED);
			theApp.LogEntry(_T("   ") + strMsg + _T("\r\n"));
			return nError;	
		}

	default:							
		return nSheetsOut; 
	}
}

BOOL CPrintSheetList::ReadyForOutput(int nSide, BOOL bAutoCmd)
{
	if ( ! bAutoCmd)
		return TRUE;

	BOOL	 bNotYetOutputted = FALSE;
	POSITION pos			  = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);

		if (rPrintSheet.GetNumPages(nSide) != rPrintSheet.GetNumPagesOccupied(nSide)) 
			return FALSE;
			
		if (rPrintSheet.NotYetOutputted(nSide))
			bNotYetOutputted = TRUE;
	}

	// when here, all sheets are complete -> if one or more sheets not yet outputted, we are ready for output

	if (bNotYetOutputted) 
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
			pDoc->m_PrintSheetList.NotifyClients(CPrintSheetList::NOTIFY_START_OUTPUT_SHEET);
		return TRUE;
	}
	else
		return FALSE;
}

void CPrintSheetList::ResetLastOutputDates()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		rPrintSheet.ResetLastOutputDates();
	}
}

void CPrintSheetList::GetOutputSheetInfo(CJobInfoExchange& rJobInfoExchange)
{
	rJobInfoExchange.m_outputSheetInfoList.RemoveAll();

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	int				 nSheetIndex = 0;
	COutputSheetInfo outputSheetInfo;
	POSITION  pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (rPrintSheet.IsEmpty())
			continue;
		CLayout* pLayout = rPrintSheet.GetLayout();
		if ( ! pLayout)
			continue;

		CPDFTargetProperties* pTargetProps = m_pdfTargets.Find(rPrintSheet.m_strPDFTarget);

		outputSheetInfo.m_nSheetIndex			= nSheetIndex++;
		outputSheetInfo.m_strSheetName			= rPrintSheet.GetNumber();
		outputSheetInfo.m_nNumPages				= rPrintSheet.GetNumPages();
		outputSheetInfo.m_nNumPagesOccupied		= rPrintSheet.GetNumPagesOccupied();
		outputSheetInfo.m_strPageRange			= "";
		outputSheetInfo.m_lastOutputDate		= rPrintSheet.GetLastOutputDate();
		outputSheetInfo.m_nOutputTargetType		= (pTargetProps) ? pTargetProps->m_nType : -1;
		outputSheetInfo.m_strOutputTargetName	= (pTargetProps) ? pTargetProps->m_strTargetName : "";
		outputSheetInfo.m_strOutputLocation		= (pTargetProps) ? pTargetProps->m_strLocation : "";
		outputSheetInfo.m_strOutputLocationBack	= (pTargetProps) ? pTargetProps->m_strLocationBack : "";
		outputSheetInfo.m_strOutputFilename		= rPrintSheet.GetOutFilename(BOTHSIDES);
		if (outputSheetInfo.m_strOutputFilename.IsEmpty())	// try again with frontside only
			outputSheetInfo.m_strOutputFilename	= rPrintSheet.GetOutFilename(FRONTSIDE);

		rPrintSheet.InitializeFoldSheetTypeList();
		for (int i = 0; i < rPrintSheet.m_FoldSheetTypeList.GetSize(); i++)
		{
			CFoldSheet* pFoldSheet = rPrintSheet.m_FoldSheetTypeList[i].m_pFoldSheet;
			CArray <short*, short*> leftPageSequence, rightPageSequence;
			pFoldSheet->GetPageSequence(leftPageSequence,  CDescriptionItem::FoldSheetLeftPart);
			pFoldSheet->GetPageSequence(rightPageSequence, CDescriptionItem::FoldSheetRightPart);

			int nMinLeft = -1, nMaxLeft = -1, nMinRight = -1, nMaxRight = -1;
			if (leftPageSequence.GetSize())
			{
				nMinLeft = *leftPageSequence[0];
				nMaxLeft = *leftPageSequence[leftPageSequence.GetSize() - 1];
			}
			if (rightPageSequence.GetSize()) 
			{
				nMinRight = *rightPageSequence[0];
				nMaxRight = *rightPageSequence[rightPageSequence.GetSize() - 1];
			}
			if (nMinLeft == -1)
			{
				nMinLeft = nMinRight;
				nMaxLeft = nMaxRight;
			}
			if (nMinRight == -1)
			{
				nMinRight = nMinLeft;
				nMaxRight = nMaxLeft;
			}

			CString strNumUp;
			int		nNumUp = rPrintSheet.m_FoldSheetTypeList[i].m_nNumUp; 
			if (nNumUp > 1)
				strNumUp.Format(_T("%d x "), nNumUp);

			if ( (nMinLeft != -1) && (nMaxLeft != -1) && (nMinRight != -1) && (nMaxRight != -1) )
			{
				int nProductIndex = pDoc->m_boundProducts.TransformPartToProductIndex(pFoldSheet->m_nProductPartIndex); 
				POSITION posMinLeft  = pDoc->m_PageTemplateList.FindIndex(nMinLeft  - 1, -nProductIndex - 1);
				POSITION posMaxLeft  = pDoc->m_PageTemplateList.FindIndex(nMaxLeft  - 1, -nProductIndex - 1);
				POSITION posMinRight = pDoc->m_PageTemplateList.FindIndex(nMinRight - 1, -nProductIndex - 1);
				POSITION posMaxRight = pDoc->m_PageTemplateList.FindIndex(nMaxRight - 1, -nProductIndex - 1);
				if (posMinLeft && posMaxLeft && posMinRight && posMaxRight)
				{
					CString strMinLeft  = pDoc->m_PageTemplateList.GetAt(posMinLeft ).m_strPageID + " - ";
					CString strMaxLeft  = pDoc->m_PageTemplateList.GetAt(posMaxLeft ).m_strPageID;
					CString strMinRight = pDoc->m_PageTemplateList.GetAt(posMinRight).m_strPageID + " - ";
					CString strMaxRight = pDoc->m_PageTemplateList.GetAt(posMaxRight).m_strPageID;
					CString strSep		= " + ";

					if ((nMaxLeft == nMinRight - 1) || ((nMinLeft == nMinRight) && (nMaxLeft == nMaxRight)) )
						outputSheetInfo.m_strPageRange += ((i > 0) ? " + " : "") + strNumUp + CString("(") + strMinLeft + strMaxRight + CString(")");
					else
						outputSheetInfo.m_strPageRange += ((i > 0) ? " + " : "") + strNumUp + CString("(") + strMinLeft + strMaxLeft + strSep + strMinRight + strMaxRight + CString(")");
				}
			}
		}
		pLayout->CreateThumbnail(outputSheetInfo.m_thumbnailBitmap, rPrintSheet, CSize(100,100));

		rJobInfoExchange.m_outputSheetInfoList.AddTail(outputSheetInfo);
	}
}

void CPrintSheetList::NotifyClients(int nMsg)
{
	//CPrintSheetTableView* pPrintSheetTableView  = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));

	//switch (nMsg)
	//{
	//case NOTIFY_START_OUTPUT_SHEET:	
	//	{
	//		if (theApp.m_nGUIStyle == CImpManApp::GUIQuiet)
	//			return;
	//		if ( ! pPrintSheetTableView)
	//		{
	//			CMainFrame frame;
	//			CNewPrintSheetFrame* pNewPrintSheetFrame = frame.CreateOrActivateNewPrintSheetFrame(theApp.m_pNewPrintSheetTemplate, CImpManDoc::GetDoc());
	//			if (pNewPrintSheetFrame)
	//			{
	//				//pNewPrintSheetFrame->m_bShowPlate	  = TRUE;
	//				//pNewPrintSheetFrame->m_bShowExposures = TRUE;
	//				pNewPrintSheetFrame->SetupViewOutputAllSheets();

	//				CPrintSheetView* pPrintSheetView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	//				pPrintSheetView->OnUpdate(NULL, HINT_UPDATE_WINDOW, NULL);
	//				pNewPrintSheetFrame->ShowWindow(SW_SHOWMAXIMIZED);

	//				pPrintSheetTableView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	//				if (pPrintSheetTableView)
	//				{
	//					pPrintSheetTableView->m_DisplayList.Invalidate();
	//					pPrintSheetTableView->Invalidate();
	//					pPrintSheetTableView->m_PrintSheetTable.Initialize(CPrintSheetTable::ShowOutputList);
	//				}

	//				pNewPrintSheetFrame->GetParentFrame()->SetMenu(NULL);
	//				pNewPrintSheetFrame->GetParentFrame()->ShowWindow(SW_SHOWNORMAL);
	//				pNewPrintSheetFrame->GetParentFrame()->UpdateWindow();

	//				if (pPrintSheetTableView)
	//				{
	//					pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_START_OUTPUT,	 FALSE);
	//					pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_OUTPUT_PROFILE,	 FALSE);
	//					pPrintSheetTableView->m_outputListToolBarCtrl.EnableButton(ID_OPEN_OUTPUT_FILE, FALSE);
	//					pPrintSheetTableView->m_outputListToolBarCtrl.Invalidate();
	//					pPrintSheetTableView->m_outputListToolBarCtrl.UpdateWindow();
	//				}
	//			}
	//		}
	//	}
	//	break;

	//case NOTIFY_END_OUTPUT_SHEET:	break;
	//case NOTIFY_UPDATE_VIEW:		if (pPrintSheetTableView) { pPrintSheetTableView->Invalidate(); pPrintSheetTableView->UpdateWindow(); }	break;
	//}

/*	CImpManDoc* pDoc = CImpManDoc::GetDoc();	
	if (pDoc)
		if (pDoc->m_pWndMain)
			pDoc->m_pWndMain->PostMessage(nMsg, 0, 0);	// don't work - why?????? :-( 
*/
}

void CPrintSheetList::ReorganizeColorInfos()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		rPrintSheet.ReorganizeColorInfos();
	}

	CPrintSheetTableView* pView = (CPrintSheetTableView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetTableView));
	if ( ! pView)
		return;

	if (pView->m_PrintSheetTable.m_nWhatToShow == CPrintSheetTable::ShowPrintColorInfo)
	{
		pView->m_DisplayList.Invalidate();
		pView->Invalidate();
		pView->m_PrintSheetTable.m_nWhatToShow = CPrintSheetTable::UnInitialized;
		pView->m_PrintSheetTable.Initialize(CPrintSheetTable::ShowPrintColorInfo);
	}
}

void CPrintSheetList::GetProcessColors(CStringArray& plateNames)
{
	CColorantList colorants;
	CStringArray  strSeps;
	POSITION      pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		rPrintSheet.GetProcessColors(BOTHSIDES, strSeps);
		colorants.AddSeparations(strSeps);
	}
	plateNames.RemoveAll();
	for (int i = 0; i < colorants.GetSize(); i++)
		plateNames.Add(colorants[i].m_strName);
}

void CPrintSheetList::SortByFoldSheets()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();

	BOOL bChanged;
	do
	{
		bChanged = FALSE;
		POSITION pos1 = GetHeadPosition();
		while (pos1)
		{
			CPrintSheet& rPrintSheet1 = GetAt(pos1);

			POSITION pos2 = pos1;
			GetNext(pos2);
			while (pos2)
			{
				CPrintSheet& rPrintSheet2 = GetNext(pos2);
				if (rPrintSheet1.m_FoldSheetTypeList.GetSize() && rPrintSheet2.m_FoldSheetTypeList.GetSize())
					if (rPrintSheet1.m_FoldSheetTypeList[0].m_pFoldSheet && rPrintSheet2.m_FoldSheetTypeList[0].m_pFoldSheet)
					{
						CFoldSheet* pFoldSheet1	   = rPrintSheet1.m_FoldSheetTypeList[0].m_pFoldSheet;
						CFoldSheet* pFoldSheet2	   = rPrintSheet2.m_FoldSheetTypeList[0].m_pFoldSheet;
						int			nProductIndex1 = pDoc->m_boundProducts.TransformPartToProductIndex(pFoldSheet1->m_nProductPartIndex);
						int			nProductIndex2 = pDoc->m_boundProducts.TransformPartToProductIndex(pFoldSheet2->m_nProductPartIndex);
						BOOL		bDoSwap		   = FALSE;
						if (nProductIndex1 > nProductIndex2)
							bDoSwap = TRUE;
						else
							if (nProductIndex1 == nProductIndex2)
								if (pFoldSheet1->m_nFoldSheetNumber > pFoldSheet2->m_nFoldSheetNumber)
									bDoSwap = TRUE;
						if (bDoSwap)
						{
							InsertBefore(&rPrintSheet1, rPrintSheet2);
							RemoveAt(&rPrintSheet2);
							bChanged = TRUE;
						}
					}
			}

			GetNext(pos1);
		}
	}
	while (bChanged);

	pDoc->m_PrintSheetList.InitializeFoldSheetTypeLists();
}

CPrintSheet* CPrintSheetList::AddBlankSheet(CPrintingProfile& rPrintingProfile, int nInsertionIndex, CPrintGroup* pPrintGroup)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	POSITION pos = pDoc->m_PrintSheetList.FindIndex(nInsertionIndex);
	if ( (nInsertionIndex == -1) || ! pos)
	{
		pDoc->m_printingProfiles.AddTail(rPrintingProfile);
		CPrintingProfile& rNewPrintingProfile = pDoc->m_printingProfiles.GetTail();

		CLayout newLayout;
		newLayout.Create(rNewPrintingProfile);
		pDoc->m_PrintSheetList.m_Layouts.AddTail(newLayout);
		int nLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;
		
		if (pPrintGroup)
			pDoc->m_printGroupList.Add(*pPrintGroup);

		CPrintSheet newPrintSheet;
		newPrintSheet.Create(nLayoutIndex, rNewPrintingProfile.GetIndex(), &pDoc->m_PrintSheetList.m_Layouts.GetTail());	
		pDoc->m_PrintSheetList.AddTail(newPrintSheet);
		pDoc->m_PrintSheetList.ReNumberPrintSheets();
	
		return &pDoc->m_PrintSheetList.GetTail();
	}
	else
	{
		CLayout* pLayout = pDoc->m_PrintSheetList.GetAt(pos).GetLayout();
		if ( ! pLayout)
			return NULL;

		int nLayoutIndex = pLayout->GetIndex();

		pos = pDoc->m_printingProfiles.FindIndex(nLayoutIndex);
		if ( ! pos)
			return NULL;

		pos = pDoc->m_printingProfiles.InsertBefore(pos, rPrintingProfile);
		CPrintingProfile& rNewPrintingProfile = pDoc->m_printingProfiles.GetAt(pos);

		if (pPrintGroup)
			pDoc->m_printGroupList.InsertAt(nInsertionIndex, *pPrintGroup);

		pos = pDoc->m_PrintSheetList.GetHeadPosition();
		while (pos)
		{
			CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
			if (rPrintSheet.m_nLayoutIndex >= nLayoutIndex)
				rPrintSheet.m_nLayoutIndex++;
		}

		CLayout newLayout;
		newLayout.Create(rNewPrintingProfile);
		pos = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nLayoutIndex);
		pos = pDoc->m_PrintSheetList.m_Layouts.InsertBefore(pos, newLayout);

		CPrintSheet newPrintSheet;
		newPrintSheet.Create(nLayoutIndex, rNewPrintingProfile.GetIndex(), &pDoc->m_PrintSheetList.m_Layouts.GetAt(pos));	
		pos = pDoc->m_PrintSheetList.FindIndex(nInsertionIndex);
		pos = CList <CPrintSheet, CPrintSheet&>::InsertBefore(pos, newPrintSheet);
		pDoc->m_PrintSheetList.ReNumberPrintSheets();

		return &pDoc->m_PrintSheetList.GetAt(pos);
	}
}

CPrintSheet* CPrintSheetList::GetBlankSheet(int nProductPartIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	//if ( (nProductPartIndex >= 0) && (nProductPartIndex < pDoc->m_printGroupList.GetSize()) )
	//	if (pDoc->m_printGroupList[nProductPartIndex].IsFlat())
	//	{
			POSITION pos = GetHeadPosition();
			while (pos)
			{
				CPrintSheet& rPrintSheet = GetNext(pos);
				if ( ! rPrintSheet.m_FoldSheetLayerRefs.GetSize() && ! rPrintSheet.HasFlatProducts())
					if (nProductPartIndex == rPrintSheet.GetPrintGroupIndex())
						return &rPrintSheet;
			}
			return NULL;
		//}

	pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if ( ! rPrintSheet.m_FoldSheetLayerRefs.GetSize() && ! rPrintSheet.HasFlatProducts())
			if (nProductPartIndex >= 0)		
			//if (nProductPartIndex != -1)
			{
				if (nProductPartIndex == rPrintSheet.GetPrintGroupIndex())
					return &rPrintSheet;
			}
			else
				return &rPrintSheet;
	}

	return NULL;
}

BOOL CPrintSheetList::ImposeNUp(CFoldSheet* pFoldSheet, CPrintSheet* pTargetPrintSheet, int nNUp)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	if ( ! pFoldSheet || ! pTargetPrintSheet)
		return FALSE;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	int nLayerIndex = 0;
	int nCurNUp		= pFoldSheet->GetNUpPrintLayout(pTargetPrintSheet, nLayerIndex);
	if (nNUp == nCurNUp)
		return FALSE;

	if ( ! ImposeNUpLayout(pFoldSheet, pTargetPrintSheet, nNUp))
		return FALSE;

	CLayout* pLayout = pTargetPrintSheet->GetLayout();

	if (nNUp < nCurNUp)
	{
		int nNumToRemove = nCurNUp - nNUp;
		while (nNumToRemove)
		{
			int nComponentRefIndex = pTargetPrintSheet->GetNextLayerRefIndex(pFoldSheet, 0);
			if (nComponentRefIndex == -1)
				break;
			CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
			while (pPrintSheet)
			{
				pPrintSheet->m_FoldSheetLayerRefs.RemoveAt(nComponentRefIndex);
				pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
			}
			nNumToRemove--;
		}
	}
	else
	{
		int nComponentRefIndex = pTargetPrintSheet->GetNextLayerRefIndex(pFoldSheet, 0);
		if (nComponentRefIndex == -1)
			return FALSE;
		int nFoldSheetIndex = pTargetPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;

		CPrintSheet* pPrintSheet = (pLayout) ? pLayout->GetFirstPrintSheet() : NULL;
		while (pPrintSheet)
		{
			int nNumToAdd = nNUp - nCurNUp;
			while (nNumToAdd)
			{
				CFoldSheetLayerRef layerRef(nFoldSheetIndex, 0);
				pPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);
				nNumToAdd--;
			}
			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}
	}

	pDoc->m_Bookblock.ArrangeCover();

	pDoc->m_Bookblock.RemoveUnusedFoldSheets();
	pDoc->m_Bookblock.ReNumberFoldSheets(ALL_PARTS);
	pDoc->m_PageTemplateList.SortPages();
	pDoc->m_PrintSheetList.RemoveUnneededFoldSheetsComingAndGoing();
	pDoc->m_PrintSheetList.SortByFoldSheets();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	//pDoc->m_Bookblock.Reorganize();

	return TRUE;
}

BOOL CPrintSheetList::ImposeNUpLayout(CFoldSheet* pFoldSheet, CPrintSheet* pTargetPrintSheet, int nNUp)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	int nLayerIndex = 0;
	int nCurNUp = pFoldSheet->GetNUpPrintLayout(pTargetPrintSheet, nLayerIndex);
	if (nNUp == nCurNUp)
		return FALSE;

	CLayout* pLayout = (pTargetPrintSheet) ? pTargetPrintSheet->GetLayout() : NULL;
	if ( ! pLayout || ! pFoldSheet)
		return FALSE;

	if (nNUp < nCurNUp)
	{
		while ( (nNUp < nCurNUp) && (pLayout->m_FoldSheetLayerRotation.GetSize() > 0) )
		{
			pLayout->RemoveFoldSheetLayer(0, NULL, TRUE);
			nCurNUp--;
		}
	}
	else
	{
		int nFoldSheetIndex = pDoc->m_Bookblock.GetFoldSheetIndex(pFoldSheet);

		int nComponentRefIndex = pTargetPrintSheet->GetNextLayerRefIndex(pFoldSheet, 0);
		while (nComponentRefIndex != -1)
		{
			pLayout->RemoveFoldSheetLayer(nComponentRefIndex);
			nComponentRefIndex = pTargetPrintSheet->GetNextLayerRefIndex(pFoldSheet, nComponentRefIndex + 1);
		}

		int nLayerIndex = 0;
		float fXPos, fYPos;
		int nNumFoldSheetsX, nNumFoldSheetsY;
		BOOL bRotateBest = pTargetPrintSheet->FindFreePosition(pFoldSheet, nLayerIndex, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY);

		float fFoldSheetCutWidth, fFoldSheetCutHeight; 
		pFoldSheet->GetCutSize(nLayerIndex, fFoldSheetCutWidth, fFoldSheetCutHeight);
		if (bRotateBest) { float fTemp = fFoldSheetCutWidth; fFoldSheetCutWidth = fFoldSheetCutHeight; fFoldSheetCutHeight = fTemp; }
		float fFoldSheetPosX = fXPos;
		float fFoldSheetPosY = fYPos;
		for (int ny = 0; (ny < nNumFoldSheetsY) && (nNUp > 0); ny++)
		{
			fFoldSheetPosX = fXPos;
			for (int nx = 0; (nx < nNumFoldSheetsX) && (nNUp > 0); nx++)
			{
				pLayout->AddFoldSheetLayer(pFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, pFoldSheet->GetProductPart(), (bRotateBest) ? ROTATE90 : ROTATE0);
				fFoldSheetPosX += fFoldSheetCutWidth;

				//CFoldSheetLayerRef layerRef(nFoldSheetIndex, nLayerIndex);
				//pTargetPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);

				nNUp--;
			}
			fFoldSheetPosY += fFoldSheetCutHeight;
		}

		CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
		pLayout->m_FrontSide.PositionLayoutSide((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);
		pLayout->m_BackSide.PositionLayoutSide ((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);
	}

	pLayout->m_FrontSide.PositionLayoutSide();
	pLayout->m_BackSide.PositionLayoutSide();

	return TRUE;
}

BOOL CPrintSheetList::ImposeCollectLayout(CFoldSheet* pFoldSheet, CPrintSheet* pTargetPrintSheet, int nNumSheets)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	CLayout* pLayout = (pTargetPrintSheet) ? pTargetPrintSheet->GetLayout() : NULL;
	if ( ! pLayout || ! pFoldSheet)
		return FALSE;

	int nLayerIndex = 0;
	float fXPos, fYPos;
	int nNumFoldSheetsX, nNumFoldSheetsY;
	BOOL bRotateBest = pTargetPrintSheet->FindFreePosition(pFoldSheet, nLayerIndex, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY);

	float fFoldSheetCutWidth, fFoldSheetCutHeight; 
	pFoldSheet->GetCutSize(nLayerIndex, fFoldSheetCutWidth, fFoldSheetCutHeight);
	if (bRotateBest) { float fTemp = fFoldSheetCutWidth; fFoldSheetCutWidth = fFoldSheetCutHeight; fFoldSheetCutHeight = fTemp; }
	float fFoldSheetPosX = fXPos;
	float fFoldSheetPosY = fYPos;
	for (int ny = 0; (ny < nNumFoldSheetsY) && (nNumSheets > 0); ny++)
	{
		fFoldSheetPosX = fXPos;
		for (int nx = 0; (nx < nNumFoldSheetsX) && (nNumSheets > 0); nx++)
		{
			pLayout->AddFoldSheetLayer(pFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, pFoldSheet->GetProductPart(), (bRotateBest) ? ROTATE90 : ROTATE0);
			fFoldSheetPosX += fFoldSheetCutWidth;

			nNumSheets--;
		}
		fFoldSheetPosY += fFoldSheetCutHeight;
	}

	CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
	pLayout->m_FrontSide.PositionLayoutSide((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);
	pLayout->m_BackSide.PositionLayoutSide ((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);

	return TRUE;
}

CFoldSheet*  CPrintSheetList::ReImposePrintSheetType(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, CPrintSheet* pTargetPrintSheet, CPrintSheet& newPrintSheet, CLayout& newLayout)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	if ( ! pOldFoldSheet ||  ! pNewFoldSheet || ! pTargetPrintSheet)
		return NULL;
	int nProductPartIndex = pNewFoldSheet->m_nProductPartIndex;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_printGroupList.GetSize()) )
		return NULL;

	//if ( ! pTargetPrintSheet->IsSuitableLayout(&newLayout))
	//	return NULL;

	CPrintSheetView* pView = (CPrintSheetView*)theApp.GetView(RUNTIME_CLASS(CPrintSheetView));
	if (pView)
		pView->UndoSave();

	CLayout* pLayout = pTargetPrintSheet->GetLayout();
	if ( ! pLayout)
		return NULL;
	pTargetPrintSheet = pLayout->GetFirstPrintSheet();
	if ( ! pTargetPrintSheet)
		return NULL;

	CPrintingProfile& rProfile = pLayout->GetPrintingProfile();

	int nOldNumPages  = pOldFoldSheet->GetNumSamePrintLayout() * pOldFoldSheet->GetNumPages();
	int	nNewNumSheets = (pNewFoldSheet->GetNumPages() > 0) ? nOldNumPages/pNewFoldSheet->GetNumPages() : 1;
	if (nNewNumSheets == 0)
		nNewNumSheets = 1;

	CArray <int, int> processedFoldSheets;
	int nFirstFoldSheetIndex = -1;
	// remove all pFoldSheets from all printsheets of this layout
	while (1)
	{
		int nComponentRefIndex = pTargetPrintSheet->GetNextLayerRefIndex(pOldFoldSheet->m_strFoldSheetTypeName, nProductPartIndex, 0);
		if (nComponentRefIndex == -1)
			break;
		
		pLayout->RemoveFoldSheetLayer(nComponentRefIndex, &rProfile.m_press.m_pressDevice, TRUE);

		BOOL		 bFirstPrintSheet	 = TRUE;
		CPrintSheet* pPrintSheetToRemove = NULL;
		CPrintSheet* pPrintSheet		 = pLayout->GetFirstPrintSheet();
		while (pPrintSheet)
		{
			if ( (nComponentRefIndex < 0) || (nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize()) )
				continue;

			int nFoldSheetIndex = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;

			BOOL bFound = FALSE;
			for (int i = 0; i < processedFoldSheets.GetSize(); i++)
				if (processedFoldSheets[i] == nFoldSheetIndex)
					bFound = TRUE;
			if ( ! bFound)
			{
				if (nNewNumSheets > 0)
					pDoc->m_Bookblock.ChangeFoldSheet(nFoldSheetIndex, pNewFoldSheet);	// change in foldsheet list
				else
					pDoc->m_Bookblock.RemoveFoldSheet(nFoldSheetIndex);					// remove from description list
				processedFoldSheets.Add(nFoldSheetIndex);
				nNewNumSheets--;
			}

			if (pDoc->m_Bookblock.GetBindingProcess(nProductPartIndex).m_bComingAndGoing)		
			{
				int nIndexCGLast  = pDoc->m_Bookblock.m_FoldSheetList.GetCount() - 1;
				int nIndexCGGoing = nIndexCGLast - nFoldSheetIndex; 
				pDoc->m_Bookblock.RemoveFoldSheet(nIndexCGGoing);
			}

			if (nFirstFoldSheetIndex == -1)
				nFirstFoldSheetIndex = nFoldSheetIndex;

			pPrintSheet->m_FoldSheetLayerRefs.RemoveAt(nComponentRefIndex);
			if (pPrintSheet->IsEmpty() && ! bFirstPrintSheet)	// don't remove 1st printsheet, because at least one sheet must remain
				pPrintSheetToRemove = pPrintSheet;

			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);

			if (pPrintSheetToRemove)
				pDoc->m_PrintSheetList.RemoveAt(pPrintSheetToRemove);

			bFirstPrintSheet = FALSE;
		}
	}

	pDoc->m_Bookblock.RemoveUnusedFoldSheets(FALSE, TRUE);	// now remove from foldsheet list also, but don't remove 1st printsheet

	*pLayout		   = newLayout;
	*pTargetPrintSheet = newPrintSheet;

	if (nNewNumSheets > 0)
	{
		CArray <int, int> newFoldSheetIndices;
		pDoc->m_Bookblock.AddFoldSheetsSimple(pNewFoldSheet, nNewNumSheets, &newFoldSheetIndices);
	}

	CArray <LAYERSORTLIST, LAYERSORTLIST> foldSheetIndexList;
	for (int nComponentRefIndex = 0; nComponentRefIndex < pTargetPrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
	{
		LAYERSORTLIST ls = {nComponentRefIndex, pTargetPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex};
		foldSheetIndexList.Add(ls);
	}
	qsort((void*)foldSheetIndexList.GetData(), foldSheetIndexList.GetSize(), sizeof(LAYERSORTLIST), CompareFoldSheetIndex);

	CFoldSheet* pFoldSheet = NULL;
	int nCount = 0;
	int nFoldSheetIndex = 0, nLayerIndex = 0;
	while (pFoldSheet = GetNextUnassignedFoldsheetLayer(*pNewFoldSheet, nFoldSheetIndex, -1, -1, nLayerIndex))
	{
		int nPrevFoldSheetIndex = (foldSheetIndexList.GetSize()) ? foldSheetIndexList[0].nFoldSheetIndex : -1;
		for (int i = 0; i < foldSheetIndexList.GetSize(); i++)
		{
			if (nPrevFoldSheetIndex != foldSheetIndexList[i].nFoldSheetIndex)		
			{
				nFoldSheetIndex++;
				if ( ! GetNextUnassignedFoldsheetLayer(*pNewFoldSheet, nFoldSheetIndex, -1, -1, nLayerIndex))
					break;
			}

			CFoldSheetLayerRef layerRef(nFoldSheetIndex, nLayerIndex); 
			pTargetPrintSheet->m_FoldSheetLayerRefs.SetAtGrow(foldSheetIndexList[i].nComponentRefIndex, layerRef);
			nCount++;

			nPrevFoldSheetIndex = foldSheetIndexList[i].nFoldSheetIndex;
		}

		if (nCount < pLayout->m_FoldSheetLayerRotation.GetSize())	// sheet not full - not enough foldsheets remaining
		{
			pDoc->m_Bookblock.RemoveUnplacedFoldSheets(pNewFoldSheet->m_nProductPartIndex);
			break;
		}

		if (nCount >= foldSheetIndexList.GetSize())
			break;
	}

	ClonePrintSheets(*pNewFoldSheet, nLayerIndex, pTargetPrintSheet);

	pLayout->UpdatePressParams(rProfile.m_press.m_pressDevice, rProfile.m_press.m_backSidePressDevice);

	pDoc->m_Bookblock.ArrangeCover();

	pDoc->m_Bookblock.RemoveUnusedFoldSheets();
	pDoc->m_Bookblock.ReNumberFoldSheets(ALL_PARTS);
	pDoc->m_PageTemplateList.SortPages();
	pDoc->m_PrintSheetList.RemoveUnneededFoldSheetsComingAndGoing();
	pDoc->m_PrintSheetList.SortByFoldSheets();
	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_MarkTemplateList.FindPrintSheetLocations();
	pDoc->m_PrintSheetList.ReNumberPrintSheets();
	//pDoc->m_Bookblock.Reorganize();

	return pDoc->m_Bookblock.GetFoldSheetByIndex(nFirstFoldSheetIndex);
}

//BOOL CPrintSheetList::ChangeNUpLayout(CFoldSheet* pFoldSheet, CPrintSheet* pTargetPrintSheet, int nNUp)
//{
//	CImpManDoc* pDoc = CImpManDoc::GetDoc();
//	if ( ! pDoc)
//		return FALSE;
//
//	int nLayerIndex = 0;
//	int nCurNUp = pFoldSheet->GetNUpPrintLayout(pTargetPrintSheet, nLayerIndex);
//	if (nNUp == nCurNUp)
//		return FALSE;
//
//	CLayout* pLayout = (pTargetPrintSheet) ? pTargetPrintSheet->GetLayout() : NULL;
//	if ( ! pLayout || ! pFoldSheet)
//		return FALSE;
//
//	if (nNUp < nCurNUp)
//	{
//		while ( (nNUp < nCurNUp) && (pLayout->m_FoldSheetLayerRotation.GetSize() > 0) )
//		{
//			pLayout->RemoveFoldSheetLayer(0);
//			nCurNUp--;
//		}
//	}
//	else
//	{
//		int nComponentRefIndex = pTargetPrintSheet->GetNextLayerRefIndex(pFoldSheet, 0);
//		while (nComponentRefIndex != -1)
//		{
//			pLayout->RemoveFoldSheetLayer(nComponentRefIndex);
//			nComponentRefIndex = pTargetPrintSheet->GetNextLayerRefIndex(pFoldSheet, nComponentRefIndex + 1);
//		}
//
//		int   nLayerIndex = 0;
//		float fXPos, fYPos;
//		int	  nNumFoldSheetsX, nNumFoldSheetsY;
//		BOOL bRotate = pTargetPrintSheet->FindFreePosition(pFoldSheet, nLayerIndex, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY);
//		if (bRotate == -1)
//			if (AfxMessageBox(IDS_FOLDSHEET_DONTFIT_WARNING, MB_YESNO) == IDNO)
//				return FALSE;
//			else
//			{
//				bRotate = FALSE; nNumFoldSheetsX = nNumFoldSheetsY = 1;
//			}
//
//		float fFoldSheetCutWidth, fFoldSheetCutHeight; 
//		pFoldSheet->GetCutSize(nLayerIndex, fFoldSheetCutWidth, fFoldSheetCutHeight);
//		if (bRotate) { float fTemp = fFoldSheetCutWidth; fFoldSheetCutWidth = fFoldSheetCutHeight; fFoldSheetCutHeight = fTemp; }
//		float fFoldSheetPosX = fXPos;
//		float fFoldSheetPosY = fYPos;
//		for (int ny = 0; (ny < nNumFoldSheetsY) && (nNUp > 0); ny++)
//		{
//			fFoldSheetPosX = fXPos;
//			for (int nx = 0; (nx < nNumFoldSheetsX) && (nNUp > 0); nx++)
//			{
//				pLayout->AddFoldSheetLayer(pFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, pDoc->m_printGroupList[pFoldSheet->m_nProductPartIndex], (bRotate) ? ROTATE90 : ROTATE0);
//				fFoldSheetPosX += fFoldSheetCutWidth;
//				nNUp--;
//			}
//			fFoldSheetPosY += fFoldSheetCutHeight;
//		}
//	}
//
//	pLayout->m_FrontSide.PositionLayoutSide();
//	pLayout->m_BackSide.PositionLayoutSide();
//
//	return TRUE;
//}

BOOL CPrintSheetList::RearrangeLayout(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, CPrintSheet* pTargetPrintSheet, int nComponentRefIndex, int nNUp)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	CLayout* pLayout = (pTargetPrintSheet) ? pTargetPrintSheet->GetLayout() : NULL;
	if ( ! pLayout || ! pOldFoldSheet || ! pNewFoldSheet)
		return FALSE;
	CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
	if ( ! pPressDevice)
		return FALSE;

	int nProductPartIndex = pNewFoldSheet->m_nProductPartIndex;
	if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_printGroupList.GetSize()) )
		return FALSE;

	int nFoldSheetIndex = pTargetPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;
	for (int i = 0; i < pTargetPrintSheet->m_FoldSheetLayerRefs.GetSize(); i++)
	{
		if (pTargetPrintSheet->m_FoldSheetLayerRefs[i].m_nFoldSheetIndex == nFoldSheetIndex)
		{
			pLayout->RemoveFoldSheetLayer(i);
			pTargetPrintSheet->m_FoldSheetLayerRefs.RemoveAt(i);
			i--;
		}
	}

	int   nLayerIndex = 0;
	float fXPos, fYPos;
	int	  nNumFoldSheetsX, nNumFoldSheetsY;
	BOOL bRotate = pTargetPrintSheet->FindFreePosition(pNewFoldSheet, nLayerIndex, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY);
	if (bRotate == -1)
		if (KIMOpenLogMessage(IDS_FOLDSHEET_DONTFIT_WARNING, MB_YESNO | MB_ICONWARNING, IDYES) == IDNO)
			return FALSE;
		else
		{
			bRotate = FALSE; nNumFoldSheetsX = nNumFoldSheetsY = 1;
		}

	float fFoldSheetCutWidth, fFoldSheetCutHeight; 
	pNewFoldSheet->GetCutSize(nLayerIndex, fFoldSheetCutWidth, fFoldSheetCutHeight);
	if (bRotate) { float fTemp = fFoldSheetCutWidth; fFoldSheetCutWidth = fFoldSheetCutHeight; fFoldSheetCutHeight = fTemp; }
	float fFoldSheetPosX = fXPos;
	float fFoldSheetPosY = fYPos;
	for (int ny = 0; (ny < nNumFoldSheetsY) && (nNUp > 0); ny++)
	{
		fFoldSheetPosX = fXPos;
		for (int nx = 0; (nx < nNumFoldSheetsX) && (nNUp > 0); nx++)
		{
			pLayout->AddFoldSheetLayer(pNewFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, pNewFoldSheet->GetProductPart(), (bRotate) ? ROTATE90 : ROTATE0);
			fFoldSheetPosX += fFoldSheetCutWidth;

			CFoldSheetLayerRef layerRef(nFoldSheetIndex, 0);
			pTargetPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);
			nNUp--;
		}
		fFoldSheetPosY += fFoldSheetCutHeight;
	}

	pLayout->m_FrontSide.PositionLayoutSide();
	pLayout->m_BackSide.PositionLayoutSide();

	return TRUE;
}

BOOL CPrintSheetList::BookblockChangeFoldSheets(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, CPrintSheet* pRefPrintSheet, int nComponentRefIndex, int nNumNewSheets)
{
	if ( ! pOldFoldSheet || ! pNewFoldSheet || ! pRefPrintSheet)
		return FALSE;
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	CLayout* pLayout = (pRefPrintSheet) ? pRefPrintSheet->GetLayout() : NULL;
	if ( ! pLayout)
		return FALSE;

	if (nComponentRefIndex == -1)
		return FALSE;

	int nProductPartIndex = pOldFoldSheet->m_nProductPartIndex;

	if (nNumNewSheets == 0)
	{
		CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
		while (pPrintSheet)
		{
			if (nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize())
				continue;

			int nFoldSheetIndex = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;
			CFoldSheet* pFoldSheet = pDoc->m_Bookblock.GetFoldSheetByIndex(nFoldSheetIndex); 
			if (pFoldSheet)
			{
				int nNum = pFoldSheet->GetNumberOnly();
				*pFoldSheet = *pNewFoldSheet;
				pFoldSheet->m_nFoldSheetNumber = nNum;
			}

			pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
		}
		return TRUE;
	}

	CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		if (nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize())
			continue;

		int nFoldSheetIndex = pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;
		CFoldSheet* pFoldSheet = pDoc->m_Bookblock.GetFoldSheetByIndex(nFoldSheetIndex); 
		if (pFoldSheet)
		{
			if (nNumNewSheets > 0)
			{
				int nNum = pFoldSheet->GetNumberOnly();
				*pFoldSheet = *pNewFoldSheet;
				pFoldSheet->m_nFoldSheetNumber = nNum;
			}
			else
				pDoc->m_Bookblock.RemoveFoldSheet(nFoldSheetIndex);
		}
		nNumNewSheets--;

		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}

	if (nNumNewSheets > 0)
		pDoc->m_Bookblock.AddFoldSheets(pNewFoldSheet, nNumNewSheets, nProductPartIndex);

	return TRUE;
}

CFoldSheet* CPrintSheetList::AddFoldSheet(CFoldSheet& rNewFoldSheet, int nNUp, CPrintSheet* pTargetPrintSheet, CFoldSheet* pFoldSheetInsertAfter, int nPrintSheetNumSheets)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	CLayout* pTargetLayout = pTargetPrintSheet->GetLayout();
	if ( ! pTargetLayout)
		return NULL;
	CBoundProductPart& rProductPart = rNewFoldSheet.GetProductPart();
	if ( (fabs(rProductPart.m_fPageWidth) < 0.1f) || (fabs(rProductPart.m_fPageHeight) < 0.1f) )
		return NULL;

	CPrintingProfile& rPrintingProfile = pTargetPrintSheet->GetPrintingProfile();

	if ( (rPrintingProfile.m_press.m_nWorkStyle == WORK_AND_TURN) || (rPrintingProfile.m_press.m_nWorkStyle == WORK_AND_TUMBLE) )
		nNUp = INT_MAX;

	int	nPages				= rNewFoldSheet.GetNumPages();
	int	nNumUnImposedPages	= pDoc->m_PageTemplateList.GetNumUnimposedPages(rNewFoldSheet.m_nProductPartIndex);
	int nNumSheets			= (nPages > 0) ? min(nNumUnImposedPages / nPages, nPrintSheetNumSheets) : 0;

	CArray <int, int> newFoldSheetIndices;
	int	nNumAddedFoldSheets = 0;
	int nLayerIndex = 0;
	if ( ! pTargetLayout->HasPages())
	{
		//pTargetLayout->MakeNUp(*pTargetPrintSheet, &rNewFoldSheet, nNUp, FALSE);

		float fXPos, fYPos;
		int nNumFoldSheetsX, nNumFoldSheetsY;
		BOOL bRotateBest = pTargetPrintSheet->FindFreePosition(&rNewFoldSheet, nLayerIndex, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY, NULL, TRUE);
		
		if ( (nNumFoldSheetsX * nNumFoldSheetsY) <= 0)
			//if (nNUp == 1)
				nNumFoldSheetsX = nNumFoldSheetsY = 1;	// at least place one foldsheet - even if it does not fit on paper
			//else
				//return NULL;

		nNumAddedFoldSheets = pDoc->m_Bookblock.AddFoldSheetsSimple(&rNewFoldSheet, 1, &newFoldSheetIndices, -1, -1, pFoldSheetInsertAfter);

		float fFoldSheetCutWidth, fFoldSheetCutHeight; 
		rNewFoldSheet.GetCutSize(nLayerIndex, fFoldSheetCutWidth, fFoldSheetCutHeight);
		if (bRotateBest) { float fTemp = fFoldSheetCutWidth; fFoldSheetCutWidth = fFoldSheetCutHeight; fFoldSheetCutHeight = fTemp; }
		float fFoldSheetPosX = fXPos;
		float fFoldSheetPosY = fYPos;
		for (int ny = 0; (ny < nNumFoldSheetsY) && (nNUp > 0); ny++)
		{
			fFoldSheetPosX = fXPos;
			for (int nx = 0; (nx < nNumFoldSheetsX) && (nNUp > 0); nx++)
			{
				pTargetLayout->AddFoldSheetLayer(&rNewFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, rProductPart, (bRotateBest) ? ROTATE90 : ROTATE0);
				fFoldSheetPosX += fFoldSheetCutWidth;

				CFoldSheetLayerRef layerRef(pDoc->m_Bookblock.m_FoldSheetList.GetSize() - 1, nLayerIndex);
				pTargetPrintSheet->m_FoldSheetLayerRefs.Add(layerRef);

				nNUp--;
			}
			fFoldSheetPosY += fFoldSheetCutHeight;
		}

		pTargetLayout->m_nProductType = WORK_AND_BACK;		// the previous loop builds a work and back layout - whatever the style was defined before

		CPressDevice* pPressDevice = pTargetLayout->m_FrontSide.GetPressDevice();
		pTargetLayout->m_FrontSide.PositionLayoutSide((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);
		pTargetLayout->m_BackSide.PositionLayoutSide ((pPressDevice) ? pPressDevice->m_fGripper : 0.0f, pPressDevice);
	}

	pTargetLayout->SetWorkStyle(rPrintingProfile.m_press.m_nWorkStyle);

	nNumAddedFoldSheets += pDoc->m_Bookblock.AddFoldSheetsSimple(&rNewFoldSheet, nNumSheets - 1, &newFoldSheetIndices);

	CFoldSheet* pLastAddedFoldSheet = NULL;
	if (nNumAddedFoldSheets > 0)
		pLastAddedFoldSheet = (newFoldSheetIndices.GetSize()) ? pDoc->m_Bookblock.GetFoldSheetByIndex(newFoldSheetIndices[newFoldSheetIndices.GetSize() - 1]) : NULL;

	ClonePrintSheets(rNewFoldSheet, nLayerIndex, pTargetPrintSheet);

	//if (pFoldSheetInsertAfter)
		pDoc->m_Bookblock.Reorganize(NULL/*pTargetPrintSheet*/, NULL, rNewFoldSheet.m_nProductPartIndex);//, pTargetLayout);

	pTargetLayout->AlignLayoutObjects(XCENTER, pTargetPrintSheet);

	pTargetLayout->m_FrontSide.Invalidate();
	pTargetLayout->m_BackSide.Invalidate();

	pDoc->m_PrintSheetList.m_Layouts.AnalyzeMarks();

	return pLastAddedFoldSheet;
}

int CPrintSheetList::CompareFoldSheetIndex(const void *p1, const void *p2)
{
	if ( ! p1 || ! p2)
		return 0;

	LAYERSORTLIST pp1 = *((LAYERSORTLIST*)p1);
	LAYERSORTLIST pp2 = *((LAYERSORTLIST*)p2);

	if (abs(pp1.nFoldSheetIndex) < abs(pp2.nFoldSheetIndex))
		return -1;
	else
		if (abs(pp1.nFoldSheetIndex) > abs(pp2.nFoldSheetIndex))
			return 1;
		else
			return 0;
}

// create as many clones of pSourcePrintsheet as unassigned rNewFoldSheet are available or nMaxNumSheets is approached
void CPrintSheetList::ClonePrintSheets(CFoldSheet& /*rNewFoldSheet*/, int nLayerIndex, CPrintSheet* pSourcePrintSheet, int nMaxNumSheets)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CLayout* pSourceLayout = (pSourcePrintSheet) ? pSourcePrintSheet->GetLayout() : NULL;
	if ( ! pSourceLayout)
		return;
	if (pSourcePrintSheet->m_FoldSheetLayerRefs.GetSize() <= 0)
		return;

	for (int i = 0; i < pSourcePrintSheet->m_FoldSheetLayerRefs.GetSize(); i++)
	{
		if ( ! pSourcePrintSheet->GetFoldSheet(i))
			continue;

		CFoldSheet& rNewFoldSheet = *pSourcePrintSheet->GetFoldSheet(i);

		CArray <LAYERSORTLIST, LAYERSORTLIST> foldSheetIndexList;
		for (int nComponentRefIndex = 0; nComponentRefIndex < pSourcePrintSheet->m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
		{
			LAYERSORTLIST ls = {nComponentRefIndex, pSourcePrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex};
			foldSheetIndexList.Add(ls);
		}
		qsort((void*)foldSheetIndexList.GetData(), foldSheetIndexList.GetSize(), sizeof(LAYERSORTLIST), CompareFoldSheetIndex);

		CFoldSheet* pFoldSheet = NULL;
		int			nFoldSheetIndex = 0, nLayerIndexRet = 0;
		POSITION	pos		= rNewFoldSheet.m_LayerList.FindIndex(nLayerIndex);
		int			nPagesX = (pos) ? rNewFoldSheet.m_LayerList.GetAt(pos).m_nPagesX : 0;
		int			nPagesY = (pos) ? rNewFoldSheet.m_LayerList.GetAt(pos).m_nPagesY : 0;
		while (pFoldSheet = GetNextUnassignedFoldsheetLayer(rNewFoldSheet, nFoldSheetIndex, nPagesX, nPagesY, nLayerIndexRet))
		{
			CPrintSheet newPrintSheet;
			newPrintSheet.Create(pSourcePrintSheet->m_nLayoutIndex, rNewFoldSheet.m_nProductPartIndex, pSourceLayout);	

			int nCount = 0;
			int nPrevFoldSheetIndex = (foldSheetIndexList.GetSize()) ? foldSheetIndexList[0].nFoldSheetIndex : -1;
			for (int i = 0; i < foldSheetIndexList.GetSize(); i++)
			{
				if (nPrevFoldSheetIndex != foldSheetIndexList[i].nFoldSheetIndex)
				{
					nFoldSheetIndex++;
					if ( ! GetNextUnassignedFoldsheetLayer(rNewFoldSheet, nFoldSheetIndex, nPagesX, nPagesY, nLayerIndexRet))
						break;
				}

				CFoldSheetLayerRef layerRef(nFoldSheetIndex, nLayerIndexRet); 
				newPrintSheet.m_FoldSheetLayerRefs.SetAtGrow(foldSheetIndexList[i].nComponentRefIndex, layerRef);
				nCount++;

				nPrevFoldSheetIndex = foldSheetIndexList[i].nFoldSheetIndex;
			}

			if (nCount < pSourceLayout->m_FoldSheetLayerRotation.GetSize())	// sheet not full - not enough foldsheets remaining
			{
				pDoc->m_Bookblock.RemoveUnplacedFoldSheets(rNewFoldSheet.m_nProductPartIndex);
				break;
			}

			POSITION newSheetPos;
			int nPrintSheetIndex = GetInsertionPos(*pFoldSheet, pFoldSheet->m_nFoldSheetNumber);
			if (nPrintSheetIndex < 0)
				newSheetPos = AddHead(newPrintSheet);
			else
			{
				if (nPrintSheetIndex >= GetCount())
					newSheetPos = AddTail(newPrintSheet);
				else
				{
					POSITION sheetPos = FindIndex(nPrintSheetIndex);
					newSheetPos = CList <CPrintSheet, CPrintSheet&>::InsertAfter(sheetPos, newPrintSheet);
				}
			}

			CPrintSheet& rNewSheet = GetAt(newSheetPos);
			rNewSheet.CalcTotalQuantity(TRUE);

			if (nMaxNumSheets != -1)
			{
				nMaxNumSheets--;
				if (nMaxNumSheets <= 0)
					return;
			}
		}
	}
}

CFoldSheet* CPrintSheetList::GetNextUnassignedFoldsheetLayer(CFoldSheet& rRefFoldSheet, int& nFoldSheetIndex, int nPagesX, int nPagesY, int& nLayerIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(nFoldSheetIndex);
	while (pos)
	{
		CFoldSheet& rFoldSheet = pDoc->m_Bookblock.m_FoldSheetList.GetNext(pos);
		if (rFoldSheet.m_nProductPartIndex != rRefFoldSheet.m_nProductPartIndex)
		{
			nFoldSheetIndex++;
			continue;
		}
		if (rFoldSheet.m_strFoldSheetTypeName != rRefFoldSheet.m_strFoldSheetTypeName)
		{
			nFoldSheetIndex++;
			continue;
		}
		if (&rFoldSheet.GetPrintComponentsGroup() != &rRefFoldSheet.GetPrintComponentsGroup())
		{
			nFoldSheetIndex++;
			continue;
		}

		for (; nLayerIndex < rFoldSheet.m_LayerList.GetCount(); nLayerIndex++)
		{
			if ( (nPagesX >= 0) && (nPagesY >= 0) ) 
			{
				POSITION layerPos = rFoldSheet.m_LayerList.FindIndex(nLayerIndex);
				if (layerPos)
					if ( (nPagesX != rFoldSheet.m_LayerList.GetAt(layerPos).m_nPagesX) || (nPagesY != rFoldSheet.m_LayerList.GetAt(layerPos).m_nPagesY) )
						continue;
			}

			if ( (FoldSheetLayerIsAssigned(nFoldSheetIndex, nLayerIndex) == 0) && CheckFoldSheetComingAndGoing(rFoldSheet))
				return &rFoldSheet;
		}
		nLayerIndex = 0;
		nFoldSheetIndex++;
	}

	return NULL;
}

BOOL CPrintSheetList::DuplicateFoldSheet(CFoldSheet& rNewFoldSheet, int nNUp, CPrintSheet* pTargetPrintSheet)
{
	//CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	//if ( ! pDoc)
	//	return FALSE;
	//CLayout* pTargetLayout = pTargetPrintSheet->GetLayout();
	//if ( ! pTargetLayout)
	//	return FALSE;
	//int nProductPartIndex = rNewFoldSheet.m_nProductPartIndex;
	//if ( (nProductPartIndex < 0) || (nProductPartIndex >= pDoc->m_printGroupList.GetSize()) )
	//	return FALSE;

	//CFoldSheet* pNewFoldSheet = (nNUp > 1) ? NULL : pFoldSheet;		// if nNUp > 1 -> just duplicate, but no new foldsheet into bookblock
	//int			nNUpToAdd	  = nNUp - pFoldSheet->GetNUpPrintLayout(this);
	return TRUE;
}

int	CPrintSheetList::GetNumOutputObjects(int nObjectType)
{
	int nNumObjects = 0;
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		nNumObjects += rPrintSheet.GetNumOutputObjects(BOTHSIDES, NULL, nObjectType);
	}

	return nNumObjects;
}

POSITION CPrintSheetList::GetPos(CPrintSheet& rPrintSheet)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rRefPrintSheet = GetAt(pos);
		if (&rRefPrintSheet == &rPrintSheet)
			return pos;
		GetNext(pos);
	}

	return NULL;
}

BOOL CPrintSheetList::PrintSheetExist(CPrintSheet* pPrintSheet)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (&rPrintSheet == pPrintSheet)
			return TRUE;
	}

	return FALSE;
}

CFoldSheet*	CPrintSheetList::GetNextNewFoldSheet(CArray <int, int>& newFoldSheets, int nNewFoldSheetIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if ( (nNewFoldSheetIndex >= 0) && (nNewFoldSheetIndex < newFoldSheets.GetSize()) )
		return pDoc->m_Bookblock.GetFoldSheetByIndex(newFoldSheets[nNewFoldSheetIndex]);
	else
		return NULL;
}

CPrintSheet* CPrintSheetList::JDFAddPrintSheet(CString strSheetName, float fPaperWidth, float fPaperHeight, float fPlateWidth, float fPlateHeight, CString strPressDeviceName, CString strPressDeviceID, int nProductPartIndex, CPrintingProfile& rPrintingProfile)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	CPrintSheet*	pPrintSheet	 = pDoc->m_PrintSheetList.FindPrintSheet(strSheetName);
	CLayout*		pLayout		 = pPrintSheet->GetLayout();
	CPressDevice*	pPressDevice = (pLayout) ? pLayout->m_FrontSide.GetPressDevice() : NULL;
	if ( ! pPressDevice)
	{
		int nPressDeviceIndex		= (strPressDeviceID.IsEmpty()) ? theApp.m_PressDeviceList.GetDeviceIndex(strPressDeviceName) : theApp.m_PressDeviceList.GetDeviceIndexByID(strPressDeviceID);
		if (nPressDeviceIndex >= 0)
		{
			if ( ! pPrintSheet)
			{
				AddBlankSheet(rPrintingProfile);
				pPrintSheet = &GetTail();
			}
			pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			if ( ! pLayout)
				return NULL;
			POSITION pos = theApp.m_PressDeviceList.FindIndex(nPressDeviceIndex);
			if (pos)
			{
				CPressDevice& rPressDevice = theApp.m_PressDeviceList.GetAt(pos);
				pLayout->GetPrintingProfile().m_press.m_pressDevice			= rPressDevice;
				pLayout->GetPrintingProfile().m_press.m_backSidePressDevice = rPressDevice;
				pPrintSheet->GetPrintingProfile().m_strName = strPressDeviceName;
			}
		}
		else
		{
			//CPressDevice* pPressDevice = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetDevice(strPressDeviceName);
			//if ( ! pPressDevice)
			//{
				CPressDevice pressDevice; 
				pressDevice.Create();
				pressDevice.m_strName				= strPressDeviceName;
				pressDevice.m_fPlateWidth			= fPlateWidth;
				pressDevice.m_fPlateHeight			= fPlateHeight;
				pressDevice.m_nPressType			= CPressDevice::SheetPress;
				pressDevice.m_fPlateEdgeToPrintEdge	= 0.0f;
				pressDevice.m_fGripper				= 0.0f;
				pressDevice.m_fXRefLine				= 0.0f;
				pressDevice.m_fYRefLine1			= 0.0f;
				pressDevice.m_fYRefLine2			= 0.0f;
				//pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.AddTail(pressDevice);
				//nPressDeviceIndex = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetCount() - 1;
			//}
			//else
			//	nPressDeviceIndex = pDoc->m_PrintSheetList.m_Layouts.m_PressDevices.GetDeviceIndex(strPressDeviceName);

			if ( ! pPrintSheet)
			{
				AddBlankSheet(rPrintingProfile);
				pPrintSheet = &GetTail();
				if (pPrintSheet->GetPrintingProfile().m_strName.IsEmpty())
					pPrintSheet->GetPrintingProfile().m_strName = strPressDeviceName;
			}
			pLayout = (pPrintSheet) ? pPrintSheet->GetLayout() : NULL;
			if ( ! pLayout)
				return NULL;
			if (pLayout)
			{
				pLayout->GetPrintingProfile().m_press.m_pressDevice			= pressDevice;
				pLayout->GetPrintingProfile().m_press.m_backSidePressDevice = pressDevice;
			}
			//pLayout->m_FrontSide.m_nPressDeviceIndex = pLayout->m_BackSide.m_nPressDeviceIndex = nPressDeviceIndex;
		}
	}
	else
	{
		pPressDevice->m_fPlateWidth	 = fPlateWidth;
		pPressDevice->m_fPlateHeight = fPlateHeight;
	}

	if (! pLayout)
		return NULL;

	pPrintSheet->m_strNumerationStyle = strSheetName;
	pPrintSheet->m_strPDFTarget		 = _T("");

	//CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
	//if (! pPressDevice)
	//	return;

	CSheet sheet;
	sheet.m_fWidth  = fPaperWidth;
	sheet.m_fHeight = fPaperHeight;
	CString strMeasureFormat;
	strMeasureFormat.Format(_T("%s x %s"), (LPCTSTR)MeasureFormat("%.1f"), (LPCTSTR)MeasureFormat("%.1f"));

	switch (theApp.settings.m_iLanguage)	// switch to current language in order to get correct string format for sheet size
	{
		case 0: setlocale(LC_ALL, "german");	break;
		case 1: setlocale(LC_ALL, "english");	break;
		case 2: break;
		case 3: break;
		case 4: break;
		case 5: break;	// No holl. MFC4x.dll available
		//Default would already be set at reading ini-file
	}
	sheet.m_strSheetName.Format(strMeasureFormat, MeasureFormat(fPaperWidth), MeasureFormat(fPaperHeight));
	setlocale(LC_ALL, "english");	// go back to english to get postscript conform numbers via Format() (i.e. 0.1 0.1 etc. instead of 0,1 0,1)

	pLayout->AssignSheet(sheet);

	pDoc->m_PrintSheetList.ReNumberPrintSheets();

	return pPrintSheet;
}

CPrintSheet* CPrintSheetList::FindPrintSheet(CString strSheetName)
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		if (rPrintSheet.GetNumber() == strSheetName)
			return &rPrintSheet;
	}

	return NULL;
}

BOOL CPrintSheetList::DoAutoMarks()
{
	BOOL bChanged = FALSE;
	POSITION pos = m_Layouts.GetHeadPosition();
	while (pos)
	{
		CLayout& rLayout = m_Layouts.GetNext(pos);
		if (rLayout.AssignAutoMarks())
			bChanged = TRUE;
	}
	if (bChanged)
	{
		CImpManDoc* pDoc = CImpManDoc::GetDoc();
		if (pDoc)
		{
			pDoc->m_PageTemplateList.FindPrintSheetLocations();
			pDoc->m_MarkTemplateList.FindPrintSheetLocations();
			pDoc->m_MarkTemplateList.RemoveUnusedTemplates();
			pDoc->m_MarkTemplateList.ReorganizePrintSheetPlates();
			pDoc->m_MarkSourceList.RemoveUnusedSources();
			pDoc->m_ColorDefinitionTable.UpdateColorInfos();
		}
		//m_Layouts.AnalyzeMarks();
		theApp.OnUpdateView(RUNTIME_CLASS(CProdDataPrintView));	// reinit marklistcontrol

		CPrintSheetView*	pPrintSheetView	   = (CPrintSheetView*)theApp.CImpManApp::GetView(RUNTIME_CLASS(CPrintSheetView));
		CProdDataPrintView* pProdDataPrintView = (CProdDataPrintView*)theApp.CImpManApp::GetView(RUNTIME_CLASS(CProdDataPrintView));
		if (pProdDataPrintView && pPrintSheetView)
		{
			CDlgLayoutObjectProperties* pDlgLayoutObjectProperties = pPrintSheetView->GetDlgLayoutObjectProperties();
			if (pDlgLayoutObjectProperties)
				if (pDlgLayoutObjectProperties->m_bModeModify)
				{
					pDlgLayoutObjectProperties->m_pMark			= pProdDataPrintView->GetCurrentMark();
					pDlgLayoutObjectProperties->m_pTemplate		= (pDlgLayoutObjectProperties->m_pMark) ? &pDlgLayoutObjectProperties->m_pMark->m_markTemplate : NULL;
					pDlgLayoutObjectProperties->m_strObjectName = (pDlgLayoutObjectProperties->m_pTemplate) ? pDlgLayoutObjectProperties->m_pTemplate->m_strPageID : "";
					if (pDlgLayoutObjectProperties->m_hWnd)
						pDlgLayoutObjectProperties->UpdateData(FALSE);
					pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_pMark			= pProdDataPrintView->GetCurrentMark();
					pDlgLayoutObjectProperties->m_dlgLayoutObjectControl.m_pObjectTemplate	= (pDlgLayoutObjectProperties->m_pMark) ? &pDlgLayoutObjectProperties->m_pMark->m_markTemplate : NULL;
					if ( ! pDlgLayoutObjectProperties->m_pMark)
						pDlgLayoutObjectProperties->DestroyWindow();
				}

			if (pPrintSheetView->IsOpenMarkSetDlg())
			{
				CDlgMarkSet* pDlg = pPrintSheetView->GetDlgMarkSet();
				if (pDlg)
					pDlg->UpdateEnableStates();
			}
		}
	}

	return bChanged;
}

void CPrintSheetList::UpdateQuantities()
{
	POSITION pos = GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = GetNext(pos);
		rPrintSheet.CalcTotalQuantity();
	}
}


/////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////
// CPrintSheet

BOOL CFoldSheetLayerRef::operator==(const CFoldSheetLayerRef& rFoldSheetLayerRef)
{
	if (m_nFoldSheetIndex == rFoldSheetLayerRef.m_nFoldSheetIndex)
		if (m_nFoldSheetLayerIndex == rFoldSheetLayerRef.m_nFoldSheetLayerIndex)
			return TRUE;

	return FALSE;
}

BOOL CFoldSheetLayerRef::operator!=(const CFoldSheetLayerRef& rFoldSheetLayerRef)
{
	return !(*this == rFoldSheetLayerRef);
}

int CFoldSheetLayerRef::CompareFoldSheetIndex(const void *p1, const void *p2)
{
	if ( ! p1 || ! p2)
		return 0;

	CFoldSheetLayerRef* pRef1 = (CFoldSheetLayerRef*)p1; CFoldSheetLayerRef* pRef2 = (CFoldSheetLayerRef*)p2;

	if (pRef1->m_nFoldSheetIndex < pRef2->m_nFoldSheetIndex)
		return -1;
	else
		if (pRef1->m_nFoldSheetIndex > pRef2->m_nFoldSheetIndex)
			return 1;
		else
			return 0;
}

BOOL CFlatProductRef::operator==(const CFlatProductRef& rFlatProductRef)
{
	if (m_nFlatProductIndex == rFlatProductRef.m_nFlatProductIndex)
		//if (m_nAttribute == rFlatProductRef.m_nAttribute)
			return TRUE;

	return FALSE;
}

BOOL CFlatProductRef::operator!=(const CFlatProductRef& rFlatProductRef)
{
	return !(*this == rFlatProductRef);
}

int CFlatProductRef::CompareFlatProductIndex(const void *p1, const void *p2)
{
	if ( ! p1 || ! p2)
		return 0;

	CFlatProductRef* pRef1 = (CFlatProductRef*)p1; CFlatProductRef* pRef2 = (CFlatProductRef*)p2;

	if (pRef1->m_nFlatProductIndex < pRef2->m_nFlatProductIndex)
		return -1;
	else
		if (pRef1->m_nFlatProductIndex > pRef2->m_nFlatProductIndex)
			return 1;
		else
			return 0;
}

int CPrintSheetComponentRef::Compare(const void *p1, const void *p2)
{
	if ( ! p1 || ! p2)
		return 0;

	CPrintSheetComponentRef* pRef1 = (CPrintSheetComponentRef*)p1; CPrintSheetComponentRef* pRef2 = (CPrintSheetComponentRef*)p2;

	if (pRef1->m_nType < pRef2->m_nType)
		return -1;
	else
		if (pRef1->m_nType > pRef2->m_nType)
			return 1;
		else
		{
			if (pRef1->m_nType == CPrintSheetComponentRef::FoldSheetLayer)
			{
				if (pRef1->m_layerRef.m_nFoldSheetIndex < pRef2->m_layerRef.m_nFoldSheetIndex)
					return -1;
				else
					if (pRef1->m_layerRef.m_nFoldSheetIndex > pRef2->m_layerRef.m_nFoldSheetIndex)
						return 1;
					else
					{
						if (pRef1->m_layerRef.m_nFoldSheetLayerIndex < pRef2->m_layerRef.m_nFoldSheetLayerIndex)
							return -1;
						else
							if (pRef1->m_layerRef.m_nFoldSheetLayerIndex > pRef2->m_layerRef.m_nFoldSheetLayerIndex)
								return 1;
							else
								return 0;
					}
			}
			else
				if (pRef1->m_nType == CPrintSheetComponentRef::FlatProduct)
				{
					if (pRef1->m_nFlatProductIndex < pRef2->m_nFlatProductIndex)
						return -1;
					else
						if (pRef1->m_nFlatProductIndex > pRef2->m_nFlatProductIndex)
							return 1;
						else
							return 0;
				}
				else
					return 0;
		}
}



IMPLEMENT_SERIAL(CPrintSheet, CObject, VERSIONABLE_SCHEMA | 7)

CPrintSheet::CPrintSheet()
{
	m_strID							  = _T("");
	m_pLayout						  = NULL;
	m_FrontSidePlates.m_pPrintSheet	  = this;
	m_BackSidePlates.m_pPrintSheet	  = this;
	m_FrontSidePlates.m_nSheetSide	  = FRONTSIDE;
	m_BackSidePlates.m_nSheetSide	  = BACKSIDE;
	m_bDisplayOpen					  = FALSE;
	m_nProductPartIndex				  = 0;
	m_nQuantityExtra				  = 0;
	m_nQuantityTotal				  = 0;
}


// copy constructor for CPrintSheet
CPrintSheet::CPrintSheet(const CPrintSheet& rPrintSheet) 
{
	m_strID							  = _T("");
	m_pLayout						  = NULL;
	m_FrontSidePlates.m_pPrintSheet	  = this;
	m_BackSidePlates.m_pPrintSheet	  = this;
	m_FrontSidePlates.m_nSheetSide	  = FRONTSIDE;
	m_BackSidePlates.m_nSheetSide	  = BACKSIDE;

	Copy(rPrintSheet);
}


// operator overload for CPrintSheet - needed by CList functions:

const CPrintSheet& CPrintSheet::operator=(const CPrintSheet& rPrintSheet)
{
	Copy(rPrintSheet);	

	return *this;
}

void CPrintSheet::ReinitLinks()
{
	m_FrontSidePlates.m_pPrintSheet	= this;
	m_BackSidePlates.m_pPrintSheet	= this;
}

void CPrintSheet::Copy(const CPrintSheet& rPrintSheet)
{
	m_strID					 = rPrintSheet.m_strID;
	m_nProductPartIndex	 = rPrintSheet.m_nProductPartIndex;
	m_dspi					 = rPrintSheet.m_dspi;

	m_nPrintSheetNumber		 = rPrintSheet.m_nPrintSheetNumber;
	m_strNumerationStyle	 = rPrintSheet.m_strNumerationStyle;
	m_nNumerationOffset		 = rPrintSheet.m_nNumerationOffset;
	m_nPrintSheetParentIndex = rPrintSheet.m_nPrintSheetParentIndex;
	m_nPrintSheetType		 = rPrintSheet.m_nPrintSheetType;
	m_fShinglingOffset		 = rPrintSheet.m_fShinglingOffset;
	m_fShinglingStep		 = rPrintSheet.m_fShinglingStep;
	m_nLayoutIndex			 = rPrintSheet.m_nLayoutIndex;
	m_nQuantityExtra		 = rPrintSheet.m_nQuantityExtra;
	m_nQuantityTotal		 = rPrintSheet.m_nQuantityTotal;

	CFoldSheetLayerRef layerRef;
	m_FoldSheetLayerRefs.RemoveAll(); // no append, so target list has to be empty 
	for (int i = 0; i < rPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
	{
		layerRef = rPrintSheet.m_FoldSheetLayerRefs[i];
		m_FoldSheetLayerRefs.Add(layerRef);
	}

	m_flatProductRefs.RemoveAll(); // no append, so target list has to be empty 
	for (int i = 0; i < rPrintSheet.m_flatProductRefs.GetSize(); i++)
	{
		m_flatProductRefs.Add(CFlatProductRef(rPrintSheet.m_flatProductRefs[i].m_nFlatProductIndex, rPrintSheet.m_flatProductRefs[i].m_nAttribute));
	}

	POSITION pos;
	CPlate plate;
	m_FrontSidePlates.RemoveAll();
	m_BackSidePlates.RemoveAll();
	pos = rPrintSheet.m_FrontSidePlates.GetHeadPosition();
	while (pos)
	{
		plate = rPrintSheet.m_FrontSidePlates.GetNext(pos);
		m_FrontSidePlates.AddTail(plate);
	}
	m_FrontSidePlates.m_defaultPlate = rPrintSheet.m_FrontSidePlates.m_defaultPlate;

	pos = rPrintSheet.m_BackSidePlates.GetHeadPosition();
	while (pos)
	{
		plate = rPrintSheet.m_BackSidePlates.GetNext(pos);
		m_BackSidePlates.AddTail(plate);
	}
	m_BackSidePlates.m_defaultPlate = rPrintSheet.m_BackSidePlates.m_defaultPlate;

	m_FrontSidePlates.m_nTopmostPlate = rPrintSheet.m_FrontSidePlates.m_nTopmostPlate;
	m_BackSidePlates.m_nTopmostPlate  = rPrintSheet.m_BackSidePlates.m_nTopmostPlate;
	m_FrontSidePlates.m_nSheetSide	  = rPrintSheet.m_FrontSidePlates.m_nSheetSide;
	m_BackSidePlates.m_nSheetSide	  = rPrintSheet.m_BackSidePlates.m_nSheetSide;

	m_strPDFTarget					  = rPrintSheet.m_strPDFTarget;
	m_bDisplayOpen					  = rPrintSheet.m_bDisplayOpen;

	m_FoldSheetTypeList.Copy(rPrintSheet.m_FoldSheetTypeList);
}


void CPrintSheet::Create(CFoldSheetLayerRef layerRef, int nLayoutIndex, int nProductPartIndex, CLayout* pLayout)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CPrintingProfile& rPrintingProfile = pLayout->GetPrintingProfile();

	CString strOutputTarget = rPrintingProfile.m_prepress.m_strPrintSheetOutputProfile;
	CPDFTargetList pdfTargetList;
	pdfTargetList.Load();
	if ( ! pDoc->m_PrintSheetList.m_pdfTargets.Find(strOutputTarget))
	{
		pDoc->m_PrintSheetList.m_pdfTargets.RemoveAll();
		for (int i = 0; i < pdfTargetList.GetSize(); i++)
			pDoc->m_PrintSheetList.m_pdfTargets.Add(pdfTargetList.ElementAt(i));
	}

	m_pLayout				 = pLayout;

	m_nPrintSheetNumber		 = 0;
	m_strNumerationStyle	 = _T("$n");
	m_nNumerationOffset		 = 0;
	m_nPrintSheetParentIndex = 0;
	m_nPrintSheetType		 = CPrintSheet::TopBottom;
	m_fShinglingOffset		 = 0.0f;
	m_fShinglingStep		 = 0.01f;
	m_nLayoutIndex			 = nLayoutIndex;
	m_strPDFTarget			 = strOutputTarget;
	m_nProductPartIndex		 = nProductPartIndex;
	m_nQuantityExtra		 = 0;
	m_nQuantityTotal		 = 0;

	// FoldSheetLayerRefs
	m_FoldSheetLayerRefs.RemoveAll(); 
	if (layerRef.m_nFoldSheetIndex >= 0)
		m_FoldSheetLayerRefs.Add(layerRef);

	m_FrontSidePlates.RemoveAll(); 
	m_BackSidePlates.RemoveAll(); 

	m_FrontSidePlates.m_defaultPlate.m_fOverbleedOutside = (fabs(rPrintingProfile.m_prepress.m_fBleed) < 0.001f) ? theApp.settings.m_fDefaultBleed : rPrintingProfile.m_prepress.m_fBleed;
	m_BackSidePlates.m_defaultPlate.m_fOverbleedOutside  = (fabs(rPrintingProfile.m_prepress.m_fBleed) < 0.001f) ? theApp.settings.m_fDefaultBleed : rPrintingProfile.m_prepress.m_fBleed;

	m_bDisplayOpen = FALSE;
}

void CPrintSheet::Create(int nLayoutIndex, int nProductPartIndex, CLayout* pLayout)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

	CPrintingProfile& rPrintingProfile = pLayout->GetPrintingProfile();

	CString strOutputTarget = rPrintingProfile.m_prepress.m_strPrintSheetOutputProfile;
	CPDFTargetList pdfTargetList;
	pdfTargetList.Load();
	if (pDoc)
	{
		if ( ! pDoc->m_PrintSheetList.m_pdfTargets.Find(strOutputTarget))
		{
			pDoc->m_PrintSheetList.m_pdfTargets.RemoveAll();
			for (int i = 0; i < pdfTargetList.GetSize(); i++)
				pDoc->m_PrintSheetList.m_pdfTargets.Add(pdfTargetList.ElementAt(i));
		}
	}

	m_strPDFTarget			 = strOutputTarget;

	m_pLayout				 = pLayout;

	m_nPrintSheetNumber		 = 1;
	m_strNumerationStyle	 = _T("$n");
	m_nNumerationOffset		 = 0;
	m_nPrintSheetParentIndex = 0;
	m_nPrintSheetType		 = CPrintSheet::TopBottom;
	m_fShinglingOffset		 = 0.0f;
	m_fShinglingStep		 = 0.01f;
	m_nLayoutIndex			 = nLayoutIndex;
	m_nProductPartIndex		 = nProductPartIndex;
	m_nQuantityExtra		 = 0;
	m_nQuantityTotal		 = 0;

	m_FoldSheetLayerRefs.RemoveAll(); 
	m_flatProductRefs.RemoveAll();

	m_FrontSidePlates.RemoveAll(); 
	m_BackSidePlates.RemoveAll(); 
	m_FoldSheetTypeList.RemoveAll();

	m_FrontSidePlates.m_defaultPlate.m_fOverbleedOutside = (fabs(rPrintingProfile.m_prepress.m_fBleed) < 0.001f) ? theApp.settings.m_fDefaultBleed : rPrintingProfile.m_prepress.m_fBleed;
	m_BackSidePlates.m_defaultPlate.m_fOverbleedOutside  = (fabs(rPrintingProfile.m_prepress.m_fBleed) < 0.001f) ? theApp.settings.m_fDefaultBleed : rPrintingProfile.m_prepress.m_fBleed;

	m_bDisplayOpen = FALSE;
}

void CPrintSheet::DeleteContents()
{
	m_FoldSheetLayerRefs.RemoveAll(); 
	m_flatProductRefs.RemoveAll();
	m_FrontSidePlates.RemoveAll(); 
	m_BackSidePlates.RemoveAll(); 
	m_FoldSheetTypeList.RemoveAll();
	m_flatProductTypeList.RemoveAll();
	m_flatProductFormatList.RemoveAll();
}

int CPrintSheet::GetIndex()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc == NULL)
		return NULL;

	int		 nIndex = 0;
	POSITION pos	= pDoc->m_PrintSheetList.GetHeadPosition();
	while (pos)
	{
		CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
		if (&rPrintSheet == this)
			return nIndex;

		nIndex++;
	}
	return -1;
}

CLayout* CPrintSheet::GetLayout(CImpManDoc* pAuxDoc)	// get print sheet's layout
{
	if (this == NULL)
		return NULL;

	if (m_pLayout)
		return m_pLayout;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		pDoc = theApp.m_pActiveDoc;
	if ( ! pDoc)		// Document not yet present - i.e. we are serializing (loading)
		pDoc = pAuxDoc;
	if ( ! pDoc)
		return NULL;

	if ((m_nLayoutIndex < 0) || (m_nLayoutIndex >= pDoc->m_PrintSheetList.m_Layouts.GetCount()))
		return NULL;
	if ( ! pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition())	// needed to avoid trouble with FindIndex() while serializing in UndoRestore()
		return NULL;

	POSITION pos  = pDoc->m_PrintSheetList.m_Layouts.FindIndex(m_nLayoutIndex);

	if ( ! pos)
		return NULL;

	return (m_pLayout = &(pDoc->m_PrintSheetList.m_Layouts.GetAt(pos)));
}

CPrintGroup& CPrintSheet::GetPrintGroup()
{
	static private CPrintGroup nullPrintGroup;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullPrintGroup;

	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return nullPrintGroup;

	BOOL	 bFound = FALSE;
	int		 nIndex = 0;
	POSITION pos = pDoc->m_PrintSheetList.m_Layouts.GetHeadPosition();
	while (pos)
	{
		if (pLayout == &pDoc->m_PrintSheetList.m_Layouts.GetNext(pos))
		{
			bFound = TRUE;
			break;
		}
		nIndex++;
	}

	if ( ! bFound)
		return nullPrintGroup;
	else
		if ( (nIndex < 0) || (nIndex >= pDoc->m_printGroupList.GetSize()) )
			return nullPrintGroup;
		else
			return pDoc->m_printGroupList[nIndex];
}

CString	CPrintSheet::GetPrintGroupName()
{
	CPrintGroup& rPrintGroup = GetPrintGroup();
	return rPrintGroup.m_strName;
}

CPrintComponent CPrintSheet::GetFirstPrintComponent()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return CPrintComponent();

	if (m_FoldSheetLayerRefs.GetSize() > 0)
		return pDoc->m_printComponentsList.FindFoldSheetLayerComponent(m_FoldSheetLayerRefs[0].m_nFoldSheetIndex, m_FoldSheetLayerRefs[0].m_nFoldSheetLayerIndex);
	else
		if (m_flatProductRefs.GetSize() > 0)
			return pDoc->m_printComponentsList.FindFlatComponent(m_flatProductRefs[0].m_nFlatProductIndex);

	// empty printsheet
	CPrintGroup& rPrintGroup = GetPrintGroup();
	if (rPrintGroup.m_componentList.GetSize() <= 0)
		return CPrintComponent();

	CPrintGroupComponent printGroupComponent = rPrintGroup.m_componentList[0];
	if (printGroupComponent.m_nProductClass == CPrintGroupComponent::Bound)
		return pDoc->m_printComponentsList.FindPageSectionComponent(printGroupComponent.m_nProductClassIndex);
	else
		return pDoc->m_printComponentsList.FindFlatComponent(printGroupComponent.m_nFlatProductIndex);

	return CPrintComponent();
}

CPrintComponentsGroup& CPrintSheet::GetFirstPrintComponentsGroup()
{
	static CPrintComponentsGroup nullGroup;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullGroup;

	return GetFirstPrintComponent().GetPrintComponentsGroup();;
}

CBoundProductPart* CPrintSheet::GetFirstBoundProductPart()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	if (IsEmpty())
	{
		CPrintGroup& rPrintGroup = GetPrintGroup();
		if (rPrintGroup.IsNull())
			return NULL;

		return rPrintGroup.GetFirstBoundProductPart();
	}

	m_boundProductPartRefList.RemoveAll();

	for (int i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = GetFoldSheet(i);
		if (pFoldSheet)
		{
			BOOL bFound = FALSE;
			for (int j = 0; j < m_boundProductPartRefList.GetSize(); j++)
			{
				if (m_boundProductPartRefList[j] == pFoldSheet->m_nProductPartIndex)
				{
					bFound = TRUE;
					break;
				}
			}
			if ( ! bFound)
				m_boundProductPartRefList.Add(pFoldSheet->m_nProductPartIndex);
		}
	}

	if (m_boundProductPartRefList.GetSize())
		return &pDoc->m_boundProducts.GetProductPart(m_boundProductPartRefList[0]);
	else
		return NULL;
}

CBoundProductPart* CPrintSheet::GetNextBoundProductPart(CBoundProductPart* pProductPart)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int nCurProductPartIndex = pProductPart->GetIndex();
	int i = 0;
	for (; i < m_boundProductPartRefList.GetSize(); i++)
	{
		if (m_boundProductPartRefList[i] == nCurProductPartIndex)
			break;
	}
	i++;
	if (i < m_boundProductPartRefList.GetSize())
		return &pDoc->m_boundProducts.GetProductPart(m_boundProductPartRefList[i]);
	else
		return NULL;
}

CFlatProductType* CPrintSheet::GetFirstFlatFormatType()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	InitializeFlatProductTypeList();

	m_flatProductFormatList.RemoveAll();
	for (int i = 0; i < m_flatProductTypeList.GetSize(); i++)
	{
		CFlatProduct* pFlatProduct = m_flatProductTypeList[i].m_pFlatProduct;

		BOOL bFound = FALSE;
		for (int j = 0; j < m_flatProductFormatList.GetSize(); j++)
		{
			CFlatProduct* pFlatProduct2 = m_flatProductFormatList[j].m_pFlatProduct;
			if ( (fabs(pFlatProduct->m_fTrimSizeWidth - pFlatProduct2->m_fTrimSizeWidth) < 0.01f) && (fabs(pFlatProduct->m_fTrimSizeHeight - pFlatProduct2->m_fTrimSizeHeight) < 0.01f) )
			{
				bFound = TRUE;
				m_flatProductFormatList[j].m_nNumUp++;
			}
		}

		if ( ! bFound)
		{
			CFlatProductType pt = m_flatProductTypeList[i];
			pt.m_nNumUp = 1;
			m_flatProductFormatList.Add(pt);
		}
	}

	if (m_flatProductFormatList.GetSize())
		return &m_flatProductFormatList[0];
	else
		return NULL;
}

CFlatProductType* CPrintSheet::GetNextFlatFormatType(CFlatProductType* pFlatProductType)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	int i = 0;
	if (pFlatProductType)
	{
		for (; i < m_flatProductFormatList.GetSize(); i++)
		{
			if (&m_flatProductFormatList[i] == pFlatProductType)
			{
				i++;
				break;
			}
		}
	}

	if (i < m_flatProductFormatList.GetSize())
		return &m_flatProductFormatList[i];
	else
		return NULL;
}

CFoldSheet*	CPrintSheet::GetFirstFoldSheetProductPart(int nProductPartIndex)
{
	for (int i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = GetFoldSheet(i);
		if ( ! pFoldSheet)
			continue;
		if (pFoldSheet->m_nProductPartIndex == nProductPartIndex)
			return pFoldSheet;
	}

	return NULL;
}

CFoldSheet*	CPrintSheet::GetFoldSheet(int nComponentRefIndex)
{
	if ((nComponentRefIndex < 0) || (nComponentRefIndex >= m_FoldSheetLayerRefs.GetSize()))
		return NULL;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	POSITION pos = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex);

	if ( ! pos)
		return NULL;

	return &(pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos));
}


CFoldSheetLayer* CPrintSheet::GetFoldSheetLayer(int nComponentRefIndex)
{
	CFoldSheet* pFoldSheet = GetFoldSheet(nComponentRefIndex);

	if (!pFoldSheet)
		return NULL;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;

	POSITION pos = pFoldSheet->m_LayerList.FindIndex(m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex);

	if ( ! pos)
		return NULL;

	return &(pFoldSheet->m_LayerList.GetAt(pos));
}

CFlatProduct& CPrintSheet::GetFlatProduct(int nComponentRefIndex)
{
	static CFlatProduct nullFlatProduct;

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return nullFlatProduct;

	if (nComponentRefIndex < 0)
		return nullFlatProduct;

	int nFlatProductIndex = (nComponentRefIndex >= m_flatProductRefs.GetSize()) ? nComponentRefIndex : m_flatProductRefs[nComponentRefIndex].m_nFlatProductIndex;
	
	return pDoc->m_flatProducts.FindByIndex(nFlatProductIndex);
}

int CPrintSheet::GetFlatProductIndex(int nComponentRefIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	if (nComponentRefIndex < 0)
		return -1;

	return (nComponentRefIndex >= m_flatProductRefs.GetSize()) ? nComponentRefIndex : m_flatProductRefs[nComponentRefIndex].m_nFlatProductIndex;
}

BOOL CPrintSheet::HasCoverProduct()
{
	CBoundProductPart* pProductPart = GetFirstBoundProductPart();
	while (pProductPart)
	{
		if (pProductPart->IsCover())
			return TRUE;
		pProductPart = GetNextBoundProductPart(pProductPart);
	}

	return FALSE;
}

BOOL CPrintSheet::HasCoverProductsOnly()
{
	CBoundProductPart* pProductPart = GetFirstBoundProductPart();
	if ( ! pProductPart)
		return FALSE;

	while (pProductPart)
	{
		if ( ! pProductPart->IsCover())
			return FALSE;
		pProductPart = GetNextBoundProductPart(pProductPart);
	}

	return TRUE;
}

BOOL CPrintSheet::HasPerfectBoundProduct()
{
	CBoundProductPart* pProductPart = GetFirstBoundProductPart();
	while (pProductPart)
	{
		if (pProductPart->GetBoundProduct().m_bindingParams.m_bindingDefs.m_nBinding == CBindingDefs::PerfectBound)
			return TRUE;
		pProductPart = GetNextBoundProductPart(pProductPart);
	}

	return FALSE;
}

BOOL CPrintSheet::HasBoundProductPart(int nProductPartIndex)
{
	CBoundProductPart* pProductPart = GetFirstBoundProductPart();
	while (pProductPart)
	{
		if (pProductPart->GetIndex() == nProductPartIndex)
			return TRUE;
		pProductPart = GetNextBoundProductPart(pProductPart);
	}

	return FALSE;
}

int CPrintSheet::GetMaxProductQuantity()
{
	int nMaxQuantity = 0;
	for (int i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = GetFoldSheet(i);
		nMaxQuantity = max(nMaxQuantity, (pFoldSheet) ? pFoldSheet->GetQuantityPlanned() : nMaxQuantity);
	}

	for (int i = 0; i < m_flatProductRefs.GetSize(); i++)
	{
		CFlatProduct& rFlatProduct = GetFlatProduct(i);
		nMaxQuantity = max(nMaxQuantity, rFlatProduct.m_nDesiredQuantity);
	}

	return nMaxQuantity;
}

int CPrintSheet::MakeFlatProductRefIndex(int nFlatProductIndex)
{
	m_flatProductRefs.Add(CFlatProductRef(nFlatProductIndex, 0));
	return m_flatProductRefs.GetSize() - 1;
}

// get the nth foldsheet layer with name strFoldSheetTypeName and nLayerIndex
int	CPrintSheet::GetNUpsFoldSheetLayerRefIndex(const CString& strFoldSheetTypeName, int nLayerIndex, int nUp)
{	
	for (int i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = GetFoldSheet(i);
		if (pFoldSheet)
			if (pFoldSheet->m_strFoldSheetTypeName == strFoldSheetTypeName)
				if (m_FoldSheetLayerRefs[i].m_nFoldSheetLayerIndex == nLayerIndex)
					if (nUp == 1)
						return i;
					else
						nUp--;
	}
	return -1;
}

// get number of foldsheet layers with name strFoldSheetTypeName
int	CPrintSheet::GetNumUpFoldSheetType(const CString& strFoldSheetTypeName, int nLayerIndex)
{
	int nUp = 0;
	for (int i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = GetFoldSheet(i);
		if (pFoldSheet)
			if (pFoldSheet->m_strFoldSheetTypeName == strFoldSheetTypeName)
				if (m_FoldSheetLayerRefs[i].m_nFoldSheetLayerIndex == nLayerIndex)
					nUp++;
	}
	return nUp;
}

int CPrintSheet::LayerTypeToLayerRef(int nLayerTypeIndex)
{
	for (int i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
		if (GetFoldSheet(i) == m_FoldSheetTypeList[nLayerTypeIndex].m_pFoldSheet)
			return i;

	return -1;
}

int CPrintSheet::FoldSheetLayerToLayerRef(CFoldSheetLayer* pLayer)
{
	for (int i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
		if (GetFoldSheetLayer(i) == pLayer)
			return i;

	return -1;
}

int	CPrintSheet::GetNextLayerRefIndex(CFoldSheet* pFoldSheet, int nComponentRefIndex)
{
	for (int i = nComponentRefIndex; i < m_FoldSheetLayerRefs.GetSize(); i++)
		if (GetFoldSheet(i) == pFoldSheet)
			return i;

	return -1;
}

int	CPrintSheet::GetNextLayerRefIndex(CString strFoldSheetTypeName, int nProductPartIndex, int nComponentRefIndex)
{
	for (int i = nComponentRefIndex; i < m_FoldSheetLayerRefs.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = GetFoldSheet(i);
		if ( ! pFoldSheet)
			continue;
		if (pFoldSheet->m_strFoldSheetTypeName == strFoldSheetTypeName)
			if (pFoldSheet->m_nProductPartIndex == nProductPartIndex)
				return i;
	}

	return -1;
}

void CPrintSheet::CopyAndPasteFoldSheet(CPrintSheet* pSourcePrintSheet, int nSourceLayerRefIndex, BOOL bSourceInnerForm,
										CFoldSheetDropTargetInfo dropTargetInfo, BOOL bTargetBackSide, BOOL bAutoAssign, CLayoutObject* pSpreadObject, BOOL bRearrange)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();

//	if (!bAutoAssign)
		CopyFoldSheetLayerData(pSourcePrintSheet, nSourceLayerRefIndex);

	if (m_nLayoutIndex == -1)	// New printsheet w/o layout yet
	{
		if (pSourcePrintSheet->m_FoldSheetLayerRefs.GetSize() == 1)	// In this case we can use this layout and that's it
			m_nLayoutIndex = pSourcePrintSheet->m_nLayoutIndex;
		else
		{
			POSITION	 pos		 = pDoc->m_Bookblock.m_FoldSheetList.FindIndex(
									   pSourcePrintSheet->m_FoldSheetLayerRefs[nSourceLayerRefIndex].m_nFoldSheetIndex);
			CFoldSheet&  rFoldSheet  = pDoc->m_Bookblock.m_FoldSheetList.GetAt(pos);
			CPrintSheet* pPrintSheet = pDoc->m_PrintSheetList.FindSuitablePrintSheet(rFoldSheet, 
									   pSourcePrintSheet->m_FoldSheetLayerRefs[nSourceLayerRefIndex].m_nFoldSheetLayerIndex); 
			if (pPrintSheet)													// Look if there is already a suitable 
				m_nLayoutIndex = pPrintSheet->m_nLayoutIndex;					// printsheet based on a layout which can
			else																// be used for the new foldsheet layer
			{
				CLayout layout;
				layout.Create(pSourcePrintSheet->GetLayout(), nSourceLayerRefIndex, rFoldSheet.m_nProductPartIndex);	// Create new layout
				pDoc->m_PrintSheetList.m_Layouts.AddTail(layout);
				m_nLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;
				pDoc->m_PrintSheetList.ReNumberPrintSheets();
			}
		}
	}
	else
	{
		// Look if more than one printsheet is based on the TARGET layout 
		//		if yes assign a copy to this printsheet and modify this copy (but only if NOT bAutoAssign)
		if ((GetLayout()->GetNumPrintSheetsBasedOn() > 1) && !bAutoAssign)
		{
			CLayout layout;
			layout = *GetLayout();
			layout.AssignDefaultName();
			pDoc->m_PrintSheetList.m_Layouts.AddTail(layout);
			m_nLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;
			pDoc->m_PrintSheetList.ReNumberPrintSheets();
			//GetLayout()->m_FrontSide.Analyse();
			//GetLayout()->m_BackSide.Analyse();

			pSpreadObject = NULL;//GetLayout()->FindSimilarObject(pSpreadObject);
		}

		// If bAutoAssign source and target layout are THE SAME -> so we just expand the target layout 
		// (which is also the source layout) and that's it.

		GetLayout()->CopyAndPasteFoldSheet(pSourcePrintSheet, this, nSourceLayerRefIndex, bSourceInnerForm, dropTargetInfo, bTargetBackSide, pSpreadObject, bRearrange);
	}

	if (GetLayout())
		GetLayout()->AnalyzeMarks();

	pDoc->m_PageTemplateList.FindPrintSheetLocations();
	pDoc->m_PageTemplateList.ReorganizePrintSheetPlates();
	pDoc->m_PrintSheetList.ReorganizeColorInfos();
}
					   

void CPrintSheet::CutAndPasteFoldSheet(CPrintSheet* pSourcePrintSheet, int nSourceLayerRefIndex, BOOL bSourceInnerForm,
									   CFoldSheetDropTargetInfo dropTargetInfo, BOOL bTargetBackSide, BOOL bAutoAssign, CLayoutObject* pSpreadObject, BOOL bRearrange)
{
	CopyAndPasteFoldSheet(pSourcePrintSheet, nSourceLayerRefIndex, bSourceInnerForm, dropTargetInfo, bTargetBackSide, bAutoAssign, pSpreadObject, bRearrange);

	// We now have to remove the source foldsheet from printsheet and layout
//	if ( ! bAutoAssign)	// _CHECK_CUTANDPASTE_
	if (1)
		pSourcePrintSheet->RemoveFoldSheetLayer(nSourceLayerRefIndex, bAutoAssign, (this == pSourcePrintSheet) || (pSourcePrintSheet->GetLayout()->GetNumPrintSheetsBasedOn() == 1));
	else
	{
		//pSourcePrintSheet->m_FoldSheetLayerRefs.RemoveAt(nSourceLayerRefIndex);
		//if (pSourcePrintSheet->m_FoldSheetLayerRefs.GetSize() == 0)					
		//	CImpManDoc::GetDoc()->m_PrintSheetList.RemoveAt(pSourcePrintSheet);	
	}

	if (GetLayout())
		GetLayout()->AnalyzeMarks();

	CImpManDoc::GetDoc()->m_PageTemplateList.FindPrintSheetLocations();
	CImpManDoc::GetDoc()->m_PageTemplateList.ReorganizePrintSheetPlates();
	CImpManDoc::GetDoc()->m_PrintSheetList.ReorganizeColorInfos();
}


// Copy LayerData taken from pPrintSheet
void CPrintSheet::CopyFoldSheetLayerData(CPrintSheet* pPrintSheet, int nComponentRefIndex)
{
	if ( (nComponentRefIndex < 0) || (nComponentRefIndex >= pPrintSheet->m_FoldSheetLayerRefs.GetSize()) )
		return;

	CFoldSheetLayerRef layerRef	= pPrintSheet->m_FoldSheetLayerRefs[nComponentRefIndex];
	m_FoldSheetLayerRefs.Add(layerRef);

	// TODO: copy layer's exposing data as well
}


// Remove first foldsheet layer of desired type (i.e. n-up layers)
BOOL CPrintSheet::RemoveFoldSheetLayerNUp(int nLayerTypeIndex)
{
	if ( ! m_FoldSheetTypeList.GetSize())
		return FALSE;

	return RemoveFoldSheetLayer(LayerTypeToLayerRef(nLayerTypeIndex));
}

BOOL CPrintSheet::RemoveFoldSheetLayer(int nComponentRefIndex, BOOL bAutoAssign, BOOL bRemoveFromLayout, BOOL bIgnoreLayout, BOOL bDontAsk, CPressDevice* pPressDevice, BOOL& bPrintSheetRemoved, BOOL bRemoveFromBookblock)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if ((nComponentRefIndex < 0) || (nComponentRefIndex >= m_FoldSheetLayerRefs.GetSize()))
		return FALSE;

	int nNum = pDoc->m_PrintSheetList.FoldSheetLayerIsAssigned(m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex, m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex);

	if (bRemoveFromBookblock && (nNum == 1))
	{
		BOOL		bReallyRemove		 = TRUE;
		CFoldSheet* pFoldSheet			 = GetFoldSheet(nComponentRefIndex);
		int			nFoldSheetIndex		 = m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex;
		int			nFoldSheetLayerIndex = m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetLayerIndex;
		for (int i = 0; i < pFoldSheet->m_LayerList.GetSize(); i++)	// really remove only if all other layer if this foldsheet are unassigned 
		{
			if (i != nFoldSheetLayerIndex)
				if (pDoc->m_PrintSheetList.FoldSheetLayerIsAssigned(nFoldSheetIndex, i) >= 1)
					bReallyRemove = FALSE;
		}
		if (bReallyRemove)
		{
			pDoc->m_Bookblock.RemoveFoldSheet(m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex);
			if (pDoc->m_productionData.m_nProductionType == 1)		// coming and going
			{
				int nIndexCGLast  = pDoc->m_Bookblock.m_FoldSheetList.GetCount() - 1;
				int nIndexCGGoing = nIndexCGLast - m_FoldSheetLayerRefs[nComponentRefIndex].m_nFoldSheetIndex; 
				pDoc->m_Bookblock.RemoveFoldSheet(nIndexCGGoing);
			}
		}
	}

	bPrintSheetRemoved = FALSE;

	int nNumBasedOn = (GetLayout()) ? GetLayout()->GetNumPrintSheetsBasedOn() : 0;
	m_FoldSheetLayerRefs.RemoveAt(nComponentRefIndex);
	if (IsEmpty())
	{
		if (nNumBasedOn > 1)	// If printsheet is empty and not the last one of this layout -> remove it
		{
			pDoc->m_PrintSheetList.RemoveAt(this);	// Corresponding layout is removed by RemoveAt() 
			bPrintSheetRemoved = TRUE;				// automatically if not used by any other printsheet
			return TRUE;
		}
		else	// if last printsheet of this layout -> keep this in order to keep printing profile for print group analyzis (clear the layout)
		{
			bIgnoreLayout = FALSE; bRemoveFromLayout = TRUE;
			if (GetLayout())
			{
				GetLayout()->m_markList.RemoveAll();
				GetLayout()->RemoveMarkObjects(-1, TRUE);
			}
		}
	}

	if ( ! bIgnoreLayout)
	{
		if (bRemoveFromLayout)
		{
			if (GetLayout())
				GetLayout()->RemoveFoldSheetLayer(nComponentRefIndex);
			int nLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetSimilarLayout(GetLayout());
			if (nLayoutIndex >= 0)
			{
				POSITION pos = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nLayoutIndex);
				pDoc->m_PrintSheetList.m_Layouts.RemoveAt(pos);

				pos = pDoc->m_printingProfiles.FindIndex(nLayoutIndex);
				pDoc->m_printingProfiles.RemoveAt(pos);
				if (nLayoutIndex < pDoc->m_printGroupList.GetSize())
					pDoc->m_printGroupList.RemoveAt(nLayoutIndex);

				pos = pDoc->m_PrintSheetList.GetHeadPosition();
				while (pos)
				{
					CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
					if (&rPrintSheet == this)
						continue;
					if (rPrintSheet.m_nLayoutIndex == nLayoutIndex)
						rPrintSheet.m_nLayoutIndex = m_nLayoutIndex;
				}
			}
		}
		else
		{
			if (GetLayout())
			{
				if ((GetLayout()->GetNumPrintSheetsBasedOn() > 1) && ! bAutoAssign)
				{
					CLayout layout;
					layout = *GetLayout();
					layout.AssignDefaultName();
					pDoc->m_PrintSheetList.m_Layouts.AddTail(layout);
					int nNewLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;
					int nOldLayoutIndex = m_nLayoutIndex;
					POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
					while (pos)
					{
						CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
						if (&rPrintSheet == this)
							continue;
						if (rPrintSheet.m_nLayoutIndex == nOldLayoutIndex)
							rPrintSheet.m_nLayoutIndex = nNewLayoutIndex;
					}
					pDoc->m_PrintSheetList.ReNumberPrintSheets();

					GetLayout()->RemoveFoldSheetLayer(nComponentRefIndex);
					GetLayout()->m_FrontSide.Invalidate();
					GetLayout()->m_BackSide.Invalidate();
					GetLayout()->m_FrontSide.Analyse();
					GetLayout()->m_BackSide.Analyse();
				}
			}
		}
	}

	return TRUE;
}

BOOL CPrintSheet::RemoveFlatProductType(int nComponentRefIndex, BOOL bAutoAssign, BOOL bRemoveFromLayout, BOOL bIgnoreLayout, BOOL bDontAsk, CPressDevice* pPressDevice, BOOL& bPrintSheetRemoved)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;

	if ((nComponentRefIndex < 0) || (nComponentRefIndex >= m_flatProductRefs.GetSize()))
		return FALSE;

	bPrintSheetRemoved = FALSE;

	int nNumBasedOn = (GetLayout()) ? GetLayout()->GetNumPrintSheetsBasedOn() : 0;
	m_flatProductRefs.RemoveAt(nComponentRefIndex);
	if (IsEmpty())
	{
		if (nNumBasedOn > 1)	// If printsheet is empty and not the last one of this layout -> remove it
		{
			pDoc->m_PrintSheetList.RemoveAt(this);	// Corresponding layout is removed by RemoveAt() 
			bPrintSheetRemoved = TRUE;				// automatically if not used by any other printsheet
			return TRUE;
		}
		else	// if last printsheet of this layout -> keep this in order to keep printing profile for print group analyzis (clear the layout)
		{
			bIgnoreLayout = FALSE; bRemoveFromLayout = TRUE;
			if (GetLayout())
			{
				GetLayout()->m_markList.RemoveAll();
				GetLayout()->RemoveMarkObjects(-1, TRUE);
			}
		}
	}

	if ( ! bIgnoreLayout)
	{
		if (bRemoveFromLayout)
		{
			if (GetLayout())
				GetLayout()->RemoveFlatProductType(nComponentRefIndex);
			int nLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetSimilarLayout(GetLayout());
			if (nLayoutIndex >= 0)
			{
				POSITION pos = pDoc->m_PrintSheetList.m_Layouts.FindIndex(nLayoutIndex);
				pDoc->m_PrintSheetList.m_Layouts.RemoveAt(pos);

				pos = pDoc->m_printingProfiles.FindIndex(nLayoutIndex);
				pDoc->m_printingProfiles.RemoveAt(pos);
				if (nLayoutIndex < pDoc->m_printGroupList.GetSize())
					pDoc->m_printGroupList.RemoveAt(nLayoutIndex);

				pos = pDoc->m_PrintSheetList.GetHeadPosition();
				while (pos)
				{
					CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
					if (&rPrintSheet == this)
						continue;
					if (rPrintSheet.m_nLayoutIndex == nLayoutIndex)
						rPrintSheet.m_nLayoutIndex = m_nLayoutIndex;
				}
			}
		}
		else
		{
			if (GetLayout())
			{
				if ((GetLayout()->GetNumPrintSheetsBasedOn() > 1) && ! bAutoAssign)
				{
					CLayout layout;
					layout = *GetLayout();
					layout.AssignDefaultName();
					pDoc->m_PrintSheetList.m_Layouts.AddTail(layout);
					int nNewLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;
					int nOldLayoutIndex = m_nLayoutIndex;
					POSITION pos = pDoc->m_PrintSheetList.GetHeadPosition();
					while (pos)
					{
						CPrintSheet& rPrintSheet = pDoc->m_PrintSheetList.GetNext(pos);
						if (&rPrintSheet == this)
							continue;
						if (rPrintSheet.m_nLayoutIndex == nOldLayoutIndex)
							rPrintSheet.m_nLayoutIndex = nNewLayoutIndex;
					}
					pDoc->m_PrintSheetList.ReNumberPrintSheets();

					GetLayout()->RemoveFlatProductType(nComponentRefIndex);
					GetLayout()->m_FrontSide.Invalidate();
					GetLayout()->m_BackSide.Invalidate();
					GetLayout()->m_FrontSide.Analyse();
					GetLayout()->m_BackSide.Analyse();
				}
			}
		}
	}

	return TRUE;
}

void CPrintSheet::RemoveFoldSheetLayerAll()
{
	m_FoldSheetLayerRefs.RemoveAll();
	m_FrontSidePlates.RemoveAll();
	m_BackSidePlates.RemoveAll();
	m_FoldSheetTypeList.RemoveAll();
}

int	CPrintSheet::GetPlatesSheetSide(CPlate* pPlate)
{
	POSITION platePos = m_FrontSidePlates.GetHeadPosition();
	while (platePos)
	{
		CPlate& rPlate = m_FrontSidePlates.GetNext(platePos);
		if (&rPlate == pPlate)
			return FRONTSIDE;
	}
	platePos = m_BackSidePlates.GetHeadPosition();
	while (platePos)
	{
		CPlate& rPlate = m_BackSidePlates.GetNext(platePos);
		if (&rPlate == pPlate)
			return BACKSIDE;
	}

	return -1;
}


CPlate*	CPrintSheet::GetTopmostPlate(int nLayoutSide)
{
	CPlateList *pPlateList;
	switch (nLayoutSide)
	{
	case FRONTSIDE: pPlateList = &m_FrontSidePlates; break;
	case BACKSIDE:  pPlateList = &m_BackSidePlates;  break;
	default:		pPlateList = &m_FrontSidePlates; break;
	}

	if ( ! pPlateList->GetCount())
	{
		pPlateList->m_defaultPlate.UpdateExposures();
		return &pPlateList->m_defaultPlate;
	}

	if ((pPlateList->m_nTopmostPlate < 0) || (pPlateList->m_nTopmostPlate >= pPlateList->GetCount()))
		return NULL;

	POSITION pos = pPlateList->FindIndex(pPlateList->m_nTopmostPlate);
	if (!pos)
		return NULL;

	return &pPlateList->GetAt(pos);
}


int	CPrintSheet::GetTopmostPlateIndex(int nLayoutSide)
{
	CPlateList *pPlateList;
	switch (nLayoutSide)
	{
	case FRONTSIDE: pPlateList = &m_FrontSidePlates; break;
	case BACKSIDE:  pPlateList = &m_BackSidePlates;  break;
	default:		pPlateList = &m_FrontSidePlates; break;
	}

	return pPlateList->m_nTopmostPlate;
}

void CPrintSheet::InitializeFoldSheetTypeList()
{
	m_FoldSheetTypeList.RemoveAll();

	int i = 0, j = 0; 
	CArray<BOOL, BOOL> processedList;	// this list holds refs, already taken over in type list - to prevent from being registered multiple times
	for (i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
		processedList.Add(FALSE);

	i = 0; 
	while (i < m_FoldSheetLayerRefs.GetSize())
	{
		CFoldSheetType ft(GetFoldSheet(i), m_FoldSheetLayerRefs[i].m_nFoldSheetLayerIndex, 1);
		processedList[i] = TRUE;

		for (j = i + 1; j < m_FoldSheetLayerRefs.GetSize(); j++)
			if (m_FoldSheetLayerRefs[i] == m_FoldSheetLayerRefs[j])
			{
				ft.m_nNumUp++;
				processedList[j] = TRUE;
			}

		m_FoldSheetTypeList.Add(ft);

		j = i + 1;	// find next different foldsheet 
		while (j < m_FoldSheetLayerRefs.GetSize())
		{
			if (m_FoldSheetLayerRefs[i] != m_FoldSheetLayerRefs[j]) 
				if ( ! processedList[j])
					break;
			j++;
		}

		i = j;
	}

	qsort((void*)m_FoldSheetTypeList.GetData(), m_FoldSheetTypeList.GetSize(), sizeof(CFoldSheetType), CFoldSheetType::Compare);
}

void CPrintSheet::InitializeFlatProductTypeList()
{
	m_flatProductTypeList.RemoveAll();

	int i = 0, j = 0; 
	CArray<BOOL, BOOL> processedList;	// this list holds refs, already taken over in type list - to prevent from being registered multiple times
	for (i = 0; i < m_flatProductRefs.GetSize(); i++)
		processedList.Add(FALSE);

	i = 0;
	while (i < m_flatProductRefs.GetSize())
	{
		CFlatProductType ft(&GetFlatProduct(i), m_flatProductRefs[i].m_nFlatProductIndex, 1);
		processedList[i] = TRUE;

		for (j = i + 1; j < m_flatProductRefs.GetSize(); j++)
			if (m_flatProductRefs[i].m_nFlatProductIndex == m_flatProductRefs[j].m_nFlatProductIndex)
			{
				ft.m_nNumUp++;
				processedList[j] = TRUE;
			}

		m_flatProductTypeList.Add(ft);

		j = i + 1;	// find next different product 
		while (j < m_flatProductRefs.GetSize())
		{
			if (m_flatProductRefs[i].m_nFlatProductIndex != m_flatProductRefs[j].m_nFlatProductIndex) 
				if ( ! processedList[j])
					break;
			j++;
		}

		i = j;
	}

	qsort((void*)m_flatProductTypeList.GetData(), m_flatProductTypeList.GetSize(), sizeof(CFlatProductType), CFlatProductType::Compare);
}

void CPrintSheet::InitializeFlatProductRefsByFormats()
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;

	CArray <CFlatProductRef, CFlatProductRef&> newFlatProductRefs;

	CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
	pLayout->m_FrontSide.m_ObjectList.GetSheetObjectRefList(this, &objectRefList, CLayoutObject::Page, TRUE);

	int nGroupIndex = 0;
	int i, j;
	CArray<BOOL, BOOL> processedList;	// this list holds refs, already taken over in type list - to prevent from being registered multiple times
	for (i = 0; i < objectRefList.GetSize(); i++)
		processedList.Add(FALSE);

	i = 0; 
	while (i < objectRefList.GetSize())
	{
		int nFlatProductIndex = objectRefList[i].m_pLayoutObject->GetCurrentFlatProduct(this).GetIndex();
		CFlatProductRef flatProductRef(nFlatProductIndex, 0);
		newFlatProductRefs.SetAtGrow(objectRefList[i].m_pLayoutObject->m_nComponentRefIndex, flatProductRef);
		objectRefList[i].m_pLayoutObject->m_nGroupIndex = nGroupIndex;

		processedList[i] = TRUE;

		for (j = i + 1; j < objectRefList.GetSize(); j++)
		{
			if ( (objectRefList[i].m_pLayoutObject->GetWidth()  == objectRefList[j].m_pLayoutObject->GetWidth()) && (objectRefList[i].m_pLayoutObject->GetHeight() == objectRefList[j].m_pLayoutObject->GetHeight()) )
			{
				objectRefList[j].m_pLayoutObject->m_nGroupIndex = nGroupIndex;
				newFlatProductRefs.SetAtGrow(objectRefList[j].m_pLayoutObject->m_nComponentRefIndex, flatProductRef);
				processedList[j] = TRUE;
			}
		}

		j = i + 1;	// find next different 
		while (j < objectRefList.GetSize())
		{
			if ( (objectRefList[i].m_pLayoutObject->GetWidth()  != objectRefList[j].m_pLayoutObject->GetWidth()) || (objectRefList[i].m_pLayoutObject->GetHeight() != objectRefList[j].m_pLayoutObject->GetHeight()) )
				if ( ! processedList[j])
					break;
			j++;
		}
		i = j;
		nGroupIndex++;
	}

	m_flatProductRefs.RemoveAll();
	m_flatProductRefs.Append(newFlatProductRefs);

	InitializeFlatProductTypeList();
	pLayout->m_FrontSide.AnalyseObjectGroups(this);
}

int CFoldSheetType::Compare(const void *p1, const void *p2)
{
	if ( ! p1 || ! p2)
		return 0;
	CFoldSheetType* pObject1 = (CFoldSheetType*)p1; CFoldSheetType* pObject2 = (CFoldSheetType*)p2;
	if ( ! pObject1->m_pFoldSheet || ! pObject2->m_pFoldSheet)
		return 0;
	return (pObject1->m_pFoldSheet->m_nFoldSheetNumber < pObject2->m_pFoldSheet->m_nFoldSheetNumber) ? -1 :
		   (pObject1->m_pFoldSheet->m_nFoldSheetNumber > pObject2->m_pFoldSheet->m_nFoldSheetNumber) ?  1 : 
		   (pObject1->m_nFoldSheetLayerIndex		   < pObject2->m_nFoldSheetLayerIndex)			 ? -1 : 
		   (pObject1->m_nFoldSheetLayerIndex		   > pObject2->m_nFoldSheetLayerIndex)			 ?  1 : 0;
}

int CFlatProductType::Compare(const void *p1, const void *p2)
{
	if ( ! p1 || ! p2)
		return 0;
	CFlatProductType* pObject1 = (CFlatProductType*)p1; CFlatProductType* pObject2 = (CFlatProductType*)p2;
	CImpManDoc*		  pDoc	   = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	if ( (pObject1->m_nFlatProductIndex < 0) || (pObject1->m_nFlatProductIndex >= pDoc->m_flatProducts.GetSize()) )
		return 0;
	if ( (pObject2->m_nFlatProductIndex < 0) || (pObject2->m_nFlatProductIndex >= pDoc->m_flatProducts.GetSize()) )
		return 0;
	CFlatProduct& rFlatProduct1 = pDoc->m_flatProducts.FindByIndex(pObject1->m_nFlatProductIndex);
	CFlatProduct& rFlatProduct2 = pDoc->m_flatProducts.FindByIndex(pObject2->m_nFlatProductIndex);
	return (rFlatProduct1.GetIndex() < rFlatProduct2.GetIndex()) ? -1 : 1;
}

BOOL CPrintSheet::IsSuitableLayout(CLayout* pLayout)
{
	if ( (m_FoldSheetLayerRefs.GetSize() <= 0) && (m_flatProductRefs.GetSize() <= 0) )
		return FALSE;

	if (m_FoldSheetLayerRefs.GetSize() > 0)
	{
		if (m_FoldSheetLayerRefs.GetSize() != pLayout->GetNumFoldSheets())
			return FALSE;

		CArray<BOOL, BOOL>	checked;	// holds layout foldsheets, already taken in account
		CObArray			objectPtrList;
		CArray<int, int>	layoutFoldSheetNPagesList;
		for (int i = 0; i < pLayout->m_FoldSheetLayerRotation.GetSize(); i++)
		{
			pLayout->m_FrontSide.GetFoldSheetObjects(i, &objectPtrList);

			layoutFoldSheetNPagesList.Add(objectPtrList.GetSize());
		}

		for (int i = 0; i < layoutFoldSheetNPagesList.GetSize(); i++)
			checked.Add(FALSE);

		for (int i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
		{
			CFoldSheetLayer* pFoldSheetLayer	  = GetFoldSheetLayer(i);
			int				 nPagesFoldSheetLayer = pFoldSheetLayer->m_nPagesX * pFoldSheetLayer->m_nPagesY;

			BOOL bFound = FALSE;
			for (int j = 0; j < layoutFoldSheetNPagesList.GetSize(); j++)
			{
				if ( ! checked[j])
					if (layoutFoldSheetNPagesList[j] == nPagesFoldSheetLayer)
					{
						checked[j] = bFound = TRUE;
						break;
					}
			}
			if (!bFound)
				return FALSE;
		}
	}

	if (m_flatProductRefs.GetSize() > 0)
	{
		if (m_flatProductRefs.GetSize() != pLayout->GetNumFlatProducts())
			return FALSE;
	}

	return TRUE;
}

int CPrintSheet::GetNumOutputObjects(int nSide, CPlate* pPlate, int nObjectType)
{
	if (nSide == -1)	// get num pages of single plate
		return (pPlate) ? pPlate->GetNumOutputObjects(nObjectType) : 0;

	int	nNumPages = 0;
	if ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) )
	{
		POSITION pos = m_FrontSidePlates.GetHeadPosition();
		if ( ! pos)
			if ( ! m_FrontSidePlates.m_defaultPlate.m_bOutputDisabled)
				nNumPages += m_FrontSidePlates.m_defaultPlate.GetNumOutputObjects(nObjectType);
		while (pos)
		{
			CPlate& rPlate = m_FrontSidePlates.GetNext(pos);
			if ( ! rPlate.m_bOutputDisabled)
				nNumPages += rPlate.GetNumOutputObjects(nObjectType);
		}
	}

	if (GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
		return nNumPages;

	if ( (nSide == BACKSIDE) || (nSide == BOTHSIDES) )
	{
		POSITION pos = m_BackSidePlates.GetHeadPosition();
		if ( ! pos)
			if ( ! m_BackSidePlates.m_defaultPlate.m_bOutputDisabled)
				nNumPages += m_BackSidePlates.m_defaultPlate.GetNumOutputObjects(nObjectType);
		while (pos)
		{
			CPlate& rPlate = m_BackSidePlates.GetNext(pos);
			if ( ! rPlate.m_bOutputDisabled)
				nNumPages += rPlate.GetNumOutputObjects(nObjectType);
		}
	}

	return nNumPages;
}

BOOL CPrintSheet::IsOutputDisabled()
{
	POSITION pos = m_FrontSidePlates.GetHeadPosition();
	if ( ! pos)
			if ( ! m_FrontSidePlates.m_defaultPlate.m_bOutputDisabled)
			return FALSE;
	while (pos)
	{
		CPlate& rPlate = m_FrontSidePlates.GetNext(pos);
		if ( ! rPlate.m_bOutputDisabled)
			return FALSE;
	}

	pos = m_BackSidePlates.GetHeadPosition();
	if ( ! pos)
		if ( ! m_BackSidePlates.m_defaultPlate.m_bOutputDisabled)
			return FALSE;
	while (pos)
	{
		CPlate& rPlate = m_BackSidePlates.GetNext(pos);
		if ( ! rPlate.m_bOutputDisabled)
			return FALSE;
	}

	return TRUE;
}

void CPrintSheet::EnableOutput(BOOL bEnable, int nSide)
{
	if ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) )
	{
		POSITION pos = m_FrontSidePlates.GetHeadPosition();
		while (pos)
			m_FrontSidePlates.GetNext(pos).m_bOutputDisabled = ! bEnable;
		m_FrontSidePlates.m_defaultPlate.m_bOutputDisabled = ! bEnable;
	}

	if ( (nSide == BACKSIDE) || (nSide == BOTHSIDES) )
	{
		POSITION pos = m_BackSidePlates.GetHeadPosition();
		while (pos)
			m_BackSidePlates.GetNext(pos).m_bOutputDisabled = ! bEnable;
		m_BackSidePlates.m_defaultPlate.m_bOutputDisabled = ! bEnable;
	}
}

CString CPrintSheet::GetOutFilename(int nSide, CPlate* pPlate)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return "";
	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(m_strPDFTarget);
	if ( ! pTargetProps)
		return "";

	int nOutFileRelation = GetOutfileRelation(pTargetProps->m_strOutputFilename);
	switch (nSide)
	{
	case BOTHSIDES:	if ( (nOutFileRelation != SheetPerFile) && (nOutFileRelation != JobPerFile) )
						return "";
					break;
	case FRONTSIDE: 
	case BACKSIDE:	if (nOutFileRelation != SidePerFile)
						return "";
					break;
	default:		if (nOutFileRelation != PlatePerFile)
						return "";
					nSide = (pPlate) ? pPlate->GetSheetSide() : -1;
					break;
	}

	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)pTargetProps->m_strOutputFilename, this, nSide, pPlate, -1, "", -1, -1);	
	
	return szDest;
}

CString	CPrintSheet::GetOutFileFullPath(int nSide, CPlate* pPlate, BOOL bJDFMarksOutput, BOOL bCreateMissingPath)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return "";

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(m_strPDFTarget);
	if ( ! pTargetProps)
		return "";

	CString strLocation = pTargetProps->m_strLocation;
	if ( ! pTargetProps->m_strLocationBack.IsEmpty() && (nSide == BACKSIDE) )
		strLocation = pTargetProps->m_strLocationBack;

	if (bJDFMarksOutput)
	{
		if (pTargetProps->m_bJDFOutputMime)
			strLocation = theApp.settings.m_szTempDir;
		else
			if (pTargetProps->m_bJDFMarksInFolder && ! pTargetProps->m_strJDFMarksFolder.IsEmpty()) 
			{
				if (PathIsRelative(pTargetProps->m_strJDFMarksFolder))
					strLocation = pTargetProps->m_strLocation + _T("\\") + pTargetProps->m_strJDFMarksFolder;
				else
					strLocation = pTargetProps->m_strJDFMarksFolder;
			}
	}

	CString strOutputPDFFile; 
	if ( (pTargetProps->m_nType == CPDFTargetProperties::TypePDFFolder) || (pTargetProps->m_nType == CPDFTargetProperties::TypeJDFFolder) )
	{
		TCHAR szDest[1024];
		CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)strLocation, this, nSide, pPlate, -1, "", -1, -1);	
		if (bCreateMissingPath)
			if ( ! theApp.DirectoryExist(szDest))
				SHCreateDirectoryEx(NULL, szDest, NULL);
		strOutputPDFFile.Format(_T("%s\\%s"), szDest, GetOutFilename(nSide, pPlate));
	}
	else
		strOutputPDFFile.Format(_T("%s\\%s"), theApp.settings.m_szTempDir, GetOutFilename(nSide, pPlate));

	return strOutputPDFFile;
}

CString	CPrintSheet::GetOutFileTmpFullPath(int nSide, CPlate* pPlate)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return ""; 

	CString strTmpOutputPDFFile; 
	strTmpOutputPDFFile.Format(_T("%s\\%s"), theApp.settings.m_szTempDir, GetOutFilename(nSide, pPlate));

	return strTmpOutputPDFFile;
}

CString CPrintSheet::GetCuttingDataOutFilename()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return "";
	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(m_strPDFTarget);
	if ( ! pTargetProps)
		return "";

	int nOutFileRelation = GetOutfileRelation(pTargetProps->m_strOutputFilename);
	if ( (nOutFileRelation != SheetPerFile) && (nOutFileRelation != JobPerFile) )
		return "";

	TCHAR szDest[1024];
	CPrintSheetView::ResolvePlaceholders(szDest, (LPCTSTR)pTargetProps->m_strCuttingDataFilename, this, BOTHSIDES, NULL, -1, "", -1, -1);	
	
	return szDest;
}

CString	CPrintSheet::GetCuttingDataOutFileFullPath()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return "";

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(m_strPDFTarget);
	if ( ! pTargetProps)
		return "";

	CString strOutputPDFFile; 
	strOutputPDFFile.Format(_T("%s\\%s"), pTargetProps->m_strCuttingDataFolder, GetCuttingDataOutFilename());

	return strOutputPDFFile;
}

CString	CPrintSheet::GetCuttingDataOutFileTmpFullPath()
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return ""; 

	CString strTmpOutputPDFFile; 
	strTmpOutputPDFFile.Format(_T("%s\\%s"), theApp.settings.m_szTempDir, GetCuttingDataOutFilename());

	return strTmpOutputPDFFile;
}

BOOL CPrintSheet::OutFileExists(BOOL bExact, int nSide, CPlate* pPlate)
{
	CFileStatus fileStatus;
	if (CFile::GetStatus(GetOutFileFullPath(nSide, pPlate), fileStatus))	
		return TRUE;	

#ifdef PDF
	if (bExact)
		return FALSE;

	// if no exact file check, look if file exists on a deaper level
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (! pDoc)
		return FALSE;
	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(m_strPDFTarget);
	if ( ! pTargetProps)
		return FALSE;

	CPlateList* pPlateList;
	POSITION	pos		  = NULL;
	int			nRelation = GetOutfileRelation(pTargetProps->m_strOutputFilename);
	switch (nRelation)
	{
	case JobPerFile:	
	case SheetPerFile:	break;	// no 'deaper' files

	case SidePerFile:	if (nSide == BOTHSIDES)
						{
							if (CFile::GetStatus(GetOutFileFullPath(FRONTSIDE), fileStatus))	
								return TRUE;	
							else
								if (CFile::GetStatus(GetOutFileFullPath(BACKSIDE), fileStatus))	
									return TRUE;
						}
						break;

	case PlatePerFile:	switch (nSide)
						{
						case BOTHSIDES:	pos = m_FrontSidePlates.GetHeadPosition();
										while (pos)
										{
											CPlate& rPlate = m_FrontSidePlates.GetNext(pos);
											if (CFile::GetStatus(GetOutFileFullPath(-1, &rPlate), fileStatus))	
												return TRUE;
										}
										pos = m_BackSidePlates.GetHeadPosition();
										while (pos)
										{
											CPlate& rPlate = m_BackSidePlates.GetNext(pos);
											if (CFile::GetStatus(GetOutFileFullPath(-1, &rPlate), fileStatus))	
												return TRUE;
										}
										break;
	
						case FRONTSIDE:
						case BACKSIDE:	pPlateList = (nSide == FRONTSIDE) ? &m_FrontSidePlates : &m_BackSidePlates;
										pos = pPlateList->GetHeadPosition();
										while (pos)
										{
											CPlate& rPlate = pPlateList->GetNext(pos);
											if (CFile::GetStatus(GetOutFileFullPath(-1, &rPlate), fileStatus))	
												return TRUE;
										}
										break;
						}
		
						break;
	}
#endif

	return FALSE;
}

int	CPrintSheet::GetOutfileRelation(const CString& strOutputFilename)
{
	if (strOutputFilename.Find("$2") >= 0)				// $2 = plate color
		return PlatePerFile;
	else
		if (strOutputFilename.Find("$3") >= 0)			// $3 = sheet side
			return SidePerFile;
		else
			if (strOutputFilename.Find("$B") >= 0)		// $B = minimum page number on sheet side
				return SidePerFile;
			else
				if (strOutputFilename.Find("$D") >= 0)		// $D = maximum page number on sheet side
					return SidePerFile;
				else
					if (strOutputFilename.Find("$R") >= 0)		// $R = all page numbers on sheet side
						return SidePerFile;
					else
						if (strOutputFilename.Find("$9") >= 0)	// $9 = sheet number
							return SheetPerFile;
						else
							return JobPerFile;
}

CString	CPrintSheet::GetLastTimestamp(int nSide, CPlate* pPlate)
{
	COleDateTime date, date1, date2;
	switch (nSide)
	{
	case BOTHSIDES:		date1 = m_FrontSidePlates.GetLastOutputTime();
						date2 = m_BackSidePlates.GetLastOutputTime();
						date = max(date1, date2);
						break;

	case FRONTSIDE:		
	case BACKSIDE:		date = (nSide == FRONTSIDE) ? m_FrontSidePlates.GetLastOutputTime() : m_BackSidePlates.GetLastOutputTime();
						break;

	case -1:			date = (pPlate) ? pPlate->m_OutputDate : 0.0;
						break;
	}
	if (date.m_dt != 0.0)
	{
		if (theApp.settings.m_iLanguage == 0)
			return date.Format(_T("%d.%m.%y %H:%M"));
		else
			return date.Format(_T("%m/%d/%y %H:%M"));
	}
	return "";
}

int CPrintSheet::GetNumPages(int nSide)
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return 0;

	int	nNumPages = 0;
	if ( (nSide == ALLSIDES) || (nSide == BOTHSIDES) || (nSide == FRONTSIDE) )
	{
		POSITION pos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(pos);
			nNumPages += ((rObject.m_nType == CLayoutObject::Page) ? 1 : 0);
		}
	}

	if (GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
		return nNumPages;

	if ( (nSide == ALLSIDES) || (nSide == BOTHSIDES) || (nSide == BACKSIDE) )
	{
		POSITION pos = pLayout->m_BackSide.m_ObjectList.GetHeadPosition();
		while (pos)
		{
			CLayoutObject& rObject = pLayout->m_BackSide.m_ObjectList.GetNext(pos);
			nNumPages += ((rObject.m_nType == CLayoutObject::Page) ? 1 : 0);
		}
	}
	return nNumPages;
}

int CPrintSheet::GetNumPagesOccupied(int nSide, CString& rstrMissingPages)
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return 0;

	int	nNumPagesComplete = 0;
	if ( (nSide == ALLSIDES) || (nSide == BOTHSIDES) || (nSide == FRONTSIDE) ) 
	{
		POSITION objectPos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::Page)
			{
				CPageTemplate* pPageTemplate = rObject.GetCurrentPageTemplate(this);
				if ( ! pPageTemplate)	// assume to be single sided -> take as complete
					nNumPagesComplete++;
				else
				{
					POSITION platePos = m_FrontSidePlates.GetHeadPosition();
					while (platePos)
					{
						CPlate& rPlate = m_FrontSidePlates.GetNext(platePos);
						if (rObject.GetExposure(&rPlate))	// page has min. 1 colorant
						{
							nNumPagesComplete++;
							break;
						}
						else
						{
							CString strName;
							if (pPageTemplate->IsFlatProduct())
								strName.Format(_T("    %s"), pPageTemplate->m_strPageID);
							else
							{
								CBoundProduct& rProduct = pPageTemplate->GetBoundProduct();
								strName.Format(_T("    %s ( %s )"), rProduct.m_strProductName, pPageTemplate->m_strPageID);
							}
							rstrMissingPages += strName;
						}
					}
				}
			}
		}
	}

	if (GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
		return nNumPagesComplete;

	if ( (nSide == ALLSIDES) || (nSide == BOTHSIDES) || (nSide == BACKSIDE) )
	{
		POSITION objectPos = pLayout->m_BackSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = pLayout->m_BackSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::Page)
			{
				POSITION platePos = m_BackSidePlates.GetHeadPosition();
				while (platePos)
				{
					CPlate& rPlate = m_BackSidePlates.GetNext(platePos);
					if (rObject.GetExposure(&rPlate))	// page has min. 1 colorant
					{
						nNumPagesComplete++;
						break;
					}
					else
					{
						CPageTemplate* pPageTemplate = rObject.GetCurrentPageTemplate(this);
						if (pPageTemplate)
						{
							CString strName;
							if (pPageTemplate->IsFlatProduct())
								strName.Format(_T("    %s"), pPageTemplate->m_strPageID);
							else
							{
								CBoundProduct& rProduct = pPageTemplate->GetBoundProduct();
								strName.Format(_T("    %s ( %s )"), rProduct.m_strProductName, pPageTemplate->m_strPageID);
							}
							rstrMissingPages += strName;
						}
					}
				}
			}
		}
	}

	return nNumPagesComplete;
}

BOOL CPrintSheet::NotYetOutputted(int nSide)
{
	if ( (nSide == ALLSIDES) || (nSide == BOTHSIDES) || (nSide == FRONTSIDE) )
	{
		POSITION platePos = m_FrontSidePlates.GetHeadPosition();
		while (platePos)
		{
			CPlate& rPlate = m_FrontSidePlates.GetNext(platePos);
			if (rPlate.m_OutputDate.m_dt == 0.0)	
				return TRUE;
		}
	}

	if (GetLayout()->m_nProductType)	// work and turn / work and tumble / flat work
		return FALSE;

	if ( (nSide == ALLSIDES) || (nSide == BOTHSIDES) || (nSide == BACKSIDE) )
	{
		POSITION platePos = m_BackSidePlates.GetHeadPosition();
		while (platePos)
		{
			CPlate& rPlate = m_BackSidePlates.GetNext(platePos);
			if (rPlate.m_OutputDate.m_dt == 0.0)	
				return TRUE;
		}
	}

	return FALSE;
}

BOOL CPrintSheet::ReadyForOutput(int nSide, BOOL bAutoCmd)
{
	if ( ! bAutoCmd)
		return TRUE;
	else
		if (NotYetOutputted(nSide))
		{
			CString strMissingPages;
			int		nNumPages		  = GetNumPages(nSide);
			int		nNumPagesOccupied = GetNumPagesOccupied(nSide, strMissingPages);
			if ((nNumPages == nNumPagesOccupied))
			{
				CImpManDoc* pDoc = CImpManDoc::GetDoc();
				if (pDoc)
					pDoc->m_PrintSheetList.NotifyClients(CPrintSheetList::NOTIFY_START_OUTPUT_SHEET);
				return TRUE;
			}
			else
			{
				CString strMsg, strSheetName;
				if (nSide == FRONTSIDE)
					strSheetName.Format(_T("%s (%s)"), GetNumber(), theApp.settings.m_szFrontName);
				else
					if (nSide == BACKSIDE)
						strSheetName.Format(_T("%s (%s)"), GetNumber(), theApp.settings.m_szBackName);
					else
						strSheetName.Format(_T("%s"), GetNumber());

				AfxFormatString1(strMsg, IDS_SHEET_INCOMPLETE, strSheetName);
				strMsg += _T("    ") + strMissingPages;
				KIMLogMessage(strMsg, MB_ICONWARNING);
				return TRUE;	// FALSE; Formerly we returned FALSE. This was needed by old AutoServer in order to output only when all PDFs received
			}					// The new server always puts out, but gives a message if something is missing
		}
		else
			return FALSE;
}

void CPrintSheet::ResetLastOutputDates(int nSide)
{
	if ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) )
	{
		m_FrontSidePlates.m_defaultPlate.m_OutputDate = 0.0f;
		POSITION platePos = m_FrontSidePlates.GetHeadPosition();
		while (platePos)
		{
			CPlate& rPlate = m_FrontSidePlates.GetNext(platePos);
			rPlate.m_OutputDate.m_dt = 0.0f;
		}
	}

	if ( (nSide == BACKSIDE) || (nSide == BOTHSIDES) )
	{
		m_BackSidePlates.m_defaultPlate.m_OutputDate = 0.0f;
		POSITION platePos = m_BackSidePlates.GetHeadPosition();
		while (platePos)
		{
			CPlate& rPlate = m_BackSidePlates.GetNext(platePos);
			rPlate.m_OutputDate.m_dt = 0.0f;
		}
	}
}

BOOL CPrintSheet::HasPageSource(CPageSource* pPageSource)
{
	POSITION platePos = m_FrontSidePlates.GetHeadPosition();
	while (platePos)
	{
		CPlate&  rPlate		 = m_FrontSidePlates.GetNext(platePos);
		POSITION exposurePos = rPlate.m_ExposureSequence.GetHeadPosition();
		while (exposurePos)
		{
			CExposure& rExposure = rPlate.m_ExposureSequence.GetNext(exposurePos);
			CLayoutObject* pObject = rExposure.GetLayoutObject();
			if ( ! pObject)		return FALSE;
			CPageTemplate* pTemplate = pObject->GetCurrentPageTemplate(this);
			if ( ! pTemplate)	return FALSE;
			if (pPageSource == pTemplate->GetObjectSource(rPlate.m_nColorDefinitionIndex, TRUE, this, rPlate.GetSheetSide()))
				return TRUE;
		}
	}
	platePos = m_BackSidePlates.GetHeadPosition();
	while (platePos)
	{
		CPlate&  rPlate		 = m_BackSidePlates.GetNext(platePos);
		POSITION exposurePos = rPlate.m_ExposureSequence.GetHeadPosition();
		while (exposurePos)
		{
			CExposure& rExposure = rPlate.m_ExposureSequence.GetNext(exposurePos);
			CLayoutObject* pObject = rExposure.GetLayoutObject();
			if ( ! pObject)		return FALSE;
			CPageTemplate* pTemplate = pObject->GetCurrentPageTemplate(this);
			if ( ! pTemplate)	return FALSE;
			if (pPageSource == pTemplate->GetObjectSource(rPlate.m_nColorDefinitionIndex, TRUE, this, rPlate.GetSheetSide()))
				return TRUE;
		}
	}
	return FALSE;
}

COleDateTime CPrintSheet::GetLastOutputDate()
{
	COleDateTime outputDateLast = 0.0f;

	if ( (m_FrontSidePlates.GetSize() <= 0) && (m_BackSidePlates.GetSize() <= 0) )	// if we have no real plates (ie.e unpopulated JDF) use defaults
	{
		outputDateLast = max(m_FrontSidePlates.m_defaultPlate.m_OutputDate, m_BackSidePlates.m_defaultPlate.m_OutputDate);	
		return outputDateLast;
	}

	POSITION pos = m_FrontSidePlates.GetHeadPosition();
	while (pos)
	{
		CPlate& rPlate = m_FrontSidePlates.GetNext(pos);
		if (rPlate.m_OutputDate.m_dt > outputDateLast)
			outputDateLast = rPlate.m_OutputDate.m_dt;
	}

	outputDateLast = (m_BackSidePlates.m_defaultPlate.m_OutputDate > outputDateLast) ? m_BackSidePlates.m_defaultPlate.m_OutputDate : outputDateLast;	
	pos = m_BackSidePlates.GetHeadPosition();
	while (pos)
	{
		CPlate& rPlate = m_BackSidePlates.GetNext(pos);
		if (rPlate.m_OutputDate.m_dt > outputDateLast)
			outputDateLast = rPlate.m_OutputDate.m_dt;
	}

	return outputDateLast;
}

CString	CPrintSheet::GetOutputFormat()
{ 
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return _T("");

	CPDFTargetProperties* pTargetProps = pDoc->m_PrintSheetList.m_pdfTargets.Find(m_strPDFTarget);
	if ( ! pTargetProps)
		return _T("");

	CString strMeasureFormat, strWidth, strHeight;
	strMeasureFormat.Format(_T("%s"), (LPCTSTR)MeasureFormat("%.2f"));
	strWidth.Format((LPCTSTR)strMeasureFormat, MeasureFormat(pTargetProps->m_mediaList[0].GetMediaWidth(this)));

	strMeasureFormat.Format(_T("%s"), (LPCTSTR)MeasureFormat("%.2f"));
	strHeight.Format((LPCTSTR)strMeasureFormat, MeasureFormat(pTargetProps->m_mediaList[0].GetMediaHeight(this)));

	CString strFormat;
	strFormat.Format(_T("%s x %s"), strWidth, strHeight);

	return strFormat;
}

int	CPrintSheet::GetMinPagenum(int nSide, int nProductPartIndex, BOOL bTakeIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return 0;

	int nMinPagenum = INT_MAX;
	if ( (nSide == BOTHSIDES) || (nSide == FRONTSIDE) )
	{
		POSITION objectPos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::Page)
			{
				CPageTemplate* pTemplate = rObject.GetCurrentPageTemplate(this);
				if (pTemplate)
					if (pTemplate->IsPartOfBoundProduct(nProductPartIndex, 7))
						nMinPagenum = min(nMinPagenum, (bTakeIndex) ? (pDoc->m_PageTemplateList.GetIndex(pTemplate, nProductPartIndex) + 1) : _ttoi(pTemplate->m_strPageID));
			}
		}
	}

	if ( (nSide == BOTHSIDES) || (nSide == BACKSIDE) )
	{
		POSITION objectPos = pLayout->m_BackSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = pLayout->m_BackSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::Page)
			{
				CPageTemplate* pTemplate = rObject.GetCurrentPageTemplate(this);
				if (pTemplate)
					if (pTemplate->IsPartOfBoundProduct(nProductPartIndex, 8))
						nMinPagenum = min(nMinPagenum, (bTakeIndex) ? (pDoc->m_PageTemplateList.GetIndex(pTemplate, nProductPartIndex) + 1) : _ttoi(pTemplate->m_strPageID));
			}
		}
	} 

	return (nMinPagenum == INT_MAX) ? 0 : nMinPagenum;
}

int	CPrintSheet::GetMaxPagenum(int nSide, int nProductPartIndex, BOOL bTakeIndex)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return 0;

	int nMaxPagenum = INT_MIN;
	if ( (nSide == BOTHSIDES) || (nSide == FRONTSIDE) )
	{
		POSITION objectPos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::Page)
			{
				CPageTemplate* pTemplate = rObject.GetCurrentPageTemplate(this);
				if (pTemplate)
					if (pTemplate->IsPartOfBoundProduct(nProductPartIndex, 9))
						nMaxPagenum = max(nMaxPagenum, (bTakeIndex) ? (pDoc->m_PageTemplateList.GetIndex(pTemplate, nProductPartIndex) + 1) : _ttoi(pTemplate->m_strPageID));
			}
		}
	}

	if ( (nSide == BOTHSIDES) || (nSide == BACKSIDE) )
	{
		POSITION objectPos = pLayout->m_BackSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = pLayout->m_BackSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::Page)
			{
				CPageTemplate* pTemplate = rObject.GetCurrentPageTemplate(this);
				if (pTemplate)
					if (pTemplate->IsPartOfBoundProduct(nProductPartIndex, 10))
						nMaxPagenum = max(nMaxPagenum, (bTakeIndex) ? (pDoc->m_PageTemplateList.GetIndex(pTemplate, nProductPartIndex) + 1) : _ttoi(pTemplate->m_strPageID));
			}
		}
	} 

	return (nMaxPagenum == INT_MIN) ? 0 : nMaxPagenum;
}

CString	CPrintSheet::GetAllPagenums(int nSide)
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return _T("");

	CArray <int, int> pageNums;

	if ( (nSide == BOTHSIDES) || (nSide == FRONTSIDE) )
	{
		POSITION objectPos = pLayout->m_FrontSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = pLayout->m_FrontSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::Page)
			{
				CPageTemplate* pTemplate = rObject.GetCurrentPageTemplate(this);
				if (pTemplate)
					pageNums.Add(atoi(pTemplate->m_strPageID));
			}
		}
	}

	if ( (nSide == BOTHSIDES) || (nSide == BACKSIDE) )
	{
		POSITION objectPos = pLayout->m_BackSide.m_ObjectList.GetHeadPosition();
		while (objectPos)
		{
			CLayoutObject& rObject = pLayout->m_BackSide.m_ObjectList.GetNext(objectPos);
			if (rObject.m_nType == CLayoutObject::Page)
			{
				CPageTemplate* pTemplate = rObject.GetCurrentPageTemplate(this);
				if (pTemplate)
					pageNums.Add(atoi(pTemplate->m_strPageID));
			}
		}
	} 

	qsort((void*)pageNums.GetData(), pageNums.GetSize(), sizeof(int), ComparePageNums);

	int nDigits = 1;
	for (int i = 0; i < pageNums.GetSize(); i++)
	{
		if ( (pageNums[i] >= 0) && (pageNums[i] <= 9) )
			nDigits = max(nDigits, 1);
		else
			if ( (pageNums[i] >= 10) && (pageNums[i] <= 99) )
				nDigits = max(nDigits, 2);
			else
				if ( (pageNums[i] >= 100) && (pageNums[i] <= 999) )
					nDigits = max(nDigits, 3);
				else
					if ( (pageNums[i] >= 1000) && (pageNums[i] <= 9999) )
						nDigits = max(nDigits, 4);
					else
						nDigits = 5;
	}

	CString strPageNum, strPageNumList, strCtrl;
	strCtrl.Format(_T("%%0%dd"), nDigits);
	for (int i = 0; i < pageNums.GetSize(); i++)
	{
		strPageNum.Format(strCtrl, pageNums[i]);
		strPageNumList += strPageNum;
	}

	return strPageNumList;
}

int CPrintSheet::ComparePageNums(const void *p1, const void *p2)
{
	int	nNum1 = *(int*)p1;
	int	nNum2 = *(int*)p2;

	if (nNum1 < nNum2)
		return -1;
	else
		if (nNum1 > nNum2)
			return 1;
		else
			return 0;
}

CString	CPrintSheet::GetNumber()		
{ 
	return CDlgNumeration::GenerateNumber(m_nPrintSheetNumber, m_strNumerationStyle, m_nNumerationOffset); 
};

BOOL CPrintSheet::ContainsPrintColor(int nColorDefinitionIndex, int nSide, CString strSeparation)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return FALSE;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return FALSE;
	if ( (nColorDefinitionIndex < 0) || (nColorDefinitionIndex >= pDoc->m_ColorDefinitionTable.GetSize()) )
		return FALSE;

	BOOL bContainsSeparated	  = FALSE;
	BOOL bContainsUnseparated = FALSE;
	CString strColorName = pDoc->m_ColorDefinitionTable[nColorDefinitionIndex].m_strColorName;

	if (pDoc->m_PageTemplateList.IsUnpopulated())
	{
		if ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) )
		{
			CPlate& rPlate = m_FrontSidePlates.m_defaultPlate;
			if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
			{
				if (strColorName.CompareNoCase(_T("Composite")) == 0)
					bContainsUnseparated = TRUE;
				else
					bContainsSeparated = TRUE;
			}
			else
				if (rPlate.m_strPlateName.CompareNoCase(_T("Composite")) == 0)
				{
					for (int i = 0; i < rPlate.m_spotColors.GetSize(); i++)
						if (rPlate.m_spotColors[i] == strColorName)
						{
							bContainsUnseparated = TRUE;
							break;
						}
				}
		}
		if ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) )
		{
			CPlate& rPlate = m_BackSidePlates.m_defaultPlate;
			if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
			{
				if (strColorName.CompareNoCase(_T("Composite")) == 0)
					bContainsUnseparated = TRUE;
				else
					bContainsSeparated = TRUE;
			}
			else
				if (rPlate.m_strPlateName.CompareNoCase(_T("Composite")) == 0)
				{
					for (int i = 0; i < rPlate.m_spotColors.GetSize(); i++)
						if (rPlate.m_spotColors[i] == strColorName)
						{
							bContainsUnseparated = TRUE;
							break;
						}
				}
		}
	}
	else
	{
		switch (nSide)
		{
		case FRONTSIDE: if ( ! m_FrontSidePlates.GetCount())									return FALSE;
						break;
		case BACKSIDE:	if ( ! m_BackSidePlates.GetCount())										return FALSE;
						break;
		case BOTHSIDES: if ( ! m_FrontSidePlates.GetCount() && ! m_BackSidePlates.GetCount())	return FALSE;
						break;
		}

		if ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) )
		{
			POSITION pos = m_FrontSidePlates.GetHeadPosition();
			while (pos)
			{
				CPlate& rPlate = m_FrontSidePlates.GetNext(pos);
				if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
				{
					if (strColorName.CompareNoCase(_T("Composite")) == 0)
					{
						if (strSeparation.IsEmpty())
							bContainsUnseparated = TRUE;
						else
						{
							for (int i = 0; i < rPlate.m_processColors.GetSize(); i++)
								if (rPlate.m_processColors[i] == strSeparation)
								{
									bContainsUnseparated = TRUE;
									break;
								}
						}
					}
					else
						bContainsSeparated = TRUE;
				}
				else
					if (rPlate.m_strPlateName.CompareNoCase(_T("Composite")) == 0)
					{
						for (int i = 0; i < rPlate.m_spotColors.GetSize(); i++)
							if (rPlate.m_spotColors[i] == strColorName)
							{
								bContainsUnseparated = TRUE;
								break;
							}
					}
			}
		}

		if ( (nSide == BACKSIDE) || (nSide == BOTHSIDES) )
		{
			POSITION pos = m_BackSidePlates.GetHeadPosition();
			while (pos)
			{
				CPlate& rPlate = m_BackSidePlates.GetNext(pos);
				if (rPlate.m_nColorDefinitionIndex == nColorDefinitionIndex)
				{
					if (strColorName.CompareNoCase(_T("Composite")) == 0)
					{
						if (strSeparation.IsEmpty())
							bContainsUnseparated = TRUE;
						else
						{
							for (int i = 0; i < rPlate.m_processColors.GetSize(); i++)
								if (rPlate.m_processColors[i] == strSeparation)
								{
									bContainsUnseparated = TRUE;
									break;
								}
						}
					}
					else
						bContainsSeparated = TRUE;
				}
				else
					if (rPlate.m_strPlateName.CompareNoCase(_T("Composite")) == 0)
					{
						for (int i = 0; i < rPlate.m_spotColors.GetSize(); i++)
							if (rPlate.m_spotColors[i] == strColorName)
							{
								bContainsUnseparated = TRUE;
								break;
							}
					}
			}
		}
	}

	if (bContainsSeparated && bContainsUnseparated)
		return CColorDefinition::Mixed;
	else
		if (bContainsSeparated)
			return CColorDefinition::Separated;
		else
			if (bContainsUnseparated)
				return CColorDefinition::Unseparated;
			else
				return FALSE;
}

int	CPrintSheet::GetSpotColor(int nSide, int nSpotColorNum)
{
	if (nSpotColorNum < 1)
		return -1;
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return -1;

	int nCurSpotColorNum = 1;
	if ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) )
	{
		POSITION pos = m_FrontSidePlates.GetHeadPosition();
		while (pos)
		{
			CPlate&	rPlate = m_FrontSidePlates.GetNext(pos);
			if (rPlate.m_strPlateName != _T("Composite"))
			{
				CColorDefinition* pColorDef  = theApp.m_colorDefTable.FindColorDefinition(rPlate.m_strPlateName);
				BOOL			  bSpotColor = (pColorDef) ? (pColorDef->m_nColorType == CColorDefinition::Spot) : TRUE;	// if color not registered
				if (bSpotColor)																								// assume color is spotcolor
				{
					if (nCurSpotColorNum == nSpotColorNum)
						return pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(rPlate.m_strPlateName);
					nCurSpotColorNum++;
				}
			}
			else
				if (nSpotColorNum <= rPlate.m_spotColors.GetSize())
					return pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(rPlate.m_spotColors[nSpotColorNum - 1]);
		}
	}

	if ( (nSide == BACKSIDE) || (nSide == BOTHSIDES) )
	{
		POSITION pos = m_BackSidePlates.GetHeadPosition();
		while (pos)
		{
			CPlate&	rPlate = m_BackSidePlates.GetNext(pos);
			if (rPlate.m_strPlateName != _T("Composite"))
			{
				CColorDefinition* pColorDef  = theApp.m_colorDefTable.FindColorDefinition(rPlate.m_strPlateName);
				BOOL			  bSpotColor = (pColorDef) ? (pColorDef->m_nColorType == CColorDefinition::Spot) : TRUE;	// if color not registered
				if (bSpotColor)																								// assume color is spotcolor
				{
					if (nCurSpotColorNum == nSpotColorNum)
						return pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(rPlate.m_strPlateName);
					nCurSpotColorNum++;
				}
			}
			else
				if (nSpotColorNum <= rPlate.m_spotColors.GetSize())
					return pDoc->m_ColorDefinitionTable.FindColorDefinitionIndex(rPlate.m_spotColors[nSpotColorNum - 1]);
		}
	}

	return -1;
}

int	CPrintSheet::GetNumColors(int nSide, BOOL bSpotsOnly)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;

	//CPrintGroup* pProductGroup = pDoc->m_printGroupList.GetPart(GetPrintGroupIndex());
	//if (pProductGroup)
	//{
	//	if (pProductGroup->m_colorantList.GetCount() > 0)
	//		if (HasObjects())
	//		{
	//			if (bSpotsOnly)
	//			{
	//				int nNumColors = 0;
	//				for (int i = 0; i < pProductGroup->m_colorantList.GetCount(); i++)
	//				{
	//					CColorDefinition* pColorDef  = theApp.m_colorDefTable.FindColorDefinition(pProductGroup->m_colorantList[i].m_strName);
	//					BOOL			  bSpotColor = (pColorDef) ? (pColorDef->m_nColorType == CColorDefinition::Spot) : TRUE;	// if color not registered
	//					if (bSpotColor)																								// assume color is spotcolor
	//						nNumColors++;
	//				}
	//				return nNumColors;
	//			}
	//			else
	//				return pProductGroup->m_colorantList.GetCount();
	//		}
	//		else
	//			return 0;
	//}

	CPlateList* pPlateList = ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) ) ? &m_FrontSidePlates : &m_BackSidePlates;

	int nNumColorsFront = 0;
	if ( ! pPlateList->GetCount())
		nNumColorsFront = pPlateList->m_defaultPlate.GetNumColors(bSpotsOnly);
	else
	{
		POSITION pos = pPlateList->GetHeadPosition();
		while (pos)
		{
			CPlate&	rPlate = pPlateList->GetNext(pos);
			nNumColorsFront = max(nNumColorsFront, rPlate.GetNumColors(bSpotsOnly));
		}
	}

	int nNumColorsBack = 0;
	if (nSide == BOTHSIDES)
	{
		pPlateList = &m_BackSidePlates;
		if ( ! pPlateList->GetCount())
			nNumColorsBack = pPlateList->m_defaultPlate.GetNumColors(bSpotsOnly);
		else
		{
			POSITION pos = pPlateList->GetHeadPosition();
			while (pos)
			{
				CPlate&	rPlate = pPlateList->GetNext(pos);
				nNumColorsBack = max(nNumColorsBack, rPlate.GetNumColors(bSpotsOnly));
			}
		}
	}
	return max(nNumColorsFront, nNumColorsBack);
}

void CPrintSheet::GetProcessColors(int nSide, CStringArray& plateNames)
{
	CColorantList colorants;
	if ( (nSide == FRONTSIDE) || (nSide == BOTHSIDES) )
	{
		colorants.AddSeparations(m_FrontSidePlates.m_defaultPlate.m_processColors);
		POSITION pos = m_FrontSidePlates.GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = m_FrontSidePlates.GetNext(pos);
			colorants.AddSeparations(rPlate.m_processColors);
		}
	}
	if ( (nSide == BACKSIDE) || (nSide == BOTHSIDES) )
	{
		colorants.AddSeparations(m_BackSidePlates.m_defaultPlate.m_processColors);
		POSITION pos = m_BackSidePlates.GetHeadPosition();
		while (pos)
		{
			CPlate& rPlate = m_BackSidePlates.GetNext(pos);
			colorants.AddSeparations(rPlate.m_processColors);
		}
	}

	plateNames.RemoveAll();
	for (int i = 0; i < colorants.GetSize(); i++)
		plateNames.Add(colorants[i].m_strName);
}

void CPrintSheet::SubstituteFoldSheetIndex(CFoldSheet& rNewFoldSheet, int nNewFoldSheetIndex, int nLayerIndex)
{
	for (int i = 0; i < m_FoldSheetLayerRefs.GetSize(); i++)
	{
		if (m_FoldSheetLayerRefs[i].m_nFoldSheetIndex == -1)	// no foldsheet assigned yet -> do assign new one
			m_FoldSheetLayerRefs[i].m_nFoldSheetIndex = nNewFoldSheetIndex;
		else
		{
			CFoldSheet* pFoldSheet = GetFoldSheet(i);
			if ( ! pFoldSheet)
				continue;
			if (pFoldSheet->m_strFoldSheetTypeName == rNewFoldSheet.m_strFoldSheetTypeName)
				if (pFoldSheet->m_nProductPartIndex == rNewFoldSheet.m_nProductPartIndex)
					if (m_FoldSheetLayerRefs[i].m_nFoldSheetLayerIndex == nLayerIndex)
						m_FoldSheetLayerRefs[i].m_nFoldSheetIndex = nNewFoldSheetIndex;
		}
	}
}

BOOL CPrintSheet::FindFreePosition(float fObjectWidth, float fObjectHeight, float& fLeft, float& fBottom, int* pnNumObjectsX, int* pnNumObjectsY, int* pnTotalNUp, CPunchingTool* pPunchTool, float fPercentTolerance)
{
	if (pnNumObjectsX && pnNumObjectsY)
	{
		*pnNumObjectsX = 0; *pnNumObjectsY = 0;
	}
	if (pnTotalNUp)
		*pnTotalNUp = 0;

	fLeft = fBottom = 0.0f;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return FALSE;

	float fRight = pLayout->m_FrontSide.m_fPaperRight;
	float fTop	 = pLayout->m_FrontSide.m_fPaperTop;
	//pLayout->m_FrontSide.m_fPaperRight *= (100.0f + fPercentTolerance)/100.0f;
	//pLayout->m_FrontSide.m_fPaperTop   *= (100.0f + fPercentTolerance)/100.0f;

	pLayout->m_FrontSide.FindFreeAreas(this);

	pLayout->m_FrontSide.m_fPaperRight = fRight;
	pLayout->m_FrontSide.m_fPaperTop   = fTop;

	return FindFreePos(fLeft, fBottom, fObjectWidth, fObjectHeight, pnNumObjectsX, pnNumObjectsY, pnTotalNUp, pPunchTool);
}

BOOL CPrintSheet::FindFreePos(float& fLeft, float& fBottom, float fObjectWidth, float fObjectHeight, int* pnNumX, int* pnNumY, int* pnTotalNUp, CPunchingTool* pPunchTool)
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return FALSE;

	long nWidth  = (long)(fObjectWidth  * 1000.0f);
	long nHeight = (long)(fObjectHeight * 1000.0f);
	if ( (nWidth == 0) || (nHeight == 0) )
		return FALSE;

	int	  nIndexFit = -1;
	long  nMinLeft  = LONG_MAX;
	int	  nNumX		= 0, nNumY	  = 0;
	int	  nMAX		= 0, nNumXMax = 0, nNumYMax = 0, nTotal = 0, nTotalRotate = 0;

	for (int i = 0; i < pLayout->m_FrontSide.m_freeAreas.GetSize(); i++)
	{
		CRect& rcFreeArea = pLayout->m_FrontSide.m_freeAreas[i];

		if ((float)rcFreeArea.top / 1000.0f < pLayout->m_FrontSide.m_fPaperBottom)
			rcFreeArea.top = (long)(pLayout->m_FrontSide.m_fPaperBottom * 1000);
		if ( (nWidth <= rcFreeArea.Width()) && (nHeight <= rcFreeArea.Height()) )
		{
			if (pPunchTool)
			{
				if ( ! pPunchTool->IsNull())
				{
					nNumX = pPunchTool->CalcMaxNUpX((float)rcFreeArea.Width()  / 1000.0f, fObjectWidth);
					nNumY = pPunchTool->CalcMaxNUpY((float)rcFreeArea.Height() / 1000.0f, fObjectHeight);
				}
				else
				{
					nNumX = rcFreeArea.Width() / nWidth; nNumY = rcFreeArea.Height() / nHeight;
				}
			}
			else
			{
				nNumX = rcFreeArea.Width() / nWidth; nNumY = rcFreeArea.Height() / nHeight;
			}

			if (rcFreeArea.left < nMinLeft)
			{
				nIndexFit = i; nNumXMax = nNumX; nNumYMax = nNumY; nMinLeft = rcFreeArea.left; 
			}
			nTotal += nNumX * nNumY;
		}
	}

	int nRet = 1;

	if (nIndexFit >= 0)
	{
		float fBlockWidth  = fObjectWidth  * nNumXMax;
		float fBlockHeight = fObjectHeight * nNumYMax;
		if (pPunchTool)
		{
			if ( ! pPunchTool->IsNull())
			{
				fBlockWidth  = pPunchTool->CalcBoxWidth (fObjectWidth,  nNumXMax);
				fBlockHeight = pPunchTool->CalcBoxHeight(fObjectHeight, nNumYMax);
			}
		}
		CRect& rcFreeArea = pLayout->m_FrontSide.m_freeAreas[nIndexFit];
		fLeft	= (float)rcFreeArea.left / 1000.0f;
		fBottom = (float)rcFreeArea.top  / 1000.0f;
		float fRight = fLeft   + fBlockWidth;
		float fTop	 = fBottom + fBlockHeight;

		if (pPunchTool)
		{
			if ( ! pPunchTool->IsNull())
			{
				fLeft   += pPunchTool->m_fGutterX;
				fBottom += pPunchTool->m_fGutterY;
			}
		}

		CLayoutObject* pClosestObject = FindClosestLayoutObject(FRONTSIDE, fLeft, fBottom, fRight, fTop, LEFT);
		if (pClosestObject)
			fBottom = max(pClosestObject->GetBottom() - pClosestObject->GetCurrentGrossMargin(this, BOTTOM), fBottom);
		else
		if (pClosestObject = FindClosestLayoutObject(FRONTSIDE, fLeft, fBottom, fRight, fTop, RIGHT))
		{
			fLeft	= (float)rcFreeArea.right / 1000.0f - fBlockWidth;
			fBottom = max(pClosestObject->GetBottom() - pClosestObject->GetCurrentGrossMargin(this, BOTTOM), fBottom);
		}
		else
		if (pClosestObject = FindClosestLayoutObject(FRONTSIDE, fLeft, fBottom, fRight, fTop, BOTTOM))
		{
			fLeft = pClosestObject->GetLeft() - pClosestObject->GetCurrentGrossMargin(this, LEFT);
		}
		else
		if (pClosestObject = FindClosestLayoutObject(FRONTSIDE, fLeft, fBottom, fRight, fTop, TOP))
		{
			fLeft	= pClosestObject->GetLeft()	  - pClosestObject->GetCurrentGrossMargin(this, LEFT);
			fBottom = pClosestObject->GetBottom() - pClosestObject->GetCurrentGrossMargin(this, BOTTOM) - fBlockHeight;
		}

		fTop = fBottom + fBlockHeight;		// calc again, because fBottom has changed
		if (fTop > (float)rcFreeArea.bottom / 1000.0f)
		{
			fTop = (float)rcFreeArea.bottom / 1000.0f;
			fBottom = fTop - fBlockHeight;
		}
	}
	else
	{
		float fLargestLeft, fLargestBottom, fLargestRight, fLargestTop;
		pLayout->m_FrontSide.FindLargestFreeArea(this, fLargestLeft, fLargestBottom, fLargestRight, fLargestTop);
		fLeft   = fLargestLeft;
		fBottom = max(fLargestBottom, pLayout->m_FrontSide.m_fPaperBottom);
		nRet = -1;
	}

	if (pnNumX && pnNumY)
	{
		*pnNumX = nNumXMax; *pnNumY = nNumYMax;
	}

	if (pnTotalNUp)
		*pnTotalNUp = max(nTotal, nTotalRotate);

	return nRet;
}

BOOL CPrintSheet::FindFreePosition(CFoldSheet* pFoldSheet, int nLayerIndex, float& fLeft, float& fBottom, int* pnNumFoldSheetsX, int* pnNumFoldSheetsY, int* pnTotalNUp, BOOL bDontRotate)
{
	if ( ! pFoldSheet)
		return FALSE;

	if (pnNumFoldSheetsX && pnNumFoldSheetsY)
	{
		*pnNumFoldSheetsX = 0; *pnNumFoldSheetsY = 0;
	}
	if (pnTotalNUp)
		*pnTotalNUp = 0;

	fLeft = fBottom = 0.0f;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return FALSE;

	pLayout->m_FrontSide.FindFreeAreas(this);

	float fFoldSheetCutWidth, fFoldSheetCutHeight; 
	pFoldSheet->GetCutSize(nLayerIndex, fFoldSheetCutWidth, fFoldSheetCutHeight);

	float fMinBottom = pLayout->m_FrontSide.m_fPaperBottom;
	CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
	if (pPressDevice)
		if (pPressDevice->m_nPressType == CPressDevice::SheetPress)
		{
			fMinBottom = pPressDevice->m_fPlateEdgeToPrintEdge - pFoldSheet->GetCutMargin(nLayerIndex, BOTTOM);
		}


	BOOL  bRotate = FALSE;
	int   nNX0 = 0;
	int   nNY0 = 0;
	int   nNX90 = 0;
	int   nNY90 = 0;
	int   nNTotal = 0;
	FindFreePos(fLeft, fBottom, fMinBottom, fFoldSheetCutWidth, fFoldSheetCutHeight, &nNX0, &nNY0, NULL, TRUE);
	int   nNumRotate0 = nNX0 * nNY0;

	pLayout->m_FrontSide.FindFreeAreas(this);

	if ( ( ! bDontRotate) || (bDontRotate == 2) )
		FindFreePos(fLeft, fBottom, fMinBottom, fFoldSheetCutWidth, fFoldSheetCutHeight, &nNX90, &nNY90, NULL, FALSE);
	
	int nNumRotate90 = nNX90 * nNY90;

	if ((nNumRotate0 < nNumRotate90) || (bDontRotate == 2) )	// ==2 means: force rotation
	{
		if (pnNumFoldSheetsX)
			*pnNumFoldSheetsX = nNX90; 
		if (pnNumFoldSheetsY)
			*pnNumFoldSheetsY = nNY90;
		if (pnTotalNUp)
			*pnTotalNUp = nNumRotate90;
		return TRUE;
	}
	else
	{
		if (pnNumFoldSheetsX)
			*pnNumFoldSheetsX = nNX0; 
		if (pnNumFoldSheetsY)
			*pnNumFoldSheetsY = nNY0;
		if (pnTotalNUp)
			*pnTotalNUp = nNumRotate0;
		return FALSE;
	}
}

BOOL CPrintSheet::FindFreePos(float& fLeft, float& fBottom, float fMinBottom, float fObjectWidth, float fObjectHeight, int* pnNumX, int* pnNumY, int* pnTotalNUp, BOOL bDontRotate, CPoint ptClicked)
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return FALSE;

	BOOL bRotate = ! bDontRotate;
	long nWidth  = ( ! bRotate) ? ((long)(fObjectWidth  * 1000.0f)) : ((long)(fObjectHeight * 1000.0f));
	long nHeight = ( ! bRotate) ? ((long)(fObjectHeight * 1000.0f)) : ((long)(fObjectWidth  * 1000.0f));
	if ( (nWidth == 0) || (nHeight == 0) )
		return bRotate;

	int	  nIndexFit = -1;
	long  nMinLeft  = LONG_MAX;
	int	  nNumX		= 0, nNumY	  = 0;
	int	  nMAX		= 0, nNumXMax = 0, nNumYMax = 0, nTotal = 0, nTotalRotate = 0;

	for (int i = 0; i < pLayout->m_FrontSide.m_freeAreas.GetSize(); i++)
	{
		CRect& rcFreeArea = pLayout->m_FrontSide.m_freeAreas[i];
		if (ptClicked != CPoint(0,0))
			if ( ! rcFreeArea.PtInRect(ptClicked))
				continue;

		if ((float)rcFreeArea.top / 1000.0f < fMinBottom)
			rcFreeArea.top = (long)(fMinBottom * 1000);
		if ( (nWidth <= rcFreeArea.Width()) && (nHeight <= rcFreeArea.Height()) )
		{
			nNumX = rcFreeArea.Width() / nWidth; nNumY = rcFreeArea.Height() / nHeight;
			if (rcFreeArea.left < nMinLeft)
			{
				nIndexFit = i; nNumXMax = nNumX; nNumYMax = nNumY; nMinLeft = rcFreeArea.left; 
			}
			nTotal += nNumX * nNumY;
		}
	}

	if (nIndexFit >= 0)
	{
		float fBlockWidth  = ((bRotate) ? fObjectHeight : fObjectWidth)  * nNumXMax;
		float fBlockHeight = ((bRotate) ? fObjectWidth  : fObjectHeight) * nNumYMax;
		CRect& rcFreeArea = pLayout->m_FrontSide.m_freeAreas[nIndexFit];
		fLeft	= (float)rcFreeArea.left / 1000.0f;
		fBottom = (float)rcFreeArea.top  / 1000.0f;
		float fRight = fLeft   + fBlockWidth;
		float fTop	 = fBottom + fBlockHeight;

		CLayoutObject* pClosestObject = FindClosestLayoutObject(FRONTSIDE, fLeft, fBottom, fRight, fTop, LEFT);
		if (pClosestObject)
			fBottom = max(pClosestObject->GetBottom() - pClosestObject->GetCurrentGrossMargin(this, BOTTOM), fBottom);
		else
		if (pClosestObject = FindClosestLayoutObject(FRONTSIDE, fLeft, fBottom, fRight, fTop, RIGHT))
		{
			fLeft	= (float)rcFreeArea.right / 1000.0f - fBlockWidth;
			fBottom = max(pClosestObject->GetBottom() - pClosestObject->GetCurrentGrossMargin(this, BOTTOM), fBottom);
		}
		else
		if (pClosestObject = FindClosestLayoutObject(FRONTSIDE, fLeft, fBottom, fRight, fTop, BOTTOM))
		{
			fLeft = pClosestObject->GetLeft() - pClosestObject->GetCurrentGrossMargin(this, LEFT);
		}
		else
		if (pClosestObject = FindClosestLayoutObject(FRONTSIDE, fLeft, fBottom, fRight, fTop, TOP))
		{
			fLeft	= pClosestObject->GetLeft()	  - pClosestObject->GetCurrentGrossMargin(this, LEFT);
			fBottom = pClosestObject->GetBottom() - pClosestObject->GetCurrentGrossMargin(this, BOTTOM) - fBlockHeight;
		}

		fTop = fBottom + fBlockHeight;		// calc again, because fBottom has changed
		if (fTop > (float)rcFreeArea.bottom / 1000.0f)
		{
			fTop = (float)rcFreeArea.bottom / 1000.0f;
			fBottom = fTop - fBlockHeight;
		}
	}
	else
	{
		float fLargestLeft, fLargestBottom, fLargestRight, fLargestTop;
		pLayout->m_FrontSide.FindLargestFreeArea(this, fLargestLeft, fLargestBottom, fLargestRight, fLargestTop);
		fLeft   = fLargestLeft;
		fBottom = max(fLargestBottom, fMinBottom);
		bRotate = -1;
	}

	if (pnNumX && pnNumY)
	{
		*pnNumX = nNumXMax; *pnNumY = nNumYMax;
	}

	if (pnTotalNUp)
		*pnTotalNUp = max(nTotal, nTotalRotate);

	return bRotate;
}

CLayoutObject* CPrintSheet::FindClosestLayoutObject(int nLayoutSide, float fLeft, float fBottom, float fRight, float fTop, int nDirection)
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return NULL;
	CLayoutSide& rLayoutSide = (nLayoutSide == FRONTSIDE) ? pLayout->m_FrontSide : pLayout->m_BackSide;

	float		   fClosestEdge	  = FLT_MAX;
	CLayoutObject* pClosestObject = NULL;
	POSITION	   pos			  = rLayoutSide.m_ObjectList.GetHeadPosition();
	while (pos)
	{
		CLayoutObject& rObject = rLayoutSide.m_ObjectList.GetNext(pos);
		if (rObject.m_nType == CLayoutObject::ControlMark)
			continue;

		switch (nDirection)
		{
		case LEFT:		if ( (rObject.GetRight() > fLeft) || (rObject.GetBottom() > fTop) || (rObject.GetTop() < fBottom) )
							continue;
						else
							if (fabs(rObject.GetRight() - fLeft) < fClosestEdge)
							{
								fClosestEdge = fabs(rObject.GetRight() - fLeft); pClosestObject = &rObject;
							}
						break;
		case RIGHT:		if ( (rObject.GetLeft() < fRight) || (rObject.GetBottom() > fTop) || (rObject.GetTop() < fBottom) )
							continue;
						else
							if (fabs(rObject.GetLeft() - fRight) < fClosestEdge)
							{
								fClosestEdge = fabs(rObject.GetLeft() - fRight); pClosestObject = &rObject;
							}
						break;
		case BOTTOM:	if ( (rObject.GetTop() > fBottom) || (rObject.GetLeft() > fRight) || (rObject.GetRight() < fLeft) )
							continue;
						else
							if (fabs(rObject.GetTop() - fBottom) < fClosestEdge)
							{
								fClosestEdge = fabs(rObject.GetTop() - fBottom); pClosestObject = &rObject;
							}
						break;
		case TOP:		if ( (rObject.GetBottom() < fTop) || (rObject.GetLeft() > fRight) || (rObject.GetRight() < fLeft) )
							continue;
						else
							if (fabs(rObject.GetBottom() - fTop) < fClosestEdge)
							{
								fClosestEdge = fabs(rObject.GetBottom() - fTop); pClosestObject = &rObject;
							}
						break;
		}
	}

	return pClosestObject;
}

void CPrintSheet::RearrangeLayout(CFoldSheet* pOldFoldSheet, CFoldSheet* pNewFoldSheet, CPrintSheet& newPrintSheet, CLayout& newLayout, int nNUp)
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return;
	if ( ! pNewFoldSheet)
		pNewFoldSheet = pOldFoldSheet;
	CPressDevice* pPressDevice = pLayout->m_FrontSide.GetPressDevice();
	if ( ! pNewFoldSheet || ! pPressDevice)
		return;

	newLayout	   = *pLayout;	
	newPrintSheet  = *this;		
	newPrintSheet.LinkLayout(&newLayout);

	if (pOldFoldSheet)
	{
		for (int i = 0; i < newPrintSheet.m_FoldSheetLayerRefs.GetSize(); i++)
		{
			if (newPrintSheet.GetFoldSheet(i) == pOldFoldSheet)
			{
				newLayout.RemoveFoldSheetLayer(i);
				newPrintSheet.m_FoldSheetLayerRefs.RemoveAt(i);
				i--;
			}
		}
	}
	else
	{
		newLayout.RemoveFoldSheetLayerAll();
		newPrintSheet.RemoveFoldSheetLayerAll();
	}

	int   nLayerIndex = 0;
	float fXPos, fYPos;
	int	  nNumFoldSheetsX, nNumFoldSheetsY;
	BOOL bRotate = newPrintSheet.FindFreePosition(pNewFoldSheet, nLayerIndex, fXPos, fYPos, &nNumFoldSheetsX, &nNumFoldSheetsY);
	if (bRotate == -1)
		if (KIMOpenLogMessage(_T("Der gew�hlte Falzbogen passt nicht auf diesen Druckbogen!\n\nTrotzdem platzieren?"), MB_YESNO, IDYES) == IDNO)
			return;
		else
		{
			bRotate = FALSE; nNumFoldSheetsX = nNumFoldSheetsY = 1;
		}

	float fFoldSheetCutWidth, fFoldSheetCutHeight; 
	pNewFoldSheet->GetCutSize(nLayerIndex, fFoldSheetCutWidth, fFoldSheetCutHeight);
	if (bRotate) { float fTemp = fFoldSheetCutWidth; fFoldSheetCutWidth = fFoldSheetCutHeight; fFoldSheetCutHeight = fTemp; }
	float fFoldSheetPosX = fXPos;
	float fFoldSheetPosY = fYPos;
	for (int ny = 0; (ny < nNumFoldSheetsY) && (nNUp > 0); ny++)
	{
		fFoldSheetPosX = fXPos;
		for (int nx = 0; (nx < nNumFoldSheetsX) && (nNUp > 0); nx++)
		{
			newLayout.AddFoldSheetLayer(pNewFoldSheet, nLayerIndex, fFoldSheetPosX, fFoldSheetPosY, pNewFoldSheet->GetProductPart(), (bRotate) ? ROTATE90 : ROTATE0);
			fFoldSheetPosX += fFoldSheetCutWidth;

			CFoldSheetLayerRef layerRef(-1, 0);
			newPrintSheet.m_FoldSheetLayerRefs.Add(layerRef);
			nNUp--;
		}
		fFoldSheetPosY += fFoldSheetCutHeight;
	}

	newLayout.m_FrontSide.PositionLayoutSide();
	newLayout.m_BackSide.PositionLayoutSide();

	//newLayout.AddFoldSheetLayer(pNewFoldSheet, nLayerIndex, fXPos, fYPos, pDoc->m_printGroupList[nProductPartIndex], bRotate);
	//int nFoldSheetIndex = -1;//pDoc->m_Bookblock.m_FoldSheetList.GetCount() - 1;
	//CFoldSheetLayerRef layerRef(nFoldSheetIndex, 0);
	//newPrintSheet.m_FoldSheetLayerRefs.Add(layerRef);
}

BOOL CPrintSheet::HasDifferentFoldSheets()
{
	if (m_FoldSheetLayerRefs.GetSize() <= 1)
		return FALSE;

	int nPrevFoldSheetIndex		 = m_FoldSheetLayerRefs[0].m_nFoldSheetIndex;
	int nPrevFoldSheetLayerIndex = m_FoldSheetLayerRefs[0].m_nFoldSheetLayerIndex;
	for (int i = 1; i < m_FoldSheetLayerRefs.GetSize(); i++)
	{
		if (nPrevFoldSheetIndex != m_FoldSheetLayerRefs[i].m_nFoldSheetIndex)
			return TRUE;
		if (nPrevFoldSheetLayerIndex != m_FoldSheetLayerRefs[i].m_nFoldSheetLayerIndex)
			return TRUE;
		nPrevFoldSheetIndex		 = m_FoldSheetLayerRefs[i].m_nFoldSheetIndex;
		nPrevFoldSheetLayerIndex = m_FoldSheetLayerRefs[i].m_nFoldSheetLayerIndex;
	}
	return FALSE;
}

BOOL CPrintSheet::HasFoldSheetsOnly()
{
	if (HasFoldSheets())
		if ( ! HasFlatProducts())
			return TRUE;

	return FALSE;
}

BOOL CPrintSheet::HasFoldSheets()
{
	if (m_FoldSheetLayerRefs.GetSize() > 0)
		return TRUE;
	else
		return FALSE;
}

BOOL CPrintSheet::HasFlatProductsOnly()
{
	if (HasFlatProducts())
		if ( ! HasFoldSheets())
			return TRUE;

	return FALSE;
}

BOOL CPrintSheet::HasFlatProducts()
{
	if (m_flatProductRefs.GetSize() > 0)
		return TRUE;
	else
		return FALSE;
}

BOOL CPrintSheet::IsEmpty()
{
	if (HasFoldSheets())
		return FALSE;
	if (HasFlatProducts())
		return FALSE;

	return TRUE;
}

int	CPrintSheet::GetNumPagesProductPart(int nProductPartIndex)
{
	int nNumPagesProductPart = 0;
	for (int nComponentRefIndex = 0; nComponentRefIndex < m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
	{
		CFoldSheet*	pFoldSheet = GetFoldSheet(nComponentRefIndex);
		if ( ! pFoldSheet)
			continue;
		if (nProductPartIndex != pFoldSheet->m_nProductPartIndex)
			continue;

		BOOL bFound = FALSE;
		for (int i = 0; i < nComponentRefIndex; i++)
		{
			if (m_FoldSheetLayerRefs[nComponentRefIndex] == m_FoldSheetLayerRefs[i])	// NUP - already counted
			{
				bFound = TRUE;
				break;
			}
		}
		if (bFound)
			continue;

		CFoldSheetLayer* pFoldSheetLayer = GetFoldSheetLayer(nComponentRefIndex);
		nNumPagesProductPart += (pFoldSheetLayer) ? pFoldSheetLayer->GetRealNumPages() : 0;//2 * pFoldSheetLayer->m_nPagesX * pFoldSheetLayer->m_nPagesY;
	}

	return nNumPagesProductPart;
}

CString CPrintSheet::GetGlobalMaskValueString(int nSide)
{
	switch (nSide)
	{
	case FRONTSIDE: return m_FrontSidePlates.GetGlobalMaskValueString();
	case BACKSIDE:  return m_BackSidePlates.GetGlobalMaskValueString();
	case BOTHSIDES:
					{
						CString strFront = m_FrontSidePlates.GetGlobalMaskValueString();
						CString strBack  = m_BackSidePlates.GetGlobalMaskValueString();
						if (strFront != strBack)
							return _T("...");
						else
							return strFront;
					}
	}
	return _T("");
}

BOOL CPrintSheet::HasLocalMaskValues(int nSide)
{
	CPlateList& rPlateList = (nSide == FRONTSIDE) ? m_FrontSidePlates : m_BackSidePlates;
	CPlate&		rPlate	   = (rPlateList.GetCount()) ? rPlateList.GetHead() : rPlateList.m_defaultPlate;
	CLayout* pLayout	   = GetLayout();
	if ( ! pLayout)
		return FALSE;

	CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;
	CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
	rObjectList.GetSheetObjectRefList(this, &objectRefList);

	for (int i = 0; i < objectRefList.GetSize(); i++)
	{
		CLayoutObject* pObject		   = objectRefList[i].m_pLayoutObject;
		CPageTemplate* pObjectTemplate = objectRefList[i].m_pObjectTemplate;
		CExposure*	   pExposure	   = (pObject) ? pObject->GetExposure(&rPlate) : NULL;
		if ( ! pObject || ! pObjectTemplate || ! pExposure)
			continue;
		if ( ! pExposure->IsBleedLocked(LEFT | RIGHT | BOTTOM | TOP))
			continue;

		if (pExposure->IsBleedLocked(LEFT))
			return TRUE;
		if (pExposure->IsBleedLocked(RIGHT))
			return TRUE;
		if (pExposure->IsBleedLocked(BOTTOM))
			return TRUE;
		if (pExposure->IsBleedLocked(TOP))
			return TRUE;
	}

	return FALSE;
}

BOOL CPrintSheet::HasAdjustContentToMask(int nSide)
{
	CPlateList& rPlateList = (nSide == FRONTSIDE) ? m_FrontSidePlates : m_BackSidePlates;
	CPlate&		rPlate	   = (rPlateList.GetCount()) ? rPlateList.GetHead() : rPlateList.m_defaultPlate;
	CLayout* pLayout	   = GetLayout();
	if ( ! pLayout)
		return FALSE;

	CLayoutObjectList& rObjectList = (nSide == FRONTSIDE) ? pLayout->m_FrontSide.m_ObjectList : pLayout->m_BackSide.m_ObjectList;
	CArray <CSheetObjectRefs, CSheetObjectRefs&> objectRefList;
	rObjectList.GetSheetObjectRefList(this, &objectRefList);

	for (int i = 0; i < objectRefList.GetSize(); i++)
	{
		CLayoutObject* pObject		   = objectRefList[i].m_pLayoutObject;
		CPageTemplate* pObjectTemplate = objectRefList[i].m_pObjectTemplate;
		CExposure*	   pExposure	   = (pObject) ? pObject->GetExposure(&rPlate) : NULL;
		if ( ! pObject || ! pObjectTemplate || ! pExposure)
			continue;

		if (pObjectTemplate->m_bAdjustContentToMask)
			return TRUE;
	}

	return FALSE;
}

CString	CPrintSheet::FindSeparation(CString strSep, int nSide)
{
	CPlate* pPlate = NULL;
	switch (nSide)
	{
	case FRONTSIDE:	pPlate = m_FrontSidePlates.GetTopmostPlate(); 
					if (pPlate) 
						if (pPlate->SeparationExists(strSep))
							return strSep;
					break;
	case BACKSIDE:	pPlate = m_BackSidePlates.GetTopmostPlate();  
					if (pPlate) 
						if (pPlate->SeparationExists(strSep))
							return strSep;	
					break;
	case BOTHSIDES: pPlate = m_FrontSidePlates.GetTopmostPlate();  
					if (pPlate) 
						if (pPlate->SeparationExists(strSep))
							return strSep;	
					pPlate = m_BackSidePlates.GetTopmostPlate();  
					if (pPlate)
						if (pPlate->SeparationExists(strSep))
							return strSep;	
					break;
	}
	
	return _T("");
}

BOOL CPrintSheet::EqualSeparationColorsFrontBack()
{
	CPlate* pFrontPlate = m_FrontSidePlates.GetTopmostPlate(); 
	CPlate* pBackPlate = m_BackSidePlates.GetTopmostPlate(); 
	if (pFrontPlate && ! pBackPlate)
		return TRUE;
	if ( ! pFrontPlate && pBackPlate)
		return TRUE;

	if (pFrontPlate && pBackPlate)
	{
		if (pFrontPlate->m_processColors.GetSize() != pBackPlate->m_processColors.GetSize())
			return FALSE;
		if (pFrontPlate->m_spotColors.GetSize() != pBackPlate->m_spotColors.GetSize())
			return FALSE;

		for (int i = 0; i < pFrontPlate->m_processColors.GetSize(); i++)
			if (pFrontPlate->m_processColors[i] != pBackPlate->m_processColors[i])
				return FALSE;

		for (int i = 0; i < pFrontPlate->m_spotColors.GetSize(); i++)
			if (pFrontPlate->m_spotColors[i] != pBackPlate->m_spotColors[i])
				return FALSE;
	}

	return TRUE;
}

CRect CPrintSheet::DrawColors(CDC* pDC, CRect rcFrame, int nSide)
{
	CRect rcBox(rcFrame.TopLeft(), CSize(0,0));
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return rcBox;

	CPlate* pPlate = NULL;
	CStringArray strSeps;
	switch (nSide)
	{
	case FRONTSIDE:	pPlate = m_FrontSidePlates.GetTopmostPlate(); 
					if (pPlate)
					{
						strSeps.Append(pPlate->m_processColors);
						strSeps.Append(pPlate->m_spotColors);
					}
					break;
	case BACKSIDE:	pPlate = m_BackSidePlates.GetTopmostPlate(); 
					if (pPlate)
					{
						strSeps.Append(pPlate->m_processColors);
						strSeps.Append(pPlate->m_spotColors);
					}
					break;
	case BOTHSIDES:	return rcBox;
	}

	if (strSeps.GetSize() <= 0)
		return rcBox;

	CPen	pen(PS_SOLID, 1, LIGHTGRAY);
	CBrush	brush(RGB(250,250,250));
	CPen*	pOldPen	  = pDC->GetCurrentPen();
	CBrush* pOldBrush = pDC->GetCurrentBrush();

	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	//pDC->Rectangle(rcFrame);
	brush.DeleteObject();

	CFont font;
	font.CreateFont (13, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	CFont* pOldFont = pDC->SelectObject(&font);

	CString strText;

	//CRect rcText = rcFrame; rcText.bottom = rcText.top + Pixel2MM(pDC, 15);
	//pDC->SetTextColor(BLACK);
	//strText.Format(_T("%d"), strSeps.GetSize()); 
	//pDC->DrawText(strText, rcText, DT_LEFT | DT_SINGLELINE | DT_TOP | DT_END_ELLIPSIS);
	//rcText.right = rcText.left + pDC->GetTextExtent(strText).cx + 5;
	//rcBox.UnionRect(rcBox, rcText);

	int nXPos = rcFrame.left;// + Pixel2MM(pDC, 5);
	int nYPos = rcFrame.top	 + Pixel2MM(pDC, 2);
	int nXOffset = Pixel2MM(pDC, 45);

	//nXPos = rcFrame.left;
	CRect rcIcon;
	int nWidth  = (pDC->IsPrinting()) ? Pixel2MM(pDC, 6) : Pixel2MM(pDC, 7);
	int nHeight = (pDC->IsPrinting()) ? Pixel2MM(pDC, 6) : Pixel2MM(pDC, 7);
	int nStep	= (pDC->IsPrinting()) ? Pixel2MM(pDC, 5) : Pixel2MM(pDC, 7);
	rcIcon.left = nXPos; rcIcon.top = nYPos + Pixel2MM(pDC, 2); rcIcon.right = rcIcon.left + nWidth; rcIcon.bottom = rcIcon.top + nHeight;
	for (int i = 0; i < strSeps.GetSize(); i++)
	{
		CColorDefinition* pColorDef = pDoc->m_ColorDefinitionTable.FindColorDefinition(strSeps[i]);
		if ( ! pColorDef)
			pColorDef = theApp.m_colorDefTable.FindColorDefinition(strSeps[i]);
		brush.CreateSolidBrush((pColorDef) ? pColorDef->m_rgb : WHITE);
		pDC->SelectObject(&brush);
		if (rcFrame.PtInRect(rcIcon.TopLeft()) && rcFrame.PtInRect(rcIcon.BottomRight()))
			pDC->Rectangle(rcIcon);
		brush.DeleteObject();
		rcBox.UnionRect(rcBox, rcIcon);

		rcIcon.OffsetRect(nStep, 0);//Pixel2MM(pDC, 8), 0);
	}
	pDC->SelectObject(pOldPen);
	pDC->SelectObject(pOldBrush);
	pen.DeleteObject();
	brush.DeleteObject();
	font.DeleteObject();

	pDC->SelectObject(pOldFont);

	return rcBox;
}

CString	CPrintSheet::CalcTotalQuantity(BOOL bReset)
{
	int nPrintSheetQuantity = (bReset) ? 0 : m_nQuantityTotal;
	InitializeFoldSheetTypeList();
	if (bReset)
	{
		for (int i = 0; i < m_FoldSheetTypeList.GetSize(); i++)
		{
			CFoldSheet* pFoldSheet	= m_FoldSheetTypeList[i].m_pFoldSheet;
			int			nNumUp		= m_FoldSheetTypeList[i].m_nNumUp;
			if ( ! pFoldSheet)
				continue;

			int nFoldSheetQuantity = pFoldSheet->GetQuantityPlanned() + pFoldSheet->GetQuantityExtra();

			div_t div_result;
			div_result = div(nFoldSheetQuantity, nNumUp);
			int nNumPrintSheets = (div_result.rem > 0) ? div_result.quot + 1 : div_result.quot;

			nPrintSheetQuantity = max(nPrintSheetQuantity, nNumPrintSheets);
		}
	}

	InitializeFlatProductTypeList();
	if (bReset)
	{
		for (int i = 0; i < m_flatProductTypeList.GetSize(); i++)
		{
			CFlatProduct* pFlatProduct = m_flatProductTypeList[i].m_pFlatProduct;
			int			  nNumUp	   = m_flatProductTypeList[i].m_nNumUp;
			if ( ! pFlatProduct)
				continue;

			int nQuantity = pFlatProduct->m_nDesiredQuantity;

			div_t div_result;
			div_result = div(nQuantity, nNumUp);
			int nNumPrintSheets = (div_result.rem > 0) ? div_result.quot + 1 : div_result.quot;

			nPrintSheetQuantity = max(nPrintSheetQuantity, nNumPrintSheets);
		}

		m_nQuantityTotal = nPrintSheetQuantity + m_nQuantityExtra;
	}
	CString strRet; strRet.Format(_T("%d"), nPrintSheetQuantity);

	for (int i = 0; i < m_FoldSheetTypeList.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet	= m_FoldSheetTypeList[i].m_pFoldSheet;
		if ( ! pFoldSheet)
			continue;

		pFoldSheet->CalcQuantityActual();
	}

	for (int i = 0; i < m_flatProductTypeList.GetSize(); i++)
	{
		CFlatProduct* pFlatProduct = m_flatProductTypeList[i].m_pFlatProduct;
		if ( ! pFlatProduct)
			continue;

		pFlatProduct->CalcRealQuantity();
	}

	return strRet;
}

int	CPrintSheet::GetNumComponents()
{
	int nNum = 0;

	InitializeFoldSheetTypeList();
	for (int i = 0; i < m_FoldSheetTypeList.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = m_FoldSheetTypeList[i].m_pFoldSheet;
		if ( ! pFoldSheet)
			continue;

		nNum++;
	}

	InitializeFlatProductTypeList();
	for (int i = 0; i < m_flatProductTypeList.GetSize(); i++)
	{
		CFlatProduct* pFlatProduct = m_flatProductTypeList[i].m_pFlatProduct;
		if ( ! pFlatProduct)
			continue;

		nNum++;
	}

	return nNum;
}

BOOL CPrintSheet::GetPaperName(CString& strPaperName)
{
	strPaperName.Empty();

	InitializeFoldSheetTypeList();
	for (int i = 0; i < m_FoldSheetTypeList.GetSize(); i++)
	{
		CFoldSheet* pFoldSheet = m_FoldSheetTypeList[i].m_pFoldSheet;
		if ( ! pFoldSheet)
			continue;
		if ( ! strPaperName.IsEmpty())
			if (strPaperName != pFoldSheet->m_strPaperName)
				return TRUE;
		strPaperName = pFoldSheet->m_strPaperName;
	}

	//InitializeLabelTypeList();
	//for (int i = 0; i < m_labelTypeList.GetSize(); i++)
	//{
	//	CLabelListEntry* pLabelListEntry = m_labelTypeList[i].m_pLabelListEntry;
	//	if ( ! pLabelListEntry)
	//		continue;
	//	if ( ! strPaperName.IsEmpty())
	//		if (strPaperName != pFoldSheet->m_strPaperName)
	//			return _T("Mehrere Sorten!");
	//	strPaperName = pFoldSheet->m_strPaperName;
	//	nNum++;
	//}

	return FALSE;
}

BOOL CPrintSheet::HasObjects()
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return FALSE;

	if (pLayout->m_FrontSide.m_ObjectList.GetCount() || pLayout->m_BackSide.m_ObjectList.GetCount())
		return TRUE;
	else
		return FALSE;
}

CLayout* CPrintSheet::SplitLayout()
{
	CImpManDoc*  pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return NULL;
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return NULL;

	CPrintingProfile profile =  pLayout->GetPrintingProfile();
	CLayout			 layout	 = *pLayout;
	layout.AssignDefaultName();
	pDoc->m_PrintSheetList.m_Layouts.AddTail(layout);
	pDoc->m_printingProfiles.AddTail(profile);

	CLayout* pNewLayout		 = &pDoc->m_PrintSheetList.m_Layouts.GetTail();
	int		 nNewLayoutIndex = pDoc->m_PrintSheetList.m_Layouts.GetCount() - 1;

	CPrintSheet* pPrintSheet = pLayout->GetFirstPrintSheet();
	while (pPrintSheet)
	{
		if (pPrintSheet != this)
		{
			pPrintSheet->LinkLayout(pNewLayout);
			pPrintSheet->m_nLayoutIndex = nNewLayoutIndex;
		}
		pPrintSheet = pLayout->GetNextPrintSheet(pPrintSheet);
	}

	//POSITION pos = pDoc->m_printingProfiles.FindIndex(nNewLayoutIndex);
	//if (pos)
	//	pDoc->m_printingProfiles.InsertBefore(pos, rProfile);
	//else
	//	pDoc->m_printingProfiles.AddTail(rProfile);
	//if (m_nLayoutIndex < pDoc->m_printGroupList.GetSize())
	//	pDoc->m_printGroupList.InsertAt(m_nLayoutIndex, CPrintGroup());
	//else
		pDoc->m_printGroupList.Add(CPrintGroup());

	pDoc->m_PrintSheetList.ReNumberPrintSheets();

	return GetLayout();
}

int CPrintSheet::GetMaxNumCloneable(CFoldSheet* pFoldSheet, int nNumPagesToImpose)
{
	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if ( ! pDoc)
		return 0;
	if (HasFlatProducts())
		return 0;

	int nFoldSheetIndex = -1;
	int nLayerIndex		= 0;
	if (IsEmpty())
	{
		if ( ! pFoldSheet)
			return 0;
		nFoldSheetIndex = (pFoldSheet) ? pFoldSheet->GetIndex() : -1;
		if (nFoldSheetIndex < 0)	// preselected new foldsheet which is about to be added to bookblock
		{
			int nNumFoldSheetPages = ( ! pFoldSheet->IsEmpty()) ? pFoldSheet->GetNumPages() : INT_MAX;
			return nNumPagesToImpose / nNumFoldSheetPages;
		}
	}
	else
	{
		if (m_FoldSheetLayerRefs.GetSize() <= 0)
			return 0;
		pFoldSheet = GetFoldSheet(0);
		nFoldSheetIndex = (pFoldSheet) ? pFoldSheet->GetIndex() : -1;
		for (int i = 1; i < m_FoldSheetLayerRefs.GetSize(); i++)
		{
			pFoldSheet = GetFoldSheet(i);
			if (pFoldSheet)
				if (pFoldSheet->GetIndex() != nFoldSheetIndex)
					return 0;
		}
	}

	CPrintComponent 		component = pDoc->m_printComponentsList.FindFoldSheetLayerComponent(nFoldSheetIndex, nLayerIndex);
	CPrintComponentsGroup&	rGroup	  = pDoc->m_printComponentsList.FindGroup(component);
	return rGroup.GetNumFoldSheets(pFoldSheet);
}

void CPrintSheet::GetFoldSheetLayerReflines(int nComponentRefIndex, int nSide, int nReferenceLineX, int nReferenceLineY, float& fLeft, float& fBottom)
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
	{
		fLeft = fBottom = 0.0f;
		return;
	}

	CLayoutSide* pLayoutSide = (nSide == FRONTSIDE) ? &pLayout->m_FrontSide : &pLayout->m_BackSide;

	float fBoxLeft, fBoxBottom, fBoxRight, fBoxTop, fBoxWidth, fBoxHeight;
	pLayoutSide->CalcBoundingRectObjects(&fBoxLeft, &fBoxBottom, &fBoxRight, &fBoxTop, &fBoxWidth, &fBoxHeight, FALSE, nComponentRefIndex, FALSE, NULL);

	switch (nReferenceLineX)
	{
	case LEFT:		fLeft = fBoxLeft;							break;										
	case RIGHT:		fLeft = fBoxRight;							break;			
	case XCENTER:	fLeft = fBoxLeft	 + fBoxWidth  / 2.0f;	break;
	default:		fLeft = 0.0f;								break;
	}
	switch (nReferenceLineY)
	{
	case BOTTOM:	fBottom = fBoxBottom;						break;		
	case TOP:		fBottom = fBoxTop;							break;			
	case YCENTER:	fBottom = fBoxBottom + fBoxHeight / 2.0f;	break;
	default:		fBottom = 0.0f;								break;
	}
}

void CPrintSheet::SetFoldSheetVisible(CFoldSheet& rFoldSheet, BOOL bVisible)
{
	CLayout* pLayout = GetLayout();
	if ( ! pLayout)
		return;

	for (int nComponentRefIndex = 0; nComponentRefIndex < m_FoldSheetLayerRefs.GetSize(); nComponentRefIndex++)
	{
		BOOL bRemove = FALSE;
		CFoldSheet* pFoldSheet = GetFoldSheet(nComponentRefIndex);
		if ( ! pFoldSheet)
			bRemove = TRUE;
		else
			if (pFoldSheet)
				if (pFoldSheet->m_strFoldSheetTypeName == rFoldSheet.m_strFoldSheetTypeName)
					if (pFoldSheet->m_nProductPartIndex == rFoldSheet.m_nProductPartIndex)
						bRemove = TRUE;
		if (bRemove)
		{
			pLayout->m_FrontSide.SetFoldSheetLayerVisible(nComponentRefIndex, bVisible);
			pLayout->m_BackSide.SetFoldSheetLayerVisible(nComponentRefIndex, bVisible);
		}
	}
}

void CPrintSheet::ReorganizeColorInfos()
{
	if ( ! m_FrontSidePlates.GetCount())
		m_FrontSidePlates.m_defaultPlate.UpdateExposures();
	if ( ! m_BackSidePlates.GetCount())
		m_BackSidePlates.m_defaultPlate.UpdateExposures();

	m_FrontSidePlates.m_defaultPlate.ReorganizeColorInfos();
	m_BackSidePlates.m_defaultPlate.ReorganizeColorInfos(); 

	POSITION platePos = m_FrontSidePlates.GetHeadPosition();
	while (platePos)
	{
		CPlate&  rPlate = m_FrontSidePlates.GetNext(platePos);
		rPlate.ReorganizeColorInfos();
	}

	platePos = m_BackSidePlates.GetHeadPosition();
	while (platePos)
	{
		CPlate&  rPlate = m_BackSidePlates.GetNext(platePos);
		rPlate.ReorganizeColorInfos();
	}	
}



// helper functions for PrintSheetList elements
template <> void AFXAPI SerializeElements <CPrintSheet> (CArchive& ar, CPrintSheet* pPrintSheet, INT_PTR nCount)
{
	ASSERT(nCount == 1);  // nCount can be >1 if called from CArray serialize,
						  // but this is not implemented yet

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	ar.SerializeClass(RUNTIME_CLASS(CPrintSheet));

	if (ar.IsStoring())
	{
		ar << pPrintSheet->GetID();
		ar << pPrintSheet->m_nPrintSheetNumber;
		ar << pPrintSheet->m_strNumerationStyle;
		ar << pPrintSheet->m_nNumerationOffset;
		ar << pPrintSheet->m_nPrintSheetParentIndex;
		ar << pPrintSheet->m_nPrintSheetType; 
		ar << pPrintSheet->m_fShinglingOffset;  
		ar << pPrintSheet->m_fShinglingStep;    
		ar << pPrintSheet->m_nLayoutIndex;
		ar << pPrintSheet->m_strPDFTarget;
		ar << pPrintSheet->GetPrintGroupIndex();
		ar << pPrintSheet->m_nQuantityExtra;
		ar << pPrintSheet->m_nQuantityTotal;
		pPrintSheet->m_FoldSheetLayerRefs.Serialize(ar);
		pPrintSheet->m_flatProductRefs.Serialize(ar);
		pPrintSheet->m_FrontSidePlates.Serialize(ar);
		pPrintSheet->m_BackSidePlates.Serialize(ar);
	}
	else
	{
		UINT nVersion = ar.GetObjectSchema();

		CString strHelper;
		int		nHelper;
		switch (nVersion)
		{
		case 1:
			ar >> pPrintSheet->m_nPrintSheetNumber;
			ar >> pPrintSheet->m_nPrintSheetParentIndex;
			ar >> pPrintSheet->m_nPrintSheetType; 
			ar >> pPrintSheet->m_fShinglingOffset;  
			ar >> pPrintSheet->m_fShinglingStep;    
			ar >> pPrintSheet->m_nLayoutIndex;
			pPrintSheet->m_strNumerationStyle = _T("$n");
			pPrintSheet->m_nNumerationOffset  = 0;
			pPrintSheet->SetPrintGroupIndex(0);
			pPrintSheet->m_nQuantityExtra = 0;
			pPrintSheet->m_nQuantityTotal = 0;
			break;
		case 2:
			ar >> pPrintSheet->m_nPrintSheetNumber;
			ar >> pPrintSheet->m_nPrintSheetParentIndex;
			ar >> pPrintSheet->m_nPrintSheetType; 
			ar >> pPrintSheet->m_fShinglingOffset;  
			ar >> pPrintSheet->m_fShinglingStep;    
			ar >> pPrintSheet->m_nLayoutIndex;
			ar >> pPrintSheet->m_strPDFTarget;
			pPrintSheet->m_strNumerationStyle = _T("$n");
			pPrintSheet->m_nNumerationOffset  = 0;
			pPrintSheet->SetPrintGroupIndex(0);
			pPrintSheet->m_nQuantityExtra = 0;
			pPrintSheet->m_nQuantityTotal = 0;
			break;
		case 3:
			ar >> pPrintSheet->m_nPrintSheetNumber;
			ar >> pPrintSheet->m_strNumerationStyle;
			ar >> pPrintSheet->m_nNumerationOffset;
			ar >> pPrintSheet->m_nPrintSheetParentIndex;
			ar >> pPrintSheet->m_nPrintSheetType; 
			ar >> pPrintSheet->m_fShinglingOffset;  
			ar >> pPrintSheet->m_fShinglingStep;    
			ar >> pPrintSheet->m_nLayoutIndex;
			ar >> pPrintSheet->m_strPDFTarget;
			pPrintSheet->SetPrintGroupIndex(0);
			pPrintSheet->m_nQuantityExtra = 0;
			pPrintSheet->m_nQuantityTotal = 0;
			break;
		case 4:
			ar >> pPrintSheet->m_nPrintSheetNumber;
			ar >> pPrintSheet->m_strNumerationStyle;
			ar >> pPrintSheet->m_nNumerationOffset;
			ar >> pPrintSheet->m_nPrintSheetParentIndex;
			ar >> pPrintSheet->m_nPrintSheetType; 
			ar >> pPrintSheet->m_fShinglingOffset;  
			ar >> pPrintSheet->m_fShinglingStep;    
			ar >> pPrintSheet->m_nLayoutIndex;
			ar >> pPrintSheet->m_strPDFTarget;
			ar >> nHelper;
			pPrintSheet->SetPrintGroupIndex(nHelper);
			pPrintSheet->m_nQuantityExtra = 0;
			pPrintSheet->m_nQuantityTotal = 0;
			break;
		case 5:
		case 6:
			ar >> pPrintSheet->m_nPrintSheetNumber;
			ar >> pPrintSheet->m_strNumerationStyle;
			ar >> pPrintSheet->m_nNumerationOffset;
			ar >> pPrintSheet->m_nPrintSheetParentIndex;
			ar >> pPrintSheet->m_nPrintSheetType; 
			ar >> pPrintSheet->m_fShinglingOffset;  
			ar >> pPrintSheet->m_fShinglingStep;    
			ar >> pPrintSheet->m_nLayoutIndex;
			ar >> pPrintSheet->m_strPDFTarget;
			ar >> nHelper;
			pPrintSheet->SetPrintGroupIndex(nHelper);
			ar >> pPrintSheet->m_nQuantityExtra;
			ar >> pPrintSheet->m_nQuantityTotal;
			break;
		case 7:
			ar >> strHelper; pPrintSheet->SetID(strHelper);
			ar >> pPrintSheet->m_nPrintSheetNumber;
			ar >> pPrintSheet->m_strNumerationStyle;
			ar >> pPrintSheet->m_nNumerationOffset;
			ar >> pPrintSheet->m_nPrintSheetParentIndex;
			ar >> pPrintSheet->m_nPrintSheetType; 
			ar >> pPrintSheet->m_fShinglingOffset;  
			ar >> pPrintSheet->m_fShinglingStep;    
			ar >> pPrintSheet->m_nLayoutIndex;
			ar >> pPrintSheet->m_strPDFTarget;
			ar >> nHelper;
			pPrintSheet->SetPrintGroupIndex(nHelper);
			ar >> pPrintSheet->m_nQuantityExtra;
			ar >> pPrintSheet->m_nQuantityTotal;
			break;
		}
		pPrintSheet->m_FoldSheetLayerRefs.RemoveAll();  // no append, so target list has to be empty 
		pPrintSheet->m_flatProductRefs.RemoveAll();
		pPrintSheet->m_FrontSidePlates.RemoveAll();
		pPrintSheet->m_BackSidePlates.RemoveAll();
		pPrintSheet->m_FoldSheetLayerRefs.Serialize(ar);
		if (nVersion >= 6)
			pPrintSheet->m_flatProductRefs.Serialize(ar);
		pPrintSheet->m_FrontSidePlates.Serialize(ar);
		pPrintSheet->m_BackSidePlates.Serialize(ar);
	}
}

void AFXAPI DestructElements(CPrintSheet* pPrintSheet, INT_PTR nCount)
{
	ASSERT(nCount == 1); // in CList context nCount has to be 1

#ifndef _DEBUG
	nCount = nCount;	// to prevent compiler-warning: "unreferenced formal parameter"
#endif

	pPrintSheet->m_strNumerationStyle.Empty();

	pPrintSheet->m_FoldSheetLayerRefs.RemoveAll();
	pPrintSheet->m_FrontSidePlates.RemoveAll();
	pPrintSheet->m_BackSidePlates.RemoveAll();

	pPrintSheet->m_strPDFTarget.Empty();
}
//
//void CPrintSheet::ConvertToFlatProductRefs()
//{
//}

