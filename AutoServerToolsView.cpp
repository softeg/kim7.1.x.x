// AutoServerToolsView.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "AutoServerFrame.h"
#include "AutoServerToolsView.h"
#include "AutoServerJobsView.h"
#include "AutoServerWorkflowsView.h"
#include "AutoServerDetailsView.h"
#include "DlgAutoServerWorkflow.h"
#include "GraphicComponent.h"
#include "KimSocketsCommon.h"


// CAutoServerToolsView

IMPLEMENT_DYNCREATE(CAutoServerToolsView, CFormView)

CAutoServerToolsView::CAutoServerToolsView()
	: CFormView(CAutoServerToolsView::IDD)
{

}

CAutoServerToolsView::~CAutoServerToolsView()
{
}

void CAutoServerToolsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAutoServerToolsView, CFormView)
	ON_COMMAND(ID_AUTOSERVERVIEW_SELECT_TOOLBAR, OnSelectView)
	ON_COMMAND(ID_AUTOSERVERVIEW_NEW, OnNew)
	ON_COMMAND(ID_AUTOSERVERVIEW_REMOVE, OnRemove)
	ON_COMMAND(ID_AUTOSERVERVIEW_STARTOUTPUT, OnStartOutput)
	ON_COMMAND(ID_AUTOSERVERVIEW_RESTARTJOB, OnRestartJob)
	ON_COMMAND(ID_AUTOSERVERVIEW_ABORT, OnAbort)
	ON_UPDATE_COMMAND_UI(ID_AUTOSERVERVIEW_SELECT_TOOLBAR, OnUpdateSelectView)
	ON_UPDATE_COMMAND_UI(ID_AUTOSERVERVIEW_NEW, OnUpdateNew)
	ON_UPDATE_COMMAND_UI(ID_AUTOSERVERVIEW_REMOVE, OnUpdateRemove)
	ON_UPDATE_COMMAND_UI(ID_AUTOSERVERVIEW_STARTOUTPUT, OnUpdateStartOutput)
	ON_UPDATE_COMMAND_UI(ID_AUTOSERVERVIEW_RESTARTJOB, OnUpdateRestartJob)
	ON_UPDATE_COMMAND_UI(ID_AUTOSERVERVIEW_ABORT, OnUpdateAbort)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CAutoServerToolsView-Diagnose

#ifdef _DEBUG
void CAutoServerToolsView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CAutoServerToolsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG



void CAutoServerToolsView::DrawItemToolBar(LPDRAWITEMSTRUCT lpDrawItemStruct, int nState)
{
	CDC*	pDC			= CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect	rcItem		= lpDrawItemStruct->rcItem;
	CString strText;
	CImage  img;
	switch ((int)lpDrawItemStruct->itemData)
	{
	case 0:		strText = "Jobs";		break;//.LoadString(IDS_SHEETPLANNING);			break;
	case 1:		strText = "Meldungen";	break;//.LoadString(IDS_PRINTSHEETLIST);		break;
	case 2:		strText = "Workflows";	break;//.LoadString(IDS_PRINTSHEETLIST);		break;
	case 3:		img.LoadFromResource(theApp.m_hInstance, IDB_NEW_WORKFLOW);	break;
	case 4:		img.LoadFromResource(theApp.m_hInstance, (nState & ODB_DISABLED) ? IDB_REMOVE_COLD  : IDB_REMOVE);		break;
	case 5:		img.LoadFromResource(theApp.m_hInstance, (nState & ODB_DISABLED) ? IDB_START_COLD   : IDB_START);		break;
	case 6:		img.LoadFromResource(theApp.m_hInstance, (nState & ODB_DISABLED) ? IDB_STOP_COLD	: IDB_STOP);		break;
	case 7:		img.LoadFromResource(theApp.m_hInstance, (nState & ODB_DISABLED) ? IDB_RESTART_COLD	: IDB_RESTART);		break;
	default:	break;
	}

	CRect rcImg;
	if ( ! img.IsNull())
	{
		rcImg.left = rcItem.CenterPoint().x - img.GetWidth()/2; rcImg.top = rcItem.CenterPoint().y - img.GetHeight()/2; rcImg.right = rcImg.left + img.GetWidth(); rcImg.bottom = rcImg.top + img.GetHeight();
		img.TransparentBlt(lpDrawItemStruct->hDC, rcImg, RGB(244,243,238));
	}

	CFont font;
	font.CreateFont(11, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_SWISS, _T("Tahoma"));
	pDC->SelectObject(&font);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(GUICOLOR_DLG_STATIC_TEXT);
	
	CRect rcText = rcItem; //rcText.top = rcItem.top + 5; 
	pDC->DrawText(strText, rcText, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	font.DeleteObject();
}

void CAutoServerToolsView::UpdateBackgroundToolBar(CDC* pDC, CRect rcRect, CWnd* pWnd)
{
	CGraphicComponent gp;
	gp.DrawTitleBar(pDC, rcRect, _T(""), RGB(245,250,255), RGB(235,240,245), 90, -1);
}

int CAutoServerToolsView::GetActJobStatus()
{
	CAutoServerJobsView* pView = theApp.GetAutoServerJobsView();
	if (pView)
		return pView->GetActJobStatus();
	else
		return -2;
}


// CAutoServerToolsView-Meldungshandler

void CAutoServerToolsView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	CRect rcButtonFrame;
	m_toolBarFrame.SubclassDlgItem(IDC_AUTOSERVERVIEW_TOOLBARFRAME, this);
	m_toolBarButtons.Create(this, GetParentFrame(), DrawItemToolBar, -1, -1, CSize(0,0), NULL);
	m_toolBarFrame.GetWindowRect(rcButtonFrame);
	ScreenToClient(rcButtonFrame);

	rcButtonFrame.left = 20;
	rcButtonFrame.right = rcButtonFrame.left + 120; rcButtonFrame.DeflateRect(0, 2);
	m_toolBarButtons.AddButton(_T(""), _T(""),					ID_AUTOSERVERVIEW_SELECT_TOOLBAR,	rcButtonFrame, -1, (ULONG_PTR)0, FALSE);
	rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
	m_toolBarButtons.AddButton(_T(""), _T(""),					ID_AUTOSERVERVIEW_SELECT_TOOLBAR,	rcButtonFrame, -1, (ULONG_PTR)1, FALSE);
	rcButtonFrame.OffsetRect(rcButtonFrame.Width(), 0);
	m_toolBarButtons.AddButton(_T(""), _T(""),					ID_AUTOSERVERVIEW_SELECT_TOOLBAR,	rcButtonFrame, -1, (ULONG_PTR)2, FALSE);

	rcButtonFrame.OffsetRect(rcButtonFrame.Width() + 50, 0); rcButtonFrame.right = rcButtonFrame.left + 60;
	m_toolBarButtons.AddButton(_T(""), _T("Neuer Workflow"),	ID_AUTOSERVERVIEW_NEW,				rcButtonFrame, -1, (ULONG_PTR)3, FALSE);

	rcButtonFrame.OffsetRect(rcButtonFrame.Width() + 20, 0); 
	m_toolBarButtons.AddButton(_T(""), _T("Entfernen"),			ID_AUTOSERVERVIEW_REMOVE,			rcButtonFrame, -1, (ULONG_PTR)4, FALSE);

	rcButtonFrame.OffsetRect(rcButtonFrame.Width() + 200, 0); rcButtonFrame.right = rcButtonFrame.left + 40;
	m_toolBarButtons.AddButton(_T(""), _T("Vorgang abbrechen"),	ID_AUTOSERVERVIEW_ABORT,			rcButtonFrame, -1, (ULONG_PTR)6, FALSE);

	rcButtonFrame.OffsetRect(rcButtonFrame.Width() + 30, 0); 
	m_toolBarButtons.AddButton(_T(""), _T("Ausgabe starten"),	ID_AUTOSERVERVIEW_STARTOUTPUT,		rcButtonFrame, -1, (ULONG_PTR)5, FALSE);

	rcButtonFrame.OffsetRect(rcButtonFrame.Width() + 30, 0); 
	m_toolBarButtons.AddButton(_T(""), _T("Job neu starten"),	ID_AUTOSERVERVIEW_RESTARTJOB,		rcButtonFrame, -1, (ULONG_PTR)7, FALSE);

	m_toolBarButtons.SelectButton(0);
	OnSelectView();

	CRect rcClient; GetClientRect(rcClient); rcClient.bottom = max(rcClient.bottom, 37);	// when window is small so scrollbars would appear, GetClientRect() returns 20 pixel hight instead of 37
	SetScaleToFitSize(rcClient.Size());	//suppress scrollbars								
}

void CAutoServerToolsView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
	if ( ! m_toolBarFrame.m_hWnd)
		return;

	CAutoServerFrame* pFrame = (CAutoServerFrame*)GetParentFrame();
	if (pFrame)
	{
		CRect rcPane, rcButtonFrame;
		CWnd* pPane = pFrame->m_wndSplitter.GetPane(1, 0);
		pPane->GetWindowRect(rcPane);
		ScreenToClient(rcPane);

		//m_toolBarFrame.GetWindowRect(rcButtonFrame);
		//ScreenToClient(rcButtonFrame);

		//rcButtonFrame.OffsetRect(rcPane.left - rcButtonFrame.left + 10, nOffset);
		//rcButtonFrame.right = rcButtonFrame.left + 120; rcButtonFrame.DeflateRect(0, 2);
		//m_toolBarButtons[5]->GetWindowRect(rcButtonFrame);
		//m_toolBarButtons[5]->MoveWindow(rcButtonFrame);
	}
}

void CAutoServerToolsView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	if ( ! m_toolBarFrame.m_hWnd)
		return;

	//CAutoServerFrame* pFrame = (CAutoServerFrame*)GetParentFrame();
	//if (pFrame)
	//{
	//	CAutoServerDetailsView* pView = theApp.GetAutoServerDetailsView();
	//	if (pView)
	//	{
	//		int nPos = pView->GetHeaderColumnPos(2);
	//		if (nPos >= 0)
	//		{
	//			CRect rcView; pView->GetWindowRect(rcView); nPos += rcView.left;

	//			CRect rcButtonFrame;
	//			m_toolBarButtons[5]->GetWindowRect(rcButtonFrame);
	//			ScreenToClient(rcButtonFrame);
	//			rcButtonFrame.OffsetRect(nPos - rcButtonFrame.left, 0);
	//			m_toolBarButtons[5]->MoveWindow(rcButtonFrame);

	//			m_toolBarButtons[6]->GetWindowRect(rcButtonFrame);
	//			ScreenToClient(rcButtonFrame);
	//			rcButtonFrame.OffsetRect(nPos - rcButtonFrame.left + 50, 0);
	//			m_toolBarButtons[6]->MoveWindow(rcButtonFrame);

	//			m_toolBarButtons[7]->GetWindowRect(rcButtonFrame);
	//			ScreenToClient(rcButtonFrame);
	//			rcButtonFrame.OffsetRect(nPos - rcButtonFrame.left + 100, 0);
	//			m_toolBarButtons[7]->MoveWindow(rcButtonFrame);
	//		}
	//	}
	//}
}

BOOL CAutoServerToolsView::PreTranslateMessage(MSG* pMsg)
{
	UpdateDialogControls( this, TRUE );

	if (m_toolBarButtons.m_pToolTip)            
		m_toolBarButtons.m_pToolTip->RelayEvent(pMsg);	

	return CFormView::PreTranslateMessage(pMsg);
}

void CAutoServerToolsView::OnSelectView()
{
	int nIndex = m_toolBarButtons.GetSelectedButtonData();
	if (nIndex < 0)
		return;

	CAutoServerFrame* pFrame = (CAutoServerFrame*)GetParentFrame();
	if ( ! pFrame)
		return;

	switch (nIndex)
	{
	case 0:		pFrame->m_nCurrentView = CAutoServerFrame::ViewJobs;		
				pFrame->SetupViewJobs();
				break;
	case 1:		pFrame->m_nCurrentView = CAutoServerFrame::ViewLogs;	
				pFrame->SetupViewLogs();
				break;
	case 2:		pFrame->m_nCurrentView = CAutoServerFrame::ViewWorkflows;	
				pFrame->SetupViewWorkflows();
				break;
	default:	break;
	}
}

void CAutoServerToolsView::OnNew()
{
	CAutoServerFrame* pFrame = theApp.GetAutoServerFrame();
	if ( ! pFrame)
		return;

	switch (pFrame->m_nCurrentView)
	{
	case CAutoServerFrame::ViewJobs:			
		{
		}
		break;
	case CAutoServerFrame::ViewWorkflows:	
		{
			CDlgAutoServerWorkflow dlg;
			if (dlg.DoModal() == IDCANCEL)
				return; 

			CString strHotfolderPath = theApp.GetAutoServerHotFolderRoot() + _T("\\") + dlg.m_strName;

			int nWorkflowID = theApp.m_kaDB.ka_SetWorkflow(dlg.m_strName.GetBuffer(), 0, kimAutoDB::Hotfolder, dlg.m_nJobdataSource, kimAutoDB::ProductionProfile, dlg.m_strProductionProfile.GetBuffer(), _T(""), strHotfolderPath.GetBuffer(), _T(""), 
														   dlg.m_nDBAttribute, dlg.m_nDBActionWhenFinished, dlg.m_nDBActionDelayMinutes, 0);
		
			theApp.m_clientSocket.CommandToServer(CMD_NEW_WORKFLOW, nWorkflowID);

			CAutoServerWorkflowsView* pView = theApp.GetAutoServerWorkflowsView();
			if (pView)
			{
				pView->Invalidate();
				pView->m_DisplayList.Invalidate();
				pView->UpdateWindow();
			}
		}
		break;
	}
}

void CAutoServerToolsView::OnRemove()
{
	CAutoServerFrame* pFrame = (CAutoServerFrame*)GetParentFrame();
	if ( ! pFrame)
		return;

	switch (pFrame->m_nCurrentView)
	{
	case CAutoServerFrame::ViewJobs:			
		{
			CAutoServerJobsView* pView = theApp.GetAutoServerJobsView();
			if ( ! pView)
				return;

			CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerJobsView, Job));
			if ( ! pDI)
				return;

			kimAutoDB::_jobs job; 
			theApp.m_kaDB.ka_GetJob(&job, (int)pDI->m_pData);

			CString strMessage;
			AfxFormatString1(strMessage, IDS_MSG_REMOVE_JOB, job.Name.c_str());
			if (AfxMessageBox(strMessage, MB_YESNO) == IDNO)
				return;

			while (pDI)
			{
				theApp.m_clientSocket.CommandToServer(CMD_REMOVE_JOB, (int)pDI->m_pData);
				Sleep(100L);
				pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CAutoServerJobsView, Job));
			}
		}
		break;
	case CAutoServerFrame::ViewWorkflows:	
		{
			CAutoServerWorkflowsView* pView = theApp.GetAutoServerWorkflowsView();
			if ( ! pView)
				return;

			CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerWorkflowsView, Workflow));
			if ( ! pDI)
				return;

			kimAutoDB::_workflows workflow;
			theApp.m_kaDB.ka_GetWorkflow(&workflow, (int)pDI->m_pData);

			CString strMessage;
			AfxFormatString1(strMessage, IDS_MSG_REMOVE_WORKLOW, workflow.name.c_str());
			if (AfxMessageBox(strMessage, MB_YESNO) == IDNO)
				return;

			while (pDI)
			{
				theApp.m_clientSocket.CommandToServer(CMD_REMOVE_WORKFLOW, (int)pDI->m_pData);
				pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CAutoServerWorkflowsView, Workflow));
			}
		}
		break;
	case CAutoServerFrame::ViewLogs:	
		{
			CAutoServerLogsView* pView = theApp.GetAutoServerLogsView();
			if ( ! pView)
				return;

			CDisplayItem* pDI = pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerLogsView, MessageLog));
			if ( ! pDI)
				return;

			if (AfxMessageBox(IDS_MSG_REMOVE_MESSAGELOG, MB_YESNO) == IDNO)
				return;

			CArray <int, int> deleteList;	// use helper list, because LogsView could be updated by server. So display list could be updated while this is running
			while (pDI)
			{
				deleteList.Add((int)pDI->m_pData);
				pDI = pView->m_DisplayList.GetNextSelectedItem(pDI, DISPLAY_ITEM_TYPEID(CAutoServerLogsView, MessageLog));
			}
			for (int i = 0; i < deleteList.GetSize(); i++)
				theApp.m_clientSocket.CommandToServer(CMD_REMOVE_MESSAGELOG, deleteList[i]);
		}
		break;
	}
}

void CAutoServerToolsView::OnStartOutput()
{
	CAutoServerJobsView* pView = theApp.GetAutoServerJobsView();
	if ( ! pView)
		return;

	theApp.m_clientSocket.CommandToServer(CMD_OUTPUT_JOB, pView->GetActJobID());
}

void CAutoServerToolsView::OnRestartJob()
{
	CAutoServerJobsView* pView = theApp.GetAutoServerJobsView();
	if ( ! pView)
		return;

	theApp.m_clientSocket.CommandToServer(CMD_RESTART_JOB, pView->GetActJobID());
}

void CAutoServerToolsView::OnAbort()
{
	if (AfxMessageBox(IDS_PDFLIB_REALLY_CANCEL, MB_YESNO) == IDNO)
		return;

	CAutoServerJobsView* pView = theApp.GetAutoServerJobsView();
	if ( ! pView)
		return;

	theApp.m_clientSocket.CommandToServer(CMD_ABORT_JOB, pView->GetActJobID());
}

void CAutoServerToolsView::OnUpdateSelectView(CCmdUI* pCmdUI)
{
	if ( ! pCmdUI->m_pOther)
		return;
	COwnerDrawnButton* pButton = (COwnerDrawnButton*)CWnd::FromHandle(pCmdUI->m_pOther->m_hWnd);
	if ( ! pButton)
		return;
	CAutoServerFrame* pFrame = theApp.GetAutoServerFrame();
	if ( ! pFrame)
		return;

	BOOL bChecked = FALSE;
	switch (pFrame->m_nCurrentView)
	{
	case CAutoServerFrame::ViewJobs:			(pButton->m_nIndex == 0) ? bChecked = TRUE : FALSE; break;
	case CAutoServerFrame::ViewLogs:			(pButton->m_nIndex == 1) ? bChecked = TRUE : FALSE; break;
	case CAutoServerFrame::ViewWorkflows:		(pButton->m_nIndex == 2) ? bChecked = TRUE : FALSE; break;
	}

	COwnerDrawnButtonList::SetCheck(pCmdUI, bChecked);
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CAutoServerToolsView::OnUpdateNew(CCmdUI* pCmdUI)
{
	CAutoServerFrame* pFrame = theApp.GetAutoServerFrame();
	int nShow = (pFrame) ? ( (pFrame->m_nCurrentView == CAutoServerFrame::ViewWorkflows) ? SW_SHOW : SW_HIDE) : SW_HIDE;
	COwnerDrawnButtonList::Show(pCmdUI, nShow);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, TRUE);
}

void CAutoServerToolsView::OnUpdateRemove(CCmdUI* pCmdUI)
{
	BOOL bEnable = FALSE;

	int nShow = SW_SHOW;
	CAutoServerFrame* pFrame = theApp.GetAutoServerFrame();
	if (pFrame)
	{
		switch (pFrame->m_nCurrentView)
		{
		case CAutoServerFrame::ViewJobs:			
			{
				CAutoServerJobsView* pView = theApp.GetAutoServerJobsView();
				if (pView)
					bEnable = (pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerJobsView, Job))) ? TRUE : FALSE;		
			}
			break;
		case CAutoServerFrame::ViewLogs:
			{
				CAutoServerLogsView* pView = theApp.GetAutoServerLogsView();
				if (pView)
					bEnable = (pView->m_DisplayList.GetFirstItem(DISPLAY_ITEM_TYPEID(CAutoServerLogsView, MessageLog))) ? TRUE : FALSE;		
			}
			break;
		case CAutoServerFrame::ViewWorkflows:	
			{
				CAutoServerWorkflowsView* pView = theApp.GetAutoServerWorkflowsView();
				if (pView)
					bEnable = (pView->m_DisplayList.GetFirstSelectedItem(DISPLAY_ITEM_TYPEID(CAutoServerWorkflowsView, Workflow))) ? TRUE : FALSE;		
			}
			break;
		}
	}
	
	COwnerDrawnButtonList::Show(pCmdUI, nShow);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);
	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
}

void CAutoServerToolsView::OnUpdateStartOutput(CCmdUI* pCmdUI)
{
	CAutoServerFrame* pFrame = theApp.GetAutoServerFrame();
	int nShow = (pFrame) ? ( (pFrame->m_nCurrentView == CAutoServerFrame::ViewJobs) ? SW_SHOW : SW_HIDE) : SW_HIDE;
	COwnerDrawnButtonList::Show(pCmdUI, nShow);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);

	BOOL bEnable = FALSE;
	CAutoServerDetailsView* pDetailsView = theApp.GetAutoServerDetailsView();
	if (pDetailsView) 
	{
		int nStatus = GetActJobStatus();
		if (nStatus != -2)
			bEnable = ( (nStatus == kimAutoDB::jobUndefined) ||  (nStatus == kimAutoDB::jobIdle) || (nStatus == kimAutoDB::jobAborted) ) ? TRUE : FALSE;
	}

	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
}

void CAutoServerToolsView::OnUpdateRestartJob(CCmdUI* pCmdUI)
{
	CAutoServerFrame* pFrame = theApp.GetAutoServerFrame();
	int nShow = (pFrame) ? ( (pFrame->m_nCurrentView == CAutoServerFrame::ViewJobs) ? SW_SHOW : SW_HIDE) : SW_HIDE;
	COwnerDrawnButtonList::Show(pCmdUI, nShow);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);

	BOOL bEnable = FALSE;
	CAutoServerDetailsView* pDetailsView = theApp.GetAutoServerDetailsView();
	if (pDetailsView) 
	{
		int nStatus = GetActJobStatus();
		if (nStatus != -2)
			bEnable = (nStatus & kimAutoDB::jobBusy) ? FALSE : TRUE;
	}

	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
}

void CAutoServerToolsView::OnUpdateAbort(CCmdUI* pCmdUI)
{
	CAutoServerFrame* pFrame = theApp.GetAutoServerFrame();
	int nShow = (pFrame) ? ( (pFrame->m_nCurrentView == CAutoServerFrame::ViewJobs) ? SW_SHOW : SW_HIDE) : SW_HIDE;
	COwnerDrawnButtonList::Show(pCmdUI, nShow);
	COwnerDrawnButtonList::SetCheck(pCmdUI, FALSE);

	BOOL bEnable = FALSE;
	CAutoServerDetailsView* pDetailsView = theApp.GetAutoServerDetailsView();
	if (pDetailsView) 
	{
		int nStatus = GetActJobStatus();
		if (nStatus != -2)
			bEnable = (nStatus & kimAutoDB::jobBusy) ? TRUE : FALSE;
	}

	COwnerDrawnButtonList::Enable(pCmdUI, bEnable);
}
