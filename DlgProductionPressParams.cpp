// DlgProductionPressParams.cpp: Implementierungsdatei
//

#include "stdafx.h"
#include "Imposition Manager.h"
#include "DlgProductionPressParams.h"
#include "DlgSheet.h"
#include "DlgPressDevice.h"
#include "InplaceEdit.h"
#include "InplaceButton.h"
#include "PrintSheetView.h"
#include "DlgProductionManager.h"
#include "LayoutDataWindow.h"
#include "GraphicComponent.h"


// CDlgProductionPressParams-Dialogfeld

IMPLEMENT_DYNAMIC(CDlgProductionPressParams, CDialog)

CDlgProductionPressParams::CDlgProductionPressParams(CWnd* pParent)
	: CDialog(CDlgProductionPressParams::IDD, pParent)
{
	m_nPaperGrain = 0;
	m_nRefEdge = 0;
	m_nNumColorStationsFront = 0;
	m_nNumColorStationsBack = 0;
	m_pParentWnd = pParent;
	m_bLocked = FALSE;
	m_ptInitialPos = CPoint(0,0);
	m_strMarkset = _T("");
}

CDlgProductionPressParams::~CDlgProductionPressParams()
{
}

int CDlgProductionPressParams::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		SetParent(m_pParentWnd);
		ModifyStyle(WS_OVERLAPPEDWINDOW | WS_POPUPWINDOW, WS_CHILD);
		ModifyStyleEx(WS_EX_DLGMODALFRAME, 0);
	}

	return 0;
}

BOOL CDlgProductionPressParams::Create(UINT nIDTemplate, CWnd* pParentWnd)
{
	m_pParentWnd = pParentWnd;
	return CDialog::Create(nIDTemplate, pParentWnd);
}

BOOL CDlgProductionPressParams::OnEraseBkgnd(CDC* pDC)
{
	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		return CDialog::OnEraseBkgnd(pDC);

	CRect rcFrame;
	GetClientRect(rcFrame);

	CGraphicComponent gc;
	gc.DrawPopupWindowBackground(pDC, rcFrame);

	return TRUE;
}

void CDlgProductionPressParams::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	if (nState == WA_INACTIVE)
	{
		if ( ! m_bLocked)
		{
			if (m_hWnd)
			{
				if ( ! m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
				{
					m_bLocked = TRUE;
					EndModalLoop(IDCANCEL);
					DestroyWindow();
				}
			}
		}
	}
	//else
	//	m_bLocked = FALSE;	// if DlgSheet is opened lock will be removed. If then AfxMessageBox() opened in DlgSheet this wnd will be destroyed which causes a crash -> so don't reset lock here
}

void CDlgProductionPressParams::OnBnClickedOk()
{
	SaveData();
	EndModalLoop(IDOK);
	m_bLocked = TRUE;
	DestroyWindow();
}

CPressParams& CDlgProductionPressParams::GetPressParams()
{
	static CPressParams nullPressParams;

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
	{
		CDlgProductionManager*	pParent	= (CDlgProductionManager*)m_pParentWnd;
		return pParent->m_productionProfile.m_press;
	}
	else
		if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CLayoutDataWindow)))
		{
			CLayoutDataWindow*	pParent	= (CLayoutDataWindow*)m_pParentWnd;
			return pParent->GetPrintingProfile().m_press;
		}

	return nullPressParams;
}

void CDlgProductionPressParams::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PD_PRESSDEVICE_COMBO, m_pressDeviceCombo);
	DDX_CBIndex(pDX, IDC_PD_WORKSTYLE_COMBO, m_nWorkStyle);
	DDX_CBIndex(pDX, IDC_PD_TURNINGTYPE_COMBO, m_nTurnStyle);
	DDX_Text(pDX, IDC_PD_PLATEFORMAT, m_strPlateFormat);
	DDX_Control(pDX, IDC_PD_PAPER_COMBO, m_paperCombo);
	DDX_CBIndex(pDX, IDC_PD_PAPERGRAIN_COMBO, m_nPaperGrain);
	DDX_CBIndex(pDX, IDC_PD_REFEDGE_COMBO, m_nRefEdge);
	DDX_CBIndex(pDX, IDC_COLORSTATIONS_FRONT_COMBO, m_nNumColorStationsFront);
	DDX_CBIndex(pDX, IDC_COLORSTATIONS_BACK_COMBO, m_nNumColorStationsBack);
	DDX_Text(pDX, IDC_PD_PRESSMARKSET, m_strMarkset);
}


BEGIN_MESSAGE_MAP(CDlgProductionPressParams, CDialog)
	ON_WM_ERASEBKGND()
	ON_WM_CREATE()
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_WM_CTLCOLOR()
	ON_CBN_SELCHANGE(IDC_PD_PRESSDEVICE_COMBO, OnCbnSelchangePdPressdeviceCombo)
	ON_BN_CLICKED(IDC_PD_PRESSDEVICE_BUTTON, OnBnClickedPdPressdeviceButton)
	ON_CBN_SELCHANGE(IDC_PD_PAPER_COMBO, OnCbnSelchangePdPaperCombo)
	ON_BN_CLICKED(IDC_PD_ADD_PAPER_BUTTON, OnBnClickedPdAddPaperButton)
	ON_BN_CLICKED(IDC_PD_REMOVE_PAPER_BUTTON, OnBnClickedPdRemovePaperButton)
	ON_CBN_SELCHANGE(IDC_PD_WORKSTYLE_COMBO, OnCbnSelchangePdWorkstyleCombo)
	ON_CBN_SELCHANGE(IDC_PD_PAPERGRAIN_COMBO, OnCbnSelchangePdPaperGrainCombo)
	ON_BN_CLICKED(IDC_PD_BROWSE_MARKSET, OnBnClickedPdBrowseMarkset)
	ON_BN_CLICKED(IDC_PD_REMOVE_MARKSET, OnBnClickedFpRemoveMarkset)
END_MESSAGE_MAP()



// CDlgProductionPressParams-Meldungshandler

BOOL CDlgProductionPressParams::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_ptInitialPos != CPoint(0,0))
	{
		CSize szScreen = CSize(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
		CRect rcDlg; GetWindowRect(rcDlg);
		if ( (m_ptInitialPos.y + rcDlg.Height()) <= szScreen.cy)
			SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
		else
			SetWindowPos(NULL, m_ptInitialPos.x, m_ptInitialPos.y - rcDlg.Height() - 12, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
	}

	if (m_pParentWnd->IsKindOf(RUNTIME_CLASS(CDlgProductionManager)))
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);

// refedge combo ////////////////////////////
	CBitmap bitmap;
	CComboBoxEx* pBox = (CComboBoxEx*)GetDlgItem (IDC_PD_REFEDGE_COMBO);
	m_refEdgeImageList.Create(23, 17, ILC_COLOR8, 4, 0);	// List not to grow
	bitmap.LoadBitmap(IDB_REFEDGE_BL);
	m_refEdgeImageList.Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	bitmap.LoadBitmap(IDB_REFEDGE_TL);
	m_refEdgeImageList.Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	bitmap.LoadBitmap(IDB_REFEDGE_TR);
	m_refEdgeImageList.Add(&bitmap, BLACK);
	bitmap.DeleteObject();
	bitmap.LoadBitmap(IDB_REFEDGE_BR);
	m_refEdgeImageList.Add(&bitmap, BLACK);
	bitmap.DeleteObject();

	pBox->SetImageList(&m_refEdgeImageList);

	CString string, strTop, strBottom, strLeft, strRight;
	strTop.LoadString(IDS_TOP), strBottom.LoadString(IDS_BOTTOM), strLeft.LoadString(IDS_LEFT); strRight.LoadString(IDS_RIGHT);
	COMBOBOXEXITEM item;
	item.mask			= CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;// | CBEIF_LPARAM;
	item.iItem			= 0; 
	string.Format(_T("%s %s"), strLeft, strBottom);
	item.pszText		= string.GetBuffer(MAX_TEXT);
	item.cchTextMax		= MAX_TEXT;
	item.iImage			= 0;
	item.iSelectedImage = 0;
	//item.iIndent		= 0;
	pBox->InsertItem(&item);
	item.iItem			= 1; 
	string.Format(_T("%s %s"), strLeft, strTop);
	item.pszText		= string.GetBuffer(MAX_TEXT);
	item.iImage			= 1;
	item.iSelectedImage = 1;
	pBox->InsertItem(&item);
	item.iItem			= 2; 
	string.Format(_T("%s %s"), strRight, strTop);
	item.pszText		= string.GetBuffer(MAX_TEXT);
	item.iImage			= 2;
	item.iSelectedImage = 2;
	pBox->InsertItem(&item);
	item.iItem			= 3; 
	string.Format(_T("%s %s"), strRight, strBottom);
	item.pszText		= string.GetBuffer(MAX_TEXT);
	item.iImage			= 3;
	item.iSelectedImage = 3;
	pBox->InsertItem(&item);
////////////////////////////////////////////////////

	InitData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// AUSNAHME: OCX-Eigenschaftenseite muss FALSE zur�ckgeben.
}

void CDlgProductionPressParams::InitData()
{
	CPressParams& rPressParams = GetPressParams();

	m_strPressDevice = rPressParams.m_pressDevice.m_strName;
	m_strPaper		 = rPressParams.m_sheetData.m_strSheetName;
	m_nPaperGrain	 = (rPressParams.m_sheetData.m_nPaperGrain == GRAIN_HORIZONTAL) ? 0 : 1;
	m_strMarkset	 = rPressParams.m_strMarkset;

	////// press params
	m_pressDeviceCombo.ResetContent();
	POSITION pos = theApp.m_PressDeviceList.GetHeadPosition();
	while (pos) 
	{
		CPressDevice& rPressDevice = theApp.m_PressDeviceList.GetNext(pos);
		m_pressDeviceCombo.AddString(rPressDevice.m_strName);
	}
	int nIndex = m_pressDeviceCombo.FindStringExact(-1, m_strPressDevice);
	m_pressDeviceCombo.SetCurSel(nIndex);
	if (nIndex == CB_ERR)
		if ( ! m_strPressDevice.IsEmpty())
		{
			m_pressDeviceCombo.AddString(m_strPressDevice);
			m_pressDeviceCombo.SelectString(0, m_strPressDevice);
		}
	m_strPlateFormat.Format(_T("%.1f x %.1f"), rPressParams.m_pressDevice.m_fPlateWidth, rPressParams.m_pressDevice.m_fPlateHeight);

	switch (rPressParams.m_nWorkStyle)
	{
	case WORK_AND_BACK:		m_nWorkStyle = 0;	break;
	case WORK_AND_TURN	:	m_nWorkStyle = 1;	break;
	case WORK_AND_TUMBLE:	m_nWorkStyle = 2;	break;
	case WORK_SINGLE_SIDE:	m_nWorkStyle = 3;	break;
	default:				m_nWorkStyle = 0;	break;
	}

	GetDlgItem(IDC_PD_TURNINGTYPE_STATIC)->ShowWindow((m_nWorkStyle == 0) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_PD_TURNINGTYPE_COMBO )->ShowWindow((m_nWorkStyle == 0) ? SW_SHOW : SW_HIDE);

	m_nTurnStyle = rPressParams.m_nTurnStyle;

	switch (rPressParams.m_nRefEdge)
	{
	case LEFT_BOTTOM:	m_nRefEdge = 0;	break;
	case LEFT_TOP:		m_nRefEdge = 1;	break;
	case RIGHT_TOP:		m_nRefEdge = 2;	break;
	case RIGHT_BOTTOM:	m_nRefEdge = 3;	break;
	}

	m_nNumColorStationsFront = rPressParams.m_nNumColorStationsFront;
	m_nNumColorStationsBack  = rPressParams.m_nNumColorStationsBack;

	m_paperCombo.ResetContent();
	for (int i = 0; i < rPressParams.m_availableSheets.GetSize(); i++)
	{
		m_paperCombo.AddString(rPressParams.m_availableSheets[i].m_strSheetName);
	}

	nIndex = m_paperCombo.FindStringExact(-1, m_strPaper);
	m_paperCombo.SetCurSel(nIndex);
	if (nIndex == CB_ERR)
	{
		m_strPaper	  = _T("");
		m_nPaperGrain = 1; //GRAIN_VERTICAL;
		rPressParams.m_sheetData = CSheet();
		//if ( ! m_strPaper.IsEmpty())
		//{
		//	m_paperCombo.AddString(m_strPaper);
		//	m_paperCombo.SelectString(0, m_strPaper);
		//}
	}

	UpdateData(FALSE);
}

void CDlgProductionPressParams::SaveData()
{
	UpdateData(TRUE);

	CPressParams& rPressParams = GetPressParams();

	rPressParams.m_pressDevice.m_strName = m_strPressDevice;
	switch(m_nWorkStyle)
	{
	case 0:		rPressParams.m_nWorkStyle = WORK_AND_BACK;		break;
	case 1:		rPressParams.m_nWorkStyle = WORK_AND_TURN;		break;
	case 2:		rPressParams.m_nWorkStyle = WORK_AND_TUMBLE;	break;
	case 3:		rPressParams.m_nWorkStyle = WORK_SINGLE_SIDE;	break;
	default:	rPressParams.m_nWorkStyle = WORK_AND_BACK;		break;
	}

	rPressParams.m_nTurnStyle = m_nTurnStyle;

	switch (m_nRefEdge)
	{
	case 0:	rPressParams.m_nRefEdge = LEFT_BOTTOM;	break;
	case 1:	rPressParams.m_nRefEdge = LEFT_TOP;		break;
	case 2:	rPressParams.m_nRefEdge = RIGHT_TOP;	break;
	case 3:	rPressParams.m_nRefEdge = RIGHT_BOTTOM;	break;
	}

	rPressParams.m_nNumColorStationsFront = m_nNumColorStationsFront;
	rPressParams.m_nNumColorStationsBack  = m_nNumColorStationsBack;

	rPressParams.m_sheetData.m_strSheetName	= m_strPaper;
	rPressParams.m_sheetData.m_nPaperGrain	= (m_nPaperGrain == 0) ? GRAIN_HORIZONTAL : GRAIN_VERTICAL;
	rPressParams.m_strMarkset				= m_strMarkset;

	int nSel = m_paperCombo.GetCurSel();
	if ( (nSel >= 0) && (nSel < rPressParams.m_availableSheets.GetSize()) )
		rPressParams.m_availableSheets[nSel] = rPressParams.m_sheetData;
}

void CDlgProductionPressParams::OnCbnSelchangePdPressdeviceCombo()
{
	UpdateData(TRUE);

	int nSel = m_pressDeviceCombo.GetCurSel();
	if (nSel == CB_ERR)
		return;

	CString strName;
	m_pressDeviceCombo.GetLBText(nSel, strName);
	CPressDevice* pPressDevice = theApp.m_PressDeviceList.GetDevice(strName);
	if (pPressDevice)
	{
		CPressParams& rPressParams = GetPressParams();
	
		rPressParams.m_pressDevice			= *pPressDevice;
		rPressParams.m_backSidePressDevice	= *pPressDevice;

		m_strPressDevice = pPressDevice->m_strName;
		m_strPlateFormat.Format(_T("%.1f x %.1f"), pPressDevice->m_fPlateWidth, pPressDevice->m_fPlateHeight);


		CSheet sheet;
		if (pPressDevice->FindLargestPaper(sheet))
		{
			rPressParams.m_sheetData = sheet;
			sheet.m_nPaperGrain  = GRAIN_VERTICAL;		// initialize always with vertical
			rPressParams.m_availableSheets.RemoveAll();
			rPressParams.m_availableSheets.Add(sheet);

			m_paperCombo.ResetContent();
			m_paperCombo.AddString(sheet.m_strSheetName);

			m_strPaper	  = sheet.m_strSheetName;
			m_nPaperGrain = (sheet.m_nPaperGrain == GRAIN_HORIZONTAL) ? 0 : 1;

			int nIndex = m_paperCombo.FindStringExact(-1, m_strPaper);
			m_paperCombo.SetCurSel(nIndex);
		}
	}
	else
		m_strPlateFormat = _T("");

	UpdateData(FALSE);
}

void CDlgProductionPressParams::OnBnClickedPdPressdeviceButton()
{
	UpdateData(TRUE);

	CPressDevice* pPressDevice = NULL;
	int nSel = m_pressDeviceCombo.GetCurSel();
	if (nSel != CB_ERR)
	{
		CString strName;
		m_pressDeviceCombo.GetLBText(nSel, strName);
		pPressDevice = theApp.m_PressDeviceList.GetDevice(strName);
	}

	m_bLocked = TRUE;

	CDlgPressDevice dlg(FALSE, FRONTSIDE, pPressDevice, NULL);  // don't modify direct inside dialog
	if (pPressDevice)
		dlg.m_pressDevice = *pPressDevice;
	int nRet = dlg.DoModal();
	
	m_bLocked = FALSE;

	if (nRet == IDCANCEL)
		return;

	GetPressParams().m_pressDevice			= dlg.m_pressDevice;
	GetPressParams().m_backSidePressDevice	= dlg.m_pressDevice;

	m_pressDeviceCombo.ResetContent();
	POSITION pos = theApp.m_PressDeviceList.GetHeadPosition();
	while (pos) 
	{
		CPressDevice& rPressDevice = theApp.m_PressDeviceList.GetNext(pos);
		m_pressDeviceCombo.AddString(rPressDevice.m_strName);
	}

	m_strPressDevice = dlg.m_pressDevice.m_strName;
	m_strPlateFormat.Format(_T("%.1f x %.1f"), dlg.m_pressDevice.m_fPlateWidth, dlg.m_pressDevice.m_fPlateHeight);

	int nIndex = m_pressDeviceCombo.FindStringExact(-1, m_strPressDevice);
	m_pressDeviceCombo.SetCurSel(nIndex);

	UpdateData(FALSE);
}

void CDlgProductionPressParams::OnCbnSelchangePdPaperCombo()
{
	UpdateData(TRUE);

	CPressParams& rPressParams = GetPressParams();

	int	nSel = m_paperCombo.GetCurSel();
	if ( (nSel < 0) || (nSel >= rPressParams.m_availableSheets.GetSize()) )
	{
		m_strPaper	  = _T("");
		m_nPaperGrain = GRAIN_HORIZONTAL;
		rPressParams.m_sheetData = CSheet();
		return;
	}

	rPressParams.m_sheetData = rPressParams.m_availableSheets[nSel];
	m_strPaper				 = rPressParams.m_sheetData.m_strSheetName;
	m_nPaperGrain			 = (rPressParams.m_sheetData.m_nPaperGrain == GRAIN_HORIZONTAL) ? 0 : 1;

	UpdateData(FALSE);
}

void CDlgProductionPressParams::OnBnClickedPdAddPaperButton()
{
	UpdateData(TRUE);

	CDlgSheet dlgSheet(FALSE);  // don't modify direct inside dialog
	dlgSheet.LoadSheets();

	int	nSel = m_paperCombo.GetCurSel();
	if (nSel != CB_ERR)
	{
		CString strName;
		m_paperCombo.GetLBText(nSel, strName);
		POSITION pos = dlgSheet.m_sheetList.FindByName(strName);
		if (pos)
		{
			CSheet& rSheet = dlgSheet.m_sheetList.GetAt(pos);
			dlgSheet.m_strName = rSheet.m_strSheetName;
			dlgSheet.m_fWidth  = rSheet.m_fWidth;
			dlgSheet.m_fHeight = rSheet.m_fHeight;
		}
	}

	m_bLocked = TRUE;
	int nRet = dlgSheet.DoModal();
	m_bLocked = FALSE;

	if (nRet == IDCANCEL)
		return;

	CSheet sheet;
	sheet.m_strSheetName = dlgSheet.m_strName;
	sheet.m_fWidth		 = dlgSheet.m_fWidth;
	sheet.m_fHeight		 = dlgSheet.m_fHeight;
	sheet.m_nPaperGrain  = GRAIN_VERTICAL;		// initialize always with vertical
	sheet.m_FanoutList	 = dlgSheet.m_FanoutList;

	GetPressParams().m_sheetData = sheet;
	m_nPaperGrain = (sheet.m_nPaperGrain == GRAIN_HORIZONTAL) ? 0 : 1;

	CPressParams& rPressParams = GetPressParams();
	BOOL bFound = FALSE;
	for (int i = 0; i < rPressParams.m_availableSheets.GetSize(); i++)
	{
		if (rPressParams.m_availableSheets[i].m_strSheetName == dlgSheet.m_strName)
			bFound = TRUE;
	}
	if ( ! bFound)
	{
		rPressParams.m_availableSheets.Add(sheet);
		m_paperCombo.AddString(sheet.m_strSheetName);
	}

	m_strPaper = sheet.m_strSheetName;

	int nIndex = m_paperCombo.FindStringExact(-1, m_strPaper);
	m_paperCombo.SetCurSel(nIndex);

	UpdateData(FALSE);
}

void CDlgProductionPressParams::OnBnClickedPdRemovePaperButton()
{
	UpdateData(TRUE);

	int nSel = m_paperCombo.GetCurSel();
	if (nSel == CB_ERR)
		return;

	m_paperCombo.DeleteString(nSel);

	CPressParams& rPressParams = GetPressParams();
	if (rPressParams.m_availableSheets.GetSize() > 0)
		rPressParams.m_availableSheets.RemoveAt(nSel);
	if (nSel >= m_paperCombo.GetCount())
		nSel--;
	m_paperCombo.SetCurSel(nSel);
	OnCbnSelchangePdPaperCombo();
}

void CDlgProductionPressParams::OnCbnSelchangePdWorkstyleCombo()
{
	UpdateData(TRUE);
	GetDlgItem(IDC_PD_TURNINGTYPE_STATIC)->ShowWindow((m_nWorkStyle == 0) ? SW_SHOW : SW_HIDE);
	GetDlgItem(IDC_PD_TURNINGTYPE_COMBO )->ShowWindow((m_nWorkStyle == 0) ? SW_SHOW : SW_HIDE);
}

void CDlgProductionPressParams::OnCbnSelchangePdPaperGrainCombo()
{
	UpdateData(TRUE);

	CPressParams& rPressParams = GetPressParams();
	rPressParams.m_sheetData.m_nPaperGrain = (m_nPaperGrain == 0) ? GRAIN_HORIZONTAL : GRAIN_VERTICAL;

	int	nSel = m_paperCombo.GetCurSel();
	if ( (nSel >= 0) && (nSel < rPressParams.m_availableSheets.GetSize()) )
		rPressParams.m_availableSheets[nSel].m_nPaperGrain = (m_nPaperGrain == 0) ? GRAIN_HORIZONTAL : GRAIN_VERTICAL;
}

void CDlgProductionPressParams::OnBnClickedPdBrowseMarkset()
{
	UpdateData(TRUE);

	CString strMarksDir		= CString(theApp.GetDataFolder()) + _T("\\PDF-Marksets");
	CString strRelativePath	= m_strMarkset;	
	strRelativePath.TrimLeft('.');
	CString strMarkset = PathFindFileName(m_strMarkset);
	PathRemoveExtension(strMarkset.GetBuffer(MAX_PATH));

	m_pDlgMarkSet = new CDlgMarkSet(this, TRUE);
	m_pDlgMarkSet->m_bBrowseOnly	    = TRUE;
	m_pDlgMarkSet->m_strMarksetFullPath = strMarksDir + strRelativePath;
	PathRemoveFileSpec(strRelativePath.GetBuffer(MAX_PATH));
	//m_pDlgMarkSet->m_strCurrentFolder	= strMarksDir + strRelativePath;
	m_pDlgMarkSet->m_strCurrentMarkset  = strMarkset;

	if (m_pDlgMarkSet->DoModal() == IDOK)
	{
		m_strMarkset = m_pDlgMarkSet->m_strMarksetFullPath;

		PathRelativePathTo(strRelativePath.GetBuffer(MAX_PATH), (LPCTSTR)strMarksDir, FILE_ATTRIBUTE_DIRECTORY, (LPCTSTR)m_strMarkset, FILE_ATTRIBUTE_DIRECTORY);
		strRelativePath.ReleaseBuffer();
		m_strMarkset = strRelativePath;

		CString strOut;
		strOut.Format(_T("  %s"), strRelativePath);
		CRect staticRect;
		GetDlgItem(IDC_PD_PRESSMARKSET)->GetWindowRect(staticRect);
		CClientDC clientDC(this);
		PathCompactPath(clientDC.m_hDC, strOut.GetBuffer(MAX_PATH), staticRect.Width());
		UpdateData(FALSE);
	}

	delete m_pDlgMarkSet;
}

void CDlgProductionPressParams::OnBnClickedFpRemoveMarkset()
{
	m_strMarkset = _T("");

	UpdateData(FALSE);
}
