#if !defined(AFX_DLGFOLDMARKCONTENT_H__20269EE1_5EF5_4404_BFAC_B6B91F75DE08__INCLUDED_)
#define AFX_DLGFOLDMARKCONTENT_H__20269EE1_5EF5_4404_BFAC_B6B91F75DE08__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgFoldMarkContent.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDlgFoldMarkContent 

class CDlgFoldMarkContent : public CDialog
{
// Konstruktion
public:
	CDlgFoldMarkContent(CWnd* pParent = NULL);   // Standardkonstruktor
	~CDlgFoldMarkContent();

// Dialogfelddaten
	//{{AFX_DATA(CDlgFoldMarkContent)
	enum { IDD = IDD_FOLDMARK_CONTENT };
	CComboBox	m_linetypeCombo;
	CComboBox	m_backgroundCombo;
	//}}AFX_DATA

public:
	class CDlgLayoutObjectControl*	m_pParent;
	CDlgSystemGenMarkColorControl	m_colorControl;
	CBrush							m_hollowBrush;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDlgFoldMarkContent)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
public:
	void LoadData();
	void SaveData();
	void Apply();

protected:

	DECLARE_MESSAGE_MAP()
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDlgFoldMarkContent)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnMarkForeground();
	//}}AFX_MSG
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DLGFOLDMARKCONTENT_H__20269EE1_5EF5_4404_BFAC_B6B91F75DE08__INCLUDED_
