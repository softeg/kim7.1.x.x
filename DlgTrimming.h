#pragma once


#include "OwnerDrawnButtonList.h"



// DlgTrimming.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgTrimming dialog

class CDlgTrimming : public CDialog
{
// Construction
public:
	CDlgTrimming(CWnd* pParent = NULL);   // standard constructor
	~CDlgTrimming();

// Dialog Data
	//{{AFX_DATA(CDlgTrimming)
	enum { IDD = IDD_TRIMMING };
	float	m_fTrimWidth;
	float	m_fTrim1;
	float	m_fTrim2;
	CSpinButtonCtrl	m_trimWidthSpin;
	CSpinButtonCtrl	m_trim1Spin;
	CSpinButtonCtrl	m_trim2Spin;
	//}}AFX_DATA

	BOOL m_bLock;
	float m_fOldTrim1, m_fOldTrim2;
	CPoint	m_point;
	COwnerDrawnButtonList m_trimToolbarButtons;

public:
	void InitData();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgTrimming)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgTrimming)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposTrimWidthSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposTrim1Spin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposTrim2Spin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateTrimWidth();
	afx_msg void OnUpdateTrim1();
	afx_msg void OnUpdateTrim2();
	afx_msg void OnTrim1();
	afx_msg void OnTrim2();
	afx_msg void OnTrim3();
	afx_msg void OnTrim4();
	afx_msg void OnTrim5();
	afx_msg void OnTrim6();
	afx_msg void OnTrim7();
	afx_msg void OnTrim8();
	afx_msg void OnUpdateUITrim1(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUITrim2(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUITrim3(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUITrim4(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUITrim5(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUITrim6(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUITrim7(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUITrim8(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnCancel();
};
