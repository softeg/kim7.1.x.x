// DlgNumeration.cpp : implementation file
//

#include "stdafx.h"
#include "imposition manager.h"
#include "DlgNumeration.h"
#include "InplaceEdit.h"
#include "ProductsView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgNumeration dialog


CDlgNumeration::CDlgNumeration(CWnd* pParent /*=NULL*/, BOOL bRunModal /*= FALSE*/)
	: CDialog(CDlgNumeration::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgNumeration)
	m_bRunModal = bRunModal;
	m_nStartNum = 1;
	m_nRangeTo = 1;
	m_nRangeFrom = 1;
	m_nSelection = -1;
	m_strNumerationStyle = _T("$n");
	m_bReverse = FALSE;
	//}}AFX_DATA_INIT

	m_nProductPartIndex = 0;
	m_nRangeToMax	    = 0;
	m_pfnApplyCallback  = NULL;
	m_pfnInitCallback   = NULL;
	m_pViewToUpdate1    = NULL;
	m_pViewToUpdate2    = NULL;
}

CDlgNumeration::~CDlgNumeration()
{
	m_pViewToUpdate1 = NULL;
	m_pViewToUpdate2 = NULL;
	if ( ! m_bRunModal)
		DestroyWindow();
}

BOOL CDlgNumeration::DestroyWindow() 
{
	BOOL bRet = CDialog::DestroyWindow();

	if (m_pViewToUpdate1)
	{
		if (m_pViewToUpdate1->IsKindOf(RUNTIME_CLASS(CProductsView)))
			((CProductsView*)m_pViewToUpdate1)->OnUpdate(NULL, 0, NULL);

		m_pViewToUpdate1->Invalidate();
		m_pViewToUpdate1->UpdateWindow();
	}
	m_pViewToUpdate1 = NULL;
	m_pViewToUpdate2 = NULL;

	return bRet;
}


void CDlgNumeration::DoDataExchange(CDataExchange* pDX) 
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgNumeration)
	DDX_Control(pDX, IDC_SPIN_STARTPAGE, m_SpinStartPage);
	DDX_Text(pDX, IDC_EDIT_STARTPAGE, m_nStartNum);
	DDX_Text(pDX, IDC_RANGE_TO, m_nRangeTo);
	DDV_MinMaxUInt(pDX, m_nRangeTo, 1, 9999);
	DDX_Text(pDX, IDC_RANGE_FROM, m_nRangeFrom);
	DDV_MinMaxUInt(pDX, m_nRangeFrom, 1, 9999);
	DDX_Radio(pDX, IDC_SELECT_MARKED, m_nSelection);
	DDX_Text(pDX, IDC_NUMERATION_STYLE, m_strNumerationStyle);
	DDX_Check(pDX, IDC_NUMERATE_REVERSE, m_bReverse);
	DDX_Control(pDX, IDC_SELECT_PRODUCTGROUP_COMBO, m_productGroupCombo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgNumeration, CDialog)
	//{{AFX_MSG_MAP(CDlgNumeration)
	ON_BN_CLICKED(IDC_APPLY_BUTTON, OnApplyButton)
	ON_EN_SETFOCUS(IDC_RANGE_FROM, OnSetfocusRangeFrom)
	ON_EN_SETFOCUS(IDC_RANGE_TO, OnSetfocusRangeTo)
	ON_EN_KILLFOCUS(IDC_RANGE_FROM, OnKillfocusRangeFrom)
	ON_EN_KILLFOCUS(IDC_RANGE_TO, OnKillfocusRangeTo)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_NUMERATE_REVERSE, OnNumerateReverse)
	ON_BN_CLICKED(IDC_NUMERATE_RESET, OnBnClickedNumerateReset)
	ON_BN_CLICKED(IDC_SELECT_ALL, OnSelectAll)
	ON_BN_CLICKED(IDC_SELECT_MARKED, OnSelectMarked)
	ON_BN_CLICKED(IDC_SELECT_RANGE, OnSelectRange)
	ON_BN_CLICKED(IDC_SELECT_PRODUCTGROUP, OnSelectProductGroup)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(IDC_SELECT_PRODUCTGROUP_COMBO, &CDlgNumeration::OnCbnSelchangeSelectProductpartCombo)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgNumeration message handlers

BOOL CDlgNumeration::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if ( ! m_strTitle.IsEmpty())
		SetWindowText(m_strTitle);
	
	m_SpinStartPage.SetRange(1, UD_MAXVAL);

	//m_nStartNum  = 1;
	if (m_nRangeFrom == 0)
		m_nRangeFrom = 1;
	if (m_nRangeTo == 0)
		m_nRangeTo = 1;
	m_nRangeToMax = m_nRangeTo;
	//m_nStartNum	  = (m_bReverse) ? m_nRangeTo : m_nRangeFrom;

	if (m_nSelection == -1)
		m_nSelection = SelectionAll;

	//if (m_strNumerationStyle.IsEmpty())
		m_strNumerationStyle = _T("$n");

	if (m_bRunModal)
	{
		GetDlgItem(IDC_SELECT_MARKED)->ShowWindow(SW_HIDE);
		GetDlgItem(IDOK)->SetWindowText(_T("OK"));
		GetDlgItem(IDC_APPLY_BUTTON)->ShowWindow(SW_HIDE);
	}

	CImpManDoc* pDoc = CImpManDoc::GetDoc();
	if (pDoc)
	{
		CBoundProduct& rBoundProduct = pDoc->GetBoundProductPart(m_nProductPartIndex).GetBoundProduct();
		for (int i = 0; i < rBoundProduct.m_parts.GetSize(); i++)
		{
			m_productGroupCombo.AddString(rBoundProduct.m_parts[i].m_strName);
		}
	}

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgNumeration::OnSelectAll() 
{
	UpdateData(TRUE);
	m_nRangeFrom = 1;
	m_nRangeTo	 = m_nRangeToMax;
	m_nStartNum  = (m_bReverse) ? m_nRangeTo : m_nRangeFrom;

	//if (m_pfnInitCallback)
	//	m_pfnInitCallback(this);

	UpdateData(FALSE);
	if (m_pViewToUpdate1)
	{
		m_pViewToUpdate1->Invalidate();
		m_pViewToUpdate1->UpdateWindow();
	}
}

void CDlgNumeration::OnSelectMarked() 
{
	UpdateData(TRUE);
	if (m_pfnInitCallback)
		m_pfnInitCallback(this);
	else
	{
		m_nRangeFrom = 1;
		m_nRangeTo	 = m_nRangeToMax;
		m_nStartNum  = (m_bReverse) ? m_nRangeTo : m_nRangeFrom;
	}
	UpdateData(FALSE);
	if (m_pViewToUpdate1)
	{
		m_pViewToUpdate1->Invalidate();
		m_pViewToUpdate1->UpdateWindow();
	}
}

void CDlgNumeration::OnSelectRange() 
{
	UpdateData(TRUE);
	m_nStartNum  = (m_bReverse) ? m_nRangeTo : m_nRangeFrom;
	//if (m_pfnInitCallback)
	//	m_pfnInitCallback(this);
	UpdateData(FALSE);
	if (m_pViewToUpdate1)
	{
		m_pViewToUpdate1->Invalidate();
		m_pViewToUpdate1->UpdateWindow();
	}
}

void CDlgNumeration::OnSelectProductGroup()
{
	UpdateData(TRUE);
	m_nProductPartIndex = m_productGroupCombo.GetCurSel();

	if (m_pfnInitCallback)
		m_pfnInitCallback(this);

	UpdateData(FALSE);
	//if (m_pViewToUpdate1)
	//{
	//	m_pViewToUpdate1->Invalidate();
	//	m_pViewToUpdate1->UpdateWindow();
	//}
}

void CDlgNumeration::OnCbnSelchangeSelectProductpartCombo()
{
	UpdateData(TRUE);
	m_nSelection = SelectionProductPart;
	UpdateData(FALSE);

	OnSelectProductGroup();
}

void CDlgNumeration::OnSetfocusRangeFrom() 
{
	UpdateData(TRUE);
	m_nSelection = SelectionRange;
	UpdateData(FALSE);
}

void CDlgNumeration::OnSetfocusRangeTo() 
{
	UpdateData(TRUE);
	m_nSelection = SelectionRange;
	UpdateData(FALSE);
}

void CDlgNumeration::OnKillfocusRangeFrom() 
{
	UpdateData(TRUE);
	m_nSelection = SelectionRange;
	UpdateData(FALSE);
	//OnApplyButton();
	//if (m_pfnInitCallback)
	//	m_pfnInitCallback(this);
	if (m_pViewToUpdate1)
	{
		m_pViewToUpdate1->Invalidate();
		m_pViewToUpdate1->UpdateWindow();
	}
}

void CDlgNumeration::OnKillfocusRangeTo() 
{
	UpdateData(TRUE);
	m_nSelection = SelectionRange;
	UpdateData(FALSE);
	//OnApplyButton();
	//if (m_pfnInitCallback)
	//	m_pfnInitCallback(this);
	if (m_pViewToUpdate1)
	{
		m_pViewToUpdate1->Invalidate();
		m_pViewToUpdate1->UpdateWindow();
	}
}

void CDlgNumeration::OnNumerateReverse() 
{
	UpdateData(TRUE);
	int nRangeFrom = m_nRangeFrom;
	int nRangeTo   = m_nRangeTo;
	if (m_pfnInitCallback)
		m_pfnInitCallback(this);
	m_nRangeFrom = nRangeFrom;
	m_nRangeTo   = nRangeTo;
	UpdateData(FALSE);
}

void CDlgNumeration::OnBnClickedNumerateReset()
{
	UpdateData(TRUE);

	if (m_pfnApplyCallback)
	{
		m_pfnApplyCallback(m_nSelection, m_nRangeFrom, m_nRangeTo, m_nStartNum, _T("reset"), m_bReverse, m_nProductPartIndex);
		m_strNumerationStyle = _T("$n");
	}
	if (m_pfnInitCallback)
		m_pfnInitCallback(this);

	UpdateData(FALSE);
}

void CDlgNumeration::OnApplyButton() 
{
	UpdateData(TRUE);

	if (m_pfnApplyCallback)
		m_pfnApplyCallback(m_nSelection, m_nRangeFrom, m_nRangeTo, m_nStartNum, m_strNumerationStyle, m_bReverse, m_nProductPartIndex);

	if (m_nSelection == SelectionRange)
		if (m_nRangeTo < m_nRangeToMax)
		{
			//m_nRangeFrom = m_nRangeTo + 1;
			//m_nRangeTo	 = m_nRangeToMax;
			//m_nStartNum	 = m_nRangeFrom;
			int nRangeFrom = m_nRangeFrom;
			int nRangeTo   = m_nRangeTo;
			if (m_pfnInitCallback)
				m_pfnInitCallback(this);
			m_nRangeFrom = nRangeFrom;
			m_nRangeTo   = nRangeTo;
			UpdateData(FALSE);
		}
}

void CDlgNumeration::OnOK() 
{
	if ( ! m_bRunModal)
		DestroyWindow();
	else
		CDialog::OnOK();
}

void CDlgNumeration::OnCancel() 
{
	if ( ! m_bRunModal)
		DestroyWindow();
	else
		CDialog::OnCancel();
}

CString CDlgNumeration::GenerateNumber(int nNumber, CString strNumerationStyle, int nNumerationOffset)
{
	if (strNumerationStyle.IsEmpty())
		strNumerationStyle = _T("$n");

	TCHAR *poi2, *poi = _tcschr(strNumerationStyle.GetBuffer(MAX_TEXT), '$');
	
	CString string;
	if (poi == NULL)
		string = strNumerationStyle;
	else
	{
		*poi = '\0';
		
		if ((*(poi + 1) == 'R') || (*(poi + 1) == 'r'))  // $r = roman
		{
			poi2 = poi + 2;
			CString strRoman = Arab2Roman(nNumber + nNumerationOffset);
			string.Format(_T("%s%s%s"), strNumerationStyle, strRoman, poi2);
			*poi = '$';
		}
		else
			if ((*(poi + 1) == 'N') || (*(poi + 1) == 'n'))  // $n = normal 
			{
				poi2 = poi + 2;
				string.Format(_T("%s%d%s"), strNumerationStyle, nNumber + nNumerationOffset, poi2);
				*poi = '$';
			}
			else
				if ((*(poi + 1) == 'A') || (*(poi + 1) == 'a'))  // $abc... = alternating 
				{
					poi2 = poi + 2;
					int nSeqLen = 1;
					while ( (*poi2 - 1 == *(poi2 - 1)) && (*poi2 != '\0') )
					{
						nSeqLen++;
						poi2++;
					}
					
					int nNum = nNumber + nNumerationOffset;
					TCHAR ch = *(poi + 1 + ((nNum + nSeqLen - 1)%nSeqLen));
					string.Format(_T("%s%d%c%s"), strNumerationStyle, (nNum + nSeqLen - 1)/nSeqLen, ch, poi2);
					*poi = '$';
				}
				else
				{
					*poi = '$';
					string = strNumerationStyle;
				}
	}

	strNumerationStyle.ReleaseBuffer();
	return string;
}

int CDlgNumeration::GetSeqLen(CString strNumerationStyle)
{
	strNumerationStyle.MakeLower();
	int nIndex = strNumerationStyle.Find(_T("$ab"));
	if (nIndex == -1)	// no alternating sequence found 
		return 1;

	TCHAR ch = 'c';
	int  nStartIndex = nIndex + 1;
	nIndex += 3;
	while (nIndex < strNumerationStyle.GetLength())
	{
		if (ch != strNumerationStyle[nIndex])
			break;
		ch++;
		nIndex++;
	}
	return nIndex - nStartIndex;
}

CString CDlgNumeration::Arab2Roman(int nNumber)
{
	CString string;
	if ((nNumber >= 10000) || (nNumber < 0))
	{
		string.Format(_T("%d"), nNumber);
		return string;
	}

	int		nNumer, nDenom;
	CString str1000, str100, str10, str1;
	
	nNumer = nNumber/1000;
	nDenom = nNumber%1000;
	
	switch (nNumer)
	{
    case 0: str1000 = _T("");     break;
    case 1: str1000 = _T("M");    break;
    case 2: str1000 = _T("MM");   break;
    case 3: str1000 = _T("MMM");  break;
    case 4: str1000 = _T("MV");   break;
    case 5: str1000 = _T("V");    break;
    case 6: str1000 = _T("VM");   break;
    case 7: str1000 = _T("VMM");  break;
    case 8: str1000 = _T("VMMM"); break;
    case 9: str1000 = _T("MX");   break;
	}
	
	nNumer = nDenom/100;
	nDenom = nDenom%100;
	
	switch (nNumer)
	{
    case 0: str100 = _T("");     break;
    case 1: str100 = _T("C");    break;
    case 2: str100 = _T("CC");   break;
    case 3: str100 = _T("CCC");  break;
    case 4: str100 = _T("CD");   break;
    case 5: str100 = _T("D");    break;
    case 6: str100 = _T("DC");   break;
    case 7: str100 = _T("DCC");  break;
    case 8: str100 = _T("DCCC"); break;
    case 9: str100 = _T("CM");   break;
	}
	
	nNumer = nDenom/10;
	nDenom = nDenom%10;
	
	switch (nNumer)
	{
    case 0: str10 = _T("");     break;
    case 1: str10 = _T("X");    break;
    case 2: str10 = _T("XX");   break;
    case 3: str10 = _T("XXX");  break;
    case 4: str10 = _T("XL");   break;
    case 5: str10 = _T("L");    break;
    case 6: str10 = _T("LX");   break;
    case 7: str10 = _T("LXX");  break;
    case 8: str10 = _T("LXXX"); break;
    case 9: str10 = _T("XC");   break;
	}
	
	nNumer = nDenom;
	
	switch (nNumer)
	{
    case 0: str1 = _T("");	   break;
    case 1: str1 = _T("I");    break;
    case 2: str1 = _T("II");   break;
    case 3: str1 = _T("III");  break;
    case 4: str1 = _T("IV");   break;
    case 5: str1 = _T("V");    break;
    case 6: str1 = _T("VI");   break;
    case 7: str1 = _T("VII");  break;
    case 8: str1 = _T("VIII"); break;
    case 9: str1 = _T("IX");   break;
	}
	
	string.Format(_T("%s%s%s%s"), str1000, str100, str10, str1);
	return string;
}


void CDlgNumeration::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);

	if (bShow)
	{
		if (m_pViewToUpdate1)
		{
			m_pViewToUpdate1->Invalidate();
			m_pViewToUpdate1->UpdateWindow();
		}
		if (m_pViewToUpdate2)
		{
			m_pViewToUpdate2->Invalidate();
			m_pViewToUpdate2->UpdateWindow();
		}
	}
}

void CDlgNumeration::PostNcDestroy() 
{
	if (m_pViewToUpdate1)
	{
		m_pViewToUpdate1->Invalidate();
		m_pViewToUpdate1->UpdateWindow();
	}
	if (m_pViewToUpdate2)
	{
		m_pViewToUpdate2->Invalidate();
		m_pViewToUpdate2->UpdateWindow();
	}
	
	CDialog::PostNcDestroy();
}
